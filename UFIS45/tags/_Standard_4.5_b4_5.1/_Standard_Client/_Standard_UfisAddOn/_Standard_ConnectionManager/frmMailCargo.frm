VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmMailCargo 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Mail and cargo connections ..."
   ClientHeight    =   10080
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   15270
   Icon            =   "frmMailCargo.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10080
   ScaleWidth      =   15270
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox pic 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   8895
      Left            =   225
      ScaleHeight     =   8895
      ScaleWidth      =   14970
      TabIndex        =   6
      Top             =   450
      Width           =   14970
      Begin VB.CommandButton cmdSearchArrival 
         Caption         =   "Find &Arrival"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1125
         TabIndex        =   33
         Top             =   1290
         Width           =   1755
      End
      Begin VB.TextBox txtArrOrig 
         Height          =   285
         Left            =   1125
         TabIndex        =   32
         Top             =   900
         Width           =   1755
      End
      Begin VB.TextBox txtArrFlno 
         Height          =   285
         Left            =   1125
         TabIndex        =   31
         Top             =   540
         Width           =   1755
      End
      Begin VB.TextBox txtSearch 
         Height          =   285
         Left            =   120
         TabIndex        =   26
         Top             =   8340
         Width           =   1695
      End
      Begin VB.CommandButton cmdSearchNext 
         Caption         =   "Search &next"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1860
         TabIndex        =   25
         Top             =   8340
         Width           =   1215
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "&Save"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4770
         TabIndex        =   24
         Top             =   8340
         Width           =   1170
      End
      Begin VB.CommandButton cmdDelete 
         Caption         =   "&Delete"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6120
         TabIndex        =   23
         Top             =   8340
         Width           =   1125
      End
      Begin VB.CommandButton cmdSelectAll 
         Caption         =   "="
         Height          =   255
         Left            =   120
         TabIndex        =   22
         ToolTipText     =   "Select all lines"
         Top             =   8040
         Width           =   255
      End
      Begin VB.CommandButton cmdDeselectAll 
         Caption         =   "O"
         Height          =   255
         Left            =   360
         TabIndex        =   21
         ToolTipText     =   "Deselect all"
         Top             =   8040
         Width           =   255
      End
      Begin VB.CommandButton cmdPrint 
         Caption         =   "&Print"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7425
         TabIndex        =   20
         Top             =   8340
         Width           =   1125
      End
      Begin VB.TextBox txtFlno 
         Height          =   285
         Left            =   12870
         TabIndex        =   16
         Top             =   540
         Width           =   1755
      End
      Begin VB.TextBox txtDest 
         Height          =   285
         Left            =   12870
         TabIndex        =   15
         Top             =   900
         Width           =   1755
      End
      Begin VB.CommandButton cmdSearchForChoiceList 
         Caption         =   "&Find Departure"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   12870
         TabIndex        =   14
         Top             =   1290
         Width           =   1755
      End
      Begin VB.CommandButton cmdAssignFlight 
         Caption         =   "<== &Add Departure Flight"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   435
         Left            =   11490
         TabIndex        =   12
         Top             =   7380
         Width           =   3315
      End
      Begin TABLib.TAB tabArrival 
         Height          =   6315
         Left            =   135
         TabIndex        =   10
         Top             =   1755
         Width           =   4440
         _Version        =   65536
         _ExtentX        =   7832
         _ExtentY        =   11139
         _StockProps     =   64
      End
      Begin TABLib.TAB tabConnections 
         Height          =   7485
         Left            =   4785
         TabIndex        =   11
         Top             =   570
         Width           =   6480
         _Version        =   65536
         _ExtentX        =   11430
         _ExtentY        =   13203
         _StockProps     =   64
      End
      Begin TABLib.TAB tabChoiceList 
         Height          =   5580
         Left            =   11490
         TabIndex        =   13
         Top             =   1755
         Width           =   3315
         _Version        =   65536
         _ExtentX        =   5847
         _ExtentY        =   9842
         _StockProps     =   64
      End
      Begin VB.Label Label7 
         BackStyle       =   0  'Transparent
         Caption         =   "Origin:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   405
         TabIndex        =   35
         Top             =   960
         Width           =   1035
      End
      Begin VB.Label Label6 
         BackStyle       =   0  'Transparent
         Caption         =   "FLNO:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   405
         TabIndex        =   34
         Top             =   600
         Width           =   555
      End
      Begin VB.Label lblTip 
         Alignment       =   2  'Center
         BackColor       =   &H80000007&
         Caption         =   "Mail"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   18
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FF00&
         Height          =   465
         Index           =   4
         Left            =   11490
         TabIndex        =   30
         Top             =   7830
         Width           =   3315
      End
      Begin VB.Label lblTip 
         BackColor       =   &H80000007&
         Caption         =   "Press <CTRL> for multi select (printing)"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FF00&
         Height          =   195
         Index           =   1
         Left            =   600
         TabIndex        =   29
         Top             =   8070
         Width           =   4005
      End
      Begin VB.Label lblTip 
         BackColor       =   &H80000007&
         Caption         =   "Press <Del> to delete ."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FF00&
         Height          =   195
         Index           =   2
         Left            =   4785
         TabIndex        =   28
         Top             =   8070
         Width           =   1995
      End
      Begin VB.Label lblTip 
         BackColor       =   &H80000007&
         Caption         =   "Double click to edit ..."
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FF00&
         Height          =   195
         Index           =   3
         Left            =   9270
         TabIndex        =   27
         Top             =   8070
         Width           =   1995
      End
      Begin VB.Label Label1 
         BackStyle       =   0  'Transparent
         Caption         =   "Search Departure Flights:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C000C0&
         Height          =   195
         Left            =   11670
         TabIndex        =   19
         Top             =   300
         Width           =   2955
      End
      Begin VB.Label Label4 
         BackStyle       =   0  'Transparent
         Caption         =   "FLNO:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   11790
         TabIndex        =   18
         Top             =   600
         Width           =   555
      End
      Begin VB.Label Label5 
         BackStyle       =   0  'Transparent
         Caption         =   "Destination:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   11790
         TabIndex        =   17
         Top             =   960
         Width           =   1035
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "Connecting flights:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   4770
         TabIndex        =   9
         Top             =   315
         Width           =   3435
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Search Arrival Flights:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00C000C0&
         Height          =   255
         Left            =   90
         TabIndex        =   8
         Top             =   315
         Width           =   3255
      End
      Begin VB.Label lblTip 
         Alignment       =   2  'Center
         BackColor       =   &H80000007&
         Caption         =   "* * *    Please double click header to sort the grids !   * * *"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000FF00&
         Height          =   195
         Index           =   0
         Left            =   0
         TabIndex        =   7
         Top             =   0
         Width           =   14940
      End
   End
   Begin TABLib.TAB tabDetPrint 
      Height          =   435
      Left            =   11400
      TabIndex        =   4
      Top             =   9420
      Visible         =   0   'False
      Width           =   1455
      _Version        =   65536
      _ExtentX        =   2566
      _ExtentY        =   767
      _StockProps     =   64
   End
   Begin TABLib.TAB tabPrint 
      Height          =   435
      Left            =   9600
      TabIndex        =   3
      Top             =   9420
      Visible         =   0   'False
      Width           =   1695
      _Version        =   65536
      _ExtentX        =   2990
      _ExtentY        =   767
      _StockProps     =   64
   End
   Begin TABLib.TAB tabTmp 
      Height          =   435
      Left            =   7500
      TabIndex        =   2
      Top             =   9450
      Visible         =   0   'False
      Width           =   1935
      _Version        =   65536
      _ExtentX        =   3413
      _ExtentY        =   767
      _StockProps     =   64
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "&Close"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5978
      TabIndex        =   0
      Top             =   9480
      Width           =   1395
   End
   Begin MSComctlLib.TabStrip TabStrip1 
      Height          =   9315
      Left            =   120
      TabIndex        =   1
      Top             =   60
      Width           =   15090
      _ExtentX        =   26617
      _ExtentY        =   16431
      HotTracking     =   -1  'True
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   2
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Mail"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "Cargo"
            ImageVarType    =   2
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblNewFlight 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000C0&
      Height          =   555
      Left            =   120
      TabIndex        =   5
      Top             =   9420
      Width           =   3255
   End
End
Attribute VB_Name = "frmMailCargo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public ogOldLineValues As String
Public ogCurrentEditLine As Long
Public isDirty As Boolean ' Changes which have not been saved
Public olLastShiftPosition As Long
Public strCurrentSelectedArrival As String ' Urno of the arrival in the left tab
Private imLastArrivalSelectedLine As Long

Private Declare Function GetPrivateProfileString Lib "kernel32" _
           Alias "GetPrivateProfileStringA" _
                 (ByVal sSectionName As String, _
                  ByVal sKeyName As String, _
                  ByVal sDefault As String, _
                  ByVal sReturnedString As String, _
                  ByVal lSize As Long, _
                  ByVal sFileName As String) As Long
                  

'-----------------------------------------------------------
' Checks for all lines in tabConnection, if not saved
' This is simply done by checking the line colors
'-----------------------------------------------------------
Public Function CheckDirty() As Boolean
    Dim cnt As Long
    Dim i As Long
    Dim tCol As Long
    Dim bCol As Long
    
    isDirty = False
    cnt = tabConnections.GetLineCount - 1
    For i = 0 To cnt
        tabConnections.GetLineColor i, tCol, bCol
        If bCol <> vbWhite Then
            i = cnt + 1
            isDirty = True
        End If
    Next i
    CheckDirty = isDirty
End Function


Private Sub btnShowFeedings_Click()
    Dim strUrno As String
    Dim llLine As Long
    Dim strLine As String
    Dim strAdid As String
    Dim strAftUrno As String
    Dim curSel As Long
    Dim strCaller As String
    Dim arr() As String
    Dim strFlno As String
    Dim strFlightDay As String
    Dim strRawFlno As String
    Dim dummy1 As String
    Dim dummy2 As String
    Dim dummy3 As String
    Dim datStoa As Date
    Dim strStoa As String
    Dim llAftLine As Long
    
    curSel = tabConnections.GetCurrentSelected
    If curSel > -1 Then
        frmData.tabData(0).GetLinesByIndexValue "URNO", tabConnections.GetFieldValue(curSel, "FLNU"), 0
        llLine = frmData.tabData(0).GetNextResultLine
        If llLine > -1 Then
            strStoa = frmData.tabData(0).GetFieldValue(llLine, "STOA")
            If strStoa <> "" Then
                datStoa = CedaFullDateToVb(strStoa)
            End If
            strRawFlno = tabConnections.GetFieldValue(curSel, "FLNO")
            arr = Split(strRawFlno, "/")
            If UBound(arr) = 1 Then
                strFlno = arr(0)
                strFlightDay = arr(1)
            Else
                strFlno = strRawFlno
                strFlightDay = ""
            End If
            strFlno = StripAftFlno(strFlno, dummy1, dummy2, dummy3)
            llAftLine = frmMain.GetCorrectDepartureByFlightDay(datStoa, strFlightDay, strFlno)
        End If
    End If
        
'    strLine = frmData.tabData(0).GetLinesByIndexValue("URNO", strAftUrno, 0)
'    llLine = frmData.tabData(0).GetNextResultLine()
    If llAftLine > -1 Then
        strAdid = frmData.tabData(0).GetFieldValue(llAftLine, "ADID")
        If strAdid = "D" Then
            strAftUrno = frmData.tabData(0).GetFieldValue(llAftLine, "URNO")
            If TabStrip1.SelectedItem.Index = 1 Then
                strCaller = "MAIL"
            Else
                strCaller = "CARGO"
            End If
            
            Load frmFeedingArrivals
            frmFeedingArrivals.Show , Me
            frmFeedingArrivals.strCalledFrom = strCaller
            frmFeedingArrivals.SetFlight strAftUrno
            frmFeedingArrivals.ShowFeedings
        End If
    End If
End Sub

Private Sub cmdAssignFlight_Click()
    Dim llCurrLine As Long
    Dim llArrCurrLine As Long
    Dim strUrnoD As String
    Dim strUrnoA As String
    Dim strFlno As String
    Dim strStod As String
    Dim strNewUrno As String
    Dim strEtod As String
    Dim strPos As String
    Dim strGate As String
    Dim strValues As String
    Dim llLine As String
    Dim strRet As String
    Dim strMailCargo As String
    Dim arr() As String
    Dim llCurrSelected As Long
    
    If TabStrip1.SelectedItem.Index = 1 Then strMailCargo = "M"
    If TabStrip1.SelectedItem.Index = 2 Then strMailCargo = "C"
    
    llCurrLine = tabChoiceList.GetCurrentSelected
    llArrCurrLine = tabArrival.GetCurrentSelected
    If llArrCurrLine < 0 Then
        MsgBox "Please select an arrival flight before you assign dapartures!"
        Exit Sub
    End If
    strNewUrno = frmData.GetNextUrno
    strUrnoA = tabArrival.GetFieldValue(llArrCurrLine, "URNO")
    '"URNO,CONT,FLNO,STA,ETA,DSSN,FLNU,TYPE,STYP,IDNT"
    If llCurrLine > -1 Then
        llCurrSelected = tabConnections.GetCurrentSelected
        If llCurrSelected > -1 Then
            tabConnections.ResetLineDecorations llCurrSelected
        End If
        strUrnoD = tabChoiceList.GetFieldValue(llCurrLine, "URNO")
        strRet = frmData.tabData(0).GetLinesByIndexValue("URNO", strUrnoD, 0)
        llLine = frmData.tabData(0).GetNextResultLine
        If llLine > -1 Then
            strPos = frmData.tabData(0).GetFieldValue(llLine, "PSTD")
            strGate = frmData.tabData(0).GetFieldValue(llLine, "GTD1")
            strStod = frmData.tabData(0).GetFieldValue(llLine, "STOD")
            strFlno = frmData.tabData(0).GetFieldValue(llLine, "FLNO") + "/" + Mid(strStod, 7, 2)
            arr = Split(strFlno, " ")
            If UBound(arr) = 1 Then
                strFlno = arr(0) + arr(1)
            Else
                strFlno = arr(0)
            End If
            strEtod = frmData.tabData(0).GetFieldValue(llLine, "ETDI")
            strValues = strNewUrno + "," + strFlno + ",," + strStod + "," + strEtod + ","
            strValues = strValues + strPos + "," + strGate + ","
            strValues = strValues + "HUB," + strUrnoA + ",LOA," + strMailCargo + "," + strUrnoD
            tabConnections.InsertTextLine strValues, False
            tabConnections.SetLineTag tabConnections.GetLineCount - 1, ""
            tabConnections.SetLineColor tabConnections.GetLineCount - 1, vbBlack, vbYellow
        End If
    End If
    tabConnections.Sort "3", True, True
    tabConnections.Refresh
    
    'And conflict check for the new line and arrival tab
    If HasNewArrivalConflict(llArrCurrLine) = True Then
        tabArrival.SetLineStatusValue llArrCurrLine, 1
        SetArrivalListLineColor llArrCurrLine
        tabArrival.Refresh
    End If
End Sub


Private Sub cmdClose_Click()
    Unload Me
End Sub

Private Sub cmdDelete_Click()
    SetLineDeleted
End Sub

Private Sub cmdDeselectAll_Click()
    Dim cnt As Long
    Dim i As Long
    
    cnt = tabArrival.GetLineCount - 1
    For i = 0 To cnt
        tabArrival.SetLineTag i, ""
        SetArrivalListLineColor i
    Next
    olLastShiftPosition = -1
    tabArrival.Refresh
End Sub

Public Sub SetArrivalListLineColor(tabLine As Long)
    Dim strTag As String
    Dim llStatusValue As Long
    
    strTag = tabArrival.GetLineTag(tabLine)
    llStatusValue = tabArrival.GetLineStatusValue(tabLine)
    If strTag = "SELECTED" And llStatusValue = 0 Then 'Selected with no conflict
        tabArrival.SetLineColor tabLine, vbBlack, vbCyan
    End If
    If strTag = "" And llStatusValue = 0 Then ' Not Selected with no conflict
        tabArrival.SetLineColor tabLine, vbBlack, vbWhite
    End If
    If strTag = "SELECTED" And llStatusValue = 1 Then 'Selected with conflict
        tabArrival.SetLineColor tabLine, vbBlack, colOrange
    End If
    If strTag = "" And llStatusValue = 1 Then ' Not Selected with conflict
        tabArrival.SetLineColor tabLine, vbBlack, vbRed
    End If
    
End Sub
Private Sub cmdPrint_Click()
    Dim strRet As String
    Dim strRet2 As String
    Dim llLine As Long
    Dim strAftUrno As String
    Dim llAftLine As Long
    Dim strDepUrno As String
    Dim myIDX As Integer
    Dim strValues As String
    Dim strArrValues As String
    Dim strStod As String
    Dim strFlno As String
    Dim strEtod As String
    Dim strPos As String
    Dim strGate As String
    Dim strMC As String
    Dim i As Long
    Dim j As Long
    Dim tCol As Long
    Dim bCol As Long
    Dim rpt As New rptMailCargo
    
    'Const sFile As String = "C:\Ufis\System\Ceda.ini"
    Dim sString As String
    Dim lSize As Long
    Dim lReturn As Long
    sString = String$(255, " ")
    lSize = Len(sString)
    
' CEDA.INI file path. The path of the BMP file is taken from the parameter 'PRINTLOGO' under 'HUBMANAGER'
    lReturn = GetPrivateProfileString("HUBMANAGER", "PRINTLOGO", "", sString, lSize, DEFAULT_CEDA_INI)
    sString = Trim(sString)
    sString = Left$(sString, Len(sString) - 1)
    
    Dim fso As FileSystemObject
    Set fso = New FileSystemObject
    
    If lReturn > 0 Then
        If fso.FileExists(sString) Then
            rpt.PrintLogo.Picture = LoadPicture(sString)
        Else
            MsgBox "Invalid print Logo bitmap file Path in CEDA.INI", vbExclamation + vbOKOnly, "Error"
        End If
    End If
    
    tabPrint.ResetContent
    tabPrint.HeaderString = "MAILCARGO,FLNO_A,STA,ETA,CONT,FLNO_D,STD,ETD,POS,GATE"
    tabPrint.LogicalFieldList = "MAILCARGO,FLNO_A,STA,ETA,CONT,FLNO_D,STD,ETD,POS,GATE"
    tabPrint.HeaderLengthString = "80,80,80,80,80,80,80,80"
    
    tabDetPrint.ResetContent
    tabDetPrint.HeaderString = "CONT,FLNO_D,STD,ETD,POS,GATE"
    tabDetPrint.LogicalFieldList = "CONT,FLNO_D,STD,ETD,POS,GATE"
    tabDetPrint.HeaderLengthString = "80,80,80,80,80,80"
    
    
    For i = 0 To tabArrival.GetLineCount - 1
        tabArrival.GetLineColor i, tCol, bCol
        If bCol = vbCyan Or bCol = colOrange Then
            
            If TabStrip1.SelectedItem.Index = 1 Then
                myIDX = 6
                strMC = "M"
            Else
                myIDX = 7
                strMC = "C"
            End If
            strArrValues = strMC + ","
            strArrValues = strArrValues + tabArrival.GetFieldValue(i, "FLNO") + ","
            strArrValues = strArrValues + tabArrival.GetFieldValue(i, "STA") + ","
            strArrValues = strArrValues + tabArrival.GetFieldValue(i, "ETA") + ","
            If i >= 0 Then
                strAftUrno = tabArrival.GetFieldValue(i, "URNO")
                strRet = frmData.tabData(myIDX).GetLinesByIndexValue("FLNU", strAftUrno, 0)
                llLine = frmData.tabData(myIDX).GetNextResultLine
                tabDetPrint.ResetContent
                While llLine > -1
                    '"URNO,CONT,FLNO,STD,ETD,DSSN,FLNU,TYPE,STYP,IDNT"
                    If strMC = frmData.tabData(myIDX).GetFieldValue(llLine, "STYP") Then
                        strDepUrno = frmData.tabData(myIDX).GetFieldValue(llLine, "IDNT")
                        If strDepUrno <> "" Then
                            strStod = ""
                            strFlno = ""
                            strEtod = ""
                            strRet2 = frmData.tabData(0).GetLinesByIndexValue("URNO", strDepUrno, 0)
                            llAftLine = frmData.tabData(0).GetNextResultLine
                            If llLine > -1 Then
                                strStod = frmData.tabData(0).GetFieldValue(llAftLine, "STOD")
                                strFlno = frmData.tabData(0).GetFieldValue(llAftLine, "FLNO")
                                strEtod = frmData.tabData(0).GetFieldValue(llAftLine, "ETDI")
                                strPos = frmData.tabData(0).GetFieldValue(llAftLine, "PSTD")
                                strGate = frmData.tabData(0).GetFieldValue(llAftLine, "GTD1")
                            End If
                            strValues = "" 'strArrValues
                            'strValues = strValues + frmData.tabData(myIDX).GetFieldValue(llLine, "VALU") + ","
                            strValues = strValues + frmData.tabData(myIDX).GetFieldValue(llLine, "ADDI") + ","
                            strValues = strValues + frmData.tabData(myIDX).GetFieldValue(llLine, "FLNO") + ","
                            strValues = strValues + strStod + "," + strEtod + ","
                            strValues = strValues + strPos + "," + strGate
                            tabDetPrint.InsertTextLine strValues, False
                        End If
                    End If
                    llLine = frmData.tabData(myIDX).GetNextResultLine
                Wend
                tabDetPrint.Refresh
                tabDetPrint.Sort "2", True, True
                For j = 0 To tabDetPrint.GetLineCount - 1
                    strValues = strArrValues
                    strValues = strValues + tabDetPrint.GetFieldValues(j, "CONT,FLNO_D,STD,ETD,POS,GATE")
                    tabPrint.InsertTextLine strValues, False
                Next j
            End If
        End If
    Next i
    rpt.Show

End Sub

Private Sub cmdSave_Click()
    Dim strMC As String
    Dim strLine As String
    Dim strVal As String
    Dim strRet As String
    Dim llLine As Long
    Dim i As Long
    Dim cnt As Long
    Dim strCommand As String
    Dim bCol As Long
    Dim tCol As Long
    Dim strUrno As String 'Long 'urno for update/delete where clause
    Dim myIDX As Integer
    Dim strFieldList As String
    Dim strLinesToDelete As String
    Dim strFrmDataToDelete As String
    Dim strCont As String
    
    cnt = tabConnections.GetLineCount - 1
    'LOATABM;URNO,FLNU,FLNO,DSSN,TYPE,STYP,VALU,IDNT;
    'LOATABC;URNO,FLNU,FLNO,DSSN,TYPE,STYP,VALU,IDNT;
    '"URNO,CONT,FLNO,STD,ETD,DSSN,FLNU,TYPE,STYP,IDNT"
    llLine = tabConnections.GetCurrentSelected
    tabConnections.ResetLineDecorations llLine
    'strFieldList = "URNO,FLNU,FLNO,DSSN,TYPE,STYP,VALU,IDNT"
    strFieldList = "URNO,FLNU,FLNO,DSSN,TYPE,STYP,ADDI,IDNT"
    For i = 0 To cnt
        strCommand = ""
        tabConnections.GetLineColor i, tCol, bCol
        tabConnections.ResetLineDecorations i
        If bCol <> vbWhite Then
            strUrno = tabConnections.GetFieldValues(i, "URNO")
            strMC = tabConnections.GetFieldValues(i, "STYP")
            If strMC = "M" Then myIDX = 6
            If strMC = "C" Then myIDX = 7
            strRet = frmData.tabData(myIDX).GetLinesByIndexValue("URNO", strUrno, 0)
            llLine = frmData.tabData(myIDX).GetNextResultLine
            strCont = tabConnections.GetFieldValues(i, "CONT")
            strVal = tabConnections.GetFieldValues(i, "URNO,FLNU,FLNO,DSSN") + ","
            strVal = strVal + tabConnections.GetFieldValues(i, "TYPE,STYP") + ","
            strVal = strVal + strCont + ","
            strVal = strVal + tabConnections.GetFieldValues(i, "IDNT")
            tabConnections.SetLineTag i, strCont
            Select Case (bCol)
                Case vbYellow
                    strCommand = "IRT"
                    'Insert an empty line and then set the values
                    frmData.tabData(myIDX).InsertTextLine ",,,,,,,,", False
                    frmData.tabData(myIDX).SetFieldValues frmData.tabData(myIDX).GetLineCount - 1, strFieldList, strVal
                    strVal = CleanNullValues(strVal)
                    frmData.SetServerParameters
                    If frmData.aUfis.CallServer("IRT", "LOATAB", strFieldList, strVal, "", "230") <> 0 Then
                            MsgBox "Insert Failed!!" & vbLf & frmData.aUfis.LastErrorMessage
                    End If
                Case vbGreen
                    strCommand = "URT"
                    If llLine > -1 Then
                        frmData.tabData(myIDX).SetFieldValues llLine, strFieldList, strVal
                        strVal = CleanNullValues(strVal)
                        frmData.SetServerParameters
                        If frmData.aUfis.CallServer("URT", "LOATAB", strFieldList, strVal, "WHERE URNO=" & strUrno, "230") <> 0 Then
                            MsgBox "Update Failed!!" & vbLf & frmData.aUfis.LastErrorMessage
                        End If
                    Else
                        frmData.tabData(myIDX).InsertTextLine strVal, False
                        strVal = CleanNullValues(strVal)
                        frmData.SetServerParameters
                        If frmData.aUfis.CallServer("IRT", "LOATAB", strFieldList, strVal, "", "230") <> 0 Then
                            MsgBox "Insert Failed!!" & vbLf & frmData.aUfis.LastErrorMessage
                        End If
                    End If
                Case colOrange
                    strCommand = "DRT"
                    If llLine > -1 Then
                        tabTmp.InsertTextLine CStr(llLine), False
                        strLinesToDelete = strLinesToDelete + CStr(i) + ","
                        frmData.SetServerParameters
                        If frmData.aUfis.CallServer("DRT", "LOATAB", "", "", "WHERE URNO=" & strUrno, "230") <> 0 Then
                            MsgBox "Delete Failed!!" & vbLf & frmData.aUfis.LastErrorMessage
                        End If
                    End If
            End Select
            tabConnections.SetLineColor i, tCol, vbWhite
            tabConnections.Refresh
        End If
    Next i
    If Len(strLinesToDelete) > 0 Then
        If Mid(strLinesToDelete, Len(strLinesToDelete), 1) = "," Then
            strLinesToDelete = Mid(strLinesToDelete, 1, Len(strLinesToDelete) - 1)
        End If
        cnt = ItemCount(strLinesToDelete, ",")
        For i = cnt - 1 To 0 Step -1
            llLine = GetRealItem(strLinesToDelete, CInt(i), ",")
            tabConnections.DeleteLine llLine
        Next i
    End If
    tabTmp.Sort "0", True, True
    cnt = tabTmp.GetLineCount - 1
    For i = cnt To 0 Step -1
        frmData.tabData(myIDX).DeleteLine Val(tabTmp.GetColumnValue(i, 0))
    Next i
    tabConnections.Refresh
    tabTmp.ResetContent
    'Sort and recreate the indexes
    frmData.tabData(6).Sort "0", True, True
    frmData.tabData(7).Sort "0", True, True
End Sub

Private Sub cmdSearchArrival_Click()
    Dim i As Long
    Dim strVal As String
    Dim strFlno As String
    Dim strOrig As String
    
    tabArrival.ResetContent
    For i = 0 To frmData.tabData(0).GetLineCount - 1
        If frmData.tabData(0).GetFieldValue(i, "ADID") = "A" Then
            strFlno = frmData.tabData(0).GetFieldValue(i, "FLNO")
            strOrig = frmData.tabData(0).GetFieldValue(i, "ORG3")
            If InStr(1, strFlno, txtArrFlno.Text) > 0 And _
               InStr(1, strOrig, txtArrOrig.Text) > 0 Then
               
               tabArrival.InsertTextLine frmData.tabData(0).GetFieldValues(i, "URNO,FLNO,STOA,ETAI,ORG3"), False
            End If
        End If
    Next i
    tabArrival.Sort "2", True, True
    tabArrival.Refresh
    CheckAllConflicts
End Sub

Private Sub cmdSearchForChoiceList_Click()
    Dim i As Long
    Dim strVal As String
    Dim strFlno As String
    Dim strDest As String
    
    tabChoiceList.ResetContent
    For i = 0 To frmData.tabData(0).GetLineCount - 1
        If frmData.tabData(0).GetFieldValue(i, "ADID") = "D" Then
            strFlno = frmData.tabData(0).GetFieldValue(i, "FLNO")
            strDest = frmData.tabData(0).GetFieldValue(i, "DES3")
            If InStr(1, strFlno, txtFlno.Text) > 0 And _
               InStr(1, strDest, txtDest.Text) > 0 Then
               tabChoiceList.InsertTextLine frmData.tabData(0).GetFieldValues(i, "URNO,FLNO,TIFD,DES3"), False
            End If
        End If
    Next i
    tabChoiceList.Sort "2", True, True
    tabChoiceList.Refresh
End Sub

Private Sub cmdSearchNext_Click()
    Dim llCurrLine As String
    Dim i As Long
    Dim strLine As String
    Dim blFound As Boolean
    Dim oldSel As Long
    Dim strFlno As String
    Dim strStoa As String
    Dim strEtoa As String
    Dim d As Date
    
    blFound = False
    llCurrLine = tabArrival.GetCurrentSelected
    If llCurrLine = -1 Then
        llCurrLine = 0
        For i = 0 To tabArrival.GetLineCount - 1
            strFlno = tabArrival.GetFieldValue(i, "FLNO")
            strLine = strFlno
            strStoa = tabArrival.GetFieldValue(i, "STA")
            If strStoa <> "" Then
                strLine = strLine + Mid(strStoa, 7, 2) + "/"
                strLine = strLine + Mid(strStoa, 9, 2) + ":"
                strLine = strLine + Mid(strStoa, 11, 2)
            End If
            strEtoa = tabArrival.GetFieldValue(i, "ETA")
            If strEtoa <> "" Then
                strLine = strLine + Mid(strEtoa, 7, 2) + "/"
                strLine = strLine + Mid(strEtoa, 9, 2) + ":"
                strLine = strLine + Mid(strEtoa, 11, 2)
            End If
            If InStr(1, strLine, txtSearch) > 0 Then
                tabArrival.SetCurrentSelection i
                tabArrival.OnVScrollTo i
                i = tabArrival.GetLineCount
                blFound = True
            End If
        Next i
    Else
        oldSel = llCurrLine
        For i = llCurrLine + 1 To tabArrival.GetLineCount - 1
            strFlno = tabArrival.GetFieldValue(i, "FLNO")
            strLine = strFlno
            strStoa = tabArrival.GetFieldValue(i, "STA")
            If strStoa <> "" Then
                strLine = strLine + Mid(strStoa, 7, 2) + "/"
                strLine = strLine + Mid(strStoa, 9, 2) + ":"
                strLine = strLine + Mid(strStoa, 11, 2)
            End If
            strEtoa = tabArrival.GetFieldValue(i, "ETA")
            If strEtoa <> "" Then
                strLine = strLine + Mid(strEtoa, 7, 2) + "/"
                strLine = strLine + Mid(strEtoa, 9, 2) + ":"
                strLine = strLine + Mid(strEtoa, 11, 2)
            End If
            If InStr(1, strLine, txtSearch) > 0 Then
                tabArrival.SetCurrentSelection i
                tabArrival.OnVScrollTo i
                i = tabArrival.GetLineCount
                blFound = True
            End If
        Next i
        If blFound = False Then ' start from top to olcurr
                For i = 0 To llCurrLine
                strFlno = tabArrival.GetFieldValue(i, "FLNO")
                strLine = strFlno
                strStoa = tabArrival.GetFieldValue(i, "STA")
                If strStoa <> "" Then
                    strLine = strLine + Mid(strStoa, 7, 2) + "/"
                    strLine = strLine + Mid(strStoa, 9, 2) + ":"
                    strLine = strLine + Mid(strStoa, 11, 2)
                End If
                strEtoa = tabArrival.GetFieldValue(i, "ETA")
                If strEtoa <> "" Then
                    strLine = strLine + Mid(strEtoa, 7, 2) + "/"
                    strLine = strLine + Mid(strEtoa, 9, 2)
                    strLine = strLine + Mid(strEtoa, 11, 2)
                End If
                If InStr(1, strLine, txtSearch) > 0 Then
                    tabArrival.SetCurrentSelection i
                    tabArrival.OnVScrollTo i
                    i = tabArrival.GetLineCount
                    blFound = True
                End If
            Next i
        End If
    End If
    If blFound = False Then
        MsgBox "No matching line found!" ', vbSystemModal
        Me.ZOrder
    End If
End Sub


Private Sub cmdSelectAll_Click()
    Dim cnt As Long
    Dim i As Long
    Dim tCol As Long
    Dim bCol As Long
    
    cnt = tabArrival.GetLineCount - 1
    For i = 0 To cnt
        tabArrival.GetLineColor i, tCol, bCol
        If bCol <> vbRed And bCol <> colOrange Then
            tabArrival.SetLineColor i, vbBlack, vbCyan
        Else
            bCol = colOrange
            tabArrival.SetLineColor i, vbBlack, bCol
        End If
    Next
    tabArrival.Refresh
End Sub


Private Sub Form_Load()
    Dim i As Long
    Dim f As Field
    
    imLastArrivalSelectedLine = -1
   ' myFrame.backColor = vbButtonFace
    tabTmp.ResetContent
    tabTmp.HeaderString = "LINE"
    tabTmp.LogicalFieldList = "LINE"
    tabTmp.HeaderLengthString = "100"
    tabTmp.LineHeight = 16
    tabTmp.FontName = "Courier New"
    tabTmp.FontSize = 16
    'tabTmp.SetHeaderFont 18, True, False, True, 0, "Courier New"
    tabTmp.EnableHeaderSizing True
    tabTmp.SetTabFontBold True
    
    tabArrival.ResetContent
    tabArrival.HeaderString = "URNO,FLNO,STA,ETA,ORIG"
    tabArrival.LogicalFieldList = "URNO,FLNO,STA,ETA,ORG3"
    tabArrival.HeaderLengthString = "0,80,80,80,70"
    tabArrival.LineHeight = 16
    tabArrival.FontName = "Courier New"
    tabArrival.FontSize = 16
    tabArrival.ShowRowSelection = False
    'tabArrival.SetHeaderFont 20, False, False, True, 0, "Arial"
    tabArrival.EnableHeaderSizing True
    tabArrival.SetInternalLineBuffer True
    tabArrival.SetTabFontBold True
    For i = 2 To 3
        tabArrival.DateTimeSetColumn i
        tabArrival.DateTimeSetColumnFormat i, "YYYYMMDDhhmmss", "DD'/'hh':'mm"
    Next i
    tabArrival.IndexCreate "URNO", 0
    
    tabConnections.ResetContent
    'IDNT is used to store the departure urno for eay find
    tabConnections.HeaderString = "URNO,FLNO,CONT,STD,ETD,Pos,Gate,DSSN,FLNU,TYPE,STYP,IDNT"
    tabConnections.LogicalFieldList = "URNO,FLNO,CONT,STD,ETD,POS,GATE,DSSN,FLNU,TYPE,STYP,IDNT"
    tabConnections.ColumnWidthString = "20,20,128,20,20,20,20,20,20,20,20,20"
    tabConnections.HeaderLengthString = "0,80,120,70,70,50,50,0,0,0,0,0"
    tabConnections.NoFocusColumns = "0,1,3,4,5,6,7,8,9,10,11"
    tabConnections.LineHeight = 16
    tabConnections.InplaceEditUpperCase = False
    tabConnections.EnableInlineEdit True
    tabConnections.FontName = "Courier New"
    tabConnections.ShowHorzScroller True
    tabConnections.ShowRowSelection = False
    tabConnections.FontSize = 16
    'tabConnections.SetHeaderFont 20, False, False, True, 0, "Arial"
    tabConnections.EnableHeaderSizing True
    tabConnections.SetTabFontBold True
    tabConnections.InplaceEditSendKeyEvents = False
    tabConnections.SetInternalLineBuffer True
    tabConnections.IndexCreate "URNO_D", 11
    tabConnections.IndexCreate "ARR_URNO", 8

    For i = 3 To 4
        tabConnections.DateTimeSetColumn i
        tabConnections.DateTimeSetColumnFormat i, "YYYYMMDDhhmmss", "DD'/'hh':'mm"
    Next i
    
    tabChoiceList.ResetContent
    tabChoiceList.HeaderString = "Urno,Flno,Time,Dest."
    tabChoiceList.LogicalFieldList = "URNO,FLNO,TIFD,DES3"
    tabChoiceList.HeaderLengthString = "0,70,70,70"
    tabChoiceList.FontName = "Courier New"
    tabChoiceList.LineHeight = 16
    tabChoiceList.FontSize = 16
    tabChoiceList.SetTabFontBold True
    tabChoiceList.DateTimeSetColumn 2
    tabChoiceList.DateTimeSetColumnFormat 2, "YYYYMMDDhhmmss", "DD'/'hh':'mm"
    tabChoiceList.Refresh
    
    For i = 0 To frmData.tabData(0).GetLineCount - 1
        If frmData.tabData(0).GetFieldValue(i, "ADID") = "A" Then
            tabArrival.InsertTextLine frmData.tabData(0).GetFieldValues(i, "URNO,FLNO,STOA,ETAI,ORG3"), False
        End If
    Next i
    tabArrival.Sort "2", True, True
    tabArrival.Refresh
    If InStr(Command, "_DEBUG") = 0 Then
        'SetFormOnTop Me, True
    End If
    For i = 0 To lblTip.count - 1
        If ShowHints = True Then
            lblTip(i).Visible = True
        Else
            lblTip(i).Visible = False
        End If
    Next i
    
    CheckAllConflicts
    DrawBackGround pic, 7, True, True
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If CheckDirty = True Then
        If MsgBox("You have made changes. Do you want to save ?", vbYesNo, "Note") = vbYes Then
            cmdSave_Click
        End If
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    frmMain.bgMailCargoIsActive = False
    frmMain.cbToolButton(5).Value = 0
    frmMain.cbToolButton(5).backColor = vbButtonFace
    frmMain.RefreshAllControls
End Sub


Private Sub tabArrival_RowSelectionChanged(ByVal LineNo As Long, ByVal Selected As Boolean)
    Dim strColor As String
    Dim i As Long
    Dim tCol As Long
    Dim bCol As Long
    
    If CheckDirty = True Then
        If MsgBox("You have made changes. Do you want to save ?", vbYesNo, "Note") = vbYes Then
            cmdSave_Click
        End If
    End If
    If Selected = False Then
        tabArrival.ResetLineDecorations LineNo
        imLastArrivalSelectedLine = LineNo
        
    Else
        If imLastArrivalSelectedLine > -1 Then
            tabArrival.GetLineColor imLastArrivalSelectedLine, tCol, bCol
            If HasArrivalConflict(imLastArrivalSelectedLine) = True Then
                tabArrival.SetLineStatusValue imLastArrivalSelectedLine, 1
                SetArrivalListLineColor imLastArrivalSelectedLine
            Else
                tabArrival.SetLineStatusValue imLastArrivalSelectedLine, 0
                SetArrivalListLineColor imLastArrivalSelectedLine
            End If
        End If
        strColor = vbBlue & "," & vbBlue
        tabArrival.CreateDecorationObject "CURSOR", "T,B", "2,2", strColor
        For i = 0 To ItemCount(tabArrival.HeaderString, ",") - 1
            tabArrival.SetDecorationObject LineNo, i, "CURSOR"
        Next i
        If Abs(GetKeyState(vbKeyShift)) > 1 Then
            If olLastShiftPosition > -1 Then
                If LineNo >= olLastShiftPosition Then
                    For i = olLastShiftPosition + 1 To LineNo
                        SetLineSelected i
                        SetArrivalListLineColor i
                    Next i
                Else
                    For i = LineNo To olLastShiftPosition - 1
                        SetLineSelected i
                        SetArrivalListLineColor i
                    Next i
                End If
            Else
                olLastShiftPosition = LineNo
            End If
        End If
        If Abs(GetKeyState(vbKeyShift)) > 1 Then
            olLastShiftPosition = LineNo
        End If
        strCurrentSelectedArrival = tabArrival.GetFieldValue(LineNo, "URNO")
        ShowConnectionEntries LineNo
        HasNewArrivalConflict LineNo
    End If
    tabArrival.Refresh

End Sub

Private Sub tabArrival_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim retKeyState As Integer
    
    retKeyState = Abs(GetKeyState(vbKeyControl))
    If retKeyState > 1 Then
        SetLineSelected LineNo
        olLastShiftPosition = LineNo
        SetArrivalListLineColor LineNo
    Else
        strCurrentSelectedArrival = tabArrival.GetFieldValue(LineNo, "URNO")
        ShowConnectionEntries LineNo
        HasNewArrivalConflict LineNo
    End If
End Sub

Public Sub SetLineSelected(ByVal LineNo As Long)
    Dim tCol As Long
    Dim bCol As Long
    
    tabArrival.GetLineColor LineNo, tCol, bCol
    If tabArrival.GetLineTag(LineNo) = "SELECTED" Then
        tabArrival.SetLineTag LineNo, ""
    Else
        tabArrival.SetLineTag LineNo, "SELECTED"
    End If
End Sub
Sub ShowConnectionEntries(LineNo As Long)
    Dim strRet As String
    Dim strRet2 As String
    Dim llLine As Long
    Dim strAftUrno As String
    Dim llAftLine As Long
    Dim strDepUrno As String
    Dim myIDX As Integer
    Dim strValues As String
    Dim strStod As String
    Dim strFlno As String
    Dim strEtod As String
    Dim strPos As String
    Dim strGate As String
    Dim strMC As String
    
    If TabStrip1.SelectedItem.Index = 1 Then
        myIDX = 6
        strMC = "M"
    Else
        myIDX = 7
        strMC = "C"
    End If
    
    If LineNo >= 0 Then
        strAftUrno = tabArrival.GetFieldValue(LineNo, "URNO")
        strRet = frmData.tabData(myIDX).GetLinesByIndexValue("FLNU", strAftUrno, 0)
        llLine = frmData.tabData(myIDX).GetNextResultLine
        tabConnections.ResetContent
        
        While llLine > -1
            '"URNO,CONT,FLNO,STD,ETD,DSSN,FLNU,TYPE,STYP,IDNT"
            If strMC = frmData.tabData(myIDX).GetFieldValue(llLine, "STYP") Then
                strDepUrno = frmData.tabData(myIDX).GetFieldValue(llLine, "IDNT")
                If strDepUrno <> "" Then
                    strStod = ""
                    strFlno = ""
                    strEtod = ""
                    strRet2 = frmData.tabData(0).GetLinesByIndexValue("URNO", strDepUrno, 0)
                    llAftLine = frmData.tabData(0).GetNextResultLine
                    If llLine > -1 Then
                        strStod = frmData.tabData(0).GetFieldValue(llAftLine, "STOD")
                        strFlno = frmData.tabData(0).GetFieldValue(llAftLine, "FLNO")
                        strEtod = frmData.tabData(0).GetFieldValue(llAftLine, "ETDI")
                        strPos = frmData.tabData(0).GetFieldValue(llAftLine, "PSTD")
                        strGate = frmData.tabData(0).GetFieldValue(llAftLine, "GTD1")
                    End If
                    strValues = frmData.tabData(myIDX).GetFieldValue(llLine, "URNO") + ","
                    '"URNO,FLNO,CONT,STD,ETD,POS,GATE,DSSN,FLNU,TYPE,STYP,IDNT"
                    strValues = strValues + frmData.tabData(myIDX).GetFieldValue(llLine, "FLNO") + ","
                    strValues = strValues + frmData.tabData(myIDX).GetFieldValue(llLine, "ADDI") + ","
                    strValues = strValues + strStod + "," + strEtod + "," + strPos + "," + strGate + ","
                    strValues = strValues + frmData.tabData(myIDX).GetFieldValue(llLine, "DSSN") + ","
                    strValues = strValues + frmData.tabData(myIDX).GetFieldValue(llLine, "FLNU") + ","
                    strValues = strValues + frmData.tabData(myIDX).GetFieldValue(llLine, "TYPE") + ","
                    strValues = strValues + frmData.tabData(myIDX).GetFieldValue(llLine, "STYP") + ","
                    strValues = strValues + frmData.tabData(myIDX).GetFieldValue(llLine, "IDNT")
                    tabConnections.InsertTextLine strValues, False
                    'tabConnections.SetLineTag tabConnections.GetLineCount - 1, frmData.tabData(myIDX).GetFieldValue(llLine, "VALU")
                    tabConnections.SetLineTag tabConnections.GetLineCount - 1, frmData.tabData(myIDX).GetFieldValue(llLine, "ADDI")
                    tabConnections.SetLineColor tabConnections.GetLineCount - 1, vbBlack, vbWhite
                End If
            End If
            llLine = frmData.tabData(myIDX).GetNextResultLine
        Wend
        tabConnections.Sort "3", True, True
        tabConnections.IndexCreate "URNO_D", 11
        tabConnections.IndexCreate "ARR_URNO", 8
        tabConnections.Refresh
    End If

End Sub

Private Sub tabArrival_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim i As Long
    Dim cnt As Long
    
    If LineNo = -1 Then
        For i = 0 To tabArrival.GetLineCount - 1
            tabArrival.ResetLineDecorations i
        Next
        If ColNo = tabArrival.CurrentSortColumn Then
            If tabArrival.SortOrderASC = True Then
                tabArrival.Sort CStr(ColNo), False, True
            Else
                tabArrival.Sort CStr(ColNo), True, True
            End If
        Else
            tabArrival.Sort CStr(ColNo), True, True
        End If
        lblNewFlight.Caption = "" ' Hide the hint for new flight insertion, due to new order
        tabArrival.Refresh
    End If
End Sub

Private Sub tabChoiceList_RowSelectionChanged(ByVal LineNo As Long, ByVal Selected As Boolean)
    Dim strUrno As String
    Dim strCaller As String
    
    If LineNo > -1 And Selected = True Then
        strUrno = tabChoiceList.GetFieldValue(LineNo, "URNO")
        If TabStrip1.SelectedItem.Index = 1 Then
            strCaller = "MAIL"
        Else
            strCaller = "CARGO"
        End If
        
        Load frmFeedingArrivals
        frmFeedingArrivals.Show , Me
        frmFeedingArrivals.strCalledFrom = strCaller
        frmFeedingArrivals.SetFlight strUrno
        frmFeedingArrivals.ShowFeedings
        tabChoiceList.SetFocus
    End If
End Sub

Private Sub tabChoiceList_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim strUrno As String
    Dim strCaller As String
    
    If LineNo > -1 Then
        strUrno = tabChoiceList.GetFieldValue(LineNo, "URNO")
        If TabStrip1.SelectedItem.Index = 1 Then
            strCaller = "MAIL"
        Else
            strCaller = "CARGO"
        End If
        
        Load frmFeedingArrivals
        frmFeedingArrivals.Show , Me
        frmFeedingArrivals.strCalledFrom = strCaller
        frmFeedingArrivals.SetFlight strUrno
        frmFeedingArrivals.ShowFeedings
        tabChoiceList.SetFocus
    End If
End Sub

Private Sub tabChoiceList_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim i As Long
    Dim cnt As Long
    
    If LineNo = -1 Then
        If ColNo = tabChoiceList.CurrentSortColumn Then
            If tabChoiceList.SortOrderASC = True Then
                tabChoiceList.Sort CStr(ColNo), False, True
            Else
                tabChoiceList.Sort CStr(ColNo), True, True
            End If
        Else
            tabChoiceList.Sort CStr(ColNo), True, True
        End If
        tabChoiceList.Refresh
    End If
End Sub

Private Sub tabConnections_CloseInplaceEdit(ByVal LineNo As Long, ByVal ColNo As Long)
    CheckValuesChanged LineNo
End Sub

Private Sub tabConnections_EditPositionChanged(ByVal LineNo As Long, ByVal ColNo As Long, ByVal Value As String)
    CheckValuesChanged LineNo
End Sub

Public Sub CheckValuesChanged(LineNo As Long)
    Dim strColor As String
    Dim i As Long
    Dim strCont As String
    
    Dim tCol As Long
    Dim bCol As Long
    
    tabConnections.GetLineColor LineNo, tCol, bCol
    If LineNo >= 0 Then
        strCont = tabConnections.GetFieldValue(LineNo, "CONT")
        If Len(strCont) > 127 Then
            strCont = Mid(strCont, 1, 128)
            tabConnections.SetFieldValues LineNo, "CONT", strCont
        End If
        If strCont <> tabConnections.GetLineTag(LineNo) Then
            If bCol = vbWhite Then
                tabConnections.SetLineColor LineNo, tCol, vbGreen
            End If
            If bCol = colOrange Then
                'tabConnections.SetLineColor LineNo, tCol, vbGreen
            End If
        Else
            If bCol = vbGreen Then
                tabConnections.SetLineColor LineNo, tCol, vbWhite
            End If
        End If
        tabConnections.Refresh
    End If
End Sub

Private Sub tabConnections_HitKeyOnLine(ByVal Key As Integer, ByVal LineNo As Long)
    Dim llCurrLine As Long
    Dim colBack As Long
    Dim colText As Long
    If Key = vbKeyDelete Then
        SetLineDeleted
    End If
    tabConnections.Refresh
End Sub


Private Sub tabConnections_RowSelectionChanged(ByVal LineNo As Long, ByVal Selected As Boolean)
    Dim strColor As String
    Dim i As Long
    Dim strCont As String
    
    CheckValuesChanged LineNo
    If Selected = False Then
        tabConnections.ResetLineDecorations LineNo
    Else
        strColor = vbBlue & "," & vbBlue
        tabConnections.CreateDecorationObject "CURSOR", "T,B", "2,2", strColor
        For i = 1 To 6
            tabConnections.SetDecorationObject LineNo, i, "CURSOR"
        Next i
    End If
    tabConnections.Refresh
End Sub

Private Sub tabConnections_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    btnShowFeedings_Click
    tabConnections.SetFocus
End Sub

Private Sub tabConnections_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim i As Long
    Dim cnt As Long
    
    If LineNo = -1 Then
        For i = 0 To tabArrival.GetLineCount - 1
            tabConnections.ResetLineDecorations i
        Next
        If ColNo = tabConnections.CurrentSortColumn Then
            If tabConnections.SortOrderASC = True Then
                tabConnections.Sort CStr(ColNo), False, True
            Else
                tabConnections.Sort CStr(ColNo), True, True
            End If
        Else
            tabConnections.Sort CStr(ColNo), True, True
        End If
        tabConnections.Refresh
    End If
End Sub

Private Sub tabConnections_SendMouseMove(ByVal LineNo As Long, ByVal ColNo As Long, ByVal Flags As String)
    Dim strVal As String
    If ColNo = 2 Then
        strVal = tabConnections.GetFieldValue(LineNo, "CONT")
        If strVal <> "" Then
            tabConnections.ToolTipText = strVal
        Else
            tabConnections.ToolTipText = ""
        End If
    Else
        tabConnections.ToolTipText = ""
    End If
End Sub

Private Sub TabStrip1_Click()
    Dim i As Long
    
    If CheckDirty = True Then
        If MsgBox("You have made changes. Do you want to save ?", vbYesNo, "Note") = vbYes Then
            cmdSave_Click
        End If
    End If
    If TabStrip1.SelectedItem.Index = 1 Then
        lblTip(4).Caption = "Mail"
    Else
        lblTip(4).Caption = "Cargo"
    End If
    CheckAllConflicts
    ShowConnectionEntries tabArrival.GetCurrentSelected
    HasNewArrivalConflict tabArrival.GetCurrentSelected

End Sub

Public Sub SetLineDeleted()
    Dim llCurrLine As Long
    Dim colBack As Long
    Dim colText As Long
    Dim strVal As String
    Dim strOldVal As String
    Dim llLine As Long
    Dim strCount As String
    
    
    llCurrLine = tabConnections.GetCurrentSelected
    strVal = tabConnections.GetFieldValue(llCurrLine, "CONT")
    strOldVal = tabConnections.GetLineTag(llCurrLine)
    tabConnections.GetLineColor llCurrLine, colText, colBack
    If colBack = vbYellow Then
        tabConnections.DeleteLine llCurrLine
        strCount = tabArrival.GetLinesByIndexValue("URNO", strCurrentSelectedArrival, 0)
        llLine = tabArrival.GetNextResultLine
        If llLine > -1 Then
            tabArrival.GetLineColor llLine, colText, colBack
            If HasNewArrivalConflict(llLine) = True Then
                tabArrival.SetLineStatusValue llLine, 1
                SetArrivalListLineColor llLine
            Else
                tabArrival.SetLineStatusValue llLine, 0
                SetArrivalListLineColor llLine
            End If
            tabArrival.Refresh
        End If
    Else
        If colBack = colOrange Then
            If strVal = strOldVal Then
                tabConnections.SetLineColor llCurrLine, colText, vbWhite
            Else
                tabConnections.SetLineColor llCurrLine, colText, vbGreen
            End If
        Else
            tabConnections.SetLineColor llCurrLine, colText, colOrange
        End If
    End If
    tabConnections.Refresh
End Sub
'-------------------------------------------------------------------
' Checks all conflicts for the current type (Mail or Cargo)
'-------------------------------------------------------------------
Public Sub CheckAllConflicts()
    Dim cnt As Long
    Dim i As Long
    
    cnt = tabArrival.GetLineCount - 1
    For i = 0 To cnt
        If HasArrivalConflict(i) = True Then
            tabArrival.SetLineStatusValue i, 1
            SetArrivalListLineColor i
        Else
            tabArrival.SetLineStatusValue i, 0
            SetArrivalListLineColor i
        End If
    Next i
    tabArrival.Refresh
End Sub

'-------------------------------------------------------------------
' Checks for new inserted but not yet stored lines
' the conflicts for a singe flight. Returns true if conflict
' Checks also, if the flight is the current one and turns the
' corresponding connection line to red color
'-------------------------------------------------------------------
Public Function HasNewArrivalConflict(LineNo As Long) As Boolean
    Dim strRet As String
    Dim strRet2 As String
    Dim llLine As Long
    Dim llDepLine As Long
    Dim strAftUrno As String
    Dim llAftLine As Long
    Dim strDepUrno As String
    Dim strValues As String
    Dim strMC As String
    Dim tCol As Long
    Dim bCol As Long
    Dim blRet As Boolean
    Dim i As Long
    
    If TabStrip1.SelectedItem.Index = 1 Then
        strMC = "M"
    Else
        strMC = "C"
    End If
    
    For i = 0 To tabConnections.GetLineCount - 1
        tabConnections.SetLineStatusValue i, 0
    Next i
    blRet = False
    If LineNo >= 0 Then
        strAftUrno = tabArrival.GetFieldValue(LineNo, "URNO")
        '"URNO,CONT,FLNO,STD,ETD,DSSN,FLNU,TYPE,STYP,IDNT"
        tabConnections.SetInternalLineBuffer True
        strRet = tabConnections.GetLinesByIndexValue("ARR_URNO", strAftUrno, 0)
        llLine = tabConnections.GetNextResultLine
        While llLine > -1
            If strMC = tabConnections.GetFieldValue(llLine, "STYP") Then
                strDepUrno = tabConnections.GetFieldValue(llLine, "IDNT")
                If strDepUrno <> "" Then
                    strRet2 = frmData.tabData(0).GetLinesByIndexValue("URNO", strDepUrno, 0)
                    llDepLine = frmData.tabData(0).GetNextResultLine
                    If llLine > -1 Then
                        If CheckConnectionTimes(strAftUrno, strDepUrno, llDepLine, strMC) = True Then
                            tabConnections.SetLineStatusValue llLine, 1
                            blRet = True
                        End If
                    End If
                End If
            End If
            llLine = tabConnections.GetNextResultLine
        Wend
    End If
    If (blRet = True) And (strAftUrno = strCurrentSelectedArrival) Then
        For i = 0 To tabConnections.GetLineCount - 1
            If tabConnections.GetLineStatusValue(i) = 1 Then
                tabConnections.GetLineColor i, tCol, bCol
                tabConnections.SetLineColor i, vbRed, bCol
                tabConnections.SetLineStatusValue i, 0
            End If
        Next i
    End If
    tabConnections.Refresh
    HasNewArrivalConflict = blRet
End Function

'-------------------------------------------------------------------
' Checks the conflicts for a singe flight. Returns true if conflict
' Checks also, if the flight is the current one and turns the
' corresponding connection line to red color
'-------------------------------------------------------------------
Public Function HasArrivalConflict(LineNo As Long) As Boolean
    Dim strRet As String
    Dim strRet2 As String
    Dim llLine As Long
    Dim llDepLine As Long
    Dim strAftUrno As String
    Dim llAftLine As Long
    Dim strDepUrno As String
    Dim myIDX As Integer
    Dim strValues As String
    Dim strMC As String
    Dim blRet As Boolean
    
    blRet = False
    If TabStrip1.SelectedItem.Index = 1 Then
        myIDX = 6
        strMC = "M"
    Else
        myIDX = 7
        strMC = "C"
    End If
    
    If LineNo >= 0 Then
        strAftUrno = tabArrival.GetFieldValue(LineNo, "URNO")
        strRet = frmData.tabData(myIDX).GetLinesByIndexValue("FLNU", strAftUrno, 0)
        llLine = frmData.tabData(myIDX).GetNextResultLine
        While llLine > -1
            If strMC = frmData.tabData(myIDX).GetFieldValue(llLine, "STYP") Then
                strDepUrno = frmData.tabData(myIDX).GetFieldValue(llLine, "IDNT")
                If strDepUrno <> "" Then
                    strRet2 = frmData.tabData(0).GetLinesByIndexValue("URNO", strDepUrno, 0)
                    llDepLine = frmData.tabData(0).GetNextResultLine
                    If llDepLine > -1 Then
                        If CheckConnectionTimes(strAftUrno, strDepUrno, llDepLine, strMC) = True Then
                            blRet = True
                        End If
                    End If
                End If
            End If
            llLine = frmData.tabData(myIDX).GetNextResultLine
        Wend
    End If
    HasArrivalConflict = blRet
End Function


Public Function CheckConnectionTimes(strArrUrno As String, strDepUrno As String, _
                                     TabLineNoDep As Long, strMC As String) As Boolean
    Dim blRet As Boolean
    Dim TabLineNoArr As Long
    Dim linNoConn As String
    Dim strRet As String
    Dim strAdid As String
    Dim strAlc As String
    Dim strAlc_Arr As String
    Dim strAlc2 As String
    Dim strAlc3 As String
    Dim datTifa As Date
    Dim datTifd As Date
    Dim strFLNO_D As String
    Dim strFLNO_A As String
    Dim llLine As Long
    Dim llConnTime As Integer
    Dim myField As String
    Dim myField_Arr As String
    Dim llDiff As Long
    Dim strLine As String
    Dim llFoundLine As Long
    
    blRet = False
    strRet = frmData.tabData(0).GetLinesByIndexValue("URNO", strArrUrno, 0)
    TabLineNoArr = frmData.tabData(0).GetNextResultLine
    If TabLineNoArr <> -1 Then
        strAdid = frmData.tabData(0).GetFieldValue(TabLineNoArr, "ADID")
        If strAdid = "A" Then
            datTifa = CedaFullDateToVb(frmData.tabData(0).GetFieldValue(TabLineNoArr, "TIFA"))
            datTifd = CedaFullDateToVb(frmData.tabData(0).GetFieldValue(TabLineNoDep, "TIFD"))
            strFLNO_D = frmData.tabData(0).GetFieldValue(TabLineNoDep, "FLNO")
            strFLNO_A = frmData.tabData(0).GetFieldValue(TabLineNoArr, "FLNO")
            strAlc = Mid(strFLNO_A, 1, 3)
            strAlc = Trim(strAlc)
            strAlc_Arr = Mid(strFLNO_D, 1, 3)
            strAlc_Arr = Trim(strAlc_Arr)
            If Len(strAlc) = 2 Then
                myField = "ALC2"
            End If
            If Len(strAlc) = 3 Then
                myField = "ALC3"
            End If
            If Len(strAlc_Arr) = 2 Then
                myField_Arr = "ALC2"
            End If
            If Len(strAlc_Arr) = 3 Then
                myField_Arr = "ALC3"
            End If
            strRet = frmData.tabData(5).GetLinesByIndexValue(myField, strAlc, 0)
            llLine = frmData.tabData(5).GetNextResultLine
            If llLine <> -1 Then
                If strMC = "M" Then
                    If strAlc = strAlc_Arr Then
                        If Val(frmData.tabData(5).GetFieldValue(llLine, "MSAM")) = 0 Then
                            llConnTime = Val(frmData.tabData(5).GetFieldValue(llLine, "MCON"))
                        Else
                            llConnTime = Val(frmData.tabData(5).GetFieldValue(llLine, "MSAM"))
                        End If
                    Else
                        llConnTime = Val(frmData.tabData(5).GetFieldValue(llLine, "MCON"))
                    End If
                    'llConnTime = Val(frmData.tabData(5).GetFieldValue(llLine, "MCON"))
                Else
                    If strAlc = strAlc_Arr Then
                        If Val(frmData.tabData(5).GetFieldValue(llLine, "CSAM")) = 0 Then
                            llConnTime = Val(frmData.tabData(5).GetFieldValue(llLine, "CCON"))
                        Else
                            llConnTime = Val(frmData.tabData(5).GetFieldValue(llLine, "CSAM"))
                        End If
                    Else
                        llConnTime = Val(frmData.tabData(5).GetFieldValue(llLine, "CCON"))
                    End If
                    'llConnTime = Val(frmData.tabData(5).GetFieldValue(llLine, "CCON"))
                End If
            Else
                If strMC = "M" Then
                    llConnTime = Val(strMailDefault)
                Else
                    llConnTime = Val(strCargoDefault)
                End If
            End If
            llDiff = DateDiff("n", datTifa, datTifd)
            If llDiff < llConnTime Then
                blRet = True
            End If
        End If
    End If
    
    CheckConnectionTimes = blRet
End Function


Public Sub OnBc_DeleteFlight(strUrno As String)
    Dim llLine As Long
    Dim strCount As String
    
    strCount = tabArrival.GetLinesByIndexValue("URNO", strUrno, 0)
    llLine = tabArrival.GetNextResultLine
    If llLine > -1 Then
        tabArrival.DeleteLine llLine
    End If
End Sub

Public Sub OnBc_UpdateFlight(strUrno As String)
    Dim llLine As Long
    Dim llAftLine As Long
    Dim llLoaLine As Long
    Dim strCount As String
    Dim strMC As String
    Dim idxTab As Integer
    Dim strArrivalUrnos As String
    Dim i As Long
    Dim itemCnt As Long
    Dim strCurrUrno As String
    Dim tCol As Long
    Dim bCol As Long
    
    If TabStrip1.SelectedItem.Index = 1 Then
        strMC = "M"
        idxTab = 6
    Else
        strMC = "C"
        idxTab = 7
    End If
    
    
    strCount = tabArrival.GetLinesByIndexValue("URNO", strUrno, 0)
    llLine = tabArrival.GetNextResultLine
    If llLine > -1 Then
        'First update the AFTDATA in arrival tab
        strCount = frmData.tabData(0).GetLinesByIndexValue("URNO", strUrno, 0)
        llAftLine = frmData.tabData(0).GetNextResultLine
        If llAftLine > -1 Then
            If frmData.tabData(0).GetFieldValue(llAftLine, "ADID") = "A" Then
                tabArrival.SetFieldValues llLine, tabArrival.LogicalFieldList, frmData.tabData(0).GetFieldValues(llAftLine, "URNO,FLNO,STOA,ETAI")
                tabArrival.GetLineColor llLine, tCol, bCol
                If HasNewArrivalConflict(llLine) = True Then
                    tabArrival.SetLineStatusValue llLine, 1
                    SetArrivalListLineColor llLine
                Else
                    tabArrival.SetLineStatusValue llLine, 0
                    SetArrivalListLineColor llLine
                End If
                tabArrival.Refresh
            End If
        End If
    Else
        'It is a Departure so
        'Get all Departures in LOADTAB with IDNT=strUrno
        'Get backward for all IDNT=strUrno all corresponding FLNU
        'and check all arrival flights with FLNU for conflicts
        strCount = frmData.tabData(idxTab).GetLinesByIndexValue("IDNT", strUrno, 0)
        llLoaLine = frmData.tabData(idxTab).GetNextResultLine
        While llLoaLine > -1
            strArrivalUrnos = strArrivalUrnos + frmData.tabData(idxTab).GetFieldValue(llLoaLine, "FLNU") + ","
            llLoaLine = frmData.tabData(idxTab).GetNextResultLine
        Wend
        If strArrivalUrnos <> "" Then 'eliminate last comma
            strArrivalUrnos = Mid(strArrivalUrnos, 1, Len(strArrivalUrnos) - 1)
        End If
        itemCnt = ItemCount(strArrivalUrnos, ",") - 1
        For i = 0 To itemCnt
            strCurrUrno = GetRealItem(strArrivalUrnos, CInt(i), ",")
            strCount = tabArrival.GetLinesByIndexValue("URNO", strCurrUrno, 0)
            llLine = tabArrival.GetNextResultLine
            If llLine > -1 Then
                tabArrival.GetLineColor llLine, tCol, bCol
                If strCurrUrno = strCurrentSelectedArrival Then
                    If HasNewArrivalConflict(llLine) = True Then
                        tabArrival.SetLineStatusValue llLine, 1
                        SetArrivalListLineColor llLine
                    Else
                        tabArrival.SetLineStatusValue llLine, 0
                        SetArrivalListLineColor llLine
                    End If
                Else
                    If HasArrivalConflict(llLine) = True Then
                        tabArrival.SetLineStatusValue llLine, 1
                        SetArrivalListLineColor llLine
                        tabArrival.Refresh
                    Else
                        tabArrival.SetLineStatusValue llLine, 0
                        SetArrivalListLineColor llLine
                        tabArrival.Refresh
                    End If
                End If
            End If
        Next i
        strCount = tabConnections.GetLinesByIndexValue("URNO_D", strUrno, 0)
        llLine = tabConnections.GetNextResultLine
        While llLine > -1
            strCount = frmData.tabData(0).GetLinesByIndexValue("URNO", strUrno, 0)
            llAftLine = frmData.tabData(0).GetNextResultLine
            If llAftLine > -1 Then
                If frmData.tabData(0).GetFieldValues(llAftLine, "ADID") = "D" Then
                    tabConnections.SetFieldValues llLine, "STD,ETD", frmData.tabData(0).GetFieldValues(llAftLine, "STOD,ETDI")
                    tabConnections.Refresh
                End If
            End If
        Wend
    End If ' llLine > -1 then
End Sub

Public Sub OnBc_InsertFlight(strUrno As String)
    Dim llLine As Long
    Dim strCount As String
    
    strCount = frmData.tabData(0).GetLinesByIndexValue("URNO", strUrno, 0)
    llLine = frmData.tabData(0).GetNextResultLine
    If llLine > -1 Then
        If frmData.tabData(0).GetFieldValue(llLine, "ADID") = "A" Then
            tabArrival.InsertTextLine frmData.tabData(0).GetFieldValues(llLine, "URNO,FLNO,STOA,ETAI"), True
            tabArrival.Sort "2,3", True, True
            tabArrival.Refresh
        End If
    End If
End Sub
