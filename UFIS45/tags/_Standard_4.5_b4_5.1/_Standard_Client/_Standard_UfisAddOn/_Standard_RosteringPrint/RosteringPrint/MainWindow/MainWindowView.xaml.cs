﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using DevExpress.Xpf.Printing;
using DevExpress.Xpf.Grid;
using RosteringPrint.Helpers;
using System.Windows;

namespace RosteringPrint.MainWindow
{
    /// <summary>
    /// Interaction logic for MainWindowView.xaml
    /// </summary>
    public partial class MainWindowView : UserControl
    {
        public MainWindowView()
        {
            InitializeComponent();
        }

        private void barButtonPreview_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            PrintableControlLink link = new PrintableControlLink((TableView)HpDxGrid.CurrentGrid.View);
            link.Margins = new System.Drawing.Printing.Margins(50, 50, 50, 50);
            //link.Landscape = true;
            link.PageHeaderTemplate = (DataTemplate)Resources["detailPrintHeaderTemplate"];
            link.ShowPrintPreviewDialog(Window.GetWindow(this));       
        }
    }
}
