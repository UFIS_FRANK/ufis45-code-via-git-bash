﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Utilities;
using System.Windows.Media.Imaging;

namespace RosteringPrint.Helpers
{
    public class HpMenus
    {
        private static List<AppMenu> _lstAppMenus;

        private static List<AppMenu> CreateMenus()
        {
            const string SMALL_IMAGE_URI = "pack://application:,,,/Ufis.Resources.ImageLibrary;v4.5.0.0;8f51493079604dd3;component/application_16x16.png";
            const string LARGE_IMAGE_URI = "pack://application:,,,/Ufis.Resources.ImageLibrary;v4.5.0.0;8f51493079604dd3;component/application_32x32.png";

            BitmapImage smallImage = new BitmapImage(new Uri(SMALL_IMAGE_URI));
            smallImage.Freeze();
            BitmapImage largeImage = new BitmapImage(new Uri(LARGE_IMAGE_URI));
            largeImage.Freeze();

            IList<AppMenu> lstAppMenus = new List<AppMenu>();

            lstAppMenus.Add(
                new AppMenu() 
                { Name = "Flight identifier mappings", ImageSmall = smallImage, ImageLarge = largeImage } );

            return lstAppMenus.ToList();
        }

        public static List<AppMenu> MenuList
        {
            get 
            { 
                if (_lstAppMenus == null)
                    _lstAppMenus = CreateMenus();

                return _lstAppMenus; 
            }
        }
    }
}
