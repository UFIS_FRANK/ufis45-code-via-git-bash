﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Entities;
using Ufis.Security;

namespace RosteringPrint.Helpers
{
    class HpUser
    {
        public static EntUser ActiveUser { get; set; }
        public static UserPrivileges Privileges { get; set; }
    }
}
