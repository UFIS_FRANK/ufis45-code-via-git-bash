﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RosteringPrint.Helpers;
using DevExpress.Xpf.Printing;
using DevExpress.Xpf.Grid;
using Ufis.Data.Ceda;

namespace RosteringPrint.Report
{
    /// <summary>
    /// Interaction logic for PayrollCompensateView.xaml
    /// </summary>
    public partial class PayrollCompensateView : UserControl
    {
        public HpParameters param1;
        public HpParameters param2;
        public HpParameters param3;
        public PayrollCompensateViewModel objViewModel;

        public PayrollCompensateView()
        {
            InitializeComponent();
        } 

        private void barButtonPreview_ItemClick(object sender, DevExpress.Xpf.Bars.ItemClickEventArgs e)
        {
            param1 = new HpParameters();
            param2 = new HpParameters();
            param3 = new HpParameters();

            param1.MyParameter = String.Format("Departement(s):    {0}", HpUser.OrganisationCode);

            param2.MyParameter = String.Format("Function(s):    {0}", HpUser.FunctionCode);

            param3.MyParameter = String.Format("From:   {0}    To:    {1} ", HpUser.FromDate, HpUser.ToDate);

            ((HpParameters)Resources["paramet1"]).MyParameter = this.param1.MyParameter;

            ((HpParameters)Resources["paramet2"]).MyParameter = this.param2.MyParameter;

            ((HpParameters)Resources["paramet3"]).MyParameter = this.param3.MyParameter;

            if (gridPayroll.VisibleRowCount > 0)
            {
                PrintableControlLink link = new PrintableControlLink((TableView)gridPayroll.View);
                link.Margins = new System.Drawing.Printing.Margins(50, 50, 50, 50);
                link.Landscape = true;
                link.PageHeaderTemplate = (DataTemplate)Resources["detailPrintHeaderTemplate"];
                link.ShowPrintPreviewDialog(Window.GetWindow(this));
            }
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            objViewModel = new PayrollCompensateViewModel();

            //IFormatProvider culture = new System.Globalization.CultureInfo("fr-FR", true);

           

            //objViewModel.BuildReportEntity(HpUser.OrganisationCode,
            //                                            CedaUtils.DateTimeToCedaDate(DateTime.Parse(HpUser.FromDate, culture, System.Globalization.DateTimeStyles.AssumeLocal)),
            //                                            CedaUtils.DateTimeToCedaDate(Convert.ToDateTime(DateTime.Parse(HpUser.ToDate, culture, System.Globalization.DateTimeStyles.AssumeLocal))),
            //                                            HpUser.FunctionCode, HpUser.Level);

            string fromFormat = "dd.MM.yyyy";
            objViewModel.BuildReportEntity(HpUser.OrganisationCode, CedaUtils.DateTimeToCedaDate(DateTime.ParseExact(HpUser.FromDate, fromFormat, null)),
                      CedaUtils.DateTimeToCedaDate(DateTime.ParseExact(HpUser.ToDate, fromFormat, null)), HpUser.FunctionCode, HpUser.Level);

           gridPayroll.ItemsSource = objViewModel.ItemSource;
        }

    }
}
