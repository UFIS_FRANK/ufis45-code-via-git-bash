﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ufis.Data;
using Ufis.Entities;
using Ufis.MVVM.ViewModel;
using RosteringPrint.Helpers;
using RosteringPrint.DataAccess;
using RosteringPrint.Entities;
using System.Windows;

namespace RosteringPrint.Report
{
    public class PayrollCompensateViewModel : WorkspaceViewModel
    {
        #region Constructor & Methods

        public PayrollCompensateViewModel()
        {

        }

        public PayrollCompensateViewModel(string sOrgCode, string sFromDate, string sToDate, string sFunctionCode, string sLevel)
        {
            //Generate Report data entity
            this.BuildReportEntity(sOrgCode, sFromDate, sToDate, sFunctionCode, sLevel);
        }
        #endregion

        /// <summary>
        /// Generate Report Data
        /// </summary>
        /// <param name="sOrgCode">System.string containing sOrgCode</param>
        /// <param name="sFromDate">System.string containing sFromDate</param>
        /// <param name="sToDate">System.string containing sToDate</param>
        /// <param name="sFunctionCode">System.string containing sFunctionCode</param>
        /// <param name="sLevel">System.string containing sLevel</param>
        public void BuildReportEntity(string sOrgCode, string sFromDate, string sToDate, string sFunctionCode, string sLevel)
        {
            // sOrgCode = HpReportHelper.FormatString(sOrgCode);
            // sFunctionCode = HpReportHelper.FormatString(sFunctionCode);
            TimeSpan tNight = TimeSpan.Zero;
            TimeSpan tHoliday = TimeSpan.Zero;

            //Load Holiday Between From and Todate
            EntityCollectionBase<EntDbHoliday> objHoliday = DlPayrollCompensate.LoadHoliday(sFromDate, sToDate);
            
            //Get Staff No and Name
            EntityCollectionBase<EntDbStaffData> objStaffData = DlPayrollCompensate.LoadStaffInformation(sOrgCode);

            //Get daily Roster
            EntityCollectionBase<EntDbDailyRoster> objDailyRoster = DlPayrollCompensate.LoadDailyRoster(sFromDate, sToDate, sOrgCode, sFunctionCode, sLevel);
            
            //Load all the basic shift data
            EntityCollectionBase<EntDbShiftBasicData> objBasicShift = DlPayrollCompensate.LoadAllShiftBasicData();

            IList<entlcPayrollCompensate> objPayRollCompensate = new List<entlcPayrollCompensate>();

            //Update the staff Info into IList entity
            foreach (EntDbStaffData tmpStaffData in objStaffData)
            {
                entlcPayrollCompensate objTmpPayRollCompensate = new entlcPayrollCompensate();
                {
                    tNight = TimeSpan.Zero;
                    tHoliday = TimeSpan.Zero;
                    //Add Urno
                    objTmpPayRollCompensate.StaffId = tmpStaffData.Urno;
                    //Add Personal Number
                    objTmpPayRollCompensate.PersonalNo = tmpStaffData.PersonalNumber;
                    //Add Personal Name
                    objTmpPayRollCompensate.StaffName = String.Format("{1}, {0}", tmpStaffData.FirstName, tmpStaffData.LastName);
                    //Get Shift Code from Daily Roster by Staff ID
                    IEnumerable<EntDbDailyRoster> qResult = objDailyRoster.Where(s => s.UrnoSTF == tmpStaffData.Urno);

                    //Get Basic Shif Info by Daily Roster's shift Code
                    foreach (EntDbDailyRoster tmpobjDailyRoster in qResult)
                    {
                        //IEnumerable<EntDbShiftBasicData> qShiftResult = objBasicShift.Where(b => b.BasicShiftCode == Convert.ToString(tmpobjDailyRoster.UrnoBSD).Trim());

                        IEnumerable<EntDbShiftBasicData> qShiftResult = objBasicShift.Where(b => b.BasicShiftCode == tmpobjDailyRoster.ShiftCode);

                        //Calculate Night or Holiday
                        foreach (EntDbShiftBasicData tmpShiftBasicData in qShiftResult)
                        {
                            //entlcWorkingHours objWorkingHours = HpReportHelper.GetWorkingHours(sFromDate, sToDate, tmpobjDailyRoster.DayShift, tmpShiftBasicData.EarliertShiftBegin, tmpShiftBasicData.ShiftEnd, objHoliday);

                            entlcWorkingHours objWorkingHours = HpReportHelper.GetWorkingHours(tmpShiftBasicData, tmpobjDailyRoster.DayShift, objHoliday);
                            tNight = tNight.Add(objWorkingHours.NightWorkingHours);
                            tHoliday = tHoliday.Add(objWorkingHours.HolidayWorkingHours);
                        }
                    }

                    int tHolidayDay = 0;
                    int tNightDay = 0;

                    if (tHoliday.Days > 0)
                    {
                        tHolidayDay = tHoliday.Days * 24;
                    }
                    if (tNight.Days > 0)
                    {
                        tNightDay = tNight.Days * 24;
                    }

                    //Add Holiday Hours
                    objTmpPayRollCompensate.HolidayWorkingHours = tHolidayDay + tHoliday.Hours + "." + tHoliday.Minutes.ToString().PadLeft(2, '0');
                    //Add Night Shift working Hours
                    objTmpPayRollCompensate.NightWorkingHours = tNightDay+ tNight.Hours + "." + tNight.Minutes.ToString().PadLeft(2, '0');
                }
                //Bind entity collection.
                objPayRollCompensate.Add(objTmpPayRollCompensate);
            }

            ItemSource = objPayRollCompensate;

        }

        IList<entlcPayrollCompensate> _itemSource;
        public IList<entlcPayrollCompensate> ItemSource
        {
            get
            {
                return _itemSource;
            }
            protected set
            {
                if (_itemSource != value)
                {
                    _itemSource = value;
                    OnPropertyChanged("ItemSource");
                }
            }
        }
    }
}
