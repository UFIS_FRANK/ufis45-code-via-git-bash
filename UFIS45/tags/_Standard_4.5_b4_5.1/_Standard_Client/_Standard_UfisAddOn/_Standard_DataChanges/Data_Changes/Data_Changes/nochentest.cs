using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace Data_Changes
{
	/// <summary>
	/// Summary description for nochentest.
	/// </summary>
	public class nochentest : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Panel panel6;
		private System.Windows.Forms.Panel panel7;
		private System.Windows.Forms.Panel panel8;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button6;
		private System.Windows.Forms.Button button7;
		private System.Windows.Forms.DateTimePicker dateTimePicker1;
		private System.Windows.Forms.DateTimePicker dateTimePicker2;
		private AxTABLib.AxTAB tabNAT;
		private System.Windows.Forms.PictureBox pictureBox1;
		private AxAATLOGINLib.AxAatLogin axAatLogin1;
		private System.Windows.Forms.Panel panel5;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public nochentest()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(nochentest));
			this.panel1 = new System.Windows.Forms.Panel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.panel3 = new System.Windows.Forms.Panel();
			this.panel4 = new System.Windows.Forms.Panel();
			this.label1 = new System.Windows.Forms.Label();
			this.panel6 = new System.Windows.Forms.Panel();
			this.panel7 = new System.Windows.Forms.Panel();
			this.panel8 = new System.Windows.Forms.Panel();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.button6 = new System.Windows.Forms.Button();
			this.button7 = new System.Windows.Forms.Button();
			this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
			this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
			this.tabNAT = new AxTABLib.AxTAB();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.axAatLogin1 = new AxAATLOGINLib.AxAatLogin();
			this.panel5 = new System.Windows.Forms.Panel();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.panel3.SuspendLayout();
			this.panel4.SuspendLayout();
			this.panel6.SuspendLayout();
			this.panel7.SuspendLayout();
			this.panel8.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tabNAT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.axAatLogin1)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(0)), ((System.Byte)(192)));
			this.panel1.Controls.Add(this.pictureBox1);
			this.panel1.Controls.Add(this.button3);
			this.panel1.Controls.Add(this.button2);
			this.panel1.Controls.Add(this.button1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(752, 48);
			this.panel1.TabIndex = 0;
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.panel5);
			this.panel2.Controls.Add(this.panel8);
			this.panel2.Controls.Add(this.panel7);
			this.panel2.Controls.Add(this.panel6);
			this.panel2.Controls.Add(this.panel4);
			this.panel2.Controls.Add(this.panel3);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(0, 48);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(752, 414);
			this.panel2.TabIndex = 1;
			// 
			// panel3
			// 
			this.panel3.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(255)), ((System.Byte)(192)));
			this.panel3.Controls.Add(this.dateTimePicker2);
			this.panel3.Controls.Add(this.dateTimePicker1);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel3.Location = new System.Drawing.Point(0, 0);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(752, 48);
			this.panel3.TabIndex = 0;
			// 
			// panel4
			// 
			this.panel4.Controls.Add(this.label1);
			this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel4.Location = new System.Drawing.Point(0, 48);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(752, 24);
			this.panel4.TabIndex = 1;
			// 
			// label1
			// 
			this.label1.BackColor = System.Drawing.Color.Brown;
			this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.label1.Location = new System.Drawing.Point(0, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(752, 24);
			this.label1.TabIndex = 0;
			this.label1.Text = "label1";
			// 
			// panel6
			// 
			this.panel6.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(192)), ((System.Byte)(192)));
			this.panel6.Controls.Add(this.button6);
			this.panel6.Dock = System.Windows.Forms.DockStyle.Left;
			this.panel6.Location = new System.Drawing.Point(0, 72);
			this.panel6.Name = "panel6";
			this.panel6.Size = new System.Drawing.Size(32, 342);
			this.panel6.TabIndex = 3;
			// 
			// panel7
			// 
			this.panel7.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(0)), ((System.Byte)(0)));
			this.panel7.Controls.Add(this.button7);
			this.panel7.Dock = System.Windows.Forms.DockStyle.Right;
			this.panel7.Location = new System.Drawing.Point(720, 72);
			this.panel7.Name = "panel7";
			this.panel7.Size = new System.Drawing.Size(32, 342);
			this.panel7.TabIndex = 4;
			// 
			// panel8
			// 
			this.panel8.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.panel8.Controls.Add(this.axAatLogin1);
			this.panel8.Controls.Add(this.tabNAT);
			this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel8.Location = new System.Drawing.Point(32, 72);
			this.panel8.Name = "panel8";
			this.panel8.Size = new System.Drawing.Size(688, 342);
			this.panel8.TabIndex = 5;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(0, 8);
			this.button1.Name = "button1";
			this.button1.TabIndex = 0;
			this.button1.Text = "button1";
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(608, 8);
			this.button2.Name = "button2";
			this.button2.TabIndex = 1;
			this.button2.Text = "button2";
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(80, 8);
			this.button3.Name = "button3";
			this.button3.TabIndex = 2;
			this.button3.Text = "button3";
			// 
			// button6
			// 
			this.button6.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(192)), ((System.Byte)(192)));
			this.button6.Location = new System.Drawing.Point(-24, 96);
			this.button6.Name = "button6";
			this.button6.TabIndex = 3;
			this.button6.Text = "button6";
			// 
			// button7
			// 
			this.button7.Location = new System.Drawing.Point(-32, 80);
			this.button7.Name = "button7";
			this.button7.TabIndex = 4;
			this.button7.Text = "button7";
			// 
			// dateTimePicker1
			// 
			this.dateTimePicker1.Location = new System.Drawing.Point(24, 16);
			this.dateTimePicker1.Name = "dateTimePicker1";
			this.dateTimePicker1.TabIndex = 0;
			// 
			// dateTimePicker2
			// 
			this.dateTimePicker2.Location = new System.Drawing.Point(544, 16);
			this.dateTimePicker2.Name = "dateTimePicker2";
			this.dateTimePicker2.TabIndex = 1;
			// 
			// tabNAT
			// 
			this.tabNAT.ContainingControl = this;
			this.tabNAT.Location = new System.Drawing.Point(24, 16);
			this.tabNAT.Name = "tabNAT";
			this.tabNAT.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabNAT.OcxState")));
			this.tabNAT.Size = new System.Drawing.Size(664, 256);
			this.tabNAT.TabIndex = 5;
			// 
			// pictureBox1
			// 
			this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(192)), ((System.Byte)(192)));
			this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Right;
			this.pictureBox1.Location = new System.Drawing.Point(652, 0);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(100, 48);
			this.pictureBox1.TabIndex = 3;
			this.pictureBox1.TabStop = false;
			// 
			// axAatLogin1
			// 
			this.axAatLogin1.ContainingControl = this;
			this.axAatLogin1.Enabled = true;
			this.axAatLogin1.Location = new System.Drawing.Point(432, 136);
			this.axAatLogin1.Name = "axAatLogin1";
			this.axAatLogin1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axAatLogin1.OcxState")));
			this.axAatLogin1.Size = new System.Drawing.Size(100, 50);
			this.axAatLogin1.TabIndex = 6;
			// 
			// panel5
			// 
			this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel5.Location = new System.Drawing.Point(32, 350);
			this.panel5.Name = "panel5";
			this.panel5.Size = new System.Drawing.Size(688, 64);
			this.panel5.TabIndex = 6;
			// 
			// nochentest
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(752, 462);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Name = "nochentest";
			this.Text = "nochentest";
			this.panel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.panel3.ResumeLayout(false);
			this.panel4.ResumeLayout(false);
			this.panel6.ResumeLayout(false);
			this.panel7.ResumeLayout(false);
			this.panel8.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.tabNAT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.axAatLogin1)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion
	}
}
