using System;
using System.IO;
using System.Drawing;
using System.Text;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
//using System.Reflection;
using Microsoft.Win32;
using System.Data;
using Ufis.Utils;
using Ufis.Data;

using Data_Changes.Ctrl;
using System.Collections.Generic;

namespace Data_Changes
{
    /// <summary>
    /// Summary description for Form1.
    /// </summary>
    public class frmMain : System.Windows.Forms.Form
    {
        #region ------ MyRegion

        private frmData DataForm = null;
        public string[] args;
        private IDatabase myDB = null;
        private ITable logTAB = null;
        private ITable logCKITAB = null;
        private bool myIsTimeInUtc = true;
        private bool myIsInit = false;
        private bool myboolDoingLogtab = false;
        private bool m_AddonFields = false;
        private string myStrWhereTimeframe = "";
        private string myStrWhereALOCIn = "";
        private string myStrCcaWhereALOCIn = "";
        private string myStrWhereFLNUIn = "";
        private string myStrREFUList = "";
        private string myStrCCAUrnoFlnuMap = "";
        private string myStrCcaWhereFLNUIn = "";
        private string myStrSortOrder = " ORDER BY TIME DESC, SEQN DESC";
        private string mySelection = "";
        private string mySelectionLOGTABKeyf = "";
        private string m_ViewMode = "LOCATIONS";
        private string m_TimeMode = "L";
        private string m_TableName = "";
        private string m_FieldName = "";
        private string m_FieldType = "";
        private string m_Listener = "";
        private string m_Urno = "0";
        private string m_UserName1 = "";
        private string m_UserName2 = "";
        private string m_UserName3 = "";
        private string myStrWhereUser = string.Empty;
        //		private string argsL = "";
        private string m_PST_Fields = "'PSTA','PSTD' ";
        private string m_EXT_Fields = "'EXT1','EXT2' ";
        private string m_GAT_Fields = "'GTD1','GTD2','GTA1','GTA2' ";
        private string m_BLT_Fields = "'BLT1','BLT2' ";
        private string m_WRO_Fields = "'WRO1' ";
        private string m_CCA_Fields = "'CKIC' ";
        private string m_RWY_Fields = "'RWYA','RWYD' ";
        private string m_CCA_ExtFields = "'CKIC','CKBS','CKES','CKBA','CKEA' ";
        private string myNeedCCA = "";
        private string m_CcaLogtab = "";
        private string m_AftLogtab = "";
        private string m_ActiveField = "";
        private string m_FlightCount = "";
        private string myIniFile = "C:\\Ufis\\System\\DATACHANGE.ini";
        private string myAllFieldsAFT = "";
        private string myAllFieldsCCA = "";
        private string myAllFieldsCKI = "";
        private bool m_DeleteFlightsChecked = false;
        private string m_SelectedTabPage = "";
        private string m_BasicTableCode = "";
        private ITable m_SysTab = null;
        private string m_HxxTabStr = ""; //Hxx table linked to Basic Data Table.
        private string m_FilterConStr = "";
        private string m_strIdentifierCodes = "";

        public static frmMain myThis;
        private bool bmMouseInsideTabControl = false;

        private string m_strCKIIdentifiers = "";
        public string m_selCKI = "";
        #endregion ----- MyRegion

        private System.Windows.Forms.ImageList imageList1;
        private Ufis.Utils.BCBlinker bcBlinker1;
        private System.Windows.Forms.Panel panelTop;
        private System.Windows.Forms.ImageList imageButtons;
        private System.Windows.Forms.Panel panelLabel;
        private System.Windows.Forms.Panel panelTab;
        private System.Windows.Forms.PictureBox pictureBoxTab;
        private AxTABLib.AxTAB tabLOG;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.CheckBox cbUTC;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Button btnNow;
        private System.Windows.Forms.DateTimePicker dateTimePickerTo;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.DateTimePicker dateTimePickerFrom;
        private System.Windows.Forms.Label lblFrom;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.CheckBox cbView;
        private System.Windows.Forms.CheckBox cbTimer;
        public System.Windows.Forms.Label lblLocations;
        public System.Timers.Timer timer2;
        private AxAATLOGINLib.AxAatLogin LoginControl;
        private AxUFISCOMLib.AxUfisCom axUfisCom1;
        private AxUFISCOMLib.AxUfisCom u;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label lblTimer;
        private System.Windows.Forms.Label lblInMinutes;
        private System.Windows.Forms.TextBox txtURNOValue;
        private System.Windows.Forms.CheckBox cbFlnu;
        private System.ComponentModel.IContainer components;


        public frmMain()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            myThis = this;
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                UT.DisposeMemDB();
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        public static frmMain GetThis()
        {
            return myThis;
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.bcBlinker1 = new Ufis.Utils.BCBlinker();
            this.panelTop = new System.Windows.Forms.Panel();
            this.cbFlnu = new System.Windows.Forms.CheckBox();
            this.txtURNOValue = new System.Windows.Forms.TextBox();
            this.lblInMinutes = new System.Windows.Forms.Label();
            this.lblTimer = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.u = new AxUFISCOMLib.AxUfisCom();
            this.axUfisCom1 = new AxUFISCOMLib.AxUfisCom();
            this.LoginControl = new AxAATLOGINLib.AxAatLogin();
            this.cbTimer = new System.Windows.Forms.CheckBox();
            this.btnPrint = new System.Windows.Forms.Button();
            this.imageButtons = new System.Windows.Forms.ImageList(this.components);
            this.cbView = new System.Windows.Forms.CheckBox();
            this.btnNow = new System.Windows.Forms.Button();
            this.dateTimePickerTo = new System.Windows.Forms.DateTimePicker();
            this.lblTo = new System.Windows.Forms.Label();
            this.dateTimePickerFrom = new System.Windows.Forms.DateTimePicker();
            this.lblFrom = new System.Windows.Forms.Label();
            this.cbUTC = new System.Windows.Forms.CheckBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnApply = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panelLabel = new System.Windows.Forms.Panel();
            this.lblLocations = new System.Windows.Forms.Label();
            this.panelTab = new System.Windows.Forms.Panel();
            this.tabLOG = new AxTABLib.AxTAB();
            this.pictureBoxTab = new System.Windows.Forms.PictureBox();
            this.timer2 = new System.Timers.Timer();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.u)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axUfisCom1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LoginControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panelLabel.SuspendLayout();
            this.panelTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabLOG)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxTab)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timer2)).BeginInit();
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.White;
            this.imageList1.Images.SetKeyName(0, "");
            this.imageList1.Images.SetKeyName(1, "");
            this.imageList1.Images.SetKeyName(2, "");
            this.imageList1.Images.SetKeyName(3, "");
            this.imageList1.Images.SetKeyName(4, "");
            this.imageList1.Images.SetKeyName(5, "");
            this.imageList1.Images.SetKeyName(6, "");
            this.imageList1.Images.SetKeyName(7, "");
            this.imageList1.Images.SetKeyName(8, "");
            this.imageList1.Images.SetKeyName(9, "");
            this.imageList1.Images.SetKeyName(10, "");
            this.imageList1.Images.SetKeyName(11, "");
            this.imageList1.Images.SetKeyName(12, "");
            this.imageList1.Images.SetKeyName(13, "");
            this.imageList1.Images.SetKeyName(14, "");
            this.imageList1.Images.SetKeyName(15, "");
            this.imageList1.Images.SetKeyName(16, "");
            // 
            // bcBlinker1
            // 
            this.bcBlinker1.BackColor = System.Drawing.SystemColors.Control;
            this.bcBlinker1.Location = new System.Drawing.Point(608, 0);
            this.bcBlinker1.Name = "bcBlinker1";
            this.bcBlinker1.Size = new System.Drawing.Size(44, 24);
            this.bcBlinker1.TabIndex = 4;
            // 
            // panelTop
            // 
            this.panelTop.Controls.Add(this.cbFlnu);
            this.panelTop.Controls.Add(this.txtURNOValue);
            this.panelTop.Controls.Add(this.lblInMinutes);
            this.panelTop.Controls.Add(this.lblTimer);
            this.panelTop.Controls.Add(this.numericUpDown1);
            this.panelTop.Controls.Add(this.u);
            this.panelTop.Controls.Add(this.axUfisCom1);
            this.panelTop.Controls.Add(this.LoginControl);
            this.panelTop.Controls.Add(this.cbTimer);
            this.panelTop.Controls.Add(this.btnPrint);
            this.panelTop.Controls.Add(this.cbView);
            this.panelTop.Controls.Add(this.btnNow);
            this.panelTop.Controls.Add(this.dateTimePickerTo);
            this.panelTop.Controls.Add(this.lblTo);
            this.panelTop.Controls.Add(this.dateTimePickerFrom);
            this.panelTop.Controls.Add(this.lblFrom);
            this.panelTop.Controls.Add(this.cbUTC);
            this.panelTop.Controls.Add(this.btnClose);
            this.panelTop.Controls.Add(this.btnApply);
            this.panelTop.Controls.Add(this.pictureBox1);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(1112, 40);
            this.panelTop.TabIndex = 5;
            // 
            // cbFlnu
            // 
            this.cbFlnu.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbFlnu.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFlnu.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cbFlnu.ImageIndex = 0;
            this.cbFlnu.ImageList = this.imageList1;
            this.cbFlnu.Location = new System.Drawing.Point(664, 0);
            this.cbFlnu.Name = "cbFlnu";
            this.cbFlnu.Size = new System.Drawing.Size(80, 20);
            this.cbFlnu.TabIndex = 110;
            this.cbFlnu.Text = "&Urno";
            this.cbFlnu.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolTip1.SetToolTip(this.cbFlnu, "Sets \"Apply\" to this flight urno");
            this.cbFlnu.Click += new System.EventHandler(this.cbFlnu_Click);
            this.cbFlnu.CheckedChanged += new System.EventHandler(this.cbFlnu_CheckedChanged);
            // 
            // txtURNOValue
            // 
            this.txtURNOValue.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtURNOValue.Location = new System.Drawing.Point(664, 20);
            this.txtURNOValue.MaxLength = 10;
            this.txtURNOValue.Name = "txtURNOValue";
            this.txtURNOValue.Size = new System.Drawing.Size(80, 20);
            this.txtURNOValue.TabIndex = 109;
            this.toolTip1.SetToolTip(this.txtURNOValue, "enter the flights urno");
            this.txtURNOValue.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtURNOValue_KeyPress);
            // 
            // lblInMinutes
            // 
            this.lblInMinutes.BackColor = System.Drawing.Color.Transparent;
            this.lblInMinutes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInMinutes.Location = new System.Drawing.Point(596, 12);
            this.lblInMinutes.Name = "lblInMinutes";
            this.lblInMinutes.Size = new System.Drawing.Size(36, 16);
            this.lblInMinutes.TabIndex = 107;
            this.lblInMinutes.Text = "[min.]";
            // 
            // lblTimer
            // 
            this.lblTimer.BackColor = System.Drawing.Color.Transparent;
            this.lblTimer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTimer.Location = new System.Drawing.Point(560, 12);
            this.lblTimer.Name = "lblTimer";
            this.lblTimer.Size = new System.Drawing.Size(36, 16);
            this.lblTimer.TabIndex = 106;
            this.lblTimer.Text = "Timer: ";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(520, 8);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown1.TabIndex = 105;
            this.toolTip1.SetToolTip(this.numericUpDown1, "Set the automatic reload timer in minutes");
            this.numericUpDown1.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // u
            // 
            this.u.Enabled = true;
            this.u.Location = new System.Drawing.Point(908, 0);
            this.u.Name = "u";
            this.u.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("u.OcxState")));
            this.u.Size = new System.Drawing.Size(40, 24);
            this.u.TabIndex = 104;
            // 
            // axUfisCom1
            // 
            this.axUfisCom1.Enabled = true;
            this.axUfisCom1.Location = new System.Drawing.Point(796, 0);
            this.axUfisCom1.Name = "axUfisCom1";
            this.axUfisCom1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axUfisCom1.OcxState")));
            this.axUfisCom1.Size = new System.Drawing.Size(48, 24);
            this.axUfisCom1.TabIndex = 103;
            // 
            // LoginControl
            // 
            this.LoginControl.Enabled = true;
            this.LoginControl.Location = new System.Drawing.Point(852, 0);
            this.LoginControl.Name = "LoginControl";
            this.LoginControl.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("LoginControl.OcxState")));
            this.LoginControl.Size = new System.Drawing.Size(48, 24);
            this.LoginControl.TabIndex = 102;
            this.LoginControl.Visible = false;
            // 
            // cbTimer
            // 
            this.cbTimer.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbTimer.BackColor = System.Drawing.Color.Transparent;
            this.cbTimer.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbTimer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTimer.ImageIndex = 16;
            this.cbTimer.ImageList = this.imageList1;
            this.cbTimer.Location = new System.Drawing.Point(500, 8);
            this.cbTimer.Name = "cbTimer";
            this.cbTimer.Size = new System.Drawing.Size(20, 20);
            this.cbTimer.TabIndex = 101;
            this.cbTimer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolTip1.SetToolTip(this.cbTimer, "Activate/Dectivate timer for automatic relad each <n> minutes");
            this.cbTimer.UseVisualStyleBackColor = false;
            this.cbTimer.CheckedChanged += new System.EventHandler(this.cbTimer_CheckedChanged);
            // 
            // btnPrint
            // 
            this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrint.BackColor = System.Drawing.Color.Transparent;
            this.btnPrint.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPrint.ImageIndex = 2;
            this.btnPrint.ImageList = this.imageButtons;
            this.btnPrint.Location = new System.Drawing.Point(952, 0);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(80, 40);
            this.btnPrint.TabIndex = 100;
            this.btnPrint.Text = "&Print";
            this.toolTip1.SetToolTip(this.btnPrint, "Print the current loaded data");
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // imageButtons
            // 
            this.imageButtons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageButtons.ImageStream")));
            this.imageButtons.TransparentColor = System.Drawing.Color.White;
            this.imageButtons.Images.SetKeyName(0, "");
            this.imageButtons.Images.SetKeyName(1, "");
            this.imageButtons.Images.SetKeyName(2, "");
            this.imageButtons.Images.SetKeyName(3, "");
            this.imageButtons.Images.SetKeyName(4, "");
            this.imageButtons.Images.SetKeyName(5, "");
            // 
            // cbView
            // 
            this.cbView.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbView.BackColor = System.Drawing.Color.Transparent;
            this.cbView.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbView.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cbView.ImageIndex = 10;
            this.cbView.ImageList = this.imageList1;
            this.cbView.Location = new System.Drawing.Point(80, 0);
            this.cbView.Name = "cbView";
            this.cbView.Size = new System.Drawing.Size(80, 40);
            this.cbView.TabIndex = 99;
            this.cbView.Text = "&View";
            this.cbView.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolTip1.SetToolTip(this.cbView, "Open the view definition dialog");
            this.cbView.UseVisualStyleBackColor = false;
            this.cbView.CheckedChanged += new System.EventHandler(this.cbView_CheckedChanged);
            // 
            // btnNow
            // 
            this.btnNow.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNow.ImageIndex = 4;
            this.btnNow.ImageList = this.imageList1;
            this.btnNow.Location = new System.Drawing.Point(468, 20);
            this.btnNow.Name = "btnNow";
            this.btnNow.Size = new System.Drawing.Size(20, 20);
            this.btnNow.TabIndex = 98;
            this.toolTip1.SetToolTip(this.btnNow, "Change the time to now");
            this.btnNow.Click += new System.EventHandler(this.btnNow_Click);
            // 
            // dateTimePickerTo
            // 
            this.dateTimePickerTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerTo.Location = new System.Drawing.Point(340, 19);
            this.dateTimePickerTo.Name = "dateTimePickerTo";
            this.dateTimePickerTo.Size = new System.Drawing.Size(128, 20);
            this.dateTimePickerTo.TabIndex = 97;
            // 
            // lblTo
            // 
            this.lblTo.BackColor = System.Drawing.Color.Transparent;
            this.lblTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTo.Location = new System.Drawing.Point(248, 20);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(88, 16);
            this.lblTo.TabIndex = 96;
            this.lblTo.Text = "Changes To:";
            // 
            // dateTimePickerFrom
            // 
            this.dateTimePickerFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerFrom.Location = new System.Drawing.Point(340, -1);
            this.dateTimePickerFrom.Name = "dateTimePickerFrom";
            this.dateTimePickerFrom.Size = new System.Drawing.Size(128, 20);
            this.dateTimePickerFrom.TabIndex = 95;
            // 
            // lblFrom
            // 
            this.lblFrom.BackColor = System.Drawing.Color.Transparent;
            this.lblFrom.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFrom.Location = new System.Drawing.Point(248, 2);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(88, 16);
            this.lblFrom.TabIndex = 94;
            this.lblFrom.Text = "Changes From:";
            // 
            // cbUTC
            // 
            this.cbUTC.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbUTC.BackColor = System.Drawing.Color.Transparent;
            this.cbUTC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbUTC.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cbUTC.ImageIndex = 16;
            this.cbUTC.ImageList = this.imageList1;
            this.cbUTC.Location = new System.Drawing.Point(0, 0);
            this.cbUTC.Name = "cbUTC";
            this.cbUTC.Size = new System.Drawing.Size(80, 40);
            this.cbUTC.TabIndex = 76;
            this.cbUTC.Text = "&UTC";
            this.cbUTC.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.toolTip1.SetToolTip(this.cbUTC, "Switch between UTC and local Times");
            this.cbUTC.UseVisualStyleBackColor = false;
            this.cbUTC.CheckedChanged += new System.EventHandler(this.cbUTC_CheckedChanged);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.BackColor = System.Drawing.Color.Transparent;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.ImageIndex = 0;
            this.btnClose.ImageList = this.imageButtons;
            this.btnClose.Location = new System.Drawing.Point(1032, 0);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(80, 40);
            this.btnClose.TabIndex = 75;
            this.btnClose.Text = "&Close";
            this.toolTip1.SetToolTip(this.btnClose, "Close the application");
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnApply
            // 
            this.btnApply.BackColor = System.Drawing.Color.Transparent;
            this.btnApply.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnApply.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnApply.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnApply.ImageIndex = 1;
            this.btnApply.ImageList = this.imageButtons;
            this.btnApply.Location = new System.Drawing.Point(160, 0);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(80, 40);
            this.btnApply.TabIndex = 74;
            this.btnApply.Text = "&Apply";
            this.toolTip1.SetToolTip(this.btnApply, "Reload the current active view filter");
            this.btnApply.UseVisualStyleBackColor = false;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1112, 40);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            // 
            // panelLabel
            // 
            this.panelLabel.BackColor = System.Drawing.Color.Transparent;
            this.panelLabel.Controls.Add(this.lblLocations);
            this.panelLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLabel.Location = new System.Drawing.Point(0, 40);
            this.panelLabel.Name = "panelLabel";
            this.panelLabel.Size = new System.Drawing.Size(1112, 24);
            this.panelLabel.TabIndex = 6;
            // 
            // lblLocations
            // 
            this.lblLocations.BackColor = System.Drawing.Color.Black;
            this.lblLocations.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLocations.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLocations.ForeColor = System.Drawing.Color.Lime;
            this.lblLocations.Location = new System.Drawing.Point(0, 0);
            this.lblLocations.Name = "lblLocations";
            this.lblLocations.Size = new System.Drawing.Size(1112, 24);
            this.lblLocations.TabIndex = 0;
            this.lblLocations.Text = "Active Locations : ";
            this.lblLocations.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblLocations.Click += new System.EventHandler(this.label1_Click);
            // 
            // panelTab
            // 
            this.panelTab.BackColor = System.Drawing.Color.Transparent;
            this.panelTab.Controls.Add(this.tabLOG);
            this.panelTab.Controls.Add(this.pictureBoxTab);
            this.panelTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelTab.Location = new System.Drawing.Point(0, 64);
            this.panelTab.Name = "panelTab";
            this.panelTab.Size = new System.Drawing.Size(1112, 485);
            this.panelTab.TabIndex = 7;
            // 
            // tabLOG
            // 
            this.tabLOG.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabLOG.Location = new System.Drawing.Point(0, 0);
            this.tabLOG.Name = "tabLOG";
            this.tabLOG.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("tabLOG.OcxState")));
            this.tabLOG.Size = new System.Drawing.Size(1112, 485);
            this.tabLOG.TabIndex = 1;
            this.tabLOG.RowSelectionChanged += new AxTABLib._DTABEvents_RowSelectionChangedEventHandler(this.tabLOG_RowSelectionChanged);            
            this.tabLOG.SendLButtonDblClick += new AxTABLib._DTABEvents_SendLButtonDblClickEventHandler(this.tabLOG_SendLButtonDblClick);            
            this.tabLOG.HitKeyOnLine += new AxTABLib._DTABEvents_HitKeyOnLineEventHandler(this.tabLOG_HitKeyOnLine);
            //Comment it to be able to use at Win xx 64 bits.
            //this.tabLOG.MouseLeave += new System.EventHandler(this.frmMain_MouseLeave);
            //this.tabLOG.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.frmMain_MouseWheel);
            //this.tabLOG.MouseEnter += new System.EventHandler(this.frmMain_MouseEnter);
            // 
            // pictureBoxTab
            // 
            this.pictureBoxTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxTab.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxTab.Name = "pictureBoxTab";
            this.pictureBoxTab.Size = new System.Drawing.Size(1112, 485);
            this.pictureBoxTab.TabIndex = 0;
            this.pictureBoxTab.TabStop = false;
            this.pictureBoxTab.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBoxTab_Paint);
            // 
            // timer2
            // 
            this.timer2.Interval = 120000;
            this.timer2.SynchronizingObject = this;
            this.timer2.Elapsed += new System.Timers.ElapsedEventHandler(this.timer2_Elapsed);
            // 
            // frmMain
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(1112, 549);
            this.Controls.Add(this.panelTab);
            this.Controls.Add(this.panelLabel);
            this.Controls.Add(this.panelTop);
            this.Controls.Add(this.bcBlinker1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.Text = "Location Changes:";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.frmMain_Closing);
            this.Resize += new System.EventHandler(this.frmMain_Resize);
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.u)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axUfisCom1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LoginControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panelLabel.ResumeLayout(false);
            this.panelTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabLOG)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxTab)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timer2)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /*		
                [STAThread]
                static void Main() 
                {
                    Application.Run(new frmMain()); 
                }
        */
        [STAThread]
        static void Main(string[] args)
        {
            frmMain olDlg = new frmMain();
            //args = new string[1];
            //args[0] = "363127;51011110;783626226;59368702,AFT,ALL FIELDS,FIELD,LOCAL,,C,";
            olDlg.args = args;
            Application.Run(olDlg);
        }

        private void pictureBox1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            //UT.DrawGradient(e.Graphics, 90f, pictureBox1, Color.WhiteSmoke, Color.LightGray);
            UT.DrawGradient(e.Graphics, 90f, pictureBox1, Color.AliceBlue, Color.LightSteelBlue);//Color.CornflowerBlue);

        }

        private bool LoginProcedure()
        {
            //return true; 
            string LoginAnsw = "";
            string tmpRegStrg = "";
            string strRet = "";
            LoginControl.ApplicationName = "LocationChanges";
            LoginControl.InfoCaption = "Info about Location Changes";
            LoginControl.InfoButtonVisible = true;
            LoginControl.InfoUfisVersion = "Ufis Version 4.5";
            LoginControl.UserNameLCase = true;
            tmpRegStrg = "LocationChanges" + ",";
            tmpRegStrg += "InitModu,InitModu,Initialisieren (InitModu),B,-";
            LoginControl.RegisterApplicationString = tmpRegStrg;

            try
            {
                strRet = LoginControl.ShowLoginDialog();
                LoginAnsw = LoginControl.GetPrivileges("InitModu");
                if (LoginAnsw != "" && strRet != "CANCEL")
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        private bool InitFormWithArgs()
        {
            if (args.Length != 0)
            {
                string[] argList = args[0].Split(',');
                if (argList.Length == 8)
                {
                    m_Urno = argList[0];
                    m_TableName = argList[1];
                    m_FieldName = argList[2];
                    m_ViewMode = argList[3];
                    m_TimeMode = argList[4];
                    m_Listener = argList[5];
                    m_FieldType = "C";

                    UT.UserName = argList[7];

                    if (m_FieldName.Equals("XXXX"))
                    {
                        m_FieldName = "ALL FIELDS";
                        myNeedCCA = ",'CCA'";
                    }


                    DateTime datLocal = dateTimePickerFrom.Value;

                    TimeSpan span = new TimeSpan(365, 0, 0, 0);
                    datLocal = datLocal - span;

                    dateTimePickerFrom.Value = new DateTime(datLocal.Year, datLocal.Month, datLocal.Day, 0, 0, 0, 0);

                }
                else
                {
                    MessageBox.Show(this, "Data Changes has been called with unexpected parameters:  " + args[0], "Error in parameterlist for Data Changes", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    Application.Exit();
                    return false;
                }
            }
            else
            {
                if (LoginProcedure() == false)
                {
                    Application.Exit();
                    return false;
                }

                //if called without args: mode is used for afttab and location changes, no information will be sended
                m_ViewMode = "LOCATIONS";
                m_TableName = "AFT";
                m_Listener = "";
                m_FieldType = "C";
            }

            return true;
        }

        private void PreFormatForm()
        {
            cbView.Parent = pictureBox1;
            cbUTC.Parent = pictureBox1;
            btnApply.Parent = pictureBox1;
            btnPrint.Parent = pictureBox1;
            btnClose.Parent = pictureBox1;
            cbFlnu.Parent = pictureBox1;
            lblTimer.Parent = pictureBox1;
            lblInMinutes.Parent = pictureBox1;

            lblFrom.Parent = pictureBox1;
            lblTo.Parent = pictureBox1;
            btnNow.Parent = pictureBox1;
            cbTimer.Parent = pictureBox1;

            dateTimePickerFrom.CustomFormat = "dd.MM.yyyy - HH:mm";
            dateTimePickerFrom.Value = new DateTime(dateTimePickerFrom.Value.Year, dateTimePickerFrom.Value.Month, dateTimePickerFrom.Value.Day, 0, 0, 0, 0);
            dateTimePickerTo.CustomFormat = "dd.MM.yyyy - HH:mm";
            dateTimePickerTo.Value = new DateTime(dateTimePickerTo.Value.Year, dateTimePickerTo.Value.Month, dateTimePickerTo.Value.Day, 23, 59, 59, 0);

            // set form to requested timemode
            if (m_TimeMode == "U")
                cbUTC.Checked = true;
            else
                cbUTC.Checked = false;
        }

        private void InitUtils()
        {
            String strConfigPath = String.Empty;


            //Read ceda.ini and set it global to UT (UfisTools)
            String strServer = "";
            String strHopo = "";
            Ufis.Utils.IniFile myIni = new IniFile("C:\\Ufis\\system\\ceda.ini");
            strServer = myIni.IniReadValue("GLOBAL", "HOSTNAME");
            strHopo = myIni.IniReadValue("GLOBAL", "HOMEAIRPORT");
            UT.ServerName = strServer;
            UT.Hopo = strHopo;



            if (UT.UserName.Length == 0)
                UT.UserName = LoginControl.GetUserName();

            UT.IsTimeInUtc = true;

            DateTime datLocal = DateTime.Now;
            DateTime datUTC = DateTime.UtcNow;
            TimeSpan span = datLocal - datUTC;
            string strUTCConvertion = "";
            strUTCConvertion = myIni.IniReadValue("GLOBAL", "UTCCONVERTIONS");
            if (strUTCConvertion == "NO")
            {
                UT.UTCOffset = 0;
                HandleUTC();
            }
            else
            {
                UT.UTCOffset = (short)span.Hours;
            }


            //DE.InitDBObjects(bcBlinker1);
            myDB = UT.GetMemDB();
            bool result = myDB.Connect(UT.ServerName, UT.UserName, UT.Hopo, "TAB", "DATACHNG");

            frmData data = new frmData();
            data.LoadSYSData();

            CtrlConfig ctrlConfig = CtrlConfig.GetInstance();
            ctrlConfig.LoadConfigInfo();
            //LinkInfoToATAble aftLinkInfo = ctrlConfig.GetLinkInfoFor("AFT");

        }

        private void CreateLOGTAB()
        {
            //login table for m_TableName
            string strLoginTable = "";
            string strShortTable = "";
            IUfisComWriter myUfisCom = UT.GetUfisCom();
            int ilRet = myUfisCom.CallServer("RT", "SYSTAB", "LOGD", "", "WHERE TANA = '" + m_TableName + "'", "230");
            long ilCount = myUfisCom.GetBufferCount();
            if (ilCount != 0)
            {
                string strBuffer = myUfisCom.GetDataBuffer(false);
                strLoginTable = myUfisCom.GetBufferLine(0);
                strShortTable = strLoginTable.Substring(0, 3);
                m_AftLogtab = strShortTable;

                myUfisCom.ClearDataBuffer();
            }

            if (ilRet != 0)
            {
                string strERR = myUfisCom.LastErrorMessage;
                MessageBox.Show(this, strERR);
            }

            // create logTAB and define tabLOG

            //logTAB = myDB.Bind("L0G", strShortTable, "Flight,Location,Tran,Old Value,New Value,Time of Change,Changed by User,Urno,Keyf,STOX", "10,10,10,10,10,14,10,10,10,14", "UREF,FINA,TRAN,h_tab_old_value(tana,fina,uref,time),VALU,TIME,USEC,URNO,KEYF,STOX");
            logTAB = myDB.Bind("L0G", strShortTable,
                CtrlLog.LOGTAB_LOGICAL_FIELDS,
                CtrlLog.LOGTAB_FIELD_LENGTHS,
                CtrlLog.LOGTAB_DB_FIELDS
                );
            AlterLogTabForFlightData();

            tabLOG.LifeStyle = true;
            tabLOG.LineHeight = 20;
            tabLOG.FontName = "Courier New"; //"Arial"
            tabLOG.FontSize = 16;
            tabLOG.HeaderFontSize = 16;
            tabLOG.DefaultCursor = false;
            tabLOG.InplaceEditUpperCase = false;
            tabLOG.EnableHeaderSizing(true);
            tabLOG.ShowHorzScroller(true);
            tabLOG.AutoSizeByHeader = true;

            // show tabLOG
            tabLOG.SetInternalLineBuffer(true);
            tabLOG.Refresh();
        }

        private void LoginTableForCCA()
        {
            string strLoginTable = "";
            IUfisComWriter myUfisCom = UT.GetUfisCom();

            int ilRet = myUfisCom.CallServer("RT", "SYSTAB", "LOGD", "", "WHERE TANA = 'CCA'", "230");
            long ilCount = myUfisCom.GetBufferCount();
            if (ilCount != 0)
            {
                string strBuffer = myUfisCom.GetDataBuffer(false);
                strLoginTable = myUfisCom.GetBufferLine(0);
                m_CcaLogtab = strLoginTable.Substring(0, 3);
                myUfisCom.ClearDataBuffer();
            }

            if (ilRet != 0)
            {
                string strERR = myUfisCom.LastErrorMessage;
                MessageBox.Show(this, strERR);
            }
        }

        private void HandleViewMode()
        {
            string strHopo = UT.Hopo;
            if (!strHopo.Equals("PVG"))
            {
                cbFlnu.Hide();
                txtURNOValue.Hide();
            }

            // handle the requested viewmode
            if (m_ViewMode == "LOCATIONS")
            {
                // set text
                this.Text = "Location Changes";
                // load all necessary tables for the viewdialog
                DataForm = new frmData();
                //				DataForm.Show();
                DataForm.Hide();
                DataForm.LoadData();
                //init view
                FillSelection();
            }
            else if (m_ViewMode == "FIELD")
            {
                // set text
                this.Text = "Data Changes";
                m_ActiveField = "Active Field: " + m_FieldName;

                // fill selection for flightdata
                string urno = m_Urno.Replace(";", ",");
                string selection = "WHERE URNO IN (" + urno + ")";
                //MessageBox.Show(this, selection, "Updating AFTTAB", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                // don't select flightdata via view
                //cbView.BackColor = Color.Red;
                cbView.Enabled = false;
                cbFlnu.Hide();
                txtURNOValue.Hide();

                // load flightdata from calling urnos
                DataForm = new frmData();
                //				DataForm.Show();
                DataForm.Hide();
                //only flightdata is needed
                DataForm.LoadAFTData(selection);
                DataForm.LoadSYSData();

                if (m_TableName.Equals("CCA"))
                    //					DataForm.LoadXXXData("CCA", "CheckIn-Counter", "URNO,FLNO,CNAM,FLNU", "30,30,30,30", "FLNU", "FLNU,CNAM", "URNO,FLNU");
                    DataForm.LoadCCAData("CCA");

                //additional for logtabs
                //				myStrWhereALOCIn = "WHERE TANA = '" + m_TableName + "'" + " AND FINA = '" + m_FieldName + "'";
                myStrWhereALOCIn = " AND TANA = '" + m_TableName + "'" + " AND FINA = '" + m_FieldName + "'";
                if (m_FieldName.Equals("ALL FIELDS"))
                {
                    //myStrWhereALOCIn = "WHERE TANA = '" + m_TableName + "'";
                    DataForm.LoadCCAData("CCA");
                    //					myStrWhereALOCIn = "WHERE TANA IN ('" + m_TableName + "'" + myNeedCCA + ")";
                    //					myStrWhereALOCIn = " AND TANA IN ('" + m_TableName + "'" + myNeedCCA + ")";
                    string strFina = "";
                    if (myAllFieldsAFT.Equals("") && myAllFieldsCCA.Equals(""))
                    {
                        strFina = "";
                        myStrWhereALOCIn = " AND TANA IN ('" + m_TableName + "'" + myNeedCCA + ")";
                    }
                    else
                    {
                        if (myAllFieldsAFT.Equals(""))
                            myAllFieldsAFT = "' '";

                        if (myAllFieldsCCA.Equals(""))
                            myAllFieldsCCA = "' '";

                        strFina = " AND (FINA IN (" + myAllFieldsAFT + ")";
                        myStrWhereALOCIn = " AND (TANA = " + "'" + m_TableName + "'" + strFina + ")";

                        myStrWhereALOCIn += " OR ";

                        strFina = " AND FINA IN (" + myAllFieldsCCA + ")";
                        myStrWhereALOCIn += " ( TANA = 'CCA'" + strFina + "))";
                    }
                }
                myStrSortOrder = " ORDER BY TIME DESC, SEQN DESC";

                //automatic selection and view
                FillSelection();

                //handle active timemode
                HandleUTC();
            }
            else//CKI
            {
                this.Text = "Common Checkin";
            }
        }

        private void frmMain_Load(object sender, System.EventArgs e)
        {
            if (!InitFormWithArgs())
                return;
            PreFormatForm();
            InitUtils();
            CreateLOGTAB();
            LoginTableForCCA();
            ReadIni();
            HandleViewMode();
            ReadRegistry();
        }
        internal string CheckFilePath(string ropFilePath)
        {
            string olFilePath = ropFilePath;
            olFilePath = olFilePath.ToUpper();
            olFilePath = olFilePath.Replace('/', '\\');

            int ind = olFilePath.LastIndexOf('\\');
            if (ind < 0)	// nothing to do
                return olFilePath;

            int i = olFilePath.Substring(0, ind).CompareTo("C:\\UFIS\\SYSTEM");

            if (olFilePath.Substring(0, ind).CompareTo("C:\\UFIS\\SYSTEM") != 0)
                return olFilePath;

            string olConfigFileName = Environment.GetEnvironmentVariable("CEDA");

            if (olConfigFileName == null)
                return olFilePath;

            olConfigFileName = olConfigFileName.ToUpper();
            olConfigFileName = olConfigFileName.Replace('/', '\\');

            int ind2 = olConfigFileName.LastIndexOf('\\');
            if (ind2 < 0)	// nothing to do
                return olFilePath;

            olFilePath = olConfigFileName.Substring(0, ind2) + olFilePath.Substring(ind);

            return olFilePath;
        }

        private void ReadIni()
        {
            myIniFile = CheckFilePath("C:\\Ufis\\System\\DATACHANGE.ini");
            //			string myAllField = "";
            FileStream fin;
            try
            {
                fin = new FileStream(myIniFile, FileMode.OpenOrCreate, FileAccess.Read);
            }
            catch (IOException exc)
            {
                MessageBox.Show(this, myIniFile, exc.Message + "Read Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }


            string txtLine = "";
            string txtFieldsAFT = "";
            string txtFieldsCCA = "";
            string strFieldsCKI = "";
            bool startAllFields = false;
            bool startAllFieldsAFT = false;
            bool startAllFieldsCCA = false;
            bool startAllFieldsCKI = false;
            StreamReader fstr_in = new StreamReader(fin);
            while ((txtLine = fstr_in.ReadLine()) != null)
            {
                if (txtLine.Equals("[ALL_FIELDS_END]"))
                {
                    startAllFields = false;
                    continue;
                }

                if (txtLine.Equals("AFTTAB_END"))
                {
                    startAllFieldsAFT = false;
                    continue;
                }

                if (txtLine.Equals("CCATAB_END"))
                {
                    startAllFieldsCCA = false;
                    continue;
                }

                if (txtLine.Equals("CCATAB_COMMON_END"))
                {
                    startAllFieldsCKI = false;
                    continue;
                }

                if (startAllFields && startAllFieldsAFT && !txtLine.Equals(""))
                {
                    txtFieldsAFT += "'" + txtLine.Substring(0, 4) + "',";
                }

                if (startAllFields && startAllFieldsCCA && !txtLine.Equals(""))
                {
                    txtFieldsCCA += "'" + txtLine.Substring(0, 4) + "',";
                }

                if (startAllFields && startAllFieldsCKI && !txtLine.Equals(""))
                {
                    strFieldsCKI += "'" + txtLine.Substring(0, 4) + "',";
                }

                if (txtLine.Equals("[ALL_FIELDS_BEG]"))
                {
                    startAllFields = true;
                    continue;
                }

                if (txtLine.Equals("AFTTAB_BEG"))
                {
                    startAllFieldsAFT = true;
                    continue;
                }

                if (txtLine.Equals("CCATAB_BEG"))
                {
                    startAllFieldsCCA = true;
                    continue;
                }

                if (txtLine.Equals("CCATAB_COMMON_BEGIN"))
                {
                    startAllFieldsCKI = true;
                    continue;
                }

            }
            fstr_in.Close();

            if (txtFieldsAFT.Length > 0)
            {
                myAllFieldsAFT = txtFieldsAFT.Substring(0, txtFieldsAFT.Length - 1);
            }

            if (txtFieldsCCA.Length > 0)
            {
                myAllFieldsCCA = txtFieldsCCA.Substring(0, txtFieldsCCA.Length - 1);
            }

            //fields to show for CKI
            if (strFieldsCKI.Length > 0)
            {
                myAllFieldsCKI = strFieldsCKI.Substring(0, strFieldsCKI.Length - 1);
            }
        }


        private void WriteRegistry()
        {
            string theKey = "Software\\UFIS\\DATACHANGES\\MainWindow";
            RegistryKey rk = Registry.CurrentUser.CreateSubKey(theKey);
            if (rk == null)
            {
                rk.CreateSubKey(theKey);
                rk = Registry.CurrentUser.OpenSubKey(theKey, true);
            }

            rk.SetValue("X", this.Left.ToString());
            rk.SetValue("Y", this.Top.ToString());
            rk.SetValue("Width", this.Width.ToString());
            rk.SetValue("Height", this.Height.ToString());
            rk.Close();
        }

        private void ReadRegistry()
        {
            int myX = -1, myY = -1, myW = -1, myH = -1;
            string regVal = "";
            string theKey = "Software\\UFIS\\DATACHANGES\\MainWindow";
            RegistryKey rk = Registry.CurrentUser.OpenSubKey(theKey, true);
            if (rk != null)
            {
                regVal = rk.GetValue("X", "-1").ToString();
                myX = Convert.ToInt32(regVal);
                regVal = rk.GetValue("Y", "-1").ToString();
                myY = Convert.ToInt32(regVal);
                regVal = rk.GetValue("Width", "-1").ToString();
                myW = Convert.ToInt32(regVal);
                regVal = rk.GetValue("Height", "-1").ToString();
                myH = Convert.ToInt32(regVal);
            }
            if (myW != -1 && myH != -1)
            {
                this.Left = myX;
                this.Top = myY;
                this.Width = myW;
                this.Height = myH;
            }
        }

        private void tabLOG_SendLButtonDblClick(object sender, AxTABLib._DTABEvents_SendLButtonDblClickEvent e)
        {
            tabLOG_SortColumn(e.lineNo, e.colNo);
        }

        private void tabLOG_SortColumn(int lineNo, int colNo)
        {
            if (lineNo == -1)
            {
                if (colNo == tabLOG.CurrentSortColumn)
                {
                    if (tabLOG.SortOrderASC == true)
                    {
                        tabLOG.Sort(colNo.ToString(), false, true);
                    }
                    else
                    {
                        tabLOG.Sort(colNo.ToString(), true, true);
                    }
                }
                else
                {
                    //tabLOG.Sort(colNo.ToString(), true, true);
                    tabLOG.Sort(colNo.ToString(), false, true);
                }
                tabLOG.Refresh();
            }
        }


        private void FillUrefLogTAB()
        {
            ArrayList arrayUREF = new ArrayList(200);
            for (int i = 0; i < logTAB.Count; i++)
            {
                string strUref = logTAB[i]["Flight"];
                //				string strUref = tabLOG.GetFieldValue(i, "UREF");
                if (!arrayUREF.Contains(strUref))
                    arrayUREF.Add(strUref);

            }

            string strUREF = "";
            for (int j = 0; j < arrayUREF.Count; j++)
            {
                if (j > 0)
                    strUREF += ",";

                strUREF += arrayUREF[j];
            }

            if (strUREF.Length > 0)
                myStrWhereFLNUIn = " AND UREF IN (" + strUREF + ")";
            else
                myStrWhereFLNUIn = "";
        }

        private void DoDeletedFlights()
        {
            //clear table
            tabLOG.ResetContent();

            //part of selection for UREF
            m_FieldName = "ALL FIELDS";
            m_ActiveField = "Active Field: " + m_FieldName;
            //RSTRST myNeedCCA = ",'CCA'";
            myNeedCCA = "";
            //			string Selection = mySelectionLOGTABKeyf + myStrWhereTimeframe;

            string myStrWhereUser = " AND USEC = '" + m_UserName1 + "'";
            string Selection;
            if (m_UserName1 != "ALL USER" && m_UserName1 != string.Empty)
                Selection = myStrWhereTimeframe + myStrWhereUser + mySelectionLOGTABKeyf;
            else
                Selection = myStrWhereTimeframe + mySelectionLOGTABKeyf;

            logTAB.Clear();
            //			MessageBox.Show(this, mySelection, "Updating LOGATB", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            /*			if (DataForm.LoadAFTData(mySelection) == false)
                        {
                            int z = 0;
                        }
            */
            logTAB.Load(Selection);

            return;
        }

        private void cbView_CheckedChanged(object sender, System.EventArgs e)
        {
            //if red m_ViewMode is "FIELD", don#t use viewdialog because flight is set via args[]
            if (cbView.BackColor == Color.Red || m_ViewMode.Equals("FIELD"))
            {
                this.DialogResult = DialogResult.None;
                return;
            }

            if (cbView.Checked == true)
            {
                if (cbFlnu.BackColor == Color.LightGreen)
                {
                    cbFlnu.Checked = false;
                    cbFlnu.BackColor = Color.Transparent;
                    cbFlnu.Refresh();
                }

                cbView.BackColor = Color.LightGreen;
                cbView.Refresh();

                // Reset the Time Fields.
                int idx = UT.GetItemNo(tabLOG.LogicalFieldList, "TIME");
                tabLOG.DateTimeResetColumn(idx);
                if (tabLOG.LogicalFieldList.IndexOf("STOX") != -1)
                {
                    idx = UT.GetItemNo(tabLOG.LogicalFieldList, "TIME");
                    tabLOG.DateTimeResetColumn(idx);
                }

                //call viewdialog
                frmView Dlg = new frmView();
                if (Dlg.ShowDialog(this) == DialogResult.OK)
                {
                    this.Cursor = Cursors.WaitCursor;


                    this.Refresh();
                    tabLOG.Refresh();
                    this.Text = Dlg.myWindowText;
                    m_DeleteFlightsChecked = Dlg.rbDeletedFlights.Checked;
                    m_SelectedTabPage = Dlg.omSelectedTabPage;
                    m_UserName1 = Dlg.User1;
                    m_UserName2 = Dlg.User2;
                    m_UserName3 = Dlg.User3;

                    m_selCKI = Dlg.m_selCKI;

                    Ufis.Utils.IniFile myIni = new IniFile("C:\\Ufis\\system\\DATACHANGE.INI");

                    // Changes for the Basic Table.
                    if ("Basic Data" == m_SelectedTabPage)
                    {
                        m_BasicTableCode = Dlg.omTableCode;
                        if (Dlg.cbTakeOver_BD.Checked == true)
                        {
                            this.dateTimePickerFrom.Value = Dlg.dateTimePickerFrm_BD.Value;
                            this.dateTimePickerTo.Value = Dlg.dateTimePickerTo_BD.Value;
                            this.dateTimePickerFrom.Refresh();
                            this.dateTimePickerTo.Refresh();
                        }
                        // Get which HXX table is linked to the Selected Basic Data Table
                        m_SysTab = myDB.Bind("SYS", "SYS", "URNO,FINA,TANA,LOGD,ADDI", "10,4,3,6,50", "URNO,FINA,TANA,LOGD,ADDI");
                        m_SysTab.Clear();
                        m_SysTab.Load("WHERE TANA IN ('" + m_BasicTableCode + "','FLG') AND LOGD != ' '");


                        if (m_SysTab.Count > 0 && m_SysTab[0]["LOGD"].Length == 6)
                        {
                            m_HxxTabStr = (m_SysTab[0]["LOGD"]).Substring(0, 3);
                        }
                        else
                        {
                            MessageBox.Show("There is no HXX Table linked to the Selected Basic Data Table in the SYSTAB");
                            this.Cursor = Cursors.Arrow;
                            cbView.Checked = false;
                            cbView.BackColor = Color.Transparent;
                            return;
                        }

                        // Fields to load from the Selected Basic Data Table.
                        //Ufis.Utils.IniFile myIni = new IniFile("C:\\Ufis\\system\\DATACHANGE.INI");
                        m_strIdentifierCodes = myIni.IniReadValue(m_BasicTableCode, "IDENTIFIER");

                        // Create the LOG Table for the Baisc Data Table
                        CreateBasicDataTable();

                        string StrTimeDuration = (Dlg.mySelection.Split(')'))[0] + ")"; // Only the Time Duration(from/to)

                        m_FilterConStr = (Dlg.mySelection.Split(')'))[1];

                        // Fille the Table with the Basic Data Changes.
                        FillBasicDataSelection(StrTimeDuration);

                        // calculate number of changes
                        string sumChanges = tabLOG.GetLineCount().ToString();
                        ITable omTabTab = myDB.Bind("TAB", "TAB", "URNO,Table-Code,Table-Name", "10,3,32", "URNO,LTNA,LNAM");
                        omTabTab.Clear();
                        omTabTab.Load("WHERE LTNA='" + m_BasicTableCode + "'");
                        lblLocations.Text = "Loaded Changes: " + sumChanges + " (for " + omTabTab[0]["Table-Name"] + " Table)";

                        return;
                    } // End Basic Data
                    else if ("Common CKI" == m_SelectedTabPage)
                    {
                        // Get which HXX table is linked to the Selected Basic Data Table
                        m_SysTab = myDB.Bind("SYS", "SYS", "URNO,FINA,TANA,LOGD,ADDI", "10,4,3,6,50", "URNO,FINA,TANA,LOGD,ADDI");
                        m_SysTab.Clear();
                        //m_SysTab.Load("WHERE TANA='CCA' AND LOGD != ' '");
                        m_SysTab.Load("WHERE TANA IN ('CCA','FLZ','FLG') ");

                        // Fields to load from the CKI Table.
                        //m_strCKIIdentifiers = myIni.IniReadValue("COMMON_CKI", "IDENTIFIER");
                        string StrTimeDuration = (Dlg.mySelection.Split(')'))[0] + ")"; // Only the Time Duration(from/to)
                        CreateCommonCKIDatatable();

                        FillTIME();
                        FillCommonCKISelection(myStrWhereTimeframe);

                        // calculate number of changes
                        string sumChanges = tabLOG.GetLineCount().ToString();
                        ITable omTabTab = myDB.Bind("TAB", "TAB", "URNO,Table-Code,Table-Name", "10,3,32", "URNO,LTNA,LNAM");
                        omTabTab.Clear();
                        omTabTab.Load("WHERE LTNA='" + "CCA" + "'");
                        lblLocations.Text = "Loaded Changes: " + sumChanges + " (for " + omTabTab[0]["Table-Name"] + " Table)";

                        return;
                    }

                    if (Dlg.cbTakeOver.Checked == true)
                    {
                        this.dateTimePickerFrom.Value = Dlg.dateTimePickerFrom.Value;
                        this.dateTimePickerTo.Value = Dlg.dateTimePickerTo.Value;
                        this.dateTimePickerFrom.Refresh();
                        this.dateTimePickerTo.Refresh();
                    }

                    //#####
                    //Load System Table.
                    DataForm.LoadSYSData();

                    //Alter the LogTable to show the Flight Data
                    AlterLogTabForFlightData();


                    myboolDoingLogtab = false;
                    mySelectionLOGTABKeyf = "";
                    string fields = "";
                    string strFrom = "";
                    string strTo = "";


                    //					if (Dlg.mySelection.StartsWith("WHERE KEYF"))
                    //if (Dlg.mySelectionDeleted.StartsWith(" KEYF"))

                    if (Dlg.rbDeletedFlights.Checked == true)
                    {
                        myboolDoingLogtab = true;
                        mySelectionLOGTABKeyf = Dlg.mySelectionDeleted;
                        m_FieldName = "ALL FIELDS";
                        myNeedCCA = ",'CCA'";

                        FillSelection();
                    }
                    else
                    {
                        //No Flights found for mySelection
                        if ("Common CKI" != m_SelectedTabPage)
                        {
                            if (DataForm.LoadAFTData(Dlg.mySelection) == false)
                            {
                                this.Cursor = Cursors.Arrow;
                                //MessageBox.Show(this, Dlg.mySelection, "No Flights found", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                MessageBox.Show(this, "No Data found!", "Data Changes", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                cbView.Checked = false;
                                cbView.BackColor = Color.Transparent;
                                return;
                            }
                        }


                        if (Dlg.rbChanged.Checked == true)
                        {
                            myboolDoingLogtab = false;
                            mySelectionLOGTABKeyf = Dlg.mySelectionDeleted;
                            m_FieldName = "ALL FIELDS";
                            myNeedCCA = ",'CCA'";
                            myStrWhereALOCIn = "";
                            //FillSelection();
                            //return;
                        }
                        else
                        {


                            //set the requested locations
                            m_ActiveField = "Active Locations: ";
                            myNeedCCA = "";
                            //						string fields = "";
                            for (int i = 0; i < Dlg.myLocation.Count; i++)
                            {
                                if (Dlg.myLocation[i].ToString() == "PST")
                                {
                                    m_ActiveField += "  Position  ";
                                    fields += m_PST_Fields;
                                }
                                if (Dlg.myLocation[i].ToString() == "GAT")
                                {
                                    m_ActiveField += "  Gate  ";
                                    fields += m_GAT_Fields;
                                }
                                if (Dlg.myLocation[i].ToString() == "BLT")
                                {
                                    m_ActiveField += "  Belt  ";
                                    fields += m_BLT_Fields;
                                }
                                if (Dlg.myLocation[i].ToString() == "WRO")
                                {
                                    m_ActiveField += "  Lounge  ";
                                    fields += m_WRO_Fields;
                                }
                                if (Dlg.myLocation[i].ToString() == "EXT")
                                {
                                    m_ActiveField += "  Exit    ";
                                    fields += m_EXT_Fields;
                                }
                                if (Dlg.myLocation[i].ToString() == "RWY") //Runway
                                {
                                    m_ActiveField += "  Runway  ";
                                    fields += m_RWY_Fields;
                                }
                                if (Dlg.myLocation[i].ToString() == "CCA")
                                {
                                    m_ActiveField += "  Check-In";
                                    //also can be used for an another table
                                    fields += m_CCA_Fields;
                                    myNeedCCA = ",'CCA'";
                                    //							DataForm.LoadXXXData("CCA", "CheckIn-Counter", "URNO,FLNO,CNAM,FLNU", "30,30,30,30", "FLNU", "FLNU,CNAM", "URNO,FLNU");
                                    DataForm.LoadCCAData("CCA");
                                }
                            }
                        }
                    }

                    //part of the selection for TANA and FINA
                    string tmp = fields.Replace(" ", ",");
                    //					tmp += "' ' ";	//SISL

                    if (tmp.Length > 0)
                        m_FieldName = tmp.Substring(0, tmp.Length - 1);

                    if (!m_FieldName.Equals(""))
                    {
                        string strFina = "AND FINA IN (" + m_FieldName + ")";
                        string strCcaFina = "AND FINA IN (" + m_CCA_ExtFields + ")";
                        if (m_FieldName.Equals("ALL FIELDS"))
                        {
                            if (myAllFieldsAFT.Equals("") && myAllFieldsCCA.Equals(""))
                                strFina = "";
                            else
                            {
                                if (myAllFieldsAFT.Equals(""))
                                    myAllFieldsAFT = "' '";

                                if (myAllFieldsCCA.Equals(""))
                                    myAllFieldsCCA = "' '";

                                strFina = " AND (FINA IN (" + myAllFieldsAFT + ")";
                                myStrWhereALOCIn = " AND (TANA = " + "'" + m_TableName + "'" + strFina + ")";

                                myStrWhereALOCIn += " OR ";

                                strFina = " AND FINA IN (" + myAllFieldsCCA + ")";
                                myStrWhereALOCIn += " ( TANA = 'CCA'" + strFina + "))";
                            }
                        }
                        else
                        {
                            //						myStrWhereALOCIn = "WHERE TANA IN ('" + m_TableName + "'" + myNeedCCA + ")" + strFina;
                            myStrWhereALOCIn = " AND TANA IN ('" + m_TableName + "'" + myNeedCCA + ")" + strFina;

                            //myStrCcaWhereALOCIn = "WHERE TANA IN ('" + m_TableName + "'" + myNeedCCA + ")" + strCcaFina;
                        }
                    }
                    else
                    {
                        myStrWhereALOCIn = "";
                    }

                    //retrive data from the logTabs
                    FillSelection();
                    this.Cursor = Cursors.Arrow;

                }
                cbView.Checked = false;
                cbView.BackColor = Color.Transparent;
            }

        }

        private void frmMain_Resize(object sender, System.EventArgs e)
        {
            pictureBox1.Invalidate();
            pictureBoxTab.Invalidate();
        }

        private void label1_Click(object sender, System.EventArgs e)
        {

        }

        private void pictureBoxTab_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            UT.DrawGradient(e.Graphics, 90f, pictureBoxTab, Color.WhiteSmoke, Color.LightGray);
        }

        private void cbUTC_CheckedChanged(object sender, System.EventArgs e)
        {
            HandleUTC();
        }


        //if more forms needs to handle the utc/local use a delegate
        private void HandleUTC()
        {


            if (cbUTC.Checked == true)
                //cbUTC.BackColor = Color.LightGreen;
                cbUTC.Text = "UTC";
            else
                //cbUTC.BackColor = Color.Transparent;
                cbUTC.Text = "Local";

            cbUTC.Refresh();

            if (!myIsInit)
                return;

            if ((cbUTC.Checked == true && myIsTimeInUtc == false) || (cbUTC.Checked == false && myIsTimeInUtc == true))
            {
                // conversion needed
            }
            else
                return;


            // convert timefields if necesarry

            string strTimeFields = logTAB.TimeFields;
            if ("Common CKI" == m_SelectedTabPage)
            {
                strTimeFields = logCKITAB.TimeFields;
            }



            string[] arrAftTimeFields = strTimeFields.Split(',');
            DateTime dat;
            string strTmp = "";

            for (int i = 0; i < tabLOG.GetLineCount(); i++)
            {
                for (int j = 0; j < arrAftTimeFields.Length; j++)
                {
                    string test = arrAftTimeFields[j];
                    strTmp = tabLOG.GetFieldValue(i, test);
                    if (strTmp.Equals(" ") || strTmp.Equals(""))
                        continue;
                    {
                        if (strTmp.Length == 12)
                            strTmp += "00";
                        dat = UT.CedaFullDateToDateTime(strTmp);

                        if (cbUTC.Checked == true && myIsTimeInUtc == false)
                            dat = UT.LocalToUtc(dat);
                        if (cbUTC.Checked == false && myIsTimeInUtc == true)
                            dat = UT.UtcToLocal(dat);

                        strTmp = UT.DateTimeToCeda(dat);
                        tabLOG.SetFieldValues(i, test, strTmp);
                    }
                }

                DateTime datRet;
                int ilYY = 0;
                int ilMo = 0;
                int ilDD = 0;
                int ilHH = 0;
                int ilMM = 0;
                int ilSS = 0;

                string str3 = "OVAL";

                strTmp = tabLOG.GetFieldValue(i, str3);

                if (strTmp.Length == 18)
                {
                    if (strTmp[15] == ':')
                    {
                        ilYY = Convert.ToInt32(strTmp.Substring(6, 4));
                        ilMo = Convert.ToInt32(strTmp.Substring(3, 2));
                        ilDD = Convert.ToInt32(strTmp.Substring(0, 2));
                        ilHH = Convert.ToInt32(strTmp.Substring(13, 2));
                        ilMM = Convert.ToInt32(strTmp.Substring(16, 2));
                        ilSS = 0;
                        datRet = new DateTime(ilYY, ilMo, ilDD, ilHH, ilMM, ilSS);

                        if (cbUTC.Checked == true && myIsTimeInUtc == false)
                            datRet = UT.LocalToUtc(datRet);
                        if (cbUTC.Checked == false && myIsTimeInUtc == true)
                            datRet = UT.UtcToLocal(datRet);

                        tabLOG.SetFieldValues(i, "OVAL", datRet.ToString("dd.MM.yyyy '/' HH:mm"));
                    }
                }

                str3 = "VALU";

                strTmp = tabLOG.GetFieldValue(i, str3);
                if (strTmp.Length == 18)
                {
                    if (strTmp[15] == ':')
                    {
                        ilYY = Convert.ToInt32(strTmp.Substring(6, 4));
                        ilMo = Convert.ToInt32(strTmp.Substring(3, 2));
                        ilDD = Convert.ToInt32(strTmp.Substring(0, 2));
                        ilHH = Convert.ToInt32(strTmp.Substring(13, 2));
                        ilMM = Convert.ToInt32(strTmp.Substring(16, 2));
                        ilSS = 0;
                        datRet = new DateTime(ilYY, ilMo, ilDD, ilHH, ilMM, ilSS);

                        if (cbUTC.Checked == true && myIsTimeInUtc == false)
                            datRet = UT.LocalToUtc(datRet);
                        if (cbUTC.Checked == false && myIsTimeInUtc == true)
                            datRet = UT.UtcToLocal(datRet);

                        tabLOG.SetFieldValues(i, "VALU", datRet.ToString("dd.MM.yyyy '/' HH:mm"));
                    }
                }
            }

            tabLOG.Refresh();

            // set viewing timemode
            if (cbUTC.Checked == true)
                myIsTimeInUtc = true;
            else
                myIsTimeInUtc = false;

        }

        private void cbTimer_CheckedChanged(object sender, System.EventArgs e)
        {
            if (cbTimer.Checked == true)
            {
                cbTimer.BackColor = Color.LightGreen;
                timer2.Enabled = true;
                timer2.Interval = (double)numericUpDown1.Value * 60000;
                numericUpDown1.Enabled = false;
            }
            else
            {
                cbTimer.BackColor = Color.Transparent;
                timer2.Enabled = false;
                numericUpDown1.Enabled = true;
            }
            cbTimer.Refresh();
        }

        private void btnClose_Click(object sender, System.EventArgs e)
        {
            DataForm.Close();
            //Store current geometry in Registry
            WriteRegistry();

            //send close (-1) to m_Listener
            if (m_Listener.Length != 0)
            {
                IUfisComWriter myUfisCom = UT.GetUfisCom();
                int ilRet = myUfisCom.CallServer("SBC", "BCLOCC", "LOC_CHANGES", "-1", m_Listener, "230");
                if (ilRet != 0)
                {
                    string strERR = myUfisCom.LastErrorMessage;
                    MessageBox.Show(this, strERR);
                }
            }
            this.Close();
        }

        private void btnNow_Click(object sender, System.EventArgs e)
        {
            DateTime olNow;
            if (myIsTimeInUtc == true)
            {
                olNow = DateTime.UtcNow;
            }
            else
            {
                olNow = DateTime.Now;
            }
            dateTimePickerTo.Value = olNow;
        }

        private void timer2_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            cbTimer.BackColor = Color.HotPink;
            btnNow.BackColor = Color.HotPink;
            cbTimer.Refresh();
            btnNow.Refresh();
            btnNow_Click(sender, e);
            // Check which Tabpage was active.
            if ("Flight Data" == m_SelectedTabPage)
            {
                FillSelection();
            }
            if ("Common CKI" == m_SelectedTabPage)
            {
                FillTIME();
                FillCommonCKISelection(myStrWhereTimeframe);
            }
            else
            {
                //part of selection for TIME
                FillTIME();
                // Fill the Changes for the Selected Basic Data Table
                FillBasicDataSelection(myStrWhereTimeframe);
            }
            cbTimer.BackColor = Color.LightGreen;
            btnNow.BackColor = Color.Transparent;
            cbTimer.Refresh();
            btnNow.Refresh();
        }

        private void FillTIME()
        {
            //requested timeframe for changes
            string strFrom = UT.DateTimeToCeda(dateTimePickerFrom.Value);
            string strTo = UT.DateTimeToCeda(dateTimePickerTo.Value);

            DateTime datFrom = dateTimePickerFrom.Value;
            DateTime datTo = dateTimePickerTo.Value;
            if (myIsTimeInUtc == false)
            {
                datFrom = UT.LocalToUtc(datFrom);
                datTo = UT.LocalToUtc(datTo);
            }
            strFrom = UT.DateTimeToCeda(datFrom);
            strTo = UT.DateTimeToCeda(datTo);

            //part of selection for TIME
            //			myStrWhereTimeframe = " AND (TIME BETWEEN '" + strFrom + "' AND '" + strTo + "')";
            myStrWhereTimeframe = "WHERE (TIME BETWEEN '" + strFrom + "' AND '" + strTo + "')";
        }

        private void FillUREF(string strTable, string strField)
        {
            //requested changes for flights (loaded with the selection of frmView or via args)
            ITable aftTable = myDB[strTable];

            if (strTable.Equals("LOG"))
                aftTable = logTAB;

            if (aftTable == null)
                return;

            //part of selection for UREF
            ArrayList arrayUREF = new ArrayList(200);
            myStrWhereFLNUIn = "";
            myStrCcaWhereFLNUIn = "";
            string strUREF = "";
            string strFLNU = "";
            for (int i = 0; i < aftTable.Count; i++)
            {
                string strUref = aftTable[i][strField];
                if (strUref.Length > 0 && !arrayUREF.Contains(strUref))
                    arrayUREF.Add(strUref);
                else
                    continue;
            }

            if (strTable.Equals("AFT") || strTable.Equals("LOG"))
                m_FlightCount = arrayUREF.Count.ToString();

            strUREF = " AND UREF IN (";
            strFLNU = "WHERE FLNU IN (";

            if (strTable == "AFT")
            {
                myStrCCAUrnoFlnuMap = "";
                myStrREFUList = "";
            }

            for (int j = 0; j < arrayUREF.Count; j++)
            {
                string strUref = "";
                strUref += arrayUREF[j];

                if (strTable == "AFT")
                {
                    if (myStrREFUList.Length > 0)
                        myStrREFUList += ",";


                    myStrREFUList += "'" + arrayUREF[j] + "'";
                }

                if ((j % 300) == 0 && j > 0)
                {
                    strUREF += strUref + ")#";
                    strFLNU += strUref + ")#";
                    myStrWhereFLNUIn += strUREF;
                    myStrCcaWhereFLNUIn += strFLNU;
                    strUREF = " AND UREF IN (";
                    strFLNU = " AND FLNU IN (";
                }
                else
                {
                    if (j == (arrayUREF.Count - 1))
                    {
                        strUREF += strUref + ")";
                        strFLNU += strUref + ")";
                    }
                    else
                    {
                        strUREF += strUref + ",";
                        strFLNU += strUref + ",";
                    }
                }
            }

            if (!strUREF.Equals(" AND UREF IN ("))
                myStrWhereFLNUIn += strUREF;

            if (!strFLNU.Equals("WHERE FLNU IN ("))
                myStrCcaWhereFLNUIn += strFLNU;

            if (arrayUREF.Count == 0)
            {
                myStrWhereFLNUIn = "";
                myStrCcaWhereFLNUIn = "";
            }
        }

        private void BuildLine(int i, List<string> lsAdditionalTables)
        {
            bool addTabInfo = false;
            if ((lsAdditionalTables != null) && (lsAdditionalTables.Contains(logTAB[i]["TANA"]))) addTabInfo = true;

            string flightUrno = "";
            if (addTabInfo)
            {
                flightUrno = logTAB[i]["Keyf"];
            }
            else
            {
                flightUrno = logTAB[i]["Flight"];
            }

            if (myboolDoingLogtab)
            {
                string strKEYF = logTAB[i]["Keyf"];
                string strFlno = "";
                string strDay = "";
                string strAdid = "";
                if (strKEYF.Length == 18)
                {
                    string strFlnu = strKEYF.Substring(0, 5);
                    string strAlc3 = strKEYF.Substring(5, 3);
                    string strFlns = strKEYF.Substring(8, 1);
                    strDay = strKEYF.Substring(9, 8);
                    strAdid = strKEYF.Substring(17, 1);

                    strFlnu.TrimStart(' ');

                    if (strAlc3.Length == 2)
                        strFlno = strAlc3 + " " + strFlnu + " ";
                    else
                        strFlno = strAlc3 + strFlnu + " ";

                    if (!strFlns.Equals("#"))
                        strFlno += strFlns;
                }
                //search for the information of flights (loaded in "AFT") and fill the line
                ITable aftTable = myDB["AFT"];
                if (aftTable == null)
                    return;

                //				IRow [] rows = aftTable.RowsByIndexValue("URNO", tabLOG.GetFieldValue(i, "UREF"));
                //				if(rows.Length > 0)
                if (aftTable.Count > 0)
                {
                    string strUrno = aftTable[0]["URNO"];
                    //strFlno = aftTable[0]["FLNO"];
                    tabLOG.SetFieldValues(i, "UREF", strFlno);
                    //					tabLOG.SetFieldValues(i, "UREF", rows[0]["FLNO"]);
                    //					strFlno = logTAB[i]["Flight"];
                }

                string strSta = "";
                string strStd = "";
                string strLine = "";


                if (strAdid.Equals("A"))
                {
                    if (m_AddonFields)
                        strLine = strFlno + "," + logTAB[i]["STOX"] + "," + strStd + "," + strAdid + "," + "," + logTAB[i]["Location"] + "," + logTAB[i]["Tran"] + "," + CheckAndConvertDBString(logTAB[i]["Old Value"]) + "," + CheckAndConvertDBString(logTAB[i]["New Value"]) + "," + logTAB[i]["Time of Change"] + "," + logTAB[i]["Changed by User"] + "," + logTAB[i]["Urno"] + "," + logTAB[i]["Keyf"] + "," + logTAB[i]["STOX"];
                    else
                        strLine = strFlno + "," + logTAB[i]["STOX"] + "," + strStd + "," + strAdid + "," + "," + logTAB[i]["Location"] + "," + logTAB[i]["Tran"] + "," + CheckAndConvertDBString(logTAB[i]["Old Value"]) + "," + CheckAndConvertDBString(logTAB[i]["New Value"]) + "," + logTAB[i]["Time of Change"] + "," + logTAB[i]["Changed by User"] + "," + logTAB[i]["Furno"];
                }
                //				if (strAdid.Equals("D"))
                else //return flights and departures
                {
                    if (m_AddonFields)
                        strLine = strFlno + "," + strSta + "," + logTAB[i]["STOX"] + "," + strAdid + "," + "," + logTAB[i]["Location"] + "," + logTAB[i]["Tran"] + "," + CheckAndConvertDBString(logTAB[i]["Old Value"]) + "," + CheckAndConvertDBString(logTAB[i]["New Value"]) + "," + logTAB[i]["Time of Change"] + "," + logTAB[i]["Changed by User"] + "," + logTAB[i]["Urno"] + "," + logTAB[i]["Keyf"] + "," + logTAB[i]["STOX"];
                    else
                        strLine = strFlno + "," + strSta + "," + logTAB[i]["STOX"] + "," + strAdid + "," + "," + logTAB[i]["Location"] + "," + logTAB[i]["Tran"] + "," + CheckAndConvertDBString(logTAB[i]["Old Value"]) + "," + CheckAndConvertDBString(logTAB[i]["New Value"]) + "," + logTAB[i]["Time of Change"] + "," + logTAB[i]["Changed by User"] + "," + logTAB[i]["Furno"];
                }

                tabLOG.InsertTextLine(strLine, false);
                //				string uref = logTAB[i]["Flight"];
                tabLOG.SetFieldValues(i, "FURN", logTAB[i]["Flight"]);
            }
            else
            {
                ITable aftTable = myDB["AFT"];
                string strLine = "";
                if (m_AddonFields)
                    strLine = logTAB[i]["Flight"] + ", , , , ," + logTAB[i]["Location"] + "," + logTAB[i]["Tran"] + "," + CheckAndConvertDBString(logTAB[i]["Old Value"]) + "," + CheckAndConvertDBString(logTAB[i]["New Value"]) + "," + logTAB[i]["Time of Change"] + "," + logTAB[i]["Changed by User"] + "," + logTAB[i]["Urno"] + "," + logTAB[i]["Keyf"] + "," + logTAB[i]["STOX"];
                else
                {


                    // Building the Line for the Deleted Flight
                    string strKEYF = logTAB[i]["Keyf"];
                    string strAdid = "";	//Arrival or Departure Flag
                    string strFlno = "";	//Flight Number
                    if (strKEYF.Length == 18)
                    {
                        string strFlnu = strKEYF.Substring(0, 5);
                        strFlnu = strFlnu.TrimStart('0');
                        string strAlc3 = strKEYF.Substring(5, 3);
                        strAdid = strKEYF.Substring(17, 1);
                        //							
                        string strAlc2 = "";
                        ITable myTab = myDB["ALT"];
                        /*
                        for(int j = 0; j < myTab.Count; j++)
                        {
                            if(strAlc3 == myTab[j]["ICAO-Code"])
                            {
                                strAlc2 = myTab[j]["IATA-Code"];
                                break;
                            }
                        }
                        if(strAlc2.Length == 0)
                        {
                            if(strAlc3 != "###") // Doesn't have Flight number(Only Callsign is also not available)
                                strAlc2 = strAlc3;
                        }
                        */
                        if (strAlc3.Length == 2)
                            strFlno = strAlc3 + " " + strFlnu + " ";
                        else
                            strFlno = strAlc3 + strFlnu + " ";
                    }
                    else if (addTabInfo)
                    {
                        //string flightUrno = logTAB[i]["Keyf"];

                        if (aftTable == null)
                            return;

                        IRow[] aftRows = aftTable.RowsByIndexValue("URNO", flightUrno);
                        if (aftRows.Length > 0)
                        //if (aftTable.Count > 0)
                        {
                            //string strUrno = rows[0]["URNO"];
                            //strFlno = aftTable[0]["FLNO"];
                            //tabLOG.SetFieldValues(i, "UREF", strFlno);
                            //					tabLOG.SetFieldValues(i, "UREF", rows[0]["FLNO"]);
                            //					strFlno = logTAB[i]["Flight"];

                            strFlno = aftRows[0]["FLNO"];
                            strAdid = aftRows[0]["ADID"];
                        }
                    }

                    if (m_DeleteFlightsChecked == true && logTAB[i]["Tran"] == "D")
                    {

                        if (strAdid == "A") // Arrival Flight
                        {
                            strLine = strFlno + "," + logTAB[i]["STOX"] + ", ," + strAdid + ", ," + logTAB[i]["Location"] + "," + logTAB[i]["Tran"] + "," + CheckAndConvertDBString(logTAB[i]["Old Value"]) + "," + CheckAndConvertDBString(logTAB[i]["New Value"]) + "," + logTAB[i]["Time of Change"] + "," + logTAB[i]["Changed by User"] + "," + logTAB[i]["Flight"];
                        }
                        else if (strAdid == "D") //Departure Flight
                        {
                            strLine = strFlno + ", ," + logTAB[i]["STOX"] + "," + strAdid + ", ," + logTAB[i]["Location"] + "," + logTAB[i]["Tran"] + "," + CheckAndConvertDBString(logTAB[i]["Old Value"]) + "," + CheckAndConvertDBString(logTAB[i]["New Value"]) + "," + logTAB[i]["Time of Change"] + "," + logTAB[i]["Changed by User"] + "," + logTAB[i]["Flight"];
                        }
                        else // No information is available for the deleted Flight
                            strLine = strFlno + ", ," + logTAB[i]["STOX"] + "," + strAdid + ", ," + logTAB[i]["Location"] + "," + logTAB[i]["Tran"] + "," + CheckAndConvertDBString(logTAB[i]["Old Value"]) + "," + CheckAndConvertDBString(logTAB[i]["New Value"]) + "," + logTAB[i]["Time of Change"] + "," + logTAB[i]["Changed by User"] + "," + logTAB[i]["Flight"];
                    }
                    else
                    {
                        if (strAdid == "A")
                            strLine = strFlno + "," + logTAB[i]["STOX"] + ", ," + strAdid + ", ," + logTAB[i]["Location"] + "," + logTAB[i]["Tran"] + "," + CheckAndConvertDBString(logTAB[i]["Old Value"]) + "," + CheckAndConvertDBString(logTAB[i]["New Value"]) + "," + logTAB[i]["Time of Change"] + "," + logTAB[i]["Changed by User"] + "," + logTAB[i]["Flight"];
                        else
                            strLine = strFlno + ", ," + logTAB[i]["STOX"] + "," + strAdid + ", ," + logTAB[i]["Location"] + "," + logTAB[i]["Tran"] + "," + CheckAndConvertDBString(logTAB[i]["Old Value"]) + "," + CheckAndConvertDBString(logTAB[i]["New Value"]) + "," + logTAB[i]["Time of Change"] + "," + logTAB[i]["Changed by User"] + "," + logTAB[i]["Flight"];
                        // strLine = logTAB[i]["Flight"] + ", , , , ," + logTAB[i]["Location"] + "," + logTAB[i]["Tran"] + "," + CheckAndConvertDBString(logTAB[i]["Old Value"]) + "," + CheckAndConvertDBString(logTAB[i]["New Value"]) + "," + logTAB[i]["Time of Change"] + "," + logTAB[i]["Changed by User"] + "," + logTAB[i]["Furno"];
                    }
                }

                tabLOG.InsertTextLine(strLine, false);

                //search for the information of flights (loaded in "AFT") and fill the line
                //ITable aftTable = myDB["AFT"];
                if (aftTable == null)
                    return;



                //IRow [] rows = aftTable.RowsByIndexValue("URNO", tabLOG.GetFieldValue(i, "UREF"));
                IRow[] rows = aftTable.RowsByIndexValue("URNO", flightUrno);
                if (rows.Length > 0)
                {
                    // Display CallSign Value when the Flight Number is Empty.

                    if (rows[0]["FLNO"] != "") //nr
                        tabLOG.SetFieldValues(i, "UREF", rows[0]["FLNO"]);
                    else
                        tabLOG.SetFieldValues(i, "UREF", rows[0][1]); //CallSign

                    if (rows[0]["FTYP"].Equals("T") || rows[0]["FTYP"].Equals("G"))
                        tabLOG.SetFieldValues(i, "ADID", "T");
                    else
                        tabLOG.SetFieldValues(i, "ADID", rows[0]["ADID"]);

                    tabLOG.SetFieldValues(i, "FURN", rows[0]["URNO"]);
                    if (rows[0]["ADID"].Equals("A"))
                        tabLOG.SetFieldValues(i, "STOA", rows[0]["STOA"]);
                    else if (rows[0]["ADID"].Equals("D"))
                        tabLOG.SetFieldValues(i, "STOD", rows[0]["STOD"]);
                    else
                    {
                        tabLOG.SetFieldValues(i, "STOA", rows[0]["STOA"]);
                        tabLOG.SetFieldValues(i, "STOD", rows[0]["STOD"]);
                    }
                }
            }
        }

        private void ReadSelection()
        {
            if (m_FieldName.Equals(""))
                return;

            this.Cursor = Cursors.WaitCursor;
            //load selection
            //also possible to do a selection for all UREF-entries if (""), but not requested yet


            string[] strArrUREF;
            strArrUREF = myStrWhereFLNUIn.Split('#');
            List<string> lsAdditionalTables = null;

            myStrWhereUser = " AND USEC = '" + m_UserName1 + "'";

            if (m_DeleteFlightsChecked == false)
            {

                if (myStrWhereFLNUIn.Equals(""))
                {
                    this.Cursor = Cursors.Arrow;
                    return;
                }
                logTAB.Clear();
                //build the whole selection (TIME,UREF,TANA,FINA)
                for (int i = 0; i < strArrUREF.Length; i++)
                {
                    myStrWhereFLNUIn = strArrUREF[i];

                    //mySelection = "WHERE (TIME BETWEEN '20111116000000' AND '20111118235959')";
                    mySelection = myStrWhereTimeframe;
                    if (m_UserName1 != "ALL USER" && m_UserName1 != string.Empty)
                        mySelection += myStrWhereUser;
                    mySelection += myStrWhereALOCIn;
                    mySelection += myStrWhereFLNUIn;
                    mySelection += myStrSortOrder;
                    logTAB.Load(mySelection);
                }

                CtrlLog.GetInstance().LoadAFTRelatedLog(myStrWhereTimeframe, logTAB, out lsAdditionalTables);
                logTAB = CtrlLog.GetInstance().LoadOtherDescriptions(logTAB, "AFT");

                myDB.Unbind("L0G2");
                ITable logTAB2 = myDB.Bind("L0G2", "L0G2",
                CtrlLog.LOGTAB_LOGICAL_FIELDS,
                CtrlLog.LOGTAB_FIELD_LENGTHS,
                CtrlLog.LOGTAB_DB_FIELDS
                );
                CtrlLog.GetInstance().LoadCCARelatedLog(myStrWhereTimeframe, logTAB2, out lsAdditionalTables, myStrREFUList);
                for (int i = 0; i < logTAB2.Count; i++)
                {
                    IRow tempRow = logTAB2[i];
                    IRow rowNew = logTAB.CreateEmptyRow();
                    rowNew.SetFieldValues(tempRow.FieldValues());
                    logTAB.Add(rowNew);
                }

                //logTAB = CtrlLog.GetInstance().LoadOtherDescriptions(logTAB, "CCA");
            }



            this.Cursor = Cursors.Arrow;
            CtrlSYSTabData ctrlSysInfo = CtrlSYSTabData.GetInstance();

            //build table
            CtrlConfig ctrlConfig = CtrlConfig.GetInstance();
            ctrlConfig.LoadConfigInfo();
            LinkInfoToATAble aftLinkInfo = ctrlConfig.GetLinkInfoFor("AFT");
            List<string> lsTables = aftLinkInfo.GetLinkTableList();

            CtrlMapConfig mapConfig = new CtrlMapConfig();

            foreach (string tbName in lsTables)
            {
                LinkInfo lInfo = aftLinkInfo.GetLinkInfo(tbName);
                mapConfig.LoadAllSectionForLink(tbName, lInfo.ListColumnToCheck);
            }

            for (int i = 0; i < logTAB.Count; i++)
            {
                /*
                                string strFields = "PSTA,PSTD,FTYP";
                                string strFina = logTAB[i]["Location"];
                                if (strFields.IndexOf(strFina) == -1)
                                    continue;
                */

                BuildLine(i, lsAdditionalTables);

                string strADID = tabLOG.GetFieldValue(i, "ADID");
                string strTRAN = tabLOG.GetFieldValue(i, "TRAN");

                if (strADID.Equals("T"))
                {
                    if (strTRAN.Equals("I"))
                    {
                        tabLOG.SetLineColor(i, -1, 0xE5BD7F);
                    }
                    else if (strTRAN.Equals("U"))
                    {
                        tabLOG.SetLineColor(i, -1, 0xFFEBCC);
                    }
                    else if (strTRAN.Equals("D"))
                    {
                        tabLOG.SetLineColor(i, -1, 0x1062FF);//orange - RGB 0xFF6210, BGR 0xFF6210
                        tabLOG.SetFieldValues(i, "VALU", "FLIGHT HAS BEEN DELETED");
                    }
                }
                else
                {
                    if (strTRAN.Equals("I"))
                        tabLOG.SetLineColor(i, -1, UT.colLightGreen);
                    else if (strTRAN.Equals("U"))
                        tabLOG.SetLineColor(i, -1, UT.colLightYellow);
                    else if (strTRAN.Equals("D"))
                    {
                        tabLOG.SetLineColor(i, -1, UT.colLightOrange);
                        tabLOG.SetFieldValues(i, "VALU", "FLIGHT HAS BEEN DELETED");
                    }
                }

                //ITable sysTable = myDB["SYS"];
                //if (sysTable == null)
                //    return;

                //IRow [] rows = sysTable.RowsByIndexValue("FINA", tabLOG.GetFieldValue(i, "FINA"));
                //if(rows.Length > 0)
                //{
                //    tabLOG.SetFieldValues(i, "ADDI", rows[0]["ADDI"]);
                //}
                string linkTable = "";
                string linkField = "";

                string strLocation = logTAB[i]["Location"];

                string mapName = mapConfig.GetMapName(logTAB[i]["TANA"], strLocation, out linkTable, out linkField);


                if (!string.IsNullOrEmpty(mapName))
                {
                    tabLOG.SetFieldValues(i, "ADDI", ctrlSysInfo.GetFieldDesc(linkTable, linkField));
                }
                else
                {
                    tabLOG.SetFieldValues(i, "ADDI", ctrlSysInfo.GetFieldDesc(logTAB[i]["TANA"], logTAB[i]["Location"]));
                }
                //tabLOG.SetFieldValues(i, "ADDI", ctrlSysInfo.GetFieldDesc(logTAB[i]["TANA"], logTAB[i]["Location"]));


            }
        }

        private void ReadSelectionCCATAB()
        {
            if (m_CCA_Fields.Equals(""))
                return;
            //////////////////////////////////////////////////

            string olTmpWhere = "WHERE TANA = 'CCA' and FINA  = 'FLNU' and VALU IN (" + myStrREFUList + ")";

            ITable logTable = myDB["L0G"];
            //remove old LoginTable
            myDB.Unbind("L0G");
            //bind new LoginTable
            //			logTable = myDB.Bind("L0G", m_CcaLogtab, "Flight,Location,Tran,Old Value,New Value,Time of Change,Changed by User,Urno,Keyf,STOX", "10,10,10,10,10,14,10,10,10,14", "UREF,FINA,TRAN,h_tab_old_value(tana,fina,uref,time),VALU,TIME,USEC,URNO,KEYF,STOX");
            logTable = myDB.Bind("L0G", m_CcaLogtab, "FINA,TRAN,OVAL,VALU,TIME,USEC,URNO,KEYF,STOX,UREF", "10,10,10,10,14,10,10,10,14,10", "FINA,TRAN,OVAL,VALU,TIME,USEC,URNO,KEYF,STOX,UREF");


            ITable logTableFlno = myDB["L0G2"];
            myDB.Unbind("L0G2");
            //remove old LoginTable
            logTableFlno = myDB.Bind("L0G2", "H00", "VALU,TIME", "10,14", "VALU,TIME");

            string myStrREFUListFlno = myStrREFUList;

            myStrREFUListFlno = myStrREFUListFlno.Replace("'", " ");

            string olTmpWhereFlno = "WHERE TANA = 'AFT' and FINA  = 'FLNO' and UREF In (" + myStrREFUListFlno + ") ORDER by TIME ASC";
            logTableFlno.Load(olTmpWhereFlno);


            logTable.Load(olTmpWhere);
            string strTMPUREF = "";

            string[] strArrUREFTmp;

            for (int i = 0; i < logTable.Count; i++)
            {
                if (strTMPUREF.Length > 0)
                    strTMPUREF += "#";

                if (myStrCCAUrnoFlnuMap.Length > 0)
                    myStrCCAUrnoFlnuMap += "#";

                myStrCCAUrnoFlnuMap += logTable[i]["UREF"] + "," + logTable[i]["VALU"];

                strTMPUREF += logTable[i]["UREF"];

            }

            strArrUREFTmp = strTMPUREF.Split('#');



            //////////////////////////////////////////////////

            //remove old LoginTable
            myDB.Unbind("L0G");
            //bind new LoginTable
            //			logTable = myDB.Bind("L0G", m_CcaLogtab, "Flight,Location,Tran,Old Value,New Value,Time of Change,Changed by User,Urno,Keyf,STOX", "10,10,10,10,10,14,10,10,10,14", "UREF,FINA,TRAN,h_tab_old_value(tana,fina,uref,time),VALU,TIME,USEC,URNO,KEYF,STOX");
            logTable = myDB.Bind("L0G", m_CcaLogtab, "Flight,Location,Tran,Old Value,New Value,Time of Change,Changed by User,Urno,Keyf,STOX", "10,10,10,10,10,14,10,10,10,14", "UREF,FINA,TRAN,OVAL,VALU,TIME,USEC,URNO,KEYF,STOX");
            //load selection
            this.Cursor = Cursors.WaitCursor;

            string[] strArrUREF;
            strArrUREF = myStrWhereFLNUIn.Split('#');
            if (myStrWhereFLNUIn.Equals("") && mySelectionLOGTABKeyf.Equals(""))
            {
                this.Cursor = Cursors.Arrow;
                return;
            }
            else
            {
                if (this.myboolDoingLogtab == true)
                {
                    string Selection = myStrWhereTimeframe + mySelectionLOGTABKeyf;
                    //logTable.Load(Selection);
                }
            }

            //build the whole selection (TIME,UREF,TANA,FINA)
            for (int i = 0; i < strArrUREF.Length; i++)
            {
                myStrWhereFLNUIn = strArrUREF[i];

                for (int j = strArrUREFTmp.Length - 1; j >= 0; j--)
                {
                    strTMPUREF = strArrUREFTmp[j];

                    if (myStrWhereFLNUIn.IndexOf(strTMPUREF) >= 0)
                        strArrUREFTmp[j] = "";
                }


                mySelection = myStrWhereTimeframe;
                string myStrWhereUser = " AND USEC = '" + m_UserName1 + "'";
                if (m_UserName1 != "ALL USER" && m_UserName1 != string.Empty)
                    mySelection += myStrWhereUser;


                if (myStrCcaWhereALOCIn.Equals(""))
                    mySelection += myStrWhereALOCIn;
                else
                    mySelection += myStrCcaWhereALOCIn;

                //				mySelection += myStrWhereTimeframe;
                mySelection += myStrWhereFLNUIn;
                mySelection += myStrSortOrder;
                //				MessageBox.Show(this, mySelection, "Updating LOGATB", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                if (!myStrWhereFLNUIn.Equals(""))
                    logTable.Load(mySelection);
            }

            // Reading deleted CCA records
            for (int j = strArrUREFTmp.Length - 1; j >= 0; j--)
            {
                strTMPUREF = strArrUREFTmp[j];

                if (strTMPUREF.Length > 0)
                {
                    myStrWhereFLNUIn = " AND  UREF = " + strTMPUREF;

                    mySelection = myStrWhereTimeframe;
                    string myStrWhereUser = " AND USEC = '" + m_UserName1 + "'";
                    if (m_UserName1 != "ALL USER" && m_UserName1 != string.Empty)
                        mySelection += myStrWhereUser;

                    string test = myStrWhereALOCIn;

                    test = test.Replace("'CKIC'", "'CKIC',' '");

                    if (myStrCcaWhereALOCIn.Equals(""))
                        mySelection += test;
                    else
                        mySelection += myStrCcaWhereALOCIn;

                    //				mySelection += myStrWhereTimeframe;
                    mySelection += myStrWhereFLNUIn;
                    mySelection += myStrSortOrder;
                    //				MessageBox.Show(this, mySelection, "Updating LOGATB", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    if (!myStrWhereFLNUIn.Equals(""))
                        logTable.Load(mySelection);  
                }
            }

            //////////////////////////////////////////


            this.Cursor = Cursors.Arrow;
            string strTmpUref = "";


            //build table
            for (int i = 0; i < logTable.Count; i++)
            {
                int j = tabLOG.GetLineCount();
                string strLine = "";
                if (m_AddonFields)
                    strLine = logTable[i]["Flight"] + ", , , , ," + logTable[i]["Location"] + "," + logTable[i]["Tran"] + "," + CheckAndConvertDBString(logTable[i]["Old Value"]) + "," + CheckAndConvertDBString(logTable[i]["New Value"]) + "," + logTable[i]["Time of Change"] + "," + logTable[i]["Changed by User"] + "," + logTable[i]["Urno"] + "," + logTable[i]["Keyf"] + "," + logTable[i]["STOX"];
                else
                    strLine = logTable[i]["Flight"] + ", , , , ," + logTable[i]["Location"] + "," + logTable[i]["Tran"] + "," + CheckAndConvertDBString(logTable[i]["Old Value"]) + "," + CheckAndConvertDBString(logTable[i]["New Value"]) + "," + logTable[i]["Time of Change"] + "," + logTable[i]["Changed by User"] + "," + logTable[i]["Furno"];

                tabLOG.InsertTextLine(strLine, false);

                //search for the information of flights (loaded in "AFT") and fill the line
                ITable aftTable = myDB["AFT"];
                ITable ccaTable = myDB["CCA"];
                if (aftTable == null || ccaTable == null)
                    return;

                strTmpUref = tabLOG.GetFieldValue(j, "UREF");

                IRow[] CcaRows = ccaTable.RowsByIndexValue("URNO", tabLOG.GetFieldValue(j, "UREF"));
                string strCcaFlno = "";
                if (CcaRows.Length > 0)
                {
                    DateTime olTime;
                    DateTime olTimeCca;
                    for (int l = logTableFlno.Count - 1; l >= 0; l--)
                    {
                        olTime = UT.CedaTimeToDateTime(logTableFlno[l]["TIME"]);
                        olTimeCca = UT.CedaTimeToDateTime(logTable[i]["Time of Change"]);

                        if (olTime < olTimeCca)
                        {
                            strCcaFlno = logTableFlno[l]["VALU"];
                            break;
                        }
                    }

                    //FLNU in CCA-record -> record of URNO in AFT
                    IRow[] AftRows = aftTable.RowsByIndexValue("URNO", CcaRows[0]["FLNU"]);
                    if (AftRows.Length > 0)
                    {
                        if (strCcaFlno != "")
                            tabLOG.SetFieldValues(j, "UREF", strCcaFlno);
                        else
                            tabLOG.SetFieldValues(j, "UREF", AftRows[0]["FLNO"]);

                        tabLOG.SetFieldValues(j, "ADID", AftRows[0]["ADID"]);
                        tabLOG.SetFieldValues(j, "FURN", AftRows[0]["URNO"]);
                        if (AftRows[0]["ADID"].Equals("A"))
                            tabLOG.SetFieldValues(j, "STOA", AftRows[0]["STOA"]);
                        else if (AftRows[0]["ADID"].Equals("D"))
                            tabLOG.SetFieldValues(j, "STOD", AftRows[0]["STOD"]);
                        else
                        {
                            tabLOG.SetFieldValues(j, "STOA", AftRows[0]["STOA"]);
                            tabLOG.SetFieldValues(j, "STOD", AftRows[0]["STOD"]);
                        }
                    }
                    else
                    {
                        string strFLNO = CcaRows[0]["FLNO"];
                        string strFlno = BuildFlightnumber(strFLNO);

                        if (strCcaFlno != "")
                            strFlno = strCcaFlno;

                        tabLOG.SetFieldValues(j, "UREF", strFlno);
                        tabLOG.SetFieldValues(j, "ADID", "D");
                        tabLOG.SetFieldValues(j, "STOD", CcaRows[0]["STOD"]);
                        tabLOG.SetFieldValues(j, "FURN", CcaRows[0]["FLNU"]);
                    }
                }
                else
                {
                    /*
                    string strKEYF = logTable[i]["Keyf"];
                    string strFlno = "";
                    string strDay = "";
                    string strAdid = "";
                    tabLOG.SetFieldValues(i, "ADID", "D");
						
                    tabLOG.SetFieldValues(i, "STOD", logTable[j]["STOX"]);
                    if (strKEYF.Length == 18)
                    {
                        string strFlnu = strKEYF.Substring(0,5);
                        string strAlc3 = strKEYF.Substring(5,3);
                        string strFlns = strKEYF.Substring(8,1);
                        strDay  = strKEYF.Substring(9,8);
                        strAdid = strKEYF.Substring(17,1);

                        strFlno = strAlc3 + " " + strFlnu + " ";
                        if (!strFlns.Equals("#"))
                            strFlno += strFlns;

                        tabLOG.SetFieldValues(j, "UREF", strFlno);
                        tabLOG.SetFieldValues(j, "ADID", "D");
						
                        tabLOG.SetFieldValues(j, "STOD", logTable[j]["STOX"]);
                        //tabLOG.SetFieldValues(j, "STOD", CcaRows[0]["STOD"]);
                        //tabLOG.SetFieldValues(j, "FURN", CcaRows[0]["FLNU"]);
                    }
                    */
                }

                string[] strMap;
                string[] strMap2;
                string strFlnu = "";

                strMap = myStrCCAUrnoFlnuMap.Split('#');

                string strTmpUrefFromMap = "";

                for (int k = 0; k < strMap.Length; k++)
                {
                    strMap2 = strMap[k].Split(',');
                    if (strMap2.Length == 2)
                    {
                        strTmpUrefFromMap = strMap2[0];

                        if (strTmpUref == strTmpUrefFromMap)
                            strFlnu = strMap2[1];
                    }
                }

                IRow[] AftRows2 = aftTable.RowsByIndexValue("URNO", strFlnu);
                if (AftRows2.Length > 0)
                {
                    if (strCcaFlno != "")
                        tabLOG.SetFieldValues(j, "UREF", strCcaFlno);
                    else
                        tabLOG.SetFieldValues(j, "UREF", AftRows2[0]["FLNO"]);
                    tabLOG.SetFieldValues(j, "ADID", AftRows2[0]["ADID"]);
                    tabLOG.SetFieldValues(j, "FURN", AftRows2[0]["URNO"]);
                    if (AftRows2[0]["ADID"].Equals("A"))
                        tabLOG.SetFieldValues(j, "STOA", AftRows2[0]["STOA"]);
                    else if (AftRows2[0]["ADID"].Equals("D"))
                        tabLOG.SetFieldValues(j, "STOD", AftRows2[0]["STOD"]);
                    else
                    {
                        tabLOG.SetFieldValues(j, "STOA", AftRows2[0]["STOA"]);
                        tabLOG.SetFieldValues(j, "STOD", AftRows2[0]["STOD"]);
                    }
                }



                string strTRAN = tabLOG.GetFieldValue(j, "TRAN");
                if (strTRAN.Equals("I"))
                    tabLOG.SetLineColor(j, -1, UT.colLightGreen);
                else if (strTRAN.Equals("U"))
                    tabLOG.SetLineColor(j, -1, UT.colLightYellow);
                else if (strTRAN.Equals("D"))
                {
                    tabLOG.SetLineColor(j, -1, UT.colLightOrange);
                    tabLOG.SetFieldValues(j, "VALU", "CHECKIN HAS BEEN DELETED");
                }

                ITable sysTable = myDB["SYS"];
                if (sysTable == null)
                    return;

                IRow[] rows = sysTable.RowsByIndexValue("FINA", tabLOG.GetFieldValue(j, "FINA"));
                if (rows.Length > 0)
                {
                    string strADDI = rows[0]["ADDI"];
                    tabLOG.SetFieldValues(j, "ADDI", rows[0]["ADDI"]);
                }
            }
            tabLOG.Refresh();
        }

        private string BuildFlightnumber(string strFLNO)
        {
            if (strFLNO.Equals(""))
                return "";

            string strFlno = "";
            string strFlnuExt = "";
            string strTmpAlc = strFLNO.Substring(0, 3);
            string strTmp = strFLNO.Substring(3, strFLNO.Length - 3);
            string strTmpFlnu = "";
            string strTmpFlns = "";

            string[] strArr = strTmp.Split(' ');
            if (strArr.Length > 1)
            {
                strTmpFlnu = strArr[0];
                strTmpFlns = strArr[strArr.Length - 1];
            }
            else
            {
                if (strTmp.Length == 6)
                {
                    strTmpFlnu = strTmp.Substring(0, 5);
                    strTmpFlns = strTmp.Substring(5, 1);
                }
                else
                {
                    strTmpFlnu = strFLNO.Substring(3, strFLNO.Length - 3);
                    strTmpFlns = "";
                }
            }

            if (strTmpAlc.EndsWith(" "))
                strTmpAlc = strTmpAlc.Substring(0, 2);

            if (strTmpFlnu.Length < 5)
            {
                int ilPos = 5 - strTmpFlnu.Length;
                for (int i = 0; i < ilPos; i++)
                {
                    strFlnuExt += "0";
                }

                strTmpFlnu = strFlnuExt + strTmpFlnu;

            }

            if (strTmpAlc.Length == 2)
            {
                ITable AltTab = myDB["ALT"];
                IRow[] rows1 = AltTab.RowsByIndexValue("ALC2", strTmpAlc);
                if (rows1.Length == 1)
                    strTmpAlc = rows1[0]["ICAO-Code"];
            }

            strFlno = strTmpAlc + " " + strTmpFlnu + " " + strTmpFlns;
            return strFlno;
        }

        private void InformListener()
        {
            tabLOG.IndexCreate("FINA", 5);
            tabLOG.SetInternalLineBuffer(true);

            // calculate number of changes
            string sumChanges = tabLOG.GetLineCount().ToString();
            lblLocations.Text = "Loaded Changes: " + sumChanges + "  (for " + m_FlightCount + " Flights) / " + m_ActiveField;

            // inform Listener, send SBC
            if (m_Listener.Length != 0)
            {
                string[] strFields;
                int i = 0;
                //Pst
                int myCount = 0;
                string fields = m_PST_Fields.Substring(0, m_PST_Fields.Length - 1);
                strFields = fields.Split(',');
                for (i = 0; i < strFields.Length; i++)
                {
                    string strRet = "";
                    string tmp = strFields[i].Replace("'", "");
                    strRet = tabLOG.GetLinesByIndexValue("FINA", tmp, 0);
                    if (strRet != "")
                    {
                        myCount += Convert.ToInt32(strRet);
                    }
                }
                string sumPST = myCount.ToString();

                //Gatt
                myCount = 0;
                fields = m_GAT_Fields.Substring(0, m_GAT_Fields.Length - 1);
                strFields = fields.Split(',');
                for (i = 0; i < strFields.Length; i++)
                {
                    string strRet = "";
                    string tmp = strFields[i].Replace("'", "");
                    strRet = tabLOG.GetLinesByIndexValue("FINA", tmp, 0);
                    if (strRet != "")
                    {
                        myCount += Convert.ToInt32(strRet);
                    }
                }
                string sumGAT = myCount.ToString();

                //Blt
                myCount = 0;
                fields = m_BLT_Fields.Substring(0, m_BLT_Fields.Length - 1);
                strFields = fields.Split(',');
                for (i = 0; i < strFields.Length; i++)
                {
                    string strRet = "";
                    string tmp = strFields[i].Replace("'", "");
                    strRet = tabLOG.GetLinesByIndexValue("FINA", tmp, 0);
                    if (strRet != "")
                    {
                        myCount += Convert.ToInt32(strRet);
                    }
                }
                string sumBLT = myCount.ToString();

                //WRO
                myCount = 0;
                fields = m_WRO_Fields.Substring(0, m_WRO_Fields.Length - 1);
                strFields = fields.Split(',');
                for (i = 0; i < strFields.Length; i++)
                {
                    string strRet = "";
                    string tmp = strFields[i].Replace("'", "");
                    strRet = tabLOG.GetLinesByIndexValue("FINA", tmp, 0);
                    if (strRet != "")
                    {
                        myCount += Convert.ToInt32(strRet);
                    }
                }
                string sumWRO = myCount.ToString();

                //Runway
                myCount = 0;
                fields = m_RWY_Fields.Substring(0, m_RWY_Fields.Length - 1);
                strFields = fields.Split(',');
                for (i = 0; i < strFields.Length; i++)
                {
                    string strRet = "";
                    string tmp = strFields[i].Replace("'", "");
                    strRet = tabLOG.GetLinesByIndexValue("FINA", tmp, 0);
                    if (strRet != "")
                    {
                        myCount += Convert.ToInt32(strRet);
                    }
                }
                string sumRWY = myCount.ToString();

                //Cca
                myCount = 0;
                fields = m_CCA_Fields.Substring(0, m_CCA_Fields.Length);
                strFields = fields.Split(',');
                for (i = 0; i < strFields.Length; i++)
                {
                    string strRet = "";
                    string tmp = strFields[i].Replace("'", "");
                    strRet = tabLOG.GetLinesByIndexValue("FINA", tmp, 0);
                    if (strRet != "")
                    {
                        myCount += Convert.ToInt32(strRet);
                    }
                }
                string sumCCA = myCount.ToString();
                string strListener = sumChanges + "," + sumPST + "," + sumGAT + "," + sumBLT + "," + sumWRO + "," + sumCCA + "," + sumRWY;
                //MessageBox.Show(this, strListener);


                IUfisComWriter myUfisCom = UT.GetUfisCom();
                int ilRet = myUfisCom.CallServer("SBC", "BCLOCC", "LOC_CHANGES", strListener, m_Listener, "230");
                if (ilRet != 0)
                {
                    string strERR = myUfisCom.LastErrorMessage;
                    MessageBox.Show(this, strERR);
                }
            }
        }


        private void FillSelection()
        {
            //clear table
            tabLOG.ResetContent();

            //part of selection for TIME
            FillTIME();

            if (myboolDoingLogtab)
                DoDeletedFlights();

            if (cbFlnu.BackColor == Color.LightGreen && !txtURNOValue.Equals(""))
            {
                string strUrnoSelection = "WHERE URNO = " + txtURNOValue.Text;
                if (DataForm.LoadAFTData(strUrnoSelection) == false)
                {
                    return;
                }
                m_FieldName = "ALL FIELDS";
                m_ActiveField = "Active Field: " + m_FieldName;
                myNeedCCA = ",'CCA'";
                //				myStrWhereALOCIn = "WHERE FINA <> ' '";
                myboolDoingLogtab = false;

            }

            //part of selection for UREF

            if (m_ViewMode == "FIELD")
            {
                FillUREF(m_TableName, "URNO");
                if (m_TableName.Equals("CCA"))
                    ReadSelectionCCATAB();
                else
                {
                    ReadSelection();
                    if (myNeedCCA.Length != 0)
                    {
                        FillUREF("CCA", "URNO");
                        ReadSelectionCCATAB();
                    }
                }
            }
            else if (m_ViewMode == "LOCATIONS")
            {
                if (myboolDoingLogtab)
                    FillUREF("LOG", "Flight");
                else
                    FillUREF(m_TableName, "URNO");

                ReadSelection();
                if (myNeedCCA.Length != 0)
                {
                    //if (myboolDoingLogtab)
                    {
                        DataForm.LoadCCAData(myStrCcaWhereFLNUIn);
                    }

                    FillUREF("CCA", "URNO");
                    ReadSelectionCCATAB();
                }
            }
            //show complete table
            //tabLOG.Refresh();

            //sort table by FLNO
            //if (m_ViewMode == "LOCATIONS")
            {
                //tabLOG.SortOrderASC = false;
                tabLOG_SortColumn(-1, 0);
                tabLOG_SortColumn(-1, 9);
            }

            // after selection has been done all data is init in utc-times
            //			myIsTimeInUtc = true;

            //Changed from FALSE to TRUE in order to correct the UTC/Local time Conversion
            //time values stored in database is in UTC
            myIsTimeInUtc = true;
            HandleUTC();
            myIsInit = true;

            //nr			tabLOG.AutoSizeColumns();

            //show complete table
            tabLOG.Refresh();

            // inform Listener, send SBC
            InformListener();
        }

        private void btnApply_Click(object sender, System.EventArgs e)
        {
            //CtrlLog.GetInstance().Test(); return;
            if ("Flight Data" == m_SelectedTabPage)
            {
                FillSelection();
            }
            else if ("Common CKI" == m_SelectedTabPage)
            {
                FillTIME();
                FillCommonCKISelection(myStrWhereTimeframe);
            }
            else
            {
                //part of selection for TIME
                FillTIME();

                // Fill the Changes for the Selected Basic Data Table
                FillBasicDataSelection(myStrWhereTimeframe);
            }
        }

        private void btnPrint_Click(object sender, System.EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            string strTimeframe = " - Timeframe : " + dateTimePickerFrom.Value.ToShortDateString() + " / " + dateTimePickerFrom.Value.ToShortTimeString() + " - " + dateTimePickerTo.Value.ToShortDateString() + " / " + dateTimePickerTo.Value.ToShortTimeString();
            string strUtcLocal = "LOCAL";
            if (myIsTimeInUtc == true)
                strUtcLocal = "UTC";
            strUtcLocal += strTimeframe;

            string strTitel = "Data Changes: " + m_FieldName;
            if (m_ViewMode == "LOCATIONS")
                strTitel = "Location Changes (" + lblLocations.Text + ")";

            ActiveReport1 rpt = new ActiveReport1(tabLOG, strUtcLocal, strTitel, m_FieldType, UT.UserName);
            frmPrintPreview preview = new frmPrintPreview(rpt);
            preview.Show();
            this.Cursor = Cursors.Arrow;
        }

        private void frmMain_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {

            DataForm.Close();
            //Store current geometry in Registry
            WriteRegistry();

            //send close (-1) to m_Listener
            if (m_Listener.Length != 0)
            {
                IUfisComWriter myUfisCom = UT.GetUfisCom();
                int ilRet = myUfisCom.CallServer("SBC", "BCLOCC", "LOC_CHANGES", "-1", m_Listener, "230");
                if (ilRet != 0)
                {
                    string strERR = myUfisCom.LastErrorMessage;
                    MessageBox.Show(this, strERR);
                }
            }
        }

        private void lblChanges_Click(object sender, System.EventArgs e)
        {

        }

        private void numericUpDown1_ValueChanged(object sender, System.EventArgs e)
        {

        }

        private void tabLOG_RowSelectionChanged(object sender, AxTABLib._DTABEvents_RowSelectionChangedEvent e)
        {
            /*
            int txtColor = -1;
            int bkColor = -1;
            tabLOG.GetLineColor(iLineNo, ref txtColor, ref bkColor);
            tabLOG.SetLineColor(iLineNo, UT.colWhite, UT.colBlue);
            */
        }

        private void tabLOG_HitKeyOnLine(object sender, AxTABLib._DTABEvents_HitKeyOnLineEvent e)
        {
            /*
            if(e.key == Convert.ToInt16( Keys.Control))
            {
            }
            */
        }

        public static DateTime GetUTC()
        {
            DateTime datRet = DateTime.Now;
            if (UT.UTCOffset == 0)
            {
                datRet = DateTime.Now;
            }
            else
            {
                datRet = DateTime.UtcNow;
            }
            return datRet;
        }

        private void txtURNOValue_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (!System.Char.IsNumber(e.KeyChar))
            {
                if (e.KeyChar == 8)
                    e.Handled = false;
                else
                    e.Handled = true;
            }
        }

        private void cbFlnu_CheckedChanged(object sender, System.EventArgs e)
        {
            /*			if(cbFlnu.Checked == true)
                        {
                            m_FieldName = "ALL FIELDS";
                            m_ActiveField = "Active Field: " + m_FieldName;
                            myNeedCCA = ",'CCA'";
                            myboolDoingLogtab = false;
                            myStrWhereALOCIn = " AND TANA IN ('" + m_TableName + "'" + myNeedCCA + ")";

            //				cbFlnu.Checked = true;	
                            cbFlnu.BackColor = Color.LightGreen;
                            cbFlnu.Refresh();
                            btnApply_Click(sender, e);
                            //				cbView.Enabled = false;
                        }
                        else
                        {
                            cbFlnu.BackColor = Color.Transparent;
            //				cbFlnu.Checked = true;
            //				cbFlnu_CheckedChanged(sender, e);
            //				cbView.Enabled = true;
                        }

                        cbFlnu.Refresh();
            //			cbView.Refresh();
            */
        }

        private void cbFlnu_Click(object sender, System.EventArgs e)
        {
            m_FieldName = "ALL FIELDS";
            m_ActiveField = "Active Field: " + m_FieldName;
            myNeedCCA = ",'CCA'";
            myboolDoingLogtab = false;

            if (m_FieldName.Equals("ALL FIELDS"))
            {
                string strFina = "";
                if (myAllFieldsAFT.Equals("") && myAllFieldsCCA.Equals(""))
                {
                    strFina = "";
                    myStrWhereALOCIn = " AND TANA IN ('" + m_TableName + "'" + myNeedCCA + ")";
                }
                else
                {
                    if (myAllFieldsAFT.Equals(""))
                        myAllFieldsAFT = "' '";

                    if (myAllFieldsCCA.Equals(""))
                        myAllFieldsCCA = "' '";

                    strFina = " AND (FINA IN (" + myAllFieldsAFT + ")";
                    myStrWhereALOCIn = " AND (TANA = " + "'" + m_TableName + "'" + strFina + ")";

                    myStrWhereALOCIn += " OR ";

                    strFina = " AND FINA IN (" + myAllFieldsCCA + ")";
                    myStrWhereALOCIn += " ( TANA = 'CCA'" + strFina + "))";
                }
            }
            else
            {
                myStrWhereALOCIn = " AND TANA IN ('" + m_TableName + "'" + myNeedCCA + ")";
            }

            cbFlnu.BackColor = Color.LightGreen;
            cbFlnu.Refresh();
            btnApply_Click(sender, e);
        }
        private void CreateCommonCKIDatatable()
        {

            tabLOG.HeaderString = "CKI,Scheduled Open,Scheduled Closed ,Field,Code,Tran,Old Value,New Value,Time of Change,Changed by User,URNO";
            tabLOG.LogicalFieldList = "CKI,CKBS,CKES,ADDI,FINA,TRAN,OVAL,VALU,TIME,USEC,URNO";
            tabLOG.HeaderLengthString = "60,170,170,200,50,40,170,170,170,120,50";
            tabLOG.ColumnAlignmentString = "L,L,L,L,L,L,L,L,L,L,L,L";

            int idx = 0;
            idx = UT.GetItemNo(tabLOG.LogicalFieldList, "TIME");
            tabLOG.DateTimeSetColumnFormat(idx, "YYYYMMDDhhmmss", "DD'.'MM'.'YYYY' / 'hh':'mm':'ss");

            idx = UT.GetItemNo(tabLOG.LogicalFieldList, "CKBS");
            tabLOG.DateTimeSetColumnFormat(idx, "YYYYMMDDhhmmss", "DD'.'MM'.'YYYY' / 'hh':'mm");

            idx = UT.GetItemNo(tabLOG.LogicalFieldList, "CKES");
            tabLOG.DateTimeSetColumnFormat(idx, "YYYYMMDDhhmmss", "DD'.'MM'.'YYYY' / 'hh':'mm");

            //idx = UT.GetItemNo(tabLOG.LogicalFieldList, "VALU");
            //tabLOG.DateTimeSetColumnFormat(idx, "YYYYMMDDhhmmss", "DD'.'MM'.'YYYY' / 'hh':'mm");

            //idx = UT.GetItemNo(tabLOG.LogicalFieldList, "OVAL");
            // tabLOG.DateTimeSetColumnFormat(idx, "YYYYMMDDhhmmss", "DD'.'MM'.'YYYY' / 'hh':'mm");



            string strColor = UT.colBlue + "," + UT.colBlue;
            tabLOG.CursorDecoration(tabLOG.LogicalFieldList, "B,T", "2,2", strColor);
            tabLOG.SetInternalLineBuffer(true);
            tabLOG.Refresh();


        }
        /// <summary>
        /// Create/Updataes Log Table for Basic Data.
        /// </summary>
        /// <param name="strIdentifierCodes">List of Identifier codes</param>
        private void CreateBasicDataTable()
        {
            string[] arrIdentCodes = m_strIdentifierCodes.Split(',');
            string strFieldNames = "";
            string strHeaderLength = "";
            string strColumnWidth = "";
            string strColumnAlignment = "";

            bool bFound = false;
            string strTmp = "";
            for (int i = 0; i < arrIdentCodes.Length; i++)
            {
                for (int j = 0; j < m_SysTab.Count; j++)
                {
                    // Check for the Field Code and get the Field Code
                    if (arrIdentCodes[i] == m_SysTab[j]["FINA"])
                    {
                        strTmp = m_SysTab[j]["ADDI"]; //First Character with UpperCase
                        if (strTmp.Length > 1)
                        {
                            strTmp = strTmp.Substring(0, 1).ToUpper() + strTmp.Substring(1);
                        }
                        strFieldNames += strTmp + ",";
                        strHeaderLength += "120" + ",";
                        strColumnWidth += m_SysTab[j]["ADDI"].Length + ",";
                        strColumnAlignment += "L" + ",";
                        bFound = true;
                        break;
                    }
                }
                if (bFound == false) //If the Table codes in the file are wrong
                {
                    MessageBox.Show("Invalid Identifier Codes in the INI file ", "Daga Changes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                bFound = false;
            }

            logTAB.Clear();
            logTAB.TimeFields = "TIME";
            tabLOG.ResetText();
            tabLOG.ResetContent();
            tabLOG.DateTimeResetColumn(1);
            tabLOG.DateTimeResetColumn(2);

            //tabLOG.HeaderString = "				0    , 1 , 2  , 3		,   4	  ,  5			 ,	6            ,7   ,	8 
            tabLOG.HeaderString = strFieldNames + "Field,Code,Tran,Old Value,New Value,Time of Change,Changed by User,Urno";
            tabLOG.LogicalFieldList = m_strIdentifierCodes + ",ADDI,FINA,TRAN,OVAL,VALU,TIME,USEC,URNO";//tabLOG.HeaderString;
            tabLOG.HeaderLengthString = strHeaderLength + "200,50,40,150,150,170,120,150";
            tabLOG.ColumnWidthString = strColumnWidth + "10,6,6,14,14,14,10,14";
            tabLOG.ColumnAlignmentString = strColumnAlignment + "L,L,L,L,L,L,L,L";

            int idx = UT.GetItemNo(tabLOG.LogicalFieldList, "TIME");
            tabLOG.DateTimeSetColumnFormat(idx, "YYYYMMDDhhmmss", "DD'.'MM'.'YYYY' / 'hh':'mm':'ss");



            string strColor = UT.colBlue + "," + UT.colBlue;
            tabLOG.CursorDecoration(tabLOG.LogicalFieldList, "B,T", "2,2", strColor);
            tabLOG.SetInternalLineBuffer(true);
            tabLOG.Refresh();
        }
        /// <summary>
        /// Get the field Names by taking the Input Field code
        /// </summary>
        /// <param name="strFieldCode"></param>
        /// <returns></returns>
        private string GetFieldNameUsingCode(string strFieldCode)
        {
            string strFieldName = "";
            for (int j = 0; j < m_SysTab.Count; j++)
            {
                // Check for the Field Code and get the Field Code
                if (strFieldCode == m_SysTab[j]["FINA"])
                {
                    strFieldName = m_SysTab[j]["ADDI"];
                    break;
                }
            }
            if (strFieldName.Length > 1) //First Character with UpperCase
            {
                strFieldName = strFieldName.Substring(0, 1).ToUpper() + strFieldName.Substring(1);
            }


            return strFieldName;
        }
        /// <summary>
        /// Alter/Create the Log/Main Table for the Flight Data.
        /// </summary>
        /// <returns></returns>
        private void AlterLogTabForFlightData()
        {
            logTAB.TimeFields = "TIME,STOA,STOD,STOX";
            if (m_FieldType.Equals("D"))
                logTAB.TimeFields = "TIME,STOA,STOD,VALU,OVAL,STOX";
            logTAB.TimeFieldsCurrentlyInUtc = true;
            logTAB.TimeFieldsInitiallyInUtc = true;


            tabLOG.ResetContent();
            //tabLOG.HeaderString = "  0 , 1 , 2 , 3 ,   4  ,  5,  6,  7    ,    8    ,     9        ,        10        ,11  ,12  , 13
            if (m_AddonFields)
            {
                tabLOG.HeaderString = "Flight,STA,STD,A/D,Field,Code,Tran,Old Value,New Value,Time of Change,Changed by User,Urno,Keyf,STOX";
                tabLOG.LogicalFieldList = "UREF,STOA,STOD,ADID,ADDI,FINA,TRAN,OVAL,VALU,TIME,USEC,URNO,KEYF,STOX";//tabLOG.HeaderString;
                tabLOG.HeaderLengthString = "90,150,150,40,200,50,40,150,150,170,120,150,150,150";
                tabLOG.ColumnWidthString = "10,14,14,6,6,10,6,14,14,14,10,14,14,14";
                tabLOG.ColumnAlignmentString = "L,L,L,C,L,L,C,L,L,L,L,L,L,L";
            }
            else
            {
                tabLOG.HeaderString = "Flight,STA,STD,A/D,Field,Code,Tran,Old Value,New Value,Time of Change,Changed by User,Furno";
                tabLOG.LogicalFieldList = "UREF,STOA,STOD,ADID,ADDI,FINA,TRAN,OVAL,VALU,TIME,USEC,FURN";//tabLOG.HeaderString;
                tabLOG.HeaderLengthString = "90,150,150,40,200,50,40,150,150,170,120,100";
                tabLOG.ColumnWidthString = "10,14,14,6,6,10,6,14,14,14,10,10";
                tabLOG.ColumnAlignmentString = "L,L,L,C,L,L,C,L,L,L,L,L";
            }
            string strColor = UT.colBlue + "," + UT.colBlue;
            tabLOG.CursorDecoration(tabLOG.LogicalFieldList, "B,T", "2,2", strColor);

            int idx = UT.GetItemNo(tabLOG.LogicalFieldList, "TIME");
            tabLOG.DateTimeSetColumnFormat(idx, "YYYYMMDDhhmmss", "DD'.'MM'.'YYYY' / 'hh':'mm':'ss");

            // Settting the Time Fields for the Flight Data
            SetFlightDataTimeFields();

        }
        /// <summary>
        /// Set the Time/Date fields of Main/Log Tabel for the Flight Data
        /// </summary>
        private void SetFlightDataTimeFields()
        {
            //Formatting of date time columns
            int idx = 0;
            idx = UT.GetItemNo(tabLOG.LogicalFieldList, "STOA");
            tabLOG.DateTimeSetColumnFormat(idx, "YYYYMMDDhhmmss", "DD'.'MM'.'YYYY' / 'hh':'mm");
            idx = UT.GetItemNo(tabLOG.LogicalFieldList, "STOD");
            tabLOG.DateTimeSetColumnFormat(idx, "YYYYMMDDhhmmss", "DD'.'MM'.'YYYY' / 'hh':'mm");
            idx = UT.GetItemNo(tabLOG.LogicalFieldList, "TIME");
            tabLOG.DateTimeSetColumnFormat(idx, "YYYYMMDDhhmmss", "DD'.'MM'.'YYYY' / 'hh':'mm':'ss");
            if (m_AddonFields)
            {
                idx = UT.GetItemNo(tabLOG.LogicalFieldList, "STOX");
                tabLOG.DateTimeSetColumnFormat(idx, "YYYYMMDDhhmmss", "DD'.'MM'.'YYYY' / 'hh':'mm':'ss");
            }
            if (m_FieldType.Equals("D"))
            {
                idx = UT.GetItemNo(tabLOG.LogicalFieldList, "VALU");
                tabLOG.DateTimeSetColumnFormat(idx, "YYYYMMDDhhmmss", "DD'.'MM'.'YYYY' / 'hh':'mm");
                idx = UT.GetItemNo(tabLOG.LogicalFieldList, "OVAL");
                tabLOG.DateTimeSetColumnFormat(idx, "YYYYMMDDhhmmss", "DD'.'MM'.'YYYY' / 'hh':'mm");
            }
        }

        /// <summary>
        /// Fill the Main table with the Changes of selected Basic Data Table.
        /// </summary>
        private void FillBasicDataSelection(string strWhere)
        {
            //clear table
            tabLOG.ResetContent();

            // Get the UREF from the HXX TAB 
            ITable hxxTab = myDB.Bind(m_HxxTabStr, m_HxxTabStr, "URNO,FINA,OVAL,VALU,TIME,UREF,TRAN,USEC", "10,4,32,32,14,10,1,12", "URNO,FINA,OVAL,VALU,TIME,UREF,TRAN,USEC");
            hxxTab.Clear();
            Ufis.Utils.IniFile myIni = new IniFile("C:\\Ufis\\system\\DATACHANGE.INI");
            string strFields = myIni.IniReadValue(m_BasicTableCode, "FIELDS");

            if (strFields == "")
            {
                ITable sysTable = myDB["SYS"];
                if (sysTable == null)
                    return;

                for (int j = 0; j < sysTable.Count; j++)
                {
                    if (sysTable[j]["TANA"] == m_BasicTableCode)
                    {
                        strFields += sysTable[j]["FINA"] + ",";
                    }
                }
            }
            if (m_UserName2 != "ALL USER" && m_UserName2 != string.Empty)
                strWhere += " AND USEC = '" + m_UserName2 + "'" + " AND TANA='" + m_BasicTableCode + "' AND FINA IN('" + strFields.Replace(",", "','") + "')";
            else
                strWhere += " AND TANA='" + m_BasicTableCode + "' AND FINA IN('" + strFields.Replace(",", "','") + "')";
            hxxTab.Load(strWhere);

            string[] arrUref = new string[hxxTab.Count]; // HXX Urno Numbers Array
            string strUrnos = "";
            for (int j = 0; j < hxxTab.Count; j++)
            {
                arrUref[j] = hxxTab[j]["UREF"];
                strUrnos += "'" + hxxTab[j]["UREF"] + "',";
            }
            //Remove ,(comma) at the end of the String				
            strUrnos = strUrnos.TrimEnd(',');

            // The fields to load from the Selected Basic Data Table.
            string[] arrIdentCodes = m_strIdentifierCodes.Split(',');
            string strIdentifiers = "URNO," + m_strIdentifierCodes;

            // Build string of field Lengh for the Indentifiers.
            string strFieldLength = "10"; // URNO length
            for (int i = 0; i < arrIdentCodes.Length; i++)
                strFieldLength += ",256";

            // Loading Selected Baisc DataTable and find out the final records.
            myDB.Unbind(m_BasicTableCode);
            ITable myTable = myDB.Bind(m_BasicTableCode, m_BasicTableCode, strIdentifiers, strFieldLength, strIdentifiers);
            myTable.Clear();

            strWhere = "WHERE URNO IN(" + strUrnos + ")" + m_FilterConStr;

            myTable.Load(strWhere);

            string strLine = "";
            for (int i = 0; i < arrUref.Length; i++)
            {
                for (int j = 0; j < myTable.Count; j++)
                {
                    if (arrUref[i] == myTable[j]["URNO"])
                    {
                        for (int k = 0; k < arrIdentCodes.Length; k++)
                            strLine += myTable[j][arrIdentCodes[k]] + ",";

                        // Build the Line to add in the Table.
                        strLine += GetFieldNameUsingCode(hxxTab[i]["FINA"]) + "," + hxxTab[i]["FINA"] + ","
                            + hxxTab[i]["TRAN"] + "," + CheckAndConvertDBString(hxxTab[i]["OVAL"]) + "," + CheckAndConvertDBString(hxxTab[i]["VALU"])
                            + "," + hxxTab[i]["TIME"] + "," + hxxTab[i]["USEC"] + "," + myTable[j]["URNO"];

                        tabLOG.InsertTextLine(strLine, false);
                        string strTRAN = tabLOG.GetFieldValue(i, "TRAN");

                        // The Insterted Line Number
                        int newLineNo = tabLOG.Lines - 1;
                        //Set the Line color
                        if (hxxTab[i]["TRAN"] == "I")
                            tabLOG.SetLineColor(newLineNo, -1, UT.colLightGreen);
                        else if (hxxTab[i]["TRAN"] == "U")
                            tabLOG.SetLineColor(newLineNo, -1, UT.colLightYellow);
                        strLine = "";
                        break;
                    }
                }
            }

            myIsTimeInUtc = true;
            HandleUTC();
            tabLOG.Refresh();
            this.Cursor = Cursors.Arrow;
            cbView.Checked = false;
            cbView.BackColor = Color.Transparent;
        }

        private string CheckAndConvertDBString(string strDateTimeStr)
        {
            string strReturn = strDateTimeStr; ;
            DateTime strOldValTime;

            if (strDateTimeStr.Length == 14)
            {
                for (int ii = 0; ii < 14; ii++)
                {
                    if (!Char.IsNumber(strDateTimeStr[ii]))
                    {
                        return strReturn;
                    }
                }
                strOldValTime = UT.CedaTimeToDateTime(strDateTimeStr);


                /* UTC conversion is completely handeled from HandleUTC()
                if (myIsTimeInUtc == false)
                {
                    strOldValTime = UT.UtcToLocal(strOldValTime);
                }
                */




                strReturn = strOldValTime.ToString("dd.MM.yyyy '/' HH:mm");
            }
            return strReturn;
        }

        private void frmMain_MouseEnter(object sender, EventArgs e)
        {
            bmMouseInsideTabControl = true;
        }

        private void frmMain_MouseLeave(object sender, EventArgs e)
        {
            bmMouseInsideTabControl = false;
        }

        private void frmMain_MouseWheel(object sender, MouseEventArgs e)
        {
            if (bmMouseInsideTabControl == false || CheckScrollingStatus(tabLOG) == false)
            {
                return;
            }
            System.Windows.Forms.Control ba = (System.Windows.Forms.Control)sender;

            if (e.Delta > 0 && tabLOG.GetVScrollPos() > 0)
            {
                tabLOG.OnVScrollTo(tabLOG.GetVScrollPos() - 1);
            }
            else if (e.Delta < 0 && (tabLOG.GetVScrollPos() + 1) < tabLOG.GetLineCount())
            {
                tabLOG.OnVScrollTo(tabLOG.GetVScrollPos() + 1);
            }
        }

        private bool CheckScrollingStatus(AxTABLib.AxTAB tabLOG)
        {
            bool blCheckScrollingStatus = false;
            int ilOffset = 0;

            if (tabLOG.HScrollMaster == true)
            {
                ilOffset = 16;
            }

            int ilHeaderLines = 0;

            if (tabLOG.MainHeaderOnly == false && tabLOG.MainHeader == true)
            {
                ilHeaderLines = 2;
            }
            else if (tabLOG.MainHeaderOnly == true && tabLOG.MainHeader == true)
            {
                ilHeaderLines = 1;
            }
            else if (tabLOG.MainHeaderOnly == true && tabLOG.MainHeader == false)
            {
                ilHeaderLines = 0;
            }
            else
            {
                ilHeaderLines = 1;
            }

            ilOffset += (ilHeaderLines * tabLOG.LineHeight);
            int ilRemainder = (int)(tabLOG.Size.Height - ilOffset) % tabLOG.LineHeight;
            int ilMaxVisibleLines = (int)((tabLOG.Size.Height - ilOffset) / tabLOG.LineHeight);

            if (ilRemainder != 0)
            {
                ilMaxVisibleLines -= 1;
            }
            if (tabLOG.GetLineCount() <= ilMaxVisibleLines)
            {
                blCheckScrollingStatus = false;
            }
            else
            {
                blCheckScrollingStatus = true;
            }
            return blCheckScrollingStatus;
        }
        private void FillCommonCKISelection(string strWhen)
        {

            if (myAllFieldsCKI.Length > 0)
            {
                //clear table
                tabLOG.ResetContent();

                //Loading Common CKI DataTable and find out the final records.
                string[] arrIdentCodes = "CKIC,CKBS,CKES".Split(',');
                string strIdentifiers = "URNO,CKIC,CKBS,CKES,FLNU,GURN";

                // Build string of field Lengh for the Indentifiers.
                string strFieldLength = "10"; // URNO length
                for (int i = 0; i < arrIdentCodes.Length; i++)
                    strFieldLength += ",256";
                myDB.Unbind("CKI");
                ITable myTable = myDB.Bind("CKI", "CCA", "URNO,CKIC,CKBS,CKES,FLNU,GURN", strFieldLength, strIdentifiers);
                myTable.Clear();
                string strWhere = "WHERE CTYP='C' AND " + m_selCKI;
                myTable.Load(strWhere);
                //get the CCA ID loaded
                string strUrnoCCA = ""; List<string> tmp = new List<string>();
                for (int j = 0; j < myTable.Count; j++)
                {
                    string uref = myTable[j]["URNO"];
                    if (uref == "" || uref == "0") continue;
                    if (!tmp.Contains(uref))
                    {
                        tmp.Add(uref);
                        strUrnoCCA += "'" + uref + "',";
                    }
                }
                strUrnoCCA = strUrnoCCA.TrimEnd(',');

                string logTableName = "";
                List<string> lsLogFields = new List<string>();
                CtrlSYSTabData.GetInstance().GetLogInfoFor("CCA", out logTableName, out lsLogFields);

                string logicalfields = "URNO,TANA,FINA,OVAL,VALU,TIME,UREF,TRAN,USEC,KEYF";
                string fieldlength = "10,4,4,32,32,14,10,1,12,12";
                string dbfields = "URNO,TANA,FINA,OVAL,VALU,TIME,UREF,TRAN,USEC,KEYF";


                //ITable hxxTab = myDB.Bind(logTableName, logTableName, logicalfields, fieldlength, dbfields);
                logCKITAB = myDB.Bind(logTableName, logTableName, logicalfields, fieldlength, dbfields);
                logCKITAB.Clear();
                if (m_UserName3 != "ALL USER" && m_UserName3 != string.Empty)
                    strWhere = strWhen + " AND USEC = '" + m_UserName3 + "'" + " AND  TANA='" + "CCA" + "' AND FINA IN(" + myAllFieldsCKI + ") AND UREF IN(" + strUrnoCCA + ")";
                else
                    strWhere = strWhen + " AND  TANA='" + "CCA" + "' AND FINA IN(" + myAllFieldsCKI + ") AND UREF IN(" + strUrnoCCA + ")";

                logCKITAB.Load(strWhere);

                List<string> lstUrno = new List<string>();
                lstUrno.Add(strUrnoCCA);

                logCKITAB = CtrlLog.GetInstance().LoadCKIRelatedLog(strWhen, logCKITAB, lstUrno, logicalfields, fieldlength, dbfields);



                if (logCKITAB.Count > 0)
                {
                    logCKITAB.Sort("TIME", true);
                }

                string[] arrUref = new string[logCKITAB.Count]; // HXX Urno Numbers Array
                string strUrnos = "";

                for (int j = 0; j < logCKITAB.Count; j++)
                {
                    string uref = logCKITAB[j]["UREF"];
                    arrUref[j] = uref;
                    strUrnos += "'" + uref + "',";
                }
                //Remove ,(comma) at the end of the String				
                strUrnos = strUrnos.TrimEnd(',');

                List<string> strTimeFields = new List<string>();
                string strLine = "";
                for (int i = 0; i < arrUref.Length; i++)
                {
                    for (int j = 0; j < myTable.Count; j++)
                    {
                        if (arrUref[i] == myTable[j]["URNO"])
                        {
                            string fina = logCKITAB[i]["FINA"];
                            string strADDIDesc = GetFieldNameUsingCode(fina);
                            string desc = GetFieldNameUsingCode(fina).Length > 0 ? strADDIDesc : fina;
                            CtrlSYSTabData.GetInstance().GetFieldDesc("CCA", fina);
                            string type = CtrlSYSTabData.GetInstance().GetFieldType("CCA", fina);
                            string oldvalue = logCKITAB[i]["OVAL"];
                            string newvalue = logCKITAB[i]["VALU"];

                            if (type == "DATE")
                            {
                                if (!strTimeFields.Contains(fina))
                                {
                                    strTimeFields.Add(fina);
                                }
                                if (oldvalue.Length > 0)
                                {
                                    oldvalue = CheckAndConvertDBString(oldvalue);
                                    //oldvalue = ChangeDT(oldvalue);
                                }

                                if (newvalue.Length > 0)
                                {
                                    newvalue = CheckAndConvertDBString(newvalue);
                                    //newvalue = ChangeDT(oldvalue);
                                }
                            }


                            strLine = myTable[j][arrIdentCodes[0]] + ","
                                    + myTable[j][arrIdentCodes[1]] + ","
                                    + myTable[j][arrIdentCodes[2]] + ","
                                    + desc + ","
                                    + logCKITAB[i]["FINA"] + ","
                                    + logCKITAB[i]["TRAN"] + ","
                                    + oldvalue + ","
                                    + newvalue + ","
                                    + logCKITAB[i]["TIME"] + ","
                                    + logCKITAB[i]["USEC"] + ","
                                    + logCKITAB[i]["URNO"];


                            tabLOG.InsertTextLine(strLine, false);
                            string strTRAN = tabLOG.GetFieldValue(i, "TRAN");

                            // The Insterted Line Number
                            int newLineNo = tabLOG.Lines - 1;
                            //Set the Line color
                            if (logCKITAB[i]["TRAN"] == "I")
                                tabLOG.SetLineColor(newLineNo, -1, UT.colLightGreen);
                            else if (logCKITAB[i]["TRAN"] == "U")
                                tabLOG.SetLineColor(newLineNo, -1, UT.colLightYellow);
                            strLine = "";
                            break;
                        }
                    }
                }
                if (strTimeFields.Count > 0)
                {
                    //logCKITAB.TimeFields = ConvListToString(strTimeFields, ",");

                    logCKITAB.TimeFields = "CKBS,CKES";
                }

                myIsTimeInUtc = true;
                HandleUTC();
                tabLOG.Refresh();
                this.Cursor = Cursors.Arrow;
                cbView.Checked = false;
                cbView.BackColor = Color.Transparent;
            }
        }

        public string ConvListToString(List<string> arr, string delimiter)
        {
            StringBuilder result = new StringBuilder();
            if ((arr != null) && (arr.Count > 0))
            {
                int cnt = arr.Count;
                result.Append((string)arr[0]);
                for (int i = 1; i < cnt; i++)
                {
                    result.Append(delimiter + (string)arr[i]);
                }
            }
            return result.ToString();
        }
        private string ChangeDT(string strDT)
        {
            DateTime dat = UT.CedaFullDateToDateTime(strDT);
            if (cbUTC.Checked == true && myIsTimeInUtc == false)
                dat = UT.LocalToUtc(dat);
            if (cbUTC.Checked == false && myIsTimeInUtc == true)
                dat = UT.UtcToLocal(dat);
            string strTmp = UT.DateTimeToCeda(dat);

            DateTime datRet;
            int ilYY = 0;
            int ilMo = 0;
            int ilDD = 0;
            int ilHH = 0;
            int ilMM = 0;
            int ilSS = 0;

            try
            {
                ilYY = Convert.ToInt32(strTmp.Substring(6, 4));
                ilMo = Convert.ToInt32(strTmp.Substring(3, 2));
                ilDD = Convert.ToInt32(strTmp.Substring(0, 2));
                ilHH = Convert.ToInt32(strTmp.Substring(13, 2));
                ilMM = Convert.ToInt32(strTmp.Substring(16, 2));
            }
            catch
            {
            }

            datRet = new DateTime(ilYY, ilMo, ilDD, ilHH, ilMM, ilSS);

            if (cbUTC.Checked == true && myIsTimeInUtc == false)
                datRet = UT.LocalToUtc(datRet);
            if (cbUTC.Checked == false && myIsTimeInUtc == true)
                datRet = UT.UtcToLocal(datRet);

            return datRet.ToString("dd.MM.yyyy '/' HH:mm");
        }

    }
}
