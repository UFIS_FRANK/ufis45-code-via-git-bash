﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace Ufis.Status_Manager.Ent
{
    public class EntObjectType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public EntSymbol Symbol { get; set; }
        public Color DefaultColor { get; set; }

        public EntObjectType(): this(0, null, null, Color.White)
        {
        }

        public EntObjectType(int id, string name, EntSymbol symbol, Color color)
        {
            this.Id = id;
            this.Name = name;
            this.Symbol = symbol;
            this.DefaultColor = color;
        }

        public override string ToString()
        {
            return base.ToString() + ";" + this.Id.ToString();
        }
    }

    public class EntObjectTypeCollection : List<EntObjectType>
    {
        public void GenerateObjectTypes()
        {
            this.Add(new EntObjectType(0, "Staff", 
                new EntSymbol(10, EntSymbol.EntSymbolShapeEnum.Circle), Color.SpringGreen));
            this.Add(new EntObjectType(1, "Equipment", 
                new EntSymbol(10, EntSymbol.EntSymbolShapeEnum.Square), Color.SteelBlue));
        }

        public EntObjectTypeCollection(): this(false)
        {
        }

        public EntObjectTypeCollection(bool autoGenerate)
        {
            if (autoGenerate)
                GenerateObjectTypes();
        }
    }
}
