﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ufis.Status_Manager.Ctrl
{
    public class CtrlMain
    {
        public const string APPLN_ID = "STATMGR";
        public const string TAB_EXT = "TAB";

        public static AxAATLOGINLib.AxAatLogin LoginControl = null;

        
        public static string GetUserId()
        {
            if (LoginControl == null) return "";
            else return LoginControl.GetUserName();
        }
    }
}
