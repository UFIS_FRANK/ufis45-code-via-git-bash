using System;
using DataDynamics.ActiveReports;
//using DataDynamics.ActiveReports.Document;
using Ufis.Utils;
using System.Drawing;
namespace Ufis.Status_Manager.Rpt
{
	public class rptDetailStatus : ActiveReport3
	{
		private string strSection = "";
		private int currLine = 0;
		AxTABLib.AxTAB tabArrival = null; 
		AxTABLib.AxTAB tabDeparture = null; 
		AxTABLib.AxTAB tabStatus = null;

		public rptDetailStatus(AxTABLib.AxTAB tArrival, AxTABLib.AxTAB tDeparture, 
						      AxTABLib.AxTAB tStatus, string sSection)
		{
			strSection = sSection;
			//FLNO,STOA,ETAI,ONBL,NA,ORIG,VIA,POS,GATE,ACT3
			tabArrival = tArrival;
			//FLNO,STOD,ETDI,OFBL,NA,VIA,DES,POS,GATE,ACT
			tabDeparture = tDeparture;
			//CONA,CONU,RULNAME,REFFIELD,STATUSNAME,CFLTYPE_DB,CONFLICTTYPE,TIME,SORT,REMA
			tabStatus = tStatus;
			InitializeReport();
			this.ShowParameterUI = true;
		}

		private void rptDetailStatus_ReportStart(object sender, System.EventArgs eArgs)
		{
			//string strtmp="";
			DateTime d		= DateTime.Now;
			DateTime dt;
			txtDate.Text	= d.ToString();
//*** Read the logo if exists and set it
			IniFile myIni = new IniFile("C:\\Ufis\\System\\Ceda.ini");
			string strPrintLogoPath = myIni.IniReadValue("STATUSMANAGER", "PRINTLOGO");
            System.Drawing.Image myImage = null;
			try
			{
				if(strPrintLogoPath != "")
				{
					myImage = new Bitmap(strPrintLogoPath);
				}
			}
			catch(Exception)
			{
			}
			if(myImage != null)
				Picture1.Image = myImage;
//*** END Read the logo if exists and set it
			lblSection.Text = strSection;
			txtFLNO_A.Text	= tabArrival.GetFieldValue(0, "FLNO");
			if(tabArrival.GetFieldValue(0, "STOA") != "")
			{
				dt = UT.CedaFullDateToDateTime(tabArrival.GetFieldValue(0, "STOA"));
				txtSTA.Text		= dt.ToString("HH:mm/dd");
			}
			if(tabArrival.GetFieldValue(0, "ETAI") != "")
			{
				dt = UT.CedaFullDateToDateTime(tabArrival.GetFieldValue(0, "ETAI"));
				txtETA.Text		= dt.ToString("HH:mm/dd");
			}
			if(tabArrival.GetFieldValue(0, "ONBL") != "")
			{
				dt = UT.CedaFullDateToDateTime(tabArrival.GetFieldValue(0, "ONBL"));
				txtONBL.Text	= dt.ToString("HH:mm/dd");
			}
			txtNA_A.Text	= tabArrival.GetFieldValue(0, "NA");
			txtORG.Text		= tabArrival.GetFieldValue(0, "ORIG");
			txtVIA_A.Text	= tabArrival.GetFieldValue(0, "VIA");
			txtPOS_A.Text	= tabArrival.GetFieldValue(0, "POS");
			txtGATE_A.Text	= tabArrival.GetFieldValue(0, "GATE"); 
			txtAC_A.Text	= tabArrival.GetFieldValue(0, "ACT3");

			txtFLNO_D.Text = tabDeparture.GetFieldValue(0, "FLNO");
			if(tabDeparture.GetFieldValue(0, "STOD") != "")
			{
				dt = UT.CedaFullDateToDateTime(tabDeparture.GetFieldValue(0, "STOD"));
				txtSTD.Text    = dt.ToString("HH:mm/dd");
			}
			if(tabDeparture.GetFieldValue(0, "ETDI") != "")
			{
				dt = UT.CedaFullDateToDateTime(tabDeparture.GetFieldValue(0, "ETDI"));
				txtETD.Text    = dt.ToString("HH:mm/dd");
			}
			if(tabDeparture.GetFieldValue(0, "OFBL") != "")
			{
				dt = UT.CedaFullDateToDateTime(tabDeparture.GetFieldValue(0, "OFBL"));
				txtOFBL.Text   = dt.ToString("HH:mm/dd");
			}
			txtNA_D.Text   = tabDeparture.GetFieldValue(0, "NA");
			txtVIA_D.Text  = tabDeparture.GetFieldValue(0, "VIA");
			txtDEST.Text   = tabDeparture.GetFieldValue(0, "DES");
			txtPOS_D.Text  = tabDeparture.GetFieldValue(0, "POS");
			txtGATE_D.Text = tabDeparture.GetFieldValue(0, "GATE"); 
			txtAC_D.Text   = tabDeparture.GetFieldValue(0, "ACT"); 
		}

		private void rptDetailStatus_FetchData(object sender, DataDynamics.ActiveReports.ActiveReport3.FetchEventArgs eArgs)
		{
			//CONA,CONU,RULNAME,REFFIELD,STATUSNAME,CFLTYPE_DB,CONFLICTTYPE,TIME,SORT,REMA
			DateTime dt;
			sep1.Y1 = 0; 
			sep2.Y1 =  0;
			sep3.Y1 =  0;
			sep4.Y1 =  0;
			sep5.Y1 =  0;
			sep6.Y1 =  0;
			sep7.Y1 =  0;
			sep8.Y1 =  0;
			sep9.Y1 =  0;
			txtUSER.Text = "";
			txtSYSTEM.Text = "";
			txtEXPECTED.Text = "";
			if(tabStatus.GetFieldValue(currLine, "CONS") != "")
			{
				string strTmp = tabStatus.GetFieldValue(currLine, "CONS");
				if(strTmp.Length == 14)
				{
					dt = UT.CedaFullDateToDateTime(tabStatus.GetFieldValue(currLine, "CONS"));
					txtEXPECTED.Text = " " + dt.ToString("HH:mm/dd");
				}
			}
			if(tabStatus.GetFieldValue(currLine, "CONA") != "")
			{
				string strTmp = tabStatus.GetFieldValue(currLine, "CONA");
				if(strTmp.Length == 14)
				{
					dt = UT.CedaFullDateToDateTime(tabStatus.GetFieldValue(currLine, "CONA"));
					
					txtSYSTEM.Text = " " + dt.ToString("HH:mm/dd");
				}
			}
			if(tabStatus.GetFieldValue(currLine, "CONU") != "")
			{
				string strTmp = tabStatus.GetFieldValue(currLine, "CONU");
				if(strTmp.Length == 14)
				{
					dt = UT.CedaFullDateToDateTime(tabStatus.GetFieldValue(currLine, "CONU"));
					txtUSER.Text = " " + dt.ToString("HH:mm/dd");
				}
			} 
			txtRULENAME.Text = " " + tabStatus.GetFieldValue(currLine, "RULNAME");
			txtSTATUSNAME.Text = " " + tabStatus.GetFieldValue(currLine, "STATUSNAME");
			txtCFLTYPE.Text =  " " + tabStatus.GetFieldValue(currLine, "CONFLICTTYPE");
			txtTIME.Text = 	" " + tabStatus.GetFieldValue(currLine, "TIME");
			txtREMARK.Text =  " " + tabStatus.GetFieldValue(currLine, "REMA");
			if(currLine == tabStatus.GetLineCount())
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
				currLine++;
			}
		}

		private void Detail_BeforePrint(object sender, System.EventArgs eArgs)
		{
			sep1.Y2 = txtREMARK.Height; 
			sep2.Y2 =  txtREMARK.Height;
			sep3.Y2 =  txtREMARK.Height;
			sep4.Y2 =  txtREMARK.Height;
			sep5.Y2 =  txtREMARK.Height;
			sep6.Y2 =  txtREMARK.Height;
			sep7.Y2 =  txtREMARK.Height;
			sep8.Y2 =  txtREMARK.Height;
			sep9.Y2 =  txtREMARK.Height;
			sepHorz.Y1 = txtREMARK.Height;
			sepHorz.Y2 = txtREMARK.Height;
		}

		#region ActiveReports Designer generated code
		private DataDynamics.ActiveReports.PageHeader PageHeader;
		private DataDynamics.ActiveReports.Label Label35;
		private DataDynamics.ActiveReports.Label Label1;
		private DataDynamics.ActiveReports.Label lblSection;
		private DataDynamics.ActiveReports.Label Label24;
		private DataDynamics.ActiveReports.Label Label2;
		private DataDynamics.ActiveReports.Label Label4;
		private DataDynamics.ActiveReports.Label Label5;
		private DataDynamics.ActiveReports.Label Label6;
		private DataDynamics.ActiveReports.Label Label7;
		private DataDynamics.ActiveReports.Label Label8;
		private DataDynamics.ActiveReports.Label Label9;
		private DataDynamics.ActiveReports.Label Label10;
		private DataDynamics.ActiveReports.Label Label11;
		private DataDynamics.ActiveReports.Label Label12;
		private DataDynamics.ActiveReports.Label Label13;
		private DataDynamics.ActiveReports.Label Label15;
		private DataDynamics.ActiveReports.Label Label16;
		private DataDynamics.ActiveReports.Label Label17;
		private DataDynamics.ActiveReports.Label Label18;
		private DataDynamics.ActiveReports.Label Label19;
		private DataDynamics.ActiveReports.Label Label21;
		private DataDynamics.ActiveReports.Label Label22;
		private DataDynamics.ActiveReports.Label Label23;
		private DataDynamics.ActiveReports.Line Line4;
		private DataDynamics.ActiveReports.Line Line25;
		private DataDynamics.ActiveReports.Line Line26;
		private DataDynamics.ActiveReports.TextBox txtFLNO_A;
		private DataDynamics.ActiveReports.TextBox txtSTA;
		private DataDynamics.ActiveReports.TextBox txtETA;
		private DataDynamics.ActiveReports.TextBox txtONBL;
		private DataDynamics.ActiveReports.TextBox txtNA_A;
		private DataDynamics.ActiveReports.TextBox txtORG;
		private DataDynamics.ActiveReports.TextBox txtVIA_A;
		private DataDynamics.ActiveReports.TextBox txtPOS_A;
		private DataDynamics.ActiveReports.TextBox txtGATE_A;
		private DataDynamics.ActiveReports.TextBox txtAC_A;
		private DataDynamics.ActiveReports.TextBox txtFLNO_D;
		private DataDynamics.ActiveReports.TextBox txtSTD;
		private DataDynamics.ActiveReports.TextBox txtETD;
		private DataDynamics.ActiveReports.TextBox txtOFBL;
		private DataDynamics.ActiveReports.TextBox txtNA_D;
		private DataDynamics.ActiveReports.TextBox txtVIA_D;
		private DataDynamics.ActiveReports.TextBox txtDEST;
		private DataDynamics.ActiveReports.TextBox txtPOS_D;
		private DataDynamics.ActiveReports.TextBox txtGATE_D;
		private DataDynamics.ActiveReports.TextBox txtAC_D;
		private DataDynamics.ActiveReports.Line Line28;
		private DataDynamics.ActiveReports.Line Line29;
		private DataDynamics.ActiveReports.Line Line30;
		private DataDynamics.ActiveReports.Line Line31;
		private DataDynamics.ActiveReports.Line Line32;
		private DataDynamics.ActiveReports.Line Line33;
		private DataDynamics.ActiveReports.Line Line34;
		private DataDynamics.ActiveReports.Line Line35;
		private DataDynamics.ActiveReports.Line Line36;
		private DataDynamics.ActiveReports.Line Line37;
		private DataDynamics.ActiveReports.Line Line38;
		private DataDynamics.ActiveReports.Line Line39;
		private DataDynamics.ActiveReports.Line Line40;
		private DataDynamics.ActiveReports.Line Line41;
		private DataDynamics.ActiveReports.Line Line42;
		private DataDynamics.ActiveReports.Line Line43;
		private DataDynamics.ActiveReports.Line Line44;
		private DataDynamics.ActiveReports.Line Line45;
		private DataDynamics.ActiveReports.Line Line46;
		private DataDynamics.ActiveReports.Line Line47;
		private DataDynamics.ActiveReports.Line Line48;
		private DataDynamics.ActiveReports.Line Line49;
		private DataDynamics.ActiveReports.Line Line50;
		private DataDynamics.ActiveReports.Label Label25;
		private DataDynamics.ActiveReports.Label Label26;
		private DataDynamics.ActiveReports.Label Label27;
		private DataDynamics.ActiveReports.Label Label28;
		private DataDynamics.ActiveReports.Label Label29;
		private DataDynamics.ActiveReports.Label Label30;
		private DataDynamics.ActiveReports.Label Label31;
		private DataDynamics.ActiveReports.Line Line51;
		private DataDynamics.ActiveReports.Line Line52;
		private DataDynamics.ActiveReports.Line Line53;
		private DataDynamics.ActiveReports.Line Line54;
		private DataDynamics.ActiveReports.Line Line55;
		private DataDynamics.ActiveReports.Line Line56;
		private DataDynamics.ActiveReports.Line Line57;
		private DataDynamics.ActiveReports.Line Line58;
		private DataDynamics.ActiveReports.Line Line59;
		private DataDynamics.ActiveReports.Line Line60;
		private DataDynamics.ActiveReports.Line Line70;
		private DataDynamics.ActiveReports.Picture Picture1;
		private DataDynamics.ActiveReports.Detail Detail;
		private DataDynamics.ActiveReports.TextBox txtEXPECTED;
		private DataDynamics.ActiveReports.TextBox txtSYSTEM;
		private DataDynamics.ActiveReports.TextBox txtUSER;
		private DataDynamics.ActiveReports.TextBox txtRULENAME;
		private DataDynamics.ActiveReports.TextBox txtSTATUSNAME;
		private DataDynamics.ActiveReports.TextBox txtCFLTYPE;
		private DataDynamics.ActiveReports.TextBox txtTIME;
		private DataDynamics.ActiveReports.TextBox txtREMARK;
		private DataDynamics.ActiveReports.Line sep3;
		private DataDynamics.ActiveReports.Line sep1;
		private DataDynamics.ActiveReports.Line sep2;
		private DataDynamics.ActiveReports.Line sep4;
		private DataDynamics.ActiveReports.Line sep5;
		private DataDynamics.ActiveReports.Line sep6;
		private DataDynamics.ActiveReports.Line sep7;
		private DataDynamics.ActiveReports.Line sep8;
		private DataDynamics.ActiveReports.Line sepHorz;
		private DataDynamics.ActiveReports.Line sep9;
		private DataDynamics.ActiveReports.PageFooter PageFooter;
		private DataDynamics.ActiveReports.TextBox TextBox1;
		private DataDynamics.ActiveReports.Label Label32;
		private DataDynamics.ActiveReports.TextBox TextBox2;
		private DataDynamics.ActiveReports.Label Label33;
		private DataDynamics.ActiveReports.Label Label34;
		private DataDynamics.ActiveReports.TextBox txtDate;
		public void InitializeReport()
		{
			this.LoadLayout(this.GetType(), "Ufis.Status_Manager.rptDetailStatus.rpx");
			this.PageHeader = ((DataDynamics.ActiveReports.PageHeader)(this.Sections["PageHeader"]));
			this.Detail = ((DataDynamics.ActiveReports.Detail)(this.Sections["Detail"]));
			this.PageFooter = ((DataDynamics.ActiveReports.PageFooter)(this.Sections["PageFooter"]));
			this.Label35 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[0]));
			this.Label1 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[1]));
			this.lblSection = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[2]));
			this.Label24 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[3]));
			this.Label2 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[4]));
			this.Label4 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[5]));
			this.Label5 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[6]));
			this.Label6 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[7]));
			this.Label7 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[8]));
			this.Label8 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[9]));
			this.Label9 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[10]));
			this.Label10 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[11]));
			this.Label11 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[12]));
			this.Label12 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[13]));
			this.Label13 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[14]));
			this.Label15 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[15]));
			this.Label16 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[16]));
			this.Label17 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[17]));
			this.Label18 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[18]));
			this.Label19 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[19]));
			this.Label21 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[20]));
			this.Label22 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[21]));
			this.Label23 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[22]));
			this.Line4 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[23]));
			this.Line25 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[24]));
			this.Line26 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[25]));
			this.txtFLNO_A = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[26]));
			this.txtSTA = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[27]));
			this.txtETA = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[28]));
			this.txtONBL = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[29]));
			this.txtNA_A = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[30]));
			this.txtORG = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[31]));
			this.txtVIA_A = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[32]));
			this.txtPOS_A = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[33]));
			this.txtGATE_A = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[34]));
			this.txtAC_A = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[35]));
			this.txtFLNO_D = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[36]));
			this.txtSTD = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[37]));
			this.txtETD = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[38]));
			this.txtOFBL = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[39]));
			this.txtNA_D = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[40]));
			this.txtVIA_D = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[41]));
			this.txtDEST = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[42]));
			this.txtPOS_D = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[43]));
			this.txtGATE_D = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[44]));
			this.txtAC_D = ((DataDynamics.ActiveReports.TextBox)(this.PageHeader.Controls[45]));
			this.Line28 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[46]));
			this.Line29 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[47]));
			this.Line30 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[48]));
			this.Line31 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[49]));
			this.Line32 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[50]));
			this.Line33 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[51]));
			this.Line34 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[52]));
			this.Line35 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[53]));
			this.Line36 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[54]));
			this.Line37 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[55]));
			this.Line38 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[56]));
			this.Line39 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[57]));
			this.Line40 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[58]));
			this.Line41 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[59]));
			this.Line42 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[60]));
			this.Line43 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[61]));
			this.Line44 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[62]));
			this.Line45 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[63]));
			this.Line46 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[64]));
			this.Line47 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[65]));
			this.Line48 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[66]));
			this.Line49 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[67]));
			this.Line50 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[68]));
			this.Label25 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[69]));
			this.Label26 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[70]));
			this.Label27 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[71]));
			this.Label28 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[72]));
			this.Label29 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[73]));
			this.Label30 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[74]));
			this.Label31 = ((DataDynamics.ActiveReports.Label)(this.PageHeader.Controls[75]));
			this.Line51 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[76]));
			this.Line52 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[77]));
			this.Line53 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[78]));
			this.Line54 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[79]));
			this.Line55 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[80]));
			this.Line56 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[81]));
			this.Line57 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[82]));
			this.Line58 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[83]));
			this.Line59 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[84]));
			this.Line60 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[85]));
			this.Line70 = ((DataDynamics.ActiveReports.Line)(this.PageHeader.Controls[86]));
			this.Picture1 = ((DataDynamics.ActiveReports.Picture)(this.PageHeader.Controls[87]));
			this.txtEXPECTED = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[0]));
			this.txtSYSTEM = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[1]));
			this.txtUSER = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[2]));
			this.txtRULENAME = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[3]));
			this.txtSTATUSNAME = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[4]));
			this.txtCFLTYPE = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[5]));
			this.txtTIME = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[6]));
			this.txtREMARK = ((DataDynamics.ActiveReports.TextBox)(this.Detail.Controls[7]));
			this.sep3 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[8]));
			this.sep1 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[9]));
			this.sep2 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[10]));
			this.sep4 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[11]));
			this.sep5 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[12]));
			this.sep6 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[13]));
			this.sep7 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[14]));
			this.sep8 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[15]));
			this.sepHorz = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[16]));
			this.sep9 = ((DataDynamics.ActiveReports.Line)(this.Detail.Controls[17]));
			this.TextBox1 = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[0]));
			this.Label32 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[1]));
			this.TextBox2 = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[2]));
			this.Label33 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[3]));
			this.Label34 = ((DataDynamics.ActiveReports.Label)(this.PageFooter.Controls[4]));
			this.txtDate = ((DataDynamics.ActiveReports.TextBox)(this.PageFooter.Controls[5]));
			// Attach Report Events
			this.ReportStart += new System.EventHandler(this.rptDetailStatus_ReportStart);
			this.FetchData += new DataDynamics.ActiveReports.ActiveReport3.FetchEventHandler(this.rptDetailStatus_FetchData);
			this.Detail.BeforePrint += new System.EventHandler(this.Detail_BeforePrint);
		}

		#endregion
	}
}
