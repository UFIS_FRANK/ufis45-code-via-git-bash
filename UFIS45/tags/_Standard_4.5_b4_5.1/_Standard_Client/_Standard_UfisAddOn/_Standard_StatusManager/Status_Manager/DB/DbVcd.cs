﻿using System;
using System.Collections.Generic;
using System.Text;

using Ufis.Data;
using Ufis.Utils;

using Ufis.Status_Manager.Util;

namespace Ufis.Status_Manager.DB
{
    public class DbVcd
    {
        #region data member
        private static DbVcd _this = null;
        private static object _lockVcdTableProp = new object();

        private IDatabase _myDB = null;
        private ITable _vcdTable = null;
        #endregion

        private IDatabase MyDb
        {
            get
            {
                if (_myDB == null) _myDB = UT.GetMemDB();
                return _myDB;
            }
        }

        private ITable VcdTable
        {
            get
            {
                _vcdTable = MyDb["VCD"];
                if (_vcdTable == null)
                {
                    lock (_lockVcdTableProp)
                    {
                        _vcdTable = MyDb["VCD"];//Get and check again
                        if (_vcdTable == null)
                        {
                            _vcdTable = MyDb.Bind("VCD", "VCD",
                                "URNO,CKEY,CTYP,TEXT,VAFR,VATO,APPN,PKNO",
                                "14,32,32,2000,18,18,32,32",
                                "URNO,CKEY,CTYP,TEXT,VAFR,VATO,APPN,PKNO");
                            //_vcdTable.UseTrackChg = true;
                            //_vcdTable.TrackChgImmediately = false;//Not to track the changes until "StartTrackChg" for the particular row.
                        }
                    }
                }
                return _vcdTable;
            }
        }

        public ITable GetVcdTable()
        {
            return VcdTable;
        }

        #region Singleton
        string _applnId = "";
        string _userId = "";

        private DbVcd()
        {
            this._applnId = Ctrl.CtrlMain.APPLN_ID;
            this._userId = Ctrl.CtrlMain.LoginControl.GetUserName();
        }

        public static DbVcd GetInstance()
        {
            if (_this == null) _this = new DbVcd();
            return _this;
        }
        #endregion
        
        private static void LogMsg(string msg)
        {
            UT.LogMsg("DbVcd:" + msg);
        }

        #region Lock and Release Table
        public void LockVcdTableForWrite()
        {
            LogMsg("B4 Lock Vcd Write");
            VcdTable.Lock.AcquireWriterLock(System.Threading.Timeout.Infinite);
            LogMsg("AF Lock Vcd Write");
        }

        public void ReleaseVcdTableWrite()
        {
            if (VcdTable.Lock.IsWriterLockHeld)
            {
                LogMsg("B4 Release Lock Vcd Write");
                VcdTable.Lock.ReleaseWriterLock();
                LogMsg("AF Release Lock Vcd Write");
            }
        }

        public void LockVcdTableForRead()
        {
            LogMsg("B4 Lock Vcd Read");
            VcdTable.Lock.AcquireReaderLock(System.Threading.Timeout.Infinite);
            LogMsg("AF Lock Vcd Read");
        }

        public void ReleaseVcdTableRead()
        {
            if (VcdTable.Lock.IsReaderLockHeld)
            {
                LogMsg("B4 Release Lock Vcd Read");
                VcdTable.Lock.ReleaseReaderLock();
                LogMsg("AF Release Lock Vcd Read");
            }
        }
        #endregion

        public void LoadDataForUser(string applnId, string strUserId)
        {
            VcdTable.Load(string.Format("WHERE APPN='{0}' AND PKNO='{1}'", applnId, strUserId));
            VcdTable.CreateIndex("CKEY", "CKEY");
            VcdTable.CreateIndex("CTYP", "CTYP");
        }

        /*
         CTYP       CKEY                TEXT
         VIEW_S     MY_VIEW             MY_VIEW;......
         
         VIEW_M     MY_VIEW2            2
         VIEW_M_D   MY_VIEW2#1          MY_VIEW2;......
         VIEW_M_D   MY_VIEW2#2          ...............
         */

        const string CTYP_SINGLE_LINE_VIEW = "VIEW_D";
        const string CTYP_MULTI_LINE_VIEW_HDR = "VIEW_M";
        const string CTYP_MULTI_LINE_DATA = "VIEW_M_D";
        const string CTYP_DEFAULT_VIEW = "DEFAULT_VIEW";
        const string CKEY_MULTI_LINE_HDR_SEPARATOR = "#$";
        const int MAX_TEXT_LEN = 1990;

        public bool SaveViewInfo(string viewName, string viewInfo)
        {
            bool ok = false;
            if (viewName.Contains(CKEY_MULTI_LINE_HDR_SEPARATOR)) throw new ApplicationException(string.Format("Invalid character '{0}' in View Name", CKEY_MULTI_LINE_HDR_SEPARATOR ));
            if (viewInfo.Length <= MAX_TEXT_LEN)
            {
                ok = SaveViewInfoSingle(viewName, viewInfo);
            }
            else
            {
                ok = SaveViewInfoMulti(viewName, viewInfo);
            }

            return ok;
        }

        public bool SaveDefaultView(string viewName)
        {
            AddToTable(CTYP_DEFAULT_VIEW, CTYP_DEFAULT_VIEW, viewName);
            return VcdTable.Save();
        }

        public string GetDefaultView()
        {
            string stDefaultViewName = "";
            IRow[] viewRows = VcdTable.RowsByIndexValue("CTYP", CTYP_DEFAULT_VIEW);

            if (viewRows != null && viewRows.Length > 0)
            {
                stDefaultViewName = viewRows[0]["TEXT"];
            }

            return stDefaultViewName;
        }

        private bool SaveViewInfoSingle(string viewName, string viewInfo)
        {
            AddToTable(CTYP_SINGLE_LINE_VIEW, viewName, viewInfo);
            return VcdTable.Save();
        }

        private void AddToTable(string cType, string cKey, string text)
        {
            IRow[] viewRows = VcdTable.RowsByIndexValue("CKEY", cKey);
            IRow row = null;
            if (viewRows != null && viewRows.Length > 0)
            {
                row = viewRows[0];
                if (
                    (row["CTYP"] != cType) ||
                    (row["CKEY"] != cKey) ||
                    (row["TEXT"] != text) ||
                    (row["APPN"] != this._applnId) ||
                    (row["PKNO"] != this._userId))
                {
                    row["CTYP"] = cType;
                    row["CKEY"] = cKey;
                    row["TEXT"] = text;
                    row["APPN"] = this._applnId;
                    row["PKNO"] = this._userId;
                    row.Status = State.Modified;
                }
            }
            else
            {
                row = VcdTable.CreateEmptyRow();
                row["CTYP"] = cType;
                row["CKEY"] = cKey;
                row["TEXT"] = text;
                row["APPN"] = this._applnId;
                row["PKNO"] = this._userId;
                row.Status = State.Created;
                VcdTable.Add(row);
            }  
        }

        private bool SaveViewInfoMulti(string viewName, string viewInfo)
        {
            List<string> lsSt = UtilString.SplitBySize(viewInfo, MAX_TEXT_LEN);
            AddToTable(CTYP_MULTI_LINE_VIEW_HDR, viewName, lsSt.Count.ToString());
            for (int i = 0; i < lsSt.Count; i++)
            {
                AddToTable(CTYP_MULTI_LINE_DATA, viewName + CKEY_MULTI_LINE_HDR_SEPARATOR + (i + 1), lsSt[i]);
            }

            return VcdTable.Save();
        }

        public Dictionary<string, string> GetViewInfo()
        {
            Dictionary<string, string> mapView = new Dictionary<string, string>();
    
            ITable tab = VcdTable;
            Dictionary<string, string> mapByKeyForMultiLineData = new Dictionary<string, string>();
            Dictionary<string, int> mapViewDataCnt = new Dictionary<string, int>();
            
            int cnt = VcdTable.Count;

            for (int i = 0; i < cnt; i++)
            {
                IRow row = VcdTable[i];
                string cType = row["CTYP"];
                string cKey = row["CKEY"];
                string stText = row["TEXT"];

                switch (cType)
                {
                    case CTYP_SINGLE_LINE_VIEW:
                        mapView.Add(cKey, stText);
                        break;
                    case CTYP_MULTI_LINE_VIEW_HDR:
                        int datCnt = 0;
                        if (Int32.TryParse(stText.Trim(), out datCnt))
                        {
                            mapViewDataCnt.Add(cKey, datCnt);
                        }
                        break;
                    case CTYP_MULTI_LINE_DATA:
                        mapByKeyForMultiLineData.Add(cKey, stText);
                        break;
                    case CTYP_DEFAULT_VIEW:
                        break;
                }
            }

            string st = "";
            foreach (string cKey in mapViewDataCnt.Keys)
            {
                int dataCnt = mapViewDataCnt[cKey];

                st = "";
                for (int i = 1; i <= dataCnt; i++)
                {
                    string dataKey = cKey + CKEY_MULTI_LINE_HDR_SEPARATOR + i;
                    st += mapByKeyForMultiLineData[dataKey];
                }

                if (mapView.ContainsKey(cKey))
                {
                    mapView[cKey] = st;
                }
                else mapView.Add(cKey, st);
            }

            return mapView;
        }
    }
}
