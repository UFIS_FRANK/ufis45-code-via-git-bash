using System;
using System.Collections;
using Ufis.Data;
using Ufis.Utils;

using System.Threading;

namespace Ufis.Status_Manager.Ctrl
{
	/// <summary>
	/// Worker Class to identify conflicts
	/// This class also provides some commonly needed
	/// helper functionality
	/// </summary>
	public class CFC
	{
		private static string currentViewName = "";
		private static int myViewIdx = -1;
		public static int TimerIntervalConflicts = 1;
		private static IDatabase myDB = null;
		private static ITable myOST = null;
		private static ITable myAFT = null;
		private static ITable mySDE = null;
		private static ITable myALT = null;
		private static ITable myNAT = null;
		private static ITable myHSS = null;
		private static ITable mySRH = null;
		private static AxTABLib.AxTAB myViewsTab = null;
		public static AxAATLOGINLib.AxAatLogin LoginControl = null;


		public CFC()
		{
		}

        public static string GetHssStr()
        {
            return myViewsTab.GetFieldValue(myViewIdx, "HSS");
        }

        public static string GetAltStr()
        {
            return myViewsTab.GetFieldValue(myViewIdx, "ALT");
        }

        public static string GetNatStr()
        {
            return myViewsTab.GetFieldValue(myViewIdx, "NAT");
        }

        public static int CurrentViewIdx
        {
            get { return myViewIdx; }
        }
	
		public static string CurrentViewName
		{
			get{ return currentViewName;}
			set
			{
				//if(value != currentViewName)//Do it only if changed!!
				{
					bool blFound = false;
					currentViewName = value;
					myViewIdx = -1;
					for(int i = 0; i < myViewsTab.GetLineCount() && blFound == false; i++)
					{
						if(currentViewName == myViewsTab.GetFieldValue(i, "NAME"))
						{
							myViewIdx = i;
							blFound = true;
						}
					}
                    CtrlCfl.GetInstance().InitConflictStatusCount();
                    //DE.Call_ViewNameChanged();
                    Thread th = new Thread(new ThreadStart(DE.Call_ViewNameChanged));
                    th.IsBackground = true;
                    th.Start();
				}
			}
		}
		public static void MyInit(AxTABLib.AxTAB pTabViews)
		{
			myViewsTab = pTabViews;
			myDB = UT.GetMemDB(); 
			myOST = myDB["OST"];
			myAFT = myDB["AFT"];
			mySDE = myDB["SDE"];
			myALT = myDB["ALT"];
			myNAT = myDB["NAT"];
			myHSS = myDB["HSS"];
			mySRH = myDB["SRH"];

            //DE.OnConflicTimerElapsed +=new Status_Manager.DE.ConflicTimerElapsed(DE_OnConflicTimerElapsed);
            //AM 20080425 - Nothing inside DE_OnConflicTimerElapsed. Commented Out.
		}
        //private static void DE_OnConflicTimerElapsed()
        //{//AM 20080425 - Nothing inside. Commented Out.

        //}

        //public static bool HasFlightStatusConflicts(string strAftUrno, ref int CflCount, 
        //                                            ref int AckCount, ref int TotalCount, ref int OSTCount)
        //{//AM 20080425 - Move to CtrlCfl
        //    bool blRet = false;
        //    try
        //    {
        //        UT.LogMsg("CFC:HasFlightStatusConflicts: b4 AFT,OST Acq Reader Lock");
        //        myAFT.Lock.AcquireReaderLock(Timeout.Infinite);
        //        myOST.Lock.AcquireReaderLock(Timeout.Infinite);
        //        UT.LogMsg("CFC:HasFlightStatusConflicts: af AFT,OST Acq Reader Lock");

        //        IRow[] ostRows = myOST.RowsByIndexValue("UAFT", strAftUrno);
        //        IRow[] aftRows = myAFT.RowsByIndexValue("URNO", strAftUrno);
        //        CflCount = 0;
        //        AckCount = 0;
        //        TotalCount = 0;
        //        OSTCount = ostRows.Length;
        //        if (ostRows.Length > -1)
        //        {
        //            if (currentViewName == "<DEFAULT>" || myViewIdx < 0)
        //            {
        //                for (int i = 0; i < ostRows.Length; i++)
        //                {
        //                    TotalCount++;
        //                    if (ostRows[i]["COTY"] == "N")
        //                    {
        //                        CflCount++;
        //                    }
        //                    if (ostRows[i]["COTY"] == "A")
        //                    {
        //                        AckCount++;
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                if (aftRows.Length > 0)
        //                {
        //                    IRow[] altRows;
        //                    IRow[] natRows;
        //                    bool altCheck = false;
        //                    bool natCheck = false;

        //                    string strViewHSS = myViewsTab.GetFieldValue(myViewIdx, "HSS");
        //                    string strViewALT = myViewsTab.GetFieldValue(myViewIdx, "ALT");
        //                    string strViewNAT = myViewsTab.GetFieldValue(myViewIdx, "NAT");

        //                    string strAftAlc = aftRows[0]["ALC2"];
        //                    string strAftTTYP = aftRows[0]["TTYP"];
        //                    if (strAftAlc == "SQ")
        //                    {
        //                        strAftAlc = "SQ";
        //                    }
        //                    if (strViewALT != "")
        //                    {
        //                        altRows = myALT.RowsByIndexValue("ALC2", strAftAlc);
        //                        if (altRows.Length == 0)
        //                        {
        //                            strAftAlc = aftRows[0]["ALC3"];
        //                            altRows = myALT.RowsByIndexValue("ALC3", strAftAlc);
        //                        }
        //                        if (altRows.Length > 0)
        //                        {
        //                            if (strViewALT.IndexOf(altRows[0]["URNO"], 0, strViewALT.Length) > -1)
        //                            {
        //                                altCheck = true;
        //                            }
        //                        }
        //                    }
        //                    else { altCheck = true; }
        //                    if (strViewNAT != "")
        //                    {
        //                        natRows = myNAT.RowsByIndexValue("TTYP", strAftTTYP.Trim());
        //                        if (natRows.Length > 0)
        //                        {
        //                            if (strViewNAT.IndexOf(natRows[0]["URNO"].Trim(), 0, strViewNAT.Length) > -1)
        //                            {
        //                                natCheck = true;
        //                            }
        //                        }
        //                    }
        //                    else { natCheck = true; }
        //                    //if flight must be checke iterate through the statuses for this flight
        //                    //otherwise no conflict is to be detected
        //                    if (natCheck == true && altCheck == true)
        //                    {
        //                        if (strViewHSS != "")// check only for the specified status sections
        //                        {
        //                            //Hier weiter machen
        //                            for (int i = 0; i < ostRows.Length; i++)
        //                            {
        //                                if (strViewHSS.IndexOf(ostRows[i]["UHSS"], 0, strViewHSS.Length) > -1)
        //                                {
        //                                    TotalCount++;
        //                                    if (ostRows[i]["COTY"] == "N")
        //                                    {
        //                                        CflCount++;
        //                                    }
        //                                    if (ostRows[i]["COTY"] == "A")
        //                                    {
        //                                        AckCount++;
        //                                    }
        //                                }
        //                            }
        //                        }
        //                        else
        //                        {
        //                            for (int i = 0; i < ostRows.Length; i++)
        //                            {
        //                                TotalCount++;
        //                                if (ostRows[i]["COTY"] == "N")
        //                                {
        //                                    CflCount++;
        //                                }
        //                                if (ostRows[i]["COTY"] == "A")
        //                                {
        //                                    AckCount++;
        //                                }
        //                            }
        //                        }
        //                    }
        //                }//if(aftRows.Length > 0)
        //            }// else of if(currentViewName == "<DEFAULT>" || myViewIdx < 0)
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        UT.LogMsg("CFC:HasFlightStatusConflicts:Err:" + ex.Message + Environment.NewLine + ex.StackTrace);
        //        throw ex;
        //    }
        //    finally
        //    {
        //        UT.LogMsg("CFC:HasFlightStatusConflicts: b4 AFT,OST Rel Reader Lock");
        //        try { if (myOST.Lock.IsReaderLockHeld) myOST.Lock.ReleaseReaderLock(); }
        //        catch { }
        //        try { if (myAFT.Lock.IsReaderLockHeld) myAFT.Lock.ReleaseReaderLock(); }
        //        catch { }
        //        UT.LogMsg("CFC:HasFlightStatusConflicts: af AFT,OST Rel Reader Lock");
        //    }
        //    if(AckCount > 0 || CflCount > 0)
        //        blRet = true;
        //    return blRet;
        //}

        //public static string GetLastActionStatusForFlight(string strAftUrno)
        //{//AM 20080425 - Move to CtrlCfl
        //    string strRet = "";
        //    int idx = -1;
        //    DateTime datBest = new DateTime(1900, 1, 1, 1, 1, 1, 1);
        //    DateTime datConu = new DateTime(1900, 1, 1, 1, 1, 1, 1);
        //    DateTime datCona = new DateTime(1900, 1, 1, 1, 1, 1, 1);
        //    DateTime datLast = new DateTime(1900, 1, 1, 1, 1, 1, 1);
        //    try
        //    {
        //        UT.LogMsg("CFC:GetLastActionStatusForFlight: b4 OST acq Reader Lock");
        //        myOST.Lock.AcquireReaderLock(Timeout.Infinite);
        //        mySDE.Lock.AcquireReaderLock(Timeout.Infinite);
        //        UT.LogMsg("CFC:GetLastActionStatusForFlight: af OST acq Reader Lock");

        //        IRow[] ostRows = myOST.RowsByIndexValue("UAFT", strAftUrno);
        //        for (int i = 0; i < ostRows.Length; i++)
        //        {
        //            if (ostRows[i]["CONU"].Trim() != "" || ostRows[i]["CONA"].Trim() != "")
        //            {
        //                if (ostRows[i]["CONU"].Trim() != "" && ostRows[i]["CONA"].Trim() != "")
        //                {
        //                    datConu = UT.CedaFullDateToDateTime(ostRows[i]["CONU"].Trim());
        //                    datCona = UT.CedaFullDateToDateTime(ostRows[i]["CONA"].Trim());
        //                    if (datConu >= datCona)
        //                        datBest = datConu;
        //                    else
        //                        datBest = datCona;
        //                }
        //                else if (ostRows[i]["CONU"].Trim() != "" && ostRows[i]["CONA"].Trim() == "")
        //                {
        //                    datBest = UT.CedaFullDateToDateTime(ostRows[i]["CONU"].Trim());
        //                }
        //                else if (ostRows[i]["CONU"].Trim() == "" && ostRows[i]["CONA"].Trim() != "")
        //                {
        //                    datBest = UT.CedaFullDateToDateTime(ostRows[i]["CONA"].Trim());
        //                }
        //                if (datBest > datLast)
        //                {
        //                    idx = i;
        //                    datLast = datBest;
        //                }
        //            }
        //        }

        //        if (idx > -1)
        //        {
        //            IRow[] sdeRow = mySDE.RowsByIndexValue("URNO", ostRows[idx]["USDE"]);
        //            if (sdeRow.Length > 0)
        //            {
        //                string strUSRH = ostRows[idx]["USRH"];
        //                IRow[] srhRows = mySRH.RowsByIndexValue("URNO", strUSRH);
        //                if (srhRows.Length > 0)
        //                {
        //                    strRet = srhRows[0]["NAME"] + " => " + sdeRow[0]["NAME"];
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LogExceptionMsg(ex, "GetLastActionStatusForFlight");
        //    }
        //    finally
        //    {
        //        UT.LogMsg("CFC:GetLastActionStatusForFlight: b4 OST rel Reader Lock");
        //        try { if (mySDE.Lock.IsReaderLockHeld) mySDE.Lock.ReleaseReaderLock(); }
        //        catch { }
        //        try { if (myOST.Lock.IsReaderLockHeld) myOST.Lock.ReleaseReaderLock(); }
        //        catch { }
        //        UT.LogMsg("CFC:GetLastActionStatusForFlight: af OST rel Reader Lock");

        //    }
        //    return strRet;
        //}

        private static void LogExceptionMsg(Exception ex, string methodName)
        {
            UT.LogMsg("CFC:" + methodName + ":" + ex.Message + Environment.NewLine + ex.StackTrace);
            throw ex;
        }

        //public static ArrayList GetStatusConflicsForFlight(string strAftUrno)
        //{//AM 20080425 - Get Status Conflicts for a Flight
        //                ArrayList arrStat = null;
        //    int ackCnt = 0;
        //    int cflCnt = 0;
        //    int totCnt = 0;
        //    int ostCnt = 0;
        //    string lastStat = "";

        //    bool hasCfl = Ctrl.CtrlCfl.GetInstance().HasConflict(strAftUrno,
        //        ref cflCnt, ref ackCnt, ref totCnt, ref ostCnt, ref lastStat, arrStat);
        //    return arrStat;
        //}
        //public static ArrayList GetStatusConflicsForFlight(string strAftUrno)
        //{//AM 20080425 - Move to CtrlCfl
        //    ArrayList arr = new ArrayList();
        //    try
        //    {
        //        UT.LogMsg("CFC:GetStatusConflicsForFlight: b4 OST acq Reader Lock");
        //        myOST.Lock.AcquireReaderLock(Timeout.Infinite);
        //        mySDE.Lock.AcquireReaderLock(Timeout.Infinite);
        //        mySRH.Lock.AcquireReaderLock(Timeout.Infinite);
        //        UT.LogMsg("CFC:GetStatusConflicsForFlight: af OST acq Reader Lock");
        //        IRow[] ostRows = myOST.RowsByIndexValue("UAFT", strAftUrno);
        //        string strCOTY = "";
        //        string strValues = "";
        //        DateTime dat;
        //        string strUSRH = "";


        //        if (UT.IsTimeInUtc == true)
        //            dat = CFC.GetUTC();// DateTime.UtcNow;
        //        else
        //            dat = DateTime.Now;
        //        if (ostRows.Length > -1)
        //        {
        //            for (int i = 0; i < ostRows.Length; i++)
        //            {
        //                strCOTY = ostRows[i]["COTY"];
        //                if (strCOTY == "N" || strCOTY == "A")
        //                {
        //                    IRow[] sdeRow = mySDE.RowsByIndexValue("URNO", ostRows[i]["USDE"]);
        //                    if (sdeRow.Length > 0)
        //                    {
        //                        strUSRH = ostRows[i]["USRH"];
        //                        IRow[] srhRows = mySRH.RowsByIndexValue("URNO", strUSRH);
        //                        if (srhRows.Length > 0)
        //                        {
        //                            strValues = srhRows[0]["NAME"] + " => " + sdeRow[0]["NAME"];
        //                        }
        //                        arr.Add(strValues);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LogExceptionMsg(ex, "GetStatusConflicsForFlight");
        //    }
        //    finally
        //    {
        //        UT.LogMsg("CFC:GetStatusConflicsForFlight: b4 OST rel Reader Lock");
        //        try { if (mySRH.Lock.IsReaderLockHeld) mySRH.Lock.ReleaseReaderLock(); }
        //        catch { }

        //        try { if (mySDE.Lock.IsReaderLockHeld) mySDE.Lock.ReleaseReaderLock(); }
        //        catch { }

        //        try { if (myOST.Lock.IsReaderLockHeld) myOST.Lock.ReleaseReaderLock(); }
        //        catch { }
        //        UT.LogMsg("CFC:GetStatusConflicsForFlight: af OST acq Reader Lock");
        //    }
        //    return arr;
        //}

		private static CtrlIni ctrlIni = CtrlIni.GetInstance();

		public static bool IsFlightPassFilter(IRow aftRow)
		{
			bool passFilter = true;
			if (!ctrlIni.IsFilterFlightOn)
			{
				passFilter = true;
			}
			else if (currentViewName == "<DEFAULT>" || myViewIdx < 0)
			{
				passFilter = true;
			}
			else
			{
				#region Check Whether need to filter out.
				IRow[] altRows;
				IRow[] natRows;
				bool altCheck = false;
				bool natCheck = false;

				string strViewALT = myViewsTab.GetFieldValue(myViewIdx, "ALT");
				string strViewNAT = myViewsTab.GetFieldValue(myViewIdx, "NAT");

				if (aftRow != null)
				{
					string strAftAlc = aftRow["ALC2"];
					string strAftTTYP = aftRow["TTYP"];

					if (strViewALT != "")
					{
						altRows = myALT.RowsByIndexValue("ALC2", strAftAlc);
						if (altRows.Length == 0)
						{
							strAftAlc = aftRow["ALC3"];
							altRows = myALT.RowsByIndexValue("ALC3", strAftAlc);
						}
						if (altRows.Length > 0)
						{
							if (strViewALT.IndexOf(altRows[0]["URNO"], 0, strViewALT.Length) > -1)
							{
								altCheck = true;
							}
						}
					}
					else { altCheck = true; }

					if (altCheck && strViewNAT != "")
					{
						natRows = myNAT.RowsByIndexValue("TTYP", strAftTTYP);
						if (natRows.Length > 0)
						{
							if (strViewNAT.IndexOf(natRows[0]["URNO"], 0, strViewNAT.Length) > -1)
							{
								natCheck = true;
							}
						}
					}
					else { natCheck = true; }
				}//end if (aftRow != null)
				else
				{//aftRow is null
					natCheck = true;
					altCheck = true;
				}

				//Now check the ost row
				if (natCheck == true && altCheck == true)
				{
					passFilter = true;
				}
				else
				{
					passFilter = false;
				}
				#endregion
			}
			return passFilter;
		}

		public static bool IsPassFilter(IRow ostRow, IRow aftRow)
		{
			bool blRet = false;
            
			if(currentViewName == "<DEFAULT>" || myViewIdx < 0)
			{
				blRet = true;
			}
			else
			{
				IRow [] altRows;
				IRow [] natRows;
				bool altCheck = false;
				bool natCheck = false;

				string strViewHSS = myViewsTab.GetFieldValue(myViewIdx, "HSS");
				string strViewALT = myViewsTab.GetFieldValue(myViewIdx, "ALT");
				string strViewNAT = myViewsTab.GetFieldValue(myViewIdx, "NAT");

				if(aftRow != null)
				{
					string strAftAlc = aftRow["ALC2"];
					string strAftTTYP = aftRow["TTYP"];
					//if(strAftAlc == "SQ")
					//{
					//    strAftAlc = "SQ";
					//}

					if(strViewALT != "")
					{
						altRows = myALT.RowsByIndexValue("ALC2", strAftAlc);
						if(altRows.Length == 0)
						{
							strAftAlc = aftRow["ALC3"];
							altRows = myALT.RowsByIndexValue("ALC3", strAftAlc);
						}
						if(altRows.Length > 0)
						{
							if(strViewALT.IndexOf(altRows[0]["URNO"], 0, strViewALT.Length) > -1)
							{
								altCheck = true;
							}
						}
					}
					else { altCheck = true; }

					if(strViewNAT != "")
					{
						natRows = myNAT.RowsByIndexValue("TTYP", strAftTTYP);
						if(natRows.Length > 0)
						{
							if(strViewNAT.IndexOf(natRows[0]["URNO"], 0, strViewNAT.Length) > -1)
							{
								natCheck = true;
							}
						}
					}
					else { natCheck = true; }
				}//end if (aftRow != null)
				else
				{//aftRow is null
					natCheck = true;
					altCheck = true;
				}

				//Now check the ost row
				if(natCheck == true && altCheck == true)
				{
					if(strViewHSS != "")// check only for the specified status sections
					{
						if(strViewHSS.IndexOf(ostRow["UHSS"], 0, strViewHSS.Length) > -1)
						{
							blRet = true;
						}
					}
					else
					{
						blRet = true;
					}
				}
			}// else of if(currentViewName == "<DEFAULT>" || myViewIdx < 0)

			return blRet;
		}

		public static DateTime GetUTC()
		{
			DateTime datRet = DateTime.Now;
			if(UT.UTCOffset == 0)
			{
				datRet = DateTime.Now;
			}
			else
			{
				datRet = DateTime.UtcNow;
			}
			return datRet;
		}
	}
}
