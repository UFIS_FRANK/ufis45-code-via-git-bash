﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Ufis.MVVM.ViewModel;
using Ufis.Data;
using Ufis.LoginWindow;
using Ufis.NotificationWindow;
using Ufis.Utilities;
using Ufis.AllocationPlan.DataAccess;
using Ufis.AllocationPlan.Helpers;
using Ufis.AllocationPlan.Shell;
using Ufis.Security;
using System.Windows.Media.Imaging;

namespace Ufis.AllocationPlan
{
    class App : Application
    {
        public App(string[] args)
        {
            InitializeComponent(args);
        }

        public event EventHandler BeforeMainWindowClosed;

        private void InitializeComponent(string[] args)
        {
            //Set current application infomation
            AppInfo appInfo = HpAppInfo.Current;
            appInfo.ProductCode = "FIPSReport";
            appInfo.ProductTitle = "Allocation Plan Report";
            appInfo.Description = "";
            appInfo.Image = new BitmapImage(new Uri("../Images/BDPSUIF_128x128.png", UriKind.Relative));
            //Registration string to register at function list
            //If New Report has been increase, please see the next line and follow that syntex
            //SUBDirectory, FunctionCode, FunctionName, Type (B=Button, I=Menu("So far I only know it")) Status
            appInfo.RegistrationString = "FIPSReport,InitModu,InitModu,Initialisieren (InitModu),B,-" +
                ",Report Menus,MALLPN,Allocation Plan,I,1";

            //Create shell window
            ShellView shell = new ShellView();
            this.MainWindow = shell;
            shell.BeforeClosed += OnMainWindowBeforeClosed;

            // Create the ViewModel to which 
            // the shell window binds.
            var viewModel = new ShellViewModel();

            // When the ViewModel asks to be closed, 
            // close the window.
            EventHandler handler = null;
            handler = (sender, e) =>
            {
                viewModel.RequestClose -= handler;
                shell.Close();
            };
            viewModel.RequestClose += handler;

            //check the prequisite requirements, if all ok
            //shows the login dialog
            string connectionError = CheckConnection();
            WorkspaceViewModel workspace = null;
            if (string.IsNullOrEmpty(connectionError))
            {
                IUfisUserAuthentication userAuthentication = DlUfisData.Current.UserAuthenticationManager;
                //check the arguments
                if (args.Length >0)
                {
                    string[] arrSplitedArgs = args[0].Split('|');
                    string strFormId = arrSplitedArgs[0].Split(',')[0];
                    string userId = arrSplitedArgs[1].Split(',')[0];
                    string sLocalUTC = arrSplitedArgs[2].Split(',')[0];

                    switch (strFormId)
                    {
                        case "ALPR":
                            HpUser.ActiveUser = new Ufis.Entities.EntUser() { UserId = userId };
                            HpUser.DirectAccess = true;
                            HpUser.TLocalUTC = sLocalUTC;        
                            DlUfisData.Current.DataContext.Connection.UserName = userId;

                            workspace = new AllocationPlanViewModel();

                            break;
                        default:
                            workspace = new NotificationViewModel()
                            {
                                NotificationInfo = new MessageInfo()
                                {
                                    InfoText = string.Format("Invalid comand found: {0}", strFormId),
                                    Severity = MessageInfo.MessageInfoSeverity.Error
                                },
                                CanContinue = false
                            };
                            break;
                    }
                }
               
                else
                {
                    LoginViewModel loginViewModel;
#if !XABPP
                    loginViewModel = new LoginViewModel(appInfo, userAuthentication, LoginViewModel.GetLastUser()) { AutoSaveLastUser = true };
#else
                loginViewModel = new LoginViewModel(appInfo, userAuthentication, false);
#endif
                    loginViewModel.ProjectInfo = DlUfisData.Current.ProjectInfo;
                    workspace = loginViewModel;
                }
            }
            //otherwise shows ErrorScreen
            else
            {
                workspace = new NotificationViewModel()
                {
                    NotificationInfo = new MessageInfo()
                    {
                        InfoText = connectionError,
                        Severity = MessageInfo.MessageInfoSeverity.Error
                    },
                    CanContinue = false
                };
            }
            viewModel.Workspace = workspace;
            //Check if LoginviewModel and Login Success cause of SSO
            //Skip the login windows and continue to Application.
            //Phyoe Khaing Min
            //24.Apr.2012
            if ((workspace is LoginViewModel) && ((LoginViewModel)workspace).LoginSucceeded)
            {
                workspace.CloseCommand.Execute(null);
            }
            // Allow all controls in the window to 
            // bind to the ViewModel by setting the 
            // DataContext, which propagates down 
            // the element tree.
            shell.DataContext = viewModel;

            shell.Show();
        }

        private string CheckConnection()
        {
            const string CONN_ERROR = "We could not connect to the application server(s).";
            const string CONN_ERROR_UNABLE_TO_CONNECT = "Could not connect to following server(s):";
            const string CONN_ERROR_NO_CONFIG = "Please make sure that you have the Ceda.ini configuration file in your system.";

            string strConnErr = null;

            string strAppName = HpAppInfo.Current.ProductCode;
            EntityDataContextBase dataContext = DlUfisData.CreateInstance(strAppName).DataContext;
            if (dataContext == null)
            {
                strConnErr = String.Format("{0}\n{1}", CONN_ERROR, CONN_ERROR_NO_CONFIG);
            }
            else
            {
                ConnectionBase connection = dataContext.Connection;
                if (connection == null)
                    strConnErr = String.Format("{0}\n{1}\n{2}", CONN_ERROR, CONN_ERROR_UNABLE_TO_CONNECT, connection.ServerList);
            }

            return strConnErr;
        }

        void OnMainWindowBeforeClosed(object sender, EventArgs e)
        {
            if (BeforeMainWindowClosed != null)
                BeforeMainWindowClosed(this, EventArgs.Empty);
        }
    }

    static class Program
    {
        static App _currentApp;

        [STAThread]
        static void Main(string[] args)
        {
            App app = new App(args);
            _currentApp = app;
            app.BeforeMainWindowClosed += OnBeforeMainWindowClosed;
            app.DispatcherUnhandledException += OnDispatcherUnhandledException;
            app.Run();
        }

        static void OnBeforeMainWindowClosed(object sender, EventArgs e)
        {
            DlUfisData dlUfisData = DlUfisData.Current;
            if (dlUfisData != null)
            {
                EntityDataContextBase dataContext = dlUfisData.DataContext;
                if (dataContext != null)
                {
                    ConnectionBase connection = dataContext.Connection;
                    if (connection != null)
                    {
                        if (connection.State == ConnectionState.Open) connection.Close();
                        connection = null;
                    }
                    dataContext.Dispose();
                }
            }
        }

        static void OnDispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            string strException = "Application is going to close.\nException occured:\n";
            Exception CurrentException = e.Exception;
            while (CurrentException != null)
            {
                strException += CurrentException.Message + "\n";
                CurrentException = CurrentException.InnerException;
            }

            App app = _currentApp;
            ShellView shell = (ShellView)app.MainWindow;
            ShellViewModel viewModel = (ShellViewModel)shell.DataContext;
            WorkspaceViewModel workspace = new NotificationViewModel()
            {
                NotificationInfo = new MessageInfo()
                {
                    InfoText = strException,
                    Severity = MessageInfo.MessageInfoSeverity.Error
                },
                CanContinue = false
            };
            viewModel.Workspace = workspace;

            e.Handled = true;
        }
    }
}
