﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Data;
using Ufis.Entities;
using Ufis.Data.Ceda;

namespace Ufis.AllocationPlan.DataAccess
{
    public class DlUserDefinedLayout
    {
        private static EntityCollectionBase<EntDbUserDefinedLayout> _userLayouts;

        public static EntityCollectionBase<EntDbUserDefinedLayout> UserLayouts
        {
            get
            {
                if (_userLayouts == null)
                    _userLayouts = LoadUserLayouts();
                return _userLayouts;
            }
        }

        public static ICollection<EntDbUserDefinedLayout> GetFilteredView(string basicDataName)
        {
            return (from ul in UserLayouts
                    where ul.Section == basicDataName
                    select ul).ToList();
        }

        private static EntityCollectionBase<EntDbUserDefinedLayout> LoadUserLayouts()
        {
            EntDbUserDefinedLayout userLayout = new EntDbUserDefinedLayout() 
            { 
                Urno = 1,
                ApplicationName = "BDPS-UIF",
                //Section = "Flight identifier mappings", 
                Section = "Allocation Plan",
                Name = "All records",
                AccessRights = "*",
                ValidFrom = DateTime.Now.AddDays(-1)
            };
            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;
            EntityCollectionBase<EntDbUserDefinedLayout> userLayouts = new CedaEntityCollection<EntDbUserDefinedLayout>(dataContext);
            userLayouts.Add(userLayout);

            return userLayouts;
        }
    }
}
