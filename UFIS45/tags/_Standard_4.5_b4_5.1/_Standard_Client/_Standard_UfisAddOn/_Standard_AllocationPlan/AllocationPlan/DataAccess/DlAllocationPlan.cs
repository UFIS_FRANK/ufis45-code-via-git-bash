﻿using System;
using Ufis.Data;
using System.Linq;
using Ufis.Entities;
using Ufis.Data.Ceda;
using System.Collections.Generic;
using Ufis.AllocationPlan.Helpers;

namespace Ufis.AllocationPlan.DataAccess
{
    public class DlAllocationPlan
    {
        /// <summary>
        /// Load Flight List by date Range for Arrival (STOA)/(ETAI) Departure (STOD)/(ETDI)
        /// </summary>
        /// <param name="sFDate">System.String containing sFDate</param>
        /// <param name="sTdate">System.String containing sTdate</param>
        /// <returns>EntityCollection for Flight</returns>
        public static EntityCollectionBase<EntDbFlight> LoadArrivalFlightListByDateRange(DateTime sFDate, DateTime sTdate, string stype)
        {
            string WHERE_CLAUSE = "";
            const string FIELD_LIST = "[RelationKey],[Urno],[FullFlightNumber],[OriginAirportIATACode],[StandardTimeOfArrival],[StandardTimeOfDeparture],[AircraftICAOCodeModified],[AircraftIATACode],[DestinationAirportIATACode]," +
                                        "[RegistrationNumber],[PositionOfArrival],[PositionOfDeparture],[GateOfArrival1],[GateOfDeparture1],[Belt1],[TotalNumberOfPassengers],[CallSign],[NatureCode],[OperationalType],[CheckInCFrom],[CheckInCTo]";

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;

            if (HpUser.TLocalUTC == "L")
            {
                dataContext.DefaultTimeZoneInfo = dataContext.GetTimeZoneInfo();
            }
            else
            {
                dataContext.DefaultTimeZoneInfo = TimeZoneInfo.Utc;
            }

            CedaDataConverter objCedaDataConverter = new CedaDataConverter();

            string sFromDate = objCedaDataConverter.DataItemToCedaItem(sFDate, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);
            string sToDate = objCedaDataConverter.DataItemToCedaItem(sTdate, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);

            switch
                 (stype)
            {
                case "A": WHERE_CLAUSE = String.Format("WHERE [OperationalType] = 'O' AND [ArrivalDepartureId] = 'A' AND ([StandardTimeOfArrival] BETWEEN '{0}' AND '{1}' OR [BestEstimatedArrivalTime] BETWEEN '{0}' AND '{1}')", sFromDate, sToDate);                    
                    break;
                case "D": WHERE_CLAUSE = String.Format("WHERE [OperationalType] = 'O' AND [ArrivalDepartureId] = 'D' AND ([StandardTimeOfDeparture] BETWEEN '{0}' AND '{1}' OR [BestEstimatedDepartureTime] BETWEEN '{0}' AND '{1}')", sFromDate, sToDate);                    
                    break;
            }
            EntityCollectionBase<EntDbFlight> objFlightList = null;           

            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntDbFlight),
                AttributeList = FIELD_LIST,
                EntityWhereClause = WHERE_CLAUSE
                
            };
            if (HpUser.TLocalUTC == "L")
            {
                dataContext.DefaultTimeZoneInfo = dataContext.GetTimeZoneInfo();
            }
            else
            {
                dataContext.DefaultTimeZoneInfo = TimeZoneInfo.Utc;
            }
            objFlightList = dataContext.OpenEntityCollection<EntDbFlight>(command);

            return objFlightList;
        }
        /// <summary>
        /// Load Flight List by date Range for Arrival (ETAI)/ Departure (ETDI)
        /// </summary>
        /// <param name="sFDate">System.String containing sFDate</param>
        /// <param name="sTdate">System.String containing sTdate</param>
        /// <returns>EntityCollection for Flight</returns>
        public static EntityCollectionBase<EntDbFlight> LoadArrivalFlightListByEstimatedDateRange(DateTime sFDate, DateTime sTdate, string stype)
        {
            string WHERE_CLAUSE = "";
            const string FIELD_LIST = "[RelationKey],[Urno],[FullFlightNumber],[OriginAirportIATACode],[StandardTimeOfArrival],[StandardTimeOfDeparture],[AircraftICAOCodeModified],[AircraftIATACode],[DestinationAirportIATACode]," +
                                        "[RegistrationNumber],[PositionOfArrival],[PositionOfDeparture],[GateOfArrival1],[GateOfDeparture1],[Belt1],[TotalNumberOfPassengers],[CallSign],[NatureCode],[OperationalType],[CheckInCFrom],[CheckInCTo]";

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;

            if (HpUser.TLocalUTC == "L")
            {
                dataContext.DefaultTimeZoneInfo = dataContext.GetTimeZoneInfo();
            }
            else
            {
                dataContext.DefaultTimeZoneInfo = TimeZoneInfo.Utc;
            }

            CedaDataConverter objCedaDataConverter = new CedaDataConverter();

            string sFromDate = objCedaDataConverter.DataItemToCedaItem(sFDate, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);
            string sToDate = objCedaDataConverter.DataItemToCedaItem(sTdate, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);

            switch
                 (stype)
            {
                case "A": WHERE_CLAUSE = String.Format("WHERE [OperationalType] = 'O' AND [ArrivalDepartureId] = 'A' AND [BestEstimatedArrivalTime] BETWEEN '{0}' AND '{1}'", sFromDate, sToDate);
                    break;
                case "D": WHERE_CLAUSE = String.Format("WHERE [OperationalType] = 'O' AND [ArrivalDepartureId] <> 'A' AND [BestEstimatedDepartureTime] BETWEEN '{0}' AND '{1}'", sFromDate, sToDate);
                    break;
            }
            EntityCollectionBase<EntDbFlight> objFlightList = null;

            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntDbFlight),
                AttributeList = FIELD_LIST,
                EntityWhereClause = WHERE_CLAUSE

            };
            if (HpUser.TLocalUTC == "L")
            {
                dataContext.DefaultTimeZoneInfo = dataContext.GetTimeZoneInfo();
            }
            else
            {
                dataContext.DefaultTimeZoneInfo = TimeZoneInfo.Utc;
            }
            objFlightList = dataContext.OpenEntityCollection<EntDbFlight>(command);

            return objFlightList;
        }
        /// <summary>
        /// Get Specific Flight Info by Relation Key (RKEY)
        /// </summary>
        /// <param name="lRKEY">system.long containing lRKEY</param>
        /// <returns>EntityCollection for Flight</returns>
        public static EntityCollectionBase<EntDbFlight> GetDepartureFlightByRKEY(int lRKEY, int lUrno)
        {
            const string FIELD_LIST = "[RelationKey],[Urno],[FullFlightNumber],[DestinationAirportIATACode],[StandardTimeOfArrival],[StandardTimeOfDeparture],[AircraftICAOCodeModified],[AircraftIATACode]," +
                                        "[RegistrationNumber],[PositionOfDeparture],[GateOfDeparture1],[Belt1],[TotalNumberOfPassengers],[CallSign],[NatureCode],[OperationalType],[CheckInCFrom],[CheckInCTo]";
            string WHERE_CLAUSE = String.Format("WHERE [RelationKey] = {0} AND [Urno] <> {1}", lRKEY, lUrno);

            EntityCollectionBase<EntDbFlight> objRKeyFlightList = null;

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;
            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntDbFlight),
                AttributeList = FIELD_LIST,
                EntityWhereClause = WHERE_CLAUSE
            };
            if (HpUser.TLocalUTC == "L")
            {
                dataContext.DefaultTimeZoneInfo = dataContext.GetTimeZoneInfo();
            }
            else
            {
                dataContext.DefaultTimeZoneInfo = TimeZoneInfo.Utc;
            }
            objRKeyFlightList = dataContext.OpenEntityCollection<EntDbFlight>(command);

            return objRKeyFlightList;
        }
        /// <summary>
        /// Load Flight List by date Range for Departure
        /// </summary>
        /// <param name="sFDate">System.String containing sFDate</param>
        /// <param name="sTdate">System.String containing sTdate</param>
        /// <returns>EntityCollection for Flight</returns>
        public static EntityCollectionBase<EntDbFlight> LoadDepartureFlightListByDateRange(DateTime sFDate, DateTime sTdate)
        {
            const string FIELD_LIST = "[RelationKey],[Urno],[FullFlightNumber],[DestinationAirportIATACode],[StandardTimeOfArrival],[StandardTimeOfDeparture],[AircraftICAOCodeModified],[AircraftIATACode]," +
                                        "[RegistrationNumber],[PositionOfDeparture],[GateOfArrival1],[Belt1],[TotalNumberOfPassengers],[CallSign],[NatureCode],[OperationalType],[CheckInCFrom],[CheckInCTo]";
            

            EntityCollectionBase<EntDbFlight> objDepFlightList = null;

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;

            if (HpUser.TLocalUTC == "L")
            {
                dataContext.DefaultTimeZoneInfo = dataContext.GetTimeZoneInfo();
            }
            else
            {
                dataContext.DefaultTimeZoneInfo = TimeZoneInfo.Utc;
            }

            CedaDataConverter objCedaDataConverter = new CedaDataConverter();

            string sFromDate = objCedaDataConverter.DataItemToCedaItem(sFDate, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);
            string sToDate = objCedaDataConverter.DataItemToCedaItem(sTdate, typeof(DateTime), dataContext.DefaultTimeZoneInfo, null, 14, false);
            
            string WHERE_CLAUSE = String.Format("WHERE [OperationalType] = 'O' AND [StandardTimeOfDeparture] BETWEEN '{0}' AND '{1}' ", sFromDate, sToDate);

            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntDbFlight),
                AttributeList = FIELD_LIST,
                EntityWhereClause = WHERE_CLAUSE
            };
            
            objDepFlightList = dataContext.OpenEntityCollection<EntDbFlight>(command);

            return objDepFlightList;
        }
        /// <summary>
        /// Get Check In Counter Allocation
        /// </summary>
        /// <param name="sFlightNo">System.string containing sFlightNo</param>
        /// <param name="lFlightUrno">System.string containing sFlightUrno</param>
        /// <returns>Return Entity Collection object of EntDbCheckInCounterAllocation</returns>
        public static EntityCollectionBase<EntDbCheckInCounterAllocation> GetCheckInCounter(long lFlightUrno)
        {
            const string FIELD_LIST = "[FLightNo],[FLightURNo],[CheckInCounter],[FixCheckInCounter],[TerminalCheckInCounter]";
            string WHERE_CLAUSE = String.Format("WHERE [FLightURNo] = {0} ORDER BY [CheckInCounter]", lFlightUrno);
            

            EntityCollectionBase<EntDbCheckInCounterAllocation> objCheckInCounter = null;

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;
            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntDbCheckInCounterAllocation),
                AttributeList = FIELD_LIST,
                EntityWhereClause = WHERE_CLAUSE
            };

            objCheckInCounter = dataContext.OpenEntityCollection<EntDbCheckInCounterAllocation>(command);

            return objCheckInCounter;
        }
        /// <summary>
        /// Get All Check In Counter Allocation
        /// </summary>
        /// <returns>Return Entity Collection object of EntDbCheckInCounterAllocation</returns>
        public static EntityCollectionBase<EntDbCheckInCounterAllocation> GetCheckInCounters()
        {
            const string FIELD_LIST = "[FLightNo],[FLightURNo],[CheckInCounter],[FixCheckInCounter],[TerminalCheckInCounter]";           

            EntityCollectionBase<EntDbCheckInCounterAllocation> objCheckInCounters = null;

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;
            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntDbCheckInCounterAllocation),
                AttributeList = FIELD_LIST
            };

            objCheckInCounters = dataContext.OpenEntityCollection<EntDbCheckInCounterAllocation>(command);

            return objCheckInCounters;
        }
        /// <summary>
        /// Get Parameter Value
        /// </summary>
        /// <param name="TXID">System.string containing TXID</param>
        /// <returns>Return Entitycollectionbase containing EntDbParameters</returns>
        public static EntityCollectionBase<EntDbParameter> GetParameterValue(string TXID)
        {
            const string FIELD_LIST = "[Value],[ApplicationName],[CreateDate],[NameOfParameter],[ParameterIdentifier],[ParameterType],[TextIdentifier],[EvaluationMode]";
            string WHERE_CLAUSE = string.Format("WHERE [ParameterIdentifier] = '{0}'", TXID);

            EntityCollectionBase<EntDbParameter> objParameter = null;

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;
            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntDbParameter),
                AttributeList = FIELD_LIST,
                EntityWhereClause = WHERE_CLAUSE               
            };

            objParameter = dataContext.OpenEntityCollection<EntDbParameter>(command);

            return objParameter;
        }
        /// <summary>
        /// Get Login User Name
        /// </summary>
        /// <param name="USID">System.string containing USID</param>
        /// <returns>Return Entitycollectionbase containing EntDbSecured</returns>
        public static EntityCollectionBase<EntDbSecured> GetLoginUserName(string USID)
        {
            const string FIELD_LIST = "[Name],[Remark],[Status],[Type]";
            string WHERE_CLAUSE = string.Format("WHERE [UserId] = '{0}'", USID);

            EntityCollectionBase<EntDbSecured> objParameter = null;

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;
            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntDbSecured),
                AttributeList = FIELD_LIST,
                EntityWhereClause = WHERE_CLAUSE
            };

            objParameter = dataContext.OpenEntityCollection<EntDbSecured>(command);

            return objParameter;
        }
    }
}