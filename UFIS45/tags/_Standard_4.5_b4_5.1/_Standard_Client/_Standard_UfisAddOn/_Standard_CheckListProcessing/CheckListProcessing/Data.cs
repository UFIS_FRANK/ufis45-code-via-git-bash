using System;
using Ufis.Utils;
using Ufis.Data;

namespace CheckListProcessing
{
	/// <summary>
	/// Summary description for Data.
	/// </summary>
	public class Data
	{
		
		static IDatabase myDB = null;

		public Data()
		{
		}

		static public void LoadData(string strAftUrno)
		{
			string strWhere = "";

			myDB = UT.GetMemDB();
			//CLBTAB -- Processing
			ITable myTable = myDB["CLB"];
			if(myTable == null)
			{
				myTable = myDB.Bind("CLB", "CLB", "URNO,ORDE,SECT,TEXT", "10,3,32,64", "URNO,ORDE,SECT,TEXT");
			}
			else
			{
				myTable.Clear();
			}
			myTable.Load("");
			myTable.CreateIndex("URNO", "URNO");
			//CLPTAB -- Processing
			myTable = myDB["CLP"];
			if(myTable == null)
			{
				myTable = myDB.Bind("CLP", "CLP", "URNO,UAFT,CHEK,ORDE,SECT,TEXT,CHTM", 
					"10,10,1,3,32,64,14", 
					"URNO,UAFT,CHEK,ORDE,SECT,TEXT,CHTM");
			}
			else
			{
				myTable.Clear();
			}
			myTable.Load("WHERE UAFT = '" + strAftUrno + "'");
			myTable.CreateIndex("URNO", "URNO");
			myTable.CreateIndex("UAFT", "UAFT");
			//REMTAB -- Processing
			myTable = myDB["REM"];
			if(myTable == null)
			{
				myTable = myDB.Bind("REM", "REM", "URNO,RURN,APPL,RTAB,PURP,TEXT,CDAT,LSTU,USEC,USEU", 
					"10,10,8,3,128,256,14,14,32,32", 
					"URNO,RURN,APPL,RTAB,PURP,TEXT,CDAT,LSTU,USEC,USEU");
			}
			else
			{
				myTable.Clear();
			}
			strWhere = "WHERE RURN = '" + strAftUrno + "'"  + 
					   " AND APPL='CHKLSTPR' AND RTAB='AFT' AND PURP='REMA'";
			myTable.Load(strWhere);
			myTable.CreateIndex("URNO", "URNO");
			
			LoadAFT(strAftUrno);
		}

		static public void LoadAFT(string strAftUrno)
		{
			myDB = UT.GetMemDB();
			ITable myTable = myDB["AFT"];
			if(myTable == null)
			{
				myTable = myDB.Bind("AFT", 
					"AFT", 
					"URNO,RKEY,FTYP,ADID,FLNO,GTA1,GTD1,GTA2,GTD2,LAND,ONBS,PSTA,PSTD,STOA,STOD,TIFA,TIFD,TISA,TISD,TMOA,TTYP,ORG3,DES3,FLTI,ETAI,ETDI,ONBL,OFBL,VIA3,ACT3,PABA,PAEA,PABS,PAES,PDBA,PDEA,PDBS,PDES,GA1X,GA1Y,GA2X,GA2Y,GD1X,GD1Y,GD2X,GD2Y,GA1B,GA1E,GA2B,GA2E,GD1B,GD1E,GD2B,GD2E,VIAL,ADER,REGN,ALC2,ALC3,AIRB,STEV", 
					"10,10,6,2,10,5,5,5,5,14,14,5,5,14,14,14,14,2,2,14,5,5,5,10,14,14,14,14,5,5,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,1000,10,1",
					"URNO,RKEY,FTYP,ADID,FLNO,GTA1,GTD1,GTA2,GTD2,LAND,ONBS,PSTA,PSTD,STOA,STOD,TIFA,TIFD,TISA,TISD,TMOA,TTYP,ORG3,DES3,FLTI,ETAI,ETDI,ONBL,OFBL,VIA3,ACT3,PABA,PAEA,PABS,PAES,PDBA,PDEA,PDBS,PDES,GA1X,GA1Y,GA2X,GA2Y,GD1X,GD1Y,GD2X,GD2Y,GA1B,GA1E,GA2B,GA2E,GD1B,GD1E,GD2B,GD2E,VIAL,ADER,REGN,ALC2,ALC3,AIRB,STEV");
			}
			else
			{
				myTable.Clear();
			}
			//myTable.Command("read",",GFR,"); ==> No!! let the CDRHDL read directly
			myTable.TimeFields = "STOA,STOD,TIFA,TIFD,TMOA,ETAI,ETDI,ONBL,OFBL,PABS,PAES,PDBS,PDES,PDBA,PDEA,PABA,PAEA";
			myTable.TimeFieldsInitiallyInUtc = true;
			myTable.TimeFieldsCurrentlyInUtc = true;
			string strWhere = "WHERE URNO=" + strAftUrno;
			myTable.Load(strWhere);
			myTable.CreateIndex("URNO", "URNO");

		}
	}
}
