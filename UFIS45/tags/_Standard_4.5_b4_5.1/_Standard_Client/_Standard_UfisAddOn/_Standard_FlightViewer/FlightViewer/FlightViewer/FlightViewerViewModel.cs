﻿using System;
using System.Linq;
using System.Collections.Generic;
using Ufis.Data;
using Ufis.Entities;
using Ufis.MVVM.ViewModel;
using FlightViewer.Helpers;
using FlightViewer.DataAccess;
using FlightViewer.Entities;

namespace FlightViewer.FlightViewer
{
    /// <summary>
    /// The ViewModel for the application's main window.
    /// </summary>
    public class FlightViewerViewModel : WorkspaceViewModel
    {
        #region Private or Public Variable(s) Declaration 

        public WorkspaceViewModel _workspace;
        private string sAirportEI, sDOOP, sFlightEI;        
        private string sValidFrom = "";
        private string sValidTo = "";
        DateTime tParamFrom, tParamTo;
        List<entlcSeasonalFlight> objEntLCSeasonalFlight;
        EntityCollectionBase<EntDbSeasonalFlightSchedule> ecbSSR;        
        #endregion

        #region Item Source
        public EntityCollectionBase<EntDbSeason> Season { get; protected set; }
        #endregion

        #region Constructor & Methods
        public FlightViewerViewModel()
        {
            //Disply Project name at title bar
            DisplayName = HpAppInfo.Current.ProductTitle;
            //Bind data to combo box
            Season = DlFlightViewer.GetSeason();
        }
        #endregion // Constructor

        #region Workspace

        public WorkspaceViewModel Workspace
        {
            get
            {
                return _workspace;
            }
            set
            {
                if (_workspace == value)
                    return;

                //Detach the old workspace event handler
                if (_workspace != null)
                    _workspace.RequestClose -= OnWorkspaceRequestClose;

                //Attach the new workspace event handler
                _workspace = value;
                if (_workspace != null)
                    _workspace.RequestClose += OnWorkspaceRequestClose;

                OnPropertyChanged("Workspace");
            }
        }

        void OnWorkspaceRequestClose(object sender, EventArgs e)
        {
            WorkspaceViewModel workspace = sender as WorkspaceViewModel;
            workspace.Dispose();
        }
        
        #endregion // Workspaces

        #region UI Control Reference
        /// <summary>
        /// Search Season according to Search criteria
        /// </summary>
        /// <param name="sSeasonName">System.String containing sSeason Name</param>
        /// <param name="sAirport">System.String Containing sValidFrom</param>
        /// <param name="sAirline">System.String Containing sValidTot</param>
        public void SearchSeason(string sSeasonName, DateTime dtValidFrom, DateTime dtValidTo,
                                string sAirportInfo, Boolean bAirportEI, string sFlightInfo, Boolean bFlightEI, char cAirport, char cFlight, string sDays)
        {
            DataErrorCollection objDE = new DataErrorCollection();
            try
            {
                if (String.Compare(sSeasonName, "", false) == 0)
                    return;

                tParamFrom = Convert.ToDateTime(dtValidFrom.ToShortDateString());
                tParamTo = Convert.ToDateTime(dtValidTo.ToShortDateString());
                sDOOP = sDays;
                sValidFrom = dtValidFrom.ToString("yyyyMMdd");
                sValidTo = dtValidTo.ToString("yyyyMMdd");

                if (String.Compare(sAirportInfo, "", false) != 0)
                {
                    sAirportInfo = sAirportInfo.ToUpper();
                    sAirportInfo = string.Format("'{0}'", sAirportInfo.Replace(" ", "','"));
                    sAirportEI = bAirportEI ? "IN(" : "NOT IN(";
                }
                if (String.Compare(sFlightInfo, "", false) != 0)
                {
                    sFlightInfo = sFlightInfo.ToUpper();
                    sFlightInfo = string.Format("'{0}'", sFlightInfo.Replace(" ", "','"));
                    sFlightEI = bFlightEI ? "IN(" : "NOT IN(";
                }

                string sConstructedDays = "";
                char[] cDays = sDays.ToCharArray();
                for (int i = 0; i < cDays.Length; i++)
                {
                    sConstructedDays = String.Compare(sConstructedDays, "", false) == 0 ? String.Format(" [DaysOfOperation] LIKE '%{0}%'", cDays[i]) : sConstructedDays + String.Format(" OR [DaysOfOperation] LIKE '%{0}%'", cDays[i]);
                }

                //Get Data
                ecbSSR = DlFlightViewer.GetFlightViewerList(sSeasonName, sValidFrom, sValidTo,
                                                                sAirportInfo, sAirportEI, sFlightInfo, sFlightEI, cAirport, cFlight, sConstructedDays);
                //ItemSource = DlFlightViewer.GetFlightViewerList(sSeasonName, sValidFrom, sValidTo,
                //                                                sAirportInfo, sAirportEI, sFlightInfo, sFlightEI, cAirport, cFlight, sDays);
                
                ItemSource = RegenerateData(ecbSSR);                
                
            }
            catch (Exception ex)
            {
                objDE.Add(new DataError(ex.Message, ex));               
            }
        }

        private List<entlcSeasonalFlight> RegenerateData(EntityCollectionBase<EntDbSeasonalFlightSchedule> ecbCollection)
        {
            /*[SeasonName],[FullFlightNumberOfArrival],[FullFlightNumberOfDeparture]," +
            "[ValidFrom],[ValidTo],[DaysOfOperation],[Frequency],[AircraftIATACode]," +
            "[OriginAirportIATACode],[ViaAirportIATACodeOfArrival],[StandardTimeOfArrival]," +
            "[StandardTimeOfDeparture],[NightsStop],[ViaAirportIATACodeOfDeparture]," +
            "[DestinationAirportIATACode],[NatureCodeOfArrival],[NatureCodeOfDeparture]," +
            "[RemarkOfArrival],[Urno],[ServiceTypeOfArrival],[ServiceTypeOfDeparture]*/
            
            if (ecbCollection.Count > 0)
            {
                objEntLCSeasonalFlight = new List<entlcSeasonalFlight>();
                foreach (EntDbSeasonalFlightSchedule objSFSchedule in ecbCollection)
                {
                    
                    entlcSeasonalFlight objentLCSFlights = new entlcSeasonalFlight(); 
                     
                        objentLCSFlights.SeasonName = objSFSchedule.SeasonName; 
                        objentLCSFlights.FullFlightNumberOfArrival = objSFSchedule.FullFlightNumberOfArrival; 
                        objentLCSFlights.FullFlightNumberOfDeparture = objSFSchedule.FullFlightNumberOfDeparture;
                        string sTmp = "";
                        if (String.Compare(sDOOP, "", false) != 0)
                        {
                            
                            objentLCSFlights.ValidFrom = DetermineDate(Convert.ToDateTime(Convert.ToDateTime(objSFSchedule.ValidFrom).ToShortDateString()), "F");
                            objentLCSFlights.ValidTo = DetermineDate(Convert.ToDateTime(Convert.ToDateTime(objSFSchedule.ValidTo).ToShortDateString()), "T");                            
                            for (int a = 1; a <= 7; a++)
                            {
                                sTmp += objSFSchedule.DaysOfOperation.Contains(a.ToString()) && sDOOP.Contains(a.ToString()) ? a.ToString() : "-";                                    
                            }                            
                            objentLCSFlights.DaysOfOperation = sTmp;
                            
                        }
                        else
                        {
                            objentLCSFlights.ValidFrom = DetermineDate(Convert.ToDateTime(Convert.ToDateTime(objSFSchedule.ValidFrom).ToShortDateString()), "F");
                            objentLCSFlights.ValidTo = DetermineDate(Convert.ToDateTime(Convert.ToDateTime(objSFSchedule.ValidTo).ToShortDateString()), "T");
                            //objentLCSFlights.DaysOfOperation = objSFSchedule.DaysOfOperation;
                            for (int a = 1; a <= 7; a++)
                            {
                                sTmp += objSFSchedule.DaysOfOperation.Contains(a.ToString()) && sDOOP.Contains(a.ToString()) ? a.ToString() : "-";
                            }
                            objentLCSFlights.DaysOfOperation = sTmp;
                        }                        
                        objentLCSFlights.Frequency= objSFSchedule.Frequency;
                        objentLCSFlights.AircraftIATACode=objSFSchedule.AircraftIATACode;
                        objentLCSFlights.OriginAirportIATACode=objSFSchedule.OriginAirportIATACode;
                        objentLCSFlights.ViaAirportIATACodeOfArrival=objSFSchedule.ViaAirportIATACodeOfArrival;
                        objentLCSFlights.StandardTimeOfArrival=objSFSchedule.StandardTimeOfArrival;
                        objentLCSFlights.StandardTimeOfDeparture=objSFSchedule.StandardTimeOfDeparture;
                        objentLCSFlights.NightsStop=objSFSchedule.NightsStop;
                        objentLCSFlights.ViaAirportIATACodeOfDeparture= objSFSchedule.ViaAirportIATACodeOfDeparture;
                        objentLCSFlights.DestinationAirportIATACode=objSFSchedule.DestinationAirportIATACode;
                        objentLCSFlights.NatureCodeOfArrival=objSFSchedule.NatureCodeOfArrival;
                        objentLCSFlights.NatureCodeOfDeparture=objSFSchedule.NatureCodeOfArrival;
                        objentLCSFlights.RemarkOfArrival = objSFSchedule.RemarkOfArrival;
                        objentLCSFlights.Urno = objSFSchedule.Urno;
                        objentLCSFlights.ServiceTypeOfArrival = objSFSchedule.ServiceTypeOfArrival;
                        objentLCSFlights.ServiceTypeOfDeparture = objSFSchedule.ServiceTypeOfDeparture;

                        objEntLCSeasonalFlight.Add(objentLCSFlights);

                        objentLCSFlights = null;
                }
            }
            return objEntLCSeasonalFlight;
        }
        private DateTime? DetermineDate(DateTime? tDate, string sWhichDate)
        {
            DateTime? tReturnValue = tDate;
            tReturnValue = String.Compare(sWhichDate, "F", false) == 0 ? tDate < tParamFrom ? tParamFrom : tDate : tDate > tParamTo ? tParamTo : tDate;
            return tReturnValue;
        }        
        /// <summary>
        /// Get Parameter Value
        /// </summary>
        /// <returns>System.string containing sReturnValue</returns>
        public string GetParameterValue()
        {
            string sReturnValue = "";

            EntityCollectionBase<EntDbParameter> objParameter = DlFlightViewer.GetParameterValue("ID_APR_FT");

            if (objParameter != null)
            {
                foreach (EntDbParameter objTmpParameter in objParameter)
                {
                    sReturnValue = objTmpParameter.Value;
                }
            }
            return sReturnValue;
        }
        /// <summary>
        /// Get Login User Name
        /// </summary>
        /// <param name="USID">System.String containing USID</param>
        /// <returns>System.string containing sReturnValue</returns>
        public string GetLoginUserName(string USID)
        {
            string sReturnValue = "";

            EntityCollectionBase<EntDbSecured> objUser = DlFlightViewer.GetLoginUserName(USID);

            if (objUser != null)
            {
                foreach (EntDbSecured objTmpUser in objUser)
                {
                    sReturnValue = objTmpUser.Name;
                }
            }
            return sReturnValue;
        }

        //EntityCollectionBase<EntDbSeasonalFlightSchedule> _itemSource;
        //public EntityCollectionBase<EntDbSeasonalFlightSchedule> ItemSource
        //{
        //    get
        //    {
        //        return _itemSource;
        //    }
        //    protected set
        //    {
        //        if (_itemSource == value)
        //            return;

        //        _itemSource = value;
        //        OnPropertyChanged("ItemSource");
        //    }
        //}
        List<entlcSeasonalFlight> _itemSource;
        public List<entlcSeasonalFlight> ItemSource
        {
            get
            {
                return _itemSource;
            }
            protected set
            {
                if (_itemSource == value)
                    return;

                _itemSource = value;
                OnPropertyChanged("ItemSource");
            }
        }
        #endregion
    }
        
}