﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Windows.Media;

namespace FlightViewer.Helpers
{
    public class HpReportLogo : INotifyPropertyChanged
    {       

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyname)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyname));
        }



        private ImageSource _RptLogo;
        public ImageSource RptLogo
        {
            get { return _RptLogo; }
            set
            {
                _RptLogo = value;
                OnPropertyChanged("RptLogo");
            }
        }
    }
}
