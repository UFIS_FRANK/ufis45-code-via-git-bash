﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Data;
using Ufis.Entities;
using Ufis.Data.Ceda;

namespace FlightViewer.DataAccess
{
    public class DlFlightViewer
    {
        /// <summary>
        /// Filtered Flight viewer list
        /// </summary>
        /// <param name="sSeason">System.String containing sSeason. </param>
        /// <param name="sflightList">System.String containing sFlightList</param>
        /// <param name="sAirportlist">System.String containing sAirportList</param>
        /// <param name="cAirport">System.char containing cAirport</param>
        /// <param name="cFlight">System.char containing cFlight</param>
        /// <param name="sAirportEI">System.string containing sAirportEI</param>
        /// <param name="sAirportInfo">System.string containing sAirportInfo</param>
        /// <param name="sDays">System.string containing sDays</param>
        /// <param name="sFlightEI">System.string containing sFlightEI</param>
        /// <param name="sFlightInfo">System.string containing sFlightInfo</param>
        /// <param name="sSeasonName">System.string containing sSeasonName</param>
        /// <param name="sValidFrom">System.string containing sValidFrom</param>
        /// <param name="sValidTo">System.string containing sValidTo</param>
        /// <returns>Return EntityCollectionBase(EntDbSeasonalFlightSchedule)</returns>
        public static EntityCollectionBase<EntDbSeasonalFlightSchedule> GetFlightViewerList(string sSeasonName, string sValidFrom, string sValidTo,
                                string sAirportInfo, string sAirportEI, string sFlightInfo, string sFlightEI, char cAirport, char cFlight, string sDays)
        {
            const string FIELD_LIST = "[SeasonName],[FullFlightNumberOfArrival],[FullFlightNumberOfDeparture]," +
                                      "[ValidFrom],[ValidTo],[DaysOfOperation],[Frequency],[AircraftIATACode]," +
                                      "[OriginAirportIATACode],[ViaAirportIATACodeOfArrival],[StandardTimeOfArrival]," +
                                      "[StandardTimeOfDeparture],[NightsStop],[ViaAirportIATACodeOfDeparture]," +
                                      "[DestinationAirportIATACode],[NatureCodeOfArrival],[NatureCodeOfDeparture]," +
                                      "[RemarkOfArrival],[Urno],[ServiceTypeOfArrival],[ServiceTypeOfDeparture]";
            string WHERE_CLAUSE = "";
            const string ORDER_BY = "  ORDER BY [FullFlightNumberOfArrival],[ValidFrom],[FullFlightNumberOfDeparture],[ValidTo]";
            //Filter by all criteria
            if ((sSeasonName != null || sValidFrom != null || sValidTo != null) && (String.Compare(sFlightInfo, "", false) != 0 && String.Compare(sAirportInfo, "", false) != 0))
            {
                switch (cAirport)
                {
                    //OR ('{1}' BETWEEN [ValidFrom] AND [ValidTo] OR '{2}' BETWEEN [ValidFrom] AND [ValidTo]) 
                    case 'O':
                        WHERE_CLAUSE = String.Format("WHERE [SeasonName] = '{0}' AND ([ValidFrom] BETWEEN '{1}' AND '{2}' OR [ValidTo] BETWEEN '{1}' AND '{2}' OR '{1}' BETWEEN [ValidFrom] AND [ValidTo] OR '{2}' BETWEEN [ValidFrom] AND [ValidTo] ) AND [OriginAirportIATACode] {3} {4}) "
                        , sSeasonName, sValidFrom.Substring(0, 8), sValidTo.Substring(0, 8), sAirportEI, sAirportInfo);
                        break;
                    case 'D':
                        WHERE_CLAUSE = String.Format("WHERE [SeasonName] = '{0}' AND ([ValidFrom] BETWEEN '{1}' AND '{2}' OR [ValidTo] BETWEEN '{1}' AND '{2}' OR '{1}' BETWEEN [ValidFrom] AND [ValidTo] OR '{2}' BETWEEN [ValidFrom] AND [ValidTo]) AND [DestinationAirportIATACode] {3} {4})"
                        , sSeasonName, sValidFrom.Substring(0, 8), sValidTo.Substring(0, 8), sAirportEI, sAirportInfo);
                        break;
                    default:
                        if (String.Compare(sAirportEI, "IN(", false) == 0)
                        {
                            WHERE_CLAUSE = String.Format("WHERE [SeasonName] = '{0}' AND ([ValidFrom] BETWEEN '{1}' AND '{2}' OR [ValidTo] BETWEEN '{1}' AND '{2}' OR '{1}' BETWEEN [ValidFrom] AND [ValidTo] OR '{2}' BETWEEN [ValidFrom] AND [ValidTo]) AND ([OriginAirportIATACode] {3} {4}) OR [DestinationAirportIATACode] {3} {4}))"
                            , sSeasonName, sValidFrom.Substring(0, 8), sValidTo.Substring(0, 8), sAirportEI, sAirportInfo);
                            break;
                        }
                        else
                        {
                            WHERE_CLAUSE = String.Format("WHERE [SeasonName] = '{0}' AND ([ValidFrom] BETWEEN '{1}' AND '{2}' OR [ValidTo] BETWEEN '{1}' AND '{2}' OR '{1}' BETWEEN [ValidFrom] AND [ValidTo] OR '{2}' BETWEEN [ValidFrom] AND [ValidTo]) AND ([OriginAirportIATACode] {3} {4}) AND [DestinationAirportIATACode] {3} {4}))"
                            , sSeasonName, sValidFrom.Substring(0, 8), sValidTo.Substring(0, 8), sAirportEI, sAirportInfo);
                            break;
                        }
                        
                }
                switch (cFlight)
                {
                    case 'C':
                        if (String.Compare(sFlightEI, "IN(", false) == 0)
                        {
                            WHERE_CLAUSE = String.Format("WHERE [SeasonName] = '{0}' AND ([ValidFrom] BETWEEN '{1}' AND '{2}' OR [ValidTo] BETWEEN '{1}' AND '{2}' OR '{1}' BETWEEN [ValidFrom] AND [ValidTo] OR '{2}' BETWEEN [ValidFrom] AND [ValidTo]) AND ([AirlineIATACodeOfDeparture] {3} {4}) OR [AirlineIATACodeOfArrival] {3} {4})) "
                                                        , sSeasonName, sValidFrom.Substring(0, 8), sValidTo.Substring(0, 8), sFlightEI, sFlightInfo);
                            break;
                        }
                        else
                        {
                            WHERE_CLAUSE = String.Format("WHERE [SeasonName] = '{0}' AND ([ValidFrom] BETWEEN '{1}' AND '{2}' OR [ValidTo] BETWEEN '{1}' AND '{2}' OR '{1}' BETWEEN [ValidFrom] AND [ValidTo] OR '{2}' BETWEEN [ValidFrom] AND [ValidTo]) AND ([AirlineIATACodeOfDeparture] {3} {4}) AND [AirlineIATACodeOfArrival] {3} {4})) "
                                                        , sSeasonName, sValidFrom.Substring(0, 8), sValidTo.Substring(0, 8), sFlightEI, sFlightInfo);
                            break; 
                        }
                    case 'D':
                        WHERE_CLAUSE = WHERE_CLAUSE + string.Format(" AND [AirlineIATACodeOfDeparture] {0} {1}) ", sFlightEI, sFlightInfo);
                        break;
                    default:
                        WHERE_CLAUSE = WHERE_CLAUSE + string.Format(" AND [AirlineIATACodeOfArrival] {0} {1}) ", sFlightEI, sFlightInfo);
                        break;
                }

                WHERE_CLAUSE = String.Compare(sDays, "", false) != 0 ? String.Format("{0} AND ({1}) {2}", WHERE_CLAUSE, sDays, ORDER_BY) : WHERE_CLAUSE + ORDER_BY;


            }
            else if ((sSeasonName != null || sValidFrom != null || sValidTo != null) && (String.Compare(sFlightInfo, "", false) == 0 && String.Compare(sAirportInfo, "", false) != 0))
            {
                switch (cAirport)
                {
                    case 'O':
                        WHERE_CLAUSE = String.Format("WHERE [SeasonName] = '{0}' AND ([ValidFrom] BETWEEN '{1}' AND '{2}' OR [ValidTo] BETWEEN '{1}' AND '{2}' OR '{1}' BETWEEN [ValidFrom] AND [ValidTo] OR '{2}' BETWEEN [ValidFrom] AND [ValidTo]) AND [OriginAirportIATACode] {3} {4}) "
                        , sSeasonName, sValidFrom.Substring(0, 8), sValidTo.Substring(0, 8), sAirportEI, sAirportInfo);
                        break;
                    case 'D':
                        WHERE_CLAUSE = String.Format("WHERE [SeasonName] = '{0}' AND ([ValidFrom] BETWEEN '{1}' AND '{2}' OR [ValidTo] BETWEEN '{1}' AND '{2}' OR '{1}' BETWEEN [ValidFrom] AND [ValidTo] OR '{2}' BETWEEN [ValidFrom] AND [ValidTo]) AND [DestinationAirportIATACode] {3} {4})"
                        , sSeasonName, sValidFrom.Substring(0, 8), sValidTo.Substring(0, 8), sAirportEI, sAirportInfo);
                        break;
                    default:
                        if (String.Compare(sAirportEI, "IN(", false) == 0)
                        {
                            WHERE_CLAUSE = String.Format("WHERE [SeasonName] = '{0}' AND ([ValidFrom] BETWEEN '{1}' AND '{2}' OR [ValidTo] BETWEEN '{1}' AND '{2}' OR '{1}' BETWEEN [ValidFrom] AND [ValidTo] OR '{2}' BETWEEN [ValidFrom] AND [ValidTo]) AND ([OriginAirportIATACode] {3} {4}) OR [DestinationAirportIATACode] {3} {4}))"
                            , sSeasonName, sValidFrom.Substring(0, 8), sValidTo.Substring(0, 8), sAirportEI, sAirportInfo);
                            break;
                        }
                        else
                        {
                            WHERE_CLAUSE = String.Format("WHERE [SeasonName] = '{0}' AND ([ValidFrom] BETWEEN '{1}' AND '{2}' OR [ValidTo] BETWEEN '{1}' AND '{2}' OR '{1}' BETWEEN [ValidFrom] AND [ValidTo] OR '{2}' BETWEEN [ValidFrom] AND [ValidTo]) AND ([OriginAirportIATACode] {3} {4}) AND [DestinationAirportIATACode] {3} {4}))"
                            , sSeasonName, sValidFrom.Substring(0, 8), sValidTo.Substring(0, 8), sAirportEI, sAirportInfo);
                            break;
                        }
                }

                WHERE_CLAUSE = String.Compare(sDays, "", false) != 0 ? String.Format("{0} AND ({1}) {2}", WHERE_CLAUSE, sDays, ORDER_BY) : WHERE_CLAUSE + ORDER_BY;
            }
            else if ((sSeasonName != null || sValidFrom != null || sValidTo != null) && (String.Compare(sFlightInfo, "", false) != 0 && String.Compare(sAirportInfo, "", false) == 0))
            {
                switch (cFlight)
                {
                    case 'C':
                        if (String.Compare(sFlightEI, "IN(", false) == 0)
                        {
                            WHERE_CLAUSE = String.Format("WHERE [SeasonName] = '{0}' AND ([ValidFrom] BETWEEN '{1}' AND '{2}' OR [ValidTo] BETWEEN '{1}' AND '{2}' OR '{1}' BETWEEN [ValidFrom] AND [ValidTo] OR '{2}' BETWEEN [ValidFrom] AND [ValidTo]) AND ([AirlineIATACodeOfDeparture] {3} {4}) OR [AirlineIATACodeOfArrival] {3} {4})) "
                                                        , sSeasonName, sValidFrom.Substring(0, 8), sValidTo.Substring(0, 8), sFlightEI, sFlightInfo);
                            break;
                        }
                        else
                        {
                            WHERE_CLAUSE = String.Format("WHERE [SeasonName] = '{0}' AND ([ValidFrom] BETWEEN '{1}' AND '{2}' OR [ValidTo] BETWEEN '{1}' AND '{2}' OR '{1}' BETWEEN [ValidFrom] AND [ValidTo] OR '{2}' BETWEEN [ValidFrom] AND [ValidTo]) AND ([AirlineIATACodeOfDeparture] {3} {4}) AND [AirlineIATACodeOfArrival] {3} {4})) "
                                                        , sSeasonName, sValidFrom.Substring(0, 8), sValidTo.Substring(0, 8), sFlightEI, sFlightInfo);
                            break;
                        }
                    case 'D':
                        WHERE_CLAUSE = String.Format("WHERE [SeasonName] = '{0}' AND ([ValidFrom] BETWEEN '{1}' AND '{2}' OR [ValidTo] BETWEEN '{1}' AND '{2}' OR '{1}' BETWEEN [ValidFrom] AND [ValidTo] OR '{2}' BETWEEN [ValidFrom] AND [ValidTo]) AND [AirlineIATACodeOfDeparture] {3} {4}) "
                                                    , sSeasonName, sValidFrom.Substring(0, 8), sValidTo.Substring(0, 8), sFlightEI, sFlightInfo);
                        break;
                    default:
                        WHERE_CLAUSE = String.Format("WHERE [SeasonName] = '{0}' AND ([ValidFrom] BETWEEN '{1}' AND '{2}' OR [ValidTo] BETWEEN '{1}' AND '{2}' OR '{1}' BETWEEN [ValidFrom] AND [ValidTo] OR '{2}' BETWEEN [ValidFrom] AND [ValidTo]) AND [AirlineIATACodeOfArrival] {3} {4}) "
                                                    , sSeasonName, sValidFrom.Substring(0, 8), sValidTo.Substring(0, 8), sFlightEI, sFlightInfo);
                        break;
                }

                WHERE_CLAUSE = String.Compare(sDays, "", false) != 0 ? String.Format("{0} AND ({1}) {2}", WHERE_CLAUSE, sDays, ORDER_BY) : WHERE_CLAUSE + ORDER_BY;
            }
            else if ((sSeasonName != null || sValidFrom != null || sValidTo != null) && (String.Compare(sFlightInfo, "", false) == 0 && String.Compare(sAirportInfo, "", false) == 0))
            {
                WHERE_CLAUSE = String.Format("WHERE [SeasonName] = '{0}' AND ([ValidFrom] BETWEEN '{1}' AND '{2}' OR [ValidTo] BETWEEN '{1}' AND '{2}' OR '{1}' BETWEEN [ValidFrom] AND [ValidTo] OR '{2}' BETWEEN [ValidFrom] AND [ValidTo])"
                    , sSeasonName, sValidFrom.Substring(0, 8), sValidTo.Substring(0, 8));

                WHERE_CLAUSE = String.Compare(sDays, "", false) != 0 ? String.Format("{0} AND ({1}) {2}", WHERE_CLAUSE, sDays, ORDER_BY) : WHERE_CLAUSE + ORDER_BY;
            }
            else
            {
                WHERE_CLAUSE = "WHERE [Urno] IS NULL";
            }

            EntityCollectionBase<EntDbSeasonalFlightSchedule> FlightViewerList = null;
            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;

            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntDbSeasonalFlightSchedule),
                AttributeList = FIELD_LIST,
                EntityWhereClause = WHERE_CLAUSE
            };

            FlightViewerList = dataContext.OpenEntityCollection<EntDbSeasonalFlightSchedule>(command);

            return FlightViewerList;
        }

        public static EntityCollectionBase<EntDbSeason> GetSeason()
        {
            //SEAS,VPFR,VPTO
            const string FIELD_LIST = "[Urno],[Name],[ValidFrom],[ValidTo]";

            const string WHERE_CLAUSE = "ORDER BY [ValidFrom] DESC ";

            EntityCollectionBase<EntDbSeason> Season = null;
            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;

            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntDbSeason),
                AttributeList = FIELD_LIST,
                EntityWhereClause = WHERE_CLAUSE
            };

            Season = dataContext.OpenEntityCollection<EntDbSeason>(command);

            return Season;
        }

        /// <summary>
        /// Get Login User Name
        /// </summary>
        /// <param name="USID">System.string containing USID</param>
        /// <returns>Return Entitycollectionbase containing EntDbSecured</returns>
        public static EntityCollectionBase<EntDbSecured> GetLoginUserName(string USID)
        {
            const string FIELD_LIST = "[Name],[Remark],[Status],[Type]";
            string WHERE_CLAUSE = string.Format("WHERE [UserId] = '{0}'", USID);

            EntityCollectionBase<EntDbSecured> objParameter = null;

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;
            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntDbSecured),
                AttributeList = FIELD_LIST,
                EntityWhereClause = WHERE_CLAUSE
            };

            objParameter = dataContext.OpenEntityCollection<EntDbSecured>(command);

            return objParameter;
        }
        /// <summary>
        /// Get Parameter Value
        /// </summary>
        /// <param name="TXID">System.string containing TXID</param>
        /// <returns>Return Entitycollectionbase containing EntDbParameters</returns>
        public static EntityCollectionBase<EntDbParameter> GetParameterValue(string TXID)
        {
            const string FIELD_LIST = "[Value],[ApplicationName],[CreateDate],[NameOfParameter],[ParameterIdentifier],[ParameterType],[TextIdentifier],[EvaluationMode]";
            string WHERE_CLAUSE = string.Format("WHERE [ParameterIdentifier] = '{0}'", TXID);

            EntityCollectionBase<EntDbParameter> objParameter = null;

            EntityDataContextBase dataContext = DlUfisData.Current.DataContext;
            DataCommand command = new CedaEntitySelectCommand()
            {
                EntityType = typeof(EntDbParameter),
                AttributeList = FIELD_LIST,
                EntityWhereClause = WHERE_CLAUSE
            };

            objParameter = dataContext.OpenEntityCollection<EntDbParameter>(command);

            return objParameter;
        }
    }
}
