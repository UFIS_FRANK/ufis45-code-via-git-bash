// UWorkBench.h : main header file for the UWORKBENCH application
//
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *	UWorkBench - Project
 *	
 *		CWinApp Mainclass 
 *
 *	ABB Airport Technologies GmbH 2000
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 *
 *		Date		Author			Comment
 *		-------------------------------------------------------------
 *
 *		????????	mwo AAT/IR		Initial version	
 *		18/10/00	cla AAT/IR		Script host support added
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#if !defined(AFX_UWORKBENCH_H__8EF75894_2D57_11D3_A1C7_0000B4984BBE__INCLUDED_)
#define AFX_UWORKBENCH_H__8EF75894_2D57_11D3_A1C7_0000B4984BBE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols
#include "MainFrm.h"
#include "ScriptHost.h"
/////////////////////////////////////////////////////////////////////////////
// CUWorkBenchApp:
// See UWorkBench.cpp for the implementation of this class
//

class CUWorkBenchApp : public CWinApp
{
public:
	CUWorkBenchApp();


	CString omVersion;

	CMultiDocTemplate* pDesignerDocTemplate;
	CMultiDocTemplate* pDesignerTpl;
	CMultiDocTemplate* pUniversalGridTpl;
	CMultiDocTemplate* pUfisGridMDI;
	CMultiDocTemplate* pFormDesignerTpl;
	CMultiDocTemplate* pFormExecuteTpl;
	CMultiDocTemplate* pVBScriptTpl;

	CMainFrame* pMainFrame;

	/*
	 * Constructor is private - Singleton
	 */
	CScriptHost *m_pScriptHost;
	void InitSetup(); 

	void OnSaveAll();

	void SaveConfigAs(char* pcpFileName, 
				      CCSPtrArray<UDBLoadSet> &ropLoadSets,
					  CCSPtrArray<UGridDefinition> &ropGridDefs,
					  CCSPtrArray<UFormProperties> &ropForms,
					  CCSPtrArray<UQuery> &ropQueries);

	void OnShowDefaultLoadSet();
	void OnObjectCopy();
	void ReadDataFromCfg();

	//Dataextraction for Version 1.0
	void PrepareCfgDataV10(CString opText,
						   CCSPtrArray<UDBLoadSet> &ropLoadSets,
						   CCSPtrArray<UGridDefinition> &ropGridDefs,
						   CCSPtrArray<UFormProperties> &ropForms,
						   CCSPtrArray<UQuery> &ropQueries);

	//Dataextraction for Version 1.1
	void PrepareCfgDataV11(CString opText,
					   CCSPtrArray<UDBLoadSet> &ropLoadSets,
					   CCSPtrArray<UGridDefinition> &ropGridDefs,
					   CCSPtrArray<UFormProperties> &ropForms,
					   CCSPtrArray<UQuery> &ropQueries);

	void ReadAnyCfgFile(CString opFileName,
						CCSPtrArray<UDBLoadSet> &ropLoadSets,
						CCSPtrArray<UGridDefinition> &ropGridDefs,
						CCSPtrArray<UFormProperties> &ropForms,
						CCSPtrArray<UQuery> &ropQueries);

	CString MakeClientString(CString ropText);
	CString MakeCedaString(CString ropText);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUWorkBenchApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CUWorkBenchApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UWORKBENCH_H__8EF75894_2D57_11D3_A1C7_0000B4984BBE__INCLUDED_)
