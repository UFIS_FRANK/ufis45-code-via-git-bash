
UScript die Sprache:
====================
Wichtig ist, da� die Sprache wortweise aufgebaut ist
Das erste Wort einer Anweisung ist jeweils das Kommando,
es sei denn es ist eine Berechnung.

Eine Funktion ist jeweils durch *FKT*<NAME> gekennzeichnet
z.B. FKT:DtimFromDateAndTimeField



Sprachkonstrukte:
=================
Simple operatoren:			+,-,*,/,%(modulo)

Zuweisungen numerisch:		NUMB a + b - c = d
			  ODER:		REAL a / b = d
						Das Ergebnis steht in d => umgekehrte notation

Logische Ausdr�cke/bool:	BOOL a = b OR c = d 
						BOOL a = b AND c = d
						BOOL (a = b AND c = d) OR (e = f)

Zuweisungen String:		STRG s1 = s3

String anh�ngen:			STRG s1 + s2

IF .. ELSE:				Es gibt ein Reihe von Check-Funktionen, die dazu f�hren, da� ein
						Vergleich durchgef�hrt wird. Diese Funktionen beginnen mit dem Pr�fix
						CHK_

						CHK_
						GRID_VALUE_COND <row> <col> = <value> ? a : b
						

Funktionen:
	*FKT*Default:
			Die UWorkBench bietet eine Reihe von Standardfunktionalit�t, wie z.B.
			- Check, ob alle Eingabeformate eingehalten sind
			- Holen der Feldinhalte aus den Controls
			- Check, ob alle Mu�felder gef�llt sind
			- etc.
			Das Standardverhalten h�ngt nat�rlich vom jeweiligen Object ab
			�hnlich wie in C++ (Aufruf der Funktion der Parentklasse) wird 
			"*FKT*Default" immer aufgerufen, d.h.:
			Beim Anlegen eines Controls oder einer Form werden alle Events
			mit "*FKT*Default" vorbelegt. Es ist jedoch manchmal von Vorteil, da� 
			"*FKT*Default" nicht ausgef�hrt wird ==> also mu� man in der Lage sein
			diese Funktion aus den Eventproperties wieder herauszunehmen.
Pr�fixsymbolik:
===============
	CTL_	:	Funktionen f�r Oberfl�chen Controls (edit, combobox ...)  				
	CDB_	:	Funktionen, die Werte von Oberfl�chencontrols in die Datenhaltung einarbeiten
	DBC_	:   Funktionen, die Werte aus die Datenhaltung in Oberfl�chencontrols  �berf�hren


			Funktionen beginned mit "CTL_" sind Controlfunktionen
			CTL_MAKE_DB_TIME_STRING <source>
			// Formatiert 11:30 ==> 1130

			CTL_MAKE_DB_DATE_STRING <source>
			// Formatiert 10.10.1999 ==> 19991010

			CTL_SET_FMT_FIELD_VALUE <SourceField> <DestField> <FromPosition> <Count>
			// Kann auch in einer festen Funktion stehen, da z.B. ein DB-Datums-Zeitfeld
			// an der Oberfl�che in 2 Felder aufgeteilt werden mu�

			Beispiel:
			*FKT*SetDateTimeField 
				//Ctl_MakeDBTimeString BSD.VAFR.TIME
				Ctl_MAKE_DB_TIME_STRING BSD.VAFR.TIME
				CTL_MAKE_DB_DATE_STRING BSD.VAFR.DATE
				CTL_SET_FMT_FIELD_VALUE BSD.VAFR.DATE BSD.VAFR 0 8 
				CTL_SET_FMT_FIELD_VALUE BSD.VAFR.TIME BSD.VAFR 8 6 'f�llt die Sekunden mit 00 auf
			*ENDFKT*

Form Fkts.:			
===========
	Soll sich die Funktion auf das aktuelle Formular beziehen
	==> wird f�r <formname> "self" eingesetzt ==> analog zum this-> pointer in C++ 
			FRM_OPEN <formname>	
			FRM_SET_MASTER_TABLE <formname> <table>
			FRM_SET_KEY_FIELD <formname> <field>
			FRM_SET_KEY_VALUE <formname> <value>
			FRM_SET_TITLE <formname> <value>
			FRM_CLOSE <formname>	
			FRM_MOVE <formname> <left> <top> <right> <bottom>
			FRM_HIDE <formname>
			FRM_SHOW <formname>
			FRM_NEXT_RECORD <formname>
			FRM_PREV_RECORD <formname>
			FRM_FIRST_RECORD <formname>
			FRM_LAST_RECORD <formname>

Control Fkts.:		
==============
		Control-Funktionen gehen grunds�tzlich davon aus, da� sie von der umgebenden Form
		aus aufgerufen werden.
		Damit wird die Vereinbarung getroffen, da� Formulare ausschlie�lich ihre eigenen
		Controls (und nicht diejenigen anderer Formulare) beeinflussen und manipulieren k�nnen.

			CTL_SET_BKCOLOR <controlname> <colorref>
			CTL_SET_TEXTCOLOR <controlname> <colorref>
			CTL_SET_VALUE	<controlname> <value>
				// Die Entscheidung was passiert h�ngt vom Controltyp ab
				// edit:	String erscheint als Text
				// combo:	String erscheint als Text
				// check:	nur 0 oder 1 zul�ssig ==> Haken setzen oder nicht
			CTL_GET_VALUE <controlname> <value>
			CTL_HIDE	<controlname>
			CTL_SHOW	<controlname>
			CTL_DISABLE <controlname>
			CTL_ENABLE	<controlname>
			CTL_MOVE	<controlname> <left> <top> <right> <bottom>
			CTL_SETTYPE <controlname> <type> 
			
			CDB_SET <controlname> <dbfield>
			DBC_SET <dbfield> <controlname>

Ceda-Basic-Data Fkts.:
======================
		BCD_ ist der Pr�fix f�r den Aufruf von Funktionalit�ten, die die interne
		Datenhaltung betreffen
		
			BCD_DELETE_RECORD <Table> <Urno>
			
Array Fkts.:
============
			ARR_MAKE_ARRAY		<arrayname> <type>
			ARR_MAKE_2D_ARRAY	<arrayname> <type> <dimension> <dimension>
			ARR_DELETE			<arrayname>
			ARR_APPEND			<arrayname>
			ARR_2D_APPEND		<arrayname>
			ARR_GET_AT			<arrayname> <index>
			ARR_2D_GET_AT		<arrayname> <index1> <index2>

Ausdr�cke:
==========
		Schleifen konstrukte
		FOR_EACH <arrayname> 
			<block>
		END_FOR_EACH

Eventfunktionen:
================
		Jedes Oberfl�chenelement hat entsprechende Events, die abgefangen werden k�nnen.
		Ein Event legt ein entsprechendes Ereignis fest. Die das Event behandelnde Funktion geht
		davon aus, da� es standardm��ig die Default-Funktion ausf�hrt. Damit wird f�r jedes
		m�gliche Event eines GUI-Elements in der jeweiligne OnXXX - Funktion per Standard
		"*FKT*Default" eingetragen. Dieser Eintrag kann entfernt oder �berschrieben werden.
		In den allermeisten F�llen kann man davon ausgehen, da� die "*FKT*Default"-Funktion
		nichts macht.


	Form-Events:	
			OnOpen			: �ffenen der Form
			OnClose			: Schlie�en der Form

			OnSize			: Die Gr��e wurde ver�ndert

			OnSaveRecord	: Toolbar Speichern wurde ausgel�st
			OnNextRecord	: Toolbar Next wurde ausgel�st
			OnPrevRecord	: Toolbar Previous wurde ausgel�st
			OnFirstRecord	: Toolbar First wurde ausgel�st
			OnLastRecord	: Toolbar Last wurde ausgel�st
			OnDelete		: Toolbar Delete wurde ausgel�st
			
			OnLButtonDown	: Linke Maustaste gedr�ckt
			OnRButtonDown	: Recte Maustaste gedr�ckt

	Edit-Events:
			OnFocusOut		: Feld hat den Focus erhalten
			OnFocusIn		: Das Feld wurde verlassen
			OnChange		: Der Feldinhalt wurde ge�ndert
			
	Combobox-Events:
			OnFocusOut		: Feld hat den Focus erhalten
			OnFocusIn		: Das Feld wurde verlassen
			OnChange		:
			OnSelChange		:

	Checkbox-Events:
			OnFocusOut		: Feld hat den Focus erhalten
			OnFocusIn		: Das Feld wurde verlassen
			OnClicked		: Die Checkbox wurde geklickt
			

	Button-Events:
			OnFocusOut		: Feld hat den Focus erhalten
			OnFocusIn		: Das Feld wurde verlassen
			OnClicked		: Button wurde gedr�ckt




