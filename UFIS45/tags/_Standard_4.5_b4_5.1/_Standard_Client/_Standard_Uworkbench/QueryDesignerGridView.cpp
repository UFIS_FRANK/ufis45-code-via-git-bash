// GridView.cpp : implementation of the CQueryDesignerGridView class
//

#include "stdafx.h"
#include "QueryDesignerGridView.h"
#include "QueryDesingerTables.h"
#include "UWorkBenchDoc.h"
#include "UniEingabe.h"
#include "UObjectDataDefinitions.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


class QueryDesingerTables;
/////////////////////////////////////////////////////////////////////////////
/////////////////// Stingray Software Objective Grid ////////////////////////
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// CQueryDesignerGridView

IMPLEMENT_DYNCREATE(CQueryDesignerGridView, CGXGridView)

BEGIN_MESSAGE_MAP(CQueryDesignerGridView, CGXGridView)
	//{{AFX_MSG_MAP(CQueryDesignerGridView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CGXGridView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CGXGridView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CGXGridView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CQueryDesignerGridView construction/destruction

CQueryDesignerGridView::CQueryDesignerGridView()
{
	// TODO: add construction code here
	bmInit = false;
	pomLineStyle1 = new CGXStyle();
	pomLineStyle2 = new CGXStyle();
	pomLineStyle3 = new CGXStyle();
	pomLineStyle4 = new CGXStyle();
	pomLineStyle5 = new CGXStyle();
	pomLineStyle6 = new CGXStyle();
	pomLineStyle7 = new CGXStyle();
	pomLineStyle8 = new CGXStyle();
	pomLineStyle9 = new CGXStyle();
	pomLineStyle10 = new CGXStyle();
}

CQueryDesignerGridView::~CQueryDesignerGridView()
{
	delete pomLineStyle1 ;
	delete pomLineStyle2 ;
	delete pomLineStyle3 ;
	delete pomLineStyle4 ;
	delete pomLineStyle5 ;
	delete pomLineStyle6 ;
	delete pomLineStyle7 ;
	delete pomLineStyle8 ;
	delete pomLineStyle9 ;
	delete pomLineStyle10;

}

BOOL CQueryDesignerGridView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CGXGridView::PreCreateWindow(cs);
}

int CQueryDesignerGridView::OnCreate( LPCREATESTRUCT lpCreateStruct )
{
#if _MFC_VER >= 0x0400
	// Enable grid to be used as data source
	EnableOleDataSource();

	// Register the grid as drop target
	VERIFY(m_objDndDropTarget.Register(this));
#endif
	return CGXGridView::OnCreate(lpCreateStruct);
}
/////////////////////////////////////////////////////////////////////////////
// CQueryDesignerGridView drawing

void CQueryDesignerGridView::OnDraw(CDC* pDC)
{
	UQueryDesingerDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	CGXGridView::OnDraw(pDC);
	// TODO: add draw code for native data here
}

void CQueryDesignerGridView::OnInitialUpdate()
{


	BOOL bFirstView = FALSE;	
	bmIsInInitialUpdate = true;
	if (GetDocument()->m_pParam == NULL)
	{
		bFirstView = TRUE;       
			// bFirstView = TRUE indicates that this is 
			// the first view connected to the document
			// and therefore the data need to be intialized.
		// construct parameter object
		GetDocument()->m_pParam = new CGXGridParam;
	}
	// else
		// bFirstView = FALSE indicates that this is 
		// only another view connected to the document
		// No data need to be initialized. They are all 
		// available in the document already.


	// pass the pointer to the grid view
	SetParam(GetDocument()->m_pParam, FALSE);
	                           //     ^-- indicates that document is 
	                        // responsible for deleting the object.

#if _MFC_VER >= 0x0400
EnableGridToolTips();
#endif


	// Lock any drawing
	BOOL bOld = LockUpdate();

	// Custom initialization of styles map
	if (GetParam()->GetStylesMap() == NULL)
	{
		// create a stylesmap and connect it with the parameter-object
		CGXStylesMap* pStyMap = new CGXStylesMap;
		
		GetParam()->SetStylesMap(pStyMap);

		// add standard styles
		pStyMap->CreateStandardStyles();
		


		SetupUserAttributes();

		// If you want to specify a specific profile section
		// for load and storing the base styles in the registry,
		// simply uncomment the next line
		// pStyMap->SetSection(_T("My Base Styles"));  // extra profile section

		//pStyMap->ReadProfile();
	}

	// Custom initialization of property object
	if (GetParam()->GetProperties() == NULL)
	{
		// create a stylesmap and connect it with the parameter-object
		CGXProperties* pProperties = new CGXProperties;

		pProperties->AddDefaultUserProperties();

		// TODO: Add here your own preferred setup of properties

		// SAMPLE CODE
		//uncomment as required
		//pProperties->SetCenterHorizontal(TRUE);
		//pProperties->SetDisplayHorzLines(FALSE);
		//pProperties->SetPrintHorzLines(FALSE);
		//pProperties->SetPrintVertLines(FALSE);
		//pProperties->SetBlackWhite(TRUE);
		//pProperties->SetPrintFrame(TRUE);
		// END OF SAMPLE CODE

		// If you want to specify a specific profile section
		// for load and storing the properties in the registry,
		// simply uncomment the next line
		// pProperties->SetSection(_T("My Properties"));  // extra profile section

		pProperties->ReadProfile();

		GetParam()->SetProperties(pProperties);
	}

	// Custom initialization of printer settings
	if (GetParam()->GetPrintDevice() == NULL )
	{
		// Initialize printer object if you want to have
		// some default settings for the grid printing, e.g. Print Landscape
	}

	CGXGridView::OnInitialUpdate();	



	// check if data need to be initialized
	if (bFirstView)
	{
		EnableHints(FALSE);

		// initialize the grid data

		// disable undo mechanism for the following commands
		GetParam()->EnableUndo(FALSE);

		GetParam()->EnableUndo(FALSE);
		SetRowCount(10);
		SetColCount(20);
		GetParam()->EnableUndo(TRUE);

		SetColWidth(0,1,100);
		SetColWidth(1,1,120);
		SetColWidth(2,2,120);
		SetColWidth(3,3,120);
		SetColWidth(4,4,120);
		SetColWidth(5,5,120);
		SetColWidth(6,6,120);
		SetColWidth(7,7,120);
		SetColWidth(8,8,120);
		SetColWidth(9,9,120);
		SetColWidth(10,10,120);
		SetColWidth(11,11,120);
		SetColWidth(12,12,120);
		SetColWidth(13,13,120);
		SetColWidth(14,14,120);
		SetColWidth(15,15,120);
		SetColWidth(16,16,120);
		SetColWidth(17,17,120);
		SetColWidth(18,18,120);
		SetColWidth(19,19,120);
		SetColWidth(20,20,120);
		SetRowHeight(0, 0, 10);
		CGXStyle olStyle;

		CGXComboBox *polComboBox = new CGXComboBox((CGXGridCore*)this, 1, 1, GXCOMBO_TEXTFIT);
		polComboBox->m_bInactiveDrawButton = FALSE; //Damit wird er Combobox-Button in allen Zellen und nicht nur in der aktiven angezeigt
		RegisterControl(GX_IDS_CTRL_COMBOBOX,  polComboBox);
		GetParam()->EnableTrackRowHeight(GX_TRACK_ALL);

		//GX_IDS_CTRL_TABBED_COMBOBOX
		CGXTabbedComboBox *polTabbedCB = new CGXTabbedComboBox((CGXGridCore*)this, 2, 3);
		polTabbedCB->m_bInactiveDrawButton = FALSE; //Damit wird er Combobox-Button in allen Zellen und nicht nur in der aktiven angezeigt
		RegisterControl(GX_IDS_CTRL_TABBED_COMBOBOX, polTabbedCB);

		CGXCheckBox *polCeckBox = new CGXCheckBox((CGXGridCore*)this, TRUE, GX_IDB_CHECK_95);
		//polCeckBox->m_bInactiveDrawButton = TRUE;
		RegisterControl(GX_IDS_CTRL_CHECKBOX3D, polCeckBox);

		SetStyleRange(CGXRange(1, 0,  1, 0), olStyle.SetValue("Table"));
		SetStyleRange(CGXRange(2, 0,  2, 0), olStyle.SetValue("Field"));
		SetStyleRange(CGXRange(3, 0,  3, 0), olStyle.SetValue("Sort"));
		SetStyleRange(CGXRange(4, 0,  4, 0), olStyle.SetValue("Criteria"));
		SetStyleRange(CGXRange(5, 0,  5, 0), olStyle.SetValue("Show"));
		SetStyleRange(CGXRange(6, 0,  6, 0), olStyle.SetValue(""));
		SetStyleRange(CGXRange(7, 0,  7, 0), olStyle.SetValue(""));
		SetStyleRange(CGXRange(8, 0,  8, 0), olStyle.SetValue(""));
		SetStyleRange(CGXRange(9, 0,  9, 0), olStyle.SetValue(""));
		SetStyleRange(CGXRange(10,0,  10, 0), olStyle.SetValue(""));

		for(int i = 1; i < 21; i++)
		{
			SetStyleRange(CGXRange(0, i, 0, i), olStyle.SetValue(""));
		}
		SetStyleRange(CGXRange(1, 1, 10, 20), olStyle.SetValue(""));
//---------
/*		if(bmInit == true)
		{
			UQueryDesingerDoc *polDoc = (UQueryDesingerDoc *)GetDocument();
			if(polDoc != NULL)
			{
				int ilColCount;

					   

				CString olChoiceList;
				if(polDoc->pomCurrentQuery != NULL)
				{
					for(i = 0; i < polDoc->pomCurrentQuery->omUsedTables.GetSize(); i++)
					{
						olChoiceList += polDoc->pomCurrentQuery->omUsedTables[i] + CString("\n");
					}
				}
				ilColCount = GetColCount();
				for(i = 1; i <= ilColCount; i++)
				{
					SetStyleRange(CGXRange(1, i), CGXStyle().SetHorizontalAlignment(DT_LEFT)
															.SetVerticalAlignment(DT_BOTTOM)
															.SetControl(GX_IDS_CTRL_COMBOBOX)
															.SetChoiceList(olChoiceList));
				}
				for(i = 1; i <= ilColCount; i++)
				{
					SetStyleRange(CGXRange(2, i), CGXStyle().SetHorizontalAlignment(DT_LEFT)
															.SetVerticalAlignment(DT_BOTTOM)
															.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX));
				}
				ilColCount = GetColCount();
				for(i = 1; i <= ilColCount; i++)
				{
					SetStyleRange(CGXRange(3, i), CGXStyle().SetHorizontalAlignment(DT_LEFT)
															.SetVerticalAlignment(DT_BOTTOM)
															.SetControl(GX_IDS_CTRL_COMBOBOX)
															.SetChoiceList("\nAsc\nDsc\n"));
				}
				
				if(GetDocument()->pomCurrentQuery != NULL)
				{
					for(i = 0; i < GetDocument()->pomCurrentQuery->omQueryDetails.GetSize(); i++)
					{
						int ilColumn = GetDocument()->pomCurrentQuery->omQueryDetails[i].imColNo;
						CString olTable = GetDocument()->pomCurrentQuery->omQueryDetails[i].Object;
						CString olField = GetDocument()->pomCurrentQuery->omQueryDetails[i].Field;
						CString olSort  = GetDocument()->pomCurrentQuery->omQueryDetails[i].Sort;
						CString olCheck = GetDocument()->pomCurrentQuery->omQueryDetails[i].omShowAsResult;
						olChoiceList = CString("");

						CCSPtrArray<RecordSet> olRecords;
						ogBCD.GetRecords("SYS", "TANA", GetDocument()->pomCurrentQuery->omQueryDetails[i].Object, &olRecords);
						for(int j = 0; j < olRecords.GetSize(); j++)
						{
							CString olSrcStr;
							olSrcStr = olRecords[j].Values[ogBCD.GetFieldIndex("SYS", "FINA")];
							olSrcStr += CString("\t ") + olRecords[j].Values[ogBCD.GetFieldIndex("SYS", "ADDI")];
 							olChoiceList += olSrcStr + CString("\n");
						}
						olRecords.DeleteAll();

						SetStyleRange(CGXRange().SetCells(2, GetDocument()->pomCurrentQuery->omQueryDetails[i].imColNo), 
									CGXStyle().SetHorizontalAlignment(DT_LEFT)
											  .SetVerticalAlignment(DT_BOTTOM)
											  .SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)
											  .SetChoiceList(olChoiceList));
						SetStyleRange(CGXRange(3, ilColumn), CGXStyle().SetHorizontalAlignment(DT_LEFT)
																.SetVerticalAlignment(DT_BOTTOM)
																.SetControl(GX_IDS_CTRL_COMBOBOX)
																.SetChoiceList("\nAsc\nDsc\n"));
						SetValueRange(CGXRange(1, ilColumn), olTable);
						SetValueRange(CGXRange(2, ilColumn), olField);
						SetValueRange(CGXRange(3, ilColumn), olSort);

						SetStyleRange(CGXRange(5, ilColumn), CGXStyle().SetChoiceList("")
																		.SetHorizontalAlignment(DT_CENTER)
																		.SetUserAttribute(GX_IDS_UA_CHECKBOX_CHECKED, _T("1"))
																		.SetUserAttribute(GX_IDS_UA_CHECKBOX_UNCHECKED, _T("0"))
																		.SetControl(GX_IDS_CTRL_CHECKBOX3D));
						SetValueRange(CGXRange(5, ilColumn), olCheck);
						GetDocument()->SetModifiedFlag(FALSE);
					}
				}
			}

		}//bmInit == true
*/
//---------
		Redraw();

		
	}
	// Register all controls for the grid

	// Unlock drawing
	LockUpdate(bOld);

	// Just to be sure that everything is redrawn
	Invalidate();

	// Enable Objective Grid internal update-hint mechanism

	// You should put this line as last command into OnInitialUpdate,
	// becaus as long as EnableHints is not called, the modified flag
	// of the document will not be changed.

	EnableHints();
	bmInit = true;
	bmIsInInitialUpdate = false;
}


void CQueryDesignerGridView::SetupUserAttributes()
{
		CGXStylesMap* stylesmap = GetParam()->GetStylesMap();
	
		// Register user attributes for the CGXStyleSheet-User Attributes page.
		// (control objects will be created on demand in CGXControlFactory).
		stylesmap->RegisterDefaultUserAttributes();
}


void CQueryDesignerGridView::OnUpdate( CView* pSender, LPARAM lHint, CObject* pHint )
{
	//CGXGridView::OnUpdate( CView* pSender, LPARAM lHint, CObject* pHint );
	//if (pSender->IsKindOf(RUNTIME_CLASS(CFormView)))

	int i;
	if(bmInit == true && bmIsInInitialUpdate == false)
	{
		UQueryDesingerDoc *polDoc = (UQueryDesingerDoc *)GetDocument();
		if(polDoc != NULL)
		{
			int ilColCount;

			       

			CString olChoiceList;
			if(polDoc->pomCurrentQuery != NULL)
			{
				for(i = 0; i < polDoc->pomCurrentQuery->omUsedTables.GetSize(); i++)
				{
					olChoiceList += polDoc->pomCurrentQuery->omUsedTables[i] + CString("\n");
				}
			}
			ilColCount = GetColCount();
			for(i = 1; i <= ilColCount; i++)
			{
				SetStyleRange(CGXRange(1, i), CGXStyle().SetHorizontalAlignment(DT_LEFT)
														.SetVerticalAlignment(DT_BOTTOM)
														.SetControl(GX_IDS_CTRL_COMBOBOX)
														.SetChoiceList(olChoiceList)/*olOldStyle*/);
			}
			for(i = 1; i <= ilColCount; i++)
			{
				SetStyleRange(CGXRange(2, i), CGXStyle().SetHorizontalAlignment(DT_LEFT)
														.SetVerticalAlignment(DT_BOTTOM)
														.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX));
			}
			ilColCount = GetColCount();
			for(i = 1; i <= ilColCount; i++)
			{
				SetStyleRange(CGXRange(3, i), CGXStyle().SetHorizontalAlignment(DT_LEFT)
														.SetVerticalAlignment(DT_BOTTOM)
														.SetControl(GX_IDS_CTRL_COMBOBOX)
														.SetChoiceList("\nAsc\nDsc\n")/*olOldStyle*/);
			}
			SetStyleRange(CGXRange(1, 1, 10, 20), CGXStyle().SetValue(""));
			
			if(GetDocument()->pomCurrentQuery != NULL)
			{
				int C = GetDocument()->pomCurrentQuery->omQueryDetails.GetSize();
				for(i = 0; i < GetDocument()->pomCurrentQuery->omQueryDetails.GetSize(); i++)
				{
					int ilColumn = GetDocument()->pomCurrentQuery->omQueryDetails[i].imColNo;
					CString olTable = GetDocument()->pomCurrentQuery->omQueryDetails[i].Object;
					CString olField = GetDocument()->pomCurrentQuery->omQueryDetails[i].Field;
					CString olSort  = GetDocument()->pomCurrentQuery->omQueryDetails[i].Sort;
					CString olCheck = GetDocument()->pomCurrentQuery->omQueryDetails[i].omShowAsResult;
					olChoiceList = CString("");

					CCSPtrArray<RecordSet> olRecords;
					ogBCD.GetRecords("SYS", "TANA", GetDocument()->pomCurrentQuery->omQueryDetails[i].Object, &olRecords);
					for(int j = 0; j < olRecords.GetSize(); j++)
					{
						CString olSrcStr;
						olSrcStr = olRecords[j].Values[ogBCD.GetFieldIndex("SYS", "FINA")];
						olSrcStr += CString("\t ") + olRecords[j].Values[ogBCD.GetFieldIndex("SYS", "ADDI")];
 						olChoiceList += olSrcStr + CString("\n");
					}
					olRecords.DeleteAll();

					SetStyleRange(CGXRange().SetCells(2, GetDocument()->pomCurrentQuery->omQueryDetails[i].imColNo), 
								CGXStyle().SetHorizontalAlignment(DT_LEFT)
										  .SetVerticalAlignment(DT_BOTTOM)
										  .SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)
										  .SetChoiceList(olChoiceList));
					SetStyleRange(CGXRange(3, ilColumn), CGXStyle().SetHorizontalAlignment(DT_LEFT)
															.SetVerticalAlignment(DT_BOTTOM)
															.SetControl(GX_IDS_CTRL_COMBOBOX)
															.SetChoiceList("\nAsc\nDsc\n")/*olOldStyle*/);

					SetStyleRange(CGXRange(5, ilColumn), CGXStyle().SetChoiceList("")
																	.SetHorizontalAlignment(DT_CENTER)
																	.SetUserAttribute(GX_IDS_UA_CHECKBOX_CHECKED, _T("1"))
																	.SetUserAttribute(GX_IDS_UA_CHECKBOX_UNCHECKED, _T("0"))
																	.SetControl(GX_IDS_CTRL_CHECKBOX3D));
					SetValueRange(CGXRange(1, ilColumn), olTable);
					SetValueRange(CGXRange(2, ilColumn), olField);
					SetValueRange(CGXRange(3, ilColumn), olSort);
					SetValueRange(CGXRange(5, ilColumn), olCheck);
					GetDocument()->SetModifiedFlag();

				}
			}
		}

	}//bmInit == true
}

void CQueryDesignerGridView::UpdateAllViews(CWnd* pSender, LPARAM lHint, CObject* pHint)
{
}

void CQueryDesignerGridView::SaveQuery()
{
	CString olQueryName;
	if(omQueryName.IsEmpty())
	{
		if(MessageBox(LoadStg(IDS_STRING61450), LoadStg(IDS_STRING61449), MB_YESNOCANCEL) == IDYES)
		{
			UniEingabe *polDlg = new UniEingabe(this, LoadStg(IDS_STRING61448), LoadStg(IDS_SAVE_AS), 32);
			int ilRet = polDlg->DoModal();
			if(ilRet == IDOK)
			{
				olQueryName = polDlg->m_Eingabe;
			}//fi
			delete polDlg;
		}//fi
	}//fi
}

/////////////////////////////////////////////////////////////////////////////
// CQueryDesignerGridView printing

BOOL CQueryDesignerGridView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CQueryDesignerGridView::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	CGXGridView::OnBeginPrinting(pDC, pInfo);
	// TODO: add extra initialization before printing
}

void CQueryDesignerGridView::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	CGXGridView::OnEndPrinting(pDC, pInfo);

	// TODO: add cleanup after printing
}

/////////////////////////////////////////////////////////////////////////////
// CQueryDesignerGridView diagnostics

//#ifdef _DEBUG
void CQueryDesignerGridView::AssertValid() const
{
//	CGXGridView::AssertValid();
}

void CQueryDesignerGridView::Dump(CDumpContext& dc) const
{
//	CGXGridView::Dump(dc);
}

UQueryDesingerDoc* CQueryDesignerGridView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(UQueryDesingerDoc)));
	return (UQueryDesingerDoc*)m_pDocument;
}
//#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CQueryDesignerGridView message handlers

void CQueryDesignerGridView::OnModifyCell(ROWCOL nRow, ROWCOL nCol)
{  
	CGXGridView::OnModifyCell(nRow, nCol);
	CString olTable, olField, olSort;
	CString olChoiceList;
//	ogBCD.SetSort("SYS","ADDI+",true);
//	ogBCD.Sort("SYS");
//GetQueryDetailByColumn(int ipCol)
	switch((int)nRow)
	{
		case 1:
		{
			olChoiceList = CString("");

			CGXComboBox *polComboBox = (CGXComboBox *) GetControl(nRow, nCol);
 			polComboBox->GetValue(olTable);
			CCSPtrArray<RecordSet> olRecords;
			ogBCD.GetRecords("SYS", "TANA", olTable, &olRecords);
			for(int i = 0; i < olRecords.GetSize(); i++)
			{
				CString olSrcStr;
				olSrcStr = olRecords[i].Values[ogBCD.GetFieldIndex("SYS", "FINA")];
				olSrcStr += CString("\t ") + olRecords[i].Values[ogBCD.GetFieldIndex("SYS", "ADDI")];
 				olChoiceList += olSrcStr + CString("\n");
			}
			olRecords.DeleteAll();

			SetStyleRange(CGXRange().SetCells(nRow+1, nCol), CGXStyle().SetHorizontalAlignment(DT_LEFT)
																		.SetVerticalAlignment(DT_BOTTOM)
																		.SetControl(GX_IDS_CTRL_TABBED_COMBOBOX)
																		.SetChoiceList(olChoiceList));
			GetDocument()->SetModifiedFlag();
		}
		break;
		case 2:
		{
			UQueryDetail *polQueryDetail = NULL;
			bool isNewQueryDetail = false;
			polQueryDetail = GetDocument()->pomCurrentQuery->GetQueryDetailByColumn(nCol);
			if(polQueryDetail == NULL)
			{
				polQueryDetail = new UQueryDetail();
				GetDocument()->pomCurrentQuery->omQueryDetails.Add(polQueryDetail);
				isNewQueryDetail = true;
			}
			polQueryDetail->imColNo = (int)nCol;
			olTable = GetValueRowCol(nRow-1, nCol);
			polQueryDetail->Object = olTable;
			CGXTabbedComboBox *polTabbedCB = (CGXTabbedComboBox *) GetControl(nRow, nCol);
			polTabbedCB->GetValue(olField);
			polQueryDetail->Field = olField;
			if(!olTable.IsEmpty() && !olField.IsEmpty())
			{
				SetStyleRange(CGXRange(5, nCol), CGXStyle().SetChoiceList("")
															.SetHorizontalAlignment(DT_CENTER)
															.SetUserAttribute(GX_IDS_UA_CHECKBOX_CHECKED, _T("1"))
															.SetUserAttribute(GX_IDS_UA_CHECKBOX_UNCHECKED, _T("0"))
															.SetControl(GX_IDS_CTRL_CHECKBOX3D));
			}
			GetDocument()->SetModifiedFlag();
		}
		break;
		case 3:
		{
			UQueryDetail *polQueryDetail = NULL;
			bool isNewQueryDetail = false;
			polQueryDetail = GetDocument()->pomCurrentQuery->GetQueryDetailByColumn(nCol);
			if(polQueryDetail == NULL)
			{
				polQueryDetail = new UQueryDetail();
				GetDocument()->pomCurrentQuery->omQueryDetails.Add(polQueryDetail);
				isNewQueryDetail = true;
			}
			CGXComboBox *polComboBox = (CGXComboBox *) GetControl(nRow, nCol);
 			polComboBox->GetValue(olSort);
			polQueryDetail->Sort = olSort;
			GetDocument()->SetModifiedFlag();
		}
		break;
		case 5:
		{
			UQueryDetail *polQueryDetail = NULL;
			bool isNewQueryDetail = false;
			polQueryDetail = GetDocument()->pomCurrentQuery->GetQueryDetailByColumn(nCol);
			if(polQueryDetail == NULL)
			{
				polQueryDetail = new UQueryDetail();
				GetDocument()->pomCurrentQuery->omQueryDetails.Add(polQueryDetail);
				isNewQueryDetail = true;
			}
			polQueryDetail->imColNo = (int)nCol;
			olTable = GetValueRowCol(nRow-1, nCol);
			CGXCheckBox *polCeckBox = (CGXCheckBox *) GetControl(nRow, nCol);
			if(polCeckBox != NULL)
			{
				CString olCheck;
				polCeckBox->GetValue(olCheck);
				polQueryDetail->omShowAsResult = olCheck;
			}
			GetDocument()->SetModifiedFlag();
		}
		break;
		default:
			break;
	}
 
}

void CQueryDesignerGridView::OnClickedButtonRowCol(ROWCOL nRow, ROWCOL nCol)
{
	if(nRow == 5)
	{
		UQueryDetail *polQueryDetail = NULL;
		bool isNewQueryDetail = false;
		polQueryDetail = GetDocument()->pomCurrentQuery->GetQueryDetailByColumn(nCol);
		if(polQueryDetail == NULL)
		{
			polQueryDetail = new UQueryDetail();
			GetDocument()->pomCurrentQuery->omQueryDetails.Add(polQueryDetail);
			isNewQueryDetail = true;
		}
		polQueryDetail->imColNo = (int)nCol;
		CGXCheckBox *polCeckBox = (CGXCheckBox *) GetControl(nRow, nCol);
		if(polCeckBox != NULL)
		{
			CString olCheck;
			polCeckBox->GetValue(olCheck);
			polQueryDetail->omShowAsResult = olCheck;
		}
	}
}
BOOL CQueryDesignerGridView::GetStyleRowCol(ROWCOL nRow, ROWCOL nCol, CGXStyle& style, GXModifyType mt, int nType)
{
   if (!CGXGridView::GetStyleRowCol(nRow, nCol, style, mt, nType))
      return FALSE;

   return TRUE;
}




BOOL CQueryDesignerGridView::StoreStyleRowCol(ROWCOL nRow, 
											  ROWCOL nCol, 
											  const CGXStyle* pStyle, 
											  GXModifyType mt /*= gxOverride*/, int nType /*= 0*/)
{
	BOOL blRet = CGXGridView::StoreStyleRowCol(nRow, nCol, pStyle, mt, nType);
	return blRet;
}
BOOL CQueryDesignerGridView::OnEndEditing(ROWCOL nRow, ROWCOL nCol)
{
	BOOL blRet = CGXGridView::OnEndEditing(nRow, nCol);

	return blRet;
}