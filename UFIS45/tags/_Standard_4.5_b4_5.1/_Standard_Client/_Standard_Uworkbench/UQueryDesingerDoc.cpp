// UQueryDesingerDoc.cpp : implementation file
//

#include "stdafx.h"
#include "UWorkBench.h"
#include "UQueryDesingerDoc.h"
#include "QueryChildFrame.h"
#include "UniEingabe.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// UQueryDesingerDoc

IMPLEMENT_DYNCREATE(UQueryDesingerDoc, CDocument)

UQueryDesingerDoc::UQueryDesingerDoc()
{
	m_pParam = NULL;
	pomCurrentQuery = NULL;
//	pomCurrentQuery = new UQuery();
}

BOOL UQueryDesingerDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
	{
		return FALSE;
	}
//	SetTitle("New Query");
	return TRUE;
}

UQueryDesingerDoc::~UQueryDesingerDoc()
{
	if (m_pParam)
		delete m_pParam;
	m_pParam = NULL;
	if(pomCurrentQuery != NULL)
	{
//		delete pomCurrentQuery;
//		pomCurrentQuery = NULL;
	}
}


BEGIN_MESSAGE_MAP(UQueryDesingerDoc, CDocument)
	//{{AFX_MSG_MAP(UQueryDesingerDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// UQueryDesingerDoc diagnostics

#ifdef _DEBUG
void UQueryDesingerDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void UQueryDesingerDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// UQueryDesingerDoc serialization

void UQueryDesingerDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// UQueryDesingerDoc commands

BOOL UQueryDesingerDoc::CanCloseFrame(CFrameWnd* pFrame) 
{
	if(IsModified() == TRUE && pomCurrentQuery != NULL)
	{
		if (pFrame->IsKindOf(RUNTIME_CLASS(CQueryChildFrame)))
		{
			CQueryChildFrame *polQueryChildFrame;
			polQueryChildFrame = (CQueryChildFrame*)pFrame;
			if(pomCurrentQuery->omName.IsEmpty())
			{
				if(MessageBox(NULL, LoadStg(IDS_STRING61450), LoadStg(IDS_STRING61449), MB_YESNOCANCEL) == IDYES)
				{
					CString olQueryName;
					UniEingabe *polDlg = new UniEingabe(NULL, LoadStg(IDS_STRING61448), LoadStg(IDS_SAVE_AS), 32);
					int ilRet = polDlg->DoModal();
					if(ilRet == IDOK)
					{
						olQueryName = polDlg->m_Eingabe;
					}//fi
					pomCurrentQuery->omName = olQueryName;
					UQuery *polQuery = new UQuery;
					*polQuery = *pomCurrentQuery;
					ogQueries.Add(polQuery);
					//ogQueries.NewAt(ogQueries.GetSize(), *pomCurrentQuery);
					if(pomCurrentQuery != NULL)
					{
						delete pomCurrentQuery;
						pomCurrentQuery = NULL;
					}
//					SetModifiedFlag(TRUE);
					UpdateAllViews(NULL);
					delete polDlg;
				}
			}//fi MessageBox
			else
			{
				//TO DO Update
			}
		}
	}
	else
	{
//		if(pomCurrentQuery != NULL)
//		{
//			delete pomCurrentQuery;
//			pomCurrentQuery = NULL;
//		}
	}
//	return TRUE;	
	SetModifiedFlag(FALSE);
	return CDocument::CanCloseFrame(pFrame);
}

void UQueryDesingerDoc::OnCloseDocument() 
{
	// TODO: Add your specialized code here and/or call the base class
	
	CDocument::OnCloseDocument();
}

BOOL UQueryDesingerDoc::OnSaveDocument(LPCTSTR lpszPathName) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CDocument::OnSaveDocument(lpszPathName);
}

void UQueryDesingerDoc::SetTitle(LPCTSTR lpszTitle) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	CDocument::SetTitle(lpszTitle);
}

void UQueryDesingerDoc::SetCurrentQueryDesignerTables(CStringArray &opTabs)
{
	if(opTabs.GetSize() > 0)
	{
		omCurrentQueryDesingerTables.RemoveAll();
		if(pomCurrentQuery != NULL)
		{
			pomCurrentQuery->omUsedTables.RemoveAll();
			int ilCount = opTabs.GetSize();
			for(int i = 0; i < ilCount; i++)
			{
				omCurrentQueryDesingerTables.Add(opTabs[i]);
				pomCurrentQuery->omUsedTables.Add(opTabs[i]);
			}
			POSITION pos = GetFirstViewPosition();
			while (pos != NULL)
			{
				CView* pView = GetNextView(pos);
				if(pView != NULL)
				{
					if(pView->IsKindOf(RUNTIME_CLASS(CQueryDesignerGridView)))
					{
						LPARAM lParam=0;
						((CQueryDesignerGridView*)pView)->OnUpdate(NULL, lParam, NULL);
						//pView->OnUpdate(pSender, lHint, pHint);
					}
				}
			}

		}
		SetModifiedFlag( TRUE );

		//UpdateAllViews(NULL);
	}
}

void UQueryDesingerDoc::DeleteContents() 
{
	if (m_pParam)
		delete m_pParam;
	m_pParam = NULL;
}

BOOL UQueryDesingerDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
	CGXGridParam* pParam = m_pParam;

	m_pParam = NULL;

/*	if (!CDocument::OnOpenDocument(pszPathName))
	{
		SetModifiedFlag(FALSE);
		m_pParam = pParam;
		return FALSE;
	}
*/
	delete pParam;

	return TRUE;
}

void UQueryDesingerDoc::SetCurrentQuery(UQuery *popQuery)
{
	pomCurrentQuery = popQuery;
	if(pomCurrentQuery != NULL)
	{
		if(pomCurrentQuery->omName.IsEmpty())
		{
			SetTitle("New View");
			omCurrentQueryName = CString("New View");
		}
		else
		{
			SetTitle(pomCurrentQuery->omName);
			omCurrentQueryName = pomCurrentQuery->omName;
		}
	}//XXX
	UpdateAllViews(NULL);
}

CString UQueryDesingerDoc::GetQueryName()
{
	if(pomCurrentQuery != NULL)
	{
		return pomCurrentQuery->omName;
	}

	return "";
}

void UQueryDesingerDoc::SetNewQuery()
{
	if(pomCurrentQuery != NULL)
	{
		delete pomCurrentQuery;
		pomCurrentQuery = NULL;
	}
	pomCurrentQuery = new UQuery();
}

void UQueryDesingerDoc::DeleteRelation(CString opRelation)
{
	if(pomCurrentQuery != NULL)
	{
		for(int i = pomCurrentQuery->omObjRelation.GetSize()-1; i >= 0; i--)
		{
			if(opRelation == pomCurrentQuery->omObjRelation[i])
			{
				pomCurrentQuery->omObjRelation.RemoveAt(i);
			}
		}
	}
	//UpdateAllViews(NULL);
}
UQuery *UQueryDesingerDoc::GetQueryByName(CString opName)
{
	for(int i = 0; i < ogQueries.GetSize(); i++)
	{
		if(ogQueries[i].omName == opName)
		{
			return &ogQueries[i];
		}
	}
	return NULL;
}
