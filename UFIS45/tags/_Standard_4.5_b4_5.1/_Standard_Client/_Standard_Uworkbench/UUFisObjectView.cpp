// UUFisObjectView.cpp : implementation file
//

#include "stdafx.h"
#include "UWorkBench.h"
#include "UUFisObjectView.h"
#include "UDBLoadSetDefinitionDlg.h"
#include "GridObjectDefinitionDlg.h"
#include "GridWizzard1.h"
#include "FormDesignerDoc.h"
#include "FormDesignerView.h"
#include "FormDesignerMDI.h"
#include "FormExecuteDoc.h"
#include "DesignChoiceDlg.h"
#include "FormWizard.h"
#include "FormDialogDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// UUFisObjectView

IMPLEMENT_DYNCREATE(UUFisObjectView, CFormView)

UUFisObjectView::UUFisObjectView()
	: CFormView(UUFisObjectView::IDD)
{
	//{{AFX_DATA_INIT(UUFisObjectView)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	bmIsInit = false;
}

UUFisObjectView::~UUFisObjectView()
{
}

void UUFisObjectView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(UUFisObjectView)
	DDX_Control(pDX, IDC_Delete, m_DeleteB);
	DDX_Control(pDX, IDC_OPEN, m_OpenB);
	DDX_Control(pDX, IDC_NEW, m_NewB);
	DDX_Control(pDX, IDC_DESIGN, m_DesignB);
	DDX_Control(pDX, IDC_OBJECT_LIST, m_ObjectList);
	DDX_Control(pDX, IDC_OBJECTS, m_ObjectsTab);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(UUFisObjectView, CFormView)
	//{{AFX_MSG_MAP(UUFisObjectView)
	ON_WM_SIZE()
	ON_WM_MOVE()
	ON_WM_CLOSE()
	ON_NOTIFY(TCN_SELCHANGE, IDC_OBJECTS, OnSelchangeObjects)
	ON_BN_CLICKED(IDC_NEW, OnNew)
	ON_LBN_DBLCLK(IDC_OBJECT_LIST, OnDblclkObjectList)
	ON_BN_CLICKED(IDC_DESIGN, OnDesign)
	ON_BN_CLICKED(IDC_Delete, OnDelete)
	ON_BN_CLICKED(IDC_OPEN, OnOpen)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// UUFisObjectView diagnostics

#ifdef _DEBUG
void UUFisObjectView::AssertValid() const
{
	CFormView::AssertValid();
}

void UUFisObjectView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// UUFisObjectView message handlers

void UUFisObjectView::OnInitialUpdate() 
{
	CFormView::OnInitialUpdate();
	
	TC_ITEM TabCtrlItem;
	TabCtrlItem.mask = TCIF_TEXT;
	TabCtrlItem.pszText = "DB-Tables";
	m_ObjectsTab.InsertItem( 0, &TabCtrlItem );
	TabCtrlItem.pszText = "Grids";
	m_ObjectsTab.InsertItem( 1, &TabCtrlItem );
	TabCtrlItem.pszText = "Ganttcharts";
	m_ObjectsTab.InsertItem( 2, &TabCtrlItem );
	TabCtrlItem.pszText = "Queries";
	m_ObjectsTab.InsertItem( 3, &TabCtrlItem );
	TabCtrlItem.pszText = "Froms";
	m_ObjectsTab.InsertItem( 4, &TabCtrlItem );
	TabCtrlItem.pszText = "DB-Data Load Set";
	m_ObjectsTab.InsertItem( 5, &TabCtrlItem );
	bmIsInit = true;
	m_ObjectList.SetColumnWidth( 200 );

//	InitTablePage();
	InitTablePage();
//	InitGridsPage();
//	InitGanttsPage();
//	InitQueriesPage();
//	InitFormsPage();
//	InitDBLoadSetPage();
	
	UQueryDesingerDoc *polDoc = (UQueryDesingerDoc *)GetDocument();
	if(polDoc != NULL)
	{
		polDoc->SetTitle("UFIS - Workbench Objects");
	}
	CRect olRect = CRect(0, 0, 520, 380);
	MoveWindow(&olRect);

}

void UUFisObjectView::OnSize(UINT nType, int cx, int cy) 
{
	CFormView::OnSize(nType, cx, cy);
	if(bmIsInit == true)
	{
		CRect olRect;	
		int ilOffSet = 80;
		GetClientRect(&olRect);
		olRect.top += 10;
		olRect.bottom -= 10;
		olRect.left += 10;
		olRect.right -= 10;
		m_ObjectsTab.MoveWindow(&olRect);
		//--Liste
		m_ObjectsTab.GetClientRect(&olRect);
		olRect.top += 40;
		olRect.bottom += 0;
		olRect.left += 20;
		olRect.right += 5 - ilOffSet;
		m_ObjectList.MoveWindow(&olRect);

		CRect OpenB, NewB, DesignB, DeleteB;
		
		m_ObjectsTab.GetClientRect(&olRect);
		m_OpenB.GetWindowRect(&OpenB);
		int ilBWidth = OpenB.right - OpenB.left;
		ScreenToClient(&OpenB);
		//olRect.right+=30;
		OpenB.right = olRect.right + 5;
		OpenB.left = OpenB.right - ilBWidth;
		m_OpenB.MoveWindow(&OpenB);

		m_DesignB.GetWindowRect(&DesignB);
		ScreenToClient(&DesignB);
		DesignB.right = olRect.right + 5;
		DesignB.left = DesignB.right - ilBWidth;
		m_DesignB.MoveWindow(&DesignB);

		m_NewB.GetWindowRect(&NewB);
		ScreenToClient(&NewB);
		NewB.right = olRect.right  + 5;
		NewB.left = NewB.right - ilBWidth;
		m_NewB.MoveWindow(&NewB);

		m_DeleteB.GetWindowRect(&NewB);
		ScreenToClient(&NewB);
		NewB.right = olRect.right  + 5;
		NewB.left = NewB.right - ilBWidth;
		m_DeleteB.MoveWindow(&NewB);
		
	}
}

void UUFisObjectView::OnMove(int x, int y) 
{
	CFormView::OnMove(x, y);
	if(bmIsInit == true)
	{

		CRect olRect;	
		m_ObjectsTab.GetClientRect(&olRect);
		olRect.top += 20;
		olRect.bottom -= 20;
		olRect.left += 20;
		olRect.right -= 20;
		m_ObjectsTab.MoveWindow(&olRect);

	}
	
	// TODO: Add your message handler code here
	
}

void UUFisObjectView::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
	
	CFormView::OnClose();
}

void UUFisObjectView::OnSelchangeObjects(NMHDR* pNMHDR, LRESULT* pResult) 
{
	int ilPage = m_ObjectsTab.GetCurSel();

	switch(ilPage)
	{
	case 0:
		imCurrentPage = PAGE_DB_TABLES;
		InitTablePage();
		break;
	case 1:
		imCurrentPage = PAGE_GRIDS;
		InitGridsPage();
		break;
	case 2:
		imCurrentPage = PAGE_GANTTS;
		InitGanttsPage();
		break;
	case 3:
		imCurrentPage = PAGE_QUERIES;
		InitQueriesPage();
		break;
	case 4:
		imCurrentPage = PAGE_FORMS;
		InitFormsPage();
		break;
	case 5:
		imCurrentPage = PAGE_DBLOADSET;
		InitDBLoadSetPage();
		break;
	default:
		break;
	}
	
	*pResult = 0;
}

void UUFisObjectView::InitTablePage()
{
	POSITION pos;

	m_ObjectList.ResetContent();
	for( pos = ogTanaMap.GetStartPosition(); pos != NULL; )
	{
		CString olKey;
		void *pNul;
		ogTanaMap.GetNextAssoc( pos, olKey, pNul);
		m_ObjectList.AddString(olKey);
	}

}

void UUFisObjectView::InitGridsPage()
{
	m_ObjectList.ResetContent();
	for(int i = 0; i < ogGridDefs.GetSize(); i++)
	{
		m_ObjectList.AddString(ogGridDefs[i].GridName);
	}
}

void UUFisObjectView::InitGanttsPage()
{
	m_ObjectList.ResetContent();
}

void UUFisObjectView::InitQueriesPage()
{
	m_ObjectList.ResetContent();
	UQueryDesingerDoc *polDoc = (UQueryDesingerDoc *)GetDocument();
	if(polDoc != NULL)
	{
		for(int i = 0; i < ogQueries.GetSize(); i++)
		{
			UQuery *polQ = &ogQueries[i];
			CString olQ = ogQueries[i].omName;
			m_ObjectList.AddString(ogQueries[i].omName);
		}
		polDoc->SetModifiedFlag(FALSE);
	}
}

void UUFisObjectView::InitFormsPage()
{
	m_ObjectList.ResetContent();
	for(int i = 0; i < ogForms.GetSize(); i++)
	{
		m_ObjectList.AddString(ogForms[i].omName);
	}
}

void UUFisObjectView::InitDBLoadSetPage()
{
	m_ObjectList.ResetContent();
	for(int i = 0; i < ogLoadSets.GetSize(); i++)
	{
		m_ObjectList.AddString(ogLoadSets[i].Name);
	}
}



void UUFisObjectView::OnNew() 
{
	UQueryDesingerDoc *polDoc = (UQueryDesingerDoc *)GetDocument();
	if(polDoc != NULL)
	{
		switch(imCurrentPage)
		{
		case PAGE_DB_TABLES:
			break;
		case PAGE_GRIDS://
			{
				UGridDefinition *polGridDef = new UGridDefinition();
				GridWizzard1 *polDlg = new GridWizzard1(this, polGridDef);
				int ilRet = -1;
				ilRet = polDlg->DoModal();
				if(ilRet != IDCANCEL)
				{
					ogGridDefs.Add(polGridDef);
					InitGridsPage();
				}
				else
				{
					delete polGridDef;
				}
				delete polDlg;
			}
			break;
		case PAGE_GANTTS:
			break;
		case PAGE_QUERIES:
			((CUWorkBenchApp*)AfxGetApp())->pMainFrame->OnNewQueryDesingerWindow();
			break;
		case PAGE_FORMS:
			{
				DesignChoiceDlg olDlg(this);
				if(olDlg.DoModal() == IDOK)
				{
					if(olDlg.m_Choice == 0)
					{
						FormDesignerDoc *polDoc;
						polDoc = (FormDesignerDoc*)((CMultiDocTemplate*)((CUWorkBenchApp*)AfxGetApp())->pFormDesignerTpl->OpenDocumentFile(NULL));
					}
					if(olDlg.m_Choice == 1)
					{
						UFormProperties *polForm = new UFormProperties;
						FormWizard olDlg(this, polForm);
						if(olDlg.DoModal() == IDOK)
						{
							ogForms.Add(polForm);
						}
						else
						{
							delete polForm;
						}
						InitFormsPage();
					}
				}
			}
			break;
		case PAGE_DBLOADSET:
			{
				UDBLoadSet *polLoadSet = new UDBLoadSet();
				UDBLoadSetDefinitionDlg *polDlg = new UDBLoadSetDefinitionDlg(this, polLoadSet);
				if(polDlg->DoModal() == IDOK)
				{
					ogLoadSets.Add(polLoadSet);
					InitDBLoadSetPage();
				}
				else
				{
					delete polLoadSet;
				}
				delete polDlg;
			}
			break;
		}
	}
	((CUWorkBenchApp*)AfxGetApp())->pMainFrame->UpdateTabView();
}

void UUFisObjectView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
	switch(imCurrentPage)
	{
	case PAGE_DB_TABLES:
		break;
	case PAGE_GRIDS:
		break;
	case PAGE_GANTTS:
		break;
	case PAGE_QUERIES:
		InitQueriesPage();
		break;
	case PAGE_FORMS:
		InitFormsPage();
		break;
	case PAGE_DBLOADSET:
		InitDBLoadSetPage();
		break;
	default:
		break;
	}
	((CUWorkBenchApp*)AfxGetApp())->pMainFrame->UpdateTabView();
}

void UUFisObjectView::OnDblclkObjectList() 
{
	switch(imCurrentPage)
	{
	case PAGE_DB_TABLES:
		break;
	case PAGE_GRIDS:
		PerFormRunGrid();
		//PerformOpenGird();
		break;
	case PAGE_GANTTS:
		break;
	case PAGE_QUERIES:
		PerFormOpenQuery();
		break;
	case PAGE_FORMS:
		PerformExecuteForm();
		break;
	case PAGE_DBLOADSET:
		PerFormOpenLoadSet();
		break;
	default:
		break;
	}
}

void UUFisObjectView::PerformOpenGird()
{
	int ilIdx = m_ObjectList.GetCurSel();
	if(ilIdx != LB_ERR)
	{
		CString olText;
		m_ObjectList.GetText(ilIdx, olText);
		UGridDefinition *polGridDef = NULL;
		int ilGridDefIndex=-1;
		for(int i = 0; i < ogGridDefs.GetSize(); i++)
		{
			if(ogGridDefs[i].GridName == olText)
			{
				polGridDef = &ogGridDefs[i];
				ilGridDefIndex = i;
				i = ogGridDefs.GetSize();
			}
		}
		if(polGridDef != NULL)
		{
			UGridDefinition *polGridTmp = new UGridDefinition();
			*polGridTmp = *polGridDef;
			GridWizzard1 *polDlg = new GridWizzard1(this, polGridTmp);
			int ilRet = -1;
			ilRet = polDlg->DoModal();
			if(ilRet != IDCANCEL) 
			{
//				ogGridDefs.SetAt(ilGridDefIndex, polGridTmp);
				*polGridDef = *polGridTmp;
//				delete polGridDef;
			}
			delete polDlg;
			delete polGridTmp;
		}
	}
}

void UUFisObjectView::PerFormOpenLoadSet()
{
	int ilIdx = m_ObjectList.GetCurSel();
	if(ilIdx != LB_ERR)
	{
		CString olText;
		m_ObjectList.GetText(ilIdx, olText);
		UDBLoadSet *polTab = NULL;
		for(int i = 0; i < ogLoadSets.GetSize(); i++)
		{
			if(ogLoadSets[i].Name == olText)
			{
				polTab = &ogLoadSets[i];
				i = ogLoadSets.GetSize();
			}
		}
		if(polTab != NULL)
		{
			UDBLoadSet *polTabTmp = new UDBLoadSet();
			*polTabTmp = *polTab;
			int ilTmp = polTab->TableArray.GetSize();
			UDBLoadSetDefinitionDlg *polDlg = new UDBLoadSetDefinitionDlg(this, polTabTmp);
			if(polDlg->DoModal() == IDOK)
			{
				*polTab = *polTabTmp;
			}
			ilTmp = polTab->TableArray.GetSize();
			delete polDlg;
			delete polTabTmp;

		}
	}
}

void UUFisObjectView::PerFormOpenQuery()
{
	UQueryDesingerDoc *polDoc = (UQueryDesingerDoc *)GetDocument();
	if(polDoc != NULL)
	{
		int ilIdx = m_ObjectList.GetCurSel();
		if(ilIdx != LB_ERR)
		{
			CString olText;
			m_ObjectList.GetText(ilIdx, olText);
			UQuery *polQuery;
			for(int i = 0; i < ogQueries.GetSize(); i++)
			{
//				CString olT = ogQueries[i].omName;
				if(ogQueries[i].omName == olText)
				{
					polQuery = &ogQueries[i];
				}
			}
			//= polDoc->GetQueryByName(olText);
			if(polQuery != NULL)
			{
				//polDoc->SetCurrentQuery(polQuery);
				((CUWorkBenchApp*)AfxGetApp())->pMainFrame->OnNewQueryDesingerWindow(polQuery);
			}
		}
	}
}

void UUFisObjectView::OnDesign() 
{
	switch(imCurrentPage)
	{
	case PAGE_DB_TABLES:
		break;
	case PAGE_GRIDS:
		PerformOpenGird();
		break;
	case PAGE_GANTTS:
		break;
	case PAGE_QUERIES:
		PerFormOpenQuery();
		break;
	case PAGE_FORMS:
		{
			PerformOpenForm();
/*			DesignChoiceDlg olDlg(this);
			if(olDlg.DoModal() == IDOK)
			{
				if(olDlg.m_Choice == 0)
				{
					PerformOpenForm();
				}
				if(olDlg.m_Choice == 1)
				{
					int ilIdx = m_ObjectList.GetCurSel();
					if(ilIdx != LB_ERR)
					{
						CString olText;
						m_ObjectList.GetText(ilIdx, olText);
						UFormProperties *polForm = NULL;
						for(int i = 0; i < ogForms.GetSize(); i++)
						{
							if(ogForms[i].omName == olText)
							{
								polForm = &ogForms[i];
								i = ogForms.GetSize();
							}
						}
						if(polForm != NULL)
						{
							FormWizard olDlg(this, polForm);
							if(olDlg.DoModal() == IDOK)
							{
							}
						}
					}
				}
			}
*/
		}
		break;
	case PAGE_DBLOADSET:
		PerFormOpenLoadSet();
		break;
	default:
		break;
	}
	((CUWorkBenchApp*)AfxGetApp())->pMainFrame->UpdateTabView();
}

void UUFisObjectView::PerFormRunGrid()
{
	int ilIdx = m_ObjectList.GetCurSel();
	if(ilIdx != LB_ERR)
	{
		CString olText;
		m_ObjectList.GetText(ilIdx, olText);
		UGridDefinition *polGridDef = NULL;
		for(int i = 0; i < ogGridDefs.GetSize(); i++)
		{
			if(ogGridDefs[i].GridName == olText)
			{
				polGridDef = &ogGridDefs[i];
				i = ogGridDefs.GetSize();
			}
		}
		if(polGridDef != NULL)
		{
			((CUWorkBenchApp*)AfxGetApp())->pMainFrame->OnShowUFISGrid(polGridDef);
		}
	}
}

void UUFisObjectView::PerformOpenForm()
{
	int ilIdx = m_ObjectList.GetCurSel();
	if(ilIdx != LB_ERR)
	{
		CString olText;
		m_ObjectList.GetText(ilIdx, olText);
		UFormProperties *polForm = NULL;
		for(int i = 0; i < ogForms.GetSize(); i++)
		{
			if(ogForms[i].omName == olText)
			{
				polForm = &ogForms[i];
				i = ogForms.GetSize();
			}
		}
		if(polForm != NULL)
		{
			FormDesignerDoc *polDoc;
			polDoc = (FormDesignerDoc*)((CMultiDocTemplate*)((CUWorkBenchApp*)AfxGetApp())->pFormDesignerTpl->OpenDocumentFile((LPCTSTR )polForm));
		}
	}
}

void UUFisObjectView::PerformExecuteForm()
{
	int ilIdx = m_ObjectList.GetCurSel();
	if(ilIdx != LB_ERR)
	{
		CString olText;
		m_ObjectList.GetText(ilIdx, olText);
		UFormProperties *polForm = NULL;
		for(int i = 0; i < ogForms.GetSize(); i++)
		{
			if(ogForms[i].omName == olText)
			{
				polForm = &ogForms[i];
				i = ogForms.GetSize();
			}
		}
		if(polForm != NULL)
		{
			if(polForm->bmModal == TRUE)
			{
				FormDialogDlg *polDlg = new FormDialogDlg(this, "INSERT", polForm, "", NULL);
				polDlg->DoModal();
				delete polDlg;
			}
			else
			{
				FormExecuteDoc *polDoc;
				polDoc = (FormExecuteDoc*)((CMultiDocTemplate*)((CUWorkBenchApp*)AfxGetApp())->pFormExecuteTpl->OpenDocumentFile((LPCTSTR )polForm));
			}
		}
	}
}



void UUFisObjectView::OnDelete() 
{
	switch(imCurrentPage)
	{
	case PAGE_DB_TABLES:
		break;
	case PAGE_GRIDS:
		PerformDeleteGrid();
		break;
	case PAGE_GANTTS:
		break;
	case PAGE_QUERIES:
		PerformDeleteQuery();
		break;
	case PAGE_FORMS:
		PerformDeleteForm();
		break;
	case PAGE_DBLOADSET:
		//PerformDeleteLoadSet();
		break;
	default:
		break;
	}
	((CUWorkBenchApp*)AfxGetApp())->pMainFrame->UpdateTabView();
}

void UUFisObjectView::OnOpen() 
{
	OnDblclkObjectList();
}

//--------------------------------------------------------
// Functions to delete objects (Grids,Queries,Forms)
//--------------------------------------------------------
void UUFisObjectView::PerformDeleteGrid()
{
	int ilIdx = m_ObjectList.GetCurSel();
	if(ilIdx != LB_ERR)
	{
		CString olText;
		m_ObjectList.GetText(ilIdx, olText);
		int ilIndex = -1;
		for(int i = 0; i < ogGridDefs.GetSize(); i++)
		{
			if(ogGridDefs[i].GridName == olText)
			{
				ilIndex = i;
			}
		}
		if(ilIndex != -1)
		{
			CString olMsg;
			olMsg = CString("Do you really want to delete Grid: ") + olText;
			if(MessageBox(olMsg, "Delete...", (MB_YESNOCANCEL)) == IDYES)
			{
				ogGridDefs.DeleteAt(ilIndex);
				InitGridsPage();
			}
		}
	}
}
//-----------------------------------------------------------
//-----------------------------------------------------------
void UUFisObjectView::PerformDeleteForm()
{
	int ilIdx = m_ObjectList.GetCurSel();
	if(ilIdx != LB_ERR)
	{
		CString olText;
		int ilIndex = -1;
		m_ObjectList.GetText(ilIdx, olText);
		for(int i = 0; i < ogForms.GetSize(); i++)
		{
			if(ogForms[i].omName == olText)
			{
				ilIndex = i;
			}
		}
		if(ilIndex != -1)
		{
			CString olMsg;
			olMsg = CString("Do you really want to delete Form: ") + olText;
			if(MessageBox(olMsg, "Delete...", (MB_YESNOCANCEL)) == IDYES)
			{
				ogForms.DeleteAt(ilIndex);
				InitFormsPage();
			}
		}
	}
}
//-----------------------------------------------------------
//-----------------------------------------------------------
void UUFisObjectView::PerformDeleteQuery()
{
	UQueryDesingerDoc *polDoc = (UQueryDesingerDoc *)GetDocument();
	if(polDoc != NULL)
	{
		int ilIdx = m_ObjectList.GetCurSel();
		if(ilIdx != LB_ERR)
		{
			CString olText;
			m_ObjectList.GetText(ilIdx, olText);
			int ilIndex = -1;
			for(int i = 0; i < ogQueries.GetSize(); i++)
			{
				if(ogQueries[i].omName == olText)
				{
					ilIndex = i;
				}
			}
			if(ilIndex != -1)
			{
				CString olMsg;
				olMsg = CString("Do you really want to delete Query: ") + olText;
				if(MessageBox(olMsg, "Delete...", (MB_YESNOCANCEL)) == IDYES)
				{
					ogQueries.DeleteAt(ilIndex);
					InitQueriesPage();
				}
			}
		}
	}
}
