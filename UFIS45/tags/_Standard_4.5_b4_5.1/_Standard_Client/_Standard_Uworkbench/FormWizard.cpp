// FormWizard.cpp : implementation file
//
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *	UWorkBench - Project
 *	
 *		CWinApp Mainclass 
 *
 *	ABB Airport Technologies GmbH 2000
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 *
 *		Date		Author			Comment
 *		-------------------------------------------------------------
 *
 *		????????	mwo AAT/IR		Initial version	
 *		29/01/01	cla AAT/ID		Hidden Refe for Combo added 
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "stdafx.h"
#include "UWorkBench.h"
#include "FormWizard.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// FormWizard dialog


FormWizard::FormWizard(CWnd* pParent, UFormProperties *popProperty)
	: CDialog(FormWizard::IDD, pParent)
{
	pomProperty = popProperty;
	//{{AFX_DATA_INIT(FormWizard)
	v_FormName = _T("");
	//}}AFX_DATA_INIT
	omPropertyGrid.EnableAutoGrow(false);
}


void FormWizard::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(FormWizard)
	DDX_Control(pDX, IDC_TABLES, m_Tables);
	DDX_Control(pDX, IDC_SOURCEFIELDS, m_SourceFields);
	DDX_Control(pDX, IDC_FORM_NAME, m_FormName);
	DDX_Text(pDX, IDC_FORM_NAME, v_FormName);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(FormWizard, CDialog)
	//{{AFX_MSG_MAP(FormWizard)
	ON_BN_CLICKED(IDC_SINGLETOLEFT, OnSingletoleft)
	ON_BN_CLICKED(IDC_SINGLETORIGHT, OnSingletoright)
	ON_LBN_SELCHANGE(IDC_SOURCEFIELDS, OnSelchangeSourcefields)
	ON_CBN_SELCHANGE(IDC_TABLES, OnSelchangeTables)
	ON_BN_CLICKED(IDC_MULTITORIGHT, OnMultitoright)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// FormWizard message handlers

void FormWizard::OnSingletoleft() 
{
	CString olTable;
	m_Tables.GetWindowText(olTable);
	CRowColArray olRows;

	omPropertyGrid.GetSelectedRows(olRows);
	for(int i = olRows.GetSize()-1; i >= 0; i--)
	//while(olRows.GetSize() > 0)
	{
		if(olRows[i] != 0)
		{
			CString olTab, olField;
			olTab = omPropertyGrid.GetValueRowCol(olRows[i], 1);
			olField = omPropertyGrid.GetValueRowCol(olRows[i], 2);
			if(olTable == olTab)
			{
				m_SourceFields.AddString(olField);
			}
			omPropertyGrid.RemoveRows(olRows[i], olRows[i]);
			bool blFound = false;
			for(int j = 0; (blFound == false) && (j < (int)omPropertyGrid.GetRowCount()); j++)
			{
				CString olT = omPropertyGrid.GetValueRowCol(j+1, 1);
				if(olTab == omPropertyGrid.GetValueRowCol(j+1, 1))
				{
					blFound = true;
				}
			}
			if(blFound == false)
			{
				int ilTabRows = omTabPropertyGrid.GetRowCount();
				bool blTabFound = false;
				int  ilLine;
				for(int j = 0; (blTabFound == false) && (j < ilTabRows); j++)
				{
					CString olV = omTabPropertyGrid.GetValueRowCol(j+1, 1); 
					if( olTab == omTabPropertyGrid.GetValueRowCol(j+1, 1))
					{
						ilLine = j+1;
						blTabFound = true;
					}
				}
				if(blTabFound == true)
				{
					omTabPropertyGrid.RemoveRows(ilLine, ilLine);

				}
			}
		}
	}

}

void FormWizard::OnSingletoright() 
{
	CString olTable;
	m_Tables.GetWindowText(olTable);
	if(!olTable.IsEmpty())
	{
		int ilIdx = m_SourceFields.GetCurSel();	
		if(ilIdx != LB_ERR)
		{
			CString olField;
			m_SourceFields.GetText(ilIdx, olField);
			omPropertyGrid.InsertRows(omPropertyGrid.GetRowCount( )+1, 1);
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 1), olTable);
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 2), olField);
			CString olUrno = ogBCD.GetFieldExt("SYS", "TANA", "FINA", olTable, olField, "URNO");
			CString olHeader;
			CString olType;
			CString olRefe;
			CString olReqf;
			RecordSet olRec;
			ogBCD.GetRecord("SYS", "URNO", olUrno, olRec);
			olHeader = olRec[ogBCD.GetFieldIndex("SYS", "LABL")];
			olHeader.TrimLeft();olHeader.TrimRight();
			olType = olRec[ogBCD.GetFieldIndex("SYS", "TYPE")];
			olReqf = olRec[ogBCD.GetFieldIndex("SYS", "REQF")];
			if(olHeader.IsEmpty())
			{
				olHeader = olRec[ogBCD.GetFieldIndex("SYS", "ADDI")];
			}
			olRefe = olRec[ogBCD.GetFieldIndex("SYS", "REFE")];
			if(olRefe ==".")
			{
				olRefe.Empty();
			}
			CString olChoiceList;
			for(int i = 0; i < ogQueries.GetSize(); i++)
			{
				olChoiceList += ogQueries[i].omName + CString("\n");
			}
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 3), olType);
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 4), olHeader);

			omPropertyGrid.SetStyleRange(CGXRange(omPropertyGrid.GetRowCount( ), 5), CGXStyle().SetChoiceList("")
															.SetHorizontalAlignment(DT_CENTER)
															.SetUserAttribute(GX_IDS_UA_CHECKBOX_CHECKED, _T("1"))
															.SetUserAttribute(GX_IDS_UA_CHECKBOX_UNCHECKED, _T("0"))
															.SetControl(GX_IDS_CTRL_CHECKBOX3D));
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 5), _T("1"));
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 7), olRefe);
			omPropertyGrid.SetStyleRange(CGXRange(omPropertyGrid.GetRowCount( ), 8), CGXStyle().SetChoiceList("")
															.SetHorizontalAlignment(DT_CENTER)
															.SetUserAttribute(GX_IDS_UA_CHECKBOX_CHECKED, _T("1"))
															.SetUserAttribute(GX_IDS_UA_CHECKBOX_UNCHECKED, _T("0"))
															.SetControl(GX_IDS_CTRL_CHECKBOX3D));
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 9), olRefe);

			if(olReqf == "Y")
			{
				omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 8), _T("1"));
			}
			else
			{
				omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 8), _T("0"));
			}

			m_SourceFields.DeleteString(ilIdx);
			int ilTabRows = omTabPropertyGrid.GetRowCount();
			bool blFound = false;
			for(int j = 0; (blFound == false) && (j < ilTabRows); j++)
			{
				if( olTable == omTabPropertyGrid.GetValueRowCol(j+1, 1))
				{
					blFound = true;
				}
			}
			if( blFound == false)
			{
				omTabPropertyGrid.InsertRows(omPropertyGrid.GetRowCount( )+1, 1);
				omTabPropertyGrid.SetValueRange(CGXRange(omTabPropertyGrid.GetRowCount( ), 1), olTable);
				omTabPropertyGrid.SetValueRange(CGXRange(omTabPropertyGrid.GetRowCount( ), 2), "");
			}
		}
	}
}

void FormWizard::OnSelchangeSourcefields() 
{
	// TODO: Add your control notification handler code here
	
}

void FormWizard::OnSelchangeTables() 
{
	int ilIdx = m_Tables.GetCurSel();	
	m_SourceFields.ResetContent();
	if(ilIdx != LB_ERR)
	{
		CString olText;
		m_Tables.GetLBText(ilIdx, olText);
		CCSPtrArray<RecordSet> olRecords;

		ogBCD.GetRecords("SYS", "TANA", olText, &olRecords);
		for(int i = 0; i < olRecords.GetSize(); i++)
		{
			m_SourceFields.AddString(olRecords[i].Values[ogBCD.GetFieldIndex("SYS", "FINA")]);
		}
		olRecords.DeleteAll();
	}
}

void FormWizard::OnOK() 
{
	if(GetValues() == true)
	{
		CDialog::OnOK();
	}
}

BOOL FormWizard::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_SourceFields.SetFont(&ogCourier_Regular_8);
	POSITION pos;

	for( pos = ogTanaMap.GetStartPosition(); pos != NULL; )
	{
		CString olKey;
		void *pNul;
		ogTanaMap.GetNextAssoc( pos, olKey, pNul);
		m_Tables.AddString(olKey);
	}

	m_FormName.SetWindowText(pomProperty->omName);
	v_FormName = pomProperty->omName;
//Fields Grid
	CString olHeader;
	int ilRowCount = pomProperty->omDBFields.GetSize();
	/*
	 * <<<cla: Hidden Refe added
	 */
	olHeader = "Table,Field,Type,Header,Visible,Key,Refe (systab),Mandatory,Hidden Refe";//LoadStg(IDS_STRING61454);
	omPropertyGrid.SubclassDlgItem(IDC_FIELD_PROPERTIES, this);
	omPropertyGrid.Initialize();
	omPropertyGrid.GetParam()->EnableUndo(FALSE);
	omPropertyGrid.LockUpdate(TRUE);
	CGXStyle olHeaderStyle;
	CStringArray olHeaderList;
	ExtractItemList(olHeader, &olHeaderList, ',');
	omPropertyGrid.SetRowCount(ilRowCount);
	imColCount = olHeaderList.GetSize();
	omPropertyGrid.SetColCount(imColCount);

	CGXComboBoxWnd *pWnd = new CGXComboBoxWnd(&omPropertyGrid);
	pWnd->Create(WS_VSCROLL | CBS_DROPDOWN | CBS_AUTOHSCROLL | CBS_SORT, 0);
	pWnd->m_bFillWithChoiceList = TRUE;
	pWnd->m_bWantArrowKeys = TRUE;
	omPropertyGrid.RegisterControl(GX_IDS_CTRL_CBS_DROPDOWN, pWnd);

	CGXCheckBox *polCeckBox = new CGXCheckBox((CGXGridCore*)(&omPropertyGrid), TRUE, GX_IDB_CHECK_95);
	omPropertyGrid.RegisterControl(GX_IDS_CTRL_CHECKBOX3D, polCeckBox);

//Table,Field,Type,Header,Visible,Key,Query
	omPropertyGrid.SetColWidth(1,1,40);
	omPropertyGrid.SetColWidth(2,2,40);
	omPropertyGrid.SetColWidth(3,3,40);
	omPropertyGrid.SetColWidth(4,4,160);
	omPropertyGrid.SetColWidth(5,5,40);
	omPropertyGrid.SetColWidth(6,6,60);
	omPropertyGrid.SetColWidth(7,7,160);
	omPropertyGrid.SetColWidth(8,8,80);
	omPropertyGrid.SetColWidth(9,9,100); // Hidden refe field

	for(int i = 0; i < olHeaderList.GetSize(); i++)
	{
		olHeaderStyle.SetValue(olHeaderList[i]);
		omPropertyGrid.SetStyleRange(CGXRange(0, i+1, 0, i+1 ), olHeaderStyle);
	}

	CString olChoiceList;
	for( i = 0; i < ogQueries.GetSize(); i++)
	{
		olChoiceList += ogQueries[i].omName + CString("\n");
	}
	for(i = 0; i < pomProperty->omDBFields.GetSize(); i++)
	{
		omPropertyGrid.SetValueRange(CGXRange(i+1, 1), pomProperty->omDBFields[i].Table);
		omPropertyGrid.SetValueRange(CGXRange(i+1, 2), pomProperty->omDBFields[i].Field);
		omPropertyGrid.SetValueRange(CGXRange(i+1, 3), pomProperty->omDBFields[i].Type);
		omPropertyGrid.SetValueRange(CGXRange(i+1, 4), pomProperty->omDBFields[i].Header);
		omPropertyGrid.SetStyleRange(CGXRange(i+1, 5), CGXStyle().SetChoiceList("")
														.SetHorizontalAlignment(DT_CENTER)
														.SetUserAttribute(GX_IDS_UA_CHECKBOX_CHECKED, _T("1"))
														.SetUserAttribute(GX_IDS_UA_CHECKBOX_UNCHECKED, _T("0"))
														.SetControl(GX_IDS_CTRL_CHECKBOX3D));
		CString olCheck = pomProperty->omDBFields[i].Visible;
		omPropertyGrid.SetValueRange(CGXRange(i+1, 5), olCheck);
		omPropertyGrid.SetValueRange(CGXRange(i+1, 6), pomProperty->omDBFields[i].Key);
/*		omPropertyGrid.SetStyleRange(CGXRange(i+1, 7), CGXStyle().SetHorizontalAlignment(DT_LEFT)
												.SetVerticalAlignment(DT_BOTTOM)
												.SetControl(GX_IDS_CTRL_COMBOBOX)
												.SetChoiceList(olChoiceList));
*/
//		omPropertyGrid.SetValueRange(CGXRange(i+1, 7), pomProperty->omDBFields[i].QueryName);
		omPropertyGrid.SetValueRange(CGXRange(i+1, 7), pomProperty->omDBFields[i].Refe);
		omPropertyGrid.SetStyleRange(CGXRange(i+1, 8), CGXStyle().SetChoiceList("")
														.SetHorizontalAlignment(DT_CENTER)
														.SetUserAttribute(GX_IDS_UA_CHECKBOX_CHECKED, _T("1"))
														.SetUserAttribute(GX_IDS_UA_CHECKBOX_UNCHECKED, _T("0"))
														.SetControl(GX_IDS_CTRL_CHECKBOX3D));
		omPropertyGrid.SetValueRange(CGXRange(i+1, 9), pomProperty->omDBFields[i].HiddenRefe);

	}
	CString olTypes;
	olTypes = LoadStg(IDS_FIELD_TYPES);


	omPropertyGrid.LockUpdate(FALSE);
	omPropertyGrid.GetParam()->EnableUndo(TRUE);


// Table Property Grid
	omTabPropertyGrid.SubclassDlgItem(IDC_TABLE_PROPERTIES, this);

	omTabPropertyGrid.Initialize();
	omTabPropertyGrid.GetParam()->EnableUndo(FALSE);
	omTabPropertyGrid.LockUpdate(TRUE);

	ilRowCount = pomProperty->omTables.GetSize();
	olHeaderList.RemoveAll();
	olHeader = LoadStg(IDS_STRING61456);
	ExtractItemList(olHeader, &olHeaderList, ',');
	omTabPropertyGrid.SetRowCount(ilRowCount);
	imColCount = olHeaderList.GetSize();
	omTabPropertyGrid.SetColCount(imColCount);


	//Table,Field,Type,Header,Visible,Key,Query
	omTabPropertyGrid.SetColWidth(0,1,20);
	omTabPropertyGrid.SetColWidth(1,1,70);
	omTabPropertyGrid.SetColWidth(2,2,60);

	for( i = 0; i < olHeaderList.GetSize(); i++)
	{
		olHeaderStyle.SetValue(olHeaderList[i]);
		omTabPropertyGrid.SetStyleRange(CGXRange(0, i+1, 0, i+1 ), olHeaderStyle);
	}

	for(i = 0; i < pomProperty->omTables.GetSize(); i++)
	{
		omTabPropertyGrid.SetValueRange(CGXRange(i+1, 1), pomProperty->omTables[i]);
		omTabPropertyGrid.SetValueRange(CGXRange(i+1, 2), ""); //For the moment	
	}

	omTabPropertyGrid.LockUpdate(FALSE);


	UpdateData(FALSE);


	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

bool FormWizard::GetValues()
{
	if(UpdateData() == FALSE)
		return false;

	if(v_FormName.IsEmpty())
	{
		MessageBox(LoadStg(IDS_STRING61452), LoadStg(IDS_STRING61453), MB_OK);
		m_FormName.SetFocus();	
		return false;
	}


	int i=0;

	int ilRows = omPropertyGrid.GetRowCount();
	pomProperty->omDBFields.DeleteAll();
	if(ilRows > 0)
	{
		pomProperty->omTable = omTabPropertyGrid.GetValueRowCol(1, 1);
		CString olTabField;
		int ilRealToReadRows = 0;
		for(i = 0; i < ilRows; i++)
		{
			UFormField *polFormFields = new UFormField();
			polFormFields->Table =		omPropertyGrid.GetValueRowCol(i+1, 1);
			polFormFields->Field =		omPropertyGrid.GetValueRowCol(i+1, 2);

			polFormFields->Type =		omPropertyGrid.GetValueRowCol(i+1, 3);
			polFormFields->Header =		omPropertyGrid.GetValueRowCol(i+1, 4);
			polFormFields->Visible = omPropertyGrid.GetValueRowCol(i+1, 5);
			polFormFields->Key =		omPropertyGrid.GetValueRowCol(i+1, 6);
			polFormFields->Refe =	omPropertyGrid.GetValueRowCol(i+1, 7);
			polFormFields->Reqf =	omPropertyGrid.GetValueRowCol(i+1, 8);
			polFormFields->HiddenRefe = omPropertyGrid.GetValueRowCol(i+1, 9);
			pomProperty->omDBFields.Add(polFormFields);
			olTabField = polFormFields->Table + CString(".") + polFormFields->Field;
			ogTools.AddFieldToDefaultSet(NULL, olTabField );
			ilRealToReadRows++;
			if(!polFormFields->Key.IsEmpty())
			{
				ilRealToReadRows++;
				ogTools.AddFieldToDefaultSet(NULL, polFormFields->Key );
			}
			/*
			 * We have a pseudo foreign key
			 * add it to the loadset
			 */

			if(polFormFields->Refe.IsEmpty == FALSE)
			{
				ogTools.AddFieldToDefaultSet(NULL,polFormFields->Refe);
				ogTools.AddFieldToDefaultSet(NULL,polFormFields->HiddenRefe);
			}
		}
		int ilTabRows = omTabPropertyGrid.GetRowCount();
		pomProperty->omTables.RemoveAll();
		for(i = 0; i < ilTabRows; i++)
		{
			CString olV = omTabPropertyGrid.GetValueRowCol(i+1, 1); 
			if(!olV.IsEmpty())
			{
				pomProperty->omTables.Add(olV);
			}
		}
	}//if(ilRows > 0)
// generate the GUI-forminformation
	pomProperty->omName = v_FormName;
	pomProperty->omRect = CRect(0,0,240,0);
	CRect olLabelRect, olCtrlRect;
	olLabelRect = CRect(  5, 11, 105, 28);
	olCtrlRect   = CRect(112,  9, 222, 32);
	CString olSpecialFields;
	olSpecialFields = CString("");//CString("URNO,CDAT,USEU,USEC,HOPO,LSTU");
	for(i = 0; i < pomProperty->omDBFields.GetSize(); i++)
	{
		bool blWithLabel = true;
		CString olType;
		if(!pomProperty->omDBFields[i].Refe.IsEmpty())
		{
				olType = "combobox";		
		}
		else
		{
			if(pomProperty->omDBFields[i].Type == "BOOL")
			{
				olType = "checkbox"; blWithLabel = false;
			}
			else
			{
				olType = "edit";
			}
		}
		
		if(blWithLabel == true)
		{
			UStaticCtrlProperties *polLabel = new UStaticCtrlProperties;
			polLabel->omCtrlType	= "static";
			polLabel->omLabel		= pomProperty->omDBFields[i].Header;		
			polLabel->omName		= pomProperty->omDBFields[i].Table + CString(".") + pomProperty->omDBFields[i].Field + CString(".Label");			
			polLabel->omDBField		= CString("");//pomProperty->omDBFields[i].Field;		
			polLabel->omOrientation	= "LEFT";	
			polLabel->omFieldType	= CString("");//pomProperty->omDBFields[i].Type;	
			polLabel->omFormatString = CString(""); 
			polLabel->omRect		= olLabelRect;
			if(olSpecialFields.Find(pomProperty->omDBFields[i].Field) != -1 )
			{
				polLabel->bmEnabled	= FALSE;
				polLabel->bmVisible	= FALSE;
			}
			else
			{
				polLabel->bmEnabled	= TRUE;
				polLabel->bmVisible	= TRUE;
			}
			polLabel->imBorderWidth = 1;
			polLabel->bmDBField = FALSE;
			pomProperty->omProperties.Add(polLabel);
		}


		UCtrlProperties *polCtrl = NULL;
		if(olType == "edit")
		{
			UEditCtrlProperties *polEdit = new UEditCtrlProperties();
			polCtrl = (UCtrlProperties *)polEdit;
		}
		if(olType == "checkbox")
		{
			UCheckboxCtrlProperties *polEdit = new UCheckboxCtrlProperties();
			polCtrl = (UCtrlProperties *)polEdit;
		}
		if(olType == "combobox")
		{
			UComboboxCtrlProperties *polEdit = new UComboboxCtrlProperties();
			polCtrl = (UCtrlProperties *)polEdit;
			polCtrl->Refe   = pomProperty->omDBFields[i].Refe;
			polCtrl->HiddenRefe = pomProperty->omDBFields[i].HiddenRefe;
		}
		polCtrl->omCtrlType     = olType;
		polCtrl->omLabel		= pomProperty->omDBFields[i].Header;		

		CString olUrno = ogBCD.GetFieldExt("SYS", "TANA", "FINA", pomProperty->omDBFields[i].Table, pomProperty->omDBFields[i].Field, "URNO");
		RecordSet olRec;
		ogBCD.GetRecord("SYS", "URNO", olUrno, olRec);
		polCtrl->imTextLimit = atoi(olRec[ogBCD.GetFieldIndex("SYS", "FELE")]);

		polCtrl->omName			= pomProperty->omDBFields[i].Table + CString(".") + pomProperty->omDBFields[i].Field;			
		polCtrl->omDBField		= pomProperty->omDBFields[i].Field;		
		polCtrl->omOrientation	= "LEFT";	
		polCtrl->omFieldType	= pomProperty->omDBFields[i].Type;	
		polCtrl->omFormatString = CString(""); 
		polCtrl->omRect			= olCtrlRect;
		if(pomProperty->omDBFields[i].Reqf == "1")
		{
			polCtrl->bmMandatory = TRUE;
			polCtrl->omBackColor = YELLOW;
		}
		else
		{
			polCtrl->bmMandatory = FALSE;
		}
		if(olSpecialFields.Find(pomProperty->omDBFields[i].Field) != -1 )
		{
			polCtrl->bmEnabled	= FALSE;
			polCtrl->bmVisible	= FALSE;
		}
		else
		{
			polCtrl->bmEnabled	= TRUE;
			polCtrl->bmVisible	= TRUE;
		}
		polCtrl->imBorderWidth = 1;
		polCtrl->bmDBField = TRUE;
		pomProperty->omProperties.Add(polCtrl);
//geometry
		if((i != 0) && ((i % 20) == 0))
		{
			olLabelRect.top		= 11; //= CRect(  5, 11, 105, 28);
			olLabelRect.left	+= 237;
			olLabelRect.right	+= 237;
			olLabelRect.bottom	= 28;
			olCtrlRect.top		= 9;   //= CRect(112,  9, 222, 32);
			olCtrlRect.left		+= 237;
			olCtrlRect.right     += 237;
			olCtrlRect.bottom	= 32;
			pomProperty->omRect.right += 225;
		}
		else
		{
			olLabelRect.top		+= 25; //= CRect(  5, 11, 105, 28);
			olLabelRect.bottom	+= 25;
			olCtrlRect.top		+= 25;   //= CRect(112,  9, 222, 32);
			olCtrlRect.bottom	+= 25;
			pomProperty->omRect.bottom += 26;
		}
	}
	return true;
}

void FormWizard::OnMultitoright() 
{
	BOOL bOld = omPropertyGrid.LockUpdate();
	

	int ilCount = m_SourceFields.GetCount();
	CString olTable;
	m_Tables.GetWindowText(olTable);
	if(!olTable.IsEmpty())
	{
		for(int i = 0; i < ilCount; i++)
		{
			CString olField;
			m_SourceFields.GetText(i, olField);
			int ilRowNo = omPropertyGrid.GetRowCount( )+1;
			omPropertyGrid.InsertRows(omPropertyGrid.GetRowCount( )+1, 1);
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 1), olTable);
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 2), olField);
			CString olUrno = ogBCD.GetFieldExt("SYS", "TANA", "FINA", olTable, olField, "URNO");
			CString olHeader;
			CString olType;
			CString olRefe;
			CString olReqf;
			RecordSet olRec;
			ogBCD.GetRecord("SYS", "URNO", olUrno, olRec);
			olHeader = olRec[ogBCD.GetFieldIndex("SYS", "LABL")];
			olHeader.TrimLeft();olHeader.TrimRight();
			olType = olRec[ogBCD.GetFieldIndex("SYS", "TYPE")];
			olReqf = olRec[ogBCD.GetFieldIndex("SYS", "REQF")];
			if(olHeader.IsEmpty())
			{
				olHeader = olRec[ogBCD.GetFieldIndex("SYS", "ADDI")];
			}
			olRefe = olRec[ogBCD.GetFieldIndex("SYS", "REFE")];
			if(olRefe ==".")
			{
				olRefe.Empty();
			}
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 3), olType);
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 4), olHeader);
			omPropertyGrid.SetStyleRange(CGXRange(omPropertyGrid.GetRowCount( ), 5), CGXStyle().SetChoiceList("")
															.SetHorizontalAlignment(DT_CENTER)
															.SetUserAttribute(GX_IDS_UA_CHECKBOX_CHECKED, _T("1"))
															.SetUserAttribute(GX_IDS_UA_CHECKBOX_UNCHECKED, _T("0"))
															.SetControl(GX_IDS_CTRL_CHECKBOX3D));
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 5), _T("1"));
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 7), olRefe);
			omPropertyGrid.SetStyleRange(CGXRange(omPropertyGrid.GetRowCount( ), 8), CGXStyle().SetChoiceList("")
															.SetHorizontalAlignment(DT_CENTER)
															.SetUserAttribute(GX_IDS_UA_CHECKBOX_CHECKED, _T("1"))
															.SetUserAttribute(GX_IDS_UA_CHECKBOX_UNCHECKED, _T("0"))
															.SetControl(GX_IDS_CTRL_CHECKBOX3D));
			omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 9), olRefe);

			if(olReqf == "Y")
			{
				omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 8), _T("1"));
			}
			else
			{
				omPropertyGrid.SetValueRange(CGXRange(omPropertyGrid.GetRowCount( ), 8), _T("0"));
			}

			//Check for existing table
			int ilTabRows = omTabPropertyGrid.GetRowCount();
			bool blFound = false;
			for(int j = 0; (blFound == false) && (j < ilTabRows); j++)
			{
				if( olTable == omTabPropertyGrid.GetValueRowCol(j+1, 1))
				{
					blFound = true;
				}
			}
			if( blFound == false)
			{
				omTabPropertyGrid.InsertRows(omPropertyGrid.GetRowCount( )+1, 1);
				omTabPropertyGrid.SetValueRange(CGXRange(omTabPropertyGrid.GetRowCount( ), 1), olTable);
				omTabPropertyGrid.SetValueRange(CGXRange(omTabPropertyGrid.GetRowCount( ), 2), "");
			}
		}
	}

	m_SourceFields.ResetContent();
	omPropertyGrid.LockUpdate(bOld);
	omPropertyGrid.Redraw();
}
