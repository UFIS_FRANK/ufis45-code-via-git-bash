// drawtool.h - interface for UFormDrawTool and derivatives
//
// This is a part of the Microsoft Foundation Classes C++ library.
// Copyright (C) 1992-1998 Microsoft Corporation
// All rights reserved.
//
// This source code is only intended as a supplement to the
// Microsoft Foundation Classes Reference and related
// electronic documentation provided with the library.
// See these sources for detailed information regarding the
// Microsoft Foundation Classes product.


#ifndef __DRAWTOOL_H__
#define __DRAWTOOL_H__

#include "UFormControlObj.h"

class FormDesignerView;

enum DrawShape
{
	selection,
	line,
	rect,
	edit,
	checkbox,
	combobox,
	button,
	c_static,
	grid,
	radiobutton,
	group
};

class UFormDrawTool
{
// Constructors
public:
	UFormDrawTool(DrawShape nDrawShape);
	UFormDrawTool();

// Overridables
	virtual void OnLButtonDown(FormDesignerView* pView, UINT nFlags, const CPoint& point);
	virtual void OnLButtonDblClk(FormDesignerView* pView, UINT nFlags, const CPoint& point);
	virtual void OnLButtonUp(FormDesignerView* pView, UINT nFlags, const CPoint& point);
	virtual void OnMouseMove(FormDesignerView* pView, UINT nFlags, const CPoint& point);
	virtual void OnEditProperties(FormDesignerView* pView);
	virtual void OnCancel();
	virtual void ShowPropertyDlg(UCtrlProperties* popProperty = NULL, UFormControlObj *popCtrlObj = NULL, FormDesignerView* popView = NULL);


// Attributes
	DrawShape m_drawShape;

	static UFormDrawTool* FindTool(DrawShape drawShape);
	static CPtrList c_tools;
	static CPoint c_down;
	static UINT c_nDownFlags;
	static CPoint c_last;
	static DrawShape c_drawShape;
};

class UFormSelectTool : public UFormDrawTool
{
// Constructors
public:
	UFormSelectTool();

// Implementation
	virtual void OnLButtonDown(FormDesignerView* pView, UINT nFlags, const CPoint& point);
	virtual void OnLButtonDblClk(FormDesignerView* pView, UINT nFlags, const CPoint& point);
	virtual void OnLButtonUp(FormDesignerView* pView, UINT nFlags, const CPoint& point);
	virtual void OnMouseMove(FormDesignerView* pView, UINT nFlags, const CPoint& point);
	virtual void OnEditProperties(FormDesignerView* pView);
};

class UFormDrawRectTool : public UFormDrawTool
{
// Constructors
public:
	UFormDrawRectTool(DrawShape drawShape);

// Implementation
	virtual void OnLButtonDown(FormDesignerView* pView, UINT nFlags, const CPoint& point);
	virtual void OnLButtonDblClk(FormDesignerView* pView, UINT nFlags, const CPoint& point);
	virtual void OnLButtonUp(FormDesignerView* pView, UINT nFlags, const CPoint& point);
	virtual void OnMouseMove(FormDesignerView* pView, UINT nFlags, const CPoint& point);
};

class UFormDrawEditTool : public UFormDrawTool
{
// Constructors
public:
	UFormDrawEditTool(DrawShape nDrawShape);

// Implementation
	virtual void OnLButtonDown(FormDesignerView* pView, UINT nFlags, const CPoint& point);
	virtual void OnLButtonDblClk(FormDesignerView* pView, UINT nFlags, const CPoint& point);
	virtual void OnLButtonUp(FormDesignerView* pView, UINT nFlags, const CPoint& point);
	virtual void OnMouseMove(FormDesignerView* pView, UINT nFlags, const CPoint& point);
};

class UFormDrawStaticTool : public UFormDrawTool
{
// Constructors
public:
	UFormDrawStaticTool(DrawShape nDrawShape);

// Implementation
	virtual void OnLButtonDown(FormDesignerView* pView, UINT nFlags, const CPoint& point);
	virtual void OnLButtonDblClk(FormDesignerView* pView, UINT nFlags, const CPoint& point);
	virtual void OnLButtonUp(FormDesignerView* pView, UINT nFlags, const CPoint& point);
	virtual void OnMouseMove(FormDesignerView* pView, UINT nFlags, const CPoint& point);
};

class UFormDrawCheckboxTool : public UFormDrawTool
{
// Constructors
public:
	UFormDrawCheckboxTool(DrawShape nDrawShape);

// Implementation
	virtual void OnLButtonDown(FormDesignerView* pView, UINT nFlags, const CPoint& point);
	virtual void OnLButtonDblClk(FormDesignerView* pView, UINT nFlags, const CPoint& point);
	virtual void OnLButtonUp(FormDesignerView* pView, UINT nFlags, const CPoint& point);
	virtual void OnMouseMove(FormDesignerView* pView, UINT nFlags, const CPoint& point);
};

class UFormDrawRadiobuttonTool : public UFormDrawTool
{
// Constructors
public:
	UFormDrawRadiobuttonTool(DrawShape nDrawShape);

// Implementation
	virtual void OnLButtonDown(FormDesignerView* pView, UINT nFlags, const CPoint& point);
	virtual void OnLButtonDblClk(FormDesignerView* pView, UINT nFlags, const CPoint& point);
	virtual void OnLButtonUp(FormDesignerView* pView, UINT nFlags, const CPoint& point);
	virtual void OnMouseMove(FormDesignerView* pView, UINT nFlags, const CPoint& point);
};

class UFormDrawComboboxTool : public UFormDrawTool
{
// Constructors
public:
	UFormDrawComboboxTool(DrawShape nDrawShape);

// Implementation
	virtual void OnLButtonDown(FormDesignerView* pView, UINT nFlags, const CPoint& point);
	virtual void OnLButtonDblClk(FormDesignerView* pView, UINT nFlags, const CPoint& point);
	virtual void OnLButtonUp(FormDesignerView* pView, UINT nFlags, const CPoint& point);
	virtual void OnMouseMove(FormDesignerView* pView, UINT nFlags, const CPoint& point);
};

class UFormDrawButtonTool : public UFormDrawTool
{
// Constructors
public:
	UFormDrawButtonTool(DrawShape nDrawShape);

// Implementation
	virtual void OnLButtonDown(FormDesignerView* pView, UINT nFlags, const CPoint& point);
	virtual void OnLButtonDblClk(FormDesignerView* pView, UINT nFlags, const CPoint& point);
	virtual void OnLButtonUp(FormDesignerView* pView, UINT nFlags, const CPoint& point);
	virtual void OnMouseMove(FormDesignerView* pView, UINT nFlags, const CPoint& point);
};

class UFormDrawGroupTool : public UFormDrawTool
{
// Constructors
public:
	UFormDrawGroupTool(DrawShape nDrawShape);

// Implementation
	virtual void OnLButtonDown(FormDesignerView* pView, UINT nFlags, const CPoint& point);
	virtual void OnLButtonDblClk(FormDesignerView* pView, UINT nFlags, const CPoint& point);
	virtual void OnLButtonUp(FormDesignerView* pView, UINT nFlags, const CPoint& point);
	virtual void OnMouseMove(FormDesignerView* pView, UINT nFlags, const CPoint& point);
};


class UFormDrawGridTool : public UFormDrawTool
{
// Constructors
public:
	UFormDrawGridTool(DrawShape nDrawShape);

// Implementation
	virtual void OnLButtonDown(FormDesignerView* pView, UINT nFlags, const CPoint& point);
	virtual void OnLButtonDblClk(FormDesignerView* pView, UINT nFlags, const CPoint& point);
	virtual void OnLButtonUp(FormDesignerView* pView, UINT nFlags, const CPoint& point);
	virtual void OnMouseMove(FormDesignerView* pView, UINT nFlags, const CPoint& point);
};

////////////////////////////////////////////////////////////////////////////




#endif // __DRAWTOOL_H__
