#if !defined(AFX_UUNIVERSALGRIDVIEW_H__EE6B3C55_47E4_11D3_B093_00001C019205__INCLUDED_)
#define AFX_UUNIVERSALGRIDVIEW_H__EE6B3C55_47E4_11D3_B093_00001C019205__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UUniversalGridView.h : header file
//

#include "UUniversalGridDoc.h"
/////////////////////////////////////////////////////////////////////////////
// UUniversalGridView view

class UUniversalGridView  : public CGXGridView
{
protected:
	UUniversalGridView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(UUniversalGridView)

// Attributes
public:
// Attributes
public:
	UUniversalGridDoc* GetDocument();
#if _MFC_VER >= 0x0400
	CGXGridDropTarget m_objDndDropTarget;
#endif

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(UUniversalGridView)
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual void OnInitialUpdate(); // called first time after construct
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~UUniversalGridView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(UUniversalGridView)
	afx_msg int OnCreate( LPCREATESTRUCT lpCreateStruct );
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UUNIVERSALGRIDVIEW_H__EE6B3C55_47E4_11D3_B093_00001C019205__INCLUDED_)
