#if !defined(AFX_FORMEXECUTEMDI_H__3596C795_2F83_11D3_A200_00500437F607__INCLUDED_)
#define AFX_FORMEXECUTEMDI_H__3596C795_2F83_11D3_A200_00500437F607__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FormExecuteMDI.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// FormExecuteMDI frame
#include "resource.h"

class FormExecuteMDI : public CMDIChildWnd
{
	DECLARE_DYNCREATE(FormExecuteMDI)
protected:
	FormExecuteMDI();           // protected constructor used by dynamic creation

// Attributes
public:

	CToolBar    m_wndToolBar; //IDR_NAVIGATOR_BAR

// Operations
public:


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(FormExecuteMDI)
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~FormExecuteMDI();




	// Generated message map functions
	//{{AFX_MSG(FormExecuteMDI)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FORMEXECUTEMDI_H__3596C795_2F83_11D3_A200_00500437F607__INCLUDED_)
