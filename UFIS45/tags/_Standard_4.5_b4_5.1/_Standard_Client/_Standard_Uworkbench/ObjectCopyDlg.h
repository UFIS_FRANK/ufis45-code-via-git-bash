#if !defined(AFX_OBJECTCOPYDLG_H__A17577C1_8952_11D4_A307_00500437F607__INCLUDED_)
#define AFX_OBJECTCOPYDLG_H__A17577C1_8952_11D4_A307_00500437F607__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ObjectCopyDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// ObjectCopyDlg dialog

class ObjectCopyDlg : public CDialog
{
// Construction
public:
	ObjectCopyDlg(CWnd* pParent = NULL);   // standard constructor

	~ObjectCopyDlg();
// Dialog Data
	//{{AFX_DATA(ObjectCopyDlg)
	enum { IDD = IDD_OBJECTCOPY };
	CListBox	m_NewGrids;
	CListBox	m_NewForms;
	CListBox	m_Grids;
	CListBox	m_Forms;
	CEdit	m_FileName;
	//}}AFX_DATA


	CString omSourceFile;
	CString omDestinationFile;

	//Arrays for Source Datastructures
	CCSPtrArray<UDBLoadSet> omSourceLoadSets;
	CCSPtrArray<UGridDefinition> omSourceGridDefs;
	CCSPtrArray<UFormProperties> omSourceForms;
	CCSPtrArray<UQuery> omSourceQueries;

	//Arrays for Destination Datastructures
	CCSPtrArray<UDBLoadSet> omDestinationLoadSets;
	CCSPtrArray<UGridDefinition> omDestinationGridDefs;
	CCSPtrArray<UFormProperties> omDestinationForms;
	CCSPtrArray<UQuery> omDestinationQueries;

	void ClearSourceArrays();

	void ClearDestinationArrays();

	void InitialFillListBoxes();

	UGridDefinition * GetSourceGridName(CString opName);

	UFormProperties * GetSourceFormName(CString opName);

	UFormProperties * GetDestinationFromName(CString opName);

	UGridDefinition * GetDestinationGridName(CString opName);

	void DeleteDestinationGridByName(CString opName);

	void DeleteDestinationFormByName(CString opName);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ObjectCopyDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(ObjectCopyDlg)
	afx_msg void OnLoadDestination();
	afx_msg void OnLoadSource();
	afx_msg void OnGridDelete();
	afx_msg void OnGridToRight();
	afx_msg void OnFormDelete();
	afx_msg void OnFormToRight();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnBrowse();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OBJECTCOPYDLG_H__A17577C1_8952_11D4_A307_00500437F607__INCLUDED_)
