#if !defined(AFX_TABOBJECTRUNTIMEVIEW_H__2C2E1893_0540_11D4_A28D_00500437F607__INCLUDED_)
#define AFX_TABOBJECTRUNTIMEVIEW_H__2C2E1893_0540_11D4_A28D_00500437F607__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TabObjectRuntimeView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// TabObjectRuntimeView view

class TabObjectRuntimeView : public CTreeView
{
protected:
	//TabObjectRuntimeView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(TabObjectRuntimeView)

// Attributes
public:
	TabObjectRuntimeView();
	TabObjectRuntimeDoc* GetDocument();
	
	CTreeCtrl*		m_pTreeCtrl;
	CImageList		m_ImageList;

// Operations
public:
	void UpdateView();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(TabObjectRuntimeView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual void OnDraw(CDC* pDC);      // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	//}}AFX_VIRTUAL

// Implementation

public:
	virtual ~TabObjectRuntimeView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#ifndef _DEBUG  // debug version in TabClassView.cpp
inline TabObjectRuntimeDoc* TabObjectRuntimeView::GetDocument()
   { return (TabObjectRuntimeDoc*)m_pDocument; }
#endif
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(TabObjectRuntimeView)
	afx_msg void OnDblclk(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnRclick(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TABOBJECTRUNTIMEVIEW_H__2C2E1893_0540_11D4_A28D_00500437F607__INCLUDED_)
