#if !defined(AFX_DETAILGRIDWIZARD_H__3ED2F4E3_86BC_11D3_A20B_00500437F607__INCLUDED_)
#define AFX_DETAILGRIDWIZARD_H__3ED2F4E3_86BC_11D3_A20B_00500437F607__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DetailGridWizard.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DetailGridWizard dialog

class DetailGridWizard : public CDialog
{
// Construction
public:
	DetailGridWizard(CWnd* pParent, UGridCtrlProperties *popProperty);   // standard constructor
	UGridCtrlProperties *pomProperty;
// Dialog Data
	//{{AFX_DATA(DetailGridWizard)
	enum { IDD = IDD_DETAIL_GRID_WIZARD };
	CEdit	m_Detailreference;
	CEdit	m_Masterreference;
	CComboBox	m_Tables;
	CListBox	m_SourceFields;
	CEdit	m_GridName;
	CString	v_GridName;
	CString	v_Masterreference;
	CString	v_Detailreference;
	//}}AFX_DATA

	GxGridTab omPropertyGrid;
	int imColCount;


	CMapStringToPtr omTableMap;

	bool GetValues();


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DetailGridWizard)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DetailGridWizard)
	afx_msg void OnSingletoleft();
	afx_msg void OnSingletoright();
	afx_msg void OnSelchangeSourcefields();
	afx_msg void OnSelchangeTables();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnMultitoright();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DETAILGRIDWIZARD_H__3ED2F4E3_86BC_11D3_A20B_00500437F607__INCLUDED_)
