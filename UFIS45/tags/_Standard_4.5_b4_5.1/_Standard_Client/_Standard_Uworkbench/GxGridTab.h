#if !defined(AFX_GXGRIDTAB_H__0B02FE9B_7E37_11D2_A14D_0000B4984BBE__INCLUDED_)
#define AFX_GXGRIDTAB_H__0B02FE9B_7E37_11D2_A14D_0000B4984BBE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GxGridTab.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// GxGridTab window
#ifndef _GXRESRC_H_
#include "gxresrc.h"
#endif

#ifndef _GXVW_H_
#include "gxvw.h"
#endif

#ifndef _GXEXT_H_
#include "gxext.h"
#endif

#ifndef _GXMSG_H_
#include "gxmsg.h"
#endif


#define GX_ROWCOL_CHANGED		(WM_USER + 9000)
#define GX_CELL_BUTTONCLICKED	(WM_USER + 9001)
#define GX_CELL_R_BUTTONCLICKED (WM_USER + 9002)

class GxGridTab;
struct GxMiniGridInfo
{
	int Row, Col;
	GxGridTab *pGrid;
	CPoint pt;
	GxMiniGridInfo(void)
	{
		Row = 0;
		Col = 0;
		pGrid = NULL;
	}
};

class GxGridTab : public CGXGridWnd
{
DECLARE_DYNCREATE(GxGridTab)

// Construction
public:
	GxGridTab();
	GxGridTab(CWnd *popParent, CString opParentType = CString(""));
	GxGridTab(const GxGridTab &opO)
	{
		this->pomParent = opO.pomParent;
		this->omParentType = opO.omParentType;
		this->bmAutoGrow = opO.bmAutoGrow;
		this->bmInEndEnditing = opO.bmInEndEnditing;
	}
	const GxGridTab &operator =(const GxGridTab &opO) 
	{
		this->pomParent = opO.pomParent;
		this->omParentType = opO.omParentType;
		this->bmAutoGrow = opO.bmAutoGrow;
		this->bmInEndEnditing = opO.bmInEndEnditing;
		return *this;
	}
	CWnd *pomParent;
	CString omParentType;
// Attributes
public:

	bool bmAutoGrow;
	bool bmInEndEnditing;
// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(GxGridTab)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~GxGridTab();
	virtual void OnClickedButtonRowCol(ROWCOL nRow, ROWCOL nCol);
	virtual BOOL OnEndEditing(ROWCOL nRow, ROWCOL nCol);
	virtual BOOL OnLeftCell(ROWCOL nRow, ROWCOL nCol, ROWCOL nNewRow, ROWCOL nNewCol);
	virtual BOOL OnRButtonClickedRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt);
	void EnableAutoGrow ( bool enable = true );
	void DoAutoGrow ( ROWCOL nRow, ROWCOL nCol);
	BOOL InsertBottomRow ();
	void CopyStyleOfLine ( ROWCOL ipSourceLine, ROWCOL ipDestLine,
						   bool bpResetValues=true );
	void CopyStyleLastLineFromPrev ( bool bpResetValues=true );
	bool DeleteEmptyRows ( ROWCOL nRow, ROWCOL nCol ); 






	// Generated message map functions
protected:
	//{{AFX_MSG(GxGridTab)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


class GxBrowseTab : public CGXBrowserWnd
{
DECLARE_DYNCREATE(GxBrowseTab)

// Construction
public:
	GxBrowseTab();
	GxBrowseTab(CWnd *popParent);
	CWnd *pomParent;
// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(GxGridTab)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~GxBrowseTab();
	virtual void OnClickedButtonRowCol(ROWCOL nRow, ROWCOL nCol);
	virtual BOOL OnEndEditing(ROWCOL nRow, ROWCOL nCol);
	virtual void InitBrowserSettings()
	{
		CGXBrowserWnd::InitBrowserSettings();
	}


	// Generated message map functions
protected:
	//{{AFX_MSG(GxGridTab)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};
/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GXGRIDTAB_H__0B02FE9B_7E37_11D2_A14D_0000B4984BBE__INCLUDED_)
