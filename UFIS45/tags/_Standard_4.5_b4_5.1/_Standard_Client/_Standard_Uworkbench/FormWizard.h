#if !defined(AFX_FORMWIZARD_H__B06F1C51_763D_11D3_A1FB_00500437F607__INCLUDED_)
#define AFX_FORMWIZARD_H__B06F1C51_763D_11D3_A1FB_00500437F607__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FormWizard.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// FormWizard dialog
#include "GxGridTab.h"
#include "CtrlProperties.h"
class FormWizard : public CDialog
{
// Construction
public:
	FormWizard(CWnd* pParent, UFormProperties *popProperty);   // standard constructor

	UFormProperties *pomProperty;
// Dialog Data
	//{{AFX_DATA(FormWizard)
	enum { IDD = IDD_FORM_WIZARD };
	CComboBox	m_Tables;
	CListBox	m_SourceFields;
	CEdit	m_FormName;
	CString	v_FormName;
	//}}AFX_DATA

	GxGridTab omPropertyGrid;
	GxGridTab omTabPropertyGrid;
	int imColCount;


	CMapStringToPtr omTableMap;

	bool GetValues();


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(FormWizard)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(FormWizard)
	afx_msg void OnSingletoleft();
	afx_msg void OnSingletoright();
	afx_msg void OnSelchangeSourcefields();
	afx_msg void OnSelchangeTables();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnMultitoright();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FORMWIZARD_H__B06F1C51_763D_11D3_A1FB_00500437F607__INCLUDED_)
