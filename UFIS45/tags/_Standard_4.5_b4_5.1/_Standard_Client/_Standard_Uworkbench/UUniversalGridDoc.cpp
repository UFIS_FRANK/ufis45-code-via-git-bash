// UUniversalGridDoc.cpp : implementation file
//

#include "stdafx.h"
#include "UWorkBench.h"
#include "UUniversalGridDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// UUniversalGridDoc

IMPLEMENT_DYNCREATE(UUniversalGridDoc, CDocument)

UUniversalGridDoc::UUniversalGridDoc()
{
	m_pParam = NULL;
	pomGridDef = NULL;
}

BOOL UUniversalGridDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;
	DeleteContents();
	SetModifiedFlag();
	return TRUE;
}

UUniversalGridDoc::~UUniversalGridDoc()
{
	if (m_pParam)
		delete m_pParam;
	m_pParam = NULL;
}


BEGIN_MESSAGE_MAP(UUniversalGridDoc, CDocument)
	//{{AFX_MSG_MAP(UUniversalGridDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// UUniversalGridDoc diagnostics

#ifdef _DEBUG
void UUniversalGridDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void UUniversalGridDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// UUniversalGridDoc serialization

void UUniversalGridDoc::SetCurrentGridDef(UGridDefinition *popGrodDef)
{
	pomGridDef = popGrodDef;
	UQuery *polQuery = NULL;
	for(int i = 0; i < ogQueries.GetSize(); i++)
	{
		if(ogQueries[i].omName == pomGridDef->DataSource)
		{
			polQuery = &ogQueries[i];
			break;
		}
	}
	if(polQuery != NULL)
	{
//MWO Aktuell Current		polQuery->omUsedTables
	}
}

void UUniversalGridDoc::Serialize(CArchive& ar)
{
	static const WORD wVersion = 1; 
	WORD wActualVersion = wVersion;

	ASSERT_VALID( this );

	if (ar.IsStoring())
	{
		ar << wVersion;
	}
	else
	{
		// Check for version first
		ar >> wActualVersion;
		if( wActualVersion > wVersion )
		{
#ifdef _DEBUG
			TRACE0( "Incompatible format while reading CGridDoc" );
			TRACE2("in %s at line %d\n", __FILE__, __LINE__);
			ASSERT(0);
			// ASSERTION-> Wrong version detected while reading object ->END
#endif
			AfxThrowArchiveException(CArchiveException::badSchema);
			return;
		}
	}

	if (ar.IsStoring())
	{
		// column settings, base styles
		ar << m_pParam;

	}
	else
	{
		// column settings, base styles
		ar >> m_pParam;

	}

}

/////////////////////////////////////////////////////////////////////////////
// UUniversalGridDoc commands

void UUniversalGridDoc::SetTitle(LPCTSTR lpszTitle) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	CDocument::SetTitle(lpszTitle);
}

void UUniversalGridDoc::DeleteContents() 
{
	if (m_pParam)
		delete m_pParam;
	m_pParam = NULL;
}
