// ChildFrm.cpp : implementation of the CChildFrame class
//

#include "stdafx.h"
#include "mdi_D.h"

#include "ChildFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChildFrame

IMPLEMENT_DYNCREATE(CChildFrame, CMDIChildWnd)

BEGIN_MESSAGE_MAP(CChildFrame, CMDIChildWnd)
	//{{AFX_MSG_MAP(CChildFrame)
	ON_WM_RBUTTONDOWN()
	ON_COMMAND(ID_EDIT_CUT, OnEditCut)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChildFrame construction/destruction

CChildFrame::CChildFrame()
{
	// TODO: add member initialization code here
	imStatus = 0;

}

CChildFrame::~CChildFrame()
{
}

BOOL CChildFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CChildFrame diagnostics

#ifdef _DEBUG
void CChildFrame::AssertValid() const
{
	CMDIChildWnd::AssertValid();
}

void CChildFrame::Dump(CDumpContext& dc) const
{
	CMDIChildWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CChildFrame message handlers

void CChildFrame::OnRButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default


	CMDIChildWnd::OnRButtonDown(nFlags, point);
}

void CChildFrame::OnEditCut() 
{
	// TODO: Add your command handler code here
	BOOL ok ; 
	m_wndToolBar.ShowWindow ( (imStatus<2 ) ? SW_SHOW : SW_HIDE );
	switch ( imStatus )
	{
	case 0:
		ok = m_wndToolBar.LoadToolBar(IDR_MAINFRAME);
		break;
	case 1:
		ok = m_wndToolBar.LoadToolBar(IDR_MAINFRAME_SHORT);
		break;
	default:

		break;
	}
	imStatus = ( ( imStatus+1 ) % 3 );
	ShowControlBar ( &m_wndToolBar, imStatus>0, FALSE );
	

}

int CChildFrame::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
						 | CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
			
	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	EnableDocking(CBRS_ALIGN_ANY);
	DockControlBar(&m_wndToolBar);
	ShowControlBar ( &m_wndToolBar, imStatus>0, FALSE );
	return 0;
}
