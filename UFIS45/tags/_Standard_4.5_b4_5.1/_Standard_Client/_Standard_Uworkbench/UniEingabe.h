#if !defined(AFX_UNIEINGABE_H__F317B051_8B4A_11D1_B444_0000B45A33F5__INCLUDED_)
#define AFX_UNIEINGABE_H__F317B051_8B4A_11D1_B444_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// UniEingabe.h : header file
//
#include "resource.h"

/////////////////////////////////////////////////////////////////////////////
// UniEingabe dialog

class UniEingabe : public CDialog
{
// Construction
public:
	UniEingabe(CWnd* pParent, CString opLabel, CString opCation, int ipTextLimit=10);   // standard constructor

	CString omCaption;
	CString omLabel;
	int imTextLimit;

// Dialog Data
	//{{AFX_DATA(UniEingabe)
	enum { IDD = IDD_UNI_EINGABE };
	CEdit	m_Text;
	CStatic	m_Label;
	CString	m_Eingabe;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(UniEingabe)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(UniEingabe)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UNIEINGABE_H__F317B051_8B4A_11D1_B444_0000B45A33F5__INCLUDED_)
