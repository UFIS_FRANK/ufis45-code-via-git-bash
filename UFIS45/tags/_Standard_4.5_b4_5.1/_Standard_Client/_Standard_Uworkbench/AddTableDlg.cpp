// AddTableDlg.cpp : implementation file
//

#include "stdafx.h"
#include "UWorkBench.h"
#include "AddTableDlg.h"
#include "CCSGlobl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// AddTableDlg dialog


AddTableDlg::AddTableDlg(CWnd* pParent, CStringArray *popList)
	: CDialog(AddTableDlg::IDD, pParent)
{
	pomList = popList;
	//{{AFX_DATA_INIT(AddTableDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void AddTableDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(AddTableDlg)
	DDX_Control(pDX, IDC_TABLE_LIST, m_List);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(AddTableDlg, CDialog)
	//{{AFX_MSG_MAP(AddTableDlg)
	ON_LBN_DBLCLK(IDC_TABLE_LIST, OnDblclkTableList)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// AddTableDlg message handlers

BOOL AddTableDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	POSITION pos;

	m_List.SetFont(&ogCourier_Regular_8);
	m_List.SetFont(&ogCourier_Regular_8);
	for( pos = ogTanaMap.GetStartPosition(); pos != NULL; )
	{
		CString olKey;
		void *pNul;
		ogTanaMap.GetNextAssoc( pos, olKey, pNul);
		m_List.AddString(olKey);
	}
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void AddTableDlg::OnOK() 
{
	// TODO: Add extra validation here

	int ilMaxItems;
	if ((ilMaxItems = m_List.GetSelCount()) == LB_ERR)
		return;

	int *pilIndex = new int [ilMaxItems];
	m_List.GetSelItems(ilMaxItems, pilIndex );
	for(int i = 0; i < ilMaxItems; i++)
	{
		CString olStr;
		m_List.GetText(pilIndex[i], olStr);
		pomList->Add(olStr);

	}

	delete pilIndex;

	CDialog::OnOK();
}

void AddTableDlg::OnDblclkTableList() 
{
	OnOK();	
}
