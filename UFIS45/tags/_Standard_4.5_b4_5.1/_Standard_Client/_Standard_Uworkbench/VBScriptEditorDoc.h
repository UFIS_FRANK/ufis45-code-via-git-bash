#if !defined(AFX_VBSCRIPTEDITORDOC_H__BD937DC1_9083_11D4_A30E_00500437F607__INCLUDED_)
#define AFX_VBSCRIPTEDITORDOC_H__BD937DC1_9083_11D4_A30E_00500437F607__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// VBScriptEditorDoc.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CVBScriptEditorDoc document

class CVBScriptEditorDoc : public CDocument
{
protected: // create from serialization only
	CVBScriptEditorDoc();
	DECLARE_DYNCREATE(CVBScriptEditorDoc)

// Attributes
public:

// Operations
public:

	CString omFileName;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CVBScriptEditorDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	virtual void OnCloseDocument();
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CVBScriptEditorDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CVBScriptEditorDoc)
	afx_msg void OnSaveVbw2();
	afx_msg void OnSaveVbw();
	afx_msg void OnSaveVbw3();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.


#endif // !defined(AFX_VBSCRIPTEDITORDOC_H__BD937DC1_9083_11D4_A30E_00500437F607__INCLUDED_)
