// FormularDesigner.cpp : implementation file
//

#include "stdafx.h"
#include "UWorkBench.h"
#include "FormularDesigner.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// FormularDesigner

IMPLEMENT_DYNCREATE(FormularDesigner, CScrollView)

FormularDesigner::FormularDesigner()
{
}

FormularDesigner::~FormularDesigner()
{
}


BEGIN_MESSAGE_MAP(FormularDesigner, CScrollView)
	//{{AFX_MSG_MAP(FormularDesigner)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// FormularDesigner drawing

void FormularDesigner::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate();

	CSize sizeTotal;
	// TODO: calculate the total size of this view
	sizeTotal.cx = sizeTotal.cy = 100;
	SetScrollSizes(MM_TEXT, sizeTotal);
}

void FormularDesigner::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// TODO: add draw code here
}

/////////////////////////////////////////////////////////////////////////////
// FormularDesigner diagnostics

#ifdef _DEBUG
void FormularDesigner::AssertValid() const
{
	CScrollView::AssertValid();
}

void FormularDesigner::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// FormularDesigner message handlers
