// TabObjectRuntimeView.cpp : implementation file
//

#include "stdafx.h"
#include "CCSGlobl.h"
#include "uworkbench.h"
#include "TabObjectRuntimeView.h"
#include "UFISGridViewerDoc.h"
#include "UFISGridMDIChild.h"
#include "UFISGridViewerView.h"
#include "UserViewDefinitionDlg.h"
#include "PrivList.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// TabObjectRuntimeView

IMPLEMENT_DYNCREATE(TabObjectRuntimeView, CTreeView)

TabObjectRuntimeView::TabObjectRuntimeView()
{
	m_pTreeCtrl = NULL;
}

TabObjectRuntimeView::~TabObjectRuntimeView()
{
}


BEGIN_MESSAGE_MAP(TabObjectRuntimeView, CTreeView)
	//{{AFX_MSG_MAP(TabObjectRuntimeView)
	ON_NOTIFY_REFLECT(NM_DBLCLK, OnDblclk)
	ON_NOTIFY_REFLECT(NM_RCLICK, OnRclick)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// TabObjectRuntimeView drawing

void TabObjectRuntimeView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// TODO: add draw code here
}

/////////////////////////////////////////////////////////////////////////////
// TabObjectRuntimeView diagnostics

#ifdef _DEBUG
void TabObjectRuntimeView::AssertValid() const
{
	CTreeView::AssertValid();
}

void TabObjectRuntimeView::Dump(CDumpContext& dc) const
{
	CTreeView::Dump(dc);
}
TabObjectRuntimeDoc* TabObjectRuntimeView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(TabObjectRuntimeDoc)));
	return (TabObjectRuntimeDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// TabObjectRuntimeView message handlers

BOOL TabObjectRuntimeView::PreCreateWindow(CREATESTRUCT& cs) 
{
	if( !CTreeView::PreCreateWindow( cs ))
		return FALSE;

	// set the style for the tree control.
	cs.style |= TVS_HASBUTTONS|TVS_HASLINES|TVS_LINESATROOT;

	return TRUE;
}

void TabObjectRuntimeView::OnInitialUpdate() 
{
	CTreeView::OnInitialUpdate();
	
	// TODO: Add your specialized code here and/or call the base class
	
}

BOOL TabObjectRuntimeView::OnPreparePrinting(CPrintInfo* pInfo) 
{
	// TODO: call DoPreparePrinting to invoke the Print dialog box
	
	return CTreeView::OnPreparePrinting(pInfo);
}

void TabObjectRuntimeView::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	CTreeView::OnBeginPrinting(pDC, pInfo);
}

void TabObjectRuntimeView::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	CTreeView::OnEndPrinting(pDC, pInfo);
}

void TabObjectRuntimeView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	
}
void TabObjectRuntimeView::UpdateView()
{
	if(!m_pTreeCtrl)
	{
		m_pTreeCtrl = &GetTreeCtrl();

		// Create the image list for the tree control
		m_ImageList.Create (IDB_IL_CLASS1, 16, 1, RGB(0,255,0));
		m_pTreeCtrl->SetImageList (&m_ImageList, TVSIL_NORMAL);
	}

	if( m_pTreeCtrl && m_pTreeCtrl->GetSafeHwnd( ))
	{
		m_pTreeCtrl->DeleteAllItems( );

		m_pTreeCtrl->LockWindowUpdate();
		
		
		// Add the parent item
		HTREEITEM htParentItem = m_pTreeCtrl->InsertItem(_T("Grids"));
		m_pTreeCtrl->SetItemState( htParentItem, TVIS_BOLD, TVIS_BOLD );

		// Add children
		for( int i = 0; i < ogGridDefs.GetSize(); i++)
		{
			m_pTreeCtrl->InsertItem (ogGridDefs[i].GridName, 1, 1, htParentItem, TVI_LAST);
		}

		m_pTreeCtrl->Expand(htParentItem, TVE_EXPAND);
		m_pTreeCtrl->SortChildren( htParentItem ); 

/*		htItem = m_pTreeCtrl->InsertItem(_T("Forms"));
		m_pTreeCtrl->SetItemState( htItem, TVIS_BOLD, TVIS_BOLD );
		// Add children
		for( i = 0; i < ogForms.GetSize(); i++)
		{
			m_pTreeCtrl->InsertItem (ogForms[i].omName, 1, 1, htItem, TVI_LAST);
		}
		m_pTreeCtrl->Expand(htItem, TVE_EXPAND);
*/		
		m_pTreeCtrl->UnlockWindowUpdate();
	}
}

void TabObjectRuntimeView::OnDblclk(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	HTREEITEM hItem, hParentItem;
	hItem = m_pTreeCtrl->GetSelectedItem();
	hParentItem = m_pTreeCtrl->GetParentItem( hItem );
	if(hParentItem != NULL)
	{
		CString olParentText = m_pTreeCtrl->GetItemText( hParentItem );
		if(olParentText == "Grids")
		{
			CString olText = m_pTreeCtrl->GetItemText( hItem );
			UGridDefinition *polGridDef = NULL;
			for(int i = 0; i < ogGridDefs.GetSize(); i++)
			{
				if(ogGridDefs[i].GridName == olText)
				{
					polGridDef = &ogGridDefs[i];
					i = ogGridDefs.GetSize();
				}
			}
			if(polGridDef != NULL)
			{
//----------------------
				char clStat;
				CString olSecGrid;
				olSecGrid = polGridDef->GridName + "_OPEN";
				clStat = ogPrivList.GetStat(olSecGrid);
				if(clStat != '1')
				{
					MessageBox(LoadStg(GRID_OPEN_NO_PRIVILEGES), polGridDef->GridName, (MB_OK|MB_ICONSTOP));//;MB_ICONEXCLAMATION));
					return;
				}
				//UGridDefinition *polGridDefTmp;
				CUFISGridViewerDoc *polDoc;
				bool blMustCreateNew = true;
				bool blDocFound = false;
				POSITION pos = ((CUWorkBenchApp*)AfxGetApp())->pUfisGridMDI->GetFirstDocPosition();
				while (pos != NULL)
				{
					polDoc = (CUFISGridViewerDoc*)((CUWorkBenchApp*)AfxGetApp())->pUfisGridMDI->GetNextDoc(pos );
					if(polDoc != NULL)
					{
						if(polDoc->pomGridDef->GridName == polGridDef->GridName)
						{
							POSITION pos2 = polDoc->GetFirstViewPosition();
							while (pos2 != NULL)
							{
								//polDoc->G
								CUFISGridViewerView* polView = (CUFISGridViewerView*)polDoc->GetNextView(pos2);
								if(polView != NULL)
								{
									((CUWorkBenchApp*)AfxGetApp())->pMainFrame->ActivateGridMDI(polGridDef->GridName);
									//((CUWorkBenchApp*)AfxGetApp())->pMainFrame->SetActiveView(polView);
									blMustCreateNew = false;
									blDocFound = true;
									*pResult = 0;
									return;
								}
							}
						}
					}
				}
				if( blMustCreateNew == true )
				{

					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
  					int ilRecords = ogBCD.SelectCount(polGridDef->omTables[0]);
					SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
					if(ilRecords > 10000)
					{
						//AfxMessageBox("Mehr als 10000 Datensätze");
						UserViewDefinitionDlg *polDlg = new UserViewDefinitionDlg(this, polGridDef);
						if(polDlg->DoModal() == IDOK)
						{
							SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
							ogTools.SetTablesForGridAndForm(polGridDef, true);
							((CUWorkBenchApp*)AfxGetApp())->pMainFrame->OnShowUFISGrid(polGridDef);
							SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
						}
						delete polDlg;
					}
					else
					{
						SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
						ogTools.SetTablesForGridAndForm(polGridDef, true);
						((CUWorkBenchApp*)AfxGetApp())->pMainFrame->OnShowUFISGrid(polGridDef);
						SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
					}
				}

//---------------------- 
			}
		}
	}
	//AfxMessageBox(olItem);
	*pResult = 0;
}

void TabObjectRuntimeView::OnRclick(NMHDR* pNMHDR, LRESULT* pResult) 
{
	// TODO: Add your control notification handler code here
	
	*pResult = 0;
}
