// UUniListView.cpp : implementation file
//

#include "stdafx.h"
#include "UWorkBench.h"
#include "UUniListView.h"
#include "UQueryDesingerDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// UUniListView

IMPLEMENT_DYNCREATE(UUniListView, CListView)

UUniListView::UUniListView()
{
}
 
UUniListView::~UUniListView()
{
}


BEGIN_MESSAGE_MAP(UUniListView, CListView)
	//{{AFX_MSG_MAP(UUniListView)
	ON_WM_CREATE()
	ON_WM_RBUTTONDOWN()
	ON_COMMAND(50, OnDeleteItem)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// UUniListView drawing

void UUniListView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// TODO: add draw code here
}

/////////////////////////////////////////////////////////////////////////////
// UUniListView diagnostics

#ifdef _DEBUG
void UUniListView::AssertValid() const
{
	CListView::AssertValid();
}

void UUniListView::Dump(CDumpContext& dc) const
{
	CListView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// UUniListView message handlers

void UUniListView::OnInitialUpdate() 
{
	CListView::OnInitialUpdate();
	UListCtrlExtended& ctlList = (UListCtrlExtended&)GetListCtrl();
	ctlList.SetFont(&ogCourier_Regular_8);
	
	// TODO: Add your specialized code here and/or call the base class
	
}

void UUniListView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{ 
	if(pSender != this)
	{
		EraseList();	
		UQueryDesingerDoc *polDoc = (UQueryDesingerDoc*)GetDocument();
		if(polDoc != NULL)
		{
			UListCtrlExtended& ctlList = (UListCtrlExtended&)GetListCtrl();
			ctlList.AddColumn("Relations                                   ", 0);
			if(polDoc->pomCurrentQuery != NULL)
			{
				for(int i = 0; i < polDoc->pomCurrentQuery->omObjRelation.GetSize(); i++)
				{
					ctlList.AddItem(i,0,polDoc->pomCurrentQuery->omObjRelation[i]);
				}
				ctlList.SetColumnWidth(-1, LVSCW_AUTOSIZE_USEHEADER  );
			}
		}
	}
}

void UUniListView::EraseList()
{
	UListCtrlExtended& ctlList = (UListCtrlExtended&)GetListCtrl();
	ctlList.DeleteAllItems();
	while(ctlList.DeleteColumn(0));
	UpdateWindow();
}


int UUniListView::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	lpCreateStruct->style |= LVS_REPORT;
	if (CListView::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	
	return 0;
}

void UUniListView::OnRButtonDown(UINT nFlags, CPoint point) 
{
	CMenu menu;
	menu.CreatePopupMenu();
	for (int i = menu.GetMenuItemCount(); --i >= 0;)
	{
	  menu.RemoveMenu(i, MF_BYPOSITION);
	}
	menu.AppendMenu(MF_STRING,50, LoadStg(IDS_STRING61451));	// confirm conflict
	ClientToScreen(&point);
	menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this, NULL);
	
	CListView::OnRButtonDown(nFlags, point);
}
void UUniListView::OnDeleteItem(UINT nFlags, CPoint point) 
{
	UListCtrlExtended& ctlList = (UListCtrlExtended&)GetListCtrl();
	POSITION pos; 
	char pclText[100]="";
	UQueryDesingerDoc *polDoc = (UQueryDesingerDoc*)GetDocument();
	if(polDoc != NULL)
	{
		pos = ctlList.GetFirstSelectedItemPosition( );
		while (pos)   
		{      
			int nItem = ctlList.GetNextSelectedItem(pos);
			ctlList.GetItemText(nItem, 0, pclText, 100);	
			CString olText = CString(pclText);
			polDoc->DeleteRelation(pclText);
			//polDoc->UpdateAllViews(this);
			ctlList.DeleteItem(nItem);
		}
	}
}