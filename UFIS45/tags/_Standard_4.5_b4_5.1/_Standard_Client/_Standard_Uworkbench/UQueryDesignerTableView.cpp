// UQueryDesignerTableView.cpp : implementation file
//

#include "stdafx.h"
#include "UWorkBench.h"
#include "UQueryDesignerTableView.h"
#include "UWorkBenchDoc.h"
#include "UQueryDesingerDoc.h"
#include "BasicData.h"
#include "AddTableDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// UQueryDesignerTableView

IMPLEMENT_DYNCREATE(UQueryDesignerTableView, CScrollView)

UQueryDesignerTableView::UQueryDesignerTableView()
{
}

UQueryDesignerTableView::~UQueryDesignerTableView()
{
	omTabWnds.DeleteAll();
}


BEGIN_MESSAGE_MAP(UQueryDesignerTableView, CScrollView)
	//{{AFX_MSG_MAP(UQueryDesignerTableView)
	ON_WM_ERASEBKGND()
	ON_WM_RBUTTONDOWN()
	ON_COMMAND(50, OnAddTables)
	ON_MESSAGE(WM_DESTROY_FLY_WND, OnDestroyFlyWnd)
	ON_MESSAGE(WM_DND_LBOX_BEGIN_DRAG, OnBeginDrag)
	ON_MESSAGE(WM_DND_LBOX_DRAG_OVER, OnDragOver)
	ON_MESSAGE(WM_DND_LBOX_DROP, OnDrop)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// UQueryDesignerTableView drawing

void UQueryDesignerTableView::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate();

	CSize sizeTotal;
	// TODO: calculate the total size of this view
	sizeTotal.cx = sizeTotal.cy = 1000;
	SetScrollSizes(MM_TEXT, sizeTotal);
}

void UQueryDesignerTableView::OnDraw(CDC* pDC)
{
	CDocument* pDoc = GetDocument();
	// TODO: add draw code here
}

/////////////////////////////////////////////////////////////////////////////
// UQueryDesignerTableView diagnostics

#ifdef _DEBUG
void UQueryDesignerTableView::AssertValid() const
{
	CScrollView::AssertValid();
}

void UQueryDesignerTableView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// UQueryDesignerTableView message handlers

void UQueryDesignerTableView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
int o = 0;
o++;	
	
}

void UQueryDesignerTableView::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	CScrollView::OnEndPrinting(pDC, pInfo);
}

void UQueryDesignerTableView::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	CScrollView::OnBeginPrinting(pDC, pInfo);
}

BOOL UQueryDesignerTableView::PreCreateWindow(CREATESTRUCT& cs) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	return CScrollView::PreCreateWindow(cs);
}



void UQueryDesignerTableView::OnRButtonDown(UINT nFlags, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	CMenu menu;
	menu.CreatePopupMenu();
	for (int i = menu.GetMenuItemCount(); --i >= 0;)
	{
	  menu.RemoveMenu(i, MF_BYPOSITION);
	}
	menu.AppendMenu(MF_STRING,50, LoadStg(IDS_STRING57608));	// confirm conflict
	ClientToScreen(&point);
	menu.TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, this, NULL);
	CScrollView::OnRButtonDown(nFlags, point);
}

void UQueryDesignerTableView::OnAddTables()
{
	CStringArray olTableList;
	AddTableDlg *polDlg = new AddTableDlg(this, &olTableList);
	polDlg->DoModal();
	delete polDlg;

	CString olStr;
	for(int i = 0; i < olTableList.GetSize(); i++)
	{
		CRect olNewRect;
		olNewRect = CalcNextFreeRect();
		TableListFrameWnd *polFlyFrame = new TableListFrameWnd(this, olTableList[i]);
		polFlyFrame->Create(NULL, "",  WS_CHILD| WS_HSCROLL | WS_CAPTION | WS_THICKFRAME | WS_VISIBLE | WS_SYSMENU/*| WS_MAXIMIZEBOX | WS_SYSMENU | WS_MINIMIZEBOX */,
							CRect(0, 0, 0, 0),this, NULL, 0, NULL);



		CRect o; polFlyFrame->GetWindowRect(&o);
		int W = o.right - o.left; 
		int H = o.bottom - o.top;
		FLYING_WINDOWS *polFlyWnd = new FLYING_WINDOWS;
		polFlyFrame->MoveWindow(&olNewRect);
		polFlyWnd->Rect = olNewRect;
		polFlyWnd->Window = polFlyFrame;
		omTabWnds.Add(polFlyWnd);
	}
	OnTablesChanged();
}
CRect UQueryDesignerTableView::CalcNextFreeRect()
{
	CRect olRect;
	CRect olWndRect;
	GetClientRect(&olWndRect);
	int ilTotalW = olWndRect.right - olWndRect.left - 20;
	int ilLeft = 20;
	int ilTop  = 10;
	int ilRight = olWndRect.right - olWndRect.left - 20;
	int ilWidth = 123;
	int ilHeight = 153;
	int ilBuffer = 50;
	int ilPerLine = (int)((ilTotalW)/(ilLeft+ilWidth));
	ilPerLine++;

	int ilCount = omTabWnds.GetSize();
	int ilCurrLine = 1;
	int ilNewLine = (int)((ilCount + 1)/(ilPerLine));
	int ilNewCol = (ilCount)%(ilPerLine-1);
	int ilNLeft, ilNRight, ilNTop, ilNBottom;
	if(ilNewCol == 0)
	{
		ilNLeft = ilLeft;
	}
	else
	{
		ilNLeft  = (ilNewCol)*(ilLeft+ilWidth)+ilLeft;
	}
	ilNRight = ilNLeft+ilWidth;
	if(ilNewLine == 0)
	{
		ilNTop   = ilTop;
	}
	else
	{
		ilNTop   = ilNewLine*(ilTop+ilHeight)+10;
	}
	ilNBottom = ilNTop + ilHeight;
	olRect = CRect(ilNLeft, ilNTop, ilNRight,ilNBottom);

	return olRect;


}

void UQueryDesignerTableView::OnTablesChanged()
{
	omTables.RemoveAll();
	for(int i = 0; i < omTabWnds.GetSize(); i++)
	{
		CString olTab;
		olTab = omTabWnds[i].Window->GetName();
		omTabWnds[i].Window->UpdateWindow();
		omTables.Add(olTab);
	}
	CUWorkBenchDoc *polDoc = (CUWorkBenchDoc *)GetDocument();
	if(polDoc != NULL)
	{
		polDoc->SetCurrentQueryDesignerTables(omTables);
	}
}

LONG UQueryDesignerTableView::OnDestroyFlyWnd(UINT wParam, LONG lParam)
{
	TableListFrameWnd *polWnd = (TableListFrameWnd*)lParam;
	for(int i = 0; i < omTabWnds.GetSize(); i++)
	{
		if(polWnd == omTabWnds[i].Window)
		{
			for(int j = 0; j < omTables.GetSize(); j++)
			{
				if(omTabWnds[i].Window->GetName() == omTables[j])
				{
					omTables.RemoveAt(j);
					omTabWnds[i].Window->DestroyWindow();
					CUWorkBenchDoc *polDoc = (CUWorkBenchDoc *)GetDocument();
					if(polDoc != NULL)
					{
						polDoc->SetCurrentQueryDesignerTables(omTables);
					}
					j = omTables.GetSize();
					omTabWnds.DeleteAt(i);
				}
			}
			i = omTabWnds.GetSize();
		}
	}
	return 0L;
}

BOOL UQueryDesignerTableView::OnEraseBkgnd(CDC* pDC) 
{

	CRect olRect;
	GetClientRect(&olRect);
	CBrush olSilverBrush;
	olSilverBrush.CreateSolidBrush(COLORREF(SILVER));
	pDC->FillRect(&olRect, &olSilverBrush);
//	return CScrollView::OnEraseBkgnd(pDC);
	return TRUE;
}

LONG UQueryDesignerTableView::OnBeginDrag(UINT wParam, LONG lParam)
{
	return 0L;
}

LONG UQueryDesignerTableView::OnDragOver(UINT wParam, LONG lParam)
{
	return 0L;
}

LONG UQueryDesignerTableView::OnDrop(UINT wParam, LONG lParam)
{
	if(!omDropSourceTable.IsEmpty() && !omDropDestinationTable.IsEmpty())
	{
		if(omDropSourceTable != omDropDestinationTable)//Avoid drop on the same table
		{
			UQueryDesingerDoc *polDoc = (UQueryDesingerDoc *)GetDocument();
			if(polDoc != NULL)
			{
				CString olRel = omDropSourceTable + CString(".") +
								 omDropSourceField + CString("=") +
								 omDropDestinationTable + CString(".") +
								 omDropDestinationField;
				polDoc->pomCurrentQuery->omObjRelation.Add(olRel);
				polDoc->SetModifiedFlag();
				polDoc->UpdateAllViews(this);
			}
		}
	}
	return 0L;
}

void UQueryDesignerTableView::SetRelSourceTable(CString opTable, CString opField)
{
	omDropSourceTable = opTable;
	omDropSourceField = opField;
}

void UQueryDesignerTableView::SetRelDestinationTable(CString opTable, CString opField)
{
	omDropDestinationTable = opTable;
	omDropDestinationField = opField;
}

