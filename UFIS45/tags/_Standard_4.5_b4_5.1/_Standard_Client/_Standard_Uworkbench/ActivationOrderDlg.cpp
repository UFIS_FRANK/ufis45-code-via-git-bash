// ActivationOrderDlg.cpp : implementation file
//

#include "stdafx.h"
#include "UWorkBench.h"
#include "ActivationOrderDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ActivationOrderDlg dialog

 
ActivationOrderDlg::ActivationOrderDlg(CWnd* pParent, UFormProperties *popProperty)
	: CDialog(ActivationOrderDlg::IDD, pParent)
{
	pomProperty = popProperty;
	//{{AFX_DATA_INIT(ActivationOrderDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void ActivationOrderDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ActivationOrderDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(ActivationOrderDlg, CDialog)
	//{{AFX_MSG_MAP(ActivationOrderDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ActivationOrderDlg message handlers

void ActivationOrderDlg::OnOK() 
{
	CGXStyle olStyle;
	int ilRows = omGrid.GetRowCount();

	pomProperty->omProperties.RemoveAll();
	for(int i = 0; i < ilRows/*pomProperty->omProperties.GetSize()*/; i++)
	{
		omGrid.GetStyleRowCol(i+1, 1, olStyle);
		void *polDataPtr = olStyle.GetItemDataPtr();
		if(polDataPtr != NULL)
		{
			pomProperty->omProperties.Add((UCtrlProperties*)polDataPtr);
		}
	}	
	CDialog::OnOK();
}

BOOL ActivationOrderDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	if(pomProperty == NULL)
		return FALSE;

	CString olText;
	olText.Format("Activation Order of Form %s", pomProperty->omName);
	SetWindowText(olText);
	omGrid.SubclassDlgItem(IDC_TABLE_LIST, this);
	omGrid.Initialize();
	omGrid.GetParam()->EnableUndo(FALSE);
	omGrid.LockUpdate(TRUE);
	omGrid.SetRowCount(pomProperty->omProperties.GetSize());
	omGrid.SetColCount(1);
	omGrid.SetColWidth(1,1,195);
	CGXStyle olStyle;

	for(int i = 0; i < pomProperty->omProperties.GetSize(); i++)
	{
		olStyle.SetItemDataPtr((void*)&(pomProperty->omProperties[i]));
		omGrid.SetStyleRange(CGXRange(i+1, 1), olStyle.SetValue(pomProperty->omProperties[i].omName));
		omGrid.SetValueRange(CGXRange(i+1, 1), pomProperty->omProperties[i].omName);
	}
	omGrid.LockUpdate(FALSE);

	return TRUE;  
	              
}
