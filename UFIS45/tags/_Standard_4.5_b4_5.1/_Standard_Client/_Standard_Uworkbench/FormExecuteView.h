#if !defined(AFX_FORMEXECUTEVIEW_H__3596C794_2F83_11D3_A200_00500437F607__INCLUDED_)
#define AFX_FORMEXECUTEVIEW_H__3596C794_2F83_11D3_A200_00500437F607__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FormExecuteView.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// FormExecuteView form view

#ifndef __AFXEXT_H__
#include <afxext.h>
#endif

#include "CCSEdit.h"
#include "GxGridTab.h"
#include "RunTimeCtrl.h"

/*class RunTimeCtrl : public CObject
{
public:
	RunTimeCtrl():CObject()
	{
		pomControl	  = NULL;
		pomProperties = NULL;
		pomFieldInfo  = NULL;
	}
	~RunTimeCtrl()
	{
		omCurrentRecords.DeleteAll();
		omNewRecords.DeleteAll();
		omDeletedRecords.DeleteAll();
		omChangedRecords.DeleteAll();
	}
	CString omType;		// "button", "edit", "checkbox", "combobox", "static", 
						// "line", "rectangle", "radio", "group", "listbox"
	CWnd	*pomControl;
	CCSPtrArray<RecordSet> omCurrentRecords;	//only used if the control is a grid
	CCSPtrArray<RecordSet> omNewRecords;		//only used if the control is a grid
	CCSPtrArray<RecordSet> omDeletedRecords;	//only used if the control is a grid
	CCSPtrArray<RecordSet> omChangedRecords;	//only used if the control is a grid

	UCtrlProperties *pomProperties;
	UFormField *pomFieldInfo;
};
*/
typedef CTypedPtrList<CObList, RunTimeCtrl*> URuntimeControls;

class FormExecuteView : public CFormView
{
protected:
	FormExecuteView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(FormExecuteView)

// Form Data
public:
	//{{AFX_DATA(FormExecuteView)
	enum { IDD = IDD_FORM_EXECUTE };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA
	FormExecuteDoc* GetDocument()
		{ return (FormExecuteDoc*)m_pDocument; }


	CString omMode;			//"update", "delete", "new"
	CString omRecordState;	//"unchanged", "changed"

	RecordSet omCurrentRecord;
	CBrush omBrush;
// Attributes
	URuntimeControls omControls;
	int imID;
public:

// Operations
	void OnFirst();
	void OnPrev();
	void OnNext();
	void OnLast();
	void OnNew();
	void OnSave();
	void OnDelete();
	void FillControl(RunTimeCtrl *popCtrl, RecordSet &olRecord);
	void SetCCSEditTpye(CCSEdit *popEdit, UCtrlProperties *popProperty);

	void InsertFormData();
	void UpdateFormData();

	RunTimeCtrl *GetCtrlByName(CString opName);
	bool FormExecuteView::MakeMasterValue(RunTimeCtrl *popChild, RecordSet &ropRecord, CString &ropErrorText);
	void MakeMasterChild(RunTimeCtrl *popChild, RecordSet &ropRecord);
	void FillGrid(RunTimeCtrl *polCtrl, UGridCtrlProperties *popGridProperties, RecordSet &ropRecord);

	bool FormExecuteView::HasChildFields(RunTimeCtrl *popCtrl);


	LONG OnCheckBoxKillFocus(UINT wParam, LONG lParam);
	LONG OnCheckBoxClicked(UINT wParam, LONG lParam);	
	LONG OnComboboxSelChanged(UINT wParam, LONG lParam);
	LONG OnComboboxKillFocus(UINT wParam, LONG lParam);

public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(FormExecuteView)
	public:
	virtual void OnInitialUpdate();
	virtual void OnPrepareDC(CDC* pDC, CPrintInfo* pInfo = NULL);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView);
	//}}AFX_VIRTUAL

// Implementation
protected: 
	virtual ~FormExecuteView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	// Generated message map functions
	//{{AFX_MSG(FormExecuteView)
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FORMEXECUTEVIEW_H__3596C794_2F83_11D3_A200_00500437F607__INCLUDED_)
