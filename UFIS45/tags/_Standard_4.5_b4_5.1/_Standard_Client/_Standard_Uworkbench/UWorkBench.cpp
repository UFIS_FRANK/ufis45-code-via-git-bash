// UWorkBench.cpp : Defines the class behaviors for the application.
//
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *	UWorkBench - Project
 *	
 *		CWinApp Mainclass 
 *
 *	ABB Airport Technologies GmbH 2000
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
 *
 *		Date		Author			Comment
 *		-------------------------------------------------------------
 *
 *		????????	mwo AAT/IR		Initial version	
 *		18/10/00	cla AAT/IR		Script host support added
 *		08/11/00	cla AAT/ID		Version change 2.0 - VB Methods added
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "stdafx.h"
#include "UWorkBench.h"

#include "MainFrm.h"
#include "ChildFrm.h"
#include "UWorkBenchDoc.h"
#include "UWorkBenchView.h"
#include "GridView.h"
#include "QueryDesingerTables.h"
#include "QueryChildFrame.h"
#include "CCSGlobl.h"
#include "UUFisObjectView.h"
#include "UUFisObjectsMDI.h"
#include "UQueryDesingerDoc.h"
#include "UScrollContainerView.h"
#include "UUniversalGridMDI.h"
#include "UUniversalGridDoc.h"
#include "UUniversalGridView.h"
#include "BasicData.h"

#include "UFISGridViewerDoc.h"
#include "UFISGridMDIChild.h"
#include "UFISGridViewerView.h"
#include "UDBLoadSetDefinitionDlg.h"

#include "FormDesignerMDI.h"
#include "FormDesignerView.h"
#include "FormDesignerDoc.h"

#include "FormExecuteDoc.h"
#include "FormExecuteView.h"
#include "FormExecuteMDI.h"

#include "VBScriptEditorDoc.h"
#include "VBScriptEditorMDI.h"
#include "VBScriptEditorView.h"

#include "LoginDialog.h"
#include "PrivList.h"
#include "RegisterDlg.h"
#include "ObjectCopyDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


SYSTEMTIME ogSysTime;

/////////////////////////////////////////////////////////////////////////////
// CUWorkBenchApp

BOOL StrToBOOL(CString opStr)
{
	if(opStr == "0")
	{
		return FALSE;
	}
	else
	{
		return TRUE;
	}
}

CString BOOL_ToString(BOOL bpBool)
{
	if(bpBool == TRUE)
	{
		return CString("1");
	}
	else
	{
		return CString("0");
	}
}

BEGIN_MESSAGE_MAP(CUWorkBenchApp, CWinApp)
	//{{AFX_MSG_MAP(CUWorkBenchApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	ON_COMMAND(ID_SAVE_ALL, OnSaveAll)
	ON_COMMAND(ID_LOADSET, OnShowDefaultLoadSet)
	ON_COMMAND(ID_OBJECTCOPY, OnObjectCopy )
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
	// Standard print setup command
	ON_COMMAND(ID_FILE_PRINT_SETUP, CWinApp::OnFilePrintSetup)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUWorkBenchApp construction

CUWorkBenchApp::CUWorkBenchApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CUWorkBenchApp object

CUWorkBenchApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CUWorkBenchApp initialization

BOOL CUWorkBenchApp::InitInstance()
{
	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	// Change the registry key under which our settings are stored.
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization.
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

	LoadStdProfileSettings();  // Load standard INI file options (including MRU)

//*****************
// VERSION of WorkBench
	omVersion = "1.0";
//*****************

	char pclConfigPath[142];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));
	char pclAccesMethod[128]="";
	char pclAccessMethod[128]="";
	char pclMode[128]="";
	CString olAccessMethod;
	CString olMode;
	char pclConnect[128]="";
	CString olConnect;
	olAccessMethod = "CEDA";
	char pclHomeAirport[100]="";
    GetPrivateProfileString("UWorkBench", "ACCESSMETHOD", "CEDA", pclAccessMethod, sizeof pclAccessMethod, pclConfigPath);
    GetPrivateProfileString("UWorkBench", "CONNECT", "CEDA", pclConnect, sizeof pclConnect, pclConfigPath);
    GetPrivateProfileString("UWorkBench", "MODE", "RUNTIME", pclMode, sizeof pclMode, pclConfigPath);

    GetPrivateProfileString("GLOBAL", "TABLEEXTENSION", "HAJ", pcgTableExt, sizeof pcgTableExt, pclConfigPath);
	GetPrivateProfileString("GLOBAL", "HOMEAIRPORT", "HAJ", pclHomeAirport, sizeof(pclHomeAirport), pclConfigPath);

	olConnect = CString(pclConnect);
	olAccessMethod = CString(pclAccessMethod);
	ogAccessMethod = olAccessMethod;
	olMode = CString(pclMode);
	ogMode = olMode;

	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.

	if(ogMode == "DESIGN")
	{
		CMultiDocTemplate* pDocTemplate;
		pDocTemplate = new CMultiDocTemplate(
			IDR_UWORKBTYPE,//IDR_GRIDTYPE,//IDR_UWORKBTYPE,
			RUNTIME_CLASS(CUWorkBenchDoc),
			RUNTIME_CLASS(CChildFrame), // custom MDI child frame
			RUNTIME_CLASS(/*CUWorkBenchView*/CGridView));
		AddDocTemplate(pDocTemplate);

		pDesignerDocTemplate = new CMultiDocTemplate(IDS_QUERYDESIGNER,
				RUNTIME_CLASS(UQueryDesingerDoc),
				RUNTIME_CLASS(CQueryChildFrame),
				RUNTIME_CLASS(/*QueryDesingerTables*/UScrollContainerView));
		AddDocTemplate(pDesignerDocTemplate);

		/*CMultiDocTemplate* */pDesignerTpl = new CMultiDocTemplate(IDS_UFIS_OBJECT_CONTAINER,
				RUNTIME_CLASS(UQueryDesingerDoc/*CUWorkBenchDoc*/),
				RUNTIME_CLASS(UUFisObjectsMDI),
				RUNTIME_CLASS(UUFisObjectView));
		AddDocTemplate(pDesignerTpl);

		pUniversalGridTpl = new CMultiDocTemplate(IDR_UNIVERSAL_GRID/*IDS_UNIVERSAL_GRID*/,
				RUNTIME_CLASS(UUniversalGridDoc/*CUWorkBenchDoc*/),
				RUNTIME_CLASS(UUniversalGridMDI),
				RUNTIME_CLASS(UUniversalGridView));
		AddDocTemplate(pUniversalGridTpl);


		pVBScriptTpl = new CMultiDocTemplate(IDS_FORMDESIGNER,//IDR_FORMDESIGNER,//IDS_FORMDESIGNER,
				RUNTIME_CLASS(CVBScriptEditorDoc),
				RUNTIME_CLASS(VBScriptEditorMDI),
				RUNTIME_CLASS(CVBScriptEditorView));
		AddDocTemplate(pVBScriptTpl);

	//Preview MDIs
		pUfisGridMDI = new CMultiDocTemplate(IDS_UFIS_GRID/*IDR_UFIS_GRID/*IDS_UNIVERSAL_GRID*/,
				RUNTIME_CLASS(CUFISGridViewerDoc),
				RUNTIME_CLASS(UFISGridMDIChild),
				RUNTIME_CLASS(CUFISGridViewerView));
		AddDocTemplate(pUfisGridMDI);
		
		pFormDesignerTpl = new CMultiDocTemplate(IDS_FORMDESIGNER,//IDR_FORMDESIGNER,//IDS_FORMDESIGNER,
				RUNTIME_CLASS(FormDesignerDoc),
				RUNTIME_CLASS(FormDesignerMDI),
				RUNTIME_CLASS(FormDesignerView));
		pFormDesignerTpl->SetContainerInfo(IDR_FORMDESIGNER);
		AddDocTemplate(pFormDesignerTpl);

		pFormExecuteTpl = new CMultiDocTemplate(IDS_FORMDESIGNER,//IDR_FORMDESIGNER,//IDS_FORMDESIGNER,
				RUNTIME_CLASS(FormExecuteDoc),
				RUNTIME_CLASS(FormExecuteMDI),
				RUNTIME_CLASS(FormExecuteView));
	//	pFormDesignerTpl->SetContainerInfo(IDR_FORMDESIGNER);
		AddDocTemplate(pFormExecuteTpl);
	}
	else
	{
		CMultiDocTemplate* pDocTemplate;
		pDocTemplate = new CMultiDocTemplate(
			IDR_UWORKBTYPE_RUNTIME,//IDR_GRIDTYPE,//IDR_UWORKBTYPE,
			RUNTIME_CLASS(CUWorkBenchDoc),
			RUNTIME_CLASS(CChildFrame), // custom MDI child frame
			RUNTIME_CLASS(/*CUWorkBenchView*/CGridView));
		AddDocTemplate(pDocTemplate);

		pDesignerDocTemplate = new CMultiDocTemplate(IDS_QUERYDESIGNER_RUNTIME,
				RUNTIME_CLASS(UQueryDesingerDoc),
				RUNTIME_CLASS(CQueryChildFrame),
				RUNTIME_CLASS(/*QueryDesingerTables*/UScrollContainerView));
		AddDocTemplate(pDesignerDocTemplate);

		/*CMultiDocTemplate* */pDesignerTpl = new CMultiDocTemplate(IDS_UFIS_OBJECT_CONTAINER_RUNTIME,
				RUNTIME_CLASS(UQueryDesingerDoc/*CUWorkBenchDoc*/),
				RUNTIME_CLASS(UUFisObjectsMDI),
				RUNTIME_CLASS(UUFisObjectView));
		AddDocTemplate(pDesignerTpl);

		pUniversalGridTpl = new CMultiDocTemplate(IDR_UNIVERSAL_GRID_RUNTIME/*IDS_UNIVERSAL_GRID*/,
				RUNTIME_CLASS(UUniversalGridDoc/*CUWorkBenchDoc*/),
				RUNTIME_CLASS(UUniversalGridMDI),
				RUNTIME_CLASS(UUniversalGridView));
		AddDocTemplate(pUniversalGridTpl);




	//Preview MDIs
		pUfisGridMDI = new CMultiDocTemplate(IDS_UFIS_GRID_RUNTIME/*IDR_UFIS_GRID/*IDS_UNIVERSAL_GRID*/,
				RUNTIME_CLASS(CUFISGridViewerDoc),
				RUNTIME_CLASS(UFISGridMDIChild),
				RUNTIME_CLASS(CUFISGridViewerView));
		AddDocTemplate(pUfisGridMDI);
		
		pVBScriptTpl = new CMultiDocTemplate(IDS_FORMDESIGNER_RUNTIME,//IDR_FORMDESIGNER,//IDS_FORMDESIGNER,
				RUNTIME_CLASS(CVBScriptEditorDoc),
				RUNTIME_CLASS(VBScriptEditorMDI),
				RUNTIME_CLASS(CVBScriptEditorView));
		AddDocTemplate(pVBScriptTpl);

		pFormDesignerTpl = new CMultiDocTemplate(IDS_FORMDESIGNER_RUNTIME,//IDR_FORMDESIGNER,//IDS_FORMDESIGNER,
				RUNTIME_CLASS(FormDesignerDoc),
				RUNTIME_CLASS(FormDesignerMDI),
				RUNTIME_CLASS(FormDesignerView));
		pFormDesignerTpl->SetContainerInfo(IDR_FORMDESIGNER_RUNTIME);
		AddDocTemplate(pFormDesignerTpl);

		pFormExecuteTpl = new CMultiDocTemplate(IDS_FORMDESIGNER_RUNTIME,//IDR_FORMDESIGNER,//IDS_FORMDESIGNER,
				RUNTIME_CLASS(FormExecuteDoc),
				RUNTIME_CLASS(FormExecuteMDI),
				RUNTIME_CLASS(FormExecuteView));
	//	pFormDesignerTpl->SetContainerInfo(IDR_FORMDESIGNER);
		AddDocTemplate(pFormExecuteTpl);
	}
/****Clean duplicated whitespace(s)
	CString olol = CString("CTL_MOVE       <left>  <top>  <right>		<bottom>	");
	while (olol.Replace("  ", " ") > 0);
	while (olol.Replace("	", " ") > 0);
	if(olol[olol.GetLength()-1] == ' ' || olol[olol.GetLength()-1] == '	')
	{
		olol.Delete(olol.GetLength()-1,1);
	}

Clean whitespace*****/



	ogPropertiesDlgRect = CRect(0,0,0,0);
	InitSetup(); // Must be feore ReadDataFromCfg();



	InitFont();
//-----
// Ceda Initialization
	//ogAppName="XXX";
	strcpy(CCSCedaData::pcmTableExt, pcgTableExt);
	strcpy(CCSCedaData::pcmHomeAirport, pclHomeAirport);
	strcpy(CCSCedaData::pcmApplName, ogAppName);


    //GetPrivateProfileString("UClassesEnv", "LANGUAGE", "ENGLISH", pcgLanguage, sizeof pcgLanguage, pclConfigPath);

//****************************
// Red configuration
	ReadDataFromCfg();
//****************************



//*NOCEDA
	if(olAccessMethod != "ODBC")
	{
		ogCommHandler.SetAppName(ogAppName);
		if (ogCommHandler.Initialize() != true)  // connection error?
		{
			AfxMessageBox(CString("CedaCom:\n") + ogCommHandler.LastError());
			return FALSE;
		}
	}
//-------------------------------------------------------
// Login Section
	char pclApplications[512]="";
	CStringArray olAppArr;
	CString olAppsStr;
    GetPrivateProfileString("UWorkBench", "APPLICATIONS", "Basicdata", pclApplications, sizeof pclApplications, pclConfigPath);
	olAppsStr = CString(pclApplications);
	if(ogAccessMethod != "ODBC")
	{
		ExtractItemList(olAppsStr, &olAppArr, ';');
		LoginDialog olLoginDlg(pcgHome,olAppArr,NULL);
		if( olLoginDlg.DoModal() != IDCANCEL )
		{
			int ilStartApp = IDOK; 
			if(ogPrivList.GetStat("InitModu") == '1')
			{
				RegisterDlg olRegisterDlg;
				ilStartApp = olRegisterDlg.DoModal();
			}
			if(ilStartApp != IDOK)
			{
				return FALSE;
			}

		}
		else
		{
			return FALSE;
		}
	}

// End Login Section
//-------------------------------------------------------
 	ogBCD.SetAccessMethod(olAccessMethod);
	ogBCD.SetODBCConnect(olConnect);
	ogBCD.SetTableExtension(pcgTableExt);
//	ogBCD.SetObject("TAB", "URNO,LTNA,LNAM");
//	ogBCD.SetObjectDesc("TAB", "TABTAB");
//	ogBCD.Read(CString("TAB"));


	CString olT;
	CTime olTime = CTime::GetCurrentTime();
	olT = olTime.Format("%H:%M:%S");
	TRACE("Start %s\n", olT.GetBuffer(0));
 	ogBCD.SetObject("SYS", "URNO,TANA,TYPE,FINA,FELE,FITY,REFE,ADDI,LABL,REQF");
	ogBCD.SetObjectDesc("SYS", "SYSTAB");
// 	ogBCD.AddKeyMap("SYS", "TANA");
  	ogBCD.Read(CString("SYS"));

// Read View Definitions 
	if(ogAccessMethod == "ODBC")
	{
 		ogBCD.SetObject("VCD", "URNO,APPN,CKEY,CTYP,PKNO,TXT1");
	}
	else
	{
 		ogBCD.SetObject("VCD", "URNO,APPN,CKEY,CTYP,PKNO,TEXT");
	}
	ogBCD.SetObjectDesc("VCD", "SYSTAB");
  	ogBCD.Read(CString("VCD"), "WHERE APPN='UBDPS'");

//	ogBCD.SetSort(CString("SYS"), "TANA+,FINA+", true);
	olTime = CTime::GetCurrentTime();
	olT = olTime.Format("%H:%M:%S");
	TRACE("End %s\n", olT.GetBuffer(0));


	int ilTanaIdx = ogBCD.GetFieldIndex("SYS", "TANA");
	int ilDataCount = ogBCD.GetDataCount("SYS");
	for(int i = 0; i < ilDataCount; i++)
	{
		RecordSet olRecord;
		ogBCD.GetRecord("SYS", i,  olRecord);
		CString Tana = olRecord[ogBCD.GetFieldIndex("SYS", "TANA")];
		if(Tana.GetLength() > 3)
		{
			int o=0;
			o++;
		}
		void *p;
		if(ogTanaMap.Lookup(olRecord[ogBCD.GetFieldIndex("SYS", "TANA")], (void*&)p) == FALSE)
		{
			ogTanaMap.SetAt(olRecord[ilTanaIdx], (void *&)olRecord[ogBCD.GetFieldIndex("SYS", "TANA")]);
		}
	}


	ogTools.InitDefaultLoadSetInfo();
//	ogTools.LoadDataWithDefaultSet();

//End Ceda Init
//------
	

	GXInit();
	GXSetNewGridLineMode(TRUE);
#if _MFC_VER >= 0x0400
	AfxOleInit();
#endif


	// create main MDI Frame window
	/*CMainFrame* */pMainFrame = new CMainFrame;
	if(olMode == "DESIGN")
	{
		if (!pMainFrame->LoadFrame(IDR_UNIVERSAL_GRID/*IDR_MAINFRAME*/))
			return FALSE;
	}
	else
	{
		if (!pMainFrame->LoadFrame(IDR_UNIVERSAL_GRID_RUNTIME/*IDR_MAINFRAME*/))
			return FALSE;
	}
	m_pMainWnd = pMainFrame;


	// Parse command line for standard shell commands, DDE, file open
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);

	// Dispatch commands specified on the command line
//	if (!ProcessShellCommand(cmdInfo))
//		return FALSE;

	if(olMode == "DESIGN")
	{
		pDesignerTpl->OpenDocumentFile(NULL);
	}
	// The main window has been initialized, so show and update it.
	pMainFrame->ShowWindow(SW_SHOWMAXIMIZED/*m_nCmdShow*/);
	pMainFrame->UpdateWindow();

	m_pScriptHost = CScriptHost::CreateInstance();

	return TRUE;
}


/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
		// No message handlers
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// App command to run the dialog
void CUWorkBenchApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

/////////////////////////////////////////////////////////////////////////////
// CUWorkBenchApp message handlers


int CUWorkBenchApp::ExitInstance() 
{
	int i;

	if(ogMode == "DESIGN")
	{
		OnSaveAll();
	}
	for( i = 0; i < ogLoadSets.GetSize(); i++)
	{
		ogLoadSets[i].TableArray.DeleteAll();
	}
	ogLoadSets.DeleteAll();
	for(i = 0; i < ogQueries.GetSize(); i++)
	{
		ogQueries[i].omQueryDetails.DeleteAll();
	}
	ogQueries.DeleteAll();
	for( i = 0; i < ogGridDefs.GetSize(); i++)
	{
		ogGridDefs[i].omGridFields.DeleteAll();
	}
	ogGridDefs.DeleteAll();
//	GXTerminate( );

	 if (ogSysTime.wYear != 0)
	 {
		SYSTEMTIME olTmpTime;

		GetSystemTime(&olTmpTime);

		ogSysTime.wHour = olTmpTime.wHour;
		ogSysTime.wMinute = olTmpTime.wMinute;
		ogSysTime.wSecond = olTmpTime.wSecond;

		SetSystemTime(&ogSysTime);
			
	 }

	 for( i = 0; i < ogForms.GetSize(); i++)
	 {
		for(int j = ogForms[i].omProperties.GetSize()-1; j >= 0; j--)
		{
			CString olCtrlType = ogForms[i].omProperties[j].omCtrlType;
			if(olCtrlType == "grid")
			{
				UGridCtrlProperties *polCtrl = (UGridCtrlProperties *)&(ogForms[i].omProperties[j]);
				delete polCtrl;
			}
			else if(olCtrlType == "edit")
			{
				UEditCtrlProperties *polCtrl = (UEditCtrlProperties *)&(ogForms[i].omProperties[j]);
				delete polCtrl;
			}
			else if(olCtrlType == "static")
			{
				UStaticCtrlProperties *polCtrl = (UStaticCtrlProperties *)&(ogForms[i].omProperties[j]);
				delete polCtrl;
			}
			else if(olCtrlType == "button")
			{
				UButtonCtrlProperties *polCtrl = (UButtonCtrlProperties *)&(ogForms[i].omProperties[j]);
				delete polCtrl;
			}
			else if(olCtrlType == "checkbox")
			{
				UCheckboxCtrlProperties *polCtrl = (UCheckboxCtrlProperties *)&(ogForms[i].omProperties[j]);
				delete polCtrl;
			}
			else if(olCtrlType == "combobox")
			{
				UComboboxCtrlProperties *polCtrl = (UComboboxCtrlProperties *)&(ogForms[i].omProperties[j]);
				delete polCtrl;
			}
			else if(olCtrlType == "line")
			{
				ULineCtrlProperties *polCtrl = (ULineCtrlProperties *)&(ogForms[i].omProperties[j]);
				delete polCtrl;
			}
			else if(olCtrlType == "rectangle")
			{
				URectCtrlProperties *polCtrl = (URectCtrlProperties *)&(ogForms[i].omProperties[j]);
				delete polCtrl;
			}
			else if(olCtrlType == "radio")
			{
				URadioCtrlProperties *polCtrl = (URadioCtrlProperties *)&(ogForms[i].omProperties[j]);
				delete polCtrl;
			}
			else
			{//
				ogForms[i].omProperties.DeleteAt(j);
			}
			//ogForms[i].omProperties.DeleteAll();
		}
		ogForms[i].omProperties.RemoveAll();
		ogForms[i].omDBFields.DeleteAll();
	 }
	 ogForms.DeleteAll();
	 ogDDXMapper.DeleteAll();
	return CWinApp::ExitInstance();
}

void CUWorkBenchApp::OnShowDefaultLoadSet()
{
	UDBLoadSet *polSet = NULL;
	for(int i = 0; i < ogLoadSets.GetSize(); i++)
	{
		if(ogLoadSets[i].Name == "Default System Set")
		{
			polSet = &ogLoadSets[i];
			break;
		}
	}
	if(polSet != NULL)
	{
		UDBLoadSetDefinitionDlg *polDlg = new UDBLoadSetDefinitionDlg(NULL, polSet/*polTabTmp*/);
		if(polDlg->DoModal() == IDOK) // Cancel gibt es nicht mehr
		{
		}
		delete polDlg;
	}
}

//------------------------------------------------------------------------
// Open the dialog for creating an new file (or update an existing)
// for Object reuse in other WorkBench applications (configs)
//------------------------------------------------------------------------
void CUWorkBenchApp::OnObjectCopy()
{
	ObjectCopyDlg *polDlg = new ObjectCopyDlg(NULL);
	polDlg->DoModal();
	delete polDlg;
}

void CUWorkBenchApp::OnSaveAll()
{
	CString olFileName;
	olFileName = ogPseudoAppName + ".cfg";
	SaveConfigAs(olFileName.GetBuffer(0), ogLoadSets, ogGridDefs, ogForms, ogQueries);
		
}//OnSaveAll()


//--------------------------------------------------------------------------------
// MWO:
// For multiple use of the saveAs method, exspecially for the extraction of
// the content of an existing cfg into an new one we have to parameterize the
// objects (and filename)
//--------------------------------------------------------------------------------
void CUWorkBenchApp::SaveConfigAs(char* pcpFileName, 
				  CCSPtrArray<UDBLoadSet> &ropLoadSets,
				  CCSPtrArray<UGridDefinition> &ropGridDefs,
				  CCSPtrArray<UFormProperties> &ropForms,
				  CCSPtrArray<UQuery> &ropQueries)
{
	int i, j, k;
	if(pcpFileName == NULL)
	{
		return;
	}
	TRY
	{
	CFile olFile(pcpFileName, CFile::modeCreate|CFile::modeWrite );
	CString olOut;
	//VERSION-INFO of .cfg-file
	olOut = "VERSION[" + omVersion + "]\n\n";
	//1st write DataLoadSetDefinition
	olOut += "[LOADSET]\n{\n";

	for( i = 0; i < ropLoadSets.GetSize(); i++)
	{
		olOut+=MakeCedaString(ropLoadSets[i].Name) + ":";
		int ilTmp = ropLoadSets[i].TableArray.GetSize();
		for( j = 0; j <  ropLoadSets[i].TableArray.GetSize(); j++)
		{
			olOut+="@";
			olOut+=MakeCedaString(ropLoadSets[i].TableArray[j].Name) + ",";//Name of table
			olOut+=MakeCedaString(ropLoadSets[i].TableArray[j].SqlPart) + ",";//Sqlpart for the table's selection 
			if(ropLoadSets[i].TableArray[j].IsTimeFrameObject == true)
			{
				//write 1 for true or 0 for false
				olOut+="1,";
			}
			else
			{
				olOut+="0,";
			}
			if(ropLoadSets[i].TableArray[j].IsAFTObject == true)
			{
				//write 1 for true or 0 for false
				olOut+="1,";
			}
			else
			{
				olOut+="0,";
			}
			if(ropLoadSets[i].TableArray[j].IsIndirectLoad == true)
			{
				//write 1 for true or 0 for false
				olOut+="1,";
			}
			else
			{
				olOut+="0,";
			}
			//the fields
			for(k = 0; k < ropLoadSets[i].TableArray[j].FieldList.GetSize(); k++)
			{
				olOut+=MakeCedaString(ropLoadSets[i].TableArray[j].FieldList[k]) + ",";
			}
		}
		olOut+="@";
		olOut+="\n";
	}	
	olOut+="}\n";

//-------------- Query Section
	olOut += "[QUERIES]\n{\n";

	for( i = 0; i < ropQueries.GetSize(); i++)
	{
		olOut += MakeCedaString(ropQueries[i].omName) + CString(":");
		//Used Tables
		for(j = 0; j < ropQueries[i].omUsedTables.GetSize(); j++)
		{ 
			if(j == 0)
			{
				olOut += MakeCedaString(ropQueries[i].omUsedTables[j]);
			}
			else
			{
				olOut += CString(",") + MakeCedaString(ropQueries[i].omUsedTables[j]);
			}
		}
		olOut += ":";
		//Relations
		for(j = 0; j < ropQueries[i].omObjRelation.GetSize(); j++)
		{ 
			if(j == 0)
			{
				olOut += MakeCedaString(ropQueries[i].omObjRelation[j]);
			}
			else
			{
				olOut += CString(",") + MakeCedaString(ropQueries[i].omObjRelation[j]);
			}
		}
		olOut += ":";
		//Query Details
		for( j = 0; j < ropQueries[i].omQueryDetails.GetSize(); j++)
		{
			CString olColNo;
			olColNo.Format("%d", ropQueries[i].omQueryDetails[j].imColNo);
			olOut += olColNo + CString(",");
			olOut += MakeCedaString(ropQueries[i].omQueryDetails[j].Object) + CString(",");	
			olOut += MakeCedaString(ropQueries[i].omQueryDetails[j].Field) + CString(",");	
			olOut += MakeCedaString(ropQueries[i].omQueryDetails[j].Sort) + CString(",");	
			olOut += MakeCedaString(ropQueries[i].omQueryDetails[j].Operator) + CString(",");
			olOut += MakeCedaString(ropQueries[i].omQueryDetails[j].Criteria) + CString(",");
			olOut += MakeCedaString(ropQueries[i].omQueryDetails[j].omShowAsResult) + CString("#");
		}
		olOut+="\n";
//MMMMM
		olFile.Write(olOut.GetBuffer(0), olOut.GetLength());
		olOut = "";
//MMMMMM
	}	
	olOut+="}\n";


	olOut += "[GRIDS]\n{\n";
	for(i = 0; i < ropGridDefs.GetSize(); i++)
	{
		CString olText;
		CString olPart;
		olPart += MakeCedaString(ropGridDefs[i].GridName) + CString(",");
		olPart += BOOL_ToString(ropGridDefs[i].AsBasicData) + CString(",");
		olPart += MakeCedaString(ropGridDefs[i].FormName) + CString(",");
		olPart += BOOL_ToString(ropGridDefs[i].AsFrameWindow) + CString(",");
		olPart += BOOL_ToString(ropGridDefs[i].AsChoiceList) + CString(",");
		olPart += MakeCedaString(ropGridDefs[i].DataSource) + CString(",");
		olPart += MakeCedaString(ropGridDefs[i].Filter) + CString(",");
		olPart += BOOL_ToString(ropGridDefs[i].HasViewButton) + CString(",");
		olPart += BOOL_ToString(ropGridDefs[i].HeaderSize) + CString(",");
		olPart += BOOL_ToString(ropGridDefs[i].IsEditable) + CString(",");
		olPart += BOOL_ToString(ropGridDefs[i].MultiSelect) + CString(",");
		olPart += BOOL_ToString(ropGridDefs[i].Printable) + CString(",");
		olPart += MakeCedaString(ropGridDefs[i].ReturnField) + CString(",");
		olPart += BOOL_ToString(ropGridDefs[i].SensitiveForChanges) + CString(",");
		olPart += BOOL_ToString(ropGridDefs[i].SetDefaultTimeFrame) + CString(",");
		olPart += BOOL_ToString(ropGridDefs[i].ShowTimeFrameDialog) + CString(",");
		olText.Format("%d,", ropGridDefs[i].Today);//15
		olPart+=olText;
		olPart+=MakeCedaString(ropGridDefs[i].TodayFrom) + CString(",");
		olPart+=MakeCedaString(ropGridDefs[i].TodayTo) + CString(",");
		olPart+=MakeCedaString(ropGridDefs[i].TomorrowFrom) + CString(",");
		olPart+=MakeCedaString(ropGridDefs[i].TomorrowTo) + CString(",");
		olText.Format("%d", ropGridDefs[i].left);
		olPart+=olText + CString(",");;
		olText.Format("%d", ropGridDefs[i].top);
		olPart+=olText + CString(",");;
		olText.Format("%d", ropGridDefs[i].right);
		olPart+=olText + CString(",");;
		olText.Format("%d", ropGridDefs[i].bottom);
		olPart+=olText + CString(",");;
		olPart+=MakeCedaString(ropGridDefs[i].omTableRelations) + CString(",");
		olPart+=MakeCedaString(ropGridDefs[i].omRowConnection) + CString(",");
		olPart+=MakeCedaString(ropGridDefs[i].omSortDirection);
		olPart+=CString("#"); //Separator for main definition part

		olOut += olPart;
		for( j = 0; j < ropGridDefs[i].omTables.GetSize(); j++)
		{
			olOut+=MakeCedaString(ropGridDefs[i].omTables[j]);
			if((j+1) < ropGridDefs[i].omTables.GetSize())
			{
				olOut+= CString(",");
			}
		}

		olOut+=CString("#"); //Separator for main definition part
		for( j = 0; j < ropGridDefs[i].omColWidths.GetSize(); j++)
		{
			olText.Format("%d", ropGridDefs[i].omColWidths[j]);
			olOut+=olText;
			if((j+1) < ropGridDefs[i].omColWidths.GetSize())
			{
				olOut+= CString(",");
			}
		}
		olOut+=CString("#"); //Separator for main definition part
		
		for( j = 0; j < ropGridDefs[i].omSortColumns.GetSize(); j++)
		{
			olText.Format("%d", ropGridDefs[i].omSortColumns[j]);
			olOut+=olText;
			if((j+1) < ropGridDefs[i].omSortColumns.GetSize())
			{
				olOut+= CString(",");
			}
		}
		olOut+=CString("#"); //Separator for main definition part
		
		olOut+= CString("@");
		for( j = 0; j < ropGridDefs[i].omGridFields.GetSize(); j++)
		{
			olOut+=MakeCedaString(ropGridDefs[i].omGridFields[j].Table) + CString(",");
			olOut+=MakeCedaString(ropGridDefs[i].omGridFields[j].Field) + CString(",");
			olOut+=MakeCedaString(ropGridDefs[i].omGridFields[j].Type) + CString(",");
			olOut+=MakeCedaString(ropGridDefs[i].omGridFields[j].Header) + CString(",");
			olOut+=MakeCedaString(ropGridDefs[i].omGridFields[j].Visible) + CString(",");
			olOut+=MakeCedaString(ropGridDefs[i].omGridFields[j].Key) + CString(",");
			olOut+=MakeCedaString(ropGridDefs[i].omGridFields[j].Unique) + CString(",");
			olOut+=MakeCedaString(ropGridDefs[i].omGridFields[j].QueryName);
			olOut+= CString("@");
		}
		olOut+=/*CString("#") + */CString("\n");
//MMMMM
		olFile.Write(olOut.GetBuffer(0), olOut.GetLength());
		olOut = "";
//MMMMMM
	}
	olOut+="}\n";

//--------- Forms ----------------------------------

	olOut += "[FORMS]\n{\n";
	for( i = 0; i < ropForms.GetSize(); i++)
	{
		CString olText; //for number formatting
		olOut += MakeCedaString(ropForms[i].omName) + CString(",");			//Name of Form
		olText.Format("%d,", ropForms[i].omRect.left);
		olOut += olText;
		olText.Format("%d,", ropForms[i].omRect.top);
		olOut += olText;
		olText.Format("%d,", ropForms[i].omRect.right);
		olOut += olText;
		olText.Format("%d,", ropForms[i].omRect.bottom);
		olOut += olText;
		olOut += MakeCedaString(ropForms[i].omTable) + CString(",");			// Main DB-Table
		olOut += MakeCedaString(ropForms[i].omBorderType) + CString(",");	// "modal" or "size"
		olOut += MakeCedaString(ropForms[i].omWindowType) + CString(",");	// 

		olText.Format("%d,", ropForms[i].imStaticCount);	// Counter for controls
		olOut += olText;
		olText.Format("%d,", ropForms[i].imEditCount	);	
		olOut += olText;
		olText.Format("%d,", ropForms[i].imComboboxCount);	
		olOut += olText;
		olText.Format("%d,", ropForms[i].imCheckboxCount	);
		olOut += olText;
		olText.Format("%d,", ropForms[i].imRadioCount	);
		olOut += olText;
		olText.Format("%d,", ropForms[i].imListBoxCount);	
		olOut += olText;
		olText.Format("%d,", ropForms[i].imButtonCount);	
		olOut += olText;
		olText.Format("%d,", ropForms[i].imRectCount	);	
		olOut += olText;
		olText.Format("%d,", ropForms[i].imLineCount	);	
		olOut += olText;

		//inherited properties
		olOut += MakeCedaString(ropForms[i].omCtrlType) + CString(",");		// Control type ==> not relevant for a form
		olOut += MakeCedaString(ropForms[i].omLabel) + CString(",");
		olOut += MakeCedaString(ropForms[i].omOrientation) + CString(",");
		olText.Format("%ld,", ropForms[i].omBackColor);
		olOut += olText;
		olText.Format("%ld,", ropForms[i].omFontColor);
		olOut += olText;
		olText.Format("%ld,", ropForms[i].omBorderColor	);
		olOut += olText;
		olOut += BOOL_ToString(ropForms[i].bmEnabled) + CString(",");
		olOut += BOOL_ToString(ropForms[i].bmVisible) + CString(",");
		olOut += BOOL_ToString(ropForms[i].bmModal) + CString(",");
		olText.Format("%d,", ropForms[i].imBorderWidth);
		olOut += olText;   
		olOut += BOOL_ToString(ropForms[i].bmDBField) + CString(",");		
		olOut += MakeCedaString(ropForms[i].omFieldType) + CString(",");
		olOut += MakeCedaString(ropForms[i].omFormatString);
		olOut += CString("|");
		//ropForms[i].omTables
		for( int j = 0; j < ropForms[i].omTables.GetSize(); j++)
		{
			olOut += MakeCedaString(ropForms[i].omTables[j]);
			if((j+1) < ropForms[i].omTables.GetSize())
			{
				 olOut += CString(",");
			}
		}
		olOut += CString("|");

		//ropForms[i].omDBFields
		for( j = 0; j < ropForms[i].omDBFields.GetSize(); j++)
		{
			olOut += MakeCedaString(ropForms[i].omDBFields[j].Table) + CString(",");						// Tablename
		    olOut += MakeCedaString(ropForms[i].omDBFields[j].Field) + CString(",");						// Fieldname
			olOut += MakeCedaString(ropForms[i].omDBFields[j].Type) + CString(",");						// Datatype
			olOut += MakeCedaString(ropForms[i].omDBFields[j].Header) + CString(",");						// Used as lablename
			olOut += MakeCedaString(ropForms[i].omDBFields[j].Visible) + CString(",");					// Field visible/invisible
			olOut += MakeCedaString(ropForms[i].omDBFields[j].Key) + CString(",");						// For the moment  from SYSTAB ==> may be changed later
			olOut += MakeCedaString(ropForms[i].omDBFields[j].Refe) + CString(",");
			olOut += MakeCedaString(ropForms[i].omDBFields[j].HiddenRefe);			
			// Reference Field (e.g. for lookup comboboxes)
			if((j+1) < ropForms[i].omDBFields.GetSize())
			{
				 olOut += CString("@");
			}

		}
//MMMMM
		olFile.Write(olOut.GetBuffer(0), olOut.GetLength());
		olOut = "";
//MMMMMM
//		olOut += CString("|");
		olOut += CString("#");


		//Form-Properties
		for( j = 0; j < ropForms[i].omProperties.GetSize(); j++)
		{
			if(ropForms[i].omProperties[j].omCtrlType == "grid")
			{
				UGridCtrlProperties *polGridProp = (UGridCtrlProperties *)&(ropForms[i].omProperties[j]);
				//0
				CString CTL_TYPE = polGridProp->omCtrlType;
				olOut += MakeCedaString(polGridProp->omName) + CString(",");			//Name of Property
				olText.Format("%d,", polGridProp->omRect.left);
				olOut += olText;
				olText.Format("%d,", polGridProp->omRect.top);
				olOut += olText;
				olText.Format("%d,", polGridProp->omRect.right);
				olOut += olText;
				olText.Format("%d,", polGridProp->omRect.bottom);
				olOut += olText;
				olOut += BOOL_ToString(polGridProp->bmEnabled) + CString(",");
				//Achtung To Do 1. Control type als erstes speichern 
				// solange das noch nicht der Fall ist wenigstens immer darauf achten,
				// Controltype an der 7. Stelle gespeichert ist
				olOut += MakeCedaString(polGridProp->omCtrlType) + CString(",");		// Control type ==> not relevant for a form
				olOut += BOOL_ToString(polGridProp->bmVisible) + CString(",");
				// So und jetzt der cast nach UGridCtrlProperties
				olOut += MakeCedaString(polGridProp->omGridTable) + CString(",");		// Control type ==> not relevant for a form
				olOut += MakeCedaString(polGridProp->omMasterDetail) + CString(",");
				for(int k = 0; k < polGridProp->omFieldlist.GetSize(); k++)
				{
					if((k+1) == polGridProp->omFieldlist.GetSize())
					{
						olOut += MakeCedaString(polGridProp->omFieldlist[k]);
					}
					else
					{
						olOut += MakeCedaString(polGridProp->omFieldlist[k]) + CString(";");
					}
				}
				olOut += CString(",");
				for( k = 0; k < polGridProp->omTypelist.GetSize(); k++)
				{
					if((k+1) == polGridProp->omTypelist.GetSize())
					{
						olOut += MakeCedaString(polGridProp->omTypelist[k]);
					}
					else
					{
						olOut += MakeCedaString(polGridProp->omTypelist[k]) + CString(";");
					}
				}
				olOut += CString(",");
				for( k = 0; k < polGridProp->omHaederlist.GetSize(); k++)
				{
					if((k+1) == polGridProp->omHaederlist.GetSize())
					{
						olOut += MakeCedaString(polGridProp->omHaederlist[k]);
					}
					else
					{
						olOut += MakeCedaString(polGridProp->omHaederlist[k]) + CString(";");
					}
				}
				olOut += CString(",");
				for( k = 0; k < polGridProp->omVisiblelist.GetSize(); k++)
				{
					if((k+1) == polGridProp->omVisiblelist.GetSize())
					{
						olOut += MakeCedaString(polGridProp->omVisiblelist[k]);
					}
					else
					{
						olOut += MakeCedaString(polGridProp->omVisiblelist[k]) + CString(";");
					}
				}
				olOut += CString(",");
				for( k = 0; k < polGridProp->omLookupfieldList.GetSize(); k++)
				{
					if((k+1) == polGridProp->omLookupfieldList.GetSize())
					{
						olOut += MakeCedaString(polGridProp->omLookupfieldList[k]);
					}
					else
					{
						olOut += MakeCedaString(polGridProp->omLookupfieldList[k]) + CString(";");
					}
				}
				olOut += CString(",");
				for( k = 0; k < polGridProp->omMandatorylist.GetSize(); k++)
				{
					if((k+1) == polGridProp->omMandatorylist.GetSize())
					{
						olOut += MakeCedaString(polGridProp->omMandatorylist[k]);
					}
					else
					{
						olOut += MakeCedaString(polGridProp->omMandatorylist[k]) + CString(";");
					}
				}
				olOut += CString(",");
				for( k = 0; k < polGridProp->omHeadersizes.GetSize(); k++)
				{
					if((k+1) == polGridProp->omHeadersizes.GetSize())
					{
						olOut += MakeCedaString(polGridProp->omHeadersizes[k]);
					}
					else
					{
						olOut += MakeCedaString(polGridProp->omHeadersizes[k]) + CString(";");
					}
				}
				olOut += CString(",");
			}
			else
			{
				CString CTL_TYPE = ropForms[i].omProperties[j].omCtrlType;
				olOut += MakeCedaString(ropForms[i].omProperties[j].omName) + CString(",");			//Name of Property
				olOut += MakeCedaString(ropForms[i].omProperties[j].omDBField) + CString(",");
				olText.Format("%d,", ropForms[i].omProperties[j].omRect.left);
				olOut += olText;
				olText.Format("%d,", ropForms[i].omProperties[j].omRect.top);
				olOut += olText;
				olText.Format("%d,", ropForms[i].omProperties[j].omRect.right);
				olOut += olText;
				olText.Format("%d,", ropForms[i].omProperties[j].omRect.bottom);
				olOut += olText;
	//			CString CTL_TYPE = ropForms[i].omProperties[j].omCtrlType;
				olOut += MakeCedaString(ropForms[i].omProperties[j].omCtrlType) + CString(",");		// Control type ==> not relevant for a form
				olOut += MakeCedaString(ropForms[i].omProperties[j].omLabel) + CString(",");
				olOut += MakeCedaString(ropForms[i].omProperties[j].omOrientation) + CString(",");
				olText.Format("%ld,", ropForms[i].omProperties[j].omBackColor);
				olOut += olText;
				olText.Format("%ld,", ropForms[i].omProperties[j].omFontColor);
				olOut += olText;
				olText.Format("%ld,", ropForms[i].omProperties[j].omBorderColor	);
				olOut += olText;
				olOut += BOOL_ToString(ropForms[i].omProperties[j].bmEnabled) + CString(",");
				olOut += BOOL_ToString(ropForms[i].omProperties[j].bmVisible) + CString(",");
				olText.Format("%d,", ropForms[i].omProperties[j].imBorderWidth);
				olOut += olText;   
				olOut += BOOL_ToString(ropForms[i].omProperties[j].bmDBField) + CString(",");		
				olOut += MakeCedaString(ropForms[i].omProperties[j].omFieldType) + CString(",");
				olOut += MakeCedaString(ropForms[i].omProperties[j].omFormatString) + CString(",");
	//XXX
				olText.Format("%d,", ropForms[i].omProperties[j].imTextLimit);
				olOut += olText;
				olOut += BOOL_ToString(ropForms[i].omProperties[j].bmMandatory) + CString(",");
				olOut += BOOL_ToString(ropForms[i].omProperties[j].bmMultiLine) + CString(",");
				olOut += MakeCedaString(ropForms[i].omProperties[j].omMasterName) + CString(",");
				for(int k = 0; k < ropForms[i].omProperties[j].omDefaultValues.GetSize(); k++)
				{
					if((k+1) == ropForms[i].omProperties[j].omDefaultValues.GetSize())
					{
						olOut += MakeCedaString(ropForms[i].omProperties[j].omDefaultValues[k]);
					}
					else
					{
						olOut += MakeCedaString(ropForms[i].omProperties[j].omDefaultValues[k]) + CString("|");
					}
				}
				olOut += CString(",");
				olOut += MakeCedaString(ropForms[i].omProperties[j].omCheckedValue);
				olOut += CString(",");
				olOut += BOOL_ToString(ropForms[i].omProperties[j].bmIsMaster);
				olOut += CString(",");
				olOut += MakeCedaString(ropForms[i].omProperties[j].Refe);
				olOut += CString(",");
				olOut += MakeCedaString(ropForms[i].omProperties[j].HiddenRefe);

				/*
				 * <<<cla:
				 * Code added for step 2 of the Workbench project
				 * VB Methods will be saved to stream
				 * Pairs of DISPID,VBMethodName
				 */

				olOut += CString(",");
				for(int indx=0;indx<ropForms[i].omProperties[j].omEventList.size();indx++)
				{	
					olText.Format("%d",ropForms[i].omProperties[j].omEventList[indx].m_dispID);
					olOut += olText;
					olOut += CString("+");
					olOut += MakeCedaString(ropForms[i].omProperties[j].omEventList[indx].m_eventExt);
					olOut += CString("+");
					olOut += MakeCedaString(ropForms[i].omProperties[j].omEventList[indx].m_vbMethodName);
					olOut += CString("~");
				}
				olOut += CString("0++~");
	//XXX
			}
			olOut += CString("#");

		}
		olOut += "\n";
//MMMMM
		olFile.Write(olOut.GetBuffer(0), olOut.GetLength());
		olOut = "";
//MMMMMM
	}//End for( i = 0; i < ropForms.GetSize(); i++)
	olOut+="}\n";

//--- End Forms
	olFile.Write(olOut.GetBuffer(0), olOut.GetLength());
	olOut = "";

	olFile.Close();
	}
	CATCH( CFileException, e )
	{   
#ifdef _DEBUG
		  afxDump << "File could not be opened " << e->m_cause << "\r\n";   
#endif
	}
	END_CATCH
		
}//End SaveConfigAs

void CUWorkBenchApp::ReadDataFromCfg()
{
	CString olFileName;
	olFileName = ogPseudoAppName + ".cfg";

	ReadAnyCfgFile(olFileName, ogLoadSets, ogGridDefs, ogForms, ogQueries);
}
//--------------------------------------------------------------------------------
// MWO:
// To read into local copies of the loadsets etc.
// This is a special read function!!
//--------------------------------------------------------------------------------
void CUWorkBenchApp::ReadAnyCfgFile(CString opFileName,
									CCSPtrArray<UDBLoadSet> &ropLoadSets,
									CCSPtrArray<UGridDefinition> &ropGridDefs,
									CCSPtrArray<UFormProperties> &ropForms,
									CCSPtrArray<UQuery> &ropQueries)
{
	TRY	
	{
		char *pclText;
		CString olText;
		CFile olFile(opFileName.GetBuffer(0) , CFile::modeRead );
		pclText = (char *)malloc((int)((olFile.GetLength())*sizeof(char))+1);
	//	olFile.Open(".\\WorkBench.cfg", CFile::modeCreate|CFile::modeWrite );
		olFile.Read(pclText, (UINT)olFile.GetLength());
		olText = CString(pclText);

		// Which Version shall be Prepared
		// There are read routines for each Version
		if(olText.Find("VERSION[1.0]") != -1)
		{
			PrepareCfgDataV10(pclText, ropLoadSets, ropGridDefs, ropForms, ropQueries);
		}
		
		int ilLS = ropLoadSets.GetSize();
		int ilGD = ropGridDefs.GetSize();
		int ilF  = ropForms.GetSize();
		int ilQ =  ropQueries.GetSize();
		olFile.Close();
		free(pclText);
	}
	CATCH( CFileException, e )
	{   
#ifdef _DEBUG
		  afxDump << "File could not be opened " << e->m_cause << "\n";   
#endif
	}
	END_CATCH
}

//--------------------------------------------------------------------------------
// Extract VERSION 1.0
//--------------------------------------------------------------------------------
void CUWorkBenchApp::PrepareCfgDataV10(CString opText,
									   CCSPtrArray<UDBLoadSet> &ropLoadSets,
									   CCSPtrArray<UGridDefinition> &ropGridDefs,
									   CCSPtrArray<UFormProperties> &ropForms,
									   CCSPtrArray<UQuery> &ropQueries)
{
	CString olRest;
	int ilEndPos = -1;
	int i = 0, j = 0;
	int ilPos = opText.Find("[LOADSET]");
	if(ilPos != -1)
	{
		olRest = opText.Mid(ilPos+1, opText.GetLength()-(ilPos+1));
		ilPos = olRest.Find("{", 0);
		if(ilPos != -1)
		{
			ilPos+=2;
			olRest = opText.Mid(ilPos+1, opText.GetLength()-(ilPos+1));
			ilEndPos = olRest.Find("}");
			if(ilEndPos != -1)
			{
				olRest = olRest.Mid(0, ilEndPos);
			}
			else
			{
				olRest = CString("");
			}
			if(!olRest.IsEmpty())
			{
				CStringArray olSets;
				ExtractItemList(olRest, &olSets, '\n');
				for(i = 0; i < olSets.GetSize(); i++)
				{
					//name of the set
					CString olS = olSets[i];
					int ilColonPos = olSets[i].Find(":");
					if(ilColonPos != -1)
					{
						CString olSetName;
						olSetName = olSets[i].Left(ilColonPos);
						UDBLoadSet *polSet = new UDBLoadSet();
						polSet->Name = MakeClientString(olSetName);
						olRest = olSets[i].Mid(ilColonPos+1, olSets[i].GetLength() - (ilColonPos+1));
						if(!olRest.IsEmpty())
						{
							//Tables in the set
							CStringArray olTabInfos;
							ExtractItemList(olRest, &olTabInfos, '@');
							for( j = 0; j < olTabInfos.GetSize(); j++)
							{
								CString olD = olTabInfos[j];
								if(!olTabInfos[j].IsEmpty())
								{
									CStringArray olTabDetails;
									ExtractItemList(olTabInfos[j], &olTabDetails, ',');
									BDLoadTable *polTab = new BDLoadTable();
									for(int k = 0; k < olTabDetails.GetSize(); k++)
									{
										switch(k)
										{
										case 0://Tablename
											polTab->Name = MakeClientString(olTabDetails[k]);
											break;
										case 1://Sqlpart
											polTab->SqlPart = MakeClientString(olTabDetails[k]);
											break;
										case 2://is timeframeobject
											{
												if(MakeClientString(olTabDetails[k]) == "0")
												{
													polTab->IsTimeFrameObject = false;
												}
												else
												{
													polTab->IsTimeFrameObject = true;
												}
											}
											break;
										case 3://is Flightobject
											{
												if(MakeClientString(olTabDetails[k]) == "0")
												{
													polTab->IsAFTObject = false;
												}
												else
												{
													polTab->IsAFTObject = true;
												}
											}
											break;
										case 4://is Indirect loaded
											{
												if(MakeClientString(olTabDetails[k]) == "0")
												{
													polTab->IsIndirectLoad = false;
												}
												else
												{
													polTab->IsIndirectLoad = true;
												}
											}
											break;
										default:
											{
												if(!olTabDetails[k].IsEmpty())
												{
													CString olS = MakeClientString(olTabDetails[k]);
													polTab->FieldList.Add(olTabDetails[k]);
												}
											}
											break;
										}
									}
									polSet->TableArray.Add(polTab);
								}
							}//for( j = 0; j < olTabInfos.GetSize(); j++)
						}//if(!olRest.IsEmpty())
						ropLoadSets.Add(polSet);
					}//if(ilColonPos != -1)
				}//for(i = 0; i < olSets.GetSize(); i++)
			}//if(!olRest.IsEmpty())
		}// after find "{" if(ilPos != -1)
	}// after find [LOADSET] if(ilPos != -1)

	bool blFound = false;
	for( i = 0; i < ropLoadSets.GetSize(); i++)
	{
		if( ropLoadSets[i].Name == "Default System Set")
		{
			blFound = true;
			break;
		}
	}
	//ensure that a load set for "SYSTEM" exists, because this set will be used
	//by initial load; it's also used by all definition windows to update the setinfo
	//automatically
	if(blFound == false)
	{
		UDBLoadSet *polSet = new UDBLoadSet();
		polSet->Name = "Default System Set";
		ropLoadSets.Add(polSet);
	}

//[QUERIES]
//{
//Query 1:ACC,ACR,ACT,AFM,AFT,ALT:ACT.ACT5=ACR.ACT3,ACR.ACLE=ACC.AUBY:1,ACC,OP08,Asc,,,1#2,ACR,ACT5,Asc,,,1#3,ACT,ACT5,Asc,,,1#4,AFM,ANAM,Asc,,,1#5,AFT,ACT5,Asc,,,1#6,ALT,VATO,Asc,,,1#
//Query 2:DSR,GHD:GHD.EURN=DSR.URNO:1,DSR,ENM2,,,,#2,DSR,ENAM,,,,#3,GHD,POTA,,,,#
//}

	ilPos = opText.Find("[QUERIES]");
	if(ilPos != -1)
	{
		olRest = opText.Mid(ilPos+1, opText.GetLength()-(ilPos+1));
		ilPos = olRest.Find("{", 0);
		if(ilPos != -1) 
		{
			ilPos+=1;
			olRest = olRest.Mid(ilPos+1, olRest.GetLength()-(ilPos+1));
			ilEndPos = olRest.Find("}");
			if(ilEndPos != -1)
			{
				olRest = olRest.Mid(0, ilEndPos);
			}
			else
			{
				olRest = CString("");
			}
			if(!olRest.IsEmpty())
			{
				CStringArray olQueriesParts;
				ExtractItemList(olRest, &olQueriesParts, '\n');
				for( i = 0; i < olQueriesParts.GetSize(); i++)
				{
					CString olPart = olQueriesParts[i];
					if(!olPart.IsEmpty())
					{
						UQuery *polQuery = new UQuery();
						CStringArray olSections;
						ExtractItemList(olPart, &olSections, ':');
						for(j = 0; j < olSections.GetSize(); j++)
						{
							
							if(j == 0)//Name
							{
								polQuery->omName = MakeClientString(olSections[j]);
							}
							if(j == 1)//Tables
							{
								CStringArray olTabs;
								ExtractItemList(olSections[j], &olTabs, ',');
								for(int k = 0; k < olTabs.GetSize(); k++)
								{
									polQuery->omUsedTables.Add(MakeClientString(olTabs[k]));
								}
							}
							if(j == 2)//Relations
							{
								CStringArray olRels;
								ExtractItemList(olSections[j], &olRels, ',');
								for(int k = 0; k < olRels.GetSize(); k++)
								{
									polQuery->omObjRelation.Add(MakeClientString(olRels[k]));
								}
							}
							if(j == 3)//Detail
							{
								CStringArray olDets;
								ExtractItemList(olSections[j], &olDets, '#');
								for(int k = 0; k < olDets.GetSize(); k++)
								{
									CString olS = MakeClientString(olDets[k]);
									CStringArray olDetFields;
									ExtractItemList(olDets[k], &olDetFields, ',');
									if(olDetFields.GetSize() == 7)
									{
										UQueryDetail *polDetail = new UQueryDetail();
										polDetail->imColNo			= atoi(MakeClientString(olDetFields[0]));
										polDetail->Object			= MakeClientString(olDetFields[1]);
										polDetail->Field			= MakeClientString(olDetFields[2]);
										polDetail->Sort				= MakeClientString(olDetFields[3]);
										polDetail->Operator			= MakeClientString(olDetFields[4]);
										polDetail->Criteria			= MakeClientString(olDetFields[5]);
										polDetail->omShowAsResult	= MakeClientString(olDetFields[6]);
										polQuery->omQueryDetails.Add(polDetail);
									}
								}
							}
						}//for(j = 0; j < olSections.GetSize(); j++)
						ropQueries.Add(polQuery);
					}//if(!olPart.IsEmpty())
				}//for( i = 0; i < olQueriesParts.GetSize(); i++)
			}//if(!olRest.IsEmpty())
		}//if(ilPos != -1)
	}//if(ilPos != -1)

///****************************************
// Reading GRID - Definitions
	ilPos = opText.Find("[GRIDS]");
	if(ilPos != -1)
	{
		olRest = opText.Mid(ilPos+1, opText.GetLength()-(ilPos+1));
		ilPos = olRest.Find("{", 0);
		if(ilPos != -1) 
		{
			ilPos+=1;
			olRest = olRest.Mid(ilPos+1, olRest.GetLength()-(ilPos+1));
			ilEndPos = olRest.Find("}");
			if(ilEndPos != -1)
			{
				olRest = olRest.Mid(0, ilEndPos);
			}
			else
			{
				olRest = CString("");
			}
		}
		if(!olRest.IsEmpty())
		{
			CStringArray olGrids;
			ExtractItemList(olRest, &olGrids, '\n');
			for( i = 0; i < olGrids.GetSize(); i++)
			{
				CString olGrid = olGrids[i];
				if(!olGrid.IsEmpty())
				{
					CStringArray olSections;
					UGridDefinition *polGridDef = new UGridDefinition();
					ExtractItemList(olGrid, &olSections, '#');
					int ilSecs = olSections.GetSize();
					if(olSections.GetSize() == 5)
					{
						CStringArray olItems;
						if(!olSections[0].IsEmpty())
						{//Single Definitions
							ExtractItemList(olSections[0], &olItems, ',');
							int ilItems = olItems.GetSize();
							if(olItems.GetSize() == 28)
							{
								polGridDef->GridName			= MakeClientString(olItems[0]);
								polGridDef->AsBasicData			= StrToBOOL(MakeClientString(olItems[1]));
								polGridDef->FormName			= MakeClientString(olItems[2]);
								polGridDef->AsFrameWindow		= StrToBOOL(MakeClientString(olItems[3]));
								polGridDef->AsChoiceList		= StrToBOOL(MakeClientString(olItems[4]));
								polGridDef->DataSource			= MakeClientString(olItems[5]);
								polGridDef->Filter				= MakeClientString(olItems[6]);
								polGridDef->HasViewButton		= StrToBOOL(MakeClientString(olItems[7]));
								polGridDef->HeaderSize			= StrToBOOL(MakeClientString(olItems[8]));
								polGridDef->IsEditable			= StrToBOOL(MakeClientString(olItems[9]));
								polGridDef->MultiSelect			= StrToBOOL(MakeClientString(olItems[10]));
								polGridDef->Printable			= StrToBOOL(MakeClientString(olItems[11]));
								polGridDef->ReturnField			= MakeClientString(olItems[12]);
								polGridDef->SensitiveForChanges = StrToBOOL(MakeClientString(olItems[13]));
								polGridDef->SetDefaultTimeFrame = StrToBOOL(MakeClientString(olItems[14]));
								polGridDef->ShowTimeFrameDialog = StrToBOOL(MakeClientString(olItems[15]));
								polGridDef->Today				= atoi(MakeClientString(olItems[16]));
								polGridDef->TodayFrom			= MakeClientString(olItems[17]);
								polGridDef->TodayTo				= MakeClientString(olItems[18]);
								polGridDef->TomorrowFrom		= MakeClientString(olItems[19]);
								polGridDef->TomorrowTo			= MakeClientString(olItems[20]);
								polGridDef->left				= atoi(MakeClientString(olItems[21]));
								polGridDef->top					= atoi(MakeClientString(olItems[22]));
								polGridDef->right				= atoi(MakeClientString(olItems[23]));
								polGridDef->bottom				= atoi(MakeClientString(olItems[24]));
								polGridDef->omTableRelations	= MakeClientString(olItems[25]);
								polGridDef->omRowConnection		= MakeClientString(olItems[26]);
								polGridDef->omSortDirection		= MakeClientString(olItems[27]);
							}
						}//if(!olSections[0].IsEmpty())
						olItems.RemoveAll();
						if(!olSections[1].IsEmpty())
						{//Tables
							ExtractItemList(olSections[1], &olItems, ',');
							for(j = 0; j < olItems.GetSize(); j++)
							{
								polGridDef->omTables.Add(MakeClientString(olItems[j]));
							}
						}//if(!olSections[1].IsEmpty())
						olItems.RemoveAll();
						if(!olSections[2].IsEmpty())
						{//ColumnsWidths
							ExtractItemList(olSections[2], &olItems, ',');
							for(j = 0; j < olItems.GetSize(); j++)
							{
								polGridDef->omColWidths.Add(atoi(MakeClientString(olItems[j])));
							}
						}//if(!olSections[2].IsEmpty())
						olItems.RemoveAll();
						if(!olSections[3].IsEmpty())
						{//SortColumns
							ExtractItemList(olSections[3], &olItems, ',');
							for(j = 0; j < olItems.GetSize(); j++)
							{
								polGridDef->omSortColumns.Add(atoi(MakeClientString(olItems[j])));
							}
						}//if(!olSections[3].IsEmpty())
						if(!olSections[4].IsEmpty())
						{//GridFields
							CStringArray olFieldList;
							ExtractItemList(olSections[4], &olFieldList, '@');
							for(j = 0; j < olFieldList.GetSize(); j++)
							{
								olItems.RemoveAll();
								if(!olFieldList[j].IsEmpty())
								{
									ExtractItemList(olFieldList[j],&olItems, ',');
									if(olItems.GetSize() == 8)
									{
										UGridFields *polGridField = new UGridFields();
										polGridField->Table		= MakeClientString(olItems[0]);
										polGridField->Field		= MakeClientString(olItems[1]);
										polGridField->Type		= MakeClientString(olItems[2]);
										polGridField->Header	= MakeClientString(olItems[3]);
										polGridField->Visible	= MakeClientString(olItems[4]);
										polGridField->Key		= MakeClientString(olItems[5]);
										polGridField->Unique	= MakeClientString(olItems[6]);
										polGridField->QueryName = MakeClientString(olItems[7]);
										polGridDef->omGridFields.Add(polGridField);
									}
								}

							}
						}//if(!olSections[4].IsEmpty())

					}//if(olSections.GetSize() == 5)
					ropGridDefs.Add(polGridDef);
				}//if(!olGrid.IsEmpty())
				
			}//for( i = 0; i < olGrids.GetSize(); i++)
		}//if(!olRest.IsEmpty())
	}
// END GRID DEFINITIONS

//Reading Form-Definition
	ilPos = opText.Find("[FORMS]");
	if(ilPos != -1)
	{
		olRest = opText.Mid(ilPos+1, opText.GetLength()-(ilPos+1));
		ilPos = olRest.Find("{", 0);
		if(ilPos != -1) 
		{
			ilPos+=1;
			olRest = olRest.Mid(ilPos+1, olRest.GetLength()-(ilPos+1));
			ilEndPos = olRest.Find("}");
			if(ilEndPos != -1)
			{
				olRest = olRest.Mid(0, ilEndPos);
			}
			else
			{
				olRest = CString("");
			}
		}
		if(!olRest.IsEmpty())
		{
			CStringArray olForms;
			ExtractItemList(olRest, &olForms, '\n');
			for( i = 0; i < olForms.GetSize(); i++)
			{
				CString olForm = olForms[i];
				if(!olForm.IsEmpty())
				{
					CStringArray olSections;
					UFormProperties *polForm = new UFormProperties();
					ExtractItemList(olForm, &olSections, '#');
					int ilSecs = olSections.GetSize();
					for( int j = 0; j < ilSecs; j++)
					{
						if( j == 0 )//Form itself
						{
							CStringArray olFormSections;
							ExtractItemList(olSections[j], &olFormSections, '|');
							for( int k = 0; k < olFormSections.GetSize(); k++ )
							{
								if( k == 0) // Formproperties
								{
									CStringArray olFormProps;
									ExtractItemList(olFormSections[k], &olFormProps, ',');
 									for( int l = 0; l < olFormProps.GetSize(); l++ ) 
									{
										if( l == 0 )polForm->omName			= MakeClientString(olFormProps[l]);
										if( l == 1 )polForm->omRect.left	= atoi(MakeClientString(olFormProps[l]));
										if( l == 2 )polForm->omRect.top		= atoi(MakeClientString(olFormProps[l]));
										if( l == 3 )polForm->omRect.right	= atoi(MakeClientString(olFormProps[l]));
										if( l == 4 )polForm->omRect.bottom	= atoi(MakeClientString(olFormProps[l]));
										if( l == 5 )polForm->omTable		= MakeClientString(olFormProps[l]);
										if( l == 6 )polForm->omBorderType	= MakeClientString(olFormProps[l]);
										if( l == 7 )polForm->omWindowType	= MakeClientString(olFormProps[l]);
										if( l == 8 )polForm->imStaticCount	= atoi(MakeClientString(olFormProps[l]));	
										if( l == 9 )polForm->imEditCount	= atoi(MakeClientString(olFormProps[l]));	
										if( l == 10 )polForm->imComboboxCount= atoi(MakeClientString(olFormProps[l]));	
										if( l == 11 )polForm->imCheckboxCount= atoi(MakeClientString(olFormProps[l]));
										if( l == 12 )polForm->imRadioCount	= atoi(MakeClientString(olFormProps[l]));
										if( l == 13 )polForm->imListBoxCount	= atoi(MakeClientString(olFormProps[l]));	
										if( l == 14 )polForm->imButtonCount	= atoi(MakeClientString(olFormProps[l]));	
										if( l == 15 )polForm->imRectCount	= atoi(MakeClientString(olFormProps[l]));	
										if( l == 16 )polForm->imLineCount	= atoi(MakeClientString(olFormProps[l]));	
										if( l == 17 )polForm->omCtrlType	= MakeClientString(olFormProps[l]);
										if( l == 18 )polForm->omLabel		= MakeClientString(olFormProps[l]);
										if( l == 19 )polForm->omOrientation = MakeClientString(olFormProps[l]);
										if( l == 20 )polForm->omBackColor	= atol(MakeClientString(olFormProps[l]));
										if( l == 21 )polForm->omFontColor	= atol(MakeClientString(olFormProps[l]));
										if( l == 22 )polForm->omBorderColor = atol(MakeClientString(olFormProps[l]));
										if( l == 23 )polForm->bmEnabled		= StrToBOOL(MakeClientString(olFormProps[l]));
										if( l == 24 )polForm->bmVisible		= StrToBOOL(MakeClientString(olFormProps[l]));
										if( l == 25 )polForm->bmModal		= StrToBOOL(MakeClientString(olFormProps[l]));
										if( l == 26 )polForm->imBorderWidth = atoi(MakeClientString(olFormProps[l]));
										if( l == 27 )polForm->bmDBField		= StrToBOOL(MakeClientString(olFormProps[l]));
										if( l == 28 )polForm->omFieldType	= MakeClientString(olFormProps[l]);
										if( l == 29 )polForm->omFormatString= MakeClientString(olFormProps[l]);
									}
								}
								if( k == 1) // Tables
								{
									CStringArray olFormTabs;
									ExtractItemList(olFormSections[k], &olFormTabs, ',');
									for( int l = 0; l < olFormTabs.GetSize(); l++ ) 
									{
										polForm->omTables.Add(MakeClientString(olFormTabs[l]));
									}
								}
								if( k == 2) // DB-Fields
								{
									CStringArray olDBFields;
									ExtractItemList(olFormSections[k], &olDBFields, '@');
									for( int l = 0; l < olDBFields.GetSize(); l++ ) 
									{
										UFormField *polDBField = new UFormField();
										polForm->omDBFields.Add(polDBField);
										CStringArray olDBFieldsInfos;
										ExtractItemList(olDBFields[l], &olDBFieldsInfos, ',');
										for( int m = 0; m < olDBFieldsInfos.GetSize(); m++)
										{
											if( m == 0)polDBField->Table		= MakeClientString(olDBFieldsInfos[m]);
											if( m == 1)polDBField->Field		= MakeClientString(olDBFieldsInfos[m]);
											if( m == 2)polDBField->Type			= MakeClientString(olDBFieldsInfos[m]);
											if( m == 3)polDBField->Header		= MakeClientString(olDBFieldsInfos[m]);
											if( m == 4)polDBField->Visible		= MakeClientString(olDBFieldsInfos[m]);
											if( m == 5)polDBField->Key			= MakeClientString(olDBFieldsInfos[m]);
											if( m == 6)polDBField->Refe			= MakeClientString(olDBFieldsInfos[m]);
											if( m == 7)polDBField->HiddenRefe	= MakeClientString(olDBFieldsInfos[m]);											
										}
									}
								}
							}
						}
						else // Controlproperties
						{
							UCtrlProperties *polCtrl = NULL;// = new UCtrlProperties();
							CStringArray olCtrlProps;
							ExtractItemList(olSections[j], &olCtrlProps, ',');
							int ilCtrl_Type_IDX = 6;
							CString olTmpCtrlType;
							if(olCtrlProps.GetSize() > 5)
							{
								if(olCtrlProps[ilCtrl_Type_IDX] == "edit")
								{
									olTmpCtrlType = "edit";
									UEditCtrlProperties *polEdit;
									polEdit = new UEditCtrlProperties();
									polCtrl = (UCtrlProperties *)polEdit;
								}
								if(olCtrlProps[ilCtrl_Type_IDX] == "static")
								{
									olTmpCtrlType = "static";
									UStaticCtrlProperties *polStatic;
									polStatic = new UStaticCtrlProperties();
									polCtrl = (UCtrlProperties *)polStatic;
								}
								if(olCtrlProps[ilCtrl_Type_IDX] == "button")
								{
									olTmpCtrlType = "button";
									UButtonCtrlProperties *polButton;
									polButton = new UButtonCtrlProperties();
									polCtrl = (UCtrlProperties *)polButton;
								}
								if(olCtrlProps[ilCtrl_Type_IDX] == "group")
								{
									olTmpCtrlType = "button";
									UGroupCtrlProperties *polGroup;
									polGroup = new UGroupCtrlProperties();
									polCtrl = (UCtrlProperties *)polGroup;
								}
								if(olCtrlProps[ilCtrl_Type_IDX] == "radio")
								{
									olTmpCtrlType = "radio";
									URadioCtrlProperties *polRadio;
									polRadio = new URadioCtrlProperties();
									polCtrl = (UCtrlProperties *)polRadio;
								}
								if(olCtrlProps[ilCtrl_Type_IDX] == "combobox")
								{
									olTmpCtrlType = "combobox";
									UComboboxCtrlProperties *polCombo;
									polCombo = new UComboboxCtrlProperties();
									polCtrl = (UCtrlProperties *)polCombo;
								}
								if(olCtrlProps[ilCtrl_Type_IDX] == "checkbox")
								{
									olTmpCtrlType = "checkbox";
									UCheckboxCtrlProperties *polCheck;
									polCheck = new UCheckboxCtrlProperties();
									polCtrl = (UCtrlProperties *)polCheck;
								}
								if(olCtrlProps[ilCtrl_Type_IDX] == "grid")
								{
									olTmpCtrlType = "grid";
									UGridCtrlProperties *polCheck;
									polCheck = new UGridCtrlProperties();
									polCtrl = (UCtrlProperties *)polCheck;
								}
								if(olCtrlProps[ilCtrl_Type_IDX] == "line")
								{
									olTmpCtrlType = "line";
									ULineCtrlProperties *polLine;
									polLine = new ULineCtrlProperties();
									polCtrl = (UCtrlProperties *)polLine;
								}
								if(olCtrlProps[ilCtrl_Type_IDX] == "rectangle")
								{
									olTmpCtrlType = "rectangle";
									URectCtrlProperties *polRect;
									polRect = new URectCtrlProperties();
									polCtrl = (UCtrlProperties *)polRect;
								}
							}
							if(polCtrl != NULL)
							{
								for( int k = 0; k < olCtrlProps.GetSize(); k++ ) 
								{
									if(olTmpCtrlType == "grid")
									{
										UGridCtrlProperties *polGrid		= (UGridCtrlProperties *)polCtrl;
										if( k == 0  )polGrid->omName		= MakeClientString(olCtrlProps[k]);
										if( k == 1  )polGrid->omRect.left	= atoi(MakeClientString(olCtrlProps[k]));
										if( k == 2  )polGrid->omRect.top	= atoi(MakeClientString(olCtrlProps[k]));
										if( k == 3  )polGrid->omRect.right	= atoi(MakeClientString(olCtrlProps[k]));
										if( k == 4  )polGrid->omRect.bottom = atoi(MakeClientString(olCtrlProps[k]));
										if( k == 5  )polGrid->bmEnabled		= StrToBOOL(MakeClientString(olCtrlProps[k]));
										if( k == 6  )polGrid->omCtrlType	= MakeClientString(olCtrlProps[k]);
										if( k == 7  )polGrid->bmVisible		= StrToBOOL(MakeClientString(olCtrlProps[k]));
										if( k == 8  )polGrid->omGridTable		= MakeClientString(olCtrlProps[k]);
										if( k == 9  )polGrid->omMasterDetail= MakeClientString(olCtrlProps[k]);
										if( k == 10 )
										{
											CStringArray olList;
											ExtractItemList(olCtrlProps[k], &olList, ';');
											for(int l = 0; l < olList.GetSize(); l++)
											{
												polGrid->omFieldlist.Add(MakeClientString(olList[l]));
											}
										}
										if( k == 11 )
										{
											CStringArray olList;
											ExtractItemList(olCtrlProps[k], &olList, ';');
											for(int l = 0; l < olList.GetSize(); l++)
											{
												polGrid->omTypelist.Add(MakeClientString(olList[l]));
											}
										}
										if( k == 12 )
										{
											CStringArray olList;
											ExtractItemList(olCtrlProps[k], &olList, ';');
											for(int l = 0; l < olList.GetSize(); l++)
											{
												polGrid->omHaederlist.Add(MakeClientString(olList[l]));
											}
										}
										if( k == 13 )
										{
											CStringArray olList;
											ExtractItemList(olCtrlProps[k], &olList, ';');
											for(int l = 0; l < olList.GetSize(); l++)
											{
												polGrid->omVisiblelist.Add(MakeClientString(olList[l]));
											}
										}
										if( k == 14 )
										{
											CStringArray olList;
											ExtractItemList(olCtrlProps[k], &olList, ';');
											for(int l = 0; l < olList.GetSize(); l++)
											{
												polGrid->omLookupfieldList.Add(MakeClientString(olList[l]));
											}
										}
										if( k == 15 )
										{
											CStringArray olList;
											ExtractItemList(olCtrlProps[k], &olList, ';');
											for(int l = 0; l < olList.GetSize(); l++)
											{
												polGrid->omMandatorylist.Add(MakeClientString(olList[l]));
											}
										}
										if( k == 16 )
										{
											CStringArray olList;
											ExtractItemList(olCtrlProps[k], &olList, ';');
											for(int l = 0; l < olList.GetSize(); l++)
											{
												polGrid->omHeadersizes.Add(MakeClientString(olList[l]));
											}
										}
									}
									else
									{
										if( k == 0  )polCtrl->omName		= MakeClientString(olCtrlProps[k]);
										if( k == 1  )polCtrl->omDBField		= MakeClientString(olCtrlProps[k]);
										if( k == 2  )polCtrl->omRect.left	= atoi(MakeClientString(olCtrlProps[k]));
										if( k == 3  )polCtrl->omRect.top	= atoi(MakeClientString(olCtrlProps[k]));
										if( k == 4  )polCtrl->omRect.right	= atoi(MakeClientString(olCtrlProps[k]));
										if( k == 5  )polCtrl->omRect.bottom = atoi(MakeClientString(olCtrlProps[k]));
										if( k == 6  )polCtrl->omCtrlType	= MakeClientString(olCtrlProps[k]);
										if( k == 7  )polCtrl->omLabel		= MakeClientString(olCtrlProps[k]);
										if( k == 8  )polCtrl->omOrientation = MakeClientString(olCtrlProps[k]);
										if( k == 9  )polCtrl->omBackColor	= atol(MakeClientString(olCtrlProps[k]));
										if( k == 10 )polCtrl->omFontColor	= atol(MakeClientString(olCtrlProps[k]));
										if( k == 11 )polCtrl->omBorderColor = atol(MakeClientString(olCtrlProps[k]));
										if( k == 12 )polCtrl->bmEnabled		= StrToBOOL(MakeClientString(olCtrlProps[k]));
										if( k == 13 )polCtrl->bmVisible		= StrToBOOL(MakeClientString(olCtrlProps[k]));
										if( k == 14 )polCtrl->imBorderWidth = atoi(MakeClientString(olCtrlProps[k]));
										if( k == 15 )polCtrl->bmDBField		= StrToBOOL(MakeClientString(olCtrlProps[k]));
										if( k == 16 )polCtrl->omFieldType	= MakeClientString(olCtrlProps[k]);
										if( k == 17 )polCtrl->omFormatString= MakeClientString(olCtrlProps[k]);
										if( k == 18 )polCtrl->imTextLimit	= atoi(olCtrlProps[k]);
										if( k == 19 )polCtrl->bmMandatory	= StrToBOOL(MakeClientString(olCtrlProps[k]));
										if( k == 20 )polCtrl->bmMultiLine	= StrToBOOL(MakeClientString(olCtrlProps[k]));
										if( k == 21 )polCtrl->omMasterName	= MakeClientString(olCtrlProps[k]);
										if( k == 22 )
										{
											CStringArray olDefVals;
											ExtractItemList(olCtrlProps[k], &olDefVals, '|');
											for(int l = 0; l < olDefVals.GetSize(); l++)
											{
												polCtrl->omDefaultValues.Add(MakeClientString(olDefVals[l]));
											}
										}
										if( k == 23 )polCtrl->omCheckedValue = MakeClientString(olCtrlProps[k]); 
										if( k == 24 )polCtrl->bmIsMaster	 = StrToBOOL(MakeClientString(olCtrlProps[k]));
										if( k == 25 )polCtrl->Refe			 = MakeClientString(olCtrlProps[k]);
										if( k == 26 )
										{
											polCtrl->HiddenRefe			 = MakeClientString(olCtrlProps[k]);
										}
//										TRACE( "Refe: %s, HiddenRefe: %s\n",polCtrl->Refe,polCtrl->HiddenRefe);
										/*
										 * <<<cla:
										 * Looking for Event entries
										 */
										if( k == 27 ) // This is the comma separator
										{
											CStringArray olEventVals; //separated single event by '+'
											CStringArray olEventListVals; //separated event list by '~'
											CCntlEvent olEvent;
											int indx,indx2;
											ExtractItemList(olCtrlProps[k],&olEventListVals, '~');

											
											for(indx = 0; indx<olEventListVals.GetSize(); indx++)
											{
												indx2 = 0;
												if((olEventListVals[indx]) == _T("0++"))
													break;
												
												ExtractItemList(olEventListVals[indx], &olEventVals, '+');
												olEvent.m_dispID = atoi(olEventVals[indx2]);
												indx2++;
												olEvent.m_eventExt = MakeClientString(olEventVals[indx2]);
												indx2++;
												olEvent.m_vbMethodName = MakeClientString(olEventVals[indx2]);
												polCtrl->omEventList.push_back(olEvent);
											}
										}
											
									}
								}
								polForm->omProperties.Add(polCtrl);
							}
						}
					}
					ropForms.Add(polForm);
				}
			}
		}
	}

}//End PrepareCfgData


//--------------------------------------------------------------------------------
// Extract VERSION 1.1
//--------------------------------------------------------------------------------
void CUWorkBenchApp::PrepareCfgDataV11(CString opText,
									   CCSPtrArray<UDBLoadSet> &ropLoadSets,
									   CCSPtrArray<UGridDefinition> &ropGridDefs,
									   CCSPtrArray<UFormProperties> &ropForms,
									   CCSPtrArray<UQuery> &ropQueries)
{
	// For the next Version of .cfg-File
}


void CUWorkBenchApp::InitSetup()
{
	//Timeframe setup
	CTime olTime = CTime::GetCurrentTime();

	char pclConfigPath[256];

    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	char plcLoadOffset[20];
	char pclLoadDuration[20];
	char pclCurrentDate[30];
	int  ilLoadOffset;
	int  ilLoadDuration;
	CString olCurrentDate;

    GetPrivateProfileString("UWorkBench", "LOADOFFSET", "", plcLoadOffset, sizeof plcLoadOffset, pclConfigPath);
    GetPrivateProfileString("UWorkBench", "LOADDURATION", "", pclLoadDuration, sizeof pclLoadDuration, pclConfigPath);
    GetPrivateProfileString("UWorkBench", "CURRENTDATE", "", pclCurrentDate, sizeof pclCurrentDate, pclConfigPath);

	ilLoadOffset = atoi(plcLoadOffset);
	ilLoadDuration = atoi(pclLoadDuration);
	olCurrentDate = pclCurrentDate;
	if (*pclCurrentDate != '\0')
	{

		SYSTEMTIME olTmpTime;

		GetSystemTime(&ogSysTime);
		GetSystemTime(&olTmpTime);
		olTmpTime.wYear = atoi(&pclCurrentDate[6]);
		olTmpTime.wMonth = atoi(&pclCurrentDate[3]);
		olTmpTime.wDay = atoi(&pclCurrentDate[0]);
	
		SetSystemTime(&olTmpTime);
		olTime = CTime::GetCurrentTime();
	}
	ogTimeFrameFrom = CTime(olTime.GetYear(), olTime.GetMonth(), olTime.GetDay(), 0, 0, 0);
	ogTimeFrameTo   = CTime(olTime.GetYear(), olTime.GetMonth(), olTime.GetDay(), 23, 59, 0);
	if(*plcLoadOffset != '\0' && *pclLoadDuration != '\0')
	{
		olTime = CTime::GetCurrentTime();
		if(ilLoadOffset < 0)
		{
			ogTimeFrameFrom = ogTimeFrameFrom - CTimeSpan(abs(ilLoadOffset), 0, 0, 0);
		}
		else
		{
			ogTimeFrameFrom = ogTimeFrameFrom - CTimeSpan(ilLoadOffset, 0, 0, 0);
		}
		
		ogTimeFrameTo = ogTimeFrameTo + CTimeSpan(ilLoadDuration, 0, 0, 0);
		CString olF = ogTimeFrameFrom.Format("%d.%m.%Y-%H:%M"); 
		CString olT = ogTimeFrameTo.Format("%d.%m.%Y-%H:%M"); 
		int o =0;o++;
	}

} 

CString CUWorkBenchApp::MakeCedaString(CString opText)
{
	CString omClientChars("\042\047\54\002\015\072");  // 34,39,44,10,13,58
						    //  {   }   [   ]   |   ^   ?  $  (  )  ;  @
	omClientChars += CString("\173\175\133\135\174\136\77\44\50\51\73\100");
	CString omServerChars("\260\261\262\264\263\277");  // 176,177,178,180,179,191
							//  {   }   [   ]   |   ^   ?   $   (   )   ;   @
	omServerChars += CString("\223\224\225\226\227\230\231\232\233\234\235\236");
	CString olRet = opText;
	int ilIndex;

	for (int ilLc = 0; ilLc < olRet.GetLength(); ilLc++)
	{
		if ((ilIndex = omClientChars.Find(olRet.GetAt(ilLc))) > -1)
		{
			olRet.SetAt(ilLc,omServerChars[ilIndex]);
		}
	}
	return olRet;
}
CString CUWorkBenchApp::MakeClientString(CString opText)
{
	CString omClientChars("\042\047\54\002\015\072");  // 34,39,44,10,13,58
						    //  {   }   [   ]   |   ^   ?  $  (  )  ;  @
	omClientChars += CString("\173\175\133\135\174\136\77\44\50\51\73\100");
	CString omServerChars("\260\261\262\264\263\277");  // 176,177,178,180,179,191
							//  {   }   [   ]   |   ^   ?   $   (   )   ;   @
	omServerChars += CString("\223\224\225\226\227\230\231\232\233\234\235\236");
	CString olRet = opText;
	for(int i = 0; i < omServerChars.GetLength(); i++)
	{
		olRet.Replace(omServerChars[i], omClientChars[i]);
	}
	return olRet;

}
