#if !defined(AFX_UDBLOADSETDEFINITIONDLG_H__CDDEF22D_31E7_11D3_B081_00001C019205__INCLUDED_)
#define AFX_UDBLOADSETDEFINITIONDLG_H__CDDEF22D_31E7_11D3_B081_00001C019205__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UDBLoadSetDefinitionDlg.h : header file
//

#include "UObjectDataDefinitions.h"
/////////////////////////////////////////////////////////////////////////////
// UDBLoadSetDefinitionDlg dialog

class UDBLoadSetDefinitionDlg : public CDialog
{
// Construction
public:
	UDBLoadSetDefinitionDlg(CWnd* pParent, UDBLoadSet *popLoadSet);   // standard constructor
	void MakeFieldDiffList(BDLoadTable *popLoadTab);
	UDBLoadSet *pomLoadSet;
// Dialog Data
	//{{AFX_DATA(UDBLoadSetDefinitionDlg)
	enum { IDD = IDD_DB_LOADSET };
	CListBox	m_ReloadList;
	CComboBox	m_SqlFunctions;
	CButton	m_IndirectLoad;
	CButton	m_FlightObject;
	CButton	m_TimeFrame;
	CEdit	m_SQL_Part;
	CStatic	m_SetName;
	CListBox	m_SourceTables;
	CListBox	m_SorceFields;
	CListBox	m_DestTableList;
	CListBox	m_DestFields;
	//}}AFX_DATA


	BDLoadTable *GetCurrentTabLoad(CString opTab);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(UDBLoadSetDefinitionDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(UDBLoadSetDefinitionDlg)
	virtual void OnOK();
	afx_msg void OnAddfield();
	afx_msg void OnAddtable();
	afx_msg void OnRemovefield();
	afx_msg void OnRemovetable();
	afx_msg void OnSelchangeSourceFieldList();
	afx_msg void OnSelchangeSourceTableList();
	afx_msg void OnSelchangeDestFieldList();
	afx_msg void OnSelchangeDestTableList();
	virtual BOOL OnInitDialog();
	afx_msg void OnKillfocusSqlPart();
	afx_msg void OnAftobject();
	afx_msg void OnTimeframe();
	afx_msg void OnSelchangeSqlFunctions();
	afx_msg void OnOrBtn();
	afx_msg void OnLessBtn();
	afx_msg void OnGtBtn();
	afx_msg void OnEquBtn();
	afx_msg void OnIndirectload();
	afx_msg void OnAndBtn();
	afx_msg void OnReload();
	afx_msg void OnReloadTable();
	afx_msg void OnUpdateLoadset();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UDBLOADSETDEFINITIONDLG_H__CDDEF22D_31E7_11D3_B081_00001C019205__INCLUDED_)
