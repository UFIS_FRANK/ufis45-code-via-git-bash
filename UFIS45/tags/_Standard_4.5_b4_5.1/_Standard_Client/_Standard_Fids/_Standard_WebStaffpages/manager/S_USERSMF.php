<?php
/*********************************************************************************
 *       Filename: S_USERSMF.php
 *       Generated with CodeCharge 1.1.16
 *       PHP & Templates build 03/28/2001
 *********************************************************************************/

include ("./common.php");
include ("./Admin.php");

session_start();

$filename = "S_USERSMF.php";

check_security(3);

$tpl = new Template($app_path);
$tpl->load_file("S_USERSMF.html", "main");
$tpl->load_file($header_filename, "Header");

$tpl->set_var("FileName", $filename);


$sS_FLDS_MENUS_USERSRecordErr = "";

$sAction = get_param("FormAction");
$sForm = get_param("FormName");
switch ($sForm) {
  case "S_FLDS_MENUS_USERSRecord":
    S_FLDS_MENUS_USERSRecord_action($sAction);
  break;
}Administration_show();
Search_show();
S_USERS_show();
S_FLDS_MENUS_USERSGrid_show();
S_FLDS_MENUS_USERSRecord_show();

$tpl->parse("Header", false);

$tpl->pparse("main", false);

//********************************************************************************


function Search_show()
{
  global $db;
  global $tpl;
  
  $tpl->set_var("ActionPage", "S_USERSMF.php");
	
  // Set variables with search parameters
  $flds_USERNAME = strip(get_param("s_USERNAME"));
    // Show fields
    $tpl->set_var("s_USERNAME", tohtml($flds_USERNAME));
  $tpl->parse("FormSearch", false);
}

function S_USERS_show()
{
  global $tpl;
  global $db;
  global $sS_USERSErr;
  $sWhere = "";
  $sOrder = "";
  $sSQL = "";
  $HasParam = false;
  
  $tpl->set_var("TransitParams", "s_USERNAME=" . tourl(strip(get_param("s_USERNAME"))) . "&");
  $tpl->set_var("FormParams", "s_USERNAME=" . tourl(strip(get_param("s_USERNAME"))) . "&");
  // Build WHERE statement
  
  $ps_USERNAME = get_param("s_USERNAME");
  if(strlen($ps_USERNAME)) 
  {
    $HasParam = true;
    // 20060717 JIM: PRF 8207: case insensitive
    $sWhere .= "upper(S.USERNAME) like " . tosql("%".strtoupper($ps_USERNAME) ."%", "Text");
  }
  if($HasParam)
    $sWhere = " WHERE (" . $sWhere . ")";

  $sDirection = "";
  $sSortParams = "";
  
  // Build ORDER statement
  $sOrder = " order by S.USERNAME Asc";
  $iSort = get_param("FormS_USERS_Sorting");
  $iSorted = get_param("FormS_USERS_Sorted");
  if(!$iSort)
  {
    $tpl->set_var("Form_Sorting", "");
  }
  else
  {
    if($iSort == $iSorted)
    {
      $tpl->set_var("Form_Sorting", "");
      $sDirection = " DESC";
      $sSortParams = "FormS_USERS_Sorting=" . $iSort . "&FormS_USERS_Sorted=" . $iSort . "&";
    }
    else
    {
      $tpl->set_var("Form_Sorting", $iSort);
      $sDirection = " ASC";
      $sSortParams = "FormS_USERS_Sorting=" . $iSort . "&FormS_USERS_Sorted=" . "&";
    }
    
    if ($iSort == 1) $sOrder = " order by S.USERNAME" . $sDirection;
  }

  // Build full SQL statement
  
  $sSQL = "select S.USERID as S_USERID, " . 
    "S.USERNAME as S_USERNAME " . 
    " from S_USERS S ";
  
  $sSQL .= $sWhere . $sOrder;
  $tpl->set_var("SortParams", $sSortParams);

  // Execute SQL statement
  $db->query($sSQL);
  
  // Select current page
  $iPage = get_param("FormS_USERS_Page");
  if(!strlen($iPage)) $iPage = 1;
  $RecordsPerPage = 10;
  if(($iPage - 1) * $RecordsPerPage != 0)
    $db->seek(($iPage - 1) * $RecordsPerPage);
  $iCounter = 0;

  if($db->next_record())
  {  
    // Show main table based on SQL query
    do
    {
      $fldUSERID = $db->f("S_USERID");
      $fldUSERNAME = $db->f("S_USERNAME");
    	$tpl->set_var("USERID", tohtml($fldUSERID));
      $tpl->set_var("USERNAME", tohtml($fldUSERNAME));
      $tpl->set_var("USERNAME_URLLink", "S_USERSMF.php");
      $tpl->set_var("Prm_USERID", tourl($db->f("S_USERID"))); 
      $tpl->parse("DListS_USERS", true);
      $iCounter++;
    } while($iCounter < $RecordsPerPage &&$db->next_record());
  }
  else
  {
    // No Records in DB
    $tpl->set_var("DListS_USERS", "");
    $tpl->parse("S_USERSNoRecords", false);
    $tpl->set_var("S_USERSScroller", "");
    $tpl->parse("FormS_USERS", false);
    return;
  }
  
  // Parse scroller
  if(@$db->next_record())
  {
    if ($iPage == 1)
    {
      $tpl->set_var("S_USERSScrollerPrevSwitch", "_");
    }
    else
    {
      $tpl->set_var("PrevPage", ($iPage - 1));
      $tpl->set_var("S_USERSScrollerPrevSwitch", "");
    }
    $tpl->set_var("NextPage", ($iPage + 1));
    $tpl->set_var("S_USERSScrollerNextSwitch", "");
    $tpl->set_var("S_USERSCurrentPage", $iPage);
    $tpl->parse("S_USERSScroller", false);
  }
  else
  {
    if ($iPage == 1)
    {
      $tpl->set_var("S_USERSScroller", "");
    }
    else
    {
      $tpl->set_var("S_USERSScrollerNextSwitch", "_");
      $tpl->set_var("PrevPage", ($iPage - 1));
      $tpl->set_var("S_USERSScrollerPrevSwitch", "");
      $tpl->set_var("S_USERSCurrentPage", $iPage);
      $tpl->parse("S_USERSScroller", false);
    }
  }
  $tpl->set_var("S_USERSNoRecords", "");
  $tpl->parse("FormS_USERS", false);
  
}

// Menu & Field For The Selected User
function S_FLDS_MENUS_USERSGrid_show()
{
  global $tpl;
  global $db;
  global $sS_FLDS_MENUS_USERSGridErr;
  $sWhere = "";
  $sOrder = "";
  $sSQL = "";
  $HasParam = false;
  
  $tpl->set_var("TransitParams", "USERID=" . tourl(strip(get_param("USERID"))) . "&");
  $tpl->set_var("FormParams", "USERID=" . tourl(strip(get_param("USERID"))) . "&");
  $bReq = true;
  // Build WHERE statement
  
  $pUSERID = get_param("USERID");
  if(is_number($pUSERID) && strlen($pUSERID))
    $pUSERID = round($pUSERID);
  else 
    $pUSERID = "";
  if(strlen($pUSERID)) 
  {
    $HasParam = true;
    $sWhere .= "S.USERID=" . $pUSERID;
  }
  else
    $bReq = false;
  if($HasParam)
    $sWhere = " AND (" . $sWhere . ")";

  $sDirection = "";
  $sSortParams = "";
  
  // Build ORDER statement
  $iSort = get_param("FormS_FLDS_MENUS_USERSGrid_Sorting");
  $iSorted = get_param("FormS_FLDS_MENUS_USERSGrid_Sorted");
  if(!$iSort)
  {
    $tpl->set_var("Form_Sorting", "");
    $sOrder = " order by S1.MENUNAME,S2.FLDNAME ASC";
  }
  else
  {
    if($iSort == $iSorted)
    {
      $tpl->set_var("Form_Sorting", "");
      $sDirection = " DESC";
      $sSortParams = "FormS_FLDS_MENUS_USERSGrid_Sorting=" . $iSort . "&FormS_FLDS_MENUS_USERSGrid_Sorted=" . $iSort . "&";
    }
    else
    {
      $tpl->set_var("Form_Sorting", $iSort);
      $sDirection = " ASC";
      $sSortParams = "FormS_FLDS_MENUS_USERSGrid_Sorting=" . $iSort . "&FormS_FLDS_MENUS_USERSGrid_Sorted=" . "&";
    }
    
    if ($iSort == 1) $sOrder = " order by S.ID" . $sDirection;
    if ($iSort == 2) $sOrder = " order by S1.MENUNAME" . $sDirection;
    if ($iSort == 3) $sOrder = " order by S2.FLDNAME" . $sDirection;
    if ($iSort == 4) $sOrder = " order by S.CSS_CLASSNAME" . $sDirection;
  }

  // Build full SQL statement
  
  $sSQL = "select S.FLD_ID as S_FLD_ID, " . 
    "S.ID as S_ID, " . 
    "S.TMPL_MENUID as S_TMPL_MENUID, " . 
    "S.USERID as S_USERID, " . 
    "S.CSS_CLASSNAME as S_STYLE, " . 
    "S.ROWNUMBER as ROWNUMBER, " . 
    "S.FLDFORMAT as FLDFORMAT, " . 
    "S1.TMPL_MENUID as S1_TMPL_MENUID, " . 
    "S1.MENUNAME as S1_MENUNAME, " . 
    "S2.FLDID as S2_FLDID, " . 
    "S2.FLDNAME as S2_FLDNAME " . 
    //"S2.CSS_CLASSNAME as S2_STYLE " . 
    " from S_TMPL_FLD S, S_TMPL_MENUS S1, S_FLDS S2" . 
    " where S1.TMPL_MENUID=S.TMPL_MENUID and S2.FLDID=S.FLD_ID  ";
  
  $sSQL .= $sWhere . $sOrder;
  $tpl->set_var("FormAction", "S_USERSMF.php");
  $tpl->set_var("SortParams", $sSortParams);
  if(!$bReq)
  {
    $tpl->set_var("DListS_FLDS_MENUS_USERSGrid", "");
    $tpl->parse("S_FLDS_MENUS_USERSGridNoRecords", false);
    $tpl->set_var("S_FLDS_MENUS_USERSGridScroller", "");
    $tpl->parse("FormS_FLDS_MENUS_USERSGrid", false);
    return;
  }

  // Execute SQL statement
  $db->query($sSQL);
  
  // Select current page
  $iPage = get_param("FormS_FLDS_MENUS_USERSGrid_Page");
  if(!strlen($iPage)) $iPage = 1;
  $RecordsPerPage = 10;
  if(($iPage - 1) * $RecordsPerPage != 0)
    $db->seek(($iPage - 1) * $RecordsPerPage);
  $iCounter = 0;

  if($db->next_record())
  {  
    // Show main table based on SQL query
    do
    {
      $fldID = $db->f("S_ID");
      $fldTMPL_MENUID = $db->f("S1_MENUNAME");
      $fldFLD_ID = $db->f("S2_FLDNAME");
      $fldCSS_CLASSNAME = $db->f("S_STYLE");
      $fldROWNUMBER = $db->f("ROWNUMBER");
      $fldFLDFORMAT = $db->f("FLDFORMAT");
      $tpl->set_var("ID", tohtml($fldID));
      $tpl->set_var("ID_URLLink", "S_USERSMF.php");
      $tpl->set_var("Prm_ID", tourl($db->f("S_ID"))); 
      $tpl->set_var("FormS_FLDS_MENUS_USERSGrid_Page", tourl($iPage)); 
      $tpl->set_var("TMPL_MENUID", tohtml($fldTMPL_MENUID));
      $tpl->set_var("FLD_ID", tohtml($fldFLD_ID));
      $tpl->set_var("CSS_CLASSNAME", tohtml($fldCSS_CLASSNAME));
      $tpl->set_var("ROWNUMBER", tohtml($fldROWNUMBER));
      $tpl->set_var("FLDFORMAT", tohtml($fldFLDFORMAT));
      $tpl->parse("DListS_FLDS_MENUS_USERSGrid", true);
      $iCounter++;
    } while($iCounter < $RecordsPerPage &&$db->next_record());
  }
  else
  {
    // No Records in DB
    $tpl->set_var("DListS_FLDS_MENUS_USERSGrid", "");
    $tpl->parse("S_FLDS_MENUS_USERSGridNoRecords", false);
    $tpl->set_var("S_FLDS_MENUS_USERSGridScroller", "");
    $tpl->parse("FormS_FLDS_MENUS_USERSGrid", false);
    return;
  }
  
  // Parse scroller
  if(@$db->next_record())
  {
    if ($iPage == 1)
    {
      $tpl->set_var("S_FLDS_MENUS_USERSGridScrollerPrevSwitch", "_");
    }
    else
    {
      $tpl->set_var("PrevPage", ($iPage - 1));
      $tpl->set_var("S_FLDS_MENUS_USERSGridScrollerPrevSwitch", "");
    }
    $tpl->set_var("NextPage", ($iPage + 1));
    $tpl->set_var("S_FLDS_MENUS_USERSGridScrollerNextSwitch", "");
    $tpl->set_var("S_FLDS_MENUS_USERSGridCurrentPage", $iPage);
    $tpl->parse("S_FLDS_MENUS_USERSGridScroller", false);
  }
  else
  {
    if ($iPage == 1)
    {
      $tpl->set_var("S_FLDS_MENUS_USERSGridScroller", "");
    }
    else
    {
      $tpl->set_var("S_FLDS_MENUS_USERSGridScrollerNextSwitch", "_");
      $tpl->set_var("PrevPage", ($iPage - 1));
      $tpl->set_var("S_FLDS_MENUS_USERSGridScrollerPrevSwitch", "");
      $tpl->set_var("S_FLDS_MENUS_USERSGridCurrentPage", $iPage);
      $tpl->parse("S_FLDS_MENUS_USERSGridScroller", false);
    }
  }
  $tpl->set_var("S_FLDS_MENUS_USERSGridNoRecords", "");
  $tpl->parse("FormS_FLDS_MENUS_USERSGrid", false);
  
}



// Menu & Field For Selected User - SQL actions
function S_FLDS_MENUS_USERSRecord_action($sAction)
{
  global $db;
  global $tpl;
  global $sForm;
  global $sS_FLDS_MENUS_USERSRecordErr;
  
  $sParams = "";
  
  $sParams = "?";
  $sParams .= "ID=" . tourl(get_param("Trn_ID"));

  $sWhere = "";
  $bErr = false;

  if($sAction == "cancel")
    header("Location: " . $sActionFileName . $sParams); 
  
  // Create WHERE statement
  if($sAction == "update" || $sAction == "delete") 
  {
    $pPKID = get_param("PK_ID");
    if( !strlen($pPKID)) return;
    $sWhere = "ID=" . tosql($pPKID, "Text");
  }

  // Load all form fields into variables
  
  $fldID = get_param("Rqd_ID");
  $fldUSERID = get_param("USERID");
  $fldTMPL_MENUID = get_param("TMPL_MENUID");
  $fldFLD_ID = get_param("FLD_ID");
  $fldCSS_CLASSNAME = get_param("CSS_CLASSNAME");
  $fldROWNUMBER = get_param("ROWNUMBER");
  $fldFLDFORMAT = get_param("FLDFORMAT");
  $fldSEARCH = get_checkbox_value(get_param("SEARCH"), "1", "0", "Number");
  // Validate fields
  if($sAction == "insert" || $sAction == "update") 
  {

    if(strlen($sS_FLDS_MENUS_USERSRecordErr)) return;
  }
  

  $sSQL = "";
  // Create SQL statement
  
  switch(strtolower($sAction)) 
  {
    case "insert":
      
        $sSQL = "insert into S_TMPL_FLD (" . 
          "ID," . 
          "USERID," . 
          "TMPL_MENUID," . 
          "FLD_ID," . 
          "CSS_CLASSNAME," . 
          "ROWNUMBER," . 
          "FLDFORMAT," . 
          "SEARCH)" . 
          " values (" . 
          tosql($fldID, "Number") . "," .
          tosql($fldUSERID, "Text") . "," .
          tosql($fldTMPL_MENUID, "Text") . "," .
          tosql($fldFLD_ID, "Text") . "," .
          tosql($fldCSS_CLASSNAME, "Text") . "," .
          tosql($fldROWNUMBER, "Text") . "," .
          tosql($fldFLDFORMAT, "Text") . "," .
          $fldSEARCH . ")";    
    break;
    case "update":
      
        $sSQL = "update S_TMPL_FLD set " .
          "USERID=" . tosql($fldUSERID, "Text") .
          ",TMPL_MENUID=" . tosql($fldTMPL_MENUID, "Text") .
          ",FLD_ID=" . tosql($fldFLD_ID, "Text") .
          ",CSS_CLASSNAME=" . tosql($fldCSS_CLASSNAME, "Text") .
          ",ROWNUMBER=" . tosql($fldROWNUMBER, "Text") .
          ",FLDFORMAT=" . tosql($fldFLDFORMAT, "Text") .
          ",SEARCH=" . $fldSEARCH;
        $sSQL .= " where " . $sWhere;
    break;
    case "delete":
      
        $sSQL = "delete from S_TMPL_FLD where " . $sWhere;
    break;
  }
  // Execute SQL statement
  if(strlen($sS_FLDS_MENUS_USERSRecordErr)) return;
	//echo "SQL:(".$sSQL.")<br>";
  $db->query($sSQL);
  
  header("Location: " . $sActionFileName . $sParams);
  
}

// Menu & Field For Selected User - display of selected record
function S_FLDS_MENUS_USERSRecord_show()
{
  global $db;
  global $tpl;
  global $sAction;
  global $sForm;
  global $sS_FLDS_MENUS_USERSRecordErr;

  $sWhere = "";
  
  $bPK = true;
  $fldID = "";
  $fldUSERID = "";
  $fldTMPL_MENUID = "";
  $fldFLD_ID = "";
  $fldCSS_CLASSNAME = "";
  $fldROWNUMBER = "";
  $fldFLDFORMAT = "";
  $fldSEARCH = "";

  if($sS_FLDS_MENUS_USERSRecordErr == "")
  {
    // Load primary key and form parameters
    $fldID = get_param("ID");
    $tpl->set_var("Trn_ID", get_param("ID"));
    $tpl->set_var("Rqd_ID", get_param("ID"));
    $pID = get_param("ID");
    $tpl->set_var("S_FLDS_MENUS_USERSRecordError", "");
  }
  else
  {
    // Load primary key, form parameters and form fields
    $fldID = strip(get_param("ID"));
    $fldUSERID = strip(get_param("USERID"));
    $fldTMPL_MENUID = strip(get_param("TMPL_MENUID"));
    $fldFLD_ID = strip(get_param("FLD_ID"));
    $fldCSS_CLASSNAME = strip(get_param("CSS_CLASSNAME"));
    $fldROWNUMBER = strip(get_param("ROWNUMBER"));
    $fldFLDFORMAT = strip(get_param("FLDFORMAT"));
    $fldSEARCH = strip(get_param("SEARCH"));
    $tpl->set_var("Trn_ID", get_param("Trn_ID"));
    $pID = get_param("PK_ID");
    $tpl->set_var("sS_FLDS_MENUS_USERSRecordErr", $sS_FLDS_MENUS_USERSRecordErr);
    $tpl->parse("S_FLDS_MENUS_USERSRecordError", false);
  }

  
  if( !strlen($pID)) $bPK = false;
  
  $sWhere .= "ID=" . tosql($pID, "Text");
  $tpl->set_var("PK_ID", $pID);

  $sSQL = "select * from S_TMPL_FLD where " . $sWhere;
	//echo "SQL:(".$sSQL.")<br>";

  if($bPK && !($sAction == "insert" && $sForm == "S_FLDS_MENUS_USERSRecord"))
  {
    // Execute SQL statement
    $db->query($sSQL);
    $db->next_record();
    
    $fldID = $db->f("ID");
    if($sS_FLDS_MENUS_USERSRecordErr == "") 
    {
      // Load data from recordset when form displayed first time
      $fldUSERID = $db->f("USERID");
      $fldTMPL_MENUID = $db->f("TMPL_MENUID");
      $fldFLD_ID = $db->f("FLD_ID");
      $fldCSS_CLASSNAME = $db->f("CSS_CLASSNAME");
			//echo "CSS:(".$fldCSS_CLASSNAME.")<br>";
      $fldROWNUMBER = $db->f("ROWNUMBER");
      $fldFLDFORMAT = $db->f("FLDFORMAT");
      $fldSEARCH = $db->f("SEARCH");
    }
    $tpl->set_var("S_FLDS_MENUS_USERSRecordInsert", "");
    $tpl->parse("S_FLDS_MENUS_USERSRecordEdit", false);
  }
  else
  {
    if($sS_FLDS_MENUS_USERSRecordErr == "")
    {
      $fldID = tohtml(get_param("ID"));
    }
    $tpl->set_var("S_FLDS_MENUS_USERSRecordEdit", "");
    $tpl->parse("S_FLDS_MENUS_USERSRecordInsert", false);
  }
  $tpl->parse("S_FLDS_MENUS_USERSRecordCancel", false);

  // Show form field
  
    $tpl->set_var("ID", tohtml($fldID));
    $tpl->set_var("LBUSERID", "");
    $tpl->set_var("ID", "");
    $tpl->set_var("Value", "");
    $tpl->set_var("CSS_CLASSNAME", tohtml($fldCSS_CLASSNAME));
    $tpl->set_var("ROWNUMBER", tohtml($fldROWNUMBER));
    $tpl->set_var("FLDFORMAT", tohtml($fldFLDFORMAT));
		switch ($fldFLDFORMAT)
		{
			case "0":
				$tpl->set_var("FLDFORMATCHECKED_0", "CHECKED");
			break;
			case "1":
				$tpl->set_var("FLDFORMATCHECKED_1", "CHECKED");
			break;
			case "2":
				$tpl->set_var("FLDFORMATCHECKED_2", "CHECKED");
			break;
			case "3":
				$tpl->set_var("FLDFORMATCHECKED_3", "CHECKED");
			break;
// 20070322 GFO: Add the POPUP possibility 
			case "4":
				$tpl->set_var("FLDFORMATCHECKED_4", "CHECKED");
			break;
			default:
			break;
		}
    $tpl->parse("LBUSERID", true);
    $dbUSERID = new DB_Sql();
    $dbUSERID->Database = DATABASE_NAME;
    $dbUSERID->User     = DATABASE_USER;
    $dbUSERID->Password = DATABASE_PASSWORD;
    $dbUSERID->Host     = DATABASE_HOST;
    
    $dbUSERID->query("select USERID, USERNAME from S_USERS order by 2");
    while($dbUSERID->next_record())
    {
      $tpl->set_var("ID", $dbUSERID->f(0));
      $tpl->set_var("Value", $dbUSERID->f(1));
      if($dbUSERID->f(0) == $fldUSERID)
        $tpl->set_var("Selected", "SELECTED" );
      else 
	{
        	$tpl->set_var("Selected", "");
	}
      $tpl->parse("LBUSERID", true);
    }
    
    $tpl->set_var("LBTMPL_MENUID", "");
    $tpl->set_var("ID", "");
    $tpl->set_var("Value", "");
    $tpl->parse("LBTMPL_MENUID", true);
    $dbTMPL_MENUID = new DB_Sql();
    $dbTMPL_MENUID->Database = DATABASE_NAME;
    $dbTMPL_MENUID->User     = DATABASE_USER;
    $dbTMPL_MENUID->Password = DATABASE_PASSWORD;
    $dbTMPL_MENUID->Host     = DATABASE_HOST;
    
    $dbTMPL_MENUID->query("select TMPL_MENUID, MENUNAME from S_TMPL_MENUS order by 2");
    while($dbTMPL_MENUID->next_record())
    {
      $tpl->set_var("ID", $dbTMPL_MENUID->f(0));
      $tpl->set_var("Value", $dbTMPL_MENUID->f(1));
      if($dbTMPL_MENUID->f(0) == $fldTMPL_MENUID)
        $tpl->set_var("Selected", "SELECTED" );
      else 
        $tpl->set_var("Selected", "");
      $tpl->parse("LBTMPL_MENUID", true);
    }
    
    $tpl->set_var("LBFLD_ID", "");
    $tpl->set_var("ID", "");
    $tpl->set_var("Value", "");
    $tpl->parse("LBFLD_ID", true);
    $dbFLD_ID = new DB_Sql();
    $dbFLD_ID->Database = DATABASE_NAME;
    $dbFLD_ID->User     = DATABASE_USER;
    $dbFLD_ID->Password = DATABASE_PASSWORD;
    $dbFLD_ID->Host     = DATABASE_HOST;
  
    $dbFLD_ID->query("select FLDID, FLDNAME, FLDTABLE from S_FLDS order by 2");
    while($dbFLD_ID->next_record())
    {
      $tpl->set_var("ID", $dbFLD_ID->f(0));
      $tpl->set_var("Value", $dbFLD_ID->f(1));
      if($dbFLD_ID->f(0) == $fldFLD_ID)
        $tpl->set_var("Selected", "SELECTED" );
      else 
        $tpl->set_var("Selected", "");
      $tpl->parse("LBFLD_ID", true);
    }
      if(strtolower($fldSEARCH) == strtolower("1")) 
        $tpl->set_var("SEARCH_CHECKED", "CHECKED");
      else
        $tpl->set_var("SEARCH_CHECKED", "");

	  $tpl->parse("FormS_FLDS_MENUS_USERSRecord", false);
}

?>
