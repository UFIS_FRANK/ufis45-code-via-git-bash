<?php
//********************************************************************************


function Administration_show()
{
  
  global $tpl;
  // Set URLs
  $fldUSR = "USERS.php";
  // 20070612 JIM: PRF 8200: User profiles: new page
  $fldPROF = "S_PROFILES.php";
  $fldMNU = "S_TMPL_MENUSRecord.php";
  $fldTTF = "S_FLDSRecord.php";
  $fldTEMP = "S_TMPLRecord.php";
  $fldUMT = "S_USERSMT.php";
  $fldUMF = "S_USERSMF.php";
  $fldLOGOUT = "Index.php";
  // Show fields
  $tpl->set_var("USR", $fldUSR);
  // 20070612 JIM: PRF 8200: User profiles: new page
  $tpl->set_var("PROF", $fldPROF);
  $tpl->set_var("MNU", $fldMNU);
  $tpl->set_var("TTF", $fldTTF);
  $tpl->set_var("TEMP", $fldTEMP);
  $tpl->set_var("UMT", $fldUMT);
  $tpl->set_var("UMF", $fldUMF);
  $tpl->set_var("LOGOUT", $fldLOGOUT);
  $tpl->parse("FormAdministration", false);
}

?>