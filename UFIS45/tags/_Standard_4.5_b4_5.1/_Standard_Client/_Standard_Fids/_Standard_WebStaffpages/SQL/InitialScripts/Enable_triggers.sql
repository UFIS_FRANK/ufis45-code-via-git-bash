--T_S_FLDS
--T_S_TMPL
--T_S_TMPL_FLD
--T_S_TMPL_MENUS
--T_S_TMPL_MENUS_USERS
--T_S_USERS
--T_S_USERS_NEW
--
CREATE OR REPLACE TRIGGER "T_S_FLDS" BEFORE INSERT ON "S_FLDS" FOR EACH ROW
begin
  select autonumber.nextval into :new.fldid from dual;
end;
/



CREATE OR REPLACE TRIGGER "T_S_TMPL" BEFORE INSERT ON "S_TMPL" FOR EACH ROW
begin
  select autonumber.nextval  into :new.tmplid from dual;
end;
/



CREATE OR REPLACE TRIGGER "T_S_TMPL_FLD" BEFORE INSERT ON "S_TMPL_FLD" FOR EACH ROW
begin
select autonumber.nextval into :new.id
from dual;
end;
/



CREATE OR REPLACE TRIGGER "T_S_TMPL_MENUS" BEFORE INSERT ON "S_TMPL_MENUS" FOR EACH ROW
begin
select autonumber.nextval into :new.tmpl_menuid
from dual;
end;
/



CREATE OR REPLACE TRIGGER "T_S_TMPL_MENUS_USERS" BEFORE INSERT ON "S_TMPL_MENUS_USERS" FOR EACH ROW
begin
select autonumber.nextval into :new.id
from dual;
end;
/



CREATE OR REPLACE TRIGGER "T_S_USERS" BEFORE INSERT ON "S_USERS" FOR EACH ROW
begin
select autonumber.nextval into :new.userid
from dual;
end;
/



CREATE OR REPLACE TRIGGER "T_S_USERS_NEW"
   BEFORE INSERT
   ON "S_USERS_NEW"
   FOR EACH ROW
BEGIN
   SELECT autonumber.NEXTVAL
     INTO :NEW.userid
     FROM DUAL;
END;
/




