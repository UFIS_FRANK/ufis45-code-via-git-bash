

Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (1, 'stod', 'afttab', 'STD', 'scheduled departure time', 
    '2', NULL, NULL, NULL, 3, 
    NULL, 1);
Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (2, 'flno', 'afttab', 'FN', 'number of flight', 
    '0', NULL, NULL, '(flno like ''%$Dynamicflno%'')', 2, 
    'AND', 1);
Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (47, 'des3', 'afttab', 'DES<BR>(IATA)', 'IATA code of destination', 
    '0', NULL, NULL, '(des3 like ''%$Dynamicdes3%'')', 4, 
    'AND', 1);
Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (322, 'etod', 'afttab', 'ETD', 'estimated departure time', 
    '1', NULL, NULL, NULL, 11, 
    NULL, 1);
Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (53, 'ofbl', 'afttab', 'OFB', 'Offblock-time', 
    '1', NULL, NULL, '(ofbl like ''%$Dynamicofbl%'')', 12, 
    'AND', 1);
Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (55, 'airb', 'afttab', 'ATD', 'Airborne time', 
    '1', NULL, NULL, NULL, 13, 
    NULL, 1);
Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (57, 'ckif', 'afttab', 'CKIF', 'Checkin counter from', 
    '0', NULL, NULL, NULL, 7, 
    NULL, 1);
Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (59, 'ckit', 'afttab', 'CKIT', 'Checkin counter to', 
    '0', NULL, NULL, NULL, 7, 
    NULL, 1);
Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (232, 'stoa', 'afttab', 'STA', 'scheduled arrival time', 
    '2', NULL, NULL, NULL, 2, 
    NULL, 1);
Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (233, 'etoa', 'afttab', 'ETA', 'Estimated arrival time', 
    '1', NULL, NULL, NULL, 8, 
    NULL, 1);
Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (234, 'onbl', 'afttab', 'ONB', 'onblock time', 
    '1', NULL, NULL, NULL, 9, 
    NULL, 1);
Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (724, 'styp', 'afttab', 'CODE', 'service type', 
    '0', NULL, NULL, NULL, 1, 
    NULL, 1);
Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (1558, 'alc3', 'afttab', NULL, NULL, 
    '0', NULL, NULL, '(alc3 LIKE ''%$airline%'' or alc2 like ''%$airline%'')', 99, 
    'AND', 0);
Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (239, 'org3', 'afttab', 'ORG <BR>(IATA)', 'IATA code of origin', 
    '0', NULL, NULL, '(org3 LIKE ''%$city%'' or via3 LIKE ''%$city%'')', 3, 
    'AND', 1);
Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (262, 'gtd1', 'afttab', 'GT', 'Gate 1', 
    '0', NULL, NULL, NULL, 9, 
    NULL, 1);
Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (273, 'remp', 'afttab', 'REMARK', 'Remark', 
    '0', NULL, NULL, '(remp like ''%$Dynamicremp%'')', 7, 
    'AND', 1);
Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (390, 'blt1', 'afttab', 'BELT', 'Belt 1', 
    '0', NULL, NULL, NULL, 13, 
    NULL, 1);
Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (743, 'alc2', 'afttab', 'ALC', 'airline code (IATA)', 
    '0', NULL, NULL, '(alc2 LIKE ''%$airline%'' or alc3 like ''%$airline%'')', 99, 
    'AND', 0);
Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (1634, 'dadr', 'devtab', 'IP', 'IP-address', 
    NULL, NULL, NULL, 'where dadr like ''%$Dynamicdadr%''', NULL, 
    NULL, 1);
Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (732, 'via1', 'afttab', 'VIA1', NULL, 
    '0', NULL, NULL, NULL, 5, 
    NULL, 1);
Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (733, 'via2', 'afttab', 'VIA2', NULL, 
    '0', NULL, NULL, NULL, 6, 
    NULL, 1);
Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (549, 'land', 'afttab', 'ATA', 'Landing time
', 
    '1', NULL, NULL, NULL, 8, 
    NULL, 1);
Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (553, 'psta', 'afttab', 'Arr. Bay', 'Aircraft position (Arr.)', 
    '0', NULL, NULL, '(psta like ''%$Dynamicpsta%'')', 10, 
    'AND', 1);
Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (556, 'regn', 'afttab', 'REG', 'Aircraft registration', 
    '0', NULL, NULL, NULL, 16, 
    NULL, 1);
Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (627, 'pstd', 'afttab', 'ST', 'Aircraft position (Dep.)', 
    '0', NULL, NULL, NULL, 14, 
    NULL, 1);
Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (712, 'act5', 'afttab', 'A/C', 'Aircraft type (ICAO)', 
    '0', NULL, NULL, NULL, 15, 
    NULL, 1);
Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (713, 'ext1', 'afttab', 'EXIT', 'Exit', 
    '0', NULL, NULL, NULL, 11, 
    NULL, 1);
Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (714, 'paxt', 'afttab', 'PAX', 'Nr. of passengers', 
    '0', NULL, NULL, NULL, 12, 
    NULL, 1);
Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (832, 'gd1x', 'afttab', 'BRD', 'start boarding', 
    '1', NULL, NULL, NULL, 8, 
    NULL, 1);
Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (1013, 'org4', 'afttab', '<BR>(ICAO)', 'ICAO code of origin', 
    '0', NULL, NULL, NULL, 4, 
    NULL, 1);
Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (1014, 'des4', 'afttab', '<BR>(ICAO)', 'ICAO code of destination', 
    '0', NULL, NULL, NULL, 4, 
    NULL, 1);
Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (1033, 'jfno', 'afttab', 'C/S', 'code share flight', 
    '0', NULL, NULL, '(jfno like ''%$jfno%'')', 2, 
    'OR', 0);
Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (1156, 'stoa', 'afttab', 'STA', 'scheduled time of arrival', 
    '1', NULL, NULL, NULL, 2, 
    NULL, 0);
Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (1157, 'stod', 'afttab', 'STD', 'scheduled time of departure', 
    '1', 1154, NULL, NULL, 2, 
    NULL, 1);
Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (1635, 'grpn', 'devtab', 'DSPHDL', 'display handler', 
    NULL, NULL, NULL, NULL, NULL, 
    NULL, 1);
Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (1636, 'dprt', 'devtab', 'PORT', 'device port', 
    NULL, NULL, NULL, NULL, NULL, 
    NULL, 1);
Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (1637, 'loca', 'devtab', 'LOC.', 'location of display', 
    NULL, NULL, NULL, NULL, NULL, 
    NULL, 1);
Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (1638, 'rema', 'devtab', 'REMARK', 'last reply from device', 
    NULL, NULL, NULL, NULL, NULL, 
    NULL, 1);
Insert into S_FLDS
   (FLDID, FLDNAME, FLDTABLE, FLDDISPLAYNAME, FLDEXPTEXT, 
    FLDFORMAT, TMPL_MENU_ID, TMPLID, QUERY, ROWNUMBER, 
    FUNCTION, DISPLAYFIELD)
 Values
   (1639, 'stat', 'devtab', 'STATUS', 'status of display', 
    NULL, NULL, NULL, 'and stat like ''%$Dynamicstat%''', NULL, 
    NULL, 1);
COMMIT;

Insert into S_TMPL
   (TMPLID, TMPLNAME)
 Values
   (1573, 'std.tmpl');
Insert into S_TMPL
   (TMPLID, TMPLNAME)
 Values
   (1673, 'std_dyn_search.tmpl');
COMMIT;

Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1642, 1635, 0, 1633, 4, 
    'TDStandard', 2, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (947, 743, 1, 1, 4, 
    'TDDeparture', 99, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (948, 724, 0, 1, 4, 
    'TDDeparture', 1, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (950, 732, 0, 1, 4, 
    'TDDeparture', 5, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (951, 733, 0, 1, 4, 
    'TDDeparture', 6, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (956, 2, 1, 2, 4, 
    'TDArrival', 2, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1559, 1558, 1, 2, 4, 
    NULL, 99, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1560, 1558, 1, 797, 4, 
    NULL, 99, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1561, 1558, 0, 1, 4, 
    NULL, 99, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1562, 1558, 1, 817, 4, 
    NULL, 99, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (945, 712, 0, 2, 4, 
    'TDArrival', 15, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (957, 232, 1, 2, 4, 
    'TDArrival', 2, '2');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (959, 233, 0, 2, 4, 
    'TDArrival', 8, '1');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (960, 549, 0, 2, 4, 
    'TDArrival', 8, '1');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (961, 234, 0, 2, 4, 
    'TDArrival', 9, '1');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (962, 553, 0, 2, 4, 
    'TDArrival', 10, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (963, 390, 0, 2, 4, 
    'TDArrival', 13, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (965, 273, 1, 2, 4, 
    'TDArrival', 7, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (968, 59, 0, 1, 4, 
    'TDDeparture', 7, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (970, 47, 1, 1, 4, 
    'TDDeparture', 4, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (972, 53, 0, 1, 4, 
    'TDDeparture', 12, '1');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (974, 556, 0, 1, 4, 
    'TDDeparture', 16, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (976, 627, 0, 1, 4, 
    'TDDeparture', 14, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (978, 832, 0, 1, 4, 
    'TDDeparture', 8, '1');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (981, 2, 1, 797, 4, 
    'TDArrival', 2, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (982, 239, 0, 797, 4, 
    'TDArrival', 3, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (985, 273, 1, 797, 4, 
    'TDArrival', 7, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (986, 233, 0, 797, 4, 
    'TDArrival', 8, '1');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (988, 234, 0, 797, 4, 
    'TDArrival', 9, '1');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (989, 713, 0, 797, 4, 
    'TDArrival', 11, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (990, 553, 1, 797, 4, 
    'TDArrival', 10, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (991, 390, 0, 797, 4, 
    'TDArrival', 13, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (993, 712, 0, 797, 4, 
    'TDArrival', 15, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (995, 724, 0, 817, 4, 
    'TDDeparture', 1, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (997, 1, 0, 817, 4, 
    'TDDeparture', 3, '2');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (998, 732, 0, 817, 4, 
    'TDDeparture', 5, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (999, 733, 0, 817, 4, 
    'TDDeparture', 6, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1000, 57, 0, 817, 4, 
    'TDDeparture', 7, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1001, 59, 0, 817, 4, 
    'TDDeparture', 7, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1003, 731, 1, 817, 4, 
    'TDDeparture', 10, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1004, 322, 0, 817, 4, 
    'TDDeparture', 11, '1');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1005, 53, 0, 817, 4, 
    'TDDeparture', 12, '1');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1006, 55, 0, 817, 4, 
    'TDDeparture', 13, '1');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1007, 627, 0, 817, 4, 
    'TDDeparture', 14, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1010, 743, 1, 817, 4, 
    'TDDeparture', 99, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1011, 743, 1, 797, 4, 
    'TDArrival', 99, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1012, 47, 0, 817, 4, 
    'TDDeparture', 4, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1026, 1013, 0, 2, 4, 
    'TDArrival', 4, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1027, 1013, 0, 797, 4, 
    'TDArrival', 4, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1018, 1014, 0, 817, 4, 
    'TDDeparture', 4, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (958, 239, 1, 2, 4, 
    'TDArrival', 3, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (966, 713, 0, 2, 4, 
    'TDArrival', 11, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (973, 55, 0, 1, 4, 
    'TDDeparture', 13, '1');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (979, 832, 0, 817, 4, 
    'TDDeparture', 8, '1');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (987, 549, 0, 797, 4, 
    'TDArrival', 8, '1');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (994, 232, 1, 797, 4, 
    'TDArrival', 2, '2');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1002, 262, 0, 817, 4, 
    'TDDeparture', 9, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1009, 556, 0, 817, 4, 
    'TDDeparture', 16, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (964, 57, 0, 1, 4, 
    'TDDeparture', 7, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (969, 1, 1, 1, 4, 
    'TDDeparture', 3, '2');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (971, 322, 0, 1, 4, 
    'TDDeparture', 11, '1');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (975, 262, 0, 1, 4, 
    'TDDeparture', 9, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (977, 731, 1, 1, 4, 
    'TDDeparture', 10, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1017, 1014, 0, 1, 4, 
    'TDDeparture', 4, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1325, 732, 0, 797, 4, 
    'TDArrival', 5, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1326, 733, 0, 797, 4, 
    'TDArrival', 6, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1327, 556, 0, 797, 4, 
    'TDArrival', 16, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1328, 724, 0, 797, 4, 
    'TDArrival', 1, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1329, 2, 0, 817, 4, 
    'TDDeparture', 2, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1330, 712, 0, 817, 4, 
    'TDDeparture', 15, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1331, 724, 0, 2, 4, 
    'TDArrival', 1, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1332, 732, 0, 2, 4, 
    'TDArrival', 5, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1333, 733, 0, 2, 4, 
    'TDArrival', 6, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1334, 556, 0, 2, 4, 
    'TDArrival', 16, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1335, 2, 0, 1, 4, 
    'TDDeparture', 2, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1336, 712, 0, 1, 4, 
    'TDDeparture', 15, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1598, NULL, 1, 797, 1595, 
    NULL, NULL, '1');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1600, 712, 0, 797, 1595, 
    NULL, NULL, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1601, NULL, 1, NULL, NULL, 
    NULL, NULL, NULL);
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1615, NULL, 0, NULL, NULL, 
    NULL, NULL, NULL);
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1616, 273, 1, 1, 4, 
    'TDDeparture', 10, NULL);
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1617, 273, 1, 817, 4, 
    'TDDeparture', 10, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1653, 1634, 1, 1633, 4, 
    'TDStandard', 1, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1643, 1636, 0, 1633, 4, 
    'TDStandard', 3, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1644, 1638, 0, 1633, 4, 
    'TDStandard', 4, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1645, 1637, 0, 1633, 4, 
    'TDStandard', 5, '0');
Insert into S_TMPL_FLD
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME, ROWNUMBER, FLDFORMAT)
 Values
   (1646, 1639, 1, 1633, 4, 
    'TDStandard', 6, '0');
COMMIT;

Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (946, 743, 1, 2, 1072, 
    'TDArrival');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (947, 743, 1, 1, 1072, 
    'TDDeparture');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (948, 724, 0, 1, 1072, 
    'TDDeparture');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (950, 732, 0, 1, 1072, 
    'TDDeparture');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (951, 733, 0, 1, 1072, 
    'TDDeparture');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (956, 2, 1, 2, 1072, 
    'TDArrival');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (945, 712, 0, 2, 1072, 
    'TDArrival');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (957, 232, 1, 2, 1072, 
    'TDArrival');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (958, 239, 1, 2, 1072, 
    'TDArrival');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (959, 233, 0, 2, 1072, 
    'TDArrival');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (960, 549, 0, 2, 1072, 
    'TDArrival');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (961, 234, 0, 2, 1072, 
    'TDArrival');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (962, 553, 0, 2, 1072, 
    'TDArrival');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (963, 390, 0, 2, 1072, 
    'TDArrival');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (964, 57, 0, 1, 1072, 
    'TDDeparture');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (965, 273, 0, 2, 1072, 
    'TDArrival');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (966, 713, 0, 2, 1072, 
    'TDArrival');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (968, 59, 0, 1, 1072, 
    'TDDeparture');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (969, 1, 1, 1, 1072, 
    'TDDeparture');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (970, 47, 1, 1, 1072, 
    'TDDeparture');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (971, 322, 0, 1, 1072, 
    'TDDeparture');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (972, 53, 0, 1, 1072, 
    'TDDeparture');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (973, 55, 0, 1, 1072, 
    'TDDeparture');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (974, 556, 0, 1, 1072, 
    'TDDeparture');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (975, 262, 0, 1, 1072, 
    'TDDeparture');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (976, 627, 0, 1, 1072, 
    'TDDeparture');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (977, 731, 0, 1, 1072, 
    'TDDeparture');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (978, 832, 0, 1, 1072, 
    'TDDeparture');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (979, 832, 0, 817, 1072, 
    'TDDeparture');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (981, 2, 1, 797, 1072, 
    'TDArrival');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (982, 239, 0, 797, 1072, 
    'TDArrival');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (985, 273, 0, 797, 1072, 
    'TDArrival');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (986, 233, 0, 797, 1072, 
    'TDArrival');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (987, 549, 0, 797, 1072, 
    'TDArrival');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (988, 234, 0, 797, 1072, 
    'TDArrival');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (989, 713, 0, 797, 1072, 
    'TDArrival');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (990, 553, 0, 797, 1072, 
    'TDArrival');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (991, 390, 0, 797, 1072, 
    'TDArrival');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (993, 712, 0, 797, 1072, 
    'TDArrival');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (994, 232, 0, 797, 1072, 
    'TDArrival');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (995, 724, 0, 817, 1072, 
    'TDDeparture');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (997, 1, 0, 817, 1072, 
    'TDDeparture');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (998, 732, 0, 817, 1072, 
    'TDDeparture');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (999, 733, 0, 817, 1072, 
    'TDDeparture');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (1000, 57, 0, 817, 1072, 
    'TDDeparture');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (1001, 59, 0, 817, 1072, 
    'TDDeparture');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (1002, 262, 0, 817, 1072, 
    'TDDeparture');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (1003, 731, 0, 817, 1072, 
    'TDDeparture');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (1004, 322, 0, 817, 1072, 
    'TDDeparture');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (1005, 53, 0, 817, 1072, 
    'TDDeparture');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (1006, 55, 0, 817, 1072, 
    'TDDeparture');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (1007, 627, 0, 817, 1072, 
    'TDDeparture');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (1009, 556, 0, 817, 1072, 
    'TDDeparture');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (1010, 743, 1, 817, 1072, 
    'TDDeparture');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (1011, 743, 1, 797, 1072, 
    'TDArrival');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (1012, 47, 0, 817, 1072, 
    'TDDeparture');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (1026, 1013, 0, 2, 1072, 
    'TDArrival');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (1027, 1013, 0, 797, 1072, 
    'TDArrival');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (1017, 1014, 0, 1, 1072, 
    'TDDeparture');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (1018, 1014, 0, 817, 1072, 
    'TDDeparture');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (1325, 732, 0, 797, 1072, 
    'TDArrival');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (1326, 733, 0, 797, 1072, 
    'TDArrival');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (1327, 556, 0, 797, 1072, 
    'TDArrival');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (1328, 724, 0, 797, 1072, 
    'TDArrival');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (1329, 2, 0, 817, 1072, 
    'TDDeparture');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (1330, 712, 0, 817, 1072, 
    'TDDeparture');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (1331, 724, 0, 2, 1072, 
    'TDArrival');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (1332, 732, 0, 2, 1072, 
    'TDArrival');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (1333, 733, 0, 2, 1072, 
    'TDArrival');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (1334, 556, 0, 2, 1072, 
    'TDArrival');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (1335, 2, 0, 1, 1072, 
    'TDDeparture');
Insert into S_TMPL_FLD_COPY
   (ID, FLD_ID, SEARCH, TMPL_MENUID, USERID, 
    CSS_CLASSNAME)
 Values
   (1336, 712, 0, 1, 1072, 
    'TDDeparture');
COMMIT;

Insert into S_TMPL_MENUS
   (MENUNAME, ROWNUMBER, QUERY, TMPL_MENUID, PAGETITLE, 
    ORGDES, REFRESHRATE, ROWSPERPAGE, HLCYCLE, SHOWTIMEVALUES, 
    TFVAL1, TFVAL2)
 Values
   ('Departures (scroll.)', 4, 'select 
urno,
decode(substr(x.styp,1,1),''F'',''CGO'',''H'',''CGO'',''X'',''E/T'',''P'',''POS'',''W'',''MIL'',''T'',''TRN'',''D'',''GEN'','' '') as styp ,
x.FLNO as flno,
x.JFNO as jfno,
x.STOD as stod,
x.DES3 as des3,
x.DES4 as des4,
substr(vial,2,3) as VIA1,
substr(vial,122,3) as VIA2,
x.CKIF as ckif,
x.CKIT as ckit,
x.GD1X as gd1x,
x.GTD1 as gtd1,
x.REMP as remp,
x.ETOD as etod,
x.OFBL as ofbl,
x.AIRB as airb,
x.PSTD as pstd,
x.PAXT as paxt,
x.ACT5 as act5,
x.REGN as regn,
x.ALC2 as alc2,
x.ALC3 as alc3 
  from ceda.afttab x WHERE ((TIFD BETWEEN ''$TfVal1'' AND ''$TfVal2'') AND (ADID = ''D'' or ADID=''B'') AND (FLNO <> '' '') AND (TTYP <> '' '')) AND FTYP NOT IN(''N'') AND STYP NOT LIKE ''E%'' ORDER BY TIFD,FLNO', 1, 'DEPARTURE', 
    'Dest.', 60, 100, 0, 1, 
    -1440, 1440);
Insert into S_TMPL_MENUS
   (MENUNAME, ROWNUMBER, QUERY, TMPL_MENUID, PAGETITLE, 
    ORGDES, REFRESHRATE, ROWSPERPAGE, HLCYCLE, SHOWTIMEVALUES, 
    TFVAL1, TFVAL2)
 Values
   ('Arrivals (scroll.)', 3, 'select urno,decode(substr(styp,1,1),''F'',''CGO'',''H'',''CGO'',''X'',''E/T'',''P'',''POS'',''W'',''MIL'',''T'',''TRN'',''D'',''GEN'','' '') as STYP,FLNO,JFNO,STOA,ORG3,ORG4,substr(vial,2,3) as via1, substr(vial,122,3) as via2,REMP,ETOA,LAND,ONBL,PSTA,EXT1,PAXT,BLT1,ACT5,REGN,alc2,alc3  from ceda.afttab WHERE ((TIFA BETWEEN ''$TfVal1'' AND ''$TfVal2'') AND (ADID = ''A'' or ADID=''B'') AND (FLNO <> '' '')  AND (TTYP <> '' '')) AND FTYP NOT IN (''N'') AND STYP NOT LIKE ''E%'' ORDER BY TIFA,FLNO  ', 2, 'ARRIVAL', 
    'Orig.', 60, 100, 0, 1, 
    -1440, 1440);
Insert into S_TMPL_MENUS
   (MENUNAME, ROWNUMBER, QUERY, TMPL_MENUID, PAGETITLE, 
    ORGDES, REFRESHRATE, ROWSPERPAGE, HLCYCLE, SHOWTIMEVALUES, 
    TFVAL1, TFVAL2)
 Values
   ('Arrivals (stat.)', 1, 'select urno,decode(substr(styp,1,1),''F'',''CGO'',''H'',''CGO'',''X'',''E/T'',''P'',''POS'',''W'',''MIL'',''T'',''TRN'',''D'',''GEN'','' '') as STYP,FLNO,JFNO,STOA,ORG3,ORG4,substr(vial,2,3) as via1, substr(vial,122,3) as via2,REMP,ETOA,LAND,ONBL,PSTA,EXT1,PAXT,BLT1,ACT5,REGN,alc2,alc3  from ceda.afttab  WHERE (TIFA like to_char(sysdate,''YYYYMMDD'')||''%'' AND (ADID = ''A'' or ADID=''B'') AND (FLNO <> '' '') AND (TTYP <> '' '')) AND FTYP NOT IN(''N'') AND STYP NOT LIKE ''E%'' ORDER BY TIFA,FLNO', 797, 'ARRIVAL', 
    'Orig.', 60, 100, 0, 1, 
    0, 0);
Insert into S_TMPL_MENUS
   (MENUNAME, ROWNUMBER, QUERY, TMPL_MENUID, PAGETITLE, 
    ORGDES, REFRESHRATE, ROWSPERPAGE, HLCYCLE, SHOWTIMEVALUES, 
    TFVAL1, TFVAL2)
 Values
   ('Departures (stat.)', 2, 'select 
urno,
decode(substr(x.styp,1,1),''F'',''CGO'',''H'',''CGO'',''X'',''E/T'',''P'',''POS'',''W'',''MIL'',''T'',''TRN'',''D'',''GEN'','' '') as styp ,
x.FLNO as flno,
x.JFNO as jfno,
x.STOD as stod,
x.DES3 as des3,
x.DES4 as des4,
substr(vial,2,3) as VIA1,
substr(vial,122,3) as VIA2,
x.CKIF as ckif,
x.CKIT as ckit,
x.GD1X as gd1x,
x.GTD1 as gtd1,
x.REMP as remp,
x.ETOD as etod,
x.OFBL as ofbl,
x.AIRB as airb,
x.PSTD as pstd,
x.PAXT as paxt,
x.ACT5 as act5,
x.REGN as regn,
x.ALC2 as alc2,
x.ALC3 as alc3
 from ceda.afttab x
 WHERE (TIFD like to_char(sysdate,''YYYYMMDD'')||''%'' AND (ADID = ''D'' or ADID=''B'') AND (FLNO <> '' '') AND (TTYP <> '' '')) AND FTYP NOT IN(''N'') AND STYP NOT LIKE ''E%'' ORDER BY TIFD,FLNO   ', 817, 'DEPARTURE', 
    'Dest.', 60, 100, 0, 1, 
    0, 0);
Insert into S_TMPL_MENUS
   (MENUNAME, ROWNUMBER, QUERY, TMPL_MENUID, PAGETITLE, 
    ORGDES, REFRESHRATE, ROWSPERPAGE, HLCYCLE, SHOWTIMEVALUES, 
    TFVAL1, TFVAL2)
 Values
   ('DEVTAB', 5, 'select dadr,grpn,dprt,loca,rema,decode(stat,''U'',''<font color=green>UP</font>'',''D'',''<font color=red>DOWN</font>'',''M'',''<font color=black>MAINT.</font>'') as stat FrOm devtab order bY dadr,grpn', 1633, 'DISPLAY STATUS', 
    NULL, 60, 100, 1, 1, 
    NULL, NULL);
COMMIT;

Insert into S_TMPL_MENUS_USERS
   (ID, TMPL_MENUID, USERID, TMPLID, STYLESHEET)
 Values
   (429, 1, 4, 1573, 'std_style.css');
Insert into S_TMPL_MENUS_USERS
   (ID, TMPL_MENUID, USERID, TMPLID, STYLESHEET)
 Values
   (206, 2, 4, 380, 'style.css');
Insert into S_TMPL_MENUS_USERS
   (ID, TMPL_MENUID, USERID, TMPLID, STYLESHEET)
 Values
   (300, 1, 4, 380, 'style.css');
Insert into S_TMPL_MENUS_USERS
   (ID, TMPL_MENUID, USERID, TMPLID, STYLESHEET)
 Values
   (754, NULL, NULL, NULL, 'style.css');
Insert into S_TMPL_MENUS_USERS
   (ID, TMPL_MENUID, USERID, TMPLID, STYLESHEET)
 Values
   (427, 2, 4, 1573, 'std_style.css');
Insert into S_TMPL_MENUS_USERS
   (ID, TMPL_MENUID, USERID, TMPLID, STYLESHEET)
 Values
   (753, NULL, NULL, NULL, 'style.css');
Insert into S_TMPL_MENUS_USERS
   (ID, TMPL_MENUID, USERID, TMPLID, STYLESHEET)
 Values
   (816, 797, 4, 1573, 'std_style.css');
Insert into S_TMPL_MENUS_USERS
   (ID, TMPL_MENUID, USERID, TMPLID, STYLESHEET)
 Values
   (818, 817, 4, 1573, 'std_style.css');
Insert into S_TMPL_MENUS_USERS
   (ID, TMPL_MENUID, USERID, TMPLID, STYLESHEET)
 Values
   (1594, 2, 1593, 1573, NULL);
Insert into S_TMPL_MENUS_USERS
   (ID, TMPL_MENUID, USERID, TMPLID, STYLESHEET)
 Values
   (1596, 797, 1595, 1573, 'std_style.ccs');
Insert into S_TMPL_MENUS_USERS
   (ID, TMPL_MENUID, USERID, TMPLID, STYLESHEET)
 Values
   (1597, 817, 1595, 1573, 'std_style.ccs');
Insert into S_TMPL_MENUS_USERS
   (ID, TMPL_MENUID, USERID, TMPLID, STYLESHEET)
 Values
   (1614, 1, 1595, 1573, 'std_style.ccs');
Insert into S_TMPL_MENUS_USERS
   (ID, TMPL_MENUID, USERID, TMPLID, STYLESHEET)
 Values
   (1613, 2, 1595, 1573, 'std_style.ccs');
Insert into S_TMPL_MENUS_USERS
   (ID, TMPL_MENUID, USERID, TMPLID, STYLESHEET)
 Values
   (1640, 1633, 4, 1673, 'std_style.css');
COMMIT;

Insert into S_USERS
   (USERID, USERNAME, PASSWD, IS_PUBLIC, SECURITY_LEVEL)
 Values
   (4, 'Admin', 'Admin', 1, 3);
Insert into S_USERS
   (USERID, USERNAME, PASSWD, IS_PUBLIC, SECURITY_LEVEL)
 Values
   (1593, 'Staff', 'Staff', 1, 0);
Insert into S_USERS
   (USERID, USERNAME, PASSWD, IS_PUBLIC, SECURITY_LEVEL)
 Values
   (1595, 'jhi', 'jhi', 1, 0);
COMMIT;

Insert into S_USERS_NEW
   (USERID, USERNAME, PASSWD, PROF_ID)
 Values
   (4, 'Admin', 'Admin', 4);
Insert into S_USERS_NEW
   (USERID, USERNAME, PASSWD, PROF_ID)
 Values
   (1593, 'Staff', 'Staff', 1593);
Insert into S_USERS_NEW
   (USERID, USERNAME, PASSWD, PROF_ID)
 Values
   (1595, 'jhi', 'jhi', 1595);
COMMIT;

