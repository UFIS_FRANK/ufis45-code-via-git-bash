// FidasTableFields.h : table fields header file for the Fidas application
//

#if !defined(AFX_FidasTableFields_H__7B1DCED8_54EB_11D5_811D_00010215BFDE__INCLUDED_)
#define AFX_FidasTableFields_H__7B1DCED8_54EB_11D5_811D_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include <vector>
class CFieldInfo;
typedef std::vector<CFieldInfo> CFieldInfoVector;
typedef std::vector<CString>	CStringVector;

class CUfisCom;
class CFidasTableFields
{
public :
	CFidasTableFields(const CString& ropTableName);
	~CFidasTableFields();
	BOOL		LoadTableFields(CUfisCom *popUfisCom);
	int			GetFieldNames(CStringVector& ropFields) const;
	int			GetFieldDescriptions(CStringVector& ropDesc) const;
	int			GetFieldInfos(CFieldInfoVector& ropInfos) const;
private:
private:
	CString				omTableName;
	CFieldInfoVector	omFieldInfos;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FidasTableFields_H__7B1DCED8_54EB_11D5_811D_00010215BFDE__INCLUDED_)
