# Microsoft Developer Studio Project File - Name="Fidas" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=FIDAS - WIN32 DEBUG
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "Fidas.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Fidas.mak" CFG="FIDAS - WIN32 DEBUG"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Fidas - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "Fidas - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Fidas - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "c:\Ufis_Bin\Release"
# PROP Intermediate_Dir "c:\Ufis_Intermediate\Fidas\Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /I ".\\" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_AFXDLL" /YX"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x407 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 c:\Ufis_Bin\Release\Ufis32.lib c:\Ufis_bin\Classlib\Release\ccsclass.lib /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "Fidas - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "c:\Ufis_Bin\Debug"
# PROP Intermediate_Dir "c:\Ufis_Intermediate\Fidas\Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /I ".\\" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_AFXDLL" /FR /YX"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x407 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 c:\Ufis_Bin\Debug\Ufis32.lib c:\Ufis_Bin\Classlib\Debug\ccsclass.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "Fidas - Win32 Release"
# Name "Fidas - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\aatlib.cpp
# End Source File
# Begin Source File

SOURCE=..\..\_Standard_Share\_Standard_Wrapper\aatlogin.cpp
# End Source File
# Begin Source File

SOURCE=.\CDpyType.cpp
# End Source File
# Begin Source File

SOURCE=.\CFDDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\ChildFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\CUfisControlDB.cpp
# End Source File
# Begin Source File

SOURCE=.\CUfisDB.cpp
# End Source File
# Begin Source File

SOURCE=.\dummy.cpp
# PROP Exclude_From_Build 1
# End Source File
# Begin Source File

SOURCE=.\Fidas.cpp
# End Source File
# Begin Source File

SOURCE=.\Fidas.odl
# End Source File
# Begin Source File

SOURCE=.\Fidas.rc
# End Source File
# Begin Source File

SOURCE=.\FidasAirlineDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\FidasAirportDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\FidasColorButton.cpp
# End Source File
# Begin Source File

SOURCE=.\FidasDateTimeFieldDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\FidasDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\FidasFieldContentDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\FidasFieldDefinitionProperties.cpp
# End Source File
# Begin Source File

SOURCE=.\FidasFieldTypesDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\FidasFormView.cpp
# End Source File
# Begin Source File

SOURCE=.\FidasLogoDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\FidasNewDocumentDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\FidasPrint.cpp
# End Source File
# Begin Source File

SOURCE=.\FidasRemarkDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\FidasSelectGroupDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\FidasSelectTableFieldDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\FidasStaticFieldDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\FidasTableFields.cpp
# End Source File
# Begin Source File

SOURCE=.\FidasTabView.cpp
# End Source File
# Begin Source File

SOURCE=.\FidasUtilities.cpp
# End Source File
# Begin Source File

SOURCE=.\FidasView.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=..\..\_Standard_Share\_Standard_Wrapper\tab.cpp
# End Source File
# Begin Source File

SOURCE=..\..\_Standard_Share\_Standard_Wrapper\ufiscom.cpp
# End Source File
# Begin Source File

SOURCE=.\VersionInfo.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\aatlib.h
# End Source File
# Begin Source File

SOURCE=..\..\_Standard_Share\_Standard_Wrapper\aatlogin.h
# End Source File
# Begin Source File

SOURCE=.\CDpyType.h
# End Source File
# Begin Source File

SOURCE=.\CFDDoc.h
# End Source File
# Begin Source File

SOURCE=.\ChildFrm.h
# End Source File
# Begin Source File

SOURCE=.\COLORSEL.H
# End Source File
# Begin Source File

SOURCE=.\CUfisControlDB.h
# End Source File
# Begin Source File

SOURCE=.\CUfisDB.h
# End Source File
# Begin Source File

SOURCE=.\Fidas.h
# End Source File
# Begin Source File

SOURCE=.\FidasAirlineDialog.h
# End Source File
# Begin Source File

SOURCE=.\FidasAirportDialog.h
# End Source File
# Begin Source File

SOURCE=.\FidasColorButton.h
# End Source File
# Begin Source File

SOURCE=.\FidasDateTimeFieldDialog.h
# End Source File
# Begin Source File

SOURCE=.\FidasDoc.h
# End Source File
# Begin Source File

SOURCE=.\FidasFieldContentDialog.h
# End Source File
# Begin Source File

SOURCE=.\FidasFieldDefinitionProperties.h
# End Source File
# Begin Source File

SOURCE=.\FidasFieldTypesDialog.h
# End Source File
# Begin Source File

SOURCE=.\FidasFormView.h
# End Source File
# Begin Source File

SOURCE=.\FidasLogoDialog.h
# End Source File
# Begin Source File

SOURCE=.\FidasNewDocumentDialog.h
# End Source File
# Begin Source File

SOURCE=.\FidasPrint.h
# End Source File
# Begin Source File

SOURCE=.\FidasRemarkDialog.h
# End Source File
# Begin Source File

SOURCE=.\FidasSelectGroupDialog.h
# End Source File
# Begin Source File

SOURCE=.\FidasSelectTableFieldDialog.h
# End Source File
# Begin Source File

SOURCE=.\FidasStaticFieldDialog.h
# End Source File
# Begin Source File

SOURCE=.\FidasTableFields.h
# End Source File
# Begin Source File

SOURCE=.\FidasTabView.h
# End Source File
# Begin Source File

SOURCE=.\FidasUtilities.h
# End Source File
# Begin Source File

SOURCE=.\FidasView.h
# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=..\..\_Standard_Share\_Standard_Wrapper\tab.h
# End Source File
# Begin Source File

SOURCE=..\..\_Standard_Share\_Standard_Wrapper\ufiscom.h
# End Source File
# Begin Source File

SOURCE=.\VersionInfo.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\Fidas.ico
# End Source File
# Begin Source File

SOURCE=.\res\Fidas.rc2
# End Source File
# Begin Source File

SOURCE=.\res\FidasDoc.ico
# End Source File
# Begin Source File

SOURCE=.\res\ico00001.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon1.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon2.ico
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# Begin Source File

SOURCE=.\res\UFIS.ICO
# End Source File
# Begin Source File

SOURCE=.\res\ufis1.ico
# End Source File
# End Group
# Begin Source File

SOURCE=.\Fidas.reg
# End Source File
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
# Section Fidas : {01D09294-4C8C-11D6-9DDA-00010204AA7E}
# 	2:5:Class:C_AATLoginControl
# 	2:10:HeaderFile:_aatlogincontrol.h
# 	2:8:ImplFile:_aatlogincontrol.cpp
# End Section
# Section Fidas : {9F4BE0C0-1F0B-11D6-9DB8-00010204AA7E}
# 	2:5:Class:C_AATLoginControl
# 	2:10:HeaderFile:_aatlogincontrol.h
# 	2:8:ImplFile:_aatlogincontrol.cpp
# End Section
# Section Fidas : {385C5AF9-66F8-11D5-805B-0001022205E4}
# 	2:21:DefaultSinkHeaderFile:_aatlogincontrol.h
# 	2:16:DefaultSinkClass:C_AATLoginControl
# End Section
# Section Fidas : {F2C3D672-557D-11D5-811E-00010215BFDE}
# 	2:21:DefaultSinkHeaderFile:_aatlogincontrol.h
# 	2:16:DefaultSinkClass:C_AATLoginControl
# End Section
# Section Fidas : {25AE8EC1-66F2-11D5-8135-00010215BFDE}
# 	2:21:DefaultSinkHeaderFile:_aatlogincontrol.h
# 	2:16:DefaultSinkClass:C_AATLoginControl
# End Section
# Section Fidas : {64E8E383-05E2-11D2-9B1D-9B0630BC8F12}
# 	2:5:Class:CTAB
# 	2:10:HeaderFile:tab.h
# 	2:8:ImplFile:tab.cpp
# End Section
# Section Fidas : {2CD200F5-5FDB-11D5-8054-0001022205E4}
# 	2:21:DefaultSinkHeaderFile:_aatlogincontrol.h
# 	2:16:DefaultSinkClass:C_AATLoginControl
# End Section
# Section Fidas : {25AE8EC0-66F2-11D5-8135-00010215BFDE}
# 	2:5:Class:C_AATLoginControl
# 	2:10:HeaderFile:_aatlogincontrol.h
# 	2:8:ImplFile:_aatlogincontrol.cpp
# End Section
# Section Fidas : {385C5AF8-66F8-11D5-805B-0001022205E4}
# 	2:5:Class:C_AATLoginControl
# 	2:10:HeaderFile:_aatlogincontrol.h
# 	2:8:ImplFile:_aatlogincontrol.cpp
# End Section
# Section Fidas : {F2C3D671-557D-11D5-811E-00010215BFDE}
# 	2:5:Class:C_AATLoginControl
# 	2:10:HeaderFile:_aatlogincontrol.h
# 	2:8:ImplFile:_aatlogincontrol.cpp
# End Section
# Section Fidas : {A2F31E95-C74F-11D3-A251-00500437F607}
# 	2:21:DefaultSinkHeaderFile:ufiscom.h
# 	2:16:DefaultSinkClass:CUfisCom
# End Section
# Section Fidas : {2CD200F4-5FDB-11D5-8054-0001022205E4}
# 	2:5:Class:C_AATLoginControl
# 	2:10:HeaderFile:_aatlogincontrol.h
# 	2:8:ImplFile:_aatlogincontrol.cpp
# End Section
# Section Fidas : {37201112-54B5-11D5-811D-00010215BFDE}
# 	2:21:DefaultSinkHeaderFile:_aatlogincontrol.h
# 	2:16:DefaultSinkClass:C_AATLoginControl
# End Section
# Section Fidas : {D15EA9A2-63C5-11D5-8130-00010215BFDE}
# 	2:21:DefaultSinkHeaderFile:_aatlogincontrol.h
# 	2:16:DefaultSinkClass:C_AATLoginControl
# End Section
# Section Fidas : {37201111-54B5-11D5-811D-00010215BFDE}
# 	2:5:Class:C_AATLoginControl
# 	2:10:HeaderFile:_aatlogincontrol.h
# 	2:8:ImplFile:_aatlogincontrol.cpp
# End Section
# Section Fidas : {A2F31E93-C74F-11D3-A251-00500437F607}
# 	2:5:Class:CUfisCom
# 	2:10:HeaderFile:ufiscom.h
# 	2:8:ImplFile:ufiscom.cpp
# End Section
# Section Fidas : {01D09295-4C8C-11D6-9DDA-00010204AA7E}
# 	2:21:DefaultSinkHeaderFile:_aatlogincontrol.h
# 	2:16:DefaultSinkClass:C_AATLoginControl
# End Section
# Section Fidas : {D15EA9A1-63C5-11D5-8130-00010215BFDE}
# 	2:5:Class:C_AATLoginControl
# 	2:10:HeaderFile:_aatlogincontrol.h
# 	2:8:ImplFile:_aatlogincontrol.cpp
# End Section
# Section Fidas : {64E8E385-05E2-11D2-9B1D-9B0630BC8F12}
# 	2:21:DefaultSinkHeaderFile:tab.h
# 	2:16:DefaultSinkClass:CTAB
# End Section
