#if !defined(AFX_FIDASFIELDTYPESDIALOG_H__50809607_5E26_11D5_812B_00010215BFDE__INCLUDED_)
#define AFX_FIDASFIELDTYPESDIALOG_H__50809607_5E26_11D5_812B_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FidasFieldTypesDialog.h : header file
//
#include <CDpyType.h>

/////////////////////////////////////////////////////////////////////////////
// CFidasFieldTypesDialog dialog

class CFidasFieldTypesDialog : public CDialog
{
// Construction
public:
	CFidasFieldTypesDialog(CWnd* pParent = NULL);   // standard constructor
	CDpyType::Type GetSelectedType() const;
	void			SetSelectedType(const CDpyType::Type& repType);

private:
// Dialog Data
	//{{AFX_DATA(CFidasFieldTypesDialog)
	enum { IDD = IDD_FIELD_TYPES };
	CListBox	m_ListFieldTypes;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFidasFieldTypesDialog)
	public:
	virtual void OnFinalRelease();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFidasFieldTypesDialog)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeListFieldTypes();
	afx_msg void OnDblclkListFieldTypes();
	afx_msg void OnSelcancelListFieldTypes();
	virtual void OnOK();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(CFidasFieldTypesDialog)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
private:
	CDpyType::Type	emSelectedType;	
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FIDASFIELDTYPESDIALOG_H__50809607_5E26_11D5_812B_00010215BFDE__INCLUDED_)
