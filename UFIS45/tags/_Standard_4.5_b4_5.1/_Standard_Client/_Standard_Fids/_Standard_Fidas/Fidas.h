// Fidas.h : main header file for the Fidas application
//

#if !defined(AFX_Fidas_H__7B1DCED8_54EB_11D5_811D_00010215BFDE__INCLUDED_)
#define AFX_Fidas_H__7B1DCED8_54EB_11D5_811D_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include <resource.h>       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CFidasApp:
// See Fidas.cpp for the implementation of this class
//

class CFidasApp : public CWinApp
{
public:
	CFidasApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFidasApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	COleTemplateServer m_server;
		// Server object for document creation
	//{{AFX_MSG(CFidasApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_Fidas_H__7B1DCED8_54EB_11D5_811D_00010215BFDE__INCLUDED_)
