// FidasAirlineDialog.cpp : implementation file
//

#include <stdafx.h>
#include <fidas.h>
#include <FidasAirlineDialog.h>
#include <FidasUtilities.h>
#include <FidasSelectTableFieldDialog.h>
#include <CFDDoc.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFidasAirlineDialog dialog

CFidasAirlineDialog::CFidasAirlineDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CFidasAirlineDialog::IDD, pParent)
{
	EnableAutomation();

	//{{AFX_DATA_INIT(CFidasAirlineDialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	pomFieldDesc = NULL;
}


void CFidasAirlineDialog::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CDialog::OnFinalRelease();
}

void CFidasAirlineDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFidasAirlineDialog)
	DDX_Control(pDX, IDC_UFIS_AIRLINE_NAME_COMBO, m_AirlineFields);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFidasAirlineDialog, CDialog)
	//{{AFX_MSG_MAP(CFidasAirlineDialog)
	ON_BN_CLICKED(IDC_UFIS_AIRLINE_OK_BUTTON, OnUfisAirlineOkButton)
	ON_BN_CLICKED(IDC_UFIS_AIRLINE_CANCEL_BUTTON, OnUfisAirlineCancelButton)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CFidasAirlineDialog, CDialog)
	//{{AFX_DISPATCH_MAP(CFidasAirlineDialog)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IFidasAirlineDialog to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {C3385C73-5A35-11D5-8124-00010215BFDE}
static const IID IID_IFidasAirlineDialog =
{ 0xc3385c73, 0x5a35, 0x11d5, { 0x81, 0x24, 0x0, 0x1, 0x2, 0x15, 0xbf, 0xde } };

BEGIN_INTERFACE_MAP(CFidasAirlineDialog, CDialog)
	INTERFACE_PART(CFidasAirlineDialog, IID_IFidasAirlineDialog, Dispatch)
END_INTERFACE_MAP()

void CFidasAirlineDialog::SetFieldDescription(CFieldDesc *prpFieldDesc)
{
	ASSERT(prpFieldDesc);
	pomFieldDesc = prpFieldDesc;
}

/////////////////////////////////////////////////////////////////////////////
// CFidasAirlineDialog message handlers

BOOL CFidasAirlineDialog::OnInitDialog() 
{
	ASSERT(pomFieldDesc && pomFieldDesc->DpyType() && pomFieldDesc->DpyType()->GetType() == CDpyType::AL);

	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	SetWindowText(CFidasUtilities::GetString(IDS_UFIS_AIRLINE_DIALOG));

	CWnd *pWnd = GetDlgItem(IDC_UFIS_AIRLINE_OK_BUTTON);
	if (pWnd)
	{
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_AIRLINE_OK_BUTTON));
	}

	pWnd = GetDlgItem(IDC_UFIS_AIRLINE_CANCEL_BUTTON);
	if (pWnd)
	{
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_AIRLINE_CANCEL_BUTTON));
	}

	pWnd = GetDlgItem(IDC_UFIS_AIRLINE_NAME);
	if (pWnd)
	{
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_AIRLINE_NAME));
	}

	// load data
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CFidasAirlineDialog::OnUfisAirlineOkButton() 
{
	// TODO: Add your control notification handler code here
	CDialog::OnOK();
}

void CFidasAirlineDialog::OnUfisAirlineCancelButton() 
{
	// TODO: Add your control notification handler code here
	CDialog::OnCancel();
	
}
