// FidasPrint.cpp : implementation of the CFidasPrint class
//

#include <stdafx.h>
#include <FidasPrint.h>
#include <WINSPOOL.H>
 

const double INCH = 0.284;



CFidasPrint::CFidasPrint(CWnd *opParent)
{
	bmIsInitialized = FALSE;
	pomParent = opParent;
	imLineNo  = 999;
	imPageNo  = 0;
	imMaxLinesPerPage = 14;
	pomBitmap = NULL;
	pomCcsBitmap = NULL;
}

int CFidasPrint::MMX(int x)
{
    return MulDiv(x,imLogPixelsX, 72)*INCH;
}

int CFidasPrint::MMY(int x)
{
    return MulDiv(x,imLogPixelsY, 72)*INCH;
}

CFidasPrint::CFidasPrint(CWnd *opParent,int ipOrientation,int ipLineHeight,int ipFirstLine,int ipLeftOffset,
						 CString opHeader1,CString opHeader2,CString opHeader3,CString opHeader4,
						 CString opFooter1,CString opFooter2,CString opFooter3)
{
	bmIsInitialized = FALSE;

	imLineHeight = ipLineHeight;
	imFirstLine  = ipFirstLine;
	imLeftOffset = ipLeftOffset;

	pomParent = opParent;
	imOrientation = ipOrientation;
	omHeader.Add(opHeader1);
	omHeader.Add(opHeader2);
	omHeader.Add(opHeader3);
	omHeader.Add(opHeader4);
	omHeader.Add(opFooter1);
	omHeader.Add(opFooter2);
	omHeader.Add(opFooter3);

	imLineNo = 999;
	imPageNo = 0;
	if (imOrientation == PRINT_LANDSCAPE)
	{
		imMaxLinesPerPage = 18;
	}
	else
	{
		imMaxLinesPerPage = 24;
	}

}


CFidasPrint::~CFidasPrint()
{
	if (bmIsInitialized == TRUE)
	{
		omCdc.DeleteDC();
		ThinPen.DeleteObject();
		MediumPen.DeleteObject();
		ThickPen.DeleteObject();
		DottedPen.DeleteObject();
		omRgn.DeleteObject();
	}

}


void CFidasPrint::SetBitmaps(CBitmap *popBitmap, CBitmap *popCcsBitmap)
{
	if(popBitmap != NULL)
	{
		pomBitmap = popBitmap;
	}

	if(popCcsBitmap != NULL)
	{
		pomCcsBitmap = popCcsBitmap;
	}
}

BOOL CFidasPrint::OnPreparePrinting(CPrintInfo* pInfo,int ipOrientation,int ipLineHeight,int ipFirstLine,int ipLeftOffset)
{
	imLineHeight = ipLineHeight;
	imFirstLine  = ipFirstLine;
	imLeftOffset = ipLeftOffset;

	imOrientation = ipOrientation;
	imLineNo	= 999;
	imPageNo	= 0;
	if (imOrientation == PRINT_LANDSCAPE)
	{
		imMaxLinesPerPage = 18;
	}
	else
	{
		imMaxLinesPerPage = 24;
	}

	char pclDevices[182];

	::GetProfileString("windows", "device","",pclDevices,180);
	CString olDevices = pclDevices;
	CString olDeviceName;
	int ilKomma = olDevices.Find(',');
	if (ilKomma > -1)
	{
		olDeviceName = olDevices.Left(ilKomma);
	}

	char pclDeviceName[182];
	strcpy(pclDeviceName,olDeviceName);

	HANDLE  hlPrinter = 0;
	static HANDLE hDevMode = 0;
	
	if (hDevMode == 0)
	{
		if (OpenPrinter(pclDeviceName,&hlPrinter,NULL) == TRUE)
		{
			LONG llDevModeLen = DocumentProperties(NULL,hlPrinter,pclDeviceName,NULL,NULL,0);
			if (llDevModeLen > 0)
			{
				hDevMode = GlobalAlloc(GMEM_MOVEABLE,llDevModeLen);
				DEVMODE *prlDevMode = (DEVMODE *) GlobalLock(hDevMode);
				DocumentProperties(NULL,hlPrinter,pclDeviceName,prlDevMode,NULL,DM_OUT_BUFFER);
				DWORD dm_orientation =  DM_ORIENTATION;
				prlDevMode->dmFields = DM_ORIENTATION;
				prlDevMode->dmOrientation = DMORIENT_LANDSCAPE;
				DocumentProperties(NULL,hlPrinter,pclDeviceName,prlDevMode,prlDevMode,DM_IN_BUFFER|DM_OUT_BUFFER);
				GlobalUnlock(hDevMode);
			}
			ClosePrinter(hlPrinter);
		}
	}

	imPageNo = 0;

	pInfo->m_bDirect = TRUE;
	if (hDevMode != 0)
	{
		pInfo->m_pPD->m_pd.hDevMode = hDevMode;
	}

	LPDEVMODE prlOldDevMode = pInfo->m_pPD->GetDevMode( );


	int ilRc = pInfo->m_pPD->DoModal();
	if (ilRc == IDOK)
		return TRUE;
	else
		return FALSE;
}


BOOL CFidasPrint::InitializePrinter(int ipOrientation)
{
	int ilRc;

	HDC hlHdc;


	char pclDevices[182];

	::GetProfileString("windows", "device","",pclDevices,180);
	CString olDevices = pclDevices;
	CString olDeviceName;
	int ilKomma = olDevices.Find(',');
	if (ilKomma > -1)
	{
		olDeviceName = olDevices.Left(ilKomma);
	}


	char pclDeviceName[182];
	strcpy(pclDeviceName,olDeviceName);

	HANDLE  hlPrinter = 0;
	static HANDLE hDevMode = 0;
	
	if (hDevMode == 0)
	{
		if (OpenPrinter(pclDeviceName,&hlPrinter,NULL) == TRUE)
		{
			{
				LONG llDevModeLen = DocumentProperties(NULL,hlPrinter,pclDeviceName,NULL,NULL,0);
				if (llDevModeLen > 0)
				{
					hDevMode = GlobalAlloc(GMEM_MOVEABLE,llDevModeLen);
					DEVMODE *prlDevMode = (DEVMODE *) GlobalLock(hDevMode);
					DocumentProperties(NULL,hlPrinter,pclDeviceName,prlDevMode,NULL,DM_OUT_BUFFER);
					DWORD dm_orientation =  DM_ORIENTATION;
					prlDevMode->dmFields = DM_ORIENTATION;
					prlDevMode->dmOrientation = DMORIENT_LANDSCAPE;
					DocumentProperties(NULL,hlPrinter,pclDeviceName,prlDevMode,prlDevMode,DM_IN_BUFFER|DM_OUT_BUFFER);
					GlobalUnlock(hDevMode);
				}
			}
			ClosePrinter(hlPrinter);
		}
	}
   
	
	imPageNo = 0;

	CPrintDialog *polPrintDialog = new CPrintDialog(
		  FALSE,PD_ALLPAGES|PD_NOPAGENUMS|PD_NOSELECTION|PD_USEDEVMODECOPIESANDCOLLATE,
		  pomParent);

	if (hDevMode != 0)
	{
		polPrintDialog->m_pd.hDevMode = hDevMode;
	}
	LPDEVMODE prlOldDevMode = polPrintDialog->GetDevMode( );


	ilRc = polPrintDialog->DoModal();
	if (ilRc != IDCANCEL )
	{
		LPDEVMODE prlDevMode = polPrintDialog->GetDevMode( );
		if (!prlDevMode)
			return FALSE;

		hlHdc = polPrintDialog->GetPrinterDC();
		if (!hlHdc)
			return FALSE;

		omCdc.Attach(hlHdc);
		omCdc.SetMapMode(MM_TEXT);
	

		imLogPixelsY = omCdc.GetDeviceCaps(LOGPIXELSY);
		imLogPixelsX = omCdc.GetDeviceCaps(LOGPIXELSX);

		omRgn.CreateRectRgn(0,0,omCdc.GetDeviceCaps(HORZRES),omCdc.GetDeviceCaps(VERTRES));
		omCdc.SelectClipRgn(&omRgn);


		TRACE("X = %d, Y = %d \n",imLogPixelsX,imLogPixelsY);
		LOGFONT rlLf;
		memset(&rlLf, 0, sizeof(LOGFONT));

		rlLf.lfCharSet= DEFAULT_CHARSET;
		rlLf.lfHeight = - MulDiv(8, imLogPixelsY, 72);
		rlLf.lfWeight = FW_NORMAL;
		rlLf.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
		lstrcpy(rlLf.lfFaceName, "Arial");
		ilRc = omSmallFont_Regular.CreateFontIndirect(&rlLf);

// MCU 29.09.98 create small font for gantt chart only
		rlLf.lfCharSet= DEFAULT_CHARSET;
		rlLf.lfHeight = - MulDiv(4, imLogPixelsY, 72);
		rlLf.lfWeight = FW_NORMAL;
		rlLf.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
		lstrcpy(rlLf.lfFaceName, "Arial");
		ilRc = omSmallFont_RegularForGantt.CreateFontIndirect(&rlLf);


		rlLf.lfCharSet= DEFAULT_CHARSET;
		rlLf.lfHeight = - MulDiv(12, imLogPixelsY, 72); 
		ilRc = omMediumFont_Regular.CreateFontIndirect(&rlLf);

		rlLf.lfCharSet= DEFAULT_CHARSET;
		rlLf.lfHeight =  - MulDiv(18, imLogPixelsY, 72);   
		ilRc = omLargeFont_Regular.CreateFontIndirect(&rlLf);

		rlLf.lfHeight = - MulDiv(8, imLogPixelsY, 72);
		rlLf.lfCharSet= DEFAULT_CHARSET;
		rlLf.lfWeight = FW_BOLD;
		ilRc = omSmallFont_Bold.CreateFontIndirect(&rlLf);

		rlLf.lfCharSet= DEFAULT_CHARSET;
		rlLf.lfHeight = - MulDiv(12, imLogPixelsY, 72); 
		ilRc = omMediumFont_Bold.CreateFontIndirect(&rlLf);

		rlLf.lfCharSet= DEFAULT_CHARSET;
		rlLf.lfHeight =  - MulDiv(18, imLogPixelsY, 72);   
		ilRc = omLargeFont_Bold.CreateFontIndirect(&rlLf);

		rlLf.lfHeight = - MulDiv(10, imLogPixelsY, 72);
		rlLf.lfPitchAndFamily = 2;
		rlLf.lfCharSet = SYMBOL_CHARSET;
		rlLf.lfWeight = 400;
		lstrcpy(rlLf.lfFaceName, "ccs");
		ilRc = omCCSFont.CreateFontIndirect(&rlLf);

		ThinPen.CreatePen(PS_SOLID, MulDiv(2, imLogPixelsX, 254), RGB(0,0,0));
		MediumPen.CreatePen(PS_SOLID, MulDiv(4, imLogPixelsX, 254), RGB(0,0,0));
		ThickPen.CreatePen(PS_SOLID, MulDiv(8, imLogPixelsX, 254), RGB(0,0,0));
		DottedPen.CreatePen(PS_DOT,1, RGB(0,0,0));
		delete polPrintDialog;

		// loading the header bitmap

		//MWO pomBitmap.LoadBitmap(IDB_PEPPER);
		omMemDc.CreateCompatibleDC(&omCdc);
		if (pomBitmap)
		{
			omMemDc.SelectObject(pomBitmap);
		}

		//MWO pomCcsBitmap.LoadBitmap(IDB_CCS);
		omCcsMemDc.CreateCompatibleDC(&omCdc);
		if (pomCcsBitmap)
		{
			omCcsMemDc.SelectObject(pomCcsBitmap);
		}

		bmIsInitialized = TRUE;

		TEXTMETRIC olTm;

		omCdc.GetTextMetrics(&olTm);
		int  ilHeight = olTm.tmHeight + olTm.tmExternalLeading;
		imBarHeight = (olTm.tmHeight + olTm.tmExternalLeading);
		imGanttLineHeight = (int) (imBarHeight * 1.2);

		// calculate vertical ofset of text in a bar
	    CFont *polOldFont = omCdc.SelectObject(&omSmallFont_Regular);
		CSize olSize = omCdc.GetTextExtent("LH 4711");
		imBarVerticalTextOffset = (int) ((imBarHeight-olSize.cy)/2);
		omCdc.SelectObject(polOldFont);
	}

	return ilRc;
}

void CFidasPrint::OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo)
{
//	omCdc.Attach(pDC->m_hDC);
	omCdc.SetOutputDC(pDC->m_hDC);
	omCdc.SetAttribDC(pDC->m_hAttribDC);

	omCdc.SetMapMode(MM_TEXT);
	

	imLogPixelsY = omCdc.GetDeviceCaps(LOGPIXELSY);
	imLogPixelsX = omCdc.GetDeviceCaps(LOGPIXELSX);

	omRgn.CreateRectRgn(0,0,omCdc.GetDeviceCaps(HORZRES),omCdc.GetDeviceCaps(VERTRES));
	omCdc.SelectClipRgn(&omRgn);


	TRACE("X = %d, Y = %d \n",imLogPixelsX,imLogPixelsY);
	LOGFONT rlLf;
	memset(&rlLf, 0, sizeof(LOGFONT));

	rlLf.lfCharSet= DEFAULT_CHARSET;
	rlLf.lfHeight = - MulDiv(8, imLogPixelsY, 72);
	rlLf.lfWeight = FW_NORMAL;
	rlLf.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
	lstrcpy(rlLf.lfFaceName, "Arial");
	int ilRc = omSmallFont_Regular.CreateFontIndirect(&rlLf);

	rlLf.lfCharSet= DEFAULT_CHARSET;
	rlLf.lfHeight = - MulDiv(4, imLogPixelsY, 72);
	rlLf.lfWeight = FW_NORMAL;
	rlLf.lfPitchAndFamily = VARIABLE_PITCH | FF_SWISS;
	lstrcpy(rlLf.lfFaceName, "Arial");
	ilRc = omSmallFont_RegularForGantt.CreateFontIndirect(&rlLf);


	rlLf.lfCharSet= DEFAULT_CHARSET;
	rlLf.lfHeight = - MulDiv(12, imLogPixelsY, 72); 
	ilRc = omMediumFont_Regular.CreateFontIndirect(&rlLf);

	rlLf.lfCharSet= DEFAULT_CHARSET;
	rlLf.lfHeight =  - MulDiv(18, imLogPixelsY, 72);   
	ilRc = omLargeFont_Regular.CreateFontIndirect(&rlLf);

	rlLf.lfCharSet= DEFAULT_CHARSET;
	rlLf.lfHeight = - MulDiv(8, imLogPixelsY, 72);
	rlLf.lfWeight = FW_BOLD;
	ilRc = omSmallFont_Bold.CreateFontIndirect(&rlLf);

	rlLf.lfCharSet= DEFAULT_CHARSET;
	rlLf.lfHeight = - MulDiv(12, imLogPixelsY, 72); 
	ilRc = omMediumFont_Bold.CreateFontIndirect(&rlLf);

	rlLf.lfCharSet= DEFAULT_CHARSET;
	rlLf.lfHeight =  - MulDiv(18, imLogPixelsY, 72);   
	ilRc = omLargeFont_Bold.CreateFontIndirect(&rlLf);

	rlLf.lfHeight = - MulDiv(10, imLogPixelsY, 72);
	rlLf.lfPitchAndFamily = 2;
	rlLf.lfCharSet = SYMBOL_CHARSET;
	rlLf.lfWeight = 400;
	lstrcpy(rlLf.lfFaceName, "ccs");
	ilRc = omCCSFont.CreateFontIndirect(&rlLf);

	ThinPen.CreatePen(PS_SOLID, MulDiv(2, imLogPixelsX, 254), RGB(0,0,0));
	MediumPen.CreatePen(PS_SOLID, MulDiv(4, imLogPixelsX, 254), RGB(0,0,0));
	ThickPen.CreatePen(PS_SOLID, MulDiv(8, imLogPixelsX, 254), RGB(0,0,0));
	DottedPen.CreatePen(PS_DOT,1, RGB(0,0,0));

	// loading the header bitmap

	//MWO pomBitmap.LoadBitmap(IDB_PEPPER);
	omMemDc.CreateCompatibleDC(&omCdc);
	omMemDc.SelectObject(pomBitmap);

	//MWO pomCcsBitmap.LoadBitmap(IDB_CCS);
	omCcsMemDc.CreateCompatibleDC(&omCdc);
	omCcsMemDc.SelectObject(pomCcsBitmap);

	bmIsInitialized = TRUE;

	TEXTMETRIC olTm;

	omCdc.GetTextMetrics(&olTm);
	int  ilHeight = olTm.tmHeight + olTm.tmExternalLeading;
	imBarHeight = (olTm.tmHeight + olTm.tmExternalLeading);
	imGanttLineHeight = (int) (imBarHeight * 1.2);

	// calculate vertical ofset of text in a bar
    CFont *polOldFont = omCdc.SelectObject(&omSmallFont_Regular);
	CSize olSize = omCdc.GetTextExtent("LH 4711");
	imBarVerticalTextOffset = (int) ((imBarHeight-olSize.cy)/2);
	omCdc.SelectObject(polOldFont);
}

void CFidasPrint::OnEndPrinting(CDC* pDC, CPrintInfo* pInfo)
{
	if (bmIsInitialized == TRUE)
	{
//		omCdc.Detach();
		omCdc.ReleaseOutputDC();
		omCdc.ReleaseAttribDC();

		ThinPen.DeleteObject();
		MediumPen.DeleteObject();
		ThickPen.DeleteObject();
		DottedPen.DeleteObject();
		omRgn.DeleteObject();

		omSmallFont_Regular.DeleteObject();
		omSmallFont_RegularForGantt.DeleteObject();
		omMediumFont_Regular.DeleteObject();
		omLargeFont_Regular.DeleteObject();
		omSmallFont_Bold.DeleteObject();
		omMediumFont_Bold.DeleteObject();
		omLargeFont_Bold.DeleteObject();
		omCCSFont.DeleteObject();

		omMemDc.DeleteDC();
		omCcsMemDc.DeleteDC();

		bmIsInitialized = FALSE;
	}
}



BOOL CFidasPrint::PrintHeader()
{
	PrintHeader(omHeader[0],omHeader[1],omHeader[2],omHeader[3]);
	return TRUE;
}


BOOL CFidasPrint::PrintHeader(CString opHeader1,CString opHeader2,CString opHeader3,CString opHeader4)
{
	CFont *polOldFont;
	CPen  *polOldPen; 

	imPageNo++;
	imLineNo = 0;

	omCdc.StretchBlt(MMX(100),MMY(100),MMX(270),MMY(270),&omMemDc,
		0,0,519,519, SRCCOPY);
	
    polOldPen = omCdc.SelectObject(&ThinPen);
	
	omCdc.Rectangle(MMX(400),MMY(150),MMX(2800),MMY(330));
    omCdc.MoveTo(MMX(420),MMY(250));
    omCdc.LineTo(MMX(2400),MMY(250));

    polOldFont = omCdc.SelectObject(&omLargeFont_Regular);
	omCdc.TextOut(MMX(430),MMY(170), opHeader1, strlen(opHeader1) );

    omCdc.SelectObject(&omSmallFont_Bold);
	omCdc.TextOut(MMX(2480),MMY(210), opHeader2, strlen(opHeader2) );

    omCdc.SelectObject(&omMediumFont_Regular);
	omCdc.TextOut(MMX(430),MMY(270), opHeader3, strlen(opHeader3) );


	omCdc.SelectObject(polOldFont);
	omCdc.SelectObject(polOldPen);

	return TRUE;
}

BOOL CFidasPrint::PrintFooter()
{
	return TRUE;

}


BOOL CFidasPrint::PrintFooter(CString opFooter1,CString opFooter2)
{

	//CFont *polOldFont;
	CPen  *polOldPen; 
	int ilXOffset = MMX(200);
	int ilYOffset = omCdc.GetDeviceCaps(VERTRES)-MMY(50);
	CSize olSize;

    polOldPen = omCdc.SelectObject(&ThinPen);
	
    omCdc.MoveTo(MMX(180),ilYOffset-MMY(50));
    omCdc.LineTo(MMX(2850),ilYOffset-MMY(50));

	ilXOffset += 200;

    omCdc.SelectObject(&omSmallFont_Regular);
	olSize = omCdc.GetTextExtent(opFooter1);
	omCdc.TextOut(ilXOffset,ilYOffset-olSize.cy,opFooter1);
 	ilXOffset += olSize.cx;

    omCdc.SelectObject(&omSmallFont_Bold);
	olSize = omCdc.GetTextExtent(opFooter2);
	omCdc.TextOut(ilXOffset,ilYOffset-olSize.cy+5,opFooter2);


 	ilXOffset = MMX(2700);
    omCdc.SelectObject(&omSmallFont_Regular);
	olSize = omCdc.GetTextExtent("Seite: ");
	omCdc.TextOut(ilXOffset,ilYOffset-olSize.cy,"Seite: ",7);
 	ilXOffset += olSize.cx;

	char olPageNo[24];
	sprintf(olPageNo,"%3d",imPageNo);
    omCdc.SelectObject(&omSmallFont_Bold);
	olSize = omCdc.GetTextExtent(olPageNo);
	omCdc.TextOut(ilXOffset,ilYOffset-olSize.cy,olPageNo,strlen(olPageNo));


	//omCdc.SelectObject(polOldFont);
	omCdc.SelectObject(polOldPen);

	return TRUE;
}


BOOL CFidasPrint::SelectFramePen(int ipFrameType)
{
	int ilRc = TRUE; 
	switch (ipFrameType)
	{
	case PRINT_FRAMETHIN: 
		omCdc.SelectObject(ThinPen);
		break;
	case PRINT_FRAMEMEDIUM: 
		omCdc.SelectObject(MediumPen);
		break;
	case PRINT_FRAMETHICK: 
		omCdc.SelectObject(ThickPen);
		break;
	default: ilRc = FALSE;

	}
	return ilRc;
}

void CFidasPrint::PrintLeft(CRect opRect,CString opText)
{

	TEXTMETRIC olTm;

	// insert a small space at the left side
	opRect.left += MMX(10);
	omCdc.GetTextMetrics(&olTm);
	int  ilHeight = olTm.tmHeight + olTm.tmExternalLeading;
	omCdc.TextOut(opRect.left,opRect.bottom-ilHeight,opText);
	
}

void CFidasPrint::PrintRight(CRect opRect,CString opText)
{

	CSize olSize = omCdc.GetTextExtent(opText);

	omCdc.TextOut(opRect.right-olSize.cx,opRect.bottom-olSize.cy,opText);
	
}

void CFidasPrint::PrintCenter(CRect opRect,CString opText)
{

	CSize olSize = omCdc.GetTextExtent(opText);
	
	int ilOffset = max(0,(int)(opRect.right-opRect.left-olSize.cx)/2);
	omCdc.TextOut(opRect.left+ilOffset,opRect.bottom-olSize.cy,opText);
	
}

BOOL CFidasPrint::PrintLine(std::vector<PRINTELEDATA> &ropPrintLine)
{
	CPen *polOldPen;
	
	int ilLeftPos = imLeftOffset;
	int ilLineTop = imFirstLine + (imLineHeight*imLineNo);

	polOldPen = omCdc.SelectObject(&ThinPen);

	for(int i = 0; i < ropPrintLine.size(); i++)
	{
		PRINTELEDATA *prlEle = &ropPrintLine[i];
		
		int ilLineOffset = imLineNo * imLineHeight;

		if (SelectFramePen(prlEle->FrameLeft) == TRUE)
		{
			omCdc.MoveTo(MMX(ilLeftPos),MMY(ilLineTop));
			omCdc.LineTo(MMX(ilLeftPos),MMY(ilLineTop+imLineHeight));
		}
		if (SelectFramePen(prlEle->FrameTop) == TRUE)
		{
			omCdc.MoveTo(MMX(ilLeftPos),MMY(ilLineTop));
			omCdc.LineTo(MMX(ilLeftPos+prlEle->Length),MMY(ilLineTop));
		}
		if (SelectFramePen(prlEle->FrameBottom) == TRUE)
		{
			omCdc.MoveTo(MMX(ilLeftPos),MMY(ilLineTop+imLineHeight));
			omCdc.LineTo(MMX(ilLeftPos+prlEle->Length),MMY(ilLineTop+imLineHeight));
		}
		if (SelectFramePen(prlEle->FrameRight) == TRUE)
		{
			omCdc.MoveTo(MMX(ilLeftPos+prlEle->Length),MMY(ilLineTop));
			omCdc.LineTo(MMX(ilLeftPos+prlEle->Length),MMY(ilLineTop+imLineHeight));
		}
		if (prlEle->Text.IsEmpty() == FALSE)
		{
			CRgn rlRgn;

			rlRgn.CreateRectRgn(MMX(ilLeftPos),MMY(ilLineTop+2),
				MMX(ilLeftPos+prlEle->Length),MMY(ilLineTop+imLineHeight-2));
			omCdc.SelectClipRgn(&rlRgn);

			CFont *polOldFont = omCdc.SelectObject(prlEle->pFont);
			CRect olRect(MMX(ilLeftPos+2),MMY(ilLineTop+2),
				MMX(ilLeftPos+prlEle->Length-2),MMY(ilLineTop+imLineHeight-2));
			switch (prlEle->Alignment)
			{
			case PRINT_LEFT: PrintLeft(olRect,prlEle->Text);
				break;
			case PRINT_RIGHT: PrintRight(olRect,prlEle->Text);
				break;
			case PRINT_CENTER: PrintCenter(olRect,prlEle->Text);
				break;
			}
			omCdc.SelectClipRgn(&omRgn);
			rlRgn.DeleteObject();
		}
		ilLeftPos += prlEle->Length;

	}
	ropPrintLine.clear();
	imLineNo++;
	return TRUE;
}




BOOL CFidasPrint::PrintText(int ipStartX,int ipStartY,int ipEndX,int ipEndY,int ipType,CString opText,BOOL ipUseOffset)
{
    CFont *polOldFont;
	int ilStartY;
	int ilEndY;

	switch ( ipType )
	{
	case PRINT_SMALLBOLD:
		polOldFont = omCdc.SelectObject(&omSmallFont_Bold);
		break;
	case PRINT_MEDIUMBOLD:
		polOldFont = omCdc.SelectObject(&omMediumFont_Bold);
		break;
	case PRINT_LARGEBOLD:
		polOldFont = omCdc.SelectObject(&omLargeFont_Bold);
		break;
	case PRINT_MEDIUM:
		polOldFont = omCdc.SelectObject(&omMediumFont_Regular);
		break;
	case PRINT_LARGE:
		polOldFont = omCdc.SelectObject(&omLargeFont_Regular);
		break;
	case PRINT_SMALL:
	default:
		// if we find no Type, use small Font as in PRINT_SMALL
		polOldFont = omCdc.SelectObject(&omSmallFont_Regular);
		break;
	}

	if (ipUseOffset == TRUE)
	{
			ilStartY = imYOffset + MMY(ipStartY);
		//	imYOffset += MMY(ipEndY);
			ilEndY = ilStartY + MMY(ipEndY); 
	}
	else 
	{
		if (ipStartY == 0)
		{
			ilStartY = imYOffset;
			imYOffset += MMY(ipEndY);
			ilEndY = imYOffset;
		}
		else
		{
			ilStartY = MMY(ipStartY);
			ilEndY = MMY(ipEndY);
		}
	}

	CRect olRect(MMX(ipStartX),ilStartY,MMX(ipEndX),ilEndY);
	omCdc.DrawText(opText,olRect,DT_LEFT|DT_WORDBREAK);
	omCdc.SelectObject(polOldFont);

	return TRUE;

}
