// FidasRemarkDialog.cpp : implementation file
//

#include <stdafx.h>
#include <fidas.h>
#include <FidasRemarkDialog.h>
#include <FidasUtilities.h>
#include <CFDDoc.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFidasRemarkDialog dialog


CFidasRemarkDialog::CFidasRemarkDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CFidasRemarkDialog::IDD, pParent)
{
	EnableAutomation();

	//{{AFX_DATA_INIT(CFidasRemarkDialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	pomFieldDesc = NULL;
}


void CFidasRemarkDialog::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CDialog::OnFinalRelease();
}

void CFidasRemarkDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFidasRemarkDialog)
	DDX_Control(pDX, IDC_UFIS_REMARK_OK_BUTTON, m_OK_Button);
	DDX_Control(pDX, IDC_UFIS_REMARK_AVAILABLELIST, m_Available_List);
	DDX_Control(pDX, IDC_UFIS_REMARK_FIELDLIST, m_Fieldlist_List);
	DDX_Control(pDX, IDC_UFIS_REMARK_CHECK_SHOWTIME, m_ShowTime_Check);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFidasRemarkDialog, CDialog)
	//{{AFX_MSG_MAP(CFidasRemarkDialog)
	ON_BN_CLICKED(IDC_UFIS_REMARK_CANCEL_BUTTON, OnUfisRemarkCancelButton)
	ON_BN_CLICKED(IDC_UFIS_REMARK_OK_BUTTON, OnUfisRemarkOkButton)
	ON_BN_CLICKED(IDC_UFIS_REMARK_BUTTON_BLINKCOLOR, OnUfisRemarkButtonBlinkcolor)
	ON_BN_CLICKED(IDC_UFIS_REMARK_BUTTON_BLINKCOLOR2, OnUfisRemarkButtonBlinkcolor2)
	ON_BN_CLICKED(IDC_UFIS_REMARK_BUTTON_REMOVEFIELD, OnUfisRemarkButtonRemovefield)
	ON_BN_CLICKED(IDC_UFIS_REMARK_BUTTON_ADDFIELDTOLIST, OnUfisRemarkButtonAddfieldtolist)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CFidasRemarkDialog, CDialog)
	//{{AFX_DISPATCH_MAP(CFidasRemarkDialog)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IFidasRemarkDialog to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {FA5AF4B6-6536-11D5-8132-00010215BFDE}
static const IID IID_IFidasRemarkDialog =
{ 0xfa5af4b6, 0x6536, 0x11d5, { 0x81, 0x32, 0x0, 0x1, 0x2, 0x15, 0xbf, 0xde } };

BEGIN_INTERFACE_MAP(CFidasRemarkDialog, CDialog)
	INTERFACE_PART(CFidasRemarkDialog, IID_IFidasRemarkDialog, Dispatch)
END_INTERFACE_MAP()

void CFidasRemarkDialog::SetFieldDescription(CFieldDesc *prpFieldDesc)
{
	ASSERT(prpFieldDesc);
	pomFieldDesc = prpFieldDesc;
}


/////////////////////////////////////////////////////////////////////////////
// CFidasRemarkDialog message handlers

void CFidasRemarkDialog::OnUfisRemarkCancelButton() 
{
	// TODO: Add your control notification handler code here
	EndDialog(IDCANCEL);
	
}

void CFidasRemarkDialog::OnUfisRemarkOkButton() 
{
	// TODO: Add your control notification handler code here
	if (m_Fieldlist_List.GetCount() <= 0)
		return;

	CDpyType_B *polType = static_cast<CDpyType_B *>(pomFieldDesc->GetDpyType());
	ASSERT(polType);

	polType->SetRemarkType("A");
	polType->SetBlinkC(16);

	if (m_ShowTime_Check.GetCheck() == 1)
		polType->SetTimeConversion('T');
	else 
		polType->SetTimeConversion('b');

	CStringVector *polFieldNames = polType->GetFieldNames();
	if (polFieldNames == NULL)
		polFieldNames = new CStringVector;
	else
		polFieldNames->clear();

	ASSERT(polFieldNames);

	for (int i = 0; i < m_Fieldlist_List.GetCount(); i++)
	{
		const CFieldInfo& rolInfo = omFieldInfos.at(m_Fieldlist_List.GetItemData(i));
		polFieldNames->push_back(rolInfo.csName);
	}

	polType->SetFieldNames(polFieldNames);
	pomFieldDesc->SetDpyType(polType);

	EndDialog(IDOK);
}

void CFidasRemarkDialog::OnUfisRemarkButtonBlinkcolor() 
{
	// TODO: Add your control notification handler code here
	
}

void CFidasRemarkDialog::OnUfisRemarkButtonBlinkcolor2() 
{
	// TODO: Add your control notification handler code here
	
}

void CFidasRemarkDialog::OnUfisRemarkButtonRemovefield() 
{
	// TODO: Add your control notification handler code here
	int ind = m_Fieldlist_List.GetCurSel();
	if (ind >= 0)
	{
		m_Fieldlist_List.DeleteString(ind);
	}
	
	m_OK_Button.EnableWindow(m_Fieldlist_List.GetCount() > 0);
}

void CFidasRemarkDialog::OnUfisRemarkButtonAddfieldtolist() 
{
	// TODO: Add your control notification handler code here
	int ind = m_Available_List.GetCurSel();
	if (ind >= 0)
	{
		CString tempstr; 
		m_Available_List.GetText(ind, tempstr);
		int ind2 = m_Fieldlist_List.AddString(tempstr);
		m_Fieldlist_List.SetItemData(ind2,m_Available_List.GetItemData(ind));
	}

	m_OK_Button.EnableWindow(m_Fieldlist_List.GetCount() > 0);
}

BOOL CFidasRemarkDialog::OnInitDialog() 
{
	ASSERT(pomFieldDesc && pomFieldDesc->DpyType() && pomFieldDesc->DpyType()->GetType() == CDpyType::B);

	CDialog::OnInitDialog();
	SetWindowText(CFidasUtilities::GetString(IDS_UFIS_REMARK));



	CWnd *pWnd = GetDlgItem(IDC_UFIS_REMARK_FRAME);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_REMARK_FRAME));

	pWnd = GetDlgItem(IDC_UFIS_REMARK_FRAME1);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_REMARK_FRAME1));

	pWnd = GetDlgItem(IDC_UFIS_REMARK_FRAME2);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_REMARK_FRAME2));

	pWnd = GetDlgItem(IDC_UFIS_REMARK_BUTTON_REMOVEFIELD);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_REMARK_BUTTON_REMOVEFIELD));

	pWnd = GetDlgItem(IDC_UFIS_REMARK_BUTTON_ADDFIELDTOLIST);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_REMARK_BUTTON_ADDFIELDTOLIST));

	pWnd = GetDlgItem(IDC_UFIS_REMARK_CHECK_SHOWTIME);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_REMARK_CHECK_SHOWTIME));

	pWnd = GetDlgItem(IDC_UFIS_REMARK_OK_BUTTON);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_OK));
	
	pWnd = GetDlgItem(IDC_UFIS_REMARK_CANCEL_BUTTON);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_CANCEL));

	
	
	// TODO: Add extra initialization here
	m_Select_BlinkColor_Button.SubclassDlgItem(IDC_UFIS_REMARK_BUTTON_BLINKCOLOR, this);
	m_Select_BlinkColor_Button2.SubclassDlgItem(IDC_UFIS_REMARK_BUTTON_BLINKCOLOR2, this);
	
	const CDpyType_B *polType = static_cast<const CDpyType_B *>(pomFieldDesc->DpyType());
	ASSERT(polType);

	if(polType->GetTimeConversion() == 'T')
	{
		m_ShowTime_Check.SetCheck(1);	
	}
	else
	{
		m_ShowTime_Check.SetCheck(0);	
	}

	const CStringVector *polFieldNames = polType->FieldNames();

	CFidasUtilities::GetFieldList(pomFieldDesc->DpyType()->GetType(),"FIDTAB",omFieldInfos);
	for (int i = 0; i < omFieldInfos.size(); i++)
	{
		int ind = m_Available_List.AddString(omFieldInfos[i].csTranslation);
		ASSERT(ind != CB_ERR);
		m_Available_List.SetItemData(ind,i);
		if (polFieldNames)
		{
			for (int j = 0; j < polFieldNames->size(); j++)
			{
				if (polFieldNames->at(j) == omFieldInfos[i].csName)
				{
					ind = m_Fieldlist_List.AddString(omFieldInfos[i].csTranslation);
					m_Fieldlist_List.SetItemData(ind,i);
					break;
				}
			}
		}
	}

	m_OK_Button.EnableWindow(m_Fieldlist_List.GetCount() > 0);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
