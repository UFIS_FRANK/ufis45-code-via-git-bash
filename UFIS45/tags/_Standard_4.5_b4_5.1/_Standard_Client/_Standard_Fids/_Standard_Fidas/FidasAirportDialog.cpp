// FidasAirportDialog.cpp : implementation file
//

#include <stdafx.h>
#include <fidas.h>
#include <FidasAirportDialog.h>
#include <FidasUtilities.h>
#include <FidasSelectTableFieldDialog.h>
#include <CFDDoc.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFidasAirportDialog dialog


CFidasAirportDialog::CFidasAirportDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CFidasAirportDialog::IDD, pParent)
{
	EnableAutomation();

	//{{AFX_DATA_INIT(CFidasAirportDialog)
	omSeparator = _T("");
	imOriginOnly = -1;
	imStandard = -1;
	//}}AFX_DATA_INIT
	pomFieldDesc = NULL;
}


void CFidasAirportDialog::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CDialog::OnFinalRelease();
}

void CFidasAirportDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFidasAirportDialog)
	DDX_Control(pDX, IDC_UFIS_AIRPORT_OK_BUTTON, m_OK_Button);
	DDX_Control(pDX, IDC_UFIS_AIPORT_AVAILABLE_LIST, m_Available_List);
	DDX_Control(pDX, IDC_UFIS_AIPORT_DISPLAY_LIST, m_Display_List);
	DDX_Text(pDX, IDC_UFIS_SEPARATOR_EDIT, omSeparator);
	DDV_MaxChars(pDX, omSeparator,127);
	DDX_Radio(pDX, IDC_UFIS_AIRPORT_ONLYORIGIN_RADIO, imOriginOnly);
	DDX_Radio(pDX, IDC_UFIS_AIRPORT_STANDARD_RADIO, imStandard);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFidasAirportDialog, CDialog)
	//{{AFX_MSG_MAP(CFidasAirportDialog)
	ON_BN_CLICKED(IDC_UFIS_AIRPORT_CANCEL_BUTTON, OnUfisAirportCancelButton)
	ON_BN_CLICKED(IDC_UFIS_AIRPORT_OK_BUTTON, OnUfisAirportOkButton)
	ON_BN_CLICKED(IDC_UFIS_AIRPORT_ADD_BUTTON, OnUfisAirportAddButton)
	ON_BN_CLICKED(IDC_UFIS_AIRPORT_REMOVE_BUTTON, OnUfisAirportRemoveButton)
	ON_EN_CHANGE(IDC_UFIS_SEPARATOR_EDIT, OnChangeUfisSeparatorEdit)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CFidasAirportDialog, CDialog)
	//{{AFX_DISPATCH_MAP(CFidasAirportDialog)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IFidasAirportDialog to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {C3385C76-5A35-11D5-8124-00010215BFDE}
static const IID IID_IFidasAirportDialog =
{ 0xc3385c76, 0x5a35, 0x11d5, { 0x81, 0x24, 0x0, 0x1, 0x2, 0x15, 0xbf, 0xde } };

BEGIN_INTERFACE_MAP(CFidasAirportDialog, CDialog)
	INTERFACE_PART(CFidasAirportDialog, IID_IFidasAirportDialog, Dispatch)
END_INTERFACE_MAP()

void CFidasAirportDialog::SetFieldDescription(CFieldDesc *prpFieldDesc)
{
	ASSERT(prpFieldDesc);
	pomFieldDesc = prpFieldDesc;
}

/////////////////////////////////////////////////////////////////////////////
// CFidasAirportDialog message handlers

BOOL CFidasAirportDialog::OnInitDialog() 
{
	ASSERT(pomFieldDesc && pomFieldDesc->DpyType() && pomFieldDesc->DpyType()->GetType() == CDpyType::A);

	CDialog::OnInitDialog();
	
	// localization
	SetWindowText(CFidasUtilities::GetString(IDS_UFIS_AIRPORT));

	CWnd *pWnd = GetDlgItem(IDC_UFIS_AIRPORT_FRAME);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_AIRPORT_FRAME));

	pWnd = GetDlgItem(IDC_UFIS_AIRPORT_ONLYORIGIN_RADIO);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_AIRPORT_ONLYORIGIN_RADIO));

	pWnd = GetDlgItem(IDC_UFIS_AIRPORT_ORIGINDESTINATION_RADIO);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_AIRPORT_ORIGINDESTINATION_RADIO));

	pWnd = GetDlgItem(IDC_UFIS_AIRPORT_SEQUENCE_FRAME);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_AIRPORT_SEQUENCE_FRAME));

	pWnd = GetDlgItem(IDC_UFIS_AIRPORT_STANDARD_RADIO);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_AIRPORT_STANDARD_RADIO));

	pWnd = GetDlgItem(IDC_UFIS_AIRPORT_REVERSE_RADIO);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_AIRPORT_REVERSE_RADIO));

	pWnd = GetDlgItem(IDC_UFIS_AIRPORT_SEPARATOR_FRAME);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_AIRPORT_SEPARATOR_FRAME));

	pWnd = GetDlgItem(IDC_UFIS_SEPARATOR_STATIC);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_SEPARATOR_STATIC));

	pWnd = GetDlgItem(IDC_UFIS_AIRPORT_FIELD_FRAME);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_AIRPORT_FIELD_FRAME));

	pWnd = GetDlgItem(IDC_UFIS_AIRPORT_DISPLAY_FRAME);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_AIRPORT_DISPLAY_FRAME));

	pWnd = GetDlgItem(IDC_UFIS_AIRPORT_REMOVE_BUTTON);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_AIRPORT_REMOVE_BUTTON));

	pWnd = GetDlgItem(IDC_UFIS_AIRPORT_AVAILABLE_FRAME);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_AIRPORT_AVAILABLE_FRAME));

	pWnd = GetDlgItem(IDC_UFIS_AIRPORT_ADD_BUTTON);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_UFIS_AIRPORT_ADD_BUTTON));

	pWnd = GetDlgItem(IDC_UFIS_AIRPORT_OK_BUTTON);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_OK));
	
	pWnd = GetDlgItem(IDC_UFIS_AIRPORT_CANCEL_BUTTON);
	if (pWnd)
		pWnd->SetWindowText(CFidasUtilities::GetString(IDS_CANCEL));


	// TODO: Add extra initialization here
	imStandard	 = 0;
	imOriginOnly = 1;
	omSeparator	 = "-";

	const CDpyType_A *polType = static_cast<const CDpyType_A *>(pomFieldDesc->DpyType());
	ASSERT(polType);
	
	const CStringVector *polNames = polType->FieldNames();

	CFidasUtilities::GetFieldList(CDpyType::A,"APTTAB",omFieldInfos);
	for (int i = 0; i < omFieldInfos.size(); i++)
	{
		const CFieldInfo& rolInfo = omFieldInfos.at(i);
		int ind = m_Available_List.AddString(rolInfo.csTranslation);
		m_Available_List.SetItemData(ind,i);

		if (polNames)
		{
			for (int j = 0; j < polNames->size(); j++)
			{
				CString olName = polNames->at(j);
				if (olName.CompareNoCase(rolInfo.csName) == 0)
				{
					int ind = m_Display_List.AddString(rolInfo.csTranslation);
					m_Display_List.SetItemData(ind,i);
				}
			}
		}
	}
	
	if (polType->GetSeparator().GetLength())
		omSeparator = polType->GetSeparator();

	switch(polType->GetSequence())
	{
	case 'S' :
		imStandard = 0;
	break;
	case 'R' :
		imStandard = 1;
	break;
	}

	switch(polType->GetNAirports())
	{
	case 1 :
		imOriginOnly = 0;
	break;
	case 2 :
		imOriginOnly = 1;
	break;
	}

	UpdateData(FALSE);

	m_OK_Button.EnableWindow(m_Display_List.GetCount() > 0);	

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CFidasAirportDialog::OnUfisAirportCancelButton() 
{
	// TODO: Add your control notification handler code here
	EndDialog(IDCANCEL);
	
}

void CFidasAirportDialog::OnUfisAirportOkButton() 
{
	// TODO: Add your control notification handler code here
	if (!UpdateData(TRUE))
		return;

	CDpyType_A *polType = static_cast<CDpyType_A *>(pomFieldDesc->GetDpyType());
	ASSERT(polType);
	
	CStringVector *polNames = polType->GetFieldNames();
	if (!polNames)
		polNames = new CStringVector;
	else
	{
		polNames->clear();
	}

	ASSERT(polNames);
	for (int i = 0; i < m_Display_List.GetCount(); i++)
	{
		CString olName = omFieldInfos.at(m_Display_List.GetItemData(i)).csName;
		polNames->push_back(olName);	
	}
	polType->SetFieldNames(polNames);
	
	polType->SetSeparator(omSeparator);
	if (imStandard == 0)
		polType->SetSequence('S');
	else
		polType->SetSequence('R');

	polType->SetNAirports(imOriginOnly+1);
	pomFieldDesc->SetDpyType(polType);				

	EndDialog(IDOK);
}

void CFidasAirportDialog::OnUfisAirportAddButton() 
{
	// TODO: Add your control notification handler code here
	int ind = m_Available_List.GetCurSel();
	if (ind != LB_ERR)
	{
		CString olTempstr;
		m_Available_List.GetText(ind, olTempstr);
		int ind2 = m_Display_List.AddString(olTempstr);
		m_Display_List.SetItemData(ind2,m_Available_List.GetItemData(ind));
	}
	m_OK_Button.EnableWindow(m_Display_List.GetCount() > 0);	
}

void CFidasAirportDialog::OnUfisAirportRemoveButton() 
{
	// TODO: Add your control notification handler code here
	int ind = m_Display_List.GetCurSel();
	if (ind != LB_ERR)
	{
		m_Display_List.DeleteString(ind);
	}

	m_OK_Button.EnableWindow(m_Display_List.GetCount() > 0);	
}

void CFidasAirportDialog::OnChangeUfisSeparatorEdit() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	CString olCurrent = omSeparator;
	UpdateData(TRUE);

	if (!CFidasUtilities::IsValidSeparatorString(olCurrent,127))
	{
		TRACE ("due to internal processing, this character is forbidden!\n");
		MessageBox(CFidasUtilities::GetString(IDS_ERROR_INVALID_CHARACTER),CFidasUtilities::GetString(IDS_ERROR_TITLE),MB_ICONHAND + MB_OK);
		omSeparator = olCurrent;
		UpdateData(FALSE);
	}

}
