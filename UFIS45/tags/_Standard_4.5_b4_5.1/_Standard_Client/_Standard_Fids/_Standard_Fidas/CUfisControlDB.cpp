/*
 * AAB/AAT UFIS Display Handler
 *
 * Implementation of CUfisDB database acces layer for local file db (test)
 * j. heilig June 17 2001
 *
 */
  
#include <CUfisControlDB.h>
#include <UfisCom.h>
#include <FidasUtilities.h>

CUfisControlDB::CUfisControlDB(CUfisCom *popUfisCom)
:omFDDFields("FDD")
,omFLVFields("FLV")
,omFIDFields("FID")
,omAPTFields("APT")
,omDEVFields("DEV") /*added by JHI 05062002*/
,omFDVFields("FDV") /*added by JHI 27072002*/
{
	ASSERT(popUfisCom);
	pomUfisCom = popUfisCom;

	omFDDFields.LoadTableFields(popUfisCom);
	omFLVFields.LoadTableFields(popUfisCom);
	omFIDFields.LoadTableFields(popUfisCom);
	omAPTFields.LoadTableFields(popUfisCom);
	omDEVFields.LoadTableFields(popUfisCom);/*added by JHI 05062002*/
	omFDVFields.LoadTableFields(popUfisCom);/*added by JHI 27072002*/
}

CUfisControlDB::~CUfisControlDB()
{

}

int CUfisControlDB::GetColumns(const char *table, CStringVector &fields,CStringVector& translated) const
{
	ASSERT(table);

	CString olTable(table);
	olTable.MakeUpper();

	if (olTable.Find("TAB") < 0)
		olTable += "TAB";

	if (olTable == "FDDTAB")
	{
		omFDDFields.GetFieldNames(fields);
		omFDDFields.GetFieldDescriptions(translated);
	}
	else if (olTable == "FLVTAB")
	{
		omFLVFields.GetFieldNames(fields);
		omFLVFields.GetFieldDescriptions(translated);
	}
	else if (olTable == "FIDTAB")
	{
		omFIDFields.GetFieldNames(fields);
		omFIDFields.GetFieldDescriptions(translated);
	}
	else if (olTable == "APTTAB")
	{
		omAPTFields.GetFieldNames(fields);
		omAPTFields.GetFieldDescriptions(translated);
	}
	else if (olTable == "DEVTAB")/*added by JHI 05062002*/
	{
		omDEVFields.GetFieldNames(fields);
		omDEVFields.GetFieldDescriptions(translated);
	}
	else if (olTable == "FDVTAB")/*added by JHI 27072002*/
	{
		omFDVFields.GetFieldNames(fields);
		omFDVFields.GetFieldDescriptions(translated);
	}
	/****************************************************/
	else
	{
		return CUfisDB::GetColumns(olTable,fields,translated);
	}

	return 0;
}

int	CUfisControlDB::GetFieldInfos(const char *table,std::vector<CFieldInfo>& dst)
{
	ASSERT(table);

	CString olTable(table);
	olTable.MakeUpper();

	if (olTable.Find("TAB") < 0)
		olTable += "TAB";

	if (olTable == "FDDTAB")
	{
		omFDDFields.GetFieldInfos(dst);
	}
	else if (olTable == "FLVTAB")
	{
		omFLVFields.GetFieldInfos(dst);
	}
	else if (olTable == "FIDTAB")
	{
		omFIDFields.GetFieldInfos(dst);
	}
	else if (olTable == "APTTAB")
	{
		omAPTFields.GetFieldInfos(dst);
	}
	else if (olTable == "DEVTAB")/* added by JHI 05062002*/
	{
		omDEVFields.GetFieldInfos(dst);
	}
	else if (olTable == "FDVTAB")/* added by JHI 27072002*/
	{
		omFDVFields.GetFieldInfos(dst);
	}
	/*****************************************************/
	else
	{
		return CUfisDB::GetFieldInfos(olTable,dst);
	}
	
	return 0;
}

int CUfisControlDB::GetPEDTable(std::vector<CPEDRec> &dst)
{
	ASSERT(pomUfisCom);

	CString olTableName	= "PED"+CFidasUtilities::TableExtension();
	CString olWhere;
	int ilStat = pomUfisCom->CallServer("RT",olTableName,"SNAM,GNAM,LANG,FSTR,DSCR,FAKE","",olWhere,"240");
	CFidasUtilities::LogDebugInfo("cmd=<%s>,table=<%s>,fields=<%s>,data=<%s>,where=<%s>,stat = %d","RT",olTableName,"SNAM,GNAM,LANG,FSTR,DSCR,FAKE","",olWhere,ilStat);
	if (ilStat == 0)
	{
		for (int ilC = 0; ilC < pomUfisCom->GetBufferCount(); ilC++)
		{
			CString olBuffer(pomUfisCom->GetBufferLine(ilC));	
			TRACE("Reading line [%d] : [%s]\n",ilC,olBuffer);

			CStringArray olItems;
			if (CFidasUtilities::ExtractTextLineFast(olItems,olBuffer,",") == 6)
			{
				olItems[0].TrimRight();
				olItems[1].TrimRight();
				olItems[2].TrimRight();
				olItems[3].TrimRight();
				olItems[4].TrimRight();
				olItems[5].TrimRight();

				CPEDRec olRec(olItems[0],"","",olItems[1],olItems[2]);
				olRec.csDef  = InFilterSep(olItems[3]);
				olRec.csDesc = InFilterSep(olItems[4]);

				dst.push_back(olRec);
				CFidasUtilities::LogDebugInfo("PEDTAB record <%d> = <%s>,<%s>,<%s>,<%s>,<%s>,<%s> read",ilC,olItems[0],olItems[1],olItems[2],InFilterSep(olItems[3]),InFilterSep(olItems[4]),olItems[5]);
			}
			else
			{
				TRACE("can't split [%s]\n",olBuffer);
				CFidasUtilities::LogDebugInfo("ERROR : PEDTAB record <%d> = <%s> is not splitable",ilC,olBuffer);
				continue;
			}
		}

	}
	else
		return 1;
	return 0;
}

int CUfisControlDB::UpdatePEDRec(const CPEDRec &ropRec)
{
	ASSERT(pomUfisCom);

	CString olTableName	= "PED"+CFidasUtilities::TableExtension();
	CString olWhere;
	olWhere.Format("WHERE SNAM='%s' AND GNAM='%s'",ropRec.csName,ropRec.csGroup);

	CString olData;
	olData.Format("%s,%s,%s,%s",ropRec.csLang+" ",OutFilterSep(ropRec.csDef)+" ",OutFilterSep(ropRec.csDesc)+" ","0");

	int ilStat = pomUfisCom->CallServer("URT",olTableName,"LANG,FSTR,DSCR,FAKE",olData,olWhere,"240");
	CFidasUtilities::LogDebugInfo("cmd=<%s>,table=<%s>,fields=<%s>,data=<%s>,where=<%s>,stat = %d","RT",olTableName,"LANG,FSTR,DSCR,FAKE",olData,olWhere,ilStat);
	if (ilStat == 0)
	{
		return 0;
	}
	else
		return 1;
}

int CUfisControlDB::InsertPEDRec(const CPEDRec &ropRec)
{
	ASSERT(pomUfisCom);

	CString olTableName	= "PED"+CFidasUtilities::TableExtension();
	CString olWhere;
	CString olData;

#if	1
	// get many urnos necessary ?
	if (omUrnos.empty())
	{
		int ilStat = pomUfisCom->CallServer("GMU",olTableName,"*","100","","240");
		CFidasUtilities::LogDebugInfo("cmd=<%s>,table=<%s>,fields=<%s>,data=<%s>,where=<%s>,stat = %d","GMU",olTableName,"*","100","",ilStat);
		if (ilStat == 0)
		{
			int ilLines = pomUfisCom->GetBufferCount();
			ASSERT(ilLines == 1);
			CString olBuffer = pomUfisCom->GetBufferLine(0);
			char *polToken = strtok(olBuffer.GetBuffer(0),",");
			while(polToken)
			{
				omUrnos.push_back(polToken);
				polToken = strtok(NULL,",");
			}
			
			olBuffer.ReleaseBuffer();
		}
		else
		{
			TRACE("GMU command failed\n");
			return 1;
		}
	}

	CString olUrno = omUrnos.at(omUrnos.size()-1);
	omUrnos.pop_back();

	olData.Format("%s,%s,%s,%s,%s,%s,%s",olUrno,ropRec.csName,ropRec.csGroup,ropRec.csLang+" ",OutFilterSep(ropRec.csDef)+" ",OutFilterSep(ropRec.csDesc)+" ","0");

	int ilStat = pomUfisCom->CallServer("IRT",olTableName,"URNO,SNAM,GNAM,LANG,FSTR,DSCR,FAKE",olData,olWhere,"240");
	CFidasUtilities::LogDebugInfo("cmd=<%s>,table=<%s>,fields=<%s>,data=<%s>,where=<%s>,stat = %d","IRT",olTableName,"URNO,SNAM,GNAM,LANG,FSTR,DSCR,FAKE",olData,olWhere,ilStat);
	if (ilStat == 0)
	{
		return 0;
	}
	else
		return 1;
#else
	olData.Format("%s,%s,%s,%s,%s,%s",ropRec.csName,ropRec.csGroup,ropRec.csLang+" ",OutFilterSep(ropRec.csDef)+" ",OutFilterSep(ropRec.csDesc)+" ","0");

	if (pomUfisCom->CallServer("IBT",olTableName,"SNAM,GNAM,LANG,FSTR,DSCR,FAKE",olData,olWhere,"240") == 0)
	{
		omLastName = ropRec.csName;
		omLastGroup= ropRec.csGroup;
		return 0;
	}
	else
		return 1;
#endif
	return 0;
}

int CUfisControlDB::DeletePEDRec(const CPEDRec &ropRec)
{
	ASSERT(pomUfisCom);

	CString olTableName	= "PED"+CFidasUtilities::TableExtension();
	CString olWhere;
	olWhere.Format("WHERE SNAM='%s' AND GNAM='%s'",ropRec.csName,ropRec.csGroup);

	int ilStat = pomUfisCom->CallServer("DRT",olTableName,"","",olWhere,"240");
	CFidasUtilities::LogDebugInfo("cmd=<%s>,table=<%s>,fields=<%s>,data=<%s>,where=<%s>,stat = %d","DRT",olTableName,"","",olWhere,ilStat);
	if (ilStat == 0)
	{
		return 0;
	}
	else
	{
		return 1;
	}
}
   
// update column that the stored procs are triggered by
int CUfisControlDB::PEDTableProc(int ind)
{
	CString olTableName	= "PED"+CFidasUtilities::TableExtension();
	CString olWhere;
//	olWhere.Format("WHERE ROWNUM=1");

	CString olData;
	olData.Format("%s","0");

	int ilStat = pomUfisCom->CallServer("URT",olTableName,"FAKE",olData,olWhere,"240");
	CFidasUtilities::LogDebugInfo("cmd=<%s>,table=<%s>,fields=<%s>,data=<%s>,where=<%s>,stat = %d","URT",olTableName,"FAKE",olData,olWhere,ilStat);
	if (ilStat == 0)
	{
		return 0;
	}
	else
		return CUfisDB::PEDTableProc(ind);
}
 
// Make CLIENT string
CString CUfisControlDB::InFilterSep(const char *pin)
{
	if (pin == NULL)
		return "";

	CString olText(pin);

	CString omClientChars("\042\047\54\012\015");  // 34,39,44,10,13
	CString omServerChars("\260\261\262\264\263");  // 176,177,178,180,179

	for(int i = 0; i < omServerChars.GetLength(); i++)
	{
		olText.Replace(omServerChars[i], omClientChars[i]);
	}
	
	return olText;
}

// make CEDA string
CString CUfisControlDB::OutFilterSep(const char *pin)
{
	if (pin == NULL)
		return "";

	CString olText(pin);

	CString omClientChars("\042\047\54\012\015");  // 34,39,44,10,13
	CString omServerChars("\260\261\262\264\263");  // 176,177,178,180,179

	for(int i = 0; i < omClientChars.GetLength(); i++)
	{
		olText.Replace(omClientChars[i], omServerChars[i]);
	}
	
	return olText;
}

