//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by UfisCom.rc
//
#define IDS_UFISCOM                     1
#define IDD_ABOUTBOX_UFISCOM            1
#define IDB_UFISCOM                     1
#define IDI_ABOUTDLL                    1
#define IDS_UFISCOM_PPG                 2
#define IDS_UFISCOM_PPG_CAPTION         200
#define IDD_PROPPAGE_UFISCOM            200

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        202
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         202
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
