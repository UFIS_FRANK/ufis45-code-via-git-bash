#if !defined(AFX_UFISSOCKET_H__A2F31EA8_C74F_11D3_A251_00500437F607__INCLUDED_)
#define AFX_UFISSOCKET_H__A2F31EA8_C74F_11D3_A251_00500437F607__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UfisSocket.h : header file
//



/////////////////////////////////////////////////////////////////////////////
// UfisSocket command target

class UfisSocket : public CSocket
{
// Attributes
public:

// Operations
public:
	UfisSocket();
	virtual ~UfisSocket();

// Overrides
public:
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(UfisSocket)
	public:
	virtual void OnReceive(int nErrorCode);
	virtual int Receive(void* lpBuf, int nBufLen, int nFlags = 0);
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(UfisSocket)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

// Implementation
protected:
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UFISSOCKET_H__A2F31EA8_C74F_11D3_A251_00500437F607__INCLUDED_)
