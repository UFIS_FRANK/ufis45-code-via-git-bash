#if !defined(AFX_BC_SIMPLECTL_H__7A003564_770E_4DFB_B8C3_EA6430FE6318__INCLUDED_)
#define AFX_BC_SIMPLECTL_H__7A003564_770E_4DFB_B8C3_EA6430FE6318__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// BC_SimpleCtl.h : Declaration of the CBC_SimpleCtrl ActiveX Control class.

/////////////////////////////////////////////////////////////////////////////
// CBC_SimpleCtrl : See BC_SimpleCtl.cpp for implementation.

class BCSocket;
class CBC_SimpleCtrl : public COleControl
{
	DECLARE_DYNCREATE(CBC_SimpleCtrl)

// Constructor
public:
	CBC_SimpleCtrl();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBC_SimpleCtrl)
	public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();
	//}}AFX_VIRTUAL

// Implementation
protected:
	~CBC_SimpleCtrl();

	DECLARE_OLECREATE_EX(CBC_SimpleCtrl)    // Class factory and guid
	DECLARE_OLETYPELIB(CBC_SimpleCtrl)      // GetTypeInfo
	DECLARE_PROPPAGEIDS(CBC_SimpleCtrl)     // Property page IDs
	DECLARE_OLECTLTYPE(CBC_SimpleCtrl)		// Type name and misc status

// Message maps
	//{{AFX_MSG(CBC_SimpleCtrl)
		// NOTE - ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Dispatch maps
	//{{AFX_DISPATCH(CBC_SimpleCtrl)
	BOOL m_amIConnected;
	afx_msg void OnAmIConnectedChanged();
	CString m_bCConnectionType;
	afx_msg void OnBCConnectionTypeChanged();
	CString m_version;
	afx_msg void OnVersionChanged();
	CString m_buildDate;
	afx_msg void OnBuildDateChanged();
	afx_msg void SetFilter(LPCTSTR strTable, LPCTSTR strCommand, BOOL bOwnBC, LPCTSTR application);
	afx_msg void TransmitFilter();
	afx_msg BOOL DisconnectFromBcComServer();
	afx_msg BOOL ConnectToBcComServer();
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()

	afx_msg void AboutBox();

// Event maps
	//{{AFX_EVENT(CBC_SimpleCtrl)
	void FireOnBc(LPCTSTR ReqId, LPCTSTR Dest1, LPCTSTR Dest2, LPCTSTR Cmd, LPCTSTR Object, LPCTSTR Seq, LPCTSTR Tws, LPCTSTR Twe, LPCTSTR Selection, LPCTSTR Fields, LPCTSTR Data, LPCTSTR BcNum, LPCTSTR Attachment, LPCTSTR Additional)
		{FireEvent(eventidOnBc,EVENT_PARAM(VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR  VTS_BSTR), ReqId, Dest1, Dest2, Cmd, Object, Seq, Tws, Twe, Selection, Fields, Data, BcNum, Attachment, Additional);}
	//}}AFX_EVENT
	DECLARE_EVENT_MAP()

// Dispatch and event IDs
public:
	enum {
	//{{AFX_DISP_ID(CBC_SimpleCtrl)
	dispidAmIConnected = 1L,
	dispidBCConnectionType = 2L,
	dispidVersion = 3L,
	dispidBuildDate = 4L,
	dispidSetFilter = 5L,
	dispidTransmitFilter = 6L,
	dispidDisconnectFromBcComServer = 7L,
	dispidConnectToBcComServer = 8L,
	eventidOnBc = 1L,
	//}}AFX_DISP_ID
	};
	//CStringArray omFilter;
	long lmCurrWriteCount;
	long lmCurrFileNo;
	CString omCurrFileName;
	CMapStringToPtr mapFilter;

	BCSocket *pomBCSocket;
	BCSocket *pomBCOut; // To Send the Filter and the disconnect message a second socket is required


	char* GetKeyItem(char *pcpResultBuff, long *plpResultSize, 
					 char *pcpTextBuff, char *pcpKeyWord, 
					 char *pcpItemEnd, bool bpCopyData);
	void Message(LPCTSTR lpszMessage);
	void SendBCData(char *pclBuf);
	void BC_To_Client();
	CString m_bCQueueID;

	CStringArray omSpooler;
	bool bmTimerIsSet;
	bool bmCedaSenderConnected;
	void ShutDownSocket(CString opWho);
	void Connect();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BC_SIMPLECTL_H__7A003564_770E_4DFB_B8C3_EA6430FE6318__INCLUDED)
