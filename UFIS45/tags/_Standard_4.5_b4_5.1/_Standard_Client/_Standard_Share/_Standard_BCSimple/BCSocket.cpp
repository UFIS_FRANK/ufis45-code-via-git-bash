// BCSocket.cpp : implementation file
//

#include "stdafx.h"
#include "bccomserver.h"
#include "BCSocket.h"
#include "BC_SimpleCtl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// BCSocket

/////////////////////////////////////////////////////////////////////////////
// BCSocket

BCSocket::BCSocket(CString opWhoAmI)
{
	pomDlg = NULL;
	omWhoAmI = opWhoAmI;
	rmMsg.AllocatedSize = 1024;
	rmMsg.UsedLen = 0;
	rmMsg.Value = (char*)malloc((size_t)1024);
	rmMsg.Value[0] = '\0';

}

BCSocket::BCSocket(CString opWhoAmI, CString opFilter)
{
	pomDlg = NULL;
	omWhoAmI = opWhoAmI;
	omCurrentFilter = opFilter;
}

BCSocket::~BCSocket()
{
}


// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(BCSocket, CAsyncSocket)
	//{{AFX_MSG_MAP(BCSocket)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

/////////////////////////////////////////////////////////////////////////////
// BCSocket member functions

void BCSocket::OnReceive(int nErrorCode) 
{
	// TODO: Add your specialized code here and/or call the base class
	if (omWhoAmI == "RECEIVER")
	{
		char *pclDataBegin=NULL;
		char TmpData[1025]="";
		CString Data;
		bool GotMessage;
		char pclResult[2500]=""; 
		long Tot=0;
		long llSize=0;
		long TotalLen=0;
		long llRealLen=0;


		memset(&TmpData, 0, (size_t)1025);
		llRealLen = Receive(TmpData, 1024);
		while(llRealLen > 0)
		{
			TotalBuffer += CString(TmpData);
			memset(&TmpData, 0, (size_t)1025);
			llRealLen = Receive(TmpData, 1024);
		}
		TotalBuffer += CString(TmpData);

		GotMessage = true;
		while( GotMessage == true)
		{
			GotMessage = false;
			if( TotalBuffer.GetLength() >= 16 )
			{
				pclDataBegin = pomDlg->GetKeyItem(pclResult, &llSize, TotalBuffer.GetBuffer(0), "{=TOT=}", "{=", true); 
				if(pclDataBegin != NULL)
				{
					TotalLen = atol(pclResult);
				}
				if (TotalBuffer.GetLength() >= TotalLen)
				{
					char *pclDataBegin;
					char pclResult[250];
					long llSize;
					CString Cmd;
					CString Table;
					GotMessage = true;
					Data = TotalBuffer.Left(TotalLen);
					TotalBuffer = TotalBuffer.Mid(TotalLen);
					pclDataBegin = pomDlg->GetKeyItem(pclResult, &llSize, Data.GetBuffer(0), "{=TBL=}", "{=", true); 
					if (pclDataBegin != NULL) 
					{ 
						Table = CString(pclResult);
						if(Table == "RELDRR")
						{
							int ili = 0;
							ili = 1;
						}
					}

					pclDataBegin = pomDlg->GetKeyItem(pclResult, &llSize, Data.GetBuffer(0), "{=CMD=}", "{=", true); 
					if (pclDataBegin != NULL) 
					{ 
						Cmd  = CString(pclResult);
						if(Cmd == "SBC")
						{
							int nnn = 0;
							nnn = 2;
						}
						if(Cmd == "CLO")
						{
							this->Send("{=ACK=}", 7);
							pomDlg->Message("Sent {=ACK=} to CDRCOM due to CLO message");
							this->Close();
						}
					}
					pomDlg->SendBCData(Data.GetBuffer(0));
				}
			}
		}

	}
	if (omWhoAmI == "BCOFF")
	{
	}


	CAsyncSocket::OnReceive(nErrorCode);
}

void BCSocket::OnConnect(int nErrorCode) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	if (omWhoAmI == "RECEIVER")
	{
		CAsyncSocket::OnConnect(nErrorCode);
		lmTotalLen = 0;
		char pclS[60000]="{=TOT=}       28{=CMD=}BCOUT";
		int err = Send(pclS, 28);
	}
	if (omWhoAmI == "BCOFF")
	{
		int ilLen = pomDlg->m_bCQueueID.GetLength();
		char pclS[100]="";
		sprintf(pclS, "{=TOT=}%09d{=CMD=}BCOFF{=QUE=}%s", ilLen + 35, pomDlg->m_bCQueueID.GetBuffer(0));
		pomDlg->Message(CString(pclS));
		int err = Send(pclS, ilLen + 35);
	}
	if(omWhoAmI == "BCFILTER")
	{
		int ilLen = pomDlg->m_bCQueueID.GetLength();
		CString olFilter = omCurrentFilter;//CString("ACTTAB;ALTTAB");
		int ilFilterLen = olFilter.GetLength();
		char pclS[1000]="";
		sprintf(pclS, "{=TOT=}%09d{=CMD=}BCKEY{=QUE=}%s{=DAT=}%s", ilLen + 42 + ilFilterLen, pomDlg->m_bCQueueID.GetBuffer(0), olFilter.GetBuffer(0));
		pomDlg->Message(CString(pclS));
		int err = Send(pclS, ilLen + 42 + ilFilterLen);
	}
}
/////////////////////////////////////////////////////////////////////////////
// BCSocket member functions

void BCSocket::OnClose(int nErrorCode) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(omWhoAmI != "RECEIVER")
	{
		pomDlg->ShutDownSocket(omWhoAmI);
	}
	CAsyncSocket::OnClose(nErrorCode);
}

