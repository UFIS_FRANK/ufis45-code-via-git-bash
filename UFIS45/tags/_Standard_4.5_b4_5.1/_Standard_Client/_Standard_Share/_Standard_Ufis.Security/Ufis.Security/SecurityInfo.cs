﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ufis.Security
{
    /// <summary>
    /// Represents the class for additional information on the ufis security class.
    /// </summary>
    public class SecurityInfo
    {
        /// <summary>
        /// Gets or sets the remaining days for user to change password.
        /// </summary>
        public int RemainingDaysToChangePassword { get; set; }
        /// <summary>
        /// Gets or sets the number of inactive days allowed.
        /// </summary>
        public int InactiveDaysAllowed { get; set; }
        /// <summary>
        /// Gets or sets the additional error message.
        /// </summary>
        public string AdditionalErrorMessage { get; set; }
    }
}
