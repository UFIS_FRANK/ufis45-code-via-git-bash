﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ufis.Security
{
    /// <summary>
    /// Enumeration for user access right.
    /// </summary>
    public enum UserAccessRight
    {
        Activated,
        Deactivated,
        Hidden,
        Unspecified
    }
}
