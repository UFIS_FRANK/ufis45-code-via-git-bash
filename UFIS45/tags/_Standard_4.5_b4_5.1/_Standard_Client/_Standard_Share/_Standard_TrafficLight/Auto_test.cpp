// Auto_test.cpp : implementation file
//

#include <stdafx.h>
#include <afxwin.h>
#include <TrafficLight.h>
#include <Amp_testCtl.h>
#include <Auto_test.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Auto_test

IMPLEMENT_DYNCREATE(Auto_test, CCmdTarget)

Auto_test::Auto_test()
{
	
	EnableAutomation();
	m_pCtrl = NULL;
	AfxOleLockApp();
}

Auto_test::~Auto_test()
{
	AfxOleUnlockApp();

}


void Auto_test::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.
	m_pCtrl = NULL;
	CCmdTarget::OnFinalRelease();
}

void Auto_test::SetControl (CAmp_testCtrl* pCtrl)
{
	m_pCtrl = pCtrl;
}



BOOL Auto_test::OnBroadcast (LPCTSTR ReqId, 
							 LPCTSTR Dest1, 
							 LPCTSTR Dest2, 
							 LPCTSTR Cmd, 
							 LPCTSTR Object,
							 LPCTSTR Seq, 
							 LPCTSTR Tws, 
							 LPCTSTR Twe, 
							 LPCTSTR Selection, 
							 LPCTSTR Fields, 
							 LPCTSTR Data, 
							 LPCTSTR BcNum)
{
	
	CString strcmd = "TIME";
	m_pCtrl->SetSomeBc (true);
	if (Cmd == strcmd && !(m_pCtrl->GetWait ()))	// is this the broadcast to my request ?
	{	
		if (m_pCtrl->GetSent () )
		{
			m_pCtrl->Setreceived (true);
			
		}
	}
	return TRUE;

}


BEGIN_MESSAGE_MAP(Auto_test, CCmdTarget)
	//{{AFX_MSG_MAP(Auto_test)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(Auto_test, CCmdTarget)
	//{{AFX_DISPATCH_MAP(Auto_test)
		DISP_FUNCTION_ID(Auto_test,"OnBcReceive",1,OnBroadcast, VT_BOOL, VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR 
						VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR)  
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IAuto_test to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {482A4E86-92ED-11D5-996A-0000865098D4}
static const IID IID_IAuto_test =
{ 0x482a4e86, 0x92ed, 0x11d5, { 0x99, 0x6a, 0x0, 0x0, 0x86, 0x50, 0x98, 0xd4 } };
static const IID IID_IBcPrxyEvents = 
{0x68e25ff2, 0x6790, 0x11d4, {0x90, 0x3b, 0x0, 0x01, 0x02, 0x04, 0xaa, 0x51}};


BEGIN_INTERFACE_MAP(Auto_test, CCmdTarget)
	INTERFACE_PART(Auto_test, IID_IBcPrxyEvents, Dispatch)
	INTERFACE_PART(Auto_test, IID_IAuto_test, Dispatch)
END_INTERFACE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Auto_test message handlers

//DEL void Auto_test::StartBCAmpel() 
//DEL {
//DEL 	m_pCtrl->StartTlc();
//DEL 	
//DEL }


