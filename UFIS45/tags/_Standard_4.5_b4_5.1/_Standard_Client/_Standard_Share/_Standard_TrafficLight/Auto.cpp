// Auto.cpp : implementation file
//

#include <stdafx.h>
#include <amp_test.h>
#include <Auto.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Auto

IMPLEMENT_DYNCREATE(Auto, CCmdTarget)

Auto::Auto()
{
	EnableAutomation();
}

Auto::~Auto()
{
}


void Auto::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CCmdTarget::OnFinalRelease();
}


BEGIN_MESSAGE_MAP(Auto, CCmdTarget)
	//{{AFX_MSG_MAP(Auto)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(Auto, CCmdTarget)
	//{{AFX_DISPATCH_MAP(Auto)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IAuto to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {6B582D91-9241-11D5-9969-0000865098D4}
static const IID IID_IAuto =
{ 0x6b582d91, 0x9241, 0x11d5, { 0x99, 0x69, 0x0, 0x0, 0x86, 0x50, 0x98, 0xd4 } };

BEGIN_INTERFACE_MAP(Auto, CCmdTarget)
	INTERFACE_PART(Auto, IID_IAuto, Dispatch)
END_INTERFACE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Auto message handlers
