#if !defined(AFX_AMP_TESTPPG_H__59BEC168_923C_11D5_9969_0000865098D4__INCLUDED_)
#define AFX_AMP_TESTPPG_H__59BEC168_923C_11D5_9969_0000865098D4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// Amp_testPpg.h : Declaration of the CAmp_testPropPage property page class.

////////////////////////////////////////////////////////////////////////////
// CAmp_testPropPage : See Amp_testPpg.cpp.cpp for implementation.

class CAmp_testPropPage : public COlePropertyPage
{
	DECLARE_DYNCREATE(CAmp_testPropPage)
	DECLARE_OLECREATE_EX(CAmp_testPropPage)

// Constructor
public:
	CAmp_testPropPage();

// Dialog Data
	//{{AFX_DATA(CAmp_testPropPage)
	enum { IDD = IDD_PROPPAGE_AMP_TEST };
	int		m_iTimeRed;
	int		m_iTimeYellow;
	BOOL	m_bNotify;
	long	m_Interval;
	//}}AFX_DATA

// Implementation
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Message maps
protected:
	void OnDefaults();
	//{{AFX_MSG(CAmp_testPropPage)
		// NOTE - ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AMP_TESTPPG_H__59BEC168_923C_11D5_9969_0000865098D4__INCLUDED)
