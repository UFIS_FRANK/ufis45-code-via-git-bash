#if !defined(AFX_AMP_TESTCTL_H__59BEC166_923C_11D5_9969_0000865098D4__INCLUDED_)
#define AFX_AMP_TESTCTL_H__59BEC166_923C_11D5_9969_0000865098D4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <bcproxy.h>
#include <dufiscom.h>

#define MAX_DELAY_TIME 1000 

friend class Auto_test;
// Amp_testCtl.h : Declaration of the CAmp_testCtrl ActiveX Control class.

/////////////////////////////////////////////////////////////////////////////
// CAmp_testCtrl : See Amp_testCtl.cpp for implementation.

class CAmp_testCtrl : public COleControl
{
	DECLARE_DYNCREATE(CAmp_testCtrl)

// Constructor
public:
	CAmp_testCtrl();
	void SetTlc(short sColor,bool refresh);
	
	
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAmp_testCtrl)
	public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();
	//}}AFX_VIRTUAL

// Implementation
protected:
	//BOOL GetNote ();
	//void SetNote (bool notify);
	void Calculate(long duration);
	void SendRequest();
	bool StartUfisCom();
	bool StartBc();
	~CAmp_testCtrl();
	CBitmap bitmapTlc; // contains actual picture of traffic light
	CBitmap bitmapGreen,bitmapYellow,bitmapRed;// contain pictures of the tree different states
	CBitmap* pActBitmap;// points to actual bitmap (green,yellow,red)
	CBitmap* pOldBitmap;// storage pointer
	
	Auto_test* pomAuto_test;// pointer to automation class 
	
	short smTlcState; //State (color) of traffic light
	
	int imTry;				// counter for timer events
	bool bmSomething_wrong;	// indicator for trouble
	bool bmSent;			//indicator for sent request
	bool bsomebc;			// indicator for any broadcast received
	bool bmReceived;		// indicator for received SBC broadcast
	long lmElapsedTime;		// time span between request and broadcast (gets raised by Timer IDT_TIMER2)
	// NOTE : lmElapsedTime is raised +1 every time, the IDT_TIMER2 event is fired. So if the duration of the timer
	// is raised to xx millisecs. , the count of lmElapsedTime is still +1 but is now worth xx millisecs.

	bool bmWait;//indicates if container wants to switch to a special color -> if so, don�t send requests
	// till the container releases the traffic light
	
	IUnknown* pIUnknown_Auto;
	IDispatch* pmIDispBcPrxy;
	IDispatch* pmIDispUfisCom;
	
	DWORD m_dwCookie;				// used for AfxConnectionAdvise(..)
	
// container params for the propertypage
	BOOL bmPropertyNote;//indicates if an additional warning message is desired
	
	long lmPropertyAppearance;// indicates the graphical appearance of the tlc
	long imPropTimeYellow;
	long imPropTimeRed;
	long lmPropIntervall;

	DECLARE_OLECREATE_EX(CAmp_testCtrl)    // Class factory and guid
	DECLARE_OLETYPELIB(CAmp_testCtrl)      // GetTypeInfo
	DECLARE_PROPPAGEIDS(CAmp_testCtrl)     // Property page IDs
	DECLARE_OLECTLTYPE(CAmp_testCtrl)		// Type name and misc status

// Message maps
	//{{AFX_MSG(CAmp_testCtrl)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Dispatch maps
	//{{AFX_DISPATCH(CAmp_testCtrl)
	CString m_buildDate;
	afx_msg void OnBuildDateChanged();
	CString m_version;
	afx_msg void OnVersionChanged();
	afx_msg void SetDispatch(LPDISPATCH FAR pIDispatch);
	afx_msg long Init();
	afx_msg void SetDispBc(LPDISPATCH  FAR pDispBc);
	afx_msg BOOL SetColor(short sColor);
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	afx_msg long GetInterval();
	afx_msg void SetInterval(long lNewTime);	
	afx_msg BOOL GetNote();
	afx_msg void SetNote(BOOL bNewValue);
	afx_msg long GetYellowTime();
	afx_msg void SetYellowTime(long lNewTime);
	afx_msg long GetRedTime();
	afx_msg void SetRedTime(long lNewTime);
	afx_msg long GetGraphicState();
	afx_msg void SetGraphicState(long lNewState);

	afx_msg void AboutBox();

// Event maps
	//{{AFX_EVENT(CAmp_testCtrl)
	void FireTime(long ltime)
		{FireEvent(eventidTime,EVENT_PARAM(VTS_I4), ltime);}
	void FireClick()
		{FireEvent(DISPID_CLICK,EVENT_PARAM(VTS_NONE));}
	//}}AFX_EVENT
	DECLARE_EVENT_MAP()


// Dispatch and event IDs
public:
	void SetSomeBc(bool bstate);
	bool GetWait();
	bool GetSent();
	void Setreceived(bool state);
	
	enum {
	//{{AFX_DISP_ID(CAmp_testCtrl)
	dispidBuildDate = 1L,
	dispidVersion = 2L,
	dispidSetDispatch = 3L,
	dispidInit = 4L,
	dispidSetDispBc = 5L,
	dispidSetColor = 6L,
	eventidTime = 1L,
	//}}AFX_DISP_ID
	};
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AMP_TESTCTL_H__59BEC166_923C_11D5_9969_0000865098D4__INCLUDED)
