// CedaAPTData.h: interface for the CedaAPTData class.

#ifndef _CEDAAPTDATA_H_
#define _CEDAAPTDATA_H_
 
#include <afxtempl.h>
#include <CCSCedaData.h>


struct APTDATA
{
	//values from seatab
	long	 Urno;
	char 	 Apc3[5];
	char 	 Apc4[5];
	char 	 Tdis[5];
	char 	 Tdiw[5];

	//local timediff for a winter or summer seaason
	CTimeSpan LocalDiffSummer;
	CTimeSpan LocalDiffWinter;

	APTDATA(void) 
	{
		memset(this,'\0',sizeof(*this));
		Urno = 0;
	}

};


/////////////////////////////////////////////////////////////////////////////
// Class declaration

//	!!!ONLY APC4 IS USED!!!

class CedaAPTDataUtcLoc: public CCSCedaData
{
// Attributes
private:
    CCSPtrArray<APTDATA> omData;
	CMapStringToPtr omApc4Map; //"APC4"->APTDATA
	char pcmFlist[1000];

// Operations
public:
    CedaAPTDataUtcLoc();
 	~CedaAPTDataUtcLoc();

	// default TAB is used
	void SetTableExtension(CString opExtension);

	//pspWhere "" reads the whole SEATAB.
	bool Read(char *pspWhere, char *pcpFieldList = NULL);

	//gets a struct for opApc4a("APC4").
	APTDATA* FindApt4(CString opApc4);

	//converts the optime from uct to local for summer or winter season at APC4
	bool Apt4UtcToLocal(CTime &opTime, const CString &opApc4, bool bpWinter = true);

	//converts the optime from local to utc for summer or winter season at APC4
	bool Apt4LocalToUtc(CTime &opTime, const CString &opApc4, bool bpWinter = true);

private:
	void ClearAll(void);

	// converts char Tdis,Tdiw to CTimeSpan LocalDiffSummer,LocalDiffWinter
	bool ConvertToTimeSpan(APTDATA *prlApt);

};

#endif
