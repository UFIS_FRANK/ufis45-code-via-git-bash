// AatHelp.cpp: implementation of the AatHelp class.
//
//////////////////////////////////////////////////////////////////////

#include <stdafx.h>
#include <AatHelp.h>
#include <HtmlHelp.h>
#include <CedaObject.h>
#include <CedaSystabData.h>

#pragma comment(lib,"HtmlHelp.lib")

typedef struct tagHH_LAST_ERROR
{
     int      cbStruct ;
     HRESULT  hr ;            
     BSTR     description ;      
} HH_LAST_ERROR ;


//////////////////////////////////////////////////////////////////////
// Class members
//////////////////////////////////////////////////////////////////////
AatHelp*	AatHelp::pomThis = NULL;
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

AatHelp::AatHelp(const CString& ropAppName)
{
	pomThis = this;

	CString olDummy;

	omState = OK; 

	CWnd *pWnd = AfxGetMainWnd();
	if (pWnd)
		hmMainWnd = pWnd->m_hWnd;
	else
		hmMainWnd = NULL;	

	dmCookie = 0;
	HtmlHelp(NULL,NULL,HH_INITIALIZE,(DWORD)&dmCookie);

	char pclTmpText[512];
	char pclConfigPath[512];
	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString("GLOBAL", "HELPDIRECTORY", "c:\\Ufis\\Help",pclTmpText,sizeof pclTmpText,pclConfigPath);
	omFileName = pclTmpText;												
	if (omFileName.IsEmpty())
	{
		omState = HELPDIRINVALID;
	}
	else
	{
		GetPrivateProfileString(ropAppName, "HELPFILE", ropAppName + ".chm",pclTmpText,sizeof pclTmpText,pclConfigPath);
		if (omFileName.IsEmpty())
		{
			omState = HELPFILEINVALID;
		}
		else
		{
			if (omFileName[omFileName.GetLength() - 1] != '\\')
				omFileName += '\\';
			omFileName += pclTmpText;
		}
	}
}

AatHelp::~AatHelp()
{
	HtmlHelp(NULL,NULL,HH_UNINITIALIZE,dmCookie);
}

void AatHelp::ExitApp()
{
	delete pomThis;
	pomThis = NULL;

	delete CedaObject::pomSystab;
	CedaObject::pomSystab = NULL;
}

//////////////////////////////////////////////////////////////////////
// Operations
//////////////////////////////////////////////////////////////////////

void AatHelp::WinHelp(DWORD dwData,UINT nCmd)
{
	if (!pomThis || pomThis->omState != OK && pomThis->omState < FAILED)
		return;

	switch(nCmd)
	{
	case HELP_COMMAND:
		;
	break;
	case HELP_CONTENTS:
		HtmlHelp(pomThis->hmMainWnd,pomThis->omFileName,HH_DISPLAY_TOC,dwData);
	break;
	case HELP_CONTEXT:
		HtmlHelp(pomThis->hmMainWnd,pomThis->omFileName,HH_DISPLAY_TOC,NULL);
//!!	HtmlHelp(pomThis->hmMainWnd,pomThis->omFileName,HH_HELP_CONTEXT,dwData);
	break;
	case HELP_CONTEXTMENU:
		HtmlHelp(pomThis->hmMainWnd,pomThis->omFileName,HH_TP_HELP_CONTEXTMENU,dwData);
	break;
	case HELP_CONTEXTPOPUP:
		HtmlHelp(pomThis->hmMainWnd,pomThis->omFileName,HH_HELP_CONTEXT,dwData);
	break;
	case HELP_FINDER:
		HtmlHelp(pomThis->hmMainWnd,pomThis->omFileName,HH_HELP_FINDER,dwData);
	break;
	case HELP_FORCEFILE:
		;
	break;
	case HELP_KEY:
		HtmlHelp(pomThis->hmMainWnd,pomThis->omFileName,HH_DISPLAY_TOPIC,dwData);
	break;
	case HELP_MULTIKEY:
		;
	break;
	case HELP_PARTIALKEY:
		;
	break;
	case HELP_QUIT:
		HtmlHelp(NULL,NULL,HH_CLOSE_ALL,0);
	break;
	case HELP_SETCONTENTS:
		;
	break;
	case HELP_SETPOPUP_POS:
		;
	break;
	case HELP_SETWINPOS:
		;
	break;
	case HELP_TCARD:
		;
	break;
	case HELP_WM_HELP:
		HtmlHelp(pomThis->hmMainWnd,pomThis->omFileName,HH_TP_HELP_WM_HELP,dwData);
	break;
	}

	CString olError;
	if (GetLastError(olError) != OK)
	{
		MessageBox(NULL,olError,"Error",MB_OK);
	}
}

//////////////////////////////////////////////////////////////////////
// Helpers
//////////////////////////////////////////////////////////////////////

BOOL AatHelp::Initialize(const CString& ropAppName)
{
	if (pomThis == NULL)
	{
		pomThis = new AatHelp(ropAppName);
	}

	if (!pomThis || pomThis->omState != OK)		
	{
		return FALSE;
	}

	return TRUE;
}

AatHelp::State AatHelp::GetLastError(CString& ropError)
{
	if (pomThis == NULL)
	{
		return NOT_INITIALIZED;		
	}
	else if (pomThis->omState != OK)
	{
		return pomThis->omState;
	}

	HH_LAST_ERROR lasterror;

	HWND hwnd = HtmlHelp(pomThis->hmMainWnd,NULL,HH_GET_LAST_ERROR,reinterpret_cast<DWORD>(&lasterror));

	// Make sure that HH_GET_LAST_ERROR succeeded.
	if (hwnd != 0)
	{
		// Only report an error if we found one:
		if (FAILED(lasterror.hr))
		{
			// Is there a text message to display...
			if (lasterror.description)
			{
				pomThis->omState = FAILED;
				// Convert the String to ANSI
				ropError = lasterror.description;
				::SysFreeString(lasterror.description) ;
			}
		}
	}
	

	return pomThis->omState;
}
