// RecordSet.h: interface for the RecordSet class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_RECORDSET_H__28E63D63_971E_11D1_8371_0080AD1DC701__INCLUDED_)
#define AFX_RECORDSET_H__28E63D63_971E_11D1_8371_0080AD1DC701__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class RecordSet  
{
public:
	RecordSet(int ipCount = 0);
	RecordSet(CString &opDataList, int ipCount = 0);
	RecordSet(CStringArray &opData, int ipCount = 0);

	virtual ~RecordSet();

	CStringArray Values;

	CStringArray *pSortValues;

	DWORD UseBy;
	DWORD TmpUse;

	int Status;


	RecordSet(const RecordSet& s);
	

	const RecordSet& operator= ( const RecordSet& s);

	CString& operator[] ( int Index );

	int GetStatus(){return Status;};

};

#endif // !defined(AFX_RECORDSET_H__28E63D63_971E_11D1_8371_0080AD1DC701__INCLUDED_)
