// FlightData.h: interface for the FlightData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_FLIGHTDATA_H__880DFE91_11BB_11D2_8572_0000C04D916B__INCLUDED_)
#define AFX_FLIGHTDATA_H__880DFE91_11BB_11D2_8572_0000C04D916B__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000


//class RecordSet;
//class FieldSet;
//class ModulData;



#include <CCSPtrArray.h>
#include <CCSBcHandle.h>
#include <CCSBasic.h>
#include <RecordSet.h>
#include <FieldSet.h>
#include <ModulData.h>
#include <CCSDdx.h>


class FlightData : public CCSCedaData  
{
public:
	FlightData(CCSDdx *popDdx);

	virtual ~FlightData();

	void Init(CString opTableExtension);

	void AddModul(DWORD dwModul, CString opFields);

	void RemoveModul(DWORD dwModul);

	ModulData *GetModul(DWORD dwModul);


	int  Read(DWORD dwModul, CString opSelection, bool bpClearAll = false);

	int GetFieldIndex(CString opField);

	static CUIntArray smIndex;
	static int CompareRecord(const RecordSet **e1, const RecordSet **e2);

	const RecordSet * GetFlightByUrno(CString opUrno);


	bool AddToKeyMap(RecordSet *prpRecord );

	CCSPtrArray<RecordSet> Data;
	CCSPtrArray<FieldSet>  Desc;
	CCSPtrArray<ModulData> Modul;

	CMapStringToPtr omIndexMap;
	CMapPtrToPtr omModulMap;

	int FieldCount;

	CString omTableExtension;

	CMapStringToPtr omRkeyMap;

	CMapStringToPtr omUrnoMap;

	RecordSet *GetDeparture(CString opRkey);
	RecordSet *GetArrival(CString opRkey);


	bool AddFlightInternal(RecordSet *prpRecord , DWORD dwModul);

	bool FillRecord(RecordSet *prpRecord , CString &opData, CString &opFieldList);

	void DelFromKeyMap(RecordSet *prpRecord );

	void Clear(DWORD dwModul);

	CCSDdx *pomDdx;	

	void  ProcessFlightBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	void UnRegister(void);
	void Register(void);


};

#endif // !defined(AFX_FLIGHTDATA_H__880DFE91_11BB_11D2_8572_0000C04D916B__INCLUDED_)
