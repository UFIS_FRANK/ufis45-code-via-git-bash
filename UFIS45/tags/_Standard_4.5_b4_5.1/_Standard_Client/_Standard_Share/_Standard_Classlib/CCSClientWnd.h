// ccsclientwnd.h : header file
//

#ifndef _CCSCLIENTWND_
#define _CCSCLIENTWND_

#include <afxwin.h>

/////////////////////////////////////////////////////////////////////////////
// CCSClientWnd window

class CCSClientWnd : public CWnd
{
// Construction
public:
    CCSClientWnd();

// Attributes
public:

// Operations
public:

// Overrides
public:

// Implementation
public:
    virtual ~CCSClientWnd();

    // Generated message map functions
protected:
    //{{AFX_MSG(CCSClientWnd)
    afx_msg BOOL OnEraseBkgnd(CDC* pDC);
    afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()

private:
    static COLORREF lmBkColor;
    static COLORREF lmTextColor;
    static COLORREF lmHilightColor;
};

/////////////////////////////////////////////////////////////////////////////


#endif // CCSCLIENTWND
