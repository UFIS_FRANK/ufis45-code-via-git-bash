// BcHandle.cpp : implementation file
//

#include <stdafx.h>
#include <ccsptrarray.h>
#include <ufis.h>
#include <CCSBcHandle.h>
#include <CCSBasicFunc.h>

/////////////////////////////////////////////////////////////////////////////
// BcHandle

#define NOTUSED -1

/********** NEW ****/
//#define DEFAULT_BC_FN		"C:\\TMP\\BCDATA.DAT"   
#define FIRSTBC				20000     
#define PACKET_LEN     			(1024)  
#define MAXBCNUM  30000
/********** NEW ****/


static bool bgIsInReRead = false;
static bool bgNoMoreGetBc = false;
static bool bgExtendLog = false;
static short sgMaxBcNum = MAXBCNUM;
static FILE *rgOutFile = NULL;
static CString ogBcDataDatFile = CCSLog::GetTmpPath("\\BCDATA.DAT");

CCSBcHandle::CCSBcHandle(CCSDdx *popDdx, CCSCedaCom *popCommHandler, CCSLog *popLog)
{
	pomDdx = popDdx;
	pomLog = popLog;
	pomCommHandler = popCommHandler;

	bmBcActiv = true;

	bmInit = false;
	lmLastBcNum = -1L; 

	pomMsgDlg = NULL;

	pcmHomeAirport[0]='\0';

	*pcmBcLogfile = '\0';

//	omCloMessage = CString("Der Server wurde umgeschaltet!\nSie k�nnen die Applikation in einer Minute wieder starten.");
	SetCloMessage ( "The server had switched. \nYou can restart the application in one minute." );

	/******* NEW ****/
	rmBcData.DataBuffer = (char *)malloc(12*(PACKET_LEN+10));
	rmBcData.DataBufferSize = (12*(PACKET_LEN+10));
	/******* NEW ****/

	//MWO/RRO 24.03.2003
	CCSCedaData::smBcProxy = NULL;
	prlBcCCmdTarget = NULL;
	TrafficLightCallBack = NULL;
	//END MWO/RRO

	bmApplCheck = false;

	char pclConfigPath[256];
	char pclTmpBuf[256];
	
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));


    ::GetPrivateProfileString("GLOBAL", "BCCHECKAPPLICATION", "NO", pclTmpBuf, sizeof pclTmpBuf, pclConfigPath);
	if (stricmp(pclTmpBuf,"YES") == 0)
	{
		bmApplCheck = true;
	}

	pimBCState = NULL;

}
//******************************************************
// Date: 24.03.2003
// Author:	MWO/RRO
// Set indicator to use BcProxy for BC-Handling
//******************************************************
void CCSBcHandle::UseBcProxy()
{
	prlBcCCmdTarget = new BcCCmdTarget(this);
}

void CCSBcHandle::UseBcCom()
{
	prlBcCCmdTarget = new BcCCmdTarget(this);
}

CCSBcHandle::~CCSBcHandle()
{
	CCSPtrArray<BcTableHandler> *prlTableHandlers;
	POSITION pos;
	CString olTmp;
	for( pos = TableKeyMap.GetStartPosition(); pos != NULL; )
	{
		TableKeyMap.GetNextAssoc( pos, olTmp, (void *&)prlTableHandlers );
		prlTableHandlers->RemoveAll();
		delete prlTableHandlers;
	}

	omTableHandlers.DeleteAll();
	if (rmBcData.DataBuffer)			//  hag990818
	{
		free (rmBcData.DataBuffer);		//  hag990818
	}
}


void CCSBcHandle::SetCloMessage(CString opText)
{ 
	char pclConfigPath[256];
	char pclTmpBuf[256];
	
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

    ::GetPrivateProfileString("GLOBAL", "CLO_BC_MSG", "", pclTmpBuf, sizeof pclTmpBuf, pclConfigPath);
	if ( pclTmpBuf[0] != '\0' )
	{
		omCloMessage = pclTmpBuf;
	}
	else
	{
		omCloMessage = opText;
	}
}

void CCSBcHandle::ExitApp()
{
	//MWO/RRO 24.03.2003
	if (prlBcCCmdTarget != NULL)
	{
		//BcCCmdTarget::~BcCCmdTarget();
		//prlBcCCmdTarget->BcCCmdTarget::~BcCCmdTarget();
		prlBcCCmdTarget->ExitApp();
		delete prlBcCCmdTarget;
		prlBcCCmdTarget = NULL;
	}
	//END MWO/RRO 24.03.2003
}


void CCSBcHandle::StartBc(void)
{
	//this was already there
	bmBcActiv = true;
	//MWO/RRO 25.03.2003
	if (prlBcCCmdTarget == NULL)
	{
		GetBc(::GetActualBcNum());
		DataChangeBc();
	}
	else
	{
		prlBcCCmdTarget->SetSpoolOff();
	}
	//END MWO/RRO 24.03.2003
}



void CCSBcHandle::StopBc(void)
{
	//this was already there
	bmBcActiv = false;
	//MWO/RRO 25.03.2003
	if (prlBcCCmdTarget != NULL)
	{
		prlBcCCmdTarget->SetSpoolOn();
	}
	//END MWO/RRO 25.03.2003
}


void CCSBcHandle::AddTableCommand(CString opTable, CString opCommand, int ipDDXType,bool bpUseOwnBc /* = false */,CString opAppl /* = "" */)
{
	BcTableHandler *prlTableHandler = new BcTableHandler;
	prlTableHandler->Tablename = opTable;
	prlTableHandler->CedaCommand = opCommand;
	prlTableHandler->DdxType = ipDDXType;
	prlTableHandler->UseOwnBc = bpUseOwnBc;
	prlTableHandler->OnlyOwnBc = false;
	prlTableHandler->Applname = opAppl;

	omTableHandlers.Add(prlTableHandler);
	AddToKeyMap(prlTableHandler);

	//MWO/RRO 25.03.2003
	if (prlBcCCmdTarget != NULL)
	{
		prlBcCCmdTarget->AddTableCommand(opTable, opCommand,bpUseOwnBc,opAppl);
	}
	//END MWO/RRO 25.03.2003


}


void CCSBcHandle::RemoveTableCommand(CString opTable, CString opCommand, int ipDDXType)
{
	for(int i = omTableHandlers.GetSize() - 1; i >= 0; i--)
	{
		if((omTableHandlers[i].Tablename == opTable) &&
		   (omTableHandlers[i].CedaCommand == opCommand) &&
		   (omTableHandlers[i].DdxType == ipDDXType))
		{
			DelFromKeyMap(&omTableHandlers[i]);
			omTableHandlers.DeleteAt(i);
			break;
		}
	}

	//MWO/RRO 25.03.2003
	if (prlBcCCmdTarget != NULL)
	{
		prlBcCCmdTarget->RemoveTableCommand (opTable, opCommand);
	}
	//END MWO/RRO 25.03.2003
}


void CCSBcHandle::AddToKeyMap(BcTableHandler *prlTableHandler)
{
	CCSPtrArray<BcTableHandler> *prlTableHandlers;

	if(TableKeyMap.Lookup( prlTableHandler->Tablename,(void *& )prlTableHandlers) == TRUE)
	{
		for(int i = prlTableHandlers->GetSize() - 1; i >= 0; i--)
		{
			if ((*prlTableHandlers)[i].CedaCommand == prlTableHandler->CedaCommand)
			{
				prlTableHandlers->RemoveAt(i);
				break;
			}
		}
		prlTableHandlers->Add(prlTableHandler);
	}
	else
	{
		prlTableHandlers = new CCSPtrArray<BcTableHandler>;
		prlTableHandlers->Add(prlTableHandler);
		TableKeyMap.SetAt(prlTableHandler->Tablename,prlTableHandlers);
	}

}

void CCSBcHandle::DelFromKeyMap(BcTableHandler *prlTableHandler)
{
	CCSPtrArray<BcTableHandler> *prlTableHandlers;

	if(TableKeyMap.Lookup( prlTableHandler->Tablename,(void *& )prlTableHandlers) == TRUE)
	{
		for(int i = prlTableHandlers->GetSize() - 1; i >= 0; i--)
		{
			if ((*prlTableHandlers)[i].CedaCommand == prlTableHandler->CedaCommand)
			{
				prlTableHandlers->RemoveAt(i);
				break;
			}
		}
		if(prlTableHandlers->GetSize() == 0)
		{
			TableKeyMap.RemoveKey( prlTableHandler->Tablename);
			delete prlTableHandlers;
		}
	}
}

void CCSBcHandle::TableCommandSetOnlyOwn(CString opTable, CString opCommand, int ipDDXType, bool bpSet)
{
	for(int i = omTableHandlers.GetSize() - 1; i >= 0; i--)
	{
		if((omTableHandlers[i].Tablename == opTable) &&
		   (omTableHandlers[i].CedaCommand == opCommand) &&
		   (omTableHandlers[i].DdxType == ipDDXType))
		{
			omTableHandlers[i].OnlyOwnBc = bpSet;
			break;
		}
	}
}

bool CCSBcHandle::DistributeBc(BcStruct &rrpBcData)
{
	if(strcmp(rrpBcData.Cmd, "CLO") == 0)
	{
		pomCommHandler->CleanUpCom();

		::MessageBox(NULL, omCloMessage, "", (MB_OK|MB_SYSTEMMODAL|MB_ICONEXCLAMATION));

		CCSBcHandle::ExitApp();

		ExitProcess(0);
	}


	int ilDDXType = NOTUSED;
	
	char pclTable[10]="";
	char pclCommand[10]="";
	char pclHomeAirport[10]="";
	strncpy(pclTable, rrpBcData.Object,6);
	strncpy(pclCommand, rrpBcData.Cmd,3);
	pclCommand[3] = '\0';
	pclTable[6]='\0';

	strncpy(pclHomeAirport, rrpBcData.Tws,3);
	pclHomeAirport[3] = '\0';
	

	if(pcmHomeAirport[0] != '\0')
	{
		if(strcmp(pclHomeAirport , pcmHomeAirport) != 0)
			return true;
	}

	CCSPtrArray<BcTableHandler> *prlTableHandlers;

	if(TableKeyMap.Lookup( pclTable,(void *& )prlTableHandlers) == TRUE)
	{
		for(int i = prlTableHandlers->GetSize() - 1; i >= 0; i--)
		{
			if ((*prlTableHandlers)[i].CedaCommand == CString(pclCommand))
			{
				if((*prlTableHandlers)[i].OnlyOwnBc)
				{
					if(strcmp(pomCommHandler->pcmReqId,rrpBcData.Dest2) == 0)
					{
						ilDDXType = (*prlTableHandlers)[i].DdxType;
						break;
					}
				}
				else
				{
					if ((*prlTableHandlers)[i].UseOwnBc == true)
					{
						ilDDXType = (*prlTableHandlers)[i].DdxType;
						break;
					}
					else if (strcmp(pomCommHandler->pcmReqId,rrpBcData.Dest2) == 0)
					{
						if (this->bmApplCheck)
						{
							if ((*prlTableHandlers)[i].Applname.GetLength() > 0 && strncmp((*prlTableHandlers)[i].Applname,rrpBcData.Twe,(*prlTableHandlers)[i].Applname.GetLength()) != 0)
							{
								ilDDXType = (*prlTableHandlers)[i].DdxType;
								break;
							}
						}
					}
					else
					{
						ilDDXType = (*prlTableHandlers)[i].DdxType;
						break;
					}
				}
			}
		}
	}

	//TRACE("DDX: BC-NUM: X %s (%s) (%s)  D1(%s) D2(%s)\n",rrpBcData.BcNum, rrpBcData.Selection, rrpBcData.Data, rrpBcData.Dest1, rrpBcData.Dest2);

	if (ilDDXType != NOTUSED)
	{
		rrpBcData.DdxType = ilDDXType;
		omData.NewAt(omData.GetSize(),rrpBcData);

		DataChangeBc();
	}

	return(true);
}

void CCSBcHandle::DataChangeBc() 
{
	if (bmBcActiv == TRUE)
	{
		while (omData.GetSize() > 0)
		{
			BcStruct rlBcData = omData[0];
			omData.DeleteAt(0);

			if(pomDdx != NULL)
			{
				pomDdx->DataChanged((void *)this, rlBcData.DdxType, &rlBcData);
			}

		}
	}
}

bool CCSBcHandle::GetAtlBc(CCSPtrArray<BcStruct> &ropBcs, int ipBcNum)
{
	static short llNextBcNum = 0;
	short slActualBcNum; 
	short slActualBcNumAfterWrap = 0; 
	bool blBcNumHasWrapped = false; 
	static long llPrevBc = -1;
	struct BcStruct rlBcData;
	char pclPacketBuf[PACKET_LEN+4];
	static char clHopos[124] = "";
	static bool blUseHopos = false;
	static short sgMaxLostBcCount = 300;

	if (bgExtendLog && *pcmBcLogfile)
	{

		rgOutFile = fopen(pcmBcLogfile,"a");
		if (rgOutFile != NULL)
		{
			fprintf(rgOutFile,"***********************************************************************************\n"); 
			fprintf(rgOutFile,"GetBc started, ipBcNum: %d llNextBcNum: %ld\n",ipBcNum,llNextBcNum);
			fclose(rgOutFile);
			rgOutFile = NULL;
		}
	}

	if (ipBcNum < 1)
	{
		return false;
	}

	if (llNextBcNum == 0)
	{
		char pclConfigPath[182];
		char pclBcExtendLog[256];
		char pclTmpText[256];

		if (getenv("CEDA") == NULL)
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		else
			strcpy(pclConfigPath, getenv("CEDA"));

		GetPrivateProfileString(pcmApplName, "BC-LOGFILE", "",
			pcmBcLogfile, sizeof(pcmBcLogfile)-1, pclConfigPath);
		TrimRight(pcmBcLogfile);

		GetPrivateProfileString(pcmApplName, "BC-EXTLOG", "NO",
			pclBcExtendLog, sizeof(pclBcExtendLog)-1, pclConfigPath);
		TrimRight(pclBcExtendLog);
		if (strcmp(pclBcExtendLog,"YES") == 0)
		{
			bgExtendLog = true;
		}

		GetPrivateProfileString("GLOBAL", "MAXBCNUM", "30000",
			pclTmpText, sizeof(pclTmpText)-1, pclConfigPath);
		TrimRight(pclTmpText);
		sgMaxBcNum = atoi(pclTmpText);


		GetPrivateProfileString("GLOBAL", "HOPOS", "",
			clHopos, sizeof(clHopos)-1, pclConfigPath);
		TrimRight(clHopos);
		if (*clHopos)
		{
			blUseHopos = true;
		}

		GetPrivateProfileString("GLOBAL", "MAXLOSTBCCOUNT", "300",pclTmpText, sizeof(pclTmpText)-1, pclConfigPath);
		TrimRight(pclTmpText);
		sgMaxLostBcCount = atoi(pclTmpText);
	}

	if (prmBcFile == NULL)
	{
		prmBcFile = (FILE *) fopen (ogBcDataDatFile,"rb");
	}
	if (prmBcFile == NULL)
	{
		return false;
	}

	slActualBcNum = (short) ipBcNum; //::GetActualBcNum(); 
	
	if (llNextBcNum == 0) 
	{
		llNextBcNum = (short) ipBcNum;  //(short) ::GetFirstBcNum(); 
	}


//	if (slActualBcNum < (llNextBcNum-1))
	if (slActualBcNum < (llNextBcNum-200))  // MCU saudumme loesung (auf deutsch: Quick Hack )
	{
		if (bgExtendLog && *pcmBcLogfile)
		{

			rgOutFile = fopen(pcmBcLogfile,"a");
			if (rgOutFile != NULL)
			{
				fprintf(rgOutFile,"BcNumHasWrapped, slActualBcNum: %d  (llNextBcNum-1): %ld\n",slActualBcNum,(llNextBcNum-1) );
				fclose(rgOutFile);
				rgOutFile = NULL;
			}
		}

		blBcNumHasWrapped = true;
		slActualBcNumAfterWrap = slActualBcNum;
		slActualBcNum = sgMaxBcNum;
	}

	COMMIF  *prlPacket = NULL;				
   	BC_HEAD  *prlBcHead = NULL;
	CMDBLK   *prlCmdBlk = NULL;

	bool blBcOK = slActualBcNum >= llNextBcNum;

	while(blBcOK)
	{
		fseek(prmBcFile,FIRSTBC+(llNextBcNum*PACKET_LEN),SEEK_SET);
		fread(pclPacketBuf,PACKET_LEN,1,prmBcFile);

		prlPacket = (COMMIF  *) pclPacketBuf;				
   		prlBcHead = (BC_HEAD  *) prlPacket->data;
		prlCmdBlk = (CMDBLK *) prlBcHead->data;

		if(prlPacket->command == 0)
		{
			// we lost at least one BC
			int ilLostBcCount = ::GetLostBcCount();
			if (ilLostBcCount > sgMaxLostBcCount)
			{
				if(*pcmBcLogfile)
				{
					rgOutFile = fopen(pcmBcLogfile,"a");
					if (rgOutFile != NULL)
					{
						fprintf(rgOutFile,"Lost Broadcast count : %d exceeds limitiation of %d lost broadcasts\n",ilLostBcCount,sgMaxLostBcCount);
						fclose(rgOutFile);
						rgOutFile = NULL;
					}
				}
				char buffer[1024];


				sprintf(buffer,"Lost Broadcast count : %d exceeds limitiation of %d lost broadcasts\nNext BC Num: %d - Current BC Num: %d \nPlease restart the application",ilLostBcCount,sgMaxLostBcCount, llNextBcNum , slActualBcNum);

				omCloMessage = CString(buffer);

				// generate a "CLO" broadcast
				CreateCLOBroadcast(&rlBcData,llNextBcNum);
				ropBcs.NewAt(ropBcs.GetSize(), rlBcData);
				return true;
			}
			else
			{
				ReReadBc();
				if(*pcmBcLogfile)
				{
					rgOutFile = fopen(pcmBcLogfile,"a");
					if (rgOutFile != NULL)
					{
						fprintf(rgOutFile,"Broadcast lost: %d\n",llNextBcNum);
						fclose(rgOutFile);
						rgOutFile = NULL;
					}
				}
				return false;
			}
		}

		if (bgExtendLog && *pcmBcLogfile)
		{
			rgOutFile = fopen(pcmBcLogfile,"a");
			if (rgOutFile != NULL)
			{
				fprintf(rgOutFile,"BcOK, we've got llNextBcNum: %d\n",llNextBcNum);
				fclose(rgOutFile);
				rgOutFile = NULL;
			}
		}


		if (prlBcHead->act_buf == 1)
		{
			memcpy(&rmBcData.BcHead,prlBcHead,sizeof(BC_HEAD));
			memcpy(&rmBcData.CmdBlk,prlBcHead->data,sizeof(CMDBLK));
		}

		if(rmBcData.DataBufferSize < prlBcHead->tot_size)
		{
			char *pclNewDataBuf = (char *)realloc(rmBcData.DataBuffer,prlBcHead->tot_size+40);
			if (pclNewDataBuf != NULL)
			{
				rmBcData.DataBufferSize = prlBcHead->tot_size;
			}
		} 
		int ilOffset = (prlBcHead->act_buf-1)*rmBcData.BcHead.cmd_size;
		if ((ilOffset + prlBcHead->cmd_size) <= rmBcData.DataBufferSize)
		{
			memcpy(rmBcData.DataBuffer+ilOffset,prlCmdBlk->data,prlBcHead->cmd_size);
			rmBcData.DataBuffer[ilOffset+prlBcHead->cmd_size] = '\0';
		}
		llNextBcNum = (short) (prlBcHead->bc_num+1);  

		if (bgExtendLog && *pcmBcLogfile)
		{

			rgOutFile = fopen(pcmBcLogfile,"a");
			if (rgOutFile != NULL)
			{
				fprintf(rgOutFile,"llNextBcNum = (short) (prlBcHead->bc_num+1): %d\n",llNextBcNum);
				fclose(rgOutFile);
				rgOutFile = NULL;
			}
			
		}
		
		
		if (llNextBcNum > sgMaxBcNum) 
		{
			llNextBcNum = 1;
			blBcNumHasWrapped = true;
//			slActualBcNum = slActualBcNumAfterWrap;

			if (bgExtendLog && *pcmBcLogfile)
			{
				rgOutFile = fopen(pcmBcLogfile,"a");
				if (rgOutFile != NULL)
				{
					fprintf(rgOutFile,"llNextBcNum = (short) (prlBcHead->bc_num+1): %d  slActualBcNum %d\n",llNextBcNum,slActualBcNum);
					fclose(rgOutFile);
					rgOutFile = NULL;
				}
			}
		}
		
		if (prlBcHead->act_buf == prlBcHead->tot_buf)
		{
			if (prlPacket != NULL)
			{
				//strncpy (rlBcData.ReqId,rmBcData.BcHead.seq_id,10); 
				strncpy (rlBcData.ReqId,rmBcData.BcHead.recv_name,10); 
				rlBcData.ReqId[10] = '\0';
				strncpy (rlBcData.Dest1,rmBcData.BcHead.dest_name,10); 
				rlBcData.Dest1[10] = '\0';
				strncpy (rlBcData.Dest2,rmBcData.BcHead.recv_name,10); 
				rlBcData.Dest2[10] = '\0';
				strncpy (rlBcData.Cmd,rmBcData.CmdBlk.command,6); 
				rlBcData.Cmd[6] = '\0';
				strncpy (rlBcData.Object,rmBcData.CmdBlk.obj_name,33);
				rlBcData.Object[33] = '\0';
				strncpy (rlBcData.Seq,rmBcData.CmdBlk.order,2);
				rlBcData.Seq[2] = '\0';
				strncpy (rlBcData.Tws,rmBcData.CmdBlk.tw_start,33);
				rlBcData.Tws[33] = '\0';
				strncpy (rlBcData.Twe,rmBcData.CmdBlk.tw_end,33);
				rlBcData.Twe[33] = '\0';
				char *pclData = rmBcData.DataBuffer;
				strncpy (rlBcData.Selection,pclData,2048);
				rlBcData.Selection[2047] = '\0';
				pclData += strlen(pclData)+1;
				strncpy (rlBcData.Fields,pclData,2048);
				rlBcData.Fields[2047] = '\0';
				pclData += strlen(pclData)+1;
				strncpy (rlBcData.Data,pclData,8196);
				rlBcData.Data[8195] = '\0';
				{
					/* this workaround removes the old data (second line)
						from broadcasts created by Bernies Shanghai SQLHDL */

					char *pclNewline = strchr(rlBcData.Data,'\n');
					if (pclNewline != NULL)
					{
						*pclNewline = '\0';
					}

				}

				sprintf(rlBcData.BcNum,"%d",prlBcHead->bc_num);

				/** MCU: 20010409 Check Hopo ***/
				char clActHopo[12] = "";
				bool blHopoOk = true;

				GetItem(1,rlBcData.Twe,clActHopo);

				if (*clActHopo && blUseHopos && *clHopos)
				{
					if(strstr(clHopos,clActHopo) == NULL)
					{
						blHopoOk = false;
					}
				}
				if (blHopoOk)
				{
					ropBcs.NewAt(ropBcs.GetSize(), rlBcData);
//					DistributeBc(rlBcData);		// No distribute, because the return will contain all broadcasts
				}

				if(*pcmBcLogfile)
				{
					CTime olNow(CTime::GetCurrentTime());

					rgOutFile = fopen(pcmBcLogfile,"a");
					if (rgOutFile != NULL)
					{
						fprintf(rgOutFile,"%s BcNum: %s\n",olNow.Format("%d.%m.%Y %H:%M:%S"),rlBcData.BcNum);
						fprintf(rgOutFile,"ReqId    : %s\n",rlBcData.ReqId);
						fprintf(rgOutFile,"Cmd      : %s\n",rlBcData.Cmd);
						fprintf(rgOutFile,"Object   : %s\n",rlBcData.Object);
						fprintf(rgOutFile,"Selection: %s\n",rlBcData.Selection);
						fprintf(rgOutFile,"Fields   : %s\n",rlBcData.Fields);
						fprintf(rgOutFile,"Data     : %s\n",rlBcData.Data);
						fclose(rgOutFile);
						rgOutFile = NULL;
					}

//					if(!blHopoOk)
//					{
//						olOut << "MESSAGE IGNORED --> HOPO=<" << clActHopo << ">" << endl;
//					}
//					olOut.close();
				}
			}
		}
		else
		{
				if(*pcmBcLogfile)
				{
					rgOutFile = fopen(pcmBcLogfile,"a");
					if (rgOutFile != NULL)
					{
						fprintf(rgOutFile,"ReqId: %s BcNum(part): %d\n",rlBcData.ReqId,prlBcHead->bc_num);
						fclose(rgOutFile);
						rgOutFile = NULL;
					}
				}

		}
		//if(llNextBcNum > slActualBcNum && !blBcNumHasWrapped)
		if((llNextBcNum > slActualBcNum) || blBcNumHasWrapped)
		{
			// we fetched all pending BCs
			blBcOK = false;
			if (bgExtendLog && *pcmBcLogfile)
			{

				if(*pcmBcLogfile)
				{
					rgOutFile = fopen(pcmBcLogfile,"a");
					if (rgOutFile != NULL)
					{
						fprintf(rgOutFile,"No more BC's pending: llNextBcNum: %ld slActualBcNum: %d  blBcNumHasWrapped: %d\n",
								llNextBcNum,slActualBcNum,blBcNumHasWrapped);
						fclose(rgOutFile);
						rgOutFile = NULL;
					}
				}

			}
		}
		else
		{
			if (bgExtendLog && *pcmBcLogfile)
			{
				rgOutFile = fopen(pcmBcLogfile,"a");
				if (rgOutFile != NULL)
				{
					fprintf(rgOutFile,"There are more BC's pending: llNextBcNum: %ld slActualBcNum: %d  blBcNumHasWrapped: %d\n",
						llNextBcNum,slActualBcNum,blBcNumHasWrapped);
					fclose(rgOutFile);
					rgOutFile = NULL;
				}

			}

		}
	}

	return true; 
} // End Fkt ::GetAtlBc
//------------------------------------------------------------------------------
// MWO: 02.07.04
// This method is called from each client, which uses the BCComClient.ocx
// to receive Broadcasts. This processes the broadcast and distributes them
// with DDX
//------------------------------------------------------------------------------
bool CCSBcHandle::ProcessBroadcast(LPCTSTR ReqId, LPCTSTR Dest1, LPCTSTR Dest2, LPCTSTR Cmd, LPCTSTR Object, LPCTSTR Seq, LPCTSTR Tws, LPCTSTR Twe, LPCTSTR Selection, LPCTSTR Fields, LPCTSTR Data, LPCTSTR BcNum, LPCTSTR Attachment, LPCTSTR Additional)
{
	struct BcStruct rlBcData;

	strncpy (rlBcData.ReqId, ReqId ,10); 
	rlBcData.ReqId[10] = '\0';
	strncpy (rlBcData.Dest1, Dest1, 10); 
	rlBcData.Dest1[10] = '\0';
	strncpy (rlBcData.Dest2, Dest2, 10); 
	rlBcData.Dest2[10] = '\0';
	strncpy (rlBcData.Cmd, Cmd, 6); 
	rlBcData.Cmd[6] = '\0';
	strncpy (rlBcData.Object, Object, 33);
	rlBcData.Object[33] = '\0';
	strncpy (rlBcData.Seq, Seq, 2);
	rlBcData.Seq[2] = '\0';
	strncpy (rlBcData.Tws, Tws, 33);
	rlBcData.Tws[33] = '\0';
	strncpy (rlBcData.Twe, Twe, 33);
	rlBcData.Twe[33] = '\0';
	strncpy (rlBcData.Selection, Selection, 2048);
	rlBcData.Selection[2047] = '\0';
	strncpy (rlBcData.Fields, Fields, 2048);
	rlBcData.Fields[2047] = '\0';
	strncpy (rlBcData.Data, Data, 8196);
	rlBcData.Data[8195] = '\0';
	{
		/* this workaround removes the old data (second line)
			from broadcasts created by Bernies Shanghai SQLHDL */

		char *pclNewline = strchr(rlBcData.Data,'\n');
		if (pclNewline != NULL)
		{
			*pclNewline = '\0';
		}

	}
	rlBcData.Attachment = Attachment;

	if(pimBCState != NULL)
	{
		*pimBCState = 2; //Set the trafficlight indicator.
	}

	DistributeBc(rlBcData);

	return true;
}

bool CCSBcHandle::GetBc(int ipBcNum)
{
	static short llNextBcNum = 0;
	short slActualBcNum; 
	short slActualBcNumAfterWrap = 0; 
	bool blBcNumHasWrapped = false; 
	static long llPrevBc = -1;
	struct BcStruct rlBcData;
	char pclPacketBuf[PACKET_LEN+4];
	static char clHopos[124] = "";
	static bool blUseHopos = false;
	static short sgMaxLostBcCount = 300;
	CTime olNowStart = CTime((time_t)-1);


	if (bgExtendLog && *pcmBcLogfile)
	{

		rgOutFile = fopen(pcmBcLogfile,"a");
		if (rgOutFile != NULL)
		{
			fprintf(rgOutFile,"***********************************************************************************\n"); 
			if (omLastBcEnd != CTime((time_t)-1))
			{
				CTime olNow(CTime::GetCurrentTime());
				CTimeSpan olSpan = olNow - omLastBcEnd;
				int totalSec = olSpan.GetTotalSeconds();
				if (totalSec > 15)
					fprintf(rgOutFile,"%d !!!!!!!!!!!!Seconds between Bc: \n",olSpan);
				else
					fprintf(rgOutFile,"%d Seconds between Bc: \n",olSpan);
			}
			fprintf(rgOutFile,"***********************************************************************************\n"); 
			fprintf(rgOutFile,"GetBc started, ipBcNum: %d llNextBcNum: %ld\n",ipBcNum,llNextBcNum);
			fclose(rgOutFile);
			rgOutFile = NULL;
		}
	}

	if (ipBcNum < 1)
	{
		return false;
	}

	if (llNextBcNum == 0)
	{
		char pclConfigPath[182];
		char pclBcExtendLog[256];
		char pclTmpText[256];

		if (getenv("CEDA") == NULL)
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		else
			strcpy(pclConfigPath, getenv("CEDA"));

		GetPrivateProfileString(pcmApplName, "BC-LOGFILE", "",
			pcmBcLogfile, sizeof(pcmBcLogfile)-1, pclConfigPath);
		TrimRight(pcmBcLogfile);

		GetPrivateProfileString(pcmApplName, "BC-EXTLOG", "NO",
			pclBcExtendLog, sizeof(pclBcExtendLog)-1, pclConfigPath);
		TrimRight(pclBcExtendLog);
		if (strcmp(pclBcExtendLog,"YES") == 0)
		{
			bgExtendLog = true;
		}

		GetPrivateProfileString("GLOBAL", "MAXBCNUM", "30000",
			pclTmpText, sizeof(pclTmpText)-1, pclConfigPath);
		TrimRight(pclTmpText);
		sgMaxBcNum = atoi(pclTmpText);


		GetPrivateProfileString("GLOBAL", "HOPOS", "",
			clHopos, sizeof(clHopos)-1, pclConfigPath);
		TrimRight(clHopos);
		if (*clHopos)
		{
			blUseHopos = true;
		}

		GetPrivateProfileString("GLOBAL", "MAXLOSTBCCOUNT", "300",pclTmpText, sizeof(pclTmpText)-1, pclConfigPath);
		TrimRight(pclTmpText);
		sgMaxLostBcCount = atoi(pclTmpText);

	}

	if (prmBcFile == NULL)
	{
		prmBcFile = (FILE *) fopen (ogBcDataDatFile,"rb");
	}
	if (prmBcFile == NULL)
	{
		return false;
	}

	slActualBcNum = (short) ipBcNum; //::GetActualBcNum(); 
	
	if (llNextBcNum == 0) 
	{
		llNextBcNum = (short) ipBcNum;  //(short) ::GetFirstBcNum(); 
	}


//	if (slActualBcNum < (llNextBcNum-1))
	if (slActualBcNum < (llNextBcNum-200))  // MCU saudumme loesung (auf deutsch: Quick Hack )
	{
		if (bgExtendLog && *pcmBcLogfile)
		{

			rgOutFile = fopen(pcmBcLogfile,"a");
			if (rgOutFile != NULL)
			{
				fprintf(rgOutFile,"BcNumHasWrapped, slActualBcNum: %d  (llNextBcNum-1): %ld\n",slActualBcNum,(llNextBcNum-1) );
				fclose(rgOutFile);
				rgOutFile = NULL;
			}
		}

		blBcNumHasWrapped = true;
		slActualBcNumAfterWrap = slActualBcNum;
		slActualBcNum = sgMaxBcNum;
	}

	COMMIF  *prlPacket = NULL;				
   	BC_HEAD  *prlBcHead = NULL;
	CMDBLK   *prlCmdBlk = NULL;

	bool blBcOK = slActualBcNum >= llNextBcNum;

	while(blBcOK)
	{
		fseek(prmBcFile,FIRSTBC+(llNextBcNum*PACKET_LEN),SEEK_SET);
		fread(pclPacketBuf,PACKET_LEN,1,prmBcFile);

		prlPacket = (COMMIF  *) pclPacketBuf;				
   		prlBcHead = (BC_HEAD  *) prlPacket->data;
		prlCmdBlk = (CMDBLK *) prlBcHead->data;

		if(prlPacket->command == 0)
		{
			// we lost at least one BC
			int ilLostBcCount = ::GetLostBcCount();
			if (ilLostBcCount > sgMaxLostBcCount)
			{
				if(*pcmBcLogfile)
				{
					rgOutFile = fopen(pcmBcLogfile,"a");
					if (rgOutFile != NULL)
					{
						fprintf(rgOutFile,"Lost Broadcast count : %d exceeds limitiation of %d lost broadcasts\n",ilLostBcCount,sgMaxLostBcCount);
						fclose(rgOutFile);
						rgOutFile = NULL;
					}
				}

				char buffer[1024];

				sprintf(buffer,"Lost Broadcast count : %d exceeds limitiation of %d lost broadcasts\nNext BC Num: %d - Current BC Num: %d \nPlease restart the application",ilLostBcCount,sgMaxLostBcCount, llNextBcNum , slActualBcNum);

				omCloMessage = CString(buffer);
				// generate a "CLO" broadcast
				CreateCLOBroadcast(&rlBcData,llNextBcNum);
				DistributeBc(rlBcData);
				return true;
			}
			else
			{
				ReReadBc();
				if(*pcmBcLogfile)
				{
					rgOutFile = fopen(pcmBcLogfile,"a");
					if (rgOutFile != NULL)
					{
						fprintf(rgOutFile,"Broadcast lost: %d\n",llNextBcNum);
						fclose(rgOutFile);
						rgOutFile = NULL;
					}
				}
				return false;
			}
		}

		if (bgExtendLog && *pcmBcLogfile)
		{
			rgOutFile = fopen(pcmBcLogfile,"a");
			if (rgOutFile != NULL)
			{
				fprintf(rgOutFile,"BcOK, we've got llNextBcNum: %d\n",llNextBcNum);
				fclose(rgOutFile);
				rgOutFile = NULL;
			}
		}


		if (prlBcHead->act_buf == 1)
		{
			memcpy(&rmBcData.BcHead,prlBcHead,sizeof(BC_HEAD));
			memcpy(&rmBcData.CmdBlk,prlBcHead->data,sizeof(CMDBLK));
		}

		if(rmBcData.DataBufferSize < prlBcHead->tot_size)
		{
			char *pclNewDataBuf = (char *)realloc(rmBcData.DataBuffer,prlBcHead->tot_size+40);
			if (pclNewDataBuf != NULL)
			{
				rmBcData.DataBufferSize = prlBcHead->tot_size;
			}
		} 
		int ilOffset = (prlBcHead->act_buf-1)*rmBcData.BcHead.cmd_size;
		if ((ilOffset + prlBcHead->cmd_size) <= rmBcData.DataBufferSize)
		{
			memcpy(rmBcData.DataBuffer+ilOffset,prlCmdBlk->data,prlBcHead->cmd_size);
			rmBcData.DataBuffer[ilOffset+prlBcHead->cmd_size] = '\0';
		}
		llNextBcNum = (short) (prlBcHead->bc_num+1);  

		if (bgExtendLog && *pcmBcLogfile)
		{

			rgOutFile = fopen(pcmBcLogfile,"a");
			if (rgOutFile != NULL)
			{
				fprintf(rgOutFile,"llNextBcNum = (short) (prlBcHead->bc_num+1): %d\n",llNextBcNum);
				fclose(rgOutFile);
				rgOutFile = NULL;
			}
			
		}
		
		
		if (llNextBcNum > sgMaxBcNum) 
		{
			llNextBcNum = 1;
			blBcNumHasWrapped = true;
//			slActualBcNum = slActualBcNumAfterWrap;

			if (bgExtendLog && *pcmBcLogfile)
			{
				rgOutFile = fopen(pcmBcLogfile,"a");
				if (rgOutFile != NULL)
				{
					fprintf(rgOutFile,"llNextBcNum = (short) (prlBcHead->bc_num+1): %d  slActualBcNum %d\n",llNextBcNum,slActualBcNum);
					fclose(rgOutFile);
					rgOutFile = NULL;
				}
			}
		}
		
		if (prlBcHead->act_buf == prlBcHead->tot_buf)
		{
			if (prlPacket != NULL)
			{
				//strncpy (rlBcData.ReqId,rmBcData.BcHead.seq_id,10); 
				strncpy (rlBcData.ReqId,rmBcData.BcHead.recv_name,10); 
				rlBcData.ReqId[10] = '\0';
				strncpy (rlBcData.Dest1,rmBcData.BcHead.dest_name,10); 
				rlBcData.Dest1[10] = '\0';
				strncpy (rlBcData.Dest2,rmBcData.BcHead.recv_name,10); 
				rlBcData.Dest2[10] = '\0';
				strncpy (rlBcData.Cmd,rmBcData.CmdBlk.command,6); 
				rlBcData.Cmd[6] = '\0';
				strncpy (rlBcData.Object,rmBcData.CmdBlk.obj_name,33);
				rlBcData.Object[33] = '\0';
				strncpy (rlBcData.Seq,rmBcData.CmdBlk.order,2);
				rlBcData.Seq[2] = '\0';
				strncpy (rlBcData.Tws,rmBcData.CmdBlk.tw_start,33);
				rlBcData.Tws[33] = '\0';
				strncpy (rlBcData.Twe,rmBcData.CmdBlk.tw_end,33);
				rlBcData.Twe[33] = '\0';
				char *pclData = rmBcData.DataBuffer;
				strncpy (rlBcData.Selection,pclData,2048);
				rlBcData.Selection[2047] = '\0';
				pclData += strlen(pclData)+1;
				strncpy (rlBcData.Fields,pclData,2048);
				rlBcData.Fields[2047] = '\0';
				pclData += strlen(pclData)+1;
				strncpy (rlBcData.Data,pclData,8196);
				rlBcData.Data[8195] = '\0';
				{
					/* this workaround removes the old data (second line)
						from broadcasts created by Bernies Shanghai SQLHDL */

					char *pclNewline = strchr(rlBcData.Data,'\n');
					if (pclNewline != NULL)
					{
						*pclNewline = '\0';
					}

				}

				sprintf(rlBcData.BcNum,"%d",prlBcHead->bc_num);

				/** MCU: 20010409 Check Hopo ***/
				char clActHopo[12] = "";
				bool blHopoOk = true;

				GetItem(1,rlBcData.Twe,clActHopo);

				if (*clActHopo && blUseHopos && *clHopos)
				{
					if(strstr(clHopos,clActHopo) == NULL)
					{
						blHopoOk = false;
					}
				}
				if (blHopoOk)
				{
					if (bgExtendLog && *pcmBcLogfile)
					{
						CTime olNow(CTime::GetCurrentTime());
						olNowStart = olNow;

						rgOutFile = fopen(pcmBcLogfile,"a");
						if (rgOutFile != NULL)
						{
							fprintf(rgOutFile,"%s distribute BcNum: %s\n",olNow.Format("%d.%m.%Y %H:%M:%S"),rlBcData.BcNum);
							fprintf(rgOutFile,"ReqId    : %s\n",rlBcData.ReqId);
							fprintf(rgOutFile,"Cmd      : %s\n",rlBcData.Cmd);
							fprintf(rgOutFile,"Object   : %s\n",rlBcData.Object);
							fprintf(rgOutFile,"Selection: %s\n",rlBcData.Selection);
							fprintf(rgOutFile,"Fields   : %s\n",rlBcData.Fields);
							fprintf(rgOutFile,"Data     : %s\n",rlBcData.Data);
							fclose(rgOutFile);
							rgOutFile = NULL;
						}
					}

//					DistributeBc(rlBcData);
					char pclMyBcInfo[10000];
/*					sprintf(pclMyBcInfo, "%s|%s|%s|%s|%s", rlBcData.Cmd,
												rlBcData.Object,
												rlBcData.Selection,
												rlBcData.Fields,
												rlBcData.Data);
*/
					sprintf(pclMyBcInfo, "%s|%s|%s|%s|%s", rlBcData.Cmd,
												rlBcData.Object,
												rlBcData.Selection,
												rlBcData.Fields,
												"");

					int ilLen = strlen(pclMyBcInfo);
					ilLen = 10000 - ilLen -1;

					char strData[10000];
					strncpy(strData,rlBcData.Data,ilLen);
					//strData = strData.Left(ilLen);
					strcat(pclMyBcInfo,strData);

					if(pomMsgDlg != NULL)
					{
						pomMsgDlg->SendMessage( BC_TO_APPLICATION, (WPARAM)pclMyBcInfo, 0);
					}
					DistributeBc(rlBcData);
/*					sprintf(pclMyBcInfo, "***%s|%s|%s|%s|%s", rlBcData.Cmd,
												rlBcData.Object,
												rlBcData.Selection,
												rlBcData.Fields,
												rlBcData.Data);
*/
					sprintf(pclMyBcInfo, "***%s|%s|%s|%s|%s", rlBcData.Cmd,
												rlBcData.Object,
												rlBcData.Selection,
												rlBcData.Fields,

					"");

					ilLen = strlen(pclMyBcInfo);
					ilLen = 10000 - ilLen -1;

					
					strncpy(strData,rlBcData.Data,ilLen);
					//strData = strData.Left(ilLen);
					strcat(pclMyBcInfo,strData);
				}

				if (*pcmBcLogfile)
				{
					CTime olNow(CTime::GetCurrentTime());

					rgOutFile = fopen(pcmBcLogfile,"a");
					if (rgOutFile != NULL)
					{
						fprintf(rgOutFile,"%s BcNum: %s\n",olNow.Format("%d.%m.%Y %H:%M:%S"),rlBcData.BcNum);
						fprintf(rgOutFile,"ReqId    : %s\n",rlBcData.ReqId);
						fprintf(rgOutFile,"Cmd      : %s\n",rlBcData.Cmd);
						fprintf(rgOutFile,"Object   : %s\n",rlBcData.Object);
						fprintf(rgOutFile,"Selection: %s\n",rlBcData.Selection);
						fprintf(rgOutFile,"Fields   : %s\n",rlBcData.Fields);
						fprintf(rgOutFile,"Data     : %s\n",rlBcData.Data);
						fclose(rgOutFile);
						rgOutFile = NULL;
					}

				}
			}
		}
		else
		{
				if(*pcmBcLogfile)
				{
					rgOutFile = fopen(pcmBcLogfile,"a");
					if (rgOutFile != NULL)
					{
						fprintf(rgOutFile,"ReqId: %s BcNum(part): %d\n",rlBcData.ReqId,prlBcHead->bc_num);
						fclose(rgOutFile);
						rgOutFile = NULL;
					}
				}

		}
		//if(llNextBcNum > slActualBcNum && !blBcNumHasWrapped)
		if((llNextBcNum > slActualBcNum) || blBcNumHasWrapped)
		{
			// we fetched all pending BCs
			blBcOK = false;
			if (bgExtendLog && *pcmBcLogfile)
			{

				if(*pcmBcLogfile)
				{
					rgOutFile = fopen(pcmBcLogfile,"a");
					if (rgOutFile != NULL)
					{
						fprintf(rgOutFile,"No more BC's pending: llNextBcNum: %ld slActualBcNum: %d  blBcNumHasWrapped: %d\n",
								llNextBcNum,slActualBcNum,blBcNumHasWrapped);
						fclose(rgOutFile);
						rgOutFile = NULL;
					}
				}

			}
		}
		else
		{
			if (bgExtendLog && *pcmBcLogfile)
			{
				rgOutFile = fopen(pcmBcLogfile,"a");
				if (rgOutFile != NULL)
				{
					fprintf(rgOutFile,"There are more BC's pending: llNextBcNum: %ld slActualBcNum: %d  blBcNumHasWrapped: %d\n",
						llNextBcNum,slActualBcNum,blBcNumHasWrapped);
					fclose(rgOutFile);
					rgOutFile = NULL;
				}

			}

		}
	}

	if (bgExtendLog && *pcmBcLogfile)
	{
		omLastBcEnd = CTime::GetCurrentTime();
		rgOutFile = fopen(pcmBcLogfile,"a");
		if (rgOutFile != NULL)
		{
			CTimeSpan olSpan = omLastBcEnd - olNowStart;
			int totalSec = olSpan.GetTotalSeconds();
			fprintf(rgOutFile,"%s BcNum End: %s\n",omLastBcEnd.Format("%d.%m.%Y %H:%M:%S"),rlBcData.BcNum);
			fprintf(rgOutFile,"%d BcNum Tmiedifferenz: %s\n",totalSec,rlBcData.BcNum);
			fclose(rgOutFile);
			rgOutFile = NULL;
		}
	}


	return true; 
} // End Fkt ::GetBc(int ipBcNum)

void CCSBcHandle::BufferBc(void)
{
	bmBcActiv = false;
}

void CCSBcHandle::ReleaseBuffer(void)
{
	StartBc();
}

bool CCSBcHandle::ReReadBc()
{
	bool blRc = true;
	struct BcStruct rlBcData;

	*rlBcData.Selection = '\0';
	*rlBcData.Data = '\0';
	*rlBcData.BcNum = '\0';
	strcpy(rlBcData.Cmd,"RBS");
	*rlBcData.Object = '\0';
	*rlBcData.Fields = '\0';

	CedaAction(rlBcData.Cmd,rlBcData.Object,rlBcData.Fields,rlBcData.Selection,"",rlBcData.Data);
	return blRc;
}

bool CCSBcHandle::SetFilterRange(CString spTable, CString spFromField, CString spToField, CString spFromValue, CString spToValue)
{
	if (prlBcCCmdTarget != NULL)
	{
		prlBcCCmdTarget->SetFilterRange(spTable, spFromField, spToField, spFromValue, spToValue);
		return true;
	}
	else
	{
		return false;
	}
}

void CCSBcHandle::AttachApplicationBCState(int *pipBCState)
{
	pimBCState = pipBCState;
}


void CCSBcHandle::CreateCLOBroadcast(struct BcStruct *prpBcData,short lpNextBcNum)
{
	if (prpBcData == NULL)
		return;

	strncpy (prpBcData->ReqId,"",10); 
	prpBcData->ReqId[10] = '\0';
	strncpy (prpBcData->Dest1,"",10); 
	prpBcData->Dest1[10] = '\0';
	strncpy (prpBcData->Dest2,"",10); 
	prpBcData->Dest2[10] = '\0';
	strncpy (prpBcData->Cmd,"CLO",6); 
	prpBcData->Cmd[6] = '\0';
	strncpy (prpBcData->Object,"CLOTAB",33);
	prpBcData->Object[33] = '\0';
	strncpy (prpBcData->Seq,"",2);
	prpBcData->Seq[2] = '\0';
	strncpy (prpBcData->Tws,"",33);
	prpBcData->Tws[33] = '\0';
	strncpy (prpBcData->Twe,"",33);
	prpBcData->Twe[33] = '\0';
	strncpy (prpBcData->Selection,"",2048);
	prpBcData->Selection[2047] = '\0';
	strncpy (prpBcData->Fields,"",2048);
	prpBcData->Fields[2047] = '\0';
	strncpy (prpBcData->Data,"",8196);
	prpBcData->Data[8195] = '\0';

	sprintf(prpBcData->BcNum,"%d",lpNextBcNum);
}


static const char *DATA_TOKEN		=	"{=DATA=}";
static const char *DATA_ENDTOKEN	=	"{=\\DATA=}";

CAttachment::CAttachment(CString &ropAttachment)
{
	bmAttachmentValid = true;

	CString olFieldsToken = "{=FIELDS=}", olFieldsEndToken  = "{=\\FIELDS=}";
	char *pclData = strstr(ropAttachment.GetBuffer(0), olFieldsToken);

	if(pclData == NULL)
		bmAttachmentValid = false; // invalid field list

	char *pclEnd = NULL;

	if(bmAttachmentValid)
	{
		pclData += strlen(olFieldsToken);
		if((pclEnd = strstr(pclData, olFieldsEndToken)) == NULL)
			pclEnd = pclData + strlen(pclData);
		omFieldList.Format("%.*s", (int) (pclEnd - pclData), pclData);
		pclData = pclEnd;
	}
	Trace("Fields: %s\n", omFieldList);

	if(bmAttachmentValid && (pclData = GetAttachmentData(pclData, "{=INSERT=}", omInsertDataList, "\n")) == NULL)
		bmAttachmentValid = false;
	if(bmAttachmentValid && (pclData = GetAttachmentData(pclData, "{=UPDATE=}", omUpdateDataList, "\n")) == NULL)
		bmAttachmentValid = false;
	if(bmAttachmentValid && (pclData = GetAttachmentData(pclData, "{=DELETE=}", omDeleteDataList, ",")) == NULL)
		bmAttachmentValid = false;

	int i = 0;
	Trace("{=INSERT=}\n");
	for(i = 0; i < omInsertDataList.GetSize(); i++)
		Trace("%s\n", omInsertDataList[i]);
	Trace("{=UPDATE=}\n");
	for(i = 0; i < omUpdateDataList.GetSize(); i++)
		Trace("%s\n", omUpdateDataList[i]);
	Trace("{=DELETE=}\n");
	for(i = 0; i < omDeleteDataList.GetSize(); i++)
		Trace("%s\n", omDeleteDataList[i]);
}

char *CAttachment::GetAttachmentData(char *pcpData, char *pcpToken, CStringArray &ropDataList, char *pcpRecSeparator)
{
	char *pclData = strstr(pcpData, pcpToken);
	if(pclData == NULL)
		return pcpData; // pcpToken not found

	if((pclData = strstr(pclData, DATA_TOKEN)) == NULL)
		return NULL; // error DATA_TOKEN not found

	pclData += strlen(DATA_TOKEN);
	char *pclEnd = strstr(pclData, DATA_ENDTOKEN);

	CString olData;	
	olData.Format("%.*s", (pclEnd != NULL) ? pclEnd - pclData : strlen(pclData), pclData);
	if(!olData.IsEmpty())
		::ExtractItemList(olData, &ropDataList, *pcpRecSeparator);

	return (pclEnd != NULL) ? pclEnd : pclData;
}
/*
char *CAttachment::GetAttachmentData(char *pcpData, char *pcpToken, CStringArray &ropDataList, char *pcpRecSeparator)
{
	char *pclData = pcpData;

	if((pclData = strstr(pclData, pcpToken)) == NULL)
		return pcpData; // token not found

	char *pclEnd = NULL;
	CString olDataToken = "{=DATA=}", olEndDataToken = "{=\\DATA=}";
	CString olCountToken = "{=COUNT=}";

	if((pclData = strstr(pclData, olCountToken)) == NULL)
		return NULL; // error (=COUNT=) not found
	pclData += strlen(olCountToken);
	int ilNumRecs = atoi(pclData);
	if(ilNumRecs <= 0)
		return pclData; // no records

	if((pclData = strstr(pclData, olDataToken)) == NULL)
		return NULL; // error (=DATA=) not found
	pclData += olDataToken.GetLength();

	int ilDataLen = 0;
	for(int ilRec = 0; ilRec < ilNumRecs; ilRec++)
	{
		int ilTokenLen = 1;
		if((pclEnd = strstr(pclData, pcpRecSeparator)) == NULL) // search for record separator
		{
			pclEnd = strstr(pclData, olEndDataToken); // or the end token
			ilTokenLen = olEndDataToken.GetLength();
		}

		ilDataLen = (pclEnd != NULL) ? pclEnd - pclData : strlen(pclData);
		CString olRec;	olRec.Format("%.*s",ilDataLen, pclData);
		ropDataList.Add(olRec);
		Trace("%s %d %d %s\n", pcpToken, ilNumRecs, ilDataLen, olRec);
		pclData = (pclEnd != NULL) ? pclEnd + ilTokenLen : pclData;
	}

	return pclData;
}*/
//		olRec.Format("%.*s",ilDataLen, pclData);
//		char *pclDataList = new char[ilDataLen + 10];
//		strncpy(pclDataList, pclData, ilDataLen);
//		pclDataList[ilDataLen] = 0x00; 
//		ropDataList.Add(pclDataList);
//		delete [] pclDataList;

// this is a safe version of TRACE (TRACE crashes when there are > 512 characters)
void CAttachment::Trace(char *pcpFormatList, ...)
{
	char pclText[512];
	memset(pclText,'\0',512);
	va_list args;
	va_start(args, pcpFormatList);
	_vsnprintf( pclText, 511, pcpFormatList, args);
	TRACE(pclText);
}
