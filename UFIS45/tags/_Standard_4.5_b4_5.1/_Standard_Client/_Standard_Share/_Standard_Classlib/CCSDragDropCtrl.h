#ifndef __CCSDRAGDROPCTRL_H__
#define __CCSDRAGDROPCTRL_H__  

#include <afxwin.h>
#include <afxole.h>

/////////////////////////////////////////////////////////////////////////////
// enum declaration

//@Man:
//@Memo: DragDropType
//@See: CCSDRAGDROPCTRL
/*@Doc:
	The following values are defined:
	\begin{itemize}
    \item  StaffFromPrePlanTable
    \item  FlightFromPrePlanTable
	\end{itemize}
*/	
enum DragDropType 
{ 
    StaffFromPrePlanTable,
    FlightFromPrePlanTable    
};
    


/////////////////////////////////////////////////////////////////////////////
// Record structure declaration

//@Man:  tagDATADRAGDROP
//@Memo: tagDATADRAGDROP
//@See:  DGDPCTRL
/*@Doc:
	The following struct-items are defined:
	\begin{itemize}
    \item  #int#	nTypeDragDrop;
    \item  #int#	nNumItem;
    \item  #int#	nItemSize;
	\end{itemize}
*/
typedef struct tagDATADRAGDROP {
    int nTypeDragDrop;
    int nNumItem;
    int nItemSize;
}DATADRAGDROP, FAR *LPDATADRAGDROP;
    


/////////////////////////////////////////////////////////////////////////////
// Class declaration of CCSDragDropCtrl

//@Man:
//@Memo: CCSDragDropCtrl
/*@Doc:
  No comment on this up to now.
*/
class CCSDragDropCtrl : public COleDropTarget
{ 
private:
    //@ManMemo:		m_dataSource
    COleDataSource  m_dataSource;
    //@ManMemo:		m_hDataSource
    HGLOBAL         m_hDataSource;
    //@ManMemo:		nCurPosData
    int             nCurPosData;
    
    //HGLOBAL		m_hDataTarget;
    //@ManMemo:		lpDataDragDrop
    LPDATADRAGDROP  lpDataDragDrop;
    //@ManMemo:		lpDataString
    LPSTR           lpDataString;
    //CWnd*			m_myParentWnd; 
    //@ManMemo:		m_callMsgWnd
    CWnd*           m_callMsgWnd;

	CWnd*			m_scrollMsgWnd;

	enum	DropScrollState	
	{
		UDROP_INSETNONE		= 0,
		UDROP_INSETLEFT		= 1,
		UDROP_INSETRIGHT	= 2,
		UDROP_INSETHORZ		= 3,
		UDROP_INSETTOP		= 4,
		UDROP_INSETBOTTOM	= 8,
		UDROP_INSETVERT		=12, 
	};

	static  UINT	umScrollInset;
	static	UINT	umScrollDelay;

	DWORD	wmTimeLast;
	UINT	umLastTest;
	CPoint	omLastPick;
	UINT	umLast;
	UINT	umVScrollCode;
	UINT	umHScrollCode;
	UINT	TestDroppablePoint(CWnd *pWnd,const CPoint& pnt);
public:                                                        
// Begin Drag section
    //@ManMemo: CreateStringData
    /*@Doc:
	  Drag section
	*/
    void CreateStringData( int nTypeSource, UINT uItemSize, UINT uMaxItem );
    //@ManMemo: AddString
    /*@Doc:
	  Drag section
	*/
    void AddString( const char *pStr );    
    //@ManMemo: CreateDWordData
    /*@Doc:
	  Drag section
	*/
    void CreateDWordData( int nTypeSource, UINT uMaxItem );
    //@ManMemo: AddDWord
    /*@Doc:
	  Drag section
	*/
    void AddDWord( DWORD dItem );
    
    //@ManMemo: BeginDrag
    /*@Doc:
	  Drag section
	*/
    void BeginDrag();
// End Drag section

// Begin Drop Section
    //@ManMemo: GetDataClass
    /*@Doc:
	  Drop Section
	*/
    int GetDataClass();
    //@ManMemo: GetDataCount
    /*@Doc:
	  Drop Section
	*/
    int GetDataCount();
    //@ManMemo: GetDataString
    /*@Doc:
	  Drop Section
	*/
    CString GetDataString( int nItemIndex ); 
    //@ManMemo: GetDataDWord
    /*@Doc:
	  Drop Section
	*/
    DWORD GetDataDWord( int nItemIndex );
// End Drop Section

public:                                  
	CCSDragDropCtrl();

    //void    RegisterSource( CWnd *pWnd );
    //@ManMemo: RegisterTarget
    void    RegisterTarget( CWnd *pWnd, CWnd* pcallMsgWnd, CWnd *pscrollMsgWnd = NULL);

		void CancelDrag();		// 050311 MVy: call this on events like OnLButtonUp or on VK_ESCAPE
    
protected:
    //@ManMemo: OnDragEnter
    virtual DROPEFFECT OnDragEnter( CWnd* pWnd, 
                                    COleDataObject* pDataObject,
                                    DWORD dwKeyState, CPoint point );
    //@ManMemo: OnDragEnter
    virtual void OnDragLeave( CWnd* pWnd );

    //@ManMemo: OnDragOver
    virtual DROPEFFECT OnDragOver ( CWnd* pWnd, 
                                    COleDataObject* pDataObject,
                                    DWORD dwKeyState, CPoint point );
    //@ManMemo: OnDrop
    virtual BOOL       OnDrop(  CWnd* pWnd, 
                                COleDataObject* pDataObject,
                                DROPEFFECT dropEffect,
                                CPoint point );



};

#endif  //__DGDPCTRL_H__
