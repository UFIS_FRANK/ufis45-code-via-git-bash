// AatHelp.h: interface for the AatHelp class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AATHELP_H__8938D163_623D_11D6_8202_00010215BFDE__INCLUDED_)
#define AFX_AATHELP_H__8938D163_623D_11D6_8202_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class AatHelp  
{
private:	// Construction && Destruction
	AatHelp(const CString& ropAppName);
	AatHelp(const AatHelp& rhs);
	virtual ~AatHelp();
public:	// Operations
	enum State
	{
		OK = 0,
		NOT_INITIALIZED = 1,
		HELPDIRINVALID	= 2,
		HELPFILEINVALID	= 3,
		FAILED			= 4,
	};

	static BOOL		Initialize(const CString& ropAppName);
	static void		WinHelp(DWORD dwData,UINT nCmd);
	static State	GetLastError(CString& ropError);
	static void		ExitApp();
private: // Helpers
private:
	HWND	hmMainWnd;
	State	omState;	
	DWORD	dmCookie;
	CString	omFileName;	
	static	AatHelp*	pomThis;
};

#endif // !defined(AFX_AATHELP_H__8938D163_623D_11D6_8202_00010215BFDE__INCLUDED_)
