#ifndef _CCSLOG_H_
#define _CCSLOG_H_


#ifdef HAVE_TEMPLATES
#else
#include <assert.h>
#endif

#include <afxwin.h>

class ostream;

//Man:  TimeStamp
//Memo: date + time stuff
//@{
///Returns time of day in #yyyymmddHHMMSS# format
extern CString TimeStamp();

///Returns time of day in #HH:MM:SS[:mmm]# format (millisecs optional)
extern CString TimeOfDayStamp(bool millisecs = true);
//@}

//@Man:
//@See: CCSObject
//@Memo:powerful logging mechanism
/*@Doc: This class implements a highly configurable logging mechanism. It
  will be used in applications to log debug messages, trace information
  and error messages.

  The application writes to the logfile via the #Debug(..)#, #Trace(..)#
  and #Error(..)# methods. The configuration can be done from inside
  the application as well as through configuration files.

  Whether a specific log is done depends on the {\bf filter settings}.
  Filters are assigned to {\em keys}, which are simply strings.
  The filter settings for #key# affect the behaviour of calls to
  #Debug(key, ..)# and #Trace(key, ..)#.
  \begin{itemize}

  \item For trace messages, the filter has a logical truth value:
        if #TraceFilter(key)# is #true# ,then
	#log.Trace(key, "Hello!")# will produce no output. However,
	it would have said "Hello!", if #log.TraceFilter(key)# were #false#.
	This could have been achieved via #log.TraceFilter(false, key)#.

  \item For debug messages, the filter can have one of the values
        #None#, #Some#, #Most#, #All#. #log.DebugFilter(key) == filter#
	means that #log.Debug(key, level, "Hello!")# would produce output
	if and only if #level > filter#. Eg, the filter could have been
	set with #log.DebugFilter(CCSLog::Some, key)#. Then
	#log.Debug(key, CCSLog::Some, "Hello!")# would say nothing, but
	#log.Debug(key, CCSLog::Most, "Hello!")# would say "Hello!" to you.

	\item For an unknown key #unknown#, maximal filtering will be assumed.
  \end{itemize}

  There is one predefined key, namely #*#. The associated filter values
  should be seen as the default filter values. Consequently, the key #*#
  should be used for 'normal' logs.

  The filter settings may be loaded from a configuration file. The file
  is read line by line. Empty lines and lines starting with \# are ignored.
  All other lines are of the form
  \begin{center}
         #key.token : value#
  \end{center}
  where
  \begin{itemize}
  \item #token# is one of #output#, #debug# and #trace#.
  \item If #token# is #debug# or #trace# then #value# is a valid filter.
  \item If #token# is #output#, then #key# {\bf must} be #*#
        (this may change) and #value# gives the name of the file,
        where future logs will go.
  \end{itemize}
  Here's an example (ignore '#\#', please):
  \begin{verbatim}
  \# send output to...
  *.Output:	/tmp/logfile

  \# default debug/trace filters
  *.Debug:	Some		\# filter debugs with level <= Some
  *.Trace:	true		\# filter traces

  \# no filters for key "test"
  test.debug:	None		\# no debug filter --> full debug
  test.trace:	false		\# no trace filter --> trace on
  \end{verbatim}
*/
class CCSLog : public CObject
{
  public:

    //@ManMemo: Enumeration of debug filter levels.
    /*@ManDoc:
	Possible values are:
	\begin{itemize}
	\item None
    \item Some
    \item Most
    \item All
    \end{itemize}
    */
    enum Level {
      None,
      Some,
      Most,
      All
    };
  
    //@ManDoc: Constructor, takes optional configuration file name.
    CCSLog(const char *configFile = 0, const char *pcpAppName = NULL);
    virtual ~CCSLog();

	void SetAppName(CString opName);
    

    //@ManDoc: Get trace filter for key
    bool TraceFilter(const char *key = "*") const;

    //@ManDoc: Set trace filter for key
    void TraceFilter(bool trace, const char *key = "*");

    //@ManDoc: Get debug filter for key
    Level DebugFilter(const char *key = "*") const;

    //@ManDoc: Set debug filter for key
    void  DebugFilter(Level level, const char *key = "*");

    /*@ManDoc:
      Debug object.
      the log is done if #level > DebugFilter(key)#.
    */
    void Debug(const char *key, Level level, const CObject& obj) const;

    /*@ManDoc:
      Debug #printf(3)#-like debug message.
      The log is done iff #level > DebugFilter(key)#.
    */
    void Debug(const char *key, Level level, const char *format ...) const;

    /*@ManDoc:
      Debug raw data (snap dump).
      The log is done iff #level > DebugFilter(key)#.
    */
    void Snap(const char *key, Level level,
	       const void *addr, int bytes = 64, int width = 16) const;

    /*@ManDoc:
      Trace #printf(3)#-like trace message.
      The log is done iff #TraceFilter(key) == false#.
    */
    void Trace(const char *key, const char *format ...) const;

    //@ManDoc: Log #printf(3)#-like error message. The log is done in any case.
    void Error(const char *format ...) const;


    //@ManDoc: Load configuration from file.
    void Load(const char *configFile);

    //@ManDoc: Dump #*this# to stream
    void Dump(ostream& os, const char *label) const;

	//@ManDoc: gets the path of the c:\tmp directory which can be specified in the TMP or TEMP environment variable
	static CString GetTmpPath(void);

	//@ManDoc: gets the path of the c:\tmp directory which can be specified in the TMP or TEMP environment variable adds additional text specified by the user to the end of the path
	static CString GetTmpPath(const char *pcpAdditionalText);

	//@ManDoc: gets the path of the c:\ufis\system directory which can be specified in the CEDA environment variable
	static CString GetUfisSystemPath(void);

	//@ManDoc: gets the path of the c:\tmp directory which can be specified in the CEDA environment variable adds additional text specified by the user to the end of the path
	static CString GetUfisSystemPath(const char *pcpAdditionalText);



  private:

    ostream *pomOStream;

    static ostream * const pomDefaultOStream;
	
	char pcmAppName[128];

    class KeyTableEntry
    {
       CString key;
       bool trace;
       Level level;

     public:

       KeyTableEntry() : key(""), trace(true), level(All) {}
       CString& Key() { return key; }
       const CString& Key() const { return key; }
       bool& Trace() { return trace; }
       bool Trace() const { return trace; }
       Level& Debug() { return level; }
       Level Debug() const { return level; }
    };

#ifdef HAVE_TEMPLATES
    CCSArray<KeyTableEntry> omTable; // table for filter values
#else
    class KeyTable // hack for the non haves!!!
      {
	KeyTableEntry tab[64];
	unsigned size;
  
      public:

	KeyTable() : size(0) {}
	KeyTableEntry& operator[](unsigned i)
	{
	  assert(size < 64);
	  if ((unsigned)i >= size)
	    size = i+1;
	  return tab[i];
	}
	const KeyTableEntry& operator[](unsigned i) const
	{
	  assert(size < 64);
	  return tab[i];
	}
	unsigned Size() const { return size; }
    } omTable;
#endif

    int  InsertKey(const char *pcpKey);      // insert key into table --> index
    int  GetIndex(const char *pcpKey) const; // --> table index for key (or -1)
};


#endif
