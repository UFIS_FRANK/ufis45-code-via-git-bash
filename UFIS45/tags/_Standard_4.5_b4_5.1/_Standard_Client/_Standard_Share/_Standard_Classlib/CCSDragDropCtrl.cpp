
#include <CCSDefines.h>
#include <ccsdragdropctrl.h>

UINT	CCSDragDropCtrl::umScrollInset = 15;
UINT	CCSDragDropCtrl::umScrollDelay = 30;

CCSDragDropCtrl::CCSDragDropCtrl()
{
	m_hDataSource = NULL;
	nCurPosData   = -1;
	lpDataDragDrop= NULL;
	lpDataString  = NULL;
	m_callMsgWnd  = NULL;
	m_scrollMsgWnd= NULL;
	wmTimeLast = 0;
	umLastTest = UDROP_INSETNONE;
	umLast	   = 0;
	umVScrollCode = 0xffff;
	umHScrollCode = 0xffff;

}

void CCSDragDropCtrl::RegisterTarget( CWnd* pWnd, CWnd* pcallMsgWnd,CWnd *pScrollMsgWnd )
{   
    m_callMsgWnd   = pcallMsgWnd;
	m_scrollMsgWnd = pScrollMsgWnd;
    Register( pWnd );
}




UINT CCSDragDropCtrl::TestDroppablePoint(CWnd *popWnd,const CPoint& ropPnt)
{
	CRect olRect;
	popWnd->GetClientRect(olRect);
	UINT uRet = UDROP_INSETNONE;

	if (olRect.PtInRect(ropPnt))
	{
		if (ropPnt.x <= olRect.left + (INT) umScrollInset)
			uRet |= UDROP_INSETLEFT;
		else if (ropPnt.x >= olRect.right - (INT) umScrollInset)
			uRet |= UDROP_INSETRIGHT;
		if (ropPnt.y <= olRect.top + (INT) umScrollInset)
			uRet |= UDROP_INSETTOP;
		else if (ropPnt.y >= olRect.bottom - (INT) umScrollInset)
			uRet |= UDROP_INSETBOTTOM;
	}

	if (uRet == UDROP_INSETNONE)
		TRACE("TestDroppablePoint outside Rect = (%d,%d - %d,%d), Pt=(%d,%d)\n",olRect.TopLeft().x,olRect.TopLeft().y,olRect.BottomRight().x,olRect.BottomRight().y,ropPnt.x,ropPnt.y);
	else
		TRACE("TestDroppablePoint inside  Rect = (%d,%d - %d,%d), Pt=(%d,%d)\n",olRect.TopLeft().x,olRect.TopLeft().y,olRect.BottomRight().x,olRect.BottomRight().y,ropPnt.x,ropPnt.y);
	return uRet;
}

DROPEFFECT CCSDragDropCtrl::OnDragEnter( CWnd* pWnd, 
                                         COleDataObject* pDataObject,
                                         DWORD dwKeyState, CPoint point )
{
    m_callMsgWnd->SendMessage( WM_DRAGENTER, dwKeyState, (LPARAM)pWnd );

    return OnDragOver( pWnd, pDataObject, dwKeyState, point );
}                                         


void CCSDragDropCtrl::OnDragLeave( CWnd* pWnd )
{
    m_callMsgWnd->SendMessage( WM_DRAGLEAVE, 0, (LPARAM)pWnd );
}                                         


                                         
DROPEFFECT CCSDragDropCtrl::OnDragOver ( CWnd* pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point )
{
    DROPEFFECT  dropEffect = DROPEFFECT_NONE;

    if ( pDataObject->IsDataAvailable(CF_TEXT) ) {

		//bool    bRet    = false;
		HGLOBAL m_hDataTarget = pDataObject->GetGlobalData( CF_TEXT );
		lpDataDragDrop  = (LPDATADRAGDROP) GlobalLock( m_hDataTarget );
		lpDataString    = (LPSTR)GlobalLock( m_hDataTarget );


		if (m_callMsgWnd->SendMessage( WM_DRAGOVER, dwKeyState, (LPARAM)pWnd ) != -1)	// no dropping?
		{
	        dropEffect = (dwKeyState & MK_CONTROL)? DROPEFFECT_COPY: DROPEFFECT_MOVE;
			// the above line is the default action of the drop action
		}
		GlobalUnlock( m_hDataTarget );
		GlobalUnlock( m_hDataTarget );
		GlobalFree( m_hDataTarget );

		if (m_scrollMsgWnd)
		{
			UINT uRet = TestDroppablePoint(pWnd,point);
			if (	(point.x == omLastPick.x)
				&&	(point.y == omLastPick.y)
				&&	(!((UDROP_INSETHORZ|UDROP_INSETVERT) & umLastTest)))
			{
				TRACE("TestDroppablePoint Point = (%d,%d)\n",point.x,point.y);			
				return dropEffect;
			}

			UINT uLast = umLastTest;
			umLastTest = uRet;
			
			if (uRet == UDROP_INSETNONE)
			{
				umVScrollCode = 0xffff;
				umHScrollCode = 0xffff;
				TRACE("TestDroppablePoint uRet == UDROP_INSETNONE\n");			
				return dropEffect;
			}

			if ((uLast & UDROP_INSETHORZ) && !(uRet & UDROP_INSETHORZ))
			{
				TRACE("Last ==  UDROP_INSETHORZ && uRet != UDROP_INSETHORZ\n");			
				umHScrollCode = 0xffff;
			}

			if (!(uLast & UDROP_INSETHORZ) && (uRet & UDROP_INSETHORZ))
			{
				TRACE("Last !=  UDROP_INSETHORZ && uRet == UDROP_INSETHORZ\n");			
				wmTimeLast    = GetTickCount();
				umHScrollCode = (0 != (UDROP_INSETLEFT & uRet)) ? SB_LINELEFT : SB_LINERIGHT;
			}

			if ((uLast & UDROP_INSETVERT) && !(uRet & UDROP_INSETVERT))
			{
				TRACE("Last ==  UDROP_INSETVERT && uRet != UDROP_INSETVERT\n");			
				umVScrollCode = 0xffff;
			}

			if (!(uLast & UDROP_INSETVERT) && (uRet & UDROP_INSETVERT))
			{
				TRACE("Last !=  UDROP_INSETVERT && uRet == UDROP_INSETVERT\n");			
				wmTimeLast    = GetTickCount();
				umVScrollCode = (0 != (UDROP_INSETTOP & uRet)) ? SB_LINEUP : SB_LINEDOWN;
			}

			if (umHScrollCode == 0xffff && umVScrollCode == 0xffff)
			{
				TRACE("NO SCROLLING\n");			
				wmTimeLast = 0;
			}

			if (wmTimeLast != 0 && (GetTickCount() - wmTimeLast) > umScrollDelay)
			{
				if (umHScrollCode != 0xffff)
				{
					HRESULT hResult = m_scrollMsgWnd->SendMessage(WM_HSCROLL,umHScrollCode,0l);
					TRACE("SendMessage WM_HSCOLL,ec = %d\n",hResult);			
					m_scrollMsgWnd->UpdateWindow();
				}

				if (umVScrollCode != 0xffff)
				{
					HRESULT hResult = m_scrollMsgWnd->SendMessage(WM_VSCROLL,umVScrollCode,0l);
					TRACE("SendMessage WM_VSCOLL,ec = %d\n",hResult);			
					m_scrollMsgWnd->UpdateWindow();
				}
			}

			omLastPick = point;
		}
	}
    return dropEffect;
}                                         

// 050311 MVy: call this on events like OnLButtonUp or on VK_ESCAPE
void CCSDragDropCtrl::CancelDrag()
{

};	// CancelDrag

BOOL CCSDragDropCtrl::OnDrop( CWnd* pWnd, 
                            COleDataObject* pDataObject,
                            DROPEFFECT dropEffect,
                            CPoint point )
{
    //bool    bRet    = false;
    HGLOBAL m_hDataTarget = pDataObject->GetGlobalData( CF_TEXT );
    lpDataDragDrop  = (LPDATADRAGDROP) GlobalLock( m_hDataTarget );
    lpDataString    = (LPSTR)GlobalLock( m_hDataTarget );

    m_callMsgWnd->SendMessage( WM_DROP, dropEffect, (LPARAM)pWnd );

    GlobalUnlock( m_hDataTarget );
    GlobalUnlock( m_hDataTarget );
    GlobalFree( m_hDataTarget );
    //return bRet;
	return TRUE;
}    

void CCSDragDropCtrl::BeginDrag()
{                      
    CRect rect(0, 0, 0, 0);
    m_dataSource.CacheGlobalData( CF_TEXT, m_hDataSource, NULL  );
    DROPEFFECT dropEffect = m_dataSource.DoDragDrop( 
                            DROPEFFECT_COPY | DROPEFFECT_MOVE, NULL );
	
    //GlobalFree( m_hDataSource );
}

void CCSDragDropCtrl::CreateStringData( int nTypeSource, UINT uItemSize, UINT uMaxItem )
{   
    m_hDataSource = GlobalAlloc( GMEM_FIXED | GMEM_SHARE, 
                                 sizeof( DATADRAGDROP ) + (uMaxItem * uItemSize) );
    if( m_hDataSource ) {
        lpDataDragDrop = (LPDATADRAGDROP)GlobalLock( m_hDataSource );
        lpDataDragDrop->nTypeDragDrop   = nTypeSource;
        lpDataDragDrop->nNumItem        = uMaxItem;
        lpDataDragDrop->nItemSize       = uItemSize;
        GlobalUnlock( m_hDataSource );
    }
        
    nCurPosData = 0;
    
}

void CCSDragDropCtrl::CreateDWordData( int nTypeSource, UINT uMaxItem )
{
    CreateStringData( nTypeSource, 30, uMaxItem );
}


void CCSDragDropCtrl::AddString( const char *pStr )
{ 
    lpDataDragDrop = (LPDATADRAGDROP)GlobalLock( m_hDataSource );
    if( (nCurPosData / lpDataDragDrop->nItemSize) < lpDataDragDrop->nNumItem ) {
        LPSTR lpStr = (LPSTR)GlobalLock( m_hDataSource );
        strcpy( lpStr + sizeof( DATADRAGDROP ) + nCurPosData, pStr );
        GlobalUnlock( m_hDataSource );
        nCurPosData += lpDataDragDrop->nItemSize;
    }
    GlobalUnlock( m_hDataSource );
}

void CCSDragDropCtrl::AddDWord( DWORD  dItem )
{
    char    cBuffer[20];
    
    _ltoa( dItem, cBuffer, 10 );
    AddString( cBuffer );
}

int CCSDragDropCtrl::GetDataClass()
{
	ASSERT( lpDataDragDrop );		// 050311 MVy: tell me if you can
	return lpDataDragDrop->nTypeDragDrop;
}                  

int CCSDragDropCtrl::GetDataCount()
{
	if( !lpDataDragDrop ) return 0 ;		// 050311 MVy: starting a drag while a drag is currently running is bad, need some universal functionality to check
	return lpDataDragDrop->nNumItem;
}

CString CCSDragDropCtrl::GetDataString( int nItemIndex )
{         
    CString RetStr;
    HGLOBAL hStr;
    LPSTR   lpStr;
    
    hStr  = GlobalAlloc( GPTR, lpDataDragDrop->nItemSize );
    lpStr = (LPSTR)GlobalLock( hStr );
    if(  nItemIndex < lpDataDragDrop->nNumItem ) {
        strncpy( lpStr, 
                   lpDataString + sizeof( DATADRAGDROP ) + ( lpDataDragDrop->nItemSize * nItemIndex ),
                   lpDataDragDrop->nItemSize );
    }
    RetStr = lpStr;
    GlobalUnlock( hStr );
    GlobalFree( hStr );
    return RetStr;
}


DWORD CCSDragDropCtrl::GetDataDWord( int nItemIndex )
{           
    char s[20];   
    strcpy( s, GetDataString( nItemIndex ) );
    return atol( s ); 
}
