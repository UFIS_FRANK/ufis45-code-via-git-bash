
#ifndef _CCSCEDADATA_H_
#define _CCSCEDADATA_H_

#include <ccsptrarray.h>
#include <CCSTime.h>
#include <ccslog.h>
#include <ccscedacom.h>
#include <RecordSet.h>
#include <FastCedaConnection.h>
#include <bcproxy.h>
#include <bccomserver.h>

extern char pcgDataBuf[];


//@Man:
//@Memo: Provide methods for CEDA services and data exchange methods
//@See:  CCSCedaCom, CedaXXXData, UFIS.DLL
/*@Doc:
  This is the base class communication with CEDA server. Provides methods for
  CEDA services and data exchange methods. Uses UFIS.DLL.

  Notes: It is the responsibility for the caller to make sure that the application
  is correctly connected to the server by using the CedaCom class.

  There are many changes in this class since the first draft of the specification.
  This class is now just maintain internal array of comma-separated string. Many methods
  are now made public to ease us using this CedaData class to retrieve some small data
  directly without deriving any class. (For example, PreplanTable will need to retrieve
  assignment data for each employee. Without making CedaAction() and GetBufferLine()
  public, PreplanTable have to implement new class derived from CedaData before being
  able to read data from the server.)
*/

typedef struct {
    int Type;   // see below
    int Offset; // from the beginning of the record (started from zero-offset)
    int Length; // in bytes
    int Flags;  // see below
	char Name[6];
	char cmDescription[128];
	char cmFormat[128];
	int imDisplay;   // 26.11.97 djs wenn 1.. dann Anzeige, wenn 0.. keine Anzeige
} CEDARECINFO;



    //@ManMemo: Possible values of CEDARECINFO.Flags
    /*@Doc:
	  Possible values of CEDARECINFO.Flags (plus multiple flags together) are:
	  \begin{itemize}
	  \item {\bf TOTRIMLEFT = 0x0001}
	  \item {\bf TOTRIMRIGHT = 0x0002}
	  \end{itemize}
    */
    enum { TOTRIMLEFT = 0x0001, TOTRIMRIGHT = 0x0002, FILLBLANK  = 0x0003};

typedef	struct EXCELVALUEINFO
{
	CString						omHeaderText;		// the header text to be displayed for this value
	CStringArray				omValues;			// the values to be displayed

	struct EXCELVALUEINFO& operator=(const struct EXCELVALUEINFO& rhs)
	{
		if (this != &rhs)
		{
			this->omHeaderText = rhs.omHeaderText;
			this->omValues.Append(rhs.omValues);
		}

		return *this;
	}

}	EXCELVALUEINFO;
	
typedef	struct
{
	CString	omName;				// the field name
	CString	omHeaderText;		// the header text to be displayed for this field
}	EXCELFIELDINFO;

typedef	struct
{
	CString	omName;				// the table name
	CString	omHeaderText;		// the header text to be displayed for this table
	CCSPtrArray<EXCELFIELDINFO>	omFields;	// the fields to be displayed
	CPtrArray					omData;		// the data to be displayed
}	EXCELTABLEINFO;

typedef	struct
{
	bool						bmUseAutomation;// use excel automation for output	
	CString						omExcelPath;	// the excel file name, if no automation is used
	CString						omDelimiter;	// the delimiter for each field, if no automation is used
	CString						omListSeparator;// the record separator, if no automation is used
	EXCELTABLEINFO				omTable;		// the main table to be displayed
	CStringArray				omUrnos;		// list of urnos to be displayed
	CCSPtrArray<EXCELVALUEINFO>	omAdditional;	// the additional values to be displayed
}	EXCELINFO;


class CCSCedaData: public CObject//CCmdTarget//CObject
{
// Attributes
public:
    //@ManMemo: Last returned code from UFIS.DLL
    //@See: UFIS.DLL, CedaData::LastError()
    /*@Doc:
      The {\bf imLastReturnCode} data member is a public variable containing the
      last returned code from ::InitCom(), ::CleanupCom(), and ::CallCeda().
    */
    int imLastReturnCode;
 
	long lmBaseID;
	static IBcPrxy* CCSCedaData::smBcProxy;
	// Instantiate an object of the class which enables fast TCP/IP communication
	static FastCedaConnection* CCSCedaData::pomFastCeda;

	//flag to use CDRHDL as writer without NETIN
	static bool CCSCedaData::bmUseNETIN;

protected:
	static int imInstCount;
	
public:

    //@ManMemo: Last received error message from UFIS.DLL
	CString omLastErrorMessage;
	char pcmExtSelection[10240];
	char pcmExtFields[4096];

    //@ManMemo: Meaning of #imLastReturnCode#
    /*@Doc:
	  The following returncodes are defined:
	  \begin{itemize}
      \item  {\bf RC_SUCCESS	  =  0}    Everthing OK
      \item  {\bf RC_FAIL		  = -1}    General serious error
      \item  {\bf RC_COMM_FAIL	  = -2}    WINSOCK.DLL error
      \item  {\bf RC_INIT_FAIL	  = -3}    Open the log file
      \item  {\bf RC_CEDA_FAIL	  = -4}    CEDA reports an error
      \item  {\bf RC_SHUTDOWN	  = -5}    CEDA reports shutdown
      \item  {\bf RC_ALREADY_INIT = -6}    InitComm called up for a second line
      \item  {\bf RC_NOT_FOUND	  = -7}    Data not found (More than 7 applications are running)
      \item  {\bf RC_DATA_CUT	  = -8}    Data incomplete
      \item  {\bf RC_TIMEOUT	  = -9}    CEDA reports a time-out
	  \end{itemize}
	*/
    enum {
        RC_SUCCESS = 0,         // Everthing OK
        RC_FAIL = -1,           // General serious error
        RC_COMM_FAIL = -2,      // WINSOCK.DLL error
        RC_INIT_FAIL = -3,      // Open the log file
        RC_CEDA_FAIL = -4,      // CEDA reports an error
        RC_SHUTDOWN = -5,       // CEDA reports shutdown
        RC_ALREADY_INIT = -6,   // InitComm called up for a second line
        RC_NOT_FOUND = -7,      // Data not found (More than 7 applications are running)
        RC_DATA_CUT = -8,       // Data incomplete
        RC_TIMEOUT = -9         // CEDA reports a time-out
    };

// Operations
public:
    //@ManMemo: Default constructor
    /*@Doc:
	  Initializes attributes to default values    
	*/
    CCSCedaData(CCSCedaCom *popCommHandler = NULL);
    //@ManMemo: Default destructor
    ~CCSCedaData();

    void SetCommHandler(CCSCedaCom *popCommHandler = NULL);


    //@ManMemo: Get the next Urno
	long CCSCedaData::GetNextUrno(void);

    //@ManMemo: Short form of CedaAction()
    //@See: Long form of CedaData::CedaAction
    //@See: CedaData::pcmTableName, CedaData::pcmFieldList
    //@See: CedaData::pcmSelection, CedaData::pcmSort
    /*@Doc:
      This is the short and handy form to do CedaAction(). The derived class can use this
      method instead of calling CedaAction() with many parameters. It will call the long
      form CedaAction() with default table name, field list, selection criteria, and sort
      order. If unspecified, field list will be "*" -- all fields, selection criteria
      will be "" -- all records, and sort order will be "" -- no sort order.

      Return: RCSuccess, RCFailure
    */

    bool CedaAction(char *pcpAction,
        char *pcpSelection = NULL, char *pcpSort = NULL,
        char *pcpData = pcgDataBuf,char *pcpDest = "BUF1",
				bool bpIsWriteAction = false, long lpID = 0);

    //@ManMemo: Provides CEDA services (e.g. read, update, insert, get URNO etc.)
    //@See: UFIS.DLL
    /*@Doc:
      Call {\bf CallCeda} in {\bf UFIS.DLL} with the appropiate parameters. Insert missing
      parameters from CCSConfig or with default values (see documentation on UFIS.DLL and
      CCSConfig for details). This method uses {\bf pcpData} for update/insert data only.
      If the {\bf pcpAction} is retrieving/deleting record, the {\bf pcpData} will has no
      meaning and can be to anything. On success, retrieve data from the internal buffer
      of UFIS.DLL to create an array of comma-separated string.

      Programmer Notes:
      Right now, the possible {\bf pcpAction} values are RTA, URT, IRT, DRT, and GNU.
      RTA for SQL - SELECT, URT for SQL - UPDATE, IRT for SQL - INSERT, DRT for SQL - DELETE,
      and GNU is a special action of UFIS.DLL to get a URNO (Unique Record Number).

      For the GNU action, you need to initialize some spaces in the {\bf pcpData} buffer.
      (It cannot be an empty string. Normally, it should a 10 character spaces followed by
      a null character. The reason (according to Ralph) is to let the UFIS.DLL know the
      length of the buffer to store the URNO back.

      Return: RCSuccess, RCFailure
    */
	bool CedaRereadBc(struct BcStruct *prlBcData);

    bool CedaAction(char *pcpAction,
        char *pcpTableName, char *pcpFieldList,
        char *pcpSelection, char *pcpSort,
         char *pcpData, char *pcpDest = "BUF1",bool bpIsWriteAction = false, long lpID = 0, CCSPtrArray<RecordSet> *pomData = NULL, int ipFieldCount = 0, CString add = "");


    //@ManMemo: Retrive one line of data from the read buffer
    //@See: CedaData::GetBufferRecord
    /*@Doc:
      Retrive one line of data from the read buffer. Does not change the format of the data.
      Copies the requested line of {\bf omDataBuf} to {\bf pcpListOfData}.

      Return: RCSuccess, RCFailure
    */
    bool GetBufferLine(int ipLineNo, CString &pcpListOfData);
	/*MWO
	Uses the alternative way of ceda communication for accelerated reading
	by using the cdrhdl
	*/

	static CString CCSCedaData::omServerName;

	bool CedaAction2(char *pcpAction,
					char *pcpSelection = NULL, char *pcpSort = NULL,
					char *pcpData = pcgDataBuf,char *pcpDest = "BUF1",
					bool bpIsWriteAction = false, long lpID = 0, bool bpDelDataArray = true, 
					char* tws = NULL, char* twe = NULL, char* pcpOraHint = NULL);

	bool ReadDataAlternative(CString &ropAppl, CString &ropCommand, CString &ropFields, CString &ropData,
							 CString &ropHopo, CString &ropIdentifier, CString &ropPort,
							 CString &ropServer,	CString &ropTabext,	CString &ropTable,
							 CString &ropUser,	CString &ropWhere, CString &ropWks, 
							 CString &ropErrorText,	CString &ropSepa, CString &ropTws, CString &ropTwe,
							 CString &ropOraHint);

	
    bool GetBufferLine2(int ipLineNo, CString &pcpListOfData);


    //@ManMemo: Retrive one record of data from the read buffer
    //@See: CedaData::GetBufferLine, CedaData::omRecInfo
    /*@Doc:
      Retrieve one record of data from the read buffer and split it into fields (separated
      by comma). Before using this method, the derived-class must initialize {\bf omRecInfo}
      already. Convert each field to the data type {\bf omRecInfo[].Type}. Process the flags
      in {\bf omRecInfo[].Flags} ({\bf TOTRIMLEFT}, {\bf TOTRIMRIGHT}) and copy the result
      into {\bf pvpDataStruct} at offset {\bf omRecInfo[].Offset}.

      Warning: Since we use CTime as our date field in C struct, our date will be valid
      only between midnight January 1, 1970 and midnight February 5, 2036.

      Return: RCSuccess, RCFailure
    */
    bool GetBufferRecord(int ipLineNo, void *pvpDataStruct);
    bool GetBufferRecord(int ipLineNo, void *pvpDataStruct, CString &olFieldList);
    bool GetBufferRecord(CCSPtrArray<CEDARECINFO> *pomRecInfo, int ipLineNo, void *pvpDataStruct);
	bool GetBufferRecord(CCSPtrArray<CEDARECINFO> *pomRecInfo, CString record, void *pvpDataStruct);
	bool GetBufferRecord(CCSPtrArray<CEDARECINFO> *pomRecInfo, CString record, void *pvpDataStruct,CString &olFieldList);
    bool GetBufferRecord(CCSPtrArray<CEDARECINFO> *pomRecInfo, int ipLineNo, void *pvpDataStruct, CString &olFieldList);

    bool GetFirstBufferRecord(void *pvpDataStruct);
    bool GetFirstBufferRecord(CCSPtrArray<CEDARECINFO> *pomRecInfo, void *pvpDataStruct);


    bool GetBufferRecord2(int ipLineNo, void *pvpDataStruct);//
	bool GetBufferRecord2(int ipLineNo, void *pvpDataStruct, CString olFieldList); //
    bool GetBufferRecord2(CCSPtrArray<CEDARECINFO> *pomRecInfo, int ipLineNo, void *pvpDataStruct); //
	bool GetBufferRecord2(CCSPtrArray<CEDARECINFO> *pomRecInfo, CString record, void *pvpDataStruct); //
	bool GetBufferRecord2(CCSPtrArray<CEDARECINFO> *pomRecInfo, CString record, void *pvpDataStruct, CString &olFieldList);
	bool GetBufferRecord2(CCSPtrArray<CEDARECINFO> *pomRecInfo, int ipLineNo, void *pvpDataStruct, CString &olFieldList);

	bool GetFirstBufferRecord2(void *pvpDataStruct);
	void ClearFastSocketBuffer();

    //@ManMemo: Meaning of the last returned code from UFIS.DLL
    //@See: UFIS.DLL, CedaCom::imLastReturnCode
    /*@Doc:
      Return the meaning of the last returned code from UFIS.DLL. This method uses the value
      kept in {\bf imLastReturnCode} data member, then returns a CString which is the
      interpreted meaning of the {\bf imLastReturnCode}.
    */
	CString LastError(void);
    CStringArray * GetDataBuff();
	void CheckDataArea(char *pcpDest,char *pcpSrc);

	bool	GenerateExcelOutput(const EXCELINFO& ropExcelInfo);
	bool	GenerateExcelOutput1(const EXCELINFO& ropExcelInfo);
	CString	GetTableName();
	CString	GetFieldList();

    int GetBufferSize(void);

	// Attributes needed to be initialized by the derived-class
protected:
    //@Man: CedaData::CEDARECINFO
    //@Memo: Record structure description -- set by derived-class
    //@See: CedaData::omRecInfo, BEGIN_CEDARECINFO
    /*@Doc:
      Originally, CedaData was designed to be a pure base class. The derived class have
      to define the data member {\bf CedaData::omRecInfo} to let the CedaData class know
      the layout of the record structure. To do so, the derived class have to set type,
      offset, length, and flags of each fields. In stead of setting type, flags and
      calculating offset by hand, I have provided a set of macros to ease this job. See
      the details in BEGIN_CEDARECINFO.
    */

    //@ManMemo: Possible values of CEDARECINFO.Type
    /*@Doc:
	  Possible values of CEDARECINFO.Type are:
	  \begin{itemize}
	  \item {\bf CEDACHAR}
	  \item {\bf CEDAINT}
	  \item {\bf CEDALONG}
	  \item {\bf CEDADATE}
	  \item {\bf CEDADOUBLE}
	  \end{itemize}
    */
    enum { CEDACHAR, CEDAINT, CEDALONG, CEDADATE, CEDAOLEDATE, CEDADOUBLE};


    //@ManMemo: Record layout descriptions 
    //@See: CedaData::CEDARECINFO
    /*@Doc:
      This array must be defined by the derived-class before calling GetBufferRecord(),
      WriteBufferRecord() and MakeCedaData(). Each entry in this array specifies each field
      in the comma-separated string record.
    */
    CCSPtrArray<CEDARECINFO> omRecInfo;


    //@ManMemo: Default table name for CedaAction() -- set by derived-class
    char pcmTableName[24];
    //@ManMemo: Default field list for CedaAction() -- set by derived-class
    char *pcmFieldList;
	CString myFieldList;
	//CString pcmFieldList;
    //@ManMemo: Default selection criteria for CedaAction() -- set by derived-class
    char *pcmSelection;
    //@ManMemo: Default sort order for CedaAction() -- set by derived-class
    char *pcmSort;

public:
	CCSPtrArray<CEDARECINFO> *GetRecInfoPtr(){return &omRecInfo;};

// Private attributes
private:
    //@ManMemo: CEDA data buffer (array of record, comma-separated string)
    CStringArray omDataBuf;
// Protected attributes
protected:
    //@ManMemo: equest ID, we need this member variable for rereading broadcasts
	CString omReqId;    

	CMapStringToPtr omFieldMap;

	CCSCedaCom *pomCommHandler;

protected:



	bool FillRecord(void *pvpDataStruct, CString &olFieldList, CString &olDataList);
	
    //@ManMemo: Convert {\bf pvpDataStruct} to comma-separated string
    /*@Doc:
      Convert {\bf pvpDataStruct} to comma-separated textline (reverse to what the
      {\bf CedaData::GetBufferRecord} do), and write it to {\bf pcpListOfData}.

      Return: RCSuccess, RCFailure
    */

    
	bool EqualizeDataSruct(void *pvpBcDataStruct, void *pvpSaveDataStruct, void *pvpChangedDataStruct);

	bool GenerateAutomatedExcelOutput(const EXCELINFO& ropExcelInfo);
	bool GenerateStreamedExcelOutput(const EXCELINFO& ropExcelInfo);

	bool GenerateAutomatedExcelOutput1(const EXCELINFO& ropExcelInfo);
	bool GenerateStreamedExcelOutput1(const EXCELINFO& ropExcelInfo);


public:
	bool MakeCedaData(CCSPtrArray<CEDARECINFO> *pomRecInfo,	CString &pcpListOfData, CString &pcpFieldList, void *pvpSaveDataStruct, void *pvpChangedDataStruct);
	bool MakeCedaData(CString &pcpListOfData, CString &pcpFieldList, void *pvpSaveDataStruct, void *pvpChangedDataStruct);
	
	
	bool MakeCedaData(CString &pcpListOfData, void *pvpDataStruct);
    bool MakeCedaData(CCSPtrArray<CEDARECINFO> *pomRecInfo,CString &pcpListOfData, void *pvpDataStruct);

	bool CCSCedaData::GetItem(int ipItemNo,char *pcpItemList,char *pcpItemData);

    //@ManMemo: retrieves the URNO from selection
    /*@Doc:
	  retrieves the URNO from selection in form: "WHERE URNO = '4711'"
	  works also on selection which contain the URNO only
    */
    // String routines used in data conversion
    void TrimLeft(char *s);
    void TrimRight(char *s);


	// Private methods
public:

	bool CCSCedaData::GetRecordFromItemList(CCSPtrArray<CEDARECINFO> *pomRecInfo,void *pvpDataStruct,
							char *pcpItemList,char *pcpDataList);

	bool CCSCedaData::GetRecordFromItemList(void *pvpDataStruct,
							char *pcpItemList,char *pcpDataList);


	long GetUrnoFromSelection(CString olSelection);
    
	// Load data from UFIS.DLL (in "data_dest") to "omDataBuf", returns code from UFIS.DLL
    int LoadData(char *data_dest, CCSPtrArray<RecordSet> *pomData = NULL, int ipFieldCount = 0);

    // Routines convert CString to field
    void StoreChar(const char *field, char *s, int n, int flags);
    void StoreInt(const char *field, int *ip);
    void StoreLong(const char *field, long *lp);
	void StoreDouble(const char *field, double *lp);
   void StoreDate(const char *field, CTime *pTime); // return -1 on error
   void StoreOleDate(const char *field, COleDateTime *pTime); // return -1 on error

   virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
   {;};
	static void MakeCedaString(CString &ropText);
	static void MakeClientString(char *pclText);
	static void MakeClientString(CString &ropText);

//RST aus SHA 01.09.99
//	CString omClientChars;  // 34,39,44,10,13
//	CString omServerChars;  // 176,177,178,180,179

	static CString omClientChars;  // 34,39,44,10,13
	static CString omServerChars;  // 176,177,178,180,179
	static CStringArray	*pomUfisCedaConfig;

	void ReadUfisCedaConfig();
//END RST
	bool StructLocalToUtc(void *pvpDataStruct);
	bool StructUtcToLocal(void *pvpDataStruct);

	bool StructLocalToUtc(void *pvpDataStruct,const CString &opApt4);
	bool StructUtcToLocal(void *pvpDataStruct,const CString &opApt4);

	void LocalToUtc(CTime &opTime);
	void UtcToLocal(CTime &opTime);


	static CTimeSpan omLocalDiff1;
	static CTimeSpan omLocalDiff2;
	static CTime	  omTich;

	static char pcmTableExt[4];
	static char pcmHomeAirport[4];
	static char pcmApplName[32];
	static char pcmVersion[32];
	static char pcmInternalBuild[32];

	static bool bmVersCheck;
	static CString omTmpPath;


	static char pcmUser[32];
	static char pcmReqId[32];

	void SetTableExt(CString opTableExt);
	void SetHomeAirport(CString opHomeAirport);

};


/////////////////////////////////////////////////////////////////////////////
// Helper macros for CEDARECINFO

//@Man: BEGIN_CEDARECINFO(type,variable)
//@Memo: Use the {\bf BEGIN_CEDARECINFO} to begin definition of your CEDARECINFO
//@See: CedaData::CEDARECINFO, CedaData::omRecInfo
/*@Doc:
  To declare CEDARECINFO in a class derived from CedaData, we provide a set of macros
  to make calculating offset and length clearer and easier then doing it by hand.
  (Please be warned that your calculation may not correct because the Visual C++
  compiler usually align element in a structure, then shifts the offset from your
  expectation.) To define a CEDARECINFO for your struct, just follows this example:

  \begin{verbatim}
        // Record structure
        struct yourStruct {
            char firstField[15];
            int secondField;
            char thirdField[30];
            CTime forthField;
            long fifthField;
        };

        // Create an array of CEDARECINFO (yourCedaRecInfo)
        BEGIN_CEDARECINFO(struct yourStruct, yourCedaRecInfo)
            FIELD_CHAR(firstField, 0)
            FIELD_INT(secondField)
            FIELD_CHAR(thirdField, TOTRIMRIGHT)
            FIELD_DATE(forthField, 0)
            FIELD_LONG(fifthField)
        END_CEDARECINFO

        // Copy the defined record 
        for (int i = 0; i < sizeof(yourCedaRecInfo)/sizeof(yourCedaRecInfo[0]); i++)
            omRecInfo.Add(yourCedaRecInfo[i]);

  \end{verbatim}

  In this example, we have a record structure layour named {\bf struct YourStruct}.
  Before we could be able to call {\bf CedaData::GetBufferRecord()}, we need to
  initialize the protected data member {\bf omRecInfo}. In this case, we declare an
  array named {\bf yourCedaRecInfo} to be copied to the {\bf omRecInfo} at the end of
  this example.
*/
#define BEGIN_CEDARECINFO(type,variable)\
    static type *_ceda_rec_pointer;\
    CEDARECINFO variable[] = {

//@Man: END_CEDARECINFO
//@Memo: Use the {\bf END_CEDARECINFO} to end definition of your CEDARECINFO
//@See: BEGIN_CEDARECINFO
#define END_CEDARECINFO };

//@Man: FIELD_CHAR(field,flags)
//@Memo: Use the {\bf FIELD_CHAR} to declare your character field
//@See: BEGIN_CEDARECINFO
/*@Doc: Flags could be a combination of:
  \begin{itemize}
  \item TOTRIMLEFT: Remove spaces on the left
  \item TOTRIMRIGHT: Remove spaces on the right
  \end{itemize}
  For no special treatment on a character field, just use {\bf flags} 0.
*/
#define CCS_FIELD_CHAR(fld,flags,name,desc,dispFlag) { CEDACHAR, _CEDA_OFFSET(fld), _CEDA_LENGTH(fld), (flags), name, desc,"\0", dispFlag},
#define FIELD_CHAR(fld,flags,name) { CEDACHAR, _CEDA_OFFSET(fld), _CEDA_LENGTH(fld), (flags), name, "\0","\0", 1},

//@Man: FIELD_CHAR_TRIM(field)
//@Memo: Use the {\bf FIELD_CHAR_TRIM} to declare your trimmed (both left/right) character field
//@See: BEGIN_CEDARECINFO, FIELD_CHAR
#define CCS_FIELD_CHAR_TRIM(fld,name,desc, dispFlag) { CEDACHAR, _CEDA_OFFSET(fld), _CEDA_LENGTH(fld), TOTRIMLEFT+TOTRIMRIGHT, name, desc,"\0"  , dispFlag},
#define FIELD_CHAR_TRIM(fld,name) { CEDACHAR, _CEDA_OFFSET(fld), _CEDA_LENGTH(fld), TOTRIMLEFT+TOTRIMRIGHT, name, "\0" ,"\0", 1},

//@Man: FIELD_INT(field)
//@Memo: Use the {\bf FIELD_INT} to declare your integer field
//@See: BEGIN_CEDARECINFO
#define CCS_FIELD_INT(fld,name, desc, dispFlag) { CEDAINT, _CEDA_OFFSET(fld), _CEDA_LENGTH(fld), 0, name, desc,"\0" , dispFlag},
#define FIELD_INT(fld,name) { CEDAINT, _CEDA_OFFSET(fld), _CEDA_LENGTH(fld), 0, name, "\0" ,"\0", 1},

//@Man: FIELD_LONG(field)
//@Memo: Use the {\bf FIELD_LONG} to declare your long integer field
//@See: BEGIN_CEDARECINFO
#define CCS_FIELD_LONG(fld,name,desc, dispFlag) { CEDALONG, _CEDA_OFFSET(fld), _CEDA_LENGTH(fld), 0 , name, desc ,"\0", dispFlag},
#define FIELD_LONG(fld,name) { CEDALONG, _CEDA_OFFSET(fld), _CEDA_LENGTH(fld), 0 , name, "\0","\0", 1},

//@Man: FIELD_DATE(field)
//@Memo: Use the {\bf FIELD_DATE} to declare your CTime field
//@See: BEGIN_CEDARECINFO
#define CCS_FIELD_DATE(fld,name,desc, dispFlag) { CEDADATE, _CEDA_OFFSET(fld), _CEDA_LENGTH(fld), 0 , name , desc,"\0", dispFlag},
#define FIELD_DATE(fld,name) { CEDADATE, _CEDA_OFFSET(fld), _CEDA_LENGTH(fld), 0 , name , "\0","\0", 1},

//@Man: FIELD_DATE(field)
//@Memo: Use the {\bf FIELD_DATE} to declare your CTime field
//@See: BEGIN_CEDARECINFO
#define CCS_FIELD_OLEDATE(fld,name,desc, dispFlag) { CEDAOLEDATE, _CEDA_OFFSET(fld), _CEDA_LENGTH(fld), 0 , name , desc,"\0", dispFlag},
#define FIELD_OLEDATE(fld,name) { CEDAOLEDATE, _CEDA_OFFSET(fld), _CEDA_LENGTH(fld), 0 , name , "\0","\0", 1},

//@Man: FIELD_DOUBLE(field)
//@Memo: Use the {\bf FIELD_DOUBLE} to declare your double field
//@See: BEGIN_CEDARECINFO
#define CCS_FIELD_DOUBLE(fld,name,desc, dispFlag) { CEDADOUBLE, _CEDA_OFFSET(fld), _CEDA_LENGTH(fld), 0 ,name, desc, "\0", dispFlag},
#define FIELD_DOUBLE(fld,name) { CEDADOUBLE, _CEDA_OFFSET(fld), _CEDA_LENGTH(fld), 0 ,name, "\0", "\0", 1},


// These macros are used internally
#define _CEDA_OFFSET(fld)   ((char *)&_ceda_rec_pointer->fld - (char *)_ceda_rec_pointer)
#define _CEDA_LENGTH(fld)   sizeof(_ceda_rec_pointer->fld)

#endif
