// ModulData.h: interface for the ModulData class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MODULDATA_H__880DFE92_11BB_11D2_8572_0000C04D916B__INCLUDED_)
#define AFX_MODULDATA_H__880DFE92_11BB_11D2_8572_0000C04D916B__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <afxwin.h>

class ModulData  
{
public:
	ModulData();
	virtual ~ModulData();

	CString Fields;
	CString Selection;
	DWORD	Modul;
	bool	BC;

};

#endif // !defined(AFX_MODULDATA_H__880DFE92_11BB_11D2_8572_0000C04D916B__INCLUDED_)
