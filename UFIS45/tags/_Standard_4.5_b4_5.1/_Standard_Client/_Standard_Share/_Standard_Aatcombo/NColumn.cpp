/************************************
  REVISION LOG ENTRY
  Revision By: Mihai Filimon
  Revised on 12/14/98 11:37:52 AM
  Comments: NColumn.cpp: implementation of the CNColumn class.
 ************************************/

#include "stdafx.h"
#include "NCombo.h"
#include "NColumn.h"
#include "NWindow.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

// Function name	: CNColumn::CNColumn
// Description	    : default constructor
// Return type		: 
CNColumn::CNColumn(CNWindow* pParent, int nCol, LPCTSTR lpszColumnName):m_sName(lpszColumnName)
{
	SetParent(pParent);
	m_edit.Create(WS_CHILD | WS_VISIBLE | GetEditStyle(nCol), CRect(0,0,0,0), pParent->GetHeader(), nCol);
}

// Function name	: CNColumn::~CNColumn
// Description	    : virtual destructor
// Return type		: 
CNColumn::~CNColumn()
{
	m_edit.DestroyWindow();
}

// Function name	: CNColumn::SetParent
// Description	    : Set the parent for this
// Return type		: void 
// Argument         : CNWindow* pParent
void CNColumn::SetParent(CNWindow* pParent)
{
	m_pParent = pParent;
}

// Function name	: CNColumn::GetEdit
// Description	    : return the attacehd edit
// Return type		: CEdit* 
CEdit* CNColumn::GetEdit()
{
	return &m_edit;
}

// Function name	: CNColumn::GetEditStyle
// Description	    : 
// Return type		: UINT 
// Argument         : int nCol
UINT CNColumn::GetEditStyle(int nCol)
{
	ASSERT (m_pParent != NULL);
	static const UINT sEditStyle[] = {ES_LEFT, ES_RIGHT | ES_MULTILINE, ES_CENTER | ES_MULTILINE};
	LVCOLUMN lvColumn;
	 lvColumn.mask = LVCF_FMT;
	VERIFY(m_pParent->GetListCtrl()->GetColumn(nCol, &lvColumn));
	return sEditStyle[lvColumn.fmt & LVCFMT_JUSTIFYMASK];
}

// Function name	: CNColumn::GetName
// Description	    : 
// Return type		: CString 
CString CNColumn::GetName()
{
	return m_sName;
}

// Function name	: CNColumn::SetName
// Description	    : Set the new column name
// Return type		: void 
// Argument         : LPCTSTR lpszColumnName
void CNColumn::SetName(LPCTSTR lpszColumnName)
{
	m_sName = lpszColumnName;
}
