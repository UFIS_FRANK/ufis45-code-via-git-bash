//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by NCombo.rc
//
#define IDS_NCOMBO                      1
#define IDD_ABOUTBOX_NCOMBO             1
#define IDB_NCOMBO                      1
#define IDI_ABOUTDLL                    1
#define IDS_NCOMBO_PPG                  2
#define IDS_NCOMBO_PPG_CAPTION          200
#define IDD_PROPPAGE_NCOMBO             200
#define IDI_ICON1                       202
#define IDI_ICON_DOWN                   202
#define IDC_COMBO1                      203
#define IDC_STATIC_1                    204
#define IDI_ICON2                       204

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        205
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         205
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
