VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "UfisServer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private Type OSVERSIONINFO
    dwOSVersionInfoSize As Long
    dwMajorVersion As Long
    dwMinorVersion As Long
    dwBuildNumber As Long
    dwPlatformId As Long
    szCSDVersion As String * 128
End Type
  
Private Declare Function GetVersionEx Lib "kernel32" _
      Alias "GetVersionExA" (lpVersionInformation As OSVERSIONINFO) As Boolean

' dwPlatforID Constants
Const VER_PLATFORM_WIN32s = 0
Const VER_PLATFORM_WIN32_WINDOWS = 1
Const VER_PLATFORM_WIN32_NT = 2

Private Declare Function GetComputerName Lib "kernel32" _
    Alias "GetComputerNameA" (ByVal lpBuffer As String, _
                              nSize As Long) As Long

Private m_HostName As String
Private m_HOPO As String
Private m_ModName As String
Private m_UserName As String
Private m_TableExtension As String
Private m_AppVersion As String
Private m_TwsCode As String
Private m_StayConnected As Boolean

Private m_oUfisCom As Object
Private m_rsUrnos As ADODB.Recordset

Private WithEvents bcProx As BcProxyLib.BcPrxy
Attribute bcProx.VB_VarHelpID = -1

Private m_CedaIsConnected As Boolean
Private m_ServerConfiguration As String
Private m_NumberOfUrnos As Long

Public Property Let HostName(ByVal sHostName As String)
    m_HostName = sHostName
End Property

Public Property Get HostName() As String
    HostName = m_HostName
End Property

Public Property Let HOPO(ByVal sHopo As String)
    m_HOPO = sHopo
End Property

Public Property Get HOPO() As String
    HOPO = m_HOPO
End Property

Public Property Let ModName(ByVal sModName As String)
    m_ModName = sModName
End Property

Public Property Get ModName() As String
    ModName = m_ModName
End Property

Public Property Let UserName(ByVal sUserName As String)
    m_UserName = sUserName
End Property

Public Property Get UserName() As String
    UserName = m_UserName
End Property

Public Property Let TableExtension(ByVal sTableExtension As String)
    m_TableExtension = sTableExtension
End Property

Public Property Get TableExtension() As String
    TableExtension = m_TableExtension
End Property

Public Property Let AppVersion(ByVal sAppVersion As String)
    m_AppVersion = sAppVersion
End Property

Public Property Get AppVersion() As String
    AppVersion = m_AppVersion
End Property

Public Property Let TwsCode(ByVal sTwsCode As String)
    m_TwsCode = sTwsCode
End Property

Public Property Get TwsCode() As String
    TwsCode = m_TwsCode
End Property

Public Property Let StayConnected(ByVal bStayConnected As Boolean)
    m_StayConnected = bStayConnected
    
    m_oUfisCom.StayConnected = m_StayConnected
End Property

Public Property Get StayConnected() As Boolean
    StayConnected = m_StayConnected
End Property

Public Property Get RowCount() As Long
    RowCount = m_oUfisCom.GetBufferCount
End Property

Public Property Get Row(ByVal RowNumber As Long) As String
    If RowNumber >= 0 And RowNumber < RowCount Then
        Row = m_oUfisCom.GetBufferLine(RowNumber)
    End If
End Property

Public Function GetMyWorkStationName() As String
    Dim sBuffer As String
  
    Dim lAns As Long
 
    sBuffer = Space$(255)
    lAns = GetComputerName(sBuffer, 255)
    If lAns <> 0 Then
        'read from beginning of string to null-terminator
        GetMyWorkStationName = Left$(sBuffer, InStr(sBuffer, Chr(0)) - 1)
    Else
        Err.Raise Err.LastDllError, , _
          "A system call returned an error code of " _
           & Err.LastDllError
   End If
End Function

Public Function ConnectToCeda() As Boolean
    Dim sServer As String
    Dim sConnectType As String
    Dim ret As Integer
    
    m_oUfisCom.CleanupCom
    
    m_CedaIsConnected = False
    
    m_oUfisCom.Tws = ""
    m_oUfisCom.Twe = ""
    
    ret = m_oUfisCom.SetCedaPerameters(m_ModName, m_HOPO, m_TableExtension)
    ret = 0
    
    sConnectType = "CEDA"
    ret = m_oUfisCom.InitCom(m_HostName, sConnectType)
    
    If ret <> 0 Then
        m_CedaIsConnected = True
    End If
    
    ConnectToCeda = m_CedaIsConnected
End Function

Public Function DisconnectFromCeda() As Boolean
    m_oUfisCom.CleanupCom
    
    m_CedaIsConnected = False
    
    DisconnectFromCeda = True
End Function

Public Sub ConnectToBcProxy()
    On Error GoTo ErrHdl
    
    If m_CedaIsConnected Then
        Set bcProx = New BcPrxy
    Else
        MsgBox "Not connected to ceda!"
    End If
    
    Exit Sub
    
ErrHdl:
    MsgBox "BcProxy not registered!"
End Sub

Public Function CallCeda(CedaResult As String, _
        RouterCmd As String, TableName As String, FieldList As String, DataList As String, _
        WhereClause As String, OrderKey As String, UseBuffer As Integer, _
        FullAnswer As Boolean, AutoRequest As Boolean) As Integer
        
    Dim RetCode As Integer
    Dim Result As String
    Dim tmpCmd As String
    Dim tmpTable As String
    Dim tmpFields As String
    Dim tmpData As String
    Dim tmpSqlKey As String
    Dim CedaAnswer As String
    Dim LineCount As Long
    Dim CurRec As Long
    Dim CurLine As String
    
    RetCode = CC_RC_NOT_CONNECTED
    
    tmpCmd = RouterCmd
    tmpTable = TableName
    tmpFields = FieldList
    tmpData = CleanNullValues(DataList)
    tmpSqlKey = WhereClause
    
    If m_CedaIsConnected Then
        If OrderKey <> "" Then tmpSqlKey = tmpSqlKey & " ORDER BY " & OrderKey
        
        If m_ModName = "" Then
            m_ModName = App.EXEName
        End If
        If m_AppVersion = "" Then
            m_AppVersion = GetApplVersion(True)
        End If
        
        m_oUfisCom.ClearDataBuffer
        m_oUfisCom.HomeAirport = m_HOPO
        m_oUfisCom.TableExt = m_TableExtension
        m_oUfisCom.Module = m_ModName & "," & m_AppVersion
        m_oUfisCom.Twe = ""
        m_oUfisCom.Tws = m_TwsCode
        m_oUfisCom.UserName = m_UserName
        
        CedaAnswer = m_oUfisCom.CallServer(tmpCmd, tmpTable, tmpFields, tmpData, tmpSqlKey, "")
                
        LineCount = m_oUfisCom.GetBufferCount - 1
        If LineCount >= 0 Then
            Result = "BUFFER" & UseBuffer & ",OK"
            If FullAnswer Then
                Result = ""
                For CurRec = 0 To LineCount
                    CurLine = m_oUfisCom.GetBufferLine(CurRec)
                    Result = Result & CurLine & vbNewLine
                Next
                Result = Left(Result, Len(Result) - 2)  'Cut the last vbNewLine
            End If
            RetCode = 0
        Else
            RetCode = CC_RC_NOT_FOUND
        End If
    End If
        
    CedaResult = Result
    
    CallCeda = RetCode
End Function

Public Function CallCedaDirect( _
        RouterCmd As String, TableName As String, FieldList As String, DataList As String, _
        WhereClause As String, OrderKey As String) As Integer
        
    Dim RetCode As Integer
    Dim Result As String
    Dim tmpCmd As String
    Dim tmpTable As String
    Dim tmpFields As String
    Dim tmpData As String
    Dim tmpSqlKey As String
    Dim CedaAnswer As String
    Dim LineCount As Long
    Dim CurRec As Long
    Dim CurLine As String
    
    RetCode = CC_RC_NOT_CONNECTED
    
    tmpCmd = RouterCmd
    tmpTable = TableName
    tmpFields = FieldList
    tmpData = CleanNullValues(DataList)
    tmpSqlKey = WhereClause
    
    If m_CedaIsConnected Then
        If OrderKey <> "" Then tmpSqlKey = tmpSqlKey & " ORDER BY " & OrderKey
        
        If m_ModName = "" Then
            m_ModName = App.EXEName
        End If
        If m_AppVersion = "" Then
            m_AppVersion = GetApplVersion(True)
        End If
        
        m_oUfisCom.ClearDataBuffer
        m_oUfisCom.HomeAirport = m_HOPO
        m_oUfisCom.TableExt = m_TableExtension
        m_oUfisCom.Module = m_ModName & "," & m_AppVersion
        m_oUfisCom.Twe = ""
        m_oUfisCom.Tws = m_TwsCode
        m_oUfisCom.UserName = m_UserName
        
        CedaAnswer = m_oUfisCom.CallServer(tmpCmd, tmpTable, tmpFields, tmpData, tmpSqlKey, "")
                
        LineCount = m_oUfisCom.GetBufferCount - 1
        If LineCount >= 0 Then
            RetCode = 0
        Else
            RetCode = CC_RC_NOT_FOUND
        End If
    End If
    
    CallCedaDirect = RetCode
End Function

Private Function GetServerConfigItem(ByVal ServerConfiguration As String, ByVal KeyCode As String, _
ByVal ItemNbr As Integer, ByVal UseDefault As String) As String
    Dim Result As String
    Dim UseCfg As String
    Dim UseKey As String
    
    UseCfg = vbLf & ServerConfiguration
    UseKey = vbLf & KeyCode & ","
    Result = GetItem(UseCfg, 2, UseKey)
    Result = GetItem(Result, 1, vbLf)
    
    If ItemNbr > 0 Then Result = GetItem(Result, ItemNbr, ",")
    If Right(Result, 1) = vbLf Then Result = Left(Result, Len(Result) - 1)
    If Right(Result, 1) = vbCr Then Result = Left(Result, Len(Result) - 1)
    If Right(Result, 1) = vbLf Then Result = Left(Result, Len(Result) - 1)
    If Result = "" Then Result = UseDefault
    
    GetServerConfigItem = Result
End Function

Private Function GetServerConfig() As String
    Dim ServerConfig As String
    Dim CloseConnex As Boolean
    
    ServerConfig = ""
    CloseConnex = False
    If Not m_CedaIsConnected Then
        ConnectToCeda
        CloseConnex = True
    End If
    
    If m_CedaIsConnected Then CallCeda ServerConfig, "GFR", "", "", "", "[CONFIG]", "", 0, True, False
    If CloseConnex Then DisconnectFromCeda
    
    GetServerConfig = ServerConfig
End Function

Public Sub SetUtcTimeDiff()
    Dim UtcInfo As String
    
    m_ServerConfiguration = GetServerConfig()
    UtcInfo = GetServerConfigItem(m_ServerConfiguration, "UTCD", 1, "")
    If UtcInfo <> "" Then
        UtcTimeDiff = Val(UtcInfo)
    Else
        'MsgBox "Couldn't determine Utc Diff."
    End If
End Sub

Public Sub UrnoPoolInit(UrnoPackages As Long)
    Set m_rsUrnos = New ADODB.Recordset
    m_rsUrnos.Fields.Append "Urno", adVarChar, 50
    m_rsUrnos.Open
    
    m_NumberOfUrnos = UrnoPackages
    If m_NumberOfUrnos > 500 Then m_NumberOfUrnos = 500
End Sub

Public Function UrnoPoolPrepare(NeededUrnos As Long) As Long
    Dim UrnoList As String
    Dim UrnoCount As Long
    Dim UrnoArray As Variant
    Dim UrnoItem As Variant
    
    If CedaIsConnected Then
        If m_rsUrnos.RecordCount < NeededUrnos Then
            UrnoCount = NeededUrnos - m_rsUrnos.RecordCount
            If UrnoCount > 500 Then
                UrnoCount = 500
                m_NumberOfUrnos = 500
            End If
            If UrnoCount < 1 Then UrnoCount = 1
            CallCeda UrnoList, "GMU", "", "*", Trim(Str(UrnoCount)), "", "", 0, True, False
            UrnoArray = Split(UrnoList, ",")
            For Each UrnoItem In UrnoArray
                m_rsUrnos.AddNew
                m_rsUrnos.Fields("Urno").Value = UrnoItem
                m_rsUrnos.Update
            Next UrnoItem
            m_rsUrnos.MoveFirst
        End If
        UrnoPoolPrepare = m_rsUrnos.RecordCount
    Else
        m_NumberOfUrnos = NeededUrnos
        UrnoPoolPrepare = m_NumberOfUrnos
    End If
    'UfisServer.m_ournotab.AutoSizeColumns
End Function

Public Function UrnoPoolGetNext() As String
    Dim UrnoList As String
    Dim UrnoArray As Variant
    Dim UrnoItem As Variant
    
    If m_CedaIsConnected Then
        If m_rsUrnos.RecordCount < 1 Then
            If m_NumberOfUrnos < 1 Then m_NumberOfUrnos = 1
            CallCeda UrnoList, "GMU", "", "*", CStr(m_NumberOfUrnos), "", "", 0, True, False
            UrnoArray = Split(UrnoList, ",")
            For Each UrnoItem In UrnoArray
                m_rsUrnos.AddNew
                m_rsUrnos.Fields("Urno").Value = UrnoItem
                m_rsUrnos.Update
            Next UrnoItem
            m_rsUrnos.MoveFirst
        End If
        UrnoPoolGetNext = m_rsUrnos.Fields("Urno").Value
        m_rsUrnos.Delete
        m_rsUrnos.MoveNext
    Else
        UrnoPoolGetNext = CStr(m_NumberOfUrnos)
        m_NumberOfUrnos = m_NumberOfUrnos + 1
    End If
    'm_ournotab.AutoSizeColumns
End Function

Private Function GetApplVersion(ShowExtended As Boolean) As String
    Dim rOsVersionInfo As OSVERSIONINFO
    
    Dim tmpTxt As String
    Dim tmpMajor As String
    Dim tmpMinor As String
    Dim tmpRevis As String
    
    tmpMajor = App.Major
    tmpMinor = App.Minor
    tmpRevis = App.Revision
    tmpTxt = ""
    
    ' Pass the size of the structure into itself for the API call
    rOsVersionInfo.dwOSVersionInfoSize = Len(rOsVersionInfo)
    If GetVersionEx(rOsVersionInfo) Then
        Select Case rOsVersionInfo.dwPlatformId
            Case VER_PLATFORM_WIN32_NT
                If rOsVersionInfo.dwMajorVersion >= 5 Then
                    If rOsVersionInfo.dwMinorVersion = 0 Then
                        'Windows 2000
                        If ShowExtended Then tmpRevis = "0." & tmpRevis
                    Else
                        'Windows XP
                        If ShowExtended Then
                            tmpMinor = Right("0000" & tmpMinor, 2)
                            tmpRevis = Right("0000" & tmpRevis, 4)
                        End If
                    End If
                Else
                    'Windows NT"
                End If
            Case VER_PLATFORM_WIN32_WINDOWS
                If rOsVersionInfo.dwMajorVersion >= 5 Then
                    'Windows ME
                ElseIf rOsVersionInfo.dwMajorVersion = 4 And rOsVersionInfo.dwMinorVersion > 0 Then
                    'Windows 98
                Else
                    'Windows 95
                End If
            Case VER_PLATFORM_WIN32s
                'Win32s
        End Select
    End If
    
    tmpTxt = tmpTxt & tmpMajor & "." & tmpMinor & "." & tmpRevis
    GetApplVersion = tmpTxt
End Function

Private Sub bcProx_OnBcReceive(ByVal ReqId As String, ByVal DestName As String, ByVal RecvName As String, ByVal CedaCmd As String, ByVal ObjName As String, ByVal Seq As String, ByVal Tws As String, ByVal Twe As String, ByVal CedaSqlKey As String, ByVal Fields As String, ByVal Data As String, ByVal BcNum As String)
    HandleBroadCast ReqId, DestName, RecvName, CedaCmd, ObjName, Seq, Tws, Twe, CedaSqlKey, Fields, Data, BcNum
End Sub

Private Sub Class_Initialize()
    Set m_oUfisCom = CreateObject("UFISCOM.UfisComCtrl.1")
    
    m_HOPO = GetIniEntry("", "GLOBAL", "", "HOMEAIRPORT", "???")
    m_TableExtension = GetIniEntry("", "GLOBAL", "", "TABLEEXTENSION", "TAB")
    m_HostName = GetIniEntry("", "GLOBAL", "", "HOSTNAME", "")
    m_ModName = App.EXEName
    m_StayConnected = True
End Sub

Private Sub Class_Terminate()
    Set m_oUfisCom = Nothing
    Set m_rsUrnos = Nothing
End Sub
