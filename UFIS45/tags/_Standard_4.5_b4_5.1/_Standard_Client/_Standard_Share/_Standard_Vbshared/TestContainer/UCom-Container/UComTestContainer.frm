VERSION 5.00
Object = "{F9478333-76BA-11D6-8067-0001022205E4}#1.0#0"; "UCom.ocx"
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form Form1 
   Caption         =   "UComTestContainer (Send2cdrhdl)"
   ClientHeight    =   6045
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   10425
   LinkTopic       =   "Form1"
   ScaleHeight     =   6045
   ScaleWidth      =   10425
   StartUpPosition =   3  'Windows Default
   Begin TABLib.TAB TAB1 
      Height          =   2580
      Left            =   90
      TabIndex        =   19
      TabStop         =   0   'False
      Top             =   3240
      Width           =   10185
      _Version        =   65536
      _ExtentX        =   17965
      _ExtentY        =   4551
      _StockProps     =   0
   End
   Begin VB.Frame Frame2 
      Height          =   1500
      Left            =   90
      TabIndex        =   13
      Top             =   945
      Width           =   10230
      Begin VB.TextBox txtWhere 
         Height          =   330
         Left            =   720
         TabIndex        =   7
         Top             =   1035
         Width           =   9375
      End
      Begin VB.TextBox txtData 
         Height          =   330
         Left            =   720
         TabIndex        =   6
         Top             =   630
         Width           =   9375
      End
      Begin VB.TextBox txtFields 
         Height          =   330
         Left            =   720
         TabIndex        =   5
         Top             =   225
         Width           =   9375
      End
      Begin VB.Label Label6 
         Caption         =   "Where:"
         Height          =   285
         Left            =   90
         TabIndex        =   16
         Top             =   1125
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "Data:"
         Height          =   285
         Left            =   90
         TabIndex        =   15
         Top             =   720
         Width           =   735
      End
      Begin VB.Label Label1 
         Caption         =   "Fieldlist:"
         Height          =   285
         Left            =   90
         TabIndex        =   14
         Top             =   315
         Width           =   735
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "&Go!"
      Height          =   375
      Left            =   4485
      TabIndex        =   8
      Top             =   2655
      Width           =   1455
   End
   Begin VB.Frame Frame1 
      Height          =   735
      Left            =   90
      TabIndex        =   9
      Top             =   135
      Width           =   10230
      Begin VB.TextBox txtTable 
         Height          =   330
         Left            =   7335
         TabIndex        =   4
         Top             =   270
         Width           =   825
      End
      Begin VB.TextBox txtCommand 
         Height          =   330
         Left            =   5985
         TabIndex        =   3
         Text            =   "RT"
         Top             =   270
         Width           =   510
      End
      Begin VB.TextBox txtHopo 
         Height          =   330
         Left            =   4680
         TabIndex        =   2
         Top             =   270
         Width           =   510
      End
      Begin VB.TextBox txtTabExt 
         Height          =   330
         Left            =   3375
         TabIndex        =   1
         Text            =   "TAB"
         Top             =   270
         Width           =   510
      End
      Begin VB.TextBox txtServer 
         Height          =   330
         Left            =   765
         TabIndex        =   0
         Top             =   270
         Width           =   1815
      End
      Begin VB.Label Label8 
         Caption         =   "Table:"
         Height          =   285
         Left            =   6705
         TabIndex        =   18
         Top             =   315
         Width           =   555
      End
      Begin VB.Label Label7 
         Caption         =   "Cmd:"
         Height          =   285
         Left            =   5355
         TabIndex        =   17
         Top             =   315
         Width           =   555
      End
      Begin VB.Label Label5 
         Caption         =   "Hopo:"
         Height          =   285
         Left            =   4050
         TabIndex        =   12
         Top             =   315
         Width           =   555
      End
      Begin VB.Label Label4 
         Caption         =   "TabExt:"
         Height          =   285
         Left            =   2700
         TabIndex        =   11
         Top             =   315
         Width           =   600
      End
      Begin VB.Label Label3 
         Caption         =   "Server:"
         Height          =   285
         Left            =   90
         TabIndex        =   10
         Top             =   315
         Width           =   600
      End
   End
   Begin UCOMLib.UCom UCom1 
      Left            =   135
      Top             =   2655
      _Version        =   65536
      _ExtentX        =   1217
      _ExtentY        =   820
      _StockProps     =   0
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Command1_Click()

    UCom1.ApplicationName = "UCom"
    UCom1.Hopo = txtHopo.Text
    UCom1.Port = "3357"
    UCom1.ServerName = txtServer.Text
    UCom1.TableExtension = txtTabExt.Text
    UCom1.UserName = "test"
    UCom1.WorkstationName = "wksXXX"
    UCom1.SendTimeoutSeconds = "120"
    UCom1.ReceiveTimeoutSeconds = "240"
    UCom1.RecordSeparator = Chr(10)
    UCom1.CedaIdentifier = "UCom"

    If UCom1.CedaAction(txtCommand.Text, txtTable.Text, txtFields.Text, txtData.Text, txtWhere.Text) = True Then
        Dim strArr() As String
        Dim headerLenStr As String
        Dim strFieldList As String
        Dim llItemCount As Long
        Dim l As Long
        TAB1.ResetContent
        TAB1.ShowHorzScroller True
        TAB1.ShowVertScroller True
        If Len(txtFields.Text) > 0 Then
            strArr = Split(txtFields.Text, ",")
            llItemCount = UBound(strArr)
            For l = 0 To llItemCount
                If l + 1 > llItemCount Then
                    headerLenStr = headerLenStr & "120"
                    strFieldList = strFieldList & UCase(strArr(l))
                Else
                    headerLenStr = headerLenStr & "120,"
                    strFieldList = strFieldList & UCase(strArr(l)) & ","
                End If
            Next l
            TAB1.HeaderLengthString = headerLenStr
            TAB1.HeaderString = strFieldList
        End If

        Dim strBuffer As String
        llItemCount = UCom1.GetBufferCount
        For l = 0 To llItemCount
            strBuffer = UCom1.GetRecord(l) & Chr(10)
            If l > 0 And l Mod 100 Then
                TAB1.InsertBuffer strBuffer, Chr(10)
                strBuffer = ""
            End If
        Next l
        If Len(strBuffer) > 0 Then
            TAB1.InsertBuffer strBuffer, Chr(10)
        End If
        TAB1.RedrawTab
    Else
        Dim strMsg As String
        strMsg = UCom1.GetLastError
        MsgBox strMsg
    End If
End Sub
