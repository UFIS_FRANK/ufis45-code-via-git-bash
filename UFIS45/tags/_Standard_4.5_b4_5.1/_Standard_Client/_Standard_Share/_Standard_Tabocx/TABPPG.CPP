// TABPpg.cpp : Implementation of the CTABPropPage property page class.

#include "stdafx.h"
#include "TAB.h"
#include "TABPpg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CTABPropPage, COlePropertyPage)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CTABPropPage, COlePropertyPage)
	//{{AFX_MSG_MAP(CTABPropPage)
	// NOTE - ClassWizard will add and remove message map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CTABPropPage, "TAB.TABPropPage.1",
	0x64e8e386, 0x5e2, 0x11d2, 0x9b, 0x1d, 0x9b, 0x6, 0x30, 0xbc, 0x8f, 0x12)


/////////////////////////////////////////////////////////////////////////////
// CTABPropPage::CTABPropPageFactory::UpdateRegistry -
// Adds or removes system registry entries for CTABPropPage

BOOL CTABPropPage::CTABPropPageFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_TAB_PPG);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}


/////////////////////////////////////////////////////////////////////////////
// CTABPropPage::CTABPropPage - Constructor

CTABPropPage::CTABPropPage() :
	COlePropertyPage(IDD, IDS_TAB_PPG_CAPTION)
{
	//{{AFX_DATA_INIT(CTABPropPage)
	// NOTE: ClassWizard will add member initialization here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_INIT
}


/////////////////////////////////////////////////////////////////////////////
// CTABPropPage::DoDataExchange - Moves data between page and properties

void CTABPropPage::DoDataExchange(CDataExchange* pDX)
{
	//{{AFX_DATA_MAP(CTABPropPage)
	// NOTE: ClassWizard will add DDP, DDX, and DDV calls here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_MAP
	DDP_PostProcessing(pDX);
}


/////////////////////////////////////////////////////////////////////////////
// CTABPropPage message handlers
