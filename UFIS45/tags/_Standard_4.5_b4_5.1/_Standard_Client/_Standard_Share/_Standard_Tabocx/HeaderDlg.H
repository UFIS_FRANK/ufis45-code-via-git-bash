#if !defined(AFX_HEADERDLG_H__64E8E398_05E2_11D2_9B1D_9B0630BC8F12__INCLUDED_)
#define AFX_HEADERDLG_H__64E8E398_05E2_11D2_9B1D_9B0630BC8F12__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// HeaderDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// HeaderDlg dialog

class HeaderDlg : public CDialog
{
// Construction
public:
	HeaderDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(HeaderDlg)
	enum { IDD = IDD_HEADER };
	CString	m_T4;
	CString	m_T5;
	CString	m_T6;
	CString	m_T1;
	CString	m_T2;
	CString	m_T3;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(HeaderDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(HeaderDlg)
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HEADERDLG_H__64E8E398_05E2_11D2_9B1D_9B0630BC8F12__INCLUDED_)
