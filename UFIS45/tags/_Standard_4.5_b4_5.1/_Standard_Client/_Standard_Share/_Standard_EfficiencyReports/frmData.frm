VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Begin VB.Form frmData 
   Caption         =   "frmData"
   ClientHeight    =   7005
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8580
   LinkTopic       =   "Form1"
   ScaleHeight     =   7005
   ScaleWidth      =   8580
   StartUpPosition =   3  'Windows Default
   Tag             =   "{=TABLE=}AFTTAB{=FIELDS=}URNO,FLNO,ADID,STOA,STOD,TIFD,TIFA,ORG3,DES3,ALC2,ACT3,ETAI,ETDI"
   Visible         =   0   'False
   Begin TABLib.TAB tab_POATAB 
      Height          =   855
      Left            =   960
      TabIndex        =   15
      Tag             =   "{=TABLE=}POATAB{=FIELDS=}URNO,UVAL,UPOL"
      Top             =   6000
      Width           =   375
      _Version        =   65536
      _ExtentX        =   661
      _ExtentY        =   1508
      _StockProps     =   64
   End
   Begin TABLib.TAB tab_ORGTAB 
      Height          =   735
      Left            =   240
      TabIndex        =   14
      Tag             =   "{=TABLE=}ORGTAB{=FIELDS=}URNO,DPT1,DPTN"
      Top             =   5160
      Width           =   375
      _Version        =   65536
      _ExtentX        =   661
      _ExtentY        =   1296
      _StockProps     =   64
   End
   Begin TABLib.TAB tab_SDITABPLAN 
      Height          =   495
      Left            =   6720
      TabIndex        =   13
      Tag             =   "{=TABLE=}SHSDITAB{=FIELDS=}URNO,UAFT,PBEA,PENA,UCCC,DATA"
      Top             =   3240
      Width           =   255
      _Version        =   65536
      _ExtentX        =   450
      _ExtentY        =   873
      _StockProps     =   64
   End
   Begin TABLib.TAB tab_SDITAB 
      Height          =   495
      Left            =   6240
      TabIndex        =   12
      Tag             =   "{=TABLE=}SDITAB{=FIELDS=}URNO,UAFT,PBEA,PENA,UCCC,DATA"
      Top             =   3240
      Width           =   255
      _Version        =   65536
      _ExtentX        =   450
      _ExtentY        =   873
      _StockProps     =   64
   End
   Begin TABLib.TAB tab_UCDTABPLAN 
      Height          =   495
      Left            =   7560
      TabIndex        =   11
      Tag             =   "{=TABLE=}SHUCDTAB{=FIELDS=}URNO,URUD,UAFT"
      Top             =   2400
      Width           =   375
      _Version        =   65536
      _ExtentX        =   661
      _ExtentY        =   873
      _StockProps     =   64
   End
   Begin TABLib.TAB tab_UCDTAB 
      Height          =   495
      Left            =   6240
      TabIndex        =   10
      Tag             =   "{=TABLE=}UCDTAB{=FIELDS=}URNO,URUD,UAFT"
      Top             =   2400
      Width           =   495
      _Version        =   65536
      _ExtentX        =   873
      _ExtentY        =   873
      _StockProps     =   64
   End
   Begin TABLib.TAB tab_CCCTAB 
      Height          =   735
      Left            =   7080
      TabIndex        =   9
      Tag             =   "{=TABLE=}CCCTAB{=FIELDS=}URNO,CICC,CICN"
      Top             =   2280
      Width           =   255
      _Version        =   65536
      _ExtentX        =   450
      _ExtentY        =   1296
      _StockProps     =   64
   End
   Begin TABLib.TAB tab_AFTTABPLAN 
      Height          =   735
      Left            =   7080
      TabIndex        =   8
      Tag             =   "{=TABLE=}SHAFTTAB{=FIELDS=}URNO,FLNO,ADID,STOA,STOD,TIFD,TIFA,ORG3,DES3,ALC2,ACT3,ETAI,ETDI"
      Top             =   3840
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1296
      _StockProps     =   64
   End
   Begin TABLib.TAB tab_AFTTAB 
      Height          =   975
      Left            =   6720
      TabIndex        =   7
      Tag             =   "{=TABLE=}AFTTAB{=FIELDS=}URNO,FLNO,ADID,STOA,STOD,TIFD,TIFA,ORG3,DES3,ALC2,ACT3,ETAI,ETDI,RKEY"
      Top             =   4800
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1720
      _StockProps     =   64
   End
   Begin TABLib.TAB tab_PFCTAB 
      Height          =   1095
      Left            =   5640
      TabIndex        =   6
      Tag             =   "{=TABLE=}PFCTAB{=FIELDS=}FCTC,FCTN"
      Top             =   4680
      Width           =   495
      _Version        =   65536
      _ExtentX        =   873
      _ExtentY        =   1931
      _StockProps     =   64
   End
   Begin TABLib.TAB tab_RPFTAB 
      Height          =   855
      Left            =   4800
      TabIndex        =   5
      Tag             =   "{=TABLE=}RPFTAB{=FIELDS=}URUD,FCCO"
      Top             =   4680
      Width           =   495
      _Version        =   65536
      _ExtentX        =   873
      _ExtentY        =   1508
      _StockProps     =   64
   End
   Begin TABLib.TAB tab_RUETAB 
      Height          =   1095
      Left            =   3960
      TabIndex        =   4
      Tag             =   "{=TABLE=}RUETAB{=FIELDS=}URNO,RUSN,RUNA"
      Top             =   4560
      Width           =   495
      _Version        =   65536
      _ExtentX        =   873
      _ExtentY        =   1931
      _StockProps     =   64
   End
   Begin TABLib.TAB tab_RUDTAB 
      Height          =   1095
      Left            =   3240
      TabIndex        =   3
      Tag             =   "{=TABLE=}RUDTAB{=FIELDS=}URNO,URUE"
      Top             =   4560
      Width           =   495
      _Version        =   65536
      _ExtentX        =   873
      _ExtentY        =   1931
      _StockProps     =   64
   End
   Begin VB.Timer TimerReloadAft 
      Interval        =   60000
      Left            =   435
      Top             =   210
   End
   Begin TABLib.TAB tab_CONFIG 
      Height          =   3030
      Left            =   405
      TabIndex        =   0
      Top             =   720
      Width           =   5445
      _Version        =   65536
      _ExtentX        =   9604
      _ExtentY        =   5345
      _StockProps     =   64
   End
   Begin TABLib.TAB tab_POLTAB 
      Height          =   1095
      Left            =   960
      TabIndex        =   1
      Tag             =   "{=TABLE=}POLTAB{=FIELDS=}URNO,POOL,NAME"
      Top             =   4560
      Width           =   735
      _Version        =   65536
      _ExtentX        =   1296
      _ExtentY        =   1931
      _StockProps     =   64
   End
   Begin TABLib.TAB tab_TPLTAB 
      Height          =   1095
      Left            =   2400
      TabIndex        =   2
      Tag             =   "{=TABLE=}TPLTAB{=FIELDS=}URNO,TNAM,APPL"
      Top             =   4560
      Width           =   375
      _Version        =   65536
      _ExtentX        =   661
      _ExtentY        =   1931
      _StockProps     =   64
   End
   Begin TABLib.TAB tab_JOBTAB 
      Height          =   975
      Left            =   5400
      TabIndex        =   16
      Tag             =   "{=TABLE=}JOBTAB{=FIELDS=}URNO,UAFT,ACFR,ACTO,PLFR,PLTO,STAT,UDEM"
      Top             =   5880
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1720
      _StockProps     =   64
   End
End
Attribute VB_Name = "frmData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'#######################################################################
'# Main form
'# WKSADMIN TOOL
'#  based on HASConfigTool Sources
'#
'#  adapted by SHA 20060324
'#
'#######################################################################

Option Explicit

Private strFieldSeparator As String
Private strConfigSepa As String
Private colURNOs As New Collection
Private strHOPO As String
Private strTableExt As String
Private strServer As String
Private strAccessMethod As String
Private sglUTCOffsetHours As Single
Private strConfigFile As String
Private ilCntMinutes As Integer
Private ilReloadMinutes As Integer
Private strTemplatePath As String
Private ilOffset As Integer
Public Sub InitialLoadData()
    
End Sub

Public Sub InitTabGeneral(ByRef rTab As TABLib.Tab)
    rTab.ResetContent
    rTab.FontName = "Courier New"
    rTab.HeaderFontSize = "18"
    rTab.FontSize = "16"
    rTab.EnableHeaderSizing True
    rTab.ShowHorzScroller True
    rTab.AutoSizeByHeader = True
    rTab.SetFieldSeparator strFieldSeparator

    Dim ilCnt As Integer
    Dim i As Integer
    Dim strFields As String
    Dim strHeaderLengthString As String
    GetKeyItem strFields, rTab.Tag, "{=FIELDS=}", "{="
    ilCnt = ItemCount(strFields, ",")
    For i = 1 To ilCnt Step 1
        strHeaderLengthString = strHeaderLengthString & "10,"
    Next i
    strHeaderLengthString = Left(strHeaderLengthString, Len(strHeaderLengthString) - 1)
    rTab.HeaderLengthString = strHeaderLengthString

    strFields = Replace(strFields, ",", strFieldSeparator)
    rTab.HeaderString = strFields
End Sub


Public Sub InitTabForCedaConnection(ByRef rTab As TABLib.Tab)
    'do the initializing for the CEDA-connection
    rTab.CedaServerName = strServer
    rTab.CedaPort = "3357"
    rTab.CedaHopo = strHOPO
    rTab.CedaCurrentApplication = gsAppName
    rTab.CedaTabext = strTableExt
    rTab.CedaUser = gsUserName
    rTab.CedaWorkstation = frmMain.AATLoginControl1.GetWorkStationName
    rTab.CedaSendTimeout = "120"
    rTab.CedaReceiveTimeout = "240"
    rTab.CedaRecordSeparator = Chr(10)
    rTab.CedaIdentifier = gsAppName
End Sub

Public Sub LoadData(ByRef rTab As TABLib.Tab, ByRef pWhere As String)
    Dim strCommand As String
    Dim strTable As String
    Dim strFieldList As String
    Dim strData As String
    Dim strWhere As String

    strCommand = "RT"
    
    GetKeyItem strTable, rTab.Tag, "{=TABLE=}", "{="
    GetKeyItem strFieldList, rTab.Tag, "{=FIELDS=}", "{="
    strData = ""
    strWhere = pWhere
    If Len(strWhere) = 0 Then
        GetKeyItem strWhere, rTab.Tag, "{=WHERE=}", "{="
    End If
    rTab.CedaAction strCommand, strTable, strFieldList, strData, strWhere
End Sub

Public Function GetUrno() As String
    Dim strRet As String
    ' do we have any reserved URNOs in "colURNOs"?
    If colURNOs.count < 1 Then
        ' no - we have to load some from the server
        Dim strURNOs As String
        Dim llRecords As Long
        Dim i As Integer

        ConnectUfisCom
        If frmMain.UfisCom1.CallServer("GMU", "", "*", "20", "", "360") = 0 Then
            llRecords = frmMain.UfisCom1.GetBufferCount
            If llRecords > 1 Then
                For i = 0 To llRecords - 1
                    colURNOs.Add GetItem(frmMain.UfisCom1.GetBufferLine(0), 1, ",")
                Next i
            Else
                strURNOs = frmMain.UfisCom1.GetBufferLine(0)
                llRecords = ItemCount(strURNOs, ",")
                For i = 1 To llRecords
                    colURNOs.Add GetItem(strURNOs, i, ",")
                Next i
            End If
        End If
        DisconnetUfisCom
    End If
    strRet = colURNOs.Item(1)
    colURNOs.Remove 1

    GetUrno = strRet
End Function

Private Sub ConnectUfisCom()
    frmMain.UfisCom1.SetCedaPerameters frmMain.AATLoginControl1.GetUserName, strHOPO, strTableExt
    frmMain.UfisCom1.InitCom strServer, strAccessMethod
End Sub

Private Sub DisconnetUfisCom()
    frmMain.UfisCom1.CleanupCom
End Sub



Private Sub Form_Load()
    ilOffset = 42
    strFieldSeparator = ","
    strConfigSepa = Chr(15)
    strServer = GetIniEntry("", gsAppName, "GLOBAL", "HOSTNAME", "XXX")
    strHOPO = GetIniEntry("", gsAppName, "GLOBAL", "HOMEAIRPORT", "XXX")
    strTableExt = GetIniEntry("", gsAppName, "GLOBAL", "TABLEEXTENSION", "TAB")
    strAccessMethod = "CEDA"
    'strConfigFile = GetIniEntry("", gsAppName, "GLOBAL", "CONFIG_FILE", "XXX")
    'If ExistFile(strConfigFile) = False Then
    '    MsgBox "The config-file for the application " & gsAppName & " does not exist!" & vbCrLf & _
    '            "Please check the setting in ceda.ini:" & vbCrLf & _
    '            "[" & gsAppName & "]" & vbCrLf & _
    '            "..." & vbCrLf & _
    '            "CONFIG_FILE=..." & vbCrLf & vbCrLf & _
    '            "The application terminates!", vbCritical, "No configuration file"
    'End If

    'ReadConfigFile
    sglUTCOffsetHours = GetUTCOffset
End Sub


Private Sub WriteToDB(ByRef rTab As TABLib.Tab, ByRef strSearchLineTag As String, ByRef strCMD As String)
    Dim strLineNo As String
    Dim strLine As String
    Dim strFields As String
    Dim strTable As String
    Dim strWhere As String
    Dim strData As String
    Dim i As Integer

    GetKeyItem strFields, rTab.Tag, "{=FIELDS=}", "{="
    GetKeyItem strTable, rTab.Tag, "{=TABLE=}", "{="

    For i = rTab.GetLineCount - 1 To 0 Step -1
        If rTab.GetLineTag(CLng(i)) = strSearchLineTag Then
            strData = rTab.GetLineValues(CLng(i))
            strWhere = "WHERE URNO = " & rTab.GetColumnValue(CLng(i), 0)
            While InStr(1, strData, ",,") > 0
                strData = Replace(strData, ",,", ", ,")
            Wend
            If Right(strData, 1) = "," Then
                strData = strData & " "
            End If
            'If frmMain.UfisCom1.CallServer(Cmd, strTable, strFields, strData, strWhere, "360") <> 0 Then
            '    MsgBox frmMain.UfisCom1.LastErrorMessage, vbCritical
            '    Exit Sub
            'End If

            If strSearchLineTag = "DELETE" Then
                rTab.DeleteLine CLng(i)
            Else
                rTab.SetLineTag CLng(i), ""
            End If
        End If
    Next i
End Sub

Public Function GetUTCOffset() As Single
    Dim strUtcArr() As String
    Dim strArr() As String
    Dim CurrStr As String
    Dim tmpStr As String

    Dim istrRet As Integer
    Dim count As Integer
    Dim i As Integer

    GetUTCOffset = 0

    ConnectUfisCom

    If frmMain.UfisCom1.CallServer("GFR", "", "", "", "[CONFIG]", "360") = 0 Then
        tmpStr = frmMain.UfisCom1.GetDataBuffer(True)
    End If
    strArr = Split(tmpStr, Chr(10))
    count = UBound(strArr)
    For i = 0 To count
        CurrStr = strArr(i)
        istrRet = InStr(1, CurrStr, "UTCD", 0)
        If istrRet <> 0 Then
            strUtcArr = Split(strArr(i), ",")
            GetUTCOffset = CSng(strUtcArr(1) / 60)
            Exit For
        End If
    Next i
    DisconnetUfisCom
End Function

Private Function DecryptString(ByRef rString As String)
    Dim i As Integer
    Dim ilLen As Integer
    Dim strRet As String
    Dim ilNewCharCode As Integer

    ilLen = Len(rString)

    For i = 1 To ilLen Step 1
        ilNewCharCode = Asc(Mid(rString, i, 1)) - ilOffset
        If ilNewCharCode < 0 Then
            ilNewCharCode = ilNewCharCode + 255
        End If
        strRet = strRet & Chr(ilNewCharCode)
    Next i
    DecryptString = strRet
End Function

Private Function EncryptString(ByRef rString As String)
    Dim i As Integer
    Dim ilLen As Integer
    Dim strRet As String
    Dim ilNewCharCode As Integer

    ilLen = Len(rString)

    For i = 1 To ilLen Step 1
        ilNewCharCode = Asc(Mid(rString, i, 1)) + ilOffset
        If ilNewCharCode > 255 Then
            ilNewCharCode = ilNewCharCode - 255
        End If
        strRet = strRet & Chr(ilNewCharCode)
    Next i
    EncryptString = strRet
End Function

Private Sub Label3_Click()

End Sub
