﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace Ufis.Utilities
{
    /// <summary>
    /// Provides the functionality to get and set property value of an object.
    /// </summary>
    public class PropertySetterGetter
    {
        /// <summary>
        /// Returns the value of the property.
        /// </summary>
        /// <param name="obj">The object whose property value will be returned.</param>
        /// <param name="propName">The property name.</param>
        /// <returns></returns>
        public static object GetValue(object obj, string propName)
        {
            object objPropValue = null;

            if (obj != null)
            {
                Type type = obj.GetType();
                PropertyInfo propertyInfo = type.GetProperty(propName);
                if (propertyInfo != null)
                    objPropValue = propertyInfo.GetValue(obj, null);
            }

            return objPropValue;
        }

        /// <summary>
        /// Sets the value of the property.
        /// </summary>
        /// <param name="obj">The object whose property value will be set.</param>
        /// <param name="propName">The property name.</param>
        /// <param name="value">The new value for this property.</param>
        public static void SetValue(object obj, string propName, object value)
        {
            if (obj != null)
            {
                Type type = obj.GetType();
                PropertyInfo propertyInfo = type.GetProperty(propName);
                if (propertyInfo != null)
                    propertyInfo.SetValue(obj, value, null);
            }
        }
    }
}
