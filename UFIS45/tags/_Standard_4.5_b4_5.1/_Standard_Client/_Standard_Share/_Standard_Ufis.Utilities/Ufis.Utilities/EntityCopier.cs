﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Entities;
using System.Reflection;

namespace Ufis.Utilities
{
    /// <summary>
    // Provides a method for performing a deep copy of an entity.
    /// </summary>
    public static class EntityCopier
    {
        /// <summary>
        /// Perform a deep copy of the entity.
        /// </summary>
        /// <typeparam name="T">The type of entity being copied.</typeparam>
        /// <param name="source">The entity instance to copy.</param>
        /// <returns>The copied entity.</returns>
        public static T CloneEntity<T>(this T source)
        {
            return CloneEntity<T>(source, false);
        }

        /// <summary>
        /// Perform a deep copy of the entity.
        /// </summary>
        /// <typeparam name="T">The type of entity being copied.</typeparam>
        /// <param name="source">The entity instance to copy.</param>
        /// <param name="excludeReadOnlyAttributes">Set whether to exclude read only attributes when copying.</param>
        /// <returns>The copied entity.</returns>
        public static T CloneEntity<T>(this T source, bool excludeReadOnlyAttributes)
        {
            Type type = source.GetType();
            if (!typeof(Entity).IsAssignableFrom(type))
                throw new System.ArgumentException("Type provided is not Entity type.");

            // Don't serialize a null object, simply return the default for that object
            if (Object.ReferenceEquals(source, null))
            {
                return default(T);
            }

            object obj = Activator.CreateInstance(type);
            T item = (T)obj;
            CopyEntityAttributes<T>(source, item, excludeReadOnlyAttributes);

            return item;
        }

        /// <summary>
        /// Perform a deep copy of the entity's attributes.
        /// </summary>
        /// <typeparam name="T">The type of entity being copied.</typeparam>
        /// <param name="source">The entity instance to copy.</param>
        /// <param name="destination">The entity instance to copy to.</param>
        /// <returns>The copied entity.</returns>
        public static void CloneEntityAttributes<T>(this T source, T destination)
        {
            CopyEntityAttributes<T>(source, destination, false);
        }

        /// <summary>
        /// Perform a deep copy of the entity's attributes.
        /// </summary>
        /// <typeparam name="T">The type of entity being copied.</typeparam>
        /// <param name="source">The entity instance to copy.</param>
        /// <param name="destination">The entity instance to copy to.</param>
        /// <param name="excludeReadOnlyAttributes">Set whether to exclude read only attributes when copying.</param>
        /// <returns>The copied entity.</returns>
        public static void CopyEntityAttributes<T>(this T source, T destination, bool excludeReadOnlyAttributes)
        {
            Type type = source.GetType();
            if (!typeof(Entity).IsAssignableFrom(type))
                throw new System.ArgumentException("Type provided is not Entity type.");

            PropertyInfo[] infos = type.GetProperties();
            foreach (PropertyInfo info in infos)
            {
                bool bCopy = !excludeReadOnlyAttributes;

                if (!bCopy)
                {
                    object[] arrAttributes = info.GetCustomAttributes(typeof(EntityAttribute), false);
                    if (arrAttributes.Length > 0)
                    {
                        EntityAttribute entityAttribute = (EntityAttribute)arrAttributes[0];
                        bCopy = !entityAttribute.IsReadOnly;
                    }
                }

                if (bCopy)
                {
                    object objSrc = info.GetValue(source, null);
                    info.SetValue(destination, objSrc, null);
                }
            }
        }
    }
}
