﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ufis.Entities;
using Ufis.Data;

namespace Ufis.Utilities
{
    /// <summary>
    /// Provides functionality to create the flight operational type list.
    /// </summary>
    public class FlightTypesBuilder
    {
        private static IList<EntCodeName> _defaultFlightTypeList;

        /// <summary>
        /// Gets the default flight operational type list.
        /// </summary>
        /// <returns>List of flight operational type.</returns>
        public static IList<EntCodeName> GetDefaultFlightTypeList()
        {
            if (_defaultFlightTypeList == null)
                _defaultFlightTypeList = GetFlightTypeList(FlightType.Unspecified);

            return _defaultFlightTypeList;
        }

        /// <summary>
        /// Gets the flight operational type list from flight operational type enumeration values.
        /// </summary>
        /// <param name="flightAdids">Flight operational type enumeration values.</param>
        /// <returns>List of flight operational type.</returns>
        public static IList<EntCodeName> GetFlightTypeList(FlightType flightTypes)
        {
            List<EntCodeName> _flightTypeList = new List<EntCodeName>();

            foreach (FlightType type in Enum.GetValues(typeof(FlightType)))
            {
                if (type != FlightType.Unspecified) 
                {
                    if (flightTypes == FlightType.Unspecified || ((flightTypes & type) == type))
                    {
                        _flightTypeList.Add(CreateFlightTypeEntity(type));
                    }
                }
            }
           
            return _flightTypeList;
        }

        /// <summary>
        /// Create flight operational type entity based on flight operational type enumeration value.
        /// </summary>
        /// <param name="flightAdid">Flight operational type enumeration value.</param>
        /// <returns>Flight operational type entity.</returns>
        public static EntCodeName CreateFlightTypeEntity(FlightType flightType)
        {
            EntCodeName flightTypeEntity = new EntCodeName();

            switch (flightType)
            {
                case FlightType.Unspecified:
                    break;
                case FlightType.Scheduled:
                    flightTypeEntity.Code = "S";
                    flightTypeEntity.Name = "Scheduled";
                    break;
                case FlightType.Operation:
                    flightTypeEntity.Code = "O";
                    flightTypeEntity.Name = "Operation";
                    break;
                case FlightType.Cancelled:
                    flightTypeEntity.Code = "X";
                    flightTypeEntity.Name = "Cancelled";
                    break;
                case FlightType.NoOperation:
                    flightTypeEntity.Code = "N";
                    flightTypeEntity.Name = "No Operation";
                    break;
                case FlightType.Diverted:
                    flightTypeEntity.Code = "D";
                    flightTypeEntity.Name = "Diverted";
                    break;
                case FlightType.ReturnTaxi:
                    flightTypeEntity.Code = "B";
                    flightTypeEntity.Name = "Return Taxi";
                    break;
                case FlightType.ReturnFlight:
                    flightTypeEntity.Code = "Z";
                    flightTypeEntity.Name = "Return Flight";
                    break;
                case FlightType.Towing:
                    flightTypeEntity.Code = "T";
                    flightTypeEntity.Name = "Towing";
                    break;
                case FlightType.GroundMovement:
                    flightTypeEntity.Code = "G";
                    flightTypeEntity.Name = "Ground Movement";
                    break;
            }

            return flightTypeEntity;
        }
    }
}
