﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media.Imaging;

namespace Ufis.Utilities
{
    /// <summary>
    /// Provides functionality to get image from Ufis image library.
    /// </summary>
    public class ImageGetter
    {
        /// <summary>
        /// Gets the image from Ufis image library.
        /// </summary>
        /// <param name="imageFileName">The image file name.</param>
        /// <returns></returns>
        public static BitmapImage GetImage(string imageFileName)
        {
            const string IMAGE_URI = "pack://application:,,,/Ufis.Resources.ImageLibrary;v4.5.0.0;8f51493079604dd3;component/{0}";

            BitmapImage image = null;
            try
            {
                image = new BitmapImage(new Uri(string.Format(IMAGE_URI, imageFileName)));                
                image.Freeze();
            }
            catch
            {
                image = null;
            }
             
            return image;
        }
    }
}
