#if !defined(AFX_MAPPBCPROXYEVENTS_H__AB71E0B0_2D31_4BC4_A3EA_884A66209414__INCLUDED_)
#define AFX_MAPPBCPROXYEVENTS_H__AB71E0B0_2D31_4BC4_A3EA_884A66209414__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MappBCProxyEvents.h : header file
//


#include "BCComClientCtl.h"
#include "bcproxy.h"
/////////////////////////////////////////////////////////////////////////////
// MappBCProxyEvents command target
class _IBcPrxyEvents;
class MappBCProxyEvents : public CCmdTarget
{
	DECLARE_DYNCREATE(MappBCProxyEvents)

	MappBCProxyEvents(CBCComClientCtrl *popCtrl);           // protected constructor used by dynamic creation
	MappBCProxyEvents();           // protected constructor used by dynamic creation
// Attributes
public:
	CBCComClientCtrl *pomCtrl;
	virtual ~MappBCProxyEvents();
// Operations
public:


	MappBCProxyEvents *pomMapBCProxy;
	_IBcPrxyEvents omBcPrxyEvents;
	IBcPrxy *pomBcPrxyInstance;
	IDispatch* pDispBcPrxyEvents;
	DWORD m_dwCookie;      // used for AfxConnectionAdvise(..) BCProxy
	IUnknown* pIUnknown_Auto;

	void Disconnect();
	void RegisterObject(LPCTSTR strTable, LPCTSTR strCommand, BOOL bOwnBC, LPCTSTR application);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(MappBCProxyEvents)
	public:
	virtual void OnFinalRelease();
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(MappBCProxyEvents)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
	// Generated OLE dispatch map functions
	//{{AFX_DISPATCH(Auto_test)
		afx_msg BOOL OnBroadcast(LPCTSTR ReqId, LPCTSTR Dest1, LPCTSTR Dest2, LPCTSTR Cmd, LPCTSTR Object, LPCTSTR Seq, LPCTSTR Tws, LPCTSTR Twe, LPCTSTR Selection, LPCTSTR Fields, LPCTSTR Data, LPCTSTR BcNum);
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAPPBCPROXYEVENTS_H__AB71E0B0_2D31_4BC4_A3EA_884A66209414__INCLUDED_)
