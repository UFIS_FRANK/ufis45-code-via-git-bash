#if !defined(AFX_BCCOMSERVERTARGET_H__F0157D91_42D7_4D44_9403_6335797BD413__INCLUDED_)
#define AFX_BCCOMSERVERTARGET_H__F0157D91_42D7_4D44_9403_6335797BD413__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BcComServerTarget.h : header file
//


#include "bccomserver.h"
#include "bccomclientctl.h"

/////////////////////////////////////////////////////////////////////////////
// CBcComServerTarget command target

class CBcComServerTarget : public CCmdTarget
{
	DECLARE_DYNCREATE(CBcComServerTarget)

	CBcComServerTarget();           // protected constructor used by dynamic creation

// Attributes
public:
	CBcComServerTarget(CBCComClientCtrl *popClientCtrl);

// Operations
public:
	BOOL ConnectToBcComServer();
	BOOL DisconnectFromBcComServer();
	void SetFilter(LPCTSTR strTable, LPCTSTR strCommand, BOOL bOwnBC, LPCTSTR application);
	void TransmitFilter();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBcComServerTarget)
	//}}AFX_VIRTUAL

// Implementation
protected:
	virtual ~CBcComServerTarget();

	// Generated message map functions
	//{{AFX_MSG(CBcComServerTarget)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

// Dispatch maps
	//{{AFX_DISPATCH(CBcComServerTarget)
	afx_msg long OnLostBc(long ilErr);
	afx_msg long OnLostBcEx(LPCTSTR strException);
	afx_msg long OnBc(long size,long currPackage,long totalPackages, LPCTSTR strData);
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()

private:
//Needed for Connection to BCComServer = TCPIP
	_IBcComAtlEvents omAtlComEvents;
	IBcComAtl *pomAtlComInstance;
	IDispatch* pDispAtlComEvents;

	DWORD m_dwAtlComCookie;// used for AfxConnectionAdvise(..) BCComServer
	IUnknown* pIUnknown_Auto;

	CBCComClientCtrl *pomClientCtrl;

};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BCCOMSERVERTARGET_H__F0157D91_42D7_4D44_9403_6335797BD413__INCLUDED_)
