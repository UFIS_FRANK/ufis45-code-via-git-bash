#if !defined(AFX_BCCOMCLIENT_H__5C63BCF5_3522_40D9_A6B7_5D11B4E1EC8B__INCLUDED_)
#define AFX_BCCOMCLIENT_H__5C63BCF5_3522_40D9_A6B7_5D11B4E1EC8B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// BCComClient.h : main header file for BCCOMCLIENT.DLL

#if !defined( __AFXCTL_H__ )
	#error include 'afxctl.h' before including this file
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CBCComClientApp : See BCComClient.cpp for implementation.

class CBCComClientApp : public COleControlModule
{
public:
	BOOL InitInstance();
	int ExitInstance();
};

extern const GUID CDECL _tlid;
extern const WORD _wVerMajor;
extern const WORD _wVerMinor;

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BCCOMCLIENT_H__5C63BCF5_3522_40D9_A6B7_5D11B4E1EC8B__INCLUDED)
