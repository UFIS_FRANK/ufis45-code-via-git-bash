// BCComClientPpg.cpp : Implementation of the CBCComClientPropPage property page class.

#include "stdafx.h"
#include "BCComClient.h"
#include "BCComClientPpg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CBCComClientPropPage, COlePropertyPage)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CBCComClientPropPage, COlePropertyPage)
	//{{AFX_MSG_MAP(CBCComClientPropPage)
	// NOTE - ClassWizard will add and remove message map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CBCComClientPropPage, "BCCOMCLIENT.BCComClientPropPage.1",
	0xf7783fb5, 0xd584, 0x40e1, 0xa3, 0x5a, 0xe, 0x35, 0x49, 0xd0, 0x59, 0xef)


/////////////////////////////////////////////////////////////////////////////
// CBCComClientPropPage::CBCComClientPropPageFactory::UpdateRegistry -
// Adds or removes system registry entries for CBCComClientPropPage

BOOL CBCComClientPropPage::CBCComClientPropPageFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_BCCOMCLIENT_PPG);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}


/////////////////////////////////////////////////////////////////////////////
// CBCComClientPropPage::CBCComClientPropPage - Constructor

CBCComClientPropPage::CBCComClientPropPage() :
	COlePropertyPage(IDD, IDS_BCCOMCLIENT_PPG_CAPTION)
{
	//{{AFX_DATA_INIT(CBCComClientPropPage)
	// NOTE: ClassWizard will add member initialization here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_INIT
}


/////////////////////////////////////////////////////////////////////////////
// CBCComClientPropPage::DoDataExchange - Moves data between page and properties

void CBCComClientPropPage::DoDataExchange(CDataExchange* pDX)
{
	//{{AFX_DATA_MAP(CBCComClientPropPage)
	// NOTE: ClassWizard will add DDP, DDX, and DDV calls here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_MAP
	DDP_PostProcessing(pDX);
}


/////////////////////////////////////////////////////////////////////////////
// CBCComClientPropPage message handlers
