// BcComServerTarget.cpp : implementation file
//

#include "stdafx.h"
#include "bccomclient.h"
#include "BcComServerTarget.h"
#include <process.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static const IID DIID__IBcComAtlEvents = {0x8D66CD3E,0x38E2,0x4631,{0xB2,0x1F,0x42,0xEA,0xCB,0x28,0xAC,0x50}};

/////////////////////////////////////////////////////////////////////////////
// CBcComServerTarget

IMPLEMENT_DYNCREATE(CBcComServerTarget, CCmdTarget)

CBcComServerTarget::CBcComServerTarget()
{
}

CBcComServerTarget::CBcComServerTarget(CBCComClientCtrl *popClientCtrl)
{
	this->pomClientCtrl = popClientCtrl;
}

CBcComServerTarget::~CBcComServerTarget()
{
}


BOOL CBcComServerTarget::ConnectToBcComServer() 
{
	EnableAutomation();

	CLSID clsid;
	HRESULT res;

	pDispAtlComEvents = NULL;
	pIUnknown_Auto = NULL;
	this->m_dwAtlComCookie = 0;

	// receiving the class-ID of BcProxy
	res = CLSIDFromProgID(L"BcComServer.BcComAtl.1",&clsid);
	if (res != S_OK)
	{
		AfxMessageBox (_T("couldn�t get clsid of BCComServer !"));
		return FALSE;
	}

	// *** creating dispatch for events
	if (!omAtlComEvents.CreateDispatch (clsid))
	{
		AfxMessageBox (_T("unable to create BcComServerEvents instance !"));
		return FALSE;
	}

	pDispAtlComEvents = omAtlComEvents.m_lpDispatch ;
	pDispAtlComEvents->AddRef();

	// ^^^^^ pointer to Disp. Interface of BcPrxyEvents

	//connect server to automation class
	pIUnknown_Auto = this->GetIDispatch(TRUE);
	if (AfxConnectionAdvise(pDispAtlComEvents,DIID__IBcComAtlEvents,pIUnknown_Auto,TRUE,&m_dwAtlComCookie))
	{
		pomAtlComInstance = (IBcComAtl*)&omAtlComEvents;
		// ^^^^^ pointer to Disp. Interface of BcPrxyInstance

		CString olAppl = "Dummy";
		CString olPID;
		olPID.Format("%d", _getpid());
		pomAtlComInstance->SetPID(olAppl,olPID);
	}
	return TRUE;
}

BOOL CBcComServerTarget::DisconnectFromBcComServer() 
{
	if (m_dwAtlComCookie != 0)
	{
		AfxConnectionUnadvise(pDispAtlComEvents,DIID__IBcComAtlEvents,pIUnknown_Auto,TRUE,m_dwAtlComCookie);
		m_dwAtlComCookie = 0;
		pDispAtlComEvents->Release();
	}

	if (omAtlComEvents)
	{
		omAtlComEvents.ReleaseDispatch ();
	}

	return TRUE;
}

void CBcComServerTarget::SetFilter(LPCTSTR strTable, LPCTSTR strCommand, BOOL bOwnBC, LPCTSTR application) 
{
	// TODO: Add your dispatch handler code here
	pomAtlComInstance->SetFilter(strTable, strCommand, bOwnBC, application);
}

void CBcComServerTarget::TransmitFilter() 
{
	pomAtlComInstance->TransmitFilter();
	pomAtlComInstance->StartBroadcasting();
}

BEGIN_MESSAGE_MAP(CBcComServerTarget, CCmdTarget)
	//{{AFX_MSG_MAP(CBcComServerTarget)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Dispatch map

BEGIN_DISPATCH_MAP(CBcComServerTarget, CCmdTarget)
	//{{AFX_DISPATCH_MAP(CBcComServerTarget)
	DISP_FUNCTION_ID(CBcComServerTarget,"OnLostBc"	,1,OnLostBc		, VT_I4, VTS_I4 )
	DISP_FUNCTION_ID(CBcComServerTarget,"OnLostBcEx",2,OnLostBcEx	, VT_I4, VTS_BSTR)
	DISP_FUNCTION_ID(CBcComServerTarget,"OnBc"		,3,OnBc			, VT_I4, VTS_I4 VTS_I4 VTS_I4 VTS_BSTR)
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

/////////////////////////////////////////////////////////////////////////////
// Event map

BEGIN_INTERFACE_MAP(CBcComServerTarget, CCmdTarget)
	INTERFACE_PART(CBcComServerTarget, DIID__IBcComAtlEvents, Dispatch)
END_INTERFACE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CBcComServerTarget message handlers

long CBcComServerTarget::OnLostBc(long ilErr)
{
	CString olMsg;
	olMsg.Format ("CEDA Socket error : %d occured",ilErr);
	AfxMessageBox(olMsg,MB_OK|MB_ICONSTOP);
	return S_OK;
}

long CBcComServerTarget::OnLostBcEx(LPCTSTR strException)
{
	CString olMsg;
	olMsg.Format ("Exception : %s occured",strException);
	AfxMessageBox(olMsg,MB_OK|MB_ICONSTOP);
	return S_OK;
}

long CBcComServerTarget::OnBc(long size,long currPackage,long totalPackages,LPCTSTR strData)
{
	this->pomClientCtrl->FireOnBc(size,currPackage,totalPackages,strData);
	return S_OK;
}

