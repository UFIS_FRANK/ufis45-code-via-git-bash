// MaaDialog.cpp : implementation file
//

#include "stdafx.h"
#include "bdpsuif.h"
#include "MaaDialog.h"
#include "CCSGlobl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMaaDialog dialog


CMaaDialog::CMaaDialog(MAADATA *popMaa /*= NULL*/, CCSPtrArray<MAADATA> *popMaaData, CWnd* pParent /*=NULL*/)
	: CDialog(CMaaDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMaaDialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT

	pomMaa = popMaa;
	pomMaaData = popMaaData;
}


void CMaaDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMaaDialog)
	DDX_Control(pDX, IDC_YEAR, m_Year);
	DDX_Control(pDX, IDC_VALUE, m_Value);
	DDX_Control(pDX, IDC_TO_WEEK, m_To_Week);
	DDX_Control(pDX, IDC_FROM_WEEK, m_From_Week);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CMaaDialog, CDialog)
	//{{AFX_MSG_MAP(CMaaDialog)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMaaDialog message handlers

void CMaaDialog::OnOK() 
{
	// TODO: Add extra validation here
	
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;

	if(m_Year.GetStatus() == false)
	{
		ilStatus = false;
		if(m_Year.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_YEAR) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_YEAR) + ogNotFormat;
		}
	}

	if(m_From_Week.GetStatus() == false)
	{
		ilStatus = false;
		if(m_From_Week.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_FROM_WEEK) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_FROM_WEEK) + ogNotFormat;
		}
	}

	if(m_To_Week.GetStatus() == false)
	{
		ilStatus = false;
		if(m_To_Week.GetWindowTextLength() > 0)
		{
			olErrorText += LoadStg(IDS_TO_WEEK) + ogNotFormat;
		}
	}

	if(m_Value.GetStatus() == false)
	{
		ilStatus = false;
		if(m_Value.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_VALUE) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_VALUE) + ogNotFormat;
		}
	}

	AfxGetApp()->DoWaitCursor(-1);

	CString olYear, olWeekFrom, olWeekTo, olValue, olWeek;
	int ilWeekFrom, ilWeekTo;
	
	m_Year.GetWindowText(olYear);
	m_From_Week.GetWindowText(olWeekFrom);
	m_To_Week.GetWindowText(olWeekTo);
	m_Value.GetWindowText(olValue);

	ilWeekFrom = atoi(olWeekFrom);
	if (!olWeekTo.IsEmpty())
		ilWeekTo = atoi(olWeekTo);
	else 
		ilWeekTo = ilWeekFrom;
	
	if(ilWeekFrom > ilWeekTo){
		ilStatus = false;
		//olErrorText += LoadStg(IDS_STRING237);
	}
	
	MAADATA *prlMaa = 0; 
	bool blFound = false;
	CString olJahr, olWoche;

	if (ilStatus == true)	{
		if(pomMaa == NULL){
			for(int i= ilWeekFrom; i<= ilWeekTo; i++){
				olWeek.Format("%i", i);
				blFound = false;
				for(int y = 0; y < pomMaaData->GetSize(); y++){
					prlMaa = 0;
					prlMaa = &pomMaaData->GetAt(y);
					olJahr = prlMaa->Year;
					olWoche = prlMaa->Week;
					if (olJahr == olYear && olWoche == olWeek){
						blFound = true;
						break;
					}
				}

				if(blFound == false){
					MAADATA *polMaa = new MAADATA;
					polMaa->Urno = ogBasicData.GetNextUrno();	//new URNO
					polMaa->Mawu = 0;
					strcpy(polMaa->Hopo,pcgHome);
					strcpy(polMaa->Year, olYear);
					strcpy(polMaa->Week, olWeek);
					strcpy(polMaa->Valu, olValue);
					polMaa->IsChanged = DATA_NEW;
					ogMaaData.Insert(polMaa);
				}	
				else{
					prlMaa->IsChanged = DATA_CHANGED;
					strcpy(prlMaa->Valu, olValue);
					ogMaaData.Update(prlMaa);
				}
			}
		}
		else{
			MAADATA *polMaa = 0;
			long llMapUrno; 
			for(int i = 0; i < pomMaaData->GetSize(); i++)
			{
		
				polMaa = &(*pomMaaData)[i];
				llMapUrno = polMaa->Urno;
				
				if (llMapUrno == pomMaa->Urno)
				{
					strcpy(polMaa->Year, olYear);
					strcpy(polMaa->Week, olWeekFrom);
					strcpy(polMaa->Valu, olValue);
					polMaa->IsChanged = DATA_CHANGED;	

					strcpy(pomMaa->Year, olYear);
					strcpy(pomMaa->Week, olWeekFrom);
					strcpy(pomMaa->Valu, olValue);
					pomMaa->IsChanged = DATA_CHANGED;	


					ogMaaData.Update(polMaa);
					break;
				}
			}
		}

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		//MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_Year.SetFocus();
	}	

}

BOOL CMaaDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	GetDlgItem(IDC_STATIC_YEAR)->SetWindowText(LoadStg(IDS_YEAR));
	GetDlgItem(IDC_STATIC_WEEK2)->SetWindowText(LoadStg(IDS_TO_WEEK));
	GetDlgItem(IDC_STATIC_VALUE)->SetWindowText(LoadStg(IDS_VALUE));

	//m_Year.SetTextLimit(1,4);
	m_Year.SetTypeToInt(2000,3000);
	m_Year.SetBKColor(YELLOW);
	m_Year.SetTextErrColor(RED);
	
	//m_From_Week.SetTextLimit(1,2);
	m_From_Week.SetTypeToInt(1,53);
	m_From_Week.SetBKColor(YELLOW);
	m_From_Week.SetTextErrColor(RED);

	//m_To_Week.SetTextLimit(0,2);
	m_To_Week.SetTypeToInt(0,53);
	m_To_Week.SetTextErrColor(RED);
	
	//m_Value.SetTextLimit(1,5);
	m_Value.SetTypeToInt(1,99999);
	m_Value.SetBKColor(YELLOW);
	m_Value.SetTextErrColor(RED);
	
	if(pomMaa == NULL){
		m_Year.SetInitText(CString(""));
		m_From_Week.SetInitText(CString(""));
		m_To_Week.SetInitText(CString(""));
		m_Value.SetInitText(CString(""));
		m_To_Week.ShowWindow(true);
		GetDlgItem(IDC_STATIC_WEEK1)->SetWindowText(LoadStg(IDS_FROM_WEEK));
		GetDlgItem(IDC_STATIC_WEEK2)->ShowWindow(true);
		m_Year.EnableWindow(true);
		m_From_Week.EnableWindow(true);
	}
	else{
		m_Year.SetInitText(pomMaa->Year);
		m_From_Week.SetInitText(pomMaa->Week);
		m_Value.SetInitText(pomMaa->Valu);
		m_To_Week.ShowWindow(false);
		GetDlgItem(IDC_STATIC_WEEK1)->SetWindowText(LoadStg(IDS_WEEK));
		GetDlgItem(IDC_STATIC_WEEK2)->ShowWindow(false);
		m_Year.EnableWindow(false);
		m_From_Week.EnableWindow(false);
	}


	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
