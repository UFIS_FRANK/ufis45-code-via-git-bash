#if !defined(AFX_ODG_DLG_H__F6CDFB94_0944_11D4_9008_0050DA1CAD13__INCLUDED_)
#define AFX_ODG_DLG_H__F6CDFB94_0944_11D4_9008_0050DA1CAD13__INCLUDED_

#include <resrc1.h>		// main symbols#
#include <basicdata.h>

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Odg_Dlg.h : Header-Datei
//

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld COdg_Dlg 

class COdg_Dlg : public CDialog
{
// Konstruktion
public:
	COdg_Dlg(CWnd* pParent = NULL);   // Standardkonstruktor
	COdg_Dlg(CString OrgCode, CString OrgUrno, CWnd* pParent = NULL);   // Standardkonstruktor

// Dialogfelddaten
	//{{AFX_DATA(COdg_Dlg)
	enum { IDD = IDD_ODG_DLG };
	CComboBox	m_CB_ODG_Code;
	CEdit	m_Ctrl_Rema;
	CEdit	m_Ctrl_Odgn;
	CEdit	m_Ctrl_Code;
	//}}AFX_DATA


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(COdg_Dlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(COdg_Dlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnDelete();
	afx_msg void OnUpdateCode();
	afx_msg void OnSelchangeCombo1();
	afx_msg void OnEditchangeCombo1();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	bool m_gb_NewCode;
	void InitCombo();
	void SaveOdg();
	CString m_Code;
	CString m_Orgu;
	bool SetOdg(CString Urno);
	int ReadOdg();
	int im_O_UrnoPos;
	int im_O_OdgcPos;
	int im_O_OdgnPos;
	int im_O_OrguPos;
	int im_O_RemaPos;	
	CString pomOrgCode;
	CString pomOrgUrno;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_ODG_DLG_H__F6CDFB94_0944_11D4_9008_0050DA1CAD13__INCLUDED_
