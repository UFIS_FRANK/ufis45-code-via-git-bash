#ifndef __DEVICETABLEVIEWER_H__
#define __DEVICETABLEVIEWER_H__

#include <stdafx.h>
#include <CedaDEVData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct DEVICETABLE_LINEDATA
{
	long	Urno;
	CString	Devn;
	CString	Grpn;
	CString	Dadr;
	CString	Dprt;
	CString	Stat;
	CString	Dpid;
	CString	Adid;
	CString	Drnp;
	CString	Dffd;
	CString Dfco;
	CString Dter;
	CString Dare;
	CString Algc;
	CString Dalz;
	CString Ddgi;
	CString Loca;
	CString Rema;
	CString Divn;
	CString Drna;
	CString Dlct;
	CString Dldc;
	CString Dlrd;
	CString Dldt;
	CString	Drgi;
	CString Dbrf;
	CString Dcof;
	CString	Vafr;
	CString	Vato;
};

/////////////////////////////////////////////////////////////////////////////
// DeviceTableViewer

class DeviceTableViewer : public CViewer
{
// Constructions
public:
    DeviceTableViewer(CCSPtrArray<DEVDATA> *popData);
    ~DeviceTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(DEVDATA *prpAirline);
	int  CompareDevice(DEVICETABLE_LINEDATA *prpAirline1, DEVICETABLE_LINEDATA *prpAirline2);
    void MakeLines();
	void MakeLine(DEVDATA *prpAirline);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(DEVICETABLE_LINEDATA *prpAirline);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(DEVICETABLE_LINEDATA *prpLine);
	void ProcessDeviceChange(DEVDATA *prpAirline);
	void ProcessDeviceDelete(DEVDATA *prpAirline);
	bool FindDevice(char *prpDeviceName, int& ilItem);
	bool CreateExcelFile(const CString& ropSeparator,const CString& ropListSeparator);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable			*pomDeviceTable;
	CCSPtrArray<DEVDATA>*pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<DEVICETABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(int ilPage,CUIntArray& ropColsPerPage,DEVICETABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader(int ilPage,CUIntArray& ropColsPerPage);
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;
	CString	omFileName;

};

#endif //__DEVICETABLEVIEWER_H__
