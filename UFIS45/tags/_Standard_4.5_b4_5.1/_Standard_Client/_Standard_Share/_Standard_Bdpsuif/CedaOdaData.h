// CedaOdaData.h

#ifndef __CEDAPODADATA__
#define __CEDAPODADATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------

struct ODADATA 
{
	CTime	 Cdat;			// Erstellungsdatum
	char	 Ctrc[5+2];		// Vertragsart.code
	char	 Dptc[8+2];		// Organisationseinheit,code
	char	 Dura[4+2];		// Dauer (Ersatzstunden)
	CTime	 Lstu;			// Datum letzte �nderung
	char	 Prfl[1+2];		// Protokollierungskennung
	char	 Rema[60+2];	// Bemerkungen
	char	 Sdac[8+2];		// Code
	char	 Sdae[8+2];		// Symbol Extern
	char	 Sdak[12+2];	// Kurzbezeichnung
	char	 Sdan[40+2];	// Bezeichnung
	char	 Sdas[8+2];		// SAP-Code
	char	 Tatp[1+2];		// Auswirkung auf ZK
	char	 Tsap[1+2];		// Send to SAP
	char	 Type[2+2];		// Type Intern
	char	 Upln[1+2];		// in Urlaubsplanung
	long	 Urno;			// Eindeutige Datensatz-Nr.
	char	 Usec[32+2];	// Anwender (Ersteller)
	char	 Useu[32+2];	// Anwender (letzte �nderung)
	char	 Free[3];		// regul�r arbeitsfrei
	char	 Work[1+2];		// absence is just like a shift
	char	 Tbsd[1+2];		// time of absence is just like the shift
	char	Abfr[4+2];		// Absent from
	char    Abto[4+2];      // Absent to
	char    Blen[4+2];      // Length of break
	char     Sdaa[1+2];		// Abk�rzung f�r Urlaubsplanung
	char	 Rgbc[6+2];		// Farbcode der Abwesenheit

	//DataCreated by this class
	int      IsChanged;

	//long, CTime
	ODADATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Cdat=-1; 
		Lstu=-1;
	}

}; // end ODADataStruct

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaOdaData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<ODADATA> omData;

	char pcmListOfFields[2048];

// OOdaations
public:
    CedaOdaData();
	~CedaOdaData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(ODADATA *prpOda);
	bool InsertInternal(ODADATA *prpOda);
	bool Update(ODADATA *prpOda);
	bool UpdateInternal(ODADATA *prpOda);
	bool Delete(long lpUrno);
	bool DeleteInternal(ODADATA *prpOda);
	bool ReadSpecialData(CCSPtrArray<ODADATA> *popOda,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(ODADATA *prpOda);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	ODADATA  *GetOdaByUrno(long lpUrno);
	void SetWhere(const CString& ropWhere){omWhere = ropWhere;} //Prf: 8795
	void GetWhere(CString& ropWhere) const{ropWhere = omWhere;} //Prf: 8795


	// Private methods
private:
    void PrepareOdaData(ODADATA *prpOdaData);
    bool MakeCedaData(CCSPtrArray<CEDARECINFO> *pomRecInfo,CString &pcpListOfData, void *pvpDataStruct);
	CString omWhere; //Prf: 8795
	bool ValidateOdaBcData(const long& lrpUnro); //Prf: 8795

};

//---------------------------------------------------------------------------------------------------------


#endif //__CEDAPODADATA__
