// PoolDlg.cpp : implementation file
//

#include <stdafx.h>
#include <BDPSUIF.h>
#include <PoolDlg.h>
#include <PrivList.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// PoolDlg dialog


PoolDlg::PoolDlg(POLDATA *popPol, CWnd* pParent /*=NULL*/) : CDialog(PoolDlg::IDD, pParent)
{
	pomPol = popPol;
	
	//{{AFX_DATA_INIT(PoolDlg)
	//}}AFX_DATA_INIT
}


void PoolDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(PoolDlg)
	DDX_Control(pDX, IDOK,			 m_OK);
	DDX_Control(pDX, IDC_CDAT_D,	 m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T,	 m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D,	 m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T,	 m_LSTUT);
	DDX_Control(pDX, IDC_USEU,		 m_USEU);
	DDX_Control(pDX, IDC_USEC,		 m_USEC);
	DDX_Control(pDX, IDC_NAME,		 m_NAME);
	DDX_Control(pDX, IDC_POOL,		 m_POOL);
	DDX_Control(pDX, IDC_DTEL,		 m_DTEL);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(PoolDlg, CDialog)
	//{{AFX_MSG_MAP(PoolDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// PoolDlg message handlers
//----------------------------------------------------------------------------------------

BOOL PoolDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_Caption = LoadStg(IDS_STRING842) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);

	char clStat = ogPrivList.GetStat("POOLDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomPol->Cdat.Format("%d.%m.%Y"));
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomPol->Cdat.Format("%H:%M"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomPol->Lstu.Format("%d.%m.%Y"));
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomPol->Lstu.Format("%H:%M"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomPol->Usec);
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomPol->Useu);
	//------------------------------------

	m_NAME.SetTypeToString("X(12)",12,1);
	m_NAME.SetTextErrColor(RED);
	m_NAME.SetBKColor(YELLOW);
	m_NAME.SetInitText(pomPol->Name);
	//------------------------------------

	m_POOL.SetTypeToString("X(32)",32,1);
	m_POOL.SetTextErrColor(RED);
	m_POOL.SetBKColor(YELLOW);
	m_POOL.SetInitText(pomPol->Pool);

	//------------------------------------

	m_DTEL.SetTypeToString("X(20)",20,0);
	m_DTEL.SetInitText(pomPol->Dtel);

	//------------------------------------

	return TRUE;
}

//----------------------------------------------------------------------------------------

void PoolDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;

	
	// Check different values types ////////////////////
	CString olPoolValu,olDtelValu,olNameValu, olValuText;
	CTime  olTime;

	if(m_NAME.GetStatus() == false)
	{
		ilStatus = false;
		if(m_NAME.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING769) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING769) + ogNotFormat;
		}
	}
	else
	{
		m_NAME.GetWindowText(olValuText);
		if(strcmp(pomPol->Name,olValuText) != 0)
		{
			if(ogPolData.PoolNameExits(olValuText))
			{
				ilStatus = false;
				olErrorText += (LoadStg(IDS_STRING844) + LoadStg(IDS_EXIST));
			}
		}
		olNameValu = olValuText;

	}

	if(m_POOL.GetStatus() == false)
	{
		ilStatus = false;
		if(m_POOL.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING769) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING769) + ogNotFormat;
		}
	}
	else
	{
		m_POOL.GetWindowText(olValuText);
		olPoolValu = olValuText;
	}

	if(m_DTEL.GetStatus() == false)
	{
		ilStatus = false;
		if(m_DTEL.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING769) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING769) + ogNotFormat;
		}
	}
	else
	{
		m_DTEL.GetWindowText(olValuText);
		olDtelValu = olValuText;
	}


	
/*
	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olEnam;
		char clWhere[100];
		CCSPtrArray<PARDATA> olParCPA;

		m_ENAM.GetWindowText(olEnam);
		sprintf(clWhere,"WHERE ENAM='%s'",olEnam);
		if(ogParData.ReadSpecialData(&olParCPA,clWhere,"URNO,ENAM",false) == true)
		{
			if(olParCPA[0].Urno != pomPar->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING288) + LoadStg(IDS_EXIST);
			}
		}
		olParCPA.DeleteAll();
	}
*/
	///////////////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{

		strcpy(pomPol->Dtel,olDtelValu);
		strcpy(pomPol->Name,olNameValu);
		strcpy(pomPol->Pool,olPoolValu);
		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
	}
}

//----------------------------------------------------------------------------------------

