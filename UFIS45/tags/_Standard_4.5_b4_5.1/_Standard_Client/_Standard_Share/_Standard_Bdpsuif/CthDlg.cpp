// CthDlg.cpp : implementation file
//

#include <stdafx.h>
#include <bdpsuif.h>
#include <CthDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCthDlg dialog


CCthDlg::CCthDlg(CTHDATA *popCth, CWnd* pParent /*=NULL*/)
	: CDialog(CCthDlg::IDD, pParent)
{
	pomCth = popCth;
	//{{AFX_DATA_INIT(CCthDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CCthDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCthDlg)
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_ABSF, m_ABSF);
	DDX_Control(pDX, IDC_ABST, m_ABST);
	DDX_Control(pDX, IDC_CTHG, m_CTHG);
	DDX_Control(pDX, IDC_REDU, m_REDU);
	DDX_Control(pDX, IDC_YEAR, m_YEAR);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEC, m_USEC);
	DDX_Control(pDX, IDC_USEU, m_USEU);
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCthDlg, CDialog)
	//{{AFX_MSG_MAP(CCthDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCthDlg message handlers

void CCthDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	CString olTmp;
	
	if(m_CTHG.GetStatus() == false)
	{
		ilStatus = false;
		if(m_CTHG.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING237) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING237) + ogNotFormat;
		}
	}

	//uhi 18.4.01
	if(m_YEAR.GetStatus() == false)
	{
		ilStatus = false;
		if(m_YEAR.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING681) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING681) + ogNotFormat;
		}
	}

	if(m_ABSF.GetStatus() == false)
	{
		ilStatus = false;
		if(m_ABSF.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING775) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING775) + ogNotFormat;
		}
	}

	if(m_ABST.GetStatus() == false)
	{
		ilStatus = false;
		if(m_ABST.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING776) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING776) + ogNotFormat;
		}
	}

	if(m_REDU.GetStatus() == false)
	{
		ilStatus = false;
		if(m_REDU.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING774) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING774) + ogNotFormat;
		}
	}
	
		
	///////////////////////////////////////////////////////////////////////////

	//Konsistenz-Check//

	if(ilStatus == true)
	{
		CString olCthg;
		CString olRedu;
		CString olYear;
		CCSPtrArray<CTHDATA> olCthCPA;
		char clWhere[100];

		m_CTHG.GetWindowText(olCthg);
		m_YEAR.GetWindowText(olYear);
		m_REDU.GetWindowText(olRedu);
		if ( (olYear.GetLength() == 4) && (olCthg.GetLength() >= 1) )
		{
			sprintf(clWhere,"WHERE CTHG='%s' AND YEAR='%s' AND REDU='%s'",olCthg,olYear,olRedu);
			if(ogCthData.ReadSpecialData(&olCthCPA,clWhere,"URNO",false) == true)
			{
				if(olCthCPA[0].Urno != pomCth->Urno)
				{
					ilStatus = false;
					olErrorText += LoadStg(IDS_STRING824) + LoadStg(IDS_EXIST);
				}
			}
		}
		else
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING824);
		}
	}

	///////////////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if(ilStatus == true)
	{
		CString olText;

		m_CTHG.GetWindowText(pomCth->Cthg,2+2);
		m_REDU.GetWindowText(pomCth->Redu,2+2);
		m_YEAR.GetWindowText(pomCth->Year,4+2);
		m_ABST.GetWindowText(olText);
		if ( (olText.GetLength() < 5) && (olText.Find(".") < 1) )
			olText += ".00";
		strcpy(pomCth->Abst,olText);
		m_ABSF.GetWindowText(olText);
		if ( (olText.GetLength() < 5) && (olText.Find(".") < 1) ) 
			olText += ".00";
		strcpy(pomCth->Absf,olText);

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_CTHG.SetFocus();
		m_CTHG.SetSel(0,-1);
	}
}

BOOL CCthDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_Caption = LoadStg(IDS_STRING778) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	char clStat = ogPrivList.GetStat("CTHDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomCth->Cdat.Format("%d.%m.%Y"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomCth->Cdat.Format("%H:%M"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomCth->Lstu.Format("%d.%m.%Y"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomCth->Lstu.Format("%H:%M"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomCth->Usec);
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomCth->Useu);
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_ABST.SetTypeToDouble(4, 2, 0000.01,9999.99);
	m_ABST.SetBKColor(YELLOW);  
	m_ABST.SetTextErrColor(RED);
	m_ABST.SetInitText(pomCth->Abst);
	//------------------------------------
	m_CTHG.SetFormat("x|#x|#");
	m_CTHG.SetTextLimit(1,2);
	m_CTHG.SetBKColor(YELLOW);  
	m_CTHG.SetTextErrColor(RED);
	m_CTHG.SetInitText(pomCth->Cthg);
	//------------------------------------
	m_REDU.SetFormat("##");
	m_REDU.SetTextLimit(0,2);
	m_REDU.SetBKColor(YELLOW);  
	m_REDU.SetTextErrColor(RED);
	m_REDU.SetInitText(pomCth->Redu);
	//------------------------------------
	m_YEAR.SetFormat("####");
	m_YEAR.SetTextLimit(4,4);
	m_YEAR.SetBKColor(YELLOW);  
	m_YEAR.SetTextErrColor(RED);
	m_YEAR.SetInitText(pomCth->Year);
	//------------------------------------
	m_ABSF.SetTypeToDouble(4, 2, 0000.01,9999.99);
	m_ABSF.SetBKColor(YELLOW);  
	m_ABSF.SetTextErrColor(RED);
	m_ABSF.SetInitText(pomCth->Absf);
	//------------------------------------
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
