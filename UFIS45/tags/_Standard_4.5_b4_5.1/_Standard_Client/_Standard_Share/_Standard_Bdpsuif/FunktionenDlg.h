#if !defined(AFX_FUNKTIONENDLG_H__AB36A831_7540_11D1_B430_0000B45A33F5__INCLUDED_)
#define AFX_FUNKTIONENDLG_H__AB36A831_7540_11D1_B430_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// FunktionenDlg.h : header file
//
#include <CedaSphData.h>
#include <CCSEdit.h>
#include <CedaPfcData.h>
#include <CedaOrgData.h>
/////////////////////////////////////////////////////////////////////////////
// FunktionenDlg dialog

class FunktionenDlg : public CDialog
{
// Construction
public:
	FunktionenDlg(PFCDATA *popPfc, CWnd* pParent = NULL);   // standard constructor

	PFCDATA *pomPfc;
	CStatic	*pomStatus;
// Dialog Data
	//{{AFX_DATA(FunktionenDlg)
	enum { IDD = IDD_FUNKTIONEN };
	CButton	m_OK;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_DPTC;
	CCSEdit	m_FCTC;
	CCSEdit	m_FCTN;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	//uhi 11.5.01
	//CCSEdit	m_PRIO;
	CCSEdit	m_REMA;
	CCSEdit	m_USEU;
	CString m_Caption;
	CCSEdit	m_USEC;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(FunktionenDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(FunktionenDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnBOrgAw();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	CString	omOldCode;
	bool	bmChangeAction;	
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FUNKTIONENDLG_H__AB36A831_7540_11D1_B430_0000B45A33F5__INCLUDED_)
