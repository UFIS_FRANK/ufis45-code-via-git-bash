#if !defined(AFX_CCSCOMBOBOX_H__D2323DF1_97C0_11D1_B3FC_0000C016B067__INCLUDED_)
#define AFX_CCSCOMBOBOX_H__D2323DF1_97C0_11D1_B3FC_0000C016B067__INCLUDED_

// CCSComboBox.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// CCSComboBox window


// Written by:
// ARE			28.01.1998


//@Memo:	The CCSComboBox class provides additional functionality of a MFC CComboBox control. 

//@See:		MFC's CWnd, MFC's CComboBox

/*@Doc:

*/

/////////////////////////////////////////////////////////////////////////////

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
/////////////////////////////////////////////////////////////////////////////

class CCSComboBox : public CComboBox
{
// Construction
public:
	CCSComboBox();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCSComboBox)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CCSComboBox();

	//Sets the current background color to the specified color.
	void SetBKColor(COLORREF opColor);
	//Sets the current text color to the specified color.
	void SetTextColor(COLORREF opColor);
	//Enables or disable the Box and set back the specified color 
	BOOL EnableWindow(BOOL BmEnable = TRUE);
	//Searches for a substring in the list box and, if the substring is found,
	//selects the string when bmBool = true. Retrieves the index of the currently selected item
	int SetCurSelByString(CString omString,bool bmBool=true);

// Generated message map functions
protected:
	//{{AFX_MSG(CCSComboBox)
	afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

protected:

	COLORREF omBKColor;
	COLORREF omActualBKColor;
	COLORREF omTextColor;
	COLORREF omActualTextColor;
	CBrush omBrush;

};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CCSCOMBOBOX_H__D2323DF1_97C0_11D1_B3FC_0000C016B067__INCLUDED_)
