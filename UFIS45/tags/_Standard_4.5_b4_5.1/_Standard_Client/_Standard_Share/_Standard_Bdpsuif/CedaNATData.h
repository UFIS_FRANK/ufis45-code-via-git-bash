// CedaNATData.h

#ifndef __CEDANATDATA__
#define __CEDANATDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------
// Record structure declaration

struct NATDATA 
{
	CTime	 Cdat; 		// Erstellungsdatum
	CTime	 Lstu; 		// Datum letzte �nderung
	char 	 Prfl[3]; 	// Protokollierungskennzeichen
	char 	 Tnam[32]; 	// Verkehrsart Name
	char 	 Ttyp[7]; 	// Verkehrsart
	long 	 Urno; 		// Eindeutige Datensatz-Nr.
	char 	 Usec[34]; 	// Anwender (Ersteller)
	char 	 Useu[34]; 	// Anwender (letzte �nderung)
	CTime	 Vafr; 		// G�ltig von
	CTime	 Vato; 		// G�ltig bis

	char     Styp[3];   //Service Type
	char     Flti[3];   //Flight-Id
	char     Apc4[5];   //Airport ICAO Code
	char     Alc3[4];   //Airline ICAO Code

	//DataCreated by this class
	int		 IsChanged;

	NATDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Cdat=-1;
		Lstu=-1;
		Vafr=-1;
		Vato=-1;
	}

}; // end NATDataStrukt


//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaNATData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;

    CCSPtrArray<NATDATA> omData;

	char pcmNATFieldList[2048];

// Operations
public:
    CedaNATData();
	~CedaNATData();

	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);

	void ClearAll(void);

    bool Read(char *pcpWhere = NULL);
	bool ReadSpecialData(CCSPtrArray<NATDATA> *popNat,char *pspWhere,char *pspFieldList,bool ipSYS = true);
	bool InsertNAT(NATDATA *prpNAT,BOOL bpSendDdx = TRUE);
	bool InsertNATInternal(NATDATA *prpNAT);
	bool UpdateNAT(NATDATA *prpNAT,BOOL bpSendDdx = TRUE);
	bool UpdateNATInternal(NATDATA *prpNAT);
	bool DeleteNAT(long lpUrno);
	bool DeleteNATInternal(NATDATA *prpNAT);
	NATDATA  *GetNATByUrno(long lpUrno);
	bool SaveNAT(NATDATA *prpNAT);
	void ProcessNATBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

	inline bool IsNatureAutoCalc() const { return bmNatureAutoCalc;}
	
	void SetWhere(const CString& ropWhere){omWhere = ropWhere;} //Prf: 8795
	void GetWhere(CString& ropWhere) const{ropWhere = omWhere;} //Prf: 8795

	// Private methods
private:
    void PrepareNATData(NATDATA *prpNATData);	
	bool ValidateNATBcData(const long& lrpUnro); //Prf: 8795
	
private:
	char pcmNatureAutoCalc[5];
	bool bmNatureAutoCalc;
	CString omWhere; //Prf: 8795
};

//---------------------------------------------------------------------------------------------------------

#endif //__CEDANATDATA__
