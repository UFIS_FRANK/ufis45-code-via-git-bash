// HandlingagentTableViewer.cpp 
//

#include <stdafx.h>
#include <HandlingagentTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void HandlingagentTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// HandlingagentTableViewer
//

HandlingagentTableViewer::HandlingagentTableViewer(CCSPtrArray<HAGDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomHandlingagentTable = NULL;
    ogDdx.Register(this, HAG_CHANGE, CString("HANDLINGAGENTTABLEVIEWER"), CString("Handlingagent Update/new"), HandlingagentTableCf);
    ogDdx.Register(this, HAG_DELETE, CString("HANDLINGAGENTTABLEVIEWER"), CString("Handlingagent Delete"), HandlingagentTableCf);
}

//-----------------------------------------------------------------------------------------------

HandlingagentTableViewer::~HandlingagentTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void HandlingagentTableViewer::Attach(CCSTable *popTable)
{
    pomHandlingagentTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void HandlingagentTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void HandlingagentTableViewer::MakeLines()
{
	int ilHandlingagentCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilHandlingagentCount; ilLc++)
	{
		HAGDATA *prlHandlingagentData = &pomData->GetAt(ilLc);
		MakeLine(prlHandlingagentData);
	}
}

//-----------------------------------------------------------------------------------------------

void HandlingagentTableViewer::MakeLine(HAGDATA *prpHandlingagent)
{

    //if( !IsPassFilter(prpHandlingagent)) return;

    // Update viewer data for this shift record
    HANDLINGAGENTTABLE_LINEDATA rlHandlingagent;

	rlHandlingagent.Urno = prpHandlingagent->Urno; 
	rlHandlingagent.Hsna = prpHandlingagent->Hsna; 
	rlHandlingagent.Hnam = prpHandlingagent->Hnam; 
	rlHandlingagent.Tele = prpHandlingagent->Tele; 
	rlHandlingagent.Faxn = prpHandlingagent->Faxn; 
	rlHandlingagent.Vafr = prpHandlingagent->Vafr.Format("%d.%m.%Y %H:%M"); 
	rlHandlingagent.Vato = prpHandlingagent->Vato.Format("%d.%m.%Y %H:%M"); 
	
	CreateLine(&rlHandlingagent);
}

//-----------------------------------------------------------------------------------------------

void HandlingagentTableViewer::CreateLine(HANDLINGAGENTTABLE_LINEDATA *prpHandlingagent)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareHandlingagent(prpHandlingagent, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	HANDLINGAGENTTABLE_LINEDATA rlHandlingagent;
	rlHandlingagent = *prpHandlingagent;
    omLines.NewAt(ilLineno, rlHandlingagent);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void HandlingagentTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 5;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomHandlingagentTable->SetShowSelection(TRUE);
	pomHandlingagentTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING271),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 332; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING271),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 83; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING271),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 83; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING271),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING271),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING271),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomHandlingagentTable->SetHeaderFields(omHeaderDataArray);

	pomHandlingagentTable->SetDefaultSeparator();
	pomHandlingagentTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Hsna;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Hnam;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Tele;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Faxn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vafr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vato;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomHandlingagentTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomHandlingagentTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString HandlingagentTableViewer::Format(HANDLINGAGENTTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool HandlingagentTableViewer::FindHandlingagent(char *pcpHandlingagentKeya, char *pcpHandlingagentKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpHandlingagentKeya) &&
			 (omLines[ilItem].Keyd == pcpHandlingagentKeyd)    )
			return TRUE;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void HandlingagentTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    HandlingagentTableViewer *polViewer = (HandlingagentTableViewer *)popInstance;
    if (ipDDXType == HAG_CHANGE) polViewer->ProcessHandlingagentChange((HAGDATA *)vpDataPointer);
    if (ipDDXType == HAG_DELETE) polViewer->ProcessHandlingagentDelete((HAGDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void HandlingagentTableViewer::ProcessHandlingagentChange(HAGDATA *prpHandlingagent)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpHandlingagent->Hsna;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpHandlingagent->Hnam;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpHandlingagent->Tele;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpHandlingagent->Faxn;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpHandlingagent->Vafr.Format("%d.%m.%Y %H:%M");
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpHandlingagent->Vato.Format("%d.%m.%Y %H:%M");
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	if (FindLine(prpHandlingagent->Urno, ilItem))
	{
        HANDLINGAGENTTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpHandlingagent->Urno;
		prlLine->Hsna = prpHandlingagent->Hsna;
		prlLine->Hnam = prpHandlingagent->Hnam;
		prlLine->Tele = prpHandlingagent->Tele;
		prlLine->Faxn = prpHandlingagent->Faxn;
		prlLine->Vafr = prpHandlingagent->Vafr.Format("%d.%m.%Y %H:%M");
		prlLine->Vato = prpHandlingagent->Vato.Format("%d.%m.%Y %H:%M");

		pomHandlingagentTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomHandlingagentTable->DisplayTable();
	}
	else
	{
		MakeLine(prpHandlingagent);
		if (FindLine(prpHandlingagent->Urno, ilItem))
		{
	        HANDLINGAGENTTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomHandlingagentTable->AddTextLine(olLine, (void *)prlLine);
				pomHandlingagentTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void HandlingagentTableViewer::ProcessHandlingagentDelete(HAGDATA *prpHandlingagent)
{
	int ilItem;
	if (FindLine(prpHandlingagent->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomHandlingagentTable->DeleteTextLine(ilItem);
		pomHandlingagentTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool HandlingagentTableViewer::IsPassFilter(HAGDATA *prpHandlingagent)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int HandlingagentTableViewer::CompareHandlingagent(HANDLINGAGENTTABLE_LINEDATA *prpHandlingagent1, HANDLINGAGENTTABLE_LINEDATA *prpHandlingagent2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpHandlingagent1->Tifd == prpHandlingagent2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpHandlingagent1->Tifd > prpHandlingagent2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void HandlingagentTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool HandlingagentTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void HandlingagentTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void HandlingagentTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING160);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool HandlingagentTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool HandlingagentTableViewer::PrintTableLine(HANDLINGAGENTTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
			case 0:
				{
					rlElement.FrameLeft  = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Hsna;
				}
				break;
			case 1:
				{
					rlElement.Text		= prpLine->Hnam;
				}
				break;
			case 2:
				{
					rlElement.Text		= prpLine->Tele;
				}
				break;
			case 3:
				{
					rlElement.Text		= prpLine->Faxn;
				}
				break;
			case 4:
				{
					rlElement.Text		= prpLine->Vafr;
				}
				break;
			case 5:
				{
					rlElement.FrameRight = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Vato;
				}
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
