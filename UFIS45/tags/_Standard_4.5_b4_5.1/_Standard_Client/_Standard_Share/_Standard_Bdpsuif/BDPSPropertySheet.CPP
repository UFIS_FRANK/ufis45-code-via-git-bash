// flplanps.cpp : implementation file
//

// These following include files are project dependent
#include <stdafx.h>
#include <BDPSUIF.h>
#include <CCSGlobl.h>
#include <CCSCedadata.h>

// These following include files are necessary
#include <cviewer.h>
#include <BasicData.h>
#include <BDPSPropertySheet.h>
#include <StringConst.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CStringArray ogFlightAlcd;

/////////////////////////////////////////////////////////////////////////////
// BDPSPropertySheet
//

BDPSPropertySheet::BDPSPropertySheet(CString opCalledFrom, CWnd* pParentWnd,
	CViewer *popViewer, UINT iSelectPage) : BasePropertySheet
	(opCalledFrom, pParentWnd, popViewer, iSelectPage)
{
	omCalledFrom = opCalledFrom;
	AddPage(&m_PSUniFilter);
	m_PSUniFilter.SetCalledFrom(omCalledFrom);
	AddPage(&m_PSUniSortPage);
	m_PSUniSortPage.SetCalledFrom(omCalledFrom);
	if(omCalledFrom == LoadStg(IDS_STRING183) || omCalledFrom == LoadStg(IDS_STRING164)) // Employee base || Basic shifts
	{
		omSorCodeText = LoadStg(IDS_STRING876);
		AddPage(&m_PSSorFilterPage);
		m_PSSorFilterPage.SetCaption(omSorCodeText);

		omSpfCodeText = LoadStg(IDS_STRING877);
		AddPage(&m_PSSpfFilterPage);
		m_PSSpfFilterPage.SetCaption(omSpfCodeText);
	}
}

void BDPSPropertySheet::LoadDataFromViewer()
{
	pomViewer->GetFilter("UNIFILTER", m_PSUniFilter.omValues);
//	pomViewer->GetSort("UNISORT", m_PSUniSortPage.omValues);

	if(omCalledFrom == LoadStg(IDS_STRING183) || omCalledFrom == LoadStg(IDS_STRING164)) // Employee base || Basic shifts
	{
		pomViewer->GetSort("SPECIALSORT", m_PSUniSortPage.omValues);

		CStringArray olPossibleFilters;
		pomViewer->GetFilterPage(olPossibleFilters);
		if (olPossibleFilters.GetSize() == 1)
		{
			olPossibleFilters.RemoveAll();
			olPossibleFilters.Add("SORFILTER");
			olPossibleFilters.Add("SPFFILTER");
			pomViewer->AddFilterPage(olPossibleFilters);

			CStringArray olAllFilter;
			olAllFilter.Add(LoadStg(IDS_ALL));
			pomViewer->SetFilter("SORFILTER",olAllFilter);
			pomViewer->SetFilter("SPFFILTER",olAllFilter);

		}

		pomViewer->GetFilter("SORFILTER", m_PSSorFilterPage.omSelectedItems);
		pomViewer->GetFilter("SPFFILTER", m_PSSpfFilterPage.omSelectedItems);
	}
	else
	{
		pomViewer->GetSort("UNISORT", m_PSUniSortPage.omValues);
	}
}

void BDPSPropertySheet::SaveDataToViewer(CString opViewName,BOOL bpSaveToDb)
{
	pomViewer->SetFilter("UNIFILTER", m_PSUniFilter.omValues);
	if(omCalledFrom == LoadStg(IDS_STRING183) || omCalledFrom == LoadStg(IDS_STRING164)) // Employee base || Basic shifts
	{
		pomViewer->SetFilter("SORFILTER", m_PSSorFilterPage.omSelectedItems);
		pomViewer->SetFilter("SPFFILTER", m_PSSpfFilterPage.omSelectedItems);

		CStringArray olSpecialSort;
		for (int i = m_PSUniSortPage.omValues.GetSize() - 1; i>= 0; i--)
		{
			if (m_PSUniSortPage.omValues[i] == "SPFCODE+" || m_PSUniSortPage.omValues[i] == "SPFCODE-")
			{
				olSpecialSort.Add(m_PSUniSortPage.omValues[i]);
				m_PSUniSortPage.omValues.RemoveAt(i);
			}
		}

		pomViewer->SetSort("SPECIALSORT",olSpecialSort);
		pomViewer->SetSort("UNISORT", m_PSUniSortPage.omValues);
	}
	else
	{
		pomViewer->SetSort("UNISORT", m_PSUniSortPage.omValues);
	}

	if (bpSaveToDb == TRUE)
	{
		pomViewer->SafeDataToDB(opViewName);
	}

}

int BDPSPropertySheet::QueryForDiscardChanges()
{
/*	CStringArray olSelectedAirlines;
	CStringArray olSelectedArrivals;
	CStringArray olSelectedDepartures;
	CStringArray olSortOrders;
	CString olGroupBy;
	CStringArray olSelectedTime;
	pomViewer->GetFilter("Airline", olSelectedAirlines);
	pomViewer->GetFilter("Gate Arrival", olSelectedArrivals);
	pomViewer->GetFilter("Gate Departure", olSelectedDepartures);
	pomViewer->GetSort(olSortOrders);
	olGroupBy = pomViewer->GetGroup();
	pomViewer->GetFilter("Zeit", olSelectedTime);

	CStringArray olInbound, olOutbound;
	pomViewer->GetFilter("Inbound", olInbound);
	pomViewer->GetFilter("Outbound", olOutbound);

	if (!IsIdentical(olSelectedAirlines, m_pageAirline.omSelectedItems) ||
		!IsIdentical(olSelectedArrivals, m_pageArrival.omSelectedItems) ||
		!IsIdentical(olSelectedDepartures, m_pageDeparture.omSelectedItems) ||
		!IsIdentical(olSortOrders, m_pageSort.omSortOrders) ||
		olGroupBy != CString(m_pageSort.m_Group + '0') ||
		!IsIdentical(olSelectedTime, m_pageZeit.omFilterValues) ||
		olInbound[0] != CString(m_pageBound.m_Inbound + '0') ||
		olOutbound[0] != CString(m_pageBound.m_Outbound + '0'))
		return MessageBox(ID_MESSAGE_LOST_CHANGES, NULL, MB_ICONQUESTION | MB_OKCANCEL);
*/
	return IDOK;
}
