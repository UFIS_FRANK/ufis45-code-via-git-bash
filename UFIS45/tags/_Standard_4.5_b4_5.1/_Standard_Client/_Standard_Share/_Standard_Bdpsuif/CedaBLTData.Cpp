// CedaBLTData.cpp
 
#include <stdafx.h>
#include <CedaBLTData.h>
#include <resource.h>
#include <BasicData.h>
#include <CedaSgmData.h>
#include <CedaAloData.h>
#include <CedaSgrData.h>

// Local function prototype
static void ProcessBLTCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

const CString BAGGAT = "BAGGAT";
const CString EXTBAG = "EXTBAG";
//--CEDADATA-----------------------------------------------------------------------------------------------

CedaBLTData::CedaBLTData() : CCSCedaData(&ogCommHandler)
{
    // Create an array of CEDARECINFO for BLTDATA
	BEGIN_CEDARECINFO(BLTDATA,BLTDataRecInfo)
		FIELD_CHAR_TRIM	(Bnam,"BNAM")
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_DATE		(Lstu,"LSTU")
//		FIELD_DATE		(Nafr,"NAFR")
//		FIELD_DATE		(Nato,"NATO")
//		FIELD_CHAR_TRIM	(Resn,"RESN")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Tele,"TELE")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_DATE		(Vafr,"VAFR")
		FIELD_DATE		(Vato,"VATO")
		FIELD_CHAR_TRIM	(Term,"TERM")
		FIELD_CHAR_TRIM	(Home,"HOME")
		FIELD_CHAR_TRIM	(Bltt,"BLTT")
		FIELD_CHAR_TRIM	(Stat,"STAT")
		FIELD_CHAR_TRIM	(Maxf,"MAXF")
		FIELD_CHAR_TRIM	(Defd,"DEFD")
		FIELD_LONG		(Ibit,"IBIT")
	END_CEDARECINFO //(BLTDataAStruct)

	// Copy the record structure
	for (int i=0; i< sizeof(BLTDataRecInfo)/sizeof(BLTDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&BLTDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"BLT");
//	strcpy(pcmBLTFieldList,"BNAM,CDAT,LSTU,NAFR,NATO,RESN,PRFL,TELE,URNO,USEC,USEU,VAFR,VATO,TERM,HOME,BLTT,STAT,MAXF,DEFD");
	strcpy(pcmBLTFieldList,"BNAM,CDAT,LSTU,PRFL,TELE,URNO,USEC,USEU,VAFR,VATO,TERM,HOME,BLTT,STAT,MAXF,DEFD");
	pcmFieldList = pcmBLTFieldList;

	omData.SetSize(0,1000);
	//omUrnoMap.InitHashTable(2001);
}

//----------------------------------------------------------------------------------------------------

void CedaBLTData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("BNAM");
	ropFields.Add("CDAT");
	ropFields.Add("LSTU");
	ropFields.Add("NAFR");
	ropFields.Add("NATO");
	ropFields.Add("RESN");
	ropFields.Add("PRFL");
	ropFields.Add("TELE");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	ropFields.Add("VAFR");
	ropFields.Add("VATO");
	ropFields.Add("TERM");
	ropFields.Add("HOME");
	ropFields.Add("BLTT");
	ropFields.Add("STAT");
	ropFields.Add("MAXF");
	ropFields.Add("DEFD");
	if (ogBasicData.IsGatPosEnabled())
		ropFields.Add("IBIT");

	ropDesription.Add(LoadStg(IDS_STRING314));
	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING364));
	ropDesription.Add(LoadStg(IDS_STRING365));
	ropDesription.Add(LoadStg(IDS_STRING366));
	ropDesription.Add(LoadStg(IDS_STRING345));
	ropDesription.Add(LoadStg(IDS_STRING294));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));
	ropDesription.Add(LoadStg(IDS_STRING230));
	ropDesription.Add(LoadStg(IDS_STRING231));
	ropDesription.Add(LoadStg(IDS_STRING296));
	ropDesription.Add(LoadStg(IDS_STRING711));
	ropDesription.Add(LoadStg(IDS_STRING1025));
	ropDesription.Add(LoadStg(IDS_STRING1023));
	ropDesription.Add(LoadStg(IDS_STRING1024));
	ropDesription.Add(LoadStg(IDS_STRING856));
	if (ogBasicData.IsGatPosEnabled())
		ropDesription.Add(LoadStg(IDS_STRING909));

	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	if (ogBasicData.IsGatPosEnabled())
		ropType.Add("String");

}

//----------------------------------------------------------------------------------------------------

void CedaBLTData::Register(void)
{
	ogDdx.Register((void *)this,BC_BLT_CHANGE,CString("BLTDATA"), CString("BLT-changed"),ProcessBLTCf);
	ogDdx.Register((void *)this,BC_BLT_DELETE,CString("BLTDATA"), CString("BLT-deleted"),ProcessBLTCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaBLTData::~CedaBLTData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaBLTData::ClearAll(void)
{
    omUrnoMap.RemoveAll();
	omNameMap.RemoveAll();
    omData.DeleteAll();
	ogDdx.UnRegister(this,NOTUSED);
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaBLTData::Read(char *pcpWhere)
{
	if (ogBasicData.IsGatPosEnabled())
		strcpy(pcmBLTFieldList,"BNAM,CDAT,LSTU,PRFL,TELE,URNO,USEC,USEU,VAFR,VATO,TERM,HOME,BLTT,STAT,MAXF,DEFD,IBIT");
	pcmFieldList = pcmBLTFieldList;

    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
	omNameMap.RemoveAll();
    omData.DeleteAll();
	if(pcpWhere != NULL)
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"), pcpWhere);
	else
		ilRc = CedaAction(ogBasicData.GetCedaCommand("RT"));
	if (ilRc != true)
	{
		return false;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		BLTDATA *prpBLT = new BLTDATA;
		if ((ilRc = GetFirstBufferRecord(prpBLT)) == true)
		{
			prpBLT->IsChanged = DATA_UNCHANGED;
			omData.Add(prpBLT);//Update omData
			omUrnoMap.SetAt((void *)prpBLT->Urno,prpBLT);
			omNameMap.SetAt(prpBLT->Bnam,prpBLT);
		}
		else
		{
			delete prpBLT;
		}
	}
	//("Read: %d gelesen\n",ilLc-1);
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaBLTData::InsertBLT(BLTDATA *prpBLT,BOOL bpSendDdx)
{
	prpBLT->IsChanged = DATA_NEW;
	if(SaveBLT(prpBLT) == false) return false; //Update Database
	InsertBLTInternal(prpBLT);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaBLTData::InsertBLTInternal(BLTDATA *prpBLT)
{
	//PrepareBLTData(prpBLT);
	ogDdx.DataChanged((void *)this, BLT_CHANGE,(void *)prpBLT ); //Update Viewer
	omData.Add(prpBLT);//Update omData
	omUrnoMap.SetAt((void *)prpBLT->Urno,prpBLT);
	omNameMap.SetAt(prpBLT->Bnam,prpBLT);
	return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaBLTData::DeleteBLT(long lpUrno)
{
	BLTDATA *prlBLT = GetBLTByUrno(lpUrno);
	if (prlBLT != NULL)
	{
		prlBLT->IsChanged = DATA_DELETED;
		if(SaveBLT(prlBLT) == false) return false; //Update Database
		DeleteFromSgrSgm(*prlBLT);
		DeleteBLTInternal(prlBLT);
	}
    return true;
}

//--DELETEFROMSGRSGM----------------------------------------------------------------------------------------

void CedaBLTData::DeleteFromSgrSgm(const BLTDATA &ropBLT)
{
	SGMDATA olSgmData;							
	long olAloUrno = ogAloData.GetAloUrnoByName(BAGGAT);
	
	olSgmData.IsChanged = 0;
	int oliCount = 0;
	char olpWhere[124];
	CCSPtrArray<SGMDATA> olaSgmData;	

	sprintf(olpWhere,"WHERE UGTY = '%ld' AND UVAL = '%ld'",olAloUrno,ropBLT.Urno);
	ogSgmData.ReadSpecial(olaSgmData,olpWhere);	

	if(olaSgmData.GetSize() != 0)
	{
		for(int i = 0; i < olaSgmData.GetSize(); i++)
		{
			olSgmData = olaSgmData.GetAt(i);
			ogSgmData.Delete(&olSgmData,true);
		}
		olaSgmData.DeleteAll();
	}
	
	memset(&olSgmData,'\0',sizeof(olSgmData));
	memset(olpWhere,'\0',124);
	olAloUrno = ogAloData.GetAloUrnoByName(EXTBAG);	
	sprintf(olpWhere,"WHERE UGTY = '%ld' AND VALU = '%ld'",olAloUrno,ropBLT.Urno);	
	ogSgmData.ReadSpecial(olaSgmData,olpWhere);
	if(olaSgmData.GetSize() != 0)
	{
		for(int i = 0; i < olaSgmData.GetSize(); i++)
		{
			olSgmData = olaSgmData.GetAt(i);
			ogSgmData.Delete(&olSgmData,true);
		}
		olaSgmData.DeleteAll();
	}

	SGRDATA olSgrData;							
	CCSPtrArray<SGRDATA> olaSgrData;
	memset(olpWhere,'\0',124);	
	sprintf(olpWhere,"WHERE UGTY = '%ld' AND STYP = '%ld'",olAloUrno,ropBLT.Urno);
	ogSgrData.ReadSpecial(olaSgrData,olpWhere);
	if(olaSgrData.GetSize() != 0)
	{
		for(int i = 0; i < olaSgrData.GetSize(); i++)
		{
			olSgrData = olaSgrData.GetAt(i);
			ogSgrData.Delete(&olSgrData,true);
		}
		olaSgrData.DeleteAll();
	}
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaBLTData::DeleteBLTInternal(BLTDATA *prpBLT)
{
	ogDdx.DataChanged((void *)this,BLT_DELETE,(void *)prpBLT); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpBLT->Urno);
	omNameMap.RemoveKey(prpBLT->Bnam);
	int ilBLTCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilBLTCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpBLT->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--PREPARE-DATA-------------------------------------------------------------------------------------------

void CedaBLTData::PrepareBLTData(BLTDATA *prpBLT)
{
	// TODO: add code here
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaBLTData::UpdateBLT(BLTDATA *prpBLT,BOOL bpSendDdx)
{
	if (GetBLTByUrno(prpBLT->Urno) != NULL)
	{
		if (prpBLT->IsChanged == DATA_UNCHANGED)
		{
			prpBLT->IsChanged = DATA_CHANGED;
		}
		if(SaveBLT(prpBLT) == false) return false; //Update Database
		UpdateBLTInternal(prpBLT);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaBLTData::UpdateBLTInternal(BLTDATA *prpBLT)
{
	BLTDATA *prlBLT = GetBLTByUrno(prpBLT->Urno);
	if (prlBLT != NULL)
	{
		*prlBLT = *prpBLT; //Update omData
		ogDdx.DataChanged((void *)this,BLT_CHANGE,(void *)prlBLT); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

BLTDATA *CedaBLTData::GetBLTByUrno(long lpUrno)
{
	BLTDATA  *prlBLT;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlBLT) == TRUE)
	{
		return prlBLT;
	}
	return NULL;
}

//--GET-BY-NAME--------------------------------------------------------------------------------------------

BLTDATA *CedaBLTData::GetBLTByName(const CString& ropName)
{
	BLTDATA  *prlBLT;
	if (omNameMap.Lookup(ropName,(void *& )prlBLT) == TRUE)
	{
		return prlBLT;
	}
	return NULL;
}
//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaBLTData::ReadSpecialData(CCSPtrArray<BLTDATA> *popBlt,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","BLT",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","BLT",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popBlt != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			BLTDATA *prpBlt = new BLTDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpBlt,CString(pclFieldList))) == true)
			{
				popBlt->Add(prpBlt);
			}
			else
			{
				delete prpBlt;
			}
		}
		if(popBlt->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaBLTData::SaveBLT(BLTDATA *prpBLT)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpBLT->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}

	switch(prpBLT->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpBLT);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpBLT->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpBLT->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpBLT);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpBLT->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpBLT->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}

    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessBLTCf(void * popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	switch(ipDDXType)
	{
	case BC_BLT_CHANGE :
	case BC_BLT_DELETE :
		((CedaBLTData *)popInstance)->ProcessBLTBc(ipDDXType,vpDataPointer,ropInstanceName);
		break;
	}
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaBLTData::ProcessBLTBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlBLTData = (struct BcStruct *) vpDataPointer;
	long llUrno = GetUrnoFromSelection(prlBLTData->Selection);

	BLTDATA *prlBLT = GetBLTByUrno(llUrno);
	if(ipDDXType == BC_BLT_CHANGE)
	{
		if (prlBLT != NULL)
		{
			GetRecordFromItemList(prlBLT,prlBLTData->Fields,prlBLTData->Data);
			if(ValidateBLTBcData(prlBLT->Urno)) //Prf: 8795
			{
				UpdateBLTInternal(prlBLT);
			}
			else
			{
				DeleteBLTInternal(prlBLT);
			}
		}
		else
		{
			prlBLT = new BLTDATA;
			GetRecordFromItemList(prlBLT,prlBLTData->Fields,prlBLTData->Data);
			if(ValidateBLTBcData(prlBLT->Urno)) //Prf: 8795
			{
				InsertBLTInternal(prlBLT);
			}
			else
			{
				delete prlBLT;
			}
		}
	}
	if(ipDDXType == BC_BLT_DELETE)
	{
		if (prlBLT != NULL)
		{
			DeleteBLTInternal(prlBLT);
		}
	}
}
//Prf: 8795
//--ValidateBLTBcData--------------------------------------------------------------------------------------

bool CedaBLTData::ValidateBLTBcData(const long& lrpUrno)
{
	bool blValidateBLTBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<BLTDATA> olBlts;
		if(!ReadSpecialData(&olBlts,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateBLTBcData = false;
		}
	}
	return blValidateBLTBcData;
}

//---------------------------------------------------------------------------------------------------------
int	 CedaBLTData::GetBLTList(CStringArray& ropList)
{
	for (int i = 0; i < omData.GetSize(); i++)
	{
		ropList.Add(omData[i].Bnam);
	}

	return ropList.GetSize();
}

//---------------------------------------------------------------------------------------------------------
bool CedaBLTData::MakeCedaData(CCSPtrArray<CEDARECINFO> *pomRecInfo,CString &pcpListOfData, void *pvpDataStruct)
{
    pcpListOfData = "";

	CString olFieldData;

    // Start conversion, field-by-field
    int nfields = pomRecInfo->GetSize();
    for (int fieldno = 0; fieldno < nfields; fieldno++)
    {

        CString s;
        char buf[33];   // ltoa() need buffer 33 bytes
        int type = (*pomRecInfo)[fieldno].Type;
        void *p = (void *)((char *)pvpDataStruct + (*pomRecInfo)[fieldno].Offset);
        int length = (*pomRecInfo)[fieldno].Length;

		if (strstr(this->pcmFieldList,(*pomRecInfo)[fieldno].Name) == NULL)
			continue;

        switch (type) {
        case CEDACHAR:
            s = (length == 0)? CString(" "): CString((char *)p).Left(length);
			MakeCedaString(s);
            olFieldData = s + ",";
            break;
        case CEDAINT:
            olFieldData = CString(itoa(*(int *)p, buf, 10)) + ",";
            break;
        case CEDALONG:
            olFieldData = CString(ltoa(*(long *)p, buf, 10)) + ",";
            break;
        case CEDADATE:  // format: YYYYMMDDHHMMSS
			olFieldData = ((CTime *)p)->Format("%Y%m%d%H%M%S") + ",";
            break;
        case CEDAOLEDATE:  // format: YYYYMMDDHHMMSS
			if(((COleDateTime *)p)->GetStatus() == COleDateTime::valid)
			{
				olFieldData = ((COleDateTime *)p)->Format("%Y%m%d%H%M%S") + ",";
			}
			else
			{
				olFieldData = " ,"; 
			}
            break;
		case CEDADOUBLE:
			char pclBuf[128];
//MWO 18.12.98
			sprintf(pclBuf,"%lf",(double *)p);
//			sprintf(pclBuf,"%.2f",(double *)p);
            olFieldData = CString(pclBuf) + ",";
			break;
        default:    // invalid field type
            return false;
        }
		pcpListOfData += olFieldData;
    }

	if(pcpListOfData.GetLength() > 0)
		pcpListOfData = pcpListOfData.Left(pcpListOfData.GetLength()-1);

	//pcpListOfData.SetAt(pcpListOfData.GetLength()-1,' ');
	//pcpListOfData.TrimRight( );

    return true;
}
