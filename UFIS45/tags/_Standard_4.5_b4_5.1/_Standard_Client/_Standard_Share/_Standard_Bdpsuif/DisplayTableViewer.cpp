// DisplayTableViewer.cpp 
//

#include <stdafx.h>
#include <DisplayTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>
#include <fstream.h>
#include <iomanip.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void DisplayTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// DisplayTableViewer
//

DisplayTableViewer::DisplayTableViewer(CCSPtrArray<DSPDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomDisplayTable = NULL;
    ogDdx.Register(this, DSP_CHANGE, CString("DISPLAYTABLEVIEWER"), CString("Display Update/new"), DisplayTableCf);
    ogDdx.Register(this, DSP_DELETE, CString("DISPLAYTABLEVIEWER"), CString("Display Delete"), DisplayTableCf);
}

//-----------------------------------------------------------------------------------------------

DisplayTableViewer::~DisplayTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void DisplayTableViewer::Attach(CCSTable *popTable)
{
    pomDisplayTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void DisplayTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void DisplayTableViewer::MakeLines()
{
	int ilDisplayCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilDisplayCount; ilLc++)
	{
		DSPDATA *prlDisplayData = &pomData->GetAt(ilLc);
		MakeLine(prlDisplayData);
	}
}

//-----------------------------------------------------------------------------------------------

void DisplayTableViewer::MakeLine(DSPDATA *prpDisplay)
{

    // Update viewer data for this shift record
    DISPLAYTABLE_LINEDATA rlDisplay;

	rlDisplay.Urno = prpDisplay->Urno; 
	rlDisplay.Dpid = prpDisplay->Dpid; 
	rlDisplay.Dnop = prpDisplay->Dnop; 
	rlDisplay.Dfpn = prpDisplay->Dfpn; 
	rlDisplay.Demt = prpDisplay->Demt; 
	rlDisplay.Dapn = prpDisplay->Dapn;
	rlDisplay.Dicf = prpDisplay->Dicf;

	rlDisplay.Pi[0] = prpDisplay->Pi01; 
	rlDisplay.Pn[0] = prpDisplay->Pn01; 
	rlDisplay.Pi[1] = prpDisplay->Pi02; 
	rlDisplay.Pn[1] = prpDisplay->Pn02; 
	rlDisplay.Pi[2] = prpDisplay->Pi03; 
	rlDisplay.Pn[2] = prpDisplay->Pn03; 
	rlDisplay.Pi[3] = prpDisplay->Pi04; 
	rlDisplay.Pn[3] = prpDisplay->Pn04; 
	rlDisplay.Pi[4] = prpDisplay->Pi05; 
	rlDisplay.Pn[4] = prpDisplay->Pn05; 
	rlDisplay.Pi[5] = prpDisplay->Pi06; 
	rlDisplay.Pn[5] = prpDisplay->Pn06; 
	rlDisplay.Pi[6] = prpDisplay->Pi07; 
	rlDisplay.Pn[6] = prpDisplay->Pn07; 
	rlDisplay.Pi[7] = prpDisplay->Pi08; 
	rlDisplay.Pn[7] = prpDisplay->Pn08; 
	rlDisplay.Pi[8] = prpDisplay->Pi09; 
	rlDisplay.Pn[8] = prpDisplay->Pn09; 
	rlDisplay.Pi[9] = prpDisplay->Pi10; 
	rlDisplay.Pn[9] = prpDisplay->Pn10; 
	rlDisplay.Pi[10]= prpDisplay->Pi11; 
	rlDisplay.Pn[10]= prpDisplay->Pn11; 
	rlDisplay.Pi[11]= prpDisplay->Pi12; 
	rlDisplay.Pn[11]= prpDisplay->Pn12; 
	rlDisplay.Pi[12]= prpDisplay->Pi13; 
	rlDisplay.Pn[12]= prpDisplay->Pn13; 
	rlDisplay.Pi[13]= prpDisplay->Pi14; 
	rlDisplay.Pn[13]= prpDisplay->Pn14; 
	rlDisplay.Pi[14]= prpDisplay->Pi15; 
	rlDisplay.Pn[14]= prpDisplay->Pn15; 
	rlDisplay.Pi[15]= prpDisplay->Pi16; 
	rlDisplay.Pn[15]= prpDisplay->Pn16; 
	rlDisplay.Pi[16]= prpDisplay->Pi17; 
	rlDisplay.Pn[16]= prpDisplay->Pn17; 
	rlDisplay.Pi[17]= prpDisplay->Pi18; 
	rlDisplay.Pn[17]= prpDisplay->Pn18; 
	rlDisplay.Pi[18]= prpDisplay->Pi19; 
	rlDisplay.Pn[18]= prpDisplay->Pn19; 
	rlDisplay.Pi[19]= prpDisplay->Pi20; 
	rlDisplay.Pn[19]= prpDisplay->Pn20; 
	rlDisplay.Pi[20]= prpDisplay->Pi21; 
	rlDisplay.Pn[20]= prpDisplay->Pn21; 
	rlDisplay.Pi[21]= prpDisplay->Pi22; 
	rlDisplay.Pn[21]= prpDisplay->Pn22; 
	rlDisplay.Pi[22]= prpDisplay->Pi23; 
	rlDisplay.Pn[22]= prpDisplay->Pn23; 
	rlDisplay.Pi[23]= prpDisplay->Pi24; 
	rlDisplay.Pn[23]= prpDisplay->Pn24; 
	rlDisplay.Pi[24]= prpDisplay->Pi25; 
	rlDisplay.Pn[24]= prpDisplay->Pn25; 
	rlDisplay.Pi[25]= prpDisplay->Pi26; 
	rlDisplay.Pn[25]= prpDisplay->Pn26; 
	rlDisplay.Pi[26]= prpDisplay->Pi27; 
	rlDisplay.Pn[26]= prpDisplay->Pn27; 
	rlDisplay.Pi[27]= prpDisplay->Pi28; 
	rlDisplay.Pn[27]= prpDisplay->Pn28; 
	rlDisplay.Pi[28]= prpDisplay->Pi29; 
	rlDisplay.Pn[28]= prpDisplay->Pn29; 
	rlDisplay.Pi[29]= prpDisplay->Pi30; 
	rlDisplay.Pn[29]= prpDisplay->Pn30; 
	rlDisplay.Pi[30]= prpDisplay->Pi31; 
	rlDisplay.Pn[30]= prpDisplay->Pn31; 
	rlDisplay.Pi[31]= prpDisplay->Pi32; 
	rlDisplay.Pn[31]= prpDisplay->Pn32; 
	rlDisplay.Pi[32]= prpDisplay->Pi33; 
	rlDisplay.Pn[32]= prpDisplay->Pn33; 
	rlDisplay.Pi[33]= prpDisplay->Pi34; 
	rlDisplay.Pn[33]= prpDisplay->Pn34; 
	rlDisplay.Pi[34]= prpDisplay->Pi35; 
	rlDisplay.Pn[34]= prpDisplay->Pn35; 
	rlDisplay.Pi[35]= prpDisplay->Pi36; 
	rlDisplay.Pn[35]= prpDisplay->Pn36; 
	rlDisplay.Pi[36]= prpDisplay->Pi37; 
	rlDisplay.Pn[36]= prpDisplay->Pn37; 
	rlDisplay.Pi[37]= prpDisplay->Pi38; 
	rlDisplay.Pn[37]= prpDisplay->Pn38; 
	rlDisplay.Pi[38]= prpDisplay->Pi39; 
	rlDisplay.Pn[38]= prpDisplay->Pn39; 
	rlDisplay.Pi[39]= prpDisplay->Pi40; 
	rlDisplay.Pn[39]= prpDisplay->Pn40; 
	rlDisplay.Pi[40]= prpDisplay->Pi41; 
	rlDisplay.Pn[40]= prpDisplay->Pn41; 
	rlDisplay.Pi[41]= prpDisplay->Pi42; 
	rlDisplay.Pn[41]= prpDisplay->Pn42; 
	rlDisplay.Pi[42]= prpDisplay->Pi43; 
	rlDisplay.Pn[42]= prpDisplay->Pn43; 
	rlDisplay.Pi[43]= prpDisplay->Pi44; 
	rlDisplay.Pn[43]= prpDisplay->Pn44; 
	rlDisplay.Pi[44]= prpDisplay->Pi45; 
	rlDisplay.Pn[44]= prpDisplay->Pn45; 
	rlDisplay.Pi[45]= prpDisplay->Pi46; 
	rlDisplay.Pn[45]= prpDisplay->Pn46; 
	rlDisplay.Pi[46]= prpDisplay->Pi47; 
	rlDisplay.Pn[46]= prpDisplay->Pn47; 
	rlDisplay.Pi[47]= prpDisplay->Pi48; 
	rlDisplay.Pn[47]= prpDisplay->Pn48; 
	rlDisplay.Pi[48]= prpDisplay->Pi49; 
	rlDisplay.Pn[48]= prpDisplay->Pn49; 
	rlDisplay.Pi[49]= prpDisplay->Pi50; 
	rlDisplay.Pn[49]= prpDisplay->Pn50; 

	rlDisplay.Vafr = prpDisplay->Vafr.Format("%d.%m.%Y %H:%M"); 
	rlDisplay.Vato = prpDisplay->Vato.Format("%d.%m.%Y %H:%M"); 
	
	if(ogDSPData.DoesRemarkCodeExist() == true)
		rlDisplay.Rema = prpDisplay->Rema;
	
	CreateLine(&rlDisplay);
}

//-----------------------------------------------------------------------------------------------

void DisplayTableViewer::CreateLine(DISPLAYTABLE_LINEDATA *prpDisplay)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareDisplay(prpDisplay, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	DISPLAYTABLE_LINEDATA rlDisplay;
	rlDisplay = *prpDisplay;
    omLines.NewAt(ilLineno, rlDisplay);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void DisplayTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 57;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomDisplayTable->SetShowSelection(TRUE);
	pomDisplayTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	CString olHeader(LoadStg(IDS_STRING933));
	for (int i = 1; i <= 50; i++)
	{
		CString olFormat;
		olFormat.Format("|Name %d|No %d",i,i);
		olHeader += olFormat;
	}

	CString olFormat;
	olFormat.Format("|From|To");
	olHeader += olFormat;

	int ilPos = 1;
	rlHeader.Length = 130; 
	rlHeader.Text = GetListItem(olHeader,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// display name
	rlHeader.Length = 20; 
	rlHeader.Text = GetListItem(olHeader,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// no of pages
	rlHeader.Length = 20; 
	rlHeader.Text = GetListItem(olHeader,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// first page number
	rlHeader.Length = 20; 
	rlHeader.Text = GetListItem(olHeader,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// default page number
	rlHeader.Length = 20; 
	rlHeader.Text = GetListItem(olHeader,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// alarm page number
	rlHeader.Length = 130; 
	rlHeader.Text = GetListItem(olHeader,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// initial config file

	for (i = 0; i < 50; i++)
	{
		rlHeader.Length = 100; 
		rlHeader.Text = GetListItem(olHeader,ilPos++,false,'|');
		omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// page id
		rlHeader.Length = 30; 
		rlHeader.Text = GetListItem(olHeader,ilPos++,false,'|');
		omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// page no
	}

	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(olHeader,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// valid from
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(olHeader,ilPos++,false,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// valid to
	
	if(ogDSPData.DoesRemarkCodeExist() == true)
	{
		olHeader += "|Remarks";
		rlHeader.Length = 300; 
		rlHeader.Text = GetListItem(olHeader,ilPos++,false,'|');
		omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// Remark
	}	
	pomDisplayTable->SetHeaderFields(omHeaderDataArray);

	pomDisplayTable->SetDefaultSeparator();
	pomDisplayTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Dpid;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Dnop;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Dfpn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Demt;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Dapn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Dicf;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		for (int i = 0; i < 50; i++)
		{
			rlColumnData.Text = omLines[ilLineNo].Pi[i];
			olColList.NewAt(olColList.GetSize(), rlColumnData);
			rlColumnData.Text = omLines[ilLineNo].Pn[i];
			olColList.NewAt(olColList.GetSize(), rlColumnData);
		}
		
		rlColumnData.Text = omLines[ilLineNo].Vafr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vato;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		if(ogDSPData.DoesRemarkCodeExist() == true)
		{
			rlColumnData.Text = omLines[ilLineNo].Rema;
			olColList.NewAt(olColList.GetSize(), rlColumnData);
		}

		pomDisplayTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomDisplayTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString DisplayTableViewer::Format(DISPLAYTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool DisplayTableViewer::FindDisplay(char *pcpDisplay, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpDisplayKeya) &&
			 (omLines[ilItem].Keyd == pcpDisplayKeyd)    )
			return TRUE;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void DisplayTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    DisplayTableViewer *polViewer = (DisplayTableViewer *)popInstance;
    if (ipDDXType == DSP_CHANGE) 
		polViewer->ProcessDisplayChange((DSPDATA *)vpDataPointer);
    else if (ipDDXType == DSP_DELETE) 
		polViewer->ProcessDisplayDelete((DSPDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void DisplayTableViewer::ProcessDisplayChange(DSPDATA *prpDisplay)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpDisplay->Dpid;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Dnop;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Dfpn;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Demt;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Dapn;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Dicf;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

//	----------------------------------------------------
	rlColumn.Text = prpDisplay->Pi01;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn01;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi02;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn02;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi03;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn03;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi04;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn04;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi05;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn05;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi06;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn06;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi07;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn07;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi08;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn08;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi09;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn09;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi10;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn10;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

//	----------------------------------------------------
	rlColumn.Text = prpDisplay->Pi11;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn11;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi12;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn12;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi13;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn13;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi14;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn14;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi15;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn15;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi16;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn16;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi17;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn17;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi18;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn18;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi19;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn19;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi20;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn20;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
//	----------------------------------------------------
	rlColumn.Text = prpDisplay->Pi21;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn21;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi22;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn22;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi23;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn23;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi24;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn24;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi25;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn25;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi26;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn26;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi27;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn27;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi28;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn28;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi29;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn29;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi30;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn30;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
//	----------------------------------------------------
	rlColumn.Text = prpDisplay->Pi31;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn31;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi32;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn32;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi33;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn33;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi34;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn34;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi35;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn35;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi36;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn36;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi37;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn37;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi38;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn38;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi39;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn39;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi40;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn40;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
//	----------------------------------------------------
	rlColumn.Text = prpDisplay->Pi41;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn41;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi42;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn42;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi43;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn43;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi44;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn44;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi45;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn45;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi46;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn46;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi47;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn47;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi48;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn48;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi49;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn49;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Pi50;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Pn50;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpDisplay->Vafr.Format("%d.%m.%Y %H:%M"); 
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpDisplay->Vato.Format("%d.%m.%Y %H:%M"); 
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	if(ogDSPData.DoesRemarkCodeExist() == true)
	{
		rlColumn.Text = prpDisplay->Rema; 
		rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
	}

	if (FindLine(prpDisplay->Urno, ilItem))
	{
        DISPLAYTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpDisplay->Urno; 
		prlLine->Dpid = prpDisplay->Dpid; 
		prlLine->Dnop = prpDisplay->Dnop; 
		prlLine->Dfpn = prpDisplay->Dfpn; 
		prlLine->Demt = prpDisplay->Demt; 
		prlLine->Dapn = prpDisplay->Dapn; 
		prlLine->Dicf = prpDisplay->Dicf; 

		prlLine->Pi[0] = prpDisplay->Pi01; 
		prlLine->Pn[0] = prpDisplay->Pn01; 
		prlLine->Pi[1] = prpDisplay->Pi02; 
		prlLine->Pn[1] = prpDisplay->Pn02; 
		prlLine->Pi[2] = prpDisplay->Pi03; 
		prlLine->Pn[2] = prpDisplay->Pn03; 
		prlLine->Pi[3] = prpDisplay->Pi04; 
		prlLine->Pn[3] = prpDisplay->Pn04; 
		prlLine->Pi[4] = prpDisplay->Pi05; 
		prlLine->Pn[4] = prpDisplay->Pn05; 
		prlLine->Pi[5] = prpDisplay->Pi06; 
		prlLine->Pn[5] = prpDisplay->Pn06; 
		prlLine->Pi[6] = prpDisplay->Pi07; 
		prlLine->Pn[6] = prpDisplay->Pn07; 
		prlLine->Pi[7] = prpDisplay->Pi08; 
		prlLine->Pn[7] = prpDisplay->Pn08; 
		prlLine->Pi[8] = prpDisplay->Pi09; 
		prlLine->Pn[8] = prpDisplay->Pn09; 
		prlLine->Pi[9] = prpDisplay->Pi10; 
		prlLine->Pn[9] = prpDisplay->Pn10; 
		prlLine->Pi[10]= prpDisplay->Pi11; 
		prlLine->Pn[10]= prpDisplay->Pn11; 
		prlLine->Pi[11]= prpDisplay->Pi12; 
		prlLine->Pn[11]= prpDisplay->Pn12; 
		prlLine->Pi[12]= prpDisplay->Pi13; 
		prlLine->Pn[12]= prpDisplay->Pn13; 
		prlLine->Pi[13]= prpDisplay->Pi14; 
		prlLine->Pn[13]= prpDisplay->Pn14; 
		prlLine->Pi[14]= prpDisplay->Pi15; 
		prlLine->Pn[14]= prpDisplay->Pn15; 
		prlLine->Pi[15]= prpDisplay->Pi16; 
		prlLine->Pn[15]= prpDisplay->Pn16; 
		prlLine->Pi[16]= prpDisplay->Pi17; 
		prlLine->Pn[16]= prpDisplay->Pn17; 
		prlLine->Pi[17]= prpDisplay->Pi18; 
		prlLine->Pn[17]= prpDisplay->Pn18; 
		prlLine->Pi[18]= prpDisplay->Pi19; 
		prlLine->Pn[18]= prpDisplay->Pn19; 
		prlLine->Pi[19]= prpDisplay->Pi20; 
		prlLine->Pn[19]= prpDisplay->Pn20; 
		prlLine->Pi[20]= prpDisplay->Pi21; 
		prlLine->Pn[20]= prpDisplay->Pn21; 
		prlLine->Pi[21]= prpDisplay->Pi22; 
		prlLine->Pn[21]= prpDisplay->Pn22; 
		prlLine->Pi[22]= prpDisplay->Pi23; 
		prlLine->Pn[22]= prpDisplay->Pn23; 
		prlLine->Pi[23]= prpDisplay->Pi24; 
		prlLine->Pn[23]= prpDisplay->Pn24; 
		prlLine->Pi[24]= prpDisplay->Pi25; 
		prlLine->Pn[24]= prpDisplay->Pn25; 
		prlLine->Pi[25]= prpDisplay->Pi26; 
		prlLine->Pn[25]= prpDisplay->Pn26; 
		prlLine->Pi[26]= prpDisplay->Pi27; 
		prlLine->Pn[26]= prpDisplay->Pn27; 
		prlLine->Pi[27]= prpDisplay->Pi28; 
		prlLine->Pn[27]= prpDisplay->Pn28; 
		prlLine->Pi[28]= prpDisplay->Pi29; 
		prlLine->Pn[28]= prpDisplay->Pn29; 
		prlLine->Pi[29]= prpDisplay->Pi30; 
		prlLine->Pn[29]= prpDisplay->Pn30; 
		prlLine->Pi[30]= prpDisplay->Pi31; 
		prlLine->Pn[30]= prpDisplay->Pn31; 
		prlLine->Pi[31]= prpDisplay->Pi32; 
		prlLine->Pn[31]= prpDisplay->Pn32; 
		prlLine->Pi[32]= prpDisplay->Pi33; 
		prlLine->Pn[32]= prpDisplay->Pn33; 
		prlLine->Pi[33]= prpDisplay->Pi34; 
		prlLine->Pn[33]= prpDisplay->Pn34; 
		prlLine->Pi[34]= prpDisplay->Pi35; 
		prlLine->Pn[34]= prpDisplay->Pn35; 
		prlLine->Pi[35]= prpDisplay->Pi36; 
		prlLine->Pn[35]= prpDisplay->Pn36; 
		prlLine->Pi[36]= prpDisplay->Pi37; 
		prlLine->Pn[36]= prpDisplay->Pn37; 
		prlLine->Pi[37]= prpDisplay->Pi38; 
		prlLine->Pn[37]= prpDisplay->Pn38; 
		prlLine->Pi[38]= prpDisplay->Pi39; 
		prlLine->Pn[38]= prpDisplay->Pn39; 
		prlLine->Pi[39]= prpDisplay->Pi40; 
		prlLine->Pn[39]= prpDisplay->Pn40; 
		prlLine->Pi[40]= prpDisplay->Pi41; 
		prlLine->Pn[40]= prpDisplay->Pn41; 
		prlLine->Pi[41]= prpDisplay->Pi42; 
		prlLine->Pn[41]= prpDisplay->Pn42; 
		prlLine->Pi[42]= prpDisplay->Pi43; 
		prlLine->Pn[42]= prpDisplay->Pn43; 
		prlLine->Pi[43]= prpDisplay->Pi44; 
		prlLine->Pn[43]= prpDisplay->Pn44; 
		prlLine->Pi[44]= prpDisplay->Pi45; 
		prlLine->Pn[44]= prpDisplay->Pn45; 
		prlLine->Pi[45]= prpDisplay->Pi46; 
		prlLine->Pn[45]= prpDisplay->Pn46; 
		prlLine->Pi[46]= prpDisplay->Pi47; 
		prlLine->Pn[46]= prpDisplay->Pn47; 
		prlLine->Pi[47]= prpDisplay->Pi48; 
		prlLine->Pn[47]= prpDisplay->Pn48; 
		prlLine->Pi[48]= prpDisplay->Pi49; 
		prlLine->Pn[48]= prpDisplay->Pn49; 
		prlLine->Pi[49]= prpDisplay->Pi50; 
		prlLine->Pn[49]= prpDisplay->Pn50; 

		prlLine->Vafr = prpDisplay->Vafr.Format("%d.%m.%Y %H:%M"); 
		prlLine->Vato = prpDisplay->Vato.Format("%d.%m.%Y %H:%M"); 

		if(ogDSPData.DoesRemarkCodeExist() == true)
		{
			prlLine->Rema = prpDisplay->Rema;
		}

		pomDisplayTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomDisplayTable->DisplayTable();
	}
	else
	{
		MakeLine(prpDisplay);
		if (FindLine(prpDisplay->Urno, ilItem))
		{
	        DISPLAYTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomDisplayTable->AddTextLine(olLine, (void *)prlLine);
				pomDisplayTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void DisplayTableViewer::ProcessDisplayDelete(DSPDATA *prpDisplay)
{
	int ilItem;
	if (FindLine(prpDisplay->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomDisplayTable->DeleteTextLine(ilItem);
		pomDisplayTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool DisplayTableViewer::IsPassFilter(DSPDATA *prpDisplay)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int DisplayTableViewer::CompareDisplay(DISPLAYTABLE_LINEDATA *prpDisplay1, DISPLAYTABLE_LINEDATA *prpDisplay2)
{
	return 0;
}

//-----------------------------------------------------------------------------------------------

void DisplayTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool DisplayTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void DisplayTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void DisplayTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING930);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	

//			calculate count of pages
			CUIntArray olColsPerPage;
			int ilPages = pomPrint->CalculateCountOfPages(this->omHeaderDataArray,olColsPerPage);

			pomPrint->omCdc.StartDoc( &rlDocInfo );
			for (int ilPage = 0; ilPage < ilPages; ilPage++)
			{
				pomPrint->imPageNo = 0;
				int ilLines = omLines.GetSize();
				pomPrint->imLineNo = pomPrint->imMaxLines + 1;
				olFooter1.Format("%d %s",ilLines,omTableName);
				for(int i = 0; i < ilLines; i++ ) 
				{
					if(pomPrint->imLineNo >= pomPrint->imMaxLines)
					{
						if(pomPrint->imPageNo > 0)
						{
							olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
							pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
							pomPrint->omCdc.EndPage();
						}
						PrintTableHeader(ilPage,olColsPerPage);
					}
					if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
					{
						PrintTableLine(ilPage,olColsPerPage,&omLines[i],true);
					}
					else
					{
						PrintTableLine(ilPage,olColsPerPage,&omLines[i],false);
					}
				}
				olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
				pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
				pomPrint->omCdc.EndPage();
			}
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool DisplayTableViewer::PrintTableHeader(int ilPage,CUIntArray& ropColsPerPage)
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		bool blColumn = false;
		if (ilPage == 0)
		{
			if (i < ropColsPerPage[ilPage])
			{
				blColumn = true;
			}
		}
		else
		{
			if (i >= ropColsPerPage[ilPage-1] && i < ropColsPerPage[ilPage])
			{
				blColumn = true;
			}
		}

		if (blColumn)
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool DisplayTableViewer::PrintTableLine(int ilPage,CUIntArray& ropColsPerPage,DISPLAYTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		bool blColumn = false;
		if (ilPage == 0)
		{
			if (i < ropColsPerPage[ilPage])
			{
				blColumn = true;
			}
		}
		else
		{
			if (i >= ropColsPerPage[ilPage-1] && i < ropColsPerPage[ilPage])
			{
				blColumn = true;
			}
		}

		if(blColumn)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
			case 0:
				{
					rlElement.FrameLeft  = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Dpid;
				}
				break;
			case 1:
				{
					rlElement.Text		= prpLine->Dnop;
				}
				break;
			case 2:
				{
					rlElement.Text		= prpLine->Dfpn;
				}
				break;
			case 3:
				{
					rlElement.Text		= prpLine->Demt;
				}
				break;
			case 4:
				{
					rlElement.Text		= prpLine->Dapn;
				}
			case 5:
				{
					rlElement.Text		= prpLine->Dicf;
				}
				break;
			case 106:
				{
					rlElement.Text		= prpLine->Vafr;
				}
				break;
			case 107:
				{
					if(ogDSPData.DoesRemarkCodeExist() == false)
					{
					rlElement.FrameRight = PRINT_FRAMETHIN;
					}
					rlElement.Text		= prpLine->Vato;
				}
				break;
			case 108:
				{
					if(ogDSPData.DoesRemarkCodeExist() == true)
					{
						rlElement.FrameRight = PRINT_FRAMETHIN;
						rlElement.Text = prpLine->Rema;
					}
				}
				break;
			default :
				{
					if (i % 2)	// odd !
					{
						rlElement.Text = prpLine->Pn[(i - 6) / 2];
					}
					else
					{
						rlElement.Text = prpLine->Pi[(i - 6) / 2];
					}
				}
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
bool DisplayTableViewer::CreateExcelFile(const CString& opTrenner,const CString& ropListSeparator)
{
	ofstream of;

	CString olTmpPath = CCSLog::GetTmpPath();

	CString olTableName;
	olTableName.Format("%s -   %s", "DSPTAB", (CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")) );


	CString olFileName = olTableName;
	olFileName.Remove('*');
	olFileName.Remove('.');
	olFileName.Remove(':');
	olFileName.Remove('/');
	olFileName.Replace(" ", "_");
	
	char pHeader[256];
	strcpy (pHeader, olTmpPath);
	CString path = pHeader;
	omFileName =  path + "\\" + olFileName + ".csv";

	of.open( omFileName, ios::out);


	of  << setw(0) << "DSPTAB" << "     "  << CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")  << endl;

	of	<< setw(0) << LoadStg(IDS_STRING940) //CString("Display name");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING956) //CString("No of pages");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING957) //CString("First page no");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING958) //CString("Default page no");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING959) //CString("Alarm page no");
		<< setw(0) << opTrenner
		<< setw(0) << LoadStg(IDS_STRING1019) //CString("Initial Config file");
		;

	CString olPageName;
	CString olPageNumber;
	for (int i = 1; i <= 50; i++)
	{
		olPageName.Format("%s %02d",LoadStg(IDS_STRING964),i);
		olPageNumber.Format("%s %02d",LoadStg(IDS_STRING980),i);
		of	<< setw(0) << opTrenner
			<< setw(0) << olPageName //CString("Page name");
			<< setw(0) << opTrenner
			<< setw(0) << olPageNumber //CString("Page no");
			;
	}

	of	<< endl;


	int ilCount = omLines.GetSize();
	COleDateTime olCurrTime = COleDateTime::GetCurrentTime();

	for(i = 0; i < ilCount; i++)
	{
		DISPLAYTABLE_LINEDATA rlLine = omLines[i];
		of.setf(ios::left, ios::adjustfield);

		of   << setw(0) << rlLine.Dpid							// Name
		     << setw(0) << opTrenner 
		     << setw(0) << rlLine.Dnop							// Vorname
		     << setw(0) << opTrenner 
		     << setw(0) << rlLine.Dfpn							// Personal Nummer
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Demt							// Organisationseinheit
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Dapn							// Funktionen
		     << setw(0) << opTrenner 
			 << setw(0) << rlLine.Dicf							// Funktionen
			 ;

		for (int i = 0; i < 50; i++)
		{
			of  << setw(0) << opTrenner 
				<< setw(0) << rlLine.Pi[i]						// Qualifikationen
				<< setw(0) << opTrenner 
				<< setw(0) << rlLine.Pn[i]						// Abk�rzung
			;
		}

		if(ogDSPData.DoesRemarkCodeExist() == true)
		{
			of << setw(0) << opTrenner
			   << setw(0) << rlLine.Rema;
		}

	    of	<< endl; 
		
	}

	of.close();
	return true;
}

//-----------------------------------------------------------------------------------------------
