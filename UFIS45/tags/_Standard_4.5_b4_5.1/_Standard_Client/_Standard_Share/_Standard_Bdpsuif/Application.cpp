// Application.cpp : implementation file
//

#include "stdafx.h"
#include "bdpsuif.h"
#include "Application.h"
#include <BasicData.h>
#include <Stammdaten.h>
#include <AatLogin.h>
#include <CCSBasicFunc.h>
#include <CedaInitModuData.h>
#include <PrivList.h>
#include <CedaBasicData.h>
#include <VersionInfo.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CStringArray  ogCmdLineStghArray;

/////////////////////////////////////////////////////////////////////////////
// CApplication

IMPLEMENT_DYNCREATE(CApplication, CCmdTarget)

CApplication::CApplication()
{
	EnableAutomation();
	
	// To keep the application running as long as an OLE automation 
	//	object is active, the constructor calls AfxOleLockApp.
	
	AfxOleLockApp();

	this->SetCmdLine("");	// the default initialization
	ogCmdLineStghArray.Add("Resource:BSD:0");
	
}

CApplication::~CApplication()
{
	// To terminate the application when all objects created with
	// 	with OLE automation, the destructor calls AfxOleUnlockApp.
	
	AfxOleUnlockApp();
}


void CApplication::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CCmdTarget::OnFinalRelease();
}


BEGIN_MESSAGE_MAP(CApplication, CCmdTarget)
	//{{AFX_MSG_MAP(CApplication)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CApplication, CCmdTarget)
	//{{AFX_DISPATCH_MAP(CApplication)
	DISP_PROPERTY_EX(CApplication, "Username", GetUsername, SetUsername, VT_BSTR)
	DISP_PROPERTY_EX(CApplication, "Password", GetPassword, SetPassword, VT_BSTR)
	DISP_PROPERTY_EX(CApplication, "ResourceTable", GetResourceTable, SetResourceTable, VT_BSTR)
	DISP_PROPERTY_EX(CApplication, "ResourceUrno", GetResourceUrno, SetResourceUrno, VT_I4)
	DISP_PROPERTY_EX(CApplication, "Caller", GetCaller, SetCaller, VT_BSTR)
	DISP_FUNCTION(CApplication, "SetCmdLine", SetCmdLine, VT_BOOL, VTS_BSTR)
	DISP_FUNCTION(CApplication, "DisplayResource", DisplayResource, VT_BOOL, VTS_NONE)
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IApplication to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {46979EBF-BE72-4FCA-9DB3-147027585565}
static const IID IID_IApplication =
{ 0x46979ebf, 0xbe72, 0x4fca, { 0x9d, 0xb3, 0x14, 0x70, 0x27, 0x58, 0x55, 0x65 } };

BEGIN_INTERFACE_MAP(CApplication, CCmdTarget)
	INTERFACE_PART(CApplication, IID_IApplication, Dispatch)
END_INTERFACE_MAP()

// {5B4896FA-255D-4728-B237-D5366F6F2791}
IMPLEMENT_OLECREATE(CApplication, "BDPSUIF.Application", 0x5b4896fa, 0x255d, 0x4728, 0xb2, 0x37, 0xd5, 0x36, 0x6f, 0x6f, 0x27, 0x91)

/////////////////////////////////////////////////////////////////////////////
// CApplication message handlers

BSTR CApplication::GetUsername() 
{
	CString strResult;
	// TODO: Add your property handler here
	strResult = 	ogCmdLineStghArray.GetAt(1);		
	return strResult.AllocSysString();
}

void CApplication::SetUsername(LPCTSTR lpszNewValue) 
{
	// TODO: Add your property handler here
	ogCmdLineStghArray.SetAt(1,lpszNewValue);
}

BSTR CApplication::GetPassword() 
{
	CString strResult;
	// TODO: Add your property handler here
	strResult = 	ogCmdLineStghArray.GetAt(2);		
	return strResult.AllocSysString();
}

void CApplication::SetPassword(LPCTSTR lpszNewValue) 
{
	// TODO: Add your property handler here
	ogCmdLineStghArray.SetAt(2,lpszNewValue);

}

BOOL CApplication::SetCmdLine(LPCTSTR pszCmdLine) 
{
	// TODO: Add your dispatch handler code here

	//Parameter�bergabe///////////////////////////////////////////////////
	ogCmdLineStghArray.RemoveAll();
	CString olCmdLine =  pszCmdLine;
	//m_lpCmdLine => "AppName,UserID,Password,Tablelist"
	if(olCmdLine.GetLength() == 0)
	{
		//dafault f�r BDPS-UIF
		ogCmdLineStghArray.Add(ogAppName);
		ogCmdLineStghArray.Add("");
		ogCmdLineStghArray.Add("");
		//Hier alle Tabellen eintragen 
		ogCmdLineStghArray.Add("ALT-ACT-ACR-APT-RWY-TWY-PST-GAT-CIC-BLT-EXT-DEN-MVT-NAT-HAG-WRO-HTY-STY-FID-STR-SPH-SEA-GHS/*-GEG*/-PER-PEF-GRM-GRN-ORG-PFC-COT-ASF-BSS-BSD-ODA-TEA-STF-HTC-WAY-PRC-CHT-TIP-BLK-HOL-WGP-SWG-PGP-AWI-CCC-VIP-AFM-ENT-SRC-PAR-POL-MFM-STS-NWH-COH-PMX-WIS-EQT-EQU-MAW-DEV-DSP-PAG-CRC-DSC-FLG-CHU-CNT");//Alle  //PRF 8378
	}
	else if (olCmdLine.Find("Resource:") > -1)	// called from FIPS
	{
		if(ExtractItemList(olCmdLine,&ogCmdLineStghArray) != 4)
		{
			MessageBox(NULL,LoadStg(IDS_STRING363),"BDPS-UIF",MB_ICONERROR);
			return FALSE;
		}
		else
		{
			CString olPara4 = ogCmdLineStghArray.GetAt(3);
			ogCmdLineStghArray.SetAt(3,"ALT-ACT-ACR-APT-RWY-TWY-PST-GAT-CIC-BLT-EXT-DEN-MVT-NAT-HAG-WRO-HTY-STY-FID-STR-SPH-SEA-GHS/*-GEG*/-PER-PEF-GRM-GRN-ORG-PFC-COT-ASF-BSS-BSD-ODA-TEA-STF-HTC-WAY-PRC-CHT-TIP-BLK-HOL-WGP-SWG-PGP-AWI-CCC-VIP-AFM-ENT-SRC-PAR-POL-MFM-STS-NWH-COH-PMX-WIS-EQT-EQU-MAW-DEV-DSP-PAG-CRC-DSC-FLG-CHU");	
			ogCmdLineStghArray.Add(olPara4);
		}
				
	}
	else										// called with old format		
	{
		if(ExtractItemList(olCmdLine,&ogCmdLineStghArray) != 4)
		{
			MessageBox(NULL,LoadStg(IDS_STRING363),"BDPS-UIF",MB_ICONERROR);
			return FALSE;
		}
		else
		{
			ogCmdLineStghArray.SetAt(3,"ALT-ACT-ACR-APT-RWY-TWY-PST-GAT-CIC-BLT-EXT-DEN-MVT-NAT-HAG-WRO-HTY-STY-FID-STR-SPH-SEA-GHS/*-GEG*/-PER-PEF-GRM-GRN-ORG-PFC-COT-ASF-BSS-BSD-ODA-TEA-STF-HTC-WAY-PRC-CHT-TIP-BLK-HOL-WGP-SWG-PGP-AWI-CCC-VIP-AFM-ENT-SRC-PAR-POL-MFM-STS-NWH-COH-PMX-WIS-EQT-EQU-MAW-DEV-DSP-PAG-CRC-DSC-FLG-CHU"); 
		}
	}
	//Parameter�bergabe Ende////////////////////////////////////////////////

	return TRUE;
}

BSTR CApplication::GetResourceTable() 
{
	CString strResult;

	// TODO: Add your property handler here
	if (ogCmdLineStghArray.GetSize() < 5)
		return strResult.AllocSysString();

	char pclTable[256];
	long llUrno;
	if (sscanf(ogCmdLineStghArray.GetAt(4),"Resource:%3s:%ld",&pclTable[0],&llUrno) != 2)
		return strResult.AllocSysString();
	else
	{
		strResult = pclTable;
		return strResult.AllocSysString();
	}
}

void CApplication::SetResourceTable(LPCTSTR lpszNewValue) 
{
	// TODO: Add your property handler here
	char pclTable[256];
	long llUrno;
	if (sscanf(ogCmdLineStghArray.GetAt(4),"Resource:%3s:%ld",&pclTable[0],&llUrno) != 2)
		return;
	else
	{
		CString olData;
		olData.Format("Resource:%3s:%ld",lpszNewValue,llUrno);
		ogCmdLineStghArray.SetAt(4,olData);
	}

}


long CApplication::GetResourceUrno() 
{
	// TODO: Add your property handler here
	if (ogCmdLineStghArray.GetSize() < 5)
		return -1;

	char pclTable[256];
	long llUrno;
	if (sscanf(ogCmdLineStghArray.GetAt(4),"Resource:%3s:%ld",&pclTable[0],&llUrno) != 2)
		return -1;
	else
	{
		return llUrno;
	}
}

void CApplication::SetResourceUrno(long nNewValue) 
{
	// TODO: Add your property handler here
	char pclTable[256];
	long llUrno;
	if (sscanf(ogCmdLineStghArray.GetAt(4),"Resource:%3s:%ld",&pclTable[0],&llUrno) != 2)
		return;
	else
	{
		CString olData;
		olData.Format("Resource:%3s:%ld",&pclTable[0],nNewValue);
		ogCmdLineStghArray.SetAt(4,olData);
	}

}

BOOL CApplication::DisplayResource() 
{
	// TODO: Add your dispatch handler code here
	if (!this->Initialization())
	{
		return FALSE;
	}

	CStammdaten olStammdatenDlg(&this->omDummyDlg);
	CWinApp *polApp = AfxGetApp();
	polApp->m_pMainWnd = &olStammdatenDlg;

	olStammdatenDlg.DoModal();

	this->omDummyDlg.DestroyWindow();

	return TRUE;
}

bool CApplication::Initialization()
{
	char pclViewEditFilter[64];
	char pclViewEditSort[64];
	char pclTmpString[64];

	static bool first = true;
	if (first)
	{
		if (ogCommHandler.Initialize() != true)  // connection error?
		{
			AfxMessageBox(CString("CedaCom:\n") + ogCommHandler.LastError());
			return FALSE;
		}

  		InitFont();
		CreateBrushes();


		// INIT Tablenames and Homeairport
		char pclConfigPath[256];

		if (getenv("CEDA") == NULL)
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		else
			strcpy(pclConfigPath, getenv("CEDA"));


		GetPrivateProfileString("GLOBAL", "HOMEAIRPORT", "ZRH", pcgHome, sizeof pcgHome, pclConfigPath);
		GetPrivateProfileString("GLOBAL", "TABLEEXTENSION", "TAB", pcgTableExt, sizeof pcgTableExt, pclConfigPath);
		GetPrivateProfileString("GLOBAL", "MULTI_AIRPORT", "FALSE",pclTmpString,sizeof pclTmpString, pclConfigPath );
		GetPrivateProfileString(pcgAppName, "UMODE", "E44SWP", pcgUMode, sizeof pcgUMode, pclConfigPath);
		GetPrivateProfileString(ogAppName, "EDIT_VIEW_FILTER", "FALSE", pclViewEditFilter, sizeof pclViewEditFilter, pclConfigPath);
		GetPrivateProfileString(ogAppName, "EDIT_VIEW_SORT"  , "FALSE", pclViewEditSort, sizeof pclViewEditSort, pclConfigPath);
		
		//BDPS-UIF Modus Varianten mit UMODE
		//UMODE = E44SWP	f�r SwissPort
		//UMODE = D44FAG	f�r FAG

		strcpy(CCSCedaData::pcmTableExt,    pcgTableExt);
		strcpy(CCSCedaData::pcmApplName,    pcgAppName);
		strcpy(CCSCedaData::pcmHomeAirport, pcgHome);

		ogHome     = pcgHome;
		ogTableExt = pcgTableExt;
		ogUIFModus = pcgUMode;

		if(strcmp(pclViewEditFilter, "TRUE") == 0)
			bgViewEditFilter = true;
		else
			bgViewEditFilter = false;

		if(strcmp(pclViewEditSort, "TRUE") == 0)
			bgViewEditSort = true;
		else
			bgViewEditSort = false;

		//setting for multi airports
		if (strcmp(pclTmpString,"TRUE")==0)
			bgUseMultiAirport = true;
		else
			bgUseMultiAirport = false;

		// Versionsinformation ermitteln
		VersionInfo rlInfo;
		VersionInfo::GetVersionInfo(NULL,rlInfo);	// must use NULL (and it works), because AFX is not initialized yet and will cause an ASSERT
		CCSCedaData::bmVersCheck = true;
		strncpy(CCSCedaData::pcmVersion,rlInfo.omFileVersion,sizeof(CCSCedaData::pcmVersion));
		CCSCedaData::pcmVersion[sizeof(CCSCedaData::pcmVersion)-1] = '\0';

		strncpy(CCSCedaData::pcmInternalBuild,rlInfo.omPrivateBuild.Right(4),sizeof(CCSCedaData::pcmInternalBuild));
		CCSCedaData::pcmInternalBuild[sizeof(CCSCedaData::pcmInternalBuild)-1] = '\0';

		//////////////////////////////////////
		AatHelp::Initialize("BDPS-UIF");


		omDummyDlg.Create(IDD_INFO,NULL);
		CAatLogin olLoginCtrl;
		olLoginCtrl.Create(NULL,WS_CHILD|WS_DISABLED,CRect(0,0,100,100),&omDummyDlg,4711);
		olLoginCtrl.SetInfoButtonVisible(TRUE);
		olLoginCtrl.SetRegisterApplicationString(ogInitModuData.GetInitModuTxt());

		if(ogCmdLineStghArray.GetAt(0) != ogAppName)
		{
			if (olLoginCtrl.DoLoginSilentMode(ogCmdLineStghArray.GetAt(1),ogCmdLineStghArray.GetAt(2)) != "OK")
			{
				omDummyDlg.DestroyWindow();
				return FALSE;
			}
		}
		else
		{
			if (olLoginCtrl.ShowLoginDialog() != "OK")
			{
				omDummyDlg.DestroyWindow();
				return FALSE;
			}

		}


		strcpy(cgUserName,olLoginCtrl.GetUserName_());
		ogBasicData.omUserID = olLoginCtrl.GetUserName_();
		ogCommHandler.SetUser(olLoginCtrl.GetUserName_());
		strcpy(CCSCedaData::pcmUser, olLoginCtrl.GetUserName_());
		strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);

		ogPrivList.Add(olLoginCtrl.GetPrivilegList());


		if(bgUseMultiAirport)
		{
			CString strSelectedExt= olLoginCtrl.GetTableExt();
			strSelectedExt.TrimLeft();
			strSelectedExt.TrimRight();
			if(strSelectedExt.GetLength()>0)
			{
				strcpy(pcgTableExt,olLoginCtrl.GetTableExt());
				ogTableExt = pcgTableExt;
				strcpy(CCSCedaData::pcmTableExt,  pcgTableExt);
			}
		}

		ogBCD.SetTableExtension(ogTableExt);
		ogBCD.SetHomeAirport(ogHome);

		CBDPSUIFApp *polAppl = (CBDPSUIFApp *)AfxGetApp();
		polAppl->LoadParameters();
		polAppl->InitBDPSTableDefaultView();

		ogCfgData.ReadUfisCedaConfig();
		ogCfgData.ReadCfgData();


		//INIT END/////////////////////////

		ogBcHandle.SetCloMessage(LoadStg(IDS_STRING709));

		ogBcHandle.AddTableCommand(CString("ALT") + ogTableExt, CString("IRT"), BC_ALT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("ALT") + ogTableExt, CString("URT"), BC_ALT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("ALT") + ogTableExt, CString("DRT"), BC_ALT_DELETE, false);

		ogBcHandle.AddTableCommand(CString("ACT") + ogTableExt, CString("IRT"), BC_ACT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("ACT") + ogTableExt, CString("URT"), BC_ACT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("ACT") + ogTableExt, CString("DRT"), BC_ACT_DELETE, false);

		ogBcHandle.AddTableCommand(CString("ACR") + ogTableExt, CString("IRT"), BC_ACR_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("ACR") + ogTableExt, CString("URT"), BC_ACR_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("ACR") + ogTableExt, CString("DRT"), BC_ACR_DELETE, false);

		ogBcHandle.AddTableCommand(CString("APT") + ogTableExt, CString("IRT"), BC_APT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("APT") + ogTableExt, CString("URT"), BC_APT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("APT") + ogTableExt, CString("DRT"), BC_APT_DELETE, false);

		ogBcHandle.AddTableCommand(CString("RWY") + ogTableExt, CString("IRT"), BC_RWY_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("RWY") + ogTableExt, CString("URT"), BC_RWY_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("RWY") + ogTableExt, CString("DRT"), BC_RWY_DELETE, false);

		ogBcHandle.AddTableCommand(CString("TWY") + ogTableExt, CString("IRT"), BC_TWY_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("TWY") + ogTableExt, CString("URT"), BC_TWY_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("TWY") + ogTableExt, CString("DRT"), BC_TWY_DELETE, false);

		ogBcHandle.AddTableCommand(CString("PST") + ogTableExt, CString("IRT"), BC_PST_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("PST") + ogTableExt, CString("URT"), BC_PST_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("PST") + ogTableExt, CString("DRT"), BC_PST_DELETE, false);

		ogBcHandle.AddTableCommand(CString("GAT") + ogTableExt, CString("IRT"), BC_GAT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("GAT") + ogTableExt, CString("URT"), BC_GAT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("GAT") + ogTableExt, CString("DRT"), BC_GAT_DELETE, false);

		ogBcHandle.AddTableCommand(CString("CIC") + ogTableExt, CString("IRT"), BC_CIC_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("CIC") + ogTableExt, CString("URT"), BC_CIC_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("CIC") + ogTableExt, CString("DRT"), BC_CIC_DELETE, false);

		ogBcHandle.AddTableCommand(CString("BLT") + ogTableExt, CString("IRT"), BC_BLT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("BLT") + ogTableExt, CString("URT"), BC_BLT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("BLT") + ogTableExt, CString("DRT"), BC_BLT_DELETE, false);

		ogBcHandle.AddTableCommand(CString("EXT") + ogTableExt, CString("IRT"), BC_EXT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("EXT") + ogTableExt, CString("URT"), BC_EXT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("EXT") + ogTableExt, CString("DRT"), BC_EXT_DELETE, false);

		ogBcHandle.AddTableCommand(CString("DEN") + ogTableExt, CString("IRT"), BC_DEN_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("DEN") + ogTableExt, CString("URT"), BC_DEN_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("DEN") + ogTableExt, CString("DRT"), BC_DEN_DELETE, false);

		ogBcHandle.AddTableCommand(CString("MVT") + ogTableExt, CString("IRT"), BC_MVT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("MVT") + ogTableExt, CString("URT"), BC_MVT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("MVT") + ogTableExt, CString("DRT"), BC_MVT_DELETE, false);

		ogBcHandle.AddTableCommand(CString("NAT") + ogTableExt, CString("IRT"), BC_NAT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("NAT") + ogTableExt, CString("URT"), BC_NAT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("NAT") + ogTableExt, CString("DRT"), BC_NAT_DELETE, false);

		ogBcHandle.AddTableCommand(CString("HAG") + ogTableExt, CString("IRT"), BC_HAG_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("HAG") + ogTableExt, CString("URT"), BC_HAG_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("HAG") + ogTableExt, CString("DRT"), BC_HAG_DELETE, false);

		ogBcHandle.AddTableCommand(CString("WRO") + ogTableExt, CString("IRT"), BC_WRO_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("WRO") + ogTableExt, CString("URT"), BC_WRO_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("WRO") + ogTableExt, CString("DRT"), BC_WRO_DELETE, false);

		ogBcHandle.AddTableCommand(CString("HTY") + ogTableExt, CString("IRT"), BC_HTY_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("HTY") + ogTableExt, CString("URT"), BC_HTY_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("HTY") + ogTableExt, CString("DRT"), BC_HTY_DELETE, false);

		ogBcHandle.AddTableCommand(CString("STY") + ogTableExt, CString("IRT"), BC_STY_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("STY") + ogTableExt, CString("URT"), BC_STY_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("STY") + ogTableExt, CString("DRT"), BC_STY_DELETE, false);

		ogBcHandle.AddTableCommand(CString("FID") + ogTableExt, CString("IRT"), BC_FID_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("FID") + ogTableExt, CString("URT"), BC_FID_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("FID") + ogTableExt, CString("DRT"), BC_FID_DELETE, false);

		ogBcHandle.AddTableCommand(CString("SEA") + ogTableExt, CString("IRT"), BC_SEA_NEW, false);
		ogBcHandle.AddTableCommand(CString("SEA") + ogTableExt, CString("URT"), BC_SEA_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("SEA") + ogTableExt, CString("DRT"), BC_SEA_DELETE, false);

		ogBcHandle.AddTableCommand(CString("STR") + ogTableExt, CString("IRT"), BC_STR_NEW, false);
		ogBcHandle.AddTableCommand(CString("STR") + ogTableExt, CString("URT"), BC_STR_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("STR") + ogTableExt, CString("DRT"), BC_STR_DELETE, false);

		ogBcHandle.AddTableCommand(CString("SPH") + ogTableExt, CString("IRT"), BC_SPH_NEW, false);
		ogBcHandle.AddTableCommand(CString("SPH") + ogTableExt, CString("URT"), BC_SPH_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("SPH") + ogTableExt, CString("DRT"), BC_SPH_DELETE, false);
		
		ogBcHandle.AddTableCommand(CString("GHS") + ogTableExt, CString("IRT"), BC_GHS_NEW, false);
		ogBcHandle.AddTableCommand(CString("GHS") + ogTableExt, CString("URT"), BC_GHS_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("GHS") + ogTableExt, CString("DRT"), BC_GHS_DELETE, false);

		//ogBcHandle.AddTableCommand(CString("GEG") + ogTableExt, CString("IRT"), BC_GEG_NEW, false);
		//ogBcHandle.AddTableCommand(CString("GEG") + ogTableExt, CString("URT"), BC_GEG_CHANGE, false);
		//ogBcHandle.AddTableCommand(CString("GEG") + ogTableExt, CString("DRT"), BC_GEG_DELETE, false);

		ogBcHandle.AddTableCommand(CString("PER") + ogTableExt, CString("IRT"), BC_PER_NEW, false);
		ogBcHandle.AddTableCommand(CString("PER") + ogTableExt, CString("URT"), BC_PER_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("PER") + ogTableExt, CString("DRT"), BC_PER_DELETE, false);
		
		ogBcHandle.AddTableCommand(CString("PEF") + ogTableExt, CString("IRT"), BC_PEF_NEW, false);
		ogBcHandle.AddTableCommand(CString("PEF") + ogTableExt, CString("URT"), BC_PEF_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("PEF") + ogTableExt, CString("DRT"), BC_PEF_DELETE, false);
		
		ogBcHandle.AddTableCommand(CString("GRM") + ogTableExt, CString("IRT"), BC_GRM_NEW, false);
		ogBcHandle.AddTableCommand(CString("GRM") + ogTableExt, CString("URT"), BC_GRM_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("GRM") + ogTableExt, CString("DRT"), BC_GRM_DELETE, false);
		
		ogBcHandle.AddTableCommand(CString("GRN") + ogTableExt, CString("IRT"), BC_GRN_NEW, false);
		ogBcHandle.AddTableCommand(CString("GRN") + ogTableExt, CString("URT"), BC_GRN_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("GRN") + ogTableExt, CString("DRT"), BC_GRN_DELETE, false);

		ogBcHandle.AddTableCommand(CString("ORG") + ogTableExt, CString("IRT"), BC_ORG_NEW, false);
		ogBcHandle.AddTableCommand(CString("ORG") + ogTableExt, CString("URT"), BC_ORG_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("ORG") + ogTableExt, CString("DRT"), BC_ORG_DELETE, false);

		ogBcHandle.AddTableCommand(CString("PFC") + ogTableExt, CString("IRT"), BC_PFC_NEW, false);
		ogBcHandle.AddTableCommand(CString("PFC") + ogTableExt, CString("URT"), BC_PFC_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("PFC") + ogTableExt, CString("DRT"), BC_PFC_DELETE, false);

		ogBcHandle.AddTableCommand(CString("COT") + ogTableExt, CString("IRT"), BC_COT_NEW, false);
		ogBcHandle.AddTableCommand(CString("COT") + ogTableExt, CString("URT"), BC_COT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("COT") + ogTableExt, CString("DRT"), BC_COT_DELETE, false);
		
		ogBcHandle.AddTableCommand(CString("ASF") + ogTableExt, CString("IRT"), BC_ASF_NEW, false);
		ogBcHandle.AddTableCommand(CString("ASF") + ogTableExt, CString("URT"), BC_ASF_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("ASF") + ogTableExt, CString("DRT"), BC_ASF_DELETE, false);

		ogBcHandle.AddTableCommand(CString("BSS") + ogTableExt, CString("IRT"), BC_BSS_NEW, false);
		ogBcHandle.AddTableCommand(CString("BSS") + ogTableExt, CString("URT"), BC_BSS_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("BSS") + ogTableExt, CString("DRT"), BC_BSS_DELETE, false);

		ogBcHandle.AddTableCommand(CString("BSD") + ogTableExt, CString("IRT"), BC_BSD_NEW, false);
		ogBcHandle.AddTableCommand(CString("BSD") + ogTableExt, CString("URT"), BC_BSD_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("BSD") + ogTableExt, CString("DRT"), BC_BSD_DELETE, false);
		
		ogBcHandle.AddTableCommand(CString("ODA") + ogTableExt, CString("IRT"), BC_ODA_NEW, false);
		ogBcHandle.AddTableCommand(CString("ODA") + ogTableExt, CString("URT"), BC_ODA_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("ODA") + ogTableExt, CString("DRT"), BC_ODA_DELETE, false);
		
		ogBcHandle.AddTableCommand(CString("TEA") + ogTableExt, CString("IRT"), BC_TEA_NEW, false);
		ogBcHandle.AddTableCommand(CString("TEA") + ogTableExt, CString("URT"), BC_TEA_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("TEA") + ogTableExt, CString("DRT"), BC_TEA_DELETE, false);
		
		ogBcHandle.AddTableCommand(CString("STF") + ogTableExt, CString("IRT"), BC_STF_NEW, false);
		ogBcHandle.AddTableCommand(CString("STF") + ogTableExt, CString("URT"), BC_STF_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("STF") + ogTableExt, CString("DRT"), BC_STF_DELETE, false);

		ogBcHandle.AddTableCommand(CString("WAY") + ogTableExt, CString("IRT"), BC_WAY_NEW, false);
		ogBcHandle.AddTableCommand(CString("WAY") + ogTableExt, CString("URT"), BC_WAY_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("WAY") + ogTableExt, CString("DRT"), BC_WAY_DELETE, false);

		ogBcHandle.AddTableCommand(CString("PRC") + ogTableExt, CString("IRT"), BC_PRC_NEW, false);
		ogBcHandle.AddTableCommand(CString("PRC") + ogTableExt, CString("URT"), BC_PRC_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("PRC") + ogTableExt, CString("DRT"), BC_PRC_DELETE, false);

		ogBcHandle.AddTableCommand(CString("CHT") + ogTableExt, CString("IRT"), BC_CHT_NEW, false);
		ogBcHandle.AddTableCommand(CString("CHT") + ogTableExt, CString("URT"), BC_CHT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("CHT") + ogTableExt, CString("DRT"), BC_CHT_DELETE, false);

		ogBcHandle.AddTableCommand(CString("TIP") + ogTableExt, CString("IRT"), BC_TIP_NEW, false);
		ogBcHandle.AddTableCommand(CString("TIP") + ogTableExt, CString("URT"), BC_TIP_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("TIP") + ogTableExt, CString("DRT"), BC_TIP_DELETE, false);

		ogBcHandle.AddTableCommand(CString("BLK") + ogTableExt, CString("IRT"), BC_BLK_NEW, false);
		ogBcHandle.AddTableCommand(CString("BLK") + ogTableExt, CString("URT"), BC_BLK_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("BLK") + ogTableExt, CString("DRT"), BC_BLK_DELETE, false);

		ogBcHandle.AddTableCommand(CString("HOL") + ogTableExt, CString("IRT"), BC_HOL_NEW, false);
		ogBcHandle.AddTableCommand(CString("HOL") + ogTableExt, CString("URT"), BC_HOL_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("HOL") + ogTableExt, CString("DRT"), BC_HOL_DELETE, false);

		ogBcHandle.AddTableCommand(CString("WGP") + ogTableExt, CString("IRT"), BC_WGP_NEW, false);
		ogBcHandle.AddTableCommand(CString("WGP") + ogTableExt, CString("URT"), BC_WGP_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("WGP") + ogTableExt, CString("DRT"), BC_WGP_DELETE, false);

		ogBcHandle.AddTableCommand(CString("PAC") + ogTableExt, CString("IRT"), BC_PAC_NEW, false);
		ogBcHandle.AddTableCommand(CString("PAC") + ogTableExt, CString("URT"), BC_PAC_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("PAC") + ogTableExt, CString("DRT"), BC_PAC_DELETE, false);

		ogBcHandle.AddTableCommand(CString("OAC") + ogTableExt, CString("IRT"), BC_OAC_NEW, false);
		ogBcHandle.AddTableCommand(CString("OAC") + ogTableExt, CString("URT"), BC_OAC_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("OAC") + ogTableExt, CString("DRT"), BC_OAC_DELETE, false);

		ogBcHandle.AddTableCommand(CString("PGP") + ogTableExt, CString("IRT"), BC_PGP_NEW, false);
		ogBcHandle.AddTableCommand(CString("PGP") + ogTableExt, CString("URT"), BC_PGP_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("PGP") + ogTableExt, CString("DRT"), BC_PGP_DELETE, false);

		ogBcHandle.AddTableCommand(CString("AWI") + ogTableExt, CString("IRT"), BC_AWI_NEW, false);
		ogBcHandle.AddTableCommand(CString("AWI") + ogTableExt, CString("URT"), BC_AWI_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("AWI") + ogTableExt, CString("DRT"), BC_AWI_DELETE, false);

		ogBcHandle.AddTableCommand(CString("CCC") + ogTableExt, CString("IRT"), BC_CCC_NEW, false);
		ogBcHandle.AddTableCommand(CString("CCC") + ogTableExt, CString("URT"), BC_CCC_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("CCC") + ogTableExt, CString("DRT"), BC_CCC_DELETE, false);

		ogBcHandle.AddTableCommand(CString("VIP") + ogTableExt, CString("IRT"), BC_VIP_NEW, false);
		ogBcHandle.AddTableCommand(CString("VIP") + ogTableExt, CString("URT"), BC_VIP_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("VIP") + ogTableExt, CString("DRT"), BC_VIP_DELETE, false);

		ogBcHandle.AddTableCommand(CString("AFM") + ogTableExt, CString("IRT"), BC_AFM_NEW, false);
		ogBcHandle.AddTableCommand(CString("AFM") + ogTableExt, CString("URT"), BC_AFM_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("AFM") + ogTableExt, CString("DRT"), BC_AFM_DELETE, false);

		ogBcHandle.AddTableCommand(CString("ENT") + ogTableExt, CString("IRT"), BC_ENT_NEW, false);
		ogBcHandle.AddTableCommand(CString("ENT") + ogTableExt, CString("URT"), BC_ENT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("ENT") + ogTableExt, CString("DRT"), BC_ENT_DELETE, false);

		ogBcHandle.AddTableCommand(CString("SRC") + ogTableExt, CString("IRT"), BC_SRC_NEW, false);
		ogBcHandle.AddTableCommand(CString("SRC") + ogTableExt, CString("URT"), BC_SRC_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("SRC") + ogTableExt, CString("DRT"), BC_SRC_DELETE, false);

		ogBcHandle.AddTableCommand(CString("PAR") + ogTableExt, CString("IRT"), BC_PAR_NEW, false);
		ogBcHandle.AddTableCommand(CString("PAR") + ogTableExt, CString("URT"), BC_PAR_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("PAR") + ogTableExt, CString("DRT"), BC_PAR_DELETE, false);

		ogBcHandle.AddTableCommand(CString("POL") + ogTableExt, CString("IRT"), BC_POL_NEW, false);
		ogBcHandle.AddTableCommand(CString("POL") + ogTableExt, CString("URT"), BC_POL_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("POL") + ogTableExt, CString("DRT"), BC_POL_DELETE, false);

		ogBcHandle.AddTableCommand(CString("SGM") + ogTableExt, CString("IRT"), BC_SGM_NEW, false);
		ogBcHandle.AddTableCommand(CString("SGM") + ogTableExt, CString("URT"), BC_SGM_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("SGM") + ogTableExt, CString("DRT"), BC_SGM_DELETE, false);
		
		ogBcHandle.AddTableCommand(CString("SGR") + ogTableExt, CString("IRT"), BC_SGR_NEW, false);
		ogBcHandle.AddTableCommand(CString("SGR") + ogTableExt, CString("URT"), BC_SGR_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("SGR") + ogTableExt, CString("DRT"), BC_SGR_DELETE, false);

		ogBcHandle.AddTableCommand(CString("MFM") + ogTableExt, CString("IRT"), BC_MFM_NEW, false);
		ogBcHandle.AddTableCommand(CString("MFM") + ogTableExt, CString("URT"), BC_MFM_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("MFM") + ogTableExt, CString("DRT"), BC_MFM_DELETE, false);

		ogBcHandle.AddTableCommand(CString("STS") + ogTableExt, CString("IRT"), BC_STS_NEW, false);
		ogBcHandle.AddTableCommand(CString("STS") + ogTableExt, CString("URT"), BC_STS_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("STS") + ogTableExt, CString("DRT"), BC_STS_DELETE, false);

		ogBcHandle.AddTableCommand(CString("NWH") + ogTableExt, CString("IRT"), BC_NWH_NEW, false);
		ogBcHandle.AddTableCommand(CString("NWH") + ogTableExt, CString("URT"), BC_NWH_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("NWH") + ogTableExt, CString("DRT"), BC_NWH_DELETE, false);

		ogBcHandle.AddTableCommand(CString("COH") + ogTableExt, CString("IRT"), BC_COH_NEW, false);
		ogBcHandle.AddTableCommand(CString("COH") + ogTableExt, CString("URT"), BC_COH_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("COH") + ogTableExt, CString("DRT"), BC_COH_DELETE, false);

		ogBcHandle.AddTableCommand(CString("VAL") + ogTableExt, CString("IRT"), BC_VAL_NEW, false);
		ogBcHandle.AddTableCommand(CString("VAL") + ogTableExt, CString("URT"), BC_VAL_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("VAL") + ogTableExt, CString("DRT"), BC_VAL_DELETE, false);

		ogBcHandle.AddTableCommand(CString("PMX") + ogTableExt, CString("IRT"), BC_PMX_NEW, false);
		ogBcHandle.AddTableCommand(CString("PMX") + ogTableExt, CString("URT"), BC_PMX_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("PMX") + ogTableExt, CString("DRT"), BC_PMX_DELETE, false);

		ogBcHandle.AddTableCommand(CString("WIS") + ogTableExt, CString("IRT"), BC_WIS_NEW, false);
		ogBcHandle.AddTableCommand(CString("WIS") + ogTableExt, CString("URT"), BC_WIS_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("WIS") + ogTableExt, CString("DRT"), BC_WIS_DELETE, false);

		ogBcHandle.AddTableCommand(CString("WGN") + ogTableExt, CString("IRT"), BC_WGN_NEW, false);
		ogBcHandle.AddTableCommand(CString("WGN") + ogTableExt, CString("URT"), BC_WGN_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("WGN") + ogTableExt, CString("DRT"), BC_WGN_DELETE, false);


		ogBcHandle.AddTableCommand(CString("DEV") + ogTableExt, CString("IRT"), BC_DEV_NEW, false);
		ogBcHandle.AddTableCommand(CString("DEV") + ogTableExt, CString("URT"), BC_DEV_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("DEV") + ogTableExt, CString("DRT"), BC_DEV_DELETE, false);

		ogBcHandle.AddTableCommand(CString("DSP") + ogTableExt, CString("IRT"), BC_DSP_NEW, false);
		ogBcHandle.AddTableCommand(CString("DSP") + ogTableExt, CString("URT"), BC_DSP_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("DSP") + ogTableExt, CString("DRT"), BC_DSP_DELETE, false);

		ogBcHandle.AddTableCommand(CString("PAG") + ogTableExt, CString("IRT"), BC_PAG_NEW, false);
		ogBcHandle.AddTableCommand(CString("PAG") + ogTableExt, CString("URT"), BC_PAG_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("PAG") + ogTableExt, CString("DRT"), BC_PAG_DELETE, false);


		ogBcHandle.AddTableCommand(CString("EQT") + ogTableExt, CString("IRT"), BC_EQT_NEW, false);
		ogBcHandle.AddTableCommand(CString("EQT") + ogTableExt, CString("URT"), BC_EQT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("EQT") + ogTableExt, CString("DRT"), BC_EQT_DELETE, false);

		ogBcHandle.AddTableCommand(CString("EQU") + ogTableExt, CString("IRT"), BC_EQU_NEW, false);
		ogBcHandle.AddTableCommand(CString("EQU") + ogTableExt, CString("URT"), BC_EQU_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("EQU") + ogTableExt, CString("DRT"), BC_EQU_DELETE, false);
		//PRF 8378
		ogBcHandle.AddTableCommand(CString("CRC") + ogTableExt, CString("IRT"), BC_CRC_NEW, false);
		ogBcHandle.AddTableCommand(CString("CRC") + ogTableExt, CString("URT"), BC_CRC_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("CRC") + ogTableExt, CString("DRT"), BC_CRC_DELETE, false);

		ogBcHandle.AddTableCommand(CString("DSC") + ogTableExt, CString("IRT"), BC_DSC_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("DSC") + ogTableExt, CString("URT"), BC_DSC_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("DSC") + ogTableExt, CString("DRT"), BC_DSC_DELETE, false);

		ogBcHandle.AddTableCommand(CString("DPT") + ogTableExt, CString("IRT"), BC_DPT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("DPT") + ogTableExt, CString("URT"), BC_DPT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("DPT") + ogTableExt, CString("DRT"), BC_DPT_DELETE, false);

		ogBcHandle.AddTableCommand(CString("FLG") + ogTableExt, CString("IRT"), BC_FLG_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("FLG") + ogTableExt, CString("URT"), BC_FLG_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("FLG") + ogTableExt, CString("DRT"), BC_FLG_DELETE, false);

		ogBcHandle.AddTableCommand(CString("CHU") + ogTableExt, CString("IRT"), BC_CHU_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("CHU") + ogTableExt, CString("URT"), BC_CHU_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("CHU") + ogTableExt, CString("DRT"), BC_CHU_DELETE, false);

		ogBcHandle.AddTableCommand(CString("SBCXXX"),CString("SBC"),BC_SBC_XXX, true);


		
		ogBCD.SetObject("ACI");
		ogBCD.SetObject("DEL");
		ogBCD.SetObject("DLG");
		ogBCD.SetObject("DRA");
		ogBCD.SetObject("DRD");
		ogBCD.SetObject("DRG");
		ogBCD.SetObject("DRR");
		ogBCD.SetObject("DSR");
		ogBCD.SetObject("DSR");
		ogBCD.SetObject("DRS");
		ogBCD.SetObject("DRW");
		ogBCD.SetObject("EQU");
		ogBCD.SetObject("GPL");

		ogBCD.SetObject("GSP");
		ogBCD.SetObject("JOB");
		ogBCD.SetObject("MAW");

		ogBCD.SetObject("MSD");

		ogBCD.SetObject("POA");
		ogBCD.SetObject("REL");
		ogBCD.SetObject("RPQ");
		ogBCD.SetObject("RPF");
		ogBCD.SetObject("SCO");
		ogBCD.SetObject("SDA");
		ogBCD.SetObject("SDT");
		ogBCD.SetObject("SEF");
		ogBCD.SetObject("SOR");
		ogBCD.SetObject("SPE");
		ogBCD.SetObject("SPF");
		ogBCD.SetObject("SWG");


		if (!ogBasicData.SetLocalDiff())
		{
			if (AfxMessageBox(LoadStg(IDS_STRING988),MB_ICONSTOP|MB_YESNO) == IDNO)
			{
				AfxAbort();
			}
		}

		first = false;
	}

	return true;
}


BSTR CApplication::GetCaller() 
{
	CString strResult;
	// TODO: Add your property handler here
	strResult = 	ogCmdLineStghArray.GetAt(0);		
	return strResult.AllocSysString();
}

void CApplication::SetCaller(LPCTSTR lpszNewValue) 
{
	// TODO: Add your property handler here
	ogCmdLineStghArray.SetAt(0,lpszNewValue);

}
