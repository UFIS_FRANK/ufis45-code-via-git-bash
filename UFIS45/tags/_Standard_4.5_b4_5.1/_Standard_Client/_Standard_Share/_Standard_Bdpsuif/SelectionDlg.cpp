// SelectionDlg.cpp : implementation file
//

#include <stdafx.h>
#include <bdpsuif.h>
#include <SelectionDlg.h>
#include <BasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// SelectionDlg dialog


SelectionDlg::SelectionDlg(CWnd* pParent /*=NULL*/)
	: CDialog(SelectionDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(SelectionDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	bmUseAllItem = false;
}

SelectionDlg::~SelectionDlg()
{
}


void SelectionDlg::SetCaption(const CString& ropCaption)
{
	omCaption = ropCaption;
}

void SelectionDlg::SetPossibleList(const CString& ropCaption,const CStringArray& ropList)
{
	omPossibleCaption = ropCaption;
	omPossibleItems.Append(ropList);
}

void SelectionDlg::SetSelectionList(const CString& ropCaption,const CStringArray& ropList)
{
	omSelectedCaption = ropCaption;
	omSelectedItems.Append(ropList);
}

void SelectionDlg::GetSelectionList(CStringArray& ropList)
{
	ropList.Append(omSelectedItems);
}

void SelectionDlg::UseAllItem(bool bpUseIt)
{
	bmUseAllItem = bpUseIt;
}

void SelectionDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(SelectionDlg)
	DDX_Control(pDX, IDC_SPIN_SELECTED_ITEMS, m_SpinSelectedItems);
	DDX_Control(pDX, IDC_BUTTON_REMOVE, m_RemoveButton);
	DDX_Control(pDX, IDC_BUTTON_ADD, m_AddButton);
	DDX_Control(pDX, IDC_BUTTON_REMOVE_ALL, m_RemoveAllButton);
	DDX_Control(pDX, IDC_BUTTON_ADD_ALL, m_AddAllButton);
	DDX_Control(pDX, IDC_CONTENTLIST, m_ContentList);
	DDX_Control(pDX, IDC_INSERTLIST, m_InsertList);
	//}}AFX_DATA_MAP
	if (pDX->m_bSaveAndValidate == FALSE)
	{
		int ilLc;
		
		// Do not update window while processing new data
		m_InsertList.SetRedraw(FALSE);
		m_ContentList.SetRedraw(FALSE);

		// Initialize all possible items into the left list box
		m_InsertList.ResetContent();
		if (bmUseAllItem)
			m_InsertList.AddString(LoadStg(IDS_ALL));

		for (ilLc = 0; ilLc < omPossibleItems.GetSize(); ilLc++)
			m_InsertList.AddString(omPossibleItems[ilLc]);

		// Initialize the selected items into the right list box,
		// also remove the corresponding items from the left list box.
		m_ContentList.ResetContent();
		bool blFoundAlle = false;
		for (ilLc = 0; ilLc < omSelectedItems.GetSize()  && blFoundAlle == false; ilLc++)
		{
			if (omSelectedItems[ilLc] == LoadStg(IDS_ALL))
			{
				/************ using all *******************/
				m_AddButton.EnableWindow(FALSE);
				blFoundAlle = true;
				/*** remove all entries except "*Alle" from content list **/
				m_ContentList.ResetContent();
				m_ContentList.AddString(LoadStg(IDS_ALL));	
				/*** show empty insert list ***/
				m_InsertList.ResetContent();
			}
			else
			{
				m_ContentList.AddString(omSelectedItems[ilLc]);
				m_InsertList.DeleteString(m_InsertList.FindStringExact(-1, omSelectedItems[ilLc]));
			}
		}

		m_InsertList.SetRedraw(TRUE);
		m_ContentList.SetRedraw(TRUE);
	}
	if (pDX->m_bSaveAndValidate == TRUE)
	{
		// Copy data from the list box back to the array of string
		omSelectedItems.SetSize(m_ContentList.GetCount());
		for (int ilLc = 0; ilLc < omSelectedItems.GetSize(); ilLc++)
			m_ContentList.GetText(ilLc, omSelectedItems[ilLc]);
	}

}


BEGIN_MESSAGE_MAP(SelectionDlg, CDialog)
	//{{AFX_MSG_MAP(SelectionDlg)
	ON_BN_CLICKED(IDC_BUTTON_REMOVE_ALL, OnButtonRemoveAll)
	ON_BN_CLICKED(IDC_BUTTON_ADD, OnButtonAdd)
	ON_BN_CLICKED(IDC_BUTTON_ADD_ALL, OnButtonAddAll)
	ON_BN_CLICKED(IDC_BUTTON_REMOVE, OnButtonRemove)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_SELECTED_ITEMS, OnDeltaposSpinSelectedItems)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// SelectionDlg message handlers

void SelectionDlg::OnButtonRemoveAll() 
{
	// TODO: Add your control notification handler code here
	CString olText;
	bool blFoundAlle = false;

	// Copy all items from right list box to left list box
	for (int ilLc = 0; ilLc < m_ContentList.GetCount() && blFoundAlle == false; ilLc++)
	{
		m_ContentList.GetText(ilLc,olText);	// load string to "s"
		if (olText == LoadStg(IDS_ALL))
		{
			m_AddButton.EnableWindow(TRUE);
			blFoundAlle = true;
			/*** rebuild insert list ***/
			m_InsertList.ResetContent();
			m_InsertList.AddString(LoadStg(IDS_ALL));
		}
		else
		{
			m_InsertList.AddString(olText);	// move string from right to left box
		}
	}
	
	// delete all items from right list box
	blFoundAlle = false;
	for (ilLc = m_ContentList.GetCount()-1; ilLc >= 0  && blFoundAlle == false; ilLc--)
	{
		m_ContentList.GetText(ilLc,olText);	// load string to "s"
		if (olText == LoadStg(IDS_ALL))
		{
			m_AddButton.EnableWindow(TRUE);
			blFoundAlle = true;
			/*** show empty content list ***/
			m_ContentList.ResetContent();
		}
		else
		{
			m_ContentList.DeleteString(ilLc);
		}
	}
	
}

void SelectionDlg::OnButtonAdd() 
{
	// TODO: Add your control notification handler code here
	CString olText;
	bool blFoundAlle = false;

	// Copy selected items from left list box to right list box
	for (int ilLc = 0; ilLc < m_InsertList.GetCount() && blFoundAlle == false; ilLc++)
	{
		if (!m_InsertList.GetSel(ilLc))	// unselected item?
			continue;

		m_InsertList.GetText(ilLc, olText);	// load string to "olText"
		if (olText == LoadStg(IDS_ALL))
		{
			/************ using all *******************/
			m_AddButton.EnableWindow(FALSE);
			blFoundAlle = true;
			/*** remove all entries except "*Alle" from content list **/
			m_ContentList.ResetContent();
			m_ContentList.AddString(LoadStg(IDS_ALL));	
		}
		else
		{
			m_ContentList.AddString(olText);	// move string from left to right box
		}
	}
	
	// delete selected items from right list box
	blFoundAlle = false;
	for (ilLc = m_InsertList.GetCount()-1; ilLc >= 0 && blFoundAlle == false; ilLc--)
	{
		if (!m_InsertList.GetSel(ilLc))	// unselected item?
			continue;
		m_InsertList.GetText(ilLc, olText);	// load string to "olText"
		if (olText == LoadStg(IDS_ALL))
		{
			/************ using all *******************/
			m_AddButton.EnableWindow(FALSE);
			blFoundAlle = true;
			/*** show empty insert list ***/
			m_InsertList.ResetContent();
		}
		else
		{
			m_InsertList.DeleteString(ilLc);
		}
	}
	
}

void SelectionDlg::OnButtonAddAll() 
{
	// TODO: Add your control notification handler code here
	CString olText;
	bool blFoundAlle = false;

	// Copy selected items from left list box to right list box
	for (int ilLc = 0; ilLc < m_InsertList.GetCount() && blFoundAlle == false; ilLc++)
	{
		m_InsertList.GetText(ilLc, olText);	// load string to "olText"
		if (olText == LoadStg(IDS_ALL))
		{
			/************ using all *******************/
			m_AddButton.EnableWindow(FALSE);
			blFoundAlle = true;
			/*** remove all entries except "*Alle" from content list **/
			m_ContentList.ResetContent();
			m_ContentList.AddString(LoadStg(IDS_ALL));	
		}
		else
		{
			m_ContentList.AddString(olText);	// move string from left to right box
		}
	}
	
	// delete selected items from right list box
	blFoundAlle = false;
	for (ilLc = m_InsertList.GetCount()-1; ilLc >= 0 && blFoundAlle == false; ilLc--)
	{
		m_InsertList.GetText(ilLc, olText);	// load string to "olText"
		if (olText == LoadStg(IDS_ALL))
		{
			/************ using all *******************/
			m_AddButton.EnableWindow(FALSE);
			blFoundAlle = true;
			/*** show empty insert list ***/
			m_InsertList.ResetContent();
		}
		else
		{
			m_InsertList.DeleteString(ilLc);
		}
	}
	
}

void SelectionDlg::OnButtonRemove() 
{
	// TODO: Add your control notification handler code here
	CString olText;
	bool blFoundAlle = false;

	// copy selected items from right list box to left list box
	for (int ilLc = 0; ilLc < m_ContentList.GetCount()  && blFoundAlle == false; ilLc++)
	{
		if (!m_ContentList.GetSel(ilLc))	// unselected item?
			continue;

		m_ContentList.GetText(ilLc,olText);	// load string to "s"
		if (olText == LoadStg(IDS_ALL))
		{
			m_AddButton.EnableWindow(TRUE);
			blFoundAlle = true;
			/*** rebuild insert list ***/
			m_InsertList.ResetContent();
			m_InsertList.AddString(LoadStg(IDS_ALL));
		}
		else
		{
			m_InsertList.AddString(olText);	// move string from right to left box
		}
	}
	

	// Delete selected items from right list box
	blFoundAlle = false;
	for (ilLc = m_ContentList.GetCount()-1; ilLc >= 0  && blFoundAlle == false; ilLc--)
	{
		if (!m_ContentList.GetSel(ilLc))	// unselected item?
			continue;

		m_ContentList.GetText(ilLc,olText);	// load string to "s"
		if (olText == LoadStg(IDS_ALL))
		{
			m_AddButton.EnableWindow(TRUE);
			blFoundAlle = true;
			/*** show empty content list ***/
			m_ContentList.ResetContent();

		}
		else
		{
			m_ContentList.DeleteString(ilLc);
		}
	}
	
}

BOOL SelectionDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	this->SetWindowText(omCaption);
	GetDlgItem(IDC_STATIC1)->SetWindowText(omPossibleCaption);
	GetDlgItem(IDC_STATIC2)->SetWindowText(omSelectedCaption);
	
	if (bmUseAllItem)
	{
		m_RemoveAllButton.ShowWindow(SW_HIDE);
		m_AddAllButton.ShowWindow(SW_HIDE);
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void SelectionDlg::OnDeltaposSpinSelectedItems(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	// TODO: Add your control notification handler code here
#if	1
	int ilSelCount = this->m_ContentList.GetSelCount();
	if (ilSelCount == 0)
	{
		*pResult = 0;
		return;
	}


	int ilCount = this->m_ContentList.GetCount() - 1;
	int *pilSelections = new int[ilSelCount];
	this->m_ContentList.GetSelItems(ilSelCount,pilSelections);

	for (int i = 0; i < ilSelCount; i++)
	{
		this->m_ContentList.SetSel(pilSelections[i],false);
	}

	if (pNMUpDown->iDelta < 0)
	{
		for (int i = 0; i < ilSelCount; i++)
		{
			for (int j = i+1; j <ilSelCount; j++)
			{
				if (pilSelections[i] == 0)
					pilSelections[j] -= 1;
				else if (pilSelections[j] < pilSelections[i])
					pilSelections[j] += 1;
			}

			CString olName;
			this->m_ContentList.GetText(pilSelections[i],olName);
			this->m_ContentList.DeleteString(pilSelections[i]);
			pilSelections[i] += pNMUpDown->iDelta;
			if (pilSelections[i] < 0)
				pilSelections[i] = -1;
			else if (pilSelections[i] > ilCount)
				pilSelections[i] = 0;
			int ilSel = this->m_ContentList.InsertString(pilSelections[i],olName);
			this->m_ContentList.SetSel(ilSel,true);

		}
	}
	else
	{
		for (int i = ilSelCount - 1; i >= 0; i--)
		{

			for (int j = i-1; j >= 0; j--)
			{
				if (pilSelections[i] == ilCount)
					pilSelections[j] += 1;
				else if (pilSelections[j] > pilSelections[i])
					pilSelections[j] += 1;
			}
			CString olName;
			this->m_ContentList.GetText(pilSelections[i],olName);
			this->m_ContentList.DeleteString(pilSelections[i]);
			pilSelections[i] += pNMUpDown->iDelta;
			if (pilSelections[i] < 0)
				pilSelections[i] = -1;
			else if (pilSelections[i] > ilCount)
				pilSelections[i] = 0;
			int ilSel = this->m_ContentList.InsertString(pilSelections[i],olName);
			this->m_ContentList.SetSel(ilSel,true);

		}
	}

	delete[] pilSelections;

	UpdateData(TRUE);
	*pResult = 0;

#else
	bool blAnythingToDo = true;
	CMapStringToPtr	olMap;

	while(blAnythingToDo)
	{
		blAnythingToDo = false;
		int ilSelCount = this->m_ContentList.GetSelCount();
		if (ilSelCount == 0)
		{
			break;
		}

		int ilCount = this->m_ContentList.GetCount() - 1;
		int *pilSelections = new int[ilSelCount];
		this->m_ContentList.GetSelItems(ilSelCount,pilSelections);

		CString olName;
		if (pNMUpDown->iDelta < 0)
		{
			for (int i = 0; i < ilSelCount; i++)
			{
				void *polValue;
				this->m_ContentList.GetText(pilSelections[i],olName);
				if (!olMap.Lookup(olName,polValue))
				{
					this->m_ContentList.DeleteString(pilSelections[i]);
					pilSelections[i] += pNMUpDown->iDelta;
					if (pilSelections[i] < 0)
						pilSelections[i] = -1;
					else if (pilSelections[i] > ilCount)
						pilSelections[i] = 0;
					int ilSel = this->m_ContentList.InsertString(pilSelections[i],olName);
					this->m_ContentList.SetSel(ilSel,true);
					olMap.SetAt(olName,NULL);
					blAnythingToDo = true;
					break;
				}
			}
		}
		else
		{
			for (int i = ilSelCount -1; i >= 0; i--)
			{
				void *polValue;
				this->m_ContentList.GetText(pilSelections[i],olName);
				if (!olMap.Lookup(olName,polValue))
				{
					this->m_ContentList.DeleteString(pilSelections[i]);
					pilSelections[i] += pNMUpDown->iDelta;
					if (pilSelections[i] < 0)
						pilSelections[i] = -1;
					else if (pilSelections[i] > ilCount)
						pilSelections[i] = 0;
					int ilSel = this->m_ContentList.InsertString(pilSelections[i],olName);
					this->m_ContentList.SetSel(ilSel,true);
					olMap.SetAt(olName,NULL);
					blAnythingToDo = true;
					break;
				}
			}
		}
		delete[] pilSelections;
	}

	UpdateData(TRUE);
	*pResult = 0;
#endif
}
