// Odg_Dlg.cpp: Implementierungsdatei
//

#include <stdafx.h>
#include <bdpsuif.h>
#include <Odg_Dlg.h>
#include <CedaBasicData.h>
//#include "CCSCedadata.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld COdg_Dlg 


COdg_Dlg::COdg_Dlg(CWnd* pParent /*=NULL*/)
	: CDialog(COdg_Dlg::IDD, pParent)
{
/*	//{{AFX_DATA_INIT(COdg_Dlg)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT
*/
}

COdg_Dlg::COdg_Dlg(CString olOrgCode, CString olOrgUrno,CWnd* pParent /*=NULL*/)
	: CDialog(COdg_Dlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(COdg_Dlg)
		// HINWEIS: Der Klassen-Assistent f�gt hier Elementinitialisierung ein
	//}}AFX_DATA_INIT
	pomOrgCode = olOrgCode;
	pomOrgUrno = olOrgUrno;
	m_gb_NewCode = false;
}




void COdg_Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(COdg_Dlg)
	DDX_Control(pDX, IDC_COMBO1, m_CB_ODG_Code);
	DDX_Control(pDX, IDC_REMA, m_Ctrl_Rema);
	DDX_Control(pDX, IDC_ODGN, m_Ctrl_Odgn);
//	DDX_Control(pDX, IDC_CODE, m_Ctrl_Code);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(COdg_Dlg, CDialog)
	//{{AFX_MSG_MAP(COdg_Dlg)
	ON_BN_CLICKED(ID_Delete, OnDelete)
	ON_CBN_SELCHANGE(IDC_COMBO1, OnSelchangeCombo1)
	ON_CBN_EDITCHANGE(IDC_COMBO1, OnEditchangeCombo1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// Behandlungsroutinen f�r Nachrichten COdg_Dlg 

BOOL COdg_Dlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_Orgu = pomOrgUrno;
	SetWindowText("Duty Group Code Dialog: <" + pomOrgCode + ">");

	// TODO: Zus�tzliche Initialisierung hier einf�gen
	ReadOdg();	
	InitCombo();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX-Eigenschaftenseiten sollten FALSE zur�ckgeben
}

void COdg_Dlg::OnOK() 
{
	// TODO: Zus�tzliche Pr�fung hier einf�gen
	SaveOdg();	
	CDialog::OnOK();
}

void COdg_Dlg::OnDelete() 
{
	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen
	m_CB_ODG_Code.GetWindowText(m_Code);

	CString Urno = ogBCD.GetFieldExt("ODG", "ORGU", "ODGC", m_Orgu, m_Code, "URNO");

	if (!Urno.IsEmpty())
	{
		ogBCD.DeleteRecord("ODG","URNO", Urno, true);
	}
	CDialog::OnOK();
}


void COdg_Dlg::OnUpdateCode() 
{
	// TODO: Wenn es sich hierbei um ein RICHEDIT-Steuerelement handelt, sendet es
	// sendet diese Benachrichtigung nur, wenn die Funktion CDialog::OnInitDialog()
	// �berschrieben wird, um die EM_SETEVENTMASK-Nachricht an das Steuerelement
	// mit dem ENM_UPDATE-Attribut Ored in die Maske lParam zu senden.
	m_Ctrl_Code.GetWindowText(m_Code);

	CString Urno = ogBCD.GetFieldExt("ODG", "ORGU", "ODGC", m_Orgu, m_Code, "URNO");

	if (!Urno.IsEmpty())
	{
		SetOdg(Urno);	
	}
	else
	{
		m_Ctrl_Odgn.SetWindowText("");
		m_Ctrl_Rema.SetWindowText("");
	}

	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen
	
}

int COdg_Dlg::ReadOdg()
{
	char querystr[1024];

	ogBCD.SetObject("ODG","URNO,ODGC,ODGN,ORGU,REMA");
		
	im_O_UrnoPos=ogBCD.GetFieldIndex("ODG","URNO");	// Eindeutige Datensatz-Nr.
	im_O_OdgcPos=ogBCD.GetFieldIndex("ODG","ODGC");	// Codereferenz ORG.CODE
	im_O_OdgnPos=ogBCD.GetFieldIndex("ODG","ODGN");	// G�ltig von
	im_O_OrguPos=ogBCD.GetFieldIndex("ODG","ORGU");	// G�ltig bis
	im_O_RemaPos=ogBCD.GetFieldIndex("ODG","REMA");	// Kostenstelle

	sprintf(querystr,"WHERE ORGU='%s'",m_Orgu);
//	CString Urno = ogBCD.GetFieldExt("ODG", "ORGU", "ODGC", m_Orgu, m_Code, "URNO");

	ogBCD.Read("ODG",querystr);

	return ogBCD.GetDataCount("ODG");
}

bool COdg_Dlg::SetOdg(CString Urno)
{

	CString olTxt = "";

//	olTxt = ogBCD.GetField("ODG", "URNO", Urno, "ODGC");
//	m_Ctrl_Code.SetWindowText(olTxt);
	olTxt = ogBCD.GetField("ODG", "URNO", Urno, "ODGN");
	m_Ctrl_Odgn.SetWindowText(olTxt);
	olTxt = ogBCD.GetField("ODG", "URNO", Urno, "REMA");
	m_Ctrl_Rema.SetWindowText(olTxt);

return true;
}

void COdg_Dlg::SaveOdg()
{
	int       cnt	= ogBCD.GetFieldCount("ODG");// to init recordset	
	bool	blerr	= false;						// return value
	RecordSet olTRecord(cnt);						// init recordset
	CString olTxt;

	m_Ctrl_Odgn.GetWindowText(olTxt);
	olTRecord.Values[im_O_OdgnPos] = olTxt;
	m_Ctrl_Rema.GetWindowText(olTxt);
	olTRecord.Values[im_O_RemaPos] = olTxt;

//	m_Ctrl_Code.GetWindowText(m_Code);
//	int i = m_CB_ODG_Code.GetCurSel();
//	if (i >= 0)
//	{
//		m_Code = ogBCD.GetField("ODG", i,"ODGC");
//	}

	
	olTRecord.Values[im_O_OdgcPos] = m_Code;
//	m_Ctrl_Code.GetWindowText(m_Orgu);
	olTRecord.Values[im_O_OrguPos] = m_Orgu;


	CString Urno = ogBCD.GetFieldExt("ODG", "ORGU", "ODGC", m_Orgu, m_Code, "URNO");

	if (!Urno.IsEmpty())
	{
		olTRecord.Values[im_O_UrnoPos] = Urno;
		ogBCD.SetRecord("ODG","URNO",Urno,olTRecord.Values,true);
	}
	else
	{	//NEW
		ogBCD.InsertRecord("ODG", olTRecord, true);
	}
}

void COdg_Dlg::InitCombo()
{
	CString opCODE;

//	CString Urno = ogBCD.GetFieldExt("ODG", "ORGU", "ODGC", m_Orgu, m_Code, "URNO");

	while (m_CB_ODG_Code.GetCount() > 0) m_CB_ODG_Code.DeleteString(0);

	for(int i=0;i<ogBCD.GetDataCount("ODG");i++)
	{
//		CString Urno = pomORGUrno->GetAt(i);
		opCODE = ogBCD.GetField("ODG",i,"ODGC");
		m_CB_ODG_Code.AddString(opCODE);
	}


}

void COdg_Dlg::OnSelchangeCombo1() 
{
CString Urno = "";

	int i = m_CB_ODG_Code.GetCurSel();
	if (i >= 0)
	{
		m_Code = ogBCD.GetField("ODG", i,"ODGC");
		Urno = ogBCD.GetField("ODG", i,"URNO");
	}


//	CString Urno = ogBCD.GetFieldExt("ODG", "ORGU", "ODGC", m_Orgu, m_Code, "URNO");

	m_gb_NewCode = false;

	if (!Urno.IsEmpty())
	{
		SetOdg(Urno);	
	}
	else
	{
		m_Ctrl_Odgn.SetWindowText("");
		m_Ctrl_Rema.SetWindowText("");
	}


}

void COdg_Dlg::OnEditchangeCombo1() 
{
	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen
	int i = m_CB_ODG_Code.GetCurSel();
	m_CB_ODG_Code.GetWindowText(m_Code);

	m_gb_NewCode = true;

	m_Ctrl_Odgn.SetWindowText("");
	m_Ctrl_Rema.SetWindowText("");

}
