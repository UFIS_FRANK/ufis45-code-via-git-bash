// ChkInCounterClass.cpp : implementation file
//

#include <stdafx.h>
#include <bdpsuif.h>
#include <ChkInCounterClass.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChkInCounterClass dialog
//---------------------------------------------------------------------------

CChkInCounterClass::CChkInCounterClass(CCCDATA *prpCcc,CWnd* pParent /*=NULL*/)
	: CDialog(CChkInCounterClass::IDD, pParent)
{
	pomCcc = prpCcc;
	//{{AFX_DATA_INIT(CChkInCounterClass)
	//}}AFX_DATA_INIT
}


void CChkInCounterClass::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChkInCounterClass)
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CdatD);
	DDX_Control(pDX, IDC_CDAT_T, m_CdatT);
	DDX_Control(pDX, IDC_CLASSCODE, m_Cicc);
	DDX_Control(pDX, IDC_CLASSNAME, m_Cicn);
	DDX_Control(pDX, IDC_LSTU_D, m_LstuD);
	DDX_Control(pDX, IDC_LSTU_T, m_LstuT);
	DDX_Control(pDX, IDC_USEC, m_Usec);
	DDX_Control(pDX, IDC_USEU, m_Useu);
	DDX_Control(pDX, IDC_VAFR_D, m_VaFrD);
	DDX_Control(pDX, IDC_VAFR_T, m_VaFrT);
	DDX_Control(pDX, IDC_VATO_D, m_VaToD);
	DDX_Control(pDX, IDC_VATO_T, m_VaToT);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CChkInCounterClass, CDialog)
	//{{AFX_MSG_MAP(CChkInCounterClass)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChkInCounterClass message handlers
//---------------------------------------------------------------------------

BOOL CChkInCounterClass::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_Caption = LoadStg(IDS_STRING202) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	char clStat = ogPrivList.GetStat("COUNTERCLASSDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CdatD.SetBKColor(SILVER);
	m_CdatD.SetInitText(pomCcc->Cdat.Format("%d.%m.%Y"));
	//m_CdatD.SetSecState(ogPrivList.GetStat("COUNTERCLASSDLG.m_CDAT"));
	m_CdatT.SetBKColor(SILVER);
	m_CdatT.SetInitText(pomCcc->Cdat.Format("%H:%M"));
	//m_CdatT.SetSecState(ogPrivList.GetStat("COUNTERCLASSDLG.m_CDAT"));
	//------------------------------------
	m_LstuD.SetBKColor(SILVER);
	m_LstuD.SetInitText(pomCcc->Lstu.Format("%d.%m.%Y"));
	//m_LstuD.SetSecState(ogPrivList.GetStat("COUNTERCLASSDLG.m_LSTU"));
	m_LstuT.SetBKColor(SILVER);
	m_LstuT.SetInitText(pomCcc->Lstu.Format("%H:%M"));
	//m_LstuT.SetSecState(ogPrivList.GetStat("COUNTERCLASSDLG.m_LSTU"));
	//------------------------------------
	m_Usec.SetBKColor(SILVER);
	m_Usec.SetInitText(pomCcc->Usec);
	//m_Usec.SetSecState(ogPrivList.GetStat("COUNTERCLASSDLG.m_USEC"));
	//------------------------------------
	m_Useu.SetBKColor(SILVER);
	m_Useu.SetInitText(pomCcc->Useu);
	//m_Useu.SetSecState(ogPrivList.GetStat("COUNTERCLASSDLG.m_USEU"));
	//------------------------------------
	m_Cicc.SetFormat("x|#x|#x|#");
	m_Cicc.SetTextLimit(1,3);
	m_Cicc.SetBKColor(YELLOW);
	m_Cicc.SetTextErrColor(RED);
	m_Cicc.SetInitText(pomCcc->Cicc);
	//m_Cicc.SetSecState(ogPrivList.GetStat("COUNTERCLASSDLG.m_CICC"));
	//------------------------------------
	m_Cicn.SetTypeToString("X(32)",32,1);
	m_Cicn.SetBKColor(YELLOW);
	m_Cicn.SetTextErrColor(RED);
	m_Cicn.SetInitText(pomCcc->Cicn);
	//m_Cicn.SetSecState(ogPrivList.GetStat("COUNTERCLASSDLG.m_CICN"));
	//------------------------------------
	m_VaFrD.SetTypeToDate(true);
	m_VaFrD.SetTextErrColor(RED);
	m_VaFrD.SetBKColor(YELLOW);
	m_VaFrD.SetInitText(pomCcc->Vafr.Format("%d.%m.%Y"));
	//m_VaFrD.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_VAFR"));
	// - - - - - - - - - - - - - - - - - -
	m_VaFrT.SetTypeToTime(true);
	m_VaFrT.SetTextErrColor(RED);
	m_VaFrT.SetBKColor(YELLOW);
	m_VaFrT.SetInitText(pomCcc->Vafr.Format("%H:%M"));
	//m_VaFrT.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_VAFR"));
	//------------------------------------
	m_VaToD.SetTypeToDate();
	m_VaToD.SetTextErrColor(RED);
	m_VaToD.SetInitText(pomCcc->Vato.Format("%d.%m.%Y"));
	//m_VaToD.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_VATO"));
	// - - - - - - - - - - - - - - - - - -
	m_VaToT.SetTypeToTime();
	m_VaToT.SetTextErrColor(RED);
	m_VaToT.SetInitText(pomCcc->Vato.Format("%H:%M"));
	//m_VaToT.SetSecState(ogPrivList.GetStat("AIRPORTDLG.m_VATO"));
	//------------------------------------

	return TRUE;
}

//---------------------------------------------------------------------------

void CChkInCounterClass::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_Cicc.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING609) + ogNotFormat;
	}
	if(m_Cicn.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING610) + ogNotFormat;// + CString(" 2")
	}
	if(m_VaFrD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VaFrD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VaFrT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VaFrT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}
	if(m_VaToD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_VaToT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
	}

	///////////////////////////////////////////////////////////////////////////
	CString olVafrd,olVafrt,olVatod,olVatot;
	CTime olTmpTimeFr,olTmpTimeTo;
	m_VaFrD.GetWindowText(olVafrd);
	m_VaFrT.GetWindowText(olVafrt);
	m_VaToD.GetWindowText(olVatod);
	m_VaToT.GetWindowText(olVatot);

	if((m_VaFrD.GetWindowTextLength() != 0 && m_VaFrT.GetWindowTextLength() == 0) || (m_VaFrD.GetWindowTextLength() == 0 && m_VaFrT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING234);
	}
	if((m_VaToD.GetWindowTextLength() != 0 && m_VaToT.GetWindowTextLength() == 0) || (m_VaToD.GetWindowTextLength() == 0 && m_VaToT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING235);
	}
	if(m_VaFrD.GetStatus() == true && m_VaFrT.GetStatus() == true && m_VaFrD.GetStatus() == true && m_VaFrT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olVafrd,olVafrt);
		olTmpTimeTo = DateHourStringToDate(olVatod,olVatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
	}

	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_Cicc.GetWindowText(pomCcc->Cicc,4);
		m_Cicn.GetWindowText(pomCcc->Cicn,31);
		pomCcc->Vafr = DateHourStringToDate(olVafrd,olVafrt);
		pomCcc->Vato = DateHourStringToDate(olVatod,olVatot);
		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_Cicc.SetFocus();
		m_Cicc.SetSel(0,-1);
	}
}

//---------------------------------------------------------------------------

void CChkInCounterClass::OnCancel() 
{
	CDialog::OnCancel();
}

//---------------------------------------------------------------------------
