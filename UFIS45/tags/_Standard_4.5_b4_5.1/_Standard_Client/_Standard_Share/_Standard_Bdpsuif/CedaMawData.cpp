// CedaMawData.cpp
 
#include "stdafx.h"
#include "CedaMawData.h"
#include "resource.h"


void ProcessMawCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaMawData::CedaMawData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for BSDDataStruct
	BEGIN_CEDARECINFO(MAWDATA,BSDDataRecInfo)
		FIELD_CHAR_TRIM	(Dflt,"DFLT")
		FIELD_CHAR_TRIM	(Hopo,"HOPO")
		FIELD_CHAR_TRIM	(Name,"NAME")
		FIELD_LONG	(Urno,"URNO")
		//FIELD_CHAR_TRIM	(Year,"YEAR")
	END_CEDARECINFO //(BSDDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(BSDDataRecInfo)/sizeof(BSDDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&BSDDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"MAW");
	strcpy(pcmListOfFields,"DFLT,HOPO,NAME,URNO"); //,YEAR");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaMawData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("DFLT");
	ropFields.Add("HOPO");
	ropFields.Add("NAME");
	ropFields.Add("URNO");
	ropFields.Add("YEAR");

	ropDesription.Add(LoadStg(IDS_STRING1042));
	ropDesription.Add(LoadStg(IDS_STRING711));
	ropDesription.Add(LoadStg(IDS_STRING1043));
	ropDesription.Add(LoadStg(IDS_STRING446));
	ropDesription.Add(LoadStg(IDS_STRING339));
	
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Long");
	ropType.Add("String");
	
}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaMawData::Register(void)
{
	ogDdx.Register((void *)this,BC_MAW_CHANGE,	CString("MAWDATA"), CString("Maw-changed"),	ProcessMawCf);
	ogDdx.Register((void *)this,BC_MAW_NEW,		CString("MAWDATA"), CString("Maw-new"),		ProcessMawCf);
	ogDdx.Register((void *)this,BC_MAW_DELETE,	CString("MAWDATA"), CString("Maw-deleted"),	ProcessMawCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaMawData::~CedaMawData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaMawData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaMawData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		MAWDATA *prlMaw = new MAWDATA;
		if ((ilRc = GetFirstBufferRecord(prlMaw)) == true)
		{
			omData.Add(prlMaw);//Update omData
			omUrnoMap.SetAt((void *)prlMaw->Urno,prlMaw);
		}
		else
		{
			delete prlMaw;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaMawData::Insert(MAWDATA *prpMaw)
{
	prpMaw->IsChanged = DATA_NEW;
	if(Save(prpMaw) == false) return false; //Update Database
	InsertInternal(prpMaw);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaMawData::InsertInternal(MAWDATA *prpMaw)
{
	ogDdx.DataChanged((void *)this, MAW_NEW,(void *)prpMaw ); //Update Viewer
	omData.Add(prpMaw);//Update omData
	omUrnoMap.SetAt((void *)prpMaw->Urno,prpMaw);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaMawData::Delete(long lpUrno)
{
	MAWDATA *prlMaw = GetMawByUrno(lpUrno);
	if (prlMaw != NULL)
	{
		prlMaw->IsChanged = DATA_DELETED;
		if(Save(prlMaw) == false) return false; //Update Database
		DeleteInternal(prlMaw);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaMawData::DeleteInternal(MAWDATA *prpMaw)
{
	ogDdx.DataChanged((void *)this,MAW_DELETE,(void *)prpMaw); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpMaw->Urno);
	int ilMawCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilMawCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpMaw->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaMawData::Update(MAWDATA *prpMaw)
{
	if (GetMawByUrno(prpMaw->Urno) != NULL)
	{
		if (prpMaw->IsChanged == DATA_UNCHANGED)
		{
			prpMaw->IsChanged = DATA_CHANGED;
		}
		if(Save(prpMaw) == false) return false; //Update Database
		UpdateInternal(prpMaw);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaMawData::UpdateInternal(MAWDATA *prpMaw)
{
	MAWDATA *prlMaw = GetMawByUrno(prpMaw->Urno);
	if (prlMaw != NULL)
	{
		*prlMaw = *prpMaw; //Update omData
		ogDdx.DataChanged((void *)this,MAW_CHANGE,(void *)prlMaw); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

MAWDATA *CedaMawData::GetMawByUrno(long lpUrno)
{
	MAWDATA  *prlMaw;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlMaw) == TRUE)
	{
		return prlMaw;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaMawData::ReadSpecialData(CCSPtrArray<MAWDATA> *popMaw,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","MAW",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","MAW",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popMaw != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			MAWDATA *prpMaw = new MAWDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpMaw,CString(pclFieldList))) == true)
			{
				popMaw->Add(prpMaw);
			}
			else
			{
				delete prpMaw;
			}
		}
		if(popMaw->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaMawData::Save(MAWDATA *prpMaw)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpMaw->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpMaw->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpMaw);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpMaw->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpMaw->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpMaw);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpMaw->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpMaw->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessMawCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogMawData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaMawData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlMawData;
	prlMawData = (struct BcStruct *) vpDataPointer;
	MAWDATA *prlMaw;
	if(ipDDXType == BC_MAW_NEW)
	{
		prlMaw = new MAWDATA;
		GetRecordFromItemList(prlMaw,prlMawData->Fields,prlMawData->Data);
		if(ValidateMawBcData(prlMaw->Urno)) //Prf: 8795
		{
			InsertInternal(prlMaw);
		}
		else
		{
			delete prlMaw;
		}
	}
	if(ipDDXType == BC_MAW_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlMawData->Selection);
		prlMaw = GetMawByUrno(llUrno);
		if(prlMaw != NULL)
		{
			GetRecordFromItemList(prlMaw,prlMawData->Fields,prlMawData->Data);
		    if(ValidateMawBcData(prlMaw->Urno)) //Prf: 8795
			{
				UpdateInternal(prlMaw);
			}
			else
			{
				DeleteInternal(prlMaw);
			}
		}
	}
	if(ipDDXType == BC_MAW_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlMawData->Selection);

		prlMaw = GetMawByUrno(llUrno);
		if (prlMaw != NULL)
		{
			DeleteInternal(prlMaw);
		}
	}
}

//Prf: 8795
//--ValidateMawBcData--------------------------------------------------------------------------------------

bool CedaMawData::ValidateMawBcData(const long& lrpUrno)
{
	bool blValidateMawBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<MAWDATA> olMaws;
		if(!ReadSpecialData(&olMaws,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateMawBcData = false;
		}
	}
	return blValidateMawBcData;
}

//---------------------------------------------------------------------------------------------------------
