#ifndef __WAITINGROOMTABLEVIEWER_H__
#define __WAITINGROOMTABLEVIEWER_H__

#include <stdafx.h>
#include <CedaWROData.h>
#include <CedaSGMData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>

struct WAITINGROOMTABLE_LINEDATA
{
	long	Urno;
	CString	Wnam;
	CString	Gte1;
	CString	Gte2;
	CString	Brca;
	CString	Term;
	CString	Tele;
	CString	Vafr;
	CString	Vato;
	CString	Wrot;
	CString	Shgn;
};

/////////////////////////////////////////////////////////////////////////////
// WaitingroomTableViewer

class WaitingroomTableViewer : public CViewer
{
// Constructions
public:
    WaitingroomTableViewer(CCSPtrArray<WRODATA> *popData);
    ~WaitingroomTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	BOOL IsPassFilter(WRODATA *prpWaitingroom);
	int CompareWaitingroom(WAITINGROOMTABLE_LINEDATA *prpWaitingroom1, WAITINGROOMTABLE_LINEDATA *prpWaitingroom2);
    void MakeLines();
	void MakeLine(WRODATA *prpWaitingroom);
// Operations
public:
	BOOL FindLine(long lpUrno, int &rilLineno);
	void DeleteAll();
	void CreateLine(WAITINGROOMTABLE_LINEDATA *prpWaitingroom);
	void DeleteLine(int ipLineno);
	bool CreateExcelFile(const CString& opTrenner,const CString& ropListSeparator);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(WAITINGROOMTABLE_LINEDATA *prpLine);
	void ProcessWaitingroomChange(WRODATA *prpWaitingroom);
	void ProcessWaitingroomDelete(WRODATA *prpWaitingroom);
	BOOL FindWaitingroom(char *prpWaitingroomKeya, char *prpWaitingroomKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	BOOL bmIsFromSearch;
// Attributes
private:
    CCSTable *pomWaitingroomTable;
	CCSPtrArray<WRODATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<WAITINGROOMTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(WAITINGROOMTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;
	CString	omFileName;

};

#endif //__WAITINGROOMTABLEVIEWER_H__
