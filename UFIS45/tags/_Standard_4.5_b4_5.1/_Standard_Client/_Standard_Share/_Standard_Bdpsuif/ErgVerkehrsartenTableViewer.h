#ifndef __ERGVERKEHRSARTENTABLEVIEWER_H__
#define __ERGVERKEHRSARTENTABLEVIEWER_H__

#include <stdafx.h>
#include <CedaSphData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct ERGVERKEHRSARTENTABLE_LINEDATA
{
	CString	Act3;	//Flugzeugtyp
	CString	Actm;	//Flugzeugtyp Gruppe
	CString	Alcm;	//Fluggesellschaft Gruppe
	CString	Apc3;	//Flughafen
	CString	Apcm;	//Flughafen Gruppe
	CString	Arde;	//Ankunft oder Abflug
	CString	Days;	//Verkehrstage
	CString	Flnc;	//Flugnummer Code
	CString	Flnn;	//Flugnummer Nummer
	CString	Flns;	//Flugnummer Suffix
	CString	Regn;	//Flugzeug-Kennzeichen
	CString	Styp;	//Servicetyp
	CString	Ttyp;	//Verkehrsartenschlüssel
	long	Urno;	//Eindeutige Datensatz-Nr.
};

/////////////////////////////////////////////////////////////////////////////
// ErgVerkehrsartenTableViewer

class ErgVerkehrsartenTableViewer : public CViewer
{
// Constructions
public:
    ErgVerkehrsartenTableViewer(CCSPtrArray<SPHDATA> *popData);
    ~ErgVerkehrsartenTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(SPHDATA *prpErgVerkehrsarten);
	int CompareErgVerkehrsarten(ERGVERKEHRSARTENTABLE_LINEDATA *prpErgVerkehrsarten1, ERGVERKEHRSARTENTABLE_LINEDATA *prpErgVerkehrsarten2);
    void MakeLines();
	void MakeLine(SPHDATA *prpErgVerkehrsarten);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(ERGVERKEHRSARTENTABLE_LINEDATA *prpErgVerkehrsarten);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(ERGVERKEHRSARTENTABLE_LINEDATA *prpLine);
	void ProcessErgVerkehrsartenChange(SPHDATA *prpErgVerkehrsarten);
	void ProcessErgVerkehrsartenDelete(SPHDATA *prpErgVerkehrsarten);
	bool FindErgVerkehrsarten(char *prpErgVerkehrsartenKeya, char *prpErgVerkehrsartenKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomErgVerkehrsartenTable;
	CCSPtrArray<SPHDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<ERGVERKEHRSARTENTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(ERGVERKEHRSARTENTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__ERGVERKEHRSARTENTABLEVIEWER_H__
