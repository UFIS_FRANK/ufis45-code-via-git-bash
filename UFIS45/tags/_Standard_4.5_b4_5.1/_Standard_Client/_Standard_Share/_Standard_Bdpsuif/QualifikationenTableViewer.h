#ifndef __QUALIFIKATIONENTABLEVIEWER_H__
#define __QUALIFIKATIONENTABLEVIEWER_H__

#include <stdafx.h>
#include <CedaPerData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct QUALIFIKATIONENTABLE_LINEDATA
{
	long	Urno;
	CString	Prio;
	CString	Prmc;
	CString	Prmn;
	CString	Rema;
};

/////////////////////////////////////////////////////////////////////////////
// QualifikationenTableViewer

class QualifikationenTableViewer : public CViewer
{
// Constructions
public:
    QualifikationenTableViewer(CCSPtrArray<PERDATA> *popData);
    ~QualifikationenTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(PERDATA *prpQualifikationen);
	int CompareQualifikationen(QUALIFIKATIONENTABLE_LINEDATA *prpQualifikationen1, QUALIFIKATIONENTABLE_LINEDATA *prpQualifikationen2);
    void MakeLines();
	void MakeLine(PERDATA *prpQualifikationen);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(QUALIFIKATIONENTABLE_LINEDATA *prpQualifikationen);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(QUALIFIKATIONENTABLE_LINEDATA *prpLine);
	void ProcessQualifikationenChange(PERDATA *prpQualifikationen);
	void ProcessQualifikationenDelete(PERDATA *prpQualifikationen);
	bool FindQualifikationen(char *prpQualifikationenKeya, char *prpQualifikationenKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomQualifikationenTable;
	CCSPtrArray<PERDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<QUALIFIKATIONENTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(QUALIFIKATIONENTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__QUALIFIKATIONENTABLEVIEWER_H__
