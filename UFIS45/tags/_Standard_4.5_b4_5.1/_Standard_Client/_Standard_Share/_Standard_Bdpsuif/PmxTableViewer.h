#ifndef __PMXTABLEVIEWER_H__
#define __PMXTABLEVIEWER_H__

#include <stdafx.h>
#include <CedaPmxData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct PMXTABLE_LINEDATA
{
	long 	Urno;
	CString	Pomo;
	CString	Pofr;
	CString	Posa;
	CString	Posu;
	CString	Poth;
	CString	Potu;
	CString	Powe;
	CString	Tifr;
	//uhi 7.5.01
	CString	Vafr;
	CString	Vato;
};

/////////////////////////////////////////////////////////////////////////////
// PmxTableViewer

class PmxTableViewer : public CViewer
{
// Constructions
public:
    PmxTableViewer(CCSPtrArray<PMXDATA> *popData);
    ~PmxTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(PMXDATA *prpPmx);
	int ComparePmx(PMXTABLE_LINEDATA *prpPmx1, PMXTABLE_LINEDATA *prpPmx2);
    void MakeLines();
	void MakeLine(PMXDATA *prpPmx);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(PMXTABLE_LINEDATA *prpPmx);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(PMXTABLE_LINEDATA *prpLine);
	void ProcessPmxChange(PMXDATA *prpPmx);
	void ProcessPmxDelete(PMXDATA *prpPmx);
	bool FindPmx(char *prpPmxKeya, char *prpPmxKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomPmxTable;
	CCSPtrArray<PMXDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<PMXTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(PMXTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__PMXTABLEVIEWER_H__
