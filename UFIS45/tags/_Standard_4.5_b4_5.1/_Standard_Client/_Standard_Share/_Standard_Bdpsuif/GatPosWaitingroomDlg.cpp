// GatPosWaitingroomDlg.cpp : implementation file
//

#include "stdafx.h"
#include "bdpsuif.h"
#include "GatPosWaitingroomDlg.h"
#include <PrivList.h>
#include <GatPosNotAvailableDlg.h>
#include <GatPosNotAvailableDlgEx.h>
#include <CedaWROData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GatPosWaitingroomDlg dialog


GatPosWaitingroomDlg::GatPosWaitingroomDlg(WRODATA *popWRO,CWnd* pParent /*=NULL*/)
	: WaitingroomDlg(popWRO,GatPosWaitingroomDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(GatPosWaitingroomDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

GatPosWaitingroomDlg::GatPosWaitingroomDlg(WRODATA *popWRO,int ipDlg,CWnd* pParent /*=NULL*/) 
: WaitingroomDlg(popWRO,ipDlg, pParent)
{
	//{{AFX_DATA_INIT(GatPosWaitingroomDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void GatPosWaitingroomDlg::DoDataExchange(CDataExchange* pDX)
{
	WaitingroomDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(GatPosWaitingroomDlg)
	DDX_Control(pDX, IDC_BLOCKING_BITMAP, m_BlockingBitmap);
	DDX_Control(pDX, IDC_SHGN,	 m_SHGN);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(GatPosWaitingroomDlg, WaitingroomDlg)
	//{{AFX_MSG_MAP(GatPosWaitingroomDlg)
	ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK,	OnTableLButtonDblclk)
	ON_BN_CLICKED(IDC_NOAVNEW, OnNoavNew)
	ON_BN_CLICKED(IDC_NOAVCOPY,OnNoavCopy)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// GatPosWaitingroomDlg message handlers

void GatPosWaitingroomDlg::OnOK() 
{
	// TODO: Add extra validation here
	if (!UpdateData(TRUE))
		return;

	bool ilStatus = true;
	CString olErrorText;

	if (m_SHGN.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING895) + ogNotFormat;
	}

	if (ilStatus == true)
	{
		m_SHGN.GetWindowText(pomWRO->Shgn,2);
		this->pomWRO->Ibit = m_BlockingBitmap.GetCurSel();
		WaitingroomDlg::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_SHGN.SetFocus();
		m_SHGN.SetSel(0,-1);
	}
}

BOOL GatPosWaitingroomDlg::OnInitDialog() 
{
	WaitingroomDlg::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_SHGN.SetFormat("X(1)");
	m_SHGN.SetTextLimit(0,1);
	m_SHGN.SetTextErrColor(RED);
	m_SHGN.SetInitText(pomWRO->Shgn);
//	m_SHGN.SetSecState(ogPrivList.GetStat("WAITINGROOMDLG.m_SHGN"));

	ogBasicData.AddBlockingImages(m_BlockingBitmap);
	m_BlockingBitmap.SetCurSel(this->pomWRO->Ibit);
	ogBasicData.SetWindowStat("WAITINGROOMDLG.m_IBIT",&m_BlockingBitmap);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

//----------------------------------------------------------------------------------------

LONG GatPosWaitingroomDlg::OnTableLButtonDblclk(UINT wParam, LONG lParam) 
{
	if(ogPrivList.GetStat("WAITINGROOMDLG.m_NOAVCHA") == '1')
	{
		int ilLineNo = pomTable->GetCurSel();
		if (ilLineNo == -1)
		{
			MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONSTOP);
		}
		else
		{
			BLKDATA *prlBlk = &omBlkPtrA[ilLineNo];
			if (prlBlk != NULL)
			{
				BLKDATA rlBlk = *prlBlk;
				GatPosNotAvailableDlg *polDlg = new GatPosNotAvailableDlg(&rlBlk,this);
				m_WNAM.GetWindowText(polDlg->omName);
				if(polDlg->DoModal() == IDOK)
				{
					*prlBlk = rlBlk;
					if(prlBlk->IsChanged != DATA_NEW) prlBlk->IsChanged = DATA_CHANGED;
					ChangeNoavTable(prlBlk, ilLineNo);
				}
				delete polDlg;
			}
		}
	}
	return 0L;
}

//----------------------------------------------------------------------------------------

void GatPosWaitingroomDlg::OnNoavNew() 
{
	BLKDATA rlBlk;

	if(ogPrivList.GetStat("WAITINGROOMDLG.m_NOAVNEW") == '1')
	{
		GatPosNotAvailableDlg *polDlg = new GatPosNotAvailableDlg(&rlBlk,this);
		m_WNAM.GetWindowText(polDlg->omName);
		if(polDlg->DoModal() == IDOK)
		{
			BLKDATA *prlBlk = new BLKDATA;
			*prlBlk = rlBlk;
			omBlkPtrA.Add(prlBlk);
			prlBlk->IsChanged = DATA_NEW;
			ChangeNoavTable(prlBlk, -1);
		}
		delete polDlg;
	}
}

//----------------------------------------------------------------------------------------

void GatPosWaitingroomDlg::OnNoavCopy() 
{
	int ilLineNo = pomTable->GetCurSel();
	if (ilLineNo == -1)
	{
		MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONSTOP);
	}
	else
	{
		if(ogPrivList.GetStat("WAITINGROOMDLG.m_NOAVCOPY") == '1')
		{
			BLKDATA rlBlk;
			rlBlk = omBlkPtrA[ilLineNo];

			GatPosNotAvailableDlgEx *polDlg = new GatPosNotAvailableDlgEx(&rlBlk,this);
			CString olSource;
			m_WNAM.GetWindowText(olSource);
			CStringArray olDestinations;
			ogWROData.GetWROList(olDestinations,olSource);
			polDlg->SetNames(olDestinations);

			if (polDlg->DoModal() == IDOK)
			{
				olDestinations.RemoveAll();
				polDlg->GetNames(olDestinations);
				for (int i = 0; i < olDestinations.GetSize(); i++)
				{
					BLKDATA *prlBlk = new BLKDATA;
					*prlBlk = rlBlk;

					WRODATA *prlWro = ogWROData.GetWROByName(olDestinations[i]);
					if (prlWro)
					{
						prlBlk->Burn = prlWro->Urno;
						prlBlk->Urno = ogBasicData.GetNextUrno();	//new URNO
						strcpy(prlBlk->Tabn,"WRO");
						ogBlkData.Insert(prlBlk);
					}
				}
			}
			delete polDlg;
		}
	}
}
