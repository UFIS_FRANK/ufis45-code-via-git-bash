// GatPosNotAvailableDlg.cpp : implementation file
//

#include "stdafx.h"
#include "bdpsuif.h"
#include "GatPosNotAvailableDlg.h"
#include <BasicData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GatPosNotAvailableDlg dialog


GatPosNotAvailableDlg::GatPosNotAvailableDlg(BLKDATA *popBlk,CWnd* pParent /*=NULL*/)
	: NotAvailableDlg(popBlk,GatPosNotAvailableDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(GatPosNotAvailableDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void GatPosNotAvailableDlg::DoDataExchange(CDataExchange* pDX)
{
	NotAvailableDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(GatPosNotAvailableDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	DDX_Control(pDX, IDC_BLOCKING_BITMAP, m_BlockingBitmap);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(GatPosNotAvailableDlg, NotAvailableDlg)
	//{{AFX_MSG_MAP(GatPosNotAvailableDlg)
	ON_BN_CLICKED(IDC_RADIO_LOCAL, OnRadioLocal)
	ON_BN_CLICKED(IDC_RADIO_UTC, OnRadioUtc)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// GatPosNotAvailableDlg message handlers

BOOL GatPosNotAvailableDlg::OnInitDialog() 
{
	NotAvailableDlg::OnInitDialog();
	
	// TODO: Add extra initialization here
	ogBasicData.AddBlockingImages(m_BlockingBitmap);
	m_BlockingBitmap.SetCurSel(this->pomBlk->Ibit);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void GatPosNotAvailableDlg::OnOK() 
{
	// TODO: Add extra validation here
	this->pomBlk->Ibit = m_BlockingBitmap.GetCurSel();
	
	NotAvailableDlg::OnOK();
}

void GatPosNotAvailableDlg::OnRadioLocal() 
{
	// TODO: Add your control notification handler code here
	NotAvailableDlg::OnLocal();	
}

void GatPosNotAvailableDlg::OnRadioUtc() 
{
	// TODO: Add your control notification handler code here
	NotAvailableDlg::OnUTC();	
	
}
