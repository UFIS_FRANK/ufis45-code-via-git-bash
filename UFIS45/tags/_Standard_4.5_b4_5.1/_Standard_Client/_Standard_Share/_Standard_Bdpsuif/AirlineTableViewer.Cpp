// AirlineTableViewer.cpp 
//

#include <stdafx.h>
#include <AirlineTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>
#include <CedaHaiData.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void AirlineTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// AirlineTableViewer
//

AirlineTableViewer::AirlineTableViewer(CCSPtrArray<ALTDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomAirlineTable = NULL;
    ogDdx.Register(this, ALT_CHANGE, CString("AIRLINETABLEVIEWER"), CString("Airline Update/new"), AirlineTableCf);
    ogDdx.Register(this, ALT_DELETE, CString("AIRLINETABLEVIEWER"), CString("Airline Delete"), AirlineTableCf);
	bmUseHandlingAgentFilter = false;
}

//-----------------------------------------------------------------------------------------------

AirlineTableViewer::~AirlineTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void AirlineTableViewer::Attach(CCSTable *popTable)
{
    pomAirlineTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void AirlineTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}


void AirlineTableViewer::RemoveHandlingAgent(CString &ropWhere)
{
	bmUseHandlingAgentFilter = false;
	if (ropWhere.Find("HSNA") == -1 && ropWhere.Find("TASK") == -1)
	{
		return;
	}
	int actualIndex = 1; 

	omConditions.DeleteAll();
	bmUseHandlingAgentFilter = true;

	while(true)
	{
		FILTERCONDITIONS *tmpCon = new FILTERCONDITIONS;
		tmpCon->column = ropWhere.Mid(actualIndex,4);
		actualIndex += 5;
		int blankIndex = ropWhere.Find(' ',actualIndex);
		if (blankIndex == -1)
		{
			free(tmpCon); 
			break;
		}
		tmpCon->condition = ropWhere.Mid(actualIndex,blankIndex-actualIndex);
		actualIndex = blankIndex+1;
	
		int quoteIndex = ropWhere.Find('\'',actualIndex+1);
		if (quoteIndex == -1)
		{
			return;
		}
		
		tmpCon->value = ropWhere.Mid(actualIndex,(quoteIndex-actualIndex)+1);
		actualIndex = quoteIndex+1;
		if(ropWhere.GetAt(actualIndex) == '@')
		{
			omConditions.Add(tmpCon);
			break;
		}
		actualIndex += 1;

		blankIndex = ropWhere.Find(' ',actualIndex);
		if (blankIndex == -1)
		{
			return;
		}
		tmpCon->op = ropWhere.Mid(actualIndex,(blankIndex-actualIndex));
		actualIndex = blankIndex+1;
		omConditions.Add(tmpCon);
	}
	ropWhere.Empty();
	ropWhere += '(';
	CString olHaiSel = "WHERE ( ";
	bool blFoundSel = false;
	for (int i = 0; i < omConditions.GetSize(); i++)
	{
		CString tmpCol = omConditions[i].column;
		if (omConditions[i].column == "HSNA" || omConditions[i].column == "TASK")
		{
			olHaiSel += " ";
			olHaiSel += omConditions[i].column;
			olHaiSel += " ";
			olHaiSel += omConditions[i].condition;
			olHaiSel += " ";

			olHaiSel += omConditions[i].value;
			olHaiSel += " ";
			olHaiSel += omConditions[i].op;
		}
		else
		{
			ropWhere += " ";
			ropWhere += omConditions[i].column;
			ropWhere += " ";
			ropWhere += omConditions[i].condition;
			ropWhere += " ";

			ropWhere += omConditions[i].value;
			ropWhere += " ";
			ropWhere += omConditions[i].op;
			blFoundSel = true;
		}

	}
	int quotePos = olHaiSel.ReverseFind('\'');
	if (quotePos != -1)
		olHaiSel = olHaiSel.Left(quotePos+1);
	quotePos = ropWhere.ReverseFind('\'');
	if (quotePos != -1)
		ropWhere = ropWhere.Left(quotePos+1);

	olHaiSel += ')';
	if (blFoundSel)
		ropWhere += "@)";
	else
		ropWhere += ")";
	if (bmUseHandlingAgentFilter)
	{
		const char *tmpSel = (LPCSTR) olHaiSel;
		ogHaiData.Read(tmpSel);
	}

}

//-----------------------------------------------------------------------------------------------
bool AirlineTableViewer::GetBDPSWhere(CString &ropWhere)
{
	CStringArray olValues;
    
	GetFilter("UNIFILTER", olValues);
	if(olValues.GetSize() > 0)
	{
		ropWhere += CString("(") + olValues[0] + CString(")");
		if (ogBasicData.UseHandlingAgentFilter() == true) 
		{
			RemoveHandlingAgent(ropWhere);
		}
	}
	if(ropWhere.IsEmpty())
	{
		return false;
	}
	if(ropWhere == "()")
	{
		ropWhere = CString("");
		return false;
	}
	bool blFound = true;
	
	//replace '@' with ' '
	while(blFound == true)
	{
		int pos = ropWhere.Find("@");
		if(pos != -1)
			ropWhere.SetAt(pos, ' ');
		else
			blFound = false;
	}
	if(ropWhere.Find("nvl(regexp_substr") < 0)
		ropWhere.Replace('?','\'');

	return true;

}

void AirlineTableViewer::MakeLines()
{
	int ilAirlineCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilAirlineCount; ilLc++)
	{
		ALTDATA *prlAirlineData = &pomData->GetAt(ilLc);
		MakeLine(prlAirlineData);
	}
}

//-----------------------------------------------------------------------------------------------

void AirlineTableViewer::MakeLine(ALTDATA *prpAirline)
{

	if (bmUseHandlingAgentFilter)
	{
		if( !IsPassFilter(prpAirline->Urno)) return;
	}

    // Update viewer data for this shift record
    AIRLINETABLE_LINEDATA rlAirline;

	rlAirline.Urno = prpAirline->Urno; 
	rlAirline.Alc2 = prpAirline->Alc2; 
	rlAirline.Alc3 = prpAirline->Alc3; 
	rlAirline.Akey = prpAirline->Akey; 
	rlAirline.Alfn = prpAirline->Alfn; 
	rlAirline.Rprt = prpAirline->Rprt; 
	rlAirline.Wrko = prpAirline->Wrko; 
	rlAirline.Admd = prpAirline->Admd; 
	rlAirline.Term = prpAirline->Term; 
	rlAirline.Cash = prpAirline->Cash; 
	rlAirline.Vafr = prpAirline->Vafr.Format("%d.%m.%Y %H:%M"); 
	rlAirline.Vato = prpAirline->Vato.Format("%d.%m.%Y %H:%M"); 
	rlAirline.Add1 = prpAirline->Add1;
	rlAirline.Add2 = prpAirline->Add2;
	rlAirline.Add3 = prpAirline->Add3;
	rlAirline.Add4 = prpAirline->Add4;
	rlAirline.Base = prpAirline->Base;
	rlAirline.Cont = prpAirline->Cont;
	rlAirline.Ctry = prpAirline->Ctry;
	rlAirline.Emps = prpAirline->Emps;
	rlAirline.Exec = prpAirline->Exec;
	rlAirline.Fond = prpAirline->Fond;
	rlAirline.Iano = prpAirline->Iano;
	rlAirline.Iata = prpAirline->Iata;
	rlAirline.Ical = prpAirline->Ical;
	rlAirline.Icao = prpAirline->Icao;
	rlAirline.Lcod = prpAirline->Lcod;
	rlAirline.Phon = prpAirline->Phon;
	rlAirline.Self = prpAirline->Self;
	rlAirline.Sita = prpAirline->Sita;
	rlAirline.Telx = prpAirline->Telx;
	rlAirline.Text = prpAirline->Text;
	rlAirline.Tfax = prpAirline->Tfax;
	rlAirline.Webs = prpAirline->Webs;
	//rlAirline.Home = prpAirline->Home;
	rlAirline.Doin = prpAirline->Doin;
	
	CreateLine(&rlAirline);
}

//-----------------------------------------------------------------------------------------------

void AirlineTableViewer::CreateLine(AIRLINETABLE_LINEDATA *prpAirline)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareAirline(prpAirline, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	AIRLINETABLE_LINEDATA rlAirline;
	rlAirline = *prpAirline;
    omLines.NewAt(ilLineno, rlAirline);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void AirlineTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 10;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomAirlineTable->SetShowSelection(TRUE);
	pomAirlineTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING252),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// 2 letter code
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING252),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// 3 letter code
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING252),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// group
	rlHeader.Length = 350; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING252),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// name
	rlHeader.Length = 40; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING252),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// report
	rlHeader.Length = 40; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING252),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// order
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING252),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// admin
	rlHeader.Length = 60; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING252),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// pay mode
	rlHeader.Length = 60; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING252),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// terminals
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING252),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// valid from
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING252),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// valid to

	rlHeader.Length = 110; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING252),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// adr 1
	rlHeader.Length = 110; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING252),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// adr 2
	rlHeader.Length = 110; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING252),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// adr 3
	rlHeader.Length = 110; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING252),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// adr 4
	rlHeader.Length = 150;											
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING252),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// home airport
	rlHeader.Length = 40; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING252),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// cont
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING252),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// Country
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING252),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// Employee
	rlHeader.Length = 100; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING252),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// Execute
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING252),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// Founded
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING252),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// IATA-No
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING252),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// IATA
	rlHeader.Length = 70; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING252),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// ICAO-Call
	rlHeader.Length = 50; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING252),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// ICAO
	rlHeader.Length = 80; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING252),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// Leasing
	rlHeader.Length = 60; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING252),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// Phone no.
	rlHeader.Length = 80; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING252),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// Sita-Code
	rlHeader.Length = 80; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING252),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// Telex
	rlHeader.Length = 90; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING252),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// Remark
	rlHeader.Length = 80; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING252),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// Fax
	rlHeader.Length = 120; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING252),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// Internet
	rlHeader.Length = 20; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING252),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);	// D/I
	
	pomAirlineTable->SetHeaderFields(omHeaderDataArray);

	pomAirlineTable->SetDefaultSeparator();
	pomAirlineTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Alc2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Alc3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Akey;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Alfn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Rprt;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Wrko;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Admd;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Cash;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Term;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vafr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vato;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = omLines[ilLineNo].Add1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Add2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Add3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Add4;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Base;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Cont;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Ctry;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Emps;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Exec;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Fond;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Iano;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Iata;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Ical;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Icao;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Lcod;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Phon;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
//		rlColumnData.Text = omLines[ilLineNo].Self;
//		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Sita;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Telx;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Text;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Tfax;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Webs;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		/*rlColumnData.Text = omLines[ilLineNo].Home;
		olColList.NewAt(olColList.GetSize(), rlColumnData);*/
		rlColumnData.Text = omLines[ilLineNo].Doin;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		
		pomAirlineTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomAirlineTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString AirlineTableViewer::Format(AIRLINETABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool AirlineTableViewer::FindAirline(char *pcpAirlineKeya, char *pcpAirlineKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpAirlineKeya) &&
			 (omLines[ilItem].Keyd == pcpAirlineKeyd)    )
			return TRUE;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void AirlineTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    AirlineTableViewer *polViewer = (AirlineTableViewer *)popInstance;
    if (ipDDXType == ALT_CHANGE) polViewer->ProcessAirlineChange((ALTDATA *)vpDataPointer);
    if (ipDDXType == ALT_DELETE) polViewer->ProcessAirlineDelete((ALTDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void AirlineTableViewer::ProcessAirlineChange(ALTDATA *prpAirline)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpAirline->Alc2;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpAirline->Alc3;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpAirline->Akey;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpAirline->Alfn;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpAirline->Rprt;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpAirline->Wrko;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpAirline->Admd;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpAirline->Cash;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpAirline->Term;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpAirline->Vafr.Format("%d.%m.%Y %H:%M"); 
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpAirline->Vato.Format("%d.%m.%Y %H:%M"); 
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);


		rlColumn.Text = prpAirline->Add1;
	rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpAirline->Add2;
	rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpAirline->Add3;
	rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpAirline->Add4;
	rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpAirline->Base;
	rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpAirline->Cont;
	rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpAirline->Ctry;
	rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpAirline->Emps;
	rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpAirline->Exec;
	rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpAirline->Fond;
	rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpAirline->Iano;
	rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpAirline->Iata;
	rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpAirline->Ical;
	rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpAirline->Icao;
	rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpAirline->Lcod;
	rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpAirline->Phon;
	rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
//		rlColumn.Text = prpAirline->Self;
//	rlColumn.Columnno = ilColumnNo++;
//		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpAirline->Sita;
	rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpAirline->Telx;
	rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpAirline->Text;
	rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpAirline->Tfax;
	rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		rlColumn.Text = prpAirline->Webs;
	rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);
		
		/*rlColumn.Text = prpAirline->Home;
	rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);*/
		rlColumn.Text = prpAirline->Doin;
	rlColumn.Columnno = ilColumnNo++;
		olLine.NewAt(olLine.GetSize(), rlColumn);


	if (FindLine(prpAirline->Urno, ilItem))
	{
        AIRLINETABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpAirline->Urno;
		prlLine->Alc2 = prpAirline->Alc2;
		prlLine->Alc3 = prpAirline->Alc3;
		prlLine->Akey = prpAirline->Akey;
		prlLine->Alfn = prpAirline->Alfn;
		prlLine->Rprt = prpAirline->Rprt;
		prlLine->Wrko = prpAirline->Wrko;
		prlLine->Admd = prpAirline->Admd;
		prlLine->Cash = prpAirline->Cash;
		prlLine->Term = prpAirline->Term;
		prlLine->Vafr = prpAirline->Vafr.Format("%d.%m.%Y %H:%M");
		prlLine->Vato = prpAirline->Vato.Format("%d.%m.%Y %H:%M");
		prlLine->Add1 = prpAirline->Add1;
		prlLine->Add2 = prpAirline->Add2;
		prlLine->Add3 = prpAirline->Add3;
		prlLine->Add4 = prpAirline->Add4;
		prlLine->Base = prpAirline->Base;
		prlLine->Cont = prpAirline->Cont;
		prlLine->Ctry = prpAirline->Ctry;
		prlLine->Emps = prpAirline->Emps;
		prlLine->Exec = prpAirline->Exec;
		prlLine->Fond = prpAirline->Fond;
		prlLine->Iano = prpAirline->Iano;
		prlLine->Iata = prpAirline->Iata;
		prlLine->Ical = prpAirline->Ical;
		prlLine->Icao = prpAirline->Icao;
		prlLine->Lcod = prpAirline->Lcod;
		prlLine->Phon = prpAirline->Phon;
		prlLine->Self = prpAirline->Self;
		prlLine->Sita = prpAirline->Sita;
		prlLine->Telx = prpAirline->Telx;
		prlLine->Text = prpAirline->Text;
		prlLine->Tfax = prpAirline->Tfax;
		prlLine->Webs = prpAirline->Webs;
		//prlLine->Home = prpAirline->Home;
		prlLine->Doin = prpAirline->Doin;



		pomAirlineTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomAirlineTable->DisplayTable();
	}
	else
	{
		MakeLine(prpAirline);
		if (FindLine(prpAirline->Urno, ilItem))
		{
	        AIRLINETABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomAirlineTable->AddTextLine(olLine, (void *)prlLine);
				pomAirlineTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void AirlineTableViewer::ProcessAirlineDelete(ALTDATA *prpAirline)
{
	int ilItem;
	if (FindLine(prpAirline->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomAirlineTable->DeleteTextLine(ilItem);
		pomAirlineTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool AirlineTableViewer::IsPassFilter(long altu)
{
	if (ogHaiData.GetHaiByAltu(altu))
	return true;

	return false;
}

//-----------------------------------------------------------------------------------------------

int AirlineTableViewer::CompareAirline(AIRLINETABLE_LINEDATA *prpAirline1, AIRLINETABLE_LINEDATA *prpAirline2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpAirline1->Tifd == prpAirline2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpAirline1->Tifd > prpAirline2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void AirlineTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool AirlineTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void AirlineTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void AirlineTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING173);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	

//			calculate count of pages
			CUIntArray olColsPerPage;
			int ilPages = pomPrint->CalculateCountOfPages(this->omHeaderDataArray,olColsPerPage);

			pomPrint->omCdc.StartDoc( &rlDocInfo );
			for (int ilPage = 0; ilPage < ilPages; ilPage++)
			{
				pomPrint->imPageNo = 0;
				int ilLines = omLines.GetSize();
				pomPrint->imLineNo = pomPrint->imMaxLines + 1;
				olFooter1.Format("%d %s",ilLines,omTableName);
				for(int i = 0; i < ilLines; i++ ) 
				{
					if(pomPrint->imLineNo >= pomPrint->imMaxLines)
					{
						if(pomPrint->imPageNo > 0)
						{
							olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
							pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
							pomPrint->omCdc.EndPage();
						}
						PrintTableHeader(ilPage,olColsPerPage);
					}
					if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
					{
						PrintTableLine(ilPage,olColsPerPage,&omLines[i],true);
					}
					else
					{
						PrintTableLine(ilPage,olColsPerPage,&omLines[i],false);
					}
				}
				olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
				pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
				pomPrint->omCdc.EndPage();
			}
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool AirlineTableViewer::PrintTableHeader(int ilPage,CUIntArray& ropColsPerPage)
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		bool blColumn = false;
		if (ilPage == 0)
		{
			if (i < ropColsPerPage[ilPage])
			{
				blColumn = true;
			}
		}
		else
		{
			if (i >= ropColsPerPage[ilPage-1] && i < ropColsPerPage[ilPage])
			{
				blColumn = true;
			}
		}

		if (blColumn)
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool AirlineTableViewer::PrintTableLine(int ilPage,CUIntArray& ropColsPerPage,AIRLINETABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		bool blColumn = false;
		if (ilPage == 0)
		{
			if (i < ropColsPerPage[ilPage])
			{
				blColumn = true;
			}
		}
		else
		{
			if (i >= ropColsPerPage[ilPage-1] && i < ropColsPerPage[ilPage])
			{
				blColumn = true;
			}
		}

		if (blColumn)
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
			case 0:
				{
					rlElement.FrameLeft  = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Alc2;
				}
				break;
			case 1:
				{
					rlElement.Text		= prpLine->Alc3;
				}
				break;
			case 2:
				{
					rlElement.Text		= prpLine->Akey;
				}
				break;
			case 3:
				{
					rlElement.Text		= prpLine->Alfn;
				}
				break;
			case 4:
				{
					rlElement.FrameRight = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Rprt;
				}
				break;
			case 5:
				{
					rlElement.FrameRight = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Wrko;
				}
				break;
			case 6:
				{
					rlElement.FrameRight = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Admd;
				}
				break;
			case 7:
				{
					rlElement.Text		= prpLine->Cash;
				}
				break;
			case 8:
				{
					rlElement.Text		= prpLine->Term;
				}
				break;
			case 9:
				{
					rlElement.Text		= prpLine->Vafr;
				}
				break;
			case 10:
				{
					rlElement.FrameRight = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Vato;
				}
				break;
			case 11:
				{
					rlElement.FrameRight = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Add1;
				}
				break;
			case 12:
				{
					rlElement.FrameRight = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Add2;
				}
				break;
			case 13:
				{
					rlElement.FrameRight = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Add3;
				}
				break;
			case 14:
				{
					rlElement.FrameRight = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Add4;
				}
				break;
			case 15:
				{
					rlElement.Text		= prpLine->Base;
				}
				break;
			case 16:
				{
					rlElement.Text		= prpLine->Cont;
				}
				break;
			case 17:
				{
					rlElement.Text		= prpLine->Ctry;
				}
				break;
			case 18:
				{
					rlElement.Text		= prpLine->Emps;
				}
				break;
			case 19:
				{
					rlElement.Text		= prpLine->Exec;
				}
				break;
			case 20:
				{
					rlElement.Text		= prpLine->Fond;
				}
				break;
			case 21:
				{
					rlElement.Text		= prpLine->Iano;
				}
				break;
			case 22:
				{
					rlElement.Text		= prpLine->Iata;
				}
				break;
			case 23:
				{
					rlElement.Text		= prpLine->Ical;
				}
				break;
			case 24:
				{
					rlElement.Text		= prpLine->Icao;
				}
				break;
			case 25:
				{
					rlElement.Text		= prpLine->Lcod;
				}
				break;
			case 26:
				{
					rlElement.Text		= prpLine->Phon;
				}
				break;
			case 27:
				{
					rlElement.Text		= prpLine->Sita;
				}
				break;
			case 28:
				{
					rlElement.Text		= prpLine->Telx;
				}
				break;
			case 29:
				{
					rlElement.Text		= prpLine->Text;
				}
				break;
			case 30:
				{
					rlElement.Text		= prpLine->Tfax;
				}
				break;
			case 31:
				{
					rlElement.Text		= prpLine->Webs;
				}
				break;
			case 32:
				{
					rlElement.FrameRight = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Doin;
				}
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
