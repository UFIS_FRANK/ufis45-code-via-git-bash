#if !defined(AFX_EXTLMGATPOSGATEDLG_H__38E30495_4C56_446B_9E13_118B71EB706B__INCLUDED_)
#define AFX_EXTLMGATPOSGATEDLG_H__38E30495_4C56_446B_9E13_118B71EB706B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ExtLMGatPosGateDlg.h : header file
//
#include <GatPosGateDlg.h>
#include <ExtLMDatGrid.h>

/////////////////////////////////////////////////////////////////////////////
// ExtLMGatPosGateDlg dialog

class ExtLMGatPosGateDlg : public GatPosGateDlg
{
// Construction
public:
	ExtLMGatPosGateDlg(GATDATA *popGAT,long lpOriginalUrno,CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(ExtLMGatPosGateDlg)
	enum { IDD = IDD_EXTLM_GATPOS_GATEDLG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ExtLMGatPosGateDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(ExtLMGatPosGateDlg)
		// NOTE: the ClassWizard will add member functions here
		virtual void OnOK();
		virtual BOOL OnInitDialog();
		afx_msg void OnDatNew();
		afx_msg void OnDatDelete();
		afx_msg void OnDatCopy();
		afx_msg void OnDatAssign();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	ExtLMDatGrid	omGrid;
	long			lmOriginalUrno;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EXTLMGATPOSGATEDLG_H__38E30495_4C56_446B_9E13_118B71EB706B__INCLUDED_)
