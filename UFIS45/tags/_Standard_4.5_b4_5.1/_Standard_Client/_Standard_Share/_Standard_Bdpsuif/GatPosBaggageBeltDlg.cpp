// GatPosBaggageBeltDlg.cpp : implementation file
//

#include <stdafx.h>
#include <bdpsuif.h>
#include <GatPosBaggageBeltDlg.h>
#include <SelectionDlg.h>
#include <CedaExtData.h>
#include <BasicData.h>
#include <PrivList.h>
#include <GatPosNotAvailableDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// GatPosBaggageBeltDlg dialog


GatPosBaggageBeltDlg::GatPosBaggageBeltDlg(BLTDATA *popBLT,CWnd* pParent /*=NULL*/)
	: BeggagebeltDlg(popBLT,GatPosBaggageBeltDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(GatPosBaggageBeltDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void GatPosBaggageBeltDlg::DoDataExchange(CDataExchange* pDX)
{
	BeggagebeltDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(GatPosBaggageBeltDlg)
	DDX_Control(pDX, IDC_BLOCKING_BITMAP, m_BlockingBitmap);
	DDX_Control(pDX, IDC_EXITS_LIST, m_ConnectedExits);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(GatPosBaggageBeltDlg, BeggagebeltDlg)
	//{{AFX_MSG_MAP(GatPosBaggageBeltDlg)
	ON_BN_CLICKED(IDC_EXITS, OnExits)
	ON_CBN_SELENDOK(IDC_BLOCKING_BITMAP, OnSelendokBlockingBitmap)
	ON_CBN_SELCHANGE(IDC_BLOCKING_BITMAP, OnSelchangeBlockingBitmap)
	ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK,	OnTableLButtonDblclk)
	ON_BN_CLICKED(IDC_NOAVNEW, OnNoavNew)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// GatPosBaggageBeltDlg message handlers

BOOL GatPosBaggageBeltDlg::OnInitDialog() 
{
	BeggagebeltDlg::OnInitDialog();
	
	// TODO: Add extra initialization here
	ogEXTData.GetEXTList(omAvailableExits);
	ogBasicData.GetExitsForBaggageBelt(this->pomBLT->Urno,omConnectedExits);
	for (int i = 0; i < omConnectedExits.GetSize(); i++)
	{
		m_ConnectedExits.AddString(omConnectedExits[i]);
	}

	CWnd *polWnd = GetDlgItem(IDC_EXITS);
	if (polWnd)
		ogBasicData.SetWindowStat("BEGGAGEBELTDLG.m_EXIT",polWnd);
	
	ogBasicData.AddBlockingImages(m_BlockingBitmap);
	m_BlockingBitmap.SetCurSel(this->pomBLT->Ibit);

	ogBasicData.SetWindowStat("BEGGAGEBELTDLG.m_IBIT",&m_BlockingBitmap);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void GatPosBaggageBeltDlg::OnExits() 
{
	// TODO: Add your control notification handler code here
	SelectionDlg olDlg(this);
	CString olCaption;
	olCaption.Format(LoadStg(IDS_STRING899),this->pomBLT->Bnam);
	olDlg.SetCaption(olCaption);
	olDlg.SetPossibleList(LoadStg(IDS_STRING910),omAvailableExits);
	olDlg.SetSelectionList(LoadStg(IDS_STRING898),omConnectedExits);
	if (olDlg.DoModal() == IDOK)
	{
		omConnectedExits.RemoveAll();
		olDlg.GetSelectionList(omConnectedExits);
		m_ConnectedExits.ResetContent();
		for (int i = 0; i < omConnectedExits.GetSize(); i++)
		{
			m_ConnectedExits.AddString(omConnectedExits[i]);
		}
	}
	
}

int GatPosBaggageBeltDlg::GetConnectedExits(CStringArray& ropExits)
{
	ropExits.Append(omConnectedExits);
	return ropExits.GetSize();
}

void GatPosBaggageBeltDlg::OnSelendokBlockingBitmap() 
{
	// TODO: Add your control notification handler code here
}

void GatPosBaggageBeltDlg::OnSelchangeBlockingBitmap() 
{
	// TODO: Add your control notification handler code here
	
}

void GatPosBaggageBeltDlg::OnOK() 
{
	// TODO: Add extra validation here
	int ipIndex = m_BlockingBitmap.GetCurSel();	
	this->pomBLT->Ibit = ipIndex;
				
	BeggagebeltDlg::OnOK();
}

//----------------------------------------------------------------------------------------

LONG GatPosBaggageBeltDlg::OnTableLButtonDblclk(UINT wParam, LONG lParam) 
{
	if(ogPrivList.GetStat("BEGGAGEBELTDLG.m_NOAVCHA") == '1')
	{
		int ilLineNo = pomTable->GetCurSel();
		if (ilLineNo == -1)
		{
			MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONSTOP);
		}
		else
		{
			BLKDATA *prlBlk = &omBlkPtrA[ilLineNo];
			if (prlBlk != NULL)
			{
				BLKDATA rlBlk = *prlBlk;
				GatPosNotAvailableDlg *polDlg = new GatPosNotAvailableDlg(&rlBlk,this);
				m_BNAM.GetWindowText(polDlg->omName);
				if(polDlg->DoModal() == IDOK)
				{
					*prlBlk = rlBlk;
					if(prlBlk->IsChanged != DATA_NEW) prlBlk->IsChanged = DATA_CHANGED;
					ChangeNoavTable(prlBlk, ilLineNo);
				}
				delete polDlg;
			}
		}
	}
	return 0L;
}

//----------------------------------------------------------------------------------------

void GatPosBaggageBeltDlg::OnNoavNew() 
{
	
	BLKDATA rlBlk;
	if(ogPrivList.GetStat("BEGGAGEBELTDLG.m_NOAVNEW") == '1')
	{
		GatPosNotAvailableDlg *polDlg = new GatPosNotAvailableDlg(&rlBlk,this);
		m_BNAM.GetWindowText(polDlg->omName);
		if(polDlg->DoModal() == IDOK)
		{
			BLKDATA *prlBlk = new BLKDATA;
			*prlBlk = rlBlk;
			omBlkPtrA.Add(prlBlk);
			prlBlk->IsChanged = DATA_NEW;
			ChangeNoavTable(prlBlk, -1);
		}
		delete polDlg;
	}
}
