#ifndef __HOLIDAYTABLEVIEWER_H__
#define __HOLIDAYTABLEVIEWER_H__

#include <stdafx.h>
#include <CedaHolData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>

struct HOLIDAYTABLE_LINEDATA
{
	long	Urno;
	CString	Rema;
	CString	Type;
	CString	Hday;
};

/////////////////////////////////////////////////////////////////////////////
// HolidayTableViewer

class HolidayTableViewer : public CViewer
{
// Constructions
public:
    HolidayTableViewer(CCSPtrArray<HOLDATA> *popData);
    ~HolidayTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	BOOL IsPassFilter(HOLDATA *prpHoliday);
	int CompareHoliday(HOLIDAYTABLE_LINEDATA *prpHoliday1, HOLIDAYTABLE_LINEDATA *prpHoliday2);
    void MakeLines();
	void MakeLine(HOLDATA *prpHoliday);
	BOOL FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(HOLIDAYTABLE_LINEDATA *prpHoliday);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(HOLIDAYTABLE_LINEDATA *prpLine);
	void ProcessHolidayChange(HOLDATA *prpHoliday);
	void ProcessHolidayDelete(HOLDATA *prpHoliday);
	BOOL FindHoliday(char *prpHolidayKeya, char *prpHolidayKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	BOOL bmIsFromSearch;
// Attributes
private:
    CCSTable *pomHolidayTable;
	CCSPtrArray<HOLDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<HOLIDAYTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(HOLIDAYTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__HOLIDAYTABLEVIEWER_H__
