// CedaWgnData.cpp
 
#include <stdafx.h>
#include <CedaWgnData.h>
#include <resource.h>


void ProcessWgnCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaWgnData::CedaWgnData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for WGNDataStruct
	BEGIN_CEDARECINFO(WGNDATA,WGNDataRecInfo)
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Algr,"ALGR")

	END_CEDARECINFO //(WGNDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(WGNDataRecInfo)/sizeof(WGNDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&WGNDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"WGN");
	strcpy(pcmListOfFields,"URNO,ALGR");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaWgnData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
/*	ropFields.Add("CDAT");
	ropFields.Add("LSTU");
	ropFields.Add("ORGC");
	ropFields.Add("REMA");
	ropFields.Add("SHIR");
	ropFields.Add("WGNC");
	ropFields.Add("WGND");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");

	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING41));
	ropDesription.Add(LoadStg(IDS_STRING805));
	ropDesription.Add(LoadStg(IDS_STRING819));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));
	ropDesription.Add(LoadStg(IDS_STRING818));
	ropDesription.Add(LoadStg(IDS_STRING501));

	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");*/

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaWgnData::Register(void)
{
	ogDdx.Register((void *)this,BC_WGN_CHANGE,	CString("WGNDATA"), CString("Wgn-changed"),	ProcessWgnCf);
	ogDdx.Register((void *)this,BC_WGN_NEW,		CString("WGNDATA"), CString("Wgn-new"),		ProcessWgnCf);
	ogDdx.Register((void *)this,BC_WGN_DELETE,	CString("WGNDATA"), CString("Wgn-deleted"),	ProcessWgnCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaWgnData::~CedaWgnData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaWgnData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaWgnData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		WGNDATA *prlWgn = new WGNDATA;
		if ((ilRc = GetFirstBufferRecord(prlWgn)) == true)
		{
			omData.Add(prlWgn);//Update omData
			omUrnoMap.SetAt((void *)prlWgn->Urno,prlWgn);
		}
		else
		{
			delete prlWgn;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaWgnData::Insert(WGNDATA *prpWgn)
{
	prpWgn->IsChanged = DATA_NEW;
	if(Save(prpWgn) == false) return false; //Update Database
	InsertInternal(prpWgn);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaWgnData::InsertInternal(WGNDATA *prpWgn)
{
	ogDdx.DataChanged((void *)this, WGN_NEW,(void *)prpWgn ); //Update Viewer
	omData.Add(prpWgn);//Update omData
	omUrnoMap.SetAt((void *)prpWgn->Urno,prpWgn);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaWgnData::Delete(long lpUrno)
{
	WGNDATA *prlWgn = GetWgnByUrno(lpUrno);
	if (prlWgn != NULL)
	{
		prlWgn->IsChanged = DATA_DELETED;
		if(Save(prlWgn) == false) return false; //Update Database
		DeleteInternal(prlWgn);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaWgnData::DeleteInternal(WGNDATA *prpWgn)
{
	ogDdx.DataChanged((void *)this,WGN_DELETE,(void *)prpWgn); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpWgn->Urno);
	int ilWgnCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilWgnCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpWgn->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaWgnData::Update(WGNDATA *prpWgn)
{
	if (GetWgnByUrno(prpWgn->Urno) != NULL)
	{
		if (prpWgn->IsChanged == DATA_UNCHANGED)
		{
			prpWgn->IsChanged = DATA_CHANGED;
		}
		if(Save(prpWgn) == false) return false; //Update Database
		UpdateInternal(prpWgn);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaWgnData::UpdateInternal(WGNDATA *prpWgn)
{
	WGNDATA *prlWgn = GetWgnByUrno(prpWgn->Urno);
	if (prlWgn != NULL)
	{
		*prlWgn = *prpWgn; //Update omData
		ogDdx.DataChanged((void *)this,WGN_CHANGE,(void *)prlWgn); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

WGNDATA *CedaWgnData::GetWgnByUrno(long lpUrno)
{
	WGNDATA  *prlWgn;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlWgn) == TRUE)
	{
		return prlWgn;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaWgnData::ReadSpecialData(CCSPtrArray<WGNDATA> *popWgn,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","WGN",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","WGN",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popWgn != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			WGNDATA *prpWgn = new WGNDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpWgn,CString(pclFieldList))) == true)
			{
				popWgn->Add(prpWgn);
			}
			else
			{
				delete prpWgn;
			}
		}
		if(popWgn->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaWgnData::Save(WGNDATA *prpWgn)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpWgn->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpWgn->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpWgn);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpWgn->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpWgn->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpWgn);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpWgn->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpWgn->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessWgnCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogWgnData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaWgnData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlWgnData;
	prlWgnData = (struct BcStruct *) vpDataPointer;
	WGNDATA *prlWgn;
	if(ipDDXType == BC_WGN_NEW)
	{
		prlWgn = new WGNDATA;
		GetRecordFromItemList(prlWgn,prlWgnData->Fields,prlWgnData->Data);
		InsertInternal(prlWgn);
	}
	if(ipDDXType == BC_WGN_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlWgnData->Selection);
		prlWgn = GetWgnByUrno(llUrno);
		if(prlWgn != NULL)
		{
			GetRecordFromItemList(prlWgn,prlWgnData->Fields,prlWgnData->Data);
			UpdateInternal(prlWgn);
		}
	}
	if(ipDDXType == BC_WGN_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlWgnData->Selection);

		prlWgn = GetWgnByUrno(llUrno);
		if (prlWgn != NULL)
		{
			DeleteInternal(prlWgn);
		}
	}
}

//---------------------------------------------------------------------------------------------------------
