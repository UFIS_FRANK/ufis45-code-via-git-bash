#ifndef __POSITIONTABLEVIEWER_H__
#define __POSITIONTABLEVIEWER_H__

#include <stdafx.h>
#include <CedaPSTData.h>
#include <CedaSgmData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>

struct POSITIONTABLE_LINEDATA
{
	long	Urno;
	CString	Pnam;
	CString	Gpus;
	CString	Acus;
	CString	Fuls;
	CString	Pcas;
	CString	Pubk;
	CString	Tele;
	CString	Taxi;
	CString	Vafr;
	CString	Vato;
	CString	Dgss;
	CString	Brgs;
	CString	Mxac;
	CString Gates;
};

/////////////////////////////////////////////////////////////////////////////
// PositionTableViewer

class PositionTableViewer : public CViewer
{
// Constructions
public:
    PositionTableViewer(CCSPtrArray<PSTDATA> *popData);
    ~PositionTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	BOOL IsPassFilter(PSTDATA *prpPosition);
	int ComparePosition(POSITIONTABLE_LINEDATA *prpPosition1, POSITIONTABLE_LINEDATA *prpPosition2);
    void MakeLines();
	void MakeLine(PSTDATA *prpPosition);
// Operations
public:
	BOOL FindLine(long lpUrno, int &rilLineno);
	void DeleteAll();
	void CreateLine(POSITIONTABLE_LINEDATA *prpPosition);
	void DeleteLine(int ipLineno);
	bool CreateExcelFile(const CString& opTrenner,const CString& ropListSeparator);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(POSITIONTABLE_LINEDATA *prpLine);
	void ProcessPositionChange(PSTDATA *prpPosition);
	void ProcessPositionDelete(PSTDATA *prpPosition);
	void ProcessPositionChange(SGMDATA *prpSgm);
	BOOL FindPosition(char *prpPositionKeya, char *prpPositionKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	BOOL bmIsFromSearch;
// Attributes
private:
    CCSTable *pomPositionTable;
	CCSPtrArray<PSTDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<POSITIONTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(POSITIONTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;
	CString	omFileName;

};

#endif //__POSITIONTABLEVIEWER_H__
