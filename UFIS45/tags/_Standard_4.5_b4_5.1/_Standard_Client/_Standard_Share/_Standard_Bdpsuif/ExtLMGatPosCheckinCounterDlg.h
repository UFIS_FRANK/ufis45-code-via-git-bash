#if !defined(AFX_EXTLMGATPOSCHECKINCOUNTERDLG_H__AC584851_6429_43E8_9D0A_9CC59DE0D2E5__INCLUDED_)
#define AFX_EXTLMGATPOSCHECKINCOUNTERDLG_H__AC584851_6429_43E8_9D0A_9CC59DE0D2E5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ExtLMGatPosCheckinCounterDlg.h : header file
//
#include <GatPosCheckinCounterDlg.h>
#include <CedaOccData.h>

/////////////////////////////////////////////////////////////////////////////
// ExtLMGatPosCheckinCounterDlg dialog

class ExtLMGatPosCheckinCounterDlg : public GatPosCheckinCounterDlg
{
// Construction
public:
	ExtLMGatPosCheckinCounterDlg(CICDATA *popCIC,CWnd* pParent = NULL);   // standard constructor
	~ExtLMGatPosCheckinCounterDlg();


// Dialog Data
	//{{AFX_DATA(ExtLMGatPosCheckinCounterDlg)
	enum { IDD = IDD_EXTLM_GATPOS_CHECKINCOUNTERDLG };
		// NOTE: the ClassWizard will add data members here
	CCSEdit m_CATR;
	CCSEdit m_CSEQ;
	CStatic	m_OCCFRAME;
	CButton	m_UTC;
	CButton	m_LOCAL;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ExtLMGatPosCheckinCounterDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(ExtLMGatPosCheckinCounterDlg)
		// NOTE: the ClassWizard will add member functions here
	virtual BOOL OnInitDialog();
	virtual void OnOK();
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	afx_msg void OnOccDel();
	afx_msg void OnOccNew();
	afx_msg	void OnUTC();
	afx_msg void OnLocal();	
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	void MakeOccTable(CString opAloc, CString opAlid);
	void ChangeOccTable(OCCDATA *prpOcc, int ipLineNo);
	void UpdateOccTable();

private:
	CCSTable	*pomOccTable;
	int			imLastSelection;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EXTLMGATPOSCHECKINCOUNTERDLG_H__AC584851_6429_43E8_9D0A_9CC59DE0D2E5__INCLUDED_)
