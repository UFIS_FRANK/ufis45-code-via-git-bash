// CedaValData.h

#ifndef __CEDAPVALDATA__
#define __CEDAPVALDATA__
 
#include <stdafx.h>
#include <basicdata.h>

//---------------------------------------------------------------------------------------------------------

struct VALDATA 
{
	char  Appl[8+2];
	CTime Cdat;
	char  Freq[7+2];
	CTime Lstu;
	char  Tabn[6+2];
	char  Timf[4+2];
	char  Timt[4+2];
	long  Urno;
	long  Urue;
	char  Usec[32+2];
	char  Useu[32+2];
	long  Uval;
	COleDateTime Vafr;
	COleDateTime Vato;
 
	//DataCreated by this class
	int      IsChanged;

	//long, CTime
	VALDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		Cdat=-1; 
		Lstu=-1;
		Vafr.SetStatus(COleDateTime::invalid);
		Vato.SetStatus(COleDateTime::invalid);
	}

}; // end VALDataStruct

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaValData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;
    CMapPtrToPtr omUvalMap;

    CCSPtrArray<VALDATA> omData;

	char pcmListOfFields[2048];

// OValations
public:
    CedaValData();
	~CedaValData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(VALDATA *prpVal);
	bool InsertInternal(VALDATA *prpVal);
	bool Update(VALDATA *prpVal);
	bool UpdateInternal(VALDATA *prpVal);
	bool Delete(long lpUrno);
	bool DeleteInternal(VALDATA *prpVal);
	bool ReadSpecialData(CCSPtrArray<VALDATA> *popVal,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(VALDATA *prpVal);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	VALDATA *GetValByUrno(long lpUrno);
	VALDATA *GetFirstValByUval(long lpUval);
	bool IsValid(long lpUval,const CString& opTimestamp);
	// Private methods
private:
    void PrepareValData(VALDATA *prpValData);

};

//---------------------------------------------------------------------------------------------------------


#endif //__CEDAPVALDATA__
