// FidsCommandDlg.cpp : implementation file
//

#include <stdafx.h>
#include <BDPSUIF.h>
#include <FidsCommandDlg.h>
#include <PrivList.h>
#include <process.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// FidsCommandDlg dialog


FidsCommandDlg::FidsCommandDlg(FIDDATA *popFID,int iDlgTyp,CWnd* pParent /*=NULL*/) : CDialog(FidsCommandDlg::IDD, pParent)
{

	pomFID = popFID;
	m_iDlgTyp = iDlgTyp ; 
	m_Util = new UtilUC();
	//{{AFX_DATA_INIT(FidsCommandDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void FidsCommandDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(FidsCommandDlg)
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_VAFR_D, m_VAFRD);
	DDX_Control(pDX, IDC_VAFR_T, m_VAFRT);
	DDX_Control(pDX, IDC_VATO_D, m_VATOD);
	DDX_Control(pDX, IDC_VATO_T, m_VATOT);
	DDX_Control(pDX, IDC_CODE,	 m_CODE);
	DDX_Control(pDX, IDC_REMI,	 m_REMI);
	DDX_Control(pDX, IDC_REMT,	 m_REMT);
	DDX_Control(pDX, IDC_BLKC,	 m_BLKC);
	DDX_Control(pDX, IDC_BEME,	 m_BEME);
	DDX_Control(pDX, IDC_BEMD,	 m_BEMD);
	DDX_Control(pDX, IDC_BET3,	 m_BET3);
	DDX_Control(pDX, IDC_BET4,	 m_BET4);
	DDX_Control(pDX, IDC_CONR,	 m_CONR);
	DDX_Control(pDX, IDC_REMA,   m_REMA);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(FidsCommandDlg, CDialog)
	//{{AFX_MSG_MAP(FidsCommandDlg)
	ON_BN_CLICKED(IDC_BUTTON1, OnForeignLanguageRemarks)
	ON_MESSAGE(WM_DATA_RECEIVED,OnReceiveMsg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// FidsCommandDlg message handlers
//----------------------------------------------------------------------------------------

BOOL FidsCommandDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_Caption = LoadStg(IDS_STRING172) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);

	char clStat = ogPrivList.GetStat("FIDSCOMMANDDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomFID->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("FIDSCOMMANDDLG.m_CDAT"));
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomFID->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("FIDSCOMMANDDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomFID->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("FIDSCOMMANDDLG.m_LSTU"));
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomFID->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("FIDSCOMMANDDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomFID->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("FIDSCOMMANDDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomFID->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("FIDSCOMMANDDLG.m_USEU"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_CODE.SetFormat("x|#x|#x|#x|#");
	m_CODE.SetTextLimit(1,4);
	m_CODE.SetBKColor(YELLOW);  
	m_CODE.SetTextErrColor(RED);
	m_CODE.SetInitText(pomFID->Code);
	m_CODE.SetSecState(ogPrivList.GetStat("FIDSCOMMANDDLG.m_CODE"));
	//------------------------------------
	m_REMI.SetFormat("x|#x|#x|#x|#");
	m_REMI.SetBKColor(YELLOW);  
	m_REMI.SetTextLimit(1,4);
	m_REMI.SetTextErrColor(RED);
	m_REMI.SetInitText(pomFID->Remi);
	m_REMI.SetSecState(ogPrivList.GetStat("FIDSCOMMANDDLG.m_REMI"));
	//------------------------------------
	m_REMT.SetFormat("x|#");
	m_REMT.SetBKColor(YELLOW);  
	m_REMT.SetTextErrColor(RED);
	m_REMT.SetInitText(pomFID->Remt);
	m_REMT.SetSecState(ogPrivList.GetStat("FIDSCOMMANDDLG.m_REMT"));
	//------------------------------------
	/*if (pomFID->Blkc[0] == 'X') m_BLKC.SetCheck(1);
	clStat = ogPrivList.GetStat("FIDSCOMMANDDLG.m_BLKC");
	SetWndStatAll(clStat,m_BLKC);*/
	m_BLKC.SetTypeToString("X(1)",1,1);
	m_BLKC.SetBKColor(WHITE);
	m_BLKC.SetTextErrColor(RED);
	m_BLKC.SetInitText(pomFID->Blkc);
	m_BLKC.SetSecState(ogPrivList.GetStat("FIDSCOMMANDDLG.m_BLKC"));
	//------------------------------------
	m_BEME.SetTypeToString("X(128)",128,1);
	m_BEME.SetBKColor(YELLOW);  
	m_BEME.SetTextErrColor(RED);

		CString olStr = pomFID->Beme;
		CString olString;
	if(bgUnicode)
	{
		m_Util->ConvOctStToByteArr(olStr,olString);
		m_BEME.SetInitText(olString);
	}
	else
	{
	m_BEME.SetInitText(pomFID->Beme);
	}
	m_BEME.SetSecState(ogPrivList.GetStat("FIDSCOMMANDDLG.m_BEME"));
	//------------------------------------
	m_BEMD.SetTypeToString("X(128)",128,0);
	m_BEMD.SetTextErrColor(RED);
	m_BEMD.SetInitText(pomFID->Bemd);
	m_BEMD.SetSecState(ogPrivList.GetStat("FIDSCOMMANDDLG.m_BEMD"));
	//------------------------------------
	m_BET3.SetTypeToString("X(128)",128,0);
	m_BET3.SetTextErrColor(RED);
	m_BET3.SetInitText(pomFID->Bet3);
	m_BET3.SetSecState(ogPrivList.GetStat("FIDSCOMMANDDLG.m_BET3"));
	//------------------------------------
	m_BET4.SetTypeToString("X(128)",128,0);
	m_BET4.SetTextErrColor(RED);
	m_BET4.SetInitText(pomFID->Bet4);
	m_BET4.SetSecState(ogPrivList.GetStat("FIDSCOMMANDDLG.m_BET4"));
	//------------------------------------
	m_CONR.SetFormat("#(3)");
	m_CONR.SetTextLimit(0,3);
	m_CONR.SetTextErrColor(RED);
	m_CONR.SetInitText(pomFID->Conr);
	m_CONR.SetSecState(ogPrivList.GetStat("FIDSCOMMANDDLG.m_CONR"));
	//------------------------------------
	m_VAFRD.SetTypeToDate(true);
	m_VAFRD.SetTextErrColor(RED);
	m_VAFRD.SetBKColor(YELLOW);
	m_VAFRD.SetInitText(pomFID->Vafr.Format("%d.%m.%Y"));
	m_VAFRD.SetSecState(ogPrivList.GetStat("FIDSCOMMANDDLG.m_VAFR"));
	// - - - - - - - - - - - - - - - - - -
	m_VAFRT.SetTypeToTime(true);
	m_VAFRT.SetTextErrColor(RED);
	m_VAFRT.SetBKColor(YELLOW);
	m_VAFRT.SetInitText(pomFID->Vafr.Format("%H:%M"));
	m_VAFRT.SetSecState(ogPrivList.GetStat("FIDSCOMMANDDLG.m_VAFR"));
	//------------------------------------
	m_VATOD.SetTypeToDate();
	m_VATOD.SetTextErrColor(RED);
	m_VATOD.SetInitText(pomFID->Vato.Format("%d.%m.%Y"));
	m_VATOD.SetSecState(ogPrivList.GetStat("FIDSCOMMANDDLG.m_VATO"));
	// - - - - - - - - - - - - - - - - - -
	m_VATOT.SetTypeToTime();
	m_VATOT.SetTextErrColor(RED);
	m_VATOT.SetInitText(pomFID->Vato.Format("%H:%M"));
	m_VATOT.SetSecState(ogPrivList.GetStat("FIDSCOMMANDDLG.m_VATO"));
	//------------------------------------
	if(m_iDlgTyp)
	{
		CWnd* pWnd ;

		//FID Remark Text box1 National Langauge
		pWnd = GetDlgItem(IDC_BEMD);
		pWnd->ShowWindow(0);


		//FID Remark Text Box3
		pWnd = GetDlgItem(IDC_BET3);
		pWnd->ShowWindow(0);
		
		//FID Remark Text Box3
		pWnd = GetDlgItem(IDC_BET4);
		pWnd->ShowWindow(0);

		//Static Text box1
		pWnd = GetDlgItem(IDC_STATIC_TXT1);
		pWnd->ShowWindow(0);

		//Static Text box3
		pWnd = GetDlgItem(IDC_STATIC_TXT3);
		pWnd->ShowWindow(0);

		pWnd = GetDlgItem(IDC_STATIC_TXT4);
		pWnd->ShowWindow(0);

		CWnd* pWndBut = GetDlgItem(IDC_BUTTON1);
		pWndBut->ShowWindow(1);

		ResizeWindow();
	}
	
	if(ogFIDData.DoesRemarkCodeExist() == true)
	{
		m_REMA.SetTypeToString("X(512)",512,1);
		m_REMA.SetBKColor(WHITE);
		m_REMA.SetTextErrColor(RED);
		m_REMA.SetInitText(pomFID->Rema);
		m_REMA.SetSecState(ogPrivList.GetStat("FIDSCOMMANDDLG.m_REMA"));
	}
	else
	{
		ResizeDialog();
	}
	
	
	//------------------------------------
	return TRUE;  
}

//----------------------------------------------------------------------------------------

void FidsCommandDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_CODE.GetStatus() == false)
	{
		ilStatus = false;
		if(m_CODE.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING237) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING237) + ogNotFormat;
		}
	}
	if(m_REMI.GetStatus() == false)
	{
		ilStatus = false;
		if(m_REMI.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING583) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING583) + ogNotFormat;
		}
	}
	if(m_REMT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_REMT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING584) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING584) + ogNotFormat;
		}
	}
	if(m_BEME.GetStatus() == false)
	{
		ilStatus = false;
		if(m_BEME.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING37) + CString(" 1") +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING37) + CString(" 1") + ogNotFormat;
		}
	}

	if(!bgUnicode)
	{
		if(m_BEMD.GetStatus() == false)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING37) + CString(" 2") + ogNotFormat;
		}

		if(m_BET3.GetStatus() == false)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING37) + CString(" 3") + ogNotFormat;
		}

		if(m_BET4.GetStatus() == false)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING37) + CString(" 4") + ogNotFormat;
		}
	}

	if(m_CONR.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING1018) + ogNotFormat;
	}

	if(m_VAFRD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VAFRT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}
	if(m_VATOD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_VATOT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
	}

	///////////////////////////////////////////////////////////////////////////
	CString olVafrd,olVafrt,olVatod,olVatot;
	CTime olTmpTimeFr,olTmpTimeTo;
	m_VAFRD.GetWindowText(olVafrd);
	m_VAFRT.GetWindowText(olVafrt);
	m_VATOD.GetWindowText(olVatod);
	m_VATOT.GetWindowText(olVatot);

	if((m_VAFRD.GetWindowTextLength() != 0 && m_VAFRT.GetWindowTextLength() == 0) || (m_VAFRD.GetWindowTextLength() == 0 && m_VAFRT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING234);
	}
	if((m_VATOD.GetWindowTextLength() != 0 && m_VATOT.GetWindowTextLength() == 0) || (m_VATOD.GetWindowTextLength() == 0 && m_VATOT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING235);
	}
	if(m_VAFRD.GetStatus() == true && m_VAFRT.GetStatus() == true && m_VATOD.GetStatus() == true && m_VATOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olVafrd,olVafrt);
		olTmpTimeTo = DateHourStringToDate(olVatod,olVatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
	}

	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olCode;
		char clWhere[100];
		CCSPtrArray<FIDDATA> olFidCPA;

		m_CODE.GetWindowText(olCode);
		sprintf(clWhere,"WHERE CODE='%s'",olCode);
		if(ogFIDData.ReadSpecialData(&olFidCPA,clWhere,"URNO,CODE",false) == true)
		{
			if(olFidCPA[0].Urno != pomFID->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING237) + LoadStg(IDS_EXIST);
			}
		}
		olFidCPA.DeleteAll();
	}

	///////////////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_CODE.GetWindowText(pomFID->Code,5);
		m_REMI.GetWindowText(pomFID->Remi,6);
		m_REMT.GetWindowText(pomFID->Remt,3);
		
		if(bgUnicode)
		{
			CString olStr;
			m_BEME.GetWindowText(olStr);

			BYTE byteArray[200] ;
			int count = 0;// j = (BYTE)'h';
			for(int i = 0;i<olStr.GetLength();i++)
			{
				byteArray[i] = (BYTE)olStr.GetAt(i);
				count++;
			}
			CString octStr;
			m_Util->ConvByteArrToOctSt(byteArray,count,octStr);
			sprintf(pomFID->Beme,"%s",octStr);

			if(!m_StrReceiveData.IsEmpty() && m_bReceiveData)
			{
				
				CString str = m_StrReceiveData;
				CStringArray olStrArray;
							
				int k = 0;
				//Abbrev1
				for(int j = 0 ; j <4;j++)
				{
					k = str.Find(",");
					CString olStr = str.Left(k);
					olStrArray.Add(str.Left(k));
					str =	str.Mid(k+1);
				}

				olStrArray.Add(str);

				sprintf(pomFID->Bemd,"%s",olStrArray.GetAt(0)); 
				sprintf(pomFID->Bet3,"%s",olStrArray.GetAt(1)); 
				sprintf(pomFID->Bet4,"%s",olStrArray.GetAt(2));
			}
		}
		else
		{
			m_BEME.GetWindowText(pomFID->Beme,129);
			m_BEMD.GetWindowText(pomFID->Bemd,129);
			m_BET3.GetWindowText(pomFID->Bet3,129);
			m_BET4.GetWindowText(pomFID->Bet4,129);
		}
		
		m_CONR.GetWindowText(pomFID->Conr,4);

		pomFID->Vafr = DateHourStringToDate(olVafrd,olVafrt);
		pomFID->Vato = DateHourStringToDate(olVatod,olVatot);
		//(m_BLKC.GetCheck() == 1) ? pomFID->Blkc[0] = 'X' : pomFID->Blkc[0] = ' ';
		m_BLKC.GetWindowText(pomFID->Blkc,2);		

		if(ogFIDData.DoesRemarkCodeExist() == true)
		{		
			m_REMA.GetWindowText(pomFID->Rema,sizeof(pomFID->Rema));		
		}		

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_CODE.SetFocus();
		m_CODE.SetSel(0,-1);
	}
}

void FidsCommandDlg::ResizeDialog()
{	
	CRect olWindowRect;
	CRect olRemarkRect;
	CRect olCreatedRect;
	int ilOffset = 0;
	
	
	((CWnd*)GetDlgItem(IDC_REMA))->GetWindowRect(olRemarkRect);
	ScreenToClient(olRemarkRect);

	((CWnd*)GetDlgItem(IDC_CDAT_D))->GetWindowRect(olCreatedRect);
	ScreenToClient(olCreatedRect);

	ilOffset = olCreatedRect.top - olRemarkRect.top;
	CRect olDeflateRect(0,-ilOffset,0,ilOffset);	
	this->GetWindowRect(&olWindowRect);
	olWindowRect.bottom -= ilOffset;
	this->MoveWindow(olWindowRect);	

	((CWnd*)GetDlgItem(IDC_STATIC_REMA))->ShowWindow(SW_HIDE);
	((CWnd*)GetDlgItem(IDC_REMA))->ShowWindow(SW_HIDE);
	
	((CWnd*)GetDlgItem(IDC_GROUP_INFO))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.bottom -= ilOffset;
	((CWnd*)GetDlgItem(IDC_GROUP_INFO))->MoveWindow(olWindowRect);

	((CWnd*)GetDlgItem(IDC_STATIC_CREATED_ON))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CWnd*)GetDlgItem(IDC_STATIC_CREATED_ON))->MoveWindow(olWindowRect);

	((CWnd*)GetDlgItem(IDC_STATIC_CREATED_BY))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CWnd*)GetDlgItem(IDC_STATIC_CREATED_BY))->MoveWindow(olWindowRect);

	((CWnd*)GetDlgItem(IDC_STATIC_CHANGED_ON))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CWnd*)GetDlgItem(IDC_STATIC_CHANGED_ON))->MoveWindow(olWindowRect);

	((CWnd*)GetDlgItem(IDC_STATIC_CHANGED_BY))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CWnd*)GetDlgItem(IDC_STATIC_CHANGED_BY))->MoveWindow(olWindowRect);

	((CWnd*)GetDlgItem(IDC_CDAT_D))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CWnd*)GetDlgItem(IDC_CDAT_D))->MoveWindow(olWindowRect);

	((CWnd*)GetDlgItem(IDC_CDAT_T))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CWnd*)GetDlgItem(IDC_CDAT_T))->MoveWindow(olWindowRect);

	((CWnd*)GetDlgItem(IDC_LSTU_D))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CWnd*)GetDlgItem(IDC_LSTU_D))->MoveWindow(olWindowRect);

	((CWnd*)GetDlgItem(IDC_LSTU_T))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CWnd*)GetDlgItem(IDC_LSTU_T))->MoveWindow(olWindowRect);

	((CWnd*)GetDlgItem(IDC_USEC))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CWnd*)GetDlgItem(IDC_USEC))->MoveWindow(olWindowRect);

	((CWnd*)GetDlgItem(IDC_USEU))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CWnd*)GetDlgItem(IDC_USEU))->MoveWindow(olWindowRect);

	((CWnd*)GetDlgItem(IDOK))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CWnd*)GetDlgItem(IDOK))->MoveWindow(olWindowRect);

	((CWnd*)GetDlgItem(IDCANCEL))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CWnd*)GetDlgItem(IDCANCEL))->MoveWindow(olWindowRect);

}
//----------------------------------------------------------------------------------------

void FidsCommandDlg::ResizeWindow()
{
	CRect olWindowRect;
	CRect olNameWindowRect;
	int olOffset = 0;

	((CEdit*)GetDlgItem(IDC_BEME))->GetWindowRect(olNameWindowRect);
	ScreenToClient(olNameWindowRect);

	((CStatic*)GetDlgItem(IDC_BET4))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);

	olOffset = olWindowRect.bottom - olNameWindowRect.bottom;

	CRect olDeflateRect(0,-(olOffset),0,olOffset);
	this->GetWindowRect(&olWindowRect);
	olWindowRect.bottom -= olOffset;
	this->MoveWindow(olWindowRect);

	((CStatic*)GetDlgItem(IDC_STATIC_REMAGRP))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.bottom =  olWindowRect.bottom - olOffset;
	//olWindowRect.DeflateRect(olDeflateRect);
	((CStatic*)GetDlgItem(IDC_STATIC_REMAGRP))->MoveWindow(olWindowRect);

	//Valid Group
	((CStatic*)GetDlgItem(IDC_STATIC_VALGRP))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CStatic*)GetDlgItem(IDC_STATIC_VALGRP))->MoveWindow(olWindowRect);

	//Valid From 
	((CStatic*)GetDlgItem(IDC_STATIC_VALFR))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CStatic*)GetDlgItem(IDC_STATIC_VALFR))->MoveWindow(olWindowRect);

	((CEdit*)GetDlgItem(IDC_VAFR_D))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_VAFR_D))->MoveWindow(olWindowRect);

	((CEdit*)GetDlgItem(IDC_VAFR_T))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_VAFR_T))->MoveWindow(olWindowRect);

	//Valid To 
	((CStatic*)GetDlgItem(IDC_STATIC_VALT))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CStatic*)GetDlgItem(IDC_STATIC_VALT))->MoveWindow(olWindowRect);

	((CEdit*)GetDlgItem(IDC_VATO_D))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_VATO_D))->MoveWindow(olWindowRect);

	((CEdit*)GetDlgItem(IDC_VATO_T))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_VATO_T))->MoveWindow(olWindowRect);

	//Created Information
	((CStatic*)GetDlgItem(IDC_GROUP_INFO))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CStatic*)GetDlgItem(IDC_GROUP_INFO))->MoveWindow(olWindowRect);
	//Remarks
	((CStatic*)GetDlgItem(IDC_STATIC_REMA))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CStatic*)GetDlgItem(IDC_STATIC_REMA))->MoveWindow(olWindowRect);
	
	((CEdit*)GetDlgItem(IDC_REMA))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_REMA))->MoveWindow(olWindowRect);

	//Created on
	((CStatic*)GetDlgItem(IDC_STATIC_CREATED_ON))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CStatic*)GetDlgItem(IDC_STATIC_CREATED_ON))->MoveWindow(olWindowRect);

	((CEdit*)GetDlgItem(IDC_CDAT_D))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_CDAT_D))->MoveWindow(olWindowRect);

	((CEdit*)GetDlgItem(IDC_CDAT_T))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_CDAT_T))->MoveWindow(olWindowRect);

	//Changed On
	((CStatic*)GetDlgItem(IDC_STATIC_CHANGED_ON))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CStatic*)GetDlgItem(IDC_STATIC_CHANGED_ON))->MoveWindow(olWindowRect);

	((CEdit*)GetDlgItem(IDC_LSTU_D))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_LSTU_D))->MoveWindow(olWindowRect);

	((CEdit*)GetDlgItem(IDC_LSTU_T))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_LSTU_T))->MoveWindow(olWindowRect);

	//Created By
	((CStatic*)GetDlgItem(IDC_STATIC_CREATED_BY))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CStatic*)GetDlgItem(IDC_STATIC_CREATED_BY))->MoveWindow(olWindowRect);

	((CEdit*)GetDlgItem(IDC_USEC))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_USEC))->MoveWindow(olWindowRect);

	//Changed By
	((CStatic*)GetDlgItem(IDC_STATIC_CHANGED_BY))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CStatic*)GetDlgItem(IDC_STATIC_CHANGED_BY))->MoveWindow(olWindowRect);

	((CEdit*)GetDlgItem(IDC_USEU))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_USEU))->MoveWindow(olWindowRect);

	//OK button
	((CButton*)GetDlgItem(IDOK))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CButton*)GetDlgItem(IDOK))->MoveWindow(olWindowRect);

	//Cancel Button
	((CStatic*)GetDlgItem(IDCANCEL))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CStatic*)GetDlgItem(IDCANCEL))->MoveWindow(olWindowRect);

}

void FidsCommandDlg::OnForeignLanguageRemarks() 
{
	CString olStr1,olStr2,olStr3,olStr4;
	olStr1 =  pomFID->Bemd ;
	olStr2 =  pomFID->Bet3;
	olStr3 =  pomFID->Bet4;
	

	CString olStrDlg = "FidsRemarks";
	olStr1 = olStrDlg + "," + olStr1 + ","+ olStr2 +","+ olStr3 + "," + "";

	char pclConfigPath[256];
	char pclTeluguDlgPath[256];

	if (getenv("CEDA") == NULL)
		strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
	else
		strcpy(pclConfigPath, getenv("CEDA"));

	GetPrivateProfileString(ogAppName, "UCDialog", "DEFAULT",
		pclTeluguDlgPath, sizeof pclTeluguDlgPath, pclConfigPath);


	if(!strcmp(pclTeluguDlgPath, "DEFAULT"))
		MessageBox(LoadStg(IDS_STRING32802), "Error:", MB_ICONERROR);

	char *args[4];
	char slRunTxt[1024];
	
	args[0] = "child";
	sprintf(slRunTxt,"%s",olStr1);
	args[1] = slRunTxt;
	args[2] = NULL;
	args[3] = NULL;

	int ilReturn = _spawnv(_P_NOWAIT,pclTeluguDlgPath,args);
}

void FidsCommandDlg::OnReceiveMsg(WPARAM wParam, LPARAM lParam)
{
	RECEIVE_DATA* polReceiveData = (RECEIVE_DATA*)lParam;
	if(polReceiveData->m_Data.CompareNoCase("6B4CCF9C-5AA9-4ede-B298-08BAAAC9FB88") == 0)
	{
		m_bReceiveData = false;
		EnableWindow();
	}
	else if(polReceiveData->m_Data.CompareNoCase("71425678-6CF3-438f-AFA7-946DF4D5674F") == 0)
	{
		EnableWindow(FALSE);
	}
	else
	{
	m_bReceiveData = true;
	m_StrReceiveData = polReceiveData->m_Data;
		EnableWindow();
	}
}