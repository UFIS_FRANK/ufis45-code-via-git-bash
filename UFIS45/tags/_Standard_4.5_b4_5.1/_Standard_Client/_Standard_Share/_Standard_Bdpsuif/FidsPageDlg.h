#if !defined(AFX_FIDSPAGEDLG_H__08849BA3_4B03_11D7_8007_00010215BFDE__INCLUDED_)
#define AFX_FIDSPAGEDLG_H__08849BA3_4B03_11D7_8007_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FidsPageDlg.h : header file
//
#include <CedaPAGData.h>
#include <CCSEdit.h>
#include <CCSTable.h>
#include <CCSComboBox.h>

/////////////////////////////////////////////////////////////////////////////
// FidsPageDlg dialog

class FidsPageDlg : public CDialog
{
// Construction
public:
	FidsPageDlg(PAGDATA *popPAG,const CString& ropCaption,CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(FidsPageDlg)
	enum { IDD = IDD_FIDS_PAGE };
	CCSEdit	m_PTFE;
	CCSEdit	m_PTFB;
	CCSEdit	m_PNOF;
	CCSEdit	m_PFNT;
	CCSEdit	m_PAGI;
	CCSEdit	m_PRFN;
	CCSEdit	m_PDTI;
	CCSEdit	m_PDPT;
	CCSEdit	m_PDSS;
	CCSEdit	m_PTD1;
	CCSEdit	m_PTD2;
	CCSEdit	m_PCFN;
	CCSEdit	m_PCTI;
	CCSEdit	m_PTDC;
	CCSEdit	m_COTB;

	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEU;
	CCSEdit	m_USEC;
	CCSEdit m_REMA;
	CButton	m_CANCEL;
	CButton	m_OK;

	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(FidsPageDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(FidsPageDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CString		m_Caption;
	PAGDATA		*pomPAG;
	void ResizeDialog();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FIDSPAGEDLG_H__08849BA3_4B03_11D7_8007_00010215BFDE__INCLUDED_)
