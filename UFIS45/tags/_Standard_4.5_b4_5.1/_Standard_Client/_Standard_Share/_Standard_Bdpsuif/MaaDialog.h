#if !defined(AFX_MAADIALOG_H__96689B44_B63C_11D5_912B_0050DADD7302__INCLUDED_)
#define AFX_MAADIALOG_H__96689B44_B63C_11D5_912B_0050DADD7302__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MaaDialog.h : header file
//

#include "CCSEdit.h"
#include "CedaMaaData.h"
#include "CedaMawdata.h"

/////////////////////////////////////////////////////////////////////////////
// CMaaDialog dialog

class CMaaDialog : public CDialog
{
// Construction
public:
	CMaaDialog(MAADATA *popMaa = NULL, CCSPtrArray<MAADATA> *popMaaData = NULL, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CMaaDialog)
	enum { IDD = IDD_MAA_DLG };
	CCSEdit	m_Year;
	CCSEdit	m_Value;
	CCSEdit	m_To_Week;
	CCSEdit	m_From_Week;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMaaDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CMaaDialog)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	MAADATA *pomMaa;
	CCSPtrArray<MAADATA> *pomMaaData;
	
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAADIALOG_H__96689B44_B63C_11D5_912B_0050DADD7302__INCLUDED_)
