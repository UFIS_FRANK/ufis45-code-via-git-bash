
// MitarbeiterStammDlg.cpp : implementation file
//

#include <stdafx.h>
#include <bdpsuif.h>
#include <MitarbeiterStammDlg.h>
#include <PrivList.h>
#include <AwDlg.h>
#include <CedaActData.h>
#include <CedaAltData.h>
#include <CedaSPFData.h>
#include <Sco_Dlg.h>
#include <SorDlg.h>
#include <ADR_Dlg.h>

#include <CedaPolData.h>
#include <CheckReferenz.h>
#include <CedaParData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// MitarbeiterStammDlg dialog
//----------------------------------------------------------------------------------------
#ifdef	IsOverlapped
#	undef	IsOverlapped
#endif

BOOL IsOverlapped(COleDateTime start1,COleDateTime end1,COleDateTime start2,COleDateTime end2)
{
	if (end1.GetStatus() != COleDateTime::valid)
		end1.SetDate(9999,12,31);

	if (end2.GetStatus() != COleDateTime::valid)
		end2.SetDate(9999,12,31);

	return ((start1) <= (end2) && (start2) <= (end1));
}


MitarbeiterStammDlg::MitarbeiterStammDlg(STFDATA *popStf, 
										 CCSPtrArray<SORDATA> *popSorData,
										 CCSPtrArray<SPFDATA> *popSpfData,
										 CCSPtrArray<SPEDATA> *popSpeData,
										 CCSPtrArray<SCODATA> *popScoData,
										 CCSPtrArray<STEDATA> *popSteData,
										 CCSPtrArray<SWGDATA> *popSwgData,
										 CCSPtrArray<SDADATA> *popSdaData,
										 CCSPtrArray<SREDATA> *popSreData,
										 CWnd* pParent /*=NULL*/)
	: CDialog(MitarbeiterStammDlg::IDD, pParent)
{

	pomSorData = popSorData;
	pomSpfData = popSpfData;
	pomSpeData = popSpeData;
	pomScoData = popScoData;
	pomSteData = popSteData;
	pomSwgData = popSwgData;
	pomSdaData = popSdaData;
	pomSreData = popSreData;
	
	pomStf = popStf;

	pomOrgTable = new CCSTable;
	pomPfcTable = new CCSTable;
	pomPerTable = new CCSTable;
	pomCotTable = new CCSTable;
	pomTeaTable = new CCSTable;
	pomWgpTable = new CCSTable;
	pomPolTable	= new CCSTable;
	pomSreTable = new CCSTable;

	ogPolData.Read("ORDER BY NAME");

	pomStatus = NULL;

	//{{AFX_DATA_INIT(MitarbeiterStammDlg)
	//}}AFX_DATA_INIT
}

void MitarbeiterStammDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(MitarbeiterStammDlg)
	DDX_Control(pDX, IDC_REMARK, m_REMA);
	DDX_Control(pDX, IDC_HELP_GRP, m_HELP_GRP);
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_TOHR, m_THOR);
	DDX_Control(pDX, IDC_HELP_TEA, m_HELP_TEA);
	DDX_Control(pDX, IDC_HELP_PFC, m_HELP_PFC);
	DDX_Control(pDX, IDC_HELP_PER, m_HELP_PER);
	DDX_Control(pDX, IDC_HELP_ORG, m_HELP_ORG);
	DDX_Control(pDX, IDC_HELP_COT, m_HELP_COT);
	DDX_Control(pDX, IDC_CDAT_D2, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T2, m_CDATT);
	DDX_Control(pDX, IDC_DODM_D, m_DODDM_D);
	DDX_Control(pDX, IDC_DOEM_D, m_DOEM_D);
	DDX_Control(pDX, IDC_FINM, m_FINM);
	//DDX_Control(pDX, IDC_GSMN, m_GSMN);
	DDX_Control(pDX, IDC_LNAM, m_LANM);
	//DDX_Control(pDX, IDC_LINO, m_LINO);
	//DDX_Control(pDX, IDC_MATR, m_MATR);
	DDX_Control(pDX, IDC_PENO, m_PENO);
	DDX_Control(pDX, IDC_PERC, m_PERC);
	DDX_Control(pDX, IDC_SHNM, m_SHNM);
	DDX_Control(pDX, IDC_TELD, m_TELD);
	DDX_Control(pDX, IDC_TELH, m_TELH);
	DDX_Control(pDX, IDC_TELP, m_TELP);
	DDX_Control(pDX, IDC_USEC2, m_USEC);
	DDX_Control(pDX, IDC_USEU2, m_USEU);
	DDX_Control(pDX, IDC_LSTU_D2, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T2, m_LSTUT);
	DDX_Control(pDX, IDC_PNOF, m_PNOF);
	DDX_Control(pDX, IDC_RELS, m_RELS);
	DDX_Control(pDX, IDC_KIND, m_KIND);
	DDX_Control(pDX, IDC_NATI, m_NATI);
	DDX_Control(pDX, IDC_DOBK, m_DOBK);
	DDX_Control(pDX, IDC_GEBU, m_GEBU);
	DDX_Control(pDX, IDC_CANO, m_CANO);
	DDX_Control(pDX, IDC_KIDS, m_KIDS);
	DDX_Control(pDX, IDC_HELP_SRE, m_HELP_SRE);
	//}}AFX_DATA_MAP
}

MitarbeiterStammDlg::~MitarbeiterStammDlg()
{
		if(pomOrgTable != NULL)
			delete pomOrgTable;
		if(pomPfcTable != NULL)
			delete pomPfcTable;
		if(pomPerTable != NULL)
			delete pomPerTable;
		if(pomCotTable != NULL)
			delete pomCotTable;
		if(pomTeaTable != NULL)
			delete pomTeaTable;
		if(pomWgpTable != NULL)
			delete pomWgpTable;
		if(pomPolTable != NULL)
			delete pomPolTable;
		if(pomSreTable != NULL)
			delete pomSreTable;

		//*** 30.08.99 SHA ***
		olAct.DeleteAll();
		olAlt.DeleteAll();

}

BEGIN_MESSAGE_MAP(MitarbeiterStammDlg, CDialog)
	//{{AFX_MSG_MAP(MitarbeiterStammDlg)
    ON_MESSAGE(WM_EDIT_KILLFOCUS, OnKillfocus)
	ON_BN_CLICKED(IDC_BF1, OnBf1)
	ON_BN_CLICKED(IDC_BF2, OnBf2)
	ON_BN_CLICKED(IDC_BF3, OnBf3)
	ON_BN_CLICKED(IDC_BF4, OnBf4)
	ON_BN_CLICKED(IDC_BF5, OnBf5)
	ON_BN_CLICKED(IDC_BG1, OnBg1)
	ON_BN_CLICKED(IDC_BG2, OnBg2)
	ON_BN_CLICKED(IDC_BG3, OnBg3)
	ON_BN_CLICKED(IDC_BG4, OnBg4)
	ON_BN_CLICKED(IDC_BG5, OnBg5)
	ON_BN_CLICKED(IDC_BO1, OnBo1)
	ON_BN_CLICKED(IDC_BO2, OnBo2)
	ON_BN_CLICKED(IDC_BO3, OnBo3)
	ON_BN_CLICKED(IDC_BO4, OnBo4)
	ON_BN_CLICKED(IDC_BO5, OnBo5)
	ON_BN_CLICKED(IDC_BQ1, OnBq1)
	ON_BN_CLICKED(IDC_BQ2, OnBq2)
	ON_BN_CLICKED(IDC_BQ3, OnBq3)
	ON_BN_CLICKED(IDC_BQ4, OnBq4)
	ON_BN_CLICKED(IDC_BQ5, OnBq5)
	ON_BN_CLICKED(IDC_BA1, OnBa1)
	ON_BN_CLICKED(IDC_BA2, OnBa2)
	ON_BN_CLICKED(IDC_BA3, OnBa3)
	ON_BN_CLICKED(IDC_BA4, OnBa4)
	ON_BN_CLICKED(IDC_BA5, OnBa5)
	ON_BN_CLICKED(IDC_BGRP1, OnBgrp1)
	ON_BN_CLICKED(IDC_BGRP2, OnBgrp2)
	ON_BN_CLICKED(IDC_BGRP3, OnBgrp3)
	ON_BN_CLICKED(IDC_BGRP4, OnBgrp4)
	ON_BN_CLICKED(IDC_BGRP5, OnBgrp5)
	ON_BN_CLICKED(IDC_ADR, OnAdr)
	ON_MESSAGE(WM_TABLE_IPEDIT_KILLFOCUS, OnTableIPEditKillfocus)
	ON_BN_CLICKED(IDC_BR1, OnBr1)
	ON_BN_CLICKED(IDC_BR2, OnBr2)
	ON_BN_CLICKED(IDC_BR3, OnBr3)
	ON_BN_CLICKED(IDC_BR4, OnBr4)
	ON_BN_CLICKED(IDC_BR5, OnBr5)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// MitarbeiterStammDlg message handlers
//----------------------------------------------------------------------------------------

BOOL MitarbeiterStammDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	omStatusText = LoadStg(IDS_STRING835);
	pomStatus->SetWindowText(omStatusText);
	AfxGetApp()->DoWaitCursor(1);

	if(m_Caption == LoadStg(IDS_STRING150))
		bmChangeAction = true;
	else
		bmChangeAction = false;

	m_Caption = LoadStg(IDS_STRING183) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	
	char clStat = ogPrivList.GetStat("MITARBEITERSTAMMDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomStf->Cdat.Format("%d.%m.%Y"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomStf->Cdat.Format("%H:%M"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomStf->Lstu.Format("%d.%m.%Y"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomStf->Lstu.Format("%H:%M"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomStf->Usec);
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomStf->Useu);
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_DODDM_D.SetTypeToDate(false);
	m_DODDM_D.SetTextErrColor(RED);
	m_DODDM_D.SetSecState(ogPrivList.GetStat("MITARBEITERSTAMMDLG.m_DODM_D"));
	if(pomStf->Dodm.GetStatus() == COleDateTime::valid)
		m_DODDM_D.SetInitText(pomStf->Dodm.Format("%d.%m.%Y"));
	else
		m_DODDM_D.SetInitText("");
	//------------------------------------
	m_DOEM_D.SetTypeToDate(true);
	m_DOEM_D.SetBKColor(YELLOW);  
	m_DOEM_D.SetTextErrColor(RED);
	m_DOEM_D.SetSecState(ogPrivList.GetStat("MITARBEITERSTAMMDLG.m_DOEM_D"));
	if(pomStf->Doem.GetStatus() == COleDateTime::valid)
		m_DOEM_D.SetInitText(pomStf->Doem.Format("%d.%m.%Y"));
	else
		m_DOEM_D.SetInitText(CString(CTime::GetCurrentTime().Format("%d.%m.%Y")));
	
	//------------------------------------
	m_FINM.SetTypeToString("X(40)", 40, 1);
	m_FINM.SetBKColor(YELLOW);  
	m_FINM.SetTextErrColor(RED);
	m_FINM.SetSecState(ogPrivList.GetStat("MITARBEITERSTAMMDLG.m_FINM"));
	m_FINM.SetInitText(pomStf->Finm);
	//------------------------------------
	m_LANM.SetTypeToString("X(40)", 40, 1);
	m_LANM.SetBKColor(YELLOW);  
	m_LANM.SetTextErrColor(RED);
	m_LANM.SetSecState(ogPrivList.GetStat("MITARBEITERSTAMMDLG.m_LNAM"));
	m_LANM.SetInitText(pomStf->Lanm);
	//------------------------------------
	m_PENO.SetTypeToString("X(20)", 20, 1);
	m_PENO.SetBKColor(YELLOW);  
	m_PENO.SetTextErrColor(RED);
	m_PENO.SetSecState(ogPrivList.GetStat("MITARBEITERSTAMMDLG.m_PENO"));
	m_PENO.SetInitText(pomStf->Peno);
	this->omOldCode = pomStf->Peno;
	//------------------------------------
	m_PERC.SetTypeToString("X(3)", 3, 1);
	m_PERC.SetTextLimit(-1,3,true);
//	m_PERC.SetBKColor(YELLOW);  
	m_PERC.SetTextErrColor(RED);
	m_PERC.SetSecState(ogPrivList.GetStat("MITARBEITERSTAMMDLG.m_PERC"));
	m_PERC.SetInitText(pomStf->Perc);
	//------------------------------------

	m_SHNM.SetTypeToString("X(40)", 40, 0);
//	m_SHNM.SetBKColor(YELLOW);  
	m_SHNM.SetTextErrColor(RED);
	m_SHNM.SetSecState(ogPrivList.GetStat("MITARBEITERSTAMMDLG.m_SHNM"));
	m_SHNM.SetInitText(pomStf->Shnm);
	//------------------------------------
	m_TELD.SetTypeToString("X(60)", 60, 0);
	//m_TELD.SetBKColor(YELLOW);  
	m_TELD.SetTextErrColor(RED);
	m_TELD.SetSecState(ogPrivList.GetStat("MITARBEITERSTAMMDLG.m_TELD"));
	m_TELD.SetInitText(pomStf->Teld);
	//------------------------------------
	m_TELH.SetTypeToString("X(20)", 20, 0);
	//m_TELH.SetBKColor(YELLOW);  
	m_TELH.SetTextErrColor(RED);
	m_TELH.SetSecState(ogPrivList.GetStat("MITARBEITERSTAMMDLG.m_TELH"));
	m_TELH.SetInitText(pomStf->Telh);
	//------------------------------------
	m_TELP.SetTypeToString("X(20)", 20, 0);
	//m_TELP.SetBKColor(YELLOW);  
	m_TELP.SetTextErrColor(RED);
	m_TELP.SetSecState(ogPrivList.GetStat("MITARBEITERSTAMMDLG.m_TELP"));
	m_TELP.SetInitText(pomStf->Telp);
	//------------------------------------
	//*** 09.09.99 SHA ***
	m_REMA.SetTypeToString("X(60)", 60, 0);
	m_REMA.SetTextErrColor(RED);
	m_REMA.SetSecState(ogPrivList.GetStat("MITARBEITERSTAMMDLG.m_REMA"));
	m_REMA.SetInitText(pomStf->Rema);
	//------------------------------------
	m_PNOF.SetTypeToString("#(10)", 10, 0);
	m_PNOF.SetTextErrColor(RED);
	m_PNOF.SetSecState(ogPrivList.GetStat("MITARBEITERSTAMMDLG.m_PNOF"));
	m_PNOF.SetInitText(pomStf->Pnof);
	//------------------------------------
	m_KIND.SetTypeToString("A(1)", 1, 0);
	m_KIND.SetFormat("'F'|'M'");
	m_KIND.SetTextErrColor(RED);
	m_KIND.SetSecState(ogPrivList.GetStat("MITARBEITERSTAMMDLG.m_KIND"));
	m_KIND.SetInitText(pomStf->Kind);
	//------------------------------------
	m_KIDS.SetTypeToString("A(1)", 1, 0);
	m_KIDS.SetFormat("'Y'|'N'");
	m_KIDS.SetSecState(ogPrivList.GetStat("MITARBEITERSTAMMDLG.m_KIDS"));
	m_KIDS.SetTextErrColor(RED);
	if (strcmp(pomStf->Kids,"1") == 0)
		m_KIDS.SetInitText("Y");
	else
		m_KIDS.SetInitText("N");
	//------------------------------------
	m_NATI.SetTypeToString("X(3)", 3, 0);
	m_NATI.SetTextErrColor(RED);
	m_NATI.SetSecState(ogPrivList.GetStat("MITARBEITERSTAMMDLG.m_NATI"));
	m_NATI.SetInitText(pomStf->Nati);
	//------------------------------------
	m_CANO.SetTypeToString("X(20)", 20, 0);
	m_CANO.SetTextErrColor(RED);
	m_CANO.SetSecState(ogPrivList.GetStat("MITARBEITERSTAMMDLG.m_CANO"));
	m_CANO.SetInitText(pomStf->Cano);
	//------------------------------------
//	m_DOBK.SetTypeToString("#(1)", 1, 0);
	m_DOBK.SetTypeToDate(false);
	if(pomStf->Dobk.GetStatus() == COleDateTime::valid && CString(pomStf->Dobk) > "0")
		m_DOBK.SetInitText(pomStf->Dobk.Format("%d.%m.%Y"));
	else
		m_DOBK.SetInitText("");
	m_DOBK.SetTextErrColor(RED);
	m_DOBK.SetSecState(ogPrivList.GetStat("MITARBEITERSTAMMDLG.m_DOBK"));
//	m_DOBK.SetInitText(pomStf->Dobk);
	//------------------------------------
//	m_GEBU.SetTypeToString("X(10)", 10, 0);
	m_GEBU.SetTypeToDate(false);
	m_GEBU.SetTextErrColor(RED);
	m_GEBU.SetSecState(ogPrivList.GetStat("MITARBEITERSTAMMDLG.m_GEBU"));

	CString olTimeString = CString(pomStf->Gebu);
	COleDateTime olDate1;
	// String-Param pr�fen
	if ((olTimeString == "") || (olTimeString.GetLength() < 8) || (olTimeString.SpanExcluding("0123456789") != ""))
	{
		olDate1.SetStatus(COleDateTime::null);
	}
	else
	{
		// COleDateTime-Objekt aus SDAY erzeugen...
		COleDateTime olTime(atoi(olTimeString.Left(4).GetBuffer(0)), // Jahr (YYYY)
							atoi(olTimeString.Mid(4,2).GetBuffer(0)), // Monat (MM)
							atoi(olTimeString.Mid(6,2).GetBuffer(0)), // Tag (DD)
							0,0,0);	// Stunde, Minute, Sekunde
		olDate1 = olTime;
	}
	
	if(olDate1.GetStatus() == COleDateTime::valid)
	{
		m_GEBU.SetInitText(olDate1.Format("%d.%m.%Y"));
	}
	else
		m_GEBU.SetInitText("");
	//------------------------------------

	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_RELS",&m_RELS);
	if(strcmp(pomStf->Rels, "1") == 0)
	{
		m_RELS.SetCheck(1);
	}
	else
	{
		m_RELS.SetCheck(0);
	}

	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_THOR",&m_THOR);
	if(strcmp(pomStf->Tohr, "J") == 0)
	{
		m_THOR.SetCheck(1);
	}
	else
	{
		m_THOR.SetCheck(0);
	}

	InitTables();

	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BOX",pomOrgTable);

	CWnd *pWnd = GetDlgItem(IDC_BO1);
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BOX",pWnd);
	pWnd = GetDlgItem(IDC_BO2);
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BOX",pWnd);
	pWnd = GetDlgItem(IDC_BO3);
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BOX",pWnd);
	pWnd = GetDlgItem(IDC_BO4);
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BOX",pWnd);
	pWnd = GetDlgItem(IDC_BO5);
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BOX",pWnd);
	
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BFX",pomPfcTable);

	pWnd = GetDlgItem(IDC_BF1);
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BFX",pWnd);
	pWnd = GetDlgItem(IDC_BF2);
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BFX",pWnd);
	pWnd = GetDlgItem(IDC_BF3);
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BFX",pWnd);
	pWnd = GetDlgItem(IDC_BF4);
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BFX",pWnd);
	pWnd = GetDlgItem(IDC_BF5);
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BFX",pWnd);
	

	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BQX",pomPerTable);

	pWnd = GetDlgItem(IDC_BQ1);
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BQX",pWnd);
	pWnd = GetDlgItem(IDC_BQ2);
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BQX",pWnd);
	pWnd = GetDlgItem(IDC_BQ3);
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BQX",pWnd);
	pWnd = GetDlgItem(IDC_BQ4);
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BQX",pWnd);
	pWnd = GetDlgItem(IDC_BQ5);
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BQX",pWnd);
	
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BAX",pomCotTable);

	pWnd = GetDlgItem(IDC_BA1);
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BAX",pWnd);
	pWnd = GetDlgItem(IDC_BA2);
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BAX",pWnd);
	pWnd = GetDlgItem(IDC_BA3);
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BAX",pWnd);
	pWnd = GetDlgItem(IDC_BA4);
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BAX",pWnd);
	pWnd = GetDlgItem(IDC_BA5);
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BAX",pWnd);
	
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BGRP",pomWgpTable);

	pWnd = GetDlgItem(IDC_BGRP1);
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BGRP",pWnd);
	pWnd = GetDlgItem(IDC_BGRP2);
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BGRP",pWnd);
	pWnd = GetDlgItem(IDC_BGRP3);
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BGRP",pWnd);
	pWnd = GetDlgItem(IDC_BGRP4);
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BGRP",pWnd);
	pWnd = GetDlgItem(IDC_BGRP5);
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BGRP",pWnd);
	
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BPOL",pomPolTable);

	pWnd = GetDlgItem(IDC_BG1);
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BPOL",pWnd);
	pWnd = GetDlgItem(IDC_BG2);
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BPOL",pWnd);
	pWnd = GetDlgItem(IDC_BG3);
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BPOL",pWnd);
	pWnd = GetDlgItem(IDC_BG4);
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BPOL",pWnd);
	pWnd = GetDlgItem(IDC_BG5);
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BPOL",pWnd);
	
	pWnd = GetDlgItem(IDC_BR1);
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BSRE",pWnd);
	pWnd = GetDlgItem(IDC_BR2);
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BSRE",pWnd);
	pWnd = GetDlgItem(IDC_BR3);
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BSRE",pWnd);
	pWnd = GetDlgItem(IDC_BR4);
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BSRE",pWnd);
	pWnd = GetDlgItem(IDC_BR5);
	ogBasicData.SetWindowStat("MITARBEITERSTAMMDLG.m_BSRE",pWnd);
	
	pomStatus->SetWindowText("");
	AfxGetApp()->DoWaitCursor(-1);

	return TRUE;  
}

//----------------------------------------------------------------------------------------

void MitarbeiterStammDlg::InitTables()
{
	int ili;
	CRect olRectBorder;
//-------------------------Tabelle 1-------------------------
	omStatusText += CString("||||||");
	pomStatus->SetWindowText(omStatusText);

	m_HELP_ORG.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);
	int ilTab = (int)((olRectBorder.right - olRectBorder.left)/3);
	ilTab -= 10;

	pomOrgTable->SetHeaderSpacing(0);
	pomOrgTable->SetMiniTable();
	pomOrgTable->SetTableEditable(true);
    pomOrgTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomOrgTable->SetSelectMode(0);

	CCSPtrArray <TABLE_HEADER_COLUMN> omHeaderDataArray;
	TABLE_HEADER_COLUMN *prlHeader[10];//*** 27.08.99 SHA *** EX 5
	pomOrgTable->SetShowSelection(false);
	pomOrgTable->ResetContent();

	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = ilTab-16; 
	prlHeader[0]->Font = &ogCourier_Regular_10;
	prlHeader[0]->Text = CString(""); 

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = ilTab+8; 
	prlHeader[1]->Font = &ogCourier_Regular_10;
	prlHeader[1]->Text = CString("");

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = ilTab+8; 
	prlHeader[2]->Font = &ogCourier_Regular_10;
	prlHeader[2]->Text = CString("");

	prlHeader[3] = new TABLE_HEADER_COLUMN;
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 0; 
	prlHeader[3]->Font = &ogCourier_Regular_10;
	prlHeader[3]->Text = CString("");

	prlHeader[4] = new TABLE_HEADER_COLUMN;
	prlHeader[4]->Alignment = COLALIGN_CENTER;
	prlHeader[4]->Length = 0; 
	prlHeader[4]->Font = &ogCourier_Regular_10;
	prlHeader[4]->Text = CString("");

	prlHeader[5] = new TABLE_HEADER_COLUMN;
	prlHeader[5]->Alignment = COLALIGN_CENTER;
	prlHeader[5]->Length = 0; 
	prlHeader[5]->Font = &ogCourier_Regular_10;
	prlHeader[5]->Text = CString("");

	for(ili = 0; ili < 6; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}
	pomOrgTable->SetHeaderFields(omHeaderDataArray);
	pomOrgTable->SetDefaultSeparator();
	omHeaderDataArray.DeleteAll();
	pomOrgTable->DisplayTable(SB_VERT);

//-------------------------Tabelle 2-------------------------

	m_HELP_PFC.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);
	ilTab = (int)((olRectBorder.right - olRectBorder.left)/4);
	ilTab -= 10;
	pomPfcTable->SetHeaderSpacing(0);
	pomPfcTable->SetMiniTable();
	pomPfcTable->SetTableEditable(true);
    pomPfcTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomPfcTable->SetSelectMode(0);

	pomPfcTable->SetShowSelection(false);
	pomPfcTable->ResetContent();
	
	int ilElements = 0;

	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = 57; 
	prlHeader[0]->Font = &ogCourier_Regular_8;
	prlHeader[0]->Text = CString(""); 

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = 83; 
	prlHeader[1]->Font = &ogCourier_Regular_10;
	prlHeader[1]->Text = CString("");

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = 83; 
	prlHeader[2]->Font = &ogCourier_Regular_10;
	prlHeader[2]->Text = CString("");

	prlHeader[3] = new TABLE_HEADER_COLUMN;		//*** PRIO ***
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 21; 
	prlHeader[3]->Font = &ogCourier_Regular_10;
	prlHeader[3]->Text = CString("");

	prlHeader[4] = new TABLE_HEADER_COLUMN;
	prlHeader[4]->Alignment = COLALIGN_CENTER;
	prlHeader[4]->Length = 0; 
	prlHeader[4]->Font = &ogCourier_Regular_10;
	prlHeader[4]->Text = CString("");

	ilElements = 5;

	for(ili = 0; ili < ilElements ; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}
	pomPfcTable->SetHeaderFields(omHeaderDataArray);
	pomPfcTable->SetDefaultSeparator();
	pomPfcTable->SetTableEditable(true);
	omHeaderDataArray.DeleteAll();
	pomPfcTable->DisplayTable(SB_VERT);
	omHeaderDataArray.DeleteAll();

//-------------------------Tabelle 3-------------------------

	m_HELP_PER.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);
	ilTab = (int)((olRectBorder.right - olRectBorder.left)/3);
	ilTab -= 10;
	pomPerTable->SetHeaderSpacing(0);
	pomPerTable->SetMiniTable();
	pomPerTable->SetTableEditable(true);
    pomPerTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomPerTable->SetSelectMode(0);


	pomPerTable->SetShowSelection(false);
	pomPerTable->ResetContent();
	

	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = ilTab-16; 
	prlHeader[0]->Font = &ogCourier_Regular_10;
	prlHeader[0]->Text = CString(""); 

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = ilTab+8; 
	prlHeader[1]->Font = &ogCourier_Regular_10;
	prlHeader[1]->Text = CString("");

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = ilTab+8; 
	prlHeader[2]->Font = &ogCourier_Regular_10;
	prlHeader[2]->Text = CString("");

	prlHeader[3] = new TABLE_HEADER_COLUMN;
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 0; 
	prlHeader[3]->Font = &ogCourier_Regular_10;
	prlHeader[3]->Text = CString("");



	for(ili = 0; ili < 4; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}
	pomPerTable->SetHeaderFields(omHeaderDataArray);
	pomPerTable->SetDefaultSeparator();
	pomPerTable->SetTableEditable(true);
	omHeaderDataArray.DeleteAll();
	pomPerTable->DisplayTable(SB_VERT);
	omHeaderDataArray.DeleteAll();

//-------------------------Tabelle 4-------------------------

	m_HELP_COT.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);
	ilTab = (int)((olRectBorder.right - olRectBorder.left)/3);
	ilTab -= 10;
	pomCotTable->SetHeaderSpacing(0);
	pomCotTable->SetMiniTable();
	pomCotTable->SetTableEditable(true);
    pomCotTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomCotTable->SetSelectMode(0);


	pomCotTable->SetShowSelection(false);
	pomCotTable->ResetContent();
	

	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = ilTab-16; 
	prlHeader[0]->Font = &ogCourier_Regular_10;
	prlHeader[0]->Text = CString(""); 

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = ilTab+8; 
	prlHeader[1]->Font = &ogCourier_Regular_10;
	prlHeader[1]->Text = CString("");

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = ilTab+8; 
	prlHeader[2]->Font = &ogCourier_Regular_10;
	prlHeader[2]->Text = CString("");

	prlHeader[3] = new TABLE_HEADER_COLUMN;
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 0; 
	prlHeader[3]->Font = &ogCourier_Regular_10;
	prlHeader[3]->Text = CString("");

	prlHeader[4] = new TABLE_HEADER_COLUMN;
	prlHeader[4]->Alignment = COLALIGN_CENTER;
	prlHeader[4]->Length = 0; 
	prlHeader[4]->Font = &ogCourier_Regular_10;
	prlHeader[4]->Text = CString("");

	prlHeader[5] = new TABLE_HEADER_COLUMN;
	prlHeader[5]->Alignment = COLALIGN_CENTER;
	prlHeader[5]->Length = 0; 
	prlHeader[5]->Font = &ogCourier_Regular_10;
	prlHeader[5]->Text = CString("");

/*	prlHeader[6] = new TABLE_HEADER_COLUMN;
	prlHeader[6]->Alignment = COLALIGN_CENTER;
	prlHeader[6]->Length = 0; 
	prlHeader[6]->Font = &ogCourier_Regular_10;
	prlHeader[6]->Text = CString("");

	prlHeader[7] = new TABLE_HEADER_COLUMN;
	prlHeader[7]->Alignment = COLALIGN_CENTER;
	prlHeader[7]->Length = 0; 
	prlHeader[7]->Font = &ogCourier_Regular_10;
	prlHeader[7]->Text = CString("");

	prlHeader[8] = new TABLE_HEADER_COLUMN;
	prlHeader[8]->Alignment = COLALIGN_CENTER;
	prlHeader[8]->Length = 0; 
	prlHeader[8]->Font = &ogCourier_Regular_10;
	prlHeader[8]->Text = CString("");

	prlHeader[9] = new TABLE_HEADER_COLUMN;
	prlHeader[9]->Alignment = COLALIGN_CENTER;
	prlHeader[9]->Length = 0; 
	prlHeader[9]->Font = &ogCourier_Regular_10;
	prlHeader[9]->Text = CString("");*/

	for(ili = 0; ili < 6/*10*/; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}
	pomCotTable->SetHeaderFields(omHeaderDataArray);
	pomCotTable->SetDefaultSeparator();
	pomCotTable->SetTableEditable(true);
	omHeaderDataArray.DeleteAll();
	pomCotTable->DisplayTable(SB_VERT);
	omHeaderDataArray.DeleteAll();

//-------------------------Tabelle 5 -------------------------

	m_HELP_TEA.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);
	ilTab = (int)((olRectBorder.right - olRectBorder.left)/3);
	ilTab -= 10;
	pomPolTable->SetHeaderSpacing(0);
	pomPolTable->SetMiniTable();
	pomPolTable->SetTableEditable(true);
    pomPolTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomPolTable->SetSelectMode(0);


	pomPolTable->SetShowSelection(false);
	pomPolTable->ResetContent();
	

	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = ilTab-8; 
	prlHeader[0]->Font = &ogCourier_Regular_10;
	prlHeader[0]->Text = CString(""); 

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = ilTab+12; 
	prlHeader[1]->Font = &ogCourier_Regular_10;
	prlHeader[1]->Text = CString("");

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = ilTab+8; 
	prlHeader[2]->Font = &ogCourier_Regular_10;
	prlHeader[2]->Text = CString("");

	prlHeader[3] = new TABLE_HEADER_COLUMN;
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 0; 
	prlHeader[3]->Font = &ogCourier_Regular_10;
	prlHeader[3]->Text = CString("");

	for(ili = 0; ili < 4; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}
	pomPolTable->SetHeaderFields(omHeaderDataArray);
	pomPolTable->SetDefaultSeparator();
	pomPolTable->SetTableEditable(true);
	omHeaderDataArray.DeleteAll();
	pomPolTable->DisplayTable(SB_VERT);
	omHeaderDataArray.DeleteAll();


//-------------------------Tabelle 6-------------------------

	m_HELP_GRP.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);
	ilTab = (int)((olRectBorder.right - olRectBorder.left)/3);
	ilTab -= 10;
	pomWgpTable->SetHeaderSpacing(0);
	pomWgpTable->SetMiniTable();
	pomWgpTable->SetTableEditable(true);
    pomWgpTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomWgpTable->SetSelectMode(0);


	pomWgpTable->SetShowSelection(false);
	pomWgpTable->ResetContent();
	

	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = ilTab-16; 
	prlHeader[0]->Font = &ogCourier_Regular_10;
	prlHeader[0]->Text = CString(""); 

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = ilTab+8; 
	prlHeader[1]->Font = &ogCourier_Regular_10;
	prlHeader[1]->Text = CString("");

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = ilTab+8; 
	prlHeader[2]->Font = &ogCourier_Regular_10;
	prlHeader[2]->Text = CString("");

	prlHeader[3] = new TABLE_HEADER_COLUMN;
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 0; 
	prlHeader[3]->Font = &ogCourier_Regular_10;
	prlHeader[3]->Text = CString("");

	for(ili = 0; ili < 4; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}
	pomWgpTable->SetHeaderFields(omHeaderDataArray);
	pomWgpTable->SetDefaultSeparator();
	pomWgpTable->SetTableEditable(true);
	omHeaderDataArray.DeleteAll();
	pomWgpTable->DisplayTable(SB_VERT);
	omHeaderDataArray.DeleteAll();

//-----------------------------------------------------------------
	m_HELP_SRE.GetWindowRect( olRectBorder );
	ScreenToClient(olRectBorder);
	olRectBorder.DeflateRect(2,2);
	ilTab = (int)((olRectBorder.right - olRectBorder.left)/3);
	ilTab -= 10;
	pomSreTable->SetHeaderSpacing(0);
	pomSreTable->SetMiniTable();
	pomSreTable->SetTableEditable(true);
    pomSreTable->SetTableData(this, olRectBorder.left, olRectBorder.right, olRectBorder.top, olRectBorder.bottom);
	pomSreTable->SetSelectMode(0);

	pomSreTable->SetShowSelection(false);
	pomSreTable->ResetContent();

	prlHeader[0] = new TABLE_HEADER_COLUMN;
	prlHeader[0]->Alignment = COLALIGN_CENTER;
	prlHeader[0]->Length = ilTab-16; 
	prlHeader[0]->Font = &ogCourier_Regular_10;
	prlHeader[0]->Text = CString(""); 

	prlHeader[1] = new TABLE_HEADER_COLUMN;
	prlHeader[1]->Alignment = COLALIGN_CENTER;
	prlHeader[1]->Length = ilTab+12; 
	prlHeader[1]->Font = &ogCourier_Regular_10;
	prlHeader[1]->Text = CString("");

	prlHeader[2] = new TABLE_HEADER_COLUMN;
	prlHeader[2]->Alignment = COLALIGN_CENTER;
	prlHeader[2]->Length = ilTab+8; 
	prlHeader[2]->Font = &ogCourier_Regular_10;
	prlHeader[2]->Text = CString("");

	prlHeader[3] = new TABLE_HEADER_COLUMN;
	prlHeader[3]->Alignment = COLALIGN_CENTER;
	prlHeader[3]->Length = 0; 
	prlHeader[3]->Font = &ogCourier_Regular_10;
	prlHeader[3]->Text = CString("");

	for(ili = 0; ili < 4; ili++)
	{
		omHeaderDataArray.Add(prlHeader[ili]);
	}
	pomSreTable->SetHeaderFields(omHeaderDataArray);
	pomSreTable->SetDefaultSeparator();
	pomSreTable->SetTableEditable(true);
	omHeaderDataArray.DeleteAll();
	pomSreTable->DisplayTable(SB_VERT);
	omHeaderDataArray.DeleteAll();

//-------------------------Tabelle Ende-------------------------

//--------------------------------------------------------------
// Attribute f�r die einzelnen Tables setzen
// Tabelle ORG
	omStatusText += CString("||||||");
	pomStatus->SetWindowText(omStatusText);

	CCSEDIT_ATTRIB rlAttribC1;
	CCSEDIT_ATTRIB rlAttribC2;
	CCSEDIT_ATTRIB rlAttribC3;
	CCSEDIT_ATTRIB rlAttribC4;
	CCSEDIT_ATTRIB rlAttribC5;
	CCSEDIT_ATTRIB rlAttribC6;
	CCSEDIT_ATTRIB rlAttribC7;
	CCSEDIT_ATTRIB rlAttribC8;
	
	rlAttribC1.Format = "X(8)";
	rlAttribC1.TextMinLenght = 0;
	rlAttribC1.TextMaxLenght = 8;
	rlAttribC1.Style = ES_UPPERCASE;
	
	rlAttribC2.Type = KT_DATE;
	rlAttribC2.ChangeDay	= true;
	rlAttribC2.TextMaxLenght = 10;

	rlAttribC3.Type = KT_DATE;
	rlAttribC3.ChangeDay	= true;
	rlAttribC3.TextMaxLenght = 10;

	rlAttribC8.Format = "X(2)";
	rlAttribC8.TextMinLenght = 0;
	rlAttribC8.TextMaxLenght = 2;
	rlAttribC8.Style = ES_UPPERCASE;



	CCSPtrArray<TABLE_COLUMN> olColList;
	char pclText[20];
	TABLE_COLUMN rlColumnData;
	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogMSSansSerif_Bold_8;//ogMS_Sans_Serif_8;
	rlColumnData.VerticalSeparator = SEPA_NORMAL;
	rlColumnData.HorizontalSeparator = SEPA_NONE;
	rlColumnData.Alignment = COLALIGN_CENTER;

	for(int i = 0; i < pomSorData->GetSize(); i++)
	{
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_LIKEVERTICAL;

		rlColumnData.Text = CString(pomSorData->GetAt(i).Code);
		rlColumnData.EditAttrib = rlAttribC1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = CString("");
		if(pomSorData->GetAt(i).Vpfr.GetStatus() == COleDateTime::valid)
			rlColumnData.Text = CString(pomSorData->GetAt(i).Vpfr.Format("%d.%m.%Y"));
		rlColumnData.EditAttrib = rlAttribC2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = CString("");
		if(pomSorData->GetAt(i).Vpto.GetStatus() == COleDateTime::valid)
			rlColumnData.Text = CString(pomSorData->GetAt(i).Vpto.Format("%d.%m.%Y"));
		rlColumnData.EditAttrib = rlAttribC3;
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_NONE;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		
		if(pomSorData->GetAt(i).Urno != 0)
		{
			sprintf(pclText, "%ld", pomSorData->GetAt(i).Urno);
			rlColumnData.Text = CString(pclText);
		}
		else
		{
			rlColumnData.Text = CString("");
		}
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_NONE;
		rlColumnData.HorizontalSeparator = SEPA_NONE;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		//Odgc
		//Lead

		rlColumnData.Text = CString(pomSorData->GetAt(i).Odgc);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = CString(pomSorData->GetAt(i).Lead);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomOrgTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll();
	}
	for(i = pomSorData->GetSize(); i < 100; i++)
	{
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_LIKEVERTICAL;
		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC3;
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_NONE;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = CString("");
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_NONE;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		olColList.NewAt(olColList.GetSize(), rlColumnData);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomOrgTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll();
	}
	pomOrgTable->DisplayTable(SB_VERT);

	pomOrgTable->SetTableEditable(true);
	pomOrgTable->SetColumnEditable(3, false);
	pomOrgTable->SetColumnEditable(4, false);
	pomOrgTable->SetColumnEditable(5, false);

//-------------------------------------------------------------------------
// Attribute f�r die einzelnen Tables setzen
// Tabelle PFC

	rlAttribC1.Format = "X(8)";
	rlAttribC1.TextMinLenght = 0;
	rlAttribC1.TextMaxLenght = 8;
	rlAttribC1.Style = ES_UPPERCASE;
	
	rlAttribC2.Type = KT_DATE;
	rlAttribC2.ChangeDay	= true;
	rlAttribC2.TextMaxLenght = 10;

	rlAttribC3.Type = KT_DATE;
	rlAttribC3.ChangeDay	= true;
	rlAttribC3.TextMaxLenght = 10;


/*#ifndef CLIENT_ZRH
	//*** 27.08.99 SHA ***
	rlAttribC4.Format = "X(3)";
	rlAttribC4.TextMinLenght = 0;
	rlAttribC4.TextMaxLenght = 3;
	rlAttribC4.Style = ES_UPPERCASE;

	rlAttribC5.Format = "X(5)";
	rlAttribC5.TextMinLenght = 0;
	rlAttribC5.TextMaxLenght = 5;
	rlAttribC5.Style = ES_UPPERCASE;

	rlAttribC6.Format = "X(2)";
	rlAttribC6.TextMinLenght = 0;
	rlAttribC6.TextMaxLenght = 2;
	rlAttribC6.Style = ES_UPPERCASE;

	rlAttribC7.Format = "X(3)";
	rlAttribC7.TextMinLenght = 0;
	rlAttribC7.TextMaxLenght = 3;
	rlAttribC7.Style = ES_UPPERCASE;
#endif*/

	rlAttribC8.Format = "X(2)";
	rlAttribC8.TextMinLenght = 0;
	rlAttribC8.TextMaxLenght = 2;
	rlAttribC8.Style = ES_UPPERCASE;

	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogMSSansSerif_Bold_8;//ogMS_Sans_Serif_8;
	rlColumnData.VerticalSeparator = SEPA_NORMAL;
	rlColumnData.HorizontalSeparator = SEPA_NONE;
	rlColumnData.Alignment = COLALIGN_CENTER;

	for(i = 0; i < pomSpfData->GetSize(); i++)
	{
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_LIKEVERTICAL;

		rlColumnData.Text = CString(pomSpfData->GetAt(i).Code);
		rlColumnData.EditAttrib = rlAttribC1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = CString("");
		if(pomSpfData->GetAt(i).Vpfr.GetStatus() == COleDateTime::valid)
			rlColumnData.Text = CString(pomSpfData->GetAt(i).Vpfr.Format("%d.%m.%Y"));
		rlColumnData.EditAttrib = rlAttribC2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = CString("");
		if(pomSpfData->GetAt(i).Vpto.GetStatus() == COleDateTime::valid)
			rlColumnData.Text = CString(pomSpfData->GetAt(i).Vpto.Format("%d.%m.%Y"));
		rlColumnData.EditAttrib = rlAttribC3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = CString(pomSpfData->GetAt(i).Prio);
		rlColumnData.EditAttrib = rlAttribC8;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		
		if(pomSpfData->GetAt(i).Urno != 0)
		{
			char pclText[20];
			sprintf(pclText, "%ld", pomSpfData->GetAt(i).Urno);
			rlColumnData.Text = CString(pclText);
		}
		else
		{
			rlColumnData.Text = CString("");
		}
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_NONE;
		olColList.NewAt(olColList.GetSize(), rlColumnData);


/*#ifndef CLIENT_ZRH
		//*** 27.08.99 SHA ***
		rlColumnData.Text = CString(pomSpfData->GetAt(i).Act3);
		rlColumnData.EditAttrib = rlAttribC4;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = CString(pomSpfData->GetAt(i).Act5);
		rlColumnData.EditAttrib = rlAttribC5;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = CString(pomSpfData->GetAt(i).Alc2);
		rlColumnData.EditAttrib = rlAttribC6;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = CString(pomSpfData->GetAt(i).Alc3);
		rlColumnData.EditAttrib = rlAttribC7;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
#endif*/

		pomPfcTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll();
	}
	for(i = pomSpfData->GetSize(); i < 100; i++)
	{
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_LIKEVERTICAL;
		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC8;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = CString("");
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_NONE;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

/*#ifndef CLIENT_ZRH
		//*** 27.08.99 SHA ***
		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC4;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC5;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC6;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC7;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
#endif*/

		pomPfcTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll();
	}
	pomPfcTable->DisplayTable(SB_VERT);
	pomPfcTable->SetColumnEditable(4, false);

/*#ifndef CLIENT_ZRH
	pomPfcTable->SetColumnEditable(4, false);
	pomPfcTable->SetColumnEditable(5, false);
	pomPfcTable->SetColumnEditable(6, false);
	pomPfcTable->SetColumnEditable(7, false);
#endif*/

//-------------------------------------------------------------------------
// Attribute f�r die einzelnen Tables setzen
// Tabelle PER

	rlAttribC1.Format = "X(5)";
	rlAttribC1.TextMinLenght = 0;
	rlAttribC1.TextMaxLenght = 5;
	rlAttribC1.Style = ES_UPPERCASE;
	
	rlAttribC2.Type = KT_DATE;
	rlAttribC2.ChangeDay	= true;
	rlAttribC2.TextMaxLenght = 10;

	rlAttribC3.Type = KT_DATE;
	rlAttribC3.ChangeDay	= true;
	rlAttribC3.TextMaxLenght = 10;


	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogMSSansSerif_Bold_8;//ogMS_Sans_Serif_8;
	rlColumnData.VerticalSeparator = SEPA_NORMAL;
	rlColumnData.HorizontalSeparator = SEPA_NONE;
	rlColumnData.Alignment = COLALIGN_CENTER;

	for(i = 0; i < pomSpeData->GetSize(); i++)
	{
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_LIKEVERTICAL;
		rlColumnData.Text = CString(pomSpeData->GetAt(i).Code);
		rlColumnData.EditAttrib = rlAttribC1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		if(pomSpeData->GetAt(i).Vpfr.GetStatus() == COleDateTime::valid)
			rlColumnData.Text = CString(pomSpeData->GetAt(i).Vpfr.Format("%d.%m.%Y"));
		rlColumnData.EditAttrib = rlAttribC2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		if(pomSpeData->GetAt(i).Vpto.GetStatus() == COleDateTime::valid)
			rlColumnData.Text = CString(pomSpeData->GetAt(i).Vpto.Format("%d.%m.%Y"));
		rlColumnData.EditAttrib = rlAttribC3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		if(pomSpeData->GetAt(i).Urno != 0)
		{
			char pclText[20];
			sprintf(pclText, "%ld", pomSpeData->GetAt(i).Urno);
			rlColumnData.Text = CString(pclText);
		}
		else
		{
			rlColumnData.Text = CString("");
		}
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_NONE;
		olColList.NewAt(olColList.GetSize(), rlColumnData);


		pomPerTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll();
	}
	for(i = pomSpeData->GetSize(); i < 100; i++)
	{
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_LIKEVERTICAL;
		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = CString("");
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_NONE;
		olColList.NewAt(olColList.GetSize(), rlColumnData);


		pomPerTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll();
	}
	pomPerTable->DisplayTable(SB_VERT);
	pomPerTable->SetColumnEditable(3, false);


//-------------------------------------------------------------------------
// Attribute f�r die einzelnen Tables setzen
// Tabelle COT

	rlAttribC1.Format = "X(8)";
	rlAttribC1.TextMinLenght = 0;
	rlAttribC1.TextMaxLenght = 8;
	rlAttribC1.Style = ES_UPPERCASE;
	
	rlAttribC2.Type = KT_DATE;
	rlAttribC2.ChangeDay	= true;
	rlAttribC2.TextMaxLenght = 10;

	rlAttribC3.Type = KT_DATE;
	rlAttribC3.ChangeDay	= true;
	rlAttribC3.TextMaxLenght = 10;

	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogMSSansSerif_Bold_8;//ogMS_Sans_Serif_8;
	rlColumnData.VerticalSeparator = SEPA_NORMAL;
	rlColumnData.HorizontalSeparator = SEPA_NONE;
	rlColumnData.Alignment = COLALIGN_CENTER;

	for(i = 0; i < pomScoData->GetSize(); i++)
	{
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_LIKEVERTICAL;
		rlColumnData.Text = CString(pomScoData->GetAt(i).Code);
		rlColumnData.EditAttrib = rlAttribC1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		if(pomScoData->GetAt(i).Vpfr.GetStatus() == COleDateTime::valid)
			rlColumnData.Text = CString(pomScoData->GetAt(i).Vpfr.Format("%d.%m.%Y"));
		rlColumnData.EditAttrib = rlAttribC2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		if(pomScoData->GetAt(i).Vpto.GetStatus() == COleDateTime::valid)
			rlColumnData.Text = CString(pomScoData->GetAt(i).Vpto.Format("%d.%m.%Y"));
		rlColumnData.EditAttrib = rlAttribC3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		if(pomScoData->GetAt(i).Urno != 0)
		{
			char pclText[20];
			sprintf(pclText, "%ld", pomScoData->GetAt(i).Urno);
			rlColumnData.Text = CString(pclText);
		}
		else
		{
			rlColumnData.Text = CString("");
		}
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_NONE;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		//Kost
		//Cweh

		rlColumnData.Text = CString(pomScoData->GetAt(i).Kost);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = CString(pomScoData->GetAt(i).Cweh);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomCotTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll();
	}
	for(i = pomScoData->GetSize(); i < 100; i++)
	{
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_LIKEVERTICAL;
		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = CString("");
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_NONE;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		olColList.NewAt(olColList.GetSize(), rlColumnData);
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomCotTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll();
	}
	pomCotTable->DisplayTable(SB_VERT);
	pomCotTable->SetColumnEditable(3, false);
	pomCotTable->SetColumnEditable(4, false);
	pomCotTable->SetColumnEditable(5, false);

//-------------------------------------------------------------------------
// Attribute f�r die einzelnen Tables setzen
// Tabelle POL

	rlAttribC1.Format = "X(12)";
	rlAttribC1.TextMinLenght = 0;
	rlAttribC1.TextMaxLenght = 12;
	rlAttribC1.Style = 0;			// ES_UPPERCASE removed due to 'Without Pool' Name
	
	rlAttribC2.Type = KT_DATE;
	rlAttribC2.ChangeDay	= true;
	rlAttribC2.TextMaxLenght = 10;

	rlAttribC3.Type = KT_DATE;
	rlAttribC3.ChangeDay	= true;
	rlAttribC3.TextMaxLenght = 10;

	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogMSSansSerif_Bold_8;//ogMS_Sans_Serif_8;
	rlColumnData.VerticalSeparator = SEPA_NORMAL;
	rlColumnData.HorizontalSeparator = SEPA_NONE;
	rlColumnData.Alignment = COLALIGN_CENTER;

	for(i = 0; i < pomSdaData->GetSize(); i++)
	{
		POLDATA *polData = ogPolData.GetPolByUrno(pomSdaData->GetAt(i).Upol);
		if (!polData)
			continue;

		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_LIKEVERTICAL;
		rlColumnData.Text = CString(polData->Name);
		rlColumnData.EditAttrib = rlAttribC1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		if(pomSdaData->GetAt(i).Vpfr.GetStatus() == COleDateTime::valid)
			rlColumnData.Text = CString(pomSdaData->GetAt(i).Vpfr.Format("%d.%m.%Y"));
		rlColumnData.EditAttrib = rlAttribC2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		if(pomSdaData->GetAt(i).Vpto.GetStatus() == COleDateTime::valid)
			rlColumnData.Text = CString(pomSdaData->GetAt(i).Vpto.Format("%d.%m.%Y"));
		rlColumnData.EditAttrib = rlAttribC3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		if(pomSdaData->GetAt(i).Urno != 0)
		{
			char pclText[20];
			sprintf(pclText, "%ld", pomSdaData->GetAt(i).Urno);
			rlColumnData.Text = CString(pclText);
		}
		else
		{
			rlColumnData.Text = CString("");
		}
		
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_NONE;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		pomPolTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll();
	}

	for(i = pomPolTable->GetLinesCount(); i < 100; i++)
	{
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_LIKEVERTICAL;
		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_NONE;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		pomPolTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll(); 
	} 
	pomPolTable->DisplayTable(SB_VERT);
	pomPolTable->SetColumnEditable(3, false);
//-------------------------------------------------------------------------
// Attribute f�r die einzelnen Tables setzen
// Tabelle WPG

	rlAttribC1.Format = "X(5)";
	rlAttribC1.TextMinLenght = 0;
	rlAttribC1.TextMaxLenght = 5;
	rlAttribC1.Style = ES_UPPERCASE;
	
	rlAttribC2.Type = KT_DATE;
	rlAttribC2.ChangeDay	= true;
	rlAttribC2.TextMaxLenght = 10;

	rlAttribC3.Type = KT_DATE;
	rlAttribC3.ChangeDay	= true;
	rlAttribC3.TextMaxLenght = 10;

	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogMSSansSerif_Bold_8;//ogMS_Sans_Serif_8;
	rlColumnData.VerticalSeparator = SEPA_NORMAL;
	rlColumnData.HorizontalSeparator = SEPA_NONE;
	rlColumnData.Alignment = COLALIGN_CENTER;

	for(i = 0; i < pomSwgData->GetSize(); i++)
	{
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_LIKEVERTICAL;
		rlColumnData.Text = CString(pomSwgData->GetAt(i).Code);
		rlColumnData.EditAttrib = rlAttribC1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		if(pomSwgData->GetAt(i).Vpfr.GetStatus() == COleDateTime::valid)
			rlColumnData.Text = CString(pomSwgData->GetAt(i).Vpfr.Format("%d.%m.%Y"));
		rlColumnData.EditAttrib = rlAttribC2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		if(pomSwgData->GetAt(i).Vpto.GetStatus() == COleDateTime::valid)
			rlColumnData.Text = CString(pomSwgData->GetAt(i).Vpto.Format("%d.%m.%Y"));
		rlColumnData.EditAttrib = rlAttribC3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		if(pomSwgData->GetAt(i).Urno != 0)
		{
			char pclText[20];
			sprintf(pclText, "%ld", pomSwgData->GetAt(i).Urno);
			rlColumnData.Text = CString(pclText);
		}
		else
		{
			rlColumnData.Text = CString("");
		}
		
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_NONE;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		pomWgpTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll();
	}
	for(i = pomSwgData->GetSize(); i < 100; i++)
	{
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_LIKEVERTICAL;
		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_NONE;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		pomWgpTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll(); 
	} 
	pomWgpTable->DisplayTable(SB_VERT);
	pomWgpTable->SetColumnEditable(3, false);

//-------------------------------------------------------------------------
// Attribute f�r die einzelnen Tables setzen
// Tabelle SRE

	rlAttribC1.Format = "X(1)";
	rlAttribC1.TextMinLenght = 0;
	rlAttribC1.TextMaxLenght = 5;
	rlAttribC1.Style = ES_UPPERCASE;
	
	rlAttribC2.Type = KT_DATE;
	rlAttribC2.ChangeDay	= true;
	rlAttribC2.TextMaxLenght = 10;

	rlAttribC3.Type = KT_DATE;
	rlAttribC3.ChangeDay	= true;
	rlAttribC3.TextMaxLenght = 10;


	rlColumnData.SeparatorType = SEPA_NONE;
	rlColumnData.Font = &ogMSSansSerif_Bold_8;//ogMS_Sans_Serif_8;
	rlColumnData.VerticalSeparator = SEPA_NORMAL;
	rlColumnData.HorizontalSeparator = SEPA_NONE;
	rlColumnData.Alignment = COLALIGN_CENTER;

	for(i = 0; i < pomSreData->GetSize(); i++)
	{
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_LIKEVERTICAL;
		
		if(strcmp(pomSreData->GetAt(i).Regi, "0") == 0)
			rlColumnData.Text = CString("I");
		else if(strcmp(pomSreData->GetAt(i).Regi, "1") == 0)
			rlColumnData.Text = CString("R");
		else if (strcmp(pomSreData->GetAt(i).Regi, "2") == 0 && "SIN" == ogParData.GetParValue("ROSTER","ID_PROD_CUSTOMER"))
		{
			rlColumnData.Text = CString("S");
		}
		else
		{
			rlColumnData.Text = CString(" ");
		}

		rlColumnData.EditAttrib = rlAttribC1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		if(pomSreData->GetAt(i).Vpfr.GetStatus() == COleDateTime::valid)
			rlColumnData.Text = CString(pomSreData->GetAt(i).Vpfr.Format("%d.%m.%Y"));
		rlColumnData.EditAttrib = rlAttribC2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		if(pomSreData->GetAt(i).Vpto.GetStatus() == COleDateTime::valid)
			rlColumnData.Text = CString(pomSreData->GetAt(i).Vpto.Format("%d.%m.%Y"));
		rlColumnData.EditAttrib = rlAttribC3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		if(pomSreData->GetAt(i).Urno != 0)
		{
			char pclText[20];
			sprintf(pclText, "%ld", pomSreData->GetAt(i).Urno);
			rlColumnData.Text = CString(pclText);
		}
		else
		{
			rlColumnData.Text = CString("");
		}
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_NONE;
		olColList.NewAt(olColList.GetSize(), rlColumnData);


		pomSreTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll();
	}
	for(i = pomSreData->GetSize(); i < 100; i++)
	{
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_LIKEVERTICAL;
		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString("");
		rlColumnData.EditAttrib = rlAttribC3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		rlColumnData.Text = CString("");
		rlColumnData.SeparatorType = SEPA_NONE;
		rlColumnData.VerticalSeparator = SEPA_NONE;
		olColList.NewAt(olColList.GetSize(), rlColumnData);


		pomSreTable->AddTextLine(olColList, (void*)(NULL));
		olColList.DeleteAll();
	}
	pomSreTable->DisplayTable(SB_VERT);
	pomSreTable->SetColumnEditable(3, false);
}

//***************************************************************************
// On OK
//***************************************************************************

void MitarbeiterStammDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText,olNewCode;
	bool ilStatus = true;
	if(m_FINM.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_FINM.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING69) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING69) + ogNotFormat;
		}
	}


	if(m_LANM.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_LANM.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING288) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING288) + ogNotFormat;
		}
	}
	if(m_PENO.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_PENO.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING71) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING71) + ogNotFormat;
		}
	}
	if(m_PERC.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_PERC.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING72) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING72) + ogNotFormat;
		}
	}
	if(m_SHNM.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_SHNM.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING301) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING301) + ogNotFormat;
		}
	}
	if(m_DOEM_D.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_DOEM_D.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING67) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING67) + ogNotFormat;
		}
	}
	if(m_DODDM_D.GetStatus() == false) 
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING65) + ogNotFormat;
	}
	if(m_TELD.GetStatus() == false) 
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING73) + ogNotFormat;
	}
	if(m_TELH.GetStatus() == false) 
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING74) + ogNotFormat;
	}
	if(m_TELP.GetStatus() == false) 
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING75) + ogNotFormat;
	}

	CString olTableErrorText;
	int i;
	//---------------------------------------------------------------------------
	for(i = 0; i < 100; i++)
	{
		int ilFilledCells = 0;
		bool blLineOK = true;
		char pclZeile[10], pclSpalte[10];
		CCSPtrArray<TABLE_COLUMN> olLine;
		//////////////////////////////////////////////////////////////////////////////////////////
		pomOrgTable->GetTextLineColumns(&olLine, i);
		for(int k = 0; k < olLine.GetSize()-1; k++)
		{
			if(olLine[k].Text != "")
			{
				ilFilledCells++;
			}
			sprintf(pclZeile, "%d", i+1);
			blLineOK = pomOrgTable->GetCellStatus(i, k);
			if(blLineOK == false)
			{
				ilStatus = false;
				sprintf(pclSpalte, "%d", k+1);
				if(k == 0 || k == 1 || k == 2)
				{
					olTableErrorText += LoadStg(IDS_STRING80) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING86) + CString(pclSpalte) + LoadStg(IDS_STRING87);
				}
			}
		}
		if(ilFilledCells != 0 && ilFilledCells < 2 && olLine[1].Text != CString(""))
		{
			ilStatus = false;
			olTableErrorText += LoadStg(IDS_STRING80) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING88);
		}

		//*** 30.09.99 SHA ***
		//*** CHECK IF THERE IS ANY ORG WITHOUT VALID FROM DATE ***
		if (olLine[0].Text != CString("") && olLine[1].Text == CString(""))
		{
			olTableErrorText += CString("<") + CString(pclZeile) + CString(">") + LoadStg(IDS_STRING723);
			ilStatus = false;
		}

		if(olLine[1].Text != CString("") && olLine[2].Text != CString(""))
		{
			COleDateTime olFrom = OleDateStringToDate(olLine[1].Text);
			COleDateTime olTo = OleDateStringToDate(olLine[2].Text);


			if(olFrom.GetStatus() == COleDateTime::invalid || olTo.GetStatus() == COleDateTime::invalid)
			{
				ilStatus = false;
				olTableErrorText += LoadStg(IDS_STRING80) + LoadStg(IDS_STRING89) + CString(pclZeile);
			}
			else
			{
				if(olTo < olFrom)
				{
					ilStatus = false;
					olTableErrorText += LoadStg(IDS_STRING80) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING90);
					pomOrgTable->SetTextColumnColor(i, 1, COLORREF(RED), COLORREF(WHITE));
					pomOrgTable->SetTextColumnColor(i, 2, COLORREF(RED), COLORREF(WHITE));
				}
			}
		}
		olLine.RemoveAll();
	}

	if (ilStatus == true && !this->CheckDateRange(pomOrgTable,1,2,-1,LoadStg(IDS_STRING80),olTableErrorText))
	{
		ilStatus = false;
	}

	//---------------------------------------------------------------------------
	for(i = 0; i < 100; i++)
	{
		bool blLineOK = true;
		int ilFilledCells = 0;
		char pclZeile[10], pclSpalte[10];
		CCSPtrArray<TABLE_COLUMN> olLine;
		pomPfcTable->GetTextLineColumns(&olLine, i);
		for( int k = 0; k < olLine.GetSize()-1; k++)
		{
			if(olLine[k].Text != "")
			{
				ilFilledCells++;
			}
			sprintf(pclZeile, "%d", i+1);
			blLineOK = pomPfcTable->GetCellStatus(i, k);
			if(blLineOK == false)
			{
				ilStatus = false;
				sprintf(pclSpalte, "%d", k+1);
				if(k == 0 || k == 1 || k == 2)
				{
					olTableErrorText += LoadStg(IDS_STRING81) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING86) + CString(pclSpalte) + LoadStg(IDS_STRING87);
				}
			}
		}
		if(ilFilledCells != 0 && ilFilledCells < 2 && olLine[1].Text != CString(""))
		{
			ilStatus = false;
			olTableErrorText += LoadStg(IDS_STRING81) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING88);
		}

		//*** 30.09.99 SHA ***
		//*** CHECK IF THERE IS ANY FUNCTION WITHOUT VALID FROM DATE ***
		if (olLine[0].Text != CString("") && olLine[1].Text == CString(""))
		{
			olTableErrorText += CString("<") + CString(pclZeile) + CString(">") + LoadStg(IDS_STRING724);
			ilStatus = false;
		}

		if(olLine[1].Text != CString("") && olLine[2].Text != CString(""))
		{
			COleDateTime olFrom = OleDateStringToDate(olLine[1].Text);
			COleDateTime olTo = OleDateStringToDate(olLine[2].Text);
			if(olFrom.GetStatus() == COleDateTime::invalid || olTo.GetStatus() == COleDateTime::invalid)
			{
				ilStatus = false;
				olTableErrorText += LoadStg(IDS_STRING81) + LoadStg(IDS_STRING89) + CString(pclZeile);
			}
			else
			{
				if(olTo < olFrom)
				{
					ilStatus = false;
					olTableErrorText += LoadStg(IDS_STRING81) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING90);
					pomPfcTable->SetTextColumnColor(i, 1, COLORREF(RED), COLORREF(WHITE));
					pomPfcTable->SetTextColumnColor(i, 2, COLORREF(RED), COLORREF(WHITE));
				}
			}
		}
		COleDateTime olFrom, olTo, olFrom1, olTo1;
		CCSPtrArray<TABLE_COLUMN> olLine1;
		//uhi 7.5.01 Checken, ob Prio 1 mehr als einmal vergeben wurde
		if (olLine[3].Text == CString("1") && olLine[1].Text != CString(""))
		{
			olFrom = OleDateStringToDate(olLine[1].Text);
			if (olFrom.GetStatus() == COleDateTime::valid)
			{
				if (olLine[2].Text != CString(""))
					olTo = OleDateStringToDate(olLine[2].Text);
				else
					olTo.SetDateTime(2200, 12, 31, 0, 0, 0);
				if (olTo.GetStatus() == COleDateTime::valid && olTo > olFrom)
				{
					for(int m = 0; m < 100; m++)	
					{
						if(m != i)
						{
							olLine1.RemoveAll();
							pomPfcTable->GetTextLineColumns(&olLine1, m);
							if(olLine1[1].Text != CString("") && olLine1[3].Text == CString("1")){
								olFrom1 = OleDateStringToDate(olLine1[1].Text);
								if(olFrom1.GetStatus() == COleDateTime::valid){
									if(olLine1[2].Text != CString(""))
										olTo1 = OleDateStringToDate(olLine1[2].Text);
									else
										olTo1.SetDateTime(2200, 12, 31, 0, 0, 0);
									if (olTo1.GetStatus() == COleDateTime::valid && olTo1 > olFrom1)
									{
										if((olFrom1 <= olTo) && (olTo1 >= olFrom))
										{
											ilStatus = false;
											olTableErrorText += LoadStg(IDS_STRING81) + LoadStg(IDS_STRING85) + CString (" ") + CString(pclZeile) + CString (" ") + LoadStg(IDS_STRING840);
											break;
										}
									}
								}
							}
						}
					}
				}
			}
		}

		olLine.RemoveAll();
	}
	//---------------------------------------------------------------------------
	for( i = 0; i < 100; i++)
	{
		int ilFilledCells = 0;
		bool blLineOK = true;
		char pclZeile[10], pclSpalte[10];
		CCSPtrArray<TABLE_COLUMN> olLine;
		olLine.DeleteAll();
		pomPerTable->GetTextLineColumns(&olLine, i);
		for(int k = 0; k < olLine.GetSize()-1; k++)
		{
			if(olLine[k].Text != "")
			{
				ilFilledCells++;
			}
			sprintf(pclZeile, "%d", i+1);
			blLineOK = pomPerTable->GetCellStatus(i, k);
			if(blLineOK == false)
			{
				ilStatus = false;
				sprintf(pclSpalte, "%d", k+1);
				if(k == 0 || k == 1 || k == 2)
				{
					olTableErrorText += LoadStg(IDS_STRING82) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING86) + CString(pclSpalte) + LoadStg(IDS_STRING87);
				}
			}
		}
		if(ilFilledCells != 0 && ilFilledCells < 2 && olLine[1].Text != CString(""))
		{
			ilStatus = false;
			olTableErrorText += LoadStg(IDS_STRING82) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING88);
		}

		//*** 30.09.99 SHA ***
		//*** CHECK IF THERE IS ANY QUALIFICATION WITHOUT VALID FROM DATE ***
		if (olLine[0].Text != CString("") && olLine[1].Text == CString(""))
		{
			olTableErrorText += CString("<") + CString(pclZeile) + CString(">") + LoadStg(IDS_STRING725);
			ilStatus = false;
		}


		if(olLine[1].Text != CString("") && olLine[2].Text != CString(""))
		{
			COleDateTime olFrom = OleDateStringToDate(olLine[1].Text);
			COleDateTime olTo = OleDateStringToDate(olLine[2].Text);
			if(olFrom.GetStatus() == COleDateTime::invalid || olTo.GetStatus() == COleDateTime::invalid)
			{
				ilStatus = false;
				olTableErrorText += LoadStg(IDS_STRING82) + LoadStg(IDS_STRING89) + CString(pclZeile);
			}
			else
			{
				if(olTo < olFrom)
				{
					ilStatus = false;
					olTableErrorText += LoadStg(IDS_STRING82) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING90);
					pomPerTable->SetTextColumnColor(i, 1, COLORREF(RED), COLORREF(WHITE));
					pomPerTable->SetTextColumnColor(i, 2, COLORREF(RED), COLORREF(WHITE));
				}
			}
		}
		olLine.RemoveAll();
	}
	//---------------------------------------------------------------------------
	for( i = 0; i < 100; i++)
	{
		/////////////////////////////////////////////////////////////////////////////
		// Pr�fe pomCotTable
		/////////////////////////////////////////////////////////////////////////////		
		
		bool blLineOK = true;
		char pclZeile[10], pclSpalte[10];
		int ilFilledCells = 0;
		CCSPtrArray<TABLE_COLUMN> olLine;
		olLine.DeleteAll();
		pomCotTable->GetTextLineColumns(&olLine, i);
		for(int k = 0; k < olLine.GetSize()-1; k++)
		{
			if(olLine[k].Text != "")
			{
				ilFilledCells++;
			}
			sprintf(pclZeile, "%d", i+1);
			blLineOK = pomCotTable->GetCellStatus(i, k);
			if(blLineOK == false)
			{
				ilStatus = false;
				sprintf(pclSpalte, "%d", k+1);
				if(k == 0 || k == 1 || k == 2)
				{
					olTableErrorText += LoadStg(IDS_STRING83) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING86) + CString(pclSpalte) + LoadStg(IDS_STRING87);
				}
			}
		}
		if(ilFilledCells != 0 && ilFilledCells < 2 && olLine[1].Text != CString(""))
		{
			ilStatus = false;
			olTableErrorText += LoadStg(IDS_STRING83) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING88);
		}

		//*** CHECK IF THERE IS ANY CONTRACT TYPE WITHOUT VALID FROM DATE ***
		if (olLine[0].Text != CString("") && olLine[1].Text == CString(""))
		{
			olTableErrorText += CString("<") + CString(pclZeile) + CString(">") + LoadStg(IDS_STRING922);
			ilStatus = false;
		}

		if(olLine[1].Text != CString("") && olLine[2].Text != CString(""))
		{
			COleDateTime olFrom = OleDateStringToDate(olLine[1].Text);
			COleDateTime olTo = OleDateStringToDate(olLine[2].Text);
			if(olFrom.GetStatus() == COleDateTime::invalid || olTo.GetStatus() == COleDateTime::invalid)
			{
				ilStatus = false;
				olTableErrorText += LoadStg(IDS_STRING83) + LoadStg(IDS_STRING89) + CString(pclZeile);
			}
			else
			{
				if(olTo < olFrom)
				{
					ilStatus = false;
					olTableErrorText += LoadStg(IDS_STRING83) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING90);
					pomCotTable->SetTextColumnColor(i, 1, COLORREF(RED), COLORREF(WHITE));
					pomCotTable->SetTextColumnColor(i, 2, COLORREF(RED), COLORREF(WHITE));
				}
			}
		}
		olLine.RemoveAll();
	}

	if (ilStatus == true && !this->CheckDateRange(pomCotTable,1,2,-1,LoadStg(IDS_STRING83),olTableErrorText))
	{
		ilStatus = false;
	}

	//---------------------------------------------------------------------------
	for( i = 0; i < 100; i++)
	{

		int ilFilledCells = 0;
		bool blLineOK = true;
		char pclZeile[10], pclSpalte[10];
		CCSPtrArray<TABLE_COLUMN> olLine;
		pomPolTable->GetTextLineColumns(&olLine, i);

		for(int k = 0; k < olLine.GetSize()-1; k++)
		{
			if(olLine[k].Text != "")
			{
				ilFilledCells++;
			}
			sprintf(pclZeile, "%d", i+1);
			blLineOK = pomPolTable->GetCellStatus(i, k);
			if(blLineOK == false)
			{
				ilStatus = false;
				sprintf(pclSpalte, "%d", k+1);
				if(k == 0 || k == 1 || k == 2)
				{
					olTableErrorText += LoadStg(IDS_STRING84) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING86) + CString(pclSpalte) + LoadStg(IDS_STRING87);
				}
			}
		}
		if(ilFilledCells != 0 && ilFilledCells < 2 && olLine[1].Text != CString(""))
		{
			ilStatus = false;
			olTableErrorText += LoadStg(IDS_STRING84) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING88);
		}

		//*** CHECK IF THERE IS ANY POOL WITHOUT VALID FROM DATE ***
		if (olLine[0].Text != CString("") && olLine[1].Text == CString(""))
		{
			olTableErrorText += CString("<") + CString(pclZeile) + CString(">") + LoadStg(IDS_STRING920);
			ilStatus = false;
		}

		if(olLine[1].Text != CString("") && olLine[2].Text != CString(""))
		{
			COleDateTime olFrom = OleDateStringToDate(olLine[1].Text);
			COleDateTime olTo = OleDateStringToDate(olLine[2].Text);
			if(olFrom.GetStatus() == COleDateTime::invalid || olTo.GetStatus() == COleDateTime::invalid)
			{
				ilStatus = false;
				olTableErrorText += LoadStg(IDS_STRING84) + LoadStg(IDS_STRING89) + CString(pclZeile);
			}
			else
			{
				if(olTo < olFrom)
				{
					ilStatus = false;
					olTableErrorText += LoadStg(IDS_STRING84) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING90);
					pomPolTable->SetTextColumnColor(i, 1, COLORREF(RED), COLORREF(WHITE));
					pomPolTable->SetTextColumnColor(i, 2, COLORREF(RED), COLORREF(WHITE));
				}
			}
		}
		olLine.RemoveAll();
	}	

	if (ilStatus == true && !this->CheckDateRange(pomPolTable,1,2,-1,LoadStg(IDS_STRING84),olTableErrorText))
	{
		ilStatus = false;
	}


	//---------------------------------------------------------------------------
	for( i = 0; i < 100; i++)
	{

		int ilFilledCells = 0;
		bool blLineOK = true;
		char pclZeile[10], pclSpalte[10];
		CCSPtrArray<TABLE_COLUMN> olLine;
		pomWgpTable->GetTextLineColumns(&olLine, i);

		for(int k = 0; k < olLine.GetSize()-1; k++)
		{
			if(olLine[k].Text != "")
			{
				ilFilledCells++;
			}
			sprintf(pclZeile, "%d", i+1);
			blLineOK = pomWgpTable->GetCellStatus(i, k);
			if(blLineOK == false)
			{
				ilStatus = false;
				sprintf(pclSpalte, "%d", k+1);
				if(k == 0 || k == 1 || k == 2)
				{
					olTableErrorText += LoadStg(IDS_STRING116) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING86) + CString(pclSpalte) + LoadStg(IDS_STRING87);
				}
			}
		}
		if(ilFilledCells != 0 && ilFilledCells < 2 && olLine[1].Text != CString(""))
		{
			ilStatus = false;
			olTableErrorText += LoadStg(IDS_STRING116) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING88);
		}

		//*** CHECK IF THERE IS ANY WORK GROUP WITHOUT VALID FROM DATE ***
		if (olLine[0].Text != CString("") && olLine[1].Text == CString(""))
		{
			olTableErrorText += CString("<") + CString(pclZeile) + CString(">") + LoadStg(IDS_STRING921);
			ilStatus = false;
		}

		if(olLine[1].Text != CString("") && olLine[2].Text != CString(""))
		{
			COleDateTime olFrom = OleDateStringToDate(olLine[1].Text);
			COleDateTime olTo = OleDateStringToDate(olLine[2].Text);
			if(olFrom.GetStatus() == COleDateTime::invalid || olTo.GetStatus() == COleDateTime::invalid)
			{
				ilStatus = false;
				olTableErrorText += LoadStg(IDS_STRING116) + LoadStg(IDS_STRING89) + CString(pclZeile);
			}
			else
			{
				if(olTo < olFrom)
				{
					ilStatus = false;
					olTableErrorText += LoadStg(IDS_STRING116) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING90);
					pomWgpTable->SetTextColumnColor(i, 1, COLORREF(RED), COLORREF(WHITE));
					pomWgpTable->SetTextColumnColor(i, 2, COLORREF(RED), COLORREF(WHITE));
				}
			}
		}
		olLine.RemoveAll();
	}
	
	if (ilStatus == true && !this->CheckDateRange(pomWgpTable,1,2,-1,LoadStg(IDS_STRING116),olTableErrorText))
	{
		ilStatus = false;
	}

	//SRE
	for( i = 0; i < 100; i++)
	{
		int ilFilledCells = 0;
		bool blLineOK = true;
		char pclZeile[10], pclSpalte[10];
		CCSPtrArray<TABLE_COLUMN> olLine;
		olLine.DeleteAll();
		pomSreTable->GetTextLineColumns(&olLine, i);
		for(int k = 0; k < olLine.GetSize()-1; k++)
		{
			if(olLine[k].Text != "")
			{
				ilFilledCells++;
			}
			sprintf(pclZeile, "%d", i+1);
			blLineOK = pomSreTable->GetCellStatus(i, k);
			if(blLineOK == false)
			{
				ilStatus = false;
				sprintf(pclSpalte, "%d", k+1);
				if(k == 0 || k == 1 || k == 2)
				{
					olTableErrorText += LoadStg(IDS_STRING984) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING86) + CString(pclSpalte) + LoadStg(IDS_STRING87);
				}
			}
		}
		if(ilFilledCells != 0 && ilFilledCells < 2 && olLine[1].Text != CString(""))
		{
			ilStatus = false;
			olTableErrorText += LoadStg(IDS_STRING985) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING88);
		}

		if (olLine[0].Text != CString("") && olLine[1].Text == CString(""))
		{
			olTableErrorText += CString("<") + CString(pclZeile) + CString(">") + LoadStg(IDS_STRING987);
			ilStatus = false;
		}


		if(olLine[1].Text != CString("") && olLine[2].Text != CString(""))
		{
			COleDateTime olFrom = OleDateStringToDate(olLine[1].Text);
			COleDateTime olTo = OleDateStringToDate(olLine[2].Text);
			if(olFrom.GetStatus() == COleDateTime::invalid || olTo.GetStatus() == COleDateTime::invalid)
			{
				ilStatus = false;
				olTableErrorText += LoadStg(IDS_STRING984) + LoadStg(IDS_STRING89) + CString(pclZeile);
			}
			else
			{
				if(olTo < olFrom)
				{
					ilStatus = false;
					olTableErrorText += LoadStg(IDS_STRING984) + LoadStg(IDS_STRING85) + CString(pclZeile) + LoadStg(IDS_STRING90);
					pomSreTable->SetTextColumnColor(i, 1, COLORREF(RED), COLORREF(WHITE));
					pomSreTable->SetTextColumnColor(i, 2, COLORREF(RED), COLORREF(WHITE));
				}
			}
		}
		olLine.RemoveAll();
	}

	if (ilStatus == true && !this->CheckDateRange(pomSreTable,1,2,-1,LoadStg(IDS_STRING984),olTableErrorText))
	{
		ilStatus = false;
	}

/*	additional checks for optional fields	*/

	CString olText;

	if(m_PNOF.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_PNOF.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING71) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING71) + ogNotFormat;
		}
	}

	if(m_KIND.GetStatus() == false) 
	{
		if(m_KIND.GetWindowTextLength() != 0)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING740) + CString("(F/M)") + ogNotFormat;
		}
	}

	if(m_KIDS.GetStatus() == false) 
	{
		if(m_KIDS.GetWindowTextLength() != 0)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING880) + CString("(Y/N)") + ogNotFormat;
		}
	}

	if(m_NATI.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_NATI.GetWindowTextLength() != 0)
		{
			olErrorText += LoadStg(IDS_STRING741) + ogNotFormat;
		}
	}

	if(m_CANO.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_CANO.GetWindowTextLength() != 0)
		{
			olErrorText += LoadStg(IDS_STRING858) + ogNotFormat;
		}
	}

	if(m_DOBK.GetStatus() == false) 
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING749) + ogNotFormat;
	}

	if (m_GEBU.GetStatus() == false) 
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING923) + ogNotFormat;
	}

	//---------------------------------------------------------------------------
	int ilSorCount = 0;
	int ilSpfCount = 0;
	int ilSpeCount = 0;
	int ilScoCount = 0;
	int ilSteCount = 0;
	int ilSwgCount = 0;
	int ilSdaCount = 0;
	int	ilSreCount = 0;

	if(ilStatus == true)
	{
		for( i = 0; i < 100; i++)
		{
			bool blLineOK = true;
			CCSPtrArray<TABLE_COLUMN> olLine;
			/////////////////////////////////////////////////////////////////////////////
			// Schreibe Daten aus pomOrgTable in SORTAB
			/////////////////////////////////////////////////////////////////////////////
			pomOrgTable->GetTextLineColumns(&olLine, i);
			for(int k = 0; k < olLine.GetSize(); k++)
			{
				blLineOK = pomOrgTable->GetCellStatus(i, k);
			}
			if(blLineOK == true)
			{
				if(olLine.GetSize() > 2)
				{
					if((olLine[0].Text != CString("")) && (olLine[1].Text != CString("")))
					{
						if(olLine[3].Text == CString(""))
						{
							SORDATA *prlSor = new SORDATA;
							strcpy(prlSor->Code, olLine[0].Text);
							prlSor->Vpfr = OleDateStringToDate(olLine[1].Text);
							prlSor->Vpto = OleDateStringToDate(olLine[2].Text);
							if (prlSor->Vpto.GetStatus() == COleDateTime::valid)
							{
								prlSor->Vpto.SetDateTime(prlSor->Vpto.GetYear(),prlSor->Vpto.GetMonth(),prlSor->Vpto.GetDay(),23,59,59);
							}

							strcpy(prlSor->Odgc, olLine[4].Text.Left(8));//Odgc 4
							strcpy(prlSor->Lead, olLine[5].Text.Left(1));//Lead 5

							prlSor->IsChanged = DATA_NEW;
							pomSorData->Add(prlSor);
						}
						else
						{
							int ilTmpUrno = atoi(olLine[3].Text);
							//Nach �nderungen Checken
							bool blFound = false;
							for(int j = 0; j < pomSorData->GetSize(); j ++)
							{
								if(ilTmpUrno == pomSorData->GetAt(j).Urno)
								{
									SORDATA *prlSor = ogSorData.GetSorByUrno(pomSorData->GetAt(j).Urno);
									CString olTmpVpfr = pomSorData->GetAt(j).Vpfr.Format("%d.%m.%Y");
									CString olTmpVpto = pomSorData->GetAt(j).Vpto.Format("%d.%m.%Y");
									if((olLine[0].Text != CString(pomSorData->GetAt(j).Code))  ||
									   (olLine[1].Text != olTmpVpfr)						   ||
									   (olLine[2].Text != olTmpVpto)						   ||
									   (olLine[4].Text != CString(pomSorData->GetAt(j).Odgc))  ||
									   (olLine[5].Text != CString(pomSorData->GetAt(j).Lead)))
									{
										if(prlSor != NULL)
										{
											strcpy(prlSor->Code, olLine[0].Text);
											prlSor->Vpfr = OleDateStringToDate(olLine[1].Text);
											prlSor->Vpto = OleDateStringToDate(olLine[2].Text);
											if (prlSor->Vpto.GetStatus() == COleDateTime::valid)
											{
												prlSor->Vpto.SetDateTime(prlSor->Vpto.GetYear(),prlSor->Vpto.GetMonth(),prlSor->Vpto.GetDay(),23,59,59);
											}

											strcpy(prlSor->Odgc, olLine[4].Text.Left(8));//Odgc 4
											strcpy(prlSor->Lead, olLine[5].Text.Left(1));//Lead 5

											prlSor->IsChanged = DATA_CHANGED;
											blFound = true;
											break;
										}
									}
									else
									{
										prlSor->IsChanged = DATA_UNCHANGED;
										blFound = true;
										break;
									}
								}
							}
						}
					}//Alle Columns sind gef�llt
					else
					{
						if((olLine[0].Text == CString("")) && (olLine[1].Text == CString("")))
						{
							if(olLine[3].Text != CString(""))
							{
								int ilTmpUrno = atoi(olLine[3].Text);
								bool blFound = false;
								for(int j = 0; j < pomSorData->GetSize(); j ++)
								{
									if(ilTmpUrno == pomSorData->GetAt(j).Urno)
									{
										SORDATA *prlSor = ogSorData.GetSorByUrno(pomSorData->GetAt(j).Urno);
										if(prlSor != NULL)
										{
											prlSor->IsChanged = DATA_DELETED;
										}
										break;
									}
								}
							}
						}
					}
				}
			}
			else
			{
				ilSorCount++;
			}
			olLine.RemoveAll();
			/////////////////////////////////////////////////////////////////////////////
			/////////////////////////////////////////////////////////////////////////////
			pomPfcTable->GetTextLineColumns(&olLine, i);
			for(k = 0; k < olLine.GetSize(); k++)
			{
				blLineOK = pomPfcTable->GetCellStatus(i, k);
			}
			if(blLineOK == true)
			{
				if(olLine.GetSize() > 2)
				{
					if((olLine[0].Text != CString("")) && (olLine[1].Text != CString("")))
					{
						if(olLine[4].Text == CString("")) //Test auf vorhandene Urno
						{
							SPFDATA *prlSpf = new SPFDATA;
							prlSpf->Surn = pomStf->Urno;
							strcpy(prlSpf->Code, olLine[0].Text);
							prlSpf->Vpfr = OleDateStringToDate(olLine[1].Text);
							prlSpf->Vpto = OleDateStringToDate(olLine[2].Text);
							if (prlSpf->Vpto.GetStatus() == COleDateTime::valid)
							{
								prlSpf->Vpto.SetDateTime(prlSpf->Vpto.GetYear(),prlSpf->Vpto.GetMonth(),prlSpf->Vpto.GetDay(),23,59,59);
							}

							strcpy(prlSpf->Prio, olLine[3].Text);

							prlSpf->IsChanged = DATA_NEW;
							this->pomSpfData->Add(prlSpf);
						}
						else
						{
							int ilTmpUrno = atoi(olLine[4].Text);
							//Nach �nderungen Checken
							bool blFound = false;
							for(int j = 0; j < pomSpfData->GetSize(); j ++)
							{
								if(ilTmpUrno == pomSpfData->GetAt(j).Urno)
								{
									SPFDATA *prlSpf = ogSpfData.GetSpfByUrno(pomSpfData->GetAt(j).Urno);
									CString olTmpVpfr = pomSpfData->GetAt(j).Vpfr.Format("%d.%m.%Y");
									CString olTmpVpto = pomSpfData->GetAt(j).Vpto.Format("%d.%m.%Y");
									CString olTmpPrio = pomSpfData->GetAt(j).Prio;
									CString olTmpCode = pomSpfData->GetAt(j).Code;

									if((olLine[0].Text != olTmpCode) ||
									   (olLine[1].Text != olTmpVpfr) ||
									   (olLine[2].Text != olTmpVpto) ||
									   (olLine[3].Text != olTmpPrio))
									{
										if(prlSpf != NULL)
										{
											strcpy(prlSpf->Code, olLine[0].Text);
											prlSpf->Vpfr = OleDateStringToDate(olLine[1].Text);
											prlSpf->Vpto = OleDateStringToDate(olLine[2].Text);
											if (prlSpf->Vpto.GetStatus() == COleDateTime::valid)
											{
												prlSpf->Vpto.SetDateTime(prlSpf->Vpto.GetYear(),prlSpf->Vpto.GetMonth(),prlSpf->Vpto.GetDay(),23,59,59);
											}

											strcpy(prlSpf->Prio, olLine[3].Text);
											prlSpf->IsChanged = DATA_CHANGED;
											blFound = true;
											break;
										}
									}
									else
									{
										prlSpf->IsChanged = DATA_UNCHANGED;
										blFound = true;
										break;
									}
								}
							}
						}
					}//Alle Columns sind gef�llt
					else
					{
						if((olLine[0].Text == CString("")) && (olLine[1].Text == CString("")))
						{
							if(olLine[4].Text != CString(""))
							{
								int ilTmpUrno = atoi(olLine[4].Text);
								bool blFound = false;
								for(int j = 0; j < pomSpfData->GetSize(); j ++)
								{
									if(ilTmpUrno == pomSpfData->GetAt(j).Urno)
									{
										SPFDATA *prlSpf = ogSpfData.GetSpfByUrno(pomSpfData->GetAt(j).Urno);
										if(prlSpf != NULL)
										{ 
											prlSpf->IsChanged = DATA_DELETED;
										}
										break;
									}
								}
							}
						}
					}
				}
			}
			else
			{
				ilSpfCount++;
			}
			olLine.RemoveAll();
			//////////////////////////////////////////////////////////////////////////////////////////
			pomPerTable->GetTextLineColumns(&olLine, i);
			for(k = 0; k < olLine.GetSize(); k++)
			{
				blLineOK = pomPerTable->GetCellStatus(i, k);
			}
			if(blLineOK == true)
			{
				if(olLine.GetSize() > 2)
				{
					if((olLine[0].Text != CString("")) && (olLine[1].Text != CString("")))
					{
						if(olLine[3].Text == CString(""))
						{
							SPEDATA *prlSpe = new SPEDATA;
							prlSpe->Surn = pomStf->Urno;
							strcpy(prlSpe->Code, olLine[0].Text);
							prlSpe->Vpfr = OleDateStringToDate(olLine[1].Text);
							prlSpe->Vpto = OleDateStringToDate(olLine[2].Text);
							if (prlSpe->Vpto.GetStatus() == COleDateTime::valid)
							{
								prlSpe->Vpto.SetDateTime(prlSpe->Vpto.GetYear(),prlSpe->Vpto.GetMonth(),prlSpe->Vpto.GetDay(),23,59,59);
							}


							prlSpe->IsChanged = DATA_NEW;
							this->pomSpeData->Add(prlSpe);
						}
						else
						{
							int ilTmpUrno = atoi(olLine[3].Text);
							//Nach �nderungen Checken
							bool blFound = false;
							for(int j = 0; j < pomSpeData->GetSize(); j ++)
							{
								if(ilTmpUrno == pomSpeData->GetAt(j).Urno)
								{
									SPEDATA *prlSpe = ogSpeData.GetSpeByUrno(pomSpeData->GetAt(j).Urno);
									CString olTmpVpfr = pomSpeData->GetAt(j).Vpfr.Format("%d.%m.%Y");
									CString olTmpVpto = pomSpeData->GetAt(j).Vpto.Format("%d.%m.%Y");

									if((olLine[0].Text != CString(pomSpeData->GetAt(j).Code))  ||
									   (olLine[1].Text != olTmpVpfr) || (olLine[2].Text != olTmpVpto))
									{
										if(prlSpe != NULL)
										{
											strcpy(prlSpe->Code, olLine[0].Text);
											prlSpe->Vpfr = OleDateStringToDate(olLine[1].Text);
											prlSpe->Vpto = OleDateStringToDate(olLine[2].Text);
											if (prlSpe->Vpto.GetStatus() == COleDateTime::valid)
											{
												prlSpe->Vpto.SetDateTime(prlSpe->Vpto.GetYear(),prlSpe->Vpto.GetMonth(),prlSpe->Vpto.GetDay(),23,59,59);
											}


											
											prlSpe->IsChanged = DATA_CHANGED;
											blFound = true;
											break;
										}
									}
									else
									{
										prlSpe->IsChanged = DATA_UNCHANGED;
										blFound = true;
										break;
									}
								}
							}
						}
					}//Alle Columns sind gef�llt
					else
					{
						if((olLine[0].Text == CString("")) && (olLine[1].Text == CString("")) /*&& (olLine[2].Text == CString(""))*/)
						{
							if(olLine[3].Text != CString(""))
							{
								int ilTmpUrno = atoi(olLine[3].Text);
								bool blFound = false;
								for(int j = 0; j < pomSpeData->GetSize(); j ++)
								{
									if(ilTmpUrno == pomSpeData->GetAt(j).Urno)
									{
										SPEDATA *prlSpe = ogSpeData.GetSpeByUrno(pomSpeData->GetAt(j).Urno);
										if(prlSpe != NULL)
										{
											prlSpe->IsChanged = DATA_DELETED;
										}
										break;
									}
								}
							}
						}
					}
				}
			}
			else
			{
				ilSpeCount++;
			}
			olLine.RemoveAll();
			/////////////////////////////////////////////////////////////////////////////
			// Schreibe Daten aus pomCotTable in SCOTAB
			/////////////////////////////////////////////////////////////////////////////
			pomCotTable->GetTextLineColumns(&olLine, i);
			for(k = 0; k < olLine.GetSize(); k++)
			{
				blLineOK = pomCotTable->GetCellStatus(i, k);
			}
			if(blLineOK == true)
			{
				if(olLine.GetSize() > 2)
				{
					if((olLine[0].Text != CString("")) && (olLine[1].Text != CString("")))
					{
						if(olLine[3].Text == CString(""))
						{
							SCODATA *prlSco = new SCODATA;
							prlSco->Surn = pomStf->Urno;
							strcpy(prlSco->Code, olLine[0].Text);
							prlSco->Vpfr = OleDateStringToDate(olLine[1].Text);
							prlSco->Vpto = OleDateStringToDate(olLine[2].Text);
							if (prlSco->Vpto.GetStatus() == COleDateTime::valid)
							{
								prlSco->Vpto.SetDateTime(prlSco->Vpto.GetYear(),prlSco->Vpto.GetMonth(),prlSco->Vpto.GetDay(),23,59,59);
							}


							strcpy(prlSco->Kost, olLine[4].Text.Left(8));//Kost 4
							strcpy(prlSco->Cweh, olLine[5].Text.Left(4));//Cweh 5

							prlSco->IsChanged = DATA_NEW;
							this->pomScoData->Add(prlSco);
						}
						else
						{	// DATA_CHANGED
							int ilTmpUrno = atoi(olLine[3].Text);
							//Nach �nderungen Checken
							bool blFound = false;
							for(int j = 0; j < pomScoData->GetSize(); j ++)
							{
								if(ilTmpUrno == pomScoData->GetAt(j).Urno)
								{
									SCODATA *prlSco = ogScoData.GetScoByUrno(pomScoData->GetAt(j).Urno);
									CString olTmpVpfr = pomScoData->GetAt(j).Vpfr.Format("%d.%m.%Y");
									CString olTmpVpto = pomScoData->GetAt(j).Vpto.Format("%d.%m.%Y"); 
									if((olLine[0].Text != CString(pomScoData->GetAt(j).Code))  ||
									   (olLine[1].Text != olTmpVpfr)						   ||
									   (olLine[2].Text != olTmpVpto)						   ||
									   (olLine[4].Text != CString(pomScoData->GetAt(j).Kost))  ||
									   (olLine[5].Text != CString(pomScoData->GetAt(j).Cweh)))
									{
										if(prlSco != NULL)
										{
											strcpy(prlSco->Code, olLine[0].Text);
											prlSco->Vpfr = OleDateStringToDate(olLine[1].Text);
											prlSco->Vpto = OleDateStringToDate(olLine[2].Text);
											if (prlSco->Vpto.GetStatus() == COleDateTime::valid)
											{
												prlSco->Vpto.SetDateTime(prlSco->Vpto.GetYear(),prlSco->Vpto.GetMonth(),prlSco->Vpto.GetDay(),23,59,59);
											}

											//Kost 5
											//Cweh 6
											strcpy(prlSco->Kost, olLine[4].Text.Left(8));
											strcpy(prlSco->Cweh, olLine[5].Text.Left(4));

											prlSco->IsChanged = DATA_CHANGED;
											blFound = true;
											break;
										}
									}
									else
									{
										prlSco->IsChanged = DATA_UNCHANGED;
										blFound = true;
										break;
									}
								}
							}
						}
					}//Alle Columns sind gef�llt
					else
					{
						if((olLine[0].Text == CString("")) && (olLine[1].Text == CString("")))
						{
							if(olLine[3].Text != CString(""))
							{
								int ilTmpUrno = atoi(olLine[3].Text);
								bool blFound = false;
								for(int j = 0; j < pomScoData->GetSize(); j ++)
								{
									if(ilTmpUrno == pomScoData->GetAt(j).Urno)
									{
										SCODATA *prlSco = ogScoData.GetScoByUrno(pomScoData->GetAt(j).Urno);
										if(prlSco != NULL)
										{
											prlSco->IsChanged = DATA_DELETED;
										}
										break;
									}
								}
							}
						}
					}
				}
			}
			else
			{
				ilScoCount++;
			}
			olLine.RemoveAll();
			//////////////////////////////////////////////////////////////////////////////////////////

			pomPolTable->GetTextLineColumns(&olLine, i);
			for(k = 0; k < olLine.GetSize(); k++)
			{
				blLineOK = pomPolTable->GetCellStatus(i, k);
			}
			if(blLineOK == true)
			{
				if(olLine.GetSize() > 2)
				{
					if((olLine[0].Text != CString("")) && (olLine[1].Text != CString("")))
					{
						if(olLine[3].Text == CString(""))
						{
							SDADATA *prlSda = new SDADATA;
							prlSda->Surn = pomStf->Urno;
							prlSda->Upol = ogPolData.GetUrnoByName(olLine[0].Text);
							prlSda->Vpfr = OleDateStringToDate(olLine[1].Text);
							prlSda->Vpto = OleDateStringToDate(olLine[2].Text);
							if (prlSda->Vpto.GetStatus() == COleDateTime::valid)
							{
								prlSda->Vpto.SetDateTime(prlSda->Vpto.GetYear(),prlSda->Vpto.GetMonth(),prlSda->Vpto.GetDay(),23,59,59);
							}

							prlSda->IsChanged = DATA_NEW;

							this->pomSdaData->Add(prlSda);
						}
						else
						{
							int ilTmpUrno = atoi(olLine[3].Text);
							//Nach �nderungen Checken
							bool blFound = false;
							for(int j = 0; j < pomSdaData->GetSize(); j ++)
							{
								if(ilTmpUrno == pomSdaData->GetAt(j).Urno)
								{
									SDADATA *prlSda = ogSdaData.GetSdaByUrno(pomSdaData->GetAt(j).Urno);
									CString olTmpVpfr = pomSdaData->GetAt(j).Vpfr.Format("%d.%m.%Y");
									CString olTmpVpto = pomSdaData->GetAt(j).Vpto.Format("%d.%m.%Y");
									CString olTmpName = ogPolData.GetNameByUrno(pomSdaData->GetAt(j).Upol);
									if (olLine[0].Text != olTmpName  || olLine[1].Text != olTmpVpfr || olLine[2].Text != olTmpVpto)
									{
										if(prlSda != NULL)
										{
											prlSda->Upol = ogPolData.GetUrnoByName(olLine[0].Text);
											prlSda->Vpfr = OleDateStringToDate(olLine[1].Text);
											prlSda->Vpto = OleDateStringToDate(olLine[2].Text);
											if (prlSda->Vpto.GetStatus() == COleDateTime::valid)
											{
												prlSda->Vpto.SetDateTime(prlSda->Vpto.GetYear(),prlSda->Vpto.GetMonth(),prlSda->Vpto.GetDay(),23,59,59);
											}

											prlSda->IsChanged = DATA_CHANGED;
											blFound = true;
											break;
										}
									}
									else
									{
										prlSda->IsChanged = DATA_UNCHANGED;
										blFound = true;
										break;
									}
								}
							}
						}
					}//Alle Columns sind gef�llt
					else
					{
						if((olLine[0].Text == CString("")) && (olLine[1].Text == CString("")))
						{
							if(olLine[3].Text != CString(""))
							{
								int ilTmpUrno = atoi(olLine[3].Text);
								bool blFound = false;
								for(int j = 0; j < pomSdaData->GetSize(); j ++)
								{
									if(ilTmpUrno == pomSdaData->GetAt(j).Urno)
									{
										SDADATA *prlSda = ogSdaData.GetSdaByUrno(pomSdaData->GetAt(j).Urno);
										if(prlSda != NULL)
										{
											prlSda->IsChanged = DATA_DELETED;
										}
										break;
									}
								}
							}
						}
					}
				}
			}
			else
			{
				ilSdaCount++;
			}
			olLine.RemoveAll();
			//////////////////////////////////////////////////////////////////////////////////////////
			pomWgpTable->GetTextLineColumns(&olLine, i);
			for(k = 0; k < olLine.GetSize(); k++)
			{
				blLineOK = pomWgpTable->GetCellStatus(i, k);
			}
			if(blLineOK == true)
			{
				if(olLine.GetSize() > 2)
				{
					if((olLine[0].Text != CString("")) && (olLine[1].Text != CString("")))
					{
						if(olLine[3].Text == CString(""))
						{
							SWGDATA *prlSwg = new SWGDATA;
							prlSwg->Surn = pomStf->Urno;
							strcpy(prlSwg->Code, olLine[0].Text);
							prlSwg->Vpfr = OleDateStringToDate(olLine[1].Text);
							prlSwg->Vpto = OleDateStringToDate(olLine[2].Text);
							if (prlSwg->Vpto.GetStatus() == COleDateTime::valid)
							{
								prlSwg->Vpto.SetDateTime(prlSwg->Vpto.GetYear(),prlSwg->Vpto.GetMonth(),prlSwg->Vpto.GetDay(),23,59,59);
							}

							prlSwg->IsChanged = DATA_NEW;
							this->pomSwgData->Add(prlSwg);
						}
						else
						{
							int ilTmpUrno = atoi(olLine[3].Text);
							//Nach �nderungen Checken
							bool blFound = false;
							for(int j = 0; j < pomSwgData->GetSize(); j ++)
							{
								if(ilTmpUrno == pomSwgData->GetAt(j).Urno)
								{
									SWGDATA *prlSwg = ogSwgData.GetSwgByUrno(pomSwgData->GetAt(j).Urno);
									CString olTmpVpfr = pomSwgData->GetAt(j).Vpfr.Format("%d.%m.%Y");
									CString olTmpVpto = pomSwgData->GetAt(j).Vpto.Format("%d.%m.%Y");
									if((olLine[0].Text != CString(pomSwgData->GetAt(j).Code))  ||
									   (olLine[1].Text != olTmpVpfr) || (olLine[2].Text != olTmpVpto))
									{
										if(prlSwg != NULL)
										{
											strcpy(prlSwg->Code, olLine[0].Text);
											prlSwg->Vpfr = OleDateStringToDate(olLine[1].Text);
											prlSwg->Vpto = OleDateStringToDate(olLine[2].Text);
											if (prlSwg->Vpto.GetStatus() == COleDateTime::valid)
											{
												prlSwg->Vpto.SetDateTime(prlSwg->Vpto.GetYear(),prlSwg->Vpto.GetMonth(),prlSwg->Vpto.GetDay(),23,59,59);
											}

											prlSwg->IsChanged = DATA_CHANGED;
											blFound = true;
											break;
										}
									}
									else
									{
										prlSwg->IsChanged = DATA_UNCHANGED;
										blFound = true;
										break;
									}
								}
							}
						}
					}//Alle Columns sind gef�llt
					else
					{
						if((olLine[0].Text == CString("")) && (olLine[1].Text == CString("")))
						{
							if(olLine[3].Text != CString(""))
							{
								int ilTmpUrno = atoi(olLine[3].Text);
								bool blFound = false;
								for(int j = 0; j < pomSwgData->GetSize(); j ++)
								{
									if(ilTmpUrno == pomSwgData->GetAt(j).Urno)
									{
										SWGDATA *prlSwg = ogSwgData.GetSwgByUrno(pomSwgData->GetAt(j).Urno);
										if(prlSwg != NULL)
										{
											prlSwg->IsChanged = DATA_DELETED;
										}
										break;
									}
								}
							}
						}
					}
				}
			}
			else
			{
				ilSwgCount++;
			}
			olLine.RemoveAll();
///////////////////////////////////////////////////////////////////////////////////
			//SRETAB
			pomSreTable->GetTextLineColumns(&olLine, i);
			for(k = 0; k < olLine.GetSize(); k++)
			{
				blLineOK = pomSreTable->GetCellStatus(i, k);
			}
			if(blLineOK == true)
			{
				if(olLine.GetSize() > 2)
				{
					if((olLine[0].Text != CString("")) && (olLine[1].Text != CString("")))
					{
						if(olLine[3].Text == CString(""))
						{
							SREDATA *prlSre = new SREDATA;
							
							CString pclRegi, olText;
							olText = olLine[0].Text;

							if(strcmp(olText, "I") == 0)
							{
								pclRegi = '0';
								strcpy(prlSre->Regi, pclRegi.GetBuffer(0));
							}
							else if(strcmp(olText, "R") == 0)
							{
								pclRegi = '1';
								strcpy(prlSre->Regi, pclRegi.GetBuffer(0));
							}
							else if(strcmp(olText, "S") == 0 && "SIN" == ogParData.GetParValue("ROSTER","ID_PROD_CUSTOMER"))
							{
								pclRegi = '2';
								strcpy(prlSre->Regi, pclRegi.GetBuffer(0));
							}
							/*else if(strcmp(olText, " ") == 0){
								pclRegi = ' ';
								strcpy(prlSre->Regi, pclRegi.GetBuffer(0));
							}*/

							prlSre->Vpfr = OleDateStringToDate(olLine[1].Text);
							prlSre->Vpto = OleDateStringToDate(olLine[2].Text);
							if (prlSre->Vpto.GetStatus() == COleDateTime::valid)
							{
								prlSre->Vpto.SetDateTime(prlSre->Vpto.GetYear(),prlSre->Vpto.GetMonth(),prlSre->Vpto.GetDay(),23,59,59);
							}

							prlSre->IsChanged = DATA_NEW;
							prlSre->Urno = ogBasicData.GetNextUrno();
							prlSre->Surn = pomStf->Urno;
							prlSre->Cdat = COleDateTime::GetCurrentTime();
							strcpy(prlSre->Usec,cgUserName);
							this->pomSreData->Add(prlSre);
						}
						else
						{	// DATA_CHANGED
							int ilTmpUrno = atoi(olLine[3].Text);
							//Nach �nderungen Checken
							bool blFound = false;
							for(int j = 0; j < pomSreData->GetSize(); j ++)
							{
								if(ilTmpUrno == pomSreData->GetAt(j).Urno)
								{
									SREDATA *prlSre = ogSreData.GetSreByUrno(pomSreData->GetAt(j).Urno);

									CString olTmpVpfr = "";
									CString olTmpVpto = "";
									if(pomSreData->GetAt(j).Vpfr.GetStatus() == COleDateTime::valid)
									{
										olTmpVpfr = pomSreData->GetAt(j).Vpfr.Format("%d.%m.%Y");
									}
									if(pomSreData->GetAt(j).Vpto.GetStatus() == COleDateTime::valid)
									{
										olTmpVpto = pomSreData->GetAt(j).Vpto.Format("%d.%m.%Y");
									}
									
									CString olRegi;
									if(strcmp(pomSreData->GetAt(j).Regi, "0") == 0)
										olRegi = CString("I");
									else if(strcmp(pomSreData->GetAt(j).Regi, "1") == 0)
										olRegi = CString("R");
									else if(strcmp(pomSreData->GetAt(j).Regi, "2") == 0 && "SIN" == ogParData.GetParValue("ROSTER","ID_PROD_CUSTOMER"))
										olRegi = CString("S");
									else
										olRegi = CString(" ");

									if((olLine[0].Text != olRegi)  ||	
									   (olLine[1].Text != olTmpVpfr) ||
									   (olLine[2].Text != olTmpVpto))
									{
										if(prlSre != NULL)
										{
											CString pclRegi, olText;
											olText = olLine[0].Text;

											if(strcmp(olText, "I") == 0)
											{
												pclRegi = '0';
												strcpy(prlSre->Regi, pclRegi.GetBuffer(0));
											}
											else if(strcmp(olText, "R") == 0)
											{
												pclRegi = '1';
												strcpy(prlSre->Regi, pclRegi.GetBuffer(0));
											}
											else if(strcmp(olText, "S") == 0 && "SIN" == ogParData.GetParValue("ROSTER","ID_PROD_CUSTOMER"))
											{
												pclRegi = '2';
												strcpy(prlSre->Regi, pclRegi.GetBuffer(0));
											}
											else
											{
												pclRegi = ' ';
												strcpy(prlSre->Regi, pclRegi.GetBuffer(0));
											}
											prlSre->Vpfr = OleDateStringToDate(olLine[1].Text);
											prlSre->Vpto = OleDateStringToDate(olLine[2].Text);
											if (prlSre->Vpto.GetStatus() == COleDateTime::valid)
											{
												prlSre->Vpto.SetDateTime(prlSre->Vpto.GetYear(),prlSre->Vpto.GetMonth(),prlSre->Vpto.GetDay(),23,59,59);
											}

											prlSre->IsChanged = DATA_CHANGED;
											prlSre->Lstu = COleDateTime::GetCurrentTime();
											strcpy(prlSre->Useu,cgUserName);
											blFound = true;
											break;
										}
									}
									else
									{
										prlSre->IsChanged = DATA_UNCHANGED;
										blFound = true;
										break;
									}
								}
							}
						}
					}//Alle Columns sind gef�llt
					else
					{
						if((olLine[0].Text == CString("")) && (olLine[1].Text == CString("")))
						{
							if(olLine[3].Text != CString(""))
							{
								int ilTmpUrno = atoi(olLine[3].Text);
								bool blFound = false;
								for(int j = 0; j < pomSreData->GetSize(); j ++)
								{
									if(ilTmpUrno == pomSreData->GetAt(j).Urno)
									{
										SREDATA *prlSre = ogSreData.GetSreByUrno(pomSreData->GetAt(j).Urno);
										if(prlSre != NULL)
										{
											prlSre->IsChanged = DATA_DELETED;
										}
										break;
									}
								}
							}
						}
					}
				}
			}
			else
			{
				ilSreCount++;
			}
			olLine.RemoveAll();
		}
	}
	bool blSoftStatus = true;
	if(ilSorCount != 0)
	{
		blSoftStatus = false;
		olErrorText += CString("\n") + LoadStg(IDS_STRING80) + LoadStg(IDS_STRING91);
	}
	if(ilSpfCount != 0)
	{
		blSoftStatus = false;
		olErrorText += CString("\n") + LoadStg(IDS_STRING81) + LoadStg(IDS_STRING91);
	}
	if(ilSpeCount != 0)
	{
		blSoftStatus = false;
		olErrorText += CString("\n") + LoadStg(IDS_STRING82) + LoadStg(IDS_STRING91);
	}
	if(ilScoCount != 0)
	{
		blSoftStatus = false;
		olErrorText += CString("\n") + LoadStg(IDS_STRING83) + LoadStg(IDS_STRING91);
	}
	if(ilSteCount != 0)
	{
		blSoftStatus = false;
		olErrorText += CString("\n") + LoadStg(IDS_STRING84) + LoadStg(IDS_STRING91);
	}
	if(ilSwgCount != 0)
	{
		blSoftStatus = false;
		olErrorText += CString("\n") + LoadStg(IDS_STRING116) + LoadStg(IDS_STRING91);
	}
	if(ilSreCount != 0)
	{
		blSoftStatus = false;
		olErrorText += CString("\n") + LoadStg(IDS_STRING984) + LoadStg(IDS_STRING91);
	}

	if(ilStatus == true)
	{
		CString olText;
		char plcTmp[100]="";
		COleDateTime olDate; 
		CTime        olTime;
		m_DODDM_D.GetWindowText(olText); 
		olDate = OleDateStringToDate(olText);
		if(olDate.GetStatus() == COleDateTime::valid)
		{
			CString olS = olDate.Format("%d.%m.%Y");
			pomStf->Dodm = COleDateTime(olDate.GetYear(),olDate.GetMonth(),olDate.GetDay(),0, 0, 0);
			olS = pomStf->Dodm.Format("%d.%m.%Y-%H:%M");
			olS = "";
		}
		else
		{
			pomStf->Dodm.SetStatus(COleDateTime::null);
		}
		m_DOEM_D.GetWindowText(olText); 
		olDate = OleDateStringToDate(olText);
		if(olDate.GetStatus() == COleDateTime::valid)
		{
			CString olS = olDate.Format("%d.%m.%Y");
			pomStf->Doem = COleDateTime(olDate.GetYear(),olDate.GetMonth(),olDate.GetDay(),0, 0, 0);
			olS = pomStf->Doem.Format("%d.%m.%Y-%H:%M");
			olS = "";
		}
		else
		{
			pomStf->Doem.SetStatus(COleDateTime::null);
		}
	}

	if(ilStatus == true)
	{
		CString olText;
		CedaStfData olData;
		CCSPtrArray<STFDATA> omData;
		char pclWhere[128];
		m_PERC.GetWindowText(olText);
		sprintf(pclWhere,"WHERE PERC='%s'", olText);
		if(olData.ReadSpecialData(&omData, pclWhere, "URNO,DOEM,DODM", false) == true)
		{	
			for (int ilC = 0; ilC < omData.GetSize(); ilC++)
			{
				if(pomStf->Urno != omData[ilC].Urno)
				{
					BOOL blIsValidRecord = IsOverlapped(pomStf->Doem,pomStf->Dodm,omData[ilC].Doem,omData[ilC].Dodm);
					if (blIsValidRecord)
					{
						olErrorText += LoadStg(IDS_STRING72) + LoadStg(IDS_EXIST);
						ilStatus = false;
						break;
					}
				}
			}
		}

		omData.DeleteAll();
		m_PENO.GetWindowText(olText);
		olNewCode = olText;
		sprintf(pclWhere,"WHERE PENO='%s'", olText);
		if(olData.ReadSpecialData(&omData, pclWhere, "URNO", false) == true)
		{	
			if(pomStf->Urno != omData[0].Urno)
			{
				olErrorText += LoadStg(IDS_STRING71) + LoadStg(IDS_EXIST);
				ilStatus = false;
			}
		}
		omData.DeleteAll();
	}

	////////////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);

	
	
	if(ilStatus == true)
	{
		bool blChangeCode = true;
		if(olNewCode != omOldCode && bmChangeAction)
		{
			// Referenz Check
			CheckReferenz olCheckReferenz;
			if(pomStatus != NULL) pomStatus->SetWindowText(LoadStg(IDS_STRING120));
			int ilCount = olCheckReferenz.Check(_STF, pomStf->Urno, omOldCode);
			if(pomStatus != NULL) pomStatus->SetWindowText(LoadStg(ST_OK));
			CString olTxt;
			if(ilCount>0)
			{
				blChangeCode = false;
				if (!ogBasicData.BackDoorEnabled())
				{
					olTxt.Format("%d %s",ilCount, LoadStg(IDS_STRING119));
					MessageBox(olTxt,LoadStg(IDS_STRING145),MB_ICONERROR);
				}
				else
				{
					olTxt.Format("%d %s",ilCount, LoadStg(IDS_STRING983));
					if(IDYES == MessageBox(olTxt,LoadStg(IDS_STRING149),(MB_ICONSTOP | MB_YESNOCANCEL | MB_DEFBUTTON2)))
					{
						blChangeCode = true;
					}
				}
			}
			// END Referenz Check
		}

		if(blChangeCode)
		{
			m_FINM.GetWindowText(olText); 
			strcpy(pomStf->Finm, olText);
			m_LANM.GetWindowText(olText); 
			strcpy(pomStf->Lanm, olText);
			m_PENO.GetWindowText(olText); 
			strcpy(pomStf->Peno, olText);
			m_PERC.GetWindowText(olText); 
			strcpy(pomStf->Perc, olText);
			m_SHNM.GetWindowText(olText); 
			strcpy(pomStf->Shnm, olText);
			m_TELD.GetWindowText(olText); 
			strcpy(pomStf->Teld, olText);
			m_TELH.GetWindowText(olText); 
			strcpy(pomStf->Telh, olText);
			m_TELP.GetWindowText(olText); 
			strcpy(pomStf->Telp, olText);
			m_REMA.GetWindowText(olText); 
			strcpy(pomStf->Rema, olText);

			if(m_THOR.GetCheck() == 1) 
				strcpy(pomStf->Tohr, "J");
			else
				strcpy(pomStf->Tohr, "N");



			m_PNOF.GetWindowText(olText); 
			strcpy(pomStf->Pnof, olText);

			m_KIND.GetWindowText(olText); 
			strcpy(pomStf->Kind, olText);

			m_KIDS.GetWindowText(olText);
			if (olText == "Y")
				strcpy(pomStf->Kids,"1");
			else
				strcpy(pomStf->Kids,"0");

			m_NATI.GetWindowText(olText); 
			strcpy(pomStf->Nati, olText);
			m_CANO.GetWindowText(olText); 
			strcpy(pomStf->Cano, olText);

			if (m_DOBK.GetWindowTextLength() != 0)
			{
				m_DOBK.GetWindowText(olText); 
				COleDateTime olDate = OleDateStringToDate(olText);
				if (olDate.GetStatus() == COleDateTime::valid)
				{
					CTime olTime		= HourStringToDate((CString)"00:00");
					olDate.Format("%d.%m.%Y");
					olTime.Format("%H:%M");
					pomStf->Dobk = COleDateTime(olDate.GetYear(),olDate.GetMonth(),olDate.GetDay(),olTime.GetHour(), olTime.GetMinute(), 0);
				}
				else
				{
					pomStf->Dobk.SetStatus(COleDateTime::null);
				}
			}
			else
			{
				pomStf->Dobk.SetStatus(COleDateTime::null);
			}

			m_GEBU.GetWindowText(olText); 
			if(!olText.IsEmpty( ))
			{
				COleDateTime olDate1 = OleDateStringToDate(olText);
				if (olDate1.GetStatus() == COleDateTime::valid)
				{
					CString test = olDate1.Format("%Y%m%d");
					strcpy(pomStf->Gebu, test);
				}
				else
				{
					strcpy(pomStf->Gebu, "");
				}
		
			}
			else
			{
				strcpy(pomStf->Gebu, "");
			}

			if(m_RELS.GetCheck() == 1) 
				strcpy(pomStf->Rels, "1");
			else
				strcpy(pomStf->Rels, "0");

			CDialog::OnOK();
		}
	}
	else
	{
		Beep(440,70);
		olErrorText += CString("\n") + olTableErrorText;
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
	}
}

//----------------------------------------------------------------------------------------

LONG MitarbeiterStammDlg::OnTableIPEditKillfocus( UINT wParam, LPARAM lParam)
{
	CCSIPEDITNOTIFY *prlNotify = (CCSIPEDITNOTIFY*)lParam;

	if(prlNotify->Text.IsEmpty())
		return 0L;
	if(prlNotify->SourceTable == pomOrgTable) 
	{
		if(prlNotify->Column == 0)
		{
			CedaOrgData olData;
			CCSPtrArray<ORGDATA> omData;
			char pclWhere[128];
			sprintf(pclWhere,"WHERE DPT1='%s'", prlNotify->Text);
			prlNotify->UserStatus = true;
			prlNotify->Status = olData.ReadSpecialData(&omData, pclWhere, "DPT1", false);
			omData.DeleteAll();
			return 0L;
		}
	}
	if(prlNotify->SourceTable == pomPfcTable) 
	{
		if(prlNotify->Column == 0)
		{
			CedaPfcData olData;
			CCSPtrArray<PFCDATA> omData;
			char pclWhere[128];
			sprintf(pclWhere,"WHERE FCTC='%s'", prlNotify->Text);
			prlNotify->UserStatus = true;
			prlNotify->Status = olData.ReadSpecialData(&omData, pclWhere, "FCTC", false);
			omData.DeleteAll();
			return 0L;
		}
	}
	if(prlNotify->SourceTable == pomPerTable) 
	{
		if(prlNotify->Column == 0)
		{
			CedaPerData olData;
			CCSPtrArray<PERDATA> omData;
			char pclWhere[128];
			sprintf(pclWhere,"WHERE PRMC='%s'", prlNotify->Text);
			prlNotify->UserStatus = true;
			prlNotify->Status = olData.ReadSpecialData(&omData, pclWhere, "PRMC", false);
			omData.DeleteAll();
			return 0L;
		}
	}
	if(prlNotify->SourceTable == pomCotTable) 
	{
		if(prlNotify->Column == 0)
		{
			CedaCotData olData;
			CCSPtrArray<COTDATA> omData;
			char pclWhere[128];
			sprintf(pclWhere,"WHERE CTRC='%s'", prlNotify->Text);
			prlNotify->UserStatus = true;
			prlNotify->Status = olData.ReadSpecialData(&omData, pclWhere, "CTRC", false);
			omData.DeleteAll();
			return 0L;
		}
	}
	if(prlNotify->SourceTable == pomPolTable) 
	{
		if(prlNotify->Column == 0)
		{
			CedaPolData olData;
			CCSPtrArray<POLDATA> omData;
			char pclWhere[128];
			sprintf(pclWhere,"WHERE NAME='%s'", prlNotify->Text);
			prlNotify->UserStatus = true;
			prlNotify->Status = olData.ReadSpecialData(&omData, pclWhere, "NAME", false);
			omData.DeleteAll();
			return 0L;
		}
	}
	if(prlNotify->SourceTable == pomWgpTable) 
	{
		if(prlNotify->Column == 0)
		{
			CedaWgpData olData;
			CCSPtrArray<WGPDATA> omData;
			char pclWhere[128];
			sprintf(pclWhere,"WHERE WGPC='%s'", prlNotify->Text);
			prlNotify->UserStatus = true;
			prlNotify->Status = olData.ReadSpecialData(&omData, pclWhere, "WGPC", false);
			omData.DeleteAll();
			return 0L;
		}
	}
	if(prlNotify->SourceTable == pomSreTable) 
	{
		if(prlNotify->Column == 0)
		{
			prlNotify->UserStatus = true;
			if (strcmp(prlNotify->Text,"R") == 0 || strcmp(prlNotify->Text,"I") == 0 || (strcmp(prlNotify->Text,"S") == 0 && "SIN" == ogParData.GetParValue("ROSTER","ID_PROD_CUSTOMER")))
				prlNotify->Status = true;
			else
				prlNotify->Status = false;	
			return 0L;
		}
	}

	return 0L;
}

//----------------------------------------------------------------------------------------

LONG MitarbeiterStammDlg::OnKillfocus(UINT lParam, LONG wParam) 
{
	CString olText;

	return 0L;
}

//----------------------------------------------------------------------------------------

void MitarbeiterStammDlg::OnBf1() 
{
	OnBf_Button(0);
}

void MitarbeiterStammDlg::OnBf2() 
{
	OnBf_Button(1);
}

void MitarbeiterStammDlg::OnBf3() 
{
	OnBf_Button(2);
}

void MitarbeiterStammDlg::OnBf4() 
{
	OnBf_Button(3);
}

void MitarbeiterStammDlg::OnBf5() 
{
	OnBf_Button(4);
}

//----------------------------------------------------------------------------------------

void MitarbeiterStammDlg::OnBf_Button(int ipColY)
{
	CStringArray olCol1, olCol2, olCol3;
	CCSPtrArray<PFCDATA> olList;
	AfxGetApp()->DoWaitCursor(1);
	AfxGetApp()->DoWaitCursor(1);
	CString olOrderBy = "ORDER BY FCTN";
	if(ogPfcData.ReadSpecialData(&olList, olOrderBy.GetBuffer(0), "URNO,FCTC,FCTN", false) == true)
	{
		AfxGetApp()->DoWaitCursor(-1);
		for(int i = 0; i < olList.GetSize(); i++)
		{
			char pclUrno[20]="";
			sprintf(pclUrno, "%ld", olList[i].Urno);
			olCol3.Add(CString(pclUrno));
			olCol1.Add(CString(olList[i].Fctc));
			olCol2.Add(CString(olList[i].Fctn));
		}
		AwDlg *pomDlg = new AwDlg(&olCol1, &olCol2, &olCol3, LoadStg(IDS_STRING237), LoadStg(IDS_STRING229), this);

/*#ifndef CLIENT_ZRH
		//*** 30.08.99 SHA ***		
		pomDlg->obShowCodes = TRUE;
		pomDlg->ogAct = &olActArray;
		pomDlg->ogAlt = &olAltArray;
#endif*/

		if(pomDlg->DoModal() == IDOK)
		{
			int ilColX = -1, ilColY = -1;
			if(((CCSTableListBox*)(pomPfcTable->GetCTableListBox()))->GetVisibleLeftTopColumn(ilColX, ilColY) == true)
			{
/*#ifndef CLIENT_ZRH
				//*** 30.08.99 SHA ***
				//*** Break out the two codes [3LC(5LC)] ACT ***
				CString olCode1,olCode2;
				olCode1 = pomDlg->olReturnAct;
				olCode2 = olCode1.Mid(olCode1.Find("(")+1);
				olCode2 = olCode2.Left(olCode2.Find(")"));
				olCode1 = olCode1.Left(olCode1.Find("("));
				if (olCode1.IsEmpty())
					olCode1 = " ";
				if (olCode2.IsEmpty())
					olCode2 = " ";
				//*** Break out the two codes [2LC(3LC)] ALC ***
				CString olCode3,olCode4;
				olCode3 = pomDlg->olReturnAlc;
				olCode4 = olCode3.Mid(olCode3.Find("(")+1);
				olCode4 = olCode4.Left(olCode4.Find(")"));
				olCode3 = olCode3.Left(olCode3.Find("("));
				if (olCode3.IsEmpty())
					olCode3 = " ";
				if (olCode4.IsEmpty())
					olCode4 = " ";
#endif*/

				ilColY += ipColY;
				pomPfcTable->SetIPValue(ilColY, 0, pomDlg->omReturnString);

/*#ifndef CLIENT_ZRH
				//*** ACT ***
				pomPfcTable->SetIPValue(ilColY, 4, olCode1);
				pomPfcTable->SetIPValue(ilColY, 5, olCode2);
				//*** ALC ***
				pomPfcTable->SetIPValue(ilColY, 6, olCode3);
				pomPfcTable->SetIPValue(ilColY, 7, olCode4);
#endif*/
				
				pomPfcTable->imCurrentColumn = 0;
				pomPfcTable->MakeInplaceEdit(ilColY);
			}
		}
		delete pomDlg;
	}
	else
	{
		MessageBox(LoadStg(IDS_STRING321), LoadStg(IDS_STRING149), (MB_ICONSTOP|MB_OK));
	}
	olList.DeleteAll();

	AfxGetApp()->DoWaitCursor(-1);
}

//----------------------------------------------------------------------------------------

void MitarbeiterStammDlg::OnBg1() 
{
	OnBg_Button(0);
}

void MitarbeiterStammDlg::OnBg2() 
{
	OnBg_Button(1);
}

void MitarbeiterStammDlg::OnBg3() 
{
	OnBg_Button(2);
}

void MitarbeiterStammDlg::OnBg4() 
{
	OnBg_Button(3);
}

void MitarbeiterStammDlg::OnBg5() 
{
	OnBg_Button(4);
}

//----------------------------------------------------------------------------------------

void MitarbeiterStammDlg::OnBg_Button(int ipColY)
{
	CStringArray olCol1, olCol2, olCol3;
	CCSPtrArray<POLDATA> olList;
	AfxGetApp()->DoWaitCursor(1);
	AfxGetApp()->DoWaitCursor(1);
	CString olOrderBy = "ORDER BY NAME";
	if(ogPolData.ReadSpecialData(&olList, olOrderBy.GetBuffer(0), "URNO,NAME,POOL", false) == true)
	{
		AfxGetApp()->DoWaitCursor(-1);
		for(int i = 0; i < olList.GetSize(); i++)
		{
			char pclUrno[20]="";
			sprintf(pclUrno, "%ld", olList[i].Urno);
			olCol3.Add(CString(pclUrno));
			olCol1.Add(CString(olList[i].Name));
			olCol2.Add(CString(olList[i].Pool));
		}
		AwDlg *pomDlg = new AwDlg(&olCol1, &olCol2, &olCol3, LoadStg(IDS_STRING237), LoadStg(IDS_STRING229), this);
		if(pomDlg->DoModal() == IDOK)
		{
			int ilColX = -1, ilColY = -1;
			if(((CCSTableListBox*)(pomPolTable->GetCTableListBox()))->GetVisibleLeftTopColumn(ilColX, ilColY) == true)
			{
				ilColY += ipColY;
				pomPolTable->SetIPValue(ilColY, 0, pomDlg->omReturnString);
				pomPolTable->imCurrentColumn = 0;
				pomPolTable->MakeInplaceEdit(ilColY);
			}
		}
		delete pomDlg;
	}
	else
	{
		MessageBox(LoadStg(IDS_STRING321), LoadStg(IDS_STRING149), (MB_ICONSTOP|MB_OK));
	}
	olList.DeleteAll();

	AfxGetApp()->DoWaitCursor(-1);
}

//----------------------------------------------------------------------------------------

void MitarbeiterStammDlg::OnBo1() 
{
	OnBo_Button(0);
}

void MitarbeiterStammDlg::OnBo2() 
{
	OnBo_Button(1);
}

void MitarbeiterStammDlg::OnBo3() 
{
	OnBo_Button(2);
}

void MitarbeiterStammDlg::OnBo4() 
{
	OnBo_Button(3);
}

void MitarbeiterStammDlg::OnBo5() 
{
	OnBo_Button(4);
}

//----------------------------------------------------------------------------------------

void MitarbeiterStammDlg::OnBo_Button(int ipColY)
{
	CCSPtrArray<ORGDATA> olList;
	AfxGetApp()->DoWaitCursor(1);
	AfxGetApp()->DoWaitCursor(1);
	if(ogOrgData.ReadSpecialData(&olList, "", "URNO", false) == true)
	{
		AfxGetApp()->DoWaitCursor(-1);

		int ilColX = -1, ilColY = -1;
		((CCSTableListBox*)(pomOrgTable->GetCTableListBox()))->GetVisibleLeftTopColumn(ilColX, ilColY);
		int ilLineNo = ipColY + ilColY;

		CCSPtrArray <TABLE_COLUMN> olLine;
		pomOrgTable->GetTextLineColumns(&olLine, ilLineNo);

		if(olLine.GetSize()>=6)
		{
			CSorDlg *pomDlg = new CSorDlg(olLine[0].Text, olLine[4].Text, olLine[5].Text, this);
			if(pomDlg->DoModal() == IDOK)
			{
				int ilColX = -1, ilColY = -1;
				if(((CCSTableListBox*)(pomOrgTable->GetCTableListBox()))->GetVisibleLeftTopColumn(ilColX, ilColY) == true)
				{
					ilColY += ipColY;
					pomOrgTable->SetIPValue(ilColY, 0, pomDlg->omCode);
					pomOrgTable->SetIPValue(ilColY, 4, pomDlg->omOdgc);
					pomOrgTable->SetIPValue(ilColY, 5, pomDlg->omLead);

					pomOrgTable->imCurrentColumn = 0;
					pomOrgTable->MakeInplaceEdit(ilColY);
				}
			}
			delete pomDlg;
		}
	}
	else
	{
		MessageBox(LoadStg(IDS_STRING321), LoadStg(IDS_STRING149), (MB_ICONSTOP|MB_OK));
	}
	olList.DeleteAll();
	AfxGetApp()->DoWaitCursor(-1);
}

//----------------------------------------------------------------------------------------

void MitarbeiterStammDlg::OnBq1() 
{
	OnBq_Button(0);
}

void MitarbeiterStammDlg::OnBq2() 
{
	OnBq_Button(1);
}

void MitarbeiterStammDlg::OnBq3() 
{
	OnBq_Button(2);
}

void MitarbeiterStammDlg::OnBq4() 
{
	OnBq_Button(3);
}

void MitarbeiterStammDlg::OnBq5() 
{
	OnBq_Button(4);
}

//----------------------------------------------------------------------------------------

void MitarbeiterStammDlg::OnBq_Button(int ipColY)
{
	CStringArray olCol1, olCol2, olCol3;
	CCSPtrArray<PERDATA> olList;

	CString cDmy;
	AfxGetApp()->DoWaitCursor(1);
	AfxGetApp()->DoWaitCursor(1);
	CString olOrderBy = "ORDER BY PRMN";
	if(ogPerData.ReadSpecialData(&olList, olOrderBy.GetBuffer(0), "URNO,PRMC,PRMN", false) == true)

	{
		AfxGetApp()->DoWaitCursor(-1);
		for(int i = 0; i < olList.GetSize(); i++)
		{
			char pclUrno[20]="";
			sprintf(pclUrno, "%ld", olList[i].Urno);
			olCol3.Add(CString(pclUrno));
			olCol1.Add(CString(olList[i].Prmc));
			olCol2.Add(CString(olList[i].Prmn));
		}

		AwDlg *pomDlg = new AwDlg(&olCol1, &olCol2, &olCol3, LoadStg(IDS_STRING237), LoadStg(IDS_STRING229), this);


		if(pomDlg->DoModal() == IDOK)
		{
			int ilColX = -1, ilColY = -1;
			if(((CCSTableListBox*)(pomPerTable->GetCTableListBox()))->GetVisibleLeftTopColumn(ilColX, ilColY) == true)
			{

				ilColY += ipColY;
				pomPerTable->SetIPValue(ilColY, 0, pomDlg->omReturnString);

				pomPerTable->imCurrentColumn = 0;
				pomPerTable->MakeInplaceEdit(ilColY);
			}
		}
		delete pomDlg;
	}
	else
	{
		MessageBox(LoadStg(IDS_STRING321), LoadStg(IDS_STRING149), (MB_ICONSTOP|MB_OK));
	}
	olList.DeleteAll();

	AfxGetApp()->DoWaitCursor(-1);
}

//----------------------------------------------------------------------------------------

void MitarbeiterStammDlg::OnBa1() 
{
	OnBa_Button(0);
}

void MitarbeiterStammDlg::OnBa2() 
{
	OnBa_Button(1);
}

void MitarbeiterStammDlg::OnBa3() 
{
	OnBa_Button(2);
}

void MitarbeiterStammDlg::OnBa4() 
{
	OnBa_Button(3);
}

void MitarbeiterStammDlg::OnBa5() 
{
	OnBa_Button(4);
}

//----------------------------------------------------------------------------------------

void MitarbeiterStammDlg::OnBa_Button(int ipColY) 
{
	/////////////////////////////////////////////////////////////////////////////
	// COT bzw. SCO
	/////////////////////////////////////////////////////////////////////////////

	CCSPtrArray<COTDATA> olList;
	AfxGetApp()->DoWaitCursor(1);
	AfxGetApp()->DoWaitCursor(1);
	if(ogCotData.ReadSpecialData(&olList, "", "URNO", false) == true)
	{
		AfxGetApp()->DoWaitCursor(-1);

		int ilColX = -1, ilColY = -1;
		((CCSTableListBox*)(pomCotTable->GetCTableListBox()))->GetVisibleLeftTopColumn(ilColX, ilColY);
		int ilLineNo = ipColY + ilColY;

		CCSPtrArray <TABLE_COLUMN> olLine;
		pomCotTable->GetTextLineColumns(&olLine, ilLineNo);
		if(olLine.GetSize() >= 6)
		{
			CSco_Dlg *pomDlg = new CSco_Dlg(olLine[0].Text, olLine[4].Text, olLine[5].Text, this);
			if(pomDlg->DoModal() == IDOK)
			{
				int ilColX = -1, ilColY = -1;
				if(((CCSTableListBox*)(pomCotTable->GetCTableListBox()))->GetVisibleLeftTopColumn(ilColX, ilColY) == true)
				{
					ilColY += ipColY;
					pomCotTable->SetIPValue(ilColY, 0, pomDlg->omCode);
					pomCotTable->SetIPValue(ilColY, 4, pomDlg->omKost);
					pomCotTable->SetIPValue(ilColY, 5, pomDlg->omCweh);

					pomCotTable->imCurrentColumn = 0;
					pomCotTable->MakeInplaceEdit(ilColY);
				}
			}
			delete pomDlg;
		}
	}
	else
	{
		MessageBox(LoadStg(IDS_STRING321), LoadStg(IDS_STRING149), (MB_ICONSTOP|MB_OK));
	}
	olList.DeleteAll();
	AfxGetApp()->DoWaitCursor(-1);

}

//----------------------------------------------------------------------------------------

void MitarbeiterStammDlg::OnBgrp1() 
{
	OnBgrp_Button(0);
	
}

void MitarbeiterStammDlg::OnBgrp2() 
{
	OnBgrp_Button(1);
	
}

void MitarbeiterStammDlg::OnBgrp3() 
{
	OnBgrp_Button(2);
	
}

void MitarbeiterStammDlg::OnBgrp4() 
{
	OnBgrp_Button(3);
	
}

void MitarbeiterStammDlg::OnBgrp5() 
{
	OnBgrp_Button(4);
	
}

void MitarbeiterStammDlg::OnBgrp_Button(int ipColY)
{
	CStringArray olCol1, olCol2, olCol3;
	CCSPtrArray<WGPDATA> olList;
	AfxGetApp()->DoWaitCursor(1);
	AfxGetApp()->DoWaitCursor(1);
	CString olOrderBy = "ORDER BY WGPN";
	if(ogWgpData.ReadSpecialData(&olList, olOrderBy.GetBuffer(0), "URNO,WGPC,WGPN", false) == true)
	{
		AfxGetApp()->DoWaitCursor(-1);
		for(int i = 0; i < olList.GetSize(); i++)
		{
			char pclUrno[20]="";
			sprintf(pclUrno, "%ld", olList[i].Urno);
			olCol3.Add(CString(pclUrno));
			olCol1.Add(CString(olList[i].Wgpc));
			olCol2.Add(CString(olList[i].Wgpn));
		}
		AwDlg *pomDlg = new AwDlg(&olCol1, &olCol2, &olCol3, LoadStg(IDS_STRING237), LoadStg(IDS_STRING229), this);
		if(pomDlg->DoModal() == IDOK)
		{
			int ilColX = -1, ilColY = -1;
			if(((CCSTableListBox*)(pomWgpTable->GetCTableListBox()))->GetVisibleLeftTopColumn(ilColX, ilColY) == true)
			{
				ilColY += ipColY;
				pomWgpTable->SetIPValue(ilColY, 0, pomDlg->omReturnString);
				pomWgpTable->imCurrentColumn = 0;
				pomWgpTable->MakeInplaceEdit(ilColY);
			}
		}
		delete pomDlg;
	}
	else
	{
		MessageBox(LoadStg(IDS_STRING321), LoadStg(IDS_STRING149), (MB_ICONSTOP|MB_OK));
	}
	olList.DeleteAll();

	AfxGetApp()->DoWaitCursor(-1);
}

//----------------------------------------------------------------------------------------

//*** 02.11.99 SHA                        ***
//*** NEW SYSTEM OF SAVING QUALIFICATIONS ***
//*** ITALIAN REQUIREMENT !               ***
//*** e.g. QUALIFIKATION: TDL_MD8_AZ      ***
long MitarbeiterStammDlg::SearchFctc(CString ilFctc)
{

	long ilCount;
	char clWhere[100];

	sprintf(clWhere,"WHERE FCTC='%s'",ilFctc);
	ogPerData.Read(clWhere);
	
	ilCount = ogPerData.omData.GetSize();

	return ilCount;
}

//*** 02.11.99 SHA ***
void MitarbeiterStammDlg::SaveNewFctc(CString ilFctc)
{
	
	PERDATA *prlPer = new PERDATA;

	sprintf(prlPer->Prmc,"%s", ilFctc);
	sprintf(prlPer->Prmn,"%s", ilFctc);
	sprintf(prlPer->Usec,"%s", "AUTO_STF_DIALOG");
	prlPer->IsChanged = DATA_NEW;
	prlPer->Urno = ogBasicData.GetNextUrno();
	prlPer->Cdat = CTime::GetCurrentTime();

	ogPerData.Save(prlPer);

	delete prlPer;

}


void MitarbeiterStammDlg::OnAdr() 
{
	CString STFU = "";
	STFU.Format("%ld",pomStf->Urno);
	CADR_Dlg *pomAdr = new CADR_Dlg(this ,STFU);
	pomAdr->DoModal();
	// TODO: Code f�r die Behandlungsroutine der Steuerelement-Benachrichtigung hier einf�gen
}

//----------------------------------------------------------------------------------------

void MitarbeiterStammDlg::OnBr1() 
{
	OnBr_Button(0);
}

void MitarbeiterStammDlg::OnBr2() 
{
	OnBr_Button(1);
}

void MitarbeiterStammDlg::OnBr3() 
{
	OnBr_Button(2);
}

void MitarbeiterStammDlg::OnBr4() 
{
	OnBr_Button(3);
}

void MitarbeiterStammDlg::OnBr5() 
{
	OnBr_Button(4);
}

//----------------------------------------------------------------------------------------

void MitarbeiterStammDlg::OnBr_Button(int ipColY)
{
	CStringArray olCol1, olCol2, olCol3;
	
	olCol3.Add(CString("4711"));
	olCol1.Add(CString("R"));
	olCol2.Add(LoadStg(IDS_STRING985));
	olCol3.Add(CString("4712"));
	olCol1.Add(CString("I"));
	olCol2.Add(LoadStg(IDS_STRING986));
	
	// split shift support for SIN only
	if ("SIN" == ogParData.GetParValue("ROSTER","ID_PROD_CUSTOMER"))
	{
		olCol3.Add(CString("4713"));
		olCol1.Add(CString("S"));
		olCol2.Add(LoadStg(IDS_STRING989));

	}
	
	AwDlg *pomDlg = new AwDlg(&olCol1, &olCol2, &olCol3, LoadStg(IDS_STRING237), LoadStg(IDS_STRING229), this);
	if(pomDlg->DoModal() == IDOK)
	{
		int ilColX = -1, ilColY = -1;
		if(((CCSTableListBox*)(pomSreTable->GetCTableListBox()))->GetVisibleLeftTopColumn(ilColX, ilColY) == true)
		{
			ilColY += ipColY;
			pomSreTable->SetIPValue(ilColY, 0, pomDlg->omReturnString);
			pomSreTable->imCurrentColumn = 0;
			pomSreTable->MakeInplaceEdit(ilColY);
		}
	}
	
	delete pomDlg;

}

bool MitarbeiterStammDlg::Range::IsOverlappedWith(const MitarbeiterStammDlg::Range& opRange)
{
	if (this->omType != opRange.omType)
		return false;
	return IsOverlapped(this->omFrom,this->omTo,opRange.omFrom,opRange.omTo);
}

bool MitarbeiterStammDlg::CheckDateRange(CCSTable *popTable,int ipFromColumn,int ipToColumn,int ipTypeColumn,const CString& ropSection,CString& ropTableErrorText)
{
	if (popTable == NULL)
		return false;

	CCSPtrArray<Range>	olRanges;		

	for(int i = 0; i < 100; i++)
	{
		CString	olRow;
		CCSPtrArray<TABLE_COLUMN> olLine;

		olRow.Format("%d",i+1);
		popTable->GetTextLineColumns(&olLine, i);

		if (olLine[ipFromColumn].Text != CString(""))
		{
			Range olRange;
			olRange.omFrom = OleDateStringToDate(olLine[ipFromColumn].Text);
			olRange.omTo   = OleDateStringToDate(olLine[ipToColumn].Text);

			if (ipTypeColumn >= 0)
				olRange.omType = olLine[ipTypeColumn].Text;

			bool blOverlapped = false;
			for (int j = 0; j < olRanges.GetSize(); j++)
			{
				if (olRange.IsOverlappedWith(olRanges[j]))
				{
					popTable->SetTextColumnColor(i,ipFromColumn, COLORREF(RED), COLORREF(WHITE));
					popTable->SetTextColumnColor(i,ipToColumn, COLORREF(RED), COLORREF(WHITE));
					ropTableErrorText += ropSection + LoadStg(IDS_STRING85) + olRow + LoadStg(IDS_STRING993);
					blOverlapped = true;
					break;
				}
			}

			if (!blOverlapped)
			{
				olRanges.New(olRange);
			}
			else
				return false;
		}

		olLine.RemoveAll();
	}

	return true;
}