#ifndef __PAGETABLEVIEWER_H__
#define __PAGETABLEVIEWER_H__

#include <stdafx.h>
#include <CedaPAGData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct PAGETABLE_LINEDATA
{
	long	Urno;
	CString	Pagi;
	CString	Pcfn;
	CString	Pdpt;
	CString	Pdti;
	CString	Pnof;
	CString	Pfnt;
	CString	Pdss;
	CString	Pcti;
	CString	Ptfb;
	CString Ptfe;
	CString Ptd1;
	CString Ptd2;
	CString Prfn;
	CString Ptdc;
	CString Cotb;
	CString Rema;
};

/////////////////////////////////////////////////////////////////////////////
// PageTableViewer

class PageTableViewer : public CViewer
{
// Constructions
public:
    PageTableViewer(CCSPtrArray<PAGDATA> *popData);
    ~PageTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(PAGDATA *prpAirline);
	int  ComparePage(PAGETABLE_LINEDATA *prpAirline1, PAGETABLE_LINEDATA *prpAirline2);
    void MakeLines();
	void MakeLine(PAGDATA *prpAirline);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(PAGETABLE_LINEDATA *prpAirline);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(PAGETABLE_LINEDATA *prpLine);
	void ProcessPageChange(PAGDATA *prpAirline);
	void ProcessPageDelete(PAGDATA *prpAirline);
	bool FindPage(char *prpPageName, int& ilItem);
	bool CreateExcelFile(const CString& ropSeparator,const CString& ropListSeparator);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable			*pomPageTable;
	CCSPtrArray<PAGDATA>*pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<PAGETABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(int ilPage,CUIntArray& ropColsPerPage,PAGETABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader(int ilPage,CUIntArray& ropColsPerPage);
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;
	CString	omFileName;

};

#endif //__PAGETABLEVIEWER_H__
