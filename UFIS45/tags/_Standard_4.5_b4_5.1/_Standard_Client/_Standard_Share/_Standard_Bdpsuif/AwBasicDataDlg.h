#if !defined(AFX_AWBASICDATADLG_H__5EF2F571_9E1E_11D1_837C_0080AD1DC701__INCLUDED_)
#define AFX_AWBASICDATADLG_H__5EF2F571_9E1E_11D1_837C_0080AD1DC701__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// AwBasicDataDlg.h : header file
//

#include "RecordSet.h"

/////////////////////////////////////////////////////////////////////////////
// AwBasicDataDlg dialog

class AwBasicDataDlg : public CDialog
{
// Construction
public:
	AwBasicDataDlg(CWnd* pParent = NULL, CString opObject = "" , CString opFields = "", CString opSortFields = "", CString opSelStrings = "");   // standard constructor

	~AwBasicDataDlg();

private:
// Dialog Data
	//{{AFX_DATA(AwBasicDataDlg)
	enum { IDD = IDD_AWBASICDATA };
	CButton	m_CB_OK;
	CButton	m_CB_Cancel;
	CStatic	m_CS_Header;
	CListBox	m_CL_List;
	//}}AFX_DATA

	CString omObject;
	CString omFields;
	CString omSortFields;
	CString omSelStrings;
	int imLastSortIndex;

	RecordSet *pomRecord;

public:
	CString GetField(CString opField);
	void SetSecState(const CString &ropSecStr);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(AwBasicDataDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

private:
	char cmSecState;


// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(AwBasicDataDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDblclkList();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AWBASICDATADLG_H__5EF2F571_9E1E_11D1_837C_0080AD1DC701__INCLUDED_)
