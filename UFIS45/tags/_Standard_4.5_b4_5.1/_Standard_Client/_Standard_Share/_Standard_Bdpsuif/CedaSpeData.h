// CedaSpeData.h

#ifndef __CEDAPSPEDATA__
#define __CEDAPSPEDATA__
 
#include <stdafx.h>
#include <basicdata.h>
#include <afxdisp.h>

//---------------------------------------------------------------------------------------------------------

struct SPEDATA 
{
	long Urno;			// Eindeutige Datensatz-Nr.
	long Surn;			// Referenz auf STF.URNO
	char Code[7];		// Codereferenz ORG.CODE	
	COleDateTime Vpfr;	// G�ltig von	
	COleDateTime Vpto;	// G�ltig bis
	
	//DataCreated by this class
	int      IsChanged;

	//long, CTime
	SPEDATA(void)
	{ 
		memset(this,'\0',sizeof(*this));
		//Vpfr=-1,Vpto=-1; //<zB.(FIELD_DATE Felder)
	}

}; // end SPEDataStruct

//---------------------------------------------------------------------------------------------------------
// Class declaratino

class CedaSpeData: public CCSCedaData
{
// Attributes
public:
    CMapPtrToPtr omUrnoMap;
	CMapPtrToPtr omStaffUrnoMap;
	CMapPtrToPtr omStaffPermMap;	

    CCSPtrArray<SPEDATA> omData;

	char pcmListOfFields[2048];

// OSpeations
public:
    CedaSpeData();
	~CedaSpeData();
	virtual void GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType);
	void Register(void);
	void ClearAll(bool bpWithRegistration = true);
    bool Read(char *pspWhere = NULL);
	bool Insert(SPEDATA *prpSpe);
	bool InsertInternal(SPEDATA *prpSpe);
	bool Update(SPEDATA *prpSpe);
	bool UpdateInternal(SPEDATA *prpSpe);
	bool Delete(long lpUrno);
	bool DeleteInternal(SPEDATA *prpSpe);
	bool ReadSpecialData(CCSPtrArray<SPEDATA> *popSpe,char *pspWhere,char *pspFieldList,bool ipSYS=true);
	bool Save(SPEDATA *prpSpe);
	void ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	SPEDATA *GetSpeByUrno(long lpUrno);
	SPEDATA *GetValidSpeBySurn(long lpSurn);
	CString	GetValidPermissionBySurn(long lpSurn);


	// Private methods
private:
    void PrepareSpeData(SPEDATA *prpSpeData);

};

//---------------------------------------------------------------------------------------------------------


#endif //__CEDAPSPEDATA__
