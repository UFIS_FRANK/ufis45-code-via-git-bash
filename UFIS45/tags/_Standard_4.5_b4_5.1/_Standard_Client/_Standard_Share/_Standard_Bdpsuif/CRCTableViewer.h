// RCTableViewer.h: interface for the CRCTableViewer class.   //PRF 8378
//
//////////////////////////////////////////////////////////////////////

#ifndef __RCTABLEVIEWER_H__
#define __RCTABLEVIEWER_H__

#include <stdafx.h>
#include <CedaCrcData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>

struct CRCTABLE_LINEDATA
{
	long	Urno;
	CString	Hopo;	// Home Airport
	CString Code;	// Reason Code
	CString Rema;	// Reason Description
	CString Gatf;	// Flag for Gate Changes
	CString Posf;	// Flag for Position Changes
	CString Cxxf;
	CString Belt;//UFIS-1184
	CString	Vafr;
	CString	Vato;
};

class CRCTableViewer : public CViewer  
{
public:
	CRCTableViewer(CCSPtrArray<CRCDATA> *popData);
	virtual ~CRCTableViewer();

	void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:

	bool IsPassFilter(CRCDATA *prpChgReasonCode);
	int CompareChgReasonCode(CRCTABLE_LINEDATA *prpChgReasonCode1, CRCTABLE_LINEDATA *prpChgReasonCode2);
    void MakeLines();
	void MakeLine(CRCDATA *prpChgReasonCode);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(CRCTABLE_LINEDATA *prpChgReasonCode);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(CRCTABLE_LINEDATA *prpLine);
	void ProcessChgReasonCodeChange(CRCDATA *prpChgReasonCode);
	void ProcessChgReasonCodeDelete(CRCDATA *prpChgReasonCode);
	bool FindChgReasonCode(char *prpChgReasonCodeKeya, char *prpChgReasonCodeKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomCRCTable;
	CCSPtrArray<CRCDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<CRCTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(CRCTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif // __RCTABLEVIEWER_H__
