// HolidayTableViewer.cpp 
//

#include <stdafx.h>
#include <HolidayTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void HolidayTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// HolidayTableViewer
//

HolidayTableViewer::HolidayTableViewer(CCSPtrArray<HOLDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomHolidayTable = NULL;
    ogDdx.Register(this, HOL_CHANGE, CString("HOLIDAYTABLEVIEWER"), CString("Holiday Update"),	HolidayTableCf);
	ogDdx.Register(this, HOL_NEW,	 CString("HOLIDAYTABLEVIEWER"), CString("Holiday New"),		HolidayTableCf);
	ogDdx.Register(this, HOL_DELETE, CString("HOLIDAYTABLEVIEWER"), CString("Holiday Delete"),	HolidayTableCf);
}

//-----------------------------------------------------------------------------------------------

HolidayTableViewer::~HolidayTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void HolidayTableViewer::Attach(CCSTable *popTable)
{
    pomHolidayTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void HolidayTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void HolidayTableViewer::MakeLines()
{
	int ilHolidayCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilHolidayCount; ilLc++)
	{
		HOLDATA *prlHolidayData = &pomData->GetAt(ilLc);
		MakeLine(prlHolidayData);
	}
}

//-----------------------------------------------------------------------------------------------

void HolidayTableViewer::MakeLine(HOLDATA *prpHoliday)
{
    //if( !IsPassFilter(prpHoliday)) return;

    // Update viewer data for this shift record
    HOLIDAYTABLE_LINEDATA rlHoliday;
	rlHoliday.Urno = prpHoliday->Urno; 
	rlHoliday.Hday = prpHoliday->Hday.Format("%d.%m.%Y"); 
	rlHoliday.Type = prpHoliday->Type;
	rlHoliday.Rema = prpHoliday->Rema; 
	CreateLine(&rlHoliday);
}

//-----------------------------------------------------------------------------------------------

void HolidayTableViewer::CreateLine(HOLIDAYTABLE_LINEDATA *prpHoliday)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareHoliday(prpHoliday, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	HOLIDAYTABLE_LINEDATA rlHoliday;
	rlHoliday = *prpHoliday;
    omLines.NewAt(ilLineno, rlHoliday);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void HolidayTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 2;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomHolidayTable->SetShowSelection(TRUE);
	pomHolidayTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 83; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING108),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING108),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 332; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING108),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomHolidayTable->SetHeaderFields(omHeaderDataArray);
	pomHolidayTable->SetDefaultSeparator();
	pomHolidayTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Hday;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Type;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Rema;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomHolidayTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomHolidayTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString HolidayTableViewer::Format(HOLIDAYTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

BOOL HolidayTableViewer::FindHoliday(char *pcpHolidayKeya, char *pcpHolidayKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpHolidayKeya) &&
			 (omLines[ilItem].Keyd == pcpHolidayKeyd)    )
			return TRUE;*/
	}
	return FALSE;
}

//-----------------------------------------------------------------------------------------------

static void HolidayTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    HolidayTableViewer *polViewer = (HolidayTableViewer *)popInstance;
    if (ipDDXType == HOL_CHANGE || ipDDXType == HOL_NEW) polViewer->ProcessHolidayChange((HOLDATA *)vpDataPointer);
    if (ipDDXType == HOL_DELETE) polViewer->ProcessHolidayDelete((HOLDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------
void HolidayTableViewer::ProcessHolidayChange(HOLDATA *prpHoliday)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpHoliday->Hday.Format("%d.%m.%Y"); 
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpHoliday->Type;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpHoliday->Rema; 
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	if (FindLine(prpHoliday->Urno, ilItem))
	{
        HOLIDAYTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpHoliday->Urno;
		prlLine->Hday = prpHoliday->Hday.Format("%d.%m.%Y");
		prlLine->Type = prpHoliday->Type;
		prlLine->Rema = prpHoliday->Rema;
	
		pomHolidayTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomHolidayTable->DisplayTable();
	}
	else
	{
		MakeLine(prpHoliday);
		if (FindLine(prpHoliday->Urno, ilItem))
		{
	        HOLIDAYTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomHolidayTable->AddTextLine(olLine, (void *)prlLine);
				pomHolidayTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void HolidayTableViewer::ProcessHolidayDelete(HOLDATA *prpHoliday)
{
	int ilItem;
	if (FindLine(prpHoliday->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomHolidayTable->DeleteTextLine(ilItem);
		pomHolidayTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

BOOL HolidayTableViewer::IsPassFilter(HOLDATA *prpHoliday)
{

	return TRUE;
}

//-----------------------------------------------------------------------------------------------

int HolidayTableViewer::CompareHoliday(HOLIDAYTABLE_LINEDATA *prpHoliday1, HOLIDAYTABLE_LINEDATA *prpHoliday2)
{
	return 0;
}

//-----------------------------------------------------------------------------------------------

void HolidayTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

BOOL HolidayTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return TRUE;
	}
    return FALSE;
}
//-----------------------------------------------------------------------------------------------

void HolidayTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void HolidayTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING198);
	int ilOrientation = PRINT_PORTRAET;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool HolidayTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool HolidayTableViewer::PrintTableLine(HOLIDAYTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
			case 0:
				{
					rlElement.FrameLeft  = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Hday;
				}
				break;
			case 1:
				{
					rlElement.Text		= prpLine->Type;
				}
				break;
			case 2:
				{
					rlElement.FrameRight = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Rema;
				}
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
