// BDPSUIF.cpp : Defines the class behaviors for the application.
//
#include <stdafx.h>
#include <BDPSUIF.h>
#include <UFIS.h>
#include <LoginDlg.h>
#include <PrivList.h>
#include <RegisterDlg.h>
#include <Stammdaten.h>
#include <CViewer.h>
#include <CedaBasicData.h>
#include <CedaAloData.h>
#include <AatHelp.h>
#include <VersionInfo.h>
#include <AatLogin.h>
#include <CedaInitModuData.h>
#include <PrmConfig.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

char cgUserName[34];
char cgUserPWD[34];


CString ogWks;
bool bgIsPrm = false;
bool bgCxxfDisp = true;//UFIS-1184
bool bgCcode ; // To accept one character as country code


CStringArray  ogCmdLineStghArray;


void CBDPSUIFApp::InitBDPSTableDefaultView()
{
	CViewer olViewer;
	CStringArray olPossibleFilters;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer.SetViewerKey("UIF_AIRLINE");
	CString olLastView = olViewer.SelectView();	// remember the current view
	olViewer.CreateView("<Default>", olPossibleFilters);
	olViewer.SelectView("<Default>");
	if (olLastView != "")
		olViewer.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer1;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer1.SetViewerKey("UIF_AIRCRAFT");
	olLastView = olViewer1.SelectView();	// remember the current view
	olViewer1.CreateView("<Default>", olPossibleFilters);
	olViewer1.SelectView("<Default>");
	if (olLastView != "")
		olViewer1.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer2;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer2.SetViewerKey("UIF_LFZREGISTR");
	olLastView = olViewer2.SelectView();	// remember the current view
	olViewer2.CreateView("<Default>", olPossibleFilters);
	olViewer2.SelectView("<Default>");
	if (olLastView != "")
		olViewer2.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer3;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer3.SetViewerKey("UIF_AIRPORT");
	olLastView = olViewer3.SelectView();	// remember the current view
	olViewer3.CreateView("<Default>", olPossibleFilters);
	olViewer3.SelectView("<Default>");
	if (olLastView != "")
		olViewer3.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer4;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer4.SetViewerKey("UIF_RUNWAY");
	olLastView = olViewer4.SelectView();	// remember the current view
	olViewer4.CreateView("<Default>", olPossibleFilters);
	olViewer4.SelectView("<Default>");
	if (olLastView != "")
		olViewer4.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer5;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer5.SetViewerKey("UIF_TAXIWAY");
	olLastView = olViewer5.SelectView();	// remember the current view
	olViewer5.CreateView("<Default>", olPossibleFilters);
	olViewer5.SelectView("<Default>");
	if (olLastView != "")
		olViewer5.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer6;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer6.SetViewerKey("UIF_POSITION");
	olLastView = olViewer6.SelectView();	// remember the current view
	olViewer6.CreateView("<Default>", olPossibleFilters);
	olViewer6.SelectView("<Default>");
	if (olLastView != "")
		olViewer6.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer7;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer7.SetViewerKey("UIF_GATE");
	olLastView = olViewer7.SelectView();	// remember the current view
	olViewer7.CreateView("<Default>", olPossibleFilters);
	olViewer7.SelectView("<Default>");
	if (olLastView != "")
		olViewer7.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer8;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer8.SetViewerKey("UIF_CHECKINCOUNTER");
	olLastView = olViewer8.SelectView();	// remember the current view
	olViewer8.CreateView("<Default>", olPossibleFilters);
	olViewer8.SelectView("<Default>");
	if (olLastView != "")
		olViewer8.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer9;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer9.SetViewerKey("UIF_BEGGAGEBELT");
	olLastView = olViewer9.SelectView();	// remember the current view
	olViewer9.CreateView("<Default>", olPossibleFilters);
	olViewer9.SelectView("<Default>");
	if (olLastView != "")
		olViewer9.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer10;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer10.SetViewerKey("UIF_EXIT");
	olLastView = olViewer10.SelectView();	// remember the current view
	olViewer10.CreateView("<Default>", olPossibleFilters);
	olViewer10.SelectView("<Default>");
	if (olLastView != "")
		olViewer10.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer11;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer11.SetViewerKey("UIF_DELAYCODE");
	olLastView = olViewer11.SelectView();	// remember the current view
	olViewer11.CreateView("<Default>", olPossibleFilters);
	olViewer11.SelectView("<Default>");
	if (olLastView != "")
		olViewer11.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer12;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer12.SetViewerKey("UIF_TELEXADRESS");
	olLastView = olViewer12.SelectView();	// remember the current view
	olViewer12.CreateView("<Default>", olPossibleFilters);
	olViewer12.SelectView("<Default>");
	if (olLastView != "")
		olViewer12.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer13;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer13.SetViewerKey("UIF_TRAFFICTYPE");
	olLastView = olViewer13.SelectView();	// remember the current view
	olViewer13.CreateView("<Default>", olPossibleFilters);
	olViewer13.SelectView("<Default>");
	if (olLastView != "")
		olViewer13.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer14;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer14.SetViewerKey("UIF_HANDLINGAGENT");
	olLastView = olViewer14.SelectView();	// remember the current view
	olViewer14.CreateView("<Default>", olPossibleFilters);
	olViewer14.SelectView("<Default>");
	if (olLastView != "")
		olViewer14.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer15;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer15.SetViewerKey("UIF_WAITINGROOM");
	olLastView = olViewer15.SelectView();	// remember the current view
	olViewer15.CreateView("<Default>", olPossibleFilters);
	olViewer15.SelectView("<Default>");
	if (olLastView != "")
		olViewer15.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer16;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer16.SetViewerKey("UIF_HANDLINGTYPE");
	olLastView = olViewer16.SelectView();	// remember the current view
	olViewer16.CreateView("<Default>", olPossibleFilters);
	olViewer16.SelectView("<Default>");
	if (olLastView != "")
		olViewer16.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer17;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer17.SetViewerKey("UIF_SERVICETYPE");
	olLastView = olViewer17.SelectView();	// remember the current view
	olViewer17.CreateView("<Default>", olPossibleFilters);
	olViewer17.SelectView("<Default>");
	if (olLastView != "")
		olViewer17.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer18;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer18.SetViewerKey("UIF_FIDSCOMMAND");
	olLastView = olViewer18.SelectView();	// remember the current view
	olViewer18.CreateView("<Default>", olPossibleFilters);
	olViewer18.SelectView("<Default>");
	if (olLastView != "")
		olViewer18.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer19;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer19.SetViewerKey("UIF_ERGVERKEHRSAR");
	olLastView = olViewer19.SelectView();	// remember the current view
	olViewer19.CreateView("<Default>", olPossibleFilters);
	olViewer19.SelectView("<Default>");
	if (olLastView != "")
		olViewer19.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer20;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer20.SetViewerKey("UIF_STDARDVERKETT");
	olLastView = olViewer20.SelectView();	// remember the current view
	olViewer20.CreateView("<Default>", olPossibleFilters);
	olViewer20.SelectView("<Default>");
	if (olLastView != "")
		olViewer20.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer21;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer21.SetViewerKey("UIF_FLUGPLANSAISON");
	olLastView = olViewer21.SelectView();	// remember the current view
	olViewer21.CreateView("<Default>", olPossibleFilters);
	olViewer21.SelectView("<Default>");
	if (olLastView != "")
		olViewer21.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer22;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer22.SetViewerKey("UIF_LEISTUNGSKATA");
	olLastView = olViewer22.SelectView();	// remember the current view
	olViewer22.CreateView("<Default>", olPossibleFilters);
	olViewer22.SelectView("<Default>");
	if (olLastView != "")
		olViewer22.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer23;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer23.SetViewerKey("UIF_QUALIFIKATIONEN");
	olLastView = olViewer23.SelectView();	// remember the current view
	olViewer23.CreateView("<Default>", olPossibleFilters);
	olViewer23.SelectView("<Default>");
	if (olLastView != "")
		olViewer23.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer24;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer24.SetViewerKey("UIF_ORGANISATIONSEINHEITEN");
	olLastView = olViewer24.SelectView();	// remember the current view
	olViewer24.CreateView("<Default>", olPossibleFilters);
	olViewer24.SelectView("<Default>");
	if (olLastView != "")
		olViewer24.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer25;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer25.SetViewerKey("UIF_ARBEITSVERTRAGSARTEN");
	olLastView = olViewer25.SelectView();	// remember the current view
	olViewer25.CreateView("<Default>", olPossibleFilters);
	olViewer25.SelectView("<Default>");
	if (olLastView != "")
		olViewer25.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer26;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer26.SetViewerKey("UIF_BEWERTUNGSFAKTOREN");
	olLastView = olViewer26.SelectView();	// remember the current view
	olViewer26.CreateView("<Default>", olPossibleFilters);
	olViewer26.SelectView("<Default>");
	if (olLastView != "")
		olViewer26.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer27;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	olPossibleFilters.Add("SORFILTER");
	olPossibleFilters.Add("SPFFILTER");
	// Initialize the default view for the gate diagram
	olViewer27.SetViewerKey("UIF_BASISSCHICHTEN");
	olLastView = olViewer27.SelectView();	// remember the current view
	olViewer27.CreateView("<Default>", olPossibleFilters);
	olViewer27.SelectView("<Default>");

	CStringArray olAllFilter;
	olAllFilter.Add(LoadStg(IDS_ALL));
	olViewer27.SetFilter("SORFILTER",olAllFilter);
	olViewer27.SetFilter("SPFFILTER",olAllFilter);

	if (olLastView != "")
		olViewer27.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer28;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer28.SetViewerKey("UIF_FUNKTIONEN");
	olLastView = olViewer28.SelectView();	// remember the current view
	olViewer28.CreateView("<Default>", olPossibleFilters);
	olViewer28.SelectView("<Default>");
	if (olLastView != "")
		olViewer28.SelectView(olLastView);	// restore the previously selected view
	
	CViewer olViewer29;

	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer29.SetViewerKey("UIF_FAHRGEMEINSCHAFTEN");
	olLastView = olViewer29.SelectView();	// remember the current view
	olViewer29.CreateView("<Default>", olPossibleFilters);
	olViewer29.SelectView("<Default>");
	if (olLastView != "")
		olViewer29.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer30;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer30.SetViewerKey("UIF_REDUKTIONEN");
	olLastView = olViewer30.SelectView();	// remember the current view
	olViewer30.CreateView("<Default>", olPossibleFilters);
	olViewer30.SelectView("<Default>");
	if (olLastView != "")
		olViewer30.SelectView(olLastView);	// restore the previously selected view

	
	CViewer olViewer31;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer31.SetViewerKey("UIF_DIENSTEUNDABWESENHEITEN");
	olLastView = olViewer31.SelectView();	// remember the current view
	olViewer31.CreateView("<Default>", olPossibleFilters);
	olViewer31.SelectView("<Default>");
	if (olLastView != "")
		olViewer31.SelectView(olLastView);	// restore the previously selected view
	
	CViewer olViewer32;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	olPossibleFilters.Add("SORFILTER");
	olPossibleFilters.Add("SPFFILTER");
	// Initialize the default view for the gate diagram
	olViewer32.SetViewerKey("UIF_MITARBEITERSTAMM");
	olLastView = olViewer32.SelectView();	// remember the current view
	olViewer32.CreateView("<Default>", olPossibleFilters);
	olViewer32.SelectView("<Default>");

	olAllFilter.Add(LoadStg(IDS_ALL));
	olViewer32.SetFilter("SORFILTER",olAllFilter);
	olViewer32.SetFilter("SPFFILTER",olAllFilter);

	if (olLastView != "")
		olViewer32.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer33;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer33.SetViewerKey("UIF_GERAETEGRUPPEN");
	olLastView = olViewer33.SelectView();	// remember the current view
	olViewer33.CreateView("<Default>", olPossibleFilters);
	olViewer33.SelectView("<Default>");
	if (olLastView != "")
		olViewer33.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer34;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer34.SetViewerKey("UIF_WEGEZEITEN");
	olLastView = olViewer34.SelectView();	// remember the current view
	olViewer34.CreateView("<Default>", olPossibleFilters);
	olViewer34.SelectView("<Default>");
	if (olLastView != "")
		olViewer34.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer35;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer35.SetViewerKey("UIF_ERGABFERTIGUNGSARTEN");
	olLastView = olViewer35.SelectView();	// remember the current view
	olViewer35.CreateView("<Default>", olPossibleFilters);
	olViewer35.SelectView("<Default>");
	if (olLastView != "")
		olViewer35.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer36;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer36.SetViewerKey("UIF_ORGANIZER");
	olLastView = olViewer36.SelectView();	// remember the current view
	olViewer36.CreateView("<Default>", olPossibleFilters);
	olViewer36.SelectView("<Default>");
	if (olLastView != "")
		olViewer36.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer37;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer37.SetViewerKey("UIF_TIMEPARAMETERS");
	olLastView = olViewer37.SelectView();	// remember the current view
	olViewer37.CreateView("<Default>", olPossibleFilters);
	olViewer37.SelectView("<Default>");
	if (olLastView != "")
		olViewer37.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer38;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer38.SetViewerKey("UIF_HOLIDAY");
	olLastView = olViewer38.SelectView();	// remember the current view
	olViewer38.CreateView("<Default>", olPossibleFilters);
	olViewer38.SelectView("<Default>");
	if (olLastView != "")
		olViewer38.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer39;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer39.SetViewerKey("UIF_ARBEITSGRUPPEN");
	olLastView = olViewer39.SelectView();	// remember the current view
	olViewer39.CreateView("<Default>", olPossibleFilters);
	olViewer39.SelectView("<Default>");
	if (olLastView != "")
		olViewer39.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer40;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer40.SetViewerKey("UIF_PLANUNGSGRUPPEN");
	olLastView = olViewer40.SelectView();	// remember the current view
	olViewer40.CreateView("<Default>", olPossibleFilters);
	olViewer40.SelectView("<Default>");
	if (olLastView != "")
		olViewer40.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer41;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer41.SetViewerKey("UIF_AIRPORTWET");
	olLastView = olViewer41.SelectView();	// remember the current view
	olViewer41.CreateView("<Default>", olPossibleFilters);
	olViewer41.SelectView("<Default>");
	if (olLastView != "")
		olViewer41.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer42;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer42.SetViewerKey("UIF_COUNTERCLASS");
	olLastView = olViewer42.SelectView();	// remember the current view
	olViewer42.CreateView("<Default>", olPossibleFilters);
	olViewer42.SelectView("<Default>");
	if (olLastView != "")
		olViewer42.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer43;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer43.SetViewerKey("UIF_VERYIMPPERS");
	olLastView = olViewer43.SelectView();	// remember the current view
	olViewer43.CreateView("<Default>", olPossibleFilters);
	olViewer43.SelectView("<Default>");
	if (olLastView != "")
		olViewer43.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer44;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer44.SetViewerKey("UIF_AIRCRAFTFAM");
	olLastView = olViewer44.SelectView();	// remember the current view
	olViewer44.CreateView("<Default>", olPossibleFilters);
	olViewer44.SelectView("<Default>");
	if (olLastView != "")
		olViewer44.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer45;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer45.SetViewerKey("UIF_ENGINETYPE");
	olLastView = olViewer45.SelectView();	// remember the current view
	olViewer45.CreateView("<Default>", olPossibleFilters); 
	olViewer45.SelectView("<Default>");
	if (olLastView != "")
		olViewer45.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer46;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer46.SetViewerKey("UIF_PARAMETER");
	olLastView = olViewer46.SelectView();	// remember the current view
	olViewer46.CreateView("<Default>", olPossibleFilters);
	olViewer46.SelectView("<Default>");
	if (olLastView != "")
		olViewer46.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer47;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer47.SetViewerKey("UIF_MFM");
	olLastView = olViewer47.SelectView();	// remember the current view
	olViewer47.CreateView("<Default>", olPossibleFilters);
	olViewer47.SelectView("<Default>");
	if (olLastView != "")
		olViewer47.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer48;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer48.SetViewerKey("UIF_STS");
	olLastView = olViewer48.SelectView();	// remember the current view
	olViewer48.CreateView("<Default>", olPossibleFilters);
	olViewer48.SelectView("<Default>");
	if (olLastView != "")
		olViewer48.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer49;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer49.SetViewerKey("UIF_NWH");
	olLastView = olViewer49.SelectView();	// remember the current view
	olViewer49.CreateView("<Default>", olPossibleFilters);
	olViewer49.SelectView("<Default>");
	if (olLastView != "")
		olViewer49.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer50;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer50.SetViewerKey("UIF_COH");
	olLastView = olViewer50.SelectView();	// remember the current view
	olViewer50.CreateView("<Default>", olPossibleFilters);
	olViewer50.SelectView("<Default>");
	if (olLastView != "")
		olViewer50.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer51;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer51.SetViewerKey("UIF_PMX");
	olLastView = olViewer51.SelectView();	// remember the current view
	olViewer51.CreateView("<Default>", olPossibleFilters);
	olViewer51.SelectView("<Default>");
	if (olLastView != "")
		olViewer51.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer52;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer52.SetViewerKey("UIF_WIS");
	olLastView = olViewer52.SelectView();	// remember the current view
	olViewer52.CreateView("<Default>", olPossibleFilters);
	olViewer52.SelectView("<Default>");
	if (olLastView != "")
		olViewer52.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer53;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer53.SetViewerKey("UIF_SRC");
	olLastView = olViewer53.SelectView();	// remember the current view
	olViewer53.CreateView("<Default>", olPossibleFilters); 
	olViewer53.SelectView("<Default>");
	if (olLastView != "")
		olViewer53.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer54;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer54.SetViewerKey("UIF_POOL");
	olLastView = olViewer54.SelectView();	// remember the current view
	olViewer54.CreateView("<Default>", olPossibleFilters);
	olViewer54.SelectView("<Default>");
	if (olLastView != "")
		olViewer54.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer55;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer55.SetViewerKey("UIF_EQUIPMENT");
	olLastView = olViewer55.SelectView();	// remember the current view
	olViewer55.CreateView("<Default>", olPossibleFilters);
	olViewer55.SelectView("<Default>");
	if (olLastView != "")
		olViewer55.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer56;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer56.SetViewerKey("UIF_EQUIPMENTTYPE");
	olLastView = olViewer56.SelectView();	// remember the current view
	olViewer56.CreateView("<Default>", olPossibleFilters);
	olViewer56.SelectView("<Default>");
	if (olLastView != "")
		olViewer56.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer57;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer57.SetViewerKey("UIF_ABSENCEPATTERN");
	olLastView = olViewer57.SelectView();	// remember the current view
	olViewer57.CreateView("<Default>", olPossibleFilters); 
	olViewer57.SelectView("<Default>");
	if (olLastView != "")
		olViewer57.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer58;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer58.SetViewerKey("UIF_DEVICES");
	olLastView = olViewer58.SelectView();	// remember the current view
	olViewer58.CreateView("<Default>", olPossibleFilters); 
	olViewer58.SelectView("<Default>");
	if (olLastView != "")
		olViewer58.SelectView(olLastView);	// restore the previously selected view
	
	CViewer olViewer59;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer59.SetViewerKey("UIF_DISPLAYS");
	olLastView = olViewer59.SelectView();	// remember the current view
	olViewer59.CreateView("<Default>", olPossibleFilters); 
	olViewer59.SelectView("<Default>");
	if (olLastView != "")
		olViewer59.SelectView(olLastView);	// restore the previously selected view
	

	CViewer olViewer60;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the gate diagram
	olViewer60.SetViewerKey("UIF_PAGES");
	olLastView = olViewer60.SelectView();	// remember the current view
	olViewer60.CreateView("<Default>", olPossibleFilters); 
	olViewer60.SelectView("<Default>");
	if (olLastView != "")
		olViewer60.SelectView(olLastView);	// restore the previously selected view
	//PRF 8378
	CViewer olViewer61;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	// Initialize the default view for the Change Reason Code diagram
	olViewer61.SetViewerKey("UIF_CHANGEREASON_CODE");
	olLastView = olViewer61.SelectView();	// remember the current view
	olViewer61.CreateView("<Default>", olPossibleFilters); 
	olViewer61.SelectView("<Default>");
	if (olLastView != "")
		olViewer61.SelectView(olLastView);	// restore the previously selected view


	CViewer olViewer62;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	olViewer62.SetViewerKey("UIF_AIRPORT_ARRIVAL_DESK");
	olLastView = olViewer62.SelectView();	// remember the current view
	olViewer62.CreateView("<Default>", olPossibleFilters); 
	olViewer62.SelectView("<Default>");
	if (olLastView != "")
		olViewer62.SelectView(olLastView);	// restore the previously selected view


	CViewer olViewer63;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	olViewer63.SetViewerKey("UIF_PDA_EQUIPMENT");
	olLastView = olViewer63.SelectView();	// remember the current view
	olViewer63.CreateView("<Default>", olPossibleFilters); 
	olViewer63.SelectView("<Default>");
	if (olLastView != "")
		olViewer63.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer64;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	olViewer64.SetViewerKey("UIF_DISPLAY_SCHEDULE");
	olLastView = olViewer64.SelectView();	// remember the current view
	olViewer64.CreateView("<Default>", olPossibleFilters); 
	olViewer64.SelectView("<Default>");
	if (olLastView != "")
		olViewer64.SelectView(olLastView);	// restore the previously selected view

	CViewer olViewer65;
	// Read the default value from the database in the server
	olPossibleFilters.RemoveAll();
	olPossibleFilters.Add("UNIFILTER");
	olViewer65.SetViewerKey("UIF_LOGO_NAMES");
	olLastView = olViewer65.SelectView();	// remember the current view
	olViewer65.CreateView("<Default>", olPossibleFilters); 
	olViewer65.SelectView("<Default>");
	if (olLastView != "")
		olViewer65.SelectView(olLastView);	// restore the previously selected view

	if(ogBasicData.IsChutesDisplayEnabled() == true)
	{
		CViewer olViewer66;
		// Read the default value from the database in the server
		olPossibleFilters.RemoveAll();
		olPossibleFilters.Add("UNIFILTER");
		olViewer66.SetViewerKey("UIF_CHUTES");
		olLastView = olViewer66.SelectView();	// remember the current view
		olViewer66.CreateView("<Default>", olPossibleFilters); 
		olViewer66.SelectView("<Default>");
		if (olLastView != "")
			olViewer66.SelectView(olLastView);	// restore the previously selected view
	}
}

/////////////////////////////////////////////////////////////////////////////
// CBDPSUIFApp

BEGIN_MESSAGE_MAP(CBDPSUIFApp, CWinApp)
	//{{AFX_MSG_MAP(CBDPSUIFApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, OnSpecialHelp)
END_MESSAGE_MAP()
//	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
//	ON_COMMAND(ID_HELP, OnSpecialHelp)

/////////////////////////////////////////////////////////////////////////////
// CBDPSUIFApp construction

CBDPSUIFApp::CBDPSUIFApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


CBDPSUIFApp::~CBDPSUIFApp()
{
	DeleteBrushes();
	//ExitProcess(0);
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CBDPSUIFApp object

CBDPSUIFApp theApp;


/////////////////////////////////////////////////////////////////////////////
// CBDPSUIFApp initialization

BOOL CBDPSUIFApp::InitInstance()
{
	
	

	// Standard initialization
	char pclViewEditFilter[64];
	char pclViewEditSort[64];
	char pclListSeparator[64];
	char pclUnicode[64];
	char pclTmp[64];
	char pclTmpString[64];


	ogNoData  = LoadStg(ST_NODATA);
	ogNotFormat = LoadStg(ST_NOTFORMAT);


	//*** 09.11.99 SHA ***
	//*** FOR THE STINGRAY GRID ***
	GXInit(); 
	
	// Initialize OLE libraries
	if (!AfxOleInit())
	{
//		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}

	AfxEnableControlContainer();


	char pclConfigPath[142];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));


	char pclUseBcProxy[256];
	GetPrivateProfileString("GLOBAL", "BCEVENTTYPE", "BCSERV",pclUseBcProxy, sizeof pclUseBcProxy, pclConfigPath);

	char tmpIsPrm[124];
	
	GetPrivateProfileString(pcgAppName, "ISPRM",  "NO",tmpIsPrm, sizeof tmpIsPrm, pclConfigPath);
	if (!strcmp(tmpIsPrm,"YES")) {
		bgIsPrm = true;
	}
	else {
 		bgIsPrm = false;
	}

	GetPrivateProfileString(pcgAppName, "CXXF_DISP",  "YES",pclTmp, sizeof pclTmp, pclConfigPath);
	if (!strcmp(pclTmp,"YES")) {
		bgCxxfDisp = true;
	}
	else {
 		bgCxxfDisp = false;
	}
   

	//MWO/RRO 25.03.03
	if (stricmp(pclUseBcProxy,"BCPROXY") == 0) 
	{
		ogBcHandle.UseBcProxy();
		ogBcHandle.StopBc();
	}
	else if (stricmp(pclUseBcProxy,"BCCOM") == 0)
	{
		ogBcHandle.UseBcCom();
		ogBcHandle.StopBc();
	}


	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	// Parse the command line to see if launched as OLE server
	if (RunEmbedded() || RunAutomated())
	{
		// Register all OLE server (factories) as running.  This enables the
		//  OLE libraries to create objects from other applications.
		COleTemplateServer::RegisterAll();
		return TRUE;
	}
	else
	{
		// When a server application is launched stand-alone, it is a good idea
		//  to update the system registry in case it has been damaged.
		COleObjectFactory::UpdateRegistryAll();
	}


	//Parameter�bergabe///////////////////////////////////////////////////
	CString olCmdLine =  LPCTSTR(m_lpCmdLine);
	if (olCmdLine.Find("REGSERVER") < 0)
	{
		ogCmdLineStghArray.RemoveAll();
		//m_lpCmdLine => "AppName,UserID,Password,Tablelist"
		if(olCmdLine.GetLength() == 0)
		{
			//dafault f�r BDPS-UIF
			ogCmdLineStghArray.Add(ogAppName);
			ogCmdLineStghArray.Add("");
			ogCmdLineStghArray.Add("");
			//Hier alle Tabellen eintragen 
			ogCmdLineStghArray.Add("ALT-ACT-ACR-APT-RWY-TWY-PST-GAT-CIC-BLT-EXT-DEN-MVT-NAT-HAG-WRO-HTY-STY-FID-STR-SPH-SEA-GHS/*-GEG*/-PER-PEF-GRM-GRN-ORG-PFC-COT-ASF-BSS-BSD-ODA-TEA-STF-HTC-WAY-PRC-CHT-TIP-BLK-HOL-WGP-SWG-PGP-AWI-CCC-VIP-AFM-ENT-SRC-PAR-POL-MFM-STS-NWH-COH-PMX-WIS-EQT-EQU-MAW-DEV-DSP-PAG-CRC-PDA-AAD-DSC-FLG-CHU_CTN");//Alle 

		}
		else if (olCmdLine.Find("Resource:") > -1)	// called from FIPS
		{
			if(ExtractItemList(olCmdLine,&ogCmdLineStghArray) != 4)
			{
				MessageBox(NULL,LoadStg(IDS_STRING363),"BDPS-UIF",MB_ICONERROR);
				return FALSE;
			}
			else
			{
				CString olPara4 = ogCmdLineStghArray.GetAt(3);
				ogCmdLineStghArray.SetAt(3,"ALT-ACT-ACR-APT-RWY-TWY-PST-GAT-CIC-BLT-EXT-DEN-MVT-NAT-HAG-WRO-HTY-STY-FID-STR-SPH-SEA-GHS/*-GEG*/-PER-PEF-GRM-GRN-ORG-PFC-COT-ASF-BSS-BSD-ODA-TEA-STF-HTC-WAY-PRC-CHT-TIP-BLK-HOL-WGP-SWG-PGP-AWI-CCC-VIP-AFM-ENT-SRC-PAR-POL-MFM-STS-NWH-COH-PMX-WIS-EQT-EQU-MAW-DEV-DSP-PAG-CRC-DSC-FLG-CHU-CTN");  
				ogCmdLineStghArray.Add(olPara4);
			}
					
		}
		else										// called with old format		
		{
			if(ExtractItemList(olCmdLine,&ogCmdLineStghArray) != 4)
			{
				MessageBox(NULL,LoadStg(IDS_STRING363),"BDPS-UIF",MB_ICONERROR);
				return FALSE;
			}
			else
			{
				ogCmdLineStghArray.SetAt(3,"ALT-ACT-ACR-APT-RWY-TWY-PST-GAT-CIC-BLT-EXT-DEN-MVT-NAT-HAG-WRO-HTY-STY-FID-STR-SPH-SEA-GHS/*-GEG*/-PER-PEF-GRM-GRN-ORG-PFC-COT-ASF-BSS-BSD-ODA-TEA-STF-HTC-WAY-PRC-CHT-TIP-BLK-HOL-WGP-SWG-PGP-AWI-CCC-VIP-AFM-ENT-SRC-PAR-POL-MFM-STS-NWH-COH-PMX-WIS-EQT-EQU-MAW-DEV-DSP-PAG-CRC-DSC-FLG-CHU-CTN");  
			}
		}
		//Parameter�bergabe Ende////////////////////////////////////////////////

		if (ogCommHandler.Initialize() != true)  // connection error?
		{
			AfxMessageBox(CString("CedaCom:\n") + ogCommHandler.LastError());
			return FALSE;
		}

  		InitFont();
		CreateBrushes();


		// INIT Tablenames and Homeairport
		char pclConfigPath[256];

		if (getenv("CEDA") == NULL)
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		else
			strcpy(pclConfigPath, getenv("CEDA"));


		GetPrivateProfileString("GLOBAL", "HOMEAIRPORT", "ZRH", pcgHome, sizeof pcgHome, pclConfigPath);
		GetPrivateProfileString("GLOBAL", "TABLEEXTENSION", "TAB", pcgTableExt, sizeof pcgTableExt, pclConfigPath);
		GetPrivateProfileString("GLOBAL", "MULTI_AIRPORT", "FALSE",pclTmpString,sizeof pclTmpString, pclConfigPath );
		GetPrivateProfileString(pcgAppName, "UMODE", "E44SWP", pcgUMode, sizeof pcgUMode, pclConfigPath);
		GetPrivateProfileString(ogAppName, "EDIT_VIEW_FILTER", "FALSE", pclViewEditFilter, sizeof pclViewEditFilter, pclConfigPath);
		GetPrivateProfileString(ogAppName, "EDIT_VIEW_SORT"  , "FALSE", pclViewEditSort, sizeof pclViewEditSort, pclConfigPath);
		GetPrivateProfileString(ogAppName, "ListSeparator", ",",pclListSeparator, sizeof pclListSeparator, pclConfigPath);


		GetPrivateProfileString(ogAppName, "UNICODE", "NO", pclUnicode, sizeof pclUnicode, pclConfigPath);
		if(strcmp(pclUnicode, "YES") == 0)
			bgUnicode = true;
		else
			bgUnicode = false;
		ogListSeparator = pclListSeparator;

		

		 //  To set to accept one character as country code // 

		char tmpCcode[10];

		GetPrivateProfileString("BDPS-UIF", "ISCCODE",  "NO",tmpCcode, sizeof tmpCcode, pclConfigPath);

		if (!strcmp(tmpCcode,"YES")) {
			bgCcode = true;
			}
		else {
 			bgCcode = false;
		}
    
	// To set to accept one character as country code // 

		//setting for multi airports
		if (strcmp(pclTmpString,"TRUE")==0)
			bgUseMultiAirport = true;
		else
			bgUseMultiAirport = false;

		
		//BDPS-UIF Modus Varianten mit UMODE
		//UMODE = E44SWP	f�r SwissPort
		//UMODE = D44FAG	f�r FAG

		strcpy(CCSCedaData::pcmTableExt,    pcgTableExt);
		strcpy(CCSCedaData::pcmApplName,    pcgAppName);
		strcpy(CCSCedaData::pcmHomeAirport, pcgHome);

		ogHome     = pcgHome;
		ogTableExt = pcgTableExt;
		ogUIFModus = pcgUMode;

		if(strcmp(pclViewEditFilter, "TRUE") == 0)
			bgViewEditFilter = true;
		else
			bgViewEditFilter = false;

		if(strcmp(pclViewEditSort, "TRUE") == 0)
			bgViewEditSort = true;
		else
			bgViewEditSort = false;

	


	
		//INIT END/////////////////////////

			#if	0
		CLoginDialog olLoginDlg(pcgTableExt,ogAppName,ogCommHandler.pcmReqId);
		if(ogCmdLineStghArray.GetAt(0) != ogAppName)
		{
			if(olLoginDlg.Login(ogCmdLineStghArray.GetAt(1),ogCmdLineStghArray.GetAt(2)) == true)
			{
				
				//Added for multiairport checking
				if(bgUseMultiAirport)
				{
						CString strSelectedExt= olLoginCtrl.GetTableExt();
						strSelectedExt.TrimLeft();
						strSelectedExt.TrimRight();
						if(strSelectedExt.GetLength()>0)
						{
								strcpy(pcgTableExt,olLoginDlg.GetTableExt());
								ogTableExt = pcgTableExt;
								strcpy(CCSCedaData::pcmTableExt,    pcgTableExt);
						}
				}

				//*** 10.09.99 SHA ***
				//*** F�R UFISCLASSES ***
				ogCfgData.ReadUfisCedaConfig();
				ogCfgData.ReadCfgData();

				LoadParameters();
				InitBDPSTableDefaultView();

				ogBCD.SetTableExtension(ogTableExt);
				ogBCD.SetHomeAirport(ogHome);

				ogBCD.SetObject("ACI");

				ogBCD.SetObject("DEL");
				ogBCD.SetObject("DLG");
				ogBCD.SetObject("DRA");
				ogBCD.SetObject("DRD");
				ogBCD.SetObject("DRG");
				ogBCD.SetObject("DRR");
				ogBCD.SetObject("DSR");
				ogBCD.SetObject("DSR");
				ogBCD.SetObject("DRS");
				ogBCD.SetObject("DRW");
				ogBCD.SetObject("EQU");
				ogBCD.SetObject("GPL");

				ogBCD.SetObject("GSP");

				ogBCD.SetObject("JOB");
				ogBCD.SetObject("MAW");

				ogBCD.SetObject("MSD");

				ogBCD.SetObject("POA");
				ogBCD.SetObject("REL");
				ogBCD.SetObject("RPQ");
				ogBCD.SetObject("RPF");
				ogBCD.SetObject("SCO");
				ogBCD.SetObject("SDA");
				ogBCD.SetObject("SDT");
				ogBCD.SetObject("SEF");
				ogBCD.SetObject("SOR");
				ogBCD.SetObject("SPE");
				ogBCD.SetObject("SPF");
				ogBCD.SetObject("SWG");

				//Added by Christine
				if(ogBasicData.IsContinentAvailable())
				{
					ogBCD.SetObject("CNT",URNO,CODE,DESP,CDAT,USEC,LSTU,USEU,VAFR,VATO,HOPO");
					CString clWhere;
					clWhere.Format("WHERE STAT<> '%s'","D");
					ogBCD.Read("CNT",clWhere);
				}


				if (!ogBasicData.SetLocalDiff())
				{
					if (AfxMessageBox(LoadStg(IDS_STRING988),MB_ICONSTOP|MB_YESNO) == IDNO)
					{
						AfxAbort();
					}

				}


				CStammdaten olStammdatenDlg;
				olStammdatenDlg.DoModal();
			}
		}
		else
		{
			if( olLoginDlg.DoModal() != IDCANCEL )
			{
				//Added for multiairport checking
				if(bgUseMultiAirport)
				{
					CString strSelectedExt= olLoginCtrl.GetTableExt();
					strSelectedExt.TrimLeft();
					strSelectedExt.TrimRight();
					if(strSelectedExt.GetLength()>0)
					{
						strcpy(pcgTableExt,olLoginDlg.GetTableExt());
						ogTableExt = pcgTableExt;
						strcpy(CCSCedaData::pcmTableExt,    pcgTableExt);
					}
				}
							
				//*** 10.09.99 SHA ***
				//*** F�R UFISCLASSES ***
				ogCfgData.ReadUfisCedaConfig();

				ogCfgData.ReadCfgData();
				
				LoadParameters();
				InitBDPSTableDefaultView();
				int ilStartApp = IDOK;
				if(ogPrivList.GetStat("InitModu") == '1')
				{
					RegisterDlg olRegisterDlg;
					ilStartApp = olRegisterDlg.DoModal();
				}
				if(ilStartApp == IDOK)
				{
					//*** 07.09.99 SHA ***
					//*** MOVED FROM POS. (1) ***
					// for referenz check in CheckReferenz
					ogBCD.SetTableExtension(ogTableExt);
					ogBCD.SetHomeAirport(ogHome);

					ogBCD.SetObject("ACI");
					ogBCD.SetObject("DEL");
					ogBCD.SetObject("DLG");
					ogBCD.SetObject("DRA");
					ogBCD.SetObject("DRD");
					ogBCD.SetObject("DRG");
					ogBCD.SetObject("DRR");
					ogBCD.SetObject("DSR");
					ogBCD.SetObject("DRS");
					ogBCD.SetObject("DRW");
					ogBCD.SetObject("EQU");
					ogBCD.SetObject("GPL");
					ogBCD.SetObject("GSP");
					ogBCD.SetObject("JOB");
					ogBCD.SetObject("MAW");
					ogBCD.SetObject("MSD");
					ogBCD.SetObject("POA");
					ogBCD.SetObject("REL");
					ogBCD.SetObject("RPQ");
					ogBCD.SetObject("RPF");
					ogBCD.SetObject("SCO");
					ogBCD.SetObject("SDA");
					ogBCD.SetObject("SDT");
					ogBCD.SetObject("SEF");
					ogBCD.SetObject("SOR");
					ogBCD.SetObject("SPE");
					ogBCD.SetObject("SPF");
					ogBCD.SetObject("SWG");

					//Added by Christine
					if(ogBasicData.IsContinentAvailable())
					{
						//ogBCD.SetObject("CTN");
						ogBCD.SetObject("CNT","URNO,CODE,DESP,CDAT,USEC,LSTU,USEU,VAFR,VATO,HOPO");
						CString clWhere;
						clWhere.Format("WHERE STAT<> '%s'","D");
						ogBCD.Read("CNT",clWhere);
					}

					if (!ogBasicData.SetLocalDiff())
					{
						if (AfxMessageBox(LoadStg(IDS_STRING988),MB_ICONSTOP|MB_YESNO) == IDNO)
						{
							AfxAbort();
						}

					}

					CStammdaten olStammdatenDlg;
					olStammdatenDlg.DoModal();	
				}
			}
		}
	#else
		CDialog olDummyDlg;	
		olDummyDlg.Create(IDD_STARTUP,NULL);
		olDummyDlg.ShowWindow(SW_HIDE);
		CAatLogin olLoginCtrl;
		olLoginCtrl.Create(NULL,WS_CHILD|WS_DISABLED,CRect(0,0,100,100),&olDummyDlg,4711);
		olLoginCtrl.SetInfoButtonVisible(TRUE);
		
		olLoginCtrl.SetRegisterApplicationString(ogInitModuData.GetInitModuTxt());

		if(ogCmdLineStghArray.GetAt(0) != ogAppName)
		{
			if (olLoginCtrl.DoLoginSilentMode(ogCmdLineStghArray.GetAt(1),ogCmdLineStghArray.GetAt(2)) != "OK")
			{
				olDummyDlg.DestroyWindow();
				return FALSE;
			}
		}
		else
		{
			if (olLoginCtrl.ShowLoginDialog() != "OK")
			{
				olDummyDlg.DestroyWindow();
				return FALSE;
			}

		}

		olDummyDlg.MoveWindow(-100000,-100000,0,0);
		olDummyDlg.ShowWindow(SW_SHOWNORMAL);

		strcpy(cgUserName,olLoginCtrl.GetUserName_());
		strcpy(cgUserPWD,olLoginCtrl.GetUserPassword());

		ogBasicData.omUserID = olLoginCtrl.GetUserName_();
		ogCommHandler.SetUser(olLoginCtrl.GetUserName_());
		strcpy(CCSCedaData::pcmUser, olLoginCtrl.GetUserName_());
		strcpy(CCSCedaData::pcmReqId, ogCommHandler.pcmReqId);

		ogPrivList.Add(olLoginCtrl.GetPrivilegList());
		ogUsername = olLoginCtrl.GetUserName_();
		ogPassword = olLoginCtrl.GetUserPassword();
		ogWks = olLoginCtrl.GetWorkstationName();

		if (ogBasicData.IsPrmHandlingAgentEnabled())
		{
			ogPrmPrivList.Login(ogHome,ogUsername,ogPassword,"GlobalPar",ogWks);
		}

		if(bgUseMultiAirport)
		{
			CString strSelectedExt= olLoginCtrl.GetTableExt();
			strSelectedExt.TrimLeft();
			strSelectedExt.TrimRight();
			if(strSelectedExt.GetLength()>0)
			{
				strcpy(pcgTableExt,olLoginCtrl.GetTableExt());
				ogTableExt = pcgTableExt;
				strcpy(CCSCedaData::pcmTableExt,    pcgTableExt);
			}
		}


		ogCfgData.ReadUfisCedaConfig();

		ogCfgData.ReadCfgData();

		LoadParameters();
		InitBDPSTableDefaultView();

		ogBCD.SetTableExtension(ogTableExt);
		ogBCD.SetHomeAirport(ogHome);

		ogBCD.SetObject("ACI");

		ogBCD.SetObject("DEL");
		ogBCD.SetObject("DLG");
		ogBCD.SetObject("DRA");
		ogBCD.SetObject("DRD");
		ogBCD.SetObject("DRG");
		ogBCD.SetObject("DRR");
		ogBCD.SetObject("DSR");
		ogBCD.SetObject("DSR");
		ogBCD.SetObject("DRS");
		ogBCD.SetObject("DRW");
		ogBCD.SetObject("EQU");
		ogBCD.SetObject("GPL");

		ogBCD.SetObject("GSP");
		ogBCD.SetObject("JOB");
		ogBCD.SetObject("MAW");

		ogBCD.SetObject("MSD");

		ogBCD.SetObject("POA");
		ogBCD.SetObject("REL");
		ogBCD.SetObject("RPQ");
		ogBCD.SetObject("RPF");
		ogBCD.SetObject("SCO");
		ogBCD.SetObject("SDA");
		ogBCD.SetObject("SDT");
		ogBCD.SetObject("SEF");
		ogBCD.SetObject("SOR");
		ogBCD.SetObject("SPE");
		ogBCD.SetObject("SPF");
		ogBCD.SetObject("SWG");
		
		//Added by Christine
		if(ogBasicData.IsContinentAvailable())
		{
			ogBCD.SetObject("CNT","URNO,CODE,DESP,CDAT,USEC,LSTU,USEU,VAFR,VATO,HOPO");
			CString clWhere;
			clWhere.Format("WHERE STAT<> '%s'","D");
			ogBCD.Read("CNT",clWhere);
		}


		if (!ogBasicData.SetLocalDiff())
		{
			if (AfxMessageBox(LoadStg(IDS_STRING988),MB_ICONSTOP|MB_YESNO) == IDNO)
			{
				AfxAbort();
			}
		}
		

		if (ogBasicData.IsPrmHandlingAgentEnabled())
		{
			ogPrmConfig.Initialize();
		}

		// check GATPOS allocations only once
		this->CheckGatPosAllocations();

		CStammdaten olStammdatenDlg(&olDummyDlg);
		m_pMainWnd = &olStammdatenDlg;

		olStammdatenDlg.DoModal();

		olDummyDlg.DestroyWindow();

	#endif
	}

		bgDEN_DECS_Exist = ogBasicData.DoesFieldExist("DEN", "DECS" );
		//bgDEN_DECS_Exist = false;

		ogBcHandle.SetCloMessage(LoadStg(IDS_STRING709));

		ogBcHandle.AddTableCommand(CString("ALT") + ogTableExt, CString("IRT"), BC_ALT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("ALT") + ogTableExt, CString("URT"), BC_ALT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("ALT") + ogTableExt, CString("DRT"), BC_ALT_DELETE, false);

		ogBcHandle.AddTableCommand(CString("ACT") + ogTableExt, CString("IRT"), BC_ACT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("ACT") + ogTableExt, CString("URT"), BC_ACT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("ACT") + ogTableExt, CString("DRT"), BC_ACT_DELETE, false);

		ogBcHandle.AddTableCommand(CString("ACR") + ogTableExt, CString("IRT"), BC_ACR_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("ACR") + ogTableExt, CString("URT"), BC_ACR_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("ACR") + ogTableExt, CString("DRT"), BC_ACR_DELETE, false);

		ogBcHandle.AddTableCommand(CString("APT") + ogTableExt, CString("IRT"), BC_APT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("APT") + ogTableExt, CString("URT"), BC_APT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("APT") + ogTableExt, CString("DRT"), BC_APT_DELETE, false);

		ogBcHandle.AddTableCommand(CString("RWY") + ogTableExt, CString("IRT"), BC_RWY_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("RWY") + ogTableExt, CString("URT"), BC_RWY_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("RWY") + ogTableExt, CString("DRT"), BC_RWY_DELETE, false);

		ogBcHandle.AddTableCommand(CString("TWY") + ogTableExt, CString("IRT"), BC_TWY_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("TWY") + ogTableExt, CString("URT"), BC_TWY_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("TWY") + ogTableExt, CString("DRT"), BC_TWY_DELETE, false);

		ogBcHandle.AddTableCommand(CString("PST") + ogTableExt, CString("IRT"), BC_PST_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("PST") + ogTableExt, CString("URT"), BC_PST_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("PST") + ogTableExt, CString("DRT"), BC_PST_DELETE, false);

		ogBcHandle.AddTableCommand(CString("GAT") + ogTableExt, CString("IRT"), BC_GAT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("GAT") + ogTableExt, CString("URT"), BC_GAT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("GAT") + ogTableExt, CString("DRT"), BC_GAT_DELETE, false);

		ogBcHandle.AddTableCommand(CString("CIC") + ogTableExt, CString("IRT"), BC_CIC_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("CIC") + ogTableExt, CString("URT"), BC_CIC_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("CIC") + ogTableExt, CString("DRT"), BC_CIC_DELETE, false);

		ogBcHandle.AddTableCommand(CString("BLT") + ogTableExt, CString("IRT"), BC_BLT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("BLT") + ogTableExt, CString("URT"), BC_BLT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("BLT") + ogTableExt, CString("DRT"), BC_BLT_DELETE, false);

		ogBcHandle.AddTableCommand(CString("EXT") + ogTableExt, CString("IRT"), BC_EXT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("EXT") + ogTableExt, CString("URT"), BC_EXT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("EXT") + ogTableExt, CString("DRT"), BC_EXT_DELETE, false);

		ogBcHandle.AddTableCommand(CString("DEN") + ogTableExt, CString("IRT"), BC_DEN_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("DEN") + ogTableExt, CString("URT"), BC_DEN_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("DEN") + ogTableExt, CString("DRT"), BC_DEN_DELETE, false);

		ogBcHandle.AddTableCommand(CString("MVT") + ogTableExt, CString("IRT"), BC_MVT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("MVT") + ogTableExt, CString("URT"), BC_MVT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("MVT") + ogTableExt, CString("DRT"), BC_MVT_DELETE, false);

		ogBcHandle.AddTableCommand(CString("NAT") + ogTableExt, CString("IRT"), BC_NAT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("NAT") + ogTableExt, CString("URT"), BC_NAT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("NAT") + ogTableExt, CString("DRT"), BC_NAT_DELETE, false);

		ogBcHandle.AddTableCommand(CString("HAG") + ogTableExt, CString("IRT"), BC_HAG_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("HAG") + ogTableExt, CString("URT"), BC_HAG_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("HAG") + ogTableExt, CString("DRT"), BC_HAG_DELETE, false);

		ogBcHandle.AddTableCommand(CString("WRO") + ogTableExt, CString("IRT"), BC_WRO_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("WRO") + ogTableExt, CString("URT"), BC_WRO_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("WRO") + ogTableExt, CString("DRT"), BC_WRO_DELETE, false);

		ogBcHandle.AddTableCommand(CString("HTY") + ogTableExt, CString("IRT"), BC_HTY_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("HTY") + ogTableExt, CString("URT"), BC_HTY_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("HTY") + ogTableExt, CString("DRT"), BC_HTY_DELETE, false);

		ogBcHandle.AddTableCommand(CString("STY") + ogTableExt, CString("IRT"), BC_STY_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("STY") + ogTableExt, CString("URT"), BC_STY_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("STY") + ogTableExt, CString("DRT"), BC_STY_DELETE, false);

		ogBcHandle.AddTableCommand(CString("FID") + ogTableExt, CString("IRT"), BC_FID_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("FID") + ogTableExt, CString("URT"), BC_FID_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("FID") + ogTableExt, CString("DRT"), BC_FID_DELETE, false);

		ogBcHandle.AddTableCommand(CString("SEA") + ogTableExt, CString("IRT"), BC_SEA_NEW, false);
		ogBcHandle.AddTableCommand(CString("SEA") + ogTableExt, CString("URT"), BC_SEA_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("SEA") + ogTableExt, CString("DRT"), BC_SEA_DELETE, false);

		ogBcHandle.AddTableCommand(CString("STR") + ogTableExt, CString("IRT"), BC_STR_NEW, false);
		ogBcHandle.AddTableCommand(CString("STR") + ogTableExt, CString("URT"), BC_STR_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("STR") + ogTableExt, CString("DRT"), BC_STR_DELETE, false);

		ogBcHandle.AddTableCommand(CString("SPH") + ogTableExt, CString("IRT"), BC_SPH_NEW, false);
		ogBcHandle.AddTableCommand(CString("SPH") + ogTableExt, CString("URT"), BC_SPH_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("SPH") + ogTableExt, CString("DRT"), BC_SPH_DELETE, false);
		
		ogBcHandle.AddTableCommand(CString("GHS") + ogTableExt, CString("IRT"), BC_GHS_NEW, false);
		ogBcHandle.AddTableCommand(CString("GHS") + ogTableExt, CString("URT"), BC_GHS_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("GHS") + ogTableExt, CString("DRT"), BC_GHS_DELETE, false);

		//ogBcHandle.AddTableCommand(CString("GEG") + ogTableExt, CString("IRT"), BC_GEG_NEW, false);
		//ogBcHandle.AddTableCommand(CString("GEG") + ogTableExt, CString("URT"), BC_GEG_CHANGE, false);
		//ogBcHandle.AddTableCommand(CString("GEG") + ogTableExt, CString("DRT"), BC_GEG_DELETE, false);

		ogBcHandle.AddTableCommand(CString("PER") + ogTableExt, CString("IRT"), BC_PER_NEW, false);
		ogBcHandle.AddTableCommand(CString("PER") + ogTableExt, CString("URT"), BC_PER_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("PER") + ogTableExt, CString("DRT"), BC_PER_DELETE, false);
		
		ogBcHandle.AddTableCommand(CString("PEF") + ogTableExt, CString("IRT"), BC_PEF_NEW, false);
		ogBcHandle.AddTableCommand(CString("PEF") + ogTableExt, CString("URT"), BC_PEF_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("PEF") + ogTableExt, CString("DRT"), BC_PEF_DELETE, false);
		
		ogBcHandle.AddTableCommand(CString("GRM") + ogTableExt, CString("IRT"), BC_GRM_NEW, false);
		ogBcHandle.AddTableCommand(CString("GRM") + ogTableExt, CString("URT"), BC_GRM_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("GRM") + ogTableExt, CString("DRT"), BC_GRM_DELETE, false);
		
		ogBcHandle.AddTableCommand(CString("GRN") + ogTableExt, CString("IRT"), BC_GRN_NEW, false);
		ogBcHandle.AddTableCommand(CString("GRN") + ogTableExt, CString("URT"), BC_GRN_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("GRN") + ogTableExt, CString("DRT"), BC_GRN_DELETE, false);

		ogBcHandle.AddTableCommand(CString("ORG") + ogTableExt, CString("IRT"), BC_ORG_NEW, false);
		ogBcHandle.AddTableCommand(CString("ORG") + ogTableExt, CString("URT"), BC_ORG_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("ORG") + ogTableExt, CString("DRT"), BC_ORG_DELETE, false);

		ogBcHandle.AddTableCommand(CString("PFC") + ogTableExt, CString("IRT"), BC_PFC_NEW, false);
		ogBcHandle.AddTableCommand(CString("PFC") + ogTableExt, CString("URT"), BC_PFC_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("PFC") + ogTableExt, CString("DRT"), BC_PFC_DELETE, false);

		ogBcHandle.AddTableCommand(CString("COT") + ogTableExt, CString("IRT"), BC_COT_NEW, false);
		ogBcHandle.AddTableCommand(CString("COT") + ogTableExt, CString("URT"), BC_COT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("COT") + ogTableExt, CString("DRT"), BC_COT_DELETE, false);
		
		ogBcHandle.AddTableCommand(CString("ASF") + ogTableExt, CString("IRT"), BC_ASF_NEW, false);
		ogBcHandle.AddTableCommand(CString("ASF") + ogTableExt, CString("URT"), BC_ASF_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("ASF") + ogTableExt, CString("DRT"), BC_ASF_DELETE, false);

		ogBcHandle.AddTableCommand(CString("BSS") + ogTableExt, CString("IRT"), BC_BSS_NEW, false);
		ogBcHandle.AddTableCommand(CString("BSS") + ogTableExt, CString("URT"), BC_BSS_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("BSS") + ogTableExt, CString("DRT"), BC_BSS_DELETE, false);

		ogBcHandle.AddTableCommand(CString("BSD") + ogTableExt, CString("IRT"), BC_BSD_NEW, false);
		ogBcHandle.AddTableCommand(CString("BSD") + ogTableExt, CString("URT"), BC_BSD_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("BSD") + ogTableExt, CString("DRT"), BC_BSD_DELETE, false);
		
		ogBcHandle.AddTableCommand(CString("ODA") + ogTableExt, CString("IRT"), BC_ODA_NEW, false);
		ogBcHandle.AddTableCommand(CString("ODA") + ogTableExt, CString("URT"), BC_ODA_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("ODA") + ogTableExt, CString("DRT"), BC_ODA_DELETE, false);
		
		ogBcHandle.AddTableCommand(CString("TEA") + ogTableExt, CString("IRT"), BC_TEA_NEW, false);
		ogBcHandle.AddTableCommand(CString("TEA") + ogTableExt, CString("URT"), BC_TEA_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("TEA") + ogTableExt, CString("DRT"), BC_TEA_DELETE, false);
		
		ogBcHandle.AddTableCommand(CString("STF") + ogTableExt, CString("IRT"), BC_STF_NEW, false);
		ogBcHandle.AddTableCommand(CString("STF") + ogTableExt, CString("URT"), BC_STF_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("STF") + ogTableExt, CString("DRT"), BC_STF_DELETE, false);

		ogBcHandle.AddTableCommand(CString("WAY") + ogTableExt, CString("IRT"), BC_WAY_NEW, false);
		ogBcHandle.AddTableCommand(CString("WAY") + ogTableExt, CString("URT"), BC_WAY_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("WAY") + ogTableExt, CString("DRT"), BC_WAY_DELETE, false);

		ogBcHandle.AddTableCommand(CString("PRC") + ogTableExt, CString("IRT"), BC_PRC_NEW, false);
		ogBcHandle.AddTableCommand(CString("PRC") + ogTableExt, CString("URT"), BC_PRC_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("PRC") + ogTableExt, CString("DRT"), BC_PRC_DELETE, false);

		ogBcHandle.AddTableCommand(CString("CHT") + ogTableExt, CString("IRT"), BC_CHT_NEW, false);
		ogBcHandle.AddTableCommand(CString("CHT") + ogTableExt, CString("URT"), BC_CHT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("CHT") + ogTableExt, CString("DRT"), BC_CHT_DELETE, false);

		ogBcHandle.AddTableCommand(CString("TIP") + ogTableExt, CString("IRT"), BC_TIP_NEW, false);
		ogBcHandle.AddTableCommand(CString("TIP") + ogTableExt, CString("URT"), BC_TIP_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("TIP") + ogTableExt, CString("DRT"), BC_TIP_DELETE, false);

		ogBcHandle.AddTableCommand(CString("BLK") + ogTableExt, CString("IRT"), BC_BLK_NEW, false);
		ogBcHandle.AddTableCommand(CString("BLK") + ogTableExt, CString("URT"), BC_BLK_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("BLK") + ogTableExt, CString("DRT"), BC_BLK_DELETE, false);

		ogBcHandle.AddTableCommand(CString("HOL") + ogTableExt, CString("IRT"), BC_HOL_NEW, false);
		ogBcHandle.AddTableCommand(CString("HOL") + ogTableExt, CString("URT"), BC_HOL_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("HOL") + ogTableExt, CString("DRT"), BC_HOL_DELETE, false);

		ogBcHandle.AddTableCommand(CString("WGP") + ogTableExt, CString("IRT"), BC_WGP_NEW, false);
		ogBcHandle.AddTableCommand(CString("WGP") + ogTableExt, CString("URT"), BC_WGP_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("WGP") + ogTableExt, CString("DRT"), BC_WGP_DELETE, false);

		ogBcHandle.AddTableCommand(CString("PAC") + ogTableExt, CString("IRT"), BC_PAC_NEW, false);
		ogBcHandle.AddTableCommand(CString("PAC") + ogTableExt, CString("URT"), BC_PAC_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("PAC") + ogTableExt, CString("DRT"), BC_PAC_DELETE, false);

		ogBcHandle.AddTableCommand(CString("OAC") + ogTableExt, CString("IRT"), BC_OAC_NEW, false);
		ogBcHandle.AddTableCommand(CString("OAC") + ogTableExt, CString("URT"), BC_OAC_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("OAC") + ogTableExt, CString("DRT"), BC_OAC_DELETE, false);

		ogBcHandle.AddTableCommand(CString("PGP") + ogTableExt, CString("IRT"), BC_PGP_NEW, false);
		ogBcHandle.AddTableCommand(CString("PGP") + ogTableExt, CString("URT"), BC_PGP_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("PGP") + ogTableExt, CString("DRT"), BC_PGP_DELETE, false);

		ogBcHandle.AddTableCommand(CString("AWI") + ogTableExt, CString("IRT"), BC_AWI_NEW, false);
		ogBcHandle.AddTableCommand(CString("AWI") + ogTableExt, CString("URT"), BC_AWI_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("AWI") + ogTableExt, CString("DRT"), BC_AWI_DELETE, false);

		ogBcHandle.AddTableCommand(CString("CCC") + ogTableExt, CString("IRT"), BC_CCC_NEW, false);
		ogBcHandle.AddTableCommand(CString("CCC") + ogTableExt, CString("URT"), BC_CCC_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("CCC") + ogTableExt, CString("DRT"), BC_CCC_DELETE, false);

		ogBcHandle.AddTableCommand(CString("VIP") + ogTableExt, CString("IRT"), BC_VIP_NEW, false);
		ogBcHandle.AddTableCommand(CString("VIP") + ogTableExt, CString("URT"), BC_VIP_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("VIP") + ogTableExt, CString("DRT"), BC_VIP_DELETE, false);

		ogBcHandle.AddTableCommand(CString("AFM") + ogTableExt, CString("IRT"), BC_AFM_NEW, false);
		ogBcHandle.AddTableCommand(CString("AFM") + ogTableExt, CString("URT"), BC_AFM_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("AFM") + ogTableExt, CString("DRT"), BC_AFM_DELETE, false);

		ogBcHandle.AddTableCommand(CString("ENT") + ogTableExt, CString("IRT"), BC_ENT_NEW, false);
		ogBcHandle.AddTableCommand(CString("ENT") + ogTableExt, CString("URT"), BC_ENT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("ENT") + ogTableExt, CString("DRT"), BC_ENT_DELETE, false);

		ogBcHandle.AddTableCommand(CString("SRC") + ogTableExt, CString("IRT"), BC_SRC_NEW, false);
		ogBcHandle.AddTableCommand(CString("SRC") + ogTableExt, CString("URT"), BC_SRC_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("SRC") + ogTableExt, CString("DRT"), BC_SRC_DELETE, false);

		ogBcHandle.AddTableCommand(CString("PAR") + ogTableExt, CString("IRT"), BC_PAR_NEW, false);
		ogBcHandle.AddTableCommand(CString("PAR") + ogTableExt, CString("URT"), BC_PAR_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("PAR") + ogTableExt, CString("DRT"), BC_PAR_DELETE, false);

		ogBcHandle.AddTableCommand(CString("POL") + ogTableExt, CString("IRT"), BC_POL_NEW, false);
		ogBcHandle.AddTableCommand(CString("POL") + ogTableExt, CString("URT"), BC_POL_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("POL") + ogTableExt, CString("DRT"), BC_POL_DELETE, false);

		ogBcHandle.AddTableCommand(CString("SGM") + ogTableExt, CString("IRT"), BC_SGM_NEW, false);
		ogBcHandle.AddTableCommand(CString("SGM") + ogTableExt, CString("URT"), BC_SGM_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("SGM") + ogTableExt, CString("DRT"), BC_SGM_DELETE, false);
		
		ogBcHandle.AddTableCommand(CString("SGR") + ogTableExt, CString("IRT"), BC_SGR_NEW, false);
		ogBcHandle.AddTableCommand(CString("SGR") + ogTableExt, CString("URT"), BC_SGR_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("SGR") + ogTableExt, CString("DRT"), BC_SGR_DELETE, false);

		ogBcHandle.AddTableCommand(CString("MFM") + ogTableExt, CString("IRT"), BC_MFM_NEW, false);
		ogBcHandle.AddTableCommand(CString("MFM") + ogTableExt, CString("URT"), BC_MFM_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("MFM") + ogTableExt, CString("DRT"), BC_MFM_DELETE, false);

		ogBcHandle.AddTableCommand(CString("STS") + ogTableExt, CString("IRT"), BC_STS_NEW, false);
		ogBcHandle.AddTableCommand(CString("STS") + ogTableExt, CString("URT"), BC_STS_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("STS") + ogTableExt, CString("DRT"), BC_STS_DELETE, false);

		ogBcHandle.AddTableCommand(CString("NWH") + ogTableExt, CString("IRT"), BC_NWH_NEW, false);
		ogBcHandle.AddTableCommand(CString("NWH") + ogTableExt, CString("URT"), BC_NWH_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("NWH") + ogTableExt, CString("DRT"), BC_NWH_DELETE, false);

		ogBcHandle.AddTableCommand(CString("COH") + ogTableExt, CString("IRT"), BC_COH_NEW, false);
		ogBcHandle.AddTableCommand(CString("COH") + ogTableExt, CString("URT"), BC_COH_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("COH") + ogTableExt, CString("DRT"), BC_COH_DELETE, false);

		ogBcHandle.AddTableCommand(CString("VAL") + ogTableExt, CString("IRT"), BC_VAL_NEW, false);
		ogBcHandle.AddTableCommand(CString("VAL") + ogTableExt, CString("URT"), BC_VAL_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("VAL") + ogTableExt, CString("DRT"), BC_VAL_DELETE, false);

		ogBcHandle.AddTableCommand(CString("PMX") + ogTableExt, CString("IRT"), BC_PMX_NEW, false);
		ogBcHandle.AddTableCommand(CString("PMX") + ogTableExt, CString("URT"), BC_PMX_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("PMX") + ogTableExt, CString("DRT"), BC_PMX_DELETE, false);

		ogBcHandle.AddTableCommand(CString("WIS") + ogTableExt, CString("IRT"), BC_WIS_NEW, false);
		ogBcHandle.AddTableCommand(CString("WIS") + ogTableExt, CString("URT"), BC_WIS_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("WIS") + ogTableExt, CString("DRT"), BC_WIS_DELETE, false);

		ogBcHandle.AddTableCommand(CString("WGN") + ogTableExt, CString("IRT"), BC_WGN_NEW, false);
		ogBcHandle.AddTableCommand(CString("WGN") + ogTableExt, CString("URT"), BC_WGN_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("WGN") + ogTableExt, CString("DRT"), BC_WGN_DELETE, false);


		ogBcHandle.AddTableCommand(CString("DEV") + ogTableExt, CString("IRT"), BC_DEV_NEW, false);
		ogBcHandle.AddTableCommand(CString("DEV") + ogTableExt, CString("URT"), BC_DEV_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("DEV") + ogTableExt, CString("DRT"), BC_DEV_DELETE, false);

		ogBcHandle.AddTableCommand(CString("DSP") + ogTableExt, CString("IRT"), BC_DSP_NEW, false);
		ogBcHandle.AddTableCommand(CString("DSP") + ogTableExt, CString("URT"), BC_DSP_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("DSP") + ogTableExt, CString("DRT"), BC_DSP_DELETE, false);

		ogBcHandle.AddTableCommand(CString("PAG") + ogTableExt, CString("IRT"), BC_PAG_NEW, false);
		ogBcHandle.AddTableCommand(CString("PAG") + ogTableExt, CString("URT"), BC_PAG_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("PAG") + ogTableExt, CString("DRT"), BC_PAG_DELETE, false);

		ogBcHandle.AddTableCommand(CString("EQT") + ogTableExt, CString("IRT"), BC_EQT_NEW, false);
		ogBcHandle.AddTableCommand(CString("EQT") + ogTableExt, CString("URT"), BC_EQT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("EQT") + ogTableExt, CString("DRT"), BC_EQT_DELETE, false);

		ogBcHandle.AddTableCommand(CString("EQU") + ogTableExt, CString("IRT"), BC_EQU_NEW, false);
		ogBcHandle.AddTableCommand(CString("EQU") + ogTableExt, CString("URT"), BC_EQU_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("EQU") + ogTableExt, CString("DRT"), BC_EQU_DELETE, false);

		ogBcHandle.AddTableCommand(CString("DAT") + ogTableExt, CString("IRT"), BC_DAT_NEW, false);
		ogBcHandle.AddTableCommand(CString("DAT") + ogTableExt, CString("URT"), BC_DAT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("DAT") + ogTableExt, CString("DRT"), BC_DAT_DELETE, false);

		ogBcHandle.AddTableCommand(CString("OCC") + ogTableExt, CString("IRT"), BC_OCC_NEW, false);
		ogBcHandle.AddTableCommand(CString("OCC") + ogTableExt, CString("URT"), BC_OCC_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("OCC") + ogTableExt, CString("DRT"), BC_OCC_DELETE, false);
		//PRF 8378
		ogBcHandle.AddTableCommand(CString("CRC") + ogTableExt, CString("IRT"), BC_CRC_NEW, false);
		ogBcHandle.AddTableCommand(CString("CRC") + ogTableExt, CString("URT"), BC_CRC_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("CRC") + ogTableExt, CString("DRT"), BC_CRC_DELETE, false);

		ogBcHandle.AddTableCommand(CString("AAD") + ogTableExt, CString("IRT"), BC_AAD_NEW, false);
		ogBcHandle.AddTableCommand(CString("AAD") + ogTableExt, CString("URT"), BC_AAD_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("AAD") + ogTableExt, CString("DRT"), BC_AAD_DELETE, false);

		ogBcHandle.AddTableCommand(CString("PDA") + ogTableExt, CString("IRT"), BC_PDA_NEW, false);
		ogBcHandle.AddTableCommand(CString("PDA") + ogTableExt, CString("URT"), BC_PDA_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("PDA") + ogTableExt, CString("DRT"), BC_PDA_DELETE, false);

		ogBcHandle.AddTableCommand(CString("DSC") + ogTableExt, CString("IRT"), BC_DSC_NEW, false);
		ogBcHandle.AddTableCommand(CString("DSC") + ogTableExt, CString("URT"), BC_DSC_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("DSC") + ogTableExt, CString("DRT"), BC_DSC_DELETE, false);

		ogBcHandle.AddTableCommand(CString("DPT") + ogTableExt, CString("IRT"), BC_DPT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("DPT") + ogTableExt, CString("URT"), BC_DPT_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("DPT") + ogTableExt, CString("DRT"), BC_DPT_DELETE, false);

    	ogBcHandle.AddTableCommand(CString("FLG") + ogTableExt, CString("IRT"), BC_FLG_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("FLG") + ogTableExt, CString("URT"), BC_FLG_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("FLG") + ogTableExt, CString("DRT"), BC_FLG_DELETE, false);

		ogBcHandle.AddTableCommand(CString("CHU") + ogTableExt, CString("IRT"), BC_CHU_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("CHU") + ogTableExt, CString("URT"), BC_CHU_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("CHU") + ogTableExt, CString("DRT"), BC_CHU_DELETE, false);

		ogBcHandle.AddTableCommand(CString("CTN") + ogTableExt, CString("IRT"), BC_CTN_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("CTN") + ogTableExt, CString("URT"), BC_CTN_CHANGE, false);
		ogBcHandle.AddTableCommand(CString("CTN") + ogTableExt, CString("DRT"), BC_CTN_DELETE, false);

		ogBcHandle.AddTableCommand(CString("SBCXXX"),CString("SBC"),BC_SBC_XXX, true);


		// Versionsinformation ermitteln
		VersionInfo rlInfo;
		VersionInfo::GetVersionInfo(NULL,rlInfo);	// must use NULL (and it works), because AFX is not initialized yet and will cause an ASSERT
		CCSCedaData::bmVersCheck = true;
		strncpy(CCSCedaData::pcmVersion,rlInfo.omFileVersion,sizeof(CCSCedaData::pcmVersion));
		CCSCedaData::pcmVersion[sizeof(CCSCedaData::pcmVersion)-1] = '\0';

		strncpy(CCSCedaData::pcmInternalBuild,rlInfo.omPrivateBuild.Right(4),sizeof(CCSCedaData::pcmInternalBuild));
		CCSCedaData::pcmInternalBuild[sizeof(CCSCedaData::pcmInternalBuild)-1] = '\0';

		//////////////////////////////////////
		AatHelp::Initialize("BDPS-UIF");
		//////////////////////////////////////



	return FALSE;
}

BOOL CBDPSUIFApp::ExitInstance()
{
	AatHelp::ExitApp();

	return FALSE;
}

bool CBDPSUIFApp::OnSpecialHelp()
{
	AfxGetApp()->WinHelp(0,HELP_CONTENTS);
	return true;
}

void CBDPSUIFApp::WinHelp(DWORD dwData, UINT nCmd)
{
	AatHelp::WinHelp(dwData,nCmd);	
}

void CBDPSUIFApp::LoadParameters()
{
	// aktuelle Zeit erhalten
	COleDateTime olTimeNow = COleDateTime::GetCurrentTime();
	CString olStringNow = olTimeNow.Format("%Y%m%d%H%M%S");
	CString olDefaultValidFrom	= ("19901010101010");
	CString olDefaultValidTo	= "";

	// Falls Parameter nicht exitiert. anlegen und Defaultwert setzen.
	// General
	ogParData.Read();
	ogValData.Read();

	ogParData.GetParById("GLOBAL","ID_GAT_POS",olStringNow,"Y","GATPOS enabled","General","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","",true);
	ogParData.GetParById("GLOBAL","ID_UNIQUEACTPAIR",olStringNow,"N","Use Unique ACT3/ACT5 pairs","General","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","",true);
	ogParData.GetParById("GLOBAL","ID_EXT_LM",olStringNow,"N","Extended Locationmanagement enabled","General","","TRIM",olDefaultValidFrom,olDefaultValidTo,"","","",true);
	
}

void CBDPSUIFApp::CheckGatPosAllocations()
{
	if (ogBasicData.IsGatPosEnabled())
	{
		if (ogAloData.ReadAloData())
		{

			ALODATA *prlAlo = ogAloData.GetAloByName("GATPOS");
			if (prlAlo == NULL)
			{
				ogAloData.Insert("GATPOS");
			}

			prlAlo = ogAloData.GetAloByName("BAGGAT");
			if (prlAlo == NULL)
			{
				ogAloData.Insert("BAGGAT");
			}

			prlAlo = ogAloData.GetAloByName("WROGAT");
			if (prlAlo == NULL)
			{
				ogAloData.Insert("WROGAT");
			}

			prlAlo = ogAloData.GetAloByName("EXTBAG");
			if (prlAlo == NULL)
			{
				ogAloData.Insert("EXTBAG");
			}

			prlAlo = ogAloData.GetAloByName("BAGGAT");
			if (prlAlo == NULL)
			{
				ogAloData.Insert("BAGGAT");
			}
		}
	}

}