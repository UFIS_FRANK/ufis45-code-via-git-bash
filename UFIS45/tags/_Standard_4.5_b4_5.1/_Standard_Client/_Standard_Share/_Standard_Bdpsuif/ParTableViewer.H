#ifndef __PARTABLEVIEWER_H__
#define __PARTABLEVIEWER_H__

#include <stdafx.h>
#include <CedaParData.h>
#include <CedaValData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>

struct PARTABLE_LINEDATA
{
	long	Urno;
	CString	Name;
	CString	Ptyp;
	CString	Valu;
	CString	Vafr;
	CString	Vato;
};

/////////////////////////////////////////////////////////////////////////////
// ParTableViewer

class ParTableViewer : public CViewer
{
// Constructions
public:
    ParTableViewer(CCSPtrArray<PARDATA> *popData);
    ~ParTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	BOOL IsPassFilter(PARDATA *prpPar);
	int ComparePar(PARTABLE_LINEDATA *prpPar1, PARTABLE_LINEDATA *prpPar2);
    void MakeLines();
	void MakeLine(PARDATA *prpPar);
	BOOL FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(PARTABLE_LINEDATA *prpPar);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(PARTABLE_LINEDATA *prpLine);
	void ProcessParChange(PARDATA *prpPar);
	void ProcessParDelete(PARDATA *prpPar);
	BOOL FindPar(char *prpParKeya, char *prpParKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	BOOL bmIsFromSearch;
// Attributes
private:
    CCSTable *pomParTable;
	CCSPtrArray<PARDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<PARTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(PARTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__PARTABLEVIEWER_H__
