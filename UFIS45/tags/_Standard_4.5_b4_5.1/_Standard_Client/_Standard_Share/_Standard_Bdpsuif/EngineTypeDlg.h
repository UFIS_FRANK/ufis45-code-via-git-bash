#if !defined(AFX_ENGINETYPEDLG_H__AB36A832_7540_11D1_B430_0000B45A33F5__INCLUDED_)
#define AFX_ENGINETYPEDLG_H__AB36A832_7540_11D1_B430_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// EngineTypeDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// EngineTypeDlg dialog
#include <CedaSphData.h>
#include <CCSEdit.h>
#include <CedaEntData.h>

class EngineTypeDlg : public CDialog
{
// Construction
public:
	EngineTypeDlg(ENTDATA *popEnt, CWnd* pParent = NULL);   // standard constructor

	ENTDATA *pomEnt;
// Dialog Data
	//{{AFX_DATA(EngineTypeDlg)
	enum { IDD = IDD_ENGINETYPE };
	CButton	m_OK;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_ENTC;
	CCSEdit	m_ENAM;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEC;
	CCSEdit	m_USEU;
	CString m_Caption;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(EngineTypeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(EngineTypeDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ENGINETYPEDLG_H__AB36A832_7540_11D1_B430_0000B45A33F5__INCLUDED_)
