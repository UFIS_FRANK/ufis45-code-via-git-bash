// CedaWayData.cpp
 
#include <stdafx.h>
#include <CedaWayData.h>
#include <resource.h>


void ProcessWayCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaWayData::CedaWayData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for WAYDATA
	BEGIN_CEDARECINFO(WAYDATA,WayDataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Pobe,"POBE")
		FIELD_CHAR_TRIM	(Poen,"POEN")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Ttgo,"TTGO")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Wtgo,"WTGO")
		FIELD_CHAR_TRIM	(Wtyp,"WTYP")
		FIELD_CHAR_TRIM	(Tybe,"TYBE")
		FIELD_CHAR_TRIM	(Tyen,"TYEN")
		FIELD_CHAR_TRIM	(Home,"HOME")

	END_CEDARECINFO //(WAYDATA)

	// Copy the record structure
	for (int i=0; i< sizeof(WayDataRecInfo)/sizeof(WayDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&WayDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"WAY");
	strcpy(pcmListOfFields,"CDAT,LSTU,POBE,POEN,PRFL,TTGO,URNO,USEC,USEU,WTGO,WTYP,TYBE,TYEN,HOME");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaWayData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("CDAT");
	ropFields.Add("LSTU");
	ropFields.Add("POBE");
	ropFields.Add("POEN");
	ropFields.Add("PRFL");
	ropFields.Add("TTGO");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	ropFields.Add("WTGO");
	ropFields.Add("WTYP");
	ropFields.Add("TYBE");
	ropFields.Add("TYEN");
	ropFields.Add("HOME");

	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING420));
	ropDesription.Add(LoadStg(IDS_STRING421));
	ropDesription.Add(LoadStg(IDS_STRING345));
	ropDesription.Add(LoadStg(IDS_STRING427));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));
	ropDesription.Add(LoadStg(IDS_STRING426));
	ropDesription.Add(LoadStg(IDS_STRING511));
	ropDesription.Add(LoadStg(IDS_STRING512));
	ropDesription.Add(LoadStg(IDS_STRING513));
	ropDesription.Add(LoadStg(IDS_STRING513));

	ropType.Add("Date");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaWayData::Register(void)
{
	ogDdx.Register((void *)this,BC_WAY_CHANGE,	CString("WAYDATA"), CString("Way-changed"),	ProcessWayCf);
	ogDdx.Register((void *)this,BC_WAY_NEW,		CString("WAYDATA"), CString("Way-new"),		ProcessWayCf);
	ogDdx.Register((void *)this,BC_WAY_DELETE,	CString("WAYDATA"), CString("Way-deleted"),	ProcessWayCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaWayData::~CedaWayData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaWayData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaWayData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		WAYDATA *prlWay = new WAYDATA;
		if ((ilRc = GetFirstBufferRecord(prlWay)) == true)
		{
			omData.Add(prlWay);//Update omData
			omUrnoMap.SetAt((void *)prlWay->Urno,prlWay);
		}
		else
		{
			delete prlWay;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaWayData::Insert(WAYDATA *prpWay)
{
	prpWay->IsChanged = DATA_NEW;
	if(Save(prpWay) == false) return false; //Update Database
	InsertInternal(prpWay);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaWayData::InsertInternal(WAYDATA *prpWay)
{
	ogDdx.DataChanged((void *)this, WAY_NEW,(void *)prpWay ); //Update Viewer
	omData.Add(prpWay);//Update omData
	omUrnoMap.SetAt((void *)prpWay->Urno,prpWay);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaWayData::Delete(long lpUrno)
{
	WAYDATA *prlWay = GetWayByUrno(lpUrno);
	if (prlWay != NULL)
	{
		prlWay->IsChanged = DATA_DELETED;
		if(Save(prlWay) == false) return false; //Update Database
		DeleteInternal(prlWay);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaWayData::DeleteInternal(WAYDATA *prpWay)
{
	ogDdx.DataChanged((void *)this,WAY_DELETE,(void *)prpWay); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpWay->Urno);
	int ilWayCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilWayCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpWay->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaWayData::Update(WAYDATA *prpWay)
{
	if (GetWayByUrno(prpWay->Urno) != NULL)
	{
		if (prpWay->IsChanged == DATA_UNCHANGED)
		{
			prpWay->IsChanged = DATA_CHANGED;
		}
		if(Save(prpWay) == false) return false; //Update Database
		UpdateInternal(prpWay);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaWayData::UpdateInternal(WAYDATA *prpWay)
{
	WAYDATA *prlWay = GetWayByUrno(prpWay->Urno);
	if (prlWay != NULL)
	{
		*prlWay = *prpWay; //Update omData
		ogDdx.DataChanged((void *)this,WAY_CHANGE,(void *)prlWay); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

WAYDATA *CedaWayData::GetWayByUrno(long lpUrno)
{
	WAYDATA  *prlWay;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlWay) == TRUE)
	{
		return prlWay;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaWayData::ReadSpecialData(CCSPtrArray<WAYDATA> *popWay,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","WAY",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","WAY",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popWay != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			WAYDATA *prpWay = new WAYDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpWay,CString(pclFieldList))) == true)
			{
				popWay->Add(prpWay);
			}
			else
			{
				delete prpWay;
			}
		}
		if(popWay->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaWayData::Save(WAYDATA *prpWay)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpWay->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpWay->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpWay);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpWay->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpWay->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpWay);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpWay->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpWay->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessWayCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogWayData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaWayData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlWayData;
	prlWayData = (struct BcStruct *) vpDataPointer;
	WAYDATA *prlWay;
	if(ipDDXType == BC_WAY_NEW)
	{
		prlWay = new WAYDATA;
		GetRecordFromItemList(prlWay,prlWayData->Fields,prlWayData->Data);
		if(ValidateWayBcData(prlWay->Urno)) //Prf: 8795
		{
			InsertInternal(prlWay);
		}
		else
		{
			delete prlWay;
		}
	}
	if(ipDDXType == BC_WAY_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlWayData->Selection);
		prlWay = GetWayByUrno(llUrno);
		if(prlWay != NULL)
		{
			GetRecordFromItemList(prlWay,prlWayData->Fields,prlWayData->Data);
			if(ValidateWayBcData(prlWay->Urno)) //Prf: 8795
			{
				UpdateInternal(prlWay);
			}
			else
			{
				DeleteInternal(prlWay);
			}
		}
	}
	if(ipDDXType == BC_WAY_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlWayData->Selection);
		prlWay = GetWayByUrno(llUrno);
		if (prlWay != NULL)
		{
			DeleteInternal(prlWay);
		}
	}
}

//Prf: 8795
//--ValidateWayBcData--------------------------------------------------------------------------------------

bool CedaWayData::ValidateWayBcData(const long& lrpUrno)
{
	bool blValidateWayBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<WAYDATA> olWays;
		if(!ReadSpecialData(&olWays,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateWayBcData = false;
		}
	}
	return blValidateWayBcData;
}

//---------------------------------------------------------------------------------------------------------
