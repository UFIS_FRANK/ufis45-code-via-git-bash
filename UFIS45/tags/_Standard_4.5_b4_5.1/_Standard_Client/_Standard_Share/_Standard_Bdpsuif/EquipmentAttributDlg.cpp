// EquipmentAttributDlg.cpp : implementation file
//

#include <stdafx.h>
#include <bdpsuif.h>
#include <EquipmentAttributDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// EquipmentAttributDlg dialog


EquipmentAttributDlg::EquipmentAttributDlg(EQADATA *popEqa, CWnd* pParent /*=NULL*/)
	: CDialog(EquipmentAttributDlg::IDD, pParent)
{
	pomEqa = popEqa;
	//{{AFX_DATA_INIT(EquipmentAttributDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void EquipmentAttributDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(EquipmentAttributDlg)
	DDX_Control(pDX, IDC_VALU, m_VALU);
	DDX_Control(pDX, IDC_NAME, m_NAME);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(EquipmentAttributDlg, CDialog)
	//{{AFX_MSG_MAP(EquipmentAttributDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// EquipmentAttributDlg message handlers

BOOL EquipmentAttributDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_NAME.SetTextLimit(1,20);
	m_NAME.SetBKColor(YELLOW);  
	m_NAME.SetTextErrColor(RED);
	m_NAME.SetInitText(pomEqa->Name);

	m_VALU.SetTextLimit(0,40);
	m_VALU.SetTextErrColor(RED);
	m_VALU.SetInitText(pomEqa->Valu);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void EquipmentAttributDlg::OnOK() 
{
	CString olErrorText;
	bool ilStatus = true;
	
	// TODO: Add extra validation here
	if(m_NAME.GetStatus() == false) 
	{
		ilStatus = false;
		if(m_NAME.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING870) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING870) + ogNotFormat;
		}
	}

	if(m_VALU.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText +=  LoadStg(IDS_STRING587) + ogNotFormat;
	}

	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_NAME.GetWindowText(pomEqa->Name, 21);
		m_VALU.GetWindowText(pomEqa->Valu, 41);
		
		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_NAME.SetFocus();
	}
}
