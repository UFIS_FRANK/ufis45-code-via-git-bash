// CedaSwgData.cpp
 
#include <stdafx.h>
#include <CedaSwgData.h>
#include <resource.h>


void ProcessSwgCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaSwgData::CedaSwgData() : CCSCedaData(&ogCommHandler)
{
	// Create an array of CEDARECINFO for SWGDataStruct
	BEGIN_CEDARECINFO(SWGDATA,SWGDataRecInfo)
		FIELD_CHAR_TRIM	(Code,"CODE")
		FIELD_LONG		(Surn,"SURN")
		FIELD_LONG		(Urno,"URNO")
		FIELD_OLEDATE	(Vpfr,"VPFR")
		FIELD_OLEDATE	(Vpto,"VPTO")

	END_CEDARECINFO //(SWGDataStruct)

	// FIELD_LONG, FIELD_DATE 
	// Copy the record structure
	for (int i=0; i< sizeof(SWGDataRecInfo)/sizeof(SWGDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&SWGDataRecInfo[i],sizeof(CEDARECINFO));
		omRecInfo.Add(prpCedaRecInfo);
	} // end for

	// initialize field names
	strcpy(pcmTableName,"SWG");
	strcpy(pcmListOfFields,"CODE,SURN,URNO,VPFR,VPTO");
	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaSwgData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("CODE");
	ropFields.Add("SURN");
	ropFields.Add("URNO");
	ropFields.Add("VPFR");
	ropFields.Add("VPTO");

	ropDesription.Add(LoadStg(IDS_STRING237));
	ropDesription.Add(LoadStg(IDS_STRING183));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING230));
	ropDesription.Add(LoadStg(IDS_STRING231));

	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("Date");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaSwgData::Register(void)
{
	ogDdx.Register((void *)this,BC_SWG_CHANGE,	CString("SWGDATA"), CString("Swg-changed"),	ProcessSwgCf);
	ogDdx.Register((void *)this,BC_SWG_NEW,		CString("SWGDATA"), CString("Swg-new"),		ProcessSwgCf);
	ogDdx.Register((void *)this,BC_SWG_DELETE,	CString("SWGDATA"), CString("Swg-deleted"),	ProcessSwgCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaSwgData::~CedaSwgData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaSwgData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omStaffUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaSwgData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	COleDateTime olCurrTime = COleDateTime::GetCurrentTime();

	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omStaffUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		SWGDATA *prlSwg = new SWGDATA;
		if ((ilRc = GetFirstBufferRecord(prlSwg)) == true)
		{
			omData.Add(prlSwg);//Update omData
			omUrnoMap.SetAt((void *)prlSwg->Urno,prlSwg);
			bool blVptoIsOk = true;
			if (prlSwg->Vpto.GetStatus() == COleDateTime::valid)
			{
				if(prlSwg->Vpto < olCurrTime)
				{
					blVptoIsOk = false;
				}
			}

			if ((prlSwg->Vpfr < olCurrTime ) && blVptoIsOk)
			{
				omStaffUrnoMap.SetAt((void *)prlSwg->Surn,prlSwg);
			}
		}
		else
		{
			delete prlSwg;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaSwgData::Insert(SWGDATA *prpSwg)
{
	prpSwg->IsChanged = DATA_NEW;
	if(Save(prpSwg) == false) return false; //Update Database
	InsertInternal(prpSwg);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaSwgData::InsertInternal(SWGDATA *prpSwg)
{
	COleDateTime olCurrTime = COleDateTime::GetCurrentTime();

	omData.Add(prpSwg);//Update omData
	omUrnoMap.SetAt((void *)prpSwg->Urno,prpSwg);
	bool blVptoIsOk = true;
	if (prpSwg->Vpto.GetStatus() == COleDateTime::valid)
	{
		if(prpSwg->Vpto < olCurrTime)
		{
			blVptoIsOk = false;
		}
	}

	if((prpSwg->Vpfr < olCurrTime ) && blVptoIsOk)
	{
		omStaffUrnoMap.SetAt((void *)prpSwg->Surn,prpSwg);
	}

	ogDdx.DataChanged((void *)this, SWG_NEW,(void *)prpSwg ); //Update Viewer
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaSwgData::Delete(long lpUrno)
{
	SWGDATA *prlSwg = GetSwgByUrno(lpUrno);
	if (prlSwg != NULL)
	{
		prlSwg->IsChanged = DATA_DELETED;
		if(Save(prlSwg) == false) return false; //Update Database
		DeleteInternal(prlSwg);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaSwgData::DeleteInternal(SWGDATA *prpSwg)
{
	COleDateTime olCurrTime = COleDateTime::GetCurrentTime();

	bool blVptoIsOk = true;

	if(prpSwg->Vpto.GetStatus() == COleDateTime::valid)
	{
		if(prpSwg->Vpto < olCurrTime)
		{
			blVptoIsOk = false;
		}
	}

	if((prpSwg->Vpfr < olCurrTime ) && blVptoIsOk)
	{
		omStaffUrnoMap.RemoveKey((void *)prpSwg->Surn);
		int ilSwgCount = omData.GetSize();
		for (int ilLc = 0; ilLc < ilSwgCount; ilLc++)
		{
			if (omData[ilLc].Urno == prpSwg->Urno)
			{
				continue;
			}
			else
			{
				bool blVptoIsOk = true;
				if (omData[ilLc].Vpto.GetStatus() == COleDateTime::valid)
				{
					if(omData[ilLc].Vpto < olCurrTime)
					{
						blVptoIsOk = false;
					}
				}

				if ((omData[ilLc].Vpfr < olCurrTime ) && blVptoIsOk)
				{
					omStaffUrnoMap.SetAt((void *)omData[ilLc].Surn,&omData[ilLc]);
				}

			}
		}
	}


	ogDdx.DataChanged((void *)this,SWG_DELETE,(void *)prpSwg); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpSwg->Urno);

	int ilSwgCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilSwgCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpSwg->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaSwgData::Update(SWGDATA *prpSwg)
{
	if (GetSwgByUrno(prpSwg->Urno) != NULL)
	{
		if (prpSwg->IsChanged == DATA_UNCHANGED)
		{
			prpSwg->IsChanged = DATA_CHANGED;
		}
		if(Save(prpSwg) == false) return false; //Update Database
		UpdateInternal(prpSwg);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaSwgData::UpdateInternal(SWGDATA *prpSwg)
{
	COleDateTime olCurrTime = COleDateTime::GetCurrentTime();

	SWGDATA *prlSwg = GetSwgByUrno(prpSwg->Urno);
	if (prlSwg != NULL)
	{
		*prlSwg = *prpSwg; //Update omData
		bool blVptoIsOk = true;
		if(prlSwg->Vpto.GetStatus() == COleDateTime::valid)
		{
			if(prlSwg->Vpto < olCurrTime)
			{
				blVptoIsOk = false;
			}
		}

		if((prlSwg->Vpfr < olCurrTime ) && blVptoIsOk)
		{
			omStaffUrnoMap.SetAt((void *)prlSwg->Surn,prlSwg);
		}
		ogDdx.DataChanged((void *)this,SWG_CHANGE,(void *)prlSwg); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

SWGDATA *CedaSwgData::GetSwgByUrno(long lpUrno)
{
	SWGDATA  *prlSwg;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlSwg) == TRUE)
	{
		return prlSwg;
	}
	return NULL;
}

//--GET-BY-SURN--------------------------------------------------------------------------------------------

SWGDATA *CedaSwgData::GetValidSwgBySurn(long lpSurn)
{
	SWGDATA  *prlSwg;
	if (omStaffUrnoMap.Lookup((void *)lpSurn,(void *& )prlSwg) == TRUE)
	{
		return prlSwg;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaSwgData::ReadSpecialData(CCSPtrArray<SWGDATA> *popSwg,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","SWG",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","SWG",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popSwg != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			SWGDATA *prpSwg = new SWGDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpSwg,CString(pclFieldList))) == true)
			{
				popSwg->Add(prpSwg);
			}
			else
			{
				delete prpSwg;
			}
		}
		if(popSwg->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaSwgData::Save(SWGDATA *prpSwg)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpSwg->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpSwg->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpSwg);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpSwg->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSwg->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpSwg);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpSwg->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpSwg->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessSwgCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogSwgData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaSwgData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlSwgData;
	prlSwgData = (struct BcStruct *) vpDataPointer;
	SWGDATA *prlSwg;
	if(ipDDXType == BC_SWG_NEW)
	{
		prlSwg = new SWGDATA;
		GetRecordFromItemList(prlSwg,prlSwgData->Fields,prlSwgData->Data);
		InsertInternal(prlSwg);
	}
	if(ipDDXType == BC_SWG_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlSwgData->Selection);
		prlSwg = GetSwgByUrno(llUrno);
		if(prlSwg != NULL)
		{
			GetRecordFromItemList(prlSwg,prlSwgData->Fields,prlSwgData->Data);
			UpdateInternal(prlSwg);
		}
	}
	if(ipDDXType == BC_SWG_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlSwgData->Selection);

		prlSwg = GetSwgByUrno(llUrno);
		if (prlSwg != NULL)
		{
			DeleteInternal(prlSwg);
		}
	}
}


//---------------------------------------------------------------------------------------------------------
