// SrcDlg.cpp : implementation file
//

#include <stdafx.h>
#include <bdpsuif.h>
#include <SrcDlg.h>
#include <PrivList.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// SrcDlg dialog


SrcDlg::SrcDlg(SRCDATA *popSrc, CWnd* pParent /*=NULL*/)
	: CDialog(SrcDlg::IDD, pParent)
{
	pomSrc = popSrc;
	//{{AFX_DATA_INIT(SrcDlg)
	//}}AFX_DATA_INIT
}


void SrcDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(SrcDlg)
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_SRCC, m_SRCC);
	DDX_Control(pDX, IDC_SRCD, m_SRCD);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEC, m_USEC);
	DDX_Control(pDX, IDC_USEU, m_USEU);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(SrcDlg, CDialog)
	//{{AFX_MSG_MAP(SrcDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// SrcDlg message handlers
//----------------------------------------------------------------------------------------

BOOL SrcDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	m_Caption = LoadStg(IDS_STRING825) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	
	char clStat = ogPrivList.GetStat("SRCDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomSrc->Cdat.Format("%d.%m.%Y"));
	//m_CDATD.SetSecState(ogPrivList.GetStat("SRCDLG.m_CDAT"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomSrc->Cdat.Format("%H:%M"));
	//m_CDATT.SetSecState(ogPrivList.GetStat("SRCDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomSrc->Lstu.Format("%d.%m.%Y"));
	//m_LSTUD.SetSecState(ogPrivList.GetStat("SRCDLG.m_LSTU"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomSrc->Lstu.Format("%H:%M"));
	//m_LSTUT.SetSecState(ogPrivList.GetStat("SRCDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomSrc->Usec);
	//m_USEC.SetSecState(ogPrivList.GetStat("SRCDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomSrc->Useu);
	//m_USEU.SetSecState(ogPrivList.GetStat("SRCDLG.m_USEU"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_SRCC.SetFormat("x|#x|#x|#x|#");
	m_SRCC.SetTextLimit(1,4);
	m_SRCC.SetBKColor(YELLOW);  
	m_SRCC.SetTextErrColor(RED);
	m_SRCC.SetInitText(pomSrc->Srcc);
	//m_SRCC.SetSecState(ogPrivList.GetStat("SRCDLG.m_SRCC"));
	//------------------------------------
	m_SRCD.SetTypeToString("X(32)",32,1);
	m_SRCD.SetBKColor(YELLOW);  
	m_SRCD.SetTextErrColor(RED);
	m_SRCD.SetInitText(pomSrc->Srcd);
	//m_SRCD.SetSecState(ogPrivList.GetStat("SRCDLG.m_SRCD"));
	//------------------------------------
	return TRUE;  
}

//----------------------------------------------------------------------------------------

void SrcDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_SRCC.GetStatus() == false)
	{
		ilStatus = false;
		if(m_SRCC.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING826) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING826) + ogNotFormat;
		}
	}
	if(m_SRCD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_SRCD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING827) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING827) + ogNotFormat;
		}
	}

	///////////////////////////////////////////////////////////////////
	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olText;//
		if(m_SRCC.GetWindowTextLength() != 0)
		{
			CCSPtrArray<SRCDATA> olSrcCPA;
			char clWhere[100];
			m_SRCC.GetWindowText(olText);
			sprintf(clWhere,"WHERE SRCC='%s'",olText);
			if(ogSrcData.ReadSpecialData(&olSrcCPA,clWhere,"URNO",false) == true)
			{
				if(olSrcCPA[0].Urno != pomSrc->Urno)
				{
					ilStatus = false;
					olErrorText += LoadStg(IDS_STRING237) + LoadStg(IDS_EXIST);
				}
			}
			olSrcCPA.DeleteAll();
		}
	}
	///////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_SRCC.GetWindowText(pomSrc->Srcc,5);
		m_SRCD.GetWindowText(pomSrc->Srcd,33);
		//wandelt Return in Blank//
		////////////////////////////

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_SRCC.SetFocus();
	}	

	
}

//----------------------------------------------------------------------------------------
