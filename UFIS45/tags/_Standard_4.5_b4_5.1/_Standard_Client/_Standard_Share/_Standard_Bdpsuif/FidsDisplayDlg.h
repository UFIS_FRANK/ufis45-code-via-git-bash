#if !defined(AFX_FIDSDISPLAYDLG_H__E8F15454_4A26_11D7_8007_00010215BFDE__INCLUDED_)
#define AFX_FIDSDISPLAYDLG_H__E8F15454_4A26_11D7_8007_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FidsDisplayDlg.h : header file
//
#include <CedaDSPData.h>
#include <CCSEdit.h>
#include <CCSTable.h>
#include <CCSComboBox.h>

/////////////////////////////////////////////////////////////////////////////
// FidsDisplayDlg dialog

class FidsDisplayDlg : public CDialog
{
// Construction
public:
	FidsDisplayDlg(DSPDATA *popDSP,const CString& ropCaption,CWnd* pParent = NULL);   // standard constructor
	~FidsDisplayDlg();

// Dialog Data
	//{{AFX_DATA(FidsDisplayDlg)
	enum { IDD = IDD_FIDS_DISPLAY };
	CCSEdit	m_DNOP;
	CCSEdit	m_DAPN;
	CCSEdit	m_DEMT;
	CCSEdit	m_DFPN;
	CCSEdit	m_DPID;
	CCSEdit	m_DICF;

	CButton	m_PAGENEW;
	CButton	m_PAGEDEL;
	CStatic	m_PAGEFRAME;

	CCSEdit	m_VAFRD;
	CCSEdit	m_VAFRT;
	CCSEdit	m_VATOD;
	CCSEdit	m_VATOT;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEU;
	CCSEdit	m_USEC;
	CCSEdit m_REMA;
	CButton	m_CANCEL;
	CButton	m_OK;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(FidsDisplayDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(FidsDisplayDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
    afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	afx_msg void OnPageDel();
	afx_msg void OnPageNew();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void MakePageTable();
	void ChangePageTable(char *popPageId,char *popPageConfig,char *popPageNo,int ipLineNo);
	char *PageIdFromIndex(int ipIndex);
	char *PageConfigFromIndex(int ipIndex);
	char *PageNoFromIndex(int ipIndex);
	bool SelectPage(char *popPageIdm);
	bool EditPageNumber(int ipLine,char *popPageNo);
	LONG OnTableIPEditKillfocus( UINT wParam, LPARAM lParam);
	void ResizeDialog();

private:
	CString		m_Caption;
	DSPDATA		*pomDSP;
	CCSTable	*pomTable;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FIDSDISPLAYDLG_H__E8F15454_4A26_11D7_8007_00010215BFDE__INCLUDED_)
