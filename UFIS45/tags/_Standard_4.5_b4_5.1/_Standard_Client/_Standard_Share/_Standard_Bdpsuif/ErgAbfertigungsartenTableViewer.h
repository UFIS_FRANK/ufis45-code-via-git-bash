#ifndef __ERGABFERTIGUNGSARTENTABLEVIEWER_H__
#define __ERGABFERTIGUNGSARTENTABLEVIEWER_H__

#include <stdafx.h>
#include <CedaSphData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct ERGABFERTIGUNGSARTENTABLE_LINEDATA
{
	CString	Act3;	//Flugzeugtyp
	CString	Actm;	//Flugzeugtyp Gruppe
	CString	Alcm;	//Fluggesellschaft Gruppe
	CString	Apc3;	//Flughafen
	CString	Apcm;	//Flughafen Gruppe
	CString	Arde;	//Ankunft oder Abflug
	CString	Days;	//Verkehrstage
	CString	Flnc;	//Flugnummer Code
	CString	Flnn;	//Flugnummer Nummer
	CString	Flns;	//Flugnummer Suffix
	CString	Regn;	//Flugzeug-Kennzeichen
	CString	Styp;	//Servicetyp
	CString	Ttyp;	//Verkehrsartenschlüssel
	long	Urno;	//Eindeutige Datensatz-Nr.
};

/////////////////////////////////////////////////////////////////////////////
// ErgAbfertigungsartenTableViewer

class ErgAbfertigungsartenTableViewer : public CViewer
{
// Constructions
public:
    ErgAbfertigungsartenTableViewer(CCSPtrArray<SPHDATA> *popData);
    ~ErgAbfertigungsartenTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(SPHDATA *prpErgAbfertigungsarten);
	int CompareErgAbfertigungsarten(ERGABFERTIGUNGSARTENTABLE_LINEDATA *prpErgAbfertigungsarten1, ERGABFERTIGUNGSARTENTABLE_LINEDATA *prpErgAbfertigungsarten2);
    void MakeLines();
	void MakeLine(SPHDATA *prpErgAbfertigungsarten);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(ERGABFERTIGUNGSARTENTABLE_LINEDATA *prpErgAbfertigungsarten);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(ERGABFERTIGUNGSARTENTABLE_LINEDATA *prpLine);
	void ProcessErgAbfertigungsartenChange(SPHDATA *prpErgAbfertigungsarten);
	void ProcessErgAbfertigungsartenDelete(SPHDATA *prpErgAbfertigungsarten);
	bool FindErgAbfertigungsarten(char *prpErgAbfertigungsartenKeya, char *prpErgAbfertigungsartenKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomErgAbfertigungsartenTable;
	CCSPtrArray<SPHDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<ERGABFERTIGUNGSARTENTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(ERGABFERTIGUNGSARTENTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__ERGABFERTIGUNGSARTENTABLEVIEWER_H__
