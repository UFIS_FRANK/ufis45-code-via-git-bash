// GatPosGateDlg.cpp : implementation file
//

#include "stdafx.h"
#include "bdpsuif.h"
#include "GatPosGateDlg.h"
#include <SelectionDlg.h>
#include <CedaBltData.h>
#include <CedaWroData.h>
#include <BasicData.h>
#include <GatPosNotAvailableDlg.h>
#include <PrivList.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
 
/////////////////////////////////////////////////////////////////////////////
// GatPosGateDlg dialog


GatPosGateDlg::GatPosGateDlg(GATDATA *popGAT,CWnd* pParent /*=NULL*/)
	: GateDlg(popGAT,IDD_GATPOS_GATEDLG, pParent)
{
	
}

GatPosGateDlg::GatPosGateDlg(GATDATA *popGAT,int ipDlg,CWnd* pParent /*=NULL*/)
	: GateDlg(popGAT,ipDlg, pParent)
{

}


void GatPosGateDlg::DoDataExchange(CDataExchange* pDX)
{
	GateDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(GatPosGateDlg)
	DDX_Control(pDX, IDC_GATES_LIST, m_ConnectedGates);
	DDX_Control(pDX, IDC_FCOM, m_FCOM);
	DDX_Control(pDX, IDC_BLOCKING_BITMAP, m_BlockingBitmap);
	DDX_Control(pDX, IDC_LOUNGE_LIST, m_ConnectedLounges);
	DDX_Control(pDX, IDC_BAGGAGEBELT_LIST, m_ConnectedBaggageBelts);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(GatPosGateDlg, GateDlg)
	//{{AFX_MSG_MAP(GatPosGateDlg)
	ON_BN_CLICKED(IDC_LOUNGE, OnLounge)
	ON_BN_CLICKED(IDC_BAGGAGE_BELT, OnBaggageBelt)
	ON_MESSAGE(WM_TABLE_LBUTTONDBLCLK,	OnTableLButtonDblclk)
	ON_BN_CLICKED(IDC_NOAVNEW, OnNoavNew)
	ON_BN_CLICKED(IDC_FCOM, OnCommonGate)
	ON_BN_CLICKED(IDC_GATES, OnGates)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// GatPosGateDlg message handlers

BOOL GatPosGateDlg::OnInitDialog() 
{
	GateDlg::OnInitDialog();
	
	// TODO: Add extra initialization here
	ogBLTData.GetBLTList(omAvailableBaggageBelts);
	ogBasicData.GetBaggageBeltsForGate(this->pomGAT->Urno,omConnectedBaggageBelts);
	for (int i = 0; i < omConnectedBaggageBelts.GetSize(); i++)
	{
		m_ConnectedBaggageBelts.AddString(omConnectedBaggageBelts[i]);
	}

	CWnd *polWnd = GetDlgItem(IDC_BAGGAGE_BELT);
	if (polWnd)
		ogBasicData.SetWindowStat("GATEDLG.m_BELT",polWnd);


	ogWROData.GetWROList(omAvailableLounges);
	ogBasicData.GetWaitingRoomsForGate(this->pomGAT->Urno,omConnectedLounges);
	for (i = 0; i < omConnectedLounges.GetSize(); i++)
	{
		m_ConnectedLounges.AddString(omConnectedLounges[i]);
	}

	polWnd = GetDlgItem(IDC_LOUNGE);
	if (polWnd)
		ogBasicData.SetWindowStat("GATEDLG.m_WAIT",polWnd);


	ogBasicData.AddBlockingImages(m_BlockingBitmap);
	m_BlockingBitmap.SetCurSel(this->pomGAT->Ibit);

	ogBasicData.SetWindowStat("GATEDLG.m_IBIT",&m_BlockingBitmap);

	if(bgCommonGate)
	{
		if ( pomGAT->Fcom[0] == 'X') 
		{
			m_FCOM.SetCheck(1);
		}
		else
		{
			((CWnd*)GetDlgItem(IDC_STATIC_GATES))->ShowWindow(SW_HIDE);
			((CWnd*)GetDlgItem(IDC_GATES))->ShowWindow(SW_HIDE);
			((CWnd*)GetDlgItem(IDC_GATES_LIST))->ShowWindow(SW_HIDE);
		}

		//Get all the Gates
		ogGATData.GetGATList(omAvailableGates);

		//Get all the selected gates
		ogBasicData.GetGatesForGate(this->pomGAT->Urno,omConnectedGates);
		for (i = 0; i < omConnectedGates.GetSize(); i++)
		{
			m_ConnectedGates.AddString(omConnectedGates[i]);
		}


	}
	else
	{
		((CWnd*)GetDlgItem(IDC_FCOM))->ShowWindow(SW_HIDE);
		((CWnd*)GetDlgItem(IDC_STATIC_GATES))->ShowWindow(SW_HIDE);
		((CWnd*)GetDlgItem(IDC_GATES))->ShowWindow(SW_HIDE);
		((CWnd*)GetDlgItem(IDC_GATES_LIST))->ShowWindow(SW_HIDE);
	}

	
	//clStat = ogPrivList.GetStat("GATEDLG.m_FCOM");
	//SetWndStatAll(clStat,m_FCOM);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void GatPosGateDlg::OnBaggageBelt() 
{
	// TODO: Add your control notification handler code here
	SelectionDlg olDlg(this);
	CString olCaption;
	olCaption.Format(LoadStg(IDS_STRING900),this->pomGAT->Gnam);
	olDlg.SetCaption(olCaption);
	olDlg.SetPossibleList(LoadStg(IDS_STRING901),omAvailableBaggageBelts);
	olDlg.SetSelectionList(LoadStg(IDS_STRING902),omConnectedBaggageBelts);
	if (olDlg.DoModal() == IDOK)
	{
		omConnectedBaggageBelts.RemoveAll();
		olDlg.GetSelectionList(omConnectedBaggageBelts);
		m_ConnectedBaggageBelts.ResetContent();
		for (int i = 0; i < omConnectedBaggageBelts.GetSize(); i++)
		{
			m_ConnectedBaggageBelts.AddString(omConnectedBaggageBelts[i]);
		}
	}
	
}

void GatPosGateDlg::OnLounge() 
{
	// TODO: Add your control notification handler code here
	SelectionDlg olDlg(this);
	CString olCaption;
	olCaption.Format(LoadStg(IDS_STRING903),this->pomGAT->Gnam);
	olDlg.SetCaption(olCaption);
	olDlg.SetPossibleList(LoadStg(IDS_STRING904),omAvailableLounges);
	olDlg.SetSelectionList(LoadStg(IDS_STRING905),omConnectedLounges);
	if (olDlg.DoModal() == IDOK)
	{
		omConnectedLounges.RemoveAll();
		olDlg.GetSelectionList(omConnectedLounges);
		m_ConnectedLounges.ResetContent();
		for (int i = 0; i < omConnectedLounges.GetSize(); i++)
		{
			m_ConnectedLounges.AddString(omConnectedLounges[i]);
		}
	}
	
}

int GatPosGateDlg::GetConnectedBaggageBelts(CStringArray& ropBaggageBelts)
{
	ropBaggageBelts.Append(omConnectedBaggageBelts);
	return ropBaggageBelts.GetSize();
}

int GatPosGateDlg::GetConnectedWaitingRooms(CStringArray& ropLounges)
{
	ropLounges.Append(omConnectedLounges);
	return ropLounges.GetSize();
}

void GatPosGateDlg::OnOK() 
{
	// TODO: Add extra validation here
	this->pomGAT->Ibit = m_BlockingBitmap.GetCurSel();

	if(bgCommonGate)
	{
		(m_FCOM.GetCheck() == 1) ? pomGAT->Fcom[0] = 'X' : pomGAT->Fcom[0] = ' ';
	}
	
	if(m_FCOM.GetCheck() != 1)
	{
		omConnectedGates.RemoveAll();
	}
	
	GateDlg::OnOK();
}

//----------------------------------------------------------------------------------------
LONG GatPosGateDlg::OnTableLButtonDblclk(UINT wParam, LONG lParam) 
{
	if(ogPrivList.GetStat("GATEDLG.m_NOAVCHA") == '1')
	{
		int ilLineNo = pomTable->GetCurSel();
		if (ilLineNo == -1)
		{
			MessageBox(LoadStg(IDS_STRING148),LoadStg(IDS_STRING149),MB_ICONSTOP);
		}
		else
		{
			BLKDATA *prlBlk = &omBlkPtrA[ilLineNo];
			if (prlBlk != NULL)
			{
				BLKDATA rlBlk = *prlBlk;
				GatPosNotAvailableDlg *polDlg = new GatPosNotAvailableDlg(&rlBlk,this);
				m_GNAM.GetWindowText(polDlg->omName);
				if(polDlg->DoModal() == IDOK)
				{
					*prlBlk = rlBlk;
					if(prlBlk->IsChanged != DATA_NEW) prlBlk->IsChanged = DATA_CHANGED;
					ChangeNoavTable(prlBlk, ilLineNo);
				}
				delete polDlg;
			}
		}
	}
	return 0L;
}

//----------------------------------------------------------------------------------------

void GatPosGateDlg::OnNoavNew() 
{
	
	BLKDATA rlBlk;

	if(ogPrivList.GetStat("GATEDLG.m_NOAVNEW") == '1')
	{
		GatPosNotAvailableDlg *polDlg = new GatPosNotAvailableDlg(&rlBlk,this);
		m_GNAM.GetWindowText(polDlg->omName);
		if(polDlg->DoModal() == IDOK)
		{
			BLKDATA *prlBlk = new BLKDATA;
			*prlBlk = rlBlk;
			omBlkPtrA.Add(prlBlk);
			prlBlk->IsChanged = DATA_NEW;
			ChangeNoavTable(prlBlk, -1);
		}
		delete polDlg;
	}
}

void GatPosGateDlg::OnCommonGate() 
{
	if(bgCommonGate)
	{
		int ilShow;
		if(m_FCOM.GetCheck() == 1)
		{
			ilShow = SW_SHOWNORMAL;;
		}
		else
		{
			ilShow = SW_HIDE;
		}

		((CWnd*)GetDlgItem(IDC_STATIC_GATES))->ShowWindow(ilShow);
		((CWnd*)GetDlgItem(IDC_GATES_LIST))->ShowWindow(ilShow);
		((CWnd*)GetDlgItem(IDC_GATES))->ShowWindow(ilShow);
	}
	
}

/*
Ufis-1320
Gates - new field for define gates which share the same common gate
*/
void GatPosGateDlg::OnGates() 
{
	SelectionDlg olDlg(this);
	CString olCaption;
	olCaption.Format(LoadStg(IDS_STRING148),this->pomGAT->Gnam);
	olDlg.SetCaption(olCaption);

	olDlg.SetPossibleList(LoadStg(IDS_STRING907),omAvailableGates);
	olDlg.SetSelectionList(LoadStg(IDS_STRING908),omConnectedGates);
	if(olDlg.DoModal() == IDOK)
	{
		omConnectedGates.RemoveAll();
		olDlg.GetSelectionList(omConnectedGates);
		for (int i = 0; i < omConnectedGates.GetSize(); i++)
		{
			m_ConnectedGates.AddString(omConnectedGates[i]);
		}
	}
	
}

int GatPosGateDlg::GetConnectedGates(CStringArray& ropGates)
{
	ropGates.Append(omConnectedGates);
	return omConnectedGates.GetSize();
}