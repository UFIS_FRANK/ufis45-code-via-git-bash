// QualifikationenTableViewer.cpp 
//

#include <stdafx.h>
#include <QualifikationenTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void QualifikationenTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// QualifikationenTableViewer
//

QualifikationenTableViewer::QualifikationenTableViewer(CCSPtrArray<PERDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomQualifikationenTable = NULL;
    ogDdx.Register(this, PER_CHANGE, CString("QUALIFIKATIONENTABLEVIEWER"), CString("Qualifikationen Update"), QualifikationenTableCf);
    ogDdx.Register(this, PER_NEW,    CString("QUALIFIKATIONENTABLEVIEWER"), CString("Qualifikationen New"),    QualifikationenTableCf);
    ogDdx.Register(this, PER_DELETE, CString("QUALIFIKATIONENTABLEVIEWER"), CString("Qualifikationen Delete"), QualifikationenTableCf);
}

//-----------------------------------------------------------------------------------------------

QualifikationenTableViewer::~QualifikationenTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void QualifikationenTableViewer::Attach(CCSTable *popTable)
{
    pomQualifikationenTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void QualifikationenTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void QualifikationenTableViewer::MakeLines()
{
	int ilQualifikationenCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilQualifikationenCount; ilLc++)
	{
		PERDATA *prlQualifikationenData = &pomData->GetAt(ilLc);
		MakeLine(prlQualifikationenData);
	}
}

//-----------------------------------------------------------------------------------------------

void QualifikationenTableViewer::MakeLine(PERDATA *prpQualifikationen)
{

    //if( !IsPassFilter(prpQualifikationen)) return;

    // Update viewer data for this shift record
    QUALIFIKATIONENTABLE_LINEDATA rlQualifikationen;

	rlQualifikationen.Prio = prpQualifikationen->Prio; 
	rlQualifikationen.Prmc = prpQualifikationen->Prmc; 
	rlQualifikationen.Prmn = prpQualifikationen->Prmn; 
	rlQualifikationen.Rema = prpQualifikationen->Rema; 
	rlQualifikationen.Urno = prpQualifikationen->Urno; 

	CreateLine(&rlQualifikationen);
}

//-----------------------------------------------------------------------------------------------

void QualifikationenTableViewer::CreateLine(QUALIFIKATIONENTABLE_LINEDATA *prpQualifikationen)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareQualifikationen(prpQualifikationen, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	QUALIFIKATIONENTABLE_LINEDATA rlQualifikationen;
	rlQualifikationen = *prpQualifikationen;
    omLines.NewAt(ilLineno, rlQualifikationen);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void QualifikationenTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 3;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomQualifikationenTable->SetShowSelection(TRUE);
	pomQualifikationenTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 80; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING278),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 332; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING278),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 80; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING278),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 498; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING278),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomQualifikationenTable->SetHeaderFields(omHeaderDataArray);

	pomQualifikationenTable->SetDefaultSeparator();
	pomQualifikationenTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Prmc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Prmn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Prio;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Rema;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomQualifikationenTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomQualifikationenTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString QualifikationenTableViewer::Format(QUALIFIKATIONENTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool QualifikationenTableViewer::FindQualifikationen(char *pcpQualifikationenKeya, char *pcpQualifikationenKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpQualifikationenKeya) &&
			 (omLines[ilItem].Keyd == pcpQualifikationenKeyd)    )
			return TRUE;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void QualifikationenTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    QualifikationenTableViewer *polViewer = (QualifikationenTableViewer *)popInstance;
    if (ipDDXType == PER_CHANGE || ipDDXType == PER_NEW) polViewer->ProcessQualifikationenChange((PERDATA *)vpDataPointer);
    if (ipDDXType == PER_DELETE) polViewer->ProcessQualifikationenDelete((PERDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void QualifikationenTableViewer::ProcessQualifikationenChange(PERDATA *prpQualifikationen)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpQualifikationen->Prmc;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpQualifikationen->Prmn;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpQualifikationen->Prio;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpQualifikationen->Rema;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	if (FindLine(prpQualifikationen->Urno, ilItem))
	{
        QUALIFIKATIONENTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Prio = prpQualifikationen->Prio;
		prlLine->Prmc = prpQualifikationen->Prmc;
		prlLine->Prmn = prpQualifikationen->Prmn;
		prlLine->Rema = prpQualifikationen->Rema;
		prlLine->Urno = prpQualifikationen->Urno;

		pomQualifikationenTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomQualifikationenTable->DisplayTable();
	}
	else
	{
		MakeLine(prpQualifikationen);
		if (FindLine(prpQualifikationen->Urno, ilItem))
		{
	        QUALIFIKATIONENTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomQualifikationenTable->AddTextLine(olLine, (void *)prlLine);
				pomQualifikationenTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void QualifikationenTableViewer::ProcessQualifikationenDelete(PERDATA *prpQualifikationen)
{
	int ilItem;
	if (FindLine(prpQualifikationen->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomQualifikationenTable->DeleteTextLine(ilItem);
		pomQualifikationenTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool QualifikationenTableViewer::IsPassFilter(PERDATA *prpQualifikationen)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int QualifikationenTableViewer::CompareQualifikationen(QUALIFIKATIONENTABLE_LINEDATA *prpQualifikationen1, QUALIFIKATIONENTABLE_LINEDATA *prpQualifikationen2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpQualifikationen1->Tifd == prpQualifikationen2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpQualifikationen1->Tifd > prpQualifikationen2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void QualifikationenTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool QualifikationenTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void QualifikationenTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void QualifikationenTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING186);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool QualifikationenTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool QualifikationenTableViewer::PrintTableLine(QUALIFIKATIONENTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
			case 0:
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Prmc;
				break;
			case 1:
				rlElement.Text		= prpLine->Prmn;
				break;
			case 2:
				rlElement.Text		= prpLine->Prio;
				break;
			case 3:
				rlElement.FrameRight = PRINT_FRAMETHIN;
				rlElement.Text		= prpLine->Rema;
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
