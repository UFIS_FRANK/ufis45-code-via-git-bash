#if !defined(AFX_SORDLG_H__4DEC38F5_03B0_11D4_9007_0050DA1CAD13__INCLUDED_)
#define AFX_SORDLG_H__4DEC38F5_03B0_11D4_9007_0050DA1CAD13__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SorDlg.h : Header-Datei
//
#include <resrc1.h>		// main symbols#
#include <basicdata.h>

/////////////////////////////////////////////////////////////////////////////
// Dialogfeld CSorDlg 

class CSorDlg : public CDialog
{
// Konstruktion
public:
	CSorDlg(CWnd* pParent = NULL);   // Standardkonstruktor

	CSorDlg(CString Code, CString Odgc, CString Lead, CWnd* pParent = NULL);   // Standardkonstruktor


	CString omCode;
	CString omOdgc;
	CString omLead;

// Dialogfelddaten
	//{{AFX_DATA(CSorDlg)
	enum { IDD = IDD_SOR_DLG };
	CComboBox	m_CB_COT;
	CComboBox	m_CB_SOR;
	CButton	m_ctrl_Lead;
	CString	m_CODE;
	CString	m_ODGC;
	CString	m_LEAD;
	//}}AFX_DATA
	CString m_SURN;


// Überschreibungen
	// Vom Klassen-Assistenten generierte virtuelle Funktionsüberschreibungen
	//{{AFX_VIRTUAL(CSorDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterstützung
	//}}AFX_VIRTUAL

// Implementierung
protected:

	// Generierte Nachrichtenzuordnungsfunktionen
	//{{AFX_MSG(CSorDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnSelchangeCombo1();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CString pomSURN;
	int pomOrgID;
	int ReadORGData();
	void InitCombo1();
	void InitCombo2();
	int ReadODGData(CString opOrgu);
	bool SaveSORData();
	void ReadSORData();
	void SetSORData();
	CStringArray	*pomColumn2;
	CStringArray	*pomColumn1;
	CStringArray	*pomUrnos;
	CString			pomSORUrno;
	CString			omHeader1;
	CString			omHeader2;
	int				im_S_UrnoPos;
	int				im_S_SurnPos;
	int				im_S_CodePos;
	int				im_S_VpfrPos;
	int				im_S_VptoPos;
	int				im_S_HopoPos;
	int				im_S_OdgcPos;
	int				im_S_LeadPos;
	int				im_O_UrnoPos;
	int				im_O_HopoPos;
	int				im_O_OdgcPos;
	int				im_O_OdgnPos;
	int				im_O_OrguPos;
	int				im_O_RemaPos;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ fügt unmittelbar vor der vorhergehenden Zeile zusätzliche Deklarationen ein.

#endif // AFX_SORDLG_H__4DEC38F5_03B0_11D4_9007_0050DA1CAD13__INCLUDED_
