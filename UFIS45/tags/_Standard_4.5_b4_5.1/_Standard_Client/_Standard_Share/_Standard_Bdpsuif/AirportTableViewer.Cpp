// AirportTableViewer.cpp 
//

#include <stdafx.h>
#include <AirportTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void AirportTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// AirportTableViewer
//

AirportTableViewer::AirportTableViewer(CCSPtrArray<APTDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomAirportTable = NULL;
    ogDdx.Register(this, APT_CHANGE, CString("AIRPORTTABLEVIEWER"), CString("Airport Update/new"), AirportTableCf);
    ogDdx.Register(this, APT_DELETE, CString("AIRPORTTABLEVIEWER"), CString("Airport Delete"), AirportTableCf);
}

//-----------------------------------------------------------------------------------------------

AirportTableViewer::~AirportTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();

}

//-----------------------------------------------------------------------------------------------

void AirportTableViewer::Attach(CCSTable *popTable)
{
    pomAirportTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void AirportTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void AirportTableViewer::MakeLines()
{
	int ilAirportCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilAirportCount; ilLc++)
	{
		APTDATA *prlAirportData = &pomData->GetAt(ilLc);
		MakeLine(prlAirportData);
	}
}

//-----------------------------------------------------------------------------------------------

void AirportTableViewer::MakeLine(APTDATA *prpAirport)
{

    //if( !IsPassFilter(prpAirport)) return;

    // Update viewer data for this shift record
    AIRPORTTABLE_LINEDATA rlAirport;
	rlAirport.Urno = prpAirport->Urno; 
	rlAirport.Apc3 = prpAirport->Apc3; 
	rlAirport.Apc4 = prpAirport->Apc4; 
	rlAirport.Apfn = prpAirport->Apfn; 
	rlAirport.Apsn = prpAirport->Apsn; 
	rlAirport.Land = prpAirport->Land; 
	rlAirport.Aptt = prpAirport->Aptt; 
	rlAirport.Etof = prpAirport->Etof; 
	rlAirport.Tich = prpAirport->Tich.Format("%d.%m.%Y %H:%M"); 
	rlAirport.Tdi1 = prpAirport->Tdi1;   
	rlAirport.Tdi2 = prpAirport->Tdi2; 
	rlAirport.Vafr = prpAirport->Vafr.Format("%d.%m.%Y %H:%M"); 
	rlAirport.Vato = prpAirport->Vato.Format("%d.%m.%Y %H:%M"); 
	//rlAirport.Home = prpAirport->Home; 
	CreateLine(&rlAirport);
}

//-----------------------------------------------------------------------------------------------

void AirportTableViewer::CreateLine(AIRPORTTABLE_LINEDATA *prpAirport)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareAirport(prpAirport, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	AIRPORTTABLE_LINEDATA rlAirport;
	rlAirport = *prpAirport;
    omLines.NewAt(ilLineno, rlAirport);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void AirportTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 11;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomAirportTable->SetShowSelection(TRUE);
	pomAirportTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	//CString olHeaderString;
	int iStrID;
	if(bgUnicode)
		iStrID = IDS_STRING32803;
	else
		iStrID = IDS_STRING253;	
	
	int ilPos = 1;
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(iStrID),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 33; 
	rlHeader.Text = GetListItem(LoadStg(iStrID),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 266; 
	rlHeader.Text = GetListItem(LoadStg(iStrID),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	if(!bgUnicode)
	{
	rlHeader.Length = 166; 
		rlHeader.Text = GetListItem(LoadStg(iStrID),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	}
	rlHeader.Length = 17; 
	rlHeader.Text = GetListItem(LoadStg(iStrID),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8; 
	rlHeader.Text = GetListItem(LoadStg(iStrID),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 33; 
	rlHeader.Text = GetListItem(LoadStg(iStrID),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(iStrID),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 33; 
	rlHeader.Text = GetListItem(LoadStg(iStrID),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 33; 
	rlHeader.Text = GetListItem(LoadStg(iStrID),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(iStrID),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 133; 
	rlHeader.Text = GetListItem(LoadStg(iStrID),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	//rlHeader.Length = 33; 
	//rlHeader.Text = GetListItem(LoadStg(IDS_STRING253),ilPos++,true,'|');
	//omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomAirportTable->SetHeaderFields(omHeaderDataArray);

	pomAirportTable->SetDefaultSeparator();
	pomAirportTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;
		rlColumnData.Text = omLines[ilLineNo].Apc3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Apc4;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Apfn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		if(!bgUnicode)
		{
		rlColumnData.Text = omLines[ilLineNo].Apsn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		}
		rlColumnData.Text = omLines[ilLineNo].Land;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Aptt;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Alignment = COLALIGN_RIGHT;
		rlColumnData.Text = omLines[ilLineNo].Etof;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = omLines[ilLineNo].Tich;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Alignment = COLALIGN_RIGHT;
		rlColumnData.Text = omLines[ilLineNo].Tdi1;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Tdi2;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Alignment = COLALIGN_LEFT;
		rlColumnData.Text = omLines[ilLineNo].Vafr;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Vato;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		//rlColumnData.Text = omLines[ilLineNo].Home;
		//olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomAirportTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomAirportTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString AirportTableViewer::Format(AIRPORTTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

BOOL AirportTableViewer::FindAirport(char *pcpAirportKeya, char *pcpAirportKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpAirportKeya) &&
			 (omLines[ilItem].Keyd == pcpAirportKeyd)    )
			return TRUE;*/
	}
	return FALSE;
}

//-----------------------------------------------------------------------------------------------

static void AirportTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    AirportTableViewer *polViewer = (AirportTableViewer *)popInstance;
    if (ipDDXType == APT_CHANGE) polViewer->ProcessAirportChange((APTDATA *)vpDataPointer);
    if (ipDDXType == APT_DELETE) polViewer->ProcessAirportDelete((APTDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void AirportTableViewer::ProcessAirportChange(APTDATA *prpAirport)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpAirport->Apc3;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpAirport->Apc4;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpAirport->Apfn;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	if(!bgUnicode)
	{
	rlColumn.Text = prpAirport->Apsn;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	}
	rlColumn.Text = prpAirport->Land;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpAirport->Aptt;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Alignment = COLALIGN_RIGHT;
	rlColumn.Text = prpAirport->Etof;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Alignment = COLALIGN_LEFT;
	rlColumn.Text = prpAirport->Tich.Format("%d.%m.%Y %H:%M");
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Alignment = COLALIGN_RIGHT;
	rlColumn.Text = prpAirport->Tdi1;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpAirport->Tdi2;
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Alignment = COLALIGN_LEFT;
	rlColumn.Text = prpAirport->Vafr.Format("%d.%m.%Y %H:%M");
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpAirport->Vato.Format("%d.%m.%Y %H:%M");
	rlColumn.Columnno = ilColumnNo++;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	//rlColumn.Text = prpAirport->Home;
	//rlColumn.Columnno = ilColumnNo++;
	//olLine.NewAt(olLine.GetSize(), rlColumn);

	if (FindLine(prpAirport->Urno, ilItem))
	{
        AIRPORTTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpAirport->Urno;
		prlLine->Apc3 = prpAirport->Apc3;
		prlLine->Apc4 = prpAirport->Apc4;
		prlLine->Apfn = prpAirport->Apfn;
		prlLine->Apsn = prpAirport->Apsn;
		prlLine->Aptt = prpAirport->Aptt;
		prlLine->Land = prpAirport->Land;
		prlLine->Etof = prpAirport->Etof;
		prlLine->Tich = prpAirport->Tich.Format("%d.%m.%Y %H:%M");
		prlLine->Tdi1 = prpAirport->Tdi1;
		prlLine->Tdi2 = prpAirport->Tdi2;
		prlLine->Home = prpAirport->Home;
		prlLine->Vafr = prpAirport->Vafr.Format("%d.%m.%Y %H:%M");
		prlLine->Vato = prpAirport->Vato.Format("%d.%m.%Y %H:%M");
	
		pomAirportTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomAirportTable->DisplayTable();
	}
	else
	{
		MakeLine(prpAirport);
		if (FindLine(prpAirport->Urno, ilItem))
		{
	        AIRPORTTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomAirportTable->AddTextLine(olLine, (void *)prlLine);
				pomAirportTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void AirportTableViewer::ProcessAirportDelete(APTDATA *prpAirport)
{
	int ilItem;
	if (FindLine(prpAirport->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomAirportTable->DeleteTextLine(ilItem);
		pomAirportTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

BOOL AirportTableViewer::IsPassFilter(APTDATA *prpAirport)
{

	return TRUE;
}

//-----------------------------------------------------------------------------------------------

int AirportTableViewer::CompareAirport(AIRPORTTABLE_LINEDATA *prpAirport1, AIRPORTTABLE_LINEDATA *prpAirport2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpAirport1->Tifd == prpAirport2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpAirport1->Tifd > prpAirport2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void AirportTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

BOOL AirportTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return TRUE;
	}
    return FALSE;
}
//-----------------------------------------------------------------------------------------------

void AirportTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}


//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void AirportTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING174);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool AirportTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool AirportTableViewer::PrintTableLine(AIRPORTTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		if(i!=12)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
			case 0:
				{
					rlElement.FrameLeft  = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Apc3;
				}
				break;
			case 1:
				{
					rlElement.Text		= prpLine->Apc4;
				}
				break;
			case 2:
				{
					rlElement.Text		= prpLine->Apfn;
				}
				break;
			case 3:
				{
					rlElement.Text		= prpLine->Apsn;
				}
				break;
			case 4:
				{
					rlElement.Text		= prpLine->Land;
				}
				break;
			case 5:
				{
					rlElement.Text		= prpLine->Aptt;
				}
				break;
			case 6:
				{
					rlElement.Alignment = PRINT_RIGHT;
					rlElement.Text		= prpLine->Etof;
				}
				break;
			case 7:
				{
					rlElement.Text		= prpLine->Tich;
				}
				break;
			case 8:
				{
					rlElement.Alignment = PRINT_RIGHT;
					rlElement.Text		= prpLine->Tdi1;
				}
				break;
			case 9:
				{
					rlElement.Alignment = PRINT_RIGHT;
					rlElement.Text		= prpLine->Tdi2;
				}
				break;
			case 10:
				{
					rlElement.Text		= prpLine->Vafr;
				}
				break;
			case 11:
				{
					rlElement.FrameRight = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Vato;
				}
				break;
			//case 12:
				//{
					//rlElement.Alignment = PRINT_RIGHT;
					//rlElement.Text		= prpLine->Home;
				//}
				//break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------











void AirportTableViewer::SetSelectedContinent(CString urno)
{
	m_ContUrno = urno;
}
