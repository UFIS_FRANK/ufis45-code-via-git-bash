#ifndef __COHTABLEVIEWER_H__
#define __COHTABLEVIEWER_H__

#include <stdafx.h>
#include <CedaCohData.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct COHTABLE_LINEDATA
{
	long 	Urno;
	CString	Ctrc;
	CString Fage;
	CString	Hold;
	CTime	Cdat;
	CTime	Lstu;
	CString	Usec;
	CString	Useu;
};

/////////////////////////////////////////////////////////////////////////////
// CohTableViewer

class CohTableViewer : public CViewer
{
// Constructions
public:
    CohTableViewer(CCSPtrArray<COHDATA> *popData);
    ~CohTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(COHDATA *prpCoh);
	int CompareCoh(COHTABLE_LINEDATA *prpCoh1, COHTABLE_LINEDATA *prpCoh2);
    void MakeLines();
	void MakeLine(COHDATA *prpCoh);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(COHTABLE_LINEDATA *prpCoh);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(COHTABLE_LINEDATA *prpLine);
	void ProcessCohChange(COHDATA *prpCoh);
	void ProcessCohDelete(COHDATA *prpCoh);
	bool FindCoh(char *prpCohKeya, char *prpCohKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomCohTable;
	CCSPtrArray<COHDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<COHTABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(COHTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__COHTABLEVIEWER_H__
