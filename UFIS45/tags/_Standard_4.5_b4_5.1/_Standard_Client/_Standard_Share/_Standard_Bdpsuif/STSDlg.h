#if !defined(AFX_STSDLG_H__2FE447E6_E6DD_11D4_BFF8_00D0B7E2A4B5__INCLUDED_)
#define AFX_STSDLG_H__2FE447E6_E6DD_11D4_BFF8_00D0B7E2A4B5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// STSDlg.h : header file
//
#include <PrivList.h>
#include <CedaSTSData.h>
#include <CCSEdit.h>

/////////////////////////////////////////////////////////////////////////////
// CSTSDlg dialog

class CSTSDlg : public CDialog
{
// Construction
public:
	CSTSDlg(STSDATA *popSts, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSTSDlg)
	enum { IDD = IDD_STS };
	CButton	m_OK;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_STSC;
	CCSEdit	m_STSD;
	CCSEdit	m_REMA;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEC;
	CCSEdit	m_USEU;
	CString m_Caption;
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSTSDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

private:
	STSDATA *pomSts;
// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSTSDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STSDLG_H__2FE447E6_E6DD_11D4_BFF8_00D0B7E2A4B5__INCLUDED_)
