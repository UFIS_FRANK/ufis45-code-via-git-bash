#if !defined(AFX_RUNWAYDLG_H__56BF0B43_2049_11D1_B38A_0000C016B067__INCLUDED_)
#define AFX_RUNWAYDLG_H__56BF0B43_2049_11D1_B38A_0000C016B067__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// RunwayDlg.h : header file
//
#include <CedaRWYData.h>
#include <CCSEdit.h>
/////////////////////////////////////////////////////////////////////////////
// RunwayDlg dialog

class RunwayDlg : public CDialog
{
// Construction
public:
	RunwayDlg(RWYDATA *popRWY,CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(RunwayDlg)
	enum { IDD = IDD_RUNWAYDLG };
	CButton	m_OK;
	CButton	m_RTYPS;
	CButton	m_RTYPL;
	CString	m_Caption;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_NAFRD;
	CCSEdit	m_NAFRT;
	CCSEdit	m_NATOD;
	CCSEdit	m_NATOT;
	CCSEdit	m_USEU;
	CCSEdit	m_USEC;
	CCSEdit	m_RESN;
	CCSEdit	m_RNAM;
	CCSEdit	m_RNUM;
	CCSEdit	m_VAFRD;
	CCSEdit	m_VAFRT;
	CCSEdit	m_VATOD;
	CCSEdit	m_VATOT;
	CCSEdit	m_HOME;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(RunwayDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(RunwayDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

	RWYDATA *pomRWY;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RUNWAYDLG_H__56BF0B43_2049_11D1_B38A_0000C016B067__INCLUDED_)
