// ErgVerkehrsartenTableViewer.cpp 
//

#include <stdafx.h>
#include <ErgVerkehrsartenTableViewer.h>
#include <CcsDdx.h>
#include <CedaGrnData.h>
#include <resource.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void ErgVerkehrsartenTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// ErgVerkehrsartenTableViewer
//

ErgVerkehrsartenTableViewer::ErgVerkehrsartenTableViewer(CCSPtrArray<SPHDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomErgVerkehrsartenTable = NULL;
    ogDdx.Register(this, SPH_NEW,	 CString("ERGVERKEHRSARTENTABLEVIEWER"), CString("ErgVerkehrsarten New"),	 ErgVerkehrsartenTableCf);
    ogDdx.Register(this, SPH_CHANGE, CString("ERGVERKEHRSARTENTABLEVIEWER"), CString("ErgVerkehrsarten Update"), ErgVerkehrsartenTableCf);
    ogDdx.Register(this, SPH_DELETE, CString("ERGVERKEHRSARTENTABLEVIEWER"), CString("ErgVerkehrsarten Delete"), ErgVerkehrsartenTableCf);
}

//-----------------------------------------------------------------------------------------------

ErgVerkehrsartenTableViewer::~ErgVerkehrsartenTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void ErgVerkehrsartenTableViewer::Attach(CCSTable *popTable)
{
    pomErgVerkehrsartenTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void ErgVerkehrsartenTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void ErgVerkehrsartenTableViewer::MakeLines()
{
	int ilErgVerkehrsartenCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilErgVerkehrsartenCount; ilLc++)
	{
		SPHDATA *prlErgVerkehrsartenData = &pomData->GetAt(ilLc);
		MakeLine(prlErgVerkehrsartenData);
	}
}

//-----------------------------------------------------------------------------------------------

void ErgVerkehrsartenTableViewer::MakeLine(SPHDATA *prpErgVerkehrsarten)
{

    //if( !IsPassFilter(prpErgVerkehrsarten)) return;

    // Update viewer data for this shift record
    ERGVERKEHRSARTENTABLE_LINEDATA rlErgVerkehrsarten;

	rlErgVerkehrsarten.Act3 = prpErgVerkehrsarten->Act3; 
	rlErgVerkehrsarten.Apc3 = prpErgVerkehrsarten->Apc3; 
	rlErgVerkehrsarten.Arde = prpErgVerkehrsarten->Arde; 
 	rlErgVerkehrsarten.Days = prpErgVerkehrsarten->Days; 
 	rlErgVerkehrsarten.Flnc = prpErgVerkehrsarten->Flnc; 
	rlErgVerkehrsarten.Flnn = prpErgVerkehrsarten->Flnn; 
	rlErgVerkehrsarten.Flns = prpErgVerkehrsarten->Flns; 
 	rlErgVerkehrsarten.Regn = prpErgVerkehrsarten->Regn; 
 	rlErgVerkehrsarten.Styp = prpErgVerkehrsarten->Styp; 
	rlErgVerkehrsarten.Ttyp = prpErgVerkehrsarten->Ttyp; 
	rlErgVerkehrsarten.Urno = prpErgVerkehrsarten->Urno;
	
	GRNDATA *prlGrn;
	prlGrn = ogGrnData.GetGrnByUrno(prpErgVerkehrsarten->Actm);
	if(prlGrn != NULL)
	{
		rlErgVerkehrsarten.Actm = prlGrn->Grsn; 
	}
	else
	{
		rlErgVerkehrsarten.Actm = " "; 
	}
	prlGrn = ogGrnData.GetGrnByUrno(prpErgVerkehrsarten->Apcm);
	if(prlGrn != NULL)
	{
		rlErgVerkehrsarten.Apcm = prlGrn->Grsn; 
	}
	else
	{
		rlErgVerkehrsarten.Apcm = " "; 
	}
	prlGrn = ogGrnData.GetGrnByUrno(prpErgVerkehrsarten->Alcm);
	if(prlGrn != NULL)
	{
		rlErgVerkehrsarten.Alcm = prlGrn->Grsn; 
	}
	else
	{
		rlErgVerkehrsarten.Alcm = " "; 
	}


	CreateLine(&rlErgVerkehrsarten);
}

//-----------------------------------------------------------------------------------------------

void ErgVerkehrsartenTableViewer::CreateLine(ERGVERKEHRSARTENTABLE_LINEDATA *prpErgVerkehrsarten)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareErgVerkehrsarten(prpErgVerkehrsarten, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	ERGVERKEHRSARTENTABLE_LINEDATA rlErgVerkehrsarten;
	rlErgVerkehrsarten = *prpErgVerkehrsarten;
    omLines.NewAt(ilLineno, rlErgVerkehrsarten);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void ErgVerkehrsartenTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 10;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomErgVerkehrsartenTable->SetShowSelection(TRUE);
	pomErgVerkehrsartenTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING263),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 75; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING263),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING263),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 100; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING263),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING263),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING263),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING263),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 25; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING263),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 58; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING263),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 17; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING263),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 8; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING263),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);

	pomErgVerkehrsartenTable->SetHeaderFields(omHeaderDataArray);
	pomErgVerkehrsartenTable->SetDefaultSeparator();
	pomErgVerkehrsartenTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;
		rlColumnData.Text = omLines[ilLineNo].Ttyp;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = CString(omLines[ilLineNo].Flnc) + CString(omLines[ilLineNo].Flnn) + CString(omLines[ilLineNo].Flns);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Alcm;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Regn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Act3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Actm;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Apc3;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Apcm;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Days;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Styp;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Arde;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomErgVerkehrsartenTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomErgVerkehrsartenTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString ErgVerkehrsartenTableViewer::Format(ERGVERKEHRSARTENTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool ErgVerkehrsartenTableViewer::FindErgVerkehrsarten(char *pcpErgVerkehrsartenKeya, char *pcpErgVerkehrsartenKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpErgVerkehrsartenKeya) &&
			 (omLines[ilItem].Keyd == pcpErgVerkehrsartenKeyd)    )
			return true;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void ErgVerkehrsartenTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
   	ErgVerkehrsartenTableViewer *polViewer = (ErgVerkehrsartenTableViewer *)popInstance;
	SPHDATA* polData =(SPHDATA *)vpDataPointer;
	if(*polData->Eart == 'V')
	{
		if (ipDDXType == SPH_CHANGE || ipDDXType == SPH_NEW) polViewer->ProcessErgVerkehrsartenChange((SPHDATA *)vpDataPointer);
		if (ipDDXType == SPH_DELETE) polViewer->ProcessErgVerkehrsartenDelete((SPHDATA *)vpDataPointer);

	} 
}

//-----------------------------------------------------------------------------------------------

void ErgVerkehrsartenTableViewer::ProcessErgVerkehrsartenChange(SPHDATA *prpErgVerkehrsarten)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;
	GRNDATA *prlGrn;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpErgVerkehrsarten->Ttyp;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = CString(prpErgVerkehrsarten->Flnc) + CString(prpErgVerkehrsarten->Flnn) + CString(prpErgVerkehrsarten->Flns);
	olLine.NewAt(olLine.GetSize(), rlColumn);

	prlGrn = ogGrnData.GetGrnByUrno(prpErgVerkehrsarten->Alcm);
	if(prlGrn != NULL)
	{
		rlColumn.Text = prlGrn->Grsn;
	}
	else
	{
		rlColumn.Text = " ";
	}
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpErgVerkehrsarten->Regn;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpErgVerkehrsarten->Act3;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	prlGrn = ogGrnData.GetGrnByUrno(prpErgVerkehrsarten->Actm);
	if(prlGrn != NULL)
	{
		rlColumn.Text = prlGrn->Grsn;
	}
	else
	{
		rlColumn.Text = " ";
	}
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpErgVerkehrsarten->Apc3;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	prlGrn = ogGrnData.GetGrnByUrno(prpErgVerkehrsarten->Apcm);
	if(prlGrn != NULL)
	{
		rlColumn.Text = prlGrn->Grsn;
	}
	else
	{
		rlColumn.Text = " ";
	}
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpErgVerkehrsarten->Days;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpErgVerkehrsarten->Styp;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	rlColumn.Text = prpErgVerkehrsarten->Arde;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	if (FindLine(prpErgVerkehrsarten->Urno, ilItem))
	{
        ERGVERKEHRSARTENTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Act3 = prpErgVerkehrsarten->Act3;
		prlLine->Apc3 = prpErgVerkehrsarten->Apc3;
		prlLine->Arde = prpErgVerkehrsarten->Arde;
		prlLine->Days = prpErgVerkehrsarten->Days;
		prlLine->Flnc = prpErgVerkehrsarten->Flnc;
		prlLine->Flnn = prpErgVerkehrsarten->Flnn;
		prlLine->Flns = prpErgVerkehrsarten->Flns;
		prlLine->Regn = prpErgVerkehrsarten->Regn;
		prlLine->Styp = prpErgVerkehrsarten->Styp;
		prlLine->Ttyp = prpErgVerkehrsarten->Ttyp;
		prlLine->Urno = prpErgVerkehrsarten->Urno;

		prlGrn = ogGrnData.GetGrnByUrno(prpErgVerkehrsarten->Actm);
		if(prlGrn != NULL)
		{
			prlLine->Actm = prlGrn->Grsn; 
		}
		else
		{
			prlLine->Actm = " "; 
		}
		prlGrn = ogGrnData.GetGrnByUrno(prpErgVerkehrsarten->Apcm);
		if(prlGrn != NULL)
		{
			prlLine->Apcm = prlGrn->Grsn;
		}
		else
		{
			prlLine->Apcm = " "; 
		}
		prlGrn = ogGrnData.GetGrnByUrno(prpErgVerkehrsarten->Alcm);
		
		if(prlGrn != NULL)
		{
			prlLine->Alcm = prlGrn->Grsn; 
		}
		else
		{
			prlLine->Alcm = " "; 
		}


		pomErgVerkehrsartenTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomErgVerkehrsartenTable->DisplayTable();
	}
	else
	{
		MakeLine(prpErgVerkehrsarten);
		if (FindLine(prpErgVerkehrsarten->Urno, ilItem))
		{
	        ERGVERKEHRSARTENTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomErgVerkehrsartenTable->AddTextLine(olLine, (void *)prlLine);
				pomErgVerkehrsartenTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void ErgVerkehrsartenTableViewer::ProcessErgVerkehrsartenDelete(SPHDATA *prpErgVerkehrsarten)
{
	int ilItem;
	if (FindLine(prpErgVerkehrsarten->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomErgVerkehrsartenTable->DeleteTextLine(ilItem);
		pomErgVerkehrsartenTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool ErgVerkehrsartenTableViewer::IsPassFilter(SPHDATA *prpErgVerkehrsarten)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int ErgVerkehrsartenTableViewer::CompareErgVerkehrsarten(ERGVERKEHRSARTENTABLE_LINEDATA *prpErgVerkehrsarten1, ERGVERKEHRSARTENTABLE_LINEDATA *prpErgVerkehrsarten2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpErgVerkehrsarten1->Tifd == prpErgVerkehrsarten2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpErgVerkehrsarten1->Tifd > prpErgVerkehrsarten2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void ErgVerkehrsartenTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool ErgVerkehrsartenTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void ErgVerkehrsartenTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
        DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void ErgVerkehrsartenTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING170);
	int ilOrientation = PRINT_PORTRAET;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool ErgVerkehrsartenTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool ErgVerkehrsartenTableViewer::PrintTableLine(ERGVERKEHRSARTENTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;
			switch(i)
			{
			case 0:
				{
					rlElement.FrameLeft  = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Ttyp;
				}
				break;
			case 1:
				{
					rlElement.Text		= CString(prpLine->Flnc) + CString(prpLine->Flnn) + CString(prpLine->Flns);
				}
				break;
			case 2:
				{
					rlElement.Text		= prpLine->Alcm;
				}
				break;
			case 3:
				{
					rlElement.Text		= prpLine->Regn;
				}
				break;
			case 4:
				{
					rlElement.Text		= prpLine->Act3;
				}
				break;
			case 5:
				{
					rlElement.Text		= prpLine->Actm;
				}
				break;
			case 6:
				{
					rlElement.Text		= prpLine->Apc3;
				}
				break;
			case 7:
				{
					rlElement.Text		= prpLine->Apcm;
				}
				break;
			case 8:
				{
					rlElement.Text		= prpLine->Days;
				}
				break;
			case 9:
				{
					rlElement.Text		= prpLine->Styp;
				}
				break;
			case 10:
				{
					rlElement.FrameRight = PRINT_FRAMETHIN;
					rlElement.Text		= prpLine->Arde;
				}
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
