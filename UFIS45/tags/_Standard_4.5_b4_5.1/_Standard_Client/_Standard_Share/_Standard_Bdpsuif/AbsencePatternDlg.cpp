// AbsencePatternDlg.cpp : implementation file
//
//VEDA:TEST FOR SVN COMMIT
#include "stdafx.h"
#include "bdpsuif.h"
#include "AbsencePatternDlg.h"
#include "MaaDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// AbsencePatternDlg dialog


AbsencePatternDlg::AbsencePatternDlg(MAWDATA *popMaw, CCSPtrArray<MAADATA> *popMaaData, CWnd* pParent /*=NULL*/)
	: CDialog(AbsencePatternDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(AbsencePatternDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	pomMaw = popMaw;
	pomMaaData = popMaaData;
	pomAbsenceGrid = NULL;
	
}

AbsencePatternDlg::~AbsencePatternDlg()
{
	if(pomAbsenceGrid != NULL)
		delete pomAbsenceGrid;
}


void AbsencePatternDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(AbsencePatternDlg)
	DDX_Control(pDX, IDOK, m_Button_OK);
	DDX_Control(pDX, IDC_BUTTON_NEW, m_Button_New);
	DDX_Control(pDX, IDC_BUTTON_DELETE, m_Button_Delete);
	DDX_Control(pDX, IDC_BUTTON_CHANGE, m_Button_Change);
	DDX_Control(pDX, IDC_EDIT_NAME, m_NAME);
	DDX_Control(pDX, IDC_EDIT_DEFAULT, m_DFLT);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(AbsencePatternDlg, CDialog)
	//{{AFX_MSG_MAP(AbsencePatternDlg)
	ON_BN_CLICKED(IDC_BUTTON_CHANGE, OnButtonChange)
	ON_BN_CLICKED(IDC_BUTTON_DELETE, OnButtonDelete)
	ON_BN_CLICKED(IDC_BUTTON_NEW, OnButtonNew)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// AbsencePatternDlg message handlers

BOOL AbsencePatternDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_Caption = LoadStg(IDS_STRING851) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);

	GetDlgItem(IDC_STATIC_NAME)->SetWindowText(LoadStg(IDS_NAME));
	GetDlgItem(IDC_STATIC_DEFAULT)->SetWindowText(LoadStg(IDS_DEFAULT_VALUE));

	// TODO: Add extra initialization here
	
	m_NAME.SetTextLimit(1,32);
	m_NAME.SetBKColor(YELLOW);
	m_NAME.SetTextErrColor(RED);
	m_NAME.SetInitText(pomMaw->Name);

	//m_DFLT.SetTextLimit(1,5);
	m_DFLT.SetTypeToInt(1, 99999);
	m_DFLT.SetBKColor(YELLOW);
	m_DFLT.SetTextErrColor(RED);
	m_DFLT.SetInitText(pomMaw->Dflt);

	// Grid erzeugen
	pomAbsenceGrid = new CGridControl ( this, IDC_ABS_GRID,ABSCOLCOUNT,0);
	
	// Abwesenheits Grid initialisieren
	InitAbsenceGrid ();

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void AbsencePatternDlg::OnOK() 
{
	// TODO: Add extra validation here
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;

	if(m_NAME.GetStatus() == false)
	{
		ilStatus = false;
		if(m_NAME.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING237) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING237) + ogNotFormat;
		}
	}
	if(m_DFLT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_DFLT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING229) + ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING229) + ogNotFormat;
		}
	}

	CGXStyle style;

	MAADATA *polMaa;

	long ilRowCount, ilUrno; 
	int i; 
	CString olYear, olWeek, olValue;	
		

	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_NAME.GetWindowText(pomMaw->Name,32);
		m_DFLT.GetWindowText(pomMaw->Dflt,5);

		ilRowCount = pomAbsenceGrid->GetRowCount();

		for (i=0; i<ilRowCount; i++){
			olWeek = pomAbsenceGrid->GetValueRowCol(i+1,1);
			olYear = pomAbsenceGrid->GetValueRowCol(i+1,2);
			olValue = pomAbsenceGrid->GetValueRowCol(i+1,3);
			
			pomAbsenceGrid->ComposeStyleRowCol(i+1, 1, &style );
			ilUrno = (long)style.GetItemDataPtr();

			if (ilUrno){
				polMaa = ogMaaData.GetMaaByUrno(ilUrno);
				if(polMaa != NULL)
				{
					polMaa->IsChanged = DATA_CHANGED;
					polMaa->Mawu = pomMaw->Urno;
					strcpy(polMaa->Valu, olValue);
					strcpy(polMaa->Week, olWeek);
					strcpy(polMaa->Year, olYear);
					ogMaaData.Update(polMaa);
				}
				else
				{	
					MAADATA *polMaa = new MAADATA;
					polMaa->Urno = ogBasicData.GetNextUrno();	//new URNO
					polMaa->IsChanged = DATA_NEW;
					polMaa->Mawu = pomMaw->Urno;
					strcpy(polMaa->Valu, olValue);
					strcpy(polMaa->Week, olWeek);
					strcpy(polMaa->Year, olYear);
					ogMaaData.Insert(polMaa);
				}

			}
		}

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_NAME.SetFocus();
	}	
}



void AbsencePatternDlg::OnButtonChange() 
{
	// TODO: Add your control notification handler code here
	CRowColArray	olRowColArray;
	CGXStyle		olStyle;
	long			ilUrno;
	
	// Ausgew�hlt MAA URNO erhalten
	// Selektion aus Grid 
	pomAbsenceGrid->GetSelectedRows (olRowColArray);
	
	// Wurden Zeilen ausgew�hlt
	if (olRowColArray.GetSize() == 0)
	{
		TRACE ("Keine Zeilen ausgew�hlt.\n");
		return;
	}
	
	// Style und damit Datenpointer einer Zelle erhalten
	pomAbsenceGrid->ComposeStyleRowCol(olRowColArray[0],1, &olStyle );
	// Urno erhalten
	ilUrno = (long)olStyle.GetItemDataPtr();
	if (!ilUrno)
	{
		// Keine g�ltige URNO gefunden, L�schen abbrechen
		TRACE ("Urno=0 in TageslisteDlg f�r Zeile%d\n",olRowColArray[0]);
		return;
	}
	
		
	//--------------------------------------------------------------------
	long llMapUrno; 
	MAADATA *prlMaa = 0; 
	bool blFound = false;
	
	for(int i = 0; i < pomMaaData->GetSize(); i++)
	{
		llMapUrno = pomMaaData->GetAt(i).Urno;
		if (llMapUrno == ilUrno){
			blFound = true;
			prlMaa = &pomMaaData->GetAt(i);
			break;
		}
	}

	// Pr�fung
	if (prlMaa == NULL)
	{
		// Keine g�ltige URNO gefunden, L�schen abbrechen
		TRACE ("Urno ung�ltig in TageslisteDlg\n");
		return;
	}
	
	// Dialog starten
	CMaaDialog olDlg(prlMaa, pomMaaData, this);
	if(olDlg.DoModal() == IDOK)
	{
		// Grid neu zeichnen.
		FillAbsenceGrid();
	}
	
}

void AbsencePatternDlg::OnButtonDelete() 
{
	CRowColArray	olRowColArray;
	CGXStyle		olStyle;
	long			ilUrno;
	
	// Selektion aus Grid 
	ROWCOL numcols=pomAbsenceGrid->GetSelectedRows (olRowColArray);
	
	// Wurden Zeilen ausgew�hlt
	if (numcols == 0)
	{
		TRACE ("Keine Zeilen ausgew�hlt.\n");
		return;
	}
	
	for (ROWCOL x=0;x<numcols;x++)  {
		// Style und damit Datenpointer einer Zelle erhalten
		pomAbsenceGrid->ComposeStyleRowCol(olRowColArray[x],1, &olStyle );
		// Urno erhalten
		ilUrno = (long)olStyle.GetItemDataPtr();
		if (!ilUrno)
		{
			// Keine g�ltige URNO gefunden, L�schen abbrechen
			TRACE ("Urno=0 in TageslisteDlg f�r Zeile%d\n",olRowColArray[0]);
			return;
		}
		else
			ogMaaData.Delete(ilUrno);
	}

	
	/*long llMapUrno; 
	
	for(int i = 0; i < pomMaaData->GetSize(); i++)
	{
		llMapUrno = pomMaaData->GetAt(i).Urno;
		if (llMapUrno == ilUrno){
			//pomMaaData->DeleteAt(i);
			ogMaaData.Delete(llMapUrno);
			break;
		}
	}*/

	
	FillAbsenceGrid();
}

void AbsencePatternDlg::OnButtonNew() 
{

	// Dialog starten
	MAADATA *prlMaa = NULL;
	CMaaDialog olDlg(prlMaa, pomMaaData, this);
	if(olDlg.DoModal() == IDOK)
	{
		//Grid neu zeichnen.
		FillAbsenceGrid();
	}
	delete olDlg;
}

void AbsencePatternDlg::InitAbsenceGrid ()
{
	// �berschriften setzen
	pomAbsenceGrid->SetValueRange(CGXRange(0,1), LoadStg(IDS_WEEK));
	pomAbsenceGrid->SetValueRange(CGXRange(0,2), LoadStg(IDS_YEAR));
	pomAbsenceGrid->SetValueRange(CGXRange(0,3), LoadStg(IDS_DEFAULT_VALUE));
	
	// Breite der Spalten einstellen
	InitAbsenceColWidths ();
	// Grid mit Daten f�llen.
	FillAbsenceGrid();
	
	pomAbsenceGrid->Initialize();
	pomAbsenceGrid->GetParam()->EnableUndo(FALSE);
	pomAbsenceGrid->LockUpdate(TRUE);
	pomAbsenceGrid->LockUpdate(FALSE);
	pomAbsenceGrid->GetParam()->EnableUndo(TRUE);
	pomAbsenceGrid->SetScrollBarMode(SB_VERT,gxnAutomatic);	
		
	// Verhindert, das die Spaltenbreite ver�ndert wird
	pomAbsenceGrid->GetParam()->EnableTrackColWidth(FALSE);
	// Es k�nnen nur ganze Zeilen markiert werden.
	//	pomDevGrid->GetParam()->EnableSelection(GX_SELROW | GX_SELMULTIPLE | GX_SELSHIFT);
	pomAbsenceGrid->GetParam()->EnableSelection(GX_SELROW);
	
	pomAbsenceGrid->SetStyleRange( CGXRange().SetTable(), CGXStyle()
		.SetVerticalAlignment(DT_VCENTER)
		.SetReadOnly(TRUE)
		//				.SetEnabled(FALSE)
		//				.SetControl(GX_IDS_CTRL_STATIC)
		);
	pomAbsenceGrid->EnableAutoGrow ( false );
	pomAbsenceGrid->SetLbDblClickAction ( WM_COMMAND, IDC_BUTTON_CHANGE);
}

void AbsencePatternDlg::InitAbsenceColWidths ()
{
	pomAbsenceGrid->SetColWidth ( 0, 0,  25);
	pomAbsenceGrid->SetColWidth ( 1, 1,  90);
	pomAbsenceGrid->SetColWidth ( 2, 2,  90);
	pomAbsenceGrid->SetColWidth ( 3, 3, 120);
}


//**********************************************************************************
// Grid mit Daten f�llen
//**********************************************************************************

void AbsencePatternDlg::FillAbsenceGrid()
{
	ROWCOL ilRowCount;
	BOOL blRet;

	// Readonly ausschalten
	pomAbsenceGrid->GetParam()->SetLockReadOnly(false);
	
	// Anzahl der Rows ermitteln
	ilRowCount = pomAbsenceGrid->GetRowCount();
	if (ilRowCount > 0)
		// Grid l�schen			
		blRet =	pomAbsenceGrid->RemoveRows(1,ilRowCount);
	
	long llUrno; 
	int ilCount = 0; // Datensatz-Z�hler

	for(int i = 0; i < pomMaaData->GetSize(); i++)
	{	// Neue Row einf�gen
		ilRowCount = pomAbsenceGrid->GetRowCount ();
		blRet = pomAbsenceGrid->InsertRows ( ilRowCount+1,1);
		
		llUrno = pomMaaData->GetAt(i).Urno;
		pomAbsenceGrid->SetStyleRange ( CGXRange(ilCount+1,1), CGXStyle().SetItemDataPtr( (void*)llUrno ) );
		
		pomAbsenceGrid->SetValueRange(CGXRange(ilCount+1,1),pomMaaData->GetAt(i).Week);
		pomAbsenceGrid->SetValueRange(CGXRange(ilCount+1,2),pomMaaData->GetAt(i).Year);
		pomAbsenceGrid->SetValueRange(CGXRange(ilCount+1,3),pomMaaData->GetAt(i).Valu);
		
		ilCount++;
	}

	CGXSortInfoArray SortInfo;
	SortInfo.SetSize(2);      // 1 key only (you can also have more keys)
	SortInfo[0].nRC = 2;   
    SortInfo[0].sortOrder = CGXSortInfo::ascending;   
    SortInfo[0].sortType = CGXSortInfo::numeric;
	SortInfo[1].nRC = 1;   
    SortInfo[1].sortOrder = CGXSortInfo::ascending;   
    SortInfo[1].sortType = CGXSortInfo::numeric;

	pomAbsenceGrid->SortRows(CGXRange().SetTable(), SortInfo);

	// Readonly einschalten
	pomAbsenceGrid->GetParam()->SetLockReadOnly(true);
}


void AbsencePatternDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	CGXStyle style;
	
	long ilUrno;
	
	MAADATA * polMaa = NULL;
	
	long ilRowCount = pomAbsenceGrid->GetRowCount();

	for (int i=0; i<ilRowCount; i++){
		pomAbsenceGrid->ComposeStyleRowCol(i+1, 1, &style );
		ilUrno = (long)style.GetItemDataPtr();

		if (ilUrno){
			polMaa = NULL;
			polMaa = ogMaaData.GetMaaByUrno(ilUrno);
			if(polMaa != NULL && polMaa->Mawu == 0)
				ogMaaData.Delete(ilUrno);
		}
	}
	
	CDialog::OnCancel();
}
