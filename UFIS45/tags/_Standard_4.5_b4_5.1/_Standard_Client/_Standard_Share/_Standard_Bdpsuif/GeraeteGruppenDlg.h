#if !defined(AFX_GERAETEGRUPPENDLG_H__CD22D6D1_7AA3_11D1_B435_0000B45A33F5__INCLUDED_)
#define AFX_GERAETEGRUPPENDLG_H__CD22D6D1_7AA3_11D1_B435_0000B45A33F5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// GeraeteGruppenDlg.h : header file
//

#include <CCSEdit.h>
#include <CedaGegData.h>
#include <PrivList.h>
/////////////////////////////////////////////////////////////////////////////
// GeraeteGruppenDlg dialog

class GeraeteGruppenDlg : public CDialog
{
// Construction
public:
	GeraeteGruppenDlg(GEGDATA *popGeg, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(GeraeteGruppenDlg)
	enum { IDD = IDD_GERAETEGRUPPEN };
	CButton	m_OK;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_GCAT;
	CCSEdit	m_GCDE;
	CCSEdit	m_GNAM;
	CCSEdit	m_GSNM;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_REMA;
	CCSEdit	m_USEC;
	CCSEdit	m_USEU;
	CString m_Caption;
	//}}AFX_DATA


	GEGDATA *pomGeg;
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(GeraeteGruppenDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(GeraeteGruppenDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GERAETEGRUPPENDLG_H__CD22D6D1_7AA3_11D1_B435_0000B45A33F5__INCLUDED_)
