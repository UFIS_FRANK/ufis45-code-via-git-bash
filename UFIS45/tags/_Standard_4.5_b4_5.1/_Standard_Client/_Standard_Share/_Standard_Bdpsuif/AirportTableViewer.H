#ifndef __AIRPORTTABLEVIEWER_H__
#define __AIRPORTTABLEVIEWER_H__

#include <stdafx.h>
#include <CedaAPTData.h>
#include <CCSTable.h>
#include <CCSPrint.h>
#include <CViewer.h>

struct AIRPORTTABLE_LINEDATA
{
	long	Urno;
	CString	Apc3;
	CString	Apc4;
	CString	Apfn;
	CString	Land;
	CString	Aptt;
	CString	Etof;
	CString	Tich;
	CString	Tdi1;
	CString	Tdi2;
	CString	Apsn;
	CString	Vafr;
	CString	Vato;
	CString	Home;
};

/////////////////////////////////////////////////////////////////////////////
// AirportTableViewer

class AirportTableViewer : public CViewer
{
// Constructions
public:
    AirportTableViewer(CCSPtrArray<APTDATA> *popData);
    ~AirportTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	BOOL IsPassFilter(APTDATA *prpAirport);
	int CompareAirport(AIRPORTTABLE_LINEDATA *prpAirport1, AIRPORTTABLE_LINEDATA *prpAirport2);
    void MakeLines();
	void MakeLine(APTDATA *prpAirport);
	BOOL FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(AIRPORTTABLE_LINEDATA *prpAirport);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(AIRPORTTABLE_LINEDATA *prpLine);
	void ProcessAirportChange(APTDATA *prpAirport);
	void ProcessAirportDelete(APTDATA *prpAirport);
	BOOL FindAirport(char *prpAirportKeya, char *prpAirportKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	BOOL bmIsFromSearch;
// Attributes
private:
    CCSTable *pomAirportTable;
	CCSPtrArray<APTDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
	CString m_ContUrno;
	void SetSelectedContinent(CString urno);
    CCSPtrArray<AIRPORTTABLE_LINEDATA> omLines;
/////////////////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(AIRPORTTABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;
};

#endif //__AIRPORTTABLEVIEWER_H__
