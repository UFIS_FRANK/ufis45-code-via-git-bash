// TraffictypeDlg.cpp : implementation file
//

#include <stdafx.h>
#include <BDPSUIF.h>
#include <TraffictypeDlg.h>
#include <PrivList.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// TraffictypeDlg dialog


TraffictypeDlg::TraffictypeDlg(NATDATA *popNAT,CWnd* pParent /*=NULL*/) : CDialog(TraffictypeDlg::IDD, pParent)
{
	pomNAT = popNAT;

	//{{AFX_DATA_INIT(TraffictypeDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void TraffictypeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(TraffictypeDlg)
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_TTYP,   m_TTYP);
	DDX_Control(pDX, IDC_TNAM,   m_TNAM);
	DDX_Control(pDX, IDC_VAFR_D, m_VAFRD);
	DDX_Control(pDX, IDC_VAFR_T, m_VAFRT);
	DDX_Control(pDX, IDC_VATO_D, m_VATOD);
	DDX_Control(pDX, IDC_VATO_T, m_VATOT);
	DDX_Control(pDX, IDC_STYP,   m_STYP);
	DDX_Control(pDX, IDC_FLTI,   m_FLTI);
	DDX_Control(pDX, IDC_APC_4,  m_APC4);
	DDX_Control(pDX, IDC_ALC3,   m_ALC3);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(TraffictypeDlg, CDialog)
	//{{AFX_MSG_MAP(TraffictypeDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// TraffictypeDlg message handlers
//----------------------------------------------------------------------------------------

BOOL TraffictypeDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_Caption = LoadStg(IDS_STRING194) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);

	char clStat = ogPrivList.GetStat("TRAFFICTYPEDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomNAT->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("TRAFFICTYPEDLG.m_CDAT"));
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomNAT->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("TRAFFICTYPEDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomNAT->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("TRAFFICTYPEDLG.m_LSTU"));
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomNAT->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("TRAFFICTYPEDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomNAT->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("TRAFFICTYPEDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomNAT->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("TRAFFICTYPEDLG.m_USEU"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
//	m_TTYP.SetFormat("x|#x|#x|#x|#x|#");
//	m_TTYP.SetTextLimit(1,5);
	m_TTYP.SetTypeToString("X(5)",5,1);
	m_TTYP.SetBKColor(YELLOW);  
	m_TTYP.SetTextErrColor(RED);
	m_TTYP.SetInitText(pomNAT->Ttyp);
	m_TTYP.SetSecState(ogPrivList.GetStat("TRAFFICTYPEDLG.m_TTYP"));
	//------------------------------------
	m_TNAM.SetTypeToString("X(30)",30,1);
	m_TNAM.SetBKColor(YELLOW);
	m_TNAM.SetTextErrColor(RED);
	m_TNAM.SetInitText(pomNAT->Tnam);
	m_TNAM.SetSecState(ogPrivList.GetStat("TRAFFICTYPEDLG.m_TNAM"));
	//------------------------------------
	m_VAFRD.SetTypeToDate(true);
	m_VAFRD.SetTextErrColor(RED);
	m_VAFRD.SetBKColor(YELLOW);
	m_VAFRD.SetInitText(pomNAT->Vafr.Format("%d.%m.%Y"));
	m_VAFRD.SetSecState(ogPrivList.GetStat("TRAFFICTYPEDLG.m_VAFR"));
	// - - - - - - - - - - - - - - - - - -
	m_VAFRT.SetTypeToTime(true);
	m_VAFRT.SetTextErrColor(RED);
	m_VAFRT.SetBKColor(YELLOW);
	m_VAFRT.SetInitText(pomNAT->Vafr.Format("%H:%M"));
	m_VAFRT.SetSecState(ogPrivList.GetStat("TRAFFICTYPEDLG.m_VAFR"));
	//------------------------------------
	m_VATOD.SetTypeToDate();
	m_VATOD.SetTextErrColor(RED);
	m_VATOD.SetInitText(pomNAT->Vato.Format("%d.%m.%Y"));
	m_VATOD.SetSecState(ogPrivList.GetStat("TRAFFICTYPEDLG.m_VATO"));
	// - - - - - - - - - - - - - - - - - -
	m_VATOT.SetTypeToTime();
	m_VATOT.SetTextErrColor(RED);
	m_VATOT.SetInitText(pomNAT->Vato.Format("%H:%M"));
	m_VATOD.SetSecState(ogPrivList.GetStat("TRAFFICTYPEDLG.m_VATO"));
	//------------------------------------

	if(ogNATData.IsNatureAutoCalc() == true)
	{
		m_STYP.SetTypeToString("X(2)",2,1);
		m_STYP.SetBKColor(WHITE);
		m_STYP.SetInitText(pomNAT->Styp);
		m_STYP.SetSecState(ogPrivList.GetStat("TRAFFICTYPEDLG.m_STYP"));		
		//------------------------------------
		m_FLTI.SetTypeToString("X(2)",2,1);
		m_FLTI.SetBKColor(WHITE);
		m_FLTI.SetInitText(pomNAT->Flti);
		m_FLTI.SetSecState(ogPrivList.GetStat("TRAFFICTYPEDLG.m_FLTI"));		
		//------------------------------------
		m_APC4.SetTypeToString("X(4)",4,1);
		m_APC4.SetBKColor(WHITE);
		m_APC4.SetInitText(pomNAT->Apc4);
		m_APC4.SetSecState(ogPrivList.GetStat("TRAFFICTYPEDLG.m_APC4"));		
		//------------------------------------
		m_ALC3.SetTypeToString("X(3)",3,1);
		m_ALC3.SetBKColor(WHITE);
		m_ALC3.SetInitText(pomNAT->Alc3);
		m_ALC3.SetSecState(ogPrivList.GetStat("TRAFFICTYPEDLG.m_ALC3"));				
	}
	else
	{
		CStatic* olpGroup = (CStatic*)GetDlgItem(IDC_NATURE_AUTO);
		olpGroup->ShowWindow(SW_HIDE);

		CStatic* olpService = (CStatic*)GetDlgItem(IDC_SERVICE_TYPE);
		olpService->ShowWindow(SW_HIDE);

		CStatic* olpFlight = (CStatic*)GetDlgItem(IDC_FLIGHT_ID);
		olpFlight->ShowWindow(SW_HIDE);

		CStatic* olpAirport = (CStatic*)GetDlgItem(IDC_AIRPORT_CODE);
		olpAirport->ShowWindow(SW_HIDE);

		CStatic* olpAirline = (CStatic*)GetDlgItem(IDC_AIRLINE_CODE);
		olpAirline->ShowWindow(SW_HIDE);

		CEdit* olpSTYP = (CEdit*)GetDlgItem(IDC_STYP);
		olpSTYP->ShowWindow(SW_HIDE);		
		
		CEdit* olpFLTI = (CEdit*)GetDlgItem(IDC_FLTI);
		olpFLTI->ShowWindow(SW_HIDE);
		
		CEdit* olpAPC4 = (CEdit*)GetDlgItem(IDC_APC_4);
		olpAPC4->ShowWindow(SW_HIDE);
		
		CEdit* olpALC3 = (CEdit*)GetDlgItem(IDC_ALC3);
		olpALC3->ShowWindow(SW_HIDE);

		ResizeWindow();
	}
	//------------------------------------	
	return TRUE;  
}

//----------------------------------------------------------------------------------------

void TraffictypeDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	if(m_TTYP.GetStatus() == false)
	{
		ilStatus = false;
		if(m_TTYP.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING237) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING237) + ogNotFormat;
		}
	}
	if(m_TNAM.GetStatus() == false)
	{
		ilStatus = false;
		if(m_TNAM.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING288) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING288) + ogNotFormat;
		}
	}
	if(m_VAFRD.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRD.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING232) + ogNotFormat;
		}
	}
	if(m_VAFRT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_VAFRT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING230) + LoadStg(IDS_STRING233) + ogNotFormat;
		}
	}
	if(m_VATOD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING232) + ogNotFormat;
	}
	if(m_VATOT.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING231) + LoadStg(IDS_STRING233) + ogNotFormat;
	}

	///////////////////////////////////////////////////////////////////////////
	CString olVafrd,olVafrt,olVatod,olVatot;
	CTime olTmpTimeFr,olTmpTimeTo;
	m_VAFRD.GetWindowText(olVafrd);
	m_VAFRT.GetWindowText(olVafrt);
	m_VATOD.GetWindowText(olVatod);
	m_VATOT.GetWindowText(olVatot);

	if((m_VAFRD.GetWindowTextLength() != 0 && m_VAFRT.GetWindowTextLength() == 0) || (m_VAFRD.GetWindowTextLength() == 0 && m_VAFRT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING234);
	}
	if((m_VATOD.GetWindowTextLength() != 0 && m_VATOT.GetWindowTextLength() == 0) || (m_VATOD.GetWindowTextLength() == 0 && m_VATOT.GetWindowTextLength() != 0))
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING235);
	}
	if(m_VAFRD.GetStatus() == true && m_VAFRT.GetStatus() == true && m_VATOD.GetStatus() == true && m_VATOT.GetStatus() == true)
	{
		olTmpTimeFr = DateHourStringToDate(olVafrd,olVafrt);
		olTmpTimeTo = DateHourStringToDate(olVatod,olVatot);
		if(olTmpTimeTo != -1 && (olTmpTimeTo < olTmpTimeFr || olTmpTimeFr == -1))
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING236);
		}
	}

	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olTtyp;
		char clWhere[100];
		CCSPtrArray<NATDATA> olNatCPA;

		m_TTYP.GetWindowText(olTtyp);
		sprintf(clWhere,"WHERE TTYP='%s'",olTtyp);
		if(ogNATData.ReadSpecialData(&olNatCPA,clWhere,"URNO,TTYP",false) == true)
		{
			if(!ogNATData.IsNatureAutoCalc() && olNatCPA[0].Urno != pomNAT->Urno)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING237) + LoadStg(IDS_EXIST);
			}
		}
		olNatCPA.DeleteAll();
	}

	///////////////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_TTYP.GetWindowText(pomNAT->Ttyp,6);
		m_TNAM.GetWindowText(pomNAT->Tnam,31);
		if(ogNATData.IsNatureAutoCalc() == true)
		{
			m_STYP.GetWindowText(pomNAT->Styp,3);
			m_FLTI.GetWindowText(pomNAT->Flti,3);
			m_APC4.GetWindowText(pomNAT->Apc4,5);
			m_ALC3.GetWindowText(pomNAT->Alc3,4);
		}
		pomNAT->Vafr = DateHourStringToDate(olVafrd,olVafrt);
		pomNAT->Vato = DateHourStringToDate(olVatod,olVatot);

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_TTYP.SetFocus();
		m_TTYP.SetSel(0,-1);
	}
}

void TraffictypeDlg::ResizeWindow()
{
	CRect olWindowRect;
	CRect olNameWindowRect;
	int olOffset = 0;

	((CEdit*)GetDlgItem(IDC_TNAM))->GetWindowRect(olNameWindowRect);
	ScreenToClient(olNameWindowRect);

	((CStatic*)GetDlgItem(IDC_NATURE_AUTO))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);

	olOffset = olWindowRect.bottom - olNameWindowRect.bottom;

	CRect olDeflateRect(0,-(olOffset),0,olOffset);
	this->GetWindowRect(&olWindowRect);
	olWindowRect.bottom -= olOffset;
	this->MoveWindow(olWindowRect);	
	
	((CStatic*)GetDlgItem(IDC_GROUP_VALID))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CStatic*)GetDlgItem(IDC_GROUP_VALID))->MoveWindow(olWindowRect);
	
	((CStatic*)GetDlgItem(IDC_VALID_FROM))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CStatic*)GetDlgItem(IDC_VALID_FROM))->MoveWindow(olWindowRect);
		
	((CStatic*)GetDlgItem(IDC_VALID_TO))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CStatic*)GetDlgItem(IDC_VALID_TO))->MoveWindow(olWindowRect);
	
	((CEdit*)GetDlgItem(IDC_VAFR_D))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_VAFR_D))->MoveWindow(olWindowRect);	
	
	((CEdit*)GetDlgItem(IDC_VAFR_T))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_VAFR_T))->MoveWindow(olWindowRect);	

	((CEdit*)GetDlgItem(IDC_VATO_D))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_VATO_D))->MoveWindow(olWindowRect);	

	((CEdit*)GetDlgItem(IDC_VATO_T))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_VATO_T))->MoveWindow(olWindowRect);
	
	((CEdit*)GetDlgItem(IDC_GROUP_CREATED))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_GROUP_CREATED))->MoveWindow(olWindowRect);	

	((CEdit*)GetDlgItem(IDC_CREATED_ON))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_CREATED_ON))->MoveWindow(olWindowRect);	

	((CEdit*)GetDlgItem(IDC_CDAT_D))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_CDAT_D))->MoveWindow(olWindowRect);	

	((CEdit*)GetDlgItem(IDC_CDAT_T))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_CDAT_T))->MoveWindow(olWindowRect);	

	((CEdit*)GetDlgItem(IDC_CHANGED_ON))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_CHANGED_ON))->MoveWindow(olWindowRect);	

	((CEdit*)GetDlgItem(IDC_LSTU_D))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_LSTU_D))->MoveWindow(olWindowRect);	

	((CEdit*)GetDlgItem(IDC_LSTU_T))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_LSTU_T))->MoveWindow(olWindowRect);	

	((CEdit*)GetDlgItem(IDC_CREATED_BY))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_CREATED_BY))->MoveWindow(olWindowRect);	

	((CEdit*)GetDlgItem(IDC_USEC))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_USEC))->MoveWindow(olWindowRect);	

	((CEdit*)GetDlgItem(IDC_CHANGED_BY))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_CHANGED_BY))->MoveWindow(olWindowRect);	

	((CEdit*)GetDlgItem(IDC_USEU))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDC_USEU))->MoveWindow(olWindowRect);	

	((CEdit*)GetDlgItem(IDOK))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDOK))->MoveWindow(olWindowRect);	

	((CEdit*)GetDlgItem(IDCANCEL))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CEdit*)GetDlgItem(IDCANCEL))->MoveWindow(olWindowRect);	
}

//----------------------------------------------------------------------------------------

