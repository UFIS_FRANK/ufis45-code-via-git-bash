
// WegezeitenDlg.cpp : implementation file
//

#include <stdafx.h>
#include <bdpsuif.h>
#include <WegezeitenDlg.h>
#include <PrivList.h>
#include <CedaPstData.h>
#include <CedaGatData.h>
#include <CedaRwyData.h>
#include <CedaTwyData.h>
//#include "CedaGrnData.h"
//#include "CedaSgrData.h"
#include <CedaWgnData.h>
#include <CedaCICData.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// WegezeitenDlg dialog


WegezeitenDlg::WegezeitenDlg(WAYDATA *popWay, CWnd* pParent /*=NULL*/)
	: CDialog(WegezeitenDlg::IDD, pParent)
{
	pomWay = popWay;
	//{{AFX_DATA_INIT(WegezeitenDlg)
	//}}AFX_DATA_INIT
}


void WegezeitenDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(WegezeitenDlg)
	DDX_Control(pDX, IDC_TYEN_GAT, m_TYENGAT);
	DDX_Control(pDX, IDC_TYEN_P, m_TYENP);
	DDX_Control(pDX, IDC_TYEN_R, m_TYENR);
	DDX_Control(pDX, IDC_TYEN_G, m_TYENG);
	DDX_Control(pDX, IDC_TYBE_P, m_TYBEP);
	DDX_Control(pDX, IDC_TYBE_R, m_TYBER);
	DDX_Control(pDX, IDC_TYBE_G, m_TYBEG);
	DDX_Control(pDX, IDC_TYBE_GAT, m_TYBEGAT);
	DDX_Control(pDX, IDC_WTYP,   m_WTYP);
	DDX_Control(pDX, IDC_POEN,	 m_POEN);
	DDX_Control(pDX, IDC_POBE,	 m_POBE);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_GRUP,	 m_GRUP);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_TTGO,	 m_TTGO);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_WTGO_S, m_WTGOS);
	DDX_Control(pDX, IDC_WTGO_T, m_WTGOT);
	DDX_Control(pDX, IDC_HOME, m_HOME);
	DDX_Control(pDX, IDOK,		 m_OK);
	DDX_Control(pDX, IDC_TYEN_C, m_TYENCIC);
	DDX_Control(pDX, IDC_TYBE_C, m_TYBECIC);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(WegezeitenDlg, CDialog)
	//{{AFX_MSG_MAP(WegezeitenDlg)
	ON_BN_CLICKED(IDC_TYBE_P, OnTybe)
	ON_BN_CLICKED(IDC_TYEN_P, OnTyen)
	ON_BN_CLICKED(IDC_TYEN_R, OnTyen)
	ON_BN_CLICKED(IDC_TYEN_G, OnTyen)
	ON_BN_CLICKED(IDC_TYBE_R, OnTybe)
	ON_BN_CLICKED(IDC_TYBE_G, OnTybe)
	ON_BN_CLICKED(IDC_TYBE_GAT, OnTybe)
	ON_BN_CLICKED(IDC_TYEN_GAT, OnTyen)
	ON_BN_CLICKED(IDC_TYBE_C, OnTybe)
	ON_BN_CLICKED(IDC_TYEN_C, OnTyen)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// WegezeitenDlg message handlers
//----------------------------------------------------------------------------------------

BOOL WegezeitenDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_Caption = LoadStg(IDS_STRING196) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	
	char clStat = ogPrivList.GetStat("WEGEZEITENDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomWay->Cdat.Format("%d.%m.%Y"));
	//m_CDATD.SetSecState(ogPrivList.GetStat("WEGEZEITENDLG.m_CDAT"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomWay->Cdat.Format("%H:%M"));
	//m_CDATT.SetSecState(ogPrivList.GetStat("WEGEZEITENDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomWay->Lstu.Format("%d.%m.%Y"));
	//m_LSTUD.SetSecState(ogPrivList.GetStat("WEGEZEITENDLG.m_LSTU"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomWay->Lstu.Format("%H:%M"));
	//m_LSTUT.SetSecState(ogPrivList.GetStat("WEGEZEITENDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomWay->Usec);
	//m_USEC.SetSecState(ogPrivList.GetStat("WEGEZEITENDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomWay->Useu);
	//m_USEU.SetSecState(ogPrivList.GetStat("WEGEZEITENDLG.m_USEU"));
	////------------------------------------
	m_GRUP.SetBKColor(SILVER);
	//m_GRUP.SetInitText(pomGAT->);
	//m_GRUP.SetSecState(ogPrivList.GetStat("WEGEZEITENDLG.m_GRUP"));
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_HOME.SetTypeToString("X(3)",3,1);
	m_HOME.SetTextErrColor(RED);
	m_HOME.SetInitText(pomWay->Home);
	//m_HOME.SetSecState(ogPrivList.GetStat("WEGEZEITENDLG.m_HOME"));
	//------------------------------------
	//char clStat1;
	char clStat2 = '1';
	m_POBE.SetBKColor(YELLOW);
	m_POEN.SetBKColor(YELLOW);
	//clStat1 = ogPrivList.GetStat("WEGEZEITENDLG.m_POBE");
	//SetWndStatPrio_1(clStat1,clStat2,m_POBEG);
	SetWndStatPrio_1('1',clStat2,m_POBE);
	//clStat1 = ogPrivList.GetStat("WEGEZEITENDLG.m_POEN");
	//SetWndStatPrio_1(clStat1,clStat2,m_POENG);
	SetWndStatPrio_1('1',clStat2,m_POEN);
	//m_POBE.SetSecState(ogPrivList.GetStat("WEGEZEITENDLG.m_POBE"));
	//m_POEN.SetSecState(ogPrivList.GetStat("WEGEZEITENDLG.m_POEN"));
	m_POBE.SetFont(&ogCourier_Regular_9);
	m_POEN.SetFont(&ogCourier_Regular_9);

	//------------------------------------
	m_TYBEP.SetCheck(0);
	m_TYBER.SetCheck(0);
	m_TYBEG.SetCheck(0);
	m_TYBEGAT.SetCheck(0);

	/*
	WAY.Tybe P = Position
	         R = Runway
		     G = Gate
		     A = Group from WGNTAB
			 C = Check in counter
	*/

	if (pomWay->Tybe[0] == 'P')
	{
		cmOldTybe = 'P';
		m_TYBEP.SetCheck(1);
		m_POBE.SetBKColor(YELLOW);
		this->InitializeComboBoxes("PST",&m_POBE);
		SetCurSelByCode(&m_POBE,pomWay->Pobe);
	}
	else if (pomWay->Tybe[0] == 'R')
	{
		cmOldTybe = 'R';
		m_TYBER.SetCheck(1);
		m_POBE.SetBKColor(YELLOW);  
		this->InitializeComboBoxes("RWY",&m_POBE);
		SetCurSelByCode(&m_POBE,pomWay->Pobe);
	}
	else if (pomWay->Tybe[0] == 'G')
	{
		cmOldTybe = 'G';
		m_TYBEGAT.SetCheck(1);
		m_POBE.SetBKColor(YELLOW);  
		this->InitializeComboBoxes("GAT",&m_POBE);
		SetCurSelByCode(&m_POBE,pomWay->Pobe);
		//m_POBEG.SetBKColor(YELLOW);
	}
	else if (pomWay->Tybe[0] == 'C')
	{
		cmOldTybe = 'C';
		m_TYBECIC.SetCheck(1);
		m_POBE.SetBKColor(YELLOW);  
		this->InitializeComboBoxes("CIC",&m_POBE);
		SetCurSelByCode(&m_POBE,pomWay->Pobe);
		//m_POBEG.SetBKColor(YELLOW);
	}

	else if (pomWay->Tybe[0] == 'A')
	{
		cmOldTybe = 'A';
		m_TYBEG.SetCheck(1);
		m_POBE.SetBKColor(YELLOW);  
		this->InitializeComboBoxes("WGN",&m_POBE);
		SetCurSelByCode(&m_POBE,pomWay->Pobe);
		//m_POBEG.SetBKColor(YELLOW);
	}
	else
	{
		cmOldTybe = 'P';
		m_TYBEP.SetCheck(1);
		m_POBE.SetBKColor(YELLOW);  
		this->InitializeComboBoxes("PST",&m_POBE);
		SetCurSelByCode(&m_POBE,pomWay->Pobe);
	}

	//------------------------------------
	m_TYENP.SetCheck(0);
	m_TYENR.SetCheck(0);
	m_TYENG.SetCheck(0);
	m_TYENGAT.SetCheck(0);

	if (pomWay->Tyen[0] == 'P')
	{
		cmOldTyen = 'P';
		m_TYENP.SetCheck(1);
		m_POEN.SetBKColor(YELLOW);  
		this->InitializeComboBoxes("PST",&m_POEN);
		SetCurSelByCode(&m_POEN,pomWay->Poen);
	}
	else if (pomWay->Tyen[0] == 'R')
	{
		cmOldTyen = 'R';
		m_TYENR.SetCheck(1);
		m_POEN.SetBKColor(YELLOW);  
		this->InitializeComboBoxes("RWY",&m_POEN);
		SetCurSelByCode(&m_POEN,pomWay->Poen);
	}
	else if (pomWay->Tyen[0] == 'G')
	{
		cmOldTyen = 'G';
		m_TYENGAT.SetCheck(1);
		m_POEN.SetBKColor(YELLOW);  
		this->InitializeComboBoxes("GAT",&m_POEN);
		SetCurSelByCode(&m_POEN,pomWay->Poen);
	}
	else if (pomWay->Tyen[0] == 'C')
	{
		cmOldTyen = 'C';
		m_TYENCIC.SetCheck(1);
		m_POEN.SetBKColor(YELLOW);  
		this->InitializeComboBoxes("CIC",&m_POEN);
		SetCurSelByCode(&m_POEN,pomWay->Poen);
	}
	else if (pomWay->Tyen[0] == 'A')
	{
		cmOldTyen = 'A';
		m_TYENG.SetCheck(1);
		m_POEN.SetBKColor(YELLOW);  
		this->InitializeComboBoxes("WGN",&m_POEN);
		SetCurSelByCode(&m_POEN,pomWay->Poen);
	}
	else 
	{
		cmOldTyen = 'P';
		m_TYENP.SetCheck(1);
		m_POEN.SetBKColor(YELLOW);  
		this->InitializeComboBoxes("PST",&m_POEN);
		SetCurSelByCode(&m_POEN,pomWay->Poen);
	}

	//------------------------------------
	m_WTGOS.SetTypeToInt(0,9999);
	m_WTGOS.SetTextErrColor(RED);
	m_WTGOT.SetFormat("x|#x|#x|#x|#x|#");
	m_WTGOT.SetTextLimit(0,5);
	m_WTGOT.SetTextErrColor(RED);
	if (pomWay->Tybe[0] == 'R'||pomWay->Tyen[0] == 'R')
	{
		m_WTGOT.SetInitText(pomWay->Wtgo);		
		//if(ogPrivList.GetStat("WEGEZEITENDLG.m_WTGO")=='1')
		//{
			m_WTGOS.SetSecState('0');

			if(bgShowTaxi)
			{
				m_WTGOT.SetSecState('1');
			}
			else
			{
				m_WTGOT.SetSecState('0');
			}
		//}
		//else
		//{
			//m_WTGOS.SetSecState(ogPrivList.GetStat("WEGEZEITENDLG.m_WTGO"));
			//m_WTGOT.SetSecState(ogPrivList.GetStat("WEGEZEITENDLG.m_WTGO"));
		//}
	}
	else 
	{
		m_WTGOS.SetInitText(pomWay->Wtgo);		
		//if(ogPrivList.GetStat("WEGEZEITENDLG.m_WTGO")=='1')
		//{
			m_WTGOS.SetSecState('1');
			
			m_WTGOT.SetSecState('0');
			

		//}
		//else
		//{
			//m_WTGOS.SetSecState(ogPrivList.GetStat("WEGEZEITENDLG.m_WTGO"));
			//m_WTGOT.SetSecState(ogPrivList.GetStat("WEGEZEITENDLG.m_WTGO"));
		//}
	}
	//------------------------------------
	m_TTGO.SetTypeToInt(0,9999);
	m_TTGO.SetBKColor(YELLOW);  
	m_TTGO.SetTextErrColor(RED);
	m_TTGO.SetInitText(pomWay->Ttgo);		
	//m_TTGO.SetSecState(ogPrivList.GetStat("WEGEZEITENDLG.m_TTGO"));
	//------------------------------------
	
	/*
		WAY.Wtype = 0 on Foot
		    Wtype = 1 equipment
		    Wtype = 2 Tow
			Wtype = 3 Taxi
	*/
	m_WTYP.AddString(LoadStg(IDS_STRING32834)); //Taxi
	m_WTYP.AddString(LoadStg(IDS_STRING432));   //equipment
	m_WTYP.AddString(LoadStg(IDS_STRING434));   //on Foot
	m_WTYP.AddString(LoadStg(IDS_STRING433));   //Tow
	
	/*
	if     (pomWay->Wtyp[0] == '1') m_WTYP.SetCurSel(0);
	else if(pomWay->Wtyp[0] == '0') m_WTYP.SetCurSel(1);
	else if(pomWay->Wtyp[0] == '2') m_WTYP.SetCurSel(2);
	else    m_WTYP.SetCurSel(0);*/

	if     (pomWay->Wtyp[0] == '0') m_WTYP.SetCurSel(2);
	else if(pomWay->Wtyp[0] == '1') m_WTYP.SetCurSel(1);
	else if(pomWay->Wtyp[0] == '2') m_WTYP.SetCurSel(3);
	else if(pomWay->Wtyp[0] == '3') m_WTYP.SetCurSel(0);
	else    m_WTYP.SetCurSel(0);


	//------------------------------------
	m_POBE.SetFocus();

	if(!bgShowTaxi)
	{
		m_WTYP.EnableWindow(FALSE);
		m_WTGOT.EnableWindow(FALSE);
		m_HOME.EnableWindow(FALSE);
	}

	return TRUE;
}

//----------------------------------------------------------------------------------------

void WegezeitenDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;

	if(m_TYBEP.GetCheck() == 1)
	{
		if(m_POBE.GetCurSel() == CB_ERR)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING420) + LoadStg(IDS_STRING422) +  ogNoData;
		}
	}
	if(m_TYBER.GetCheck() == 1)
	{
		if(m_POBE.GetCurSel() == CB_ERR)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING420) + LoadStg(IDS_STRING423) +  ogNoData;
		}
	}
	if(m_TYBEGAT.GetCheck() == 1)
	{
		if(m_POBE.GetCurSel() == CB_ERR)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING420) + LoadStg(IDS_STRING996) +  ogNoData;
		}
	}
	if(m_TYBECIC.GetCheck() == 1)
	{
		if(m_POBE.GetCurSel() == CB_ERR)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING420) + LoadStg(IDS_STRING997) +  ogNoData;
		}
	}
	if(m_TYBEG.GetCheck() == 1)
	{
		if(m_POBE.GetCurSel() == CB_ERR)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING420) + LoadStg(IDS_STRING424) +  ogNoData;
		}
	}
	if(m_TYENP.GetCheck() == 1)
	{
		if(m_POEN.GetCurSel() == CB_ERR)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING421) + LoadStg(IDS_STRING422) +  ogNoData;
		}
	}
	if(m_TYENR.GetCheck() == 1)
	{
		if(m_POEN.GetCurSel() == CB_ERR)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING421) + LoadStg(IDS_STRING423) +  ogNoData;
		}

	}
	if(m_TYENGAT.GetCheck() == 1)
	{
		if(m_POEN.GetCurSel() == CB_ERR)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING421) + LoadStg(IDS_STRING996) +  ogNoData;
		}

	}
	if(m_TYENCIC.GetCheck() == 1)
	{
		if(m_POEN.GetCurSel() == CB_ERR)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING421) + LoadStg(IDS_STRING997) +  ogNoData;
		}

	}

	if(m_TYENG.GetCheck() == 1)
	{
		if(m_POEN.GetCurSel() == CB_ERR)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING421) + LoadStg(IDS_STRING424) +  ogNoData;
		}
	}
	if(m_TYBER.GetCheck() == 1 || m_TYENR.GetCheck() == 1)
	{
		if(m_WTGOT.GetStatus() == false)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING425) + ogNotFormat;
		}
	}
	else
	{
		if(m_WTGOS.GetStatus() == false)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING426) + ogNotFormat;
		}
	}
	if(m_TTGO.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING427) + ogNotFormat;
	}

	///////////////////////////////////////////////////////////////////
	//Konsistenz-Check//
	if(ilStatus == true)
	{
		char clWhere[100];
		CString olTmpTXT;

		if(m_TTGO.GetWindowTextLength() == 0)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING427) +  ogNoData;
		}
		if(m_TYBEP.GetCheck() == 1)
		{
			m_POBE.GetWindowText(olTmpTXT);
			olTmpTXT = olTmpTXT.Left(20);
			sprintf(clWhere,"WHERE PNAM='%s'",olTmpTXT);
			if(ogPSTData.ReadSpecialData(NULL,clWhere,"PNAM",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING428);
			}
		}
		if(m_TYBER.GetCheck() == 1)
		{
			m_POBE.GetWindowText(olTmpTXT);
			olTmpTXT = olTmpTXT.Left(20);
			sprintf(clWhere,"WHERE RNAM='%s'",olTmpTXT);
			if(ogRWYData.ReadSpecialData(NULL,clWhere,"RNAM",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING429);
			}
		}
		if(m_TYBEGAT.GetCheck() == 1)
		{
			m_POBE.GetWindowText(olTmpTXT);
			olTmpTXT = olTmpTXT.Left(20);
			sprintf(clWhere,"WHERE GNAM='%s'",olTmpTXT);
			if(ogGATData.ReadSpecialData(NULL,clWhere,"GNAM",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING750);
			}
		}
		if(m_TYBECIC.GetCheck() == 1)
		{
			m_POBE.GetWindowText(olTmpTXT);
			olTmpTXT = olTmpTXT.Left(20);
			sprintf(clWhere,"WHERE CNAM='%s'",olTmpTXT);
			if(ogCICData.ReadSpecialData(NULL,clWhere,"CNAM",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING994);
			}
		}

		if(m_TYENP.GetCheck() == 1)
		{
			m_POEN.GetWindowText(olTmpTXT);
			olTmpTXT = olTmpTXT.Left(20);
			sprintf(clWhere,"WHERE PNAM='%s'",olTmpTXT);
			if(ogPSTData.ReadSpecialData(NULL,clWhere,"PNAM",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING430);
			}
		}
		if(m_TYENR.GetCheck() == 1)
		{
			m_POEN.GetWindowText(olTmpTXT);
			olTmpTXT = olTmpTXT.Left(20);
			sprintf(clWhere,"WHERE RNAM='%s'",olTmpTXT);
			if(ogRWYData.ReadSpecialData(NULL,clWhere,"RNAM",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING431);
			}
		}
		if(m_TYENGAT.GetCheck() == 1)
		{
			m_POEN.GetWindowText(olTmpTXT);
			olTmpTXT = olTmpTXT.Left(20);
			sprintf(clWhere,"WHERE GNAM='%s'",olTmpTXT);
			if(ogGATData.ReadSpecialData(NULL,clWhere,"GNAM",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING751);
			}
		}
		if(m_TYENCIC.GetCheck() == 1)
		{
			m_POEN.GetWindowText(olTmpTXT);
			olTmpTXT = olTmpTXT.Left(20);
			sprintf(clWhere,"WHERE CNAM='%s'",olTmpTXT);
			if(ogCICData.ReadSpecialData(NULL,clWhere,"CNAM",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING995);
			}
		}

		if(m_TYENR.GetCheck() == 1 || m_TYBER.GetCheck() == 1)
		{
			m_WTGOT.GetWindowText(olTmpTXT);
			olTmpTXT = olTmpTXT.Left(20);
			sprintf(clWhere,"WHERE TNAM='%s'",olTmpTXT);
			if(ogTWYData.ReadSpecialData(NULL,clWhere,"TNAM",false) == false)
			{
				//ilStatus = false;
				//olErrorText += LoadStg(IDS_STRING100);
			}
		}
	}
	///////////////////////////////////////////////////////////////////

	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		CString olWtyp;
		m_WTYP.GetWindowText(olWtyp);

		if(bgShowTaxi)
		{
			if(olWtyp == LoadStg(IDS_STRING432)) pomWay->Wtyp[0] = '1';
			else if(olWtyp == LoadStg(IDS_STRING433)) pomWay->Wtyp[0] = '2';
			else if(olWtyp == LoadStg(IDS_STRING434)) pomWay->Wtyp[0] = '0';
			else if(olWtyp == LoadStg(IDS_STRING32834)) pomWay->Wtyp[0] = '3';
			else pomWay->Wtyp[0] = '1';
		}

		if(m_TYBEP.GetCheck() == 1)
		{
			pomWay->Tybe[0] = 'P';
			strcpy(pomWay->Pobe, GetCodeBySetCurSel(&m_POBE));
		}
		if(m_TYBER.GetCheck() == 1)
		{
			pomWay->Tybe[0] = 'R';
			strcpy(pomWay->Pobe, GetCodeBySetCurSel(&m_POBE));
		}
		if(m_TYBEGAT.GetCheck() == 1)
		{
			pomWay->Tybe[0] = 'G';
			strcpy(pomWay->Pobe, GetCodeBySetCurSel(&m_POBE));
		}
		if(m_TYBECIC.GetCheck() == 1)
		{
			pomWay->Tybe[0] = 'C';
			strcpy(pomWay->Pobe, GetCodeBySetCurSel(&m_POBE));
		}
		if(m_TYBEG.GetCheck() == 1)
		{
			pomWay->Tybe[0] = 'A';
			strcpy(pomWay->Pobe, GetCodeBySetCurSel(&m_POBE));
		}
		if(m_TYENP.GetCheck() == 1)
		{
			pomWay->Tyen[0] = 'P';
			strcpy(pomWay->Poen, GetCodeBySetCurSel(&m_POEN));
		}
		if(m_TYENR.GetCheck() == 1)
		{
			pomWay->Tyen[0] = 'R';
			strcpy(pomWay->Poen, GetCodeBySetCurSel(&m_POEN));
		}
		if(m_TYENGAT.GetCheck() == 1)
		{
			pomWay->Tyen[0] = 'G';
			strcpy(pomWay->Poen, GetCodeBySetCurSel(&m_POEN));
		}
		if(m_TYENCIC.GetCheck() == 1)
		{
			pomWay->Tyen[0] = 'C';
			strcpy(pomWay->Poen, GetCodeBySetCurSel(&m_POEN));
		}
		if(m_TYENG.GetCheck() == 1)
		{
			pomWay->Tyen[0] = 'A';
			strcpy(pomWay->Poen, GetCodeBySetCurSel(&m_POEN));
		}
		if(m_TYENR.GetCheck() == 1 || m_TYBER.GetCheck() == 1)
		{
			m_WTGOT.GetWindowText(pomWay->Wtgo,6);
		}
		else
		{
			m_WTGOS.GetWindowText(pomWay->Wtgo,6);
		}

		m_TTGO.GetWindowText(pomWay->Ttgo,5);

		
		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_POBE.SetFocus();
	}	
}

//----------------------------------------------------------------------------------------

void WegezeitenDlg::OnTybe() 
{
	if (cmOldTybe !='P' && m_TYBEP.GetCheck() == 1)
	{
		if(cmOldTyen != 'R' && cmOldTybe == 'R')
		{
			m_WTGOT.SetSecState('0');
			m_WTGOS.SetSecState('1');
		}


		this->InitializeComboBoxes("PST",&m_POBE);
		cmOldTybe ='P';
	}
	else if(cmOldTybe !='C' && m_TYBECIC.GetCheck() == 1)
	{
		if(cmOldTyen != 'R' && cmOldTybe == 'R')
		{
			m_WTGOT.SetSecState('0');
			m_WTGOS.SetSecState('1');
		}

		this->InitializeComboBoxes("CIC",&m_POBE);
		cmOldTybe ='C';
	}

	else if(cmOldTybe !='R' && m_TYBER.GetCheck() == 1)
	{
		if(cmOldTyen != 'R')
		{
			
			if(bgShowTaxi)
			{
				m_WTGOT.SetSecState('1');
			}
			else
			{
				m_WTGOT.SetSecState('0');
			}
			m_WTGOS.SetSecState('0');
		}

		this->InitializeComboBoxes("RWY",&m_POBE);
		cmOldTybe ='R';
	}
	else if(cmOldTybe !='G' && m_TYBEGAT.GetCheck() == 1)
	{
		if(m_TYENR.GetCheck() != 1)
		{
			m_WTGOT.SetSecState('0');
			m_WTGOS.SetSecState('1');
		}

		this->InitializeComboBoxes("GAT",&m_POBE);
		cmOldTybe ='G';
	}
	else if(cmOldTybe !='A' && m_TYBEG.GetCheck() == 1)
	{
		if(cmOldTyen != 'R' && cmOldTybe == 'R')
		{
			m_WTGOT.SetSecState('0');
			m_WTGOS.SetSecState('1');
		}

		m_WTGOT.SetSecState('0');
		m_WTGOS.SetSecState('1');

		this->InitializeComboBoxes("WGN",&m_POBE);
		cmOldTybe ='A';
	}

	
	m_POBE.InvalidateRect(NULL);
	m_POBE.UpdateWindow();
	m_WTGOT.InvalidateRect(NULL);
	m_WTGOT.UpdateWindow();
	m_WTGOS.InvalidateRect(NULL);
	m_WTGOS.UpdateWindow();
}

//----------------------------------------------------------------------------------------

void WegezeitenDlg::OnTyen() 
{
	if(cmOldTyen !='P' && m_TYENP.GetCheck() == 1)
	{
		if(cmOldTyen == 'R' && cmOldTybe != 'R')
		{
			m_WTGOT.SetSecState('0');
			m_WTGOS.SetSecState('1');
		}

		this->InitializeComboBoxes("PST",&m_POEN);
		cmOldTyen ='P';
	}
	else if(cmOldTyen !='R'  && m_TYENR.GetCheck() == 1)
	{
		if(cmOldTybe != 'R')
		{
			if(bgShowTaxi)
			{
				m_WTGOT.SetSecState('1');
			}
			else
			{
				m_WTGOT.SetSecState('0');
			}
			m_WTGOS.SetSecState('0');
		}

		this->InitializeComboBoxes("RWY",&m_POEN);
		cmOldTyen ='R';
	}
	else if(cmOldTyen !='G'  && m_TYENGAT.GetCheck() == 1)
	{
		if(m_TYBER.GetCheck() != 1)
		{
			m_WTGOT.SetSecState('0');
			m_WTGOS.SetSecState('1');
		}


		this->InitializeComboBoxes("GAT",&m_POEN);
		cmOldTyen ='G';
	}
	else if(cmOldTyen !='C'  && m_TYENCIC.GetCheck() == 1)
	{
		if(m_TYBER.GetCheck() != 1)
		{
			m_WTGOT.SetSecState('0');
			m_WTGOS.SetSecState('1');
		}

		this->InitializeComboBoxes("CIC",&m_POEN);
		cmOldTyen ='C';
	}
	else if(cmOldTyen !='A' && m_TYENG.GetCheck() == 1)
	{
		if(cmOldTyen == 'R' && cmOldTybe != 'R')
		{
			m_WTGOT.SetSecState('0');
			m_WTGOS.SetSecState('1');
		}
		
		m_WTGOT.SetSecState('0');
		m_WTGOS.SetSecState('1');

		this->InitializeComboBoxes("WGN",&m_POEN);
		cmOldTyen ='A';
	}

	m_POEN.InvalidateRect(NULL);
	m_POEN.UpdateWindow();
	m_WTGOT.InvalidateRect(NULL);
	m_WTGOT.UpdateWindow();
	m_WTGOS.InvalidateRect(NULL);
	m_WTGOS.UpdateWindow();
}

//----------------------------------------------------------------------------------------

bool WegezeitenDlg::InitializeComboBoxes(const CString& ropTableName,CComboBox* popCBox) 
{
	popCBox->SetCurSel(-1);
	popCBox->SetWindowText("");
	popCBox->ResetContent();

	if (ropTableName == "CIC")
	{
		CString olGrupp;
		CCSPtrArray<CICDATA> olCicCPA;

		if(ogCICData.ReadSpecialData(&olCicCPA,"","URNO,CNAM",false) == true)
		{
			int ilCicSize = olCicCPA.GetSize();
			for(int i=0;i<ilCicSize;i++)
			{
				olGrupp.Format("%-20s              URNO=%i",olCicCPA[i].Cnam,olCicCPA[i].Urno);
				popCBox->AddString(olGrupp);
			}
			olCicCPA.DeleteAll();
		}
		else
		{
			olCicCPA.DeleteAll();
			return false;
		}
	}

	else if (ropTableName == "GAT")
	{
		CString olGrupp;
		CCSPtrArray<GATDATA> olGatCPA;

		if(ogGATData.ReadSpecialData(&olGatCPA,"","URNO,GNAM",false) == true)
		{
			int ilGatSize = olGatCPA.GetSize();
			for(int i=0;i<ilGatSize;i++)
			{
				olGrupp.Format("%-20s              URNO=%i",olGatCPA[i].Gnam,olGatCPA[i].Urno);
				popCBox->AddString(olGrupp);
			}
			olGatCPA.DeleteAll();
		}
		else
		{
			olGatCPA.DeleteAll();
			return false;
		}
	}

	else if (ropTableName == "PST")
	{
		CString olGrupp;
		CCSPtrArray<PSTDATA> olPstCPA;

		if(ogPSTData.ReadSpecialData(&olPstCPA,"","URNO,PNAM",false) == true)
		{
			int ilPstSize = olPstCPA.GetSize();
			for(int i=0;i<ilPstSize;i++)
			{
				olGrupp.Format("%-20s              URNO=%i",olPstCPA[i].Pnam,olPstCPA[i].Urno);
				popCBox->AddString(olGrupp);
			}
			olPstCPA.DeleteAll();
		}
		else
		{
			olPstCPA.DeleteAll();
			return false;
		}
	}

	else if (ropTableName == "RWY")
	{
		CString olGrupp;
		CCSPtrArray<RWYDATA> olRwyCPA;

		if(ogRWYData.ReadSpecialData(&olRwyCPA,"","URNO,RNAM",false) == true)
		{
			int ilRwySize = olRwyCPA.GetSize();
			for(int i=0;i<ilRwySize;i++)
			{
				olGrupp.Format("%-20s              URNO=%i",olRwyCPA[i].Rnam,olRwyCPA[i].Urno);
				popCBox->AddString(olGrupp);
			}
			olRwyCPA.DeleteAll();
		}
		else
		{
			olRwyCPA.DeleteAll();
			return false;
		}
	}

	else if (ropTableName == "WGN")
	{
		CString olGrupp;
		CCSPtrArray<WGNDATA> olWgnCPA;

		if(ogWgnData.ReadSpecialData(&olWgnCPA,"","URNO,ALGR",false) == true)
		{
			int ilWgnSize = olWgnCPA.GetSize();
			for(int i=0;i<ilWgnSize;i++)
			{
				olGrupp.Format("%-20s              URNO=%i",olWgnCPA[i].Algr,olWgnCPA[i].Urno);
				popCBox->AddString(olGrupp);
			}
			olWgnCPA.DeleteAll();
		}
		else
		{
			olWgnCPA.DeleteAll();
			return false;
		}
	}

	return true;
}

//----------------------------------------------------------------------------------------

void WegezeitenDlg::SetCurSelByUrno(CComboBox* popCBox,CString opUrno)
{
	CString olGrupp,olTmp;
	olGrupp.Format("URNO=%s",opUrno);
	int ilCount = popCBox->GetCount();
	for(int i=0; i<ilCount; i++)
	{
		popCBox->GetLBText(i,olTmp);
		if((olTmp.Find(olGrupp))!=-1)
		{
			popCBox->SetCurSel(i);
			break;
		}
	}
}

//----------------------------------------------------------------------------------------

CString WegezeitenDlg::GetUrnoBySetCurSel(CComboBox* popCBox)
{
	CString olGrupp,olUrno = "0";
	popCBox->GetWindowText(olGrupp);

	if(olGrupp.GetLength() > (olGrupp.Find("URNO=")+5))
		olUrno = olGrupp.Mid(olGrupp.Find("URNO=")+5);
	return olUrno;
}

//----------------------------------------------------------------------------------------

void WegezeitenDlg::SetCurSelByCode(CComboBox* popCBox,CString opCode)
{
	CString olGrupp,olTmp;
	olGrupp.Format("%s   ",opCode);
	int ilCount = popCBox->GetCount();
	for(int i=0; i<ilCount; i++)
	{
		popCBox->GetLBText(i,olTmp);
		if((olTmp.Find(olGrupp))!=-1)
		{
			popCBox->SetCurSel(i);
			break;
		}
	}
}

//----------------------------------------------------------------------------------------

CString WegezeitenDlg::GetCodeBySetCurSel(CComboBox* popCBox)
{
	CString olGrupp,olCode;
	popCBox->GetWindowText(olGrupp);
	olCode = olGrupp.Left(20);
	olCode.TrimLeft( );
	olCode.TrimRight( );
	return olCode;
}

//----------------------------------------------------------------------------------------
