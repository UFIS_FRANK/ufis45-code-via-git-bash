// ArbeitsgruppenTableViewer.cpp 
//

#include <stdafx.h>
#include <ArbeitsgruppenTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void ArbeitsgruppenTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// ArbeitsgruppenTableViewer
//

ArbeitsgruppenTableViewer::ArbeitsgruppenTableViewer(CCSPtrArray<WGPDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomArbeitsGruppenTable = NULL;
    ogDdx.Register(this, WGP_CHANGE, CString("ArbeitsgruppenTableViewer"), CString("Arbeitsgruppen Update"), ArbeitsgruppenTableCf);
    ogDdx.Register(this, WGP_NEW,    CString("ArbeitsgruppenTableViewer"), CString("Arbeitsgruppen New"),    ArbeitsgruppenTableCf);
    ogDdx.Register(this, WGP_DELETE, CString("ArbeitsgruppenTableViewer"), CString("Arbeitsgruppen Delete"), ArbeitsgruppenTableCf);

    ogDdx.Register(this, PGP_CHANGE, CString("ArbeitsgruppenTableViewer"), CString("Planungsgruppen Update"), ArbeitsgruppenTableCf);
    ogDdx.Register(this, PGP_DELETE, CString("ArbeitsgruppenTableViewer"), CString("Planungsgruppen Delete"), ArbeitsgruppenTableCf);

}

//-----------------------------------------------------------------------------------------------

ArbeitsgruppenTableViewer::~ArbeitsgruppenTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
	ogPgpData.ClearAll();
}

//-----------------------------------------------------------------------------------------------

void ArbeitsgruppenTableViewer::Attach(CCSTable *popTable)
{
    pomArbeitsGruppenTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void ArbeitsgruppenTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void ArbeitsgruppenTableViewer::MakeLines()
{
	int ilArbeitsgruppenCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilArbeitsgruppenCount; ilLc++)
	{
		WGPDATA *prlArbeitsgruppenData = &pomData->GetAt(ilLc);
		MakeLine(prlArbeitsgruppenData);
	}
}

//-----------------------------------------------------------------------------------------------

void ArbeitsgruppenTableViewer::MakeLine(WGPDATA *prpArbeitsgruppen)
{

    //if( !IsPassFilter(prpArbeitsgruppen)) return;

    // Update viewer data for this shift record
    ARBEITSGRUPPENTABLE_LINEDATA rlArbeitsgruppen;

	rlArbeitsgruppen.Urno = prpArbeitsgruppen->Urno;
	rlArbeitsgruppen.Wgpc = prpArbeitsgruppen->Wgpc;
	rlArbeitsgruppen.Wgpn = prpArbeitsgruppen->Wgpn;
	rlArbeitsgruppen.Pgpu.Format("%ld",prpArbeitsgruppen->Pgpu);
	rlArbeitsgruppen.Rema = prpArbeitsgruppen->Rema;

	CreateLine(&rlArbeitsgruppen);
}

//-----------------------------------------------------------------------------------------------

void ArbeitsgruppenTableViewer::CreateLine(ARBEITSGRUPPENTABLE_LINEDATA *prpArbeitsgruppen)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareArbeitsgruppen(prpArbeitsgruppen, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	ARBEITSGRUPPENTABLE_LINEDATA rlArbeitsgruppen;
	rlArbeitsgruppen = *prpArbeitsgruppen;
    omLines.NewAt(ilLineno, rlArbeitsgruppen);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void ArbeitsgruppenTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 2;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomArbeitsGruppenTable->SetShowSelection(TRUE);
	pomArbeitsGruppenTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING266),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 300; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING266),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 230; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING266),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	pomArbeitsGruppenTable->SetHeaderFields(omHeaderDataArray);
	rlHeader.Length = 400; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING266),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	pomArbeitsGruppenTable->SetHeaderFields(omHeaderDataArray);

	pomArbeitsGruppenTable->SetDefaultSeparator();
	pomArbeitsGruppenTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Wgpc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Wgpn;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = GetPgpNameByUrno(omLines[ilLineNo].Pgpu);
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Rema;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomArbeitsGruppenTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomArbeitsGruppenTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString ArbeitsgruppenTableViewer::Format(ARBEITSGRUPPENTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool ArbeitsgruppenTableViewer::FindArbeitsgruppen(char *pcpArbeitsgruppenKeya, char *pcpArbeitsgruppenKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpArbeitsgruppenKeya) &&
			 (omLines[ilItem].Keyd == pcpArbeitsgruppenKeyd)    )
			return TRUE;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void ArbeitsgruppenTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    ArbeitsgruppenTableViewer *polViewer = (ArbeitsgruppenTableViewer *)popInstance;
    if (ipDDXType == WGP_CHANGE || ipDDXType == WGP_NEW) 
		polViewer->ProcessArbeitsgruppenChange((WGPDATA *)vpDataPointer);
    else if (ipDDXType == WGP_DELETE) 
		polViewer->ProcessArbeitsgruppenDelete((WGPDATA *)vpDataPointer);
    else if (ipDDXType == PGP_CHANGE) 
		polViewer->ProcessPlanungsgruppenChange((PGPDATA *)vpDataPointer);
    else if (ipDDXType == PGP_DELETE) 
		polViewer->ProcessPlanungsgruppenChange((PGPDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void ArbeitsgruppenTableViewer::ProcessPlanungsgruppenChange(PGPDATA *prpPlanungsgruppen)
{
	CString olPgpUrno;
	bool blChanged = false;

	if (prpPlanungsgruppen == NULL)
		return;

	olPgpUrno.Format("%ld",prpPlanungsgruppen->Urno);

	for (int ilItem = 0; ilItem < omLines.GetSize(); ilItem++)
	{
        ARBEITSGRUPPENTABLE_LINEDATA *prlLine = &omLines[ilItem];
		if (prlLine->Pgpu == olPgpUrno) 
		{
			TABLE_COLUMN rlColumn;
			CCSPtrArray <TABLE_COLUMN> olLine;

			rlColumn.Lineno = ilItem;
			rlColumn.VerticalSeparator = SEPA_NONE;
			rlColumn.SeparatorType = SEPA_NONE;
			rlColumn.BkColor = WHITE;
			rlColumn.Font = &ogCourier_Regular_10;
			rlColumn.Alignment = COLALIGN_LEFT;

			rlColumn.Text = prlLine->Wgpc;
			olLine.NewAt(olLine.GetSize(), rlColumn);
			rlColumn.Text = prlLine->Wgpn;
			olLine.NewAt(olLine.GetSize(), rlColumn);
			rlColumn.Text = GetPgpNameByUrno(prlLine->Pgpu);
			olLine.NewAt(olLine.GetSize(), rlColumn);
			rlColumn.Text = prlLine->Rema;
			olLine.NewAt(olLine.GetSize(), rlColumn);

			pomArbeitsGruppenTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
			olLine.DeleteAll();
			blChanged = true;
		}
	}
	if (blChanged)
		pomArbeitsGruppenTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

void ArbeitsgruppenTableViewer::ProcessArbeitsgruppenChange(WGPDATA *prpArbeitsgruppen)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpArbeitsgruppen->Wgpc;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpArbeitsgruppen->Wgpn;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	CString olPgpu;
	olPgpu.Format("%ld",prpArbeitsgruppen->Pgpu);
	rlColumn.Text = GetPgpNameByUrno(olPgpu);
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpArbeitsgruppen->Rema;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	
	if (FindLine(prpArbeitsgruppen->Urno, ilItem))
	{
        ARBEITSGRUPPENTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpArbeitsgruppen->Urno;
		prlLine->Wgpc = prpArbeitsgruppen->Wgpc;
		prlLine->Wgpn = prpArbeitsgruppen->Wgpn;
		prlLine->Pgpu.Format("%ld",prpArbeitsgruppen->Pgpu);
		prlLine->Rema = prpArbeitsgruppen->Rema;


		pomArbeitsGruppenTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomArbeitsGruppenTable->DisplayTable();
	}
	else
	{
		MakeLine(prpArbeitsgruppen);
		if (FindLine(prpArbeitsgruppen->Urno, ilItem))
		{
	        ARBEITSGRUPPENTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomArbeitsGruppenTable->AddTextLine(olLine, (void *)prlLine);
				pomArbeitsGruppenTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void ArbeitsgruppenTableViewer::ProcessArbeitsgruppenDelete(WGPDATA *prpArbeitsgruppen)
{
	int ilItem;
	if (FindLine(prpArbeitsgruppen->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomArbeitsGruppenTable->DeleteTextLine(ilItem);
		pomArbeitsGruppenTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool ArbeitsgruppenTableViewer::IsPassFilter(WGPDATA *prpArbeitsgruppen)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int ArbeitsgruppenTableViewer::CompareArbeitsgruppen(ARBEITSGRUPPENTABLE_LINEDATA *prpArbeitsgruppen1, ARBEITSGRUPPENTABLE_LINEDATA *prpArbeitsgruppen2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpArbeitsgruppen1->Tifd == prpArbeitsgruppen2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpArbeitsgruppen1->Tifd > prpArbeitsgruppen2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void ArbeitsgruppenTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool ArbeitsgruppenTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void ArbeitsgruppenTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void ArbeitsgruppenTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING199);
	int ilOrientation = PRINT_LANDSCAPE;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool ArbeitsgruppenTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool ArbeitsgruppenTableViewer::PrintTableLine(ARBEITSGRUPPENTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;

			switch(i)
			{
			case 0:
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text		 = prpLine->Wgpc;
				break;
			case 1:
				rlElement.Text		 = prpLine->Wgpn;
				break;
			case 2:
				rlElement.Text		 = GetPgpNameByUrno(prpLine->Pgpu);
				break;
			case 3:
				rlElement.FrameRight = PRINT_FRAMETHIN;
				rlElement.Text		 = prpLine->Rema;
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
CString ArbeitsgruppenTableViewer::GetPgpNameByUrno(CString opUrno)
{
	CString olTmp;
	long llUrno = atol(opUrno);

	PGPDATA *prlPgp = ogPgpData.GetPgpByUrno(llUrno);
	if (prlPgp != NULL)
	{
		olTmp = prlPgp->Pgpc + CString(" (") + prlPgp->Pgpn + CString(")");
	}

	return olTmp;
}
//-----------------------------------------------------------------------------------------------
