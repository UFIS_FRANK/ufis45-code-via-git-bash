#if !defined(AFX_POOLDLG_H__B35519F1_3320_11D1_B39D_0000C016B067__INCLUDED_)
#define AFX_POOLDLG_H__B35519F1_3320_11D1_B39D_0000C016B067__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// ParameterDlg.h : header file
//
#include <CedaPolData.h>
#include <CCSEdit.h>
/////////////////////////////////////////////////////////////////////////////
// PoolDlg dialog

class PoolDlg : public CDialog
{
// Construction
public:
	PoolDlg(POLDATA *popPol, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(ParameterDlg)
	enum { IDD = IDD_POOLDLG };
	CButton	m_OK;
	CString	m_Caption;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEU;
	CCSEdit	m_USEC;
	CCSEdit	m_NAME;
	CCSEdit	m_POOL;
	CCSEdit	m_DTEL;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ParameterDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(ParameterDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

	POLDATA *pomPol;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_POOLDLG_H__B35519F1_3320_11D1_B39D_0000C016B067__INCLUDED_)
