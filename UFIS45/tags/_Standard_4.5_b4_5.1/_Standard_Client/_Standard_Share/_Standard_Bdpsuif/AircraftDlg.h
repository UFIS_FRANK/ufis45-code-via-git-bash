#if !defined(AFX_AIRCRAFTDLG_H__771D42C3_1DEB_11D1_B387_0000C016B067__INCLUDED_)
#define AFX_AIRCRAFTDLG_H__771D42C3_1DEB_11D1_B387_0000C016B067__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// AircraftDlg.h : header file
//

#include <CedaACTData.h>
#include <CCSEdit.h>
#include <CCSTable.h>
#include <CCSComboBox.h>
#include <CedaEntData.h>
#include <AwDlg.h>

/////////////////////////////////////////////////////////////////////////////
// AircraftDlg dialog

class AircraftDlg : public CDialog
{
// Construction
public:
	AircraftDlg(ACTDATA *popACT, CWnd* pParent = NULL);

// Dialog Data
	//{{AFX_DATA(AircraftDlg)
	enum { IDD = IDD_AIRCRAFTDLG };
	CCSButtonCtrl	m_BLACK_LIST;
	CCSEdit	m_MING;
	CButton	m_CANCEL;
	CButton	m_OK;
	CButton	m_NADS;
	CString	m_Caption;
	CCSEdit	m_ACFN;
	CCSEdit	m_ACHE;
	CCSEdit	m_ACLE;
	CCSEdit	m_ACWS;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_ENNO;
	CCSEdit	m_ENTY;
	CCSEdit	m_GRUP;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEU;
	CCSEdit	m_USEC;
	CCSEdit	m_SEAT;
	CCSEdit	m_SEAF;
	CCSEdit	m_SEAB;
	CCSEdit	m_SEAE;
	CCSEdit	m_ACT3;
	CCSEdit	m_ACT5;
	CCSEdit	m_VAFRD;
	CCSEdit	m_VAFRT;
	CCSEdit	m_VATOD;
	CCSEdit	m_VATOT;
	CCSEdit	m_ACTI;
	CCSEdit	m_AFMC;
	CCSEdit	m_ALTC;
	CCSEdit	m_MODC;
	CCSEdit	m_ACBT;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(AircraftDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(AircraftDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnBSelEngine();
	afx_msg void OnBSelAlt3();
	afx_msg void OnBSelAfm();
	afx_msg void OnBlackList();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:

	CCSTable *pomTable;
	ACTDATA *pomACT;
	DWORD mdBlackListProcId;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_AIRCRAFTDLG_H__771D42C3_1DEB_11D1_B387_0000C016B067__INCLUDED_)
