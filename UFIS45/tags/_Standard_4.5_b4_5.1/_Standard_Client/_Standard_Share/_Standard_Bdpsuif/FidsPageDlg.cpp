// FidsPageDlg.cpp : implementation file
//

#include <stdafx.h>
#include <BDPSUIF.h>
#include <FidsPageDlg.h>
#include <PrivList.h>
#include <AwDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// FidsPageDlg dialog


FidsPageDlg::FidsPageDlg(PAGDATA *popPAG,const CString& ropCaption,CWnd* pParent /*=NULL*/)
	: CDialog(FidsPageDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(FidsPageDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	pomPAG	  = popPAG;
	m_Caption = ropCaption;
}


void FidsPageDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(FidsPageDlg)
	DDX_Control(pDX, IDC_EDIT_TIME_FRAME_END, m_PTFE);
	DDX_Control(pDX, IDC_EDIT_TIME_FRAME_BEGIN, m_PTFB);
	DDX_Control(pDX, IDC_EDIT_NO_OF_FLIGHTS, m_PNOF);
	DDX_Control(pDX, IDC_EDIT_NATURE, m_PFNT);
	DDX_Control(pDX, IDC_EDIT_NAME, m_PAGI);
	DDX_Control(pDX, IDC_EDIT_FIELD_NAME_PTD2, m_PRFN);
	DDX_Control(pDX, IDC_EDIT_DISPLAY_TYPE_INTERNAL, m_PDTI);
	DDX_Control(pDX, IDC_EDIT_DISPLAY_TYPE, m_PDPT);
	DDX_Control(pDX, IDC_EDIT_DISPLAY_SEQUENCE, m_PDSS);
	DDX_Control(pDX, IDC_EDIT_DELETE_AFTER_TIFA, m_PTD1);
	DDX_Control(pDX, IDC_EDIT_DELETE_AFTER_PRFN, m_PTD2);
	DDX_Control(pDX, IDC_EDIT_CONFIGURATION, m_PCFN);
	DDX_Control(pDX, IDC_EDIT_CAROUSEL_TIME, m_PCTI);
	DDX_Control(pDX, IDC_EDIT_CANCELLED_AFTER_TIFA, m_PTDC);
	DDX_Control(pDX, IDC_EDIT_COTB, m_COTB);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEU,	 m_USEU);
	DDX_Control(pDX, IDC_USEC,	 m_USEC);
	DDX_Control(pDX, IDC_REMA,	 m_REMA);
	DDX_Control(pDX, IDCANCEL, m_CANCEL);
	DDX_Control(pDX, IDOK, m_OK);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(FidsPageDlg, CDialog)
	//{{AFX_MSG_MAP(FidsPageDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// FidsPageDlg message handlers

BOOL FidsPageDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_Caption = LoadStg(IDS_STRING930) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	char clStat = ogPrivList.GetStat("PAGEDLG.m_OK");
	SetWndStatAll(clStat,m_OK);
	
	m_PAGI.SetTypeToString("X(12)",12,1);
	m_PAGI.SetTextLimit(1,12,false);
	m_PAGI.SetTextErrColor(RED);
	m_PAGI.SetBKColor(YELLOW);
	m_PAGI.SetInitText(pomPAG->Pagi);
	m_PAGI.SetSecState(ogPrivList.GetStat("PAGEDLG.m_PAGI"));

	m_PCFN.SetTypeToString("X(12)",12,1);
	m_PCFN.SetTextLimit(1,12,false);
	m_PCFN.SetTextErrColor(RED);
	m_PCFN.SetBKColor(YELLOW);
	m_PCFN.SetInitText(pomPAG->Pcfn);
	m_PCFN.SetSecState(ogPrivList.GetStat("PAGEDLG.m_PCFN"));

	//------------------------------------
	m_PDPT.SetFormat("A(1)");
	m_PDPT.SetTextErrColor(RED);
	m_PDPT.SetBKColor(YELLOW);
	m_PDPT.SetInitText(pomPAG->Pdpt);
	m_PDPT.SetSecState(ogPrivList.GetStat("PAGEDLG.m_PDPT"));

	//------------------------------------
	m_PDTI.SetFormat("A(3)");
	m_PDTI.SetTextLimit(-1,3,true);
	m_PDTI.SetTextErrColor(RED);
	m_PDTI.SetInitText(pomPAG->Pdti);
	m_PDTI.SetSecState(ogPrivList.GetStat("PAGEDLG.m_PDTI"));

	//------------------------------------
	m_PNOF.SetRegularExpression("([0-9]{1,2}\\.[0-9]{1,2})|([0-9]{1,5})");
	m_PNOF.SetTextLimit(-1,5,true);
	m_PNOF.SetTextErrColor(RED);
	m_PNOF.SetInitText(pomPAG->Pnof);
	m_PNOF.SetSecState(ogPrivList.GetStat("PAGEDLG.m_PNOF"));

	//------------------------------------
	m_PFNT.SetFormat("A(1)");
	m_PFNT.SetTextLimit(-1,1,true);
	m_PFNT.SetTextErrColor(RED);
	m_PFNT.SetInitText(pomPAG->Pfnt);
	m_PFNT.SetSecState(ogPrivList.GetStat("PAGEDLG.m_PFNT"));

	//------------------------------------
	m_PDSS.SetFormat("X(2)");
	m_PDSS.SetTextLimit(-1,2,true);
	m_PDSS.SetTextErrColor(RED);
	m_PDSS.SetInitText(pomPAG->Pdss);
	m_PDSS.SetSecState(ogPrivList.GetStat("PAGEDLG.m_PDSS"));

	//------------------------------------
	m_PCTI.SetFormat("#(2)");
	m_PCTI.SetTextLimit(-1,2,true);
	m_PCTI.SetTextErrColor(RED);
	m_PCTI.SetInitText(pomPAG->Pcti);
	m_PCTI.SetSecState(ogPrivList.GetStat("PAGEDLG.m_PCTI"));

	//------------------------------------
	m_PTFB.SetTypeToInt(-999,9999);
	m_PTFB.SetTextErrColor(RED);
	m_PTFB.SetInitText(pomPAG->Ptfb);
	m_PTFB.SetSecState(ogPrivList.GetStat("PAGEDLG.m_PTFB"));

	//------------------------------------
	m_PTFE.SetTypeToInt(-999,9999);
	m_PTFE.SetTextErrColor(RED);
	m_PTFE.SetInitText(pomPAG->Ptfe);
	m_PTFE.SetSecState(ogPrivList.GetStat("PAGEDLG.m_PTFE"));

	//------------------------------------
	m_PTD1.SetTypeToInt(-999,9999);
	m_PTD1.SetTextErrColor(RED);
	m_PTD1.SetInitText(pomPAG->Ptd1);
	m_PTD1.SetSecState(ogPrivList.GetStat("PAGEDLG.m_PTD1"));

	//------------------------------------
	m_PTD2.SetTypeToInt(-999,9999);
	m_PTD2.SetTextErrColor(RED);
	m_PTD2.SetInitText(pomPAG->Ptd2);
	m_PTD2.SetSecState(ogPrivList.GetStat("PAGEDLG.m_PTD2"));

	//------------------------------------
	m_PRFN.SetFormat("x(4)");
	m_PRFN.SetTextLimit(-1,4,true);
	m_PRFN.SetTextErrColor(RED);
	m_PRFN.SetInitText(pomPAG->Prfn);
	m_PRFN.SetSecState(ogPrivList.GetStat("PAGEDLG.m_PRFN"));

	//------------------------------------
	m_PTDC.SetTypeToInt(-999,9999);
	m_PTDC.SetTextErrColor(RED);
	m_PTDC.SetInitText(pomPAG->Ptdc);
	m_PTDC.SetSecState(ogPrivList.GetStat("PAGEDLG.m_PTDC"));

	//------------------------------------
	m_COTB.SetFormat("#(3)");
	m_COTB.SetTextLimit(-1,3,true);
	m_COTB.SetTextErrColor(RED);
	m_COTB.SetInitText(pomPAG->Cotb);
	m_COTB.SetSecState(ogPrivList.GetStat("PAGEDLG.m_COTB"));

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);			
	m_CDATD.SetInitText(pomPAG->Cdat.Format("%d.%m.%Y"));
	m_CDATD.SetSecState(ogPrivList.GetStat("PAGEDLG.m_CDAT"));
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomPAG->Cdat.Format("%H:%M"));
	m_CDATT.SetSecState(ogPrivList.GetStat("PAGEDLG.m_CDAT"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomPAG->Lstu.Format("%d.%m.%Y"));
	m_LSTUD.SetSecState(ogPrivList.GetStat("PAGEDLG.m_LSTU"));
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomPAG->Lstu.Format("%H:%M"));
	m_LSTUT.SetSecState(ogPrivList.GetStat("PAGEDLG.m_LSTU"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomPAG->Usec);
	m_USEC.SetSecState(ogPrivList.GetStat("PAGEDLG.m_USEC"));
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomPAG->Useu);
	m_USEU.SetSecState(ogPrivList.GetStat("PAGEDLG.m_USEU"));
	//------------------------------------
	
	if(ogPAGData.DoesRemarkCodeExist() == true)
	{
		m_REMA.SetTypeToString("X(512)",512,1);
		m_REMA.SetBKColor(WHITE);
		m_REMA.SetTextErrColor(RED);
		m_REMA.SetInitText(pomPAG->Rema);
		m_REMA.SetSecState(ogPrivList.GetStat("PAGEDLG.m_REMA"));
	}
	else
	{
		ResizeDialog();
	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void FidsPageDlg::OnOK() 
{
	// TODO: Add extra validation here
	CWaitCursor olWait;
	CString olErrorText;
	bool ilStatus = true;

	if(m_PAGI.GetStatus() == false)
	{
		ilStatus = false;
		if(m_PAGI.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING964) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING964) + ogNotFormat;
		}
	}

	if(m_PCFN.GetStatus() == false)
	{
		ilStatus = false;
		if(m_PCFN.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING965) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING965) + ogNotFormat;
		}
	}

	if(m_PDPT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_PDPT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING966) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING966) + ogNotFormat;
		}
	}

	if(m_PDTI.GetStatus() == false)
	{
		ilStatus = false;
		if(m_PDTI.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING967) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING967) + ogNotFormat;
		}
	}

	if(m_PNOF.GetStatus() == false)
	{
		ilStatus = false;
		if(m_PNOF.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING968) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING968) + ogNotFormat;
		}
	}

	if(m_PFNT.GetStatus() == false)
	{
		ilStatus = false;
		if(m_PFNT.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING969) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING969) + ogNotFormat;
		}
	}

	if(m_PDSS.GetStatus() == false)
	{
		ilStatus = false;
		if(m_PDSS.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING970) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING970) + ogNotFormat;
		}
	}

	if(m_PCTI.GetStatus() == false)
	{
		ilStatus = false;
		if(m_PCTI.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING971) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING971) + ogNotFormat;
		}
	}

	if(m_PTFB.GetStatus() == false)
	{
		ilStatus = false;
		if(m_PTFB.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING972) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING972) + ogNotFormat;
		}
	}

	if(m_PTFE.GetStatus() == false)
	{
		ilStatus = false;
		if(m_PTFE.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING973) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING973) + ogNotFormat;
		}
	}

	if(m_PTD1.GetStatus() == false)
	{
		ilStatus = false;
		if(m_PTD1.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING974) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING974) + ogNotFormat;
		}
	}

	if(m_PTD2.GetStatus() == false)
	{
		ilStatus = false;
		if(m_PTD2.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING975) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING975) + ogNotFormat;
		}
	}

	if(m_PRFN.GetStatus() == false)
	{
		ilStatus = false;
		if(m_PRFN.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING976) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING976) + ogNotFormat;
		}
	}

	if(m_PTDC.GetStatus() == false)
	{
		ilStatus = false;
		if(m_PTDC.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING977) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING977) + ogNotFormat;
		}
	}

	if(m_COTB.GetStatus() == false)
	{
		ilStatus = false;
		if(m_COTB.GetWindowTextLength() == 0)
		{
			olErrorText += LoadStg(IDS_STRING1020) +  ogNoData;
		}
		else
		{
			olErrorText += LoadStg(IDS_STRING1020) + ogNotFormat;
		}
	}


	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olPagi;
		char clWhere[100];
		CCSPtrArray<PAGDATA> olPAGCPA;
		m_PAGI.GetWindowText(olPagi);
		if (olPagi.GetLength() > 0)
		{
			sprintf(clWhere,"WHERE PAGI='%s'",olPagi);
			if(ogPAGData.ReadSpecialData(&olPAGCPA,clWhere,"URNO,PAGI",false) == true)
			{
				if(olPAGCPA[0].Urno != pomPAG->Urno)
				{
					ilStatus = false;
					olErrorText += LoadStg(IDS_STRING288) + LoadStg(IDS_EXIST);
				}
			}
			olPAGCPA.DeleteAll();
		}
	}

	AfxGetApp()->DoWaitCursor(-1);
	if (ilStatus == true)
	{
		m_PAGI.GetWindowText(pomPAG->Pagi,sizeof(pomPAG->Pagi));
		m_PCFN.GetWindowText(pomPAG->Pcfn,sizeof(pomPAG->Pcfn));
		m_PDPT.GetWindowText(pomPAG->Pdpt,sizeof(pomPAG->Pdpt));
		m_PDTI.GetWindowText(pomPAG->Pdti,sizeof(pomPAG->Pdti));
		m_PNOF.GetWindowText(pomPAG->Pnof,sizeof(pomPAG->Pnof));
		m_PFNT.GetWindowText(pomPAG->Pfnt,sizeof(pomPAG->Pfnt));
		m_PDSS.GetWindowText(pomPAG->Pdss,sizeof(pomPAG->Pdss));
		m_PCTI.GetWindowText(pomPAG->Pcti,sizeof(pomPAG->Pcti));
		m_PTFB.GetWindowText(pomPAG->Ptfb,sizeof(pomPAG->Ptfb));
		m_PTFE.GetWindowText(pomPAG->Ptfe,sizeof(pomPAG->Ptfe));
		m_PTD1.GetWindowText(pomPAG->Ptd1,sizeof(pomPAG->Ptd1));
		m_PTD2.GetWindowText(pomPAG->Ptd2,sizeof(pomPAG->Ptd2));
		m_PRFN.GetWindowText(pomPAG->Prfn,sizeof(pomPAG->Prfn));
		m_PTDC.GetWindowText(pomPAG->Ptdc,sizeof(pomPAG->Ptdc));
		m_COTB.GetWindowText(pomPAG->Cotb,sizeof(pomPAG->Cotb));

		if(ogPAGData.DoesRemarkCodeExist() == true)
		{
			m_REMA.GetWindowText(pomPAG->Rema,sizeof(pomPAG->Rema));
		}

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_PAGI.SetFocus();
		m_PAGI.SetSel(0,-1);

	}

}

void FidsPageDlg::ResizeDialog()
{
	CRect olWindowRect;
	CRect olRemarkRect;
	CRect olCreatedRect;
	int ilOffset = 0;
	
	
	((CWnd*)GetDlgItem(IDC_REMA))->GetWindowRect(olRemarkRect);
	ScreenToClient(olRemarkRect);

	((CWnd*)GetDlgItem(IDC_CDAT_D))->GetWindowRect(olCreatedRect);
	ScreenToClient(olCreatedRect);

	ilOffset = olCreatedRect.top - olRemarkRect.top;
	CRect olDeflateRect(0,-ilOffset,0,ilOffset);	
	this->GetWindowRect(&olWindowRect);
	olWindowRect.bottom -= ilOffset;
	this->MoveWindow(olWindowRect);	

	((CWnd*)GetDlgItem(IDC_STATIC_REMA))->ShowWindow(SW_HIDE);
	((CWnd*)GetDlgItem(IDC_REMA))->ShowWindow(SW_HIDE);
	
	((CWnd*)GetDlgItem(IDC_GROUP_INFO))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.bottom -= ilOffset;
	((CWnd*)GetDlgItem(IDC_GROUP_INFO))->MoveWindow(olWindowRect);

	((CWnd*)GetDlgItem(IDC_STATIC_CREATED_ON))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CWnd*)GetDlgItem(IDC_STATIC_CREATED_ON))->MoveWindow(olWindowRect);

	((CWnd*)GetDlgItem(IDC_STATIC_CREATED_BY))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CWnd*)GetDlgItem(IDC_STATIC_CREATED_BY))->MoveWindow(olWindowRect);

	((CWnd*)GetDlgItem(IDC_STATIC_CHANGED_ON))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CWnd*)GetDlgItem(IDC_STATIC_CHANGED_ON))->MoveWindow(olWindowRect);

	((CWnd*)GetDlgItem(IDC_STATIC_CHANGED_BY))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CWnd*)GetDlgItem(IDC_STATIC_CHANGED_BY))->MoveWindow(olWindowRect);

	((CWnd*)GetDlgItem(IDC_CDAT_D))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CWnd*)GetDlgItem(IDC_CDAT_D))->MoveWindow(olWindowRect);

	((CWnd*)GetDlgItem(IDC_CDAT_T))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CWnd*)GetDlgItem(IDC_CDAT_T))->MoveWindow(olWindowRect);

	((CWnd*)GetDlgItem(IDC_LSTU_D))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CWnd*)GetDlgItem(IDC_LSTU_D))->MoveWindow(olWindowRect);

	((CWnd*)GetDlgItem(IDC_LSTU_T))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CWnd*)GetDlgItem(IDC_LSTU_T))->MoveWindow(olWindowRect);

	((CWnd*)GetDlgItem(IDC_USEC))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CWnd*)GetDlgItem(IDC_USEC))->MoveWindow(olWindowRect);

	((CWnd*)GetDlgItem(IDC_USEU))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CWnd*)GetDlgItem(IDC_USEU))->MoveWindow(olWindowRect);

	((CWnd*)GetDlgItem(IDOK))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CWnd*)GetDlgItem(IDOK))->MoveWindow(olWindowRect);

	((CWnd*)GetDlgItem(IDCANCEL))->GetWindowRect(olWindowRect);
	ScreenToClient(olWindowRect);
	olWindowRect.DeflateRect(olDeflateRect);
	((CWnd*)GetDlgItem(IDCANCEL))->MoveWindow(olWindowRect);

}
