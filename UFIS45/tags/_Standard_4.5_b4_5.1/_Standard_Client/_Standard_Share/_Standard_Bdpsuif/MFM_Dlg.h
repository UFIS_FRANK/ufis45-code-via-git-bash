#if !defined(AFX_MFM_DLG_H__2FE447E3_E6DD_11D4_BFF8_00D0B7E2A4B5__INCLUDED_)
#define AFX_MFM_DLG_H__2FE447E3_E6DD_11D4_BFF8_00D0B7E2A4B5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MFM_Dlg.h : header file
//
#include <PrivList.h>
#include <CedaMfmData.h>
#include <CCSEdit.h>
#include <CedaCotData.h>
#include <AwDlg.h>

/////////////////////////////////////////////////////////////////////////////
// MFM_Dlg dialog

class MFM_Dlg : public CDialog
{
// Construction
public:
	MFM_Dlg(MFMDATA *popMfm, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(MFM_Dlg)
	enum { IDD = IDD_MFMDLG };
	CCSEdit	m_FM01;
	CCSEdit	m_FM02;
	CCSEdit	m_FM03;
	CCSEdit	m_FM04;
	CCSEdit	m_FM05;
	CCSEdit	m_FM06;
	CCSEdit	m_FM07;
	CCSEdit	m_FM08;
	CCSEdit	m_FM09;
	CCSEdit	m_FM10;
	CCSEdit	m_FM11;
	CCSEdit	m_FM12;
	CButton	m_OK;
	CCSEdit	m_CDATD;
	CCSEdit	m_CDATT;
	CCSEdit	m_DPT1;
	CCSEdit	m_LSTUD;
	CCSEdit	m_LSTUT;
	CCSEdit	m_USEU;
	CString m_Caption;
	CCSEdit	m_USEC;
	CCSEdit	m_YEAR;

	
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(MFM_Dlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

private:
	MFMDATA *pomMfm;
// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(MFM_Dlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnBOrgAw();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MFM_DLG_H__2FE447E3_E6DD_11D4_BFF8_00D0B7E2A4B5__INCLUDED_)
