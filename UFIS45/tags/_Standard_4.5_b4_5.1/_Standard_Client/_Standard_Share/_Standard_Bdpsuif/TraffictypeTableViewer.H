#ifndef __TRAFFICTYPETABLEVIEWER_H__
#define __TRAFFICTYPETABLEVIEWER_H__

#include <stdafx.h>
#include <CedaNATData.h>
#include <CCSTable.h>
#include <CCSPrint.h>
#include <CViewer.h>


struct TRAFFICTYPETABLE_LINEDATA
{
	long	Urno;
	CString	Ttyp;
	CString	Tnam;
	CString	Vafr;
	CString	Vato;
	CString Styp;
	CString Flti;
	CString Apc4;
	CString Alc3;
};

/////////////////////////////////////////////////////////////////////////////
// TraffictypeTableViewer

class TraffictypeTableViewer : public CViewer
{
// Constructions
public:
    TraffictypeTableViewer(CCSPtrArray<NATDATA> *popData);
    ~TraffictypeTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(NATDATA *prpTraffictype);
	int CompareTraffictype(TRAFFICTYPETABLE_LINEDATA *prpTraffictype1, TRAFFICTYPETABLE_LINEDATA *prpTraffictype2);
    void MakeLines();
	void MakeLine(NATDATA *prpTraffictype);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(TRAFFICTYPETABLE_LINEDATA *prpTraffictype);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(TRAFFICTYPETABLE_LINEDATA *prpLine);
	void ProcessTraffictypeChange(NATDATA *prpTraffictype);
	void ProcessTraffictypeDelete(NATDATA *prpTraffictype);
	bool FindTraffictype(char *prpTraffictypeKeya, char *prpTraffictypeKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomTraffictypeTable;
	CCSPtrArray<NATDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<TRAFFICTYPETABLE_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(TRAFFICTYPETABLE_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__TRAFFICTYPETABLEVIEWER_H__
