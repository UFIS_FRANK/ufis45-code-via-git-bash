#ifndef __AADTABLEVIEWER_H__
#define __AADTABLEVIEWER_H__

#include <stdafx.h>
#include <CedaAADDATA.h>
#include <CCSTable.h>
#include <CViewer.h>
#include <CCSPrint.h>


struct AAD_LINEDATA
{
	long	Urno;
	CString	Deid;
	CString	Name;
	CString	Moti;
	CString	Vafr;
	CString	Vato;
};

/////////////////////////////////////////////////////////////////////////////
// AadTableViewer

class AadTableViewer : public CViewer
{
// Constructions
public:
    AadTableViewer(CCSPtrArray<AADDATA> *popData);
    ~AadTableViewer();

    void Attach(CCSTable *popAttachWnd);
    virtual void ChangeViewTo(const char *pcpViewName);
// Internal data processing routines
private:
	bool IsPassFilter(AADDATA *prpAad);
	int CompareAad(AAD_LINEDATA *prpAad1, AAD_LINEDATA *prpAad2);
    void MakeLines();
	void MakeLine(AADDATA *prpAad);
	bool FindLine(long lpUrno, int &rilLineno);
// Operations
public:
	void DeleteAll();
	void CreateLine(AAD_LINEDATA *prpAad);
	void DeleteLine(int ipLineno);
// Window refreshing routines
public:
	void UpdateDisplay();
	CString Format(AAD_LINEDATA *prpLine);
	void ProcessAadChange(AADDATA *prpAad);
	void ProcessAadDelete(AADDATA *prpAad);
	bool FindAad(char *prpAadKeya, char *prpAadKeyd, int& ilItem);
// Attributes used for filtering condition
private:
	bool bmIsFromSearch;
// Attributes
private:
    CCSTable *pomAadTable;
	CCSPtrArray<AADDATA> *pomData;
// Methods which handle changes (from Data Distributor)
public:
    CCSPtrArray<AAD_LINEDATA> omLines;
///////
	CCSPtrArray<TABLE_HEADER_COLUMN> omHeaderDataArray;
//Print 
	void PrintTableView();
	bool PrintTableLine(AAD_LINEDATA *prpLine,bool bpLastLine);
	bool PrintTableHeader();
	CCSPrint *pomPrint;
	//CBitmap omBitmap;
	CString omTableName;

};

#endif //__AADTABLEVIEWER_H__
