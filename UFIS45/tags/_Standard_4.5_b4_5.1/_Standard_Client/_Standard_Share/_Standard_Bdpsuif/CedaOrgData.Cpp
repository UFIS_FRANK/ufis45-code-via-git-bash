// CedaOrgData.cpp
 
#include <stdafx.h>
#include <CedaOrgData.h>
#include <resource.h>
#include <CedaBasicData.h>


void ProcessOrgCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//--CEDADATA-----------------------------------------------------------------------------------------------

CedaOrgData::CedaOrgData() : CCSCedaData(&ogCommHandler)
{
    BEGIN_CEDARECINFO(ORGDATA, OrgDataRecInfo)
		FIELD_DATE		(Cdat,"CDAT")
		FIELD_CHAR_TRIM	(Dpt1,"DPT1")
		FIELD_CHAR_TRIM	(Dpt2,"DPT2")
		FIELD_CHAR_TRIM	(Dptn,"DPTN")
		FIELD_DATE		(Lstu,"LSTU")
		FIELD_CHAR_TRIM	(Prfl,"PRFL")
		FIELD_CHAR_TRIM	(Rema,"REMA")
		FIELD_LONG		(Urno,"URNO")
		FIELD_CHAR_TRIM	(Usec,"USEC")
		FIELD_CHAR_TRIM	(Useu,"USEU")
		FIELD_CHAR_TRIM	(Odgl,"ODGL")
		FIELD_CHAR_TRIM	(Odkl,"ODKL")
		FIELD_CHAR_TRIM	(Odsl,"ODSL")
	END_CEDARECINFO

    // Copy the record structure
    for (int i = 0; i < sizeof(OrgDataRecInfo)/sizeof(OrgDataRecInfo[0]); i++)
	{
		CEDARECINFO *prpCedaRecInfo = new CEDARECINFO;
		memcpy(prpCedaRecInfo,&OrgDataRecInfo[i],sizeof(CEDARECINFO));
        omRecInfo.Add(prpCedaRecInfo);
	}
    // Initialize table names and field names
    strcpy(pcmTableName,"ORG");
    sprintf(pcmListOfFields,"CDAT,DPT1,DPT2,DPTN,LSTU,PRFL,REMA,URNO,USEC,USEU,ODGL,ODKL,ODSL");

	pcmFieldList = pcmListOfFields; //because in CedaData pcmFieldList is just a pointer

	omData.RemoveAll();
}

//---------------------------------------------------------------------------------------------------------

void CedaOrgData::GetDataInfo(CStringArray &ropFields, CStringArray &ropDesription, CStringArray &ropType)
{
	ropFields.Add("CDAT");
	ropFields.Add("DPT1");
	ropFields.Add("DPT2");
	ropFields.Add("DPTN");
	ropFields.Add("LSTU");
	ropFields.Add("PRFL");
	ropFields.Add("REMA");
	ropFields.Add("URNO");
	ropFields.Add("USEC");
	ropFields.Add("USEU");
	ropFields.Add("ODGL");
	ropFields.Add("ODKL");
	ropFields.Add("ODSL");

	ropDesription.Add(LoadStg(IDS_STRING343));
	ropDesription.Add(LoadStg(IDS_STRING237));
	ropDesription.Add(LoadStg(IDS_STRING463));
	ropDesription.Add(LoadStg(IDS_STRING229));
	ropDesription.Add(LoadStg(IDS_STRING344));
	ropDesription.Add(LoadStg(IDS_STRING345));
	ropDesription.Add(LoadStg(IDS_STRING238));
	ropDesription.Add(LoadStg(IDS_STRING346));
	ropDesription.Add(LoadStg(IDS_STRING347));
	ropDesription.Add(LoadStg(IDS_STRING348));

	ropDesription.Add(LoadStg(IDS_STRING743));
	ropDesription.Add(LoadStg(IDS_STRING744));
	ropDesription.Add(LoadStg(IDS_STRING745));

	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("Date");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");
	ropType.Add("String");

}

//--REGISTER----------------------------------------------------------------------------------------------

void CedaOrgData::Register(void)
{
	ogDdx.Register((void *)this,BC_ORG_CHANGE,	CString("ORGDATA"), CString("Org-changed"),	ProcessOrgCf);
	ogDdx.Register((void *)this,BC_ORG_NEW,		CString("ORGDATA"), CString("Org-new"),		ProcessOrgCf);
	ogDdx.Register((void *)this,BC_ORG_DELETE,	CString("ORGDATA"), CString("Org-deleted"),	ProcessOrgCf);
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaOrgData::~CedaOrgData(void)
{
	omRecInfo.DeleteAll();
	ClearAll();
}

//--CLEAR-ALL----------------------------------------------------------------------------------------------

void CedaOrgData::ClearAll(bool bpWithRegistration)
{
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(bpWithRegistration)
	{
		ogDdx.UnRegister(this,NOTUSED);
	}
}

//--READ-ALL-----------------------------------------------------------------------------------------------

bool CedaOrgData::Read(char *pspWhere /*NULL*/)
{
    // Select data from the database
	bool ilRc = true;
    omUrnoMap.RemoveAll();
    omData.DeleteAll();
	if(pspWhere == NULL)
	{	
		ilRc = CedaAction("RT");
	}
	else
	{
		ilRc = CedaAction("RT", pspWhere);
	}
	if (ilRc != true)
	{
		return ilRc;
	}
	// Load data from CedaData into the dynamic array of record
	for (int ilLc = 0; ilRc == true; ilLc++)
	{
		ORGDATA *prlOrg = new ORGDATA;
		if ((ilRc = GetFirstBufferRecord(prlOrg)) == true)
		{
			omData.Add(prlOrg);//Update omData
			omUrnoMap.SetAt((void *)prlOrg->Urno,prlOrg);
		}
		else
		{
			delete prlOrg;
		}
	}
    return true;
}

//--INSERT-------------------------------------------------------------------------------------------------

bool CedaOrgData::Insert(ORGDATA *prpOrg)
{
	prpOrg->IsChanged = DATA_NEW;
	if(Save(prpOrg) == false) return false; //Update Database
	InsertInternal(prpOrg);
    return true;
}

//--INSERT-INTERNAL--------------------------------------------------------------------------------------

bool CedaOrgData::InsertInternal(ORGDATA *prpOrg)
{
	ogDdx.DataChanged((void *)this, ORG_NEW,(void *)prpOrg ); //Update Viewer
	omData.Add(prpOrg);//Update omData
	omUrnoMap.SetAt((void *)prpOrg->Urno,prpOrg);
    return true;
}

//--DELETE-------------------------------------------------------------------------------------------------

bool CedaOrgData::Delete(long lpUrno)
{
	ORGDATA *prlOrg = GetOrgByUrno(lpUrno);
	if (prlOrg != NULL)
	{
		prlOrg->IsChanged = DATA_DELETED;
		if(Save(prlOrg) == false) return false; //Update Database
		DeleteInternal(prlOrg);
	}
    return true;
}

//--DELETE-INTERNAL----------------------------------------------------------------------------------------

bool CedaOrgData::DeleteInternal(ORGDATA *prpOrg)
{
	ogDdx.DataChanged((void *)this,ORG_DELETE,(void *)prpOrg); //Update Viewer
	omUrnoMap.RemoveKey((void *)prpOrg->Urno);
	int ilOrgCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilOrgCount; ilLc++)
	{
		if (omData[ilLc].Urno == prpOrg->Urno)
		{
			omData.DeleteAt(ilLc);//Update omData
			break;
		}
	}
    return true;
}

//--UPDATE-------------------------------------------------------------------------------------------------

bool CedaOrgData::Update(ORGDATA *prpOrg)
{
	if (GetOrgByUrno(prpOrg->Urno) != NULL)
	{
		if (prpOrg->IsChanged == DATA_UNCHANGED)
		{
			prpOrg->IsChanged = DATA_CHANGED;
		}
		if(Save(prpOrg) == false) return false; //Update Database
		UpdateInternal(prpOrg);
	}
    return true;
}

//--UPDATE-INTERNAL--------------------------------------------------------------------------------------------

bool CedaOrgData::UpdateInternal(ORGDATA *prpOrg)
{
	ORGDATA *prlOrg = GetOrgByUrno(prpOrg->Urno);
	if (prlOrg != NULL)
	{
		*prlOrg = *prpOrg; //Update omData
		ogDdx.DataChanged((void *)this,ORG_CHANGE,(void *)prlOrg); //Update Viewer
	}
    return true;
}

//--GET-BY-URNO--------------------------------------------------------------------------------------------

ORGDATA *CedaOrgData::GetOrgByUrno(long lpUrno)
{
	ORGDATA  *prlOrg;
	if (omUrnoMap.Lookup((void *)lpUrno,(void *& )prlOrg) == TRUE)
	{
		return prlOrg;
	}
	return NULL;
}

//--READSPECIALDATA-------------------------------------------------------------------------------------

bool CedaOrgData::ReadSpecialData(CCSPtrArray<ORGDATA> *popOrg,char *pspWhere,char *pspFieldList,bool ipSYS/*=true*/)
{
	bool ilRc = true;
	char pclFieldList[256] = " ";

	if(strlen(pspFieldList) > 0) 
	{
		strcpy(pclFieldList, pspFieldList);
	}
	if(ipSYS == true) 
	{
		if (CedaAction("SYS","ORG",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	else
	{
		if (CedaAction("RT","ORG",pclFieldList,pspWhere,"",pcgDataBuf) == false) return false;
	}
	if(popOrg != NULL)
	{
		for (int ilLc = 0; ilRc == true; ilLc++)
		{
			ORGDATA *prpOrg = new ORGDATA;
			if ((ilRc = GetBufferRecord(ilLc,prpOrg,CString(pclFieldList))) == true)
			{
				popOrg->Add(prpOrg);
			}
			else
			{
				delete prpOrg;
			}
		}
		if(popOrg->GetSize() == 0) return false;
	}
    return true;
}

//--SAVE---------------------------------------------------------------------------------------------------

bool CedaOrgData::Save(ORGDATA *prpOrg)
{
	bool ilRc = true;
	CString olListOfData;
	char pclSelection[124];
	char pclData[524];

	if (prpOrg->IsChanged == DATA_UNCHANGED)
	{
		return true; // no change, nothing to do
	}
	switch(prpOrg->IsChanged)
	{
	case DATA_NEW:
		MakeCedaData(&omRecInfo,olListOfData,prpOrg);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("IRT","","",pclData);
		prpOrg->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_CHANGED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpOrg->Urno);
		MakeCedaData(&omRecInfo,olListOfData,prpOrg);
		strcpy(pclData,olListOfData);
		ilRc = CedaAction("URT",pclSelection,"",pclData);
		prpOrg->IsChanged = DATA_UNCHANGED;
		break;
	case DATA_DELETED:
		sprintf(pclSelection, "WHERE URNO = %ld", prpOrg->Urno);
		ilRc = CedaAction("DRT",pclSelection);
		break;
	}
    return ilRc;
}

//--PROCESS-CF---------------------------------------------------------------------------------------------

void  ProcessOrgCf(void *vpInstance,int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	ogOrgData.ProcessBc(ipDDXType,vpDataPointer,ropInstanceName);
}

//--PROCESS-BC---------------------------------------------------------------------------------------------

void  CedaOrgData::ProcessBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName)
{
	struct BcStruct *prlOrgData;
	prlOrgData = (struct BcStruct *) vpDataPointer;
	ORGDATA *prlOrg;
	if(ipDDXType == BC_ORG_NEW)
	{
		prlOrg = new ORGDATA;
		GetRecordFromItemList(prlOrg,prlOrgData->Fields,prlOrgData->Data);
		if(ValidateOrgBcData(prlOrg->Urno)) //Prf: 8795
		{
			InsertInternal(prlOrg);
		}
		else
		{
			delete prlOrg;
		}
	}
	if(ipDDXType == BC_ORG_CHANGE)
	{
		long llUrno = GetUrnoFromSelection(prlOrgData->Selection);
		prlOrg = GetOrgByUrno(llUrno);
		if(prlOrg != NULL)
		{
			GetRecordFromItemList(prlOrg,prlOrgData->Fields,prlOrgData->Data);
			if(ValidateOrgBcData(prlOrg->Urno)) //Prf: 8795
			{
				UpdateInternal(prlOrg);
			}			
			else
			{
				DeleteInternal(prlOrg);
			}
		}
	}
	if(ipDDXType == BC_ORG_DELETE)
	{
		long llUrno = GetUrnoFromSelection(prlOrgData->Selection);

		prlOrg = GetOrgByUrno(llUrno);
		if (prlOrg != NULL)
		{
			DeleteInternal(prlOrg);
		}
	}
}

//Prf: 8795
//--ValidateOrgBcData--------------------------------------------------------------------------------------

bool CedaOrgData::ValidateOrgBcData(const long& lrpUrno)
{
	bool blValidateOrgBcData = true;
	if(!omWhere.IsEmpty()) //It means it is not the default view and we need to check in the database, since we don't need to check for the default view
	{
		char chWhere[20];
		sprintf(chWhere, "URNO='%ld'",lrpUrno);
		CString olWhere = omWhere + CString(" AND ") + CString(chWhere);
		CCSPtrArray<ORGDATA> olOrgs;
		if(!ReadSpecialData(&olOrgs,olWhere.GetBuffer(0),"URNO",false))
		{		
			blValidateOrgBcData = false;
		}
	}
	return blValidateOrgBcData;
}

//---------------------------------------------------------------------------------------------------------

int CedaOrgData::ReadODG()
{
		char querystr[1024];

		ogBCD.SetObject("ODG","URNO,HOPO,ODGC,ODGN,ORGU,REMA");
		
		im_UrnoPos=ogBCD.GetFieldIndex("ODG","URNO");
		im_HopoPos=ogBCD.GetFieldIndex("ODG","HOPO");
		im_OdgcPos=ogBCD.GetFieldIndex("ODG","ODGC");
		im_OdgnPos=ogBCD.GetFieldIndex("ODG","ODGN");
		im_OrguPos=ogBCD.GetFieldIndex("ODG","ORGU");
		im_RemaPos=ogBCD.GetFieldIndex("ODG","REMA");

		sprintf(querystr,"");

		ogBCD.Read("ODG",querystr);

		return ogBCD.GetDataCount("ODG");
}

bool CedaOrgData::SaveODGData()
{
	int       cnt	= ogBCD.GetFieldCount("ODG");	// to init recordset	
	bool	blerr	= false;						// return value
	RecordSet olTRecord(cnt);						// init recordset

/*	m_ctrl_Code.GetWindowText(m_CODE);
	m_ctrl_Vpfr.GetWindowText(m_VPFR);
	m_ctrl_Vpto.GetWindowText(m_VPTO);	// KADC
	m_ctrl_Odgc.GetWindowText(m_ODGC);	// CPFC
	m_ctrl_Lead.GetWindowText(m_LEAD);
*/
	olTRecord.Values[im_HopoPos] = "FRA";//m_HOPO;	
	olTRecord.Values[im_OdgcPos] = "P1";//m_ODGC;
	olTRecord.Values[im_OdgnPos] = "TEST";//m_ODGN;
	olTRecord.Values[im_OrguPos] = "";//m_ORGU;
	olTRecord.Values[im_RemaPos] = "TEST TEST";//m_REMA;	

//	if (!pomODGUrno.IsEmpty())
//	{	//Update
//		blerr = ogBCD.SetRecord("ODG","URNO",pomODGUrno,olTRecord.Values,true);
//	}
//	else
//	{	//NEW
		blerr = ogBCD.InsertRecord("ODG", olTRecord, true);
//	}

	return blerr;
}

void CedaOrgData::GetAllCodes(CStringArray &olList)
{
	olList.RemoveAll();
	int ilOrgCount = omData.GetSize();
	for (int ilLc = 0; ilLc < ilOrgCount; ilLc++)
	{
		olList.Add(omData[ilLc].Dpt1);
		
	}
}