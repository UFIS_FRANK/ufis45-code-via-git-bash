#if !defined(AFX_GATPOSCHECKINCOUNTERDLG_H__7B7C3CE7_BF13_11D6_8216_00010215BFDE__INCLUDED_)
#define AFX_GATPOSCHECKINCOUNTERDLG_H__7B7C3CE7_BF13_11D6_8216_00010215BFDE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GatPosCheckinCounterDlg.h : header file
//
#include <CheckinCounterDlg.h>
#include <AatBitmapComboBox.h>

/////////////////////////////////////////////////////////////////////////////
// GatPosCheckinCounterDlg dialog

class GatPosCheckinCounterDlg : public CheckinCounterDlg
{
// Construction
public:
	GatPosCheckinCounterDlg(CICDATA *popCIC,CWnd* pParent = NULL);   // standard constructor
	GatPosCheckinCounterDlg(CICDATA *popCIC,int ipDlg,CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(GatPosCheckinCounterDlg)
	enum { IDD = IDD_GATPOS_CHECKINCOUNTERDLG };
	AatBitmapComboBox	m_BlockingBitmap;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(GatPosCheckinCounterDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(GatPosCheckinCounterDlg)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	afx_msg LONG OnTableLButtonDblclk(UINT wParam, LONG lParam);
	afx_msg void OnNoavNew();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GATPOSCHECKINCOUNTERDLG_H__7B7C3CE7_BF13_11D6_8216_00010215BFDE__INCLUDED_)
