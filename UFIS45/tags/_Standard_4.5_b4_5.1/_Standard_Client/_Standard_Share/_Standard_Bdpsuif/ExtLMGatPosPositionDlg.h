#if !defined(AFX_EXTLMGATPOSPOSITIONDLG_H__51432297_B4E3_4E1F_BFA9_E96A276AF86A__INCLUDED_)
#define AFX_EXTLMGATPOSPOSITIONDLG_H__51432297_B4E3_4E1F_BFA9_E96A276AF86A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ExtLMGatPosPositionDlg.h : header file
//
#include <GatPosPositionDlg.h>
#include <ExtLMDatGrid.h>

/////////////////////////////////////////////////////////////////////////////
// ExtLMGatPosPositionDlg dialog

class ExtLMGatPosPositionDlg : public GatPosPositionDlg
{
// Construction
public:
	ExtLMGatPosPositionDlg(PSTDATA *popPST,long lpOriginalUrno,CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(ExtLMGatPosPositionDlg)
	enum { IDD = IDD_EXTLM_GATPOS_POSITIONDLG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ExtLMGatPosPositionDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(ExtLMGatPosPositionDlg)
		virtual void OnOK();
		virtual BOOL OnInitDialog();
		afx_msg void OnDatNew();
		afx_msg void OnDatDelete();
		afx_msg void OnDatCopy();
		afx_msg void OnDatAssign();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	ExtLMDatGrid	omGrid;
	long			lmOriginalUrno;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EXTLMGATPOSPOSITIONDLG_H__51432297_B4E3_4E1F_BFA9_E96A276AF86A__INCLUDED_)
