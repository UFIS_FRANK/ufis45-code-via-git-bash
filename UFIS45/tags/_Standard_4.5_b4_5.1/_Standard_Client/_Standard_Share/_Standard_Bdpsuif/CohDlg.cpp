// CohDlg.cpp : implementation file
//

#include <stdafx.h>
#include <bdpsuif.h>
#include <CohDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCohDlg dialog


CCohDlg::CCohDlg(COHDATA *popCoh, CWnd* pParent /*=NULL*/) : CDialog(CCohDlg::IDD, pParent)
{
	pomCoh = popCoh;
	//{{AFX_DATA_INIT(CCohDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CCohDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCohDlg)
	DDX_Control(pDX, IDOK, m_OK);
	DDX_Control(pDX, IDC_CDAT_D, m_CDATD);
	DDX_Control(pDX, IDC_CDAT_T, m_CDATT);
	DDX_Control(pDX, IDC_CTRC, m_CTRC);
	DDX_Control(pDX, IDC_FAGE, m_FAGE);
	DDX_Control(pDX, IDC_HOLD, m_HOLD);
	DDX_Control(pDX, IDC_LSTU_D, m_LSTUD);
	DDX_Control(pDX, IDC_LSTU_T, m_LSTUT);
	DDX_Control(pDX, IDC_USEC, m_USEC);
	DDX_Control(pDX, IDC_USEU, m_USEU);
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCohDlg, CDialog)
	//{{AFX_MSG_MAP(CCohDlg)
	ON_BN_CLICKED(IDC_B_COT_AW, OnBCotAw)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCohDlg message handlers

void CCohDlg::OnBCotAw() 
{
	CStringArray olCol1, olCol2, olCol3;
	CCSPtrArray<COTDATA> olList;
	if(ogCotData.ReadSpecialData(&olList, "", "URNO,CTRC,CTRN", false) == true)
	{
		for(int i = 0; i < olList.GetSize(); i++)
		{
			char pclUrno[20]="";
			sprintf(pclUrno, "%ld", olList[i].Urno);
			olCol3.Add(CString(pclUrno));
			olCol1.Add(CString(olList[i].Ctrc));
			olCol2.Add(CString(olList[i].Ctrn));
		}
		AwDlg *pomDlg = new AwDlg(&olCol1, &olCol2, &olCol3, LoadStg(IDS_STRING237), LoadStg(IDS_STRING229), this);
		if(pomDlg->DoModal() == IDOK)
		{
			m_CTRC.SetWindowText(pomDlg->omReturnString);
			m_CTRC.SetFocus();
		}
		delete pomDlg;
	}
	else
	{
		MessageBox(LoadStg(IDS_STRING321), LoadStg(IDS_STRING149), (MB_ICONSTOP|MB_OK));
	}
	olList.DeleteAll();
	
}

void CCohDlg::OnOK() 
{
	AfxGetApp()->DoWaitCursor(1);
	CString olErrorText;
	bool ilStatus = true;
	CString olTmp;
	if(m_CTRC.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING299) + ogNotFormat;
	}
	if(m_FAGE.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING777) + ogNotFormat;//+ CString(" 2") 
	}
	if(m_HOLD.GetStatus() == false)
	{
		ilStatus = false;
		olErrorText += LoadStg(IDS_STRING766) + ogNotFormat;//+ CString(" 2") 
	}
	///////////////////////////////////////////////////////////////////////////

	//Konsistenz-Check//
	if(ilStatus == true)
	{
		CString olCtrc;
		char clWhere[100];

		m_CTRC.GetWindowText(olCtrc);
		if(olCtrc.GetLength() >= 1)
		{
			sprintf(clWhere,"WHERE CTRC='%s'",olCtrc);
			if(ogCotData.ReadSpecialData(NULL,clWhere,"CTRC",false) == false)
			{
				ilStatus = false;
				olErrorText += LoadStg(IDS_STRING349);
			}
		}
		else
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING349);
		}

	}
	if(ilStatus == true)
	{
		CString olCtrc;
		CString olFage;
		CCSPtrArray<COHDATA> olCohCPA;
		COHDATA olCoh;
		char clWhere[100];

		m_CTRC.GetWindowText(olCtrc);
		m_FAGE.GetWindowText(olFage);
		if (olFage.GetLength() >= 1)
		{
			sprintf(clWhere,"WHERE CTRC='%s' AND FAGE='%s'",olCtrc,olFage);
			if(ogCohData.ReadSpecialData(&olCohCPA,clWhere,"URNO",false) == true)
			{
				olCoh = olCohCPA[0];
				if(olCohCPA[0].Urno != pomCoh->Urno)
				{
					ilStatus = false;
					olErrorText += LoadStg(IDS_STRING822);
				}
			}
		}
		else
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING766);
		}

	}
	if(ilStatus == true)
	{
		CString olHold;
		m_HOLD.GetWindowText(olHold);
		if (olHold.GetLength() == 0)
		{
			ilStatus = false;
			olErrorText += LoadStg(IDS_STRING766);
		}

	}

	///////////////////////////////////////////////////////////////////////////
	AfxGetApp()->DoWaitCursor(-1);
	if(ilStatus == true)
	{
		CString olText;
		m_CTRC.GetWindowText(pomCoh->Ctrc,6);
		m_FAGE.GetWindowText(pomCoh->Fage,3);
		m_HOLD.GetWindowText(pomCoh->Hold,3);

		CDialog::OnOK();
	}
	else
	{
		Beep(440,70);
		MessageBox(olErrorText,LoadStg(IDS_STRING145),MB_ICONERROR);
		m_CTRC.SetFocus();
		m_CTRC.SetSel(0,-1);
	}
}

BOOL CCohDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	m_Caption = LoadStg(IDS_STRING133) + " <" + m_Caption + ">";
	SetWindowText(m_Caption);
	char clStat = ogPrivList.GetStat("COHDLG.m_OK");
	SetWndStatAll(clStat,m_OK);

	//------------------------------------
	m_CDATD.SetBKColor(SILVER);
	m_CDATD.SetInitText(pomCoh->Cdat.Format("%d.%m.%Y"));
	// - - - - - - - - - - - - - - - - - -
	m_CDATT.SetBKColor(SILVER);
	m_CDATT.SetInitText(pomCoh->Cdat.Format("%H:%M"));
	//------------------------------------
	m_LSTUD.SetBKColor(SILVER);
	m_LSTUD.SetInitText(pomCoh->Lstu.Format("%d.%m.%Y"));
	// - - - - - - - - - - - - - - - - - -
	m_LSTUT.SetBKColor(SILVER);
	m_LSTUT.SetInitText(pomCoh->Lstu.Format("%H:%M"));
	//------------------------------------
	m_USEC.SetBKColor(SILVER);
	m_USEC.SetInitText(pomCoh->Usec);
	//------------------------------------
	m_USEU.SetBKColor(SILVER);
	m_USEU.SetInitText(pomCoh->Useu);
	//------------------------------------//WHITE,YELLOW,SILVER,RED,LYELLOW
	m_CTRC.SetFormat("x|#x|#x|#x|#x|#");
	m_CTRC.SetTextLimit(1,5);
	m_CTRC.SetBKColor(YELLOW);  
	m_CTRC.SetTextErrColor(RED);
	m_CTRC.SetInitText(pomCoh->Ctrc);
	//------------------------------------
	m_FAGE.SetFormat("##");
	m_FAGE.SetTextLimit(1,2);
	m_FAGE.SetBKColor(YELLOW);  
	m_FAGE.SetTextErrColor(RED);
	m_FAGE.SetInitText(pomCoh->Fage);
	//------------------------------------
	m_HOLD.SetFormat("##");
	m_HOLD.SetTextLimit(1,2);
	m_HOLD.SetBKColor(YELLOW);  
	m_HOLD.SetTextErrColor(RED);
	m_HOLD.SetInitText(pomCoh->Hold);
	//------------------------------------
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

// Umwandlung von 03:00 / 3:00 / 0300 aus der Gui 
// immer ins Format 180 min der DB
CString CCohDlg::Gui2DBmin(CString guimin)
{
	CString slOut	= "";
	int ilPos		= 0;
	int ilHour		= 0;
	int ilMin		= 0;
	
	ilPos = guimin.Find(":");
	if (ilPos > 0)
	{	// Format hh:mm oder h:mm
		slOut  = guimin.Left(ilPos);	
		ilHour = atoi(slOut);
		ilHour = ilHour * 60;
		slOut  = guimin.Right(guimin.GetLength()-(ilPos+1));
		ilMin  = atoi(slOut);
		ilMin  = ilHour + ilMin;
	}
	else
	{	// Format hhmm
		slOut  = guimin.Left(2);	
		ilHour = atoi(slOut);
		ilHour = ilHour * 60;
		slOut  = guimin.Right(2);	
		ilMin  = ilHour + ilMin;	
	}
	slOut.Format("%d",ilMin);

	return slOut;
}
