// SrcTableViewer.cpp 
//

#include <stdafx.h>
#include <SrcTableViewer.h>
#include <CcsDdx.h>
#include <resource.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif


// Local function prototype
static void SrcTableCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

//-----------------------------------------------------------------------------------------------
// SrcTableViewer
//

SrcTableViewer::SrcTableViewer(CCSPtrArray<SRCDATA> *popData)
{
	pomData = popData;

	bmIsFromSearch = FALSE;
    pomSrcTable = NULL;
    ogDdx.Register(this, SRC_CHANGE, CString("SrcTableViewer"), CString("Src Update"), SrcTableCf);
    ogDdx.Register(this, SRC_NEW,    CString("SrcTableViewer"), CString("Src New"),    SrcTableCf);
    ogDdx.Register(this, SRC_DELETE, CString("SrcTableViewer"), CString("Src Delete"), SrcTableCf);
}

//-----------------------------------------------------------------------------------------------

SrcTableViewer::~SrcTableViewer()
{
	ogDdx.UnRegister(this, NOTUSED);
    DeleteAll();
	omHeaderDataArray.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void SrcTableViewer::Attach(CCSTable *popTable)
{
    pomSrcTable = popTable;
}

//-----------------------------------------------------------------------------------------------

void SrcTableViewer::ChangeViewTo(const char *pcpViewName)
{
    DeleteAll();    
    MakeLines();
	UpdateDisplay();
}

//-----------------------------------------------------------------------------------------------

void SrcTableViewer::MakeLines()
{
	int ilSrcCount = pomData->GetSize();
	for (int ilLc = 0; ilLc < ilSrcCount; ilLc++)
	{
		SRCDATA *prlSrcData = &pomData->GetAt(ilLc);
		MakeLine(prlSrcData);
	}
}

//-----------------------------------------------------------------------------------------------

void SrcTableViewer::MakeLine(SRCDATA *prpSrc)
{

    //if( !IsPassFilter(prpSrc)) return;

    // Update viewer data for this shift record
    SRCTABLE_LINEDATA rlSrc;

	rlSrc.Urno = prpSrc->Urno;
	rlSrc.Srcc = prpSrc->Srcc;
	rlSrc.Srcd = prpSrc->Srcd;

	CreateLine(&rlSrc);
}

//-----------------------------------------------------------------------------------------------

void SrcTableViewer::CreateLine(SRCTABLE_LINEDATA *prpSrc)
{
    int ilLineCount = omLines.GetSize();
    for (int ilLineno = ilLineCount; ilLineno > 0; ilLineno--)
        if (CompareSrc(prpSrc, &omLines[ilLineno-1]) >= 0)
	        break;							 // should be inserted after Lines[ilLineno-1]
	SRCTABLE_LINEDATA rlSrc;
	rlSrc = *prpSrc;
    omLines.NewAt(ilLineno, rlSrc);
}

//-----------------------------------------------------------------------------------------------
// UpdateDisplay: Load data selected by filter conditions to the display by using "omTable"

void SrcTableViewer::UpdateDisplay()
{
	int ilLines = omLines.GetSize();
	const int ilColumns = 2;	//<===== Spaltenanzahl setzen (von 0 ab)
	omHeaderDataArray.DeleteAll();

	pomSrcTable->SetShowSelection(TRUE);
	pomSrcTable->ResetContent();

	TABLE_HEADER_COLUMN rlHeader;
	rlHeader.Alignment = COLALIGN_LEFT;
	rlHeader.Font = &ogCourier_Regular_10;

	int ilPos = 1;
	rlHeader.Length = 42; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING641),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
	rlHeader.Length = 332; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING641),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
/*	rlHeader.Length = 498; 
	rlHeader.Text = GetListItem(LoadStg(IDS_STRING641),ilPos++,true,'|');
	omHeaderDataArray.NewAt(omHeaderDataArray.GetSize(), rlHeader);
*/	pomSrcTable->SetHeaderFields(omHeaderDataArray);

	pomSrcTable->SetDefaultSeparator();
	pomSrcTable->SetTableEditable(FALSE);

	TABLE_COLUMN rlColumnData;
	rlColumnData.Font = &ogCourier_Regular_10;
	rlColumnData.BkColor = WHITE;
	rlColumnData.Alignment = COLALIGN_LEFT;
	
    for (int ilLineNo = 0; ilLineNo < ilLines; ilLineNo++)
	{
		CCSPtrArray<TABLE_COLUMN> olColList;

		rlColumnData.Text = omLines[ilLineNo].Srcc;
		olColList.NewAt(olColList.GetSize(), rlColumnData);
		rlColumnData.Text = omLines[ilLineNo].Srcd;
		olColList.NewAt(olColList.GetSize(), rlColumnData);

		pomSrcTable->AddTextLine(olColList,(void*)NULL);
		olColList.DeleteAll();
	}
	pomSrcTable->DisplayTable();
}

//-----------------------------------------------------------------------------------------------

CString SrcTableViewer::Format(SRCTABLE_LINEDATA *prpLine)
{
    CString s;
	// ???
    return s;
}

//-----------------------------------------------------------------------------------------------

bool SrcTableViewer::FindSrc(char *pcpSrcKeya, char *pcpSrcKeyd, int& ilItem)
{
	int ilCount = omLines.GetSize();
    for (ilItem = 0; ilItem < ilCount; ilItem++)
    {
/*		if ( (omLines[ilItem].Keya == pcpSrcKeya) &&
			 (omLines[ilItem].Keyd == pcpSrcKeyd)    )
			return TRUE;*/
	}
	return false;
}

//-----------------------------------------------------------------------------------------------

static void SrcTableCf(void *popInstance, int ipDDXType,
    void *vpDataPointer, CString &ropInstanceName)
{
    SrcTableViewer *polViewer = (SrcTableViewer *)popInstance;
    if (ipDDXType == SRC_CHANGE || ipDDXType == SRC_NEW) polViewer->ProcessSrcChange((SRCDATA *)vpDataPointer);
    if (ipDDXType == SRC_DELETE) polViewer->ProcessSrcDelete((SRCDATA *)vpDataPointer);
} 


//-----------------------------------------------------------------------------------------------

void SrcTableViewer::ProcessSrcChange(SRCDATA *prpSrc)
{
	int ilItem = 0;
	int ilColumnNo = 0;
	TABLE_COLUMN rlColumn;
	CCSPtrArray <TABLE_COLUMN> olLine;

	rlColumn.Lineno = ilItem;
	rlColumn.VerticalSeparator = SEPA_NONE;
	rlColumn.SeparatorType = SEPA_NONE;
	rlColumn.BkColor = WHITE;
	rlColumn.Font = &ogCourier_Regular_10;
	rlColumn.Alignment = COLALIGN_LEFT;

	rlColumn.Text = prpSrc->Srcc;
	olLine.NewAt(olLine.GetSize(), rlColumn);
	rlColumn.Text = prpSrc->Srcd;
	olLine.NewAt(olLine.GetSize(), rlColumn);

	
	if (FindLine(prpSrc->Urno, ilItem))
	{
        SRCTABLE_LINEDATA *prlLine = &omLines[ilItem];

		prlLine->Urno = prpSrc->Urno;
		prlLine->Srcc = prpSrc->Srcc;
		prlLine->Srcd = prpSrc->Srcd;


		pomSrcTable->ChangeTextLine(ilItem, &olLine, (void *)prlLine);
		pomSrcTable->DisplayTable();
	}
	else
	{
		MakeLine(prpSrc);
		if (FindLine(prpSrc->Urno, ilItem))
		{
	        SRCTABLE_LINEDATA *prlLine = &omLines[ilItem];
			if(prlLine != NULL)
			{
				pomSrcTable->AddTextLine(olLine, (void *)prlLine);
				pomSrcTable->DisplayTable();
			}
		}
	}
	olLine.DeleteAll();
}

//-----------------------------------------------------------------------------------------------

void SrcTableViewer::ProcessSrcDelete(SRCDATA *prpSrc)
{
	int ilItem;
	if (FindLine(prpSrc->Urno, ilItem))
	{
		DeleteLine(ilItem);
		pomSrcTable->DeleteTextLine(ilItem);
		pomSrcTable->DisplayTable();
	}
}

//-----------------------------------------------------------------------------------------------

bool SrcTableViewer::IsPassFilter(SRCDATA *prpSrc)
{

	return true;
}

//-----------------------------------------------------------------------------------------------

int SrcTableViewer::CompareSrc(SRCTABLE_LINEDATA *prpSrc1, SRCTABLE_LINEDATA *prpSrc2)
{
/*	int ilCase=0;
	int	ilCompareResult;
	if(prpSrc1->Tifd == prpSrc2->Tifd)
	{
		ilCompareResult= 0;
	}
	else
	{
		if(prpSrc1->Tifd > prpSrc2->Tifd)
		{
			ilCompareResult = 1;
		}
		else
		{
			ilCompareResult = -1;
		}
	}
	return ilCompareResult;
*/  
	return 0;
}

//-----------------------------------------------------------------------------------------------

void SrcTableViewer::DeleteLine(int ipLineno)
{
	omLines.DeleteAt(ipLineno);
}

//-----------------------------------------------------------------------------------------------

bool SrcTableViewer::FindLine(long lpUrno, int &rilLineno)
{
    for (rilLineno = 0; rilLineno < omLines.GetSize(); rilLineno++)
	{
        if (omLines[rilLineno].Urno == lpUrno)
            return true;
	}
    return false;
}
//-----------------------------------------------------------------------------------------------

void SrcTableViewer::DeleteAll()
{
    while (omLines.GetSize() > 0)
    DeleteLine(0);
}

//-----------------------------------------------------------------------------------------------
// Printing Routines
//-----------------------------------------------------------------------------------------------

void SrcTableViewer::PrintTableView()
{
	CString olFooter1,olFooter2;
	omTableName = LoadStg(IDS_STRING825);
	int ilOrientation = PRINT_PORTRAET;//  PRINT_PORTRAET  PRINT_LANDSCAPE
	pomPrint = new CCSPrint((CWnd*)this,ilOrientation,45);// FirstLine(P=200,L=200),LeftOffset(P=200,L=50)
	//pomPrint->imMaxLines = 38;  // (P=57,L=38)
	//omBitmap.LoadBitmap(IDB_HAJLOGO);
	//pomPrint->SetBitmaps(&omBitmap,&omBitmap);
	if (pomPrint != NULL)
	{
		if (pomPrint->InitializePrinter(ilOrientation) == TRUE)
		{
			SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
			pomPrint->imLineNo = pomPrint->imMaxLines + 1;
			DOCINFO	rlDocInfo;
			memset(&rlDocInfo, 0, sizeof(DOCINFO));
			rlDocInfo.cbSize = sizeof( DOCINFO );
			rlDocInfo.lpszDocName = omTableName;	
			pomPrint->omCdc.StartDoc( &rlDocInfo );
			pomPrint->imPageNo = 0;
			int ilLines = omLines.GetSize();
			olFooter1.Format("%d %s",ilLines,omTableName);
			for(int i = 0; i < ilLines; i++ ) 
			{
				if(pomPrint->imLineNo >= pomPrint->imMaxLines)
				{
					if(pomPrint->imPageNo > 0)
					{
						olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
						pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
						pomPrint->omCdc.EndPage();
					}
					PrintTableHeader();
				}
				if(pomPrint->imLineNo == (pomPrint->imMaxLines-1) || i == (ilLines-1))
				{
					PrintTableLine(&omLines[i],true);
				}
				else
				{
					PrintTableLine(&omLines[i],false);
				}
			}
			olFooter2.Format("%s: %d",LoadStg(IDS_STRING222),pomPrint->imPageNo);
			pomPrint->PrintUIFFooter(olFooter1,"",olFooter2);
			pomPrint->omCdc.EndPage();
			pomPrint->omCdc.EndDoc();
		}
	SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
	delete pomPrint;
	//omBitmap.DeleteObject();
	pomPrint = NULL;
	}
}

//-----------------------------------------------------------------------------------------------

bool SrcTableViewer::PrintTableHeader()
{
	pomPrint->omCdc.StartPage();
	pomPrint->imPageNo++;
	pomPrint->imLineNo = 0;

	pomPrint->PrintUIFHeader(omTableName,CString(CTime::GetCurrentTime().Format("%d.%m.%Y/%H:%M")),pomPrint->imFirstLine-10);

	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;

	rlElement.Alignment   = PRINT_LEFT;
	rlElement.FrameLeft   = PRINT_NOFRAME;
	rlElement.FrameRight  = PRINT_NOFRAME;
	rlElement.FrameTop    = PRINT_NOFRAME;
	rlElement.FrameBottom = PRINT_FRAMEMEDIUM;
	rlElement.pFont       = &pomPrint->ogCourierNew_Bold_8;

	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=2)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Text   = omHeaderDataArray[i].Text;
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();


	return true;
}

//-----------------------------------------------------------------------------------------------

bool SrcTableViewer::PrintTableLine(SRCTABLE_LINEDATA *prpLine,bool bpLastLine)
{
	CCSPtrArray <PRINTELEDATA> rlPrintLine;
	PRINTELEDATA rlElement;
	rlElement.pFont      = &pomPrint->ogCourierNew_Regular_8;
	int ilSize = omHeaderDataArray.GetSize();
	for(int i=0;i<ilSize;i++)
	{
		//if(i!=1&&i!=1)// <= setzen, wenn Spalten nicht angezeigt werden sollen 
		{
			rlElement.Length = (int)(omHeaderDataArray[i].Length*dgCCSPrintFactor);
			if(rlElement.Length < igCCSPrintMinLength)rlElement.Length+=igCCSPrintMoreLength; 
			rlElement.Alignment  = PRINT_LEFT;
			rlElement.FrameTop   = PRINT_FRAMETHIN;
			if(bpLastLine == true)
			{
				rlElement.FrameBottom = PRINT_FRAMETHIN;
			}
			else
			{
				rlElement.FrameBottom = PRINT_NOFRAME;
			}
			rlElement.FrameLeft  = PRINT_NOFRAME;
			rlElement.FrameRight = PRINT_NOFRAME;

			switch(i)
			{
			case 0:
				rlElement.FrameLeft  = PRINT_FRAMETHIN;
				rlElement.Text		 = prpLine->Srcc;
				break;
			case 1:
				rlElement.FrameRight = PRINT_FRAMETHIN;
				rlElement.Text		 = prpLine->Srcd;
				break;
			}
			rlPrintLine.NewAt(rlPrintLine.GetSize(),rlElement);
		}
	}
	pomPrint->PrintLine(rlPrintLine);
	rlPrintLine.DeleteAll();
	return true;
}

//-----------------------------------------------------------------------------------------------
