﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ufis.Data
{
    /// <summary>
    /// Enumeration for flight ADID.
    /// </summary>
    [FlagsAttribute]
    public enum FlightAdid
    {
        Unspecified = 0,
        Arrival = 1,
        Departure = 2
    }
}
