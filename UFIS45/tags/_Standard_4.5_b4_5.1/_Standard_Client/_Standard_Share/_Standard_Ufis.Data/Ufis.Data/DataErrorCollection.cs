﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Ufis.Data
{
    /// <summary>
    /// Represents a collection of Ufis.Data.DataError object.
    /// </summary>
    public class DataErrorCollection : IEnumerator, IEnumerable
    {
        private int _position = -1;
        private List<DataError> _errors;

        /// <summary>
        /// Gets the total number of Ufis.Data.DataError objects in this collection.
        /// </summary>
        public int Count
        {
            get { return _errors.Count; }
        }

        /// <summary>
        /// Adds Ufis.Data.Ceda.CedaError object to this collection.
        /// </summary>
        /// <param name="row">The Ufis.Data.DataError object.</param>
        public void Add(DataError error)
        {
            this._errors.Add(error);
        }

        /// <summary>
        /// </summary>
        /// <param name="index">The zero-based index of error in the collection.</param>
        /// <returns></returns>
        public DataError this[int index]
        {
            get { return this._errors[index]; }
        }

        /// <summary>
        /// Initializes the new instance of Ufis.Data.DataErrorCollection class.
        /// </summary>
        public DataErrorCollection()
        {
            this._errors = new List<DataError>();
        }

        //IEnumerator and IEnumerable require these methods.
        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)this;
        }

        //IEnumerator
        public bool MoveNext()
        {
            _position++;
            return (_position < _errors.Count);
        }

        //IEnumerable
        public void Reset()
        { _position = 0; }

        //IEnumerable
        public object Current
        {
            get { return _errors[_position]; }
        }
    }
}
