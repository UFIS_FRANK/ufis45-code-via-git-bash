﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Ufis.Entities;

namespace Ufis.Data
{
    /// <summary>
    /// Represents the collection of entity table mapping objects.
    /// </summary>
    public class EntityTableMappingCollection
    {
        private readonly IList<EntityTableMapping> _lstEntityTableMaps;
        private readonly IList<Type> _lstEntityTypes;
        private readonly IList<string> _lstTableNames;

        /// <summary>
        /// Creates entity table mapping based on property info and add it into the collection of this 
        /// instance of Ufis.Entities.EntityTableMappingCollection.
        /// </summary>
        /// <param name="entityType">An entity type.</param>
        public void Add(Type entityType)
        {
            if (!typeof(Entity).IsAssignableFrom(entityType))
            {
                throw new ArgumentException("EntityType");
            }
            
            if (!_lstEntityTypes.Contains(entityType))
            {
                EntityTableMapping entityTableMapping = new EntityTableMapping() { EntityType = entityType };

                object[] arrEntityAttributes = entityType.GetCustomAttributes(typeof(EntityAttribute), false);
                if (arrEntityAttributes.Length > 0)
                {
                    string strTableName = ((EntityAttribute)arrEntityAttributes[0]).SerializedName;                    
                    entityTableMapping.TableName = strTableName;
                }

                PropertyInfo[] arrInfos = entityType.GetProperties();
                foreach (PropertyInfo info in arrInfos)
                {
                    entityTableMapping.ColumnMappings.Add(info);
                }

                _lstEntityTableMaps.Add(entityTableMapping);
                _lstEntityTypes.Add(entityType);
                _lstTableNames.Add(entityTableMapping.TableName);
            }
        }

        /// <summary>
        /// Gets the number of items in this instance of Ufis.Entities.EntityTableMappingCollection.
        /// </summary>
        public int Count
        {
            get
            {
                return _lstEntityTableMaps.Count;
            }
        }

        /// <summary>
        /// Gets the entity table mapping object in this instance of Ufis.Entities.EntityTableMappingCollection.
        /// </summary>
        /// <param name="entityType">An entity type.</param>
        /// <returns></returns>
        public EntityTableMapping this[Type entityType]
        {
            get
            {
                int intIndex = _lstEntityTypes.IndexOf(entityType);
                if (intIndex >= 0)
                    return _lstEntityTableMaps[intIndex];
                else
                    return null;
            }
        }

        /// <summary>
        /// Gets the entity table mapping object in this instance of Ufis.Entities.EntityTableMappingCollection.
        /// </summary>
        /// <param name="tableName">A table name.</param>
        /// <returns></returns>
        public EntityTableMapping this[string tableName]
        {
            get
            {
                int intIndex = _lstTableNames.IndexOf(tableName);
                if (intIndex >= 0)
                    return _lstEntityTableMaps[intIndex];
                else
                    return null;
            }
        }

        /// <summary>
        /// Gets the entity table mapping object in this instance of Ufis.Entities.EntityTableMappingCollection.
        /// </summary>
        /// <param name="index">An index of table mapping object in the collection</param>
        /// <returns></returns>
        public EntityTableMapping this[int index]
        {
            get
            {
                if (index >= 0 && index < Count)
                    return _lstEntityTableMaps[index];
                else
                    return null;
            }
        }

        /// <summary>
        /// Initializes the new instance of Ufis.Entities.EntityTableMappingCollection object.
        /// </summary>
        public EntityTableMappingCollection()
        {
            _lstEntityTableMaps = new List<EntityTableMapping>();
            _lstEntityTypes = new List<Type>();
            _lstTableNames = new List<string>();
        }
    }
}
