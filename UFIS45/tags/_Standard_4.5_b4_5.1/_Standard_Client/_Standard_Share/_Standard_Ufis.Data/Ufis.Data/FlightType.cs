﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ufis.Data
{
    /// <summary>
    /// Enumeration for flight operational type.
    /// </summary>
    [FlagsAttribute]
    public enum FlightType
    {
        Unspecified = 0,
        Scheduled = 1,
        Operation = 2,
        Cancelled = 4,
        NoOperation = 8,
        Diverted = 16,
        ReturnTaxi = 32,
        ReturnFlight = 64,
        Towing = 128,
        GroundMovement = 256
    }
}
