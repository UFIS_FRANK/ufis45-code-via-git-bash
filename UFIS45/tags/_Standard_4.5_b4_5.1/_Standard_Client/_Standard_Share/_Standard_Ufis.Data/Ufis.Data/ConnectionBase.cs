﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ufis.Data
{
    /// <summary>
    /// Represents an abstract class to open connection to CEDA.
    /// </summary>
    public abstract class ConnectionBase
    {
        private ConnectionState _connectionState;
        private string _strConnectedServer;

        /// <summary>
        /// Gets or sets the name of application which requests the connection.
        /// </summary>
        public string ApplicationName { get; set; }
        /// <summary>
        /// Gets or sets the version of application which requests the connection.
        /// </summary>
        public string ApplicationVersion { get; set; }
        /// <summary>
        /// Gets or sets the current home airport.
        /// </summary>
        public string HomeAirport { get; set; }
        /// <summary>
        /// Gets or sets the table extention.
        /// </summary>
        public string TableExtension { get; set; }
        /// <summary>
        /// Gets or sets the workstation name where the client application is running.
        /// </summary>
        public string WorkstationName { get; set; }
        /// <summary>
        /// Gets or sets the logged in user of the application.
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// Gets or sets the list of servers' name (delimited by comma) to connect to.
        /// </summary>
        public string ServerList { get; set; }
        /// <summary>
        /// Gets the current state of the connection.
        /// </summary>
        public ConnectionState State { get { return _connectionState; } }
        /// <summary>
        /// Gets the current connected server.
        /// </summary>
        public string ConnectedServer { get { return _strConnectedServer; } }
        /// <summary>
        /// Property Get/Set of the Domain Name
        /// </summary>
        public string DomainName { get; set; }
        /// <summary>
        /// Property Get/Set of the AD Flag
        /// </summary>
        public string ADFlag { get; set; }
        /// <summary>
        /// Initializes the new instance of Ufis.Data.ConnectionBase class.
        /// </summary>
        public ConnectionBase()
        {
            ServerList = string.Empty;
            _connectionState = ConnectionState.Closed;
            TableExtension = "TAB";
            UserName = string.Empty;
            WorkstationName = string.Empty;
        }

        /// <summary>
        /// Opens an connection to Ufis database.
        /// </summary>
        public void Open()
        {
            DataErrorCollection errors = new DataErrorCollection();

            _connectionState = ConnectionState.Connecting;

            _strConnectedServer = null;

            string[] arrServers = ServerList.Split(',');
            foreach (string strServer in arrServers)
            {
                bool bHasErrors = false;

                try
                {
                    bHasErrors = !OpenConnection(strServer);
                }
                catch (Exception ex)
                {
                    errors.Add(new DataError("Unable to connect to server " + strServer, ex));

                    bHasErrors = true;
                }

                if (!bHasErrors)
                {
                    _strConnectedServer = strServer;
                    break;
                }
            }

            if (_strConnectedServer == null)
            {
                Close();
                _connectionState = ConnectionState.Closed;

                throw new DataException(errors);
            }
            else
            {
                _connectionState = ConnectionState.Open;
            }
        }

        /// <summary>
        /// Closes the connection to Ufis database.
        /// </summary>
        public void Close()
        {
            if (_connectionState == ConnectionState.Open)
            {
                if (CloseConnection())
                {
                    _connectionState = ConnectionState.Closed;
                }
            }
        }

        protected abstract bool OpenConnection(string serverName);
        protected abstract bool CloseConnection();
    }
}
