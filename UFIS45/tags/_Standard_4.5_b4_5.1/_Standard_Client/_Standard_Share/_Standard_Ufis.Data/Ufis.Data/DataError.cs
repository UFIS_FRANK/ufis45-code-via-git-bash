﻿using System;

namespace Ufis.Data
{
    /// <summary>
    /// Collects information relevant to a data warning or error.
    /// </summary>
    public class DataError
    {
        /// <summary>
        /// Gets the short description of the error.
        /// </summary>
        public string Message { get; internal set; }
        /// <summary>
        /// Gets the System.Exception instance that caused this error.
        /// </summary>
        public Exception InnerException { get; internal set; }

        /// <summary>
        /// Initializes the new instance of Ufis.Data.DataError class.
        /// </summary>
        public DataError() : this (null, null) { }

        /// <summary>
        /// Initializes the new instance of Ufis.Data.DataError class.
        /// </summary>
        /// <param name="message">A short description of the error.</param>
        /// <param name="exception">A System.Exception instance that caused this error.</param>
        public DataError(string message, Exception exception)
        {
            this.Message = message;
            this.InnerException = exception;
        }
    }
}
