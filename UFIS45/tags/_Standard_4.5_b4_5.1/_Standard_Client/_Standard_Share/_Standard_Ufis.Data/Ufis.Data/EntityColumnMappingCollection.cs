﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Ufis.Entities;

namespace Ufis.Data
{
    /// <summary>
    /// Represents the collection of entity column mapping objects.
    /// </summary>
    public class EntityColumnMappingCollection
    {
        private readonly IList<EntityColumnMapping> _lstEntityColumnMaps;
        private readonly IList<PropertyInfo> _lstPropertyInfos;
        private readonly IList<string> _lstColumnNames;

        /// <summary>
        /// Creates entity column mapping based on property info and add it into the collection of this 
        /// instance of Ufis.Entities.EntityColumnMappingCollection.
        /// </summary>
        /// <param name="propertyInfo">A property info of entity's attribute.</param>
        public void Add(PropertyInfo propertyInfo)
        {
            if (!_lstPropertyInfos.Contains(propertyInfo))
            {
                EntityColumnMapping entityColumnMapping = new EntityColumnMapping() { PropertyInfo = propertyInfo };

                object[] arrAttributes = propertyInfo.GetCustomAttributes(typeof(EntityAttribute), false);
                if (arrAttributes.Length > 0)
                {
                    EntityAttribute entityAttribute = (EntityAttribute)arrAttributes[0];
                    entityColumnMapping.ColumnName = entityAttribute.SerializedName;
                    entityColumnMapping.ColumnDataType = entityAttribute.SourceDataType;
                    entityColumnMapping.IsPrimaryKey = entityAttribute.IsPrimaryKey;
                    entityColumnMapping.IsUnique = entityAttribute.IsUnique;
                    entityColumnMapping.IsMandatory = entityAttribute.IsMandatory;
                    entityColumnMapping.IsReadOnly = entityAttribute.IsReadOnly;
                    entityColumnMapping.MaxLength = entityAttribute.MaxLength;
                    if (propertyInfo.PropertyType == typeof(bool))
                    {
                        entityColumnMapping.BoolConverter = new EntityColumnMapping.BooleanConverter()
                        {
                            TrueValue = entityAttribute.TrueValue,
                            FalseValue = entityAttribute.FalseValue
                        };
                    }
                }

                _lstEntityColumnMaps.Add(entityColumnMapping);
                _lstPropertyInfos.Add(propertyInfo);
                _lstColumnNames.Add(entityColumnMapping.ColumnName);
            }
        }

        /// <summary>
        /// Gets the number of items in this instance of Ufis.Entities.EntityColumnMappingCollection.
        /// </summary>
        public int Count
        {
            get
            {
                return _lstEntityColumnMaps.Count;
            }
        }

        /// <summary>
        /// Gets the entity column mapping object in this instance of Ufis.Entities.EntityColumnMappingCollection.
        /// </summary>
        /// <param name="propertyInfo">A property info of entity's attribute.</param>
        /// <returns></returns>
        public EntityColumnMapping this[PropertyInfo propertyInfo]
        {
            get
            {
                int intIndex = _lstPropertyInfos.IndexOf(propertyInfo);
                if (intIndex >= 0)
                    return _lstEntityColumnMaps[intIndex];
                else
                    return null;
            }
        }

        /// <summary>
        /// Gets the entity column mapping object in this instance of Ufis.Entities.EntityColumnMappingCollection.
        /// </summary>
        /// <param name="columnName">A table's column name.</param>
        /// <returns></returns>
        public EntityColumnMapping this[string columnName]
        {
            get
            {
                int intIndex = _lstColumnNames.IndexOf(columnName);
                if (intIndex >= 0)
                    return _lstEntityColumnMaps[intIndex];
                else
                    return null;
            }
        }

        /// <summary>
        /// Gets the entity column mapping object in this instance of Ufis.Entities.EntityColumnMappingCollection.
        /// </summary>
        /// <param name="index">An index of column mapping object in the collection.</param>
        /// <returns></returns>
        public EntityColumnMapping this[int index]
        {
            get
            {
                if (index >= 0 && index < Count)
                    return _lstEntityColumnMaps[index];
                else
                    return null;
            }
        }

        /// <summary>
        /// Initializes the new instance of Ufis.Entities.EntityColumnMappingCollection object.
        /// </summary>
        public EntityColumnMappingCollection()
        {
            _lstEntityColumnMaps = new List<EntityColumnMapping>();
            _lstPropertyInfos = new List<PropertyInfo>();
            _lstColumnNames = new List<string>();
        }
    }
}