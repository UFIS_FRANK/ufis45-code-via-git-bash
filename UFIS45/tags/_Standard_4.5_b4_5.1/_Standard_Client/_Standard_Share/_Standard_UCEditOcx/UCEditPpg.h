#if !defined(AFX_UCEDITPPG_H__56AA91FF_A8FD_4AC5_81BF_D86DAEADAA21__INCLUDED_)
#define AFX_UCEDITPPG_H__56AA91FF_A8FD_4AC5_81BF_D86DAEADAA21__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// UCEditPpg.h : Declaration of the CUCEditPropPage property page class.

////////////////////////////////////////////////////////////////////////////
// CUCEditPropPage : See UCEditPpg.cpp.cpp for implementation.

class CUCEditPropPage : public COlePropertyPage
{
	DECLARE_DYNCREATE(CUCEditPropPage)
	DECLARE_OLECREATE_EX(CUCEditPropPage)

// Constructor
public:
	CUCEditPropPage();

// Dialog Data
	//{{AFX_DATA(CUCEditPropPage)
	enum { IDD = IDD_PROPPAGE_UCEDIT };
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA

// Implementation
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Message maps
protected:
	//{{AFX_MSG(CUCEditPropPage)
		// NOTE - ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UCEDITPPG_H__56AA91FF_A8FD_4AC5_81BF_D86DAEADAA21__INCLUDED)
