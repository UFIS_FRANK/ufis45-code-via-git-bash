VERSION 5.00
Begin VB.Form frmFilter 
   Caption         =   "Filter Dialog ..."
   ClientHeight    =   2460
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   3690
   Icon            =   "frmFilter.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   2460
   ScaleWidth      =   3690
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox txtUSEC 
      Height          =   315
      Left            =   900
      TabIndex        =   9
      Top             =   1740
      Width           =   2535
   End
   Begin VB.TextBox txtTIME 
      Height          =   315
      Left            =   900
      TabIndex        =   8
      Top             =   1380
      Width           =   2535
   End
   Begin VB.TextBox txtSTAB 
      Height          =   315
      Left            =   900
      TabIndex        =   7
      Top             =   1020
      Width           =   2535
   End
   Begin VB.TextBox txtSFKT 
      Height          =   315
      Left            =   900
      TabIndex        =   6
      Top             =   660
      Width           =   2535
   End
   Begin VB.TextBox txtORNO 
      Height          =   315
      Left            =   900
      TabIndex        =   0
      Top             =   300
      Width           =   2535
   End
   Begin VB.Label Label1 
      Caption         =   "USEC:"
      Height          =   195
      Index           =   4
      Left            =   180
      TabIndex        =   5
      Top             =   1800
      Width           =   615
   End
   Begin VB.Label Label1 
      Caption         =   "TIME:"
      Height          =   195
      Index           =   3
      Left            =   180
      TabIndex        =   4
      Top             =   1440
      Width           =   615
   End
   Begin VB.Label Label1 
      Caption         =   "STAB:"
      Height          =   195
      Index           =   2
      Left            =   180
      TabIndex        =   3
      Top             =   1080
      Width           =   615
   End
   Begin VB.Label Label1 
      Caption         =   "SFKT:"
      Height          =   195
      Index           =   1
      Left            =   180
      TabIndex        =   2
      Top             =   720
      Width           =   615
   End
   Begin VB.Label Label1 
      Caption         =   "ORNO:"
      Height          =   195
      Index           =   0
      Left            =   180
      TabIndex        =   1
      Top             =   360
      Width           =   615
   End
End
Attribute VB_Name = "frmFilter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

