VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "tab.ocx"
Begin VB.Form frmEventDetails 
   Caption         =   "Details for Event ..."
   ClientHeight    =   8475
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4875
   ControlBox      =   0   'False
   Icon            =   "frmEventDetails.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   8475
   ScaleWidth      =   4875
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdLoad 
      Caption         =   "&Load"
      Height          =   315
      Left            =   4140
      TabIndex        =   12
      Top             =   1140
      Width           =   615
   End
   Begin VB.CheckBox cbRealName 
      Caption         =   "&Show readable Fieldname"
      Height          =   255
      Left            =   720
      TabIndex        =   11
      Top             =   1560
      Value           =   1  'Checked
      Width           =   3375
   End
   Begin TABLib.TAB tabData 
      Height          =   6375
      Left            =   60
      TabIndex        =   10
      Top             =   2040
      Width           =   4755
      _Version        =   65536
      _ExtentX        =   8387
      _ExtentY        =   11245
      _StockProps     =   64
   End
   Begin VB.TextBox txtUrno 
      Height          =   315
      Left            =   720
      TabIndex        =   9
      Top             =   1140
      Width           =   3375
   End
   Begin VB.TextBox txtTime 
      Height          =   315
      Left            =   720
      TabIndex        =   7
      Top             =   780
      Width           =   3375
   End
   Begin VB.TextBox txtUser 
      Height          =   315
      Left            =   720
      TabIndex        =   5
      Top             =   420
      Width           =   3375
   End
   Begin VB.TextBox txtTable 
      Height          =   315
      Left            =   2640
      TabIndex        =   3
      Top             =   60
      Width           =   1455
   End
   Begin VB.TextBox txtEvent 
      Height          =   315
      Left            =   720
      TabIndex        =   1
      Top             =   60
      Width           =   1215
   End
   Begin VB.Line Line1 
      BorderWidth     =   2
      X1              =   60
      X2              =   4860
      Y1              =   1920
      Y2              =   1920
   End
   Begin VB.Label Label5 
      Caption         =   "Urno:"
      Height          =   195
      Left            =   60
      TabIndex        =   8
      Top             =   1200
      Width           =   615
   End
   Begin VB.Label Label4 
      Caption         =   "Time:"
      Height          =   195
      Left            =   60
      TabIndex        =   6
      Top             =   840
      Width           =   615
   End
   Begin VB.Label Label3 
      Caption         =   "User:"
      Height          =   195
      Left            =   60
      TabIndex        =   4
      Top             =   480
      Width           =   615
   End
   Begin VB.Label Label2 
      Caption         =   "Table:"
      Height          =   195
      Left            =   2040
      TabIndex        =   2
      Top             =   120
      Width           =   555
   End
   Begin VB.Label Label1 
      Caption         =   "Event:"
      Height          =   195
      Left            =   60
      TabIndex        =   0
      Top             =   120
      Width           =   615
   End
End
Attribute VB_Name = "frmEventDetails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public myUrno As String

Private Sub cbRealName_Click()
    SetData myUrno
End Sub

Private Sub cmdLoad_Click()
    Dim strUrno As String
    Dim strWhere As String
    Dim strLogTab As String
    
    strLogTab = frmLoadDefinition.txtLogTable

    frmHiddenData.tabData(2).ResetContent
    frmHiddenData.tabData(2).HeaderString = "URNO,ORNO,SFKT,STAB,TIME,USEC,FLST,FVAL"
    frmHiddenData.tabData(2).HeaderLengthString = "80,80,80,80,100,80,200,200"
    frmHiddenData.tabData(2).LogicalFieldList = "URNO,ORNO,SFKT,STAB,TIME,USEC,FLST,FVAL"
    frmHiddenData.tabData(2).CedaServerName = strServer
    frmHiddenData.tabData(2).CedaPort = "3357"
    frmHiddenData.tabData(2).CedaHopo = strHOPO
    frmHiddenData.tabData(2).CedaCurrentApplication = "LogTabViewer" & "," & frmHiddenData.GetApplVersion(True)
    frmHiddenData.tabData(2).CedaTabext = "TAB"
    frmHiddenData.tabData(2).CedaUser = GetWindowsUserName()
    frmHiddenData.tabData(2).CedaWorkstation = GetWorkStationName()
    frmHiddenData.tabData(2).CedaSendTimeout = "3"
    frmHiddenData.tabData(2).CedaReceiveTimeout = "240"
    frmHiddenData.tabData(2).CedaRecordSeparator = Chr(10)
    frmHiddenData.tabData(2).CedaIdentifier = "LogTab"
    frmHiddenData.tabData(2).ShowHorzScroller True
    frmHiddenData.tabData(2).EnableHeaderSizing True

    frmLoadDefinition.cbUsers.Text = "**ALL USERS**"
    frmLoadDefinition.cbType.Text = "**ALL COMMANDS**"
    strWhere = strWhere + " WHERE ORNO='" + txtUrno.Text + "' ORDER BY TIME"
    frmHiddenData.tabData(2).CedaAction "RT", strLogTab, frmHiddenData.tabData(2).HeaderString, "", strWhere
    frmHiddenData.tabData(2).Refresh
    frmHiddenData.lblTab(2).Caption = strLogTab & ": Records = " & CStr(frmHiddenData.tabData(2).GetLineCount())
    frmHiddenData.lblTab(2).Refresh
    frmMain.SetGridFromLogTable

End Sub

Private Sub Form_Load()
    Dim ilLeft As Integer
    Dim ilTop As Integer
    
'Read position from registry and move the window
    ilLeft = GetSetting(AppName:="LogTabViewerDetail", _
                        section:="Startup", _
                        Key:="frmMainLeft", _
                        Default:=-1)
    ilTop = GetSetting(AppName:="LogTabViewerDetail", _
                        section:="Startup", _
                        Key:="frmMainTop", _
                        Default:=-1)
    If ilTop = -1 Or ilLeft = -1 Then
        Me.Move 0, 0
    Else
        Me.Move ilLeft, ilTop
    End If
'END: Read position from registry and move the window
    tabData.ResetContent
    tabData.HeaderString = "Field,Data"
    tabData.HeaderLengthString = "175,135"
    tabData.EnableHeaderSizing True

End Sub

Private Sub Form_Resize()
    Dim top As Integer
    Dim w As Integer
    Dim tabHeight As Integer
    top = tabData.top
    tabHeight = tabData.Height
    If tabHeight > 0 And Me.ScaleHeight > top + 200 Then
        tabHeight = top - (Me.ScaleHeight - top)
        If tabHeight > 0 Then
            tabData.Height = tabHeight
        End If
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim ilLeft As Integer
    Dim ilTop As Integer

    ilLeft = Me.left
    ilTop = Me.top
    SaveSetting "LogTabViewerDetail", _
                "Startup", _
                "frmMainLeft", _
                ilLeft
    SaveSetting "LogTabViewerDetail", _
                "Startup", _
                "frmMainTop", ilTop


End Sub
'=============================================
' sets the URNO, fetches the data from
' HiddenData and shows all fields in the grid
'=============================================
Public Sub SetData(strUrno As String)
    Dim strLine As String
    Dim ilItem As Long
    Dim ilItem2 As Long
    Dim cnt As Long
    Dim strFields As String
    Dim strField As String
    Dim strValues As String
    Dim myTab As TABLib.TAB
    Dim i As Long
    Dim strBuffer As String
    
    Set myTab = frmHiddenData.tabData(2)
    
    myUrno = strUrno
    tabData.ResetContent
    tabData.HeaderString = "Field,Data"
    If cbRealName.Value = vbChecked Then
        tabData.HeaderLengthString = "175,135"
    Else
        tabData.HeaderLengthString = "80,135"
    End If
    tabData.EnableHeaderSizing True
    tabData.FontName = "Arial"
    tabData.HeaderFontSize = 14
    
    strLine = myTab.GetLinesByColumnValue(0, strUrno, 0)
    If Len(strLine) > 0 Then
        ilItem = GetRealItem(strLine, 0, ",")
        
        txtEvent.Text = myTab.GetFieldValue(ilItem, "SFKT")
        txtTable.Text = myTab.GetFieldValue(ilItem, "STAB")
        txtUser.Text = myTab.GetFieldValue(ilItem, "USEC")
        txtTime.Text = myTab.GetFieldValue(ilItem, "TIME")
        If txtTime <> "" Then
            txtTime.Text = Format(CDate(CedaFullDateToVb(txtTime)), "DD.MM.YY-HH:MM")
        End If
        'txtUrno.Text = strUrno
        
        strFields = myTab.GetFieldValue(ilItem, "FLST")
        strValues = myTab.GetFieldValue(ilItem, "FVAL")
        cnt = ItemCount(strFields, Chr(30))
        For i = 0 To cnt - 1
            strField = GetRealItem(strFields, i, Chr(30))
            If strField = "URNO" Then
                txtUrno.Text = GetRealItem(strValues, i, Chr(30))
            End If
            If cbRealName.Value = vbChecked Then
                strLine = frmHiddenData.tabData(1).GetLinesByMultipleColumnValue("0,1", "AFT," + strField, 0)
                If Len(strLine) > 0 Then
                    ilItem2 = GetRealItem(strLine, 0, ",")
                    strField = frmHiddenData.tabData(1).GetFieldValue(ilItem2, "ADDI")
                End If
            End If
            strBuffer = strBuffer + strField + "," + GetRealItem(strValues, i, Chr(30)) + vbLf
        Next i
        tabData.InsertBuffer strBuffer, vbLf
    End If
    tabData.Refresh
End Sub
