VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{A2F31E92-C74F-11D3-A251-00500437F607}#1.0#0"; "ufiscom.ocx"
Begin VB.Form MainForm 
   Caption         =   "Ufis� Database Tool"
   ClientHeight    =   10590
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11115
   Icon            =   "MainForm.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   706
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   741
   StartUpPosition =   3  'Windows Default
   Begin TABLib.TAB TAB1 
      Height          =   4950
      Left            =   225
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   4860
      Width           =   10605
      _Version        =   65536
      _ExtentX        =   18706
      _ExtentY        =   8731
      _StockProps     =   64
   End
   Begin VB.Frame Frame4 
      Height          =   510
      Left            =   5265
      TabIndex        =   35
      Top             =   9915
      Width           =   2895
      Begin VB.Label lblStatus 
         Alignment       =   2  'Center
         Caption         =   "Ready."
         Height          =   240
         Left            =   45
         TabIndex        =   39
         Top             =   180
         Width           =   2805
      End
   End
   Begin VB.Frame Frame3 
      Height          =   510
      Left            =   8280
      TabIndex        =   33
      Top             =   9915
      Width           =   2625
      Begin VB.Label lblConnectStatus 
         Alignment       =   2  'Center
         Caption         =   "Not connected."
         Height          =   240
         Left            =   90
         TabIndex        =   34
         Top             =   180
         Width           =   2490
      End
   End
   Begin VB.Frame frameNavigator 
      Height          =   510
      Left            =   225
      TabIndex        =   24
      Top             =   9915
      Width           =   4920
      Begin VB.CommandButton cmdNavigator 
         Height          =   240
         Index           =   0
         Left            =   900
         Picture         =   "MainForm.frx":030A
         Style           =   1  'Graphical
         TabIndex        =   30
         Top             =   180
         Width           =   285
      End
      Begin VB.CommandButton cmdNavigator 
         DisabledPicture =   "MainForm.frx":0504
         Height          =   240
         Index           =   1
         Left            =   1260
         Picture         =   "MainForm.frx":0688
         Style           =   1  'Graphical
         TabIndex        =   29
         Top             =   180
         Width           =   285
      End
      Begin VB.CommandButton cmdNavigator 
         DisabledPicture =   "MainForm.frx":080C
         Height          =   240
         Index           =   2
         Left            =   2610
         Picture         =   "MainForm.frx":09D8
         Style           =   1  'Graphical
         TabIndex        =   28
         Top             =   180
         Width           =   285
      End
      Begin VB.CommandButton cmdNavigator 
         Height          =   240
         Index           =   3
         Left            =   2970
         Picture         =   "MainForm.frx":0BA4
         Style           =   1  'Graphical
         TabIndex        =   27
         Top             =   180
         Width           =   285
      End
      Begin VB.CommandButton cmdNavigator 
         DisabledPicture =   "MainForm.frx":0D76
         Height          =   240
         Index           =   4
         Left            =   3330
         Picture         =   "MainForm.frx":0FF2
         Style           =   1  'Graphical
         TabIndex        =   26
         Top             =   180
         Width           =   285
      End
      Begin VB.TextBox txtNoActual 
         Height          =   240
         Left            =   1620
         TabIndex        =   25
         Top             =   180
         Width           =   915
      End
      Begin VB.Label lblRecord 
         Alignment       =   2  'Center
         Caption         =   "Record:"
         Height          =   195
         Left            =   90
         TabIndex        =   32
         Top             =   190
         Width           =   780
      End
      Begin VB.Label lblNo 
         Height          =   240
         Left            =   3690
         TabIndex        =   31
         Top             =   195
         Width           =   1095
      End
   End
   Begin VB.Frame Frame2 
      Height          =   3780
      Left            =   240
      TabIndex        =   12
      Top             =   960
      Width           =   10635
      Begin VB.CommandButton btnExtras 
         Caption         =   "E&xtras ..."
         Height          =   315
         Left            =   9135
         TabIndex        =   44
         Top             =   945
         Width           =   1400
      End
      Begin VB.CommandButton btnRunCommand 
         Caption         =   "&Run Special:"
         Height          =   375
         Left            =   4365
         TabIndex        =   42
         Top             =   690
         Width           =   1400
      End
      Begin VB.CheckBox cbAutoSizeGridHeader 
         Caption         =   "&Auto Size Grid Header"
         Height          =   195
         Left            =   5940
         TabIndex        =   41
         Top             =   315
         Width           =   1875
      End
      Begin VB.CommandButton btnDeleteFieldList 
         Caption         =   "Delete &Fieldlist"
         Height          =   315
         Left            =   9135
         TabIndex        =   40
         Top             =   1350
         Width           =   1400
      End
      Begin VB.ListBox lbFields 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1110
         Left            =   1320
         MultiSelect     =   2  'Extended
         Sorted          =   -1  'True
         TabIndex        =   20
         Top             =   1350
         Width           =   7665
      End
      Begin VB.ComboBox cmbAction 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   2
         Left            =   1305
         TabIndex        =   38
         Top             =   3255
         Width           =   9195
      End
      Begin VB.ComboBox cmbAction 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   1
         Left            =   1305
         TabIndex        =   37
         Top             =   2910
         Width           =   9195
      End
      Begin VB.ComboBox cmbAction 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   0
         Left            =   1305
         TabIndex        =   36
         Top             =   2565
         Width           =   9195
      End
      Begin VB.CommandButton btnEnlarge 
         Caption         =   "&Enlarge List"
         Height          =   315
         Left            =   9135
         TabIndex        =   23
         Top             =   1740
         Width           =   1400
      End
      Begin VB.CommandButton btnUseSelectedFields 
         Caption         =   "&Append Fieldlist"
         Height          =   315
         Left            =   9135
         TabIndex        =   22
         Top             =   2130
         Width           =   1400
      End
      Begin VB.ComboBox cbCommand 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "MainForm.frx":126E
         Left            =   5940
         List            =   "MainForm.frx":127E
         Style           =   2  'Dropdown List
         TabIndex        =   19
         Top             =   720
         Width           =   1455
      End
      Begin VB.ComboBox cbTabtab 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1320
         Sorted          =   -1  'True
         TabIndex        =   18
         Top             =   240
         Width           =   4200
      End
      Begin VB.CommandButton btnUpdate 
         Caption         =   "&Update/Delete"
         Height          =   375
         Left            =   2835
         TabIndex        =   7
         Top             =   690
         Width           =   1400
      End
      Begin VB.CommandButton btnQuery 
         Caption         =   "&Query"
         Height          =   375
         Left            =   1305
         TabIndex        =   6
         Top             =   690
         Width           =   1400
      End
      Begin VB.Label lblVersion 
         Caption         =   "Version: 4.5.1"
         Height          =   240
         Left            =   9225
         TabIndex        =   43
         Top             =   180
         Width           =   1275
      End
      Begin VB.Label Label11 
         Caption         =   "Field Selector:"
         Height          =   240
         Left            =   180
         TabIndex        =   21
         Top             =   1395
         Width           =   1095
      End
      Begin VB.Label Label10 
         Caption         =   "Data:"
         Height          =   240
         Left            =   180
         TabIndex        =   17
         Top             =   3435
         Width           =   615
      End
      Begin VB.Label Label8 
         Caption         =   "Command:"
         Height          =   240
         Left            =   180
         TabIndex        =   16
         Top             =   705
         Width           =   780
      End
      Begin VB.Label Label4 
         Caption         =   "Table:"
         Height          =   240
         Left            =   180
         TabIndex        =   15
         Top             =   285
         Width           =   495
      End
      Begin VB.Label Label5 
         Caption         =   "Fields:"
         Height          =   240
         Left            =   180
         TabIndex        =   14
         Top             =   2745
         Width           =   495
      End
      Begin VB.Label Label6 
         Caption         =   "Where:"
         Height          =   240
         Left            =   180
         TabIndex        =   13
         Top             =   3090
         Width           =   615
      End
   End
   Begin UFISCOMLib.UfisCom Ufis 
      Left            =   10485
      Top             =   4680
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   661
      _StockProps     =   0
   End
   Begin VB.Frame Frame1 
      Height          =   690
      Left            =   225
      TabIndex        =   8
      Top             =   135
      Width           =   10635
      Begin VB.CommandButton btnDisconnect 
         Caption         =   "&Disconnect"
         Height          =   375
         Left            =   9135
         TabIndex        =   5
         Top             =   215
         Width           =   1400
      End
      Begin VB.CommandButton btnConnect 
         Caption         =   "&Connect"
         Height          =   375
         Left            =   7605
         TabIndex        =   4
         Top             =   215
         Width           =   1400
      End
      Begin VB.TextBox txtTableExt 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4905
         TabIndex        =   3
         Text            =   "TAB"
         Top             =   225
         Width           =   600
      End
      Begin VB.TextBox txtHopo 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2970
         TabIndex        =   2
         Text            =   "ZRH"
         Top             =   225
         Width           =   855
      End
      Begin VB.TextBox txtServer 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   810
         TabIndex        =   1
         Text            =   "DIEBURG"
         Top             =   225
         Width           =   1425
      End
      Begin VB.Label Label3 
         Caption         =   "TableExt:"
         Height          =   255
         Left            =   4140
         TabIndex        =   11
         Top             =   270
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "Hopo:"
         Height          =   255
         Left            =   2475
         TabIndex        =   10
         Top             =   270
         Width           =   495
      End
      Begin VB.Label Label1 
         Caption         =   "Server:"
         Height          =   255
         Left            =   225
         TabIndex        =   9
         Top             =   270
         Width           =   615
      End
   End
End
Attribute VB_Name = "MainForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Declare Function GetUserName Lib "advapi32.dll" _
        Alias "GetUserNameA" (ByVal lpBuffer As String, _
        nSize As Long) As Long

Private strTable As String
Private strFieldList As String
Private strWhere As String
Private strCommand As String
Private strData As String
Private strTableExt As String
Public bmIsConnected As Boolean
Private cmUrnoCollection As New Collection
Private imVisibleLines As Integer
Private imMaxHistory As Integer

Private Sub btnDeleteFieldList_Click()
    cmbAction(0).Text = ""
End Sub

Private Sub btnEnlarge_Click()
    Dim height As Integer

    height = lbFields.height
    If lbFields.height = 1110 Then
        lbFields.height = 2160
        btnEnlarge.Caption = "Sh&rink"
    Else
        lbFields.height = 1110
        btnEnlarge.Caption = "&Enlarge"
    End If
End Sub

Private Sub btnExtras_Click()
    If frmExtras.Visible = False Then
        frmExtras.Show
    End If
End Sub

Private Sub btnRunCommand_Click()
    If bmIsConnected = True Then
        If ConnectUfisCom = False Then
            MsgBox "Cannot connect to server!", vbCritical, "No connection possible!"
            DisconnectUfisCom
        End If
        strCommand = cbCommand.Text
        strData = cmbAction(2).Text
        strWhere = cmbAction(1).Text

        If strCommand = "DRT" Then
            RunDRT
        ElseIf strCommand = "IBT" Then
            RunIBT
        ElseIf strCommand = "SYS" Then
            RunSYS
        Else
            MsgBox "Not implemented yet!", vbInformation, "Sorry!"
        End If
        DisconnectUfisCom
    Else
        MsgBox "Not connected. Please connect to server, please!", vbInformation, "Not connected"
    End If
End Sub

Private Sub btnUseSelectedFields_Click()
    Dim strInsert As String
    Dim strField As String
    Dim count As Long
    Dim i As Integer

    count = lbFields.ListCount
    strField = cmbAction(0).Text
    For i = count - 1 To 0 Step -1
        If lbFields.Selected(i) = True Then
            If strField <> "" Then
                strField = strField + "," + GetItem(lbFields.List(i), 1, ",")
            Else
                strField = strField + GetItem(lbFields.List(i), 1, ",")
            End If
            lbFields.Selected(i) = False
        End If
    Next i
    cmbAction(0).Text = strField
End Sub

Private Sub cbAutoSizeGridHeader_Click()
    Dim blAuto As Boolean
    TAB1.AutoSizeByHeader = cbAutoSizeGridHeader.Value
    TAB1.AutoSizeColumns
End Sub

Private Sub cbTabtab_Click()
    Dim strSel As String
    Dim strTable As String
    Dim strField As String
    Dim strLines As String
    Dim i As Integer
    Dim idx As Integer
    Dim cnt As Integer
    Dim strLine As String
    Dim retStr As String
    Dim strInsert As String
    Dim idxFina As Integer

    strSel = cbTabtab.Text
    strTable = GetItem(strSel, 1, ",")
    If strTable = "" Then
        Exit Sub
    End If

    lbFields.Clear
    cmbAction(0).Text = ""
    cmbAction(1).Text = ""
    cmbAction(2).Text = ""
    idx = GetRealItemNo(frmDebug.tabData(0).HeaderString, "TANA")
    retStr = frmDebug.tabData(0).GetLinesByColumnValue(idx, strTable, 1)
    If Len(retStr) = 0 Then
        If Len(strTable) > 3 Then
            retStr = frmDebug.tabData(0).GetLinesByColumnValue(idx, Left(strTable, 3), 1)
        ElseIf Len(strTable) = 3 Then
            retStr = frmDebug.tabData(0).GetLinesByColumnValue(idx, strTable & "TAB", 1)
        End If
    End If
    cnt = ItemCount(retStr, ",")
    idxFina = GetRealItemNo(frmDebug.tabData(0).HeaderString, "FINA") + 1
    For i = 1 To cnt
        strLine = frmDebug.tabData(0).GetLineValues(CLng(GetItem(retStr, i, ",")))
        strField = GetItem(strLine, idxFina, ",")
        strInsert = strField + ", "
        strInsert = strInsert + GetItem(strLine, GetRealItemNo(frmDebug.tabData(0).HeaderString, "ADDI") + 1, ",")
        lbFields.AddItem strInsert
    Next i
End Sub

Private Sub cmdNavigator_Click(Index As Integer)
    Select Case Index
    Case 0:
        TAB1.OnVScrollTo 0
        TAB1.SetCurrentSelection 0
        TAB1.RedrawTab
        txtNoActual.Text = "1"
        CheckEnableNavigatorButtons
    Case 1:
        TAB1.SetCurrentSelection TAB1.GetCurrentSelected - 1
        txtNoActual.Text = TAB1.GetCurrentSelected + 1
        cmdNavigator(Index).Tag = 1
        cmdNavigator(Index + 1).Tag = 0
        CheckEnableNavigatorButtons
    Case 2:
        TAB1.SetCurrentSelection TAB1.GetCurrentSelected + 1
        txtNoActual.Text = TAB1.GetCurrentSelected + 1
        cmdNavigator(Index).Tag = 1
        cmdNavigator(Index - 1).Tag = 0
        CheckEnableNavigatorButtons
    Case 3:
        TAB1.OnVScrollTo TAB1.GetLineCount - 1
        TAB1.SetCurrentSelection TAB1.GetLineCount - 1
        TAB1.RedrawTab
        txtNoActual.Text = CStr(TAB1.GetLineCount)
        CheckEnableNavigatorButtons
    Case 4:
        NewRecord
        CheckEnableNavigatorButtons
    End Select
End Sub

Private Sub Form_Load()
    Dim strTmp As String
    bmIsConnected = False
    lblVersion.Caption = "Version: " & App.Major & "." & App.Minor & "." & App.Revision

    TAB1.LineHeight = 17
    TAB1.FontName = "Courier"
    TAB1.HeaderFontSize = 16
    TAB1.FontSize = 14
    TAB1.EnableHeaderSizing True
    TAB1.ShowHorzScroller True
    TAB1.EnableInlineEdit True
    TAB1.Visible = False
    TAB1.InplaceEditUpperCase = False
    TAB1.InplaceEditSendKeyEvents = False
    frmDebug.Visible = False

    'calculating the number of visible lines
    imVisibleLines = TAB1.height / TAB1.LineHeight

    'reading settings from the registry
    txtServer.Text = GetSetting(appName:="UfisDatabaseTool", _
                                section:="Startup", _
                                Key:="ServerName", _
                                Default:="DIEBURG")
    txtTableExt.Text = GetSetting(appName:="UfisDatabaseTool", _
                                section:="Startup", _
                                Key:="TableExtension", _
                                Default:="TAB")
    txtHopo.Text = GetSetting(appName:="UfisDatabaseTool", _
                                section:="Startup", _
                                Key:="HomeAirport", _
                                Default:="ZRH")
    strTmp = GetSetting(appName:="UfisDatabaseTool", _
                                section:="Startup", _
                                Key:="MaxHistoryEntries", _
                                Default:="20")
    'defining the maximum number of history-entries
    If IsNumeric(strTmp) = True Then
        imMaxHistory = CInt(strTmp)
    Else
        imMaxHistory = 20
    End If
    InitHistoryTabs
    HandleHistoryTabs

    'deleting entries
    Dim olComboBox As ComboBox
    For Each olComboBox In cmbAction
        olComboBox.Text = ""
    Next
End Sub

Private Sub Form_Resize()
    Dim ilTabHeight As Integer
    Dim ilTabWidth As Integer

    'calculating new dimensions of the TAB
    ilTabWidth = MainForm.Width * (1 / Screen.TwipsPerPixelX) - 40
    ilTabHeight = MainForm.height * (1 / Screen.TwipsPerPixelY) - 400

    'checking, if valid size (e.g. form is minimized)
    If ilTabWidth > 0 Then
        TAB1.Width = ilTabWidth
    End If
    If ilTabHeight > 0 Then
        TAB1.height = ilTabHeight
    End If

    'arranging bottom-frames
    frameNavigator.Top = TAB1.Top + TAB1.height + 6
    Frame3.Top = frameNavigator.Top
    Frame4.Top = frameNavigator.Top

    'calculating the number of visible lines
    imVisibleLines = TAB1.height / TAB1.LineHeight
End Sub

Private Sub Form_Unload(Cancel As Integer)
    'killing the netin on the server
    Ufis.CleanupCom

    'saving the last actions
    SaveHistoryTabs

    Unload frmDebug
    Unload frmExtras
End Sub

Private Sub btnConnect_Click()
    If bmIsConnected = False Then
        Dim sServer As String
        Dim sHopo As String
        Dim sTableExt As String
        Dim sConnectType As String
        Dim ret As Integer
        Dim i As Integer
        Dim strTab As String
        Dim sBuffer As String
        Dim strUserName As String
        Dim lSize As Long

        sServer = txtServer.Text
        sHopo = txtHopo.Text
        strTableExt = txtTableExt.Text
        sConnectType = "CEDA"
        strUserName = User_Name

        If LoadTabs = True Then
            AfterConnect
        End If

    End If
End Sub

Private Sub btnDisconnect_Click()
    Ufis.CleanupCom
    lblConnectStatus.Caption = "Not connected."
    cbTabtab.Clear
    lbFields.Clear
    bmIsConnected = False
End Sub

Private Sub btnQuery_Click()
    Dim strCmd As String
    strCmd = "RT"
    Query strCmd
End Sub

Private Sub Query(ByRef sCommand As String)
    If bmIsConnected = True Then
        Screen.MousePointer = vbHourglass
        Dim ItemCount As Integer
        Dim i As Integer
        Dim strArr() As String
        Dim headerLenStr As String
        Dim Records As Long
        Dim Line As Long
        Dim dataStr As String
        Dim recCounter As Long
        Dim llRecords As Long
        Dim strSel As String

        recCounter = 0
        strSel = cbTabtab.Text
        strTable = GetItem(strSel, 1, ",")

        If Len(strTable) < 6 Then
            strTable = UCase(strTable) & strTableExt
        End If
        strFieldList = cmbAction(0).Text
        strWhere = cmbAction(1).Text

        If Len(sCommand) = 0 Then
            strCommand = "RT"
        Else
            strCommand = sCommand
        End If

        strData = cmbAction(2).Text

        TAB1.ResetContent
        If Len(strFieldList) = 0 Then
            Dim ilIdxTana As Integer
            Dim ilIdxFina As Integer
            Dim ilIdxSyst As Integer
            Dim strLineNo As String
            Dim blAddField As Boolean
            ilIdxTana = GetItemNo(frmDebug.tabData(0).HeaderString, "TANA") - 1
            ilIdxFina = GetItemNo(frmDebug.tabData(0).HeaderString, "FINA") - 1
            ilIdxSyst = GetItemNo(frmDebug.tabData(0).HeaderString, "SYST") - 1
            strLineNo = frmDebug.tabData(0).GetLinesByColumnValue(ilIdxTana, Left(strTable, 3), 0)
            For i = 1 To UfisLib.ItemCount(strLineNo, ",")
                blAddField = True
                If strCommand = "SYS" Then
                    If "Y" <> frmDebug.tabData(0).GetColumnValue(CLng(GetItem(strLineNo, i, ",")), CLng(ilIdxSyst)) Then
                        blAddField = False
                    End If
                End If
                If blAddField = True Then
                    strFieldList = strFieldList + frmDebug.tabData(0).GetColumnValue(CLng(GetItem(strLineNo, i, ",")), CLng(ilIdxFina)) + ","
                End If
            Next i
            If Len(strFieldList) > 0 Then
                strFieldList = Left(strFieldList, Len(strFieldList) - 1)
            End If
        End If
        
        If Len(strFieldList) = 0 Then
            MsgBox "There is no field-information about the " & strTable & " in the SYSTAB. Please enter at least one field!", vbInformation, "No field"
            Screen.MousePointer = vbDefault
            Exit Sub
        End If
        strArr = Split(strFieldList, ",")
        strFieldList = ""
        ItemCount = UBound(strArr)
        For i = 0 To ItemCount
            If i + 1 > ItemCount Then
                headerLenStr = headerLenStr & "120"
                strFieldList = strFieldList & UCase(strArr(i))
            Else
                headerLenStr = headerLenStr & "120,"
                strFieldList = strFieldList & UCase(strArr(i)) & ","
            End If
        Next i
        TAB1.HeaderLengthString = headerLenStr
        TAB1.HeaderString = strFieldList
        If CInt(TAB1.GetHScrollPos) > ItemCount Then TAB1.OnHScrollTo 0
        TAB1.RedrawTab
        Me.Refresh

        InitTabForCedaConnection TAB1
        If TAB1.CedaAction(strCommand, strTable, strFieldList, strData, strWhere) = False Then
            MsgBox "Connection error!" & vbCrLf & TAB1.GetLastCedaError, vbCritical, "CallServer failed!"
        End If
        llRecords = TAB1.GetLineCount
        TAB1.AutoSizeColumns

        lblNo.Caption = "from " & llRecords
        TAB1.Visible = True
        TAB1.RedrawTab
        txtNoActual.Text = ""

        Dim idxURNO As Integer
        idxURNO = GetItemNo(strFieldList, "URNO")
        If idxURNO > 0 Then
            TAB1.NoFocusColumns = CStr(idxURNO - 1)
            lblStatus.Caption = "Ready."
        Else
            lblStatus.Caption = "No URNO in fieldlist!"
        End If

        HandleHistoryTabs
        Screen.MousePointer = vbDefault
    Else
        MsgBox "Not connected. Please connect to server, please!", vbInformation, "Not connected"
    End If
End Sub

Private Sub btnUpdate_Click()
    Dim ilUrnoCol As Integer
    ilUrnoCol = GetItemNo(strFieldList, "URNO")
    If ilUrnoCol < 1 Then
        MsgBox "No update possible, because there is no URNO in the fieldlist!", vbInformation, "No update possible"
        Exit Sub
    End If
    
    If ConnectUfisCom = False Then
        MsgBox "Cannot connect to server!", vbCritical, "No connection possible!"
        DisconnectUfisCom
        Exit Sub
    End If

    Dim strLineNo As String
    Dim strLine As String
    Dim strUpdateWhere As String
    Dim strTmp As String
    Dim llLine As Long
    Dim i As Integer
    Dim j As Integer

    Dim strUpdateString As String
    Dim strUpdateFieldlist As String

'looking for deleting lines
    strLineNo = TAB1.GetLinesByStatusValue(2, 1)
    If Len(strLineNo) > 0 Then
        Dim strDeleteFieldList As String
        strDeleteFieldList = cmbAction(0).Text
        If Len(strDeleteFieldList) = 0 Then
            'we have to build a field-list containing all available fields
            For i = 1 To ItemCount(TAB1.HeaderString, ",") - 1
                strDeleteFieldList = strDeleteFieldList & GetItem(TAB1.HeaderString, i, ",") & ","
            Next i
            strDeleteFieldList = Left(strDeleteFieldList, Len(strDeleteFieldList) - 1)
        End If

        For i = 1 To ItemCount(strLineNo, ",")
            llLine = GetItem(strLineNo, i, ",")
            strUpdateWhere = strUpdateWhere & "'" & TAB1.GetColumnValue(llLine, ilUrnoCol - 1) & "',"
        Next i
        strUpdateWhere = Left(strUpdateWhere, Len(strUpdateWhere) - 1)
        strUpdateWhere = "WHERE URNO IN (" & strUpdateWhere & ")"

        'do the delete
        Ufis.CallServer "DRT", strTable, strDeleteFieldList, "", strUpdateWhere, "240"

        'delete rows from TAB, so you can see it straight away
        For i = ItemCount(strLineNo, ",") To 1 Step -1
            llLine = GetItem(strLineNo, i, ",")
            TAB1.DeleteLine llLine
        Next i
        lblNo.Caption = "from " & TAB1.GetLineCount
        TAB1.RedrawTab
    End If

'looking for the updates
    strLineNo = TAB1.GetLinesByStatusValue(1, 1)
    If Len(strLineNo) > 0 Then
        'building the update-fieldlist
        For i = 1 To ItemCount(strFieldList, ",")
            If i <> ilUrnoCol Then
                strUpdateFieldlist = strUpdateFieldlist & GetItem(strFieldList, i, ",") & ","
            End If
        Next i
        strUpdateFieldlist = Left(strUpdateFieldlist, Len(strUpdateFieldlist) - 1)

        For i = 1 To ItemCount(strLineNo, ",")
            llLine = GetItem(strLineNo, i, ",")

            'building the update-string
            strUpdateString = ""
            For j = 1 To ItemCount(strFieldList, ",")
                If j <> ilUrnoCol Then
                    strTmp = TAB1.GetColumnValue(llLine, j - 1)
                    If Len(strTmp) = 0 Then strTmp = " "
                    strUpdateString = strUpdateString & strTmp & ","
                End If
            Next j
            strUpdateString = Left(strUpdateString, Len(strUpdateString) - 1)

            'building the where-statement
            strUpdateWhere = "WHERE URNO = '" & TAB1.GetColumnValue(llLine, ilUrnoCol - 1) & "'"

            'do the update
            Ufis.CallServer "URT", strTable, strUpdateFieldlist, strUpdateString, strUpdateWhere, "240"

            'reset line-status & colors
            TAB1.SetLineStatusValue llLine, 0
            TAB1.SetLineColor llLine, vbBlack, vbWhite
            TAB1.RedrawTab
        Next i
    End If

'looking for the inserts
    strLineNo = TAB1.GetLinesByStatusValue(4, 1)
    If Len(strLineNo) > 0 Then
        'building the update-fieldlist
        For i = 1 To ItemCount(strFieldList, ",")
            If i <> ilUrnoCol Then
                strUpdateFieldlist = strUpdateFieldlist & GetItem(strFieldList, i, ",") & ","
            End If
        Next i
        strUpdateFieldlist = Left(strUpdateFieldlist, Len(strUpdateFieldlist) - 1)

        For i = 1 To ItemCount(strLineNo, ",")
            llLine = GetItem(strLineNo, i, ",")

            'building the insert-string
            strUpdateString = TAB1.GetLineValues(llLine)
            While InStr(strUpdateString, ",,") > 0
                strUpdateString = Replace(strUpdateString, ",,", ", ,")
            Wend

            'building the where-statement
            strUpdateWhere = ""
    
            'do the insert
            Ufis.CallServer "IRT", strTable, strFieldList, strUpdateString, strUpdateWhere, "240"

            'reset line-status & colors
            TAB1.SetLineStatusValue llLine, 0
            TAB1.SetLineColor llLine, vbBlack, vbWhite
            TAB1.RedrawTab
        Next i
    End If

    DisconnectUfisCom
    lblStatus.Caption = "Ready."
End Sub

Private Sub lbFields_DblClick()
    btnUseSelectedFields_Click
End Sub

Private Sub TAB1_HitKeyOnLine(ByVal Key As Integer, ByVal LineNo As Long)
    If Key = 46 Then
        Dim llLineStatus As Long
        llLineStatus = TAB1.GetLineStatusValue(LineNo)
        If (llLineStatus And 2) = 0 Then
            'the line has to be deleted
            llLineStatus = llLineStatus + 2
            TAB1.SetLineColor LineNo, vbBlack, 12640511
            lblStatus.Caption = "Update necessary."
        Else
            'don't delete the line
            llLineStatus = llLineStatus - 2
            If (llLineStatus And 1) = 1 Then      'line is not updated yet
                TAB1.SetLineColor LineNo, vbBlack, 12648384
            Else
                TAB1.SetLineColor LineNo, vbBlack, vbWhite
            End If
        End If
        TAB1.SetLineStatusValue LineNo, llLineStatus
    End If
    TAB1.RedrawTab
End Sub

Private Sub TAB1_InplaceEditCell(ByVal LineNo As Long, ByVal ColNo As Long, ByVal NewValue As String, ByVal OldValue As String)
    If NewValue <> OldValue Then
        Dim llLineStatus As Long
        llLineStatus = TAB1.GetLineStatusValue(LineNo)
        If (llLineStatus And 2) = 0 Then                'line is not deleted yet
            If (llLineStatus And 4) = 0 Then            'it is not an insert-line
                If (llLineStatus And 1) = 0 Then        'line is not updated yet
                    llLineStatus = llLineStatus + 1
                    TAB1.SetLineStatusValue LineNo, llLineStatus
                    lblStatus.Caption = "Update necessary."
                    TAB1.SetLineColor LineNo, vbBlack, 12648384
                    TAB1.RedrawTab
                End If
            End If
        Else
            TAB1.SetColumnValue LineNo, ColNo, OldValue
        End If
    End If
End Sub

Public Function LoadTabs() As Boolean
    Dim myTab As TABLib.TAB
    Dim ilRet As Integer

    Screen.MousePointer = vbHourglass

    frmDebug.tabData(0).HeaderString = "TANA,FINA,ADDI,TYPE,FELE,FITY,REFE,LABL,URNO,SYST"
    frmDebug.tabData(0).HeaderLengthString = "80,80,120,80,80,80,80,80,80"
    frmDebug.tabData(1).HeaderString = "LTNA,ADDI,DBTY,DTYP,DVIW,LNAM,LTAB,MODU,OOPT,SMMF,SNAM,TATY,TSPA"
    frmDebug.tabData(1).HeaderLengthString = "80,80,80,80,80,80,80,80,80,80,80,80,80"

    For Each myTab In frmDebug.tabData
        myTab.ResetContent
        InitTabForCedaConnection myTab
        ilRet = myTab.CedaAction("RT", myTab.Tag, myTab.HeaderString, "", "WHERE URNO > 0")
        If ilRet <> -1 Then
            LoadTabs = False
            Screen.MousePointer = vbDefault
            Exit Function
        End If
        myTab.RedrawTab
    Next

    LoadTabs = True

    'systab sortieren
    frmDebug.tabData(0).Sort "0,1", True, True
    frmDebug.lblInfo(0).Caption = frmDebug.tabData(0).Tag + " " + CStr(frmDebug.tabData(0).GetLineCount) + " Lines read"
    frmDebug.lblInfo(1).Caption = frmDebug.tabData(1).Tag + " " + CStr(frmDebug.tabData(1).GetLineCount) + " Lines read"

    Screen.MousePointer = vbDefault
End Function

Private Function NewRecord()
    Dim ilUrnoRow As Integer
    ilUrnoRow = GetItemNo(strFieldList, "URNO")
    If ilUrnoRow < 1 Then
        MsgBox "No insert possible, because there is no URNO in the fieldlist!", vbInformation, "No insert possible"
        Exit Function
    End If

    Dim i As Integer
    Dim ilRecords As Integer
    Dim strURNO As String
    Dim strInsert As String

    'looking for enough URNOs
    If cmUrnoCollection.count < 1 Then
        If ConnectUfisCom = False Then
            MsgBox "Cannot connect to server!", vbCritical, "No connection possible!"
            DisconnectUfisCom
            Exit Function
        End If

        If Ufis.CallServer("GMU", strTable, "*", "10", "", "240") = 0 Then
            strURNO = Ufis.GetBufferLine(0)
            ilRecords = ItemCount(strURNO, ",")
            For i = 1 To ilRecords
                cmUrnoCollection.Add GetItem(strURNO, i, ",")
            Next i
        End If
        DisconnectUfisCom
    End If

    'getting the first URNO and removing it from the collection
    strURNO = cmUrnoCollection.item(1)
    cmUrnoCollection.Remove 1

    'building the update-fieldlist, putting it into the TAB and setting the line-status
    For i = 1 To ItemCount(strFieldList, ",")
        If i <> ilUrnoRow Then
            strInsert = strInsert & ","
        Else
            strInsert = strInsert & strURNO & ","
        End If
    Next i
    strInsert = Left(strInsert, Len(strInsert) - 1)
    TAB1.InsertTextLine strInsert, True
    TAB1.SetLineStatusValue TAB1.GetLineCount - 1, 4
    TAB1.SetLineColor TAB1.GetLineCount - 1, vbBlack, 12648384

    'updating the labels
    lblStatus.Caption = "Update necessary."
    lblNo.Caption = "from " & TAB1.GetLineCount
End Function

Private Function CheckEnableNavigatorButtons()
    'looking for enabling buttons
    If TAB1.GetCurrentSelected > 0 Then
        cmdNavigator(1).Enabled = True
    Else
        cmdNavigator(1).Enabled = False
    End If
    If TAB1.GetCurrentSelected < TAB1.GetLineCount - 1 Then
        cmdNavigator(2).Enabled = True
    Else
        cmdNavigator(2).Enabled = False
    End If

    'looking for scrolling the selected line into the visible area
    If TAB1.GetCurrentSelected > TAB1.GetVScrollPos + imVisibleLines - 3 Then
        TAB1.OnVScrollTo TAB1.GetVScrollPos + 1
    ElseIf TAB1.GetCurrentSelected < TAB1.GetVScrollPos Then
        TAB1.OnVScrollTo TAB1.GetVScrollPos - 1
    End If
End Function

Private Sub TAB1_PrintFinishedTab()
    TAB1.PrintRepeatStatus -1
End Sub

Private Sub TAB1_RowSelectionChanged(ByVal LineNo As Long, ByVal Selected As Boolean)
    If Selected = True Then
        TAB1_SendLButtonClick LineNo, 0
    End If
End Sub

Private Sub TAB1_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    TAB1.RedrawTab
    txtNoActual.Text = LineNo + 1
    CheckEnableNavigatorButtons
End Sub

Private Sub TAB1_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo = -1 Then
        TAB1.Sort ColNo, True, True
        TAB1.RedrawTab
    End If
End Sub

Private Sub txtNoActual_LostFocus()
    Dim blError As Boolean
    blError = False
    If IsNumeric(txtNoActual.Text) = True Then
        If CLng(txtNoActual.Text) < 1 Or CLng(txtNoActual.Text) > TAB1.GetLineCount Then
            blError = True
        Else
            TAB1.OnVScrollTo CLng(txtNoActual.Text) - 1
            TAB1.SetCurrentSelection CLng(txtNoActual.Text) - 1
            CheckEnableNavigatorButtons
        End If
    Else
        blError = True
    End If
    If blError = True Then
        MsgBox "Please enter valid record number!", vbInformation, "Invalid number"
    End If
End Sub

Private Sub HandleHistoryTabs()
    Dim strLineNo As String
    Dim strLine As String
    Dim ilLineNo As Integer
    Dim i As Integer
    Dim j As Integer
    Dim blHistory(3) As Boolean

    'saving the last action in the history-TABs
    For i = 0 To frmDebug.TabHistory.count - 1
        If Len(Trim(cmbAction(i).Text)) > 0 Then
            strLineNo = frmDebug.TabHistory(i).GetLinesByColumnValue(0, cmbAction(i).Text, 0)
            frmDebug.TabHistory(i).InsertTextLineAt 0, cmbAction(i).Text, False
            If Len(strLineNo) > 0 And IsNumeric(strLineNo) = True Then
                ilLineNo = CInt(strLineNo)
                frmDebug.TabHistory(i).DeleteLine ilLineNo + 1
            End If
            blHistory(i + 1) = True
        Else
            blHistory(i + 1) = False
        End If
    Next i

    'looking after the maximal number of history-items
    For i = 0 To frmDebug.TabHistory.count - 1
        If frmDebug.TabHistory(i).GetLineCount > imMaxHistory Then
            frmDebug.TabHistory(i).DeleteLine imMaxHistory
        End If
    Next i

    'reset and refill the comboboxes
    For i = 0 To cmbAction.count - 1
        cmbAction(i).Clear
        For j = 0 To frmDebug.TabHistory(i).GetLineCount - 1
            strLine = frmDebug.TabHistory(i).GetLineValues(j)
            If Len(strLine) > 2 Then
                strLine = Left(strLine, Len(strLine) - 3)
                cmbAction(i).AddItem strLine
            End If
        Next j
        If cmbAction(i).ListCount > 0 Then
            If blHistory(i + 1) = True Then
                cmbAction(i).ListIndex = 0
            End If
        End If
    Next i
End Sub

Private Sub InitHistoryTabs()
    Dim i As Integer
    Dim j As Integer
    Dim strBuffer As String
    Dim strTmp As String

    For i = 0 To frmDebug.TabHistory.count - 1
        frmDebug.TabHistory(i).SetFieldSeparator "@|@"
        frmDebug.TabHistory(i).HeaderLengthString = "50"
        frmDebug.TabHistory(i).HeaderString = "ITEM"
        frmDebug.TabHistory(i).ResetContent

        'reading from the registry
        strBuffer = ""
        For j = 1 To imMaxHistory
            strTmp = GetSetting(appName:="UfisDatabaseTool", section:=CStr(i), Key:=CStr(j), Default:="")
            If Len(Trim(strTmp)) > 0 Then
                If j < imMaxHistory Then
                    strBuffer = strBuffer & strTmp & Chr(10)
                Else
                    strBuffer = strBuffer & strTmp
                End If
            End If
        Next j
        If Len(strBuffer) > 0 Then
            frmDebug.TabHistory(i).InsertBuffer strBuffer, Chr(10)
        End If
    Next i
End Sub

Private Sub SaveHistoryTabs()
    Dim i As Integer
    Dim j As Integer
    Dim strLine As String

    For i = 0 To frmDebug.TabHistory.count - 1
        'saving the history in the registry
        For j = 1 To frmDebug.TabHistory(i).GetLineCount
            strLine = frmDebug.TabHistory(i).GetLineValues(j - 1)
            If Len(strLine) > 0 Then
                SaveSetting "UfisDatabaseTool", CStr(i), CStr(j), strLine
            End If
        Next j
    Next i
End Sub

Private Sub InitTabForCedaConnection(ByRef rTab As TABLib.TAB)
    'do the initializing for the CEDA-connection
    rTab.CedaServerName = txtServer.Text
    rTab.CedaPort = "3357"
    rTab.CedaHopo = txtHopo.Text
    rTab.CedaCurrentApplication = "UDT"
    rTab.CedaTabext = "TAB"
    rTab.CedaUser = User_Name

    rTab.CedaWorkstation = GetWorkStationName
    rTab.CedaSendTimeout = "120"
    rTab.CedaReceiveTimeout = "240"
    rTab.CedaRecordSeparator = Chr(10)
    rTab.CedaIdentifier = "UDT"

    'do general initializing
    rTab.ShowHorzScroller True
    rTab.EnableHeaderSizing True
End Sub

Private Sub RunIBT()
    'are you sure?
    Select Case MsgBox("Are you sure to run the IBT-command for all displayed records?", vbOKCancel, "Insert without URNOs?")
        Case vbCancel
            Exit Sub
        Case Else
            'just continue
    End Select
    
    Dim ilUrnoCol As Integer
    Dim i As Integer
    Dim l As Long
    Dim strInsertFieldlist As String
    Dim strInsertString As String
    Dim strTmp As String
    Dim strRetURNO As String

    'looking after the URNO-column
    ilUrnoCol = GetItemNo(TAB1.HeaderString, "URNO")
    If ilUrnoCol < 1 Then
        TAB1.HeaderString = TAB1.HeaderString & ",URNO"
        TAB1.RedrawTab
        ilUrnoCol = GetItemNo(TAB1.HeaderString, "URNO")
    End If

    'building the insert-fieldlist
    For i = 1 To ItemCount(TAB1.HeaderString, ",") - 1
        If i <> ilUrnoCol Then
            strInsertFieldlist = strInsertFieldlist & GetItem(TAB1.HeaderString, i, ",") & ","
        End If
    Next i
    strInsertFieldlist = Left(strInsertFieldlist, Len(strInsertFieldlist) - 1)

    'running through the lines of the TAB
    For l = 0 To TAB1.GetLineCount - 1
        lblStatus.Caption = CStr(TAB1.GetLineCount - l) & " to process."
        'building the insert-string
        strInsertString = ""
        For i = 1 To ItemCount(strFieldList, ",")
            If i <> ilUrnoCol Then
                strTmp = TAB1.GetColumnValue(l, i - 1)
                If Len(strTmp) = 0 Then strTmp = " "
                strInsertString = strInsertString & strTmp & ","
            End If
        Next i
        strInsertString = Left(strInsertString, Len(strInsertString) - 1)

        'do the insert
        strRetURNO = ""
        If Ufis.CallServer("IBT", strTable, strInsertFieldlist, strInsertString, strRetURNO, "240") = 0 Then
            'looking after the urno
            If Len(strRetURNO) = 0 Then
                strRetURNO = GetItem(Ufis.GetDataBuffer(0), 1, ",")
            End If

            'put the received URNO into the TAB and look after status and color
            TAB1.SetColumnValue l, ilUrnoCol - 1, strRetURNO
            TAB1.SetLineStatusValue l, 0
            TAB1.SetLineColor l, vbBlack, vbWhite
        Else
            MsgBox Ufis.LastErrorMessage, vbCritical, "CallServer failed!"
            'continue?
            Dim strMsg As String
            strMsg = "Do you want to go on? The last record was no. " & CStr(l) & vbCrLf & vbCrLf & _
                    "Fieldlist: " & strInsertFieldlist & vbCrLf & _
                    "Data:      " & strInsertString
            Select Case MsgBox(strMsg, vbOKCancel, "Continue?")
                Case vbCancel
                    Exit Sub
                Case Else
                    'just continue
            End Select
        End If
    Next l
    TAB1.RedrawTab
    lblStatus.Caption = "Ready."
End Sub
Private Sub RunDRT()
    'are you sure?
    Select Case MsgBox("Are you sure to run the delete-command?", vbOKCancel, "Deleting records?")
        Case vbCancel
            Exit Sub
        Case Else
            'just continue
    End Select

    Dim llRecords As Long
    Dim recCounter As Long
    Dim l As Long
    Dim dataStr As String
    Dim strFieldList As String
    Dim i As Integer
    
    strFieldList = cmbAction(0).Text
    If Len(strFieldList) = 0 Then
        'we have to build a field-list containing all available fields
        For i = 1 To ItemCount(TAB1.HeaderString, ",") - 1
            strFieldList = strFieldList & GetItem(TAB1.HeaderString, i, ",") & ","
        Next i
        strFieldList = Left(strFieldList, Len(strFieldList) - 1)
    End If

    ' Read from ceda and insert the buffer directly into the grid
    If Ufis.CallServer(strCommand, strTable, strFieldList, strData, strWhere, "240") = 0 Then
        llRecords = Ufis.GetBufferCount()
        For l = 0 To llRecords - 1
            recCounter = recCounter + 1
            dataStr = dataStr + Ufis.GetBufferLine(l) + Chr(10)
            If recCounter = 500 Then
                recCounter = 0
                TAB1.InsertBuffer dataStr, Chr(10)
                dataStr = ""
            End If
        Next l
        If dataStr <> "" Then
            TAB1.InsertBuffer dataStr, Chr(10)
        End If
    Else
        MsgBox Ufis.LastErrorMessage, vbCritical, "CallServer failed!"
    End If
End Sub

Private Sub RunSYS()
    Dim strCmd As String
    strCmd = "SYS"
    Query strCmd
End Sub

Private Function User_Name() As String
  Dim l&, Ergebnis&, Fehler&
  Dim User$, Puffer$
 
      'Benutzernamen ermitteln
      User = Space(255)
      l = 255
      Ergebnis = GetUserName(User, l)
 
      If Ergebnis <> 0 Then
         User_Name = Left$(User, l - 1)
      Else
         User_Name = ""
      End If
End Function
 
Private Function ConnectUfisCom() As Boolean
    Dim sServer As String
    Dim sHopo As String
    Dim sTableExt As String
    Dim sConnectType As String
    Dim ret As Integer
    Dim strTab As String
    Dim sBuffer As String
    Dim strUserName As String

    Ufis.CleanupCom

    sServer = txtServer.Text
    sHopo = txtHopo.Text
    strTableExt = txtTableExt.Text
    sConnectType = "CEDA"
    strUserName = User_Name

    ret = Ufis.InitCom(sServer, sConnectType)
    If ret <> -1 Then
        ConnectUfisCom = False
        Exit Function
    End If

    Ufis.WorkStation = GetWorkStationName
    Ufis.Twe = sHopo & "," & strTableExt & "," & "UDT"
    ret = Ufis.SetCedaPerameters(strUserName, sHopo, strTableExt)
    If ret <> -1 Then
        ConnectUfisCom = False
        Exit Function
    End If

    ConnectUfisCom = True
End Function

Private Sub DisconnectUfisCom()
    Ufis.CleanupCom
End Sub

Public Sub AfterConnect()
    Dim i As Integer
    Dim strTab As String

    bmIsConnected = True
    strTableExt = txtTableExt.Text
    lblConnectStatus.Caption = "Connected with: " & txtServer.Text
    cbTabtab.Clear
    Dim ilLtnaIdx As Integer
    Dim ilLnamIdx As Integer
    ilLtnaIdx = GetItemNo(frmDebug.tabData(1).HeaderString, "LTNA")
    ilLnamIdx = GetItemNo(frmDebug.tabData(1).HeaderString, "LNAM")

    For i = 0 To frmDebug.tabData(1).GetLineCount - 1
        strTab = CStr(frmDebug.tabData(1).GetColumnValue(i, ilLtnaIdx - 1) + ", " + frmDebug.tabData(1).GetColumnValue(i, ilLnamIdx - 1))
        cbTabtab.AddItem strTab
    Next i

    ' Place the server-settings in the registry.
    SaveSetting "UfisDatabaseTool", "Startup", "ServerName", txtServer.Text
    SaveSetting "UfisDatabaseTool", "Startup", "TableExtension", txtTableExt.Text
    SaveSetting "UfisDatabaseTool", "Startup", "HomeAirport", txtHopo.Text
    SaveSetting "UfisDatabaseTool", "Startup", "MaxHistoryEntries", CStr(imMaxHistory)
End Sub
