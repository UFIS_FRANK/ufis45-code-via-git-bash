﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Ufis.Entities;
using System.Drawing;

namespace Ufis.Data.Ceda
{
    /// <summary>
    /// Represents the class for coverting data retrieved from CEDA server into EntityCollection object.
    /// </summary>
    public class CedaDataConverter
    {   
        /// <summary>
        /// Initializes the new instance of Ufis.Data.Ceda.CedaDataConverter class.
        /// </summary>
        public CedaDataConverter() { }

        /// <summary>
        /// Fills list of entities with rows from Ufis.Data.Ceda.CedaDataRowCollection object.
        /// </summary>
        /// <param name="entityList">A System.Collections.Generic.IList to fill in.</param>
        /// <param name="rows">A Ufis.Data.Ceda.CedaDataRowCollection object that provides the data.</param>
        /// <param name="entityColumnMappings">The ColumnMappings object that contains
        /// mapping of entity's attributes to the table's columns.</param>
        /// <param name="columnList">The column list.</param>
        /// <param name="timeZoneInfo">The TimeZoneInfo class that represents local time zone.</param>
        /// <param name="acceptChanges">A value indicating whether Ufis.Entities.Entity.AcceptChanges() 
        ///  is called on Ufis.Entities.Entity after it is added to the collection.</param>        
        public void FillEntityList<T>(IList<T> entityList, DataRowCollection rows,
            EntityColumnMappingCollection entityColumnMappings, string columnList, 
            TimeZoneInfo timeZoneInfo, bool acceptChanges)
        {
            if (rows != null)
            {
                Type type = typeof(T);
                string[] arrColumns = columnList.Split(',');

                foreach (DataRow row in rows.GetRowArray())
                {
                    if (row.ItemArray.Length >= arrColumns.Length)
                    {
                        object objItem = Activator.CreateInstance(type);

                        if (entityColumnMappings != null)
                        {
                            int intIndex = 0;
                            foreach (string strColumn in arrColumns)
                            {
                                EntityColumnMapping entityColumnMapping = entityColumnMappings[strColumn];
                                if (entityColumnMapping != null)
                                {
                                    string strRowValue = row[intIndex++];
                                    //remove the \0 characters
                                    strRowValue = strRowValue.Replace("\0", string.Empty);

                                    PropertyInfo info = entityColumnMapping.PropertyInfo;
                                    object objValue = CedaItemToDataItem(info.PropertyType, strRowValue, timeZoneInfo, entityColumnMapping.BoolConverter);
                                    info.SetValue(objItem, objValue, null);
                                }
                            }
                        }
                        else
                        {
                            PropertyInfo[] arrInfos = type.GetProperties();
                            foreach (PropertyInfo info in arrInfos)
                            {
                                object[] arrAttributes = info.GetCustomAttributes(typeof(EntityAttribute), false);
                                if (arrAttributes.Length > 0)
                                {
                                    EntityAttribute entityAttribute = (EntityAttribute)arrAttributes[0];
                                    string strColumnName = entityAttribute.SerializedName;
                                    int intIndex = Array.IndexOf(arrColumns, strColumnName);
                                    if (intIndex >= 0)
                                    {
                                        Ufis.Data.EntityColumnMapping.BooleanConverter boolConverter =
                                            new EntityColumnMapping.BooleanConverter() 
                                            { 
                                                TrueValue = entityAttribute.TrueValue,
                                                FalseValue = entityAttribute.FalseValue
                                            };
                                        object objValue = CedaItemToDataItem(info.PropertyType, row[intIndex], timeZoneInfo, boolConverter);
                                        info.SetValue(objItem, objValue, null);
                                    }
                                }
                            }
                        }

                        Entity entity = (Entity)objItem;
                        T item = (T)objItem;
                        if (Array.IndexOf(arrColumns, "URNO") < 0 || !entityList.Contains(item))
                        {
                            entityList.Add((T)objItem);
                        }
                        else
                        {
                            int intIndex = entityList.IndexOf(item);
                            entityList[intIndex] = item;
                        }

                        if (acceptChanges) entity.AcceptChanges();
                    }
                }
            }
        }

        /// <summary>
        /// Converts ceda item value into primitive value.
        /// </summary>
        /// <param name="dataType">Data type</param>
        /// <param name="value">A ceda item value</param>
        /// <param name="timeZoneInfo">The TimeZoneInfo class that represents local time zone.</param>
        /// <param name="boolConverter">The Ufis.Data.EntityColumnMapping.BooleanConverter object.</param>
        /// <returns></returns>
        public object CedaItemToDataItem(Type dataType, string value, TimeZoneInfo timeZoneInfo, Ufis.Data.EntityColumnMapping.BooleanConverter boolConverter)
        {            
            object objDataItem = null;

            if (!string.IsNullOrEmpty(value) || dataType == typeof(System.String) || dataType == typeof(System.Boolean))
            {
                if (dataType == typeof(System.Int32) || dataType == typeof(System.Int32?))
                {
                    Int32 iValue;
                    if (Int32.TryParse(value, out iValue))
                    {
                        objDataItem = iValue;
                    }
                }
                if (dataType == typeof(System.Int64) || dataType == typeof(System.Int64?))
                {
                    Int64 iValue;
                    if (Int64.TryParse(value, out iValue))
                    {
                        objDataItem = iValue;
                    }
                }
                if (dataType == typeof(System.Double) || dataType == typeof(System.Double?))
                {
                    Double dValue;
                    if (Double.TryParse(value, out dValue))
                    {
                        objDataItem = dValue;
                    }
                }
                if (dataType == typeof(System.DateTime) || dataType == typeof(System.DateTime?))
                {
                    int valueLength = value.Length;
                    DateTime dateTime;
                    if (valueLength == 14 && CedaUtils.CedaFullDateToDateTime(value, out dateTime))
                    {
                        dateTime = TimeZoneInfo.ConvertTime(dateTime, timeZoneInfo);
                        objDataItem = dateTime;
                    }
                    else if (valueLength == 8 && CedaUtils.CedaDateToDateTime(value, out dateTime))
                    {
                        dateTime = TimeZoneInfo.ConvertTime(dateTime, timeZoneInfo);
                        objDataItem = dateTime;
                    }
                }
                if (dataType == typeof(System.TimeSpan) || dataType == typeof(System.TimeSpan?))
                {
                    TimeSpan timeSpan;
                    if (CedaUtils.CedaTimeToTimeSpan(value, out timeSpan))
                    {
                        objDataItem = timeSpan;
                    }
                }
                if (dataType == typeof(System.Boolean))
                {
                    objDataItem = (boolConverter != null && boolConverter.TrueValue != null && value != null && boolConverter.TrueValue.Equals(value));
                }
                if (dataType == typeof(System.String))
                {
                    if (value == null) value = string.Empty;
                    value = CedaUtils.ConvertStringForClient(value.TrimEnd());
                    objDataItem = value;
                }
            }

            return objDataItem;
        }

        /// <summary>
        /// Converts primitive value into ceda item value.
        /// </summary>
        /// <param name="value">A primitive value.</param>
        /// <param name="dataType">Value data type.</param>
        /// <param name="timeZoneInfo">The TimeZoneInfo class that represents local time zone.</param>
        /// <param name="boolConverter">The Ufis.Data.EntityColumnMapping.BooleanConverter object.</param>
        /// <param name="maxLength">The maximum length of data for the column.</param>
        /// <param name="withParentheses">Indicates whether to enclose the data with parentheses.</param>
        /// <returns></returns>
        public string DataItemToCedaItem(object value, Type dataType, TimeZoneInfo timeZoneInfo, Ufis.Data.EntityColumnMapping.BooleanConverter boolConverter, 
            int maxLength, bool withParentheses)
        {            
            string strCedaItem = " ";
            
            if (dataType == typeof(Int32) || dataType == typeof(Int32?) ||
                dataType == typeof(Int64) || dataType == typeof(Int64?) ||
                dataType == typeof(Double) || dataType == typeof(Double?))
            {
                if (value == null && withParentheses)
                    strCedaItem = "0";
                else
                    strCedaItem = (value == null ? " " : value.ToString());
            }
            if (dataType == typeof(DateTime) || dataType == typeof(DateTime?))
            {
                if (value != null)
                {
                    DateTime dateTime = new DateTime(((DateTime)value).Ticks, DateTimeKind.Unspecified);
                    dateTime = TimeZoneInfo.ConvertTimeToUtc(dateTime, timeZoneInfo);
                    if (maxLength == 8)
                        strCedaItem = CedaUtils.DateTimeToCedaDate(dateTime);
                    else
                        strCedaItem = CedaUtils.DateTimeToCedaDateTime(dateTime);
                }
                if (withParentheses)
                    strCedaItem = string.Format("{0}{1}{0}", "'", strCedaItem);
            }
            if (dataType == typeof(TimeSpan) || dataType == typeof(TimeSpan?))
            {
                if (value != null)
                {
                    TimeSpan timeSpan = (TimeSpan)value;
                    strCedaItem = CedaUtils.TimeSpanToCedaTime(timeSpan);
                }
                if (withParentheses)
                    strCedaItem = string.Format("{0}{1}{0}", "'", strCedaItem);
            }
            if (dataType == typeof(Boolean))
            {
                Boolean boolean = (Boolean)value;
                if (boolConverter != null)
                {
                    if (boolean && boolConverter.TrueValue != null)
                        strCedaItem = boolConverter.TrueValue.ToString();
                    else if ((!boolean) && boolConverter.FalseValue != null)
                        strCedaItem = boolConverter.FalseValue.ToString();
                }
                if (withParentheses)
                    strCedaItem = string.Format("{0}{1}{0}", "'", strCedaItem);
            }
            if (value is String)
            {
                if (value != null)
                    strCedaItem = CedaUtils.ConvertStringForServer(value.ToString());
                if (withParentheses)
                    strCedaItem = string.Format("{0}{1}{0}", "'", strCedaItem);
            }

            return strCedaItem;
        }

    }
}
