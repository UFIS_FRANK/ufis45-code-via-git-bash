﻿using System;
using System.Net;
using System.Net.Sockets;

namespace Ufis.Data.Ceda
{
    /// <summary>
    /// Represents an open connection to CEDA.
    /// </summary>
    public class CedaConnection : ConnectionBase
    {
        private Socket _writerSocket;

        /// <summary>
        /// Gets the socket class that connects to SQLHDL.
        /// </summary>
        internal Socket WriterSocket { get { return _writerSocket; } }
        /// <summary>
        /// Gets or sets the connection identifier to CEDA.<br>
        /// Currently not used.
        /// </summary>
        internal string Identifier { get; set; }
        /// <summary>
        /// Gets or sets the error simulation identifier.
        /// </summary>
        internal string SimulateError { get; set; }

        /// <summary>
        /// Gets or sets the packet size of client/server data exchange.
        /// </summary>
        public long PacketSize { get; set; }
        /// <summary>
        /// Gets or sets the row separator of data received from server.
        /// </summary>
        public string RowSeparator { get; set; }
        /// <summary>
        /// Gets or sets windows code page for encoding data recieved from server.
        /// </summary>
        public int WindowsCodePage { get; set; }
        /// <summary>
        /// Gets or sets the port number for CDRHDL. Default is 3357.
        /// </summary>
        public int ReaderPort { get; set; }
        /// <summary>
        /// Gets or sets the port number for SQLHDL. Default is 3350.
        /// </summary>
        public int WriterPort { get; set; }
        /// <summary>
        /// Gets or sets whether to use CDRHDL to fetch data from server for RT/RTA command.
        /// </summary>
        public bool UseCedaReader { get; set; }
        
        /// <summary>
        /// Initializes the new instance of Ufis.Data.Ceda.CedaConnection class.
        /// </summary>
        public CedaConnection()
        {
            ReaderPort = 3357;
            WriterPort = 3350;
            UseCedaReader = true;
            RowSeparator = "\n";
            SimulateError = string.Empty;
            WindowsCodePage = 1252;
            PacketSize = 1000;            
        }

        protected override bool OpenConnection(string serverName)
        {
            _writerSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            try
            {
                IPHostEntry ipHostEntry = Dns.GetHostEntry(serverName);
                IPAddress ipAddress = ipHostEntry.AddressList[0];
                IPEndPoint ipEndPoint = new IPEndPoint(ipAddress, WriterPort);
                _writerSocket.Connect(ipEndPoint);
            }
            catch (Exception ex)
            {
                _writerSocket = null;

                throw ex;
            }

            return true;
        }

        protected override bool CloseConnection()
        {
            if (_writerSocket != null)
            {
                _writerSocket.Shutdown(SocketShutdown.Both);
                _writerSocket.Close();
                _writerSocket = null;
            }
            return true;
        }
    }
}
