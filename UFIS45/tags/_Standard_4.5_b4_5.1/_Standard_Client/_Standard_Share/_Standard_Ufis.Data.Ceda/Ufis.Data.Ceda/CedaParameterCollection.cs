﻿using System;

namespace Ufis.Data.Ceda
{
    /// <summary>
    /// Represents a collection of parameters relevant to an Ufis.Data.CedaCommand.
    /// </summary>
    public class CedaParameterCollection
    {
        private string[] _arrCommandParameters = new string[] { "", "", "", "" };

        /// <summary>
        /// </summary>
        /// <param name="index">The zero-based index parameter to retrieve.</param>
        /// <returns></returns>
        public string this[int index]
        {
            get { return _arrCommandParameters[index]; }
            set { _arrCommandParameters[index] = value; }
        }
    }
}
