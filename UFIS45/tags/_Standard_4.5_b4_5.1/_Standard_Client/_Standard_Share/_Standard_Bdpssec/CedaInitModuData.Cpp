// CedaInitModuData.cpp
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
 
#include <stdafx.h>
#include <CedaInitModuData.h>
#include <resource.h>
#include <BasicData.h>

CedaInitModuData ogInitModuData;


//--CEDADATA-----------------------------------------------------------------------------------------------

CedaInitModuData::CedaInitModuData()
{
	strcpy(pcmInitModuFieldList,"APPL,SUBD,FUNC,FUAL,TYPE,STAT");
}

//--~CEDADATA----------------------------------------------------------------------------------------------

CedaInitModuData::~CedaInitModuData(void)
{
}

CString CedaInitModuData::GetInitModuTxt()
{
	CString olInitModuData;
	olInitModuData  = CString(pcgAppName)+",";
	olInitModuData += "InitModu,InitModu,InitModu,B,0";
	return olInitModuData;
}

//--SEND INIT MODULS-------------------------------------------------------------------------------------------

bool CedaInitModuData::SendInitModu()
{
	bool ilRc = true;

	CString olInitModuData;
	olInitModuData  = GetInitModuTxt();

	char *pclInitModuData = new char[olInitModuData.GetLength()+3];
	strcpy(pclInitModuData,olInitModuData);
	char pclTable[5];
	sprintf(pclTable,"%s ",pcgTableExt);

	char pclSort[50] = "";
	char pclWhere[50] = "";
	char pclCom[50] = "SMI";
	ilRc = CedaAction(pclCom,pclTable,pcmInitModuFieldList,pclWhere,pclSort,pclInitModuData);
	if(ilRc==false)
	{
		AfxMessageBox(GetString(IDS_DATABASE_ERROR) + omLastErrorMessage,MB_ICONEXCLAMATION);
	}
	delete pclInitModuData;
	return ilRc;
}

