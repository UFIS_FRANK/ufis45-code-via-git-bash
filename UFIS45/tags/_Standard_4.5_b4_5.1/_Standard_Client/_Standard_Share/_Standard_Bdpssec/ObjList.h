#ifndef __OBJLIST_H__
#define __OBJLIST_H__

#include <CCSPtrArray.h>


#define OBJLIST_KEYWORD	"#OBJLIST"

struct STATLIST {

	char	*STAT;	// status eg. "0"
	char	*DESC;	// description of status eg. "Enabled"
};

struct OBJLIST {

	char	*TYPE;	// object type eg. "B"
	char	*DESC;	// object description eg. "Button"
    CCSPtrArray <STATLIST> Stats; // list of statuses
};


class ObjList
{
private:

    CCSPtrArray <OBJLIST> omObjList;
	bool	bmValidLine;
	char	mDescNotFound[100];
	void	ProcessLine(const char *pcpLine);
	void	CheckForError(const char *pcpItem);
	char*	ProcessItem(char *pcpLine, char **pppItem);
	void	RemoveAll();
	void	DeleteRec(OBJLIST *prpObjRec);
	CString GetLanguageFromCedaIni(void);
	bool	ReadSection(const char *pcpFilename, CString opSectionName);

public:

	char pcmLastError[500]; // errors from file open/read

	ObjList();
	~ObjList();
	bool	Load(const char *pcpFilename);
	bool	GetDesc(const char *pcpType, const char *pcpStat, char *pcpTypeDesc, char *pcpStatDesc);
	bool	NextStat(const char *pcpType, const char *pcpStat, char *pcpNextStat, char *pcpNextStatDesc);

};



extern ObjList ogObjList;



#endif // __OBJLIST_H__