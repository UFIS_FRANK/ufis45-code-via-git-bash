#ifndef __CEDASEC_DATA__
#define __CEDASEC_DATA__

#include <CedaCalData.h>	// need CALDATA --> returned from create user
#include <CedaGrpData.h>	// need GRPDATA --> returned from create user

static void CedaSecDataCf(void *popInstance, int ipDDXType, void *vpDataPointer, CString &ropInstanceName);

#include <CCSGlobl.h>
#include <CCSCedaData.h>
/////////////////////////////////////////////////////////////////////////////
struct SECDATA {

	char	URNO[URNOLEN];
	char	USID[USIDLEN];
	char	NAME[NAMELEN];
	char	TYPE[TYPELEN];
	char	LANG[LANGLEN]; // [0] '1' = superuser  [1] count of attempted logins (not used in BDPSSEC)
	char	STAT[STATLEN];
	char	REMA[REMALEN];

	int IsChanged;

	SECDATA(void)
	{
		memset(URNO, 0, sizeof(URNO));
		memset(USID, 0, sizeof(USID));
		memset(NAME, 0, sizeof(NAME));
		memset(TYPE, 0, sizeof(TYPE));
		memset(LANG, 0, sizeof(LANG));
		memset(STAT, 0, sizeof(STAT));
		memset(REMA, 0, sizeof(REMA));
		IsChanged = DATA_UNCHANGED;
	}

	SECDATA& SECDATA::operator=(const SECDATA& rhs)
	{
		if (&rhs != this)
		{
			strcpy(URNO, rhs.URNO);
			strcpy(USID, rhs.USID);
			strcpy(NAME, rhs.NAME);
			strcpy(TYPE, rhs.TYPE);
			strcpy(LANG, rhs.LANG);
			strcpy(STAT, rhs.STAT);
			strcpy(REMA, rhs.REMA);
			IsChanged = rhs.IsChanged;
		}		
		return *this;
	}
};

#define ORA_NOT_FOUND "ORA-01403"

class CedaSecData: public CCSCedaData
{

// Attributes
public:
    CCSPtrArray <SECDATA> omData;
	CMapStringToPtr omUrnoMap;
	CCSPtrArray <CEDARECINFO> omRetRecInfo; // contains record info for reading CRU return values


// Operations
public:
    CedaSecData();
	~CedaSecData();
	void ClearAll(void);

    bool Read();
	bool Add(SECDATA *prpSec);

	SECDATA* GetSecByUrno(const char *pcpUrno);
	SECDATA* GetSecByUsid(const char *pcpUSID, SEC_REC_TYPE rpCurrType);
	bool NoRecords(const char *pcpType);

	// NEWSEC - insert into DB function call
	bool NewSec( SECDATA  *prpSecRec, CCSPtrArray <CALDATA> &ropCalRecs, const int ipMODU, char *pcpErrorMessage);

	// NEWAPP - insert appl into DB
	bool NewApp( SECDATA  *prpSecRec, CCSPtrArray <CALDATA> &ropCalRecs, char *pcpErrorMessage);

	// UPDSEC - update DB function call
	bool UpdSec( SECDATA  *prpSecRec, char *pcpErrorMessage);

	// DELSEC - delete from DB function call
	bool DelSec( const char *pcpUrno, char *pcpErrorMessage);

	// DELAPP - delete appl from DB function call
	bool DelApp( const char *pcpUrno, char *pcpErrorMessage);

	// SPW - reset password
	bool SetPwd( const char *pcpUrno, const char *pcpPass, char *pcpErrorMessage );

	// update a record in omData
	void Update( SECDATA  *prpSec );

	// insert a record into omData
	void Insert( SECDATA  *prpSec);

	// delete a record from omData
	void Delete( const char  *pcpUrno);

	bool IsSuperuser(SECDATA  *prpSec);
	bool IsSuperuser(const char *pcpLANG);
	void SetIsSuperuser(SECDATA  *prpSec, bool bpIsSuperuser = true);


	void ProcessSecBc(int ipDDXType, void *vpDataPointer, CString &ropInstanceName);
	SECDATA *AddSecInternal(SECDATA &rrpSec);
	void DeleteSecInternal(char *pcpUrno);
};

extern CedaSecData ogCedaSecData;

#endif //__CEDASEC_DATA__
