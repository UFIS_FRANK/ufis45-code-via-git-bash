// Ldap.h: interface for the CLdap class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LDAP_H__E186D079_B52C_46E0_8E7A_060DFAA6004F__INCLUDED_)
#define AFX_LDAP_H__E186D079_B52C_46E0_8E7A_060DFAA6004F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "winldap.h" 

enum AUTHENTICATION_METHOD {
   AUTH_ANNONYMOUS   = 1,            // login as nobody
   AUTH_CLEAR_TEXT   = 2,            // user info is visible to through the net
   AUTH_LOGGED_USER  = 3             // this will only work on W2k
}  ;

enum LDAP_WAIT_RESULT {
   LDAP_WAIT_SUCCESS = 0,            // request succeded...
   LDAP_WAIT_ERROR   = 1,            // an error occurs while waiting...
   LDAP_WAIT_TIMEOUT = 2             // wait timeout occurs...
}  ;


extern char *LDAP_NAMEATTR_DN ;
extern char *LDAP_NAMEATTR_ObjectClass ;
extern char *LDAP_NAMEATTR_CN ;
extern char *LDAP_NAMEATTR_SAMAccount ;
extern char *LDAP_NAMEATTR_displayName ;
extern char *LDAP_NAMEATTR_userPassword ;
extern char *LDAP_TEMPLATE_ACCT ;
extern char *LDAP_TEMPLATE_CN ;
extern char *LDAP_TEMPLATE_USER_DN ;
extern char *LDAP_TEMPLATE_ACCT_DN ;
extern int   LDAP_NAMEATTR_userPasswordLen ;

class CLdap  
{
public:
	
	void LoadSettings();
	CString GetErrorDescription(int errcode);
	CString GetLoginUserName();
	CString GetActiveUser();
	CString GetLoginComputerName();
	CString GetDomainName();
	bool IsDomainUser();


	//LDAP related functions
	bool AuthenticateUser(char *pUserId,char *pPassword,AUTHENTICATION_METHOD eAuthMethod);
	bool Connect(char *pLdapServer);
	LDAP_WAIT_RESULT WaitForOneResult(int iMessid);

	CLdap();
	virtual ~CLdap();

	char *pHttpHost ;              // pointer to the HTTP host
	char *pHttpService ;           // pointer to HTTP service name
	char *pHttpRequest ;           // pointer to request
	char *pHttpResponse ;          // pointer to good response
	char *pHttpFirstHeadChars ;    // pointer to first header line
	char *pLdapHost ;             // pointe to LDAP host
    char *pLogUserId ;            // login user id
    char *pLogPassword ;          // login password

	ULONG     uLastErr ;          // keep last error

	LDAP  *psLdap ;

	CString m_machineName;
	CString m_domain;
	CString m_osUsername;
	long   DefaultTimeout ;        // default timeout


};

class LDAPEntry {
   private:

      char        *pAttr ;            // pointer to attribute
      char       **pVals ;            // pointer to values array
      int          iVal ;             // index of current value
      BerElement  *pBer ;             // pointer to ber element
      LDAPMessage *pResult ;          // pointer to message result on find...

   public:

      LDAP        *psLdap ;           // pointer to ldap object
      LDAPMessage *pEntry ;           // message use to traverse directory

      bool  Eof ;                     // true if there are no more entries

      char *pNamingContext ;          
      char *pEntryName ;
      char *pEntryValue ;

      void Init(LDAP *pLdap,char *pDN) { pEntry = NULL ; psLdap = pLdap ;
         pNamingContext = pDN ;
         pEntryName = pEntryValue = NULL ; pAttr = NULL ; pBer = NULL ;
         pVals = NULL ; pResult = NULL ; } ;

      LDAPEntry() { Init(NULL,NULL) ; } ;
      LDAPEntry(LDAP *pLdap,char *pDN) {Init(pLdap,pDN) ; } ;

     ~LDAPEntry() { Free() ; } ;

      void Free() {
            if (pEntryValue) free(pEntryValue) ;
            if (pAttr)       ldap_memfree(pAttr) ;
            if (pVals)       ldap_value_free(pVals) ;
            if (pResult)     ldap_msgfree(pResult) ;
            Init(psLdap,pNamingContext) ;
         }  ;

      inline bool GetDistinguisedName(TCHAR *pOut,int iMaxLen)
         { TCHAR *dn = ldap_get_dn(psLdap,pEntry) ;

           if (dn) {
              int ilen = strlen(dn) ;
              if (ilen > iMaxLen) ilen = iMaxLen ;
              memcpy(pOut,dn,ilen) ;
              pOut += ilen ;
              ldap_memfree(dn) ;
           }
           *pOut = NULL ;
           return(dn != NULL) ;
         } ;

      bool Find(char *pBase,ULONG scope,char *pFilter,char **pAttrs) ;
      bool Find(char *pBase,ULONG scope,char *pFilter,char *pAttr) ;
      bool Find(char *pBase,ULONG scope,char *pFilter) ;
      bool Find(char *pBase)
		{ return(Find(pBase,LDAP_SCOPE_BASE,TEXT("objectclass=*"))) ; } ;
      bool Find()
         { return(Find(NULL)) ; } ;

      bool First() ;
      bool Next() ;

}  ;

#endif // !defined(AFX_LDAP_H__E186D079_B52C_46E0_8E7A_060DFAA6004F__INCLUDED_)
