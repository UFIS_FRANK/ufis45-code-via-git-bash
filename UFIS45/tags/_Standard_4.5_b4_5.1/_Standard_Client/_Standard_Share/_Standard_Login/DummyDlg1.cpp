// DummyDlg1.cpp : implementation file
//

#include "stdafx.h"
#include "AatLogin.h"
#include "DummyDlg1.h"
#include "UfisCom.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDummyDlg dialog


CDummyDlg::CDummyDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDummyDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDummyDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CDummyDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDummyDlg)
	DDX_Control(pDX, IDC_UFISCOMCTRL1, m_UfisCom);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDummyDlg, CDialog)
	//{{AFX_MSG_MAP(CDummyDlg)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDummyDlg message handlers

BOOL CDummyDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

IDispatch *CDummyDlg::GetUfisComCtrl()
{
	this->m_UfisCom.EnableAutomation();
	IUnknown* lpIUnknown = this->m_UfisCom.GetControlUnknown ();

	IDispatch*lpDispatch = NULL;
	lpIUnknown->QueryInterface (IID_IDispatch,(void**)&lpDispatch);
	
	if (lpDispatch == NULL)
	{
		MessageBox(_T("no UfisCom created !"));
	}
	
	return lpDispatch;
}

void CDummyDlg::OnDestroy() 
{
	LPDISPATCH lpDispatch = this->GetUfisComCtrl();
	if (lpDispatch)
	{
		// first release from GetControlUnknown
		ULONG nRef = lpDispatch->Release();

		// next release from QueryInterface
		 nRef = lpDispatch->Release();
	}
	CDialog::OnDestroy();
	
	// TODO: Add your message handler code here
}
