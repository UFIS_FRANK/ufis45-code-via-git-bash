// LoginDlg.cpp : implementation file
//

#include "stdafx.h"
#include "AatLogin.h"
#include "LoginDlg.h"
#include "InfoDlg.h"
#include "AatLoginCtl.h"
#include "RegisterDlg.h"
#include "PasswordDlg.h"
#include <DUfisCom.h>
#include <CCSBasicFunc.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLoginDlg dialog

#define BLACK   RGB(  0,   0,   0)
#define MAROON  RGB(128,   0,   0)          // dark red
#define GREEN   RGB(  0, 128,   0)          // dark green
#define OLIVE   RGB(128, 128,   0)          // dark yellow
#define NAVY    RGB(  0,   0, 128)          // dark blue
#define PURPLE  RGB(128,   0, 128)          // dark magenta
#define TEAL    RGB(  0, 128, 128)          // dark cyan
#define GRAY    RGB(128, 128, 128)          // dark gray
#define SILVER  RGB(192, 192, 192)          // light gray
#define RED     RGB(255,   0,   0)
#define ORANGE  RGB(255, 132,   0)
#define LIME    RGB(  0, 255,   0)          // green
#define YELLOW  RGB(255, 255,   0)
#define BLUE    RGB(  0,   0, 255)
#define FUCHSIA RGB(255,   0, 255)          // magenta
#define AQUA    RGB(  0, 255, 255)          // cyan
#define WHITE   RGB(255, 255, 255)

CLoginDlg::CLoginDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLoginDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLoginDlg)
	//}}AFX_DATA_INIT
	pomLoginCtrl = (CAatLoginCtrl *)pParent;
	bmExtendedSecurity = FALSE;
	imMinPasswordLength= 1;
}

CLoginDlg::CLoginDlg(CWnd* pParent /*=NULL*/,CAatLoginCtrl *popLoginCtrl)
	: CDialog(CLoginDlg::IDD, pParent)
{
	pomLoginCtrl = popLoginCtrl;
	bmExtendedSecurity = FALSE;
	imMinPasswordLength= 1;
}


void CLoginDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLoginDlg)
	DDX_Control(pDX, IDC_STATIC_SCENE, m_LB_Scene);
	DDX_Control(pDX, IDC_SCENE, m_CE_Scene);
	DDX_Control(pDX, IDC_COMBO_SCENE, m_CB_Scene);
	DDX_Control(pDX, IDC_USERNAME, m_TxtUserName);
	DDX_Control(pDX, IDC_PASSWORD, m_TxtPassword);
	DDX_Control(pDX, IDC_USIDCAPTION, m_LblUserName);
	DDX_Control(pDX, IDC_PASSCAPTION, m_LblPassword);
	DDX_Control(pDX, IDC_VERSION_STRING, m_LblVersion);
	DDX_Control(pDX, IDOK, m_BtnOk);
	DDX_Control(pDX, IDCANCEL, m_BtnCancel);
	DDX_Control(pDX, IDC_ABOUT, m_BtnAbout);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLoginDlg, CDialog)
	//{{AFX_MSG_MAP(CLoginDlg)
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_ABOUT, OnAbout)
	ON_WM_CTLCOLOR()
	ON_CBN_SELCHANGE(IDC_COMBO_SCENE, OnSelchangeComboScene)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLoginDlg message handlers

BOOL CLoginDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	ASSERT(pomLoginCtrl);

	CString olUserName;

	if (pomLoginCtrl->GetHomeAirport() == "SIN")
	{
		this->bmExtendedSecurity = TRUE;
		this->imMinPasswordLength= 8;
	}

	olUserName = this->GetSetting("AatLoginCtrl","Login","LastUserName","");
	if (olUserName.GetLength() == 0)
	{
		char polBuffer[512];
		ULONG nSize = sizeof(polBuffer);

		if (::GetUserNameA(polBuffer,&nSize))
		{
			polBuffer[nSize] = '\0';
			olUserName = polBuffer;
			if (pomLoginCtrl->bmUserNameLCase)
			{
				olUserName.MakeLower();
			}
		}
	}

	this->m_TxtUserName.SetTypeToString("X(32)",32,1);
	this->m_TxtUserName.SetBKColor(YELLOW);
	this->m_TxtUserName.SetTextErrColor(RED);
	this->m_TxtUserName.SetWindowText(olUserName);

	this->m_TxtPassword.SetTypeToString("X(32)",32,1);

	this->m_TxtPassword.SetBKColor(YELLOW);
	this->m_TxtPassword.SetTextErrColor(RED);


	this->m_TxtUserName.SetFocus();

	if(!pomLoginCtrl->IsUsingMultipleAirports())
	{
		this->m_LB_Scene.ShowWindow(SW_HIDE);
		this->m_CB_Scene.ShowWindow(SW_HIDE);
		this->m_CE_Scene.ShowWindow(SW_HIDE);
	}
	else
	{
		ConnectToCeda();
		// Read from ceda
		_DUfisCom olUfisCom;
		olUfisCom.AttachDispatch(pomLoginCtrl->GetUfisComCtrl(),TRUE);

		if (olUfisCom.CallServer("PLOG", pomLoginCtrl->GetHomeAirport(), "", "","", "0") == 0)
		{
				int llRecords = olUfisCom.GetBufferCount();
		 		if(llRecords > 0)
				{
					
					CString olTabExt;
					for (int i = 0; i < llRecords; i++)
					{
						CString olTmp = CString(olUfisCom.GetBufferLine(i));
						olTabExt += "\n" + olTmp;
					}
					
					GetTableExtensions(olTabExt);
				}

		}

		//Add the extension to combobox
		CMapStringToString *polExtensionMap = pomLoginCtrl->GetTableExtensionsMap();
		CString olKey,olValue;
		m_CB_Scene.AddString(" ");
		for (POSITION pos = polExtensionMap->GetStartPosition(); pos != NULL;)
		{
			polExtensionMap->GetNextAssoc(pos,olKey,olValue);
			m_CB_Scene.AddString(olKey);
		}
		m_CB_Scene.SetCurSel(0);

	}

/******	
	int ilY = ::GetSystemMetrics(SM_CYSCREEN);
	CRect olRect, olNewRect;
	GetWindowRect(&olRect);
	int ilHeight = olRect.bottom - olRect.top;
	int ilWidth = olRect.right - olRect.left;
	olNewRect.top = (int)((int)(ilY/2) - (int)(ilHeight/2));
	olNewRect.left = 300;
	olNewRect.bottom = olNewRect.top + ilHeight;
	olNewRect.right = olNewRect.left + ilWidth;

	MoveWindow(&olNewRect);
********/

	if (pomLoginCtrl->omInfoAppVersion.GetLength() > 0)
	{
		this->m_LblVersion.SetWindowText(pomLoginCtrl->omInfoAppVersion);
		CWnd *polWnd = GetDlgItem(IDC_STATIC_PICTURE);
		if (polWnd && polWnd->IsWindowVisible())
			this->m_LblVersion.SetWindowPos(polWnd,0,0,0,0,SWP_NOSIZE|SWP_NOMOVE);  
	}
	else
	{
		this->m_LblVersion.ShowWindow(SW_HIDE);
	}

	if (!pomLoginCtrl->bmInfoButtonVisible)
	{
		this->m_BtnAbout.ShowWindow(SW_HIDE);
	}



	

	CString olCaption;
	olCaption = LoadStg(IDS_STRING1000) + " '" + pomLoginCtrl->omInfoAppName + "' ";
	olCaption+= LoadStg(IDS_STRING1001) + pomLoginCtrl->HostName() + " / " + pomLoginCtrl->HostType();
    this->SetWindowText(olCaption);

	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CLoginDlg::OnPaint() 
{
	// TODO: Add your message handler code here
	CWnd *polWnd = GetDlgItem(IDC_STATIC_PICTURE);
	if (!polWnd || !polWnd->IsWindowVisible())
	{
		CPaintDC dc(this); // device context for painting
    
		// TODO: Add your message handler code here
/****
		CBrush olBrush( RGB( 192, 192, 192 ) );
		CBrush *polOldBrush = dc.SelectObject( &olBrush );
		CRect olRect;
		GetClientRect( &olRect );
		dc.FillRect( &olRect, &olBrush );
		dc.SelectObject( polOldBrush );
******/
		CDC olDC;
		olDC.CreateCompatibleDC( &dc );
		CBitmap olBitmap;
		olBitmap.LoadBitmap( IDB_LOGIN );
		olDC.SelectObject( &olBitmap );
		dc.BitBlt( 0, 140, 409, 52, &olDC, 0, 0, SRCCOPY );
		olDC.DeleteDC( );
		// Do not call CDialog::OnPaint() for painting messages
	}

	if (pomLoginCtrl != NULL)
	{
		CPaintDC dc(this); // device context for painting
		CRect olRect;
		GetClientRect( &olRect );
		pomLoginCtrl->DrawLifeStyle(&dc,olRect,7,0,true,70);
	}
}
						
void CLoginDlg::OnOK() 
{


	// TODO: Add extra validation here
	CString olUserName;
	CString olPassword;
	CString	olData;

	this->m_TxtUserName.GetWindowText(olUserName);
	this->m_TxtPassword.GetWindowText(olPassword);

	if (!this->m_TxtUserName.GetStatus())
	{
		MessageBox(LoadStg(IDS_STRING1040),LoadStg(IDS_ERRORCAPTION),MB_ICONSTOP|MB_OK);
		this->m_TxtUserName.SetFocus();
		return;
	}

	if (!this->m_TxtPassword.GetStatus())
	{
		MessageBox(LoadStg(IDS_STRING1041),LoadStg(IDS_ERRORCAPTION),MB_ICONSTOP|MB_OK);

		this->m_TxtPassword.SetFocus();
		return;
	}


	this->pomLoginCtrl->omUserName = olUserName;
	this->pomLoginCtrl->omPassword = olPassword;

	this->pomLoginCtrl->SetLoginResult("");

	CString olTabExt("");
	if(this->pomLoginCtrl->IsUsingMultipleAirports())
	{
		
		int ilCurSel = m_CB_Scene.GetCurSel();
		m_CB_Scene.GetLBText(ilCurSel, olTabExt);
		this->pomLoginCtrl->SetSelectedExt(olTabExt);
	}

	ConnectToCeda();

	// Read from ceda
	_DUfisCom olUfisCom;
	olUfisCom.AttachDispatch(pomLoginCtrl->GetUfisComCtrl(),TRUE);

again:
    olUfisCom.SetUserName(olUserName);
	this->SaveSetting("AatLoginCtrl","Login","LastUserName",olUserName);
	olUfisCom.SetTwe(pomLoginCtrl->GetTwe());

	if(olTabExt.GetLength()>0)
	{
		olUfisCom.SetTableExt(olTabExt);
	}
	olData = olUserName + "," + olPassword + "," + pomLoginCtrl->omApplicationName + "," + pomLoginCtrl->GetRealWorkstationName();

	
    if (olUfisCom.CallServer("GPR", pomLoginCtrl->GetHomeAirport(), "USID,PASS,APPL,WKST", olData,"", "360") == 0)
	{
        if (olUfisCom.GetBufferCount() > 0 && olUfisCom.GetBufferLine(0) == "[PRV]")
		{
			CString olText;

            int llRecords = olUfisCom.GetBufferCount();
			int ilStart = 1;

			if(llRecords > 1)
			{
				CString olTmp = CString(olUfisCom.GetBufferLine(1));
				if( olTmp.Find("[DAY]") >= 0)
				{
					int ilDays;
					if(olTmp.GetLength() > 5)
					{
						CString olTmp2; 
						olTmp2 = olTmp.Mid(5, olTmp.GetLength());
						ilDays = atoi(olTmp2.GetBuffer(0));
						if(ilDays != 0)
						{
							olText.Format("You must change your Password within the next %d days.", ilDays);
							MessageBox(olText, "Information", MB_OK |  MB_ICONINFORMATION);
						}

					}
					ilStart = 2;
				}
				else if(olTmp.Find("[PRO]") >= 0)
				{
					CMapStringToString *polProfileMap = pomLoginCtrl->GetProfileMap();
					if(olTmp.GetLength() > 0)
					{
						CStringArray olItemList;
						::ExtractItemList(olTmp,&olItemList,',');
						//not blank
						if(olItemList.GetSize() > 5)
						{
							CString olStrprofileID = olItemList[0];
							if (!polProfileMap->Lookup(olStrprofileID,olStrprofileID))
							{
								polProfileMap->SetAt(olStrprofileID,olStrprofileID);
							}
						}
					}
				}
			}
			

            // delete all entries in the lookup-collection
			CMapStringToString *polStatusMap = pomLoginCtrl->GetStatusMap();
			polStatusMap->RemoveAll();

			// add the entries to the collection
             llRecords = olUfisCom.GetBufferCount();
            for (int i = ilStart; i < llRecords; i++)
			{
				CString olTmpStr = olUfisCom.GetBufferLine(i);
				CStringArray olItemList;
				::ExtractItemList(olTmpStr,&olItemList,',');
				if (olItemList.GetSize() >= 3)
				{
					CString olStrKey = olItemList[1];
					olStrKey.TrimRight();
					olStrKey.TrimLeft();

					CString olStrItem = olItemList[2];
					olStrItem.TrimRight();
					olStrItem.TrimLeft();
					
					CString olValue;
					if (!polStatusMap->Lookup(olStrKey,olValue))
					{
						polStatusMap->SetAt(olStrKey,olStrItem);
					}
				}
			}

			olUfisCom.ClearDataBuffer();

            if ((*polStatusMap)["InitModu"] == "1" && this->pomLoginCtrl->omApplicationName != CString("BDPS-SEC"))
			{
                this->ShowWindow(SW_HIDE);
				CRegisterDlg olRegisterDlg(this->pomLoginCtrl);
				olRegisterDlg.DoModal();
			}
            else
			{
				this->pomLoginCtrl->SetLoginResult("OK");
			}

		}
		else
		{
			if (MessageBox(GetErrorMessageFromCode(olUfisCom.GetLastErrorMessage()),LoadStg(IDS_ERRORCAPTION),MB_ICONSTOP|MB_RETRYCANCEL) == IDRETRY)
				this->pomLoginCtrl->SetLoginResult("ERROR");
			else
				this->pomLoginCtrl->SetLoginResult("CANCEL");
		}
	}
	else
	{
		CString olErrorMessage = olUfisCom.GetLastErrorMessage();
		// KKH - RFC 434 Password Expiry Feature 11/01/2007
		int cnt;		
		if(olErrorMessage.Left(37) == "LOGINERROR MUST_CHANGE_PASSWORD_FIRST")
		//if (olErrorMessage == "LOGINERROR MUST_CHANGE_PASSWORD_FIRST")
		{
			cnt = olErrorMessage.GetLength() - 37;
			olErrorMessage = olErrorMessage.Right(cnt);
			
			// KKH - RFC 434 Password Expiry Feature 11/01/2007
			int ilStat = MessageBox(LoadStg(IDS_CHANGE_PASSWORD) + olErrorMessage,LoadStg(IDS_ERRORCAPTION),MB_ICONSTOP|MB_YESNOCANCEL);
			//int ilStat = MessageBox(GetErrorMessageFromCode(olErrorMessage),LoadStg(IDS_ERRORCAPTION),MB_ICONSTOP|MB_YESNOCANCEL);			
			if (ilStat == IDYES)
			{
				CPasswordDlg olPasswordDlg(this,this->pomLoginCtrl);
				olPasswordDlg.DoModal();
				if (this->pomLoginCtrl->GetLoginResult() == "OK")
				{
					olPassword = this->pomLoginCtrl->omPassword;
					goto again;
				}
			}
			else if (ilStat == IDNO)
			{
				this->pomLoginCtrl->SetLoginResult("ERROR");
			}
			else
			{
				this->pomLoginCtrl->SetLoginResult("CANCEL");
			}
		}
		else if(olErrorMessage == "LOGINERROR INVALID_PASSWORD DEACTIVATE_USER")
		{
			CString olErr1, olErr2;
			olErr1.Format(LoadStg(IDS_USER_DEACTIVATED), olUserName);
			olErr2.Format("%s\n\n%s", GetErrorMessageFromCode("LOGINERROR INVALID_PASSWORD"), olErr1);
			MessageBox(olErr2,LoadStg(IDS_ERRORCAPTION),MB_ICONSTOP|MB_OK);
			this->pomLoginCtrl->SetLoginResult("ERROR");
		}
		else if(olErrorMessage == "LOGINERROR EXPIRED_USER")
		{
			CString olErr1, olErr2;
			olErr1.Format(LoadStg(IDS_USER_DEACTIVATED), olUserName);
			olErr2.Format("%s\n\n%s", GetErrorMessageFromCode("LOGINERROR INVALID_PASSWORD"), olErr1);
			MessageBox(olErr2,LoadStg(IDS_ERRORCAPTION),MB_ICONSTOP|MB_OK);
			this->pomLoginCtrl->SetLoginResult("ERROR");
		}
		else if(olErrorMessage == "LOGINERROR OVERDUE LOGIN")
		{
			CString olErr1, olErr2;
			/*	"Your user account has been deactivated because it has not been used for $PAR days"*/

			int ilDays = GetDaysBeforeDeactivation();

			olErr1.Format(LoadStg(IDS_OVERDUE_LOGIN), ilDays);
			MessageBox(olErr1,LoadStg(IDS_ERRORCAPTION),MB_ICONSTOP|MB_OK);
			this->pomLoginCtrl->SetLoginResult("ERROR");
		}
		else if(this->pomLoginCtrl->imLoginAttempts > 1)
		{
			if (MessageBox(GetErrorMessageFromCode(olErrorMessage),LoadStg(IDS_ERRORCAPTION),MB_ICONSTOP|MB_RETRYCANCEL) == IDRETRY)
				this->pomLoginCtrl->SetLoginResult("ERROR");
			else
				this->pomLoginCtrl->SetLoginResult("CANCEL");
		}
		else
		{
			MessageBox(GetErrorMessageFromCode(olErrorMessage),LoadStg(IDS_ERRORCAPTION),MB_ICONSTOP|MB_OK);
			this->pomLoginCtrl->SetLoginResult("ERROR");
		}
	}

	/*
	if (this->pomLoginCtrl->GetLoginResult() == "OK")
	{
		if (pomLoginCtrl->omPassword.GetLength() < this->imMinPasswordLength)
		{
			CString olMsg;
			olMsg.Format(LoadStg(IDS_INVALID_PASSWORD_LENGTH),this->imMinPasswordLength);
			int ilStat = MessageBox(olMsg,LoadStg(IDS_ERRORCAPTION),MB_ICONSTOP|MB_YESNOCANCEL);
			if (ilStat == IDYES)
			{
				CPasswordDlg olPasswordDlg(this,this->pomLoginCtrl);
				olPasswordDlg.DoModal();
			}
			else if (ilStat == IDNO)
			{
				this->pomLoginCtrl->SetLoginResult("ERROR");
			}
			else
			{
				this->pomLoginCtrl->SetLoginResult("CANCEL");
			}
		}

	}
	*/

	if (this->pomLoginCtrl->GetLoginResult() == "OK")
	{
		CString olApp;
		CString olCustomer;

		if(this->pomLoginCtrl->GetWarning(olApp, olCustomer))
		{
			CString olText = LoadStg(IDS_WARNING_LOGIN);

			olText.Format(IDS_WARNING_LOGIN, olApp, olCustomer);	
			MessageBox(olText,LoadStg(IDS_WARNINGCAPTION),MB_ICONINFORMATION|MB_OK);
		}

		/*
		if (this->bmExtendedSecurity)
		{
			MessageBox(LoadStg(IDS_UNAUTHORIZED_ACCESS),LoadStg(IDS_WARNINGCAPTION),MB_ICONINFORMATION|MB_OK);
		}
		*/

		if (this->pomLoginCtrl->pomDummyDlg != NULL)
		{
			olUfisCom.CleanupCom();
		}
	}


	CDialog::OnOK();
}

void CLoginDlg::OnCancel() 
{
	this->pomLoginCtrl->SetLoginResult("CANCEL");
	CDialog::OnCancel();
}

void CLoginDlg::OnAbout() 
{
	// TODO: Add your control notification handler code here
	this->ShowWindow(SW_HIDE);
	CInfoDlg olInfoDlg(this,this->pomLoginCtrl);
	olInfoDlg.DoModal();
	this->ShowWindow(SW_SHOWNORMAL);
	this->RedrawWindow();
}

BOOL CLoginDlg::ConnectToCeda()
{
	this->pomLoginCtrl->SetCedaIsConnected(FALSE);

	_DUfisCom olUfisCom;
	olUfisCom.AttachDispatch(pomLoginCtrl->GetUfisComCtrl(),TRUE);

    olUfisCom.CleanupCom();
	olUfisCom.ClearDataBuffer();

	olUfisCom.SetCedaPerameters(olUfisCom.GetUserName(),pomLoginCtrl->GetHomeAirport(),pomLoginCtrl->TableExtension());

    int ilRet = olUfisCom.InitCom(pomLoginCtrl->HostName(),"CEDA");

    if (ilRet == 0)
	{
		CString olMsg;
		olMsg.Format("Connection to CEDA failed! \n Homeairport: %s \n Hostname: %s \n TableExtension: %s\n ConnectType: %s",
					pomLoginCtrl->GetHomeAirport(),
					pomLoginCtrl->HostName(),
					pomLoginCtrl->TableExtension(),
					"CEDA"
					);

		MessageBox(olMsg,LoadStg(IDS_ERRORCAPTION),MB_ICONSTOP|MB_OK);

        this->pomLoginCtrl->SetLoginResult("ERROR");
	}
    else
	{
        this->pomLoginCtrl->SetCedaIsConnected(TRUE);
	}

	return this->pomLoginCtrl->GetCedaIsConnected();
}

CString CLoginDlg::DoSilentLogin(const CString& ropUserName,const CString& ropPassword)
{
	CString	olData;

	this->pomLoginCtrl->omUserName = ropUserName;
	this->pomLoginCtrl->omPassword = ropPassword;

	if (!this->ConnectToCeda())
	{
		return this->pomLoginCtrl->GetLoginResult();
	}

	// Read from ceda
	_DUfisCom olUfisCom;
	olUfisCom.AttachDispatch(pomLoginCtrl->GetUfisComCtrl(),TRUE);

    olUfisCom.SetUserName(ropUserName);

	olUfisCom.SetTwe(pomLoginCtrl->GetTwe());

	olData = ropUserName + "," + ropPassword + "," + pomLoginCtrl->omApplicationName + "," + pomLoginCtrl->GetRealWorkstationName();

	CString olText;

    if (olUfisCom.CallServer("GPR", pomLoginCtrl->GetHomeAirport(), "USID,PASS,APPL,WKST", olData,"", "360") == 0)
	{
        if (olUfisCom.GetBufferCount() > 0 && CString(olUfisCom.GetBufferLine(0)).Find("[PRV]") >= 0)
		{
            // delete all entries in the lookup-collection
			CMapStringToString *polStatusMap = pomLoginCtrl->GetStatusMap();
			polStatusMap->RemoveAll();

			// add the entries to the collection
            int llRecords = olUfisCom.GetBufferCount();
            for (int i = 1; i < llRecords; i++)
			{
				CString olTmpStr = olUfisCom.GetBufferLine(i);

				CStringArray olItemList;
				::ExtractItemList(olTmpStr,&olItemList,',');

				if (olItemList.GetSize() >= 3)
				{
					CString olStrKey = olItemList[1];
					olStrKey.TrimRight();
					olStrKey.TrimLeft();

					CString olStrItem = olItemList[2];
					olStrItem.TrimRight();
					olStrItem.TrimLeft();
					
					CString olValue;
					if (!polStatusMap->Lookup(olStrKey,olValue))
					{
						polStatusMap->SetAt(olStrKey,olStrItem);
					}
				}
			}

            if ((*polStatusMap)["InitModu"] == "1")
			{
                this->ShowWindow(SW_HIDE);
				CRegisterDlg olRegisterDlg(this->pomLoginCtrl);
				olRegisterDlg.DoModal();
			}
            else
			{
				this->pomLoginCtrl->SetLoginResult("OK");
			}

		}
		else
		{
			this->pomLoginCtrl->SetLoginResult("ERROR");
		}
	}
	else
	{
		this->pomLoginCtrl->SetLoginResult("ERROR");
	}

	return this->pomLoginCtrl->GetLoginResult();
	
}


CString	CLoginDlg::GetSetting(const CString& ropAppName,const CString& ropSection,const CString& ropKey,const CString& ropDefaultValue)
{
	HKEY hAppKey;
	if (RegOpenKey(HKEY_CURRENT_USER,ropAppName,&hAppKey) != ERROR_SUCCESS)
		return ropDefaultValue;

	HKEY hSectionKey;
	if (RegOpenKey(hAppKey,ropSection,&hSectionKey) != ERROR_SUCCESS)
	{
		RegCloseKey(hAppKey);
		return ropDefaultValue;
	}

	char pchBuffer[512];
	long nSize = sizeof(pchBuffer);
	if (RegQueryValue(hSectionKey,ropKey,pchBuffer,&nSize) != ERROR_SUCCESS)
	{
		RegCloseKey(hSectionKey);
		RegCloseKey(hAppKey);
		return ropDefaultValue;
	}

	pchBuffer[nSize] = '\0';
	RegCloseKey(hSectionKey);
	RegCloseKey(hAppKey);

	return CString(pchBuffer);
}

BOOL CLoginDlg::SaveSetting(const CString& ropAppName,const CString& ropSection,const CString& ropKey,const CString& ropValue)
{
	HKEY hAppKey;
	if (RegOpenKey(HKEY_CURRENT_USER,ropAppName,&hAppKey) != ERROR_SUCCESS)
	{
		if (RegCreateKey(HKEY_CURRENT_USER,ropAppName,&hAppKey) != ERROR_SUCCESS)
			return FALSE;
	}

	HKEY hSectionKey;
	if (RegOpenKey(hAppKey,ropSection,&hSectionKey) != ERROR_SUCCESS)
	{
		if (RegCreateKey(hAppKey,ropSection,&hSectionKey) != ERROR_SUCCESS)
		{
			RegCloseKey(hAppKey);
			return FALSE;
		}
	}

	char pchBuffer[512];
	strcpy(pchBuffer,ropValue);
	long nSize = strlen(pchBuffer);
	if (RegSetValue(hSectionKey,ropKey,REG_SZ,pchBuffer,nSize) != ERROR_SUCCESS)
	{
		RegCloseKey(hSectionKey);
		RegCloseKey(hAppKey);
		return FALSE;
	}

	RegCloseKey(hSectionKey);
	RegCloseKey(hAppKey);

	return TRUE;
}

CString CLoginDlg::GetErrorMessageFromCode(const CString& ropErrCode)
{
	CString olErrTxt;

	if (ropErrCode == "LOGINERROR INVALID_USER") 
	{ 
		olErrTxt = LoadStg(IDS_INVALID_USERNAME);
	}
	else if (ropErrCode == "LOGINERROR INVALID_APPLICATION") 
	{
		olErrTxt = LoadStg(IDS_INVALID_APPLICATION);
	}
	else if (ropErrCode == "LOGINERROR INVALID_PASSWORD")  
	{
		olErrTxt = LoadStg(IDS_INVALID_PASSWORD);
	}
	else if(ropErrCode == "LOGINERROR EXPIRED_USER")
	{
		olErrTxt = LoadStg(IDS_EXPIRED_USERNAME);
	}
	else if(ropErrCode == "LOGINERROR EXPIRED_APPLICATION")
	{
		olErrTxt = LoadStg(IDS_EXPIRED_APPLICATION);
	}
	else if(ropErrCode == "LOGINERROR EXPIRED_WORKSTATION") 
	{
		olErrTxt = LoadStg(IDS_EXPIRED_WORKSTATION);
	}
	else if(ropErrCode == "LOGINERROR DISABLED_USER")
	{
		olErrTxt = LoadStg(IDS_DISABLED_USERNAME);
	}
	else if(ropErrCode == "LOGINERROR DISABLED_APPLICATION")
	{
		olErrTxt = LoadStg(IDS_DISABLED_APPLICATION);
	}
	else if(ropErrCode == "LOGINERROR DISABLED_WORKSTATION")
	{
		olErrTxt = LoadStg(IDS_DISABLED_WORKSTATION);
	}
	else if(ropErrCode == "LOGINERROR UNDEFINED_PROFILE")
	{
		olErrTxt = LoadStg(IDS_UNDEFINED_PROFILE);
	}
	else if(ropErrCode == "LOGINERROR MUST_CHANGE_PASSWORD_FIRST")
	{
		olErrTxt = LoadStg(IDS_CHANGE_PASSWORD);
	}
	else 
	{
		olErrTxt.Format(LoadStg(IDS_DB_ERROR),ropErrCode);
	}
	
	return olErrTxt;
}

HBRUSH CLoginDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{

	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	
	// TODO: Change any attributes of the DC here
	
	// TODO: Return a different brush if the default is not desired
	return hbr;
}

int CLoginDlg::GetDaysBeforeDeactivation()
{
	int ilRet = -1;
	bool blRc = true;

	ConnectToCeda();

	// Read from ceda
	_DUfisCom olUfisCom;
	olUfisCom.AttachDispatch(pomLoginCtrl->GetUfisComCtrl(),TRUE);

	olUfisCom.SetUserName(pomLoginCtrl->omUserName);

	CString olData;
	blRc = olUfisCom.CallServer("RT","PARTAB","PAID,VALU",olData,"WHERE APPL='SEC'","360");

	CString olKey;
	CString olValue;

	if(blRc == 0)
	{
		if (olUfisCom.GetBufferCount() > 0 )
		{
            int llRecords = olUfisCom.GetBufferCount();
            for (int i = 0; i < llRecords; i++)
			{
				CString olTmpStr = olUfisCom.GetBufferLine(i);
				CStringArray olItemList;
				::ExtractItemList(olTmpStr,&olItemList,',');
				if (olItemList.GetSize() >= 2)
				{
					CString olKey = olItemList[0];
					olKey.TrimRight();
					olKey.TrimLeft();

					CString olValue = olItemList[1];
					olValue.TrimRight();
					olValue.TrimLeft();

					if(olKey == CString("ID_USER_DEACT"))
					{
						ilRet = atoi(olValue.GetBuffer(0));
						break;
					}

				}
			}
		}
	}
	return ilRet;
}

CString CLoginDlg::GetTableExtensions(CString ropExtensions)
{
	char pclKeyWord[100];
	long llSize;
	char *pclTotalDataBuffer = NULL;
	CString omTabext;
	char *pclDataBegin;
	char pclResult[25000];
	strcpy(pclKeyWord,"<SCOLST>"); 

	CMapStringToString *polExtensionMap = pomLoginCtrl->GetTableExtensionsMap();
	polExtensionMap->RemoveAll();

	pclDataBegin = GetKeyItem(pclResult, &llSize, ropExtensions.GetBuffer(0), pclKeyWord, "</SCOLST>", true); 
	if (pclDataBegin != NULL) 
	{ 
		omTabext = CString(pclResult);

		CStringArray olItemList;
		ExtractItemList(omTabext,&olItemList,'\n');

		for (int i = 0; i < olItemList.GetSize(); i++)
		{
			CString olTemp = olItemList[i];
			CStringArray olItemListTmp;
			ExtractItemList(olTemp,&olItemListTmp,',');
			
			if(olItemListTmp.GetSize()>0)
			{
				CString olStrKey = olItemListTmp[0];//Table extension
				olStrKey.TrimLeft();
				olStrKey.TrimRight();

				CString olStrItem = olItemListTmp[1];//Table extension description
				olStrItem.TrimLeft();
				olStrItem.TrimRight();
			
				CString olValue;
				if (!polExtensionMap->Lookup(olStrKey,olValue))
				{
					polExtensionMap->SetAt(olStrKey,olStrItem);
				}
			}	
		}
	}	
	return omTabext;
}

char* CLoginDlg::GetKeyItem(char *pcpResultBuff, long *plpResultSize, 
				 char *pcpTextBuff, char *pcpKeyWord, 
				 char *pcpItemEnd, bool bpCopyData) 
{ 
	long llDataSize = 0L; 
	char *pclDataBegin = NULL; 
	char *pclDataEnd = NULL; 
	pclDataBegin = strstr(pcpTextBuff, pcpKeyWord); 
	/* Search the keyword */ 
	if (pclDataBegin != NULL) 
	{ 
		/* Did we find it? Yes. */ 
		pclDataBegin += strlen(pcpKeyWord); 
		/* Skip behind the keyword */ 
		pclDataEnd = strstr(pclDataBegin, pcpItemEnd); 
		/* Search end of data */ 
		if (pclDataEnd == NULL) 
		{ 
			/* End not found? */ 
			pclDataEnd = pclDataBegin + strlen(pclDataBegin); 
			/* Take the whole string */ 
		} /* end if */ 
		llDataSize = pclDataEnd - pclDataBegin; 
		/* Now calculate the length */ 
		if (bpCopyData == true) 
		{ 
			/* Shall we copy? */ 
			strncpy(pcpResultBuff, pclDataBegin, llDataSize); 
			/* Yes, strip out the data */ 
		} /* end if */ 
	} /* end if */ 
	if (bpCopyData == true) 
	{ 
		/* Allowed to set EOS? */ 
		pcpResultBuff[llDataSize] = 0x00; 
		/* Yes, terminate string */ 
	} /* end if */ 
	*plpResultSize = llDataSize; 
	/* Pass the length back */ 
	return pclDataBegin; 
	/* Return the data's begin */ 
} /* end GetKeyItem */ 



void CLoginDlg::OnSelchangeComboScene() 
{
	CMapStringToString *polExtensionMap = pomLoginCtrl->GetTableExtensionsMap();

	CString olTabExt("");
	int ilCurSel = m_CB_Scene.GetCurSel();
	m_CB_Scene.GetLBText(ilCurSel, olTabExt);


	CString strResult;
	if(polExtensionMap!= NULL)
	{
		polExtensionMap->Lookup(olTabExt,strResult);
	}

	m_CE_Scene.SetWindowText(strResult);
	
}
