#if !defined(AFX_INFODLG_H__66D9EA15_DF45_4529_A541_746EB2DAF71E__INCLUDED_)
#define AFX_INFODLG_H__66D9EA15_DF45_4529_A541_746EB2DAF71E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// InfoDlg.h : header file
//
#include "AatStatic.h"

/////////////////////////////////////////////////////////////////////////////
// CInfoDlg dialog
class CAatLoginCtrl;

class CInfoDlg : public CDialog
{
// Construction
public:
	CInfoDlg(CWnd* pParent = NULL);   // standard constructor
	CInfoDlg(CWnd* pParent,CAatLoginCtrl *popLoginCtrl);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CInfoDlg)
	enum { IDD = IDD_ABOUTBOX };
		// NOTE: the ClassWizard will add data members here
	AatStatic	m_LblUfisVersion;
	AatStatic	m_LblApplication;
	AatStatic m_LblCopyright;
	AatStatic m_LblAat;
	AatStatic m_LblLabel1;
	AatStatic m_LblLabel2;
	AatStatic m_LblLabel3;
	AatStatic m_LblConnection;
	AatStatic m_LblUser;
	AatStatic m_LblNumberLogin;
	CButton	m_BtnOk;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CInfoDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CInfoDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	CAatLoginCtrl	*pomLoginCtrl;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INFODLG_H__66D9EA15_DF45_4529_A541_746EB2DAF71E__INCLUDED_)
