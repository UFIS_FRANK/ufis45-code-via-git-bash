// _Standard_Wrapper.h : main header file for the _STANDARD_WRAPPER application
//

#if !defined(AFX__STANDARD_WRAPPER_H__E0978B0A_943F_4D57_9669_2D27090EDF7D__INCLUDED_)
#define AFX__STANDARD_WRAPPER_H__E0978B0A_943F_4D57_9669_2D27090EDF7D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// C_Standard_WrapperApp:
// See _Standard_Wrapper.cpp for the implementation of this class
//

class C_Standard_WrapperApp : public CWinApp
{
public:
	C_Standard_WrapperApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(C_Standard_WrapperApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(C_Standard_WrapperApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX__STANDARD_WRAPPER_H__E0978B0A_943F_4D57_9669_2D27090EDF7D__INCLUDED_)
