#if !defined(AFX__AATLOGINCONTROL_H__C35B9C7F_DB8F_11D6_8093_0001022205E4__INCLUDED_)
#define AFX__AATLOGINCONTROL_H__C35B9C7F_DB8F_11D6_8093_0001022205E4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Machine generated IDispatch wrapper class(es) created by Microsoft Visual C++

// NOTE: Do not modify the contents of this file.  If this class is regenerated by
//  Microsoft Visual C++, your modifications will be overwritten.

/////////////////////////////////////////////////////////////////////////////
// C_AATLoginControl wrapper class

class C_AATLoginControl : public CWnd
{
protected:
	DECLARE_DYNCREATE(C_AATLoginControl)
public:
	CLSID const& GetClsid()
	{
		static CLSID const clsid
			= { 0x1d09295, 0x4c8c, 0x11d6, { 0x9d, 0xda, 0x0, 0x1, 0x2, 0x4, 0xaa, 0x7e } };
		return clsid;
	}
	virtual BOOL Create(LPCTSTR lpszClassName,
		LPCTSTR lpszWindowName, DWORD dwStyle,
		const RECT& rect,
		CWnd* pParentWnd, UINT nID,
		CCreateContext* pContext = NULL)
	{ return CreateControl(GetClsid(), lpszWindowName, dwStyle, rect, pParentWnd, nID); }

    BOOL Create(LPCTSTR lpszWindowName, DWORD dwStyle,
		const RECT& rect, CWnd* pParentWnd, UINT nID,
		CFile* pPersist = NULL, BOOL bStorage = FALSE,
		BSTR bstrLicKey = NULL)
	{ return CreateControl(GetClsid(), lpszWindowName, dwStyle, rect, pParentWnd, nID,
		pPersist, bStorage, bstrLicKey); }

// Attributes
public:

// Operations
public:
	CString ShowLoginDialog();
	CString DoLoginSilentMode(LPCTSTR strUserName, LPCTSTR strUserPassword);
	CString GetRegisterApplicationString();
	void SetRegisterApplicationString(LPCTSTR lpszNewValue);
	CString GetApplicationName();
	void SetApplicationName(LPCTSTR lpszNewValue);
	short GetLoginAttempts();
	void SetLoginAttempts(short nNewValue);
	CString GetVersionString();
	void SetVersionString(LPCTSTR lpszNewValue);
	CString GetPrivileges(BSTR* strItem);
	CString GetUserName_();
	CString GetUserPassword();
	BOOL GetInfoButtonVisible();
	void SetInfoButtonVisible(BOOL bNewValue);
	CString GetInfoUfisVersion();
	void SetInfoUfisVersion(LPCTSTR lpszNewValue);
	CString GetInfoCopyright();
	void SetInfoCopyright(LPCTSTR lpszNewValue);
	CString GetInfoAAT();
	void SetInfoAAT(LPCTSTR lpszNewValue);
	CString GetInfoCaption();
	void SetInfoCaption(LPCTSTR lpszNewValue);
	LPDISPATCH GetUfisComCtrl();
	void SetRefUfisComCtrl(LPDISPATCH* newValue);
	CString GetInfoAppVersion();
	void SetInfoAppVersion(LPCTSTR lpszNewValue);
	BOOL GetUserNameLCase();
	void SetUserNameLCase(BOOL bNewValue);
	BOOL WriteErrToLog(BSTR* sFileName, BSTR* sFunction, LPDISPATCH* ErrObj);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX__AATLOGINCONTROL_H__C35B9C7F_DB8F_11D6_8093_0001022205E4__INCLUDED_)
