// BcPrxy.cpp : Implementation of CBcPrxy
#include <stdafx.h>
#include <BcProxy.h>
#include <BcPrxy.h>
#include <CCSGlobl.h>
#include <BcProxyDlg.h>
#include <atlbase.h>

/////////////////////////////////////////////////////////////////////////////
// CBcPrxy
CBcPrxy::CBcPrxy()
{
	bmSpool = false;

	CString strCmd;
	strCmd = CString ("@;ALL;@");

	PROXY_INSTANCES* prlProxyInstance = new PROXY_INSTANCES;
	prlProxyInstance->pProxy = this;
	prlProxyInstance->mapTabCmd.SetAt(strCmd, NULL);
	ogConnections.Add(prlProxyInstance);
}

CBcPrxy::~CBcPrxy()
{
	olBcArray.DeleteAll();

	for (int i = 0; i < ogClientFilterArray.GetSize(); i++)
	{
		if (ogClientFilterArray[i].pClient == this)
		{
			ogClientFilterArray.DeleteAt(i);
			break;
		}
	}

	for (i = 0; i < ogConnections.GetSize(); i++)
	{
		if (ogConnections[i].pProxy == this)
		{
			CString olString;
			CString *p = NULL;
			for (POSITION pos = ogConnections[i].mapTabCmd.GetStartPosition(); pos != NULL;)
			{
				ogConnections[i].mapTabCmd.GetNextAssoc(pos,olString,(void *&)p);
				delete p;
			}
			ogConnections[i].mapTabCmd.RemoveAll();
			ogConnections.DeleteAt(i);
		}
	}
}

STDMETHODIMP CBcPrxy::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IBcPrxy
	};
	for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

STDMETHODIMP CBcPrxy::RegisterObject(BSTR table, BSTR command,BOOL bpUseOwnBc,BSTR appl)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString olKeyString = CString(table) + CString(command);


	void *p;

	PROXY_INSTANCES* prlProxyInstance;
	for (int i = 0; i < ogConnections.GetSize(); i++)
	{
		if (ogConnections[i].pProxy == this)
		{
			prlProxyInstance = &ogConnections[i];
			bool blFound = false;
			if(prlProxyInstance->mapTabCmd.Lookup(CString ("@;ALL;@"), p) == TRUE)
			{
				prlProxyInstance->mapTabCmd.RemoveKey(CString ("@;ALL;@"));				
			}

			CString *polKeyValue = new CString(olKeyString);
			if (bpUseOwnBc)
			{
				(*polKeyValue) += "|1";
			}
			else
			{
				(*polKeyValue) += "|0|";
				(*polKeyValue) += appl;
			}

			prlProxyInstance->mapTabCmd.SetAt(olKeyString,polKeyValue);
		}
	}

	return S_OK;
}

STDMETHODIMP CBcPrxy::UnregisterObject(BSTR table, BSTR command)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString olKeyString;

	olKeyString = CString (table) + CString (command);
	PROXY_INSTANCES* prlProxyInstance;
	CString *p=NULL;

	for (int i = 0; i < ogConnections.GetSize(); i++)
	{
		prlProxyInstance = &ogConnections[i];
		if (prlProxyInstance->pProxy == this)
		{
			if(prlProxyInstance->mapTabCmd.Lookup(olKeyString,(void *&) p) == TRUE)
			{
				prlProxyInstance->mapTabCmd.RemoveKey(olKeyString);
				delete p;
			}
			break;
		}
	}

	return S_OK;
}

STDMETHODIMP CBcPrxy::Unadvise(DWORD dwCookie)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	int i;

	for (i = 0; i < ogClientFilterArray.GetSize(); i++)
	{
		if (ogClientFilterArray[i].pClient == this)
		{
			ogClientFilterArray.DeleteAt(i);
			break;
		}
	}

	for (i = ogConnections.GetSize() -1; i > -1; i--)
	{
		if (ogConnections[i].pProxy == this)
		{
			ogConnections.DeleteAt(i);
		}
	}

	IConnectionPointImplMT<CBcPrxy, &DIID__IBcPrxyEvents, CComDynamicUnkArray>::Unadvise(dwCookie);

	return S_OK;
}

STDMETHODIMP CBcPrxy::get_Version(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CComBSTR bstrVersion("4.5.1.3");
	*pVal = bstrVersion.Detach();

	return S_OK;
}

STDMETHODIMP CBcPrxy::SetSpoolOn()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	bmSpool = true;

	return S_OK;
}

STDMETHODIMP CBcPrxy::SetSpoolOff()
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())
	bmSpool = false;
	CComBSTR	strReqId,
				strDest1,
				strDest2,
				strCmd,
				strObject,
				strSeq,
				strTws,
				strTwe,
				strSelection,
				strFields,
				strData,
				strBcNum;

	for(int i = 0; i < olBcArray.GetSize(); i++)
	{
		strReqId	 = olBcArray[i].ReqId;
		strDest1	 = olBcArray[i].Dest1;
		strDest2	 = olBcArray[i].Dest2;
		strCmd		 = olBcArray[i].Cmd;
		strObject	 = olBcArray[i].Object;
		strSeq		 = olBcArray[i].Seq;
		strTws		 = olBcArray[i].Tws;
		strTwe		 = olBcArray[i].Twe;
		strSelection = olBcArray[i].Selection;
		strFields	 = olBcArray[i].Fields;
		strData		 = olBcArray[i].Data;
		strBcNum	 = olBcArray[i].BcNum;

		Fire_OnBcReceive (BSTR(strReqId),
						  BSTR(strDest1),
						  BSTR(strDest2),
						  BSTR(strCmd),
					 	  BSTR(strObject),
						  BSTR(strSeq),
					 	  BSTR(strTws),
						  BSTR(strTwe),
						  BSTR(strSelection),
						  BSTR(strFields),
						  BSTR(strData),
						  BSTR(strBcNum));
	}
	olBcArray.DeleteAll();

	return S_OK;
}

STDMETHODIMP CBcPrxy::GetNextBufferdBC(BSTR *pReqId, BSTR *pDest1, BSTR *pDest2, BSTR *pCmd, BSTR *pObject, BSTR *pSeq, BSTR *pTws, BSTR *pTwe, BSTR *pSelection, BSTR *pFields, BSTR *pData, BSTR *pBcNum)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	if ( olBcArray.GetSize() > 0 )
	{
		CComBSTR bstrReqId		(olBcArray[0].ReqId	);
		CComBSTR bstrDest1		(olBcArray[0].Dest1	);		
		CComBSTR bstrDest2		(olBcArray[0].Dest2	);		
		CComBSTR bstrCmd		(olBcArray[0].Cmd		);   		
		CComBSTR bstrObject		(olBcArray[0].Object	);		
		CComBSTR bstrSeq		(olBcArray[0].Seq		);		
		CComBSTR bstrTws		(olBcArray[0].Tws		);		
		CComBSTR bstrTwe		(olBcArray[0].Twe		);		
		CComBSTR bstrSelection	(olBcArray[0].Selection);
		CComBSTR bstrFields		(olBcArray[0].Fields	);		
		CComBSTR bstrData		(olBcArray[0].Data	);		
		CComBSTR bstrBcNum		(olBcArray[0].BcNum);		

		*pReqId			= bstrReqId.Detach();
		*pDest1			= bstrDest1.Detach();
		*pDest2			= bstrDest2.Detach();
		*pCmd			= bstrCmd.Detach();
		*pObject		= bstrObject.Detach();
		*pSeq			= bstrSeq.Detach();
		*pTws			= bstrTws.Detach();
		*pTwe			= bstrTwe.Detach();
		*pSelection		= bstrSelection.Detach();
		*pFields		= bstrFields.Detach();
		*pData			= bstrData.Detach();
		*pBcNum			= bstrBcNum.Detach();

		olBcArray.DeleteAt(0);
	}

	return S_OK;
}

STDMETHODIMP CBcPrxy::SetFilterRange(BSTR table, BSTR sFromField, BSTR sToField, BSTR sFromValue, BSTR sToValue)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CString strFromField = CString(sFromField);
	CString strToField = CString (sToField);
	CString strFromValue = CString (sFromValue);
	CString strToValue = CString (sToValue);
	CString strTable = CString (table);

	bool blFound = false;

	for (int i = 0; i < ogClientFilterArray.GetSize(); i++)
	{
		if (ogClientFilterArray[i].pClient == this)
		{
			if (strTable == ogClientFilterArray[i].strTable)
			{
				//update
				blFound = true;
				ogClientFilterArray[i].strFromField = strFromField;
				ogClientFilterArray[i].strToField = strToField;
				ogClientFilterArray[i].strFromValue = strFromValue;
				ogClientFilterArray[i].strToValue = strToValue;
			}
		}
	}

	if (blFound == false)
	{
		CLIENT_FILTER* prlClient = new CLIENT_FILTER;
		prlClient->strFromField = strFromField;
		prlClient->strToField = strToField;
		prlClient->strFromValue = strFromValue;
		prlClient->strToValue = strToValue;
		prlClient->pClient = this;
		prlClient->strTable = strTable;
		ogClientFilterArray.Add(prlClient);
	}

	return S_OK;
}

STDMETHODIMP CBcPrxy::get_BuildDate(BSTR *pVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState())

	CComBSTR bstrVersion(CString(__DATE__) + " - " + CString(__TIME__));
	*pVal = bstrVersion.Detach();

	return S_OK;
}
