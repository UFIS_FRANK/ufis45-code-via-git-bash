//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by BcProxy.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_BCPROXY_DIALOG              102
#define IDS_STRING103                   102
#define IDR_BCPROXY                     103
#define IDR_BCPRXY                      104
#define IDR_MAINFRAME                   128
#define IDC_BCLIST                      1000
#define IDC_BUTTON1                     1001
#define IDC_TEST_BUTTON                 1001
#define IDC_BUTTON_TESTFIRE             1001
#define IDC_BUTTON_FIRE                 1002
#define IDC_EDIT_TABLE                  1003
#define IDC_EDIT_COMMAND                1004

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1005
#define _APS_NEXT_SYMED_VALUE           105
#endif
#endif
