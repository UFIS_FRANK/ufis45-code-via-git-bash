// UtilUC.h: interface for the UtilUC class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_UTILUC_H__72FB4988_6901_463D_8E4E_72ABC79BBEDD__INCLUDED_)
#define AFX_UTILUC_H__72FB4988_6901_463D_8E4E_72ABC79BBEDD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class UtilUC  
{
public:
	UtilUC();
	virtual ~UtilUC();
	bool ConvByteArrToHexSt( BYTE* pbpByteArr, short len, char* pspSt );
	bool ConvHexStToByteArr( char* pspSt, BYTE* pbpByteArr, short& byteCnt );
	bool ConvByteToHexSt( BYTE bpByte, char* st );
	bool ConvHexStToByte( char* st, BYTE& bpByte );
	bool ConvHexCharToInt( char cpCh, int& num );
	bool ConvIntToHexChar( int num, char& ch );

	bool ConvByteArrToOctSt( BYTE* pbpByteArr, short len, char* pspSt );
	bool ConvOctStToByteArr( char* pspSt, BYTE* pbpByteArr, short& byteCnt );
	bool ConvByteToOctSt( BYTE bpByte, char* pcpSt );
	bool ConvOctStToByte( char* st, BYTE& bpByte );
	bool ConvOctCharToInt( char cpCh , int& num);
	bool ConvIntToOctChar( int num, char& ch );


};

#endif // !defined(AFX_UTILUC_H__72FB4988_6901_463D_8E4E_72ABC79BBEDD__INCLUDED_)
