// aatedit5.odl : type library source for ActiveX Control project.

// This file will be processed by the Make Type Library (mktyplib) tool to
// produce the type library (aatedit5.tlb) that will become a resource in
// aatedit5.ocx.
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 *	AATEdit5:	ActiveX Wrapper for CCSEdit
 *
 *	This ActiveX control is a wrapper for the existing functionality
 *	of the old CCSEdit MFC Control.
 *	
 *	ABB Airport Technologies GmbH 2001
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *		Author			Date			Comment
 *	------------------------------------------------------------------
 *
 *		cla	AAT/ID		09/02/2001		Initial version
 *		cla AAT/ID		21/02/2001		SetTypeToInt added
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <olectl.h>
#include <idispids.h>

[ uuid(C3865242-FE62-11D4-90BC-00010204AA51), version(1.0),
  helpfile("aatedit5.hlp"),
  helpstring("aatedit5 ActiveX Control module"),
  control ]
library AATEDIT5Lib
{
	importlib(STDOLE_TLB);
	importlib(STDTYPE_TLB);

	//  Primary dispatch interface for CAatedit5Ctrl

	[ uuid(C3865243-FE62-11D4-90BC-00010204AA51),
	  helpstring("Dispatch interface for Aatedit5 Control"), hidden ]
	dispinterface _DAatedit5
	{
		properties:
			// NOTE - ClassWizard will maintain property information here.
			//    Use extreme caution when editing this section.
			//{{AFX_ODL_PROP(CAatedit5Ctrl)
			//}}AFX_ODL_PROP

		methods:
			// NOTE - ClassWizard will maintain method information here.
			//    Use extreme caution when editing this section.
			//{{AFX_ODL_METHOD(CAatedit5Ctrl)
			[id(1)] void SetBkColor(long BackColor);
			[id(2)] void SetTypeToDouble(long PrePoint, long PostPoint, double From, double To);
			[id(3)] void SetTypeToMoney(long PrePoint, long PostPoint, double From, double To);
			[id(4)] void SetTypeToTime(boolean Req, boolean ChangeDay);
			[id(5)] void SetTypeToDate(boolean Req);
			[id(6)] void SetInitText(BSTR IniText, boolean ChangeColor);
			[id(7)] void SetReadOnly(boolean ReadOnly);
			[id(8)] void SetTextConstraint(long MinLength, long MaxLength, boolean EmptyValid);
			[id(9)] void SetFormat(BSTR FormatStr);
			[id(10)] void SetRegularExpression(BSTR Expr);
			[id(11)] boolean CheckRegMatch(BSTR Val);
			[id(12)] void SetRange(long From, long To);
			[id(13)] void SetPrecision(long PrePoint, long PostPoint);
			[id(14)] void SetTextColor(long TextColor);
			[id(15)] void SetTextChangedColor(long TextChangedColor);
			[id(16)] BSTR GetWindowText();
			[id(17)] void SetSecState(BSTR key);
			[id(18)] long GetTextColor();
			[id(19)] void SetTextErrColor(long ErrColor);
			[id(20)] boolean GetStatus();
			[id(21)] void SetStatus(boolean status);
			[id(22)] boolean IsChanged();
			[id(23)] void SetTypeToString(BSTR Format, long MaxLength, long MinLength);
			[id(24)] void SetTypeToInt(long MinVal, long MaxVal);
			[id(25)] void SetTextLimit(long MinLen,long MaxLen,boolean EmptyValid);
			//}}AFX_ODL_METHOD

			[id(DISPID_ABOUTBOX)] void AboutBox();
	};

	//  Event dispatch interface for CAatedit5Ctrl

	[ uuid(C3865244-FE62-11D4-90BC-00010204AA51),
	  helpstring("Event interface for Aatedit5 Control") ]
	dispinterface _DAatedit5Events
	{
		properties:
			//  Event interface has no properties

		methods:
			// NOTE - ClassWizard will maintain event information here.
			//    Use extreme caution when editing this section.
			//{{AFX_ODL_EVENT(CAatedit5Ctrl)
			[id(1)] void Change();
			[id(2)] void LButtonDown();
			[id(3)] void KillFocus();
			//}}AFX_ODL_EVENT
	};

	//  Class information for CAatedit5Ctrl

	[ uuid(C3865245-FE62-11D4-90BC-00010204AA51),
	  helpstring("Aatedit5 Control"), control ]
	coclass Aatedit5
	{
		[default] dispinterface _DAatedit5;
		[default, source] dispinterface _DAatedit5Events;
	};


	//{{AFX_APPEND_ODL}}
	//}}AFX_APPEND_ODL}}
};
