// Aatedit5Ppg.cpp : Implementation of the CAatedit5PropPage property page class.

#include "stdafx.h"
#include "aatedit5.h"
#include "Aatedit5Ppg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CAatedit5PropPage, COlePropertyPage)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CAatedit5PropPage, COlePropertyPage)
	//{{AFX_MSG_MAP(CAatedit5PropPage)
	// NOTE - ClassWizard will add and remove message map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CAatedit5PropPage, "AATEDIT5.Aatedit5PropPage.1",
	0xc3865246, 0xfe62, 0x11d4, 0x90, 0xbc, 0, 0x1, 0x2, 0x4, 0xaa, 0x51)


/////////////////////////////////////////////////////////////////////////////
// CAatedit5PropPage::CAatedit5PropPageFactory::UpdateRegistry -
// Adds or removes system registry entries for CAatedit5PropPage

BOOL CAatedit5PropPage::CAatedit5PropPageFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_AATEDIT5_PPG);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}


/////////////////////////////////////////////////////////////////////////////
// CAatedit5PropPage::CAatedit5PropPage - Constructor

CAatedit5PropPage::CAatedit5PropPage() :
	COlePropertyPage(IDD, IDS_AATEDIT5_PPG_CAPTION)
{
	//{{AFX_DATA_INIT(CAatedit5PropPage)
	// NOTE: ClassWizard will add member initialization here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_INIT
}


/////////////////////////////////////////////////////////////////////////////
// CAatedit5PropPage::DoDataExchange - Moves data between page and properties

void CAatedit5PropPage::DoDataExchange(CDataExchange* pDX)
{
	//{{AFX_DATA_MAP(CAatedit5PropPage)
	// NOTE: ClassWizard will add DDP, DDX, and DDV calls here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_MAP
	DDP_PostProcessing(pDX);
}


/////////////////////////////////////////////////////////////////////////////
// CAatedit5PropPage message handlers
