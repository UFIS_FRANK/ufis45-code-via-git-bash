Attribute VB_Name = "LocalLib"
Option Explicit

Public strHOPO As String
Public strServer As String
Public strShowHiddenData As String
Public llIdxALTTAB As Integer
Public llIdxACTTAB As Integer
Public llIdxGATTAB As Integer
Public llIdxPSTTAB As Integer
Public llIdxNATTAB As Integer
Public llIdxAPTTAB As Integer
Public llIdxBLTTAB As Integer
Public llIdxACRTAB As Integer
Public llIdxCICTAB As Integer
Public llIdxGRNTAB As Integer
Public llIdxGRMTAB As Integer
Public llIdxGHPTAB As Integer

'=========================================================
' Author:   MWO
' Date:     June, 28. 2002
' The purpose of this module is to provide
' common functionality related to the problem
' of this application
'=========================================================

'==============================================
' handles the blank separated list within the
' rules entries
Public Function MakeItemList(list As String) As String
    Dim strRet As String
    Dim cnt As Integer
    Dim strItem As String
    Dim strTmp As String
    Dim i As Integer
    
    cnt = ItemCount(list, " ")
    For i = 0 To cnt
        strItem = GetRealItem(list, i, " ")
        If strItem <> "" Then
            strTmp = Replace(strItem, "!", "")
            strItem = Replace(strTmp, "=", "")
            strTmp = Trim(strItem)
            strRet = strRet + strItem + ","
        End If
    Next i
    If Len(strRet) > 0 Then
        strRet = left(strRet, Len(strRet) - 1)
    End If
    MakeItemList = strRet
End Function
'=======================================================
' Checks a list of nature for existence
Public Function CheckNatureList(strValue As String, Optional i As Integer = -1) As String
    Dim strResult As String
    Dim cnt2 As Integer
    Dim ilItemCnt As Integer
    Dim strLineNos As String
    Dim j As Integer
    Dim lineInfo As String
    
    If i = -1 Then
        lineInfo = ""
    Else
        lineInfo = CStr(i + 1) + ","
    End If
    cnt2 = ItemCount(strValue, ",")
    For j = 0 To cnt2 - 1
        '0     1    2    3     4
        'URNO,CDAT,LSTU,TNAM,TTYP,USEC,USEU,VAFR,ALGA,ALPO
        strLineNos = frmHiddenData.tabData(4).GetLinesByColumnValue(4, GetRealItem(strValue, j, ","), 1)
        ilItemCnt = ItemCount(strLineNos, ",")
        If ilItemCnt > 1 Then
            strResult = strResult + lineInfo + "Nature code not unique: " + GetRealItem(strValue, j, ",") + vbCrLf
        End If
        If ilItemCnt = 0 Then
            strResult = strResult + lineInfo + "Nature code does not exist: " + GetRealItem(strValue, j, ",") + vbCrLf
        End If
    Next j
    
    CheckNatureList = strResult
End Function
'===================================================
' Checks a Related gates for existence
Public Function CheckRelatedGateList(strValue As String, Optional i As Integer = -1) As String
    Dim strResult As String
    Dim cnt2 As Integer
    Dim ilItemCnt As Integer
    Dim strLineNos As String
    Dim j As Integer
    Dim lineInfo As String
    
    If i = -1 Then
        lineInfo = ""
    Else
        lineInfo = CStr(i + 1) + ","
    End If

    cnt2 = ItemCount(strValue, ",")
    For j = 0 To cnt2 - 1
        '0     1    2    3     4
        'URNO,BNAF,BNAT,CDAT,GNAM,LSTU,NAFR,NATO,NOBR,RGA1,RGA2,TELE,USEC,USEU,VAFR,VATO,RESN,RESB,RBAB,BUSG,TERM,GATR"
        strLineNos = frmHiddenData.tabData(2).GetLinesByColumnValue(4, GetRealItem(strValue, j, ","), 1)
        ilItemCnt = ItemCount(strLineNos, ",")
        If ilItemCnt > 1 Then
            strResult = strResult + lineInfo + "Related gates not unique: " + GetRealItem(strValue, j, ",") + vbCrLf
        End If
        If ilItemCnt = 0 Then
            strResult = strResult + lineInfo + "Related gates does not exist: " + GetRealItem(strValue, j, ",") + vbCrLf
        End If
    Next j
    CheckRelatedGateList = strResult
End Function
'===================================================
' Checks a Position for existence
Public Function CheckPositionList(strValue As String, Optional i As Integer = -1) As String
    Dim strResult As String
    Dim cnt2 As Integer
    Dim ilItemCnt As Integer
    Dim strLineNos As String
    Dim j As Integer
    Dim lineInfo As String
    
    If i = -1 Then
        lineInfo = ""
    Else
        lineInfo = CStr(i + 1) + ","
    End If

' 0     1   2     3    4    5    6   7     8    9   10   11
'URNO,ACUS,CDAT,FULS,GPUS,LSTU,NAFR,NATO,PCAS,PNAM,PRFL,PUBK,TAXI,TELE,USEC,USEU,VAFR,VATO,RESN,DGSS,ACGR,POSR
    If strValue <> "" Then
        strLineNos = frmHiddenData.tabData(3).GetLinesByColumnValue(9, strValue, 0)
        ilItemCnt = ItemCount(strLineNos, ",")
        If ilItemCnt > 1 Then
            strResult = strResult + lineInfo + "Position not unique: " + strValue + vbCrLf
        End If
        If ilItemCnt = 0 Then
            strResult = strResult + lineInfo + "Position does not exist: " + strValue + vbCrLf
        End If
    End If
    CheckPositionList = strResult
End Function
'===================================================
' Checks a of Aircrafts for existence
Public Function CheckAircraftList(strValue As String, Optional i As Integer = -1) As String
    Dim strResult As String
    Dim cnt2 As Integer
    Dim ilItemCnt As Integer
    Dim ilItemCnt2 As Integer
    Dim strLineNos As String
    Dim j As Integer
    Dim lineInfo As String
    
    If i = -1 Then
        lineInfo = ""
    Else
        lineInfo = CStr(i + 1) + ","
    End If

    cnt2 = ItemCount(strValue, ",")
    For j = 0 To cnt2 - 1
        '0     1    2    3     4
        'URNO,ACT3,ACT5,ACTI,ACFN,ACWS,ACLE,ACHE,VAFR,VATO
        strLineNos = frmHiddenData.tabData(1).GetLinesByColumnValue(1, GetRealItem(strValue, j, ","), 1)
        ilItemCnt = ItemCount(strLineNos, ",")
        strLineNos = frmHiddenData.tabData(1).GetLinesByColumnValue(2, GetRealItem(strValue, j, ","), 1)
        ilItemCnt2 = ItemCount(strLineNos, ",")
        If ilItemCnt = 0 And ilItemCnt2 = 0 Then
            strResult = strResult + lineInfo + "A/C does not exist: " + GetRealItem(strValue, j, ",") + vbCrLf
        End If
    Next j
    CheckAircraftList = strResult
End Function
'===================================================
' Checks a of Airlines for existence
Public Function CheckAirlinesList(strValue As String, Optional i As Integer = -1) As String
    Dim strResult As String
    Dim cnt2 As Integer
    Dim ilItemCnt As Integer
    Dim ilItemCnt2 As Integer
    Dim strLineNos As String
    Dim j As Integer
    Dim lineInfo As String
    
    If i = -1 Then
        lineInfo = ""
    Else
        lineInfo = CStr(i + 1) + ","
    End If

    cnt2 = ItemCount(strValue, ",")
    For j = 0 To cnt2 - 1
        '0     1    2    3     4    5    6   7
        'URNO,CDAT,USEC,LSTU,PRFL,USEU,ALC2,ALC3,ALFN,CASH,VAFR,VATO
        strLineNos = frmHiddenData.tabData(0).GetLinesByColumnValue(6, GetRealItem(strValue, j, ","), 1)
        ilItemCnt = ItemCount(strLineNos, ",")
        strLineNos = frmHiddenData.tabData(0).GetLinesByColumnValue(7, GetRealItem(strValue, j, ","), 1)
        ilItemCnt2 = ItemCount(strLineNos, ",")
        If ilItemCnt = 0 And ilItemCnt2 = 0 Then
            strResult = strResult + lineInfo + "Airline does not exist: " + GetRealItem(strValue, j, ",") + vbCrLf
        End If
    Next j
    CheckAirlinesList = strResult
End Function
'===================================================
' Checks a list of Belts for existence
Public Function CheckRelatedBeltList(strValue As String, Optional i As Integer = -1) As String
    Dim strResult As String
    Dim cnt2 As Integer
    Dim ilItemCnt As Integer
    Dim ilItemCnt2 As Integer
    Dim strLineNos As String
    Dim j As Integer
    Dim lineInfo As String
    
    If i = -1 Then
        lineInfo = ""
    Else
        lineInfo = CStr(i + 1) + ","
    End If

    ' 0     1   2     3    4    5    6   7     8    9   10   11
    'STAT,HOPO,VATO,VAFR,USEC,URNO,TELE,RESN,NATO,NAFR,CDAT,BNAM,BLTT,USEU,TERM,LSTU,MAXF,DEFD
    If strValue <> "" Then
        strLineNos = frmHiddenData.tabData(6).GetLinesByColumnValue(11, strValue, 0)
        ilItemCnt = ItemCount(strLineNos, ",")
        If ilItemCnt > 1 Then
            strResult = strResult + lineInfo + "Related belt not unique: " + strValue + vbCrLf
        End If
        If ilItemCnt = 0 Then
            strResult = strResult + lineInfo + "Related belt does not exist: " + strValue + vbCrLf
        End If
    End If
    CheckRelatedBeltList = strResult
End Function

'===================================================
' Checks a list of Airports for existence
Public Function CheckAirportList(strValue As String, Optional i As Integer = -1) As String
    Dim strResult As String
    Dim cnt2 As Integer
    Dim ilItemCnt As Integer
    Dim ilItemCnt2 As Integer
    Dim strLineNos As String
    Dim j As Integer
    Dim lineInfo As String
    
    If i = -1 Then
        lineInfo = ""
    Else
        lineInfo = CStr(i + 1) + ","
    End If

    cnt2 = ItemCount(strValue, ",")
    For j = 0 To cnt2 - 1
        '0     1    2    3     4    5   6    7    8    9    10   11   12
        'URNO,APC3,APC4,APFN,LAND,ETOF,TICH,TDI1,TDI2,APSN,CDAT,USEC,LSTU,PRFL,USEU,VAFR,VATO
        strLineNos = frmHiddenData.tabData(5).GetLinesByColumnValue(1, GetRealItem(strValue, j, ","), 1)
        ilItemCnt = ItemCount(strLineNos, ",")
        strLineNos = frmHiddenData.tabData(5).GetLinesByColumnValue(2, GetRealItem(strValue, j, ","), 1)
        ilItemCnt2 = ItemCount(strLineNos, ",")
        If ilItemCnt = 0 And ilItemCnt2 = 0 Then
            strResult = strResult + lineInfo + "Airport does not exist: " + GetRealItem(strValue, j, ",") + vbCrLf
        End If
    Next j
    CheckAirportList = strResult
End Function
'===================================================
' Checks the Neighbourhood between pos and
'left/right combination
Public Function CheckPositionNeighbourhood(strPos As String, strLeft As String, strRight As String, Currline As Integer) As String
    Dim strLine As String
    Dim llLine As Long
    Dim strResult As String
    Dim strNext As String
    Dim strPrev As String
    
    '0    1  2   3       4       5      6             7         8  9   10  11 12 13 14   15
    'URNO,#,Pos,Gates,Wingspan,Length,Height,A/C Parameterlist,Pos,Max,Pos,Max,M,P,Nat.,A/L Parameter
    If strLeft <> "" Then
        strLine = frmMain.tabPositions.GetLinesByColumnValue(2, strLeft, 0)
        If strLine <> "" Then
            llLine = CLng(strLine)
            If strLine <> "" Then
                strNext = frmMain.tabPositions.GetColumnValue(llLine, 8)
                strPrev = frmMain.tabPositions.GetColumnValue(llLine, 10)
                If strPrev <> strPos And strNext <> strPos Then
                    strResult = CStr(Currline + 1) + ",Pos: " + strPos + " wrong next: " + strLeft + vbCrLf
                End If
            End If
        Else
            strResult = CStr(Currline + 1) + ",Pos: " + strPos + " Next does not exist: " + strLeft + vbCrLf
        End If
    End If
    If strRight <> "" Then
        strLine = frmMain.tabPositions.GetLinesByColumnValue(2, strRight, 0)
        If strLine <> "" Then
            llLine = CLng(strLine)
            If strLine <> "" Then
                strNext = frmMain.tabPositions.GetColumnValue(llLine, 8)
                strPrev = frmMain.tabPositions.GetColumnValue(llLine, 10)
                If strPrev <> strPos And strNext <> strPos Then
                    strResult = CStr(Currline + 1) + ",Pos: " + strPos + " wrong next: " + strRight + vbCrLf
                End If
            End If
        Else
            strResult = CStr(Currline + 1) + ",Pos: " + strPos + " Next does not exist: " + strRight + vbCrLf
        End If
    End If
    CheckPositionNeighbourhood = strResult
End Function
Public Function CallRELHDL(strToDoList As String)
    '"REL", table, fieldlist, selection
    'CedaAction("REL","LATE","",olUpdateString.GetBuffer(0));
     frmHiddenData.Ufis.CallServer "REL", "LATE,NOBC,NOACTION,NOLOG", "", strToDoList, "", "240"
End Function

Public Function Synchronize(bpActive As Boolean)
'void CedaGhdData::Synchronise(bool bpActive)
'{
'    return;
'    CString olOrigFieldList = CString(pcmListOfFields);
'    char pclData[1024]="";
'    char pclSelection[1024]="";
'    CTime olCurrentTime;
'    CTime olToTime;
'    olCurrentTime = CTime::GetCurrentTime();
'    olToTime = olCurrentTime + CTimeSpan(0, 0, 5, 0);
'    CString olStrFrom = CTimeToDBString(olCurrentTime,olCurrentTime);
'    CString olStrTo = CTimeToDBString(olToTime,olToTime);
'    Switch (bpActive)
'    {
'    Case True:
'        {
'            // BEGIN SYNCHRONISATION
'            bool blSyncOk = false;
'            while(blSyncOk == false)
'            {
'                sprintf(pclData, "*CMD*,SYN%s,SYNC,%s,N,%s,%s, ",
'                    pcgTableExt, pcmTableName,olStrFrom.GetBuffer(0),olStrTo.GetBuffer(0));
'                strcpy(pclSelection, "LATE");
'                CedaAction("REL",pclSelection,"",pclData);
'                strcpy(pcmListOfFields, olOrigFieldList.GetBuffer(0));
'                if(strstr(pclSelection,"NOT OK") == NULL)
'                {
'                    SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
'                    blSyncOk = true;
'                }
'                Else
'                {
'                    SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
'                    Sleep(2000);
'                    if(CTime::GetCurrentTime() >= olToTime)
'                    {
'                        SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
'                        blSyncOk = true;
'                    }
'                }
'            }
'        }
'        break;
'    Case False:
'    // END SYNCHRONISATION
'        sprintf(pclData, "*CMD*,SYN%s,SYNC,%s, ,%s,%s, ",
'            pcgTableExt,pcmTableName,olStrFrom.GetBuffer(0),olStrTo.GetBuffer(0));
'        CedaAction("REL","LATE","",pclData);
'        strcpy(pcmListOfFields, olOrigFieldList);
'        break;
'    }
'}
End Function
