VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmMain 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "FIPS Rules and Consistency Checker"
   ClientHeight    =   6075
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11250
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   405
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   750
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdTest 
      Caption         =   "&Test"
      Enabled         =   0   'False
      Height          =   435
      Left            =   9240
      TabIndex        =   13
      Top             =   4980
      Visible         =   0   'False
      Width           =   1095
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   12
      Top             =   5820
      Width           =   11250
      _ExtentX        =   19844
      _ExtentY        =   450
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   17224
         EndProperty
      EndProperty
   End
   Begin VB.Timer Timer1 
      Interval        =   10
      Left            =   10500
      Top             =   60
   End
   Begin VB.Frame fraTab 
      Height          =   4335
      Index           =   1
      Left            =   3300
      TabIndex        =   5
      Top             =   420
      Visible         =   0   'False
      Width           =   3885
      Begin VB.CommandButton btnSetup 
         Caption         =   "&ACR Setup"
         Height          =   375
         Left            =   1200
         TabIndex        =   40
         Top             =   3840
         Width           =   1035
      End
      Begin VB.CheckBox cbCIC 
         Caption         =   "Check Counter"
         Height          =   195
         Left            =   1740
         TabIndex        =   36
         Top             =   3420
         Value           =   1  'Checked
         Width           =   1935
      End
      Begin VB.CheckBox cbBLT 
         Caption         =   "Check Baggage Belts"
         Height          =   195
         Left            =   1740
         TabIndex        =   35
         Top             =   2940
         Value           =   1  'Checked
         Width           =   1995
      End
      Begin VB.CheckBox cbAPT 
         Caption         =   "Check Airports"
         Height          =   255
         Left            =   1740
         TabIndex        =   34
         Top             =   2460
         Value           =   1  'Checked
         Width           =   1635
      End
      Begin VB.CheckBox cbACR 
         Caption         =   "Check Registrations"
         Height          =   195
         Left            =   1740
         TabIndex        =   33
         Top             =   1980
         Value           =   1  'Checked
         Width           =   1755
      End
      Begin VB.CheckBox cbACT 
         Caption         =   "Check Aircrafts"
         Height          =   195
         Left            =   1740
         TabIndex        =   32
         Top             =   1500
         Value           =   1  'Checked
         Width           =   1395
      End
      Begin VB.CheckBox cbALT 
         Caption         =   "Check Airlines"
         Height          =   195
         Left            =   1740
         TabIndex        =   31
         Top             =   1020
         Value           =   1  'Checked
         Width           =   1335
      End
      Begin VB.CheckBox cbNAT 
         Caption         =   "Check Nature"
         Height          =   195
         Left            =   1740
         TabIndex        =   30
         Top             =   540
         Value           =   1  'Checked
         Width           =   1335
      End
      Begin VB.CommandButton btnCheck 
         Caption         =   "&Check"
         Height          =   375
         Left            =   120
         TabIndex        =   29
         Top             =   3840
         Width           =   1035
      End
      Begin TABLib.TAB tabCIC 
         Height          =   315
         Left            =   120
         TabIndex        =   27
         Top             =   3360
         Width           =   1575
         _Version        =   65536
         _ExtentX        =   2778
         _ExtentY        =   556
         _StockProps     =   64
      End
      Begin TABLib.TAB tabBLT 
         Height          =   315
         Left            =   120
         TabIndex        =   25
         Top             =   2880
         Width           =   1575
         _Version        =   65536
         _ExtentX        =   2778
         _ExtentY        =   556
         _StockProps     =   64
      End
      Begin TABLib.TAB tabAPT 
         Height          =   315
         Left            =   120
         TabIndex        =   23
         Top             =   2400
         Width           =   1575
         _Version        =   65536
         _ExtentX        =   2778
         _ExtentY        =   556
         _StockProps     =   64
      End
      Begin TABLib.TAB tabACR 
         Height          =   315
         Left            =   120
         TabIndex        =   19
         Top             =   1920
         Width           =   1575
         _Version        =   65536
         _ExtentX        =   2778
         _ExtentY        =   556
         _StockProps     =   64
      End
      Begin TABLib.TAB tabACT 
         Height          =   315
         Left            =   120
         TabIndex        =   18
         ToolTipText     =   "Hallo"
         Top             =   1440
         Width           =   1575
         _Version        =   65536
         _ExtentX        =   2778
         _ExtentY        =   556
         _StockProps     =   64
      End
      Begin TABLib.TAB tabALT 
         Height          =   315
         Left            =   120
         TabIndex        =   17
         Top             =   960
         Width           =   1575
         _Version        =   65536
         _ExtentX        =   2778
         _ExtentY        =   556
         _StockProps     =   64
      End
      Begin TABLib.TAB tabNatures 
         Height          =   315
         Index           =   0
         Left            =   120
         TabIndex        =   11
         Top             =   480
         Width           =   1575
         _Version        =   65536
         _ExtentX        =   2778
         _ExtentY        =   556
         _StockProps     =   64
      End
      Begin VB.Frame grpBoxCheck 
         Caption         =   "To be checked: "
         Height          =   3495
         Left            =   1680
         TabIndex        =   37
         Top             =   300
         Width           =   2175
      End
      Begin VB.CommandButton cmdUpdate1 
         Caption         =   "Update Changes"
         Height          =   375
         Left            =   2280
         TabIndex        =   47
         Top             =   3840
         Width           =   915
      End
      Begin VB.Label lblCIC 
         Caption         =   "Checkin Counter"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   28
         Top             =   3180
         Width           =   1335
      End
      Begin VB.Label lblBLT 
         Caption         =   "Baggage Belts"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   26
         Top             =   2700
         Width           =   1155
      End
      Begin VB.Label lblAPT 
         Caption         =   "Airports"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   24
         Top             =   2220
         Width           =   1275
      End
      Begin VB.Label lblACR 
         Caption         =   "Registrations"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   22
         Top             =   1740
         Width           =   1095
      End
      Begin VB.Label lblACT 
         Caption         =   "Aircrafts"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   21
         Top             =   1260
         Width           =   1035
      End
      Begin VB.Label lblALT 
         Caption         =   "Airlines"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   20
         Top             =   780
         Width           =   1035
      End
      Begin VB.Label lblNatures 
         Caption         =   "Nature Rules"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Index           =   0
         Left            =   120
         TabIndex        =   10
         Top             =   150
         Width           =   2115
      End
   End
   Begin VB.Frame fraTab 
      Height          =   4335
      Index           =   0
      Left            =   120
      TabIndex        =   3
      Top             =   420
      Visible         =   0   'False
      Width           =   3165
      Begin VB.CommandButton cmdCheckGateData 
         Caption         =   "Check &Gate Rules"
         Height          =   375
         Left            =   180
         TabIndex        =   39
         Top             =   3600
         Width           =   1635
      End
      Begin VB.CommandButton cmdCheckData 
         Caption         =   "Check &Position Rules"
         Height          =   375
         Left            =   180
         TabIndex        =   38
         Top             =   3180
         Width           =   1635
      End
      Begin TABLib.TAB tabGates 
         Height          =   975
         Left            =   120
         TabIndex        =   9
         Top             =   1680
         Width           =   1575
         _Version        =   65536
         _ExtentX        =   2778
         _ExtentY        =   1720
         _StockProps     =   64
      End
      Begin TABLib.TAB tabPositions 
         Height          =   855
         Left            =   120
         TabIndex        =   8
         Top             =   480
         Width           =   1575
         _Version        =   65536
         _ExtentX        =   2778
         _ExtentY        =   1508
         _StockProps     =   64
      End
      Begin VB.Label lblGates 
         Caption         =   "Gate Rules"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   120
         TabIndex        =   7
         Top             =   1440
         Width           =   1275
      End
      Begin VB.Label lblPositions 
         BackStyle       =   0  'Transparent
         Caption         =   "Position Rules"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   180
         Index           =   0
         Left            =   120
         TabIndex        =   4
         Top             =   150
         Width           =   1305
      End
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "&Close"
      Height          =   405
      Left            =   2265
      TabIndex        =   1
      Top             =   4965
      Width           =   1530
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "hidden"
      Height          =   405
      Left            =   3960
      TabIndex        =   0
      Top             =   4965
      Visible         =   0   'False
      Width           =   1530
   End
   Begin VB.Frame fraTab 
      Height          =   3075
      Index           =   2
      Left            =   7260
      TabIndex        =   14
      Top             =   540
      Visible         =   0   'False
      Width           =   3585
      Begin VB.CommandButton cmdClearRuleGrp 
         Caption         =   "Clear Rule Ref. Err"
         Enabled         =   0   'False
         Height          =   375
         Left            =   1620
         TabIndex        =   52
         Top             =   2640
         Visible         =   0   'False
         Width           =   795
      End
      Begin TABLib.TAB tabBasicData 
         Height          =   315
         Index           =   3
         Left            =   120
         TabIndex        =   50
         Top             =   1920
         Width           =   1215
         _Version        =   65536
         _ExtentX        =   2143
         _ExtentY        =   556
         _StockProps     =   64
      End
      Begin VB.CommandButton cmdUpdate2 
         Caption         =   "Update Changes"
         Height          =   375
         Left            =   2520
         TabIndex        =   49
         Top             =   2640
         Width           =   795
      End
      Begin VB.CommandButton cmdGroupMarkAll 
         Caption         =   "Clear unref. lines"
         Height          =   375
         Left            =   780
         TabIndex        =   48
         Top             =   2640
         Width           =   795
      End
      Begin TABLib.TAB tabBasicData 
         Height          =   315
         Index           =   2
         Left            =   120
         TabIndex        =   46
         Top             =   1440
         Width           =   1215
         _Version        =   65536
         _ExtentX        =   2143
         _ExtentY        =   556
         _StockProps     =   64
      End
      Begin TABLib.TAB tabBasicData 
         Height          =   315
         Index           =   1
         Left            =   120
         TabIndex        =   42
         Top             =   960
         Width           =   1215
         _Version        =   65536
         _ExtentX        =   2143
         _ExtentY        =   556
         _StockProps     =   64
      End
      Begin TABLib.TAB tabBasicData 
         Height          =   315
         Index           =   0
         Left            =   120
         TabIndex        =   41
         Top             =   480
         Width           =   1215
         _Version        =   65536
         _ExtentX        =   2143
         _ExtentY        =   556
         _StockProps     =   64
      End
      Begin VB.CommandButton btnCheck2 
         Caption         =   "C&heck"
         Height          =   375
         Left            =   60
         TabIndex        =   44
         Top             =   2640
         Width           =   675
      End
      Begin VB.Label lblBasicData 
         Caption         =   "Counter Rules"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   3
         Left            =   120
         TabIndex        =   51
         Top             =   1740
         Width           =   1635
      End
      Begin VB.Label lblBasicData 
         Caption         =   "Group members"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   2
         Left            =   120
         TabIndex        =   45
         Top             =   1260
         Width           =   1395
      End
      Begin VB.Label lblBasicData 
         Caption         =   "Gates"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   1
         Left            =   120
         TabIndex        =   43
         Top             =   780
         Width           =   1155
      End
      Begin VB.Label lblBasicData 
         Caption         =   "Positions"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   15
         Top             =   180
         Width           =   795
      End
   End
   Begin VB.Label lblTab 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Basicdata (2)"
      ForeColor       =   &H80000008&
      Height          =   240
      Index           =   2
      Left            =   2820
      TabIndex        =   16
      Top             =   120
      Width           =   1335
   End
   Begin VB.Label lblTab 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Basicdata (1)"
      ForeColor       =   &H80000008&
      Height          =   240
      Index           =   1
      Left            =   1380
      TabIndex        =   6
      Top             =   90
      Width           =   1455
   End
   Begin VB.Label lblTab 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Gate Mgmnt."
      ForeColor       =   &H80000008&
      Height          =   240
      Index           =   0
      Left            =   60
      TabIndex        =   2
      ToolTipText     =   "Hallo Mattes"
      Top             =   90
      Width           =   1155
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' A Tab construction without the Tab-control
' Works as long as you don't need more than one row of Tabs
'
' H o w   t o   p r o c e e d
' 1. insert a copy of frmOptions into your project
' 2. remove the End keyword in cmdOK & cmdCancel
' 3. from within your project you call frmOptions as follows:
'
'        Load frmOptions
'        frmOptions. ... = ...         ' set current values
'        ...
'        frmOptions.Show 1, Me         ' show as modal form
'        If frmOptions.OK = True Then
'           ... = frmOptions. ...      ' read & process new values
'           ...
'           End If
'        Unload frmOptions
'
' 4. provide the proper amount of tab-controls (delete or add existing ones)
'        lblTab(...)
'        fraTab(...)
'        lblTabTitle(...) - if you want to use titles, that is
'    adjust the TabsCount constant, here beneath
' 5. fill in the lblTab() / lblTabTitle() .captions
' 6. position the cmdOK (cmdCancel accordingly) - the drawing of
'    raised borders + width of this form, will be aligned/adjusted
'    to this position
' 7. adjust the fraTab() size to what you need and place your own
'    option controls inside each frame plus their proper code

Option Explicit

Public OK As Boolean
Const TabsCount = 3
Dim CurrentTab As Integer
Public MainStatusBar As StatusBar
Public Sub SelectTab(ByVal Index As Integer)
   Dim X1 As Long, X2 As Long, y As Long
   fraTab(CurrentTab).Visible = False
   lblTab(CurrentTab).FontBold = False
   CurrentTab = Index
   fraTab(CurrentTab).Visible = True
   lblTab(CurrentTab).FontBold = True

   X1 = lblTab(Index).left + 1   ' draw new Tab selection
   X2 = lblTab(Index).left + lblTab(Index).Width - 2
   y = lblTab(Index).top + lblTab(Index).Height
   Me.Cls
   Me.Line (X1, y)-(X2, y), vbButtonFace
   Me.PSet (X1 - 1, y), vb3DHighlight
   Me.PSet (X2, y), vb3DShadow
   Me.PSet (X2 + 1, y), vb3DDKShadow
   
End Sub

Private Sub StartTab()
    Dim i As Long
    Dim X1 As Long, Y1 As Long
    Dim X2 As Long, Y2 As Long
    
    Me.AutoRedraw = True
    
    lblTab(0).left = 5                  ' position first lblTab in upper left corner
    lblTab(0).top = 10
    lblTab(0).BorderStyle = 0           ' no borders - all borders are only used in design time
    fraTab(0).left = 10                 ' position first fraTab beneath lblTab
    fraTab(0).top = 32
    fraTab(0).BorderStyle = 0
    
    For i = 1 To TabsCount - 1
       lblTab(i).left = lblTab(i - 1).left + lblTab(i - 1).Width
       lblTab(i).top = lblTab(0).top    ' align Tops
       lblTab(i).BorderStyle = 0        ' no borders
       fraTab(i).left = fraTab(0).left  ' all frames in same place
       fraTab(i).top = fraTab(0).top
       fraTab(i).BorderStyle = 0
       fraTab(i).Visible = False
    Next i
    For i = 0 To TabsCount - 1          ' draw raised borders
       X1 = lblTab(i).left              ' for each lblTab
       Y1 = lblTab(i).top - 4
       X2 = lblTab(i).left + lblTab(i).Width - 2
       Y2 = lblTab(i).top + lblTab(i).Height
       Me.Line (X1 + 1, Y1)-(X2 - 1, Y1), vb3DHighlight
       Me.Line (X1, Y1 + 1)-(X1, Y2), vb3DHighlight
       Me.Line (X2, Y1 + 1)-(X2, Y2), vb3DShadow
       Me.Line (X2 + 1, Y1 + 2)-(X2 + 1, Y2), vb3DDKShadow
    Next i
    
    X1 = lblTab(0).left                   ' draw raised borders
    Y1 = lblTab(0).top + lblTab(0).Height ' around the Tab-body
    X2 = cmdOK.left + cmdOK.Width - 2 + 690   ' aligned to cmdOK
    Y2 = cmdOK.top - 8
    Me.Line (X1, Y1)-(X2, Y1), vb3DHighlight
    Me.Line (X1, Y1 + 1)-(X1, Y2 - 1), vb3DHighlight
    Me.Line (X1 + 1, Y2)-(X2, Y2), vb3DShadow
    Me.Line (X1, Y2 + 1)-(X2 + 1, Y2 + 1), vb3DDKShadow
    Me.Line (X2, Y1 + 1)-(X2, Y2 + 1), vb3DShadow
    Me.Line (X2 + 1, Y1)-(X2 + 1, Y2 + 1), vb3DDKShadow
    
    Me.Picture = Me.Image               ' make permanent background
    Me.AutoRedraw = False
    For i = 0 To 2
        fraTab(i).Height = cmdOK.top - 50
        fraTab(i).Width = 1042
    Next i
    
    lblGates.Move 120, 5300
    tabGates.Move 120, 5500, 15300, 4500
    tabPositions.Height = 4500
    tabPositions.Width = 15300
    tabNatures(0).Height = 4000
    tabNatures(0).Width = 3000
    lblALT.Move 3500, lblNatures(0).top, 3000, 200
    tabALT.Move 3500, 470, 3300, 4000
    
    lblACT.Move 7000, lblNatures(0).top, 3000, 200
    tabACT.Move 7000, 470, 3300, 4000
    
    lblACR.Move 10500, lblNatures(0).top, 3000, 200
    tabACR.Move 10500, 470, 3300, 4000
    
    lblAPT.Move 120, 4700, 3300, 200
    tabAPT.Move 120, 5000, 3300, 4000
    
    lblBLT.Move 3500, 4700, 3300, 200
    tabBLT.Move 3500, 5000, 3300, 4000
    
    lblCIC.Move 7000, 4700, 3300, 200
    tabCIC.Move 7000, 5000, 3300, 4000
    '-------------
    'ceck boxes
    cbNAT.Move 11000, 5000, 2000, 200
    cbALT.Move 11000, 5300, 2000, 200
    cbACT.Move 11000, 5600, 2000, 200
    cbACR.Move 11000, 5900, 2000, 200
    cbAPT.Move 11000, 6200, 2000, 200
    cbBLT.Move 11000, 6500, 2000, 200
    cbCIC.Move 11000, 6800, 2000, 200
    grpBoxCheck.Move 10500, 4700, 3500, 4000
    btnCheck.Move 6000, 10000, 1500, 400
    cmdUpdate1.Move 9300, 10000, 1500, 400
    btnCheck2.Move 6000, 10000, 1500, 400
    cmdClearRuleGrp.Move 9300, 10000, 1500, 400
    cmdUpdate2.Move 13000, 10000, 1500, 400
    
    lblBasicData(0).Move 120, 200, 3300, 200
    tabBasicData(0).Move 120, 470, 3300, 4000
    lblBasicData(1).Move 3500, 200, 3300, 200
    tabBasicData(1).Move 3500, 470, 3300, 4000
    lblBasicData(2).Move 7000, 200, 3300, 200
    tabBasicData(2).Move 7000, 470, 8000, 4000
    lblBasicData(3).Move 120, 4700, 3300, 200
    tabBasicData(3).Move 120, 5000, 15000, 4000

End Sub

Private Sub btnCheck_Click()
    Dim strResult As String
    
    Screen.MousePointer = vbHourglass
    InitBasicDataTabs 1
    If cbACT.Value = 1 Then
        CheckACTBasicData
        lblACT.Caption = "Errors: " + CStr(tabACT.GetLineCount) + " of " + CStr(frmHiddenData.tabData(1).GetLineCount)
    End If
    If cbALT.Value = 1 Then CheckALTBasicData
    If cbNAT.Value = 1 Then
        CheckTABBasicDataSimple tabNatures(0), frmHiddenData.tabData(4), 4, "TTYP", "NAT", "0,4,3"  'CheckNATBasicData
        lblNatures(0).Caption = "Errors: " + CStr(tabNatures(0).GetLineCount) + " of " + CStr(frmHiddenData.tabData(4).GetLineCount)
    End If
    If cbAPT.Value = 1 Then CheckAPTBasicData
    If cbBLT.Value = 1 Then
        CheckTABBasicDataSimple tabBLT, frmHiddenData.tabData(6), 11, "BNAM", "BLT", "0,11" 'CheckBLTBasicData
        lblBLT.Caption = "Errors: " + CStr(tabBLT.GetLineCount) + " of " + CStr(frmHiddenData.tabData(6).GetLineCount)
    End If
    If cbCIC.Value = 1 Then
        CheckTABBasicDataSimple tabCIC, frmHiddenData.tabData(8), 1, "CNAM", "CIC", "0,1" 'CheckCICBasicData
        lblCIC.Caption = "Errors: " + CStr(tabCIC.GetLineCount) + " of " + CStr(frmHiddenData.tabData(8).GetLineCount)
    End If
    If cbACR.Value = 1 Then CheckACRBasicData
    StatusBar1.Panels(1).Text = "Ready"
    Screen.MousePointer = vbArrow
    
End Sub

Private Sub btnCheck2_Click()
    Screen.MousePointer = vbHourglass
    InitBasicDataTabs 2
    CheckTABBasicDataSimple tabBasicData(0), _
                            frmHiddenData.tabData(3), _
                            9, "PNAM", "PST", "0,9"  'CheckPSTBasicData
    lblBasicData(0).Caption = "Errors: " + CStr(tabBasicData(0).GetLineCount) + " of " + CStr(frmHiddenData.tabData(3).GetLineCount)
    CheckTABBasicDataSimple tabBasicData(1), _
                            frmHiddenData.tabData(2), _
                            4, "GNAM", "GAT", "0,4"  'CheckGATBasicData
    lblBasicData(1).Caption = "Errors: " + CStr(tabBasicData(1).GetLineCount) + " of " + CStr(frmHiddenData.tabData(2).GetLineCount)
    CheckGRNData
    lblBasicData(2).Caption = "Errors: " + CStr(tabBasicData(2).GetLineCount) + " of " + CStr(frmHiddenData.tabData(10).GetLineCount)
    CheckGHPData
    lblBasicData(3).Caption = "Errors: " + CStr(tabBasicData(3).GetLineCount) + " of " + CStr(frmHiddenData.tabData(llIdxGHPTAB).GetLineCount)
    StatusBar1.Panels(1).Text = "Ready"
    Screen.MousePointer = vbArrow
End Sub

Private Sub btnSetup_Click()
    frmSetup.Show vbModal
End Sub

Private Sub cmdCancel_Click()
   OK = False: Hide
   End
End Sub

'===========================================
' Validates throug all rule related data and
' verfies the existence and plausibility
'===========================================
Private Sub cmdCheckData_Click()
    Dim i As Integer, j As Integer
    Dim cnt As Long, cnt2 As Long
    Dim strValue As String
    Dim strTmp As String
    Dim ilItemCnt As Integer
    Dim strResult As String
    Dim strLeft As String, strRight As String
    Dim s As Integer
    '0    1  2   3       4       5      6             7         8  9   10  11 12 13 14   15
    'URNO,#,Pos,Gates,Wingspan,Length,Height,A/C Parameterlist,Pos,Max,Pos,Max,M,P,Nat.,A/L Parameter
    cnt = tabPositions.GetLineCount
    Screen.MousePointer = vbHourglass

    For i = 0 To cnt - 1
        strValue = tabPositions.GetColumnValue(i, 2)
        strResult = strResult + CheckPositionList(strValue, i)
        '=================
        'Related gates
        strTmp = tabPositions.GetColumnValue(i, 3)
        strValue = MakeItemList(strTmp)
        StatusBar1.Panels(1).Text = "Checking related gates Line: " + CStr(i)
        strResult = strResult + CheckRelatedGateList(strValue, i)
    Next i
    For i = 0 To cnt - 1
        '=================
        'Aircrafts
        StatusBar1.Panels(1).Text = "Checking aircrafts Line: " + CStr(i)
        strTmp = tabPositions.GetColumnValue(i, 7)
        strValue = MakeItemList(strTmp)
        strResult = strResult + CheckAircraftList(strValue, i)
    Next i
    For i = 0 To cnt - 1
        '=================
        'Nature Codes
        StatusBar1.Panels(1).Text = "Checking natures Line: " + CStr(i)
        strTmp = tabPositions.GetColumnValue(i, 14)
        strValue = MakeItemList(strTmp)
        strResult = strResult + CheckNatureList(strValue, i)
    Next i
    For i = 0 To cnt - 1
        '=================
        'Airlines
        StatusBar1.Panels(1).Text = "Checking airlines Line: " + CStr(i)
        strTmp = tabPositions.GetColumnValue(i, 15)
        strValue = MakeItemList(strTmp)
        strResult = strResult + CheckAirlinesList(strValue, i)
    Next i
'    For i = 0 To cnt - 1
'        strValue = tabPositions.GetColumnValue(i, 2)
'        strResult = strResult + CheckPositionList(strValue, i)
'        '=================
'        'Neighbourhood check between positions
'        strValue = tabPositions.GetColumnValue(i, 2)
'        strLeft = tabPositions.GetColumnValue(i, 8)
'        strRight = tabPositions.GetColumnValue(i, 10)
'        StatusBar1.Panels(1).Text = "Checking neighbourhood relations: " + CStr(i)
'        strResult = strResult + CheckPositionNeighbourhood(strValue, strLeft, strRight, i)
'    Next i
    If strResult = "" Then
        strResult = "No irregularities found"
        frmCheckResults.Hide
        MsgBox strResult
    Else
        frmCheckResults.WhichData = "POS"
        frmCheckResults.SetTabData strResult
        frmCheckResults.Show , Me
    End If
    StatusBar1.Panels(1).Text = "Ready"
    Screen.MousePointer = vbArrow

End Sub

Private Sub cmdCheckGateData_Click()
    Dim i As Integer, j As Integer
    Dim cnt As Long, cnt2 As Long
    Dim strValue As String
    Dim strTmp As String
    Dim strLineNos As String
    Dim ilItemCnt As Integer
    Dim ilItemCnt2 As Integer
    Dim strResult As String
    Dim s As Integer
    ' 0   1  2   3  4    5   6  7 8 9 10 11        12          13
    'URNO,#,Gate,T,R.Blt,ID,Max,M,I,O,P,Nat,A/L Parameter,Dest. Parameter
    cnt = tabGates.GetLineCount
    Screen.MousePointer = vbHourglass

    For i = 0 To cnt - 1
        '================
        'Related belt
        StatusBar1.Panels(1).Text = "Checking belts Line: " + CStr(i)
        strValue = tabGates.GetColumnValue(i, 4)
        strResult = strResult + CheckRelatedBeltList(strValue, i)
    Next i
    For i = 0 To cnt - 1
        '=================
        'Airlines
        StatusBar1.Panels(1).Text = "Checking airlines Line: " + CStr(i)
        strTmp = tabGates.GetColumnValue(i, 12)
        strValue = MakeItemList(strTmp)
        strResult = strResult + CheckAirlinesList(strValue, i)
    Next i
    For i = 0 To cnt - 1
        '=================
        'Orig Dest Airports
        StatusBar1.Panels(1).Text = "Checking airports Line: " + CStr(i)
        strTmp = tabGates.GetColumnValue(i, 13)
        strValue = MakeItemList(strTmp)
        strResult = strResult + CheckAirportList(strValue, i)
    Next i
    For i = 0 To cnt - 1
        '=================
        'Nature Codes
        StatusBar1.Panels(1).Text = "Checking natures Line: " + CStr(i)
        strTmp = tabGates.GetColumnValue(i, 11)
        strValue = MakeItemList(strTmp)
        strResult = strResult + CheckNatureList(strValue, i)
    Next i
    If strResult = "" Then
        strResult = "No irregularities found"
        frmCheckResults.Hide
        MsgBox strResult
    Else
        frmCheckResults.WhichData = "GAT"
        frmCheckResults.SetTabData strResult
        frmCheckResults.Show , Me
    End If
    StatusBar1.Panels(1).Text = "Ready"
    Screen.MousePointer = vbArrow


End Sub

Private Sub cmdClearRuleGrp_Click()
    Dim i As Long, j As Long
    Dim strTag As String
    Dim cnt As Long
    Dim cntTag As Integer
    Dim idxTag As Integer
    Dim idxField As Integer
    Dim strOldValue As String
    
    cnt = tabBasicData(3).GetLineCount
    idxTag = GetRealItemNo(tabBasicData(3).HeaderString, "tag")
    For i = 0 To cnt - 1
        strTag = tabBasicData(3).GetColumnValue(i, idxTag)
        For j = 0 To ItemCount(strTag, ";")
            idxField = GetRealItemNo(tabBasicData(3).HeaderString, GetRealItem(strTag, CInt(j), ";"))
            strOldValue = tabBasicData(3).GetColumnValue(i, idxField)
            tabBasicData(3).SetColumnValue i, idxField, ""
            InplaceEditCell tabBasicData(3), i, idxField, "", strOldValue
        Next j
    Next i
    tabBasicData(3).RedrawTab
    For i = 0 To tabBasicData(2).GetLineCount
        'tabBasicData(2).SetLineStatusValue i, 2
        SetLineDeleted tabBasicData(2), vbKeyDelete, i
    Next i
End Sub

Private Sub cmdGroupMarkAll_Click()
    Dim i As Long, j As Long
    Dim strTag As String
    Dim cnt As Long
    Dim cntTag As Integer
    Dim idxTag As Integer
    Dim idxField As Integer
    Dim strOldValue As String
    
    cnt = tabBasicData(3).GetLineCount
    idxTag = GetRealItemNo(tabBasicData(3).HeaderString, "tag")
    For i = 0 To cnt - 1
        strTag = tabBasicData(3).GetColumnValue(i, idxTag)
        For j = 0 To ItemCount(strTag, ";")
            idxField = GetRealItemNo(tabBasicData(3).HeaderString, GetRealItem(strTag, CInt(j), ";"))
            strOldValue = tabBasicData(3).GetColumnValue(i, idxField)
            tabBasicData(3).SetColumnValue i, idxField, ""
            InplaceEditCell tabBasicData(3), i, idxField, "", strOldValue
        Next j
    Next i
    tabBasicData(3).RedrawTab
    For i = 0 To tabBasicData(2).GetLineCount
        'tabBasicData(2).SetLineStatusValue i, 2
        SetLineDeleted tabBasicData(2), vbKeyDelete, i
    Next i
End Sub

Private Sub cmdOk_Click()
   OK = True: Hide
   End
End Sub

Private Sub cmdTest_Click()
'    tabPositions.ResetContent
'    tabPositions.HeaderLengthString = "0"
'    tabGates.ResetContent
'    tabGates.HeaderLengthString = "0"
'    tabPositions.RedrawTab
'    tabGates.RedrawTab
'    PreparePositionData
'    PrepareGateData
'    StatusBar1.Panels(1).Text = "Ready"

End Sub


Private Sub cmdUpdate1_Click()
    Screen.MousePointer = vbHourglass
    
    GeneralUpdatePerRELHDL frmHiddenData.tabData(llIdxACRTAB), _
                           tabACR, "ACR", "URNO,REGN,ACT3", "URNO,REGN,ACT3", _
                           frmHiddenData.lblRecCount(llIdxACRTAB)
                           
    GeneralUpdatePerRELHDL frmHiddenData.tabData(llIdxACTTAB), _
                           tabACT, "ACT", "URNO,ACT3,ACT5,ACWS,ACHE,ACLE", "URNO,ACT3,ACT5,ACWS,ACHE,ACLE", _
                           frmHiddenData.lblRecCount(llIdxACTTAB)
    
    GeneralUpdatePerRELHDL frmHiddenData.tabData(llIdxALTTAB), _
                           tabALT, "ALT", "URNO,ALC2,ALC3", "URNO,ALC2,ALC3", _
                           frmHiddenData.lblRecCount(llIdxALTTAB)
    
    GeneralUpdatePerRELHDL frmHiddenData.tabData(llIdxNATTAB), _
                           tabNatures(0), "NAT", "URNO,TTYP,TNAM", "URNO,TTYP,TNAM", _
                           frmHiddenData.lblRecCount(llIdxNATTAB)
    
    GeneralUpdatePerRELHDL frmHiddenData.tabData(llIdxAPTTAB), _
                           tabAPT, "APT", "URNO,APC3,APC4,APFN", "URNO,APC3,APC4,APFN", _
                           frmHiddenData.lblRecCount(llIdxAPTTAB)
    
    GeneralUpdatePerRELHDL frmHiddenData.tabData(llIdxBLTTAB), _
                           tabBLT, "BLT", "URNO,BNAM", "URNO,BNAM", _
                           frmHiddenData.lblRecCount(llIdxBLTTAB)
                           
    GeneralUpdatePerRELHDL frmHiddenData.tabData(llIdxCICTAB), _
                           tabCIC, "CIC", "URNO,CNAM", "URNO,CNAM", _
                           frmHiddenData.lblRecCount(llIdxCICTAB)
    Screen.MousePointer = vbArrow
End Sub
Public Sub PerformSave(strTable As String, strFields As String, strUpdates As String, strDeletes As String, _
                       ipNoUpdates As Integer, ipNoDeletes As Integer)
    If ipNoUpdates > 0 Then
         CallRELHDL strUpdates
    End If
    If ipNoDeletes > 0 Then
        CallRELHDL strDeletes
    End If
End Sub

Private Sub cmdUpdate2_Click()
    Screen.MousePointer = vbHourglass
    
    GeneralUpdatePerRELHDL frmHiddenData.tabData(llIdxPSTTAB), _
                           tabBasicData(0), "PST", "URNO,PNAM", "URNO,PNAM", _
                           frmHiddenData.lblRecCount(llIdxPSTTAB)
                           
    GeneralUpdatePerRELHDL frmHiddenData.tabData(llIdxGATTAB), _
                           tabBasicData(1), "GAT", "URNO,GNAM", "URNO,GNAM", _
                           frmHiddenData.lblRecCount(llIdxGATTAB)
                           
    GeneralUpdatePerRELHDL frmHiddenData.tabData(llIdxGRMTAB), _
                           tabBasicData(2), "GRM", "URNO", "URNO", _
                           frmHiddenData.lblRecCount(llIdxGRMTAB)
                           
    GeneralUpdatePerRELHDL frmHiddenData.tabData(llIdxGHPTAB), _
                           tabBasicData(3), "GHP", _
                           "URNO,PRNA,PRSN,ACTM,ALMA,ALMD,HTGA,HTGD,PCAA,PCAD,TRRA,TRRD,TTGA,TTGD", _
                           "URNO,PRNA,PRSN,ACTM,ALMA,ALMD,HTGA,HTGD,PCAA,PCAD,TRRA,TRRD,TTGA,TTGD", _
                           frmHiddenData.lblRecCount(llIdxGHPTAB)
    Screen.MousePointer = vbArrow
End Sub

Private Sub Form_Load()
    Dim i As Integer
    
    Me.Width = 16000  '(cmdOK.Left + cmdOK.Width + 10) * 15
    Me.Height = 12800
    Set MainStatusBar = StatusBar1
    cmdOK.top = 770
    cmdCancel.top = 770
    cmdCancel.Move (450)
    
    cmdCheckData.top = 10200
    cmdCheckData.Move (2000)
    cmdCheckGateData.top = 10200
    cmdCheckGateData.Move (3700)
    cmdTest.top = 770
    cmdTest.Move (820)
    btnSetup.Move 7700, 10000, 1500, 400
    cmdGroupMarkAll.Move 7700, 10000, 1500, 400
    StartTab
    tabPositions.ResetContent
    tabPositions.HeaderLengthString = "0"
    tabGates.ResetContent
    tabGates.HeaderLengthString = "0"
    tabPositions.RedrawTab
    tabGates.RedrawTab
    
    '-----------
    'Basic data
    tabACT.HeaderLengthString = "0"
    tabACT.HeaderLengthString = ","
    tabALT.HeaderLengthString = "0"
    tabALT.HeaderLengthString = ","
    tabNatures(0).HeaderLengthString = "0"
    tabNatures(0).HeaderLengthString = ","
    tabACR.HeaderString = ","
    tabACR.HeaderLengthString = "0"
    tabAPT.HeaderString = ","
    tabAPT.HeaderLengthString = "0"
    tabBLT.HeaderString = ","
    tabBLT.HeaderLengthString = "0"
    tabCIC.HeaderString = ","
    tabCIC.HeaderLengthString = "0"
    
    tabACT.InplaceEditSendKeyEvents = False
    tabALT.InplaceEditSendKeyEvents = False
    tabNatures(0).InplaceEditSendKeyEvents = False
    tabACR.InplaceEditSendKeyEvents = False
    tabAPT.InplaceEditSendKeyEvents = False
    tabBLT.InplaceEditSendKeyEvents = False
    tabCIC.InplaceEditSendKeyEvents = False
    
    For i = 0 To 3
        tabBasicData(i).ResetContent
        tabBasicData(i).InplaceEditSendKeyEvents = False
        tabBasicData(i).HeaderLengthString = "0"
        tabBasicData(i).HeaderString = ","
        tabBasicData(i).RedrawTab
    Next i
End Sub

Private Sub Form_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
   Dim i As Long
   For i = 0 To TabsCount - 1
      If x > lblTab(i).left And x < lblTab(i).left + lblTab(i).Width Then SelectTab i: Exit For
   Next i
End Sub

Private Sub Form_Paint()
   SelectTab CurrentTab
End Sub

Private Sub Form_Unload(Cancel As Integer)
    End
End Sub

Private Sub lblTab_Click(Index As Integer)
   SelectTab Index
End Sub
'=======================================================
' Generates the data of position TAB
'=======================================================
Public Sub PreparePositionData()
    Dim count As Integer
    Dim LineData As String
    Dim i As Integer
    Dim myTabPst As TABLib.TAB
    Dim myTabGat As TABLib.TAB
    Dim tabFound As Boolean
    Dim linecount As Long
    Dim idx As Integer
    Dim strPnam As String
    
    Set myTabPst = frmHiddenData.tabData(3)
    Set myTabGat = frmHiddenData.tabData(2)
    tabPositions.ResetContent
    tabPositions.HeaderString = "URNO,#,Pos,Gates,Wingspan,Length,Height,A/C Parameterlist,Pos,Max,Pos,Max,M,P,Nat.,A/L Parameter"
    tabPositions.HeaderLengthString = "0,30,50,100,100,100,100,180,50,60,50,60,20,20,180,180"
    tabPositions.AutoSizeByHeader = True
    tabPositions.ShowHorzScroller True
    tabPositions.EnableHeaderSizing True

    linecount = myTabPst.GetLineCount
    For i = 0 To linecount - 1
        LineData = ""
        idx = GetRealItemNo(frmHiddenData.PSTTAB, "URNO")
        LineData = myTabPst.GetColumnValue(i, idx) + ","
        
        
        idx = GetRealItemNo(frmHiddenData.PSTTAB, "PNAM")
        strPnam = myTabPst.GetColumnValue(i, idx)
        LineData = LineData + CStr(i + 1) + "," + strPnam + ","
        If strPnam <> "" Then
            LineData = LineData + GetGatInfo(strPnam) + ","
        End If
        LineData = LineData + ExtractGatLine(myTabPst.GetColumnValue(i, 21))
        tabPositions.InsertTextLine LineData, False
    Next i
    tabPositions.RedrawTab
End Sub

Public Function GetGatInfo(strPnam As String) As String
    Dim myTabGat As TABLib.TAB
    Dim strRet As String
    Dim strRga1 As String
    Dim strRga2 As String
    Dim idx As Integer
    Dim linecount As Integer
    Dim lines As String
    Dim blFound As Boolean
    Dim strTmp As String
    Dim strGnam As String
    Dim i As Integer
    
    blFound = False
    Set myTabGat = frmHiddenData.tabData(2)
    lines = myTabGat.GetLinesByColumnValue(10, strPnam, 0) 'RGA2
    For i = 1 To ItemCount(lines, ",")
        strTmp = GetRealItem(lines, i - 1, ",")
        If strTmp <> "" Then
            strGnam = myTabGat.GetColumnValue(CLng(strTmp), 4) + " " 'GNAM
        End If
    Next i
    lines = myTabGat.GetLinesByColumnValue(9, strPnam, 0) 'RGA1
    For i = 1 To ItemCount(lines, ",")
        strTmp = GetRealItem(lines, i - 1, ",")
        If strTmp <> "" Then
            strGnam = strGnam + myTabGat.GetColumnValue(CLng(strTmp), 4) + " " 'GNAM
        End If
    Next i
    GetGatInfo = strGnam
End Function

Public Function ExtractGatLine(ByVal strPosr As String) As String
    Dim strValues As String
    Dim strTmp As String
    Dim i As Integer
    Dim strRet As String
    
    strRet = ""
    strTmp = Replace(strPosr, Chr(157), ",")
    strValues = Replace(strTmp, ";", ",")
    i = ItemCount(strValues, ",")
    strRet = strRet + GetRealItem(strValues, 0, ",") + "," 'Span
    strRet = strRet + GetRealItem(strValues, 1, ",") + ","  'Len
    strRet = strRet + GetRealItem(strValues, 2, ",") + ","  'Height
    strRet = strRet + GetRealItem(strValues, 3, ",") + ","  'Aircrafts
    strRet = strRet + GetRealItem(strValues, 4, ",") + ","  'Nextpos
    strRet = strRet + GetRealItem(strValues, 5, ",") + ","  'Restricted to
    strRet = strRet + GetRealItem(strValues, 6, ",") + ","  'Nextpos
    strRet = strRet + GetRealItem(strValues, 7, ",") + ","  'Restricted to
    strRet = strRet + GetRealItem(strValues, 8, ",") + ","  'MaxAC
    strRet = strRet + GetRealItem(strValues, 9, ",") + ","  'Prio
    strRet = strRet + GetRealItem(strValues, 10, ",") + ","  'Natures
    strRet = strRet + GetRealItem(strValues, 11, ",") + ","  'Airlines
    
    ExtractGatLine = strRet
End Function
Public Sub PrepareGateData()
    Dim count As Integer
    Dim LineData As String
    Dim i As Integer
    Dim myTabGat As TABLib.TAB
    Dim tabFound As Boolean
    Dim linecount As Long
    Dim idx As Integer
    Dim strRaw As String
    Dim strRawValues As String
    Dim strGatr As String
    Dim lfd As Integer
    
    Set myTabGat = frmHiddenData.tabData(2)
    tabGates.ResetContent
    tabGates.HeaderString = "URNO,#,Gate,T,R.Blt,ID,Max,M,I,O,P,Nat,A/L Parameter,Dest. Parameter"
    tabGates.HeaderLengthString = "0,30,50,30,100,50,80,30,30,30,30,180,180,180"
    tabGates.AutoSizeByHeader = True
    tabGates.ShowHorzScroller True
    tabGates.EnableHeaderSizing True

    linecount = myTabGat.GetLineCount
    For i = 0 To linecount - 1
        LineData = ""
        lfd = 0
'                          0   1    2    3    4    5    6     7    8    9   10   11   12   13   14   15   16   17   18   19   20    21
'Public GATTAB As String 'Urno,Bnaf,Bnat,Cdat,Gnam,Lstu,Nafr,Nato,Nobr,Rga1,Rga2,Tele,Usec,Useu,Vafr,Vato,Resn,Resb,Rbab,Busg,Term,Gatr
        idx = GetRealItemNo(frmHiddenData.GATTAB, "URNO")
        LineData = myTabGat.GetColumnValue(i, idx) + ","
        LineData = LineData + CStr(i + 1) + ","
        LineData = LineData + myTabGat.GetColumnValue(i, 4) + ","  'GNAM
        LineData = LineData + myTabGat.GetColumnValue(i, 20) + ","  'Term
        LineData = LineData + myTabGat.GetColumnValue(i, 18) + ","  'Related Belt
       
        strGatr = myTabGat.GetColumnValue(i, 21)
        strRaw = Replace(strGatr, Chr(157), ",")
        strRawValues = Replace(strRaw, ";", ",")
        LineData = LineData + GetRealItem(strRawValues, lfd, ",") + ","
        lfd = lfd + 1
        LineData = LineData + GetRealItem(strRawValues, lfd, ",") + ","
        lfd = lfd + 1
        LineData = LineData + GetRealItem(strRawValues, lfd, ",") + ","
        lfd = lfd + 1
        LineData = LineData + GetRealItem(strRawValues, lfd, ",") + ","
        lfd = lfd + 1
        LineData = LineData + GetRealItem(strRawValues, lfd, ",") + ","
        lfd = lfd + 1
        LineData = LineData + GetRealItem(strRawValues, lfd, ",") + ","
        lfd = lfd + 1
        LineData = LineData + GetRealItem(strRawValues, lfd, ",") + ","
        lfd = lfd + 1
        LineData = LineData + GetRealItem(strRawValues, lfd, ",") + ","
        lfd = lfd + 1
        LineData = LineData + GetRealItem(strRawValues, lfd, ",") + ","
        
        tabGates.InsertTextLine LineData, False
    Next i
    tabGates.RedrawTab
End Sub

Private Sub tabACR_SendRButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim strOldValue As String
    If ColNo = 2 Then
        frmChoiceList.strFieldList = "ACT3,ACT5"
        frmChoiceList.strHeaderList = "ACT3,ACT5"
        frmChoiceList.strHeaderLenList = "80,80"
        frmChoiceList.imResultIndex = 0
        frmChoiceList.dataTabIndex = 1
        frmChoiceList.imSortIdx = 0
        frmChoiceList.Show vbModal
        If frmChoiceList.strResultValue <> "" Then
            strOldValue = tabACR.GetColumnValue(LineNo, ColNo)
            tabACR.SetColumnValue LineNo, ColNo, frmChoiceList.strResultValue
            InplaceEditCell tabACR, LineNo, ColNo, frmChoiceList.strResultValue, strOldValue
        End If
    End If
End Sub

Private Sub tabACT_HitKeyOnLine(ByVal Key As Integer, ByVal LineNo As Long)
    SetLineDeleted tabACT, Key, LineNo
End Sub
Private Sub tabALT_HitKeyOnLine(ByVal Key As Integer, ByVal LineNo As Long)
    SetLineDeleted tabALT, Key, LineNo
End Sub '<<
Private Sub tabAPT_HitKeyOnLine(ByVal Key As Integer, ByVal LineNo As Long)
    SetLineDeleted tabAPT, Key, LineNo
End Sub

Private Sub tabBasicData_HitKeyOnLine(Index As Integer, ByVal Key As Integer, ByVal LineNo As Long)
    SetLineDeleted tabBasicData(Index), Key, LineNo
End Sub

Private Sub tabBLT_HitKeyOnLine(ByVal Key As Integer, ByVal LineNo As Long)
    SetLineDeleted tabBLT, Key, LineNo
End Sub
Private Sub tabCIC_HitKeyOnLine(ByVal Key As Integer, ByVal LineNo As Long)
    SetLineDeleted tabCIC, Key, LineNo
End Sub

Private Sub tabACR_HitKeyOnLine(ByVal Key As Integer, ByVal LineNo As Long)
    SetLineDeleted tabACR, Key, LineNo
End Sub
Public Sub SetLineDeleted(ByRef myTab As TABLib.TAB, ByVal Key As Integer, ByVal LineNo As Long)
    Dim bkColor As Long
    Dim txtColor As Long
   Dim llLineStatus As Long
    
    If Key = vbKeyDelete Then
        llLineStatus = myTab.GetLineStatusValue(LineNo)
        If (llLineStatus And 2) = 0 Then
            'the line has to be deleted
            llLineStatus = llLineStatus + 2
            myTab.SetLineColor LineNo, vbBlack, 12640511
            'lblStatus.Caption = "Update necessary."
        Else
            'don't delete the line
            llLineStatus = llLineStatus - 2
            If (llLineStatus And 1) = 1 Then      'line is not updated yet
                myTab.SetLineColor LineNo, vbBlack, 12648384
            Else
                myTab.SetLineColor LineNo, vbBlack, vbWhite
            End If
        End If
        myTab.SetLineStatusValue LineNo, llLineStatus
    End If
    myTab.RedrawTab
End Sub

Private Sub tabACT_InplaceEditCell(ByVal LineNo As Long, ByVal ColNo As Long, ByVal NewValue As String, ByVal OldValue As String)
    InplaceEditCell tabACT, LineNo, ColNo, NewValue, OldValue
End Sub
Private Sub tabALT_InplaceEditCell(ByVal LineNo As Long, ByVal ColNo As Long, ByVal NewValue As String, ByVal OldValue As String)
    InplaceEditCell tabALT, LineNo, ColNo, NewValue, OldValue
End Sub

Private Sub tabNatures_HitKeyOnLine(Index As Integer, ByVal Key As Integer, ByVal LineNo As Long)
    SetLineDeleted tabNatures(Index), Key, LineNo
End Sub

Private Sub tabNatures_InplaceEditCell(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long, ByVal NewValue As String, ByVal OldValue As String)
    InplaceEditCell tabNatures(Index), LineNo, ColNo, NewValue, OldValue
End Sub
Private Sub tabAPT_InplaceEditCell(ByVal LineNo As Long, ByVal ColNo As Long, ByVal NewValue As String, ByVal OldValue As String)
    InplaceEditCell tabAPT, LineNo, ColNo, NewValue, OldValue
End Sub
Private Sub tabBLT_InplaceEditCell(ByVal LineNo As Long, ByVal ColNo As Long, ByVal NewValue As String, ByVal OldValue As String)
    InplaceEditCell tabBLT, LineNo, ColNo, NewValue, OldValue
End Sub
Private Sub tabCIC_InplaceEditCell(ByVal LineNo As Long, ByVal ColNo As Long, ByVal NewValue As String, ByVal OldValue As String)
    InplaceEditCell tabCIC, LineNo, ColNo, NewValue, OldValue
End Sub

Private Sub tabACR_InplaceEditCell(ByVal LineNo As Long, ByVal ColNo As Long, ByVal NewValue As String, ByVal OldValue As String)
    InplaceEditCell tabACR, LineNo, ColNo, NewValue, OldValue
End Sub
Public Sub InplaceEditCell(ByRef myTab As TABLib.TAB, ByVal LineNo As Long, ByVal ColNo As Long, ByVal NewValue As String, ByVal OldValue As String)
    If NewValue <> OldValue Then
        Dim llLineStatus As Long
        llLineStatus = myTab.GetLineStatusValue(LineNo)
        If (llLineStatus And 2) = 0 Then                'line is not deleted yet
            If (llLineStatus And 4) = 0 Then            'it is not an insert-line
                If (llLineStatus And 1) = 0 Then        'line is not updated yet
                    llLineStatus = llLineStatus + 1
                    myTab.SetLineStatusValue LineNo, llLineStatus
                    'lblStatus.Caption = "Update necessary."
                    myTab.SetLineColor LineNo, vbBlack, 12648384
                    myTab.RedrawTab
                End If
            End If
        Else
            myTab.SetColumnValue LineNo, ColNo, OldValue
        End If
    End If
End Sub


Private Sub tabACR_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo = -1 Then
        tabACR.Sort ColNo, True, False
        tabACR.RedrawTab
    End If

End Sub

Private Sub tabACR_SendMouseMove(ByVal LineNo As Long, ByVal ColNo As Long, ByVal Flags As String)
    Dim strTip As String
    tabACR.ToolTipText = ""
    If LineNo >= 0 Then
        strTip = tabACR.GetLineTag(LineNo)
    End If
    tabACR.ToolTipText = strTip
End Sub

Private Sub tabACT_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo = -1 Then
        tabACT.Sort ColNo, True, False
        tabACT.RedrawTab
    End If

End Sub

Private Sub tabACT_SendMouseMove(ByVal LineNo As Long, ByVal ColNo As Long, ByVal Flags As String)
    Dim strTip As String
    tabACT.ToolTipText = ""
    If LineNo >= 0 Then
       strTip = tabACT.GetLineTag(LineNo)
    End If
    tabACT.ToolTipText = strTip
End Sub

Private Sub tabALT_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo = -1 Then
        tabALT.Sort ColNo, True, False
        tabALT.RedrawTab
    End If
End Sub

Private Sub tabALT_SendMouseMove(ByVal LineNo As Long, ByVal ColNo As Long, ByVal Flags As String)
    Dim strTip As String
    tabALT.ToolTipText = ""
    If LineNo >= 0 Then
        strTip = tabALT.GetLineTag(LineNo)
    End If
    tabALT.ToolTipText = strTip
End Sub

Private Sub tabAPT_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo = -1 Then
        tabAPT.Sort ColNo, True, False
        tabAPT.RedrawTab
    End If

End Sub

Private Sub tabAPT_SendMouseMove(ByVal LineNo As Long, ByVal ColNo As Long, ByVal Flags As String)
    Dim strTip As String
    tabAPT.ToolTipText = ""
    If LineNo >= 0 Then
        strTip = tabAPT.GetLineTag(LineNo)
    End If
    tabAPT.ToolTipText = strTip
End Sub

Private Sub tabBasicdata_SendLButtonClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo = -1 Then
        tabBasicData(Index).Sort ColNo, True, False
        tabBasicData(Index).RedrawTab
    End If

End Sub

Private Sub tabBasicdata_SendMouseMove(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long, ByVal Flags As String)
    Dim strTip As String
   tabBasicData(Index).ToolTipText = ""
    If LineNo >= 0 Then
        strTip = tabBasicData(Index).GetLineTag(LineNo)
    End If
   tabBasicData(Index).ToolTipText = strTip
End Sub

Private Sub tabBLT_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo = -1 Then
        tabBLT.Sort ColNo, True, False
        tabBLT.RedrawTab
    End If

End Sub

Private Sub tabBLT_SendMouseMove(ByVal LineNo As Long, ByVal ColNo As Long, ByVal Flags As String)
    Dim strTip As String
    tabBLT.ToolTipText = ""
    If LineNo >= 0 Then
        strTip = tabBLT.GetLineTag(LineNo)
    End If
    tabBLT.ToolTipText = strTip
End Sub


Private Sub tabCIC_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo = -1 Then
        tabCIC.Sort ColNo, True, False
        tabCIC.RedrawTab
    End If

End Sub

Private Sub tabCIC_SendMouseMove(ByVal LineNo As Long, ByVal ColNo As Long, ByVal Flags As String)
    Dim strTip As String
    tabCIC.ToolTipText = ""
    If LineNo >= 0 Then
        strTip = tabBLT.GetLineTag(LineNo)
    End If
    tabCIC.ToolTipText = strTip
End Sub

Private Sub tabGates_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim LineValues As String
    Dim Values As String
    Dim strRaw As String
    Dim val As String
    Dim i As Integer
    
    LineValues = tabGates.GetLineValues(LineNo)
    ' 0   1  2   3  4    5   6  7 8 9 10 11        12          13
    'URNO,#,Gate,T,R.Blt,ID,Max,M,I,O,P,Nat,A/L Parameter,Dest. Parameter
    Values = GetRealItem(LineValues, 0, ",") + "," 'Urno
    
    Values = Values + GetRealItem(LineValues, 5, ",") + "," 'FlightId
    Values = Values + GetRealItem(LineValues, 6, ",") + "," 'Max
    Values = Values + GetRealItem(LineValues, 10, ",") + "," 'Priority
    Values = Values + GetRealItem(LineValues, 8, ",") + "," 'Inbound gate
    Values = Values + GetRealItem(LineValues, 9, ",") + "," 'Outbaound gate
    Values = Values + GetRealItem(LineValues, 7, ",") + "," 'Multiple flights
    Values = Values + GetRealItem(LineValues, 12, ",") + "," 'AL Parameter
    Values = Values + GetRealItem(LineValues, 13, ",") + "," 'Orig dest
    Values = Values + GetRealItem(LineValues, 11, ",") + "," 'Nature

    frmGates.Fieldlist = "Urno,Flighid,Max,Prio,Inbound,Outbound,Multipl,Airlines,Airports,Natures"
    frmGates.Fieldlens = "10,1,4,1,1,1,2,150,150,150"
    frmGates.Typelist = "long,string,long,long,string,string,long,string,string,string"
    frmGates.Datalist = Values
    frmGates.Tabindex = LineNo
    frmGates.CaptionPart = GetRealItem(LineValues, 2, ",")
    frmGates.Show vbModal


End Sub

Private Sub tabNatures_SendLButtonClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo = -1 Then
        tabNatures(Index).Sort ColNo, True, False
        tabNatures(Index).RedrawTab
    End If

End Sub

Private Sub tabNatures_SendMouseMove(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long, ByVal Flags As String)
    Dim strTip As String
    tabNatures(Index).ToolTipText = ""
    If LineNo >= 0 Then
        strTip = tabNatures(Index).GetLineTag(LineNo)
    End If
    tabNatures(Index).ToolTipText = strTip
End Sub

Private Sub tabPositions_HitKeyOnLine(ByVal Key As Integer, ByVal LineNo As Long)
'<>
    If Key = vbKeyReturn Then
        tabPositions_SendLButtonDblClick LineNo, 0
    End If
End Sub
Public Sub ShowPositionDilalogForLine(ByVal LineNo As Long, ByVal ColNo As Long)
    tabPositions_SendLButtonDblClick LineNo, ColNo
End Sub
Public Sub ShowGateDilalogForLine(ByVal LineNo As Long, ByVal ColNo As Long)
    tabGates_SendLButtonDblClick LineNo, ColNo
End Sub

Private Sub tabPositions_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim LineValues As String
    Dim Values As String
    Dim strRaw As String
    Dim val As String
    Dim i As Integer
    Dim strHeight As String
    Dim strLen As String
    Dim strSpan As String
    
    LineValues = tabPositions.GetLineValues(LineNo)
    ' 0   1  2    3      4       5      6           7           8   9   10  11 12 13  14    15
    'URNO,#,Pos,Gates,Wingspan,Length,Height,A/C Parameterlist,Pos,Max,Pos,Max, M, P,Nat.,A/L Parameter
    Values = GetRealItem(LineValues, 0, ",") + ","
    For i = 4 To 6
        strRaw = GetRealItem(LineValues, i, ",")
        If strRaw <> "" Or strRaw <> " " Or strRaw <> "-" Then
            If i = 4 Then strSpan = GetRealItem(strRaw, 0, "-") + "," + GetRealItem(strRaw, 1, "-") + ","
            If i = 5 Then strLen = GetRealItem(strRaw, 0, "-") + "," + GetRealItem(strRaw, 1, "-") + ","
            If i = 6 Then strHeight = GetRealItem(strRaw, 0, "-") + "," + GetRealItem(strRaw, 1, "-") + ","
        End If
    Next i
    Values = Values + strSpan + strHeight + strLen
    Values = Values + GetRealItem(LineValues, 8, ",") + "," 'Nextpos1
    Values = Values + GetRealItem(LineValues, 9, ",") + "," 'Max1
    Values = Values + GetRealItem(LineValues, 10, ",") + "," 'Nextpos2
    Values = Values + GetRealItem(LineValues, 11, ",") + "," 'Max2
    Values = Values + GetRealItem(LineValues, 12, ",") + "," 'M
    Values = Values + GetRealItem(LineValues, 13, ",") + "," 'P
    Values = Values + GetRealItem(LineValues, 15, ",") + "," 'AL Parameter
    Values = Values + GetRealItem(LineValues, 7, ",") + "," 'AC Types
    Values = Values + GetRealItem(LineValues, 14, ",") + "," 'Nextpos1

    frmPosition.Fieldlist = "Urno,SpanMin,SpanMax,LenMin,LenMax,HeightMin,HeightMax,Aircrafts,Nextpos1,Restrictedto1,Nextpos2,Restrictedto2,MaxAC,Prio,Natures,Airlines"
    frmPosition.Fieldlens = "10,5,5,5,5,4,4,5,5,5,5,3,1,150,150,150"
    frmPosition.Typelist = "long,double,double,double,double,double,double,string,double,string,double,long,long,string,string,string"
    frmPosition.Datalist = Values
    frmPosition.Tabindex = LineNo
    frmPosition.CaptionPart = GetRealItem(LineValues, 2, ",")
    frmPosition.Show vbModal
End Sub

Private Sub Timer1_Timer()
    Timer1.Enabled = False
    'strHOPO = GetIniEntry("C:\UFIS\SYSTEM\CEDA.INI", "ConsistencyChecker", "GLOBAL", "HOMEAIRPORT", "") '"ATH"
    'strServer = GetIniEntry("C:\UFIS\SYSTEM\CEDA.INI", "ConsistencyChecker", "GLOBAL", "HOSTNAME", "") '"tonga"
    'strShowHiddenData = GetIniEntry("C:\UFIS\SYSTEM\CEDA.INI", "ConsistencyChecker", "GLOBAL", "HIDDENDATA", "HIDE") '"tonga"
    strHOPO = GetIniEntry(DEFAULT_CEDA_INI, "ConsistencyChecker", "GLOBAL", "HOMEAIRPORT", "") '"ATH"
    strServer = GetIniEntry(DEFAULT_CEDA_INI, "ConsistencyChecker", "GLOBAL", "HOSTNAME", "") '"tonga"
    strShowHiddenData = GetIniEntry(DEFAULT_CEDA_INI, "ConsistencyChecker", "GLOBAL", "HIDDENDATA", "HIDE") '"tonga"
    Load frmHiddenData
    If strShowHiddenData = "SHOW" Then
        frmHiddenData.Show
    Else
        frmHiddenData.Hide
    End If
    Screen.MousePointer = vbHourglass

    frmHiddenData.LoadTabData
    PreparePositionData
    PrepareGateData
    Screen.MousePointer = vbArrow
    StatusBar1.Panels(1).Text = "Ready"
End Sub

Public Function CheckACTBasicData() As String
    Dim strResult As String
    Dim cnt As Long
    Dim i As Long
    Dim strLineNos As String
    Dim strValue As String
    Dim strTmp As String
    Dim strACWS As String, strACHE As String, strACLE As String
    Dim dDim As Double
    Dim blLineCf As Boolean
    Dim strInsert As String
    Dim ToolTipText As String
    blLineCf = False
    
    
    blLineCf = False
    cnt = frmHiddenData.tabData(1).GetLineCount
    For i = 0 To cnt - 1
        '==============
        'Check for unique
'Urno,Act3,Act5,Acti,Acfn,Acws,Acle,Ache,Vafr,Vato
        blLineCf = False
        ToolTipText = ""
        StatusBar1.Panels(1).Text = "Checking A/C irregularities: " + CStr(i)
        If i Mod 20 = 0 Then
            DoEvents
            tabACT.OnVScrollTo tabALT.GetLineCount - 1
        End If
        strValue = frmHiddenData.tabData(1).GetColumnValue(i, 1)
        strLineNos = frmHiddenData.tabData(1).GetLinesByColumnValue(1, strValue, 0)
        If strValue = "B14" Then
            dDim = 0
        End If
        If ItemCount(strLineNos, ",") > 1 Then
            strResult = strResult + CStr(i + 1) + "," + strValue + ": Unique violation ACT3: " + vbCrLf
            ToolTipText = ToolTipText + strValue + ": Unique violation ACT3;"
            blLineCf = True
        End If
        strACHE = frmHiddenData.tabData(1).GetColumnValue(i, 7)
        If strACHE = "" Then
            strResult = strResult + CStr(i + 1) + "," + strValue + ": Height is 0: " + vbCrLf
            ToolTipText = ToolTipText + strValue + ": Height is 0; "
            blLineCf = True
        End If
        strACLE = frmHiddenData.tabData(1).GetColumnValue(i, 6)
        If strACLE = "" Then
            strResult = strResult + CStr(i + 1) + "," + strValue + ": Length is 0 " + vbCrLf
            ToolTipText = ToolTipText + strValue + ": Length is 0; "
            blLineCf = True
        End If
        strACWS = frmHiddenData.tabData(1).GetColumnValue(i, 5)
        If strACWS = "" Then
            strResult = strResult + CStr(i + 1) + "," + strValue + ": Wingspan is 0: " + vbCrLf
            ToolTipText = ToolTipText + strValue + ": Winspan is 0; "
            blLineCf = True
        End If
        If blLineCf = True Then
            strInsert = frmHiddenData.tabData(1).GetColumnValue(i, 0) + ","
            strInsert = strInsert + frmHiddenData.tabData(1).GetColumnValue(i, 1) + ","
            strInsert = strInsert + frmHiddenData.tabData(1).GetColumnValue(i, 2) + ","
            strInsert = strInsert + frmHiddenData.tabData(1).GetColumnValue(i, 5) + ","
            strInsert = strInsert + frmHiddenData.tabData(1).GetColumnValue(i, 7) + ","
            strInsert = strInsert + frmHiddenData.tabData(1).GetColumnValue(i, 6)
            tabACT.InsertTextLine strInsert, True
            tabACT.SetLineTag tabACT.GetLineCount - 1, ToolTipText
            If strACHE = "" Then tabACT.SetDecorationObject tabACT.GetLineCount - 1, 4, "CONF"
            If strACLE = "" Then tabACT.SetDecorationObject tabACT.GetLineCount - 1, 5, "CONF"
            If strACWS = "" Then tabACT.SetDecorationObject tabACT.GetLineCount - 1, 3, "CONF"
            If ItemCount(strLineNos, ",") > 1 Then tabACT.SetDecorationObject tabACT.GetLineCount - 1, 1, "CONF"
        End If
    Next i
    CheckACTBasicData = strResult
    tabACT.Sort 1, True, False
    tabACT.OnVScrollTo 0
End Function
Public Function CheckALTBasicData() As String
    Dim strResult As String
    Dim cnt As Long
    Dim i As Long
    Dim strLineNosAlc2 As String, strLineNosAlc3 As String
    Dim strValue As String
    Dim strAlc2 As String, strAlc3 As String
    Dim strTmp As String
    Dim strACWS As String, strACHE As String, strACLE As String
    Dim dDim As Double
    Dim blLineCf As Boolean
    Dim strInsert As String
    Dim ToolTipText As String
    Dim strOldValue As String
    Dim llVal As Long
    
    cnt = frmHiddenData.tabData(0).GetLineCount
    frmHiddenData.tabData(0).Sort 6, True, False 'Sort for ACT3
    '==========================
    ' Check ALC2
    For i = 0 To cnt - 1
        frmHiddenData.tabData(0).SetLineStatusValue i, 0
        frmHiddenData.tabData(0).SetLineTag i, ""
        ToolTipText = ""
        If i Mod 200 = 0 Then
            DoEvents
            StatusBar1.Panels(1).Text = "Checking airlines 2LC: " + CStr(i) + " of " + CStr(cnt - 1)
        End If
        strValue = frmHiddenData.tabData(0).GetColumnValue(i, 6)
        If strValue <> "" Then
            If strValue = strOldValue Then
                strTmp = frmHiddenData.tabData(1).GetLineTag(i)
                strTmp = strTmp + "1D;"  'Duplicate value in col1
                frmHiddenData.tabData(0).SetLineStatusValue i, 1
                frmHiddenData.tabData(0).SetLineTag i, strTmp
                If i > 0 Then
                    frmHiddenData.tabData(0).SetLineStatusValue i - 1, 1
                    frmHiddenData.tabData(0).SetLineTag i - 1, strTmp
                End If
            End If
        Else
'            strTmp = frmHiddenData.tabData(1).GetLineTag(i)
'            strTmp = strTmp + "1E;"  'Duplicate value in col1
'            frmHiddenData.tabData(0).SetLineStatusValue i, 1
'            frmHiddenData.tabData(0).SetLineTag i, strTmp
        End If
        strOldValue = strValue
    Next i
    '==========================
    ' Check ALC3
    strOldValue = ""
    frmHiddenData.tabData(0).Sort 7, True, False 'Sort for ACT3
    For i = 0 To cnt - 1
        ToolTipText = ""
        If i Mod 2000 = 0 Then
            DoEvents
            StatusBar1.Panels(1).Text = "Checking airlines 3LC: " + CStr(i) + " of " + CStr(cnt - 1)
        End If
        strValue = frmHiddenData.tabData(0).GetColumnValue(i, 7)
        If strValue <> "" Then
            If strValue = strOldValue Then
                strTmp = frmHiddenData.tabData(1).GetLineTag(i)
                If strTmp <> "" Then
                    strTmp = strTmp
                End If
                strTmp = strTmp + "2D;"  'Duplicate value in col1
                frmHiddenData.tabData(0).SetLineStatusValue i, 1
                frmHiddenData.tabData(0).SetLineTag i, strTmp
                If i > 0 Then
                    frmHiddenData.tabData(0).SetLineStatusValue i - 1, 1
                    frmHiddenData.tabData(0).SetLineTag i - 1, strTmp
                End If
            End If
        Else
            strTmp = frmHiddenData.tabData(0).GetLineTag(i)
                If strTmp <> "" Then
                    strTmp = strTmp
                End If
            strTmp = strTmp + "2E;"  'Duplicate value in col1
            frmHiddenData.tabData(0).SetLineStatusValue i, 1
            frmHiddenData.tabData(0).SetLineTag i, strTmp
        End If
        strOldValue = strValue
    Next i
    '=============================
    ' Calculate the status of each line
    For i = 0 To cnt - 1
        llVal = frmHiddenData.tabData(0).GetLineStatusValue(i)
        If llVal = 1 Then
            strInsert = strInsert + frmHiddenData.tabData(0).GetColumnValue(i, 0) + ","
            strInsert = strInsert + frmHiddenData.tabData(0).GetColumnValue(i, 6) + ","
            strInsert = strInsert + frmHiddenData.tabData(0).GetColumnValue(i, 7) + ","
            strInsert = strInsert + frmHiddenData.tabData(0).GetColumnValue(i, 8) + ","
            strInsert = strInsert + frmHiddenData.tabData(0).GetLineTag(CLng(i)) + vbCrLf
            If i Mod 200 = 0 Then
                tabALT.InsertBuffer strInsert, vbCrLf
                strInsert = ""
                tabALT.RedrawTab
                tabALT.OnVScrollTo tabALT.GetLineCount - 1
                DoEvents
            End If
        End If
    Next i
    
    tabALT.InsertBuffer strInsert, vbCrLf
    For i = 0 To tabALT.GetLineCount - 1
        If i Mod 50 = 0 Then
            StatusBar1.Panels(1).Text = "Build error information: " + CStr(i) + " of " + CStr(cnt - 1)
        End If
        strTmp = tabALT.GetColumnValue(i, 4)
        If InStr(1, strTmp, "1D") > 0 Then
            ToolTipText = tabALT.GetLineTag(i)
            ToolTipText = ToolTipText + "2LC duplicate;"
            tabALT.SetLineTag i, ToolTipText
            tabALT.SetDecorationObject i, 1, "CONF"
        End If
        If InStr(1, strTmp, "1E") > 0 Then
            ToolTipText = tabALT.GetLineTag(i)
            ToolTipText = ToolTipText + "2LC empty;"
            tabALT.SetLineTag i, ToolTipText
            tabALT.SetDecorationObject i, 1, "CONF2"
        End If
        If InStr(1, strTmp, "2D") > 0 Then
            ToolTipText = tabALT.GetLineTag(i)
            ToolTipText = ToolTipText + "3LC duplicate;"
            tabALT.SetLineTag i, ToolTipText
            tabALT.SetDecorationObject i, 2, "CONF"
        End If
        If InStr(1, strTmp, "2E") > 0 Then
            ToolTipText = tabALT.GetLineTag(i)
            ToolTipText = ToolTipText + "3LC empty;"
            tabALT.SetLineTag i, ToolTipText
            tabALT.SetDecorationObject i, 2, "CONF2"
        End If
    Next i
    CheckALTBasicData = strResult
    tabALT.Sort 1, True, False
    tabALT.RedrawTab
    tabALT.OnVScrollTo 0
    lblALT.Caption = "Errors: " + CStr(tabALT.GetLineCount) + " of " + CStr(frmHiddenData.tabData(0).GetLineCount)
End Function
Public Function CheckAPTBasicData() As String
    Dim strResult As String
    Dim cnt As Long
    Dim i As Long
    Dim strLineNosAlc2 As String, strLineNosAlc3 As String
    Dim strValue As String
    Dim strAlc2 As String, strAlc3 As String
    Dim strTmp As String
    Dim strACWS As String, strACHE As String, strACLE As String
    Dim dDim As Double
    Dim blLineCf As Boolean
    Dim strInsert As String
    Dim ToolTipText As String
    Dim strOldValue As String
    Dim llVal As Long
''  0   1    2    3    4     5    6    7    8    9   10   11
''URNO,APC3,APC4,APFN,LAND,ETOF,TICH,TDI1,TDI2,APSN,CDAT,USEC,LSTU,PRFL,USEU,VAFR,VATO
    
    cnt = frmHiddenData.tabData(5).GetLineCount
    frmHiddenData.tabData(5).Sort 1, True, False 'Sort for APC3
    '==========================
    ' Check APC3
    For i = 0 To cnt - 1
        frmHiddenData.tabData(5).SetLineStatusValue i, 0
        frmHiddenData.tabData(5).SetLineTag i, ""
        ToolTipText = ""
        If i Mod 200 = 0 Then
            DoEvents
            StatusBar1.Panels(1).Text = "Checking airports 3LC: " + CStr(i) + " of " + CStr(cnt - 1)
        End If
        strValue = frmHiddenData.tabData(5).GetColumnValue(i, 1)
        If strValue <> "" Then
            If strValue = strOldValue Then
                strTmp = frmHiddenData.tabData(5).GetLineTag(i)
                strTmp = strTmp + "1D;"  'Duplicate value in col1
                frmHiddenData.tabData(5).SetLineStatusValue i, 1
                frmHiddenData.tabData(5).SetLineTag i, strTmp
                If i > 0 Then
                    frmHiddenData.tabData(5).SetLineStatusValue i - 1, 1
                    frmHiddenData.tabData(5).SetLineTag i - 1, strTmp
                End If
            End If
        Else
'            strTmp = frmHiddenData.tabData(1).GetLineTag(i)
'            strTmp = strTmp + "1E;"  'Duplicate value in col1
'            frmHiddenData.tabData(5).SetLineStatusValue i, 1
'            frmHiddenData.tabData(5).SetLineTag i, strTmp
        End If
        strOldValue = strValue
    Next i
    '==========================
    ' Check ALC3
    strOldValue = ""
    frmHiddenData.tabData(5).Sort 2, True, False 'Sort for APC4
    For i = 0 To cnt - 1
        ToolTipText = ""
        If i Mod 2000 = 0 Then
            DoEvents
            StatusBar1.Panels(1).Text = "Checking airports 4LC: " + CStr(i) + " of " + CStr(cnt - 1)
        End If
        strValue = frmHiddenData.tabData(5).GetColumnValue(i, 2)
        If strValue <> "" Then
            If strValue = strOldValue Then
                strTmp = frmHiddenData.tabData(5).GetLineTag(i)
                If strTmp <> "" Then
                    strTmp = strTmp
                End If
                strTmp = strTmp + "2D;"  'Duplicate value in col1
                frmHiddenData.tabData(5).SetLineStatusValue i, 1
                frmHiddenData.tabData(5).SetLineTag i, strTmp
                If i > 0 Then
                    frmHiddenData.tabData(5).SetLineStatusValue i - 1, 1
                    frmHiddenData.tabData(5).SetLineTag i - 1, strTmp
                End If
            End If
        Else
            strTmp = frmHiddenData.tabData(5).GetLineTag(i)
                If strTmp <> "" Then
                    strTmp = strTmp
                End If
            strTmp = strTmp + "2E;"  'Duplicate value in col1
            frmHiddenData.tabData(5).SetLineStatusValue i, 1
            frmHiddenData.tabData(5).SetLineTag i, strTmp
        End If
        strOldValue = strValue
    Next i
    '=============================
    ' Calculate the status of each line
    For i = 0 To cnt - 1
        llVal = frmHiddenData.tabData(5).GetLineStatusValue(i)
        If llVal = 1 Then
            strInsert = strInsert + frmHiddenData.tabData(5).GetColumnValue(i, 0) + ","
            strInsert = strInsert + frmHiddenData.tabData(5).GetColumnValue(i, 1) + ","
            strInsert = strInsert + frmHiddenData.tabData(5).GetColumnValue(i, 2) + ","
            strInsert = strInsert + frmHiddenData.tabData(5).GetColumnValue(i, 3) + ","
            strInsert = strInsert + frmHiddenData.tabData(5).GetLineTag(CLng(i)) + vbCrLf
            If i Mod 200 = 0 Then
                tabAPT.InsertBuffer strInsert, vbCrLf
                strInsert = ""
                tabAPT.RedrawTab
                tabAPT.OnVScrollTo tabAPT.GetLineCount - 1
                DoEvents
            End If
        End If
    Next i
    
    tabAPT.InsertBuffer strInsert, vbCrLf
    For i = 0 To tabAPT.GetLineCount - 1
        If i Mod 50 = 0 Then
            StatusBar1.Panels(1).Text = "Build error information: " + CStr(i) + " of " + CStr(cnt - 1)
        End If
        strTmp = tabAPT.GetColumnValue(i, 4)
        If InStr(1, strTmp, "1D") > 0 Then
            ToolTipText = tabAPT.GetLineTag(i)
            ToolTipText = ToolTipText + "3LC duplicate;"
            tabAPT.SetLineTag i, ToolTipText
            tabAPT.SetDecorationObject i, 1, "CONF"
        End If
        If InStr(1, strTmp, "1E") > 0 Then
            ToolTipText = tabAPT.GetLineTag(i)
            ToolTipText = ToolTipText + "3LC empty;"
            tabAPT.SetLineTag i, ToolTipText
            tabAPT.SetDecorationObject i, 1, "CONF2"
        End If
        If InStr(1, strTmp, "2D") > 0 Then
            ToolTipText = tabAPT.GetLineTag(i)
            ToolTipText = ToolTipText + "4LC duplicate;"
            tabAPT.SetLineTag i, ToolTipText
            tabAPT.SetDecorationObject i, 2, "CONF"
        End If
        If InStr(1, strTmp, "2E") > 0 Then
            ToolTipText = tabAPT.GetLineTag(i)
            ToolTipText = ToolTipText + "4LC empty;"
            tabAPT.SetLineTag i, ToolTipText
            tabAPT.SetDecorationObject i, 2, "CONF2"
        End If
    Next i
    CheckAPTBasicData = strResult
    tabAPT.Sort 1, True, False
    tabAPT.RedrawTab
    tabAPT.OnVScrollTo 0
    lblAPT.Caption = "Errors: " + CStr(tabAPT.GetLineCount) + " of " + CStr(frmHiddenData.tabData(5).GetLineCount)
End Function
Public Function CheckACRBasicData() As String
    Dim strResult As String
    Dim cnt As Long
    Dim i As Long
    Dim ilLine As Long
    Dim strLineNos As String
    Dim strValue As String
    Dim strOldValue As String
    Dim strNextValue As String
    Dim strTmp As String
    Dim strACT3 As String
    Dim strACT3List As String
    Dim dDim As Double
    Dim blLineCf As Boolean
    Dim strInsert As String
    Dim strInsert2 As String
    Dim ToolTipText As String
    Dim blLineInserted As Boolean
    Dim strBuff As String
    Dim llVal As Long
    Dim ilMinLen As Integer
    Dim strExclude As String
    Dim strREGNCFG As String
    Dim strACT3CFG As String
    '------------ Setupstring
    ' 0      1      2     3         4          5
    'Table,Fields,Unique,Blank,Exclude Char,Min.Len."
    strLineNos = frmHiddenData.tabSetup.GetLinesByColumnValue(0, "ACR", 0)
    For i = 0 To ItemCount(strLineNos, ",") - 1
        strValue = frmHiddenData.tabSetup.GetLineValues(CLng(GetRealItem(strLineNos, CInt(i), ",")))
        If InStr(1, strValue, "ACT3") > 0 Then
            strACT3CFG = strValue
        End If
        If InStr(1, strValue, "REGN") > 0 Then
            strREGNCFG = strValue
        End If
    Next i
    blLineCf = False
    strOldValue = ""
    strValue = ""
    ilMinLen = val(GetRealItem(strREGNCFG, 5, ","))
    strExclude = GetRealItem(strREGNCFG, 4, ",")
    
    cnt = frmHiddenData.tabData(7).GetLineCount
    For i = 0 To cnt - 1
        '==============
        'Check for unique
'  0   1    2    3    4     5    6    7    8    9   10   11
'Urno,Regn,Act3
        blLineCf = False
        blLineInserted = False
        frmHiddenData.tabData(7).SetLineStatusValue i, 0
        frmHiddenData.tabData(7).SetLineTag i, ""
        ToolTipText = ""
        If i Mod 2000 = 0 Then
            DoEvents
            StatusBar1.Panels(1).Text = "Checking registrations (duplicates): " + CStr(i) + " of " + CStr(cnt - 1)
        End If
        strValue = frmHiddenData.tabData(7).GetColumnValue(i, 1)
        '=================
        ' LineTagValues
        ' 1D  = REGN duplicate
        ' 1E  = REGN empty
        ' 1EC = REGN excluded character
        ' 1S  = REGN len is to short
        ' 2N  = ACT3 doesn't exist
        ' 2E  = ACT3 empty
        '
        If strValue <> "" Then
            If strValue = strOldValue Then
                If GetRealItem(strREGNCFG, 2, ",") = "Y" Then 'Check for duplicate ?
                    strTmp = frmHiddenData.tabData(7).GetLineTag(i)
                    strTmp = strTmp + "1D;"  'REGN duplicate
                    frmHiddenData.tabData(7).SetLineStatusValue i, 1
                    frmHiddenData.tabData(7).SetLineTag i, strTmp
                    If i > 0 Then
                        frmHiddenData.tabData(7).SetLineStatusValue i - 1, 1
                        frmHiddenData.tabData(7).SetLineTag i - 1, strTmp
                    End If
                    blLineCf = True
                End If
            End If
            If Len(strValue) < ilMinLen Then 'Check for len
                strTmp = frmHiddenData.tabData(7).GetLineTag(i)
                strTmp = strTmp + "1S;"  'REGN too short
                frmHiddenData.tabData(7).SetLineStatusValue i, 1
                frmHiddenData.tabData(7).SetLineTag i, strTmp
            End If
            If strExclude <> "" Then 'check for exclude chars
                If InStr(1, strValue, strExclude) > 0 Then
                    strTmp = frmHiddenData.tabData(7).GetLineTag(i)
                    strTmp = strTmp + "1EC;"  'REGN contains exclude chars
                    frmHiddenData.tabData(7).SetLineStatusValue i, 1
                    frmHiddenData.tabData(7).SetLineTag i, strTmp
                End If
            End If
        Else
            If GetRealItem(strREGNCFG, 3, ",") = "N" Then 'Check for blanks ?
                strTmp = frmHiddenData.tabData(7).GetLineTag(i)
                strTmp = strTmp + "1E;"  'REGN empty
                frmHiddenData.tabData(7).SetLineStatusValue i, 1
                frmHiddenData.tabData(7).SetLineTag i, strTmp
            End If
        End If
        strOldValue = strValue
    Next i
    '===============
    ' Check for act3<<
    strACT3List = frmHiddenData.tabData(1).SelectDistinct("1", "", "", ",", True)
    For i = 0 To cnt - 1
        If i Mod 2000 = 0 Then
            DoEvents
            StatusBar1.Panels(1).Text = "Checking registrations (ACT3): " + CStr(i) + " of " + CStr(cnt - 1)
        End If
        strInsert2 = frmHiddenData.tabData(7).GetColumnValue(i, 1)
        strValue = frmHiddenData.tabData(7).GetColumnValue(i, 2)
        If strValue = "" Then
            If GetRealItem(strACT3CFG, 3, ",") = "N" Then 'Check for blanks ?
                strTmp = frmHiddenData.tabData(7).GetLineTag(i)
                strTmp = strTmp + "2E;"  'ACT3 empty
                frmHiddenData.tabData(7).SetLineStatusValue i, 1
                frmHiddenData.tabData(7).SetLineTag i, strTmp
            End If
        Else
            If InStr(1, strACT3List, strValue) = 0 Then
                strTmp = frmHiddenData.tabData(7).GetLineTag(i)
                strTmp = strTmp + "2N;"  'ACT3 doesnt exist
                frmHiddenData.tabData(7).SetLineStatusValue i, 1
                frmHiddenData.tabData(7).SetLineTag i, strTmp
            End If
        End If
    Next i
    ' End Check for act3
    '===============
    
    For i = 0 To cnt - 1
        llVal = frmHiddenData.tabData(7).GetLineStatusValue(i)
        If llVal = 1 Then
            strInsert = strInsert + frmHiddenData.tabData(7).GetColumnValue(i, 0) + ","
            strInsert = strInsert + frmHiddenData.tabData(7).GetColumnValue(i, 1) + ","
            strInsert = strInsert + frmHiddenData.tabData(7).GetColumnValue(i, 2) + ","
            strInsert = strInsert + frmHiddenData.tabData(7).GetLineTag(CLng(i)) + vbCrLf
            If i Mod 200 = 0 Then
                tabACR.InsertBuffer strInsert, vbCrLf
                strInsert = ""
                tabACR.RedrawTab
                tabACR.OnVScrollTo tabACR.GetLineCount - 1
                DoEvents
            End If
        End If
    Next i
    
    tabACR.InsertBuffer strInsert, vbCrLf
    For i = 0 To tabACR.GetLineCount - 1
        If i Mod 50 = 0 Then
            StatusBar1.Panels(1).Text = "Checking registrations (ACT3): " + CStr(i) + " of " + CStr(cnt - 1)
        End If
        strTmp = tabACR.GetColumnValue(i, 3)
        If InStr(1, strTmp, "1D;") > 0 Then
            ToolTipText = tabACR.GetLineTag(i)
            ToolTipText = ToolTipText + "REGN duplicate; "
            tabACR.SetLineTag i, ToolTipText
            tabACR.SetDecorationObject i, 1, "CONF"
        End If
        If InStr(1, strTmp, "1E;") > 0 Then
            ToolTipText = tabACR.GetLineTag(i)
            ToolTipText = ToolTipText + "REGN empty; "
            tabACR.SetLineTag i, ToolTipText
            tabACR.SetDecorationObject i, 1, "CONF"
        End If
        If InStr(1, strTmp, "1EC;") > 0 Then
            ToolTipText = tabACR.GetLineTag(i)
            ToolTipText = ToolTipText + "REGN contains " + strExclude + "; "
            tabACR.SetLineTag i, ToolTipText
            tabACR.SetDecorationObject i, 1, "CONF2"
        End If
        If InStr(1, strTmp, "1S;") > 0 Then
            ToolTipText = tabACR.GetLineTag(i)
            ToolTipText = ToolTipText + "REGN too short; "
            tabACR.SetLineTag i, ToolTipText
            tabACR.SetDecorationObject i, 1, "CONF2"
        End If
        If InStr(1, strTmp, "2N;") > 0 Then
            ToolTipText = tabACR.GetLineTag(i)
            ToolTipText = ToolTipText + "ACT3 doesn't exist; "
            tabACR.SetLineTag i, ToolTipText
            tabACR.SetDecorationObject i, 2, "CONF"
        End If
        If InStr(1, strTmp, "2E;") > 0 Then
            ToolTipText = tabACR.GetLineTag(i)
            ToolTipText = ToolTipText + "ACT3 empty; "
            tabACR.SetLineTag i, ToolTipText
            tabACR.SetDecorationObject i, 2, "CONF2"
        End If
        
    Next i
    tabACR.Sort 1, True, False
    tabACR.RedrawTab
    tabACR.OnVScrollTo 0
    lblACR.Caption = "Errors: " + CStr(tabACR.GetLineCount) + " of " + CStr(frmHiddenData.tabData(7).GetLineCount)
    CheckACRBasicData = strResult

End Function
'=======================================================
' Reintitializes the tabs according to the ipPage parameter
' Page maens the tabfolder
Public Sub InitBasicDataTabs(ipPage As Integer)
    If ipPage = 1 Then
        tabACT.ResetContent
        tabACT.MainHeader = True
        tabACT.SetMainHeaderValues "6", "Aircrafts", "12632256," + CStr(vbBlack)
        tabACT.HeaderLengthString = "0,40,40,50,50,50,0"
        tabACT.HeaderString = "URNO,ACT3,ACT5,Wing,Height,Length,tag"
        tabACT.EnableHeaderSizing True
        tabACT.EnableInlineEdit True
        tabACT.NoFocusColumns = "0,6"
        tabACT.CreateDecorationObject "CONF", "B", 1, 255
        
        tabALT.ResetContent
        tabALT.MainHeader = True
        tabALT.SetMainHeaderValues "5", "Airlines", "12632256," + CStr(vbBlack)
        tabALT.HeaderLengthString = "0,40,40,150,0"
        tabALT.HeaderString = "URNO,ALC2,ALC3,ALFN,tag"
        tabALT.EnableHeaderSizing True
        tabALT.EnableInlineEdit True
        tabALT.NoFocusColumns = "0,4"
        tabALT.CreateDecorationObject "CONF", "B", 1, 255
        tabALT.CreateDecorationObject "CONF2", "B", 1, 8438015
    
        tabNatures(0).ResetContent
        tabNatures(0).MainHeader = True
        tabNatures(0).SetMainHeaderValues "3", "Natures", "12632256," + CStr(vbBlack)
        tabNatures(0).HeaderLengthString = "0,40,180,0"
        tabNatures(0).HeaderString = "URNO,TTYP,TNAM,tag"
        tabNatures(0).EnableHeaderSizing True
        tabNatures(0).EnableInlineEdit True
        tabNatures(0).NoFocusColumns = "0,3"
        tabNatures(0).CreateDecorationObject "CONF", "B", 1, 255
        tabNatures(0).CreateDecorationObject "CONF2", "B", 1, 8438015
        
        tabAPT.ResetContent
        tabAPT.MainHeader = True
        tabAPT.SetMainHeaderValues "5", "Airports", "12632256," + CStr(vbBlack)
        tabAPT.HeaderLengthString = "0,40,40,150,0"
        tabAPT.HeaderString = "URNO,APC3,APC4,APFN,tag"
        tabAPT.EnableHeaderSizing True
        tabAPT.EnableInlineEdit True
        tabAPT.NoFocusColumns = "0,4"
        tabAPT.CreateDecorationObject "CONF", "B", 1, 255
        tabAPT.CreateDecorationObject "CONF2", "B", 1, 8438015
    
        tabBLT.ResetContent
        tabBLT.MainHeader = True
        tabBLT.SetMainHeaderValues "3", "Baggage Belts", "12632256," + CStr(vbBlack)
        tabBLT.HeaderLengthString = "0,250,0"
        tabBLT.HeaderString = "URNO,BNAM,tag"
        tabBLT.EnableHeaderSizing True
        tabBLT.EnableInlineEdit True
        tabBLT.NoFocusColumns = "0,2"
        tabBLT.CreateDecorationObject "CONF", "B", 1, 255
        tabBLT.CreateDecorationObject "CONF2", "B", 1, 8438015
    
        tabCIC.ResetContent
        tabCIC.MainHeader = True
        tabCIC.SetMainHeaderValues "3", "Checkin Counter", "12632256," + CStr(vbBlack)
        tabCIC.HeaderLengthString = "0,250,0"
        tabCIC.HeaderString = "URNO,CNAM,tag"
        tabCIC.EnableHeaderSizing True
        tabCIC.EnableInlineEdit True
        tabCIC.NoFocusColumns = "0,2"
        tabCIC.CreateDecorationObject "CONF", "B", 1, 255
        tabCIC.CreateDecorationObject "CONF2", "B", 1, 8438015
        
        tabACR.ResetContent
        tabACR.MainHeader = True
        tabACR.SetMainHeaderValues "4", "Registrations", "12632256," + CStr(vbBlack)
        tabACR.HeaderLengthString = "0,100,100,0"
        tabACR.HeaderString = "URNO,REGN,ACT3,Tag"
        tabACR.EnableHeaderSizing True
        tabACR.EnableInlineEdit True
        tabACR.NoFocusColumns = "0,3"
        tabACR.CreateDecorationObject "CONF", "B", 1, 255
        tabACR.CreateDecorationObject "CONF2", "B", 1, 8438015
    End If
    If ipPage = 2 Then
        tabBasicData(0).ResetContent
        tabBasicData(0).MainHeader = True
        tabBasicData(0).SetMainHeaderValues "3", "Positions", "12632256," + CStr(vbBlack)
        tabBasicData(0).HeaderLengthString = "0,250,0"
        tabBasicData(0).HeaderString = "URNO,PNAM,tag"
        tabBasicData(0).EnableHeaderSizing True
        tabBasicData(0).EnableInlineEdit True
        tabBasicData(0).NoFocusColumns = "0,2"
        tabBasicData(0).CreateDecorationObject "CONF", "B", 1, 255
        tabBasicData(0).CreateDecorationObject "CONF2", "B", 1, 8438015
        
        tabBasicData(1).ResetContent
        tabBasicData(1).MainHeader = True
        tabBasicData(1).SetMainHeaderValues "3", "Gates", "12632256," + CStr(vbBlack)
        tabBasicData(1).HeaderLengthString = "0,250,0"
        tabBasicData(1).HeaderString = "URNO,GNAM,tag"
        tabBasicData(1).EnableHeaderSizing True
        tabBasicData(1).EnableInlineEdit True
        tabBasicData(1).NoFocusColumns = "0,2"
        tabBasicData(1).CreateDecorationObject "CONF", "B", 1, 255
        tabBasicData(1).CreateDecorationObject "CONF2", "B", 1, 8438015
        
        tabBasicData(2).ResetContent
        tabBasicData(2).MainHeader = True
        tabBasicData(2).SetMainHeaderValues "6", "Groupmembers", "12632256," + CStr(vbBlack)
        tabBasicData(2).HeaderLengthString = "80,80,100,80,80,100"
        tabBasicData(2).HeaderString = "URNO,GURN,RNAM,VALU,RefTab,error"
        tabBasicData(2).EnableHeaderSizing True
        'tabBasicData(2).EnableInlineEdit True
        tabBasicData(2).NoFocusColumns = "0,5"
        tabBasicData(2).CreateDecorationObject "CONF", "B", 1, 255
        tabBasicData(2).CreateDecorationObject "CONF2", "B", 1, 8438015
        
        'ACTM,ALMA,ALMD,HTGA,HTGD,PCAA,PCAD,TRRA,TRRD,TTGA,TTGD
        tabBasicData(3).ResetContent
        tabBasicData(3).MainHeader = True
        tabBasicData(3).SetMainHeaderValues "15", "CIC Rules", "12632256," + CStr(vbBlack)
        tabBasicData(3).HeaderLengthString = "80,100,80,80,80,80,80,80,80,80,80,80,80,80,0"
        tabBasicData(3).HeaderString = "UNRO,PRNA,PRSN,ACTM,ALMA,ALMD,HTGA,HTGD,PCAA,PCAD,TRRA,TRRD,TTGA,TTGD,tag"
        tabBasicData(3).EnableHeaderSizing True
        tabBasicData(3).EnableInlineEdit True
        'tabBasicData(3).NoFocusColumns = "0,5"
        tabBasicData(3).CreateDecorationObject "CONF", "B", 1, 255
        tabBasicData(3).CreateDecorationObject "CONF2", "B", 1, 8438015
        tabBasicData(3).ShowHorzScroller True
    End If
End Sub

Public Function CheckTABBasicDataSimple( _
                ByRef destTab As TABLib.TAB, _
                ByRef srcTab As TABLib.TAB, _
                idxCheckField As Integer, _
                strFieldName As String, _
                strTableName As String, _
                strVisibleColumns As String) As String
                
    Dim strResult As String
    Dim cnt As Long
    Dim i As Long, j As Long
    Dim strLineNosAlc2 As String, strLineNosAlc3 As String
    Dim strValue As String
    Dim strTmp As String
    Dim blLineCf As Boolean
    Dim strInsert As String
    Dim ToolTipText As String
    Dim strOldValue As String
    Dim llVal As Long
    
    cnt = srcTab.GetLineCount
    srcTab.Sort idxCheckField, True, False 'Sort for checkfield
    '==========================
    For i = 0 To cnt - 1
        srcTab.SetLineStatusValue i, 0
        srcTab.SetLineTag i, ""
        ToolTipText = ""
        If i Mod 200 = 0 Then
            DoEvents
            StatusBar1.Panels(1).Text = "Checking " + strTableName + " " + CStr(i) + " of " + CStr(cnt - 1)
        End If
        strValue = srcTab.GetColumnValue(i, idxCheckField)
        If strValue <> "" Then
            If strValue = strOldValue Then
                strTmp = srcTab.GetLineTag(i)
                strTmp = strTmp + "1D;"  'Duplicate value in col1
                srcTab.SetLineStatusValue i, 1
                srcTab.SetLineTag i, strTmp
                If i > 0 Then
                    srcTab.SetLineStatusValue i - 1, 1
                    srcTab.SetLineTag i - 1, strTmp
                End If
            End If
        Else
            strTmp = srcTab.GetLineTag(i)
            strTmp = strTmp + "1E;"  'Duplicate value in col1
            srcTab.SetLineStatusValue i, 1
            srcTab.SetLineTag i, strTmp
        End If
        strOldValue = strValue
    Next i
    '==========================


    '=============================
    ' Calculate the status of each line
    For i = 0 To cnt - 1
        llVal = srcTab.GetLineStatusValue(i)
        If llVal = 1 Then
            For j = 0 To ItemCount(strVisibleColumns, ",") - 1
                strTmp = GetRealItem(strVisibleColumns, CInt(j), ",")
                strInsert = strInsert + srcTab.GetColumnValue(i, CLng(strTmp)) + ","
            Next j
            strInsert = strInsert + srcTab.GetLineTag(CLng(i)) + vbCrLf
            If i Mod 200 = 0 Then
                destTab.InsertBuffer strInsert, vbCrLf
                strInsert = ""
                destTab.RedrawTab
                destTab.OnVScrollTo destTab.GetLineCount - 1
                DoEvents
            End If
        End If
    Next i
    
    destTab.InsertBuffer strInsert, vbCrLf
    For i = 0 To destTab.GetLineCount - 1
        If i Mod 50 = 0 Then
            StatusBar1.Panels(1).Text = "Build error information: " + CStr(i) + " of " + CStr(cnt - 1)
        End If
        strTmp = destTab.GetColumnValue(i, ItemCount(strVisibleColumns, ","))
        If InStr(1, strTmp, "1D") > 0 Then
            ToolTipText = destTab.GetLineTag(i)
            ToolTipText = ToolTipText + strFieldName + " duplicate;"
            destTab.SetLineTag i, ToolTipText
            destTab.SetDecorationObject i, 1, "CONF"
        End If
        If InStr(1, strTmp, "1E") > 0 Then
            ToolTipText = destTab.GetLineTag(i)
            ToolTipText = ToolTipText + strFieldName + "  empty;"
            destTab.SetLineTag i, ToolTipText
            destTab.SetDecorationObject i, 1, "CONF2"
        End If
    Next i
    CheckTABBasicDataSimple = strResult
    destTab.Sort idxCheckField, True, False
    destTab.RedrawTab
    destTab.OnVScrollTo 0

End Function

Public Function CheckGRNData() As String
    Dim strResult As String
    Dim cnt As Long
    Dim i As Long, j As Long
    Dim strLineNos As String
    Dim strValue As String
    Dim strTmp As String
    Dim blLineCf As Boolean
    Dim strInsert As String
    Dim ToolTipText As String
    Dim strOldValue As String
    Dim llVal As Long
    Dim strTables As String
    Dim strTableUrnosIdx As String
    Dim strLookupFields As String
    Dim ilIdx As Integer
    Dim ilTabIdx As Integer
    Dim idxGurn As Integer
    Dim idxTabn As Integer
    Dim idxValu As Integer
    Dim idxLineGrn As Long
    Dim strGrnUrno As String
    Dim strGurn As String
    Dim blNotFound As Boolean
    Dim strDBTable As String
    
    idxGurn = 2
    idxTabn = 2
    idxValu = 1
    idxLineGrn = -1
    strGurn = ""
    strOldValue = ""
    blNotFound = False
    strTables = "CIC,ALT,NAT,APT,PST,ACT,HTY"
    strTableUrnosIdx = "0,0,0,0,0,0,0"
    strLookupFields = "URNO;CNAM,URNO;ALC2;ALC3,URNO;TTYP,URNO;APC3;APC4,URNO;PNAM,URNO;ACT3;ACT5,URNO;HNAM"
    ilTabIdx = GetDataTabIndex("GRMTAB")
    cnt = frmHiddenData.tabData(10).GetLineCount
    frmHiddenData.tabData(10).Sort 2, True, False 'Sort for checkfield
    '==========================
    For i = 0 To cnt - 1
        frmHiddenData.tabData(ilTabIdx).SetLineStatusValue i, 0
        frmHiddenData.tabData(ilTabIdx).SetLineTag i, ""
        ToolTipText = ""
        If i Mod 200 = 0 Then
            DoEvents
            StatusBar1.Panels(1).Text = "Checking groupmembers for dead bodies: " + " " + CStr(i) + " of " + CStr(cnt - 1)
        End If
        strGurn = frmHiddenData.tabData(10).GetColumnValue(i, idxGurn)
        If strGurn <> "" Then
            If strGurn = strOldValue Then
                If blNotFound = True Then
                    strTmp = frmHiddenData.tabData(10).GetLineTag(i)
                    strTmp = strTmp + "1E;"  'Gurn has group
                    frmHiddenData.tabData(10).SetLineStatusValue i, 1
                    frmHiddenData.tabData(10).SetLineTag i, strTmp
                End If
            Else
                blNotFound = False
                idxLineGrn = idxLineGrn + 1
                strLineNos = frmHiddenData.tabData(9).GetLinesByColumnValue(0, strGurn, 0)
                If ItemCount(strLineNos, ",") > 0 Then
                Else
                    'Member has not longer a group
                    blNotFound = True
                    strTmp = frmHiddenData.tabData(10).GetLineTag(i)
                    strTmp = strTmp + "1E;"  'Gurn has group
                    frmHiddenData.tabData(10).SetLineStatusValue i, 1
                    frmHiddenData.tabData(10).SetLineTag i, strTmp
                End If
            End If
        Else
        End If
        strOldValue = strGurn
    Next i
    '=========================================
    ' Checking VALU for existence in basicdata
    For i = 0 To cnt - 1
        'check only lines without errors
        If i Mod 20 = 0 Then
            DoEvents
            StatusBar1.Panels(1).Text = "Checking referenced groupmembers for existence: " + " " + CStr(i) + " of " + CStr(cnt - 1)
        End If
        If frmHiddenData.tabData(10).GetLineStatusValue(i) = 0 Then
            strGurn = frmHiddenData.tabData(10).GetColumnValue(i, idxGurn)
            strValue = frmHiddenData.tabData(10).GetColumnValue(i, 1)
            strLineNos = frmHiddenData.tabData(9).GetLinesByColumnValue(0, strGurn, 0)
            If Len(strLineNos) > 0 Then
                strTmp = frmHiddenData.tabData(9).GetColumnValue(CLng(GetRealItem(strLineNos, 0, ",")), 2)
                strDBTable = left(strTmp, 3) + "TAB"
                ilTabIdx = GetDataTabIndex(strDBTable)
                If ilTabIdx > -1 Then
                    strLineNos = frmHiddenData.tabData(ilTabIdx).GetLinesByColumnValue(0, strValue, 0)
                    If ItemCount(strLineNos, ",") = 0 Then
                        strTmp = frmHiddenData.tabData(10).GetLineTag(i)
                        strTmp = strTmp + "1N;"  'VALU does not exist in Basic data
                        frmHiddenData.tabData(10).SetLineStatusValue i, 1
                        frmHiddenData.tabData(10).SetLineTag i, strTmp
                    End If
                End If
            Else
            End If
        End If
    Next i
    '=============================
    ' Calculate the status of each line
    For i = 0 To cnt - 1
        llVal = frmHiddenData.tabData(10).GetLineStatusValue(i)
        If llVal = 1 Then
            strGurn = frmHiddenData.tabData(10).GetColumnValue(i, idxGurn)
            strValue = frmHiddenData.tabData(10).GetColumnValue(i, 1)
            strLineNos = frmHiddenData.tabData(9).GetLinesByColumnValue(0, strGurn, 0)
            If Len(strLineNos) > 0 Then
                strInsert = strInsert + frmHiddenData.tabData(10).GetColumnValue(i, 0) + ","
                strInsert = strInsert + strGurn + ","
                strInsert = strInsert + frmHiddenData.tabData(9).GetColumnValue(CLng(GetRealItem(strLineNos, 0, ",")), 5) + ","
                strTmp = frmHiddenData.tabData(9).GetColumnValue(CLng(GetRealItem(strLineNos, 0, ",")), 2)
                strInsert = strInsert + strValue + ","
                strDBTable = left(strTmp, 3)
                strInsert = strInsert + strDBTable + ","
                strInsert = strInsert + frmHiddenData.tabData(10).GetLineTag(CLng(i)) + vbCrLf
            Else 'is a missing reference
                strInsert = strInsert + frmHiddenData.tabData(10).GetColumnValue(i, 0) + ","
                strInsert = strInsert + strGurn + ","
                strInsert = strInsert + ","
                strInsert = strInsert + strValue + ","
                strInsert = strInsert + ","
                strInsert = strInsert + frmHiddenData.tabData(10).GetLineTag(CLng(i)) + vbCrLf
            End If
            If i Mod 200 = 0 Then
                tabBasicData(2).InsertBuffer strInsert, vbCrLf
                strInsert = ""
                tabBasicData(2).RedrawTab
                tabBasicData(2).OnVScrollTo tabBasicData(2).GetLineCount - 1
                DoEvents
            End If
        End If
    Next i
    
    tabBasicData(2).InsertBuffer strInsert, vbCrLf
    For i = 0 To tabBasicData(2).GetLineCount - 1
        If i Mod 50 = 0 Then
            StatusBar1.Panels(1).Text = "Build error information: " + CStr(i) + " of " + CStr(cnt - 1)
        End If
        strTmp = tabBasicData(2).GetColumnValue(i, 5)
        If InStr(1, strTmp, "1N;") > 0 Then
            ToolTipText = tabBasicData(2).GetLineTag(i)
            ToolTipText = ToolTipText + "Value doesn't exist in basicdata; "
            tabBasicData(2).SetLineTag i, ToolTipText
            tabBasicData(2).SetDecorationObject i, 3, "CONF"
        End If
        If InStr(1, strTmp, "1E;") > 0 Then
            ToolTipText = tabBasicData(2).GetLineTag(i)
            ToolTipText = ToolTipText + "Groupmember without Group; "
            tabBasicData(2).SetLineTag i, ToolTipText
            tabBasicData(2).SetDecorationObject i, 1, "CONF"
        End If
    Next i
    tabBasicData(2).Sort 5, True, False
    CheckGRNData = strResult
End Function
Public Function CheckGHPData() As String
    Dim strResult As String
    Dim cnt As Long
    Dim i As Long, j As Long
    Dim strLineNos As String
    Dim strValue As String
    Dim strTmp As String
    Dim blLineCf As Boolean
    Dim strInsert As String
    Dim ToolTipText As String
    Dim strOldValue As String
    Dim llVal As Long
    Dim strCheckFields As String
    Dim idxField As Integer
    Dim strTag As String
    
    
    strCheckFields = "ACTM,ALMA,ALMD,HTGA,HTGD,PCAA,PCAD,TRRA,TRRD,TTGA,TTGD"
    strOldValue = ""
    frmHiddenData.tabData(llIdxGHPTAB).Sort 1, True, False
    cnt = frmHiddenData.tabData(llIdxGHPTAB).GetLineCount
    '==========================
    For i = 0 To cnt - 1
        frmHiddenData.tabData(llIdxGHPTAB).SetLineStatusValue i, 0
        frmHiddenData.tabData(llIdxGHPTAB).SetLineTag i, ""
        ToolTipText = ""
        If i Mod 200 = 0 Then
            DoEvents
            StatusBar1.Panels(1).Text = "Checking CIC Rules for group reference: " + " " + CStr(i) + " of " + CStr(cnt - 1)
        End If
            For j = 0 To ItemCount(strCheckFields, ",") - 1
            idxField = GetRealItemNo(frmHiddenData.tabData(llIdxGHPTAB).HeaderString, GetRealItem(strCheckFields, CInt(j), ","))
            If idxField <> -1 Then
                strValue = frmHiddenData.tabData(llIdxGHPTAB).GetColumnValue(i, idxField)
                If strValue <> "" Then
                    strLineNos = frmHiddenData.tabData(llIdxGRNTAB).GetLinesByColumnValue(0, strValue, 0)
                    If Len(strLineNos) = 0 Then
                        strTmp = frmHiddenData.tabData(llIdxGHPTAB).GetLineTag(i)
                        strTmp = strTmp + GetRealItem(strCheckFields, CInt(j), ",") + ";"  'Gurn has group
                        frmHiddenData.tabData(llIdxGHPTAB).SetLineStatusValue i, 1
                        frmHiddenData.tabData(llIdxGHPTAB).SetLineTag i, strTmp
                    End If
                End If
            End If
        Next j
    Next i
    For i = 0 To cnt - 1
        llVal = frmHiddenData.tabData(llIdxGHPTAB).GetLineStatusValue(i)
        If llVal = 1 Then
            strValue = frmHiddenData.tabData(llIdxGHPTAB).GetColumnValue(i, 0) + "," + _
                       frmHiddenData.tabData(llIdxGHPTAB).GetColumnValue(i, 1) + "," + _
                       frmHiddenData.tabData(llIdxGHPTAB).GetColumnValue(i, 2) + ","
            For j = 0 To ItemCount(strCheckFields, ",") - 1
                idxField = GetRealItemNo(frmHiddenData.tabData(llIdxGHPTAB).HeaderString, GetRealItem(strCheckFields, CInt(j), ","))
                If idxField <> -1 Then
                    strTmp = frmHiddenData.tabData(llIdxGHPTAB).GetColumnValue(i, idxField)
                    strValue = strValue + strTmp + ","
                End If
            Next j
            strTag = frmHiddenData.tabData(llIdxGHPTAB).GetLineTag(i)
            strValue = strValue + frmHiddenData.tabData(llIdxGHPTAB).GetLineTag(i)
            tabBasicData(3).InsertTextLine strValue, False
            For j = 0 To ItemCount(strTag, ";") - 1
                idxField = GetRealItemNo(tabBasicData(3).HeaderString, GetRealItem(strTag, CInt(j), ";"))
                If idxField > -1 Then
                    tabBasicData(3).SetDecorationObject tabBasicData(3).GetLineCount - 1, idxField, "CONF"
                End If
            Next j
        End If
    Next i
    tabBasicData(3).RedrawTab
End Function
Public Function GetDataTabIndex(strTab As String) As Integer
    Dim cnt As Integer
    Dim ilRet As Integer
    Dim i As Integer
    Dim arr() As String
    
    ilRet = -1
    cnt = frmHiddenData.tabData.UBound
    For i = 0 To cnt
        arr = Split(frmHiddenData.tabData(i).Tag, ";")
        If arr(0) = strTab Then
            ilRet = i
        End If
    Next i
    GetDataTabIndex = ilRet
End Function

Public Function GeneralUpdatePerRELHDL( _
                ByRef destTab As TABLib.TAB, _
                ByRef srcTab As TABLib.TAB, _
                strDBTable As String, _
                strDestTabFields As String, _
                strSrcTabFields As String, _
                lblCounter As Label)

    Dim cnt As Long, cnt2 As Long
    Dim i As Long, j As Long
    Dim strLineNos As String
    Dim strDeletedUrnoList As String
    Dim strUpdatedUrnoList As String
    Dim strValue As String
    Dim strTmp As String
    Dim strUrno As String
    Dim llVal As Long
    Dim llLine As Long
    Dim strResult As String
    Dim idxCol As Integer
    
    Dim strIRT As String
    Dim strURT As String
    Dim strDRT As String
    Dim strFieldList As String
    Dim strDataList As String
    Dim strUpdateString As String
    Dim strInsertString As String
    Dim strDeleteString As String
    Dim counter As Integer
    Dim ilNoUpdates As Integer
    Dim ilNoDeletes As Integer
    Dim strData As String

    If strDBTable = "" Or strDestTabFields = "" Or strSrcTabFields = "" Then
        MsgBox "DBTable, source or dest. fields is empty!"
        Exit Function
    End If
'====================================
' For the parameterizes table
' Detect and update changes or deletes
' "LATE" = Return wenn fertig
' "NOBC" = keinen BC
' "NOACTION" = nicht zum action schicken
' "NOLOG"  = ?
    counter = 0
    ilNoUpdates = 0
    ilNoDeletes = 0
    If Len(strDBTable) = 3 Then
        strDBTable = strDBTable + "TAB"
    End If
    cnt = srcTab.GetLineCount
    strIRT = "*CMD*," + strDBTable + ",IRT," + CStr(ItemCount(strSrcTabFields, ",")) + ","
    strURT = "*CMD*," + strDBTable + ",URT," + CStr(ItemCount(strSrcTabFields, ",")) + ","
    strDRT = "*CMD*," + strDBTable + ",DRT,-1,"
    'URNO,ACT3,ACT5,ACTI,ACFN,ACWS,ACLE,ACHE,VAFR,VATO,HOPO
    strInsertString = strIRT + strSrcTabFields + vbLf
    strUpdateString = strURT + strSrcTabFields + ",[URNO=:VURNO]" + vbLf
    strDeleteString = strDRT + ",[URNO=:VURNO]" + vbLf
    For i = 0 To cnt - 1
        llVal = srcTab.GetLineStatusValue(i)
        If llVal = 2 Or llVal = 3 Then 'was a delete
            strUrno = srcTab.GetColumnValue(i, 0)
            strDeletedUrnoList = strDeletedUrnoList + strUrno + ","
            strDeleteString = strDeleteString + strUrno + vbLf
            counter = counter + 1
            ilNoDeletes = ilNoDeletes + 1
        End If
        If llVal = 1 Then ' was updated
            strUrno = srcTab.GetColumnValue(i, 0)
            strTmp = destTab.GetLinesByColumnValue(0, strUrno, 0)
            If Len(strTmp) > 0 Then
                llLine = GetRealItem(strTmp, 0, ",")
                For j = 1 To ItemCount(strSrcTabFields, ",") - 1
                    idxCol = GetRealItemNo(destTab.HeaderString, GetRealItem(strSrcTabFields, CInt(j), ","))
                    destTab.SetColumnValue llLine, idxCol, srcTab.GetColumnValue(i, j)
                Next j
            End If
            'strUrno = "'" + strUrno + "'"
            strData = strUrno + ","
            For j = 1 To ItemCount(strSrcTabFields, ",") - 1
                strData = strData + srcTab.GetColumnValue(i, j) + ","
            Next j
            strUpdateString = strUpdateString + strData + strUrno + vbLf
            counter = counter + 1
            ilNoUpdates = ilNoUpdates + 1
        End If
        If counter > 50 Then
            PerformSave strDBTable, strSrcTabFields, strUpdateString, strDeleteString, ilNoUpdates, ilNoDeletes
            counter = 0
            ilNoUpdates = 0
            ilNoDeletes = 0
            strInsertString = strIRT + strSrcTabFields + vbLf
            strUpdateString = strURT + strSrcTabFields + ",[URNO=:VURNO]" + vbLf
            strDeleteString = strDRT + ",[URNO=:VURNO]" + vbLf
        End If
    Next i
    PerformSave strDBTable, strSrcTabFields, strUpdateString, strDeleteString, ilNoUpdates, ilNoDeletes
    'MsgBox frmHiddenData.Ufis.LastErrorMessage
    '-------------------------------
    ' delete from local memory
    strLineNos = destTab.GetLinesByColumnValues(0, strDeletedUrnoList, 0)
    For i = 0 To ItemCount(strLineNos, ",") - 1
        llVal = GetRealItem(strLineNos, CInt(i), ",")
        destTab.DeleteLine llVal
    Next i
    destTab.RedrawTab
    lblCounter = CStr(destTab.GetLineCount())
End Function


