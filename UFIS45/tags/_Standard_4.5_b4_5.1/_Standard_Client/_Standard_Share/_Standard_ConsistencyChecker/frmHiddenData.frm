VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{A2F31E92-C74F-11D3-A251-00500437F607}#1.0#0"; "UfisCom.ocx"
Begin VB.Form frmHiddenData 
   Caption         =   "Form1"
   ClientHeight    =   10740
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9930
   LinkTopic       =   "Form1"
   ScaleHeight     =   10740
   ScaleWidth      =   9930
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin TABLib.TAB tabSetup 
      Height          =   975
      Left            =   4860
      TabIndex        =   24
      Top             =   180
      Width           =   5055
      _Version        =   65536
      _ExtentX        =   8916
      _ExtentY        =   1720
      _StockProps     =   0
   End
   Begin UFISCOMLib.UfisCom Ufis 
      Left            =   4080
      Top             =   0
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   556
      _StockProps     =   0
   End
   Begin TABLib.TAB tabData 
      Height          =   840
      Index           =   0
      Left            =   60
      TabIndex        =   0
      Tag             =   "ALTTAB;URNO,CDAT,USEC,LSTU,PRFL,USEU,ALC2,ALC3,ALFN,CASH,VAFR,VATO,HOPO"
      Top             =   300
      Width           =   4695
      _Version        =   65536
      _ExtentX        =   8281
      _ExtentY        =   1482
      _StockProps     =   0
   End
   Begin TABLib.TAB tabData 
      Height          =   840
      Index           =   1
      Left            =   60
      TabIndex        =   2
      Tag             =   "ACTTAB;URNO,ACT3,ACT5,ACTI,ACFN,ACWS,ACLE,ACHE,VAFR,VATO,HOPO"
      Top             =   1380
      Width           =   4695
      _Version        =   65536
      _ExtentX        =   8281
      _ExtentY        =   1482
      _StockProps     =   0
   End
   Begin TABLib.TAB tabData 
      Height          =   840
      Index           =   2
      Left            =   60
      TabIndex        =   4
      Tag             =   "GATTAB;URNO,BNAF,BNAT,CDAT,GNAM,LSTU,NAFR,NATO,NOBR,RGA1,RGA2,TELE,USEC,USEU,VAFR,VATO,RESN,RESB,RBAB,BUSG,TERM,GATR,HOPO"
      Top             =   2460
      Width           =   4695
      _Version        =   65536
      _ExtentX        =   8281
      _ExtentY        =   1482
      _StockProps     =   0
   End
   Begin TABLib.TAB tabData 
      Height          =   840
      Index           =   3
      Left            =   60
      TabIndex        =   6
      Tag             =   "PSTTAB;URNO,ACUS,CDAT,FULS,GPUS,LSTU,NAFR,NATO,PCAS,PNAM,PRFL,PUBK,TAXI,TELE,USEC,USEU,VAFR,VATO,RESN,DGSS,ACGR,POSR,HOPO"
      Top             =   3540
      Width           =   4695
      _Version        =   65536
      _ExtentX        =   8281
      _ExtentY        =   1482
      _StockProps     =   0
   End
   Begin TABLib.TAB tabData 
      Height          =   840
      Index           =   4
      Left            =   60
      TabIndex        =   8
      Tag             =   "NATTAB;URNO,CDAT,LSTU,TNAM,TTYP,USEC,USEU,VAFR,ALGA,ALPO,HOPO"
      Top             =   4620
      Width           =   4695
      _Version        =   65536
      _ExtentX        =   8281
      _ExtentY        =   1482
      _StockProps     =   0
   End
   Begin TABLib.TAB tabData 
      Height          =   840
      Index           =   5
      Left            =   60
      TabIndex        =   10
      Tag             =   "APTTAB;URNO,APC3,APC4,APFN,LAND,ETOF,TICH,TDI1,TDI2,APSN,CDAT,USEC,LSTU,PRFL,USEU,VAFR,VATO,HOPO"
      Top             =   5700
      Width           =   4695
      _Version        =   65536
      _ExtentX        =   8281
      _ExtentY        =   1482
      _StockProps     =   0
   End
   Begin TABLib.TAB tabData 
      Height          =   840
      Index           =   6
      Left            =   60
      TabIndex        =   18
      Tag             =   "BLTTAB;STAT,HOPO,VATO,VAFR,USEC,URNO,TELE,RESN,NATO,NAFR,CDAT,BNAM,BLTT,USEU,TERM,LSTU,MAXF,DEFD"
      Top             =   6780
      Width           =   4695
      _Version        =   65536
      _ExtentX        =   8281
      _ExtentY        =   1482
      _StockProps     =   0
   End
   Begin TABLib.TAB tabData 
      Height          =   840
      Index           =   7
      Left            =   60
      TabIndex        =   21
      Tag             =   "ACRTAB;URNO,REGN,ACT3"
      Top             =   7860
      Width           =   4695
      _Version        =   65536
      _ExtentX        =   8281
      _ExtentY        =   1482
      _StockProps     =   0
   End
   Begin TABLib.TAB tabData 
      Height          =   840
      Index           =   8
      Left            =   60
      TabIndex        =   26
      Tag             =   "CICTAB;URNO,CNAM,VATO,VAFR,TERM"
      Top             =   8940
      Width           =   4695
      _Version        =   65536
      _ExtentX        =   8281
      _ExtentY        =   1482
      _StockProps     =   0
   End
   Begin TABLib.TAB tabData 
      Height          =   840
      Index           =   9
      Left            =   4860
      TabIndex        =   29
      Tag             =   "GRNTAB;URNO,USEU,TABN,LSTU,GRSN,GRPN,GRDS,FLDN,APPL"
      Top             =   1380
      Width           =   4695
      _Version        =   65536
      _ExtentX        =   8281
      _ExtentY        =   1482
      _StockProps     =   0
   End
   Begin TABLib.TAB tabData 
      Height          =   840
      Index           =   10
      Left            =   4860
      TabIndex        =   32
      Tag             =   "GRMTAB;URNO,VALU,GURN,USEU,LSTU"
      Top             =   2460
      Width           =   4695
      _Version        =   65536
      _ExtentX        =   8281
      _ExtentY        =   1482
      _StockProps     =   0
   End
   Begin TABLib.TAB tabData 
      Height          =   840
      Index           =   11
      Left            =   4860
      TabIndex        =   35
      Tag             =   "GHPTAB;URNO,PRNA,PRSN,ACTM,ALMA,ALMD,HTGA,HTGD,PCAA,PCAD,TRRA,TRRD,TTGA,TTGD"
      Top             =   3540
      Width           =   4695
      _Version        =   65536
      _ExtentX        =   8281
      _ExtentY        =   1482
      _StockProps     =   0
   End
   Begin VB.Label Label1 
      Caption         =   "CIC Rules"
      Height          =   255
      Index           =   11
      Left            =   4860
      TabIndex        =   37
      Top             =   3300
      Width           =   1155
   End
   Begin VB.Label lblRecCount 
      Caption         =   "Data Record Count"
      Height          =   195
      Index           =   11
      Left            =   6180
      TabIndex        =   36
      Top             =   3300
      Width           =   2595
   End
   Begin VB.Label lblRecCount 
      Caption         =   "Data Record Count"
      Height          =   195
      Index           =   10
      Left            =   6180
      TabIndex        =   34
      Top             =   2220
      Width           =   2595
   End
   Begin VB.Label Label1 
      Caption         =   "Groupmembers"
      Height          =   255
      Index           =   10
      Left            =   4860
      TabIndex        =   33
      Top             =   2220
      Width           =   1155
   End
   Begin VB.Label lblRecCount 
      Caption         =   "Data Record Count"
      Height          =   195
      Index           =   9
      Left            =   6180
      TabIndex        =   31
      Top             =   1140
      Width           =   2595
   End
   Begin VB.Label Label1 
      Caption         =   "Groupnames"
      Height          =   255
      Index           =   9
      Left            =   4860
      TabIndex        =   30
      Top             =   1140
      Width           =   1155
   End
   Begin VB.Label Label1 
      Caption         =   "Counter"
      Height          =   255
      Index           =   8
      Left            =   60
      TabIndex        =   28
      Top             =   8700
      Width           =   1155
   End
   Begin VB.Label lblRecCount 
      Caption         =   "Data Record Count"
      Height          =   195
      Index           =   8
      Left            =   1380
      TabIndex        =   27
      Top             =   8700
      Width           =   2595
   End
   Begin VB.Label Label2 
      Caption         =   "Setup"
      Height          =   195
      Left            =   4860
      TabIndex        =   25
      Top             =   0
      Width           =   2295
   End
   Begin VB.Label lblRecCount 
      Caption         =   "Data Record Count"
      Height          =   195
      Index           =   7
      Left            =   1380
      TabIndex        =   23
      Top             =   7620
      Width           =   2595
   End
   Begin VB.Label Label1 
      Caption         =   "Registrations"
      Height          =   255
      Index           =   7
      Left            =   60
      TabIndex        =   22
      Top             =   7620
      Width           =   1155
   End
   Begin VB.Label Label1 
      Caption         =   "Belts"
      Height          =   255
      Index           =   6
      Left            =   60
      TabIndex        =   20
      Top             =   6540
      Width           =   1155
   End
   Begin VB.Label lblRecCount 
      Caption         =   "Data Record Count"
      Height          =   195
      Index           =   6
      Left            =   1380
      TabIndex        =   19
      Top             =   6540
      Width           =   2595
   End
   Begin VB.Label lblRecCount 
      Caption         =   "Data Record Count"
      Height          =   195
      Index           =   5
      Left            =   1380
      TabIndex        =   17
      Top             =   5460
      Width           =   2595
   End
   Begin VB.Label lblRecCount 
      Caption         =   "Data Record Count"
      Height          =   195
      Index           =   4
      Left            =   1380
      TabIndex        =   16
      Top             =   4380
      Width           =   2595
   End
   Begin VB.Label lblRecCount 
      Caption         =   "Data Record Count"
      Height          =   195
      Index           =   3
      Left            =   1380
      TabIndex        =   15
      Top             =   3300
      Width           =   2595
   End
   Begin VB.Label lblRecCount 
      Caption         =   "Data Record Count"
      Height          =   195
      Index           =   2
      Left            =   1380
      TabIndex        =   14
      Top             =   2220
      Width           =   2595
   End
   Begin VB.Label lblRecCount 
      Caption         =   "Data Record Count"
      Height          =   195
      Index           =   1
      Left            =   1380
      TabIndex        =   13
      Top             =   1140
      Width           =   2595
   End
   Begin VB.Label lblRecCount 
      Caption         =   "Data Record Count"
      Height          =   195
      Index           =   0
      Left            =   1380
      TabIndex        =   12
      Top             =   60
      Width           =   2595
   End
   Begin VB.Label Label1 
      Caption         =   "Airports"
      Height          =   255
      Index           =   5
      Left            =   60
      TabIndex        =   11
      Top             =   5460
      Width           =   1155
   End
   Begin VB.Label Label1 
      Caption         =   "Natures"
      Height          =   255
      Index           =   4
      Left            =   60
      TabIndex        =   9
      Top             =   4380
      Width           =   1155
   End
   Begin VB.Label Label1 
      Caption         =   "Positions"
      Height          =   255
      Index           =   3
      Left            =   60
      TabIndex        =   7
      Top             =   3300
      Width           =   1155
   End
   Begin VB.Label Label1 
      Caption         =   "Gates"
      Height          =   255
      Index           =   2
      Left            =   60
      TabIndex        =   5
      Top             =   2220
      Width           =   1155
   End
   Begin VB.Label Label1 
      Caption         =   "Aircrafts"
      Height          =   255
      Index           =   1
      Left            =   60
      TabIndex        =   3
      Top             =   1140
      Width           =   1155
   End
   Begin VB.Label Label1 
      Caption         =   "Airlines"
      Height          =   255
      Index           =   0
      Left            =   60
      TabIndex        =   1
      Top             =   60
      Width           =   1155
   End
End
Attribute VB_Name = "frmHiddenData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public ALTTAB As String 'Urno,Cdat,Usec,Lstu,Prfl,Useu,Alc2,Alc3,Alfn,Cash,Vafr,Vato
Public ACTTAB As String 'Urno,Act3,Act5,Acti,Acfn,Acws,Acle,Ache,Vafr,Vato
'                          0   1    2    3    4    5    6     7    8    9   10   11   12   13   14   15   16   17   18   19   20    21
Public GATTAB As String 'Urno,Bnaf,Bnat,Cdat,Gnam,Lstu,Nafr,Nato,Nobr,Rga1,Rga2,Tele,Usec,Useu,Vafr,Vato,Resn,Resb,Rbab,Busg,Term,Gatr
Public PSTTAB As String 'Urno,Acus,Cdat,Fuls,Gpus,Lstu,Nafr,Nato,Pcas,Pnam,Prfl,Pubk,Taxi,Tele,Usec,Useu,Vafr,Vato,Resn,Dgss,Acgr,Posr
Public NATTAB As String 'Urno,Cdat,Lstu,Tnam,Ttyp,Usec,Useu,Vafr,Alga,Alpo
Public APTTAB As String 'URNO,APC3,APC4,APFN,LAND,ETOF,TICH,TDI1,TDI2,APSN,CDAT,USEC,LSTU,PRFL,USEU,VAFR,VATO
Public BLTTAB As String 'STAT,HOPO,VATO,VAFR,USEC,URNO,TELE,RESN,NATO,NAFR,CDAT,BNAM,BLTT,USEU,TERM,LSTU,MAXF,DEFD
Public ACRTAB As String 'URNO,REGN,ACT3
Public CICTAB As String 'URNO,CNAM,VATO,VAFR,TERM
Public GRNTAB As String 'URNO,USEU,TABN,LSTU,GRSN,GRPN,GRDS,FLDN,APPL
Public GRMTAB As String 'URNO,VALU,GURN,USEU,LSTU
Public GHPTAB As String 'URNO,PRNA,PRSN,ACTM,ALMA,ALMD,HTGA,HTGD,PCAA,PCAD,TRRA,TRRD,TTGA,TTGD




Private Sub Form_Load()
    Dim sServer, sHopo, sTableExt, sConnectType As String
    Dim ret As Integer
    Move 0, 0
ALTTAB = "URNO,CDAT,USEC,LSTU,PRFL,USEU,ALC2,ALC3,ALFN,CASH,VAFR,VATO,HOPO"
ACTTAB = "URNO,ACT3,ACT5,ACTI,ACFN,ACWS,ACLE,ACHE,VAFR,VATO,HOPO"
GATTAB = "URNO,BNAF,BNAT,CDAT,GNAM,LSTU,NAFR,NATO,NOBR,RGA1,RGA2,TELE,USEC,USEU,VAFR,VATO,RESN,RESB,RBAB,BUSG,TERM,GATR,HOPO"
PSTTAB = "URNO,ACUS,CDAT,FULS,GPUS,LSTU,NAFR,NATO,PCAS,PNAM,PRFL,PUBK,TAXI,TELE,USEC,USEU,VAFR,VATO,RESN,DGSS,ACGR,POSR,HOPO"
NATTAB = "URNO,CDAT,LSTU,TNAM,TTYP,USEC,USEU,VAFR,ALGA,ALPO,HOPO"
APTTAB = "URNO,APC3,APC4,APFN,LAND,ETOF,TICH,TDI1,TDI2,APSN,CDAT,USEC,LSTU,PRFL,USEU,VAFR,VATO"
BLTTAB = "STAT,HOPO,VATO,VAFR,USEC,URNO,TELE,RESN,NATO,NAFR,CDAT,BNAM,BLTT,USEU,TERM,LSTU,MAXF,DEFD"
CICTAB = "URNO,CNAM,VATO,VAFR,TERM"
ACRTAB = "URNO,REGN,ACT3"
GRNTAB = "URNO,USEU,TABN,LSTU,GRSN,GRPN,GRDS,FLDN,APPL"
GRMTAB = "URNO,VALU,GURN,USEU,LSTU"
GHPTAB = "URNO,PRNA,PRSN,ACTM,ALMA,ALMD,HTGA,HTGD,PCAA,PCAD,TRRA,TRRD,TTGA,TTGD"

llIdxALTTAB = 0
llIdxACTTAB = 1
llIdxGATTAB = 2
llIdxPSTTAB = 3
llIdxNATTAB = 4
llIdxAPTTAB = 5
llIdxBLTTAB = 6
llIdxACRTAB = 7
llIdxCICTAB = 8
llIdxGRNTAB = 9
llIdxGRMTAB = 10
llIdxGHPTAB = 11

'    sServer = "tonga"
'    sHopo = "ATH"
'    sTableExt = "TAB"
    sServer = strServer
    sHopo = strHOPO
    sTableExt = "TAB"
    sConnectType = "CEDA"
    Ufis.CleanupCom
    ret = Ufis.InitCom(sServer, sConnectType)
    ret = Ufis.SetCedaPerameters("XXX", sHopo, sTableExt)
    Ufis.Twe = strHOPO + "," + "TAB," + "Consistency Checker"

    tabSetup.ResetContent
    tabSetup.HeaderLengthString = "40,40,30,30,80,80"
    tabSetup.HeaderString = "Table,Fields,Unique,Blank,Exclude Char,Min.Len."
    tabSetup.SetColumnBoolProperty 2, "Y", "N"
    tabSetup.SetColumnBoolProperty 3, "Y", "N"
    tabSetup.ShowRowSelection = False
    
'    tabSetup.InsertTextLine "ACT,ACT3,Y,N", False
'    tabSetup.InsertTextLine "ACT,ACT5,N,Y", False
'    tabSetup.InsertTextLine "ALT,ALC2,Y,Y", False
'    tabSetup.InsertTextLine "ALT,ALC3,N,Y", False
    tabSetup.InsertTextLine "ACR,REGN,Y,N,-,4", False
    tabSetup.InsertTextLine "ACR,ACT3,Y,N", False
'    tabSetup.InsertTextLine "APT,APC3,Y,N", False
'    tabSetup.InsertTextLine "APT,APC4,Y,N", False
'    tabSetup.InsertTextLine "BLT,BNAM,Y,N", False
'    tabSetup.InsertTextLine "PST,PNAM,Y,N", False
'    tabSetup.InsertTextLine "GAT,GNAM,Y,N", False
'    tabSetup.InsertTextLine "CIC,CNAM,Y,N", False
'    tabSetup.InsertTextLine "NAT,TTYP,Y,N", False

    
    tabSetup.RedrawTab

End Sub

Public Sub LoadTabData()
    Dim myTab As TABLib.TAB
    Dim ilTabCnt As Integer
    Dim arr() As String
    Dim arrFields() As String
    Dim ilCount As Integer
    Dim i As Integer
    
    ilTabCnt = 0
    For Each myTab In tabData
        arr = Split(myTab.Tag, ";")
        arrFields = Split(arr(1), ",")
        myTab.ResetContent
        myTab.AutoSizeByHeader = True
        myTab.HeaderString = ""
        myTab.HeaderLengthString = ""
        myTab.HeaderString = arr(1)
        myTab.EnableInlineEdit True
        For i = 0 To UBound(arrFields)
            If i = 0 Then
                myTab.HeaderLengthString = "80,"
            Else
                myTab.HeaderLengthString = myTab.HeaderLengthString + "80,"
            End If
        Next i
        myTab.CedaServerName = strServer
        myTab.CedaPort = "3357"
        myTab.CedaHopo = strHOPO
        myTab.CedaCurrentApplication = "Test"
        myTab.CedaTabext = "TAB"
        myTab.CedaUser = "MWO"
        myTab.CedaWorkstation = "wks104"
        myTab.CedaSendTimeout = "3"
        myTab.CedaReceiveTimeout = "240"
        myTab.CedaRecordSeparator = Chr(10)
        myTab.CedaIdentifier = "Test_App"
        myTab.ShowHorzScroller True
        myTab.EnableHeaderSizing True
        frmMain.MainStatusBar.Panels(1).Text = "Loading: " + arr(0) + " ..."
        'If arr(0) <> "ACRTAB" Then
            myTab.CedaAction "RT", arr(0), arr(1), "", ""
        'End If
        myTab.RedrawTab
        lblRecCount(ilTabCnt) = "Table: " + arr(0) + "  Records: " + CStr(myTab.GetLineCount)
        
        ilTabCnt = ilTabCnt + 1
        myTab.AutoSizeColumns
    Next
    tabData(3).Sort 9, True, True
    tabData(2).Sort 4, True, True
    tabData(4).Sort 4, True, True
    tabData(0).Sort 7, True, True
    tabData(7).Sort 1, True, True
    tabData(9).ColumnWidthString = "10,32,7,14,60,30,60,10,10"
    tabData(9).Sort 0, True, True
    tabData(10).ColumnWidthString = "10,10,10,40,14"
    tabData(10).Sort 2, True, True
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Ufis.CleanupCom
End Sub
