/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * 
 *	UFISAppMang:	UFIS Client Application Manager Project
 *
 *	Proxy Communicator for Client Applications
 *	
 *	ABB Airport Technologies GmbH 2000
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 *		Author			Date			Comment
 *	------------------------------------------------------------------
 *
 *		cla				02/09/2000		Initial version
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
// ClntCon.h: Schnittstelle f�r die Klasse CClntCon.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CLNTCON_H__E2232CB6_80C8_11D4_81C3_00805FADA006__INCLUDED_)
#define AFX_CLNTCON_H__E2232CB6_80C8_11D4_81C3_00805FADA006__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <ClntTypes.h>

class CClntCon  
{
public:

	IUnknown* m_pUnk;
	ClntTags m_appID;
	DWORD m_cookie;

	CClntCon();
	CClntCon(IUnknown* pUnkSink,DWORD pdwCookie,ClntTags tag);

	// Copy Constructor

	CClntCon(const CClntCon& rhs);
    
	/*
	 * operator needed for the STL container
	 */

	CClntCon& operator=(const CClntCon& rhs);
	bool operator==(const CClntCon& rhs)const;
	bool operator==(const DWORD cookie)const;

	virtual ~CClntCon();

};

#endif // !defined(AFX_CLNTCON_H__E2232CB6_80C8_11D4_81C3_00805FADA006__INCLUDED_)
