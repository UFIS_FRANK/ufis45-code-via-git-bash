﻿<ResourceDictionary 
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    xmlns:local="clr-namespace:Ufis.Resources">

    <!-- Shell background -->
    <LinearGradientBrush x:Key="{ComponentResourceKey TypeInTargetAssembly={x:Type local:SharedResources}, ResourceId=ShellBackground}"
                         StartPoint="0,0" EndPoint="0,1">
        <GradientBrush.GradientStops>
            <GradientStopCollection>
                <GradientStop Offset="0.0" Color="#DEE1E7"/>
                <GradientStop Offset="0.5" Color="#103C87"/>
                <GradientStop Offset="1.0" Color="#DEE1E7"/>
            </GradientStopCollection>
        </GradientBrush.GradientStops>
    </LinearGradientBrush>

    <!-- UFIS Logo Template -->
    <DataTemplate x:Key="{ComponentResourceKey TypeInTargetAssembly={x:Type local:SharedResources}, ResourceId=UFISLogoTemplate}">
        <StackPanel Margin="20,20,20,0">
            <StackPanel Orientation="Horizontal">
                <Image Source="{DynamicResource {x:Static local:SharedImages.UFIS48}}" Stretch="None" HorizontalAlignment="Left"/>
                <TextBlock Text="Airport Solutions" Style="{DynamicResource {x:Static local:SharedResources.AiportSolutionsStyle}}"
                                   Margin="-10,0,0,0" VerticalAlignment="Bottom"/>
            </StackPanel>
        </StackPanel>
    </DataTemplate>

    <!-- Shell button text -->
    <Style x:Key="{ComponentResourceKey TypeInTargetAssembly={x:Type local:SharedResources}, ResourceId=ShellButtonTextStyle}" 
           TargetType="TextBlock">
        <Setter Property="Foreground" Value="MidnightBlue"/>
        <Setter Property="FontSize" Value="14"/>
        <Setter Property="FontWeight" Value="Bold"/>
    </Style>

    <!-- Data template for MessageInfo -->
    <DataTemplate x:Key="{ComponentResourceKey TypeInTargetAssembly={x:Type local:SharedResources}, ResourceId=MessageInfoTemplate}">
        <Grid>
            <Grid.ColumnDefinitions>
                <ColumnDefinition Width="Auto"/>
                <ColumnDefinition Name="MessageTextColumn"/>
            </Grid.ColumnDefinitions>
            
            <Image Grid.Column="0" Margin="2,10,0,0" Height="32" VerticalAlignment="Top">
                <Image.Style>
                    <Style TargetType="{x:Type Image}">
                        <Style.Triggers>
                            <DataTrigger Binding="{Binding Path=Severity}" Value="Error">
                                <Setter Property="Source" Value="{DynamicResource {x:Static local:SharedImages.Error32}}"/>
                            </DataTrigger>
                            <DataTrigger Binding="{Binding Path=Severity}" Value="Warning">
                                <Setter Property="Source" Value="{DynamicResource {x:Static local:SharedImages.Warning32}}"/>
                            </DataTrigger>
                            <DataTrigger Binding="{Binding Path=Severity}" Value="Information">
                                <Setter Property="Source" Value="{DynamicResource {x:Static local:SharedImages.Info32}}"/>
                            </DataTrigger>
                            <DataTrigger Binding="{Binding Path=Severity}" Value="OK">
                                <Setter Property="Source" Value="{DynamicResource {x:Static local:SharedImages.OK32}}"/>
                            </DataTrigger>
                        </Style.Triggers>
                    </Style>
                </Image.Style>
            </Image>
            <TextBlock Grid.Column="1" Text="{Binding Path=InfoText}" Margin="5,8" 
                       FontWeight="Bold" TextWrapping="WrapWithOverflow" FontSize="14">
                <TextBlock.Style>
                    <Style TargetType="{x:Type TextBlock}">
                        <Style.Triggers>
                            <DataTrigger Binding="{Binding Path=Severity}" Value="Error">
                                <Setter Property="Foreground" Value="Maroon"/>
                            </DataTrigger>
                            <DataTrigger Binding="{Binding Path=Severity}" Value="Warning">
                                <Setter Property="Foreground" Value="DarkOrange"/>
                            </DataTrigger>
                            <DataTrigger Binding="{Binding Path=Severity}" Value="Information">
                                <Setter Property="Foreground" Value="LightSteelBlue"/>
                            </DataTrigger>
                            <DataTrigger Binding="{Binding Path=Severity}" Value="OK">
                                <Setter Property="Foreground" Value="#8CB72B"/>
                            </DataTrigger>
                        </Style.Triggers>
                    </Style>
                </TextBlock.Style>
            </TextBlock>
        </Grid>
    </DataTemplate>

    <!-- Error background -->
    <LinearGradientBrush x:Key="ErrorBackground"
                         StartPoint="0,0" EndPoint="0,1">
        <GradientBrush.GradientStops>
            <GradientStopCollection>
                <GradientStop Offset="0.0" Color="MistyRose"/>
                <GradientStop Offset="1.0" Color="Snow"/>
            </GradientStopCollection>
        </GradientBrush.GradientStops>
    </LinearGradientBrush>

    <!-- Warning background -->
    <LinearGradientBrush x:Key="WarningBackground"
                         StartPoint="0,0" EndPoint="0,1">
        <GradientBrush.GradientStops>
            <GradientStopCollection>
                <GradientStop Offset="0.0" Color="LemonChiffon"/>
                <GradientStop Offset="1.0" Color="Snow"/>
            </GradientStopCollection>
        </GradientBrush.GradientStops>
    </LinearGradientBrush>

    <!-- Information background -->
    <LinearGradientBrush x:Key="InformationBackground"
                         StartPoint="0,0" EndPoint="0,1">
        <GradientBrush.GradientStops>
            <GradientStopCollection>
                <GradientStop Offset="0.0" Color="Lavender"/>
                <GradientStop Offset="1.0" Color="Snow"/>
            </GradientStopCollection>
        </GradientBrush.GradientStops>
    </LinearGradientBrush>

    <!-- OK background -->
    <LinearGradientBrush x:Key="OKBackground"
                         StartPoint="0,0" EndPoint="0,1">
        <GradientBrush.GradientStops>
            <GradientStopCollection>
                <GradientStop Offset="0.0" Color="LightGreen"/>
                <GradientStop Offset="1.0" Color="Snow"/>
            </GradientStopCollection>
        </GradientBrush.GradientStops>
    </LinearGradientBrush>

    <!-- Airport solutions text -->
    <Style x:Key="{ComponentResourceKey TypeInTargetAssembly={x:Type local:SharedResources}, ResourceId=AiportSolutionsStyle}" 
           TargetType="TextBlock">
        <Setter Property="Foreground" Value="DimGray"/>
        <Setter Property="FontSize" Value="14"/>
        <Setter Property="FontFamily" Value="Tahoma"/>
        <Setter Property="FontWeight" Value="Bold"/>
    </Style>

    <!-- Mandatory field background -->
    <LinearGradientBrush x:Key="{ComponentResourceKey TypeInTargetAssembly={x:Type local:SharedResources}, ResourceId=MandatoryFieldBackground}"
                         StartPoint="0,0" EndPoint="1,0">
        <GradientBrush.GradientStops>
            <GradientStopCollection>
                <GradientStop Offset="0.0" Color="LemonChiffon"/>
                <GradientStop Offset="0.3" Color="Yellow"/>
                <GradientStop Offset="1.0" Color="Wheat"/>
            </GradientStopCollection>
        </GradientBrush.GradientStops>
    </LinearGradientBrush>

    <!-- ReadOnly field background -->
    <LinearGradientBrush x:Key="{ComponentResourceKey TypeInTargetAssembly={x:Type local:SharedResources}, ResourceId=ReadOnlyFieldBackground}"
                         StartPoint="0,0" EndPoint="1,0">
        <GradientBrush.GradientStops>
            <GradientStopCollection>
                <GradientStop Offset="0.0" Color="WhiteSmoke"/>
                <GradientStop Offset="0.3" Color="Gainsboro"/>
                <GradientStop Offset="1.0" Color="Silver"/>
            </GradientStopCollection>
        </GradientBrush.GradientStops>
    </LinearGradientBrush>

    <local:SortIndexToTextConverter x:Key="SortIndexToTextConverter"/>
    
    <!-- Template for displaying the sort index in column header -->
    <DataTemplate x:Key="{ComponentResourceKey TypeInTargetAssembly={x:Type local:SharedResources}, ResourceId=SortIndexInColumnHeaderTemplate}">
        <TextBlock FontSize="10">        
            <TextBlock.Text>
                <MultiBinding Converter="{StaticResource SortIndexToTextConverter}">
                    <Binding Path="Column.View.DataControl"/>
                    <Binding Path="Column.SortIndex"/>
                </MultiBinding>
            </TextBlock.Text>
        </TextBlock>
    </DataTemplate>
</ResourceDictionary>