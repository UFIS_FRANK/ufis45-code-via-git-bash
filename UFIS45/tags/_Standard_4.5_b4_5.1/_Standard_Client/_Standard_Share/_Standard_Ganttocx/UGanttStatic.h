#if !defined(AFX_UGANTTSTATIC_H__6B43D0E3_A9BE_11D4_BFE3_0001022205E4__INCLUDED_)
#define AFX_UGANTTSTATIC_H__6B43D0E3_A9BE_11D4_BFE3_0001022205E4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// UGanttStatic.h : header file
//
/////////////////////////////////////////////////////////////////////////////
// CUGanttStatic window

class CUGanttStatic : public CStatic
{
// Construction
public:
	CUGanttStatic();
 
// Attributes
public:
// Operations
public:
  // A time member (used for TimeMarkers to remember the exact position)
  COleDateTime omTime;

  // The background color
  COLORREF omBkColor;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUGanttStatic)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CUGanttStatic();
	void MyDraw(CDC* pdc, CRect opRect);


	// Generated message map functions
protected:
	//{{AFX_MSG(CUGanttStatic)
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UGANTTSTATIC_H__6B43D0E3_A9BE_11D4_BFE3_0001022205E4__INCLUDED_)
