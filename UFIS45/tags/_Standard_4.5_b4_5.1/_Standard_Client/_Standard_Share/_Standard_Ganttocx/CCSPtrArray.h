#ifndef _CCSPTRARR
#define _CCSPTRARR


//#include "stdafx.h"
#include <afxwin.h>

template <class TYPE>
class CCSPtrArray: public CPtrArray
{
// These methods are borrowed from the MFC's CTypedPtrArray
public:
	TYPE &ElementAt(int n) { return *(TYPE *)CPtrArray::ElementAt(n); }
	TYPE GetAt(int n) const { return *(TYPE *)CPtrArray::GetAt(n); }
	TYPE &operator [](int n) { return *(TYPE *)CPtrArray::operator [](n); }
	TYPE operator [](int n) const { return *(TYPE *)CPtrArray::operator [](n); }

// Additional contructors
// This constructor was added to allow copying from one array to another
public:
	CCSPtrArray<TYPE>() {}
	CCSPtrArray<TYPE>(const CCSPtrArray<TYPE> &array) {
		int n = array.GetSize(); SetSize(n);
		for (int i = 0; i < n; i++)
			CPtrArray::ElementAt(i) = (void *)((CPtrArray *)&array)->ElementAt(i);
	}

// This constructor was added to allow copying from one array to another
public:
	const CCSPtrArray<TYPE> &operator =(const CCSPtrArray<TYPE> &array) {
		int n = array.GetSize(); SetSize(n);
		for (int i = 0; i < n; i++)
			CPtrArray::ElementAt(i) = (void *)((CPtrArray *)&array)->ElementAt(i);
		return *this;
	}

// Additional methods for allocating and freeing elements in the array
// To help you use CCSPtrArray without dealing with memory allocation directly,
// these methods are provided:
//
//	New			Allocate a new member at the end of array -- replace MFC Add()
//	NewAt		Allocate a new member at the specified position -- replace MFC InsertAt()
//	DeleteAt	Deallocate a member at the specified position -- replace MFC RemoveAt()
//	DeleteAll	Deallocate every members from the array -- replace MFC RemoveAll()
//
public:
	int New() { return Add(new TYPE); }
	int New(const TYPE &value) { int i = Add(new TYPE); this->ElementAt(i) = value; return i; }
	void NewAt(int i) { InsertAt(i, new TYPE); }
	void NewAt(int i, const TYPE &value) { InsertAt(i, new TYPE); this->ElementAt(i) = value; }
	void DeleteAt(int i) 
	{
		delete &this->ElementAt(i); 
		RemoveAt(i); 
	}
	//void DeleteAll() { for (int i = 0; i < GetSize(); i++) delete &this->ElementAt(i); RemoveAll(); }
	void DeleteAll() { for (int i = GetSize()-1; i >= 0; i--) delete &this->ElementAt(i); RemoveAll(); }
	// Additional methods for sorting the array
public:
	void Sort(int (*compare)(const TYPE **elem1, const TYPE **elem2)) {
		qsort(m_pData, GetSize(), sizeof(void *), (int (*)(const void *, const void *))compare);
		// have to use the m_pData for qsort()
	}
};

#endif
