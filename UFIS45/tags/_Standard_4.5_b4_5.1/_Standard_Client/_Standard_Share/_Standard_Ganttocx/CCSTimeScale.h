// CCSTimeScale.h : header file
//

#ifndef _CCSTimeScale_
#define _CCSTimeScale_

#include <afxwin.h>
//#include "CCSDefines.h"
#include <ccsptrarray.h>
//#include "CCSTime.h"
/////////////////////////////////////////////////////////////////////////////
// CTopScaleIndicator class


/////////////////////////////////////////////////////////////////////////////
// Class declaration of CTopScaleIndicator

//@Man:
//@Memo: Baseclass
/*@Doc:
  No comment on this up to now.
*/



class CTopScaleIndicator
{
public:
    //@ManMemo: lmColor
    COLORREF lmColor;
    //@ManMemo: omStart and omEnd
    COleDateTime omStart, omEnd;
};

/////////////////////////////////////////////////////////////////////////////
// CCSTimeScale window

////////////////////////////////////////////////////////////////////////
// RST 15/07/97:
//	
//@Memo: WM_TSCALE_MARKERMOVED
#define WM_TSCALE_MARKERMOVED        (WM_USER + 510)	// lParam is the MarkerID
														// wParam is the new Time
//@Memo: WM_TSCALE_MARKERSELECT
#define WM_TSCALE_MARKERSELECT       (WM_USER + 511)	// lParam is the selected MarkerID or NULL
														// wParam is the Time

/////////////////////////////////////////////////////////////////////////////
// Class declaration of CCSTimeScale

//@Man:
//@Memo: Baseclass
/*@Doc:
  No comment on this up to now.
*/


enum {TSM_UP, TSM_LEFT, TSM_RIGHT};

class CCSTimeScale : public CWnd
{
// Construction
public:
    //@ManMemo: Default constructor
    CCSTimeScale(CWnd *popParent = NULL);

// Attributes
public:
// Operations
	//MWO: 25.06.03
	int lfTimeScale_ColorIdx;
	int lfTimeScale_Percent;
	BOOL lfTimeScale_StyleUp;
	int lfTimeScale_Type;

	BOOL m_lifeStyle;
	void SetLifeStyle(int pTimeScale_ColorIdx, int pTimeScale_Percent, BOOL pTimeScale_StyleUp, int pTimeScale_Type);
	void DrawLifeStyle(CDC *pdc, CRect opRect, int MyColor, int ipMode, BOOL DrawDown, int ipPercent);
	//END MWO: 25.06.03
public:
    //@ManMemo: SetDisplayStartTime
    void SetDisplayStartTime(COleDateTime opDisplayStart);
    //@ManMemo: SetTimeInterval
    void SetTimeInterval(COleDateTimeSpan opInterval);
    //@ManMemo: SetDisplayTimeFrame
    void SetDisplayTimeFrame(COleDateTime opDisplayStart, COleDateTimeSpan opDuration, COleDateTimeSpan opInterval);
    //@ManMemo: GetDisplayDuration
    COleDateTimeSpan GetDisplayDuration(void);
    //@ManMemo: GetDisplayStartTime
    COleDateTime GetDisplayStartTime(void);
////////////////////////////////////////////////////////////////////////
// Damkerng 06/27/96:
// This block of code is used for make the TimeScale be able to help
// the other classes which desire to calculate the time backward from the
// given point.
    //@ManMemo: GetTimeFromX
    /*@ManDoc: 
	  This block of code is used for 
	  make the TimeScale be able to help
      the other classes which desire to 
	  calculate the time backward from the
      given point.
    */
	COleDateTime GetTimeFromX(int ipX);
////////////////////////////////////////////////////////////////////////
    //@ManMemo: GetXFromTime
    int GetXFromTime(COleDateTime opTime);

    //@ManMemo: GetXFromTime
	void UpdateCurrentTimeLine(COleDateTime opTime);
    
    //@ManMemo: AddTopScaleIndicator
    void AddTopScaleIndicator(CTopScaleIndicator *popTSI) { omTSIArray.Add(popTSI); };
    //@ManMemo: AddTopScaleIndicator
    void AddTopScaleIndicator(COleDateTime opStartTime, COleDateTime opEndTime, COLORREF lpColor);
    //@ManMemo: DisplayTopScaleIndicator
    void DisplayTopScaleIndicator(CDC *popDC);
    //@ManMemo: DisplayTopScaleIndicator
    void DisplayTopScaleIndicator(void);
    //@ManMemo: RemoveAllTopScaleIndicator
    void RemoveAllTopScaleIndicator(void);
	//@ManMemo: EnableDisplayCurrentTime
	void EnableDisplayCurrentTime(bool bpDisplayCurrentTime);
////////////////////////////////////////////////////////////////////////
// RST 07/15/97:
// This block of code is used to add and change Marker in the TimeScale 
//	
    //@ManMemo: Marker
    /*@ManDoc:
	  Marker: a colored triangle which 'marks' a time or a range 
	  of time in the timescale.
	  This block of code is used to add 
	  and change Marker in the TimeScale.
    */

    //@ManMemo: enum Markeralignment: TSM_UP, TSM_LEFT, TSM_RIGHT
	enum {TSM_UP, TSM_LEFT, TSM_RIGHT};

    //@ManMemo: AddMarker
	void AddMarker(int ipID, COleDateTime opTime, 
				   COLORREF opColor = RGB(255,0,0), 
				   int ipRasterWidth = 15, 
				   int ipAlign = TSM_UP,
				   int ipIDAssign = 0,
				   COleDateTime opRangeFrom = COleDateTime(), 
				   COleDateTime opRangeTo = COleDateTime(),
				   CString opToolTipText = "");
    
    //@ManMemo: DeleteMarker
	void DeleteMarker(int ipID);
    //@ManMemo: DeleteAllMarker
	void DeleteAllMarker();
    //@ManMemo: ChangeMarkerRange
    bool ChangeMarkerRange(int ipID, COleDateTime opNewTime, COleDateTime opRangeFrom, COleDateTime opRangeTo);
    //@ManMemo: ChangeMarkerColor
    bool ChangeMarkerColor(int ipID, COLORREF opColor);
    //@ManMemo: ChangeMarkerPos
    bool ChangeMarkerPos(int ipID, COleDateTime opTime);
    //@ManMemo: SetMarkerRaster
	void SetMarkerRaster(int ipID, int ipRasterWidth);
////////////////////////////////////////////////////////////////////////
    
// Implementation
public:
    //@ManMemo: Default destructor
    virtual ~CCSTimeScale();
	void MyDraw( CDC* pdc );
protected:
	
protected:
    // Generated message map functions
    //{{AFX_MSG(CCSTimeScale)
    afx_msg void OnPaint();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
    DECLARE_MESSAGE_MAP()
	//afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
    
    //@ManMemo: TSIPos
    bool TSIPos(int *ipLeft, int *ipRight);

protected:
	
////////////////////////////////////////////////////////////////////////
// RST 15/07/97:
// This block of code includes the member to handle the Marker in the TimeScale 
//	see also the new windowmessages at the top of the headerfile
    //@ManMemo:  struct MARKER
	struct MARKER
	{
		int ID;
		COleDateTime ActTime;
		COleDateTime RangeFrom;
		COleDateTime RangeTo;
		int Assign;
		int Align;
		COLORREF Color;
		int	  RasterWidth;
		CString ToolTipText;
		MARKER(){;};
	};
	//MWO:21.08.2002
	CBitmap		bmpArea;
    //@ManMemo: omMarker
    CCSPtrArray<MARKER> omMarker;
	//@ManMemo: *prmCurrMarker 
	MARKER *prmCurrMarker;
    //@ManMemo: bmLButtonDown
	bool bmLButtonDown;
    //@ManMemo: pomStaticToolTip
	CStatic *pomStaticToolTip;
    //@ManMemo: TestMarkerAssignPos
	bool TestMarkerAssignPos(MARKER *prpMarker, COleDateTime opNewTime);
    //@ManMemo: pomParent
	CWnd *pomParent;
////////////////////////////////////////////////////////////////////////

	//@ManMemo: omDisplayStart
    COleDateTime omDisplayStart;
    //@ManMemo: omInterval
    COleDateTimeSpan omInterval;
    //@ManMemo: fmIntervalWidth
    double fmIntervalWidth;
    
    //@ManMemo: imP0
    int imP0;
    //@ManMemo: imP1
    int imP1;
    //@ManMemo: imP2
    int imP2;
    //@ManMemo: imP3
    int imP3;
    
    //@ManMemo: omOldCurrentTime
    COleDateTime omOldCurrentTime;
    //@ManMemo: omCurrentTime
	COleDateTime omCurrentTime;
    //@ManMemo: omTSIArray
    CPtrArray omTSIArray;
    //@ManMemo: bmDisplayCurrentTime
	bool bmDisplayCurrentTime;

public:
    CFont *pomFont;
	CFont *pomLifeStyleFont;
    COLORREF lmBkColor;
    COLORREF lmTextColor;
    COLORREF lmHilightColor;
};

/////////////////////////////////////////////////////////////////////////////

#endif // _CCSTimeScale_

