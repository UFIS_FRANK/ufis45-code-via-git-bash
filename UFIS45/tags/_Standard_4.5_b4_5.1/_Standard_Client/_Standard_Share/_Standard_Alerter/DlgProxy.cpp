// DlgProxy.cpp : implementation file
//

#include <stdafx.h>
#include <Alerter.h>
#include <DlgProxy.h>
#include <AlerterDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAlerterDlgAutoProxy

IMPLEMENT_DYNCREATE(CAlerterDlgAutoProxy, CCmdTarget)

CAlerterDlgAutoProxy::CAlerterDlgAutoProxy()
{
	EnableAutomation();
	
	// To keep the application running as long as an automation 
	//	object is active, the constructor calls AfxOleLockApp.
	AfxOleLockApp();

	// Get access to the dialog through the application's
	//  main window pointer.  Set the proxy's internal pointer
	//  to point to the dialog, and set the dialog's back pointer to
	//  this proxy.
	ASSERT (AfxGetApp()->m_pMainWnd != NULL);
	ASSERT_VALID (AfxGetApp()->m_pMainWnd);
	ASSERT_KINDOF(CAlerterDlg, AfxGetApp()->m_pMainWnd);
	m_pDialog = (CAlerterDlg*) AfxGetApp()->m_pMainWnd;
	m_pDialog->m_pAutoProxy = this;
}

CAlerterDlgAutoProxy::~CAlerterDlgAutoProxy()
{
	// To terminate the application when all objects created with
	// 	with automation, the destructor calls AfxOleUnlockApp.
	//  Among other things, this will destroy the main dialog
	if (m_pDialog != NULL)
		m_pDialog->m_pAutoProxy = NULL;
	AfxOleUnlockApp();
}

void CAlerterDlgAutoProxy::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CCmdTarget::OnFinalRelease();
}

BEGIN_MESSAGE_MAP(CAlerterDlgAutoProxy, CCmdTarget)
	//{{AFX_MSG_MAP(CAlerterDlgAutoProxy)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CAlerterDlgAutoProxy, CCmdTarget)
	//{{AFX_DISPATCH_MAP(CAlerterDlgAutoProxy)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_DISPATCH_MAP
END_DISPATCH_MAP()

// Note: we add support for IID_IAlerter to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .ODL file.

// {409F0E55-5608-11D7-8009-00010215BFDE}
static const IID IID_IAlerter =
{ 0x409f0e55, 0x5608, 0x11d7, { 0x80, 0x9, 0x0, 0x1, 0x2, 0x15, 0xbf, 0xde } };

BEGIN_INTERFACE_MAP(CAlerterDlgAutoProxy, CCmdTarget)
	INTERFACE_PART(CAlerterDlgAutoProxy, IID_IAlerter, Dispatch)
END_INTERFACE_MAP()

// The IMPLEMENT_OLECREATE2 macro is defined in StdAfx.h of this project
// {409F0E53-5608-11D7-8009-00010215BFDE}
IMPLEMENT_OLECREATE2(CAlerterDlgAutoProxy, "Alerter.Application", 0x409f0e53, 0x5608, 0x11d7, 0x80, 0x9, 0x0, 0x1, 0x2, 0x15, 0xbf, 0xde)

/////////////////////////////////////////////////////////////////////////////
// CAlerterDlgAutoProxy message handlers
