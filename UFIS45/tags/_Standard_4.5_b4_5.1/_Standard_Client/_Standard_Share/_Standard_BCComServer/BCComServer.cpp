// BCComServer.cpp : Implementation of WinMain


// Note: Proxy/Stub Information
//      To build a separate proxy/stub DLL, 
//      run nmake -f BCComServerps.mk in the project directory.

#include "stdafx.h"
#include "resource.h"
#include <initguid.h>
#include "BCComServer.h"
#include "MonitorDlg.h"

#include "BCComServer_i.c"
#include "BCComAtl.h"

CBcComServerApp theApp;


CCSPtrArray<ProxyInstance> CBcComServerApp::ogAtlConnections;
MonitorDlg *CBcComServerApp::pogMonitorDlg;
char CBcComServerApp::sgHostName[512];
BOOL CBcComServerApp::bgApplicationHidden;

const DWORD dwTimeOut = 5000; // time for EXE to be idle before shutting down
const DWORD dwPause = 1000; // time to wait for threads to finish up

// Passed to CreateThread to monitor the shutdown event
static DWORD WINAPI MonitorProc(void* pv)
{
    CExeModule* p = (CExeModule*)pv;
    p->MonitorShutdown();
    return 0;
}

LONG CExeModule::Unlock()
{
    LONG l = CComModule::Unlock();
    if (l == 0)
    {
        bActivity = true;
        SetEvent(hEventShutdown); // tell monitor that we transitioned to zero
    }
    return l;
}

//Monitors the shutdown event
void CExeModule::MonitorShutdown()
{
    while (1)
    {
        WaitForSingleObject(hEventShutdown, INFINITE);
        DWORD dwWait=0;
        do
        {
            bActivity = false;
            dwWait = WaitForSingleObject(hEventShutdown, dwTimeOut);
        } while (dwWait == WAIT_OBJECT_0);
        // timed out
        if (!bActivity && m_nLockCnt == 0) // if no activity let's really bail
        {
#if _WIN32_WINNT >= 0x0400 & defined(_ATL_FREE_THREADED)
            CoSuspendClassObjects();
            if (!bActivity && m_nLockCnt == 0)
#endif
                break;
        }
    }
    CloseHandle(hEventShutdown);
    PostThreadMessage(dwThreadID, WM_QUIT, 0, 0);
}

bool CExeModule::StartMonitor()
{
    hEventShutdown = CreateEvent(NULL, false, false, NULL);
    if (hEventShutdown == NULL)
        return false;
    DWORD dwThreadID;
    HANDLE h = CreateThread(NULL, 0, MonitorProc, this, 0, &dwThreadID);
    return (h != NULL);
}

CExeModule _Module;

BEGIN_OBJECT_MAP(ObjectMap)
OBJECT_ENTRY(CLSID_BcComAtl, CBcComAtl)
END_OBJECT_MAP()


LPCTSTR FindOneOf(LPCTSTR p1, LPCTSTR p2)
{
    while (p1 != NULL && *p1 != NULL)
    {
        LPCTSTR p = p2;
        while (p != NULL && *p != NULL)
        {
            if (*p1 == *p)
                return CharNext(p1);
            p = CharNext(p);
        }
        p1 = CharNext(p1);
    }
    return NULL;
}

BOOL CBcComServerApp::InitInstance()
{
	// Initialize OLE libraries.
	if (!AfxOleInit())
	{
		AfxMessageBox(_T("OLE Initialization Failed!"));
		return FALSE;
	}

	if (!AfxSocketInit())
	{
		AfxMessageBox("Socket Init Failed");
		return FALSE;
	}

	// Initialize CcomModule.
	_Module.Init(ObjectMap, m_hInstance);
	_Module.dwThreadID = GetCurrentThreadId();

	// Check command line arguments.
	TCHAR szTokens[] = _T("-/");
	m_bRun = TRUE;
	LPCTSTR lpszToken = FindOneOf(m_lpCmdLine, szTokens);
	while (lpszToken != NULL)
	{
		// Register ATL and MFC class factories.
		if (lstrcmpi(lpszToken, _T("Embedding"))==0 ||
		lstrcmpi(lpszToken, _T("Automation"))==0)
		{
			AfxOleSetUserCtrl(FALSE);
			break;
		}
	// Unregister servers.
	// There is no unregistration code for MFC
	// servers. Refer to <LINK TYPE="ARTICLE" VALUE="Q186212">Q186212</LINK> "HOWTO: Unregister MFC
	// Automation Servers" for adding unregistration
	// code.
		else if (lstrcmpi(lpszToken, _T("UnregServer"))==0)
		{
			VERIFY(SUCCEEDED(_Module.UpdateRegistryFromResource(IDR_BcComServer, FALSE)));
//Replace  IDR_ServerS2B with your project specific resource ID for the registry script resource
			VERIFY(SUCCEEDED(_Module.UnregisterServer(TRUE)));
			m_bRun = FALSE;
			break;
		}
	// Register ATL and MFC objects in the registry.
		else if (lstrcmpi(lpszToken, _T("RegServer"))==0)
		{
			VERIFY(SUCCEEDED(_Module.UpdateRegistryFromResource(IDR_BcComServer, TRUE)));
			VERIFY(SUCCEEDED(_Module.RegisterServer(TRUE)));
			COleObjectFactory::UpdateRegistryAll();
			m_bRun = FALSE;
			break;
		}
		lpszToken = FindOneOf(lpszToken, szTokens);
	}
	//Read ceda.ini
	char pclConfigPath[256];
    if (getenv("CEDA") == NULL)
        strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
    else
        strcpy(pclConfigPath, getenv("CEDA"));

	char pclShowMode[128];
	CBcComServerApp::bgApplicationHidden = true;
    GetPrivateProfileString("BCCOMSERVER", "MODE", "HIDE", pclShowMode, sizeof pclShowMode, pclConfigPath);
	if(strcmp(pclShowMode, "SHOW") == 0)
	{
		CBcComServerApp::bgApplicationHidden = false;
	}

    GetPrivateProfileString("GLOBAL", "HOSTNAME", "", sgHostName, sizeof sgHostName, pclConfigPath);


	if (m_bRun)
	{
		// Comment out the next line if not using VC 6-generated
		// code.
		_Module.StartMonitor();

		VERIFY(SUCCEEDED(_Module.RegisterClassObjects(CLSCTX_LOCAL_SERVER, REGCLS_MULTIPLEUSE)));
		VERIFY(COleObjectFactory::RegisterAll());
		// To run the EXE standalone, you need to create a window
		// and assign the CWnd* to m_pMainWnd.
		LPCTSTR szClass = AfxRegisterWndClass(NULL);
		MonitorDlg olDlg;
		pogMonitorDlg = &olDlg;
		olDlg.DoModal();
		//m_pMainWnd = new CWnd;
		//m_pMainWnd->CreateEx(0, szClass, _T("SomeName"), 0, CRect(0, 0, 0, 0), NULL, 1234);
	}
	return TRUE;
}

int CBcComServerApp::ExitInstance()
{
	// MFC's class factories registration is
	// automatically revoked by MFC itself.
	if (m_bRun)
	{
		_Module.RevokeClassObjects();
        Sleep(dwPause); //wait for any threads to finish
    }
	ogAtlConnections.DeleteAll();
    _Module.Term();
	return 0;
} 
