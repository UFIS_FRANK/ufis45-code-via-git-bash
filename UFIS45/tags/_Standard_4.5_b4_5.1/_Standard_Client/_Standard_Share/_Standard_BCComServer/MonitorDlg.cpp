// MonitorDlg.cpp : implementation file
//

#include "stdafx.h"
#include "bccomserver.h"
#include "MonitorDlg.h"
#include "BcComAtl.h"
#include "ServerInfoDlg.h"
#include "BcComClientDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// MonitorDlg dialog


MonitorDlg::MonitorDlg(CWnd* pParent /*=NULL*/)
	: CDialog(MonitorDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(MonitorDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	bmMultithreaded = false;
}


void MonitorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(MonitorDlg)
	DDX_Control(pDX, IDC_CONNECTION, m_Connection);
	DDX_Control(pDX, IDC_BCLIST, m_BcList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(MonitorDlg, CDialog)
	//{{AFX_MSG_MAP(MonitorDlg)
	ON_WM_PAINT()
	ON_WM_TIMER()
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_SAVELISTAS, OnSavelistas)
	ON_WM_CLOSE()
	ON_MESSAGE(WM_THREADFIREEVENT,OnFireEventForThread) //custom handler
	ON_BN_CLICKED(IDC_BUTTON_INFO, OnButtonInfo)
	ON_BN_CLICKED(IDC_CLIENT_INFO, OnClientInfo)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// MonitorDlg message handlers

BOOL MonitorDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	if(CBcComServerApp::bgApplicationHidden == TRUE)
	{
		this->ShowWindow(SW_HIDE);
	}
	
	CString str = "Connected to: " + CString(CBcComServerApp::sgHostName);
	m_Connection.SetWindowText(str);

	this->SetTimer(0,10,NULL);

	// TODO: Add extra initialization here
	COleMessageFilter *polFilter = AfxOleGetMessageFilter();
	if (polFilter)
	{
		char pclConfigPath[256];

		if (getenv("CEDA") == NULL)
			strcpy(pclConfigPath, "C:\\UFIS\\SYSTEM\\CEDA.INI"); 
		else
			strcpy(pclConfigPath, getenv("CEDA"));


		char pclRetryReply[128];
		GetPrivateProfileString("BcComServer", "RetryReply", "10000", pclRetryReply, sizeof pclRetryReply, pclConfigPath);
		
		DWORD dwRetryReply = atol(pclRetryReply);
		polFilter->SetRetryReply(dwRetryReply);

		char pclPendingDelay[128];
		GetPrivateProfileString("BcComServer", "PendingDelay", "8000", pclPendingDelay, sizeof pclPendingDelay, pclConfigPath);

		DWORD dwPendingDelay = atol(pclPendingDelay);
		polFilter->SetMessagePendingDelay(dwPendingDelay);

		char pclBusyDialog[128];
		GetPrivateProfileString("BcComServer", "BusyDialog", "YES", pclBusyDialog, sizeof pclBusyDialog, pclConfigPath);
		if (stricmp(pclBusyDialog,"NO") == 0)
		{
			polFilter->EnableBusyDialog(FALSE);
		}
		else
		{
			polFilter->EnableBusyDialog(TRUE);
		}

		char pclNotRespondingDialog[128];
		GetPrivateProfileString("BcComServer", "NotRespondingDialog", "YES", pclNotRespondingDialog, sizeof pclNotRespondingDialog, pclConfigPath);
		if (stricmp(pclNotRespondingDialog,"NO") == 0)
		{
			polFilter->EnableNotRespondingDialog(FALSE);
		}
		else
		{
			polFilter->EnableNotRespondingDialog(TRUE);
		}

		char pclMultithreaded[128];
		GetPrivateProfileString("BcComServer", "MultiThreaded", "NO", pclMultithreaded, sizeof pclMultithreaded, pclConfigPath);
		if (stricmp(pclMultithreaded,"YES") == 0)
		{
			this->bmMultithreaded = true;
		}
		else
		{
			this->bmMultithreaded = false;
		}


	}

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void MonitorDlg::OnPaint() 
{
	if (CBcComServerApp::bgApplicationHidden == TRUE && this->IsWindowVisible())
	{
		this->ShowWindow(SW_HIDE);
	}
	else
	{
		CDialog::OnPaint();
	}
}

void MonitorDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	static bool smNoUpdatesNow = false;

	if (smNoUpdatesNow == false)
	{
		smNoUpdatesNow = true;
		BC_To_Client();
		smNoUpdatesNow = false;
	}
	
	CDialog::OnTimer(nIDEvent);
}


void MonitorDlg::BC_To_Client()
{
	CString olBc;
	int ilErr;

	if (this->bmMultithreaded)
	{
		int ilCount = m_BcList.GetCount();
		if (ilCount >= 500)
		{
			m_BcList.DeleteString (ilCount - 1);
		}
		return;
	}

	for (int i = CBcComServerApp::ogAtlConnections.GetSize()-1; i >= 0;i--)
	{
		try
		{
			olBc = CBcComServerApp::ogAtlConnections[i].pomBcCom->BC_To_Client();
			if (!olBc.IsEmpty())
			{
				olBc = CBcComServerApp::ogAtlConnections[i].pomBcCom->Application() + CString('.') + CBcComServerApp::ogAtlConnections[i].pomBcCom->ProcessID() + CString(':') + olBc;
				ilErr = this->m_BcList.InsertString(0,olBc);
				int ilCount = m_BcList.GetCount();
 				if (ilCount >= 500)
				{
					m_BcList.DeleteString (ilCount - 1);
				}
			}
		}
		catch(...)	// COM - object no longer valid due to client abort etc...
		{
			ilErr = this->m_BcList.InsertString(0,"Send broadcast to connection failed, removing connection from list...");
			int ilCount = m_BcList.GetCount();
 			if (ilCount >= 500)
			{
				m_BcList.DeleteString (ilCount - 1);
			}

			CBcComServerApp::ogAtlConnections.DeleteAt(i);
			if (CBcComServerApp::ogAtlConnections.GetSize() == 0)
			{
				::PostQuitMessage(0);	
			}
		}
	}
}

void MonitorDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	// TODO: Add your message handler code here
	KillTimer(0);
}

void MonitorDlg::OnSavelistas() 
{
	// TODO: Add your control notification handler code here
	CString olPath;
	CString olFile;
	CTime olTime = CTime::GetCurrentTime();

	olPath = "C:\\Ufis\\Broadcastlist" + olTime.Format("%Y%m%d-%H%M");

	CFileDialog olDlg( FALSE, "log", olPath, OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT, "*.log", this);
	if (olDlg.DoModal() == IDOK)
	{
		olPath = olDlg.GetPathName();
		olFile = olDlg.GetFileName();
		ofstream of;
		int ilCount, i;

		of.open(olPath, ios::out);

		ilCount = m_BcList.GetCount();
		for(i = 0; i < ilCount; i++)
		{
			CString strLine;
			m_BcList.GetText(i, strLine);
			of << strLine << endl;
		}
			
		of.close();
	}
}

void MonitorDlg::TransmitFilter()
{
	CMapStringToPtr	olMapFilter;

	bool blUseNoFilter = false;

	for (int i = 0; i < CBcComServerApp::ogAtlConnections.GetSize(); i++)
	{
		const CMapStringToPtr& rolCurrFilter = CBcComServerApp::ogAtlConnections[i].pomBcCom->Filter();
		if (rolCurrFilter.GetCount() == 0)
		{
			blUseNoFilter = true;
			olMapFilter.RemoveAll();
			break;
		}

		for (POSITION pos = rolCurrFilter.GetStartPosition(); pos != NULL;)
		{
			CString olString;
			CString *p = NULL;
			rolCurrFilter.GetNextAssoc(pos,olString,(void *&)p);
			if (!olString.IsEmpty())
			{
				CString *p1 = NULL;
				if (!olMapFilter.Lookup(olString,(void *&)p1))
				{
					olMapFilter.SetAt(olString,p);
				}
			}
		}

	}

	CBcComAtl::TransmitFilter(olMapFilter);
}


void MonitorDlg::OnClose() 
{
	// TODO: Add your message handler code here and/or call default
	AfxAbort();

	CDialog::OnClose();
}

LRESULT MonitorDlg::OnFireEventForThread(WPARAM wParam,LPARAM lParam)
{
	char *pszMsg = (char *)lParam;

	int ilErr = this->m_BcList.InsertString(0,pszMsg);
	int ilCount = m_BcList.GetCount();
	if (ilCount >= 500)
	{
		m_BcList.DeleteString (ilCount - 1);
	}

	delete[] pszMsg;

	return 0L;
}

void MonitorDlg::OnButtonInfo() 
{
	// TODO: Add your control notification handler code here
	CServerInfoDlg olDlg(this);
	olDlg.DoModal();
}

bool MonitorDlg::AddToSpooler(const STR_DESC& rlMsg)
{
	for (int i = CBcComServerApp::ogAtlConnections.GetSize()-1; i >= 0;i--)
	{
		try
		{
			CBcComAtl *polDlg = CBcComServerApp::ogAtlConnections[i].pomBcCom;
			if (polDlg != NULL)
			{
				CSingleLock lock(&polDlg->omSection);
				if (lock.Lock())
				{
					++polDlg->lmItemsReceived;
					polDlg->omSpooler.Add(rlMsg.Value);
					lock.Unlock();
				}
						
				if (polDlg->hEvent != NULL)
				{
					SetEvent(polDlg->hEvent);
				}
			}
		}
		catch(...)	// COM - object no longer valid due to client abort etc...
		{
			int ilErr = this->m_BcList.InsertString(0,"Add broadcast to spooler failed, removing connection from list...");
			int ilCount = m_BcList.GetCount();
 			if (ilCount >= 500)
			{
				m_BcList.DeleteString (ilCount - 1);
			}

			CBcComServerApp::ogAtlConnections.DeleteAt(i);
			if (CBcComServerApp::ogAtlConnections.GetSize() == 0)
			{
				::PostQuitMessage(0);	
			}
		}
	}

	return true;
}

bool MonitorDlg::Fire_OnLostBc(int ilErr)
{
	bool blOk = false;
	for (int i = CBcComServerApp::ogAtlConnections.GetSize()-1; i >= 0;i--)
	{
		try
		{
			CBcComAtl *polDlg = CBcComServerApp::ogAtlConnections[i].pomBcCom;
			if (polDlg != NULL)
			{
				HRESULT hr = polDlg->Fire_OnLostBc(ilErr);
				if (hr == S_OK)
					blOk = true;
			}
		}
		catch(...)	// COM - object no longer valid due to client abort etc...
		{
			int ilErr = this->m_BcList.InsertString(0,"Add broadcast to spooler failed, removing connection from list...");
			int ilCount = m_BcList.GetCount();
 			if (ilCount >= 500)
			{
				m_BcList.DeleteString (ilCount - 1);
			}

			CBcComServerApp::ogAtlConnections.DeleteAt(i);
			if (CBcComServerApp::ogAtlConnections.GetSize() == 0)
			{
				::PostQuitMessage(0);	
			}
		}
	}

	return blOk;
}

bool MonitorDlg::Fire_OnLostBcEx(const CComBSTR& ropMsg)
{
	bool blOk = false;
	for (int i = CBcComServerApp::ogAtlConnections.GetSize()-1; i >= 0;i--)
	{
		try
		{
			CBcComAtl *polDlg = CBcComServerApp::ogAtlConnections[i].pomBcCom;
			if (polDlg != NULL)
			{
				HRESULT hr = polDlg->Fire_OnLostBcEx(ropMsg);
				if (hr == S_OK)
					blOk = true;
			}
		}
		catch(...)	// COM - object no longer valid due to client abort etc...
		{
			int ilErr = this->m_BcList.InsertString(0,"Add broadcast to spooler failed, removing connection from list...");
			int ilCount = m_BcList.GetCount();
 			if (ilCount >= 500)
			{
				m_BcList.DeleteString (ilCount - 1);
			}

			CBcComServerApp::ogAtlConnections.DeleteAt(i);
			if (CBcComServerApp::ogAtlConnections.GetSize() == 0)
			{
				::PostQuitMessage(0);	
			}
		}
	}

	return blOk;
}

void MonitorDlg::OnClientInfo() 
{
	// TODO: Add your control notification handler code here
	BcComClientDlg olDlg(this);
	olDlg.DoModal();
	
}
