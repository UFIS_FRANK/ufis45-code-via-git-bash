#if !defined(AFX_BCCOMCLIENTDLG_H__CBA2C7C2_DCA9_45C8_B183_9FBE85DD5658__INCLUDED_)
#define AFX_BCCOMCLIENTDLG_H__CBA2C7C2_DCA9_45C8_B183_9FBE85DD5658__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// BcComClientDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// BcComClientDlg dialog

class BcComClientDlg : public CDialog
{
// Construction
public:
	BcComClientDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(BcComClientDlg)
	enum { IDD = IDD_CLIENTINFODLG };
		// NOTE: the ClassWizard will add data members here
	CListCtrl	m_InfoList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(BcComClientDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(BcComClientDlg)
		// NOTE: the ClassWizard will add member functions here
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BCCOMCLIENTDLG_H__CBA2C7C2_DCA9_45C8_B183_9FBE85DD5658__INCLUDED_)
