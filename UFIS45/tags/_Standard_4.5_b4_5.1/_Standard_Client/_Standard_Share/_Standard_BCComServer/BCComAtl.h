// BcComAtl.h : Declaration of the CBcComAtl

#ifndef __BCCOMATL_H_
#define __BCCOMATL_H_

#include "resource.h"       // main symbols
#include "BcComServerCP.h"
#include <iomanip.h>
#include <iostream.h>
#include <fstream.h>


/////////////////////////////////////////////////////////////////////////////
// CBcComAtl

struct _StringDescriptor
{
        char* Value;                    /* string with values */
        long  AllocatedSize;    /* byte count from malloc or realloc */
        long  UsedLen;                  /* calulated value from strlen */
};
typedef struct _StringDescriptor STR_DESC;

class CBlockingSocket;

class ATL_NO_VTABLE CBcComAtl : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CBcComAtl, &CLSID_BcComAtl>,
	public ISupportErrorInfo,
	public IConnectionPointContainerImpl<CBcComAtl>,
	public IDispatchImpl<IBcComAtl, &IID_IBcComAtl, &LIBID_BCCOMSERVERLib>,
	public CProxy_IBcComAtlEvents< CBcComAtl >
{
public:
	CBcComAtl();
	~CBcComAtl(); //Destructor

DECLARE_REGISTRY_RESOURCEID(IDR_BCCOMATL)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(CBcComAtl)
	COM_INTERFACE_ENTRY(IBcComAtl)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
	COM_INTERFACE_ENTRY(IConnectionPointContainer)
	COM_INTERFACE_ENTRY_IMPL(IConnectionPointContainer)
END_COM_MAP()
BEGIN_CONNECTION_POINT_MAP(CBcComAtl)
CONNECTION_POINT_ENTRY(DIID__IBcComAtlEvents)
END_CONNECTION_POINT_MAP()


// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

// IBcComAtl
public:
	STDMETHOD(get_BuildDate)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(get_Version)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(GetNextBufferdBC)(/*[out]*/ BSTR *pReqId, /*[out]*/ BSTR *pDest1,/*[out]*/ BSTR *pDest2,/*[out]*/ BSTR *pCmd,/*[out]*/ BSTR *pObject,/*[out]*/ BSTR *pSeq,/*[out]*/ BSTR *pTws,/*[out]*/ BSTR *pTwe,/*[out]*/ BSTR *pSelection,/*[out]*/ BSTR *pFields,/*[out]*/ BSTR *pData,/*[out]*/ BSTR *pBcNum);
	STDMETHOD(StopBroadcasting)();
	STDMETHOD(StartBroadcasting)();
	STDMETHOD(TransmitFilter)();
	STDMETHOD(SetFilter)(BSTR strTable,BSTR strCommand,BOOL bOwnBC,BSTR application);
	STDMETHOD(SetPID)(BSTR strAppl,BSTR strPID);

	CString			BC_To_Client();
	CString			Application()	{ return this->omAppl;}
	CString			ProcessID()		{ return this->omPID; }
	
	const CMapStringToPtr&	Filter()	{ return mapFilter; }

	static	void	TransmitFilter(const CMapStringToPtr& ropMapFilter);
	bool			IsFilterDefined();
	static	bool	GetServerInfo(const CString& ropHostName,int ipTcpPort,CStringArray& ropResult);
	static	bool	StopConnection(const CString& ropHostName,int ipTcpPort,const CString& ropQueueId);

private:	// implementation data
	long				lmMaxBCRecords; //0=No logging, any other value: The number of BCs for one file
	long				lmBCSpoolTimer;
	bool				bmTimerIsSet;
	long				lmCurrWriteCount;
	long				lmCurrFileNo;
	CString				omCurrFileName;

	static char			pcmHostName[512];
	static int			imTcpPort;
	static CWinThread*	pomWorkerThread;
	static CString		m_bCQueueID;
	static BOOL			m_amIConnected;

	CCriticalSection	omSection;
	CStringArray		omSpooler;
	CMapStringToPtr		mapFilter;
	ofstream			of;
	CString				omAppl;
	CString				omPID;
	bool				bmSendSuspended;
	long				lmItemsReceived;
	long				lmItemsSend;
	CWinThread			*pomSendThread;

private:	// helper functions
	static bool		CreateThread();
	static UINT		WorkerFunction( LPVOID pParam );
	static BOOL		ReadMsgFromSock(FILE *fp,CBlockingSocket &ropSock, STR_DESC *rpMsg, BOOL ipGetAck);
	static char*	GetKeyItem(char *pcpResultBuff, long *plpResultSize,char *pcpTextBuff, char *pcpKeyWord, char *pcpItemEnd, bool bpCopyData);
	bool			CreateSendThread();
	static UINT		SendFunction( LPVOID pParam );
	HANDLE				hEvent;
	static HANDLE	hEventWorkerThreadExit;

	friend class		MonitorDlg;
	friend class		BcComClientDlg;
};

#endif //__BCCOMATL_H_
