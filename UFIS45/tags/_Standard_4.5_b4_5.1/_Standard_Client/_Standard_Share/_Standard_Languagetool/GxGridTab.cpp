// GxGridTab.cpp : implementation file
//
//
// GxGridTab is part of the CoCo project
//
// GxGridTab is the inherited class and mainly the same as the GxGridWnd-class except that it
//	implements automatic sorting when clicking on the header cells.
//
//	Version 1.0 on 04.11.1999 by WES
//
//

#include <stdafx.h>
#include <GxGridTab.h>
#include <CCSGlobl.h>
#include <coco3.h>
#include <coco3dlg.h>
#include <AddEntryDlg.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

int	bmSortAscend;

/////////////////////////////////////////////////////////////////////////////
// GxGridTab

/*CCE*/IMPLEMENT_DYNCREATE(GxGridTab, CGXGridWnd)

GxGridTab::GxGridTab():CGXGridWnd()
{
	pomParent = NULL;
}

GxGridTab::GxGridTab(CWnd *popParent):CGXGridWnd()
{
	pomParent = NULL;
	pomParent = popParent;
}

GxGridTab::~GxGridTab()
{
}


void GxGridTab::SetParent(CWnd *popParent)
{
	pomParent = popParent;
}

void GxGridTab::OnClickedButtonRowCol(ROWCOL nRow, ROWCOL nCol)
{
	CGXGridWnd::OnClickedButtonRowCol(nRow, nCol);
	if(pomParent != NULL)
	{
		pomParent->SendMessage(GX_CELL_BUTTONCLICKED, (WPARAM)nRow, (LPARAM)nCol);
	}
}


BOOL dontsort = FALSE;

void GxGridTab::OnLButtonUp(UINT nFlags, CPoint point)
{
	CGXGridWnd::OnLButtonUp(nFlags, point);

	ROWCOL Row;
	ROWCOL Col;

	HitTest(point, &Row,&Col, NULL);

	if (Col>GetColCount()) return;

	if (dontsort) { dontsort=false; }
	else
	{
		if ((Row == 0) && (Col != 0))
			SortTable ( Col );
	}
}

BOOL GxGridTab::OnStartTracking(ROWCOL nRow, ROWCOL nCol, int nTrackingMode)
{
	CGXGridWnd::OnStartTracking(nRow, nCol, nTrackingMode);
	dontsort=TRUE;

	return TRUE;
}

void GxGridTab::OnEndTracking(ROWCOL nRow, ROWCOL nCol, int nTrackingMode, CSize& size)
{
	CGXGridWnd::OnEndTracking(nRow, nCol, nTrackingMode, size);
	//dontsort=FALSE;		// reseting the global variable is done when updating the display!
}

BOOL  GxGridTab::OnEndEditing(ROWCOL nRow, ROWCOL nCol)
{
	CGXGridWnd::OnEndEditing(nRow, nCol);
	if ((nCol == 5) && (nRow>0))		// only string-column is editable
	{
		CString MyUrno, newtext, oldtext;
		BOOL blModified = FALSE;
		CGXControl *polControl = GetControl(nRow, nCol);
		if ( polControl )
			blModified = polControl->GetModify();

		// get urno from col 1 and new text string from col 5
		MyUrno=GetValueRowCol(nRow,1);
		if ( blModified && strcmp(MyUrno,"") != 0)
		{
			//ogBCD.GetField("TXT", "URNO",MyUrno, "STRG", oldtext);
			//newtext=GetValueRowCol(nRow,5);
			
			//if (strcmp(newtext,oldtext) != 0)
			//{
				// convert to replace some special characters
			//	Grid2Internal(newtext); 
			//	MakeCedaString(newtext);
				// update record by urno: new text and status is TRANSLATED
			//	ogBCD.SetField("TXT", "URNO",MyUrno, "STRG", newtext,AutoCommitChanges);
				ogBCD.SetField("TXT", "URNO",MyUrno, "STAT", "TRANSLATED",AutoCommitChanges);
				Redraw ();
				isModified=true;
			//}
			if ( pomParent )
				pomParent->SendMessage (GX_ROWCOL_CHANGED, nRow, nCol );

		}
	}

	return TRUE;
}

BOOL GxGridTab::OnStartEditing(ROWCOL nRow, ROWCOL nCol)
{
	CGXGridWnd::OnStartEditing(nRow, nCol);
	if (nCol == 5)		// only string-column is editable
	{
		return TRUE;
	}
	else return FALSE;
}

BOOL GxGridTab::OnLButtonDblClkRowCol(ROWCOL nRow, ROWCOL nCol, UINT nFlags, CPoint pt)
{
	/*if (nRow == 0) dontsort=TRUE;  // no special sorting here !
	else*/ CGXGridWnd::OnLButtonDblClkRowCol(nRow, nCol, nFlags, pt);
	if (/*(nCol == 0) &&*/ (nCol != 5) &&(nRow > 0))
	{
		// edit row
		AddEntryDlg	MyDlg;
		CString s;

		globfilterapp=GetValueRowCol(nRow,2);
		globfiltercoco=GetValueRowCol(nRow,3);

		globUrno=GetValueRowCol(nRow,1);		// get urno from grid
		globStatus=GetValueRowCol(nRow,4);
		globString=GetValueRowCol(nRow,5);
		globTextID=GetValueRowCol(nRow,6);
		globStringID=GetValueRowCol(nRow,7);


		if ( MyDlg.DoModal() == IDOK )
		{
			int cnt=ogBCD.GetFieldCount("TXT");
			RecordSet olRecord(cnt);
			CCoCo3Dlg *polMainDlg=0;
		
			if ( pomParent && 
				 (pomParent->GetRuntimeClass() == RUNTIME_CLASS(CCoCo3Dlg) ) )
				polMainDlg = (CCoCo3Dlg*)pomParent;

			if (MyDlg.m_NewApplication != globfilterapp) 
			{
				s=MyDlg.m_NewApplication; 
				Grid2Internal(s); 
				MakeCedaString(s); 
				ogBCD.SetField("TXT", "URNO",globUrno, "APPL", s,AutoCommitChanges); 
				SetValueRange(CGXRange(nRow,2),MyDlg.m_NewApplication);
				isModified=true;
				if ( polMainDlg )
					polMainDlg->AddApplIfNew(MyDlg.m_NewApplication);
			}
			if (MyDlg.m_NewCoCo != globfiltercoco) 
			{
				s=MyDlg.m_NewCoCo; 
				Grid2Internal(s); 
				MakeCedaString(s); 
				ogBCD.SetField("TXT", "URNO",globUrno, "COCO", s,AutoCommitChanges); 
				SetValueRange(CGXRange(nRow,3),MyDlg.m_NewCoCo);
				isModified=true;
				if ( polMainDlg )
					polMainDlg->AddCocoIfNew(MyDlg.m_NewCoCo);
			}

			if (MyDlg.m_NewString != globString) 
			{
				s=MyDlg.m_NewString; 
				Grid2Internal(s); 
				MakeCedaString(s); 
				ogBCD.SetField("TXT", "URNO",globUrno, "STRG", s,AutoCommitChanges);

				theApp.TextChanged ( MyDlg.m_NewCoCo, globString, MyDlg.m_NewString );
				SetValueRange(CGXRange(nRow,5),MyDlg.m_NewString);
				isModified=true;
			}
			if (MyDlg.m_NewStringID != globStringID) 
			{
				s=MyDlg.m_NewStringID; 
				Grid2Internal(s); 
				MakeCedaString(s); 
				ogBCD.SetField("TXT", "URNO",globUrno, "STID", s,AutoCommitChanges); 
				SetValueRange(CGXRange(nRow,7),MyDlg.m_NewStringID);
				isModified=true;
			}
			if (MyDlg.m_NewTextID != globTextID) 
			{
				s=MyDlg.m_NewTextID; 
				Grid2Internal(s); 
				MakeCedaString(s); 
				ogBCD.SetField("TXT", "URNO",globUrno, "TXID", s,AutoCommitChanges); 
				SetValueRange(CGXRange(nRow,6),MyDlg.m_NewTextID);
				isModified=true;
			}
		
			if (((MyDlg.m_NewStatus == 1) && (globStatus != "TRANSLATED")) ||  
				((MyDlg.m_NewStatus == 0) && (globStatus != "NEW")))
			{
				if (MyDlg.m_NewStatus == 0) s="NEW";
				else s="TRANSLATED";
				Grid2Internal(s); 
				MakeCedaString(s); 
				ogBCD.SetField("TXT", "URNO",globUrno, "STAT", s,AutoCommitChanges); 
				SetValueRange(CGXRange(nRow,4),s); //;----------------------)
				isModified=true;
			}
			if ( pomParent )
				pomParent->SendMessage (GX_ROWCOL_CHANGED, nRow, 5 );


//			UpdateDisplay();		// no need to update
			Redraw ();
		}
	}
	return TRUE;
}

//
// this code modified by WES, taken from a code snippet of HAG on 11.11.1999
//	now, the searching coloumn is warpped arround and if not found in this col, the next
//
//
void GxGridTab::OnTextNotFound(LPCTSTR)
{
	ROWCOL		ilRow, ilCol, ilColAnz;
	BOOL		blFound = FALSE;
	ilColAnz = GetColCount();
	GX_FR_STATE	*pslState = GXGetLastFRState();

	if ( GetCurrentCell( &ilRow, &ilCol ) && pslState )
	{
		if ( pslState->bNext )		//  d.h Abw�rts suchen
		{
			
			while ( !blFound && ( ilCol < ilColAnz ) ) //  und noch nicht in letzter Spalte
			{
				ilCol ++;
				ilRow = GetHeaderRows ()+1;
				blFound = FindText( ilRow, ilCol, true ) ;
			}
			if (!blFound) 
			{ 
				ROWCOL i=1;
				blFound = FindText(i,ilCol,true);
			}

		}
		else 
		{	//  d.h Aufw�rts suchen
			
			while ( !blFound &&  ( ilCol > GetHeaderCols () + 1 ) )	
			{
				ilCol --;
				ilRow = GetRowCount ();
				blFound = FindText( ilRow, ilCol, true );
			}
			if (!blFound)
			{
				ROWCOL i =GetRowCount();
				blFound = FindText(i,ilCol,true);
			}
		}
		if ( blFound )
			return;

	}
	MessageBeep(0);
}

//
// automatic select full row
//
//
BOOL GxGridTab::OnStartSelection(ROWCOL nRow, ROWCOL nCol, UINT flags, CPoint point)
{
//	SelectRange(CGXRange( ).SetRows(nRow,nRow), TRUE);
//	SetSelection(NULL, nRow, 0, nRow, 7);
	return true;

//	return CGXGridWnd::OnStartSelection(nRow,nCol, flags, point);

}

BOOL GxGridTab::ProcessKeys(CWnd* pSender, UINT nMessage, UINT nChar, UINT nRepCnt, UINT flags)
{
	//TRACE( "%i, %i\n", nChar, flags );
	if ((nChar == 6) && (flags == 33)) // catch "ctrl+f" and pop up the find-dialog
	{
		OnShowFindReplaceDialog(TRUE);
		return true;
	}
	else return CGXGridWnd::ProcessKeys(pSender, nMessage, nChar, nRepCnt , flags);
}

void GxGridTab::SortTable ( ROWCOL ipCol )
{
	CGXSortInfoArray sortInfo;
	DWORD llStart = GetTickCount();
	sortInfo.SetSize(1);
	if (bmSortAscend)
	{
		sortInfo[0].sortOrder = CGXSortInfo::descending;
		bmSortAscend = false;
	}
	else
	{
		sortInfo[0].sortOrder = CGXSortInfo::ascending;
		bmSortAscend = true;
	}

	sortInfo[0].nRC = ipCol;
	sortInfo[0].sortType = CGXSortInfo::autodetect;
	SortRows(CGXRange().SetTable(),sortInfo);
	DWORD llEnd = GetTickCount();
	llEnd -= llStart;
	CString olText;
	double dlSec = (double)llEnd  / 1000.0;
	olText.Format ( "Funktion dauerte %.3lf sec", dlSec );
	TRACE ( "%s   %s\n", "<GxGridTab::OnLButtonUp>", olText );   
//	MessageBox ( olText, "<GxGridTab::OnLButtonUp>" );

	pomParent->SendMessage(GX_GRID_SORTED,0,0);
}

BEGIN_MESSAGE_MAP(GxGridTab, CGXGridWnd)
	//{{AFX_MSG_MAP(GxGridTab)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		ON_WM_LBUTTONUP()				// wes
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// GxGridTab message handlers
BOOL GxGridTab::OnTrackColWidth(ROWCOL nCol)
{
	//------------------------------------
	// overwritten virtual of CGXGridCore
	//------------------------------------
	if (IsColHidden(nCol) == TRUE)
		return FALSE;
	else
		return TRUE;

}
