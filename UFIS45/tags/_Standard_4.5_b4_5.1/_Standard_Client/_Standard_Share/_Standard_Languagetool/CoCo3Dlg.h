// CoCo3Dlg.h : header file
//

#include <GxGridTab.h>
//#include "DialogGrid.h"
#include <CCSEdit.h>


#if !defined(AFX_COCO3DLG_H__FD0204C5_9102_11D3_8FB0_0050DA1CAD13__INCLUDED_)
#define AFX_COCO3DLG_H__FD0204C5_9102_11D3_8FB0_0050DA1CAD13__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <txtgrid.h>

/////////////////////////////////////////////////////////////////////////////
// CCoCo3Dlg dialog

class CCoCo3Dlg : public CDialog
{
	DECLARE_DYNCREATE(CCoCo3Dlg)
// Construction
public:
	CCoCo3Dlg(CWnd* pParent = NULL);	// standard constructor

	CTxtGrid omCoCoGrid;
	bool blBCSinceLastLoad ;
//	CGridFenster omCoCoGrid;


// Dialog Data
	//{{AFX_DATA(CCoCo3Dlg)
	enum { IDD = IDD_COCO3_DIALOG };
	CButton	m_ConfirmationBtn;
	CButton	m_ImportRCButton;
	CButton	m_SaveButton;
	CComboBox	m_CoCos;
	CComboBox	m_Applications;
	CButton	m_ShowGeneral;
	BOOL	m_AutoTranslate;
	BOOL	m_WithConfirmation;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCoCo3Dlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
public:
	void UpdateDisplay();
	void AddApplIfNew ( LPCSTR pcpAppl );
	void AddCocoIfNew ( LPCSTR pcpCoco );
	void SynchrCocoApplList();
	void OnTXTBC ( RecordSet *polRec, int ipDDXType );

protected:
	HICON m_hIcon;

	void TestModifiedState();
	void IniStringGrid ();

	// Generated message map functions
	//{{AFX_MSG(CCoCo3Dlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnButton1();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnSelchangeCombo1();
	afx_msg void OnSelchangeCombo2();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnButton5();
	afx_msg void OnButton2();
	afx_msg void OnButton3();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnButton6();
	afx_msg void OnShowGeneral();
	afx_msg void OnButton40();
	afx_msg void OnSelchangeCombo3();
	afx_msg void OnSelchangeCombo4();
	afx_msg void OnClose();
	afx_msg void OnDofindgrid();
	afx_msg void OnButton4();
	afx_msg void OnButton7();
	afx_msg void OnAddInGrid();
	afx_msg void OnWantConfirmationChk();
	afx_msg void OnAutoTranslateChk();
	//}}AFX_MSG
	void OnGridEndEditing( WPARAM nRow, LPARAM nCol );
	LONG OnBcAdd(UINT wParam, LONG lParam);
	DECLARE_MESSAGE_MAP()
};

static void MainDDXCallback(void *popInstance, int ipDDXType,
							void *vpDataPointer, CString &ropInstanceName);

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COCO3DLG_H__FD0204C5_9102_11D3_8FB0_0050DA1CAD13__INCLUDED_)
