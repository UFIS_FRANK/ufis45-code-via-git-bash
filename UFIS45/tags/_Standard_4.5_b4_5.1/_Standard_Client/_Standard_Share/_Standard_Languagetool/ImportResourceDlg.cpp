// ImportResourceDlg.cpp : implementation file
//

#include <stdafx.h>
#include <CoCo3.h>
#include <ImportResourceDlg.h>
#include <CCSGlobl.h>
#include <AddEntryDlg.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CImportResourceDlg dialog


CImportResourceDlg::CImportResourceDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CImportResourceDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CImportResourceDlg)
	//}}AFX_DATA_INIT
}


void CImportResourceDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CImportResourceDlg)
	DDX_Control(pDX, IDC_BUTTON3, m_Create);
	DDX_Control(pDX, IDC_BUTTON2, m_CreateAll);
	DDX_Control(pDX, IDC_LIST3, m_output);
	DDX_Control(pDX, IDC_LIST2, m_rc);
	DDX_Control(pDX, IDC_LIST1, m_resource);
	DDX_Control(pDX, IDC_CHECK1, m_Confirm);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CImportResourceDlg, CDialog)
	//{{AFX_MSG_MAP(CImportResourceDlg)
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_LBN_DBLCLK(IDC_LIST2, OnDblclkList2)
	ON_BN_CLICKED(IDC_BUTTON1, OnButton1)
	ON_LBN_DBLCLK(IDC_LIST3, OnDblclkList3)
	ON_BN_CLICKED(IDC_BUTTON2, OnButton2)
	ON_BN_CLICKED(IDC_BUTTON3, OnButton3)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CImportResourceDlg message handlers

void CImportResourceDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	
	if (m_resource)
	{
		RECT myrect;

		GetClientRect(&myrect);

		myrect.top+=24;
		int i=(myrect.right-myrect.left)/3;
		myrect.right=i;
		m_resource.MoveWindow(&myrect,true);
		myrect.left=i;
		myrect.right=i*2;
		m_rc.MoveWindow(&myrect,true);
		myrect.left=i*2;
		myrect.right=i*3;
		m_output.MoveWindow(&myrect,true);
	}
	
}

void CImportResourceDlg::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	RECT myrect;

	GetClientRect(&myrect);

	myrect.top+=24;
	int i=(myrect.right-myrect.left)/3;
	myrect.right=i;
	m_resource.MoveWindow(&myrect,true);
	myrect.left=i;
	myrect.right=i*2;
	m_rc.MoveWindow(&myrect,true);
	myrect.left=i*2;
	myrect.right=i*3;
	m_output.MoveWindow(&myrect,true);	

//	m_Appl.SetWindowText("GENERAL");
//	m_Coco.SetWindowText("US");
//	m_Confirm.SetCheck(1);

	m_CreateAll.EnableWindow(m_output.GetCount() >0);
	m_Create.EnableWindow(m_output.GetCount() >0);

	if ((globfiltercoco == "") || (globfilterapp == ""))
	{
		m_Confirm.SetCheck(1);
		m_Confirm.EnableWindow(false);
	}
	else m_Confirm.EnableWindow(true);

}

//
// lookup entries in left and right list and select them (if existing)
//
//
void CImportResourceDlg::OnDblclkList2() 
{
	int i=m_rc.GetCurSel();

	CString mystr, mystr2, mystr3;

	m_rc.GetText(i,mystr);

	i=mystr.Find(']');
	mystr3=mystr.Right(mystr.GetLength()-i-2);
	mystr=mystr.Left(i);
	mystr=mystr.Right(mystr.GetLength()-1);		// mystr now holds the ID_xxx constant

	// autoselect in rc-list
	for (i=m_resource.GetCount()-1; i>=0; i--)
	{
		m_resource.GetText(i,mystr2);
		if (mystr2.Find(mystr) != -1)
		{
			//m_resource.SetSel(i,true);
			m_resource.SetCurSel(i);
			i=-1;
		}
		else m_resource.SetCurSel(-1);
	}
	// autoselect in output list
	for (i=m_output.GetCount()-1; i>=0; i--)
	{
		m_output.GetText(i,mystr2);
		if (mystr2.Find(mystr) != -1)
		{
			m_output.SetCurSel(i);
			i=-1;
		}
		else m_output.SetCurSel(-1);
	}
}

//
// import a new resource file
//
//
void CImportResourceDlg::OnButton1() 
{
	CFileDialog mydlg(true,"*.rc","*.rc",OFN_FILEMUSTEXIST || OFN_HIDEREADONLY || OFN_PATHMUSTEXIST);
	if (mydlg.DoModal() == IDOK)
	{
		LoadResource(mydlg.GetPathName(), mydlg.GetFileName());
		//m_Filename.SetWindowText(mydlg.GetFileName());
	}
}

//
// read the given files and update all lists
//
//
void CImportResourceDlg::LoadResource(CString myfullname, CString myfilename)
{
	CStdioFile file1, file2;

	bool error=false;

	m_rc.ResetContent();
	m_resource.ResetContent();
	m_output.ResetContent();

	CString mypath=myfullname.Left(myfullname.GetLength()-myfilename.GetLength());

	if( !file1.Open(myfullname, CFile::modeRead| CFile::typeText )) 
	{
		MessageBox("The resource (.rc)-file does not exist!", "sorry:", MB_OK | MB_ICONEXCLAMATION   );
		error=true;
	}
	if( !file2.Open( mypath+"resource.h", CFile::modeRead| CFile::typeText )) 
	{
		MessageBox("The resource.h-file does not exist!", "sorry:", MB_OK | MB_ICONEXCLAMATION   );
		error=true;
	}

	CString mytext;

	if (!error)
	{
		// read resource files and store contents in Stringlists
		CString myline;
		CStringArray hlist/*, rlist*/;

		int len1=file2.GetLength();
		while ((file2.ReadString(myline)) != NULL) 
		{
			if ((myline != "") && ((myline[0] != '/') && (myline.Find("#define ",0) != -1)))	// always ignore comments 
			{
				myline=myline.Mid(8,myline.GetLength()-8);
    			hlist.Add(myline); 		
				if (mytext == "") m_resource.AddString(myline);
				else m_resource.AddString(mytext+" ** "+myline);

			}
		}
		file2.Close();

		int charpos;
		CString TXID,STID,STRG,mystr;
		len1=file1.GetLength();
		while ((file1.ReadString(myline) != NULL) /*&& (myline != "")*/ ) 
		{
			if ((myline != "") && (myline[0] == 'S'))
			{
				myline == myline;
			}
			if ((myline != "") && ((myline == "STRINGTABLE PRELOAD DISCARDABLE ") ||
				(myline == "STRINGTABLE DISCARDABLE ")))
			{
				file1.ReadString(myline);		// "BEGIN"
				
				while ((file1.ReadString(myline) != NULL) && (strcmp(myline,"END") != 0))
				{
					// parse the line
					myline.TrimLeft();
					charpos=myline.Find(' ',0);
					TXID=myline.Left(charpos);							// extract TXID
					myline=myline.Right(myline.GetLength()-charpos);
					myline.TrimLeft();								// extract STRG
					STRG=myline;

//					rlist.Add("["+TXID+"] "+STRG);
					m_rc.AddString("["+TXID+"] "+STRG);
			

					for (int i=hlist.GetSize()-1; i>=0; i--)
					{
						mystr=hlist.GetAt(i);
						if (mystr.Find(TXID) != -1)
						{
							i=mystr.Find(' ');
							mystr=mystr.Right(mystr.GetLength()-i);
							mystr.TrimLeft();
							STID=mystr;
							m_output.AddString("STID= "+STID+" TXID= "+TXID+" STRG= "+STRG);
							i=-1;
						}
					}
				}
			}
		}
		file1.Close();
	}
	m_CreateAll.EnableWindow(m_output.GetCount() >0);
	m_Create.EnableWindow(m_output.GetCount() >0);

	if ((globfiltercoco == "") || (globfilterapp == ""))
	{
		m_Confirm.SetCheck(1);
		m_Confirm.EnableWindow(false);
	}
	else m_Confirm.EnableWindow(true);
}

//
// copy the selected entry to the database. Prompt for confirmation if APPL or COCO are not set
//
//
void CImportResourceDlg::OnDblclkList3() 
{
	int cnt=ogBCD.GetFieldCount("TXT");
	RecordSet olRecord(cnt);
		
	CString s, mysel,TXID,STID,STRG, APPL, COCO, STAT;

	m_output.GetText(m_output.GetCurSel(),mysel);
	
	int i,e;
	e=mysel.Find("=",0)+2;
	i=mysel.Find(" ",e);
	STID=mysel.Mid(e,i-e);

	e=mysel.Find("=",i)+2;
	i=mysel.Find(" ",e);
	TXID=mysel.Mid(e,i-e);

	e=mysel.Find("=",i)+2;
	STRG=mysel.Mid(e+1,mysel.GetLength()-e-2);		// avoid quoted string

	COCO=globfiltercoco; 
	APPL=globfilterapp;
	STAT="NEW";

	bool doadd = true;

	if ((m_Confirm.GetCheck() == 1) || ((COCO == "") || (APPL == "")))
	{
		AddEntryDlg	MyDlg;
		CString s;

		globStatus=STAT;
		globString=STRG;
		globTextID=TXID;
		globStringID=STID;

		if (doadd = (MyDlg.DoModal() == IDOK))
		{
			if (MyDlg.m_NewStatus == 0) { STAT="NEW"; }
			else { STAT="TRANSLATED"; }
			STRG=MyDlg.m_NewString;
			STID=MyDlg.m_NewStringID;
			TXID=MyDlg.m_NewTextID;
			APPL=MyDlg.m_NewApplication;
			COCO=MyDlg.m_NewCoCo;
		}
	}

	if (doadd)
	{
			// add new appl or coco to selection lists
//			if ((m_Applications.FindString(0,AppName) == CB_ERR) && (AppName != "")) { m_Applications.AddString(AppName); }
//			if ((m_CoCos.FindString(0,MyDlg.m_CloneCoCo) == CB_ERR) && (MyDlg.m_CloneCoCo != "")) { m_CoCos.AddString(MyDlg.m_CloneCoCo); CoCoList.Add(MyDlg.m_CloneCoCo); }

//		int i=omCoCoGrid.GetRowCount()+1;		// append one field
//		omCoCoGrid.SetRowCount(i);

		Grid2Internal(STID); olRecord.Values[POS_STID]=STID;
		Grid2Internal(STRG); MakeCedaString(STRG);	olRecord.Values[POS_STRN]=STRG;
		Grid2Internal(APPL); olRecord.Values[POS_APPL]=APPL;
		Grid2Internal(TXID); olRecord.Values[POS_TXID]=TXID;
		MakeCedaString(STAT); olRecord.Values[POS_STAT]=STAT;
		Grid2Internal(COCO); olRecord.Values[POS_COCO]=COCO;
		CString newurno; newurno.Format("%ld",ogBCD.GetNextUrno()); olRecord.Values[POS_URNO]=newurno;

		if (ogBCD.InsertRecord("TXT",olRecord, true /*AutoCommitChanges*/) == false)
		{
			ogLog.Trace("DEBUG", "[LanguageTool::ImportResource] Failed appending a record into TXTTAB.");
		}
		else
		{
			//globStrList.Add(STRG);				// keep track on strings
			globUrnoList.Add(newurno);				// and also remember urnos
		}

		isModified=true;
	}
}

//
// copy all entries in the output-listbox to the database
//
//
void CImportResourceDlg::OnButton2() 
{
	if (MessageBox("Do you really want to import all resource strings into the database?", 
		           "please confirm:", MB_YESNOCANCEL | MB_DEFBUTTON2 | MB_ICONQUESTION) == IDYES)
	{
		int t=m_output.GetCount()-1;

/*		if ((refAppl=="") || (refCoCo == ""))
		{
		}
		else*/
		{
			while (t>=0)
			{
				m_output.SetCurSel(t);
				OnDblclkList3();
			}
		}
	}
}

//
// create a new entry -> use function of listobx
//
//
void CImportResourceDlg::OnButton3() 
{
	if (m_output.GetCurSel() == -1)
	{
		MessageBox("Can not create because no item is selected. Please select one and try again.", 
		           "sorry ...", MB_OK | MB_ICONINFORMATION);
	}
	else OnDblclkList3();	
}
