#if !defined(AFX_ADDENTRYDLG_H__1C627D13_9117_11D3_8FB1_0050DA1CAD13__INCLUDED_)
#define AFX_ADDENTRYDLG_H__1C627D13_9117_11D3_8FB1_0050DA1CAD13__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AddEntryDlg.h : header file
//
#include <CedaBasicData.h>
#include <resource.h>

/////////////////////////////////////////////////////////////////////////////
// AddEntryDlg dialog

class AddEntryDlg : public CDialog
{
// Construction
public:
	AddEntryDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(AddEntryDlg)
	enum { IDD = IDD_ADDENTRY_DLG };
	CComboBox	m_EditApp;
	CComboBox	m_EditStatus;
	CComboBox	m_EditCoCo;
	CEdit	m_EditStrID;
	CEdit	m_EditText;
	CEdit	m_EditString;
	CString	m_NewTextID;
	CString	m_NewStringID;
	CString	m_NewString;
	CString	m_NewApplication;
	CString	m_NewCoCo;
	int		m_NewStatus;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(AddEntryDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(AddEntryDlg)
	virtual void OnOK();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ADDENTRYDLG_H__1C627D13_9117_11D3_8FB1_0050DA1CAD13__INCLUDED_)
