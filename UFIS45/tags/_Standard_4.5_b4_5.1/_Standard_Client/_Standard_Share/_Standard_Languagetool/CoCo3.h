// CoCo3.h : main header file for the COCO3 application
//

#if !defined(AFX_COCO3_H__FD0204C3_9102_11D3_8FB0_0050DA1CAD13__INCLUDED_)
#define AFX_COCO3_H__FD0204C3_9102_11D3_8FB0_0050DA1CAD13__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include <resource.h>		// main symbols
#include <ccsglobl.h>		// main symbols


/////////////////////////////////////////////////////////////////////////////
// CCoCo3App:
// See CoCo3.cpp for the implementation of this class
//

class CCoCo3App : public CWinApp
{
public:
	int TextChanged ( CString &ropAppl, CString &ropOldText, CString &ropNewText );
	CCoCo3App();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCoCo3App)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation
	int AddTxtRecord ( CString &ropAppl, CString &ropCoco, CString &ropStat,
					   CString &ropStrg, CString &ropTxid, CString &ropStid, 
					   CString &ropUrno, bool bpSave=AutoCommitChanges );
	bool UseExistingDuplicateString ( CWnd *popParent, CString &ropStrg, 
									  CString &ropCoco, CString &ropAppl );

	//{{AFX_MSG(CCoCo3App)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CString GetInsertTXTErrorMsg ( int ipEcd );

/////////////////////////////////////////////////////////////////////////////
extern CCoCo3App theApp;

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COCO3_H__FD0204C3_9102_11D3_8FB0_0050DA1CAD13__INCLUDED_)
