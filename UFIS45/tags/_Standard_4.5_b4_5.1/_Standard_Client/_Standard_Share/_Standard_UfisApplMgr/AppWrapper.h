// AppWrapper.h: interface for the AppWrapper class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_APPWRAPPER_H__1ADC7723_3408_11D6_9952_0000865098D4__INCLUDED_)
#define AFX_APPWRAPPER_H__1ADC7723_3408_11D6_9952_0000865098D4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CUfisAppMgr;			//forward declaration
class AppWrapper  
{
public:
	AppWrapper();

	virtual ~AppWrapper();

	DWORD			lId;
	CComBSTR		strAppPathName;
	CUfisAppMgr*	pUfisAppMgr;
};

#endif // !defined(AFX_APPWRAPPER_H__1ADC7723_3408_11D6_9952_0000865098D4__INCLUDED_)
