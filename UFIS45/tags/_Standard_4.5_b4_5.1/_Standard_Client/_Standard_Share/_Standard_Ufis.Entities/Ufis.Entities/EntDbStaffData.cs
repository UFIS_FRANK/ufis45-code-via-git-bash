﻿#region ===== Source Code Information =====
/*
Project Name    : Ufis.Entities
Project Type    : Class Library
Platform        : Microsoft Visual C#.NET
File Name       : EntDbEmployeeData.cs

Version         : 1.0.0
Created Date    : 22 - Dec - 2011
Complete Date   : 22 - Dec - 2011
Created By      : Siva

Date Reason Updated By
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------

Copyright (C) All rights reserved.
*/
#endregion
using System;

namespace Ufis.Entities
{
    [Entity(SerializedName = " STFTAB")]
    public class EntDbStaffData : ValidityBaseEntity
    {
        private string _cardNumber;
        private DateTime? _entraceDate;
        private DateTime? _releaveDate;
        private DateTime? _joinDate;
        private string _firstName;
        private string _lastName;
        private string _dob;
        private string _mobileNumber;
        private string _allowInput;
        private bool _kids;
        private string _gender;
        private string _holiday;
        private string _licenseNumber;
        private string _ring;
        private string _matricola;
        private string _nationality;
        private string _fullDuration;
        private string _shortDuration;
        private string __personalNumber;
        private string _memberCode;
        private string _fullNumber;
        private string _shortNumber;
        private string _employeeID;
        private string _flag;
        private string _indicator;
        private string _automaticRelease;
        private string _remark;
        private string _shortName;
        private string _shiftID;
        private string _workTelephone;
        private string _mobileTelephone;
        private string _homeTelephone;
        private string _sendToHR;
        private string _holidayByDisability;
        private string _HolidayByExemptionWork;
        private string _holidayByNightShift;
        private string _holidayByRotatingShift;
        private string _fILT;


        [Entity(SerializedName = "CANO", MaxLength = 20, IsMandatory = true)]
        public string CardNumber
        {
            get { return _cardNumber; }
            set
            {
                if (_cardNumber != value)
                    _cardNumber = value;
                OnPropertyChanged("CardNumber");
            }
        }

        [Entity(SerializedName = "DOBK", SourceDataType = typeof(String), IsMandatory = true)]
        public DateTime? EntraceDate
        {
            get { return _entraceDate; }
            set
            {
                if (_entraceDate != value)
                {
                    _entraceDate = value;
                    OnPropertyChanged("EntraceDate");
                }
            }
        }

        [Entity(SerializedName = "DODM", SourceDataType = typeof(String), IsMandatory = true)]
        public DateTime? ReleaveDate
        {
            get { return _releaveDate; }
            set
            {
                if (_releaveDate != value)
                {
                    _releaveDate = value;
                    OnPropertyChanged("ReleaveDate");
                }
            }
        }

        [Entity(SerializedName = "DOEM", SourceDataType = typeof(String), IsMandatory = true)]
        public DateTime? JoinDate
        {
            get { return _joinDate; }
            set
            {
                if (_joinDate != value)
                {
                    _joinDate = value;
                    OnPropertyChanged("JoinDate");
                }
            }
        }

        [Entity(SerializedName = "FINM", MaxLength = 30, IsMandatory = true)]
        public string FirstName
        {
            get { return _firstName; }
            set
            {
                if (_firstName != value)
                    _firstName = value;
                OnPropertyChanged("FirstName");
            }
        }

        [Entity(SerializedName = "LANM", MaxLength = 40, IsMandatory = true)]
        public string LastName
        {
            get { return _lastName; }
            set
            {
                if (_lastName != value)
                    _lastName = value;
                OnPropertyChanged("LastName");
            }
        }

        [Entity(SerializedName = "GEBU", MaxLength = 8, IsMandatory = true)]
        public string DOB
        {
            get { return _dob; }
            set
            {
                if (_dob != value)
                    _dob = value;
                OnPropertyChanged("DOB");
            }
        }

        [Entity(SerializedName = "GSMN", MaxLength = 20, IsMandatory = true)]
        public string MobileNumber
        {
            get { return _mobileNumber; }
            set
            {
                if (_mobileNumber != value)
                    _mobileNumber = value;
                OnPropertyChanged("MobileNumber");
            }
        }

        [Entity(SerializedName = "INFR", MaxLength = 1, IsMandatory = true)]
        public string AllowInput
        {
            get { return _allowInput; }
            set
            {
                if (_allowInput != value)
                {
                    _allowInput = value;
                    OnPropertyChanged("AllowInput");
                }
            }
        }

        [Entity(SerializedName = "KIDS", SourceDataType = typeof(String), MaxLength = 1, TrueValue = "1", FalseValue = "0")]
        public bool Kids
        {
            get { return _kids; }
            set
            {
                if (_kids != value)
                {
                    _kids = value;
                    OnPropertyChanged("Kids");
                }
            }
        }

        [Entity(SerializedName = "KIND", MaxLength = 1, IsMandatory = true)]
        public string Gender
        {
            get { return _gender; }
            set
            {
                if (_gender != value)
                {
                    _gender = value;
                    OnPropertyChanged("Gender");
                }
            }
        }

        [Entity(SerializedName = "KTOU", MaxLength = 5, IsMandatory = true)]
        public string Holiday
        {
            get { return _holiday; }
            set
            {
                if (_holiday != value)
                {
                    _holiday = value;
                    OnPropertyChanged("Holiday");
                }
            }
        }

        [Entity(SerializedName = "LINO", MaxLength = 20, IsMandatory = true)]
        public string LicenseNumber
        {
            get { return _licenseNumber; }
            set
            {
                if (_licenseNumber != value)
                {
                    _licenseNumber = value;
                    OnPropertyChanged("LicenseNumber");
                }
            }
        }

        [Entity(SerializedName = "MAKR", MaxLength = 5, IsMandatory = true)]
        public string Ring
        {
            get { return _ring; }
            set
            {
                if (_ring != value)
                {
                    _ring = value;
                    OnPropertyChanged("Ring");
                }
            }
        }

        [Entity(SerializedName = "MATR", MaxLength = 5, IsMandatory = true)]
        public string Matricola
        {
            get { return _matricola; }
            set
            {
                if (_matricola != value)
                {
                    _matricola = value;
                    OnPropertyChanged("Matricola");
                }
            }
        }

        [Entity(SerializedName = "NATI", MaxLength = 3, IsMandatory = true)]
        public string Nationality
        {
            get { return _nationality; }
            set
            {
                if (_nationality != value)
                {
                    _nationality = value;
                    OnPropertyChanged("Nationality");
                }
            }
        }

        [Entity(SerializedName = "PDGL", MaxLength = 4, IsMandatory = true)]
        public string FullDuration  
        {
            get { return _fullDuration; }
            set
            {
                if (_fullDuration != value)
                {
                    _fullDuration = value;
                    OnPropertyChanged("FullDuration");
                }
            }
        }

        [Entity(SerializedName = "PDKL", MaxLength = 4, IsMandatory = true)]
        public string ShortDuration
        {
            get { return _shortDuration; }
            set
            {
                if (_shortDuration != value)
                {
                    _shortDuration = value;
                    OnPropertyChanged("ShortDuration");
                }
            }
        }

        [Entity(SerializedName = "PENO", MaxLength = 20, IsMandatory = true)]
        public string PersonalNumber
        {
            get { return __personalNumber; }
            set
            {
                if (__personalNumber != value)
                {
                    __personalNumber = value;
                    OnPropertyChanged("Nationality");
                }
            }
        }

        [Entity(SerializedName = "PERC", MaxLength = 5, IsMandatory = true)]
        public string MemberCode
        {
            get { return _memberCode; }
            set
            {
                if (_memberCode != value)
                {
                    _memberCode = value;
                    OnPropertyChanged("MemberCode");
                }
            }
        }

        [Entity(SerializedName = "PMAG", MaxLength = 2, IsMandatory = true)]
        public string FullNumber
        {
            get { return _fullNumber; }
            set
            {
                if (_fullNumber != value)
                {
                    _fullNumber = value;
                    OnPropertyChanged("FullNumber");
                }
            }
        }

        [Entity(SerializedName = "PMAK", MaxLength = 2, IsMandatory = true)]
        public string ShortNumber
        {
            get { return _shortNumber; }
            set
            {
                if (_shortNumber != value)
                {
                    _shortNumber = value;
                    OnPropertyChanged("ShortNumber");
                }
            }
        }

        [Entity(SerializedName = "PNOF", MaxLength = 10, IsMandatory = true)]
        public string EmployeeID
        {
            get { return _employeeID; }
            set
            {
                if (_employeeID != value)
                {
                    _employeeID = value;
                    OnPropertyChanged("EmployeeID");
                }
            }
        }

        [Entity(SerializedName = "PRFL", MaxLength = 1, IsMandatory = true)]
        public string Flag
        {
            get { return _flag; }
            set
            {
                if (_flag != value)
                {
                    _flag = value;
                    OnPropertyChanged("Flag");
                }
            }
        }

        [Entity(SerializedName = "REGI", MaxLength = 1, IsMandatory = true)]
        public string Indicator
        {
            get { return _indicator; }
            set
            {
                if (_indicator != value)
                {
                    _indicator = value;
                    OnPropertyChanged("Indicator");
                }
            }
        }

        [Entity(SerializedName = "RELS", MaxLength = 1, IsMandatory = true)]
        public string AutomaticRelease
        {
            get { return _automaticRelease; }
            set
            {
                if (_automaticRelease != value)
                {
                    _automaticRelease = value;
                    OnPropertyChanged("AutomaticRelease");
                }
            }
        }

        [Entity(SerializedName = "REMA", MaxLength = 60, IsMandatory = true)]
        public string Remark
        {
            get { return _remark; }
            set
            {
                if (_remark != value)
                {
                    _remark = value;
                    OnPropertyChanged("Remark");
                }
            }
        }

        [Entity(SerializedName = "SHNM", MaxLength = 12, IsMandatory = true)]
        public string ShortName
        {
            get { return _shortName; }
            set
            {
                if (_shortName != value)
                {
                    _shortName = value;
                    OnPropertyChanged("ShortName");
                }
            }
        }

        [Entity(SerializedName = "SKEN", MaxLength = 8, IsMandatory = true)]
        public string ShiftID
        {
            get { return _shiftID; }
            set
            {
                if (_shiftID != value)
                {
                    _shiftID = value;
                    OnPropertyChanged("ShiftID");
                }
            }
        }

        [Entity(SerializedName = "TELD", MaxLength = 60)]
        public string WorkTelephone
        {
            get { return _workTelephone; }
            set
            {
                if (_workTelephone != value)
                {
                    _workTelephone = value;
                    OnPropertyChanged("WorkTelephone");
                }
            }
        }

        [Entity(SerializedName = "TELH", MaxLength = 20)]
        public string MobileTelephone
        {
            get { return _mobileTelephone; }
            set
            {
                if (_mobileTelephone != value)
                {
                    _mobileTelephone = value;
                    OnPropertyChanged("MobileTelephone");
                }
            }
        }

        [Entity(SerializedName = "TELP", MaxLength = 20)]
        public string HomeTelephone
        {
            get { return _homeTelephone; }
            set
            {
                if (_homeTelephone != value)
                {
                    _homeTelephone = value;
                    OnPropertyChanged("HomeTelephone");
                }
            }
        }

        [Entity(SerializedName = "TOHR", MaxLength = 1)]
        public string SendToHR
        {
            get { return _sendToHR; }
            set
            {
                if (_sendToHR != value)
                {
                    _sendToHR = value;
                    OnPropertyChanged("SendToHR");
                }
            }
        }

        [Entity(SerializedName = "ZUTB", MaxLength = 5)]
        public string HolidayByDisability
        {
            get { return _holidayByDisability; }
            set
            {
                if (_holidayByDisability != value)
                {
                    _holidayByDisability = value;
                    OnPropertyChanged("SendToHR");
                }
            }
        }

        [Entity(SerializedName = "ZUTF", MaxLength = 5)]
        public string HolidayByExemptionWork
        {
            get { return _HolidayByExemptionWork; }
            set
            {
                if (_HolidayByExemptionWork != value)
                {
                    _HolidayByExemptionWork = value;
                    OnPropertyChanged("HolidayByExemptionWork");
                }
            }
        }

        [Entity(SerializedName = "ZUTN", MaxLength = 5)]
        public string HolidayByNightShift
        {
            get { return _holidayByNightShift; }
            set
            {
                if (_holidayByNightShift != value)
                {
                    _holidayByNightShift = value;
                    OnPropertyChanged("HolidayByNightShift");
                }
            }
        }

        [Entity(SerializedName = "ZUTW", MaxLength = 5)]
        public string HolidayByRotatingShift
        {
            get { return _holidayByRotatingShift; }
            set
            {
                if (_holidayByRotatingShift != value)
                {
                    _holidayByRotatingShift = value;
                    OnPropertyChanged("HolidayByRotatingShift");
                }
            }
        }

        [Entity(SerializedName = "FILT", MaxLength = 6)]
        public string FILT
        {
            get { return _fILT; }
            set
            {
                if (_fILT != value)
                {
                    _fILT = value;
                    OnPropertyChanged("FILT");
                }
            }
        }
    }
}
