﻿using System;

namespace Ufis.Entities
{
    public interface IIdentityEntity
    {
        int Urno { get; set; }
    }
}
