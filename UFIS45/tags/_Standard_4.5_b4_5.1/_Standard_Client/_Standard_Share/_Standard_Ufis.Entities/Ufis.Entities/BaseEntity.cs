﻿using System;

namespace Ufis.Entities
{
    public abstract class BaseEntity : NoIdBaseEntity, IIdentityEntity
    {
        private int _urno;

        [EntityAttribute(SerializedName = "URNO", IsPrimaryKey = true, IsReadOnly = true)]
        public int Urno 
        {
            get { return _urno; }
            set
            {
                if (_urno != value)
                {
                    _urno = value;
                    OnPropertyChanged("Urno");
                }
            }
        }

        public override bool Equals(object obj)
        {
            //Check for null and compare run-time types.
            if (obj == null || GetType() != obj.GetType()) return false;

            BaseEntity entity = (BaseEntity)obj;
            return (Urno == entity.Urno);
        }

        public override int GetHashCode()
        {
            return Urno.GetHashCode();
        }
    }
}
