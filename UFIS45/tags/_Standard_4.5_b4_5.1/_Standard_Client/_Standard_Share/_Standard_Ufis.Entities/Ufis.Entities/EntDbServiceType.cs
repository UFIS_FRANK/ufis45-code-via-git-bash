﻿using System;

namespace Ufis.Entities
{
    [Entity(SerializedName = "STYTAB")]
    public class EntDbServiceType : ValidityBaseEntity
    {
        private string _code;
        private string _name;

        [Entity(SerializedName = "STYP", MaxLength = 2, IsMandatory = true, IsUnique = true)]
        public string Code
        {
            get { return _code; }
            set
            {
                if (_code != value)
                {
                    _code = value;
                    OnPropertyChanged("Code");
                }
            }
        }

        [Entity(SerializedName = "SNAM", MaxLength = 30)]
        public string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    _name = value;
                    OnPropertyChanged("Name");
                }
            }
        }
    }
}
