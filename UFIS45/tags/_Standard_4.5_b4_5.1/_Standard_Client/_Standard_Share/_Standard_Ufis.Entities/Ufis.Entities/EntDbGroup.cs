﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ufis.Entities
{
    [Entity(SerializedName = "GRPTAB")]
    public class EntDbGroup : ValidityBaseEntity
    {
        private int _groupOrProfileUrno;
        private int _memberUrno;
        private string _type;
        private string _status;

        [Entity(SerializedName = "FREL", MaxLength = 10)]
        public int GroupOrProfileUrno
        {
            get { return _groupOrProfileUrno; }
            set
            {
                if (_groupOrProfileUrno != value)
                {
                    _groupOrProfileUrno = value;
                    OnPropertyChanged("GroupOrProfileUrno");
                }
            }
        }

        [Entity(SerializedName = "FSEC", MaxLength = 10)]
        public int MemberUrno
        {
            get { return _memberUrno; }
            set
            {
                if (_memberUrno != value)
                {
                    _memberUrno = value;
                    OnPropertyChanged("MemberUrno");
                }
            }
        }

        [Entity(SerializedName = "STAT", MaxLength = 1)]
        public string Status
        {
            get { return _status; }
            set
            {
                if (_status != value)
                {
                    _status = value;
                    OnPropertyChanged("Status");
                }
            }
        }

        [Entity(SerializedName = "TYPE", MaxLength = 1)]
        public string Type
        {
            get { return _type; }
            set
            {
                if (_type != value)
                {
                    _type = value;
                    OnPropertyChanged("Type");
                }
            }
        }
    }
}
