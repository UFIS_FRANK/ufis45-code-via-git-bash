﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ufis.Entities
{
    [Entity(SerializedName = "ALTTAB")]
    public class EntDbAirline : ValidityBaseEntity
    {
        private string _airlineIATACode;
        private string _airlineICAOCode;

        [Entity(SerializedName = "ALC2", MaxLength = 2)]
        public string AirlineIATACode 
        {
            get { return _airlineIATACode; }
            set
            {
                if (_airlineIATACode != value)
                {
                    _airlineIATACode = value;
                    OnPropertyChanged("AirlineIATACode");
                }
            }
        }

        [Entity(SerializedName = "ALC3", MaxLength = 3)]
        public string AirlineICAOCode
        {
            get { return _airlineICAOCode; }
            set
            {
                if (_airlineICAOCode != value)
                {
                    _airlineICAOCode = value;
                    OnPropertyChanged("AirlineICAOCode");
                }
            }
        }
    }
}
