﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ufis.Entities
{
    [Entity(SerializedName = "PARTAB")]
    public class EntDbParameter : BaseEntity
    {
        private string _ApplName = "";
        private DateTime? _CreateDate;
        private string _Name = "";
        private string _UniqParIdentifier = "";
        private string _ParType = "";
        private string _TextIden = "";
        private string _EvaMode = "";
        private string _Valu = "";

        
        [Entity(SerializedName = "APPL", MaxLength = 8)]
        /// <summary>
        /// Application Name {Field = APPL}
        /// </summary>
        public string ApplicationName
        {
            get { return _ApplName; }
            set
            {
                if (_ApplName != value)
                {
                    _ApplName = value;
                    OnPropertyChanged("ApplicationName");
                }
            }
        }

        [Entity(SerializedName = "CDAT")]
        /// <summary>
        /// Creation Date {Field = CDAT}
        /// </summary>
        public DateTime? CreateDate
        {
            get { return _CreateDate; }
            set
            {
                if (_CreateDate != value)
                {
                    _CreateDate = value;
                    OnPropertyChanged("CreateDate");
                }
            }
        }
        [Entity(SerializedName = "NAME",MaxLength=100)]
        /// <summary>
        /// Name or Default Text {Field = NAME}
        /// </summary>
        public string NameOfParameter
        {
            get { return _Name; }
            set
            {
                if (_Name  != value)
                {
                    _Name = value;
                    OnPropertyChanged("NameOfParameter");
                }
            }
        }
        [Entity(SerializedName = "PAID", MaxLength = 16)]
        /// <summary>
        /// Parameter Identifier (Unique) {Field = PAID}
        /// </summary>
        public string ParameterIdentifier
        {
            get { return _UniqParIdentifier; }
            set
            {
                if (_UniqParIdentifier != value)
                {
                    _UniqParIdentifier = value;
                    OnPropertyChanged("ParameterIdentifier");
                }
            }
        }
        [Entity(SerializedName = "PTYP", MaxLength = 16)]
        /// <summary>
        /// Parameter Type (Working Rules, General, etc) {Field = PTYP}
        /// </summary>
        public string ParameterType
        {
            get { return _ParType; }
            set
            {
                if (_ParType != value)
                {
                    _ParType = value;
                    OnPropertyChanged("ParameterType");
                }
            }
        }
        [Entity(SerializedName = "TXID", MaxLength = 32)]
        /// <summary>
        /// Text Identifier for TXTTAB {Field = TXID}
        /// </summary>
        public string TextIdentifier
        {
            get { return _TextIden; }
            set
            {
                if (_TextIden != value)
                {
                    _TextIden = value;
                    OnPropertyChanged("TextIdentifier");
                }
            }
        }
        [Entity(SerializedName = "TYPE", MaxLength = 4)]
        /// <summary>
        /// Evaluation Mode of Parameter(Trim, Date, Long, Text, etc.) {Field = TYPE}
        /// </summary>
        public string EvaluationMode
        {
            get { return _EvaMode; }
            set
            {
                if (_EvaMode != value)
                {
                    _EvaMode = value;
                    OnPropertyChanged("EvaluationMode");
                }
            }
        }
        [Entity(SerializedName = "VALU", MaxLength = 64)]
        /// <summary>
        /// Value {Field = VALU}
        /// </summary>
        public string Value
        {
            get { return _Valu; }
            set
            {
                if (_Valu != value)
                {
                    _Valu = value;
                    OnPropertyChanged("Value");
                }
            }
        }
    }
}
