﻿#region ===== Source Code Information =====
/*
Project Name    : Ufis.Entities
Project Type    : Class Library
Platform        : Microsoft Visual C#.NET
File Name       : EntDbFlight.cs

Version         : 1.0.0
Created Date    : 
Complete Date   : 
Developed By    : 

Date Reason Updated By
---------------------------------------------------------------------------------------------------------------------------------------------
 * File Version - 4.5.0.6
 * Update by    - Phyoe Khaing Min
 * Updated On   - 25-Apr-2012
 * Description  - Added CKIF, CKIT Fields
 
---------------------------------------------------------------------------------------------------------------------------------------------

Copyright (C) All rights reserved.
*/
#endregion
using System;

namespace Ufis.Entities
{
    [EntityAttribute(SerializedName = "AFTTAB")]
    public class EntDbFlight : BaseEntity
    {
        private string _aircraftIATACode;
        private string _aircraftICAOCode;
        private string _aircraftICAOCodeModified;
        private string _arrivalDepartureId;
        private DateTime? _airborneTime;
        private string _airlineIATACode;
        private string _airlineICAOCode;
        private int _urnoOfArrivalFlight;
        private string _belt1;
        private string _belt2;
        private string _callSign;
        private string _delayCodeOfDeparture1;
        private string _delayCodeOfDeparture2;
        private DateTime? _delayTimeOfDeparture1;
        private DateTime? _delayTimeOfDeparture2;
        private string _destinationAirportIATACode;
        private string _destinationAirportICAOCode;
        private DateTime? _bestEstimatedArrivalTime;
        private DateTime? _bestEstimatedDepartureTime;
        private string _fullFlightNumber;
        private string _internationalDomesticId;
        private string _operationalType;
        private DateTime? _arrivalScheduleBeginForGate1;
        private DateTime? _arrivalScheduleBeginForGate2;
        private DateTime? _arrivalActualBeginForGate1;
        private DateTime? _arrivalActualBeginForGate2;
        private DateTime? _arrivalScheduleEndForGate1;
        private DateTime? _arrivalScheduleEndForGate2;
        private DateTime? _arrivalActualEndForGate1;
        private DateTime? _arrivalActualEndForGate2;
        private DateTime? _departureScheduleBeginForGate1;
        private DateTime? _departureScheduleBeginForGate2;
        private DateTime? _departureActualBeginForGate1;
        private DateTime? _departureActualBeginForGate2;
        private DateTime? _departureScheduleEndForGate1;
        private DateTime? _departureScheduleEndForGate2;
        private DateTime? _departureActualEndForGate1;
        private DateTime? _departureActualEndForGate2;
        private string _gateOfArrival1;
        private string _gateOfArrival2;
        private string _gateOfDeparture1;
        private string _gateOfDeparture2;
        private string _handlingAgentList;
        private DateTime? _landingTime;
        private int? _numberOfSeats;
        private DateTime? _offblockTime;
        private DateTime? _onblockTime;
        private string _originAirportIATACode;
        private string _originAirportICAOCode;
        private int? _numberOfPassengers1;
        private int? _numberOfPassengers2;
        private int? _numberOfPassengers3;
        private int? _totalNumberOfPassengers;
        private string _positionOfArrival;
        private string _positionOfDeparture;
        private string _registrationNumber;
        private string _remark1;
        private string _remark2;
        private int? _relationKey;
        private string _rotationType;
        private string _runwayOfArrival;
        private string _runwayOfDeparture;
        private string _status;
        private DateTime? _standardTimeOfArrival;
        private DateTime? _standardTimeOfDeparture;
        private string _terminalGateOfArrival1;
        private string _terminalGateOfArrival2;
        private string _terminalGateOfDeparture1;
        private string _terminalGateOfDeparture2;
        private DateTime? _timeframeOfArrival;
        private DateTime? _timeframeOfDeparture;
        private string _natureCode;
        private string _versionOfSchedule;
        private string _viaAirportIATACode;
        private string _viaAirportICAOCode;
        private string _viaAirportList;
        private string _viaAirportCount;
        private DateTime? _targetOffblockTime;
        private bool _readyForTowing;
        private string _CheckInCFrom;
        private string _CheckInCTo;
        private int? _totalNumberOfBaggages;
        private string _terminalBaggageBelt1;


        [Entity(SerializedName = "ACT3", MaxLength = 3)]
        public virtual string AircraftIATACode
        {
            get { return _aircraftIATACode; }
            set
            {
                if (_aircraftIATACode != value)
                {
                    _aircraftIATACode = value;
                    OnPropertyChanged("AircraftIATACode");
                }
            }
        }

        [Entity(SerializedName = "ACT5", MaxLength = 5)]
        public virtual string AircraftICAOCode
        {
            get { return _aircraftICAOCode; }
            set
            {
                if (_aircraftICAOCode != value)
                {
                    _aircraftICAOCode = value;
                    OnPropertyChanged("AircraftICAOCode");
                }
            }
        }

        [Entity(SerializedName = "ACTI", MaxLength = 5)]
        public virtual string AircraftICAOCodeModified
        {
            get { return _aircraftICAOCodeModified; }
            set
            {
                if (_aircraftICAOCodeModified != value)
                {
                    _aircraftICAOCodeModified = value;
                    OnPropertyChanged("AircraftICAOCodeModified");
                }
            }
        }

        //[Field(SerializedName = "ADER")]

        [Entity(SerializedName = "ADID", MaxLength = 1)]
        public virtual string ArrivalDepartureId
        {
            get { return _arrivalDepartureId; }
            set
            {
                if (_arrivalDepartureId != value)
                {
                    _arrivalDepartureId = value;
                    OnPropertyChanged("ArrivalDepartureId");
                }
            }
        }

        //[Field(SerializedName = "AIRA")]

        [Entity(SerializedName = "AIRB", SourceDataType = typeof(String))]
        public virtual DateTime? AirborneTime
        {
            get { return _airborneTime; }
            set
            {
                if (_airborneTime != value)
                {
                    _airborneTime = value;
                    OnPropertyChanged("AirborneTime");
                }
            }
        }

        //[Field(SerializedName = "AIRD")]
        //[Field(SerializedName = "AIRE")]
        //[Field(SerializedName = "AIRU")]

        [Entity(SerializedName = "ALC2", MaxLength = 2)]
        public virtual string AirlineIATACode
        {
            get { return _airlineIATACode; }
            set
            {
                if (_airlineIATACode != value)
                {
                    _airlineIATACode = value;
                    OnPropertyChanged("AirlineIATACode");
                }
            }
        }

        [Entity(SerializedName = "ALC3", MaxLength = 3)]
        public virtual string AirlineICAOCode
        {
            get { return _airlineICAOCode; }
            set
            {
                if (_airlineICAOCode != value)
                {
                    _airlineICAOCode = value;
                    OnPropertyChanged("AirlineICAOCode");
                }
            }
        }

        //[Field(SerializedName = "ANNX")]

        [Entity(SerializedName = "AURN", MaxLength = 10, SourceDataType = typeof(String))]
        public virtual int UrnoOfArrivalFlight
        {
            get { return _urnoOfArrivalFlight; }
            set
            {
                if (_urnoOfArrivalFlight != value)
                {
                    _urnoOfArrivalFlight = value;
                    OnPropertyChanged("UrnoOfArrivalFlight");
                }
            }
        }

        //[Field(SerializedName = "B1BA")]
        //[Field(SerializedName = "B1BS")]
        //[Field(SerializedName = "B1EA")]
        //[Field(SerializedName = "B1ES")]
        //[Field(SerializedName = "B2BA")]
        //[Field(SerializedName = "B2BS")]
        //[Field(SerializedName = "B2EA")]
        //[Field(SerializedName = "B2ES")]
        //[Field(SerializedName = "B3FP")]
        //[Field(SerializedName = "BAA3")]
        //[Field(SerializedName = "BAC1")]
        //[Field(SerializedName = "BAC2")]
        //[Field(SerializedName = "BAC3")]
        //[Field(SerializedName = "BAC4")]
        //[Field(SerializedName = "BAC5")]
        //[Field(SerializedName = "BAC6")]
        //[Field(SerializedName = "BAE1")]
        //[Field(SerializedName = "BAE2")]
        //[Field(SerializedName = "BAE3")]
        //[Field(SerializedName = "BAE4")]
        //[Field(SerializedName = "BAE5")]
        //[Field(SerializedName = "BAE6")]
        //[Field(SerializedName = "BAG0")]
        //[Field(SerializedName = "BAGN")]
        //[Field(SerializedName = "BAGW")]
        //[Field(SerializedName = "BAO1")]
        //[Field(SerializedName = "BAO2")]
        //[Field(SerializedName = "BAO3")]
        //[Field(SerializedName = "BAO4")]
        //[Field(SerializedName = "BAO5")]
        //[Field(SerializedName = "BAO6")]
        //[Field(SerializedName = "BAS1")]
        //[Field(SerializedName = "BAS2")]
        //[Field(SerializedName = "BAS3")]
        //[Field(SerializedName = "BAS4")]
        //[Field(SerializedName = "BAS5")]
        //[Field(SerializedName = "BAS6")]
        //[Field(SerializedName = "BAST")]
        //[Field(SerializedName = "BAZ1")]
        //[Field(SerializedName = "BAZ2")]
        //[Field(SerializedName = "BAZ3")]
        //[Field(SerializedName = "BAZ4")]
        //[Field(SerializedName = "BAZ5")]
        //[Field(SerializedName = "BAZ6")]
        //[Field(SerializedName = "BBAA")]
        //[Field(SerializedName = "BBFA")]

        [Entity(SerializedName = "BLT1", MaxLength = 5)]
        public virtual string Belt1
        {
            get { return _belt1; }
            set
            {
                if (_belt1 != value)
                {
                    _belt1 = value;
                    OnPropertyChanged("Belt1");
                }
            }
        }

        [Entity(SerializedName = "BLT2", MaxLength = 5)]
        public virtual string Belt2
        {
            get { return _belt2; }
            set
            {
                if (_belt2 != value)
                {
                    _belt2 = value;
                    OnPropertyChanged("Belt2");
                }
            }
        }

        //[Field(SerializedName = "BOAC")]
        //[Field(SerializedName = "BOAO")]
        //[Field(SerializedName = "CGOT")]
        //[Field(SerializedName = "CHGI")]
        //[Field(SerializedName = "CHT3")]
        //[Field(SerializedName = "CKIF")]

        [Entity(SerializedName = "CKIF", MaxLength = 5)]
        public virtual string CheckInCFrom
        {
            get { return _CheckInCFrom; }
            set
            {
                if (_CheckInCFrom != value)
                {
                    _CheckInCFrom = value;
                    OnPropertyChanged("CheckInCFrom");
                }
            }
        }

        [Entity(SerializedName = "CKIT", MaxLength = 5)]
        public virtual string CheckInCTo
        {
            get { return _CheckInCTo; }
            set
            {
                if (_CheckInCTo != value)
                {
                    _CheckInCTo = value;
                    OnPropertyChanged("CheckInCTo");
                }
            }
        }
        //[Field(SerializedName = "CONF")]
        [Entity(SerializedName = "CSGN", MaxLength = 8)]
        public virtual string CallSign
        {
            get { return _callSign; }
            set
            {
                if (_callSign != value)
                {
                    _callSign = value;
                    OnPropertyChanged("CallSign");
                }
            }
        }

        //[Field(SerializedName = "CTOT")]

        [Entity(SerializedName = "DCD1", MaxLength = 2)]
        public virtual string DelayCodeOfDeparture1
        {
            get { return _delayCodeOfDeparture1; }
            set
            {
                if (_delayCodeOfDeparture1 != value)
                {
                    _delayCodeOfDeparture1 = value;
                    OnPropertyChanged("DelayCodeOfDeparture1");
                }
            }
        }

        [Entity(SerializedName = "DCD2", MaxLength = 2)]
        public virtual string DelayCodeOfDeparture2
        {
            get { return _delayCodeOfDeparture2; }
            set
            {
                if (_delayCodeOfDeparture2 != value)
                {
                    _delayCodeOfDeparture2 = value;
                    OnPropertyChanged("DelayCodeOfDeparture2");
                }
            }
        }

        //[Field(SerializedName = "DDLF")]
        //[Field(SerializedName = "DDLR")]
        //[Field(SerializedName = "DELA")]
        //[Field(SerializedName = "DELD")]

        [Entity(SerializedName = "DES3", MaxLength = 3)]
        public virtual string DestinationAirportIATACode
        {
            get { return _destinationAirportIATACode; }
            set
            {
                if (_destinationAirportIATACode != value)
                {
                    _destinationAirportIATACode = value;
                    OnPropertyChanged("DestinationAirportIATACode");
                }
            }
        }

        [Entity(SerializedName = "DES4", MaxLength = 4)]
        public virtual string DestinationAirportICAOCode
        {
            get { return _destinationAirportICAOCode; }
            set
            {
                if (_destinationAirportICAOCode != value)
                {
                    _destinationAirportICAOCode = value;
                    OnPropertyChanged("DestinationAirportICAOCode");
                }
            }
        }

        //[Field(SerializedName = "DIVR")]
        //[Field(SerializedName = "DOOA")]
        //[Field(SerializedName = "DOOD")]
        //[Field(SerializedName = "DSSF")]

        [Entity(SerializedName = "DTD1", SourceDataType = typeof(string))]
        public virtual DateTime? DelayTimeOfDeparture1
        {
            get { return _delayTimeOfDeparture1; }
            set
            {
                if (_delayTimeOfDeparture1 != value)
                {
                    _delayTimeOfDeparture1 = value;
                    OnPropertyChanged("DelayTimeOfDeparture1");
                }
            }
        }

        [Entity(SerializedName = "DTD2", SourceDataType = typeof(string))]
        public virtual DateTime? DelayTimeOfDeparture2
        {
            get { return _delayTimeOfDeparture2; }
            set
            {
                if (_delayTimeOfDeparture2 != value)
                {
                    _delayTimeOfDeparture2 = value;
                    OnPropertyChanged("DelayTimeOfDeparture2");
                }
            }
        }

        //[Field(SerializedName = "ENTY")]
        //[Field(SerializedName = "ETAA")]
        //[Field(SerializedName = "ETAC")]
        //[Field(SerializedName = "ETAE")]

        [Entity(SerializedName = "ETAI", SourceDataType = typeof(String))]
        public virtual DateTime? BestEstimatedArrivalTime
        {
            get { return _bestEstimatedArrivalTime; }
            set
            {
                if (_bestEstimatedArrivalTime != value)
                {
                    _bestEstimatedArrivalTime = value;
                    OnPropertyChanged("BestEstimatedArrivalTime");
                }
            }
        }

        //[Field(SerializedName = "ETAS")]
        //[Field(SerializedName = "ETAU")]
        //[Field(SerializedName = "ETDA")]
        //[Field(SerializedName = "ETDC")]
        //[Field(SerializedName = "ETDE")]

        [Entity(SerializedName = "ETDI", SourceDataType = typeof(String))]
        public virtual DateTime? BestEstimatedDepartureTime
        {
            get { return _bestEstimatedDepartureTime; }
            set
            {
                if (_bestEstimatedDepartureTime != value)
                {
                    _bestEstimatedDepartureTime = value;
                    OnPropertyChanged("BestEstimatedDepartureTime");
                }
            }
        }

        //[Field(SerializedName = "ETDS")]
        //[Field(SerializedName = "ETDU")]
        //[Field(SerializedName = "ETOA")]
        //[Field(SerializedName = "ETOD")]
        //[Field(SerializedName = "EXT1")]
        //[Field(SerializedName = "EXT2")]
        //[Field(SerializedName = "FCAL")]
        //[Field(SerializedName = "FDAT")]
        //[Field(SerializedName = "FKEY")]
        //[Field(SerializedName = "FLDA")]

        [Entity(SerializedName = "FLNO", MaxLength = 9)]
        public virtual string FullFlightNumber
        {
            get { return _fullFlightNumber; }
            set
            {
                if (_fullFlightNumber != value)
                {
                    _fullFlightNumber = value;
                    OnPropertyChanged("FullFlightNumber");
                }
            }
        }

        //[Field(SerializedName = "FLNS")]
        //[Field(SerializedName = "FLTC")]

        [Entity(SerializedName = "FLTI", MaxLength = 1)]
        public virtual string InternationalDomesticId
        {
            get { return _internationalDomesticId; }
            set
            {
                if (_internationalDomesticId != value)
                {
                    _internationalDomesticId = value;
                    OnPropertyChanged("InternationalDomesticId");
                }
            }
        }

        //[Field(SerializedName = "FLTN")]
        //[Field(SerializedName = "FLTO")]
        //[Field(SerializedName = "FREA")]
        //[Field(SerializedName = "FRED")]

        [Entity(SerializedName = "FTYP", MaxLength = 1)]
        public virtual string OperationalType
        {
            get { return _operationalType; }
            set
            {
                if (_operationalType != value)
                {
                    _operationalType = value;
                    OnPropertyChanged("OperationalType");
                }
            }
        }

        [Entity(SerializedName = "GA1B", SourceDataType = typeof(string))]
        public virtual DateTime? ArrivalScheduleBeginForGate1
        {
            get { return _arrivalScheduleBeginForGate1; }
            set
            {
                if (_arrivalScheduleBeginForGate1 != value)
                {
                    _arrivalScheduleBeginForGate1 = value;
                    OnPropertyChanged("ArrivalScheduleBeginForGate1");
                }
            }
        }

        [Entity(SerializedName = "GA1E", SourceDataType = typeof(string))]
        public virtual DateTime? ArrivalScheduleEndForGate1
        {
            get { return _arrivalScheduleEndForGate1; }
            set
            {
                if (_arrivalScheduleEndForGate1 != value)
                {
                    _arrivalScheduleEndForGate1 = value;
                    OnPropertyChanged("ArrivalScheduleEndForGate1");
                }
            }
        }

        [Entity(SerializedName = "GA1X", SourceDataType = typeof(string))]
        public virtual DateTime? ArrivalActualBeginForGate1
        {
            get { return _arrivalActualBeginForGate1; }
            set
            {
                if (_arrivalActualBeginForGate1 != value)
                {
                    _arrivalActualBeginForGate1 = value;
                    OnPropertyChanged("ArrivalActualBeginForGate1");
                }
            }
        }

        [Entity(SerializedName = "GA1Y", SourceDataType = typeof(string))]
        public virtual DateTime? ArrivalActualEndForGate1
        {
            get { return _arrivalActualEndForGate1; }
            set
            {
                if (_arrivalActualEndForGate1 != value)
                {
                    _arrivalScheduleEndForGate1 = value;
                    OnPropertyChanged("ArrivalScheduleEndForGate1");
                }
            }
        }

        [Entity(SerializedName = "GD2B", SourceDataType = typeof(string))]
        public virtual DateTime? ArrivalScheduleBeginForGate2
        {
            get { return _arrivalScheduleBeginForGate2; }
            set
            {
                if (_arrivalScheduleBeginForGate2 != value)
                {
                    _arrivalScheduleBeginForGate2 = value;
                    OnPropertyChanged("ArrivalScheduleBeginForGate2");
                }
            }
        }

        [Entity(SerializedName = "GD2E", SourceDataType = typeof(string))]
        public virtual DateTime? ArrivalScheduleEndForGate2
        {
            get { return _arrivalScheduleEndForGate2; }
            set
            {
                if (_arrivalScheduleEndForGate2 != value)
                {
                    _arrivalScheduleEndForGate2 = value;
                    OnPropertyChanged("ArrivalScheduleEndForGate2");
                }
            }
        }

        [Entity(SerializedName = "GD2X", SourceDataType = typeof(string))]
        public virtual DateTime? ArrivalActualBeginForGate2
        {
            get { return _arrivalActualBeginForGate2; }
            set
            {
                if (_arrivalActualBeginForGate2 != value)
                {
                    _arrivalActualBeginForGate2 = value;
                    OnPropertyChanged("ArrivalActualBeginForGate2");
                }
            }
        }

        [Entity(SerializedName = "GD2Y", SourceDataType = typeof(string))]
        public virtual DateTime? ArrivalActualEndForGate2
        {
            get { return _arrivalActualEndForGate2; }
            set
            {
                if (_arrivalActualEndForGate2 != value)
                {
                    _arrivalScheduleEndForGate2 = value;
                    OnPropertyChanged("ArrivalScheduleEndForGate2");
                }
            }
        }


        //[Field(SerializedName = "GA2F")]


        [Entity(SerializedName = "GD1B", SourceDataType = typeof(string))]
        public virtual DateTime? DepartureScheduleBeginForGate1
        {
            get { return _departureScheduleBeginForGate1; }
            set
            {
                if (_departureScheduleBeginForGate1 != value)
                {
                    _departureScheduleBeginForGate1 = value;
                    OnPropertyChanged("DepartureScheduleBeginForGate1");
                }
            }
        }

        [Entity(SerializedName = "GD1E", SourceDataType = typeof(string))]
        public virtual DateTime? DepartureScheduleEndForGate1
        {
            get { return _departureScheduleEndForGate1; }
            set
            {
                if (_departureScheduleEndForGate1 != value)
                {
                    _departureScheduleEndForGate1 = value;
                    OnPropertyChanged("DepartureScheduleEndForGate1");
                }
            }
        }

        [Entity(SerializedName = "GD1X", SourceDataType = typeof(string))]
        public virtual DateTime? DepartureActualBeginForGate1
        {
            get { return _departureActualBeginForGate1; }
            set
            {
                if (_departureActualBeginForGate1 != value)
                {
                    _departureActualBeginForGate1 = value;
                    OnPropertyChanged("DepartureActualBeginForGate1");
                }
            }
        }

        [Entity(SerializedName = "GD1Y", SourceDataType = typeof(string))]
        public virtual DateTime? DepartureActualEndForGate1
        {
            get { return _departureActualEndForGate1; }
            set
            {
                if (_departureActualEndForGate1 != value)
                {
                    _departureActualEndForGate1 = value;
                    OnPropertyChanged("DepartureActualEndForGate1");
                }
            }
        }

        [Entity(SerializedName = "GD2B", SourceDataType = typeof(string))]
        public virtual DateTime? DepartureScheduleBeginForGate2
        {
            get { return _departureScheduleBeginForGate2; }
            set
            {
                if (_departureScheduleBeginForGate2 != value)
                {
                    _departureScheduleBeginForGate2 = value;
                    OnPropertyChanged("DepartureScheduleBeginForGate2");
                }
            }
        }

        [Entity(SerializedName = "GD2E", SourceDataType = typeof(string))]
        public virtual DateTime? DepartureScheduleEndForGate2
        {
            get { return _departureScheduleEndForGate2; }
            set
            {
                if (_departureScheduleEndForGate2 != value)
                {
                    _departureScheduleEndForGate2 = value;
                    OnPropertyChanged("DepartureScheduleEndForGate2");
                }
            }
        }

        [Entity(SerializedName = "GD2X", SourceDataType = typeof(string))]
        public virtual DateTime? DepartureActualBeginForGate2
        {
            get { return _departureActualBeginForGate2; }
            set
            {
                if (_departureActualBeginForGate2 != value)
                {
                    _departureActualBeginForGate2 = value;
                    OnPropertyChanged("DepartureActualBeginForGate2");
                }
            }
        }

        [Entity(SerializedName = "GD2Y", SourceDataType = typeof(string))]
        public virtual DateTime? DepartureActualEndForGate2
        {
            get { return _departureActualEndForGate2; }
            set
            {
                if (_departureActualEndForGate2 != value)
                {
                    _departureActualEndForGate2 = value;
                    OnPropertyChanged("DepartureActualEndForGate2");
                }
            }
        }


        //[Field(SerializedName = "GD1B")]
        //[Field(SerializedName = "GD1E")]
        //[Field(SerializedName = "GD1X")]
        //[Field(SerializedName = "GD1Y")]
        //[Field(SerializedName = "GD2B")]
        //[Field(SerializedName = "GD2E")]
        //[Field(SerializedName = "GD2X")]
        //[Field(SerializedName = "GD2Y")]

        [Entity(SerializedName = "GTA1", MaxLength = 5)]
        public virtual string GateOfArrival1
        {
            get { return _gateOfArrival1; }
            set
            {
                if (_gateOfArrival1 != value)
                {
                    _gateOfArrival1 = value;
                    OnPropertyChanged("GateOfArrival1");
                }
            }
        }

        [Entity(SerializedName = "GTA2", MaxLength = 5)]
        public virtual string GateOfArrival2
        {
            get { return _gateOfArrival2; }
            set
            {
                if (_gateOfArrival2 != value)
                {
                    _gateOfArrival2 = value;
                    OnPropertyChanged("GateOfArrival2");
                }
            }
        }

        [EntityAttribute(SerializedName = "GTD1", MaxLength = 5)]
        public virtual string GateOfDeparture1
        {
            get { return _gateOfDeparture1; }
            set
            {
                if (_gateOfDeparture1 != value)
                {
                    _gateOfDeparture1 = value;
                    OnPropertyChanged("GateOfDeparture1");
                }
            }
        }

        [Entity(SerializedName = "GTD2", MaxLength = 5)]
        public virtual string GateOfDeparture2
        {
            get { return _gateOfDeparture2; }
            set
            {
                if (_gateOfDeparture2 != value)
                {
                    _gateOfDeparture2 = value;
                    OnPropertyChanged("GateOfDeparture2");
                }
            }
        }

        //[Field(SerializedName = "HACA")]
        //[Field(SerializedName = "HACG")]
        //[Field(SerializedName = "HACI")]
        //[Field(SerializedName = "HACL")]
        //[Field(SerializedName = "HADL")]
        //[Field(SerializedName = "HALF")]
        //[Field(SerializedName = "HAMM")]
        //[Field(SerializedName = "HAPX")]
        //[Field(SerializedName = "HARA")]

        [Entity(SerializedName = "HDLL", MaxLength = 40)]
        public virtual string HandlingAgentList
        {
            get { return _handlingAgentList; }
            set
            {
                if (_handlingAgentList != value)
                {
                    _handlingAgentList = value;
                    OnPropertyChanged("HandlingAgentList");
                }
            }
        }

        //[Field(SerializedName = "HTYP")]
        //[Field(SerializedName = "IFRA")]
        //[Field(SerializedName = "IFRD")]
        //[Field(SerializedName = "ISKD")]
        //[Field(SerializedName = "ISRE")]
        //[Field(SerializedName = "JCNT")]
        //[Field(SerializedName = "JFNO")]

        [Entity(SerializedName = "LAND", SourceDataType = typeof(String))]
        public virtual DateTime? LandingTime
        {
            get { return _landingTime; }
            set
            {
                if (_landingTime != value)
                {
                    _landingTime = value;
                    OnPropertyChanged("LandingTime");
                }
            }
        }

        //[Field(SerializedName = "LNDA")]
        //[Field(SerializedName = "LNDD")]
        //[Field(SerializedName = "LNDU")]
        //[Field(SerializedName = "MAIL")]
        //[Field(SerializedName = "MING")]
        //[Field(SerializedName = "MTOW")]

        [Entity(SerializedName = "NOSE", MaxLength = 3, SourceDataType = typeof(String))]
        public virtual int? NumberOfSeats
        {
            get { return _numberOfSeats; }
            set
            {
                if (_numberOfSeats != value)
                {
                    _numberOfSeats = value;
                    OnPropertyChanged("NumberOfSeats");
                }
            }
        }

        //[Field(SerializedName = "NXTI")]
        //[Field(SerializedName = "OFBD")]

        [Entity(SerializedName = "OFBL", SourceDataType = typeof(String))]
        public virtual DateTime? OffblockTime
        {
            get { return _offblockTime; }
            set
            {
                if (_offblockTime != value)
                {
                    _offblockTime = value;
                    OnPropertyChanged("OffblockTime");
                }
            }
        }

        //[Field(SerializedName = "OFBS")]
        //[Field(SerializedName = "OFBU")]
        //[Field(SerializedName = "OFLB")]
        //[Field(SerializedName = "OFLC")]
        //[Field(SerializedName = "OFLM")]
        //[Field(SerializedName = "ONBD")]
        //[Field(SerializedName = "ONBE")]

        [Entity(SerializedName = "ONBL", SourceDataType = typeof(String))]
        public virtual DateTime? OnblockTime
        {
            get { return _onblockTime; }
            set
            {
                if (_onblockTime != value)
                {
                    _onblockTime = value;
                    OnPropertyChanged("OnblockTime");
                }
            }
        }

        //[Field(SerializedName = "ONBS")]
        //[Field(SerializedName = "ONBU")]

        [Entity(SerializedName = "ORG3", MaxLength = 3)]
        public virtual string OriginAirportIATACode
        {
            get { return _originAirportIATACode; }
            set
            {
                if (_originAirportIATACode != value)
                {
                    _originAirportIATACode = value;
                    OnPropertyChanged("OriginAirportIATACode");
                }
            }
        }

        [Entity(SerializedName = "ORG4", MaxLength = 4)]
        public virtual string OriginAirportICAOCode
        {
            get { return _originAirportICAOCode; }
            set
            {
                if (_originAirportICAOCode != value)
                {
                    _originAirportICAOCode = value;
                    OnPropertyChanged("OriginAirportICAOCode");
                }
            }
        }

        //[Field(SerializedName = "PABA")]
        //[Field(SerializedName = "PABS")]
        //[Field(SerializedName = "PAEA")]
        //[Field(SerializedName = "PAES")]
        //[Field(SerializedName = "PAID")]

        [Entity(SerializedName = "PAX1", MaxLength = 3, SourceDataType = typeof(String))]
        public virtual int? NumberOfPassengers1
        {
            get { return _numberOfPassengers1; }
            set
            {
                if (_numberOfPassengers1 != value)
                {
                    _numberOfPassengers1 = value;
                    OnPropertyChanged("NumberOfPassengers1");
                }
            }
        }

        [Entity(SerializedName = "PAX2", MaxLength = 3, SourceDataType = typeof(String))]
        public virtual int? NumberOfPassengers2
        {
            get { return _numberOfPassengers2; }
            set
            {
                if (_numberOfPassengers2 != value)
                {
                    _numberOfPassengers2 = value;
                    OnPropertyChanged("NumberOfPassengers2");
                }
            }
        }

        [Entity(SerializedName = "PAX3", MaxLength = 3, SourceDataType = typeof(String))]
        public virtual int? NumberOfPassengers3
        {
            get { return _numberOfPassengers3; }
            set
            {
                if (_numberOfPassengers3 != value)
                {
                    _numberOfPassengers3 = value;
                    OnPropertyChanged("NumberOfPassengers3");
                }
            }
        }

        //[Field(SerializedName = "PAXF")]
        //[Field(SerializedName = "PAXI")]

        [Entity(SerializedName = "PAXT", MaxLength = 3, SourceDataType = typeof(String))]
        public virtual int? TotalNumberOfPassengers
        {
            get { return _totalNumberOfPassengers; }
            set
            {
                if (_totalNumberOfPassengers != value)
                {
                    _totalNumberOfPassengers = value;
                    OnPropertyChanged("TotalNumberOfPassengers");
                }
            }
        }

        //[Field(SerializedName = "PDBA")]
        //[Field(SerializedName = "PDBS")]
        //[Field(SerializedName = "PDEA")]
        //[Field(SerializedName = "PDES")]
        //[Field(SerializedName = "PRFL")]

        [Entity(SerializedName = "PSTA", MaxLength = 5)]
        public virtual string PositionOfArrival
        {
            get { return _positionOfArrival; }
            set
            {
                if (_positionOfArrival != value)
                {
                    _positionOfArrival = value;
                    OnPropertyChanged("PositionOfArrival");
                }
            }
        }

        [Entity(SerializedName = "PSTD", MaxLength = 5)]
        public virtual string PositionOfDeparture
        {
            get { return _positionOfDeparture; }
            set
            {
                if (_positionOfDeparture != value)
                {
                    _positionOfDeparture = value;
                    OnPropertyChanged("PositionOfDeparture");
                }
            }
        }

        //[Field(SerializedName = "RACO")]

        [Entity(SerializedName = "REGN", MaxLength = 12)]
        public virtual string RegistrationNumber
        {
            get { return _registrationNumber; }
            set
            {
                if (_registrationNumber != value)
                {
                    _registrationNumber = value;
                    OnPropertyChanged("RegistrationNumber");
                }
            }
        }

        [Entity(SerializedName = "REM1", MaxLength = 256)]
        public virtual string Remark1
        {
            get { return _remark1; }
            set
            {
                if (_remark1 != value)
                {
                    _remark1 = value;
                    OnPropertyChanged("Remark1");
                }
            }
        }

        [Entity(SerializedName = "REM2", MaxLength = 256)]
        public virtual string Remark2
        {
            get { return _remark2; }
            set
            {
                if (_remark2 != value)
                {
                    _remark2 = value;
                    OnPropertyChanged("Remark2");
                }
            }
        }

        //[Field(SerializedName = "REMP")]        
        [Entity(SerializedName = "RKEY", MaxLength = 10)]
        public virtual int? RelationKey
        {
            get { return _relationKey; }
            set
            {
                if (_relationKey != value)
                {
                    _relationKey = value;
                    OnPropertyChanged("RelationKey");
                }
            }
        }

        [Entity(SerializedName = "RTYP", MaxLength = 1)]
        public virtual string RotationType
        {
            get { return _rotationType; }
            set
            {
                if (_rotationType != value)
                {
                    _rotationType = value;
                    OnPropertyChanged("RotationTypel");
                }
            }
        }

        [Entity(SerializedName = "RWYA", MaxLength = 4)]
        public virtual string RunwayOfArrival
        {
            get { return _runwayOfArrival; }
            set
            {
                if (_runwayOfArrival != value)
                {
                    _runwayOfArrival = value;
                    OnPropertyChanged("RunwayOfArrival");
                }
            }
        }

        [Entity(SerializedName = "RWYD", MaxLength = 4)]
        public virtual string RunwayOfDeparture
        {
            get { return _runwayOfDeparture; }
            set
            {
                if (_runwayOfDeparture != value)
                {
                    _runwayOfDeparture = value;
                    OnPropertyChanged("RunwayOfDeparture");
                }
            }
        }

        //[Field(SerializedName = "SEAS")]
        //[Field(SerializedName = "SFIF")]
        //[Field(SerializedName = "SKEY")]
        //[Field(SerializedName = "SLOT")]
        //[Field(SerializedName = "SLOU")]
        //[Field(SerializedName = "SSRC")]
        //[Field(SerializedName = "STAB")]

        [Entity(SerializedName = "STAT", MaxLength = 10)]
        public virtual string Status
        {
            get { return _status; }
            set
            {
                if (_status != value)
                {
                    _status = value;
                    OnPropertyChanged("Status");
                }
            }
        }

        //[Field(SerializedName = "STEA")]
        //[Field(SerializedName = "STED")]
        //[Field(SerializedName = "STEV")]
        //[Field(SerializedName = "STHT")]
        //[Field(SerializedName = "STLD")]

        [Entity(SerializedName = "STOA", SourceDataType = typeof(String))]
        public virtual DateTime? StandardTimeOfArrival
        {
            get { return _standardTimeOfArrival; }
            set
            {
                if (_standardTimeOfArrival != value)
                {
                    _standardTimeOfArrival = value;
                    OnPropertyChanged("StandardTimeOfArrival");
                }
            }
        }

        [Entity(SerializedName = "STOD", SourceDataType = typeof(String))]
        public virtual DateTime? StandardTimeOfDeparture
        {
            get { return _standardTimeOfDeparture; }
            set
            {
                if (_standardTimeOfDeparture != value)
                {
                    _standardTimeOfDeparture = value;
                    OnPropertyChanged("StandardTimeOfDeparture");
                }
            }
        }

        //[Field(SerializedName = "STOF")]
        //[Field(SerializedName = "STON")]
        //[Field(SerializedName = "STSL")]
        //[Field(SerializedName = "STTM")]
        //[Field(SerializedName = "STTT")]
        //[Field(SerializedName = "STYP")]
        //[Field(SerializedName = "TCCI")]
        //[Field(SerializedName = "TET1")]
        //[Field(SerializedName = "TET2")]

        [Entity(SerializedName = "TGA1", MaxLength = 1)]
        public virtual string TerminalGateOfArrival1
        {
            get { return _terminalGateOfArrival1; }
            set
            {
                if (_terminalGateOfArrival1 != value)
                {
                    _terminalGateOfArrival1 = value;
                    OnPropertyChanged("TerminalGateOfArrival1");
                }
            }
        }

        [Entity(SerializedName = "TGA2", MaxLength = 1)]
        public virtual string TerminalGateOfArrival2
        {
            get { return _terminalGateOfArrival2; }
            set
            {
                if (_terminalGateOfArrival2 != value)
                {
                    _terminalGateOfArrival2 = value;
                    OnPropertyChanged("TerminalGateOfArrival2");
                }
            }
        }

        [Entity(SerializedName = "TGD1", MaxLength = 1)]
        public virtual string TerminalGateOfDeparture1
        {
            get { return _terminalGateOfDeparture1; }
            set
            {
                if (_terminalGateOfDeparture1 != value)
                {
                    _terminalGateOfDeparture1 = value;
                    OnPropertyChanged("TerminalGateOfDeparture1");
                }
            }
        }

        [Entity(SerializedName = "TGD2", MaxLength = 1)]
        public virtual string TerminalGateOfDeparture2
        {
            get { return _terminalGateOfDeparture2; }
            set
            {
                if (_terminalGateOfDeparture2 != value)
                {
                    _terminalGateOfDeparture2 = value;
                    OnPropertyChanged("TerminalGateOfDeparture2");
                }
            }
        }

        [Entity(SerializedName = "TIFA", SourceDataType = typeof(String))]
        public virtual DateTime? TimeframeOfArrival
        {
            get { return _timeframeOfArrival; }
            set
            {
                if (_timeframeOfArrival != value)
                {
                    _timeframeOfArrival = value;
                    OnPropertyChanged("TimeframeOfArrival");
                }
            }
        }

        [Entity(SerializedName = "TIFD", SourceDataType = typeof(String))]
        public virtual DateTime? TimeframeOfDeparture
        {
            get { return _timeframeOfDeparture; }
            set
            {
                if (_timeframeOfDeparture != value)
                {
                    _timeframeOfDeparture = value;
                    OnPropertyChanged("TimeframeOfDeparture");
                }
            }
        }

        //[Field(SerializedName = "TISA")]
        //[Field(SerializedName = "TISD")]
        //[Field(SerializedName = "TMAA")]
        //[Field(SerializedName = "TMAU")]  

        [Entity(SerializedName = "TMB1", MaxLength = 1)]
        public virtual string TerminalBaggageBelt1
        {
            get { return _terminalBaggageBelt1; }
            set
            {
                if (_terminalBaggageBelt1 != value)
                {
                    _terminalBaggageBelt1 = value;
                    OnPropertyChanged("TerminalBaggageBelt1");
                }
            }
        }

        //[Field(SerializedName = "TMB2")]
        //[Field(SerializedName = "TMB3")]
        //[Field(SerializedName = "TMOA")]
        //[Field(SerializedName = "TRKN")]

        [Entity(SerializedName = "TTYP", MaxLength = 5)]
        public virtual string NatureCode
        {
            get { return _natureCode; }
            set
            {
                if (_natureCode != value)
                {
                    _natureCode = value;
                    OnPropertyChanged("NatureCode");
                }
            }
        }

        //[Field(SerializedName = "TWR1")]
        [Entity(SerializedName = "VERS", MaxLength = 20)]
        public virtual string VersionOfSchedule
        {
            get { return _versionOfSchedule; }
            set
            {
                if (_versionOfSchedule != value)
                {
                    _versionOfSchedule = value;
                    OnPropertyChanged("VersionOfSchedule");
                }
            }
        }

        [Entity(SerializedName = "VIA3", MaxLength = 3)]
        public virtual string ViaAirportIATACode
        {
            get { return _viaAirportIATACode; }
            set
            {
                if (_viaAirportIATACode != value)
                {
                    _viaAirportIATACode = value;
                    OnPropertyChanged("ViaAirportIATACode");
                }
            }
        }

        [Entity(SerializedName = "VIA4", MaxLength = 4)]
        public virtual string ViaAirportICAOCode
        {
            get { return _viaAirportICAOCode; }
            set
            {
                if (_viaAirportICAOCode != value)
                {
                    _viaAirportICAOCode = value;
                    OnPropertyChanged("ViaAirportICAOCode");
                }
            }
        }

        [Entity(SerializedName = "VIAL", MaxLength = 1024)]
        public virtual string ViaAirportList
        {
            get { return _viaAirportList; }
            set
            {
                if (_viaAirportList != value)
                {
                    _viaAirportList = value;
                    OnPropertyChanged("ViaAirportList");
                }
            }
        }

        [Entity(SerializedName = "VIAN", MaxLength = 2)]
        public virtual string ViaAirportCount
        {
            get { return _viaAirportCount; }
            set
            {
                if (_viaAirportCount != value)
                {
                    _viaAirportCount = value;
                    OnPropertyChanged("ViaAirportCount");
                }
            }
        }

        //[Field(SerializedName = "W1BA")]
        //[Field(SerializedName = "W1BS")]
        //[Field(SerializedName = "W1EA")]
        //[Field(SerializedName = "W1ES")]
        //[Field(SerializedName = "WR1F")]
        //[Field(SerializedName = "WRO1")]
        //[Field(SerializedName = "ATRN")]
        //[Field(SerializedName = "B1FP")]
        //[Field(SerializedName = "B2FP")]
        //[Field(SerializedName = "BAA1")]
        //[Field(SerializedName = "BAA2")]
        //[Field(SerializedName = "BAA4")]
        //[Field(SerializedName = "BAA5")]
        //[Field(SerializedName = "BAAA")]
        //[Field(SerializedName = "BABS")]
        //[Field(SerializedName = "BADU")]
        //[Field(SerializedName = "BAEA")]

        [Entity(SerializedName = "BAGS", MaxLength = 6)]
        public virtual int? TotalNumberOfBaggages
        {
            get { return _totalNumberOfBaggages; }
            set
            {
                if (_totalNumberOfBaggages != value)
                {
                    _totalNumberOfBaggages = value;
                    OnPropertyChanged("TotalNumberOfBaggages");
                }
            }
        }

        //[Field(SerializedName = "BALG")]
        //[Field(SerializedName = "BALS")]
        //[Field(SerializedName = "BAMA")]
        //[Field(SerializedName = "BAOI")]
        //[Field(SerializedName = "GA1F")]
        //[Field(SerializedName = "GD1F")]
        //[Field(SerializedName = "GD2F")]
        //[Field(SerializedName = "NADS")]
        //[Field(SerializedName = "PAFP")]
        //[Field(SerializedName = "PDFP")]
        //[Field(SerializedName = "W2BA")]
        //[Field(SerializedName = "W2BS")]
        //[Field(SerializedName = "W2EA")]
        //[Field(SerializedName = "W2ES")]
        //[Field(SerializedName = "WRO2")]
        //[Field(SerializedName = "B3BA")]
        //[Field(SerializedName = "B3BS")]
        //[Field(SerializedName = "B3EA")]
        //[Field(SerializedName = "B3ES")]
        //[Field(SerializedName = "BLT3")]
        //[Field(SerializedName = "PRMC")]
        //[Field(SerializedName = "PRMU")]
        //[Field(SerializedName = "PRMS")]
        //[Field(SerializedName = "FPSA")]
        //[Field(SerializedName = "FPSD")]
        //[Field(SerializedName = "VIPA")]
        //[Field(SerializedName = "VIPD")]
        //[Field(SerializedName = "PCOM")]
        //[Field(SerializedName = "AAAA")]

        [Entity(SerializedName = "TOBT", SourceDataType = typeof(String))]
        public virtual DateTime? TargetOffblockTime
        {
            get { return _targetOffblockTime; }
            set
            {
                if (_targetOffblockTime != value)
                {
                    _targetOffblockTime = value;
                    OnPropertyChanged("TargetOffblockTime");
                }
            }
        }

        [Entity(SerializedName = "RTOW", SourceDataType = typeof(String), MaxLength = 1, TrueValue = "R", FalseValue = " ")]
        public virtual bool ReadyForTowing
        {
            get { return _readyForTowing; }
            set
            {
                if (_readyForTowing != value)
                {
                    _readyForTowing = value;
                    OnPropertyChanged("ReadyForTowing");
                }
            }
        }
    }
}
