﻿#region ===== Source Code Information =====
/*
Project Name    : Ufis.Entities
Project Type    : Class Library
Platform        : Microsoft Visual C#.NET
File Name       : EntDbHoliday.cs

Version         : 1.0.0
Created Date    : 03 - Jan - 2012
Complete Date   : 03 - Jan - 2012
Created By      : Siva

Date Reason Updated By
---------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------

Copyright (C) All rights reserved.
*/
#endregion
using System;

namespace Ufis.Entities
{
    [Entity(SerializedName = "HOLTAB")]
    public class EntDbHoliday : ValidityBaseEntity
    {
        private string _holidayDate; 
        private string _flag;
        private string _remark;
        private string _holidaytype;



        [Entity(SerializedName = "HDAY",SourceDataType = typeof(String), IsMandatory = true)]
        public string HolidayDate
        {
            get { return _holidayDate; }
            set
            {
                if (_holidayDate != value)
                {
                    _holidayDate = value;
                    OnPropertyChanged("HolidayDate");
                }
            }
        } 

        [Entity(SerializedName = "PRFL", MaxLength = 1)]
        public string Flag
        {
            get { return _flag; }
            set
            {
                if (_flag != value)
                {
                    _flag = value;
                    OnPropertyChanged("Flag");
                }
            }
        }


        [Entity(SerializedName = "REMA", MaxLength = 40)]
        public string Remark
        {
            get { return _remark; }
            set
            {
                if (_remark != value)
                {
                    _remark = value;
                    OnPropertyChanged("Remark");
                }
            }
        }

        [Entity(SerializedName = "TYPE", MaxLength = 1,IsMandatory = true)]
        public string HolidayType
        {
            get { return _holidaytype; }
            set
            {
                if (_holidaytype != value)
                {
                    _holidaytype = value;
                    OnPropertyChanged("HolidayType");
                }
            }
        }

    }

}
