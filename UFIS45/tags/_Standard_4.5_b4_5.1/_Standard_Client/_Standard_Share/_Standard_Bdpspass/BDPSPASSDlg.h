// BDPSPASSDlg.h : header file
//

#if !defined(AFX_BDPSPASSDLG_H__30D43766_5080_11D1_A384_0080AD1DC9A3__INCLUDED_)
#define AFX_BDPSPASSDLG_H__30D43766_5080_11D1_A384_0080AD1DC9A3__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <CCSEdit.h>
#include <CedaBasicData.h>

/////////////////////////////////////////////////////////////////////////////
// CBDPSPASSDlg dialog

class CBDPSPASSDlg : public CDialog
{
// Construction
public:
	CBDPSPASSDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CBDPSPASSDlg)
	enum { IDD = IDD_BDPSPASS_DIALOG };
	CCSEdit	m_NewPass1;
	CCSEdit	m_NewPass2;
	CCSEdit	m_OldPass;
	CCSEdit	m_Usid;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBDPSPASSDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

	void SetText(int ipId, int ipStringId);

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CBDPSPASSDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnDestroy();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual void OnOK();
	afx_msg void OnAbout();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BDPSPASSDLG_H__30D43766_5080_11D1_A384_0080AD1DC9A3__INCLUDED_)
