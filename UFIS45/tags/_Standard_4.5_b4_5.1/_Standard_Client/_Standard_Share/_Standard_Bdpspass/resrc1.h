//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by BDPSPASS.rc
//
#define IDC_ABOUT                       3
#define IDS_WAITDLG_LOADING             109
#define IDS_USIDHEAD                    110
#define IDS_OLDPASSHEAD                 111
#define IDS_NEWPASS1HEAD                112
#define IDS_NEWPASS2HEAD                113
#define IDS_PASSWORDALREADYUSED         114
#define IDS_IDENTICALPASSWORDS          115
#define IDS_CONSECUTIVE                 116
#define IDS_NUMBERS                     117
#define IDS_SMALL_LETTER                118
#define IDS_CAPITAL_LETTER              119
#define IDD_WAIT_DIALOG                 143
#define IDR_FILECOPY                    158
#define IDC_TEXT                        1003
#define IDC_CLASSLIBVERSION             1009
#define IDC_ANIMATE1                    1262

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1010
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
