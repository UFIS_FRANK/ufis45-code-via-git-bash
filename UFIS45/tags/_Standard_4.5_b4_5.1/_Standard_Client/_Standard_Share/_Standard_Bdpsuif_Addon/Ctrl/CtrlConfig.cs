﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using System.Windows.Forms;

using Ufis.Utils;

namespace _Standard_Bdpsuif_Addon.Ctrl
{
    public class CtrlConfig
    {
        IniFile _myIni = new IniFile("C:\\Ufis\\system\\ceda.ini");
        public IniFile MyIni
        {
            get { return _myIni; }
        }


        #region Singleton
        private static CtrlConfig _this = null;
        private CtrlConfig()
        {

        }

        public static CtrlConfig GetInstance()
        {
            if (_this == null) _this = new CtrlConfig();
            return _this;
        }
        #endregion

        private static Dictionary<string, string> parameters = new Dictionary<string, string>();

        public static XmlNode GetConfigNode(string appType)
        {
            XmlNode config = null;

            string ufisSystemPath = System.IO.Path.GetDirectoryName(_this.MyIni.path);
            string xmlFileName = System.IO.Path.GetFileNameWithoutExtension(Application.ExecutablePath) + ".xml";
            string xmlFile = ufisSystemPath + System.IO.Path.DirectorySeparatorChar + xmlFileName;
            if (System.IO.File.Exists(xmlFile))
            {
                try
                {
                    XmlTextReader reader = new XmlTextReader(xmlFile);
                    XmlDocument doc = new XmlDocument();
                    doc.Load(reader);
                    reader.Close();

                    XmlElement root = doc.DocumentElement;
                    config = root.SelectSingleNode("/configurations/configuration[@configId='" + appType + "']");
                }
                catch
                {
                    config = null;
                }
            }
            else
            {
                MessageBox.Show(string.Format("Config file {0} not found!", xmlFile), "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return config;
        }

        public static CtrlBDValidity GetBDValidity(string appType, string urno, string userName)
        {
            CtrlBDValidity bdValidity = null;

            parameters.Clear();
            parameters.Add("urno", urno);


            XmlNode config = GetConfigNode(appType);
            if (config != null)
            {
                bdValidity = new CtrlBDValidity();
                foreach (XmlNode node in config.ChildNodes)
                {
                    switch (node.LocalName)
                    {
                        case "datasource":
                            ProcessDataSource(bdValidity, node);
                            break;
                        case "layout":
                            ProcessLayout(bdValidity, node);
                            break;
                    }
                }
            }

            return bdValidity;
        }

        public static CtrlBDIns GetBDIns(string appType)
        {
            CtrlBDIns bdIns = null;

            XmlNode config = GetConfigNode(appType);
            if (config != null)
            {
                bdIns = new CtrlBDIns();
                foreach (XmlNode node in config.ChildNodes)
                {
                    switch (node.LocalName)
                    {
                        case "separators":
                            ProcessSeparators(bdIns.Separators, node);
                            break;
                        case "tables":
                            ProcessTables(bdIns, node);
                            break;
                    }
                }
            }

            return bdIns;
        }

        private static void ProcessDataSource(CtrlBDValidity bdExtend, XmlNode xNode)
        {
            foreach (XmlNode node in xNode.ChildNodes)
            {
                switch (node.LocalName)
                {
                    case "tables":
                        ProcessTables(bdExtend, node);
                        break;
                    case "columnmaps":
                        ProcessColumnMaps(bdExtend, node);
                        break;
                }
            }
        }

        private static void ProcessTables(CtrlBDValidity bdExtend, XmlNode xNode)
        {
            foreach (XmlNode node in xNode.ChildNodes)
            {
                if (node.LocalName == "table")
                {
                    CtrlBDValidity.BDDataSource.Table table = new CtrlBDValidity.BDDataSource.Table();
                    ProcessTable(table, node);
                    bdExtend.DataSource.Tables.Add(table);
                }
            }
        }

        private static void ProcessTable(CtrlBDValidity.BDDataSource.Table table, XmlNode xNode)
        {
            foreach (XmlNode node in xNode.ChildNodes)
            {
                switch (node.LocalName)
                {
                    case "name":
                        table.TableName = GetNodeValue(node);
                        break;
                    case "alias":
                        table.TableAlias = GetNodeValue(node);
                        break;
                    case "filter":
                        table.Filter = EvalParams(GetNodeValue(node), parameters);
                        break;
                    case "sort":
                        table.Sort = GetNodeValue(node);
                        break;
                    case "type":
                        string[] types = GetNodeValue(node).Split(',');
                        foreach (string type in types)
                        {
                            table.Type = table.Type | CtrlBDValidity.BDDataSource.Table.GetTableType(type);
                        }
                        break;
                    case "columns":
                        ProcessColumns(table, node);
                        break;
                }
            }
        }

        private static void ProcessColumns(CtrlBDValidity.BDDataSource.Table table, XmlNode xNode)
        {
            foreach (XmlNode node in xNode.ChildNodes)
            {
                if (node.LocalName == "column")
                {
                    CtrlBDValidity.BDDataSource.Table.Column column = new CtrlBDValidity.BDDataSource.Table.Column();
                    ProcessColumn(column, node);
                    table.Columns.Add(column);
                }
            }
        }

        private static void ProcessColumn(CtrlBDValidity.BDDataSource.Table.Column column, XmlNode xNode)
        {
            foreach (XmlNode node in xNode.ChildNodes)
            {
                switch (node.LocalName)
                {
                    case "name":
                        column.Name = GetNodeValue(node);
                        break;
                    case "type":
                        column.DataType = CtrlBDValidity.BDDataSource.Table.Column.GetColumnDataType(GetNodeValue(node));
                        break;
                    case "length":
                        int length;
                        if (int.TryParse(GetNodeValue(node), out length))
                            column.Length = length;
                        break;
                    case "required":
                        bool required;
                        if (bool.TryParse(GetNodeValue(node), out required))
                            column.Required = required;
                        break;
                    case "unique":
                        bool unique;
                        if (bool.TryParse(GetNodeValue(node), out unique))
                            column.Unique = unique;
                        break;
                    case "default":
                        XmlAttribute xAttr = node.Attributes["type"];
                        if (xAttr != null)
                        {
                            CtrlBDValidity.BDDataSource.Table.Column.ColumnDefaultValueTypeEnum defaultValueType =
                                CtrlBDValidity.BDDataSource.Table.Column.GetColumnDefaultValueType(xAttr.Value);
                            column.DefaultValueType = defaultValueType;
                        }

                        if (column.DefaultValueType == CtrlBDValidity.BDDataSource.Table.Column.ColumnDefaultValueTypeEnum.Text)
                        {
                            column.DefaultValue = EvalParams(GetNodeValue(node), parameters);
                        }
                        else
                        {
                            column.DefaultValue = GetNodeValue(node);
                        }

                        break;
                }
            }
        }

        private static void ProcessColumnMaps(CtrlBDValidity bdExtend, XmlNode xNode)
        {
            foreach (XmlNode node in xNode.ChildNodes)
            {
                if (node.LocalName == "columnmap")
                {
                    CtrlBDValidity.BDDataSource.ColumnMap columnMap = new CtrlBDValidity.BDDataSource.ColumnMap();
                    ProcessColumnMap(columnMap, node);
                    bdExtend.DataSource.ColumnMaps.Add(columnMap);
                }
            }
        }

        private static void ProcessColumnMap(CtrlBDValidity.BDDataSource.ColumnMap columnMap, XmlNode xNode)
        {
            foreach (XmlNode node in xNode.ChildNodes)
            {
                switch (node.LocalName)
                {
                    case "schedulercolumn":
                        columnMap.SchedulerColumnName = GetNodeValue(node);
                        break;
                    case "mapvalue":
                        columnMap.Value = GetNodeValue(node);

                        XmlAttribute xAtrr = node.Attributes["type"];
                        if (xAtrr != null)
                        {
                            CtrlBDValidity.BDDataSource.ColumnMap.ColumnMapValueTypeEnum valueType
                                = CtrlBDValidity.BDDataSource.ColumnMap.GetColumnMapValueType(xAtrr.Value);
                            columnMap.ValueType = valueType;
                        }
                        xAtrr = node.Attributes["separator"];
                        if (xAtrr != null)
                        {
                            string separator = xAtrr.Value;
                            columnMap.Separator = separator[0];
                        }
                        break;
                    case "iskeycolumn":
                        bool isKeyColumn;
                        if (bool.TryParse(GetNodeValue(node), out isKeyColumn))
                            columnMap.IsKeyColumn = isKeyColumn;
                        break;
                }
            }
        }

        private static void ProcessLayout(CtrlBDValidity bdExtend, XmlNode xNode)
        {
            foreach (XmlNode node in xNode.ChildNodes)
            {
                switch (node.LocalName)
                {
                    case "title":
                        bdExtend.Layout.Title = GetNodeValue(node);
                        break;
                    case "header":
                        ProcessSection(bdExtend.Layout.Header, node);
                        break;
                    case "body":
                        ProcessBodySection(bdExtend.Layout.Body, node);
                        break;
                    case "footer":
                        ProcessSection(bdExtend.Layout.Footer, node);
                        break;
                }
            }
        }

        private static void ProcessSection(CtrlBDValidity.BDLayout.Section section, XmlNode xNode)
        {
            foreach (XmlNode node in xNode.ChildNodes)
            {
                if (node.LocalName == "items")
                {
                    ProcessSectionItems(section, node);
                }
            }
        }

        private static void ProcessSectionItems(CtrlBDValidity.BDLayout.Section section, XmlNode xNode)
        {
            foreach (XmlNode node in xNode.ChildNodes)
            {
                if (node.LocalName == "item")
                {
                    CtrlBDValidity.BDLayout.Section.SectionItem item = new CtrlBDValidity.BDLayout.Section.SectionItem();
                    item.Value = GetNodeValue(node);

                    XmlAttribute xAtrr = node.Attributes["type"];
                    if (xAtrr != null)
                    {
                        CtrlBDValidity.BDLayout.Section.SectionItem.SectionItemValueTypeEnum valueType
                            = CtrlBDValidity.BDLayout.Section.SectionItem.GetSectionItemValueType(xAtrr.Value);
                        item.ValueType = valueType;
                    }
                    xAtrr = node.Attributes["datasource"];
                    if (xAtrr != null)
                    {
                        item.DataSource = xAtrr.Value;
                    }
                    xAtrr = node.Attributes["borderstyle"];
                    if (xAtrr != null)
                    {
                        BorderStyle borderStyle = CtrlBDValidity.BDLayout.Section.SectionItem.GetSectionItemBorderStyle(xAtrr.Value);
                        item.BorderStyle = borderStyle;
                    }
                    xAtrr = node.Attributes["fontbold"];
                    if (xAtrr != null)
                    {
                        bool fontBold;
                        if (bool.TryParse(xAtrr.Value, out fontBold)) item.FontBold = fontBold;
                    }
                    xAtrr = node.Attributes["fontitalic"];
                    if (xAtrr != null)
                    {
                        bool fontItalic;
                        if (bool.TryParse(xAtrr.Value, out fontItalic)) item.FontItalic = fontItalic;
                    }
                    xAtrr = node.Attributes["fontunderline"];
                    if (xAtrr != null)
                    {
                        bool fontUnderline;
                        if (bool.TryParse(xAtrr.Value, out fontUnderline)) item.FontUnderline = fontUnderline;
                    }
                    xAtrr = node.Attributes["fontsize"];
                    if (xAtrr != null)
                    {
                        float fontSize;
                        if (float.TryParse(xAtrr.Value, out fontSize)) item.FontSize = fontSize;
                    }
                    xAtrr = node.Attributes["fontname"];
                    if (xAtrr != null)
                    {
                        item.FontName = xAtrr.Value;
                    }
                    section.Items.Add(item);
                }
            }
        }

        private static void ProcessBodySection(CtrlBDValidity.BDLayout.BodySection section, XmlNode xNode)
        {
            foreach (XmlNode node in xNode.ChildNodes)
            {
                switch (node.LocalName)
                {
                    case "showdeletebutton":
                        bool showdeletebutton;
                        if (bool.TryParse(GetNodeValue(node), out showdeletebutton))
                            section.ShowDeleteButton = showdeletebutton;
                        break;
                    case "columns":
                        ProcessViewColumns(section, node);
                        break;
                    case "paircolumns":
                        ProcessPairViewColumns(section, node);
                        break;
                }
            }
        }

        private static void ProcessViewColumns(CtrlBDValidity.BDLayout.BodySection section, XmlNode xNode)
        {
            foreach (XmlNode node in xNode.ChildNodes)
            {
                if (node.LocalName == "column")
                {
                    CtrlBDValidity.BDLayout.BodySection.ViewColumn column = new CtrlBDValidity.BDLayout.BodySection.ViewColumn();
                    ProcessViewColumn(column, node);
                    section.Columns.Add(column);
                }
            }
        }

        private static void ProcessViewColumn(CtrlBDValidity.BDLayout.BodySection.ViewColumn column, XmlNode xNode)
        {
            foreach (XmlNode node in xNode.ChildNodes)
            {
                switch (node.LocalName)
                {
                    case "boundfield":
                        column.BoundField = GetNodeValue(node);
                        break;
                    case "caption":
                        column.Caption = GetNodeValue(node);
                        break;
                    case "contentalignment":
                        DataGridViewContentAlignment contentAlignment =
                            CtrlBDValidity.BDLayout.BodySection.ViewColumn.GetViewColumnAlignment(GetNodeValue(node));
                        column.ContentAlignment = contentAlignment;
                        break;
                    case "headeralignment":
                        DataGridViewContentAlignment headerAlignment =
                            CtrlBDValidity.BDLayout.BodySection.ViewColumn.GetViewColumnAlignment(GetNodeValue(node));
                        column.HeaderAlignment = headerAlignment;
                        break;
                    case "displayformat":
                        column.DisplayFormat = GetNodeValue(node);
                        break;
                    case "inputformats":
                        column.InputFormats = GetNodeValue(node).Split(';');
                        break;
                    case "width":
                        int width;
                        if (int.TryParse(GetNodeValue(node), out width))
                            column.Width = width;
                        break;
                    case "readonly":
                        bool isReadOnly;
                        if (bool.TryParse(GetNodeValue(node), out isReadOnly))
                            column.ReadOnly = isReadOnly;
                        break;
                    case "visible":
                        bool isVisible;
                        if (bool.TryParse(GetNodeValue(node), out isVisible))
                            column.Visible = isVisible;
                        break;
                    case "valuechangedrule":
                        CtrlBDValidity.BDLayout.BodySection.ViewColumn.ViewColumnValueChangedRule
                            viewColumnValueChangedRule = new CtrlBDValidity.BDLayout.BodySection.ViewColumn.ViewColumnValueChangedRule();
                        viewColumnValueChangedRule.Expression = GetNodeValue(node);
                        XmlAttribute xColAttr = node.Attributes["refcolumn"];
                        if (xColAttr != null)
                        {
                            viewColumnValueChangedRule.ColumnName = xColAttr.Value;
                        }
                        column.ValueChangedRule = viewColumnValueChangedRule;
                        break;
                    case "editablerule":
                        column.EditableRule = GetNodeValue(node);
                        break;
                    case "type":
                        CtrlBDValidity.BDLayout.BodySection.ViewColumn.ViewColumnTypeEnum columnType =
                            CtrlBDValidity.BDLayout.BodySection.ViewColumn.GetViewColumnType(GetNodeValue(node));
                        column.ColumnType = columnType;

                        XmlAttribute xAtrr = node.Attributes["sourcetype"];
                        if (xAtrr != null)
                        {
                            CtrlBDValidity.BDLayout.BodySection.ViewColumn.ViewColumnSourceTypeEnum sourceType =
                                CtrlBDValidity.BDLayout.BodySection.ViewColumn.GetViewColumnSourceType(xAtrr.Value);
                            column.SourceType = sourceType;
                        }
                        xAtrr = node.Attributes["datasource"];
                        if (xAtrr != null)
                        {
                            column.DataSource = xAtrr.Value;
                        }
                        xAtrr = node.Attributes["displaymember"];
                        if (xAtrr != null)
                        {
                            column.DisplayMember = xAtrr.Value;
                        }
                        xAtrr = node.Attributes["valuemember"];
                        if (xAtrr != null)
                        {
                            column.ValueMember = xAtrr.Value;
                        }
                        break;
                }
            }
        }

        private static void ProcessPairViewColumns(CtrlBDValidity.BDLayout.BodySection section, XmlNode xNode)
        {
            foreach (XmlNode node in xNode.ChildNodes)
            {
                if (node.LocalName == "paircolumn")
                {
                    CtrlBDValidity.BDLayout.BodySection.PairViewColumn pairColumn = new CtrlBDValidity.BDLayout.BodySection.PairViewColumn();
                    ProcessPairViewColumn(pairColumn, node);
                    section.PairColumns.Add(pairColumn);
                }
            }
        }

        private static void ProcessPairViewColumn(CtrlBDValidity.BDLayout.BodySection.PairViewColumn pairColumn, XmlNode xNode)
        {
            foreach (XmlNode node in xNode.ChildNodes)
            {
                switch (node.LocalName)
                {
                    case "columns":
                        CtrlBDValidity.BDLayout.BodySection.PairViewColumn.ColumnCollection columns =
                            new CtrlBDValidity.BDLayout.BodySection.PairViewColumn.ColumnCollection();
                        ProcessPairViewColumnCollection(columns, node);
                        pairColumn.Columns = columns;
                        break;
                    case "datasource":
                        pairColumn.DataSource = GetNodeValue(node);
                        break;
                }
            }
        }

        private static void ProcessPairViewColumnCollection(CtrlBDValidity.BDLayout.BodySection.PairViewColumn.ColumnCollection columns, XmlNode xNode)
        {
            foreach (XmlNode node in xNode.ChildNodes)
            {
                if (node.LocalName == "column")
                {
                    CtrlBDValidity.BDLayout.BodySection.PairViewColumn.Column column =
                        new CtrlBDValidity.BDLayout.BodySection.PairViewColumn.Column();
                    ProcessPairViewColumnItem(column, node);
                    columns.Add(column);
                }
            }
        }

        private static void ProcessPairViewColumnItem(CtrlBDValidity.BDLayout.BodySection.PairViewColumn.Column column, XmlNode xNode)
        {
            foreach (XmlNode node in xNode.ChildNodes)
            {
                switch (node.LocalName)
                {
                    case "viewcolumnname":
                        column.ViewColumnName = GetNodeValue(node);
                        break;
                    case "fieldname":
                        column.FieldName = GetNodeValue(node);
                        break;
                }
            }
        }

        private static string GetNodeValue(XmlNode xNode)
        {
            string result = null;

            if (xNode.NodeType == XmlNodeType.Element && xNode.HasChildNodes)
            {
                XmlNode childNode = xNode.ChildNodes[0];
                if (childNode.NodeType == XmlNodeType.Text)
                    result = childNode.Value;
            }

            return result;
        }

        private static string EvalParams(string text, Dictionary<string, string> parameters)
        {
            string result = text;

            foreach (string key in parameters.Keys)
            {
                string value;
                if (parameters.TryGetValue(key, out value))
                    result = result.Replace("{:" + key + "}", value);
            }

            return result;
        }

        private static void ProcessSeparators(CtrlBDIns.SeparatorCollection bdInsSeparators, XmlNode xNode)
        {
            foreach (XmlNode node in xNode.ChildNodes)
            {
                if (node.LocalName == "separator")
                {
                    char sep;

                    string value = GetNodeValue(node);
                    if (string.IsNullOrEmpty(value))
                    {
                        sep = ',';
                    }
                    else
                    {
                        int intValue;
                        if (int.TryParse(value, out intValue))
                        {
                            sep = (char)intValue;
                        }
                        else
                        {
                            sep = value[0];
                        }
                    }

                    bdInsSeparators.Add(sep);
                }
            }
        }

        private static void ProcessTables(CtrlBDIns bdIns, XmlNode xNode)
        {
            foreach (XmlNode node in xNode.ChildNodes)
            {
                if (node.LocalName == "table")
                {
                    CtrlBDIns.Table table = new CtrlBDIns.Table();
                    ProcessTable(table, node);
                    bdIns.Tables.Add(table);
                }
            }
        }

        private static void ProcessTable(CtrlBDIns.Table table, XmlNode xNode)
        {
            foreach (XmlNode node in xNode.ChildNodes)
            {
                switch (node.LocalName)
                {
                    case "name":
                        table.TableName = GetNodeValue(node);
                        break;
                    case "type":
                        table.Type = CtrlBDIns.Table.GetTableType(GetNodeValue(node));
                        break;
                    case "columns":
                        ProcessColumns(table, node);
                        break;
                }
            }
        }

        private static void ProcessColumns(CtrlBDIns.Table table, XmlNode xNode)
        {
            foreach (XmlNode node in xNode.ChildNodes)
            {
                if (node.LocalName == "column")
                {
                    CtrlBDIns.Table.Column column = new CtrlBDIns.Table.Column();
                    ProcessColumn(column, node);
                    table.Columns.Add(column);
                }
            }
        }

        private static void ProcessColumn(CtrlBDIns.Table.Column column, XmlNode xNode)
        {
            foreach (XmlNode node in xNode.ChildNodes)
            {
                switch (node.LocalName)
                {
                    case "name":
                        column.Name = GetNodeValue(node);
                        break;
                    case "type":
                        column.DataType = CtrlBDIns.Table.Column.GetColumnDataType(GetNodeValue(node));
                        break;
                    case "refcolumn":
                        column.ReferenceColumnName = GetNodeValue(node);
                        break;
                    case "updatemastertable":
                        bool updateMasterTable;
                        if (bool.TryParse(GetNodeValue(node), out updateMasterTable))
                            column.UpdateMasterTable = updateMasterTable;
                        break;
                    case "expression":
                        column.Expression = GetNodeValue(node);
                        break;
                }
            }
        }
    }
}