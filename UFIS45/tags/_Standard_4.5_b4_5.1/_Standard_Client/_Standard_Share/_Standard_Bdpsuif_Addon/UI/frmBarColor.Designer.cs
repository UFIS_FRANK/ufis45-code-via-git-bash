﻿namespace _Standard_Bdpsuif_Addon.UI
{
    partial class frmBarColor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBarColor));
            this.panelOkButton = new System.Windows.Forms.FlowLayoutPanel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.panelSaveButton = new System.Windows.Forms.FlowLayoutPanel();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnLoad = new System.Windows.Forms.Button();
            this.lblBarColor = new System.Windows.Forms.Label();
            this.txtBarColor = new System.Windows.Forms.TextBox();
            this.lblTextColor = new System.Windows.Forms.Label();
            this.txtTextColor = new System.Windows.Forms.TextBox();
            this.btnDefault = new System.Windows.Forms.Button();
            this.dgvData = new _Standard_Bdpsuif_Addon.UserControl.MyDataGridView();
            this.panelOkButton.SuspendLayout();
            this.panelSaveButton.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
            this.SuspendLayout();
            // 
            // panelOkButton
            // 
            this.panelOkButton.Controls.Add(this.btnCancel);
            this.panelOkButton.Controls.Add(this.btnOK);
            this.panelOkButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelOkButton.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.panelOkButton.Location = new System.Drawing.Point(0, 367);
            this.panelOkButton.Name = "panelOkButton";
            this.panelOkButton.Size = new System.Drawing.Size(658, 33);
            this.panelOkButton.TabIndex = 4;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(580, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(499, 3);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "&OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // panelSaveButton
            // 
            this.panelSaveButton.Controls.Add(this.btnSave);
            this.panelSaveButton.Controls.Add(this.btnLoad);
            this.panelSaveButton.Controls.Add(this.lblBarColor);
            this.panelSaveButton.Controls.Add(this.txtBarColor);
            this.panelSaveButton.Controls.Add(this.lblTextColor);
            this.panelSaveButton.Controls.Add(this.txtTextColor);
            this.panelSaveButton.Controls.Add(this.btnDefault);
            this.panelSaveButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSaveButton.Location = new System.Drawing.Point(0, 0);
            this.panelSaveButton.Name = "panelSaveButton";
            this.panelSaveButton.Size = new System.Drawing.Size(658, 33);
            this.panelSaveButton.TabIndex = 5;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(3, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(97, 23);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "&Save as Default";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnLoad
            // 
            this.btnLoad.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnLoad.Location = new System.Drawing.Point(106, 3);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(110, 23);
            this.btnLoad.TabIndex = 1;
            this.btnLoad.Text = "&Load as Default";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // lblBarColor
            // 
            this.lblBarColor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblBarColor.AutoSize = true;
            this.lblBarColor.Location = new System.Drawing.Point(222, 8);
            this.lblBarColor.Name = "lblBarColor";
            this.lblBarColor.Size = new System.Drawing.Size(59, 13);
            this.lblBarColor.TabIndex = 4;
            this.lblBarColor.Text = "Back Color";
            // 
            // txtBarColor
            // 
            this.txtBarColor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtBarColor.BackColor = System.Drawing.Color.Turquoise;
            this.txtBarColor.Location = new System.Drawing.Point(287, 4);
            this.txtBarColor.Name = "txtBarColor";
            this.txtBarColor.ReadOnly = true;
            this.txtBarColor.Size = new System.Drawing.Size(20, 20);
            this.txtBarColor.TabIndex = 2;
            this.txtBarColor.Click += new System.EventHandler(this.txtBarColor_Click);
            // 
            // lblTextColor
            // 
            this.lblTextColor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblTextColor.AutoSize = true;
            this.lblTextColor.Location = new System.Drawing.Point(313, 8);
            this.lblTextColor.Name = "lblTextColor";
            this.lblTextColor.Size = new System.Drawing.Size(55, 13);
            this.lblTextColor.TabIndex = 5;
            this.lblTextColor.Text = "Text Color";
            // 
            // txtTextColor
            // 
            this.txtTextColor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtTextColor.BackColor = System.Drawing.Color.Black;
            this.txtTextColor.Location = new System.Drawing.Point(374, 4);
            this.txtTextColor.Name = "txtTextColor";
            this.txtTextColor.ReadOnly = true;
            this.txtTextColor.Size = new System.Drawing.Size(23, 20);
            this.txtTextColor.TabIndex = 3;
            this.txtTextColor.Click += new System.EventHandler(this.txtTextColor_Click);
            // 
            // btnDefault
            // 
            this.btnDefault.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnDefault.Location = new System.Drawing.Point(403, 3);
            this.btnDefault.Name = "btnDefault";
            this.btnDefault.Size = new System.Drawing.Size(84, 23);
            this.btnDefault.TabIndex = 6;
            this.btnDefault.Text = "&Apply to All";
            this.btnDefault.UseVisualStyleBackColor = true;
            this.btnDefault.Click += new System.EventHandler(this.btnDefault_Click);
            // 
            // dgvData
            // 
            this.dgvData.AllowUserToAddRows = false;
            this.dgvData.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvData.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvData.Location = new System.Drawing.Point(0, 33);
            this.dgvData.Name = "dgvData";
            this.dgvData.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvData.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvData.RowHeadersVisible = false;
            this.dgvData.RowHeadersWidth = 20;
            this.dgvData.Size = new System.Drawing.Size(658, 334);
            this.dgvData.TabIndex = 6;
            this.dgvData.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvData_CellBeginEdit);
            this.dgvData.Sorted += new System.EventHandler(this.dgvData_Sorted);
            // 
            // frmBarColor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(658, 400);
            this.Controls.Add(this.dgvData);
            this.Controls.Add(this.panelSaveButton);
            this.Controls.Add(this.panelOkButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmBarColor";
            this.Text = "Bar Color by Nature Code";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmBarColor_FormClosing);
            this.Load += new System.EventHandler(this.frmBarColor_Load);
            this.panelOkButton.ResumeLayout(false);
            this.panelSaveButton.ResumeLayout(false);
            this.panelSaveButton.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel panelOkButton;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.FlowLayoutPanel panelSaveButton;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.Label lblBarColor;
        private System.Windows.Forms.TextBox txtBarColor;
        private System.Windows.Forms.TextBox txtTextColor;
        private System.Windows.Forms.Label lblTextColor;
        private UserControl.MyDataGridView dgvData;
        private System.Windows.Forms.Button btnDefault;
    }
}