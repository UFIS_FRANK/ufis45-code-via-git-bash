﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace _Standard_Bdpsuif_Addon.UserControl
{
    public partial class ImageComboBox : System.Windows.Forms.UserControl
    {
        private int _imageWidth;
        public int ImageWidth
        {
            get
            {
                if (_imageWidth == 0)
                {
                    _imageWidth = this.imageList1.ImageSize.Width;
                }
                return _imageWidth; 
            }
            set
            {
                _imageWidth = value;
            }
        }

        private int _imageHeight;
        public int ImageHeight
        {
            get
            {
                if (_imageHeight == 0)
                {
                    _imageHeight = this.imageList1.ImageSize.Height;
                }
                return _imageHeight; 
            }
            set
            {
                _imageHeight = value;
            }
        }

        public int SelectedIndex
        {
            get
            {
                return comboBox1.SelectedIndex;
            }
            set
            {
                if (value >= 0 && value < comboBox1.Items.Count)
                    comboBox1.SelectedIndex = value;
            }
        }


        public ImageComboBox(Dictionary<string, string> imgKeyPath,Size imgSize)
        {
            InitializeComponent();
            this.imageList1.ImageSize = imgSize;
            ImageWidth = imgSize.Width;
            ImageHeight = imgSize.Height;

            Image img;
            foreach (KeyValuePair<string, string> keyPath in imgKeyPath)
            {
                img = Image.FromFile(keyPath.Value);
                img = CreateImageBorder(img);
                
                imageList1.Images.Add(keyPath.Key, img);
            }
        }

        private Image CreateImageBorder(Image orgImg)
        {
            orgImg = CreateTileImage(3, orgImg);

            Bitmap newImage = new Bitmap(orgImg.Width + 2, orgImg.Height + 2);
            using(Graphics graphics = Graphics.FromImage(newImage))
            {
                graphics.Clear(Color.Black);
                int x = (newImage.Width - orgImg.Width) / 2;
                int y = (newImage.Height - orgImg.Height) / 2;
                graphics.DrawImage(orgImg, x, y);
            }
            newImage.SetResolution(200.0F, 200.0F);
            
            return (Image)newImage;
        }

        private Image CreateTileImage(int imgCount,Image tile)
        {
            //Bitmap tile = new Bitmap(@"D:\Ufis\System\notvalid1.bmp");
            Bitmap resultImg = new Bitmap(tile.Width * imgCount, tile.Height);
            using (Graphics g = Graphics.FromImage(resultImg))
            {
                for (int i = 0; i < imgCount; i++)
                {
                    g.DrawImage(tile, new Point(i * tile.Width, 0));
                }
            }
            //resultImg.Save(@"D:\Ufis\System\notvalidTest.bmp");

            PictureBox pb = new PictureBox();
            pb.Image = (Image)resultImg;
            this.Controls.Add(pb);
            return resultImg;
        }

   

        private void Control_Load(object sender, EventArgs e)
        {
            
            string[] items = new string[this.imageList1.Images.Count];
            for (Int32 i = 0; i <= this.imageList1.Images.Count - 1; i++)
            {
                items[i] = "Item " + i.ToString();
            }
            this.comboBox1.Items.AddRange(items);
            this.comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
            this.comboBox1.DrawMode = DrawMode.OwnerDrawVariable;
            this.comboBox1.ItemHeight = ImageHeight;
            this.comboBox1.Width = ImageWidth + 24;
            this.comboBox1.MaxDropDownItems = 5; //consistent with BDPS-UIF //this.imageList1.Images.Count;
            this.comboBox1.SelectedIndex = 0;
            this.comboBox1.Height = ImageHeight;

            this.Height = this.comboBox1.Height + 10;
        }

        private void ComboBox1_DrawItem(object sender, System.Windows.Forms.DrawItemEventArgs e)
        {
            if (e.Index != -1)
            {
                //this.imageList1.Images[e.Index].SelectActiveFrame(System.Drawing.Imaging.FrameDimension.Resolution, 10);

                e.Graphics.DrawImage(this.imageList1.Images[e.Index], e.Bounds.Left, e.Bounds.Top);

                
            }

        }


        private void ComboBox1_MeasureItem(object sender, System.Windows.Forms.MeasureItemEventArgs e)
        {

            e.ItemHeight = ImageHeight;// this.imageList1.ImageSize.Height;
            e.ItemWidth = ImageWidth;// this.imageList1.ImageSize.Width;

        }
    }
}
