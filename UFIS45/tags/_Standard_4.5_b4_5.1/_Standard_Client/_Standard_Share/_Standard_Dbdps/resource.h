//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by DBDPS.rc
//
#define ID_BUTTON_SAVE                  3
#define ID_BUTTON_DELETE                4
#define ID_BUTTON_APPLY                 5
#define ID_BUTTON_RIGHT                 6
#define ID_BUTTON_UP                    7
#define ID_BUTTON_DOWN                  8
#define ID_BUTTON_RIGHT_ALL             9
#define ID_BUTTON_LEFT_ALL              10
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_STARTETYPE                  129
#define IDD_DIALOG_SELECT               131
#define IDD_DIALOG_FILTER_SORT          132
#define IDD_DIALOG_REGISTER             133
#define IDD_DIALOG_TOOLBAR              137
#define IDD_DIALOG_LOGIN                138
#define IDB_UFIS                        140
#define IDC_LIST_CHOICE                 1001
#define IDC_EDIT_USER_NAME              1001
#define IDC_STATIC_SEC                  1001
#define IDC_EDIT_FILTER                 1002
#define IDC_EDIT_PASSWORD               1002
#define ID_REGISTRY                     1002
#define IDC_EDIT_CONDITION              1003
#define IDC_PASSWORD                    1003
#define ID_START                        1003
#define IDC_COMBO_OPERATOR              1004
#define IDC_USER_NAME                   1004
#define ID_BUTTON_LEFT                  1005
#define IDC_TAB                         1006
#define ID_MY_OK                        1006
#define IDC_COMBO_VIEW                  1007
#define IDC_VERSION                     1007
#define IDC_TEXT_FILTER                 1008
#define IDC_TEXT_DATA_FIELD             1009
#define ID_RADIOBUTTON_AND              1010
#define ID_RADIOBUTTON_OR               1011
#define IDC_DATA_FIELD                  1012
#define IDC_TEXT_OPERATOR               1013
#define IDC_TEXT_CONDITION              1014
#define IDC_TEXT_AND_OR                 1015
#define ID_BUTTON_ASC_DESC              1016
#define ID_BUTTON_NEW                   1017
#define ID_BUTTON_SETTING               1018
#define ID_BUTTON_LEFT_PARENTHESIS      1019
#define ID_BUTTON_RIGHT_PARENTHESIS     1020
#define IDC_LIST_SORT                   1021
#define IDC_TEXT_CHOICE                 1022
#define IDC_TEXT_SORT                   1025
#define ID_VIEW_MIN                     30000
#define ID_VIEW_MAX                     30100
#define ID_VIEW_MORE                    30101
#define IDC_LIST_TABELLEN               30102
#define IDC_COMBO_FILTER                30103
#define ID_EXPORT                       32783
#define ID_REFRESH                      32784
#define ID_SORT                         32785
#define ID_FILTER_SORT                  32786
#define ID_DELETE_ROW                   32787
#define ID_EDIT_PASTEROW                32789
#define ID_EDIT_PASTEREGION             32790
#define ID_EDIT_UNDO_PASTEREGION        32794
#define ID_SEARCH_DLG                   32797
#define ID_SAVE_SETTINGS                32800
#define ID_DELETE_SETTINGS              32801
#define IDS_STRING_DBDPS_0001           61204

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        142
#define _APS_NEXT_COMMAND_VALUE         32802
#define _APS_NEXT_CONTROL_VALUE         1006
#define _APS_NEXT_SYMED_VALUE           109
#endif
#endif
