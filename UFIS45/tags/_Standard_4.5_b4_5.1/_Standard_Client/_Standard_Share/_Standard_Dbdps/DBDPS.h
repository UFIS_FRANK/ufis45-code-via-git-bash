// DBDPS.h : main header file for the DBDPS application
//

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include <resource.h>       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CDBDPSApp:
// See DBDPS.cpp for the implementation of this class
//

class CDBDPSApp : public CWinApp
{
public:
	CDBDPSApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDBDPSApp)
	public:
	virtual BOOL InitInstance();
	virtual BOOL ExitInstance();
	//}}AFX_VIRTUAL
	int ExtractItemList(CString opSubString, CStringArray *popStrArray, char cpTrenner)	;
// Implementation
public:
	bool RefreshMenu(CDocument*);

	//{{AFX_MSG(CDBDPSApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////
