// FilterSortDlg.cpp : implementation file
//

#include <stdafx.h>
#include <DBDPS.h>
#include <FilterSortDlg.h>
#include <Globals.h>

#ifdef ABBCCS_RELEASE
#include <UfisCEDA.h>
#else 
#include <UfisSWH.h>
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

int CountOfParanthesis;	//count of left paranthesis that are not bounded with a rigth peranthesis
CButton* myAnd;
CButton* myOr;
BOOL ViewIsValid;	// flag of a valid filter and sort clause of the selected view

/////////////////////////////////////////////////////////////////////////////
// CFilterSortDlg dialog

CFilterSortDlg::CFilterSortDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFilterSortDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CFilterSortDlg)
	//}}AFX_DATA_INIT
}

void CFilterSortDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFilterSortDlg)
	DDX_Control(pDX, IDC_TEXT_SORT, m_TextSort);
	DDX_Control(pDX, IDC_TEXT_CHOICE, m_TextChoice);
	DDX_Control(pDX, IDC_LIST_SORT, m_ListSort);
	DDX_Control(pDX, IDC_LIST_CHOICE, m_ListChoice);
	DDX_Control(pDX, IDC_COMBO_VIEW, m_ComboView);
	DDX_Control(pDX, ID_BUTTON_UP, m_ButtonUp);
	DDX_Control(pDX, ID_BUTTON_RIGHT_ALL, m_ButtonRightAll);
	DDX_Control(pDX, ID_BUTTON_RIGHT, m_ButtonRight);
	DDX_Control(pDX, ID_BUTTON_LEFT_ALL, m_ButtonLeftAll);
	DDX_Control(pDX, ID_BUTTON_LEFT, m_ButtonLeft);
	DDX_Control(pDX, ID_BUTTON_DOWN, m_ButtonDown);
	DDX_Control(pDX, ID_BUTTON_ASC_DESC, m_ButtonAscDesc);
	DDX_Control(pDX, IDC_DATA_FIELD, m_ComboDataField);
	DDX_Control(pDX, IDC_EDIT_FILTER, m_EditFilter);
	DDX_Control(pDX, IDC_EDIT_CONDITION, m_EditCondition);
	DDX_Control(pDX, ID_BUTTON_LEFT_PARENTHESIS, m_ButtonLeftParanthesis);
	DDX_Control(pDX, ID_BUTTON_NEW, m_ButtonNew);
	DDX_Control(pDX, ID_BUTTON_RIGHT_PARENTHESIS, m_ButtonRightParanthesis);
	DDX_Control(pDX, ID_BUTTON_SETTING, m_ButtonSetting);
	DDX_Control(pDX, IDC_COMBO_OPERATOR, m_ComboOperator);
	DDX_Control(pDX, IDC_TEXT_FILTER, m_TextFilter);
	DDX_Control(pDX, IDC_TEXT_OPERATOR, m_TextOperator);
	DDX_Control(pDX, IDC_TEXT_DATA_FIELD, m_TextDataField);
	DDX_Control(pDX, IDC_TEXT_CONDITION, m_TextCondition);
	DDX_Control(pDX, IDC_TEXT_AND_OR, m_TextAndOr);
	DDX_Control(pDX, IDC_TAB, m_Tab);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CFilterSortDlg, CDialog)
	//{{AFX_MSG_MAP(CFilterSortDlg)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB, OnSelchangeTab)
	ON_BN_CLICKED(ID_BUTTON_ASC_DESC, OnButtonAscDesc)
	ON_BN_CLICKED(ID_BUTTON_DOWN, OnButtonDown)
	ON_BN_CLICKED(ID_BUTTON_LEFT, OnButtonLeft)
	ON_BN_CLICKED(ID_BUTTON_LEFT_ALL, OnButtonLeftAll)
	ON_BN_CLICKED(ID_BUTTON_RIGHT, OnButtonRight)
	ON_BN_CLICKED(ID_BUTTON_RIGHT_ALL, OnButtonRightAll)
	ON_BN_CLICKED(ID_BUTTON_UP, OnButtonUp)
	ON_BN_CLICKED(ID_BUTTON_NEW, OnButtonNew)
	ON_BN_CLICKED(ID_BUTTON_LEFT_PARENTHESIS, OnButtonLeftParenthesis)
	ON_BN_CLICKED(ID_BUTTON_RIGHT_PARENTHESIS, OnButtonRightParenthesis)
	ON_BN_CLICKED(ID_BUTTON_SETTING, OnButtonSetting)
	ON_BN_CLICKED(ID_BUTTON_APPLY, OnButtonApply)
	ON_BN_CLICKED(ID_BUTTON_SAVE, OnButtonSave)
	ON_BN_CLICKED(ID_BUTTON_DELETE, OnButtonDelete)
	ON_CBN_SELCHANGE(IDC_COMBO_VIEW, OnSelchangeComboView)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFilterSortDlg message handlers and funcions

//-----------------------------------------------------------------------
// FillCombo
//	
//	The function filld the view combo
//-----------------------------------------------------------------------
BOOL CFilterSortDlg::FillCombo()
{
	CString UrnoText; 
	CString ViewName; 
	CString TableName;
	CString FilterText;
	CString SortText;

	m_ComboView.ResetContent();	// remove all items from the list box and edit
	for(int i = 0; i < mySavedViews.GetSize(); i++)
	{
		mySavedViews.GetAt(i, UrnoText, ViewName, TableName, FilterText, SortText);
		if (m_ComboView.AddString(ViewName) < 0)	// add the view name to combo
			return FALSE;
	}

	return TRUE;
}

//-----------------------------------------------------------------------
// InitTab
//	
//	The function initialized the tab-control
//-----------------------------------------------------------------------
void CFilterSortDlg::InitTab()
{
	int myTabIndex;
	TC_ITEM NewTabCtrlItem;

	NewTabCtrlItem.mask = TCIF_TEXT;
	NewTabCtrlItem.pszText = _T("Filter");	// set the name of the new control item
	myTabIndex = m_Tab.InsertItem(0, &NewTabCtrlItem);	// add new tab control item
	if(myTabIndex == -1)
		AfxMessageBox("InitTab(). InsertItem fault.", MB_OK + MB_ICONSTOP);

	NewTabCtrlItem.pszText = _T("Sort");	// set the name of the new control item
	myTabIndex = m_Tab.InsertItem(1, &NewTabCtrlItem);	// add new tab control item
	if(myTabIndex == -1)
		AfxMessageBox("InitTab(). InsertItem fault.", MB_OK + MB_ICONSTOP);
}

//-----------------------------------------------------------------------
// InitData
//	
//	The function initialized the dialog data
//-----------------------------------------------------------------------
// CHANGES:
//-----------------------------------------------------------------------
//	DATE       | AUTHOR            | DESCRIPTION
//-----------------------------------------------------------------------
// 14.04.1999 | Martin Danihel    | Added 'default' view functionality
//-----------------------------------------------------------------------
BOOL CFilterSortDlg::InitData()
{
	LoadSavedViews(m_TableName, &mySavedViews, TRUE);	// load saved views with 'default' view

	if(!FillCombo())	// fill the combo with saved views
		return FALSE;

	CFieldsDesc *myFieldDescriptor;
	CString myString;

	// look for the description of the table
	gTablesDesc.Lookup(m_TableName, myFieldDescriptor);
	for(int i = 0; i < myFieldDescriptor->GetSize(); i++)
	{
		myString = myFieldDescriptor->GetAt(i)->mFieldName + _T("...") + myFieldDescriptor->GetAt(i)->mLongFieldName;
		m_ComboDataField.AddString(myString);	// fill the combo DataField
		if(myFieldDescriptor->GetAt(i)->mSortable)	// if the table field is sortable
		  if(myFieldDescriptor->GetAt(i)->mVisible)
			m_ListChoice.AddString(myString);	// fill the list Choice
	}

	return TRUE;
}

//-----------------------------------------------------------------------
// SetDefaultValues
//	
//	The function sets the init settings of the dialog objects
//-----------------------------------------------------------------------
void CFilterSortDlg::SetDefaultValues()
{
	CountOfParanthesis = 0;	// initially is the count of not bounded left paran. 0
	m_EditFilter.SetWindowText((LPCTSTR) _T(""));
	m_EditCondition.SetWindowText((LPCTSTR) _T(""));
	m_ComboDataField.SetCurSel(-1);	// set no selection
	m_ComboOperator.SetCurSel(-1);	// set no selection

	// set radio button And as default
	CWnd* myCheck;
	myCheck = GetDlgItem(ID_RADIOBUTTON_AND);
	myAnd = (CButton*) myCheck;
	myAnd->SetCheck(TRUE);
	myCheck = GetDlgItem(ID_RADIOBUTTON_OR);
	myOr = (CButton*) myCheck;
	myOr->SetCheck(FALSE);
}

//-----------------------------------------------------------------------
// SetFilterSortClause
//	
//	The function sets the init settings of the filter and sort clause
//-----------------------------------------------------------------------
void CFilterSortDlg::SetFilterSortClause()
{
	// set default filter clause
	if(!m_FilterClause.IsEmpty())
		m_EditFilter.SetWindowText(m_FilterClause);

	// set default sort clause
	if(!m_SortClause.IsEmpty())
		SetSortConditionFromText(m_SortClause);
}

//-----------------------------------------------------------------------
// EnableFilter
//	
//	The function enables or disables changing of the filter clause in dialog
//-----------------------------------------------------------------------
void CFilterSortDlg::EnableFilter(BOOL pEnable)
{
	m_ButtonNew.EnableWindow(pEnable);
	m_ButtonSetting.EnableWindow(pEnable);
	m_ButtonLeftParanthesis.EnableWindow(pEnable);
	m_ButtonRightParanthesis.EnableWindow(pEnable);
}

//-----------------------------------------------------------------------
// EnableSort
//	
//	The function enables or disables changing of the sort clause in dialog
//-----------------------------------------------------------------------
void CFilterSortDlg::EnableSort(BOOL pEnable)
{
	m_ButtonRightAll.EnableWindow(pEnable);
	m_ButtonRight.EnableWindow(pEnable);
	m_ButtonLeft.EnableWindow(pEnable);
	m_ButtonLeftAll.EnableWindow(pEnable);
	m_ButtonUp.EnableWindow(pEnable);
	m_ButtonAscDesc.EnableWindow(pEnable);
	m_ButtonDown.EnableWindow(pEnable);
}

//-----------------------------------------------------------------------
// SetSecurity
//	
//	The function sets the dialog concerning the security state
//-----------------------------------------------------------------------
void CFilterSortDlg::SetSecurity()
{
	CString myStatus;

	myStatus = GetSecurityStatus(_T("VIEW_SORT_DLG"), _T("SPECIFY_FILTER"));
	if(myStatus == SEC_STAT_ACT)
	{
//		if(gTableLoad)
//		{
			myStatus = GetSecurityStatus(m_TableName + gTableExt, _T("SPECIFY_FILTER"));
			if(myStatus == SEC_STAT_ACT)
				EnableFilter(TRUE);
			else
				EnableFilter(FALSE);
//		}
//		else
//			EnableFilter(TRUE);
	}
	else
		EnableFilter(FALSE);

	myStatus = GetSecurityStatus(_T("VIEW_SORT_DLG"), _T("SPECIFY_SORT"));
	if(myStatus == SEC_STAT_ACT)
	{
//		if(gTableLoad)
//		{
			myStatus = GetSecurityStatus(m_TableName + gTableExt, _T("SPECIFY_SORT"));
			if(myStatus == SEC_STAT_ACT)
				EnableSort(TRUE);
			else
				EnableSort(FALSE);
//		}
//		else
//			EnableSort(TRUE);
	}
	else
		EnableSort(FALSE);
}

//-----------------------------------------------------------------------
// Prepare
//	
//	The function sets the dialog into init state
//-----------------------------------------------------------------------
// CHANGES:
//-----------------------------------------------------------------------
//	DATE       | AUTHOR            | DESCRIPTION
//-----------------------------------------------------------------------
// 02.05.1999 | Martin Danihel    | Saved views adjustment
//-----------------------------------------------------------------------
BOOL CFilterSortDlg::Prepare()
{
	InitTab();	// initialize the tab-control
	if(!InitData())	// initialize dialog data
		return FALSE;
	SetDefaultValues();	// sets the init settings of the dialog objects
	SetSecurity();

	if(!m_ViewUrno.IsEmpty() && m_ShowView == TRUE) // if some view is selected in the application toolbar and should be displayed
	{
		CString UrnoText;
		CString ViewName;
		CString TableName;
		CString FilterText;
		CString SortText;
		for(int i = 0; i < mySavedViews.GetSize(); i++)
		{
			// search for the URNO of the view
			mySavedViews.GetAt(i, UrnoText, ViewName, TableName, FilterText, SortText);
			if(UrnoText == m_ViewUrno)
			{
				CString tmpViewName;
				for(int myPosition = 0; myPosition < m_ComboView.GetCount( ) ; myPosition++)
				{
					m_ComboView.GetLBText(myPosition, tmpViewName);
					if(tmpViewName == ViewName)
					{
						m_ComboView.SetCurSel(myPosition);
						OnSelchangeComboView();
						return TRUE;
					}
				}
			}
		}
	}
	else
		SetFilterSortClause();	// else sets the init state of the filter and sort clause

	ViewIsValid = FALSE;	//	no new selected view (this initialization must be set on this place of code)

	return TRUE;
}

//-----------------------------------------------------------------------
// OnInitDialog
//	
//	The function is called during initializing of the dialog
//-----------------------------------------------------------------------
BOOL CFilterSortDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	Prepare();	// initialization of the dialog

	return TRUE;
}

//-----------------------------------------------------------------------
// DestroyWindow
//	
//	The function is called during destroying of the dialog
//-----------------------------------------------------------------------
BOOL CFilterSortDlg::DestroyWindow() 
{
	mySavedViews.RemoveAll();	// free used objects

	return CDialog::DestroyWindow();
}

//-----------------------------------------------------------------------
// OnSelchangeTab
//	
//	The function changed the tab-control pages
//-----------------------------------------------------------------------
void CFilterSortDlg::OnSelchangeTab(NMHDR* pNMHDR, LRESULT* pResult) 
{
	int myPosition;

	// retrieve the currently selected tab in a tab control
	myPosition = m_Tab.GetCurSel();
	if(myPosition == 0)
	{
		ShowTabSort(SW_HIDE);	// hide the tab-control page Sort
		ShowTabFilter(SW_SHOW);	// show the tab-control page Filter
	}

	else
	{
		ShowTabFilter(SW_HIDE);	// hide the tab-control page Filter
		ShowTabSort(SW_SHOW);	// show the tab-control page Sort
	}

	*pResult = 0;
}

//-----------------------------------------------------------------------
// ShowTabFilter
//	
//	The function shows or hides the controls in a tab-control page Filter
//-----------------------------------------------------------------------
void CFilterSortDlg::ShowTabFilter(int pCmdShow)
{
	m_TextFilter.ShowWindow(pCmdShow);
	m_EditFilter.ShowWindow(pCmdShow);
	m_ButtonNew.ShowWindow(pCmdShow);
	m_ButtonSetting.ShowWindow(pCmdShow);
	m_ButtonLeftParanthesis.ShowWindow(pCmdShow);
	m_ButtonRightParanthesis.ShowWindow(pCmdShow);
	m_TextAndOr.ShowWindow(pCmdShow);
	myAnd->ShowWindow(pCmdShow);
	myOr->ShowWindow(pCmdShow);
	m_TextDataField.ShowWindow(pCmdShow);
	m_ComboDataField.ShowWindow(pCmdShow);
	m_TextOperator.ShowWindow(pCmdShow);
	m_ComboOperator.ShowWindow(pCmdShow);
	m_TextCondition.ShowWindow(pCmdShow);
	m_EditCondition.ShowWindow(pCmdShow);
}

//-----------------------------------------------------------------------
// ShowTabSort
//	
//	The function shows or hides the controls in a tab-control page Sort
//-----------------------------------------------------------------------
void CFilterSortDlg::ShowTabSort(int pCmdShow)
{
	m_TextChoice.ShowWindow(pCmdShow);
	m_ListChoice.ShowWindow(pCmdShow);
	m_TextSort.ShowWindow(pCmdShow);
	m_ListSort.ShowWindow(pCmdShow);
	m_ButtonRightAll.ShowWindow(pCmdShow);
	m_ButtonRight.ShowWindow(pCmdShow);
	m_ButtonLeft.ShowWindow(pCmdShow);
	m_ButtonLeftAll.ShowWindow(pCmdShow);
	m_ButtonUp.ShowWindow(pCmdShow);
	m_ButtonAscDesc.ShowWindow(pCmdShow);
	m_ButtonDown.ShowWindow(pCmdShow);
}

//-----------------------------------------------------------------------
// OnButtonAscDesc
//	
//	The function changed ascendig -> descending or descending -> ascending
// in the sort list
//-----------------------------------------------------------------------
// CHANGES:
//-----------------------------------------------------------------------
//	DATE       | AUTHOR            | DESCRIPTION
//-----------------------------------------------------------------------
// 03.05.1999 | Martin Danihel    | Saved views adjustment
//-----------------------------------------------------------------------
void CFilterSortDlg::OnButtonAscDesc()
{
	int mySelection;
	CString myText;
	CString mySubstr;
	char sign;

	mySelection = m_ListSort.GetCurSel();	// get actualy selected list item
	if(mySelection != LB_ERR)
	{
		m_ListSort.GetText(mySelection, (CString&) myText);	// get the text of the item
		mySubstr = myText.Right(myText.GetLength() - 2);		// without sign and blank
		sign = myText[0];	// get the sign
    	if(m_ListSort.DeleteString(mySelection) == LB_ERR)	// delete the list item
			AfxMessageBox("OnButtonAscDesc(). DeleteString => LB_ERR.", MB_OK + MB_ICONSTOP);

		switch(sign)
		{
			case '+':
				myText = "- " + mySubstr;	// '-' instead of '+'
				break;
			case '-':
				myText = "+ " + mySubstr;	// '+' instead of '-'
				break;
			default:
				AfxMessageBox("OnButtonAscDesc(). Switch sign.", MB_OK + MB_ICONSTOP);
		}
		m_ListSort.InsertString(mySelection, myText);	// insert changed list item
		m_ListSort.SetCurSel(mySelection);	// set selection to the changed list item
		ViewIsValid = FALSE;	// if some view is selected, now is this invalid
	}
}

//-----------------------------------------------------------------------
// OnButtonUp
//	
//	The function changed position of the selected list-item upwards in
// sort list
//-----------------------------------------------------------------------
// CHANGES:
//-----------------------------------------------------------------------
//	DATE       | AUTHOR            | DESCRIPTION
//-----------------------------------------------------------------------
// 03.05.1999 | Martin Danihel    | Saved views adjustment
//-----------------------------------------------------------------------
void CFilterSortDlg::OnButtonUp() 
{
	int mySelection;

	mySelection = m_ListSort.GetCurSel();
	if(mySelection != LB_ERR)
		if(mySelection > 0)
		{
			Exchange(mySelection, mySelection - 1);	// exchange two list items
			m_ListSort.SetCurSel(mySelection - 1);
			ViewIsValid = FALSE;	// if some view is selected, now is this invalid
		}
}

//-----------------------------------------------------------------------
// OnButtonDown
//	
//	The function changed position of the selected list-item downward in
// sort list
//-----------------------------------------------------------------------
// CHANGES:
//-----------------------------------------------------------------------
//	DATE       | AUTHOR            | DESCRIPTION
//-----------------------------------------------------------------------
// 03.05.1999 | Martin Danihel    | Saved views adjustment
//-----------------------------------------------------------------------
void CFilterSortDlg::OnButtonDown() 
{
	int mySelection;

	mySelection = m_ListSort.GetCurSel();
	if(mySelection != LB_ERR)
		if(mySelection < m_ListSort.GetCount() - 1)	// test if the item is not bottommost
		{
			Exchange(mySelection, mySelection + 1);	// exchange two list items
			m_ListSort.SetCurSel(mySelection + 1);
			ViewIsValid = FALSE;	// if some view is selected, now is this invalid
		}
}

//-----------------------------------------------------------------------
// OnButtonLeft
//	
//	The function moved the selected list-item from sort list to choice
// list
//-----------------------------------------------------------------------
// CHANGES:
//-----------------------------------------------------------------------
//	DATE       | AUTHOR            | DESCRIPTION
//-----------------------------------------------------------------------
// 03.05.1999 | Martin Danihel    | Saved views adjustment
//-----------------------------------------------------------------------
void CFilterSortDlg::OnButtonLeft()
{
	int mySelection;
	CString myText;

	mySelection = m_ListSort.GetCurSel();
	if(mySelection != LB_ERR)
	{
		m_ListSort.GetText(mySelection, (CString&) myText);	// get the text of the item
		myText = myText.Right(myText.GetLength() - 2);			// without sign and blank
		if(m_ListSort.DeleteString(mySelection) == LB_ERR)	// delete the item in sort list
			AfxMessageBox("OnButtonLeft(). DeleteString => LB_ERR.", MB_OK + MB_ICONSTOP);
		m_ListChoice.AddString(myText);	// add the item into choice list

		if(mySelection < m_ListSort.GetCount())	// update the selection in sort list
			m_ListSort.SetCurSel(mySelection);
		else if(mySelection != 0)
			m_ListSort.SetCurSel(mySelection - 1);
		ViewIsValid = FALSE;	// if some view is selected, now is this invalid
	}
}

//-----------------------------------------------------------------------
// OnButtonLeftAll
//	
//	The function moved all list-items from sort list to choice list
//-----------------------------------------------------------------------
// CHANGES:
//-----------------------------------------------------------------------
//	DATE       | AUTHOR            | DESCRIPTION
//-----------------------------------------------------------------------
// 03.05.1999 | Martin Danihel    | Saved views adjustment
//-----------------------------------------------------------------------
void CFilterSortDlg::OnButtonLeftAll() 
{
	int myCount;
	CString myText;

	myCount = m_ListSort.GetCount();
	if(myCount != LB_ERR)
	{
		for(int i = 0; i < myCount; i++)
		{
			m_ListSort.GetText(0, (CString&) myText);			// get the text of the item
			myText = myText.Right(myText.GetLength() - 2);	// without sign and blank
			if(m_ListSort.DeleteString(0) == LB_ERR)	// delete the item in sort list
				AfxMessageBox("OnButtonLeftAll(). DeleteString => LB_ERR.", MB_OK + MB_ICONSTOP);
			m_ListChoice.AddString(myText);	// add the item into choice list
		}
		ViewIsValid = FALSE;	// if some view is selected, now is this invalid
	}
}

//-----------------------------------------------------------------------
// OnButtonRight
//	
//	The function calls the function MoveRight with parameter set to TRUE
// (sign will be '+')
//-----------------------------------------------------------------------
void CFilterSortDlg::OnButtonRight()
{
	MoveRight(TRUE);
}

//-----------------------------------------------------------------------
// MoveRight
//	
//	The function moved the selected list-item from choice list to sort
// list
//-----------------------------------------------------------------------
// CHANGES:
//-----------------------------------------------------------------------
//	DATE       | AUTHOR            | DESCRIPTION
//-----------------------------------------------------------------------
// 03.05.1999 | Martin Danihel    | Saved views adjustment
//-----------------------------------------------------------------------
void CFilterSortDlg::MoveRight(BOOL pSign)
{
	int mySelection;
	CString myText;

	mySelection = m_ListChoice.GetCurSel();
	if(mySelection != LB_ERR)
	{
		m_ListChoice.GetText(mySelection, (CString&) myText);	// get the text of the item
		if(pSign)	// if the sign must be '+'
			myText = "+ " + myText;	// set sign to '+'
		else
			myText = "- " + myText;	// set sign to '-'
		if(m_ListChoice.DeleteString(mySelection) == LB_ERR)	// delete the item in choice list
			AfxMessageBox("MoveRight(). DeleteString => LB_ERR.", MB_OK + MB_ICONSTOP);
		m_ListSort.AddString(myText);	// add the item into choice list

		if(mySelection < m_ListChoice.GetCount())	// update the selection in choice list
			m_ListChoice.SetCurSel(mySelection);
		else if(mySelection != 0)
			m_ListChoice.SetCurSel(mySelection - 1);
		ViewIsValid = FALSE;	// if some view is selected, now is this invalid
	}
}

//-----------------------------------------------------------------------
// OnButtonRightAll
//	
//	The function moved all list-items from choice list to sort list
//-----------------------------------------------------------------------
// CHANGES:
//-----------------------------------------------------------------------
//	DATE       | AUTHOR            | DESCRIPTION
//-----------------------------------------------------------------------
// 03.05.1999 | Martin Danihel    | Saved views adjustment
//-----------------------------------------------------------------------
void CFilterSortDlg::OnButtonRightAll() 
{
	int myCount;
	CString myText;

	myCount = m_ListChoice.GetCount();
	if(myCount != LB_ERR)
	{
		for(int i = 0; i < myCount; i++)
		{
			m_ListChoice.GetText(0, (CString&) myText);	// get the text of the item
			myText = "+ " + myText;	// add the sign '+'
			if(m_ListChoice.DeleteString(0) == LB_ERR)	// delete the item in choice list
				AfxMessageBox("OnButtonRightAll(). DeleteString => LB_ERR.", MB_OK + MB_ICONSTOP);
			m_ListSort.AddString(myText);	// add the item into sort list
		}
		ViewIsValid = FALSE;	// if some view is selected, now is this invalid
	}
}

//-----------------------------------------------------------------------
// Exchange(ind1, ind2)
//	
//	The function changed the positions of the sort list items in locations
// ind1 and ind2
//-----------------------------------------------------------------------
void CFilterSortDlg::Exchange(int ind1,int ind2)
{
	CString myText1;
	CString myText2;

	if(ind1 > ind2)	// because of correct deleting and inserting order
	{
		int tmp = ind1;
		ind1 = ind2;
		ind2 = tmp;
	}
	m_ListSort.GetText(ind1, (CString&) myText1);
	m_ListSort.GetText(ind2, (CString&) myText2);

	if(m_ListSort.DeleteString(ind2) == LB_ERR || 
		m_ListSort.DeleteString(ind1) == LB_ERR)
		AfxMessageBox("Exchange(). DeleteString => LB_ERR.", MB_OK + MB_ICONSTOP);

	m_ListSort.InsertString(ind1, (LPCTSTR) myText2);
	m_ListSort.InsertString(ind2, (LPCTSTR) myText1);
}

//-----------------------------------------------------------------------
// OnButtonNew
//	
//	The function clears the content of the filter edit control
//-----------------------------------------------------------------------
// CHANGES:
//-----------------------------------------------------------------------
//	DATE       | AUTHOR            | DESCRIPTION
//-----------------------------------------------------------------------
// 03.05.1999 | Martin Danihel    | Saved views adjustment
//-----------------------------------------------------------------------
void CFilterSortDlg::OnButtonNew() 
{
	m_EditFilter.SetWindowText((LPCTSTR) _T(""));
	ViewIsValid = FALSE;	// if some view is selected, now is this invalid
	SetDefaultValues();
}

//-----------------------------------------------------------------------
// OnButtonLeftParenthesis
//	
//	The function adds a left paranthesis to the content of the filter edit
// control
//-----------------------------------------------------------------------
// CHANGES:
//-----------------------------------------------------------------------
//	DATE       | AUTHOR            | DESCRIPTION
//-----------------------------------------------------------------------
// 03.05.1999 | Martin Danihel    | Saved views adjustment
//-----------------------------------------------------------------------
void CFilterSortDlg::OnButtonLeftParenthesis() 
{
	CString myEditText;

	m_EditFilter.GetWindowText(myEditText);
	// check if 'AND' or 'OR' text must be added also
	if(myEditText.GetLength() > 0 && myEditText.Right(1) != LBR_STRING)
		if(myAnd->GetCheck())
			myEditText += AND_STRING;
		else
			myEditText += OR_STRING;
	myEditText += LBR_STRING;	// add left paranthesis

	if(myEditText.GetLength() > MAX_LEN_FILTER)	// if the text length is not greater as the limit length
		AfxMessageBox("OnButtonLeftParenthesis(). String too long.", MB_OK + MB_ICONSTOP);
	else
	{
		m_EditFilter.SetWindowText((LPCTSTR) myEditText);
		ViewIsValid = FALSE;	// if some view is selected, now is this invalid
		CountOfParanthesis++;
	}	
}

//-----------------------------------------------------------------------
// OnButtonRightParenthesis
//	
//	The function adds a right paranthesis to the content of the filter edit
// control
//-----------------------------------------------------------------------
// CHANGES:
//-----------------------------------------------------------------------
//	DATE       | AUTHOR            | DESCRIPTION
//-----------------------------------------------------------------------
// 03.05.1999 | Martin Danihel    | Saved views adjustment
//-----------------------------------------------------------------------
void CFilterSortDlg::OnButtonRightParenthesis() 
{
	if(CountOfParanthesis > 0)	// if the parantesis can be bound with a some left paranthesis
	{
		CString myEditText;
		m_EditFilter.GetWindowText(myEditText);
		if(myEditText.Right(1) != LBR_STRING)	// if the last character is not left paranthesis
		{
			myEditText += RBR_STRING;	// add right paranthesis
			// if the text length is not greater as the limit length
			if(myEditText.GetLength() > MAX_LEN_FILTER)
				AfxMessageBox("OnButtonRightParenthesis(). String too long.", MB_OK + MB_ICONSTOP);
			else
			{
				m_EditFilter.SetWindowText((LPCTSTR) myEditText);
				ViewIsValid = FALSE;	// if some view is selected, now is this invalid
				CountOfParanthesis--;	// one left paranthesis bound with the right paranthesis
			}
		}
	}	
}

//-----------------------------------------------------------------------
// OnButtonSetting
//	
//	The function adds the selected condition to the filter text
//-----------------------------------------------------------------------
// CHANGES:
//-----------------------------------------------------------------------
//	DATE       | AUTHOR            | DESCRIPTION
//-----------------------------------------------------------------------
// 03.05.1999 | Martin Danihel    | Saved views adjustment
//-----------------------------------------------------------------------
void CFilterSortDlg::OnButtonSetting()
{
	CString myEditText;
	CString ComboDataField;
	CString ComboOperator;
	CString myEditCondition;
	int myPosition;

	m_EditFilter.GetWindowText(myEditText);	// get the text from EditFilter

	myPosition = m_ComboDataField.GetCurSel();
	if(myPosition != CB_ERR)
	{
		m_ComboDataField.GetLBText(myPosition, ComboDataField);
		// get the text of a table field from combo DataField
		ComboDataField = ComboDataField.Left(LEN_FIELD_NAME);
	}
	else
		return;

	myPosition = m_ComboOperator.GetCurSel();
	if(myPosition != CB_ERR)	// get the text of condition operator from combo Operator
		m_ComboOperator.GetLBText(myPosition, ComboOperator);
	else
		return;

	// get the text of condition from edit Condition
	m_EditCondition.GetWindowText(myEditCondition);
	if(myEditCondition.IsEmpty())
		return;
	else
	{
		CString myDBString;
		// converts the input string to DB value
		if(!ConvertInToDB(ComboDataField, myEditCondition, myDBString))
		{
			AfxMessageBox("OnButtonSetting(). Not a valid condition text.", MB_OK + MB_ICONSTOP);
			m_EditCondition.SetFocus();
			return;
		}
		else
			myEditCondition = myDBString;
	}

	// get the operand if this will be need
	if(myEditText.GetLength() > 0 && myEditText.Right(1) != LBR_STRING)
		if(myAnd->GetCheck())
			myEditText += AND_STRING;
		else
			myEditText += OR_STRING;

//h - start
	if (ComboOperator == _T("LIKE")) 
		myEditText += ComboDataField + BLANK_STRING + ComboOperator + _T(" '") + 
			myEditCondition + WILD_ALL_CHAR + _T("'");	// create the additional condition text
	else
//h - end
		myEditText += ComboDataField + BLANK_STRING + ComboOperator + _T(" '") + 
			myEditCondition + _T("'");	// create the additional condition text

	if(myEditText.GetLength() > MAX_LEN_FILTER)	// if the text length is not greater as the limit
		AfxMessageBox("OnButtonSetting(). String too long.", MB_OK + MB_ICONSTOP);
	else
	{
		m_EditFilter.SetWindowText((LPCTSTR) myEditText);
		ViewIsValid = FALSE;	// if some view is selected, now is this invalid
	}
}

//-----------------------------------------------------------------------
// ConvertInToDB
//	
//	The function converts the input format to DB format
//-----------------------------------------------------------------------
BOOL CFilterSortDlg::ConvertInToDB
	(
		CString &pNameOfTheColumn,
		const CString &pInputValue,
		CString &pDBValue
	)
{
	CFieldsDesc *myFieldDescriptor;

	// look for the description of the table
	gTablesDesc.Lookup(m_TableName, myFieldDescriptor);
	// look for the description of the table field
	for(int i = 0; i < myFieldDescriptor->GetSize(); i++)
	{
		if(myFieldDescriptor->GetAt(i)->mFieldName == pNameOfTheColumn)	// desc. found
		{
			// make necessary conversions depending on the logical type of the table field
			switch(myFieldDescriptor->GetAt(i)->mLGType)
			{
				case LG_DATE:
					if(!DateInToDB
							(
								pInputValue,
								pDBValue
							))
						return FALSE;
					break;
				case LG_TIME:
					if(!TimeInToDB
							(
								pInputValue,
								pDBValue,
								myFieldDescriptor->GetAt(i)->mTimeZoneDif
							))
						return FALSE;
					break;
				case LG_LSTU:
				case LG_CDAT:
				case LG_DTIM:
					if(!DateTimeInToDB
							(
								pInputValue,
								pDBValue,
								myFieldDescriptor->GetAt(i)->mTimeZoneDif
							))
						return FALSE;
					break;
				case LG_REAL:
				case LG_CURR:
					if(!RealInToDB
							(
								pInputValue,
								pDBValue
							))
						return FALSE;
					break;

				default:
					pDBValue = pInputValue;
			}
			// convert the string to server format
			ConvertStringToServer(pDBValue);
			break;
		}
	}

	return TRUE;
}

//-----------------------------------------------------------------------
// DateInToDB
//	
//	The function converts the input date format to database format
//-----------------------------------------------------------------------
BOOL CFilterSortDlg::DateInToDB
	(
		const CString &pInValue,
		CString &pDBValue
	)
{
	COleDateTime dt;
	if(!dt.ParseDateTime(pInValue, VAR_DATEVALUEONLY))
		return FALSE;
	
	pDBValue.Format(_T("%04u%02u%02u000000"), dt.GetYear(), dt.GetMonth(), dt.GetDay());
	return TRUE;
}

//-----------------------------------------------------------------------
// TimeInToDB
//	
//	The function converts the input time format to database format
//-----------------------------------------------------------------------
BOOL CFilterSortDlg::TimeInToDB
	(
		const CString &pInValue,
		CString &pDBValue,
		int pTimeZoneDif
	)
{
   COleDateTime dt;
	if(!dt.ParseDateTime(pInValue, VAR_TIMEVALUEONLY))
		return FALSE;
	dt = dt + COleDateTimeSpan(0, 0, pTimeZoneDif, 0);

	pDBValue.Format(_T("00000000%02d%02d00"), dt.GetHour(), dt.GetMinute());
	return TRUE;
}

//-----------------------------------------------------------------------
// DateTimeInToDB
//	
//	The function converts the input data-time format to database format
//-----------------------------------------------------------------------
BOOL CFilterSortDlg::DateTimeInToDB
	(
		const CString &pInValue, 
		CString &pDBValue,
		int pTimeZoneDif
	)
{
   COleDateTime dt;
	if(!dt.ParseDateTime(pInValue))
		return FALSE;
	dt = dt + COleDateTimeSpan(0, 0, pTimeZoneDif, 0);

	pDBValue.Format(_T("%04d%02d%02d%02d%02d00"), 
						dt.GetYear(), dt.GetMonth(), dt.GetDay(),
						dt.GetHour(), dt.GetMinute());
	return true;
}

//-----------------------------------------------------------------------
// RealInToDB
//	
//	The function converts the input real format to database format
//-----------------------------------------------------------------------
BOOL CFilterSortDlg::RealInToDB
	(
		const CString &pInValue,
		CString &pDBValue
	)
{
	CString myValue;
	double pCellNumValue;
	int index;

	myValue = pInValue;
	// replace decimal separator with separator defined in regional settings
	if ((index = myValue.Find(NUM_CON_POINT)) != -1)	
		myValue.SetAt(index, gDecimalSymbol);
	if ((index = myValue.Find(NUM_CON_COMMA)) != -1)	
		myValue.SetAt(index, gDecimalSymbol);
	// check if the value had not more then 2 decimal places
	if(((index = myValue.Find(gDecimalSymbol)) != -1) && (myValue.GetLength() > (index + 3)))
		return FALSE;
	
	TCHAR* tmpStopString;

	pCellNumValue = _tcstod(myValue, &tmpStopString);
	if(!((CString) tmpStopString).IsEmpty())
		return FALSE;

	if(pCellNumValue == 0)	// if the value is null, then sets the output string to "0"
	{
		pDBValue = _T("0");
		return TRUE;
	}

	int tmpDecimal;
	int tmpSign;
   char *buffer;
	CString tmpString;

   buffer = _fcvt(pCellNumValue, 2, &tmpDecimal, &tmpSign);	// converts double to string
	tmpString = _T(buffer);
	pDBValue = _T("");
	pDBValue = pDBValue + tmpString.Left(tmpDecimal) + gDecimalSymbol;	// sets the decimal symbol
	if(tmpDecimal == -1)	// if the value < 0.1, then adds the "0" to the string -> "0.0"
		pDBValue = pDBValue + _T("0");
	pDBValue = pDBValue + tmpString.Mid(tmpDecimal);
	if(tmpSign)	// if the value is negative adds a minus symbol
		pDBValue = _T("-") + pDBValue;
	if(pDBValue.Right(1) == _T("0"))	// cuts the final null
		pDBValue = pDBValue.Left(pDBValue.GetLength() - 1);
	if(pDBValue.Right(1) == _T("0"))	// cuts the semi-final null
		pDBValue = pDBValue.Left(pDBValue.GetLength() - 2);

	return TRUE;
}

//-----------------------------------------------------------------------
// SetFilterCondition
//	
//	The function sets the member variable which represent final filter
// condition
//-----------------------------------------------------------------------
void CFilterSortDlg::SetFilterCondition()
{
	CString tmpFilterClause;
	m_FilterClause.Empty();
	m_EditFilter.GetWindowText(tmpFilterClause);	// get the text from EditFilter
	if(tmpFilterClause.GetLength() > 0)
		m_FilterClause = m_FilterClause + WHERE_STRING + tmpFilterClause;
}

//-----------------------------------------------------------------------
// SetSortCondition
//	
//	The function sets the member variable which represent final sort
// condition
//-----------------------------------------------------------------------
void CFilterSortDlg::SetSortCondition()
{
	int myCount;
	CString myText;

	m_SortClause.Empty();
	myCount = m_ListSort.GetCount();
	if(myCount != LB_ERR)
	{
		for(int i = 0; i < myCount; i++)
		{
			m_ListSort.GetText(i, (CString&) myText);
			// add the sort info in form: 'XXXX+ ' or 'XXXX- '
			m_SortClause += myText.Mid(2, 4) + 
				myText.Left(1) + BLANK_STRING;
		}
		if(i > 0)
			m_SortClause = m_SortClause.Left(m_SortClause.GetLength() - 1);
	}
}

//-----------------------------------------------------------------------
// OnOK
//	
//	The function executed the selected conditions
//-----------------------------------------------------------------------
// CHANGES:
//-----------------------------------------------------------------------
//	DATE       | AUTHOR            | DESCRIPTION
//-----------------------------------------------------------------------
// 03.05.1999 | Martin Danihel    | Saved views adjustment
//-----------------------------------------------------------------------
void CFilterSortDlg::OnOK() 
{
	// some tests if the Filter and Sort is OK
	if(CountOfParanthesis > 0)	// if there are left parantesis there are not bound with right paranthesis
		AfxMessageBox("OnOK(). Too many left parenthesis.", MB_OK + MB_ICONSTOP);
	else
	{
		SetFilterCondition();
		SetSortCondition();
		if(ViewIsValid)
		{
			int myPosition;

			// test if some view is selected
			myPosition = m_ComboView.GetCurSel();
			if(myPosition >= 0)
			{
				CString myViewName;
				CString UrnoText;
				CString ViewName;
				CString TableName;
				CString FilterText;
				CString SortText;
				m_ComboView.GetLBText(myPosition, myViewName);	// get the view name
				for(int i = 0; i < mySavedViews.GetSize(); i++)
				{
					mySavedViews.GetAt(i, UrnoText, ViewName, TableName, FilterText, SortText);
					if(ViewName == myViewName)	// compare the view names
						m_ViewUrno = UrnoText;
				}
			}
		}
		else
			m_ViewUrno.Empty();

		CDialog::OnOK();
	}
}

//-----------------------------------------------------------------------
// OnButtonApply
//	
//	The function execute the selected view in view combo
//-----------------------------------------------------------------------
// CHANGES:
//-----------------------------------------------------------------------
//	DATE       | AUTHOR            | DESCRIPTION
//-----------------------------------------------------------------------
// 24.02.1999 | Martin Danihel    | The function makes the same action as 
//            |                   | the function OnOK()
//-----------------------------------------------------------------------
void CFilterSortDlg::OnButtonApply()
{
	OnOK();
/*
	CString myViewName;
	int position;
	CString UrnoText;
	CString ViewName;
	CString TableName;
	CString FilterText;
	CString SortText;

	position = m_ComboView.GetCurSel();
	if(position == CB_ERR)
	{
		AfxMessageBox("OnButtonApply(). No saved view is selected.", MB_OK + MB_ICONSTOP);
		return;
	}
	else
	{
		m_ComboView.GetLBText(position, myViewName);	// get the view name
		for(int i = 0; i < mySavedViews.GetSize(); i++)
		{
			mySavedViews.GetAt(i, UrnoText, ViewName, TableName, FilterText, SortText);
			if(ViewName == myViewName)
			{
				m_FilterClause.Empty();
				m_FilterClause = m_FilterClause + WHERE_STRING + FilterText;
				m_SortClause = SortText;
				CDialog::OnOK();
			}
		}
	}
*/
}

//-----------------------------------------------------------------------
// OnButtonSave
//	
//	The function saved the selected filter and sort clause into the table
// VCD
//-----------------------------------------------------------------------
// CHANGES:
//-----------------------------------------------------------------------
//	DATE       | AUTHOR            | DESCRIPTION
//-----------------------------------------------------------------------
// 14.04.1999 | Martin Danihel    | Added 'default' view functionality
//-----------------------------------------------------------------------
// 02.05.1999 | Martin Danihel    | Saved views adjustment
//-----------------------------------------------------------------------
void CFilterSortDlg::OnButtonSave() 
{
	CString myViewName;
	CString ParamTableName = VCDTAB + gTableExt;
	ConvertStringToServer(ParamTableName);
	CString ParamTableFields;
	CString ParamValues;
	CString FilterSortText;
	CString NewFilterText;
	CString NewSortText;
	bool NeedUpdate = TRUE;
	int myPosition;
	CString tmpString;

	m_ComboView.GetWindowText(tmpString);
	tmpString.TrimLeft();
	tmpString.TrimRight();
	m_ComboView.SetWindowText(tmpString);
	m_ComboView.ShowDropDown(TRUE);
	m_ComboView.ShowDropDown(FALSE);

	//test if some view is selected
	myPosition = m_ComboView.GetCurSel();
	if(myPosition == CB_ERR)
	{
		m_ComboView.GetWindowText(myViewName);	// get the text from combo View
		myViewName.TrimLeft();
		myViewName.TrimRight();
		if(myViewName.IsEmpty())
		{
			AfxMessageBox("OnButtonSave(). No view is selected or no valid name entered.", MB_OK + MB_ICONSTOP);
			return;
		}
		if(myViewName.GetLength() > MAX_LEN_VIEW_NAME)
		{
			AfxMessageBox("OnButtonSave(). View name is too long.", MB_OK + MB_ICONSTOP);
			return;
		}
		NeedUpdate = FALSE;
	}
	else
		m_ComboView.GetLBText(myPosition, myViewName);	// get the view name

	if(myViewName == DEF_VIEW_NAME)
	{
		CString OutputStr;
		OutputStr.Format(_T("OnButtonSave(). View %s cannot be changed."), 
							DEF_VIEW_NAME);
		AfxMessageBox((LPCTSTR)OutputStr, MB_OK + MB_ICONSTOP);
		return;
	}

	ConvertStringToServer(myViewName);

	// some tests if the Filter and Sort is OK
	if(CountOfParanthesis > 0)	// if there are left parantesis there are not bound with right paranthesis
	{
		AfxMessageBox("OnButtonSave(). Too many left parenthesis.", MB_OK + MB_ICONSTOP);
		return;
	}
	else
	{
		m_EditFilter.GetWindowText(FilterSortText);	// get the text from EditFilter
		FilterSortText.TrimLeft();
		FilterSortText.TrimRight();
		if (FilterSortText.GetLength() > 0)
			NewFilterText = FilterSortText;

		CString myText;
		CString mySortText;
		int myCount;

		myCount = m_ListSort.GetCount();
		if(myCount != LB_ERR)
		{
			int i;

			for(i = 0; i < myCount; i++)	// add the sorted fields to sort text
			{
				m_ListSort.GetText(i, (CString&) myText);
				mySortText += myText.Mid(2, 4) + 
					myText.Left(1) + BLANK_STRING;
			}
			if(i > 0)
			{
				mySortText = mySortText.Left(mySortText.GetLength() - 1);
				mySortText.TrimLeft();
				mySortText.TrimRight();
				if (mySortText.GetLength() > 0)
				{
					FilterSortText += FILTER_SORT_SEPARATOR + mySortText;
					NewSortText = mySortText;
				}
			}
		}
		else
		{
			AfxMessageBox("OnButtonSave(). m_ListSort.GetCount() = LB_ERR.", MB_OK + MB_ICONSTOP);
			return;
		}
	}
	// set the data field list
	ParamValues = _T("");
	ParamValues = ParamValues +
		APP_NAME +
		DELIMITER_CHAR +
		gUserName +
		DELIMITER_CHAR +
		myViewName +
		DELIMITER_CHAR +
		m_TableName + gTableExt +
		DELIMITER_CHAR;

	if(FilterSortText.GetLength() > MAX_LEN_FILTER + MAX_LEN_SORT)
	{
		AfxMessageBox("OnButtonSave(). Sort text too long.", MB_OK + MB_ICONSTOP);
		return;
	}

	if(FilterSortText.GetLength() > 0)
	{
		ConvertStringToServer(FilterSortText);
		ParamValues += FilterSortText;
	}
	else
		ParamValues += DB_NULL_VALUE;

	// set the field name list
	ParamTableFields = _T("APPN");
	ParamTableFields = ParamTableFields +
		DELIMITER_CHAR +
		_T("PKNO") + 
		DELIMITER_CHAR +
		_T("CTYP") +
		DELIMITER_CHAR +
		_T("CKEY") +
#ifdef ABBCCS_RELEASE
		DELIMITER_CHAR +_T("TEXT");
#else
		DELIMITER_CHAR +_T("TXT2");
#endif

	int ret = 0;
	if(NeedUpdate)
	{
		CString ParamWhereClause;
		CString UrnoText;
		CString ViewName;
		CString TableName;
		CString FilterText;
		CString SortText;

		for(int i = 0; i < mySavedViews.GetSize(); i++)
		{
			// search for the URNO of the view
			mySavedViews.GetAt(i, UrnoText, ViewName, TableName, FilterText, SortText);
			if(ViewName == myViewName)
			{
				// set the selection criteria
				ParamWhereClause = ParamWhereClause + WHERE_STRING + _T("URNO = ") + UrnoText;

				ret = DBDPSCallCeda(UFIS_UPDATE, ParamTableName, NULL,
									ParamWhereClause, ParamTableFields, ParamValues);
				if (ret)
					AfxMessageBox("OnButtonSave(). Error on CallCeda. " + gErrorMessage + POINT_CHAR,
									MB_OK + MB_ICONSTOP);
				else
				{
					mySavedViews.SetAt(i, UrnoText, ViewName, TableName, NewFilterText, NewSortText);
					if(m_ViewUrno == UrnoText)
						m_ViewUrno.Empty();	// change selected view
					ViewIsValid = TRUE;	// selected view is valid
				}
				return;
			}
		}
	}
	else
	{
		CString myUrnoText;
		if (!GetNewURNO(myUrnoText))		// create the new URNO
		{
			AfxMessageBox("OnButtonSave(). Error on GetNewURNO.", MB_OK + MB_ICONSTOP);
			return;
		}

		// add the URNO value to the data field list
		ParamValues = DELIMITER_CHAR + ParamValues;
		ParamValues = myUrnoText + ParamValues;
		// add the URNO to the field name list
		ParamTableFields = DELIMITER_CHAR + ParamTableFields;
		ParamTableFields = _T("URNO") + ParamTableFields;

		ret = DBDPSCallCeda(UFIS_INSERT, ParamTableName, NULL,
							NULL, ParamTableFields, ParamValues);
		if (ret)
			AfxMessageBox("OnButtonSave(). Error on CallCeda. " + gErrorMessage + POINT_CHAR, 
							MB_OK + MB_ICONSTOP);
		else
		{
			// add new view to intern view list
			mySavedViews.Add(myUrnoText, myViewName, gTableName, NewFilterText, NewSortText);
			if(!FillCombo())	// refill the view combo
				AfxMessageBox("OnButtonSave(). FillCombo = FALSE.", MB_OK + MB_ICONSTOP);
			m_ComboView.SetCurSel(m_ComboView.GetCount() - 1);
			ViewIsValid = TRUE;	// selected view is valid
			return;
		}
	}
}

//-----------------------------------------------------------------------
// OnButtonDelete
//	
//	The function deleted the selected view in the table VCD
//-----------------------------------------------------------------------
// CHANGES:
//-----------------------------------------------------------------------
//	DATE       | AUTHOR            | DESCRIPTION
//-----------------------------------------------------------------------
// 14.04.1999 | Martin Danihel    | Added 'default' view functionality
//-----------------------------------------------------------------------
// 02.05.1999 | Martin Danihel    | Saved views adjustment
//-----------------------------------------------------------------------
void CFilterSortDlg::OnButtonDelete() 
{
	int myPosition;
	CString tmpString;

	m_ComboView.GetWindowText(tmpString);
	tmpString.TrimLeft();
	tmpString.TrimRight();
	m_ComboView.SetWindowText(tmpString);
	m_ComboView.ShowDropDown(TRUE);
	m_ComboView.ShowDropDown(FALSE);

	//test if some view is selected
	myPosition = m_ComboView.GetCurSel();
	if(myPosition == CB_ERR)
	{
		AfxMessageBox("OnButtonDelete(). No saved view is selected.", MB_OK + MB_ICONSTOP);
		return;
	}
	else
	{
		CString myViewName;
		CString UrnoText;
		CString ViewName;
		CString TableName;
		CString FilterText;
		CString SortText;
		m_ComboView.GetLBText(myPosition, myViewName);	// get the view name
		if(myViewName == DEF_VIEW_NAME)
		{
			CString OutputStr;
			OutputStr.Format(_T("OnButtonDelete(). View %s cannot be deleted."), 
								DEF_VIEW_NAME);
			AfxMessageBox((LPCTSTR)OutputStr, MB_OK + MB_ICONSTOP);
			return;
		}
		for(int i = 0; i < mySavedViews.GetSize(); i++)
		{
			mySavedViews.GetAt(i, UrnoText, ViewName, TableName, FilterText, SortText);
			if(ViewName == myViewName)	// compare the view names
			{
				CString ParamTableName = _T("VCD") + gTableExt;
				CString ParamWhereClause;
				ParamWhereClause = ParamWhereClause + WHERE_STRING + _T("URNO = ") + UrnoText;

				ConvertStringToServer(ParamTableName);

				int ret = DBDPSCallCeda(UFIS_DELETE, ParamTableName, NULL,
										ParamWhereClause, NULL, NULL);
				if (ret)
				{
					AfxMessageBox("OnButtonDelete(). Error on CallCeda. " + gErrorMessage + POINT_CHAR,
									MB_OK + MB_ICONSTOP);
					return;
				}
				mySavedViews.RemoveAt(myPosition);
				if(!FillCombo())
					AfxMessageBox("OnButtonDelete(). FillCombo = FALSE.", MB_OK + MB_ICONSTOP);
				if(m_ViewUrno == UrnoText)
					m_ViewUrno.Empty();	// delete selected view
				ViewIsValid = FALSE;	// no selected view

				return;
			}
		}
	}
}

//-----------------------------------------------------------------------
// OnSelchangeComboView
//	
//	The function shows the selected view from the table VCD in dialog
//-----------------------------------------------------------------------
// CHANGES:
//-----------------------------------------------------------------------
//	DATE       | AUTHOR            | DESCRIPTION
//-----------------------------------------------------------------------
// 03.05.1999 | Martin Danihel    | Saved views adjustment
//-----------------------------------------------------------------------
void CFilterSortDlg::OnSelchangeComboView()
{
	int myPosition;

	// test if some view is selected
	myPosition = m_ComboView.GetCurSel();
	if(myPosition == CB_ERR)
		return;
	else
	{
		CString myViewName;
		CString UrnoText;
		CString ViewName;
		CString TableName;
		CString FilterText;
		CString SortText;
		m_ComboView.GetLBText(myPosition, myViewName);	// get the view name
		for(int i = 0; i < mySavedViews.GetSize(); i++)
		{
			mySavedViews.GetAt(i, UrnoText, ViewName, TableName, FilterText, SortText);
			if(ViewName == myViewName)	// compare the view names
			{
				m_EditFilter.SetWindowText(FilterText);
				CountOfParanthesis = 0;	
				SetSortConditionFromText(SortText);
				ViewIsValid = TRUE;	// selected view is valid
				return;
			}
		}
	}
}

//-----------------------------------------------------------------------
// SetSortConditionFromText
//	
//	The function sets the select-tab with the pSortText
//-----------------------------------------------------------------------
BOOL CFilterSortDlg::SetSortConditionFromText(LPCTSTR pSortText)
{
	CString mySortText = pSortText;

	OnButtonLeftAll();	// sets the default state of sort-tab

	if(mySortText.GetLength() > 0)	// if the sort string is not empty
	{
		if(mySortText < (SYS_FIELD_LENGTH[F_FINA] + 1))	// mySortText is corrupt
		{
			AfxMessageBox("SetSortConditionFromText(). Sort string is corrupt.", MB_OK + MB_ICONSTOP);
			return FALSE;
		}
		CString myFieldName;
		BOOL mySignPlus;
		int myCount;
		CString myText;
		BOOL myFound;

		while(TRUE)
		{
			myFieldName = mySortText.Left(SYS_FIELD_LENGTH[F_FINA]);	// get the field name
			mySignPlus = (mySortText.Mid(SYS_FIELD_LENGTH[F_FINA], 1) == _T("+"));	// get the sign

			myCount = m_ListChoice.GetCount();
			if(myCount == LB_ERR)
			{
				AfxMessageBox("SetSortConditionFromText(). m_ListChoice.GetCount() = LB_ERR.", MB_OK + MB_ICONSTOP);
				return FALSE;
			}

			myFound = FALSE;
			// search for the item in choice list
			for(int i = 0; i < myCount; i++)
			{
				m_ListChoice.GetText(i, (CString&) myText);
				myText = myText.Left(SYS_FIELD_LENGTH[F_FINA]);
				if(myText == myFieldName)
				{
					m_ListChoice.SetCurSel(i);	// select my item
					MoveRight(mySignPlus);	// move my item to sort list
					myFound = TRUE;
					break;
				}
			}
			if(!myFound)
			{
				AfxMessageBox("SetSortConditionFromText(). Column name not found.", MB_OK + MB_ICONSTOP);
				return FALSE;
			}

			if(mySortText.GetLength() < (SYS_FIELD_LENGTH[F_FINA] + 2))
				break;
			else
				mySortText = mySortText.Right(mySortText.GetLength() - (SYS_FIELD_LENGTH[F_FINA] + 2));
		}
	}

	return TRUE;
}
