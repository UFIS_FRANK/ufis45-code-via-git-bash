#if !defined(AFX_FILTERSORTDLG_H__76E05881_B6D1_11D2_A34B_006097321D34__INCLUDED_)
#define AFX_FILTERSORTDLG_H__76E05881_B6D1_11D2_A34B_006097321D34__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// FilterSortDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CFilterSortDlg dialog

#include <Globals.h>

class CFilterSortDlg : public CDialog
{
// Construction
public:
	CFilterSortDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CFilterSortDlg)
	enum { IDD = IDD_DIALOG_FILTER_SORT };
	CStatic	m_TextSort;
	CStatic	m_TextChoice;
	CListBox	m_ListSort;
	CListBox	m_ListChoice;
	CComboBox	m_ComboView;
	CButton	m_ButtonUp;
	CButton	m_ButtonRightAll;
	CButton	m_ButtonRight;
	CButton	m_ButtonLeftAll;
	CButton	m_ButtonLeft;
	CButton	m_ButtonDown;
	CButton	m_ButtonAscDesc;
	CComboBox	m_ComboDataField;
	CEdit	m_EditFilter;
	CEdit	m_EditCondition;
	CButton	m_ButtonLeftParanthesis;
	CButton	m_ButtonNew;
	CButton	m_ButtonRightParanthesis;
	CButton	m_ButtonSetting;
	CComboBox	m_ComboOperator;
	CStatic	m_TextFilter;
	CStatic	m_TextOperator;
	CStatic	m_TextDataField;
	CStatic	m_TextCondition;
	CStatic	m_TextAndOr;
	CTabCtrl	m_Tab;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFilterSortDlg)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
public:
	CString m_TableName;
	CString m_FilterClause;
	CString m_SortClause;
	CString m_ViewUrno;
	BOOL m_ShowView;
	CSavedViews mySavedViews;

protected:
	BOOL FillCombo();
	void InitTab();
	void SetDefaultValues();
	void SetFilterSortClause();
	BOOL InitData();
	BOOL Prepare();
	void SetSecurity();	
	void EnableFilter(BOOL pEnable);	
	void EnableSort(BOOL pEnable);	
	void ShowTabFilter(int);
	void ShowTabSort(int);
	void Exchange(int, int);
	void SetFilterCondition();
	void SetSortCondition();
	BOOL SetSortConditionFromText(LPCTSTR);
	void MoveRight(BOOL);
	BOOL ConvertInToDB
		(
			CString &pNameOfTheColumn,
			const CString &pInputValue,
			CString &pDBValue
		);
	BOOL DateInToDB(const CString&, CString&);
	BOOL TimeInToDB(const CString&, CString&, int);
	BOOL DateTimeInToDB(const CString&, CString&, int);
	BOOL RealInToDB(const CString&, CString&);

	// Generated message map functions
	//{{AFX_MSG(CFilterSortDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeTab(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnButtonAscDesc();
	afx_msg void OnButtonDown();
	afx_msg void OnButtonLeft();
	afx_msg void OnButtonLeftAll();
	afx_msg void OnButtonRight();
	afx_msg void OnButtonRightAll();
	afx_msg void OnButtonUp();
	afx_msg void OnButtonNew();
	afx_msg void OnButtonLeftParenthesis();
	afx_msg void OnButtonRightParenthesis();
	afx_msg void OnButtonSetting();
	virtual void OnOK();
	afx_msg void OnButtonApply();
	afx_msg void OnButtonSave();
	afx_msg void OnButtonDelete();
	afx_msg void OnSelchangeComboView();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FILTERSORTDLG_H__76E05881_B6D1_11D2_A34B_006097321D34__INCLUDED_)
