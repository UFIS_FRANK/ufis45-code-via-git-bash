#if !defined(AFX_UFISEXCELCTL_H__1F95709B_1B65_4BE5_BC5C_A8F12E8B2C4A__INCLUDED_)
#define AFX_UFISEXCELCTL_H__1F95709B_1B65_4BE5_BC5C_A8F12E8B2C4A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// UfisExcelCtl.h : Declaration of the CUfisExcelCtrl ActiveX Control class.

/////////////////////////////////////////////////////////////////////////////
// CUfisExcelCtrl : See UfisExcelCtl.cpp for implementation.

class CUfisExcelCtrl : public COleControl
{
	DECLARE_DYNCREATE(CUfisExcelCtrl)

// Constructor
public:
	CUfisExcelCtrl();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUfisExcelCtrl)
	public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();
	//}}AFX_VIRTUAL

// Implementation
protected:
	~CUfisExcelCtrl();

	DECLARE_OLECREATE_EX(CUfisExcelCtrl)    // Class factory and guid
	DECLARE_OLETYPELIB(CUfisExcelCtrl)      // GetTypeInfo
	DECLARE_PROPPAGEIDS(CUfisExcelCtrl)     // Property page IDs
	DECLARE_OLECTLTYPE(CUfisExcelCtrl)		// Type name and misc status

// Message maps
	//{{AFX_MSG(CUfisExcelCtrl)
		// NOTE - ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

// Dispatch maps
	//{{AFX_DISPATCH(CUfisExcelCtrl)
	afx_msg BOOL GenerateFromCSV(LPCTSTR FileName);
	afx_msg BOOL GenerateFromXSL(LPCTSTR FileName);
	afx_msg BOOL GenerateFromTabCtrl(LPDISPATCH ITabCtrl);
	afx_msg BOOL GenerateFromTab(OLE_HANDLE HTabCtrl);
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()

	afx_msg void AboutBox();

// Event maps
	//{{AFX_EVENT(CUfisExcelCtrl)
	void FireNewWorkBook(LPDISPATCH popWorkbook)
		{FireEvent(eventidNewWorkBook,EVENT_PARAM(VTS_DISPATCH), popWorkbook);}
	void FireSheetSelectionChange(LPDISPATCH popSheet, LPDISPATCH popTarget)
		{FireEvent(eventidSheetSelectionChange,EVENT_PARAM(VTS_DISPATCH  VTS_DISPATCH), popSheet, popTarget);}
	void FireSheetBeforeDoubleClick(LPDISPATCH popSheet, LPDISPATCH popTarget, BOOL FAR* popCancel)
		{FireEvent(eventidSheetBeforeDoubleClick,EVENT_PARAM(VTS_DISPATCH  VTS_DISPATCH  VTS_PBOOL), popSheet, popTarget, popCancel);}
	void FireSheetBeforeRightClick(LPDISPATCH popSheet, LPDISPATCH popTarget, BOOL FAR* popCancel)
		{FireEvent(eventidSheetBeforeRightClick,EVENT_PARAM(VTS_DISPATCH  VTS_DISPATCH  VTS_PBOOL), popSheet, popTarget, popCancel);}
	void FireSheetActivate(LPDISPATCH popSheet)
		{FireEvent(eventidSheetActivate,EVENT_PARAM(VTS_DISPATCH), popSheet);}
	void FireSheetDeactivate(LPDISPATCH popSheet)
		{FireEvent(eventidSheetDeactivate,EVENT_PARAM(VTS_DISPATCH), popSheet);}
	void FireSheetCalculate(LPDISPATCH popSheet)
		{FireEvent(eventidSheetCalculate,EVENT_PARAM(VTS_DISPATCH), popSheet);}
	void FireSheetChange(LPDISPATCH popSheet, LPDISPATCH popTarget)
		{FireEvent(eventidSheetChange,EVENT_PARAM(VTS_DISPATCH  VTS_DISPATCH), popSheet, popTarget);}
	void FireWorkbookOpen(LPDISPATCH popWorkbook)
		{FireEvent(eventidWorkbookOpen,EVENT_PARAM(VTS_DISPATCH), popWorkbook);}
	void FireWorkbookActivate(LPDISPATCH popWorkbook)
		{FireEvent(eventidWorkbookActivate,EVENT_PARAM(VTS_DISPATCH), popWorkbook);}
	void FireWorkbookDeactivate(LPDISPATCH popWorksheet)
		{FireEvent(eventidWorkbookDeactivate,EVENT_PARAM(VTS_DISPATCH), popWorksheet);}
	void FireWorkbookBeforeClose(LPDISPATCH popWorkbook, BOOL FAR* popCancel)
		{FireEvent(eventidWorkbookBeforeClose,EVENT_PARAM(VTS_DISPATCH  VTS_PBOOL), popWorkbook, popCancel);}
	void FireWorkbookBeforeSave(LPDISPATCH popWorkbook, BOOL popCancelUI, BOOL FAR* popCancel)
		{FireEvent(eventidWorkbookBeforeSave,EVENT_PARAM(VTS_DISPATCH  VTS_BOOL  VTS_PBOOL), popWorkbook, popCancelUI, popCancel);}
	void FireWorkbookBeforePrint(LPDISPATCH popWorkbook, BOOL FAR* popCancel)
		{FireEvent(eventidWorkbookBeforePrint,EVENT_PARAM(VTS_DISPATCH  VTS_PBOOL), popWorkbook, popCancel);}
	void FireWorkbookNewSheet(LPDISPATCH popWorkbook, LPDISPATCH popSheet)
		{FireEvent(eventidWorkbookNewSheet,EVENT_PARAM(VTS_DISPATCH  VTS_DISPATCH), popWorkbook, popSheet);}
	void FireWorkbookAddinInstall(LPDISPATCH popWorkbook)
		{FireEvent(eventidWorkbookAddinInstall,EVENT_PARAM(VTS_DISPATCH), popWorkbook);}
	void FireWorkbookAddinUninstall(LPDISPATCH popWorkbook)
		{FireEvent(eventidWorkbookAddinUninstall,EVENT_PARAM(VTS_DISPATCH), popWorkbook);}
	void FireWindowResize(LPDISPATCH popWorkbook, LPDISPATCH popWindow)
		{FireEvent(eventidWindowResize,EVENT_PARAM(VTS_DISPATCH  VTS_DISPATCH), popWorkbook, popWindow);}
	void FireWindowActivate(LPDISPATCH popWorkbook, LPDISPATCH popWindow)
		{FireEvent(eventidWindowActivate,EVENT_PARAM(VTS_DISPATCH  VTS_DISPATCH), popWorkbook, popWindow);}
	void FireWindowDeactivate(LPDISPATCH popWorkbook, LPDISPATCH popWindow)
		{FireEvent(eventidWindowDeactivate,EVENT_PARAM(VTS_DISPATCH  VTS_DISPATCH), popWorkbook, popWindow);}
	//}}AFX_EVENT
	DECLARE_EVENT_MAP()

// Dispatch and event IDs
public:
	enum {
	//{{AFX_DISP_ID(CUfisExcelCtrl)
	dispidGenerateFromCSV = 1L,
	dispidGenerateFromXSL = 2L,
	dispidGenerateFromTabCtrl = 3L,
	dispidGenerateFromTab = 4L,
	eventidNewWorkBook = 1L,
	eventidSheetSelectionChange = 2L,
	eventidSheetBeforeDoubleClick = 3L,
	eventidSheetBeforeRightClick = 4L,
	eventidSheetActivate = 5L,
	eventidSheetDeactivate = 6L,
	eventidSheetCalculate = 7L,
	eventidSheetChange = 8L,
	eventidWorkbookOpen = 9L,
	eventidWorkbookActivate = 10L,
	eventidWorkbookDeactivate = 11L,
	eventidWorkbookBeforeClose = 12L,
	eventidWorkbookBeforeSave = 13L,
	eventidWorkbookBeforePrint = 14L,
	eventidWorkbookNewSheet = 15L,
	eventidWorkbookAddinInstall = 16L,
	eventidWorkbookAddinUninstall = 17L,
	eventidWindowResize = 18L,
	eventidWindowActivate = 19L,
	eventidWindowDeactivate = 20L,
	//}}AFX_DISP_ID
	};
	friend class CExcelEventSink;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UFISEXCELCTL_H__1F95709B_1B65_4BE5_BC5C_A8F12E8B2C4A__INCLUDED)
