// AppEventListener.cpp : implementation file
//

#include "stdafx.h"
#include "UfisExcel.h"
#include "AppEventListener.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAppEventListener

IMPLEMENT_DYNCREATE(CAppEventListener, CCmdTarget)

CAppEventListener::CAppEventListener()
{
}

CAppEventListener::~CAppEventListener()
{
}


BEGIN_MESSAGE_MAP(CAppEventListener, CCmdTarget)
	//{{AFX_MSG_MAP(CAppEventListener)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAppEventListener message handlers
