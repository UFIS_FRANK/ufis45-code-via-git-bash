#if !defined(AFX_UFISEXCELPPG_H__9DA1AFB9_4A19_4EDB_B70A_720989E4E1D3__INCLUDED_)
#define AFX_UFISEXCELPPG_H__9DA1AFB9_4A19_4EDB_B70A_720989E4E1D3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// UfisExcelPpg.h : Declaration of the CUfisExcelPropPage property page class.

////////////////////////////////////////////////////////////////////////////
// CUfisExcelPropPage : See UfisExcelPpg.cpp.cpp for implementation.

class CUfisExcelPropPage : public COlePropertyPage
{
	DECLARE_DYNCREATE(CUfisExcelPropPage)
	DECLARE_OLECREATE_EX(CUfisExcelPropPage)

// Constructor
public:
	CUfisExcelPropPage();

// Dialog Data
	//{{AFX_DATA(CUfisExcelPropPage)
	enum { IDD = IDD_PROPPAGE_UFISEXCEL };
		// NOTE - ClassWizard will add data members here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA

// Implementation
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Message maps
protected:
	//{{AFX_MSG(CUfisExcelPropPage)
		// NOTE - ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UFISEXCELPPG_H__9DA1AFB9_4A19_4EDB_B70A_720989E4E1D3__INCLUDED)
