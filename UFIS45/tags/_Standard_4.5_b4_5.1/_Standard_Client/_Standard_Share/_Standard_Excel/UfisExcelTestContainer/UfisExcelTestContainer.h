// UfisExcelTestContainer.h : main header file for the UFISEXCELTESTCONTAINER application
//

#if !defined(AFX_UFISEXCELTESTCONTAINER_H__27B9A8B2_8899_4AFD_A9F5_FE4A8E11D4E9__INCLUDED_)
#define AFX_UFISEXCELTESTCONTAINER_H__27B9A8B2_8899_4AFD_A9F5_FE4A8E11D4E9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CUfisExcelTestContainerApp:
// See UfisExcelTestContainer.cpp for the implementation of this class
//

class CUfisExcelTestContainerApp : public CWinApp
{
public:
	CUfisExcelTestContainerApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CUfisExcelTestContainerApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CUfisExcelTestContainerApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_UFISEXCELTESTCONTAINER_H__27B9A8B2_8899_4AFD_A9F5_FE4A8E11D4E9__INCLUDED_)
