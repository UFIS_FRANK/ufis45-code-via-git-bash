// UfisExcelTestContainerDlg.cpp : implementation file
//

#include "stdafx.h"
#include "UfisExcelTestContainer.h"
#include "UfisExcelTestContainerDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUfisExcelTestContainerDlg dialog

CUfisExcelTestContainerDlg::CUfisExcelTestContainerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CUfisExcelTestContainerDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CUfisExcelTestContainerDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CUfisExcelTestContainerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CUfisExcelTestContainerDlg)
	DDX_Control(pDX, IDC_TABCTRL1, m_Tab);
	DDX_Control(pDX, IDC_UFISEXCELCTRL1, m_Excel);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CUfisExcelTestContainerDlg, CDialog)
	//{{AFX_MSG_MAP(CUfisExcelTestContainerDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_EXCELXSL, OnButtonExcelxsl)
	ON_BN_CLICKED(IDC_BUTTON_EXCELCSV, OnButtonExcelcsv)
	ON_BN_CLICKED(IDC_BUTTON_EXCELTAB, OnButtonExceltab)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CUfisExcelTestContainerDlg message handlers

BOOL CUfisExcelTestContainerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	this->m_Tab.SetHeaderString("Column 1,Column2,Colum3,Column4");
	this->m_Tab.SetHeaderLengthString("30,30,50,50");
	this->m_Tab.SetLogicalFieldList("COL1,COL2,COL3,COL4");
	this->m_Tab.SetColumnWidthString("30,30,30,30");
	this->m_Tab.SetColumnAlignmentString("L,L,L,L");
	this->m_Tab.ShowHorzScroller(true);
	this->m_Tab.EnableHeaderSizing(true);
	this->m_Tab.ResetContent();

	this->m_Tab.InsertTextLineAt(0,"Test1,Test2,Test3,Test4",FALSE);
	this->m_Tab.InsertTextLineAt(0,"Test1,Test2,Test3,Test4",FALSE);
	this->m_Tab.InsertTextLineAt(0,"Test1,Test2,Test3,Test4",FALSE);
	this->m_Tab.InsertTextLineAt(0,"Test1,Test2,Test3,Test4",FALSE);
	this->m_Tab.InsertTextLineAt(0,"Test1,Test2,Test3,Test4",FALSE);
	this->m_Tab.InsertTextLineAt(0,"Test1,Test2,Test3,Test4",FALSE);
	this->m_Tab.InsertTextLineAt(0,"Test1,Test2,Test3,Test4",FALSE);
	this->m_Tab.InsertTextLineAt(0,"Test1,Test2,Test3,Test4",FALSE);

	this->m_Tab.RedrawTab();
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CUfisExcelTestContainerDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CUfisExcelTestContainerDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CUfisExcelTestContainerDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CUfisExcelTestContainerDlg::OnButtonExcelxsl() 
{
	// TODO: Add your control notification handler code here
	this->m_Excel.GenerateFromXSL("C:\\Documents and Settings\\tho\\My Documents\\TestLoginControl_PRF4182.xls");
}

void CUfisExcelTestContainerDlg::OnButtonExcelcsv() 
{
	// TODO: Add your control notification handler code here
	this->m_Excel.GenerateFromCSV("C:\\Documents and Settings\\tho\\My Documents\\TestLoginControl_PRF4182.csv");
}

void CUfisExcelTestContainerDlg::OnButtonExceltab() 
{
	// TODO: Add your control notification handler code here
	IUnknown *pIUnknown   = this->m_Tab.GetControlUnknown();
	IDispatch *pIDispatch =	NULL;
	HRESULT rc = pIUnknown->QueryInterface(IID_IDispatch,(void **)&pIDispatch);
	
	this->m_Excel.GenerateFromTabCtrl(pIDispatch);

}

BEGIN_EVENTSINK_MAP(CUfisExcelTestContainerDlg, CDialog)
    //{{AFX_EVENTSINK_MAP(CUfisExcelTestContainerDlg)
	ON_EVENT(CUfisExcelTestContainerDlg, IDC_UFISEXCELCTRL1, 1 /* NewWorkBook */, OnNewWorkBookUfisexcelctrl1, VTS_DISPATCH)
	ON_EVENT(CUfisExcelTestContainerDlg, IDC_UFISEXCELCTRL1, 8 /* SheetChange */, OnSheetChangeUfisexcelctrl1, VTS_DISPATCH VTS_DISPATCH)
	//}}AFX_EVENTSINK_MAP
END_EVENTSINK_MAP()

void CUfisExcelTestContainerDlg::OnNewWorkBookUfisexcelctrl1(LPDISPATCH popWorkbook) 
{
	// TODO: Add your control notification handler code here
	AfxMessageBox("Yep, New Excel Workbook");
	
}

void CUfisExcelTestContainerDlg::OnSheetChangeUfisexcelctrl1(LPDISPATCH popSheet, LPDISPATCH popTarget) 
{
	// TODO: Add your control notification handler code here
	AfxMessageBox("Yep, Sheet change");
	
}
