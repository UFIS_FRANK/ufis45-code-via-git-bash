rem Copies all generated files from the SCM-Structue into the Install-Shield directory.
rem 

rem set drive to J: (IS 6.2) or K: (IS 8.0) 
set drive=k:

rem Copy ini- and config-files
copy c:\ufis_bin\ceda.ini %drive%\ADR_LM_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\bdps_sec.cfg %drive%\ADR_LM_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\*.bmp %drive%\ADR_LM_build\ufis_client_appl\system\*.*


rem copy applications
copy c:\ufis_bin\debug\bdps_sec.exe %drive%\ADR_LM_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\bdpspass.exe %drive%\ADR_LM_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\bdpsuif.exe %drive%\ADR_LM_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\fips.exe %drive%\ADR_LM_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\rules.exe %drive%\ADR_LM_build\ufis_client_appl\applications\*.*



REM Copy system-files
copy c:\ufis_bin\release\bcproxy.exe %drive%\ADR_LM_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcproxy.tlb %drive%\ADR_LM_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcserv32.exe %drive%\ADR_LM_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufis32.dll %drive%\ADR_LM_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisappmng.exe %drive%\ADR_LM_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisappmng.tlb %drive%\ADR_LM_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\tab.ocx %drive%\ADR_LM_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\tab.tlb %drive%\ADR_LM_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufiscom.ocx %drive%\ADR_LM_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufiscom.tlb %drive%\ADR_LM_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ucom.ocx %drive%\ADR_LM_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ucom.tlb %drive%\ADR_LM_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ugantt.ocx %drive%\ADR_LM_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ugantt.tlb %drive%\ADR_LM_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\aatlogin.ocx %drive%\ADR_LM_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\aatlogin.tlb %drive%\ADR_LM_build\ufis_client_appl\system\*.*


rem Copy Help-Files
rem copy c:\ufis_bin\Help\*.chm %drive%\ADR_LM_build\ufis_client_appl\help\*.*
