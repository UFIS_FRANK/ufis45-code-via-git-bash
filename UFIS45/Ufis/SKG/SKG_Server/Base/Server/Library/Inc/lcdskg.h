#ifndef _DEF_mks_version_lcdhdl_h
  #define _DEF_mks_version_lcdhdl_h
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_lcdhdl_h[]   MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/SKG/SKG_Server/Base/Server/Library/Inc/lcdskg.h 1.6 2005/11/16 00:25:10SGT jwe Exp  $";
#endif /*_DEF_mks_version_lcdhdl_h*/
/*********************************************************/
/* LCDHDL (AEG-MIS LCD board controller handler )        */
/* header file                                           */
/*********************************************************/
#ifndef BOOL
#define BOOL int
#endif

/***************/
/* ALL DEFINES */
/***************/
#define XXS_BUFF  32
#define XS_BUFF  128
#define S_BUFF  512
#define M_BUFF  1024
#define L_BUFF  2048
#define XL_BUFF 4096
#define JF_BUFF  200

#ifndef MAX_BUFFER_LEN
	#define MAX_BUFFER_LEN 2048
#endif
#define DATABLK_SIZE  524288 /* Maximum space for sql_if data_area&sql-buffer*/
#define CONNECT_TIMEOUT 2
#define READ_TIMEOUT 2
#define WRITE_TIMEOUT 2

#define RIGHT 1100
#define LEFT 1101

#define CFG_ALPHA 1200
#define CFG_NUM		1201
#define CFG_ALPHANUM	1202
#define CFG_IGNORE	1203
#define CFG_PRINT	1204

#define ENGLISH 0
#define LOCAL	1


/************************/
/* CFG-File - STRUCTURE */
/************************/
typedef struct 
{
	/* MAIN section in .cfg file*/    
	char *refresh_cycle;		/* refresh-time for board-group */ 
	char *send_delay;	
	int ilSendDelay;
	char *lcd_ips;					/* IP-address(es) or hostname(s) of the LCD-boards */ 
	char *board_cols;				/* columns of board */
	char *board_rows;				/* rows of board */
	char *service_port; 		/* service name for receiving data */
	char *use_counter_table;	/* use ccatab or afttab ? */
	char *db_tables; 				/* tables */
	char *db_est_time_field;	/* field to use for estimated times */
	char *db_fields; 				/* field list from db*/
	char *start_min; 	/* number of minutes to substract from time for DB-select */
	char *end_min; 	  /* number of minutes to add to time for DB-select */
	char *t_para1; 							/* time paramter 1 for select */
	char *t_para2; 							/* time paramter 2 for select */
	char *db_cond; 							/* where clause for db request (english && local)*/
	char *db_field_offset;			/* offset on LCD board for display */
	char *db_field_len; 				/* len to show */
	char *repl_fields; 					/* fields to replace with full-texts */
	char *replacement; 					/* replacement for field */
	char *reference; 						/* connection between the fields */
	char *repl_table; 					/* table which is used for resolving */
	char *date_fields; 					/* date fields */
	char *date_format; 					/* format of date fields */
	char *repl_board_letter;  	/* number of letter to replace */
	char *repl_char; 				  	/* char to use for replacement */
	char *blink_letter;
	char *blink_ref;
	char *blink_cond;
	char *blink_char;
	char *sos_seq;
	char sosbuf[128];
	int  soslen;
	char *eos_seq;
	char eosbuf[128];
	int  eoslen;
	char *blankline; 
	char stdseq[8]; 
	int  stdseqlen;
	char sotseq[8]; 
	int  sotseqlen;
	char *EstTimeUse;         /* shall est. times be set into remarks or not ?*/
}CFG;

typedef struct
{
  time_t tlNextConnect ;
	time_t tlNextRefresh ;
	int ilLang;
	int ilGroupNr;
	int ilGroupRows;
	int ilMaxOffs;
	int ilLastLen;
	int ilValid;
	CFG rlCfg;
	LPLISTHEADER prlBoards;
	LPLISTHEADER prlFlightInfo;
	char *pclReplacements;
	char *pclEngBuff;
	/* added 09.08.2002 - JWE for convert. counter entries from e.g. 02-02 to 02 only */
	int ilCkif_offset;
	int ilCkif_length;
	int ilCkit_offset;
	int ilCkit_length;
} GROUPINFO ;

typedef struct
{
	int ilSocket;
	int ilSendDelay;
	unsigned long ilIp;
	int ilPort;
	int ilCols;
	int ilRows;
	int ilPlan;
	int ilAct;
}BOARDINFO;

typedef struct
{
	int  ilActivItem;
	char cpPJfno[JF_BUFF];
}CODE_SHARE_FLNO ;

typedef struct
{
	int  ilActivItem;
	char *cpPRem ;
}REMARKS ;


typedef struct
{
	long urno;
	int ilRow;
	char pclBlinkCond[128];
	BOARDINFO       *prlBoard;
	CODE_SHARE_FLNO prlCodeShareFlnos;
	REMARKS         *prlRemarks;
}FLIGHTINFO;
