
#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/CAAS_GMS/CAAS_GMS_Server/Base/Server/Bline/fcshdl.c 1.31 2013/06/25 10:17:28CET dka Exp fei(2012/05/31 08:50:03CET) $";
#endif /* _DEF_mks_version */
/**********************************************************************************/
/*                                                                                */
/* Ufis to FCS Interface process                                                  */
/*                                                                                */
/* Author         : jim                                                           */
/* Date           : 20061104                                                      */
/* Description    : Interface between FCS and UFIS in Singapur using bline driver */
/*                                                                                */
/* Update history :                                                               */
/*  20061128: added configurable Pause igNapTime                                  */
/*  20061129: send arrival record on split also                                   */
/*  20061130: reformat of LH 001 to LH1                                           */
/*  20061130: added tow stand                                                     */
/*  20061222: commented InsertUfis2FcsFlno                                        */
/*            relocated libsol-blinedriver-c-1.0.a to $PB/Library/3PartyLib for   */
/*            use of blineError()                                                 */
/*  20070316	DKA	PAUSING upper limit change to 5000ms.
 *  			Fixed bug of not awaiting for initial "1 " FCREQ if
 *  			started in mid-stream of FCS flow.
 *  			"nap" for PAUSING moved to sending to FCS, otherwise
 *  			we would wait even while reading FCS...
 *  20070320	DKA	Reinstated a nap() in BlineRead to reduce CPU time.
 *			PAUSING change to seconds since cannot call nap()
 *			more than once in the program. The second nap does not
 *			wait for the specified ms. So use sleep() which reads
 *			seconds. To reduce confusion, just modulo the PAUSING.
 *  20070326    DKA	Introduce NUMBER_OUTGOING_MSGS in case need to tune to
 *			to lower than value of 12.
 *			Increase upper limit of PAUSING to 10 seconds.
 *  20070328 1.11  DKA	Do not send our Type 6 flow controls before initial
 *			"1 " from FCS. This is to cater for FCS difference from
 *			ELS. (Read - "a bug in FCS"..)
 *			Reset MAX RETRY (igBlineRetry) to zero when initial
 *			"1 " is received after a prior timeout to avoid never
 *			returning ONLINE.
 * 20070510 1.12   DKA	CGMS requires that we send the PSTA of only the first
 *			towing record (i.e. where the aircraft is first towed
 *			to after landing) to FCS. Use AURN of the tow record to
 *			figure this out.
 * 20070515 1.14   DKA	Send tow blanking if all towing records are deleted. 
 *			We handle DFR for this.
 *			For towing DFR, add arrivals STOA to ceda field list.
 *			For towing UFR, replace tow STOA with arrival STOA. This
 *			avoids potential FLDA mixup if towing crosses midnight.
 * 20070516 1.15   DKA	Change actions when tow DFR received.
 * 20070521 1.16   DKA	Fix crash when processing Ceda Message in
 *			HandleData. (per BST advise). Requires ignoring msgs
 *			fm ROG,FOG's. Their module id's are added to config.
 *			Try fix "repeated sending" bug with new flag for if
 *			a type 6 msg has been sent.
 * 20070534 1.17   DKA	Rationalize towing handling all within new function
 *			HandleTowing () which replaces iIsFirstTowing ().
 * 20070528 1.18   DKA	Reset bline API when bline error encountered during
 *			blineRead (). Add sleep in Reset().
 * 20071114 1.19   DKA	CR07020 - Send terminals for non-tow records to FCS.
 *			Terminal is sent only with gate. 
 * 20071210	   DKA	Per NCS advice, the terminal indicator must be sent at

 *			the start of any message block.
 * 20071224	   DKA	Bug fix - ACKPENDING msgs in MQSTAB will be CONFIRMED
 *			without a resend after FCS returns from a long timeout.
 *			Fixed in the section tagged with '20071224'.
 * 20071231 1.20   DKA	CR07020 - CAAS requirements related to SQ's operations
 *			spanning T2 & T3 at Changi Airport. 
 *			- Do not send Belts and terminals for arrivals 2 hours
 *			before STOA.
 *			- Every minute, send all non-blank allocations for
 *			those flights with STOA exactly 2 hours (configurable)
 *			from now. "Timing" was originally thought to be a trigger
 *			from NTISCH, but instead thought just add a timing check
 *			in CheckBline() since we already check for timeouts
 *			there. Also easier maintenance.
 * 20080109 1.21   DKA	Fix in BeforeDateTimeOffset() to use TIFA instead of
 *			STOA for early arrivals. Delayed/On-time still use STOA.
 *			Introduce "IGNORE_TERMINAL" in fcshdl.cfg. This is the
 *			terminal (only one for first release) whose flights are
 *			ignored at the sending of the two-hour ahead message.
 *			This is because CGMS does not allocate budget term flts,
 *			but sends the blanked allocations to them...!
 * 20080205 1.22   DKA	Filter the 2-hour select with an additional clause
 *			to test for STEV. This is to allow flights that are 
 *			allocated but not being released. This corresponds to
 *			"Key code" in FIPS.Daily_Mask. Default value to test
 *			is 'X', but leave it as configurable. This is so that
 *			no need to update fcshdl.cfg.
 * 20080211		Add a check for tow records in "CheckArrKeycode" so that
 *			if master arrival has STEV set, do not send tow records.
 * 20081028 1.23   DKA	Actually *count* FCS incoming messages so that our replies
 *			to incoming flow control count requests reflect reality..
 *			This is 99% the cause of "missing" ATD/ATA problem.
 *			Reset the count upon the receipt of FC Req, or link
 *			going offline.
 * 20090114 1.24   DKA	Accept RTYP (Rotation Type) from ROGHDL for handling
 * 			join/split from ROGHDL (via EXCHDL - SQ interface).
 * 20090422 1.25   DKA	Rolled back v.1.24 method due to excessive updates.
 *			Use RAC message from ROGHDL for similiar functionality
 *			with hopefully lower number of resulting msgs to fcs.
 *			Incremented version no. for ease of maintenance & because
 *			of some modified v.1.24 code.
 *			Specification per CR09001.
 * 20090512		Discovered a bug from Jibbo's original code for split/joing:
 *			A split for a former joined arrival was sent if the departure
 *			was updated such that dep.URNO==dep.RKEY (i.e. dep split).
 *			This split for the arrival is only true if arrival is not
 *			now joined to another new dep.
 * 20090520		Workaround to suppress join/split ('028') msgs if they are
 *			repeated just before or one before.
 * 20100419 1.26 DKA    Bugfix - string buffer overflow when getting a large number
 *                      of RKEYS from ROGHDL in HandleRoghdlMsg ();
 * 20100419 1.27   DKA	New Auto_Tow tow insertion from FOGDHD handling.
 * 20100517 1.28   DKA  Non-Sched flights CR08005. Requires new cfg property
 *			NON_SCHED_TTYP which is a comma separated list identical
 *			to that one in IMPHDL.cfg.
 * 20100701 		Fix towing bug for non-sched in HandleTowing(). Due to the CEDA
 *			data being non-standard for a tow record, e.g. CSGN is "TOWING", &
 *			TTYP is either ' ' or 'S'.
 * 20110603 1.29   DKA	Changes for FCSU.
 *			- add date of joined flight when sending flight matches to FCSU.
 *			  To be configurable. Default is do not send. 
 *			  FCS Code to be defined once known.
 *			- count type 6 flow control from a different base. (SendMyAck)
 * 20110706 	   DKA	FCS code for date of linked flight given by NCS.
 * 20110708	   DKA  Forgot to add the link flight date into CheckJoinSplitRelease
 * 20110713 	   DKA	NCS request - for split, add thejoin flight date code plus a
 *			blank date.
 * 20111115 1.30   DKA	Pre-validate outgoing data messages in a similar manner as FCSU
 *			to prevent type 6 count mismatch which will lead to endless
 *			re-sending of outgoing blocks. The check is performed in
 *			blineDataWrite, using new function iValidateOutgoingMessage.
 * 20120611 1.30a  FYA  Additional logging to debug a reported issue.
 * 20130625 1.31   DKA	SINCAG-120/UFIS-3296. Fix ROWNUM use in SpoolToSend.
 *			SINCAG-110/UFIS-3014. Send blanked belts at offline-release.
 *
 */

/**********************************************************************************/
/*                                                                                */
/* be carefule with strftime or similar functions !!!                             */
/*                                                                                */
/**********************************************************************************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "db_if.h"
#include "bline-c.h"
#include "AATArray.h"

/* comment out header bline-c.h above and use following dummies if bline 
   lib not available: 

struct blineMessage
{
    int                 size;
    char                data [1000];
};

#define BlineDriver int
int blineRead (int igBlineHandle, struct blineMessage *igBlineMessage)
{
  return 0;
}
int blineCreate (char *xxx)
{
  return 0;
}
void blineDestroy (int igBlineHandle)
{
  return ;
}
int blinePeek (int igBlineHandle, struct blineMessage *igBlineMessage)
{
  return 0;
}
void blineWrite (int igBlineHandle, char* Data)
{
  dbg(TRACE,"blineWrite: to send (%s)",Data);
  return ;
}
*/

/******************************************************************************/
/* Macros */
/******************************************************************************/

#define		TERM_CODE	"039"
#define		TERM_CODE_LEN	3
#define		TERM_LEN	1
#define		PREAMBLE_LEN	17	/* e.g. "31USQ999...070131" */
#define		BLT1_CODE	"202"
#define		BLT1_CODE_LEN	3
#define		BLT1_LEN	2 
#define		GTA1_CODE	"043"
#define		GTA1_CODE_LEN	3
#define		GTA1_LEN	4
#define		PSTA_CODE	"047"
#define		PSTA_CODE_LEN	3
#define		PSTA_LEN	4 
#define		JOIN_FLT_DATE_CODE	"231"
#define		JOIN_FLT_DATE_CODE_LEN	3
#define		JOIN_FLT_DATE_LEN	6
#define		FLNO_BLANK	"        " /* small step away from hard-coding */
#define		FLNO_LEN	8

#define N_MAX_NON_SCHED_TTYP    12	/* CR08005 */

/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
int  debug_level = DEBUG;

/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static int   igInitOK      = FALSE;
static char  cgConfigFile[512];
static char  cgCedaSend[10] = "\0";
static char  cgCedaSendPrio[10] = "\0";
static char  cgCedaSendCommand[10] = "\0";
static char  cgMqrInterfaceName[10] = "fcs";
static char  cgHopo[8] = "\0";                         /* default home airport    */
static char  cgTabEnd[8] ="\0";                       /* default table extension */
static int   igWaitForFcsAck = TRUE; /* FCS must begin with ACK*/
static int   igWaitForMyAck = FALSE;
static int   igBlineLineNo = 0;
static int   igBlineLineNoCount = 12;
static char  pcgAckReqMsg[16];
static int   igSpooling=FALSE;
static time_t tgLastAckTime = 0;
static int   igBlineRetry=0;
static int   igBlineMaxRetry = 3;
static int   igBlineTimeOut  = 60;
static int   igLastInsTime   = 0;
static int   igNumPerTime    = 0;
static int   igNapTime = 50;
static int   igModId_Roghdl = 99999;
static int   igModId_Fogdhd = 99999;
static int   igModId_Fogshd = 99999;
static int   igWroteTypeSix = FALSE;
static int   igMinsBeforeStoa = 120;
static long  igStoaSendIntervalSecs = 60;
static char  cgIgnoreTerminal [2];
static char  cgIgnore_STEV [2];

/*
	The flwg two are STOA's in UTC. They are for v.1.20, and are 
	boundaries for selection from AFTTAB for arrivals that must
	be resent to FCS. 
*/

static time_t tgLastArrRelStoa = 0;
static time_t tgCurrArrRelStoa = 0;
static int    igFlagInit_ArrRelStoa = 0;

typedef struct {
  int Pos;
  int Len;
  char Flag[4];
} LenChecksRecs;
static LenChecksRecs LenChecks[10];
int igLenCheckLoops=0;
static int igFcsMsgCount = 0; 
static long  igMinsBeforeRacJoin = 360;
static long  igRacJoinIntervalSecs = 300; 
/*
	The flwg are STOA & STOD in UTC. For v.1.25.
	Boundaries for selection from AFTTAB for arr/dep's whose joing/splits
	are pre-sent to FCS. 
*/ 
static time_t tgLast_RAC_RKEY_STOA_D = 0;
static time_t tgCurr_RAC_RKEY_STOA_D = 0;

/*
	20090520 - workaround to prevent duplicate join/splits to FCS.
*/

static char pcgLastJoinSplit [128];
static char pcgLastButOneJoinSplit [128];

/* 	20100517 - CR08005 - Non Sched flights */
char cgNonSchedTtyp [N_MAX_NON_SCHED_TTYP];

/*	20110603 - Date of joined flight */
static int igAddJoinFlightDate = 0;

/*	20130625 - SINCAG-110/UFIS-3014 - add blanked belt */
static int igAddBlankedBeltOfArrival = 1;	/* note default is TRUE */
static int igToAddBlankBelt = 0;		/* this is set for each FLIGHT msg */

/******************************************************************************/
/* Function prototypes                                                          */
/******************************************************************************/
static int Init_fcshdl();
static int    Reset(void);                       /* Reset program          */
static void    Terminate(void);                   /* Terminate program      */
static void    HandleSignal(int);                 /* Handles signals        */
static void    HandleErr(int);                    /* Handles general errors */
static void    HandleQueErr(int);                 /* Handles queuing errors */
static int    HandleData(void);                  /* Handles event data     */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/

int blineDataWrite (char *pcpMsg);
static int HandleTowing (char *pclFld, char *pclData, int iDelFlag);
/* static void HandleNtischTrigger (char *ntisch_cmd); */
static void InsertTerminal (char *pclTerm, char *pclFcsMsg, int *iLastPos);
static int  BeforeDateTimeOffset (char *pclStoa, char *pclTifa, int iPeriod,
	char *pclType);
static void SetArrRelStoa (void);
static void CheckArrRelease (void);
static int  CheckArrKeycode (char *pclAurn, char *keycode);
static void HandleRoghdlMsg (char *pclCmd, char *pclSel, char *pclFld,
	char *pclData); 
static int  GetFlightFieldsByUrno (char *pclWhere,
	char *pclFlno, char *pclStoa, char *pclStod, char *pclAdid,
        char *pclCsgn, char *pclTtyp);
static void Handle_RAC_RKEY (long rkey, int iCheckTime);
static void CheckJoinSplitRelease (void);
static int  GetMatch (char *pclInputUrno, char *pclInputAdid,
		char *pclOutputUrno);
static int IsRepeatedMatch (char *next_msg);
static void HandleFogdhdMsg (char *pclCmd, char *pclSel, char *pclFld,
	char *pclData); 
static int isNonSchedTtyp (char clTtyp);
static int iValidateOutgoingMessage (char *pcpMsg);
static void CheckArrBlankedBelt (char *pcpFld, char *pcpData);
/******************************************************************************/
/* External functions                                                         */
/******************************************************************************/
extern int get_real_item (char *, char *, int);
extern int get_item_no (char *, char *, int);
/******************************************************************************/
/* Internal variables                                                         */
/******************************************************************************/
#define DATABLK_SIZE (1024)

static char pcgDataArea[DATABLK_SIZE];
static char pcgSqlBuf[DATABLK_SIZE];

int  igDbgSave = DEBUG;
int  igSendToModid = 9560;
int  igSendPrio = 3;

BlineDriver     igBlineHandle;
struct blineMessage   igBlineMessage;

static int TimeToStr(char *pcpTime,time_t lpTime)
{
    struct tm *_tm;
    char   _tmpc[6];

    /*_tm = (struct tm *)localtime(&lpTime); */
    _tm = (struct tm *)gmtime(&lpTime);
    /*      strftime(pcpTime,15,"%" "Y%" "m%" "d%" "H%" "M%" "S",_tm); */


    sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
            _tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
            _tm->tm_min,_tm->tm_sec);

    return RC_SUCCESS;

}     /* end of TimeToStr */


void SendMyAck()
{
      /*
	ver.1.29 - FCSU actually counts Type 6's, and basis is 'space' for zero.
		Code below used to be 'A', changed to '!'. So if igBlineLineNo is
		zero, we send a space (ASCII one less than exclamation mark.
      */
      sprintf(pcgAckReqMsg,"6%c",(char) ('!'+igBlineLineNo-1));
      pcgAckReqMsg[2]= 0;
      dbg (TRACE, "SendMyAck: sending Ack request <%s>",pcgAckReqMsg);
      blineWrite (igBlineHandle, pcgAckReqMsg);
      igWaitForMyAck=TRUE;
      igWroteTypeSix=TRUE;
      tgLastAckTime = time(NULL);
}

static int SendToCeda(char *pcpBuffer,char *pcpMyTime,long plpMyReason, char *pcpUrno) 
{
    int ilRC = RC_SUCCESS;
    short slFkt = 0;
    short slCursor = 0;
    char clData[100] = "\0";
    char clUrno[12] = "\0";
    char clRecUrno[12] = "\0";
    int ilCount = 0;
    int ilAckRC = RC_FAIL;
    char clNowTime[20] = "\0";
    char clSqlBuf[4500] ="\0";
    char clTwStart[30] = "\0";
    char clTwEnd[30] = "\0";
    int i = 0;
    BC_HEAD *prlBchead       = NULL;
    CMDBLK  *prlCmdblk       = NULL;
    char    *pclSelection    = NULL;
    char    *pclFields       = NULL;
    char    *pclData         = NULL;
    char    *pclRow          = NULL;

    strcpy(clUrno,pcpUrno);
    TimeToStr(clNowTime,time(NULL));


    dbg(TRACE,"%05d: -----SendToCeda start -----",__LINE__);
    dbg(DEBUG,"%05d: pcpBuffer: <%s>, pcpMyTime: <%s>, plpMyReason: <%d>, pcpUrno: <%s>",__LINE__,pcpBuffer,pcpMyTime,plpMyReason,pcpUrno);


/* NOW SEND EVENT TO CEDA */

	sprintf(clTwStart,"%s,%s",cgMqrInterfaceName,clUrno);
	sprintf(clTwEnd,"%s,%s,%s",cgHopo,cgTabEnd,mod_name);
	dbg(TRACE,"Now Send Event to <%d>",atoi(cgCedaSend));
/*
	if (strlen(cgSelection) > 0)
	{
		ilRC = SendCedaEvent(atoi(cgCedaSend),0,mod_name,"TOOL",clTwStart,clTwEnd,cgCedaSendCommand,"",cgSelection,"",pcpBuffer,"",atoi(cgCedaSendPrio),RC_SUCCESS);
	}
	else
	{
*/
		ilRC = SendCedaEvent(atoi(cgCedaSend),0,mod_name,"TOOL",clTwStart,clTwEnd,cgCedaSendCommand,"","","",clUrno,"",atoi(cgCedaSendPrio),RC_SUCCESS);
/*
	}
	if (ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"%05d: command <%s> could not be sent to <%s>, MQRTAB-Urno: <%s>",__LINE__,cgCedaSendCommand,cgCedaSend,clUrno);

	}
	
	if(strcmp(cgDbLogging,"ON") == 0)
	{
		if(strlen(cgSelection) > 0)
		{
			sprintf(clSqlBuf,"UPDATE MQRTAB SET STAT='OK', LSTU='%s', COUN = 0 WHERE URNO=%d",clNowTime,atol(clUrno));
		}
		else
		{
			sprintf(clSqlBuf,"UPDATE MQRTAB SET STAT='IN WORK', LSTU='%s', COUN = 0 WHERE URNO=%d",clNowTime,atol(clUrno));
		}
	}


*/
    dbg(TRACE,"%05d: -----SendToCeda end -----",__LINE__);

    return ilRC;
}


void HandleBlineData(char *pcpMsg)
{
    int ilRC = RC_SUCCESS;
    char clNowTime[20] = "\0";
    char clUrno[16] = "\0";
    char clTPutTime[20] = "\0";
    int igCurrInsTime;
    short slFkt = 0;
    short slCursor = 0;
    char clSqlBuf[4500] ="\0";
    char clData[100] = "\0";
    int ilLoop = 0;

    /* check length of message according to type of message, if configured */
    for (ilLoop= 0 ; ilLoop<igLenCheckLoops; ilLoop++)
    {
       if (pcpMsg[LenChecks[ilLoop].Pos-1]==LenChecks[ilLoop].Flag[0])
       {  /* type of message configured: check len */
          if(strlen(pcpMsg)!=LenChecks[ilLoop].Len)
          {
             dbg(TRACE,"HandleBlineData: !! FCS data rejected: Data with '%c' at pos <%d> should have length <%d>! ",
                LenChecks[ilLoop].Flag[0],LenChecks[ilLoop].Pos,LenChecks[ilLoop].Len);
             return;
          }
          break;
       }
    }
/* INSERT MESSAGE INTO MQRTAB */

      ilRC = GetNextValues(clUrno,1);
      igCurrInsTime   = time(NULL);
      if (igLastInsTime != igCurrInsTime)
         igNumPerTime = 0;
      else
         igNumPerTime++;
      igLastInsTime= igCurrInsTime;
      TimeToStr(clNowTime,igLastInsTime);
      sprintf(clTPutTime,"%s%02d",clNowTime,igNumPerTime);

      ConvertClientStringToDb(pcpMsg);
    
      sprintf(clSqlBuf,"INSERT INTO MQRTAB (URNO,INAM,TPUT,DATA,COUN,STAT,REAS,LSTU) VALUES (%d,'%s','%s','%s',%d,'%s',%d,'%s')",
          atol(clUrno),cgMqrInterfaceName,clTPutTime,pcpMsg,1,"RECEIVED",0,clNowTime);
      dbg(DEBUG,"HandleBlineData: clSqlBuf: <%s>",clSqlBuf);
      slFkt = START;
      slCursor = 0;
      ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clData);
      if (ilRC != RC_SUCCESS)
      {
        dbg(TRACE,"HandleBlineData: sql_if failed ###################");
      }
      commit_work();
      close_my_cursor (&slCursor);

      /* Should be safe now to bump the counter... */
      igFcsMsgCount++;
        
      ConvertDbStringToClient(pcpMsg);

/* SEND DATA TO CEDA */
      if(atoi(cgCedaSend)!=0)
      {
         ilRC = SendToCeda(pcpMsg,clNowTime,0,clUrno);
      }

}

#define TYPE_POS 0
#define ADID_POS TYPE_POS+1
#define UCOD_POS ADID_POS+1
#define FLNO_POS UCOD_POS+1
#define DATE_POS FLNO_POS+8
#define DATE_LEN 6

#define RC_DATA_ERR 2222

int GetFcsFlno(char *pcpFields, char *pcpData, char *pcpFcsFlno,
               char *pcpFlno, char *pcpStox, char *pcpAdid)
{
  int ilRC = RC_DATA_ERR;
  short slFkt = 0;
  short slCursor = 0;
  char clSqlBuf[4500] ="\0";
  int ilItemNo = 0 ;

  if (pcpFcsFlno[0]==0)
  {
    ilItemNo = get_item_no (pcpFields, "FLNO", 5) + 1;
    if (ilItemNo == 0)
    {
      return ilRC;
    }
    (void) get_real_item (pcpFlno, pcpData, ilItemNo);
    ilItemNo = get_item_no (pcpFields, "ADID", 5) + 1;
    if (ilItemNo == 0)
    {
      return ilRC;
    }
    (void) get_real_item (pcpAdid, pcpData, ilItemNo);
    if  (pcpAdid[0]=='A')
    {
      ilItemNo = get_item_no (pcpFields, "STOA", 5) + 1;
    }
    else
    {
      ilItemNo = get_item_no (pcpFields, "STOD", 5) + 1;
    }
    if (ilItemNo == 0)
    {
      return ilRC;
    }
    /* when we are here, ilRC will get RC_SUCCESS, RC_NOTFOUND or RC_FAIL */
    (void) get_real_item (pcpStox, pcpData, ilItemNo);
    /* sprintf(clSqlBuf,"SELECT FCSF FROM FCSTAB WHERE FLNO ='%s' AND STOX='%s' AND ADID='%s'",pcpFlno,pcpStox,pcpAdid);*/
    /* the spelling of FCS flight number should not be changing from day to day, arr/dep so don't use ADID for search */
    /* use date part of STOx without YY */
    sprintf(clSqlBuf,"SELECT FCSF FROM FCSTAB WHERE UFLN='%-8s%6.6s'",pcpFlno,&pcpStox[2]);
  }
  else
  {
    sprintf(clSqlBuf,"SELECT FCSF FROM FCSTAB WHERE UFLN='%s'",pcpFcsFlno);
  }

  dbg(TRACE,"GetFcsFlno: clSqlBuf: <%s>",clSqlBuf);
  slFkt = START;
  slCursor = 0;
  ilRC = sql_if (slFkt, &slCursor, clSqlBuf, pcpFcsFlno);
  if (ilRC == RC_SUCCESS)
  {
    dbg(TRACE,"GetFcsFlno: %s/%s at %s has FCS Flno: %s",pcpFlno,pcpAdid,pcpStox,pcpFcsFlno);
  }
  else if (ilRC == RC_NOTFOUND)
  {
    dbg(TRACE,"GetFcsFlno: %s/%s at %s has no FCS Flno...",pcpFlno,pcpAdid,pcpStox);
  }
  else 
  {
    dbg(TRACE,"GetFcsFlno: SELECT from FCSTAB failed <%d> ##########################",ilRC);
  }
  commit_work();
  close_my_cursor (&slCursor);
  return ilRC; /* is one of RC_SUCCESS, RC_NOTFOUND or RC_FAIL */
}

/* moved insert int FCSTAB into IMPHDL because on joining of connection
   flight FLIGHT send both arrival and departure with same "fcs,<URNO-of-MQRTAB>"
   which leads to LH 028 (arrival) mapped to LH28 (departure)

void InsertUfis2FcsFlno(char *pcpTw_start, char *pcpFields, char *pcpData)
{
    int ilRC = RC_SUCCESS;
    short slFkt = 0;
    short slCursor = 0;
    char clSqlBuf[4500] ="\0";
    char pclFcsFlno[DATABLK_SIZE] = "\0";
    char pclFlno[16] = "\0";
    char pclStox[16] = "\0";
    char pclAdid[16] = "\0";
    char *pclMqrUrno = NULL;

    memset(pclFcsFlno,0,DATABLK_SIZE);
    ilRC= GetFcsFlno(pcpFields, pcpData, pclFcsFlno,pclFlno,pclStox,pclAdid);
    if (ilRC==RC_NOTFOUND)
    {
      pclMqrUrno=strstr(pcpTw_start,"fcs,") + 4;

      sprintf(clSqlBuf,"INSERT INTO FCSTAB (FCSF,UFLN) VALUES "
                       "((SELECT SUBSTR(DATA,4,8) FROM MQRTAB WHERE URNO =%s),'%-8s%6.6s') ",pclMqrUrno,pclFlno, &pclStox[2]);

      dbg(TRACE,"InsertUfis2FcsFlno: clSqlBuf: <%s>",clSqlBuf);
      slFkt = START;
      slCursor = 0;
      ilRC = sql_if (slFkt, &slCursor, clSqlBuf, pclFcsFlno);
      if (ilRC != RC_SUCCESS)
      {
        dbg(TRACE,"InsertUfis2FcsFlno: INSERT into FCSTAB failed!");
        commit_work();
        close_my_cursor (&slCursor);
        return;
      }
      commit_work();
      close_my_cursor (&slCursor);
    }
    else if (ilRC==RC_SUCCESS)
    {
        dbg(TRACE,"InsertUfis2FcsFlno: no need to insert!");
    }
    else 
    {
        dbg(TRACE,"InsertUfis2FcsFlno: don't know what to do!");
    }

}
*/

/******************************************************************************/
/* The SpoolTooSend routine spools all messages comming in while waiting for  */
/* an FCS Ack respond                                                         */
/* v.1.31 ROWNUM use corrected.						      */
/******************************************************************************/
void SpoolToSend(char *pclStatBefore, char *pclStatAfter)
{
    int ilRC = RC_SUCCESS;
    char clNowTime[20] = "\0";
    short slFkt = 0;
    short slCursor = 0;
    char clSqlBuf[4500] ="\0";
    int ilLoop = 0;
  int ilNoOfRec = 0;
  char pclUrno[16] = "\0";
  char pclUrnoList[240] = "\0";
  char *pclData;
  int ilGetRc = DB_SUCCESS;

      igSpooling=TRUE; /* do not save spooled data again in MQSTAB */
      sprintf (pcgSqlBuf,
        "SELECT * FROM (SELECT URNO,DATA FROM MQSTAB WHERE STAT='%s' AND INAM='%s' ORDER BY TPUT) "
        "WHERE ROWNUM <= %d", 
         pclStatBefore, cgMqrInterfaceName, 13);
      dbg (TRACE, "SpoolToSend: %s", pcgSqlBuf);
      ilNoOfRec = 0;
      slFkt = START;
      slCursor = 0;
      ilGetRc = DB_SUCCESS;
      /* now respooling at least up to next igWaitForMyAck */
      while ((ilGetRc == DB_SUCCESS) &&  (igWaitForMyAck==FALSE))
      {
        ilGetRc = sql_if (slFkt, &slCursor, pcgSqlBuf, pcgDataArea);
        if (ilGetRc == DB_SUCCESS)
        {
          ilNoOfRec++;
          BuildItemBuffer (pcgDataArea, "URNO,DATA ", 2, ",");
          dbg (TRACE, "SpoolToSend: %d.: DATA <%s>", ilNoOfRec, pcgDataArea);
          (void) get_real_item (pclUrno, pcgDataArea, 1);
          pclData= strchr(pcgDataArea,','); /* don't use get_real_item here, it trims tailing spaces */
          if (pclData!=NULL)
          {
            pclData++;
            (void) blineDataWrite (pclData);
            strcat(pclUrnoList,pclUrno);
            strcat(pclUrnoList,",");
          }
        }                       /* end if */
        else
        {
          if (ilGetRc == RC_FAIL)
          {
            dbg (TRACE, "SpoolToSend: ORACLE ERROR !!");
            ilRC = RC_FAIL;
          }                     /* end if */
        }                       /* end else */
        slFkt = NEXT;
      }                         /* while */
      close_my_cursor (&slCursor);
      if ((ilNoOfRec>0) && (pclStatAfter[0]!=0))
      {
        if (pclUrnoList[0]!=0) 
           pclUrnoList[strlen(pclUrnoList)-1]=0; /* remove trailing ',' */
        TimeToStr(clNowTime,time(NULL));
        sprintf (pcgSqlBuf, "UPDATE MQSTAB SET STAT='%s',LSTU='%s' WHERE URNO IN (%s)", pclStatAfter,clNowTime,pclUrnoList);
        dbg (TRACE, "SpoolToSend: %s", pcgSqlBuf);
        slFkt = START;
        slCursor = 0;
        ilGetRc = DB_SUCCESS;
        ilGetRc = sql_if (slFkt, &slCursor, pcgSqlBuf, pcgDataArea);
        if (ilGetRc == DB_SUCCESS)
        {
          dbg (TRACE, "SpoolToSend: Next %d records updated in MQSTAB", ilNoOfRec);
        }                       /* end if */
        else
        {
          if (ilGetRc == RC_FAIL)
          {
            dbg (TRACE, "SpoolToSend: ORACLE ERROR !!");
            ilRC = RC_FAIL;
          }                     /* end if */
          else
          {
            igSpooling=FALSE;   /* no data found, so continue normal operation */
          }                     /* end if */
        }                     /* end if */
        close_my_cursor (&slCursor);
      }
            igSpooling=FALSE;   /* no data found, so continue normal operation ???? */
}

/******************************************************************************/
/* The MqsTabAckCompleted routine clears the status of all messages sent      */
/* before last Ack                                                            */
/******************************************************************************/
void  MqsTabAckCompleted()
{
    int ilRC = RC_SUCCESS;
    char clNowTime[20] = "\0";
    short slFkt = 0;
    short slCursor = 0;
    char clSqlBuf[4500] ="\0";
    char clData[100] = "\0";
    int ilLoop = 0;

      TimeToStr(clNowTime,time(NULL));
      sprintf(clSqlBuf,"UPDATE MQSTAB SET STAT='CONFIRMED',LSTU='%s' WHERE STAT='ACKPENDING' AND INAM='%s' ",
          clNowTime,cgMqrInterfaceName);
      dbg(TRACE,"MqsTabAckCompleted: clSqlBuf: <%s>",clSqlBuf);
      slFkt = START;
      slCursor = 0;
      ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clData);
      if (ilRC == RC_FAIL)
      {
        dbg (TRACE, "MqsTabAckCompleted: ORACLE ERROR !!");
      }                     /* end if */
      commit_work();
      close_my_cursor (&slCursor);
      igBlineLineNo = 0;
      
      dbg(TRACE,"line<%d>SpoolToSend(\"TO-SEND\",\"ACKPENDING\");",__LINE__);
      SpoolToSend("TO-SEND","ACKPENDING");
}

/******************************************************************************/
/* The HandleBlineAck routine checks if own Ack is expected and reacts        */
/******************************************************************************/
void HandleBlineAck(char *pcpMsg)
{
  
  if (igWaitForMyAck==TRUE)
  {
    if (strcmp(pcpMsg,pcgAckReqMsg)==0)
    {
      dbg(TRACE,"HandleBlineAck: received Ack i am waiting for: <%s> ", pcgAckReqMsg);
      igWaitForMyAck= FALSE;
      igBlineRetry= 0;
      MqsTabAckCompleted();
    }
    else
    {
      dbg(TRACE,"HandleBlineAck: received Ack i dit not wait for: <%s> != <%s>", pcpMsg,pcgAckReqMsg);
      /*dbg(TRACE,"HandleBlineAck: under construction: resend ACK with data");*/
      /* igWaitForMyAck= FALSE; ? continue ?*/
    }
  }
  else
  {
    dbg(TRACE,"HandleBlineAck: unexpected received Ack: <%s> ", pcpMsg);
    /*dbg(TRACE,"HandleBlineAck: under construction: resend ACK with data");*/
    /* igWaitForMyAck= FALSE; ? continue ?*/
  }
}

/******************************************************************************/
/* The CheckBline routine checks for data from BLINE driver and handles them  */
/******************************************************************************/
void CheckBline()
{
    int     ilRC = 0;
    int     ilLoop = 0;
    char    cFcsMsgCount [4];

/*    dbg(TRACE,"BLINE: checking...");*/

    /*	Workaround for FCS problem - they send ACK for our type 6 msg even
     *	though they have not processed the msgs. So we pause before sending.
     *	However, the pause should be in the sending to FCS code, not during
     *	reading..
     */

    /* nap(igNapTime); */

    /*	Seems like a nap is needed to reduce CPU time.  */
    nap(50);

    /* v.1.20 - check what arrivals need allocations to be manually sent */
    CheckArrRelease ();

    /* v.1.25 - force check what flights to force send joins/splits manually */
    CheckJoinSplitRelease ();

    ilRC = blinePeek (igBlineHandle, &igBlineMessage);

    if (ilRC == -1)
    {
        dbg(TRACE,"CheckBline: BLINE error <%s>!",blineError ());
	Reset ();
    }
    else if (!ilRC)
    {
/*        dbg(TRACE,"BLINE: No message available..."); */
    }
    else
    {
        ilRC = blineRead (igBlineHandle, &igBlineMessage);
        if (ilRC == -1)
        {
           dbg(TRACE,"CheckBline: BLINE error <%s>!",blineError ());
           Reset ();
        }
        else
        {
           dbg(TRACE,"CheckBline: Message (%d): [%s]\n", igBlineMessage.size, igBlineMessage.data);
           if (igBlineMessage.data[0]=='3')
           {
             if (igWaitForFcsAck==FALSE)
             {
               HandleBlineData(igBlineMessage.data);
             }
             else
             {
               dbg(TRACE,"CheckBline: FCS had not send introducing ACK, ignoring data...");
             }
           }
           else if (igBlineMessage.data[0]=='6')
           {
             if (igWaitForFcsAck==FALSE)
             {
                HandleBlineAck(igBlineMessage.data);
             }
             else
             {
               dbg(TRACE,"CheckBline: FCS had not send introducing ACK, ignoring old ack from previous run ...");
             }
           }
           else if (igBlineMessage.data[0]=='1')
           {
             if (igBlineMessage.data[2]==0)
             {
               dbg(TRACE,"CheckBline: remote Ack request is for <%d> messages",(int) igBlineMessage.data[1] - 0x20);

	       /*
		* At program start, we could be in the middle of a stream
		* of FCS data. If so, do not reply to FC Req's, 
		* otherwise we will discard the msgs, and FCS will think we
		* are processing them!
		*/

	       if (igWaitForFcsAck == FALSE)
	       {
		  dbg(TRACE,
		    "CheckBline: Reply Ack req with our count [%d]",
		    igFcsMsgCount);
		  /* blineWrite (igBlineHandle, igBlineMessage.data); */
		  cFcsMsgCount [0] = '1';
		  cFcsMsgCount [1] = (char) (0x20 + igFcsMsgCount);
		  cFcsMsgCount [2] = '\0';
		  blineWrite (igBlineHandle, cFcsMsgCount); 
		  /*
			Reset global fcs msg count. If we receive the same ack request
			from FCS before a Type 3 message, it could mean that we missed
			16 msgs, so it cannot be correct for us to resend the last 
			igFcsMsgCount. By right, if FCS misses our reply to their
			ack req, they should first resend the type 3's then re-send
			the same ack req.  
		  */
		  igFcsMsgCount = 0;
		  igBlineRetry= 0;
	       }
	       else
		  dbg(TRACE,
		    "CheckBline: ignore remote Ack request - "
		    "awaiting initial handshake"); 

               if (igBlineMessage.data[1]==' ')
               { 
                    /* 20071224 - Hack - bug discovered somehow introduced with
	            with igWroteTypeSix. If already max retries are reached, then
	            must try to resend the "ACKPENDING". The igWroteTypeSix fixed
	            problem of resending such msgs while online, but side effect is
	            that when FCS returns after a long offline period, such msgs
	            are simply 'CONFIRMED' in MQSTAB and never resent.
	            Got to hack in flwg block so that other code is untouched.
	            It appears that igBlineLineNo must be reset before
	            SpoolToSend("ACKPENDING") is called, but not before
	            SpoolToSend("TO-SEND").  
	         */
	         if (igWaitForFcsAck == TRUE)   /* We are OFFLINE */
	         {
	           dbg(TRACE,"CheckBline: Resend ACKPENDING from MQSTAB");
                   igWaitForFcsAck= FALSE;      /* Otherwise blineDataWrite will */
                                                /* not write.. */ 
	           igBlineLineNo = 0;
	           
	           dbg(TRACE,"line<%d>calling SpoolToSend(\"ACKPENDING\",\"\");",__LINE__);
                   SpoolToSend("ACKPENDING",""); 
	         } 

                 igWaitForFcsAck= FALSE;
                 dbg(TRACE,"CheckBline: initial remote Ack received, going online"); 

		 /*
			v.1.11 - reset the counter otherwise we will keep
			going offline even at the initial "1 ".  
		 */
                 igBlineRetry = 0;
                 dbg(TRACE,"CheckBline: Reset MAX RETRY count to zero");

                 /* if (igWaitForMyAck!=TRUE) */
                 if ((igWaitForMyAck!=TRUE) && (igWroteTypeSix == TRUE))
                 { 
                   igBlineLineNo = 0;
                   
                   dbg(TRACE,"line<%d>calling SpoolToSend(\"ACKPENDING\",\"\");",__LINE__);
                   SpoolToSend("ACKPENDING","");
                   /* if (igWaitForMyAck!=TRUE) */
                   if ((igWaitForMyAck!=TRUE) && (igWroteTypeSix == TRUE))
                   { 
                   	dbg(TRACE,"line<%d>calling SpoolToSend(\"TO-SEND\",\"ACKPENDING\");",__LINE__);
                     SpoolToSend("TO-SEND","ACKPENDING");
                   }
                 }
               }
             }
             else
             {
               dbg(TRACE,"CheckBline: seems to be a block start/end message, ignoring.... ");
             }
           }
        }
        if (igWaitForFcsAck==FALSE)
           dbg(TRACE,"==================== START/END ==== ONLINE  =============");
        else
           dbg(TRACE,"==================== START/END ==== OFFLINE =============");
    }
    if (time(NULL) - tgLastAckTime > igBlineTimeOut)
    {
      dbg (TRACE, "CheckBline: ACK timeout <%d> reached, retry/init", igBlineTimeOut);
      if ((igBlineRetry>0) || igWaitForMyAck==TRUE)
      {
        dbg (TRACE, "CheckBline: respool up to last ACK", igBlineTimeOut);
        igBlineLineNo = 0;
        igWaitForMyAck= FALSE;
        
        dbg(TRACE,"line<%d>calling SpoolToSend(\"ACKPENDING\",\"\");",__LINE__);
        SpoolToSend("ACKPENDING","");
      }
      igBlineRetry++;
      if (igBlineRetry > igBlineMaxRetry)
      { /* avoid filling up memory of blindrv ( == blinedriver )  */
        if (igWaitForFcsAck==TRUE)
        {
          dbg (TRACE, "CheckBline: MAX RETRY <%d> reached, stay offline, wait for INIT ACK by FCS ", igBlineMaxRetry);
        }
        else
        {
          dbg (TRACE, "CheckBline: MAX RETRY <%d> reached, go offline, wait for INIT ACK by FCS ", igBlineMaxRetry);
          igWaitForFcsAck= TRUE; 
	  /*
		v.1.23 - reset global fcs msg count. This allows a clean restart when FCS returns,
		and also will ensure FCS resends last messages before we went offline.
	  */
	  igFcsMsgCount = 0;
        }
      }

      /*
	v.1.11 - do not send type 6 FCREQ to FCS until we receive the first
	"1 " from them. If we are waiting for the this initial "1 " we just
	update the time of last FCREQ so that we do not enter this block
	unnecessarily.  
      */

      if (igWaitForFcsAck == FALSE)
        SendMyAck();
      else
	tgLastAckTime = time(NULL);

      if (igWaitForFcsAck==FALSE)
         dbg(TRACE,"==================== START/END ==== ONLINE  =============");
      else
         dbg(TRACE,"==================== START/END ==== OFFLINE =============");
    }
}

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
    int    ilRC = RC_SUCCESS;            /* Return code            */
    int    ilCnt = 0;
    
    INITIALIZE;            /* General initialization    */

    dbg(TRACE,"MAIN: version <%s>",mks_version);
    /* handles signal		*/
    (void)SetSignals(HandleSignal);
    (void)UnsetSignals();

    /* Attach to the MIKE queues */
    do
    {
        ilRC = init_que();
        if(ilRC != RC_SUCCESS)
        {
            dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        }/* end of if */
    }while((ilCnt < 10) && (ilRC != RC_SUCCESS));
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
        sleep(60);
        exit(1);
    }else{
        dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
    }/* end of if */

    sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
    ilRC = TransferFile(cgConfigFile);
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */
    sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
    /* uncomment if necessary */
    /* ilRC = TransferFile(cgConfigFile); */
    /* if(ilRC != RC_SUCCESS) */
    /* { */
    /*     dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile); */
    /* } */ /* end of if */
    ilRC = SendRemoteShutdown(mod_id);
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    } /* end of if */
    if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
        dbg(DEBUG,"MAIN: waiting for status switch ...");
        HandleQueues();
    }/* end of if */
    if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
        dbg(TRACE,"MAIN: initializing ...");
        if(igInitOK == FALSE)
        {
            ilRC = Init_fcshdl();
            if(ilRC != RC_SUCCESS)
            {
                dbg(TRACE,"MAIN: init failed!");
            } /* end of if */
        }/* end of if */
    } else {
        Terminate();
    }/* end of if */
    dbg(TRACE,"MAIN: initializing OK");
    dbg(TRACE,"==================== START/END =================");
    for(;;)
    {
        ilRC = que(QUE_GETBIGNW,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text;
                
        if( ilRC == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRC != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRC);
            } /* fi */
            
            switch( prgEvent->command )
            {
            case    HSB_STANDBY    :
                ctrl_sta = prgEvent->command;
                HandleQueues();
                break;    
            case    HSB_COMING_UP    :
                ctrl_sta = prgEvent->command;
                HandleQueues();
                break;    
            case    HSB_ACTIVE    :
                ctrl_sta = prgEvent->command;
                break;    
            case    HSB_ACT_TO_SBY    :
                ctrl_sta = prgEvent->command;
                /* CloseConnection(); */
                HandleQueues();
                break;    
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate();
                break;    
            case    HSB_STANDALONE    :
                ctrl_sta = prgEvent->command;

                break;    

            case    SHUTDOWN    :
                /* process shutdown - maybe from uutil */
                Terminate();
                break;
                    
            case    RESET        :
                ilRC = Reset();
                break;
                    
            case    EVENT_DATA    :
                if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
                {
                    ilRC = HandleData();
                    if(ilRC != RC_SUCCESS)
                    {
                        HandleErr(ilRC);
                    }/* end of if */
                }else{
                    dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
                    DebugPrintItem(TRACE,prgItem);
                    DebugPrintEvent(TRACE,prgEvent);
                }/* end of if */
                break;
                    
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;
            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;
            default            :
                dbg(TRACE,"MAIN: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
            if (igWaitForFcsAck==FALSE)
               dbg(TRACE,"==================== START/END ==== ONLINE  =============");
            else
               dbg(TRACE,"==================== START/END ==== OFFLINE =============");
        }
        CheckBline();
        
    } /* end for */
    
    exit(0);
    
} /* end of MAIN */

static int ReadConfigEntry(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer, char *pcpDefault)
{
    int ilRC = RC_SUCCESS;

    char clSection[124] = "\0";
    char clKeyword[124] = "\0";

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRC = iGetConfigEntry(cgConfigFile,clSection,clKeyword,
			   CFG_STRING,pcpCfgBuffer);
    if(ilRC != RC_SUCCESS)
    {
	dbg(TRACE,"ReadConfigEntry: Not found in %s: [%s] <%s>",cgConfigFile,clSection,clKeyword);
	dbg(TRACE,"ReadConfigEntry: use default-value: <%s>",pcpDefault);
	strcpy(pcpCfgBuffer,pcpDefault);
    } 
    else
    {
	dbg(DEBUG,"ReadConfigEntry: Config Entry [%s],<%s>:<%s> found in %s",
        clSection, clKeyword ,pcpCfgBuffer, cgConfigFile);
    }/* end of if */
    return ilRC;
}


/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int Init_fcshdl()
{
 int    ilRC = RC_SUCCESS;            /* Return code */
 char     pclDbgLevel[iMIN_BUF_SIZE];
 char     pclDummy2[iMIN_BUF_SIZE];
 int ilCnt = 0 ;
 int ilItem = 0 ;
 int ilLoop = 0 ;
 int jj, kk;

  /* now reading from configfile or from database */
  SetSignals(HandleSignal);

  do
  {
    ilRC = init_db();
    if (ilRC != RC_SUCCESS)
    {
      check_ret(ilRC);
      dbg(TRACE,"Init_fcshdl: init_db() failed! waiting 6 sec ...");
      sleep(6);
      ilCnt++;
    } /* end of if */
  } while((ilCnt < 10) && (ilRC != RC_SUCCESS));

  if(ilRC != RC_SUCCESS)
  {
    dbg(TRACE,"Init_fcshdl: init_db() failed! waiting 60 sec ...");
    sleep(60);
    exit(2);
  }else{
    dbg(TRACE,"Init_fcshdl: init_db() OK!");
  } /* end of if */

  (void) ReadConfigEntry("MAIN","debug",pclDbgLevel,"TRACE");
  if (strcmp(pclDbgLevel,"DEBUG") == 0)
  {
    dbg(DEBUG,"Init_fcshdl: DEBUG_LEVEL IS <DEBUG>");
    igDbgSave = DEBUG;
  } 
  else if (strcmp(pclDbgLevel,"OFF") == 0) 
  {
    dbg(DEBUG,"Init_fcshdl: DEBUG_LEVEL IS <OFF>");
    igDbgSave = 0;
  } 
  else 
  {
    dbg(DEBUG,"Init_fcshdl: DEBUG_LEVEL IS <TRACE>");
    igDbgSave = TRACE;
  }

  (void) ReadConfigEntry("IGNORE_MODULES","ROGHDL", pclDbgLevel, "99999");
  igModId_Roghdl = atoi (pclDbgLevel); 
  (void) ReadConfigEntry("IGNORE_MODULES","FOGDHD", pclDbgLevel, "99999");
  igModId_Fogdhd = atoi (pclDbgLevel); 
  (void) ReadConfigEntry("IGNORE_MODULES","FOGSHD", pclDbgLevel, "99999");
  igModId_Fogshd = atoi (pclDbgLevel);

  dbg (TRACE, "Ignore CEDA msgs from [%d] [%d] [%d]",
    igModId_Roghdl, igModId_Fogdhd, igModId_Fogshd);

  (void) ReadConfigEntry("QUEUE","CEDA_SEND",cgCedaSend,"");
  (void) ReadConfigEntry("QUEUE","CEDA_SEND_PRIO",cgCedaSendPrio,"5");
  (void) ReadConfigEntry("QUEUE","CEDA_SEND_COMMAND",cgCedaSendCommand,"");
  (void) ReadConfigEntry("QUEUE","IFNAME",cgMqrInterfaceName,"fcs");

  (void) ReadConfigEntry("TIMEOUT","NUMBER_OUTGOING_MSGS",pclDbgLevel,"12");
  igBlineLineNoCount = atoi(pclDbgLevel); 
  dbg(TRACE, "NUMBER_OUTGOING_MSGS: [%d]", igBlineLineNoCount);

  (void) ReadConfigEntry("TIMEOUT","MAX_RETRY",pclDbgLevel,"3");
  igBlineMaxRetry = atoi(pclDbgLevel); 
  dbg(TRACE, "MAX_RETRY : [%d]", igBlineMaxRetry);
  (void) ReadConfigEntry("TIMEOUT","SECONDS",pclDbgLevel,"60");
  igBlineTimeOut  = atoi(pclDbgLevel);
  dbg(TRACE, "SECONDS for BlineTimeout : [%d]", igBlineTimeOut); 
  /* (void) ReadConfigEntry("TIMEOUT","PAUSING",pclDbgLevel,"50"); */
  (void) ReadConfigEntry("TIMEOUT","PAUSING",pclDbgLevel,"1000");
  igNapTime = atoi(pclDbgLevel); 

  igNapTime = igNapTime / 1000;	/* Added by DKA - from ms to seconds */
  dbg(TRACE,
    "Init_fcshdl: PAUSING converted to: <%d> seconds", igNapTime);

  /* if (igNapTime<50) */
  if (igNapTime<0)
  {
    /* igNapTime= 50;
    dbg(TRACE,"Init_fcshdl: PAUSING to small, set to: <%d> milliseconds", igNapTime */

    igNapTime= 0;
    dbg(TRACE,"init_fcshdl: PAUSING too small, set to: <%d> seconds",
	igNapTime);
  }
  /* else if (igNapTime>5000) */
  else if (igNapTime>10)
  {
    /* igNapTime= 5000;
    dbg(TRACE,"Init_fcshdl: PAUSING to big, set to: <%d> milliseconds", igNapTime ); */ 

    igNapTime= 10;
    dbg(TRACE,
	"Init_fcshdl: PAUSING to big, set to: <%d> seconds", igNapTime);

  }

  dbg(TRACE,"Init_fcshdl: bline configuration: <%s>", cgConfigFile );
  igBlineHandle = blineCreate (cgConfigFile);
  dbg(TRACE,"Init_fcshdl: blineCreate returned handle <%d>",igBlineHandle );

  /* ver 1.20 - offset from current time to arrival's STOA */
  (void) ReadConfigEntry("CHECKS","MINS_BEFORE_STOA", pclDbgLevel, "120");
  igMinsBeforeStoa = atoi (pclDbgLevel); 

  /* ver 1.20 - initialize the STOA of arrivals to send all allocations */
  /* This can be called only after igMinsBeforeStoa has been read! */
  SetArrRelStoa ();

  /* ver 1.20 - the interval to check and send deferred arrival belt/term updates */ 
  (void) ReadConfigEntry("CHECKS","STOA_SEND_INTERVAL_SECS", pclDbgLevel, "60");
  igStoaSendIntervalSecs = atoi (pclDbgLevel); 

  /* ver 1.21 - the terminal whose flts are ignored at the deferred interval */
  /* Default value CANNOT be a blank because that is possible */
  (void) ReadConfigEntry("CHECKS","IGNORE_TERMINAL", pclDbgLevel, "?");
  strcpy (cgIgnoreTerminal, pclDbgLevel); 

  /* ver 1.22 - the FIPS.Daily_Mask.Key_Code which is ignored at the */
  /*  deferred interval */
  /* Default value is what users will be entering, but this code is to */
  /* allow it to be configurable if needed later on */
  (void) ReadConfigEntry("CHECKS","IGNORE_KEYCODE", pclDbgLevel, "X");
  strcpy (cgIgnore_STEV, pclDbgLevel); 

  /* ver 1.25 - offset from current time to STOA/STOD for RAC joins */
  (void) ReadConfigEntry("CHECKS","MINS_BEFORE_RAC_JOIN", pclDbgLevel, "360");
  igMinsBeforeRacJoin = atoi (pclDbgLevel); 

  /* ver 1.25 - the interval to check and send joins */ 
  (void) ReadConfigEntry("CHECKS","RAC_JOIN_INTERVAL_SECS", pclDbgLevel, "360");
  igRacJoinIntervalSecs = atoi (pclDbgLevel); 

  /* ver 1.25 - hard code the initialization of the RAC_RTYP times */
  /* Send 5 minutes of joins at startup.			   */
  tgCurr_RAC_RKEY_STOA_D = (time ('\0')) + (igMinsBeforeRacJoin * 60); 
  tgLast_RAC_RKEY_STOA_D = tgCurr_RAC_RKEY_STOA_D - 300;

  /* ver 1.25 additional hack 20090520 */

  strcpy (pcgLastJoinSplit, "random_string_1");
  strcpy (pcgLastButOneJoinSplit, "random_string_2");

  /* ver 1.28 - Non sched TTYP's CR08005 */ 
  (void) ReadConfigEntry("CHECKS","NON_SCHED_TTYP", pclDbgLevel, "N");
  jj = 0; kk = 0;
  while (pclDbgLevel [jj] != '\0')
  {
    if (pclDbgLevel [jj] == ',')
    {
      jj++;
      continue;
    }
    cgNonSchedTtyp [kk] = pclDbgLevel [jj];
    dbg(TRACE, "Extracted Non-Sched TTYP [%c]", cgNonSchedTtyp [kk]);
    kk++;jj++;
  }
  /* Null byte is used to detect end of matches in isNonSchedTtyp () */
  cgNonSchedTtyp [kk] = '\0';
  
  /* ver 1.29 - Joined flight's date */

  (void) ReadConfigEntry("CHECKS","ADD_JOIN_FLIGHT_DATE", pclDbgLevel, "N");
  if ((pclDbgLevel [0] == 'Y') || (pclDbgLevel [0] == 'y'))
  {
    igAddJoinFlightDate = 1;
    dbg(TRACE, "CHECKS.ADD_JOIN_FLIGHT_DATE [%c] add the date", pclDbgLevel [0]);
  }
  else
  {
    igAddJoinFlightDate = 0;
    dbg(TRACE, "CHECKS.ADD_JOIN_FLIGHT_DATE [%c] do not add the date", pclDbgLevel [0]);
  }

  /* ver 1.31 - adding of blanked belts to FCS */

  (void) ReadConfigEntry("CHECKS","ADD_BLANKED_BELT", pclDbgLevel, "Y");
  if ((pclDbgLevel [0] == 'N') || (pclDbgLevel [0] == 'n'))
  {
    igAddBlankedBeltOfArrival = 0;
    dbg(TRACE, "CHECKS.ADD_BLANKED_BELT [%c]", pclDbgLevel [0]);
  }
  else
  {
    igAddBlankedBeltOfArrival = 1;   
    dbg(TRACE, "CHECKS.ADD_BLANKED_BELT [%c]", pclDbgLevel [0]);
  }



  (void) ReadConfigEntry("CHECKS","MSG_LEN_CHECK",pclDbgLevel,"");
  ilItem= 1;
  memset(LenChecks,0,sizeof(LenChecks));
  for (ilLoop= 0; ilLoop<sizeof(LenChecks)/sizeof(LenChecksRecs); ilLoop++)
  {
      if (get_item(ilItem, pclDbgLevel, pclDummy2, 0, ",", "\0", "\0")!=TRUE)
      {
         break;
      }
      ilItem++;
      LenChecks[ilLoop].Pos= atoi(pclDummy2);
      if (get_item(ilItem, pclDbgLevel, pclDummy2, 0, ",", "\0", "\0")!=TRUE)
      {
         break;
      }
      ilItem++;
      LenChecks[ilLoop].Flag[0]= pclDummy2[0];
      if (get_item(ilItem, pclDbgLevel, pclDummy2, 0, ",", "\0", "\0")!=TRUE)
      {
         break;
      }
      ilItem++;
      LenChecks[ilLoop].Len= atoi(pclDummy2);
      igLenCheckLoops++;
      dbg(TRACE,"Init_fcshdl: Lencheck[%d]: Pos %d, Char %c, Max len %d",ilLoop,
              LenChecks[ilLoop].Pos,LenChecks[ilLoop].Flag[0],LenChecks[ilLoop].Len);
  }

    if(ilRC == RC_SUCCESS)
    {
	/* read HomeAirPort from SGS.TAB */
	ilRC = tool_search_exco_data("SYS", "HOMEAP", cgHopo);
	if (ilRC != RC_SUCCESS)
	{
	    dbg(TRACE,"Init_fcshdl: EXTAB,SYS,HOMEAP not found in SGS.TAB");
	    Terminate();
	} else {
	    dbg(TRACE,"Init_fcshdl: home airport <%s>",cgHopo);
	}
    }

	
    if(ilRC == RC_SUCCESS)
    {

	ilRC = tool_search_exco_data("ALL","TABEND", cgTabEnd);
	if (ilRC != RC_SUCCESS)
	{
	    dbg(TRACE,"Init_fcshdl: EXTAB,ALL,TABEND not found in SGS.TAB");
	    Terminate();
	} else {
	    dbg(TRACE,"Init_fcshdl: table extension <%s>",cgTabEnd);
	}
    }

  igFcsMsgCount = 0; /* V.1.23 */

  igInitOK = TRUE;
  debug_level = igDbgSave;
  return(ilRC);

} /* end of initialize */

/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
    int    ilRC = RC_SUCCESS;                /* Return code */
    
    
    dbg(TRACE,"Reset: destroying bline handle <%d>",igBlineHandle );
    blineDestroy (igBlineHandle);
    dbg(TRACE,"Reset: bline configuration: <%s>", cgConfigFile);
    sleep (1);
    igBlineHandle = blineCreate (cgConfigFile);
    dbg(TRACE,"Reset: blineCreate returned handle <%d>",igBlineHandle );
    
    return ilRC;
    
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate()
{

    blineDestroy (igBlineHandle);
    dbg(TRACE,"Terminate: now leaving ...");
    
    exit(0);
    
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int ipSig)
{
    int    ilRC = RC_SUCCESS;            /* Return code */
    switch(ipSig)
    {
        case SIGTERM:
            dbg(TRACE,"HandleSignal: Received Signal <%d> (SIGTERM). Terminating now...",ipSig);
            break;
        case SIGALRM:
            dbg(TRACE,"HandleSignal: Received Signal<%d>(SIGALRM)",ipSig);
            return;
            break;
        default:
            dbg(TRACE,"HandleSignal: Received Signal<%d>",ipSig);
            return;
            break;
    } /* end of switch */
    exit(0);
    
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
    int    ilRC = RC_SUCCESS;
    return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    int    ilRC = RC_SUCCESS;
    
    switch(pipErr) {
    case    QUE_E_FUNC    :    /* Unknown function */
        dbg(TRACE,"HandleQueErr: <%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :    /* Malloc reports no memory */
        dbg(TRACE,"HandleQueErr: <%d> malloc failed",pipErr);
        break;
    case    QUE_E_SEND    :    /* Error using msgsnd */
        dbg(TRACE,"HandleQueErr: <%d> msgsnd failed",pipErr);
        break;
    case    QUE_E_GET    :    /* Error using msgrcv */
        dbg(TRACE,"HandleQueErr: <%d> msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"HandleQueErr: <%d> route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"HandleQueErr: <%d> route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX    :
        dbg(TRACE,"HandleQueErr: <%d> unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"HandleQueErr: <%d>  unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE    :
        dbg(TRACE,"HandleQueErr: <%d> queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"HandleQueErr: <%d> missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES    :
        dbg(TRACE,"HandleQueErr: <%d> queue does not exist",pipErr);
        break;
    case    QUE_E_RESP    :    /* No response on CREATE */
        dbg(TRACE,"HandleQueErr: <%d> no response on create",pipErr);
        break;
    case    QUE_E_FULL    :
        dbg(TRACE,"HandleQueErr: <%d> too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG    :    /* No message on queue */
        dbg(TRACE,"HandleQueErr: <%d> no messages on queue",pipErr);
        break;
    case    QUE_E_INVORG    :    /* Mod id by que call is 0 */
        dbg(TRACE,"HandleQueErr: <%d> invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :    /* Queues is not initialized*/
        dbg(TRACE,"HandleQueErr: <%d> queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"HandleQueErr: <%d> requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"HandleQueErr: <%d> receive buffer to small ",pipErr);
        break;
    default            :    /* Unknown queue error */
        dbg(TRACE,"HandleQueErr: <%d> unknown error",pipErr);
        break;
    } /* end switch */
         
    return;
} /* end of HandleQueErr */


/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
    int    ilRC = RC_SUCCESS;            /* Return code */
    int    ilBreakOut = FALSE;
    
    do
    {
        ilRC = que(QUE_GETBIGNW,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text;    
        if( ilRC == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRC != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRC);
            } /* fi */
        
            switch( prgEvent->command )
            {
            case    HSB_STANDBY    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_COMING_UP    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_ACTIVE    :
                ctrl_sta = prgEvent->command;
                ilBreakOut = TRUE;
                break;    
            case    HSB_ACT_TO_SBY    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate();
                break;    
    
            case    HSB_STANDALONE    :
                ctrl_sta = prgEvent->command;

                ilBreakOut = TRUE;
                break;    

            case    SHUTDOWN    :
                Terminate();
                break;
                        
            case    RESET        :
                ilRC = Reset();
                break;
                        
            case    EVENT_DATA    :
                dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
                    
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;
            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;
            default            :
                dbg(TRACE,"HandleQueues: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        }
        CheckBline();
    } while (ilBreakOut == FALSE);
    if(igInitOK == FALSE)
    {
            ilRC = Init_fcshdl();
            if(ilRC != RC_SUCCESS)
            {
                dbg(TRACE,"HandleQueues: : init failed!");
            } /* end of if */
    }/* end of if */
} /* end of HandleQueues */
    

typedef struct {     /******************************************************************************/
  char *UfisName;    /* Fieldname in UFIS                                                          */
  char *FcsOutName;  /* mapped to code in FCS, is empty for fixed position in FCS out Data         */
  uint Start;        /* if Start is set, it is fixed position in FCS out data                      */
  uint Length;       /* maximum length in FCS out data                                             */
  uint DateStart;    /* if DateStart is set, cut of YY from Date                                   */
} MapFields;         /******************************************************************************/
                     
static MapFields cgFcsArrFields [] = { 
  {"FLNO",    "", FLNO_POS, 8, 0},
  {"STOA",    "", DATE_POS, 6, 2}, /* DateStart relevant for date: cutoff YY  */
  {"RFLN", "028",        0, 8, 0},
  {"GTA1", "043",        0, 4, 0},
  {"PSTA", "047",        0, 4, 0},
  {"BLT1", "202",        0, 2, 0},
  {"TGA1", TERM_CODE,    0, TERM_LEN, 0},
  {"CSGN",    "", FLNO_POS, 8, 0}
}; 

static MapFields cgFcsDepFields [] = { 
  {"FLNO",    "", FLNO_POS, 8, 0},
  {"STOD",    "", DATE_POS, 6, 2}, /* DateStart relevant for date: cutoff YY  */
  {"RFLN", "028",        0, 8, 0},
  {"GTD1", "043",        0, 4, 0},
  {"PSTD", "047",        0, 4, 0},
  {"TGD1", TERM_CODE,    0, TERM_LEN, 0},
  {"CSGN",    "", FLNO_POS, 8, 0}
}; 

static MapFields cgFcsTowFields [] = { 
  {"FLNO",    "", FLNO_POS, 8, 0},
  {"STOA",    "", DATE_POS, 6, 2}, /* DateStart relevant for date: cutoff YY  */
  {"PSTA", "221",        0, 4, 0},
  {"CSGN",    "", FLNO_POS, 8, 0}
}; 

int igMaxFlds;
int igMaxArrFlds = sizeof(cgFcsArrFields) / sizeof(MapFields);
int igMaxDepFlds = sizeof(cgFcsDepFields) / sizeof(MapFields);
int igMaxTowFlds = sizeof(cgFcsTowFields) / sizeof(MapFields);

/************************************************************************/
/* HandleConnetionFlight - get some fields of the arrival connected to	*/
/* a given departure's RKEY						*/
/* 2010057 - CR08005 - add CSGN & TTYP to selected items.			*/
/************************************************************************/

void HandleConnetionFlight(char *pclRkey, char *pclArrdata)
{
  int ilRC = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  int ilNoOfRec = 0;
  short slFkt = 0;
  short slCursor = 0;

      sprintf (pcgSqlBuf, "SELECT FLNO,STOA,CSGN,TTYP FROM AFTTAB WHERE URNO=%s", pclRkey);
      dbg (TRACE, "HandleConnetionFlight: %s", pcgSqlBuf);
      ilNoOfRec = 0;
      slFkt = START;
      slCursor = 0;
      ilGetRc = DB_SUCCESS;
      while (ilGetRc == DB_SUCCESS)
      {
        ilGetRc = sql_if (slFkt, &slCursor, pcgSqlBuf, pclArrdata);
        if (ilGetRc == DB_SUCCESS)
        {
          ilNoOfRec++;
          BuildItemBuffer (pclArrdata, "FLNO,STOA,CSGN,TTYP", 4, ",");
          dbg (TRACE, "HandleConnetionFlight: %d.: DATA <%s>",
            ilNoOfRec, pclArrdata);
        }                       /* end if */
        else
        {
          if (ilGetRc == RC_FAIL)
          {
            dbg (TRACE, "HandleConnetionFlight: ORACLE ERROR !!");
            ilRC = RC_FAIL;
          }                     /* end if */
        }                       /* end else */
        slFkt = NEXT;
      }                         /* while */
      close_my_cursor (&slCursor);
}


/*******************************************************************************/
/* MqsTabAckPending: Update status of just sent message to status pending Ack  */
/*******************************************************************************/
int MqsTabAckPending(char *pcpUrno)
{
    int ilRC = RC_SUCCESS;
    char clNowTime[20] = "\0";
    short slFkt = 0;
    short slCursor = 0;
    char clSqlBuf[4500] ="\0";
    char clData[100] = "\0";
    int ilLoop = 0;

      TimeToStr(clNowTime,time(NULL));
      sprintf(clSqlBuf,"UPDATE MQSTAB SET STAT='ACKPENDING',LSTU='%s' WHERE URNO=%d",
          clNowTime,atol(pcpUrno));
      dbg(TRACE,"MqsTabAckPending: clSqlBuf: <%s>",clSqlBuf);
      slFkt = START;
      slCursor = 0;
      ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clData);
      if (ilRC == RC_FAIL)
      {
        dbg(TRACE,"MqsTabAckPending: sql_if failed ###################");
      }
      commit_work();
      close_my_cursor (&slCursor);
}

/*******************************************************************************/
/* MqsTabWrite : All data written to FCS (beside ACK) will be written to MQSTAB*/
/* for respooling and debugging purpose                                        */
/*******************************************************************************/
int MqsTabWrite (char *pcpMsg, char *pcpUrno)
{
    int ilRC = RC_SUCCESS;
    char clNowTime[20] = "\0";
    char clTPutTime[20] = "\0";
    int igCurrInsTime;
    short slFkt = 0;
    short slCursor = 0;
    char clSqlBuf[4500] ="\0";
    char clData[100] = "\0";
    int ilLoop = 0;

/* INSERT MESSAGE INTO MQRTAB */

    dbg(DEBUG,"MqsTabWrite: saving <%s>",pcpMsg);
      ilRC = GetNextValues(pcpUrno,1);
      igCurrInsTime   = time(NULL);
      if (igLastInsTime != igCurrInsTime)
         igNumPerTime = 0;
      else
         igNumPerTime++;
      igLastInsTime= igCurrInsTime;
      TimeToStr(clNowTime,igLastInsTime);
      sprintf(clTPutTime,"%s%02d",clNowTime,igNumPerTime);

      ConvertClientStringToDb(pcpMsg);
    
      sprintf(clSqlBuf,"INSERT INTO MQSTAB (URNO,INAM,TPUT,DATA,COUN,STAT,REAS,LSTU) VALUES (%d,'%s','%s','%s',%d,'%s',%d,'%s')",
          atol(pcpUrno),cgMqrInterfaceName,clTPutTime,pcpMsg,1,"TO-SEND",0,clNowTime);
      dbg(TRACE,"MqsTabWrite: clSqlBuf: <%s>",clSqlBuf);
      slFkt = START;
      slCursor = 0;
      ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clData);
      if (ilRC != RC_SUCCESS)
      {
        dbg(TRACE,"MqsTabWrite: sql_if failed ###################");
      }
      commit_work();
      close_my_cursor (&slCursor);
        
      ConvertDbStringToClient(pcpMsg);
}

/******************************************************************************/
/* blineDataWrite : The Data writing to FCS. After every 12 nessage written   */
/* a control message is send                                                  */
/*
 * This function sends type 5 data updates to FCS, so it the correct place to
 * implement the PAUSING.
 *
 * v.1.30 - perform a validation on the outgoing message in a similar manner as
 * FCSU. This is to prevent FCSU not incrementing their receipt counter when we
 * send garbled data, because we will increment *our* counter. This would lead 
 * to an endless loop of us re-sending the same block of messages because our
 * Type 6 flow control will not be matched with an equivalent count from FCSU.
 *
 * The above problem was not encoutered with FCS because FCS did not count 
 * incoming but just echoed whatever Type 6 count was sent to them.
 ******************************************************************************/

int blineDataWrite (char *pcpMsg)
{
  char pclUrno[16] = "\0";

  if (iValidateOutgoingMessage (pcpMsg) == FALSE)
  {
    dbg(TRACE,"blineDataWrite: =========== WARNING ===========");
    dbg(TRACE,"blineDataWrite: Dropping incorrect format message <%s>",
      pcpMsg);
    return 0; 
  }

  if (igSpooling==FALSE)
  {
    MqsTabWrite(pcpMsg,pclUrno);
  }
  if ((igWaitForMyAck==FALSE) && (igWaitForFcsAck == FALSE))
  {
    dbg(TRACE,"blineDataWrite: sending <%s>",pcpMsg);
    blineWrite (igBlineHandle, pcpMsg);
    igWroteTypeSix=FALSE;
    tgLastAckTime = time(NULL);

    /*  Pause here for PAUSING ms. Note that the time() is called before we
     *  pause, so that the calculation for acks is unaffected.. (?)
     *  Also, it gives the pause before sending pur type 6 FCReq, so that
     *  is also not lost at FCS.
     */

    sleep (igNapTime); 

    if (igSpooling==FALSE)
    {
      MqsTabAckPending(pclUrno);
    }
    igBlineLineNo++;
    if (igBlineLineNo == igBlineLineNoCount)
    {
      SendMyAck();
    }
  }
  else if (igWaitForFcsAck == TRUE)
  {
    dbg(TRACE,"blineDataWrite: FCS not initialized, not sending <%s>",pcpMsg);
  }
  else 
  {
    dbg(TRACE,"blineDataWrite: ACK pending, not sending <%s>",pcpMsg);
  }
}

/******************************************************************************/
/* The ClrSpcInFlno converts "LH 4711" to "LH4711" for FCS                    */
/* and "LH 001" to "LH1"                                                      */
/******************************************************************************/
int ClrSpcInFlno(char *pclFlno)
{
int ilLen;
int ii;
char pclAlc[4];
char pclFltn[12];
char pclFcsFlno[DATABLK_SIZE] = "\0";

   strcpy(pclFcsFlno,pclFlno);
   if (GetFcsFlno("", "", pclFcsFlno,"","","")==RC_SUCCESS)
   {
      dbg(TRACE,
        "ClrSpcInFlno: patch FLNO <%-8.8s> to <%-8.8s>",pclFlno,pclFcsFlno);
      strcpy(pclFlno,pclFcsFlno);
  }
/*
  memset(pclAlc,0,sizeof(pclAlc));
  memset(pclFltn,0,sizeof(pclFltn));
  strncpy(pclAlc,pclFlno,3);
  if (pclAlc[2]==' ')
  {
    pclAlc[2]= 0;
  }
  strcpy(pclFltn,&pclFlno[3]);

  sprintf(pclFlno,"%s%d",pclAlc,atoi(pclFltn));
*/
  ilLen=strlen(pclFlno);
  if (ilLen>8)
  { /*  */
    ilLen=8;
  }
  return ilLen;
}

/*****************************************************************************/
/* The DataToFcs forwards the AFTTAB change to FCS. But a check is done to   */
/* see if any data is relevant for FCS                                       */
/* iDelFlag introduced in v.1.14					     */
/* v.1.19 - skip TGA1/TGD1 if they are blank. TGA1/TGD1 is sent only with    */ 
/* a gate allocation.							     */
/* v.1.20 - belts and terminals for arrivals sent based on a time criteria   */
/* viz., compare the STOA with current system time plus configurable offset  */
/* v.1.25 - send arrival split only if arrival is not joined to a different  */
/*   new departure.							     */
/* v.1.25 - 20090520 - Hold off first : do not include the test if a 	     */
/*   split/match is repeated (IsRepeatedMatch(). because all updates will be */
/*   tested, and must set a flag to indicate a '028' msg is being tested.    */
/*   It is possible that the problem is isolated to Handle_RAC ()     	     */
/*   Added the above because if UFR is received before RAC, then there	     */
/*   will be at least one repeated link for the first RAC.		     */
/* v.1.28 - 20100517 - Non-scheduled flights handled.			     */
/* v.1.29 - 20110603 - Add date of joined flight for FCSU		     */
/*****************************************************************************/
int DataToFcs(char *pclFld, char *pclData, int iDelFlag)
{
  int ilRC= RC_SUCCESS;
  int ilRkeyNo = 0;
  int ilSplit = FALSE;
  char pclCurrField[16];
  char pclCurrDat[16];
  char pclCurrDt2[16], pclCurrUrno [16], pclOldRkey [16], pclMatch [16];
  char pclRkey[16];
  char pclOutStr[100];
  char pclArrdata[100] = "\0";
  char pclArrFlno[16];
  char pclArrDate[16], pclJoinArrDateLocal [16], pclJoinDepDateLocal [16];
  char pclDepFlno[16];
  char pclArrStoa[16], pclArrTifa[16];
  char pclArrBlt1[8];
  char *pclOldData, pclTerm [TERM_LEN + 1];
  char pclArrCsgn [16], pclDepCsgn [16], pclTtyp [4], pclTtypArr [4];

  int  jj,src,dst;
  int  flagBelt, flagGate, flagIsSplitOrJoin;

  MapFields *clFcsFields;

      int ilItemNo = 0;
      int ilLenF  = 0;
      int ilLenD  = 0;
      int ilDatPos;
      int ilLen  = 0;
      int ii;
      int ilBothFlag = 0;
      int ilBothRun;
      ilDatPos= DATE_POS + DATE_LEN;
      /* Pre-Fill entire output to FCS with spaces */
      memset(pclOutStr,' ', sizeof(pclOutStr)); 
      pclOutStr[TYPE_POS]='5';
      pclOutStr[UCOD_POS]='U'; 
      strcpy (pclTtyp, ""); 
      strcpy (pclTtypArr, ""); 
      memset (pclArrCsgn, ' ', 16);
      memset (pclDepCsgn, ' ', 16);
      ilItemNo = get_item_no (pclFld, "ADID", 5) + 1;
      if (ilItemNo > 0)
      { /* first check ADID: */
        (void) get_real_item (pclCurrDat, pclData, ilItemNo);
        if (pclCurrDat[0]=='A')
        {
           pclOutStr[ADID_POS]='1';
           clFcsFields= cgFcsArrFields;
           igMaxFlds=   igMaxArrFlds;
        }
        else if (pclCurrDat[0]=='D')
        {
           pclOutStr[ADID_POS]='2';
           clFcsFields= cgFcsDepFields;
           igMaxFlds=   igMaxDepFlds;
        }
        else if (pclCurrDat[0]=='B')
        {
           pclOutStr[ADID_POS]='1';
           clFcsFields= cgFcsArrFields;
           igMaxFlds=   igMaxArrFlds;
           ilBothFlag = 1;
        }
      }
      ilItemNo = get_item_no (pclFld, "FTYP", 5) + 1;
      if (ilItemNo > 0)
      { /* next check FTYP: */
        (void) get_real_item (pclCurrDat, pclData, ilItemNo);
        if (pclCurrDat[0]=='T')
        { 
	  (void) HandleTowing (pclFld, pclData, iDelFlag);
	  return 0;
        } 
      }

      flagGate = 0;
      flagBelt = 0;
      strcpy (pclArrTifa, " ");
      flagIsSplitOrJoin = 0;

      /* CR08005 - Non Sched check */
      ilItemNo = get_item_no (pclFld, "TTYP", 5) + 1;
      if (ilItemNo > 0)
      {
        (void) get_real_item (pclTtyp, pclData, ilItemNo); 
        /* A tow message should not arrive at this location */
      }
      else
      {
        dbg (TRACE,
          "*** Config Error - No TTYP from FLIGHT - assume Scheduled Flt ***");
        strcpy (pclTtyp, "S"); 
      }

      /* run for ADID 'A',  or ADID 'B' or (ADID 'A' and ADID 'D' in case of ADID 'B' )*/
      for (ilBothRun=0 ; ilBothRun<=ilBothFlag ; ilBothRun++)
      {
        ilItemNo = 0;
        do
        { /* check received fields for FCS data */
          ilItemNo++;
          ilLenF= get_item(ilItemNo, pclFld, pclCurrField, 0, ",", "\0", "\0");
          if (ilLenF == TRUE)
          {
            ilLenD= get_item(ilItemNo, pclData, pclCurrDat, 0, ",", "\0", "\0");
            dbg(DEBUG,"(%d.) <%s> <%s>",ilItemNo,pclCurrField,pclCurrDat);
            for (ii= 0; ii<igMaxFlds; ii++)
            {
              if (strcmp(pclCurrField,clFcsFields[ii].UfisName)==0)
              {
                  dbg(DEBUG,"<%s> <%s> <%d> <%d>",
                  clFcsFields[ii].UfisName,clFcsFields[ii].FcsOutName,clFcsFields[ii].Start,clFcsFields[ii].Length);
		  /* Test for a blanked terminal indicator, save if non blank */

		  if ((strcmp (pclCurrField, "TGA1") == 0) ||
		     (strcmp (pclCurrField, "TGD1") == 0))
		  { 
		     if (strlen (pclCurrDat) == 0) 
		     {
		        pclTerm [0] = '\0'; 
		     }
		     else 
		     {
		        strcpy (pclTerm, pclCurrDat); 
		     } 
		     /* append the terminal after other fields */
		     continue;		
	 	  } 

		  /* Flag if gate encountered */ 

		  if ((strcmp (pclCurrField, "GTA1") == 0) ||
		     (strcmp (pclCurrField, "GTD1") == 0))
		  { 
		     flagGate = 1;
		  }

	          /* Save belt and stoa for v.1.20 */

		  if (strcmp (pclCurrField, "BLT1") == 0)
		  { 
		     flagBelt = 1;
		     strcpy (pclArrBlt1, pclCurrDat);
		     if (strlen (pclArrBlt1) == 0)	/* blanked */
	             {
		       memset (pclArrBlt1, ' ', BLT1_LEN);
		       memset (&pclArrBlt1 [BLT1_LEN], '\0', 1);
	             }
		     continue;
		  } 
		  if (strcmp (pclCurrField, "STOA") == 0)
		  { 
		     strcpy (pclArrStoa, pclCurrDat);
		  } 

                  /* v.1.28 non sched */
		  if (strcmp (pclCurrField, "CSGN") == 0)
		  { 
                    if (pclOutStr[ADID_POS] == '1')
                      strcpy (pclArrCsgn, pclCurrDat);
                    if (pclOutStr[ADID_POS] == '2')       
                      strcpy (pclDepCsgn, pclCurrDat);
                  } 

                  ilLen=strlen(pclCurrDat);
                  if (ilLen>clFcsFields[ii].Length)
                  { /* relevant for date: HHMMSS */
                    ilLen=clFcsFields[ii].Length;
                  }
                  if (clFcsFields[ii].Start==0)
                  {
                    memcpy(&pclOutStr[ilDatPos],clFcsFields[ii].FcsOutName,3);
                    memcpy(&pclOutStr[ilDatPos+3],pclCurrDat,ilLen);
                    ilDatPos = ilDatPos + clFcsFields[ii].Length + 3;
                  }
                  else
                  {
                    if (clFcsFields[ii].DateStart==0)
                    { 
                      if (clFcsFields[ii].Start==FLNO_POS) /* Can be FLNO or CSGN */
                      {
                        if (((isNonSchedTtyp (pclTtyp [0])) == 0)
                          && ((strcmp (pclCurrField, "FLNO")) == 0))
                        {
                          ilLen= ClrSpcInFlno(pclCurrDat);
                          memcpy(&pclOutStr[clFcsFields[ii].Start],pclCurrDat,ilLen);
                        }

                        if (((isNonSchedTtyp (pclTtyp [0])) > 0)
                          && ((strcmp (pclCurrField, "CSGN")) == 0))
                        {  /* Non sched flight - use CSGN in FLNO position */
                          if (pclOutStr[ADID_POS] == '1')
                          { 
                            memcpy (&pclOutStr [clFcsFields [ii].Start],
                             pclArrCsgn,
                             ((ilLen = strlen (pclArrCsgn)) > 8) ? 8: ilLen);
                          } 
                          else if (pclOutStr[ADID_POS] == '2')
                          { 
                            memcpy (&pclOutStr [clFcsFields [ii].Start],
                             pclDepCsgn,
                             ((ilLen = strlen (pclDepCsgn)) > 8) ? 8 : ilLen);
                          } 
                          else
                            dbg (TRACE,
                             "DataToFCS: *** Unexpected ADID at this point [%c]",
                             pclOutStr[ADID_POS]);
                        }
                      }
                    }
                    else
                    { /* FCS expects tha day in local time */
                      UtcToLocalTimeFixTZ(pclCurrDat);
                      memcpy(&pclOutStr[clFcsFields[ii].Start],&pclCurrDat[2],ilLen);
                    }
                  }
                break;
              }
	      else	/* if a non-FCS sendable field encountered */
	      { 
	        if (strcmp (pclCurrField, "TIFA") == 0)
	        { 
	          strcpy (pclArrTifa, pclCurrDat);
	        } 
	      }
            }
          }
        }
        while (ilLenF==TRUE);

        /* v.1.31 */
        if (igAddBlankedBeltOfArrival == TRUE)
        {
          /* Only if BLT1 was not already handled in field-list loop above */
          if ((igToAddBlankBelt == 1) && (flagBelt == 0)) 
          {
            dbg(TRACE,"DataToFcs: adding blanked belt from AFT select, not CEDA msg");
            flagBelt = 1;
            memset (pclArrBlt1, ' ', BLT1_LEN);
            memset (&pclArrBlt1 [BLT1_LEN], '\0', 1);
          }
        }
        /*** end v.1.31 ***************/

	/* Departures: Add terminal if gate is in msg for FCS */

	if ((flagGate == 1) && (pclOutStr[ADID_POS] == '2'))
	{
	  InsertTerminal (pclTerm, pclOutStr, &ilDatPos);
	}

	/* Arrivals: first check if before critical time */

	if (pclOutStr[ADID_POS] == '1')
	{
	  if ((BeforeDateTimeOffset (pclArrStoa, pclArrTifa,
	         igMinsBeforeStoa, "UTC")))
	  {
	    if (flagGate == 1)	/* poke in the terminal */
	    {
	      InsertTerminal (pclTerm, pclOutStr, &ilDatPos);
	    } 
	    if (flagBelt == 1)	/* append the belt */
	    {
	      memcpy (&(pclOutStr [ilDatPos]), BLT1_CODE, BLT1_CODE_LEN);
	      /* strlen, not BLT1_LEN o'wise null gets copied.. */
	      memcpy (&(pclOutStr [ilDatPos + BLT1_CODE_LEN]), pclArrBlt1, strlen (pclArrBlt1));
	      ilDatPos = ilDatPos + BLT1_CODE_LEN + BLT1_LEN;
	    }
	  }
	}

        /*
	  DKA's explanation for JIM's code:
	  As the RKEY of an arrival never changes, the only way to detect a
	  split or join of a departure OR arrival is to test the old RKEY, if
	  any, of *departure* data sent to FCSHDL from FLIGHT.
        */
        if (pclOutStr[ADID_POS]=='2')
        {
          /*
		Save a copy for later use. Date is already in local YYMMDD.
          */
          memcpy (&(pclJoinDepDateLocal [0]), &(pclOutStr [DATE_POS]), DATE_LEN);
          strcpy (pclOldRkey, ""); /* Pre-set for later use, might be modified */
          ilRkeyNo = get_item_no (pclFld, "RKEY", 5) + 1;
          if (ilRkeyNo > 0)
          { /* check conection flight */
            (void) get_real_item (pclRkey, pclData, ilRkeyNo); 
            ilItemNo = get_item_no (pclFld, "URNO", 5) + 1;
            if (ilItemNo > 0)
            {  /* compare RKEY and URNO, equal: clear FLNO */
               (void) get_real_item (pclCurrDt2, pclData, ilItemNo);
               strcpy (pclCurrUrno, pclCurrDt2);	/* save for later use */
               if (strcmp(pclRkey,pclCurrDt2)==0)
               {  /* compare RKEY and URNO, equal: clear FLNO */
                 /* DKA's explanation for JIM's code: the flwg line is bcos old
		    data in CEDA is appended following a null byte in the data
		    buffer. So pclOldData is a pointer into the start of old data.
                 */
                 pclOldData= pclData + strlen(pclData) + 1;
                 if (strlen(pclOldData)>0)
                 {
                   (void) get_real_item (pclRkey, pclOldData, ilRkeyNo);
                   if (strlen(pclRkey)>1)
                   {
                     /* Because implication is that RKEY has changed */ 
                     strcpy (pclOldRkey, pclRkey); /* Store for later use */
                     ilSplit = TRUE;
                     strcpy(pclDepFlno,"        ");
                   }
                   else
                   { /* if RKEY has no old data, this is a insert, and RKEY==URNO is arrival: skip */ 
                     ilRkeyNo= 0;
                   }
                 }
                 else
                 { /* if RKEY has no old data, this is a insert, and RKEY==URNO is arrival: skip */
                   ilRkeyNo= 0;
                 }
               }
               else
               {
                 strncpy(pclDepFlno,&pclOutStr[FLNO_POS],8);
                 pclDepFlno[8]= 0;
               }

               if (ilRkeyNo!=0)
               { 
                 flagIsSplitOrJoin = 1;
                 /* RKEY and URNO not equal: join, ilSplit TRUE: split , else skip */
                 (void) HandleConnetionFlight(pclRkey, pclArrdata);
                 (void) get_real_item (pclTtypArr, pclArrdata, 4);
                 if (isNonSchedTtyp (pclTtypArr [0]) == 0)
                 {
                   (void) get_real_item (pclArrFlno, pclArrdata, 1);
                   ilLen= ClrSpcInFlno(pclArrFlno);
                 }
                 else 
                   (void) get_real_item (pclArrCsgn, pclArrdata, 3);
                 
                 (void) get_real_item (pclArrDate, pclArrdata, 2);
                 /*
			Redundant copy to avoid messing with existing logic.
                 */
                 strcpy (pclJoinArrDateLocal, pclArrDate); 
                 UtcToLocalTimeFixTZ(pclJoinArrDateLocal);
                 dbg(TRACE,"DataToFcs: pclArrDate <%s> ==> pclJoinArrDateLocal <%s>",
                   pclArrDate, pclJoinArrDateLocal);

                 if (ilSplit == TRUE)
                 {
                    strncpy(&pclOutStr[ilDatPos],"028",3);
                    ilDatPos+=11; /* just past the blank corresponding flno */
 
                    if (igAddJoinFlightDate != 0)
                    {
                      /*
			NCS request to add a blank date when link flight date is needed.
			6 spaces corresponding to blanked YYMMDD.
                      */

                      memcpy (&(pclOutStr [ilDatPos]), JOIN_FLT_DATE_CODE, JOIN_FLT_DATE_CODE_LEN);
                      memcpy (&(pclOutStr [ilDatPos + JOIN_FLT_DATE_CODE_LEN]), "      ", JOIN_FLT_DATE_LEN);
                      ilDatPos = ilDatPos + JOIN_FLT_DATE_CODE_LEN + JOIN_FLT_DATE_LEN;
                    } 
                 }
                 else
                 {
                    strncpy(&pclOutStr[ilDatPos],"028",3);
                    if (isNonSchedTtyp (pclTtypArr [0]) == 0)
                      memcpy(&pclOutStr[ilDatPos+3],pclArrFlno,ilLen);
                    else
                    { 
                      memcpy(&pclOutStr[ilDatPos+3],pclArrCsgn,
                       (((ilLen = strlen (pclArrCsgn)) > 8) ? 8 : ilLen)); 
                    }
                    ilDatPos+=11;

                    if (igAddJoinFlightDate != 0)
                    {
                      /* Add the joined arrival' local sdate and code */
	              memcpy (&(pclOutStr [ilDatPos]), JOIN_FLT_DATE_CODE, JOIN_FLT_DATE_CODE_LEN);
                      /* Drop the century from the year */
                      memcpy (&(pclOutStr [ilDatPos + JOIN_FLT_DATE_CODE_LEN]), &(pclJoinArrDateLocal [2]), JOIN_FLT_DATE_LEN);
                      ilDatPos = ilDatPos + JOIN_FLT_DATE_CODE_LEN + JOIN_FLT_DATE_LEN;
                      pclOutStr [ilDatPos] = '\0';
                    }
                 }
               }
            }
          }
        } 

        pclOutStr[ilDatPos]= 0;
        dbg(DEBUG,"DataToFcs: pclOutStr <%d> <%s>",ilDatPos,pclOutStr); 
	/* At the end of the iterations above, there were no matches in the Ceda msg
	   with FCS sendable fields
	*/
        if (ilDatPos==DATE_POS + DATE_LEN) 
        {
          dbg(TRACE,"DataToFcs: nothing FCS relevant changed, no FCS output  ....");
        }
        else 
        { /* FCS output */
          if (flagIsSplitOrJoin == 1)
          {
            if (!IsRepeatedMatch (pclOutStr))
              blineDataWrite (pclOutStr);
          }
          else
          {
            blineDataWrite (pclOutStr);
          }
          if (ilRkeyNo!=0)
          {/* send connecting flight for arrival also */

           /*
		v.1.25 - send the match-msg for the arrival only if it is
		joined to this same departure or is unmatched. This is
		to avoid sending incorrect arrival split when it is joined
		to a different departure. The former is handled with original
	        code if we get here. So only need to test if former
		arrival is really unmatched if ilSplit is TRUE.
		pclOldRkey would be the pclOldDat's RKEY (i.e.the former
		arrival's URNO)
           */
            if (ilSplit == TRUE)
              (void) GetMatch (pclOldRkey, "A", pclMatch);

	    if (((ilSplit == TRUE) && (!strcmp (pclMatch, "")))
			    || (ilSplit == FALSE))
	    {
              pclOutStr[ADID_POS]='1';
              ilDatPos= DATE_POS + DATE_LEN;
              /* copy current FLNO to position of corresponding FLNO */
              strncpy(&pclOutStr[ilDatPos],"028",3);
              if (isNonSchedTtyp (pclTtyp [0]) == 0)
                memcpy(&pclOutStr[ilDatPos+3],pclDepFlno,8);
              else
              {
                jj = strlen (pclDepCsgn);
                if (jj > 8)
                  jj = 8;
                memcpy(&pclOutStr[ilDatPos+3],pclDepCsgn,jj);
              }

              /* clear current FLNO */
              memset(&pclOutStr[FLNO_POS],' ',8);
              /* copy corresponding FLNO to cuurent FLNO */
              if (isNonSchedTtyp (pclTtypArr [0]) == 0)
                memcpy(&pclOutStr[FLNO_POS],pclArrFlno,ilLen);
              else
              { 
                jj = strlen (pclArrCsgn);
                if (jj > 8)
                  jj = 8;
                memcpy(&pclOutStr[FLNO_POS],pclArrCsgn,jj);
              }
              /* get date of corresponding flight */
              dbg(TRACE,"DataToFcs: pclArrDate ==> <%s>",pclArrDate);
              UtcToLocalTimeFixTZ(pclArrDate);
              dbg(TRACE,"DataToFcs: pclArrDate ==> <%s>",pclArrDate);
              /* set date of corresponding flight */
              memcpy(&pclOutStr[DATE_POS],&pclArrDate[2],DATE_LEN);

              /*
		Append the code and data for the joined departure's sdate.
		Start appending from after length of '028' and flno/csgn length.
              */
              ilDatPos = ilDatPos + 3 + FLNO_LEN;
              if ((ilSplit == FALSE) && (igAddJoinFlightDate != 0))
              { 
                memcpy (&(pclOutStr [ilDatPos]), JOIN_FLT_DATE_CODE, JOIN_FLT_DATE_CODE_LEN);
                memcpy (&(pclOutStr [ilDatPos + JOIN_FLT_DATE_CODE_LEN]), pclJoinDepDateLocal, JOIN_FLT_DATE_LEN);
                ilDatPos = ilDatPos + JOIN_FLT_DATE_CODE_LEN + JOIN_FLT_DATE_LEN;
              }
              else if ((ilSplit == TRUE) && (igAddJoinFlightDate != 0))
              {
                /*
			NCS request to add a blank date when link flight date is needed.
			6 spaces correcponding to blanked YYMMDD.
                */

                memcpy (&(pclOutStr [ilDatPos]), JOIN_FLT_DATE_CODE, JOIN_FLT_DATE_CODE_LEN);
                memcpy (&(pclOutStr [ilDatPos + JOIN_FLT_DATE_CODE_LEN]), "      ", JOIN_FLT_DATE_LEN);
                ilDatPos = ilDatPos + JOIN_FLT_DATE_CODE_LEN + JOIN_FLT_DATE_LEN;
              }

              pclOutStr[ilDatPos]= 0;
              dbg(DEBUG,"DataToFcs: pclOutStr <%d> <%s>",ilDatPos,pclOutStr);
              if (!IsRepeatedMatch (pclOutStr))
                blineDataWrite (pclOutStr);
	    }
          }
        }
        if (ilBothRun<=ilBothFlag)
        {
          /* in case of ADID='B' now change Outstring and loop again */
          ilDatPos= DATE_POS + DATE_LEN;
          memset(pclOutStr,' ', sizeof(pclOutStr)); 
          pclOutStr[TYPE_POS]='5';
          pclOutStr[UCOD_POS]='U';
          pclOutStr[ADID_POS]='2';
          clFcsFields= cgFcsDepFields;
          igMaxFlds=   igMaxDepFlds;
        }
      }
}


/******************************************************************************/
/* The handle data routine handles updates to AFTTAB done by i.e. FLIGHT      */
/* If the update has no flag, that it was initiated by FCS, the update has    */
/* to be forwarded to FCS.                                                    */
/* 20071231 - v.1.20 - add handling for NTISCH TRIG command.		      */
/******************************************************************************/
static int HandleData()
{
  char pclUrnoRkey[32] = "";
  char pclDepFlno[32] = "";
  int    ilRC = RC_SUCCESS;            /* Return code */
  DebugPrintItem(DEBUG,prgItem);
  DebugPrintEvent(DEBUG,prgEvent);
  int que_out;   /* Sender que id */
  BC_HEAD *prlBchd = NULL;		/* Broadcast header		*/
  CMDBLK  *prlCmdblk = NULL;	/* Command Block 		*/
  char *pclSel=NULL,*pclFld=NULL,*pclData=NULL, *pclOldData=NULL;	

  que_out = prgEvent->originator; 	/* Queue-id des Absenders */
  prlBchd = (BC_HEAD *) ((char *)prgEvent + sizeof(EVENT));
  prlCmdblk = (CMDBLK  *) ((char *)prlBchd->data);

  /*
	Branch off to handler before attempting to parse other info from
	command message, in case that causes a crash..  
  */

//  if (!(strcmp (prlCmdblk->command, "TRIG"))) 
//  {
//    /* HandleNtischTrigger ("TRIG"); */
//    return ilRC;
//  }

  pclSel = prlCmdblk->data;
  pclFld = pclSel + strlen(pclSel)+1;
  pclData = pclFld + strlen(pclFld)+1;

  pclOldData= strtok(pclData,"\n");
  pclOldData= strtok(NULL,"\n");
  dbg(TRACE,"FROM <%d> TBL <%s>",que_out,prlCmdblk->obj_name);
  dbg(TRACE,"Cmd <%s> Que (%d) WKS <%s> Usr <%s>", prlCmdblk->command, prgEvent->originator, prlBchd->recv_name, prlBchd->dest_name);
  dbg(TRACE,"Prio (%d) TWS <%s> TWE <%s>", prgItem->priority, prlCmdblk->tw_start, prlCmdblk->tw_end);
  dbg(TRACE,"Sel <%s> ", pclSel);
  dbg(TRACE,"Fld <%s> ", pclFld);
  dbg(TRACE,"Dat <%s> ", pclData);
  dbg(TRACE,"Old <%s> ", pclOldData);

  /* if (strstr(prlCmdblk->tw_start,"fcs,")==NULL) */
  /*
	BST - Do not process if from ROGHDL, FOGDHD, FODSHD because then all the
	parameters are not filled, leading to crash in DataToFcs.
  */

  igToAddBlankBelt = 0;	/* v.1.31 */

  /* v.1.25 - Handle RAC messages from ROG */
  if (que_out == igModId_Roghdl)
  {
    HandleRoghdlMsg (prlCmdblk->command, pclSel, pclFld, pclData);
    return ilRC;
  }

  /* v.1.26 - Handle UFR Auto_Tow insertion from FOGDHD */
  if (que_out == igModId_Fogdhd)
  { 
    HandleFogdhdMsg (prlCmdblk->command, pclSel, pclFld, pclData); 
    return ilRC;
  }

  if ((strstr(prlCmdblk->tw_start,"fcs,")==NULL)
	&& (que_out != igModId_Roghdl)
	&& (que_out != igModId_Fogdhd)
	&& (que_out != igModId_Fogshd)) 
  {
    if (strcmp(prlCmdblk->command,"UFR")==0)
    { /* Send Update to FCS */

      /* v.1.31 */
      if (igAddBlankedBeltOfArrival == TRUE)
      {
        (void) CheckArrBlankedBelt (pclFld,pclData); 
      }
      DataToFcs(pclFld, pclData, 0);
    }
    else if (strcmp(prlCmdblk->command,"DFR")==0) 
    { /* Delete of tow record is still an update to FCS but we flag it because */
      /* the ceda message contains fields of a deleted record, special handling*/
      DataToFcs(pclFld, pclData, 1);
    }

    else if (strcmp(prlCmdblk->command,"READ")==0)
    { /* Simulate FCS input */
    }
  }
  else
  {
    dbg(TRACE,"flight replied to my input or msg fm IGNORE_MODULES");
    /* InsertUfis2FcsFlno(prlCmdblk->tw_start,pclFld,pclData); */
  }

  return ilRC;
    
} /* end of HandleData */

/********************************************************************************/
/* HandleTowing will select from AFTTAB where the URNO == the given AURN.	*/
/* If the returned record has PSTA == to the given PSTD, implles that with hgh	*/
/* probability that we have been passed the first tow record, i.e. first towing	*/
/* after the landing.								*/
/* BST suggested that FLIGHT+ACTION be configured to send AURN always. This has	*/
/* been done. If that is ever rolled back, then AURN must be selected from DB	*/
/* in this function.								*/
/* For tow insert, the IFR does not have AURN yet. We must ignore that msg!	*/
/* We instead use the UPJ that follows which is when the tow record has been	*/
/* joined to the arrival. But that record has no PSTD because it has not been	*/
/* changed! So must select again then.						*/
/*										*/
/* Return 0 for failure, and non-zero positive for success.			*/
/*										*/
/* v.1.14 - add flag to know if we are handling a DFR of towing record.		*/
/* We then have all the fields and data of the tow record which is deleted from	*/
/* the DB. We hence do not select the tow record, but only the AURN arrival.	*/
/* We must get STOA of arrival record because a tow delete msg does not have 	*/
/* this.									*/
/* v.1.28 - non-sched changes - use CSGN instead of FLNO. FLIGHT and ACTION	*/
/* must be configured to always send these fields to FCSHDL.			*/
/* -- BUT-- CSGN for a tow is always "TOWING", and its TTYP can be ' ' or 'S',  */
/* so explicit select the CSGN of the arrival record, and detect a non-sched	*/
/* towing by testing if FLNO of the tow record is blank/empty.			*/
/********************************************************************************/

static int HandleTowing (char *pcpFld, char *pcpData, int iDelFlag)
{ 
  int ilRC = RC_DATA_ERR, res;
  short slFkt = 0;
  short slCursor = 0;
  char clSqlBuf[2048] ="\0";	/* probably too large, but do not know if	*/
  char clDataArea [2048];	/* sql_if does anything special with it		*/
  int ilItemNo = 0 ;
  int jj;
  char pclAurn [16], pclTowPstd [8], pclUrno [16], pclArrFlno [16], pclRkey [16];
  char pclDummy [64], pclFcsMsg [32];
  char pclTowPsta [8], pclArrStoa [16], pclKeycode [4];
  char pclTtyp [4], pclCsgn [16], pclArrCsgn [16];

  res = 1;

  memset (pclAurn, '\0', 16);
  jj = get_item_no (pcpFld, "AURN", 5) + 1;  
  (void) get_real_item (pclAurn, pcpData, jj);

  if (!strlen (pclAurn))
  {
    dbg(TRACE,"HandleTowing: Empty AURN, doing nothing",clSqlBuf); 
    return 0;
  } 

  /* rev 1.22 - if STEV for arrival record set, do not send tow to FCS */
  (void) CheckArrKeycode (pclAurn, pclKeycode);
  if (!strcmp (pclKeycode, cgIgnore_STEV))
  {
    dbg(TRACE, "Ignore: Tow AURN [%s] has Arrival STEV [%s]", pclAurn, pclKeycode);
    return 0;
  }

  jj = get_item_no (pcpFld, "URNO", 5) + 1;  
  (void) get_real_item (pclUrno, pcpData, jj); 

  /*
	We obtain towing PSTD via SQL because if only towing PSTA changes,
	ACTION will not send PSTD. We cannot configure ACTION to always send
	PSTD because FCSHDL will then always send PSTD for any departure flight,
	even if no change.  

	v.1.14 - only if not a tow delete.
  */ 

  if (iDelFlag == 0)
  { 
    sprintf(clSqlBuf,"SELECT PSTA, PSTD FROM AFTTAB WHERE URNO=%8s",pclUrno);
    dbg(TRACE,"HandleTowing: clSqlBuf: <%s>",clSqlBuf); 
    slFkt = START;
    slCursor = 0;
    ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clDataArea); 
    if (ilRC == RC_SUCCESS)
    {
      BuildItemBuffer (clDataArea, "PSTA,PSTD ", 2, ",");
      get_real_item (pclTowPsta, clDataArea, 1);
      get_real_item (pclTowPstd, clDataArea, 2);

      dbg(TRACE,"HandleTowing: Towing PSTD [%s] PSTA [%s]",pclTowPstd, pclTowPsta); 
    }
    else if (ilRC == RC_NOTFOUND)
    {
      dbg(TRACE,"HandleTowing: Towing record not found!");
      res = 0;
    }
    else 
    {
      dbg(TRACE,"HandleTowing: SELECT for Towing PSTD failed <%d> ##########################",ilRC);
      res = 0;
    }

    commit_work();
    close_my_cursor (&slCursor);
  }
  else  /* this is towing delete */
  { 
    /*
	Check if towing records still exist, in which case we must change this
	from DFR of last tow to an UFR of the new first towing. This is because
	the former first towing is deleted, the 2nd towing is now the first,
	but we do not get an UFR of the promotion of the 2nd to 1st.
    */

    memset (pclRkey, '\0', 16);
    jj = get_item_no (pcpFld, "RKEY", 5) + 1;  
    (void) get_real_item (pclRkey, pcpData, jj); 

    sprintf(clSqlBuf,
      "SELECT PSTA, PSTD FROM AFTTAB WHERE RKEY=%8s AND FTYP='T' ORDER BY STOA",
      pclAurn, pclTowPstd);
    dbg(TRACE,"HandleTowing: clSqlBuf: <%s>",clSqlBuf); 
    slFkt = START;
    slCursor = 0;
    ilRC = sql_if (slFkt, &slCursor, clSqlBuf, pclDummy); 
    if (ilRC == RC_SUCCESS)
    {
      BuildItemBuffer (pclDummy, "PSTA,PSTD", 2, ",");
      (void) get_real_item (pclTowPsta, pclDummy, 1);
      (void) get_real_item (pclTowPstd, pclDummy, 2);

      dbg(TRACE,"HandleTowing: Found: First towing PSTD [%s] PSTA [%s] RKEY [%s]",
	pclTowPstd, pclTowPsta, pclRkey); 
      res = 1;
    }

    else if (ilRC == RC_NOTFOUND)
    {
      /* Blanked tow stand will be sent to FCS */
      dbg(TRACE,"HandleTowing: No Tow records RKEY [%s], blank towing", pclRkey);


      /* PSTD was in DFR field list and are for the former first tow */

      memset (pclTowPstd, '\0', 8);
      jj = get_item_no (pcpFld, "PSTD", 5) + 1;  
      (void) get_real_item (pclTowPstd, pcpData, jj); 

      /* this is sent blanked to FCS */
      strcpy (pclTowPsta, "     "); 
      
      res = 1;
    }
    else 
    {
      dbg(TRACE,"HandleTowing: SELECT for Towing records failed <%d> #####################",ilRC);
      res = 0;
    }

    commit_work();
    close_my_cursor (&slCursor); 

  }


  /* Hunt for the related arrival flight */

  if (res != 0)
  { 
    if (iDelFlag == 0)
    {
     /* For update/insert of towing, psta is valid check */
     sprintf(clSqlBuf,"SELECT STOA,CSGN FROM AFTTAB WHERE URNO=%8s AND PSTA='%s'", pclAurn, pclTowPstd); 
    }
    if (iDelFlag == 1)
    {
     /* For deletes, all we want is stoa of arrival, we already know the correct psta for tow */
     sprintf(clSqlBuf,"SELECT STOA,CSGN FROM AFTTAB WHERE URNO=%8s", pclAurn, pclTowPstd);
    }
    dbg(TRACE,"HandleTowing: clSqlBuf: <%s>",clSqlBuf); 
    slFkt = START;
    slCursor = 0;
    ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clDataArea); 
    if (ilRC == RC_SUCCESS)
    {
      BuildItemBuffer (clDataArea, "STOA,CSGN ", 2, ",");
      get_real_item (pclArrStoa, clDataArea, 1);
      get_real_item (pclArrCsgn, clDataArea, 2);
      dbg(TRACE,"HandleTowing: Arrival STOA [%s] CSGN [%s]", pclArrStoa, pclArrCsgn);
    }
    else if (ilRC == RC_NOTFOUND)
    {
      dbg(TRACE,"HandleTowing: Arrival record not found");
      res = 0;
    }
    else 
    {
      dbg(TRACE,"HandleTowing: SELECT for matching Arrival failed <%d> #####################",ilRC);
      res = 0;
    }

    commit_work();
    close_my_cursor (&slCursor);
  } 

  if (res != 0)
  {
    memset (pclFcsMsg, ' ', 32);		/* FCS expects spaces between char's */
    memcpy (pclFcsMsg, "51U", 3); 
    jj = get_item_no (pcpFld, "TTYP", 5) + 1;  
    (void) get_real_item (pclTtyp, pcpData, jj); 

    /* Non sched FLNO would be empty */
    jj = get_item_no (pcpFld, "FLNO", 5) + 1;  
    memset (pclArrFlno, '\0', 16);
    (void) get_real_item (pclArrFlno, pcpData, jj); 

    /* if ((isNonSchedTtyp (pclTtyp [0])) == 0) */
    if (pclArrFlno  [0] != '\0')		/* Scheduled flight */
    { 
     ClrSpcInFlno (pclArrFlno);			/* actually calls GetFcsFlno */
     memcpy (&pclFcsMsg [3], pclArrFlno, 8);	/* FCS 8 char FLNO */
    }
    else					/* Non scheduled - use CSGN */
    { 
      jj = strlen (pclArrCsgn);
      if (jj > 8)
        jj = 8;
      memcpy (&pclFcsMsg [3], pclArrCsgn, jj);	/* FCS 8 char FLNO */
    }
    UtcToLocalTimeFixTZ (pclArrStoa);		/* not the tow record's, */
    memcpy (&pclFcsMsg [11], &pclArrStoa [2], 6);  /* FCS YYMMDD */ 
    memcpy (&pclFcsMsg [17], "221", 3);		/* Towing code */
    memcpy (&pclFcsMsg [20], pclTowPsta, strlen (pclTowPsta));	/* Towing value without the null byte! */
    pclFcsMsg [24] = '\0'; 
    blineDataWrite (pclFcsMsg); 
  } 
  
  return res;

}


/********************************************************************************/
/* InsertTerminal 							.	*/
/* Pokes in the terminal and terminal code just after the flight no. and sdate	*/
/* of a prepared message to FCS.						*/
/*										*/
/********************************************************************************/

static void InsertTerminal (char *pclTerm, char *pclFcsMsg, int *ilLastPos)
{
    int dst, src;

    if (pclTerm [0] != '\0')
    {
      /*
	Per NCS request, terminal code must precede all other updatable
	fields. But that means that it is has to be inserted just after the
	preamble of "3[1,2]U<flno><flda>".
      */

      /* Move updatable fields away by TERM_CODE_LEN + TERM_LEN */

      for (dst = ((*ilLastPos) + TERM_CODE_LEN + TERM_LEN), src = *ilLastPos;
                src >= PREAMBLE_LEN;
	        src--, dst--)
      {
	pclFcsMsg [dst] = pclFcsMsg [src];
      }

      /* Poke in the terminal code and terminal in the hole created */

      memcpy (&(pclFcsMsg [PREAMBLE_LEN]), TERM_CODE, TERM_CODE_LEN);
      memcpy (&(pclFcsMsg [PREAMBLE_LEN + TERM_CODE_LEN]), pclTerm, TERM_LEN); 
	    
      /* In case there is also a join/split in next block w/RKEY*/
      (*ilLastPos) = (*ilLastPos) + TERM_CODE_LEN + TERM_LEN; 

    }
} 

/********************************************************************************/
/* BeforeDateTimeOffset  							*/
/* Test if a given CEDA date/time (YYYYMMDDHHMMSS) is before the time defined	*/
/* by (now + (iPeriod in minutes)). Function requires an argument pclType	*/
/* which defines "UTC"or "LOC". This ties in with the Ceda library function 	*/
/* called.									*/
/* Return 1 if before the offset, else 0. Errors are <0. 			*/
/*										*/
/* v.1.21 - additional test using TIFA for case when flight is arriving early	*/
/********************************************************************************/
static int  BeforeDateTimeOffset (char *pclCedaDT_stoa, char *pclCedaDT_tifa,
    int iPeriod, char *pclType)
{
    char	pclNow [16], pclDTtmp [16];
    long	stoa, tifa, now; 
    long	period_secs;
    int		res = 0;
    
    GetServerTimeStamp (pclType, 1, 0, pclNow);    
    StringToDate (pclNow, "%Y%m%d%H%M%S", &now);
    StringToDate (pclCedaDT_stoa, "%Y%m%d%H%M%S", &stoa); 
    
    if (!(strcmp (pclCedaDT_tifa, " ")))	/* paranoia */
      tifa = stoa + 3600;
    else
    {
      StringToDate (pclCedaDT_tifa, "%Y%m%d%H%M%S", &tifa); 
    }

      /*
	tifa is equal to stoa only at flight creation. If ETAA or LAND
	has been set, then tifa is 5 mins more that ETAA/LAND. Adjust
	the tifa for a correct comparison with stoa.
      */
      
      if (tifa != stoa)
        tifa -= 300;
 
    period_secs = iPeriod * 60;

    if (stoa <= tifa)
    {
      if (stoa <= (now + period_secs))
      {
        dbg(TRACE,
	    "B4DTOffs: STOA [%s] <= NOW [%s] + [%d] mins, TIFA [%s]", 
	    pclCedaDT_stoa, pclNow, iPeriod, pclCedaDT_tifa);
        res = 1;
      } 
      else
      { 
        dbg(TRACE,
	    "B4DTOffs: STOA [%s] > NOW [%s] + [%d] mins, TIFA [%s]", 
	    pclCedaDT_stoa, pclNow, iPeriod, pclCedaDT_tifa); 
      } 
    }
    else
    { 
      if (tifa <= (now + period_secs))
      {
        dbg(TRACE,
	    "B4DTOffs: TIFA [%s] <= NOW [%s] + [%d] mins, STOA [%s]", 
	    pclCedaDT_tifa, pclNow, iPeriod, pclCedaDT_stoa);
        res = 1;
      } 
      else 
      {
        dbg(TRACE,
	    "B4DTOffs: TIFA [%s] > NOW [%s] + [%d] mins, STOA [%s]", 
	    pclCedaDT_tifa, pclNow, iPeriod, pclCedaDT_stoa); 
      } 
    } 

    return res;
}

/********************************************************************************/
/* SetArrRelStoa ()								*/
/* At pgm start, inits the last stoa. The init value is for now set to one min.	*/
/* before current time. Future enhancement may set it to a value taken from	*/
/* a flag file, or minus one minute if file not found.				*/
/*										*/
/* After init, every call will							*/
/* - set the last stoa to the last "current" value				*/
/* - set the current stoa to current date/time in UTC with seconds zeroed.	*/
/*   This is so that it can be used to select from AFTTAB. 			*/
/*										*/
/* The STOA's created here have the seconds zeroed out as not used in STOA.	*/
/* This ensures that when the Curr and Last STOA's are compared, a difference	*/
/* will be seen only if there is at least a minute's increment.			*/
/********************************************************************************/

static void SetArrRelStoa (void)
{ 
    tgCurrArrRelStoa = (time ('\0')) + (igMinsBeforeStoa * 60); 

    if (igFlagInit_ArrRelStoa == 0)
    { 
      /* This forces a resend of 5 minutes of STOA's at startup */
      tgLastArrRelStoa = tgCurrArrRelStoa - 300;
      igFlagInit_ArrRelStoa = 1;
    }
}

/********************************************************************************/
/* CheckArrRelease ()								*/
/* If we are now equal or more than the check interval, select eligible 	*/ 
/* arrival records for force sending to FCS					*/
/*										*/
/* v.1.21 - ignore for IGNORE_TERMINAL						*/ 
/* v.1.28 - CR08005 - non scheds - use CSGN instead of FLNO in msg to FCS	*/
/********************************************************************************/

static void CheckArrRelease (void)
{
  char	pclNow [16], pclLast [16]; 
  struct tm   *ptm; 
  int ilRC = RC_DATA_ERR, res;
  short slFkt = 0;
  short slCursor = 0;
  char clSqlBuf[2048] ="\0";
  char clDataArea [2048];
  int ilNoOfRec = 0, pos; 
  char fcs_flno [16], stoa [16], gta1 [8], psta [8], blt1 [8], tga1 [4];
  char ttyp [4], csgn [16];
  char fcsMsg [64];

  SetArrRelStoa ();

  if ((tgCurrArrRelStoa - tgLastArrRelStoa) >= igStoaSendIntervalSecs)
  { 
    memset (pclNow, '\0', 16); 
    memset (pclLast, '\0', 16); 

    /* Note the two zeroes to implement minutes precision */
    ptm = gmtime (&tgCurrArrRelStoa);
    strftime (pclNow, 16, "%Y%m%d%H%M00", ptm);
    ptm = gmtime (&tgLastArrRelStoa);
    strftime (pclLast, 16, "%Y%m%d%H%M00", ptm); 

    /* if not defined in config file, no filter on terminal */
    if (!(strcmp (cgIgnoreTerminal, "?")))
      sprintf(clSqlBuf,
        "SELECT FLNO,STOA,GTA1,PSTA,BLT1,TGA1,URNO,CSGN,TTYP FROM AFTTAB WHERE "
        "(STOA > '%s'  AND STOA <= '%s') AND ADID = 'A' "
        "AND STEV <> '%s' "
        "ORDER BY STOA", 
          pclLast, pclNow, cgIgnore_STEV); 
    else
      sprintf(clSqlBuf,
      "SELECT FLNO,STOA,GTA1,PSTA,BLT1,TGA1,URNO,CSGN,TTYP FROM AFTTAB WHERE "
      "(STOA > '%s'  AND STOA <= '%s') AND ADID = 'A' "
      "AND STEV <> '%s' "
      "AND TGA1 <> '%s' ORDER BY STOA", 
        pclLast, pclNow, cgIgnore_STEV, cgIgnoreTerminal); 

    dbg(TRACE,"CheckArrRelease: <%s>",clSqlBuf);

    slFkt = START;
    slCursor = 0;
    ilRC = RC_SUCCESS;
    
    while (ilRC == RC_SUCCESS)
    { 
      ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clDataArea);  

      if (ilRC == RC_SUCCESS)
      {  
        ilNoOfRec++;
        BuildItemBuffer (clDataArea,
          "FLNO,STOA,GTA1,PSTA,BLT1,TGA1,URNO,CSGN,TTYP", 9, ",");
        dbg (TRACE, "CheckArrRelease: Rec %d:<%s>", ilNoOfRec, clDataArea); 

        (void) get_real_item (ttyp, clDataArea, 9); 
        if (isNonSchedTtyp (ttyp [0]) == 0)
        {
          (void) get_real_item (fcs_flno, clDataArea, 1); /* FLNO */
          ClrSpcInFlno (fcs_flno);
        }
        else
        {
          /*
		FLNO might be non-blank, but for FCS consistency we only use
          	CSGN in the place of FLNO. 
          */
          memset (fcs_flno, ' ', 16);
          (void) get_real_item (csgn, clDataArea, 8); /* CSGN */
          memcpy (fcs_flno, csgn, strlen (csgn));
        }
        (void) get_real_item (stoa, clDataArea, 2); 
        UtcToLocalTimeFixTZ(stoa);
        
        (void) get_real_item (gta1, clDataArea, 3);
        if (gta1 [0] == '\0')
	    memset (gta1, ' ', GTA1_LEN);
        (void) get_real_item (psta, clDataArea, 4); 
        if (psta [0] == '\0')
	    memset (psta, ' ', PSTA_LEN);
        (void) get_real_item (blt1, clDataArea, 5); 
        if (blt1 [0] == '\0')
	    memset (blt1, ' ', BLT1_LEN);
        (void) get_real_item (tga1, clDataArea, 6); 
        
        pos = 3;
	memset (fcsMsg, ' ', 64);
        memcpy (&fcsMsg [0], "51U", 3);

        memcpy (&fcsMsg [pos], fcs_flno, 8);
        pos += 8;

        memcpy (&fcsMsg [pos], &stoa [2], 6);
        pos += 6;

	if (tga1[0] != '\0')
        {
	  memcpy (&fcsMsg [pos], TERM_CODE, TERM_CODE_LEN);
	  pos += TERM_CODE_LEN;
          memcpy (&fcsMsg [pos], tga1, TERM_LEN);
          pos += TERM_LEN;
        }

        memcpy (&fcsMsg [pos], GTA1_CODE, GTA1_CODE_LEN);
	pos += GTA1_CODE_LEN;
	/* strlen() and not GTA1_LEN, o'wise also null byte is copied! */
        memcpy (&fcsMsg [pos], gta1, strlen (gta1)); 
        pos += GTA1_LEN;

        memcpy (&fcsMsg [pos], PSTA_CODE, PSTA_CODE_LEN);
	pos += PSTA_CODE_LEN;
        memcpy (&fcsMsg [pos], psta, strlen (psta));
        pos += PSTA_LEN;

        memcpy (&fcsMsg [pos], BLT1_CODE, BLT1_CODE_LEN);
	pos += BLT1_CODE_LEN;
        memcpy (&fcsMsg [pos], blt1, strlen (blt1));
        pos += BLT1_LEN;

        fcsMsg [pos] = '\0';
        blineDataWrite (fcsMsg);
      } 
      else
      {
        if (ilRC == RC_FAIL)
        {
          dbg (TRACE, "CheckArrRel : ORACLE ERROR !!");
          ilRC = RC_FAIL;
        }
      }
      slFkt = NEXT;
    } 

    tgLastArrRelStoa = tgCurrArrRelStoa;
    commit_work();
    close_my_cursor (&slCursor);
    dbg (TRACE, "============ END - CheckArrRelease ==========");
  } 
}

/********************************************************************************/
/* CheckArrKeycode ()								*/
/* Return the STEV of the associated arrival for a given AURN.			*/
/********************************************************************************/

static int  CheckArrKeycode (char *pclAurn, char *keycode)
{ 
  int ilRC = RC_DATA_ERR;
  short slFkt = 0;
  short slCursor = 0;
  char clSqlBuf[2048] ="\0";
  char clDataArea [2048];
  
  strcpy (keycode, "");

  sprintf(clSqlBuf,"SELECT STEV FROM AFTTAB WHERE URNO='%8s'", pclAurn);
  /* dbg(TRACE,"CheckArrKeycode: clSqlBuf: <%s>",clSqlBuf); */
  slFkt = START;
  slCursor = 0;
  ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clDataArea); 
  if (ilRC == RC_SUCCESS)
  {
    BuildItemBuffer (clDataArea, "STEV", 1, ",");
    get_real_item (keycode, clDataArea, 1);
  }
  else if (ilRC == RC_NOTFOUND)
  {
    dbg(TRACE,"CheckArrKeycode: no Arrival rec for AURN [%s]!", pclAurn);
  }
  else 
  {
    dbg(TRACE,"CheckArrKeycode: SELECT for arrival failed <%d> ##########################",ilRC); 
  }

  commit_work();
  close_my_cursor (&slCursor); 

  return ilRC; 
}

/********************************************************************************/
/* HandleRoghdlMsg ()								*/
/* Perform splits and joins that originate from ROGHDL. For some reason	these	*/
/* do not come in from FLIGHT, in which case would have been handled in		*/
/* DataToFcs.*/
/* 20100419 - pclTempStr size increase from 256 to 4096...                      */
/********************************************************************************/

static void HandleRoghdlMsg (char *pclCmd, char *pclSel, char *pclFld,
	char *pclData)
{
  char	pclRkey [16], pclTempStr [4096];
  char  *pCh; 
  long	lRkey;

  if ((!strcmp (pclFld, "RKEY")) && (!strcmp (pclCmd, "RAC")))
  { 
    /*
	Local copy due to strtok() modifying args. Expect one or more RKEY's.
    */

    (void) strcpy (pclTempStr, pclData);
    pCh = strtok (pclTempStr, ","); 
    while (pCh != '\0')
    {
      strcpy (pclRkey, pCh);
      lRkey = atol (pclRkey);
      /* Call Handle_RAC_RKEY with flag to check against release period */
      Handle_RAC_RKEY (lRkey, 1);
      pCh = strtok ('\0', ","); 
    } 
  }
  return;
}

/********************************************************************************/
/* GetFlightFieldsByUrno () */
/* Long overdue handy */ 
/* Input : selection where clause. This should return one row only... */
/* Output : hardcoded which makes it a bother if a field is added */
/* To-do - change to a field and value list in arguments*/
/* Return val is DB return error code */
/*									*/
/* 20100517 - CR08005 - add CSGN & TTYP to output.				*/
/********************************************************************************/

static int  GetFlightFieldsByUrno (char *pclWhere,
	char *pclFlno, char *pclStoa, char *pclStod, char *pclAdid, 
	char *pclCsgn, char *pclTtyp)
{ 
  int	ilItemNo;

  /* for DB access */
  int ilRC = RC_DATA_ERR, res;
  short slFkt = 0;
  short slCursor = 0;
  char clSqlBuf[2048] = "\0";
  char clDataArea [2048];
  int ilNoOfRec = 0;
  
  sprintf(clSqlBuf, "SELECT FLNO,STOA,STOD,ADID,CSGN,TTYP FROM AFTTAB WHERE %8s",
    pclWhere);

  dbg(TRACE,"SQL: [%s]", pclWhere);

  slFkt = START;
  slCursor = 0;
  ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clDataArea); 
  if (ilRC == RC_SUCCESS)
  { 
      BuildItemBuffer (clDataArea, "FLNO,STOA,STOD,ADID,CSGN,TTYP", 6, ","); 
      get_real_item (pclFlno, clDataArea, 1); 
      get_real_item (pclStoa, clDataArea, 2); 
      get_real_item (pclStod, clDataArea, 3); 
      get_real_item (pclAdid, clDataArea, 4);
      get_real_item (pclCsgn, clDataArea, 5);
      get_real_item (pclTtyp, clDataArea, 6); 
  }
  else if (ilRC == RC_NOTFOUND)
  {
    dbg(TRACE,"GetFlightFieldsByUrno: no fields for [%s]!", pclWhere);
  }
  else 
  {
    dbg(TRACE,"GetFlightFieldsByUrno: SELECT failed <%d> #####",ilRC); 
  }

  commit_work();
  close_my_cursor (&slCursor); 

  return ilRC;
}

/********************************************************************************/
/* Handle_RAC_RKEY () */
/* A key basis of the algorithm here is that there can be only at most exactly 	*/
/* one arrival & one departure sharing one unique RKEY				*/
/* iCheckTime - zero means do not test if a flight is before the configured time*/
/*	period. If '1' means that it is an online RAC update and must test the	*/
/*	stoa/stod against that configured period.				*/
/* If iCheckTime non-zero, then release per these criteria 			*/
/* (a) unmatched arrival: if stoa is less than MINS_BEFORE_RAC_JOIN from now	*/
/*	send.*/
/* (b) match: send the arr and dep testing only the arrival stoa per (s)	*/
/* (c) unmatched dep: if stod if less than MINS_BEFORE_RAC_JOIN from now	*/
/* */
/* If the customer ever changes their mind about testing online RAC, can	*/
/* probably just remove iCheckTime. A problem in this design is that iCheckTime	*/
/* tests against a hardcoded '1' or '0'. The '1' should be from the config file */
/* but does not seem economical to introduce it at first release.		*/
/* */
/* A possible defect in the specification wrt performing the "6 hour" select is	*/
/* what to do if an online RAC results in an arrival split within the 6 hours.	*/
/* For correctness, the departure's split must also be sent irregardless if it	*/
/* is beyond the 6-hour period. However, that is not possible to flag as that 	*/
/* is handled with a separate call to this function. I.e. the arrival and dep.	*/
/* have different RKEY's at this point. As the order of the appearance of RKEY's*/
/* is not in some order, the only way to handle this is to unconditionally send	*/
/* online RAC's, implying that the 6-hour selection is not required!		*/
/* */
/* 20090520 - add a check for a repeated msg					*/
/* 20100527 - CR08005 Non Scheduled flights - added CSGN and TTYP to call to	*/
/*   GetFlightFieldsByUrno (). But discovered that have to increase size of	*/
/*   pclAdid from 2 to 8, otherwise pclCsgn* will be corrupted when it returns	*/
/*   to us. I.e. the argument list of a function must have some minimum spacing	*/
/*   between variables.								*/
/********************************************************************************/

void Handle_RAC_RKEY (long rkey, int iCheckTime)
{
  char	pclWhere [128]; 
  char	pclAdid [8], pclUrno [16], pclRkey [16], pclTempStr [256];
  char  pclFlnoDep [16], pclFlnoArr [16], pclFlno [16];
  char  pclStod [16], pclStoa [16], pclDummy [16], *pCh, pclStodLocal [16], pclStoaLocal [16];
  char  fcsMsgArr [64], fcsMsgDep [64];
  int	ilItemNo, iResArr, iResDep, iArrSentJoin, jj, ilDatPos;
  long	lRkey;
  char	pclCsgnArr [32];
  char  pclTtypArr [32];
  char	pclCsgnDep [32];
  char  pclTtypDep [32];


  /* The RKEY can be for an arrival and/or departure. */

  dbg (TRACE, "Handle_RAC_RKEY: [%ld]", rkey); 
  pclFlnoDep [0] = '\0';
  pclFlnoArr [0] = '\0';
  
  sprintf (pclWhere, "RKEY=%ld AND ADID='A'", rkey); 
  iResArr = GetFlightFieldsByUrno (pclWhere, pclFlnoArr, pclStoa, pclDummy,
    pclAdid, pclCsgnArr, pclTtypArr);

  sprintf (pclWhere, "RKEY=%ld AND ADID='D'", rkey); 
  iResDep = GetFlightFieldsByUrno (pclWhere, pclFlnoDep, pclDummy, pclStod,
    pclAdid, pclCsgnDep, pclTtypDep); 

  if (iResArr == RC_SUCCESS)
  { 
    strcpy (pclStoaLocal, pclStoa);
    UtcToLocalTimeFixTZ (pclStoaLocal);
  } 
  if (iResDep == RC_SUCCESS)
  { 
    strcpy (pclStodLocal, pclStod);
    UtcToLocalTimeFixTZ (pclStodLocal);
  }

  iArrSentJoin = 0;
  if (iResArr == RC_SUCCESS)
  {
    if (((iCheckTime == 1)
      && (BeforeDateTimeOffset (pclStoa, pclStoa, igMinsBeforeRacJoin, "UTC"))) 
	|| (iCheckTime == 0)) 
    { 
      memset (fcsMsgArr, ' ', 64);
      memcpy (&fcsMsgArr [TYPE_POS], "51U", (FLNO_POS-TYPE_POS));

      if ((isNonSchedTtyp (pclTtypArr [0])) == 0)
      {
        ClrSpcInFlno (pclFlnoArr);		/* Convert to FCS flight no */
        memcpy (&fcsMsgArr [FLNO_POS], pclFlnoArr, (DATE_POS-FLNO_POS));
      }
      else
      { 
        memcpy (&fcsMsgArr [FLNO_POS], pclCsgnArr, 
          (((jj = strlen (pclCsgnArr)) > (DATE_POS-FLNO_POS))?(DATE_POS-FLNO_POS):jj));
      }

      memcpy (&fcsMsgArr [DATE_POS], &pclStoaLocal [2], DATE_LEN); /* YYMMDD */
      memcpy (&fcsMsgArr [DATE_POS+DATE_LEN], "028", 3); 
      if (iResDep == RC_DATA_ERR) 
      { 
        dbg (TRACE, "Handle_RAC_RKEY: RC_DATA_ERR - do nothing");
        return;
      }
      else if (iResDep == RC_NOTFOUND) 
      { 
        strcpy (pclDummy, FLNO_BLANK); 
      }
      else if (iResDep == RC_SUCCESS) 
      {
        /*
		Need temp copy otherwise subsequent block will try to double
		convert pclFlnoDep
	*/ 
        iArrSentJoin = 1;

        if (isNonSchedTtyp (pclTtypDep [0]) == 0)
        {
          strcpy (pclDummy, pclFlnoDep);	
          ClrSpcInFlno (pclDummy);		/* Convert to FCS flight no */  
        }
        else
        {
          strcpy (pclDummy, "                "); 
          memcpy (pclDummy, pclCsgnDep, strlen (pclCsgnDep));
        } 
      }
      else
      { 
        dbg (TRACE, "Handle_RAC_RKEY: Unexpected DB return code [%d]", iResDep);
        return;
      }
      
      memcpy (&fcsMsgArr [DATE_POS+DATE_LEN + 3], pclDummy, 8); 

      /*
	v.1.29 - add linked flight date if linked flight was not blank
	and we are configured to add link flight date.
      */
      if ((iResDep == RC_SUCCESS) &&  (igAddJoinFlightDate != 0))
      {
        ilDatPos = DATE_POS + DATE_LEN + 3 + FLNO_LEN; 
        memcpy (&fcsMsgArr [ilDatPos], JOIN_FLT_DATE_CODE, JOIN_FLT_DATE_CODE_LEN);
        ilDatPos += JOIN_FLT_DATE_CODE_LEN;
        memcpy (&fcsMsgArr [ilDatPos], &pclStodLocal [2], JOIN_FLT_DATE_LEN); /* YYMMDD */
        ilDatPos += JOIN_FLT_DATE_LEN;
        fcsMsgArr [ilDatPos] = '\0';
      }
      else if ((iResDep == RC_SUCCESS) &&  (igAddJoinFlightDate == 0))
      {
        fcsMsgArr [DATE_POS+DATE_LEN + 3 + FLNO_LEN] = '\0';
      }
      else if ((iResDep == RC_NOTFOUND) && (igAddJoinFlightDate == 0))
      {
        fcsMsgArr [DATE_POS+DATE_LEN + 3 + FLNO_LEN] = '\0';
      } 
      else if ((iResDep == RC_NOTFOUND) && (igAddJoinFlightDate != 0))
      {
        /*
           NCS request - add blank date if configured to send link flt date.  
           6 spaces for YYMMDD.
        */
        
        ilDatPos = DATE_POS + DATE_LEN + 3 + FLNO_LEN;  /* '3' is the length of '028' code */
        memcpy (&fcsMsgArr [ilDatPos], JOIN_FLT_DATE_CODE, JOIN_FLT_DATE_CODE_LEN);
        ilDatPos += JOIN_FLT_DATE_CODE_LEN;
        memcpy (&fcsMsgArr [ilDatPos], "      ", JOIN_FLT_DATE_LEN);
        ilDatPos += JOIN_FLT_DATE_LEN;
        fcsMsgArr [ilDatPos] = '\0';
      }


      
      if (!IsRepeatedMatch (fcsMsgArr))
        blineDataWrite (fcsMsgArr); 
    }
  }

  if (iResDep == RC_SUCCESS)
  { 
    /*
	Online RAC update for a join - just send w/o time period check but only
	that the corresponding arrival was sent. This is in case the arrival was
	out of the eligible time period, then we do not send the departure
   */
    if (((iCheckTime == 1) && (iResArr == RC_SUCCESS) && (iArrSentJoin == 1)) 

    /* Online RAC update for a dep split - check if before configured period */
    || ((iCheckTime == 1)
          && (iResArr != RC_SUCCESS)
          && (BeforeDateTimeOffset (pclStod, pclStod, igMinsBeforeRacJoin, "UTC"))) 
    
    /*
	Mass selection of flights at configured period:
	a. if there is a corresponding arrival, we send a join msg irregardless of
	   dep's stod.
	b. if no corr. arrival, we send a dep. split message.
	I.e. in either case we send something. It is implied that a previously sent
	arrival will be resent. There is no easy way to prevent this short of flagging
	the arrival's AFT record.
    */ 
    || (iCheckTime == 0)) 
    {
      memset (fcsMsgDep, ' ', 64);
      memcpy (&fcsMsgDep [TYPE_POS], "52U", (FLNO_POS-TYPE_POS));
      if (isNonSchedTtyp (pclTtypDep [0]) == 0)
      { 
        ClrSpcInFlno (pclFlnoDep); 
        memcpy (&fcsMsgDep [FLNO_POS], pclFlnoDep, (DATE_POS-FLNO_POS));
      }
      else
      { 
       memcpy (&fcsMsgDep [FLNO_POS], pclCsgnDep, 
          (((jj = strlen (pclCsgnDep)) > (DATE_POS-FLNO_POS))?(DATE_POS-FLNO_POS):jj));
      }

      memcpy (&fcsMsgDep [DATE_POS], &pclStodLocal [2], DATE_LEN); /* YYMMDD */
      memcpy (&fcsMsgDep [DATE_POS+DATE_LEN], "028", 3);

      if (iResArr == RC_DATA_ERR) 
      { 
        dbg (TRACE, "Handle_RAC_RKEY: RC_DATA_ERR - do nothing");
        return;
      }
      else if (iResArr == RC_NOTFOUND) 
      { 
        strcpy (pclDummy, FLNO_BLANK); 
      }
      else if (iResArr == RC_SUCCESS) 
      {
        /*
		Need temp copy otherwise subsequent block will try to double
		convert pclFlnoArr
	*/ 

        if (isNonSchedTtyp (pclTtypArr [0]) == 0)       
        {
          strcpy (pclDummy, pclFlnoArr);	
          ClrSpcInFlno (pclDummy);		/* Convert to FCS flight no */  
        } 
        else
        {
          strcpy (pclDummy, "                "); 
          memcpy (pclDummy, pclCsgnArr, strlen (pclCsgnArr));
        }
      } 
      else
      { 
        dbg (TRACE, "Handle_RAC_RKEY: Unexpected DB return code [%d]", iResArr);
        return;
      }
      
      memcpy (&fcsMsgDep [DATE_POS+DATE_LEN + 3], pclDummy, 8); 
        
      /*
	v.1.29 - add linked flight date if linked flight was not blank
	and we are configured to add it.
      */

      if ((iResArr == RC_SUCCESS) && (igAddJoinFlightDate != 0))
      {
        ilDatPos = DATE_POS + DATE_LEN + 3 + FLNO_LEN; 
        memcpy (&fcsMsgDep [ilDatPos], JOIN_FLT_DATE_CODE, JOIN_FLT_DATE_CODE_LEN);
        ilDatPos += JOIN_FLT_DATE_CODE_LEN;
        memcpy (&fcsMsgDep [ilDatPos], &pclStoaLocal [2], JOIN_FLT_DATE_LEN); /* YYMMDD */
        ilDatPos += JOIN_FLT_DATE_LEN;
        fcsMsgDep [ilDatPos] = '\0';
      }
      else if ((iResArr == RC_SUCCESS) && (igAddJoinFlightDate == 0))
      {
        fcsMsgDep [DATE_POS+DATE_LEN + 3 + FLNO_LEN] = '\0';
      }
      else if ((iResArr == RC_NOTFOUND)  && (igAddJoinFlightDate == 0))
      {
        fcsMsgDep [DATE_POS+DATE_LEN + 3 + FLNO_LEN] = '\0';
      } 
      else if ((iResArr == RC_NOTFOUND) && (igAddJoinFlightDate != 0))
      { 
        /*
           NCS request - add blank date if configured to send link flt date.  
           6 spaces for YYMMDD.
        */
        
        ilDatPos = DATE_POS + DATE_LEN + 3 + FLNO_LEN;  /* '3' is the length of '028' code */
        memcpy (&fcsMsgDep [ilDatPos], JOIN_FLT_DATE_CODE, JOIN_FLT_DATE_CODE_LEN);
        ilDatPos += JOIN_FLT_DATE_CODE_LEN;
        memcpy (&fcsMsgDep [ilDatPos], "      ", JOIN_FLT_DATE_LEN);
        ilDatPos += JOIN_FLT_DATE_LEN;
        fcsMsgDep [ilDatPos] = '\0';
      }


      if (!IsRepeatedMatch (fcsMsgDep))
        blineDataWrite (fcsMsgDep); 
    }
  }
  return;
} 

/********************************************************************************/
/* CheckJoinSplitRelease ()							*/
/* If we are now equal or more than the check interval, select eligible 	*/ 
/* arrival & departure records for force sending of joins and splits to FCS	*/
/* As this function is of similiar logic as CheckArrRelease, the WHERE clauses	*/
/* cater for IGNORE_TERMINAL & STEV						*/ 
/* Important: thought should not filter for SQ and MI. As RAC might be generated*/
/* even if FIPS updates REGN, such an update might have been filtered out 	*/
/* during online RAC update. Hence this function is relevant for those flights.	*/ 
/* Also for REGN's from FCS							*/
/* v.1.28 - CR08005 - Non scheduled flights.					*/
/********************************************************************************/

static void CheckJoinSplitRelease (void)
{
  char	pclNow [16], pclLast [16]; 
  struct tm   *ptm; 
  int ilRC = RC_DATA_ERR, res;
  short slFkt = 0;
  short slCursor = 0;
  char clSqlBuf[2048] ="\0";
  char clDataArea [2048];
  int ilNoOfRec = 0, pos; 
  char flno [16], adid [2], flda [16], urno [32], rkey [32];
  char fcsMsg [64]; 

  /*
	Make use of this function if there is ever a need to have a different 
	selection interval from the arrival release.
	SetArrRelStoa (); 
	In which case, the following line would be moved there..
  */

  tgCurr_RAC_RKEY_STOA_D = (time ('\0')) + (igMinsBeforeRacJoin * 60); 

  if ((tgCurr_RAC_RKEY_STOA_D - tgLast_RAC_RKEY_STOA_D) >= igRacJoinIntervalSecs)
  { 
    memset (pclNow, '\0', 16); 
    memset (pclLast, '\0', 16); 

    /* Note the two zeroes to implement minutes precision */
    ptm = gmtime (&tgCurr_RAC_RKEY_STOA_D);
    strftime (pclNow, 16, "%Y%m%d%H%M00", ptm);
    ptm = gmtime (&tgLast_RAC_RKEY_STOA_D);
    strftime (pclLast, 16, "%Y%m%d%H%M00", ptm); 

    tgLast_RAC_RKEY_STOA_D = tgCurr_RAC_RKEY_STOA_D;

    /*
	The select statement below will return joined departures that had
	been previously sent when their corresponding arrivals were sent.
	Handle_RAC_Join will take this into account, such that these
	departures will not be re-sent.  
    */ 

    /* if not defined in config file, no filter on terminal */
    if (!(strcmp (cgIgnoreTerminal, "?")))
      sprintf(clSqlBuf,
        "SELECT FLNO,ADID,FLDA,URNO,RKEY FROM AFTTAB WHERE " 
        "(((STOA > '%s'  AND STOA <= '%s') AND ADID = 'A') "
        "OR ((STOD > '%s'  AND STOD <= '%s') AND ADID = 'D')) "
        "AND STEV <> '%s' " 
        "ORDER BY STOD,STOA", 	/* force arrivals to be first */
          pclLast, pclNow, pclLast, pclNow, cgIgnore_STEV); 
    else
      sprintf(clSqlBuf,
      "SELECT FLNO,ADID,FLDA,URNO,RKEY FROM AFTTAB WHERE "      
      "(((STOA > '%s'  AND STOA <= '%s') AND ADID = 'A') "
      "OR ((STOD > '%s'  AND STOD <= '%s') AND ADID = 'D')) "
      "AND STEV <> '%s' " 
      "AND TGA1 <> '%s' ORDER BY STOD,STOA", 
        pclLast, pclNow, pclLast, pclNow, cgIgnore_STEV, cgIgnoreTerminal); 

    dbg(TRACE,"CheckJoinSplitRelease: <%s>",clSqlBuf);

    slFkt = START;
    slCursor = 0;
    ilRC = RC_SUCCESS;
    
    while (ilRC == RC_SUCCESS)
    { 
      ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clDataArea);  

      if (ilRC == RC_SUCCESS)
      {  
        ilNoOfRec++;
        BuildItemBuffer (clDataArea,
          "FLNO,ADID,FLDA,URNO,RKEY", 5, ","); 
        dbg (TRACE, "Rec %d:<%s>", ilNoOfRec, clDataArea);
        (void) get_real_item (rkey, clDataArea, 5); 
        if (rkey [0] != '\0') 
          Handle_RAC_RKEY (atol (rkey), 0); 
      } 
      else
      {
        if (ilRC == RC_FAIL)
        {
          dbg (TRACE, "CheckJoinSplitRelease : ORACLE ERROR !!");
          ilRC = RC_FAIL;
        }
      }
      slFkt = NEXT;
    } 
    
    commit_work();
    close_my_cursor (&slCursor);
    dbg (TRACE, "============ END - CheckJoinSplitRelease ==========");
  } 
}

/********************************************************************************/
/* GetMatch () 									*/
/* Handy for fixing a logic error in DataToFcs()'s code when sending a split 	*/
/* for an arrival								*/
/* Input : URNO of an arrival/dep, input ADID 					*/
/* Output : URNO of the matched flight, if any. Empty string if not matched.    */
/* Return:  DB error code 							*/
/* IMPORTANT: first implementation - only do arrival inputs.			*/
/********************************************************************************/

static int  GetMatch (char *pclInputUrno, char *pclInputAdid,
		char *pclOutputUrno)
{ 
  int	ilItemNo;
  long  jj;
  
  /* for DB access */
  int ilRC = RC_DATA_ERR, res;
  short slFkt = 0;
  short slCursor = 0;
  char clSqlBuf[2048] = "\0";
  char clDataArea [2048];
  int ilNoOfRec = 0;

  /* Safety to avoid DBIF error */
  if (atol (pclInputUrno) <= 0) 
  {
    dbg(TRACE,"GetMatch: Invalid input urno [%s]", pclInputUrno); 
    strcpy (pclOutputUrno, "");
    return RC_NOTFOUND;
  }

  sprintf(clSqlBuf,
      "SELECT URNO FROM AFTTAB WHERE ADID='D' and RKEY = '%8s'",
      pclInputUrno);
  
  dbg(TRACE,"SQL: [%s]", clSqlBuf);
  
  slFkt = START;
  slCursor = 0;
  ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clDataArea); 
  if (ilRC == RC_SUCCESS)
  { 
     BuildItemBuffer (clDataArea, "URNO", 1, ","); 
     get_real_item (pclOutputUrno, clDataArea, 1); 
  }
  else if (ilRC == RC_NOTFOUND)
  {
    dbg(TRACE,"GetMatch: no match for [%s]", pclInputUrno);
    strcpy (pclOutputUrno, "");
  }
  else 
  {
    dbg(TRACE,"GetMatch: SELECT failed <%d> #####",ilRC); 
    strcpy (pclOutputUrno, "");	/* or maybe "ERROR" */
  }

  commit_work();
  close_my_cursor (&slCursor); 

  return ilRC;
}

/********************************************************************************/
/* IsRepeatedMatch () 								*/
/* Input :  The formatted '028' msg to be sent to FCS				*/
/* Output:  Nothing.								*/
/* Return:  1 - was sent either in one of the two cases of:			*/
/*   a. Exactly the last msg just sent, irregardless how long ago, or		*/
/*   b. Exactly the last but one msg, but only if the last msg was not for the	*/
/*      same flight. E.g.							*/
/*   51USQ123   090519028SQ999							*/
/*   51USQ123   090519028SQ888							*/
/*   51USQ123   090519028SQ999 - must send this one because the same flt has	*/
/*                             changed. Detect by comparing first 17 char's	*/
/*			 of msg, viz, '51USQ123   090519028' to be sure it is	*/
/*			 same flight and it was a join/split.			*/
/*										*/
/*   51USQ123   090519028SQ999							*/
/*   51UBA5678  090519028BA1231							*/
/*   51USQ123   090519028SQ999 - cannot send this one				*/ 
/* 										*/
/********************************************************************************/

static int IsRepeatedMatch (char *next_msg)
{
  int	is_repeated;

  is_repeated = 0;

  if ((!strcmp (next_msg, pcgLastJoinSplit))
    || ((strncmp (next_msg, pcgLastJoinSplit, 17)) &&
      (!strcmp (next_msg, pcgLastButOneJoinSplit))))
  {
    dbg (TRACE, "Dont send [%s] - repeated within last two msgs", next_msg); 
    is_repeated = 1;
  }
  else
  {
    is_repeated = 0;
    /*
	Roll up the last two msgs by one place. IMPORTANT - MUST send the
	message otherwise subsequent check will be incorrect!  
    */
    
    strcpy (pcgLastButOneJoinSplit, pcgLastJoinSplit);
    strcpy (pcgLastJoinSplit, next_msg); 
  }

  return is_repeated;
}

/********************************************************************************/
/* HandleFogdhdMsg ()								*/
/* Create a tow message to FCS based on the AUTO_TOW of FOGDHD.			*/
/*										*/ 
/********************************************************************************/

static void HandleFogdhdMsg (char *pclCmd, char *pclSel, char *pclFld,
	char *pclData)
{
  char	pclFtyp [4];
  long	jj; 

  memset (pclFtyp, '\0', 4);
  jj = get_item_no (pclFld, "FTYP", 5) + 1;
  (void) get_real_item (pclFtyp, pclData, jj);

  if (!strcmp (pclFtyp, "T"))
  {
    if (!strcmp (pclCmd, "UFR"))
    {
      dbg (TRACE, "HandleFogdhdMsg: Cmd [%s] FTYP [%s]",
        pclCmd, pclFtyp); 
      (void) HandleTowing (pclFld, pclData, 0); 
    }
  } 
  return;
}

/************************************************************************/
/*									*/
/*	int isNonSchedTtyp - CR08005					*/
/*	Input : the ttyp character from the incoming FCS msg		*/
/*	Return value :	0 - does not match any of the non-sched char	*/
/*	codes non-zero - matches a non-sched code			*/ 
/*									*/
/************************************************************************/

int isNonSchedTtyp (char clTtyp)
{
  int jj = 0; 

  while (cgNonSchedTtyp [jj] != '\0')
  {
    if (cgNonSchedTtyp [jj] == clTtyp)
      return 1; 
    jj++; 
  }
  return 0;
}

/************************************************************************/
/*									*/
/*	iValidateOutgoingMessage 					*/
/*									*/
/*	Input : the proposed message to FCS.				*/
/*	Return value :	FALSE - does not comply with pre-count criteria	*/
/*	of FCSU. TRUE - complies. Note that the data payload could be	*/
/*	garbled but that is okay (?) as FCSU will still count it as an	*/
/*	incoming message.						*/
/*									*/
/************************************************************************/

static int iValidateOutgoingMessage (char *pcpMsg)
{
  char pclFunc[] = "iValidateOutgoingMessage: ";
  char pclBuf [256];	/* overkill, but so what */
  int  ilRC = TRUE;
  int  jj;

  /* in case so garbled no null byte even */
  memset (pclBuf, '\0', 256);
  strncpy (pclBuf,pcpMsg,255);
  
  dbg(DEBUG,"%s Checking <%s>",pclFunc,pclBuf);

  if (pclBuf [0] != '5')
  {
    dbg(TRACE,"%s Missed code 5 <%s>", pclFunc,pclBuf);
    ilRC = FALSE;
  }
  else if ((pclBuf [1] != '1') && (pclBuf [1] != '2')) 
  {
    dbg(TRACE,"%s Indicator is neither arrival nor departure <%s>", pclFunc,pclBuf);
    ilRC = FALSE;
  }
  else if (pclBuf [2] != 'U')
  { 
    dbg(TRACE,"%s Update code not found <%s>", pclFunc,pclBuf);
    ilRC = FALSE;
  }
  
  if (ilRC == TRUE)
  {
    for (jj = 3; jj <= 10; jj++)
    {
      if (!isalnum (pclBuf [jj]) && (pclBuf [jj] != ' '))
      {
        dbg(TRACE,"%s FLNO/CSGN has non-alphanum non-space char <%s>", pclFunc,pclBuf);
        ilRC = FALSE;
        break;
      }
    }
  }

  if (ilRC != TRUE)
  {
    jj = 0; 
  }
  else if ((!isdigit (pclBuf [11])) || (!isdigit (pclBuf [12])))
  {
    dbg(TRACE,"%s Scheduled Date YY invalid <%s>", pclFunc,pclBuf);
    ilRC = FALSE; 
  } 
  /* cannot do atoi of MM because e.g. "1x" returns 1 from atoi) */
  /* first posn of MM */
  else if ((pclBuf [13] != '0') && (pclBuf [13] != '1')) 
  {
    dbg(TRACE,"%s First posn of MM invalid <%s>", pclFunc,pclBuf);
    ilRC = FALSE; 
  } 
  /* second posn of MM */
  else if (!isdigit (pclBuf [14]))
  {
    dbg(TRACE,"%s Second posn of MM invalid <%s>", pclFunc,pclBuf);
    ilRC = FALSE; 
  } 
  /* special case of MM = 00 */ 
  else if ((pclBuf [13] == '0') && (pclBuf [14] == '0')) 
  {
    dbg(TRACE,"%s 00 MM invalid <%s>", pclFunc,pclBuf);
    ilRC = FALSE; 
  }
  /* ditto for DD = 00 */ 
  else if ((pclBuf [15] == '0') && (pclBuf [16] == '0')) 
  {
    dbg(TRACE,"%s 00 DD invalid <%s>", pclFunc,pclBuf);
    ilRC = FALSE; 
  } 
  /* first posn of DD */
  else if ((pclBuf [15] != '0') && (pclBuf [15] != '1')
    && (pclBuf [15] != '2') && (pclBuf [15] != '3')) 
  {
    dbg(TRACE,"%s First posn of DD invalid <%s>", pclFunc,pclBuf);
    ilRC = FALSE; 
  } 
  /* second posn of DD */
  else if (!isdigit (pclBuf [16]))
  {
    dbg(TRACE,"%s Second posn of DD invalid <%s>", pclFunc,pclBuf);
    ilRC = FALSE; 
  }
  else
  {
    jj = 0;
  }

  return ilRC; 
}

/************************************************************************/
/*									*/
/*	CheckArrBlankedBelt 					*/
/*									*/
/*	Input : fields and values from FLIGHT				*/
/*	If Belt is already in the field list, nothing more to do.	*/
/*	If belt is not present, then get AFTTAB.BLT1. If it is blank,	*/
/*	flag to add it to the field and value list.			*/
/*	Only consider arrivals.						*/
/*	SINCAG-110/UFIS-3014.						*/
/*	We cannot append to pcpFld/pdpData because they are only 	*/
/*	pointers to the CEDA message.					*/
/*									*/
/************************************************************************/

static void CheckArrBlankedBelt (char *pcpFld, char *pcpData)
{
  char pclFunc[] = "CheckArrBlankedBelt: ";

  int ilRC = RC_DATA_ERR;
  short slFkt = 0;
  short slCursor = 0;
  char clSqlBuf[2048] = "";
  char clDataArea [2048] = "";
  char pclBlt1 [32] = "";
  char pclAftUrno [32] = "";
  int  jj,kk,ilItemNo;
  
  igToAddBlankBelt = 0;

  if (strstr (pcpFld,"BLT1") != '\0')
  {
    dbg(DEBUG,"%s BLT1 is already in field list",pclFunc);
    return;
  }

  if ((strstr (pcpFld,"PSTA") == '\0') &&
      (strstr (pcpFld,"PSTD") == '\0') &&
      (strstr (pcpFld,"GTA1") == '\0') &&
      (strstr (pcpFld,"GTD1") == '\0'))
  {
    dbg(DEBUG,"%s No stand or gate in field list",pclFunc);
    return;
  }


  ilItemNo = get_item_no (pcpFld, "URNO", 5) + 1;
  if (ilItemNo < 0)
  { 
    dbg(TRACE,"%s *** URNO NOT IN FIELD LIST - CHECK action.cfg",pclFunc);
    return;
  }
  
  (void) get_real_item (pclAftUrno,pcpData,ilItemNo); 

  sprintf (clSqlBuf,
    "SELECT BLT1 FROM AFTTAB WHERE ADID='A' AND URNO='%s'", pclAftUrno);
  dbg(DEBUG,"%s <%s>",pclFunc,clSqlBuf); 
  slFkt = START;
  slCursor = 0;
  ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clDataArea); 
  if (ilRC == RC_SUCCESS)
  {
    BuildItemBuffer (clDataArea, "BLT1", 1, ",");
    (void) get_real_item (pclBlt1, clDataArea, 1);
    if (pclBlt1 [0] == '\0')
    {
       dbg(DEBUG,"%s BLT1 is blank in AFT, flag to send to FCS",pclFunc);
       igToAddBlankBelt = 1;
    } 
  }
  else if (ilRC == RC_NOTFOUND)
  {
    /* This is not an error */
  }
  else 
  {
    dbg(TRACE,"%s: SELECT failed <%d> ##########################",pclFunc,ilRC); 
  }

  commit_work();
  close_my_cursor (&slCursor); 

  return;
}

/********************************************************************************/
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

