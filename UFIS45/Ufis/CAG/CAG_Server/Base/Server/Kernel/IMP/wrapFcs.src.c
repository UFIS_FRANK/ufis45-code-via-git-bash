#ifndef _DEF_mks_version_wrapFCS
  #define _DEF_mks_version_wrapFCS
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_wrapFcs[] = "@(#) "UFIS_VERSION" $Id: Ufis/CAAS_GMS/CAAS_GMS_Server/Base/Server/Kernel/IMP/wrapFcs.src.c 1.7k 2010/07/05 17:26:40CET jim Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program                                                         */
/*                                                                            */
/* Author         : JIM                                                       */
/* Date           : 2. Nov. 2006                                              */
/* Description    : FCS to UFIS Inteface Include file                         */
/*                  This Include file reformats the FCS updates received by   */
/*                  IMPHDL via FCSHDL (with BLINE interface) and handles      */
/*                  requests from FCS                                         */
/*                                                                            */
/* Update history :                                                           */
/* 20061102 JIM: init                                                         */
/* 20061208 JIM:  added ADJUST_FIELD_LENGTH for parallel test run             */
/* 20061213 JIM:  differenc between received len and used len                 */
/* 20061213 JIM:  supress BELT                                                */
/* 20061213 JIM:  FTYP X                                                      */
/* 20061214 JIM:  complete configuration of ARR and DEP fields                */
/* 20070417 DKA:  v.1.7	Ignore REGN from FCS				      */ 
/* 20070418 DKA:  v.1.7b - Configurable acceptance of belts, stands, gates,   */
/*		  and registrations. Default setting in imphdl.cfg under      */
/*		  will be "NO" for these.				      */
/* 20070418 DKA:  v.1.7c - Use best guess for belt date (B1BA) from LAND.     */
/*		  Copy GTA1 -> PSTA if PSTA is null			      */
/*		  Copy GTD1 -> PSTD if PSTD is null			      */
/* 20070418 DKA:  v.1.7d - Implement best guess for belt date (B1BA) from     */
/*		  LAND date						      */
/* 20070514 DKA:  v.1.7e - Implement deletes. Configurable as either update	*/
/*		  FTYP to 'X' or as DFR. If the former, cannot "uncancel" via	*/
/*		  FCSHDL.  							*/
/*		  Change criteria for detecting an FCS delete to the code 'D'	*/
/*		  rather than the msg length.  					*/
/*		  Retained code for:						*/
/*		  Configurable time window within which deletes are ignored	*/
/*		  so that nightly block deletes are not processed.		*/
/*										*/
/*		  Instead implemented delete only if flda of fcs msg is equal	*/
/*		  or greater than today.				 	*/
/*										*/
/*		  Configurable FTYP for the pseudo-delete, default is 'X'	*/
/* 20071123 DKA:  v.1.7f - CR07020						*/
/*		  Add lookup function check if a flight exists to decide if 	*/
/*		  if an insert or update. Based on this, accept or ignore	*/
/*		  terminal from FCS. Prepend "- " to TGA1/TGD1 for this.	*/
/* 20080205 DKA:  v.1.7g - If FCS sends ATA w/o ONBL, copy ATA to ONBL only if  */
/*		  DB record ONBL is empty.					*/
/*		  Upgrade FlightInAFT() to return a field's value.		*/
/* 20080226 DKA:  v.1.7h - since FLIGHT does not for whatever reason		*/
/*		  delete towings when an arrival is deleted, we got to send	*/
/*		  explicit DFR for towing to FLIGHT...				*/
/* 20080331 DKA:  v.1.7i - explicit check for daily departure in wrapFCSData	*/
/*		  so unexpected timetable records from FCS will not be  treated */
/*		  as normal updates.						*/
/* 20080402 DKA:  strncpy() 6 bytes of FCS SDATE to avoid getting an invalid	*/
/*		  date in HandleFCSDelete ().					*/
/* 20080908 DKA:  v.1.7j - strip the suffix and send it to FLIGHT as FLNS.	*/
/*		  Should not need to change saving algorithm into FCSTAB	*/
/*		  (imphdl.c) as that is probably done after FLNO has been	*/
/*		  created.							*/
/* 20100504 DKA:  v.1.7k - CR08005 (a.k.a. CR07006) - Non Scheduled flights.    */
/*		  FCS sends callsigns as the flight no.				*/
/*		  Identify them via TTYP's. 					*/
/*		  DSSF is sent to FLIGHT only for inserts of non-sched to fool 	*/
/*		  FLIGHT, and never for updates.				*/
/*		  As there is no translation of flight no., FCSTAB is not	*/
/*		  used. (Corresponding change is needed in FCSHDL).		*/
/* 20100705 DKA:  Bugfix for non sched joins without REGN			*/
/*										*/
/********************************************************************************/

#define CFG_ENTRY_LENGTH 124
#define N_MAX_NON_SCHED_TTYP	12

int   igArrFixMax= 0;            /* number of known fix format fields */
int   igDepFixMax= 0;            /* number of known fix format fields */
int   igIgnoreFromTo= FALSE; /* never trust FCS for VPFR, VPTO and FREQ */
char *pcgFCSOutF;      /* global pointer to the pclFldOut */
char *pcgFCSOutD;      /* global pointer to the pclDatOut */
char *pcgFCSSel;       /* global pointer to the pclSelOut */
char *pcgFCSCmd;       /* global pointer to the pclCmdOut */
char *pcgFCSDat;       /* global pointer to the pclCmdOut */
int  igRealDelete;	/* global, flag for deleting by DFR or UFR (FTYP->'X') */
char pcgDeleteCode [2]; /* global FTYP when a delete is received */ 
int  igArrTtypPos = -1; /* ver.1.7k CR08005 */
int  igDepTtypPos = -1; /* ver.1.7k CR08005 */
char cNonSchedTtyp [N_MAX_NON_SCHED_TTYP]; /* ver.1.7k CR08005, overkill */
int  igIsNonSched;	/* global flag set on test of incoming FCS update msg */
/* int  igIgnoreDelStart; */  /* global, window start for ignoring deletes */ 
/* int  igIgnoreDelEnd; */	/* global, window end for ignoring deletes */

int  igAllowEmptyMFF= 0;  /* empty MFF will result in error */

typedef struct _FCSToAftFix {/* structure for fixed field definitions */
   char Name[16];             /* UFIS name of field */
   int Start;                 /* fixed value start in record */
   int Len;                   /* fixed value length in record */
   int Type;                  /* Date: add 2000, else ok */
   int ReceivedLen;           /* Received length */
} FCS_TO_AFT_FIX;
#define NORMAL_STR 0
#define DATE_STR   1
#define TIME_STR   2
#define VIA_STR    3

static int igMaxFormats= 3;
static char *pcgFormats[] = {"NORMAL_STR", "DATE_STR", "TIME_STR", "VIA_STR"} ;

/* the following array is used in "GetFixField" to fill ".Value" for arrival flights */
static FCS_TO_AFT_FIX pcgArrFixFields[] = { /* array of fixed field definitions */
  {"FLNO"            ,  3,  8, NORMAL_STR, 8 },    /* flight number, FLNO begins at pos.  2, length 10 */
  {"FLDU"            , 11,  6,   DATE_STR, 6 },   /* flight day, FLDA begins at pos. 12, length  6 */
  {"VPFR"            , 17,  6,   DATE_STR, 6 },   /* valid period from, VPFR begins at pos. 17, length  6 */
  {"VPTO"            , 23,  6,   DATE_STR, 6 },   /* valid period to, VPTO begins at pos. 23, length  6 */
  {"FREQ"            , 29,  7, NORMAL_STR, 7 },   /* days, 1-7, 1=Mon, FREQ begins at pos. 29 */
/*{"*STOA"           , 11,  6,   DATE_STR, 6 }, * flight day, FLDA begins at pos. 12, length  6 */
  {"STOA"            , 36,  4,   TIME_STR, 4 },    /* time part of STOA */
  {"- Handler "      , 40,  4, NORMAL_STR, 4 },    /* Handler, ALTTAB => HAGTAB ?*/
  {"- TGA1"          , 44,  1, NORMAL_STR, 1 },    /* Terminal 1,2,3 or 4 */
/*{"ACT"             , 45,  8, NORMAL_STR, 8 },    /* Aircraft Type, may be no ICAO or IATA code */
  {"ACT5"            , 45,  5, NORMAL_STR, 8 },    /* Aircraft Type, may be no ICAO or IATA code */
  {"TTYP"            , 53,  1, NORMAL_STR, 1 },    /* Nature */
  {"JFLN"            , 54,  8, NORMAL_STR, 8 },   /* Corresponding flight number */
  {"ORG3"            , 62,  3, NORMAL_STR, 5 },    /* Origin, IATA code */
  {"*VIAL1"          , 67,  5,    VIA_STR, 5 },   /* Vial list */
  {"*VIAL2"          , 72,  5,    VIA_STR, 5 },   /* Vial list */
  {"*VIAL3"          , 77,  5,    VIA_STR, 5 },   /* Vial list */
  {"*VIAL4"          , 82,  5,    VIA_STR, 5 },   /* Vial list */
  {"*VIAL5"          , 87,  5,    VIA_STR, 5 },   /* Vial list */
  {"*VIAL6"          , 92,  5,    VIA_STR, 5 },   /* Vial list */
  {"VIAL"            , 97,  5,    VIA_STR, 5 },   /* Vial list */
  {"*ETOA"           ,106,  6,   DATE_STR, 6 },  /* Date part estimated time */
  {"ETOA"            ,102,  4,   TIME_STR, 4 },   /* Time part estimated time */
  {"*LAND"           ,116,  6,   DATE_STR, 6 },  /* Date part actual time */
  {"LAND"            ,112,  4,   TIME_STR, 4 },   /* Time part actual time */
  {"- *HANG"         ,126,  6,   DATE_STR, 6 },  /* Date part actual time */
  {"- HANG"          ,122,  4, NORMAL_STR, 4 },  /* Time part actual time */
  {"- *ONBS"           ,136,  6,   DATE_STR, 6 },  /* Date part actual time */
  {"- ONBS"            ,132,  4,   TIME_STR, 4 },   /* Time part actual time */
  {"- In Terminal T" ,142,  4, NORMAL_STR, 4 },  /* time part in termin time */
  {"- BLT1"          ,146,  2, NORMAL_STR, 2 },   /* time part in termin time */
  {"- BLT2"          ,148,  2, NORMAL_STR, 2 },   /* time part in termin time */
  {"*B1BA"           ,158,  6,   DATE_STR, 6 },  /* Date part first bag time */ 
  {"B1BA"            ,150,  4,   TIME_STR, 4 },   /* Time part first bag time */
  {"*B1EA"           ,158,  6,   DATE_STR, 6 },  /* Date part last bag time */
  {"B1EA"            ,154,  4,   TIME_STR, 4 },   /* Time part last bag time */
  {"- GTA1"          ,164,  4, NORMAL_STR, 4 },   /* Gate (phone by Dilip 20061122: shall not be sent by FCS )*/
  {"- prev Gate"     ,168,  4, NORMAL_STR, 4 },  /* */
  {"- sched Gate"    ,172,  4, NORMAL_STR, 4 },  /* */
  {"- PSTA"          ,176,  4, NORMAL_STR, 4 },   /* Position (phone by Dilip 20061122: shall not be sent by FCS )*/
  {"- prev pos"      ,180,  4, NORMAL_STR, 4 },  /* */
  {"- sched pos"     ,184,  4, NORMAL_STR, 4 },  /* */
  {"REMP"            ,188,  2, NORMAL_STR, 2 },  /* Remark code */
  {"- remark history",190, 14, NORMAL_STR,14 },  /* */
  {"- Configuration" ,204, 12, NORMAL_STR,12 },  /* */
  {"- REGN"            ,216,  7, NORMAL_STR, 7 },  /* Registration */ 	/* Ignore per discussion during cutover attempt 20070417 */
  {"- master"        ,223,  8, NORMAL_STR, 8 },  /* Code shares */
  {"- next pool"     ,231,  8, NORMAL_STR, 8 },  /* Code shares */
};

/* the following array is used in "GetFixField" to fill ".Value" for departure flights */
static FCS_TO_AFT_FIX pcgDepFixFields[] = { /* array of fixed field definitions */
  {"FLNO"            ,  3,  8, NORMAL_STR, 8 },    /* flight number, FLNO begins at pos.  2, length 10 */
  {"FLDU"            , 11,  6,   DATE_STR, 6 },   /* flight day, FLDA begins at pos. 12, length  6 */
  {"VPFR"            , 17,  6,   DATE_STR, 6 },   /* valid period from, VPFR begins at pos. 17, length  6 */
  {"VPTO"            , 23,  6,   DATE_STR, 6 },   /* valid period to, VPTO begins at pos. 23, length  6 */
  {"FREQ"            , 29,  7, NORMAL_STR, 7 },   /* days, 1-7, 1=Mon, FREQ begins at pos. 29 */
/*{"*STOA"           , 11,  6,   DATE_STR, 6 }, * flight day, FLDA begins at pos. 12, length  6 */
  {"STOD"            , 36,  4,   TIME_STR, 4 },    /* time part of STOA */
  {"- Handler "      , 40,  4, NORMAL_STR, 4 },    /* Handler, ALTTAB => HAGTAB ?*/
  {"- TGD1"          , 44,  1, NORMAL_STR, 1 },    /* Terminal 1,2,3 or 4 */
/*{"ACT"             , 45,  8, NORMAL_STR, 8 },    /* Aircraft Type, may be no ICAO or IATA code */
  {"ACT5"            , 45,  5, NORMAL_STR, 8 },    /* Aircraft Type, may be no ICAO or IATA code */
  {"TTYP"            , 53,  1, NORMAL_STR, 1 },    /* Nature */
  {"JFLN"            , 54,  8, NORMAL_STR, 8 },   /* Corresponding flight number */
  {"DES3"            , 62,  3, NORMAL_STR, 5 },    /* Origin, IATA code */
  {"*VIAL1"          , 67,  5,    VIA_STR, 5 },   /* Vial list */
  {"*VIAL2"          , 72,  5,    VIA_STR, 5 },   /* Vial list */
  {"*VIAL3"          , 77,  5,    VIA_STR, 5 },   /* Vial list */
  {"*VIAL4"          , 82,  5,    VIA_STR, 5 },   /* Vial list */
  {"*VIAL5"          , 87,  5,    VIA_STR, 5 },   /* Vial list */
  {"*VIAL6"          , 92,  5,    VIA_STR, 5 },   /* Vial list */
  {"VIAL"            , 97,  5,    VIA_STR, 5 },   /* Vial list */
  {"*ETOD"           ,106,  6,   DATE_STR, 6 },  /* Date part estimated time */
  {"ETOD"            ,102,  4,   TIME_STR, 4 },  /* Time part estimated time */
  {"*AIRB"           ,116,  6,   DATE_STR, 6 },  /* Date part actual time */
  {"AIRB"            ,112,  4,   TIME_STR, 4 },  /* Time part actual time */
  {"- *HANG"         ,126,  6,   DATE_STR, 6 },  /* Date part actual time */
  {"- HANG"          ,122,  4, NORMAL_STR, 4 },  /* Time part actual time */
  {"- *GD1X"         ,152,  6,   DATE_STR, 6 },  /* Time part gate close (mail by Dilip 20061128: shall not be sent by FCS )*/
  {"- GD1X"          ,132,  4,   TIME_STR, 4 },  /* Time part gate open (mail by Dilip 20061128: shall not be sent by FCS )*/
  {"- *Gate cls  d"  ,152,  6, NORMAL_STR, 6 },  /* Time part gate close */
  {"- Gate brd  t"   ,136,  4,   TIME_STR, 4 },  /* Time part gate boarding */
  {"- *FCAL"         ,152,  6,   DATE_STR, 6 },  /* Time part gate close */
  {"- FCAL"          ,140,  4,   TIME_STR, 4 },  /* Time part last call 1*/
  {"- *Gate cls  d"  ,152,  6, NORMAL_STR, 6 },  /* Time part gate close */
  {"- last call 2"   ,144,  4,   TIME_STR, 4 },  /* Time part last call 2*/
  {"- *GD1Y"         ,152,  6,   DATE_STR, 6 },  /* Time part gate close (mail by Dilip 20061128: shall not be sent by FCS )*/
  {"- GD1Y"          ,148,  4,   TIME_STR, 4 },  /* Time part gate close (mail by Dilip 20061128: shall not be sent by FCS )*/
  {"*OFBS"           ,162,  6,   DATE_STR, 6 },  /* Date part actual time */
  {"OFBS"            ,158,  4,   TIME_STR, 4 },  /* Time part actual time */
  {"- Spur"          ,168, 12, NORMAL_STR,12 },  /* Spur */
  {"- GTD1"          ,180,  4, NORMAL_STR, 4 },  /* Gate (phone by Dilip 20061122: shall not be sent by FCS )*/
  {"- prev Gate"     ,184,  4, NORMAL_STR, 4 },  /* */
  {"- sched Gate"    ,188,  4, NORMAL_STR, 4 },  /* */
  {"- PSTD"          ,192,  4, NORMAL_STR, 4 },  /* Position (phone by Dilip 20061122: shall not be sent by FCS )*/
  {"- prev pos"      ,196,  4, NORMAL_STR, 4 },  /* */
  {"- sched pos"     ,200,  4, NORMAL_STR, 4 },  /* */
  {"CKIF"            ,204,  4, NORMAL_STR, 4 },  /* receiving 8 Chars! */
  {"CKIT"            ,208,  4, NORMAL_STR, 4 },  /* receiving 8 Chars! */
  {"- sched CKIF"    ,212,  8, NORMAL_STR, 8 },  /* */
  {"REMP"            ,220,  2, NORMAL_STR, 2 },  /* Remark code */
  {"- remark history",222, 14, NORMAL_STR,14 },  /* */
  {"- Configuration" ,236, 12, NORMAL_STR,12 },  /* */
  {"- REGN"            ,248,  7, NORMAL_STR, 7 },  /* Registration */	/* Ignore per discussion during cutover attempt 20070417 */
  {"- master"        ,255,  8, NORMAL_STR, 8 },  /* Code shares */
  {"- next pool"     ,263,  8, NORMAL_STR, 8 },  /* Code shares */
};

/******************************************************************************/
/* Function prototypes 								*/
/******************************************************************************/
void AdjustFixedArrayField (FCS_TO_AFT_FIX *, int, char *, char *); 
int  HandleFCSDelete (char *pclData, char *pclCmd, char *pclSelOut,
	char *pclFldOut,char *pclDatOut,
	char *pcRecvName, char *pcDestName, char *pcTwStart, char *pcTwEnd);
int  FlightInAFT (char *flno, char *flda, char *adid, long *pfound, 
	char *clFieldName, char *clFieldValue);
int  NonSchedFlightInAFT (char *csgn, char *flda, char *adid, long *pfound,
	char *clFieldName, char *clFieldValue);
void new_parse_fcs_flno (
	char *fcs,
	char *ufis_flno,
	char *ufis_flns); 
int getFieldPosition (FCS_TO_AFT_FIX *pclFixFields, int ipFixMax,
  char *pcFieldName);
int isNonSchedTtyp (char clTtyp);
/******************************************************************************/
/* Routine: FcsReadConfigDefault                                                 */
/*          read configuration, use default if keyword not found              */
/******************************************************************************/
static void FcsReadConfigDefault(char *pcpSection,char *pcpKeyword, 
                             char *pcpCfgBuffer, char *pcpDefault)
{
 int ilRc = RC_SUCCESS;

 char clSection[124] = "\0";
 char clKeyword[124] = "\0";

   strcpy(clSection,pcpSection);
   strcpy(clKeyword,pcpKeyword);

   ilRc = iGetConfigEntry(cgConfigFile,clSection,clKeyword,
	      CFG_STRING,pcpCfgBuffer);
   if(ilRc != RC_SUCCESS)
   {
     strcpy(pcpCfgBuffer, pcpDefault);
     dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
     dbg(TRACE,"using default <%s> for <%s>",pcpCfgBuffer,clKeyword);
   } 
   else
   {
      dbg(DEBUG,"Config Entry <%s>,<%s>:<%s> found in %s",
          clSection, clKeyword ,pcpCfgBuffer, cgConfigFile);
   }/* end of if */
}

/******************************************************************************/
/* The AdjustFCS_Length routine                                               */
/* This routine loops through the FCS fields name of the array sgFCSToAft and */
/* adjusts the start positions, if some field lengths changes, i.e. if FLTN   */
/* only is received with 6 chars instead of 8                                 */
/******************************************************************************/
void AdjustFCS_Length(
       FCS_TO_AFT_FIX *pclFixFields, /* arrival or departure FCS structure */
       int ipFixMax,                 /* number of elements od ""  */
       char *pcpAdjustLenList,       /* list like "FLNO:6,xxxx:5" */
       char *pcpFlag)                /* debugging "arr" or "dep"  */
{
  int ilCurr, ilFound, ilNewLen, ilDiffLen;
  char *pclAdjName, *pclAdjNext, *pclNewLen;
  char pclFieldLenList[CFG_ENTRY_LENGTH] = "\0";

  strcpy(pclFieldLenList,pcpAdjustLenList);
  pclAdjName=strtok(pclFieldLenList,",");
  while (pclAdjName!=NULL)
  {
    pclAdjNext=strtok(NULL,","); /* remember for later */
    if (pclAdjNext!=NULL)
    {
      pclAdjNext++;
    }
    pclNewLen=strchr(pclAdjName,':');
    if (pclNewLen==NULL)
    {
      dbg(TRACE,"AdjustFCS_Length: format error for %s <%s>: no ':'", pcpFlag, pclAdjName);
      pclAdjName=pclAdjNext;
      continue;
    }
    else
    {
      *pclNewLen= 0; /* terminate name */
      pclNewLen++;   /* here begins new length */
    }
    if (strlen(pclAdjName)==0)
    {
      pclAdjName=pclAdjNext;
      continue;
    }
    ilNewLen=atoi(pclNewLen);
    dbg(TRACE,"AdjustFCS_Length: new len of <%s>: <%d>", pclAdjName, ilNewLen);
    ilFound= FALSE;
    for (ilCurr= 0; ilCurr < ipFixMax ; ilCurr++)
    {
      if (ilFound==FALSE)
      {
        if (strcmp(pclFixFields[ilCurr].Name,pclAdjName) == 0)
        {
          ilDiffLen= ilNewLen - pclFixFields[ilCurr].ReceivedLen;
          dbg(TRACE,"AdjustFCS_Length: found <%s>: Length: old <%d> new <%d> diff <%d>", 
                     pclFixFields[ilCurr].Name,pclFixFields[ilCurr].ReceivedLen,ilNewLen,ilDiffLen);
          pclFixFields[ilCurr].ReceivedLen= ilNewLen;
          if (pclFixFields[ilCurr].Len > ilNewLen)
          {
            pclFixFields[ilCurr].Len= ilNewLen;
          }
          ilFound= TRUE;
        }
      }
      else
      {
        pclFixFields[ilCurr].Start= pclFixFields[ilCurr].Start + ilDiffLen;
          dbg(TRACE,"AdjustFCS_Length: new pos of <%s>: <%d> ", 
                     pclFixFields[ilCurr].Name,pclFixFields[ilCurr].Start);
      }
    }
    pclAdjName=pclAdjNext;
  }
}

/******************************************************************************/
/* The AdjustFCS_Position routine                                               */
/* This routine loops through the FCS fields name of the array sgFCSToAft and */
/* adjusts the start positions, if some field lengths changes, i.e. if FLTN   */
/* only is received with 6 chars instead of 8                                 */
/******************************************************************************/
void AdjustFCS_Position(
       FCS_TO_AFT_FIX *pclFixFields, /* arrival or departure FCS structure */
       int ipFixMax,                 /* number of elements od ""  */
       char *pcpAdjustPosList,       /* list like "FLNO:6,xxxx:5" */
       char *pcpFlag)                /* debugging "arr" or "dep"  */
{
  int ilCurr, ilFound, ilNewPos, ilDiffPos;
  char *pclAdjName, *pclAdjNext, *pclNewPos;
  char pclFieldPosList[CFG_ENTRY_LENGTH] = "\0";

  strcpy(pclFieldPosList,pcpAdjustPosList);
  pclAdjName=strtok(pclFieldPosList,",");
  while (pclAdjName!=NULL)
  {
    pclAdjNext=strtok(NULL,","); /* remember for later */
    if (pclAdjNext!=NULL)
    {
      pclAdjNext++;
    }
    pclNewPos=strchr(pclAdjName,':');
    if (pclNewPos==NULL)
    {
      dbg(TRACE,"AdjustFCS_Position: format error for %s <%s>: no ':'", pcpFlag, pclAdjName);
      pclAdjName=pclAdjNext;
      continue;
    }
    else
    {
      *pclNewPos= 0; /* terminate name */
      pclNewPos++;   /* here begins new length */
    }
    if (strlen(pclAdjName)==0)
    {
      pclAdjName=pclAdjNext;
      continue;
    }
    ilNewPos=atoi(pclNewPos);
    dbg(TRACE,"AdjustFCS_Position: new position of <%s>: <%d>", pclAdjName, ilNewPos);
    ilFound= FALSE;
    for (ilCurr= 0; ilCurr < ipFixMax ; ilCurr++)
    {
      if (ilFound==FALSE)
      {
        if (strcmp(pclFixFields[ilCurr].Name,pclAdjName) == 0)
        {
          ilDiffPos= ilNewPos - pclFixFields[ilCurr].Start;
          dbg(TRACE,"AdjustFCS_Position: found <%s>: Position: old <%d> new <%d> diff <%d>", 
                     pclFixFields[ilCurr].Name,pclFixFields[ilCurr].Start,ilNewPos,ilDiffPos);
          ilFound= TRUE;
        }
      }
      else
      {
        pclFixFields[ilCurr].Start= pclFixFields[ilCurr].Start + ilDiffPos;
          dbg(TRACE,"AdjustFCS_Position: new pos of <%s>: <%d> ", 
                     pclFixFields[ilCurr].Name,pclFixFields[ilCurr].Start);
      }
    }
    pclAdjName=pclAdjNext;
  }
}

/******************************************************************************/
/* The AdjustFCS_Fields routine                                               */
/* This routine loops through the FCS fields given as parameter and reads new */
/* start position and new length for the fields in that list. All following   */
/* fields are removed from preconfigured list!                                */
/******************************************************************************/
/*
 * [FCS]
 * ARR_FIELDS=FLNO,FLDU,VAFR,FATO,FREQ,..
 * DEP_FIELDS=FLNO,FLDU,VAFR,FATO,FREQ,..
 *
 * ARR_FLNO=3,8
 * ARR_FLDU=11,8
 *
 * DEP_FLNO=3,8
 * DEP_FLDU=11,8
 * [FCS_END]

 * AdjustFCS_Fields(pcgArrFixFields, &igArrFixMax, clConfig, "ARR");
 * AdjustFCS_Fields(pcgDepFixFields, &igDepFixMax, clConfig, "DEP");
*/
int AdjustFCS_Fields(
       FCS_TO_AFT_FIX *pclFixFields, /* arrival or departure FCS structure */
       int *ipFixMax,                 /* number of elements od ""  */
       char *pcpAdjustList,          /* list like "FLNO,FLDU,VAFR,FATO,FREQ,.." */
       char *pcpFlag)                /* debugging "arr" or "dep"  */
{
  int ilCurr, ilNewLen, ilNewType, ilNewStart, ilCurrTyp;
  char *pclAdjName, *pclAdjNext, *pclNewLen, *pclNewTyp;
  char pclFieldList[CFG_ENTRY_LENGTH] = "\0";
  char pclFieldParam[CFG_ENTRY_LENGTH] = "\0";

  strcpy(pclFieldList,pcpAdjustList);
  pclAdjName=pclFieldList;
  ilCurr= 0;

  while (pclAdjName!=NULL && strlen(pclAdjName)>0 )
  {
    pclAdjNext=strchr(pclAdjName,',');
    if (pclAdjNext!=NULL)
    {
      *pclAdjNext= 0;
      pclAdjNext++;
    }
    if (strlen(pclAdjName)>=sizeof(pclFixFields[ilCurr].Name))
    {
      dbg(TRACE,"AdjustFCS_Fields: format error for %s <%s>: name to long, skipped!", pcpFlag, pclAdjName);
      pclAdjName=pclAdjNext;
      continue;
    }
    sprintf(pclFieldParam,"%s_%s",pcpFlag,pclAdjName);
    FcsReadConfigDefault("FCS",pclFieldParam,pclFieldParam,"");
    pclNewLen=strchr(pclFieldParam,',');
    if (pclNewLen==NULL)
    {
      dbg(TRACE,"AdjustFCS_Fields: format error for %s <%s>: no ',' before length, skipped!", pcpFlag, pclAdjName);
      pclAdjName=pclAdjNext;
      continue;
    }
    else
    {
      *pclNewLen= 0; /* terminate start pos */
      pclNewLen++;   /* here begins new length */
      ilNewStart=atoi(pclFieldParam);
    }
    pclNewTyp=strchr(pclNewLen,',');
    if (pclNewTyp==NULL)
    {
      dbg(TRACE,"AdjustFCS_Fields: format error for %s <%s>: no ',' before type, skipped!", pcpFlag, pclAdjName);
      pclAdjName=pclAdjNext;
      continue;
    }
    else
    {
      *pclNewTyp= 0; /* terminate start pos */
      pclNewTyp++;   /* here begins new length */
      ilNewLen=atoi(pclNewLen);
    }

    ilNewType= -1;
    for (ilCurrTyp= 0 ; ilCurrTyp <= igMaxFormats ; ilCurrTyp++)
    {
      if (strncmp(pclNewTyp,pcgFormats[ilCurrTyp],strlen(pcgFormats[ilCurrTyp]))==0)
      {
        ilNewType= ilCurrTyp;
      }
    }

    if (ilNewType==-1)
    {
      dbg(TRACE,"AdjustFCS_Fields: added %s <%s>: no valid type, skipped!", pcpFlag, pclAdjName);
    }
    else 
    {
      strcpy(pclFixFields[ilCurr].Name,pclAdjName);
      pclFixFields[ilCurr].Start= ilNewStart; /* terminated above */
      pclFixFields[ilCurr].Len= ilNewLen;
      pclFixFields[ilCurr].Type= ilNewType;
      ilCurr++;
      *ipFixMax= ilCurr; /* after first entry preconfigured values are unavailable */
    }
    pclAdjName=pclAdjNext;
  }
  if (ilCurr>0)
  {
    for (ilCurr=0 ; ilCurr < *ipFixMax; ilCurr++)
    {
      dbg(TRACE,"AdjustFCS_Fields: added %s <%s>: Start/Len <%d/%d,%s>", pcpFlag, 
                 pclFixFields[ilCurr].Name,pclFixFields[ilCurr].Start,
                 pclFixFields[ilCurr].Len,pcgFormats[pclFixFields[ilCurr].Type]);
    }
    dbg(TRACE,"AdjustFCS_Fields: %s: ==> only above listed fields are valid! <== ", pcpFlag);
  }
  return ilCurr;
}

/******************************************************************************/
/* Routine: Init_FCS
   first initialization (a second initialization is done on first queue 
   event (for debugging)) 
*/
/******************************************************************************/
void Init_FCS( void )
{ 
 char clConfig[CFG_ENTRY_LENGTH] = "\0";
 char clAdjustLen[CFG_ENTRY_LENGTH] = "\0";
 char clAdjustPos[CFG_ENTRY_LENGTH] = "\0";

 /* Added 20070418 - DKA - for configurable acceptance of belt,stand,gate,regn */ 
 int ipFixMax;
 int ilCurr, jj, kk;



    dbg(TRACE,"%s",mks_version_wrapFcs);
    igArrFixMax = sizeof(pcgArrFixFields) / sizeof(FCS_TO_AFT_FIX);
    igDepFixMax = sizeof(pcgDepFixFields) / sizeof(FCS_TO_AFT_FIX);
    dbg(TRACE,"Init_FCS: number of FCS fields: <%d>",igArrFixMax);
    FcsReadConfigDefault("FCS","IGNORE_FCS_FROM_TO",clConfig,"YES");
    if ((clConfig[0]=='Y') || (clConfig[0]=='y'))
    {
      igIgnoreFromTo= TRUE;
    }
    FcsReadConfigDefault("FCS","ADJUST_FIELD_LENGTH",clAdjustLen,"");
    FcsReadConfigDefault("FCS","ADJUST_POSITION",clAdjustPos,"");

    FcsReadConfigDefault("FCS","ARR_FIELDS",clConfig,"");
    if (AdjustFCS_Fields(pcgArrFixFields, &igArrFixMax, clConfig, "ARR") == 0)
    {
      AdjustFCS_Length(pcgArrFixFields, igArrFixMax, clAdjustLen, "arr");
    }
    else
    {
      dbg(TRACE,"Init_FCS: ARR_FIELDS instead of ADJUST_FIELD_LENGTH used....");
      AdjustFCS_Position(pcgArrFixFields, igArrFixMax, clAdjustPos, "arr");
    }
    FcsReadConfigDefault("FCS","DEP_FIELDS",clConfig,"");
    if (AdjustFCS_Fields(pcgDepFixFields, &igDepFixMax, clConfig, "DEP") == 0)
    {
      AdjustFCS_Length(pcgDepFixFields, igDepFixMax, clAdjustLen, "dep");
    }
    else
    {
      dbg(TRACE,"Init_FCS: DEP_FIELDS instead of ADJUST_FIELD_LENGTH used....");
      AdjustFCS_Position(pcgDepFixFields, igDepFixMax, clAdjustPos, "dep");
    }

    /*
	200770418 - DKA - Configurable acceptance of belt, parking stand, and
	gates. Registration acceptance also configurable. Only need to hack the
	format of the UFIS db field name in the array. The default behaviour
	hard-coded in the array is "NO". Too much work to make each field 
	configurable at this stage.
    */

    FcsReadConfigDefault("FCS","ACCEPT_FCS_BELT",clConfig,"NO");
    if ((clConfig[0]=='Y') || (clConfig[0]=='y'))
    { 
	/* Need only change the arrival array */
	AdjustFixedArrayField (pcgArrFixFields ,igArrFixMax, "BLT1", "arrival");
	AdjustFixedArrayField (pcgArrFixFields ,igArrFixMax, "BLT2", "arrival");
    }

    FcsReadConfigDefault("FCS","ACCEPT_FCS_GATE",clConfig,"NO");
    if ((clConfig[0]=='Y') || (clConfig[0]=='y'))
    { 
	AdjustFixedArrayField (pcgArrFixFields ,igArrFixMax, "GTA1", "arrival");
	AdjustFixedArrayField (pcgDepFixFields ,igDepFixMax, "GTD1", "departure");
    }

    FcsReadConfigDefault("FCS","ACCEPT_FCS_PARKSTAND",clConfig,"NO");
    if ((clConfig[0]=='Y') || (clConfig[0]=='y'))
    { 
	AdjustFixedArrayField (pcgArrFixFields ,igArrFixMax, "PSTA", "arrival");
	AdjustFixedArrayField (pcgDepFixFields ,igDepFixMax, "PSTD", "departure");
    }
  
    FcsReadConfigDefault("FCS","ACCEPT_FCS_REGIST",clConfig,"NO");
    if ((clConfig[0]=='Y') || (clConfig[0]=='y'))
    { 
	AdjustFixedArrayField (pcgArrFixFields ,igArrFixMax, "REGN", "arrival");
	AdjustFixedArrayField (pcgDepFixFields ,igDepFixMax, "REGN", "departure");
    } 

    FcsReadConfigDefault("FCS","REAL_DELETES",clConfig,"NO");
    if ((clConfig[0]=='Y') || (clConfig[0]=='y'))
    { 
	igRealDelete = 1;
	dbg(TRACE,"InitFCS: Will handle deletes as DFR");
    } 
    else
    {
	igRealDelete = 0;
	dbg(TRACE,"InitFCS: Will handle deletes as UFR of FTYP -> X");
    }

    FcsReadConfigDefault("FCS","DELETE_FTYP", pcgDeleteCode, "X"); 

    /*
	CR08005 - v.1.7k - At this point, the two FCS_TO_AFT_FIX arrays have
	been modified in above lines using imphdl.cfg entries. We now find &
	save the modified position of the ttyp code for quick detection of
	flight type in GetFCSToAft (). 
    */

    igArrTtypPos = getFieldPosition (pcgArrFixFields ,igArrFixMax, "TTYP");
    igDepTtypPos = getFieldPosition (pcgDepFixFields ,igDepFixMax, "TTYP");

    FcsReadConfigDefault("FCS", "NON_SCHED_TTYP", clConfig, "N"); 
    jj = 0; kk = 0;
    while (clConfig [jj] != '\0')
    {
      if (clConfig [jj] == ',')
      {
        jj++;
        continue;
      }
      cNonSchedTtyp [kk] = clConfig [jj];
      dbg(TRACE, "Extracted Non-Sched TTYP [%c]", cNonSchedTtyp [kk]);
      kk++;jj++;
    } 
    /* Null byte is used to detect end of matches in isNonSchedTtyp () */
    cNonSchedTtyp [kk] = '\0'; 

    /*
    FcsReadConfigDefault("FCS","IGNORE_DELETES_WINDOW_START", clConfig,"0030");
    igIgnoreDelStart = atoi (clConfig); 
    FcsReadConfigDefault("FCS","IGNORE_DELETES_WINDOW_END", clConfig,"0400"); 
    igIgnoreDelEnd = atoi (clConfig); 
    dbg(TRACE,
	"Init_FCS: Ignore deletes after [%.4dh] and before [%.4dh]",
	igIgnoreDelStart, igIgnoreDelEnd); 
    */
}


/******************************************************************************/
/* The GetFCSToAft routine                                                   */
/* This routine loops throug the FCS fields name of the array sgFCSToAft and */
/* copies fields and values.                                                 */
/*									*/ 
/******************************************************************************/
void GetFCSToAft(FCS_TO_AFT_FIX *pclFixFields, int ipFixMax, char *cpAdid)
{
  char pclDummy[124];	
  char pclDummy2[16], pclBeltTime[32];
  static char pclLandDate [32], pclLandTime [32];  /* Stored values needed */
						  /* when evaluating B1BA time */
  static char pclLandDT [32];			/* redundant needed for v.1.7g */
  static char pclOnBlockDT [32];

  static char pclGTA1 [8], pclGTD1 [8]; 
  int  iLandTime, iBeltTime; 
  /* Flwg for CR07020 - for accepting FCS terminal only at creation	*/
  /* No need for 'clFlda' because can re-use pclFldu below		*/
  char clFlno [16], clTerm [8], clTermFieldName [8];
  long found;

  char pclWhereHlp[100];
  char pclFldu[16];
  char pclAlc[4];
  char pclFltn[12];
  char pclFlnoTmp [32];
  char pclFlns [2];
  int ilCurr;
  int ilAddFtypX = FALSE;
  int ilTotalRecLen = strlen(pcgFCSDat);
  char clCsgn [12];

  for (ilCurr= 0; ilCurr < ipFixMax ; ilCurr++)
  {
    if (pclFixFields[ilCurr].Start + pclFixFields[ilCurr].Len > ilTotalRecLen )
    {
      dbg(TRACE,"GetFixField: position  + length of <%s> exceeded total received length <%d>",
                 pclFixFields[ilCurr].Name,ilTotalRecLen);
      dbg(TRACE,"GetFixField: rest ignored!");
      break;
    }

    if (pclFixFields[ilCurr].Type == DATE_STR)
    {
      strcpy(pclDummy,"20");
      if (pcgFCSDat[pclFixFields[ilCurr].Start]==' ')
      { /* do nat add YY if date part is empty */
         strcpy(pclDummy,"  ");
      }
      strncat(pclDummy,&pcgFCSDat[pclFixFields[ilCurr].Start],pclFixFields[ilCurr].Len);
      pclDummy[pclFixFields[ilCurr].Len+2]=0;
    }
    else if (pclFixFields[ilCurr].Type == VIA_STR)
    {
      pclDummy[0]= 0;
      if (pcgFCSDat[pclFixFields[ilCurr].Start]!=' ')
      {
/*										*/
/*		  Allow uncancellation of a flight by FCS			*/
        strncpy(pclDummy2,&pcgFCSDat[pclFixFields[ilCurr].Start],pclFixFields[ilCurr].Len);
        pclDummy2[pclFixFields[ilCurr].Len]=0;
        sprintf(pclDummy," %-119s",pclDummy2);
      }
    }
    else 
    {
      strncpy(pclDummy,&pcgFCSDat[pclFixFields[ilCurr].Start],pclFixFields[ilCurr].Len);
      pclDummy[pclFixFields[ilCurr].Len]=0;
      if ((pclFixFields[ilCurr].Type == TIME_STR) && (strncmp(pclFixFields[ilCurr].Name,"STO",3)!=0))
      {
        if (strcmp(pclDummy,"    ")!=0)
        {
          strcat(pclDummy,"00");
        }
        else
        {
          strcat(pclDummy,"  ");
        }
      }
    }
    if (strcmp(pclFixFields[ilCurr].Name,"FLDU")==0)
    {
      strcpy(pclFldu,pclDummy);

    } else if (strncmp(pclFixFields[ilCurr].Name,"VP",2)==0)
    {
      if ((strcmp(pclDummy,"        ")==0) || (igIgnoreFromTo==TRUE))
      {
        strcpy(pclDummy,pclFldu);
      } 
    } else if (strcmp(pclFixFields[ilCurr].Name,"FREQ")==0)
    {
      if ((strcmp(pclDummy,"        ")==0) || (igIgnoreFromTo==TRUE))
      {
        strcpy(pclDummy,"1234567");
      } 
    } else if (strcmp(pclFixFields[ilCurr].Name,"JFLN")==0)
    {
      if (strcmp(pclDummy,"        ")==0)
      { /* avoid adding empty corresponding flight! ToDo: Is split requested?? */
        continue;
      } 
    } else if (strcmp(pclFixFields[ilCurr].Name,"REMP")==0)
    {
      if (strcmp(pclDummy,"CN")==0)
      ilAddFtypX= TRUE;
    } 
    /*
	20070418 - DKA - If B1BA time received, use best guess from comparison
	with landed time to figure it date is same or next day of landed. Most
	of the time should be the landed, occasionally if landed is before mid-
	night, it could be the next day. Since date/time is from FCS, they are
	all local. We first make a copy of landed date.
	Assumption - that a belt1 time will never be sent without a
	landed time.
    */
    else if (strcmp(pclFixFields[ilCurr].Name,"*LAND")==0)
    { 
      strcpy (pclLandDate, pclDummy);	/* Date year already adjusted YY->YYYY */
      strcpy (pclLandDT, pclDummy);
    } 
    else if (strcmp(pclFixFields[ilCurr].Name,"LAND")==0)
    { 
      strcpy (pclLandTime, pclDummy);  /* Time already adjusted to HHMMSS */ 
      strcat (pclLandDT, pclDummy);   /* safe because land time is always after land date */
    } 
    else if (strcmp(pclFixFields[ilCurr].Name,"- *ONBS")==0)
    { 
      strcpy (pclOnBlockDT, pclDummy);	/* Date year already adjusted YY->YYYY */ 
    } 
    else if (strcmp(pclFixFields[ilCurr].Name,"- ONBS")==0)
    { 
      strcat (pclOnBlockDT, pclDummy);  /* Time already adjusted to HHMMSS */ 
    } 

    else if (strcmp(pclFixFields[ilCurr].Name,"*B1BA")==0) 
    { 
      /* peek forward to get the belt1 time */
      memset (pclBeltTime, '\0', 16);
      strncpy(pclBeltTime,&pcgFCSDat[pclFixFields[ilCurr + 1].Start],pclFixFields[ilCurr + 1].Len);

      /* DEBUG */
      /* dbg(DEBUG,"GetFixField: DEBUG Belt1 time as peeked [%s]", pclBeltTime); */

      /* we got a belt1 and landed time. Note the different lengths */ 
      if ((strcmp (pclBeltTime, "    ") != 0) && (strcmp (pclLandTime, "      ") != 0))	
      {

      /* DEBUG */
      /* dbg(DEBUG,"GetFixField: DEBUG non null Belt1 time as peeked [%s]", pclBeltTime); */

        iLandTime = atoi (pclLandTime);		/* was already HHMMSS when saved */
	strcat (pclBeltTime, "00");
        iBeltTime = atoi (pclBeltTime);

	/* dbg(DEBUG,"GetFixField: DEBUG stored LandDate is [%s]", pclLandDate);  */
        dbg(DEBUG,"GetFixField: Compare LandTime [%s] and Belt1 Time [%s]", pclLandTime, pclBeltTime); 
        if (iBeltTime >= iLandTime)	/* Same date */
        {
          /* dbg(DEBUG,"GetFixField: Both are probably in the same date"); */
	  strcpy (pclDummy, pclLandDate);
          dbg(DEBUG,"GetFixField: Set Belt 1 Date to Land date of [%s]", pclDummy); 
        }
        else				/* Belt time is next day, prob past midnight */
        {
          /* dbg(DEBUG,"GetFixField: Belt 1 time probably next day"); */
	  strcat (pclLandDate, "000000");	/* YYYYMMDD -> YYYYMMDDHHMMSS */
	  
	  if (AddSecondsToCEDATime (pclLandDate, 86400, 1) == RC_SUCCESS)
	  {
	    memset (pclDummy, '\0', 16);
	    strncpy (pclDummy, pclLandDate, 8);
	    dbg(DEBUG,"GetFixField: Set Belt 1 Date to Land date plus one [%s]", pclDummy); 
	  } 
        } 
      } 
    } 
    else if (strstr (pclFixFields[ilCurr].Name, "GTA1")!= '\0')
    { 
	strcpy (pclGTA1, pclDummy);	/* save for later PSTA/D */ 
    }

    else if (strstr (pclFixFields[ilCurr].Name, "GTD1") != '\0')
    { 
	strcpy (pclGTD1, pclDummy);	/* save for later PSTA/D */ 
    }

    else if (strcmp(pclFixFields[ilCurr].Name,"PSTA")==0)	/* only if are accepting it from FCS */
    { 
      if (strcmp (pclDummy, "    ") == 0)	/* nothing from FCS, then we overwrite with gate */
      {
        strcpy (pclDummy, pclGTA1);
        dbg(DEBUG,"GetFixField: Replace blank FCS PSTA with GTA1 [%s]", pclGTA1);
      } 
    } 
    else if (strcmp(pclFixFields[ilCurr].Name,"PSTD")==0)	/* only if are accepting it from FCS */
    { 
      if (strcmp (pclDummy, "    ") == 0)	/* nothing from FCS, then we overwrite with gate */
      {
        strcpy (pclDummy, pclGTD1);
        dbg(DEBUG,"GetFixField: Replace blank FCS PSTD with GTD1 [%s]", pclGTD1);
      } 
    } 
    else if ((strcmp(pclFixFields[ilCurr].Name,"- TGA1")==0) ||
	(strcmp(pclFixFields[ilCurr].Name,"- TGD1")==0))
    {
	/* save a copy */
	strcpy (clTerm, pclDummy);
	strcpy (clTermFieldName, &(pclFixFields[ilCurr].Name[2])); 
    }


/*    dbg(TRACE,"GetFixField: checking <%s> <%s>", pclFixFields[ilCurr].Name,pclDummy);*/
    if (pclFixFields[ilCurr].Name[0]!='-')
    {
      if (pclFixFields[ilCurr].Name[0]!='*')
      {
        if ((strcmp(pclFixFields[ilCurr].Name,"FLNO")==0) ||
            (strcmp(pclFixFields[ilCurr].Name,"JFLN")==0))
        {
          if (igIsNonSched)
          {
            if (strcmp(pclFixFields[ilCurr].Name,"FLNO")==0)
            {
              strcpy (clCsgn, pclDummy);	/* save a copy */
              strcat(pcgFCSOutD,pclDummy);	/* which is the CSGN */
              strcat(pcgFCSOutD,", "); 		/* comma and a blank FLNO */
              strcat(pcgFCSOutF,"CSGN,");           
              dbg(TRACE, 
               "GetFixField: added <CSGN> <%s>", clCsgn); 
              strcpy (pclDummy, " ");		/* FLNO set blank in CEDA msg */
            }

            if (strcmp(pclFixFields[ilCurr].Name,"JFLN")==0)
            { 
               strcat(pcgFCSOutD,pclDummy);	/* which is the linked CSGN */
            }
          }
          else
          {
            /*
		Original inline FLNO parsing replaced with new_flno_parse()
		in v.1.7j
            */ 
            new_parse_fcs_flno (pclDummy, pclFlnoTmp, pclFlns);
            strcat(pcgFCSOutD,pclFlnoTmp); 
          }
        }
        else
        {
           strcat(pcgFCSOutD,pclDummy);
        } 

        dbg(TRACE,"GetFixField: added <%s> <%s>", pclFixFields[ilCurr].Name,pclDummy);
        strcat(pcgFCSOutF,pclFixFields[ilCurr].Name);
        strcat(pcgFCSOutF,",");
        strcat(pcgFCSOutD,","); 

        if ((strcmp(pclFixFields[ilCurr].Name,"FLNO")==0) 
          && (igIsNonSched == 0)) 
        {
          /* save copy of FLNO for check to add terminal. */ 
          strcpy (clFlno, pclFlnoTmp);

          if (pclFlns [0] != '\0')
          {
            strcat (pcgFCSOutD,pclFlns);    /* No hard code bcos not always 'D'... */
            strcat (pcgFCSOutD,","); 
            strcat(pcgFCSOutF,"FLNS,"); 
          }
        } 
      }
      else
      {
        strcat(pcgFCSOutD,pclDummy);
        dbg(TRACE,"GetFixField: added partly <%s>  <%s>", pclFixFields[ilCurr].Name,pclDummy);
      }
    }
    else
    {
      dbg(TRACE,"GetFixField: skipped <%s> <%s>", pclFixFields[ilCurr].Name,pclDummy);
    }
  }
  strcat(pcgFCSOutF,"ADID");
  strcat(pcgFCSOutD,cpAdid);
  if (ilAddFtypX == TRUE)
  {
    strcat(pcgFCSOutF,",FTYP");
    strcat(pcgFCSOutD,",X");
  } 

  /* If flight is not in AFTTAB, add terminal */ 
  /* Important: ONBS is used somewhere below to test ONBS updating */
  if (igIsNonSched == 0)
    (void) FlightInAFT (clFlno, pclFldu, cpAdid, &found, "ONBS", pclDummy); 
  else
    (void) NonSchedFlightInAFT (clCsgn, pclFldu, cpAdid, &found, "ONBS", pclDummy);

  if (found == 0)
  { 
    strcat(pcgFCSOutF,",");
    strcat(pcgFCSOutF, clTermFieldName);
    strcat(pcgFCSOutD, ",");
    strcat(pcgFCSOutD, clTerm);

    if (igIsNonSched != 0)
    { 
      /* only for inserts, never for updates */
      strcat(pcgFCSOutF,",");
      strcat(pcgFCSOutF, "DSSF");
      strcat(pcgFCSOutD, ",");
      strcat(pcgFCSOutD, "U"); 
    }
  } 

  /* Handle arrival onblock time  - only for arrivals */
  if (!(strcmp (cpAdid, "A")))
  {
    if (pclOnBlockDT [0] != ' ')	/* ONBS in FCS msg is not empty */
    {
      /* Accept ONBS unconditionally */
      strcat(pcgFCSOutF,",");
      strcat(pcgFCSOutF, "ONBS");
      strcat(pcgFCSOutD, ",");
      strcat(pcgFCSOutD, pclOnBlockDT); 
      dbg(TRACE,"Accept non-blank FCS ONBS");
    }
    /*
	Empty FCS ONBS - obviously with this change, FCS cannot blank
	out the ONBS ! We copy ATA into ONBS only if AFTTAB.ONBS is
	empty. Note that if ATA is empty we just pass over the blank
	ATA as ONBS to AFTTAB.
    */ 
    else
    {
      if ((found > 0) && (pclDummy [0] == ' '))
      {
        strcat(pcgFCSOutF,",");
        strcat(pcgFCSOutF, "ONBS");
        strcat(pcgFCSOutD, ",");
        strcat(pcgFCSOutD, pclLandDT); 
        dbg(TRACE,"Empty AFTTAB.ONBS - Copy FCS LAND->ONBS");
      } 
    }
  } 
}



/******************************************************************************/
/* The ResetAftValues routine                                                 */
/* This routine resets all pointers and output buffers for online and         */
/* scheduled FCS data                                                        */
/******************************************************************************/
void *ResetAftValues(void)
{

  pcgFCSOutF[0]= 0;
  pcgFCSOutD[0]= 0;
  pcgFCSSel[0] = 0;

}

/******************************************************************************/
/* The AdjustFixedArrayField function.						*/
/* This routine loops through the fields in the fixed array defining FCS fields	*/
/* and Ufis names, and adjusts the Ufis name so that imphdl will accept that	*/
/* field's values instead of the default ignore.				*/ 
/******************************************************************************/
void AdjustFixedArrayField (FCS_TO_AFT_FIX *pclFixFields, int ipFixMax, char *pclUfisFieldName, char *cadid)
{ 
  int ilCurr; 
 
  for (ilCurr= 0; ilCurr < ipFixMax; ilCurr++)
  {
    if (strstr (pclFixFields [ilCurr].Name, pclUfisFieldName) != '\0')
    {
	dbg(DEBUG,"Init_FCS: Default %s field was [%s]", cadid, pclFixFields [ilCurr].Name);
	strcpy (pclFixFields [ilCurr].Name, pclUfisFieldName);
	dbg(DEBUG,"Init_FCS: Changed to [%s]", pclFixFields [ilCurr].Name);
	break;
    } 
  }   
}

/********************************************************************************/
/* HandleFCSDelete 								*/
/* Convert FCS delete message into an  update to FTYP=X (cancelled, default)	*/
/* Problem is that we only get the ADID, FLDA and unconverted FLNO		*/
/* Delete only if we are not in the configurable window times where deletes	*/
/* are ignored. This is so that we do not delete per FCS block nightly deletesy	*/
/*										*/
/* Attempt to get UFIS FLNO from FCSTAB first. Rationale is that we can delete	*/
/* only flights that were already created by FCS, in which case they should 	*/
/* have an entry in FCSTAB.							*/
/*										*/
/* v.1.7h - delete towings together with the arrival.				*/
/* v.1.7k - copy 6 bytes from FCS date.						*/
/* CR08005 - try to cater for a non scheduled flight. The FCS delete message	*/
/*  does not have nature code, so if no entry in FCSTAB, then try to select 	*/
/*  with the FCS flno untranslated directly from AFTTAB.			*/
/********************************************************************************/

int  HandleFCSDelete (char *pclData, char *pclCmd, char *pclSelOut,
	char *pclFldOut,char *pclDatOut, 
	char *pcRecvName, char *pcDestName, char *pcTwStart, char *pcTwEnd)
{
  char adid, fcs_flno [9], flda [9], ufis_flno [16], csAdid [2]; 
  int ilRC = RC_SUCCESS;
  short slFkt = 0;
  short slCursor = 0;
  char clSqlBuf[4500] ="\0", clSqlBufTow[2048];
  char clData [32];
  char clFields [32];
  char clCmd [8];
  long found = 0;
  long foundNonSched = 0;

  int     mm;
  /* char    clTimeofday [8]; 
  int	  iTimeofday; */

  char	  cToday [7];	/* YYMMDD, save processing time in conversion to YYYYMMDD */
			/* Assume Ufis CGMS will be replaced by 2099 .... */
  int	  iToday;
  char	  cFCS_FLDA [7], cURNO [32], cURNO_NonSched [32];
  int	  iFCS_FLDA;
  struct tm       *now;
  time_t          epoch;


  epoch = time ('\0');
  now = localtime (&epoch);

  /*
  memset (clTimeofday, '\0', 8);
  strftime (clTimeofday, 8, "%H%M", now);
  iTimeofday = atoi (clTimeofday);
  */

  memset (cToday, '\0', 7);
  strftime (cToday, 7, "%y%m%d", now);
  iToday = atoi (cToday); 
  strncpy (cFCS_FLDA, &pcgFCSDat [11], 6); 
  cFCS_FLDA [6] = '\0';
  iFCS_FLDA = atoi (cFCS_FLDA);

  /* if ((iTimeofday > igIgnoreDelStart) && (iTimeofday < igIgnoreDelEnd)) */
  if (iFCS_FLDA < iToday)
  {
    /* dbg(TRACE,"HandleFCSDelete: Ignoring delete at [%.4d] hours", iTimeofday); */
    dbg(TRACE,"HandleFCSDelete: Don't delete for [%.6d] on [%.6d]", iFCS_FLDA, iToday);
    return RC_SUCCESS; 
  }

  if (pcgFCSDat[1]=='1')
  {
    adid = 'A';
    strcpy (csAdid, "A");
  }
  else
  {
    adid = 'D';
    strcpy (csAdid, "D");
  }
  /*
	It is safe to set this flag here, because for FCS deletes this would be
	the first time it is being initialized for the current FCS message. 
  */
  igIsNonSched = 0;

  memset (fcs_flno, '\0', 9);
  strncpy (fcs_flno, &pcgFCSDat [3], 8); 
  memset (flda, '\0', 9);
  strcpy (flda, "20");
  strncpy (&flda [2], &pcgFCSDat [11], 6);
  
  sprintf(clSqlBuf,"SELECT UFLN FROM FCSTAB WHERE FCSF='%s'", fcs_flno); 
  dbg(TRACE,"HandleFCSDelete: clSqlBuf: <%s>",clSqlBuf);
  slFkt = START;
  slCursor = 0;
  ilRC = sql_if (slFkt, &slCursor, clSqlBuf, ufis_flno);
  if (ilRC == RC_SUCCESS)
  {
    dbg(TRACE,"HandleFCSDelete: [%s] has Ufis Flno: [%s]", fcs_flno, ufis_flno);
  } 
  else if (ilRC == RC_NOTFOUND)
  {
    dbg(TRACE,"HandleFCSDelete: %s has no Ufis Flno, try as CSGN instead", fcs_flno); 
    (void) NonSchedFlightInAFT (fcs_flno, flda, csAdid, &foundNonSched, "URNO",
      cURNO_NonSched);
    if (foundNonSched > 0)
    {
      ilRC = RC_SUCCESS;
      igIsNonSched = 1;
      dbg(TRACE,"HandleFCSDelete: CSGN [%s] [%c] is in AFT", fcs_flno, adid); 
    }
    else
      dbg(TRACE,"HandleFCSDelete: no CSGN [%s] [%c] in AFT", fcs_flno, adid); 
  }
  else
  {
    dbg(TRACE,"HandleFCSDelete: SELECT from FCSTAB failed <%d?%d>",ilRC,RC_NOTFOUND);
  }
  commit_work();
  close_my_cursor (&slCursor);

  if (ilRC == RC_SUCCESS)
  {
    /* v.1.7h - get the URNO of the record for arrivals */
    if (adid == 'A')
    { 
      strcpy (csAdid, "A");
      if (igIsNonSched == 0)
      { 
        (void) FlightInAFT (ufis_flno, flda, csAdid, &found, "URNO", cURNO);
        if (found > 0)	/* Exists */
        { 
          sprintf (clSqlBufTow,
            "WHERE AURN = '%s' AND FTYP IN ('T','G')", cURNO);
        }
      }
      else 
      {
        /* Exists as non-sched */ 
        sprintf (clSqlBufTow,
            "WHERE AURN = '%s' AND FTYP IN ('T','G')", cURNO_NonSched);
      } 
    }

    /*  How about just "WHERE URNO = '%d'" ?? - DKA 20100511 */ 
    if (igIsNonSched == 0)
      sprintf (clSqlBuf, "WHERE FLNO = '%.9s' AND FLDA = '%s' and ADID = '%c'",
	ufis_flno, flda, adid);
    else 
      sprintf (clSqlBuf, "WHERE CSGN = '%.9s' AND FLDA = '%s' and ADID = '%c'",
	fcs_flno, flda, adid);

    dbg(TRACE,"HandleFCSDelete: pclSelection <%s>",clSqlBuf);
    if (adid == 'A')
      dbg(TRACE,"HandleFCSDelete: Towing Selection  <%s>", clSqlBufTow); 

    if (igRealDelete == 1)
    {
      ilRC = SendCedaEvent (7800,		/* Route ID */
			0,		/* Origin ID */
			pcDestName,	/* Dest_name */
			pcRecvName,	/* Recv_name */
			pcTwStart,	/* CMDBLK TwStart */
			pcTwEnd,	/* CMDBLK TwEnd */
			"DFR",		/* CMDBLK command */
			"AFTTAB",	/* CMDBLK Obj name */
			clSqlBuf,	/* Selection block */
			"",		/* Field block */
			"",		/* Data block */
			"",		/* Error description */
			4,		/* priority */
			NETOUT_NO_ACK);	/* return code */ 

      /* v.1.7h - additional for towing records only for arrivals */
      if ((found > 0) && (adid == 'A'))
      {
        (void)SendCedaEvent (7800,	/* Route ID */
			0,		/* Origin ID */
			pcDestName,	/* Dest_name */
			pcRecvName,	/* Recv_name */
			pcTwStart,	/* CMDBLK TwStart */
			pcTwEnd,	/* CMDBLK TwEnd */
			"DFR",		/* CMDBLK command */
			"AFTTAB",	/* CMDBLK Obj name */
			clSqlBufTow,	/* Selection block for towings */
			"",		/* Field block */
			"",		/* Data block */
			"",		/* Error description */
			4,		/* priority */
			NETOUT_NO_ACK);	/* return code */ 
      }
    }

    if (igRealDelete == 0)
    {
      ilRC = SendCedaEvent (7800,		/* Route ID */
			0,		/* Origin ID */
			pcDestName,	/* Dest_name */
			pcRecvName,	/* Recv_name */
			pcTwStart,	/* CMDBLK TwStart */
			pcTwEnd,	/* CMDBLK TwEnd */
			"UFR",		/* CMDBLK command */ 
			"AFTTAB",	/* CMDBLK Obj name */
			clSqlBuf,	/* Selection block */
			"FTYP", 	/* Field block */ 
			pcgDeleteCode, 	/* Data block */ 
			"",		/* Error description */
			4,		/* priority */
			NETOUT_NO_ACK);	/* return code */ 
    }

  }

  return ilRC; /* is one of RC_SUCCESS, RC_NOTFOUND or RC_FAIL */ 

}

/******************************************************************************/
/* The wrapFCSData routine                                                   */
/* This is the interface routine called by IMPMAN                             */
/* In:  pclData:   pointer to the data record received from FCS              */
/* Out: pclCmd:    pointer to store the command "UFR" or "DFR"                */
/*      pclSelOut: pointer to store the selection for IMPMAN                  */
/*      pclFldOut: pointer to store the field list for IMPMAN                 */
/*      pclDatOut: pointer to store the data list for IMPMAN                  */
/*										*/
/*	14May07	DKA	Change deletion detection.				*/
/*			Add arguments for SendCedaEvent call during deletes	*/
/*	31Mar08 DKA	Explicit check for daily departure			*/
/*	05May10	DKA	CR08005 - Post-process FCS->AFT translated fields to	*/
/*			detect non-scheduled flights to tweak them for FLIGHT	*/
/*										*/
/********************************************************************************/
int wrapFCSData(char *pclData, char *pclCmd,
		char *pclSelOut,char *pclFldOut,char *pclDatOut,
		char *pcRecvName, /* BC-Head used as WKS-Name */
		char *pcDestName, /* BC-Head used as USR-Name */
		char *pcTwStart, /* BC-Head used as Stamp */
		char *pcTwEnd)   /* ditto */
{
    int ilRC = RC_SUCCESS;
    
    if (igArrFixMax == 0)
    {
       Init_FCS();
    }

    pcgFCSSel= pclSelOut;
    pcgFCSOutF= pclFldOut;
    pcgFCSOutD= pclDatOut;
    pcgFCSDat= pclData;
    pcgFCSCmd= pclCmd;
    
    /*
	if neither daily departures nor daily arrivals do nothing.  
    */ 
      
    if ((pcgFCSDat[1] != '1') && (pcgFCSDat[1] != '2'))
    {
      dbg(TRACE,"wrapFCSData: Unexpected FCS File [%c]", pcgFCSDat[1]);
      return RC_IGNORE;
    } 

    ResetAftValues();
    strcpy(pcgFCSSel,"IFNAME:FCS,TIME:LOCAL,PRIO:4");
    strcpy(pclCmd,"IFR");
    if (pcgFCSDat[2]=='D')
    /* if (strlen(pcgFCSDat)==17) */ 
    { 
       ilRC = HandleFCSDelete (pclData, pclCmd, pclSelOut, pclFldOut, pclDatOut,
		pcRecvName, pcDestName, pcTwStart, pcTwEnd); 
       if (ilRC == RC_SUCCESS)
	 return RC_IGNORE; /* Otherwise MQRTAB will not be ack'd */
       else
	 return RC_FAIL;   /* update mqrtab to 'DATA ERR' */
    }

    /*
	Very Important: the following check cannot be made before the call to
	HandleFCSDelete (above) because an FCS delete message contains only "flight
	number" and FLDA. Within HandleFCSDelete() is code to handle the possibility
	that a non-sched flight is being deleted.  
    */

    igIsNonSched = 0;
    if (
     ((pcgFCSDat [1] == '1') && (isNonSchedTtyp (pcgFCSDat [igArrTtypPos])))
         ||
     ((pcgFCSDat [1] == '2') && (isNonSchedTtyp (pcgFCSDat [igDepTtypPos])))
    )
    {
      igIsNonSched = 1;
      dbg(TRACE, "wrapFCSData: Detected Non Sched Flight");
    }
 
    if (pcgFCSDat[1]=='1')
    { /* Arrival Data */
       GetFCSToAft(pcgArrFixFields,igArrFixMax,"A");
       return ilRC;
    }

    if (pcgFCSDat[1]=='2')
    { /* Departure Data */
       GetFCSToAft(pcgDepFixFields,igDepFixMax,"D");
    }

    
    return ilRC;
}

/****************************************************************************/
/*	FlightInAFT - lookup AFTTAB if FLNO+FLDA exists. 		    */
/*		'found' is set to -1 if not in AFTTAB, otherwise it is the  */
/*		URNO.							    */ 
/*		Return value is 0 if no sql_if error, and 2 if error   	    */
/*		Argument 'found' set to -1 if sql_if error, 0 if flight not */
/*		in AFTTAB, otherwise set to the URNO if found.		    */
/*		WHERE is based on FLNO and FLDA because they are the primary*/
/*		key of flight tables in FCS. Add ADID to WHERE to avoid	    */
/*		selecting a tow record, & for case where arr & dep have	    */
/*		identical flight no.					    */
/*		v.1.7g - allow the retrieval of a specific field.	    */
/****************************************************************************/
int  FlightInAFT (char *flno, char *flda, char *adid, long *pfound,
	char *clFieldName, char *clFieldValue)
{ 
  int ilRC, res, res2;
  short slFkt = 0;
  short slCursor = 0;
  char clSqlBuf[2048] ="\0";
  char clDataArea [2048], clUrno [16], clItemNames [64], clDummy2 [64];

  res = 0; 
  if (clFieldName [0] == '\0')	/* select a dummy field */
  {
    sprintf(clSqlBuf,
      "SELECT URNO, STOA FROM AFTTAB WHERE FLNO='%s' AND FLDA='%s' AND ADID = '%s'", 
	flno, flda, adid);
    strcpy (clItemNames, "URNO,STOA"); 
  }
  else
  {
    sprintf(clSqlBuf,
      "SELECT URNO, %s FROM AFTTAB WHERE FLNO='%s' AND FLDA='%s' AND ADID = '%s'", 
	clFieldName, flno, flda, adid);
    sprintf (clItemNames, "URNO,%s", clFieldName); 
  }

  dbg(TRACE,"FlightInAFT: clSqlBuf: <%s>",clSqlBuf); 
  slFkt = START;
  slCursor = 0;
  /* ilRC = sql_if (slFkt, &slCursor, clSqlBuf, pcl_ufis_iata);  */
  ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clDataArea); 
  if (ilRC == RC_SUCCESS)
  { 
    BuildItemBuffer (clDataArea, clItemNames, 2, ",");
    get_real_item (clUrno, clDataArea, 1); 
    *pfound = atol (clUrno);
    dbg(TRACE,"FlightInAFT: FLNO [%s] FLDA [%s] ADID [%s] has URNO [%ld]",
	flno, flda, adid, *pfound); 

    if (clFieldName [0] != '\0')
    {
      /* get_real_item() nullifies blanks.. */
      get_real_item (clFieldValue, clDataArea, 2);
      if (clFieldValue [0] == '\0')
        strcpy (clFieldValue, " "); 
    }
  }
  else if (ilRC == RC_NOTFOUND)
  {
      dbg(TRACE,"FlightInAFT: FLNO [%s] FLDA [%s] ADID [%s] not in AFTTAB",
	flno, flda, adid);
      res = 1; 
      *pfound = 0;
  }
  else 
  {
      dbg(TRACE,"FlightInAFT: sql_if failed <%d> #############",ilRC);
      res = 2; 
      *pfound = -1;
  }

  commit_work();
  close_my_cursor (&slCursor); 
  return res; 
}


/********************************************************************************/
/*	new_parse_fcs_flno 							*/
/*	Parse the FCS flno such that the FLNS is detected and saved.		*/
/*	Input: "fcs" - fcs flno							*/
/*	Output: ufis_flno (with the suffix in last position)			*/
/*		ufis_flns (suffix, empty string if none)			*/
/********************************************************************************/

void	new_parse_fcs_flno (
	char *fcs,
	char *ufis_flno,
	char *ufis_flns)
{ 
  int jj, kk;
  /* Flwg two variables are equivalent to pclFltn, pclAlc in GetFCSToAft() */
  char ufis_fltn [12];	/* number portion only */
  char ufis_alc [4];

  /*
	Get the airline code taking into account 2 and 3 letter codes. 
	IATA standard allows first 2 char's to be alphanumric. 3rd char
	must be alphabet. 

	Allow for space(s) between FCS airline code and number, although that
	is a rarity from FCS.
  */

  memset(ufis_alc,0,sizeof(ufis_alc));
  ufis_alc [0] = fcs [0];	/* first char of airl code can be number */
  ufis_alc [1] = fcs [1];	/* second char of airl code can be number */ 
 
  /* 3rd char of IATA code if any, excluding possible blank from isalpha */
  if ((isalpha (fcs [2])) && (fcs [2] != ' '))
  {
    ufis_alc [2] = fcs [2]; 
    jj = 3;
  }
  else
    jj = 2;

  /* skip spaces if any after ALC */
  while (fcs [jj] != '\0')
  { 
    if (fcs [jj] != ' ')
      break;
    jj++;
  }

  memset(ufis_fltn,0,sizeof(ufis_fltn));

  for (kk = 0; isdigit (fcs [jj]); jj++, kk++)
  {
    ufis_fltn [kk] = fcs [jj];
  }

  /* skip spaces if any after flt num */ 
  while (fcs [jj] != '\0')
  { 
    if (fcs [jj] != ' ')
      break; 
    jj++;
  } 

  ufis_flns [0] = '\0';
  if (isalpha (fcs [jj]))	/* Look for suffix */
  {
    ufis_flns [0] = fcs [jj];
    ufis_flns [1] = '\0';
  }

  /* Create Ufis FLNO without the suffix */
  sprintf(ufis_flno,"%-3s%03d", ufis_alc, atoi(ufis_fltn)); 

  /*
	If suffix, append (more than required) spaces after the FLNO,
	then plug in the suffix at position #9. Spaces are added this way
	to save time trying to figure out where the FLNO ends..
  */
    
  if (ufis_flns [0] != '\0')
  {
    strcat (ufis_flno, "        ");
    ufis_flno [8] = ufis_flns [0];
    ufis_flno [9] = '\0'; 
  } 
}

/********************************************************************************/
/*	getFieldPosition ()							*/
/*	Find the location of a given field specified by UFIS fieldname in the	*/
/*	FCS dep and arr message formats. 					*/
/*	Input: FCS_TO_AFT_FIX arrays and lengths				*/
/*	Output: start position as return value. Return -1 if not found.		*/
/*										*/
/********************************************************************************/

int getFieldPosition (FCS_TO_AFT_FIX *pclFixFields, int ipFixMax,
  char *pcFieldName)
{
  char pclDummy[124];	
  char pclTtyp [4];
  int iFieldPos = -1;
  int ilCurr;
  
  dbg (TRACE, "getFieldPosition: Looking for posn of [%s]", pcFieldName);

  for (ilCurr= 0; ilCurr < ipFixMax ; ilCurr++)
  { 
    if (strcmp (pclFixFields[ilCurr].Name, pcFieldName) == 0)
    {
      iFieldPos = pclFixFields[ilCurr].Start;
      dbg (TRACE, "getFieldPosition: Found posn of [%s] at [%d]",
        pcFieldName, iFieldPos);
      break; 
    } 
  }
  if (iFieldPos < 0)
    dbg (TRACE, "getFieldPosition: **** ERROR *** : Could not find posn of [%s]",
      pcFieldName);
  return iFieldPos;
}

/********************************************************************************/
/*	int isNonSchedTtyp - CR08005						*/
/*	Input : the ttyp character from the incoming FCS msg			*/
/*	Return value :	0 - does not match any of the non-sched char codes	*/
/*			non-zero - matches a non-sched code			*/ 
/*										*/
/********************************************************************************/

int isNonSchedTtyp (char clTtyp)
{
  int jj = 0; 

  while (cNonSchedTtyp [jj] != '\0')
  {
    if (cNonSchedTtyp [jj] == clTtyp)
      return 1; 
    
    jj++; 
  }
  return 0;
}

/****************************************************************************/
/*	NonSchedFlightInAFT - lookup AFTTAB if CSGN+FLDA+ADID exists.		*/
/*		'found' is set to -1 if not in AFTTAB, otherwise it is the  */
/*		URNO.							    */ 
/*		Return value is 0 if no sql_if error, and 2 if error   	    */
/*		Argument 'found' set to -1 if sql_if error, 0 if flight not */
/*		in AFTTAB, otherwise set to the URNO if found.		    */
/*		WHERE is based on (CSGN+FLDA+ADID). In FCS, the CSGN is	stored 	*/
/*		in their FLNO. So we need to cater FLNO and FLDA because they are the primary*/
/*		key of flight tables in FCS. Add ADID to WHERE to avoid	    */
/*		selecting a tow record, & for case where arr & dep have	    */
/*		identical flight no.					    */
/*		v.1.7g - allow the retrieval of a specific field.	    */
/****************************************************************************/
int  NonSchedFlightInAFT (char *csgn, char *flda, char *adid, long *pfound,
	char *clFieldName, char *clFieldValue)
{ 
  int ilRC, res, res2;
  short slFkt = 0;
  short slCursor = 0;
  char clSqlBuf[2048] ="\0";
  char clDataArea [2048], clUrno [16], clItemNames [64], clDummy2 [64];

  res = 0; 
  if (clFieldName [0] == '\0')	/* select a dummy field */
  {
    sprintf(clSqlBuf,
      "SELECT URNO, STOA FROM AFTTAB WHERE CSGN='%s' AND FLDA='%s' AND ADID = '%s'", 
	csgn, flda, adid);
    strcpy (clItemNames, "URNO,STOA"); 
  }
  else
  {
    sprintf(clSqlBuf,
      "SELECT URNO, %s FROM AFTTAB WHERE CSGN='%s' AND FLDA='%s' AND ADID = '%s'", 
	clFieldName, csgn, flda, adid);
    sprintf (clItemNames, "URNO,%s", clFieldName); 
  }

  dbg(TRACE,"NonSchedFlightInAFT: clSqlBuf: <%s>",clSqlBuf); 
  slFkt = START;
  slCursor = 0;
  ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clDataArea); 
  if (ilRC == RC_SUCCESS)
  { 
    BuildItemBuffer (clDataArea, clItemNames, 2, ",");
    get_real_item (clUrno, clDataArea, 1); 
    *pfound = atol (clUrno);
    dbg(TRACE,"NonSchedFlightInAFT: CSGN [%s] FLDA [%s] ADID [%s] has URNO [%ld]",
	csgn, flda, adid, *pfound); 

    if (clFieldName [0] != '\0')
    {
      /* get_real_item() nullifies blanks.. */
      get_real_item (clFieldValue, clDataArea, 2);
      if (clFieldValue [0] == '\0')
        strcpy (clFieldValue, " "); 
    }
  }
  else if (ilRC == RC_NOTFOUND)
  {
      dbg(TRACE,"NonSchedFlightInAFT: FLNO [%s] FLDA [%s] ADID [%s] not in AFTTAB",
	csgn, flda, adid);
      res = 1; 
      *pfound = 0;
  }
  else 
  {
      dbg(TRACE,"NonSchedFlightInAFT: sql_if failed <%d> #############",ilRC);
      res = 2; 
      *pfound = -1;
  }

  commit_work();
  close_my_cursor (&slCursor); 
  return res; 
}
