#ifndef _DEF_mks_version
#define _DEF_mks_version
#include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Sin/SIN_Server/Base/Server/Kernel/IMP/wrapCsked.src.c 1.5 2003/06/25 12:59:15CEST heb Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         : Hans-J�rgen Ebert                                         */
/* Date           : 19.03.2003                                                */
/* Description    :                                                           */
/*                                                                            */
/* Update history :                                                           */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */




/* Function Prototypes */

static int wrapCskedData(char *pcpData,char *pcpCommand, char *pcpSelection, char *pcpFields, char *pcpNewData);
static int CskedAircraftChange(char *pcpFields, char *pcpData);
static int CskedScheduleChange(char *pcpCommand, char *pcpFields, char *pcpData);
static int CskedOperationalUpdate(char *pcpFields, char *pcpData);
static void CskedTrimRight(char *s);



static int wrapCskedData(char *pcpData,char *pcpCommand, char *pcpSelection, char *pcpFields, char *pcpNewData)
/* pcpData: The original Data that has to be transformed*/
/* Command, Selection,Fields,Data: the return-values of the function.... input for impman*/
{
    int ilRC = RC_SUCCESS;
    char clCommand[10] = "\0";
    char clSelection[50] = "\0";
    char clFields[500] = "\0";
    char clData[2002] = "\0";
    char *pclData = NULL;
    char clUpdCode[6] = "\0";
    int ilLen = 0;
    int ilDataLen = 0;
    
    ilDataLen = strlen(pcpData);

    if (ilDataLen < 2002)
    {
	strncpy(clData,pcpData,ilDataLen);
	clData[ilDataLen+1] = '\0';
    }


    strcpy(pcpSelection,"IFNAME:CSKED,TIME:UTC");

    dbg(TRACE,"%05d: Funktion Csked received: Data <%s>",__LINE__,clData);
    
    pclData = clData; /*Pointer auf beginn von Data*/
    pclData += 60;    /*Pointer hinter den Header verschieben (laut ICD 60 character)*/
    
    dbg(TRACE,"%05d: Data without Header: <%s>",__LINE__,pclData);
    strncpy(clUpdCode,pclData,3);
    clUpdCode[3] = '\0';
    strcpy(clData,pclData);
    dbg(TRACE,"%05d: Data without Header2: <%s>",__LINE__,pclData);
    dbg(TRACE," This is cldata to give to functions: <%s>",clData);
    dbg(TRACE," This is the update-Code: <%s>",clUpdCode);
    if(strcmp(clUpdCode,"CRG") == 0)
    {

	ilRC = CskedAircraftChange(pcpFields, clData);
	if(ilRC == RC_SUCCESS)
	{
	    strcpy(pcpData,pclData);
	    strcpy(pcpCommand,"UFR");
	}
	else
	{
	    strcpy(pcpData,"\0");
	    strcpy(pcpCommand,"ERR");
	    strcpy(pcpSelection,"Corrupted Data");
	    dbg(TRACE,"%05d: Corrupted Data");
	    ilRC = RC_FAIL;
	}
    }
    else
    {
	if((strcmp(clUpdCode,"NFL") == 0) ||
	   (strcmp(clUpdCode,"DFL") == 0) ||
	   (strcmp(clUpdCode,"CFL") == 0) ||
	   (strcmp(clUpdCode,"RIS") == 0) ||
	   (strcmp(clUpdCode,"NOP") == 0))
	{
	    ilRC = CskedScheduleChange(pcpCommand, pcpFields, clData);
	    if(ilRC != RC_SUCCESS)
	    {
		strcpy(pcpData,"\0");
		strcpy(pcpCommand,"ERR");
		strcpy(pcpSelection,"Corrupted Data");
		dbg(TRACE,"%05d: Corrupted Data");
		ilRC = RC_FAIL;
	    }
	}
	else
	{
	    if(strcmp(clUpdCode,"MVT") == 0)
	    {
		ilRC = CskedOperationalUpdate(pcpFields, clData);
		strcpy(pcpCommand,"UFR");
	    }
	    else
	    {
		strcpy(pcpData,"\0");
		strcpy(pcpCommand,"ERR");
		strcpy(pcpSelection,"Corrupted Data");
		dbg(TRACE,"%05d: Update-Code conforms not to CSKED-ICD");
		ilRC = RC_FAIL;
	    }
	}
    }
    strcpy(pcpNewData,clData);

    return ilRC;
}


static int CskedAircraftChange(char *pcpFields, char *pcpData)
{
    int ilRC = RC_SUCCESS;
    char *pclData = NULL;
    char *pclFields = NULL;
    char clTemp[10] = "\0";
    char clData[2002] = "\0";

    dbg(TRACE,"%05d: ----- CskedAircraftChange start -----",__LINE__);
    dbg(TRACE,"%05d: pcpFields: <%s>",__LINE__,pcpFields);
    dbg(TRACE,"%05d: pcpData  : <%s>",__LINE__,pcpData);
    
    pcpFields[0] = '\0';
    pclData = pcpData;
    pclData += 3; /* Update-Code ignorieren*/
/* Now get ALC2/3 */
    strncpy(clTemp,pclData,3);
    pclData += 3;
    clTemp[3] = '\0';
    CskedTrimRight(clTemp);
    if(strlen(clTemp) == 2)
    {
	strcat(pcpFields,"ALC2,");
    }
    else
    {
	strcat(pcpFields,"ALC3,");
    }
    if((strcmp(clTemp,"SQ") != 0) && (strcmp(clTemp,"MI") != 0))
    {
	return RC_FAIL;
    }
    strcat(clData,clTemp);
    strcat(clData,",");
    
/* Now get Flight-Number */
    strcat(clData,clTemp);
    strcat(clData," ");
    clTemp[0] = '\0';
    strncpy(clTemp,pclData,5);
    pclData += 5;
    clTemp[5] = '\0';
    CskedTrimRight(clTemp);
    strcat(pcpFields,"FLNO,");
    strcat(clData,clTemp);
    strcat(clData,",");

/* Now get scheduled time of deparutre */
/* Now the date*/
    clTemp[0] = '\0';
    strncpy(clTemp,pclData,8);
    pclData += 8;
    clTemp[8] = '\0';
    strcat(pcpFields,"FLDA,");
    strcat(clData,clTemp);
/* Skipp the  time */    
    
    pclData += 4;
    strcat(clData,",");

/* Now get sector Number*/
    clTemp[0] = '\0';
    strncpy(clTemp,pclData,1);
    pclData += 1;
    clTemp[1] = '\0';
    strcat(pcpFields,"SECN,");
    strcat(clData,clTemp);
    strcat(clData,",");

/* Now get Origin 3LC */
    clTemp[0] = '\0';
    strncpy(clTemp,pclData,3); /* 3LC but field is 5 char*/
    pclData += 5;
    clTemp[3] = '\0';
    strcat(pcpFields,"ORG3,");
    strcat(clData,clTemp);
    strcat(clData,",");
    
/* Now get Destination 3LC*/
    clTemp[0] = '\0';
    strncpy(clTemp,pclData,3); /* 3LC but field is 5 char*/
    pclData += 5;
    clTemp[3] = '\0';
    strcat(pcpFields,"DES3,");
    strcat(clData,clTemp);
    strcat(clData,",");
 
/* Now get Registration */
    clTemp[0] = '\0';
    strncpy(clTemp,pclData,8);
    pclData += 8;
    clTemp[8] = '\0';
    CskedTrimRight(clTemp);
    strcat(pcpFields,"REGN,");
    strcat(clData,clTemp);
    strcat(clData,",");

/* Now get Internal Aircraft-Type */
    clTemp[0] = '\0';
    strncpy(clTemp,pclData,5);
    pclData += 5;
    clTemp[5] = '\0';
    strcat(pcpFields,"ACT5,");
    strcat(clData,clTemp);
    strcat(clData,",");

/* Now get Aircraft Type */
    clTemp[0] = '\0';
    strncpy(clTemp,pclData,5);
    pclData += 5;
    clTemp[5] = '\0';
    CskedTrimRight(clTemp);
    if(strlen(clTemp) == 3)
    {
	strcat(pcpFields,"ACT3");
    }
    else
    {
	return RC_FAIL;
    }

    strcat(clData,clTemp);

    strcat(clData,"\0");
    dbg(TRACE," end of Aircraftchange: clData: <%s>",clData);
    strcpy(pcpData,clData);
    dbg(TRACE," end of Aircraftchange: pcpData: <%s>",pcpData);

    return ilRC;
}


static int CskedScheduleChange(char *pcpCommand, char *pcpFields, char *pcpData)
{
    int ilRC = RC_SUCCESS;
    char *pclData = NULL;
    char *pclFields = NULL;
    char clTemp[10] = "\0";
    char clData[2002] = "\0";
    char clUpdateCode[10] = "\0";
    char clAlc2[10] = "\0";
    char clFlno[10] = "\0";
    char clFlda[10] = "\0";
    char clNums[10] = "\0";
    char clActi[10] = "\0";
    char clAct[10] = "\0";
    char clData1[50] = "\0";
    int ilNums = 0;
    int i = 0;

    dbg(TRACE,"%05d: ----- CskedScheduleChange start -----",__LINE__);
    dbg(TRACE,"%05d: pcpFields: <%s>",__LINE__,pcpFields);
    dbg(TRACE,"%05d: pcpData  : <%s>",__LINE__,pcpData);
    
    pcpFields[0] = '\0';
    pclData = pcpData;
/*Now get update-code*/
    strncpy(clUpdateCode,pclData,3);
    pclData += 3;
    clUpdateCode[3] = '\0';

    if(strcmp(clUpdateCode,"NFL") == 0)
    {
	strcpy(pcpCommand,"IFR");
    }
    else
    {
	if(strcmp(clUpdateCode,"DFL") == 0)
	{
	    strcpy(pcpCommand,"DFR");
	}
	else
	{
	    strcpy(pcpCommand,"UFR");
	}
    }

/* Now set FTYP if necessary */
    if(strcmp(clUpdateCode,"RIS") == 0)
    {
	strcat(pcpFields,"FTYP,");
	strcat(clData,"S,");
    }
    
    if(strcmp(clUpdateCode,"NOP") == 0)
    {
	strcat(pcpFields,"FTYP,");
	strcat(clData,"X,");
    }
    
    
/* Now get ALC2/3 */
    strncpy(clTemp,pclData,3);
    pclData += 3;
    clTemp[3] = '\0';
    CskedTrimRight(clTemp);
    if((strcmp(clTemp,"MI") == 0) || (strcmp(clTemp,"SQ") == 0))
    {
	strcpy(clAlc2,clTemp);
	strcat(pcpFields,"ALC2,");
	strcat(clData,clTemp);
	strcat(clData,",");
    }
    else
    {
	dbg(TRACE,"%05d: Wrong Airline-code: not MI odr SQ, skipp this record",__LINE__);
	return RC_FAIL;
    }
    
/* Now get Flight-Number */
    strcat(clData,clTemp);
    strcat(clData," ");
    clTemp[0] = '\0';
    strncpy(clTemp,pclData,5);
    pclData += 5;
    clTemp[5] = '\0';
    CskedTrimRight(clTemp);
    strcpy(clFlno,clTemp);
    strcat(pcpFields,"FLNO,");
    strcat(clData,clTemp);
    strcat(clData,",");

/* Now get scheduled time of deparutre (first sector date)*/
/* Now the date*/
    clTemp[0] = '\0';
    strncpy(clTemp,pclData,8);
    pclData += 8;
    clTemp[8] = '\0';
    strcpy(clFlda,clTemp);
    strcat(pcpFields,"FLDA,");
    strcat(clData,clTemp);
/* Now skipp the time, we dont need it */    
    pclData += 4;
    strcat(clData,",");

/* Now get number of sectors*/
    clTemp[0] = '\0';
    strncpy(clTemp,pclData,1);
    pclData += 1;
    clTemp[1] = '\0';
    strcpy(clNums,clTemp);
    strcat(pcpFields,"NUMS,");
    strcat(clData,clTemp);
    strcat(clData,",");

/* Now get Internal Aircraft-Type */
    clTemp[0] = '\0';
    strncpy(clTemp,pclData,5);
    pclData += 5;
    clTemp[5] = '\0';
    CskedTrimRight(clTemp);
    strcpy(clActi,clTemp);
    strcat(pcpFields,"ACT5,");
    strcat(clData,clTemp);
    strcat(clData,",");

/* Now get Aircraft Type */
    clTemp[0] = '\0';
    strncpy(clTemp,pclData,5);
    pclData += 5;
    clTemp[5] = '\0';
    CskedTrimRight(clTemp);
    if(strlen(clTemp) == 3)
    {
	strcat(pcpFields,"ACT3,");
    }
    else
    {
	return RC_FAIL;
    }
    strcpy(clAct,clTemp);
    strcat(clData,clTemp);
    strcat(clData,",");
   
    if(strcmp(clUpdateCode,"NFL") == 0)
    {
	strcat(pcpFields,"TTYP,");
	clTemp[0] = '\0';
	strncpy(clTemp,pclData,3);
	pclData += 3;
	clTemp[3] = '\0';
	strcat(clData,clTemp);
	strcat(clData,",");
	
    }

    else
    {
	pclData += 3;
   }
    strcat(clData,"\0");
    strcpy(clData1,clData);
    
    ilNums = atoi(clNums);
    
    strcat(pcpFields,"SECN,ORG3,DES3,STOD,STOA");
    for (i=1 ; i<=ilNums ; i++)
    {
/* Now get Sector-number*/
	clTemp[0] = '\0';
	strncpy(clTemp,pclData,1);
	pclData += 1;
	clTemp[1] = '\0';
	strcat(clData,clTemp);
	strcat(clData,",");
	
/* Now get Org3 */
	clTemp[0] = '\0';
	strncpy(clTemp,pclData,5);
	pclData += 5;
	clTemp[5] = '\0';
	CskedTrimRight(clTemp);
	strcat(clData,clTemp);
	strcat(clData,",");
	
/* Now get Des3 */
	clTemp[0] = '\0';
	strncpy(clTemp,pclData,5);
	pclData += 5;
	clTemp[5] = '\0';
	CskedTrimRight(clTemp);
	strcat(clData,clTemp);
	strcat(clData,",");
	
/* Now get stod */
/*date*/
	clTemp[0] = '\0';
	strncpy(clTemp,pclData,8);
	pclData += 8;
	clTemp[8] = '\0';
	strcat(clData,clTemp);
/*time*/
	clTemp[0] = '\0';
	strncpy(clTemp,pclData,4);
	pclData += 4;
	clTemp[4] = '\0';
	strcat(clData,clTemp);
	strcat(clData,"00");
	strcat(clData,",");
	
/* Now get stoa */
/*date*/
	clTemp[0] = '\0';
	strncpy(clTemp,pclData,8);
	pclData += 8;
	clTemp[8] = '\0';
	strcat(clData,clTemp);
/*time*/
	clTemp[0] = '\0';
	strncpy(clTemp,pclData,4);
	pclData += 4;
	clTemp[4] = '\0';
	strcat(clData,clTemp);
	strcat(clData,"00");
	
	if((ilNums > 1) && (i != ilNums))
	{
	    strcat(clData,"\n");
	    strcat(clData,clData1);
	}

	
    }/*end of for*/

    strcpy(pcpData,clData);
    dbg(TRACE,"%05d: ----- CskedScheduleChange end -----",__LINE__);
 
    return RC_SUCCESS;
}


static int CskedOperationalUpdate(char *pcpFields, char *pcpData)
{
    int ilRC = RC_SUCCESS;
    char *pclData = NULL;
    char *pclFields = NULL;
    char clTemp[10] = "\0";
    char clData[2002] = "\0";
    char clUpdateCode[10] = "\0";
    char clAlc2[10] = "\0";
    char clFlno[10] = "\0";
    char clFlda[10] = "\0";
    char clSecn[10] = "\0";
    char clActi[10] = "\0";
    char clAct[10] = "\0";
    char clOrg3[10] = "\0";
    char clDes3[10] = "\0";
    char clData1[50] = "\0";
    int ilBlocks = 0;
    int i = 0;

    dbg(TRACE,"%05d: ----- CskedScheduleChange start -----",__LINE__);
    dbg(TRACE,"%05d: pcpFields: <%s>",__LINE__,pcpFields);
    dbg(TRACE,"%05d: pcpData  : <%s>",__LINE__,pcpData);
    
    pcpFields[0] = '\0';
    pclData = pcpData;
    ilBlocks = (strlen(pclData) - 34) / 14;
    dbg(TRACE,"%05d: Number of Movementblocks: <%d>",__LINE__,ilBlocks);

    pclData += 3;   /*Update-Code ueberspringen*/

/* Now get ALC2/3 */
    strncpy(clTemp,pclData,3);
    pclData += 3;
    clTemp[3] = '\0';
    CskedTrimRight(clTemp);
    if((strcmp(clTemp,"SQ") == 0) || (strcmp(clTemp,"MI") == 0))
    {
	strcpy(clAlc2,clTemp);
	strcat(pcpFields,"ALC2,");
	strcat(clData,clTemp);
	strcat(clData,",");
    }
    else
    {
	dbg(TRACE,"%05d: Wrong Airline-code: not MQ odr SI, skipp this record",__LINE__);
	return RC_FAIL;
    }
    
/* Now get Flight-Number */
    strcat(clData,clTemp);
    strcat(clData," ");
    clTemp[0] = '\0';
    strncpy(clTemp,pclData,5);
    pclData += 5;
    clTemp[5] = '\0';
    CskedTrimRight(clTemp);
    strcpy(clFlno,clTemp);
    strcat(pcpFields,"FLNO,");
    strcat(clData,clTemp);
    strcat(clData,",");

/* Now get scheduled time of deparutre (first sector date)*/
/* Now the date*/
    clTemp[0] = '\0';
    strncpy(clTemp,pclData,8);
    pclData += 8;
    clTemp[8] = '\0';
    strcpy(clFlda,clTemp);
    strcat(pcpFields,"FLDA,");
    strcat(clData,clTemp);
/* Now skipp the time, we dont need it */    
    pclData += 4;
    strcat(clData,",");

/* Now get number of sectors*/
    clTemp[0] = '\0';
    strncpy(clTemp,pclData,1);
    pclData += 1;
    clTemp[1] = '\0';
    strcpy(clSecn,clTemp);
    strcat(pcpFields,"SECN,");
    strcat(clData,clTemp);
    strcat(clData,",");

/* Now get Origin 3LC */
    clTemp[0] = '\0';
    strncpy(clTemp,pclData,3); /* 3LC but field is 5 char*/
    pclData += 5;
    clTemp[3] = '\0';
    strcpy(clOrg3,clTemp);
    strcat(pcpFields,"ORG3,");
    strcat(clData,clTemp);
    strcat(clData,",");
    
/* Now get Destination 3LC*/
    clTemp[0] = '\0';
    strncpy(clTemp,pclData,3); /* 3LC but field is 5 char*/
    pclData += 5;
    clTemp[3] = '\0';
    strcpy(clDes3,clTemp);
    strcat(pcpFields,"DES3");
    strcat(clData,clTemp);


    for(i=1 ; i <= ilBlocks ; i++)
    {
	clTemp[0] = '\0';
	strncpy(clTemp,pclData,2);
	clTemp[2] = '\0';
	pclData += 2;
	
	/* check for ED */
	if(strcmp(clTemp,"EA") != 0)
	{
	    strcat(pcpFields,",");
	    strcat(clData,",");

	    if(strcmp(clTemp,"ED") == 0)
	    {
	    	    strcat(pcpFields,"ETDA");
	    } 
	    else
	    {
		if(strcmp(clTemp,"AD") == 0)
		{
	    	    strcat(pcpFields,"OFBS");
		} 
		else
		{
		    if(strcmp(clTemp,"AA") == 0)
		    {
			strcat(pcpFields,"ONBS");
		    } 
		    else
		    {
			if(strcmp(clTemp,"RD") == 0)
			{
			    strcat(pcpFields,"ETDC");
			} 
			else
			{
			    if(strcmp(clTemp,"RA") == 0)
			    {
				strcat(pcpFields,"ETAC");
			    } 
			    else
			    {
				if(strcmp(clTemp,"AN") == 0)
				{
				    strcat(pcpFields,"AIRA");
				} 
				else
				{
				    if(strcmp(clTemp,"LN") == 0)
				    {
					strcat(pcpFields,"LNDA");
				    } 
				    else
				    {
					dbg(TRACE,"%05d: MVT-Code <%s> does not correspond to ICD, skipp this record",__LINE__,clTemp);
					return RC_FAIL;
				    }
				}
			    }
			}
		    }
		}
	    }

	    
	    clTemp[0] = '\0';
	    strncpy(clTemp,pclData,8);
	    clTemp[8] = '\0';
	    pclData += 8;
	    strcat(clData,clTemp);
	    clTemp[0] = '\0';
	    strncpy(clTemp,pclData,4);
	    clTemp[4] = '\0';
	    pclData += 4;
	    strcat(clData,clTemp);
	    strcat(clData,"00");

	}/* end of "not EA"*/
	else
	{
	    pclData += 12;
	}

    }

    strcpy(pcpData,clData);

    return RC_SUCCESS;
}


static void CskedTrimRight(char *s)
{
    int i = 0;    /* search for last non-space character */
    for (i = strlen(s) - 1; i >= 0 && isspace(s[i]); i--); /* trim off right spaces */
    s[++i] = '\0';
}
