/**
 * @ version 1.0
 * @ Author Thanooj Kumar Putsala
 * @ Utility Class used to create DB connection. 
 * @ Create 13-SEP-2006
**/
package cgms;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.*;

public class TanDBConnection {
	Driver 				drv  = null;
	Connection		 	conn = null;
	Statement			stmt = null;

	String				dbip	= "";
	String				dbsid	= "";
	String				dbuser	= "";
	String				dbpwd	= "";
	String				dbport	= "";

	public TanDBConnection(){
		initConnection();
	}

	public TanDBConnection(String dbip1, String dbsid1, String dbport1, String dbuser1, String dbpwd1){
		dbip	= dbip1;
		dbsid   = dbsid1;
		dbuser	= dbuser1;
		dbpwd	= dbpwd1;
		dbport  = dbport1;

		initConnection();
	}

	public void initConnection(){
		
		try {
			 drv = (Driver)Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();
			 //conn = DriverManager.getConnection("jdbc:oracle:thin:@cgmsvdb1:1521:UFIS", "CEDA", "CEDA");
			 conn = DriverManager.getConnection("jdbc:oracle:thin:@" + dbip + ":" + dbport + ":" + dbsid, dbuser, dbpwd);
			 stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
		}catch (Exception e){
			 print("initConnection() :  " + e.toString());
		}
	}

	public Connection getConnection(){
		try {
			 drv = (Driver)Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();
			 conn = DriverManager.getConnection("jdbc:oracle:thin:@" + dbip + ":" + dbport + ":" + dbsid, dbuser, dbpwd);
			 //conn = DriverManager.getConnection("jdbc:oracle:thin:@cgmsvdb1:1521:UFIS", "CEDA", "CEDA");
			 stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
		} catch (Exception e) {
			print("getConnection() : " + e.toString());
		}
		return conn;
	}
	
	public PreparedStatement getPreparedStatement(String query){
		PreparedStatement ps = null;
		try{
			ps = conn.prepareStatement(query);
		}catch(Exception e){
			print("getPreparedStatement() : " + e.toString());
		}
		return ps;
	}

	public CallableStatement getCallableStatement(String query){
		CallableStatement cs = null;
		try{
			cs = conn.prepareCall(query);
		}catch(Exception e){
			print("getCallableStatement() : " + e.toString());
		}
		return cs;
	}

	public ResultSet getResultSet(String query){
		ResultSet res1	= null;
		try{
			res1 = stmt.executeQuery(query);
		}catch(Exception e){
			print("getResultSet() : " + e.toString());
		}
		return res1;
	}

	public int getTotalRows(ResultSet rs){
		int			totRow	= 0;
		ResultSet	rs1		= rs;
		try{
			if(rs1 != null){
				while(rs1.next()){
					++totRow;	
				}
			}
		}catch(Exception e){
			print("getTotalRows() : " + e.toString());
		}finally{
			rs1 = null;
		}

		return totRow;
	}

	public void destroy(){
		try{
			stmt = null;
			conn.close();
			conn = null;
			drv = null;
		}catch(Exception e){
			print("destroy() : " + e.toString());
		}
	}

	public static void print(String value){
		System.out.println(value);
	}
}
