/*
 * @(#)ConfigMessage.java	1.0 
 */

 /*
 * @(#)Utility Class used to retrieve Configuration settings from configuration file. 
 * @version 1.0 
 * Author: Thanooj Kumar Putsala
 */
package cgms;

import java.io.*;
import java.util.*;

public class  ConfigMessage{
	static FileInputStream FINcf = null;
	static PropertyResourceBundle resourceBundle = null;
	
	public ConfigMessage(){
		//Get the finance.conf file
		try {
			//FINcf = new FileInputStream("/ceda/conf/finance_new.conf");
			FINcf = new FileInputStream("./conf/finance_new.conf");
		} catch(FileNotFoundException FNF) {
			System.out.println("File not found exception : " + FNF);
		} catch(Exception ex) {
			System.out.println("Exception : " + ex);
		}
		
		//prepare resourceBundle class
		try {
			resourceBundle = new PropertyResourceBundle(FINcf);
		} catch(IOException IOE) {
			System.out.println("IO exception : " + IOE);
		} catch(Exception ex) {
			System.out.println("Exception : " + ex);
		}
	}

	/**
		This method returns configuration string message from the resource bundle
	*/
	public String getString(String s)	{
		return resourceBundle.getString(s);
	}
}
