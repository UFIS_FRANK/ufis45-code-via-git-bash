/**
 * @ version 1.0
 * @ Author Thanooj Kumar Putsala
 * @ Utility Class used to convert GMT to UTC Dateformat. 
 * @ Create 13-SEP-2006
**/
package cgms;

import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

public class TanDate {
   private SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
   private Calendar cal = Calendar.getInstance();
   
   /**
    * applies given offset hours to input date string 
    * @dateStr a date string in yyyyMMddHHmmss format
    * @offset offset hours
    * @return GMT converted date string in yyyyMMddHHmmss format
    * @throws ParseException if input date string does not match yyyyMMddHHmmss format
    */
   public synchronized String getGMTDate(String dateStr, int offset) throws ParseException{
	
	if(dateStr == null || dateStr.equals("")) return "";
	Date d = null;
	try {
		d = df.parse(dateStr);               
	}
	catch(ParseException pe) {
		throw pe;
	}
	cal.clear();
	cal.setTime(d);
	cal.add(Calendar.HOUR_OF_DAY, offset);                                 
	return df.format(cal.getTime());      
   }

   public synchronized String getLocalDate(String dateStr, int offset) throws ParseException{
	  if(dateStr == null || dateStr.equals("")) return "";
      Date d = null;
      try {
         d = df.parse(dateStr);               
      }
      catch(ParseException pe) {
         throw pe;
      }
      cal.clear();
      cal.setTime(d);
      cal.add(Calendar.HOUR_OF_DAY, offset);                                 
      return df.format(cal.getTime());      
   }


   public Date getPreviousDate(Date dt){
	  cal.clear();
      cal.setTime(dt);
      cal.add(Calendar.DAY_OF_YEAR, -1);
	  return cal.getTime();
   }



   public synchronized String getPreviousDate(String dateStr) throws ParseException{
      Date d = null;
      try {
         d = df.parse(dateStr);
      }
      catch(ParseException pe) {
         throw pe;
      }
      cal.clear();
      cal.setTime(d);
      
      final int currentDstOffset = cal.get(Calendar.DST_OFFSET);
      cal.add(Calendar.DAY_OF_YEAR, -1);
      
      final int preciousDstOffset = cal.get(Calendar.DST_OFFSET);
      if (currentDstOffset > preciousDstOffset) 
    	  cal.add(Calendar.MILLISECOND, currentDstOffset);
      else if (currentDstOffset < preciousDstOffset)
    	  cal.add(Calendar.MILLISECOND, -preciousDstOffset);
    	  
      return df.format(cal.getTime());      
   }

   public synchronized String getDateBefore(String dateStr,int offset) throws ParseException{
      Date d = null;
      try {
         d = df.parse(dateStr);               
      }
      catch(ParseException pe) {
         throw pe;
      }
      cal.clear();
      cal.setTime(d);
      cal.add(Calendar.DAY_OF_YEAR, offset);                                 
      return df.format(cal.getTime());      
   }

   public int getBillingCycle(Date dt){
		int day;
		day = dt.getDate();

		if(day >= 1 && day <= 6) return 1;
		if(day >= 7 && day <= 12) return 2;
		if(day >= 13 && day <= 18) return 3;
		if(day >= 19 && day <= 24) return 4;
		if(day >= 25 && day <= 31) return 5;
		else return 0;
	}

	public int getBillingCycle1(String dateStr) throws ParseException{
		Date d = null;
		try {
			d = df.parse(dateStr);               
		}
		catch(ParseException pe) {
			throw pe;
		}
		cal.clear();
		cal.setTime(d);

		return getBillingCycle(d);
	}

	public synchronized int getDateDifferenceInHours(String strDate1,String strDate2) throws ParseException{
		Date d1 = null;
		Date d2 = null;

		long dt1Time = 0;
		long dt2Time = 0;
		long diffInHours = 0;

		try {
			d1 = df.parse(strDate1);
			d2 = df.parse(strDate2);
		}catch(ParseException pe) {
			throw pe;
		}

		Calendar cal1 = null; 
        Calendar cal2 = null;

		cal1=Calendar.getInstance(); 
        cal2=Calendar.getInstance(); 
        
        // different date might have different offset
        cal1.setTime(d1);          
        long ldate1 = d1.getTime() + cal1.get(Calendar.ZONE_OFFSET) + cal1.get(Calendar.DST_OFFSET);
        
        cal2.setTime(d2);
        long ldate2 = d2.getTime() + cal2.get(Calendar.ZONE_OFFSET) + cal2.get(Calendar.DST_OFFSET);
        
        // Use integer calculation, truncate the decimals
        int hr1   = (int)(ldate1/3600000); //60*60*1000
        int hr2   = (int)(ldate2/3600000);

        //int days1 = (int)hr1/24;
        //int days2 = (int)hr2/24;
        int dateDiff  = hr2 - hr1;
		
		return dateDiff;
	}

	public static String getVial(String via){
		String retViaStr = "";

		//Each via contains 120 characters  ..... via structure looks like below
		/*
		struct VialType {
		char display[1];
		char apc3[3];
		char apc4[4];
		char stoa[14];
		char etoa[14];
		char land[14];
		char onbl[14];
		char stod[14];
		char etod[14];
		char ofbl[14];
		char airb[14];
	  };
	  */

	  int viaLen = via.length();
	  System.out.println("Via length: " + viaLen);
	  
	  if(viaLen > 0){
		  int viaCounter = 0;
		  int viaValCounter = 1;
		  while(viaCounter < viaLen){
				retViaStr = retViaStr + via.substring(viaValCounter,viaValCounter+3);
				System.out.println("First Via " + via.substring(viaCounter,viaCounter+120));
				viaValCounter = viaValCounter + 120;
				viaCounter = viaCounter + 120;
		  }

		  int retViaStrCount = 0;
		  System.out.println("Before Return Via String: " + retViaStr + " Length: " + retViaStr.length());

		  retViaStrCount = retViaStr.length() / 3;
		  for(int i=0; i< 7-retViaStrCount; i++)
			  retViaStr = retViaStr + "   ";
		  System.out.println("After Return Via String: " + retViaStr + " Length: " + retViaStr.length());

		  return retViaStr;

	  } else return retViaStr;

	}
   
   public static void main(String[] args) {
      TanDate tanDt = new TanDate();
      
      try {

    	  String str = tanDt.getPreviousDate(args[0] + "000000");
    	  //String str = tanDt.getPreviousDate("20121030" + "000000");
    	  System.out.println(str);
		} catch (Exception e) {
			
		}
   }

}