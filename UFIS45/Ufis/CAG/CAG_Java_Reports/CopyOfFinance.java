/**********************************************************************
 * File Name		:	Finance.java
 * Created By	:	Thanooj Kumar Putsala	October 18th, 2006
 * Company		:  	UFIS-AS
 * Description	:	This class is generated to create Airtraffic and Billing reports for CGMS project.
 *					The reports generated based on Finance Interface document.
 *
 * Functions		:
 *					1.	void checkUfis() --- Checks whether UFIS System is up or not
 *					2.	void dailyRequestFunction() --- This function generates reports based on
 *						crontab/scheduled time
 *					3.	void userRequestFunction(String[] args) --- This function generates reports
 *						based on user input. Interactive interface, where user can request to generate
 *						either airtraffic/Billing reports.
 *					4.	void prepareAirtrafficArrival(String strDate) --- This function generates
 *						Airtraffic arrival reportssets.
 *					5.	void prepareAirtrafficDeparture(String strDate) --- This function generates
 *						Airtraffic Departure reports.
 *					6.	void prepareBillingReports(String strDate) --- sets the
 *						recipients list.
 *					7.	void billingHeaderReports(ResultSet rs, String strDate) --- This function
 *						generates Billing Header reports.
 *					8.  void billingDetailReports(ResultSet rs, String strDate) --- This function
 *						generates Billing Detail Reports
 *					9.  void prepareLongstayReports(String strDate) --- This function calls
 *						billingLongStayHeader() & billingLongstayDetail() functions.
 *					10. void billingLongstayHeader(ResultSet rsFirst, ResultSet rsSecond, String
 *						strDate) --- This function generates Billing longstay header report
 *					11. void billingLongstayDetail(ResultSet rsFirst, ResultSet rsSecond, String
 *						strDate) ---  This function generates Billing longstay detail report
 *					12. String getVial(String via) --- This function prepare and
 *						returns the VIA based on ICD from original via of the flight
 *					13. String getVialDep(String via) --- This function prepares and returns the VIA
 *						for departure flights based on ICD from original via of the flight
 *					14. long encryptFile(String source, String destination) --- This function encrypt
 *						the file
 *					15. void sendMail(String subject) --- This function sends subject as mail.
 *
 * Status		:  	Version 1.0
 * Modifications:
 *
 *	12Apr07	ATK	2.2	Bug fixes
 *	30May07	DKA	2.3	Try fix crashes with null pointer in billingDetailReports()
 *
 ***********************************************************************/

package cgms;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;

import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;

public class CopyOfFinance {
	static String sysDate = "";
	static String sysTime = "";
	static String year = "";
	static String year4 = "";
	static String month = "";
	static String curMonth = "";
	static String day = "";
	static String dbuser = "";
	static String dbpassword = "";
	static String dbip = "";
	static String dbsid = "";
	static String mailIds = "";
	static String dbport = "";
	static String airpath = "";
	static String bilpath = "";
	static String dbgpath = "";
	static long utcOffset = 0;
	static int billCycle = 0;
	static boolean longstayFlag;
	static TanDate tanDt = new TanDate(); // This object used for date
											// manipulations UTC/Local etc
	static TanDBConnection dbCon;
	static TanDBConnection dbCon1;
	static Logger log;

	/**
	 * Initializes finance necessary variables with values. Its static block,
	 * which will be called before * main() function
	 * <p>
	 * <p>
	 * No Parameters
	 * <p>
	 * <p>
	 */
	static {
		Date dt = new Date();
		if (dt.getDate() == 1) {
			longstayFlag = true;
			curMonth = ((dt.getMonth() + 1) < 10) ? ("0" + (dt.getMonth() + 1))
					: ("" + (dt.getMonth() + 1));
		} else
			longstayFlag = false;

		curMonth = ((dt.getMonth() + 1) < 10) ? ("0" + (dt.getMonth() + 1))
				: ("" + (dt.getMonth() + 1));

		dt = tanDt.getPreviousDate(dt);
		billCycle = tanDt.getBillingCycle(dt);

		day = (dt.getDate() < 10) ? ("0" + dt.getDate()) : ("" + dt.getDate());
		month = ((dt.getMonth() + 1) < 10) ? ("0" + (dt.getMonth() + 1))
				: ("" + (dt.getMonth() + 1));
		year = ("" + (dt.getYear() + 1900)).substring(2, 4);
		year4 = "" + (dt.getYear() + 1900);

		log = getProcessLogger(CopyOfFinance.class.getName(), year4 + month + day + "_new");

		// sysTime = "" + ((dt.getHours()<10)? ("0"+dt.getHours()):
		// (""+dt.getHours())) + ((dt.getMinutes()<10)? ("0"+dt.getMinutes()):
		// (""+dt.getMinutes()));
		// sysDate = "" + (dt.getYear()+1900) + month + day;

		log.info("Starting Finance Module ...");
		log.info("Reading configurations start...");

		ConfigMessage conf = new ConfigMessage();
		dbuser = conf.getString("DBUSER");
		dbpassword = conf.getString("DBPASSWORD");
		dbip = conf.getString("DBIP");
		dbsid = conf.getString("DBSID");
		dbport = conf.getString("DBPORT");
		utcOffset = Long.parseLong(conf.getString("UTCOFFSET"));
		mailIds = conf.getString("MAILIDS");
		airpath = conf.getString("AIRPATH");
		bilpath = conf.getString("BILPATH");
		dbgpath = conf.getString("DBGPATH");

		log.info("Reading configurations end...");

		dbCon = new TanDBConnection(dbip, dbsid, dbport, dbuser, dbpassword);
		dbCon1 = new TanDBConnection(dbip, dbsid, dbport, dbuser, dbpassword);
	}

	/**
	 * Start of Finance program.
	 * <p>
	 * <p>
	 * 
	 * @param args
	 *            <code>String[]</code> is the command line arguments.
	 *            <p>
	 *            <p>
	 */
	public static void main(String[] args) {
		// Check whether Ufis is down or running ....
		// if (checkUfis() == 0)
		// log.info("Ufis is Running");
		// else {
		// log.fatal("UFIS IS DOWN .. EXIT FINANCE");
		// log.fatal("Exiting ... ");
		// // Below line commented as CGMS don't have mail access & dont want
		// // below functionality. If they request for the same, then uncomment
		// // below line.
		// // sendMail("FINANCEINTERFACE#ALERT#UFISISDOWN");
		// System.exit(0);
		// }

		// If Ufis is running .. then check the request came from user or from
		// its a batch job.
		// Check command line parameters ...
		// If arguments are more than zero means its user request, then take him
		// to interactive options.
		if (args.length > 0) {
			// call userRequestForm
			userRequestFunction(args);
		} else {
			// daily call general function
			dailyRequestFunction();
		}
	}

	/**
	 * This function checks whether UFIS is down or running.
	 * <p>
	 * <p>
	 * 
	 * @No parameters.
	 *     <p>
	 *     <p>
	 */
	public static long checkUfis() {
		String lastLine = "";
		int count = 0;
		try {
			Runtime runTime = Runtime.getRuntime();
			Process process = runTime.exec("myscript.sh");
			DataInputStream dis = new DataInputStream(process.getInputStream());
			lastLine = dis.readLine();
			// System.out.println("Last line from unix command :" + lastLine);
		} catch (Exception e) {
			log.error("Error in executing myscript.sh in checkUfis() function : "
					+ e.toString());
		}

		if (lastLine != null && !lastLine.equals(""))
			return Long.parseLong(lastLine);
		else
			return 1;
	}

	/**
	 * This function will be called when scheduler starts the finance program.
	 * <p>
	 * <p>
	 * 
	 * @No parameters.
	 *     <p>
	 *     <p>
	 */
	public static void dailyRequestFunction() {
		prepareAirtrafficArrival("");
		prepareAirtrafficDeparture("");
		prepareBillingReports("");
		if (longstayFlag)
			prepareLongstayReports("");
	}

	/**
	 * This function will be called when user wants to run finance program
	 * explicitly at command prompt.
	 * <p>
	 * <p>
	 * 
	 * @param args
	 *            <code>String[]</code> is the command line arguments.
	 *            <p>
	 *            <p>
	 */
	public static void userRequestFunction(String[] args) {
		BufferedReader buff;
		StringTokenizer st;
		String s;
		char[] ca;
		char c;

		if (args.length < 2) {
			System.out.println("Invalid command line parameters ... Exiting");
			log.error("Invalid command line parameters ... Exiting");
			System.exit(0);
		}

		if (args[0].equals("Airtraffic")) {
			try {
				buff = new BufferedReader(new InputStreamReader(System.in));
				System.out.println("Welcome to Airtraffic Reports");
				System.out.println("Enter A: Arrival flight report");
				System.out.println("Enter B: Departure flight report");
				System.out.print("Enter your choice:");
				System.out.flush();
				s = buff.readLine();
				st = new StringTokenizer(s);
				ca = st.nextToken().toCharArray();
				c = ca[0];
				System.out.println("You entered " + c);

				if (c == 'A')
					prepareAirtrafficArrival(args[1]);
				else {
					if (c == 'B')
						prepareAirtrafficDeparture(args[1]);
					else {
						System.out.println("Invalid input ... Exiting");
						log.error(" User entered value is '" + c
								+ "' .. which is wrong");
						log.error("Invalid input ... Exiting");
						System.exit(0);
					}
				}
			} catch (Exception e) {
				System.out.println("Error in userRequestFunction(); Error: "
						+ e.toString());
			}
		} else {
			if (args[0].equals("Billing")) {
				try {
					buff = new BufferedReader(new InputStreamReader(System.in));
					System.out.println("Welcome to Billing Reports");
					System.out.println("Enter A: Billing Daily Reports");
					System.out.println("Enter B: Billing Longstay Reports");
					System.out.print("Enter your choice:");
					System.out.flush();
					s = buff.readLine();
					st = new StringTokenizer(s);
					ca = st.nextToken().toCharArray();
					c = ca[0];
					System.out.println("You entered " + c);

					if (c == 'A')
						prepareBillingReports(args[1]);
					else {
						if (c == 'B') {
							if ((args[1].substring(6, 8)).equals("01"))
								prepareLongstayReports(args[1]);
							else {
								System.out
										.println("Invalid input - Day should be '01' for Billing long stay reports, Example: 20061101 ");
								log.error("Invalid input - Day should be '01' for Billing long stay reports, Example: 20061101 ");
								System.out
										.println("Please enter valid date for Billing long stay reports ... Exiting");
								log.error("Please enter valid date for Billing long stay reports ... Exiting");
								System.exit(0);
							}
						} else {
							System.out.println("Invalid input ... Exiting");
							log.error(" User entered value is '" + c
									+ "' .. which is wrong");
							log.error("Invalid input ... Exiting");
							System.exit(0);
						}
					}
				} catch (Exception e) {
					log.error("Error in userRequestFunction(); Error: "
							+ e.toString());
				}

			} else {
				System.out
						.println("Invalid command line parameters ... Exiting");
				log.error("Invalid command line parameters ... Exiting");
				System.exit(0);
			}
		}
	}

	/**
	 * This function prepares Airtraffic arrival flight reports.
	 * <p>
	 * <p>
	 * 
	 * @param strDate
	 *            <code>String</code> is the date for which reports to be
	 *            generated.
	 *            <p>
	 *            <p>
	 */
	public static void prepareAirtrafficArrival(String strDate) {
		String strFileLine = "";
		String strTemp = "";
		String regn = "";
		String stoa = "";
		String etoa = "";
		String flno = "";
		String route = "";
		String tga1 = "";
		String gta1 = "";
		String ttyp = "";
		String act5 = "";
		String alc3 = "";
		String dooa = "";
		String org3 = "";
		String ftyp = "";
		String strDateFrom = "";
		String strDateTo = "";
		String dbStr = "";
		String arrDetFile = "";
		String arrExpFile = "";

		long txtCount = 0;
		long expCount = 0;
		BufferedWriter bw = null;
		BufferedWriter bw1 = null;

		if (strDate.equals("")) {
			arrDetFile = airpath + "A" + year + month + day + "_new" + ".TXT";
			arrExpFile = airpath + "A" + year + month + day + "_new" + ".EXP";

			dbStr = "Select ADID, trim(REGN) as REGN, trim(LAND) as LAND, (select FCSF from FCSTAB where UFLN=FLNO and ROWNUM=1) as FLNO, trim(STOA) as STOA,getdayoftheweekinlocal(LAND) as DOOA, ALC3, ORG3, TRIM(VIAL) as VIAL, TGA1, GTA1, TTYP, ACT5, FTYP from afttab WHERE (LAND BETWEEN TO_CHAR(SYSDATE-2,'YYYYMMDD')||'160000' AND TO_CHAR(SYSDATE-1,'YYYYMMDD')||'155959' AND (DES3 = 'SIN' ) ) AND FTYP <> 'T' AND FTYP <> 'G' ORDER BY LAND";
		} else {
			try {
				strDate = tanDt.getPreviousDate(strDate + "000000");
				strDateFrom = tanDt.getGMTDate(strDate.substring(0, 8)
						+ "000000", -8);
				strDateTo = tanDt.getGMTDate(
						strDate.substring(0, 8) + "235959", -8);
			} catch (Exception e) {
				log.error("Error .. Converting user inserted date to GMT Date. Error in function prepareAirtrafficArrival(), Error :"
						+ e.toString());
			}

			arrDetFile = airpath + "A" + strDate.substring(2, 8) + "_new" + ".TXT";
			arrExpFile = airpath + "A" + strDate.substring(2, 8) + "_new" + ".EXP";

			dbStr = "Select ADID, trim(REGN) as REGN, trim(LAND)as LAND, (select FCSF from FCSTAB where UFLN=FLNO and ROWNUM=1) as FLNO, trim(STOA) as STOA, getdayoftheweekinlocal(LAND) as DOOA, ALC3, ORG3, TRIM(VIAL) as VIAL, TGA1, GTA1, TTYP, ACT5, FTYP from afttab WHERE (LAND BETWEEN '"
					+ strDateFrom
					+ "' AND '"
					+ strDateTo
					+ "' AND (DES3 = 'SIN' ) ) AND FTYP <> 'T' AND FTYP <> 'G' ORDER BY LAND";
		}

		try {
			bw = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(arrDetFile), "UTF-8"));
			bw1 = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(arrExpFile), "UTF-8"));
		} catch (Exception e) {
			log.error("Error .. While creating FileOutputStream. Error in function prepareAirtrafficArrival(), Error :"
					+ e.toString());
		}

		// System.out.println("Date from : " + strDateFrom + "  Date To: " +
		// strDateTo);
		// System.out.println("DB String : " + dbStr);

		try {
			ResultSet rs = dbCon.getResultSet(dbStr);

			// To create first row ...
			int headerCount = 0;
			while (rs.next()) {
				regn = rs.getString("REGN");
				if (regn != null && !regn.equals(""))
					headerCount++;
			}

			strFileLine = " " + Long.toString(headerCount);
			strTemp = "";
			for (int i = 0; i < 72 + 9 - strFileLine.length(); i++)
				strTemp = strTemp + " ";
			strFileLine = strFileLine + strTemp;

			bw.write(strFileLine);
			bw.newLine();
			rs.beforeFirst();

			try {
				if (rs != null) {
					while (rs.next()) {
						regn = rs.getString("REGN");
						strFileLine = "1";

						etoa = rs.getString("LAND");
						if (etoa != null && !etoa.equals(""))
							// etoa = tanDt.getLocalDate(etoa,-utcOffset);
							try {
								etoa = tanDt.getLocalDate(etoa, 8);
							} catch (Exception e) {
								log.error("Error .. Converting LAND time to GMTDate. LAND time in DB: "
										+ etoa
										+ " for Flight: "
										+ rs.getString("FLNO")
										+ " Error in function prepareAirtrafficArrival(), Error :"
										+ e.toString());
								etoa = "              ";
							}
						else
							etoa = "              ";

						stoa = rs.getString("STOA");
						if (stoa != null && !stoa.equals(""))
							// stoa = tanDt.getLocalDate(stoa,-utcOffset);
							try {
								stoa = tanDt.getLocalDate(stoa, 8);
							} catch (Exception e) {
								log.error("Error .. Converting STOA time to GMTDate. LAND time in DB: "
										+ stoa
										+ " for Flight: "
										+ rs.getString("FLNO")
										+ " Error in function prepareAirtrafficArrival(), Error :"
										+ e.toString());
								stoa = "              ";
							}
						else
							stoa = "              ";

						flno = rs.getString("FLNO");
						if (flno == null || flno.equals(""))
							flno = "        ";
						if (flno.length() < 8) {
							strTemp = "";
							for (int i = 0; i < 8 - flno.length(); i++)
								strTemp = strTemp + " ";
							flno = flno + strTemp;
						}

						dooa = rs.getString("DOOA");
						if (dooa == null || flno.equals(""))
							dooa = " ";

						regn = rs.getString("REGN");
						if (regn == null || regn.equals(""))
							regn = "        ";
						if (regn.length() < 8) {
							strTemp = "";
							for (int i = 0; i < 8 - regn.length(); i++)
								strTemp = strTemp + " ";
							regn = regn + strTemp;
						}
						if (regn.length() > 8)
							regn = regn.substring(0, 8);

						alc3 = rs.getString("ALC3");
						if (alc3 == null || alc3.equals(""))
							alc3 = "   ";
						if (alc3.length() < 3) {
							strTemp = "";
							for (int i = 0; i < 3 - alc3.length(); i++)
								strTemp = strTemp + " ";
							alc3 = alc3 + strTemp;
						}

						org3 = rs.getString("ORG3");
						if (org3 == null || org3.equals(""))
							org3 = "   ";
						if (org3.length() < 3) {
							strTemp = "";
							for (int i = 0; i < 3 - org3.length(); i++)
								strTemp = strTemp + " ";
							org3 = org3 + strTemp;
						}

						route = rs.getString("VIAL");
						if (route == null || route.equals(""))
							route = "                     ";
						route = getVial(route);

						tga1 = rs.getString("TGA1");
						if (tga1 == null || tga1.equals(""))
							tga1 = " ";

						gta1 = rs.getString("GTA1");
						if (gta1 == null || gta1.equals(""))
							gta1 = "    ";
						if (gta1.length() < 4) {
							strTemp = "";
							for (int i = 0; i < 4 - gta1.length(); i++)
								strTemp = strTemp + " ";
							gta1 = gta1 + strTemp;
						}
						if (gta1.length() > 4)
							gta1 = gta1.substring(0, 4);

						ttyp = rs.getString("TTYP");
						if (ttyp == null || ttyp.equals(""))
							ttyp = " ";
						if (ttyp.length() > 2)
							ttyp = ttyp.substring(0, 1);

						// ftyp = rs.getString("FTYP");
						// if (ftyp==null || ftyp.equals("")) ftyp = " ";

						act5 = rs.getString("ACT5");
						if (act5 == null || act5.equals(""))
							act5 = "     ";
						if (act5.length() < 5) {
							strTemp = "";
							for (int i = 0; i < 5 - act5.length(); i++)
								strTemp = strTemp + " ";
							act5 = act5 + strTemp;
						}

						strFileLine = strFileLine + etoa.substring(0, 8) + flno
								+ etoa.substring(8, 12) + stoa.substring(0, 12)
								+ dooa + regn + alc3 + org3 + route + "1"
								+ tga1 + gta1 + ttyp + act5;

						regn = rs.getString("REGN");

						if (regn == null || regn.equals("")) {
							bw1.write(strFileLine);
							bw1.newLine();
							expCount++;
						} else {
							bw.write(strFileLine);
							bw.newLine();
							txtCount++;
						}
					}
					// System.out.println("Arrival records: " + txtCount +
					// "  Arrival Exception Count: " + expCount);

					bw.flush();
					bw.close();
					bw1.flush();
					bw1.close();
				}
			} catch (Exception e) {
				// System.out.println("While reading the info, its wrong() : " +
				// e.toString());
				log.error("While reading the info, its wrong() Error in function prepareAirtrafficArrival(), Error : "
						+ e.toString());
			}

		} catch (Exception e) {
			// System.out.println("Error in getting result set: " +
			// e.getMessage());
			log.error("Error in getting result set: Error in function prepareAirtrafficArrival(), Error : "
					+ e.toString());
		}

	}

	/**
	 * This function prepares Airtraffic departure flight reports.
	 * <p>
	 * <p>
	 * 
	 * @param strDate
	 *            <code>String</code> is the date for which reports to be
	 *            generated.
	 *            <p>
	 *            <p>
	 */
	public static void prepareAirtrafficDeparture(String strDate) {
		String strFileLine = "";
		String strTemp = "";
		String regn = "";
		String stod = "";
		String etod = "";
		String flno = "";
		String route = "";
		String tga1 = "";
		String gta1 = "";
		String ttyp = "";
		String act5 = "";
		String alc3 = "";
		String dooa = "";
		String ftyp = "";
		String des3 = "";
		String strDateFrom = "";
		String strDateTo = "";
		String dbStr = "";
		String depDetFile = "";
		String depExpFile = "";

		long txtCount = 0;
		long expCount = 0;
		BufferedWriter bw = null;
		BufferedWriter bw1 = null;

		if (strDate.equals("")) {

			depDetFile = airpath + "D" + year + month + day + "_new" + ".TXT";
			depExpFile = airpath + "D" + year + month + day + "_new" + ".EXP";

			dbStr = "Select ADID, trim(REGN) as REGN, trim(AIRB)as AIRB, (select FCSF from FCSTAB where UFLN=FLNO and ROWNUM=1) as FLNO, trim(STOD) as STOD, getdayoftheweekinlocal(AIRB) as DOOA, ALC3, DES3, TRIM(VIAL) as VIAL, TGD1, GTD1, TTYP, ACT5, FTYP from afttab WHERE (AIRB BETWEEN TO_CHAR(SYSDATE-2,'YYYYMMDD')||'160000' AND TO_CHAR(SYSDATE-1,'YYYYMMDD')||'155959' AND (ORG3 = 'SIN' ) ) AND FTYP <> 'T' AND FTYP <> 'G' ORDER BY AIRB";
		} else {
			try {
				strDate = tanDt.getPreviousDate(strDate + "000000");
				strDateFrom = tanDt.getGMTDate(strDate.substring(0, 8)
						+ "000000", -8);
				strDateTo = tanDt.getGMTDate(
						strDate.substring(0, 8) + "235959", -8);
			} catch (Exception e) {
				log.error("Error .. Converting user inserted date to GMT Date. Error in function prepareAirtrafficDeparture(), Error :"
						+ e.toString());
			}

			depDetFile = airpath + "D" + strDate.substring(2, 8) + "_new" + ".TXT";
			depExpFile = airpath + "D" + strDate.substring(2, 8) + "_new" + ".EXP";

			dbStr = "Select ADID, trim(REGN) as REGN, trim(AIRB)as AIRB, (select FCSF from FCSTAB where UFLN=FLNO and ROWNUM=1) as FLNO, trim(STOD) as STOD, getdayoftheweekinlocal(AIRB) as DOOA, ALC3, DES3, TRIM(VIAL) as VIAL, TGD1, GTD1, TTYP, ACT5, FTYP from afttab WHERE (AIRB BETWEEN '"
					+ strDateFrom
					+ "' AND '"
					+ strDateTo
					+ "' AND (ORG3 = 'SIN' ) ) AND FTYP <> 'T' AND FTYP <> 'G' ORDER BY AIRB";
		}

		try {
			bw = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(depDetFile), "UTF-8"));
			bw1 = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(depExpFile), "UTF-8"));
		} catch (Exception e) {
			log.error("Error .. While creating FileOutputStream. Error in function prepareAirtrafficDeparture(), Error :"
					+ e.toString());
		}

		// System.out.println("strDate  : " + strDate);
		// System.out.println("Date from : " + strDateFrom + "  Date To: " +
		// strDateTo);
		// System.out.println("DB String : " + dbStr);

		try {
			ResultSet rs = dbCon.getResultSet(dbStr);

			// To create first row ...
			int headerCount = 0;
			while (rs.next()) {
				regn = rs.getString("REGN");
				if (regn != null && !regn.equals(""))
					headerCount++;
			}

			strFileLine = " " + Long.toString(headerCount);
			strTemp = "";
			for (int i = 0; i < 72 + 9 - strFileLine.length(); i++)
				strTemp = strTemp + " ";
			strFileLine = strFileLine + strTemp;

			bw.write(strFileLine);
			bw.newLine();
			rs.beforeFirst();
			// upto here ... for first row.

			try {
				if (rs != null) {
					while (rs.next()) {
						// regn = rs.getString("REGN");
						strFileLine = "2";

						etod = rs.getString("AIRB");
						if (etod != null && !etod.equals(""))
							try {
								etod = tanDt.getLocalDate(etod, 8);
							} catch (Exception e) {
								log.error("Error .. Converting AIRB time to GMTDate. AIRB time in DB: "
										+ etod
										+ " for Flight: "
										+ rs.getString("FLNO")
										+ " Error in function prepareAirtrafficDeparture(), Error :"
										+ e.toString());
								etod = "              ";
							}
						else
							etod = "              ";

						stod = rs.getString("STOD");
						if (stod != null && !stod.equals(""))
							// stoa = tanDt.getLocalDate(stoa,-utcOffset);
							try {
								stod = tanDt.getLocalDate(stod, 8);
							} catch (Exception e) {
								log.error("Error .. Converting STOD time to GMTDate. STOD time in DB: "
										+ stod
										+ " for Flight: "
										+ rs.getString("FLNO")
										+ " Error in function prepareAirtrafficDeparture(), Error :"
										+ e.toString());
								stod = "              ";
							}
						else
							stod = "              ";

						flno = rs.getString("FLNO");
						if (flno == null || flno.equals(""))
							flno = "        ";
						if (flno.length() < 8) {
							strTemp = "";
							for (int i = 0; i < 8 - flno.length(); i++)
								strTemp = strTemp + " ";
							flno = flno + strTemp;
						}

						dooa = rs.getString("DOOA");
						if (dooa == null || flno.equals(""))
							dooa = " ";

						// REGN field contains CHAR(12) in DB
						regn = rs.getString("REGN");
						if (regn == null || regn.equals(""))
							regn = "        ";
						if (regn.length() < 8) {
							strTemp = "";
							for (int i = 0; i < 8 - regn.length(); i++)
								strTemp = strTemp + " ";
							regn = regn + strTemp;
						}
						if (regn.length() > 8)
							regn = regn.substring(0, 8);

						alc3 = rs.getString("ALC3");
						if (alc3 == null || alc3.equals(""))
							alc3 = "   ";
						if (alc3.length() < 3) {
							strTemp = "";
							for (int i = 0; i < 3 - alc3.length(); i++)
								strTemp = strTemp + " ";
							alc3 = alc3 + strTemp;
						}

						des3 = rs.getString("DES3");
						if (des3 == null || des3.equals(""))
							des3 = "   ";
						if (des3.length() < 3) {
							strTemp = "";
							for (int i = 0; i < 3 - des3.length(); i++)
								strTemp = strTemp + " ";
							des3 = des3 + strTemp;
						}

						route = rs.getString("VIAL");
						if (route == null || route.equals(""))
							route = "";
						route = getVialDep(route);
						route = route + des3;
						strTemp = "";
						for (int i = 0; i < 24 - route.length(); i++)
							strTemp = strTemp + " ";
						route = route + strTemp;

						/*
						 * route = rs.getString("VIAL"); if (route==null ||
						 * route.equals("")) route = "                        ";
						 * strTemp = ""; for(int i=0; i<24; i++) strTemp =
						 * strTemp + " "; route = strTemp;
						 */

						tga1 = rs.getString("TGD1");
						if (tga1 == null || tga1.equals(""))
							tga1 = " ";

						// DB field for GTA1 contains Char(5).
						gta1 = rs.getString("GTD1");
						if (gta1 == null || gta1.equals(""))
							gta1 = "    ";
						if (gta1.length() < 4) {
							strTemp = "";
							for (int i = 0; i < 4 - gta1.length(); i++)
								strTemp = strTemp + " ";
							gta1 = gta1 + strTemp;
						}
						if (gta1.length() > 4)
							gta1 = gta1.substring(0, 4);

						ttyp = rs.getString("TTYP");
						if (ttyp == null || ttyp.equals(""))
							ttyp = " ";
						if (ttyp.length() > 2)
							ttyp = ttyp.substring(0, 1);

						// ftyp = rs.getString("FTYP");
						// if (ftyp==null || ftyp.equals("")) ftyp = " ";

						act5 = rs.getString("ACT5");
						if (act5 == null || act5.equals(""))
							act5 = "     ";
						if (act5.length() < 5) {
							strTemp = "";
							for (int i = 0; i < 5 - act5.length(); i++)
								strTemp = strTemp + " ";
							act5 = act5 + strTemp;
						}

						strFileLine = strFileLine + etod.substring(0, 8) + flno
								+ etod.substring(8, 12) + stod.substring(0, 12)
								+ dooa + regn + alc3 + route + "1" + tga1
								+ gta1 + ttyp + act5;

						regn = rs.getString("REGN");

						if (regn == null || regn.equals("")) {
							bw1.write(strFileLine);
							bw1.newLine();
							expCount++;
						} else {
							bw.write(strFileLine);
							bw.newLine();
							txtCount++;
						}
					}
					// System.out.println("Departure records: " + txtCount +
					// "  Departure Exception Count: " + expCount);

					bw.flush();
					bw.close();
					bw1.flush();
					bw1.close();
				}
			} catch (Exception e) {
				log.error("While reading the info, its wrong() Error in function prepareAirtrafficDeparture(), Error : "
						+ e.toString());
			}

		} catch (Exception e) {
			log.error("Error in getting result set: Error in function prepareAirtrafficDeparture(), Error : "
					+ e.toString());
		}

	}

	/**
	 * This function sends mail with subject.
	 * <p>
	 * <p>
	 * 
	 * @param subject
	 *            <code>String</code> is the subject of the mail.
	 *            <p>
	 *            <p>
	 */
	public static void sendMail(String subject) {
		try {
			Runtime runTime = Runtime.getRuntime();
			Process process = runTime.exec("mailx -s " + subject + mailIds);
			System.out.println("Mail sent successfully");
		} catch (Exception e) {
			System.out.println("Error in sending mail, Error reason is : " + e);
			log.error("Error in sending mail, Error in function sendMail(), Error : "
					+ e.toString());
		}
	}// End of sendMail function ...

	/**
	 * This function prepares Billing reports.
	 * <p>
	 * <p>
	 * 
	 * @param strDate
	 *            <code>String</code> is the date for which reports to be
	 *            generated.
	 *            <p>
	 *            <p>
	 */
	public static void prepareBillingReports(String strDate) {

		String strDateFrom = "";
		String strDateTo = "";
		String dbStr = "";
		String strDateTemp = "";
		
		if (strDate.equals("")) {
			// dbStr =
			// "Select a.flno AFLNO, a.alc3 AALC3, a.regn AREGN, a.onbl AONBL, a.ttyp ATTYP, b.flno BFLNO, b.alc3 BALC3, b.regn BREGN, b.ofbl BOFBL, b.ttyp BTTYP, b.act5 BACT5, b.ftyp BFTYP from afttab a, afttab b where b.AIRB BETWEEN TO_CHAR(SYSDATE-2,'YYYYMMDD')||'160000' AND TO_CHAR(SYSDATE-1,'YYYYMMDD')||'155959' AND a.URNO = B.RKEY and B.RKEY <> B.URNO ORDER BY b.RKEY";

			// dbStr =
			// "Select trim(a.ALC2)||trim(a.FLTN)||trim(a.FLNS) as AFLNO, a.alc3 AALC3, a.regn AREGN, a.land ALAND, a.ttyp ATTYP, a.act5 AACT5, a.rkey ARKEY, trim(b.ALC2)||trim(b.FLTN)||trim(b.FLNS) as BFLNO, b.alc3 BALC3, b.regn BREGN, b.airb BAIRB, b.ttyp BTTYP from afttab a, afttab b where b.AIRB BETWEEN TO_CHAR(SYSDATE-2,'YYYYMMDD')||'160000' AND TO_CHAR(SYSDATE-1,'YYYYMMDD')||'155959' AND a.URNO = B.RKEY and B.RKEY <> B.URNO ORDER BY b.RKEY";

			// 2012-02-15 Update by JJ, add the csgn rem1 ftyp paid field to
			// report
			// dbStr =
			// "Select (select FCSF from FCSTAB where UFLN=a.FLNO and ROWNUM=1) as AFLNO, a.alc3 AALC3, trim(a.regn) AREGN, trim(a.land) ALAND, trim(a.ttyp) ATTYP, trim(a.act5) AACT5, a.rkey ARKEY, trim(b.pstd) APSTA, (select FCSF from FCSTAB where UFLN=b.FLNO and ROWNUM=1) as BFLNO, b.alc3 BALC3, b.regn BREGN, trim(b.airb) BAIRB, b.ttyp BTTYP, trim(b.ofbl) BOFBL,GETPLBSTATUS(b.pstd) PLB,trim(a.vipa) VIPA,trim(b.vipd)VIPD,trim(b.adid) ADID,trim(a.psta) PSTA,trim(b.onbl) BONBL from afttab a, afttab b where a.URNO = B.RKEY and B.RKEY <> B.URNO and a.rkey in (select c.rkey from afttab c, afttab d where d.AIRB BETWEEN TO_CHAR(SYSDATE-2,'YYYYMMDD')||'160000' AND TO_CHAR(SYSDATE-1,'YYYYMMDD')||'155959' and c.URNO = d.RKEY and d.RKEY <> d.URNO) ORDER BY a.FLNO,a.RKEY,b.TIFA";
			dbStr = "Select (select FCSF from FCSTAB where UFLN=a.FLNO and ROWNUM=1) as AFLNO, a.alc3 AALC3, trim(a.regn) AREGN, trim(a.csgn) ACSGN, trim(a.land) ALAND, trim(a.ttyp) ATTYP, trim(a.act5) AACT5, a.rkey ARKEY, trim(b.pstd) APSTA, (select FCSF from FCSTAB where UFLN=b.FLNO and ROWNUM=1) as BFLNO, b.alc3 BALC3, b.regn BREGN, trim(b.csgn) BCSGN, trim(b.ftyp) BFTYP, trim(b.rem1) BREM1, trim(b.paid) BPAID, trim(b.airb) BAIRB, b.ttyp BTTYP, trim(b.ofbl) BOFBL,GETPLBSTATUS(b.pstd) PLB,trim(a.vipa) VIPA,trim(b.vipd)VIPD,trim(b.adid) ADID,trim(a.psta) PSTA,trim(b.onbl) BONBL, trim(b.rcno) BRCNO from afttab a, afttab b where a.URNO = B.RKEY and B.RKEY <> B.URNO and a.rkey in (select c.rkey from afttab c, afttab d where d.AIRB BETWEEN TO_CHAR(SYSDATE-2,'YYYYMMDD')||'160000' AND TO_CHAR(SYSDATE-1,'YYYYMMDD')||'155959' and c.URNO = d.RKEY and d.RKEY <> d.URNO) ORDER BY a.FLNO,a.RKEY,b.TIFA";
		} else {
			try {
				strDateTemp = tanDt.getPreviousDate(strDate + "000000");
				strDateFrom = tanDt.getGMTDate(strDateTemp.substring(0, 8)
						+ "000000", -8);
				strDateTo = tanDt.getGMTDate(strDateTemp.substring(0, 8)
						+ "235959", -8);
			} catch (Exception e) {
				log.error("Error .. Converting user inserted date to GMT Date. Error in function prepareBillingReports(), Error :"
						+ e.toString());
			}

			// dbStr =
			// " Select a.flno AFLNO, a.alc3 AALC3, a.regn AREGN, a.onbl AONBL, a.ttyp ATTYP, b.flno BFLNO, b.alc3 BALC3, b.regn BREGN, b.ofbl BOFBL, b.ttyp BTTYP, b.act5 BACT5, b.ftyp BFTYP from afttab a, afttab b where b.AIRB BETWEEN '"
			// + strDateFrom + "' AND '" + strDateTo +
			// "' AND a.URNO = B.RKEY and B.RKEY <> B.URNO ORDER BY b.RKEY ";

			// dbStr =
			// "Select trim(a.ALC2)||trim(a.FLTN)||trim(a.FLNS) as AFLNO, a.alc3 AALC3, a.regn AREGN, a.land ALAND, a.ttyp ATTYP, a.act5 AACT5, a.rkey ARKEY, trim(b.ALC2)||trim(b.FLTN)||trim(b.FLNS) as BFLNO, b.alc3 BALC3, b.regn BREGN, b.airb BAIRB, b.ttyp BTTYP from afttab a, afttab b where b.STOD BETWEEN '"
			// + strDateFrom + "' AND '" + strDateTo +
			// "' AND a.URNO = B.RKEY and B.RKEY <> B.URNO ORDER BY b.RKEY ";

			dbStr = "Select (select FCSF from FCSTAB where UFLN=a.FLNO and ROWNUM=1) as AFLNO, a.alc3 AALC3, trim(a.regn) AREGN, trim(a.csgn) ACSGN, trim(a.land) ALAND, trim(a.ttyp) ATTYP, trim(a.act5) AACT5, a.rkey ARKEY, trim(b.pstd) APSTA, (select FCSF from FCSTAB where UFLN=b.FLNO and ROWNUM=1) as BFLNO, b.alc3 BALC3, b.regn BREGN, trim(b.csgn) BCSGN, trim(b.ftyp) BFTYP, trim(b.rem1) BREM1, trim(b.paid) BPAID, trim(b.airb) BAIRB, b.ttyp BTTYP, trim(b.ofbl) BOFBL,GETPLBSTATUS(b.pstd) PLB,trim(a.vipa) VIPA,trim(b.vipd)VIPD,trim(b.adid) ADID,trim(a.psta) PSTA,trim(b.onbl) BONBL, trim(b.rcno) BRCNO from afttab a, afttab b where a.URNO = B.RKEY and B.RKEY <> B.URNO and a.rkey in (select c.rkey from afttab c, afttab d where d.AIRB BETWEEN '"
					+ strDateFrom
					+ "' AND '"
					+ strDateTo
					+ "' and c.URNO = d.RKEY and d.RKEY <> d.URNO) ORDER BY a.FLNO,a.RKEY,b.TIFA";

			// System.out.println("Date from : " + strDateFrom + "  Date To: " +
			// strDateTo);
			// System.out.println("DB String : " + dbStr);

		}

		try {
			ResultSet rs = dbCon.getResultSet(dbStr);
			// System.out.println("Number of rows : " + dbCon.getTotalRows(rs));
			// rs.beforeFirst();
			ArrayList exceptionList = new ArrayList();
			if (rs != null) {

				// Billing Header reports generation - H
				exceptionList = billingHeaderReports(rs, strDate);

				// Billing Detail Report generation - D
				rs.beforeFirst();
				billingDetailReports(rs, strDate, exceptionList);

				// Billing Statistical Report preparation - S
				rs.beforeFirst();
				billingStatisticalReport(rs, strDate);

			} else {
				log.error("Result Set is Null: Error in function prepareBillingReports() ");
			}
		} catch (Exception e) {
			log.error("Error in getting result set: Error in function prepareBillingReports(), Error : "
					+ e.toString());
			System.exit(0);
		}

	}

	/**
	 * This function prepares Billing Header Reports - H for Header & E for
	 * Header Exception.
	 * <p>
	 * <p>
	 * 
	 * @param strDate
	 *            <code>String</code> is the date for which reports to be
	 *            generated.
	 * @param rs
	 *            <code>ResultSet</code> is the ResultSet which contains the
	 *            records to be processed.
	 *            <p>
	 *            <p>
	 */
	public static ArrayList billingHeaderReports(ResultSet rs, String strDate) {
		String strFileLine = "";
		String strFileLine1 = "";
		String strTemp = "";
		String regn1 = "";
		String aregn = "";
		String bregn = "";
		String aflno = "";
		String aalc3 = "";
		String aland = "";
		String afltType = "";
		String bfltType = "";
		String arrVipStatus = "0";
		String depVipStatus = "0";
		String bflno = "";
		String balc3 = "";
		String bairb = "";
		String aact5 = "";

		// 2012-02-15 Update by JJ, CSGN, start
		String acsgn = "";
		String bcsgn = "";
		// 2012-02-15 Update by JJ, CSGN, end

		String parkingStandVal = "";
		String noLanding = "";
		String landIndicator = "";
		String frtIndicator = "N";
		String billStatus = " ";
		String receiptNumber = "         ";
		String billCycleDate = "";
		String strDateFrom = "";
		String strDateTo = "";
		String dbStr = "";
		String depDetFile = "";
		String depExpFile = "";
		String depDetFileTemp = "";
		String depExpFileTemp = "";
		String arkey = "";
		String nextRkey = "";
		String excepIndicator = "";
		ArrayList excepList = new ArrayList();
		String strToWrite = "";
		String strExcepToWrite = "";
		String bofbl = "";
		String bonbl = "";

		int txtCount = 0;
		int expCount = 0;
		int detCount = 0;
		int detExpCount = 0;
		int parkingStandCount = 0;
		BufferedWriter bw = null;
		BufferedWriter bw1 = null;
		String headerRecordsVal = "";
		String detailRecordsVal = "";
		String excepHeaderRecordsVal = "";
		String excepDetailRecordsVal = "";
		String apsta = "";

		if (strDate.equals("")) {
			depDetFile = bilpath + "H" + year + month + billCycle + day + "_new";
			depExpFile = bilpath + "E" + year + month + billCycle + day + "_new";
			billCycleDate = year4 + month + billCycle + day + "   ";
		} else {
			try {
				strDate = tanDt.getPreviousDate(strDate + "000000");
				billCycle = tanDt.getBillingCycle1(strDate);
			} catch (Exception e) {
				log.error("Error .. Converting user inserted date to GMT Date. Error in function billingHeaderReports(), Error :"
						+ e.toString());
			}

			depDetFile = bilpath + "H" + strDate.substring(2, 6) + billCycle
					+ strDate.substring(6, 8) + "_new";
			depExpFile = bilpath + "E" + strDate.substring(2, 6) + billCycle
					+ strDate.substring(6, 8) + "_new";
			billCycleDate = strDate.substring(0, 6) + billCycle
					+ strDate.substring(6, 8) + "   ";
		}

		depDetFileTemp = bilpath + "HTEMP";
		depExpFileTemp = bilpath + "ETEMP";

		try {
			bw = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(depDetFileTemp), "UTF-8"));
			bw1 = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(depExpFileTemp), "UTF-8"));
		} catch (Exception e) {
			log.error("Error .. While creating FileOutputStream. Error in function billingHeaderReports(), Error :"
					+ e.toString());
		}

		try {

			// To create first row in billing header report file.
			int headerRecords = 0;
			int detailRecords = 0;

			String prevRkey = "";

			// while(rs.next()){
			// arkey = rs.getString("ARKEY");
			// if(!arkey.equals(prevRkey)) headerRecords++;
			// detailRecords++;
			// prevRkey = arkey;
			// }
			//
			// headerRecordsVal = Long.toString(headerRecords);
			// detailRecordsVal = Long.toString(detailRecords);
			//
			// if (headerRecordsVal==null || headerRecordsVal.equals(""))
			// headerRecordsVal = "    ";
			// if(headerRecordsVal.length() < 4){
			// strTemp = "";
			// for(int i=0; i<4-headerRecordsVal.length(); i++) strTemp =
			// strTemp + " ";
			// headerRecordsVal =strTemp+ headerRecordsVal ;
			// }
			//
			// if (detailRecordsVal==null || detailRecordsVal.equals(""))
			// detailRecordsVal = "        ";
			// if(detailRecordsVal.length() < 8){
			// strTemp = "";
			// for(int i=0; i<8-detailRecordsVal.length(); i++) strTemp =
			// strTemp + " ";
			// detailRecordsVal = strTemp+detailRecordsVal;
			// }
			//
			// // Record length --- Total 102 characters ....
			// strTemp = "";for(int i=0; i<21; i++) strTemp = strTemp + " ";
			// strFileLine = strTemp + detailRecordsVal+ headerRecordsVal ;
			// strTemp = "";for(int i=0; i<69; i++) strTemp = strTemp + " ";
			// strFileLine = strFileLine + strTemp;
			//
			// bw.write(strFileLine); bw.newLine();

			rs.beforeFirst();
			// upto here header report first line generation ..

			try {
				if (rs != null) {
					while (rs.next()) {
						strFileLine = "";
						excepIndicator = "N";
						// Flight Number
						aflno = rs.getString("AFLNO");
						if (aflno == null || aflno.equals("")) {
							aflno = "        ";
							// 2012-02-15 update by JJ
							// only both flightNo and registration are null or
							// blank will be include in exception list.
							// excepIndicator = "Y";
						}
						if (aflno.length() < 8) {
							strTemp = "";
							for (int i = 0; i < 8 - aflno.length(); i++)
								strTemp = strTemp + " ";
							aflno = aflno + strTemp;
						}

						// Airline code
						aalc3 = rs.getString("AALC3");
						if (aalc3 == null || aalc3.equals("")) {
							aalc3 = "   ";
							excepIndicator = "Y";
						}
						if (aalc3.length() < 3) {
							strTemp = "";
							for (int i = 0; i < 3 - aalc3.length(); i++)
								strTemp = strTemp + " ";
							aalc3 = aalc3 + strTemp;
						}

						// REGN field contains CHAR(12) in DB
						regn1 = rs.getString("AREGN");
						aregn = rs.getString("AREGN");
						if (aregn == null || aregn.equals("")) {
							aregn = "          ";
							// 2012-02-15 update by JJ, start
							// only both flightNo and registration are null or
							// blank will be include in exception list.
							// excepIndicator = "Y";
						}

						if (aregn.length() < 10) {
							strTemp = "";
							for (int i = 0; i < 10 - aregn.length(); i++)
								strTemp = strTemp + " ";
							aregn = aregn + strTemp;
						}

						if (aregn.length() > 10)
							aregn = aregn.substring(0, 10);

						// 2012-02-15 Update by JJ, CSGN field contains CHAR(8)
						// in DB, start
						acsgn = rs.getString("ACSGN");
						if (acsgn == null || acsgn.equals("")) {
							acsgn = "        ";
							excepIndicator = "Y";
						}

						if (acsgn.length() < 8) {
							strTemp = "";
							for (int i = 0; i < 8 - acsgn.length(); i++) {
								strTemp += " ";
							}
							acsgn = acsgn + strTemp;
						}
						// 2012-02-15 Update by JJ, CSGN field contains CHAR(8)
						// in DB, end

						// 2012-02-15 update by JJ, start
						// no flightNo + no registration -> exception report
						if (aflno.trim().equals("") && aregn.trim().equals("")) {
							excepIndicator = "Y";
						}
						// 2012-02-15 update by JJ, end

						aland = rs.getString("ALAND");
						if (aland != null && !aland.equals(""))
							try {
								aland = tanDt.getLocalDate(aland, 8);
							} catch (Exception e) {
								log.error("Error .. Converting LAND time to GMTDate. LAND time in DB: "
										+ aland
										+ " for Flight: "
										+ aflno
										+ " Error in function billingHeaderReports(), Error :"
										+ e.toString());
								aland = "              ";
							}
						else {
							aland = "              ";
							excepIndicator = "Y";
						}

						afltType = rs.getString("ATTYP");
						if (afltType != null && !afltType.equals("")) {
							if (afltType.length() > 2)
								afltType = afltType.substring(0, 1);
						} else {
							afltType = " ";
							// System.out.println("afltType");
							excepIndicator = "Y";
							log.error("Error in billingHeaderReports(): Flight type is mandatory field. Error: TTYP is empty for flight : "
									+ aflno);
						}
						// arrVipStatus=rs.getString("VIPA");
						// if(arrVipStatus!=null && !arrVipStatus.equals(""))
						// {
						// if(arrVipStatus.equals("X"))arrVipStatus="1";
						// else arrVipStatus="0";
						//
						// }
						// else
						// {
						// arrVipStatus="0";
						// }

						if (afltType.equals("T"))
							noLanding = "0";
						else
							noLanding = "1";

						if (afltType.equals("T"))
							landIndicator = "1";
						else
							landIndicator = "0";

						// if(afltType.equals("F")) frtIndicator = "Y";
						// else frtIndicator = "N";

						aact5 = rs.getString("AACT5");
						if (aact5 == null || aact5.equals("")) {
							aact5 = "     ";
							excepIndicator = "Y";
						}
						if (aact5.length() < 5) {
							strTemp = "";
							for (int i = 0; i < 5 - aact5.length(); i++)
								strTemp = strTemp + " ";
							aact5 = aact5 + strTemp;
						}

						arkey = rs.getString("ARKEY");
						nextRkey = "";
						parkingStandCount = 1;
						parkingStandVal = "";
						int recCnt = 0;
						// System.out.println(aflno+":"+excepIndicator);

						do {
							bofbl = rs.getString("BOFBL");
							bonbl = rs.getString("BONBL");
							// apsta=rs.getRecordName("APSTA");
							if (!rs.next())
								break;

							nextRkey = rs.getString("ARKEY");
							// System.out.println("arkey :" + arkey +
							// " nextRkey :" + nextRkey);
							if (arkey.equals(nextRkey)) {
								if (recCnt == 0) {
									if (getExceptionIndicator(bofbl, recCnt)
											.equals("Y")) {
										excepIndicator = "Y";
										// System.out.println("bofbl");
									}
								} else {
									if (getExceptionIndicator(bonbl, recCnt)
											.equals("Y")) {
										excepIndicator = "Y";
										// System.out.println("bonbl");
									}

								}
								recCnt++;
								parkingStandCount++;
								// if(!rs.next()) {
								// break;
								// }
							} else
								break;

						} while (true);
						rs.previous();
						// System.out.println(aflno+":"+excepIndicator);
						parkingStandVal = (parkingStandCount < 10) ? (" " + parkingStandCount)
								: ("" + parkingStandCount);

						bflno = rs.getString("BFLNO");
						if (bflno == null || bflno.equals("")) {
							bflno = "        ";
							// 2012-02-15 update by JJ, start
							// only both flightNo and registration are null or
							// blank will be include in exception list.
							// excepIndicator = "Y";
						}
						if (bflno.length() < 8) {
							strTemp = "";
							for (int i = 0; i < 8 - bflno.length(); i++)
								strTemp = strTemp + " ";
							bflno = bflno + strTemp;
						}

						balc3 = rs.getString("BALC3");
						if (balc3 == null || balc3.equals("")) {
							balc3 = "   ";
							excepIndicator = "Y";
						}
						if (balc3.length() < 3) {
							strTemp = "";
							for (int i = 0; i < 3 - balc3.length(); i++)
								strTemp = strTemp + " ";
							balc3 = balc3 + strTemp;
						}

						// REGN field contains CHAR(12) in DB
						bregn = rs.getString("BREGN");
						if (bregn == null || bregn.equals("")) {
							bregn = "          ";
							// 2012-02-15 update by JJ, start
							// only both flightNo and registration are null or
							// blank will be include in exception list.
							// excepIndicator = "Y";
						}
						if (bregn.length() < 10) {
							strTemp = "";
							for (int i = 0; i < 10 - bregn.length(); i++)
								strTemp = strTemp + " ";
							bregn = bregn + strTemp;
						}
						if (bregn.length() > 10)
							bregn = bregn.substring(0, 10);

						// 2012-02-15 Update by JJ, CSGN field contains CHAR(8)
						// in DB, start
						bcsgn = rs.getString("BCSGN");
						if (bcsgn == null || bcsgn.equals("")) {
							bcsgn = "        ";
							excepIndicator = "Y";
						}
						if (bcsgn.length() < 8) {
							strTemp = "";
							for (int i = 0; i < 8 - bcsgn.length(); i++) {
								strTemp += " ";
							}
							bcsgn = bcsgn + strTemp;
						}
						// 2012-02-15 Update by JJ, CSGN field contains CHAR(8)
						// in DB, end

						// 2012-02-15 update by JJ, start
						// no flightNo + no registration -> exception report
						if (bflno.trim().equals("") && bregn.trim().equals("")) {
							excepIndicator = "Y";
						}
						// 2012-02-15 update by JJ, end

						// 2012-02-20 update by JJ, cash indicator
						billStatus = rs.getString("BPAID");
						if (billStatus == null || billStatus.equals("")) {
							billStatus = " ";
						}
						
						// 2012-03-19 update by JJ, add receipt numer
						receiptNumber = rs.getString("BRCNO");
						if (receiptNumber == null || receiptNumber.equals("")) {
							receiptNumber = "         ";
						}
						if (receiptNumber.length() < 9) {
							strTemp = "";
							for (int i = 0; i < 9 - receiptNumber.length(); i++) {
								strTemp += " ";
							}
							receiptNumber += strTemp;
						}
						if (receiptNumber.length() > 9) {
							receiptNumber = receiptNumber.substring(0, 9);
						}
						
						bairb = rs.getString("BAIRB");
						if (bairb != null && !bairb.equals(""))
							try {
								bairb = tanDt.getLocalDate(bairb, 8);
							} catch (Exception e) {
								log.error("Error .. Converting AIRB time to GMTDate. AIRB time in DB: "
										+ bairb
										+ " for Flight: "
										+ bflno
										+ " Error in function billingHeaderReports(), Error :"
										+ e.toString());
								bairb = "              ";
							}
						else {
							bairb = "              ";
							excepIndicator = "Y";
						}

						bfltType = rs.getString("BTTYP");
						if (bfltType != null && !bfltType.equals("")) {
							if (bfltType.length() > 2)
								bfltType = bfltType.substring(0, 1);
						} else {
							bfltType = " ";
							// System.out.println("bfltType");
							excepIndicator = "Y";
							log.error("Error in billingHeaderReports(): Flight type is mandatory field. Error: TTYP is empty for flight : "
									+ bflno);
						}
						// depVipStatus=rs.getString("VIPD");
						// if(depVipStatus!=null && !depVipStatus.equals(""))
						// {
						// if(depVipStatus.equals("X"))depVipStatus="1";
						// else depVipStatus="0";
						//
						// }
						// else
						// {
						// depVipStatus="0";
						// }

						// 2012-02-15 update by JJ, add the csgn field into
						// report, start
						// strFileLine = strFileLine + aflno + aalc3 + aregn
						// + aland.substring(0, 12) + afltType
						// + arrVipStatus + bflno + balc3 + bregn
						// + bairb.substring(0, 12) + bfltType
						// + depVipStatus + aact5 + billCycleDate
						// + parkingStandVal + noLanding + landIndicator
						// + frtIndicator + billStatus + receiptNumber;
						strFileLine = strFileLine + aflno + aalc3 + aregn
								+ acsgn + aland.substring(0, 12) + afltType
								+ arrVipStatus + bflno + balc3 + bregn + bcsgn
								+ bairb.substring(0, 12) + bfltType
								+ depVipStatus + aact5 + billCycleDate
								+ parkingStandVal + noLanding + landIndicator
								+ frtIndicator + billStatus + receiptNumber;
						// 2012-02-15 update by JJ, add the csgn field into
						// report, end

						// System.out.println(aflno+":"+excepIndicator);
						// System.out.println("strFileLine : " + strFileLine);

						// if(regn1 == null || regn1.equals("")){
						// bw1.write(strFileLine); bw1.newLine();
						// expCount++;
						// }
						if (excepList.size() != 0) {
							if (excepList.contains(arkey)) {
								excepIndicator.equals("Y");
							}

						}
						if (excepIndicator.equals("Y")) {
							// bw1.write(strFileLine); bw1.newLine();
							strExcepToWrite += strFileLine + ";";
							if (!excepList.contains(arkey)) {
								excepList.add(arkey);
							}
							detExpCount += parkingStandCount;
							expCount++;
						} else if (excepIndicator.equals("N")) {
							strToWrite += strFileLine + ";";
							// bw.write(strFileLine); bw.newLine();
							detCount += parkingStandCount;

							txtCount++;
						}

					}
					// System.out.println("Header Records Count : " + txtCount +
					// "  Header Exception Count: " + expCount);

					headerRecordsVal = Integer.toString(txtCount);
					detailRecordsVal = Integer.toString(detCount);
					excepHeaderRecordsVal = Integer.toString(expCount);
					excepDetailRecordsVal = Integer.toString(detExpCount);
					if (headerRecordsVal == null || headerRecordsVal.equals(""))
						headerRecordsVal = "    ";
					if (headerRecordsVal.length() < 4) {
						strTemp = "";
						for (int i = 0; i < 4 - headerRecordsVal.length(); i++)
							strTemp = strTemp + " ";
						headerRecordsVal = strTemp + headerRecordsVal;
					}

					if (detailRecordsVal == null || detailRecordsVal.equals(""))
						detailRecordsVal = "        ";
					if (detailRecordsVal.length() < 8) {
						strTemp = "";
						for (int i = 0; i < 8 - detailRecordsVal.length(); i++)
							strTemp = strTemp + " ";
						detailRecordsVal = strTemp + detailRecordsVal;
					}

					// Record length --- Total 102 characters ....
					strTemp = "";
					for (int i = 0; i < 21; i++)
						strTemp = strTemp + " ";
					strFileLine = strTemp + detailRecordsVal + headerRecordsVal;
					strTemp = "";
					for (int i = 0; i < 69; i++)
						strTemp = strTemp + " ";
					strFileLine1 = strFileLine + strTemp;

					bw.write(strFileLine1);
					bw.newLine();

					if (excepHeaderRecordsVal == null
							|| excepHeaderRecordsVal.equals(""))
						excepHeaderRecordsVal = "    ";
					if (excepHeaderRecordsVal.length() < 4) {
						strTemp = "";
						for (int i = 0; i < 4 - excepHeaderRecordsVal.length(); i++)
							strTemp = strTemp + " ";
						excepHeaderRecordsVal = strTemp + excepHeaderRecordsVal;
					}

					if (excepDetailRecordsVal == null
							|| excepDetailRecordsVal.equals(""))
						excepDetailRecordsVal = "        ";
					if (excepDetailRecordsVal.length() < 8) {
						strTemp = "";
						for (int i = 0; i < 8 - excepDetailRecordsVal.length(); i++)
							strTemp = strTemp + " ";
						excepDetailRecordsVal = strTemp + excepDetailRecordsVal;
					}

					// Record length --- Total 102 characters ....
					strTemp = "";
					for (int i = 0; i < 21; i++)
						strTemp = strTemp + " ";
					strFileLine = strTemp + excepDetailRecordsVal
							+ excepHeaderRecordsVal;
					strTemp = "";
					for (int i = 0; i < 69; i++)
						strTemp = strTemp + " ";
					strFileLine1 = strFileLine + strTemp;

					bw1.write(strFileLine1);
					bw1.newLine();

					StringTokenizer sToken = new StringTokenizer(strToWrite,
							";");
					while (sToken.hasMoreTokens()) {
						bw.write(sToken.nextToken());
						bw.newLine();
					}
					bw.flush();
					bw.close();
					StringTokenizer sToken1 = new StringTokenizer(
							strExcepToWrite, ";");
					while (sToken1.hasMoreTokens()) {
						bw1.write(sToken1.nextToken());
						bw1.newLine();
					}
					bw1.flush();
					bw1.close();
				}
			} catch (Exception e) {
				log.error("While reading the info, its wrong() Error in function billingHeaderReports(), Error : "
						+ e.toString());
			}

		} catch (Exception e) {
			log.error("Error in getting result set: Error in function billingHeaderReports(), Error : "
					+ e.toString());
			System.exit(0);
		}

		if (encryptFile(depDetFileTemp, depDetFile) != 0)
			log.error("Error while encrypting the file in billingHeaderReports() function");

		if (encryptFile(depExpFileTemp, depExpFile) != 0)
			log.error("Error while encrypting the file in billingHeaderReports() function");
		return excepList;

	}

	private static String getExceptionIndicator(String depTime, int cnt) {
		String excepIndicator = "";
		try {

			if (depTime == null || depTime.equals("")) {

				excepIndicator = "Y";
			}

		}

		catch (Exception e) {
		}

		return excepIndicator;
	}

	/**
	 * This function prepares Billing Detail Reports - D for detail and X for
	 * exception.
	 * <p>
	 * <p>
	 * 
	 * @param strDate
	 *            <code>String</code> is the date for which reports to be
	 *            generated.
	 * @param rs
	 *            <code>ResultSet</code> is the ResultSet which contains the
	 *            records to be processed.
	 *            <p>
	 *            <p>
	 */
	public static void billingDetailReports(ResultSet rs, String strDate,
			ArrayList excepList) {
		String strFileLine = "";
		String strTemp = "";
		String strTemp2 = "";
		String strTemp3 = "";
		String regn1 = "";
		String aregn = "";
		String bregn = "";
		String aflno = "";
		String aalc3 = "";
		String aland = "";
		String bflno = "";
		String balc3 = "";
		String bairb = "";
		String bofbl = "";
		String aact5 = "";
		String parkingStandCode = "";
		String arrParkingStandCode = "";
		String bonbl = "";
		String plbUsed = "";

		// 2012-02-15 Update by JJ, CSGN FTYP REM1
		String acsgn = "";
		String bcsgn = "";
		String bftyp = "";
		String brem1 = "";

		String strDateFrom = "";
		String strDateTo = "";
		String dbStr = "";
		String depDetFile = "";
		String depExpFile = "";
		String depDetFileTemp = "";
		String depExpFileTemp = "";
		String arkey = "";
		String prevRkey = "";
		String adid = "";
		int parkingStandCount = 0;
		String arrIndicator = "";
		long txtCount = 0;
		long expCount = 0;

		BufferedWriter bw = null;
		BufferedWriter bw1 = null;

		if (strDate.equals("")) {
			depDetFile = bilpath + "D" + year + month + billCycle + day + "_new";
			depExpFile = bilpath + "X" + year + month + billCycle + day + "_new";
		} else {
			try {
				strDate = tanDt.getPreviousDate(strDate + "000000");
				billCycle = tanDt.getBillingCycle1(strDate);
			} catch (Exception e) {
				log.error("Error .. Converting user inserted date to GMT Date. Error in function billingDetailReports(), Error :"
						+ e.toString());
			}
			depDetFile = bilpath + "D" + strDate.substring(2, 6) + billCycle
					+ strDate.substring(6, 8) + "_new";
			depExpFile = bilpath + "X" + strDate.substring(2, 6) + billCycle
					+ strDate.substring(6, 8) + "_new";
		}

		depDetFileTemp = bilpath + "DTEMP";
		depExpFileTemp = bilpath + "XTEMP";

		try {
			bw = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(depDetFileTemp), "UTF-8"));
			bw1 = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(depExpFileTemp), "UTF-8"));
		} catch (Exception e) {
			log.error("Error .. While creating FileOutputStream. Error in function billingDetailReports(), Error :"
					+ e.toString());
		}

		try {
			if (rs != null) {
				while (rs.next()) {
					parkingStandCount = 0;
					strFileLine = "";

					// Flight Number
					strTemp2 = "AFLNO";
					aflno = rs.getString("AFLNO");
					if (aflno == null || aflno.equals(""))
						aflno = "        ";
					if (aflno.length() < 8) {
						strTemp = "";
						for (int i = 0; i < 8 - aflno.length(); i++)
							strTemp = strTemp + " ";
						aflno = aflno + strTemp;
					}
					strTemp3 = aflno;
					// Airline code
					strTemp2 = "AALC3";
					aalc3 = rs.getString("AALC3");
					if (aalc3 == null || aalc3.equals(""))
						aalc3 = "   ";
					if (aalc3.length() < 3) {
						strTemp = "";
						for (int i = 0; i < 3 - aalc3.length(); i++)
							strTemp = strTemp + " ";
						aalc3 = aalc3 + strTemp;
					}

					// REGN field contains CHAR(12) in DB
					strTemp2 = "AREGN";
					regn1 = rs.getString("AREGN");
					aregn = rs.getString("AREGN");
					if (aregn == null || aregn.equals(""))
						aregn = "          ";
					if (aregn.length() < 10) {
						strTemp = "";
						for (int i = 0; i < 10 - aregn.length(); i++)
							strTemp = strTemp + " ";
						aregn = aregn + strTemp;
					}
					if (aregn.length() > 10)
						aregn = aregn.substring(0, 10);

					// 2012-02-15 Update by JJ, CSGN field contains CHAR(8) in
					// DB, start
					strTemp2 = "ACSGN";
					acsgn = rs.getString("ACSGN");
					if (acsgn == null || acsgn.equals("")) {
						acsgn = "        ";
					}
					if (acsgn.length() < 8) {
						strTemp = "";
						for (int i = 0; i < 8 - acsgn.length(); i++) {
							strTemp += " ";
						}
						acsgn = acsgn + strTemp;
					}
					// 2012-02-15 Update by JJ, CSGN field contains CHAR(8) in
					// DB, end

					strTemp2 = "ALAND";
					aland = rs.getString("ALAND");
					if (aland != null && !aland.equals(""))
						try {
							aland = tanDt.getLocalDate(aland, 8);
						} catch (Exception e) {
							log.error("Error .. Converting LAND time to GMTDate. LAND time in DB: "
									+ aland
									+ " for Flight: "
									+ aflno
									+ " Error in function billingDetailReports(), Error :"
									+ e.toString());
							aland = "              ";
						}
					else
						aland = "              ";

					strTemp2 = "AACT5";
					aact5 = rs.getString("AACT5");
					if (aact5 == null || aact5.equals(""))
						aact5 = "     ";
					if (aact5.length() < 5) {
						strTemp = "";
						for (int i = 0; i < 5 - aact5.length(); i++)
							strTemp = strTemp + " ";
						aact5 = aact5 + strTemp;
					}

					// Parking Stand Code
					strTemp2 = "APSTA";
					parkingStandCode = rs.getString("APSTA");
					// arrParkingStandCode=rs.getString("PSTA");
					// if(parkingStandCode.equals(arrParkingStandCode))
					// {
					// arrIndicator="A";
					// }
					// else
					// {
					// arrIndicator="B";
					// }
					arkey = rs.getString("ARKEY");
					if (!rs.isFirst()) {
						rs.previous();
						String prevArkey = rs.getString("ARKEY");
						if (!(arkey.equals(prevArkey))) {

							arrIndicator = "A";

						} else {

							arrIndicator = "B";
						}

						rs.next();
					}
					if (parkingStandCode == null || parkingStandCode.equals(""))
						parkingStandCode = "   ";
					if (parkingStandCode.length() < 4) {
						strTemp = "";
						for (int i = 0; i < 4 - parkingStandCode.length(); i++)
							strTemp = strTemp + " ";
						parkingStandCode = parkingStandCode + strTemp;
					}

					/*
					 * bairb = rs.getString("BAIRB"); if (bairb!= null &&
					 * !bairb.equals("")) bairb = tanDt.getLocalDate(bairb,8);
					 * else{ bairb = "            ";
					 */

					// From here
					// Below script is to generate bairb, if bairb is empty ...
					// bit complex script.
					strTemp2 = "BAIRB";
					bairb = rs.getString("BAIRB");
					if (bairb != null && !bairb.equals(""))
						try {
							bairb = tanDt.getLocalDate(bairb, 8);
						} catch (Exception e) {
							log.error("Error .. Converting AIRB time to GMTDate. AIRB time in DB: "
									+ bairb
									+ " for Flight: "
									+ aflno
									+ " Error in function billingDetailReports(), Error :"
									+ e.toString());
							bairb = "              ";
						}
					else {
						bairb = "";
						arkey = rs.getString("ARKEY");
						String nextRkey = "";

						// rs.next();
						do {
							if (!rs.next())
								break;

							nextRkey = rs.getString("ARKEY");
							// System.out.println("arkey :" + arkey +
							// " nextRkey :" +
							// nextRkey+"pstandcnt"+parkingStandCount);
							if (arkey.equals(nextRkey)) {
								parkingStandCount++;
								/*
								 * Commented of by Alphy since these lines are
								 * not needed and are skipping records
								 */
								// if(!rs.next()) {
								// break;
								// }
							} else
								break;

						} while (true);

						rs.previous();
						bairb = rs.getString("BAIRB");
						if (bairb != null && !bairb.equals("")) {
							try {
								bairb = tanDt.getLocalDate(bairb, 8);
							} catch (Exception e) {
								log.error("Error .. Converting AIRB time to GMTDate. AIRB time in DB: "
										+ bairb
										+ " for Flight: "
										+ aflno
										+ " Error in function billingDetailReports(), Error :"
										+ e.toString());
								bairb = "              ";
							}
						} else
							bairb = "              ";

						// System.out.println("AIRB :" + bairb);
						bflno = rs.getString("BFLNO");
						if (bflno == null || bflno.equals(""))
							bflno = "        ";
						if (bflno.length() < 8) {
							strTemp = "";
							for (int i = 0; i < 8 - bflno.length(); i++)
								strTemp = strTemp + " ";
							bflno = bflno + strTemp;
						}
						balc3 = rs.getString("BALC3");
						if (balc3 == null || balc3.equals(""))
							balc3 = "   ";
						if (balc3.length() < 3) {
							strTemp = "";
							for (int i = 0; i < 3 - balc3.length(); i++)
								strTemp = strTemp + " ";
							balc3 = balc3 + strTemp;
						}

						// REGN field contains CHAR(12) in DB
						strTemp2 = "BREGN";
						bregn = rs.getString("BREGN");
						if (bregn == null || bregn.equals(""))
							bregn = "          ";
						if (bregn.length() < 10) {
							strTemp = "";
							for (int i = 0; i < 10 - bregn.length(); i++)
								strTemp = strTemp + " ";
							bregn = bregn + strTemp;
						}
						if (bregn.length() > 10)
							bregn = bregn.substring(0, 10);

						// 2012-02-15 Update by JJ, CSGN field contains CHAR(8)
						// in DB, start
						strTemp2 = "BCSGN";
						bcsgn = rs.getString("BCSGN");
						if (bcsgn == null || bcsgn.equals("")) {
							bcsgn = "        ";
						}
						if (bcsgn.length() < 8) {
							strTemp = "";
							for (int i = 0; i < 8 - bcsgn.length(); i++) {
								strTemp += " ";
							}
							bcsgn = bcsgn + strTemp;
						}
						// 2012-02-15 Update by JJ, CSGN field contains CHAR(8)
						// in DB, end

						for (int i = 0; i < parkingStandCount; i++)
							rs.previous();
					}

					// To here
					if (parkingStandCount == 0) {
						strTemp2 = "BFLNO";
						bflno = rs.getString("BFLNO");
						if (bflno == null || bflno.equals(""))
							bflno = "        ";
						if (bflno.length() < 8) {
							strTemp = "";
							for (int i = 0; i < 8 - bflno.length(); i++)
								strTemp = strTemp + " ";
							bflno = bflno + strTemp;
						}

						strTemp2 = "BALC3";
						balc3 = rs.getString("BALC3");
						if (balc3 == null || balc3.equals(""))
							balc3 = "   ";
						if (balc3.length() < 3) {
							strTemp = "";
							for (int i = 0; i < 3 - balc3.length(); i++)
								strTemp = strTemp + " ";
							balc3 = balc3 + strTemp;
						}

						// REGN field contains CHAR(12) in DB
						strTemp2 = "BREGN";
						bregn = rs.getString("BREGN");
						if (bregn == null || bregn.equals(""))
							bregn = "          ";
						if (bregn.length() < 10) {
							strTemp = "";
							for (int i = 0; i < 10 - bregn.length(); i++)
								strTemp = strTemp + " ";
							bregn = bregn + strTemp;
						}
						if (bregn.length() > 10)
							bregn = bregn.substring(0, 10);

						// 2012-02-15 Update by JJ, CSGN field contains CHAR(8)
						// in DB, start
						strTemp2 = "BCSGN";
						bcsgn = rs.getString("BCSGN");
						if (bcsgn == null || bcsgn.equals("")) {
							bcsgn = "        ";
						}
						if (bcsgn.length() < 8) {
							strTemp = "";
							for (int i = 0; i < 8 - bcsgn.length(); i++) {
								strTemp += " ";
							}
							bcsgn = bcsgn + strTemp;
						}
						// 2012-02-15 Update by JJ, CSGN field contains CHAR(8)
						// in DB, end
					}
					if (arrIndicator.equals("A")) {
						strTemp2 = "BOFBL";
						bofbl = rs.getString("BOFBL");
						if (bofbl != null && !bofbl.equals(""))
							try {
								bofbl = tanDt.getLocalDate(bofbl, 8);
							} catch (Exception e) {
								log.error("Error .. Converting AIRB time to GMTDate. AIRB time in DB: "
										+ bofbl
										+ " for Flight: "
										+ bflno
										+ " Error in function billingDetailReports(), Error :"
										+ e.toString());
								bofbl = "              ";
							}
						else
							bofbl = "              ";
					} else {
						bofbl = rs.getString("BONBL");
						if (bofbl != null && !bofbl.equals(""))
							try {
								bofbl = tanDt.getLocalDate(bofbl, 8);
							} catch (Exception e) {
								log.error("Error .. Converting AIRB time to GMTDate. AIRB time in DB: "
										+ bofbl
										+ " for Flight: "
										+ bflno
										+ " Error in function billingDetailReports(), Error :"
										+ e.toString());
								bofbl = "              ";
							}
						else
							bofbl = "              ";

					}

					/*
					 * Added by Alphy on 20-07-2007 to get AIRB for departure
					 * Flight instead of OFBL
					 */
					adid = rs.getString("ADID");
					if (adid != null && !adid.equals("")) {
						if (adid.equals("D"))
							bofbl = bairb;

					}
					/* Added for PLB Feature */
					plbUsed = rs.getString("PLB");
					if (plbUsed != null && !plbUsed.equals("")) {
						plbUsed = plbUsed;

					} else

					{
						plbUsed = "N";
					}

					// 2012-02-16 update by JJ, remark "REMOTE" in FIPS -> PLB =
					// "N"
					bftyp = rs.getString("BFTYP");
					brem1 = rs.getString("BREM1");
					if (bftyp != null && bftyp.equals("T") && brem1 != null
							&& (brem1.indexOf("REMOTE") > -1)) {
						plbUsed = "N";
					}

					/* End 20-07-2007 */
					strTemp2 = "Before creating strFileLine";
					strFileLine = strFileLine + aflno + aalc3 + aregn + acsgn
							+ aland.substring(0, 12) + bflno + balc3 + bregn
							+ bcsgn + bairb.substring(0, 12) + aact5
							+ bofbl.substring(0, 12) + parkingStandCode
							+ plbUsed;

					// if(regn1 == null || regn1.equals("")){
					// bw1.write(strFileLine); bw1.newLine();
					// expCount++;
					// }
					if (excepList.contains(arkey)) {
						bw1.write(strFileLine);
						bw1.newLine();
						expCount++;
					} else {
						bw.write(strFileLine);
						bw.newLine();
						txtCount++;
					}

				}
				// System.out.println("Header Records Count : " + txtCount +
				// "  Header Exception Count: " + expCount);

				bw.flush();
				bw.close();
				bw1.flush();
				bw1.close();
			}
		} catch (Exception e) {
			log.error("While reading the info, its wrong() Error in function billingDetailReports(), Error : "
					+ e.toString()
					+ ", in location : "
					+ strTemp2
					+ ", FLNO : " + strTemp3);
		}

		if (encryptFile(depDetFileTemp, depDetFile) != 0)
			log.error("Error while encrypting the file in billingDetailReports() function");

		if (encryptFile(depExpFileTemp, depExpFile) != 0)
			log.error("Error while encrypting the file in billingDetailReports() function");
	}

	/**
	 * This function prepares Billing Statistical Report - S for statistical.
	 * <p>
	 * <p>
	 * 
	 * @param strDate
	 *            <code>String</code> is the date for which reports to be
	 *            generated.
	 * @param rs
	 *            <code>ResultSet</code> is the ResultSet which contains the
	 *            records to be processed.
	 *            <p>
	 *            <p>
	 */
	public static void billingStatisticalReport(ResultSet rs, String strDate) {
		String strFileLine = "";
		String strTemp = "";
		String afltType = "";

		String strDateFrom = "";
		String strDateTo = "";

		String depDetFile = "";
		String depDetFileTemp = "";
		String arkey = "";

		long ordCount = 0;
		long trgCount = 0;
		long trgLanding = 0;
		String ordVal = "";
		String trgVal = "";
		String trgLandingVal = "";
		String cash = "0   ";
		String exempted = "0   ";

		BufferedWriter bw = null;

		if (strDate.equals("")) {
			depDetFile = bilpath + "S" + year + month + billCycle + day + "_new";
		} else {
			try {
				strDate = tanDt.getPreviousDate(strDate + "000000");
				billCycle = tanDt.getBillingCycle1(strDate);
			} catch (Exception e) {
				log.error("Error .. Converting user inserted date to GMT Date. Error in function billingStatisticalReport(), Error :"
						+ e.toString());
			}
			depDetFile = bilpath + "S" + strDate.substring(2, 6) + billCycle
					+ strDate.substring(6, 8) + "_new";
		}

		depDetFileTemp = bilpath + "STEMP";

		try {
			bw = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(depDetFileTemp), "UTF-8"));
		} catch (Exception e) {
			log.error("Error .. While creating FileOutputStream. Error in function billingStatisticalReport(), Error :"
					+ e.toString());
		}

		try {
			if (rs != null) {
				while (rs.next()) {
					strFileLine = "";

					afltType = rs.getString("ATTYP");
					if (afltType != null && !afltType.equals("")) {
						if (afltType.length() > 2)
							afltType = afltType.substring(0, 1);
					} else {
						afltType = " ";
						log.error("Error in billingStatisticalReport(): Flight type is mandatory field.");
					}

					if (afltType.equals("T"))
						trgCount++;
					else
						ordCount++;
				}

				trgLanding = trgCount;

				ordVal = Long.toString(ordCount);
				trgVal = Long.toString(trgCount);
				trgLandingVal = Long.toString(trgLanding);

				if (ordVal == null || ordVal.equals(""))
					ordVal = "    ";
				if (ordVal.length() < 4) {
					strTemp = "";
					for (int i = 0; i < 4 - ordVal.length(); i++)
						strTemp = strTemp + " ";
					ordVal = ordVal + strTemp;
				}

				if (trgVal == null || trgVal.equals(""))
					trgVal = "    ";
				if (trgVal.length() < 4) {
					strTemp = "";
					for (int i = 0; i < 4 - trgVal.length(); i++)
						strTemp = strTemp + " ";
					trgVal = trgVal + strTemp;
				}

				if (trgLandingVal == null || trgLandingVal.equals(""))
					trgLandingVal = "    ";
				if (trgLandingVal.length() < 4) {
					strTemp = "";
					for (int i = 0; i < 4 - trgLandingVal.length(); i++)
						strTemp = strTemp + " ";
					trgLandingVal = trgLandingVal + strTemp;
				}

				strFileLine = strFileLine + ordVal + trgVal + trgLandingVal
						+ cash + exempted;

				bw.write(strFileLine);
				bw.newLine();
				bw.flush();
				bw.close();

			}
		} catch (Exception e) {
			log.error("While reading the info, its wrong() Error in function billingStatisticalReport(), Error : "
					+ e.toString());
		}

		if (encryptFile(depDetFileTemp, depDetFile) != 0)
			log.error("Error while encrypting the file in billingStatisticalReport() function");

	}

	/**
	 * This function prepares Longstay Reports.
	 * <p>
	 * <p>
	 * 
	 * @param strDate
	 *            <code>String</code> is the date for which reports to be
	 *            generated.
	 *            <p>
	 *            <p>
	 */
	public static void prepareLongstayReports(String strDate) {

		String strDateFromFirst = "";
		String strDateToFirst = "";
		String strDateFromSecond = "";
		String strDateToSecond = "";

		String dbStrFirst = "";
		String dbStrSecond = "";
		String strDate1 = "";

		strDate1 = "" + strDate;
		// System.out.println("LongStay Reports Module under construction .. Try after sometime");
		// System.exit(0);

		if (strDate.equals("")) {
			strDateFromFirst = year4 + month + "01000000";
			strDateToFirst = year4 + curMonth + "01000000";
			strDateFromSecond = year4 + month + "01000000";
			strDateToSecond = year4 + curMonth + "01000000";

			try {
				strDateFromFirst = tanDt.getGMTDate(strDateFromFirst, -8);
				strDateToFirst = tanDt.getGMTDate(strDateToFirst, -8);

				strDateFromSecond = tanDt.getDateBefore(strDateFromSecond, -90);
				strDateToSecond = tanDt.getDateBefore(strDateToSecond, -10);

				strDateFromSecond = tanDt.getGMTDate(strDateFromSecond, -8);
				strDateToSecond = tanDt.getGMTDate(strDateToSecond, -8);

			} catch (Exception e) {
				log.error("Error .. Converting user inserted date to GMT Date. Error in function prepareLongstayReports(), Error :"
						+ e.toString());
			}
		} else {
			try {
				strDateToFirst = strDate1.substring(0, 6) + "01000000";
				strDateToSecond = strDate1.substring(0, 6) + "01000000";

				strDate1 = tanDt.getPreviousDate(strDate1 + "000000");
				strDateFromFirst = strDate1.substring(0, 6) + "01000000";
				strDateFromSecond = strDate1.substring(0, 6) + "01000000";

				strDateFromFirst = tanDt.getGMTDate(strDateFromFirst, -8);
				strDateToFirst = tanDt.getGMTDate(strDateToFirst, -8);

				strDateFromSecond = tanDt.getDateBefore(strDateFromSecond, -90);
				strDateToSecond = tanDt.getDateBefore(strDateToSecond, -10);
				//strDateFromSecond = tanDt.getDateBefore(strDateFromSecond, -30);
				//strDateToSecond = tanDt.getDateBefore(strDateToSecond, -10);

				strDateFromSecond = tanDt.getGMTDate(strDateFromSecond, -8);
				strDateToSecond = tanDt.getGMTDate(strDateToSecond, -8);

			} catch (Exception e) {
				log.error("Error .. Converting user inserted date to GMT Date. Error in function prepareLongstayReports(), Error :"
						+ e.toString());
			}
		}

		// 2012-02-16 update by JJ, add csgn ftyp rem1 paid field, and NOT
		// include the departed flight, Start
		// dbStrFirst =
		// "Select (select FCSF from FCSTAB where UFLN=a.FLNO and ROWNUM=1) as AFLNO, a.alc3 AALC3, trim(a.regn) AREGN, trim(a.csgn) ACSGN, trim(a.land) ALAND, trim(a.ttyp) ATTYP, a.act5 AACT5, a.rkey ARKEY, trim(b.pstd) APSTA, (select FCSF from FCSTAB where UFLN=b.FLNO and ROWNUM=1) as BFLNO, b.alc3 BALC3, b.regn BREGN, b.csgn BCSGN, trim(b.ftyp) BFTYP, trim(b.rem1) BREM1, trim(b.paid) BPAID, getdeptime(a.rkey) BAIRB, b.ttyp BTTYP, trim(b.ofbl) BOFBL,GETPLBSTATUS(b.pstd) PLB,trim(a.vipa) VIPA,trim(b.vipd)VIPD,trim(b.adid) ADID,trim(a.psta) PSTA,trim(b.onbl) BONBL from afttab a, afttab b where a.URNO = B.RKEY and B.RKEY <> B.URNO and a.rkey in (select c.rkey from afttab c, afttab d where d.AIRB BETWEEN '"
		// + strDateFromFirst
		// + "' AND '"
		// + strDateToFirst
		// +
		// "' and c.URNO = d.RKEY and d.RKEY <> d.URNO) ORDER BY a.FLNO,a.RKEY,b.TIFA ";

		dbStrSecond = "Select (select FCSF from FCSTAB where UFLN=a.FLNO and ROWNUM=1) as AFLNO, a.alc3 AALC3, trim(a.regn) AREGN, trim(a.csgn) ACSGN, trim(a.land) ALAND, trim(a.ttyp) ATTYP, a.act5 AACT5, a.rkey ARKEY, trim(b.pstd) APSTA, (select FCSF from FCSTAB where UFLN=b.FLNO and ROWNUM=1) as BFLNO, b.alc3 BALC3, b.regn BREGN, b.csgn BCSGN, trim(b.ftyp) BFTYP, trim(b.rem1) BREM1, trim(b.paid) BPAID, trim(b.airb) BAIRB, b.ttyp BTTYP, trim(b.ofbl) BOFBL,GETPLBSTATUS(b.pstd) PLB,trim(a.vipa) VIPA,trim(b.vipd)VIPD,trim(b.adid) ADID,trim(a.psta) PSTA,trim(b.onbl) BONBL, trim(b.rcno) BRCNO from afttab a, afttab b where a.URNO = B.RKEY and B.RKEY <> B.URNO and getDepTime(b.rkey) is null and a.rkey in (select c.rkey from afttab c, afttab d where c.LAND BETWEEN '"
				+ strDateFromSecond
				+ "' AND '"
				+ strDateToSecond
				+ "' and c.URNO = d.RKEY and d.RKEY <> d.URNO and getDepTime(d.rkey) is null) ORDER BY a.FLNO,a.RKEY,b.TIFA";
		// 2012-02-16 update by JJ, end

		// dbStrFirst =
		// "Select (select FCSF from FCSTAB where UFLN=a.FLNO and ROWNUM=1)  as AFLNO, a.alc3 AALC3, trim(a.regn) AREGN, trim(a.land) ALAND, trim(a.ttyp) ATTYP, a.act5 AACT5, a.rkey ARKEY, trim(a.psta) APSTA, (select FCSF from FCSTAB where UFLN=b.FLNO and ROWNUM=1) as BFLNO, b.alc3 BALC3, b.regn BREGN, trim(b.airb) BAIRB, b.ttyp BTTYP, trim(b.ofbl) BOFBL from afttab a, afttab b where a.URNO = B.RKEY and B.RKEY <> B.URNO and b.FTYP <> 'T' AND b.FTYP <> 'G' and a.rkey in (select c.rkey from afttab c, afttab d where d.AIRB BETWEEN '"
		// + strDateFromFirst + "' AND '" + strDateToFirst +
		// "' and c.URNO = d.RKEY and d.RKEY <> d.URNO AND d.FTYP <> 'T' AND d.FTYP <> 'G') ORDER BY a.FLNO,a.RKEY,b.AIRB";

		// dbStrSecond =
		// "Select (select FCSF from FCSTAB where UFLN=a.FLNO and ROWNUM=1) as AFLNO, a.alc3 AALC3, trim(a.regn) AREGN, trim(a.land) ALAND, trim(a.ttyp) ATTYP, a.act5 AACT5, a.rkey ARKEY, trim(a.psta) APSTA, (select FCSF from FCSTAB where UFLN=b.FLNO and ROWNUM=1) as BFLNO, b.alc3 BALC3, b.regn BREGN, trim(b.airb) BAIRB, b.ttyp BTTYP, trim(b.ofbl) BOFBL from afttab a, afttab b where a.URNO = B.RKEY and B.RKEY <> B.URNO and b.airb=' ' AND b.FTYP <> 'T' AND b.FTYP <> 'G' and  a.rkey in (select c.rkey from afttab c, afttab d where d.LAND BETWEEN '"
		// + strDateFromSecond + "' AND '" + strDateToSecond +
		// "' and c.URNO = d.RKEY and d.RKEY <> d.URNO and d.AIRB=' ' AND d.FTYP <> 'T' AND d.FTYP <> 'G') ORDER BY a.FLNO,a.RKEY,a.LAND";

		// System.out.println("Date from : " + strDateFromFirst + "  Date To: "
		// + strDateToFirst);
		// System.out.println("DB String : " + dbStrFirst);
		// System.out.println("Date from : " + strDateFromSecond + "  Date To: "
		// + strDateToSecond);
		// System.out.println("DB String : " + dbStrSecond);

		try {
			//ResultSet rsFirst = dbCon.getResultSet(dbStrFirst);
			ResultSet rsFirst = null;
			ResultSet rsSecond = dbCon1.getResultSet(dbStrSecond);

			// System.out.println("Number of rows : " + dbCon.getTotalRows(rs));

			// 2012-02-22 update by JJ, the already departed flight will not
			// inculde in the report.
			// if (rsFirst != null && rsSecond != null) {
			if (rsSecond != null) {

				// Billing Longstay Header Report preparation - L
				billingLongstayHeader(rsFirst, rsSecond, strDate);

				//rsFirst.beforeFirst();
				rsSecond.beforeFirst();
				// Billing Longstay Detail Report preparation - M
				billingLongstayDetail(rsFirst, rsSecond, strDate);

			} else {
				log.error("Result Set is Null: Error in function prepareLongstayReports() ");
			}

		} catch (Exception e) {
			log.error("Error in getting result set: Error in function prepareLongstayReports(), Error : "
					+ e.toString());
			System.exit(0);
		}
	}

	/**
	 * This function Billing Longstay Header Report - L.
	 * <p>
	 * <p>
	 * 
	 * @param strDate
	 *            <code>String</code> is the date for which reports to be
	 *            generated.
	 * @param rsFirst
	 *            <code>ResultSet</code> is the ResultSet which contains the
	 *            records to be processed.
	 * @param rsSecond
	 *            <code>ResultSet</code> is the ResultSet which contains the
	 *            records to be processed.
	 *            <p>
	 *            <p>
	 */
	public static void billingLongstayHeader(ResultSet rsFirst,
			ResultSet rsSecond, String strDate) {
		String strFileLine = "";
		String strTemp = "";
		String regn1 = "";
		String aregn = "";
		String bregn = "";
		String aflno = "";
		String aalc3 = "";
		String aland = "";
		String afltType = "";
		String bfltType = "";
		String arrVipStatus = "0";
		String depVipStatus = "0";
		String bflno = "";
		String balc3 = "";
		String bairb = "";
		String aact5 = "";
		String parkingStandVal = "";
		String noLanding = "";
		String landIndicator = "";
		String frtIndicator = "";
		String billStatus = " ";
		String receiptNumber = "         ";

		// 2012-02-16 update by JJ, add csgn field into report.
		String acsgn = "";
		String bcsgn = "";

		String billCycleDate = "";
		String strDateFrom = "";
		String strDateTo = "";
		String dbStr = "";
		String depDetFile = "";
		String depDetFileTemp = "";
		String depExpFile = "";
		String arkey = "";
		String nextRkey = "";
		String adid = "";

		long txtCount = 0;
		long expCount = 0;
		int parkingStandCount = 0;
		long hrsDiff = 0;
		BufferedWriter bw = null;
		BufferedWriter bw1 = null;

		if (strDate.equals("")) {
			// depDetFile = "H" + year + month + billCycle + day + ".TXT";
			// depExpFile = "E" + year + month + billCycle + day + ".EXP";
			depDetFile = bilpath + "L" + year + month + billCycle + day + "_new";
			billCycleDate = year4 + month + billCycle + day + "   ";
		} else {
			try {
				strDate = tanDt.getPreviousDate(strDate + "000000");
				billCycle = tanDt.getBillingCycle1(strDate);
			} catch (Exception e) {
				log.error("Error .. Converting user inserted date to GMT Date. Error in function billingLongstayHeader(), Error :"
						+ e.toString());
			}

			depDetFile = bilpath + "L" + strDate.substring(2, 6) + billCycle
					+ strDate.substring(6, 8) + "_new";
			billCycleDate = strDate.substring(0, 6) + billCycle
					+ strDate.substring(6, 8) + "   ";
		}

		depDetFileTemp = bilpath + "LTEMP";

		try {
			bw = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(depDetFileTemp), "UTF-8"));
		} catch (Exception e) {
			log.error("Error .. While creating FileOutputStream. Error in function billingLongstayHeader(), Error :"
					+ e.toString());
		}

		try {

			// To create first row in billing header report file.
			int headerRecords = 0;
			int detailRecords = 0;
			String headerRecordsVal = "";
			String detailRecordsVal = "";
			String prevRkey = "";

			/*
			 * while(rs.next()){ arkey = rs.getString("ARKEY");
			 * if(!arkey.equals(prevRkey)) headerRecords++; detailRecords++;
			 * prevRkey = arkey; }
			 */

			// 2012-02-22 update by JJ, the already departed flight will not
			// inculde in the report.
			// while (rsFirst.next()) {
			// aland = rsFirst.getString("ALAND");
			// bairb = rsFirst.getString("BAIRB");
			// hrsDiff = 0;
			//
			// // If difference between Airborn & Landing is greater than 10
			// // days, then only need to push into report file.
			// if (aland != null && !aland.equals("") && bairb != null
			// && !bairb.equals("")) {
			// try {
			// hrsDiff = tanDt.getDateDifferenceInHours(aland, bairb);
			// } catch (Exception e) {
			// log.error("Error in getting date difference at beginning in hours (LAND="
			// + aland
			// + " and AIRB="
			// + bairb
			// + "). Error in function billingLongstayHeader(), Error :"
			// + e.toString());
			// hrsDiff = 0;
			// }
			// if (hrsDiff >= 240) {
			// arkey = rsFirst.getString("ARKEY");
			// if (!arkey.equals(prevRkey))
			// headerRecords++;
			// detailRecords++;
			// prevRkey = arkey;
			// // headerRecords++;
			// // detailRecords++;
			// }
			// }
			// }

			while (rsSecond.next()) {
				arkey = rsSecond.getString("ARKEY");
				if (!arkey.equals(prevRkey))
					headerRecords++;
				detailRecords++;
				prevRkey = arkey;
				// headerRecords++;
				// detailRecords++;
			}

			headerRecordsVal = Long.toString(headerRecords);
			detailRecordsVal = Long.toString(detailRecords);
			if (headerRecordsVal == null || headerRecordsVal.equals(""))
				headerRecordsVal = "    ";
			if (headerRecordsVal.length() < 4) {
				strTemp = "";
				for (int i = 0; i < 4 - headerRecordsVal.length(); i++)
					strTemp = strTemp + " ";
				headerRecordsVal = strTemp + headerRecordsVal;
			}

			if (detailRecordsVal == null || detailRecordsVal.equals(""))
				detailRecordsVal = "        ";
			if (detailRecordsVal.length() < 8) {
				strTemp = "";
				for (int i = 0; i < 8 - detailRecordsVal.length(); i++)
					strTemp = strTemp + " ";
				detailRecordsVal = strTemp + detailRecordsVal;
			}

			// Record length --- Total 102 characters ....
			strTemp = "";
			for (int i = 0; i < 21; i++)
				strTemp = strTemp + " ";
			strFileLine = strTemp + detailRecordsVal + headerRecordsVal;
			strTemp = "";
			for (int i = 0; i < 69; i++)
				strTemp = strTemp + " ";
			strFileLine = strFileLine + strTemp;

			bw.write(strFileLine);
			bw.newLine();

			// rsFirst.beforeFirst();
			rsSecond.beforeFirst();

			// System.out.println("Header count : " + headerRecords +
			// " Detail Records: " + detailRecords);

			// upto here header report first line generation ..

			// 2012-02-22 update by JJ, the already departed flight will not
			// inculde in the report.
			// rsFirst ResultSet values to push into monthly report file
//			if (rsFirst != null) {
//				while (rsFirst.next()) {
//					strFileLine = "";
//
//					aland = rsFirst.getString("ALAND");
//					if (aland != null && !aland.equals("")) {
//						/*
//						 * if (aland!= null && !aland.equals("")) aland =
//						 * tanDt.getLocalDate(aland,8); else aland =
//						 * "              ";
//						 */
//
//						bairb = rsFirst.getString("BAIRB");
//						/*
//						 * if (bairb!= null && !bairb.equals("")) bairb =
//						 * tanDt.getLocalDate(bairb,8); else bairb =
//						 * "              ";
//						 */
//
//						hrsDiff = 0;
//						try {
//							hrsDiff = tanDt.getDateDifferenceInHours(aland,
//									bairb);
//						} catch (Exception e) {
//							log.error("Error in getting date difference in hours (LAND="
//									+ aland
//									+ " and AIRB="
//									+ bairb
//									+ "). Error in function billingLongstayHeader(), Error :"
//									+ e.toString());
//							hrsDiff = 0;
//						}
//
//						// If difference between Airborn & Landing is greater
//						// than 10 days, then only need to push into report
//						// file.
//						if (hrsDiff >= 240) {
//
//							// Flight Number
//							aflno = rsFirst.getString("AFLNO");
//							if (aflno == null || aflno.equals(""))
//								aflno = "        ";
//							if (aflno.length() < 8) {
//								strTemp = "";
//								for (int i = 0; i < 8 - aflno.length(); i++)
//									strTemp = strTemp + " ";
//								aflno = aflno + strTemp;
//							}
//
//							// Airline code
//							aalc3 = rsFirst.getString("AALC3");
//							if (aalc3 == null || aalc3.equals(""))
//								aalc3 = "   ";
//							if (aalc3.length() < 3) {
//								strTemp = "";
//								for (int i = 0; i < 3 - aalc3.length(); i++)
//									strTemp = strTemp + " ";
//								aalc3 = aalc3 + strTemp;
//							}
//
//							// REGN field contains CHAR(12) in DB
//							aregn = rsFirst.getString("AREGN");
//							if (aregn == null || aregn.equals(""))
//								aregn = "          ";
//							if (aregn.length() < 10) {
//								strTemp = "";
//								for (int i = 0; i < 10 - aregn.length(); i++)
//									strTemp = strTemp + " ";
//								aregn = aregn + strTemp;
//							}
//							if (aregn.length() > 10)
//								aregn = aregn.substring(0, 10);
//
//							// 2012-02-16 update by JJ, CSGN field contains
//							// CHAR(8) in DB, start
//							acsgn = rsFirst.getString("ACSGN");
//							if (acsgn == null || acsgn.equals(""))
//								acsgn = "        ";
//							if (acsgn.length() < 8) {
//								strTemp = "";
//								for (int i = 0; i < 8 - acsgn.length(); i++)
//									strTemp += " ";
//								acsgn += strTemp;
//							}
//							// 2012-02-16 update by JJ, CSGN field contains
//							// CHAR(8) in DB, end
//
//							afltType = rsFirst.getString("ATTYP");
//							if (afltType != null && !afltType.equals("")) {
//								if (afltType.length() > 2)
//									afltType = afltType.substring(0, 1);
//							} else {
//								afltType = " ";
//								log.error("Error in billingLongstayHeader(): Flight type is mandatory field. Flight: "
//										+ aflno);
//							}
//
//							if (aland != null && !aland.equals("")) {
//								try {
//									aland = tanDt.getLocalDate(aland, 8);
//								} catch (Exception e) {
//									log.error("Error .. Converting LAND time to GMTDate. LAND time in DB: "
//											+ aland
//											+ " for Flight: "
//											+ aflno
//											+ " Error in function billingLongstayHeader(), Error :"
//											+ e.toString());
//									aland = "              ";
//								}
//							} else {
//								aland = "              ";
//							}
//
//							if (bairb != null && !bairb.equals("")) {
//								try {
//									bairb = tanDt.getLocalDate(bairb, 8);
//								} catch (Exception e) {
//									log.error("Error .. Converting AIRB time to GMTDate. AIRB time in DB: "
//											+ bairb
//											+ " for Flight: "
//											+ bflno
//											+ " Error in function billingLongstayHeader(), Error :"
//											+ e.toString());
//									bairb = "              ";
//								}
//							} else {
//								bairb = "              ";
//							}
//
//							if (afltType.equals("T"))
//								noLanding = "0";
//							else
//								noLanding = "1";
//
//							if (afltType.equals("T"))
//								landIndicator = "1";
//							else
//								landIndicator = "0";
//
//							if (afltType.equals("F"))
//								frtIndicator = "Y";
//							else
//								frtIndicator = "N";
//
//							aact5 = rsFirst.getString("AACT5");
//							if (aact5 == null || aact5.equals(""))
//								aact5 = "     ";
//							if (aact5.length() < 5) {
//								strTemp = "";
//								for (int i = 0; i < 5 - aact5.length(); i++)
//									strTemp = strTemp + " ";
//								aact5 = aact5 + strTemp;
//							}
//							arrVipStatus = rsFirst.getString("VIPA");
//							if (arrVipStatus != null
//									&& !arrVipStatus.equals("")) {
//								if (arrVipStatus.equals("X"))
//									arrVipStatus = "1";
//								else
//									arrVipStatus = "0";
//
//							} else {
//								arrVipStatus = "0";
//							}
//							parkingStandCount = 1;
//							parkingStandVal = "";
//							arkey = rsFirst.getString("ARKEY");
//							nextRkey = "";
//
//							int recCnt = 0;
//
//							do {
//								if (!rsFirst.next())
//									break;
//
//								nextRkey = rsFirst.getString("ARKEY");
//								// System.out.println("arkey :" + arkey +
//								// " nextRkey :" + nextRkey);
//								if (arkey.equals(nextRkey)) {
//
//									parkingStandCount++;
//									// if(!rs.next()) {
//									// break;
//									// }
//								} else
//									break;
//
//							} while (true);
//							rsFirst.previous();
//
//							parkingStandVal = (parkingStandCount < 10) ? (" " + parkingStandCount)
//									: ("" + parkingStandCount);
//
//							bflno = rsFirst.getString("BFLNO");
//							if (bflno == null || bflno.equals(""))
//								bflno = "        ";
//							if (bflno.length() < 8) {
//								strTemp = "";
//								for (int i = 0; i < 8 - bflno.length(); i++)
//									strTemp = strTemp + " ";
//								bflno = bflno + strTemp;
//							}
//
//							balc3 = rsFirst.getString("BALC3");
//							if (balc3 == null || balc3.equals(""))
//								balc3 = "   ";
//							if (balc3.length() < 3) {
//								strTemp = "";
//								for (int i = 0; i < 3 - balc3.length(); i++)
//									strTemp = strTemp + " ";
//								balc3 = balc3 + strTemp;
//							}
//
//							// REGN field contains CHAR(12) in DB
//							bregn = rsFirst.getString("BREGN");
//							if (bregn == null || bregn.equals(""))
//								bregn = "          ";
//							if (bregn.length() < 10) {
//								strTemp = "";
//								for (int i = 0; i < 10 - bregn.length(); i++)
//									strTemp = strTemp + " ";
//								bregn = bregn + strTemp;
//							}
//							if (bregn.length() > 10)
//								bregn = bregn.substring(0, 10);
//
//							// 2012-02-16 update by JJ, CSGN field contains
//							// CHAR(8) in DB, start
//							bcsgn = rsFirst.getString("BCSGN");
//							if (bcsgn == null || bcsgn.equals(""))
//								bcsgn = "        ";
//							if (bcsgn.length() < 8) {
//								strTemp = "";
//								for (int i = 0; i < 8 - bcsgn.length(); i++)
//									strTemp += " ";
//								bcsgn += strTemp;
//							}
//							// 2012-02-16 update by JJ, CSGN field contains
//							// CHAR(8) in DB, end
//
//							// 2012-02-20 update by JJ, cash indicator
//							billStatus = rsFirst.getString("BPAID");
//							if (billStatus == null || billStatus.equals("")) {
//								billStatus = " ";
//							}
//
//							bfltType = rsFirst.getString("BTTYP");
//							if (bfltType != null && !bfltType.equals("")) {
//								if (bfltType.length() > 2)
//									bfltType = bfltType.substring(0, 1);
//							} else {
//								bfltType = " ";
//								log.error("Error in billingLongstayHeader(): Flight type is mandatory field. Flight: "
//										+ bflno);
//							}
//							depVipStatus = rsFirst.getString("VIPD");
//							if (depVipStatus != null
//									&& !depVipStatus.equals("")) {
//								if (depVipStatus.equals("X"))
//									depVipStatus = "1";
//								else
//									depVipStatus = "0";
//
//							} else {
//								depVipStatus = "0";
//							}
//
//							strFileLine = strFileLine + aflno + ";" + aalc3
//									+ ";" + aregn + ";" + acsgn + ";"
//									+ aland.substring(0, 12) + ";" + afltType
//									+ ";" + arrVipStatus + ";" + bflno + ";"
//									+ balc3 + ";" + bregn + ";" + bcsgn + ";"
//									+ bairb.substring(0, 12) + ";" + bfltType
//									+ ";" + depVipStatus + ";" + aact5 + ";"
//									+ billCycleDate + ";" + parkingStandVal
//									+ ";" + noLanding + ";" + landIndicator
//									+ ";" + frtIndicator + ";" + billStatus
//									+ ";" + receiptNumber;
//
//							// System.out.println("strFileLine : " +
//							// strFileLine);
//
//							bw.write(strFileLine);
//							bw.newLine();
//						} // End of if loop
//					}
//				} // End of While loop
//			} // End of if rsFirst != null

			// Upto here ... rsSecond Result Set

			// rsSecond ResultSet values to push into monthly report file
			if (rsSecond != null) {
				while (rsSecond.next()) {
					strFileLine = "";

					// Flight Number
					aflno = rsSecond.getString("AFLNO");
					if (aflno == null || aflno.equals(""))
						aflno = "        ";
					if (aflno.length() < 8) {
						strTemp = "";
						for (int i = 0; i < 8 - aflno.length(); i++)
							strTemp = strTemp + " ";
						aflno = aflno + strTemp;
					}

					// Airline code
					aalc3 = rsSecond.getString("AALC3");
					if (aalc3 == null || aalc3.equals(""))
						aalc3 = "   ";
					if (aalc3.length() < 3) {
						strTemp = "";
						for (int i = 0; i < 3 - aalc3.length(); i++)
							strTemp = strTemp + " ";
						aalc3 = aalc3 + strTemp;
					}

					// REGN field contains CHAR(12) in DB
					regn1 = rsSecond.getString("AREGN");
					aregn = rsSecond.getString("AREGN");
					if (aregn == null || aregn.equals(""))
						aregn = "          ";
					if (aregn.length() < 10) {
						strTemp = "";
						for (int i = 0; i < 10 - aregn.length(); i++)
							strTemp = strTemp + " ";
						aregn = aregn + strTemp;
					}
					if (aregn.length() > 10)
						aregn = aregn.substring(0, 10);

					// 2012-02-16 update by JJ, CSGN field contains CHAR(8) in
					// DB, start
					acsgn = rsSecond.getString("ACSGN");
					if (acsgn == null || acsgn.equals(""))
						acsgn = "        ";
					if (acsgn.length() < 8) {
						strTemp = "";
						for (int i = 0; i < 8 - acsgn.length(); i++)
							strTemp += " ";
						acsgn += strTemp;
					}
					// 2012-02-16 update by JJ, CSGN field contains CHAR(8) in
					// DB, end
					
					// 2012-02-16 update by JJ, CSGN field contains CHAR(8)
					// in DB, start
					acsgn = rsSecond.getString("ACSGN");
					if (acsgn == null || acsgn.equals(""))
						acsgn = "        ";
					if (acsgn.length() < 8) {
						strTemp = "";
						for (int i = 0; i < 8 - acsgn.length(); i++)
							strTemp += " ";
						acsgn += strTemp;
					}

					aland = rsSecond.getString("ALAND");
					if (aland != null && !aland.equals(""))
						try {
							aland = tanDt.getLocalDate(aland, 8);
						} catch (Exception e) {
							log.error("Error .. Converting LAND time to GMTDate. LAND time in DB: "
									+ aland
									+ " for Flight: "
									+ aflno
									+ " Error in function billingLongstayHeader(), Error :"
									+ e.toString());
							aland = "              ";
						}
					else
						aland = "              ";

					afltType = rsSecond.getString("ATTYP");
					if (afltType != null && !afltType.equals("")) {
						if (afltType.length() > 2)
							afltType = afltType.substring(0, 1);
					} else {
						afltType = " ";
						log.error("Error in billingLongstayHeader(): Flight type is mandatory field. Flight: "
								+ aflno);
					}

					if (afltType.equals("T"))
						noLanding = "0";
					else
						noLanding = "1";

					if (afltType.equals("T"))
						landIndicator = "1";
					else
						landIndicator = "0";

					if (afltType.equals("F"))
						frtIndicator = "Y";
					else
						frtIndicator = "N";

					aact5 = rsSecond.getString("AACT5");
					if (aact5 == null || aact5.equals(""))
						aact5 = "     ";
					if (aact5.length() < 5) {
						strTemp = "";
						for (int i = 0; i < 5 - aact5.length(); i++)
							strTemp = strTemp + " ";
						aact5 = aact5 + strTemp;
					}
					arrVipStatus = rsSecond.getString("VIPA");
					if (arrVipStatus != null && !arrVipStatus.equals("")) {
						if (arrVipStatus.equals("X"))
							arrVipStatus = "1";
						else
							arrVipStatus = "0";

					} else {
						arrVipStatus = "0";
					}
					parkingStandCount = 1;
					parkingStandVal = "";

					arkey = rsSecond.getString("ARKEY");
					nextRkey = "";

					do {
						if (!rsSecond.next())
							break;

						nextRkey = rsSecond.getString("ARKEY");
						// System.out.println("arkey :" + arkey + " nextRkey :"
						// + nextRkey);
						if (arkey.equals(nextRkey)) {
							parkingStandCount++;
							// if(!rs.next()) {
							// break;
							// }
						} else
							break;

					} while (true);
					rsSecond.previous();

					parkingStandVal = (parkingStandCount < 10) ? (" " + parkingStandCount)
							: ("" + parkingStandCount);
					adid = rsSecond.getString("ADID");
					if (adid.equals("D")) {
						bflno = rsSecond.getString("BFLNO");
						if (bflno == null || bflno.equals(""))
							bflno = "        ";
						if (bflno.length() < 8) {
							strTemp = "";
							for (int i = 0; i < 8 - bflno.length(); i++)
								strTemp = strTemp + " ";
							bflno = bflno + strTemp;
						}

						balc3 = rsSecond.getString("BALC3");
						if (balc3 == null || balc3.equals(""))
							balc3 = "   ";
						if (balc3.length() < 3) {
							strTemp = "";
							for (int i = 0; i < 3 - balc3.length(); i++)
								strTemp = strTemp + " ";
							balc3 = balc3 + strTemp;
						}

						// REGN field contains CHAR(12) in DB
						bregn = rsSecond.getString("BREGN");
						if (bregn == null || bregn.equals(""))
							bregn = "          ";
						if (bregn.length() < 10) {
							strTemp = "";
							for (int i = 0; i < 10 - bregn.length(); i++)
								strTemp = strTemp + " ";
							bregn = bregn + strTemp;
						}
						if (bregn.length() > 10)
							bregn = bregn.substring(0, 10);

						// 2012-02-16 update by JJ, CSGN field contains CHAR(8)
						// in DB, start
						bcsgn = rsSecond.getString("BCSGN");
						if (bcsgn == null || bcsgn.equals(""))
							bcsgn = "        ";
						if (bcsgn.length() < 8) {
							strTemp = "";
							for (int i = 0; i < 8 - bcsgn.length(); i++)
								strTemp += " ";
							bcsgn += strTemp;
						}
						// 2012-02-16 update by JJ, CSGN field contains CHAR(8)
						// in DB, end

						// 2012-02-20 update by JJ, cash indicator
						billStatus = rsSecond.getString("BPAID");
						if (billStatus == null || billStatus.equals("")) {
							billStatus = " ";
						}
						
						// 2012-03-19 update by JJ, add receipt numer
						receiptNumber = rsSecond.getString("BRCNO");
						if (receiptNumber == null || receiptNumber.equals("")) {
							receiptNumber = "         ";
						}
						if (receiptNumber.length() < 9) {
							strTemp = "";
							for (int i = 0; i < 9 - receiptNumber.length(); i++) {
								strTemp += " ";
							}
							receiptNumber += strTemp;
						}
						if (receiptNumber.length() > 9) {
							receiptNumber = receiptNumber.substring(0, 9);
						}

						bairb = rsSecond.getString("BAIRB");
						if (bairb != null && !bairb.equals(""))
							try {
								bairb = tanDt.getLocalDate(bairb, 8);
							} catch (Exception e) {
								log.error("Error .. Converting AIRB time to GMTDate. AIRB time in DB: "
										+ bairb
										+ " for Flight: "
										+ bflno
										+ " Error in function billingLongstayHeader(), Error :"
										+ e.toString());
								bairb = "              ";
							}
						else
							bairb = "              ";

						bfltType = rsSecond.getString("BTTYP");
						if (bfltType != null && !bfltType.equals("")) {
							if (bfltType.length() > 2)
								bfltType = bfltType.substring(0, 1);
						} else {
							bfltType = " ";
							log.error("Error in billingLongstayHeader(): Flight type is mandatory field. Flight: "
									+ bflno);
						}
						depVipStatus = rsSecond.getString("VIPD");
						if (depVipStatus != null && !depVipStatus.equals("")) {
							if (depVipStatus.equals("X"))
								depVipStatus = "1";
							else
								depVipStatus = "0";

						} else {
							depVipStatus = "0";
						}
					} else {
						bflno = "        ";
						balc3 = "   ";
						bregn = "          ";
						bairb = "              ";
						bfltType = " ";
					}

					strFileLine = strFileLine + aflno + ";" + aalc3 + ";"
							+ aregn + ";" + acsgn + ";"
							+ aland.substring(0, 12) + ";" + afltType + ";"
							+ arrVipStatus + ";" + bflno + ";" + balc3 + ";"
							+ bregn + ";" + bcsgn + ";"
							+ bairb.substring(0, 12) + ";" + bfltType + ";"
							+ depVipStatus + ";" + aact5 + ";" + billCycleDate
							+ ";" + parkingStandVal + ";" + noLanding + ";"
							+ landIndicator + ";" + frtIndicator + ";"
							+ billStatus + ";" + receiptNumber;

					// System.out.println("strFileLine : " + strFileLine);

					bw.write(strFileLine);
					bw.newLine();
					txtCount++;

				}
			}

			// Upto here ... rsSecond Result Set

			bw.flush();
			bw.close();

		} catch (Exception e) {
			log.error("While reading the info, its wrong() Error in function billingLongstayHeader(), Error : "
					+ e.toString());
		}

		if (encryptFile(depDetFileTemp, depDetFile) != 0)
			log.error("Error while encrypting the file in billingLongstayHeader() function");

	}

	/**
	 * This function Billing Longstay Detail Report - M.
	 * <p>
	 * <p>
	 * 
	 * @param strDate
	 *            <code>String</code> is the date for which reports to be
	 *            generated.
	 * @param rsFirst
	 *            <code>ResultSet</code> is the ResultSet which contains the
	 *            records to be processed.
	 * @param rsSecond
	 *            <code>ResultSet</code> is the ResultSet which contains the
	 *            records to be processed.
	 *            <p>
	 *            <p>
	 */
	public static void billingLongstayDetail(ResultSet rsFirst,
			ResultSet rsSecond, String strDate) {
		String strFileLine = "";
		String strTemp = "";
		String regn1 = "";
		String aregn = "";
		String bregn = "";
		String aflno = "";
		String aalc3 = "";
		String aland = "";
		String bflno = "";
		String balc3 = "";
		String bairb = "";
		String bofbl = "";
		String aact5 = "";
		String parkingStandCode = "";
		String plbUsed = "";

		// 2012-02-16 update by JJ, add csgn field into report.
		String acsgn = "";
		String bcsgn = "";
		String bftyp = "";
		String brem1 = "";
		;

		String strDateFrom = "";
		String strDateTo = "";
		String dbStr = "";
		String depDetFile = "";
		String depDetFileTemp = "";
		String arkey = "";
		String prevRkey = "";
		String arrIndicator = "";
		String adid = "";
		long txtCount = 0;
		long expCount = 0;
		long hrsDiff = 0;
		int parkingStandCount = 0;
		BufferedWriter bw = null;
		BufferedWriter bw1 = null;

		if (strDate.equals("")) {
			depDetFile = bilpath + "M" + year + month + billCycle + day + "_new";
		} else {
			try {
				strDate = tanDt.getPreviousDate(strDate + "000000");
				billCycle = tanDt.getBillingCycle1(strDate);
			} catch (Exception e) {
				log.error("Error .. Converting user inserted date to GMT Date. Error in function billingLongstayDetail(), Error :"
						+ e.toString());
			}
			depDetFile = bilpath + "M" + strDate.substring(2, 6) + billCycle
					+ strDate.substring(6, 8) + "_new";
		}

		depDetFileTemp = bilpath + "MTEMP";

		try {
			bw = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(depDetFileTemp), "UTF-8"));
		} catch (Exception e) {
			log.error("Error .. While creating FileOutputStream. Error in function billingLongstayDetail(), Error :"
					+ e.toString());
		}

		try {
			// 2012-02-22 update by JJ, the already departed flight will not
			// inculde in the report.
//			if (rsFirst != null) {
//				while (rsFirst.next()) {
//					// System.out.println("Inside LongStay Detail");
//					strFileLine = "";
//					parkingStandCount = 0;
//					aland = rsFirst.getString("ALAND");
//					if (aland != null && !aland.equals(""))
//						try {
//							aland = tanDt.getLocalDate(aland, 8);
//						} catch (Exception e) {
//							log.error("Error .. Converting LAND time to GMTDate. LAND time in DB: "
//									+ aland
//									+ " for Flight: "
//									+ aflno
//									+ " Error in function billingLongstayDetail(), Error :"
//									+ e.toString());
//							aland = "              ";
//						}
//					else
//						aland = "              ";
//
//					bairb = rsFirst.getString("BAIRB");
//					if (bairb != null && !bairb.equals(""))
//						try {
//							bairb = tanDt.getLocalDate(bairb, 8);
//						} catch (Exception e) {
//							log.error("Error .. Converting AIRB time to GMTDate. AIRB time in DB: "
//									+ bairb
//									+ " for Flight: "
//									+ bflno
//									+ " Error in function billingLongstayDetail(), Error :"
//									+ e.toString());
//							bairb = "            ";
//						}
//					else
//						bairb = "            ";
//
//					hrsDiff = 0;
//					try {
//						hrsDiff = tanDt.getDateDifferenceInHours(aland, bairb);
//					} catch (Exception e) {
//						log.error("Error in getting date difference in hours (LAND="
//								+ aland
//								+ " and AIRB="
//								+ bairb
//								+ "). Error in function billingLongstayDetail(), Error :"
//								+ e.toString());
//						hrsDiff = 0;
//					}
//
//					if (hrsDiff >= 240) {
//
//						// Flight Number
//						aflno = rsFirst.getString("AFLNO");
//						if (aflno == null || aflno.equals(""))
//							aflno = "        ";
//						if (aflno.length() < 8) {
//							strTemp = "";
//							for (int i = 0; i < 8 - aflno.length(); i++)
//								strTemp = strTemp + " ";
//							aflno = aflno + strTemp;
//						}
//						// System.out.println(aflno);
//						// Airline code
//						aalc3 = rsFirst.getString("AALC3");
//						if (aalc3 == null || aalc3.equals(""))
//							aalc3 = "   ";
//						if (aalc3.length() < 3) {
//							strTemp = "";
//							for (int i = 0; i < 3 - aalc3.length(); i++)
//								strTemp = strTemp + " ";
//							aalc3 = aalc3 + strTemp;
//						}
//
//						// REGN field contains CHAR(12) in DB
//						regn1 = rsFirst.getString("AREGN");
//						aregn = rsFirst.getString("AREGN");
//						if (aregn == null || aregn.equals(""))
//							aregn = "          ";
//						if (aregn.length() < 10) {
//							strTemp = "";
//							for (int i = 0; i < 10 - aregn.length(); i++)
//								strTemp = strTemp + " ";
//							aregn = aregn + strTemp;
//						}
//						if (aregn.length() > 10)
//							aregn = aregn.substring(0, 10);
//
//						// 2012-02-16 update by JJ, CSGN field contains CHAR(8)
//						// in DB, start
//						acsgn = rsFirst.getString("ACSGN");
//						if (acsgn == null || acsgn.equals(""))
//							acsgn = "        ";
//						if (acsgn.length() < 8) {
//							strTemp = "";
//							for (int i = 0; i < 8 - acsgn.length(); i++)
//								strTemp += " ";
//							acsgn += strTemp;
//						}
//						// 2012-02-16 update by JJ, CSGN field contains CHAR(8)
//						// in DB, end
//
//						aact5 = rsFirst.getString("AACT5");
//						if (aact5 == null || aact5.equals(""))
//							aact5 = "     ";
//						if (aact5.length() < 5) {
//							strTemp = "";
//							for (int i = 0; i < 5 - aact5.length(); i++)
//								strTemp = strTemp + " ";
//							aact5 = aact5 + strTemp;
//						}
//
//						// Parking Stand Code
//						parkingStandCode = rsFirst.getString("APSTA");
//						if (parkingStandCode == null
//								|| parkingStandCode.equals(""))
//							parkingStandCode = "   ";
//						if (parkingStandCode.length() < 4) {
//							strTemp = "";
//							for (int i = 0; i < 4 - parkingStandCode.length(); i++)
//								strTemp = strTemp + " ";
//							parkingStandCode = parkingStandCode + strTemp;
//						}
//						arkey = rsFirst.getString("ARKEY");
//						if (!rsFirst.isFirst()) {
//							rsFirst.previous();
//							String prevArkey = rsFirst.getString("ARKEY");
//							if (!(arkey.equals(prevArkey))) {
//
//								arrIndicator = "A";
//
//							} else {
//
//								arrIndicator = "B";
//							}
//
//							rsFirst.next();
//						}
//						if (parkingStandCode == null
//								|| parkingStandCode.equals(""))
//							parkingStandCode = "   ";
//						if (parkingStandCode.length() < 4) {
//							strTemp = "";
//							for (int i = 0; i < 4 - parkingStandCode.length(); i++)
//								strTemp = strTemp + " ";
//							parkingStandCode = parkingStandCode + strTemp;
//						}
//
//						/*
//						 * bairb = rs.getString("BAIRB"); if (bairb!= null &&
//						 * !bairb.equals("")) bairb =
//						 * tanDt.getLocalDate(bairb,8); else{ bairb =
//						 * "            ";
//						 */
//
//						// From here
//						// Below script is to generate bairb, if bairb is empty
//						// ... bit complex script.
//						// strTemp2 = "BAIRB";
//						// bairb = rsFirst.getString("BAIRB");
//						// if (bairb!= null && !bairb.equals(""))
//						// try{
//						// bairb = tanDt.getLocalDate(bairb,8);
//						// }catch(Exception e){
//						// log.error("Error .. Converting AIRB time to GMTDate. AIRB time in DB: "
//						// + bairb + " for Flight: " + aflno +
//						// " Error in function billingDetailReports(), Error :"
//						// + e.toString());
//						// bairb = "              ";
//						// }
//						// else{
//						// bairb = "";
//						arkey = rsFirst.getString("ARKEY");
//						String nextRkey = "";
//
//						// rs.next();
//						do {
//							if (!rsFirst.next())
//								break;
//
//							nextRkey = rsFirst.getString("ARKEY");
//							// System.out.println("arkey :" + arkey +
//							// " nextRkey :" +
//							// nextRkey+"pstandcnt"+parkingStandCount);
//							if (arkey.equals(nextRkey)) {
//								parkingStandCount++;
//								/*
//								 * Commented of by Alphy since these lines are
//								 * not needed and are skipping records
//								 */
//								// if(!rs.next()) {
//								// break;
//								// }
//							} else
//								break;
//
//						} while (true);
//
//						rsFirst.previous();
//						bairb = rsFirst.getString("BAIRB");
//						if (bairb != null && !bairb.equals("")) {
//							try {
//								bairb = tanDt.getLocalDate(bairb, 8);
//							} catch (Exception e) {
//								log.error("Error .. Converting AIRB time to GMTDate. AIRB time in DB: "
//										+ bairb
//										+ " for Flight: "
//										+ aflno
//										+ " Error in function billingDetailReports(), Error :"
//										+ e.toString());
//								bairb = "              ";
//							}
//						} else
//							bairb = "              ";
//
//						// System.out.println("AIRB :" + bairb);
//						bflno = rsFirst.getString("BFLNO");
//						if (bflno == null || bflno.equals(""))
//							bflno = "        ";
//						if (bflno.length() < 8) {
//							strTemp = "";
//							for (int i = 0; i < 8 - bflno.length(); i++)
//								strTemp = strTemp + " ";
//							bflno = bflno + strTemp;
//						}
//						balc3 = rsFirst.getString("BALC3");
//						if (balc3 == null || balc3.equals(""))
//							balc3 = "   ";
//						if (balc3.length() < 3) {
//							strTemp = "";
//							for (int i = 0; i < 3 - balc3.length(); i++)
//								strTemp = strTemp + " ";
//							balc3 = balc3 + strTemp;
//						}
//
//						// REGN field contains CHAR(12) in DB
//						// strTemp2 = "BREGN";
//						bregn = rsFirst.getString("BREGN");
//						if (bregn == null || bregn.equals(""))
//							bregn = "          ";
//						if (bregn.length() < 10) {
//							strTemp = "";
//							for (int i = 0; i < 10 - bregn.length(); i++)
//								strTemp = strTemp + " ";
//							bregn = bregn + strTemp;
//						}
//						if (bregn.length() > 10)
//							bregn = bregn.substring(0, 10);
//
//						// 2012-02-15 Update by JJ, CSGN field contains CHAR(8)
//						// in DB, start
//						bcsgn = rsFirst.getString("BCSGN");
//						if (bcsgn == null || bcsgn.equals("")) {
//							bcsgn = "        ";
//						}
//						if (bcsgn.length() < 8) {
//							strTemp = "";
//							for (int i = 0; i < 8 - bcsgn.length(); i++) {
//								strTemp += " ";
//							}
//							bcsgn = bcsgn + strTemp;
//						}
//						// 2012-02-15 Update by JJ, CSGN field contains CHAR(8)
//						// in DB, end
//
//						for (int i = 0; i < parkingStandCount; i++)
//							rsFirst.previous();
//						// }
//
//						// To here
//						if (parkingStandCount == 0) {
//							// strTemp2 = "BFLNO";
//							bflno = rsFirst.getString("BFLNO");
//							if (bflno == null || bflno.equals(""))
//								bflno = "        ";
//							if (bflno.length() < 8) {
//								strTemp = "";
//								for (int i = 0; i < 8 - bflno.length(); i++)
//									strTemp = strTemp + " ";
//								bflno = bflno + strTemp;
//							}
//
//							// strTemp2 = "BALC3";
//							balc3 = rsFirst.getString("BALC3");
//							if (balc3 == null || balc3.equals(""))
//								balc3 = "   ";
//							if (balc3.length() < 3) {
//								strTemp = "";
//								for (int i = 0; i < 3 - balc3.length(); i++)
//									strTemp = strTemp + " ";
//								balc3 = balc3 + strTemp;
//							}
//
//							// REGN field contains CHAR(12) in DB
//							// strTemp2 = "BREGN";
//							bregn = rsFirst.getString("BREGN");
//							if (bregn == null || bregn.equals(""))
//								bregn = "          ";
//							if (bregn.length() < 10) {
//								strTemp = "";
//								for (int i = 0; i < 10 - bregn.length(); i++)
//									strTemp = strTemp + " ";
//								bregn = bregn + strTemp;
//							}
//							if (bregn.length() > 10)
//								bregn = bregn.substring(0, 10);
//
//							// 2012-02-15 Update by JJ, CSGN field contains
//							// CHAR(8)
//							// in DB, start
//							bcsgn = rsFirst.getString("BCSGN");
//							if (bcsgn == null || bcsgn.equals("")) {
//								bcsgn = "        ";
//							}
//							if (bcsgn.length() < 8) {
//								strTemp = "";
//								for (int i = 0; i < 8 - bcsgn.length(); i++) {
//									strTemp += " ";
//								}
//								bcsgn = bcsgn + strTemp;
//							}
//							// 2012-02-15 Update by JJ, CSGN field contains
//							// CHAR(8)
//							// in DB, end
//						}
//						if (arrIndicator.equals("A")) {
//							// strTemp2 = "BOFBL";
//							bofbl = rsFirst.getString("BOFBL");
//							if (bofbl != null && !bofbl.equals(""))
//								try {
//									bofbl = tanDt.getLocalDate(bofbl, 8);
//								} catch (Exception e) {
//									log.error("Error .. Converting AIRB time to GMTDate. AIRB time in DB: "
//											+ bofbl
//											+ " for Flight: "
//											+ bflno
//											+ " Error in function billingDetailReports(), Error :"
//											+ e.toString());
//									bofbl = "              ";
//								}
//							else
//								bofbl = "              ";
//						} else {
//							bofbl = rsFirst.getString("BONBL");
//							if (bofbl != null && !bofbl.equals(""))
//								try {
//									bofbl = tanDt.getLocalDate(bofbl, 8);
//								} catch (Exception e) {
//									log.error("Error .. Converting AIRB time to GMTDate. AIRB time in DB: "
//											+ bofbl
//											+ " for Flight: "
//											+ bflno
//											+ " Error in function billingDetailReports(), Error :"
//											+ e.toString());
//									bofbl = "              ";
//								}
//							else
//								bofbl = "              ";
//
//						}
//
//						/*
//						 * Added by Alphy on 20-07-2007 to get AIRB for
//						 * departure Flight instead of OFBL
//						 */
//						adid = rsFirst.getString("ADID");
//						if (adid != null && !adid.equals("")) {
//							if (adid.equals("D"))
//								bofbl = bairb;
//
//						}
//						/* Added for PLB Feature */
//						plbUsed = rsFirst.getString("PLB");
//						if (plbUsed != null && !plbUsed.equals("")) {
//							plbUsed = plbUsed;
//
//						} else
//
//						{
//							plbUsed = "N";
//						}
//
//						// 2012-02-16 update by JJ, remark "REMOTE" in FIPS ->
//						// PLB = "N"
//						bftyp = rsFirst.getString("BFTYP");
//						brem1 = rsFirst.getString("BREM1");
//						if (bftyp != null && bftyp.equals("T") && brem1 != null
//								&& (brem1.indexOf("REMOTE") > -1)) {
//							plbUsed = "N";
//						}
//
//						strFileLine = strFileLine + aflno + ";" + aalc3 + ";"
//								+ aregn + ";" + acsgn + ";"
//								+ aland.substring(0, 12) + ";" + bflno + ";"
//								+ balc3 + ";" + bregn + ";" + bcsgn + ";"
//								+ bairb.substring(0, 12) + ";" + aact5 + ";"
//								+ bofbl.substring(0, 12) + ";"
//								+ parkingStandCode + ";" + plbUsed;
//						// System.out.println(strFileLine);
//						bw.write(strFileLine);
//						bw.newLine();
//					}// End of if(true) loop
//				} // End of while loop
//			} // End of if(rsFirst != null) loop

			if (rsSecond != null) {
				while (rsSecond.next()) {
					// System.out.println("Inside rsSecond");
					strFileLine = "";
					parkingStandCount = 0;
					aland = rsSecond.getString("ALAND");
					if (aland != null && !aland.equals(""))
						try {
							aland = tanDt.getLocalDate(aland, 8);
						} catch (Exception e) {
							log.error("Error .. Converting LAND time to GMTDate. LAND time in DB: "
									+ aland
									+ " for Flight: "
									+ aflno
									+ " Error in function billingLongstayDetail(), Error :"
									+ e.toString());
							aland = "              ";
						}
					else
						aland = "              ";

					bairb = rsSecond.getString("BAIRB");
					if (bairb != null && !bairb.equals(""))
						try {
							bairb = tanDt.getLocalDate(bairb, 8);
						} catch (Exception e) {
							log.error("Error .. Converting AIRB time to GMTDate. AIRB time in DB: "
									+ bairb
									+ " for Flight: "
									+ bflno
									+ " Error in function billingLongstayDetail(), Error :"
									+ e.toString());
							bairb = "            ";
						}
					else
						bairb = "            ";

					// Flight NumberrsSecond
					aflno = rsSecond.getString("AFLNO");
					if (aflno == null || aflno.equals(""))
						aflno = "        ";
					if (aflno.length() < 8) {
						strTemp = "";
						for (int i = 0; i < 8 - aflno.length(); i++)
							strTemp = strTemp + " ";
						aflno = aflno + strTemp;
					}
					// System.out.println("rssecond"+aflno);
					// Airline code
					aalc3 = rsSecond.getString("AALC3");
					if (aalc3 == null || aalc3.equals(""))
						aalc3 = "   ";
					if (aalc3.length() < 3) {
						strTemp = "";
						for (int i = 0; i < 3 - aalc3.length(); i++)
							strTemp = strTemp + " ";
						aalc3 = aalc3 + strTemp;
					}

					// REGN field contains CHAR(12) in DB
					regn1 = rsSecond.getString("AREGN");
					aregn = rsSecond.getString("AREGN");
					if (aregn == null || aregn.equals(""))
						aregn = "          ";
					if (aregn.length() < 10) {
						strTemp = "";
						for (int i = 0; i < 10 - aregn.length(); i++)
							strTemp = strTemp + " ";
						aregn = aregn + strTemp;
					}
					if (aregn.length() > 10)
						aregn = aregn.substring(0, 10);

					// 2012-02-16 update by JJ, CSGN field contains CHAR(8) in
					// DB, start
					acsgn = rsSecond.getString("ACSGN");
					if (acsgn == null || acsgn.equals(""))
						acsgn = "        ";
					if (acsgn.length() < 8) {
						strTemp = "";
						for (int i = 0; i < 8 - acsgn.length(); i++)
							strTemp += " ";
						acsgn += strTemp;
					}
					// 2012-02-16 update by JJ, CSGN field contains CHAR(8) in
					// DB, end
					
					// 2012-02-16 update by JJ, CSGN field contains
					// CHAR(8) in DB, start
					acsgn = rsSecond.getString("ACSGN");
					if (acsgn == null || acsgn.equals(""))
						acsgn = "        ";
					if (acsgn.length() < 8) {
						strTemp = "";
						for (int i = 0; i < 8 - acsgn.length(); i++)
							strTemp += " ";
						acsgn += strTemp;
					}
					// 2012-02-16 update by JJ, CSGN field contains
					// CHAR(8) in DB, end

					aact5 = rsSecond.getString("AACT5");
					if (aact5 == null || aact5.equals(""))
						aact5 = "     ";
					if (aact5.length() < 5) {
						strTemp = "";
						for (int i = 0; i < 5 - aact5.length(); i++)
							strTemp = strTemp + " ";
						aact5 = aact5 + strTemp;
					}

					// Parking Stand Code
					parkingStandCode = rsSecond.getString("APSTA");
					if (parkingStandCode == null || parkingStandCode.equals(""))
						parkingStandCode = "   ";
					if (parkingStandCode.length() < 4) {
						strTemp = "";
						for (int i = 0; i < 4 - parkingStandCode.length(); i++)
							strTemp = strTemp + " ";
						parkingStandCode = parkingStandCode + strTemp;
					}
					arkey = rsSecond.getString("ARKEY");
					if (!rsSecond.isFirst()) {
						rsSecond.previous();
						String prevArkey = rsSecond.getString("ARKEY");
						if (!(arkey.equals(prevArkey))) {

							arrIndicator = "A";

						} else {

							arrIndicator = "B";
						}

						rsSecond.next();
					}
					if (parkingStandCode == null || parkingStandCode.equals(""))
						parkingStandCode = "   ";
					if (parkingStandCode.length() < 4) {
						strTemp = "";
						for (int i = 0; i < 4 - parkingStandCode.length(); i++)
							strTemp = strTemp + " ";
						parkingStandCode = parkingStandCode + strTemp;
					}

					/*
					 * bairb = rs.getString("BAIRB"); if (bairb!= null &&
					 * !bairb.equals("")) bairb = tanDt.getLocalDate(bairb,8);
					 * else{ bairb = "            ";
					 */

					// From here
					// Below script is to generate bairb, if bairb is empty ...
					// bit complex script.
					// strTemp2 = "BAIRB";
					bairb = rsSecond.getString("BAIRB");
					if (bairb != null && !bairb.equals(""))
						try {
							bairb = tanDt.getLocalDate(bairb, 8);
						} catch (Exception e) {
							log.error("Error .. Converting AIRB time to GMTDate. AIRB time in DB: "
									+ bairb
									+ " for Flight: "
									+ aflno
									+ " Error in function billingDetailReports(), Error :"
									+ e.toString());
							bairb = "              ";
						}
					else {
						bairb = "";
						arkey = rsSecond.getString("ARKEY");
						String nextRkey = "";

						// rs.next();
						do {
							if (!rsSecond.next())
								break;

							nextRkey = rsSecond.getString("ARKEY");
							// System.out.println("arkey :" + arkey +
							// " nextRkey :" +
							// nextRkey+"pstandcnt"+parkingStandCount);
							if (arkey.equals(nextRkey)) {
								parkingStandCount++;
								/*
								 * Commented of by Alphy since these lines are
								 * not needed and are skipping records
								 */
								// if(!rs.next()) {
								// break;
								// }
							} else
								break;

						} while (true);

						rsSecond.previous();
						adid = rsSecond.getString("ADID");
						if (!adid.equals("D")) {
							bairb = "              ";
							bflno = "        ";
							balc3 = "   ";
							bregn = "          ";
							for (int i = 0; i < parkingStandCount; i++)
								rsSecond.previous();
						} else {
							bairb = rsSecond.getString("BAIRB");
							if (bairb != null && !bairb.equals("")) {
								try {
									bairb = tanDt.getLocalDate(bairb, 8);
								} catch (Exception e) {
									log.error("Error .. Converting AIRB time to GMTDate. AIRB time in DB: "
											+ bairb
											+ " for Flight: "
											+ aflno
											+ " Error in function billingDetailReports(), Error :"
											+ e.toString());
									bairb = "              ";
								}
							} else
								bairb = "              ";

							// System.out.println("AIRB :" + bairb);
							bflno = rsSecond.getString("BFLNO");
							if (bflno == null || bflno.equals(""))
								bflno = "        ";
							if (bflno.length() < 8) {
								strTemp = "";
								for (int i = 0; i < 8 - bflno.length(); i++)
									strTemp = strTemp + " ";
								bflno = bflno + strTemp;
							}

							balc3 = rsSecond.getString("BALC3");
							if (balc3 == null || balc3.equals(""))
								balc3 = "   ";
							if (balc3.length() < 3) {
								strTemp = "";
								for (int i = 0; i < 3 - balc3.length(); i++)
									strTemp = strTemp + " ";
								balc3 = balc3 + strTemp;
							}

							// REGN field contains CHAR(12) in DB
							// strTemp2 = "BREGN";
							bregn = rsSecond.getString("BREGN");
							if (bregn == null || bregn.equals(""))
								bregn = "          ";
							if (bregn.length() < 10) {
								strTemp = "";
								for (int i = 0; i < 10 - bregn.length(); i++)
									strTemp = strTemp + " ";
								bregn = bregn + strTemp;
							}
							if (bregn.length() > 10)
								bregn = bregn.substring(0, 10);

							// 2012-02-16 update by JJ, CSGN field contains
							// CHAR(8) in DB, start
							bcsgn = rsSecond.getString("BCSGN");
							if (bcsgn == null || bcsgn.equals(""))
								bcsgn = "        ";
							if (bcsgn.length() < 8) {
								strTemp = "";
								for (int i = 0; i < 8 - bcsgn.length(); i++)
									strTemp += " ";
								bcsgn += strTemp;
							}
							// 2012-02-16 update by JJ, CSGN field contains
							// CHAR(8) in DB, end

							for (int i = 0; i < parkingStandCount; i++)
								rsSecond.previous();

							// To here
							// if(parkingStandCount==0)
							// {
							// //strTemp2 = "BFLNO";
							// bflno = rsSecond.getString("BFLNO");
							// if (bflno==null || bflno.equals("")) bflno =
							// "        ";
							// if(bflno.length() < 8){
							// strTemp = "";
							// for(int i=0; i<8-bflno.length(); i++) strTemp =
							// strTemp + " ";
							// bflno = bflno + strTemp;
							// }
							//
							//
							// //strTemp2 = "BALC3";
							// balc3 =rsSecond.getString("BALC3");
							// if (balc3==null || balc3.equals("")) balc3 =
							// "   ";
							// if(balc3.length() < 3){
							// strTemp = "";
							// for(int i=0; i<3-balc3.length(); i++) strTemp =
							// strTemp + " ";
							// balc3 = balc3 + strTemp;
							// }
							//
							// // REGN field contains CHAR(12) in DB
							// //strTemp2 = "BREGN";
							// bregn = rsSecond.getString("BREGN");
							// if (bregn==null || bregn.equals("")) bregn =
							// "          ";
							// if(bregn.length() < 10){
							// strTemp = "";
							// for(int i=0; i<10-bregn.length(); i++) strTemp =
							// strTemp + " ";
							// bregn = bregn + strTemp;
							// }
							// if (bregn.length() > 10) bregn =
							// bregn.substring(0,10);
							// }
						}
						if (arrIndicator.equals("A")) {
							// strTemp2 = "BOFBL";
							bofbl = rsSecond.getString("BOFBL");
							if (bofbl != null && !bofbl.equals(""))
								try {
									bofbl = tanDt.getLocalDate(bofbl, 8);
								} catch (Exception e) {
									log.error("Error .. Converting AIRB time to GMTDate. AIRB time in DB: "
											+ bofbl
											+ " for Flight: "
											+ bflno
											+ " Error in function billingDetailReports(), Error :"
											+ e.toString());
									bofbl = "              ";
								}
							else
								bofbl = "              ";
						} else {
							bofbl = rsSecond.getString("BONBL");
							if (bofbl != null && !bofbl.equals(""))
								try {
									bofbl = tanDt.getLocalDate(bofbl, 8);
								} catch (Exception e) {
									log.error("Error .. Converting AIRB time to GMTDate. AIRB time in DB: "
											+ bofbl
											+ " for Flight: "
											+ bflno
											+ " Error in function billingDetailReports(), Error :"
											+ e.toString());
									bofbl = "              ";
								}
							else
								bofbl = "              ";

						}

						/*
						 * Added by Alphy on 20-07-2007 to get AIRB for
						 * departure Flight instead of OFBL
						 */
						adid = rsSecond.getString("ADID");
						if (adid != null && !adid.equals("")) {
							if (adid.equals("D"))
								bofbl = bairb;

						}
						/* Added for PLB Feature */
						plbUsed = rsSecond.getString("PLB");
						if (plbUsed != null && !plbUsed.equals("")) {
							plbUsed = plbUsed;

						} else

						{
							plbUsed = "N";
						}

						// 2012-02-16 update by JJ, remark "REMOTE" in FIPS ->
						// PLB = "N"
						bftyp = rsSecond.getString("BFTYP");
						brem1 = rsSecond.getString("BREM1");
						if (bftyp != null && bftyp.equals("T") && brem1 != null
								&& (brem1.indexOf("REMOTE") > -1)) {
							plbUsed = "N";
						}

						strFileLine = strFileLine + aflno + ";" + aalc3 + ";"
								+ aregn + ";" + acsgn + ";"
								+ aland.substring(0, 12) + ";" + bflno + ";"
								+ balc3 + ";" + bregn + ";" + bcsgn + ";"
								+ bairb.substring(0, 12) + ";" + aact5 + ";"
								+ bofbl.substring(0, 12) + ";"
								+ parkingStandCode + ";" + plbUsed;
						// System.out.println(strFileLine);
						bw.write(strFileLine);
						bw.newLine();

					} // End of while loop
				} // End of if(rsFirst != null) loop
			}

			bw.flush();
			bw.close();

		} catch (Exception e) {
			log.error("While reading the info, its wrong() Error in function billingLongstayDetail(), Error : "
					+ e.toString());
		}

		if (encryptFile(depDetFileTemp, depDetFile) != 0)
			log.error("Error while encrypting the file in billingLongstayDetail() function");
	}

	/**
	 * This function prepares VIA string according to finance requirements for
	 * flight.
	 * <p>
	 * <p>
	 * 
	 * @param via
	 *            <code>String</code> is the original VIA value from Database.
	 *            <p>
	 *            <p>
	 */
	public static String getVial(String via) {
		String retViaStr = "";

		// Each via contains 120 characters ..... via structure looks like below
		/*
		 * struct VialType { char display[1]; char apc3[3]; char apc4[4]; char
		 * stoa[14]; char etoa[14]; char land[14]; char onbl[14]; char stod[14];
		 * char etod[14]; char ofbl[14]; char airb[14]; };
		 */

		int viaLen = via.length();

		if (viaLen > 0) {
			int viaCounter = 0;
			int viaValCounter = 1;
			while (viaCounter < viaLen) {
				retViaStr = retViaStr
						+ via.substring(viaCounter, viaCounter + 3);
				viaValCounter = viaValCounter + 120;
				viaCounter = viaCounter + 120;
			}

			int retViaStrCount = 0;
			retViaStrCount = retViaStr.length() / 3;
			for (int i = 0; i < 7 - retViaStrCount; i++)
				retViaStr = retViaStr + "   ";

			return retViaStr;

		} else
			return retViaStr;

	}

	/**
	 * This function prepares VIA string according to finance requirements for
	 * departure flight.
	 * <p>
	 * <p>
	 * 
	 * @param via
	 *            <code>String</code> is the original VIA value from Database.
	 *            <p>
	 *            <p>
	 */
	public static String getVialDep(String via) {
		String retViaStr = "";
		int viaLen = via.length();

		if (viaLen > 0) {
			int viaCounter = 0;
			int viaValCounter = 1;
			while (viaCounter < viaLen) {
				retViaStr = retViaStr
						+ via.substring(viaCounter, viaCounter + 3);
				viaValCounter = viaValCounter + 120;
				viaCounter = viaCounter + 120;
			}
			return retViaStr;

		} else
			return retViaStr;

	}

	/**
	 * This function prepares VIA string according to finance requirements for
	 * flight.
	 * <p>
	 * <p>
	 * 
	 * @param source
	 *            <code>String</code> is the source file to be encrypted.
	 * @param destination
	 *            <code>String</code> is the destination file to be created
	 *            after encryption.
	 *            <p>
	 *            <p>
	 */
	public static long encryptFile(String source, String destination) {
		String lastLine = "";
		try {
			Runtime runTime = Runtime.getRuntime();
			// Process process = runTime.exec("chmod 755 " + source);
			Process process = runTime.exec("encrypt.cgms.sh " + source + " "
					+ destination + " " + " pass_phrase.txt");
			// process = runTime.exec("encrypt.cgms.sh " + source + " " +
			// destination + " " + " pass_phrase.txt");
			DataInputStream dis = new DataInputStream(process.getInputStream());
			lastLine = dis.readLine();
			process = runTime.exec("rm " + source);
		} catch (Exception e) {
			log.error("Error while encrypting the file in encryptFile() function, Error : "
					+ e);
		}

		if (lastLine != null && !lastLine.equals(""))
			return Long.parseLong(lastLine);
		else
			return 1;
	}

	/**
	 * This function create the logger with specified name.
	 * <p>
	 * <p>
	 * 
	 * @param className
	 *            <code>String</code> is the class name.
	 * @param date
	 *            <code>String</code> is the date value to be added to the log
	 *            file name.
	 *            <p>
	 *            <p>
	 */
	public static org.apache.log4j.Logger getProcessLogger(String className,
			String date) {
		String fileName = "/ceda/debug/" + className + date + ".log";
		org.apache.log4j.Logger procLog4j = org.apache.log4j.Logger
				.getLogger(fileName);
		FileAppender appender = null;
		String pattern = "%d [%t] %-5p - %m%n";
		PatternLayout layout = new PatternLayout(pattern);
		try {
			appender = new FileAppender(layout, fileName);
			procLog4j.addAppender(appender);
			procLog4j.setLevel((Level) Level.INFO);
		} catch (Exception e) {
			log.error("While creating the logger in getProcessLogger(), Error : "
					+ e);
		}
		return procLog4j;
	}

}
