rem Copies all generated files from the SCM-Structue into the Install-Shield directory.
rem 

rem set drive to J: (IS 6.2) or K: (IS 8.0) 
set drive=C:\InstallShield

rem Copy ini- and config-files
copy c:\ufis_bin\ceda.ini %drive%\AZ_RMS_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\grp.ini %drive%\AZ_RMS_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\bdps_sec.cfg %drive%\AZ_RMS_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\telexpool.ini %drive%\AZ_RMS_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\LoaTab*.* %drive%\AZ_RMS_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\*.bmp %drive%\AZ_RMS_build\ufis_client_appl\system\*.*


rem copy applications
copy c:\ufis_bin\debug\bdps_sec.exe %drive%\AZ_RMS_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\bdpspass.exe %drive%\AZ_RMS_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\bdpsuif.exe %drive%\AZ_RMS_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\autocoverage.exe %drive%\AZ_RMS_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\fips.exe %drive%\AZ_RMS_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\rules.exe %drive%\AZ_RMS_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\grp.exe %drive%\AZ_RMS_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\opsspm.exe %drive%\AZ_RMS_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\poolalloc.exe %drive%\AZ_RMS_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\regelwerk.exe %drive%\AZ_RMS_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\servicecatalog.exe %drive%\AZ_RMS_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\wgrtool.exe %drive%\AZ_RMS_build\ufis_client_appl\applications\*.*
# copy c:\ufis_bin\debug\profileeditor.exe %drive%\AZ_RMS_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\rostering.exe %drive%\AZ_RMS_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\telexpool.exe %drive%\AZ_RMS_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\loadtabviewer.exe %drive%\AZ_RMS_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\rosteringprint.exe %drive%\AZ_RMS_build\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\alrtrsrv.exe %drive%\AZ_RMS_build\ufis_client_appl\applications\*.*


REM Copy system-files
copy c:\ufis_bin\release\bcproxy.exe %drive%\AZ_RMS_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcproxy.tlb %drive%\AZ_RMS_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcserv32.exe %drive%\AZ_RMS_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufis32.dll %drive%\AZ_RMS_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisappmng.exe %drive%\AZ_RMS_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisappmng.tlb %drive%\AZ_RMS_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisapplmgr.exe %drive%\AZ_RMS_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisapplmgr.tlb %drive%\AZ_RMS_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\tab.ocx %drive%\AZ_RMS_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\tab.tlb %drive%\AZ_RMS_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufiscom.ocx %drive%\AZ_RMS_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufiscom.tlb %drive%\AZ_RMS_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ucom.ocx %drive%\AZ_RMS_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ucom.tlb %drive%\AZ_RMS_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ugantt.ocx %drive%\AZ_RMS_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ugantt.tlb %drive%\AZ_RMS_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\aatlogin.ocx %drive%\AZ_RMS_build\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\aatlogin.tlb %drive%\AZ_RMS_build\ufis_client_appl\system\*.*


rem Copy Help-Files
copy c:\ufis_bin\Help\*.chm %drive%\AZ_RMS_build\ufis_client_appl\help\*.*
