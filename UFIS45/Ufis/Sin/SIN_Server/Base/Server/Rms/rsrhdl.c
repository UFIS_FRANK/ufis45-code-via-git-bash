#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Sin/SIN_Server/Base/Server/Rms/rsrhdl.c 1.7 2005/08/29 23:22:57SGT hag Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :                                                           */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history :                                                           */
/* 20041206 THO: Initial version for PRF 6682								  */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */
/*static char sccs_version[] ="@(#) UFIS (c) ABB AAT/I dummy.c 44.2 / "__DATE__" "__TIME__" / VBL";*/
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "AATArray.h"
#include "db_if.h"
#include "cedatime.h"
#include "fditools.h"
#include "libccstr.h"
#include "l_ccstime.h"

/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE *outp       = NULL;*/
int  debug_level = DEBUG;
#ifdef _HPUX_SOURCE
	extern int daylight;
	extern long timezone;
	extern char *tzname[2];
#else
	extern int _daylight;
	extern long _timezone;
	extern char *_tzname[2];
#endif

#define ARR_NAME_LEN   (18)
#define ARR_FLDLST_LEN (256)

typedef struct _arrayinfo 
{
	HANDLE   rrArrayHandle;
	char     crArrayName[ARR_NAME_LEN+1];
	char     crArrayFieldList[ARR_FLDLST_LEN+1];
	long     lrArrayFieldCnt;
	char   *pcrArrayRowBuf;
	long     lrArrayRowLen;
	long   *plrArrayFieldOfs;
	long   *plrArrayFieldLen;
	HANDLE   rrIdx01Handle;
	char     crIdx01Name[ARR_NAME_LEN+1];
	char     crIdx01FieldList[ARR_FLDLST_LEN+1];
	long     lrIdx01FieldCnt;
	char   *pcrIdx01RowBuf;
	long     lrIdx01RowLen;
	long   *plrIdx01FieldPos;
	long   *plrIdx01FieldOrd;

/*****************/
	HANDLE   rrIdx02Handle;
	char     crIdx02Name[ARR_NAME_LEN+1];
	HANDLE   rrIdx03Handle;
	char     crIdx03Name[ARR_NAME_LEN+1];
	HANDLE   rrIdx04Handle;
	char     crIdx04Name[ARR_NAME_LEN+1];
	/****************************/
} ARRAYINFO;


typedef	char	RELEASEBUFFER[12];	

typedef struct _ReleaseData
{
	int	 bDelete;
	char cUsec[32];
	char cHopo[8];
	char cDateFrTo[18];
	char cShiftPlan[12];
	char pcUrnoList[50*1024];
	char pcUrnoList2[50*1024];
} RELEASEDATA;

/*	MFC Array class implementation	*/

typedef struct	_array_impl
{
	size_t	m_element_size;
	size_t	m_nMaxSize;
	size_t	m_nGrowBy;
	size_t	m_nSize;
	void *	m_pData;
}	ARRAY_IMPL;

typedef struct _ReleaseStaffData
{
	char			cStfUrno[12];
	int				iBeginWeek;
	ARRAY_IMPL		oShifts;
	ARRAY_IMPL		oShifts2;
	ARRAY_IMPL		oFunctions;
	ARRAY_IMPL		oFunctions2;

	time_t			oGPLVafr;
	time_t 			oGPLVato;

	time_t 			oRelFrom;
	time_t 			oRelTo;

	time_t 			oDODM; 			/*	leaving date		*/
	time_t 			oDOEM; 			/*	entry date			*/
	time_t 			oDOBK; 			/*	re-entrance date	*/
} RELEASE_STAFF_DATA;

/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static int   igInitOK      = FALSE;
static char  cgConfigFile[512];
/******************************************************************************/
/* Function prototypes                                                          */
/******************************************************************************/
static int 	SetArrayInfo(char *pcpArrayName, char *pcpTableName,char *pcpArrayFieldList,char *pcpAddFields,long *pcpAddFieldLens,ARRAYINFO *prpArrayInfo,char *pcpSelection,BOOL bpFill);
static int 	FindItemInArrayList(char *pcpInput, char *pcpPattern,char cpDel,int *ipLine,int *ipCol,int *ipPos);
static int 	ReadConfigEntry(char *pcpSection,char *pcpKeyword,char *pcpCfgBuffer);
static int 	GetRowLength(char *pcpHopo, char *pcpTana, char *pcpFieldList,long *pipLen);
static int 	InitRsrhdl();
static int 	Reset(void);                       /* Reset program          */
static void	Terminate(int);                   /* Terminate program      */
static void	HandleSignal(int);                 /* Handles signals        */
static void HandleErr(int);                    /* Handles general errors */
static void HandleQueErr(int);                 /* Handles queuing errors */
static int  HandleData(EVENT *prpEvent);       /* Handles event data     */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/
static void StringTrimRight(char *pcpBuffer);
static int GetItemNbr(char *Item, char *Data, char Delim, long Nbr);
static int ItemCnt(char *pcgData, char cpDelim);
static int StrToTime(int ipUTC,char *pcpTime,time_t *pclTime);
static void  StrAddTime(char *,char*,char *);
static int TimeToStr(int ipUTC,char *pcpTime,time_t lpTime);
static int SendAnswer(int ipDest, int ipRc,char *pcpAnswer);
static int DumpArray(ARRAYINFO *prpArray,int ipIndex,char *pcpValue,char *pcpFields);


static char cgCfgBuffer[1024];
static char cgTabEnd[12];


static ARRAYINFO rgSplArray;
static ARRAYINFO rgGplArray;
static ARRAYINFO rgGspArray;
static ARRAYINFO rgStfArray;
static ARRAYINFO rgOdaArray;
static ARRAYINFO rgBsdArray;
static ARRAYINFO rgPfcArray;
static ARRAYINFO rgDrrArray;
static ARRAYINFO rgScoArray;	

static int igGplURNO;
static int igGplPERI;
static int igGplVAFR;
static int igGplVATO;

static int igGspURNO;
static int igGspWEEK;
static int igGspBUMO;
static int igGspSUMO;
static int igGspP1MO;
static int igGspP2MO;
static int igGspBUTU;
static int igGspSUTU;
static int igGspP1TU;
static int igGspP2TU;
static int igGspBUWE;
static int igGspSUWE;
static int igGspP1WE;
static int igGspP2WE;
static int igGspBUTH;
static int igGspSUTH;
static int igGspP1TH;
static int igGspP2TH;
static int igGspBUFR;
static int igGspSUFR;
static int igGspP1FR;
static int igGspP2FR;
static int igGspBUSA;
static int igGspSUSA;
static int igGspP1SA;
static int igGspP2SA;
static int igGspBUSU;
static int igGspSUSU;
static int igGspP1SU;
static int igGspP2SU;
static int igGspSTFU;

static int igStfDODM;
static int igStfDOEM;

static int igBsdURNO;
static int igBsdBSDC;
static int igBsdESBG;
static int igBsdLSEN;
static int igBsdFCTC;
static int igBsdBKF1;
static int igBsdBKT1;
static int igBsdBKD1;

static int igPfcURNO;
static int igPfcFCTC;

static int igDrrSCOD;
static int igDrrSDAY;
static int igDrrSTFU;

static int igOdaSDAC;
static int igOdaABFR;
static int igOdaABTO;
static int igOdaBLEN;
static int igOdaTBSD;

static int igScoURNO;
static int igScoSURN;
static int igScoVPFR;
static int igScoVPTO;
static int igScoCWEH;

const char *SPL_FIELDS = "URNO";
const char *GPL_FIELDS = "URNO,SPLU,PERI,VAFR,VATO";
const char *GSP_FIELDS = "URNO,GPLU,WEEK,BUMO,SUMO,P1MO,P2MO,BUTU,SUTU,P1TU,P2TU,BUWE,SUWE,P1WE,P2WE,BUTH,SUTH,P1TH,P2TH,BUFR,SUFR,P1FR,P2FR,BUSA,SUSA,P1SA,P2SA,BUSU,SUSU,P1SU,P2SU,STFU";
const char *STF_FIELDS = "URNO,DODM,DOEM";
const char *ODA_FIELDS = "URNO,SDAC,ABFR,ABTO,BLEN,TBSD";
const char *BSD_FIELDS = "URNO,BSDC,ESBG,LSEN,FCTC,BKF1,BKT1,BKD1";
const char *PFC_FIELDS = "URNO,FCTC";
const char *DRR_FIELDS = "URNO,SCOD,FCTC,DRRN,SDAY,ROSL,ROSS,STFU,CDAT,LSTU,USEC,USEU,SCOO,BSDU,AVFR,AVTO,SBFR,SBTO,PRFL,SBLU,DRS2";
const char *SCO_FIELDS = "URNO,SURN,VPFR,VPTO,CWEH";

static char	cgSplFields[512];
static char	cgGplFields[512];
static char	cgGspFields[512];
static char	cgStfFields[512];
static char	cgOdaFields[512];
static char	cgBsdFields[512];
static char	cgPfcFields[512];
static char	cgDrrFields[512];
static char cgScoFields[512];
static char cgDrrReloadHint[256];

static AAT_PARAM *prgRsrInf = NULL;

#define FIELDVAL(rrArray,pcpBuf,ipFieldNo) (&pcpBuf[rrArray.plrArrayFieldOfs[ipFieldNo]])
#define PFIELDVAL(prArray,pcpBuf,ipFieldNo) (&pcpBuf[prArray->plrArrayFieldOfs[ipFieldNo]])
extern int get_no_of_items(char *s);


static int igStartUpMode = TRACE;
static int igRuntimeMode = TRACE;

static char cgUser[48] = "RSRHDL";
static char cgHopo[24] = "";

static char cgTwStart[128];
static char cgTwEnd[128];
static char cgRecvName[128];
static char cgDestName[128];
static int	igQueOut;

static BOOL bgSendBroadcast = TRUE;
static BOOL bgSend2Action = TRUE;

static int  igBchdlModid = 1900;
static int 	igActionModid = 7400; 

#define CMD_IRT     (0)
#define CMD_URT     (1)
#define CMD_DRT     (2)
#define CMD_RSR     (3)

#define IDX_IRTD 4	/* Index of List "INSERT" in prgDemInf->DataList */
#define IDX_URTD 5  /* Index of List "UPDATE" in prgDemInf->DataList */
#define IDX_DRTU 6  /* Index of List "DELETE" in prgDemInf->DataList */
#define IDX_IRTU 7	/* Index of List "IRT_URNO" in prgDemInf->DataList */
#define IDX_URTU 8	/* Index of List "URT_URNO" in prgDemInf->DataList */
#define IDX_DRTD 9	/* Index of List "DRT_DATA" in prgDemInf->DataList */

static char *prgCmdTxt[] = { "IRT",   "URT",   "DRT",  "RSR", NULL };
static int    rgCmdDef[] = { CMD_IRT, CMD_URT,CMD_DRT,CMD_RSR, 0 };

/*
** URNO pool
*/
#define	URNO_POOL_SIZE 100        /* number of URNOS fetched with each refill of pool */
#define	URNO_SIZE 12              /* maximum size of URNO in string representation    */ 

/*	MFC Array class implementation	*/

ARRAY_IMPL ArrayCreate(size_t element_size)
{
	ARRAY_IMPL impl;

	impl.m_element_size = element_size;
	impl.m_nMaxSize   	= 0;
	impl.m_nGrowBy	  	= 0;
	impl.m_nSize	  	= 0;
	impl.m_pData		= NULL;

	return impl;
}

int	ArrayGetSize(ARRAY_IMPL *impl)
{
	if (impl == NULL)
	{
		dbg(TRACE,"MfcArray:%s ASSERT(%s) failed at line %d","GetSize","impl != NULL",__LINE__);
		return -1;
	}
	return impl->m_nSize;
}

void ArraySetSize(ARRAY_IMPL *impl,size_t nNewSize,int nGrowBy)
{
	if (impl == NULL)
	{
		dbg(TRACE,"MfcArray::%s ASSERT(%s) failed at line %d","SetSize","impl != NULL",__LINE__);
		return;
	}

	if (nGrowBy != -1)
	{
		impl->m_nGrowBy = nGrowBy;
	}
	
	if (nNewSize == 0)
	{
		free(impl->m_pData);
		impl->m_pData = NULL;
		impl->m_nSize = impl->m_nMaxSize = 0;		
	}
	else if (impl->m_pData == NULL)
	{
		impl->m_pData = malloc(nNewSize * impl->m_element_size);
		memset(impl->m_pData,0,nNewSize * impl->m_element_size);
		impl->m_nSize = impl->m_nMaxSize = nNewSize;		
	}
	else if (nNewSize <= impl->m_nMaxSize)
	{
		if (nNewSize > impl->m_nSize)
		{
			char *dest = (char *)impl->m_pData + impl->m_nSize * impl->m_element_size;		
			memset(dest,0,(nNewSize-impl->m_nSize)*	impl->m_element_size);			
		}
		
		impl->m_nSize = nNewSize;
	}
	else
	{
		int nNewMax;
		void *pNewData;
		char *dest;
		
		int nGrowBy = impl->m_nGrowBy;
		if (nGrowBy == 0)
		{
/*			nGrowBy = min(1024,max(4,impl->m_nSize / 8));			*/
			nGrowBy = 100;
		}		
		
		if (nNewSize < impl->m_nMaxSize + nGrowBy)
			nNewMax = impl->m_nMaxSize + nGrowBy;
		else
			nNewMax = nNewSize;
			
		pNewData = malloc(nNewMax * impl->m_element_size);
		
		memcpy(pNewData,impl->m_pData,impl->m_nSize * impl->m_element_size);
		
		dest = (char *)impl->m_pData + impl->m_nSize * impl->m_element_size;		
		memset(dest,0,(nNewSize-impl->m_nSize)*	impl->m_element_size);
		
		free(impl->m_pData);
		impl->m_pData 	= pNewData;
		impl->m_nSize 	= nNewSize;
		impl->m_nMaxSize= nNewMax;
	}
}

void ArrayDelete(ARRAY_IMPL *impl)
{
	if (impl == NULL)
	{
		dbg(TRACE,"MfcArray::%s ASSERT(%s) failed at line %d","Delete","impl != NULL",__LINE__);
		return;
	}

	if (impl->m_pData != NULL)
	{
		free(impl->m_pData);
		impl->m_pData = NULL;
	}

	impl->m_nMaxSize = 0;
	impl->m_nSize = 0;
}

void ArraySetAt(ARRAY_IMPL *impl,size_t nIndex,void *element)
{
	char *dest = NULL;

	int result = -1;

	if (impl == NULL)
	{
		dbg(TRACE,"MfcArray::%s ASSERT(%s) failed at line %d","SetAt","impl != NULL",__LINE__);
		return;
	}

	if (nIndex < 0 || nIndex >= impl->m_nSize)
	{
		dbg(TRACE,"MfcArray::%s ASSERT(%s) failed at line %d","SetAt","nIndex >= 0 && nIndex < impl->m_nSize",__LINE__);
		return;
	}
	
	if (element == NULL)
	{
		dbg(TRACE,"MfcArray::%s ASSERT(%s) failed at line %d","SetAt","element != null",__LINE__);
		return;
	}

	dest = (char *)impl->m_pData + nIndex * impl->m_element_size;
	
	memmove(dest,element,impl->m_element_size);	
}

void ArraySetAtGrow(ARRAY_IMPL *impl,size_t nIndex,void *element)
{
	if (impl == NULL)
	{
		dbg(TRACE,"MfcArray::%s ASSERT(%s) failed at line %d","SetAtGrow","impl != NULL",__LINE__);
		return;
	}

	if (nIndex >= impl->m_nSize)
	{
		ArraySetSize(impl,nIndex + 1,-1);		
	}

	ArraySetAt(impl,nIndex,element);
}

int ArrayAdd(ARRAY_IMPL *impl,void *element)
{
	int nIndex;
	
	if (impl == NULL)
	{
		dbg(TRACE,"MfcArray::%s ASSERT(%s) failed at line %d","Add","impl != NULL",__LINE__);
		return -1;
	}

	if (element == NULL)
	{
		dbg(TRACE,"MfcArray::%s ASSERT(%s) failed at line %d","Add","element != NULL",__LINE__);
		return -1;
	}

	nIndex = impl->m_nSize;
	ArraySetAtGrow(impl,nIndex,element);
	return nIndex;
}

void* ArrayGetAt(ARRAY_IMPL *impl,size_t nIndex)
{
	char *dest = NULL;

	if (impl == NULL)
	{
		dbg(TRACE,"MfcArray::%s ASSERT(%s) failed at line %d","GetAt","impl != NULL",__LINE__);
		return NULL;
	}

	if (nIndex < 0 || nIndex >= impl->m_nSize)
	{
		dbg(TRACE,"MfcArray::%s ASSERT(%s) failed at line %d","GetAt","nIndex >= 0 && nIndex < impl->m_nSize",__LINE__);
		return NULL;
	}

	dest = (char *)impl->m_pData + nIndex * impl->m_element_size;

	return dest;
}

int ArrayAppend(ARRAY_IMPL *impl,ARRAY_IMPL *src)
{
	int nOldSize;
	char *dest;
	
	if (impl == NULL)
	{
		dbg(TRACE,"MfcArray::%s ASSERT(%s) failed at line %d","Append","impl != NULL",__LINE__);
		return -1;
	}

	if (src == NULL)
	{
		dbg(TRACE,"MfcArray::%s ASSERT(%s) failed at line %d","Append","src != NULL",__LINE__);
		return -1;
	}

	if (impl->m_element_size != src->m_element_size)
	{
		dbg(TRACE,"MfcArray::%s ASSERT(%s) failed at line %d","Append","impl->m_element_size == src->m_element_size",__LINE__);
		return -1;
	}
		
	nOldSize = impl->m_nSize;
	ArraySetSize(impl,impl->m_nSize + src->m_nSize,-1);
	
	dest = (char *)impl->m_pData + nOldSize * impl->m_element_size;
	memcpy(dest,src->m_pData,src->m_nSize * src->m_element_size);	
	
	return nOldSize;
}


void ArrayRemoveAll(ARRAY_IMPL *impl)
{
	if (impl == NULL)
	{
		dbg(TRACE,"MfcArray::%s ASSERT(%s) failed at line %d","RemoveAll","impl != NULL",__LINE__);
		return;
	}

	ArraySetSize(impl,0,-1);
}

void ArrayRemoveAt(ARRAY_IMPL *impl,size_t nIndex)
{
	int nCount = 1;
	int nMoveCount;
	
	if (impl == NULL)
	{
		dbg(TRACE,"MfcArray::%s ASSERT(%s) failed at line %d","RemoveAt","impl != NULL",__LINE__);
		return;
	}

	if (nIndex + nCount > impl->m_nSize)
	{
		dbg(TRACE,"MfcArray::%s ASSERT(%s) failed at line %d","RemoveAt","nIndex + nCount <= impl->m_nSize",__LINE__);
		return;
	}

	nMoveCount = impl->m_nSize - (nIndex + nCount);
	
	if (nMoveCount)
	{
		char *src = (char *)impl->m_pData + (nIndex + nCount) * impl->m_element_size;
		char *dest= (char *)impl->m_pData + nIndex * impl->m_element_size;
		memmove(dest,src,nMoveCount * impl->m_element_size);		
		impl->m_nSize -= nCount;
	}
}

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
    int    ilRc = RC_SUCCESS;            /* Return code            */
    int    ilCnt = 0;
    
    INITIALIZE;            /* General initialization    */

    dbg(TRACE,"MAIN: version <%s>",mks_version);
    /* Attach to the MIKE queues */
    do
    {
        ilRc = init_que();
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        }/* end of if */
    }
    while((ilCnt < 10) && (ilRc != RC_SUCCESS));
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
        sleep(60);
        exit(1);
    }
    else
    {
        dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
    }/* end of if */

	do
	{
		ilRc = init_db();
		if (ilRc != RC_SUCCESS)
		{
			check_ret(ilRc);
			dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		} /* end of if */
	} 
	while((ilCnt < 10) && (ilRc != RC_SUCCESS));

    /* uncomment if necessary */
    sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
    if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
        dbg(DEBUG,"MAIN: waiting for status switch ...");
        HandleQueues();
    }/* end of if */
    
    if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
        dbg(TRACE,"MAIN: initializing ...");
        if(igInitOK == FALSE)
        {
            ilRc = InitRsrhdl();
            if(ilRc != RC_SUCCESS)
            {
                dbg(TRACE,"InitRsrhdl: init failed!");
            } /* end of if */
        }/* end of if */
    } 
    else 
    {
        Terminate(1);
    }/* end of if */
    
    dbg(TRACE,"MAIN: initializing OK");
    for(;;)
    {
        ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text;
                
        if( ilRc == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRc != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRc);
            } /* fi */
            
            switch( prgEvent->command )
            {
            case    HSB_STANDBY    :
                ctrl_sta = prgEvent->command;
                HandleQueues();
                break;    
            case    HSB_COMING_UP    :
                ctrl_sta = prgEvent->command;
                HandleQueues();
                break;    
            case    HSB_ACTIVE    :
                ctrl_sta = prgEvent->command;
                break;    
            case    HSB_ACT_TO_SBY    :
                ctrl_sta = prgEvent->command;
                /* CloseConnection(); */
                HandleQueues();
                break;    
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate(1);
                break;    
            case    HSB_STANDALONE    :
                ctrl_sta = prgEvent->command;

                break;    

            case    SHUTDOWN    :
                /* process shutdown - maybe from uutil */
                Terminate(1);
                break;
                    
            case    RESET        :
                ilRc = Reset();
                break;
                    
            case    EVENT_DATA    :
                if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
                {
                    ilRc = HandleData(prgEvent);
                    if(ilRc != RC_SUCCESS)
                    {
                        HandleErr(ilRc);
                    }/* end of if */
                }
                else
                {
                    dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
                    DebugPrintItem(TRACE,prgItem);
                    DebugPrintEvent(TRACE,prgEvent);
                }/* end of if */
                break;
                    
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;
            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;
            default            :
                dbg(TRACE,"MAIN: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        } 
        else 
        {
            /* Handle queuing errors */
            HandleQueErr(ilRc);
        } /* end else */
        
    } /* end for */
    
} /* end of MAIN */

static int FindItemInArrayList(char *pcpInput, char *pcpPattern,char cpDel,int *ipLine,int *ipCol,int *ipPos)
{
	int ilRc = RC_SUCCESS;

	ilRc = FindItemInList(pcpInput,pcpPattern,cpDel,ipLine,ipCol,ipPos);
	*ipLine = (*ipLine) -1;
	
	return ilRc;
}

static int InitFieldIndex()
{
	int ilRc = RC_SUCCESS;
	int ilCol,ilPos;

	strcpy(cgGplFields,GPL_FIELDS);
	FindItemInArrayList(cgGplFields,"URNO",',',&igGplURNO,&ilCol,&ilPos);
	FindItemInArrayList(cgGplFields,"PERI",',',&igGplPERI,&ilCol,&ilPos);
	FindItemInArrayList(cgGplFields,"VAFR",',',&igGplVAFR,&ilCol,&ilPos);
	FindItemInArrayList(cgGplFields,"VATO",',',&igGplVATO,&ilCol,&ilPos);

	strcpy(cgGspFields,GSP_FIELDS);
	FindItemInArrayList(cgGspFields,"URNO",',',&igGspURNO,&ilCol,&ilPos);
	FindItemInArrayList(cgGspFields,"WEEK",',',&igGspWEEK,&ilCol,&ilPos);
	FindItemInArrayList(cgGspFields,"BUMO",',',&igGspBUMO,&ilCol,&ilPos);
	FindItemInArrayList(cgGspFields,"SUMO",',',&igGspSUMO,&ilCol,&ilPos);
	FindItemInArrayList(cgGspFields,"P1MO",',',&igGspP1MO,&ilCol,&ilPos);
	FindItemInArrayList(cgGspFields,"P2MO",',',&igGspP2MO,&ilCol,&ilPos);
	FindItemInArrayList(cgGspFields,"BUTU",',',&igGspBUTU,&ilCol,&ilPos);
	FindItemInArrayList(cgGspFields,"SUTU",',',&igGspSUTU,&ilCol,&ilPos);
	FindItemInArrayList(cgGspFields,"P1TU",',',&igGspP1TU,&ilCol,&ilPos);
	FindItemInArrayList(cgGspFields,"P2TU",',',&igGspP2TU,&ilCol,&ilPos);
	FindItemInArrayList(cgGspFields,"BUWE",',',&igGspBUWE,&ilCol,&ilPos);
	FindItemInArrayList(cgGspFields,"SUWE",',',&igGspSUWE,&ilCol,&ilPos);
	FindItemInArrayList(cgGspFields,"P1WE",',',&igGspP1WE,&ilCol,&ilPos);
	FindItemInArrayList(cgGspFields,"P2WE",',',&igGspP2WE,&ilCol,&ilPos);
	FindItemInArrayList(cgGspFields,"BUTH",',',&igGspBUTH,&ilCol,&ilPos);
	FindItemInArrayList(cgGspFields,"SUTH",',',&igGspSUTH,&ilCol,&ilPos);
	FindItemInArrayList(cgGspFields,"P1TH",',',&igGspP1TH,&ilCol,&ilPos);
	FindItemInArrayList(cgGspFields,"P2TH",',',&igGspP2TH,&ilCol,&ilPos);
	FindItemInArrayList(cgGspFields,"BUFR",',',&igGspBUFR,&ilCol,&ilPos);
	FindItemInArrayList(cgGspFields,"SUFR",',',&igGspSUFR,&ilCol,&ilPos);
	FindItemInArrayList(cgGspFields,"P1FR",',',&igGspP1FR,&ilCol,&ilPos);
	FindItemInArrayList(cgGspFields,"P2FR",',',&igGspP2FR,&ilCol,&ilPos);
	FindItemInArrayList(cgGspFields,"BUSA",',',&igGspBUSA,&ilCol,&ilPos);
	FindItemInArrayList(cgGspFields,"SUSA",',',&igGspSUSA,&ilCol,&ilPos);
	FindItemInArrayList(cgGspFields,"P1SA",',',&igGspP1SA,&ilCol,&ilPos);
	FindItemInArrayList(cgGspFields,"P2SA",',',&igGspP2SA,&ilCol,&ilPos);
	FindItemInArrayList(cgGspFields,"BUSU",',',&igGspBUSU,&ilCol,&ilPos);
	FindItemInArrayList(cgGspFields,"SUSU",',',&igGspSUSU,&ilCol,&ilPos);
	FindItemInArrayList(cgGspFields,"P1SU",',',&igGspP1SU,&ilCol,&ilPos);
	FindItemInArrayList(cgGspFields,"P2SU",',',&igGspP2SU,&ilCol,&ilPos);
	FindItemInArrayList(cgGspFields,"STFU",',',&igGspSTFU,&ilCol,&ilPos);

	strcpy(cgStfFields,STF_FIELDS);
	FindItemInArrayList(cgStfFields,"DODM",',',&igStfDODM,&ilCol,&ilPos);
	FindItemInArrayList(cgStfFields,"DOEM",',',&igStfDOEM,&ilCol,&ilPos);

	strcpy(cgBsdFields,BSD_FIELDS);
	FindItemInArrayList(cgBsdFields,"URNO",',',&igBsdURNO,&ilCol,&ilPos);
	FindItemInArrayList(cgBsdFields,"BSDC",',',&igBsdBSDC,&ilCol,&ilPos);
	FindItemInArrayList(cgBsdFields,"ESBG",',',&igBsdESBG,&ilCol,&ilPos);
	FindItemInArrayList(cgBsdFields,"LSEN",',',&igBsdLSEN,&ilCol,&ilPos);
	FindItemInArrayList(cgBsdFields,"FCTC",',',&igBsdFCTC,&ilCol,&ilPos);
	FindItemInArrayList(cgBsdFields,"BKF1",',',&igBsdBKF1,&ilCol,&ilPos);
	FindItemInArrayList(cgBsdFields,"BKT1",',',&igBsdBKT1,&ilCol,&ilPos);
	FindItemInArrayList(cgBsdFields,"BKD1",',',&igBsdBKD1,&ilCol,&ilPos);

	strcpy(cgPfcFields,PFC_FIELDS);
	FindItemInArrayList(cgPfcFields,"URNO",',',&igPfcURNO,&ilCol,&ilPos);
	FindItemInArrayList(cgPfcFields,"FCTC",',',&igPfcFCTC,&ilCol,&ilPos);

	strcpy(cgDrrFields,DRR_FIELDS);
	FindItemInArrayList(cgDrrFields,"SCOD",',',&igDrrSCOD,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"SDAY",',',&igDrrSDAY,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"STFU",',',&igDrrSTFU,&ilCol,&ilPos);

	strcpy(cgOdaFields,ODA_FIELDS);
	FindItemInArrayList(cgOdaFields,"SDAC",',',&igOdaSDAC,&ilCol,&ilPos);
	FindItemInArrayList(cgOdaFields,"ABFR",',',&igOdaABFR,&ilCol,&ilPos);
	FindItemInArrayList(cgOdaFields,"ABTO",',',&igOdaABTO,&ilCol,&ilPos);
	FindItemInArrayList(cgOdaFields,"BLEN",',',&igOdaBLEN,&ilCol,&ilPos);
	FindItemInArrayList(cgOdaFields,"TBSD",',',&igOdaTBSD,&ilCol,&ilPos);

	strcpy(cgScoFields,SCO_FIELDS);
	FindItemInArrayList(cgScoFields,"URNO",',',&igScoURNO,&ilCol,&ilPos);
	FindItemInArrayList(cgScoFields,"SURN",',',&igScoSURN,&ilCol,&ilPos);
	FindItemInArrayList(cgScoFields,"VPFR",',',&igScoVPFR,&ilCol,&ilPos);
	FindItemInArrayList(cgScoFields,"VPTO",',',&igScoVPTO,&ilCol,&ilPos);
	FindItemInArrayList(cgScoFields,"CWEH",',',&igScoCWEH,&ilCol,&ilPos);


	return ilRc;

}

static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,char *pcpCfgBuffer)
{
	int ilRc = RC_SUCCESS;

	char clSection[124];
	char clKeyword[124];

		strcpy(clSection,pcpSection);
		strcpy(clKeyword,pcpKeyword);

		ilRc = iGetConfigEntry(cgConfigFile,clSection,clKeyword,
				CFG_STRING,pcpCfgBuffer);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
		} 
		else
		{
			dbg(DEBUG,"Config Entry <%s>,<%s>:<%s> found in %s",&clSection[0],&clKeyword[0],pcpCfgBuffer,cgConfigFile);
		}/* end of if */
		return ilRc;
}

static int GetDebugLevel(char *pcpMode, int *pipMode)
{
	int		ilRc = RC_SUCCESS;
	char	clCfgValue[64];

	ilRc = iGetConfigRow(cgConfigFile, "SYSTEM", pcpMode, CFG_STRING,clCfgValue);

	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"SYSTEM %s not found in <%s>", pcpMode, cgConfigFile);
	}
	else
	{
		dbg(TRACE, "SYSTEM %s <%s>", pcpMode, clCfgValue);
		if (!strcmp(clCfgValue, "DEBUG"))
			*pipMode = DEBUG;
		else if (!strcmp(clCfgValue, "TRACE"))
			*pipMode = TRACE;
		else
			*pipMode = 0;
	}

	return ilRc;
}

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int InitRsrhdl()
{
	int  ilRc = RC_SUCCESS;			/* Return code */
  	char clSection[64];
  	char clKeyword[64];
  	char clNow[64];
	/*time_t tlNow;*/
	char pclSelection[1024];

  	SetSignals(HandleSignal);
  	igInitOK = TRUE;

	if(ilRc == RC_SUCCESS)
	{
		 GetDebugLevel("STARTUP_MODE", &igStartUpMode);
		 GetDebugLevel("RUNTIME_MODE", &igRuntimeMode);
	}
	debug_level = igStartUpMode;

	GetServerTimeStamp("UTC",1,0,clNow);

	dbg(DEBUG,"Now = <%s>",clNow);

	/* now reading from configfile or from database */

	if(ilRc == RC_SUCCESS)
	{
		/* read HomeAirPort from SGS.TAB */
		memset((void*)&cgHopo[0], 0x00, 8);

		sprintf(&clSection[0],"SYS");
		sprintf(&clKeyword[0],"HOMEAP");

		ilRc = tool_search_exco_data(&clSection[0], &clKeyword[0], &cgHopo[0]);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"<InitRsrhdl> EXTAB,%s,%s not found in SGS.TAB",&clSection[0], &clKeyword[0]);
			Terminate(30);
		} else {
			dbg(TRACE,"<InitRsrhdl> home airport    <%s>",&cgHopo[0]);
		}/* end of if */
	}/* end of if */

	
	if(ilRc == RC_SUCCESS)
	{
		sprintf(&clSection[0],"ALL");
		sprintf(&clKeyword[0],"TABEND");

		ilRc = tool_search_exco_data(&clSection[0], &clKeyword[0], &cgTabEnd[0]);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"<InitRsrhdl> EXTAB,%s,%s not found in SGS.TAB",&clSection[0], &clKeyword[0]);
			Terminate(30);
		} else {
			dbg(TRACE,"<InitRsrhdl> table extension <%s>",&cgTabEnd[0]);
		}/* end of if */
	}/* end of if */

#ifndef _LINUX
	#ifdef _HPUX_SOURCE
	dbg ( TRACE, "Initial values daylight <%d> timezone <%ld>, zone0 <%s> zone1 <%s>",
				 daylight, timezone, tzname[0], tzname[1] );
	daylight = 0;
	#else
	dbg ( TRACE, "Initial values _daylight <%d> _timezone <%ld>, zone0 <%s> zone1 <%s>",
				 _daylight, _timezone, _tzname[0], _tzname[1] );
	_daylight = 0;
	#endif
#endif

	if (ilRc == RC_SUCCESS)
	{
		if((ilRc = tool_get_q_id("bchdl")) != RC_FAIL)
		{
			igBchdlModid = ilRc;
		}
		if((ilRc = tool_get_q_id("action")) != RC_FAIL)
		{
			igActionModid = ilRc;
		}

		ilRc = RC_SUCCESS;
	}

  	ilRc = iGetConfigRow(cgConfigFile,"MAIN","DrrReloadHint", CFG_STRING,cgDrrReloadHint);
   	if ( ilRc != RC_SUCCESS )
   	{
    	strcpy ( cgDrrReloadHint, "/*+ INDEX(DRRTAB DRRTAB_SDAY_ROSL) */");
    }
    
   	dbg(TRACE,"InitPolhdl: Hint for reloading DRRTAB <%s>", cgDrrReloadHint );


	if(ilRc == RC_SUCCESS)
	{
		ilRc = InitFieldIndex();
	}
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CEDAArrayInitialize(20,20);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitRsrhdl: CEDAArrayInitialize failed <%d>",ilRc);
		}
	}
	
	/* Check fieldlist for SPL */
	if (ilRc == RC_SUCCESS)
	{
		strcpy(cgSplFields,SPL_FIELDS);
		sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);

		dbg(TRACE,"InitRsrhdl: SetArrayInfo SPLTAB array");
		ilRc = SetArrayInfo("SPLTAB","SPLTAB",cgSplFields,NULL,NULL,&rgSplArray,pclSelection,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitRsrhdl: SetArrayInfo SPLTAB failed <%d>",ilRc);
		}
		else
		{
			strcpy(rgSplArray.crIdx01Name,"URNO");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgSplArray.rrArrayHandle,
												rgSplArray.crArrayName,
												&rgSplArray.rrIdx01Handle,
												rgSplArray.crIdx01Name,"URNO");
			if (ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"Index <%s> not created RC=%d", rgSplArray.crIdx01Name,ilRc);
			}
			
			if (ilRc == RC_SUCCESS)
			{
				dbg(DEBUG,"InitRsrhdl: SetArrayInfo SPLTAB OK");
				dbg(DEBUG,"<%s> %d",rgSplArray.crArrayName, rgSplArray.rrArrayHandle);
				ilRc = ConfigureAction("SPLTAB", 0, "URT,IRT,DRT" );
				if(ilRc != RC_SUCCESS)
				{
					dbg(TRACE,"InitRsrhdl: ConfigureAction failed <%d>",ilRc);
				}
			}
		}
	}

	/* Check fieldlist for GPL */
	if (ilRc == RC_SUCCESS)
	{
		strcpy(cgGplFields,GPL_FIELDS);
		sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);

		dbg(TRACE,"InitRsrhdl: SetArrayInfo GPLTAB array");
		ilRc = SetArrayInfo("GPLTAB","GPLTAB",cgGplFields,NULL,NULL,&rgGplArray,pclSelection,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitRsrhdl: SetArrayInfo GPLTAB failed <%d>",ilRc);
		}
		else
		{
			strcpy(rgGplArray.crIdx01Name,"URNO");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgGplArray.rrArrayHandle,
												rgGplArray.crArrayName,
												&rgGplArray.rrIdx01Handle,
												rgGplArray.crIdx01Name,"URNO");
			if (ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"Index <%s> not created RC=%d", rgGplArray.crIdx01Name,ilRc);
			}
			
			if (ilRc == RC_SUCCESS)
			{

				strcpy(rgGplArray.crIdx02Name,"SPLU");
				ilRc = CEDAArrayCreateSimpleIndexUp(&rgGplArray.rrArrayHandle,
													rgGplArray.crArrayName,
													&rgGplArray.rrIdx02Handle,
													rgGplArray.crIdx02Name,"SPLU");
				if (ilRc != RC_SUCCESS)
				{
					dbg(TRACE,"Index <%s> not created RC=%d", rgGplArray.crIdx02Name,ilRc);
				}
				
				if (ilRc == RC_SUCCESS)
				{				
					dbg(DEBUG,"InitRsrhdl: SetArrayInfo GPLTAB OK");
					dbg(DEBUG,"<%s> %d",rgGplArray.crArrayName, rgGplArray.rrArrayHandle);
					ilRc = ConfigureAction("GPLTAB", 0, "URT,IRT,DRT" );
					if(ilRc != RC_SUCCESS)
					{
						dbg(TRACE,"InitRsrhdl: ConfigureAction failed <%d>",ilRc);
					}
				}
			}
		}
	}

	/* Check fieldlist for GSP */
	if (ilRc == RC_SUCCESS)
	{
		strcpy(cgGspFields,GSP_FIELDS);
		sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);

		dbg(TRACE,"InitRsrhdl: SetArrayInfo GSPTAB array");
		ilRc = SetArrayInfo("GSPTAB","GSPTAB",cgGspFields,NULL,NULL,&rgGspArray,pclSelection,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitRsrhdl: SetArrayInfo GSPTAB failed <%d>",ilRc);
		}
		else
		{
			strcpy(rgGspArray.crIdx01Name,"URNO");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgGspArray.rrArrayHandle,
												rgGspArray.crArrayName,
												&rgGspArray.rrIdx01Handle,
												rgGspArray.crIdx01Name,"URNO");
			if (ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"Index <%s> not created RC=%d", rgGspArray.crIdx01Name,ilRc);
			}
			
			if (ilRc == RC_SUCCESS)
			{

				strcpy(rgGspArray.crIdx02Name,"GPLU");
				ilRc = CEDAArrayCreateSimpleIndexUp(&rgGspArray.rrArrayHandle,
													rgGspArray.crArrayName,
													&rgGspArray.rrIdx02Handle,
													rgGspArray.crIdx02Name,"GPLU");
				if (ilRc != RC_SUCCESS)
				{
					dbg(TRACE,"Index <%s> not created RC=%d", rgGspArray.crIdx02Name,ilRc);
				}
				
				if (ilRc == RC_SUCCESS)
				{				
					dbg(DEBUG,"InitRsrhdl: SetArrayInfo GSPTAB OK");
					dbg(DEBUG,"<%s> %d",rgGspArray.crArrayName, rgGspArray.rrArrayHandle);
					ilRc = ConfigureAction("GSPTAB", 0, "URT,IRT,DRT" );
					if(ilRc != RC_SUCCESS)
					{
						dbg(TRACE,"InitRsrhdl: ConfigureAction failed <%d>",ilRc);
					}
				}
			}
		}
	}

	/* Check fieldlist for STF */
	if (ilRc == RC_SUCCESS)
	{
		strcpy(cgStfFields,STF_FIELDS);
		sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);

		dbg(TRACE,"InitRsrhdl: SetArrayInfo STFTAB array");
		ilRc = SetArrayInfo("STFTAB","STFTAB",cgStfFields,NULL,NULL,&rgStfArray,pclSelection,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitRsrhdl: SetArrayInfo STFTAB failed <%d>",ilRc);
		}
		else
		{
			strcpy(rgStfArray.crIdx01Name,"URNO");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgStfArray.rrArrayHandle,
												rgStfArray.crArrayName,
												&rgStfArray.rrIdx01Handle,
												rgStfArray.crIdx01Name,"URNO");
			if (ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"Index <%s> not created RC=%d", rgStfArray.crIdx01Name,ilRc);
			}
			
			if (ilRc == RC_SUCCESS)
			{
				dbg(DEBUG,"InitRsrhdl: SetArrayInfo STFTAB OK");
				dbg(DEBUG,"<%s> %d",rgStfArray.crArrayName, rgStfArray.rrArrayHandle);
				ilRc = ConfigureAction("STFTAB", 0, "URT,IRT,DRT" );
				if(ilRc != RC_SUCCESS)
				{
					dbg(TRACE,"InitRsrhdl: ConfigureAction failed <%d>",ilRc);
				}
			}
		}
	}

	/* Check fieldlist for BSD */
	if (ilRc == RC_SUCCESS)
	{
		strcpy(cgBsdFields,BSD_FIELDS);
		sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);

		dbg(TRACE,"InitRsrhdl: SetArrayInfo BSDTAB array");
		ilRc = SetArrayInfo("BSDTAB","BSDTAB",cgBsdFields,NULL,NULL,&rgBsdArray,pclSelection,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitRsrhdl: SetArrayInfo BSDTAB failed <%d>",ilRc);
		}
		else
		{
			strcpy(rgBsdArray.crIdx01Name,"URNO");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgBsdArray.rrArrayHandle,
												rgBsdArray.crArrayName,
												&rgBsdArray.rrIdx01Handle,
												rgBsdArray.crIdx01Name,"URNO");
			if (ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"Index <%s> not created RC=%d", rgBsdArray.crIdx01Name,ilRc);
			}
			
			if (ilRc == RC_SUCCESS)
			{
				dbg(DEBUG,"InitRsrhdl: SetArrayInfo BSDTAB OK");
				dbg(DEBUG,"<%s> %d",rgBsdArray.crArrayName, rgBsdArray.rrArrayHandle);
				ilRc = ConfigureAction("BSDTAB", 0, "URT,IRT,DRT" );
				if(ilRc != RC_SUCCESS)
				{
					dbg(TRACE,"InitRsrhdl: ConfigureAction failed <%d>",ilRc);
				}
			}
		}
	}


	/* Check fieldlist for ODA */
	if (ilRc == RC_SUCCESS)
	{
		strcpy(cgOdaFields,ODA_FIELDS);
		sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);

		dbg(TRACE,"InitRsrhdl: SetArrayInfo ODATAB array");
		ilRc = SetArrayInfo("ODATAB","ODATAB",cgOdaFields,NULL,NULL,&rgOdaArray,pclSelection,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitRsrhdl: SetArrayInfo ODATAB failed <%d>",ilRc);
		}
		else
		{
			strcpy(rgOdaArray.crIdx01Name,"URNO");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgOdaArray.rrArrayHandle,
												rgOdaArray.crArrayName,
												&rgOdaArray.rrIdx01Handle,
												rgOdaArray.crIdx01Name,"URNO");
			if (ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"Index <%s> not created RC=%d", rgOdaArray.crIdx01Name,ilRc);
			}
			
			if (ilRc == RC_SUCCESS)
			{
				dbg(DEBUG,"InitRsrhdl: SetArrayInfo ODATAB OK");
				dbg(DEBUG,"<%s> %d",rgOdaArray.crArrayName, rgOdaArray.rrArrayHandle);
				ilRc = ConfigureAction("ODATAB", 0, "URT,IRT,DRT" );
				if(ilRc != RC_SUCCESS)
				{
					dbg(TRACE,"InitRsrhdl: ConfigureAction failed <%d>",ilRc);
				}
			}
		}
	}

	/* Check fieldlist for PFC */
	if (ilRc == RC_SUCCESS)
	{
		strcpy(cgPfcFields,PFC_FIELDS);
		sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);

		dbg(TRACE,"InitRsrhdl: SetArrayInfo PFCTAB array");
		ilRc = SetArrayInfo("PFCTAB","PFCTAB",cgPfcFields,NULL,NULL,&rgPfcArray,pclSelection,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitRsrhdl: SetArrayInfo PFCTAB failed <%d>",ilRc);
		}
		else
		{
			strcpy(rgPfcArray.crIdx01Name,"URNO");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgPfcArray.rrArrayHandle,
												rgPfcArray.crArrayName,
												&rgPfcArray.rrIdx01Handle,
												rgPfcArray.crIdx01Name,"URNO");
			if (ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"Index <%s> not created RC=%d", rgPfcArray.crIdx01Name,ilRc);
			}

			if (ilRc == RC_SUCCESS)
			{
				strcpy(rgPfcArray.crIdx02Name,"FCTC");
				ilRc = CEDAArrayCreateSimpleIndexUp(&rgPfcArray.rrArrayHandle,
													rgPfcArray.crArrayName,
													&rgPfcArray.rrIdx02Handle,
													rgPfcArray.crIdx02Name,"FCTC");
				if (ilRc != RC_SUCCESS)
				{
					dbg(TRACE,"Index <%s> not created RC=%d", rgPfcArray.crIdx02Name,ilRc);
				}
			
				if (ilRc == RC_SUCCESS)
				{
					dbg(DEBUG,"InitRsrhdl: SetArrayInfo PFCTAB OK");
					dbg(DEBUG,"<%s> %d",rgPfcArray.crArrayName, rgPfcArray.rrArrayHandle);
					ilRc = ConfigureAction("PFCTAB", 0, "URT,IRT,DRT" );
					if(ilRc != RC_SUCCESS)
					{
						dbg(TRACE,"InitRsrhdl: ConfigureAction failed <%d>",ilRc);
					}
				}
			}
		}
	}

	/* Check fieldlist for SCO */
	if (ilRc == RC_SUCCESS)
	{
		strcpy(cgScoFields,SCO_FIELDS);
		sprintf(pclSelection,"WHERE HOPO = '%s' order by SURN,VPFR",cgHopo);

		dbg(TRACE,"InitRsrhdl: SetArrayInfo SCOTAB array");
		ilRc = SetArrayInfo("SCOTAB","SCOTAB",cgScoFields,NULL,NULL,&rgScoArray,pclSelection,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitRsrhdl: SetArrayInfo SCOTAB failed <%d>",ilRc);
		}
		else
		{
			strcpy(rgScoArray.crIdx01Name,"SURN");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgScoArray.rrArrayHandle,
												rgScoArray.crArrayName,
												&rgScoArray.rrIdx01Handle,
												rgScoArray.crIdx01Name,"SURN");
			if (ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"Index <%s> not created RC=%d", rgScoArray.crIdx01Name,ilRc);
			}
			
			if (ilRc == RC_SUCCESS)
			{
				dbg(DEBUG,"InitRsrhdl: SetArrayInfo SCOTAB OK");
				dbg(DEBUG,"<%s> %d",rgScoArray.crArrayName, rgScoArray.rrArrayHandle);
				ilRc = ConfigureAction("SCOTAB", 0, "URT,IRT,DRT" );
				if(ilRc != RC_SUCCESS)
				{
					dbg(TRACE,"InitRsrhdl: ConfigureAction failed <%d>",ilRc);
				}
			}
		}
	}


	/* Check fieldlist for DRR */
	if (ilRc == RC_SUCCESS)
	{
		strcpy(cgDrrFields,DRR_FIELDS);
		sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);

		dbg(TRACE,"InitRsrhdl: SetArrayInfo STFTAB array");
		ilRc = SetArrayInfo("DRRTAB","DRRTAB",cgDrrFields,NULL,NULL,&rgDrrArray,pclSelection,FALSE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitRsrhdl: SetArrayInfo DRRTAB failed <%d>",ilRc);
		}
		else
		{
			strcpy(rgDrrArray.crIdx01Name,"STFU");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgDrrArray.rrArrayHandle,
												rgDrrArray.crArrayName,
												&rgDrrArray.rrIdx01Handle,
												rgDrrArray.crIdx01Name,"STFU,SDAY,DRRN");
			if (ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"Index <%s> not created RC=%d", rgDrrArray.crIdx01Name,ilRc);
			}
			
			if (ilRc == RC_SUCCESS)
			{
				dbg(DEBUG,"InitRsrhdl: SetArrayInfo STFTAB OK");
				dbg(DEBUG,"<%s> %d",rgDrrArray.crArrayName, rgDrrArray.rrArrayHandle);
				strcpy(rgDrrArray.crIdx01FieldList,"STFU,SDAY,DRRN");
			}
		}
	}


	
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CEDAArrayReadAllHeader(outp);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitRsrhdl: CEDAArrayReadAllHeader failed <%d>",ilRc);
		}else{
			dbg(TRACE,"InitRsrhdl: CEDAArrayReadAllHeader OK");
		}/* end of if */
	}/* end of if */


	if(ilRc == RC_SUCCESS)
	{
		ilRc = InitFieldIndex();
	}
	if(ilRc == RC_SUCCESS)
	{
		igInitOK = TRUE;
		dbg(TRACE,"InitRsrhdl: InitFieldIndex OK");
	}/* end of if */
	else
	{
		dbg(TRACE,"InitRsrhdl: InitFieldIndex failed");
	}

	return(ilRc);
	
}
 /* end of initialize */

/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
    int    ilRc = RC_SUCCESS;                /* Return code */
    
    dbg(TRACE,"Reset: now resetting");
    
    return ilRc;
    
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
	/* unset SIGCHLD ! DB-Child will terminate ! */

	dbg(TRACE,"Terminate: now DB logoff ...");

	signal(SIGCHLD,SIG_IGN);

	logoff();

	dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);


	dbg(TRACE,"Terminate: now leaving ...");

	fclose(outp);

	sleep(ipSleep < 1 ? 1 : ipSleep);
	
	exit(0);
	
}


/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int pipSig)
{
    int    ilRc = RC_SUCCESS;            /* Return code */
    dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);
    switch(pipSig)
    {
    default    :
        Terminate(1);
        break;
    } /* end of switch */
    exit(0);
    
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
    int    ilRc = RC_SUCCESS;
    return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    int    ilRc = RC_SUCCESS;
    
    switch(pipErr) {
    case    QUE_E_FUNC    :    /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :    /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;
    case    QUE_E_SEND    :    /* Error using msgsnd */
        dbg(TRACE,"<%d> : msgsnd failed",pipErr);
        break;
    case    QUE_E_GET    :    /* Error using msgrcv */
        dbg(TRACE,"<%d> : msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX    :
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE    :
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES    :
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        break;
    case    QUE_E_RESP    :    /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;
    case    QUE_E_FULL    :
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG    :    /* No message on queue */
        dbg(TRACE,"<%d> : no messages on queue",pipErr);
        break;
    case    QUE_E_INVORG    :    /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :    /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;
    default            :    /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
    } /* end switch */
         
    return;
} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
    int    ilRc = RC_SUCCESS;            /* Return code */
    int    ilBreakOut = FALSE;
    
    do
    {
        ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text;    
        if( ilRc == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRc != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRc);
            } /* fi */
        
            switch( prgEvent->command )
            {
            case    HSB_STANDBY    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_COMING_UP    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_ACTIVE    :
                ctrl_sta = prgEvent->command;
                ilBreakOut = TRUE;
                break;    
            case    HSB_ACT_TO_SBY    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate(1);
                break;    
    
            case    HSB_STANDALONE    :
                ctrl_sta = prgEvent->command;

                ilBreakOut = TRUE;
                break;    

            case    SHUTDOWN    :
                Terminate(1);
                break;
                        
            case    RESET        :
                ilRc = Reset();
                break;
                        
            case    EVENT_DATA    :
                dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
                    
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;
            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;
            default            :
                dbg(TRACE,"HandleQueues: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        } else {
            /* Handle queuing errors */
            HandleQueErr(ilRc);
        } /* end else */
    } while (ilBreakOut == FALSE);
    if(igInitOK == FALSE)
    {
            ilRc = InitRsrhdl();
            if(ilRc != RC_SUCCESS)
            {
                dbg(TRACE,"InitRsrhdl: init failed!");
            } /* end of if */
    }/* end of if */
    /* OpenConnection(); */
} /* end of HandleQueues */
    

static int GetCommand(char *pcpCommand, int *pipCmd)
{
	int	ilRc = RC_SUCCESS;			/* Return code */
	int ilLoop = 0;
	char clCommand[48];
	
	memset(&clCommand[0],0x00,8);

	ilRc = get_real_item(&clCommand[0],pcpCommand,1);
	if(ilRc > 0)
	{
		ilRc = RC_FAIL;
	}/* end of if */

	while((ilRc != 0) && (prgCmdTxt[ilLoop] != NULL))
	{
		ilRc = strcmp(&clCommand[0],prgCmdTxt[ilLoop]);
		ilLoop++;
	}/* end of while */

	if(ilRc == 0)
	{
		ilLoop--;
		dbg(TRACE,"GetCommand: <%s> <%d>",prgCmdTxt[ilLoop],rgCmdDef[ilLoop]);
		*pipCmd = rgCmdDef[ilLoop];
	}else{
		dbg(TRACE,"GetCommand: <%s> is not valid",&clCommand[0]);
		ilRc = RC_FAIL;
	}/* end of if */

	return(ilRc);
	
}

static int AddToUrnoList(char *pcpUrno,char *pcpList,long lpListLength,long *plpActualLength)
{
	int ilRc = RC_SUCCESS;
	
  int ilNewLen;

	ilNewLen = strlen(pcpUrno) + 1;
	if (((*plpActualLength) + ilNewLen) > (lpListLength-50))
	{
		char *pclTmp;
		pclTmp = realloc(pcpList,lpListLength + 10000);
		if (pclTmp != NULL)
		{
			pcpList = pclTmp;
			lpListLength += 10000;
			if (*plpActualLength > 100000)
			{
				memmove(pcpList,pcpList+10000,10000);
				plpActualLength -= 10000;
			}
		}
		else
		{
			dbg(TRACE,"unable to reallocate %ld bytes for Urnolist",
					lpListLength+50000);
			Terminate(30);
		}
	}

	if (*pcpList != '\0')
	{
		sprintf(&pcpList[*plpActualLength],",%s",pcpUrno);
		*plpActualLength += ilNewLen;
	}
	else
	{
		sprintf(&pcpList[*plpActualLength],"%s",pcpUrno);
		*plpActualLength += ilNewLen-1;
	}

	dbg(DEBUG,"AddToUrnoList: <%s>",pcpList);

	return ilRc;
}

static int ExtractItem(int ilItemNo, char* pcpSource,char* pcpTarget, char cpDelim,int ilMaxLen)
{
	/* delimiter (= item) counter */
	int ilCountDelim = 1;
	/* length of item */
	int ilLength = 0;
	/* char counter */
	int ilPos;

	for (ilPos = 0; ilPos < strlen(pcpSource); ilPos += 1)
	{
		/* this char = delimiter ? */
		if (*(pcpSource+ilPos) == cpDelim)
		{
			/* start of next item in string */
			ilCountDelim += 1;
		}
		/* in scope of item? */
		else if (ilCountDelim == ilItemNo)
		{
			/* max. length reached? */
			/* MCU 20050524: should be > instead of >= */
			if (ilLength > ilMaxLen)
			{
				/* yes -> can't copy byte, print error and terminate */
				dbg(DEBUG,"ExtractItem: items exceeds max. length of <%d>!!!",ilMaxLen);
				return -1;
			}
			/* copy next byte from source */
			*(pcpTarget+ilLength) = *(pcpSource+ilPos);
			ilLength += 1;
			/* zero-terminate target buffer */
			*(pcpTarget+ilLength) = '\0';
		}
		else if (ilCountDelim > ilItemNo)
		{
			/* next item reached -> break loop */
			break;
		}
	}

	/* if we get here and the item number is greater than the counter, 
	   there are fewer items in the string than expected */
	if (ilCountDelim < ilItemNo)
	{
		/* dump error info and terminate with error */
		dbg(DEBUG,"ExtractItem: fewer items in string than expected (<%d> items found in '%s')",ilCountDelim,pcpSource);
		return -2;
	}

	/* everything is ok -> return size of item */
	return ilLength;
}

 
/******************************************************************************/
/* Fill ReleaseData Struct                                                        */
/******************************************************************************/
static int FillReleaseData(char *pcpData,RELEASEDATA *popRelData)
{
    int ilRc			= RC_SUCCESS;		/* Return code */
    char clDel			= '|';				/* item delimiter in data string */
    long llDataLen		= 0;				/* length of urno list */
	int ilItemLen		= 0;				/* length of single item (like USEC) in data string */
	int ilTotalItemLen	= 0;				/* length of all params in data string without urno list */
	char clStfUrno[12];
	char clTmp[16];	
	char clMode[16];
		
	/****Trace pcpData and fill Struct****/
    dbg(DEBUG,"FillReleaseData: START-->");

	/* extract release parameters from data string and store them in <rgRelData> */
	/* format of data string is supposed to be '<USEC>|<DORS>|<HOPO>|<ROSL>|<RLFR-RLTO>|<... STFU-list>' */
	/* get USEC, max. size = 10 */
	if ((ilItemLen = ExtractItem(1,pcpData,popRelData->cUsec,clDel,10)) < 0)
	{
		/* error extracting USEC from data string -> terminate function */
		return RC_FAILURE;
	}
	ilTotalItemLen += ilItemLen + 1;
	
	/* get HOPO, max. size = 3 */
	if ((ilItemLen = ExtractItem(1,pcpData+ilTotalItemLen,popRelData->cHopo,clDel,3)) < 0)
	{
		/* error extracting HOPO from data string -> terminate function */
		return RC_FAILURE;
	}
	ilTotalItemLen += ilItemLen + 1;

	/* get mode	*/
	if ((ilItemLen = ExtractItem(1,pcpData+ilTotalItemLen,clMode,clDel,1)) < 0)
	{
		/* error extracting HOPO from data string -> terminate function */
		return RC_FAILURE;
	}
	else
	{
		popRelData->bDelete = strcmp(clMode,"D") == 0 ? 1 : 0;		
	}

	ilTotalItemLen += ilItemLen + 1;
	

	/* get RLFR-RLTO, max. size = 17 */
	if ((ilItemLen = ExtractItem(1,pcpData+ilTotalItemLen,popRelData->cDateFrTo,clDel,17)) < 0)
	{
		/* error extracting RLFR-RLTO from data string -> terminate function */
		return RC_FAILURE;
	}
	ilTotalItemLen += ilItemLen + 1;

	/* get SPL, max. size = 10 */
	if ((ilItemLen = ExtractItem(1,pcpData+ilTotalItemLen,popRelData->cShiftPlan,clDel,10)) < 0)
	{
		/* error extracting Shift plan urno from data string -> terminate function */
		return RC_FAILURE;
	}
	ilTotalItemLen += ilItemLen + 1;

	/* copy urno list */
	llDataLen = strlen(pcpData) - ilTotalItemLen;
	if (llDataLen >= sizeof(popRelData->pcUrnoList))
	{
		/* urno list exceeds limit -> terminate function */
		return RC_FAILURE;
	}
	
	strncpy(popRelData->pcUrnoList,pcpData+ilTotalItemLen,llDataLen);
	popRelData->pcUrnoList[llDataLen] = '\0';

	
	popRelData->pcUrnoList2[0] = '\0';
	while ((ilItemLen = ExtractItem(1,pcpData+ilTotalItemLen,clStfUrno,'-',10)) > 0)
	{
		if (popRelData->pcUrnoList2[0] == '\0')
		{
			sprintf(clTmp,"'%s'",clStfUrno);
		}
		else
		{
			sprintf(clTmp,",'%s'",clStfUrno);
		}
	
		strcat(popRelData->pcUrnoList2,clTmp);						
		
		ilTotalItemLen += ilItemLen + 1;
	}
	
	dbg(DEBUG,"FillReleaseData: <-- ENDE ");

	return (ilRc);
}

/*	original code starts here	*/
static char*	rtrim(char *pcpString)
{
	int ilCount;
	for (ilCount = strlen(pcpString) - 1; ilCount >= 0; ilCount--)
	{
		if (pcpString[ilCount] == ' ')
			pcpString[ilCount] = '\0';
	}
	return pcpString;
}

/******************************************************************************/
/* getFreeUrno                                                                */
/*                                                                            */
/* Maintains a pool of URNOS. If no more URNOS are available, the pool is     */
/* refilled with a set of URNO_POOL_SIZE.                                     */
/*                                                                            */
/* Input:                                                                     */
/*       Nothing                                                              */
/*                                                                            */
/* Return:                                                                    */
/*       cpUrno -- the buffer pointed to is filled with a new URNO            */
/*       RC_SUCCESS/RC_FAIL                                                   */
/*                                                                            */
/******************************************************************************/
static int GetFreeUrno(char *cpUrno)
{
	static uint ilNumUrnos = 0;
  	static uint ilFirstUrno = 0;
  	static uint ilNextUrno = 0;
  	char clNewUrno[URNO_SIZE];
  	int ilRc;

	if (ilNumUrnos == 0) 
	{
    	ilRc = GetNextValues (clNewUrno, URNO_POOL_SIZE);
    	if (ilRc == RC_SUCCESS) 
    	{
      		ilFirstUrno = atoi(clNewUrno);
      		ilNextUrno = ilFirstUrno;
      		ilNumUrnos = 100;
    	}
    	else 
    		return RC_FAIL;
  	}
  	
  	sprintf(cpUrno, "%d", ilNextUrno);
  	ilNextUrno++;
  	ilNumUrnos--;
  	dbg(DEBUG,"<getFreeUrno> URNO First:<%d> Next:<%d> Num:<%d>",ilFirstUrno, ilNextUrno, ilNumUrnos);
  	return RC_SUCCESS;
}

static int CreateDrrData(char *pcpStfUrno,char *pcpDrrDay,char *opRosl,char *opRoss,char *opDrrn,long *lpDrrRowNum,char **pcpDrrRowBuf)
{
	/* create new drr record	*/
	char pclData[2048];
	char clNewURNO[URNO_SIZE];
	char clTimeStamp[32];
	
	long llRowNum = -1;
	int ilRC = RC_SUCCESS;

    dbg(DEBUG,"CreateDrrData at line %d: START-->",__LINE__);
	
	memset(pclData,0x00,2048);
	
	ilRC = CEDAArrayAddRow(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,&llRowNum,(void *)pclData);
	if (ilRC != RC_SUCCESS)
	{
		/* error */
	    dbg(TRACE,"CreateDrrData:CEDAArrayAddRow for <%s> at line %d failed!",rgDrrArray.crArrayName,__LINE__);
		return -3;
	}
	
	*lpDrrRowNum = llRowNum;
	
	GetFreeUrno(clNewURNO);

	/* change urno  */
	if ((ilRC = CEDAArrayPutField(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,NULL,"URNO",llRowNum,clNewURNO)) != RC_SUCCESS)
	{
		/* error */
		dbg(TRACE,"CreateDrrData: CEDAArrayPutField for <%s> failed at line %d",rgDrrArray.crArrayName,__LINE__);
		return -4;
	}

	GetServerTimeStamp("UTC",1,0,clTimeStamp);
	
	/* change creation date */
	if ((ilRC = CEDAArrayPutField(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,NULL,"CDAT",llRowNum,clTimeStamp)) != RC_SUCCESS)
	{
		/* error */
	    dbg(TRACE,"CreateDrrData: CEDAArrayPutField for <%s> failed at line %d!",rgDrrArray.crArrayName,__LINE__);
		return -4;
	}

	if ((ilRC = CEDAArrayPutField(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,NULL,"USEC",llRowNum,cgUser)) != RC_SUCCESS)
	{
	    dbg(TRACE,"CreateDrrData: CEDAArrayPutField for <%s> at line %d failed!",rgDrrArray.crArrayName,__LINE__);
		return -4;
	}

	if ((ilRC = CEDAArrayPutField(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,NULL,"STFU",llRowNum,pcpStfUrno)) != RC_SUCCESS)
	{
	    dbg(TRACE,"CreateDrrData: CEDAArrayPutField for <%s> at line %d failed!",rgDrrArray.crArrayName,__LINE__);
		return -4;
	}

	if ((ilRC = CEDAArrayPutField(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,NULL,"SDAY",llRowNum,pcpDrrDay)) != RC_SUCCESS)
	{
	    dbg(TRACE,"CreateDrrData: CEDAArrayPutField for <%s> at line %d failed!",rgDrrArray.crArrayName,__LINE__);
		return -4;
	}
	
	if ((ilRC = CEDAArrayPutField(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,NULL,"ROSL",llRowNum,opRosl)) != RC_SUCCESS)
	{
	    dbg(TRACE,"CreateDrrData: CEDAArrayPutField for <%s> at line %d failed!",rgDrrArray.crArrayName,__LINE__);
		return -4;
	}

	if ((ilRC = CEDAArrayPutField(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,NULL,"ROSS",llRowNum,opRoss)) != RC_SUCCESS)
	{
	    dbg(TRACE,"CreateDrrData: CEDAArrayPutField for <%s> at line %d failed!",rgDrrArray.crArrayName,__LINE__);
		return -4;
	}

	if ((ilRC = CEDAArrayPutField(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,NULL,"DRRN",llRowNum,opDrrn)) != RC_SUCCESS)
	{
	    dbg(TRACE,"CreateDrrData: CEDAArrayPutField for <%s> at line %d failed!",rgDrrArray.crArrayName,__LINE__);
		return -4;
	}

	if ((ilRC = CEDAArrayPutField(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,NULL,"DRS2",llRowNum,"1")) != RC_SUCCESS)
	{
	    dbg(TRACE,"CreateDrrData: CEDAArrayPutField for <%s> at line %d failed!",rgDrrArray.crArrayName,__LINE__);
		return -4;
	}

	if ((ilRC = CEDAArrayPutField(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,NULL,"PRFL",llRowNum,"1")) != RC_SUCCESS)
	{
	    dbg(TRACE,"CreateDrrData: CEDAArrayPutField for <%s> at line %d failed!",rgDrrArray.crArrayName,__LINE__);
		return -4;
	}

	ilRC = CEDAArrayGetRowPointer(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,llRowNum,(void *)pcpDrrRowBuf);
	if (ilRC != RC_SUCCESS)
	{
	    dbg(TRACE,"CreateDrrData: CEDAArrayGetRowPointer for <%s> at line %d failed with ec = %d!",rgDrrArray.crArrayName,__LINE__,ilRC);
	}

    dbg(DEBUG,"CreateDrrData at line %d: <--END",__LINE__);

	return ilRC;
}

static time_t	AddOffsetToTime(time_t pTime,long pDays,long pHours,long pMinutes,long pSeconds)
{
	pTime += pDays * 24 * 60 * 60;
	pTime +=     pHours	* 60 * 60;
	pTime +=	    pMinutes * 60;
	pTime +=		pSeconds;		
	return pTime;													
}

static int HourMinStringToTimeSpan(char *opHourMin,long *lpTimeSpan)
{
	char clHourMin[256];
	char clHour[4];
	char clMinute[4];
	char clSecond[4];
	int	 ilStrLen;
	
    dbg(DEBUG,"HourMinStringToTimeSpan at line %d: START-->",__LINE__);
	
	*lpTimeSpan = 0;
	
	strcpy(clHourMin,opHourMin);
	rtrim(clHourMin);
	
	ilStrLen = strlen(clHourMin);
	
	if (ilStrLen != 4 && ilStrLen != 6)
	{
	    dbg(DEBUG,"HourMinStringToTimeSpan at line %d ASSERT(%s) failed",__LINE__,"ilStrLen == 4 || ilStrLen == 6");
		return 0;
	}
		
	strncpy(clHour,clHourMin,2);
	clHour[2] = '\0';
	if (atoi(clHour) == 24)
	{
		*lpTimeSpan = 23 * 60 * 60 + 59 * 60 + 59;		
	}
	else
	{
		*lpTimeSpan = atoi(clHour) * 60 * 60;
		strncpy(clMinute,&clHourMin[2],2);
		clMinute[2] = '\0';

		*lpTimeSpan += atoi(clMinute) * 60;
		
		if (ilStrLen == 6)
		{
			strncpy(clSecond,&clHourMin[4],2);
			clSecond[2] = '\0';
			*lpTimeSpan += atoi(clSecond);
		}
	}

    dbg(DEBUG,"HourMinStringToTimeSpan at line %d: <--END",__LINE__);

	return 1;						
}

static long GetCommonDayWorkingTime(char *pcpStaffUrno,time_t olSday)
{
	char clVpfr[32];
	char clVpto[32];

	char *pclScoRowBuf = NULL;
		
	long llScoRowNum;
	int	 ilRC;
	
	time_t	olVpfr;
	time_t	olVpto;

    dbg(DEBUG,"GetCommonDayWorkingTime at line %d: START-->",__LINE__);
	
	llScoRowNum = ARR_FIRST;
	ilRC = CEDAArrayFindRowPointer(&(rgScoArray.rrArrayHandle),&(rgScoArray.crArrayName[0]),&(rgScoArray.rrIdx01Handle),&(rgScoArray.crIdx01Name[0]),pcpStaffUrno,&llScoRowNum,(void *) &pclScoRowBuf);
	while (ilRC == RC_SUCCESS)
	{
		strcpy(clVpfr,FIELDVAL(rgScoArray,pclScoRowBuf,igScoVPFR));
		strcpy(clVpto,FIELDVAL(rgScoArray,pclScoRowBuf,igScoVPTO));

	    dbg(DEBUG,"GetCommonDayWorkingTime at line %d: Vpfr = <%s>,Vpto = <%s>",__LINE__,clVpfr,clVpto);
			
		if (StrToTime(0,clVpfr,&olVpfr) == RC_SUCCESS)
		{
			if (olVpfr <= olSday)
			{
				if (StrToTime(0,clVpto,&olVpto) == RC_SUCCESS)
				{
					if (olSday < olVpto)
					{
						long olTimeSpan = 0;
						if (HourMinStringToTimeSpan(FIELDVAL(rgScoArray,pclScoRowBuf,igScoCWEH),&olTimeSpan) == RC_SUCCESS)
						{
							return olTimeSpan;
						}
						else
						{
							return 0;
						}					
					}					
				}
				else
				{
					long olTimeSpan = 0;
					if (HourMinStringToTimeSpan(FIELDVAL(rgScoArray,pclScoRowBuf,igScoCWEH),&olTimeSpan) == RC_SUCCESS)
					{
						return olTimeSpan;
					}
					else
					{
						return 0;
					}					
				}
			}
		}
										
		llScoRowNum = ARR_NEXT;
		ilRC = CEDAArrayFindRowPointer(&(rgScoArray.rrArrayHandle),&(rgScoArray.crArrayName[0]),&(rgScoArray.rrIdx01Handle),&(rgScoArray.crIdx01Name[0]),pcpStaffUrno,&llScoRowNum,(void *) &pclScoRowBuf);
	}
	
    dbg(DEBUG,"GetCommonDayWorkingTime at line %d: <--END",__LINE__);
	
	return 0;			
}

static int SetDrrDataScod(long llRowNum,int bpCodeIsBsd,char *pcpNewShiftUrno,char *opNewFctc,int bpSetEditFlag,int bpCheckMainFuncOnly)
{
	char clAvfr[32];
	char clAvto[32];
	char clSbfr[32];
	char clSbto[32];
	char clSday[32];
	char clSblu[32];	
	
	char *pclDrrRowBuf = NULL;
	char *pclBsdRowBuf = NULL;
	char *pclOdaRowBuf = NULL;
	
	long llBsdRowNum;
	long llOdaRowNum;
	
	int ilRC = RC_FAILURE;

	time_t	olSday;
	time_t	olAvfr;
	time_t	olAvto;
	time_t	olSbfr;
	time_t	olSbto;
	
	long	olEsbg;
	long	olLsen;
	long	olBkf1;
	long	olBkt1;
	long	olAbfr;
	long	olAbto;

    dbg(DEBUG,"SetDrrDataScod at line %d: START-->",__LINE__);
	
	ilRC = CEDAArrayGetRowPointer(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,llRowNum,(void *)&pclDrrRowBuf);       
	if (ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"SetDrrDataScod : ASSERT(%s) failed at line %d!","CEDAArrayGetRowPointer == RC_SUCCESS",__LINE__);
		return ilRC;
	}

	strcpy(clSday,FIELDVAL(rgDrrArray,pclDrrRowBuf,igDrrSDAY));
	strcat(clSday,"000000");
	StrToTime(0,clSday,&olSday);
	
	if (bpCodeIsBsd)
	{
		llBsdRowNum = ARR_FIRST;
		ilRC = CEDAArrayFindRowPointer(&(rgBsdArray.rrArrayHandle),&(rgBsdArray.crArrayName[0]),&(rgBsdArray.rrIdx01Handle),&(rgBsdArray.crIdx01Name[0]),pcpNewShiftUrno,&llBsdRowNum,(void *) &pclBsdRowBuf);
		if (ilRC == RC_SUCCESS)
		{
			if (strcmp(FIELDVAL(rgDrrArray,pclDrrRowBuf,igDrrSCOD),FIELDVAL(rgBsdArray,pclBsdRowBuf,igBsdBSDC)) != 0)
			{
				if ((ilRC = CEDAArrayPutField(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,NULL,"PRFL",llRowNum,"1")) != RC_SUCCESS)
				{
				    dbg(TRACE,"SetDrrDataScod: CEDAArrayPutField for <%s> at line %d failed!",rgDrrArray.crArrayName,__LINE__);
					return -4;
				}
			}
			
			if ((ilRC = CEDAArrayPutField(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,NULL,"SCOO",llRowNum,FIELDVAL(rgDrrArray,pclDrrRowBuf,igDrrSCOD))) != RC_SUCCESS)
			{
			    dbg(TRACE,"SetDrrDataScod: CEDAArrayPutField for <%s> at line %d failed!",rgDrrArray.crArrayName,__LINE__);
				return -4;
			}
			
			if ((ilRC = CEDAArrayPutField(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,NULL,"SCOD",llRowNum,FIELDVAL(rgBsdArray,pclBsdRowBuf,igBsdBSDC))) != RC_SUCCESS)
			{
			    dbg(TRACE,"SetDrrDataScod: CEDAArrayPutField for <%s> at line %d failed!",rgDrrArray.crArrayName,__LINE__);
				return -4;
			}

			if (strlen(opNewFctc) == 0)
			{
				if ((ilRC = CEDAArrayPutField(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,NULL,"FCTC",llRowNum,FIELDVAL(rgBsdArray,pclBsdRowBuf,igBsdFCTC))) != RC_SUCCESS)
				{
				    dbg(TRACE,"SetDrrDataScod: CEDAArrayPutField for <%s> at line %d failed!",rgDrrArray.crArrayName,__LINE__);
					return -4;
				}
			}	
			else
			{
				if ((ilRC = CEDAArrayPutField(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,NULL,"FCTC",llRowNum,opNewFctc)) != RC_SUCCESS)
				{
				    dbg(TRACE,"SetDrrDataScod: CEDAArrayPutField for <%s> at line %d failed!",rgDrrArray.crArrayName,__LINE__);
					return -4;
				}
			}		
			
			if ((ilRC = CEDAArrayPutField(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,NULL,"BSDU",llRowNum,pcpNewShiftUrno)) != RC_SUCCESS)
			{
			    dbg(TRACE,"SetDrrDataScod: CEDAArrayPutField for <%s> at line %d failed!",rgDrrArray.crArrayName,__LINE__);
				return -4;
			}
			
			if (HourMinStringToTimeSpan(FIELDVAL(rgBsdArray,pclBsdRowBuf,igBsdESBG),&olEsbg) == 0)
			{
			    dbg(TRACE,"SetDrrDataScod: HourMinStringToTimeSpan for <%s> at line %d failed!",rgBsdArray.crArrayName,__LINE__);
				return -5;
			}
		
			olAvfr = olSday + olEsbg;
			TimeToStr(0,clAvfr,olAvfr);
			
			if ((ilRC = CEDAArrayPutField(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,NULL,"AVFR",llRowNum,clAvfr)) != RC_SUCCESS)
			{
			    dbg(TRACE,"SetDrrDataScod: CEDAArrayPutField for <%s> at line %d failed!",rgDrrArray.crArrayName,__LINE__);
				return -4;
			}
			
			if (HourMinStringToTimeSpan(FIELDVAL(rgBsdArray,pclBsdRowBuf,igBsdLSEN),&olLsen) == 0)
			{
			    dbg(TRACE,"SetDrrDataScod: HourMinStringToTimeSpan for <%s> at line %d failed!",rgBsdArray.crArrayName,__LINE__);
				return -5;
			}
			
			if (olLsen <= olEsbg)
			{
				/* add one day	*/				
				olLsen += 24 * 60 * 60;
			}

			olAvto = olSday + olLsen;
			TimeToStr(0,clAvto,olAvto);
			
			if ((ilRC = CEDAArrayPutField(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,NULL,"AVTO",llRowNum,clAvto)) != RC_SUCCESS)
			{
			    dbg(TRACE,"SetDrrDataScod: CEDAArrayPutField for <%s> at line %d failed!",rgDrrArray.crArrayName,__LINE__);
				return -4;
			}

			strcpy(clSblu,FIELDVAL(rgBsdArray,pclBsdRowBuf,igBsdBKD1));
			
			if ((ilRC = CEDAArrayPutField(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,NULL,"SBLU",llRowNum,clSblu)) != RC_SUCCESS)
			{
			    dbg(TRACE,"SetDrrDataScod: CEDAArrayPutField for <%s> at line %d failed!",rgDrrArray.crArrayName,__LINE__);
				return -4;
			}

			if (HourMinStringToTimeSpan(FIELDVAL(rgBsdArray,pclBsdRowBuf,igBsdBKF1),&olBkf1) == 0)
			{
			    dbg(TRACE,"SetDrrDataScod: HourMinStringToTimeSpan for <%s> at line %d failed!",rgBsdArray.crArrayName,__LINE__);
				return -5;
			}
			
			if (strcmp(FIELDVAL(rgBsdArray,pclBsdRowBuf,igBsdBKF1),FIELDVAL(rgBsdArray,pclBsdRowBuf,igBsdESBG)) < 0)
			{
				olBkf1 += 24 * 60 * 60;
			}

			olSbfr = olSday + olBkf1;
			TimeToStr(0,clSbfr,olSbfr);
		
			if ((ilRC = CEDAArrayPutField(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,NULL,"SBFR",llRowNum,clSbfr)) != RC_SUCCESS)
			{
			    dbg(TRACE,"SetDrrDataScod: CEDAArrayPutField for <%s> at line %d failed!",rgDrrArray.crArrayName,__LINE__);
				return -4;
			}
			
			if (HourMinStringToTimeSpan(FIELDVAL(rgBsdArray,pclBsdRowBuf,igBsdBKT1),&olBkt1) == 0)
			{
			    dbg(TRACE,"SetDrrDataScod: HourMinStringToTimeSpan for <%s> at line %d failed!",rgBsdArray.crArrayName,__LINE__);
				return -5;
			}
			
			if (strcmp(FIELDVAL(rgBsdArray,pclBsdRowBuf,igBsdBKT1),FIELDVAL(rgBsdArray,pclBsdRowBuf,igBsdBKF1)) < 0 || strcmp(FIELDVAL(rgBsdArray,pclBsdRowBuf,igBsdBKT1),FIELDVAL(rgBsdArray,pclBsdRowBuf,igBsdESBG)) < 0) 
			{
				olBkt1 += 24 * 60 * 60;
			}
		
			olSbto = olSday + olBkt1;
			TimeToStr(0,clSbto,olSbto);
		
			if ((ilRC = CEDAArrayPutField(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,NULL,"SBTO",llRowNum,clSbto)) != RC_SUCCESS)
			{
			    dbg(TRACE,"SetDrrDataScod: CEDAArrayPutField for <%s> at line %d failed!",rgDrrArray.crArrayName,__LINE__);
				return -4;
			}
			
			
		}
				
	}
	else
	{
		llOdaRowNum = ARR_FIRST;
		ilRC = CEDAArrayFindRowPointer(&(rgOdaArray.rrArrayHandle),&(rgOdaArray.crArrayName[0]),&(rgOdaArray.rrIdx01Handle),&(rgOdaArray.crIdx01Name[0]),pcpNewShiftUrno,&llOdaRowNum,(void *) &pclOdaRowBuf);
		if (ilRC == RC_SUCCESS)
		{
			if (strcmp(FIELDVAL(rgDrrArray,pclDrrRowBuf,igDrrSCOD),FIELDVAL(rgOdaArray,pclOdaRowBuf,igOdaSDAC)) != 0)
			{
				if ((ilRC = CEDAArrayPutField(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,NULL,"PRFL",llRowNum,"1")) != RC_SUCCESS)
				{
				    dbg(TRACE,"SetDrrDataScod: CEDAArrayPutField for <%s> at line %d failed!",rgDrrArray.crArrayName,__LINE__);
					return -4;
				}
			}
		
			if ((ilRC = CEDAArrayPutField(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,NULL,"SCOO",llRowNum,FIELDVAL(rgDrrArray,pclDrrRowBuf,igDrrSCOD))) != RC_SUCCESS)
			{
			    dbg(TRACE,"SetDrrDataScod: CEDAArrayPutField for <%s> at line %d failed!",rgDrrArray.crArrayName,__LINE__);
				return -4;
			}
			
			if ((ilRC = CEDAArrayPutField(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,NULL,"SCOD",llRowNum,FIELDVAL(rgOdaArray,pclOdaRowBuf,igOdaSDAC))) != RC_SUCCESS)
			{
			    dbg(TRACE,"SetDrrDataScod: CEDAArrayPutField for <%s> at line %d failed!",rgDrrArray.crArrayName,__LINE__);
				return -4;
			}

			if ((ilRC = CEDAArrayPutField(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,NULL,"BSDU",llRowNum,pcpNewShiftUrno)) != RC_SUCCESS)
			{
			    dbg(TRACE,"SetDrrDataScod: CEDAArrayPutField for <%s> at line %d failed!",rgDrrArray.crArrayName,__LINE__);
				return -4;
			}
						
			if ((ilRC = CEDAArrayPutField(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,NULL,"FCTC",llRowNum," ")) != RC_SUCCESS)
			{
			    dbg(TRACE,"SetDrrDataScod: CEDAArrayPutField for <%s> at line %d failed!",rgDrrArray.crArrayName,__LINE__);
				return -4;
			}

			clSbfr[0] = '\0';
			clSbto[0] = '\0';

			if (HourMinStringToTimeSpan(FIELDVAL(rgOdaArray,pclOdaRowBuf,igOdaABFR),&olAbfr) == 1 && HourMinStringToTimeSpan(FIELDVAL(rgOdaArray,pclOdaRowBuf,igOdaABTO),&olAbto) == 1)
			{
				olAvfr = olSday + olAbfr;
				olAvto = olSday + olAbto;
				
				if (olAvto <= olAvfr)				
				{
					olAvto += 24 * 60 * 60;
				}
				
				strcpy(clSblu,FIELDVAL(rgOdaArray,pclOdaRowBuf,igOdaBLEN));
				
			}
			else
			{
				int blGenerateTime = 0;
				
				if (strcmp(FIELDVAL(rgOdaArray,pclOdaRowBuf,igOdaTBSD),"1") == 0)
				{
					blGenerateTime = 1;
				}
				else
				{
					blGenerateTime = 1;
				}
			
				if (blGenerateTime == 1)
				{
					olAvfr = olSday + 8 * 60 * 60 + 30 * 60;
					olAvto = olAvfr + GetCommonDayWorkingTime(FIELDVAL(rgDrrArray,pclDrrRowBuf,igDrrSTFU),olSday);
					strcpy(clSblu,"0000");
				    dbg(TRACE,"SetDrrDataScod: No times in ODATAB!");
				}
			}

			TimeToStr(0,clAvfr,olAvfr);
			TimeToStr(0,clAvto,olAvto);

			if ((ilRC = CEDAArrayPutField(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,NULL,"AVFR",llRowNum,clAvfr)) != RC_SUCCESS)
			{
			    dbg(TRACE,"SetDrrDataScod: CEDAArrayPutField for <%s> at line %d failed!",rgDrrArray.crArrayName,__LINE__);
				return -4;
			}
			
			if ((ilRC = CEDAArrayPutField(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,NULL,"AVTO",llRowNum,clAvto)) != RC_SUCCESS)
			{
			    dbg(TRACE,"SetDrrDataScod: CEDAArrayPutField for <%s> at line %d failed!",rgDrrArray.crArrayName,__LINE__);
				return -4;
			}
			
			if ((ilRC = CEDAArrayPutField(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,NULL,"SBFR",llRowNum,clSbfr)) != RC_SUCCESS)
			{
			    dbg(TRACE,"SetDrrDataScod: CEDAArrayPutField for <%s> at line %d failed!",rgDrrArray.crArrayName,__LINE__);
				return -4;
			}
			
			if ((ilRC = CEDAArrayPutField(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,NULL,"SBTO",llRowNum,clSbto)) != RC_SUCCESS)
			{
			    dbg(TRACE,"SetDrrDataScod: CEDAArrayPutField for <%s> at line %d failed!",rgDrrArray.crArrayName,__LINE__);
				return -4;
			}
						
			if ((ilRC = CEDAArrayPutField(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,NULL,"SBLU",llRowNum,clSblu)) != RC_SUCCESS)
			{
			    dbg(TRACE,"SetDrrDataScod: CEDAArrayPutField for <%s> at line %d failed!",rgDrrArray.crArrayName,__LINE__);
				return -4;
			}
						
		}
		
	}

	if (bpSetEditFlag)
	{
		char clTimeStamp[32];
		
		GetServerTimeStamp("UTC",1,0,clTimeStamp);
				
		/* change creation date */
		if ((ilRC = CEDAArrayPutField(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,NULL,"LSTU",llRowNum,clTimeStamp)) != RC_SUCCESS)
		{
			/* error */
		    dbg(TRACE,"SetDrrDataScod: CEDAArrayPutField for <%s> at line %d failed!",rgDrrArray.crArrayName,__LINE__);
			return -4;
		}
			
		if ((ilRC = CEDAArrayPutField(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,NULL,"USEU",llRowNum,cgUser)) != RC_SUCCESS)
		{
		    dbg(TRACE,"SetDrrDataScod: CEDAArrayPutField for <%s> at line %d failed!",rgDrrArray.crArrayName,__LINE__);
			return -4;
		}
			
	}

    dbg(DEBUG,"SetDrrDataScod at line %d: <--END",__LINE__);

	return ilRC;			
}

static int HandleDrrData(RELEASEDATA *popRelData)
{
	int ilRc = RC_SUCCESS;
	char clNow[32]; 
	char clDateFrom[32];
	char clDateTo[32];
	char clDummy[32];
	char clSelection[50 * 1024];	
	char clGPLUrno[12];	
	char clStfUrno[12];
	int ilPeri;
	int ilDay;
	int ilIndex;
	int ilStaffSize;
	int ilStaff;
	int ilStaffData;
	int ilShift;
	int ilMove;
	
	long llGplRowNum;
	long llGspRowNum;
	long llStfRowNum;
	long llDrrRowNum;
	long llBsdRowNum;
	long llPfcRowNum;
	
	char *pclGplRowBuf = NULL;
	char *pclGspRowBuf = NULL;
	char *pclStfRowBuf = NULL;
	char *pclDrrRowBuf = NULL;
	char *pclBsdRowBuf = NULL;
	char *pclPfcRowBuf = NULL;
	
	int blSetEdit;
	
	time_t olTTmp;
	time_t opFrom;
	time_t opTo;

	ARRAY_IMPL	olStaffData = ArrayCreate(sizeof(RELEASE_STAFF_DATA *));

    dbg(DEBUG,"HandleDrrData at line %d: START-->",__LINE__);
	
	GetServerTimeStamp("UTC", 1, 0, clNow);

	dbg(TRACE,"HandleDrrData : Release shift roster <%s> from/to <%s> at line %d",popRelData->cShiftPlan,popRelData->cDateFrTo,__LINE__);
	
	if (popRelData->pcUrnoList[0] == '\0')
	{
		dbg(TRACE,"HandleDrrData : no STFUs to release at line %d",__LINE__);
		return RC_SUCCESS;
	}

	GetItemNbr(clDateFrom,popRelData->cDateFrTo,'-',1);
	GetItemNbr(clDateTo,popRelData->cDateFrTo,'-',2);

	/* fill the new drr array with already existing data	*/
	sprintf(clSelection,"WHERE STFU IN (%s) AND SDAY BETWEEN '%s' AND '%s' AND ROSL = '1'",	popRelData->pcUrnoList2,clDateFrom,clDateTo);
	dbg(DEBUG, "HandleDrrData : read New Drr: Selection %s at line %d", clSelection,__LINE__);
	
	ilRc = CEDAArrayRefillHint(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,clSelection,NULL,ARR_FIRST,cgDrrReloadHint);
	if (ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"HandleDrrData : CedaArrayRefillHint failed with ec = %d at line %d",ilRc,__LINE__);
		return ilRc;
	}

	/* Calculate time_t values	*/
	strcat(clDateFrom,"000000");
	StrToTime(0,clDateFrom,&opFrom);
	
	strcat(clDateTo,"235959");
	StrToTime(0,clDateTo,&opTo);


	/* loop over all GPL's according to the specified shift plan	*/
	llGplRowNum = ARR_FIRST;
	while (CEDAArrayFindRowPointer(&(rgGplArray.rrArrayHandle),&(rgGplArray.crArrayName[0]),&(rgGplArray.rrIdx02Handle),&(rgGplArray.crIdx02Name[0]),popRelData->cShiftPlan,&llGplRowNum,(void *) &pclGplRowBuf) == RC_SUCCESS)
	{
		strcpy(clGPLUrno,FIELDVAL(rgGplArray,pclGplRowBuf,igGplURNO));
		ilPeri = atoi(FIELDVAL(rgGplArray,pclGplRowBuf,igGplPERI));

	    dbg(DEBUG,"HandleDrrData : GPL with urno = %s and peri = %d matches SPL Urno %s at line %d",clGPLUrno,ilPeri,popRelData->cShiftPlan,__LINE__);
						
		if (ilPeri > 0)
		{
			int ilGPLWeeks  = ilPeri / 7;
			int ilWeekCount = 0;

						
			ARRAY_IMPL olShiftArray		= ArrayCreate(sizeof(RELEASEBUFFER));
			ARRAY_IMPL olShift2Array	= ArrayCreate(sizeof(RELEASEBUFFER));
			ARRAY_IMPL olPfcArray 		= ArrayCreate(sizeof(RELEASEBUFFER));
			ARRAY_IMPL olPfc2Array 		= ArrayCreate(sizeof(RELEASEBUFFER));

		    dbg(DEBUG,"HandleDrrData : GPLWeeks = %d at line %d",ilGPLWeeks,__LINE__);

			ArraySetSize(&olShiftArray,ilPeri,-1);
			ArraySetSize(&olShift2Array,ilPeri,-1);
			ArraySetSize(&olPfcArray,ilPeri,-1);
			ArraySetSize(&olPfc2Array,ilPeri,-1);
			
			/* loop over all GSP's according to the specified GSP	*/
			llGspRowNum = ARR_FIRST;
			while (CEDAArrayFindRowPointer(&(rgGspArray.rrArrayHandle),&(rgGspArray.crArrayName[0]),&(rgGspArray.rrIdx02Handle),&(rgGspArray.crIdx02Name[0]),clGPLUrno,&llGspRowNum,(void *) &pclGspRowBuf) == RC_SUCCESS)
			{
				RELEASEBUFFER clBsd;
				RELEASEBUFFER clBsd2;
				RELEASEBUFFER clFctc;
				RELEASEBUFFER clFctc2;
								
				int ilGSPWeek = atoi(FIELDVAL(rgGspArray,pclGspRowBuf,igGspWEEK));

			    dbg(DEBUG,"HandleDrrData : GSP with Urno = %s and Week = %d at line %d",FIELDVAL(rgGspArray,pclGspRowBuf,igGspURNO),ilGSPWeek,__LINE__);
				
				for (ilDay = 0; ilDay < 7; ilDay++)
				{
					if (popRelData->bDelete == 0)
					{					
						switch(ilDay)
						{
						case 0:
							strcpy(clBsd  ,FIELDVAL(rgGspArray,pclGspRowBuf,igGspBUMO));
							strcpy(clBsd2 ,FIELDVAL(rgGspArray,pclGspRowBuf,igGspSUMO));
							strcpy(clFctc ,FIELDVAL(rgGspArray,pclGspRowBuf,igGspP1MO));
							strcpy(clFctc2,FIELDVAL(rgGspArray,pclGspRowBuf,igGspP2MO));
						break;
						case 1:
							strcpy(clBsd  ,FIELDVAL(rgGspArray,pclGspRowBuf,igGspBUTU));
							strcpy(clBsd2 ,FIELDVAL(rgGspArray,pclGspRowBuf,igGspSUTU));
							strcpy(clFctc ,FIELDVAL(rgGspArray,pclGspRowBuf,igGspP1TU));
							strcpy(clFctc2,FIELDVAL(rgGspArray,pclGspRowBuf,igGspP2TU));
						break;
						case 2:
							strcpy(clBsd  ,FIELDVAL(rgGspArray,pclGspRowBuf,igGspBUWE));
							strcpy(clBsd2 ,FIELDVAL(rgGspArray,pclGspRowBuf,igGspSUWE));
							strcpy(clFctc ,FIELDVAL(rgGspArray,pclGspRowBuf,igGspP1WE));
							strcpy(clFctc2,FIELDVAL(rgGspArray,pclGspRowBuf,igGspP2WE));
						break;						
						case 3:
							strcpy(clBsd  ,FIELDVAL(rgGspArray,pclGspRowBuf,igGspBUTH));
							strcpy(clBsd2 ,FIELDVAL(rgGspArray,pclGspRowBuf,igGspSUTH));
							strcpy(clFctc ,FIELDVAL(rgGspArray,pclGspRowBuf,igGspP1TH));
							strcpy(clFctc2,FIELDVAL(rgGspArray,pclGspRowBuf,igGspP2TH));
						break;						
						case 4:
							strcpy(clBsd  ,FIELDVAL(rgGspArray,pclGspRowBuf,igGspBUFR));
							strcpy(clBsd2 ,FIELDVAL(rgGspArray,pclGspRowBuf,igGspSUFR));
							strcpy(clFctc ,FIELDVAL(rgGspArray,pclGspRowBuf,igGspP1FR));
							strcpy(clFctc2,FIELDVAL(rgGspArray,pclGspRowBuf,igGspP2FR));
						break;						
						case 5:
							strcpy(clBsd  ,FIELDVAL(rgGspArray,pclGspRowBuf,igGspBUSA));
							strcpy(clBsd2 ,FIELDVAL(rgGspArray,pclGspRowBuf,igGspSUSA));
							strcpy(clFctc ,FIELDVAL(rgGspArray,pclGspRowBuf,igGspP1SA));
							strcpy(clFctc2,FIELDVAL(rgGspArray,pclGspRowBuf,igGspP2SA));
						break;						
						case 6:
							strcpy(clBsd  ,FIELDVAL(rgGspArray,pclGspRowBuf,igGspBUSU));
							strcpy(clBsd2 ,FIELDVAL(rgGspArray,pclGspRowBuf,igGspSUSU));
							strcpy(clFctc ,FIELDVAL(rgGspArray,pclGspRowBuf,igGspP1SU));
							strcpy(clFctc2,FIELDVAL(rgGspArray,pclGspRowBuf,igGspP2SU));
						break;						
						}					
					}
					else
					{
						strcpy(clBsd,"0");						
						strcpy(clBsd2,"0");						
						strcpy(clFctc,"0");						
						strcpy(clFctc2,"0");						
					}

			    	dbg(DEBUG,"HandleDrrData : day = %d,BSD = %s,BSD2 = %s,FCTC = %s,FCTC2 = %s at line %d",ilDay,clBsd,clBsd2,clFctc,clFctc2,__LINE__);

								
					/*	write shift codes to shift array	*/
					ilIndex = (ilGSPWeek - 1) * 7 + ilDay;
					
					dbg(DEBUG,"HandleDrrData : ilIndex %d,ilPeri %d at line %d",ilIndex,ilPeri,__LINE__);
					
					if (ilIndex < ilPeri)
					{
						ArraySetAtGrow(&olShiftArray,ilIndex,clBsd);								
						ArraySetAtGrow(&olShift2Array,ilIndex,clBsd2);								
						ArraySetAtGrow(&olPfcArray,ilIndex,clFctc);								
						ArraySetAtGrow(&olPfc2Array,ilIndex,clFctc2);								
					}
					else
					{
						dbg(TRACE,"HandleDrrData : ilIndex %d >= ilPeri %d at line %d",ilIndex,ilPeri,__LINE__);
						return RC_FAIL;
					}
				}								
				
				
				/* loop over each staff	*/
				ilStaffSize = ItemCnt(FIELDVAL(rgGspArray,pclGspRowBuf,igGspSTFU),'\227');
				
				dbg(DEBUG,"HandleDrrData : ilStaffSize %d at line %d",ilStaffSize,__LINE__);
				
				for (ilStaff = 0; ilStaff < ilStaffSize; ilStaff++)
				{
					/* extract staff urno */
					ExtractItem(ilStaff+1,FIELDVAL(rgGspArray,pclGspRowBuf,igGspSTFU),clStfUrno,'\227',10);

					rtrim(clStfUrno);
					dbg(DEBUG,"HandleDrrData : staff urno = <%s> at line %d",clStfUrno,__LINE__);
				
					if (strlen(clStfUrno) > 0 && strstr(popRelData->pcUrnoList,clStfUrno) != NULL)
					{
						dbg(DEBUG,"HandleDrrData : staff urno = <%s> found in %s at line %d",clStfUrno,popRelData->pcUrnoList,__LINE__);
						
						llStfRowNum = ARR_FIRST;
						if (CEDAArrayFindRowPointer(&(rgStfArray.rrArrayHandle),&(rgStfArray.crArrayName[0]),&(rgStfArray.rrIdx02Handle),&(rgStfArray.crIdx02Name[0]),clStfUrno,&llStfRowNum,(void *) &pclStfRowBuf) == RC_SUCCESS)
						{
							RELEASE_STAFF_DATA *prlStaff = (RELEASE_STAFF_DATA *)malloc(sizeof(RELEASE_STAFF_DATA));
							prlStaff->iBeginWeek = ilGSPWeek;
							strcpy(prlStaff->cStfUrno,clStfUrno);				
							StrToTime(0,FIELDVAL(rgStfArray,pclStfRowBuf,igStfDODM),&prlStaff->oDODM);	/* leaving date	*/
							StrToTime(0,FIELDVAL(rgStfArray,pclStfRowBuf,igStfDOEM),&prlStaff->oDOEM);	/* entry date	*/
							prlStaff->oShifts  	 = ArrayCreate(sizeof(RELEASEBUFFER));
							prlStaff->oShifts2 	 = ArrayCreate(sizeof(RELEASEBUFFER));
							prlStaff->oFunctions = ArrayCreate(sizeof(RELEASEBUFFER));
							prlStaff->oFunctions2= ArrayCreate(sizeof(RELEASEBUFFER));

							
							StrToTime(0,FIELDVAL(rgGplArray,pclGplRowBuf,igGplVAFR),&olTTmp);
							if (olTTmp != -1)
							{
								prlStaff->oGPLVafr = olTTmp;
								if (prlStaff->oGPLVafr < opFrom)
								{
									prlStaff->oRelFrom = opFrom;								
								}
								else
								{
									prlStaff->oRelFrom = prlStaff->oGPLVafr;
								}							
							}
							
							if (prlStaff->oDOEM != -1)
							{
								if (prlStaff->oDOEM > prlStaff->oRelFrom)
								{
									prlStaff->oRelFrom = prlStaff->oDOEM;								
								}							
							}
							
							StrToTime(0,FIELDVAL(rgGplArray,pclGplRowBuf,igGplVATO),&olTTmp);
							if (olTTmp != -1)
							{
								prlStaff->oGPLVato = olTTmp;
								if (prlStaff->oGPLVato > opTo)
								{
									prlStaff->oRelTo = opTo;								
								}
								else
								{
									prlStaff->oRelTo = prlStaff->oGPLVato;
								}							
							}
							
							if (prlStaff->oDODM != -1)
							{
								if (prlStaff->oDODM < prlStaff->oRelTo)
								{
									prlStaff->oRelTo = prlStaff->oDODM;								
								}							
							}
	
							ArrayAdd(&olStaffData,&prlStaff);
						}
					}
				}								
				ilWeekCount++;
				
				if (ilGPLWeeks == ilWeekCount)
				{
					break;					
				}
				
				llGspRowNum = ARR_NEXT;
			}			

			for (ilStaffData = 0; ilStaffData < ArrayGetSize(&olStaffData); ilStaffData++)
			{
				int ilBeginDayOfWeek;
				int ilMoveDays = 0;
				
				RELEASE_STAFF_DATA *prlStaff = *(RELEASE_STAFF_DATA **)ArrayGetAt(&olStaffData,ilStaffData);

				
				ArrayAppend(&prlStaff->oShifts,&olShiftArray);
				ArrayAppend(&prlStaff->oShifts2,&olShift2Array);
				ArrayAppend(&prlStaff->oFunctions,&olPfcArray);
				ArrayAppend(&prlStaff->oFunctions2,&olPfc2Array);

				dbg(DEBUG,"HandleDrrData : ArrayAppend staff urno = <%s> ArrayGetSize = %d at line %d",prlStaff->cStfUrno,ArrayGetSize(&prlStaff->oShifts),__LINE__);
				
				TimeToStr(0,clDummy,prlStaff->oRelFrom);
				ilBeginDayOfWeek = DayOfWeek(&prlStaff->oRelFrom) - 1;
				
				ilMoveDays = (prlStaff->iBeginWeek - 1) * 7 + ilBeginDayOfWeek;

				dbg(DEBUG,"HandleDrrData : Move for staff urno = <%s> ilBeginDayOfWeek = %d,ilMoveDays = %d at line %d",prlStaff->cStfUrno,ilBeginDayOfWeek,ilMoveDays,__LINE__);
				
				for (ilMove = 0; ilMove < ilMoveDays; ilMove++)
				{
					RELEASEBUFFER olMoveStr;
					
					/* first shift	*/
					strcpy(olMoveStr,ArrayGetAt(&prlStaff->oShifts,0));
					ArrayRemoveAt(&prlStaff->oShifts,0);
					ArrayAdd(&prlStaff->oShifts,olMoveStr);
					
					/* second shift	*/
					strcpy(olMoveStr,ArrayGetAt(&prlStaff->oShifts2,0));
					ArrayRemoveAt(&prlStaff->oShifts2,0);
					ArrayAdd(&prlStaff->oShifts2,olMoveStr);
					
					/* first function	*/					
					strcpy(olMoveStr,ArrayGetAt(&prlStaff->oFunctions,0));
					ArrayRemoveAt(&prlStaff->oFunctions,0);
					ArrayAdd(&prlStaff->oFunctions,olMoveStr);
					
					/* second function	*/					
					strcpy(olMoveStr,ArrayGetAt(&prlStaff->oFunctions2,0));
					ArrayRemoveAt(&prlStaff->oFunctions2,0);
					ArrayAdd(&prlStaff->oFunctions2,olMoveStr);
				}			
			}

			ArrayDelete(&olShiftArray);
			ArrayDelete(&olShift2Array);
			ArrayDelete(&olPfcArray);
			ArrayDelete(&olPfc2Array);		
		}						
		
		/* Create or modify DRR records	*/
		for (ilStaffData = 0; ilStaffData < ArrayGetSize(&olStaffData); ilStaffData++)
		{
			RELEASE_STAFF_DATA *prlStaff = *(RELEASE_STAFF_DATA **)ArrayGetAt(&olStaffData,ilStaffData);
			
			double dlGPLSpan   = difftime(prlStaff->oRelTo,prlStaff->oRelFrom) / 60;
			double dlDayOffset = difftime(prlStaff->oRelFrom,prlStaff->oGPLVafr) / 60;
			int ilGPLDays   = (int)(dlGPLSpan / 1440) + 1;
			int ilDayOffset = (int)(dlDayOffset / 1440);
			ilDayOffset = ilDayOffset - (ilDayOffset % 7);
			if (ilDayOffset < 0)
				ilDayOffset = 0;
			
			dbg(DEBUG,"HandleDrrData : CreateOrModify for staff urno = <%s> ilGPLDays = %d,ilDayOffset = %d at line %d",prlStaff->cStfUrno,ilGPLDays,ilDayOffset,__LINE__);
			
			for (ilDay = 0; ilDay < ilGPLDays; ilDay++)
			{
				int    blFirstShift = 1;
				time_t olDrrDay = prlStaff->oRelFrom + ilDay * 1440 * 60;
				long   llStfUrno= atol(prlStaff->cStfUrno);
				
				int ilElement = ilDay + ilDayOffset;
				int ilTmpSize = ArrayGetSize(&prlStaff->oShifts);
				
				long   llUrno;
				
				/* rotate	*/
				while(ilElement >= ilTmpSize)
				{
					ilElement -= ilTmpSize;
					if (ilTmpSize == 0)
					{
						ilElement = -1;
					}
				} 	
				
				if (ilElement == -1)
				{
					dbg(TRACE,"HandleDrrData : VERIFICATION(%s) at line %d failed","ilElement >= 0",__LINE__);
					return RC_FAIL;					
				}

				dbg(DEBUG,"HandleDrrData : CreateOrModify for staff urno = <%s> ilElement = %d at line %d",prlStaff->cStfUrno,ilElement,__LINE__);
			
				llUrno = atol(ArrayGetAt(&prlStaff->oShifts,ilElement));
				if (llUrno != 0)
				{
					char clDrrDay[20];
					char clPrimaryKey[512];
					
					TimeToStr(0,clDrrDay,olDrrDay);
					clDrrDay[8] = '\0';
					sprintf(clPrimaryKey,"%ld,%s,%d",llStfUrno,clDrrDay,1);
					llDrrRowNum = ARR_FIRST;
					if (CEDAArrayFindRowPointer(&(rgDrrArray.rrArrayHandle),&(rgDrrArray.crArrayName[0]),&(rgDrrArray.rrIdx01Handle),&(rgDrrArray.crIdx01Name[0]),clPrimaryKey,&llDrrRowNum,(void *) &pclDrrRowBuf) != RC_SUCCESS)
					{
						/* create new drr record	*/
						int ilRC = CreateDrrData(prlStaff->cStfUrno,clDrrDay,"1","","1",&llDrrRowNum,&pclDrrRowBuf);
						if (ilRC != RC_SUCCESS)
						{
							dbg(TRACE,"HandleDrrData: Error %d creating empty DRR record for Staff urno %ld and day %s at line %d",ilRC,llStfUrno,clDrrDay,__LINE__);
							return ilRc;
						}
						
						blSetEdit = 0;							
					}
					else
					{
						blSetEdit = 1;						
					}
					
					if (ilElement >= 0)
					{
						int blCodeIsBsd;						
						
						llBsdRowNum = ARR_FIRST;
						blCodeIsBsd = (CEDAArrayFindRowPointer(&(rgBsdArray.rrArrayHandle),&(rgBsdArray.crArrayName[0]),&(rgBsdArray.rrIdx01Handle),&(rgBsdArray.crIdx01Name[0]),ArrayGetAt(&prlStaff->oShifts,ilElement),&llBsdRowNum,(void *) &pclBsdRowBuf) == RC_SUCCESS);
						
						llPfcRowNum = ARR_FIRST;
						if (CEDAArrayFindRowPointer(&(rgPfcArray.rrArrayHandle),&(rgPfcArray.crArrayName[0]),&(rgPfcArray.rrIdx01Handle),&(rgPfcArray.crIdx01Name[0]),ArrayGetAt(&prlStaff->oFunctions,ilElement),&llPfcRowNum,(void *) &pclPfcRowBuf) == RC_SUCCESS)
						{
							if (SetDrrDataScod(llDrrRowNum,blCodeIsBsd,ArrayGetAt(&prlStaff->oShifts,ilElement),FIELDVAL(rgPfcArray,pclPfcRowBuf,igPfcFCTC),blSetEdit,1) != RC_SUCCESS)
							{
								dbg(TRACE,"HandleDrrData: Existing DRR record for Staff urno %ld and day %s must not be changed at line %d",llStfUrno,clDrrDay,__LINE__);								
							}
						}
						else
						{
							if (SetDrrDataScod(llDrrRowNum,blCodeIsBsd,ArrayGetAt(&prlStaff->oShifts,ilElement),"",blSetEdit,1) != RC_SUCCESS)
							{
								dbg(TRACE,"HandleDrrData: Existing DRR record for Staff urno %ld and day %s must not be changed at line %d",llStfUrno,clDrrDay,__LINE__);								
							}
						}
		
					}						
				}
				else
				{
					char clDrrDay[20];
					char clPrimaryKey[512];

					/* prevent from creating second shift		*/
					blFirstShift = 0;
					
					TimeToStr(0,clDrrDay,olDrrDay);
					clDrrDay[8] = '\0';
					sprintf(clPrimaryKey,"%ld,%s,%d",llStfUrno,clDrrDay,1);
					llDrrRowNum = ARR_FIRST;
					if (CEDAArrayFindRowPointer(&(rgDrrArray.rrArrayHandle),&(rgDrrArray.crArrayName[0]),&(rgDrrArray.rrIdx01Handle),&(rgDrrArray.crIdx01Name[0]),clPrimaryKey,&llDrrRowNum,(void *) &pclDrrRowBuf) == RC_SUCCESS)
					{
						/* delete already existing shifts, if any	*/
						int ilRC = CEDAArrayDeleteRow(&(rgDrrArray.rrArrayHandle),&(rgDrrArray.crArrayName[0]),llDrrRowNum);
						if (ilRC != RC_SUCCESS)
						{
							dbg(TRACE,"HandleDrrData: Error %d deleting existing DRR record for Staff urno %ld and day %s at line %d",ilRC,llStfUrno,clDrrDay,__LINE__);								
						}										
					}
										
					sprintf(clPrimaryKey,"%ld,%s,%d",llStfUrno,clDrrDay,2);
					llDrrRowNum = ARR_FIRST;
					if (CEDAArrayFindRowPointer(&(rgDrrArray.rrArrayHandle),&(rgDrrArray.crArrayName[0]),&(rgDrrArray.rrIdx01Handle),&(rgDrrArray.crIdx01Name[0]),clPrimaryKey,&llDrrRowNum,(void *) &pclDrrRowBuf) == RC_SUCCESS)
					{
						/* delete already existing shifts, if any	*/
						int ilRC = CEDAArrayDeleteRow(&(rgDrrArray.rrArrayHandle),&(rgDrrArray.crArrayName[0]),llDrrRowNum);
						if (ilRC != RC_SUCCESS)
						{
							dbg(TRACE,"HandleDrrData: Error %d deleting existing DRR record for Staff urno %ld and day %s at line %d",ilRC,llStfUrno,clDrrDay,__LINE__);								
						}										
					}
					
					/* create empty record	*/
					if (CreateDrrData(prlStaff->cStfUrno,clDrrDay,"1","","1",&llDrrRowNum,&pclDrrRowBuf) != RC_SUCCESS)
					{
						dbg(TRACE,"HandleDrrData: Error creating empty DRR record for Staff urno %ld and day %s at line %d",llStfUrno,clDrrDay,__LINE__);
					}
				}
				

				/*	second shifts	*/
				ilElement = ilDay+ilDayOffset;
				ilTmpSize = ArrayGetSize(&prlStaff->oShifts2);
						
				/*	rotate	*/
				while(ilElement >= ilTmpSize)
				{
					ilElement -= ilTmpSize;
					if (ilTmpSize == 0) 
					ilElement = -1;
				}

				if (ilElement == -1)
				{
					dbg(TRACE,"HandleDrrData : VERIFICATION(%s) at line %d failed","ilElement >= 0",__LINE__);
					return RC_FAIL;					
				}

				llUrno = atol(ArrayGetAt(&prlStaff->oShifts2,ilElement));
				if (llUrno != 0 && blFirstShift == 1)
				{
					char clDrrDay[20];
					char clPrimaryKey[512];
					
					TimeToStr(0,clDrrDay,olDrrDay);
					clDrrDay[8] = '\0';
					sprintf(clPrimaryKey,"%ld,%s,%d",llStfUrno,clDrrDay,2);
					llDrrRowNum = ARR_FIRST;
					if (CEDAArrayFindRowPointer(&(rgDrrArray.rrArrayHandle),&(rgDrrArray.crArrayName[0]),&(rgDrrArray.rrIdx01Handle),&(rgDrrArray.crIdx01Name[0]),clPrimaryKey,&llDrrRowNum,(void *) &pclDrrRowBuf) != RC_SUCCESS)
					{
						/* create new drr record	*/
						int ilRC = CreateDrrData(prlStaff->cStfUrno,clDrrDay,"1","","2",&llDrrRowNum,&pclDrrRowBuf);
						if (ilRC != RC_SUCCESS)
						{
							dbg(TRACE,"HandleDrrData: Error %d creating empty DRR record for Staff urno %ld and day %s at line %d",ilRC,llStfUrno,clDrrDay,__LINE__);
							return ilRc;
						}
						
						blSetEdit = 0;							
					}
					else
					{
						blSetEdit = 1;						
					}
					
					if (ilElement >= 0)
					{
						int blCodeIsBsd;						
						llBsdRowNum = ARR_FIRST;
						blCodeIsBsd = (CEDAArrayFindRowPointer(&(rgBsdArray.rrArrayHandle),&(rgBsdArray.crArrayName[0]),&(rgBsdArray.rrIdx01Handle),&(rgBsdArray.crIdx01Name[0]),ArrayGetAt(&prlStaff->oShifts2,ilElement),&llBsdRowNum,(void *) &pclBsdRowBuf) == RC_SUCCESS);
						if (CEDAArrayFindRowPointer(&(rgPfcArray.rrArrayHandle),&(rgPfcArray.crArrayName[0]),&(rgPfcArray.rrIdx01Handle),&(rgPfcArray.crIdx01Name[0]),ArrayGetAt(&prlStaff->oFunctions2,ilElement),&llPfcRowNum,(void *) &pclPfcRowBuf) == RC_SUCCESS)
						{
							if (SetDrrDataScod(llDrrRowNum,blCodeIsBsd,ArrayGetAt(&prlStaff->oShifts2,ilElement),FIELDVAL(rgPfcArray,pclPfcRowBuf,igPfcFCTC),blSetEdit,1) != RC_SUCCESS)
							{
								dbg(TRACE,"HandleDrrData: Existing DRR record for Staff urno %ld and day %s must not be changed at line %d",llStfUrno,clDrrDay,__LINE__);								
							}
						}
						else
						{
							if (SetDrrDataScod(llDrrRowNum,blCodeIsBsd,ArrayGetAt(&prlStaff->oShifts2,ilElement),"",blSetEdit,1) != RC_SUCCESS)
							{
								dbg(TRACE,"HandleDrrData: Existing DRR record for Staff urno %ld and day %s must not be changed at line %d",llStfUrno,clDrrDay,__LINE__);								
							}
						}
		
					}						
				}
				else
				{
					char clDrrDay[20];
					char clPrimaryKey[512];

					TimeToStr(0,clDrrDay,olDrrDay);
					clDrrDay[8] = '\0';
					sprintf(clPrimaryKey,"%ld,%s,%d",llStfUrno,clDrrDay,2);
					llDrrRowNum = ARR_FIRST;
					if (CEDAArrayFindRowPointer(&(rgDrrArray.rrArrayHandle),&(rgDrrArray.crArrayName[0]),&(rgDrrArray.rrIdx01Handle),&(rgDrrArray.crIdx01Name[0]),clPrimaryKey,&llDrrRowNum,(void *) &pclDrrRowBuf) == RC_SUCCESS)
					{
						/* delete already existing shifts, if any	*/
						int ilRC = CEDAArrayDeleteRow(&(rgDrrArray.rrArrayHandle),&(rgDrrArray.crArrayName[0]),llDrrRowNum);
						if (ilRC != RC_SUCCESS)
						{
							dbg(TRACE,"HandleDrrData: Error %d deleting existing DRR record for Staff urno %ld and day %s at line %d",ilRC,llStfUrno,clDrrDay,__LINE__);								
						}										
					}
				}
							
			}			
		}
		
		/*	delete olStaffData of current GPL	*/
		for (ilStaffData = 0; ilStaffData < ArrayGetSize(&olStaffData); ilStaffData++)
		{
			RELEASE_STAFF_DATA *prlStaff = *(RELEASE_STAFF_DATA**)ArrayGetAt(&olStaffData,ilStaffData);
			ArrayDelete(&prlStaff->oShifts);
			ArrayDelete(&prlStaff->oShifts2);
			ArrayDelete(&prlStaff->oFunctions);
			ArrayDelete(&prlStaff->oFunctions2);
			free(prlStaff);
		}
		
		ArrayDelete(&olStaffData);
		llGplRowNum = ARR_NEXT;
				
	}

#ifdef	OLD_BEHAVIOR
	/*	write changes to database	*/                        
  	CEDAArrayWriteDB(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName, NULL, NULL, NULL, NULL,ARR_COMMIT_ALL_OK);
#else
		/* Initialise AAT_PARAM structure for old and new demands */
	ilRc = AATArrayCreateArgDesc(rgDrrArray.crArrayName, &prgRsrInf );
	if (ilRc == RC_SUCCESS )
	{
		prgRsrInf->AppendData = FALSE;
		ToolsListSetInfo(&(prgRsrInf->DataList[7]), "IRT_URNO", "DRRTAB","IRT",rgDrrArray.crArrayFieldList, "","",",",4096);
		ToolsListSetInfo(&(prgRsrInf->DataList[8]), "URT_URNO", "DRRTAB","URT",rgDrrArray.crArrayFieldList, "","",",",4096);
		ToolsListSetInfo(&(prgRsrInf->DataList[9]), "DRT_DATA", "DRRTAB","DRT", rgDrrArray.crArrayFieldList, "","",",",-1);
		ToolsListSetInfo(&(prgRsrInf->DataList[10]),"CMD_LIST","","","","","","\n",-1);
		ToolsListSetInfo(&(prgRsrInf->DataList[11]),"SEL_LIST","","","","","","\n",-1);
		ToolsListSetInfo(&(prgRsrInf->DataList[12]),"FLD_LIST","","","","","","\n",-1);
		ToolsListSetInfo(&(prgRsrInf->DataList[13]),"DAT_LIST","","","","","","\n",-1);
	}
	
	AATArraySaveChanges(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,&prgRsrInf, ARR_COMMIT_ALL_OK);
	prgRsrInf->AppendData = TRUE;
#endif	

	commit_work();

	if (bgSendBroadcast == TRUE ||	bgSend2Action == TRUE)
	{
		char *pclData;
		char *pclTrenner;

    	/* Late Answer */
    	dbg(TRACE,"LATE ANSWER SENT TO %d",igQueOut);

		dbg(DEBUG,"vorher: <%s>",popRelData->pcUrnoList);
		do
		{
			pclTrenner = strchr(popRelData->pcUrnoList,'-');
			if (pclTrenner != NULL)
		   	{
				*pclTrenner = 178;
		   	}
		} while (pclTrenner != NULL);

		dbg(DEBUG,"nachher: <%s>",popRelData->pcUrnoList);
		pclData = malloc(strlen(popRelData->cDateFrTo) + strlen(popRelData->pcUrnoList) + 100 );
		sprintf(pclData,"%s-%s-%s%c%s",popRelData->cDateFrTo,popRelData->pcUrnoList,"0",178,"1");

		dbg(TRACE,"%05d:Release from %s to %s",__LINE__,"0","1");



		if (bgSendBroadcast == TRUE)
		{
			dbg(TRACE,"Send to bchdl follows");
			ilRc = AATArraySetSbcInfo(rgDrrArray.crArrayName, prgRsrInf, "1900","3", "SBC", "RELDRR", cgRecvName, cgDestName, cgTwStart,cgTwEnd, "", "", pclData );
			if (ilRc == RC_SUCCESS)
			{
				char clLists[24];

				sprintf ( clLists, "%d,%d,%d", IDX_IRTD, IDX_URTD, IDX_DRTU );
				ilRc = AATArrayCreateSbc(rgDrrArray.crArrayName, prgRsrInf, 0, clLists, FALSE);
			}
		}
		
		if (bgSend2Action == TRUE)
		{
			dbg(TRACE,"Send to action follows"); 
    		(void) tools_send_info_flag(igActionModid,0, cgDestName, "", cgRecvName,"", "", cgTwStart, cgTwEnd,"SBC","RELDRR","OK","EOT,RM",pclData,0);
		}
		
		free(pclData);
	}

	/*** clean up **/
	CEDAArrayDelete(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName);

    dbg(DEBUG,"HandleDrrData at line %d: <--END",__LINE__);
	
	return ilRc;		
}

static int ReleaseShiftRoster(char *pcpData)
{
	int ilRc = RC_SUCCESS;
	RELEASEDATA	olRelData;
	
	/* evaluate input parameters	*/
	ilRc = FillReleaseData(pcpData,&olRelData);	
	if (ilRc != RC_SUCCESS)
		return ilRc;
		
	strcpy(cgUser,olRelData.cUsec);
			
	/* handle drr data	*/			
	ilRc = HandleDrrData(&olRelData);
	if (ilRc != RC_SUCCESS)
		return ilRc;
	
	return ilRc;		
}

/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(EVENT *prpEvent)
{
	int	   ilRc           = RC_SUCCESS;			/* Return code */
	int      i, ilCmd          = 0;

	BC_HEAD *prlBchead       = NULL;
	CMDBLK  *prlCmdblk       = NULL;
	char    *pclSelection    = NULL;
	char    *pclFields       = NULL;
	char    *pclData         = NULL;
	char 	clUrnoList[2400];
	char 	clTable[34];
	char	*pclNewline = NULL;
	char 	clUrno[124];
	static long lgEvtCnt = 0L;

	prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
	prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
	pclSelection = prlCmdblk->data;
	pclFields    = pclSelection + strlen(pclSelection) + 1;
	pclData      = pclFields + strlen(pclFields) + 1;

	strcpy(clTable,prlCmdblk->obj_name);

	dbg(TRACE,"========== START <%10.10d> ==========",lgEvtCnt);

	igQueOut = prgEvent->originator;

  	/* Save Global Elements of CMDBLK */
  	strcpy(cgTwStart,prlCmdblk->tw_start);
  	strcpy(cgTwEnd,prlCmdblk->tw_end);
  	/* Save Global Elements of BC_HEAD */
  	strcpy(cgRecvName,prlBchead->recv_name);
  	strcpy(cgDestName,prlBchead->dest_name);

	/****************************************/
	DebugPrintBchead(DEBUG,prlBchead);
	DebugPrintCmdblk(TRACE,prlCmdblk);

	dbg(TRACE,"originator<%d>",prpEvent->originator);
	dbg(TRACE,"selection <%s>",pclSelection);
	dbg(TRACE,"fields    <%s>",pclFields);
	dbg(TRACE,"data      <%s>",pclData);
	/****************************************/

	/* delete old data from pclData */
	if ((pclNewline = strchr(pclData,'\n')) != NULL)
	{
		*pclNewline = '\0';
	}
	
	if (strstr(pclSelection,"NOACTION") == NULL)
  	{
    	dbg(TRACE,"Changes send to Action"); 
		CEDAArraySendChanges2ACTION(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,TRUE);
		bgSend2Action = FALSE;
	}
	else
	{
    	dbg(TRACE,"don't send changes to Action"); 
		CEDAArraySendChanges2ACTION(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,FALSE);
		bgSend2Action = TRUE;
  	}	
  
  	if (strstr(pclSelection,"NOBC") == NULL)
  	{
  		dbg(TRACE,"Changes send to BcHdl"); 
		CEDAArraySendChanges2BCHDL(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,TRUE);
		bgSendBroadcast = FALSE;
	}
  	else
  	{
    	dbg(TRACE,"don't send changes to BcHdl");
		CEDAArraySendChanges2BCHDL(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,FALSE);
		bgSendBroadcast = TRUE;
  	}
	
	ilRc = GetCommand(&prlCmdblk->command[0],&ilCmd);
	if(ilRc == RC_SUCCESS)
	{
		switch (ilCmd)
		{
		case CMD_IRT :
		case CMD_URT :
    		dbg(DEBUG,"%05d:HandleData: %s",__LINE__,prgCmdTxt[ilCmd]);
			
			ilRc = CEDAArrayEventUpdate2(prgEvent,clUrnoList,clTable);
				
			if (ilRc != RC_SUCCESS)
			{
      			dbg(TRACE,"HandleData: CEDAArrayEventUpdate failed <%d>",ilRc);
			}
			else if (strcmp(clTable,"GSPTAB") == 0)
			{
				/***
				DumpArray(&rgGspArray,2,"195230914","URNO,GPLU,STFU");
				***/
			}
			break;
		case CMD_DRT :
			ilRc = CEDAArrayEventUpdate2(prgEvent,clUrnoList,clTable);
			if (ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"HandleData: CEDAArrayEventUpdate failed <%d>",ilRc);
			}
			break;	
		case CMD_RSR:
			if (strcmp(pclSelection,"[CHECK_AVAILABLE]") == 0)
			{
				SendAnswer(1900,RC_SUCCESS,""); 
				dbg(TRACE,"HandleData: [CHECK_AVAILABLE] request received");
			}
			else
			{
				SendAnswer(1900,NETOUT_NO_ACK,""); 
				ilRc = ReleaseShiftRoster(pclData);
				if (ilRc != RC_SUCCESS)
				{
					dbg(TRACE,"HandleData: ReleaseShiftRoster failed <%d>",ilRc);
				}
			}
			break;
		default: 
			;
		}		/* end of switch */
	}
	
	dbg(TRACE,"==========  END  <%10.10d> ==========",lgEvtCnt);
	return(RC_SUCCESS);
}


static int GetFieldLength(char *pcpHopo, char *pcpTana, char *pcpFina,
							long *plpLen)
{
	int  ilRc = RC_SUCCESS;			/* Return code */
	int  ilCnt = 0;
	char clTaFi[32];
	char clFele[16];
	char clFldLst[16];

	ilCnt = 1;
	sprintf(&clTaFi[0],"%s,%s",pcpTana,pcpFina);
	sprintf(&clFldLst[0],"TANA,FINA"); /* wird von syslib zerstört */

	/*dbg(TRACE,"GetFieldLength Tana <%s> Fina <%s>",pcpTana,pcpFina);*/

	ilRc = syslibSearchSystabData(&cgTabEnd[0],&clFldLst[0],&clTaFi[0],"FELE",&clFele[0],&ilCnt,"");
	switch (ilRc)
	{
	case RC_SUCCESS :
		*plpLen = atoi(&clFele[0]);
		/* dbg(DEBUG,"GetFieldLength: <%s,%s,%s> <%d>",pcpHopo,pcpTana,pcpFina,*plpLen);*/
		break;

	case RC_NOT_FOUND :
		dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_NOT_FOUND <%s>",ilRc,&clTaFi[0]);
		break;

	case RC_FAIL :
		dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_FAIL",ilRc);
		break;

	default :
		dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> unexpected",ilRc);
		break;
	}/* end of switch */

	return(ilRc);
	
}


static int GetRowLength(char *pcpHopo, char *pcpTana, char *pcpFieldList,
						long *plpLen)
{
	int	 ilRc = RC_SUCCESS;			/* Return code */
	int  ilNoOfItems = 0;
	int  ilLoop = 0;
	long llFldLen = 0;
	long llRowLen = 0;
	char clFina[8];
	
	ilNoOfItems = get_no_of_items(pcpFieldList);

	ilLoop = 1;
	do
	{
		ilRc = get_real_item(&clFina[0],pcpFieldList,ilLoop);
		if(ilRc > 0)
		{
			ilRc = GetFieldLength(pcpHopo,pcpTana,&clFina[0],&llFldLen);
			if(ilRc == RC_SUCCESS)
			{
				llRowLen++;
				llRowLen += llFldLen;
			}/* end of if */
		}/* end of if */
		ilLoop++;
	}while((ilRc == RC_SUCCESS) && (ilLoop <= ilNoOfItems));

	if(ilRc == RC_SUCCESS)
	{
		*plpLen = llRowLen;
	}/* end of if */

	return(ilRc);
	
}


static int SetArrayInfo(char *pcpArrayName, char *pcpTableName, char *pcpArrayFieldList,char *pcpAddFields,long *pcpAddFieldLens,ARRAYINFO *prpArrayInfo,
	char *pcpSelection,BOOL bpFill)
{
	int	ilRc = RC_SUCCESS;				/* Return code */

	if(prpArrayInfo == NULL)
	{
		dbg(TRACE,"SetArrayInfo: invalid last parameter : null pointer not valid");
		ilRc = RC_FAIL;
	}else
	{
		memset(prpArrayInfo,0x00,sizeof(prpArrayInfo));

		prpArrayInfo->rrArrayHandle = -1;
		prpArrayInfo->rrIdx01Handle = -1;

		if(pcpArrayName != NULL)
		{
			if(strlen(pcpArrayName) <= ARR_NAME_LEN)
			{
				strcpy(prpArrayInfo->crArrayName,pcpArrayName);
			}/* end of if */
		}/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		prpArrayInfo->lrArrayFieldCnt = get_no_of_items(pcpArrayFieldList);
		if (pcpAddFields != NULL)
		{
			prpArrayInfo->lrArrayFieldCnt += get_no_of_items(pcpAddFields);
		}

		prpArrayInfo->plrArrayFieldOfs = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
		if(prpArrayInfo->plrArrayFieldOfs == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrArrayFieldOfs calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		prpArrayInfo->plrArrayFieldLen = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
		if(prpArrayInfo->plrArrayFieldLen == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrArrayFieldLen calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}else{
			*(prpArrayInfo->plrArrayFieldLen) = -1;
		}/* end of if */
	}/* end of if */

   	if(ilRc == RC_SUCCESS)
   	{
       	ilRc = GetRowLength(&cgHopo[0],pcpTableName,pcpArrayFieldList,
							&(prpArrayInfo->lrArrayRowLen));
       	if(ilRc != RC_SUCCESS)
       	{
           	dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpArrayName,pcpArrayFieldList);
      	}/* end of if */
   	}/* end of if */

   	if(ilRc == RC_SUCCESS)
   	{
		prpArrayInfo->lrArrayRowLen+=100;
		prpArrayInfo->pcrArrayRowBuf = (char *) calloc(1,(prpArrayInfo->lrArrayRowLen+10));
   		if(prpArrayInfo->pcrArrayRowBuf == NULL)
   		{
      			ilRc = RC_FAIL;
      			dbg(TRACE,"SetArrayInfo: calloc failed");
   		}/* end of if */
   	}/* end of if */

   	if(ilRc == RC_SUCCESS)
   	{
		prpArrayInfo->pcrIdx01RowBuf = (char *) calloc(1,(prpArrayInfo->lrIdx01RowLen+1));
   		if(prpArrayInfo->pcrIdx01RowBuf == NULL)
   		{
      			ilRc = RC_FAIL;
      			dbg(TRACE,"SetArrayInfo: calloc failed");
   		}/* end of if */
   	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		dbg(TRACE,"CEDAArrayCreate follows Array <%s> Table <%s>",pcpArrayName,pcpTableName);
		ilRc = CEDAArrayCreate(&(prpArrayInfo->rrArrayHandle),
				pcpArrayName,pcpTableName,pcpSelection,pcpAddFields,
				pcpAddFieldLens,pcpArrayFieldList,
				&(prpArrayInfo->plrArrayFieldLen[0]),
				&(prpArrayInfo->plrArrayFieldOfs[0]));

		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"SetArrayInfo: CEDAArrayInitialize failed <%d>",ilRc);
		}/* end of if */
	}/* end of if */

		if(pcpArrayFieldList != NULL)
		{
			if(strlen(pcpArrayFieldList) <= ARR_FLDLST_LEN)
			{
				strcpy(prpArrayInfo->crArrayFieldList,pcpArrayFieldList);
			}/* end of if */
		}/* end of if */
		if(pcpAddFields != NULL)
		{
			if(strlen(pcpAddFields) + 
				strlen(prpArrayInfo->crArrayFieldList) <= (size_t)ARR_FLDLST_LEN)
			{
				strcat(prpArrayInfo->crArrayFieldList,",");
				strcat(prpArrayInfo->crArrayFieldList,pcpAddFields);
				dbg(TRACE,"<%s>",prpArrayInfo->crArrayFieldList);
			}/* end of if */
		}/* end of if */


	if(ilRc == RC_SUCCESS && bpFill)
	{
		ilRc = CEDAArrayFill(&(prpArrayInfo->rrArrayHandle),pcpArrayName,NULL);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"SetArrayInfo: CEDAArrayFill failed <%d>",ilRc);
		}/* end of if */
	}/* end of if */

	return(ilRc);
}


static void StringTrimRight(char *pcpBuffer)
{
	if (*pcpBuffer != '\0')
	{
		char *pclBlank = &pcpBuffer[strlen(pcpBuffer)-1];
		while(isspace(*pclBlank) && pclBlank != pcpBuffer)
		{
			*pclBlank = '\0';
			pclBlank--;
		}
	}
}

static int ItemCnt(char *pcgData, char cpDelim)
{
	int	ilItemCount=0;

	if (pcgData && strlen(pcgData)) 
	{
	    ilItemCount = 1;
	    while (*pcgData) 
		{
			if (*pcgData++ == cpDelim) 
			{
				ilItemCount++;
			} /* end if */
		} /* end while */
	} /* end if */
	return ilItemCount;
}

/******************************************************************************/
/* Get Item Nbr n from DATA with Delim '|' ...                                */
/******************************************************************************/
static int GetItemNbr(char *Item, char *Data, char Delim, long Nbr)
{
    int ilRC = RC_SUCCESS;              /* Return code */
    long llItemCnt = -1;
	int ilLoop;
    int ilFirstItemlngth = -1;
    char  *pclData   = NULL;
    char  *pclDelim   = NULL;
    
/*    dbg(DEBUG,"GetItemNbr: START-->");
*/
    pclData  = Data ;
    pclDelim = &Delim ;

    /*dbg(DEBUG,"GetItemNbr: counting items in '%s', delim is '%c'",Data,Delim);*/

    llItemCnt = ItemCnt(Data,Delim); 
    if (Nbr > llItemCnt)
    {
	    dbg(DEBUG,"GetItemNbr: less than <%d> items in string (<%d>)!!!",Nbr,llItemCnt);
        ilRC = RC_FAIL;
    }
    else
    {
    	for(ilLoop = 0 ; ilLoop < Nbr ; ilLoop++)
	    {
            ilFirstItemlngth = GetNextDataItem(Item,&pclData,pclDelim,"","\0\0");
        }
    }
   
/*	dbg(DEBUG,"GetItemNbr: <-- ENDE ");
*/
    return (ilRC);
}

static int TimeToStr(int ipUTC,char *pcpTime,time_t lpTime)
{
	struct tm *_tm;
 	
 	if (ipUTC)
 	{
		_tm = (struct tm *)gmtime(&lpTime);
	}
	else
	{
		_tm = (struct tm *)localtime(&lpTime);
	}

  	sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",_tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,_tm->tm_min,_tm->tm_sec);
  	return (_tm->tm_wday == 0 ? 7 : _tm->tm_wday) + '0';
}                                                                                

static int StrToTime(int ipUTC,char *pcpTime,time_t *plpTime)
{
	struct tm	*_tm;
	time_t 		now;
	char   		_tmpc[6];
	long 		llUtcDiff;

	dbg(0,"StrToTime, <%s>",pcpTime);
	if (strlen(pcpTime) < 12 )
	{
		*plpTime = -1;
		return RC_FAIL;
	} /* end if */

	now = time(0L);
	_tm = (struct tm *)gmtime(&now);

	_tmpc[2] = '\0';
	_tm -> tm_sec = 0;
	strncpy(_tmpc,pcpTime+10,2);
	_tm -> tm_min = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+8,2);
	_tm -> tm_hour = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+6,2);
	_tm -> tm_mday = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+4,2);
	_tm -> tm_mon = atoi(_tmpc)-1;
	strncpy(_tmpc,pcpTime,4);
	_tmpc[4] = '\0';
	_tm ->tm_year = atoi(_tmpc)-1900;
	_tm ->tm_wday = 0;
	_tm ->tm_yday = 0;
	_tm->tm_isdst = -1; /* no adjustment of daylight saving time */
	now = mktime(_tm);
	dbg(0,"StrToTime: %02d.%02d.%04d %02d:%02d Now: %ld",_tm->tm_mday,_tm->tm_mon+1,_tm->tm_year+1900,_tm->tm_hour,_tm->tm_min,now);
	
	/* valid time ?	*/
	if (now != (time_t) -1)
	{
		if (ipUTC)
		{		
			time_t utc,local;
		
			_tm = (struct tm *)gmtime(&now);
			utc = mktime(_tm);
			_tm = (struct tm *)localtime(&now);
			local = mktime(_tm);
			dbg(TRACE,"utc %ld, local %ld, utc-diff %ld",utc,local,local-utc);
			llUtcDiff = local-utc;
			now +=  + llUtcDiff;
		}
		
		*plpTime = now;
		return RC_SUCCESS;
	}
	else
	{
		*plpTime = -1;
		return RC_FAIL;
	}
}

void	StrAddTime(char *pcpBase,char *pcpOffs,char *pcpDest)
{

	if (pcpBase != pcpDest)
	{
		strcpy(pcpDest,pcpBase);
	}
	AddSecondsToCEDATime(pcpDest,(time_t)atol(pcpOffs),1);

} /* end of StrAddTime */

static int SendAnswer(int ipDest,int ipRc, char *pcpAnswer)
{
	int			ilRC;
	int			ilLen;
	EVENT		*prlOutEvent 	= NULL;
	BC_HEAD		*prlOutBCHead 	= NULL;
	CMDBLK		*prlOutCmdblk	= NULL;

    dbg(DEBUG,"SendAnswer: START-->");

	/* calculate size of memory we need */
	ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + strlen(pcpAnswer) + 32 ;

	/* get memory for out event */
	if ((prlOutEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
	{
		dbg(TRACE,"<SendAnswer> %05d malloc failed, can't allocate %d bytes", __LINE__, ilLen) ;
		dbg(DEBUG,"<SendAnswer> ----- END -----") ;
		return RC_FAIL ;
	}

	/* clear buffer */
	memset((void*)prlOutEvent, 0x00, ilLen) ;

	/* set structure members */
	prlOutEvent->type	   = SYS_EVENT ;
	prlOutEvent->command 	   = EVENT_DATA ;
	prlOutEvent->originator  = ipDest ;            /* mod_id ;   */ 
	prlOutEvent->retry_count = 0 ;
	prlOutEvent->data_offset = sizeof(EVENT) ;
	prlOutEvent->data_length = ilLen - sizeof(EVENT) ;

	/* BCHead members */
	prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT)) ;
	prlOutBCHead->rc = ipRc;
	strncpy(prlOutBCHead->dest_name, "ACTION", 10) ;

	/* CMDBLK members */
	prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data) ;
	strcpy(prlOutCmdblk->command, "FIR") ;
	strcpy(prlOutCmdblk->tw_end, "ANS,WER,ACTION") ;

	/* data */
	strcpy(prlOutCmdblk->data+2, pcpAnswer) ;

	/* send this message */
	dbg(DEBUG,"<SendAnswer> sending to Mod-ID: %d", prgEvent->originator) ;
	if ((ilRC = que(QUE_PUT, prgEvent->originator, mod_id, PRIORITY_3, ilLen, (char*)prlOutEvent)) != RC_SUCCESS)
	{
		/* delete memory */
		free((void*)prlOutEvent) ;
		dbg(TRACE,"<SendAnswer> %05d QUE_PUT returns: %d", __LINE__, ilRC) ;
		dbg(DEBUG,"<SendAnswer> ----- END -----") ;
		return RC_FAIL ;
	}

/*  
	DebugPrintBchead (DEBUG, prlOutBCHead) ;
	DebugPrintCmdblk (DEBUG, prlOutCmdblk) ;
	DebugPrintEvent  (DEBUG, prlOutEvent) ;               
*/
	/* release memory */
	free((void*)prlOutEvent);
	
    dbg(DEBUG,"SendAnswer: <-- ENDE ");
	
	/* bye bye */
	return RC_SUCCESS;
}

static int DumpArray(ARRAYINFO *prpArray,int ipIndex,char *pcpValue,char *pcpFields)
{
	int ilRc;
	int ilRecord = 0;
	long llRowNum = ARR_FIRST;
	char *pclRowBuf;
	char clBuffer[2048];	

	dbg(TRACE,"Arraydump: %s", prpArray->crArrayName);
	if (pcpFields != NULL)
	{
		dbg(TRACE,"Arraydump: Fields = %s", pcpFields);
	}
	else
	{
		dbg(TRACE,"Arraydump: Fields = %s",prpArray->crArrayFieldList);
	}

	if (ipIndex == 1)
	{						
		while(CEDAArrayFindRowPointer(&(prpArray->rrArrayHandle),&(prpArray->crArrayName[0]),&(prpArray->rrIdx01Handle),&(prpArray->crIdx01Name[0]),pcpValue,&llRowNum,(void *) &pclRowBuf) == RC_SUCCESS)
		{
				int ilFieldNo;
			
				ilRecord++;
				llRowNum = ARR_NEXT;
	
				dbg(TRACE,"Record No: %d",ilRecord);
				for (ilFieldNo = 1; ilFieldNo <= prpArray->lrArrayFieldCnt; ilFieldNo++)
				{
					char pclFieldName[24];
	
					*pclFieldName = '\0';
					GetDataItem(pclFieldName,prpArray->crArrayFieldList,
						ilFieldNo,',',"","\0\0");
					if (pcpFields != NULL)      
					{
						if(strstr(pcpFields,pclFieldName) == NULL)
						{
							continue;
						}
					}
					strcpy(clBuffer,PFIELDVAL(prpArray,pclRowBuf,ilFieldNo-1));
					rtrim(clBuffer);
					dbg(TRACE,"%s: <%s>",pclFieldName,clBuffer);
				}
		}
	}
	else
	{
		while(CEDAArrayFindRowPointer(&(prpArray->rrArrayHandle),&(prpArray->crArrayName[0]),&(prpArray->rrIdx02Handle),&(prpArray->crIdx02Name[0]),pcpValue,&llRowNum,(void *) &pclRowBuf) == RC_SUCCESS)
		{
				int ilFieldNo;
			
				ilRecord++;
				llRowNum = ARR_NEXT;
	
				dbg(TRACE,"Record No: %d",ilRecord);
				for (ilFieldNo = 1; ilFieldNo <= prpArray->lrArrayFieldCnt; ilFieldNo++)
				{
					char pclFieldName[24];
	
					*pclFieldName = '\0';
					GetDataItem(pclFieldName,prpArray->crArrayFieldList,
						ilFieldNo,',',"","\0\0");
					if (pcpFields != NULL)      
					{
						if(strstr(pcpFields,pclFieldName) == NULL)
						{
							continue;
						}
					}
					strcpy(clBuffer,PFIELDVAL(prpArray,pclRowBuf,ilFieldNo-1));
					rtrim(clBuffer);
					dbg(TRACE,"%s: <%s>",pclFieldName,clBuffer);
				}
		}
	}
	return ilRc;

}

/******************************************************************************/
/* Space for Appendings (Source-Code Control)                                 */
/******************************************************************************/
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
