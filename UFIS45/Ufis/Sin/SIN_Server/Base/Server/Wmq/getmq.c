#ifndef _DEF_mks_version
#define _DEF_mks_version
#include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Sin/SIN_Server/Base/Server/Wmq/getmq.c 1.0 2008/07/17 12:37:18SGT ble Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :                                                           */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history : HEB: added Alerter-Funktionality                          */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */

static char sccs_version[] ="@(#) UFIS45 (c) ABB AAT/I putmq.c 45.3 / 04.06.2002 HEB";


/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */


#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>

/* includes for WebsphereMQ  */
#include <cmqc.h>

/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/

/*Handles for Sending*/
MQHCONN  HconSend;                   /* connection handle             */
MQHOBJ   HobjSend;                   /* object handle                 */

/*Handles for Receiving */
MQHCONN  HconRecv;                 /* connection handle             */
MQHOBJ   HobjRecv;
MQHCONN  HconRecvBrowse;                 /* connection handle             */
MQHOBJ   HobjRecvBrowse;

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
int main( int argc, char *argv[] )
{
	MQOD mqOd = {MQOD_DEFAULT};
	MQMD mqMd = {MQMD_DEFAULT};
	MQGMO mqGmo = {MQGMO_DEFAULT};
	MQBYTE mqpclMessage[10000];
	MQLONG mqllCompletionCode;
	MQLONG mqllCompletionReason;
	MQLONG mqllOptions;
	MQLONG mqllOpenCode;
	MQLONG mqllQueueReason;
	MQLONG mqllRecvLen;
	int ili, ilLoops, ilj;
	char pclQManager[300];
	char pclQName[300];
	char pclQChannel[300];
	char pclQServer[1000];
	char pclTmpStr[20];

	if( argc < 5 )
    {
		printf( "Usage: getmq Qmanager Qname QChannel loop=1\n" );
		printf( "   Eg: getmq QDMSD00 UFIS_TO_TIG \"QDMSD00.CLIENT.ITREK/TCP/green(1414)\" loop=3\n" );
		exit( -1 );
    }
    sprintf( pclQManager, "%s", argv[1] );
    sprintf( pclQName,    "%s", argv[2] );
    sprintf( pclQChannel, "%s", argv[3] );
    sprintf( pclTmpStr,   "%s", &argv[4][5] );
	ilLoops = atoi(pclTmpStr);

	printf( "QManager = <%s>\n", pclQManager );
	printf( "QName    = <%s>\n", pclQName );
	printf( "QChannel = <%s>\n", pclQChannel );
	printf( "Loops    = <%d>\n", ilLoops );

    sprintf( pclQServer, "MQSERVER=%s", pclQChannel );
	putenv(pclQServer);

	mqllCompletionCode = MQCC_FAILED;
	MQCONN( pclQManager, &HconRecv, &mqllCompletionCode, &mqllCompletionReason );
	if( mqllCompletionCode == MQCC_FAILED )
	{
		printf( "Error in connecting Reason <%d>\n", mqllCompletionReason );
	    exit( 2 );	
	}
	printf( "Connected\n" );
		



	mqllOptions = MQOO_INPUT_AS_Q_DEF + 
		          MQOO_FAIL_IF_QUIESCING;
	strcpy( mqOd.ObjectName, pclQName );

    MQOPEN( HconRecv, &mqOd, mqllOptions, &HobjRecv, &mqllCompletionCode, &mqllCompletionReason );
	if( mqllCompletionReason != MQRC_NONE )
	{
		printf( "Error in opening Q Reason <%d>\n", mqllCompletionReason );
	    exit( 2 );	
	}
	printf( "Opened\n" );
 

	for( ili = 0; ili < ilLoops; ili++ )
	{
		memcpy( mqMd.Format, MQFMT_STRING, MQ_FORMAT_LENGTH );
		memcpy( mqMd.MsgId,  MQMI_NONE, sizeof(mqMd.MsgId) );
		memcpy( mqMd.CorrelId,  MQCI_NONE, sizeof(mqMd.CorrelId) );
		mqMd.Priority = 5;
		mqGmo.Options = MQGMO_WAIT + 
			            MQGMO_FAIL_IF_QUIESCING +
				        MQGMO_ACCEPT_TRUNCATED_MSG;
		mqGmo.WaitInterval = MQWI_UNLIMITED;

		mqllRecvLen = sizeof(mqpclMessage) - 1;


		MQGET( HconRecv, HobjRecv, &mqMd, &mqGmo, sizeof(mqpclMessage), 
			   mqpclMessage, &mqllRecvLen, &mqllCompletionCode, &mqllCompletionReason );
		if( mqllCompletionReason != MQRC_NONE )
		{
			printf( "Error in getting msg from Q Reason <%d>\n", mqllCompletionReason );
			break;	
		}
		mqpclMessage[mqllRecvLen] = '\0';
		for( ilj = 0; ilj < mqllRecvLen; ilj++ )
		{
			if( mqpclMessage[ilj] == '\0' )
			    mqpclMessage[ilj] = ' ';
		}
		printf( "Len=%d Message (%s)\n\n", mqllRecvLen, mqpclMessage );
	}

	mqllOptions = MQCO_NONE;
	MQCLOSE( HconRecv, &HobjRecv, mqllOptions, &mqllCompletionCode, &mqllCompletionReason );
	MQDISC( &HconRecv, &mqllCompletionCode, &mqllCompletionReason  );
	if( mqllCompletionCode != MQRC_NONE )
	{
		printf( "Error in closing Q Reason <%d>\n", mqllCompletionReason );
	    exit( 2 );	
	}
	printf( "Closed\n" );
}
