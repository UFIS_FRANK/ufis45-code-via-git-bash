#ifndef _DEF_mks_version
#define _DEF_mks_version
#include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Sin/SIN_Server/Base/Server/Wmq/putmq.c 1.0 2008/07/17 12:35:18SGT ble Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :                                                           */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history : HEB: added Alerter-Funktionality                          */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */

static char sccs_version[] ="@(#) UFIS45 (c) ABB AAT/I putmq.c 45.3 / 04.06.2002 HEB";


/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */


#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>

/* includes for WebsphereMQ  */
#include <cmqc.h>

#define YES 2
#define NO  -2

/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/

/*Handles for Sending*/
MQHCONN  HconSend;                   /* connection handle             */
MQHOBJ   HobjSend;                   /* object handle                 */

/*Handles for Receiving */
MQHCONN  HconRecBrowse;              /* connection handle             */
MQHCONN  HconRecGet;                 /* connection handle             */
MQHOBJ   HobjRecBrowse;              /* object handle                 */
MQHOBJ   HobjRecGet;

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
int main( int argc, char *argv[] )
{
    FILE *prlFptr;
    MQOD mqOd = {MQOD_DEFAULT};
    MQMD mqMd = {MQMD_DEFAULT};
    MQPMO mqPmo = {MQPMO_DEFAULT};
    MQLONG mqllCompletionCode;
    MQLONG mqllCompletionReason;
    MQLONG mqllOptions;
    MQLONG mqllOpenCode;
    MQLONG mqllQueueReason;
    int ilInputMessage = NO;
    int ilInputFile = NO;
    int ilOpenFile = NO;
    char pclQManager[300];
    char pclQName[300];
    char pclQChannel[300];
    char pclMessage[50000];
    char pclLine[1000];
    char pclFileName[300];
    char pclQServer[1000];

    if( argc < 4 )
    {
        printf( "Usage: putmq Qmanager Qname QChannel\n" );
        printf( "       putmq Qmanager Qname QChannel msg=\"message\"\n" );
        printf( "       putmq Qmanager Qname QChannel file=\"filename\"\n" );
        printf( "  Eg.: putmq TIGMQ UFIS_TO_TIG \"TIGMQ/TCP/RP4440C(1416)\"\n" );
        printf( "  Eg.: putmq QDMSD00 UFIS_TO_TIG \"QDMSD00.CLIENT.ITREK/TCP/green(1414)\"\n" );
        printf( "  Eg.: putmq QDMSD00 UFIS_TO_TIG \"QDMSD00.CLIENT.ITREK/TCP/green(1414)\" msg=\"Try this\"\n" );
        printf( "  Eg.: putmq QDMSD00 UFIS_TO_TIG \"QDMSD00.CLIENT.ITREK/TCP/green(1414)\" file=\"/ceda/exco/tighdl/UFIS_TO_TIG.000001\"\n" );
        exit( -1 );
    }
    sprintf( pclQManager, "%s", argv[1] );
    sprintf( pclQName,    "%s", argv[2] );
    sprintf( pclQChannel, "%s", argv[3] );
    if( argc > 4 )
    {
        if( !strncmp(argv[4], "msg=", 4) )
        {
            ilInputMessage = YES;
            sprintf( pclMessage, "%s", &argv[4][4] );
        }
        else if( !strncmp(argv[4], "file=", 5) )
        {
            ilInputFile = YES;
            sprintf( pclFileName, "%s", &argv[4][5] );
        }
    }

    printf( "QManager = <%s>\n", pclQManager );
    printf( "QName    = <%s>\n", pclQName );
    printf( "QChannel = <%s>\n", pclQChannel );

    sprintf( pclQServer, "MQSERVER=%s", pclQChannel );
    putenv(pclQServer);

    mqllCompletionCode = MQCC_FAILED;
    MQCONN( pclQManager, &HconSend, &mqllCompletionCode, &mqllCompletionReason );
    if( mqllCompletionCode == MQCC_FAILED )
    {
        printf( "Error in connecting Reason <%d>\n", mqllCompletionReason );
        exit( 2 );  
    }
    printf( "Connected\n" );
        


    mqllOptions = MQOO_OUTPUT + 
                  MQOO_FAIL_IF_QUIESCING;
    strcpy( mqOd.ObjectName, pclQName );

    MQOPEN( HconSend, &mqOd, mqllOptions, &HobjSend, &mqllCompletionCode, &mqllCompletionReason );
    if( mqllCompletionReason != MQRC_NONE )
    {
        printf( "Error in opening Q Reason <%d>\n", mqllCompletionReason );
        exit( 2 );  
    }
    printf( "Opened\n" );

    if( ilInputFile == YES )
    {
        prlFptr = fopen( pclFileName, "r" );
        if( prlFptr == NULL )
        {
            printf( "Error in opening file %d<%s>\n", errno, strerror(errno) );
            exit( 3 );
        }
        printf( "File (%s) opened for reading\n", pclFileName );
        while( !feof(prlFptr) )
        {
            if( (fgets( pclLine, sizeof(pclLine), prlFptr )) == NULL )
                break;
            /*if( pclLine[strlen(pclLine)] == '\n' )
                pclLine[strlen(pclLine)] = ' ';*/
            strcat(pclMessage, pclLine);
        }
        pclMessage[strlen(pclMessage)] = '\0';
    }

    for( ;; )
    {
        if( ilInputMessage == NO && ilInputFile == NO )
        {
            printf( "Enter text to send:\n" );
            gets( pclMessage );
            if( !strncmp( pclMessage, "END", 3 ) )
                break;
            if( strlen(pclMessage) <= 0 )
                break;
        }
        pclMessage[strlen(pclMessage)] = '\0';
        printf( "Sending text (%s)\n", pclMessage );

        memcpy( mqMd.Format, MQFMT_STRING, MQ_FORMAT_LENGTH );
        memcpy( mqMd.MsgId,  MQMI_NONE, sizeof(mqMd.MsgId) );
        mqMd.Priority = 5;
        mqPmo.Options = MQPMO_NEW_MSG_ID + 
                        MQOO_FAIL_IF_QUIESCING;

        MQPUT( HconSend, HobjSend, &mqMd, &mqPmo, strlen(pclMessage), 
               pclMessage, &mqllCompletionCode, &mqllCompletionReason );
        if( mqllCompletionReason != MQRC_NONE )
        {
            printf( "Error in putting msg to Q Reason <%d>\n", mqllCompletionReason );
            break;  
        }
        printf( "Put\n" );

        if( ilInputMessage == YES || ilInputFile == YES )
            break;
        sleep (1);
    }


    
    mqllOptions = MQCO_NONE;
    MQCLOSE( HconSend, &HobjSend, mqllOptions, &mqllCompletionCode, &mqllCompletionReason );
    MQDISC( &HconSend, &mqllCompletionCode, &mqllCompletionReason  );
    if( mqllCompletionCode != MQRC_NONE )
    {
        printf( "Error inclosing Q Reason <%d>\n", mqllCompletionReason );
        exit( 2 );  
    }
    printf( "Closed\n" );
    if( ilInputFile == YES )
        fclose( prlFptr );
}
