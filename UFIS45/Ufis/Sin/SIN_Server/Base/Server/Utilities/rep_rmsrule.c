#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Sin/SIN_Server/Base/Server/Utilities/rep_rmsrule.c 1.1 2009/10/13 15:39:11SGT ble Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :  Mei, Lee Bee Suan                                        */
/* Date           :  27-AUG-2009                                              */
/* Description    :  To extract RegelWerk Data into text file                 */
/* Notes          :                                                           */
/* 1. TPL.TNAM = 'Security'                                                   */
/* 2. RUE.UTPL = TPL.URNO                                                     */
/* 3. VAL.UVAL = RUE.URNO                                                     */
/* 4. RUD.URUE = RUE.URNO                                                     */
/* 5. SER.URNO = RUD.UGHS                                                     */
/* 6. RPF.URUD = RUD.URNO                                                     */
/*                                                                            */
/* Update history :                                                           */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */

static char sccs_version[] ="@(#) UFIS44 (c) ABB AAT/I skeleton.c 44.1.0 / 11.12.2002 HEB";

/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */

#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <unistd.h>
#include "ugccsma.h"
#include "glbdef.h"
#include "uevent.h"
#include "db_if.h"
#include <time.h>
#include <cedatime.h>
#include <stdarg.h>

#define MAX_LOG_SIZE    1048576   /* 1 MB */

typedef struct 
{
    char TNAM[68];
    char APPL[12];
    char URNO[12];
} TPLREC;

typedef struct 
{
    char RUSN[64];
    char EVTY[64];
    char RUNA[512];
    char EXCL[4];
    char EVTT[1024];
    char HOPO[4];
    char EVTA[1024];
    char EVTD[1024];
    char URNO[12];
} RUEREC;

typedef struct 
{
    char VAFR[16];
    char VATO[16];
    char APPL[8];
    char TABN[8];
    char UVAL[12];   /* think there is a bug in RegelWerk, the field URUE was not stored with the URNO */
} VALREC;

typedef struct 
{
    char DEDU[12];
    char RTDB[8];
    char TSDB[12];
    char RTDE[8];
    char TSDE[12];
    char URUE[12];
    char URNO[12];
    char UGHS[12];
    char DRTY[4];
} RUDREC;

typedef struct 
{
    char SNAM[512];
} SERREC;

typedef struct 
{
    char FCCO[12];
} RPFREC;

typedef struct 
{
    short SqlCursor;
    short SqlFunc;
    char SqlBuf[1024];
} SQLREC;

typedef struct 
{
    char ALC2[4];
    char FLTN[12];
    char TTYP[12];
    char DES3[8];
} CONDREC;

char cgCRMDelimiter = ',';
char pcgRepFname[256];
char pcgRuleType[4][2] = { "T", "I", "O", "-" };

extern int debug_level = 0;

extern void dbglog(char *fmt, ...);
extern int RunSql( char *pcpSqlBuf, char *pcpSqlData );
extern void ConvertChar( char *pcpInStr );
extern void GetCondition( char *pcpEVT, CONDREC *rpCOND );

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
    FILE *fptr;
    SQLREC rlSRUE, rlSRUD;
    TPLREC rlTPL;
    RUEREC rlRUE;
    VALREC rlVAL;
    RUDREC rlRUD;
    SERREC rlSER;
    RPFREC rlRPF;
    CONDREC rlCOND;
    int ilRc;
    int ilTries = 1;
    int ilRec = 1;
    int ili, ilj;
    int ilEVTY, ilDRTY;
    int ilLen;
    char pclSqlBuf[1024];
    char pclSqlData[2048] = "\0";
    char pclTmpStr[1024];

    if( argc < 3 )
    {
        dbglog( "Usage: rep_secrule template_name report_fname" );
        dbglog( "   Eg: rep_secrule \"Load Control\" \"/ceda/exco/CRMSECRULES.csv\"" );
        exit( -1 );
    }
    memset( &rlTPL, 0, sizeof(TPLREC) );
    sprintf( rlTPL.TNAM, "%s", argv[1] );
    sprintf( pcgRepFname, "%s", argv[2] );
    dbglog( "Creating report for Template <%s>...", rlTPL.TNAM );

    do
    {
        ilRc = init_db();
        if( ilRc != DB_SUCCESS )
        {
            dbglog( "Cannot connect to UFIS database, try again\n" );
            sleep( 60 );
            ilTries++;
        }
    } while( ilTries <= 10 && ilRc != DB_SUCCESS );

    if( ilTries > 10 && ilRc != DB_SUCCESS )
    {
        dbglog( "Fail to connect to UFIS DB" );
        exit( -1 );
    }
    dbglog( "UFIS DB connected" );

    memset( &rlSRUE, 0, sizeof(SQLREC) );
    memset( &rlSRUD, 0, sizeof(SQLREC) );
    
    sprintf( pclSqlBuf, "SELECT TRIM(URNO) FROM TPLTAB WHERE TNAM = '%s' AND APPL = 'RULE_AFT'", rlTPL.TNAM );
    ilRc = RunSql( pclSqlBuf, pclSqlData );
    if( ilRc != DB_SUCCESS )
    {
        dbglog( "Template <%s> not found in TPLTAB. Abort!", rlTPL.TNAM );
        exit( -1 );
    }
    /* NOTE: RUST = Rule Status (=deactive, 1=active, 2=archived) */
    strcpy( rlTPL.URNO, pclSqlData );
    sprintf( rlSRUE.SqlBuf, "SELECT TRIM(URNO),TRIM(RUSN),TRIM(EVTY),TRIM(RUNA),TRIM(EXCL),"
                           "TRIM(EVTT),TRIM(HOPO),TRIM(EVTA),TRIM(EVTD) "
                           "FROM RUETAB WHERE UTPL = '%s' AND APPL = 'RULE_AFT' AND RUST = '1'", rlTPL.URNO );
    rlSRUE.SqlCursor = 0;
    rlSRUE.SqlFunc = START;
    fptr = fopen( pcgRepFname, "w" );
    if( fptr == NULL )
    {
        dbglog( "Cannot open file for writing <%s> error <%s><%d>", pcgRepFname, errno, strerror(errno) );
        exit( -1 );
    }
    fprintf( fptr, "%s","RUSN,RUNA,VAFR,VATO,RUTY,"
                        "EXCL,ACT3,HOPO,SNAM,DURA,"
                        "FCCO,INAL,INFL,INTY,RTDB,"
                        "TSDB,OUAL,OUFL,OUTY,OUDE,"
                        "RTDE,TSDE\n" );
    memset( &rlRUE, 0, sizeof(RUEREC) );
    while( (sql_if( rlSRUE.SqlFunc, &rlSRUE.SqlCursor, rlSRUE.SqlBuf, pclSqlData )) == DB_SUCCESS )
    {
        rlSRUE.SqlFunc = NEXT;
        get_fld(pclSqlData,FIELD_1,STR,10,rlRUE.URNO); dbglog( "%4d: RUE.URNO = <%s>", ilRec, rlRUE.URNO ); 
        get_fld(pclSqlData,FIELD_2,STR,14,rlRUE.RUSN); dbglog( "%4d: RUE.RUSN = <%s>", ilRec, rlRUE.RUSN ); 
        get_fld(pclSqlData,FIELD_3,STR,1,rlRUE.EVTY);  dbglog( "%4d: RUE.EVTY = <%s>", ilRec, rlRUE.EVTY );
        get_fld(pclSqlData,FIELD_4,STR,64,rlRUE.RUNA); dbglog( "%4d: RUE.RUNA = <%s>", ilRec, rlRUE.RUNA ); 
        get_fld(pclSqlData,FIELD_5,STR,1,rlRUE.EXCL);  dbglog( "%4d: RUE.EXCL = <%s>", ilRec, rlRUE.EXCL );
        get_fld(pclSqlData,FIELD_6,STR,400,rlRUE.EVTT);dbglog( "%4d: RUE.EVTT = <%s>", ilRec, rlRUE.EVTT );
        get_fld(pclSqlData,FIELD_7,STR,3,rlRUE.HOPO);  dbglog( "%4d: RUE.HOPO = <%s>", ilRec, rlRUE.HOPO ); 
        get_fld(pclSqlData,FIELD_8,STR,800,rlRUE.EVTA);dbglog( "%4d: RUE.EVTA = <%s>", ilRec, rlRUE.EVTA ); 
        get_fld(pclSqlData,FIELD_9,STR,800,rlRUE.EVTD);dbglog( "%4d: RUE.EVTD = <%s>", ilRec, rlRUE.EVTD );  

        ConvertChar( rlRUE.RUSN ); dbglog( "%4d: [CONV] RUE.RUSN = <%s>", ilRec, rlRUE.RUSN );
        ConvertChar( rlRUE.RUNA ); dbglog( "%4d: [CONV] RUE.RUNA = <%s>", ilRec, rlRUE.RUNA );
        ConvertChar( rlRUE.EVTT ); dbglog( "%4d: [CONV] RUE.EVTT = <%s>", ilRec, rlRUE.EVTT );
        ConvertChar( rlRUE.EVTA ); dbglog( "%4d: [CONV] RUE.EVTA = <%s>", ilRec, rlRUE.EVTA );
        ConvertChar( rlRUE.EVTD ); dbglog( "%4d: [CONV] RUE.EVTD = <%s>", ilRec, rlRUE.EVTD );                

        memset( &rlVAL, 0, sizeof(VALREC) );
        sprintf( pclSqlBuf, "SELECT TRIM(VAFR),TRIM(VATO) FROM VALTAB WHERE "
                            "UVAL = '%s' AND TABN = 'RUE' AND APPL = 'RULE_AFT'", rlRUE.URNO );
        ilRc = RunSql( pclSqlBuf, pclSqlData );
        if( ilRc != DB_SUCCESS )
            memset( &rlVAL, 0, sizeof(VALREC) );
        else
        {            
            get_fld(pclSqlData,FIELD_1,STR,16,rlVAL.VAFR); dbglog( "%4d: VAL.VAFR = <%s>", ilRec, rlVAL.VAFR ); 
            get_fld(pclSqlData,FIELD_2,STR,16,rlVAL.VATO); dbglog( "%4d: VAL.VATO = <%s>", ilRec, rlVAL.VATO );
        }

        sprintf( rlSRUD.SqlBuf, "SELECT TRIM(URNO),TRIM(UGHS),TRIM(DEDU),TRIM(RTDB),TRIM(TSDB),"
                               "TRIM(RTDE),TRIM(TSDE),TRIM(DRTY) FROM RUDTAB WHERE URUE = '%s'", rlRUE.URNO );
        rlSRUD.SqlCursor = 0;
        rlSRUD.SqlFunc = START;
        while( (sql_if( rlSRUD.SqlFunc, &rlSRUD.SqlCursor, rlSRUD.SqlBuf, pclSqlData )) == DB_SUCCESS )
        {
            ilRec++;
            rlSRUD.SqlFunc = NEXT;          
            get_fld(pclSqlData,FIELD_1,STR,10,rlRUD.URNO); dbglog( "%4d: RUD.URNO = <%s>", ilRec, rlRUD.URNO );
            get_fld(pclSqlData,FIELD_2,STR,10,rlRUD.UGHS); dbglog( "%4d: RUD.UGHS = <%s>", ilRec, rlRUD.UGHS );
            get_fld(pclSqlData,FIELD_3,STR,10,rlRUD.DEDU); dbglog( "%4d: RUD.DEDU = <%s>", ilRec, rlRUD.DEDU );
            get_fld(pclSqlData,FIELD_4,STR,4,rlRUD.RTDB);  dbglog( "%4d: RUD.RTDB = <%s>", ilRec, rlRUD.RTDB );
            get_fld(pclSqlData,FIELD_5,STR,10,rlRUD.TSDB); dbglog( "%4d: RUD.TSDB = <%s>", ilRec, rlRUD.TSDB );
            get_fld(pclSqlData,FIELD_6,STR,4,rlRUD.RTDE);  dbglog( "%4d: RUD.RTDE = <%s>", ilRec, rlRUD.RTDE );
            get_fld(pclSqlData,FIELD_7,STR,10,rlRUD.TSDE); dbglog( "%4d: RUD.TSDE = <%s>", ilRec, rlRUD.TSDE );
            get_fld(pclSqlData,FIELD_8,STR,1,rlRUD.DRTY);  dbglog( "%4d: RUD.DRTY = <%s>", ilRec, rlRUD.DRTY );
            

            memset( &rlSER, 0, sizeof(SERREC) );
            sprintf( pclSqlBuf, "SELECT TRIM(SNAM) FROM SERTAB WHERE URNO = '%s'", rlRUD.UGHS );
            ilRc = RunSql( pclSqlBuf, pclSqlData ); dbglog( "%4d: SER.SNAM = <%s>", ilRec, pclSqlData );
            ConvertChar( pclSqlData );
            if( ilRc == DB_SUCCESS )
                strcpy( rlSER.SNAM, pclSqlData );
            dbglog( "%4d: [CONV] SER.SNAM = <%s>", ilRec, rlSER.SNAM );

            memset( &rlRPF, 0, sizeof(RPFREC) );
            sprintf( pclSqlBuf, "SELECT TRIM(FCCO) FROM RPFTAB WHERE URUD = '%s'", rlRUD.URNO );
            ilRc = RunSql( pclSqlBuf, pclSqlData ); dbglog( "%4d: RPF.FCCO = <%s>", ilRec, rlRPF.FCCO );
            if( ilRc == DB_SUCCESS )
                strcpy( rlRPF.FCCO, pclSqlData );
            dbglog( "%4d: [CONV] RPF.FCCO = <%s>", ilRec, rlRPF.FCCO ); 

            fprintf( fptr, "%.14s%c", rlRUE.RUSN, cgCRMDelimiter  );    /* RULE SHORT NAME */
            fprintf( fptr, "%.64s%c", rlRUE.RUNA, cgCRMDelimiter  );    /* RULE FULL NAME */

            memset( pclTmpStr, 0, sizeof(pclTmpStr) );
            if( strlen(rlVAL.VAFR) > 0 )
                sprintf( pclTmpStr, "%2.2s/%2.2s/%4.4s", &rlVAL.VAFR[6], &rlVAL.VAFR[4], rlVAL.VAFR );
            fprintf( fptr, "%s%c", pclTmpStr, cgCRMDelimiter  );        /* VALID FROM */

            memset( pclTmpStr, 0, sizeof(pclTmpStr) );
            if( strlen(rlVAL.VATO) > 0 )
                sprintf( pclTmpStr, "%2.2s/%2.2s/%4.4s", &rlVAL.VATO[6], &rlVAL.VATO[4], rlVAL.VATO );
            fprintf( fptr, "%s%c", pclTmpStr, cgCRMDelimiter  );        /* VALID TO */

            ilEVTY = atoi(rlRUE.EVTY);
            if( ilEVTY < 0 || ilEVTY >= 3 )
                ilEVTY = 3;
            fprintf( fptr, "%c%c", pcgRuleType[ilEVTY][0], cgCRMDelimiter );  /* RULE INDICATOR - T/I/O */
            fprintf( fptr, "%c%c", rlRUE.EXCL[0], cgCRMDelimiter );     /* EXCLUDE FLAG */

            memset( pclTmpStr, 0, sizeof(pclTmpStr) );
            if( strlen(rlRUE.EVTT) > 0 && !strncmp(rlRUE.EVTT,"0.AFT.ACT3=",11) )
                strcpy( pclTmpStr, &rlRUE.EVTT[11] );
            fprintf( fptr, "%.3s%c", pclTmpStr, cgCRMDelimiter  );      /* AIRCRAFT TYPE */

            fprintf( fptr, "%.3s%c", rlRUE.HOPO, cgCRMDelimiter );      /* HOME AIRPORT */
            fprintf( fptr, "\"%s\"%c", rlSER.SNAM, cgCRMDelimiter );     /* SERVICE NAME */

            sprintf( pclTmpStr, "%d", atoi(rlRUD.DEDU)/60 );
            fprintf( fptr, "%.10s%c", pclTmpStr, cgCRMDelimiter );      /* DURATION */

            fprintf( fptr, "%.5s%c", rlRPF.FCCO, cgCRMDelimiter );      /* FUNCTION CODE */

            ilDRTY = atoi(rlRUD.DRTY);
            /*********** INBOUND ***********/
            memset( &rlCOND, 0, sizeof(CONDREC) );
            GetCondition( rlRUE.EVTA, &rlCOND );
            fprintf( fptr, "%.3s%c%.3s%c%.3s%c", rlCOND.ALC2, cgCRMDelimiter, 
                                                 rlCOND.FLTN, cgCRMDelimiter, 
                                                 rlCOND.TTYP, cgCRMDelimiter );
            if( ilDRTY == 2 )
            {
                memset( rlRUD.RTDB, 0, sizeof(rlRUD.RTDB) );
                memset( rlRUD.TSDB, 0, sizeof(rlRUD.TSDB) );
            }
            fprintf( fptr, "%.4s%c", rlRUD.RTDB, cgCRMDelimiter );      /* INBOUND REFERENCE INDICATOR */
            fprintf( fptr, "%d%c", atoi(rlRUD.TSDB)/60, cgCRMDelimiter );     /* INBOUND REFERENCE TIME */

            /*********** OUTBOUND ***********/
            memset( &rlCOND, 0, sizeof(CONDREC) );
            GetCondition( rlRUE.EVTD, &rlCOND );
            fprintf( fptr, "%.3s%c%.3s%c%.3s%c%.3s%c", rlCOND.ALC2, cgCRMDelimiter, 
                                                       rlCOND.FLTN, cgCRMDelimiter, 
                                                       rlCOND.TTYP, cgCRMDelimiter, 
                                                       rlCOND.DES3, cgCRMDelimiter );
            if( ilDRTY == 1 )
            {
                memset( rlRUD.RTDE, 0, sizeof(rlRUD.RTDB) );
                memset( rlRUD.TSDE, 0, sizeof(rlRUD.TSDB) );
            }
            fprintf( fptr, "%.4s%c", rlRUD.RTDE, cgCRMDelimiter );      /* OUTBOUND REFERENCE INDICATOR */
            fprintf( fptr, "%d", atoi(rlRUD.TSDE)/60 );     /* OUTBOUND REFERENCE TIME */
            
            
            fprintf( fptr, "\n" );
            dbglog( "----------------------------------------------------------------" );

            memset( &rlRUD, 0, sizeof(RUDREC) );
        } /* while loop of RUD */
        close_my_cursor( &rlSRUD.SqlCursor );
        memset( &rlRUE, 0, sizeof(RUEREC) );
    } /* while loop of RUE */

    close_my_cursor( &rlSRUE.SqlCursor );
    fclose( fptr );
    dbglog( "File <%s> successfully created", pcgRepFname );
}

void dbglog(char *fmt, ...)
{
    va_list args;
    struct tm *tmptr;
    char pclTmStr[24];
    time_t tlNow;
    char pclPrint[1024];


    tlNow = time(0);
    tmptr = localtime( (time_t *)&tlNow );
    sprintf( pclTmStr, "%2.2d-%2.2d-%2.2d %2.2d:%2.2d:%2.2d ", 
                       tmptr->tm_mday, tmptr->tm_mon+1, tmptr->tm_year%100,
                       tmptr->tm_hour, tmptr->tm_min, tmptr->tm_sec );

     va_start(args, fmt);
     sprintf( pclPrint, "%s", pclTmStr );
     strcat( pclPrint, fmt );
     strcat( pclPrint, "\n" );
     vprintf( pclPrint, args );
     va_end( args );
}

int RunSql( char *pcpSqlBuf, char *pcpSqlData )
{
    int ilRc;
    short slSqlCursor;
    char pclError[256];

    slSqlCursor = 0;
    dbglog( "SQL: <%s>", pcpSqlBuf );
    ilRc = sql_if( START, &slSqlCursor, pcpSqlBuf, pcpSqlData );
    close_my_cursor( &slSqlCursor );
    if( ilRc == DB_ERROR )
    {
        get_ora_err( ilRc, (char *)&pclError );
        dbglog( "ORACLE ERROR: <%s>", pclError );
    }
    return ilRc;
}

void ConvertChar( char *pcpInStr )
{
    int ili, ilj;
    int ilLen;
    char *pclStr;

    ilLen = strlen(pcpInStr);
    if( ilLen <= 0 )
        return;
    pclStr = (char *)malloc( ilLen * 3 );
    ilj = 0;
    for( ili = 0; ili < ilLen; ili++ )
    {
        if( pcpInStr[ili] == '\233' )       /* 0x9B - '(' */
            pclStr[ilj] = '\050';
        else if( pcpInStr[ili] == '\234' )  /* 0x9C - ')' */
            pclStr[ilj] = '\051';
        else if( pcpInStr[ili] == '\235' )  /* 0x9D - '^\' */
            pclStr[ilj] = '\034';     
        else if( pcpInStr[ili] == '\260' )  /* 0x22 - '"' */
        {
            pclStr[ilj++] = '\042';
            pclStr[ilj] = '\042';
        }
        else if( pcpInStr[ili] == '\261' )  /* 0x27 - ''' */
            pclStr[ilj] = '\047';
        else if( pcpInStr[ili] == '\262' )  /* 0x2C - ',' */
            pclStr[ilj] = '\054';
        else
            pclStr[ilj] = pcpInStr[ili];
        ilj++;
    }
    pclStr[ilj] = '\0';
    /*memset( pcpInStr, 0, ilLen );*/
    strcpy( pcpInStr, pclStr );
    free( pclStr );
}

void GetCondition( char *pcpEVT, CONDREC *rpCOND )
{
    int ili, ilc;
    int ilLen;
    char pclCondition[128];

    ilLen = strlen(pcpEVT);
    ilc = 0;
    for( ili = 0; ili < ilLen; ili++ )
    {
        if( pcpEVT[ili] != 0x1C )
        {
            pclCondition[ilc++] = pcpEVT[ili];
            if( ili+1 < ilLen )
                continue;
        }
        pclCondition[ilc] = '\0';
        dbglog( "ili = %d Condition = <%s>", ili, pclCondition );
        if( ilc > 0 )
        {
            if( !strncmp( pclCondition, "0.AFT.ALC2=", 11 ) )
                strcpy( rpCOND->ALC2, &pclCondition[11] );
            else if( !strncmp( pclCondition, "0.AFT.FLTN=", 11 ) )
                strcpy( rpCOND->FLTN, &pclCondition[11] );
            else if( !strncmp( pclCondition, "0.AFT.TTYP=", 11 ) )
                strcpy( rpCOND->TTYP, &pclCondition[11] );
            else if( !strncmp( pclCondition, "0.AFT.DES3=", 11 ) )
                strcpy( rpCOND->DES3, &pclCondition[11] );
        }
        ilc = 0;
    }
    dbglog( "1ALC2 = <%s> FLTN = <%s> TTYP = <%s>", rpCOND->ALC2, rpCOND->FLTN, rpCOND->TTYP );
}
