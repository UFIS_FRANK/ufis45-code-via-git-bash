#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" Id: Ufis/Sin/SIN_Server/Base/Server/Interface/itkhdl.c 2.1 2013/06/04 13:53:05SGT ble Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :  Thanooj Kumar Putsala                                    */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history :                                                           */
/* 25-JAN-07   BLE: For Phase II Implementation on iTrek                      */ 
/*                : 4 new Oracle Tables are created-                          */
/*                :     IMGTAB = iTrek Message Table                          */
/*                :     IMRTAB = iTrek Message Received Table                 */
/* 29-APR-08   BLE: 1. Upd ITRTAB when status timing is received              */
/*                : 2. UPd JOBTAB when MAB status is received, exclude TurnDDArr */
/*                : 3. Change logic of trips handling                         */
/*                : 4. Include URNO in CPM XML message                        */
/*                : 5. Process CPM telex upd from fdihdl and send to web srv  */
/* 12-MAY-08   BLE: 1. Change the setting of JOB's status to C00 configurable */
/* 04-JUL-08   BLE:                                                           */
/* 1. Store apron jobs in mini-jobtab called ITKJOB, to prevent sending of redundant job delete to webs server */
/* 2. To have PENO info stored in OSTTAB when doing the status update timing                                   */
/* 3. Configurable recipent name for status timing update. Web server sending AFLIGHT, in the config file,     */
/*    A=Apron,Load Control etc                                                                                 */
/* 4. to schedule for mini-JOBTAB to be cleaned at  03:35 am every day                                         */
/* 5. to handle CPMOnline on empty UTLX given by fdihdl, this caused strtok to fail and process to exit        */
/* 6. Reduce trip xml text by removing same info                                                               */
/* 7. Split BULK flight and jobs timing                                                                        */
/* 8. Exclude some FTYP when sending flight to Web server                                                      */
/* 9. Verify time of flight (TIFA/TIFD) before forwarding to iTrek Web Server                                  */
/*                                                                                                             */
/* 18-JUL-08   BLE: Modification on Apron Template checking in Apr affects the sending of Turnarnd flight upd  */
/* 28-JUL-08   BLE: Merge DLS code                                                                             */
/* 28-JUL-08   BLE: For FDIHDL messages, if is meant for ARR flight CPM, ignore the telex                      */
/* 28-JUL-08   BLE: Shift basic function to Inc/itrek_fn.inc in case code gets too length for 1 .c file        */
/* 01-AUG-08   BLE: Amend STTY from N to L when LTRIP xml is received                                          */
/* 01-AUG-08   BLE: Implement message detail retrieval for 1 msg                                               */
/* 01-AUG-08   BLE: Amend break tag of DLS to new line char                                                    */
/* 28-AUG-08   BLE: 1) Add FLIGHT as recipent for messages. iTC gives "F!"in TO_ID, iTS strips off this "F!"   */
/*                     and return back                                                                         */
/* 09-SEP-08   BLE: Error in sending redundant XML to iTC, after turnarnd, when no changes on job, xtra flight */
/*                  was sent                                                                                   */
/* 25-SEP-08   BLE: Filter flights which are non-SATs handling                                                 */
/* 29-SEP-08   BLE: Check TIFD/TIFA timing in the range for both amended timings                               */
/* 29-SEP-08   BLE: Confirm DEP job when ARR job MAB is received                                               */
/* 30-SEP-08   BLE: Bug in Turnaround -> Split for job reassigned to DEP flight                                */
/* 30-SEP-08   BLE: Change in BULK flight to include 3 timings check instead of jus TIFD/TIFA                  */
/* 13-OCT-08   BLE: Set up status manager rules settings request/reply xml                                     */
/* 14-OCT-08   BLE: Bug in setting the CONA time in OSTTAB, previously only 1 record is changed                */
/* 24-NOV-08   BLE: ACTION did not provide UAFT, DETY when Turnaround Transit flights were reassigned to       */
/*                  staff. Change in SendJobOnline()                                                           */
/* 26-NOV-08   BLE: DLS did not show the weight of BULK items                                                  */
/* 26-NOV-08   BLE: Seperate JOB CONFIRM with status update outcome                                            */
/* 18-DEC-08   BLE: Seperate Reload Handling Agent Cmd with ITKJOB cleaning                                    */
/* 02-MAR-09   BLE: Added STAFF request for the XML to DB enhancement changed                                  */
/* 02-JUN-09   BLE: GSE Implementation                                                                         */
/* 24-JUN-09   BLE: Added FTYP and RKEY for flight data                                                        */
/* 09-FEB-10   BLE: Added 2 new officer code, changes have been made in itkaut.cfg                             */
/* 09-FEB-10   BLE: In case of fixes in RMS module, bulk jobs are added with more filtering rules              */
/* 12-APR-10   BLE: Lengthy func code for baggage,need to expand memory - OFFICER_FCODE_LEN                    */ 
/* 29-APR-10   BLE: Add in tracking facilities                                                                 */ 
/* 23-AUG_10   BLE: New status "PLB_D"                                                                         */
/* 11-MAR-11   BLE: New group AIC                                                                              */
/* 25-JAN-13   BLE: A major skipping of version. Discard changes made of v1.36 till v1.39 as FML/CSGN changes are not needed */
/*                  This v2.0 version is meant for iTrek PTS to be forwarded with DLS messages                 */
/* 03-JUN-13   BLE: Massage ULD number for freighter CPM telexes UFIS-3507 */
/***************************************************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */

static char sccs_version[] ="@(#) UFIS44 (c) ABB AAT/I skeleton.c 44.1.0 / 11.12.2002 HEB";

/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */

#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "fditools.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "rmschkrul.h"
#include "syslib.h"
#include "itrek_fn.inc"
#include "db_if.h"
#include "urno_fn.inc"
#include <time.h>
#include <cedatime.h>
#include <stdarg.h>

#ifdef _HPUX_SOURCE
    extern long timezone;
    extern char *tzname[2];
    extern int _daylight;
    extern long _timezone;
    extern char *_tzname[2];
#endif

#ifndef BOOL
#define BOOL int
#endif


/* MEI: Phase II Implementation for iTrek */
#define URNOLEN  11
#define USIDLEN  32
#define NAMELEN  40
#define SDNMLEN  50
#define FNLNLEN  40
#define BODYLEN  256     /* For Messaging - Message Body */
#define DATELEN  14
#define MQRDLEN  15000   /* For Msg from the MQ */
#define DBQRLEN  5000    /* For DB SELECT query */
#define SSTPLEN  10      /* For Load(CPM) Query */
#define ULDTLEN  16      /* For Load(CPM) Query */
#define ULDPLEN  16      /* For Load(CPM) Query */
#define APC3LEN  3       /* For Load(CPM) Query */
#define VALULEN  6       /* For Load(CPM) Query */
#define ADDILEN  132     /* For Load(CPM) Query */
#define STATLEN  20      /* For Status Name */
#define TPTYLEN  1       /* For Trip Information */
#define DESTLEN  5       /* For Trip Information */
#define ICPMLEN  1       /* For Trip Information */
#define ULDILEN  60      /* For Trip Information */
#define SESSLEN  50      /* For SEssion Information */
#define FCCOLEN  10      /* For Job table */
#define UTPLLEN  10      /* For Job table */
#define DETYLEN  1       /* For Job table */
#define FLNOLEN  9       /* For Aft table */
#define COTYLEN  3       /* For Aft table */
#define REGNLEN  12      /* For Aft table */
#define GATELEN  5      /* For Aft table */
#define ADIDLEN  1      /* For Aft table */
#define PSTALEN  5      /* For Aft table */
#define BELTLEN  5      /* For Aft table */
#define TGADLEN  1      /* For Aft table */
#define FTYPLEN  1      /* For Aft table */
#define TTYPLEN  5      /* For Aft table */
#define ALC2LEN  2      /* For Aft table */
#define ALC3LEN  3      /* For Aft table */
#define PENOLEN 20      /* For Stf table */

#define TYPE_BULK    1
#define TYPE_INSERT  2
#define TYPE_UPDATE  3
#define TYPE_DELETE  4
#define TYPE_BULK_JOB 5
#define TYPE_BULK_AFT 6
#define TYPE_ARRIVAL  'A'
#define TYPE_ANY      'N'

#define FLIGHT_TOID  "F!"

#define SRCH_BY_PENO 1 
#define SRCH_BY_URNO 2

#define DEF_YES 'Y'
#define DEF_NO  'N'

#define MAX_DB_RECORDS 500
#define MAX_FLIGHTS 1000
#define MAX_FLIGHT_FIELDS 23
#define NUM_TIME_FIELDS 6 /* For Flight Info Update */

#define OFFICER_FCODE_LEN 1024
#define OFFICER_TITLE_LEN 20
#define NUM_OFFICER_TYPE 13

#define START_DATA_SIZE  40000
#define BLK_DATA_SIZE    5000

/* DLS */
#define END_TELEX -1

#define HSS_AO  0
#define HSS_BO  1
#define HSS_AIC  2

#define ARR_FLIGHT 0
#define DEP_FLIGHT 1
#define BLK_JOB    2
#define NUM_BULK_TIME 3
#define STAT_UNKNOWN -8

/* record structure for table IMGTAB = iTrek Msg Table */
typedef struct
{
    char URNO[URNOLEN+1];
    char SDID[USIDLEN+1];
    char SDNM[SDNMLEN+1];
    char SDFN[FNLNLEN+1];
    char SDLN[FNLNLEN+1];
    char MGBY[BODYLEN+1];
    char SDDT[DATELEN+1];
} IMGREC;

/* record structure for table IMRTAB = iTrek Msg Receive Table */
typedef struct
{
    char URNO[URNOLEN+1];
    char MGNO[URNOLEN+1];
    char TOID[USIDLEN+1];
    char TOFN[FNLNLEN+1];
    char TOLN[FNLNLEN+1];
    char RCID[USIDLEN+1];
    char RCFN[FNLNLEN+1];
    char RCLN[FNLNLEN+1];
    char RCDT[DATELEN+1];
} IMRREC;

/* Subset of LOATAB fields */
typedef struct
{
    char SSTP[SSTPLEN+1];
    char ULDT[ULDTLEN+1];
    char ULDP[ULDPLEN+1];
    char APC3[APC3LEN+1];
    char VALU[VALULEN+1];
    char NOP[4];
    char URNO[URNOLEN+1];
    char FLNO[FLNOLEN+1];
    char ALC2[ALC2LEN+1];
    char ADDI[ADDILEN+1];
} LOAREC;  


/* record structure for table ITRTAB = iTrek Trip Table */
typedef struct
{
    int  STNO;
    int  RTNO;
    char URNO[URNOLEN+1];
    char UAFT[URNOLEN+1];
    char ULDI[ULDILEN+1];  /* ULD information to be displayed on mobile */
    char TPTY[TPTYLEN+1];
    char STTY[TPTYLEN+1];
    char RTTY[TPTYLEN+1];
    char DEST[DESTLEN+1];
    char ICPM[ICPMLEN+1];
    char SDDT[DATELEN+1];
    char SDBY[USIDLEN+1];
    char SDRM[BODYLEN+1];
    char RCDT[DATELEN+1];
    char RCBY[USIDLEN+1];
    char RCRM[BODYLEN+1];
} ITRREC;

typedef struct
{
    char URNO[URNOLEN+2]; /* Buffer one char to stored id of Table */
    char USTF[URNOLEN+1];
    char UAFT[URNOLEN+1];
    char UEQU[URNOLEN+1];
    char FCCO[FCCOLEN+1];
    char UTPL[UTPLLEN+1];
    char DETY[DETYLEN+1];
    char ACFR[DATELEN+1];
    char ACTO[DATELEN+1];
} JOBREC;

typedef struct
{
    char URNO[URNOLEN+1];
    char FLNO[FLNOLEN+1];
    char STOD[DATELEN+1];
    char STOA[DATELEN+1];
    char REGN[REGNLEN+1];
    char DES3[COTYLEN+1];
    char ORG3[COTYLEN+1];
    char ETDI[DATELEN+1];
    char ETAI[DATELEN+1];
    char GTA1[GATELEN+1];
    char GTD1[GATELEN+1];
    char ADID[ADIDLEN+1];
    char PSTA[PSTALEN+1];
    char PSTD[PSTALEN+1];
    char BLT1[BELTLEN+1];
    char OFBL[DATELEN+1];
    char ONBL[DATELEN+1];
    char TGA1[TGADLEN+1];
    char TGD1[TGADLEN+1];
    char TTYP[TTYPLEN+1];
    char FTYP[FTYPLEN+1];
    char ACT3[COTYLEN+1];
    char RKEY[URNOLEN+1];
    char TURN[URNOLEN+1];      /* Transit Flight URNO, do not exist in AFTTAB */
    char TIFD[DATELEN+1];
    char FLDA[DATELEN+1];
    char ALC3[ALC3LEN+1];
} AFTREC;

/* For mapping of Status name from iTrek Client with iTrek Server(Status Mgr) */
/* This mapping setting is originated from itkhdl.cfg file */
typedef struct
{
    char iTC_name[24];
    char iTS_name[24];
} STAREC; 

typedef struct
{
    int URNO;
    int TURN;
} FLTDAT;

/* record structure for table STFTAB = Staff Table */
typedef struct
{
    char PENO[PENOLEN+1];
    char FINM[NAMELEN+1];
    char LANM[NAMELEN+1];
    char URNO[URNOLEN+1];
} STFREC;

int debug_level = TRACE;
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
STAREC *prgStatName;                   /* For Status Name of Status Mgr */
static ITEM  *prgItem      = NULL;             /* The queue item pointer  */
static EVENT *prgEvent     = NULL;             /* The event pointer       */
static int   igItemLen     = 0;                /* length of incoming item */
static EVENT *prgOutEvent  = NULL;
static FILE *prgTrackEventFile = NULL;
FLTDAT rgFltDat[MAX_FLIGHTS];                       /* To record which flight are transit flight */
BOOL bgUpdStatus = FALSE;
BOOL bgJobConfirm = FALSE;
BOOL bgTrackEvent = FALSE;
BOOL bgFwdDepCPM = FALSE;

static char cgProcessName[20] = "\0";
static char cgConfigFile[512] = "\0";
static char cgConfigOfficerFile[512] = "\0";
static char cgHopo[8] = "\0";                 /* default home airport    */
static char cgTabEnd[8] ="\0";                /* default table extension */

static int igFLIGHT;
static int igSQLHDL;
static int igITKREP;
static int igITKHDL;
static int rgITKHDL[3];

/** MQ Process **/
static int igITKWMA;
static int igITKWMJ;
static int igITKWMJ ;
static int igITKCPM;
static int igITKREQ;
static int igITKRPT;
static int igITKMSG;

static int igIdxATA;
static int igIdxATD;
static int igNumFlights = 0;
static int igNumdayInDB = 0;
static int igNumStatus = 0;
static long lgEvtCnt = 0;
static char cgBulkTimeframe[20] = "\0";
static char cgOnlineTimeframe[20] = "\0";
static char cgSendPrio[10] = "\0";
static char cgJobTemplates[100] = "\0";
static char pcgStatusAirlines[200] = "\0";
static char pcgHssName[3][512];
static char pcgJobConfirmStatus[12];
char pcgExcludeFtyp[40];

int igXmlCapSize = 0;

static char pcgBulkStartTime[NUM_BULK_TIME][20];
static char pcgBulkEndTime[NUM_BULK_TIME][20];
static long lgBulkStartTime[NUM_BULK_TIME];
static long lgBulkEndTime[NUM_BULK_TIME];

#define FIELDS_LEN 1000
/*char pcgSqlInsField_ITRTAB[FIELDS_LEN] = "STNO,RTNO,URNO,UAFT,ULDI,TPTY,DEST,ICPM,SDDT,SDBY,SDRM,RCDT,RCBY,RCRM";*/
char pcgSqlInsField_ITRTAB[FIELDS_LEN] = "STNO,RTNO,URNO,UAFT,ULDI,STTY,RTTY,DEST,ICPM,SDDT,SDBY,SDRM,RCDT,RCBY,RCRM";
char pcgSqlUpdSField_ITRTAB[FIELDS_LEN] = "STNO,SDDT,SDBY,SDRM";
char pcgSqlUpdRField_ITRTAB[FIELDS_LEN] = "RTNO,RCDT,RCBY,RCRM";

char pcgSqlInsField_IMGTAB[FIELDS_LEN] = "URNO,SDID,SDNM,SDFN,SDLN,MGBY,SDDT";
char pcgSqlInsField_IMRTAB[FIELDS_LEN] = "URNO,MGNO,TOID,TOFN,TOLN,RCID,RCFN,RCLN,RCDT";


char pcgDepartureFields[FIELDS_LEN] = "TRIM(URNO), TRIM(RKEY), TRIM(FLNO), TRIM(STOD), TRIM(STOA)," \
                                      "TRIM(REGN), TRIM(DES3), TRIM(ORG3), TRIM(ETDI), TRIM(ETAI)," \
                                      "TRIM(GTA1), TRIM(GTD1), TRIM(ADID), TRIM(PSTA), TRIM(PSTD)," \
                                      "TRIM(BLT1), TRIM(OFBL), TRIM(ONBL), TRIM(TGA1), TRIM(TGD1)," \
                                      "TRIM(TTYP), TRIM(ACT3), TRIM(FTYP) ";

char pcgMonths[12][4] = { "Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec" };
 
/* Mei */
char pcgDefnOfficer[NUM_OFFICER_TYPE][OFFICER_FCODE_LEN];
char pcgOfficerTitle[NUM_OFFICER_TYPE][OFFICER_TITLE_LEN] = { "AO", "ACC", "ADM", "AADMIN", "BO",
                                                              "BCC", "BDM", "BADMIN", "SOCC", "LCC",
                                                              "AST", "SECURE", "AIC" };

/* For Handling Agent */
static REC_DESC rgDbReturn;
static char pcgHandledAirlines[5120];
static char pcgReqdAgent[100];

char pcgXmlStr[START_DATA_SIZE];

/** For DLS Format **/
char cgDLSDelimitter;
char pcgBreakTag[8] = "\n";

/******************************************************************************/
/* Function prototypes                                                        */
/******************************************************************************/
static int  Init_Process();
static int ReadConfigEntry(char *pcpFname, char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer);
static int ReadConfigLine(char *pcpFname, char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer);


static int  Reset(void);                       /* Reset program          */
static void Terminate(int ipSleep);            /* Terminate program      */
static void HandleSignal(int);                 /* Handles signals        */
static void HandleErr(int);                    /* Handles general errors */
static void HandleQueErr(int);                 /* Handles queuing errors */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/
static void HandleBulkData( int ipBulkType );

static int  HandleData(EVENT *prpEvent);       /* Handles event data     */
static void HandleRequest(char *pclFields, char *pclData);
static int GetElementValue(char *pclDest, char *pclOrigData, char *pclBegin, char *pclEnd);


/*Created .. Thanooj*/
static void SendAftBulkData();
static void SendJobBulkData();
static void SendEquBulkData();
static int  TriggerAction(char *pcpTableName);
static int  WaitAndCheckQueue(int ipTimeout, int ipModId, ITEM **prpItem);
static int  CheckQueue(int ipModId, ITEM **prpItem);
static int  GetRowLength(char *pcpHopo, char *pcpTana, char *pcpFieldList,long *pipLen);
static int  GetFieldLength(char *pcpHopo, char *pcpTana, char *pcpFina,long *pipLen);
static char* rtrim(char *pcpString);
static int getOrPutDBData(char *pcpSelection, char *pcpData, unsigned int ipMode);
static void getAndSendShowDLS(char *clData);
static void getAndSendAllOK2L(char *clData);
static void getAndSendTask(char *clData);
static void getAndSendStaff( char *pcpData );
static void HandleFlightOnlineData(char *pcpFields, char *pcpData, char *pclCmd, char *pcpSelection);
static void HandleJobOnlineData(char *pcpFields, char *pcpData, char *pcpCmd,char *pcpSelection);
static void HandleEquipOnlineData(char *pcpJobUrno);
static void HandleStaffOnlineData(char *pcpFields, char *pcpData, char *pclCmd,char *pcpSelection);
static void HandleATDUpdate(long,char *);

/* MEI: Implemented for Phase II of iTrek */
static void HandleATDMQ(char *pclFields, char *pclData);
static void HandleStatusUpd(long,char *);
static void HandleTripInfo(long,char *);

static void HandleMSGMQ(char *pclFields, char *pclData);
static void HandleMessaging(long, char *);
static void HandleMessageQuery(long, char *);
static void HandleMessageLogRequest(long, char *);
static void HandleOneMessage(long lpMqrUrno, char *pcpData);
int BulkMessageSending( char *pcpXmlType, int ipDuration );
int MessageSending( char *pcpXmlType, char *pcpMsgUrno, char *pcpRcvUrno );
void SendStatusXML(char *pcpSelection, char *pcpData, char *pcpTw_start);

static void HandleLoadQuery(char *pcpFields, char *pcpData );
static void HandleOnlineCPM(char *pcpSelection);
static void HandleOnlineDLS(char *pcpSelection);   /* v2.0 */

void InitStatusConfig();
void InitOfficerType();
int GetStatusNameIndex( const char *pcpName, int *pipStatusNameIndex, BOOL bpFromiTC );
BOOL GetEmpType( char *pcpEmpType, char *FuncCode );
int GetStaffName( char *pcpSrchStr, char *pcpResStr, char *pcpFirstName, char *pcpLastName, int ipSrchType );
int GetUserStat( char *pcpUser, char *pcpStat );
int GetSortOrder( char *pcpOrderBy, char *pcpSortOrder );
int PackFlightXml( char *pcpXmlStr, AFTREC rpAftrec, int ipPackType );
int PackJobXml( char *pcpXmlStr, JOBREC rpJobrec, int ipPackType );
int PackStaffXml( STFREC rpStfrec, char cpReas, char *pcpXmlStr );
int GetTransitFlight( AFTREC, AFTREC * );
int NewFlight( char *pcpUAFT, char *pcpTURN );
int ChkTransit( int ipUrno );
void SendFlightTURN ( char *pcpUAFT, char *pcpTURN );
int TrimUrno(char *pcpString);
int UpdateNLTrip( char *pcpRole, char *pcpUAFT, char *pcpTripChange );
int StoreJob( char *pcpUJOB, char *pcpACTO );
int CleanJob( int ipNumdays );
void GetFlight( char *pcpUaft, char *pcpFname, char *pcpLname );
void GetTask();
void GetHandledAirlines ();
int ReadFlightRecord( char *pcpUaft, AFTREC * );

/** For DLS functionalities **/
void FormatDLS ( char *pcpTxt1, char *pcpFinalTxt );
void getString( char *pcpInStr, char *pcpOutStr, char cgStopChr );
int getLine( char *pcpInStr, char *pcpOutStr, char cgStopChr, int ipInIdx, int ipNumLine );
int getField( char *pcpInStr, char *pcpOutStr, char cgStopChr, int ipNumField );
void cutString( char *pcpInStr, char *pcpOutStr, int ipMaxChrPerRow );
void printTelex( char *pcpTelex );

void TrackEvent(char *fmt, ...);
/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
    int ilRc = RC_SUCCESS;          /* Return code          */
    int ilCnt = 0;
    int ilOldDebugLevel = 0;



    INITIALIZE;         /* General initialization   */

    strcpy(cgProcessName,argv[0]);

    dbg(TRACE,"MAIN: version <%s>",sccs_version);

    /* Attach to the MIKE queues */
    do
    {
        ilRc = init_que();
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        }/* end of if */
    }while((ilCnt < 10) && (ilRc != RC_SUCCESS));

    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
        sleep(60);
        exit(1);
    }else{
        dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
    }/* end of if */

    do
    {
        ilRc = init_db();
        if (ilRc != RC_SUCCESS)
        {
            check_ret(ilRc);
            dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        } /* end of if */
    } while((ilCnt < 10) && (ilRc != RC_SUCCESS));

    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
        sleep(60);
        exit(2);
    }else{
        dbg(TRACE,"MAIN: init_db() OK!");
    } /* end of if */

    /* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */
    sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
    dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
    ilRc = TransferFile(cgConfigFile);
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */

    sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
    dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
    ilRc = TransferFile(cgConfigFile);
    if(ilRc != RC_SUCCESS)
    { 
        dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */

    ilRc = SendRemoteShutdown(mod_id);
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    } /* end of if */

    if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
        dbg(DEBUG,"MAIN: waiting for status switch ...");
        HandleQueues();
    }/* end of if */

    if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
        dbg(TRACE,"MAIN: initializing ...");
        ilRc = Init_Process();
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"Init_Process: init failed!");
        } /* end of if */
    } 
    else 
    {
        Terminate(30);
    }/* end of if */

    InitStatusConfig(); /* MEI */
    InitOfficerType(); /* MEI */

    if( mod_id != rgITKHDL[0] )
        bgTrackEvent = FALSE;

    dbg(TRACE,"=====================");
    dbg(TRACE,"MAIN: initializing OK");
    dbg(TRACE,"=====================");
    
    /*
     When process starts, Prepare the Bulk Flight(AFT) message, Bulk JOB message & send to iTrek MQ.
     HandleBulkData() function contains both HandleFlightBulkData & HandleJOBBulkData functions.
     */
    GetHandledAirlines();
    HandleBulkData( TYPE_BULK_JOB );
    HandleBulkData( TYPE_BULK_AFT );

    for(;;)
    {
        ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* Acknowledge the item */
        ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
        if( ilRc != RC_SUCCESS ) 
        {
            /* handle que_ack error */
            HandleQueErr(ilRc);
        } /* fi */

        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */

        prgEvent = (EVENT *) prgItem->text;
                
        if( ilRc == RC_SUCCESS )
        {
            
            lgEvtCnt++;

            switch( prgEvent->command )
            {
            case    HSB_STANDBY :
                ctrl_sta = prgEvent->command;
                HandleQueues();
                break;  
            case    HSB_COMING_UP   :
                ctrl_sta = prgEvent->command;
                HandleQueues();
                break;  
            case    HSB_ACTIVE  :
                ctrl_sta = prgEvent->command;
                break;  
            case    HSB_ACT_TO_SBY  :
                ctrl_sta = prgEvent->command;
                /* CloseConnection(); */
                HandleQueues();
                break;  
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate(1);
                break;  
            case    HSB_STANDALONE  :
                ctrl_sta = prgEvent->command;
                ResetDBCounter();
                break;  
            case    REMOTE_DB :
                /* ctrl_sta is checked inside */
                HandleRemoteDB(prgEvent);
                break;
            case    SHUTDOWN    :
                /* process shutdown - maybe from uutil */
                Terminate(1);
                break;
                    
            case    RESET       :
                ilRc = Reset();
                break;
                    
            case    EVENT_DATA  :
                if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
                {
                    ilRc = HandleData(prgEvent);
                    if(ilRc != RC_SUCCESS)
                    {
                        HandleErr(ilRc);
                    }/* end of if */
                }else{
                    dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
                    DebugPrintItem(TRACE,prgItem);
                    DebugPrintEvent(TRACE,prgEvent);
                }/* end of if */
                break;
                

            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;
            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;
            default         :
                dbg(TRACE,"MAIN: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */

        } else {
            /* Handle queuing errors */
            HandleQueErr(ilRc);
        } /* end else */
        
    } /* end for */
    
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int ReadConfigEntry(char *pcpFname, char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer)
{
    int ilRc = RC_SUCCESS;

    char clSection[124] = "\0";
    char clKeyword[124] = "\0";

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRc = iGetConfigEntry(pcpFname,clSection,clKeyword,
               CFG_STRING,pcpCfgBuffer);
    if(ilRc != RC_SUCCESS){
        dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
    } 
    else{
        dbg(DEBUG,"Config Entry <%s>,<%s>:<%s> found in %s",
        clSection, clKeyword ,pcpCfgBuffer, cgConfigFile);
    }/* end of if */
    return ilRc;
}

static int ReadConfigLine(char *pcpFname, char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer)
{
    FILE *rlFptr;
    BOOL ilFound = FALSE;
    BOOL ilStartFound = FALSE;
    char pclLine[2048];
    char pclStartSec[32];
    char pclEndSec[32];
    char *pclToken;

    rlFptr = fopen( pcpFname, "r" );
    if( rlFptr == NULL )
    {
        dbg(TRACE,"Error in reading Config Entry errno = %d<%s>", errno, strerror(errno) );
        return RC_FAIL;
    }
    sprintf( pclStartSec, "[%s]", pcpSection );
    sprintf( pclEndSec, "[%s_END]", pcpSection );

    while( !feof(rlFptr) )
    {
        if( fgets( pclLine, sizeof(pclLine), rlFptr ) == NULL )
            break;
        if( !strncmp(pclLine, pclEndSec, strlen(pclEndSec)) )
           break;
        if( !strncmp(pclLine, pclStartSec, strlen(pclStartSec)) )
        {
           ilStartFound = TRUE;
           continue;
        }
        if( ilStartFound == FALSE )
           continue;;
        pclToken = (char *)strtok( pclLine, " " );
        if( pclToken == NULL )
            continue;
        if( strncmp(pclToken, pcpKeyword, strlen(pcpKeyword)) )
            continue;
      
        pclToken = (char *)strtok( NULL, "'" );
        pclToken = (char *)strtok( NULL, "'" );
        memcpy( pcpCfgBuffer, pclToken, strlen(pclToken) );
        pcpCfgBuffer[strlen(pclToken)] = '\0';
        ilFound = TRUE;
        break;
    }
    fclose(rlFptr);
    if( ilFound == FALSE )
        return RC_FAIL;
    return RC_SUCCESS;
}

static int Init_Process()
{
    int  ilRc = RC_SUCCESS;         /* Return code */
    char clSection[64] = "\0";
    char clKeyword[64] = "\0";
    int ilOldDebugLevel = 0;
    long pclAddFieldLens[12];
    char pclAddFields[256] = "\0";
    char pclSqlBuf[2560] = "\0";
    char pclSelection[1024] = "\0";
    short slCursor = 0;
    short slSqlFunc = 0;
    char  clBreak[24] = "\0";
    char  clTmpStr[100] = "\0";
    char  pclFileLoc[50] = "\0";
    char *pclFunc = "Init_Process";


    if(ilRc == RC_SUCCESS)
    {
        /* read HomeAirPort from SGS.TAB */
        ilRc = tool_search_exco_data("SYS", "HOMEAP", cgHopo);
        if (ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"<%s> EXTAB,SYS,HOMEAP not found in SGS.TAB", pclFunc);
            Terminate(30);
        } else {
            dbg(TRACE,"<%s> home airport <%s>", pclFunc,cgHopo);
        }
    }

    
    if(ilRc == RC_SUCCESS)
    {

        ilRc = tool_search_exco_data("ALL","TABEND", cgTabEnd);
        if (ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"<%s> EXTAB,ALL,TABEND not found in SGS.TAB", pclFunc);
            Terminate(30);
        } else {
            dbg(TRACE,"<%s> table extension <%s>", pclFunc,cgTabEnd);
        }
    }

    if (ilRc == RC_SUCCESS)
    {
        igITKWMA = tool_get_q_id( "itkwma" );
        dbg(TRACE,"<%s> igITKWMA <%d>", pclFunc, igITKWMA );

        igITKWMJ = tool_get_q_id( "itkwmj" );
        dbg(TRACE,"<%s> igITKWMJ <%d>", pclFunc, igITKWMJ );

        igITKREQ = tool_get_q_id( "itkreq" );
        dbg(TRACE,"<%s> igITKREQ <%d>", pclFunc, igITKREQ );

        igITKCPM = tool_get_q_id( "itkcpm" );
        dbg(TRACE,"<%s> igITKCPM <%d>", pclFunc, igITKCPM );

        igITKMSG = tool_get_q_id( "itkmsg" );
        dbg(TRACE,"<%s> igITKMSG <%d>", pclFunc, igITKMSG );

        igITKRPT = tool_get_q_id( "itkrpt" );
        dbg(TRACE,"<%s> igITKRPT <%d>", pclFunc, igITKRPT );

        igFLIGHT = tool_get_q_id( "flight" );
        dbg(TRACE,"<%s> igFLIGHT <%d>", pclFunc, igFLIGHT );

        ilRc = ReadConfigEntry( cgConfigFile, "MAIN","SEND_TO_MODID_SQLHDL",clTmpStr);
        igSQLHDL = atoi(clTmpStr);
        dbg(TRACE,"<%s> igSQLHDL <%d>", pclFunc, igSQLHDL );

        igITKREP = tool_get_q_id( "itkrep" );
        dbg(TRACE,"<%s> ITKREP ID <%d>", pclFunc, igITKREP );

        rgITKHDL[0] = tool_get_q_id( "itkhdl" );
        rgITKHDL[1] = tool_get_q_id( "itkhdl1" );
        rgITKHDL[2] = tool_get_q_id( "itkhdl2" );
        dbg(TRACE,"<%s> ITKHDLs ID 1:<%d>   2:<%d>", pclFunc, rgITKHDL[1], rgITKHDL[2] );


        ilRc = ReadConfigEntry( cgConfigFile, "MAIN","JOB_TEMPLATES",cgJobTemplates);
        dbg(TRACE,"<%s> cgJobTemplates <%s>",pclFunc, cgJobTemplates );

        ilRc = ReadConfigEntry( cgConfigFile, "MAIN","SEND_PRIO",cgSendPrio);
        dbg(TRACE,"<%s> cgSendPrio <%s>", pclFunc, cgSendPrio );


        /* Getting bulk job/flight searching time range */
        memset( lgBulkStartTime, 0, sizeof(lgBulkStartTime) );
        memset( lgBulkEndTime,   0, sizeof(lgBulkEndTime)   );
        ilRc = ReadConfigEntry( cgConfigFile, "BULK_DATA","BULK_START_ARR_FLIGHT",clTmpStr);
        lgBulkStartTime[ARR_FLIGHT] = atoi(clTmpStr) * 3600;
        ilRc = ReadConfigEntry( cgConfigFile, "BULK_DATA","BULK_END_ARR_FLIGHT",clTmpStr);
        lgBulkEndTime[ARR_FLIGHT] = atoi(clTmpStr) * 3600;
        dbg(TRACE,"<%s> ARR_FLIGHT Bulk Start Time <%d> sec Bulk End Time <%d> sec", pclFunc, 
                           lgBulkStartTime[ARR_FLIGHT], lgBulkEndTime[ARR_FLIGHT] );

        ilRc = ReadConfigEntry( cgConfigFile, "BULK_DATA","BULK_START_DEP_FLIGHT",clTmpStr);
        lgBulkStartTime[DEP_FLIGHT] = atoi(clTmpStr) * 3600;
        ilRc = ReadConfigEntry( cgConfigFile, "BULK_DATA","BULK_END_DEP_FLIGHT",clTmpStr);
        lgBulkEndTime[DEP_FLIGHT] = atoi(clTmpStr) * 3600;
        dbg(TRACE,"<%s> DEP_FLIGHT Bulk Start Time <%d> sec Bulk End Time <%d> sec", pclFunc, 
                           lgBulkStartTime[DEP_FLIGHT], lgBulkEndTime[DEP_FLIGHT] );

        ilRc = ReadConfigEntry( cgConfigFile, "BULK_DATA","BULK_START_JOB",clTmpStr);
        lgBulkStartTime[BLK_JOB] = atoi(clTmpStr) * 3600;
        ilRc = ReadConfigEntry( cgConfigFile, "BULK_DATA","BULK_END_JOB",clTmpStr);
        lgBulkEndTime[BLK_JOB] = atoi(clTmpStr) * 3600;
        dbg(TRACE,"<%s> BLK_JOB Bulk Start Time <%d> sec Bulk End Time <%d> sec", pclFunc, 
                           lgBulkStartTime[BLK_JOB], lgBulkEndTime[BLK_JOB] );


        /* MEI */
        ilRc = ReadConfigEntry( cgConfigFile, "MAIN","OFFICER_DEFN_FILE",pclFileLoc);
        sprintf(cgConfigOfficerFile,"%s/%s",getenv("CFG_PATH"), pclFileLoc);
        dbg(TRACE,"<%s> ConfigOfficerFile <%s>", pclFunc, cgConfigOfficerFile );

        ilRc = ReadConfigEntry( cgConfigFile, "MAIN","DEBUG_LEVEL",pclSelection);
        debug_level = TRACE;
        if( !strncmp( pclSelection, "DEBUG", 5 ) )
            debug_level = DEBUG;
        dbg(TRACE,"<%s> debug_level <%s><%d>",pclFunc, pclSelection, debug_level);

        ilRc = ReadConfigEntry( cgConfigFile, "MAIN","XML_CAP_SIZE",pclSelection);
        igXmlCapSize = atoi( pclSelection );
        if( igXmlCapSize <= 0 )
            igXmlCapSize = 1 * 1024 * 1024;  /* Default to 1MB */
        dbg(TRACE,"<%s> XML_CAP_SIZE <%s><%d>",pclFunc, pclSelection, igXmlCapSize);
    
        ilRc = ReadConfigEntry( cgConfigFile, "MAIN","NUMDAY_IN_ITKJOB",clTmpStr);
        igNumdayInDB = atoi(clTmpStr);
        dbg(TRACE,"<%s> NUMDAY_IN_ITKJOB <%s><%d>",pclFunc, clTmpStr, igNumdayInDB);

        ilRc = ReadConfigEntry( cgConfigFile, "FLIGHT_INFO","EXCLUDE_FTYP",pcgExcludeFtyp );
        if( ilRc != RC_SUCCESS )
            strcpy( pcgExcludeFtyp, "'T','G'" );
        dbg(TRACE,"<%s> FlightExcludeFtyp:<%s>", pclFunc, pcgExcludeFtyp );

        ilRc = ReadConfigEntry( cgConfigFile, "FLIGHT_INFO","HANDLING_AGENTS",pcgReqdAgent );
        if( ilRc != RC_SUCCESS )
            strcpy( pcgReqdAgent, "SATS" );
        dbg(TRACE,"<%s> Handling Agents:<%s>", pclFunc, pcgReqdAgent );

    }
    bgFwdDepCPM = FALSE; 
    ReadConfigEntry( cgConfigFile, "ONLINE_DATA","FORWARD_DEP_CPM",pclSelection);
    if( !strcmp(pclSelection,"TRUE") )
        bgFwdDepCPM = TRUE;
    dbg(TRACE,"<%s> FORWARD_DEP_CPM <%s><%d>",pclFunc, pclSelection, bgFwdDepCPM);

    if(ilRc == RC_SUCCESS)
    {
        ilRc = CEDAArrayInitialize(20,20);
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"<%s> CEDAArrayInitialize failed <%d>", pclFunc, ilRc);
        }/* end of if */
    }/* end of if */

    if( mod_id == rgITKHDL[0] )
        ilRc = TriggerAction("STFTAB");

    if (ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"<%s> failed", pclFunc);
    }

    return(ilRc);
    
} /* end of initialize */

/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
    int ilRc = RC_SUCCESS;              /* Return code */
    
    dbg(TRACE,"Reset: now resetting");
    
    return ilRc;
    
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
    /* unset SIGCHLD ! DB-Child will terminate ! */

    dbg(TRACE,"Terminate: now DB logoff ...");

    signal(SIGCHLD,SIG_IGN);

    logoff();

    dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);


    dbg(TRACE,"Terminate: now leaving ...");

    fclose(outp);

    sleep(ipSleep < 1 ? 1 : ipSleep);
    
    exit(0);
    
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/

static void HandleSignal(int pipSig)
{
    dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);

    switch(pipSig)
    {
    default :
        Terminate(1);
        break;
    } /* end of switch */

    exit(1);
    
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
    dbg(TRACE,"HandleErr: <%d> : unknown ERROR",pipErr);

    return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    switch(pipErr) {
    case    QUE_E_FUNC  :   /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :   /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;
    case    QUE_E_SEND  :   /* Error using msgsnd */
        dbg(TRACE,"<%d> : msgsnd failed",pipErr);
        break;
    case    QUE_E_GET   :   /* Error using msgrcv */
        dbg(TRACE,"<%d> : msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX   :
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE  :
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES  :
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        break;
    case    QUE_E_RESP  :   /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;
    case    QUE_E_FULL  :
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG :   /* No message on queue */
        dbg(TRACE,"<%d> : no messages on queue",pipErr);
        break;
    case    QUE_E_INVORG    :   /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :   /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;
    default         :   /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
    } /* end switch */
         
    return;

} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
    int ilRc = RC_SUCCESS;          /* Return code */
    int ilBreakOut = FALSE;
    
    do
    {
        ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text; 

        if( ilRc == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRc != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRc);
            } /* fi */
        
            switch( prgEvent->command )
            {
            case    HSB_STANDBY :
                ctrl_sta = prgEvent->command;
                break;  
    
            case    HSB_COMING_UP   :
                ctrl_sta = prgEvent->command;
                break;  
    
            case    HSB_ACTIVE  :
                ctrl_sta = prgEvent->command;
                ilBreakOut = TRUE;
                break;  

            case    HSB_ACT_TO_SBY  :
                ctrl_sta = prgEvent->command;
                break;  
    
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate(1);
                break;  
    
            case    HSB_STANDALONE  :
                ctrl_sta = prgEvent->command;
                ResetDBCounter();
                ilBreakOut = TRUE;
                break;  

            case    REMOTE_DB :
                /* ctrl_sta is checked inside */
                HandleRemoteDB(prgEvent);
                break;

            case    SHUTDOWN    :
                Terminate(1);
                break;
                        
            case    RESET       :
                ilRc = Reset();
                break;
                        
            case    EVENT_DATA  :
                dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
                    
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;

            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;

            default         :
                dbg(TRACE,"HandleQueues: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        } else {
            /* Handle queuing errors */
            HandleQueErr(ilRc);
        } /* end else */
    } while (ilBreakOut == FALSE);

            ilRc = Init_Process();
            if(ilRc != RC_SUCCESS)
            {
                dbg(TRACE,"InitItkhdl: init failed!");
            } 
     

} /* end of HandleQueues */
    
/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(EVENT *prpEvent)
{
    int ilRc = RC_SUCCESS;         
    int ilCmd = 0;
    int ilUpdPoolJob = TRUE;
    int ilNumdays;

    BC_HEAD *prlBchead       = NULL;
    CMDBLK  *prlCmdblk       = NULL;
    char    *pclSelection    = NULL;
    char    *pclFields       = NULL;
    char    *pclData         = NULL;
    char    *pclRow          = NULL;
    char    clUrnoList[2400];
    char    clTable[34];

    prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
    prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
    pclSelection = prlCmdblk->data;
    pclFields    = pclSelection + strlen(pclSelection) + 1;
    pclData      = pclFields + strlen(pclFields) + 1;


    strcpy(clTable,prlCmdblk->obj_name);

    dbg(TRACE,"========== START <%10.10d> ==========",lgEvtCnt);

    /****************************************/
    DebugPrintBchead(TRACE,prlBchead);
    DebugPrintCmdblk(TRACE,prlCmdblk);
    dbg(TRACE,"Command: <%s>",prlCmdblk->command);
    dbg(TRACE,"Tw_start: <%s>",prlCmdblk->tw_start);
    dbg(TRACE,"originator follows event = %p ",prpEvent);

    dbg(TRACE,"originator<%d>",prpEvent->originator);
    dbg(TRACE,"selection follows Selection = %p ",pclSelection);
    dbg(TRACE,"selection <%s>",pclSelection);
    dbg(TRACE,"fields    <%s>",pclFields);
    dbg(TRACE,"data      <%s>",pclData);
    /****************************************/

    TrackEvent( "CMD <%s> TW_START <%s> TW_END <%s>", prlCmdblk->command, prlCmdblk->tw_start, prlCmdblk->tw_end );
    TrackEvent( "ORG <%d> SEL <%s>", prpEvent->originator, pclSelection );
    TrackEvent( "FLD <%s>", pclFields );
    TrackEvent( "DAT <%s>\n===========", pclData );


    /* From NTISCH */
    if( !strcmp(prlCmdblk->command,"BLKJ") )
        HandleBulkData( TYPE_BULK_JOB );
    else if( !strcmp(prlCmdblk->command,"BLKA") )
        HandleBulkData( TYPE_BULK_AFT );
    else if (strcmp(prlCmdblk->command,"CDB") == 0)
    {
        CleanJob( igNumdayInDB );
        GetHandledAirlines();
    }
    else if (strcmp(prlCmdblk->command,"TIME") == 0)
        GetHandledAirlines();

    /* This is sent from ACTION handler when there are any update to AFTTAB and JOBTAB */
    /* From ACTION process */
    if ((strcmp(prlCmdblk->obj_name,"UPDJOB") == 0) || (strcmp(prlCmdblk->obj_name,"JOBTAB") == 0))
    {
        HandleJobOnlineData(pclFields, pclData, prlCmdblk->command, pclSelection);
    }
    /* Handle online-flight updates */
    if (strcmp(prlCmdblk->obj_name,"AFTTAB") == 0)
    {
        HandleFlightOnlineData(pclFields, pclData, prlCmdblk->command, pclSelection);
    }

    /*  This is to insert/update/delete staff info from AATArray */
        /* From ACTION process */
    /*if((strcmp(prlCmdblk->obj_name,"STFTAB") == 0) && (strcmp(prlCmdblk->command,"IRT") == 0 || 
            strcmp(prlCmdblk->command,"URT") == 0 || strcmp(prlCmdblk->command,"DRT") == 0))
    {
        CEDAArrayEventUpdate(prgEvent);
    }*/

    /* Upd for Status Mgr is successful */
    if(!strcmp(prlCmdblk->obj_name,"OSTTAB") && !strcmp(prlCmdblk->command,"URT") )
    {
        /* Dun need to do anything now. 26-NOV-08 */
        ;
        /*SendStatusXML(pclSelection, pclData, prlCmdblk->tw_start);*/
    }

    if (strcmp(prlCmdblk->obj_name,"STFTAB") == 0)
    {
        HandleStaffOnlineData(pclFields, pclData, prlCmdblk->command, pclSelection);
    }

    /* Send from iTrek Client via MQ handler(ITKWMU) */ 
    /* Time OFBS timing update by Client on AFTTAB */
    if (strcmp(prlCmdblk->command,"ATD") == 0)
        HandleATDMQ(pclFields, pclData);

    /* Send from iTrek Client via MQ handler(ITKREQ) */ 
    /* request date */
    else if(strcmp(prlCmdblk->command,"REQ") == 0)
        HandleRequest(pclFields, pclData);
    
    /* Send from iTrek Client via MQ handler(ITKMSG) */ 
    /* MEI implementation on Phase II of iTrek */
    else if(strcmp(prlCmdblk->command,"MSG") == 0)
        HandleMSGMQ(pclFields, pclData);

    /* Send from iTrek Client via MQ handler(ITKCPM) */ 
    else if(strcmp(prlCmdblk->command,"LOA") == 0)
        HandleLoadQuery(pclFields, pclData );

    else if(strcmp(prlCmdblk->command,"CPM") == 0)
        HandleOnlineCPM(pclSelection);
   
    else if(strcmp(prlCmdblk->command,"DLS") == 0)
        HandleOnlineDLS(pclSelection);
    dbg(TRACE,"==========  END  <%10.10d> ==========",lgEvtCnt);

    return(RC_SUCCESS);
    
} /* end of HandleData */


/* Function HandleBulkData() will be called whenever process restarts & as well as when gets a request for Bulk messages from scheduler.
*/
static void HandleBulkData( int ipBulkType )
{
    int ili;
    char pclNowTime[20] = "\0";
    char pclTmpStr[1024];
    char *pclFunc = "HandleBulkData";

    if( mod_id != rgITKHDL[0] )
        return;

    dbg( TRACE,"<%s> ------------------ START ------------------", pclFunc );

    memset( pcgBulkStartTime, 0, sizeof(pcgBulkStartTime) );
    memset( pcgBulkEndTime,   0, sizeof(pcgBulkEndTime)   );
    
    GetServerTimeStamp("UTC",1,0,pclNowTime);
    for( ili = 0; ili < NUM_BULK_TIME; ili++ )
    {
        strcpy( pcgBulkStartTime[ili], pclNowTime );
        strcpy( pcgBulkEndTime[ili], pclNowTime );
    }

    /* Arrival Flight */
    AddSecondsToCEDATime(pcgBulkStartTime[ARR_FLIGHT],lgBulkStartTime[ARR_FLIGHT],1);
    pcgBulkStartTime[ARR_FLIGHT][14] = '\0';
    AddSecondsToCEDATime(pcgBulkEndTime[ARR_FLIGHT],lgBulkEndTime[ARR_FLIGHT],1);
    pcgBulkEndTime[ARR_FLIGHT][14] = '\0';
    dbg(TRACE,"[ARR_FLIGHT] START: <%s>  END: <%s>", pcgBulkStartTime[ARR_FLIGHT], pcgBulkEndTime[ARR_FLIGHT] );

    /* Departure Flight */
    AddSecondsToCEDATime(pcgBulkStartTime[DEP_FLIGHT],lgBulkStartTime[DEP_FLIGHT],1);
    pcgBulkStartTime[DEP_FLIGHT][14] = '\0';
    AddSecondsToCEDATime(pcgBulkEndTime[DEP_FLIGHT],lgBulkEndTime[DEP_FLIGHT],1);
    pcgBulkEndTime[DEP_FLIGHT][14] = '\0';
    dbg(TRACE,"[DEP_FLIGHT] START: <%s>  END: <%s>", pcgBulkStartTime[DEP_FLIGHT], pcgBulkEndTime[DEP_FLIGHT] );

    /* Bulk Job */
    AddSecondsToCEDATime(pcgBulkStartTime[BLK_JOB],lgBulkStartTime[BLK_JOB],1);
    pcgBulkStartTime[BLK_JOB][14] = '\0';
    AddSecondsToCEDATime(pcgBulkEndTime[BLK_JOB],lgBulkEndTime[BLK_JOB],1);
    pcgBulkEndTime[BLK_JOB][14] = '\0';
    dbg(TRACE,"[BLK_JOB] START: <%s>  END: <%s>", pcgBulkStartTime[BLK_JOB], pcgBulkEndTime[BLK_JOB] );

    if( ipBulkType == TYPE_BULK_AFT )
        SendAftBulkData();
    else if( ipBulkType == TYPE_BULK_JOB )
    {
        /*sprintf( pclTmpStr, "SELECT URNO FROM AFTTAB WHERE "
                            "(((TIFD BETWEEN '%s' AND '%s') AND (ORG3 = 'SIN')) "
                            "OR ((TIFA BETWEEN '%s' AND '%s') AND (DES3='SIN'))) "
                            "OR (((STOD BETWEEN '%s' AND '%s') AND (ORG3 = 'SIN')) "
                            "OR ((STOA BETWEEN '%s' AND '%s') AND (DES3='SIN'))) "
                            "OR (((ETOD BETWEEN '%s' AND '%s') AND (ORG3 = 'SIN')) "
                            "OR ((ETOA BETWEEN '%s' AND '%s') AND (DES3='SIN'))) "
                            "AND FTYP NOT IN (%s) "
                            "AND ALC3 IN (SELECT ALC3 FROM ALTTAB WHERE URNO IN (SELECT ALTU FROM HAITAB WHERE HSNA IN (%s))) ",
                            pcgBulkStartTime[DEP_FLIGHT], pcgBulkEndTime[DEP_FLIGHT],
                            pcgBulkStartTime[ARR_FLIGHT], pcgBulkEndTime[ARR_FLIGHT], 
                            pcgBulkStartTime[DEP_FLIGHT], pcgBulkEndTime[DEP_FLIGHT],
                            pcgBulkStartTime[ARR_FLIGHT], pcgBulkEndTime[ARR_FLIGHT], 
                            pcgBulkStartTime[DEP_FLIGHT], pcgBulkEndTime[DEP_FLIGHT],
                            pcgBulkStartTime[ARR_FLIGHT], pcgBulkEndTime[ARR_FLIGHT], 
                            pcgExcludeFtyp, pcgReqdAgent ); */
        sprintf( pclTmpStr, "SELECT URNO FROM AFTTAB WHERE "
                            "((((TIFD BETWEEN '%s' AND '%s') AND (ORG3 = 'SIN')) "
                            "OR ((TIFA BETWEEN '%s' AND '%s') AND (DES3='SIN'))) "
                            "OR (((STOD BETWEEN '%s' AND '%s') AND (ORG3 = 'SIN')) "
                            "OR ((STOA BETWEEN '%s' AND '%s') AND (DES3='SIN'))) "
                            "OR (((ETOD BETWEEN '%s' AND '%s') AND (ORG3 = 'SIN')) "
                            "OR ((ETOA BETWEEN '%s' AND '%s') AND (DES3='SIN')))) "
                            "AND FTYP NOT IN (%s) ",
                            pcgBulkStartTime[DEP_FLIGHT], pcgBulkEndTime[DEP_FLIGHT],
                            pcgBulkStartTime[ARR_FLIGHT], pcgBulkEndTime[ARR_FLIGHT], 
                            pcgBulkStartTime[DEP_FLIGHT], pcgBulkEndTime[DEP_FLIGHT],
                            pcgBulkStartTime[ARR_FLIGHT], pcgBulkEndTime[ARR_FLIGHT], 
                            pcgBulkStartTime[DEP_FLIGHT], pcgBulkEndTime[DEP_FLIGHT],
                            pcgBulkStartTime[ARR_FLIGHT], pcgBulkEndTime[ARR_FLIGHT], 
                            pcgExcludeFtyp );
        SendJobBulkData(pclTmpStr);
        SendEquBulkData(pclTmpStr);
    }

    dbg( TRACE,"<%s> ------------------ END ------------------", pclFunc );
} /* HandleBulkData */


static void SendAftBulkData()
{
    AFTREC rlAftrec;
    AFTREC rlTAftrec;                       /* For Transit Record */
    int ilRc = 0;
    int ilRecordCount = 0;
    int ilIndex;
    int ili;
    char *bulkMessage;
    short slSqlFunc = 0;
    short slCursor = 0;
    short slCursor2 = 0;
    char pclDataArea[4000] = "\0";
    char clErr[1024] = "\0";
    char pclTmpStr[500] = "\0";
    char pclTmpXml[500] = "\0";
    char pclDemandType[DETYLEN+1] = "\0";
    char *pclFunc = "SendAftBulkData";
    char pclTmpSelection[1000] = "\0";
    char clDepartureSelection[1000] = "\0";


    /*** Actual SQL Queries ***/
    /*sprintf( clDepartureSelection, "SELECT %s FROM AFTTAB WHERE  (((TIFD BETWEEN '%s' AND '%s') "
                                   "AND (ORG3 = 'SIN')) OR ((TIFA BETWEEN '%s' AND '%s') AND (DES3='SIN'))) "
                                   "OR (((STOD BETWEEN '%s' AND '%s') AND (ORG3 = 'SIN')) "
                                   "OR ((STOA BETWEEN '%s' AND '%s') AND (DES3='SIN'))) "
                                   "OR (((ETOD BETWEEN '%s' AND '%s') AND (ORG3 = 'SIN')) "
                                   "OR ((ETOA BETWEEN '%s' AND '%s') AND (DES3='SIN'))) "
                                   "AND FTYP NOT IN (%s) "
                                   "AND ALC3 IN (SELECT ALC3 FROM ALTTAB WHERE URNO IN "
                                   "(SELECT ALTU FROM HAITAB WHERE HSNA IN (%s))) "
                                   "ORDER BY RKEY ", 
                                   pcgDepartureFields, 
                                   pcgBulkStartTime[DEP_FLIGHT], pcgBulkEndTime[DEP_FLIGHT],
                                   pcgBulkStartTime[ARR_FLIGHT], pcgBulkEndTime[ARR_FLIGHT], 
                                   pcgBulkStartTime[DEP_FLIGHT], pcgBulkEndTime[DEP_FLIGHT],
                                   pcgBulkStartTime[ARR_FLIGHT], pcgBulkEndTime[ARR_FLIGHT], 
                                   pcgBulkStartTime[DEP_FLIGHT], pcgBulkEndTime[DEP_FLIGHT],
                                   pcgBulkStartTime[ARR_FLIGHT], pcgBulkEndTime[ARR_FLIGHT], 
                                   pcgExcludeFtyp, pcgReqdAgent ); */
    sprintf( clDepartureSelection, "SELECT TRIM(ALC3), %s FROM AFTTAB WHERE  "
                                   "((((TIFD BETWEEN '%s' AND '%s') AND (ORG3 = 'SIN')) "
                                   "OR ((TIFA BETWEEN '%s' AND '%s') AND (DES3='SIN'))) "
                                   "OR (((STOD BETWEEN '%s' AND '%s') AND (ORG3 = 'SIN')) "
                                   "OR ((STOA BETWEEN '%s' AND '%s') AND (DES3='SIN'))) "
                                   "OR (((ETOD BETWEEN '%s' AND '%s') AND (ORG3 = 'SIN')) "
                                   "OR ((ETOA BETWEEN '%s' AND '%s') AND (DES3='SIN'))) )"
                                   "AND FTYP NOT IN (%s) ORDER BY RKEY",
                                   pcgDepartureFields, 
                                   pcgBulkStartTime[DEP_FLIGHT], pcgBulkEndTime[DEP_FLIGHT],
                                   pcgBulkStartTime[ARR_FLIGHT], pcgBulkEndTime[ARR_FLIGHT], 
                                   pcgBulkStartTime[DEP_FLIGHT], pcgBulkEndTime[DEP_FLIGHT],
                                   pcgBulkStartTime[ARR_FLIGHT], pcgBulkEndTime[ARR_FLIGHT], 
                                   pcgBulkStartTime[DEP_FLIGHT], pcgBulkEndTime[DEP_FLIGHT],
                                   pcgBulkStartTime[ARR_FLIGHT], pcgBulkEndTime[ARR_FLIGHT], 
                                   pcgExcludeFtyp );


    sprintf(pclTmpSelection, "SELECT COUNT(*) %s", strstr( clDepartureSelection, "FROM" ) ); 
    /**********************/

    igNumFlights = 0;
    memset( rgFltDat, 0, sizeof(FLTDAT) * MAX_FLIGHTS );

    dbg(DEBUG,"####### SendAftBulkData - START ##########" );
    dbg(DEBUG,"<%s> Count Query: <%s>",pclFunc,pclTmpSelection);

    /* ilItemCount = get_no_of_items(pcgDepartureFields);
    dbg(DEBUG,"<%s> Number of items: <%d>",pclFunc,ilItemCount);*/

    ilRc = getOrPutDBData(pclTmpSelection,pclDataArea,START);
    ilRecordCount = atoi(pclDataArea);
    dbg(TRACE,"<%s> No. of Flights: <%d> ------------",pclFunc,ilRecordCount);
    bulkMessage = (char *) calloc (ilRecordCount+30 , sizeof(pclTmpXml));


    strcpy(bulkMessage,"<FLIGHTS>\n");
    ilRecordCount = 0;
    dbg(DEBUG,"<%s> SqlBuf: <%s>",pclFunc,clDepartureSelection);
    slSqlFunc = START;
    slCursor = 0;

    while((ilRc = sql_if(slSqlFunc,&slCursor,clDepartureSelection,pclDataArea)) == DB_SUCCESS)
    {
        memset( &rlAftrec, 0, sizeof(rlAftrec) );
        ilRecordCount++;
        slSqlFunc = NEXT;

        /* Check Handling Agent */
        get_fld(pclDataArea,FIELD_1,STR,ALC3LEN,rlAftrec.ALC3); 
        sprintf( pclTmpStr, "%s;", rlAftrec.ALC3 );
        dbg(DEBUG,"<%s> =========== Rec No: <%d> ALC3 <%s> ========== ",pclFunc,ilRecordCount, rlAftrec.ALC3);
        if( !strstr( pcgHandledAirlines, pclTmpStr ) )
            continue;

        get_fld(pclDataArea,FIELD_2,STR,URNOLEN,rlAftrec.URNO);
        dbg(DEBUG,"<%s> URNO <%s> ",pclFunc, rlAftrec.URNO);

        if( strlen(rlTAftrec.URNO) >= 0 )
        {
            /* ignore record since record has been processed previously */
            if( !strcmp(rlTAftrec.URNO, rlAftrec.URNO) )
            {
                dbg(DEBUG, "<%s> URNO <%s> Record has been processed previously IGNORE!", pclFunc, rlAftrec.URNO );
                continue;
            }
        }

        get_fld(pclDataArea,FIELD_3,STR,URNOLEN,rlAftrec.RKEY); strip_special_char( rlAftrec.RKEY );
        get_fld(pclDataArea,FIELD_4,STR,FLNOLEN,rlAftrec.FLNO);  strip_special_char( rlAftrec.FLNO );
        get_fld(pclDataArea,FIELD_5,STR,DATELEN,rlAftrec.STOD);  UtcToLocalTimeFixTZ(rlAftrec.STOD); 
        get_fld(pclDataArea,FIELD_6,STR,DATELEN,rlAftrec.STOA);  UtcToLocalTimeFixTZ(rlAftrec.STOA); 
        get_fld(pclDataArea,FIELD_7,STR,REGNLEN,rlAftrec.REGN);  strip_special_char( rlAftrec.REGN );
        get_fld(pclDataArea,FIELD_8,STR,COTYLEN,rlAftrec.DES3);  strip_special_char( rlAftrec.DES3 );
        get_fld(pclDataArea,FIELD_9,STR,COTYLEN,rlAftrec.ORG3);  strip_special_char( rlAftrec.ORG3 );
        get_fld(pclDataArea,FIELD_10,STR,DATELEN,rlAftrec.ETDI);  UtcToLocalTimeFixTZ(rlAftrec.ETDI); 
        get_fld(pclDataArea,FIELD_11,STR,DATELEN,rlAftrec.ETAI);  UtcToLocalTimeFixTZ(rlAftrec.ETAI); 
        get_fld(pclDataArea,FIELD_12,STR,GATELEN,rlAftrec.GTA1); strip_special_char( rlAftrec.GTA1 );
        get_fld(pclDataArea,FIELD_13,STR,GATELEN,rlAftrec.GTD1); strip_special_char( rlAftrec.GTD1 );
        get_fld(pclDataArea,FIELD_14,STR,ADIDLEN,rlAftrec.ADID); strip_special_char( rlAftrec.ADID );
        get_fld(pclDataArea,FIELD_15,STR,PSTALEN,rlAftrec.PSTA); strip_special_char( rlAftrec.PSTA );
        get_fld(pclDataArea,FIELD_16,STR,PSTALEN,rlAftrec.PSTD); strip_special_char( rlAftrec.PSTD );
        get_fld(pclDataArea,FIELD_17,STR,BELTLEN,rlAftrec.BLT1); strip_special_char( rlAftrec.BLT1 );
        get_fld(pclDataArea,FIELD_18,STR,DATELEN,rlAftrec.OFBL); UtcToLocalTimeFixTZ(rlAftrec.OFBL);
        get_fld(pclDataArea,FIELD_19,STR,DATELEN,rlAftrec.ONBL); UtcToLocalTimeFixTZ(rlAftrec.ONBL);
        get_fld(pclDataArea,FIELD_20,STR,TGADLEN,rlAftrec.TGA1); strip_special_char( rlAftrec.TGA1 );
        get_fld(pclDataArea,FIELD_21,STR,TGADLEN,rlAftrec.TGD1); strip_special_char( rlAftrec.TGD1 );
        get_fld(pclDataArea,FIELD_22,STR,TTYPLEN,rlAftrec.TTYP); strip_special_char( rlAftrec.TTYP );
        get_fld(pclDataArea,FIELD_23,STR,COTYLEN,rlAftrec.ACT3); strip_special_char( rlAftrec.ACT3 );
        get_fld(pclDataArea,FIELD_24,STR,FTYPLEN,rlAftrec.FTYP); strip_special_char( rlAftrec.FTYP );
    

        memset( &rlTAftrec, 0, sizeof(AFTREC) );
        if( GetTransitFlight( rlAftrec, &rlTAftrec ) == RC_SUCCESS )
        {
            dbg( DEBUG, "<%s> TRANSIT! <%s>", pclFunc, rlAftrec.FLNO );
            strcpy( rlAftrec.TURN, rlTAftrec.URNO );
            strcpy( rlTAftrec.TURN, rlAftrec.URNO );
            PackFlightXml( pclTmpXml, rlTAftrec, TYPE_BULK );
            strcat(bulkMessage,pclTmpXml);
            dbg(DEBUG,"<%s> TFlight Xml <%s>", pclFunc, pclTmpXml);
            ilRecordCount++;

            NewFlight(rlAftrec.URNO, rlTAftrec.URNO);
            NewFlight(rlTAftrec.URNO, rlAftrec.URNO);
        }
        else
            NewFlight(rlAftrec.URNO, NULL);

        PackFlightXml( pclTmpXml, rlAftrec, TYPE_BULK );
        strcat(bulkMessage,pclTmpXml);
        dbg(DEBUG,"<%s> Flight Xml <%s>", pclFunc, pclTmpXml);
    }
    close_my_cursor(&slCursor);
    strcat(bulkMessage,"</FLIGHTS>\n");
    dbg(DEBUG,"<%s> MsgLen  : <%d>",pclFunc,strlen(bulkMessage));
    
    /*Send to iTREK MQ - Bulk message */
    ilRc = SendCedaEvent(igITKWMA,0,mod_name,"","","","ITK","","","",bulkMessage,"",
                         atoi(cgSendPrio),RC_SUCCESS);

    free(bulkMessage);
    dbg(DEBUG,"<%s> ####### END ##########",pclFunc);
}


static void SendJobBulkData(char *pcpAftSelection)
{
    JOBREC rlJobrec;
    int ilRc = 0;
    int ili = 0;
    int ilXmlStrSize = 0;
    short slSqlFunc = START;
    short slSqlFunc1 = START;
    short slSqlCursor = 0;
    char pclData[DBQRLEN] = "\0";
    char pclData2[30] = "\0";
    char pclSqlBuf[DBQRLEN] = "\0";
    char pclTmpStr[DBQRLEN] = "\0";
    char pclSqlFields[200] = " TRIM(URNO),TRIM(USTF),TRIM(UAFT),TRIM(FCCO),TRIM(UTPL),TRIM(DETY),ACTO";
    char *pclFunc = "SendJobBulkData";
    char *pclXmlStr;

    dbg(TRACE,"<%s> ####### START ##########", pclFunc );

    pclXmlStr = (char *) malloc(START_DATA_SIZE);
    if( pclXmlStr == NULL )
    {
        dbg( TRACE, "%s: ERROR!!! in getting memory!!!", pclFunc );
        exit( 3 );
    }

    ilXmlStrSize = START_DATA_SIZE;
    strcpy(pclXmlStr,"<JOBS>\n");


    sprintf(pclSqlBuf,"SELECT %s FROM JOBTAB WHERE ACTO BETWEEN '%s' AND '%s' "
                       "AND ACFR < '%s' AND UTPL IN (%s) "
                       "AND UAFT <> '0' AND USTF NOT IN ('0',' ') AND UAFT IN "
                       "(%s) ", pclSqlFields, pcgBulkStartTime[BLK_JOB],
                                pcgBulkEndTime[BLK_JOB], pcgBulkEndTime[BLK_JOB], 
                                cgJobTemplates, pcpAftSelection);
    dbg(TRACE,"<%s> Sql Buf <%s>",pclFunc,pclSqlBuf);
    
    ili = 0;
    while((ilRc = sql_if(slSqlFunc,&slSqlCursor,pclSqlBuf,pclData)) == DB_SUCCESS)
    {
        slSqlFunc = NEXT;
        if( strlen( pclXmlStr ) > (ilXmlStrSize - 1000) )
        {
            if( (ilXmlStrSize+BLK_DATA_SIZE) > igXmlCapSize )
            {
                dbg (TRACE, "<%s> Too much data to be sent!! ABORT!", pclFunc );
                free( pclXmlStr );
                close_my_cursor(&slSqlCursor);
                return;
            }

            pclXmlStr = (char *)realloc( pclXmlStr, ilXmlStrSize + BLK_DATA_SIZE ); 
            if( pclXmlStr == NULL )
            {
                dbg (TRACE, "<%s> Fail to enlarge XML buffer", pclFunc );
                exit( 2 );
            }
            ilXmlStrSize += BLK_DATA_SIZE;
            dbg (TRACE, "<%s> Increase XML Str size to <%d>", pclFunc, ilXmlStrSize );
        }

        memset( &rlJobrec, 0, sizeof(JOBREC) );
        get_fld(pclData,FIELD_1,STR,URNOLEN,rlJobrec.URNO); 
        dbg(DEBUG,"<%s> Rec No: <%d> URNO <%s> ------------",pclFunc,++ili, rlJobrec.URNO);
        get_fld(pclData,FIELD_2,STR,URNOLEN,rlJobrec.USTF);
        get_fld(pclData,FIELD_3,STR,URNOLEN,rlJobrec.UAFT);
        get_fld(pclData,FIELD_4,STR,FCCOLEN,rlJobrec.FCCO); strip_special_char(rlJobrec.FCCO);
        get_fld(pclData,FIELD_5,STR,UTPLLEN,rlJobrec.UTPL); strip_special_char(rlJobrec.UTPL);
        get_fld(pclData,FIELD_6,STR,DETYLEN,rlJobrec.DETY); strip_special_char(rlJobrec.DETY);
        get_fld(pclData,FIELD_7,STR,DATELEN,rlJobrec.ACTO); strip_special_char(rlJobrec.ACTO);

        ilRc = PackJobXml( pclTmpStr, rlJobrec, TYPE_BULK );

        dbg( DEBUG, "<%s> Job Xml <%s>", pclFunc, pclTmpStr );
        strcat( pclXmlStr, pclTmpStr );

        /*
        sprintf( pclTmpStr, "SELECT TRIM(ALC3) FROM AFTTAB WHERE URNO = %s", rlJobrec.UAFT );
        dbg( DEBUG, "<%s> ALC3(AFT) SqlBuf <%s>", pclFunc, pclTmpStr );
        ilRc = getOrPutDBData(pclTmpStr,pclData2,START);
        dbg( DEBUG, "<%s> ALC3(AFT) <%s>", pclFunc, pclData2 );
        if( ilRc == DB_SUCCESS && !strstr( pcgHandledAirlines, pclData2 ) )
            continue;
        */

        /* Arrival Flight which was not stored in JOBTAB. Get info from AFTTAB instead */
        if( rlJobrec.DETY[0] == '0' )
        {
            sprintf( pclTmpStr, "SELECT TRIM(URNO) FROM AFTTAB WHERE RKEY = "
                                "(SELECT RKEY FROM AFTTAB WHERE URNO = '%s') "
                                "AND ADID = 'A' "
                                "AND URNO <> %s "
                                "AND FTYP NOT IN (%s)", rlJobrec.UAFT, rlJobrec.UAFT, pcgExcludeFtyp );
            dbg( TRACE, "<%s> URNO(AFT) Sql Query <%s>", pclFunc, pclTmpStr );

            slSqlFunc1 = START;
            ilRc = getOrPutDBData(pclTmpStr,pclData2,slSqlFunc1);
            dbg( TRACE, "<%s> ilRc <%d>", pclFunc, ilRc );
            if( ilRc == DB_SUCCESS )
            {
                if( strlen(pclData2) >= URNOLEN )
                    pclData2[URNOLEN] = '\0';
                strcpy( rlJobrec.UAFT, pclData2 );
                sprintf( pclTmpStr, "A%s", rlJobrec.URNO );
                strcpy( rlJobrec.URNO, pclTmpStr );

                memset( pclTmpStr, 0, sizeof(pclTmpStr) );

                ilRc = PackJobXml( pclTmpStr, rlJobrec, TYPE_BULK );

                dbg(DEBUG, "<%s> TJob Xml <%s>", pclFunc, pclTmpStr );
                strcat( pclXmlStr, pclTmpStr );
            }  
        }
    }
    close_my_cursor(&slSqlCursor);
    strcat(pclXmlStr,"</JOBS>\n");
    dbg(TRACE,"<%s> Msglen <%d> No. of records <%d>", pclFunc, strlen(pclXmlStr), ili);
    
    ilRc = SendCedaEvent(igITKWMJ,0,mod_name,"","","","ITK","","","",pclXmlStr,"",atoi(cgSendPrio),RC_SUCCESS);
    free(pclXmlStr);

    dbg(TRACE,"<%s> ####### END ##########",pclFunc); 
} /* SendJobBulkData */

static void SendEquBulkData(char *pcpAftSelection)
{
    AFTREC rlAftrec;
    JOBREC rlJobrec;
    int ilRc = 0;
    int ili = 0;
    int ilXmlStrSize = 0;
    short slSqlFunc = START;
    short slSqlFunc1 = START;
    short slSqlCursor = 0;
    char pclData[DBQRLEN] = "\0";
    char pclData2[128] = "\0";
    char pclSqlBuf[DBQRLEN] = "\0";
    char pclTmpStr[DBQRLEN] = "\0";
    char *pclFunc = "SendEquBulkData";
    char *pclXmlStr;

    dbg(TRACE,"<%s> ####### START ##########", pclFunc );

    pclXmlStr = (char *) malloc(START_DATA_SIZE);
    if( pclXmlStr == NULL )
    {
        dbg( TRACE, "%s: ERROR!!! in getting memory!!!", pclFunc );
        exit( 3 );
    }

    ilXmlStrSize = START_DATA_SIZE;
    strcpy(pclXmlStr,"<EQJOBS>\n");


    sprintf(pclSqlBuf,"SELECT TRIM(URNO),TRIM(UAFT),TRIM(UEQU),TRIM(ACFR),TRIM(ACTO),TRIM(UTPL),TRIM(DETY) "
                      "FROM JOBTAB WHERE ACTO BETWEEN '%s' AND '%s' "
                      "AND ACFR < '%s' AND UTPL IN (%s) "
                      "AND UAFT <> '0' AND UEQU <> 0 AND UAFT IN "
                      "(%s) ", pcgBulkStartTime[BLK_JOB],pcgBulkEndTime[BLK_JOB], pcgBulkEndTime[BLK_JOB], 
                               cgJobTemplates, pcpAftSelection);
    dbg(TRACE,"<%s> SqlBuf <%s>",pclFunc,pclSqlBuf);
    
    ili = 0;
    while((ilRc = sql_if(slSqlFunc,&slSqlCursor,pclSqlBuf,pclData)) == DB_SUCCESS)
    {
        slSqlFunc = NEXT;
        if( strlen( pclXmlStr ) > (ilXmlStrSize - 1000) )
        {
            if( (ilXmlStrSize+BLK_DATA_SIZE) > igXmlCapSize )
            {
                dbg (TRACE, "<%s> Too much data to be sent!! ABORT!", pclFunc );
                free( pclXmlStr );
                close_my_cursor(&slSqlCursor);
                return;
            }

            pclXmlStr = (char *)realloc( pclXmlStr, ilXmlStrSize + BLK_DATA_SIZE ); 
            if( pclXmlStr == NULL )
            {
                dbg (TRACE, "<%s> Fail to enlarge XML buffer", pclFunc );
                exit( 2 );
            }
            ilXmlStrSize += BLK_DATA_SIZE;
            dbg (TRACE, "<%s> Increase XML Str size to <%d>", pclFunc, ilXmlStrSize );
        }

        memset( &rlJobrec, 0, sizeof(JOBREC) );
        get_fld(pclData,FIELD_1,STR,URNOLEN,rlJobrec.URNO); 
        dbg(DEBUG,"<%s> Rec No: <%d> URNO <%s> ------------",pclFunc,++ili, rlJobrec.URNO);
        get_fld(pclData,FIELD_2,STR,URNOLEN,rlJobrec.UAFT);
        get_fld(pclData,FIELD_3,STR,URNOLEN,rlJobrec.UEQU);
        get_fld(pclData,FIELD_4,STR,DATELEN,rlJobrec.ACFR); strip_special_char(rlJobrec.ACFR);
        get_fld(pclData,FIELD_5,STR,DATELEN,rlJobrec.ACTO); strip_special_char(rlJobrec.ACTO);
        get_fld(pclData,FIELD_6,STR,UTPLLEN,rlJobrec.UTPL); strip_special_char(rlJobrec.UTPL);
        get_fld(pclData,FIELD_7,STR,DETYLEN,rlJobrec.DETY); strip_special_char(rlJobrec.DETY);

        ilRc = PackJobXml( pclTmpStr, rlJobrec, TYPE_BULK );

        dbg( DEBUG, "<%s> Job Xml <%s>", pclFunc, pclTmpStr );
        strcat( pclXmlStr, pclTmpStr );

        /* Arrival Flight which was not stored in JOBTAB. Get info from AFTTAB instead */
        if( rlJobrec.DETY[0] == '0' )
        {
            memset( &rlAftrec, 0, sizeof(AFTREC) );
            sprintf( pclTmpStr, "SELECT TRIM(URNO),TRIM(ADID) FROM AFTTAB WHERE RKEY = "
                                "(SELECT RKEY FROM AFTTAB WHERE URNO = %s) "
                           /*     "AND ADID = 'A' "*/
                                "AND URNO <> %s "
                                "AND FTYP NOT IN (%s)", rlJobrec.UAFT, rlJobrec.UAFT, pcgExcludeFtyp );
            dbg( TRACE, "<%s> URNO(AFT) Sql <%s>", pclFunc, pclTmpStr );

            ilRc = getOrPutDBData(pclTmpStr,pclData2,START);    
            dbg( TRACE, "<%s> ilRc <%d>", pclFunc, ilRc );

            if( ilRc == DB_SUCCESS )
            {
                get_fld(pclData2,FIELD_1,STR,URNOLEN,rlAftrec.URNO); 
                get_fld(pclData2,FIELD_2,STR,ADIDLEN,rlAftrec.ADID);

                strcpy( rlJobrec.UAFT, rlAftrec.URNO );
                /*sprintf( pclTmpStr, "%c%s", rlAftrec.ADID[0], rlJobrec.URNO );*/
                /* MEI 27-AUG-09 iTrek cant handle first char to be non-A */
                sprintf( pclTmpStr, "A%s", rlJobrec.URNO );
                strcpy( rlJobrec.URNO, pclTmpStr );

                memset( pclTmpStr, 0, sizeof(pclTmpStr) );
                ilRc = PackJobXml( pclTmpStr, rlJobrec, TYPE_BULK );

                dbg(DEBUG, "<%s> TJob Xml <%s>", pclFunc, pclTmpStr );
                strcat( pclXmlStr, pclTmpStr );
            }  
        }
    }
    close_my_cursor(&slSqlCursor);
    strcat(pclXmlStr,"</EQJOBS>\n");
    dbg(TRACE,"<%s> Msglen <%d> No. of records <%d>", pclFunc, strlen(pclXmlStr), ili);
    
    ilRc = SendCedaEvent(igITKWMJ,0,mod_name,"","","","ITK","","","",pclXmlStr,"",atoi(cgSendPrio),RC_SUCCESS);
    free(pclXmlStr);

    dbg(TRACE,"<%s> ####### END ##########",pclFunc); 
} /* SendEquBulkData */

static void HandleFlightOnlineData(char *pcpFields, char *pcpData, char *pcpCmd, char *pcpSelection)
{
    AFTREC rlAftrec;
    BOOL blNewUpdate = FALSE;
    time_t ilNowTm, ilStartTm, ilEndTm;
    int ilRc = RC_SUCCESS;
    int ilDataType;
    int ilTimeIndex;
    int ilItemCount = 0;
    int ilItemPosition = 0;
    int ilTifPos;
    int ili, ilj;
    char pclStartTimer[16] = "\0";
    char pclEndTimer[16] = "\0";
    char pclTif[16] = "\0";
    char pclSqlBuf[512];
    char pclSqlData[512];
    char pclTmpStr[DBQRLEN] = "\0";     
    char pclData[2][DBQRLEN];
    char pclInfo[2][100];
    char pclFTYP[12];
    char pclALC[4];
    char pclTimeFields[NUM_TIME_FIELDS][5] = { "STOD", "STOA", "ETDI", "ETAI", "OFBL", "ONBL" };
    char pclFields[MAX_FLIGHT_FIELDS][5]   = { "URNO", "FLNO", "STOD", "STOA", "REGN", 
                                               "DES3", "ORG3", "ETDI", "ETAI", "GTA1", 
                                               "GTD1", "ADID", "PSTA", "PSTD", "BLT1", 
                                               "OFBL", "ONBL", "TGA1", "TGD1", "TTYP", 
                                               "ACT3", "FTYP", "RKEY" };
    char *pclFunc = "HandleFlightOnlineData";
    char *pclToken;

    if( mod_id != rgITKHDL[0] )
        return;

    dbg(TRACE,"<%s> ---------- START ---------",pclFunc);

    memset( pclInfo, 0, sizeof(pclInfo) );
    memset( pclData, 0, sizeof(pclData) );

    strcpy( pclData[0], pcpData );
    pclToken = (char *)strstr( pclData[0], "\n" );
    if( pclToken != NULL )
    {
        *pclToken = '\0';
        pclToken++;
        strcpy( pclData[1], pclToken );
    }
    strip_special_char(pclData[0]);
    strip_special_char(pclData[1]);
    dbg(DEBUG,"<%s> Str1 <%s>",pclFunc, pclData[0] );
    dbg(DEBUG,"<%s> Str2 <%s>",pclFunc, pclData[1] );

    /* Delete */
    if( !strncmp( pcpCmd, "DFR", 3 ) )
        ilDataType = TYPE_DELETE;
    else if( !strncmp( pcpCmd, "UFR", 3 ) )
        ilDataType = TYPE_UPDATE;
    else if( !strncmp( pcpCmd, "IFR", 3 ) )
        ilDataType = TYPE_INSERT;
    else
        return;

    /****************************************************************************
     Check whether this flight is the required flight type
    ****************************************************************************/
    ilItemPosition = get_item_no(pcpFields,"FTYP",5);
    if( ilItemPosition >= 0 )
        get_real_item(pclFTYP, pclData[0], ilItemPosition+1);

    strcpy( pclTmpStr, pcgExcludeFtyp );
    if( strlen(pclFTYP) > 0 && strstr( pclTmpStr, pclFTYP ) != NULL )
    {
        dbg( TRACE, "<%s> Not required FTYP <%s>. Ignore", pclFunc, pclFTYP );
        return;
    }

    /****************************************************************************
     25-SEP-08: Check whether this flight is the required handling agent
    ****************************************************************************/
    memset( pclALC, 0, sizeof(pclALC) );
    ilItemPosition = get_item_no(pcpFields,"ALC3",5);
    if( ilItemPosition >= 0 )
    {
        get_real_item(pclALC, pclData[0], ilItemPosition+1);
        if( strlen(pclALC) > 0 )
        {
            sprintf ( pclTmpStr, "%s;", pclALC );
            if( !strstr( pcgHandledAirlines, pclTmpStr ) )
            {
                dbg( TRACE, "<%s> ALC3: Not the required handling agent <%s> Ignore.", pclFunc, pclALC );
                return;
            }
        }
    }
    else
    {
        ilItemPosition = get_item_no(pcpFields,"FLNO",5);
        if( ilItemPosition >= 0 )
        {
            get_real_item(pclTmpStr, pclData[0], ilItemPosition+1);
            strncpy( pclALC, pclTmpStr, 3 );
            if( pclALC[2] == ' ' ) /* Check ALC2 */
            {
                sprintf( pclSqlBuf, "SELECT HSNA FROM HAITAB WHERE ALTU = "
                                    "(SELECT URNO FROM ALTTAB WHERE ALC2 = '%2.2s') AND HSNA IN (%s)", pclALC, pcgReqdAgent );
                dbg( DEBUG, "<%s> HSNA SqlBuf <%s>", pclFunc, pclSqlBuf );
                ilRc = getOrPutDBData(pclSqlBuf,pclSqlData,START);
            }
            else
            {
                sprintf ( pclTmpStr, "%s;", pclALC );
                if( !strstr( pcgHandledAirlines, pclTmpStr ) )
                    ilRc = RC_FAIL;
            }
            if( ilRc != DB_SUCCESS )
            {
                dbg( TRACE, "<%s> FLNO: Not the required handling agent <%s> Ignore.", pclFunc, pclALC );
                return;
            }
        }
    } /* ALC3 */


    /* Check validity of time */
    ilTimeIndex = -1;
    ilItemPosition = get_item_no(pcpFields,"ORG3",5);
    if( ilItemPosition >= 0 )
    {
        get_real_item(pclTmpStr, pclData[0], ilItemPosition+1);
        if( !strncmp( pclTmpStr, cgHopo, 3 ) ) /* departure flight */
        {
            ilItemPosition = get_item_no(pcpFields,"TIFD",5);
            ilTifPos = ilItemPosition;
            ilTimeIndex = DEP_FLIGHT;
        }
    }
    ilItemPosition = get_item_no(pcpFields,"DES3",5);
    if( ilItemPosition >= 0 )
    {
        get_real_item(pclTmpStr, pclData[0], ilItemPosition+1);
        if( !strncmp( pclTmpStr, cgHopo, 3 ) ) /* arrival flight */
        {
            ilItemPosition = get_item_no(pcpFields,"TIFA",5);
            ilTifPos = ilItemPosition;
            ilTimeIndex = ARR_FLIGHT;
        }
    }

    /** Ignore updates only if both old and new timing is out of the range **/
    if( ilTimeIndex > -1 && ilTifPos >= 0 )
    {
        ilNowTm   = time(0);
        ilStartTm = ilNowTm + lgBulkStartTime[ARR_FLIGHT];
        ilEndTm   = ilNowTm + lgBulkEndTime[ARR_FLIGHT];
        FormatTime( ilStartTm, pclStartTimer, "UTC" );
        FormatTime( ilEndTm,   pclEndTimer,   "UTC" );

        get_real_item(pclTif, pclData[0], ilTifPos+1);
        dbg( TRACE, "<%s> NEW: ChkTime Start <%s> Tif <%s> End <%s>", pclFunc, pclStartTimer, pclTif, pclEndTimer );
        if ((strcmp(pclTif,pclStartTimer) < 0) || (strcmp(pclTif,pclEndTimer) > 0))
        {
            if( ilDataType == TYPE_INSERT )
                return;
            get_real_item(pclTif, pclData[1], ilTifPos+1);
            dbg( TRACE, "<%s> OLD: ChkTime Start <%s> Tif <%s> End <%s>", pclFunc, pclStartTimer, pclTif, pclEndTimer );
            if ((strcmp(pclTif,pclStartTimer) < 0) || (strcmp(pclTif,pclEndTimer) > 0))
            {
                dbg( TRACE, "<%s> Skip. Wrong time.", pclFunc );
                dbg(TRACE,"<%s> ---------- END ---------",pclFunc);
                return;
            }
        }
    }

    sprintf( pcgXmlStr, "%s", "<FLIGHT>\n" );

    blNewUpdate = FALSE;

    /* Insert/Update */
    for( ili = 0; ili < MAX_FLIGHT_FIELDS; ili++ )
    {
        memset( pclInfo[0], 0, sizeof(pclInfo[0]) ); 
        ilItemPosition = get_item_no(pcpFields,pclFields[ili],5);
        if( ilItemPosition >= 0 )
        {
            get_real_item(pclInfo[0], pclData[0], ilItemPosition+1);
            if( ili > 0 && ilDataType == TYPE_UPDATE )
            {
                get_real_item(pclInfo[1], pclData[1], ilItemPosition+1);
                if( !strcmp( pclInfo[0], pclInfo[1] ) )
                    continue;
                blNewUpdate = TRUE;
            }
        }
        else
        {
            /* Item not found. If is for Update, then can ignore this field */
            if( ili > 0 ) /* Exclude URNO */
                continue;

            /** MEI 28-JULY-2009 URNO not found in field list **/
            dbg( DEBUG, "<%s> URNO not found in field list. Use selection", pclFunc );
            pclToken = (char *)strtok(pcpSelection, "=" );
            if( pclToken != NULL )
                pclToken = (char *)strtok(NULL, "=" );          
            if( pclToken != NULL )
            {
                strcpy( pclInfo[0], pclToken );
                TrimSpace( pclInfo[0] );
                TrimUrno( pclInfo[0] );
                dbg( DEBUG, "<%s> URNO <%s>", pclFunc, pclInfo[0] );
            }
        }

        for( ilj = 0; ilj < NUM_TIME_FIELDS; ilj++ )
        {
            if( !strncmp( pclTimeFields[ilj], pclFields[ili], 5 ) )
            {   
                UtcToLocalTimeFixTZ(pclInfo[0]);
                break;
            }
        }

        sprintf( pclTmpStr, "<%s>%s</%s>\n", pclFields[ili], pclInfo[0], pclFields[ili] );
        strcat( pcgXmlStr, pclTmpStr );

        if( ili == 0 )
        {
            if( ilDataType == TYPE_DELETE )
            {
                sprintf( pclTmpStr, "%s", "<ACTION>DELETE</ACTION>\n" );
                strcat( pcgXmlStr, pclTmpStr );
                break;
            }
            if( ilDataType == TYPE_INSERT )
            {
                ilRc = ReadFlightRecord( pclInfo[0], &rlAftrec );
                if( ilRc != DB_SUCCESS )
                    return;   
                PackFlightXml( pcgXmlStr, rlAftrec, TYPE_INSERT );
                break;
            }
            if( ilDataType == TYPE_UPDATE )
            {
                sprintf( pclTmpStr, "%s", "<ACTION>UPDATE</ACTION>\n" );
                strcat( pcgXmlStr, pclTmpStr );
            }
        }
    } /* For */

    if( ilDataType == TYPE_UPDATE && blNewUpdate == FALSE )
    {
        dbg( TRACE, "<%s> Nothing to update for this flight", pclFunc );
        return;
    }
    if( ilDataType != TYPE_INSERT )
    {
        sprintf( pclTmpStr, "</FLIGHT>\n" );
        strcat( pcgXmlStr, pclTmpStr );
    }
    dbg(TRACE,"<%s> Xml <%s>",pclFunc, pcgXmlStr);
    ilRc = SendCedaEvent(igITKWMA,0,mod_name,"","","","ITK","","","", pcgXmlStr,"",atoi(cgSendPrio),RC_SUCCESS);
    if( ilRc != RC_SUCCESS )
        dbg(TRACE,"<%s> Sending to MQ Fail",pclFunc );

    dbg(TRACE,"<%s> ---------- END ---------",pclFunc);
                                    
} /* HandleFlightOnline Data */

/* NOTE: WebServer is updating both turnaround job and job at the same time if both exist, meaning, J1 is sent to WebServer */
/*       if J1 is turnarnd job, JA1 will exist, thus, upd will be performed on both job. Be careful when sending job upd    */
/*       for turnaround job, J1 should be sent first followed by JA1.                                                       */
static void HandleJobOnlineData(char *pcpFields, char *pcpData, char *pcpCmd, char *pcpSelection)
{
    JOBREC rlJobrec_new;
    JOBREC rlJobrec_old;
    JOBREC rlJobrec;
    AFTREC rlAftrec;
    BOOL blTurnDDJob = FALSE;
    BOOL blChangesToUpd = FALSE;
    BOOL blEquipmentJob = STAT_UNKNOWN;
    int ilXmlPackType = 0;
    int ilRc = 0;
    int ilNum = 0;
    int ilItemPosition = 0;
    int ilItemCount = 0;
    int ilDETYPos, ilUAFTPos, ilUTPLPos;
    int ilACTOPos, ilACFRPos;
    short slSqlFunc = START;
    short slSqlCursor = 0;
    char pclCurrent[400] = "\0";
    char pclOld[400] = "\0";
    char pclTmpStr1[400] = "\0";
    char pclTmpStr2[400] = "\0";
    char pclData[DBQRLEN] = "\0";
    char pclSqlBuf[DBQRLEN] = "\0";
    char pclFLNO[20]= "\0";
    char pclUAFT[20] = "\0";
    char *pclFunc = "HandleJobOnlineData";
    char *pclTmpPtr;
    
    dbg(TRACE,"<%s> ######## Start ##########",pclFunc );

    memset( &rlJobrec, 0, sizeof(JOBREC) );
    memset( &rlJobrec_new, 0, sizeof(JOBREC) );
    memset( pcgXmlStr, 0, sizeof(pcgXmlStr) );

    strcpy(pclCurrent,pcpData);
    pclTmpPtr = strstr(pclCurrent,"\n");
    if (pclTmpPtr != NULL)
    {
        *pclTmpPtr = '\0';
        pclTmpPtr++;
        strcpy(pclOld,pclTmpPtr);
    }

    strip_special_char(pclCurrent);
    strip_special_char(pclOld);
    dbg(DEBUG," Command --- > %s",pcpCmd);
    ilItemPosition = get_item_no(pcpFields,"URNO",5);
    if(ilItemPosition < 0)
    {
        dbg( TRACE, "<%s> URNO not found ilItemPosition <%d>", pclFunc, ilItemPosition );
        return;
    }
    
    /******* Delete Job ********/
    if( !strncmp(pcpCmd,"DRT", 3) )
    {
        dbg(TRACE,"<%s> DELETE!!! pcpSelection: <%s>",pclFunc, pcpSelection);
        if( GetElementValue(rlJobrec.URNO, pcpSelection, "'", "'") == FALSE )
        {
            memset( &rlJobrec.URNO, 0, sizeof( rlJobrec.URNO ) );
            TrimUrno(pcpSelection);
            strcpy( rlJobrec.URNO, pcpSelection );
        }
        if( strlen(rlJobrec.URNO) > URNOLEN )
            rlJobrec.URNO[URNOLEN] = '\0';                


        /* Check whether it is a change in the shift info instead, For this, no flight is given */
        if( strlen(pclOld) > 0 ) 
        {
            ilItemPosition = get_item_no(pcpFields,"UAFT",5);
            if( ilItemPosition >= 0 )
            {
                get_real_item (pclTmpStr1, pclOld, ilItemPosition+1);
                if( !strcmp( pclTmpStr1, "0" ) )
                {
                    dbg( TRACE, "<%s> Shift info.No flight is assigned.Do not send to Web", pclFunc );
                    return;
                }
            }
        }

        /* Check whether it is Apron jobs */
        sprintf( pclSqlBuf, "SELECT TRIM(UJOB) FROM ITKJOB WHERE UJOB IN ('%s','E%s')", rlJobrec.URNO, rlJobrec.URNO );
        dbg( DEBUG,"<%s> SelJob pclSqlBuf <%s>",pclFunc, pclSqlBuf );
        ilRc = getOrPutDBData(pclSqlBuf,pclData,START);
        if( ilRc != DB_SUCCESS )
        {
            dbg( DEBUG,"<%s> Error in finding Job <%s> in ITKJOB. Do not send DELETE", pclFunc, rlJobrec.URNO );
            return;
        }
        /* Equipment Job */
        if( pclData[0] == 'E' )
            strcpy( rlJobrec.UEQU, "1" );
        else
            strcpy( rlJobrec.USTF, "1" );

        ilRc = PackJobXml( pcgXmlStr, rlJobrec, TYPE_DELETE );
        if( ilRc == RC_SUCCESS )
        {
            dbg( TRACE, "<%s> DELETE!!! ilRc <%d> Xml <%s>", pclFunc, ilRc, pcgXmlStr );
            ilRc = SendCedaEvent(igITKWMJ,0,mod_name,"","","","ITK","","","",pcgXmlStr,"",
                                 atoi(cgSendPrio),RC_SUCCESS);
            dbg( TRACE, "<%s> ilRc <%d>", pclFunc, ilRc );
        }
        return;
    }
    /******* Delete *******/

    if( !strncmp(pcpCmd,"URT", 3) ) 
        ilXmlPackType = TYPE_UPDATE;
    else if( !strncmp(pcpCmd,"IRT", 3) ) 
        ilXmlPackType = TYPE_INSERT;
    else
    {
        dbg( TRACE, "<%s> Invalid Cmd <%s>", pclFunc, pcpCmd );
        return;
    }

    get_real_item (rlJobrec.URNO, pclCurrent, ilItemPosition+1);

    ilItemCount = itemCount(pcpFields);
    if( ilItemCount <= 1 )
    {
        dbg( TRACE, "<%s> Invalid number of field received FieldCount=%d. Ignore.", pclFunc, ilItemCount );
        return;
    }
/***
    ilItemPosition = get_item_no(pcpFields,"UEQU",5);
    strcpy( rlJobrec.UEQU, "-1" );
    if(ilItemPosition >= 0)
    {
        get_real_item ( rlJobrec.UEQU, pclCurrent, ilItemPosition+1);
        if( atoi(rlJobrec.UEQU) > 0 )
        {
            HandleEquipOnlineData( rlJobrec.URNO );
            return;
        }
    }
***/
    ilItemPosition = get_item_no(pcpFields,"UEQU",5);
    strcpy( rlJobrec.UEQU, "-1" );
    if(ilItemPosition >= 0)
    {
        get_real_item (pclTmpStr1, pclCurrent, ilItemPosition+1);
        get_real_item (pclTmpStr2, pclOld, ilItemPosition+1);
        strcpy( rlJobrec_new.UEQU, pclTmpStr1 );
        strcpy( rlJobrec_old.UEQU, pclTmpStr2 );

        if( (strcmp(rlJobrec_old.UEQU,rlJobrec_new.UEQU) != 0) || ilXmlPackType == TYPE_INSERT )
            strcpy( rlJobrec.UEQU, rlJobrec_new.UEQU );
        blEquipmentJob = TRUE;
        if( !strncmp( rlJobrec_new.UEQU, "0", 1 ) )
            blEquipmentJob = FALSE;
    }

    ilItemPosition = get_item_no(pcpFields,"USTF",5);
    strcpy( rlJobrec.USTF, "-1" );
    if(ilItemPosition >= 0)
    {
        get_real_item (pclTmpStr1, pclCurrent, ilItemPosition+1);
        get_real_item (pclTmpStr2, pclOld, ilItemPosition+1);
        strcpy( rlJobrec_new.USTF, pclTmpStr1 );
        strcpy( rlJobrec_old.USTF, pclTmpStr2 );

        if( (strcmp(rlJobrec_old.USTF,rlJobrec_new.USTF) != 0) || ilXmlPackType == TYPE_INSERT )
            strcpy( rlJobrec.USTF, rlJobrec_new.USTF );
        if( !strncmp( rlJobrec.USTF, "0", 1 ) )
        {
            if( blEquipmentJob == FALSE )
            {
                dbg( DEBUG, "<%s> Ignore. Not equip/staff job", pclFunc );
                return;
            }
        }
    }
 
    ilUAFTPos = get_item_no(pcpFields,"UAFT",5);
    strcpy( rlJobrec.UAFT, "-1" );
    if(ilUAFTPos >= 0)
    {
        get_real_item (rlJobrec_new.UAFT, pclCurrent, ilUAFTPos+1); /* For DB retrieval of flight record */
        get_real_item (rlJobrec_old.UAFT, pclOld, ilUAFTPos+1); /* For upd of split transit flight */
        
        if( !strncmp( rlJobrec_old.UAFT, "0", 1 ) && !strncmp( rlJobrec_new.UAFT, "0", 1 ) )
        {
            dbg( DEBUG, "<%s> Ignore 1. No flight info", pclFunc );
            return;
        }

        if( (strcmp(rlJobrec_old.UAFT,rlJobrec_new.UAFT) != 0) || ilXmlPackType == TYPE_INSERT )
            strcpy( rlJobrec.UAFT, rlJobrec_new.UAFT );

        if( !strncmp( rlJobrec.UAFT, "0", 1 ) )
        {
            dbg( DEBUG, "<%s> Ignore 2. No flight info", pclFunc );
            return;
        }
    }

    ilDETYPos = get_item_no(pcpFields,"DETY",5);
    strcpy( rlJobrec.DETY, "-1" );
    if(ilDETYPos >= 0)
    {
        get_real_item (rlJobrec_new.DETY, pclCurrent, ilDETYPos +1);
        get_real_item (rlJobrec_old.DETY, pclOld, ilDETYPos +1);
            
        if( (strcmp(rlJobrec_old.DETY,rlJobrec_new.DETY) != 0) || ilXmlPackType == TYPE_INSERT )
            strcpy( rlJobrec.DETY, rlJobrec_new.DETY );
    }

    ilItemPosition = get_item_no(pcpFields,"FCCO",5);
    strcpy( rlJobrec.FCCO, "-1" );
    if(ilItemPosition >= 0)
    {
        get_real_item (rlJobrec_new.FCCO, pclCurrent, ilItemPosition+1);
        get_real_item (rlJobrec_old.FCCO, pclOld, ilItemPosition+1);
            
        if( (strcmp(rlJobrec_old.FCCO,rlJobrec_new.FCCO) != 0) || ilXmlPackType == TYPE_INSERT )
            strcpy( rlJobrec.FCCO, rlJobrec_new.FCCO );
    }

    ilUTPLPos = get_item_no(pcpFields,"UTPL",5);
    strcpy( rlJobrec.UTPL, "-1" );
    if(ilUTPLPos >= 0)
    {
        get_real_item (rlJobrec_new.UTPL, pclCurrent, ilUTPLPos+1);
        get_real_item (rlJobrec_old.UTPL, pclOld, ilUTPLPos+1);
            
        if( (strcmp(rlJobrec_old.UTPL,rlJobrec_new.UTPL) != 0) || ilXmlPackType == TYPE_INSERT )
            strcpy( rlJobrec.UTPL, rlJobrec_new.UTPL );

        sprintf( pclTmpStr1, "'%s'", rlJobrec.UTPL );  /* Amended template */
        if( !strncmp( rlJobrec.UTPL, "-1", 2 ) )
            /* No change in template but still need to check whether it belongs to reqd template */
            sprintf( pclTmpStr1, "'%s'", rlJobrec_old.UTPL );
        dbg( DEBUG, "%s: UTPL srch str <%s>", pclFunc, pclTmpStr1 );
    }

    /* Get ACTO for mini JOBTAB and equip jobs */
    ilACTOPos = get_item_no(pcpFields,"ACTO",5);
    if( ilACTOPos >= 0 )
        get_real_item (rlJobrec.ACTO, pclCurrent, ilACTOPos+1);
    ilACFRPos = get_item_no(pcpFields,"ACFR",5);
    if( ilACFRPos >= 0 )
        get_real_item (rlJobrec.ACFR, pclCurrent, ilACFRPos+1);
    if( (ilACTOPos >= 0 || ilACFRPos >= 0) && blEquipmentJob == TRUE )
    {
        strcpy( rlJobrec.UEQU, rlJobrec_new.UEQU );
        strcpy( rlJobrec.UAFT, rlJobrec_new.UAFT );
    }

    /* If UAFT, DETY or UTPL are NOT found in the data given by ACTION, get from JOBTAB */
    if( ilUAFTPos < 0 || ilDETYPos < 0 || ilUTPLPos < 0 )
    {
        sprintf( pclSqlBuf, "SELECT TRIM(UAFT), TRIM(DETY), TRIM(UTPL), TRIM(ACFR), TRIM(ACTO) "
                            "FROM JOBTAB WHERE URNO = '%s' ", rlJobrec.URNO );
        dbg( TRACE, "<%s> MissingInfo Sql Query <%s>", pclFunc, pclSqlBuf );

        ilRc = getOrPutDBData(pclSqlBuf,pclData,START);
        if( ilRc != DB_SUCCESS )
        {
            dbg( TRACE, "<%s> Job record not found URNO <%s>", pclFunc, rlJobrec.URNO );
            return;
        }
        if( ilUAFTPos < 0 )
        {
            get_fld(pclData,FIELD_1,STR,URNOLEN,rlJobrec_old.UAFT);        /* UAFT */
            strcpy( rlJobrec_new.UAFT, rlJobrec_old.UAFT );
            dbg( TRACE, "<%s> JOBTAB - UAFT <%s>", pclFunc, rlJobrec_old.UAFT );
        }

        if( ilDETYPos < 0 )
        {
            get_fld(pclData,FIELD_2,STR,DETYLEN,rlJobrec_old.DETY);        /* DETY */
            strcpy( rlJobrec_new.DETY, rlJobrec_old.DETY );
            dbg( TRACE, "<%s> JOBTAB - DETY <%s>", pclFunc, rlJobrec_old.DETY );
        }

        if( ilUTPLPos < 0 )
        {
            get_fld(pclData,FIELD_3,STR,UTPLLEN,rlJobrec_old.UTPL);        /* UTPL */
            strcpy( rlJobrec_new.UTPL, rlJobrec_old.UTPL );
            sprintf( pclTmpStr1, "'%s'", rlJobrec_old.UTPL );
            dbg( TRACE, "<%s> JOBTAB - UTPL <%s>", pclFunc, pclTmpStr1 );
        }
        if( strlen(rlJobrec.ACFR) <= 0 )
            get_fld(pclData,FIELD_4,STR,DATELEN,rlJobrec.ACFR);
        if( strlen(rlJobrec.ACTO) <= 0 )
            get_fld(pclData,FIELD_5,STR,DATELEN,rlJobrec.ACTO);

        if( blEquipmentJob == TRUE )
            strcpy( rlJobrec.UAFT, rlJobrec_new.UAFT );
    }


    /* Check for Apron Templates */
    pclTmpPtr = (char *)strstr( cgJobTemplates, pclTmpStr1 );
    if( pclTmpPtr ==  NULL )
        return;


    /* Send Job */
    blChangesToUpd = TRUE;
    ilRc = PackJobXml( pcgXmlStr, rlJobrec, ilXmlPackType );
    if( ilRc != RC_SUCCESS )
        blChangesToUpd = FALSE;

    if( blChangesToUpd == TRUE )
    {
        dbg( TRACE, "<%s> ilXmlPackType <%d> Xml <%s>", pclFunc, ilXmlPackType, pcgXmlStr );
        ilRc = SendCedaEvent(igITKWMJ,0,mod_name,"","","","ITK","","","",pcgXmlStr,"",
                             atoi(cgSendPrio),RC_SUCCESS);
        dbg( DEBUG, "<%s> ilRc <%d>", pclFunc, ilRc );

        if( blEquipmentJob == FALSE && rlJobrec_old.DETY[0] != '0' && rlJobrec_new.DETY[0] != '0' )
            SendFlightTURN( rlJobrec_new.UAFT, NULL ); 
    }

    /* Chk for turnaround demand job, need to send corresponding job xml */
    if( rlJobrec_new.DETY[0] == '0' )
        blTurnDDJob = TRUE;
    /* No change in DETY and FLIGHT */
    if( ilXmlPackType == TYPE_UPDATE && !strncmp( rlJobrec.DETY, "-1", 2 ) 
        && !strncmp( rlJobrec.UAFT, "-1", 2) )
        return;
    if( blTurnDDJob == TRUE ) 
    {
        sprintf( pclSqlBuf, "SELECT TRIM(URNO), TRIM(FLNO), TRIM(ADID) FROM AFTTAB WHERE RKEY = "
                            "(SELECT RKEY FROM AFTTAB WHERE URNO = '%s') AND FTYP NOT IN (%s) ", 
                            rlJobrec_new.UAFT, pcgExcludeFtyp );
        dbg( TRACE, "<%s> AFTTAB Sql Query <%s>", pclFunc, pclSqlBuf );

        slSqlCursor = 0;
        slSqlFunc = START;
        memset( &rlAftrec, 0, sizeof(AFTREC) );
        while((ilRc = sql_if(slSqlFunc,&slSqlCursor,pclSqlBuf,pclData)) == DB_SUCCESS)
        {
            slSqlFunc = NEXT;
            get_fld(pclData,FIELD_1,STR,URNOLEN,pclUAFT);        /* UAFT */
            get_fld(pclData,FIELD_2,STR,FLNOLEN,pclFLNO); strip_special_char(pclTmpStr2); /* FLNO */
            get_fld(pclData,FIELD_3,STR,ADIDLEN,rlAftrec.ADID);  /* ADID */

            if( blEquipmentJob == FALSE && strlen(rlAftrec.FLNO) > 0 )
            {
                /* Transit flight is classified as Turnaround but SAME flight number */
                if( !strcmp(rlAftrec.FLNO, pclFLNO) )
                {
                    SendFlightTURN ( rlAftrec.URNO, pclUAFT );
                    /*SendFlightTURN ( pclUAFT, rlAftrec.URNO );*/
                }
            }

            strcpy(rlAftrec.URNO, pclUAFT);
            strcpy(rlAftrec.FLNO, pclFLNO);

            dbg( TRACE, "<%s> AFT: UAFT <%s> FLNO <%s> ADID <%s>", pclFunc, rlAftrec.URNO, rlAftrec.FLNO, rlAftrec.ADID );
            dbg( TRACE, "<%s> JOB: new UAFT <%s>", pclFunc, rlJobrec_new.UAFT );

            /* Turnaround Arrival Job */
            if( strcmp(rlAftrec.URNO, rlJobrec_new.UAFT) )
            {
/*              if( rlAftrec.ADID[0] == 'A' && blEquipmentJob == FALSE )
                    return;
                if( rlAftrec.ADID[0] == 'D' && blEquipmentJob == FALSE )
                    return;
*/
                strcpy( rlJobrec.UAFT, rlAftrec.URNO ); 
                /*sprintf( pclTmpStr1, "%c%s", rlAftrec.ADID[0], rlJobrec.URNO ); */
                sprintf( pclTmpStr1, "A%s", rlJobrec.URNO ); 
                strcpy( rlJobrec.URNO, pclTmpStr1 ); 
                
                memset( pcgXmlStr, 0, sizeof(pcgXmlStr) );

                /* Normal to Turnaround */
                if( ilXmlPackType == TYPE_UPDATE && rlJobrec_old.DETY[0] != '0' )
                {
                    ilXmlPackType = TYPE_INSERT;
                    strcpy( rlJobrec.USTF, rlJobrec_new.USTF );
                    strcpy( rlJobrec.UTPL, rlJobrec_new.UTPL );
                    strcpy( rlJobrec.FCCO, rlJobrec_new.FCCO  );
                }

                ilRc = PackJobXml( pcgXmlStr, rlJobrec, ilXmlPackType );
                if( ilRc != RC_SUCCESS )
                    continue;

                dbg( TRACE, "<%s> TurnArnd ilXmlPackType <%d> Xml <%s>", pclFunc, ilXmlPackType, pcgXmlStr );
                ilRc = SendCedaEvent(igITKWMJ,0,mod_name,"","","","ITK","","","",pcgXmlStr,"",
                                     atoi(cgSendPrio),RC_SUCCESS);
                dbg( TRACE, "<%s> TurnArnd ilRc <%d>", pclFunc, ilRc );
            } /* turnaround arrival flight */
        } /* while */
        close_my_cursor(&slSqlCursor);
    } /* Turn Arnd DD job */
    else
    {
        /* From TurnArnd to Normal DD job */
        if( rlJobrec_old.DETY[0] == '0' )
        {
            dbg(TRACE,"<%s> DELETE Turnaround Job <%s>!!!",pclFunc, rlJobrec.URNO );
            sprintf (pclTmpStr1, "A%s", rlJobrec.URNO );
            strcpy( rlJobrec.URNO, pclTmpStr1 );

            ilRc = PackJobXml( pcgXmlStr, rlJobrec, TYPE_DELETE );
            if( ilRc != RC_SUCCESS )
                return;

            dbg( TRACE, "<%s> DELETE Turnaround Job !!! ilRc <%d> Xml <%s>", pclFunc, ilRc, pcgXmlStr );
            ilRc = SendCedaEvent(igITKWMJ,0,mod_name,"","","","ITK","","","",pcgXmlStr,"",
                                 atoi(cgSendPrio),RC_SUCCESS);
            dbg( TRACE, "<%s> ilRc <%d>", pclFunc, ilRc );

            /* 09-OCT-08: Only need to send 1 empty TURN as iTC will amended both flights on the TURN */
            if( blEquipmentJob == FALSE )
                SendFlightTURN ( rlJobrec_new.UAFT, NULL );
        } /* old DETY is turnaround */
    }

    dbg( TRACE,"<%s> ######## End ##########", pclFunc );
} /*HandleJobOnlineData */

static void HandleEquipOnlineData(char *pcpJobUrno)
{
    JOBREC rlJobrec;
    int ilRc;
    char pclSqlBuf[1024] = "\0";
    char pclSqlData[512] = "\0";
    char *pclFunc = "HandleEquipOnlineData";

    dbg(TRACE,"<%s> ---------- START ---------",pclFunc);

    sprintf( pclSqlBuf, "SELECT TRIM(UAFT),TRIM(UEQU),TRIM(ACFR),TRIM(ACTO) FROM JOBTAB "
                        "WHERE URNO = '%s'", pcpJobUrno );
    dbg( TRACE, "<%s> SelEquJob <%s>", pclFunc, pclSqlBuf );

    ilRc = getOrPutDBData(pclSqlBuf,pclSqlData,START);
    if( ilRc != DB_SUCCESS )
    {
        dbg(TRACE,"<%s> EquipJob <%s> not found", pclFunc, pcpJobUrno );
        dbg(TRACE,"<%s> ---------- END ---------",pclFunc );
        return;
    }
    memset( &rlJobrec, 0 , sizeof(JOBREC) );
    strcpy( rlJobrec.URNO, pcpJobUrno );
    get_fld(pclSqlData,FIELD_1,STR,URNOLEN,rlJobrec.UAFT); 
    get_fld(pclSqlData,FIELD_2,STR,URNOLEN,rlJobrec.UEQU); 
    get_fld(pclSqlData,FIELD_3,STR,DATELEN,rlJobrec.ACFR); 
    get_fld(pclSqlData,FIELD_4,STR,DATELEN,rlJobrec.ACTO); 

    PackJobXml( pcgXmlStr, rlJobrec, TYPE_UPDATE );
    dbg( TRACE, "<%s> Equipment Job !!! ilRc <%d> Xml <%s>", pclFunc, ilRc, pcgXmlStr );
    ilRc = SendCedaEvent(igITKWMJ,0,mod_name,"","","","ITK","","","",pcgXmlStr,"",
                         atoi(cgSendPrio),RC_SUCCESS);
    dbg( TRACE, "<%s> ilRc <%d>", pclFunc, ilRc );

    dbg(TRACE,"<%s> ---------- END ---------",pclFunc);
}

static void HandleStaffOnlineData(char *pcpFields, char *pcpData, char *pcpCmd, char *pcpSelection)
{
    STFREC rlStfrec;
    int ilRc = RC_SUCCESS;
    int ilItemCount = 0;
    int ilItemPosition = 0;
    char clAction;
    char pclTmpStr[512] = "\0";     
    char pclData[2][1024];
    char *pclFunc = "HandleStaffOnlineData";
    char *pclToken;


    dbg(TRACE,"<%s> ---------- START ---------",pclFunc);

    memset( pclData, 0, sizeof(pclData) );

    strcpy( pclData[0], pcpData );
    pclToken = (char *)strstr( pclData[0], "\n" );
    if( pclToken != NULL )
    {
        *pclToken = '\0';
        pclToken++;
        strcpy( pclData[1], pclToken );
    }
    strip_special_char(pclData[0]);
    strip_special_char(pclData[1]);
    dbg(DEBUG,"<%s> Str1 <%s>",pclFunc, pclData[0] );
    dbg(DEBUG,"<%s> Str2 <%s>",pclFunc, pclData[1] );

    memset( &rlStfrec, 0, sizeof(STFREC) );
    /* Delete */
    if( !strncmp( pcpCmd, "DRT", 3 ) )
    {
        pclToken = (char *)strstr( pcpSelection, "=" );
        pclToken++;
        strcpy( rlStfrec.URNO, pclToken );
        TrimSpace( rlStfrec.URNO );
        clAction = 'D';
    }
    else
    {
        clAction = 'U';
        if( !strncmp( pcpCmd, "IRT", 3 ) )
            clAction = 'I';

        ilItemPosition = get_item_no(pcpFields,"PENO",5);
        if( ilItemPosition >= 0 )
            get_real_item( rlStfrec.PENO, pclData[0], ilItemPosition+1 );
        ilItemPosition = get_item_no(pcpFields,"URNO",5);
        if( ilItemPosition >= 0 )
            get_real_item( rlStfrec.URNO, pclData[0], ilItemPosition+1 );
        ilItemPosition = get_item_no(pcpFields,"FINM",5);
        if( ilItemPosition >= 0 )
            get_real_item( rlStfrec.FINM, pclData[0], ilItemPosition+1 );
        ilItemPosition = get_item_no(pcpFields,"LANM",5);
        if( ilItemPosition >= 0 )
            get_real_item( rlStfrec.LANM, pclData[0], ilItemPosition+1 );
    }
    PackStaffXml( rlStfrec, clAction, pclTmpStr );
    sprintf( pcgXmlStr, "<STFS>\n"
                        "%s"
                        "</STFS>", pclTmpStr );
    dbg(TRACE,"<%s> Xml <%s>",pclFunc, pcgXmlStr);
    ilRc = SendCedaEvent(igITKREQ,0,mod_name,"","","","ITK","","","", pcgXmlStr,"",atoi(cgSendPrio),RC_SUCCESS);
    if( ilRc != RC_SUCCESS )
        dbg(TRACE,"<%s> Sending to MQ Fail",pclFunc );

    dbg(TRACE,"<%s> ---------- END ---------",pclFunc);
                                    
} /* HandleStaffOnline */

static void HandleATDMQ(char *pclFields, char *pcpData)
{
    int ilRc = RC_SUCCESS;
    long llMqrUrno = 0;
    short slSqlFunc = 0;
    char pclTmpStr[MQRDLEN] = "\0";
    char pclData[MQRDLEN] = "\0";
    char *pclFunc = "HandleATDMQ";
    int ilFound;

    dbg(TRACE,"<%s> ========== Start ==========", pclFunc);

    /* Get the URNO of MQRTAB */
    /*llMqrUrno = atol(pcpData);*/

    /* Below line will fetch the message(DATA) from MQRTAB for URNO */
    /*slSqlFunc = START;
    sprintf(pclSqlBuf,"SELECT DATA FROM MQRTAB WHERE URNO = %d",llMqrUrno);
    dbg(DEBUG,"<%s> now read from DB: <%s>",pclFunc,pclSqlBuf);
    ilRc = getOrPutDBData(pclSqlBuf,pclData,slSqlFunc);*/

    strcpy( pclData, pcpData );
    /* check for any special characters available in the message */
    ConvertDbStringToClient(pclData); 
    dbg(TRACE,"<%s> ------------- Message received: -------------- <%s>",pclFunc,pclData);

    ilFound = GetElementValue(pclTmpStr, pclData, "<DFLIGHT>", "</DFLIGHT>");
    if( ilFound == TRUE )
    {
        dbg(TRACE,"<%s> passing msg to HandleStatusUpd",pclFunc);
        HandleStatusUpd(llMqrUrno,pclData);
        return;
    }
    ilFound = GetElementValue(pclTmpStr, pclData, "<AFLIGHT>", "</AFLIGHT>");
    if( ilFound == TRUE )
    {
        dbg(TRACE,"<%s> passing msg to HandleStatusUpd",pclFunc);
        HandleStatusUpd(llMqrUrno,pclData);
        return;
    }
    ilFound = GetElementValue(pclTmpStr, pclData, "<TRIP>", "</TRIP>");
    if( ilFound == TRUE )
    {
        HandleTripInfo(llMqrUrno, pclData); 
        return;
    }
    ilFound = GetElementValue(pclTmpStr, pclData, "<ATD>", "</ATD>");
    if( ilFound == TRUE )
    {   /* Phase I */
        dbg(TRACE,"<%s> passing msg to HandleATDUpdate",pclFunc); 
        HandleATDUpdate(llMqrUrno, pclData);
        return;
    }
    dbg(TRACE,"<%s> ========== End ==========",pclFunc);
} /* HandleATDMQ */

static void HandleMSGMQ(char *pclFields, char *pcpData)
{
    int ilRc = RC_SUCCESS;
    long llMqrUrno = 0;
    short slSqlFunc = 0;
    char pclTmpStr[MQRDLEN] = "\0";
    /*char pclSqlBuf[DBQRLEN] = "\0";*/
    char pclData[MQRDLEN] = "\0";
    char *pclFunc = "HandleMSGMQ";
    int ilFound;

    dbg(TRACE,"<%s> ========== Start ==========", pclFunc);

    /* Get the URNO of MQRTAB */
    /*llMqrUrno = atol(pcpData);*/

    /* Below line will fetch the message(DATA) from MQRTAB for URNO */
    /*slSqlFunc = START;
    sprintf(pclSqlBuf,"SELECT DATA FROM MQRTAB WHERE URNO = %d",llMqrUrno);
    dbg(DEBUG,"<%s> now read from DB: <%s>",pclFunc,pclSqlBuf);
    ilRc = getOrPutDBData(pclSqlBuf,pclData,slSqlFunc);*/

    strcpy( pclData, pcpData );
    /* check for any special characters available in the message */
    ConvertDbStringToClient(pclData); 
    dbg(TRACE,"<%s> ------------- Message received: -------------- <%s>",pclFunc,pclData);


    
    ilFound = GetElementValue(pclTmpStr, pclData, "<MESSAGE>", "</MESSAGE>");
    if( ilFound == TRUE )
    {   
        dbg(TRACE,"<%s> passing msg to HandleMessageQuery",pclFunc); 
        HandleMessageQuery(llMqrUrno, pclData);
        return;
    }
    ilFound = GetElementValue(pclTmpStr, pclData, "<MESSAGETO>", "</MESSAGETO>");
    if( ilFound == TRUE )
    {
        dbg(TRACE,"<%s> passing msg to HandleMessageQuery",pclFunc); 
        HandleMessageQuery(llMqrUrno, pclData);
        return;
    }
    ilFound = GetElementValue(pclTmpStr, pclData, "<MSG>", "</MSG>");
    if( ilFound == TRUE )
    {
        dbg(TRACE,"<%s> passing msg to HandleMessaging",pclFunc);
        HandleMessaging(llMqrUrno,pclData);
        return;
    }
    ilFound = GetElementValue(pclTmpStr, pclData, "<MSGTO>", "</MSGTO>");
    if( ilFound == TRUE )
    {
        dbg(TRACE,"<%s> passing msg to HandleMessaging",pclFunc);
        HandleMessaging(llMqrUrno,pclData);
        return;
    }
    ilFound = GetElementValue(pclTmpStr, pclData, "<MSGLOG_REQ>", "</MSGLOG_REQ>");
    if( ilFound == TRUE )
    {
        dbg(TRACE,"<%s> passing msg to HandleMessageLogRequest",pclFunc);
        HandleMessageLogRequest(llMqrUrno,pclData);
        return;
    }
    ilFound = GetElementValue(pclTmpStr, pclData, "<IMSGD>", "</IMSGD>");
    if( ilFound == TRUE )
    {
        dbg(TRACE,"<%s> passing msg to HandleOneMessage",pclFunc);
        HandleOneMessage(llMqrUrno,pclData);
        return;
    }
    dbg(TRACE,"<%s> ========== End ==========",pclFunc);
} /* HandleMSGMQ */

static void HandleATDUpdate(long lpMqrUrno, char *pcpData)
{
    
    int ilRc = RC_SUCCESS;
    long llMqrUrno = 0;
    long llLoaUrno = 0;
    short slSqlFunc = 0;
    char pclUrno[20] = "\0";
    char pclAtd[20] = "\0";
    char pclAta[20] = "\0";
    char pclSelection[50] = "\0";
    /*char clSqlBuf[DBQRLEN] = "\0";*/
    char pclData[DBQRLEN] = "\0";
    char *pclFunc = "HandleATDUpdate";
    int ilUrnoFound = 0;
    int ilAtdFound = 0;
    int ilAtaFound = 0;

    dbg(TRACE,"<%s> ========== Start ==========", pclFunc);

    llMqrUrno = lpMqrUrno;
    /*memcpy( pclData, pcpData, strlen(pcpData) );*/
    strcpy( pclData, pcpData );

    ilUrnoFound = GetElementValue(pclUrno, pclData, "<URNO>", "</URNO>");
    ilAtdFound  = GetElementValue(pclAtd, pclData, "<ATD>", "</ATD>");
    ilAtaFound  = GetElementValue(pclAta, pclData, "<ATA>", "</ATA>");

    if(ilUrnoFound)
    {
        sprintf(pclSelection,"WHERE URNO=%s",pclUrno);
        if(ilAtdFound)
        {
            LocalTimeToUtcFixTZ(pclAtd);
            ilRc = SendCedaEvent(igFLIGHT, 0, mod_name, "", "", "","UFR", "AFTTAB", 
                                 pclSelection,"OFBS", pclAtd, "", atoi(cgSendPrio), NETOUT_NO_ACK) ;
        }
        else if(ilAtaFound)
        {
            LocalTimeToUtcFixTZ(pclAta);
            ilRc = SendCedaEvent(igFLIGHT, 0, mod_name, "", "", "","UFR", "AFTTAB", 
                                 pclSelection,"ONBS", pclAta, "", atoi(cgSendPrio), NETOUT_NO_ACK) ;
        }
    }
    else
        dbg(TRACE,"<%s> ATD Message is wrong, Message:<%s>",pclFunc, pclData);
    dbg(TRACE,"<%s> ========== End ==========",pclFunc);
} /* HandleATDUpdate */

static void HandleRequest(char *pclFields, char *pclData)
{
    int ilRc = RC_SUCCESS;
    long llMqrUrno = 0;
    long llLoaUrno = 0;
    short slSqlFunc = 0;
    /*char clSqlBuf[250] = "\0";*/
    char clData[DBQRLEN] = "\0";
    char request[10] = "\0";

    dbg(TRACE,"%05d: ========== Start HandleRequest ==========",__LINE__);

    strcpy( clData, pclData ); 
    dbg(TRACE,"%05d: ------------- Message received: -------------- \n<%s>",__LINE__,clData);
    
    strncpy(request,clData,9);

    if( !strncmp(clData,"<SHOWDLS>", 9) )
        getAndSendShowDLS(clData);
    else if( !strncmp(clData,"<ALLOK2L>",9) )
        getAndSendAllOK2L(clData);
    else if( !strncmp(clData,"<HAG>",5) )
        getAndSendTask(clData);
    else if( !strncmp(clData,"<STFREQ>",8) )
        getAndSendStaff(clData);

    dbg(TRACE,"%05d: ========== End HandleRequest ==========",__LINE__);
} /* HandleRequest */

static void getAndSendShowDLS(char *pcpData)
{
    int ilRc = RC_SUCCESS;
    BOOL blUrnoFound;
    BOOL blSessionFound;
    char pclSqlBuf[512] = "\0";
    char pclSqlData[1024] = "\0";
    char pclSession[72] = "\0";
    char pclTxt1[4004] = "\0";
    char pclFinalTxt[8008] = "\0";
    char pclUaft[40] = "\0";
    /*char pclGtd1[40] = "\0";
    char pclTifd[40] = "\0";*/
    char *pclFunc = "getAndSendShowDLS";

    dbg( TRACE,"<%s> ========== Start ==========", pclFunc );
    blUrnoFound = GetElementValue( pclUaft, pcpData, "<URNO>", "</URNO>" );
    blSessionFound = GetElementValue( pclSession, pcpData, "<SESSIONID>", "</SESSIONID>" );

    if( blUrnoFound && blSessionFound )
    {
        TrimSpace( pclUaft );
        sprintf( pclSqlBuf,"SELECT TRIM(TXT1) FROM TLXTAB WHERE TTYP='DLS' AND FLNU = %s ORDER BY CDAT DESC", pclUaft );
        dbg( TRACE,"<%s> SelTLX <%s>", pclFunc, pclSqlBuf );
        ilRc = getOrPutDBData( pclSqlBuf,pclTxt1,START );

        if( ilRc == DB_SUCCESS )
        {
            dbg( DEBUG, "<%s> Txt1 (%100s)", pclFunc, pclTxt1 );
            cgDLSDelimitter = pclTxt1[0];
            printTelex( pclTxt1 );
           /* sprintf( pclSqlBuf,"SELECT TRIM(GTD1),TRIM(TIFD) FROM AFTTAB WHERE URNO = %s", pclUaft );
            dbg( TRACE,"<%s> SelAFT <%s>", pclFunc, pclSqlBuf );

            ilRc = getOrPutDBData( pclSqlBuf,pclSqlData,START );
            if( ilRc == DB_SUCCESS )
            {
                get_fld(pclSqlData,FIELD_1,STR,GATELEN,pclGtd1);
                get_fld(pclSqlData,FIELD_2,STR,DATELEN,pclTifd);
                TrimSpace( pclGtd1 );
                TrimSpace( pclTifd );
                dbg( DEBUG, "<%s> Gtd1=(%s) Tifd=(%s)", pclFunc, pclGtd1, pclTifd );
            }
            FormatDLS( pclUaft, pclTxt1, pclGtd1, pclTifd, pclFinalTxt );*/
            FormatDLS( pclTxt1, pclFinalTxt );
            dbg( DEBUG, "<%s> Final Text (%s)", pclFunc, pclFinalTxt );
        }
        sprintf( pcgXmlStr ,"<SHOWDLS>\n"
                            "<URNO>%s</URNO>\n"
                            "<SESSIONID>%s</SESSIONID>\n"
                            "<DLS>%s</DLS>\n"
                            "</SHOWDLS>\n", pclUaft, pclSession, pclFinalTxt );

        dbg( TRACE," DLS Message To Sent: %s", pcgXmlStr );

        ilRc = SendCedaEvent(igITKREQ,0, " ", " ", " ", " ","ITK", " ", " "," ", pcgXmlStr, "", 4, 0) ;
    }
    else
        dbg( TRACE, "<%s> ----  Missing URNO/SESSION ----- ", pclFunc );

    dbg( TRACE, "<%s> ========== End ==========", pclFunc );
} /* getAndSendShowDLS */


static void getAndSendAllOK2L(char *reqData)
{
    int ilRc = RC_SUCCESS;
    long llMqrUrno = 0;
    long llLoaUrno = 0;
    short slSqlFunc = 0;
    char clSqlBuf[250] = "\0";
    char clData[MQRDLEN] = "\0";
    char resMessage[MQRDLEN] = "\0";
    char request[10] = "\0";
    char urno[20] = "\0";
    char sesMes[50] = "\0";
    int urnoFound;
    int sesFound;
    int recCount=0;
    char strTemp[50] = "\0";
    short slCursor = 0;

    dbg(DEBUG,"%05d: ========== Start getAndSendAllOK2L ==========",__LINE__);
    urnoFound = GetElementValue(urno, reqData, "<URNO>", "</URNO>");
    sesFound  = GetElementValue(sesMes, reqData, "<SESSIONID>", "</SESSIONID>");

    if(urnoFound && sesFound)
    {
        slSqlFunc = START;
        /* Changed below query to select OKL messages based on order by TIME. */
        sprintf(clSqlBuf,"SELECT TRIM(ULDT) FROM LOATAB WHERE DSSO='OKL' AND FLNU = %s ORDER BY TIME DESC",urno);
        dbg(DEBUG,"%05d: now read from DB: <%s>",__LINE__,clSqlBuf);
        
        sprintf(resMessage,"<ALLOK2L>\n<URNO>%s</URNO>\n<SESSIONID>%s</SESSIONID>\n",urno,sesMes);

        while((ilRc = sql_if(slSqlFunc,&slCursor,clSqlBuf,clData)) == DB_SUCCESS)
        {
            recCount++;
            strip_special_char(clData);
            sprintf(strTemp,"<ULD%d>%s</ULD%d>\n",recCount,clData,recCount);
            strcat(resMessage,strTemp);

            slSqlFunc = NEXT;
        }
        close_my_cursor(&slCursor);
        /*commit_work();*/
        slCursor = 0;

        strcat(resMessage,"</ALLOK2L>\n");
        
        dbg(TRACE,"  ALL OK2L Message To Sent %s",resMessage);

        ilRc = SendCedaEvent(igITKREQ,0, " ", " ", " ", " ","ITK", " ", " "," ", resMessage, "", 4, 0) ;
    }
    else
    {
        dbg(TRACE,"----  Received DLS Message %s is wrong ----- ",reqData);
    }
    
    dbg(DEBUG,"%05d: ========== End getAndSendAllOK2L ==========",__LINE__);
} /* getAndSendAllOK2L */


/********************************************************************************
 Get the ALC3/ALC2 which are tasked by the required worker group
********************************************************************************/
void getAndSendTask( char *pcpData )
{
    int ilRc;
    int ili;
    int ilCount;
    short slSqlCursor;
    short slSqlFunc;
    char pclSqlBuf[512];
    char pclGrpList[512];
    char pclOneGrp[40];
    char pclTmpStr[512];
    char pclAirlines[5120];
    char *pclToken;
    char *pclFunc = "getAndSendTask";

    dbg( TRACE, "<%s> --------- START ----------", pclFunc );

    ilRc = GetElementValue(pclGrpList, pcpData, "<HAG>", "</HAG>");
    if( ilRc != TRUE )
    {
        dbg( TRACE, "<%s> Invalid <HAG> received", pclFunc );
        return;
    }

    memset( pcgXmlStr, 0, sizeof(pcgXmlStr) ); 
    sprintf( pcgXmlStr, "%s\n", "<?xml version=\"1.0\" standalone=\"yes\"?>\n"
                                "<DSTasks xmlns=\"http://tempuri.org/DSTasks.xsd\">" );
    pclToken = (char *)strtok( pclGrpList, ";" );
    while( pclToken != NULL )
    {
        strcpy( pclOneGrp, pclToken );
        pclToken = (char *)strtok( NULL, ";" );
        sprintf( pclSqlBuf, "SELECT TRIM(ALC2) FROM ALTTAB WHERE ALC2 <> ' ' AND URNO IN "
                            "(SELECT DISTINCT(ALTU) FROM HAITAB WHERE HSNA IN (%s) AND TASK LIKE '%s%%')", 
                            pcgReqdAgent, pclOneGrp );
        dbg( TRACE, "<%s> GetALC2 SqlBuf <%s>", pclFunc, pclSqlBuf );

        memset( &rgDbReturn, 0, sizeof(rgDbReturn) );
        rgDbReturn.RecSep = ';';
        slSqlCursor = 0;
        slSqlFunc = START;
        ilCount = 0;
        memset( pclAirlines, 0, sizeof(pclAirlines) );

        while( (ilRc = SqlIfArray( slSqlFunc, &slSqlCursor, pclSqlBuf, 0, 1000, &rgDbReturn )) >= 0 )
        {
            if( rgDbReturn.LineCount <= 0 )
                break;

            slSqlFunc = NEXT;
            if( rgDbReturn.AppendBuf == TRUE )
                strcat( pclAirlines, ";" );
            strcat( pclAirlines, rgDbReturn.Value );
            ilCount += rgDbReturn.LineCount;

            if( rgDbReturn.LineCount < 1000 )
                break;
        }
        close_my_cursor(&slSqlCursor);

        if( ilCount > 0 )
            strcat( pclAirlines, ";" );
        sprintf( pclSqlBuf, "SELECT TRIM(ALC3) FROM ALTTAB WHERE ALC2 = ' ' AND URNO IN "
                            "(SELECT DISTINCT(ALTU) FROM HAITAB WHERE HSNA IN (%s) AND TASK LIKE '%s%%')", 
                            pcgReqdAgent, pclOneGrp );
        dbg( TRACE, "<%s> GetALC3 SqlBuf <%s>", pclFunc, pclSqlBuf );

        memset( &rgDbReturn, 0, sizeof(rgDbReturn) );
        rgDbReturn.RecSep = ';';
        slSqlCursor = 0;
        slSqlFunc = START;

        while( (ilRc = SqlIfArray( slSqlFunc, &slSqlCursor, pclSqlBuf, 0, 1000, &rgDbReturn )) >= 0 )
        {
            if( rgDbReturn.LineCount <= 0 )
                break;

            slSqlFunc = NEXT;
            if( rgDbReturn.AppendBuf == TRUE )
                strcat( pclAirlines, ";" );
            strcat( pclAirlines, rgDbReturn.Value );
            ilCount += rgDbReturn.LineCount;

            if( rgDbReturn.LineCount < 1000 )
                break;
        }
        close_my_cursor(&slSqlCursor);
        sprintf( pclTmpStr, "<HAG>\n"
                            "<TASK>%s</TASK>\n"
                            "<ALC2>%s</ALC2>\n"
                            "</HAG>\n", pclOneGrp, pclAirlines );
        strcat( pcgXmlStr, pclTmpStr );
    }
    strcat( pcgXmlStr, "</DSTasks>" );

    ilRc = SendCedaEvent(igITKREQ,0, " ", " ", " ", " ","ITK", " ", " "," ", pcgXmlStr, "", 3, 0) ;
    dbg (TRACE, "<%s> pcgXmlStr (%s)", pclFunc, pcgXmlStr );
    dbg (TRACE, "<%s> ilRc for SendCedaEvent = %d", pclFunc, ilRc );

    dbg( TRACE, "<%s> --------- END ----------", pclFunc );

} /* getAndSendTask */

/********************************************************************************
 Get the ALC3/ALC2 which are tasked by the required worker group
********************************************************************************/
void getAndSendStaff( char *pcpData )
{
    STFREC rlStfrec;
    int ilRc;
    int ili;
    int ilCount;
    int ilFound;
    int ilUntagCnt = 0;
    char pclTmpStr[512] = "\0";
    char *pclFunc = "getAndSendStaff";

    dbg( TRACE, "<%s> --------- START ----------", pclFunc );

    ilRc = GetElementValue(pclTmpStr, pcpData, "<STFREQ>", "</STFREQ>");
    if( ilRc != TRUE )
    {
        dbg( TRACE, "<%s> Invalid <STFREQ> received", pclFunc );
        return;
    }

    memset( pcgXmlStr, 0, sizeof(pcgXmlStr) ); 
    /*sprintf( pcgXmlStr, "%s", "<?xml version=\"1.0\" standalone=\"yes\"?>"
                                "<DSTasks xmlns=\"http://tempuri.org/DSTasks.xsd\">\n" ); */
    sprintf( pcgXmlStr, "%s\n", "<STFS>" );

    ilUntagCnt = 0; 
    ilFound = TRUE;
    while( ilFound == TRUE )
    {
        ilUntagCnt++;
        memset( &rlStfrec, 0, sizeof( STFREC ) );
        ilFound = xml_untag( rlStfrec.PENO, pcpData, "PNO", ilUntagCnt );
        if( ilFound == FALSE )
        {
            if( ilUntagCnt > 1 ) /* No more element */ 
               break;
            /* No recipent stated */
            dbg(TRACE,"<%s> Tag <PNO> is missing Msg:%s",pclFunc, pcpData);
            return;
        }
        dbg(DEBUG,"<%s> STFREC-PENO <%s> UntagCnt <%d>", pclFunc, rlStfrec.PENO, ilUntagCnt );

        ilRc = GetStaffName( rlStfrec.PENO, rlStfrec.URNO, rlStfrec.FINM, rlStfrec.LANM, SRCH_BY_PENO );
        if( ilRc != RC_SUCCESS )
        {
            dbg( TRACE, "<%s> PENO <%s> not found in STFTAB", pclFunc, rlStfrec.PENO );
            return;
        }
        dbg(DEBUG,"<%s> STFREC-URNO <%s>", pclFunc, rlStfrec.URNO );
        dbg(DEBUG,"<%s> STFREC-FINM <%s>", pclFunc, rlStfrec.FINM );
        dbg(DEBUG,"<%s> STFREC-LANM <%s>", pclFunc, rlStfrec.LANM );

        PackStaffXml( rlStfrec, 'R', pclTmpStr );
        strcat( pcgXmlStr, pclTmpStr );
    }
    
    strcat( pcgXmlStr, "</STFS>" );

    ilRc = SendCedaEvent(igITKREQ,0, " ", " ", " ", " ","ITK", " ", " "," ", pcgXmlStr, "", 3, 0) ;
    dbg (TRACE, "<%s> pcgXmlStr (%s)", pclFunc, pcgXmlStr );
    dbg (TRACE, "<%s> ilRc for SendCedaEvent = %d", pclFunc, ilRc );
    dbg( TRACE, "<%s> --------- END ----------", pclFunc );

} /* getAndSendStaff */

int PackStaffXml( STFREC rpStfrec, char cpReas, char *pcpXmlStr )
{
    char *pclFunc = "PackStaffXml";

    sprintf( pcpXmlStr, "<STF>\n"
                        "<URNO>%s</URNO>\n"
                        "<PNO>%s</PNO>\n"
                        "<FNM>%s</FNM>\n"
                        "<LNM>%s</LNM>\n"
                        "<CHG>%c</CHG>\n"
                        "</STF>\n", rpStfrec.URNO, rpStfrec.PENO, rpStfrec.FINM, 
                                    rpStfrec.LANM, cpReas );
    return RC_SUCCESS;

} /* PackStaffXml */


static int GetElementValue(char *pclDest, char *pclOrigData, char *pclBegin, char *pclEnd)
{
    char *pclTemp = NULL;
    long llSize = 0;
    
    pclTemp = CedaGetKeyItem(pclDest, &llSize, pclOrigData, pclBegin, pclEnd, TRUE);
    if(pclTemp != NULL)
        return TRUE;
    else
        return FALSE;
} /* GetElementValue */

static int TriggerAction(char *pcpTableName)
{
  int           ilRc          = RC_SUCCESS ;    /* Return code */
  EVENT        *prlEvent      = NULL ;
  BC_HEAD      *prlBchead     = NULL ;
  CMDBLK       *prlCmdblk     = NULL ;
  /*ACTIONConfig *prlAction     = NULL ;*/
    ACTIONConfig rlAction;
    
  char         *pclSelection  = NULL ;
  char         *pclFields     = NULL ;
  char         *pclData       = NULL ;
  long          llActSize     = 0 ;
  int           ilAnswerQueue = 0 ;
  char          clQueueName[16] ;
    
  if(ilRc == RC_SUCCESS)
  {

  }/* end of if */
    

  if (ilRc == RC_SUCCESS)
  {
   sprintf (&clQueueName[0],"%s2", mod_name) ;

   ilRc = GetDynamicQueue(&ilAnswerQueue,&clQueueName[0]);
   if (ilRc != RC_SUCCESS)
   {
    dbg(TRACE,"TriggerAction: GetDynamicQueue failed <%d>",ilRc);
   }
   else
   {
    dbg(DEBUG,"TriggerAction: got <%d> for <%s>",ilAnswerQueue, &clQueueName[0]);
   }/* end of if */
  }/* end of if */
    
  if (ilRc == RC_SUCCESS)
  {
   llActSize = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + sizeof(ACTIONConfig) + 666;
   prgOutEvent = realloc(prgOutEvent,llActSize);
   if(prgOutEvent == NULL)
   {
    dbg(TRACE,"TriggerAction: realloc out event <%d> bytes failed",llActSize);
    ilRc = RC_FAIL ;
   } /* end of if */
  } /* end of if */

  if(ilRc == RC_SUCCESS)
  {
   prlBchead     = (BC_HEAD *) ((char *) prgOutEvent + sizeof(EVENT)) ;
   prlCmdblk     = (CMDBLK *)  ((char *) prlBchead->data) ;
   pclSelection  = prlCmdblk->data ;
   pclFields     = pclSelection + strlen (pclSelection) + 1 ;
   pclData       = pclFields + strlen (pclFields) + 1 ;
   strcpy (pclData, "DYN") ;
    
        memset(&rlAction,0,sizeof(ACTIONConfig));
        
    prlBchead->rc = RC_SUCCESS;
    
   prgOutEvent->type        = USR_EVENT ;
   prgOutEvent->command     = EVENT_DATA ;
   prgOutEvent->originator  = ilAnswerQueue ;
   prgOutEvent->retry_count = 0 ;
   prgOutEvent->data_offset = sizeof(EVENT) ;
   prgOutEvent->data_length = llActSize-sizeof(EVENT) ;

   rlAction.iEmptyFieldHandling = iDELETE_ALL_BLANKS ;
   rlAction.iIgnoreEmptyFields = 0 ;
   strcpy(rlAction.pcSndCmd, "") ;
   sprintf(rlAction.pcSectionName,"%s_%s", mod_name,pcpTableName) ;
   strcpy(rlAction.pcTableName, pcpTableName) ;
   strcpy(rlAction.pcFields, "-") ;
     /*********************** MCU TEST ***************/
   strcpy(rlAction.pcFields, "") ;
     /*********************** MCU TEST ***************/
     /************/
     if (!strcmp(pcpTableName,"SPRTAB"))
        {
    strcpy(rlAction.pcFields, "PKNO,USTF,ACTI,FCOL") ;
     }
     /************/
   strcpy(rlAction.pcSectionCommands, "IRT,URT,DRT") ;
   rlAction.iModID = mod_id ;

   /****************************************
     DebugPrintEvent(DEBUG,prgOutEvent) ;
     DebugPrintBchead(DEBUG,prlBchead) ;
     DebugPrintCmdblk(DEBUG,prlCmdblk) ;
     dbg (DEBUG,"TriggerAction: selection <%s>",pclSelection) ;
     dbg (DEBUG,"TriggerAction: fields    <%s>",pclFields) ;
     dbg (DEBUG,"TriggerAction: data      <%s>",pclData) ;
     DebugPrintACTIONConfig(DEBUG,&rlAction) ;
   ****************************************/

   if(ilRc == RC_SUCCESS)
   {
    rlAction.iADFlag = iDELETE_SECTION ;
             memcpy(pclData + strlen(pclData) + 1,&rlAction,sizeof(ACTIONConfig));

    ilRc = que(QUE_PUT, 7400, ilAnswerQueue, PRIORITY_3, llActSize, (char *) prgOutEvent) ; 
    if(ilRc != RC_SUCCESS)
    {
     dbg(TRACE,"TriggerAction: QUE_PUT failed <%d>",ilRc) ;
    }
    else
    {
     ilRc = WaitAndCheckQueue(10, ilAnswerQueue,&prgItem) ;
     if(ilRc != RC_SUCCESS)
     {
      dbg(TRACE,"TriggerAction: WaitAndCheckQueue failed <%d>",ilRc) ;
     }
     else
     {
      /* return value for delete does not matter !!! */
        
      /****************************************
      prlEvent     = (EVENT *)   ((char *)prgItem->text) ;
      prlBchead    = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT)) ;
      prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data) ;
      pclSelection = (char *)    prlCmdblk->data ;
      pclFields    = (char *)    pclSelection + strlen(pclSelection) + 1;
      pclData      = (char *)    pclFields + strlen(pclFields) + 1;
      DebugPrintItem(DEBUG, prgItem) ;
      DebugPrintEvent(DEBUG, prlEvent) ;
      DebugPrintBchead(DEBUG, prlBchead) ;
      DebugPrintCmdblk(DEBUG, prlCmdblk) ;
      dbg(DEBUG,"TriggerAction: selection <%s>",pclSelection) ;
      dbg(DEBUG,"TriggerAction: fields    <%s>",pclFields) ;
      dbg(DEBUG,"TriggerAction: data      <%s>",pclData) ;
      ****************************************/
    } /* end of if */
   }/* end of if */
  }/* end of if */

  if(ilRc == RC_SUCCESS)
  {
    rlAction.iADFlag = iADD_SECTION ;
      memcpy(pclData + strlen(pclData) + 1,&rlAction,sizeof(ACTIONConfig));
         
   ilRc = que(QUE_PUT, 7400, ilAnswerQueue, PRIORITY_3, llActSize,
                                              (char *) prgOutEvent ) ;
   if(ilRc != RC_SUCCESS)
   {
    dbg(TRACE,"TriggerAction: QUE_PUT failed <%d>",ilRc) ;
   }
   else
   {
    ilRc = WaitAndCheckQueue(10,ilAnswerQueue,&prgItem) ;
    if(ilRc != RC_SUCCESS)
    {
     dbg(TRACE,"TriggerAction: WaitAndCheckQueue failed <%d>",ilRc) ;
    }
    else
    {
     prlEvent     = (EVENT *)   ((char *)prgItem->text) ;
     prlBchead    = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT)) ;
     prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data) ;
     pclSelection = (char *)    prlCmdblk->data ;
     pclFields    = (char *)    pclSelection + strlen(pclSelection) + 1 ;
     pclData      = (char *)    pclFields + strlen(pclFields) + 1 ;

     if(strcmp(pclData,"SUCCESS") != 0)
     {
      dbg(TRACE,"TriggerAction: add dynamic action config failed") ;
      DebugPrintItem(DEBUG,prgItem) ;
      DebugPrintEvent(DEBUG,prlEvent) ;
      DebugPrintBchead(DEBUG,prlBchead) ;
      DebugPrintCmdblk(DEBUG,prlCmdblk) ;
      dbg (DEBUG,"TriggerAction: selection <%s>",pclSelection) ;
      dbg (DEBUG,"TriggerAction: fields    <%s>",pclFields) ;
      dbg(DEBUG,"TriggerAction: data      <%s>",pclData) ;
      ilRc = RC_FAIL ;
     }
     else
     {
      dbg(DEBUG,"TriggerAction: add dynamic action config OK") ;
     }/* end of if */
    }/* end of if */
   }/* end of if */
  }/* end of if */

  ilRc = que(QUE_DELETE,ilAnswerQueue,ilAnswerQueue,0,0,0) ;
  if(ilRc != RC_SUCCESS)
  {
   dbg(TRACE,"que QUE_DELETE <%d> failed <%d>",ilAnswerQueue, ilRc) ;
  }
  else
  {
   dbg(DEBUG,"TriggerAction: queue <%d> <%s> deleted", ilAnswerQueue, &clQueueName[0]) ;
  }/* end of if */
 }/* end of if */

 return (ilRc) ;
    
}
static int WaitAndCheckQueue(int ipTimeout, int ipModId, ITEM **prpItem)
{
    int ilRc          = RC_SUCCESS;                      /* Return code */
    int ilWaitCounter = 0;

    do
    {
        sleep(1);
        ilWaitCounter += 1;

        ilRc = CheckQueue(ipModId,prpItem);
        if(ilRc != RC_SUCCESS)
        {
            if(ilRc != QUE_E_NOMSG)
            {
                dbg(TRACE,"WaitAndCheckQueue: CheckQueue <%d> failed <%d>",mod_id,ilRc);
            }/* end of if */
        }/* end of if */
    }while((ilRc == QUE_E_NOMSG) && (ilWaitCounter < ipTimeout));

    if(ilWaitCounter >= ipTimeout)
    {
        dbg(TRACE,"WaitAndCheckQueue: timeout reached <%d>",ipTimeout);
        ilRc = RC_FAIL;
    }/* end of if */

    return ilRc;
}


static int CheckQueue(int ipModId, ITEM **prpItem)
{
    int     ilRc       = RC_SUCCESS;                      /* Return code */
    int     ilItemSize = 0;
    EVENT *prlEvent    = NULL;
static  ITEM  *prlItem      = NULL;
    
    ilItemSize = I_SIZE;
    
    ilRc = que(QUE_GETBIGNW,ipModId,ipModId,PRIORITY_3,ilItemSize,(char *) &prlItem);
    if(ilRc == RC_SUCCESS)
    {
        dbg(TRACE,"DebugPrintItem with prlItem follows");
        DebugPrintItem(TRACE,prlItem);
        *prpItem = prlItem;
        dbg(TRACE,"DebugPrintItem with prpItem follows");
        DebugPrintItem(TRACE,*prpItem);
        prlEvent = (EVENT*) ((prlItem)->text);

        switch( prlEvent->command )
        {
            case    HSB_STANDBY :
                dbg(TRACE,"CheckQueue: HSB_STANDBY");
                HandleQueues();
                ctrl_sta = prlEvent->command;
                break;
    
            case    HSB_COMING_UP   :
                dbg(TRACE,"CheckQueue: HSB_COMING_UP");
                ctrl_sta = prlEvent->command;
                break;
    
            case    HSB_ACTIVE  :
                dbg(TRACE,"CheckQueue: HSB_ACTIVE");
                ctrl_sta = prlEvent->command;
                break;
    
            case    HSB_ACT_TO_SBY  :
                dbg(TRACE,"CheckQueue: HSB_ACT_TO_SBY");
                HandleQueues();
                ctrl_sta = prlEvent->command;
                break;
    
            case    HSB_DOWN    :
                dbg(TRACE,"CheckQueue: HSB_DOWN");
                ctrl_sta = prlEvent->command;
                /* Acknowledge the item 
                ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
                if( ilRc != RC_SUCCESS )
                {
                    HandleQueErr(ilRc);
                }  */
                Terminate(30);
                break;
        
        case    HSB_STANDALONE  :
                dbg(TRACE,"CheckQueue: HSB_STANDALONE");
                ctrl_sta = prlEvent->command;
                break;
    
            case    SHUTDOWN    :
                dbg(TRACE,"CheckQueue: SHUTDOWN");
                /* Acknowledge the item */
                ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
                if( ilRc != RC_SUCCESS )
                {
                    /* handle que_ack error */
                    HandleQueErr(ilRc);
                } /* fi */
                Terminate(30);
                break;
    
            case    RESET       :
                ilRc = Reset();
                break;
    
            case    EVENT_DATA  :
                /* DebugPrintItem(DEBUG,prgItem); */
                /* DebugPrintEvent(DEBUG,prlEvent); */
                break;
    
            case    TRACE_ON :
                dbg_handle_debug(prlEvent->command);
                break;
    
            case    TRACE_OFF :
                dbg_handle_debug(prlEvent->command);
                break;


            default         :
                dbg(TRACE,"CheckQueue: unknown event");
                DebugPrintItem(TRACE,prlItem);
                DebugPrintEvent(TRACE,prlEvent);
                break;
    
        } /* end switch */
    
        /* Acknowledge the item */
        ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
        if( ilRc != RC_SUCCESS )
        {
            /* handle que_ack error */
            HandleQueErr(ilRc);
        } /* fi */
    }else{
        if(ilRc != QUE_E_NOMSG)
        {
            dbg(TRACE,"CheckQueue: que(QUE_GETBIG,%d,%d,...) failed <%d>",ipModId,ipModId,ilRc);
            HandleQueErr(ilRc);
        }/* end of if */
    }/* end of if */
       
    return ilRc;
}/* end of CheckQueue */

/******************************************************************************/
/******************************************************************************/
static int GetRowLength(char *pcpHopo, char *pcpTana, char *pcpFieldList,
                        long *plpLen)
{
    int  ilRc = RC_SUCCESS;         /* Return code */
    int  ilNoOfItems = 0;
    int  ilLoop = 0;
    long llFldLen = 0;
    long llRowLen = 0;
    char clFina[8];
    
    ilNoOfItems = get_no_of_items(pcpFieldList);

    ilLoop = 1;
    do
    {
        ilRc = get_real_item(&clFina[0],pcpFieldList,ilLoop);
        if(ilRc > 0)
        {
            ilRc = GetFieldLength(pcpHopo,pcpTana,&clFina[0],&llFldLen);
            if(ilRc == RC_SUCCESS)
            {
                llRowLen++;
                llRowLen += llFldLen;
            }/* end of if */
        }/* end of if */
        ilLoop++;
    }while((ilRc == RC_SUCCESS) && (ilLoop <= ilNoOfItems));

    if(ilRc == RC_SUCCESS)
    {
        *plpLen = llRowLen;
    }/* end of if */

    return(ilRc);
    
} /* end of GetRowLength */


/******************************************************************************/
/******************************************************************************/
static int GetFieldLength(char *pcpHopo, char *pcpTana, char *pcpFina,
                            long *plpLen)
{
    int  ilRc = RC_SUCCESS;         /* Return code */
    int  ilCnt = 0;
    char clTaFi[32];
    char clFele[16];
    char clFldLst[16];

    ilCnt = 1;
    sprintf(&clTaFi[0],"%s,%s",pcpTana,pcpFina);
    sprintf(&clFldLst[0],"TANA,FINA"); /* wird von syslib zerst�rt */

    /*dbg(TRACE,"GetFieldLength Tana <%s> Fina <%s>",pcpTana,pcpFina);*/

    ilRc = syslibSearchSystabData(&cgTabEnd[0],&clFldLst[0],&clTaFi[0],"FELE",&clFele[0],&ilCnt,"");
    switch (ilRc)
    {
    case RC_SUCCESS :
        *plpLen = atoi(&clFele[0]);
        /* dbg(DEBUG,"GetFieldLength: <%s,%s,%s> <%d>",pcpHopo,pcpTana,pcpFina,*plpLen);*/
        break;

    case RC_NOT_FOUND :
        dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_NOT_FOUND <%s>",ilRc,&clTaFi[0]);
        break;

    case RC_FAIL :
        dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_FAIL",ilRc);
        break;

    default :
        dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> unexpected",ilRc);
        break;
    }/* end of switch */

    return(ilRc);
    
} /* end of GetFieldLength */

static int getOrPutDBData(char *pcpSelection, char *pcpData, unsigned int ipMode)
{
  int ilGetRc = DB_SUCCESS;
  short slFkt = 0;
  short slCursor = 0;
  char errBuff[128];

  slFkt = ipMode;
  slCursor = 0;
  ilGetRc = sql_if (slFkt, &slCursor, pcpSelection, pcpData);
  close_my_cursor (&slCursor);

  if (ilGetRc == DB_ERROR ) 
  {
      get_ora_err(ilGetRc, &errBuff[0]);
      dbg (TRACE, "<getOrPutDBData> Error getting Oracle data error <%s>", &errBuff[0]);
  }
  return ilGetRc;
} /* getOrPutDBData */

/***********************************************************/
/* Implementation of Phase II iTrek                        */
/***********************************************************/

static void HandleMessaging(long lpMqrUrno, char *pcpData)
{
    IMGREC rlImgrec;
    IMRREC rlImrrec;
    long llMqrUrno = 0;
    int ilRc = RC_SUCCESS;
    int ilUrno = 0;
    int ilFound = 0;
    int ilUntagCnt = 0;
    short slSqlFunc = 0;
    char pclSqlBuf[DBQRLEN] = "\0";
    char pclData[DBQRLEN] = "\0";    
    char pclUrno[URNOLEN+1] = "\0";
    char *pclFunc = "HandleMessaging";

    dbg(TRACE,"<%s> ========== Start ==========",pclFunc);

    /* Get the URNO of MQRTAB */
    llMqrUrno = lpMqrUrno;
    memcpy( pclData, pcpData, strlen(pcpData) );
  
    memset( &rlImgrec, 0, sizeof( IMGREC ) );
    ilFound = GetElementValue( rlImgrec.SDID, pclData, "<SENT_BY_ID>", "</SENT_BY_ID>" );

    /* Someone sending a new message. Prepare to insert into table IMGTAB and IMRTAB */
    if( ilFound == TRUE ) /* New Msg */
    {
        dbg(DEBUG,"<%s> IMGREC-SDID <%s>", pclFunc, rlImgrec.SDID );
        ilFound = GetElementValue( rlImgrec.SDNM, pclData, "<SENT_BY_NAME>", "</SENT_BY_NAME>" );
        if( ilFound == FALSE )
        {
            dbg(TRACE,"<%s> Tag <SEND_BY_NAME> is missing, Msg:%s",pclFunc, pclData);
            return;
        }
        dbg(DEBUG,"<%s> IMGREC-SDNM <%s>", pclFunc, rlImgrec.SDNM );

        ilFound = GetElementValue( rlImgrec.MGBY, pclData, "<MSG_BODY>", "</MSG_BODY>" );
        if( ilFound == FALSE )
        {
            dbg(TRACE,"<%s> Tag <MSG_BODY> is missing Msg:%s",pclFunc, pclData);
            return;
        }
        dbg(DEBUG,"<%s> IMGREC-MGBY <%s>", pclFunc, rlImgrec.MGBY );
  
        GetStaffName( rlImgrec.SDID, pclUrno, rlImgrec.SDFN, rlImgrec.SDLN, SRCH_BY_PENO );
        dbg(DEBUG,"<%s> IMGREC-SDFN <%s>", pclFunc, rlImgrec.SDFN );
        dbg(DEBUG,"<%s> IMGREC-SDLN <%s>", pclFunc, rlImgrec.SDLN );

        ilUntagCnt = 0; 
        while( ilFound == TRUE )
        {
            ilUntagCnt++;
            memset( &rlImrrec, 0, sizeof( IMRREC ) );
            ilFound = xml_untag( rlImrrec.TOID, pclData, "MSG_TO_ID", ilUntagCnt );
            if( ilFound == FALSE )
            {
                if( ilUntagCnt > 1 ) /* No more element */ 
                   break;
                /* No recipent stated */
                dbg(TRACE,"<%s> Tag <MSG_TO_ID> is missing Msg:%s",pclFunc, pclData);
                return;
            }
            dbg(DEBUG,"<%s> IMRREC-TOID <%s> UntagCnt <%d>", pclFunc, rlImrrec.TOID, ilUntagCnt );

            if( !strncmp( rlImrrec.TOID, FLIGHT_TOID, 2 ) )
                GetFlight( &rlImrrec.TOID[2], rlImrrec.TOFN, rlImrrec.TOLN );
            else
                GetStaffName( rlImrrec.TOID, pclUrno, rlImrrec.TOFN, rlImrrec.TOLN, SRCH_BY_PENO );
            dbg(DEBUG,"<%s> IMRREC-TOFN <%s>", pclFunc, rlImrrec.TOFN );
            dbg(DEBUG,"<%s> IMRREC-TOLN <%s>", pclFunc, rlImrrec.TOLN );

            /* Ready to insert rec into DB */
            slSqlFunc = START;
            if( ilUntagCnt == 1 ) /* Only do this once */
            {
                /*(ilRc = GetNextValues( rlImgrec.URNO, 1 );*/ /* For msg */
                ilRc = NewUrnos( "IMGTAB", 1 ); /* For msg */
                if( ilRc == DB_ERROR )
                {
                    dbg(TRACE,"<%s> Failure to get IMGTAB URNO Msg:%s",pclFunc, pclData);
                    exit( 3 );
                }
                sprintf( rlImgrec.URNO, "%d", ilRc );
                GetServerTimeStamp( "LOC", 1, 0, rlImgrec.SDDT );
                dbg(TRACE,"<%s> Server Time Stamp:%s", pclFunc, rlImgrec.SDDT );

                /* Insert record into Table */
                slSqlFunc = START;
                sprintf(pclSqlBuf,"INSERT INTO IMGTAB (URNO, SDID, SDNM, SDFN, SDLN, MGBY, SDDT) "
                                  "VALUES ('%s','%s','%s','%s','%s','%s', '%s')",
                                  rlImgrec.URNO, rlImgrec.SDID, rlImgrec.SDNM, 
                                  rlImgrec.SDFN, rlImgrec.SDLN, rlImgrec.MGBY, rlImgrec.SDDT);
                ilRc = getOrPutDBData(pclSqlBuf,pclData,slSqlFunc);
                dbg( DEBUG,"<%s> ilRc = %d  SqlBuf <%s>",pclFunc, ilRc, pclSqlBuf );
                /* In case of indefinite looping */
                if( ilUntagCnt > 10 )
                {
                    dbg( TRACE,"<%s> Exceed untag count <%d>! BREAK!",pclFunc, ilUntagCnt );
                    break;
                }
            } /* Insert msg */

            /*ilRc = GetNextValues( rlImrrec.URNO, 1 );*/ /* For msg */
            ilRc = NewUrnos( "IMRTAB", 1 );
            if( ilRc == DB_ERROR )
            {
                dbg(TRACE,"<%s> Failure to get IMRTAB URNO Msg:%s",pclFunc, pclData);
                exit( 4 );
            }
            sprintf( rlImrrec.URNO, "%d", ilRc );
            dbg(DEBUG,"<%s> IMRREC-URNO <%s>", pclFunc, rlImrrec.URNO );
            memcpy( rlImrrec.MGNO, rlImgrec.URNO, URNOLEN+1 );
            dbg(DEBUG,"<%s> IMRREC-MGNO <%s>", pclFunc, rlImrrec.MGNO );

            /* Insert record into Table */
            slSqlFunc = START;
            sprintf(pclSqlBuf,"INSERT INTO IMRTAB (URNO, MGNO, TOID, TOFN, TOLN, RCID, RCFN, RCLN, RCDT) "
                              "VALUES('%s','%s','%s','%s','%s',' ',' ',' ',' ')",
                              rlImrrec.URNO, rlImrrec.MGNO, rlImrrec.TOID, rlImrrec.TOFN, rlImrrec.TOLN );
            ilRc = getOrPutDBData(pclSqlBuf,pclData,slSqlFunc);
            dbg( DEBUG,"<%s> add IMRTAB ilRc = %d Sql <%s>",pclFunc, ilRc, pclSqlBuf );

        } /* while loop */

        dbg( DEBUG,"<%s> #of TO_ID =%d", pclFunc, ilUntagCnt );

        /* Preparing the XML reply pkt */
        MessageSending( "MESSAGING", rlImgrec.URNO, NULL );

    } /* New Msg */
    else /* Upd existing msg */
    {
        ilFound = GetElementValue( rlImrrec.URNO, pclData, "<URNO>", "</URNO>" );
        if( ilFound == FALSE ) 
        {
        dbg(TRACE,"<%s> Tag <URNO> Msg:%s",pclFunc, pclData);
            return;
        }
        dbg(DEBUG,"<%s> IMRREC-URNO <%s>", pclFunc, rlImrrec.URNO );

        ilFound = GetElementValue( rlImrrec.RCID, pclData, "<RCV_BY>", "</RCV_BY>" );
        if( ilFound == FALSE ) 
        {
        dbg(TRACE,"<%s> Tag <RCV_BY> Msg:%s",pclFunc, pclData);
            return;
        }
        dbg(DEBUG,"<%s> IMRREC-RCID <%s>", pclFunc, rlImrrec.RCID );
 
        GetStaffName( rlImrrec.RCID, pclUrno, rlImrrec.RCFN, rlImrrec.RCLN, SRCH_BY_PENO );
        dbg(DEBUG,"<%s> IMRREC-RCFN <%s>", pclFunc, rlImrrec.RCFN );
        dbg(DEBUG,"<%s> IMRREC-RCLN <%s>", pclFunc, rlImrrec.RCLN );

        /* Get Server TimeStamp */
        GetServerTimeStamp( "LOC", 1, 0, rlImrrec.RCDT );

        /* Update record */
        slSqlFunc = START;
        sprintf(pclSqlBuf,"UPDATE IMRTAB SET RCID = '%s', RCFN = '%s', RCLN = '%s', "
                          "RCDT = '%s' WHERE URNO = '%s'",
                          rlImrrec.RCID, rlImrrec.RCFN, rlImrrec.RCLN,
                          rlImrrec.RCDT, rlImrrec.URNO );
        ilRc = getOrPutDBData(pclSqlBuf,pclData,slSqlFunc);
        dbg( DEBUG,"<%s> upd IMRTAB ilRc = %d Sql <%s>",pclFunc, ilRc, pclSqlBuf );


        /* Preparing the XML reply pkt */
        MessageSending( "MESSAGING", NULL, rlImrrec.URNO );
    } /* Messaging */
} /*HandleMessaging*/

static void HandleMessageQuery(long lpMqrUrno, char *pcpData)
{
    IMGREC rlImgrec;
    IMRREC rlImrrec;
    long llMqrUrno = 0;
    int ilUrno = 0;
    int ilFound = 0;
    int ilDuration = 0;
    int ilRc;
    short slSqlFunc = 0;
    char pclSqlBuf[DBQRLEN] = "\0";
    char pclData[DBQRLEN] = "\0";    
    char pclTmpStr[DBQRLEN] = "\0";    
    char *pclFunc = "HandleMessageQuery";

    dbg(TRACE,"<%s> ========== Start ==========",pclFunc);
    if( mod_id == rgITKHDL[0] )
    {
        if ( igITKHDL == 1 )
            igITKHDL = 2;
        else 
            igITKHDL = 1;
        ilRc = SendCedaEvent(rgITKHDL[igITKHDL],0, " ", " ", " ", " ","MSG", " ", " "," ", pcpData, "", 4, 0) ;
        dbg( TRACE, "<%s> Pass to itkhdl%d to process ilRc <%d>", pclFunc, igITKHDL, ilRc );
        return;
    }

    /* Get the URNO of MQRTAB */
    llMqrUrno = lpMqrUrno;
    /*memcpy( pclData, pcpData, strlen(pcpData) );*/
    strcpy( pclData, pcpData );

    memset( &rlImgrec, 0, sizeof( IMGREC ) );
    ilFound = GetElementValue( pclTmpStr, pclData, "<MESSAGE>", "</MESSAGE>" );
    /* Query on message itself */
    if( ilFound  == TRUE ) /* get data from IMGREC table */
    {
        ilFound = GetElementValue( pclTmpStr, pclData, "<DURATION>", "</DURATION>" );
        if( ilFound == FALSE )
        {
        dbg(TRACE,"<%s> Tag <DURATION> is missing Msg:%s",pclFunc, pclData);
            return;
        }
        ilDuration = atoi( pclTmpStr );
        dbg(TRACE,"<%s> ilDuration = %d",pclFunc, ilDuration);
        if( ilDuration <= 0 )
        {
        dbg(TRACE,"<%s> DURATION is invalid Msg:%s",pclFunc, pclData);
            return;
        }
        BulkMessageSending( "MESSAGE", ilDuration ); 
        return;
    }
    ilFound = GetElementValue( pclTmpStr, pclData, "<MESSAGETO>", "</MESSAGETO>" );
    if( ilFound == TRUE ) 
    {
        ilFound = GetElementValue( pclTmpStr, pclData, "<DURATION>", "</DURATION>" );
        if( ilFound == RC_FAIL ) 
        {
            dbg(TRACE,"<%s> Tag <DURATION> is missing Msg:%s",pclFunc, pclData);
            return;
        }
        ilDuration = atoi( pclTmpStr );
        if( ilDuration <= 0 )
        {
            dbg(TRACE,"<%s> DURATION is invalid Msg:%s",pclFunc, pclData);
            return;
        } 
        BulkMessageSending( "MESSAGETO", ilDuration );
        return;
    }        
    dbg(TRACE,"<%s> Invalid MQ Msg:%s",pclFunc, pclData);
    return;
} /* HandleMessageQuery */

static void HandleMessageLogRequest(long lpMqrUrno, char *pcpData)
{
    IMGREC rlImgrec;
    IMRREC rlImrrec;
    long llMqrUrno = 0;
    int ilFound = 0;
    int ilRowId = 0;
    int ilCount = 0;
    int ili = 0;
    int ilCountLimit = 0;
    int ilTotalCount = 0;
    int ilRc = RC_FAIL;
    BOOL blSortData = FALSE;
    BOOL blTagAND = FALSE;
    short slSqlFunc = 0;
    short slSqlCursor = 0;
    char pclSqlBuf[DBQRLEN] = "\0";
    char pclCountSqlBuf[DBQRLEN] = "\0";
    char pclData[DBQRLEN] = "\0";    
    char pclTmpStr[DBQRLEN] = "\0";  
    char pclSessionId[SESSLEN+1] = "\0";  
    char pclSentBy[SDNMLEN+1] = "\0";
    char pclFromDateTime[DATELEN+1] = "\0";
    char pclToDateTime[DATELEN+1] = "\0";
    char pclOrderBy[30] = "\0";
    char pclAscDes[6] = "\0";
    char pclSortOrder[100] = "\0";
    char *pclXmlStr = "\0";
    char *pclFunc = "HandleMessageLogRequest";
    char *pclImsgVWFields = "TRIM(UIMG),TRIM(SDID),TRIM(SDNM),"
                            "TRIM(SDFN),TRIM(SDLN),TRIM(MGBY),TRIM(SDDT),"
                            "TRIM(UIMR),TRIM(TOID),TRIM(TOFN),TRIM(TOLN),"
                            "TRIM(RCID),TRIM(RCFN),TRIM(RCLN),TRIM(RCDT) ";

    dbg(TRACE,"<%s> ========== Start ==========",pclFunc);
    if( mod_id == rgITKHDL[0] )
    {
        if ( igITKHDL == 1 )
            igITKHDL = 2;
        else 
            igITKHDL = 1;
        ilRc = SendCedaEvent(rgITKHDL[igITKHDL],0, " ", " ", " ", " ","MSG", " ", " "," ", pcpData, "", 4, 0) ;
        dbg( TRACE, "<%s> Pass to itkhdl%d to process ilRc <%d>", pclFunc, igITKHDL, ilRc );
        return;
    }

    /* Get the URNO of MQRTAB */
    llMqrUrno = lpMqrUrno;
    /*memcpy( pclData, pcpData, strlen(pcpData) );*/
    strcpy( pclData, pcpData );

    ilFound = GetElementValue( pclSessionId, pclData, "<SESSID>", "</SESSID>" );
    if( ilFound  == FALSE ) 
    {
        dbg(TRACE,"<%s> Tag <SESSIONID> is missing Msg:%s",pclFunc, pclData);
        return;
    }      
    dbg( TRACE, "<%s> SESSID <%s>", pclFunc, pclSessionId );

    ilFound = GetElementValue( pclSentBy, pclData, "<SENTBY>", "</SENTBY>" );
    if( ilFound  == FALSE ) 
    {
        dbg(TRACE,"<%s> Tag <SENTBY> is missing Msg:%s",pclFunc, pclData);
        return;
    }
    dbg( TRACE, "<%s> SENTBY <%s>", pclFunc, pclSentBy );

    ilFound = GetElementValue( pclFromDateTime, pclData, "<FR>","</FR>" );
    if( ilFound  == FALSE ) 
    {
        dbg(TRACE,"<%s> Tag <FR> is missing Msg:%s",pclFunc, pclData);
        return;
    }
    dbg( TRACE, "<%s> FR <%s>", pclFunc, pclFromDateTime );

    ilFound = GetElementValue( pclToDateTime, pclData,"<TO>","</TO>" );
    if( ilFound == FALSE )
    {
    dbg(TRACE,"<%s> Tag <TO> is missing Msg:%s",pclFunc, pclData);
        return;
    }
    dbg( TRACE, "<%s> TO <%s>", pclFunc, pclToDateTime );

    ilFound = GetElementValue( pclTmpStr, pclData,"<SID>","</SID>" );
    if( ilFound == FALSE )
    {
    dbg(TRACE,"<%s> Tag <SID> is missing Msg:%s",pclFunc, pclData);
        return;
    }
    ilRowId = atoi(pclTmpStr);
    dbg( TRACE, "<%s> SID <%s>-<%d>", pclFunc, pclTmpStr, ilRowId );

    ilFound = GetElementValue( pclTmpStr, pclData,"<CNT>","</CNT>" );
    if( ilFound == FALSE )
    {
    dbg(TRACE,"<%s> Tag <CNT> is missing Msg:%s",pclFunc, pclData);
        return;
    }
    ilCountLimit = atoi(pclTmpStr);
    dbg( TRACE, "<%s> CNT <%s>-<%d>", pclFunc, pclTmpStr, ilCountLimit );
    if( ilCountLimit <= 0 || ilCountLimit > 100 )  /* In case error in count */
        ilCountLimit = 25;

    ilFound = GetElementValue( pclOrderBy, pclData,"<ORDBY>","</ORDBY>" );
    if( ilFound == FALSE )
    {
    dbg(TRACE,"<%s> Tag <ORDBY> is missing Msg:%s",pclFunc, pclData);
        return;
    }
    dbg( TRACE, "<%s> ORDBY <%s>", pclFunc, pclOrderBy );


    /** Retrieve info from DB **/
    blSortData = TRUE;
    if( GetSortOrder( pclOrderBy, pclSortOrder ) == RC_FAIL )
        blSortData = FALSE;
    if( blSortData == TRUE )
    {
        dbg( TRACE, "<%s> SortOrder <%s>", pclFunc, pclSortOrder );
        ilFound = GetElementValue( pclTmpStr, pclData,"<AD>","</AD>" );
        if( ilFound == FALSE )
        {
        dbg(TRACE,"<%s> Tag <AD> is missing Msg:%s",pclFunc, pclData);
            return;
        }
        if( pclTmpStr[0] == 'A' )
            strcpy( pclAscDes, "ASC" );
        else if( pclTmpStr[0] == 'D' )
            strcpy( pclAscDes, "DESC" );
        else
            strcpy( pclAscDes, " " );
        dbg( TRACE, "<%s> AD <%s> Full String <%s>", pclFunc, pclTmpStr, pclAscDes );
    }

    blTagAND = FALSE;
    sprintf( pclSqlBuf, "SELECT %s FROM IMSGVW ", pclImsgVWFields );
    dbg( TRACE, "<%s> <%d> SqlBuf <%s>", pclFunc, __LINE__, pclSqlBuf );
    if( pclSentBy != NULL && strlen(pclSentBy) > 0 )
    {
        to_upper( pclSentBy );
        sprintf( pclTmpStr, "WHERE (UPPER(SDNM) LIKE '%%%s%%' OR UPPER(SDID) LIKE '%%%s%%') ", pclSentBy, pclSentBy );
        strcat( pclSqlBuf, pclTmpStr );
        blTagAND = TRUE;
    }
    dbg( TRACE, "<%s> <%d> SqlBuf <%s>", pclFunc, __LINE__, pclSqlBuf );
    if( pclFromDateTime != NULL )
    {
        sprintf( pclTmpStr, "%s", "WHERE " );
        if( blTagAND == TRUE )
            sprintf( pclTmpStr, "%s", "AND " );
        strcat( pclSqlBuf, pclTmpStr );
        sprintf( pclTmpStr, "SDDT >= '%s' ", pclFromDateTime );
        strcat( pclSqlBuf, pclTmpStr );
        blTagAND = TRUE;
    }
    dbg( TRACE, "<%s> <%d> SqlBuf <%s>", pclFunc, __LINE__, pclSqlBuf );
    if( pclToDateTime != NULL )
    {
        sprintf( pclTmpStr, "%s", "WHERE " );
        if( blTagAND == TRUE )
            sprintf( pclTmpStr, "%s", "AND " );
        strcat( pclSqlBuf, pclTmpStr );
        sprintf( pclTmpStr, "SDDT <= '%s' ", pclToDateTime );
        strcat( pclSqlBuf, pclTmpStr );
    }
    dbg( TRACE, "<%s> <%d> SqlBuf <%s>", pclFunc, __LINE__, pclSqlBuf );
    if( blSortData == TRUE )
    {
        sprintf( pclTmpStr, "ORDER BY %s %s", pclSortOrder, pclAscDes );
        strcat( pclSqlBuf, pclTmpStr );
    }
    dbg( TRACE, "<%s> <%d> SqlBuf <%s>", pclFunc, __LINE__, pclSqlBuf );
    
    sprintf( pclCountSqlBuf, "SELECT COUNT(*) %s", strstr( pclSqlBuf, "FROM" ) );
    dbg( TRACE, "<%s> CountSqlBuf <%s>", pclFunc, pclCountSqlBuf );
    ilTotalCount = 0;
    slSqlFunc = START;
    ilRc = getOrPutDBData( pclCountSqlBuf,pclData,slSqlFunc );  
    ilTotalCount = 0;
    if( ilRc == DB_SUCCESS )
        ilTotalCount = atoi(pclData);
    dbg( TRACE, "<%s> Total rec <%d>", pclFunc, ilTotalCount );

    /*if( ilRowId > 0 )
    {
        SELECT * FROM (SELECT a.*, ROWNUM RN FROM (SELECT * FROM imsgvw WHERE rcid NOT LIKE '%Peo%' ORDER BY rcln, rcid) a 
        WHERE ROWNUM <= 1) 

        sprintf( pclTmpStr, "SELECT * FROM (SELECT a.*, ROWNUM RN FROM (%s) a "
                            "WHERE ROWNUM <= %d) WHERE RN > %d", pclSqlBuf, ilRowId+ilCountLimit, ilRowId );
        sprintf( pclSqlBuf, "%s", pclTmpStr );
        dbg( TRACE, "<%s> <%d> SqlBuf <%s>", pclFunc, __LINE__, pclSqlBuf );
    }*/

    pclXmlStr = (char *) calloc (ilTotalCount+10 , 700);

    sprintf( pclXmlStr, "<MSGLOGS>"
                        "<SESSID>%s</SESSID>"
                        "<SID>%d</SID>"
                        "<TOTCNT>%d</TOTCNT>",  
                        pclSessionId, ilRowId, ilTotalCount );

    slSqlFunc = START;
    slSqlCursor = 0;
    ilCount = 0;
    while( (sql_if(slSqlFunc,&slSqlCursor,pclSqlBuf,pclData)) == DB_SUCCESS ) 
    {
        slSqlFunc = NEXT;
        ili++;
    
        if( ilRowId > 0 && ili <= ilRowId )
            continue;

        ilCount++;

        memset( &rlImgrec, 0, sizeof(IMGREC) );
        memset( &rlImrrec, 0, sizeof(IMRREC) );
        get_fld(pclData,FIELD_1,STR,URNOLEN,rlImgrec.URNO); 
        get_fld(pclData,FIELD_2,STR,USIDLEN,rlImgrec.SDID); strip_special_char(rlImgrec.SDID);
        get_fld(pclData,FIELD_3,STR,SDNMLEN,rlImgrec.SDNM); strip_special_char(rlImgrec.SDNM);
        get_fld(pclData,FIELD_4,STR,NAMELEN,rlImgrec.SDFN); strip_special_char(rlImgrec.SDFN);
        get_fld(pclData,FIELD_5,STR,NAMELEN,rlImgrec.SDLN); strip_special_char(rlImgrec.SDLN);
        get_fld(pclData,FIELD_6,STR,BODYLEN,rlImgrec.MGBY); strip_special_char(rlImgrec.MGBY);
        get_fld(pclData,FIELD_7,STR,DATELEN,rlImgrec.SDDT); 

        get_fld(pclData,FIELD_8,STR,URNOLEN,rlImrrec.URNO); 
        get_fld(pclData,FIELD_9,STR,USIDLEN,rlImrrec.TOID);  strip_special_char(rlImrrec.TOID);
        get_fld(pclData,FIELD_10,STR,NAMELEN,rlImrrec.TOFN); strip_special_char(rlImrrec.TOFN);
        get_fld(pclData,FIELD_11,STR,NAMELEN,rlImrrec.TOLN); strip_special_char(rlImrrec.TOLN);
        get_fld(pclData,FIELD_12,STR,USIDLEN,rlImrrec.RCID); strip_special_char(rlImrrec.RCID);
        get_fld(pclData,FIELD_13,STR,NAMELEN,rlImrrec.RCFN); strip_special_char(rlImrrec.RCFN);
        get_fld(pclData,FIELD_14,STR,NAMELEN,rlImrrec.RCLN); strip_special_char(rlImrrec.RCLN);
        get_fld(pclData,FIELD_15,STR,DATELEN,rlImrrec.RCDT); 

        /** Strip the ID type **/
        if( !strncmp( rlImrrec.TOID, FLIGHT_TOID, 2 ) )
        {
            strcpy( pclTmpStr, &rlImrrec.TOID[2] );
            strcpy( rlImrrec.TOID, pclTmpStr );
        }


        sprintf( pclTmpStr, "<LOG>\n"
                            "<URNO>%s</URNO>\n"
                            "<SBY>%s(%s)</SBY>\n"
                            "<SON>%s</SON>\n"
                            "<STO>%s(%s)</STO>\n", rlImrrec.URNO,
                            rlImgrec.SDNM, rlImgrec.SDID,
                            rlImgrec.SDDT, 
                            rlImrrec.TOFN, rlImrrec.TOID );
        strcat( pclXmlStr, pclTmpStr );

        if( strlen(rlImrrec.RCLN) <= 0 && strlen(rlImrrec.RCID) <= 0 )
            sprintf( pclTmpStr, "%s", "<RBY></RBY>\n" ); 
        else
            sprintf( pclTmpStr, "<RBY>%s(%s)</RBY>\n", rlImrrec.RCFN, rlImrrec.RCID );
        strcat( pclXmlStr, pclTmpStr );


        sprintf( pclTmpStr, "<RON>%s</RON>\n"
                            "<MSG>%.30s</MSG>\n"
                            "</LOG>\n", rlImrrec.RCDT, rlImgrec.MGBY );
        strcat( pclXmlStr, pclTmpStr );

        if( ilCount >= ilCountLimit )
        {
            dbg( TRACE, "<%s> Exceed <%d>  message log, ENOUGH!", pclFunc, ilCountLimit );
            break;
        }
    } /* while loop */
    close_my_cursor(&slSqlCursor);
    sprintf( pclTmpStr, "%s", "</MSGLOGS>" );
    strcat( pclXmlStr, pclTmpStr );

    /*if( ilCount > 0 )*/
    /*{*/
        dbg( TRACE, "<%s> Xml <%s>", pclFunc, pclXmlStr );
        ilRc = SendCedaEvent(igITKMSG,0, " ", " ", " ", " ","ITK", " ", " "," ", pclXmlStr, "", 4, 0) ;
        dbg (TRACE, "<%s> ilRc for SendCedaEvent = %d", pclFunc, ilRc );
        free( pclXmlStr );
    /*}*/
    return;

} /*HandleMessageLogRequest*/

static void HandleOneMessage(long lpMqrUrno, char *pcpData)
{
    IMGREC rlImgrec;
    IMRREC rlImrrec;
    int ilRc = RC_SUCCESS;
    int ilFound = 0;
    short slSqlFunc = 0;
    char pclSession[512];
    char pclSqlBuf[DBQRLEN] = "\0";
    char pclData[DBQRLEN] = "\0";    
    char pclMsgUrno[12] = "\0";
    char pclTmpStr[20];
    char *pclFunc = "HandleOneMessage";

    dbg(TRACE,"<%s> ========== Start ==========",pclFunc);

    memcpy( pclData, pcpData, strlen(pcpData) );
  
    memset( &rlImgrec, 0, sizeof( IMGREC ) );
    ilFound = GetElementValue( pclMsgUrno, pclData, "<URNO>", "</URNO>" );
    if( ilFound != TRUE )
    {
        dbg( TRACE, "<%s> Tag <URNO> is missing, Msg:%s", pclFunc, pclData );
        return;
    }
    ilFound = GetElementValue( pclSession, pclData, "<SSID>", "</SSID>" );
    if( ilFound != TRUE )
    {
        dbg( TRACE, "<%s> Tag <SSID> is missing, Msg:%s", pclFunc, pclData );
        return;
    }
    /* Get Message Record */
    slSqlFunc = START;
    sprintf(pclSqlBuf,"SELECT TRIM(SDID),TRIM(SDNM),TRIM(MGBY),TRIM(SDDT),TRIM(TOID),TRIM(TOFN)," 
                      "TRIM(RCID),TRIM(RCFN),TRIM(RCDT) FROM IMSGVW WHERE UIMR = '%s'", pclMsgUrno );
    dbg( TRACE,"<%s> SelIMG <%s>",pclFunc, pclSqlBuf );
    ilRc = getOrPutDBData(pclSqlBuf,pclData,slSqlFunc);

    if( ilRc != DB_SUCCESS )
    {
        dbg( TRACE, "<%s> Record not found", pclFunc );
        return;
    }
    
    memset( &rlImgrec, 0, sizeof(IMGREC) );
    memset( &rlImrrec, 0, sizeof(IMRREC) );
    get_fld(pclData,FIELD_1,STR,USIDLEN,rlImgrec.SDID); strip_special_char(rlImgrec.SDID);
    get_fld(pclData,FIELD_2,STR,SDNMLEN,rlImgrec.SDNM); strip_special_char(rlImgrec.SDNM);
    get_fld(pclData,FIELD_3,STR,BODYLEN,rlImgrec.MGBY); strip_special_char(rlImgrec.MGBY);
    get_fld(pclData,FIELD_4,STR,DATELEN,rlImgrec.SDDT); 

    get_fld(pclData,FIELD_5,STR,USIDLEN,rlImrrec.TOID); strip_special_char(rlImrrec.TOID);
    get_fld(pclData,FIELD_6,STR,NAMELEN,rlImrrec.TOFN); strip_special_char(rlImrrec.TOFN);
    get_fld(pclData,FIELD_7,STR,USIDLEN,rlImrrec.RCID); strip_special_char(rlImrrec.RCID);
    get_fld(pclData,FIELD_8,STR,NAMELEN,rlImrrec.RCFN); strip_special_char(rlImrrec.RCFN);
    get_fld(pclData,FIELD_9,STR,DATELEN,rlImrrec.RCDT); 

    /** Strip the ID type **/
    if( !strncmp( rlImrrec.TOID, FLIGHT_TOID, 2 ) )
    {
        strcpy( pclTmpStr, &rlImrrec.TOID[2] );
        strcpy( rlImrrec.TOID, pclTmpStr );
    }


    sprintf( pcgXmlStr, "<SMSGD>\n"
                        "<URNO>%s</URNO>\n"
                        "<SSID>%s</SSID>\n"
                        "<SBY>%s(%s)</SBY>\n"
                        "<SON>%s</SON>\n"
                        "<STO>%s(%s)</STO>\n"
                        "<RBY>%s(%s)</RBY>\n"
                        "<RON>%s</RON>\n"
                        "<MSG>%s</MSG>\n"
                        "</SMSGD>\n", pclMsgUrno, pclSession, rlImgrec.SDNM, rlImgrec.SDID,
                                      rlImgrec.SDDT, rlImrrec.TOFN, rlImrrec.TOID,
                                      rlImrrec.RCFN, rlImrrec.RCID, rlImrrec.RCDT, rlImgrec.MGBY );

    dbg( TRACE, "<%s> Xml <%s>", pclFunc, pcgXmlStr );
    ilRc = SendCedaEvent(igITKMSG,0, " ", " ", " ", " ","ITK", " ", " "," ", pcgXmlStr, "", 4, 0) ;
    dbg (TRACE, "<%s> ilRc for SendCedaEvent = %d", pclFunc, ilRc );

} /*HandleOneMessage*/


static void HandleLoadQuery(char *pcpFields, char *pcpData )
{
    LOAREC rlLoarec;
    BOOL blSetDssn = FALSE;
    BOOL blChopMsg = FALSE;
    long llMqrUrno = 0;
    int ilRc = RC_SUCCESS;
    int ilCount = 0;
    int ilFound = 0;
    int ilBRS = FALSE;
    short slSqlFunc = 0;
    short slSqlCursor = 0;
    char pclSqlBuf[DBQRLEN] = "\0";
    char pclData[DBQRLEN] = "\0";    
    char pclTmpStr[DBQRLEN] = "\0";
    char pclFlnu[URNOLEN+1] = "\0";
    char pclAlc2[4] = "\0";
    char pclDssn[4];
    char pclTTYP[8] = "\0";
    char *pclP;
    char *pclFunc = "HandleLoadQuery";

    dbg(TRACE,"<%s>: ========== Start HandleLoadQuery ==========",pclFunc);

    strcpy( pclData, pcpData );
    dbg(TRACE,"<%s>: ------------- Message received: -------------- <%s>",pclFunc,pclData);
    
    memset( &rlLoarec, 0, sizeof( LOAREC ) );

    /* This is the URNO of AFTTAB */
    ilFound = GetElementValue( pclFlnu, pclData, "<UAFT>", "</UAFT>" );
    if( ilFound  != TRUE ) 
    {
        dbg(TRACE,"<%s>: Tag <UAFT> is missing, Msg:%s",pclFunc, pclData);
        return;
    }
    dbg( DEBUG, "<%s> UAFT <%s>", pclFunc, pclFlnu );

    ilFound = GetElementValue( pclTmpStr, pclData, "<TYPE>", "</TYPE>" );
    if( ilFound  != TRUE ) 
    {
        dbg(TRACE,"<%s>: Tag <TYPE> is missing, Msg:%s",pclFunc, pclData);
        return;
    }
    ilBRS = FALSE;
    if( !strcmp( pclTmpStr, "BRS" ) )
        ilBRS = TRUE;

    sprintf( pclSqlBuf, "SELECT TRIM(ALC2),ADID,TTYP FROM AFTTAB WHERE URNO = %s", pclFlnu );
    dbg( TRACE, "<%s> SelAFT <%s>", pclFunc, pclSqlBuf );
    ilRc = getOrPutDBData(pclSqlBuf,pclData,START);
    if( ilRc != DB_SUCCESS )
    {
        dbg( TRACE, "<%s> Error in getting flight ALC2 and ADID", pclFunc );
        return;
    }
    get_fld(pclData,FIELD_1,STR,ALC2LEN,pclAlc2);
    get_fld(pclData,FIELD_2,STR,1,pclTmpStr); 
    get_fld(pclData,FIELD_3,STR,8,pclTTYP); TrimSpace( pclTTYP );
    dbg( DEBUG, "%s: Flight Nature. TTYP = <%s>", pclFunc, pclTTYP );

    if( ilBRS == FALSE )
    {
        sprintf( pclDssn, "%s", "CPM" );
        if( bgFwdDepCPM == FALSE && pclTmpStr[0] != 'A' )
        {
            dbg( DEBUG, "%s: Ignore departure CPM", pclFunc );
            return;
        }
        if( !strcmp(pclTTYP,"FRT") )
        {
            dbg( DEBUG, "%s: This is a FRIEGHTER flight, chop message", pclFunc );
            blChopMsg = TRUE;
        }
    }
    else 
        sprintf( pclDssn, "%s", "BRS" );

    /* Retrieve record */
    memset( pcgXmlStr, 0, sizeof( pcgXmlStr ) );
    slSqlFunc = START;
    slSqlCursor = 0;
    sprintf(pclSqlBuf,"SELECT TRIM(SSTP), TRIM(ULDT), TRIM(ULDP), TRIM(APC3), "
                      "TRIM(VALU), TRIM(URNO), FLNO, ADDI FROM LOATAB "
                      "WHERE FLNU = %s AND TYPE = 'ULD' AND DSSN = '%s'", pclFlnu, pclDssn );
    dbg (TRACE, "<%s> sqlBuf = %s", pclFunc, pclSqlBuf );

    ilCount = 0;

    while( (sql_if(slSqlFunc,&slSqlCursor,pclSqlBuf,pclData)) == DB_SUCCESS ) 
    {
        slSqlFunc = NEXT;

        memset( &rlLoarec, 0, sizeof( LOAREC ) );
        get_fld(pclData,FIELD_1,STR,SSTPLEN,rlLoarec.SSTP); strip_special_char(rlLoarec.SSTP);
        get_fld(pclData,FIELD_2,STR,ULDTLEN,rlLoarec.ULDT); strip_special_char(rlLoarec.ULDT);
        get_fld(pclData,FIELD_3,STR,ULDPLEN,rlLoarec.ULDP); strip_special_char(rlLoarec.ULDP);
        get_fld(pclData,FIELD_4,STR,APC3LEN,rlLoarec.APC3); strip_special_char(rlLoarec.APC3);
        get_fld(pclData,FIELD_5,STR,VALULEN,rlLoarec.VALU); strip_special_char(rlLoarec.VALU);
        get_fld(pclData,FIELD_6,STR,URNOLEN,rlLoarec.URNO); strip_special_char(rlLoarec.URNO);
        get_fld(pclData,FIELD_7,STR,FLNOLEN,rlLoarec.FLNO); strip_special_char(rlLoarec.FLNO);
        get_fld(pclData,FIELD_8,STR,ADDILEN,rlLoarec.ADDI); 

        /* v2.1 */
        if( blChopMsg == TRUE )
        {
            pclP = strchr( rlLoarec.ULDT, '.' );
            if( pclP != NULL )
                *pclP = '\0';
        }

        if( ilBRS == FALSE )
        {
            if( strlen(rlLoarec.ULDP) <= 0 )
            {
                dbg(DEBUG,"<%s> Position at FLIGHT is not stated! IGNORE! ULDP = (%s) ULDT=(%s)",
                       pclFunc, rlLoarec.ULDP, rlLoarec.ULDT);
                continue;;
            }

            if( strlen(rlLoarec.ULDT) <= 0 )
            {
                /* Only provide record if POSF is of type 'Bxxx' or 'TBxxx', since ULD is NIL */
                if( rlLoarec.SSTP[0] != 'B' && strncmp( rlLoarec.SSTP, "TB", 2 ) )
                {
                    dbg(DEBUG,"<%s> FLIGHT is not of required type! IGNORE! SSTP = (%s) ULDT=(%s)",
                              pclFunc, rlLoarec.SSTP, rlLoarec.ULDT);
                    continue;
                }
            }

        }
        else
        {
            /* Is BRS */
            if( strlen(rlLoarec.ULDT) <= 0 )
            {
                dbg(DEBUG, "<%s>Throw ULDT=(%s)", pclFunc, rlLoarec.ULDT );
                continue;
            } 
            /* Use the 2nd - 4th bytes of ADDI as NUMBER OF PIECES */
            sprintf( rlLoarec.NOP, "%3.3s", &rlLoarec.ADDI[2] );
            /* Use the 5th - 10th bytes of ADDI as VOLUME */
            sprintf( rlLoarec.VALU, "%5.5s", &rlLoarec.ADDI[5] );
        }

        dbg(DEBUG,"<%s> SSTP = (%s)",pclFunc, rlLoarec.SSTP);
        dbg(DEBUG,"<%s> ULDT = (%s)",pclFunc, rlLoarec.ULDT);
        dbg(DEBUG,"<%s> ULDP = (%s)",pclFunc, rlLoarec.ULDP);
        dbg(DEBUG,"<%s> APC3 = (%s)",pclFunc, rlLoarec.APC3);
        dbg(DEBUG,"<%s> VALU = (%s)",pclFunc, rlLoarec.VALU);
        dbg(DEBUG,"<%s> NOP  = (%s)",pclFunc, rlLoarec.NOP);
        dbg(DEBUG,"<%s> URNO = (%s)",pclFunc, rlLoarec.URNO);
        dbg(DEBUG,"<%s> FLNO = (%s)",pclFunc, rlLoarec.FLNO);



        if( ilCount == 0 ) /* For header info */
            sprintf( pcgXmlStr, "<%sS>\n", pclDssn );

        ilCount++; 
        if( ilCount >= MAX_DB_RECORDS )
            break;

        /* Preparing the XML reply pkt */
        sprintf( pclTmpStr, "%s\n", "<CPMDET>" );
        strcat( pcgXmlStr, pclTmpStr );

        if( ilCount == 1 ) /* Do this only for the 1st CPM */
        {
            sprintf( pclTmpStr, "<FLNU>%s</FLNU>\n"
                                "<ALC2>%2.2s</ALC2>\n", pclFlnu, pclAlc2 );
            strcat( pcgXmlStr, pclTmpStr );
        }

        sprintf( pclTmpStr, "<ULDN>%s</ULDN>\n" 
                            "<DSSN>%s</DSSN>\n"
                            "<APC3>%s</APC3>\n"
                            "<CONT>%s</CONT>\n"
                            "<POSF>%s</POSF>\n"
                            "<VALU>%s</VALU>\n"
                            "<NOP>%s</NOP>\n"
                            "<URNO>%s</URNO>\n"
                            "</CPMDET>\n", rlLoarec.ULDT, pclDssn, rlLoarec.APC3, rlLoarec.SSTP, 
                                           rlLoarec.ULDP, rlLoarec.VALU, rlLoarec.NOP,
                                           rlLoarec.URNO );

        strcat( pcgXmlStr, pclTmpStr );

    } /* end while cursor loop */
    sprintf( pclTmpStr, "</%sS>", pclDssn );
    strcat( pcgXmlStr, pclTmpStr );
    close_my_cursor(&slSqlCursor);

    /* Send to MQ handler process ITKxxx */
    dbg (TRACE, "<%s> No. of Rec found = %d", pclFunc, ilCount );
    if( ilCount > 0 )
    {
        ilRc = SendCedaEvent(igITKCPM,0, " ", " ", " ", " ","ITK", " ", " "," ", pcgXmlStr, "", 4, 0) ;
        dbg (TRACE, "<%s> ilRc for SendCedaEvent = %d", pclFunc, ilRc );
        dbg (TRACE, "<%s> pcgXmlStr (%s)", pclFunc, pcgXmlStr );
    }
    return;

} /* end HandleLoadQuery */

static void HandleOnlineCPM(char *pcpSelection)
{
    int ilUtlx, ilUaft;
    char pclSelection[512];
    char pclFields[8];
    char pclData[128];
    char *pclFunc = "HandleOnlineCPM";
    char *pclToken;

    dbg( TRACE, "<%s> =========== START =============", pclFunc );
    strcpy( pclSelection, pcpSelection );

    pclToken = (char *)strtok( pclSelection, "=" );
    pclToken = (char *)strtok( NULL, "," );
    ilUaft = atoi( pclToken );

    /*pclToken = (char *)strtok( NULL, "=" );
    pclToken = (char *)strtok( NULL, "\0" );
    ilUtlx = atoi( pclToken );*/

    dbg( TRACE, "<%s> UAFT <%d>", pclFunc, ilUaft );

    sprintf( pclData, "<LOA>\n<TYPE>CPM</TYPE>\n<UAFT>%d</UAFT>\n</LOA>\n", ilUaft );
    HandleLoadQuery( pclFields, pclData );

    dbg( TRACE, "<%s> =========== END =============", pclFunc );
} /* HandleOnlineCPM */


static void HandleStatusUpd(long lpMqrUrno, char *pcpData)
{
    int ilRc = RC_SUCCESS;
    int ilCount = 0;
    int ilFound = 0;
    int ilStatIndex = -1;
    int ilHssIndex;
    short slSqlFunc;
    short slSqlCursor = 0;
    char pclSqlBuf[DBQRLEN] = "\0";
    char pclFields[1024] = "\0";
    char pclNowTime[20] = "\0";
    char pclData[DBQRLEN] = "\0";
    char pclTmpStr[1024] = "\0";
    char pclTmpStr2[1024] = "\0";
    char pclHssStr[512] = "\0";
    char pclFlnu[20] = "\0";
    char pclTagName[36] = "\0";
    char pclTagValue[20] = "\0";
    char pclOrigTagValue[20] = "\0";
    char pclJOBUrno[12] = "\0";
    char pclOSTUrno[12] = "\0";
    char pclSTFUrno[12] = "\0";
    char pclDepUrno[12] = "\0";
    char pclDETY[4] = "\0";
    char pclUTPL[12] = "\0"; 
    char pclSTAT[8] = "\0";
    char pclPeno[32] = "\0";
    char pclOrgCode[32] = "\0";
    char *pclToken;
    char *pclFunc = "HandleStatusUpd";

    dbg(TRACE,"<%s>: ========== Start ==========",pclFunc);

    /*memcpy( pclData, pcpData, strlen(pcpData) );*/
    strcpy( pclData, pcpData );

    /* This is the URNO of AFTTAB */
    ilFound = GetElementValue( pclTmpStr, pclData, "<AFLIGHT>", "</AFLIGHT>" );
    if( ilFound != TRUE ) 
    {
        ilFound = GetElementValue( pclTmpStr, pclData, "<DFLIGHT>", "</DFLIGHT>" );
        if( ilFound  != TRUE ) 
        {
            dbg(TRACE,"<%s>: Tag <A/DFLIGHT> is missing, Msg:%s",pclFunc, pclData);
            return;
        }
    }
    ilFound = GetElementValue( pclFlnu, pclData, "<URNO>", "</URNO>" );
    if( ilFound  != TRUE ) 
    {
        dbg(TRACE,"<%s>: Tag <URNO> is missing, Msg:%s",pclFunc, pclData);
        return;
    }

    /* Not compulsory */
    ilFound = GetElementValue( pclPeno, pclData, "<PENO>", "</PENO>" );
    if( ilFound == TRUE )
        dbg(DEBUG,"<%s> UAFT <%s> PENO <%s>",pclFunc, pclFlnu, pclPeno);

    ilFound = GetElementValue( pclOrgCode, pclData, "<ORG>", "</ORG>" );
    if( ilFound  != TRUE ) 
    {
        dbg(TRACE,"<%s>: Tag <ORG> is missing, Msg:%s",pclFunc, pclData);
        return;
    }
    dbg(DEBUG,"<%s> ORG <%s>",pclFunc, pclOrgCode);
    if( !strncmp( pclOrgCode, "AO", 2 ) )
        ilHssIndex = HSS_AO;
    else if( !strncmp( pclOrgCode, "BO", 2 ) ) 
        ilHssIndex = HSS_BO;
    else if( !strncmp( pclOrgCode, "AIC", 3 ) ) 
        ilHssIndex = HSS_AIC;
    else
    {
        dbg(TRACE,"<%s>: Invalid ORG field Msg:%s",pclFunc, pclData);
        return;
    }

    memset( pclTagName, 0, sizeof( pclTagName ) );
    memset( pclTagValue, 0, sizeof( pclTagValue ) );
    ilRc = GetTagName( pclData, pclTagName, pclTagValue );

    if( ilRc == RC_FAIL )
        return;

    /* Treat it as delete */
    if( pclTagValue == NULL || strlen(pclTagValue) <= 0 )
        strcpy( pclTagValue, " " );
    else
    {
        strcpy( pclOrigTagValue, pclTagValue );
        LocalTimeToUtcFixTZ(pclTagValue);
    }
    if( pclTagName == NULL || strlen(pclTagName) <= 0 )
    {
        dbg( TRACE, "<%s> Tag Name is empty!", pclFunc );
        return;
    }
    dbg (TRACE, "<%s> TagName = (%s) TagValue = (%s)", pclFunc, pclTagName, pclTagValue );

    if( bgUpdStatus == FALSE )
    {
        dbg (TRACE, "<%s> Information NOT updated. Flag is TURN OFF", pclFunc );
        return;
    }
 
    /* Get Status name */
    ilRc = GetStatusNameIndex( pclTagName, &ilStatIndex, TRUE );
    if( ilRc == RC_FAIL )
    {
        dbg(TRACE,"<%s>: Tag NAME (%s) not found in config file",pclFunc, pclTagName);
        return;
    }
    if( ilStatIndex < 0 || ilStatIndex >= igNumStatus )
    {
        dbg(TRACE,"<%s>: Invalid index = %d found for Tag NAME <%s>",pclFunc, ilStatIndex, pclTagName);
        return;
    }

    /* Update FIPS by updating AFTTAB on OFBS and ONBS fields */
    if( ilStatIndex == igIdxATA || ilStatIndex == igIdxATD )
    {
        dbg( TRACE, "<%s> Updating AFTTAB on ATA/ATD fields", pclFunc );

        sprintf( pclSqlBuf, "WHERE URNO = %s AND ALC2 NOT IN (%s)", pclFlnu, pcgStatusAirlines );
        dbg( TRACE, "<%s> Selection <%s>", pclFunc, pclSqlBuf );

        strcpy( pclTmpStr, "OFBS" );
        if( ilStatIndex == igIdxATA )
            strcpy( pclTmpStr, "ONBS" );
        ilRc = SendCedaEvent(igFLIGHT, 0, mod_name, "", "", "","UFR", "AFTTAB", 
                             pclSqlBuf, pclTmpStr, pclTagValue, "", atoi(cgSendPrio), NETOUT_NO_ACK) ;
        if( ilRc != RC_SUCCESS )
            dbg(TRACE,"<%s>: Not successful in updating AFTTAB", pclFunc );
    }


    /** MEI: 26-NOV-2008 **/
    /** To update ITRTAB and JOBTAB(Job Confirm) irregardless of how the status manager outcome will be **/
   /* sprintf( pclFields, "%s", "NAME,TIME,UAFT" );
    sprintf( pclData, "%s,%s,%s", prgStatName[ilStatIndex].iTC_name, pclOrigTagValue, pclFlnu );
    dbg( TRACE, "<%s> Forward to ITRREP <%s>", pclFunc, pclData );
    ilRc = SendCedaEvent(igITKREP,0, " ", " ", " ", " ","TIM", " ", " ",pclFields, pclData, "", 3, 0) ;
   */

    memset( pclSqlBuf, 0, sizeof(pclSqlBuf) );
    /*** Obsolete: Trip info is now stored in iTrek DB  ***/
    /*if( !strncmp(prgStatName[ilStatIndex].iTC_name, "FTRIP_B", 7) )
        sprintf( pclSqlBuf, "UPDATE ITRTAB SET RCDT = '%s' WHERE UAFT = '%s' AND RTTY = 'F'", pclOrigTagValue, pclFlnu );
    else if( !strncmp(prgStatName[ilStatIndex].iTC_name, "LTRIP_B", 7) )
        sprintf( pclSqlBuf, "UPDATE ITRTAB SET RCDT = '%s' WHERE UAFT = '%s' AND RTTY = 'L'", pclOrigTagValue, pclFlnu );
    else if( !strncmp(prgStatName[ilStatIndex].iTC_name, "FTRIP", 5) )
        sprintf( pclSqlBuf, "UPDATE ITRTAB SET SDDT = '%s' WHERE UAFT = '%s' AND STTY = 'F'", pclOrigTagValue, pclFlnu );
    else if( !strncmp(prgStatName[ilStatIndex].iTC_name, "LTRIP", 5) )
        sprintf( pclSqlBuf, "UPDATE ITRTAB SET SDDT = '%s' WHERE UAFT = '%s' AND STTY = 'L'", pclOrigTagValue, pclFlnu );
    if( strlen(pclSqlBuf) > 0 )
    {
       ilRc = getOrPutDBData( pclSqlBuf,pclData,START );
       if( ilRc == DB_SUCCESS )
           dbg( TRACE, "<%s>: Updated ITRTAB <%s>", pclFunc, prgStatName[ilStatIndex].iTC_name );
    }*/


    /** MAB = JOB Confirm in JOBTAB **/
    if( bgJobConfirm == TRUE && !strncmp( prgStatName[ilStatIndex].iTC_name, "MAB", 3 ) )
    {
        /* Get the Staff URNO in STFTAB */
        sprintf(pclSqlBuf,"SELECT URNO FROM STFTAB WHERE PENO = '%s'", pclPeno );
        dbg (TRACE, "<%s> SelStf <%s>", pclFunc, pclSqlBuf );
        ilRc = getOrPutDBData( pclSqlBuf,pclData,START );   
        if( ilRc != DB_SUCCESS )
        {
            dbg(TRACE,"<%s>: Error in getting info from STFTAB", pclFunc );
            ilRc = RC_FAIL;
        }
        if( ilRc == RC_SUCCESS )
        {
            strcpy( pclSTFUrno, pclData );
            dbg( TRACE, "<%s> From STFTAB USTF <%s>", pclFunc, pclSTFUrno );

            sprintf(pclSqlBuf,"SELECT TRIM(URNO), TRIM(DETY), TRIM(UTPL), TRIM(STAT) FROM JOBTAB WHERE "
                              "UAFT = '%s' AND USTF = '%s'", pclFlnu, pclSTFUrno );
            dbg (TRACE, "<%s> SelJob <%s>", pclFunc, pclSqlBuf );
            ilRc = getOrPutDBData( pclSqlBuf,pclData,START );   
            if( ilRc != DB_SUCCESS )
            {
                dbg(TRACE,"<%s>: Arr record for turnarnd flight. Upd Dep job", pclFunc );
                sprintf( pclSqlBuf, "SELECT URNO FROM AFTTAB WHERE RKEY = %s AND ADID = 'D'", pclFlnu );
                dbg (TRACE, "<%s> SelDAFT <%s>", pclFunc, pclSqlBuf );
                ilRc = getOrPutDBData( pclSqlBuf,pclDepUrno,START );   
                if( ilRc != DB_SUCCESS )
                    dbg( TRACE, "<%s> Fail to find DEP flight in AFT for turnaround arr flight <%s> recv", pclFunc, pclFlnu );
                else
                {
                    sprintf(pclSqlBuf,"SELECT TRIM(URNO), TRIM(DETY), TRIM(UTPL), TRIM(STAT) FROM JOBTAB WHERE "
                                      "UAFT = '%s' AND USTF = '%s'", pclDepUrno, pclSTFUrno );
                    dbg (TRACE, "<%s> SelDJob <%s>", pclFunc, pclSqlBuf );
                    ilRc = getOrPutDBData( pclSqlBuf,pclData,START );   
                    if( ilRc != DB_SUCCESS )
                        dbg(TRACE,"<%s>: Error in getting info from JOBTAB", pclFunc );
                }
            }
            if( ilRc == RC_SUCCESS )
            {
                get_fld(pclData,FIELD_1,STR,URNOLEN,pclJOBUrno);
                get_fld(pclData,FIELD_2,STR,1,pclDETY);
                get_fld(pclData,FIELD_3,STR,10,pclUTPL);
                get_fld(pclData,FIELD_4,STR,12,pclSTAT);
                dbg(TRACE,"<%s>: UJOB <%s> DETY <%s> UTPL <%s> STAT <%s>", pclFunc, pclJOBUrno, 
                                                                  pclDETY, pclUTPL, pclSTAT );

                /* Make sure it is Apron Templates */
                if( strstr( cgJobTemplates, pclUTPL ) != NULL && strcmp( pclSTAT, pcgJobConfirmStatus ) )
                {
                    GetServerTimeStamp("UTC",1,0,pclNowTime);
                    sprintf( pclFields, "STAT,USEU,LSTU" );
                    sprintf( pclData, "%-10.10s,iTrek %s,%14.14s", pcgJobConfirmStatus, pclPeno, pclNowTime );
                    sprintf( pclTmpStr, "WHERE URNO = '%s'", pclJOBUrno );
                    dbg( TRACE, "<%s> Forward to SQLHDL <%s>", pclFunc, pclData );
                    ilRc = SendCedaEvent(igSQLHDL,0, " ", " ", " ", " ","URT", "JOBTAB", pclTmpStr,
                                         pclFields, pclData, "" , 3, 0) ;
                    dbg(DEBUG,"<%s>: ilRc <%d> ", pclFunc, ilRc );
                }
            }
        }
    } /* MAB */

    /* Get URNO of OSTTAB from view STATVW */
    /* It seems like the cursor dun work for 4-tables view, so has to break up the HSS name for searching */
    strcpy( pclHssStr, pcgHssName[ilHssIndex] );
    pclToken = (char *)strtok( pclHssStr, "," );
    while( pclToken != NULL )
    {
        strcpy( pclTmpStr, pclToken );
        sprintf(pclSqlBuf,"SELECT TRIM(UOST) FROM STATVW WHERE UAFT = '%s' AND DENM = '%s' AND HSNM = %s", 
                                                   pclFlnu, prgStatName[ilStatIndex].iTS_name, pclTmpStr );
        dbg (TRACE, "<%s> SelSTATVW = %s", pclFunc, pclSqlBuf );
        
        slSqlFunc = START;
        slSqlCursor = 0;
        while( (ilRc = sql_if( slSqlFunc,&slSqlCursor,pclSqlBuf,pclOSTUrno )) == DB_SUCCESS )
        {
            slSqlFunc = NEXT;
            dbg (TRACE, "<%s> Flnu = <%s> OSTUrno = <%s>", pclFunc, pclFlnu, pclOSTUrno );
            sprintf( pclTmpStr2,"WHERE URNO = '%s'", pclOSTUrno );

            sprintf( pclTmpStr, "I%s", pclPeno );
            ilRc = SendCedaEvent(igSQLHDL,0,pclTmpStr,"",pclPeno,"","URT","OSTTAB",pclTmpStr2,
                                 "CONA",pclTagValue,"",atoi(cgSendPrio),RC_SUCCESS);
            memset( pclOSTUrno, 0, sizeof( pclOSTUrno ) );
        }
        pclToken = (char *)strtok( NULL, "," );
        close_my_cursor(&slSqlCursor);
    }



    dbg(TRACE,"<%s> ========== End ==========",pclFunc);
    return;
} /* end HandleStatusUpd */

static void HandleTripInfo(long lpMqrUrno, char *pcpData) 
{
    ITRREC rlItrrec;
    BOOL blReceive = FALSE;
    BOOL blInsert = FALSE;
    BOOL blUpdateLTrip = FALSE;
    long llMqrUrno = 0;
    int ilRc = RC_SUCCESS;
    int ilNumrecs = 0;
    int ilFound = 0;
    int ilUntagCnt = 0;
    int ilTripCnt = 0;
    short slSqlFunc = 0;
    short slSqlCursor = 0;
    char pclSqlBuf[DBQRLEN] = "\0";
    char pclData[MQRDLEN] = "\0";    
    char pclTmpStr[MQRDLEN] = "\0";
    char pclTripNo[10] = "\0";
    char pclTripType[4];
    char *pclFunc = "HandleTripInfo";

    dbg(TRACE,"<%s> ========== Start ==========",pclFunc);

    /* Get the URNO of MQRTAB */
    llMqrUrno = lpMqrUrno;
    /*memcpy(pclData, pcpData, strlen(pcpData) );*/
    strcpy( pclData, pcpData );

    /* This is the URNO of AFTTAB */
    ilFound = GetElementValue( pclTmpStr, pclData, "<TRIP>", "</TRIP>" );
    if( ilFound  != TRUE ) 
    {
        dbg(TRACE,"<%s>: Invalid XML, Msg:%s",pclFunc, pclData);
        return;
    }

    /* From this point onwards, store structure information */
    memset( &rlItrrec, 0, sizeof( ITRREC ) );
    ilFound = GetElementValue( rlItrrec.UAFT, pclData, "<UAFT>", "</UAFT>" );
    if( ilFound  != TRUE ) 
    {
        dbg(TRACE,"<%s>: Tag <UAFT> is missing, Msg:%s",pclFunc, pclData);
        return;
    }
    dbg(DEBUG,"<%s> UAFT <%s>",pclFunc, rlItrrec.UAFT);

    /* When this special XML tag is recv from iTC, switch previous record of N trip to L */
    ilFound = GetElementValue( pclTmpStr, pclData, "<NAME>", "</NAME>" );
    if( ilFound  == TRUE ) 
    {
        dbg(DEBUG,"<%s> NAME <%s>",pclFunc, pclTmpStr);
        if( strncmp(pclTmpStr, "LTRIP", 5 ) )
        {
            dbg(TRACE,"<%s> Invalid Trip NAME <%s>",pclFunc, pclTmpStr);
            return;
        }
        UpdateNLTrip( "SENDER", rlItrrec.UAFT, "N-L" );
        return;
    }

    /* Found out SENDER or RECEIVER first */
    GetElementValue( pclTmpStr, pclData, "<SR>", "</SR>" );
    if( pclTmpStr[0] != 'S' && pclTmpStr[0] != 'R' )
    {
        dbg(TRACE,"<%s>: Tag <SR> is invalid, Msg:%s",pclFunc, pclData);
        return;
    }
    dbg(DEBUG,"<%s> SR <%c>",pclFunc, pclTmpStr[0]);

    blReceive = TRUE;
    if( pclTmpStr[0] == 'S' )  /* Send */
        blReceive = FALSE;

    ilFound = GetElementValue( rlItrrec.TPTY, pclData, "<TRPTYPE>", "</TRPTYPE>" );
    if( rlItrrec.TPTY[0] != 'F' && rlItrrec.TPTY[0] != 'N' && rlItrrec.TPTY[0] != 'L' )
    {
        dbg(TRACE,"<%s>: Tag <TRPTYPE> is invalid, Msg:%s",pclFunc, pclData);
        return;
    }
    dbg(DEBUG,"<%s> TPTY <%s>",pclFunc, rlItrrec.TPTY);


    /* Tabulate trip number */
    slSqlFunc = START;
    slSqlCursor = 0;
    if( blReceive == FALSE )
        sprintf(pclSqlBuf,"SELECT MAX(STNO) FROM ITRTAB WHERE UAFT = '%s'", rlItrrec.UAFT );
    else
        sprintf(pclSqlBuf,"SELECT MAX(RTNO) FROM ITRTAB WHERE UAFT = '%s'", rlItrrec.UAFT );
    dbg (TRACE, "<%s> sqlBuf = %s", pclFunc, pclSqlBuf );

    memset( pclTmpStr, 0, sizeof( pclTmpStr ) );
    ilRc = sql_if(slSqlFunc,&slSqlCursor,pclSqlBuf,pclTmpStr);
    close_my_cursor(&slSqlCursor);
    dbg(TRACE,"<%s>: ilRc <%d> Trip No <%s>", pclFunc, ilRc, pclTmpStr );
    if( ilRc != DB_SUCCESS && ilRc != ORA_NOT_FOUND ) 
    {
        dbg(TRACE,"<%s>: Error in selecting info for UAFT <%s>", pclFunc, rlItrrec.UAFT );
        return;
    }
    if( ilRc != DB_SUCCESS && ilRc == ORA_NOT_FOUND ) 
    {
        if( blReceive == FALSE ) rlItrrec.STNO = 1;
        else rlItrrec.RTNO = 1;
    }
    else
    {
        if( blReceive == FALSE ) rlItrrec.STNO = atoi( pclTmpStr ) + 1;
        else rlItrrec.RTNO = atoi( pclTmpStr ) + 1;
    }
 
    if( blReceive == TRUE )
    {
        dbg(TRACE,"<%s>: Rcv Trip No. <%d> UAFT <%s>", pclFunc, rlItrrec.RTNO, rlItrrec.UAFT );

        GetElementValue( rlItrrec.RCDT, pclData, "<SRDT>", "</SRDT>" );
        GetElementValue( rlItrrec.RCBY, pclData, "<SRBY>", "</SRBY>" );
        strcpy( rlItrrec.RTTY, rlItrrec.TPTY );
        strcpy( rlItrrec.STTY, " " );     
        dbg(DEBUG,"<%s> SRDT <%s> SRBY <%s> RTTY <%c>",pclFunc, rlItrrec.RCDT, rlItrrec.RCBY, rlItrrec.RTTY[0] );
    }
    else
    {
        dbg(TRACE,"<%s>: Snd Trip No. <%d> UAFT <%s>", pclFunc, rlItrrec.STNO, rlItrrec.UAFT );

        GetElementValue( rlItrrec.SDDT, pclData, "<SRDT>", "</SRDT>" );
        GetElementValue( rlItrrec.SDBY, pclData, "<SRBY>", "</SRBY>" );
        strcpy( rlItrrec.STTY, rlItrrec.TPTY );
        strcpy( rlItrrec.RTTY, " " );
        dbg(DEBUG,"<%s> SDDT <%s> SDBY <%s> STTY <%c>",pclFunc, rlItrrec.SDDT, rlItrrec.SDBY, rlItrrec.STTY[0] );
    }

    /* Is the ULD and UAFT existing records? */
    ilUntagCnt = 0; 
    blUpdateLTrip = FALSE;
    ilTripCnt = 0;
    while( ilFound == TRUE )
    {
        ilUntagCnt++;
        memset( pclTmpStr, 0, sizeof( pclTmpStr ) );
        memset( rlItrrec.URNO, 0, URNOLEN+1 );
        memset( rlItrrec.ULDI, 0, ULDILEN+1 );
        memset( rlItrrec.RCRM, 0, BODYLEN+1 );
        memset( rlItrrec.SDRM, 0, BODYLEN+1 );
        memset( rlItrrec.DEST, 0, DESTLEN+1 );
        memset( rlItrrec.ICPM, 0, ICPMLEN+1 );

        /* RECEIVER */
        /* NOTE: No RMK for RECEIVER */
        /*GetElementValue( rlItrrec.RCRM, pclTmpStr, "<RMK>", "</RMK>" );
        dbg(TRACE,"<%s>: Rcv RMK <%s>",pclFunc, rlItrrec.RCRM ); */
        if( blReceive == TRUE )
        {
            ilFound = xml_untag( pclTmpStr, pclData, "URNO", ilUntagCnt );
            if( ilFound == FALSE )
            {
                if( ilUntagCnt > 1 ) /* No more element */ 
                    break;
                /* No ULD stated */
                dbg(TRACE,"<%s>: Tag <URNO> is missing Msg:%s",pclFunc, pclData);
                return;
            }
            dbg(TRACE,"<%s>: URNO string <%s>",pclFunc, pclTmpStr);

            memset( rlItrrec.URNO, 0, sizeof(rlItrrec.URNO) );
            strcpy( rlItrrec.URNO, pclTmpStr );
            dbg(TRACE,"<%s>: Receiver URNO <%s>",pclFunc, rlItrrec.URNO );

            sprintf(pclSqlBuf,"SELECT TRIM(RTTY) FROM ITRTAB WHERE URNO = '%s'", rlItrrec.URNO );
            dbg (TRACE, "<%s> RTTYSqlBuf <%s>", pclFunc, pclSqlBuf );
            ilRc = getOrPutDBData(pclSqlBuf,pclTmpStr,START);
            if( ilRc != DB_SUCCESS )
            {
                dbg( TRACE, "<%s> Throw away URNO <%s> as no record found in ITRTAB for upd", pclFunc, rlItrrec.URNO );
                continue;
            }
            dbg( TRACE, "<%s> RTTY <%s>", pclFunc, pclTmpStr );

            if( rlItrrec.RTTY[0] == pclTmpStr[0] )
            {
                dbg( TRACE, "<%s> Throw away URNO <%s> as trip type is the same", pclFunc, rlItrrec.URNO );
                continue;
            }

            if( rlItrrec.RTTY[0] == 'L' )
            {
                if( blUpdateLTrip == FALSE )
                {
                    UpdateNLTrip( "RECEIVER", rlItrrec.UAFT, "L-N" );
                    blUpdateLTrip = TRUE;
                }
            }
            else
            {
                rlItrrec.TPTY[0] = 'N';
                if( rlItrrec.RTNO <= 1 )
                    rlItrrec.TPTY[0] = 'F';
            }

            sprintf(pclSqlBuf,"UPDATE ITRTAB SET RTNO = '%d', RCDT = '%s', RCBY = '%s', RCRM = '%s', RTTY = '%c' "
                              "WHERE URNO = '%s'", 
                              rlItrrec.RTNO, rlItrrec.RCDT, rlItrrec.RCBY, rlItrrec.RCRM, 
                              rlItrrec.TPTY[0], rlItrrec.URNO );
            dbg(TRACE,"<%s>: UpdITRSqlBuf <%s>",pclFunc, pclSqlBuf );
 
            ilRc = getOrPutDBData(pclSqlBuf,pclData,START);
            if( ilRc == RC_FAIL )
            {
                dbg(TRACE,"<%s>: Unable to update RECEIVER record in DB",pclFunc );
                continue;
            }
            /* There are changes to the info received */
            if( rlItrrec.TPTY[0] != rlItrrec.RTTY[0] )
            {
                if( ilTripCnt <= 0 )
                    sprintf( pcgXmlStr, "<TRIP>\n"
                                       "<RTTY>%c</RTTY>\n", rlItrrec.TPTY[0] );
                sprintf( pclTmpStr, "<URNO>%s</URNO>\n", rlItrrec.URNO );
                strcat( pcgXmlStr, pclTmpStr );
                ilTripCnt++;
            }
        } /* RECEIVER */

        else /* SENDER */
        {
            ilFound = xml_untag( pclTmpStr, pclData, "ULD", ilUntagCnt );
            if( ilFound == FALSE )
            {
                if( ilUntagCnt > 1 ) /* No more element */ 
                    break;
                /* No ULD stated */
                dbg(TRACE,"<%s>: Tag <ULD> is missing Msg:%s",pclFunc, pclData);
                return;
            }
            dbg(TRACE,"<%s>: ULD string <%s>",pclFunc, pclTmpStr);

            /* Retrieve ULD information */
            GetElementValue( rlItrrec.ULDI, pclTmpStr, "<ULDN>", "</ULDN>" );
            dbg(TRACE,"<%s>: ULDI <%s>",pclFunc, rlItrrec.ULDI );
            GetElementValue( rlItrrec.SDRM, pclTmpStr, "<RMK>", "</RMK>" );
            dbg(TRACE,"<%s>: Snd RMK <%s>",pclFunc, rlItrrec.SDRM );
            GetElementValue( rlItrrec.DEST, pclTmpStr, "<DES>", "</DES>" );
            dbg(TRACE,"<%s>: DEST <%s>",pclFunc, rlItrrec.DEST );
            GetElementValue( rlItrrec.ICPM, pclTmpStr, "<ISCPM>", "</ISCPM>" );
            dbg(TRACE,"<%s>: ISCPM <%s>",pclFunc, rlItrrec.ICPM );

            /* Start Processing */
            sprintf(pclSqlBuf,"SELECT COUNT(*) FROM ITRTAB WHERE UAFT = '%s' AND ULDI = '%s'", 
                                                                    rlItrrec.UAFT, rlItrrec.ULDI );
            dbg (TRACE, "<%s> CntITRSqlBuf <%s>", pclFunc, pclSqlBuf );
            ilRc = getOrPutDBData(pclSqlBuf,pclTmpStr,START);
            ilNumrecs = 0;
            if( ilRc == DB_SUCCESS )
                ilNumrecs = atoi(pclTmpStr);
            dbg( TRACE, "<%s> #ULD <%d> records found for SENDER", pclFunc, ilNumrecs );
            if( ilNumrecs > 0 )
            {
                if( rlItrrec.ICPM[0] == 'Y' )
                {
                    dbg( TRACE, "<%s> Throw away XML as ISCPM = Y", pclFunc );
                    continue;
                }
            }
            
            /* Check trip type */
            if( rlItrrec.STTY[0] == 'L' )
            {
                if( blUpdateLTrip == FALSE )
                {
                    UpdateNLTrip( "SENDER", rlItrrec.UAFT, "L-N" );
                    blUpdateLTrip = TRUE;
                }
            }
            else
            {
                rlItrrec.STTY[0] = 'N';
                if( rlItrrec.STNO <= 1 )
                    rlItrrec.STTY[0] = 'F';
            }

            ilRc = NewUrnos( "ITRTAB", 1 ); /* For msg */
            if( ilRc == DB_ERROR )
            {
                dbg(TRACE,"<%s>: Failure to get ITRTAB URNO",pclFunc );
                exit( 5 );
            }
            sprintf( rlItrrec.URNO, "%d", ilRc );
            dbg (DEBUG, "<%s> URNO <%s>", pclFunc, rlItrrec.URNO );
    
            sprintf(pclSqlBuf,"INSERT INTO ITRTAB (%s) "
                        "VALUES( %d,%d,'%s','%s','%s','%c','%c','%s','%s','%s','%s','%s','%s','%s','%s' )",
                        pcgSqlInsField_ITRTAB, rlItrrec.STNO, rlItrrec.RTNO, rlItrrec.URNO, rlItrrec.UAFT, 
                        rlItrrec.ULDI, rlItrrec.STTY[0], rlItrrec.RTTY[0], rlItrrec.DEST, rlItrrec.ICPM, 
                        rlItrrec.SDDT, rlItrrec.SDBY, rlItrrec.SDRM, rlItrrec.RCDT, 
                        rlItrrec.RCBY, rlItrrec.RCRM );
            dbg (TRACE, "<%s> InsITR SqlBuf <%s>", pclFunc, pclSqlBuf );

            ilRc = getOrPutDBData(pclSqlBuf,pclData,slSqlFunc);
            if( ilRc == RC_FAIL )
            {
                dbg(TRACE,"<%s>: Unable to insert record in DB",pclFunc );
                return;
            }

            /* Format the generic XML reply */
            sprintf( pclTmpStr, "<URNO>%s</URNO>\n"
                                "<ULDN>%s</ULDN>\n"
                                "<DES>%s</DES>\n"
                                "<SEND_RMK>%s</SEND_RMK>\n"
                                "<ISCPM>%c</ISCPM>\n"
                                "</TRIP>\n",
                                rlItrrec.URNO, rlItrrec.ULDI, rlItrrec.DEST,
                                rlItrrec.SDRM, rlItrrec.ICPM[0] );
                                
            if( ilTripCnt == 0 ) /* Very first ULD */
            {
                memset( pcgXmlStr, 0, sizeof( pcgXmlStr ) );
                sprintf( pcgXmlStr, "<TRIPS>\n"
                                    "<TRIP>\n"
                                    "<UAFT>%s</UAFT>\n"
                                    "<STRPNO>%d</STRPNO>\n"
                                    "<SEND_DT>%s</SEND_DT>\n"
                                    "<SEND_BY>%s</SEND_BY>\n"
                                    "<STTY>%c</STTY>\n",
                                    rlItrrec.UAFT, rlItrrec.STNO, rlItrrec.SDDT, 
                                    rlItrrec.SDBY, rlItrrec.STTY[0] );
            }
            else
                strcat( pcgXmlStr, "<TRIP>\n" );
            strcat( pcgXmlStr, pclTmpStr );
            ilTripCnt++;
        } /* SENDER */
    } /* while loop */

    /* For Sending only */
    if( ilTripCnt > 0 )
    {
        if( blReceive == FALSE )
            strcat( pcgXmlStr, "</TRIPS>\n" ); 
        else
            strcat( pcgXmlStr, "</TRIP>\n" ); 
        dbg (TRACE, "<%s> Xml Str = (%s)", pclFunc, pcgXmlStr ); 

        /* Send to MQ handler process ITKxxx */
        ilRc = SendCedaEvent(igITKCPM,0, " ", " ", " ", " ","ITK", " ", " "," ", pcgXmlStr, "", 4, 0) ;
        dbg (DEBUG, "<%s> ilRc for Trip XML = %d", pclFunc, ilRc );
    }

    if( ilTripCnt <= 0 )
        dbg( TRACE, "<%s> No trip info to be sent", pclFunc );

    dbg(TRACE,"<%s> ========== End ==========",pclFunc);
    return;
} /* end HandleTripInfo */

int MessageSending( char *pcpXmlType, char *pcpMsgUrno, char *pcpRcverUrno )
{
    IMGREC rlImgrec;
    IMRREC rlImrrec;
    ITRREC rlItrrec;
    int ilRc;
    int ilCount = 0;
    short slSqlFunc = 0;
    short slSqlCursor = 0;
    char pclSqlBuf[DBQRLEN] = "\0";
    char pclData[DBQRLEN] = "\0";
    char pclTmpStr[MQRDLEN] = "\0";
    char pclAction[30] = "\0";
    char pclToType[20];
    char *pclFunc = "MessageSending";

    memset( &pclData, 0, sizeof( pclData ) );
    if( !strcmp( pcpXmlType, "MESSAGING" ) )
    {
        memset( &rlImgrec, 0, sizeof( IMGREC ) );
        memset( &rlImrrec, 0, sizeof( IMRREC ) );

        if(pcpMsgUrno == NULL && pcpRcverUrno == NULL) 
        {
            dbg (TRACE, "<%s> Invalid Msg Urno", pclFunc );
            return RC_FAIL;
        }
        if( pcpMsgUrno != NULL )  /* Send full msg pkt as insertion has occured */
        {
            sprintf( pclSqlBuf, "SELECT TRIM(URNO),TRIM(SDID),TRIM(SDNM),TRIM(MGBY),TRIM(SDDT) "
                                "FROM IMGTAB WHERE URNO = '%s'", pcpMsgUrno );  
            dbg (DEBUG, "<%s> SqlBuf <%s>", pclFunc, pclSqlBuf );
            slSqlFunc = START;
            slSqlCursor = 0;
            if(sql_if(slSqlFunc,&slSqlCursor,pclSqlBuf,pclData) != DB_SUCCESS) 
            {
                close_my_cursor(&slSqlCursor);
                dbg (TRACE, "<%s> %d: SQL Error ", pclFunc, __LINE__ );
                return RC_FAIL;
            }
            close_my_cursor(&slSqlCursor);
            get_fld(pclData,FIELD_1,STR,URNOLEN,rlImgrec.URNO); 
            get_fld(pclData,FIELD_2,STR,USIDLEN,rlImgrec.SDID); strip_special_char(rlImgrec.SDID);
            get_fld(pclData,FIELD_3,STR,SDNMLEN,rlImgrec.SDNM); strip_special_char(rlImgrec.SDNM);
            get_fld(pclData,FIELD_4,STR,BODYLEN,rlImgrec.MGBY); strip_special_char(rlImgrec.MGBY);
            get_fld(pclData,FIELD_5,STR,DATELEN,rlImgrec.SDDT); 
            sprintf( pcgXmlStr, "<MSG>\n"
                                "<URNO>%s</URNO>\n"
                                "<ACTION>INSERT</ACTION>\n"
                                "<SENT_BY_ID>%s</SENT_BY_ID>\n"
                                "<SENT_BY_NAME>%s</SENT_BY_NAME>\n"
                                "<MSG_BODY>%s</MSG_BODY>\n"
                                "<SENT_DT>%s</SENT_DT>\n"
                                "<RCV>\n",
                                rlImgrec.URNO, rlImgrec.SDID, rlImgrec.SDNM, rlImgrec.MGBY, rlImgrec.SDDT );
            /* MSI: 20080919: SEND changed to SENT */   

            sprintf( pclSqlBuf, "SELECT TRIM(URNO), TRIM(MGNO), TRIM(TOID), TRIM(TOFN), TRIM(RCID), "
                                "TRIM(RCFN), TRIM(RCDT) FROM IMRTAB WHERE MGNO = '%s'", pcpMsgUrno );  
            sprintf( pclAction, "%s", " " );
            dbg (DEBUG, "<%s> Action <%s>", pclFunc, pclAction );
        } /* Insertion msg */
        else
        {
            sprintf( pclSqlBuf, "SELECT TRIM(URNO), TRIM(MGNO), TRIM(TOID), TRIM(TOFN), TRIM(RCID), "
                                "TRIM(RCFN), TRIM(RCDT) FROM IMRTAB WHERE URNO = '%s'", pcpRcverUrno );  
            sprintf( pclAction, "%s", "<ACTION>UPDATE</ACTION>" );
            dbg (DEBUG, "<%s> Action <%s>", pclFunc, pclAction );
        }

        dbg (TRACE, "<%s> SQL Query (%s)", pclFunc, pclSqlBuf );

        slSqlFunc = START;
        slSqlCursor = 0;
        ilCount = 0;
        while( (sql_if(slSqlFunc,&slSqlCursor,pclSqlBuf,pclData)) == DB_SUCCESS) 
        {
            slSqlFunc = NEXT;
            ilCount++;

            memset( &rlImrrec, 0, sizeof( IMRREC ) );

            get_fld(pclData,FIELD_1,STR,URNOLEN,rlImrrec.URNO); 
            get_fld(pclData,FIELD_2,STR,URNOLEN,rlImrrec.MGNO); 
            get_fld(pclData,FIELD_3,STR,USIDLEN,rlImrrec.TOID); strip_special_char(rlImrrec.TOID);
            get_fld(pclData,FIELD_4,STR,FNLNLEN,rlImrrec.TOFN); strip_special_char(rlImrrec.TOFN);
            get_fld(pclData,FIELD_5,STR,USIDLEN,rlImrrec.RCID); strip_special_char(rlImrrec.RCID);
            get_fld(pclData,FIELD_6,STR,FNLNLEN,rlImrrec.RCFN); strip_special_char(rlImrrec.RCFN);
            get_fld(pclData,FIELD_7,STR,DATELEN,rlImrrec.RCDT); 

            /** Strip the ID type **/
            strcpy( pclToType, "<TP></TP>" );
            if( !strncmp( rlImrrec.TOID, FLIGHT_TOID, 2 ) )
            {
                sprintf( pclToType, "<TP>%c</TP>", rlImrrec.TOID[0] );

                strcpy( pclTmpStr, &rlImrrec.TOID[2] );
                strcpy( rlImrrec.TOID, pclTmpStr );
            }

            sprintf( pclTmpStr, "<MSGTO>\n"
                                "<URNO>%s</URNO>\n"
                                "%s\n"
                                "<MSG_URNO>%s</MSG_URNO>\n"
                                "<MSG_TO_ID>%s</MSG_TO_ID>\n"
                                "<TONM>%s</TONM>\n"
                                "<RCV_BY>%s</RCV_BY>\n"
                                "<RCNM>%s</RCNM>\n"
                                "<RCV_DT>%s</RCV_DT>\n"
                                "%s\n"
                                "</MSGTO>\n",
                                rlImrrec.URNO, pclAction, rlImrrec.MGNO, 
                                rlImrrec.TOID, rlImrrec.TOFN, rlImrrec.RCID, 
                                rlImrrec.RCFN, rlImrrec.RCDT, pclToType );
            dbg (TRACE, "<%s> ilCount <%d> TmpStr <%s>", pclFunc, ilCount, pclTmpStr );
            if( pcpRcverUrno != NULL )
            {
                strcpy( pcgXmlStr, pclTmpStr );
                break;  /* Enough */
            }
            strcat( pcgXmlStr, pclTmpStr );
            if( ilCount >= MAX_DB_RECORDS )
                break;
        } /* while loop */
        close_my_cursor(&slSqlCursor);

        if( pcpRcverUrno == NULL )
        {
            sprintf( pclTmpStr, "%s", "</RCV>\n</MSG>\n" ); /* Only for insertion msg */
            strcat( pcgXmlStr, pclTmpStr ); /* Only for insertion msg */
        }
    } /* MESSAGING handler */
    
    dbg (TRACE, "<%s> XmlType = (%s)", pclFunc, pcpXmlType ); 
    if( pcpRcverUrno != NULL )
        dbg (TRACE, "<%s> RcvURNO = <%s>", pclFunc, pcpRcverUrno ); 
    if( pcpMsgUrno != NULL )
        dbg (TRACE, "<%s> MsgURNO = <%s>", pclFunc, pcpMsgUrno ); 
    dbg (TRACE, "<%s> Xml Str = (%s)", pclFunc, pcgXmlStr ); 
    dbg (TRACE, "<%s> ilCount = %d", pclFunc, ilCount ); 

    /* Send to MQ handler process ITKxxx */
    ilRc = SendCedaEvent(igITKMSG,0, " ", " ", " ", " ","ITK", " ", " "," ", pcgXmlStr, "", 4, 0) ;
    dbg (TRACE, "<%s> ilRc for SendCedaEvent = %d", pclFunc, ilRc );
    return;

} /* end MessageSending */

/* pcpTripChange = "L-N" or "N-L" */
int UpdateNLTrip( char *pcpRole, char *pcpUAFT, char *pcpTripChange )
{
    BOOL blSender = FALSE;
    int ilRc = DB_SUCCESS;
    int ilCount = 0;
    int ilMaxTripNo = 0;
    short slSqlFunc = 0;
    short slSqlCursor = 0;
    char pclSqlBuf[DBQRLEN] = "\0";
    char pclSqlData[512] = "\0";
    char pclSqlBuf2[DBQRLEN] = "\0";
    char pclTripTag[8] = "\0";
    char pclTripNoTag[8] = "\0";
    char pclTripNo[32] = "\0";
    char pclUrno[32] = "\0";
    char pclTmpStr[512] = "\0";
    char clGetTripType;
    char clSetTripType = ' ';
    char *pclFunc = "UpdateNLTrip";

    blSender = FALSE;
    if( !strncmp( pcpRole, "SENDER", 6 ) )
        blSender = TRUE;
    clGetTripType = pcpTripChange[0];
    if( strlen(pcpTripChange) >= 3 )
        clSetTripType = pcpTripChange[2];
    dbg( DEBUG, "<%s> From %c to %c", pclFunc, clGetTripType, clSetTripType );

    /* Is Sender, Check STTY - send trip type */
    strcpy( pclTripTag, "RTTY" );
    strcpy( pclTripNoTag, "RTNO" );
    if( blSender == TRUE )
    {
        strcpy( pclTripTag, "STTY" );
        strcpy( pclTripNoTag, "STNO" );
    }

    sprintf( pclSqlBuf, "SELECT URNO, %s FROM ITRTAB WHERE UAFT = '%s' AND %s = '%c' ORDER BY %s DESC", 
                        pclTripNoTag, pcpUAFT, pclTripTag, clGetTripType, pclTripNoTag );
    dbg( TRACE, "<%s> SelITR <%s>", pclFunc, pclSqlBuf );

    slSqlFunc = START;
    slSqlCursor = 0;
    ilCount = 0;
    sprintf( pcgXmlStr, "<TRIP>"
                        "<%s>%c</%s>", pclTripTag, clSetTripType, pclTripTag );

    while ( sql_if(slSqlFunc,&slSqlCursor,pclSqlBuf,pclSqlData) == DB_SUCCESS )
    {
        get_fld(pclSqlData,FIELD_1,STR,URNOLEN,pclUrno); 
        get_fld(pclSqlData,FIELD_2,STR,10,pclTripNo); 

        /* Only for N-L then we only update the highest trip number with L */
        if( !strncmp( pcpTripChange, "N-L", 3 ) )
        {
            if( !ilMaxTripNo )
            {
                ilMaxTripNo = atoi(pclTmpStr);
                dbg( DEBUG, "<%s> MaxSTNO <%d>", pclFunc, ilMaxTripNo );
            }
            else
            {
                if( atoi(pclTmpStr) != ilMaxTripNo )
                {
                    dbg( TRACE,"<%s> Enough upd. Only upd Highest STNO", pclFunc );
                    break;
                }            
            }
        }
        slSqlFunc = NEXT;
        sprintf( pclSqlBuf2, "UPDATE ITRTAB SET %s = '%c' WHERE URNO = '%s'", pclTripTag, clSetTripType, pclUrno );  
        dbg( TRACE, "<%s> SqlBuf2 <%s>", pclFunc, pclSqlBuf2 );
        ilRc = getOrPutDBData( pclSqlBuf2,pclTmpStr,START );   
        if( ilRc == DB_SUCCESS )
        {
            ilCount++;
            TrimUrno( pclUrno );
            sprintf( pclTmpStr, "<URNO>%s</URNO>", pclUrno );
            strcat( pcgXmlStr, pclTmpStr );
        }
    } /* while */
    close_my_cursor(&slSqlCursor);

    if( ilCount <= 0 )
        return RC_FAIL;

    strcat( pcgXmlStr, "</TRIP>" );
    dbg( TRACE, "<%s> XML <%s>", pclFunc, pcgXmlStr );

    ilRc = SendCedaEvent(igITKCPM,0, " ", " ", " ", " ","ITK", " ", " "," ", pcgXmlStr, "", 4, 0) ;
    dbg( DEBUG, "<%s> ilRc for SendCedaEvent = %d", pclFunc, ilRc );
} /* UpdateNLTrip */


int BulkMessageSending( char *pcpXmlType, int ipDuration )
{
    struct tm *prlTime;
    IMGREC rlImgrec;
    IMRREC rlImrrec;
    time_t ilNow;
    int ilRc = 0;
    int ilMsgCount = 0;
    int ilMsgToCount = 0;
    int ilXmlStrSize = 0;
    short slSqlFunc = 0;
    short slSqlFunc2 = 0;
    short slSqlCursor = 0;
    short slSqlCursor2 = 0;
    char pclSqlBuf[DBQRLEN] = "\0";
    char pclSqlBuf2[DBQRLEN] = "\0";
    char pclData[DBQRLEN] = "\0";
    char pclTmpStr[MQRDLEN] = "\0";  
    char pclNowTime[DATELEN+1];
    char pclHistTime[DATELEN+1];
    char pclMsgUrno[URNOLEN+1] = "\0";
    char pclToType[20];
    char *pclFunc = "BulkMessageSending";
    char *pclXmlStr;

    memset( &rlImgrec, 0, sizeof( rlImgrec ) );
    memset( &rlImrrec, 0, sizeof( rlImrrec ) );
    memset( &pclData, 0, sizeof( pclData ) );

    dbg (TRACE, "<%s> : DURATION = %d", pclFunc, ipDuration);
    if( ipDuration <= 0 || ipDuration > 12 )  /* in terms of HOUR */
    {
        dbg (TRACE, "<%s> : Invalid DURATION = %d Set to 1 hour", pclFunc, ipDuration);
        ipDuration = 1;
    }

    ilNow = time( (time_t *)0 );
    prlTime = localtime( (time_t *)&ilNow );
    sprintf( pclNowTime, "%4.4d%2.2d%2.2d%2.2d%2.2d%2.2d", prlTime->tm_year+1900, prlTime->tm_mon+1, prlTime->tm_mday,
                                                            prlTime->tm_hour, prlTime->tm_min, prlTime->tm_sec );
    ilNow = time( (time_t *)0 ) - (ipDuration * 3600);
    prlTime = localtime( (time_t *)&ilNow );
    sprintf( pclHistTime, "%4.4d%2.2d%2.2d%2.2d%2.2d%2.2d", prlTime->tm_year+1900, prlTime->tm_mon+1, prlTime->tm_mday,
                                                            prlTime->tm_hour, prlTime->tm_min, prlTime->tm_sec );

    dbg (TRACE, "<%s> Searching msg between (%s) and (%s)", pclFunc, pclHistTime, pclNowTime );

    pclXmlStr = (char *)malloc( START_DATA_SIZE );
    if( pclXmlStr == NULL )
    {
        dbg( TRACE, "%s: ERROR!!! in getting memory!!!", pclFunc );
        exit( 3 );
    }
    ilXmlStrSize = START_DATA_SIZE;

    if( !strcmp( pcpXmlType, "MESSAGETO" ) )
    {
        sprintf( pclXmlStr, "%s", "<MSGSTO>" );

        sprintf( pclSqlBuf2, "SELECT TRIM(URNO) FROM IMGTAB WHERE SDDT BETWEEN '%s' AND '%s'",
                             pclHistTime, pclNowTime );
        dbg (DEBUG, "<%s> IMGTAB SqlBuf <%s>", pclFunc, pclSqlBuf2 );

        slSqlFunc2 = START;
        slSqlCursor2 = 0;
        ilMsgCount = 0;
        while( (sql_if(slSqlFunc2,&slSqlCursor2,pclSqlBuf2,pclMsgUrno)) == DB_SUCCESS) 
        {
            ilMsgCount++;
            slSqlFunc2 = NEXT;

            sprintf( pclSqlBuf, "SELECT TRIM(URNO),TRIM(MGNO),TRIM(TOID),TRIM(TOFN), "
                                "TRIM(RCID),TRIM(RCFN),TRIM(RCDT) "
                                "FROM IMRTAB WHERE MGNO = '%s'", pclMsgUrno );
            dbg (DEBUG, "<%s> IMRTAB SqlBuf <%s>", pclFunc, pclSqlBuf );

            slSqlFunc = START;
            slSqlCursor = 0;
            ilMsgToCount = 0;
            while( (sql_if(slSqlFunc,&slSqlCursor,pclSqlBuf,pclData)) == DB_SUCCESS) 
            {
                if( strlen( pclXmlStr ) > (ilXmlStrSize - 1000) )
                {
                    if( (ilXmlStrSize+BLK_DATA_SIZE) > igXmlCapSize )
                    {
                        dbg (TRACE, "<%s> Too much data to be sent!! ABORT!", pclFunc );
                        free( pclXmlStr );
                        close_my_cursor(&slSqlCursor);
                        return;
                    }

                    pclXmlStr = (char *)realloc( pclXmlStr, ilXmlStrSize + BLK_DATA_SIZE ); 
                    if( pclXmlStr == NULL )
                    {
                        dbg (TRACE, "<%s> MSGTO Fail to enlarge XML buffer", pclFunc );
                        exit( 2 );
                    }
                    ilXmlStrSize += BLK_DATA_SIZE;
                    dbg (TRACE, "<%s> MSGTO Increase XML Str size to <%d>", pclFunc, ilXmlStrSize );
                }

                slSqlFunc = NEXT;
                ilMsgToCount++;
                memset( &rlImrrec, 0, sizeof( IMRREC ) );

                get_fld(pclData,FIELD_1,STR,URNOLEN,rlImrrec.URNO); 
                get_fld(pclData,FIELD_2,STR,URNOLEN,rlImrrec.MGNO); 
                get_fld(pclData,FIELD_3,STR,USIDLEN,rlImrrec.TOID); strip_special_char(rlImrrec.TOID); 
                get_fld(pclData,FIELD_4,STR,FNLNLEN,rlImrrec.TOFN); strip_special_char(rlImrrec.TOFN);
                get_fld(pclData,FIELD_5,STR,USIDLEN,rlImrrec.RCID); strip_special_char(rlImrrec.RCID);
                get_fld(pclData,FIELD_6,STR,FNLNLEN,rlImrrec.RCFN); strip_special_char(rlImrrec.RCFN);
                get_fld(pclData,FIELD_7,STR,DATELEN,rlImrrec.RCDT); 

                /** Strip the ID type **/
                strcpy( pclToType, "<TP></TP>" );
                if( !strncmp( rlImrrec.TOID, FLIGHT_TOID, 2 ) )
                {
                    sprintf( pclToType, "<TP>%c</TP>", rlImrrec.TOID[0] );

                    strcpy( pclTmpStr, &rlImrrec.TOID[2] );
                    strcpy( rlImrrec.TOID, pclTmpStr );
                }

                sprintf( pclTmpStr, "<MSGTO>\n"
                            "<URNO>%s</URNO>\n"
                            "<MSG_URNO>%s</MSG_URNO>\n"
                            "<MSG_TO_ID>%s</MSG_TO_ID>\n"
                            "<TONM>%s</TONM>\n"
                            "<RCV_BY>%s</RCV_BY>\n"
                            "<RCNM>%s</RCNM>\n"
                            "<RCV_DT>%s</RCV_DT>\n"
                            "%s\n"
                            "</MSGTO>\n",
                            rlImrrec.URNO, rlImrrec.MGNO, 
                            rlImrrec.TOID, rlImrrec.TOFN, rlImrrec.RCID, 
                            rlImrrec.RCFN, rlImrrec.RCDT, pclToType );
                strcat( pclXmlStr, pclTmpStr );
                /*if( ilMsgToCount >= MAX_DB_RECORDS )
                    break;*/
            } /* end while MESSAGETO */
            dbg (TRACE, "<%s> ilMsgToCount = %d", pclFunc, ilMsgToCount ); 
            close_my_cursor(&slSqlCursor);
            /*if( ilMsgCount >= MAX_DB_RECORDS )
               break;*/
        } /* end while MESSAGE */
        close_my_cursor(&slSqlCursor2);

        sprintf( pclTmpStr, "%s", "</MSGSTO>" );
        strcat( pclXmlStr, pclTmpStr );

        dbg (TRACE, "<%s> ilMsgCount = %d", pclFunc, ilMsgCount ); 
        /*if( ilMsgCount > 0 )
        {*/
            dbg (TRACE, "<%s> Xml Str = (%s)", pclFunc, pclXmlStr ); 
            ilRc = SendCedaEvent(igITKMSG, 0, " ", " ", " ", " ","ITK", " ", " "," ", pclXmlStr, "", 3, 0) ;
            dbg (TRACE, "<%s> ilRc for SendCedaEvent = %d", pclFunc, ilRc );
        /*}*/
        free( pclXmlStr );
        return;
    } /* MESSAGETO Handler */

    if( !strcmp( pcpXmlType, "MESSAGE" ) )
    {
        sprintf( pclSqlBuf, "SELECT TRIM(URNO),TRIM(SDID),TRIM(SDNM),TRIM(MGBY),TRIM(SDDT) "
                            "FROM IMGTAB WHERE SDDT BETWEEN '%s' AND '%s'", pclHistTime, pclNowTime );
        dbg (DEBUG, "<%s> IMGTAB SqlBuf <%s>", pclFunc, pclSqlBuf );

        slSqlFunc = START;
        slSqlCursor = 0;
        ilMsgCount = 0;
        sprintf( pclXmlStr, "%s", "<MSGS>" );
        while( (sql_if(slSqlFunc,&slSqlCursor,pclSqlBuf,pclData)) == DB_SUCCESS) 
        {
            slSqlFunc = NEXT;
            ilMsgCount++;

            if( strlen( pclXmlStr ) > (ilXmlStrSize - 1000) )
            {
                pclXmlStr = realloc( pclXmlStr, ilXmlStrSize + BLK_DATA_SIZE ); 
                if( pclXmlStr == NULL )
                {
                    dbg (TRACE, "<%s> MSG: Fail to enlarge XML buffer", pclFunc );
                    free(pclXmlStr);
                    close_my_cursor(&slSqlCursor);
                    return;
                }
                ilXmlStrSize += BLK_DATA_SIZE;
                dbg (TRACE, "<%s> MSG: Increase XML Str size to <%d>", pclFunc, ilXmlStrSize );
            }

            memset( &rlImgrec, 0, sizeof( IMGREC ) );

            get_fld(pclData,FIELD_1,STR,URNOLEN,rlImgrec.URNO); 
            get_fld(pclData,FIELD_2,STR,USIDLEN,rlImgrec.SDID); strip_special_char(rlImgrec.SDID);
            get_fld(pclData,FIELD_3,STR,SDNMLEN,rlImgrec.SDNM); strip_special_char(rlImgrec.SDNM);
            get_fld(pclData,FIELD_4,STR,BODYLEN,rlImgrec.MGBY); strip_special_char(rlImgrec.MGBY);
            get_fld(pclData,FIELD_5,STR,DATELEN,rlImgrec.SDDT); 

            sprintf( pclTmpStr, "<MSG>\n"
                            "<URNO>%s</URNO>\n"
                            "<SENT_BY_ID>%s</SENT_BY_ID>\n"
                            "<SENT_BY_NAME>%s</SENT_BY_NAME>\n"
                            "<MSG_BODY>%s</MSG_BODY>\n"
                            "<SENT_DT>%s</SENT_DT>\n</MSG>\n",
                            rlImgrec.URNO, rlImgrec.SDID, rlImgrec.SDNM, rlImgrec.MGBY, rlImgrec.SDDT );
            strcat( pclXmlStr, pclTmpStr );
            /*if( ilMsgCount >= MAX_DB_RECORDS )
                break;*/
        }
        close_my_cursor(&slSqlCursor);
        sprintf( pclTmpStr, "%s", "</MSGS>\n" );
        strcat( pclXmlStr, pclTmpStr );

        dbg (TRACE, "<%s> ilMsgCount = %d", pclFunc, ilMsgCount ); 
        /*if( ilMsgCount > 0 )
        {*/
            dbg (TRACE, "<%s> Xml Str = (%s)", pclFunc, pclXmlStr ); 
            ilRc = SendCedaEvent(igITKMSG, 0, " ", " ", " ", " ","ITK", " ", " "," ", pclXmlStr, "", 3, 0) ;
            dbg (TRACE, "<%s> ilRc for SendCedaEvent = %d", pclFunc, ilRc );
        /*}*/
        free( pclXmlStr );
        return ilRc;
    } /* MESSAGE Handler */

    dbg (TRACE, "<%s> Invalid XmlType (%s)", pclFunc, pcpXmlType );

} /* BulkMessageSending */


/* selection <WHERE UAFT = '1421467246' AND USDE = '1476843519'> */
/* data      <20070202021010,20070212075154,ITKHDL> */
/* 29-Apr-2008: Change to upd JOBTAB's STAT as C00 when MAB is recv */
void SendStatusXML(char *pcpSelection, char *pcpData, char *pcpTw_start)
{
    int ilRc = RC_SUCCESS;
    int ilStatIndex = -1;
    char pclSqlBuf[1024] = "\0";
    char pclData[1024] = "\0";    
    char pclSelection[1024] = "\0";
    char pclAdid[4] = "\0";
    char pclOSTUrno[12] = "\0";
    char pclAFTUrno[12] = "\0";
    char pclTagValue[16] = "\0";
    char pclTagName[36] = "\0";
    char pclNowTime[20] = "\0";
    char *pclToken;
    char *pclFunc = "SendStatusXml";

    dbg(TRACE,"<%s> ========== Start ==========",pclFunc);

    pclToken = (char *)strtok( pcpSelection, "'" );
    pclToken = (char *)strtok( NULL, "'" );
    if( pclToken == NULL )
    {
        dbg(TRACE,"<%s> Error in getting AFTTAB Urno <%s>", pclFunc, pcpSelection);
        return;
    }
    strcpy( pclOSTUrno, pclToken );
    pclOSTUrno[strlen(pclToken)] = '\0';
    dbg(TRACE,"<%s> UOST <%s>", pclFunc, pclOSTUrno);

    sprintf(pclSqlBuf,"SELECT TRIM(ADID), TRIM(UAFT), TRIM(CONA), TRIM(DENM) FROM STATVW WHERE UOST = '%s'", pclOSTUrno );
    dbg (TRACE, "<%s> sqlBuf = %s", pclFunc, pclSqlBuf );
    ilRc = getOrPutDBData( pclSqlBuf,pclData,START );   
    if( ilRc != DB_SUCCESS )
    {
        dbg(TRACE,"<%s>: Error in getting info from STATVW", pclFunc );
        return;
    }
    get_fld(pclData,FIELD_1,STR,1,pclAdid);
    get_fld(pclData,FIELD_2,STR,URNOLEN,pclAFTUrno);
    get_fld(pclData,FIELD_3,STR,DATELEN,pclTagValue); strip_special_char(pclTagValue);
    get_fld(pclData,FIELD_4,STR,32,pclTagName);       strip_special_char(pclTagName);
    UtcToLocalTimeFixTZ(pclTagValue);

    /* Get Status name */
    ilStatIndex = -1;
    ilRc = GetStatusNameIndex( pclTagName, &ilStatIndex, FALSE );
    if( ilRc == RC_FAIL )
    {
        dbg(TRACE,"<%s>: Tag NAME (%s) not found in config file",pclFunc, pclTagName);
        return;
    }
    if( ilStatIndex < 0 || ilStatIndex >= igNumStatus )
    {
        dbg(TRACE,"<%s>: Invalid index = %d found for Tag NAME <%s>",pclFunc, ilStatIndex, pclTagName);
        return;
    }

    /* Commented on 08-May-2008. Web Server is keeping the time */
    /*sprintf( pcgXmlStr, "<%cFLIGHT>"
                        "<URNO>%s</URNO>"
                        "<%s>%s</%s>"
                        "</%cFLIGHT>", pclAdid[0], pclAFTUrno, 
                                       prgStatName[ilStatIndex].iTC_name,
                                       pclTagValue,
                                       prgStatName[ilStatIndex].iTC_name,
                                       pclAdid[0] );
    dbg(TRACE,"<%s> XML <%s>", pclFunc, pcgXmlStr );

    ilRc = SendCedaEvent(igITKREQ,0, " ", " ", " ", " ","ITK", " ", " "," ", 
                         pcgXmlStr, "", atoi(cgSendPrio), RC_SUCCESS) ;
    if( ilRc != RC_SUCCESS )
    {
        dbg(TRACE,"<%s>: Not successful in sending reply", pclFunc );
        return;
    } */

    /** Inform ITKREP to update ITRTAB on the timing **/
/*  sprintf( pclFields, "%s", "NAME,TIME,UAFT" );
    sprintf( pclData, "%s,%s,%s", prgStatName[ilStatIndex].iTC_name, pclTagValue, pclAFTUrno );
    dbg( TRACE, "<%s> Forward to ITRREP <%s>", pclFunc, pclData );
    ilRc = SendCedaEvent(igITKREP,0, " ", " ", " ", " ","TIM", " ", " ",pclFields, pclData, "", 3, 0) ;

    if( bgJobConfirm == TRUE && !strncmp( prgStatName[ilStatIndex].iTC_name, "MAB", 3 ) )
    {
        strcpy( pclPeno, pcpTw_start );
        dbg( TRACE, "<%s> PENO <%s>", pclFunc, pclPeno );

        sprintf(pclSqlBuf,"SELECT URNO FROM STFTAB WHERE PENO = '%s'", pclPeno );
        dbg (TRACE, "<%s> SelStf <%s>", pclFunc, pclSqlBuf );
        ilRc = getOrPutDBData( pclSqlBuf,pclData,START );   
        if( ilRc != DB_SUCCESS )
        {
            dbg(TRACE,"<%s>: Error in getting info from STFTAB", pclFunc );
            return;
        }
        strcpy( pclSTFUrno, pclData );
        dbg( TRACE, "<%s> From STFTAB USTF <%s>", pclFunc, pclSTFUrno );

        sprintf(pclSqlBuf,"SELECT TRIM(URNO), TRIM(DETY), TRIM(UTPL), TRIM(STAT) FROM JOBTAB WHERE "
                          "UAFT = '%s' AND USTF = '%s'", pclAFTUrno, pclSTFUrno );
        dbg (TRACE, "<%s> SelJob <%s>", pclFunc, pclSqlBuf );
        ilRc = getOrPutDBData( pclSqlBuf,pclData,START );   
        if( ilRc != DB_SUCCESS )
        {
            dbg(TRACE,"<%s>: Arr record for turnarnd flight. Upd Dep job", pclFunc );
            sprintf( pclSqlBuf, "SELECT URNO FROM AFTTAB WHERE RKEY = '%s' AND ADID = 'D'", pclAFTUrno );
            dbg (TRACE, "<%s> SelDAFT <%s>", pclFunc, pclSqlBuf );
            ilRc = getOrPutDBData( pclSqlBuf,pclDAFTUrno,START );   
            if( ilRc != DB_SUCCESS )
                dbg( TRACE, "<%s> Fail to find DEP flight in AFT for turnaround arr flight <%s> recv", pclFunc, pclAFTUrno );
            else
            {
                sprintf(pclSqlBuf,"SELECT TRIM(URNO), TRIM(DETY), TRIM(UTPL), TRIM(STAT) FROM JOBTAB WHERE "
                                  "UAFT = '%s' AND USTF = '%s'", pclDAFTUrno, pclSTFUrno );
                dbg (TRACE, "<%s> SelDJob <%s>", pclFunc, pclSqlBuf );
                ilRc = getOrPutDBData( pclSqlBuf,pclData,START );   
                if( ilRc != DB_SUCCESS )
                {
                    dbg(TRACE,"<%s>: Error in getting info from JOBTAB", pclFunc );
                    return;
                }
            }
        }
        get_fld(pclData,FIELD_1,STR,URNOLEN,pclJOBUrno);
        get_fld(pclData,FIELD_2,STR,1,pclDETY);
        get_fld(pclData,FIELD_3,STR,10,pclUTPL);
        get_fld(pclData,FIELD_4,STR,12,pclSTAT);
        dbg(TRACE,"<%s>: UJOB <%s> DETY <%s> UTPL <%s> STAT <%s>", pclFunc, pclJOBUrno, pclDETY, pclUTPL, pclSTAT );

        if( strstr( cgJobTemplates, pclUTPL ) != NULL && strcmp( pclSTAT, pcgJobConfirmStatus ) )
        {
            GetServerTimeStamp("UTC",1,0,pclNowTime);
            sprintf( pclFields, "STAT,USEU,LSTU" );
            sprintf( pclData, "%-10.10s,iTrek %s,%14.14s", pcgJobConfirmStatus, pclSTFUrno, pclNowTime );
            sprintf( pclSelection, "WHERE URNO = '%s'", pclJOBUrno );
            dbg( TRACE, "<%s> Forward to SQLHDL <%s>", pclFunc, pclData );
            ilRc = SendCedaEvent(igSQLHDL,0, " ", " ", " ", " ","URT", "JOBTAB", pclSelection,
                                 pclFields, pclData, "" , 3, 0) ;
            dbg(DEBUG,"<%s>: ilRc <%d> ", pclFunc, ilRc );
        }
    }
*/
    dbg(TRACE,"<%s> ========== End ==========",pclFunc);
    return;
} /* SendStatusXML */

void InitStatusConfig()
{
    int ilRc = RC_SUCCESS;
    int ili = 0;
    char pclTmpStr[128];
    char pclStatus[1024];
    char *pclFunc = "InitStatusConfig";


    ilRc = ReadConfigEntry( cgConfigFile, "STATUS_UPD","DEPT_AO", pcgHssName[HSS_AO] );
    if( ilRc != RC_SUCCESS )
        sprintf( pcgHssName[HSS_AO], "%s", "'Apron'" );
    ilRc = ReadConfigEntry( cgConfigFile, "STATUS_UPD","DEPT_BO", pcgHssName[HSS_BO] );
    if( ilRc != RC_SUCCESS )
        sprintf( pcgHssName[HSS_BO], "%s", "'Baggage'" );
    ilRc = ReadConfigEntry( cgConfigFile, "STATUS_UPD","DEPT_AIC", pcgHssName[HSS_AIC] );
    if( ilRc != RC_SUCCESS )
        sprintf( pcgHssName[HSS_AIC], "%s", "'A/C Interior Cleaning'" );
    dbg (TRACE, "<%s> HssName[AO] = <%s>", pclFunc, pcgHssName[HSS_AO] );
    dbg (TRACE, "<%s> HssName[BO] = <%s>", pclFunc, pcgHssName[HSS_BO] );
    dbg (TRACE, "<%s> HssName[AIC] = <%s>", pclFunc, pcgHssName[HSS_AIC] );


    bgUpdStatus = FALSE;
    ilRc = ReadConfigEntry( cgConfigFile, "STATUS_UPD","UPD_STATUS", pclTmpStr );
    if( !strncmp( pclTmpStr, "ON", 2 ) ) 
        bgUpdStatus = TRUE;

    if( bgUpdStatus == FALSE )
    {
        dbg (TRACE, "<%s> DO NOT UPDATE STATUS <%s>", pclFunc, pclTmpStr );
        return;
    }

    /* Mei, 12-May-2008 */
    bgJobConfirm = FALSE;
    ilRc = ReadConfigEntry( cgConfigFile, "STATUS_UPD","JOB_CONFIRM", pclTmpStr );
    if( !strncmp( pclTmpStr, "ON", 2 ) ) 
        bgJobConfirm = TRUE;
    dbg (TRACE, "<%s> JOB CONFIRM <%d> = ON-1 OFF-0", pclFunc, bgJobConfirm );

    /* Mei, 21-Aug-2008 */
    ilRc = ReadConfigLine( cgConfigFile, "STATUS_UPD","JCONFIRM_STATUS", pcgJobConfirmStatus );
    if( ilRc != RC_SUCCESS )
        strcpy( pcgJobConfirmStatus, "C00" );
    dbg (TRACE, "<%s> ilRc <%d> JOB CONFIRM STATUS <%s>", pclFunc, ilRc, pcgJobConfirmStatus );

    /* Mei, 22-Apr-2010 */
    bgTrackEvent = FALSE;
    ilRc = ReadConfigEntry( cgConfigFile, "MAIN", "TRACK_EVENT", pclTmpStr );
    if( !strncmp( pclTmpStr, "YES", 3 ) ) 
        bgTrackEvent = TRUE;
    dbg (TRACE, "<%s> TRACK EVENT <%d> = YES-1 NO-0", pclFunc, bgTrackEvent );

    ilRc = ReadConfigEntry( cgConfigFile, "STATUS_UPD","EXCLUDE_AIRLINES", pcgStatusAirlines );
    if( ilRc != RC_SUCCESS )
        strcpy( pcgStatusAirlines, "'SQ','MI'" );
    dbg( TRACE, "<%s> Exclude Airlines for ATA/ATD update on Fips <%s>", pclFunc, pcgStatusAirlines );

    ilRc = ReadConfigEntry( cgConfigFile, "STATUS_UPD","STATUS_LIST", pclStatus );
    if( ilRc != RC_SUCCESS )
    {
        dbg( TRACE, "<%s> WARNING!!! NO STATUS INFORMATION CONFIGURE", pclFunc );
        return;
    }
    dbg( TRACE, "<%s> Status Listing <%s>", pclFunc, pclStatus );

    igNumStatus = get_no_of_items(pclStatus);
    dbg( TRACE, "<%s> Number of Status <%d>", pclFunc, igNumStatus );

    prgStatName = (STAREC *)malloc( sizeof(STAREC) * igNumStatus );
    if( prgStatName == NULL )
    {
        dbg( TRACE, "<%s> ERROR!!! NO MEMORY LEFT FOR STATUS INFORMATION CONFIGURATION", pclFunc );
        return;
    }

    memset( prgStatName, 0, sizeof(STAREC)*igNumStatus );
    for( ili = 0; ili < igNumStatus; ili++ )
    {
        get_real_item( prgStatName[ili].iTC_name, pclStatus, ili + 1 );
        ilRc = ReadConfigLine( cgConfigFile, "STATUS_UPD", prgStatName[ili].iTC_name, prgStatName[ili].iTS_name );
        if( ilRc != RC_SUCCESS ) 
            memset( prgStatName[ili].iTS_name, 0, sizeof(prgStatName[ili].iTS_name) );
        dbg (TRACE, "<%s> iTCName <%s> iTSName <%s>", pclFunc, 
                               prgStatName[ili].iTC_name, prgStatName[ili].iTS_name );
        if( !strncmp( prgStatName[ili].iTC_name, "ATA", 3 ) )
            igIdxATA = ili;
        else if( !strncmp( prgStatName[ili].iTC_name, "ATD", 3 ) )
            igIdxATD = ili;
    }
    dbg (TRACE, "<%s> igIdxATA <%d> igIdxATA <%d>", pclFunc, igIdxATA, igIdxATD );

} /* InitStatusConfig */

void InitOfficerType()
{
    int ili = 0;
    char *pclFunc = "InitOfficerType";

    memset( pcgDefnOfficer, 0, sizeof( pcgDefnOfficer ) );
    for( ili = 0; ili < NUM_OFFICER_TYPE; ili++ )
    {
        ReadConfigEntry( cgConfigOfficerFile, "OFFICER_DEFN",pcgOfficerTitle[ili], pcgDefnOfficer[ili]);
        dbg(TRACE,"Officer Type <%d> Title <%s>: Fcode <%s>", ili, pcgOfficerTitle[ili], pcgDefnOfficer[ili]);
    }
} /* InitOfficerType */

int GetStatusNameIndex( const char *pcpName, int *pipStatusNameIndex, BOOL bpFromiTC )
{
    int ili;
    char *pclFunc = "GetStatusName";

    for( ili = 0; ili < igNumStatus; ili++ )
    {
        if( bpFromiTC == TRUE )
        {
            if( strncmp( prgStatName[ili].iTC_name, pcpName, strlen(pcpName) ) )
                continue;
        }
        else
        {
            if( strncmp( prgStatName[ili].iTS_name, pcpName, strlen(pcpName) ) )
                continue;
        }
        *pipStatusNameIndex = ili;
        dbg (TRACE, "<%s> Status name index <%d> found for SDE name <%s>", 
                              pclFunc, *pipStatusNameIndex, pcpName );
        return RC_SUCCESS;
    }
    return RC_FAIL;
} /* GetStatusNameIndex */

BOOL GetEmpType( char *pcpEmpType, char *pcpFuncCode )
{
    int ili;
    char pclFuncCode[100];
    char pclTmpStr[OFFICER_FCODE_LEN];
    char *pclFunc = "GetEmpType";
    char *pclToken;

    sprintf( pclFuncCode, ",%s,", pcpFuncCode );
    for( ili = 0; ili < NUM_OFFICER_TYPE; ili++ )
    {
        strcpy( pclTmpStr, pcgDefnOfficer[ili] );  /* In case it alter the string */
        /*dbg( DEBUG, "<%s> pcgDefnOfficer[%d] <%s> Fcode <%s>", pclFunc, ili, pclTmpStr, pclFuncCode );*/

        pclToken = (char *)strstr( pclTmpStr, pclFuncCode );
        if( pclToken != NULL )
        {
            strcpy( pcpEmpType, pcgOfficerTitle[ili] );
            break;
        }
    }
    if( ili < NUM_OFFICER_TYPE )
        return TRUE;
    return FALSE; 
} /* GetEmpType */

int GetStaffName( char *pcpSrchStr, char *pcpResStr, char *pcpFirstName, char *pcpLastName, int ipSrchType )
{
    int ilRc; 
    short slSqlFunc;
    short slSqlCursor = 0;
    char pclSqlBuf[DBQRLEN];
    char pclData[DBQRLEN];
    char pclFirstName[NAMELEN+1];
    char pclLastName[NAMELEN+1];
    char pclSTFUrno[URNOLEN+1];
    char pclUsid[USIDLEN+1];
    char *pclFunc = "GetStaffName";

    if( ipSrchType == SRCH_BY_PENO )
        sprintf(pclSqlBuf,"SELECT TRIM(URNO), TRIM(FINM),TRIM(LANM) FROM STFTAB WHERE PENO = '%s'",pcpSrchStr);
    else if( ipSrchType == SRCH_BY_URNO )
        sprintf(pclSqlBuf,"SELECT TRIM(PENO), TRIM(FINM),TRIM(LANM) FROM STFTAB WHERE URNO = %s",pcpSrchStr);
    else
        return;
    dbg (TRACE, "<%s> sqlBuf = %s", pclFunc, pclSqlBuf );

    slSqlFunc = START;
    slSqlCursor = 0;
    ilRc = getOrPutDBData(pclSqlBuf,pclData,slSqlFunc);
    if( ilRc != DB_SUCCESS )
    {
        dbg (TRACE, "<%s> No name found for SrchStr <%s>", pclFunc, pcpSrchStr );
        sprintf( pcpFirstName, "%s", " " );
        sprintf( pcpLastName, "%s", " " );
        return RC_FAIL;
    }           
    
    if( ipSrchType == SRCH_BY_PENO )
    {
        get_fld(pclData,FIELD_1,STR,URNOLEN,pclSTFUrno);
        strcpy( pcpResStr, pclSTFUrno );
        if( pclSTFUrno != NULL )
            dbg(TRACE,"<%s> Find URNO Usid <%s> STFUrno <%s>", pclFunc, pcpSrchStr, pcpResStr );
    }
    else
    {
        get_fld(pclData,FIELD_1,STR,USIDLEN,pclUsid); strip_special_char(pclUsid);
        strcpy( pcpResStr, pclUsid );
        if( pclUsid != NULL )
            dbg(TRACE,"<%s> Find PENO Usid <%s> STFUrno <%s>", pclFunc,pcpResStr, pcpSrchStr );
    }
    get_fld(pclData,FIELD_2,STR,NAMELEN,pclFirstName); strip_special_char(pclFirstName);
    get_fld(pclData,FIELD_3,STR,NAMELEN,pclLastName);  strip_special_char(pclLastName);

    strcpy( pcpFirstName, pclFirstName );
    strcpy( pcpLastName, pclLastName );

    if( pclFirstName != NULL )
        dbg(TRACE,"<%s> FirstName <%s>", pclFunc,pclFirstName );
    if( pclLastName != NULL )
        dbg(TRACE,"<%s> LastName <%s>", pclFunc,pclLastName );
    return RC_SUCCESS;
} /* GetStaffName */


int GetSortOrder( char *pcpOrderBy, char *pcpSortOrder )
{
    char pclOrderBy[30] = "\0";
    char pclSortOrder[100] = "\0";
    char *pclFunc = "GetSortOrder";

    strcpy( pclOrderBy, pcpOrderBy );
    memset( pclSortOrder, 0, sizeof( pclSortOrder ) );

    if( !strncmp( pclOrderBy, "SBY", 3 ) )
        sprintf( pclSortOrder, "%s", "SDNM, SDID" );
    else if( !strncmp( pclOrderBy, "SON", 3 ) )
        sprintf( pclSortOrder, "%s", "SDDT" );
    else if( !strncmp( pclOrderBy, "STO", 3 ) )
        sprintf( pclSortOrder, "%s", "TOFN, TOID" );
    else if( !strncmp( pclOrderBy, "RBY", 3 ) )
        sprintf( pclSortOrder, "%s", "RCFN, RCID" );
    else if( !strncmp( pclOrderBy, "RON", 3 ) )
        sprintf( pclSortOrder, "%s", "RCDT" );
    else
    {
    dbg( TRACE, "<%s> No Sort Order", pclFunc );
        return RC_FAIL;
    }
    strcpy( pcpSortOrder, pclSortOrder );
    dbg( TRACE, "<%s> OrderBy <%s> Sort Order <%s>", pclFunc, pcpOrderBy, pcpSortOrder );
    return RC_SUCCESS;
} /* GetSortOrder */


int PackFlightXml( char *pcpXmlStr, AFTREC rpAftrec, int ipPackType )
{
    char pclTmpStr[1024] = "\0";
    char *pclFunc = "PackFlightXml";

    if( ipPackType == TYPE_INSERT )
        sprintf( pclTmpStr, "%s", "<ACTION>INSERT</ACTION>\n" );
    
    /* 22 field tags */
    sprintf( pcpXmlStr, "<FLIGHT>\n"
                        "<URNO>%s</URNO>\n"
                        "%s"
                        "<FLNO>%s</FLNO>\n"
                        "<STOD>%s</STOD>\n"
                        "<STOA>%s</STOA>\n"
                        "<REGN>%s</REGN>\n"
                        "<DES3>%s</DES3>\n"
                        "<ORG3>%s</ORG3>\n"
                        "<ETDI>%s</ETDI>\n"
                        "<ETAI>%s</ETAI>\n"
                        "<GTA1>%s</GTA1>\n"
                        "<GTD1>%s</GTD1>\n"
                        "<ADID>%s</ADID>\n"
                        "<PSTA>%s</PSTA>\n"
                        "<PSTD>%s</PSTD>\n"
                        "<BLT1>%s</BLT1>\n"
                        "<OFBL>%s</OFBL>\n"
                        "<ONBL>%s</ONBL>\n"
                        "<TGA1>%s</TGA1>\n"
                        "<TGD1>%s</TGD1>\n"
                        "<TTYP>%s</TTYP>\n"
                        "<ACT3>%s</ACT3>\n"
                        "<TURN>%s</TURN>\n"
                        "<FTYP>%s</FTYP>\n"
                        "<RKEY>%s</RKEY>\n"
                        "</FLIGHT>\n", rpAftrec.URNO, 
                                     pclTmpStr, rpAftrec.FLNO, rpAftrec.STOD,
                                     rpAftrec.STOA, rpAftrec.REGN, rpAftrec.DES3,
                                     rpAftrec.ORG3, rpAftrec.ETDI, rpAftrec.ETAI,
                                     rpAftrec.GTA1, rpAftrec.GTD1, rpAftrec.ADID,
                                     rpAftrec.PSTA, rpAftrec.PSTD, rpAftrec.BLT1,
                                     rpAftrec.OFBL, rpAftrec.ONBL, rpAftrec.TGA1,
                                     rpAftrec.TGD1, rpAftrec.TTYP, rpAftrec.ACT3, 
                                     rpAftrec.TURN, rpAftrec.FTYP, rpAftrec.RKEY );
} /* PackFlightXml */

int PackJobXml( char *pcpXmlStr, JOBREC rpJobrec, int ipPackType )
{
    int ilRc = 0;
    int ilStaff;
    int ilEquip;
    BOOL blGetEmpType = FALSE;
    BOOL blFCCOUpd = FALSE;
    BOOL blUpdate = FALSE;
    char pclSqlBuf[512] = "\0";
    char pclXmlStr[2048] = "\0";
    char pclTmpStr[2048] = "\0";
    char pclFirstName[NAMELEN+1] = "\0";
    char pclLastName[NAMELEN+1] = "\0";
    char pclEmpType[20] = "\0";
    char pclUsid[USIDLEN+1] = "\0";
    char pclEquipName[44];
    char *pclFunc = "PackJobXml";

    dbg(TRACE,"<%s> PackType <%d> USTF <%s> UEQU <%s>", pclFunc, ipPackType, rpJobrec.USTF, rpJobrec.UEQU ); 

    /* This is an Equipment Job */
    ilStaff = atoi( rpJobrec.USTF ); 
    ilEquip = atoi( rpJobrec.UEQU ); 
    if( ilEquip > 0 && ilStaff <= 0 ) 
    {
        if( ipPackType == TYPE_DELETE )
        {
            sprintf( pcpXmlStr, "<EQJOB>\n"
                                "<URNO>%s</URNO>\n"
                                "<ACTION>DELETE</ACTION>\n"
                                "</EQJOB>\n", rpJobrec.URNO ) ;
            return RC_SUCCESS;
        }
        sprintf(pclSqlBuf,"SELECT TRIM(ENAM) FROM EQUTAB WHERE URNO = %s", rpJobrec.UEQU );
        dbg( TRACE, "<%s> SelEQU <%s>", pclFunc, pclSqlBuf );
        ilRc = getOrPutDBData(pclSqlBuf,pclEquipName,START);
        if( ilRc != DB_SUCCESS )
        {
            dbg( TRACE, "<%s> No equipment found for UEQU <%s>", pclFunc, rpJobrec.UEQU );
            return RC_FAIL;
        }
        UtcToLocalTimeFixTZ(rpJobrec.ACFR);
        UtcToLocalTimeFixTZ(rpJobrec.ACTO);

        sprintf( pclTmpStr, "<EQJOB>\n"
                            "<URNO>%s</URNO>\n"
                            "<UAFT>%s</UAFT>\n"
                            "<ENAM>%s</ENAM>\n"
                            "<ACFR>%s</ACFR>\n"
                            "<ACTO>%s</ACTO>\n"
                            "</EQJOB>\n", rpJobrec.URNO, rpJobrec.UAFT, pclEquipName, rpJobrec.ACFR, rpJobrec.ACTO );

        if( ipPackType == TYPE_BULK )
            strcpy( pcpXmlStr, pclTmpStr );
        else
        {
            sprintf( pcpXmlStr, "<EQJOBS>\n"
                                "%s"
                                "</EQJOBS>\n", pclTmpStr );
        }
        if( isdigit(rpJobrec.URNO[0]) )
        {
            sprintf( pclTmpStr, "E%s", rpJobrec.URNO );
            StoreJob( pclTmpStr, rpJobrec.ACTO );
        }
        return RC_SUCCESS;
    }

    sprintf(pclXmlStr,"\n<JOB>\n"
                      "<URNO>%s</URNO>\n", rpJobrec.URNO );
    switch( ipPackType )
    {
        case( TYPE_INSERT ):
            strcpy(pclTmpStr,"<ACTION>INSERT</ACTION>\n" );
            strcat(pclXmlStr, pclTmpStr);
        case( TYPE_BULK ):
            if( strlen(rpJobrec.USTF) <= 0 )
            {
                strcpy( pclTmpStr, rpJobrec.URNO );
                if( !isdigit(rpJobrec.URNO[0]) )
                    strcpy( pclTmpStr, &rpJobrec.URNO[1] );
                sprintf(pclSqlBuf,"SELECT TRIM(USTF),TRIM(FCCO) FROM JOBTAB WHERE URNO = '%s'", pclTmpStr );
                dbg( TRACE, "<%s> SelSTFFromJOB <%s>", pclFunc, pclSqlBuf );
                ilRc = getOrPutDBData(pclSqlBuf,pclTmpStr,START);
                if( ilRc != DB_SUCCESS )
                {
                    dbg( TRACE, "<%s> No staff found for UJOB <%s>", pclFunc, rpJobrec.URNO );
                    return RC_FAIL;
                }
                get_fld(pclTmpStr,FIELD_1,STR,URNOLEN,rpJobrec.USTF);
                get_fld(pclTmpStr,FIELD_2,STR,FCCOLEN,rpJobrec.FCCO);
            }
            /* Get Staff Name from STFTAB */
            dbg(DEBUG,"<%s> New Search Staff Name STFU  = <%s>",pclFunc,rpJobrec.USTF); 
            GetStaffName( rpJobrec.USTF, pclUsid, pclFirstName, pclLastName, SRCH_BY_URNO );
            dbg(DEBUG,"<%s> New PENO <%s> FINM <%s> LANM <%s>", pclFunc, pclUsid, pclFirstName, pclLastName); 
      
            /* Get Employment Type base on FCCO from the itkaut.cfg */
            blGetEmpType = GetEmpType( pclEmpType, rpJobrec.FCCO );
            if( blGetEmpType == FALSE )
                strcpy(pclEmpType, " ");

            dbg(DEBUG,"<%s> New EMPTYPE <%s> blGetEmpType <%d>", pclFunc, pclEmpType, blGetEmpType ); 

            sprintf(pclTmpStr, "<PENO>%s</PENO>\n"
                               "<FINM>%s</FINM>\n"
                               "<LANM>%s</LANM>\n"
                               "<UAFT>%s</UAFT>\n"
                               "<FCCO>%s</FCCO>\n"
                               "<UTPL>%s</UTPL>\n"
                               "<EMPTYPE>%s</EMPTYPE>\n"
                               "</JOB>\n", pclUsid, pclFirstName, pclLastName,
                                           rpJobrec.UAFT, rpJobrec.FCCO, 
                                           rpJobrec.UTPL, pclEmpType );
            strcat( pclXmlStr, pclTmpStr );
            strcpy(pcpXmlStr, pclXmlStr );
            StoreJob( rpJobrec.URNO, rpJobrec.ACTO );
            return RC_SUCCESS;
        case( TYPE_DELETE ):
            strcpy(pclTmpStr,"<ACTION>DELETE</ACTION>\n</JOB>\n" );
            strcat(pclXmlStr, pclTmpStr );
            strcpy(pcpXmlStr, pclXmlStr );
            return RC_SUCCESS;

        case( TYPE_UPDATE ):
            strcpy(pclTmpStr,"<ACTION>UPDATE</ACTION>\n" );
            strcat(pclXmlStr, pclTmpStr);
    
            blUpdate = FALSE;
            if( ilStaff != -1 )
            {
                dbg(DEBUG,"<%s> Upd Pkt Search Staff Name STFU  = <%s>",pclFunc,rpJobrec.USTF); 
                GetStaffName( rpJobrec.USTF, pclUsid, pclFirstName, pclLastName, SRCH_BY_URNO );
                dbg(DEBUG,"<%s> Upd PENO <%s> FINM <%s> LANM <%s>", pclFunc, pclUsid, pclFirstName, pclLastName); 
                sprintf(pclTmpStr, "<PENO>%s</PENO>\n"
                                   "<FINM>%s</FINM>\n"
                                   "<LANM>%s</LANM>\n", pclUsid, pclFirstName, pclLastName );
                strcat(pclXmlStr, pclTmpStr);
                blUpdate = TRUE;
            }
            if( strncmp( rpJobrec.UAFT, "-1", 2 ) )
            {
                sprintf(pclTmpStr, "<UAFT>%s</UAFT>\n", rpJobrec.UAFT );
                strcat(pclXmlStr, pclTmpStr);
                blUpdate = TRUE;
            }
            if( strncmp( rpJobrec.FCCO, "-1", 2 ) )
            {
                blGetEmpType = GetEmpType( pclEmpType, rpJobrec.FCCO );
                if( blGetEmpType == FALSE )
                    strcpy(pclEmpType, " ");

                dbg(DEBUG,"<%s> New EMPTYPE <%s> blGetEmpType <%d>", pclFunc, pclEmpType, blGetEmpType ); 
                sprintf(pclTmpStr, "<FCCO>%s</FCCO>\n", rpJobrec.FCCO );
                strcat(pclXmlStr, pclTmpStr);
                blUpdate = TRUE;
                blFCCOUpd = TRUE;
            }
            if( strncmp( rpJobrec.UTPL, "-1", 2 ) )
            {
                sprintf(pclTmpStr, "<UTPL>%s</UTPL>\n", rpJobrec.UTPL );
                strcat(pclXmlStr, pclTmpStr);
                blUpdate = TRUE;
            }
            if( blFCCOUpd == TRUE )
            {
                sprintf(pclTmpStr, "<EMPTYPE>%s</EMPTYPE>\n", pclEmpType );
                strcat(pclXmlStr, pclTmpStr);
            }
            strcpy( pclTmpStr, "</JOB>\n" );
            strcat(pclXmlStr, pclTmpStr);
            if( blUpdate == TRUE )
            {
                strcpy( pcpXmlStr, pclXmlStr );
                if( strlen(rpJobrec.ACTO) > 0 )
                    StoreJob( rpJobrec.URNO, rpJobrec.ACTO );
                return RC_SUCCESS;
            }
            else
            {
                dbg( TRACE, "<%s> Nothing to update", pclFunc );
                return RC_FAIL;
            }

        default:
            return RC_FAIL;
    }
} /* PackJobXml */

/****** For Transit Flight *******/

int GetTransitFlight( AFTREC rlAftrec, AFTREC *rlTAftrec )
{
    int ilRc = RC_SUCCESS;
    char pclTmpSelection[DBQRLEN] = "\0";
    char pclDataArea[DBQRLEN] = "\0";
    char pclDemandType[10] = "\0";
    char *pclFunc = "GetTransitFlight";

    /* Ignore time frame */
    sprintf( pclTmpSelection, "SELECT %s FROM AFTTAB WHERE RKEY = %s AND URNO <> %s "
                              "AND FLNO = '%s' AND FTYP NOT IN (%s)", 
                              pcgDepartureFields, rlAftrec.RKEY, rlAftrec.URNO, rlAftrec.FLNO, pcgExcludeFtyp );
    dbg( TRACE, "%s: Transit SqlBuf = <%s>", pclFunc, pclTmpSelection );

    /* Check whether it is transit flight from JOBTAB */
    ilRc = getOrPutDBData(pclTmpSelection,pclDataArea,START);
    if( ilRc == DB_SUCCESS )
    {
         get_fld(pclDataArea,FIELD_1,STR,URNOLEN,rlTAftrec->URNO);
         dbg(DEBUG,"<%s> TURNO <%s> ---------",pclFunc, rlTAftrec->URNO);
         get_fld(pclDataArea,FIELD_2,STR,URNOLEN,rlTAftrec->RKEY);
         
         /* Since JOBTAB only contains departure jobs for turnaround flight */
         if( rlAftrec.ADID[0] == 'D' )
             sprintf( pclTmpSelection, "SELECT TRIM(DETY) FROM JOBTAB WHERE UAFT = '%s' AND " 
                                       "UTPL IN (%s)", rlAftrec.URNO, cgJobTemplates );
         else
             sprintf( pclTmpSelection, "SELECT TRIM(DETY) FROM JOBTAB WHERE UAFT = '%s' AND " 
                                       "UTPL IN (%s)", rlTAftrec->URNO, cgJobTemplates );

         dbg( TRACE, "%s: Dety1 SqlBuf = <%s>", pclFunc, pclTmpSelection );

         /* Check whether it is transit flight from JOBTAB */
         /* Transit flight has job demand type 0 */
         ilRc = getOrPutDBData(pclTmpSelection,pclDemandType,START);

         /* Those transit jobs seem to have arrival flight in JOBTAB when parking stand is assigned */
         /*if( ilRc == NOTFOUND )
         {
             if( rlAftrec.ADID[0] == 'D' )
                 sprintf( pclTmpSelection, "SELECT TRIM(DETY) FROM JOBTAB WHERE UAFT = '%s' AND " 
                                           "UTPL IN (%s)", rlTAftrec->URNO, cgJobTemplates );
             else
                 sprintf( pclTmpSelection, "SELECT TRIM(DETY) FROM JOBTAB WHERE UAFT = '%s' AND " 
                                           "UTPL IN (%s)", rlAftrec.URNO, cgJobTemplates );
             dbg( TRACE, "%s: Dety2 SqlBuf = <%s>", pclFunc, pclTmpSelection );
             ilRc = getOrPutDBData(pclTmpSelection,pclDemandType,START);
         }*/
         if( ilRc == DB_SUCCESS && pclDemandType[0] == '0' )
         {
             get_fld(pclDataArea,FIELD_3,STR,FLNOLEN,rlTAftrec->FLNO);  strip_special_char(rlTAftrec->FLNO);
             get_fld(pclDataArea,FIELD_4,STR,DATELEN,rlTAftrec->STOD);  UtcToLocalTimeFixTZ(rlTAftrec->STOD); 
             get_fld(pclDataArea,FIELD_5,STR,DATELEN,rlTAftrec->STOA);  UtcToLocalTimeFixTZ(rlTAftrec->STOA); 
             get_fld(pclDataArea,FIELD_6,STR,REGNLEN,rlTAftrec->REGN);  strip_special_char(rlTAftrec->REGN);
             get_fld(pclDataArea,FIELD_7,STR,COTYLEN,rlTAftrec->DES3);  strip_special_char(rlTAftrec->DES3);
             get_fld(pclDataArea,FIELD_8,STR,COTYLEN,rlTAftrec->ORG3);  strip_special_char(rlTAftrec->ORG3);
             get_fld(pclDataArea,FIELD_9,STR,DATELEN,rlTAftrec->ETDI);  UtcToLocalTimeFixTZ(rlTAftrec->ETDI); 
             get_fld(pclDataArea,FIELD_10,STR,DATELEN,rlTAftrec->ETAI); UtcToLocalTimeFixTZ(rlTAftrec->ETAI); 
             get_fld(pclDataArea,FIELD_11,STR,GATELEN,rlTAftrec->GTA1); strip_special_char(rlTAftrec->GTA1);
             get_fld(pclDataArea,FIELD_12,STR,GATELEN,rlTAftrec->GTD1); strip_special_char(rlTAftrec->GTD1);
             get_fld(pclDataArea,FIELD_13,STR,ADIDLEN,rlTAftrec->ADID); strip_special_char(rlTAftrec->ADID);
             get_fld(pclDataArea,FIELD_14,STR,PSTALEN,rlTAftrec->PSTA); strip_special_char(rlTAftrec->PSTA);
             get_fld(pclDataArea,FIELD_15,STR,PSTALEN,rlTAftrec->PSTD); strip_special_char(rlTAftrec->PSTD);
             get_fld(pclDataArea,FIELD_16,STR,BELTLEN,rlTAftrec->BLT1); strip_special_char(rlTAftrec->BLT1);
             get_fld(pclDataArea,FIELD_17,STR,DATELEN,rlTAftrec->OFBL); UtcToLocalTimeFixTZ(rlTAftrec->OFBL);
             get_fld(pclDataArea,FIELD_18,STR,DATELEN,rlTAftrec->ONBL); UtcToLocalTimeFixTZ(rlTAftrec->ONBL);
             get_fld(pclDataArea,FIELD_19,STR,TGADLEN,rlTAftrec->TGA1); strip_special_char(rlTAftrec->TGA1);
             get_fld(pclDataArea,FIELD_20,STR,TGADLEN,rlTAftrec->TGD1); strip_special_char(rlTAftrec->TGD1);
             get_fld(pclDataArea,FIELD_21,STR,TTYPLEN,rlTAftrec->TTYP); strip_special_char(rlTAftrec->TTYP);
             get_fld(pclDataArea,FIELD_22,STR,COTYLEN,rlTAftrec->ACT3); strip_special_char(rlTAftrec->ACT3);
             return RC_SUCCESS;
         }
     }
     memset(rlTAftrec, 0, sizeof(AFTREC));
     return RC_FAIL;
} /* GetTransitFlight */

int NewFlight( char *pcpUAFT, char *pcpTURN )
{
    int ilOrigNum;
    char *pclFunc = "NewFlight";

    if( pcpTURN !=  NULL )
        dbg( TRACE, "<%s> index = %d UAFT <%s> TURN <%s>", pclFunc, igNumFlights, pcpUAFT, pcpTURN );
    else
        dbg( TRACE, "<%s> index = %d UAFT <%s> TURN <NIL>", pclFunc, igNumFlights, pcpUAFT );

    ilOrigNum = igNumFlights;

    if( igNumFlights < MAX_FLIGHTS )
    {
        rgFltDat[igNumFlights].URNO = atoi(pcpUAFT);
        rgFltDat[igNumFlights].TURN = 0;
        if( pcpTURN != NULL )
            rgFltDat[igNumFlights].TURN = atoi(pcpTURN);
        igNumFlights++;
    }
    else
        dbg( TRACE, "<%s> Exceed max flts, ignore transit flight info", pclFunc );

    return ilOrigNum;
} /* NewFlight */

int ChkTransit( int ipUrno )
{
    int ili;
    char *pclFunc = "ChkTransit";

    dbg( TRACE, "<%s> Chk UAFT <%d>", pclFunc, ipUrno );

    for( ili = 0; ili < igNumFlights; ili++ )
    {
        if( rgFltDat[ili].URNO == ipUrno )
        {
            dbg( DEBUG, "<%s> UAFT <%d> -> TURN <%d>", pclFunc, ipUrno, rgFltDat[ili].TURN );
            break;
        }
    }
    dbg( TRACE, "<%s> NUM FLIGHTS <%d> return <%d>", pclFunc, igNumFlights, ili );
    return ili;
} /* ChkTransit */

int TrimUrno(char *pcpString)
{
    int ilLen;
    int ili;
    char *pclString;
    char *pclFunc = "TrimUrno";

    ilLen = strlen(pcpString);
    pclString = (char *)malloc( ilLen );

    for( ili = 0; ili < ilLen; ili++ )
    {
        if( !isdigit(pcpString[ili]) )
            break;
        pclString[ili] = pcpString[ili];
    }
    pclString[ili] = '\0';
    strcpy( pcpString, pclString );
    free( (char *)pclString );
} /* TrimUrno */

/****** For Mini-JOBTAB = ITKJOB *******/

int StoreJob( char *pcpUJOB, char *pcpACTO )
{
    int ilRc = RC_SUCCESS;
    char pclSqlBuf[DBQRLEN] = "\0";
    char pclSqlData[DBQRLEN] = "\0";
    char *pclFunc = "StoreJob";

    /* Dun need to insert turnaround job demand since it shares same job number */
    if( pcpUJOB[0] == 'A' )
        return;

    sprintf( pclSqlBuf, "SELECT ACTO FROM ITKJOB WHERE UJOB = '%s'", pcpUJOB );
    dbg( TRACE, "%s: SelJob SqlBuf = <%s>", pclFunc, pclSqlBuf );

    ilRc = getOrPutDBData(pclSqlBuf,pclSqlData,START);
    if( ilRc != DB_SUCCESS )
    {
        sprintf( pclSqlBuf, "INSERT INTO ITKJOB (UJOB,ACTO) VALUES ('%s','%s')", pcpUJOB, pcpACTO );
        dbg( TRACE, "%s: InsJob SqlBuf = <%s>", pclFunc, pclSqlBuf );
        
    }
    else
    {
        sprintf( pclSqlBuf, "UPDATE ITKJOB SET ACTO = '%s' WHERE UJOB = '%s'", pcpACTO, pcpUJOB );
        dbg( TRACE, "%s: UpdJob SqlBuf = <%s>", pclFunc, pclSqlBuf );
    }
    ilRc = getOrPutDBData(pclSqlBuf,pclSqlData,START);
    dbg( DEBUG, "ilRc = <%d>", ilRc );
    return;
} /* StoreJob */

int CleanJob( int ipNumdays )
{
    long llCleanEnd = 0;
    int ilRC = RC_SUCCESS;
    int ili = 0;
    short slSqlCursor = 0;
    char pclSqlBuf[200] = "\0";
    char pclData[40] = "\0";
    char pclDelTime[32] = "\0";
    char *pclFunc = "CleanJob";


    dbg( TRACE,"<%s> ----- START -----", pclFunc );

    FormatTime( time(NULL), pclDelTime, "UTC" );
    llCleanEnd = ipNumdays*SECONDS_PER_DAY*(-1);
    AddSecondsToCEDATime(pclDelTime,llCleanEnd,1);
    dbg(TRACE,"<%s> Delete Records older than <%s> from ITKJOB", pclFunc, pclDelTime );

 
    sprintf( pclSqlBuf,"DELETE FROM ITKJOB WHERE ACTO < '%s'", pclDelTime );
    ilRC = sql_if ( START, &slSqlCursor, pclSqlBuf, pclData );
    if (ilRC != RC_SUCCESS)
        dbg(TRACE,"<%s> sql_if failed or nothing to delete in ITKJOB", pclFunc );
    commit_work();
    close_my_cursor (&slSqlCursor);
} /* CleanJob */

/****** For DLS *******/

void getString( char *pcpInStr, char *pcpOutStr, char cgStopChr )
{
    int ili = 0;
    int ilLen = 0;

    ilLen = strlen( pcpInStr );
    for( ili = 0; ili < ilLen; ili++ )
    {
        if( pcpInStr[ili] == cgStopChr )
            break;
        pcpOutStr[ili] = pcpInStr[ili];
    }
    pcpOutStr[ili] = '\0';
} /* getString */

int getLine( char *pcpInStr, char *pcpOutStr, char cgStopChr, int ipInIdx, int ipNumLine )
{
    int ili, ilj;
    int ilLine;
    int ilLen;


    ilLen = strlen(pcpInStr);

    if( ipInIdx < 0 || ipInIdx >= ilLen )
        return END_TELEX;

    ilLine = 0;
    ilj = 0;
    for( ili = ipInIdx; ili < ilLen, ilLine < ipNumLine; ili++ )
    {
        if( pcpInStr[ili] == cgStopChr )
        {
            ilLine++;
            pcpOutStr[ilj] = '\0';
            ilj = 0;
            if( strlen(pcpOutStr) <= 0 )
                ilLine--;
            if( ilLine >= ipNumLine )
                break;
        }
        else
           pcpOutStr[ilj++] = pcpInStr[ili];
    }
    return (ili+1);
} /* getLine */

int getField( char *pcpInStr, char *pcpOutStr, char cgStopChr, int ipNumField )
{
    int ili, ilj;
    int ilField;
    int ilLen;

    ilLen = strlen(pcpInStr);

    ilField = 0;
    ilj = 0;
    for( ili = 0; ili < ilLen, ilField < ipNumField; ili++ )
    {
        if( pcpInStr[ili] == cgStopChr )
        {
            ilField++;
            pcpOutStr[ilj] = '\0';
            ilj = 0;
            if( ilField >= ipNumField )
                break;
        }
        else
           pcpOutStr[ilj++] = pcpInStr[ili];
    }
    return (ili+1);
} /* getField */

void cutString( char *pcpInStr, char *pcpOutStr, int ipMaxChrPerRow )
{
    int ili;
    int ilLen;
    char pclTmpStr[1000] = "\0";
    char *pclFunc = "cutString";

    TrimSpace( pcpInStr );
    dbg( DEBUG, "<%s> InStr (%s)", pclFunc, pcpInStr );
    ilLen = strlen( pcpInStr );
    memset( pcpOutStr, 0, strlen(pcpOutStr) );
    for( ili = 0; ili < ilLen; ili+=ipMaxChrPerRow )
    {
        memset( pclTmpStr, 0, sizeof( pclTmpStr ) );
        strncpy( pclTmpStr, &pcpInStr[ili], ipMaxChrPerRow );
        strcat( pclTmpStr, pcgBreakTag );
        strcat( pcpOutStr, pclTmpStr );       
    }
    strcat( pcpOutStr, "\0");
    dbg( DEBUG, "<%s> OutStr (%s)", pclFunc, pcpOutStr );
} /* cutString */

void printTelex( char *pcpTelex )
{
    int i, j;
    int ilLen;
    char p[5120] = "\0";

    ilLen = strlen(pcpTelex);

    dbg( TRACE, "\nPrinting Telex......" );

    j = 0;
    p[j++] = '\n';
    for( i = 0; i < ilLen; i++ )
    {
        if( pcpTelex[i] == cgDLSDelimitter )
            p[j++] = '\n';
        else
            p[j++] = pcpTelex[i];
    }
    p[j] = '\0';
    dbg( TRACE, "(%s)\n", p );
    dbg( TRACE, "Done" );

} /* printTelex */


void FormatDLS ( char *pcpTxt1, char *pcpFinalTxt )
{
    int ili = 0, ilj = 0, ilk = 0, ilt = 0;
    int ilLen;
    int ilDLSLen;
    int ilMaxChrPerRow = 24;
    char pclTxt1[4004] = "\0";
    char pclSectorInfo[8120] = "\0";        /* Include html break */
    char pclSqlBuf[1024] = "\0";
    char pclSqlData[1024] = "\0";
    char pclTmpStr[1024] = "\0";
    char pclTmpStr2[1024] = "\0";
    char pclOneLine[1024] = "\0";
    char pclHeader[1024] = "\0";
    char pclSummaryHdr1[20] = "\0";
    char pclSummaryHdr2[20] = "\0";
    char pclFinalTxt[10000];
    char *pclFunc = "FormatDLS";

    printf( "<%s> --------- START ----------", pclFunc );

    ilDLSLen = strlen(pcpTxt1);
    if( ilDLSLen <= 0 )
        return;

    strcpy( pclTxt1, pcpTxt1 );

    ilt = 0;
    ilt = getLine( pclTxt1, pclOneLine, cgDLSDelimitter, ilt, 6 );
    dbg( DEBUG, "%d. Line = (%s)", __LINE__, pclOneLine );

    getString( pclOneLine, pclTmpStr, '/' );

    /*sprintf( pclHeader, "%s-D-%4.4s-%s"
                        "%s"
                        "SHOW DLS%s", pclTmpStr, &pcpTifd[8], pcpGtd1, pcgBreakTag,
                                      pcgBreakTag );
    dbg( DEBUG, "%d. Header = (%s)", __LINE__, pclHeader ); */

    /* Skip 1st word */
    ili = 0;
    while( pclOneLine[ili] != ' ' )
        ili++;
    ili++;

    strcpy( pclTmpStr, &pclOneLine[ili] );
    cutString( pclTmpStr, pclTmpStr2, ilMaxChrPerRow );
    strcpy( pclHeader, pclTmpStr2 );        /* container remarks */
    dbg( DEBUG, "%d. Header = (%s)", __LINE__, pclHeader );

    ilt = getLine( pclTxt1, pclOneLine, cgDLSDelimitter, ilt, 1 );
    dbg( DEBUG, "%d. Line = (%s)", __LINE__, pclOneLine );

    /* Sector Information */
    while( ilt != END_TELEX && strncmp( pclOneLine, "OSI", 3 ) &&
           strncmp( pclOneLine, "SUMMARY", 7 ) && strncmp( pclOneLine, "TOTAL", 5 ) )
    {
        if( pclOneLine[0] == '-' )
        {
            cutString( pclOneLine, pclTmpStr, ilMaxChrPerRow );
            strcat( pclSectorInfo, pclTmpStr );
            dbg( DEBUG, "%d. Sector = (%s)", __LINE__, pclSectorInfo );

            ilt = getLine( pclTxt1, pclOneLine, cgDLSDelimitter, ilt, 1 );
            TrimSpace( pclOneLine );
            dbg( DEBUG, "%d. Line = (%s)", __LINE__, pclOneLine );

            if( !strncmp( pclOneLine, "OSI", 3 ) )
                break;

            sprintf( pclTmpStr, "%s%s", pclOneLine, pcgBreakTag );
            strcat( pclSectorInfo, pclTmpStr );
            dbg( DEBUG, "%d. Sector = (%s)", __LINE__, pclSectorInfo );

            ilt = getLine( pclTxt1, pclOneLine, cgDLSDelimitter, ilt, 1 );
            dbg( DEBUG, "%d. Line = (%s)", __LINE__, pclOneLine );
        }
        else
        {
            if( strncmp( pclOneLine, "   ", 3 ) )
            {
                strncpy( pclTmpStr, pclOneLine, 22 );
                pclTmpStr[22] = '\0';

                if( !strncmp( &pclOneLine[5], "BULK", 4 ) )
                {
                    strncat( pclTmpStr, &pclOneLine[30], 7 );
                    pclTmpStr[29] = '\0';
                }
                TrimSpace( pclTmpStr );
                strcat( pclTmpStr, pcgBreakTag );           /* container id */
                strcat( pclSectorInfo, pclTmpStr );
                dbg( DEBUG, "%d. Sector = (%s)", __LINE__, pclSectorInfo );

                memset( pclTmpStr, 0, sizeof( pclTmpStr ) );
                strncpy( pclTmpStr, &pclOneLine[40], 10 );
                TrimSpace( pclTmpStr );

                ilt = getLine( pclTxt1, pclOneLine, cgDLSDelimitter, ilt, 1 );
                dbg( DEBUG, "%d. Line = (%s)", __LINE__, pclOneLine );

                if( strncmp( pclOneLine, "   ", 3 ) )
                {
                    cutString( pclTmpStr, pclTmpStr2, ilMaxChrPerRow );
                    strcat( pclSectorInfo, pclTmpStr2 );        /* container remarks */
                    dbg( DEBUG, "%d. Sector = (%s)", __LINE__, pclSectorInfo );
                }
            }
            else
            {
                strcat( pclTmpStr, pclOneLine );
                TrimSpace( pclTmpStr );
                strcat( pclTmpStr, pcgBreakTag );
                strcat( pclSectorInfo, pclTmpStr );
                dbg( DEBUG, "%d. Sector = (%s)", __LINE__, pclSectorInfo );
                ilt = getLine( pclTxt1, pclOneLine, cgDLSDelimitter, ilt, 1 );
                dbg( DEBUG, "%d. Line = (%s)", __LINE__, pclOneLine );
            }
        }
    } /* while !OSI */

    while( ilt!= END_TELEX && strncmp( pclOneLine, "SUMMARY", 7 ) && strncmp( pclOneLine, "TOTAL", 5 )  )
    {
        cutString( pclOneLine, pclTmpStr2, ilMaxChrPerRow );
        strcat( pclSectorInfo, pclTmpStr2 );        
        dbg( DEBUG, "%d. Sector = (%s)", __LINE__, pclSectorInfo );
        ilt = getLine( pclTxt1, pclOneLine, cgDLSDelimitter, ilt, 1 );
        dbg( DEBUG, "%d. Line = (%s)", __LINE__, pclOneLine );
    } /* summary while */

    if( !strncmp( pclOneLine, "SUMMARY", 7 ) )
    {
        /* Skip SUMMARY heading and 1st 2 fields*/
        ilt = getLine( pclTxt1, pclOneLine, cgDLSDelimitter, ilt, 1 );
        dbg( DEBUG, "%d. Line = (%s)", __LINE__, pclOneLine );
        TrimSpace( pclOneLine );

        getField( pclOneLine, pclSummaryHdr1, ' ', 3 ); strcat( pclSummaryHdr1, ": " );
        getField( pclOneLine, pclSummaryHdr2, ' ', 4 ); strcat( pclSummaryHdr2, ": " );
        dbg( DEBUG, "SummaryHdr1 <%s> SummaryHdr2 <%s>", pclSummaryHdr1, pclSummaryHdr2 );

        ilt = getLine( pclTxt1, pclOneLine, cgDLSDelimitter, ilt, 1 );
        dbg( DEBUG, "%d. Line = (%s)", __LINE__, pclOneLine );

        ilt = getLine( pclTxt1, pclOneLine, cgDLSDelimitter, ilt, 1 );
        TrimSpace( pclOneLine );
        dbg( DEBUG, "%d. Line = (%s)", __LINE__, pclOneLine );

        while( ilt != END_TELEX && strncmp( pclOneLine, "TOTAL", 5 )  )
        {
            getField( pclOneLine, pclTmpStr, ' ', 1 );
            getField( pclOneLine, pclTmpStr2, ' ', 2 );

            strcat( pclTmpStr, " " );
            strcat( pclTmpStr, pclTmpStr2 );
            strcat( pclTmpStr, " " );
            strcat( pclTmpStr, pclSummaryHdr1 );
            getField( pclOneLine, pclTmpStr2, ' ', 5 );
            strcat( pclTmpStr, pclTmpStr2 );

            strcat( pclTmpStr, " " );
            strcat( pclTmpStr, pclSummaryHdr2 );
            getField( pclOneLine, pclTmpStr2, ' ', 6 );
            strcat( pclTmpStr, pclTmpStr2 );

            cutString( pclTmpStr, pclTmpStr2, ilMaxChrPerRow );
            strcat( pclHeader, pclTmpStr2 );      
            dbg( DEBUG, "%d. Header = (%s)", __LINE__, pclSectorInfo );

            ilt = getLine( pclTxt1, pclOneLine, cgDLSDelimitter, ilt, 1 );
            dbg( DEBUG, "%d. Line = (%s)", __LINE__, pclOneLine );
            TrimSpace( pclOneLine );
        }
    }

    if( ilt != END_TELEX )
    {
        strcat( pclSectorInfo, "-" );
        strcat( pclSectorInfo, pcgBreakTag );
        dbg( DEBUG, "%d. Sector = (%s)\n", __LINE__, pclSectorInfo );
    }

    while( ilt != END_TELEX )
    {
        ilt = getLine( pclTxt1, pclOneLine, cgDLSDelimitter, ilt, 1 );
        if( ilt == END_TELEX )
            break;
        dbg( DEBUG, "%d. Line = (%s)", __LINE__, pclOneLine );

        cutString( pclOneLine, pclTmpStr2, ilMaxChrPerRow );
        strcat( pclSectorInfo, pclTmpStr2 );       
        dbg( DEBUG, "%d. Sector = (%s)", __LINE__, pclSectorInfo );
    }

    dbg( DEBUG, "Header = (%s)", pclHeader );
    dbg( DEBUG, "Sector = (%s)", pclSectorInfo );
    strcpy( pcpFinalTxt, pclHeader );
    strcat( pcpFinalTxt, pclSectorInfo );
    dbg( DEBUG, "Final = (%s)", pcpFinalTxt );

    dbg( TRACE, "<%s> --------- END ----------", pclFunc );
} /* FormatDLS */

/** For Flight message recipent **/
void GetFlight( char *pcpUaft, char *pcpFname, char *pcpLname )
{
    AFTREC rlAftrec;
    int ilRc;
    int ilMonth;
    char pclSqlBuf[512];
    char pclSqlData[512];
    char pclTmpStr[40];
    char *pclFunc = "GetFlight";

    dbg( TRACE, "<%s> --------- START ----------", pclFunc );
    memset( &rlAftrec, 0, sizeof(AFTREC) );
    sprintf( pclSqlBuf, "SELECT TRIM(FLNO), TRIM(ADID), TRIM(FLDA) FROM AFTTAB WHERE URNO = %s", pcpUaft);
    dbg( TRACE, "<%s> SelTOID SqlBuf <%s>", pclFunc, pclSqlBuf );
    ilRc = getOrPutDBData(pclSqlBuf,pclSqlData,START);
    if( ilRc == DB_SUCCESS )
    {
        get_fld(pclSqlData,FIELD_1,STR,FLNOLEN,rlAftrec.FLNO); strip_special_char(rlAftrec.FLNO);
        get_fld(pclSqlData,FIELD_2,STR,ADIDLEN,rlAftrec.ADID);
        get_fld(pclSqlData,FIELD_3,STR,DATELEN,rlAftrec.FLDA);

        dbg( DEBUG, "<%s> FLNO <%s> ADID <%c> FLDA <%s> ", pclFunc, rlAftrec.FLNO, rlAftrec.ADID[0], rlAftrec.FLDA );

        sprintf( pclTmpStr, "%2.2s", &rlAftrec.FLDA[4] );
        ilMonth = atoi(pclTmpStr);
        memset( pclTmpStr, 0, sizeof(pclTmpStr) );
        if( ilMonth >= 1 && ilMonth <= 12 )
            sprintf( pclTmpStr, "%3.3s", pcgMonths[ilMonth-1] );
        sprintf( pcpFname, "%s-%2.2s/%s/%4.4s-%c", rlAftrec.FLNO, &rlAftrec.FLDA[6], 
                                               pclTmpStr, rlAftrec.FLDA, rlAftrec.ADID[0] );
        sprintf( pcpLname, "%s", rlAftrec.FLNO );
        dbg( TRACE, "<%s> FNAME <%s>", pclFunc, pcpFname );
    }
    dbg( TRACE, "<%s> --------- END ----------", pclFunc );
}

void SendFlightTURN ( char *pcpUAFT, char *pcpTURN )
{
    int ilRc;
    int ilIdxF1, ilIdxF2;
    int ilUaft = 0;
    int ilTurn = 0;
    int ilOldTURN = 0;
    BOOL blTransit;
    BOOL blSendF1, blSendF2;
    char pclUaft[20] = "\0";
    char pclTurn[20] = "\0";
    char pclTmpStr[100];
    char *pclFunc = "SendFlightTURN";

    blSendF1 = blSendF2 = FALSE;
    blTransit = FALSE;
    
    ilUaft = atoi(pcpUAFT);
    if( ilUaft <= 0 )
        return;
    strcpy( pclUaft, pcpUAFT );
    blTransit = FALSE;
    ilTurn = 0;
    if( pcpTURN != NULL )
    {
        strcpy( pclTurn, pcpTURN );
        blTransit = TRUE;
        ilTurn = atoi(pcpTURN);
    }

    /* This function will send corresponding TURN */
    if( blTransit == TRUE )
        dbg( TRACE, "<%s> UAFT received <%s> TURN <%s>", pclFunc, pcpUAFT, pcpTURN );
    else
        dbg( TRACE, "<%s> UAFT received <%s> TURN - NIL", pclFunc, pcpUAFT );

    ilIdxF1 = ChkTransit(ilUaft);
    /* F1 not found */
    if( ilIdxF1 >= igNumFlights && igNumFlights < MAX_FLIGHTS )
    {
        ilIdxF1 = NewFlight( pcpUAFT, pcpTURN );
        blSendF1 = TRUE;
    }

    if( ilIdxF1 >= 0 && ilIdxF1 < igNumFlights && rgFltDat[ilIdxF1].TURN != ilTurn )
    {
        ilOldTURN = rgFltDat[ilIdxF1].TURN;
        rgFltDat[ilIdxF1].TURN = ilTurn;
        blSendF1 = TRUE;
    }
    if( ilIdxF1 >= 0 && ilIdxF1 < igNumFlights )
        dbg( TRACE, "<%s> F1[%d] -> UAFT <%d> TURN <%d>", pclFunc, ilIdxF1, 
                                    rgFltDat[ilIdxF1].URNO, rgFltDat[ilIdxF1].TURN  );

    if( blTransit == TRUE )
    {
        ilIdxF2 = ChkTransit(ilTurn);
        /* F2 not found */
        if( ilIdxF2 >= igNumFlights && igNumFlights < MAX_FLIGHTS )
        {
            ilIdxF2 = NewFlight( pcpTURN, pcpUAFT );
            blSendF2 = TRUE;
        }
        if( ilIdxF2 >= 0 && ilIdxF2 < igNumFlights && rgFltDat[ilIdxF2].TURN != ilUaft )
        {
            rgFltDat[ilIdxF2].TURN = ilUaft;
            blSendF2 = TRUE;
        }
    }
    else
    {
        if( ilOldTURN != 0 )
        {
            ilIdxF2 = ChkTransit(ilOldTURN);
            sprintf( pclTurn, "%d", ilOldTURN );
            /* No F2 found */
            if( ilIdxF2 >= igNumFlights && igNumFlights < MAX_FLIGHTS )
            {
                ilIdxF2 = NewFlight( pclTurn, NULL );
                blSendF2 = TRUE;
            }
            if( ilIdxF2 >= 0 && ilIdxF2 < igNumFlights && rgFltDat[ilIdxF2].TURN != 0 )
            {
                rgFltDat[ilIdxF2].TURN = 0;
                blSendF2 = TRUE;
            }
        }
    }
    if( ilIdxF2 >= 0 && ilIdxF2 < igNumFlights )
        dbg( TRACE, "<%s> F2[%d] -> UAFT <%d> TURN <%d>", pclFunc, ilIdxF2, 
                                    rgFltDat[ilIdxF2].URNO, rgFltDat[ilIdxF2].TURN  );

    memset( pclTmpStr, 0, sizeof(pclTmpStr) );
    if( blSendF1 == TRUE )
    {
        if( blTransit == TRUE )
            strcpy( pclTmpStr, pclTurn );
        sprintf( pcgXmlStr, "<FLIGHT>\n"
                            "<URNO>%s</URNO>\n"
                            "<ACTION>UPDATE</ACTION>\n"
                            "<TURN>%s</TURN>\n"
                            "</FLIGHT>\n", pclUaft, pclTmpStr );
        ilRc = SendCedaEvent(igITKWMA,0,mod_name,"","","","ITK","","","",
                             pcgXmlStr,"",atoi(cgSendPrio),RC_SUCCESS);
        dbg(TRACE, "<%s> F1-F2 Xml <%s>", pclFunc, pcgXmlStr );
    }
    if( blSendF2 == TRUE )
    {
        if( blTransit == TRUE )
            strcpy( pclTmpStr, pclUaft );
        sprintf( pcgXmlStr, "<FLIGHT>\n"
                            "<URNO>%s</URNO>\n"
                            "<ACTION>UPDATE</ACTION>\n"
                            "<TURN>%s</TURN>\n"
                            "</FLIGHT>\n", pclTurn, pclTmpStr );
        ilRc = SendCedaEvent(igITKWMA,0,mod_name,"","","","ITK","","","",
                             pcgXmlStr,"",atoi(cgSendPrio),RC_SUCCESS);
        dbg(TRACE, "<%s> F2-F1 Xml <%s>", pclFunc, pcgXmlStr );
    }
} /* SendFlightTURN */


void GetHandledAirlines ()
{
    int ilRc;
    int ili;
    int ilCount;
    short slSqlCursor;
    short slSqlFunc;
    char pclSqlBuf[512];
    char pclTmpStr[512];
    char *pclToken;
    char *pclFunc = "GetHandledAirlines";

    dbg( TRACE, "<%s> --------- START ----------", pclFunc );

    sprintf( pclSqlBuf, "SELECT TRIM(ALC3) FROM ALTTAB WHERE URNO IN "
                        "(SELECT DISTINCT(ALTU) FROM HAITAB WHERE HSNA IN (%s)) AND ALC3 <> ' '", pcgReqdAgent );
    dbg( TRACE, "<%s> GetALC3 SqlBuf <%s>", pclFunc, pclSqlBuf );

    memset( &rgDbReturn, 0, sizeof(rgDbReturn) );
    rgDbReturn.RecSep = ';';
    slSqlCursor = 0;
    slSqlFunc = START;
    ilCount = 0;
    memset( pcgHandledAirlines, 0, sizeof(pcgHandledAirlines) );

    while( (ilRc = SqlIfArray( slSqlFunc, &slSqlCursor, pclSqlBuf, 0, 1000, &rgDbReturn )) >= 0 )
    {
        if( rgDbReturn.LineCount <= 0 )
                break;

        slSqlFunc = NEXT;
        if( rgDbReturn.AppendBuf == TRUE )
            strcat( pcgHandledAirlines, ";" );
        strcat( pcgHandledAirlines, rgDbReturn.Value );
        ilCount += rgDbReturn.LineCount;

        if( rgDbReturn.LineCount < 1000 )
            break;
    }
    strcat( pcgHandledAirlines, ";" );
    close_my_cursor(&slSqlCursor);
    dbg (TRACE, "<%s> pcgHandledAirlines (%s)", pclFunc, pcgHandledAirlines );

    dbg( TRACE, "<%s> --------- END ----------", pclFunc );

} /*GetHandledAirlines*/

int ReadFlightRecord( char *pcpUaft, AFTREC *prpAftrec )
{
    int ilRc;
    short slSqlCursor;
    short slSqlFunc;
    char pclSqlBuf[1024];
    char pclSqlData[5120];
    char *pclFunc = "ReadFlightRecord";

    memset( prpAftrec, 0, sizeof(AFTREC) );

    sprintf( pclSqlBuf, "SELECT %s FROM AFTTAB WHERE URNO = %s", pcgDepartureFields, pcpUaft );
    dbg( TRACE, "%s: SqlBuf = <%s>", pclFunc, pclSqlBuf );

    /* Check whether it is transit flight from JOBTAB */
    ilRc = getOrPutDBData(pclSqlBuf,pclSqlData,START);
    if( ilRc == DB_SUCCESS )
    {
         get_fld(pclSqlData,FIELD_1,STR,URNOLEN,prpAftrec->URNO);
         get_fld(pclSqlData,FIELD_2,STR,URNOLEN,prpAftrec->RKEY);
         get_fld(pclSqlData,FIELD_3,STR,FLNOLEN,prpAftrec->FLNO);  strip_special_char(prpAftrec->FLNO);
         get_fld(pclSqlData,FIELD_4,STR,DATELEN,prpAftrec->STOD);  UtcToLocalTimeFixTZ(prpAftrec->STOD); 
         get_fld(pclSqlData,FIELD_5,STR,DATELEN,prpAftrec->STOA);  UtcToLocalTimeFixTZ(prpAftrec->STOA); 
         get_fld(pclSqlData,FIELD_6,STR,REGNLEN,prpAftrec->REGN);  strip_special_char(prpAftrec->REGN);
         get_fld(pclSqlData,FIELD_7,STR,COTYLEN,prpAftrec->DES3);  strip_special_char(prpAftrec->DES3);
         get_fld(pclSqlData,FIELD_8,STR,COTYLEN,prpAftrec->ORG3);  strip_special_char(prpAftrec->ORG3);
         get_fld(pclSqlData,FIELD_9,STR,DATELEN,prpAftrec->ETDI);  UtcToLocalTimeFixTZ(prpAftrec->ETDI); 
         get_fld(pclSqlData,FIELD_10,STR,DATELEN,prpAftrec->ETAI); UtcToLocalTimeFixTZ(prpAftrec->ETAI); 
         get_fld(pclSqlData,FIELD_11,STR,GATELEN,prpAftrec->GTA1); strip_special_char(prpAftrec->GTA1);
         get_fld(pclSqlData,FIELD_12,STR,GATELEN,prpAftrec->GTD1); strip_special_char(prpAftrec->GTD1);
         get_fld(pclSqlData,FIELD_13,STR,ADIDLEN,prpAftrec->ADID); strip_special_char(prpAftrec->ADID);
         get_fld(pclSqlData,FIELD_14,STR,PSTALEN,prpAftrec->PSTA); strip_special_char(prpAftrec->PSTA);
         get_fld(pclSqlData,FIELD_15,STR,PSTALEN,prpAftrec->PSTD); strip_special_char(prpAftrec->PSTD);
         get_fld(pclSqlData,FIELD_16,STR,BELTLEN,prpAftrec->BLT1); strip_special_char(prpAftrec->BLT1);
         get_fld(pclSqlData,FIELD_17,STR,DATELEN,prpAftrec->OFBL); UtcToLocalTimeFixTZ(prpAftrec->OFBL);
         get_fld(pclSqlData,FIELD_18,STR,DATELEN,prpAftrec->ONBL); UtcToLocalTimeFixTZ(prpAftrec->ONBL);
         get_fld(pclSqlData,FIELD_19,STR,TGADLEN,prpAftrec->TGA1); strip_special_char(prpAftrec->TGA1);
         get_fld(pclSqlData,FIELD_20,STR,TGADLEN,prpAftrec->TGD1); strip_special_char(prpAftrec->TGD1);
         get_fld(pclSqlData,FIELD_21,STR,TTYPLEN,prpAftrec->TTYP); strip_special_char(prpAftrec->TTYP);
         get_fld(pclSqlData,FIELD_22,STR,COTYLEN,prpAftrec->ACT3); strip_special_char(prpAftrec->ACT3);
         get_fld(pclSqlData,FIELD_23,STR,FTYPLEN,prpAftrec->FTYP); strip_special_char(prpAftrec->FTYP);
     }
     return ilRc;

} /* ReadFlightRecord */

void TrackEvent(char *fmt, ...)
{
    struct tm rlTm;
    static int ilWday = -1;
    va_list args;
    time_t tlNow;
    char pclFMode[4];
    char pclFname[256];
    char pclLog[2048];

    if( bgTrackEvent == FALSE )
        return;

    tlNow = time(0);
    localtime_r( &tlNow, &rlTm ); 
    memset( pclLog, 0, sizeof(pclLog) );
    sprintf( pclLog, "%2.2d-%2.2d-%2.2d %2.2d:%2.2d:%2.2d ", rlTm.tm_mday, rlTm.tm_mon+1, rlTm.tm_year%100,
                                                             rlTm.tm_hour, rlTm.tm_min, rlTm.tm_sec );

    strcpy( pclFMode, "w" );
    if( rlTm.tm_wday == ilWday )
        strcpy( pclFMode, "a" );

    ilWday = rlTm.tm_wday;
    sprintf( pclFname, "%s/itkhdl_%2.2d_event.log", getenv("DBG_PATH"), ilWday );
    prgTrackEventFile = fopen( pclFname, pclFMode ); 

    va_start(args, fmt);
    strcat( pclLog, fmt );
    strcat( pclLog, "\n" );
    vfprintf( prgTrackEventFile, pclLog, args ); 
    fflush( prgTrackEventFile );
    va_end(args);                        
    fclose( prgTrackEventFile );

} /* TrackEvent */


/*********************************************************************************/
/* v2.0 - To forward DLS msg to iTrek                                            */
/*********************************************************************************/
static void HandleOnlineDLS(char *pcpSelection)
{
    int ilRc;
    int ilUtlx, ilUaft;
    char pclSelection[512];
    char pclData[128] = "\0";
    char *pclFunc = "HandleOnlineDLS";
    char *pclToken;

    dbg( TRACE, "<%s> =========== START =============", pclFunc );
    strcpy( pclSelection, pcpSelection );

    pclToken = (char *)strtok( pclSelection, "=" );
    pclToken = (char *)strtok( NULL, "," );
    ilUaft = atoi( pclToken );

    dbg( TRACE, "<%s> UAFT <%d>", pclFunc, ilUaft );

    sprintf( pclData, "\n<DLSS>\n<DLS>\n"
                      "<FLNU>%d</FLNU>\n"
                      "</DLS>\n</DLSS>\n", ilUaft );
    ilRc = SendCedaEvent(igITKCPM,0, " ", " ", " ", " ","ITK", " ", " "," ", pclData, "", 3, 0) ;
    dbg (TRACE, "<%s> ilRc for SendCedaEvent = %d", pclFunc, ilRc );
    dbg (TRACE, "<%s> pcgXmlStr (%s)", pclFunc, pclData );

    dbg( TRACE, "<%s> =========== END =============", pclFunc );
} /* HandleOnlineDLS */
