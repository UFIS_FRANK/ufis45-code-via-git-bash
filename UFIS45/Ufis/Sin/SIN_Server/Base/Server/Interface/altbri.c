#ifndef _DEF_mks_version
#define _DEF_mks_version
#include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Sin/SIN_Server/Base/Server/Interface/altbri.c 1.1 2013/04/01 02:06:21CEST ble $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :                                                           */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history :                                                           */
/* 2013-04-01 MEI: Race condition when both ALTEA and KRISCOM returned result at same time */
/*                 Intentional delay to process Kriscom msg first (v1.1)      */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */

static char sccs_version[] ="@(#) UFIS44 (c) ABB AAT/I skeleton.c 44.1.0 / 11.12.2002 HEB";

/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */


#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "fditools.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "rmschkrul.h"
#include "syslib.h"
#include "db_if.h"
#include "urno_fn.inc"
#include <time.h>
#include <cedatime.h>

#ifdef _HPUX_SOURCE
extern int daylight;
extern long timezone;
extern char *tzname[2];
#else
extern int _daylight;
extern long _timezone;
extern char *_tzname[2];
#endif

int debug_level = DEBUG;
#define RC_NODATA -129
#define DATABLK_SIZE (256*1024)
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static EVENT *prgOutEvent  = NULL;
static int igStartUpMode = TRACE;
static int igRuntimeMode = 0;
static int igSendQueue = 0;


static char cgProcessName[20] = "\0";
static char  cgConfigFile[512] = "\0";
static char  cgHopo[8] = "\0";                         /* default home airport    */
static char  cgTabEnd[8] ="\0";                       /* default table extension */
static char cgService[124] = "3521";
static int igService = 3521;
static char cgJavaLogFileName[1024];
static char cgJRE[1024] = "\0";
static char cgJREPara[1024] = "\0";
static char cgJavaProgram[1024] = "\0";
static char cgJavaScript[1024] = "\0";
static char cgJavaShell[1024] = "\0";
static int igTcpTimeout = 10;
static int igConnectTimeout = 20;
static int igCheckTime = 20;
static time_t tgLastCheck = 0;
static int igMaxBytes = 8192;

static long lgEvtCnt = 0;
static int  igStartSocket = -1;
static int  igWorkSocket = -1;

static char  pcgSqlBuf[12 * 1024];
static char  pcgDataArea[DATABLK_SIZE];

/* MEI v1.1 */
static int  igResponseDelay;

/******************************************************************************/
/* External functions                                                         */
/******************************************************************************/
extern int SendCedaEvent(int,int,char*,char*,char*,char*,char*,char*,char*,char*,char*,char*,int,int);
extern int nap(unsigned long);
extern int tcp_create_socket (int type, char *Pservice);
extern int tcp_open_connection (int socket, char *Pservice, char *Phostname);

/******************************************************************************/
/* Function prototypes                                                      */
/******************************************************************************/
static int  Init_Process();
static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,
   char *pcpCfgBuffer);
static int ReadConfigRow(char *pcpSection,char *pcpKeyword,
 char *pcpCfgBuffer);
static char *GetKeyItem(char *pcpResultBuff, long *plpResultSize, 
char *pcpTextBuff, char *pcpKeyWord, char *pcpItemEnd, int bpCopyData);
static int GetDebugLevel(char *pcpMode, int *pipMode);
static void ForkChild();
int TcpCreateSocket(int type, short spService);
static char *GetCommandFromSocket(char *pcpCommand,char **pcpData, char *pcpTimeOutCmd);
static int PutCommandOnSocket(char *pcpCommand,char *pcpData, char *pcpFields, char *pcpSelection, char *pcpTwEnd);


static int Reset(void);                       /* Reset program          */
static void Terminate(int ipSleep);            /* Terminate program      */
static void HandleSignal(int);                 /* Handles signals        */
static void HandleErr(int);                    /* Handles general errors */
static void   HandleQueErr(int);                 /* Handles queuing errors */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/

static int HandleData(EVENT *prpEvent);       /* Handles event data     */
static int  GetCommand(char *pcpCommand, int *pipCmd);
int getOrPutDBData(uint ipMode);

static void SocketDataToWERTAB(char *UAFT, char *TCPData, char *HOPO,char *USEC);
/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
    int ilRc = RC_SUCCESS;/* Return code*/
    int ilCnt = 0;
    int ilOldDebugLevel = 0;
    char clAlert[200] = "\0";

    INITIALIZE;/* General initialization*/


    debug_level = TRACE|DEBUG;

    strcpy(cgProcessName,argv[0]);

    dbg(TRACE,"MAIN: version <%s>",sccs_version);

    /* Attach to the MIKE queues */
    do {
    ilRc = init_que();
    if(ilRc != RC_SUCCESS)
    {
      dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
      sleep(6);
      ilCnt++;
      }/* end of if */
    } while((ilCnt < 10) && (ilRc != RC_SUCCESS));

    if(ilRc != RC_SUCCESS)
    {
dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
sleep(60);
exit(1);
    }else{
dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
    }/* end of if */

    do {
ilRc = init_db();
if (ilRc != RC_SUCCESS)
{
    check_ret(ilRc);
    dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
    sleep(6);
    ilCnt++;
} /* end of if */
    } while((ilCnt < 10) && (ilRc != RC_SUCCESS));

    if(ilRc != RC_SUCCESS)
    {
dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
sleep(60);
exit(2);
    }else{
dbg(TRACE,"MAIN: init_db() OK!");
    } /* end of if */

    /* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */

    sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
    dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
    ilRc = TransferFile(cgConfigFile);
    if(ilRc != RC_SUCCESS)
    {
dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */

    sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
    dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
    ilRc = TransferFile(cgConfigFile);
    if(ilRc != RC_SUCCESS)
    { 
dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */

    ilRc = SendRemoteShutdown(mod_id);
    if(ilRc != RC_SUCCESS)
    {
dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    } /* end of if */

    if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
dbg(DEBUG,"MAIN: waiting for status switch ...");
HandleQueues();
    }/* end of if */

    if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
dbg(TRACE,"MAIN: initializing ...");
ilRc = Init_Process();
if(ilRc != RC_SUCCESS)
{
    dbg(TRACE,"Init_Process: init failed!");
} /* end of if */
    } 
    else 
    {
       Terminate(30);
    }/* end of if */



    dbg(TRACE,"=====================");
    dbg(TRACE,"MAIN: initializing OK");
    dbg(TRACE,"=====================");

    ForkChild();

    for(;;)
    {
      ilRc = que(QUE_GETBIGNW,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
/* depending on the size of the received item  */
/* a realloc could be made by the que function */
/* so do never forget to set event pointer !!! */
      prgEvent = (EVENT *) prgItem->text;

if( ilRc == RC_SUCCESS )
{
    /* Acknowledge the item */
    ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
    if( ilRc != RC_SUCCESS ) 
    {
/* handle que_ack error */
HandleQueErr(ilRc);
    } /* fi */

    lgEvtCnt++;

    switch( prgEvent->command )
    {
case HSB_STANDBY:
    ctrl_sta = prgEvent->command;
    HandleQueues();
    break;
case HSB_COMING_UP:
    ctrl_sta = prgEvent->command;
    HandleQueues();
    break;
case HSB_ACTIVE:
    ctrl_sta = prgEvent->command;
    break;
case HSB_ACT_TO_SBY:
    ctrl_sta = prgEvent->command;
    /* CloseConnection(); */
    HandleQueues();
    break;
case HSB_DOWN:
    /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
    ctrl_sta = prgEvent->command;
    dbg(TRACE,"MAIN: HSB_DOWN <%d>",ctrl_sta);
    Terminate(1);
    break;
case HSB_STANDALONE:
    ctrl_sta = prgEvent->command;
    ResetDBCounter();
    break;
case REMOTE_DB :
    /* ctrl_sta is checked inside */
    
    HandleRemoteDB(prgEvent);
    break;
case SHUTDOWN:
    /* process shutdown - maybe from uutil */
    dbg(TRACE,"MAIN: SHUTDOWN ");
    Terminate(1);
    break;

case RESET:
    ilRc = Reset();
    break;

case EVENT_DATA:
    if((ctrl_sta == HSB_STANDALONE) || 
       (ctrl_sta == HSB_ACTIVE) || 
       (ctrl_sta == HSB_ACT_TO_SBY))
    {
ilRc = HandleData(prgEvent);
if(ilRc != RC_SUCCESS)
{
    HandleErr(ilRc);
}/* end of if */
    }else{
dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
DebugPrintItem(TRACE,prgItem);
DebugPrintEvent(TRACE,prgEvent);
    }/* end of if */
    break;

case TRACE_ON :
    dbg_handle_debug(prgEvent->command);
    break;
case TRACE_OFF :
    dbg_handle_debug(prgEvent->command);
    break;
default:
    dbg(TRACE,"MAIN: unknown event");
    DebugPrintItem(TRACE,prgItem);
    DebugPrintEvent(TRACE,prgEvent);
    break;
    } /* end switch */
} 
else if (ilRc == QUE_E_NOMSG)
{
    if(time(NULL) > (tgLastCheck + igCheckTime))
    {
char clCmd[1024];
char clTOCmd[100] = "\0";
char *pclData = NULL;

PutCommandOnSocket("CHK","xx", " ", " "," ");
GetCommandFromSocket(clCmd,&pclData,clTOCmd);
//SocketDataToWERTAB(" ",pclData,"SIN",cgService);
//ilRc = snap(pclData, 20, outp);
if (strncmp(clCmd,"COC",3) != 0)
{
    dbg(TRACE,"invalid reply to CHK command <%s> %p",clCmd,pclData);

    sprintf(clAlert,"Fatal Error while starting <%s>",mod_name);
    //ilRc = AddAlert2("ALTEA"," "," ","E",clAlert," ",TRUE,FALSE,"ALERT",mod_name," "," ");
    Terminate(30);
}
    }
    else
    {
nap(500);
    }
}
else {
    /* Handle queuing errors */
    HandleQueErr(ilRc);
} /* end else */
    }

} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/

static int CheckQueue()
{
    int ilRc = RC_SUCCESS;

    ilRc = que(QUE_GETBIGNW,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
    /* depending on the size of the received item  */
    /* a realloc could be made by the que function */
    /* so do never forget to set event pointer !!! */
    prgEvent = (EVENT *) prgItem->text;

    if( ilRc == RC_SUCCESS )
    {

lgEvtCnt++;

switch( prgEvent->command )
{
    case HSB_STANDBY:
ctrl_sta = prgEvent->command;
HandleQueues();
break;
    case HSB_COMING_UP:
ctrl_sta = prgEvent->command;
HandleQueues();
break;
    case HSB_ACTIVE:
ctrl_sta = prgEvent->command;
break;
    case HSB_ACT_TO_SBY:
ctrl_sta = prgEvent->command;
/* CloseConnection(); */
HandleQueues();
break;
    case HSB_DOWN:
/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
ctrl_sta = prgEvent->command;
dbg(TRACE,"<CheckQueue> HSB_DOWN <%d>",ctrl_sta);
Terminate(1);
break;
    case HSB_STANDALONE:
ctrl_sta = prgEvent->command;
ResetDBCounter();
break;
    case REMOTE_DB :
/* ctrl_sta is checked inside */
HandleRemoteDB(prgEvent);
break;
    case SHUTDOWN:
/* process shutdown - maybe from uutil */
dbg(TRACE,"<CheckQueue> SHUTDOWN ");
Terminate(1);
break;

    case RESET:
ilRc = Reset();
break;

    case EVENT_DATA:
if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
{
    ilRc = HandleData(prgEvent);
    if(ilRc != RC_SUCCESS)
    {
HandleErr(ilRc);
    }/* end of if */
}else{
    dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
    DebugPrintItem(TRACE,prgItem);
    DebugPrintEvent(TRACE,prgEvent);
}/* end of if */
break;


    case TRACE_ON :
dbg_handle_debug(prgEvent->command);
break;
    case TRACE_OFF :
dbg_handle_debug(prgEvent->command);
break;
    default:
dbg(TRACE,"MAIN: unknown event");
DebugPrintItem(TRACE,prgItem);
DebugPrintEvent(TRACE,prgEvent);
break;
} /* end switch */

/* Acknowledge the item */
ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
if( ilRc != RC_SUCCESS ) 
{
    /* handle que_ack error */
    HandleQueErr(ilRc);
} /* fi */
    } else {
/* Handle queuing errors */
HandleQueErr(ilRc);
    } /* end else */

}

static int OpenConnectionOld(char *pcpPort)
{
    int   ilRc = RC_FAIL;   /* Return code                  */
    int i=0;
    int ilReady=FALSE;


    igStartSocket = tcp_create_socket (SOCK_STREAM, NULL); /* create the socket*/
    if (igStartSocket < 0)
    {
dbg(TRACE,"OpenConn: Create socket failed: %s ", strerror( errno));
ilRc = RC_FAIL;
ilReady = TRUE;
    }
    else
    {
alarm(igConnectTimeout);
errno = 0;
ilRc = tcp_open_connection (igStartSocket, pcpPort, "localhost");
alarm(0);
if (ilRc != RC_SUCCESS)
{
    dbg(TRACE,"OpenConn: on %s failed: %s ", "localhost",
strerror( errno));
    close(igStartSocket);
    igStartSocket = 0;
}
else
{
    ilReady = TRUE;
} 
    } 
    dbg(TRACE,"OpenConnection: returns %d ",ilRc);
    return ilRc;
}
/* ******************************************************************** */
/* The OpenConn routine                                                 */
/* ******************************************************************** */
static int OpenConnection(char *pcpPort)
{
    int   ilRc = RC_FAIL;   /* Return code                  */
    int i=0;
    //int ilReady=FALSE;
    
    fd_set fds;
    struct timeval timeout;
    FD_ZERO(&fds);

    //1. create socket
    igStartSocket = TcpCreateSocket(SOCK_STREAM,atoi(pcpPort)); /* create the socket*/
    if (igStartSocket < 0)
    {
        dbg(TRACE,"<OpenConnection> Create socket failed <%s> ", strerror( errno));
        ilRc = RC_FAIL;
        //ilReady = TRUE;
    }
    else
    {
        dbg(TRACE,"<OpenConnection> Socket created listen on port <%s> follows",pcpPort);
        alarm(igConnectTimeout);
        errno = 0;
        
        //2. listen
        ilRc = listen (igStartSocket,5);
        alarm(0);
        dbg(TRACE,"<OpenConnection> RC from listen <%d> errno=%d <%s>",ilRc,errno,strerror(errno));
        if (ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"<OpenConnection> on %s failed: %s ", "localhost",
            strerror( errno));
            close(igStartSocket);
            igStartSocket = -1;
        }
        else
        {
            char *clTmpBuffer[1024];
            FD_SET(igStartSocket,&fds);
            timeout.tv_sec=igConnectTimeout;
            dbg(TRACE,"<OpenConnection> Selecting will Timeout in <%d> Seconds",timeout.tv_sec);
            timeout.tv_usec=0;
            dbg(TRACE,"%05d",__LINE__);

            //3. select
            int GETFD=FALSE;
            int n;
        
            //while(GETFD!=TRUE) {
            switch(n = select(igStartSocket+1,&fds,0,0,&timeout)) 
            {
                case -1:  //select error
                    dbg(TRACE,"<OpenConnection> RC from Select <%d> errno=%d <%s>",ilRc,errno,strerror(errno));
                    return RC_FAIL;
                    break;     
                case 0:  //time out
                    dbg(TRACE,"<OpenConnection> Select Time Out ");
                    return RC_FAIL;
                    break;    
                default:
                    dbg(TRACE,"%d descriptors ready ...\n", n);
                    if(FD_ISSET(igStartSocket,&fds)) 
                    {
                        igWorkSocket = accept(igStartSocket, NULL, NULL);
                        GETFD = TRUE;
                        dbg(TRACE,"<OpenConnection> Accept <%d>", igWorkSocket);
                        ilRc=RC_SUCCESS;
                    }
                break;
            } // end switch
            //}
     
            //alarm(igConnectTimeout);
            //igWorkSocket = accept(igStartSocket, NULL, NULL);
            //alarm(0);
            //ilRc=RC_SUCCESS;
            dbg(TRACE,"%05d",__LINE__);
            if (igWorkSocket < 0)
            {
                dbg(TRACE,"%05d",__LINE__);
                dbg(TRACE,"<OpenConnection> accept error: %d,<%s>",errno,strerror(errno));
                ilRc=RC_FAIL;
            }
            //ilReady = TRUE;
            //dbg(TRACE,"%05d",__LINE__);
            //alarm(igConnectTimeout);
            //errno = 0;
            //dbg(TRACE,"%05d",__LINE__);
            /*ilRc = read(igWorkSocket,clTmpBuffer,1024);*/
            //alarm(0);
            //dbg(TRACE,"%05d",__LINE__);
            //if (ilRc < 0)
            //{
            //dbg(TRACE,"OpenConnection, read: %d,<%s>",errno,strerror(errno));
            //}
            //if (ilRc > 0)
            //{
            //dbg(TRACE,"OpenConnection: <%s>",clTmpBuffer);
            //ilRc = RC_SUCCESS;
            //}
        } 
        tgLastCheck = time(NULL);
    } 
    dbg(TRACE,"<OpenConnection> returns %d ",ilRc);
    return ilRc;
}


#if 0
int dummy()
{
    int rc=RC_SUCCESS;
    int ilLocalLen;
    int ilBytesToRead;
    int ilBytesAlreadyRead;
    int ilCnt;
    char pclLocalLen[20];
    BC_HEAD *bchd;/* Broadcast header*/
    CMDBLK *cmdblk;/* Command block */
    char *datablk;/* Data Block*/
    int msglen=0;      
    char *my_msg;
  
    ilLocalLen = DFHEAD_LEN;
    ilBytesToRead = ilLocalLen;
    ilBytesAlreadyRead = 0;
    ilCnt = 0;
    memset(prgDfHead->data,0,PDFLEN-DFHEAD_LEN);
    do
    {
        dbg(DEBUG,"reading %d Header bytes from DVS",ilLocalLen);
        alarm(1);
        errno = 0;
        rc = read(sock,(prgDfHead+ilBytesAlreadyRead),ilBytesToRead);
        alarm(0);
        if(rc <= 0)
        {
            if (++ilCnt > 10)
            {
                rc = RC_FAIL;
            }
            dbg(TRACE,"read: error %d %s",errno,strerror(errno));
        }
        else
        {
            ilBytesToRead -= rc;
            ilBytesAlreadyRead += rc;
            if (rc != DFHEAD_LEN)
            {
                dbg(TRACE,"read header: rc %d",rc); 
            }
            dbg(DEBUG,"Header: <%s>",prgDfHead);
            rc = RC_SUCCESS;
        }
    } while ((rc == RC_SUCCESS) && ( ilBytesToRead > 0));

    if (rc == RC_SUCCESS)
    {
        if (strncmp(prgDfHead,pcgDfHeaderStart,igDfHeaderStartLen) != 0)
        {
            dbg(TRACE,"invalid DF-HEAD: <%s>",prgDfHead);
            rc = RC_FAIL;
        }
    }
    if (rc == RC_SUCCESS)
    {
        strncpy(pclLocalLen,prgDfHead->len,4);
        pclLocalLen[4] = '\0';
        ilLocalLen = atoi(pclLocalLen);
        if (ilLocalLen > PDFLEN)
        {
            rc = RC_FAIL;
        }
    }
    if (rc == RC_SUCCESS)
    {
        /*snap (prgDfHead, rc, outp);*/
        strncpy(pclLocalLen,prgDfHead->len,4);
        pclLocalLen[4] = '\0';
        ilLocalLen = atoi(pclLocalLen);
        ilBytesToRead = ilLocalLen;
        ilBytesAlreadyRead = 0;
        do
        {
            dbg(DEBUG,"reading %d <%s> bytes from DVS",ilLocalLen,pclLocalLen);
            if ((rc = read (sock, (prgDfHead->data+ilBytesAlreadyRead),ilBytesToRead)) <= 0)
            {
                rc = RC_FAIL;
                dbg(TRACE,"read: error %d %s",errno,strerror(errno));
            }
            else
            {
                ilBytesToRead -= rc;
                ilBytesAlreadyRead += rc;
                dbg(DEBUG,"read: rc %d",rc); 
                rc = RC_SUCCESS;
            }
        } while ((rc == RC_SUCCESS) && ( ilBytesToRead > 0));
    }
  
    if (rc == RC_SUCCESS)
    {
        /*snap (prgDfHead->data, rc, outp);*/
        if (pgLogFile)
        {
            fwrite(prgDfHead->data,sizeof(char),ilLocalLen,pgLogFile);
            fwrite("\n",sizeof(char),1,pgLogFile);
            fflush(pgLogFile);
        }
        prgDfHead->data[ilLocalLen] = '\0';
        my_msg = (char *)prgDfHead->data;
        msglen = strlen(prgDfHead->data) + sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 4;
        out_event = (EVENT *) calloc (1, msglen);
        if (out_event == NULL)
        {
            rc = RC_FAIL;
        } /* fi */
        if (rc == RC_SUCCESS)
        {
            bchd = (BC_HEAD *) (((char *)out_event) + sizeof(EVENT));
            cmdblk= (CMDBLK  *) ((char *)bchd->data);
            datablk = (char *) cmdblk->data;
            strcpy(cmdblk->command, "DVS");
            strcpy(datablk, my_msg);
            bchd->rc = RC_SUCCESS;
            out_event->data_length = msglen - sizeof(EVENT);
            out_event->command = EVENT_DATA;
            out_event->originator = mod_id;
            out_event->data_offset = sizeof(EVENT);
#ifdef REALBETRIEB
            rc = que(QUE_PUT, QUE_ID_OUT, mod_id, 3, msglen, (char *)out_event);
            if (rc != SUCCESS)
            {
                dbg("Error sending to %d\n",QUE_ID_OUT);
            }/* end if */
#endif
        } /* end if */
        free (out_event);
        rc = RC_SUCCESS;
    }
    return rc;
}
#endif 

static char *GetCommandFromSocket(char *pcpCommand,char **pcpData, char *pcpTimeOutCmd)
{

    int ilRc = RC_SUCCESS;
    long llLocalLen;
    long llBytesToRead;
    long llBytesAlreadyRead;
    long llResultLen = 0L;
    int ilCnt = 0;
    char clBytes[120];
    static char *pclTcpBuffer = NULL;
    static long llTcpBufferLen = 0L;
    char *pclTmpBuffer;
    char clAlert[200] = "\0";
  
    llLocalLen = 15;
    llBytesToRead = llLocalLen;
    llBytesAlreadyRead = 0;
    ilCnt = 0;

    //dbg(TRACE,"<GetCommandFromSocket> Start");
    if (llTcpBufferLen < llLocalLen)
    {
        pclTcpBuffer = realloc(pclTcpBuffer,llLocalLen);
        if (pclTcpBuffer == NULL)
        {
            dbg(TRACE,"<GetCommandFromSocket>: could not allocate TcpBuffer, reason %d <%s>",
            errno,strerror(errno));

            sprintf(clAlert,"Fatal Error while starting <%s>",mod_name);
            //ilRc = AddAlert2("ALTEA"," "," ","E",clAlert," ",TRUE,FALSE,"ALERT",mod_name," "," ");
            Terminate(30);
        }
        llTcpBufferLen = llLocalLen;
    }

    pclTmpBuffer = pclTcpBuffer;

    if (igStartSocket < 0)
    {
        if(OpenConnection(cgService) != RC_SUCCESS)
        {
            dbg(TRACE,"<GetCommandFromSocket>: OpenConnection could not establish connection on port <%s>",cgService);
            sprintf(clAlert,"Fatal Error while starting <%s>",mod_name);
            //ilRc = AddAlert2("ALTEA"," "," ","E",clAlert," ",TRUE,FALSE,"ALERT",mod_name," "," ");
            Terminate(30);
        }
    }
    
    if (igStartSocket >= 0)
    {
        memset(pclTcpBuffer,0,llTcpBufferLen);
        do
        {
            dbg(TRACE,"<GetCommandFromSocket>: reading %d bytes from socket",llBytesToRead);
            alarm(igTcpTimeout);
            errno = 0;
            ilRc = read(igWorkSocket,pclTmpBuffer,llBytesToRead);
            dbg(TRACE,"<GetCommandFromSocket>: RC from read is %d <%d>",ilRc,strlen(pclTcpBuffer));
            alarm(0);
            if(ilRc <= 0)
            {
                if (++ilCnt > 10)
                {
                    ilRc = RC_FAIL;
                }
                dbg(TRACE,"<GetCommandFromSocket>: read error %d %s",errno,strerror(errno));
            }
            else
            {
                ilCnt = 0;
                llBytesToRead -= ilRc;
                llBytesAlreadyRead += ilRc;
                pclTmpBuffer += ilRc;
                ilRc = RC_SUCCESS;
            }
        } 
        while (ilRc == RC_SUCCESS && llBytesToRead > 0);

dbg(TRACE,"<GetCommandFromSocket>: read data  <%s> total/read Bytes %d/%d",
    pclTcpBuffer,llLocalLen,llBytesAlreadyRead);

if (GetKeyItem(clBytes,&llResultLen,pclTcpBuffer,"#SIZE#<",">;",TRUE) == NULL)
{
    dbg(TRACE,"<GetCommandFromSocket>: invalid size parameter <%s>",pclTcpBuffer);
    sprintf(clAlert,"Fatal Error while starting <%s>",mod_name);
    //ilRc = AddAlert2("ALTEA"," "," ","E",clAlert," ",TRUE,FALSE,"ALERT",mod_name," "," ");
    Terminate(30);
}
llLocalLen = atol(clBytes);
llBytesAlreadyRead  = 0;
dbg(TRACE,"<GetCommandFromSocket>: Size read <%ld> <%s>",llLocalLen,clBytes);

if (llTcpBufferLen < llLocalLen)
{
    pclTcpBuffer = realloc(pclTcpBuffer,llLocalLen*sizeof(char)+1);
    if (pclTcpBuffer == NULL)
    {
dbg(TRACE,"GetCommandFromSocket: could not allocate %d Bytes for "
    "TcpBuffer, reason %d <%s>",llLocalLen,errno,strerror(errno));
sprintf(clAlert,"Fatal Error while starting <%s>",mod_name);
//ilRc = AddAlert2("ALTEA"," "," ","E",clAlert," ",TRUE,FALSE,"ALERT",mod_name," "," ");
Terminate(30);
    }
    llTcpBufferLen = llLocalLen;
    //dbg(TRACE,"<GetCommandFromSocket>: realloc momery <%d>",llTcpBufferLen);
}

pclTmpBuffer = pclTcpBuffer;
llBytesToRead = llLocalLen; 
memset(pclTcpBuffer,0,llTcpBufferLen);

  do
  {
    dbg(TRACE,"<GetCommandFromSocket>: reading %d bytes from socket",llBytesToRead);
    alarm(igTcpTimeout);
    errno = 0;
    ilRc = read(igWorkSocket,pclTmpBuffer,llBytesToRead);
    alarm(0);
    //dbg(TRACE,"<GetCommandFromSocket>: RC from read is %d <%d>",ilRc,strlen(pclTcpBuffer));
    if(ilRc <= 0)
    {
      if (++ilCnt > 10)
      {
       ilRc = RC_FAIL;
      }
      dbg(TRACE,"<GetCommandFromSocket>: read error %d %s",errno,strerror(errno));
    }
    else
    {
      ilCnt = 0;
      llBytesToRead -= ilRc;
      llBytesAlreadyRead += ilRc;
      pclTmpBuffer += ilRc;
      ilRc = RC_SUCCESS;
    }
  } while (ilRc == RC_SUCCESS && llBytesToRead > 0);
}

    dbg(TRACE,"<GetCommandFromSocket>: TcpBuffer (first 1000chars)  \n    <%.1000s> total/read Bytes %d/%d",
pclTcpBuffer,llLocalLen,llBytesAlreadyRead);
//dbg(TRACE,"<GetCommandFromSocket>: pclTcpBuffer:<%s>",pclTcpBuffer);

    if (igStartSocket >= 0)
    {
        pcpCommand = GetKeyItem(pcpCommand,&llResultLen,pclTcpBuffer,"#CMD#<",">;",TRUE);
GetKeyItem(pcpTimeOutCmd,&llResultLen,pclTcpBuffer,"#TIMEOUT#<",">",TRUE);
dbg(TRACE,"<GetCommandFromSocket>: pcpTimeOutCmd<%s>",pcpTimeOutCmd);
pclTmpBuffer = GetKeyItem(NULL,&llResultLen,pclTcpBuffer,"#DAT#[","]",FALSE);
//dbg(TRACE,"TmpBuffer from GetKeyItem(%s)= %p <%s> ResultLen=%ld","#CMD#<",
    //pclTmpBuffer, pclTmpBuffer != NULL ? pclTmpBuffer : "NULL",llResultLen);
    
if (pclTmpBuffer != NULL)
{
    pclTmpBuffer[llResultLen] = '\0';
}
*pcpData = pclTmpBuffer;
tgLastCheck = time(NULL);
return(pclTmpBuffer);
    }
    else
    {
*pcpCommand = '\0';
dbg(TRACE,"<GetCommandFromSocket>: Command is NULL");
return NULL;
    }
}


static int PutCommandOnSocket(char *pcpCommand,char *pcpData, char *pcpFields, char *pcpSelection, char *pcpTwEnd)
{

    int ilRc = RC_SUCCESS;
    long llLocalLen;
    long llBytesToRead;
    long llBytesAlreadyWritten;
    int ilCnt = 0;
    static char *pclTcpBuffer = NULL;
    char *pclTmpBuffer = NULL;
    static long llTcbBufferSize = 0L;
    char clAlert[200] = "\0";
    
    char clFields [2048];
    char clValues [2048];

    llBytesAlreadyWritten = 0;
    ilCnt = 0;

    dbg(TRACE,"<PutCommandOnSocket>: Cmd <%s> Data <%s>",pcpCommand,pcpData);
    if (igStartSocket < 0)
    {
dbg(TRACE,"1OpenConnection follows");
dbg(TRACE,"OpenConnection follows");
if(OpenConnection(cgService) != RC_SUCCESS)
{
    dbg(TRACE,"OpenConnection: could not establish connection on port <%s>",cgService);
    sprintf(clAlert,"Fatal Error while starting <%s>",mod_name);
    //ilRc = AddAlert2("ALTEA"," "," ","E",clAlert," ",TRUE,FALSE,"ALERT",mod_name," "," ");
    Terminate(30);
}
    }
    if (igWorkSocket > 0)
    {
long llActualCommandLen;

llActualCommandLen = 31;  /* 15+8+8 */
llActualCommandLen += strlen(pcpCommand);
llActualCommandLen += strlen(pcpData);
llActualCommandLen += 8;
llActualCommandLen += strlen(pcpFields);
llActualCommandLen += 8;
llActualCommandLen += strlen(pcpSelection);


if (llActualCommandLen > llTcbBufferSize)
{
    pclTcpBuffer = realloc(pclTcpBuffer,llActualCommandLen);
    llTcbBufferSize = llActualCommandLen;
    if (pclTcpBuffer == NULL)
    {
dbg(TRACE,"could not allocate Tcp buffer, reason: %d <%s>",
    errno,strerror(errno));
sprintf(clAlert,"Fatal Error while starting <%s>",mod_name);
//ilRc = AddAlert2("ALTEA"," "," ","E",clAlert," ",TRUE,FALSE,"ALERT",mod_name," "," ");
Terminate(30);
    }
}

pclTmpBuffer = pclTcpBuffer;
sprintf(pclTcpBuffer,"#SIZE#<%06ld>;#CMD#<%s>;#DAT#<%s>;#FLD#<%s>;#SEL#<%s>",
llActualCommandLen-15,pcpCommand,pcpData,pcpFields,pcpSelection);
llLocalLen = llActualCommandLen;
do
{
    //dbg(TRACE,"<PutCommandOnSocket>: writing %d bytes to socket",llLocalLen);
    dbg(TRACE,"<PutCommandOnSocket>: writing  %d <%s> to socket",strlen(pclTmpBuffer),pclTmpBuffer);
    alarm(igTcpTimeout);
    errno = 0;
    ilRc = write(igWorkSocket,pclTmpBuffer,llLocalLen);
    alarm(0);
    if(ilRc <= 0)
    {
if (++ilCnt > 10)
{
    ilRc = RC_FAIL;
}
dbg(TRACE,"write: error %d %s",errno,strerror(errno));
    }
    else
    {
ilCnt = 0;
llLocalLen -= ilRc;
llBytesAlreadyWritten += ilRc;
pclTmpBuffer += ilRc;
ilRc = RC_SUCCESS;


    }
} while (ilRc == RC_SUCCESS && llLocalLen > 0);
if (ilRc == RC_SUCCESS && abs(strcmp(pcpCommand,"CHK")) && abs(strcmp(pcpCommand,"PARALOG")) && abs(strcmp(pcpCommand,"SHUTDOWN")))
{
//write to db
//char mySelection[strlen(pcpSelection)+1];
//strncpy(mySelection,pcpSelection,strlen(pcpSelection));
pclTmpBuffer -= llBytesAlreadyWritten;
char myUAFT[10];
strncpy(myUAFT,pcpData,9);
myUAFT[9]='\0';

long myURNO = NewUrnos("WESTAB",1);

//char *myUAFT = strtok(mySelection, ";");
//if (myUAFT == NULL) {
 //   dbg(TRACE, "<PutCommandOnSocket> Fatal -- no UAFT");
 //   myUAFT=" ";
 // }
  
  char myTwEnd[strlen(pcpTwEnd)+1];
  strncpy(myTwEnd,pcpTwEnd,strlen(pcpTwEnd));
  char *myHOPO = strtok(myTwEnd,";");
  if (myHOPO == NULL) {
  dbg(TRACE, "<PutCommandOnSocket> Fatal -- no HOPO");
    myHOPO=" ";
  }

int myPARN=1;
int myPART=1;
int myPAID=1;

char  myCurrentUtcTime[15];
GetServerTimeStamp("UTC", 1, 0, myCurrentUtcTime);
sprintf(clFields,"URNO,UAFT,DATA,PARN,PART,PAID,HOPO,CDAT,USEC");
sprintf(clValues,"'%d','%s','%s','%d','%d','%d','%s','%s','%s'",
          myURNO,myUAFT,pclTmpBuffer,myPARN,myPART,myPAID,myHOPO,myCurrentUtcTime,mod_name);
sprintf(pcgSqlBuf,"INSERT INTO WESTAB (%s) VALUES(%s)", clFields, clValues);
      int ilRC = getOrPutDBData(START|COMMIT);
      if (ilRC != RC_SUCCESS) {
         dbg(TRACE, "<PutCommandOnSocket> Unable to write data to WESTAB,UAFT <%s>", myUAFT);
      }
      else
      dbg(TRACE, "<PutCommandOnSocket> Write Data to WESTAB,UAFT <%s> URNO <%d>", myUAFT,myURNO);
}
    }
    return ilRc;
}


static int PutCommandOnQueue()
{

    int ilRc = RC_SUCCESS;


    return ilRc;
}




static int ReadConfigEntry(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer)
{
    int ilRc = RC_SUCCESS;

    char clSection[124] = "\0";
    char clKeyword[124] = "\0";

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRc = iGetConfigEntry(cgConfigFile,clSection,clKeyword,CFG_STRING,pcpCfgBuffer);
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
    } 
    else
    {
        dbg(DEBUG,"Config Entry <%s>,<%s>:<%s> found in %s", clSection, clKeyword ,pcpCfgBuffer, cgConfigFile);
    }/* end of if */
    return ilRc;
}


static int ReadConfigRow(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer)
{
    int ilRc = RC_SUCCESS;

    char clSection[124] = "\0";
    char clKeyword[124] = "\0";

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRc = iGetConfigRow(cgConfigFile,clSection,clKeyword,CFG_STRING,pcpCfgBuffer);
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
    } 
    else
    {
        dbg(DEBUG,"Config Entry <%s>,<%s>:<%s> found in %s",clSection, clKeyword ,pcpCfgBuffer, cgConfigFile);
    }/* end of if */
    return ilRc;
}


static int Init_Process()
{
    int  ilRc = RC_SUCCESS;/* Return code */
    char clSection[64] = "\0";
    char clKeyword[64] = "\0";
    char clTmpString[164] = "\0";
    int ilOldDebugLevel = 0;
    long pclAddFieldLens[12];
    char pclAddFields[256] = "\0";
    char pclSqlBuf[2560] = "\0";
    char pclSelection[1024] = "\0";
    short slCursor = 0;
    short slSqlFunc = 0;
    char  clBreak[24] = "\0";
    char clAlert[200] = "\0";

    if(ilRc == RC_SUCCESS)
    {
        /* read HomeAirPort from SGS.TAB */
        ilRc = tool_search_exco_data("SYS", "HOMEAP", cgHopo);
        if (ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"<Init_Process> EXTAB,SYS,HOMEAP not found in SGS.TAB");
  
            sprintf(clAlert,"Fatal Error while starting <%s>",mod_name);
            //ilRc = AddAlert2("ALTEA"," "," ","E",clAlert," ",TRUE,FALSE,"ALERT",mod_name," "," ");
            Terminate(30);
        } 
        else 
        {
            dbg(TRACE,"<Init_Process> home airport <%s>",cgHopo);
        }
    }

    if(ilRc == RC_SUCCESS)
    {
        ilRc = tool_search_exco_data("ALL","TABEND", cgTabEnd);
        if (ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"<Init_Process> EXTAB,ALL,TABEND not found in SGS.TAB");
            sprintf(clAlert,"Fatal Error while starting <%s>",mod_name);
            //ilRc = AddAlert2("ALTEA"," "," ","E",clAlert," ",TRUE,FALSE,"ALERT",mod_name," "," ");
            Terminate(30);
        } 
        else 
        {
            dbg(TRACE,"<Init_Process> table extension <%s>",cgTabEnd);
        }
    }

    if(ilRc == RC_SUCCESS)
    {
        GetDebugLevel("STARTUP_MODE", &igStartUpMode);
        GetDebugLevel("RUNTIME_MODE", &igRuntimeMode);
 
        debug_level = igStartUpMode;

        ilRc = ReadConfigEntry("MAIN","SENDTOQUEUE",clTmpString);
        if(ilRc == RC_SUCCESS)
        {
            igSendQueue = atoi(clTmpString);
            dbg(TRACE,"Messages will be send to %d",igSendQueue);
        }
        /* MEI v1.1 */
        igResponseDelay = 0;
        ilRc = ReadConfigEntry("MAIN","RESPONSE_DELAY_SECS",clTmpString);
        if( ilRc == RC_SUCCESS )
            igResponseDelay = atoi(clTmpString);
        if( igResponseDelay < 0 )
            igResponseDelay = 0;
        dbg( TRACE,"Intentional DELAY in response sending = %d seconds", igResponseDelay );

        ilRc = ReadConfigEntry("NETWORK","SERVICE",cgService);
        if(ilRc == RC_SUCCESS)
        {
            igService = atoi(cgService);
            dbg(TRACE,"Port to be listened to %d",igService);
        }

        ilRc = ReadConfigEntry("NETWORK","TCP_TIMEOUT",clTmpString);
        if(ilRc == RC_SUCCESS)
        {
            igTcpTimeout = atoi(clTmpString);
            dbg(TRACE,"TCP_TIMEOUT %d",igTcpTimeout);
        }

        ilRc = ReadConfigEntry("NETWORK","CONNECT_TIMEOUT",clTmpString);
        if(ilRc == RC_SUCCESS)
        {
            igConnectTimeout = atoi(clTmpString);
            dbg(TRACE,"CONNECT_TIMEOUT %d",igConnectTimeout);
        }

        ilRc = ReadConfigEntry("NETWORK","CHECKTIME",clTmpString);  
        if(ilRc == RC_SUCCESS)
        {
            igCheckTime = atoi(clTmpString);
            dbg(TRACE,"CHECKTIME %d",igCheckTime);
        }

        ilRc = ReadConfigRow("JAVA","JRE",cgJRE);
        if(ilRc == RC_SUCCESS)
        {
            dbg(TRACE,"Path to Java Runtime is <%s>",cgJRE);
        }

        ilRc = ReadConfigRow("JAVA","JRE_PARAMETER",cgJREPara);
        if(ilRc == RC_SUCCESS)
        {
            dbg(TRACE,"Parameter for JRE is <%s>",cgJREPara);
        }

        ilRc = ReadConfigEntry("JAVA","PROGRAM",cgJavaProgram);
        if(ilRc == RC_SUCCESS)
        {
            dbg(TRACE,"Java part of bridge is <%s>",cgJavaProgram);
        }

        ilRc = ReadConfigEntry("JAVA","SCRIPT",cgJavaScript);
        if(ilRc == RC_SUCCESS)
        {
            dbg(TRACE,"Java part of bridge is <%s>",cgJavaScript);
        }

        ilRc = ReadConfigEntry("JAVA","SHELL",cgJavaShell);
        if(ilRc == RC_SUCCESS)
        {
            dbg(TRACE,"Java part of bridge is <%s>",cgJavaShell);
        }

        ilRc = ReadConfigEntry("PARAMETER","PARALOG",cgJavaLogFileName);
        if(ilRc == RC_SUCCESS)
        {
            dbg(TRACE,"Java part of bridge is <%s>",cgJavaLogFileName);
        }

        if (ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"Init_Process failed");
        }
    }

    //debug_level = DEBUG;
    dbg(TRACE,"debug_level set to %d",igRuntimeMode);
    return(ilRc);

} /* end of initialize */


static int GetDebugLevel(char *pcpMode, int *pipMode)
{
    int ilRc = RC_SUCCESS;
    char clCfgValue[64];

    ilRc = iGetConfigRow(cgConfigFile, "SYSTEM", pcpMode, CFG_STRING,
 clCfgValue);

    if(ilRc != RC_SUCCESS)
    {
dbg(TRACE,"SYSTEM %s not found in <%s>", pcpMode, cgConfigFile);
    }
    else
    {
dbg(TRACE, "SYSTEM %s <%s>", pcpMode, clCfgValue);
if (!strcmp(clCfgValue, "DEBUG"))
    *pipMode = DEBUG;
else if (!strcmp(clCfgValue, "TRACE"))
    *pipMode = TRACE;
else
    *pipMode = 0;
    }
    return ilRc;
}

/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
    int ilRc = RC_SUCCESS;/* Return code */

    dbg(TRACE,"Reset: now resetting");

    return ilRc;

} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
    /* unset SIGCHLD ! DB-Child will terminate ! */

    dbg(TRACE,"Terminate: now DB logoff ...");

    if (igWorkSocket > -1)
    {
PutCommandOnSocket("SHUTDOWN","", " ", " "," ");
    }
    signal(SIGCHLD,SIG_IGN);

    logoff();

    dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);
    sleep(ipSleep < 1 ? 1 : ipSleep);

    dbg(TRACE,"Terminate: now leaving ...");
    fclose(outp);
    exit(0);

} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/

static void HandleSignal(int pipSig)
{
    dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);

    switch(pipSig)
    {
default:
    Terminate(1);
    break;
    } /* end of switch */

    exit(1);

} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
    dbg(TRACE,"HandleErr: <%d> : unknown ERROR",pipErr);

    return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    switch(pipErr) {
case QUE_E_FUNC:/* Unknown function */
    dbg(TRACE,"<%d> : unknown function",pipErr);
    break;
case QUE_E_MEMORY:/* Malloc reports no memory */
    dbg(TRACE,"<%d> : malloc failed",pipErr);
    break;
case QUE_E_SEND:/* Error using msgsnd */
    dbg(TRACE,"<%d> : msgsnd failed",pipErr);
    break;
case QUE_E_GET:/* Error using msgrcv */
    dbg(TRACE,"<%d> : msgrcv failed",pipErr);
    break;
case QUE_E_EXISTS:
    dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
    break;
case QUE_E_NOFIND:
    dbg(TRACE,"<%d> : route not found ",pipErr);
    break;
case QUE_E_ACKUNEX:
    dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
    break;
case QUE_E_STATUS:
    dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
    break;
case QUE_E_INACTIVE:
    dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
    break;
case QUE_E_MISACK:
    dbg(TRACE,"<%d> : missing ack ",pipErr);
    break;
case QUE_E_NOQUEUES:
    dbg(TRACE,"<%d> : queue does not exist",pipErr);
    break;
case QUE_E_RESP:/* No response on CREATE */
    dbg(TRACE,"<%d> : no response on create",pipErr);
    break;
case QUE_E_FULL:
    dbg(TRACE,"<%d> : too many route destinations",pipErr);
    break;
case QUE_E_NOMSG:/* No message on queue */
    dbg(TRACE,"<%d> : no messages on queue",pipErr);
    break;
case QUE_E_INVORG:/* Mod id by que call is 0 */
    dbg(TRACE,"<%d> : invalid originator=0",pipErr);
    break;
case QUE_E_NOINIT:/* Queues is not initialized*/
    dbg(TRACE,"<%d> : queues are not initialized",pipErr);
    break;
case QUE_E_ITOBIG:
    dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
    break;
case QUE_E_BUFSIZ:
    dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
    break;
default:/* Unknown queue error */
    dbg(TRACE,"<%d> : unknown error",pipErr);
    break;
    } /* end switch */
         
    return;

} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
    int ilRc = RC_SUCCESS;/* Return code */
    int ilBreakOut = FALSE;

    do
    {
ilRc = que(QUE_GETBIGNW,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
/* depending on the size of the received item  */
/* a realloc could be made by the que function */
/* so do never forget to set event pointer !!! */
prgEvent = (EVENT *) prgItem->text;

if( ilRc == RC_SUCCESS )
{
    /* Acknowledge the item */
    ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
    if( ilRc != RC_SUCCESS ) 
    {
       /* handle que_ack error */
       HandleQueErr(ilRc);
    } /* fi */

    switch( prgEvent->command )
    {
case HSB_STANDBY:
    ctrl_sta = prgEvent->command;
    break;

case HSB_COMING_UP:
    ctrl_sta = prgEvent->command;
    break;

case HSB_ACTIVE:
    ctrl_sta = prgEvent->command;
    ilBreakOut = TRUE;
    break;

case HSB_ACT_TO_SBY:
    ctrl_sta = prgEvent->command;
    break;

case HSB_DOWN:
    /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
    ctrl_sta = prgEvent->command;
    dbg(TRACE,"<HandleQueues> HSB_DOWN <%d>",ctrl_sta);
    Terminate(1);
    break;

case HSB_STANDALONE:
    ctrl_sta = prgEvent->command;
    ResetDBCounter();
    ilBreakOut = TRUE;
    break;

case REMOTE_DB :
    /* ctrl_sta is checked inside */
    HandleRemoteDB(prgEvent);
    break;

case SHUTDOWN:
      dbg(TRACE,"<HandleQueues> SHUTDOWN <%d>",ctrl_sta);
    Terminate(1);
    break;

case RESET:
    ilRc = Reset();
    break;

case EVENT_DATA:
    dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
    DebugPrintItem(TRACE,prgItem);
    DebugPrintEvent(TRACE,prgEvent);
    break;

case TRACE_ON :
    dbg_handle_debug(prgEvent->command);
    break;

case TRACE_OFF :
    dbg_handle_debug(prgEvent->command);
    break;

default:
    dbg(TRACE,"HandleQueues: unknown event");
    DebugPrintItem(TRACE,prgItem);
    DebugPrintEvent(TRACE,prgEvent);
    break;
    } /* end switch */
} else {
    /* Handle queuing errors */
    HandleQueErr(ilRc);
} /* end else */
    } while (ilBreakOut == FALSE);

    ilRc = Init_Process();
    if(ilRc != RC_SUCCESS)
    {
dbg(TRACE,"Init_Process: init failed!");
    } 
     

} /* end of HandleQueues */




/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(EVENT *prpEvent)
{
    int   ilRc           = RC_SUCCESS;/* Return code */
    int      ilCmd          = 0;

    BC_HEAD *prlBchead       = NULL;
    CMDBLK  *prlCmdblk       = NULL;
    char    *pclSelection    = NULL;
    char    *pclFields       = NULL;
    char    *pclData         = NULL;
    char    *pclRow          = NULL;
    char clCmd[2400];
    char clTimeoutCmd[100] = "\0";
    char clTable[34];
    int  ilUpdPoolJob = TRUE;
    char clAlert[200] = "\0"; 
    char myFields[1024] = "\0"; 
    

    prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
    prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
    pclSelection = prlCmdblk->data;
    pclFields    = pclSelection + strlen(pclSelection) + 1;
    pclData      = pclFields + strlen(pclFields) + 1;


    strcpy(clTable,prlCmdblk->obj_name);

    dbg(TRACE,"========== <HandleData> START <%10.10d> ==========",lgEvtCnt);

    /****************************************/
    /*DebugPrintBchead(TRACE,prlBchead);*/
    /*DebugPrintCmdblk(TRACE,prlCmdblk);*/
    dbg(TRACE,"<HandleData> Command: <%s>",prlCmdblk->command);
    dbg(TRACE,"<HandleData> TwEnd: <%s>",prlCmdblk->tw_end);
    dbg(TRACE,"<HandleData> originator follows event = %p ",prpEvent);

    dbg(TRACE,"<HandleData> originator<%d>",prpEvent->originator);
    dbg(TRACE,"<HandleData> selection follows Selection = %p ",pclSelection);
    dbg(TRACE,"<HandleData> selection <%s>",pclSelection);
    dbg(TRACE,"<HandleData> fields    <%s>",pclFields);
    dbg(TRACE,"<HandleData> data      <%s>",pclData);
    /****************************************/
    char myUAFT[20]="\0";
    sprintf(myUAFT,"%s",pclFields); 
    //char myUAFT[10];
    //strncpy(myUAFT,pclData,9);
    //myUAFT[9]='\0';

    PutCommandOnSocket(prlCmdblk->command,pclData,pclFields,pclSelection,prlCmdblk->tw_end); 
    if ((pclData = GetCommandFromSocket(clCmd,&pclData,clTimeoutCmd)) != NULL)
    {
        dbg(TRACE, "<HandleData> send to walhdl pclData <%s> pclSelection<%s>",pclData,pclSelection);
        //if(clTimeoutCmd == NULL)
        if(clTimeoutCmd[0] == '\0')
        {
            dbg(TRACE, "<HandleData> No Time Out");
            sprintf(myFields,"%s",pclFields);
        }
        else
        {
            dbg(TRACE, "<HandleData> Time Out<%s>",clTimeoutCmd);
            sprintf(myFields,"%s,%s",pclFields,clTimeoutCmd);
        }
        /* MEI v1.1 */
        if( igResponseDelay > 0 )
        {
            dbg(TRACE, "<HandleData> SLEEP... for <%d> seconds before sending back response", igResponseDelay );
            sleep( igResponseDelay );
        }

        SendCedaEvent(prpEvent->originator,0,prlBchead->dest_name,prlBchead->recv_name,
                      prlCmdblk->tw_start,prlCmdblk->tw_end,
                      prlCmdblk->command,prlCmdblk->obj_name,pclSelection,
                      myFields,pclData,"", 5,RC_SUCCESS) ;
        //dbg(TRACE,"<HandleData>: got answer from java client <%s> <%s>",clCmd,pclData);
    
        //write to DB
        //long myURNO = NewUrnos("WERTAB",1);

        //char *myUAFT = strtok(DataforWERTAB, ";");
        //if (myUAFT == NULL) {
        //dbg(TRACE, "<PutCommandOnSocket> Fatal -- no UAFT");
        //myUAFT=" ";
        //}
  
        char *myHOPO = strtok(prlCmdblk->tw_end,";");
        char *myUSEC;
        if (myHOPO == NULL) 
        {
            dbg(TRACE, "<PutCommandOnSocket> Fatal -- no HOPO");
            myHOPO=" ";
        }
        else 
        {
            strtok(NULL,";");
            myUSEC=strtok(NULL,";");
            if(myUSEC == NULL)
                myUSEC=" ";
        }

        //int myPARN=0;
        //int myPART=0;
        //int myPAID=0;
        char tmpdata[strlen(pclData)+1];
        tmpdata[strlen(pclData)]='\0';
        strncpy(tmpdata,pclData,strlen(pclData));
        SocketDataToWERTAB(myUAFT,tmpdata,myHOPO,mod_name);
    }
    else
    {
        dbg(TRACE,"<HandleData> no answer from java client");
        sprintf(clAlert,"<HandleData> Fatal Error while starting <%s>",mod_name);
        //ilRc = AddAlert2("ALTEA"," "," ","E",clAlert," ",TRUE,FALSE,"ALERT",mod_name," "," ");
        Terminate(30);
    }
    dbg(TRACE,"==========  END  <%10.10d> ==========",lgEvtCnt);

    return(RC_SUCCESS);

} /* end of HandleData */

static void SocketDataToWERTAB(char *UAFT, char *TCPData, char *HOPO,char *USEC)
{
    
  long myURNO = NewUrnos("WERTAB",1);
    int WERTAB_DATA_LIMIT =4000;
  int myPARN=1;
  int myPART=1;
  int myPAID=myURNO;
  int ParNLen;
  char clValues [5000];//must large than WERTAB_DATA_LIMIT
  char clFields [2048];
  char  myCurrentTime[15];
  char *myDataPtr;
  char myData[WERTAB_DATA_LIMIT+1];
  myData[WERTAB_DATA_LIMIT]='\0';
    
    //1.caculate PART
    int DataLen=strlen(TCPData);
    myPART = DataLen / WERTAB_DATA_LIMIT +1;
    if(DataLen%WERTAB_DATA_LIMIT==0) myPART--;
    
    GetServerTimeStamp("UTC", 1, 0, myCurrentTime);
    //UtcToLocalTimeFixTZ(myCurrentTime);
    myDataPtr=TCPData;
  for(myPARN=1;myPARN<=myPART;++myPARN)
  {
    
    if (myPARN==myPART)
        {
            //2.cut data
            ParNLen = DataLen- (myPARN-1)*WERTAB_DATA_LIMIT;
           strncpy(myData,myDataPtr,ParNLen);
           myData[ParNLen]='\0';
         }
    else
        {
            ParNLen=WERTAB_DATA_LIMIT;
        strncpy(myData,myDataPtr,WERTAB_DATA_LIMIT);
    }
    //3.write DB
    //sprintf(clFields,"URNO,UAFT,DATA,PARN,PART,PAID,HOPO,CDAT,USEC");
    //sprintf(clValues,"'%d','%s','%s','%d','%d','%d','%s','%s','%s'",
    //myURNO,UAFT,myData,myPARN,myPART,myPAID,HOPO,myCurrentTime,mod_name);
    sprintf(clFields,"URNO,UAFT,DATA,PARN,PART,HOPO,CDAT,USEC");
    sprintf(clValues,"'%d','%s','%s','%d','%d','%s','%s','%s'",
    myURNO,UAFT,myData,myPARN,myPART,HOPO,myCurrentTime,mod_name);
    sprintf(pcgSqlBuf,"INSERT INTO WERTAB (%s) VALUES(%s)", clFields, clValues);
    int ilRC = getOrPutDBData(START|COMMIT);
    
    if (ilRC != RC_SUCCESS) {
    dbg(TRACE, "<SocketDataToWERTAB> Unable to write data to WERTAB,UAFT:<%s>", UAFT);
    return;
   }
   else
   {
    dbg(TRACE, "<SocketDataToWERTAB> Write Data to WERTAB,UAFT:<%s> URNO<%d> PARN<%d> PART<%d> PAID<%d>", 
        UAFT,myURNO,myPARN,myPART,myPAID);
    //4. get new URNO
    if(myPARN<myPART) 
        {
          myURNO = NewUrnos("WERTAB",1);
          myDataPtr += ParNLen;
      }
   }  
 }

} 

static void ForkChild()
{
    int ilRc;
    pid_t plChild = 0;
    int ilCPid = 0;

    errno = 0;
    if ((plChild = fork()) == 0)
    {
char clPid[24];

ilCPid = getpid();
sprintf(clPid,"%d",ilCPid);

/* ############ child process ############ */
dbg(TRACE,"Child: Child <%d> ##### START ##### ",ilCPid);
/*dbg(TRACE,"Java Program will be started <%s><%s><%s><%d>",cgJRE,cgJavaProgram,cgService,ilCPid);*/
dbg(TRACE,"Java Programm will be started by script: <%s><%s><%s><%s><%s>",cgJavaScript,cgService,clPid,cgJavaLogFileName,cgJavaProgram);
/* now start java program */
/*ilRc = execl(cgJRE,cgJRE,cgJREPara,cgJavaProgram,cgService,clPid,cgJavaLogFileName,NULL);*/
//ilRc = execl(cgJavaShell,cgJavaShell,cgJavaScript,cgService,clPid,cgJavaLogFileName,NULL);
ilRc = execl(cgJavaShell,cgJavaShell,cgJavaScript,cgService,clPid,cgJavaLogFileName,cgJavaProgram,NULL);
if (ilRc != RC_SUCCESS)
{
    dbg(TRACE,"Error starting Java Program <%s><%s><%s>",
cgJRE,cgJavaProgram,cgService);
    dbg(TRACE,"erroro while Java Programm will be started by script: <%s><%s><%s><%s>",cgJavaScript,cgService,clPid,cgJavaLogFileName);
    exit(0);
}
    }
    /** this is the parent **/

if (plChild < 0)
{
dbg(TRACE,"could not fork, reason: %d <%s>",errno,strerror(errno));
}
else
{
char clCmd[124];
char clTOCmd[100];
char *pclData;

dbg(TRACE,"PutCommandOnSocket follows");
PutCommandOnSocket("PARALOG",cgJavaLogFileName, " ", " "," ");

GetCommandFromSocket(clCmd,&pclData,clTOCmd);
//SocketDataToWERTAB(" ",pclData,"SIN",cgService);

//ilRc = snap(pclData, 20, outp);
if (strcmp(clCmd,"PARALOG") != 0 || pclData == NULL)
{
  dbg(TRACE,"ForkChild: invalid reply to PARALOG command <%s> %p",clCmd,pclData);
}
else
{
  //if (strcmp(pclData,"PARAOK") != 0)
  //{
//dbg(TRACE,"ForkChild: invalid reply to PARALOG command <%s>",clCmd);
  //}
  //else
  //{
dbg(TRACE,"<ForkChild> Get  Reply : command <%s> data <%s>",clCmd,pclData);
  //}
}
}
}


static char *GetKeyItem(char *pcpResultBuff, long *plpResultSize, 
char *pcpTextBuff, char *pcpKeyWord, char *pcpItemEnd, int bpCopyData)
{
    long llDataSize    = 0L;
    char *pclDataBegin = NULL;
    char *pclDataEnd   = NULL;
    pclDataBegin = strstr(pcpTextBuff, pcpKeyWord);/* Search the keyword*/
    if (pclDataBegin != NULL)
    {/* Did we find it? Yes.*/
pclDataBegin += strlen(pcpKeyWord);/* Skip behind the keyword*/
pclDataEnd = strstr(pclDataBegin, pcpItemEnd);/* Search end of data*/
if (pclDataEnd == NULL)
{/* End not found?*/
    pclDataEnd = pclDataBegin + strlen(pclDataBegin);/* Take the whole string*/
} /* end if */
llDataSize = pclDataEnd - pclDataBegin;/* Now calculate the length*/
if (bpCopyData == TRUE)
{/* Shall we copy?*/
    strncpy(pcpResultBuff, pclDataBegin, llDataSize);/* Yes, strip out the data*/
} /* end if */
    } /* end if */
    if (bpCopyData == TRUE)
    {/* Allowed to set EOS?*/
pcpResultBuff[llDataSize] = 0x00;/* Yes, terminate string*/
    } /* end if */
    *plpResultSize = llDataSize;/* Pass the length back*/
    return pclDataBegin;/* Return the data's begin*/
}
/******************************************************************************/
/* getOrPutDBData                                                             */
/*                                                                            */
/* Fetch data as described in the global pcgSqlBuf and return the result in   */
/* global buffer pcgDataArea.                                                 */
/*                                                                            */
/* Input:                                                                     */
/*        ipMode -- function mode mask like START|COMMIT                      */
/*                                                                            */
/******************************************************************************/
int getOrPutDBData(uint ipMode)
{
  int ilRC = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  short slFkt = 0;
  short slCursor = 0;
  char errBuff[128];

  slFkt = ipMode;
  slCursor = 0;
  ilGetRc = sql_if (slFkt, &slCursor, pcgSqlBuf, pcgDataArea);
  close_my_cursor (&slCursor);
  if (ilGetRc == DB_SUCCESS) {
    return RC_SUCCESS;
  }
  else {
    if (ilGetRc != NOTFOUND) {
      get_ora_err(ilGetRc, &errBuff[0]);
      dbg (TRACE, "<getOrPutDBData> Error getting Oracle data error <%s>", &errBuff[0]);
      return RC_FAIL;
    }
    else  return RC_NODATA;
  }
}
int TcpCreateSocket(int type, short spService)
{
    int ilRC;
    int rc = RC_SUCCESS;/* Return code */
    int sock = 0;
    int ilKeepalive = 0;
    size_t ilRecLen = 0;
    struct sockaddr_in name;

    int opt_bool=1;/* Arg for setsockopt */
    struct linger ling;/* Arg for setsockopt */

    sock = socket(AF_INET, type, 0); /* create socket */

    if (sock < 0 )
    {
dbg(TRACE,"tcp_create_socket: Error can't open socket: %s", strerror( errno));
rc = RC_FAIL;
    }
    else
    {
dbg(DEBUG,"tcp_create_socket: Socket %d opened", sock);
    }


    if(rc == RC_SUCCESS)
    {
ilKeepalive = 0;

#if defined(_UNIXWARE) || defined(_SOLARIS) || defined(_LINUX)
ilRecLen = sizeof(int);
#endif

/* int getsockopt(int s, int level, int optname, void *optval, size_t *optlen); */

rc = getsockopt(sock, SOL_SOCKET, SO_KEEPALIVE, (char *) &ilKeepalive, &ilRecLen);
if (rc == -1 )
{
    rc = RC_FAIL;
    dbg(TRACE,"tcp_create_socket: getsockopt <%s>", strerror(errno));
}
else
{
    if(ilKeepalive == 1)
    {
dbg(DEBUG,"tcp_create_socket: SO_KEEPALIVE already set");
    }
    else
    {
ilKeepalive = 1;
rc = setsockopt(sock, SOL_SOCKET, SO_KEEPALIVE, (char *) &ilKeepalive, sizeof(ilKeepalive));
if (rc == -1 )
{
    rc = RC_FAIL;
    dbg(TRACE,"tcp_create_socket: setsockopt <%s>", strerror(errno));
}
else
{
    dbg(DEBUG,"tcp_create_socket: SO_KEEPALIVE set");
}/* end of if */
    }/* end of if */
}/* end of if */
    }/* end of if */


    if (rc == RC_SUCCESS)
    {
rc = setsockopt (sock, SOL_SOCKET, SO_REUSEADDR, (char *) &opt_bool, sizeof (opt_bool));
if (rc == -1 )
{
    dbg(TRACE,"tcp_create_socket: Error Setsockopt REUSEADDR: %s", strerror(errno));
    rc = RC_FAIL;
}
    }


    if (rc == RC_SUCCESS)
    {
#if defined(_WINNT)
lingstruct.l_onoff = 0;
lingstruct.l_linger = 0;
#else
ling.l_onoff = 0;
ling.l_linger = 0;
#endif
rc = setsockopt (sock, SOL_SOCKET, SO_LINGER, (char *) &ling, sizeof (ling));
if (rc == -1 )
{
    dbg(TRACE,"tcp_create_socket: Error Setsockopt LINGER: %s", strerror(errno));
    rc = RC_FAIL;
}
    }


#if defined(_WINNT)
    rc = RC_SUCCESS;
#endif

    if (rc == RC_SUCCESS)
    {
  
name.sin_addr.s_addr = INADDR_ANY;
name.sin_family = AF_INET;
//name.sin_port   = (u_short) spService;
 name.sin_port   = htons(spService); 
#ifdef _WINNT
while ((rc = bind(sock, &name, sizeof(struct sockaddr_in))) < 0)
#else
    errno = 0;
while ((rc = bind(sock, (struct sockaddr *) &name, sizeof(struct sockaddr_in))) < 0)
#endif
{
    dbg(DEBUG,"tcp_create_socket: bind returns: %d - sleep 10 seconds now -> <%s>", rc, strerror(errno));
    sleep(10);
}
    }


    if (rc == RC_SUCCESS)
    {
dbg(DEBUG,"tcp_create_socket: return sock %d",sock);
return sock;
    }
    else
    {
dbg(DEBUG,"tcp_create_socket: return RC_FAIL");
return RC_FAIL;
    }

}
