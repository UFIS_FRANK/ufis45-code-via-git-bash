#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Sin/SIN_Server/Base/Server/Interface/bsrhdl.c 1.8 2006/09/05 16:48:32SGT mcu Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :  MCU                                                      */
/* Date           :  03.03.2003                                               */
/* Description    :  TA2HDL import from and export to TuA                     */
/*                                                                            */
/* Update history :                                                           */
/*                                                                            */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/

#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <dirent.h>
#include <fcntl.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "syslib.h"
#include "AATArray.h"
#include "db_if.h"
#include <time.h>
#include <cedatime.h>

#define BSDFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgBsdArray.plrArrayFieldOfs[ipFieldNo]])
#define ODAFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgOdaArray.plrArrayFieldOfs[ipFieldNo]])
#define FIELDVAL(rrArray,pcpBuf,ipFieldNo) (&pcpBuf[rrArray.plrArrayFieldOfs[ipFieldNo]])
#define PFIELDVAL(prArray,pcpBuf,ipFieldNo) (&pcpBuf[prArray->plrArrayFieldOfs[ipFieldNo]])

#define CLIENTTODB 0
#define DBTOCLIENT 1
#define NOCONVERT 2

#define CMD_BSR     (0)

#define OVERLAP(start1,end1,start2,end2) strcmp(start1,end2) <= 0 && strcmp(start2,end1) <= 0
#define ISBETWEEN(start1,end1,time) strcmp(start1,time) <= 0 && strcmp(end1,time) >= 0

static char *prgCmdTxt[] = {"BSR",""};
static int    rgCmdDef[] = {CMD_BSR,0};

static char cgFunctionDummy[100];

static char *cgWeekDays[8] = {
	"SO","MO","DI","MI","DO","FR","SA","SO" };
static char *cgWeekDaysEng[8] = {
	"SU","MO","TU","WE","TH","FR","SA","SU" };
static int igTestOutput = 9;

/******************************************************************************/
/***   Fields                                                             *****/
/******************************************************************************/



#define STF_FIELDS "URNO,PENO"


static char cgStfFields[256] = "";

/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */

extern FILE *outp;
int  debug_level = TRACE;

extern errno;

/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static int   igLineNo     = 0;                   /* length of incoming item */
static EVENT *prgOutEvent  = NULL;
static int   igInitOK      = FALSE;
static long  lgEvtCnt      = 0;
static char  cgConfigFile[512];
static char  cgHopo[8];                         /* default home airport    */
static char  cgBrkUjty[24] = "";                         /* default home airport    */
static char  cgFlightUjty[24] = "";                         /* default home airport    */
static char  cgPolUjty[24] = "";                         /* default home airport    */
static char  cgTabEnd[8];                       /* default table extension */
static	int  igStartUpMode = TRACE;
static	int  igRuntimeMode = 0;
static	int  igClockInTolerance = -30*60;
static	int  igClockOutTolerance = 300*60;
static char  cgProcessName[80];                  /* name of executable */
static char  cgErrorString[1024];
static char cgUpdEmpSequence[12] = "Y";
static char cgUpdAbsSequence[12] = "Y";
static char cgTohr[4] = "Y";
static char cgAlertName[124] = "TAA";

static char cgCmdBuf[240];
static char cgSelection[50*1024];
static char cgFldBuf[8196];
static char cgSqlBuf[5*1024];
static char cgTwStart[128];
static char cgTwEnd[128];
static char cgRecvName[128];
static char cgDestName[128];
static int igQueOut;

static int igBchdl = 1900;
static int igAction = 7400;
static int igLoghdl = 7700;
static int igTaa = 7288;
static char cgTaa[24];

static char *pcgStfuList = NULL;
static long lgStfuListLength = 50000;
static long lgStfuListActualLength = 0;

static char cgDrrBuf[2048];
static char cgErrMsg[1024];
static char cgCfgBuffer[256];
static char cgDataArea[5*1024];

static BOOL bgBcActiv = TRUE;
static BOOL bgBreak = FALSE;
static BOOL bgSendBroadcast = TRUE;

static char cgBsrFileId[6] = "FBSR";

static char cgFileBase[256] = "/ceda/exco/SAP/";
static char cgFileNew[24] = "new/";
static char cgFileArc[24] = "arc/";
static char cgFileLog[24] = "log/";
static char cgFileError[24] = "error/";
static char cgTmpPath[1024] = "tmp/";
static char cgExportPath[1024] = "export/";
static char cgInputFilePath[312];
static char cgInputFileName[312];
static char cgLineBuf[1024];
static char cgArcFileName[312];
static char cgRegi[36];
static int  igExpOffset = 0; 
static int  igDailyOffset = -3; 
static int  igExpEnd = 3; 
static int  igDailyEnd = 3; 

static long lgSuccessCount;
static long lgErrorCount;

FILE *pfgInput = NULL;
FILE *pfgError = NULL;
FILE *pfgLog = NULL;

/*DRRSTF_FIELDS "AVFR,AVTO,DRRN,SBFR,SBLU,SBTO,SCOD,SDAY,STFU,PENO,DRRTAB.URNO"*/
struct _DrrStruct
{
	char Avfr[16];
	char Avto[16];
	char Drrn[2];
	char Sbfr[16];
	char Sblu[16];
	char Sbto[16];
	char Scod[32];
	char Sday[16];
	char Stfu[16];
	char Peno[32];
	char Urno[12];
} rgDrrData1,rgDrrData2;

/* static array informations */

#define ARR_NAME_LEN   (18)
#define ARR_FLDLST_LEN (256)

struct _arrayinfo {
	HANDLE   rrArrayHandle;
	char     crArrayTableName[ARR_NAME_LEN+1];
	char     crArrayName[ARR_NAME_LEN+1];
	char     crArrayFieldList[ARR_FLDLST_LEN+1];
	long     lrArrayFieldCnt;
	char   *pcrArrayRowBuf;
	long     lrArrayRowLen;
	long   *plrArrayFieldOfs;
	long   *plrArrayFieldLen;
	HANDLE   rrIdx01Handle;
	char     crIdx01Name[ARR_NAME_LEN+1];
	char     crIdx01FieldList[ARR_FLDLST_LEN+1];
	long     lrIdx01FieldCnt;
	char   *pcrIdx01RowBuf;
	long     lrIdx01RowLen;
	long   *plrIdx01FieldPos;
	long   *plrIdx01FieldOrd;

/*****************/
	HANDLE   rrIdx02Handle;
	char     crIdx02Name[ARR_NAME_LEN+1];
	char     crIdx02FieldList[ARR_FLDLST_LEN+1];
	long     lrIdx02FieldCnt;
	char   *pcrIdx02RowBuf;
	long     lrIdx02RowLen;
	long   *plrIdx02FieldPos;
	long   *plrIdx02FieldOrd;
	/****************************/
	HANDLE   rrIdx03Handle;
	char     crIdx03Name[ARR_NAME_LEN+1];
	char     crIdx03FieldList[ARR_FLDLST_LEN+1];
	long     lrIdx03FieldCnt;
	char   *pcrIdx03RowBuf;
	long     lrIdx03RowLen;
	long   *plrIdx03FieldPos;
	long   *plrIdx03FieldOrd;
};
typedef struct _arrayinfo ARRAYINFO;

static ARRAYINFO rgStfArray;

static int igStfURNO;
static int igStfPENO;


/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
int check_ret1(int ipRc,int ipLine);
static int  InitBsrhdl();
static long GetLengtOfMonth(int ipMonth,int ipYear);

static int SetIndexInfo(ARRAYINFO *prpArrayInfo,char *pcpFieldList,int ilIndex);
static int SetArrayInfo(char *pcpArrayName, char *pcpTableName, 
		char *pcpArrayFieldList,char *pcpAddFields,long *pcpAddFieldLens,
			ARRAYINFO *prpArrayInfo,char *pcpSelection,BOOL bpFill);
static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,
	char *pcpCfgBuffer);

static int GetItemNbr(char *Item, char *Data, char Delim, long Nbr);
static int ItemCnt(char *pcgData, char cpDelim);
static void StringPut(char *pcpDest,char *pcpSource,long lpLen);
static int CheckDate14(char *pcpDate);
static long GetSequence(char *pcpKey);
static int GetFileNames(short spFunc,char *pcpFileId,char *pcpFileDir,char *pcpFileName);

static int SendAnswer(int ipDest, char *pcpAnswer);

extern int get_no_of_items(char *s);
extern int itemCount(char *);

static int	Reset(void);                       /* Reset program          */
static void	Terminate(int ipSleep);            /* Terminate program      */
/*static void	HandleSignal(int); */                /* Handles signals        */
static void	HandleErr(int);                    /* Handles general errors */
static void	HandleQueErr(int);                 /* Handles queuing errors */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/

static void StringTrimRight(char *pcpBuffer);
static int  GetFieldLength(char *pcpHopo, char *pcpTana, char *pcpFina,
							long *pipLen);
static int  GetRowLength(char *pcpHopo, char *pcpTana, char *pcpFieldList,
							long *pipLen);

static int  GetCommand(char *pcpCommand, int *pipCmd);

static int HandleData(EVENT *prpEvent);       /* Handles event data     */
static int SaveIndexInfo (ARRAYINFO *prpArrayInfo);
static int MySaveIndexInfo (ARRAYINFO *prpArrayInfo);

static void IncrementCount(int ipRc);
static void MoveToErrorFile(char *pcpLineBuf);
static int LogError(char *pcpFileName,char *pcpErrorString,BOOL bpAlert);
static int StrToTime(char *pcpTime,time_t *pclTime);
static void  StrAddTime(char *,char*,char *);
static int LocalToUtc(char *pcpTime);
static int MyNewToolsSendSql(int ipRouteID,
		BC_HEAD *prpBchd,
		CMDBLK *prpCmdblk,
		char *pcpSelection,
		char *pcpFields,
		char *pcpData);
static int  TriggerAction(char *pcpTableName,char *pcpFields);
static int  WaitAndCheckQueue(int ipTimeout, int ipModId, ITEM **prpItem);
static int  CheckQueue(int ipModId, ITEM **prpItem);
static int FindItemInArrayList(char *pcpInput, char *pcpPattern,char cpDel,int *ipLine,int *ipCol,int *ipPos);

static int GetDebugLevel(char *, int *);
static int GetRealTimeDiff ( char *pcpLocal1, char *pcpLocal2, time_t *plpDiff );
static int DumpArray(ARRAYINFO *prpArray,char *pcpFields);
static int DoSelect(short spFunction,short *pspCursor,char *pcpTable,char *pcpSelection,char *pcpFields,char *pcpDataArea);
static int DoSingleSelect(char *pcpTable,char *pcpSelection,char *pcpFields,char *pcpDataArea);

static int WriteTrailer(FILE *pflFile);
static int WriteHeader(FILE *pflFile,char *pcpFileId,char *pcpFileName,long lpLineCount);
static int MakeFileName(char *pcpFileId,char *pcpFileName);
static int ExportThreeDayRoster(FILE *pfpFile,char *pcpSday);
static int AddToUrnoList(char *pcpUrno,char *pcpList,long lpListLength,long *plpActualLength);
static int ReloadArrays(BOOL bpForExport);
static int ExportBsr();
static int WriteBsrFile(FILE *pfpFile,char *pcpSday);
static int CreateOneBsrLine(char *pcpLineBuffer,char *pcpGsnm,char *pcpPeno);
static int CheckRegi(char *pcpSurn,char *pcpSday);
static void StringMid(char *pcpDest,char *pcpSource,long lpFrom,long lpLen,int ipConvert);
static int CheckOrgUnit(char *pcpCode,char *pcpNameOfDepartment);
static int GetAbsenceRemark(char *pcpCode,char *pcpRema);
static int DumpRecord(ARRAYINFO *prpArray,char *pcpFields,char *pcpBuf);
static int ConvertDbStringToClient(char *pcpString);
static int ConvertClientStringToDb(char *pcpString);



static int TimeToStr(char *pcpTime,time_t lpTime)
{
  struct tm *_tm;
 
  _tm = (struct tm *)localtime(&lpTime);
  sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
      _tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
      _tm->tm_min,_tm->tm_sec);
  return (_tm->tm_wday == 0 ? 7 : _tm->tm_wday) + '0';
}                                                                                

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
	int	ilRc = RC_SUCCESS;			/* Return code			*/
	int	ilCnt = 0;
	int ilOldDebugLevel;

	/************************
	FILE *prlErrFile = NULL;
	char clErrFile[512];
	************************/

	INITIALIZE;			/* General initialization	*/

	/****************************************************************************
	sprintf(&clErrFile[0],"%s/%s%5.5d.err",getenv("DBG_PATH"),mod_name,getpid());
	prlErrFile = freopen(&clErrFile[0],"w",stderr);
	fprintf(stderr,"HoHoHo\n");fflush(stderr);
	dbg(TRACE,"MAIN: error file <%s>",&clErrFile[0]);
	****************************************************************************/


	strcpy(cgProcessName,argv[0]);

	dbg(TRACE,"MAIN: version <%s>",mks_version);

	/* Attach to the MIKE queues */
	do
	{
		ilRc = init_que();
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		}/* end of if */
	}while((ilCnt < 10) && (ilRc != RC_SUCCESS));

	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
		sleep(60);
		exit(1);
	}else{
		dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
	}/* end of if */

	do
	{
		ilRc = init_db();
		if (ilRc != RC_SUCCESS)
		{
			check_ret(ilRc);
			dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		} /* end of if */
	} while((ilCnt < 10) && (ilRc != RC_SUCCESS));

	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
		sleep(60);
		exit(2);
	}else{
		dbg(TRACE,"MAIN: init_db() OK!");
	} /* end of if */

	/* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */

	sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
	dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
	ilRc = TransferFile(cgConfigFile);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */

	sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
	dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
	ilRc = TransferFile(cgConfigFile);
	if(ilRc != RC_SUCCESS)
	{ 
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */

	ilRc = SendRemoteShutdown(mod_id);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
	} /* end of if */

	if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
	{
		dbg(DEBUG,"MAIN: waiting for status switch ...");
		HandleQueues();
	}/* end of if */

	if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
	{
		dbg(TRACE,"MAIN: initializing ...");
		if(igInitOK == FALSE)
		{
			ilRc = InitBsrhdl();
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitBsrhdl: init failed!");
			} /* end of if */
		}/* end of if */
	} else {
		Terminate(30);
	}/* end of if */

/***
	dbg(TRACE,"MAIN: initializing OK sleep(60) ...");
	sleep(60);
***/

	dbg(TRACE,"=====================");
	dbg(TRACE,"MAIN: initializing OK");
	dbg(TRACE,"=====================");
	
	debug_level = igRuntimeMode;

	for(;;)
	{
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;
				
		if( ilRc == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRc != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRc);
			} /* fi */
			
			lgEvtCnt++;

			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				break;	
			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				/* CloseConnection(); */
				HandleQueues();
				break;	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate(1);
				break;	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				break;	
			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;
			case	SHUTDOWN	:
				/* process shutdown - maybe from uutil */
				Terminate(1);
				break;
					
			case	RESET		:
				ilRc = Reset();
				break;
					
			case	EVENT_DATA	:
				if((ctrl_sta == HSB_STANDALONE) || 
					(ctrl_sta == HSB_ACTIVE) || 
					(ctrl_sta == HSB_ACT_TO_SBY))
				{
					ilRc = HandleData(prgEvent);
					if(ilRc != RC_SUCCESS)
					{
						HandleErr(ilRc);
					}/* end of if */
				}else{
					dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
				}/* end of if */
				break;
				
			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;
			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;
			default			:
				dbg(TRACE,"MAIN: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		} /* end else */
		
	} /* end for */
	
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/

static int FindItemInArrayList(char *pcpInput, char *pcpPattern,char cpDel,int *ipLine,int *ipCol,int *ipPos)
{
	int ilRc = RC_SUCCESS;

	ilRc = FindItemInList(pcpInput,pcpPattern,cpDel,ipLine,ipCol,ipPos);
	*ipLine = (*ipLine) -1;
	
	return ilRc;
}



static int InitFieldIndex()
{
	int ilRc = RC_SUCCESS;
	int ilCol,ilPos;
	int ilDBFields;

	/*FindItemInArrayList(DEM_FIELDS,"ALID",',',&igDemALID,&ilCol,&ilPos);*/

	FindItemInArrayList(cgStfFields,"URNO",',',&igStfURNO,&ilCol,&ilPos);
	FindItemInArrayList(cgStfFields,"PENO",',',&igStfPENO,&ilCol,&ilPos);

/******************************************************************************/


	return ilRc;
}


static int ReadConfigLine(char *pcpSection,char *pcpKeyword,
	char *pcpCfgBuffer)
{
	int ilRc = RC_SUCCESS;

	char clSection[124];
	char clKeyword[124];
	char clBuffer[256];
	char *pclEquals;

	strcpy(clSection,pcpSection);
	strcpy(clKeyword,pcpKeyword);

	ilRc = iGetConfigLine(cgConfigFile,clSection,clKeyword,clBuffer);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
	} 
	else
	{
			pclEquals = strchr(clBuffer,'=');
			if (pclEquals != NULL)
			{
				strcpy(pcpCfgBuffer,&pclEquals[2]);
			}
			else
			{
				strcpy(pcpCfgBuffer,clBuffer);
			}
			StringTrimRight(pcpCfgBuffer);
			dbg(DEBUG,"Config Line <%s>,<%s>:<%s> found in %s",
					&clSection[0],&clKeyword[0],pcpCfgBuffer,cgConfigFile);
	}/* end of if */
	return ilRc;
}

static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,
	char *pcpCfgBuffer)
{
	int ilRc = RC_SUCCESS;

	char clSection[124];
	char clKeyword[124];

	strcpy(clSection,pcpSection);
	strcpy(clKeyword,pcpKeyword);

	ilRc = iGetConfigEntry(cgConfigFile,clSection,clKeyword,
			CFG_STRING,pcpCfgBuffer);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
	} 
	else
	{
			dbg(DEBUG,"Config Entry <%s>,<%s>:<%s> found in %s",
					&clSection[0],&clKeyword[0],pcpCfgBuffer,cgConfigFile);
	}/* end of if */
	return ilRc;
}

static int InitBsrhdl()
{
	int  ilRc = RC_SUCCESS;			/* Return code */
  char clSection[64];
  char clKeyword[64];
  char clNow[64];
	time_t tlNow;
	int ilOldDebugLevel;
	long pclAddFieldLens[12];
	char pclAddFields[256];
	char pclSqlBuf[2560];
	char pclSelection[1024];
 	short slCursor;
	short slSqlFunc;
	char  clBreak[24];
	int ilLc;
	int ilTmpModId;
	char clTmpString[124];

	tlNow = time(NULL);
	tlNow -= tlNow % 86400;

	TimeToStr(clNow,tlNow);

	dbg(TRACE,"%05d:Now = <%s>",__LINE__,clNow);

	memset(cgFunctionDummy,' ',40);
	cgFunctionDummy[40] = '\0';

	/* now reading from configfile or from database */


	if ((ilTmpModId = tool_get_q_id("bchdl")) == RC_SUCCESS)
	{
		igBchdl = ilTmpModId;
		dbg(TRACE,"InitBsrhdl: Mod-Id for <bchdl> set to %d",igBchdl);
	}
	else
	{
		dbg(TRACE,"InitBsrhdl: Mod-Id for <bchdl> not found, set to default %d",igBchdl);
	}


	if ((ilTmpModId = tool_get_q_id("action")) == RC_SUCCESS)
	{
		igAction = ilTmpModId;
		dbg(TRACE,"InitBsrhdl: Mod-Id for <action> set to %d",igAction);
	}
	else
	{
		dbg(TRACE,"InitBsrhdl: Mod-Id for <action> not found, set to default %d",igAction);
	}


	if ((ilTmpModId = tool_get_q_id("loghdl")) == RC_SUCCESS)
	{
		igLoghdl = ilTmpModId;
		dbg(TRACE,"InitBsrhdl: Mod-Id for <loghdl> set to %d",igLoghdl);
	}
	else
	{
		dbg(TRACE,"InitBsrhdl: Mod-Id for <loghdl> not found, set to default %d",igLoghdl);
	}


	if(ilRc == RC_SUCCESS)
	{
		/* read HomeAirPort from SGS.TAB */
		memset((void*)&cgHopo[0], 0x00, 8);

		sprintf(&clSection[0],"SYS");
		sprintf(&clKeyword[0],"HOMEAP");

		ilRc = tool_search_exco_data(&clSection[0], &clKeyword[0], &cgHopo[0]);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"<InitBsrhdl> EXTAB,%s,%s not found in SGS.TAB",&clSection[0], &clKeyword[0]);
			Terminate(30);
		} else {
			dbg(TRACE,"<InitBsrhdl> home airport    <%s>",&cgHopo[0]);
		}/* end of if */
	}/* end of if */

	
	if(ilRc == RC_SUCCESS)
	{
		sprintf(&clSection[0],"ALL");
		sprintf(&clKeyword[0],"TABEND");

		ilRc = tool_search_exco_data(&clSection[0], &clKeyword[0], &cgTabEnd[0]);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"<InitBsrhdl> EXTAB,%s,%s not found in SGS.TAB",&clSection[0], &clKeyword[0]);
			Terminate(30);
		} else {
			dbg(TRACE,"<InitBsrhdl> table extension <%s>",&cgTabEnd[0]);
		}/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		 GetDebugLevel("STARTUP_MODE", &igStartUpMode);
		 GetDebugLevel("RUNTIME_MODE", &igRuntimeMode);
		 
		 debug_level = igStartUpMode;


		ilRc = ReadConfigEntry("FILEIDS","BSR",cgBsrFileId);
		if(ilRc == RC_SUCCESS)
		{
			dbg(TRACE,"FILEID BSR set to <%s>",cgBsrFileId);
		}
		else
		{
			dbg(TRACE,"FILEID BSR not found, set to <%s>",cgBsrFileId);
			ilRc = RC_SUCCESS;
		}

		ilRc = ReadConfigEntry("DIRECTORIES","FILEBASE",cgFileBase);
		if(ilRc == RC_SUCCESS)
		{
			dbg(TRACE,"FILEID FILEBASE set to <%s>",cgFileBase);
		}
		else
		{
			dbg(TRACE,"FILEID FILEBASE not found, set to <%s>",cgFileBase);
			ilRc = RC_SUCCESS;
		}

		ilRc = ReadConfigEntry("DIRECTORIES","LOG",cgFileLog);
		if(ilRc == RC_SUCCESS)
		{
			dbg(TRACE,"Directory for LOG files set to <%s>",cgFileLog);
		}
		else
		{
			dbg(TRACE,"Directory for LOG files not found, set to <%s>",cgFileLog);
			ilRc = RC_SUCCESS;
		}
		ilRc = ReadConfigEntry("DIRECTORIES","ERROR",cgFileError);
		if(ilRc == RC_SUCCESS)
		{
			dbg(TRACE,"Directory for ERROR files set to <%s>",cgFileError);
		}
		else
		{
			dbg(TRACE,"Directory for ERROR files not found, set to <%s>",cgFileError);
			ilRc = RC_SUCCESS;
		}
		ilRc = ReadConfigEntry("DIRECTORIES","TMP",cgTmpPath);
		if(ilRc == RC_SUCCESS)
		{
			dbg(TRACE,"Directory for temporary files set to <%s>",cgTmpPath);
		}
		else
		{
			dbg(TRACE,"Directory for temporary files not found, set to <%s>",cgTmpPath);
			ilRc = RC_SUCCESS;
		}
		ilRc = ReadConfigEntry("DIRECTORIES","EXPORT",cgExportPath);
		if(ilRc == RC_SUCCESS)
		{
			dbg(TRACE,"Directory for EXPORT files set to <%s>",cgExportPath);
		}
		else
		{
			dbg(TRACE,"Directory for temporary files not found, set to <%s>",cgExportPath);
			ilRc = RC_SUCCESS;
		}

		ilRc = ReadConfigEntry("MAIN","SHIFTINDICATOR",cgRegi);
		if(ilRc == RC_SUCCESS)
		{
			dbg(TRACE,"Shift indicator to send set to <%s>",cgRegi);
		}
		else
		{
			dbg(TRACE,"Shift indicator to send not found, set to <%s>",cgRegi);
			ilRc = RC_SUCCESS;
		}


		ilRc = ReadConfigEntry("MAIN","EXPORTDAYOFFSET",clTmpString);
		if(ilRc == RC_SUCCESS)
		{

			igExpOffset = atoi(clTmpString);
			dbg(DEBUG,"Send basic shifts roster for <%s> %d ",clTmpString,igExpOffset);
		}
		else
		{
			dbg(TRACE,"EXPORTDAYOFFSET not found set to %d",igExpOffset);
			ilRc = RC_SUCCESS;
		}

		ilRc = ReadConfigLine("SYSTEM","ALERT_NAME",cgAlertName);
		if(ilRc == RC_SUCCESS)
		{
			dbg(TRACE,"Alert Name set to <%s>",cgAlertName);
		}
		else
		{
			strcpy(cgAlertName,"T&A");
			dbg(TRACE,"Alert Name not found, set to <%s>",
						cgAlertName);
			ilRc = RC_SUCCESS;
		}
		ilRc = ReadConfigLine("SYSTEM","TOHR",cgTohr);
		if(ilRc == RC_SUCCESS)
		{
			dbg(TRACE,"TOHR set to <%s>",cgTohr);
		}
		else
		{
			strcpy(cgTohr,"Y");
			dbg(TRACE,"TOHR not found, set to <%s>",cgTohr);
			ilRc = RC_SUCCESS;
		}
	}


	if(ilRc == RC_SUCCESS)
	{
		ilRc = CEDAArrayInitialize(20,20);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitBsrhdl: CEDAArrayInitialize failed <%d>",ilRc);
		}/* end of if */
	}/* end of if */


	if(ilRc == RC_SUCCESS)
	{

		strcpy(cgStfFields,STF_FIELDS);
		*pclSelection = '\0';
		dbg(TRACE,"InitBsrhdl: SetArrayInfo STFTAB array");
		ilRc = SetArrayInfo("STFTAB","STFTAB",cgStfFields,
				NULL,NULL,&rgStfArray,pclSelection,FALSE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitBsrhdl: SetArrayInfo STFTAB failed <%d>",ilRc);
		}
		else
		{
			dbg(DEBUG,"InitBsrhdl: SetArrayInfo STFTAB OK");
			dbg(DEBUG,"<%s> %d",rgStfArray.crArrayName,rgStfArray.rrArrayHandle);
			strcpy(rgStfArray.crIdx01Name,"STFTAB_URNO");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgStfArray.rrArrayHandle,
				rgStfArray.crArrayName,
				&rgStfArray.rrIdx01Handle,
				rgStfArray.crIdx01Name,"URNO");
				strcpy(rgStfArray.crIdx01FieldList,"URNO");
			SetIndexInfo(&rgStfArray,"URNO",1);
		}
	}


	if(ilRc == RC_SUCCESS)
	{
		ilRc = CEDAArrayReadAllHeader(outp);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitBsrhdl: CEDAArrayReadAllHeader failed <%d>",ilRc);
		}else{
			dbg(TRACE,"InitBsrhdl: CEDAArrayReadAllHeader OK");
		}/* end of if */
	}/* end of if */

	
	if(ilRc == RC_SUCCESS)
	{
		ilRc = InitFieldIndex();
	}


	if(ilRc == RC_SUCCESS)
	{
		igInitOK = TRUE;
		dbg(TRACE,"InitBsrhdl: InitFieldIndex OK");
	}
	else
	{
		dbg(TRACE,"InitBsrhdl: InitFieldIndex failed");
	}

	dbg(TRACE,"Debug Level is %d",debug_level);
	return (ilRc);	
} /* end of initialize */


static int SetIndexInfo(ARRAYINFO *prpArrayInfo,char *pcpFieldList,int ilIndex)
{
	int	ilRc = RC_SUCCESS;				/* Return code */
  int ilLc;
	char **ppclIdxRowBuf;
	long *pllIdxRowLen;

	dbg(DEBUG,"%05d:SetIndexInfo:",__LINE__);
	switch(ilIndex)
	{
		case 1:
			ppclIdxRowBuf = &prpArrayInfo->pcrIdx01RowBuf;
			pllIdxRowLen = &prpArrayInfo->lrIdx01RowLen;
			break;
		case 2:
			ppclIdxRowBuf = &prpArrayInfo->pcrIdx02RowBuf;
			pllIdxRowLen = &prpArrayInfo->lrIdx02RowLen;
			break;
		case 3:
			ppclIdxRowBuf = &prpArrayInfo->pcrIdx03RowBuf;
			pllIdxRowLen = &prpArrayInfo->lrIdx03RowLen;
			break;
		default:
			ilRc = RC_FAIL;
			dbg(DEBUG,"SetIndexInfo:ivalid index %d",ilIndex);
	}

	if (ilRc == RC_SUCCESS)
	{
  ilRc = GetRowLength(cgHopo,prpArrayInfo->crArrayTableName,pcpFieldList,
							pllIdxRowLen);
	*ppclIdxRowBuf = (char *) calloc(1,(*pllIdxRowLen+10));
	if(*ppclIdxRowBuf == NULL)
  {
    ilRc = RC_FAIL;
    dbg(TRACE,"SetArrayInfo: calloc failed");
   }/* end of if */
	}

	return ilRc;
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
static int SetArrayInfo(char *pcpArrayName, char *pcpTableName, char *pcpArrayFieldList,char *pcpAddFields,long *pcpAddFieldLens,ARRAYINFO *prpArrayInfo,
	char *pcpSelection,BOOL bpFill)
{
	int	ilRc = RC_SUCCESS;				/* Return code */
  int ilLc;

	if(prpArrayInfo == NULL)
	{
		dbg(TRACE,"SetArrayInfo: invalid last parameter : null pointer not valid");
		ilRc = RC_FAIL;
	}else
	{
		memset(prpArrayInfo,0x00,sizeof(prpArrayInfo));

		prpArrayInfo->rrArrayHandle = -1;
		prpArrayInfo->rrIdx01Handle = -1;

		if(pcpArrayName != NULL)
		{
			if(strlen(pcpArrayName) <= ARR_NAME_LEN)
			{
				strcpy(prpArrayInfo->crArrayName,pcpArrayName);
			}/* end of if */
		}/* end of if */
	}/* end of if */

	strcpy(prpArrayInfo->crArrayTableName,pcpTableName);
	if(ilRc == RC_SUCCESS)
	{
		prpArrayInfo->lrArrayFieldCnt = get_no_of_items(pcpArrayFieldList);
		if (pcpAddFields != NULL)
		{
			prpArrayInfo->lrArrayFieldCnt += get_no_of_items(pcpAddFields);
		}

		prpArrayInfo->plrArrayFieldOfs = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
		if(prpArrayInfo->plrArrayFieldOfs == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrArrayFieldOfs calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		prpArrayInfo->plrArrayFieldLen = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
		if(prpArrayInfo->plrArrayFieldLen == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrArrayFieldLen calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}else{
			*(prpArrayInfo->plrArrayFieldLen) = -1;
		}/* end of if */
	}/* end of if */

   	if(ilRc == RC_SUCCESS)
   	{
       	ilRc = GetRowLength(&cgHopo[0],pcpTableName,pcpArrayFieldList,
							&(prpArrayInfo->lrArrayRowLen));
       	if(ilRc != RC_SUCCESS)
       	{
           	dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpArrayName,pcpArrayFieldList);
      	}/* end of if */
   	}/* end of if */

   	if(ilRc == RC_SUCCESS)
   	{
		prpArrayInfo->lrArrayRowLen+=100;
		prpArrayInfo->pcrArrayRowBuf = (char *) calloc(1,(prpArrayInfo->lrArrayRowLen+10));
   		if(prpArrayInfo->pcrArrayRowBuf == NULL)
   		{
      			ilRc = RC_FAIL;
      			dbg(TRACE,"SetArrayInfo: calloc failed");
   		}/* end of if */
   	}/* end of if */

   	if(ilRc == RC_SUCCESS)
   	{
		prpArrayInfo->pcrIdx01RowBuf = (char *) calloc(1,(prpArrayInfo->lrIdx01RowLen+1));
   		if(prpArrayInfo->pcrIdx01RowBuf == NULL)
   		{
      			ilRc = RC_FAIL;
      			dbg(TRACE,"SetArrayInfo: calloc failed");
   		}/* end of if */
   	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		dbg(DEBUG,"CEDAArrayCreate follows Array <%s> Table <%s>",pcpArrayName,pcpTableName);
		ilRc = CEDAArrayCreate(&(prpArrayInfo->rrArrayHandle),
				pcpArrayName,pcpTableName,pcpSelection,pcpAddFields,
				pcpAddFieldLens,pcpArrayFieldList,
				&(prpArrayInfo->plrArrayFieldLen[0]),
				&(prpArrayInfo->plrArrayFieldOfs[0]));

		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"SetArrayInfo: CEDAArrayInitialize failed <%d>",ilRc);
		}/* end of if */
	}/* end of if */

		if(pcpArrayFieldList != NULL)
		{
			if(strlen(pcpArrayFieldList) <= ARR_FLDLST_LEN)
			{
				strcpy(prpArrayInfo->crArrayFieldList,pcpArrayFieldList);
			}/* end of if */
		}/* end of if */
		if(pcpAddFields != NULL)
		{
			if(strlen(pcpAddFields) + 
				strlen(prpArrayInfo->crArrayFieldList) <= (size_t)ARR_FLDLST_LEN)
			{
				strcat(prpArrayInfo->crArrayFieldList,",");
				strcat(prpArrayInfo->crArrayFieldList,pcpAddFields);
				dbg(DEBUG,"<%s>",prpArrayInfo->crArrayFieldList);
			}/* end of if */
		}/* end of if */


	if(ilRc == RC_SUCCESS && bpFill)
	{
		ilRc = CEDAArrayFill(&(prpArrayInfo->rrArrayHandle),pcpArrayName,NULL);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"SetArrayInfo: CEDAArrayFill failed <%d>",ilRc);
		}/* end of if */
	}/* end of if */

   for(ilLc = 0; ilLc < prpArrayInfo->lrArrayFieldCnt; ilLc++)
   {
			char	pclFieldName[25];

			*pclFieldName  = '\0';

			GetDataItem(pclFieldName,prpArrayInfo->crArrayFieldList,
						ilLc+1,',',"","\0\0");
     dbg(DEBUG,"%02d %-10s Offset: %d",
				ilLc,pclFieldName,prpArrayInfo->plrArrayFieldOfs[ilLc]);
   }   

	return(ilRc);
}



/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
	int	ilRc = RC_SUCCESS;				/* Return code */
	
	dbg(TRACE,"Reset: now resetting");
	
	return ilRc;
	
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
	/* unset SIGCHLD ! DB-Child will terminate ! */

	dbg(TRACE,"Terminate: now DB logoff ...");

	signal(SIGCHLD,SIG_IGN);

	logoff();

	dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);


	dbg(TRACE,"Terminate: now leaving ...");

	fclose(outp);

	sleep(ipSleep < 1 ? 1 : ipSleep);
	
	exit(0);
	
} /* end of Terminate */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
	dbg(TRACE,"HandleErr: <%d> : unknown ERROR",pipErr);

	return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
	switch(pipErr) {
	case	QUE_E_FUNC	:	/* Unknown function */
		dbg(TRACE,"<%d> : unknown function",pipErr);
		break;
	case	QUE_E_MEMORY	:	/* Malloc reports no memory */
		dbg(TRACE,"<%d> : malloc failed",pipErr);
		break;
	case	QUE_E_SEND	:	/* Error using msgsnd */
		dbg(TRACE,"<%d> : msgsnd failed",pipErr);
		break;
	case	QUE_E_GET	:	/* Error using msgrcv */
		dbg(TRACE,"<%d> : msgrcv failed",pipErr);
		break;
	case	QUE_E_EXISTS	:
		dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
		break;
	case	QUE_E_NOFIND	:
		dbg(TRACE,"<%d> : route not found ",pipErr);
		break;
	case	QUE_E_ACKUNEX	:
		dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
		break;
	case	QUE_E_STATUS	:
		dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
		break;
	case	QUE_E_INACTIVE	:
		dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
		break;
	case	QUE_E_MISACK	:
		dbg(TRACE,"<%d> : missing ack ",pipErr);
		break;
	case	QUE_E_NOQUEUES	:
		dbg(TRACE,"<%d> : queue does not exist",pipErr);
		break;
	case	QUE_E_RESP	:	/* No response on CREATE */
		dbg(TRACE,"<%d> : no response on create",pipErr);
		break;
	case	QUE_E_FULL	:
		dbg(TRACE,"<%d> : too many route destinations",pipErr);
		break;
	case	QUE_E_NOMSG	:	/* No message on queue */
		dbg(TRACE,"<%d> : no messages on queue",pipErr);
		break;
	case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
		dbg(TRACE,"<%d> : invalid originator=0",pipErr);
		break;
	case	QUE_E_NOINIT	:	/* Queues is not initialized*/
		dbg(TRACE,"<%d> : queues are not initialized",pipErr);
		break;
	case	QUE_E_ITOBIG	:
		dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
		break;
	case	QUE_E_BUFSIZ	:
		dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
		break;
	default			:	/* Unknown queue error */
		dbg(TRACE,"<%d> : unknown error",pipErr);
		break;
	} /* end switch */
         
	return;

} /* end of HandleQueErr */


/******************************************************************************/
/******************************************************************************/
static int GetRowLength(char *pcpHopo, char *pcpTana, char *pcpFieldList,
						long *plpLen)
{
	int	 ilRc = RC_SUCCESS;			/* Return code */
	int  ilNoOfItems = 0;
	int  ilLoop = 0;
	long llFldLen = 0;
	long llRowLen = 0;
	char clFina[8];
	
	ilNoOfItems = get_no_of_items(pcpFieldList);
	ilLoop = 1;
	do
	{
		ilRc = get_real_item(&clFina[0],pcpFieldList,ilLoop);
		if(ilRc > 0)
		{
			ilRc = GetFieldLength(pcpHopo,pcpTana,&clFina[0],&llFldLen);
			if(ilRc == RC_SUCCESS)
			{
				llRowLen++;
				llRowLen += llFldLen;
			}/* end of if */
		}/* end of if */
		ilLoop++;
	}while((ilRc == RC_SUCCESS) && (ilLoop <= ilNoOfItems));

	if(ilRc == RC_SUCCESS)
	{
		*plpLen = llRowLen;
	}/* end of if */

	return(ilRc);
	
} /* end of GetRowLength */


/******************************************************************************/
/******************************************************************************/
static int GetFieldLength(char *pcpHopo, char *pcpTana, char *pcpFina,
							long *plpLen)
{
	int  ilRc = RC_SUCCESS;			/* Return code */
	int  ilCnt = 0;
	char clTaFi[32];
	char clFele[16];
	char clFldLst[16];

	ilCnt = 1;
	sprintf(&clTaFi[0],"%s,%s",pcpTana,pcpFina);
	sprintf(&clFldLst[0],"TANA,FINA"); /* wird von syslib zerst�rt */


	ilRc = syslibSearchSystabData(&cgTabEnd[0],&clFldLst[0],&clTaFi[0],"FELE",&clFele[0],&ilCnt,"");
	switch (ilRc)
	{
	case RC_SUCCESS :
		*plpLen = atoi(&clFele[0]);
		/* dbg(DEBUG,"GetFieldLength: <%s,%s,%s> <%d>",pcpHopo,pcpTana,pcpFina,*plpLen);*/
		break;

	case RC_NOT_FOUND :
		dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_NOT_FOUND <%s> <%s>",
				ilRc,&clTaFi[0],pcpFina);
		break;

	case RC_FAIL :
		dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_FAIL",ilRc);
		break;

	default :
		dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> unexpected",ilRc);
		break;
	}/* end of switch */

	return(ilRc);
	
} /* end of GetFieldLength */


/******************************************************************************/
/******************************************************************************/
static int GetCommand(char *pcpCommand, int *pipCmd)
{
	int	ilRc = RC_SUCCESS;			/* Return code */
	int ilLoop = 0;
	char clCommand[48];
	
	memset(&clCommand[0],0x00,8);

	ilRc = get_real_item(&clCommand[0],pcpCommand,1);
	if(ilRc > 0)
	{
		ilRc = RC_FAIL;
	}/* end of if */

	while((ilRc != 0) && (prgCmdTxt[ilLoop] != NULL))
	{
		ilRc = strcmp(&clCommand[0],prgCmdTxt[ilLoop]);
		ilLoop++;
	}/* end of while */

	if(ilRc == 0)
	{
		ilLoop--;
		dbg(DEBUG,"GetCommand: <%s> <%d>",prgCmdTxt[ilLoop],rgCmdDef[ilLoop]);
		*pipCmd = rgCmdDef[ilLoop];
	}else{
		dbg(TRACE,"GetCommand: <%s> is not valid",&clCommand[0]);
		ilRc = RC_FAIL;
	}/* end of if */

	return(ilRc);
	
} /* end of GetCommand */



static int ExportBsr()
{
	int ilRc = RC_SUCCESS;
	char clFileName[34];
	char clFilePath[234];
	char clSelection[234];
	char clExportPath[234];
	long llAction;
	FILE *pflFile = NULL;
	char clTimeStamp[24];
	int ilRecordCount = 0;
	int ilLc;


	MakeFileName(cgBsrFileId,clFileName);
	sprintf(clFilePath,"%s%s%s",cgFileBase,cgTmpPath,clFileName);
	dbg(DEBUG,"%05d:  <%s> <%s> <%s> ",__LINE__,cgFileBase,cgTmpPath,clFileName);

	dbg(TRACE,"Export File <%s> will be created",clFilePath);
	errno = 0;
	pflFile = fopen(clFilePath,"wt");
	errno = 0;
	if (pflFile == NULL)
	{
		dbg(TRACE,"Export File <%s> not created, reason: %d %s",
			clFilePath,errno,strerror(errno));
		ilRc = RC_FAIL;
	}
	else
	{
		int ilExpEnd;

		errno = 0;
		dbg(DEBUG,"Export file created");
		WriteHeader(pflFile,cgBsrFileId,clFileName,0);


		sprintf(clSelection,"WHERE TOHR = '%s'",cgTohr);
		CEDAArrayRefill(&rgStfArray.rrArrayHandle,rgStfArray.crArrayName,
			clSelection,NULL,llAction);


		GetServerTimeStamp("LOC", 1,0,clTimeStamp);
		AddSecondsToCEDATime(clTimeStamp,igExpOffset*86400,1);
		clTimeStamp[8] = '\0';
		ilRecordCount += WriteBsrFile(pflFile,clTimeStamp);

		dbg(TRACE,"ExportBsr: %d lines exported",ilRecordCount);
		WriteTrailer(pflFile);
		rewind(pflFile);
		WriteHeader(pflFile,cgBsrFileId,clFileName,ilRecordCount);
		sprintf(clExportPath,"%s%s%s",cgFileBase,cgExportPath,clFileName);
		fclose(pflFile);
	  dbg(TRACE,"ExportBsr: export file will be renamed from <%s> to <%s>",
				clFilePath,clExportPath);
		rename(clFilePath,clExportPath);
	}
  return ilRc;
}

static int CreateOneBsrLine(char *pcpLineBuffer,char *pcpGsnm,char *pcpPeno)
{
	int ilRc = RC_SUCCESS;
	char clLineBuffer[124];
	char clKey[24];
	int ilBytes = 0;
	long llRow;
	char clTmpStr[204];
	char *pclRowBuf;
	char *pclLineBuffer;

	ConvertDbStringToClient(pcpGsnm);

	pclLineBuffer = pcpLineBuffer;
	*pclLineBuffer = '\0';
	
	StringPut(pclLineBuffer,"20",2); /* 20 Record identifier **/
	pclLineBuffer += 2;
	ilBytes += 2;
	/* 01 Employee Number */
	StringPut(pclLineBuffer,pcpPeno,8);  
	pclLineBuffer += 8;
	ilBytes += 8;
/* 02 Basic shift roster name */
	StringPut(pclLineBuffer,pcpGsnm,32);
	pclLineBuffer += 32;
	ilBytes += 32;

	StringPut(pclLineBuffer,"\r\n",2);
	ilBytes += 2;
	
	return(ilBytes);
}



static int WriteTrailer(FILE *pflFile)
{
	int ilRc = RC_SUCCESS;
	char clTrailer[24];

	int ilBytes;

	sprintf(clTrailer,"99\r\n");
	ilBytes = strlen(clTrailer);
	errno = 0;
	if(fwrite(clTrailer,sizeof(char),ilBytes,pflFile) != ilBytes)
	{
		dbg(TRACE,"could not write Trailer: <%s>, reason %d <%s>",
				clTrailer,errno,strerror(errno));
		ilRc = RC_FAIL;
	}
	return ilRc;
}

static int WriteHeader(FILE *pflFile,char *pcpFileId,char *pcpFileName,long lpLineCount)
{
	int ilRc = RC_SUCCESS;
	char clTimeStamp[24];
	char clHeader[300];
	int ilBytes;
	
	GetServerTimeStamp("LOC", 1,0,clTimeStamp);
	clTimeStamp[8] = '\0';
	sprintf(clHeader,"00%s%s%s%010d\r\n",pcpFileId,pcpFileName,clTimeStamp,lpLineCount);
	dbg(DEBUG,"WriteHeader: <%s> <%s> <%s> %ld",
					clHeader,pcpFileName,clTimeStamp,lpLineCount);
	ilBytes = strlen(clHeader);
	errno = 0;
	if(fwrite(clHeader,sizeof(char),ilBytes,pflFile) != ilBytes)
	{
		dbg(TRACE,"could not write header: <%s>, reason %d <%s>",
				clHeader,errno,strerror(errno));
		ilRc = RC_FAIL;
	}
	return ilRc;
}


static int MakeFileName(char *pcpFileId,char *pcpFileName)
{
	int ilRc = RC_SUCCESS;
	char clTimeStamp[24];
	
	GetServerTimeStamp("LOC", 1,0,clTimeStamp);
	clTimeStamp[12] = '\0';

	sprintf(pcpFileName,"%s%s.dat",pcpFileId,clTimeStamp);

	return ilRc;
}


static void StringTrimRight(char *pcpBuffer)
{
	if (*pcpBuffer != '\0')
	{
		char *pclBlank = &pcpBuffer[strlen(pcpBuffer)-1];
		while(isspace(*pclBlank) && pclBlank != pcpBuffer)
		{
			*pclBlank = '\0';
			pclBlank--;
		}
	}
}

static void StringPut(char *pcpDest,char *pcpSource,long lpLen)
{

	int i,ilSourceLen;

	ilSourceLen = strlen(pcpSource);

	for(i = 0; i < lpLen; i++)
	{
		if (i < ilSourceLen)
		{
			pcpDest[i] = pcpSource[i];
		}
		else
		{
			pcpDest[i] = ' ';
		}
	}
}

static void StringMid(char *pcpDest,char *pcpSource,long lpFrom,long lpLen,int ipConvert)
{
	pcpDest = strncpy(pcpDest,&pcpSource[lpFrom],lpLen);
	pcpDest[lpLen] = '\0';
	switch(ipConvert)
	{
	case CLIENTTODB:
		ConvertClientStringToDb(pcpDest);
		break;
	case DBTOCLIENT:
		ConvertDbStringToClient(pcpDest);
		break;
	default:
			/* do nothing */
			;
	}
}

static char *ItemPut(char *pcpDest,char *pcpSource,char cpDelimiter)
{
	strcpy(pcpDest,pcpSource);
	pcpDest += strlen(pcpDest);
	if (cpDelimiter != '\0')
	{
		*pcpDest = cpDelimiter;
		pcpDest++;
	}
	return (pcpDest);
}

static int LogError(char *pcpFileName,char *pcpErrorString,BOOL bpAlert)
{
	int ilRc = RC_SUCCESS;

	if (pfgLog == NULL)
	{
		char clLogFileName[512];

		sprintf(clLogFileName,"%s%staahdl%ld",cgFileBase,cgFileLog,getpid());
		pfgLog = fopen(clLogFileName,"a");
		if (pfgLog != NULL)
		{
			dbg(DEBUG,"Logfile <%s> opened",clLogFileName);
		}
		else
		{
			dbg(DEBUG,"Logfile <%s> not opened",clLogFileName);
		}
	}
	if (pfgLog != NULL)
	{
		char clTimeStamp[24];

		GetServerTimeStamp("LOC", 1,0,clTimeStamp);
		fprintf(pfgLog,"%s: Error: <%s> Line: <%s>\r\n",
				clTimeStamp,pcpErrorString,cgLineBuf);
		dbg(DEBUG,"%s <%s>",pcpErrorString,cgLineBuf);
		fclose(pfgLog);
		pfgLog = NULL;
		/*
		AddAlert(cgAlertName,"R",clTimeStamp,"D",pcpErrorString,TRUE,TRUE,
				cgDestName,cgRecvName,cgTwStart,cgTwEnd);
				**/
		if (bpAlert == TRUE)
		{
			AddAlert2(cgAlertName,"R"," "," ",pcpErrorString,cgLineBuf,TRUE,FALSE,
				cgDestName,cgRecvName,cgTwStart,cgTwEnd);
		}
	}
	return ilRc;
}

static int DoSelect(short spFunction,short *pspCursor,char *pcpTable,char *pcpSelection,char *pcpFields,char *pcpDataArea)
{
	int ilRc = RC_SUCCESS;
	static char clSqlBuf[4096];

	char *pclDataArea;
	int ilLc,ilItemCount;

	sprintf(clSqlBuf,"SELECT %s FROM %s %s",pcpFields,pcpTable,pcpSelection);

	dbg(DEBUG,"DoSelect: clSqlBuf <%s>",clSqlBuf);
  *pcpDataArea = '\0';
	ilRc = sql_if(spFunction,pspCursor,clSqlBuf,pcpDataArea);
	if(ilRc != RC_SUCCESS && ilRc != NOTFOUND)
	{
		/* there must be a database error */
		get_ora_err(ilRc, cgErrMsg);
		dbg(TRACE,"DoSelect failed RC=%d <%s>",ilRc,cgErrMsg);
		dbg(TRACE,"SQL: <%s>",clSqlBuf);
	}
	if (ilRc != RC_SUCCESS)
	{
		close_my_cursor(pspCursor);
		*pspCursor = 0;
	}

	ilItemCount = get_no_of_items(pcpFields); 
	pclDataArea = pcpDataArea;
	for(ilLc =  1; ilLc < ilItemCount; ilLc++)
	{
		while(*pclDataArea != '\0')
		{
			pclDataArea++;
		}
			*pclDataArea = ',';
	}
  if (ilRc == RC_SUCCESS)
	{
		dbg(DEBUG,"DoSelect: RC=%d  items=%d <%s>",ilRc,ilItemCount,pcpDataArea);
	}
	else
	{
		dbg(DEBUG,"DoSelect: RC=%d",ilRc,pcpDataArea);
	}
	return(ilRc);
}

static int DoSingleSelect(char *pcpTable,char *pcpSelection,char *pcpFields,char *pcpDataArea)
{
	int ilRc = RC_SUCCESS;
	static char clSqlBuf[4096];

	short slCursor = 0;
	short slFunction;
	char *pclDataArea;
	int ilLc,ilItemCount;

	sprintf(clSqlBuf,"SELECT %s FROM %s %s",pcpFields,pcpTable,pcpSelection);

  *pcpDataArea = '\0';
	ilRc = sql_if(START,&slCursor,clSqlBuf,pcpDataArea);
	if(ilRc != RC_SUCCESS && ilRc != NOTFOUND)
	{
		/* there must be a database error */
		get_ora_err(ilRc, cgErrMsg);
		dbg(TRACE,"DoSingleSelect failed RC=%d <%s>",ilRc,cgErrMsg);
		dbg(TRACE,"SQL: <%s>",clSqlBuf);
	}
	close_my_cursor(&slCursor);
	slCursor = 0;

	ilItemCount = get_no_of_items(pcpFields); 
	pclDataArea = pcpDataArea;
	for(ilLc =  1; ilLc < ilItemCount; ilLc++)
	{
		while(*pclDataArea != '\0')
		{
			pclDataArea++;
		}
			*pclDataArea = ',';
		}
  if (ilRc == RC_SUCCESS)
	{
		dbg(DEBUG,"DoSingleSelect: RC=%d <%s>",ilRc,pcpDataArea);
	}
	else
	{
		dbg(DEBUG,"DoSingleSelect: RC=%d",ilRc);
	}
	return(ilRc);
}

static int CheckRegi(char *pcpSurn,char *pcpSday)
{
	char clAcfr[24];
	char clActo[24];
	char clSelection[240];

	int ilRc = RC_SUCCESS;
	sprintf(clAcfr,"%s000000",pcpSday);
	sprintf(clActo,"%s235959",pcpSday);
	sprintf(clSelection,"WHERE REGI IN(%s) AND SURN = '%s' AND "
	" (VPTO >= '%s' OR VPTO = ' ') AND VPFR <= '%s'",
			cgRegi,pcpSurn,clAcfr,clActo);
	ilRc = DoSingleSelect("SRETAB",clSelection,"URNO",cgDataArea);
	if (ilRc != RC_SUCCESS)
	{
		dbg(DEBUG,"No regular shift worker: RC=%d <%s>",ilRc,clSelection);
	}
	else
	{
		dbg(DEBUG,"Is regular shift worker: RC=%d <%s>",ilRc,clSelection);
	}
	return ilRc;
}


static int WriteBsrFile(FILE *pfpFile,char *pcpSday)
{
	int ilRc = RC_SUCCESS;
	char clDataArea[2048];
	char clStfu[2048];
	char clSelection[262];
	char clGspSelection[262];
	char clStfUrno[12];
	char clVafr[24],clVato[24];
	long llAction = ARR_FIRST;
  long llRow = 0;
	char clSurn[24];
	char clPeno[24];
	char clGsnm[34];
	char clGplu[24];
	char *pclDataArea;
	int ilBytes;
	char clLineBuffer[1024];
	int ilStfu;
	short slFunction;
	short slCursor;
	short slGspFunction;
	short slGspCursor;
	long llCount = 0;


	sprintf(clVafr,"%s000000",pcpSday);
	sprintf(clVato,"%s235959",pcpSday);
	sprintf(clSelection,"WHERE (VATO >= '%s' OR VATO = ' ') AND "
			" VAFR <= '%s'",clVafr,clVato);

	slFunction = START;
	slCursor = 0;
	while(DoSelect(slFunction,&slCursor,"GPLTAB",clSelection,
					"URNO,GSNM",clDataArea) == RC_SUCCESS)
	{
		long llRow = ARR_FIRST;

		slFunction = NEXT;

		get_real_item(clGplu,clDataArea,1);
		get_real_item(clGsnm,clDataArea,2);
		sprintf(clGspSelection,"WHERE GPLU = '%s'",clGplu);
		slGspCursor = 0;
		slGspFunction = START; 
		while(DoSelect(slGspFunction,&slGspCursor,"GSPTAB",
							clGspSelection,"STFU",clStfu) == RC_SUCCESS)
		{
			slGspFunction = NEXT;
			ConvertDbStringToClient(clStfu);
			pclDataArea = clStfu;
			while(*pclDataArea != '\0')
			{
				if (*pclDataArea == '|')
				{
					*pclDataArea = ',';
				}
				pclDataArea++;
			}
			ilStfu = 1; 
			dbg(TRACE,"STFU <%s>",clStfu);
			while (get_real_item(clSurn,clStfu,ilStfu) > -1)
			{
			
				ilStfu++;
				dbg(TRACE,"SURN <%s> found",clSurn);
				llRow = ARR_FIRST;
				ilRc = CEDAArrayFindRowPointer(&rgStfArray.rrArrayHandle,
					rgStfArray.crArrayName,&rgStfArray.rrIdx01Handle,
							rgStfArray.crIdx01Name,clSurn,&llRow,
									(void **)&rgStfArray.pcrArrayRowBuf);
				if (ilRc == RC_SUCCESS)
				{
					strcpy(clPeno,FIELDVAL(rgStfArray,rgStfArray.pcrArrayRowBuf,igStfPENO));
				}
				else
				{
					dbg(TRACE,"Peno not found for SURN <%s> RC=%d",clSurn,ilRc);
					continue;
				}

				ilRc = CheckRegi(clSurn,pcpSday);
				if (ilRc == RC_SUCCESS)
				{
					memset(clLineBuffer,0,sizeof(clLineBuffer));
					ilBytes = CreateOneBsrLine(clLineBuffer,clGsnm,clPeno);

					if((ilRc  = fwrite(clLineBuffer,sizeof(char),ilBytes,pfpFile)) != ilBytes)
					{
						dbg(TRACE,"could not write %d Bytes to roster line: <%s>, reason %d <%s>",
								ilBytes,clLineBuffer,errno,strerror(errno));
						ilRc = RC_FAIL;
					}
					else
					{
						ilRc = RC_SUCCESS;
						llCount++;
					}
				}
			}
		}
	}
	close_my_cursor(&slCursor);
  return llCount;
}

	

/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(EVENT *prpEvent)
{
	int	   ilRc           = RC_SUCCESS;			/* Return code */
	int      ilCmd          = 0;

	BC_HEAD *prlBchd       = NULL;
	CMDBLK  *prlCmdblk       = NULL;
	char    *pclSelection    = NULL;
	char    *pclFields       = NULL;
	char    *pclData         = NULL;
	char    *pclRow          = NULL;
	char 		clTable[34];
	char    clTmpJob[12];
	char    clUrnoList[50000];

	prlBchd    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
	prlCmdblk    = (CMDBLK *)  ((char *)prlBchd->data);
	pclSelection = prlCmdblk->data;
	pclFields    = pclSelection + strlen(pclSelection) + 1;
	pclData      = pclFields + strlen(pclFields) + 1;

	strcpy(clTable,prlCmdblk->obj_name);

	dbg(TRACE,"==========START <%10.10d> ==========",lgEvtCnt);

  igQueOut = prgEvent->originator;

  /* Save Global Elements of CMDBLK */
  strcpy(cgTwStart,prlCmdblk->tw_start);
  strcpy(cgTwEnd,prlCmdblk->tw_end);
  /* Save Global Elements of BC_HEAD */
  strcpy(cgRecvName,prlBchd->recv_name);
  strcpy(cgDestName,prlBchd->dest_name);
 
	/****************************************/
	DebugPrintBchead(DEBUG,prlBchd);
	DebugPrintCmdblk(DEBUG,prlCmdblk);
  dbg(DEBUG,"TwStart   <%s>",cgTwStart);
  dbg(DEBUG,"TwEnd     <%s>",cgTwEnd);
	dbg(DEBUG,"originator<%d>",prpEvent->originator);
	dbg(DEBUG,"selection <%s>",pclSelection);
	dbg(DEBUG,"fields    <%s>",pclFields);
	dbg(DEBUG,"data      <%s>",pclData);
	/****************************************/
	{ /* delete old data from pclData */
		char *pclNewline = NULL;

		if ((pclNewline = strchr(pclData,'\n')) != NULL)
		{
			*pclNewline = '\0';
		}
	}

	ilRc = GetCommand(&prlCmdblk->command[0],&ilCmd);
	dbg(DEBUG,"RC from GetCommand(%s) is %d",&ilCmd,ilRc);
	if(ilRc == RC_SUCCESS)
	{
		switch (ilCmd)
		{
   	case CMD_BSR :
				dbg(TRACE,"Command is BSR");
				ExportBsr();
			 break;
		default:
			  dbg(TRACE,"unknown command <%s>",prlCmdblk->command);
		}
	}

	dbg(TRACE,"==========END  <%10.10d> ==========",lgEvtCnt);
	dbg(TRACE,"now terminating, next event tomorrow!");

	Terminate(1);

	return(RC_SUCCESS);
	
} /* end of HandleData */


static int StrToTime(char *pcpTime,time_t *plpTime)
{
	struct tm *_tm;
	time_t now;
	char   _tmpc[6];
	long llUtcDiff;

	dbg(0,"StrToTime, <%s>",pcpTime);
	if (strlen(pcpTime) < 12 )
	{
		*plpTime = time(0L);
		return RC_FAIL;
	} /* end if */




	now = time(0L);
	_tm = (struct tm *)gmtime(&now);

	_tmpc[2] = '\0';
/*
	strncpy(_tmpc,pcpTime+12,2);
	_tm -> tm_sec = atoi(_tmpc);
*/
	_tm -> tm_sec = 0;
	
	strncpy(_tmpc,pcpTime+10,2);
	_tm -> tm_min = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+8,2);
	_tm -> tm_hour = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+6,2);
	_tm -> tm_mday = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+4,2);
	_tm -> tm_mon = atoi(_tmpc)-1;
	strncpy(_tmpc,pcpTime,4);
	_tmpc[4] = '\0';
	_tm ->tm_year = atoi(_tmpc)-1900;
	_tm ->tm_wday = 0;
	_tm ->tm_yday = 0;
	_tm->tm_isdst = -1; /* no adjustment of daylight saving time */
	now = mktime(_tm);
	dbg(0,"StrToTime: %02d.%02d.%04d %02d:%02d Now: %ld",_tm->tm_mday,
_tm->tm_mon+1,_tm->tm_year+1900,_tm->tm_hour,_tm->tm_min,now);
	if (now != (time_t) -1)
	{
	{
		time_t utc,local;

	_tm = (struct tm *)gmtime(&now);
	utc = mktime(_tm);
	_tm = (struct tm *)localtime(&now);
	local = mktime(_tm);
	dbg(TRACE,"utc %ld, local %ld, utc-diff %ld",
		utc,local,local-utc);
		llUtcDiff = local-utc;
	}
		now +=  + llUtcDiff;
		*plpTime = now;
		return RC_SUCCESS;
	}
	*plpTime = time(NULL);
	return RC_FAIL;
}


/**
dbg(TRACE,"%05d:",__LINE__);
***/



/*
 *     struct  tm {
 *             int     tm_sec;    seconds after the minute -- [0, 59]  
 *             int     tm_min;    minutes after the hour -- [0, 59] 
 *             int     tm_hour;   hour since midnight -- [0, 23] 
 *             int     tm_mday;   day of the month -- [1, 31] 
 *             int     tm_mon;    months since January -- [0, 11] 
 *             int     tm_year;   years since 1900 
 *             int     tm_wday;   days since Sunday -- [0, 6] 
 *             int     tm_yday;   days since January 1 -- [0, 365] 
 *             int     tm_isdst;  flag for daylight savings time 
 *             long    tm_tzadj;  seconds from GMT (east < 0) 
 *             char    tm_name[LTZNMAX];   name of timezone 
 *     };
 */




void	StrAddTime(char *pcpBase,char *pcpOffs,char *pcpDest)
{

	if (pcpBase != pcpDest)
	{
		strcpy(pcpDest,pcpBase);
	}
	AddSecondsToCEDATime(pcpDest,(time_t)atol(pcpOffs),1);

} /* end of StrAddTime */



int check_ret1(int ipRc,int ipLine)
{
	int ilOldDebugLevel;

	ilOldDebugLevel = debug_level;

	debug_level = TRACE;

	dbg(TRACE,"DB-ERROR in Line: %d",ipLine);
	debug_level  = ilOldDebugLevel;
	return check_ret(ipRc);
}

static void HandleQueues()
{
	int	ilRc = RC_SUCCESS;			/* Return code */
	int	ilBreakOut = FALSE;
	
	do
	{
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;	

		if( ilRc == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRc != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRc);
			} /* fi */
		
			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				ilBreakOut = TRUE;
				break;	

			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate(1);
				break;	
	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				ilBreakOut = TRUE;
				break;	

			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;

			case	SHUTDOWN	:
				Terminate(1);
				break;
						
			case	RESET		:
				ilRc = Reset();
				break;
						
			case	EVENT_DATA	:
				dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
					
			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;

			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;

			default			:
				dbg(TRACE,"HandleQueues: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		} /* end else */
	} while (ilBreakOut == FALSE);

	if(igInitOK == FALSE)
	{
			ilRc = InitBsrhdl();
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitBsrhdl failed!");
			} /* end of if */
	}/* end of if */

}

static int TriggerAction(char *pcpTableName,char *pcpFields)
{
  int           ilRc          = RC_SUCCESS ;    /* Return code */
  EVENT        *prlEvent      = NULL ;
  BC_HEAD      *prlBchead     = NULL ;
  CMDBLK       *prlCmdblk     = NULL ;
  /*ACTIONConfig *prlAction     = NULL ;*/
	ACTIONConfig rlAction;
  char         *pclSelection  = NULL ;
  char         *pclFields     = NULL ;
  char         *pclData       = NULL ;
  long          llActSize     = 0 ;
  int           ilAnswerQueue = 0 ;
  char          clQueueName[16] ;
	
  if(ilRc == RC_SUCCESS)
  {

  }/* end of if */
	

  if (ilRc == RC_SUCCESS)
  {
   sprintf (&clQueueName[0],"%s2", mod_name) ;

   ilRc = GetDynamicQueue(&ilAnswerQueue,&clQueueName[0]);
   if (ilRc != RC_SUCCESS)
   {
    dbg(TRACE,"TriggerAction: GetDynamicQueue failed <%d>",ilRc);
   }
   else
   {
    dbg(DEBUG,"TriggerAction: got <%d> for <%s>",ilAnswerQueue, &clQueueName[0]);
   }/* end of if */
  }/* end of if */
	
  if (ilRc == RC_SUCCESS)
  {
   llActSize = sizeof(EVENT) + sizeof(BC_HEAD) + 
	 		sizeof(CMDBLK) + sizeof(ACTIONConfig) + strlen(pcpFields) + 666;
   prgOutEvent = realloc(prgOutEvent,llActSize);
   if(prgOutEvent == NULL)
   {
    dbg(TRACE,"TriggerAction: realloc out event <%d> bytes failed",llActSize);
    ilRc = RC_FAIL ;
   } /* end of if */
  } /* end of if */

  if(ilRc == RC_SUCCESS)
  {
   prlBchead     = (BC_HEAD *) ((char *) prgOutEvent + sizeof(EVENT)) ;
   prlCmdblk     = (CMDBLK *)  ((char *) prlBchead->data) ;
   pclSelection  = prlCmdblk->data ;
   pclFields     = pclSelection + strlen (pclSelection) + 1 ;
	 strcpy(pclFields,pcpFields);
   pclData       = pclFields + strlen (pclFields) + 1 ;
   strcpy (pclData, "DYN") ;
	memset(&rlAction,0,sizeof(ACTIONConfig));
   /*prlAction     = (ACTIONConfig *) (pclData + strlen(pclData) + 1) ;*/
	
	prlBchead->rc = RC_SUCCESS;
	
   prgOutEvent->type        = USR_EVENT ;
   prgOutEvent->command     = EVENT_DATA ;
   prgOutEvent->originator  = ilAnswerQueue ;
   prgOutEvent->retry_count = 0 ;
   prgOutEvent->data_offset = sizeof(EVENT) ;
   prgOutEvent->data_length = llActSize-sizeof(EVENT) ;

	rlAction.iEmptyFieldHandling = iDELETE_ALL_BLANKS ; 
	rlAction.iIgnoreEmptyFields = 0 ; 
	strcpy(rlAction.pcSndCmd, "") ; 
	sprintf(rlAction.pcSectionName,"%s_%s", mod_name,pcpTableName) ; 
	strcpy(rlAction.pcTableName, pcpTableName) ; 
	strcpy(rlAction.pcFields, pcpFields) ; 
  strcpy(rlAction.pcSectionCommands, "IRT,URT,DRT") ;
	rlAction.iModID = mod_id ;   

	 /***
   prlAction->iEmptyFieldHandling = iDELETE_ALL_BLANKS ;
   prlAction->iIgnoreEmptyFields = 0 ;
   strcpy(prlAction->pcSndCmd, "") ;
   sprintf(prlAction->pcSectionName,"%s_%s", mod_name,pcpTableName) ;
   strcpy(prlAction->pcTableName, pcpTableName) ;
   strcpy(prlAction->pcFields, pcpFields) ;
   strcpy(prlAction->pcSectionCommands, "IRT,URT,DRT") ;
   prlAction->iModID = mod_id ;
	 ***/
	 /*********************** MCU TEST **************
   strcpy(prlAction->pcFields, "-") ;
   strcpy(prlAction->pcFields, "") ;
	 *********************** MCU TEST ***************/
	 /***********
	 if (!strcmp(pcpTableName,"SPRTAB"))
		{
   	strcpy(prlAction->pcFields, "PKNO,USTF,ACTI,FCOL") ;
	 }
	 ************/

   /****************************************
     DebugPrintEvent(DEBUG,prgOutEvent) ;
     DebugPrintBchead(DEBUG,prlBchead) ;
     DebugPrintCmdblk(DEBUG,prlCmdblk) ;
     dbg (DEBUG,"TriggerAction: selection <%s>",pclSelection) ;
     dbg (DEBUG,"TriggerAction: fields    <%s>",pclFields) ;
     dbg (DEBUG,"TriggerAction: data      <%s>",pclData) ;
     DebugPrintACTIONConfig(DEBUG,prlAction) ;
   ****************************************/

   if(ilRc == RC_SUCCESS)
   {
    rlAction.iADFlag = iDELETE_SECTION ;
	 memcpy(pclData + strlen(pclData) + 1,&rlAction,sizeof(ACTIONConfig));

    ilRc = que(QUE_PUT, 7400, ilAnswerQueue, PRIORITY_3, llActSize, (char *) prgOutEvent) ; 
    if(ilRc != RC_SUCCESS)
    {
     dbg(TRACE,"TriggerAction: QUE_PUT failed <%d>",ilRc) ;
    }
    else
    {
     ilRc = WaitAndCheckQueue(100, ilAnswerQueue,&prgItem) ;
     if(ilRc != RC_SUCCESS)
     {
      dbg(TRACE,"TriggerAction: WaitAndCheckQueue failed <%d>",ilRc) ;
     }
     else
     {
      /* return value for delete does not matter !!! */
		
      /****************************************
      prlEvent     = (EVENT *)   ((char *)prgItem->text) ;
      prlBchead    = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT)) ;
      prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data) ;
      pclSelection = (char *)    prlCmdblk->data ;
      pclFields    = (char *)    pclSelection + strlen(pclSelection) + 1;
      pclData      = (char *)    pclFields + strlen(pclFields) + 1;
      DebugPrintItem(DEBUG, prgItem) ;
      DebugPrintEvent(DEBUG, prlEvent) ;
      DebugPrintBchead(DEBUG, prlBchead) ;
      DebugPrintCmdblk(DEBUG, prlCmdblk) ;
      dbg(DEBUG,"TriggerAction: selection <%s>",pclSelection) ;
      dbg(DEBUG,"TriggerAction: fields    <%s>",pclFields) ;
      dbg(DEBUG,"TriggerAction: data      <%s>",pclData) ;
      ****************************************/
    } /* end of if */
   }/* end of if */
  }/* end of if */

  if(ilRc == RC_SUCCESS)
  {
   rlAction.iADFlag = iADD_SECTION ;
	 memcpy(pclData + strlen(pclData) + 1,&rlAction,sizeof(ACTIONConfig));

   ilRc = que(QUE_PUT, 7400, ilAnswerQueue, PRIORITY_3, llActSize,
                                              (char *) prgOutEvent ) ;
   if(ilRc != RC_SUCCESS)
   {
    dbg(TRACE,"TriggerAction: QUE_PUT failed <%d>",ilRc) ;
   }
   else
   {
    ilRc = WaitAndCheckQueue(100,ilAnswerQueue,&prgItem) ;
    if(ilRc != RC_SUCCESS)
    {
     dbg(TRACE,"TriggerAction: WaitAndCheckQueue failed <%d>",ilRc) ;
    }
    else
    {
     prlEvent     = (EVENT *)   ((char *)prgItem->text) ;
     prlBchead    = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT)) ;
     prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data) ;
     pclSelection = (char *)    prlCmdblk->data ;
     pclFields    = (char *)    pclSelection + strlen(pclSelection) + 1 ;
     pclData      = (char *)    pclFields + strlen(pclFields) + 1 ;

     if(strcmp(pclData,"SUCCESS") != 0)
     {
      dbg(TRACE,"TriggerAction: add dynamic action config failed") ;
      DebugPrintItem(DEBUG,prgItem) ;
      DebugPrintEvent(DEBUG,prlEvent) ;
      DebugPrintBchead(DEBUG,prlBchead) ;
      DebugPrintCmdblk(DEBUG,prlCmdblk) ;
      dbg (DEBUG,"TriggerAction: selection <%s>",pclSelection) ;
      dbg (DEBUG,"TriggerAction: fields    <%s>",pclFields) ;
      dbg(DEBUG,"TriggerAction: data      <%s>",pclData) ;
      ilRc = RC_FAIL ;
     }
     else
     {
      dbg(DEBUG,"TriggerAction: add dynamic action config OK") ;
     }/* end of if */
    }/* end of if */
   }/* end of if */
  }/* end of if */

  ilRc = que(QUE_DELETE,ilAnswerQueue,ilAnswerQueue,0,0,0) ;
  if(ilRc != RC_SUCCESS)
  {
   dbg(TRACE,"que QUE_DELETE <%d> failed <%d>",ilAnswerQueue, ilRc) ;
  }
  else
  {
   dbg(DEBUG,"TriggerAction: queue <%d> <%s> deleted", ilAnswerQueue, &clQueueName[0]) ;
  }/* end of if */
 }/* end of if */

 return (ilRc) ;
	
}
static int WaitAndCheckQueue(int ipTimeout, int ipModId, ITEM **prpItem)
{
	int ilRc          = RC_SUCCESS;                      /* Return code */
	int ilWaitCounter = 0;

	do
	{
		nap(50);
		ilWaitCounter += 1;

		ilRc = CheckQueue(ipModId,prpItem);
		if(ilRc != RC_SUCCESS)
		{
			if(ilRc != QUE_E_NOMSG)
			{
				dbg(TRACE,"WaitAndCheckQueue: CheckQueue <%d> failed <%d>",mod_id,ilRc);
			}/* end of if */
		}/* end of if */
	}while((ilRc == QUE_E_NOMSG) && (ilWaitCounter < ipTimeout));

	if(ilWaitCounter >= ipTimeout)
	{
		dbg(TRACE,"WaitAndCheckQueue: timeout reached <%d>",ipTimeout);
		ilRc = RC_FAIL;
	}/* end of if */

	return ilRc;
}


static int CheckQueue(int ipModId, ITEM **prpItem)
{
	int     ilRc       = RC_SUCCESS;                      /* Return code */
	int     ilItemSize = 0;
	EVENT *prlEvent    = NULL;
static	ITEM  *prlItem      = NULL;
	
	ilItemSize = I_SIZE;
	
	ilRc = que(QUE_GETBIGNW,ipModId,ipModId,PRIORITY_3,ilItemSize,(char *) &prlItem);
	if(ilRc == RC_SUCCESS)
	{
		dbg(TRACE,"DebugPrintItem with prlItem follows");
		DebugPrintItem(TRACE,prlItem);
		*prpItem = prlItem;
		dbg(TRACE,"DebugPrintItem with prpItem follows");
		DebugPrintItem(TRACE,*prpItem);
		prlEvent = (EVENT*) ((prlItem)->text);

		switch( prlEvent->command )
		{
			case    HSB_STANDBY :
				dbg(TRACE,"CheckQueue: HSB_STANDBY");
				HandleQueues();
				ctrl_sta = prlEvent->command;
				break;
	
			case    HSB_COMING_UP   :
				dbg(TRACE,"CheckQueue: HSB_COMING_UP");
				ctrl_sta = prlEvent->command;
				break;
	
			case    HSB_ACTIVE  :
				dbg(TRACE,"CheckQueue: HSB_ACTIVE");
				ctrl_sta = prlEvent->command;
				break;
	
			case    HSB_ACT_TO_SBY  :
				dbg(TRACE,"CheckQueue: HSB_ACT_TO_SBY");
				HandleQueues();
				ctrl_sta = prlEvent->command;
				break;
	
			case    HSB_DOWN    :
				dbg(TRACE,"CheckQueue: HSB_DOWN");
				ctrl_sta = prlEvent->command;
				/* Acknowledge the item 
				ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
				if( ilRc != RC_SUCCESS )
				{
					HandleQueErr(ilRc);
				}  */
				Terminate(30);
				break;
		
		case    HSB_STANDALONE  :
				dbg(TRACE,"CheckQueue: HSB_STANDALONE");
				ctrl_sta = prlEvent->command;
				break;
	
			case    SHUTDOWN    :
				dbg(TRACE,"CheckQueue: SHUTDOWN");
				/* Acknowledge the item */
				ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
				if( ilRc != RC_SUCCESS )
				{
					/* handle que_ack error */
					HandleQueErr(ilRc);
				} /* fi */
				Terminate(30);
				break;
	
			case    RESET       :
				ilRc = Reset();
				break;
	
			case    EVENT_DATA  :
				/* DebugPrintItem(DEBUG,prgItem); */
				/* DebugPrintEvent(DEBUG,prlEvent); */
				break;
	
			case    TRACE_ON :
				dbg_handle_debug(prlEvent->command);
				break;
	
			case    TRACE_OFF :
				dbg_handle_debug(prlEvent->command);
				break;


			default         :
				dbg(TRACE,"CheckQueue: unknown event");
				DebugPrintItem(TRACE,prlItem);
				DebugPrintEvent(TRACE,prlEvent);
				break;
	
		} /* end switch */
	
		/* Acknowledge the item */
		ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
		if( ilRc != RC_SUCCESS )
		{
			/* handle que_ack error */
			HandleQueErr(ilRc);
		} /* fi */
	}else{
		if(ilRc != QUE_E_NOMSG)
		{
			dbg(TRACE,"CheckQueue: que(QUE_GETBIG,%d,%d,...) failed <%d>",ipModId,ipModId,ilRc);
			HandleQueErr(ilRc);
		}/* end of if */
	}/* end of if */
       
	return ilRc;
}/* end of CheckQueue */



/****************************************************************************
* Function: GetUrnoFromSelection
* Parameter:           
*			 
* Return:
* Description: gets the Urno from the SQL Statement "WHERE URNO = 'xxxxxxx'"
*			   
*****************************************************************************/
static int GetUrnoFromSelection(char *pcpSelection,char *pcpUrno)
{
	char *pclFirst;
	char *pclLast;
	char pclTmp[24];
	int ilLen;
	
	pclFirst = strchr(pcpSelection,'\'');
	if (pclFirst != NULL)
	{
		pclLast  = strrchr(pcpSelection,'\'');
		if (pclLast == NULL)
		{
			pclLast = pclFirst + strlen(pclFirst);
		}
		ilLen = pclLast-pclFirst < 22 ? pclLast - pclFirst : 22;
		strncpy(pclTmp,++pclFirst,ilLen);
		pclTmp[ilLen-1] = '\0';
		strcpy(pcpUrno,pclTmp);
	}
	else
		{
		 pclFirst = pcpSelection;
		 while (!isdigit(*pclFirst) && *pclFirst != '\0')
		 {
			pclFirst++;
			}
		strcpy(pcpUrno,pclFirst);
		}
	return RC_SUCCESS;
}


static int SaveIndexInfo (ARRAYINFO *prpArrayInfo)
{
  int    ilRc       = RC_SUCCESS ;
  long   llRow      = 0 ;
  FILE  *prlFile    = NULL ;
  char   clDel      = ',' ;
  char   clFile[512] ;
  char  *pclTrimBuf = NULL ;

  dbg (TRACE,"SaveIndexInfo: <%s>",prpArrayInfo->crArrayName) ;

  if (ilRc == RC_SUCCESS)
  {
   sprintf (&clFile[0],"%s/%s_%s.info",getenv("DBG_PATH"),mod_name,prpArrayInfo->crArrayName) ;

   errno = 0 ;
   prlFile = fopen (&clFile[0], "w") ;
   if (prlFile == NULL)
   {
    dbg (TRACE, "SaveIndexInfo: fopen <%s> failed <%d-%s>", &clFile[0], errno, strerror(errno)) ;
    ilRc = RC_FAIL ;
   } /* end of if */
  } /* end of if */

  if (ilRc == RC_SUCCESS)
  {
   if (igTestOutput > 2) 
   {
    dbg (DEBUG, "ArrayName      <%s>\n", prpArrayInfo->crArrayName) ;      
    dbg (DEBUG, "ArrayFieldList <%s>\n", prpArrayInfo->crArrayFieldList) ;  
    dbg (DEBUG, "Idx01FieldList <%s>\n", prpArrayInfo->crIdx01FieldList) ;  
   }

   fprintf (prlFile, "ArrayName      <%s>\n", prpArrayInfo->crArrayName) ; fflush(prlFile) ;
   fprintf (prlFile, "ArrayFieldList <%s>\n", prpArrayInfo->crArrayFieldList) ; fflush(prlFile) ;
   fprintf (prlFile, "Idx01FieldList <%s>\n", prpArrayInfo->crIdx01FieldList) ; fflush(prlFile) ;

   if (igTestOutput > 1) 
   {
    dbg(DEBUG,"IndexData") ; 
   }
   fprintf(prlFile,"IndexData\n"); fflush(prlFile);

   llRow = ARR_FIRST ;
   do
   {
    ilRc = CEDAArrayFindKey (&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), &(prpArrayInfo->rrIdx01Handle), prpArrayInfo->crIdx01Name, &llRow, prpArrayInfo->lrIdx01RowLen, prpArrayInfo->pcrIdx01RowBuf);
    if (ilRc == RC_SUCCESS)
    {
     if (igTestOutput > 1) 
     {
      dbg(DEBUG, "SaveIndexInfo: Key <%ld> <%s>",llRow,prpArrayInfo->pcrIdx01RowBuf) ;
     }

     if (strlen (prpArrayInfo->pcrIdx01RowBuf) == 0)
     {
      ilRc = CEDAArrayGetRow(&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), llRow, clDel, prpArrayInfo->lrArrayRowLen, prpArrayInfo->pcrArrayRowBuf);
      if(ilRc == RC_SUCCESS)
      {
       if (igTestOutput > 1) 
       {
        dbg (DEBUG, "SaveIndexInfo: Row <%ld> <%s>", llRow,prpArrayInfo->pcrArrayRowBuf) ;
       }
      } /* end of if */
     } /* end of if */

     pclTrimBuf = strdup (prpArrayInfo->pcrIdx01RowBuf) ;
     if (pclTrimBuf != NULL)
     {
      ilRc = GetDataItem (pclTrimBuf,prpArrayInfo->pcrIdx01RowBuf, 1, clDel, "", "  ") ;
      if (ilRc > 0)
      {
       ilRc = RC_SUCCESS ;
       fprintf (prlFile,"Key            <%ld><%s>\n",llRow,pclTrimBuf) ; fflush(prlFile) ;
      } /* end of if */

      free (pclTrimBuf) ;
      pclTrimBuf = NULL ;
     } /* end of if */

     llRow = ARR_NEXT;
    }
    else
    {
     dbg (TRACE, "SaveIndexInfo: CEDAArrayFindKey failed <%d>", ilRc) ;
    } /* end of if */
   } while (ilRc == RC_SUCCESS) ;

   if (igTestOutput > 1) 
   {
    dbg (DEBUG,"ArrayData") ;
   }
   fprintf (prlFile,"ArrayData\n"); fflush(prlFile);

   llRow = ARR_FIRST ;
   do
   {
    ilRc = CEDAArrayFindKey (&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), &(prpArrayInfo->rrIdx01Handle), prpArrayInfo->crIdx01Name, &llRow, prpArrayInfo->lrIdx01RowLen, prpArrayInfo->pcrIdx01RowBuf);
    if (ilRc == RC_SUCCESS)
    {
     ilRc = CEDAArrayGetRow(&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), llRow, clDel, prpArrayInfo->lrArrayRowLen, prpArrayInfo->pcrArrayRowBuf);
     if (ilRc == RC_SUCCESS)
     {
      if (igTestOutput > 1) 
      {
       dbg(DEBUG,"SaveIndexInfo: Row <%ld> <%s>",llRow,prpArrayInfo->pcrArrayRowBuf) ;
      }
      fprintf (prlFile,"Row            <%ld><%s>\n",llRow,prpArrayInfo->pcrArrayRowBuf) ; fflush(prlFile) ;
     }
     else
     {
      dbg (TRACE, "SaveIndexInfo: CEDAArrayGetRow failed <%d>", ilRc) ;
     } /* end of if */

     llRow = ARR_NEXT ;
    } 
    else
    {
     dbg (TRACE, "SaveIndexInfo: CEDAArrayFindKey failed <%d>", ilRc) ;
    } /* end of if */
   } while (ilRc == RC_SUCCESS) ;

   if (ilRc == RC_NOTFOUND)
   {
    ilRc = RC_SUCCESS ;
   } /* end of if */
  } /* end of if */

  if (prlFile != NULL)
  {
   fclose(prlFile) ;
   prlFile = NULL ;
   if (igTestOutput > 1) 
   {
    dbg (TRACE, "SaveIndexInfo: event saved - file <%s>", &clFile[0]) ;
   }
  } /* end of if */

  return (ilRc) ;

}

		
static int SendAnswer(int ipDest, char *pcpAnswer)
{
	int			ilRC;
	int			ilLen;
	EVENT			*prlOutEvent 	= NULL;
	BC_HEAD		*prlOutBCHead 	= NULL;
	CMDBLK		*prlOutCmdblk	= NULL;

    dbg(DEBUG,"SendAnswer: START-->");

	/* calculate size of memory we need */
	ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + strlen(pcpAnswer) + 32 ;

	/* get memory for out event */
	if ((prlOutEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
	{
		dbg(TRACE,"<SendAnswer> %05d malloc failed, can't allocate %d bytes", __LINE__, ilLen) ;
		dbg(DEBUG,"<SendAnswer> ----- END -----") ;
		return RC_FAIL ;
	}

	/* clear buffer */
	memset((void*)prlOutEvent, 0x00, ilLen) ;

	/* set structure members */
	prlOutEvent->type	   = SYS_EVENT ;
	prlOutEvent->command 	   = EVENT_DATA ;
	prlOutEvent->originator  = ipDest ;            /* mod_id ;   */ 
	prlOutEvent->retry_count = 0 ;
	prlOutEvent->data_offset = sizeof(EVENT) ;
	prlOutEvent->data_length = ilLen - sizeof(EVENT) ;

	/* BCHead members */
	prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT)) ;
	prlOutBCHead->rc = NETOUT_NO_ACK ;
	strncpy(prlOutBCHead->dest_name, "ACTION", 10) ;

	/* CMDBLK members */
	prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data) ;
	strcpy(prlOutCmdblk->command, "FIR") ;
	strcpy(prlOutCmdblk->tw_end, "ANS,WER,ACTION") ;

	/* data */
	strcpy(prlOutCmdblk->data+2, pcpAnswer) ;

	/* send this message */
	dbg(DEBUG,"<SendAnswer> sending to Mod-ID: %d", prgEvent->originator) ;
	if ((ilRC = que(QUE_PUT, prgEvent->originator, mod_id, PRIORITY_3, ilLen, (char*)prlOutEvent)) != RC_SUCCESS)
	{
		/* delete memory */
		free((void*)prlOutEvent) ;
		dbg(TRACE,"<SendAnswer> %05d QUE_PUT returns: %d", __LINE__, ilRC) ;
		dbg(DEBUG,"<SendAnswer> ----- END -----") ;
		return RC_FAIL ;
	}

/*  
	DebugPrintBchead (DEBUG, prlOutBCHead) ;
	DebugPrintCmdblk (DEBUG, prlOutCmdblk) ;
	DebugPrintEvent  (DEBUG, prlOutEvent) ;               
*/
	/* release memory */
	free((void*)prlOutEvent);
	
    dbg(DEBUG,"SendAnswer: <-- ENDE ");
	
	/* bye bye */
	return RC_SUCCESS;
}

static int GetDebugLevel(char *pcpMode, int *pipMode)
{
	int		ilRC = RC_SUCCESS;
	char	clCfgValue[64];

	ilRC = iGetConfigRow(cgConfigFile, "SYSTEM", pcpMode, CFG_STRING,
							clCfgValue);

	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"SYSTEM %s not found in <%s>", pcpMode, cgConfigFile);
	}
	else
	{
		dbg(TRACE, "SYSTEM %s <%s>", pcpMode, clCfgValue);
		if (!strcmp(clCfgValue, "DEBUG"))
			*pipMode = DEBUG;
		else if (!strcmp(clCfgValue, "TRACE"))
			*pipMode = TRACE;
		else
			*pipMode = 0;
	}

	return ilRC;
}

static int ItemCnt(char *pcgData, char cpDelim)
{
	int	ilItemCount=0;

	if (pcgData && strlen(pcgData)) 
	{
	    ilItemCount = 1;
	    while (*pcgData) 
		{
			if (*pcgData++ == cpDelim) 
			{
				ilItemCount++;
			} /* end if */
		} /* end while */
	} /* end if */
	return ilItemCount;
}

static int DumpRecord(ARRAYINFO *prpArray,char *pcpFields,char *pcpBuf)
{
	int ilRc;
	int ilRecord = 0;
	long llRowNum = ARR_FIRST;
	char *pclRowBuf;

	dbg(TRACE,"Recorddump: %s", prpArray->crArrayName);
	if (pcpFields != NULL)
	{
		dbg(TRACE,"Recorddump: Fields = %s", pcpFields);
	}
	else
	{
		dbg(TRACE,"Recorddump: Fields = %s",prpArray->crArrayFieldList);
	}

	{
			int ilFieldNo;
		
			for (ilFieldNo = 1; ilFieldNo <= prpArray->lrArrayFieldCnt; ilFieldNo++)
			{
				char pclFieldName[24];

				*pclFieldName = '\0';
				GetDataItem(pclFieldName,prpArray->crArrayFieldList,
					ilFieldNo,',',"","\0\0");
				if (pcpFields != NULL)      
				{
					if(strstr(pcpFields,pclFieldName) == NULL)
					{
						dbg(DEBUG,"Field <%s> not in field list",pclFieldName);
						continue;
					}
				}
				dbg(TRACE,"%s: <%s>",
						pclFieldName,PFIELDVAL((prpArray),pcpBuf,ilFieldNo-1));
			}
	}

	return ilRc;

}
static int DumpArray(ARRAYINFO *prpArray,char *pcpFields)
{
	int ilRc;
	int ilRecord = 0;
	long llRowNum = ARR_FIRST;
	char *pclRowBuf;

	dbg(TRACE,"Arraydump: %s", prpArray->crArrayName);
	if (pcpFields != NULL)
	{
		dbg(TRACE,"Arraydump: Fields = %s", pcpFields);
	}
	else
	{
		dbg(TRACE,"Arraydump: Fields = %s",prpArray->crArrayFieldList);
	}

	while(CEDAArrayGetRowPointer(&prpArray->rrArrayHandle,
		prpArray->crArrayName,llRowNum,(void *)&pclRowBuf) == RC_SUCCESS)       
	{
			int ilFieldNo;
		
			ilRecord++;
			llRowNum = ARR_NEXT;

			dbg(TRACE,"Record No: %d",ilRecord);
			for (ilFieldNo = 1; ilFieldNo <= prpArray->lrArrayFieldCnt; ilFieldNo++)
			{
				char pclFieldName[24];

				*pclFieldName = '\0';
				GetDataItem(pclFieldName,prpArray->crArrayFieldList,
					ilFieldNo,',',"","\0\0");
				if (pcpFields != NULL)      
				{
					if(strstr(pcpFields,pclFieldName) == NULL)
					{
						dbg(DEBUG,"Field <%s> not in field list",pclFieldName);
						continue;
					}
				}
				dbg(TRACE,"%s: <%s>",
						pclFieldName,PFIELDVAL((prpArray),pclRowBuf,ilFieldNo-1));
			}
	}

	return ilRc;

}



/***
char cgClientString[] = "\042\047\012\015\054\072\173\175\133\135\174\136\077\050\051\073\100\000";
char cgServerString[] = "\260\261\264\263\262\277\223\224\225\226\227\230\231\233\234\235\236\000";
*************/



static char cgClientString[] = "\174\042\047\054\012\015\000";
static char cgServerString[] = "\227\260\261\262\264\263\000";


static int ConvertDbStringToClient(char *pcpString)
{
     char *pclPos;
		int ilPos;

			while(*pcpString != '\0')
			{
				pclPos = strchr(cgServerString,*pcpString);
				if (pclPos != NULL)
				{
					ilPos = pclPos - cgServerString;
					*pcpString = cgClientString[ilPos];
				}
			pcpString++;
			}
		return RC_SUCCESS;
}

static int ConvertClientStringToDb(char *pcpString)
{
	char *pclPos;
	int ilPos;
	char *pclTmpString;

	while(*pcpString != '\0')
	{
		pclPos = strchr(cgClientString,*pcpString);
		if (pclPos != NULL)
		{
			ilPos = pclPos - cgClientString;
			*pcpString = cgServerString[ilPos];
		}
	pcpString++;
	}
	return RC_SUCCESS;
}




