#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/Sin/SIN_Server/Base/Server/Interface/totams.c 1.6 2004/08/12 00:06:16SGT jim Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program                                                         */
/*                                                                            */
/* Author         : JIM                                                       */
/* Date           : 11. Feb. 2003                                             */
/* Description    : TAMS to UFIS Inteface, receives updates and requests via  */
/*                  an CEDA MQ Interface. The TAMSIN sends the updates to     */
/*                  IMPMGR and request to TOTAMS                              */
/*                                                                            */
/* Update history :                                                           */
/* 20021127 JIM: init                                                         */
/* 20030509 JIM: process delete msg on move of CKIC                           */
/* 20030722 JIM: process delete by CCKD by evaluating old data                */
/* 20040115 JIM: on inserts second line is empty and pclOldData is NULL cores,*/
/*               see PRF 5617                                                 */
/* 20040116 JIM: TAMS flight day in UTC                                       */
/* 20040810 JIM: mark mks_version as possibly unused, removed some unused vars*/
/******************************************************************************/
/*                                                                            */
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a CEDA main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "oracle.h"
#include "db_if.h"
#include "l_ccstime.h"

void Init_Process(void);

/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
int  debug_level = DEBUG;
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static int   igInitOK      = FALSE;
static char  cgConfigFile[512];
/******************************************************************************/
/* Function prototypes                                                          */
/******************************************************************************/
static int Init_Main();
static int    Reset(void);                       /* Reset program          */
static void    Terminate(void);                   /* Terminate program      */
static void    HandleSignal(int);                 /* Handles signals        */
static void    HandleErr(int);                    /* Handles general errors */
static void    HandleQueErr(int);                 /* Handles queuing errors */
static int    HandleData(void);                  /* Handles event data     */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
    int    ilRC = RC_SUCCESS;            /* Return code            */
    int    ilCnt = 0;
    
    INITIALIZE;            /* General initialization    */

    dbg(TRACE,"MAIN: <%s>",mks_version);
    /* Attach to the MIKE queues */
    do
    {
        ilRC = init_que();
        if(ilRC != RC_SUCCESS)
        {
            dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        }/* end of if */
    }while((ilCnt < 10) && (ilRC != RC_SUCCESS));
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
        sleep(60);
        exit(1);
    }else{
        dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
    }/* end of if */

    sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
    ilRC = TransferFile(cgConfigFile);
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */
    /* uncomment if necessary */
    /* sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name); */
    /* ilRC = TransferFile(cgConfigFile); */
    /* if(ilRC != RC_SUCCESS) */
    /* { */
    /*     dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile); */
    /* } */ /* end of if */
    ilRC = SendRemoteShutdown(mod_id);
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    } /* end of if */
    if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
        dbg(DEBUG,"MAIN: waiting for status switch ...");
        HandleQueues();
    }/* end of if */
    if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
        dbg(TRACE,"MAIN: initializing ...");
        if(igInitOK == FALSE)
        {
            ilRC = Init_Main();
            if(ilRC != RC_SUCCESS)
            {
                dbg(TRACE,"Init_Main: init failed!");
            } /* end of if */
        }/* end of if */
    } else {
        Terminate();
    }/* end of if */
    dbg(TRACE,"MAIN: initializing OK");
    for(;;)
    {
        ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text;
                
        if( ilRC == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRC != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRC);
            } /* fi */
            
            switch( prgEvent->command )
            {
            case    HSB_STANDBY    :
                ctrl_sta = prgEvent->command;
                HandleQueues();
                break;    
            case    HSB_COMING_UP    :
                ctrl_sta = prgEvent->command;
                HandleQueues();
                break;    
            case    HSB_ACTIVE    :
                ctrl_sta = prgEvent->command;
                break;    
            case    HSB_ACT_TO_SBY    :
                ctrl_sta = prgEvent->command;
                /* CloseConnection(); */
                HandleQueues();
                break;    
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate();
                break;    
            case    HSB_STANDALONE    :
                ctrl_sta = prgEvent->command;

                break;    

            case    SHUTDOWN    :
                /* process shutdown - maybe from uutil */
                Terminate();
                break;
                    
            case    RESET        :
                ilRC = Reset();
                break;
                    
            case    EVENT_DATA    :
                if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
                {
                    ilRC = HandleData();
                    if(ilRC != RC_SUCCESS)
                    {
                        HandleErr(ilRC);
                    }/* end of if */
                }else{
                    dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
                    DebugPrintItem(TRACE,prgItem);
                    DebugPrintEvent(TRACE,prgEvent);
                }/* end of if */
                break;
                    
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;
            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;
            default            :
                dbg(TRACE,"MAIN: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        } else {
            /* Handle queuing errors */
            HandleQueErr(ilRC);
        } /* end else */
        
    } /* end for */
    
    exit(0);
    
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int Init_Main()
{
    int    ilRC = RC_SUCCESS;            /* Return code */
    /* now reading from configfile or from database */
    SetSignals(HandleSignal);
    igInitOK = TRUE;
    debug_level =DEBUG;
    Init_Process();
    return(ilRC);
    
} /* end of initialize */

/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
    int    ilRC = RC_SUCCESS;                /* Return code */
    
    dbg(TRACE,"Reset: now resetting");
    
    return ilRC;
    
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate()
{

    dbg(TRACE,"Terminate: now leaving ...");
    
    exit(0);
    
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int pipSig)
{
    dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);
    switch(pipSig)
    {
    default    :
        Terminate();
        break;
    } /* end of switch */
    exit(0);
    
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
    return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    
    switch(pipErr) {
    case    QUE_E_FUNC    :    /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :    /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;
    case    QUE_E_SEND    :    /* Error using msgsnd */
        dbg(TRACE,"<%d> : msgsnd failed",pipErr);
        break;
    case    QUE_E_GET    :    /* Error using msgrcv */
        dbg(TRACE,"<%d> : msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX    :
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE    :
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES    :
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        break;
    case    QUE_E_RESP    :    /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;
    case    QUE_E_FULL    :
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG    :    /* No message on queue */
        dbg(TRACE,"<%d> : no messages on queue",pipErr);
        break;
    case    QUE_E_INVORG    :    /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :    /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;
    default            :    /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
    } /* end switch */
         
    return;
} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
    int    ilRC = RC_SUCCESS;            /* Return code */
    int    ilBreakOut = FALSE;
    
    do
    {
        ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text;    
        if( ilRC == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRC != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRC);
            } /* fi */
        
            switch( prgEvent->command )
            {
            case    HSB_STANDBY    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_COMING_UP    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_ACTIVE    :
                ctrl_sta = prgEvent->command;
                ilBreakOut = TRUE;
                break;    
            case    HSB_ACT_TO_SBY    :
                ctrl_sta = prgEvent->command;
                break;    
    
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate();
                break;    
    
            case    HSB_STANDALONE    :
                ctrl_sta = prgEvent->command;

                ilBreakOut = TRUE;
                break;    

            case    SHUTDOWN    :
                Terminate();
                break;
                        
            case    RESET        :
                ilRC = Reset();
                break;
                        
            case    EVENT_DATA    :
                dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
                    
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;
            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;
            default            :
                dbg(TRACE,"HandleQueues: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        } else {
            /* Handle queuing errors */
            HandleQueErr(ilRC);
        } /* end else */
    } while (ilBreakOut == FALSE);
    if(igInitOK == FALSE)
    {
            ilRC = Init_Main();
            if(ilRC != RC_SUCCESS)
            {
                dbg(TRACE,"Init_Main: init failed!");
            } /* end of if */
    }/* end of if */
    /* OpenConnection(); */
} /* end of HandleQueues */
    


/******************************************************************************/
/* All before is from "dummy.c" and is the usual CEDA init and queue handling */
/* Exception: The predefinition of the process initialization "Init_Process"  */
/* All after this is specific to TAMSIN                                       */
/******************************************************************************/

static char  pcgRecvName[64] ;  /* BC-Head used as WKS-Name */
static char  pcgDestName[64] ;  /* BC-Head used as USR-Name */
static char  pcgTwStart[64] ;   /* BC-Head used as Stamp */
static char  pcgTwEnd[64] ;     /* BC-Head used as Stamp */
static char  pcgChkWks[128] ;   /* WKS-Name */
static char  pcgChkUsr[128] ;   /* USR-Name */
static char  pcgActCmd[16];     /* EventCommand */
static int   igUseShm = TRUE;
static int   igTamsQue = 0;     /* Queue ID of TAMS WMQ for CheckIn Counter upd.*/
static char  pcgCfgFile[256];

/******************************************************************************/
static int GetCfg (char *pcpGroup, char *pcpLine, short spNoOfLines, char *pcpDestBuff, char *pcpDefault)
{
  int ilRC = RC_SUCCESS;

  pcpDestBuff[0] = 0x00;
  ilRC = iGetConfigRow (pcgCfgFile, pcpGroup, pcpLine, spNoOfLines, pcpDestBuff);
  if (ilRC != RC_SUCCESS)
  {
    /* use default entry */
    strcpy (pcpDestBuff, pcpDefault);
  }                             /* end if */
  return ilRC;
}                               /* end GetCfg */

/******************************************************************************/
/* Routine: GetOutModId
   first check for QueueId of pcpProcName in my .cfg, next if -1 returned check
   for QueueId of pcpProcName in PNTAB. 
   If Zero is returned, no queue output is done.
*/
/******************************************************************************/
static int GetOutModId (char *pcpProcName)
{
  int ilQueId = -1;
  char pclCfgCode[32];

  /* here temporarily we use pcgChkWks and pcgChkUsr */ 
  sprintf (pclCfgCode, "QUE_TO_%s", pcpProcName);
  dbg (TRACE, "trying to find queue id of <[QUEUE_DEFINES]/%s> in <%s>",
              pcpProcName,pcgCfgFile);
  (void) GetCfg ("QUEUE_DEFINES", pclCfgCode, CFG_STRING, pcgChkWks, "-1");
  get_real_item (pcgChkUsr, pcgChkWks, 1);
  ilQueId = atoi (pcgChkUsr);
  if (ilQueId < 0)
  { /* not configured,  */
    dbg (TRACE, "queue id <%s> not found in <%s>",pcpProcName,pcgCfgFile);
    dbg (TRACE, "trying to find queue id of <%s> in SGSTAB/SHM",pcpProcName);
    ilQueId = tool_get_q_id (pcpProcName);
    if (ilQueId <= 0)
    {
       dbg (TRACE, "queue id <%s> not found in SGSTAB/SHM",pcpProcName);
       dbg (TRACE, "setting queue id of <%s> hardcoded!",pcpProcName);
       ilQueId = 7284;
    }
    else
    {
       dbg (TRACE, "SGSTAB/SHM queue id of <%s> is <%d>",pcpProcName,ilQueId);
    }
  }                             /* end if */
  else
  {
    dbg (TRACE, "queue id of <%s> in <%s> is <%d>",pcpProcName,pcgCfgFile,ilQueId);
  }

  return ilQueId;
}                               /* end of GetOutModId */

/******************************************************************************/
/* Routine: Init_Process
   first initialization (a second initialization is done on first queue 
   event (for debugging)) 
*/
/******************************************************************************/
void Init_Process(void)
{ 
  int ilTryAgain = 10;
  int ilCnt = 0;
  int ilWaitSec;
  int ilWaitTtl;
  int ilRC = RC_SUCCESS;
  char pclActPath[128];
  char *pclPtrPath;

    /* Attach Config File */
  if ((pclPtrPath = getenv ("CFG_PATH")) == NULL)
  {
    strcpy (pclActPath, "/ceda/conf");
  }                           /* end if */
  else
  {
    strcpy (pclActPath, pclPtrPath);
  }                           /* end else */
  sprintf (pcgCfgFile, "%s/%s.cfg", pclActPath, mod_name);
  dbg (TRACE, "using <%s> as config file",pcgCfgFile);

  igTamsQue = GetOutModId ("tams");
  
  /* Init the DataBase (Login) */
  if (ilRC == RC_SUCCESS)
  {
    ilCnt = ilTryAgain;
    ilWaitSec = 5;
    ilWaitTtl = 0;
    do
    {
      ilRC = init_db ();
      if (ilRC != RC_SUCCESS)
      {
        sleep (ilWaitSec);
        ilCnt--;
        ilWaitTtl += ilWaitSec;
      }                         /* end if */
    }
    while ((ilCnt > 0) && (ilRC != RC_SUCCESS));

    if ((ilRC != RC_SUCCESS) || (ilWaitTtl > 0))
    {
      dbg (TRACE, "WAITED %d SECONDS FOR LOGIN DATABASE", ilWaitTtl);
      if (ilRC != RC_SUCCESS)
      {
        dbg (TRACE, "LOGIN DATABASE FAILED, NOW TERMINATING!");
        Terminate();
      }                         /* end if */
    }                           /* end if */
  }                             /* end if */

  dbg(TRACE,"Init_Process: OK");
}

/******************************************************************************/
/* The GetBasicData routine                                                   */
/* check for i.e. ALC2 by known ALC2, first look in shared memory, when not   */
/* found there, check data base                                               */
/******************************************************************************/
static int GetBasicData (char *pcpTable, char *pcpFields, char *pcpData, char *pcpResultFields, char *pcpResultData,
                         int ipMaxLines, int ipDefRc)
{
  int ilRC = RC_FAIL;
  int ilGetRc = RC_SUCCESS;
  int ilCount = 0;
  int ilUseOra = FALSE;
  short slCursor = 0;
  short slFkt = START;
  char *pclNlPos;
  char pclSqlBuf[512];
  char pclSqlKey[128];
  char pclTmpBuf3[128] = "";

  pcpResultData[0] = 0x00;


  if (igUseShm ==TRUE)
  {
    ilRC = RC_SUCCESS;
  }  
/*
  if (igUseShm == TRUE)
  {
    ilRC = CheckSysShmField (pcpTable, pcpFields, RC_SUCCESS);
    ilRC = CheckSysShmField (pcpTable, pcpResultFields, ilRC);
  }                             * end if *
*/

  if (ilRC == RC_SUCCESS)
  {
    ilCount = ipMaxLines;
/* ignore HOPO-Gedoehns: */
      strcpy (pclSqlBuf, pcpFields);
      strcpy (pclSqlKey, pcpData);
/*
    if (strcmp (pcgDefTblExt, "TAB") == 0)
    {
      sprintf (pclSqlBuf, "%s,HOPO", pcpFields);
      sprintf (pclSqlKey, "%s,%s", pcpData, pcgH3LC);
    }                           * end if *
    else
    {
      strcpy (pclSqlBuf, pcpFields);
      strcpy (pclSqlKey, pcpData);
    }                           * end else *
*/
    dbg (DEBUG, "SYSLIB: TBL <%s> FLD <%s> DAT <%s>", pcpTable, pclSqlBuf, pclSqlKey);
    ilRC = syslibSearchDbData (pcpTable, pclSqlBuf, pclSqlKey, pcpResultFields, pclTmpBuf3, &ilCount, "\n");
    if (ilRC == RC_SUCCESS)
    {
      if ((ipMaxLines < 1) || (ipMaxLines > ilCount))
      {
        ipMaxLines = ilCount;
      }                         /* end if */
      pclNlPos = pclTmpBuf3;
      pclNlPos++;
      while (ipMaxLines > 0)
      {
        pclNlPos = strstr (pclNlPos, "\n");
        if (pclNlPos != NULL)
        {
          ipMaxLines--;
          pclNlPos++;
        }                       /* end if */
        else
        {
          ipMaxLines = 0;
        }                       /* end else */
      }                         /* end while */
      if (pclNlPos != NULL)
      {
        pclNlPos--;
        if (*pclNlPos == '\n')
        {
          *pclNlPos = 0x00;
          dbg (TRACE, "DATA CUT! \n<%s>", pclTmpBuf3);
        }                       /* end if */
      }                         /* end if */
      strcpy (pcpResultData, pclTmpBuf3);
    }                           /* end if */
    else
    {
      if (ilRC == RC_FAIL)
      {
        ilUseOra = TRUE;
        igUseShm = FALSE;
        dbg (TRACE, "=====================================================");
        dbg (TRACE, "ERROR: SHARED MEMORY FAILURE !!");
        dbg (TRACE, "USE TBL <%s>", pcpTable);
        dbg (TRACE, "GET FLD <%s>", pcpResultFields);
        dbg (TRACE, "KEY FLD <%s>", pcpFields);
        dbg (TRACE, "KEY DAT <%s>", pcpData);
        dbg (TRACE, "GET BASIC DATA SWITCHED TO USE OF ORACLE");
        dbg (TRACE, "=====================================================");
      }                         /* end if */
      if (ilRC == RC_NOT_FOUND)
      {
        ilRC = ipDefRc;
      }                         /* end if */
    }                           /* end else */
  }                             /* end if */
  else
  {
    ilUseOra = TRUE;
  }                             /* end else */

  if (ilUseOra == TRUE)
  {
    sprintf (pclSqlKey, "WHERE %s='%s'", pcpFields, pcpData);
/* ignore HOPO-Gedoehns: 
    CheckWhereClause (TRUE, pclSqlKey, FALSE, TRUE, "\0");
*/
    sprintf (pclSqlBuf, "SELECT %s FROM %s %s", pcpResultFields, pcpTable, pclSqlKey);
    dbg (TRACE, "<%s>", pclSqlBuf);
    slFkt = START;
    slCursor = 0;
    ilGetRc = sql_if (slFkt, &slCursor, pclSqlBuf, pcpResultData);
    if (ilGetRc == DB_SUCCESS)
    {
      BuildItemBuffer (pcpResultData, pcpResultFields, 0, ",");
      ilRC = RC_SUCCESS;
    }                           /* end if */
    else
    {
      ilRC = ipDefRc;
    }                           /* end else */
    close_my_cursor (&slCursor);
  }                             /* end else */
  dbg (DEBUG, "RESULT <%s> <%s>", pcpResultFields, pcpResultData);
  return ilRC;
}                               /* end GetBasicData */

/******************************************************************************/
/* The ProcessErr routine                                                     */
/* In case of errors, which disable further data processing, this routine     */
/* sends an ERR back to the TAMS process                                      */
/******************************************************************************/
void ProcessErr(char *pclSelection)
{
   SendCedaEvent(prgEvent->originator,0,"TOTAMS","",pcgTwStart,pcgTwEnd,
                 "ERR","",pclSelection,"","","", 5,RC_SUCCESS) ;
}

/******************************************************************************/
/* The SkipBlanksLeftRight routine                                            */
/* This routine set the first trailing blank in pcpIn to '\0' and returns the */
/* non blank beginnig of the Strings                                          */
/* Very quick, but it modifies pcpIn and returns a pointer within pcpIn !     */
/******************************************************************************/
char *SkipBlanksLeftRight(char *pcpIn)
{
   int ilLen=0;
   char *pclDummy= pcpIn;

   /* dbg(TRACE,"SkipBlanksLeftRight: <%s>",pclDummy); */
   if (pclDummy != NULL)
   {
      /* set pointer to first non blank char: */
      for ( ;  (pclDummy[0] != 0 ) && (pclDummy[0] == ' ') ; pclDummy++ ) 
      {
         ;  
      }
      
      /* pointer now is first non blank char, may be \0 : */
      if ( pclDummy[0] != 0 )
      {
         /* not \0 , so ilLen must get now >= 0 : */
         ilLen=strlen(pclDummy)-1;
         for ( ; (ilLen > 0) && (pclDummy[ilLen] == ' ') ; ilLen-- )
         {
            ;  
         }
         pclDummy[ilLen+1]=0;
      }
   }
   /* dbg(TRACE,"SkipBlanksLeftRight: <%s>",pclDummy); */
   return pclDummy;
}

static char pcgSqlBuf[1024];
static char pcgDataArea[1024];
static char pcgSel[1024];
static char pcgOut[1024];

void GetFieldVal(char* pcpFld, char* pcpDat, char* pcpField, char* pcpVal)
{
  int ilNum;
  
  /* dbg(TRACE,"GetFieldValue: Fields <%s> ",pcpFld); */
  ilNum= get_item_no (pcpFld, pcpField, strlen(pcpField)+1) + 1;
  if (ilNum > 0)
  {
     (void) get_real_item (pcpVal, pcpDat, ilNum);
  }
  else
  {
     pcpVal[0]= 0;
  }
  dbg(TRACE,"GetFieldValue: %s: <%s> <#%d>",pcpField,pcpVal,ilNum);
}

void SendCCA2Tams(char *pcpOutCmd,char *pcpFlno,char *pcpStod,
                  char *pcpCkic, char *pcpCkil,
                  char *pcpCkbs,char *pcpCkes)
{

   if (pcpOutCmd[0] == 0)
   {
      dbg(TRACE,"SendCCA2Tams: missing value: Cmd! not processed...");
      return;
   }
   if (pcpFlno[0] == 0)
   {
      dbg(TRACE,"SendCCA2Tams: missing value: FLNO! not processed...");
      return;
   }
   if (pcpStod[0] == 0)
   {
      dbg(TRACE,"SendCCA2Tams: missing value: STOD! not processed...");
      return;
   }
   if (pcpCkic[0] == 0)
   {
      dbg(TRACE,"SendCCA2Tams: missing value: CKIC! not processed...");
      return;
   }
   if (pcpCkil[0] == 0)
   {
      dbg(TRACE,"SendCCA2Tams: missing value: CICL! not processed...");
      return;
   }
   if (pcpCkbs[0] == 0)
   {
      dbg(TRACE,"SendCCA2Tams: missing value: CKBS! not processed...");
      return;
   }
   if (pcpCkes[0] == 0) 
   {
      dbg(TRACE,"SendCCA2Tams: missing value: CKES! not processed...");
      return;
   }
   /* now we will calculate local times from UFIS UTC times: */
   UtcToLocalTimeFixTZ(pcpCkbs);
   UtcToLocalTimeFixTZ(pcpCkes);
   /* 20040116 JIM: TAMS flight day in UTC: */
   UtcToLocalTimeFixTZ(pcpStod);
   pcpStod[8] = 0; /* ignore HHMMSS of STOD */
   /* C FLNO      FLDA    NR   ROW  CKBS          CKES           */
   /* CU,DLH4711S  ,20030131,12345,12345,20030131000000,20030131235959 */
   sprintf(pcgOut,"%2s,%-10s,%8s,%5s,%5s,%14s,%14s\n",
           pcpOutCmd,pcpFlno,pcpStod,pcpCkic, pcpCkil,pcpCkbs,pcpCkes);
   dbg (TRACE, "SendCCA2Tams: : sending: <%s>", pcgOut);
   SendCedaEvent(igTamsQue,0,"TOTAMS","",pcgTwStart,pcgTwEnd,
                 "OUT","","","",pcgOut,"", 5,RC_SUCCESS) ;
}

/******************************************************************************/
/* The ProcessTamsRqCCA routine                                                  */
/******************************************************************************/
void ProcessTamsRqCCA(char *pcpFld, char *pcpDat, char *pclSel)
{
  int ilGetRc = RC_SUCCESS;
  int ilRC = RC_SUCCESS;
  int ilLine= 0;
  short slFkt = 0;
  short slLocalCursor = 0;
  char *pclCkbsIn;
  char *pclCkesIn;
  char *pclTmp;
  char pclCkbs[32], pclCkes[32];
  char pclFlno[32], pclStod[32], pclCkic[32], pclCkil[32];
  char pclOutCmd[] = "CU";
  
    pclCkbsIn= strtok(pcpDat,",");
    pclCkesIn= strtok(NULL,",");
    if ( (pclCkesIn!=NULL) && (pclCkesIn!=NULL) )
    {
      if (strcmp(pclCkbsIn,pclCkesIn)>0)
      {
         pclTmp=pclCkesIn;
         pclCkesIn=pclCkbsIn;
         pclCkbsIn=pclTmp;
      }
      /* TAMS request comes with local times */
      /* now we will calculate UFIS UTC times from local times for DB access: */
      LocalTimeToUtcFixTZ(pclCkbsIn);
      LocalTimeToUtcFixTZ(pclCkesIn);
      dbg(TRACE,"ProcessTamsRqCCA: getting dedicated CheckIn Counter:");
      slFkt = START;
      sprintf (pcgSqlBuf, "SELECT CCA.FLNO,AFT.STOD,CKIC,CKBS,CKES FROM CCATAB CCA, AFTTAB AFT "
                          "WHERE AFT.URNO = CCA.FLNU  AND CCA.FLNU IN "
             "(SELECT URNO FROM AFTTAB WHERE (STOD>='%s'  AND STOD<='%s' AND CKIF<>' ')) "
             "AND CCA.CKIC<>' '"
               ,pclCkbsIn,pclCkesIn);
      dbg (DEBUG, "ProcessTamsRqCCA: pcgSqlBuf: <%s>", pcgSqlBuf);
      slLocalCursor = 0;
      slFkt = START;
      ilLine= 0;
      strcpy(pclOutCmd,"CU");
      
      pcgDataArea[0] = 0x00;
      while ((ilGetRc = sql_if (slFkt, &slLocalCursor, pcgSqlBuf, pcgDataArea) == DB_SUCCESS))
      {
        ilLine++;
        BuildItemBuffer (pcgDataArea, "", 5, ",");
        dbg (TRACE, "ProcessTamsRqCCA: Line <%d> <%s>", ilLine,pcgDataArea);
        (void) get_real_item (pclFlno, pcgDataArea, 1);
        (void) get_real_item (pclStod, pcgDataArea, 2);
        (void) get_real_item (pclCkic, pcgDataArea, 3);
        ilRC = GetBasicData ("CICTAB", "CNAM", pclCkic, "CICL", pclCkil, 1, RC_FAIL);
        if (ilRC == RC_FAIL)
        {
           strcpy(pclCkil,"     ");
        }
        (void) get_real_item (pclCkbs, pcgDataArea, 4);
        (void) get_real_item (pclCkes, pcgDataArea, 5);

        SendCCA2Tams(pclOutCmd,pclFlno,pclStod,pclCkic,pclCkil,pclCkbs,pclCkes);

        slFkt = NEXT;
        pcgDataArea[0] = 0x00;
      }
      dbg (TRACE, "sql_if finished with <%d>",ilGetRc);
      close_my_cursor (&slLocalCursor);

      dbg(TRACE,"ProcessTamsRqCCA: getting Common CheckIn Counter:");
      slFkt = START;
      sprintf (pcgSqlBuf, "SELECT AF.FLNO,AF.STOD,CC.CKIC,CC.CKBS,CC.CKES "
             "FROM AFTTAB AF, ALTTAB AL , CCATAB CC "
             "WHERE AF.ALC2 = AL.ALC2 AND AF.ALC3 = AL.ALC3 AND "
             "AF.STOD BETWEEN CC.CKBS AND CC.CKES AND " /* STOD must be in CkeckIn Counter time range */
             "AF.STOD BETWEEN '%s' AND '%s' AND "       /* and in requested time range */
             "AL.URNO = CC.FLNU AND "                   /* get Airline Urno from CCATAB */
             "(CC.CKBS BETWEEN '%s' AND '%s' "          /* (Begin in requested time range */
             " OR CC.CKES BETWEEN '%s' AND '%s' "       /*  or End in requested time range */
             " OR (CC.CKBS < '%s' AND CC.CKES  > '%s') "/*  or requested time range between Begin and End */
             ") AND CC.CTYP <> ' ' AND CC.CKIC <> ' '"  /* ) Common or Precheck Checkin, not deleted */
               ,pclCkbsIn,pclCkesIn,pclCkbsIn,pclCkesIn,
                pclCkbsIn,pclCkesIn,pclCkbsIn,pclCkesIn);
      dbg (DEBUG, "ProcessTamsRqCCA: pcgSqlBuf: <%s>", pcgSqlBuf);
      slLocalCursor = 0;
      slFkt = START;
      ilLine= 0;
      strcpy(pclOutCmd,"CU");
      
      pcgDataArea[0] = 0x00;
      while ((ilGetRc = sql_if (slFkt, &slLocalCursor, pcgSqlBuf, pcgDataArea) == DB_SUCCESS))
      {
        ilLine++;
        BuildItemBuffer (pcgDataArea, "", 5, ",");
        dbg (TRACE, "ProcessTamsRqCCA: Line <%d> <%s>", ilLine,pcgDataArea);
        (void) get_real_item (pclFlno, pcgDataArea, 1);
        (void) get_real_item (pclStod, pcgDataArea, 2);
        (void) get_real_item (pclCkic, pcgDataArea, 3);
        ilRC = GetBasicData ("CICTAB", "CNAM", pclCkic, "CICL", pclCkil, 1, RC_FAIL);
        if (ilRC == RC_FAIL)
        {
           strcpy(pclCkil,"     ");
        }
        (void) get_real_item (pclCkbs, pcgDataArea, 4);
        (void) get_real_item (pclCkes, pcgDataArea, 5);

        SendCCA2Tams(pclOutCmd,pclFlno,pclStod,pclCkic,pclCkil,pclCkbs,pclCkes);

        slFkt = NEXT;
        pcgDataArea[0] = 0x00;
      }
      dbg (TRACE, "sql_if finished with <%d>",ilGetRc);
      close_my_cursor (&slLocalCursor);
   }
}

/******************************************************************************/
/* The CcaActionToTams routine                                                */
/* This routine acts on queue messages received from ACTION for Checkin       */
/* Counter updates. In first run the NEW data is processed. In second run,    */
/* OLD data is checked for change of CKIC. In this case a delete message is   */
/* generated.                                                                 */
/******************************************************************************/
void CcaActionToTams(char *pcpFld, char *pcpDat, char *pclSel)
{
  int ilGetRc = RC_SUCCESS;
  int ilRC = RC_SUCCESS;
  int ii, ilLine= 0;
  short slFkt = 0;
  short slLocalCursor = 0;
  char pclUrno[32], pclCtyp[32], pclCkbs[32], pclCkes[32], pclCkba[32], pclCkea[32];
  char pclFlno[32], pclStod[32], pclCkic[32], pclCkil[32];
  char *pclLine;
  char *pclOldData;
  int ilInitLine= 1;
  char pclOutCmd[] = "CU";
  
  pclLine=pcpDat;
  pclOldData=strtok(pcpDat,"\n");
  pclOldData=strtok(NULL,"\n");
  dbg(TRACE,"CcaActionToTams: pcpDat <%s> ",pcpDat);
  
  /* there are two ways to delete CCA: rename CKIC or send CCKD */
  if (strcmp(pcgActCmd,"CCKI") == 0)
  {
     ilInitLine= 1;
     strcpy(pclOutCmd,"CU");
     dbg(TRACE,"CcaActionToTams: evaluating NEW data record");
     GetFieldVal(pcpFld, pclLine, "CKIC", pclCkic); /* counter name */
  }
  else if (strcmp(pcgActCmd,"CCKD") == 0)
  {
     ilInitLine= 2;
     pclLine= pclOldData;
     strcpy(pclOutCmd,"CD");
     dbg(TRACE,"CcaActionToTams: deleting by OLD data record");
     GetFieldVal(pcpFld, pclLine, "CKIC", pclCkic); /* counter name */
  }

  for (ii=ilInitLine ; ii<3 ; ii++)
  {
     GetFieldVal(pcpFld, pclLine, "FLNU", pclUrno); /* ' ': AFTTAB Urno, 'C' (or 'P'): ALTTAB Urno */
     if (pclCkic[0]==0)
     {
        dbg(TRACE,"CcaActionToTams: CKIC is empty, nothing to do.");
     }
     else if (pclUrno[0]==0)
     {
        dbg(TRACE,"CcaActionToTams: URNO is empty, not processed!");
     }
     else
     {
       GetFieldVal(pcpFld, pclLine, "CTYP", pclCtyp); /* counter type: 'C' commen, ('P' pre-Checkin), ' ' dedicated */
       GetFieldVal(pcpFld, pclLine, "CKBS", pclCkbs); /* scheduled begin */
       GetFieldVal(pcpFld, pclLine, "CKES", pclCkes); /* scheduled end */
       GetFieldVal(pcpFld, pclLine, "CKBA", pclCkba); /* actual    begin */
       GetFieldVal(pcpFld, pclLine, "CKEA", pclCkea); /* actual    end */
       if ((pclCkbs[0]==0) || (pclCkes[0]==0))
       {
          dbg(TRACE,"CcaActionToTams: missing one of CKBS,BKES ");
          dbg(TRACE,"CcaActionToTams: record not processed");
       }
       else if (pclCtyp[0]==0)
       { /* dedicated CkeckIn Counter */
          dbg(TRACE,"CcaActionToTams: no CTYP: dedicated CkeckIn Counter assumed for <%s>",pclUrno);

          sprintf (pcgSqlBuf, "SELECT FLNO,STOD FROM AFTTAB WHERE URNO = %s",pclUrno);
          dbg (DEBUG, "CcaActionToTams: pcgSqlBuf: <%s>", pcgSqlBuf);
          slLocalCursor = 0;
          slFkt = START;
          ilLine= 0;
          
          pcgDataArea[0] = 0x00;
          if ((ilGetRc = sql_if (slFkt, &slLocalCursor, pcgSqlBuf, pcgDataArea) == DB_SUCCESS))
          {
            ilLine++;
            BuildItemBuffer (pcgDataArea, "", 2, ",");
            dbg (TRACE, "CcaActionToTams: Line <%d> <%s>", ilLine,pcgDataArea);
            (void) get_real_item (pclFlno, pcgDataArea, 1);
            (void) get_real_item (pclStod, pcgDataArea, 2);
            ilRC = GetBasicData ("CICTAB", "CNAM", pclCkic, "CICL", pclCkil, 1, RC_FAIL);
            if (ilRC == RC_FAIL)
            {
               strcpy(pclCkil,"     ");
            }
            SendCCA2Tams(pclOutCmd,pclFlno,pclStod,pclCkic,pclCkil,pclCkbs,pclCkes);

            slFkt = NEXT;
            pcgDataArea[0] = 0x00;
          }
          close_my_cursor (&slLocalCursor);
       }
       else
       { /* common CkeckIn Counter */
          dbg(TRACE,"CcaActionToTams: no CTYP: common CkeckIn Counter assumed for <%s>",pclUrno);
          sprintf (pcgSqlBuf, "SELECT AFT.FLNO,AFT.STOD FROM AFTTAB AFT, ALTTAB ALT "
                              "WHERE AFT.ALC2 = ALT.ALC2 AND AFT.ALC3 = ALT.ALC3 AND "
                              "ALT.URNO = '%s' AND "
                              "AFT.STOD BETWEEN '%s' AND '%s'",pclUrno,pclCkbs,pclCkes);
          dbg (DEBUG, "CcaActionToTams: pcgSqlBuf: <%s>", pcgSqlBuf);
          slLocalCursor = 0;
          slFkt = START;
          ilLine= 0;
          
          pcgDataArea[0] = 0x00;
          while ((ilGetRc = sql_if (slFkt, &slLocalCursor, pcgSqlBuf, pcgDataArea) == DB_SUCCESS))
          {
            ilLine++;
            BuildItemBuffer (pcgDataArea, "", 2, ",");
            dbg (TRACE, "CcaActionToTams: Line <%d> <%s>", ilLine,pcgDataArea);
            (void) get_real_item (pclFlno, pcgDataArea, 1);
            (void) get_real_item (pclStod, pcgDataArea, 2);
            ilRC = GetBasicData ("CICTAB", "CNAM", pclCkic, "CICL", pclCkil, 1, RC_FAIL);
            if (ilRC == RC_FAIL)
            {
               strcpy(pclCkil,"     ");
            }
            SendCCA2Tams(pclOutCmd,pclFlno,pclStod,pclCkic,pclCkil,pclCkbs,pclCkes);

            slFkt = NEXT;
            pcgDataArea[0] = 0x00;
          }
          close_my_cursor (&slLocalCursor);
       }
     }
     /* 20040115 JIM: on inserts pclOldData is empty and GetFieldVal cores 
        if (ii==1)
     */
     if (ii==1)
     { /* first run, prepare for OLD data */
       if ((pclOldData!=NULL) && (pclOldData[0]!=0))
       {
         dbg(TRACE,"CcaActionToTams: evaluating OLD data record");
         pclLine= pclOldData;
         GetFieldVal(pcpFld, pclLine, "CKIC", pclCkil); /* counter name, but stored in CounterRow to compare */
         if ((pclCkil[0]!=0) && (strcmp(pclCkic,pclCkil) != 0))
         {
           strcpy(pclOutCmd,"CD");
           dbg(TRACE,"CcaActionToTams: Counter name changed, have to send delete message!");
           strcpy(pclCkic,pclCkil);
         }
         else
         {
           ii++; /* leave loop */
         }
       }
       else
       {
         ii++; /* leave loop */
       }
     }
   } /* end for */

}

/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData()
{
  int    ilRC = RC_SUCCESS;            /* Return code */
  BC_HEAD *prlBchd;
  CMDBLK *prlCmdblk;
  char *pclSelKey;
  char *pclFields;
  char *pclData;
  int que_out = 0;

    DebugPrintItem(DEBUG,prgItem);
    DebugPrintEvent(DEBUG,prgEvent);
    que_out = prgEvent->originator;
    prlBchd = (BC_HEAD *) ((char *) prgEvent + sizeof (EVENT));
    prlCmdblk = (CMDBLK *) ((char *) prlBchd->data);
    pclSelKey = (char *) prlCmdblk->data;
    pclFields = (char *) pclSelKey + strlen (pclSelKey) + 1;
    pclData = (char *) pclFields + strlen (pclFields) + 1;

		/* Save Global Elements of BC_HEAD */
		/* WorkStation */
		memset (pcgRecvName, '\0', (sizeof(prlBchd->recv_name) + 1)) ; 
		strncpy (pcgRecvName, prlBchd->recv_name, sizeof(prlBchd->recv_name)) ;

		/* User */
		memset (pcgDestName, '\0', sizeof(prlBchd->recv_name)) ; 
		strcpy (pcgDestName, prlBchd->dest_name) ;   /* UserLoginName */
    sprintf (pcgChkWks, ",%s,", pcgRecvName) ; 
		sprintf (pcgChkUsr, ",%s,", pcgDestName) ; 

		/* SaveGlobal Info of CMDBLK */
		strcpy (pcgTwStart, prlCmdblk->tw_start) ; 
		strcpy (pcgTwEnd, prlCmdblk->tw_end) ; 
    strcpy (pcgActCmd,prlCmdblk->command);

    dbg(TRACE,"CMD <%s>  WKS <%s> USR <%s>",
							pcgActCmd, pcgRecvName, pcgDestName) ; 
    dbg(TRACE,"SPECIAL INFO TWS <%s>  TWE <%s> ", 
							pcgTwStart, pcgTwEnd ); 

    dbg(TRACE,"HandleData: Fields: <%s>",pclFields);
    dbg(TRACE,"HandleData: Data:   <%s>",pclData);
    dbg(TRACE,"HandleData: Sel:    <%s>",pclSelKey);
    
    if (strcmp(pcgActCmd,"RT") == 0)
    {
       ProcessTamsRqCCA(pclFields,pclData,pclSelKey);
    }
    else if ((strcmp(pcgActCmd,"CCKI") == 0) || (strcmp(pcgActCmd,"CCKD") == 0))
    {
       CcaActionToTams(pclFields,pclData,pclSelKey);
    }
    else
    {
       sprintf(pcgSel,"unknown command: <%s>",pcgActCmd);
       ProcessErr(pcgSel);
    }

    dbg(TRACE,"================== START/END ==================");
    return ilRC;
    
} /* end of HandleData */

