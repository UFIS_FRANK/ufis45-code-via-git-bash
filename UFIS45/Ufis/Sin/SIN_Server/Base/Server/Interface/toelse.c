#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Sin/SIN_Server/Base/Server/Interface/toelse.c 1.4 2013/05/15 16:08:47SGT pdi Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :  Hans-Juergen Ebert                                       */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history :                                                           */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */

static char sccs_version[] ="@(#) UFIS44 (c) ABB AAT/I skeleton.c 44.1.0 / 11.12.2002 HEB";

/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */


#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "fditools.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "rmschkrul.h"
#include "syslib.h"
#include "db_if.h"
#include <time.h>
#include <cedatime.h>

#ifdef _HPUX_SOURCE
	extern int daylight;
	extern long timezone;
	extern char *tzname[2];
#else
	extern int _daylight;
	extern long _timezone;
	extern char *_tzname[2];
#endif

int debug_level = TRACE;
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static EVENT *prgOutEvent  = NULL;

static char cgProcessName[20] = "\0";
static char  cgConfigFile[512] = "\0";
static char  cgHopo[8] = "\0";                         /* default home airport    */
static char  cgTabEnd[8] ="\0";                       /* default table extension */

static long lgEvtCnt = 0;
static char cgBulkTimeframe[20] = "\0";
static char cgOnlineTimeframe[20] = "\0";
static long lgBulkTimeframe = 0;
static long lgOnlineTimeframe = 0;
static char cgActualTimeForTest[20] = "\0";
static char cgSendToModid[10] = "\0";
static char cgSendPrio[10] = "\0";
static char cgFilterEmptyCIC[10] = "\0";




/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
static int  Init_Process();
static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,
			   char *pcpCfgBuffer);


static int	Reset(void);                       /* Reset program          */
static void	Terminate(int ipSleep);            /* Terminate program      */
static void	HandleSignal(int);                 /* Handles signals        */
static void	HandleErr(int);                    /* Handles general errors */
static void   HandleQueErr(int);                 /* Handles queuing errors */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/
static void HandleBulkData();
static void HandleOnlineData();
static void SendArrivalBulkData();
static void SendDepartureBulkData();
static int FormatAndSendArrivalData(char *pclArrivalData, char *pclArrivalFields, char *pclActionType);
static int FormatAndSendDepartureData(char *pclDepartureData, char *pclDepartureFields, char *pclActionType);
static void HandleOnlineData(char *pcpFields, char *pcpData);
static int	HandleData(EVENT *prpEvent);       /* Handles event data     */
static int  GetCommand(char *pcpCommand, int *pipCmd);
static void HandleRequest(char *pclFields, char *pclData);
static void	HandleQuery(char *pclData);
static int GetElementValue(char *pclDest, char *pclOrigData, char *pclBegin, char *pclEnd);

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
	int	ilRc = RC_SUCCESS;			/* Return code			*/
	int	ilCnt = 0;
	int ilOldDebugLevel = 0;


	INITIALIZE;			/* General initialization	*/



	debug_level = DEBUG;

	strcpy(cgProcessName,argv[0]);

	dbg(TRACE,"MAIN: version <%s>",sccs_version);

	/* Attach to the MIKE queues */
	do
	{
		ilRc = init_que();
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		}/* end of if */
	}while((ilCnt < 10) && (ilRc != RC_SUCCESS));

	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
		sleep(60);
		exit(1);
	}else{
		dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
	}/* end of if */

	do
	{
		ilRc = init_db();
		if (ilRc != RC_SUCCESS)
		{
			check_ret(ilRc);
			dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		} /* end of if */
	} while((ilCnt < 10) && (ilRc != RC_SUCCESS));

	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
		sleep(60);
		exit(2);
	}else{
		dbg(TRACE,"MAIN: init_db() OK!");
	} /* end of if */

	/* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */

	sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
	dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
	ilRc = TransferFile(cgConfigFile);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */

	sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
	dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
	ilRc = TransferFile(cgConfigFile);
	if(ilRc != RC_SUCCESS)
	{ 
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */

	ilRc = SendRemoteShutdown(mod_id);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
	} /* end of if */

	if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
	{
		dbg(DEBUG,"MAIN: waiting for status switch ...");
		HandleQueues();
	}/* end of if */

	if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
	{
		dbg(TRACE,"MAIN: initializing ...");
			ilRc = Init_Process();
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"Init_Process: init failed!");
			} /* end of if */

	} else {
		Terminate(30);
	}/* end of if */



	dbg(TRACE,"=====================");
	dbg(TRACE,"MAIN: initializing OK");
	dbg(TRACE,"=====================");
	

	for(;;)
	{
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* Acknowledge the item */
		ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
		if( ilRc != RC_SUCCESS ) 
		{
			/* handle que_ack error */
			HandleQueErr(ilRc);
		} /* fi */

		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */

		prgEvent = (EVENT *) prgItem->text;
				
		if( ilRc == RC_SUCCESS )
		{
			
			lgEvtCnt++;

			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				break;	
			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				/* CloseConnection(); */
				HandleQueues();
				break;	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate(1);
				break;	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				break;	
			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;
			case	SHUTDOWN	:
				/* process shutdown - maybe from uutil */
				Terminate(1);
				break;
					
			case	RESET		:
				ilRc = Reset();
				break;
					
			case	EVENT_DATA	:
				if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
				{
					ilRc = HandleData(prgEvent);
					if(ilRc != RC_SUCCESS)
					{
						HandleErr(ilRc);
					}/* end of if */
				}else{
					dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
				}/* end of if */
				break;
				

			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;
			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;
			default			:
				dbg(TRACE,"MAIN: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */

		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		} /* end else */
		
	} /* end for */
	
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/



static int ReadConfigEntry(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer)
{
    int ilRc = RC_SUCCESS;

    char clSection[124] = "\0";
    char clKeyword[124] = "\0";

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRc = iGetConfigEntry(cgConfigFile,clSection,clKeyword,
			   CFG_STRING,pcpCfgBuffer);
    if(ilRc != RC_SUCCESS)
    {
	dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
    } 
    else
    {
	dbg(DEBUG,"Config Entry <%s>,<%s>:<%s> found in %s",
	    clSection, clKeyword ,pcpCfgBuffer, cgConfigFile);
    }/* end of if */
    return ilRc;
}


static int Init_Process()
{
	int  ilRc = RC_SUCCESS;			/* Return code */
  char clSection[64] = "\0";
  char clKeyword[64] = "\0";
  int ilOldDebugLevel = 0;
	long pclAddFieldLens[12];
	char pclAddFields[256] = "\0";
	char pclSqlBuf[2560] = "\0";
	char pclSelection[1024] = "\0";
 	short slCursor = 0;
	short slSqlFunc = 0;
	char  clBreak[24] = "\0";


	if(ilRc == RC_SUCCESS)
	{
		/* read HomeAirPort from SGS.TAB */
		ilRc = tool_search_exco_data("SYS", "HOMEAP", cgHopo);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"<Init_Process> EXTAB,SYS,HOMEAP not found in SGS.TAB");
			Terminate(30);
		} else {
			dbg(TRACE,"<Init_Process> home airport <%s>",cgHopo);
		}
	}

	
	if(ilRc == RC_SUCCESS)
	{

		ilRc = tool_search_exco_data("ALL","TABEND", cgTabEnd);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"<Init_Process> EXTAB,ALL,TABEND not found in SGS.TAB");
			Terminate(30);
		} else {
			dbg(TRACE,"<Init_Process> table extension <%s>",cgTabEnd);
		}
	}

	if (ilRc == RC_SUCCESS)
	{
		ilRc = ReadConfigEntry("MAIN","ACTUAL_TIME_FOR_TEST",cgActualTimeForTest);
		ilRc = ReadConfigEntry("MAIN","SEND_TO_MODID",cgSendToModid);
		ilRc = ReadConfigEntry("MAIN","SEND_PRIO",cgSendPrio);
		ilRc = ReadConfigEntry("BULK_DATA","BULK_TIMEFRAME",cgBulkTimeframe);
		ilRc = ReadConfigEntry("BULK_DATA","FILTER_EMPTY_CIC",cgFilterEmptyCIC);
		ilRc = ReadConfigEntry("ONLINE_DATA","ONLINE_TIMEFRAME",cgOnlineTimeframe);
		lgOnlineTimeframe = atoi(cgOnlineTimeframe) * 60 * 60; /*transform timeframe from hours to seconds*/
	}


	if (ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"Init_Process failed");
	}

		return(ilRc);
	
} /* end of initialize */




/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
	int	ilRc = RC_SUCCESS;				/* Return code */
	
	dbg(TRACE,"Reset: now resetting");
	
	return ilRc;
	
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
	/* unset SIGCHLD ! DB-Child will terminate ! */

	dbg(TRACE,"Terminate: now DB logoff ...");

	signal(SIGCHLD,SIG_IGN);

	logoff();

	dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);


	dbg(TRACE,"Terminate: now leaving ...");

	fclose(outp);

	sleep(ipSleep < 1 ? 1 : ipSleep);
	
	exit(0);
	
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/

static void HandleSignal(int pipSig)
{
	dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);

	switch(pipSig)
	{
	default	:
		Terminate(1);
		break;
	} /* end of switch */

	exit(1);
	
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
	dbg(TRACE,"HandleErr: <%d> : unknown ERROR",pipErr);

	return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
	switch(pipErr) {
	case	QUE_E_FUNC	:	/* Unknown function */
		dbg(TRACE,"<%d> : unknown function",pipErr);
		break;
	case	QUE_E_MEMORY	:	/* Malloc reports no memory */
		dbg(TRACE,"<%d> : malloc failed",pipErr);
		break;
	case	QUE_E_SEND	:	/* Error using msgsnd */
		dbg(TRACE,"<%d> : msgsnd failed",pipErr);
		break;
	case	QUE_E_GET	:	/* Error using msgrcv */
		dbg(TRACE,"<%d> : msgrcv failed",pipErr);
		break;
	case	QUE_E_EXISTS	:
		dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
		break;
	case	QUE_E_NOFIND	:
		dbg(TRACE,"<%d> : route not found ",pipErr);
		break;
	case	QUE_E_ACKUNEX	:
		dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
		break;
	case	QUE_E_STATUS	:
		dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
		break;
	case	QUE_E_INACTIVE	:
		dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
		break;
	case	QUE_E_MISACK	:
		dbg(TRACE,"<%d> : missing ack ",pipErr);
		break;
	case	QUE_E_NOQUEUES	:
		dbg(TRACE,"<%d> : queue does not exist",pipErr);
		break;
	case	QUE_E_RESP	:	/* No response on CREATE */
		dbg(TRACE,"<%d> : no response on create",pipErr);
		break;
	case	QUE_E_FULL	:
		dbg(TRACE,"<%d> : too many route destinations",pipErr);
		break;
	case	QUE_E_NOMSG	:	/* No message on queue */
		dbg(TRACE,"<%d> : no messages on queue",pipErr);
		break;
	case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
		dbg(TRACE,"<%d> : invalid originator=0",pipErr);
		break;
	case	QUE_E_NOINIT	:	/* Queues is not initialized*/
		dbg(TRACE,"<%d> : queues are not initialized",pipErr);
		break;
	case	QUE_E_ITOBIG	:
		dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
		break;
	case	QUE_E_BUFSIZ	:
		dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
		break;
	default			:	/* Unknown queue error */
		dbg(TRACE,"<%d> : unknown error",pipErr);
		break;
	} /* end switch */
         
	return;

} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
	int	ilRc = RC_SUCCESS;			/* Return code */
	int	ilBreakOut = FALSE;
	
	do
	{
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;	

		if( ilRc == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRc != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRc);
			} /* fi */
		
			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				ilBreakOut = TRUE;
				break;	

			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate(1);
				break;	
	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				ilBreakOut = TRUE;
				break;	

			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;

			case	SHUTDOWN	:
				Terminate(1);
				break;
						
			case	RESET		:
				ilRc = Reset();
				break;
						
			case	EVENT_DATA	:
				dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
					
			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;

			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;

			default			:
				dbg(TRACE,"HandleQueues: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		} /* end else */
	} while (ilBreakOut == FALSE);

			ilRc = Init_Process();
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitDemhdl: init failed!");
			} 
     

} /* end of HandleQueues */
	



/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(EVENT *prpEvent)
{
	int	   ilRc           = RC_SUCCESS;			/* Return code */
	int      ilCmd          = 0;

	BC_HEAD *prlBchead       = NULL;
	CMDBLK  *prlCmdblk       = NULL;
	char    *pclSelection    = NULL;
	char    *pclFields       = NULL;
	char    *pclData         = NULL;
	char    *pclRow          = NULL;
	char 		clUrnoList[2400];
	char 		clTable[34];
	int			ilUpdPoolJob = TRUE;

	prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
	prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
	pclSelection = prlCmdblk->data;
	pclFields    = pclSelection + strlen(pclSelection) + 1;
	pclData      = pclFields + strlen(pclFields) + 1;


	strcpy(clTable,prlCmdblk->obj_name);

	dbg(TRACE,"========== START <%10.10d> ==========",lgEvtCnt);

	/****************************************/
	DebugPrintBchead(TRACE,prlBchead);
	DebugPrintCmdblk(TRACE,prlCmdblk);
	dbg(TRACE,"Command: <%s>",prlCmdblk->command);
	dbg(TRACE,"originator follows event = %p ",prpEvent);

	dbg(TRACE,"originator<%d>",prpEvent->originator);
	dbg(TRACE,"selection follows Selection = %p ",pclSelection);
	dbg(TRACE,"selection <%s>",pclSelection);
	dbg(TRACE,"fields    <%s>",pclFields);
	dbg(TRACE,"data      <%s>",pclData);
	/****************************************/


	/* Handle daily Bulk update for ELSE */

	if (strcmp(prlCmdblk->command,"BLK") == 0)
	{
		HandleBulkData();
	}

	/* Handle online-updates for ELSE */
	if (strcmp(prlCmdblk->command,"ONL") == 0)
	{
		HandleOnlineData(pclFields, pclData);
	}

	/* request date / Send query to else */
	if(strcmp(prlCmdblk->command,"REQ") == 0)
	{
		HandleRequest(pclFields, pclData);
	}

	/* process query sent from else */
	if(strcmp(prlCmdblk->command,"QRY") == 0)
	{
		HandleQuery(pclData);
	}
	dbg(TRACE,"==========  END  <%10.10d> ==========",lgEvtCnt);

	return(RC_SUCCESS);
	
} /* end of HandleData */



static void HandleBulkData()
{
	char clBulkTime[20] = "\0";
	char clBulkDay[20] = "\0";
	char clTmpSelection[200] = "\0";
	char clDepartureFields[100] = "FLNO,STOD,CKIF,CKIT";
	char clDepartureSelection[200] = "SELECT %s FROM AFTTAB WHERE STOD LIKE '%s%%' AND (ADID = 'D' OR ADID = 'B')";


	int i = 0;
	int ilBulkDays = 0;

	if (strlen(cgActualTimeForTest) > 0)
	{
		strcpy(clBulkTime,cgActualTimeForTest);
	}
	else
	{
		GetServerTimeStamp("UTC",1,0,clBulkTime);
	}
	ilBulkDays = atoi(cgBulkTimeframe);

	for (i=1 ; i<=ilBulkDays ; i++)
	{
		AddSecondsToCEDATime(clBulkTime,(24*60*60),1);
		dbg(TRACE,"%05d: i: %d, clBulkTime: %s",__LINE__,i,clBulkTime);
		strcpy(clBulkDay,clBulkTime);
		clBulkDay[8] = '\0';
		sprintf(clTmpSelection,clDepartureSelection,clDepartureFields,clBulkDay);
		SendDepartureBulkData(clTmpSelection,clDepartureFields);
	}


}


static void SendDepartureBulkData(char *pclTmpSelection, char *pclDepartureFields)
{
	short slSqlFunc = 0;
	short slCursor = 0;
	char clDataArea[4000] = "\0";
	char clErr[1024] = "\0";
	int ilRc = 0;
	int ilItemCount,ilLc;
	char *pclDataArea;
	int ilRecordCount = 0;
	char clTmp[100] = "\0";

	dbg(TRACE,"%05d: ####### SendDepartureBulkData - START ##########",__LINE__);
	dbg(DEBUG,"%05d: <SendDepartureBulkData> using selection: <%s>",__LINE__,pclTmpSelection);
	slSqlFunc = START;
	slCursor = 0;

	ilItemCount = get_no_of_items(pclDepartureFields);

	while((ilRc = sql_if(slSqlFunc,&slCursor,pclTmpSelection,clDataArea)) == DB_SUCCESS)
	{
		ilRecordCount++;
		dbg(TRACE,"%05d: -------- process Departure-Bulk-Record No: <%d> ------------",__LINE__,ilRecordCount);
		pclDataArea = clDataArea;
        for(ilLc =  1; ilLc < ilItemCount; ilLc++)
        {
                while(*pclDataArea != '\0')
                {
                        pclDataArea++;
                }
                        *pclDataArea = ',';
        }

		ilRc = FormatAndSendDepartureData(clDataArea, pclDepartureFields, "integrate");
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"%05d: A problem in FormatAndSendDepartureData occured with the following data:",__LINE__);

		}
		slSqlFunc = NEXT;
	}
	close_my_cursor(&slCursor);
	commit_work();

	if ((ilRc == SQL_NOTFOUND) && (ilRecordCount > 0))
	{
		dbg(TRACE,"%05d: %d Record found with selection: <%s>",__LINE__,ilRecordCount,pclTmpSelection);
	}
	else
	{
		if ((ilRc == SQL_NOTFOUND) && (ilRecordCount == 0))
		{
			dbg(TRACE,"%05d: <SendDepartureBulkData> No data found with the following selection:\nSqlBuf <%s>",__LINE__,pclTmpSelection);
		}
		else
		{
			get_ora_err(ilRc,clErr);
			dbg(TRACE,"%05d: <SendDepartureBulkData> ilRc <%d>\nORA-ERR <%s>\nSqlBuf <%s>",__LINE__,ilRc,clErr,pclTmpSelection);
			sleep(10);
			ilRc = RC_FAIL;
		}
	}
}


static void SendArrivalBulkData(char *pclTmpSelection, char *pclArrivalFields)
{
	short slSqlFunc = 0;
	short slCursor = 0;
	char clDataArea[4000] = "\0";
	char clErr[1024] = "\0";
	int ilRc = 0;
	int ilItemCount,ilLc;
	char *pclDataArea;
	int ilRecordCount = 0;
	char clTmp[100] = "\0";

	dbg(TRACE,"%05d: ####### SendArrivalBulkData - START ##########",__LINE__);
	dbg(DEBUG,"%05d: <SendArrivalBulkData> using selection: <%s>",__LINE__,pclTmpSelection);
	slSqlFunc = START;
	slCursor = 0;

	ilItemCount = get_no_of_items(pclArrivalFields);

	while((ilRc = sql_if(slSqlFunc,&slCursor,pclTmpSelection,clDataArea)) == DB_SUCCESS)
	{
		ilRecordCount++;
		dbg(TRACE,"%05d: -------- process Arrival-Bulk-Record No: <%d> ------------",__LINE__,ilRecordCount);
		pclDataArea = clDataArea;
        for(ilLc =  1; ilLc < ilItemCount; ilLc++)
        {
                while(*pclDataArea != '\0')
                {
                        pclDataArea++;
                }
                        *pclDataArea = ',';
        }

		ilRc = FormatAndSendArrivalData(clDataArea, pclArrivalFields, "integrate");
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"%05d: A problem in FormatAndSendArrivalData occured with the following data:",__LINE__);

		}
		slSqlFunc = NEXT;
	}
	close_my_cursor(&slCursor);
	commit_work();

	if ((ilRc == SQL_NOTFOUND) && (ilRecordCount > 0))
	{
		dbg(TRACE,"%05d: %d Record found with selection: <%s>",__LINE__,ilRecordCount,pclTmpSelection);
	}
	else
	{
		if ((ilRc == SQL_NOTFOUND) && (ilRecordCount == 0))
		{
			dbg(TRACE,"%05d: <SendArrivalBulkData> No data found with the following selection:\nSqlBuf <%s>",__LINE__,pclTmpSelection);
		}
		else
		{
			get_ora_err(ilRc,clErr);
			dbg(TRACE,"%05d: <SendArrivalBulkData> ilRc <%d>\nORA-ERR <%s>\nSqlBuf <%s>",__LINE__,ilRc,clErr,pclTmpSelection);
			sleep(10);
			ilRc = RC_FAIL;
		}
	}
}



static int FormatAndSendDepartureData(char *pclDepartureData, char *pclDepartureFields, char *pclActionType)
{
	int ilRc = RC_SUCCESS;
	char clTmp[200] = "\0";
	char clDepartureMessage[400] = "\0";
	char clFLNO[20] = "\0";
	char clSTOD[20] = "\0";
	char clCKIF[20] = "\0";
	char clCKIT[20] = "\0";
	char clOFBL[20] = "\0";
	int blFLNOfound = FALSE;
	int blSTODfound = FALSE;
	int blCKIFfound = FALSE;
	int blCKITfound = FALSE;
	int blOFBLfound = FALSE;
	int ilItemNumber = 0;
	char clActualTime[20] = "\0";
	int blSQMIfound = FALSE;
	
	ilItemNumber = get_item_no(pclDepartureFields,"FLNO",5) + 1;
	dbg(TRACE,"ItemNumber FLNO: <%d>",ilItemNumber);
	if(ilItemNumber > 0)
	{
		get_real_item (clFLNO, pclDepartureData, ilItemNumber);
		dbg(DEBUG,"%05d: Received data: FLNO:<%s>",__LINE__,clFLNO);
		blFLNOfound = TRUE;
		/*test - commenting for UFIS 2489 */
		/*if((strstr(clFLNO,"SQ") != NULL) || (strstr(clFLNO,"MI") != NULL))
		{
			blSQMIfound = TRUE;
		}
		else
		{
			dbg(TRACE,"%05d: Airline is NOT SQ/MI so ignore OFBL",__LINE__);
			blSQMIfound = FALSE;
		}*/
	}
	else
	{
		dbg(TRACE,"%05d: FLNO missing in <%s>",__LINE__,pclDepartureFields);
		blFLNOfound = FALSE;
	}
	
	ilItemNumber = get_item_no(pclDepartureFields,"STOD",5) + 1;
	dbg(TRACE,"ItemNumber STOD: <%d>",ilItemNumber);
	if(ilItemNumber > 0)
	{
		get_real_item (clSTOD, pclDepartureData, ilItemNumber);
		/*now check if flight is within the given Timeframe*/
		GetServerTimeStamp("UTC",1,0,clActualTime);
		AddSecondsToCEDATime(clActualTime,lgOnlineTimeframe,1);
		if(strcmp(clSTOD,clActualTime) <= 0)
		{
			UtcToLocalTimeFixTZ(clSTOD);
			clSTOD[8] = '\0';
			dbg(DEBUG,"%05d: Received data: STOD:<%s>",__LINE__,clSTOD);
			blSTODfound = TRUE;
		}
		else
		{
			dbg(DEBUG,"%05d: Received data: STOD <%s> out of bulk-date-timeframe (timeframe ends at: <%s>)",__LINE__,clSTOD,clActualTime);
			blSTODfound = FALSE;
		}

	}
	else
	{
		dbg(TRACE,"%05d: STOD missing in <%s>",__LINE__,pclDepartureFields);
		blSTODfound = FALSE;
	}


	ilItemNumber = get_item_no(pclDepartureFields,"CKIF",5) + 1;
	dbg(TRACE,"ItemNumber CKIF: <%d>",ilItemNumber);
	if(ilItemNumber > 0)
	{
		get_real_item (clTmp, pclDepartureData, ilItemNumber);
		dbg(DEBUG,"%05d: Received data: CKIF:<%s>",__LINE__,clTmp);
		if(strlen(clTmp) > 0)
		{
			strcpy(clCKIF,&clTmp[1]);
			dbg(DEBUG,"%05d: Cut of first char: CKIF:<%s>",__LINE__,clCKIF);
			blCKIFfound = TRUE;
		}
		else
		{
			if((strcmp(cgFilterEmptyCIC,"YES") == 0) && (strcmp(pclActionType,"integrate") == 0))
			{
				blCKIFfound = FALSE; /*do not send empty CIC-message*/
			}
			else
			{
				blCKIFfound = TRUE;
			}
		}
	}
	else
	{
		dbg(DEBUG,"%05d: Check-In-From missing in <%s>",__LINE__,pclDepartureFields);
		blCKIFfound = FALSE;
	}

	ilItemNumber = get_item_no(pclDepartureFields,"CKIT",5) + 1;

	dbg(TRACE,"ItemNumber CKIT: <%d>",ilItemNumber);	
	if(ilItemNumber > 0)
	{
		get_real_item (clTmp, pclDepartureData, ilItemNumber);
		dbg(DEBUG,"%05d: Received data: CKIT:<%s>",__LINE__,clTmp);
		if(strlen(clTmp) > 0)
		{
			strcpy(clCKIT,&clTmp[1]);
			dbg(DEBUG,"%05d: Cut of first char: CKIT:<%s>",__LINE__,clCKIT);
			blCKITfound = TRUE;
		}
		else
		{
			if((strcmp(cgFilterEmptyCIC,"YES") == 0) && (strcmp(pclActionType,"integrate") == 0))
			{
				blCKITfound = FALSE; /*do not send empty CIC-message*/
			}
			else
			{
				blCKITfound = TRUE;
			}
		}

	}
	else
	{
		dbg(DEBUG,"%05d: CKIT missing in <%s>",__LINE__,pclDepartureFields);
		blCKITfound = FALSE;
	}

	ilItemNumber = get_item_no(pclDepartureFields,"OFBL",5) + 1;
	dbg(TRACE,"ItemNumber OFBL: <%d>",ilItemNumber);
	if(ilItemNumber > 0)
	{
		get_real_item (clOFBL, pclDepartureData, ilItemNumber);
		dbg(DEBUG,"%05d: Received data: OFBL:<%s>",__LINE__,clOFBL);
		UtcToLocalTimeFixTZ(clOFBL);
		clOFBL[12] = '\0';
		dbg(DEBUG,"%05d: without seconds OFBL:<%s>",__LINE__,clOFBL);
		blOFBLfound = TRUE;
	}
	else
	{
		dbg(DEBUG,"%05d: OFBL missing in <%s>",__LINE__,pclDepartureFields);
		blOFBLfound = FALSE;
	}


	if( (blFLNOfound && blSTODfound) && (blCKITfound || blCKIFfound || blOFBLfound) )
	{
		strcat(clDepartureMessage,"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>\n");
		strcat(clDepartureMessage,"<update table=\"DD\">\n");
		sprintf(clTmp,"  <key flight=\"%s\" sdate=\"%s\" />\n",clFLNO,clSTOD);
		strcat(clDepartureMessage,clTmp);
		if(strcmp(pclActionType,"integrate") == 0)
		{
			strcat(clDepartureMessage,"  <integrate>\n");
		}		
		if(blCKIFfound == TRUE)
		{
			if ((strlen(clCKIF) > 0) && (clCKIF[0] != ' '))
			{
				sprintf(clTmp,"    <check_1>%s</check_1>\n",clCKIF);
				strcat(clDepartureMessage,clTmp);
			}
			else
			{
				if(strcmp(pclActionType,"integrate") != 0)
				{
					strcat(clDepartureMessage,"    <check_1/>\n");
				}
			}
		}

		if(blCKITfound == TRUE)
		{
			if((strlen(clCKIT) > 0) && (clCKIT[0] != ' '))
			{
				sprintf(clTmp,"    <check_2>%s</check_2>\n",clCKIT);
				strcat(clDepartureMessage,clTmp);
			}
			else
			{
				if(strcmp(pclActionType,"integrate") != 0)
				{
					strcat(clDepartureMessage,"    <check_2/>\n");
				}
			}
		}

		/*test - removing blSQMIfound for UFIS 2489 */
		//if((blOFBLfound == TRUE) && (blSQMIfound == TRUE))
		if(blOFBLfound == TRUE)
		{
			if(strlen(clOFBL) > 0)
			{
				sprintf(clTmp,"    <blocks>%s</blocks>\n",clOFBL);
				strcat(clDepartureMessage,clTmp);
			}
			else
			{
				if(strcmp(pclActionType,"integrate") != 0)
				{
					strcat(clDepartureMessage,"    <blocks/>\n");
				}
			}
		}

		if(strcmp(pclActionType,"integrate") == 0)
		{
			strcat(clDepartureMessage,"  </integrate>\n");
		}
		strcat(clDepartureMessage,"</update>\n");
		
		/*test - commenting for UFIS 2489 */
		//dbg(TRACE,"blOFBL: %d, blCKIT: %d, blCKIF: %d, blSQMI: %d",blOFBLfound,blCKITfound,blCKIFfound,blSQMIfound);
		dbg(TRACE,"blOFBL: %d, blCKIT: %d, blCKIF: %d",blOFBLfound,blCKITfound,blCKIFfound);
		/*if((blOFBLfound == TRUE) && (blCKITfound == FALSE) && (blCKIFfound == FALSE) && (blSQMIfound == FALSE))
		{
			dbg(TRACE,"%05d: only OFBL to be send but airline is NOT SQ/MI. No data to send!",__LINE__);
		}
		else
		{*/
			dbg(TRACE,"%05d: SendMessage: \n%s",__LINE__,clDepartureMessage);
			SendCedaEvent(atoi(cgSendToModid),0,mod_name,"","","","ELS","","","",clDepartureMessage,"",atoi(cgSendPrio),RC_SUCCESS);
		//}
	}
	else
	{
		dbg(DEBUG,"%05d: ############# No Data to send !!!! ################",__LINE__);
	}


	return ilRc;
}



static int FormatAndSendArrivalData(char *pclArrivalData, char *pclArrivalFields, char *pclActionType)
{
	int ilRc = RC_SUCCESS;
	char clTmp[200] = "\0";
	char clArrivalMessage[400] = "\0";
	char clFLNO[20] = "\0";
	char clSTOA[20] = "\0";
	char clONBL[20] = "\0";
	int blFLNOfound = FALSE;
	int blSTOAfound = FALSE;
	int blONBLfound = FALSE;
	int ilItemNumber = 0;
	char clActualTime[20] = "\0";


	
	ilItemNumber = get_item_no(pclArrivalFields,"FLNO",5) + 1;
	if(ilItemNumber > 0)
	{
		get_real_item (clFLNO, pclArrivalData, ilItemNumber);
		dbg(DEBUG,"%05d: Received data: FLNO:<%s>",__LINE__,clFLNO);
		blFLNOfound = TRUE;
		/*test - commenting for UFIS 2489 */
		/*if((strstr(clFLNO,"SQ") != NULL) || (strstr(clFLNO,"MI") != NULL))
		{
			blFLNOfound = TRUE;
		}
		else
		{
			dbg(TRACE,"%05d: Airline is NOT SQ/MI so ignore data",__LINE__);
			blFLNOfound = FALSE;
			return RC_SUCCESS;
		}*/
	}
	else
	{
		dbg(TRACE,"%05d: FLNO missing in <%s>",__LINE__,pclArrivalFields);
		blFLNOfound = FALSE;
	}
	
	ilItemNumber = get_item_no(pclArrivalFields,"STOA",5) + 1;
	if(ilItemNumber > 0)
	{
		get_real_item (clSTOA, pclArrivalData, ilItemNumber);
		/*now check if flight is within the given Timeframe*/
		GetServerTimeStamp("UTC",1,0,clActualTime);
		AddSecondsToCEDATime(clActualTime,lgOnlineTimeframe,1);
		if(strcmp(clSTOA,clActualTime) <= 0)
		{
			UtcToLocalTimeFixTZ(clSTOA);
			clSTOA[8] = '\0';
			dbg(DEBUG,"%05d: Received data: STOA <%s>",__LINE__,clSTOA);
			blSTOAfound = TRUE;
		}
		else
		{
			dbg(DEBUG,"%05d: Received data: STOA <%s> out of bulk-data-timeframe (timeframe ends at: <%s>)",__LINE__,clSTOA,clActualTime);
			blSTOAfound = FALSE;
		}

	}
	else
	{
		dbg(TRACE,"%05d: STOA missing in <%s>",__LINE__,pclArrivalFields);
		blSTOAfound = FALSE;
	}


	ilItemNumber = get_item_no(pclArrivalFields,"ONBL",5) + 1;
	if(ilItemNumber > 0)
	{
		get_real_item (clONBL, pclArrivalData, ilItemNumber);
		dbg(DEBUG,"%05d: Received data: ONBL:<%s>",__LINE__,clONBL);
		UtcToLocalTimeFixTZ(clONBL);
		clONBL[12] = '\0';
		dbg(DEBUG,"%05d: without seconds: ONBL:<%s>",__LINE__,clONBL);
		blONBLfound = TRUE;
	}
	else
	{
		dbg(DEBUG,"%05d: ONBL missing in <%s>",__LINE__,pclArrivalFields);
		blONBLfound = FALSE;
	}


	if( (blFLNOfound && blSTOAfound) && blONBLfound )
	{
		strcat(clArrivalMessage,"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>\n");
		strcat(clArrivalMessage,"<update table=\"DA\">\n");
		sprintf(clTmp,"  <key flight=\"%s\" sdate=\"%s\" />\n",clFLNO,clSTOA);
		strcat(clArrivalMessage,clTmp);
		if(strcmp(pclActionType,"integrate") == 0)
		{
			strcat(clArrivalMessage,"  <integrate>\n");
		}		
		if(blONBLfound == TRUE)
		{
			if(strlen(clONBL) > 0)
			{
				sprintf(clTmp,"    <blocks>%s</blocks>\n",clONBL);
				strcat(clArrivalMessage,clTmp);
			}
			else
			{
				if(strcmp(pclActionType,"integrate") != 0)
				{
					strcat(clArrivalMessage,"    <blocks/>\n");
				}
			}
		}
		if(strcmp(pclActionType,"integrate") == 0)
		{
			strcat(clArrivalMessage,"  </integrate>\n");
		}
		strcat(clArrivalMessage,"</update>\n");
		dbg(DEBUG,"%05d: SendMessage: \n%s",__LINE__,clArrivalMessage);
		SendCedaEvent(atoi(cgSendToModid),0,mod_name,"","","","ELS","","","",clArrivalMessage,"",atoi(cgSendPrio),RC_SUCCESS);
	}
	else
	{
		dbg(DEBUG,"%05d: ############# No Data to send !!!! ################",__LINE__);
	}

	return ilRc;
}


static void HandleOnlineData(char *pcpFields, char *pcpData)
{
	int ilItemNumber = 0;
	int ilItemCount = 0;
	int ilRc = 0;
	char clADID[100] = "\0";

	dbg(TRACE,"%05d: ######## HandleOnlineData ##########",__LINE__);

	ilItemCount = get_no_of_items(pcpFields);


	ilItemNumber = get_item_no(pcpFields,"ADID",5) + 1;

	if(ilItemNumber > 0)
	{
		get_real_item (clADID, pcpData, ilItemNumber);
		dbg(DEBUG,"%05d: Received data: ADID <%s>",__LINE__,clADID);
	}
	else
	{
		dbg(TRACE,"%05d: ADID missing in <%s>, Data will not be processed",__LINE__,pcpFields);
		ilRc = RC_FAIL;
	}

	if((clADID[0] == 'A') || (clADID[0] == 'B'))
	{
		FormatAndSendArrivalData(pcpData, pcpFields, "update");
	}

	if((clADID[0] == 'D') || (clADID[0] == 'B'))
	{
		FormatAndSendDepartureData(pcpData, pcpFields, "update");
	}

}


static void HandleRequest(char *pclFields, char *pclData)
{
	int ilItemNumber = 0;
	char clADID[4] = "\0";	/*Query arrival/departure flights*/
	char clDATE[20] = "\0"; /*Query all flights for this date*/
	char clSECS[20] = "\0"; /*Query all flights since SECS - seconds*/
	char clQuery[500] = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>\n";
	char clTemp[100] = "\0";

	ilItemNumber = get_item_no(pclFields,"ADID",5) + 1;
	if(ilItemNumber > 0)
	{
		get_real_item (clADID, pclData, ilItemNumber);
		sprintf(clTemp,"<query table=\"D%s\">\n",clADID);
		strcat(clQuery,clTemp);
	}
	else
	{
		dbg(TRACE,"05d: no arrival-departure-id found. Can not build query",__LINE__);
		return;
	}

	ilItemNumber = get_item_no(pclFields,"DATE",5) + 1;
	if(ilItemNumber > 0)
	{
		get_real_item (clDATE, pclData, ilItemNumber);
		sprintf(clTemp,"  <condition sdate=\"%s\" />\n",clDATE);
		strcat(clQuery,clTemp);
	}
	else if ((ilItemNumber = get_item_no(pclFields,"SECS",5) + 1) > 0)
	{
		get_real_item (clSECS, pclData, ilItemNumber);
		sprintf(clTemp,"  <condition updated_at=\"%s\" />\n",clSECS);
		strcat(clQuery,clTemp);
	}
	else
	{
		dbg(TRACE,"%05d: no condition found. Can not build query",__LINE__);
		return;
	}

	strcat(clQuery,"</query>\n");

	dbg(TRACE,"%05d: now send query to else :\n%s\n",__LINE__,clQuery);
	SendCedaEvent(atoi(cgSendToModid),0,mod_name,"","","","ELS","","","",clQuery,"",atoi(cgSendPrio),RC_SUCCESS);

}

static int GetElementValue(char *pclDest, char *pclOrigData, char *pclBegin, char *pclEnd)
{
	char *pclTemp = NULL;
	long llSize = 0;
	char clBuffer[200] = "\0";
	
	pclTemp = CedaGetKeyItem(pclDest, &llSize, pclOrigData, pclBegin, pclEnd, TRUE);
	if(pclTemp != NULL)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}


static void	HandleQuery(char *pclData)
{
	char clADID[4] = "\0";
	int  blADIDFound = FALSE;
	char clSDATE[20] = "\0";
	int  blSDATEFound = FALSE;
	char clSECONDS[40] = "\0";
	int  blSECONDSFound = FALSE;
	char clDepartureFields[100] = "FLNO,STOD,CKIF,CKIT,OFBL";
	char clArrivalFields[100] = "FLNO,STOA,ONBL";
	char clSDATESelection[200] = "SELECT %s FROM AFTTAB WHERE FLDA='%s' AND (ADID = '%s' OR ADID = 'B')";
	char clSECONDSSelection[200] = "SELECT %s FROM AFTTAB WHERE LSTU >='%s' AND (ADID = '%s' OR ADID = 'B')";
	char clALLSelection[200] = "SELECT %s FROM AFTTAB WHERE (FLDA BETWEEN '%s' AND '%s') AND (ADID = '%s' OR ADID = 'B')";
	char clTempSelection[500] = "\0";
	char clQueryTime[20] = "\0";
	char clQueryBeginTime[20] = "\0";
	char clQueryEndTime[20] = "\0";

	dbg(TRACE,"%05d: ------------ HandleQuery start -------------",__LINE__);
	blADIDFound			= GetElementValue(clADID, pclData, "<query table=\"D", "\"");
	blSDATEFound		= GetElementValue(clSDATE, pclData, "sdate=\"", "\"");
	blSECONDSFound		= GetElementValue(clSECONDS, pclData, "updated_at=\"", "\"");

	if((blADIDFound == TRUE) && (blSDATEFound == TRUE))
	{
		if(clADID[0] == 'D')
		{
			sprintf(clTempSelection,clSDATESelection,clDepartureFields,clSDATE,clADID);
			SendDepartureBulkData(clTempSelection,clDepartureFields);
		}
		if(clADID[0] == 'A')
		{
			sprintf(clTempSelection,clSDATESelection,clArrivalFields,clSDATE,clADID);
			SendArrivalBulkData(clTempSelection,clArrivalFields);
		}
	}
	else if((blADIDFound == TRUE) && (blSECONDSFound == TRUE))
	{
		GetServerTimeStamp("UTC",1,0,clQueryTime);
		AddSecondsToCEDATime(clQueryTime,( - atoi(clSECONDS)),1);

		if(clADID[0] == 'D')
		{
			sprintf(clTempSelection,clSECONDSSelection,clDepartureFields,clQueryTime,clADID);
			SendDepartureBulkData(clTempSelection,clDepartureFields);
		}
		if(clADID[0] == 'A')
		{
			sprintf(clTempSelection,clSECONDSSelection,clArrivalFields,clQueryTime,clADID);
			SendArrivalBulkData(clTempSelection,clArrivalFields);
		}
	}
	else if(blADIDFound == TRUE)
	{
		GetServerTimeStamp("UTC",1,0,clQueryBeginTime);
		AddSecondsToCEDATime(clQueryBeginTime,(-7*24*60*60),1);
		GetServerTimeStamp("UTC",1,0,clQueryEndTime);
		AddSecondsToCEDATime(clQueryEndTime,(7*24*60*60),1);
		if(clADID[0] == 'D')
		{
			sprintf(clTempSelection,clALLSelection,clDepartureFields,clQueryBeginTime,clQueryEndTime,clADID);
			SendDepartureBulkData(clTempSelection,clDepartureFields);
		}
		if(clADID[0] == 'A')
		{
			sprintf(clTempSelection,clALLSelection,clArrivalFields,clQueryBeginTime,clQueryEndTime,clADID);
			SendArrivalBulkData(clTempSelection,clArrivalFields);
		}
	}

	dbg(TRACE,"%05d: ------------ HandleQuery end ---------------",__LINE__);
}
