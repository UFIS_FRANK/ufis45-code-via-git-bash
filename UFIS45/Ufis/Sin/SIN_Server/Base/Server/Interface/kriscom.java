import com.datalex.has.core.HostAccessServiceManager;
import com.datalex.has.core.HostAccessServiceNotFoundException;
import com.datalex.has.travelservices.terminalservice.TerminalSessionManager;
import com.datalex.has.travelservices.terminalservice.TerminalSession;
import com.datalex.has.travelservices.terminalservice.TerminalDevice;
import com.datalex.has.travelservices.terminalservice.TerminalSessionClosureEvent;
import com.datalex.has.travelservices.terminalservice.TerminalDeviceErrorEvent;
import com.datalex.has.travelservices.terminalservice.TerminalSessionValidationException;
import com.datalex.has.travelservices.terminalservice.TerminalDeviceStatusEvent;
import com.datalex.has.travelservices.terminalservice.TerminalEvent;
import com.datalex.has.travelservices.terminalservice.TerminalDeviceDataReceivedEvent;
import com.datalex.has.travelservices.terminalservice.TerminalDeviceDataSentEvent;
import com.datalex.has.travelservices.terminalservice.TerminalServiceFailoverEvent;
import com.datalex.has.travelservices.terminalservice.TerminalSessionAboutToCloseEvent;

import java.io.*;
import src.java.util.*;
import java.lang.*;
import java.util.*;

public class kriscom implements TerminalSessionManager.TerminalEventListener
{
    static String hostResponse = "no response";
    static String sgServiceName;
    static String sgMonitorPeriod;
    static String sgAgency;
    static String sgAppName;
    static String sgWorkstationId;
    static String sgPassword;
    static String sgLogin = "thelogin";
    static String sgCommand = "thecommand";
    static String sgLogoff = "thelogoff";
    static long llTimeOut = 20000;
    private static transient TerminalSessionManager sessionManager=null;
    private static transient TerminalSession myTerminalSession;
	//Venka 15/07/03
	private static transient boolean bInit =false;
    String banane;

    public kriscom()
	{

	}

	//Venka 15/07/03  Try to connect for 20 seconds.  Ignore all exceptions
	public boolean getHasInit(String myServiceName)
	{

		long lnow = System.currentTimeMillis();

		long time = 200000;
		while (lnow + time > System.currentTimeMillis())
		{

			try
			{

				Thread.sleep(50);
			}
			catch(Exception e) {/* To be ignored*/}

			Object obj = null;
			try
			{
				obj = HostAccessServiceManager.getService(myServiceName);
			}
			catch(Exception e) {/* To be ignored*/ }

			if (obj != null)
			{

				long timetaken= System.currentTimeMillis() - lnow;
				System.out.println("Time taken (msec) for service to be registered successfully: " + timetaken);

				return true;
			}
		}
		return false;
	}





    public static void main(String[] args)
	{
  		try
  		{
	    Object obj;
	    
	    int ilPeriod = 60;

	    kriscom mykriscom = new kriscom();
      	System.out.println("hier isses");

	    //TerminalEventListener EventListener = new TerminalEventListener();

		//Venka 15/07/03
		//Do this only once per start up of server
		if(!bInit)
		{
			HostAccessServiceManager.startUp();
                        System.out.println("After HostAccessServiceManager.startUp()");
			bInit = true;
		}

	    getConfigEntry();

	    try
	    {
			//Venka 15/07/03
      		//Shd be first thing done  Moved few lines up. To be done only once per start up
			//HostAccessServiceManager.startUp();

			//Venka 15/07/03
			//Open new Terminal Session Manager object before asking Terminal service if such a TS exists
			sessionManager = new TerminalSessionManager(sgServiceName,mykriscom,30);

             //Venka 15/07/03
             System.out.println("After new TerminalSessionManager command");

			//obj = HostAccessServiceManager.getService(sgServiceName);
			//while (obj != null)
			//{
            //        System.out.println("in while");
			//    obj = HostAccessServiceManager.getService(sgServiceName);
			//}


			//Venka 15/07/03  Try to conenct a total of 3 times.  Can be made configurable if neceesary
			int retry = 3;
			while (retry > 0)
			{

				if (mykriscom.getHasInit(sgServiceName))
				{
					break;
				}

				if (retry > 0)
				{

					retry--;
					System.out.println("Timeout has occured. Retrying " + retry + " time(s) more ");
				}
				else
				{
					String error = "Unable to start up service due to " + sgServiceName + " not found .." ;
					System.out.println(error);
				}
			}

	    }
	    catch (Exception e)
	    {
		System.out.println("HostAccessServiceManager.startUp() -- HostAccessServiceNotFoundException: " + e.toString());
	    }

      	System.out.println("after startup");

      	//Venka 15/07/03. 2nd step to be done b/f anything else.  Moved up a few lines
	    //sessionManager = new TerminalSessionManager(sgServiceName,mykriscom,30);
      	System.out.println("after new TerminalSessionManager");
	    try
	    {
      System.out.println("sgAppname: <"+ sgAppName +">");
      System.out.println("sgWorkstationId: <"+sgWorkstationId +">");
      System.out.println("sgPassword: <"+sgPassword +">");
		myTerminalSession = sessionManager.openSession(sgAppName,sgWorkstationId,sgPassword);
	    }
	    catch (java.rmi.RemoteException e)
	    {
		System.out.println("sessionManager.openSession -- java.rmi.RemoteException: " + e.toString());
	    }
	    catch (TerminalSessionValidationException e)
	    {
		System.out.println("sessionManager.openSession -- TerminalSessionValidationException: " + e.toString());
	    }
	    catch (HostAccessServiceNotFoundException e)
	    {
		System.out.println("sessionManager.openSession -- HostAccessServiceNotFoundException: " + e.toString());
	    }

	    try
	    {
		String mySessionID =myTerminalSession.getSessionID();
	    }
	    catch (java.rmi.RemoteException e)
	    {
		System.out.println("myTerminalSession.getSessionID() -- java.rmi.RemoteException: " + e.toString());
	    }
	    catch (TerminalSessionValidationException e)
	    {
		System.out.println("myTerminalSession.getSessionID() -- TerminalSessionValidationException: "+ e.toString());
	    }

	    TerminalDevice devices[] = null;
	    devices = sessionManager.getDevices();
	    //sessionManager.requestAllEventNotification();
	    try
	    {
		sessionManager.requestDataReceivedEventNotification(devices[0]);
		sessionManager.requestDataSentEventNotification(devices[0]);
		sessionManager.requestErrorEventNotification(devices[0]);
		sessionManager.requestSessionAboutToCloseEventNotification();
		sessionManager.requestSessionClosureEventNotification();
	    }
	    catch (com.datalex.has.travelservices.terminalservice.TerminalSessionManager. UnknownTerminalDeviceException e)
	    {
		System.out.println("sessionManager.request -- UnknownTerminalDeviceException: "+ e.toString());
	    }
	    catch (java.rmi.RemoteException e)
	    {
		System.out.println("sessionManager.request -- java.rmi.RemoteException: " + e.toString());
	    }
	    catch (TerminalSessionValidationException e)
	    {
		System.out.println("sessionManager.request -- TerminalSessionValidationException: "+ e.toString());
	    }

	    try
	    {
		hostResponse = devices[0].sendAndReceive(sgLogin,llTimeOut);
		System.out.println("hostResponse for Login: <"+ hostResponse + ">");
		hostResponse = devices[0].sendAndReceive(sgCommand,llTimeOut);
		System.out.println("hostResponse for Command: <"+ hostResponse + ">");
		hostResponse = devices[0].sendAndReceive(sgLogoff,llTimeOut);
		System.out.println("hostResponse for Logoff: <"+ hostResponse + ">");
	    }
	    catch (java.io.IOException e)
	    {
		System.out.println("devices[0].sendAndReceive -- java.io.IOException: "+ e.toString());
	    }
	    catch (TerminalSessionValidationException e)
	    {
		System.out.println("devices[0].sendAndReceive -- TerminalSessionValidationException: "+ e.toString());
	    }
  }/*try*/
  catch (java.lang.NoClassDefFoundError e)
  {
    System.out.println("MAIN -- java.lang.NoClassDefFoundError: "+ e.toString());
    //e.printStackTrace(System.out);
  }

	}


    private static void getConfigEntry()
	{

	    boolean eof = false;
	    int ilIndex = 0;

	    try
	    {
		FileReader ConfigFile = new FileReader("/ceda/conf/kriscom.cfg");
		BufferedReader ConfigBuffer = new BufferedReader(ConfigFile);

		while (!eof)
		{
		    String ConfigLine = ConfigBuffer.readLine();
		    if (ConfigLine == null)
		    {
			eof = true;
		    }
		    else
		    {
			if (ConfigLine.startsWith("SERVICE_NAME"))
			{
			    ilIndex = ConfigLine.indexOf('=');
			    ConfigLine = ConfigLine.substring(ilIndex+1);
			    ConfigLine = ConfigLine.trim();
			    sgServiceName = ConfigLine;
			    System.out.println("ConfigEntry: SERVICE_NAME found. Value: <" + sgServiceName + ">");
			}
			if (ConfigLine.startsWith("MONITOR_PERIOD"))
			{
			    ilIndex = ConfigLine.indexOf('=');
			    ConfigLine = ConfigLine.substring(ilIndex+1);
			    ConfigLine = ConfigLine.trim();
			    sgMonitorPeriod = ConfigLine;
			    System.out.println("ConfigEntry: MONITOR_PERIOD found. Value: <" + sgMonitorPeriod + ">");
			}
			if (ConfigLine.startsWith("AGENCY"))
			{
			    ilIndex = ConfigLine.indexOf('=');
			    ConfigLine = ConfigLine.substring(ilIndex+1);
			    ConfigLine = ConfigLine.trim();
			    sgAgency = ConfigLine;
			    System.out.println("ConfigEntry: AGENCY found. Value: <" + sgAgency + ">");
			}
			if (ConfigLine.startsWith("APP_NAME"))
			{
			    ilIndex = ConfigLine.indexOf('=');
			    ConfigLine = ConfigLine.substring(ilIndex+1);
			    ConfigLine = ConfigLine.trim();
			    sgAppName = ConfigLine;
			    System.out.println("ConfigEntry: APP_NAME found. Value: <" + sgAppName + ">");
			}
			if (ConfigLine.startsWith("WORKSTATION_ID"))
			{
			    ilIndex = ConfigLine.indexOf('=');
			    ConfigLine = ConfigLine.substring(ilIndex+1);
			    ConfigLine = ConfigLine.trim();
			    sgWorkstationId = ConfigLine;
			    System.out.println("ConfigEntry: WORKSTATION_ID found. Value: <" + sgWorkstationId + ">");
			}
			if (ConfigLine.startsWith("PASSWORT"))
			{
			    ilIndex = ConfigLine.indexOf('=');
			    ConfigLine = ConfigLine.substring(ilIndex+1);
			    ConfigLine = ConfigLine.trim();
			    sgPassword = ConfigLine;
			    System.out.println("ConfigEntry: PASSWORT found. Value: <" + sgPassword + ">");
			}
		    }
		}
		ConfigBuffer.close();
	    }
	    catch (IOException e)
	    {
		System.out.println("Error -- " + e.toString());
	    }

	}/*end getConfigEntry*/

    private static void initKriscom()
	{

	}
    public synchronized void notify(TerminalEvent e)
	{
	    System.out.println("notify: Event received: " + e.toString());
	}
}/*end class kriscom*/


