#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Sin/SIN_Server/Base/Server/Interface/taahdl.c 1.34 2010/05/25 17:13:41SGT ble Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :  MCU                                                      */
/* Date           :  03.03.2003                                               */
/* Description    :  TAAHDL import from and export to TuA                     */
/*                                                                            */
/* Update history :                                                           */
/* 2009-06-11 MEI  Under GetReasonForBreaks and GetReason, the string size may exceed what is allocated in the caller function */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/

#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <dirent.h>
#include <fcntl.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "syslib.h"
#include "AATArray.h"
#include "db_if.h"
#include <time.h>
#include <cedatime.h>

#define BSDFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgBsdArray.plrArrayFieldOfs[ipFieldNo]])
#define ODAFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgOdaArray.plrArrayFieldOfs[ipFieldNo]])
#define FIELDVAL(rrArray,pcpBuf,ipFieldNo) (&pcpBuf[rrArray.plrArrayFieldOfs[ipFieldNo]])
#define PFIELDVAL(prArray,pcpBuf,ipFieldNo) (&pcpBuf[prArray->plrArrayFieldOfs[ipFieldNo]])

#define CLIENTTODB 0
#define DBTOCLIENT 1
#define NOCONVERT 2

#define CMD_IRT     (0)
#define CMD_URT     (1)
#define CMD_DRT     (2)
#define CMD_TAA     (3)
#define CMD_SPR     (4)
#define CMD_SBC     (5)
#define CMD_BR1     (6)
#define CMD_BR2     (7)
#define CMD_BR3     (8)
#define CMD_BR4     (9)

#define OVERLAP(start1,end1,start2,end2) strcmp(start1,end2) <= 0 && strcmp(start2,end1) <= 0
#define ISBETWEEN(start1,end1,time) strcmp(start1,time) <= 0 && strcmp(end1,time) >= 0

static char *prgCmdTxt[] = {"IRT","URT","DRT","TAA","SPR","SBC","BR1","BR2","BR3","BR4",""};
static int    rgCmdDef[] = {CMD_IRT,CMD_URT,CMD_DRT,CMD_TAA,CMD_SPR,CMD_SBC,CMD_BR1,CMD_BR2,CMD_BR3,CMD_BR4,0};

static char cgFunctionDummy[100];

static char *cgWeekDays[8] = {
    "SO","MO","DI","MI","DO","FR","SA","SO" };
static char *cgWeekDaysEng[8] = {
    "SU","MO","TU","WE","TH","FR","SA","SU" };
static int igTestOutput = 9;

/******************************************************************************/
/***   Fields                                                             *****/
/******************************************************************************/


/**
#define DRR_FIELDS "AVFR,AVTO,BKDP,BSDU,BUFU,CDAT,DRRN,DRS1,DRS2,DRS3,DRS4,"\
           "DRSF,EXPF,FCTC,HOPO,LSTU,REMA,ROSL,ROSS,SBFR,SBLP,SBLU,"\
           "DRRN,SBTO,SCOD,SCOO,SDAY,STFU,URNO,USEC,USEU"
             ***/

#define DRR_FIELDS "AVFR,AVTO,DRS1,DRS3,DRS4,HOPO,REMA,SBFR,SBLU,"\
           "DRRN,SBTO,SCOD,SDAY,STFU,ROSS,ROSL,URNO,USEC,USEU,FCTC,OABS"


#define JOB_FIELDS "URNO,UJTY,ACFR,ACTO,UDSR"
#define JOB_FIELDS_ACTION "URNO!,USTF!,UJTY,ACFR,ACTO,UDSR"
#define JAV_FIELDS "ACFR,ACTO,USTF,FLNO,ADID,LAND,ETOA,ONBL,STOA,AIRB,ETOD,OFBL,STOD,URNO"

#define STF_FIELDS "URNO,PENO,TOHR,DODM"

#define PFC_FIELDS "URNO,FCTC,FCTN"
#define SPF_FIELDS "URNO,CODE,SURN,VPFR,VPTO,PRIO"

/*#define DRRSTF_FIELDS "AVFR,AVTO,DRRN,SBFR,SBLU,SBTO,SCOD,SDAY,STFU,PENO,DRRTAB.URNO"*/

static char cgDrrFields[512] = "";
static char cgStfFields[256] = "";
static char cgSpfFields[256] = "";
static char cgPfcFields[256] = "";

static char cgJobFields[256] = JOB_FIELDS;
static char cgJavFields[256] = JAV_FIELDS;
/*static char cgDrrStfFields[256] = DRRSTF_FIELDS;*/

static char cgDrrReloadHintRosl[256];
static char cgDrrReloadHintStfu[256];
static char cgJobReloadHint[256];
static char cgPoolJobHint[256];
static char cgJavHint[256];




#define ADD_DRR_FIELDS "USED,DUMY"

#define STF_INSERT_HOST ":VPENO,:VFINM,:VLANM,:VSHNM,:VGEBU,:VKIND,:VNATI,:VDOBK,:VDOEM,:VDODM,:VREMA,:VTOHR,:VRELS" 
#define STF_INSERT_FIELDS "PENO,FINM,LANM,SHNM,GEBU,KIND,NATI,DOBK,DOEM,DODM,REMA,TOHR,RELS"
#define STF_UPDATE_FIELDS "PENO=:VPENO,FINM=:VFINM,LANM=:VLANM,GEBU=:VGEBU,KIND=:VKIND,NATI=:VNATI,DOBK=:VDOBK,DOEM=:VDOEM,DODM=:VDODM,REMA=:VREMA,TOHR=:VTOHR,RELS=:VRELS"

#define SCO_INSERT_FIELDS "KOST,CODE,SURN,VPFR"
#define SCO_INSERT_HOST ":VKOST,:VCODE,:VSURN,:VVPFR"
#define SCO_UPDATE_FIELDS "KOST=:VKOST,CODE=:VCODE,VPFR=:VVPFR"

#define SOR_INSERT_FIELDS "CODE,SURN,VPFR"
#define SOR_INSERT_HOST ":VCODE,:VSURN,:VVPFR"
#define SOR_UPDATE_FIELDS "CODE=:VCODE,VPFR=:VVPFR"

#define ADR_INSERT_FIELDS "PENO,VAFR,VATO,STRA,ADRC,ZIPA,CITY,LANA,STFU"
#define ADR_INSERT_HOST ":VPENO,:VVAFR,:VVATO,:VSTRA,:VADRC,:VZIPA,:VCITY,:VLANA,:VSTFU"
#define ADR_UPDATE_FIELDS "PENO=:VPENO,VAFR=:VVAFR,VATO=:VVATO,STRA=:VSTRA,ADRC=:VADRC,ZIPA=:VZIPA,CITY=:VCITY,LANA=:VLANA"

#define DRA_INSERT_FIELDS "BSDU,SDAC,SDAY,ABFR,ABTO,STFU,DRRN"
#define DRA_INSERT_HOST ":VBSDU,:VSDAC,:VSDAY,:VABFR,:VABTO,:VSTFU,:VDRRN"
#define DRA_UPDATE_FIELDS  "SDAC=:VSDAC,ABFR=:VABFR,ABTO=:VABTO,DRRN=:VDRRN"

#define DRR_INSERT_FIELDS "BSDU,SCOD,SCOO,SDAY,AVFR,AVTO,STFU,DRRN,ROSS,ROSL,SBFR,SBTO,SBLU"
#define DRR_INSERT_HOST ":VBSDU,:VSCOD,:VSCOO,:VSDAY,:VAVFR,:VAVTO,:VSTFU,:VDRRN,:VROSS,:VROSL,:VSBFR,:VSBTO,:VSBLU"
#define DRR_UPDATE_FIELDS "BSDU=:VBSDU,SCOD=:VSCOD,AVFR=:VAVFR,AVTO=:VAVTO"
#define DRR_DELETE_FIELDS "BSDU=:VBSDU,SCOD=:VSCOD,SCOO=:VSCOO,SBFR=:VSBFR,SBTO=:VSBTO,SBLU=:VSBLU,AVFR=:VAVFR,AVTO=:VAVTO"

#define ORG_FIELDS "URNO,DPT1,DPTN,DPT2"
#define ODA_FIELDS "URNO,SDAC,FREE,SDAS,TYPE,ABFR,ABTO,SDAN"
#define BSD_FIELDS "URNO,BSDC,ESBG,LSEN"

#define DRA_FIELDS "URNO,ABFR,ABTO,REMA,SDAY,SDAC,DRRN,STFU"
#define MAXBREAKS 12

static char cgOrgFields[256] = ORG_FIELDS;
static char cgOdaFields[256] = ODA_FIELDS;
static char cgBsdFields[256] = BSD_FIELDS;
static char cgDraFields[256] = DRA_FIELDS;

/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */

extern FILE *outp;
int  debug_level = TRACE;

extern errno;

/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static int   igLineNo     = 0;                   /* length of incoming item */
static EVENT *prgOutEvent  = NULL;
static int   igInitOK      = FALSE;
static long  lgEvtCnt      = 0;
static char  cgConfigFile[512];
static char  cgHopo[8];                         /* default home airport    */
static char  cgBrkUjty[24] = "";                         /* default home airport    */
static char  cgFlightUjty[24] = "";                         /* default home airport    */
static char  cgPolUjty[24] = "";                         /* default home airport    */
static char  cgTabEnd[8];                       /* default table extension */
static  int  igStartUpMode = TRACE;
static  int  igRuntimeMode = 0;
static  int  igClockInTolerance = -30*60;
static  int  igClockOutTolerance = 300*60;
static char  cgProcessName[80];                  /* name of executable */
static char  cgErrorString[1024];
static char cgUpdEmpSequence[12] = "Y";
static char cgUpdAbsSequence[12] = "Y";
static char cgTohr[4] = "Y";
static char cgAlertName[124] = "TAA";
static char cgDefaultPeno[24] = "00000001";

static char cgCmdBuf[240];
static char cgSelection[50*1024];
static char cgFldBuf[8196];
static char cgSqlBuf[5*1024];
static char cgTwStart[128];
static char cgTwEnd[128];
static char cgRecvName[128];
static char cgDestName[128];
static int igQueOut;

static int igBchdl = 1900;
static int igAction = 7400;
static int igLoghdl = 7700;
static int igTaa = 7288;
static char cgTaa[24];

static char *pcgStfuList = NULL;
static long lgStfuListLength = 50000;
static long lgStfuListActualLength = 0;

static char cgDrrBuf[2048];
static char cgErrMsg[1024];
static char cgCfgBuffer[256];
static char cgDataArea[5*1024];
static char cgStartDay[24] = "";
static char cgEndDay[24] = "";

static BOOL bgBcActiv = TRUE;
static BOOL bgBreak = FALSE;
static BOOL bgSendBroadcast = TRUE;
static BOOL bgIsTaahdl = TRUE;

static struct _BreakStruct
{
    char Start[12];
    char End[12];
    BOOL used;
} rgBreaks[MAXBREAKS+1];

struct _AbsenceStruct
{
    char Sdac[36];
    char Rema[80];
    char Abfr[24];
    char Abto[24];
} rgAbsences[2];

static char cg3DrFileId[6] = "FTAA";

static char cgFileBase[256] = "/ceda/exco/SAP/";
static char cgFileNew[24] = "new/";
static char cgFileArc[24] = "arc/";
static char cgFileLog[24] = "log/";
static char cgFileError[24] = "error/";
static char cgTmpPath[1024] = "tmp/";
static char cgExportPath[1024] = "export/";
static char cgInputFilePath[312];
static char cgInputFileName[312];
static char cgLineBuf[1024];
static char cgArcFileName[312];
static char cgRegi[36];
static int  igExpOffset = 0; 
static int  igDailyOffset = -3; 
static int  igExpEnd = 3; 
static int  igDailyEnd = 3; 

static long lgSuccessCount;
static long lgErrorCount;

FILE *pfgInput = NULL;
FILE *pfgError = NULL;
FILE *pfgLog = NULL;

/*DRRSTF_FIELDS "AVFR,AVTO,DRRN,SBFR,SBLU,SBTO,SCOD,SDAY,STFU,PENO,DRRTAB.URNO"*/
struct _DrrStruct
{
    char Avfr[16];
    char Avto[16];
    char Drrn[2];
    char Sbfr[16];
    char Sblu[16];
    char Sbto[16];
    char Scod[32];
    char Sday[16];
    char Stfu[16];
    char Peno[32];
    char Urno[12];
} rgDrrData1,rgDrrData2;

/* static array informations */

#define ARR_NAME_LEN   (18)
#define ARR_FLDLST_LEN (256)

struct _arrayinfo {
    HANDLE   rrArrayHandle;
    char     crArrayTableName[ARR_NAME_LEN+1];
    char     crArrayName[ARR_NAME_LEN+1];
    char     crArrayFieldList[ARR_FLDLST_LEN+1];
    long     lrArrayFieldCnt;
    char   *pcrArrayRowBuf;
    long     lrArrayRowLen;
    long   *plrArrayFieldOfs;
    long   *plrArrayFieldLen;
    HANDLE   rrIdx01Handle;
    char     crIdx01Name[ARR_NAME_LEN+1];
    char     crIdx01FieldList[ARR_FLDLST_LEN+1];
    long     lrIdx01FieldCnt;
    char   *pcrIdx01RowBuf;
    long     lrIdx01RowLen;
    long   *plrIdx01FieldPos;
    long   *plrIdx01FieldOrd;

/*****************/
    HANDLE   rrIdx02Handle;
    char     crIdx02Name[ARR_NAME_LEN+1];
    char     crIdx02FieldList[ARR_FLDLST_LEN+1];
    long     lrIdx02FieldCnt;
    char   *pcrIdx02RowBuf;
    long     lrIdx02RowLen;
    long   *plrIdx02FieldPos;
    long   *plrIdx02FieldOrd;
    /****************************/
    HANDLE   rrIdx03Handle;
    char     crIdx03Name[ARR_NAME_LEN+1];
    char     crIdx03FieldList[ARR_FLDLST_LEN+1];
    long     lrIdx03FieldCnt;
    char   *pcrIdx03RowBuf;
    long     lrIdx03RowLen;
    long   *plrIdx03FieldPos;
    long   *plrIdx03FieldOrd;
};
typedef struct _arrayinfo ARRAYINFO;

static ARRAYINFO rgBsdArray;
static ARRAYINFO rgOdaArray;
static ARRAYINFO rgDraArray;
static ARRAYINFO rgDrrArray;
static ARRAYINFO rgDrrArrayP;
static ARRAYINFO rgOrgArray;
static ARRAYINFO rgStfArray;
static ARRAYINFO rgJobArray;
static ARRAYINFO rgPolJobArray;
static ARRAYINFO rgJavArray;
static ARRAYINFO rgSpfArray;
static ARRAYINFO rgPfcArray;


static int igDrrAVFR;
static int igDrrAVTO;
static int igDrrDRS1;
static int igDrrDRS3;
static int igDrrDRS4;
static int igDrrHOPO;
static int igDrrREMA;
static int igDrrSBFR;
static int igDrrSBLU;
static int igDrrDRRN;
static int igDrrROSL;
static int igDrrSBTO;
static int igDrrSCOD;
static int igDrrSDAY;
static int igDrrSTFU;
static int igDrrURNO;
static int igDrrUSEC;
static int igDrrUSEU;
static int igDrrFCTC;
static int igDrrOABS;

static int igCotURNO;
static int igCotCTRC;

static int igOrgURNO;
static int igOrgDPT1;
static int igOrgDPTN;

static int igStfURNO;
static int igStfPENO;
static int igStfTOHR;
static int igStfDODM;

static int igOdaURNO;
static int igOdaABFR;
static int igOdaABTO;
static int igOdaSDAC;
static int igOdaFREE;
static int igOdaTYPE;
static int igOdaSDAS;
static int igOdaSDAN;

static int igBsdURNO;
static int igBsdBSDC;
static int igBsdESBG;
static int igBsdLSEN;

static int igDraURNO;
static int igDraABFR;
static int igDraABTO;
static int igDraSDAY;
static int igDraSDAC;
static int igDraREMA;
static int igDraDRRN;
static int igDraSTFU;

static int igJobURNO;
static int igJobUJTY;
static int igJobACFR;
static int igJobACTO;
static int igJobUDSR;

static int igJavACFR;
static int igJavACTO;
static int igJavUSTF;
static int igJavFLNO;
static int igJavADID;
static int igJavURNO;
static int igJavLAND;
static int igJavETOA;
static int igJavONBL;
static int igJavSTOA;
static int igJavAIRB;
static int igJavETOD;
static int igJavOFBL;
static int igJavSTOD;

static int igSpfURNO;
static int igSpfCODE;
static int igSpfSURN;
static int igSpfVPFR;
static int igSpfVPTO;
static int igSpfPRIO;

static int igPfcURNO;
static int igPfcFCTC;
static int igPfcFCTN;


/******************************************************************************/
/* Function prototypes                                                        */
/******************************************************************************/
int check_ret1(int ipRc,int ipLine);
static int  InitTaahdl();
static long GetLengtOfMonth(int ipMonth,int ipYear);

static int SetIndexInfo(ARRAYINFO *prpArrayInfo,char *pcpFieldList,int ilIndex);
static int SetArrayInfo(char *pcpArrayName, char *pcpTableName, 
        char *pcpArrayFieldList,char *pcpAddFields,long *pcpAddFieldLens,
            ARRAYINFO *prpArrayInfo,char *pcpSelection,BOOL bpFill);
static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,
    char *pcpCfgBuffer);

static int GetItemNbr(char *Item, char *Data, char Delim, long Nbr);
static int ItemCnt(char *pcgData, char cpDelim);
static void StringPut(char *pcpDest,char *pcpSource,long lpLen);
static int CheckDate14(char *pcpDate);
static long GetSequence(char *pcpKey);
static int GetFileNames(short spFunc,char *pcpFileId,char *pcpFileDir,char *pcpFileName);

extern int get_no_of_items(char *s);
extern int itemCount(char *);

static int  Reset(void);                       /* Reset program          */
static void Terminate(int ipSleep);            /* Terminate program      */
/*static void   HandleSignal(int); */                /* Handles signals        */
static void HandleErr(int);                    /* Handles general errors */
static void HandleQueErr(int);                 /* Handles queuing errors */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/

static void StringTrimRight(char *pcpBuffer);
static int  GetFieldLength(char *pcpHopo, char *pcpTana, char *pcpFina,
                            long *pipLen);
static int  GetRowLength(char *pcpHopo, char *pcpTana, char *pcpFieldList,
                            long *pipLen);

static int  GetCommand(char *pcpCommand, int *pipCmd);

static int HandleData(EVENT *prpEvent);       /* Handles event data     */
static int SaveIndexInfo (ARRAYINFO *prpArrayInfo);
static int MySaveIndexInfo (ARRAYINFO *prpArrayInfo);

static void IncrementCount(int ipRc);
static void MoveToErrorFile(char *pcpLineBuf);
static int LogError(char *pcpFileName,char *pcpErrorString,BOOL bpAlert);
static int StrToTime(char *pcpTime,time_t *pclTime);
static void  StrAddTime(char *,char*,char *);
static int MyNewToolsSendSql(int ipRouteID,
        BC_HEAD *prpBchd,
        CMDBLK *prpCmdblk,
        char *pcpSelection,
        char *pcpFields,
        char *pcpData);
static int  TriggerAction(char *pcpTableName,char *pcpFields);
static int  WaitAndCheckQueue(int ipTimeout, int ipModId, ITEM **prpItem);
static int  CheckQueue(int ipModId, ITEM **prpItem);
static int FindItemInArrayList(char *pcpInput, char *pcpPattern,char cpDel,int *ipLine,int *ipCol,int *ipPos);

static int GetDebugLevel(char *, int *);
static int GetRealTimeDiff ( char *pcpLocal1, char *pcpLocal2, time_t *plpDiff );
static int DumpArray(ARRAYINFO *prpArray,char *pcpFields);
static int DoSelect(short spFunction,short *pspCursor,char *pcpTable,char *pcpSelection,char *pcpFields,char *pcpDataArea);
static int DoSingleSelect(char *pcpTable,char *pcpSelection,char *pcpFields,char *pcpDataArea);

static int WriteTrailer(FILE *pflFile);
static int WriteHeader(FILE *pflFile,char *pcpFileId,char *pcpFileName,long lpLineCount);
static int MakeFileName(char *pcpFileId,char *pcpFileName);
static int ExportThreeDayRoster(FILE *pfpFile,char *pcpSday);
static int AddToUrnoList(char *pcpUrno,char *pcpList,long lpListLength,long *plpActualLength);
static int ReloadArrays(BOOL bpForExport);
static int CreateOneShiftLine(char *pcpLineBuffer,char *pcpDrr,char *pcpPeno,BOOL bpToFile,BOOL bpIsLongTerm);
static int CheckRegi(char *pcpSurn,char *pcpSday);
static void StringMid(char *pcpDest,char *pcpSource,long lpFrom,long lpLen,int ipConvert);
static int CheckOrgUnit(char *pcpCode,char *pcpNameOfDepartment);
static int GetAbsenceRemark(char *pcpCode,char *pcpRema);
static int DumpRecord(ARRAYINFO *prpArray,char *pcpFields,char *pcpBuf);
static BOOL ExportAbsence(char *pcpCode);
static int HandleRelDrr(char *pcpBCData);


static int TimeToStr(char *pcpTime,time_t lpTime)
{
  struct tm *_tm;
 
  _tm = (struct tm *)localtime(&lpTime);
  sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
      _tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
      _tm->tm_min,_tm->tm_sec);
  return (_tm->tm_wday == 0 ? 7 : _tm->tm_wday) + '0';
}                                                                                

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
    int ilRc = RC_SUCCESS;          /* Return code          */
    int ilCnt = 0;
    int ilOldDebugLevel;

    /************************
    FILE *prlErrFile = NULL;
    char clErrFile[512];
    ************************/

    INITIALIZE;         /* General initialization   */

    /****************************************************************************
    sprintf(&clErrFile[0],"%s/%s%5.5d.err",getenv("DBG_PATH"),mod_name,getpid());
    prlErrFile = freopen(&clErrFile[0],"w",stderr);
    fprintf(stderr,"HoHoHo\n");fflush(stderr);
    dbg(TRACE,"MAIN: error file <%s>",&clErrFile[0]);
    ****************************************************************************/


    strcpy(cgProcessName,argv[0]);

    dbg(TRACE,"MAIN: version <%s>",mks_version);
#ifdef __FILESTAT__
    dbg(TRACE,"MAIN: Filestat <%s>",__FILESTAT__);
#endif

    /* Attach to the MIKE queues */
    do
    {
        ilRc = init_que();
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        }/* end of if */
    }while((ilCnt < 10) && (ilRc != RC_SUCCESS));

    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
        sleep(60);
        exit(1);
    }else{
        dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
    }/* end of if */

    do
    {
        ilRc = init_db();
        if (ilRc != RC_SUCCESS)
        {
            check_ret(ilRc);
            dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        } /* end of if */
    } while((ilCnt < 10) && (ilRc != RC_SUCCESS));

    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
        sleep(60);
        exit(2);
    }else{
        dbg(TRACE,"MAIN: init_db() OK!");
    } /* end of if */

    /* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */

    sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
    dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
    ilRc = TransferFile(cgConfigFile);
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */

    sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
    dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
    ilRc = TransferFile(cgConfigFile);
    if(ilRc != RC_SUCCESS)
    { 
        dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */

    ilRc = SendRemoteShutdown(mod_id);
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    } /* end of if */

    if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
        dbg(DEBUG,"MAIN: waiting for status switch ...");
        HandleQueues();
    }/* end of if */

    if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
        dbg(TRACE,"MAIN: initializing ...");
        if(igInitOK == FALSE)
        {
            ilRc = InitTaahdl();
            if(ilRc != RC_SUCCESS)
            {
                dbg(TRACE,"InitTaahdl: init failed!");
            } /* end of if */
        }/* end of if */
    } else {
        Terminate(30);
    }/* end of if */

/***
    dbg(TRACE,"MAIN: initializing OK sleep(60) ...");
    sleep(60);
***/

    dbg(TRACE,"=====================");
    dbg(TRACE,"MAIN: initializing OK");
    dbg(TRACE,"=====================");
    
    debug_level = igRuntimeMode;

    for(;;)
    {
        ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text;
                
        if( ilRc == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRc != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRc);
            } /* fi */
            
            lgEvtCnt++;

            switch( prgEvent->command )
            {
            case    HSB_STANDBY :
                ctrl_sta = prgEvent->command;
                HandleQueues();
                break;  
            case    HSB_COMING_UP   :
                ctrl_sta = prgEvent->command;
                HandleQueues();
                break;  
            case    HSB_ACTIVE  :
                ctrl_sta = prgEvent->command;
                break;  
            case    HSB_ACT_TO_SBY  :
                ctrl_sta = prgEvent->command;
                /* CloseConnection(); */
                HandleQueues();
                break;  
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate(1);
                break;  
            case    HSB_STANDALONE  :
                ctrl_sta = prgEvent->command;
                ResetDBCounter();
                break;  
            case    REMOTE_DB :
                /* ctrl_sta is checked inside */
                HandleRemoteDB(prgEvent);
                break;
            case    SHUTDOWN    :
                /* process shutdown - maybe from uutil */
                Terminate(1);
                break;
                    
            case    RESET       :
                ilRc = Reset();
                break;
                    
            case    EVENT_DATA  :
                if((ctrl_sta == HSB_STANDALONE) || 
                    (ctrl_sta == HSB_ACTIVE) || 
                    (ctrl_sta == HSB_ACT_TO_SBY))
                {
                    ilRc = HandleData(prgEvent);
                    if(ilRc != RC_SUCCESS)
                    {
                        HandleErr(ilRc);
                    }/* end of if */
                }else{
                    dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
                    DebugPrintItem(TRACE,prgItem);
                    DebugPrintEvent(TRACE,prgEvent);
                }/* end of if */
                break;
                
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;
            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;
            default         :
                dbg(TRACE,"MAIN: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        } else {
            /* Handle queuing errors */
            HandleQueErr(ilRc);
        } /* end else */
        
    } /* end for */
    
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/

static int FindItemInArrayList(char *pcpInput, char *pcpPattern,char cpDel,int *ipLine,int *ipCol,int *ipPos)
{
    int ilRc = RC_SUCCESS;

    ilRc = FindItemInList(pcpInput,pcpPattern,cpDel,ipLine,ipCol,ipPos);
    *ipLine = (*ipLine) -1;
    
    return ilRc;
}



static int InitFieldIndex()
{
    int ilRc = RC_SUCCESS;
    int ilCol,ilPos;
    int ilDBFields;

    /*FindItemInArrayList(DEM_FIELDS,"ALID",',',&igDemALID,&ilCol,&ilPos);*/

    FindItemInArrayList(cgDrrFields,"AVFR",',',&igDrrAVFR,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"AVTO",',',&igDrrAVTO,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"DRS1",',',&igDrrDRS1,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"DRS3",',',&igDrrDRS3,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"DRS4",',',&igDrrDRS4,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"HOPO",',',&igDrrHOPO,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"REMA",',',&igDrrREMA,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"SBFR",',',&igDrrSBFR,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"SBLU",',',&igDrrSBLU,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"DRRN",',',&igDrrDRRN,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"ROSL",',',&igDrrROSL,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"SBTO",',',&igDrrSBTO,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"SCOD",',',&igDrrSCOD,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"SDAY",',',&igDrrSDAY,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"STFU",',',&igDrrSTFU,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"URNO",',',&igDrrURNO,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"USEC",',',&igDrrUSEC,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"USEU",',',&igDrrUSEU,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"FCTC",',',&igDrrFCTC,&ilCol,&ilPos);
    FindItemInArrayList(cgDrrFields,"OABS",',',&igDrrOABS,&ilCol,&ilPos);

    FindItemInArrayList(cgOrgFields,"URNO",',',&igOrgURNO,&ilCol,&ilPos);
    FindItemInArrayList(cgOrgFields,"DPT1",',',&igOrgDPT1,&ilCol,&ilPos);
    FindItemInArrayList(cgOrgFields,"DPTN",',',&igOrgDPTN,&ilCol,&ilPos);

    FindItemInArrayList(cgStfFields,"URNO",',',&igStfURNO,&ilCol,&ilPos);
    FindItemInArrayList(cgStfFields,"PENO",',',&igStfPENO,&ilCol,&ilPos);
    FindItemInArrayList(cgStfFields,"TOHR",',',&igStfTOHR,&ilCol,&ilPos);
    FindItemInArrayList(cgStfFields,"DODM",',',&igStfDODM,&ilCol,&ilPos);

    FindItemInArrayList(cgOdaFields,"URNO",',',&igOdaURNO,&ilCol,&ilPos);
    FindItemInArrayList(cgOdaFields,"SDAC",',',&igOdaSDAC,&ilCol,&ilPos);
    FindItemInArrayList(cgOdaFields,"FREE",',',&igOdaFREE,&ilCol,&ilPos);
    FindItemInArrayList(cgOdaFields,"SDAS",',',&igOdaSDAS,&ilCol,&ilPos);
    FindItemInArrayList(cgOdaFields,"TYPE",',',&igOdaTYPE,&ilCol,&ilPos);
    FindItemInArrayList(cgOdaFields,"ABFR",',',&igOdaABFR,&ilCol,&ilPos);
    FindItemInArrayList(cgOdaFields,"ABTO",',',&igOdaABTO,&ilCol,&ilPos);
    FindItemInArrayList(cgOdaFields,"SDAN",',',&igOdaSDAN,&ilCol,&ilPos);

    FindItemInArrayList(cgBsdFields,"URNO",',',&igBsdURNO,&ilCol,&ilPos);
    FindItemInArrayList(cgBsdFields,"BSDC",',',&igBsdBSDC,&ilCol,&ilPos);
    FindItemInArrayList(cgBsdFields,"ESBG",',',&igBsdESBG,&ilCol,&ilPos);
    FindItemInArrayList(cgBsdFields,"LSEN",',',&igBsdLSEN,&ilCol,&ilPos);

    FindItemInArrayList(cgDraFields,"URNO",',',&igDraURNO,&ilCol,&ilPos);
    FindItemInArrayList(cgDraFields,"ABFR",',',&igDraABFR,&ilCol,&ilPos);
    FindItemInArrayList(cgDraFields,"ABTO",',',&igDraABTO,&ilCol,&ilPos);
    FindItemInArrayList(cgDraFields,"SDAY",',',&igDraSDAY,&ilCol,&ilPos);
    FindItemInArrayList(cgDraFields,"SDAC",',',&igDraSDAC,&ilCol,&ilPos);
    FindItemInArrayList(cgDraFields,"REMA",',',&igDraREMA,&ilCol,&ilPos);
    FindItemInArrayList(cgDraFields,"DRRN",',',&igDraDRRN,&ilCol,&ilPos);
    FindItemInArrayList(cgDraFields,"STFU",',',&igDraSTFU,&ilCol,&ilPos);

    FindItemInArrayList(cgJobFields,"UJTY",',',&igJobUJTY,&ilCol,&ilPos);
    FindItemInArrayList(cgJobFields,"URNO",',',&igJobURNO,&ilCol,&ilPos);
    FindItemInArrayList(cgJobFields,"ACFR",',',&igJobACFR,&ilCol,&ilPos);
    FindItemInArrayList(cgJobFields,"ACTO",',',&igJobACTO,&ilCol,&ilPos);
    FindItemInArrayList(cgJobFields,"UDSR",',',&igJobUDSR,&ilCol,&ilPos);

    FindItemInArrayList(cgJavFields,"ACFR",',',&igJavACFR,&ilCol,&ilPos);
    FindItemInArrayList(cgJavFields,"ACTO",',',&igJavACTO,&ilCol,&ilPos);
    FindItemInArrayList(cgJavFields,"USTF",',',&igJavUSTF,&ilCol,&ilPos);
    FindItemInArrayList(cgJavFields,"FLNO",',',&igJavFLNO,&ilCol,&ilPos);
    FindItemInArrayList(cgJavFields,"ADID",',',&igJavADID,&ilCol,&ilPos);
    FindItemInArrayList(cgJavFields,"LAND",',',&igJavLAND,&ilCol,&ilPos);
    FindItemInArrayList(cgJavFields,"ETOA",',',&igJavETOA,&ilCol,&ilPos);
    FindItemInArrayList(cgJavFields,"ONBL",',',&igJavONBL,&ilCol,&ilPos);
    FindItemInArrayList(cgJavFields,"STOA",',',&igJavSTOA,&ilCol,&ilPos);
    FindItemInArrayList(cgJavFields,"AIRB",',',&igJavAIRB,&ilCol,&ilPos);
    FindItemInArrayList(cgJavFields,"ETOD",',',&igJavETOD,&ilCol,&ilPos);
    FindItemInArrayList(cgJavFields,"OFBL",',',&igJavOFBL,&ilCol,&ilPos);
    FindItemInArrayList(cgJavFields,"STOD",',',&igJavSTOD,&ilCol,&ilPos);
    FindItemInArrayList(cgJavFields,"URNO",',',&igJavURNO,&ilCol,&ilPos);

    FindItemInArrayList(cgSpfFields,"URNO",',',&igSpfURNO,&ilCol,&ilPos);
    FindItemInArrayList(cgSpfFields,"CODE",',',&igSpfCODE,&ilCol,&ilPos);
    FindItemInArrayList(cgSpfFields,"SURN",',',&igSpfSURN,&ilCol,&ilPos);
    FindItemInArrayList(cgSpfFields,"VPFR",',',&igSpfVPFR,&ilCol,&ilPos);
    FindItemInArrayList(cgSpfFields,"VPTO",',',&igSpfVPTO,&ilCol,&ilPos);
    FindItemInArrayList(cgSpfFields,"PRIO",',',&igSpfPRIO,&ilCol,&ilPos);

    FindItemInArrayList(cgPfcFields,"URNO",',',&igPfcURNO,&ilCol,&ilPos);
    FindItemInArrayList(cgPfcFields,"FCTC",',',&igPfcFCTC,&ilCol,&ilPos);
    FindItemInArrayList(cgPfcFields,"FCTN",',',&igPfcFCTN,&ilCol,&ilPos);


/******************************************************************************/


    return ilRc;
}


static int ReadConfigLine(char *pcpSection,char *pcpKeyword,
    char *pcpCfgBuffer)
{
    int ilRc = RC_SUCCESS;

    char clSection[124];
    char clKeyword[124];
    char clBuffer[256];
    char *pclEquals;

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRc = iGetConfigLine(cgConfigFile,clSection,clKeyword,clBuffer);
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
    } 
    else
    {
            pclEquals = strchr(clBuffer,'=');
            if (pclEquals != NULL)
            {
                strcpy(pcpCfgBuffer,&pclEquals[2]);
            }
            else
            {
                strcpy(pcpCfgBuffer,clBuffer);
            }
            StringTrimRight(pcpCfgBuffer);
            dbg(DEBUG,"Config Line <%s>,<%s>:<%s> found in %s",
                    &clSection[0],&clKeyword[0],pcpCfgBuffer,cgConfigFile);
    }/* end of if */
    return ilRc;
}

static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,
    char *pcpCfgBuffer)
{
    int ilRc = RC_SUCCESS;

    char clSection[124];
    char clKeyword[124];

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRc = iGetConfigEntry(cgConfigFile,clSection,clKeyword,
            CFG_STRING,pcpCfgBuffer);
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
    } 
    else
    {
            dbg(DEBUG,"Config Entry <%s>,<%s>:<%s> found in %s",
                    &clSection[0],&clKeyword[0],pcpCfgBuffer,cgConfigFile);
    }/* end of if */
    return ilRc;
}

static int InitTaahdl()
{
    int  ilRc = RC_SUCCESS;         /* Return code */
  char clSection[64];
  char clKeyword[64];
  char clNow[64];
    time_t tlNow;
    int ilOldDebugLevel;
    long pclAddFieldLens[12];
    char pclAddFields[256];
    char pclSqlBuf[2560];
    char pclSelection[1024];
    short slCursor;
    short slSqlFunc;
    char  clBreak[24];
    int ilLc;
    int ilTmpModId;
    char clTmpString[124];

    tlNow = time(NULL);
    tlNow -= tlNow % 86400;

    TimeToStr(clNow,tlNow);

    dbg(TRACE,"%05d:Now = <%s>",__LINE__,clNow);

    memset(cgFunctionDummy,' ',40);
    cgFunctionDummy[40] = '\0';

    /* now reading from configfile or from database */


    if ((ilTmpModId = tool_get_q_id("bchdl")) == RC_SUCCESS)
    {
        igBchdl = ilTmpModId;
        dbg(TRACE,"InitTaahdl: Mod-Id for <bchdl> set to %d",igBchdl);
    }
    else
    {
        dbg(TRACE,"InitTaahdl: Mod-Id for <bchdl> not found, set to default %d",igBchdl);
    }


    if ((ilTmpModId = tool_get_q_id("action")) == RC_SUCCESS)
    {
        igAction = ilTmpModId;
        dbg(TRACE,"InitTaahdl: Mod-Id for <action> set to %d",igAction);
    }
    else
    {
        dbg(TRACE,"InitTaahdl: Mod-Id for <action> not found, set to default %d",igAction);
    }


    if ((ilTmpModId = tool_get_q_id("loghdl")) == RC_SUCCESS)
    {
        igLoghdl = ilTmpModId;
        dbg(TRACE,"InitTaahdl: Mod-Id for <loghdl> set to %d",igLoghdl);
    }
    else
    {
        dbg(TRACE,"InitTaahdl: Mod-Id for <loghdl> not found, set to default %d",igLoghdl);
    }


    if(ilRc == RC_SUCCESS)
    {
        /* read HomeAirPort from SGS.TAB */
        memset((void*)&cgHopo[0], 0x00, 8);

        sprintf(&clSection[0],"SYS");
        sprintf(&clKeyword[0],"HOMEAP");

        ilRc = tool_search_exco_data(&clSection[0], &clKeyword[0], &cgHopo[0]);
        if (ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"<InitTaahdl> EXTAB,%s,%s not found in SGS.TAB",&clSection[0], &clKeyword[0]);
            Terminate(30);
        } else {
            dbg(TRACE,"<InitTaahdl> home airport    <%s>",&cgHopo[0]);
        }/* end of if */
    }/* end of if */

    
    if(ilRc == RC_SUCCESS)
    {
        sprintf(&clSection[0],"ALL");
        sprintf(&clKeyword[0],"TABEND");

        ilRc = tool_search_exco_data(&clSection[0], &clKeyword[0], &cgTabEnd[0]);
        if (ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"<InitTaahdl> EXTAB,%s,%s not found in SGS.TAB",&clSection[0], &clKeyword[0]);
            Terminate(30);
        } else {
            dbg(TRACE,"<InitTaahdl> table extension <%s>",&cgTabEnd[0]);
        }/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
         GetDebugLevel("STARTUP_MODE", &igStartUpMode);
         GetDebugLevel("RUNTIME_MODE", &igRuntimeMode);
         
         debug_level = igStartUpMode;


        ilRc = ReadConfigEntry("FILEIDS","3DR",cg3DrFileId);
        if(ilRc == RC_SUCCESS)
        {
            dbg(TRACE,"FILEID 3DR set to <%s>",cg3DrFileId);
        }
        else
        {
            dbg(TRACE,"FILEID 3DR not found, set to <%s>",cg3DrFileId);
            ilRc = RC_SUCCESS;
        }

        ilRc = ReadConfigEntry("DIRECTORIES","FILEBASE",cgFileBase);
        if(ilRc == RC_SUCCESS)
        {
            dbg(TRACE,"FILEID FILEBASE set to <%s>",cgFileBase);
        }
        else
        {
            dbg(TRACE,"FILEID FILEBASE not found, set to <%s>",cgFileBase);
            ilRc = RC_SUCCESS;
        }

        ilRc = ReadConfigEntry("DIRECTORIES","NEW",cgFileNew);
        if(ilRc == RC_SUCCESS)
        {
            dbg(TRACE,"FILEID NEW set to <%s>",cgFileNew);
        }
        else
        {
            dbg(TRACE,"FILEID NEW not found, set to <%s>",cgFileNew);
            ilRc = RC_SUCCESS;
        }

        ilRc = ReadConfigEntry("DIRECTORIES","ARC",cgFileArc);
        if(ilRc == RC_SUCCESS)
        {
            dbg(TRACE,"FILEID ARC set to <%s>",cgFileArc);
        }
        else
        {
            dbg(TRACE,"FILEID ARC not found, set to <%s>",cgFileArc);
            ilRc = RC_SUCCESS;
        }

        ilRc = ReadConfigEntry("DIRECTORIES","LOG",cgFileLog);
        if(ilRc == RC_SUCCESS)
        {
            dbg(TRACE,"Directory for LOG files set to <%s>",cgFileLog);
        }
        else
        {
            dbg(TRACE,"Directory for LOG files not found, set to <%s>",cgFileLog);
            ilRc = RC_SUCCESS;
        }
        ilRc = ReadConfigEntry("DIRECTORIES","ERROR",cgFileError);
        if(ilRc == RC_SUCCESS)
        {
            dbg(TRACE,"Directory for ERROR files set to <%s>",cgFileError);
        }
        else
        {
            dbg(TRACE,"Directory for ERROR files not found, set to <%s>",cgFileError);
            ilRc = RC_SUCCESS;
        }
        ilRc = ReadConfigEntry("DIRECTORIES","TMP",cgTmpPath);
        if(ilRc == RC_SUCCESS)
        {
            dbg(TRACE,"Directory for temporary files set to <%s>",cgTmpPath);
        }
        else
        {
            dbg(TRACE,"Directory for temporary files not found, set to <%s>",cgTmpPath);
            ilRc = RC_SUCCESS;
        }
        ilRc = ReadConfigEntry("DIRECTORIES","EXPORT",cgExportPath);
        if(ilRc == RC_SUCCESS)
        {
            dbg(TRACE,"Directory for EXPORT files set to <%s>",cgExportPath);
        }
        else
        {
            dbg(TRACE,"Directory for temporary files not found, set to <%s>",cgExportPath);
            ilRc = RC_SUCCESS;
        }

        ilRc = ReadConfigEntry("MAIN","SHIFTINDICATOR",cgRegi);
        if(ilRc == RC_SUCCESS)
        {
            dbg(TRACE,"Shift indicator to send set to <%s>",cgRegi);
        }
        else
        {
            dbg(TRACE,"Shift indicator to send not found, set to <%s>",cgRegi);
            ilRc = RC_SUCCESS;
        }


        ilRc = ReadConfigEntry("MAIN","CLOCKINTOLERANZ",clTmpString);
        if(ilRc == RC_SUCCESS)
        {

            igClockInTolerance = atoi(clTmpString)*60;
            dbg(TRACE,"Clock in allowed for %d minutes before shift begin",igClockInTolerance/60);
        }
        else
        {
            dbg(TRACE,"Clock in tolerance not found set to %d minutes before shift begin",igClockInTolerance/60);
            ilRc = RC_SUCCESS;
        }


        ilRc = ReadConfigEntry("MAIN","CLOCKOUTTOLERANZ",clTmpString);
        if(ilRc == RC_SUCCESS)
        {

            igClockOutTolerance = atoi(clTmpString)*60;
            dbg(TRACE,"Clock out allowed for %d minutes after shift end",igClockOutTolerance/60);
        }
        else
        {
            dbg(TRACE,"Clock out tolerance not found set to %d minutes after shift end",igClockOutTolerance/60);
            ilRc = RC_SUCCESS;
        }



        ilRc = ReadConfigEntry("TEST","DAILYOFFSET",clTmpString);
        if(ilRc == RC_SUCCESS)
        {
            char clTmpOffset[24];

            dbg(DEBUG,"Send actual updates from <%s> days",clTmpString);
            ilRc = get_real_item(&clTmpOffset[0],clTmpString,1);
            igDailyOffset = atoi(clTmpOffset);
            ilRc = get_real_item(&clTmpOffset[0],clTmpString,2);
            igDailyEnd = atoi(clTmpOffset);
            dbg(TRACE,"Send daily updates for the next %d days, start with offset %d",
                        igDailyEnd,igDailyOffset);
        }
        else
        {
            dbg(TRACE,"DAILYOFFSET not found send daily updates for %d days, "
            " start with offset %d",
                    igExpEnd,igExpOffset);
            ilRc = RC_SUCCESS;
        }


        ilRc = ReadConfigEntry("MAIN","PERIOD",clTmpString);
        if(ilRc == RC_SUCCESS)
        {
            char clTmpOffset[24];

            dbg(DEBUG,"Send shifts for the next <%s> days",clTmpString);
            ilRc = get_real_item(&clTmpOffset[0],clTmpString,1);
            igExpOffset = atoi(clTmpOffset);
            ilRc = get_real_item(&clTmpOffset[0],clTmpString,2);
            igExpEnd = atoi(clTmpOffset);
            dbg(TRACE,"Send shifts for the next %d days, start with offset %d",
                        igExpEnd,igExpOffset);
        }
        else
        {
            dbg(TRACE,"Period not found send shifts for %d days, start with offset %d",
                    igExpEnd,igExpOffset);
            ilRc = RC_SUCCESS;
        }

        ilRc = ReadConfigEntry("MAIN","SENDTOWMQ",clTmpString);
        if(ilRc == RC_SUCCESS)
        {

            strncpy(cgTaa,clTmpString,12);
            cgTaa[12] = '\0';
            igTaa = atoi(cgTaa);
            dbg(TRACE,"Send shift lines to <%s> %d",clTmpString,igTaa);
        }
        else
        {
            strcpy(cgTaa,"taa");
            igTaa = 7288;
            dbg(TRACE,"Process to send shift lines to not found in config file."
            "Set to<%s> %d ",cgTaa,igTaa);
        }

        if ((ilTmpModId = tool_get_q_id(cgTaa)) > RC_SUCCESS)
        {
            igAction = ilTmpModId;
            dbg(TRACE,"InitTaahdl: Mod-Id for <%s> set to %d",cgTaa,igTaa);
        }
        else
        {
            dbg(TRACE,"InitTaahdl: Mod-Id for <%s> not found, set to default %d",cgTaa,igTaa);
        }

        ilRc = ReadConfigEntry("SYSTEM","TAAFIL",clTmpString);
        if (ilRc == RC_SUCCESS)
        {
            if (strncmp(clTmpString,"YES",3) == 0)
            {
            bgIsTaahdl = FALSE;
            }
                }
            else
        {
            ilRc = RC_SUCCESS;
            dbg(TRACE,"Key TAAFIL in SYSTEM not found");
        }
        dbg(TRACE,"IsTaahdl set to %s",bgIsTaahdl ? "TRUE" : "FALSE");
        memset(rgBreaks,'\0',sizeof(rgBreaks));
        for( ilLc = 0; ilLc < MAXBREAKS; ilLc++)
        {
            char clTmpBreak[24];
            char clTmpBreakTime[124];

            sprintf(clTmpBreak,"BREAK%02d",ilLc+1);
            ilRc = ReadConfigEntry("BREAKS",clTmpBreak,clTmpBreakTime);
            if(ilRc == RC_SUCCESS)
            {
                char clBreakStart[24];
                char clBreakEnd[24];

                dbg(DEBUG,"Break found <%s>",clTmpBreakTime);
                
                strncpy(clBreakStart,clTmpBreakTime,4);
                clBreakStart[4] = '\0';
                strcpy(clBreakEnd,&clTmpBreakTime[5]);
                strcpy(rgBreaks[ilLc].Start,clBreakStart);
                strcpy(rgBreaks[ilLc].End,clBreakEnd);
                rgBreaks[ilLc].used = TRUE;
                dbg(TRACE,"Break No %d used %d from <%s>to <%s>",ilLc,  rgBreaks[ilLc].used ,
                        rgBreaks[ilLc].Start,rgBreaks[ilLc].End);
            }
            else
            {
                dbg(TRACE,"No more breaks found");
                ilRc = RC_SUCCESS;
                break;
            }
        }

        ilRc = ReadConfigLine("SYSTEM","ALERT_NAME",cgAlertName);
        if(ilRc == RC_SUCCESS)
        {
            dbg(TRACE,"Alert Name set to <%s>",cgAlertName);
        }
        else
        {
            strcpy(cgAlertName,"Y");
            dbg(TRACE,"Alert Name not found, set to <%s>",
                        cgAlertName);
            ilRc = RC_SUCCESS;
        }
        ilRc = ReadConfigLine("SYSTEM","TOHR",cgTohr);
        if(ilRc == RC_SUCCESS)
        {
            dbg(TRACE,"TOHR set to <%s>",cgTohr);
        }
        else
        {
            strcpy(cgTohr,"Y");
            dbg(TRACE,"TOHR not found, set to <%s>",cgTohr);
            ilRc = RC_SUCCESS;
        }
        ilRc = ReadConfigLine("SYSTEM","DEF_PENO",cgDefaultPeno);
        if(ilRc == RC_SUCCESS)
        {
            dbg(TRACE,"Default peno set to <%s>",cgDefaultPeno);
        }
        else
        {
            strcpy(cgDefaultPeno,"00000001");
            dbg(TRACE,"Default Peno not found, set to <%s>",
                        cgDefaultPeno);
            ilRc = RC_SUCCESS;
        }
    }

        ilRc = ReadConfigLine("MAIN","DrrReloadHintStfu",cgDrrReloadHintStfu);
    if ( ilRc != RC_SUCCESS )
        {
      strcpy (cgDrrReloadHintStfu, "/*+ INDEX(DRRTAB DRRTAB_STFU) */");
        ilRc = RC_SUCCESS;
        }
    dbg(TRACE,"InitTaahdl: Hint for reloading DRRTAB without ROSL <%s>", 
            cgDrrReloadHintStfu );
        ilRc = ReadConfigLine("MAIN","DrrReloadHintRosl",cgDrrReloadHintRosl);
    if ( ilRc != RC_SUCCESS )
        {
      strcpy (cgDrrReloadHintRosl, "/*+ INDEX(DRRTAB DRRTAB_SDAY_ROSL) */");
        ilRc = RC_SUCCESS;
        }
    dbg(TRACE,"InitPolhdl: Hint for reloading DRRTAB with ROSL <%s>", 
            cgDrrReloadHintRosl );
    
        ilRc = ReadConfigLine("MAIN","JobReloadHint",cgJobReloadHint);
    if ( ilRc != RC_SUCCESS )
        {
      strcpy (cgJobReloadHint, "/*+ INDEX(JOBTAB JOBTAB_ACTO) */");
        ilRc = RC_SUCCESS;
        }
    
        ilRc = ReadConfigLine("MAIN","PoolJobHint",cgPoolJobHint);
    if ( ilRc != RC_SUCCESS )
        {
      strcpy (cgPoolJobHint, "/*+ INDEX(JOBTAB JOBTAB_ACTO) */");
        ilRc = RC_SUCCESS;
        }
        ilRc = ReadConfigLine("MAIN","JavHint",cgJavHint);
    if ( ilRc != RC_SUCCESS )
        {
      strcpy (cgJavHint, "");
        ilRc = RC_SUCCESS;
        }
    
    
    if(ilRc == RC_SUCCESS)
    {
        ilRc = CEDAArrayInitialize(20,20);
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"InitTaahdl: CEDAArrayInitialize failed <%d>",ilRc);
        }/* end of if */
    }/* end of if */



    if(ilRc == RC_SUCCESS)
    {

        strcpy(cgJavFields,JAV_FIELDS);
        *pclSelection = '\0';
        dbg(TRACE,"InitTaahdl: SetArrayInfo JAVTAB array");
        ilRc = SetArrayInfo("JAVTAB","JAVTAB",cgJavFields,
                NULL,NULL,&rgJavArray,pclSelection,FALSE);
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"InitTaahdl: SetArrayInfo JAVTAB failed <%d>",ilRc);
        }
        else
        {
            dbg(DEBUG,"InitTaahdl: SetArrayInfo JAVTAB OK");
            dbg(DEBUG,"<%s> %d",rgJavArray.crArrayName,rgJavArray.rrArrayHandle);

            dbg(DEBUG,"<%s> %d",rgJavArray.crArrayName,rgJavArray.rrArrayHandle);
            strcpy(rgJavArray.crIdx01Name,"JAVTAB_USTF");
            ilRc = CEDAArrayCreateSimpleIndexUp(&rgJavArray.rrArrayHandle,
                rgJavArray.crArrayName,
                &rgJavArray.rrIdx01Handle,
                rgJavArray.crIdx01Name,"USTF");
                strcpy(rgJavArray.crIdx01FieldList,"USTF");
            SetIndexInfo(&rgJavArray,"USTF",1);
        }
    }
    if(ilRc == RC_SUCCESS)
    {

        strcpy(cgJobFields,JOB_FIELDS);
        *pclSelection = '\0';
        dbg(TRACE,"InitTaahdl: SetArrayInfo POL-JOB array");
        ilRc = SetArrayInfo("POL-JOB","JOBTAB",cgJobFields,
                NULL,NULL,&rgPolJobArray,pclSelection,FALSE);
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"InitTaahdl: SetArrayInfo JOBTAB failed <%d>",ilRc);
        }
        else
        {
            dbg(DEBUG,"InitTaahdl: SetArrayInfo POL-JOB OK");
            dbg(DEBUG,"<%s> %d",rgPolJobArray.crArrayName,rgPolJobArray.rrArrayHandle);

            dbg(DEBUG,"<%s> %d",rgPolJobArray.crArrayName,rgPolJobArray.rrArrayHandle);
            strcpy(rgPolJobArray.crIdx01Name,"POLTAB_UDSR");
            ilRc = CEDAArrayCreateSimpleIndexUp(&rgPolJobArray.rrArrayHandle,
                rgPolJobArray.crArrayName,
                &rgPolJobArray.rrIdx01Handle,
                rgPolJobArray.crIdx01Name,"UDSR");
                strcpy(rgPolJobArray.crIdx01FieldList,"UDSR");
            SetIndexInfo(&rgPolJobArray,"UDSR",1);
        }
    }

    if(ilRc == RC_SUCCESS)
    {

        strcpy(cgJobFields,JOB_FIELDS);
        *pclSelection = '\0';
        dbg(TRACE,"InitTaahdl: SetArrayInfo JOBTAB array");
        ilRc = SetArrayInfo("JOBTAB","JOBTAB",cgJobFields,
                NULL,NULL,&rgJobArray,pclSelection,FALSE);
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"InitTaahdl: SetArrayInfo JOBTAB failed <%d>",ilRc);
        }
        else
        {
            dbg(DEBUG,"InitTaahdl: SetArrayInfo JOBTAB OK");
            dbg(DEBUG,"<%s> %d",rgJobArray.crArrayName,rgJobArray.rrArrayHandle);

            dbg(DEBUG,"<%s> %d",rgJobArray.crArrayName,rgJobArray.rrArrayHandle);
            strcpy(rgJobArray.crIdx01Name,"JOBTAB_UDSR");
            ilRc = CEDAArrayCreateSimpleIndexUp(&rgJobArray.rrArrayHandle,
                rgJobArray.crArrayName,
                &rgJobArray.rrIdx01Handle,
                rgJobArray.crIdx01Name,"UDSR");
                strcpy(rgJobArray.crIdx01FieldList,"UDSR");
            SetIndexInfo(&rgJobArray,"UDSR",1);
        }
    }

    if(ilRc == RC_SUCCESS)
    {
        strcpy(cgDraFields,DRA_FIELDS);
        *pclSelection = '\0';
        dbg(TRACE,"InitTaahdl: SetArrayInfo DRATAB array");
        ilRc = SetArrayInfo("DRATAB","DRATAB",cgDraFields,
                NULL,NULL,&rgDraArray,pclSelection,FALSE);
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"InitTaahdl: SetArrayInfo DRATAB failed <%d>",ilRc);
        }
        else
        {
            dbg(DEBUG,"InitTaahdl: SetArrayInfo DRATAB OK");
            dbg(DEBUG,"<%s> %d",rgDraArray.crArrayName,rgDraArray.rrArrayHandle);

            strcpy(rgDraArray.crIdx01Name,"DRA_STFU");
            ilRc = CEDAArrayCreateSimpleIndexUp(&rgDraArray.rrArrayHandle,
                rgDraArray.crArrayName,
                &rgDraArray.rrIdx01Handle,
                rgDraArray.crIdx01Name,"STFU");
                strcpy(rgDraArray.crIdx01FieldList,"STFU,DRRN");
            SetIndexInfo(&rgDraArray,"STFU",1);
            
            strcpy(rgDraArray.crIdx02Name,"DRA_URNO");
            ilRc = CEDAArrayCreateSimpleIndexUp(&rgDraArray.rrArrayHandle,
                rgDraArray.crArrayName,
                &rgDraArray.rrIdx02Handle,
                rgDraArray.crIdx02Name,"URNO");
                strcpy(rgDraArray.crIdx02FieldList,"URNO");
            SetIndexInfo(&rgDraArray,"URNO",1);
        }
    }

    if(ilRc == RC_SUCCESS)
    {
        long pclAddFieldLens[12];  


         pclAddFieldLens[0] = 2;  
         pclAddFieldLens[1] = 2;  
         pclAddFieldLens[1] = 0;  

        strcpy(cgDrrFields,DRR_FIELDS);
        *pclSelection = '\0';
        dbg(TRACE,"InitTaahdl: SetArrayInfo DRRTAB array");
        ilRc = SetArrayInfo("DRRTABP","DRRTAB",cgDrrFields,
                NULL,NULL,&rgDrrArrayP,pclSelection,FALSE);
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"InitTaahdl: SetArrayInfo DRRTAB-P failed <%d>",ilRc);
        }
        else
        {
            dbg(DEBUG,"InitTaahdl: SetArrayInfo DRRTAB-P OK");
            dbg(DEBUG,"<%s> %d",rgDrrArrayP.crArrayName,rgDrrArrayP.rrArrayHandle);

            strcpy(rgDrrArrayP.crIdx01Name,"DRRP_STFU");
            ilRc = CEDAArrayCreateSimpleIndexUp(&rgDrrArrayP.rrArrayHandle,
                rgDrrArrayP.crArrayName,
                &rgDrrArrayP.rrIdx01Handle,
                rgDrrArrayP.crIdx01Name,"STFU");
                strcpy(rgDrrArrayP.crIdx01FieldList,"STFU");
            SetIndexInfo(&rgDrrArrayP,"STFU",1);
        }
        dbg(DEBUG,"<%s> %d",rgDrrArrayP.crArrayName,rgDrrArrayP.rrArrayHandle);
    }

    if(ilRc == RC_SUCCESS)
    {
        long pclAddFieldLens[12];  


         pclAddFieldLens[0] = 2;  
         pclAddFieldLens[1] = 2;  
         pclAddFieldLens[1] = 0;  

        strcpy(cgDrrFields,DRR_FIELDS);
        *pclSelection = '\0';
        dbg(TRACE,"InitTaahdl: SetArrayInfo DRRTAB array");
        ilRc = SetArrayInfo("DRRTAB","DRRTAB",cgDrrFields,
                NULL,NULL,&rgDrrArray,pclSelection,FALSE);
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"InitTaahdl: SetArrayInfo DRRTAB failed <%d>",ilRc);
        }
        else
        {
            dbg(DEBUG,"InitTaahdl: SetArrayInfo DRRTAB OK");
            dbg(DEBUG,"<%s> %d",rgDrrArray.crArrayName,rgDrrArray.rrArrayHandle);

            dbg(DEBUG,"<%s> %d",rgDrrArray.crArrayName,rgDrrArray.rrArrayHandle);
            strcpy(rgDrrArray.crIdx01Name,"DRRPLAN_STFU");
            ilRc = CEDAArrayCreateSimpleIndexUp(&rgDrrArray.rrArrayHandle,
                rgDrrArray.crArrayName,
                &rgDrrArray.rrIdx01Handle,
                rgDrrArray.crIdx01Name,"STFU,ROSS");
                strcpy(rgDrrArray.crIdx01FieldList,"STFU,ROSS");
            SetIndexInfo(&rgDrrArray,"STFU,ROSS",1);

            strcpy(rgDrrArray.crIdx02Name,"DRR_URNO");
            ilRc = CEDAArrayCreateSimpleIndexUp(&rgDrrArray.rrArrayHandle,
                rgDrrArray.crArrayName,
                &rgDrrArray.rrIdx02Handle,
                rgDrrArray.crIdx02Name,"URNO");
                strcpy(rgDrrArray.crIdx02FieldList,"URNO");
            SetIndexInfo(&rgDrrArray,"URNO",2);

            strcpy(rgDrrArray.crIdx03Name,"DRR_STFU");
            ilRc = CEDAArrayCreateSimpleIndexUp(&rgDrrArray.rrArrayHandle,
                rgDrrArray.crArrayName,
                &rgDrrArray.rrIdx03Handle,
                rgDrrArray.crIdx03Name,"STFU,SDAY,ROSL");
                strcpy(rgDrrArray.crIdx03FieldList,"STFU,SDAY,ROSL");
            SetIndexInfo(&rgDrrArray,"STFU,SDAY,ROSL",3);
        }
        dbg(DEBUG,"<%s> %d",rgDrrArray.crArrayName,rgDrrArray.rrArrayHandle);
    }

    if(ilRc == RC_SUCCESS)
    {

        sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);
        dbg(TRACE,"InitTaahdl: SetArrayInfo ODATAB array \n<%s>",pclSelection);
        ilRc = SetArrayInfo("ODATAB","ODATAB",ODA_FIELDS,NULL,NULL,&rgOdaArray,pclSelection,TRUE);
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"InitTaahdl: SetArrayInfo ODATAB failed <%d>",ilRc);
        }
        else
        {
            dbg(DEBUG,"InitTaahdl: SetArrayInfo ODATAB OK");
            dbg(DEBUG,"<%s> %d",rgOdaArray.crArrayName,rgOdaArray.rrArrayHandle);
            strcpy(rgOdaArray.crIdx01Name,"ODA_URNO");
            ilRc = CEDAArrayCreateSimpleIndexUp(&rgOdaArray.rrArrayHandle,
                rgOdaArray.crArrayName,
                &rgOdaArray.rrIdx01Handle,
                rgOdaArray.crIdx01Name,"URNO");
                strcpy(rgOdaArray.crIdx01FieldList,"URNO");
            SetIndexInfo(&rgOdaArray,"URNO",1);

            strcpy(rgOdaArray.crIdx02Name,"ODA_SDAC");
            ilRc = CEDAArrayCreateSimpleIndexUp(&rgOdaArray.rrArrayHandle,
                rgOdaArray.crArrayName,
                &rgOdaArray.rrIdx02Handle,
                rgOdaArray.crIdx02Name,"SDAC");
                strcpy(rgOdaArray.crIdx02FieldList,"SDAC");
            SetIndexInfo(&rgOdaArray,"SDAC",2);
        }

    }
    if(ilRc == RC_SUCCESS)
    {

        sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);
        dbg(TRACE,"InitTaahdl: SetArrayInfo BSDTAB array \n<%s>",pclSelection);
        ilRc = SetArrayInfo("BSDTAB","BSDTAB",BSD_FIELDS,NULL,NULL,&rgBsdArray,pclSelection,TRUE);
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"InitTaahdl: SetArrayInfo BSDTAB failed <%d>",ilRc);
        }
        else
        {
            dbg(DEBUG,"InitTaahdl: SetArrayInfo BSDTAB OK");
            dbg(DEBUG,"<%s> %d",rgBsdArray.crArrayName,rgBsdArray.rrArrayHandle);
            strcpy(rgBsdArray.crIdx01Name,"BSD_URNO");
            ilRc = CEDAArrayCreateSimpleIndexUp(&rgBsdArray.rrArrayHandle,
                rgBsdArray.crArrayName,
                &rgBsdArray.rrIdx01Handle,
                rgBsdArray.crIdx01Name,"URNO");
                strcpy(rgBsdArray.crIdx01FieldList,"URNO");
            SetIndexInfo(&rgBsdArray,"URNO",1);

            strcpy(rgBsdArray.crIdx02Name,"BSD_BSDC");
            ilRc = CEDAArrayCreateSimpleIndexUp(&rgBsdArray.rrArrayHandle,
                rgBsdArray.crArrayName,
                &rgBsdArray.rrIdx02Handle,
                rgBsdArray.crIdx02Name,"BSDC");
                strcpy(rgBsdArray.crIdx02FieldList,"BSDC");
            SetIndexInfo(&rgBsdArray,"BSDC",2);
        }
    }



    if(ilRc == RC_SUCCESS)
    {
        sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);
        dbg(TRACE,"InitTaahdl: SetArrayInfo ORGTAB array \n<%s>",pclSelection);
        ilRc = SetArrayInfo("ORGTAB","ORGTAB",ORG_FIELDS,NULL,NULL,&rgOrgArray,pclSelection,TRUE);
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"InitTaahdl: SetArrayInfo ORGTAB failed <%d>",ilRc);
        }
        else
        {
            dbg(DEBUG,"InitTaahdl: SetArrayInfo ORGTAB OK");
            dbg(DEBUG,"<%s> %d",rgOrgArray.crArrayName,rgOrgArray.rrArrayHandle);
            strcpy(rgOrgArray.crIdx01Name,"ORG_URNO");
            ilRc = CEDAArrayCreateSimpleIndexUp(&rgOrgArray.rrArrayHandle,
                rgOrgArray.crArrayName,
                &rgOrgArray.rrIdx01Handle,
                rgOrgArray.crIdx01Name,"URNO");
                strcpy(rgOrgArray.crIdx01FieldList,"URNO");
            SetIndexInfo(&rgOrgArray,"URNO",1);

            strcpy(rgOrgArray.crIdx02Name,"ORG_DPT1");
            ilRc = CEDAArrayCreateSimpleIndexUp(&rgOrgArray.rrArrayHandle,
                rgOrgArray.crArrayName,
                &rgOrgArray.rrIdx02Handle,
                rgOrgArray.crIdx02Name,"DPT1");
                strcpy(rgOrgArray.crIdx02FieldList,"DPT1");
            SetIndexInfo(&rgOrgArray,"DPT1",2);
        }

    }

    /*DumpArray(&rgOrgArray,"URNO,DPT1,DPTN");*/
    if(ilRc == RC_SUCCESS)
    {
        strcpy(cgStfFields,STF_FIELDS);
        sprintf(pclSelection,"WHERE HOPO = '%s' ",cgHopo);

        dbg(TRACE,"InitTaahdl: SetArrayInfo STFTAB array \n<%s>",pclSelection);
        ilRc = SetArrayInfo("STFTAB","STFTAB",cgStfFields,NULL,NULL,&rgStfArray,pclSelection,TRUE);
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"InitTaahdl: SetArrayInfo STFTAB failed <%d>",ilRc);
        }
        else
        {
            dbg(DEBUG,"InitTaahdl: SetArrayInfo STFTAB OK");
            dbg(DEBUG,"<%s> %d",rgStfArray.crArrayName,rgStfArray.rrArrayHandle);
            strcpy(rgStfArray.crIdx01Name,"STF_URNO");
            ilRc = CEDAArrayCreateSimpleIndexUp(&rgStfArray.rrArrayHandle,
                rgStfArray.crArrayName,
                &rgStfArray.rrIdx01Handle,
                rgStfArray.crIdx01Name,"URNO");
                strcpy(rgStfArray.crIdx01FieldList,"URNO");
                SetIndexInfo(&rgStfArray,"URNO",1);
            strcpy(rgStfArray.crIdx02Name,"STF_PENO");
            ilRc = CEDAArrayCreateSimpleIndexUp(&rgStfArray.rrArrayHandle,
                rgStfArray.crArrayName,
                &rgStfArray.rrIdx02Handle,
                rgStfArray.crIdx02Name,"PENO");
                strcpy(rgStfArray.crIdx02FieldList,"PENO");
                SetIndexInfo(&rgStfArray,"PENO",2);
        }
    }

    /*DumpArray(&rgStfArray,"URNO,PENO");*/

    if(ilRc == RC_SUCCESS)
    {
        strcpy(cgPfcFields,PFC_FIELDS);
        sprintf(pclSelection,"WHERE HOPO = '%s' ",cgHopo);

        dbg(TRACE,"InitTaahdl: SetArrayInfo PFCTAB array \n<%s>",pclSelection);
        ilRc = SetArrayInfo("PFCTAB","PFCTAB",cgPfcFields,NULL,NULL,&rgPfcArray,pclSelection,TRUE);
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"InitTaahdl: SetArrayInfo PFCTAB failed <%d>",ilRc);
        }
        else
        {
            dbg(DEBUG,"InitTaahdl: SetArrayInfo PFCTAB OK");
            dbg(DEBUG,"<%s> %d",rgPfcArray.crArrayName,rgPfcArray.rrArrayHandle);
            strcpy(rgPfcArray.crIdx01Name,"PFC_URNO");
            ilRc = CEDAArrayCreateSimpleIndexUp(&rgPfcArray.rrArrayHandle,
                rgPfcArray.crArrayName,
                &rgPfcArray.rrIdx01Handle,
                rgPfcArray.crIdx01Name,"URNO");
                strcpy(rgPfcArray.crIdx01FieldList,"URNO");
                SetIndexInfo(&rgPfcArray,"URNO",1);
            strcpy(rgPfcArray.crIdx02Name,"PFC_FCTC");
            ilRc = CEDAArrayCreateSimpleIndexUp(&rgPfcArray.rrArrayHandle,
                rgPfcArray.crArrayName,
                &rgPfcArray.rrIdx02Handle,
                rgPfcArray.crIdx02Name,"FCTC");
                strcpy(rgPfcArray.crIdx02FieldList,"FCTC");
                SetIndexInfo(&rgPfcArray,"FCTC",2);
        }
    }
    
    if(ilRc == RC_SUCCESS)
    {
        strcpy(cgSpfFields,SPF_FIELDS);
        sprintf(pclSelection,"WHERE HOPO = '%s' ",cgHopo);

        dbg(TRACE,"InitTaahdl: SetArrayInfo SPFTAB array \n<%s>",pclSelection);
        ilRc = SetArrayInfo("SPFTAB","SPFTAB",cgSpfFields,NULL,NULL,&rgSpfArray,pclSelection,TRUE);
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"InitTaahdl: SetArrayInfo SPFTAB failed <%d>",ilRc);
        }
        else
        {
            dbg(DEBUG,"InitTaahdl: SetArrayInfo SPFTAB OK");
            dbg(DEBUG,"<%s> %d",rgSpfArray.crArrayName,rgSpfArray.rrArrayHandle);
            strcpy(rgSpfArray.crIdx01Name,"SPF_URNO");
            ilRc = CEDAArrayCreateSimpleIndexUp(&rgSpfArray.rrArrayHandle,
                rgSpfArray.crArrayName,
                &rgSpfArray.rrIdx01Handle,
                rgSpfArray.crIdx01Name,"URNO");
                strcpy(rgSpfArray.crIdx01FieldList,"URNO");
                SetIndexInfo(&rgSpfArray,"URNO",1);
            strcpy(rgSpfArray.crIdx02Name,"PFC_SURN");
            ilRc = CEDAArrayCreateSimpleIndexUp(&rgSpfArray.rrArrayHandle,
                rgSpfArray.crArrayName,
                &rgSpfArray.rrIdx02Handle,
                rgSpfArray.crIdx02Name,"SURN");
                strcpy(rgSpfArray.crIdx02FieldList,"SURN");
                SetIndexInfo(&rgSpfArray,"SURN",2);
        }
    }

    if(ilRc == RC_SUCCESS)
    {
        ilRc = CEDAArrayReadAllHeader(outp);
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"InitTaahdl: CEDAArrayReadAllHeader failed <%d>",ilRc);
        }else{
            dbg(TRACE,"InitTaahdl: CEDAArrayReadAllHeader OK");
        }/* end of if */
    }/* end of if */

    
    pcgStfuList = malloc(lgStfuListLength);
    if(pcgStfuList == NULL)
    {
        dbg(TRACE,"unable to allocate %ld Bytes for StfuList",lgStfuListLength);
        ilRc = RC_FAIL;
    }
    else
    {
        memset(pcgStfuList,0,lgStfuListLength);
    }

    if(ilRc == RC_SUCCESS)
    {
        ilRc = InitFieldIndex();
    }


    if(ilRc == RC_SUCCESS)
    {
        char clSelection[244];

        sprintf(clSelection,"WHERE NAME='FLT' AND HOPO='%s'",cgHopo);

        ilRc = DoSingleSelect("JTYTAB",clSelection,"URNO",cgDataArea);
        if (ilRc == RC_SUCCESS)
        {
            strcpy(cgFlightUjty,cgDataArea);
            dbg(TRACE,"InitTaahdl: UJTY set to <%s>",cgFlightUjty);
        }
        else
        {
            dbg(TRACE,"InitTaahdl: <FLT> not found in JTYTAB UJTY not set");
        }
    }

    if(ilRc == RC_SUCCESS)
    {
        char clSelection[244];

        sprintf(clSelection,"WHERE NAME='POL' AND HOPO='%s'",cgHopo);

        ilRc = DoSingleSelect("JTYTAB",clSelection,"URNO",cgDataArea);
        if (ilRc == RC_SUCCESS)
        {
            strcpy(cgPolUjty,cgDataArea);
            dbg(TRACE,"InitTaahdl: UJTY for Pooljobs set to <%s>",cgPolUjty);
        }
        else
        {
            dbg(TRACE,"InitTaahdl: <JOB> not found in JTYTAB UJTY not set");
        }
    }
    if(ilRc == RC_SUCCESS)
    {
        char clSelection[244];

        sprintf(clSelection,"WHERE NAME='BRK' AND HOPO='%s'",cgHopo);

        ilRc = DoSingleSelect("JTYTAB",clSelection,"URNO",cgDataArea);
        if (ilRc == RC_SUCCESS)
        {
            strcpy(cgBrkUjty,cgDataArea);
            dbg(TRACE,"InitTaahdl: UJTY set to <%s>",cgBrkUjty);
        }
        else
        {
            dbg(TRACE,"InitTaahdl: <BRK> not found in JTYTAB UJTY not set");
        }
    }

    if(ilRc == RC_SUCCESS)
    {
        igInitOK = TRUE;
        dbg(TRACE,"InitTaahdl: InitFieldIndex OK");
    }
    else
    {
        dbg(TRACE,"InitTaahdl: InitFieldIndex failed");
    }


    ReloadArrays(FALSE);
    if (bgIsTaahdl)
    {
        TriggerAction("JOBTAB",JOB_FIELDS_ACTION);
        TriggerAction("SPRTAB","ACTI!,FCOL!,PKNO!");
        TriggerAction("DRATAB",DRA_FIELDS);
    }
    TriggerAction("ODATAB",ODA_FIELDS);
    TriggerAction("STFTAB","URNO!,PENO!,TOHR!");
    TriggerAction("BSDTAB",BSD_FIELDS);
    TriggerAction("ORGTAB",ORG_FIELDS);
    TriggerAction("SPFTAB",SPF_FIELDS);
    TriggerAction("PFCTAB",PFC_FIELDS);
    /*TriggerAction("DRRTAB","AVFR,AVTO,DRS1,DRS3,DRS4,HOPO,REMA,SBFR,SBLU,"
           "DRRN,SBTO,SCOD,SDAY,STFU,URNO,USEC,USEU");*/
    dbg(TRACE,"Debug Level is %d",debug_level);
    return (ilRc);  
} /* end of initialize */


static int SetIndexInfo(ARRAYINFO *prpArrayInfo,char *pcpFieldList,int ilIndex)
{
    int ilRc = RC_SUCCESS;              /* Return code */
  int ilLc;
    char **ppclIdxRowBuf;
    long *pllIdxRowLen;

    dbg(DEBUG,"%05d:SetIndexInfo:",__LINE__);
    switch(ilIndex)
    {
        case 1:
            ppclIdxRowBuf = &prpArrayInfo->pcrIdx01RowBuf;
            pllIdxRowLen = &prpArrayInfo->lrIdx01RowLen;
            break;
        case 2:
            ppclIdxRowBuf = &prpArrayInfo->pcrIdx02RowBuf;
            pllIdxRowLen = &prpArrayInfo->lrIdx02RowLen;
            break;
        case 3:
            ppclIdxRowBuf = &prpArrayInfo->pcrIdx03RowBuf;
            pllIdxRowLen = &prpArrayInfo->lrIdx03RowLen;
            break;
        default:
            ilRc = RC_FAIL;
            dbg(DEBUG,"SetIndexInfo:ivalid index %d",ilIndex);
    }

    if (ilRc == RC_SUCCESS)
    {
  ilRc = GetRowLength(cgHopo,prpArrayInfo->crArrayTableName,pcpFieldList,
                            pllIdxRowLen);
    *ppclIdxRowBuf = (char *) calloc(1,(*pllIdxRowLen+10));
    if(*ppclIdxRowBuf == NULL)
  {
    ilRc = RC_FAIL;
    dbg(TRACE,"SetArrayInfo: calloc failed");
   }/* end of if */
    }

    return ilRc;
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
static int SetArrayInfo(char *pcpArrayName, char *pcpTableName, char *pcpArrayFieldList,char *pcpAddFields,long *pcpAddFieldLens,ARRAYINFO *prpArrayInfo,
    char *pcpSelection,BOOL bpFill)
{
    int ilRc = RC_SUCCESS;              /* Return code */
  int ilLc;

    if(prpArrayInfo == NULL)
    {
        dbg(TRACE,"SetArrayInfo: invalid last parameter : null pointer not valid");
        ilRc = RC_FAIL;
    }else
    {
        memset(prpArrayInfo,0x00,sizeof(prpArrayInfo));

        prpArrayInfo->rrArrayHandle = -1;
        prpArrayInfo->rrIdx01Handle = -1;

        if(pcpArrayName != NULL)
        {
            if(strlen(pcpArrayName) <= ARR_NAME_LEN)
            {
                strcpy(prpArrayInfo->crArrayName,pcpArrayName);
            }/* end of if */
        }/* end of if */
    }/* end of if */

    strcpy(prpArrayInfo->crArrayTableName,pcpTableName);
    if(ilRc == RC_SUCCESS)
    {
        prpArrayInfo->lrArrayFieldCnt = get_no_of_items(pcpArrayFieldList);
        if (pcpAddFields != NULL)
        {
            prpArrayInfo->lrArrayFieldCnt += get_no_of_items(pcpAddFields);
        }

        prpArrayInfo->plrArrayFieldOfs = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
        if(prpArrayInfo->plrArrayFieldOfs == NULL)
        {
            dbg(TRACE,"SetArrayInfo: plrArrayFieldOfs calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
            ilRc = RC_FAIL;
        }/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
        prpArrayInfo->plrArrayFieldLen = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
        if(prpArrayInfo->plrArrayFieldLen == NULL)
        {
            dbg(TRACE,"SetArrayInfo: plrArrayFieldLen calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
            ilRc = RC_FAIL;
        }else{
            *(prpArrayInfo->plrArrayFieldLen) = -1;
        }/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
        ilRc = GetRowLength(&cgHopo[0],pcpTableName,pcpArrayFieldList,
                            &(prpArrayInfo->lrArrayRowLen));
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpArrayName,pcpArrayFieldList);
        }/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
        prpArrayInfo->lrArrayRowLen+=100;
        prpArrayInfo->pcrArrayRowBuf = (char *) calloc(1,(prpArrayInfo->lrArrayRowLen+10));
        if(prpArrayInfo->pcrArrayRowBuf == NULL)
        {
                ilRc = RC_FAIL;
                dbg(TRACE,"SetArrayInfo: calloc failed");
        }/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
        prpArrayInfo->pcrIdx01RowBuf = (char *) calloc(1,(prpArrayInfo->lrIdx01RowLen+1));
        if(prpArrayInfo->pcrIdx01RowBuf == NULL)
        {
                ilRc = RC_FAIL;
                dbg(TRACE,"SetArrayInfo: calloc failed");
        }/* end of if */
    }/* end of if */

    if(ilRc == RC_SUCCESS)
    {
        dbg(DEBUG,"CEDAArrayCreate follows Array <%s> Table <%s>",pcpArrayName,pcpTableName);
        ilRc = CEDAArrayCreate(&(prpArrayInfo->rrArrayHandle),
                pcpArrayName,pcpTableName,pcpSelection,pcpAddFields,
                pcpAddFieldLens,pcpArrayFieldList,
                &(prpArrayInfo->plrArrayFieldLen[0]),
                &(prpArrayInfo->plrArrayFieldOfs[0]));

        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"SetArrayInfo: CEDAArrayInitialize failed <%d>",ilRc);
        }/* end of if */
    }/* end of if */

        if(pcpArrayFieldList != NULL)
        {
            if(strlen(pcpArrayFieldList) <= ARR_FLDLST_LEN)
            {
                strcpy(prpArrayInfo->crArrayFieldList,pcpArrayFieldList);
            }/* end of if */
        }/* end of if */
        if(pcpAddFields != NULL)
        {
            if(strlen(pcpAddFields) + 
                strlen(prpArrayInfo->crArrayFieldList) <= (size_t)ARR_FLDLST_LEN)
            {
                strcat(prpArrayInfo->crArrayFieldList,",");
                strcat(prpArrayInfo->crArrayFieldList,pcpAddFields);
                dbg(DEBUG,"<%s>",prpArrayInfo->crArrayFieldList);
            }/* end of if */
        }/* end of if */


    if(ilRc == RC_SUCCESS && bpFill)
    {
        ilRc = CEDAArrayFill(&(prpArrayInfo->rrArrayHandle),pcpArrayName,NULL);
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"SetArrayInfo: CEDAArrayFill failed <%d>",ilRc);
        }/* end of if */
    }/* end of if */

   for(ilLc = 0; ilLc < prpArrayInfo->lrArrayFieldCnt; ilLc++)
   {
            char    pclFieldName[25];

            *pclFieldName  = '\0';

            GetDataItem(pclFieldName,prpArrayInfo->crArrayFieldList,
                        ilLc+1,',',"","\0\0");
     dbg(DEBUG,"%02d %-10s Offset: %d",
                ilLc,pclFieldName,prpArrayInfo->plrArrayFieldOfs[ilLc]);
   }   

    return(ilRc);
}



/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
    int ilRc = RC_SUCCESS;              /* Return code */
    
    dbg(TRACE,"Reset: now resetting");
    
    return ilRc;
    
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
    /* unset SIGCHLD ! DB-Child will terminate ! */

    dbg(TRACE,"Terminate: now DB logoff ...");

    signal(SIGCHLD,SIG_IGN);

    logoff();

    dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);


    dbg(TRACE,"Terminate: now leaving ...");

    fclose(outp);

    sleep(ipSleep < 1 ? 1 : ipSleep);
    
    exit(0);
    
} /* end of Terminate */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
    dbg(TRACE,"HandleErr: <%d> : unknown ERROR",pipErr);

    return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    switch(pipErr) {
    case    QUE_E_FUNC  :   /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :   /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;
    case    QUE_E_SEND  :   /* Error using msgsnd */
        dbg(TRACE,"<%d> : msgsnd failed",pipErr);
        break;
    case    QUE_E_GET   :   /* Error using msgrcv */
        dbg(TRACE,"<%d> : msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX   :
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE  :
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES  :
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        break;
    case    QUE_E_RESP  :   /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;
    case    QUE_E_FULL  :
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG :   /* No message on queue */
        dbg(TRACE,"<%d> : no messages on queue",pipErr);
        break;
    case    QUE_E_INVORG    :   /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :   /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;
    default         :   /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
    } /* end switch */
         
    return;

} /* end of HandleQueErr */


/******************************************************************************/
/******************************************************************************/
static int GetRowLength(char *pcpHopo, char *pcpTana, char *pcpFieldList,
                        long *plpLen)
{
    int  ilRc = RC_SUCCESS;         /* Return code */
    int  ilNoOfItems = 0;
    int  ilLoop = 0;
    long llFldLen = 0;
    long llRowLen = 0;
    char clFina[8];
    
    ilNoOfItems = get_no_of_items(pcpFieldList);
    ilLoop = 1;
    do
    {
        ilRc = get_real_item(&clFina[0],pcpFieldList,ilLoop);
        if(ilRc > 0)
        {
            ilRc = GetFieldLength(pcpHopo,pcpTana,&clFina[0],&llFldLen);
            if(ilRc == RC_SUCCESS)
            {
                llRowLen++;
                llRowLen += llFldLen;
            }/* end of if */
        }/* end of if */
        ilLoop++;
    }while((ilRc == RC_SUCCESS) && (ilLoop <= ilNoOfItems));

    if(ilRc == RC_SUCCESS)
    {
        *plpLen = llRowLen;
    }/* end of if */

    return(ilRc);
    
} /* end of GetRowLength */


/******************************************************************************/
/******************************************************************************/
static int GetFieldLength(char *pcpHopo, char *pcpTana, char *pcpFina,
                            long *plpLen)
{
    int  ilRc = RC_SUCCESS;         /* Return code */
    int  ilCnt = 0;
    char clTaFi[32];
    char clFele[16];
    char clFldLst[16];

    ilCnt = 1;
    sprintf(&clTaFi[0],"%s,%s",pcpTana,pcpFina);
    sprintf(&clFldLst[0],"TANA,FINA"); /* wird von syslib zerst�rt */


    ilRc = syslibSearchSystabData(&cgTabEnd[0],&clFldLst[0],&clTaFi[0],"FELE",&clFele[0],&ilCnt,"");
    switch (ilRc)
    {
    case RC_SUCCESS :
        *plpLen = atoi(&clFele[0]);
        /* dbg(DEBUG,"GetFieldLength: <%s,%s,%s> <%d>",pcpHopo,pcpTana,pcpFina,*plpLen);*/
        break;

    case RC_NOT_FOUND :
        dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_NOT_FOUND <%s> <%s>",
                ilRc,&clTaFi[0],pcpFina);
        break;

    case RC_FAIL :
        dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_FAIL",ilRc);
        break;

    default :
        dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> unexpected",ilRc);
        break;
    }/* end of switch */

    return(ilRc);
    
} /* end of GetFieldLength */


/******************************************************************************/
/******************************************************************************/
static int GetCommand(char *pcpCommand, int *pipCmd)
{
    int ilRc = RC_SUCCESS;          /* Return code */
    int ilLoop = 0;
    char clCommand[48];
    
    memset(&clCommand[0],0x00,8);

    ilRc = get_real_item(&clCommand[0],pcpCommand,1);
    if(ilRc > 0)
    {
        ilRc = RC_FAIL;
    }/* end of if */

    while((ilRc != 0) && (prgCmdTxt[ilLoop] != NULL))
    {
        ilRc = strcmp(&clCommand[0],prgCmdTxt[ilLoop]);
        ilLoop++;
    }/* end of while */

    if(ilRc == 0)
    {
        ilLoop--;
        dbg(DEBUG,"GetCommand: <%s> <%d>",prgCmdTxt[ilLoop],rgCmdDef[ilLoop]);
        *pipCmd = rgCmdDef[ilLoop];
    }else{
        dbg(TRACE,"GetCommand: <%s> is not valid",&clCommand[0]);
        ilRc = RC_FAIL;
    }/* end of if */

    return(ilRc);
    
} /* end of GetCommand */


static int ReloadDrrArrays(char *pcpStfuList)
{
    int ilRc = RC_SUCCESS;
    char clStfUrno[12];
    char clKey[22];
    char clPlfr[24],clPlto[24],clPlSCOD[24],clPeno[36];
    char clAcfr[24],clActo[24];
    long llAction = ARR_FIRST;
    long llDrrPlanAction = ARR_FIRST;
    long llJobAction = ARR_FIRST;
    long llPolJobAction = ARR_FIRST;
    long llDraAction = ARR_FIRST;
  long llRowCount = 0;
  long llRow = 0;
    char clSurn[24];
    char clDrrDataArea[1024];
    char clDrrPlanSelection[12224];
    char clDrrSelection[12224];
    short slFunction,slCursor;
    char *pclRowBuf;
    char *pclDrrRowBuf;
    char clTmpStr[24];
    struct _DrrData *prlDrr;
    char *pclDrr;
    int ilBytes;
    int ilOffset;
    int ilEnd;
    char clLineBuffer[1024];
    char clStart[24],clEnd[24];
    char clUtcStart[24];
    char clUtcStartDay[24];

    GetServerTimeStamp("LOC", 1,0,clStart);
    strcpy(&clStart[8],"000000");
    strcpy(clEnd,clStart);
    ilOffset = igDailyOffset;
    ilEnd = igDailyEnd;
    
    if (ilOffset != 0)
    {
        AddSecondsToCEDATime(clStart,(time_t)(ilOffset*86400),1);
    }
    strcpy(clUtcStart,clStart);
    AddSecondsToCEDATime(clUtcStart,(time_t)-86400,1);
    AddSecondsToCEDATime(clEnd,(time_t)(ilEnd*86400),1);

    strcpy(cgStartDay,clStart);
    strcpy(clUtcStartDay,clUtcStart);
    strcpy(cgEndDay,clEnd);
    cgStartDay[8] = '\0';
    cgEndDay[8] = '\0';
    clUtcStartDay[8] = '\0';

/***** fill DRR planned array (level = 1) **/
    sprintf(clDrrPlanSelection,"WHERE SDAY >= '%s' AND SDAY <= '%s' AND ROSL='1' AND DRRN = '1' "
            "  AND STFU in (%s)",cgStartDay,cgEndDay,pcpStfuList);
    dbg(DEBUG,"ExportThreeDayRoster: Select all planned shifts <%s>",
        clDrrPlanSelection);
    CEDAArrayRefillHint(&rgDrrArrayP.rrArrayHandle,rgDrrArrayP.crArrayName,
            clDrrPlanSelection,NULL,ARR_NEXT,cgDrrReloadHintRosl);
    CEDAArrayGetRowCount(&rgDrrArrayP.rrArrayHandle,
                rgDrrArrayP.crArrayName,&llRowCount);
    dbg(TRACE,"%d planned shifts reloaded",llRowCount);

    sprintf(clDrrSelection,"WHERE SDAY >= '%s' AND SDAY <= '%s' AND DRRN = '1' "
            "  AND STFU IN (%s)",cgStartDay,cgEndDay,pcpStfuList);
    CEDAArrayRefillHint(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,
            clDrrSelection,NULL,ARR_NEXT,cgDrrReloadHintStfu);
    CEDAArrayGetRowCount(&rgDrrArray.rrArrayHandle,
                rgDrrArray.crArrayName,&llRowCount);
    dbg(TRACE,"%d shifts reloaded",llRowCount);
  return ilRc;
}


static int ReloadArrays(BOOL bpForExport)
{
    int ilRc = RC_SUCCESS;
    char clSelection[262];
    char clStfUrno[12];
    char clKey[22];
    char clPlfr[24],clPlto[24],clPlSCOD[24],clPeno[36];
    char clAcfr[24],clActo[24];
    long llAction = ARR_FIRST;
    long llDrrPlanAction = ARR_FIRST;
    long llJobAction = ARR_FIRST;
    long llPolJobAction = ARR_FIRST;
    long llDraAction = ARR_FIRST;
  long llRowCount = 0;
  long llRow = 0;
    char clSurn[24];
    char clDrrDataArea[1024];
    char clDrrPlanSelection[224];
    char clDrrSelection[224];
    char clDraSelection[224];
    char clJobSelection[224];
    char clJavSelection[224];
    char clStfSelection[224];
    short slFunction,slCursor;
    char *pclRowBuf;
    char *pclDrrRowBuf;
    char clTmpStr[24];
    struct _DrrData *prlDrr;
    char *pclDrr;
    int ilBytes;
    int ilOffset;
    int ilEnd;
    char clLineBuffer[1024];
    char clStart[24],clEnd[24];
    char clUtcStart[24];
    char clUtcStartDay[24];

    GetServerTimeStamp("LOC", 1,0,clStart);
    strcpy(&clStart[8],"000000");
    strcpy(clEnd,clStart);
    if (bpForExport == TRUE)
    {
         ilOffset = igExpOffset;
         ilEnd = igExpEnd;
    }
    else
    {
        ilOffset = igDailyOffset;
         ilEnd = igDailyEnd;
    }
    if (ilOffset != 0)
    {
        AddSecondsToCEDATime(clStart,(time_t)(ilOffset*86400),1);
    }
    strcpy(clUtcStart,clStart);
    AddSecondsToCEDATime(clUtcStart,(time_t)-86400,1);
    AddSecondsToCEDATime(clEnd,(time_t)(ilEnd*86400),1);

    strcpy(cgStartDay,clStart);
    strcpy(clUtcStartDay,clUtcStart);
    strcpy(cgEndDay,clEnd);
    cgStartDay[8] = '\0';
    cgEndDay[8] = '\0';
    clUtcStartDay[8] = '\0';

    /** select all breaks **/
    sprintf(clAcfr,"%s000000",clUtcStartDay);
    sprintf(clActo,"%s235959",cgEndDay);
    LocalTimeToUtcFixTZ(clAcfr);
    sprintf(clJobSelection,"WHERE (ACTO >= '%s' AND ACFR <= '%s') "
            " AND UJTY='%s' AND HOPO = '%s' ",
        clAcfr,clActo,cgBrkUjty,cgHopo);
    dbg(DEBUG,"ExportDailyRoster: Select all breaks <%s>",
        clJobSelection);
    CEDAArrayRefillHint(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,
            clJobSelection,NULL,llJobAction,cgJobReloadHint);
    /*CEDAArrayRefill(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,
            clJobSelection,NULL,llJobAction);*/
    CEDAArraySetFilter(&rgJobArray.rrArrayHandle,
                    rgJobArray.crArrayName,"UJTY == 2011");
    CEDAArrayGetRowCount(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,&llRowCount);
    dbg(DEBUG,"%d jobs loaded",llRowCount);

    /** select all pool jobs **/
    sprintf(clJobSelection,"WHERE (ACTO >= '%s' AND ACFR <= '%s') "
            " AND UJTY='%s' AND HOPO = '%s' ",
        clAcfr,clActo,cgPolUjty,cgHopo);
    dbg(DEBUG,"ExportSingleRoster: Select all pool jobs <%s>",
        clJobSelection);
    CEDAArrayRefillHint(&rgPolJobArray.rrArrayHandle,rgPolJobArray.crArrayName,
            clJobSelection,NULL,llPolJobAction,cgPoolJobHint);
    CEDAArrayGetRowCount(&rgPolJobArray.rrArrayHandle,rgPolJobArray.crArrayName,&llRowCount);
    dbg(DEBUG,"%d pool jobs loaded",llRowCount);

    sprintf(clDraSelection,"WHERE SDAY >= '%s'  AND SDAY <= '%s' AND HOPO = '%s'",
            cgStartDay,cgEndDay,cgHopo);
    dbg(DEBUG,"ExportDailyRoster: Select all absences <%s>",
        clDraSelection);
    CEDAArrayRefill(&rgDraArray.rrArrayHandle,rgDraArray.crArrayName,
            clDraSelection,NULL,llDraAction);
    CEDAArrayGetRowCount(&rgDraArray.rrArrayHandle,rgDraArray.crArrayName,&llRowCount);
    dbg(DEBUG,"%d absences loaded",llRowCount);


/***** fill DRR planned array (level = 1) **/
    sprintf(clDrrPlanSelection,"WHERE SDAY >= '%s' AND SDAY <= '%s' AND ROSL='1' AND DRRN = '1' "
            "  AND HOPO='%s'",cgStartDay,cgEndDay,cgHopo);
    dbg(DEBUG,"ExportThreeDayRoster: Select all planned shifts <%s>",
        clDrrPlanSelection);
    CEDAArrayRefillHint(&rgDrrArrayP.rrArrayHandle,rgDrrArrayP.crArrayName,
            clDrrPlanSelection,NULL,ARR_FIRST,cgDrrReloadHintRosl);
    CEDAArrayGetRowCount(&rgDrrArrayP.rrArrayHandle,
                rgDrrArrayP.crArrayName,&llRowCount);
    dbg(TRACE,"%d planned shifts loaded",llRowCount);

    sprintf(clDrrSelection,"WHERE SDAY >= '%s' AND SDAY <= '%s' AND DRRN = '1' "
            "  AND HOPO='%s'",cgStartDay,cgEndDay,cgHopo);
    CEDAArrayRefill(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,
            clDrrSelection,NULL,ARR_FIRST);
    CEDAArrayGetRowCount(&rgDrrArray.rrArrayHandle,
                rgDrrArray.crArrayName,&llRowCount);
    dbg(TRACE,"%d shifts loaded",llRowCount);

    sprintf(clJavSelection,"WHERE (ACTO >= '%s' AND ACFR <= '%s') "
            " AND UJTY='%s'",
        clAcfr,clActo,cgFlightUjty,cgHopo);
    CEDAArrayRefillHint(&rgJavArray.rrArrayHandle,rgJavArray.crArrayName,
            clJavSelection,NULL,ARR_FIRST,cgJavHint);
    sprintf(clJavSelection,"(ACTO >= '%s' & ACFR <= '%s') & UJTY='%s'",
        clAcfr,clActo,cgFlightUjty);
    CEDAArraySetFilter(&rgJavArray.rrArrayHandle,
                    rgJavArray.crArrayName,clJavSelection);
    CEDAArrayGetRowCount(&rgJavArray.rrArrayHandle,
                rgJavArray.crArrayName,&llRowCount);
    dbg(TRACE,"%d jobs loaded",llRowCount);
  return ilRc;
}

static int BreakRosterExport(int ipCommand)
{
    int ilRc = RC_SUCCESS;
    long llSequence;
    char clLineBuffer[4096];
    int ilBytes;
    char clBreakStart[16];
    char clBreakEnd[16];
    char clRosterStart[16];
    char clRosterEnd[16];
    char clNow[16];
    char clTimeStamp[24];
    int ilRecordCount = 0;
    int ilLc;
  long llRow;
    char *pclTmp;
    int ilTmpItemNo;
    char clUrno[24];
    char clStfu[24];
    char clSday[24];
    char clPeno[54];
    char clRosl[8];
    int ilBreakIndex;
    char *pclRowBuf = NULL;

    switch(ipCommand)
    {
    case CMD_BR1: ilBreakIndex = 0;
        break;
    case CMD_BR2: ilBreakIndex = 1;
        break;
    case CMD_BR3: ilBreakIndex = 2;
        break;
    case CMD_BR4: ilBreakIndex = 3;
        break;
    default:
        dbg(TRACE,"invalid command %d",ipCommand);
        return RC_FAIL;
    }

    dbg(DEBUG,"BreakRosterExport for Break %d",ilBreakIndex+1);
    GetServerTimeStamp("LOC", 1,0,clNow);
    clNow[8] = '\0';
    sprintf(clBreakStart,"%s%s",clNow,rgBreaks[ilBreakIndex].Start);
    sprintf(clBreakEnd,"%s%s",clNow,rgBreaks[ilBreakIndex].End);
    dbg(DEBUG,"BreakStart: <%s>, BreakEnd <%s>",clBreakStart,clBreakEnd);
    llRow = ARR_FIRST;
    while(CEDAArrayGetRowPointer(&rgDrrArray.rrArrayHandle,
        rgDrrArray.crArrayName,llRow,(void *)&pclRowBuf) == RC_SUCCESS)       
    {
            int ilFieldNo;
        
            llRow = ARR_NEXT;

            strcpy(clRosl,FIELDVAL(rgDrrArray,pclRowBuf,igDrrROSL));
            if(strncmp(clRosl,"3",1) == 0)
            {
            strcpy(clRosterStart,FIELDVAL(rgDrrArray,pclRowBuf,igDrrAVFR));
            strcpy(clRosterEnd,FIELDVAL(rgDrrArray,pclRowBuf,igDrrAVTO));
            if (OVERLAP(clBreakStart,clBreakEnd,clRosterStart,clRosterEnd))
            {
                dbg(DEBUG,"Shift from %s to %s overlaps break from %s to %s",
                    clRosterStart,clRosterEnd,clBreakStart,clBreakEnd);
                strcpy(clStfu,FIELDVAL(rgDrrArray,pclRowBuf,igDrrSTFU));
                strcpy(clSday,FIELDVAL(rgDrrArray,pclRowBuf,igDrrSDAY));
                if (CheckRegi(clStfu,clSday) == RC_SUCCESS)
                {
                    llRow = ARR_FIRST;
                    ilRc = CEDAArrayFindRowPointer(&rgStfArray.rrArrayHandle,
                        rgStfArray.crArrayName,&rgStfArray.rrIdx01Handle,
                            rgStfArray.crIdx01Name,clStfu,&llRow,
                                (void **)&rgStfArray.pcrArrayRowBuf);
                    if (ilRc == RC_SUCCESS)
                    {
                        char clTohr[4] = "";
                        strcpy(clTohr,FIELDVAL(rgStfArray,rgStfArray.pcrArrayRowBuf,igStfTOHR));
                        if(*clTohr == *cgTohr)
                        {
                            char clDodm[24];

                            strcpy(clPeno,FIELDVAL(rgStfArray,rgStfArray.pcrArrayRowBuf,igStfPENO));
                            strcpy(clDodm,FIELDVAL(rgStfArray,rgStfArray.pcrArrayRowBuf,igStfDODM));
                            clDodm[8] = '\0';
                            if ((strcmp(clDodm,clSday) > 0) || *clDodm == ' ')
                            {
                                dbg(TRACE,"PENO: <%s> CreateOneShiftLine follows",clPeno);
                                memset(clLineBuffer,0,sizeof(clLineBuffer));
                                ilBytes = CreateOneShiftLine(clLineBuffer,pclRowBuf,clPeno,FALSE,FALSE);
                                if (ilBytes > 0)
                                {
                                    dbg(TRACE,"CreateOneShiftLine finished RC=%d\n<%s>",ilBytes,clLineBuffer);
                                    SendCedaEvent(igTaa,0,cgDestName,cgAlertName,cgTwStart, cgTwEnd,
                                    "TAS","DRRTAB"," "," ",clLineBuffer,"",3,0);          
                                }
                            }
                            else
                            {
                                dbg(DEBUG,"Not exported because emp. is already "
                                    "resigned <%s> SDAY <%s>",clDodm,clSday);
                            }
                        }
                        else
                        {
                            dbg(TRACE,"TOHR = <%s>",clTohr);
                        }
                    }
                    else
                    {
                        dbg(TRACE,"Peno not found for STFU <%s>",clStfu);
                    }
                }
            }
            else
            {
                dbg(DEBUG,"Shift from %s to %s don't overlap break from %s to %s",
                    clRosterStart,clRosterEnd,clBreakStart,clBreakEnd);
            }
            }
            else
            {
                dbg(DEBUG,"Shift from %s to %s is level %s don't send",
                    clRosterStart,clRosterEnd,clRosl);
            }
            llRow = ARR_NEXT;
    }
  return ilRc;
}



static int SingleRosterExport(char *pcpTable,char *pcpSelection,
            char *pcpFields,char *pcpData)
{
    int ilRc = RC_SUCCESS;
    char clFileName[34];
    char clFilePath[234];
    char clExportPath[234];
    long llSequence;
    FILE *pflFile = NULL;
    char clLineBuffer[4096];
    int ilBytes;
    char clTimeStamp[24];
    int ilRecordCount = 0;
    int ilLc;
  long llRow;
    char *pclTmp;
    int ilTmpItemNo;
    char clUrno[24];
    char clStfu[24];
    char clSday[24];
    char clPeno[54];

    pclTmp = strchr(pcpData,'\n');
    if (pclTmp != NULL)
    {
        *pclTmp = '\0';
    }
    if (strcmp(pcpTable,"DRRTAB") == 0)
    {
            ilTmpItemNo = get_item_no(pcpFields,"URNO",5) + 1;
            get_real_item(clUrno,pcpData,ilTmpItemNo);

            llRow = ARR_FIRST;
            ilRc = CEDAArrayFindRowPointer(&(rgDrrArray.rrArrayHandle),
                                                                            &(rgDrrArray.crArrayName[0]),
                                                                            &(rgDrrArray.rrIdx02Handle),
                                                                            &(rgDrrArray.crIdx02Name[0]),
                                                                            clUrno,&llRow,
                                                                            (void **)&rgDrrArray.pcrArrayRowBuf);

        /***
        if (debug_level == DEBUG)
        {
                DumpRecord(&rgDrrArray,NULL,rgDrrArray.pcrArrayRowBuf);
        }
        ***/
        if(ilRc == RC_SUCCESS)
        {
            strcpy(clStfu,FIELDVAL(rgDrrArray,rgDrrArray.pcrArrayRowBuf,igDrrSTFU));
            strcpy(clSday,FIELDVAL(rgDrrArray,rgDrrArray.pcrArrayRowBuf,igDrrSDAY));
            if (CheckRegi(clStfu,clSday) == RC_SUCCESS)
            {
                llRow = ARR_FIRST;
                ilRc = CEDAArrayFindRowPointer(&rgStfArray.rrArrayHandle,
                    rgStfArray.crArrayName,&rgStfArray.rrIdx01Handle,
                        rgStfArray.crIdx01Name,clStfu,&llRow,
                                (void **)&rgStfArray.pcrArrayRowBuf);
                if (ilRc == RC_SUCCESS)
                {
                    char clTohr[4] = "";
                    strcpy(clTohr,FIELDVAL(rgStfArray,rgStfArray.pcrArrayRowBuf,igStfTOHR));
                    if(*clTohr == *cgTohr)
                    {
                        char clDodm[24];

                        strcpy(clPeno,FIELDVAL(rgStfArray,rgStfArray.pcrArrayRowBuf,igStfPENO));
                        strcpy(clDodm,FIELDVAL(rgStfArray,rgStfArray.pcrArrayRowBuf,igStfDODM));
                        clDodm[8] = '\0';
                        if ((strcmp(clDodm,clSday) > 0) || *clDodm == ' ')
                        {
                            dbg(TRACE,"PENO: <%s> CreateOneShiftLine follows",clPeno);
                            memset(clLineBuffer,0,sizeof(clLineBuffer));
                            ilBytes = CreateOneShiftLine(clLineBuffer,rgDrrArray.pcrArrayRowBuf,clPeno,FALSE,FALSE);
                            if (ilBytes > 0)
                            {
                                dbg(TRACE,"CreateOneShiftLine finished RC=%d\n<%s>",ilBytes,clLineBuffer);
                                SendCedaEvent(igTaa,0,cgDestName,cgAlertName,cgTwStart, cgTwEnd,
                                    "TAS","DRRTAB"," "," ",clLineBuffer,"",3,0);          
                            }
                        }
                        else
                        {
                            dbg(DEBUG,"Not exported because emp. is already "
                                "resigned <%s> SDAY <%s>",clDodm,clSday);
                        }
                    }
                    else
                    {
                        dbg(TRACE,"TOHR = <%s>",clTohr);
                    }
                }
                else
                {
                    dbg(TRACE,"Peno not found for STFU <%s>",clStfu);
                }
            }
        }
        else
        {
            dbg(TRACE,"DRR record with URNO <%s> not found",clUrno);
        }
    }
  return ilRc;
}

static int ThreeDayRosterExport()
{
    int ilRc = RC_SUCCESS;
    char clFileName[34];
    char clFilePath[234];
    char clExportPath[234];
    long llSequence;
    FILE *pflFile = NULL;
    char clTimeStamp[24];
    int ilRecordCount = 0;
    int ilLc;


    MakeFileName(cg3DrFileId,clFileName);
    sprintf(clFilePath,"%s%s%s",cgFileBase,cgTmpPath,clFileName);
    dbg(DEBUG,"%05d:  <%s> <%s> <%s> ",__LINE__,cgFileBase,cgTmpPath,clFileName);

    dbg(TRACE,"Export File for three day roster export <%s> will be created",clFilePath);
    errno = 0;
    pflFile = fopen(clFilePath,"wt");
    errno = 0;
    if (pflFile == NULL)
    {
        dbg(TRACE,"ThreeDayRoster File <%s> not created, reason: %d %s",
            clFilePath,errno,strerror(errno));
        ilRc = RC_FAIL;
    }
    else
    {
        int ilExpEnd;

        errno = 0;
        dbg(DEBUG,"Three day roster export file created");
        WriteHeader(pflFile,cg3DrFileId,clFileName,0);

        ilExpEnd = igExpOffset + igExpEnd;
        for(ilLc = igExpOffset; ilLc < ilExpEnd; ilLc++)
        {
            GetServerTimeStamp("LOC", 1,0,clTimeStamp);
            AddSecondsToCEDATime(clTimeStamp,ilLc*86400,1);
            clTimeStamp[8] = '\0';
            ilRecordCount += ExportThreeDayRoster(pflFile,clTimeStamp);
        }
        dbg(TRACE,"ExportThreeDayRoster: %d lines exported",ilRecordCount);
        WriteTrailer(pflFile);
        rewind(pflFile);
        WriteHeader(pflFile,cg3DrFileId,clFileName,ilRecordCount);
        sprintf(clExportPath,"%s%s%s",cgFileBase,cgExportPath,clFileName);
        fclose(pflFile);
      dbg(TRACE,"ExportThreeDayRoster: export file will be renamed from <%s> to <%s>",
                clFilePath,clExportPath);
        rename(clFilePath,clExportPath);
    }
  return ilRc;
}

static int GetOffRestIndicator(char *pcpIndicator,char *pcpScod)
{
    int ilRc = RC_SUCCESS;
    char clKey[24];
    long llRow;
    char clIndicator[4];

    strcpy(clKey,pcpScod);
    llRow = ARR_FIRST;
    ilRc = CEDAArrayFindRowPointer(&rgOdaArray.rrArrayHandle,
            rgOdaArray.crArrayName,&rgOdaArray.rrIdx02Handle,
                    rgOdaArray.crIdx02Name,clKey,&llRow,
                            (void **)&rgOdaArray.pcrArrayRowBuf);
    if (ilRc == RC_SUCCESS)
    {
        strcpy(pcpIndicator,
                FIELDVAL(rgOdaArray,rgOdaArray.pcrArrayRowBuf,igOdaSDAS));
        if (*pcpIndicator == ' ')
        {
                strcpy(pcpIndicator,"0");
        }
        /**************
        strcpy(clIndicator,
                FIELDVAL(rgOdaArray,rgOdaArray.pcrArrayRowBuf,igOdaFREE));
        if (*clIndicator == 'x')
        {
            strcpy(pcpIndicator,"1");
        }
        else 
        {   
            strcpy(clIndicator,
                FIELDVAL(rgOdaArray,rgOdaArray.pcrArrayRowBuf,igOdaTYPE));
            if (*clIndicator == 'S')
            {
                strcpy(pcpIndicator,"2");
            }
        }
        **********/
        dbg(DEBUG,"Off/Rest indicator got from ODATAB <%s> for SCOD <%s>",
                        pcpIndicator,clKey);
    }
    else
    {
        strcpy(pcpIndicator,"0");
        dbg(DEBUG,"SCOD <%s> not found in ODATAB, Off/Rest indicator set to '0'",
                    clKey);
    }

    return ilRc;
}

#if 0
{
    int ilRc = RC_SUCCESS;
    char clKey[24];
    long llRow;

    strcpy(clKey,pcpScod);
    llRow = ARR_FIRST;
    ilRc = CEDAArrayFindRowPointer(&rgOdaArray.rrArrayHandle,
            rgOdaArray.crArrayName,&rgOdaArray.rrIdx02Handle,
                    rgOdaArray.crIdx02Name,clKey,&llRow,
                            (void **)&rgOdaArray.pcrArrayRowBuf);
    if (ilRc == RC_SUCCESS)
    {
        strcpy(pcpIndicator,
                FIELDVAL(rgOdaArray,rgOdaArray.pcrArrayRowBuf,igOdaFREE));
        if (*pcpIndicator == 'x' || *pcpIndicator == 'X')
        {
            strcpy(pcpIndicator,"1");
        }
        else
        {
                strcpy(pcpIndicator,FIELDVAL(rgOdaArray,rgOdaArray.pcrArrayRowBuf
                        ,igOdaTYPE));
                if (*pcpIndicator == 'S')
                {
                    strcpy(pcpIndicator,"2");
                }
                else
                {
                    strcpy(pcpIndicator,"0");
                }
        }
        dbg(DEBUG,"Off/Rest indicator got from ODATAB <%s> for SCOD <%s>",
                        pcpIndicator,clKey);
    }
    else
    {
        strcpy(pcpIndicator,"0");
        dbg(DEBUG,"SCOD <%s> not found in ODATAB, Off/Rest indicator set to '0'",
                    clKey);
    }

    return ilRc;

}
#endif

static int GetTeamCode(char *pcpDrr,char *pcpTeamCode,char *pcpTeamDescription)
{
    int ilRc = RC_SUCCESS;
    long llRow;
    char clTmpStr[24];
    char clPolJobUrno[24];
    char clWorkgroup[24];
    char clWorkgroupName[44];
    char clSelection[240];
    char clKey[24];
    char clStfu[24];
    char clSday[24];
    char clVafr[24];
    char clVato[24];
    char *pclRowBuf;

    *pcpTeamCode = '\0';
    *pcpTeamDescription = '\0';
    *clWorkgroup = '\0';

    strcpy(clKey,FIELDVAL(rgDrrArray,pcpDrr,igDrrURNO));
    strcpy(clStfu,FIELDVAL(rgDrrArray,pcpDrr,igDrrSTFU));
    strcpy(clSday,FIELDVAL(rgDrrArray,pcpDrr,igDrrSDAY));
    llRow = ARR_FIRST;
    ilRc = CEDAArrayFindRowPointer(&rgPolJobArray.rrArrayHandle,
            rgPolJobArray.crArrayName,&rgPolJobArray.rrIdx01Handle,
                    rgPolJobArray.crIdx01Name,clKey,&llRow,
                            (void **)&pclRowBuf);

    if (ilRc == RC_SUCCESS)
    {
    /** search workgroup by DLGTAB (UJOB == JOBTAB.URNO) **/

        strcpy(clPolJobUrno,FIELDVAL(rgPolJobArray,pclRowBuf,igJobURNO)); 
        sprintf(clSelection,"WHERE UJOB = '%s'",clPolJobUrno); 
        dbg(DEBUG,"GetTeamCode: search team by DLGTAB pool job urno <%s>",clPolJobUrno);
        ilRc = DoSingleSelect("DLGTAB",clSelection,
                    "WGPC",clWorkgroup);
        if (ilRc == RC_SUCCESS)
        {
            dbg(DEBUG,"GetTeamCode: found team by DLGTAB pool job urno <%s> team <%s>",
                    clPolJobUrno,clWorkgroup);
        }
        else
        {
            dbg(DEBUG,"GetTeamCode: team not found by DLGTAB pool job urno <%s> ",clPolJobUrno);
        }
    }
    else
    {
        dbg(DEBUG,"GetTeamCode: search team by DLGTAB (pool job urno) found no pool job",clPolJobUrno);
    }
    if (*clWorkgroup == '\0')
    {
    /** search workgroup by DRGTAB (STFU + SDAY) **/

        sprintf(clSelection,"WHERE STFU = '%s' AND SDAY = '%s' AND HOPO ='%s'",
            clStfu,clSday,cgHopo);
        dbg(DEBUG,"GetTeamCode: search team by DRGTAB stfu and sday <%s>",clSelection);
        ilRc = DoSingleSelect("DRGTAB",clSelection,
                    "WGPC",clWorkgroup);
        if (ilRc == RC_SUCCESS)
        {
            dbg(DEBUG,"GetTeamCode: search team <%s> by DRGTAB stfu and sday <%s>",
                    clWorkgroup,clSelection);
        }
    }
    if (*clWorkgroup == '\0')
    {
    /** search workgroup by SWGTAB (SURN + SDAY in VAFR/VATO) **/
        char clVpfr[24];
        char clVpto[24];

            sprintf(clVpfr,"%s000000",clSday);
            sprintf(clVpto,"%s235959",clSday);


        sprintf(clSelection,"WHERE SURN = '%s' AND VPFR <= '%s' AND "
        " (VPTO >='%s' OR VPTO = ' ')",
            clStfu,clSday,clSday,cgHopo);
        dbg(DEBUG,"GetTeamCode: search team by SWGTAB stfu and sday in VPFR/FPTO<%s>",
                clSelection);
        ilRc = DoSingleSelect("SWGTAB",clSelection,
                    "CODE",clWorkgroup);
        if (ilRc == RC_SUCCESS)
        {
            dbg(DEBUG,"GetTeamCode: search team <%s> by SWGTAB stfu and sday in VPFR/VPTO<%s>",
                    clWorkgroup,clSelection);
        }
    }

    if (*clWorkgroup != '\0')
    {
            sprintf(clSelection,"WHERE WGPC = '%s' AND HOPO = '%s'",clWorkgroup,cgHopo);
            ilRc = DoSingleSelect("WGPTAB",clSelection,
                    "WGPN",clWorkgroupName);
        if (ilRc == RC_SUCCESS)
        {
            dbg(DEBUG,"Workgroup found: <%s> <%s>",clWorkgroup,clWorkgroupName);
            strcpy(pcpTeamCode,clWorkgroup);
            strcpy(pcpTeamDescription,clWorkgroupName);
        }
        else
        {
            dbg(DEBUG,"Workgroup <%s> not found",clWorkgroup);
        }
    }
    else
    {
        dbg(TRACE,"Workgroup for <%s/%s> not found in any table",clStfu,clSday);
    }
    return ilRc;
}

/*static int GetAbsence(char *pcpDrr,char *pcpSday,char *pcpNo,
    char *pcpSdac,char *pcpRema,char *pcpAbfr,char *pcpAbto)*/
static int GetAbsence(char *pcpDrr,char *pcpSday,char *pcpDrrn,
    struct _AbsenceStruct *prpAbsences)
{
    int ilRc = RC_NOTFOUND;
    char clKey[24];
    long llRow = ARR_FIRST;
    char clDrrn[12];
    int ilAbsenceNo = 0;
    int ilAbsencesFound = 0;
    int ilLc;

    memset(rgAbsences,0,sizeof(rgAbsences));

    for (ilLc = 0; ilLc < 2; ilLc++)
    {
        strcpy(rgAbsences[ilLc].Abfr,"000000000000");
        strcpy(rgAbsences[ilLc].Abto,"000000000000");
    }

    sprintf(clKey,"%s",FIELDVAL(rgDrrArray,pcpDrr,igDrrSTFU));
    dbg(DEBUG,"GetAbsences: STFU <%s>",clKey);
    llRow = ARR_FIRST;
    while(CEDAArrayFindRowPointer(&rgDraArray.rrArrayHandle,
            rgDraArray.crArrayName,&rgDraArray.rrIdx01Handle,
                    rgDraArray.crIdx01Name,clKey,&llRow,
                            (void **)&rgDraArray.pcrArrayRowBuf) == RC_SUCCESS)
    {
        llRow = ARR_NEXT;
            
        if(strncmp(pcpSday,
                FIELDVAL(rgDraArray,rgDraArray.pcrArrayRowBuf,igDraSDAY),8) != 0)
        {
            continue;
        }
        ilAbsencesFound++;
        strcpy(clDrrn,FIELDVAL(rgDraArray,rgDraArray.pcrArrayRowBuf,igDraDRRN));
        if (*pcpDrrn != *clDrrn)
        {
            dbg(DEBUG,"GetAbsence: wrong DRRN <%s> for Key <%s>",clDrrn,clKey);
            continue;
        }

        ilRc = RC_SUCCESS;
/* 12 Absence Code */
        strcpy(prpAbsences[ilAbsenceNo].Sdac,
                    FIELDVAL(rgDraArray,rgDraArray.pcrArrayRowBuf,igDraSDAC));
/* 13 Absence Description 1 */
        /**********
        strcpy(prpAbsences[ilAbsenceNo].Rema,
                    FIELDVAL(rgDraArray,rgDraArray.pcrArrayRowBuf,igDraREMA));
                    ***********/
        GetAbsenceRemark(prpAbsences[ilAbsenceNo].Sdac,prpAbsences[ilAbsenceNo].Rema);
/* 14 Start Time of absence/attendance 1 */
        strcpy(prpAbsences[ilAbsenceNo].Abfr,
                    FIELDVAL(rgDraArray,rgDraArray.pcrArrayRowBuf,igDraABFR));
/* 15 End Time of absence/attendance 1 */
        strcpy(prpAbsences[ilAbsenceNo].Abto,
                    FIELDVAL(rgDraArray,rgDraArray.pcrArrayRowBuf,igDraABTO));
        dbg(TRACE,"absence found: <%s> <%s> from %s to %s",
                clKey,prpAbsences[ilAbsenceNo].Sdac,
                    prpAbsences[ilAbsenceNo].Abfr,prpAbsences[ilAbsenceNo].Abto);
        ilAbsenceNo++;
        if (ilAbsenceNo > 1)
        {
            break;
        }
    }
    dbg(DEBUG,"%d Absences for <%s> found",ilAbsencesFound,pcpSday);
    return ilRc;
}

static int GetReasonForBreaks(int ipReasonLen,char *pcpReason,char *pcpStfu,char *pcpAVFR, char *pcpAVTO)
{
    int ilRc = RC_SUCCESS;
    int ilLc;
    int ilStrFull = FALSE;
    int ilLen = 0;
    long llAction = ARR_FIRST;
    char clStart[24],clEnd[24];
    char clStartTime[12],clEndTime[12];
    char clTmpReason[64];

    /* MEI */
    /*pclLineBuffer = &pcpReason[strlen(pcpReason)];*/  
    memset( pcpReason, 0, ipReasonLen );

    while(CEDAArrayFindRowPointer(&rgJavArray.rrArrayHandle,rgJavArray.crArrayName,
          &rgJavArray.rrIdx01Handle,rgJavArray.crIdx01Name,pcpStfu,
          &llAction,(void **)&rgJavArray.pcrArrayRowBuf) == RC_SUCCESS && ilStrFull == FALSE)
    {
        llAction = ARR_NEXT;
        strcpy(clStart,FIELDVAL(rgJavArray,rgJavArray.pcrArrayRowBuf,igJavACFR));
        strcpy(clEnd,FIELDVAL(rgJavArray,rgJavArray.pcrArrayRowBuf,igJavACTO));
        UtcToLocalTimeFixTZ(clStart);
        UtcToLocalTimeFixTZ(clEnd);
        if (OVERLAP(pcpAVFR,pcpAVTO,clStart,clEnd))
        {
            dbg(TRACE,"Job overlapped shift: %s-%s Job: %s-%s",pcpAVFR,pcpAVTO,clStart,clEnd);
            strcpy(clStartTime,&clStart[8]);
            strcpy(clEndTime,&clEnd[8]);
            clStartTime[4] = '\0';
            clEndTime[4] = '\0';

            for( ilLc = 0; ilLc < MAXBREAKS && rgBreaks[ilLc].used == TRUE; ilLc++)
            {

                if (OVERLAP(rgBreaks[ilLc].Start,rgBreaks[ilLc].End,clStartTime,clEndTime))
                {
                    char clFlno[24];
                    char clJavUrno[24];

                    dbg(DEBUG,"Overlapping Break found: JobStart <%s> JobEnd <%s>"
                              " Break start <%s> end <%s>", clStartTime,clEndTime,
                              rgBreaks[ilLc].Start,rgBreaks[ilLc].End);
                    strcpy(clFlno,FIELDVAL(rgJavArray,rgJavArray.pcrArrayRowBuf,igJavFLNO));
                    strcpy(clJavUrno,FIELDVAL(rgJavArray,rgJavArray.pcrArrayRowBuf,igJavURNO));
                    StringTrimRight(clFlno);
                    if(strlen(clFlno) < 6)
                    {
                        int ilFlnoRc;
                        char clSelection[124];

                        dbg(DEBUG,"FLNO for job not set");
                        sprintf(clSelection,"WHERE URNO = '%s'",clJavUrno);
                        ilFlnoRc = DoSingleSelect("JAVTAB",clSelection,"FLNO",clFlno);
                        dbg(DEBUG,"FLNO for Job <%s> selected %d <%s>",clJavUrno,ilFlnoRc,clFlno);
                    }

                    /* MEI */
                    sprintf( clTmpReason, "%s;", clFlno );
                    ilLen = strlen(pcpReason);
                    if( (strlen(clTmpReason)+ilLen) >= ipReasonLen )
                    {
                        ilStrFull = TRUE;
                        break;
                    }
                    strcat(pcpReason,clTmpReason);
                }
                else
                {
                    dbg(DEBUG,"Break does not Overlap: JobStart <%s> JobEnd <%s>"
                              " Break start <%s> end <%s>",
                              clStartTime,clEndTime,rgBreaks[ilLc].Start,
                              rgBreaks[ilLc].End);
                }
            } /* for */
        }
        else
        {
            dbg(TRACE,"Job don't overlap shift: %s-%s Job: %s-%s",pcpAVFR,pcpAVTO,clStart,clEnd);
        }
    } /* while */
    ilLen = strlen(pcpReason);
    if (ilLen <= 1)
        strcpy(pcpReason," ");
    else
        pcpReason[ilLen-1] = '\0';  /* Last char will be semi-colon */
    dbg(DEBUG,"GetReasonForBreaks: <%s>", pcpReason);    
    return ilRc;
}

static int GetReason(int ipReasonLen,char *pcpReason,char *pcpStfu,char *pcpFrom,char *pcpTo)
{
    int ilRc = RC_SUCCESS;
    int ilLc;
    int ilLen = 0;
    long llAction = ARR_FIRST;
    char clStart[24],clEnd[24];
    char clJobStart[24],clJobEnd[24];
    char clTmpReason[64];

    /* MEI */
    /*pclLineBuffer = &pcpReason[strlen(pcpReason)];*/  
    memset( pcpReason, 0, ipReasonLen );

    sprintf(clStart,"%s00",pcpFrom);
    sprintf(clEnd,"%s00",pcpTo);

    while(CEDAArrayFindRowPointer(&rgJavArray.rrArrayHandle,rgJavArray.crArrayName,
                                  &rgJavArray.rrIdx01Handle,rgJavArray.crIdx01Name,pcpStfu,
                                  &llAction,(void **)&rgJavArray.pcrArrayRowBuf) == RC_SUCCESS)
    {
        llAction = ARR_NEXT;

        strcpy(clJobStart,FIELDVAL(rgJavArray,rgJavArray.pcrArrayRowBuf,igJavACFR));
        strcpy(clJobEnd,FIELDVAL(rgJavArray,rgJavArray.pcrArrayRowBuf,igJavACTO));
        UtcToLocalTimeFixTZ(clJobStart);
        UtcToLocalTimeFixTZ(clJobEnd);
        
        if (OVERLAP(clStart,clEnd,clJobStart,clJobEnd))
        {
            char clFlno[24];
            char clJavUrno[24];
                
            dbg(TRACE,"Job overlapped, Timeframe: %s-%s Job: %s-%s",clStart,clEnd,clJobStart,clJobEnd);

            strcpy(clFlno,FIELDVAL(rgJavArray,rgJavArray.pcrArrayRowBuf,igJavFLNO));
            strcpy(clJavUrno,FIELDVAL(rgJavArray,rgJavArray.pcrArrayRowBuf,igJavURNO));
            StringTrimRight(clFlno);
            if(strlen(clFlno) < 6)
            {
                int ilFlnoRc;
                char clSelection[124];

                dbg(DEBUG,"FLNO for job not set");
                sprintf(clSelection,"WHERE URNO = '%s'",clJavUrno);
                ilFlnoRc = DoSingleSelect("JAVTAB",clSelection,"FLNO",clFlno);
                dbg(DEBUG,"FLNO for Job <%s> selected %d <%s>",clJavUrno,ilFlnoRc,clFlno);
            }

            /* MEI */
            sprintf( clTmpReason, "%-8.8s;", clFlno );
            ilLen = strlen(pcpReason);
            if( (strlen(clTmpReason)+ilLen) >= ipReasonLen )
                break;
            strcat(pcpReason,clTmpReason);
        }
        else
        {
            dbg(TRACE,"Job don't overlapp, Timeframe: %s-%s Job: %s-%s",clStart,clEnd,clJobStart,clJobEnd);
        }
    } /* while */

    ilLen = strlen(pcpReason);
    if (ilLen <= 1)
        strcpy(pcpReason," ");
    else
        pcpReason[ilLen-1] = '\0';  /* Last char will be semi-colon */
    dbg(DEBUG,"GetReason: <%s>", pcpReason);  

    return ilRc;
}

static int GetJobsForShift(char *pcpFlightJobs,char *pcpStfu,char *pcpFrom,char *pcpTo)
{
    int ilRc = RC_SUCCESS;
    long llAction = ARR_FIRST;
    char *pclLineBuffer;
    char clStart[24],clEnd[24];
    char clJobStart[24],clJobEnd[24];
    int ilLc;

    pclLineBuffer = &pcpFlightJobs[strlen(pcpFlightJobs)];
    pclLineBuffer = pcpFlightJobs;

    sprintf(clStart,"%s00",pcpFrom);
    sprintf(clEnd,"%s00",pcpTo);

    dbg(DEBUG,"GetJobsForShift");

    while(CEDAArrayFindRowPointer(&rgJavArray.rrArrayHandle,rgJavArray.crArrayName,
    &rgJavArray.rrIdx01Handle,rgJavArray.crIdx01Name,pcpStfu,
        &llAction,(void **)&rgJavArray.pcrArrayRowBuf) == RC_SUCCESS)
    {
            llAction = ARR_NEXT;

            strcpy(clJobStart,FIELDVAL(rgJavArray,rgJavArray.pcrArrayRowBuf,igJavACFR));
            strcpy(clJobEnd,FIELDVAL(rgJavArray,rgJavArray.pcrArrayRowBuf,igJavACTO));
        UtcToLocalTimeFixTZ(clJobStart);
        UtcToLocalTimeFixTZ(clJobEnd);
            if (OVERLAP(clStart,clEnd,clJobStart,clJobEnd))
            {
                char clFlno[24];
                char clJavUrno[24];
                char clAdid[24];
                char clLand[24];
                char clEtoa[24];
                char clOnbl[24];
                char clStoa[24];
                char clAirb[24];
                char clEtod[24];
                char clOfbl[24];
                char clStod[24];
                char clTmp[512];
                
                dbg(DEBUG,"GetFlightJobs:Job overlapped, Timeframe: %s-%s Job: %s-%s",
                        clStart,clEnd,clJobStart,clJobEnd);

                strcpy(clFlno,FIELDVAL(rgJavArray,rgJavArray.pcrArrayRowBuf,
                        igJavFLNO));
                strcpy(clJavUrno,FIELDVAL(rgJavArray,rgJavArray.pcrArrayRowBuf,
                        igJavURNO));
                StringTrimRight(clFlno);
                if(strlen(clFlno) < 6)
                {
                    int ilFlnoRc;
                    char clSelection[124];
                    char clFlightData[1024];

                    dbg(DEBUG,"FLNO for job not set");
                    sprintf(clSelection,"WHERE URNO = '%s'",clJavUrno);
                    ilFlnoRc = DoSingleSelect("JAVTAB",clSelection,"FLNO,ADID,LAND,ETOA,ONBL,STOA,AIRB,ETOD,OFBL,STOD",clFlightData);
                    dbg(DEBUG,"FlightData for Job <%s> selected %d <%s>",
                                clJavUrno,ilFlnoRc,clFlightData);
                ilRc=GetDataItem(clFlno,clFlightData,1,',',"","");
                ilRc=GetDataItem(clAdid,clFlightData,2,',',"","");
                ilRc=GetDataItem(clLand,clFlightData,3,',',"","");
                ilRc=GetDataItem(clEtoa,clFlightData,4,',',"","");
                ilRc=GetDataItem(clOnbl,clFlightData,5,',',"","");
                ilRc=GetDataItem(clStoa,clFlightData,6,',',"","");
                ilRc=GetDataItem(clAirb,clFlightData,7,',',"","");
                ilRc=GetDataItem(clEtod,clFlightData,8,',',"","");
                ilRc=GetDataItem(clOfbl,clFlightData,9,',',"","");
                ilRc=GetDataItem(clStod,clFlightData,10,',',"","");
                }
                else
                {
                    strcpy(clAdid,FIELDVAL(rgJavArray,rgJavArray.pcrArrayRowBuf,
                        igJavADID));
                    strcpy(clLand,FIELDVAL(rgJavArray,rgJavArray.pcrArrayRowBuf,
                        igJavLAND));
                    strcpy(clEtoa,FIELDVAL(rgJavArray,rgJavArray.pcrArrayRowBuf,
                        igJavETOA));
                    strcpy(clOnbl,FIELDVAL(rgJavArray,rgJavArray.pcrArrayRowBuf,
                        igJavONBL));
                    strcpy(clStoa,FIELDVAL(rgJavArray,rgJavArray.pcrArrayRowBuf,
                        igJavSTOA));
                    strcpy(clAirb,FIELDVAL(rgJavArray,rgJavArray.pcrArrayRowBuf,
                        igJavAIRB));
                    strcpy(clEtod,FIELDVAL(rgJavArray,rgJavArray.pcrArrayRowBuf,
                        igJavETOD));
                    strcpy(clOfbl,FIELDVAL(rgJavArray,rgJavArray.pcrArrayRowBuf,
                        igJavOFBL));
                    strcpy(clStod,FIELDVAL(rgJavArray,rgJavArray.pcrArrayRowBuf,
                        igJavSTOD));
                }

                if (*pcpFlightJobs != '\0')
                {
                    StringPut(pclLineBuffer,"#",1);  
                    pclLineBuffer ++;
                }
          UtcToLocalTimeFixTZ(clLand);
          UtcToLocalTimeFixTZ(clEtoa);
          UtcToLocalTimeFixTZ(clOnbl);
          UtcToLocalTimeFixTZ(clStoa);
          UtcToLocalTimeFixTZ(clAirb);
          UtcToLocalTimeFixTZ(clEtod);
          UtcToLocalTimeFixTZ(clOfbl);
          UtcToLocalTimeFixTZ(clStod);
                sprintf(clTmp,"%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
                    clFlno,clAdid,clLand,clEtoa,clOnbl,clStoa,clAirb,clEtod,clOfbl,clStod);
                StringPut(pclLineBuffer,clTmp,strlen(clTmp));

                pclLineBuffer += strlen(clTmp);
            }
            else
            {
                dbg(TRACE,"Job don't overlapp, Timeframe: %s-%s Job: %s-%s",clStart,clEnd,
                    clJobStart,clJobEnd);
            }
    }
    return ilRc;
}


static int CheckAndMoveDate(char *pcpStart,char *pcpEnd)
{
    int ilRc = RC_SUCCESS;

    if (strcmp(pcpStart,pcpEnd) > 0)
    {
            AddSecondsToCEDATime(pcpEnd,(time_t)86400,1);
            dbg(DEBUG,"%05d: start <%s> > end <%s> , moved end to next day",
                __LINE__,pcpStart,pcpEnd);
    }
    return ilRc;
}

static int GetBreak(char *pcpDrr,char *pcpBreakStart,char *pcpBreakEnd,BOOL bpIsLongTerm)
{
    int ilRc = RC_SUCCESS;
    char clTmpStr[24];
    char clKey[32];
    char clStfu[12];
    char clSday[12];
    char clRosl[6];
    char clDrrUrno[32];
    char *pclDrr, *pclRowBuf;
    long llRow;

    strcpy(clRosl,FIELDVAL(rgDrrArray,pcpDrr,igDrrROSL));
    strcpy(clStfu,FIELDVAL(rgDrrArray,pcpDrr,igDrrSTFU));
    strcpy(clSday,FIELDVAL(rgDrrArray,pcpDrr,igDrrSDAY));
    if(strncmp(clRosl,"4",1) == 0)
    {
        /* there is no break job connected to shift level 4, use
                level 3 instead */
            llRow = ARR_FIRST;
            sprintf(clKey,"%s,%s,3",clStfu,clSday);
            if (CEDAArrayFindRowPointer(&rgDrrArray.rrArrayHandle,
                rgDrrArray.crArrayName,&rgDrrArray.rrIdx03Handle,
                    rgDrrArray.crIdx03Name,clKey,&llRow,
                            (void **)&pclDrr) == RC_SUCCESS)
            {
            dbg(DEBUG,"Level 3 shift found for Key <%s>",clKey);
            strcpy(clDrrUrno,FIELDVAL(rgDrrArray,pclDrr,igDrrURNO));
            }
            else
            {
                char clSelection[124];

                dbg(TRACE,"no level 3 shift in memory found for <%s>",clKey);
                sprintf(clSelection,"WHERE STFU = '%s' AND SDAY = '%s' and ROSL = '3'",
                        clStfu,clSday);
                    if (DoSingleSelect("DRRTAB",clSelection,"URNO",clDrrUrno) 
                    != RC_SUCCESS)
                    {
                    dbg(TRACE,"no valid level 3 shift found for <%s>",clKey);
                    *clDrrUrno = '\0';
                    }
            }
    }
    else
    {
        strcpy(clDrrUrno,FIELDVAL(rgDrrArray,pcpDrr,igDrrURNO));
        dbg(DEBUG,"Shift is level %s using drr urno <%s> to search break job",
            FIELDVAL(rgDrrArray,pcpDrr,igDrrROSL),clDrrUrno);
    }

    sprintf(clKey,"%s",clDrrUrno);

    if (bpIsLongTerm == FALSE && *clKey != '\0')
    {
    llRow = ARR_FIRST;
    ilRc = CEDAArrayFindRowPointer(&rgJobArray.rrArrayHandle,
            rgJobArray.crArrayName,&rgJobArray.rrIdx01Handle,
                    rgJobArray.crIdx01Name,clKey,&llRow,
                            (void **)&pclRowBuf);
    }
    else
    {
        /* for LongTermRosterExport use planned break only */
        ilRc = RC_NOTFOUND;
        dbg(DEBUG, "LongTermRosterExport, use break from shift only");
    }

    if (ilRc == RC_SUCCESS)
    {
        char clTmpStr[24];

        dbg(DEBUG,"Break job found: <%s>",pclRowBuf);
        strcpy(pcpBreakStart,FIELDVAL(rgJobArray,pclRowBuf,igJobACFR));
    UtcToLocalTimeFixTZ(pcpBreakStart);
        pcpBreakStart[12] = '\0';
        strcpy(pcpBreakEnd,FIELDVAL(rgJobArray,pclRowBuf,igJobACTO));
    UtcToLocalTimeFixTZ(pcpBreakEnd);
        pcpBreakEnd[12] = '\0';
        dbg(DEBUG,"Break job found: <%s> <%s><%s>",
                pcpBreakStart,pcpBreakEnd,pclRowBuf);
    }
    else
    {
        if ((*clRosl != '3' && *clRosl != '4') || (bpIsLongTerm != FALSE))
        {

            strcpy(clTmpStr,FIELDVAL(rgDrrArray,pcpDrr,igDrrSBFR));
            if ((atoi(FIELDVAL(rgDrrArray,pcpDrr,igDrrSBLU)) == 0) || 
                    (*clTmpStr == '\0') ||(*clTmpStr == ' '))
            {
                strcpy(pcpBreakStart,"000000000000");
                strcpy(pcpBreakEnd,"000000000000");
            }
            else
            {
                strcpy(clTmpStr,FIELDVAL(rgDrrArray,pcpDrr,igDrrSBFR));
                strcpy(pcpBreakStart,clTmpStr);
                strcpy(clTmpStr,FIELDVAL(rgDrrArray,pcpDrr,igDrrSBLU));
                dbg(DEBUG,"SBLU=<%s> BreakStart=<%s>",
                    FIELDVAL(rgDrrArray,pcpDrr,igDrrSBLU),pcpBreakStart);
                strcpy(pcpBreakEnd,pcpBreakStart);
                AddSecondsToCEDATime(pcpBreakEnd,(time_t)atoi(clTmpStr)*60,1);
                dbg(DEBUG,"BreakEnd: <%s>",pcpBreakEnd);
            }
            dbg(DEBUG,"Break job not found and Rosl = <%s>, using DRR: <%s> <%s><%s>",
                    clRosl,pcpBreakStart,pcpBreakEnd,pcpDrr);
        }
    }
    return ilRc;
}       


static int GetFunctionDescription(char *pcpDesc,char *pcpFctc)
{
    int ilRc = RC_SUCCESS;
    long llRow = ARR_FIRST;

    ilRc = CEDAArrayFindRowPointer(&(rgPfcArray.rrArrayHandle),
                                                                            &(rgPfcArray.crArrayName[0]),
                                                                            &(rgPfcArray.rrIdx02Handle),
                                                                            &(rgPfcArray.crIdx02Name[0]),
                                                                            pcpFctc,&llRow,
                                                                            (void **)&rgPfcArray.pcrArrayRowBuf);

  if (ilRc == RC_SUCCESS)
  {
        strcpy(pcpDesc,FIELDVAL(rgPfcArray,rgPfcArray.pcrArrayRowBuf,igPfcFCTN));
    }
    else
    {
        dbg(TRACE,"Function <%s> not found",pcpFctc);
        *pcpDesc = '\0';
    }
    return ilRc;

}

static int GetRegularFunction(char *pcpFctc,char *pcpStfu,char *pcpSday)
{
    int ilRc = RC_SUCCESS;
    long llAction = ARR_FIRST;
    char clStart[24],clEnd[24],clFctc[24],clPrio[12];
    int ilPrio = 4711;
    int ilLastPrio = 4711;

    *pcpFctc = '\0';

    while(CEDAArrayFindRowPointer(&rgSpfArray.rrArrayHandle,rgSpfArray.crArrayName,
        &rgSpfArray.rrIdx02Handle,rgSpfArray.crIdx02Name,pcpStfu,
            &llAction,(void **)&rgSpfArray.pcrArrayRowBuf) == RC_SUCCESS)
    {
        llAction = ARR_NEXT;
        strcpy(clStart,FIELDVAL(rgSpfArray,rgSpfArray.pcrArrayRowBuf,igSpfVPFR));
        strcpy(clEnd,FIELDVAL(rgSpfArray,rgSpfArray.pcrArrayRowBuf,igSpfVPTO));
        clStart[8] = '\0';
        clEnd[8] = '\0';
        if (*clEnd == ' ' || clEnd == '\0')
        {
            strcpy(clEnd,pcpSday);
        }
        if (OVERLAP(pcpSday,pcpSday,clStart,clEnd))
        {
            strcpy(clFctc,FIELDVAL(rgSpfArray,rgSpfArray.pcrArrayRowBuf,igSpfCODE));
            strcpy(clPrio,FIELDVAL(rgSpfArray,rgSpfArray.pcrArrayRowBuf,igSpfPRIO));
            ilPrio = atoi(clPrio);
            ilPrio = ilPrio == 0 ? 4710 : ilPrio;
            dbg(DEBUG,"Valid function <%s> PRIO %d<%s> found "
            " VPFR <%s> VPTO <%s> SDAY <%s>",
                    clFctc,ilPrio,clPrio,clStart,clEnd,pcpSday);
            if (ilPrio < ilLastPrio && ilPrio > 0)
            {
                ilLastPrio = ilPrio;
                strcpy(pcpFctc,clFctc);
                if (ilPrio == 1)
                {
                    dbg(DEBUG,"Function with Prio 1 found");
                    break;
                }
            }

        }
        else
        {
            dbg(DEBUG,"Function is not valid VPFR <%s> VPTO <%s> for SDAY <%s>",clStart,clEnd,pcpSday);
        }
    }
    if (*pcpFctc == '\0')
    {
        dbg(TRACE,"no Function found for: <%s> <%s>",pcpStfu,pcpSday);
    }
    return ilRc;
}

static int GetPlannedShift(char **pcpDrr,char *pcpStfu,char *pcpSday)
{
    int ilRc = RC_FAIL;
    long llRow = ARR_FIRST;
    char *pclDrr;

    pclDrr = *pcpDrr;

    while(CEDAArrayFindRowPointer(&rgDrrArrayP.rrArrayHandle,
            rgDrrArrayP.crArrayName,&rgDrrArrayP.rrIdx01Handle,
                    rgDrrArrayP.crIdx01Name,pcpStfu,&llRow,
                            (void **)&pclDrr) == RC_SUCCESS)
        {
            llRow = ARR_NEXT;
            if(strncmp(pcpSday,
                    FIELDVAL(rgDrrArrayP,pclDrr,igDrrSDAY),8) == 0)
            {
                ilRc = RC_SUCCESS;
                dbg(DEBUG,"Planned shift found: %s %s %s",
                        FIELDVAL(rgDrrArrayP,pclDrr,igDrrSTFU),
                        FIELDVAL(rgDrrArrayP,pclDrr,igDrrSDAY),
                        FIELDVAL(rgDrrArrayP,pclDrr,igDrrDRRN));
                *pcpDrr = pclDrr;
                break;
            }
        }
    if (pclDrr == NULL)
    {
        dbg(TRACE,"no planned shift found for <%s> <%s>",pcpStfu,pcpSday);
        *pcpDrr = NULL;
    }
        return ilRc;
}

static int CheckPeno(char *pcpDest,char *pcpPeno)
{
    int ilRc = RC_SUCCESS;
    int i;

    for ( i  = 0; i < 8; i++)
    {
            if (pcpPeno[i] < '0' || pcpPeno[i] > '9')
            {
                strcpy(pcpDest,cgDefaultPeno);
                return RC_FAIL;
            }
    }
  strcpy(pcpDest,pcpPeno);
    return ilRc;
}


static int CreateOneShiftLine(char *pcpLineBuffer,char *pcpDrr,char *pcpPeno,BOOL bpToFile,BOOL bpIsLongTerm)
{
    int ilRc = RC_SUCCESS;
    char clLineBuffer[124];
    char clKey[24];
    char clOffRestIndicator[12] = "";
    char clPlannedOffRestIndicator[12];
    int ilBytes = 0;
    long llRow;
    char clJobSelection[244];
    char clSbfr[24],clSbto[24],clDrrUrno[24],clSday[22],clSblu[12],clTmpSblu[24];
    char clAvfr[24],clAvto[24],clScod[32];
    char clSdac[24],clRema[240],clAbfr[32],clAbto[24];
    char clDrrn[12],clRosl[4],clStfu[12];
    char clTmpStr[204];
    char *pclRowBuf;
    char *pclLineBuffer;
    BOOL blIsBsd = FALSE;
    char clDrs1[4], clDrs3[4], clDrs4[4];
    char clTeamCode[24], clTeamDescription[124];
    char clSorVpto[24],clSorVpfr[24],clSorSelection[324],clSelection[324];
    char clRosterStart[24];
    char clRosterEnd[24];
    char clPeno[24];

    char clBreakStart[24] = "000000000000";
    char clBreakEnd[24] = "000000000000";

    char clOt1Start[24] = "000000000000";
    char clOt1End[24] = "000000000000";

    char clOt2Start[24] = "000000000000";
    char clOt2End[24] = "000000000000";

    char clBsdc[44];
    long llAction = ARR_FIRST;

    char clBsdStart[24];
    char clBsdEnd[24];

    char clReason[128];

    char clUser[48];
    BOOL blExport = FALSE;
    char clOabs[24];


    strcpy(clDrs1,FIELDVAL(rgDrrArray,pcpDrr,igDrrDRS1));
    strcpy(clDrs3,FIELDVAL(rgDrrArray,pcpDrr,igDrrDRS3));
    strcpy(clDrs4,FIELDVAL(rgDrrArray,pcpDrr,igDrrDRS4));
    strcpy(clDrrn,FIELDVAL(rgDrrArray,pcpDrr,igDrrDRRN));
    strcpy(clRosl,FIELDVAL(rgDrrArray,pcpDrr,igDrrROSL));
    strcpy(clSday,FIELDVAL(rgDrrArray,pcpDrr,igDrrSDAY));
    strcpy(clScod,FIELDVAL(rgDrrArray,pcpDrr,igDrrSCOD));
    strcpy(clStfu,FIELDVAL(rgDrrArray,pcpDrr,igDrrSTFU));
    strcpy(clOabs,FIELDVAL(rgDrrArray,pcpDrr,igDrrOABS));

    StringTrimRight(clScod);
    if ((strcmp(clScod," ") == 0) || (*clScod == '\0'))
    {
        /* deleted Shift, do nothing */
        dbg(DEBUG,"Shift <%s> is deleted",clScod);
        return 0;
    }
    strcpy(clRosterStart,FIELDVAL(rgDrrArray,pcpDrr,igDrrAVFR));
    strcpy(clRosterEnd,FIELDVAL(rgDrrArray,pcpDrr,igDrrAVTO));

    dbg(DEBUG,"shift sday <%s> start <%s> end <%s>",clSday,clRosterStart,clRosterEnd);
    dbg(DEBUG,"OT flags: DRS1 <%s> DRS3 <%s> DRS4 <%s>",clDrs1,clDrs3,clDrs4);
    /* we need the basic shift times */
    /* BSD_FIELDS "URNO,BSDC,ESBG,LSEN"*/

    strcpy(clBsdc,FIELDVAL(rgDrrArray,pcpDrr,igDrrSCOD));
    dbg(TRACE,"searching basic shift with <%s>",clBsdc);

    ilRc = CEDAArrayFindRowPointer(&rgBsdArray.rrArrayHandle,rgBsdArray.crArrayName,
                                   &rgBsdArray.rrIdx02Handle,rgBsdArray.crIdx02Name,clBsdc,
                                   &llAction,(void **)&rgBsdArray.pcrArrayRowBuf);

    if (ilRc == RC_SUCCESS)
    {
        /* it is a shift, so it must be exported */
        blExport = TRUE;
        blIsBsd = TRUE;
    }
    else if (bpIsLongTerm == FALSE)
    {
        /* or it is an online update and should be exported anyway */
        blExport = TRUE;
        blIsBsd = FALSE;
    }
    else
    {
        /* it is not a shift, but maybe Regular Free or Sleepday */
        strcpy(clBsdc,FIELDVAL(rgDrrArray,pcpDrr,igDrrSCOD));
        blExport = ExportAbsence(clBsdc);
        blIsBsd = FALSE;
    }

    if (blExport == TRUE)
    {
        sprintf(clBsdStart,"%s%s00", FIELDVAL(rgDrrArray,pcpDrr,igDrrSDAY),
                                     FIELDVAL(rgBsdArray,rgBsdArray.pcrArrayRowBuf,igBsdESBG));
        sprintf(clBsdEnd,"%s%s00",   FIELDVAL(rgDrrArray,pcpDrr,igDrrSDAY),
                                     FIELDVAL(rgBsdArray,rgBsdArray.pcrArrayRowBuf,igBsdLSEN));

        dbg(TRACE,"%05d: RC=%d clBsdStart <%s> BsdEnd <%s>",__LINE__,ilRc,clBsdStart,clBsdEnd);
        if (strcmp(clBsdStart,clBsdEnd) > 0)
        {
            AddSecondsToCEDATime(clBsdEnd,(time_t)86400,1);
            dbg(DEBUG,"%05d: shift start <%s> > end <%s> , move end to next day", __LINE__,clBsdStart,clBsdEnd);
        }

        if (*clDrs4 == '1')
        {
                char *pclDrr = NULL;

                /* complete shift is overtime */
                strcpy(clRosterStart,FIELDVAL(rgDrrArray,pcpDrr,igDrrAVFR));
                strcpy(clRosterEnd,FIELDVAL(rgDrrArray,pcpDrr,igDrrAVTO));
                strcpy(clOt1Start,clRosterStart);
                strcpy(clOt1End,clRosterEnd);
                strcpy(clRosterEnd,clRosterStart); /* absence always for only one day */
                strcpy(clOt2Start,"000000000000");
                strcpy(clOt2End,"000000000000");
                strcpy(&clRosterStart[8],"0000");
                strcpy(&clRosterEnd[8],"0000");
                StringTrimRight(clOabs);
                dbg(TRACE,"DRS4 set to '%s' checking OABS <%s>",clDrs4,clOabs);
                if(strstr("OFF,RD",clOabs) == NULL)
                {
                    if( GetPlannedShift(&pclDrr,clStfu,clSday) == RC_SUCCESS)
                    {
                        GetOffRestIndicator(clOffRestIndicator,FIELDVAL(rgDrrArrayP,pclDrr,igDrrSCOD));
                        dbg(DEBUG,"Planned %s %s %s used to get Off/Rest Indicator %s",
                                FIELDVAL(rgDrrArrayP,pclDrr,igDrrSTFU),
                                FIELDVAL(rgDrrArrayP,pclDrr,igDrrSDAY),
                                FIELDVAL(rgDrrArrayP,pclDrr,igDrrDRRN),
                                clOffRestIndicator);
                    }
                    else
                    {
                        GetOffRestIndicator(clOffRestIndicator,clOabs);
                    }
                }
        }
        else 
        {
            if (*clDrs1 == '1')
            {
                /* overtime before begin of shift */
                if (blIsBsd)
                {
                    strcpy(clOt1Start,FIELDVAL(rgDrrArray,pcpDrr,igDrrAVFR));
                    sprintf(clOt1End,"%s%s",FIELDVAL(rgDrrArray,pcpDrr,igDrrSDAY),
                        FIELDVAL(rgBsdArray,rgBsdArray.pcrArrayRowBuf,igBsdESBG));
                    strcpy(clRosterStart,clOt1End);
                }
                else
                {
                    sprintf(clOt1Start,"%s0000",FIELDVAL(rgDrrArray,pcpDrr,igDrrSDAY));
                    strcpy(clOt1End,clOt1End);
                    strcpy(clRosterStart,clOt1End);
                }
            }
            if (*clDrs3 == '1')
            {
                /* overtime after end of shift */
                if (blIsBsd)
                {
                    strcpy(clOt2End,FIELDVAL(rgDrrArray,pcpDrr,igDrrAVTO));
                    /***
                    sprintf(clOt2Start,"%s%s",FIELDVAL(rgDrrArray,pcpDrr,igDrrSDAY),
                        FIELDVAL(rgBsdArray,rgBsdArray.pcrArrayRowBuf,igBsdLSEN));
                        ***/
                    strcpy(clOt2Start,clBsdEnd);
                    if (strcmp(clOt2Start,clOt2End) > 0)
                    {
                        dbg(TRACE,"%05d: OT 2 start <%s> > end <%s> , move end to next day",
                            __LINE__,clOt2Start,clOt2End);
                        AddSecondsToCEDATime(clOt2End,(time_t)86400,1);
                    }
                    strcpy(clRosterEnd,clOt2Start);
                }
                else
                {
                    sprintf(clOt2Start,"%s0000",FIELDVAL(rgDrrArray,pcpDrr,igDrrSDAY));
                    strcpy(clOt2End,clOt2Start);
                    strcpy(clRosterStart,clOt2Start);
                }
            }
        }

        /** now get the break times **/
        /* get SBFR,SBTO from break job */
        /* JOB_FIELDS = "ACFR,ACTO,UDSR"*/

        if (blIsBsd == TRUE)
        {
            GetBreak(pcpDrr,clBreakStart,clBreakEnd,bpIsLongTerm);
            GetAbsence(pcpDrr,clSday,clDrrn,rgAbsences);
        }
        else
        {
        /*** Full day Absence, get original shift **/
            long llRow = ARR_FIRST;
            BOOL blShiftFound = FALSE;
            char clKey[32];
            char clBsdc[32];
            char *pclDrr;
            int ilRosl;

            /* because we don't export full absences anymore we should never come here */
            dbg(DEBUG,"Full Day Absence, this log entry should never occur except for RD/OFF");
            strcpy(&clRosterStart[8],"0000");
            strcpy(&clRosterEnd[8],"0000");
            strcpy(clBreakStart,"000000000000");
            strcpy(clBreakEnd,"000000000000");
            dbg(DEBUG,"shift is OFF/RD set times to <0> Start: <%s> End: <%s>",
                clRosterStart,clRosterEnd);

            memset(rgAbsences,0,sizeof(rgAbsences));

            strcpy(rgAbsences[1].Abfr,"000000000000");
            strcpy(rgAbsences[1].Abto,"000000000000");

            strcpy(rgAbsences[0].Sdac,clScod);
            sprintf(rgAbsences[0].Abfr,"%s0000",clSday);
            sprintf(rgAbsences[0].Abto,"%s0000",clSday);
            GetAbsenceRemark(rgAbsences[0].Sdac,rgAbsences[0].Rema);
            dbg(DEBUG,"Full Day Absence, Absence 1 filled with: <%s> <%s> <%s> <%s>",
                        rgAbsences[0].Sdac,rgAbsences[0].Abfr,
                        rgAbsences[0].Abto,rgAbsences[0].Rema);
                    
#if 0
            for(ilRosl = atoi(clRosl); ilRosl >= 1; ilRosl--)
            {
                llRow = ARR_FIRST;
                sprintf(clKey,"%s,%s,%d",clStfu,clSday,ilRosl);
                if (CEDAArrayFindRowPointer(&rgDrrArray.rrArrayHandle,
                    rgDrrArray.crArrayName,&rgDrrArray.rrIdx03Handle,
                        rgDrrArray.crIdx03Name,clKey,&llRow,
                                (void **)&pclDrr) == RC_SUCCESS)
                {
                dbg(DEBUG,"Shift found for Key <%s>",clKey);
                    llRow = ARR_FIRST;
                    strcpy(clBsdc,FIELDVAL(rgDrrArray,pclDrr,igDrrSCOD));
                    if(CEDAArrayFindRowPointer(&rgBsdArray.rrArrayHandle,rgBsdArray.crArrayName,
                &rgBsdArray.rrIdx02Handle,rgBsdArray.crIdx02Name,clBsdc,
                    &llRow,(void **)&rgBsdArray.pcrArrayRowBuf) == RC_SUCCESS)
                    {
                        blShiftFound = TRUE;
                        dbg(DEBUG,"<%s> found in BSDTAB",clBsdc);
                        break;
                    }
                    else
                    {
                        dbg(DEBUG,"<%s> not found in BSDTAB",clBsdc);
                    }
                }
            }
            if (blShiftFound == TRUE)
            {
                    strcpy(clRosterStart,FIELDVAL(rgDrrArray,pclDrr,igDrrAVFR));
                    strcpy(clRosterEnd,FIELDVAL(rgDrrArray,pclDrr,igDrrAVTO));
                    strcpy(clScod,FIELDVAL(rgDrrArray,pclDrr,igDrrSCOD));
                    dbg(DEBUG,"Shift found for full day absence: <%s> ROSL <%s>",clScod,
                        FIELDVAL(rgDrrArray,pclDrr,igDrrROSL));
                    GetBreak(pclDrr,clBreakStart,clBreakEnd,bpIsLongTerm);
            }
        else
        {
            dbg(DEBUG,"no shift found for key <%s> full day absence",clKey);
            strcpy(&clRosterStart[8],"0000");
            strcpy(&clRosterEnd[8],"0000");
            strcpy(clBreakStart,"000000000000");
            strcpy(clBreakEnd,"000000000000");
        }
#endif

    }

    if (*clOffRestIndicator == '\0')
    {
        char clTmpScod[24];
        if((strstr("OFF,RD",clOabs) != NULL) &&
            (strstr("OFF,RD",clScod) == NULL))
        {
            strcpy(clTmpScod,clOabs);
        }
        else
        {
            strcpy(clTmpScod,clScod);
        }
        GetOffRestIndicator(clOffRestIndicator,clTmpScod);
    }
/************
#define DRR_FIELDS "AVFR,AVTO,BKDP,BSDU,BUFU,CDAT,DRRN,DRS1,DRS2,DRS3,DRS4,"\
           "DRSF,EXPF,FCTC,HOPO,LSTU,REMA,ROSL,ROSS,SBFR,SBLP,SBLU,"\
           "DRRN,SBTO,SCOD,SCOO,SDAY,STFU,URNO,USEC,USEU"
             ************/

    pclLineBuffer = pcpLineBuffer;
    *pclLineBuffer = '\0';

    StringPut(pclLineBuffer,"10",2); /* 00 Record identifier **/
    pclLineBuffer += 2;
    ilBytes += 2;
    /* 01 Employee Number */
    StringPut(pclLineBuffer,pcpPeno,8);  
    pclLineBuffer += 8;
    ilBytes += 8;
/* 02 Actual Shift Code */
    StringPut(pclLineBuffer,clScod,8);
    pclLineBuffer += 8;
    ilBytes += 8;
/* 03 Roster Start */
    if (*clRosterStart == ' ')  
        strcpy(clRosterStart,"000000000000");
    StringPut(pclLineBuffer,clRosterStart,12);
    pclLineBuffer += 12;
    ilBytes += 12;
/* 04 Roster End */
    if (*clRosterEnd == ' ')  
        strcpy(clRosterEnd,"000000000000");
    StringPut(pclLineBuffer,clRosterEnd,12);
    pclLineBuffer += 12;
    ilBytes += 12;
/* 05 Actual Break Start */
    StringPut(pclLineBuffer,clBreakStart,12);
    pclLineBuffer += 12;
    ilBytes += 12;
/* 06 Actual Break End */
    StringPut(pclLineBuffer,clBreakEnd,12);
    pclLineBuffer += 12;
    ilBytes += 12;
/* 07 Actual OT 1 Start */
    StringPut(pclLineBuffer,clOt1Start,12);
    pclLineBuffer += 12;
    ilBytes += 12;
/* 08 Actual OT 1 End */
    StringPut(pclLineBuffer,clOt1End,12);
    pclLineBuffer += 12;
    ilBytes += 12;
/* 09 Actual OT 2 Start */
    StringPut(pclLineBuffer,clOt2Start,12);
    pclLineBuffer += 12;
    ilBytes += 12;
/* 10 Actual OT 2 End */
    StringPut(pclLineBuffer,clOt2End,12);
    pclLineBuffer += 12;
    ilBytes += 12;
/* 11 OFF/REST day indicator */
    StringPut(pclLineBuffer,clOffRestIndicator,1);
    pclLineBuffer += 1;
    ilBytes += 1;
/* 12 Absence Code 1 */
    StringPut(pclLineBuffer,rgAbsences[0].Sdac,5);
    pclLineBuffer += 5;
    ilBytes += 5;
/* 13 Absence Description 1 */
    StringPut(pclLineBuffer,rgAbsences[0].Rema,40);
    pclLineBuffer += 40;
    ilBytes += 40;
/* 14 Start Time of absence/attendance 1 */
    StringPut(pclLineBuffer,rgAbsences[0].Abfr,12);
    pclLineBuffer += 12;
    ilBytes += 12;
/* 15 End Time of absence/attendance 1 */
    StringPut(pclLineBuffer,rgAbsences[0].Abto,12);
    pclLineBuffer += 12;
    ilBytes += 12;
/* 16 Absence Code 2 */
    StringPut(pclLineBuffer,rgAbsences[1].Sdac,5);
    pclLineBuffer += 5;
    ilBytes += 5;
/* 17 Absence Description 2 */
    StringPut(pclLineBuffer,rgAbsences[1].Rema,40);
    pclLineBuffer += 40;
    ilBytes += 40;
/* 18 Start Time of absence/attendance 2 */
    StringPut(pclLineBuffer,rgAbsences[1].Abfr,12);
    pclLineBuffer += 12;
    ilBytes += 12;
/* 19 End Time of absence/attendance 2 */
    StringPut(pclLineBuffer,rgAbsences[1].Abto,12);
    pclLineBuffer += 12;
    ilBytes += 12;
/* 20 Reason (MAC) Flight-Number of jobs done during contractual break times*/

    *clReason = '\0';
    GetReasonForBreaks(sizeof(clReason),clReason,FIELDVAL(rgDrrArray,pcpDrr,igDrrSTFU),clRosterStart,clRosterEnd);
    StringPut(pclLineBuffer,clReason,50);
    pclLineBuffer += 50;
    ilBytes += 50;
    memset(clReason,0,sizeof(clReason));
    if (strcmp(clOt1Start,clOt1End) != 0)
    {
        GetReason(sizeof(clReason),clReason,FIELDVAL(rgDrrArray,pcpDrr,igDrrSTFU),clOt1Start,clOt1End);
    }
/* 21 Reason (OT 1) Flight-Number of jobs done during the overtime 1 */
    StringPut(pclLineBuffer,clReason,50);
    pclLineBuffer += 50;
    ilBytes += 50;
    memset(clReason,0,sizeof(clReason));
    if (strcmp(clOt2Start,clOt2End) != 0)
    {
        GetReason(sizeof(clReason),clReason,FIELDVAL(rgDrrArray,pcpDrr,igDrrSTFU),clOt2Start,clOt2End);
    }
/* 22 Reason (OT 2) Flight-Number of jobs done during the overtime 2 */
    StringPut(pclLineBuffer,clReason,50);
    pclLineBuffer += 50;
    ilBytes += 50;
/* 23 Shift Remark The remark field of the shift (DRR) */
    StringPut(pclLineBuffer,FIELDVAL(rgDrrArray,pcpDrr,igDrrREMA),40);
    pclLineBuffer += 40;
    ilBytes += 40;
/* 24 Team Code */
    GetTeamCode(pcpDrr,clTeamCode,clTeamDescription);
    StringPut(pclLineBuffer,clTeamCode,5);
    pclLineBuffer += 5;
    ilBytes += 5;
/* 25 Team Code Description */
    StringPut(pclLineBuffer,clTeamDescription,60);
    pclLineBuffer += 60;
    ilBytes += 60;
/* 26 User ID The user ID of the user who applied the last change to the shift data */

        strcpy(clUser,FIELDVAL(rgDrrArray,pcpDrr,igDrrUSEU));
        StringTrimRight(clUser);
        if ((strcmp(clUser," ") == 0) || (strncmp(clUser,"SAP-HR",6) == 0))
        {
                strcpy(clUser,FIELDVAL(rgDrrArray,pcpDrr,igDrrUSEC));
        }
    CheckPeno(clPeno,clUser);
    StringPut(pclLineBuffer,clPeno,8);
    pclLineBuffer += 8;
    ilBytes += 8;
/* 27 Organizational Unit */
    sprintf(clSorVpfr,"%s000000",FIELDVAL(rgDrrArray,pcpDrr,igDrrSDAY));
    sprintf(clSorVpto,"%s235959",FIELDVAL(rgDrrArray,pcpDrr,igDrrSDAY));
    sprintf(clSorSelection,"WHERE SURN = '%s'  AND VPFR <= '%s' "
            " AND (VPTO >= '%s' OR VPTO = ' ')",
        clStfu,
            clSorVpfr,
            clSorVpto);
    if (DoSingleSelect("SORTAB",clSorSelection,"CODE",cgDataArea) == RC_SUCCESS)
    {
        char clNameOfDepartment[124] = "";

        StringPut(pclLineBuffer,cgDataArea,8);
        pclLineBuffer += 8;
        ilBytes += 8;
        
        /* get name of department */
        CheckOrgUnit(cgDataArea,clNameOfDepartment);
        /* 28 Organizational Unit description */
        StringPut(pclLineBuffer,clNameOfDepartment,40);
        pclLineBuffer += 40;
        ilBytes += 40;
    }
    else
    {
        dbg(DEBUG,"No org.unit found: <%s>",clSorSelection);
        StringPut(pclLineBuffer," ",8);
        pclLineBuffer += 8;
        ilBytes += 8;
        StringPut(pclLineBuffer," ",40);
        pclLineBuffer += 40;
        ilBytes += 40;
    }
    
    {
        char clFctc[24];

        strcpy(clFctc,FIELDVAL(rgDrrArray,pcpDrr,igDrrFCTC));
        StringPut(pclLineBuffer,clFctc,5);
        pclLineBuffer += 5;
        ilBytes += 5;

        GetFunctionDescription(clTmpStr,clFctc);
        StringPut(pclLineBuffer,clTmpStr,40);
        pclLineBuffer += 40;
        ilBytes += 40;

        GetRegularFunction(clFctc,clStfu,clSday);
        StringPut(pclLineBuffer,clFctc,5);
        pclLineBuffer += 5;
        ilBytes += 5;

        GetFunctionDescription(clTmpStr,clFctc);
        StringPut(pclLineBuffer,clTmpStr,40);
        pclLineBuffer += 40;
        ilBytes += 40;
    }
    /** add all jobs **/
    if (!bpIsLongTerm)
    {
            char clStart[24];
            char clEnd[24];
            char clFlightJobs[2048] = "";
            int ilLen;

            if (strcmp(clOt1Start,"000000000000"))
            {
                strcpy(clStart,clOt1Start);
            }
            else
            {
                strcpy(clStart,clRosterStart);
            }
            if (strcmp(clOt2End,"000000000000"))
            {
                strcpy(clEnd,clOt2End);
            }
            else
            {
                strcpy(clEnd,clRosterEnd);
            }

            GetJobsForShift(clFlightJobs,clStfu,clStart,clEnd);
            ilLen = strlen(clFlightJobs);
            if (ilLen > 0)
            {
                StringPut(pclLineBuffer,clFlightJobs,ilLen);
                pclLineBuffer += ilLen;
                ilBytes += ilLen;
            }
    }

    if (bpToFile)
    {
        StringPut(pclLineBuffer,"\r\n",2);
        ilBytes += 2;
    }
    }
    else
    {
        dbg(TRACE,"BSD record with BSDC <%s> not found",clBsdc);
        blIsBsd = FALSE;
    }
    
    return(ilBytes);
}


static int WriteTrailer(FILE *pflFile)
{
    int ilRc = RC_SUCCESS;
    char clTrailer[24];

    int ilBytes;

    sprintf(clTrailer,"99\r\n");
    ilBytes = strlen(clTrailer);
    errno = 0;
    if(fwrite(clTrailer,sizeof(char),ilBytes,pflFile) != ilBytes)
    {
        dbg(TRACE,"could not write Trailer: <%s>, reason %d <%s>",
                clTrailer,errno,strerror(errno));
        ilRc = RC_FAIL;
    }
    return ilRc;
}

static int WriteHeader(FILE *pflFile,char *pcpFileId,char *pcpFileName,long lpLineCount)
{
    int ilRc = RC_SUCCESS;
    char clTimeStamp[24];
    char clHeader[300];
    int ilBytes;
    
    GetServerTimeStamp("UTC", 1,0,clTimeStamp);
    sprintf(clHeader,"00%s%s%s%010d\r\n",pcpFileId,pcpFileName,clTimeStamp,lpLineCount);
    dbg(DEBUG,"WriteHeader: <%s> <%s> <%s> %ld",
                    clHeader,pcpFileName,clTimeStamp,lpLineCount);
    ilBytes = strlen(clHeader);
    errno = 0;
    if(fwrite(clHeader,sizeof(char),ilBytes,pflFile) != ilBytes)
    {
        dbg(TRACE,"could not write header: <%s>, reason %d <%s>",
                clHeader,errno,strerror(errno));
        ilRc = RC_FAIL;
    }
    return ilRc;
}


static int MakeFileName(char *pcpFileId,char *pcpFileName)
{
    int ilRc = RC_SUCCESS;
    char clTimeStamp[24];
    
    GetServerTimeStamp("LOC", 1,0,clTimeStamp);
    clTimeStamp[12] = '\0';

    sprintf(pcpFileName,"%s%s.dat",pcpFileId,clTimeStamp);

    return ilRc;
}


static int GetFileNames(short spFunc,char *pcpFileId,char *pcpFileDir,char *pcpFileName)
{
    static DIR *prlNewFiles = NULL;
    char clFileName[512] = "";
    int ilRc = RC_FAIL;
    int ilRcLocal;
    struct dirent *prlBuf;

    if (prlNewFiles == NULL)
    {
        sprintf(clFileName,"%s%s",cgFileBase,pcpFileDir);
        dbg(DEBUG,"GetFileNames: open directory <%s>",clFileName);
        prlNewFiles = opendir(clFileName);
      if (prlNewFiles == NULL)
        {
            dbg(TRACE,"GetFileNames: directory <%s> not opened",clFileName);
            dbg(TRACE,"<%s>",strerror(errno));
        }
    }
    else
    {
        if (spFunc == START)
        {
            rewinddir(prlNewFiles);
        }
    }
    if (prlNewFiles)
    {
        int ilRcLocal = 0;
    
      while((prlBuf = readdir(prlNewFiles)) != NULL)
        {
            if (prlNewFiles == NULL)
            {
                dbg(DEBUG,"GetFileNames: no more file entry in <%s>",clFileName);
                closedir(prlNewFiles);
                prlNewFiles = NULL; 
                break;
            }
            else 
            {
                dbg(DEBUG,"GetFileNames: File <%s> found",prlBuf->d_name);
                if (strncmp(prlBuf->d_name,pcpFileId,strlen(pcpFileId)) == 0)
                {
                    sprintf(pcpFileName,"%s%s%s",cgFileBase,pcpFileDir,prlBuf->d_name);
                    ilRc = RC_SUCCESS;
                    break;
                }
            }
        }
    }
        dbg(DEBUG,"end of GetFileNames:");
        return ilRc;
    }

static void StringTrimRight(char *pcpBuffer)
{
    if (*pcpBuffer != '\0')
    {
        char *pclBlank = &pcpBuffer[strlen(pcpBuffer)-1];
        while(isspace(*pclBlank) && pclBlank != pcpBuffer)
        {
            *pclBlank = '\0';
            pclBlank--;
        }
    }
}

static void StringPut(char *pcpDest,char *pcpSource,long lpLen)
{

    int i,ilSourceLen;

    ilSourceLen = strlen(pcpSource);

    for(i = 0; i < lpLen; i++)
    {
        if (i < ilSourceLen)
        {
            pcpDest[i] = pcpSource[i];
        }
        else
        {
            pcpDest[i] = ' ';
        }
    }
}

static int CheckDateFormat(char *pcpDate)
{
    int ilRc = RC_SUCCESS;

    char clYear[8];
    char clMonth[4];
    char clDay[4];
    char clHour[4];
    char clMin[4];
    char clSec[4];
    char *pclDate;
    int ilActualPos = 0;
    int ilLc;
    static int igDaysPerMonth[12] = {31,28,31,30,31,30,31,31,30,31,30,31};

    int ilYear,ilMonth,ilDay,ilHour,ilMin,ilSec;

    for (ilLc = 0; pcpDate[ilLc] != '\0'; ilLc++)
    {
            if(! (pcpDate[ilLc] >= '0' && pcpDate[ilLc] <= '9'))
            {
                strcpy(pcpDate," ");
                return (RC_FAIL);
            }
    }

    pclDate = pcpDate;

    StringMid(clYear,pclDate,ilActualPos,4,NOCONVERT);
    ilActualPos += 4;
    StringMid(clMonth,pclDate,ilActualPos,2,NOCONVERT);
    ilActualPos += 2;
    StringMid(clDay,pclDate,ilActualPos,2,NOCONVERT);
    ilActualPos += 2;
    StringMid(clHour,pclDate,ilActualPos,2,NOCONVERT);
    ilActualPos += 2;
    StringMid(clMin,pclDate,ilActualPos,2,NOCONVERT);
    ilActualPos += 2;
    StringMid(clSec,pclDate,ilActualPos,2,NOCONVERT);
    ilActualPos += 2;

    dbg(TRACE,"<%s><%s><%s><%s><%s><%s>",clYear,clMonth,clDay,clHour,clMin,clSec);
    ilYear = atoi(clYear);
    ilMonth = atoi(clMonth);
    ilDay = atoi(clDay);
    ilHour = atoi(clHour);
    ilMin = atoi(clMin);
    ilSec = atoi(clSec);

    if (ilYear < 1900 || ilYear > 9999)
    {
        dbg(TRACE,"CheckDateFormat: invalid year %d <%s>",ilYear,pcpDate);
        ilRc = RC_FAIL;
    }
    else
    {
        if (ilMonth < 1 || ilMonth > 12)
        {
            dbg(TRACE,"CheckDateFormat: invalid month <%s>",pcpDate);
            ilRc = RC_FAIL;
        }
        else
        {
            if (ilMonth == 2)
            {
                if ( ((ilYear % 4) == 0) && (((ilYear % 100) != 0) || (ilYear % 400) == 0)) 
                {
                    /* is leap year */
                    if (ilDay > 29 || ilDay < 1)
                    {
                        dbg(TRACE,"CheckDateFormat: invalid day <%s> in leap year",pcpDate);
                        ilRc = RC_FAIL;
                    }
                }
                else if (ilDay < 1 || ilDay > igDaysPerMonth[ilMonth-1])
                {
                    dbg(TRACE,"CheckDateFormat: invalid day <%s>",pcpDate);
                    ilRc = RC_FAIL;
                }
            }
            else
            {
                if (ilDay < 1 || ilDay > igDaysPerMonth[ilMonth-1])
                {
                    dbg(TRACE,"CheckDateFormat: invalid day <%s>",pcpDate);
                    ilRc = RC_FAIL;
                }
            }
            if (ilHour < 0 || ilHour > 24)
            {
                dbg(TRACE,"CheckDateFormat: invalid hour <%s>",pcpDate);
                ilRc = RC_FAIL;
            }
            if (ilMin < 0 || ilMin > 60)
            {
                dbg(TRACE,"CheckDateFormat: invalid minute <%s>",pcpDate);
                ilRc = RC_FAIL;
            }
            if (ilSec < 0 || ilSec > 60)
            {
                dbg(TRACE,"CheckDateFormat: invalid second <%s>",pcpDate);
                ilRc = RC_FAIL;
            }
        }
    }

    return ilRc;
}
static int CheckDate14(char *pcpDate)
{
    int ilRc = RC_SUCCESS;
    int ilLc;

    dbg(DEBUG,"CheckDate14: <%s>",pcpDate);
    for (ilLc = 0; ilLc < 14 && pcpDate[ilLc] != '\0'; ilLc++)
    {
            if(! (pcpDate[ilLc] >= '0' && pcpDate[ilLc] <= '9'))
            {
                strcpy(pcpDate," ");
                return (RC_FAIL);
            }
    }
    for (; ilLc < 14 ; ilLc++)
    {
            pcpDate[ilLc] = '0';
    }
    pcpDate[ilLc] = '\0';
    dbg(DEBUG,"CheckDate14: <%s>",pcpDate);

    return ilRc;
}   

static void StringMid(char *pcpDest,char *pcpSource,long lpFrom,long lpLen,int ipConvert)
{
    pcpDest = strncpy(pcpDest,&pcpSource[lpFrom],lpLen);
    pcpDest[lpLen] = '\0';
    switch(ipConvert)
    {
    case CLIENTTODB:
        ConvertClientStringToDb(pcpDest);
        break;
    case DBTOCLIENT:
        ConvertDbStringToClient(pcpDest);
        break;
    default:
            /* do nothing */
            ;
    }
}

static char *ItemPut(char *pcpDest,char *pcpSource,char cpDelimiter)
{
    strcpy(pcpDest,pcpSource);
    pcpDest += strlen(pcpDest);
    if (cpDelimiter != '\0')
    {
        *pcpDest = cpDelimiter;
        pcpDest++;
    }
    return (pcpDest);
}

static int LogError(char *pcpFileName,char *pcpErrorString,BOOL bpAlert)
{
    int ilRc = RC_SUCCESS;

    if (pfgLog == NULL)
    {
        char clLogFileName[512];

        sprintf(clLogFileName,"%s%staahdl%ld",cgFileBase,cgFileLog,getpid());
        pfgLog = fopen(clLogFileName,"a");
        if (pfgLog != NULL)
        {
            dbg(DEBUG,"Logfile <%s> opened",clLogFileName);
        }
        else
        {
            dbg(DEBUG,"Logfile <%s> not opened",clLogFileName);
        }
    }
    if (pfgLog != NULL)
    {
        char clTimeStamp[24];

        GetServerTimeStamp("UTC", 1,0,clTimeStamp);
        fprintf(pfgLog,"%s: Error: <%s> Line: <%s>\r\n",
                clTimeStamp,pcpErrorString,cgLineBuf);
        dbg(DEBUG,"%s <%s>",pcpErrorString,cgLineBuf);
        fclose(pfgLog);
        pfgLog = NULL;
        /*
        AddAlert(cgAlertName,"R",clTimeStamp,"D",pcpErrorString,TRUE,TRUE,
                cgDestName,cgRecvName,cgTwStart,cgTwEnd);
                **/
        if (bpAlert == TRUE)
        {
            AddAlert2(cgAlertName,"R"," "," ",pcpErrorString,cgLineBuf,TRUE,FALSE,
                cgDestName,cgRecvName,cgTwStart,cgTwEnd);
        }
    }
    return ilRc;
}



static void PutSequence(char *pcpKey,long lpSequence)
{
    int ilRc;

    short slCursor = 0;
    char clSqlBuf[200];

    sprintf(clSqlBuf,"UPDATE NUMTAB SET ACNU='%10d' WHERE KEYS='%s' "
        " AND HOPO='%s'",lpSequence,pcpKey,cgHopo);
    ilRc = sql_if(START,&slCursor,clSqlBuf,cgDataArea);
    if(ilRc == RC_NOTFOUND)
    {
        /* sequence not yet written, create new one */
        sprintf(clSqlBuf,"INSERT INTO NUMTAB VALUES(%d,' ','%s','%s',9999999999,1)",
            lpSequence,cgHopo,pcpKey);
        close_my_cursor(&slCursor);
        slCursor = 0;
        ilRc = sql_if(START,&slCursor,clSqlBuf,cgDataArea);
        if (ilRc != RC_SUCCESS)
        {
            /* there must be a database error */
            get_ora_err (ilRc, cgErrMsg);
            dbg(TRACE,"Inserting Sequence <%s> failed RC=%d",pcpKey,ilRc);
        }
        close_my_cursor(&slCursor);
  }
    else if (ilRc != DB_SUCCESS)
    { 
        /* there must be a database error */
        get_ora_err(ilRc, cgErrMsg);
        dbg(TRACE,"Reading Sequence <%s> failed RC=%d",pcpKey,ilRc);
    }
    commit_work();
    close_my_cursor(&slCursor);
    slCursor = 0;
}


static long GetSequence(char *pcpKey)
{
    int ilRc;
    long llSequence = 0;
    short slCursor = 0;
    char clSqlBuf[200];

    sprintf(clSqlBuf,"SELECT ACNU FROM NUMTAB WHERE KEYS='%s' AND HOPO='%s'",
            pcpKey,cgHopo);
    ilRc = sql_if(START,&slCursor,clSqlBuf,cgDataArea);
    if(ilRc == RC_SUCCESS)
    {
        llSequence = atol(cgDataArea);
  }
    else if(ilRc == RC_NOTFOUND)
    {
        PutSequence(pcpKey,0);
        llSequence = 0;
    } 
    else
    { 
        /* there must be a database error */
        get_ora_err (ilRc, cgErrMsg);
        dbg(TRACE,"Reading Sequence <%s> failed RC=%d",pcpKey,ilRc);
    }
    close_my_cursor(&slCursor);
    slCursor = 0;

    return llSequence;
}

static int DoSelect(short spFunction,short *pspCursor,char *pcpTable,char *pcpSelection,char *pcpFields,char *pcpDataArea)
{
    int ilRc = RC_SUCCESS;
    static char clSqlBuf[4096];

    char *pclDataArea;
    int ilLc,ilItemCount;

    sprintf(clSqlBuf,"SELECT %s FROM %s %s",pcpFields,pcpTable,pcpSelection);

    dbg(DEBUG,"DoSelect: clSqlBuf <%s>",clSqlBuf);
  *pcpDataArea = '\0';
    ilRc = sql_if(spFunction,pspCursor,clSqlBuf,pcpDataArea);
    if(ilRc != RC_SUCCESS && ilRc != NOTFOUND)
    {
        /* there must be a database error */
        get_ora_err(ilRc, cgErrMsg);
        dbg(TRACE,"DoSelect failed RC=%d <%s>",ilRc,cgErrMsg);
        dbg(TRACE,"SQL: <%s>",clSqlBuf);
    }
    if (ilRc != RC_SUCCESS)
    {
        close_my_cursor(pspCursor);
        *pspCursor = 0;
    }

    ilItemCount = get_no_of_items(pcpFields); 
    pclDataArea = pcpDataArea;
    for(ilLc =  1; ilLc < ilItemCount; ilLc++)
    {
        while(*pclDataArea != '\0')
        {
            pclDataArea++;
        }
            *pclDataArea = ',';
    }
  if (ilRc == RC_SUCCESS)
    {
        dbg(DEBUG,"DoSelect: RC=%d  items=%d <%s>",ilRc,ilItemCount,pcpDataArea);
    }
    else
    {
        dbg(DEBUG,"DoSelect: RC=%d",ilRc,pcpDataArea);
    }
    return(ilRc);
}

static int DoSingleSelect(char *pcpTable,char *pcpSelection,char *pcpFields,char *pcpDataArea)
{
    int ilRc = RC_SUCCESS;
    static char clSqlBuf[4096];

    short slCursor = 0;
    short slFunction;
    char *pclDataArea;
    int ilLc,ilItemCount;

    sprintf(clSqlBuf,"SELECT %s FROM %s %s",pcpFields,pcpTable,pcpSelection);

  *pcpDataArea = '\0';
    ilRc = sql_if(START,&slCursor,clSqlBuf,pcpDataArea);
    if(ilRc != RC_SUCCESS && ilRc != NOTFOUND)
    {
        /* there must be a database error */
        get_ora_err(ilRc, cgErrMsg);
        dbg(TRACE,"DoSingleSelect failed RC=%d <%s>",ilRc,cgErrMsg);
        dbg(TRACE,"SQL: <%s>",clSqlBuf);
    }
    close_my_cursor(&slCursor);
    slCursor = 0;

    ilItemCount = get_no_of_items(pcpFields); 
    pclDataArea = pcpDataArea;
    for(ilLc =  1; ilLc < ilItemCount; ilLc++)
    {
        while(*pclDataArea != '\0')
        {
            pclDataArea++;
        }
            *pclDataArea = ',';
        }
  if (ilRc == RC_SUCCESS)
    {
        dbg(DEBUG,"DoSingleSelect: RC=%d <%s>",ilRc,pcpDataArea);
    }
    else
    {
        dbg(DEBUG,"DoSingleSelect: RC=%d",ilRc);
    }
    return(ilRc);
}

static int  DoDelete(char *pcpTable,char *pcpSelection)
{
    int ilRc = RC_SUCCESS;
    static char clSqlBuf[4096];
    char clTimeStamp[24];

    short slCursor = 0;
    short slFunction;

    sprintf(clSqlBuf,"DELETE FROM %s %s",pcpTable,pcpSelection);

    if (debug_level == DEBUG)
    {
        dbg(DEBUG,"DoDelete: <%s>",clSqlBuf);
    }
    ilRc = sql_if(START,&slCursor,clSqlBuf,cgDataArea);
    if(ilRc != RC_SUCCESS)
    {
        if(ilRc != RC_NOTFOUND)
        { 
            /* there must be a database error */
            get_ora_err(ilRc, cgErrMsg);
            dbg(TRACE,"DoDelete failed RC=%d <%s>",ilRc,cgErrMsg);
            dbg(TRACE,"SQL: <%s>",clSqlBuf);
        }
    }
    commit_work();
    close_my_cursor(&slCursor);
    slCursor = 0;
    return(ilRc);
}

static int  DoUpdate(char *pcpTable,char *pcpSelection,char *pcpFields,char *pcpDataArea)
{
    int ilRc = RC_SUCCESS;
    static char clSqlBuf[4096];
    static char clDataArea[40000];
    char clTimeStamp[24];

    short slCursor = 0;
    short slFunction;
    char *pclDataArea;

    strcpy(clDataArea,pcpDataArea);
    /************* don't set USEU because this should contain the 
    old user id 
    sprintf(clSqlBuf,"UPDATE %s SET %s,LSTU=:VLSTU,USEU=:VUSEU %s",
        pcpTable,pcpFields,pcpSelection);
        ***********/
    sprintf(clSqlBuf,"UPDATE %s SET %s,LSTU=:VLSTU %s",
        pcpTable,pcpFields,pcpSelection);

    if (clDataArea[strlen(clDataArea)-1] != ',')
    {
        strcat(clDataArea,",");
    }
    pclDataArea = clDataArea + strlen(clDataArea);
    GetServerTimeStamp("UTC", 1,0,clTimeStamp);
    pclDataArea = ItemPut(pclDataArea,clTimeStamp,',');
    /*pclDataArea = ItemPut(pclDataArea,cgAlertName,'\0');*/

    if (debug_level == DEBUG)
    {
        dbg(DEBUG,"DoUpdate: <%s>",clSqlBuf);
        dbg(DEBUG,"DoUpdate: <%s>",clDataArea);
    }
    delton(clDataArea);
    ilRc = sql_if(START,&slCursor,clSqlBuf,clDataArea);
    if(ilRc != RC_SUCCESS)
    {
        if(ilRc != RC_NOTFOUND)
        { 
            /* there must be a database error */
            get_ora_err(ilRc, cgErrMsg);
            dbg(TRACE,"DoUpdate failed RC=%d <%s>",ilRc,cgErrMsg);
            dbg(TRACE,"SQL: <%s>",clSqlBuf);
        }
    }
    commit_work();
    close_my_cursor(&slCursor);
    slCursor = 0;
    return(ilRc);
}

static int DoInsert(char *pcpTable,char *pcpFields,
                    char *pcpInsertFields,char *pcpDataArea)
{
    int ilRc = RC_SUCCESS;
    static char clSqlBuf[4096];
    static char clDataArea[40000];
    char clTimeStamp[24];
    static char clFieldList[2000];
    char clUrno[24];

    short slCursor = 0;
    short slFunction;
    char *pclDataArea;
    char *pclData;

    strcpy(clDataArea,pcpDataArea);
    dbg(DEBUG,"<%s>",clDataArea);
    sprintf(clFieldList,"%s,CDAT,LSTU,USEC,URNO,HOPO",pcpFields);
    sprintf(clSqlBuf,"INSERT INTO %s (%s,CDAT,LSTU,USEC,URNO,HOPO) VALUES(%s,"
    ":VCDAT,:VLSTU,:VUSEC,:VURNO,:VHOPO)",
        pcpTable,pcpFields,pcpInsertFields);

    if (clDataArea[strlen(clDataArea)-1] != ',')
    {
        strcat(clDataArea,",");
    }
    pclDataArea = clDataArea + strlen(clDataArea);
    GetServerTimeStamp("UTC", 1,0,clTimeStamp);
    pclDataArea = ItemPut(pclDataArea,clTimeStamp,',');
    pclDataArea = ItemPut(pclDataArea,clTimeStamp,',');
    pclDataArea = ItemPut(pclDataArea,cgAlertName,',');
    GetNextValues(clUrno,1);
    pclDataArea = ItemPut(pclDataArea,clUrno,',');
    pclDataArea = ItemPut(pclDataArea,cgHopo,'\0');
    if (debug_level == DEBUG)
    {
        dbg(DEBUG,"DoInsert: <%s>",clSqlBuf);
        dbg(DEBUG,"DoInsert: <%s>",clDataArea);
    }
    pclData = strdup(clDataArea);
    delton(clDataArea);
    ilRc = sql_if(START,&slCursor,clSqlBuf,clDataArea);
    if(ilRc != RC_SUCCESS)
    {
        /* there must be a database error */
        get_ora_err(ilRc, cgErrMsg);
        dbg(TRACE,"DoInsert failed RC=%d <%s>",ilRc,cgErrMsg);
        dbg(TRACE,"SQL: <%s>",clSqlBuf);
        rollback();
    }
    else
    {
        commit_work();
        /* send inserts to loghdl */
        if (pclData != NULL)
        {
        SendCedaEvent(igLoghdl,0,cgDestName,cgAlertName,cgTwStart, cgTwEnd,
          "IRT",pcpTable," ",clFieldList,pclData,"",5,0);          
        SendCedaEvent(igBchdl,0,cgDestName,cgAlertName,cgTwStart, cgTwEnd,
          "IRT",pcpTable," ",clFieldList,pclData,"",3,0);          
        SendCedaEvent(igAction,0,cgDestName,cgAlertName,cgTwStart, cgTwEnd,
          "IRT",pcpTable," ",clFieldList,pclData,"",3,0);          
            free(pclData);
        }
        else
        {
            dbg(TRACE,"DoInsert: not enough memory to send data to loghdl");
            Terminate(20);
        }
    }

    strcpy(pcpDataArea,clUrno);
    close_my_cursor(&slCursor);
    slCursor = 0;
    return(ilRc);
}



static int SaveOabs(char *pcpDrrUrno,char *pcpOabs)
{
        int ilRc = RC_SUCCESS;
        char clSelection[124];

        sprintf(clSelection,"WHERE URNO = '%s'",pcpDrrUrno);
        ilRc = DoUpdate("DRRTAB",clSelection,"OABS=:VOABS",pcpOabs);

        return ilRc;
}



static int CheckRegi(char *pcpSurn,char *pcpSday)
{
    char clAcfr[24];
    char clActo[24];
    char clSelection[240];

    int ilRc = RC_SUCCESS;
    sprintf(clAcfr,"%s000000",pcpSday);
    sprintf(clActo,"%s235959",pcpSday);
    LocalTimeToUtcFixTZ(clAcfr);
    sprintf(clSelection,"WHERE REGI IN(%s) AND SURN = '%s' AND "
    " (VPTO >= '%s' OR VPTO = ' ') AND VPFR <= '%s'",
            cgRegi,pcpSurn,clAcfr,clActo);
    ilRc = DoSingleSelect("SRETAB",clSelection,"URNO",cgDataArea);
    if (ilRc != RC_SUCCESS)
    {
        dbg(DEBUG,"No regular shift worker: RC=%d <%s>",ilRc,clSelection);
    }
    else
    {
        dbg(DEBUG,"Is regular shift worker: RC=%d <%s>",ilRc,clSelection);
    }
    return ilRc;
}


static int CheckOrgUnit(char *pcpCode,char *pcpNameOfDepartment)
{
    int ilRc = RC_FAIL;
    char clKey[124];
    long llRow = ARR_FIRST;
    char *pclRowBuf;

    strcpy(clKey,pcpCode);

    pclRowBuf = "vorher";
    dbg(DEBUG,"CheckOrgUnit: Code <%s> <%s>",pcpCode,clKey);
    if(pcpNameOfDepartment != NULL)
    {
        ilRc = CEDAArrayFindRowPointer(&rgOrgArray.rrArrayHandle,rgOrgArray.crArrayName,
        &rgOrgArray.rrIdx02Handle,rgOrgArray.crIdx02Name,clKey,
            &llRow,(void **)&rgOrgArray.pcrArrayRowBuf);

        if (ilRc == RC_SUCCESS)
        {
            strcpy(pcpNameOfDepartment,
                    FIELDVAL(rgOrgArray,rgOrgArray.pcrArrayRowBuf,igOrgDPTN));
            dbg(DEBUG,"CheckOrgUnit: description found <%s> <%s> row %ld",pcpNameOfDepartment,
                    rgOrgArray.pcrArrayRowBuf,llRow);
        }
        else
        {
            *pcpNameOfDepartment = '\0';
            dbg(DEBUG,"CheckOrgUnit: no description found for <%s>",pcpCode);
        }
    }
    return ilRc;
}


static int GetAbsenceRemark(char *pcpCode,char *pcpRema)
{
    int ilRc = RC_SUCCESS;
    char clKey[124];
    char clOdaUrno[124];
    long llRow = ARR_FIRST;

    strcpy(clKey,pcpCode);

    ilRc = CEDAArrayFindRowPointer(&rgOdaArray.rrArrayHandle,rgOdaArray.crArrayName,
    &rgOdaArray.rrIdx02Handle,rgOdaArray.crIdx02Name,clKey,
        &llRow,(void **)&rgOdaArray.pcrArrayRowBuf);
    if (ilRc == RC_SUCCESS)
    {
        strcpy(pcpRema,PFIELDVAL((&rgOdaArray),rgOdaArray.pcrArrayRowBuf,igOdaSDAN));
        strcpy(clOdaUrno,PFIELDVAL((&rgOdaArray),rgOdaArray.pcrArrayRowBuf,igOdaURNO));
      dbg(DEBUG,"GetAbsenceRemark: OdaURNO <%s> Remark: <%s>",clOdaUrno,pcpRema);
    }
    else
    {
        dbg(TRACE,"Absence Code <%s> in ODATAB not found",pcpCode);
        *pcpRema = '\0';
    }

    return ilRc;
}

static int AddToUrnoList(char *pcpUrno,char *pcpList,long lpListLength,long *plpActualLength)
{
    int ilRc = RC_SUCCESS;
    
  int ilNewLen;

    ilNewLen = strlen(pcpUrno) + 1;
    if (((*plpActualLength) + ilNewLen) > (lpListLength+50))
    {
        char *pclTmp;
        pclTmp = realloc(pcpList,lpListLength + 10000);
        if (pclTmp != NULL)
        {
            pcpList = pclTmp;
            lpListLength += 10000;
        }
        else
        {
            dbg(TRACE,"unable to reallocate %ld bytes for Urnolist",
                    lpListLength+50000);
            Terminate(30);
        }
    }

    if (*pcpList != '\0')
    {
        sprintf(&pcpList[*plpActualLength],",'%s'",pcpUrno);
        *plpActualLength += ilNewLen;
    }
    else
    {
        sprintf(&pcpList[*plpActualLength],"'%s'",pcpUrno);
        *plpActualLength += ilNewLen-1;
    }

    dbg(DEBUG,"AddToUrnoList: <%s>",pcpList);

    return ilRc;
}



static void MoveToErrorFile(char *pcpLineBuf)
{
    char clFileName[312];
    FILE *fplErrorFile = NULL;

    sprintf(clFileName,"%s%s%s.err",cgFileBase,cgFileError,cgInputFileName);
  fplErrorFile = fopen (clFileName, "a") ;
  if (fplErrorFile == NULL)
  {
    dbg (TRACE, "MoveToErrorFile: fopen <%s> failed <%d-%s>", clFileName, errno, strerror(errno)) ;
  } 
    else
    {
        fprintf(fplErrorFile,"%s\n",pcpLineBuf);
        fclose(fplErrorFile);
    }
}


static void IncrementCount(int ipRc)
{
        if (ipRc == RC_SUCCESS)
        {
            lgSuccessCount++;
        }
        else
        {
            lgErrorCount++;
            MoveToErrorFile(cgLineBuf);
        }
}

static int ExportThreeDayRoster(FILE *pfpFile,char *pcpSday)
{
    int ilRc = RC_SUCCESS;
    char clSelection[262];
    char clStfUrno[12];
    char clPlfr[24],clPlto[24],clPlSCOD[24],clPeno[36];
    char clAcfr[24],clActo[24];
    long llAction = ARR_FIRST;
    long llDrrPlanAction = ARR_FIRST;
    long llJobAction = ARR_FIRST;
    long llDraAction = ARR_FIRST;
  long llCount = 0;
  long llRowCount = 0;
  long llRow = 0;
    char clSurn[24];
    char clKey[24];
    char clDrrDataArea[1024];
    char clDrrPlanSelection[224];
    char clDrrSelection[224];
    char clDraSelection[224];
    char clJobSelection[224];
    char clStfSelection[224];
    short slFunction,slCursor;
    char *pclRowBuf;
    char *pclDrrRowBuf;
    char clTmpStr[24];
    char clTohr[4];
    struct _DrrData *prlDrr;
    char *pclDrr;
    int ilBytes;
    char clLineBuffer[4096];

    /*ReloadArrays(TRUE);*/


    sprintf(clAcfr,"%s000000",pcpSday);
    sprintf(clActo,"%s235959",pcpSday);
    LocalTimeToUtcFixTZ(clAcfr);
    sprintf(clSelection,"WHERE REGI IN(%s) AND (VPTO >= '%s' OR VPTO = ' ') AND "
            " VPFR <= '%s'",
            cgRegi,clAcfr,clActo);

    slFunction = START;
    slCursor = 0;
    while(DoSelect(slFunction,&slCursor,"SRETAB",clSelection,
                    "DISTINCT SURN",clSurn) == RC_SUCCESS)
    {
        long llRow = ARR_FIRST;
        dbg(TRACE,"SURN <%s> found",clSurn);
        /*StringTrimRight(clSurn);*/
        ilRc = CEDAArrayFindRowPointer(&rgStfArray.rrArrayHandle,
            rgStfArray.crArrayName,&rgStfArray.rrIdx01Handle,
                    rgStfArray.crIdx01Name,clSurn,&llRow,
                            (void **)&rgStfArray.pcrArrayRowBuf);
        slFunction = NEXT;
            if (ilRc == RC_SUCCESS)
            {
                char clDodm[24];
                strcpy(clPeno,FIELDVAL(rgStfArray,rgStfArray.pcrArrayRowBuf,igStfPENO));
                strcpy(clTohr,FIELDVAL(rgStfArray,rgStfArray.pcrArrayRowBuf,igStfTOHR));
                dbg(TRACE,"PENO: <%s> TOHR=<%s> %d %d",clPeno,clTohr,
                    igStfPENO,igStfTOHR);
                if (*clTohr != *cgTohr)
                {
                    continue;
                }
                strcpy(clDodm,FIELDVAL(rgStfArray,rgStfArray.pcrArrayRowBuf,igStfDODM));
                clDodm[8] = '\0';
                if ((strcmp(clDodm,pcpSday) <= 0) && *clDodm != ' ')
                {
                    dbg(DEBUG,"Peno <%s> Not exported because emp. is already "
                            "resigned <%s> SDAY <%s>",clPeno,clDodm,pcpSday);
                    continue;
                }
                    dbg(DEBUG,"Peno <%s> will be exported "
                            "DODM <%s> SDAY <%s>",clPeno,clDodm,pcpSday);
            }
            else
            {
                dbg(TRACE,"Peno not found for SURN <%s>",clSurn);
                continue;
            }
    /*** now search for DRR records ***/
        llRow = ARR_FIRST;

        ilRc = RC_NOTFOUND;
        sprintf(clKey,"%s,A",clSurn);
        while(CEDAArrayFindRowPointer(&rgDrrArray.rrArrayHandle,
            rgDrrArray.crArrayName,&rgDrrArray.rrIdx01Handle,
                    rgDrrArray.crIdx01Name,clKey,&llRow,
                            (void **)&pclDrr) == RC_SUCCESS)
        {
            llRow = ARR_NEXT;
            if(strncmp(pcpSday,
                    FIELDVAL(rgDrrArray,pclDrr,igDrrSDAY),8) == 0)
            {
                ilRc = RC_SUCCESS;
                break;
            }
        }
        llRow = ARR_FIRST;
        if(ilRc != RC_SUCCESS)
        {
            /* we have no actual DRR record found, so we use the level '1' */
            while(CEDAArrayFindRowPointer(&rgDrrArrayP.rrArrayHandle,
            rgDrrArrayP.crArrayName,&rgDrrArrayP.rrIdx01Handle,
                    rgDrrArrayP.crIdx01Name,clSurn,&llRow,
                            (void **)&pclDrr) == RC_SUCCESS)
        {
            llRow = ARR_NEXT;
            if(strncmp(pcpSday,
                    FIELDVAL(rgDrrArrayP,pclDrr,igDrrSDAY),8) == 0)
            {
                ilRc = RC_SUCCESS;
                break;
            }
        }
        }
        if (ilRc == RC_SUCCESS)
        {
            memset(clLineBuffer,0,sizeof(clLineBuffer));
            ilBytes = CreateOneShiftLine(clLineBuffer,pclDrr,clPeno,TRUE,TRUE);
            if (ilBytes > 0)
            {
                if((ilRc  = fwrite(clLineBuffer,sizeof(char),ilBytes,pfpFile)) != ilBytes)
                {
                    dbg(TRACE,"could not write %d Bytes to roster line: <%s>, reason %d <%s>",
                            ilBytes,clLineBuffer,errno,strerror(errno));
                    ilRc = RC_FAIL;
                }
                else
                {
                    ilRc = RC_SUCCESS;
                    llCount++;
                }
            }
        }
        else
        {
            dbg(TRACE,"no shift found for Peno: <%s> Stfu: <%s>",clPeno,clSurn);
        }
    }
    close_my_cursor(&slCursor);
  return llCount;
}

static int GetItem(char* pcpTarget,char* pcpData,char* pcpFieldlist,char* pcpField,int piDeleteBlanks)
{
  int ilRc = RC_FAIL;
  int ilItemNo=0;

  ilItemNo=get_item_no(pcpFieldlist,pcpField,5);
  ilItemNo++;
  ilRc=GetDataItem(pcpTarget,pcpData,ilItemNo,',',"","");
  if(piDeleteBlanks==TRUE)
    {
      DeleteCharacterInString(pcpTarget,cBLANK);
    }
  return ilRc;
}


static int CreateSPRData(char *pcpBuf,char *pcpFields,char *pcpData)
{
    int ilRc = RC_SUCCESS;
    char clPkno[36] = "";
    char clActi[24] = "";
    char clFcol[12] = "";
    char *pclLineBuffer;

    GetItem(clPkno,pcpData,pcpFields,"PKNO",TRUE);
    GetItem(clActi,pcpData,pcpFields,"ACTI",TRUE);
    GetItem(clFcol,pcpData,pcpFields,"FCOL",TRUE);
    
        if (*clFcol == '1')
        {
            *clFcol = 'I';
        }
        else if (*clFcol == '0')
        {
            *clFcol = 'O';
        }
    pclLineBuffer = pcpBuf;
    *pclLineBuffer = '\0';
    
    StringPut(pclLineBuffer,"10",2); /* 00 Record identifier **/
    pclLineBuffer += 2;
    StringPut(pclLineBuffer,clPkno,8); /* 00 Employee Number **/
    pclLineBuffer += 8;
    StringPut(pclLineBuffer,clActi,12); /* 00 Clocking date/time **/
    pclLineBuffer += 12;
    StringPut(pclLineBuffer,clFcol,1); /* 00 Indicator */
    pclLineBuffer += 1;
    StringPut(pclLineBuffer,"\r\n",2); /* 00 Indicator */
    pclLineBuffer += 2;

    return ilRc;
}

static int ImportSPR(char *pcpBuf,BOOL bpIsFromTaa)
{
    int ilRc = RC_SUCCESS;
    long llRow = ARR_FIRST; 
    int ilActualPos = 0;

    char clInsertData[124];
    char clRecIdent[12];
    char clUstf[24];
    char clPeno[24];
    char clClockingTime[24];
    char clClockTimeForTest[24];
    char clIndicator[6];
    char clRosterStart[22];
    char clRosterEnd[22];
    char clSelection[220];

    /* Record Identifier always 10 */
    strcpy(cgLineBuf,pcpBuf);
    StringMid(clRecIdent,pcpBuf,ilActualPos,2,CLIENTTODB);
    ilActualPos += 2;
    if (strcmp(clRecIdent,"10") == 0)
    {
        StringMid(clPeno,pcpBuf,ilActualPos,8,CLIENTTODB);
        ilActualPos += 8;
        StringMid(clClockingTime,pcpBuf,ilActualPos,12,CLIENTTODB);
        ilActualPos += 12;
        sprintf(clClockTimeForTest,"%s00",clClockingTime);
        if(CheckDateFormat(clClockTimeForTest) != RC_SUCCESS)
        {
            sprintf(cgErrorString,"invalid date format in clocking time <%s>",clClockingTime);
            LogError(cgInputFilePath,cgErrorString,TRUE);
            return(RC_FAIL);
    }

        StringMid(clIndicator,pcpBuf,ilActualPos,1,CLIENTTODB);
        if (*clIndicator == 'I')
        {
            *clIndicator = '1';
        }
        else if (*clIndicator == 'O')
        {
            *clIndicator = '0';
        }
        else
        {
            dbg(TRACE,"ImportSPR: invalid Indicator <%s> should be 'O' or 'I'",
                    clIndicator);
            sprintf(cgErrorString,"invalid Indicator <%s> should be 'O' or 'I'",
                    clIndicator);
            LogError("T&A",cgErrorString,TRUE);
            ilRc = RC_FAIL;
        }

        strcat(clClockingTime,"00");
        if (bpIsFromTaa == FALSE)
        {
        UtcToLocalTimeFixTZ(clClockingTime);
    }

        if (ilRc == RC_SUCCESS)
        {
            char clKey[24];

            sprintf(clKey,"%-20s",clPeno);

        StringTrimRight(clKey);

        ilRc = CEDAArrayFindRowPointer(&rgStfArray.rrArrayHandle,
            rgStfArray.crArrayName,&rgStfArray.rrIdx02Handle,
                    rgStfArray.crIdx02Name,clKey,&llRow,
                            (void **)&rgStfArray.pcrArrayRowBuf);
            if (ilRc == RC_SUCCESS)
            {
                strcpy(clUstf,FIELDVAL(rgStfArray,rgStfArray.pcrArrayRowBuf,igStfURNO));
                dbg(TRACE,"ImportSPR PENO: <%s>",clPeno);

                if(*clIndicator == '1')
                {
            /** employee found, now search shift **/
            /** check clocking time for clock in only */
                char clKey[32];

                ilRc = RC_NOTFOUND;
                llRow = ARR_FIRST;
                sprintf(clKey,"%s,A",clUstf);
                while(CEDAArrayFindRowPointer(&(rgDrrArray.rrArrayHandle),
                                                                            &(rgDrrArray.crArrayName[0]),
                                                                            &(rgDrrArray.rrIdx01Handle),
                                                                            &(rgDrrArray.crIdx01Name[0]),
                                                                            clKey,&llRow,
                                                                            (void **)&rgDrrArray.pcrArrayRowBuf) == RC_SUCCESS)
                    {
                        char clScod[24];

                        llRow = ARR_NEXT;

                        strcpy(clScod,FIELDVAL(rgDrrArray,rgDrrArray.pcrArrayRowBuf,
                                    igDrrSCOD));
                        StringTrimRight(clScod);
                        if (strstr("OFF,RD",clScod) != NULL)
                        {
                            dbg(TRACE,"Shift is <%s>, continue",clScod);
                            continue;
                        }
                        else
                        {
                            dbg(TRACE,"Shift is <%s> ",clScod);
                        }
                        strcpy(clRosterStart,FIELDVAL(rgDrrArray,rgDrrArray.pcrArrayRowBuf,igDrrAVFR));
                        strcpy(clRosterEnd,FIELDVAL(rgDrrArray,rgDrrArray.pcrArrayRowBuf,igDrrAVTO));
                    
                        AddSecondsToCEDATime(clRosterStart,(time_t)igClockInTolerance,1);
                        if (ISBETWEEN(clRosterStart,clRosterEnd,clClockingTime))
                        {
                            dbg(DEBUG,"clock in at %s is allowed for shift <%s> from (%s)%s to %s",
                                    clClockingTime,FIELDVAL(rgDrrArray,rgDrrArray.pcrArrayRowBuf,igDrrURNO),
                                    clRosterStart,
                                    FIELDVAL(rgDrrArray,rgDrrArray.pcrArrayRowBuf,igDrrAVFR),
                                    FIELDVAL(rgDrrArray,rgDrrArray.pcrArrayRowBuf,igDrrAVTO));
                            ilRc = RC_SUCCESS;
                            break;
                        }
                        else
                        {
                            dbg(DEBUG,"clock in at %s is not allowed for shift <%s> from %s to %s",
                                    clClockingTime,FIELDVAL(rgDrrArray,rgDrrArray.pcrArrayRowBuf,igDrrURNO),
                                    FIELDVAL(rgDrrArray,rgDrrArray.pcrArrayRowBuf,igDrrAVFR),
                                    FIELDVAL(rgDrrArray,rgDrrArray.pcrArrayRowBuf,igDrrAVTO));
                        }
                    }
            }
            else
            {
            /** employee found, now search shift **/
            /** check clocking time for clock out */
                char clKey[32];

                ilRc = RC_NOTFOUND;
                llRow = ARR_FIRST;
                sprintf(clKey,"%s,A",clUstf);
                while(CEDAArrayFindRowPointer(&(rgDrrArray.rrArrayHandle),
                                                                            &(rgDrrArray.crArrayName[0]),
                                                                            &(rgDrrArray.rrIdx01Handle),
                                                                            &(rgDrrArray.crIdx01Name[0]),
                                                                            clKey,&llRow,
                                                                            (void **)&rgDrrArray.pcrArrayRowBuf) == RC_SUCCESS)
                    {
                        char clScod[24];

                        llRow = ARR_NEXT;

                        strcpy(clScod,FIELDVAL(rgDrrArray,rgDrrArray.pcrArrayRowBuf,
                                    igDrrSCOD));
                        StringTrimRight(clScod);
                        if (strstr("OFF,RD",clScod) != NULL)
                        {
                            dbg(TRACE,"Shift is <%s>, continue",clScod);
                            continue;
                        }
                        else
                        {
                            dbg(TRACE,"Shift is <%s> ",clScod);
                        }
                        
                        strcpy(clRosterStart,FIELDVAL(rgDrrArray,rgDrrArray.pcrArrayRowBuf,igDrrAVFR));
                        strcpy(clRosterEnd,FIELDVAL(rgDrrArray,rgDrrArray.pcrArrayRowBuf,igDrrAVTO));
                        
                        AddSecondsToCEDATime(clRosterEnd,(time_t)igClockOutTolerance,1);
                        if (ISBETWEEN(clRosterStart,clRosterEnd,clClockingTime))
                        {
                            dbg(DEBUG,"clock out at %s is allowed for shift <%s> from %s to %s",
                                    clClockingTime,FIELDVAL(rgDrrArray,rgDrrArray.pcrArrayRowBuf,igDrrURNO),
                                    FIELDVAL(rgDrrArray,rgDrrArray.pcrArrayRowBuf,igDrrAVFR),
                                    FIELDVAL(rgDrrArray,rgDrrArray.pcrArrayRowBuf,igDrrAVTO));
                            ilRc = RC_SUCCESS;
                            break;
                        }
                        else
                        {
                            dbg(DEBUG,"clock out at %s is not allowed for shift <%s> from %s to %s",
                                    clClockingTime,FIELDVAL(rgDrrArray,rgDrrArray.pcrArrayRowBuf,igDrrURNO),
                                    FIELDVAL(rgDrrArray,rgDrrArray.pcrArrayRowBuf,igDrrAVFR),
                                    FIELDVAL(rgDrrArray,rgDrrArray.pcrArrayRowBuf,igDrrAVTO));
                        }
                    }
            }


            if (ilRc == RC_NOTFOUND)
            {
                sprintf(cgErrorString,"no valid shift found for <%s> at <%s>",
                    clPeno,clClockingTime);
                LogError("T&A",cgErrorString,TRUE);
            }
    
        if(ilRc == RC_SUCCESS)
        {
                char clSday[24];

                if (bpIsFromTaa == TRUE)
                {
                    LocalTimeToUtcFixTZ(clClockingTime);
                    /*SPR_INSERT_FIELDS "ACTI,FCOL,PKNO,USTF"*/
                    sprintf(clInsertData,"%s,%s,%s,%s",clClockingTime,clIndicator,clPeno,clUstf);
                    ilRc = DoInsert("SPRTAB","ACTI,FCOL,PKNO,USTF",
                        ":VACTI,:VFCOL,:VPKNO,:VUSTF",clInsertData);
                }
                strcpy(clSday,FIELDVAL(rgDrrArray,rgDrrArray.pcrArrayRowBuf,igDrrSDAY));
                if(*clIndicator == '0')
                {
                    char clData[24];

                    sprintf(clData,"%s",FIELDVAL(rgDrrArray,rgDrrArray.pcrArrayRowBuf,igDrrURNO));
                    SingleRosterExport("DRRTAB"," ","URNO",clData);
                }
                
        }
    }
    else
        {
                dbg(TRACE,"ImportSPR: Urno not found for PENO <%s> RC=%d",clKey,ilRc);
        sprintf(cgErrorString,"Employee unknown <%s>",clKey);
            LogError("T&A",cgErrorString,TRUE);
            }
    }
    }
    else
    {
        dbg(TRACE,"ImportSPR: invalid record identifier <%s>",pcpBuf);
        sprintf(cgErrorString,"invalid record identifier <%s>",pcpBuf);
            LogError("T&A",cgErrorString,TRUE);
        ilRc = RC_FAIL;
    }

    return ilRc;
}
    
static int UpdateArraysFromEvent(EVENT *prpEvent,char *pcpUrnoList,
        char *pcpTable,char *pcpCmdblkTable)
{
    int    ilRc           = RC_SUCCESS;         /* Return code */

    
    ilRc = CEDAArrayEventUpdate2(prpEvent,pcpUrnoList,pcpTable);
    if(strcmp(pcpCmdblkTable,"JOBTAB") == 0)
    {
        strcpy(pcpCmdblkTable,"JAVTAB");
        strcpy(pcpTable,"JAVTAB");
        CEDAArrayEventUpdate2(prpEvent,pcpUrnoList,pcpTable);
    }

    return ilRc;
}

/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(EVENT *prpEvent)
{
    int    ilRc           = RC_SUCCESS;         /* Return code */
    int      ilCmd          = 0;

    BC_HEAD *prlBchd       = NULL;
    CMDBLK  *prlCmdblk       = NULL;
    char    *pclSelection    = NULL;
    char    *pclFields       = NULL;
    char    *pclData         = NULL;
    char    *pclRow          = NULL;
    char        clTable[34];
    char    clTmpJob[12];
    char    clOabs[12] = "";
    char    clUrnoList[500];
    char clDrrUrno[24];

    prlBchd    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
    prlCmdblk    = (CMDBLK *)  ((char *)prlBchd->data);
    pclSelection = prlCmdblk->data;
    pclFields    = pclSelection + strlen(pclSelection) + 1;
    pclData      = pclFields + strlen(pclFields) + 1;

    strcpy(clTable,prlCmdblk->obj_name);

    dbg(TRACE,"==========START <%10.10d> ==========",lgEvtCnt);

  igQueOut = prgEvent->originator;

  /* Save Global Elements of CMDBLK */
  strcpy(cgTwStart,prlCmdblk->tw_start);
  strcpy(cgTwEnd,prlCmdblk->tw_end);
  /* Save Global Elements of BC_HEAD */
  strcpy(cgRecvName,prlBchd->recv_name);
  strcpy(cgDestName,prlBchd->dest_name);
 
    /****************************************/
    DebugPrintBchead(DEBUG,prlBchd);
    DebugPrintCmdblk(DEBUG,prlCmdblk);
  dbg(DEBUG,"TwStart   <%s>",cgTwStart);
  dbg(DEBUG,"TwEnd     <%s>",cgTwEnd);
    dbg(DEBUG,"originator<%d>",prpEvent->originator);
    dbg(DEBUG,"selection <%s>",pclSelection);
    dbg(DEBUG,"fields    <%s>",pclFields);
    dbg(DEBUG,"data      <%s>",pclData);
    /****************************************/

  
    memset(pcgStfuList,0,lgStfuListLength);
    lgStfuListActualLength = 0;

    ilRc = GetCommand(&prlCmdblk->command[0],&ilCmd);
    dbg(DEBUG,"RC from GetCommand(%s/%d) is %d",&prlCmdblk->command,&ilCmd,ilRc);
    if(ilRc == RC_SUCCESS)
    {
        switch (ilCmd)
        {
    case CMD_SPR :
                dbg(TRACE,"Command is SPR");
                ImportSPR(pclData,TRUE);
             break;
    case CMD_TAA :
                dbg(TRACE,"Command is TAA");
                ReloadArrays(TRUE);
                ThreeDayRosterExport();
                ReloadArrays(FALSE);
             break;
        case CMD_IRT :
                { /* delete old data from pclData */
                    char *pclNewline = NULL;

                    if ((pclNewline = strchr(pclData,'\n')) != NULL)
                    {
                        *pclNewline = '\0';
                    }
                }           
                if(strcmp(clTable,"SPRTAB") == 0)
                {
                    /* special handling for SPRTAB */
                    char clLineBuf[248];

                    CreateSPRData(clLineBuf,pclFields,pclData);
                    ImportSPR(clLineBuf,FALSE);
                }
                else
                {
                    BOOL blExport = TRUE;

                    if (strcmp(clTable,"DRRTAB") == 0)
                    {
                        char clRosl[24];
                        char clAvfr[24];
                        char clAvto[24];
                        int ilItemNo;

                        ilItemNo = get_item_no(pclFields,"AVFR",5) + 1;

                        if (ilItemNo > 0)
                        {
                            get_real_item(clAvfr, pclData, ilItemNo);
                            ilItemNo = get_item_no(pclFields,"AVTO",5) + 1;
                            if (ilItemNo > 0)
                            {
                                get_real_item(clAvto, pclData, ilItemNo);
                                clAvfr[8] = '\0';
                                clAvto[8] = '\0';
                                if (OVERLAP(clAvfr,clAvto,cgStartDay,cgEndDay))
                                {

                                    ilRc = GetItem(clRosl,pclData,pclFields,"ROSL",TRUE);
                                    if (ilRc == RC_SUCCESS && 
                                    ((strcmp(clRosl,"4") == 0) || (strcmp(clRosl,"3") == 0)))
                                    {
                                        dbg(DEBUG,"Insert into DRRTAB for level %s, don't export",
                                            clRosl);
                                        blExport = FALSE;
                                    }

                                    ilRc = UpdateArraysFromEvent(prgEvent,clUrnoList,clTable,
                                            prlCmdblk->obj_name);
                                    dbg(TRACE,"Command is IRT RC from EventUpdate=%d",ilRc);
                                    if ((blExport == TRUE)&& (ilRc == RC_SUCCESS))
                                    {
                                            SingleRosterExport(clTable,pclSelection,pclFields,pclData);
                                    }
                                }
                                else
                                {
                                    dbg(DEBUG,"shift not exported due to not overlapping loading period: shift start/end <%s/%s> loading period <%s>/%s>",clAvfr,clAvto,cgStartDay,cgEndDay);
                                }
                            }
                        }
                    }
                    else
                    {
                        ilRc = UpdateArraysFromEvent(prgEvent,clUrnoList,clTable,
                            prlCmdblk->obj_name);
                    }
                }
            break;
    case CMD_URT :


            if (strcmp(clTable,"DRRTAB") == 0)
            {
                char clScod[24];
                char clSday[24];
                char clNewValue[24];
                char clOldValue[24];
                char *pclOldData;
                char clDrs2[12];
                char clOldDrs2[12];
                BOOL blDrrRelevantChange = TRUE;
                int ilItemNo;

                if ((pclOldData = strchr(pclData,'\n')) != NULL)
                {
                    *pclOldData = '\0';
                    pclOldData++;

                    ilItemNo = get_item_no(pclFields,"SCOD",5) + 1;

                    if (ilItemNo > 0)
                    {
                        get_real_item(clNewValue, pclData, ilItemNo);
                        get_real_item(clOldValue, pclOldData, ilItemNo);

                        dbg(DEBUG,"new SCOD <%s>, old SCOD <%s>",clNewValue,clOldValue);
                        if (strcmp(clNewValue,clOldValue) != NULL)
                        {
                            /* shift code has changed */
                            if (strstr("OFF,RD",clOldValue) != NULL)
                            {
                                long llAction = ARR_FIRST;
                                /* old shift code is OFF or RD Day */
                                dbg(DEBUG,"%05d: old SCOD is <%s>",__LINE__,clOldValue);
                                ilRc = CEDAArrayFindRowPointer(&rgBsdArray.rrArrayHandle,
                                        rgBsdArray.crArrayName,&rgBsdArray.rrIdx02Handle,
                                            rgBsdArray.crIdx02Name,clNewValue,&llAction,
                                                (void **)&rgBsdArray.pcrArrayRowBuf);

                                if (ilRc == RC_SUCCESS)
                                {
                                    dbg(DEBUG,"%05d: OFF/RD recall found ",__LINE__);
                                    ilItemNo = get_item_no(pclFields,"URNO",5) + 1;

                                    if (ilItemNo > 0)
                                    {
                                        get_real_item(clDrrUrno, pclData, ilItemNo);
                                        strcpy(clOabs,clNewValue);
                                        SaveOabs(clDrrUrno,clOldValue);
                                    }
                                    else
                                    {
                                        dbg(TRACE,"%05d: saving OABS not possible due to "
                                                " missing DRR-URNO in message",__LINE__);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            { /* delete old data from pclData  for DRRTAB this
                     is already done above */
                char *pclNewline = NULL;

                if ((pclNewline = strchr(pclData,'\n')) != NULL)
                {
                    *pclNewline = '\0';
                }
            }

            if(strcmp(clTable,"SPRTAB") == 0)
            {
                /* special handling for SPRTAB */
                char clLineBuf[248];

                CreateSPRData(clLineBuf,pclFields,pclData);
                ImportSPR(clLineBuf,FALSE);
            }
            else
            {
                ilRc = UpdateArraysFromEvent(prgEvent,clUrnoList,clTable,
                        prlCmdblk->obj_name);
                dbg(TRACE,"Command is URT RC from EventUpdate=%d",ilRc);
                if (ilRc == RC_SUCCESS && strcmp(clTable,"DRRTAB") == 0)
                {
                    if ((*clOabs != '\0') && (*clDrrUrno != '\0'))
                    {
                        long llOabs = ARR_FIRST;
                        char clDrrSelection[124];
                        long llRowCount;

                        /* change the OABS to new value before export to T&A is made */
                        long llRow = ARR_FIRST;
                        ilRc = CEDAArrayFindRowPointer(&(rgDrrArray.rrArrayHandle),
                                                                        &(rgDrrArray.crArrayName[0]),
                                                                        &(rgDrrArray.rrIdx02Handle),
                                                                        &(rgDrrArray.crIdx02Name[0]),
                                                                        clDrrUrno,&llRow,
                                                                        (void **)&rgDrrArray.pcrArrayRowBuf);

                        AATArrayDeleteRow(&(rgDrrArray.rrArrayHandle),
                                                                        &(rgDrrArray.crArrayName[0]),llRow);

                        CEDAArrayGetRowCount(&rgDrrArray.rrArrayHandle,
                                rgDrrArray.crArrayName,&llRowCount);
                        dbg(DEBUG,"%d shifts loaded before refill",llRowCount);
                                            sprintf(clDrrSelection,"WHERE URNO = '%s' ",clDrrUrno);
                        CEDAArrayRefill(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,
                                                    clDrrSelection,NULL,ARR_NEXT);
                        CEDAArrayGetRowCount(&rgDrrArray.rrArrayHandle,
                                rgDrrArray.crArrayName,&llRowCount);
                        dbg(DEBUG,"%d shifts loaded after refill",llRowCount);

                                            /****************
                        if (ilRc == RC_SUCCESS)
                        {
                            CEDAArrayPutField(&rgDrrArray.rrArrayHandle,
                                rgDrrArray.crArrayName,&llOabs,"OABS",llRow,clOabs);
                        }
                        else
                        {
                            dbg(TRACE,"%05d: DRR record not found in array. "
                            " Can not update OABS",__LINE__);
                        }
                        *****************/

                    }
                    SingleRosterExport(clTable,pclSelection,pclFields,pclData);
                }
            }
            break;
    case CMD_DRT:
            dbg(TRACE,"Command is DRT");
            ilRc = UpdateArraysFromEvent(prgEvent,clUrnoList,clTable,
                    prlCmdblk->obj_name);
            if(strcmp(clTable,"DRATAB") != 0)
            {
                if (bgIsTaahdl)
                {
                /*ilRc = UpdateArraysFromEvent(prgEvent,clUrnoList,clTable);*/
                SingleRosterExport(clTable,pclSelection,pclFields,pclData);
                }
                else
                {
                    dbg(TRACE,"DRT command received, but will not process it because its TAAFIL");
                }
            }
            break;
    case CMD_SBC:
            dbg(TRACE,"Command is SBC");
            if(strcmp(clTable,"RELDRR") == 0)
            {
                if (bgIsTaahdl)
                {
                    dbg(TRACE,"RELDRR command received");
                    HandleRelDrr(pclData);
                }
                else
                {
                    dbg(TRACE,"RELDRR command received, but will not process it because its TAAFIL");
                }
            }
            break;
            case CMD_BR1:
            case CMD_BR2:
            case CMD_BR3:
            case CMD_BR4:
                BreakRosterExport(ilCmd);
                break;
    default:
            dbg(TRACE,"unknown command <%s>",prlCmdblk->command);
        }
    }

    dbg(TRACE,"==========END  <%10.10d> ==========",lgEvtCnt);

    return(RC_SUCCESS);
    
} /* end of HandleData */

static int HandleRelDrr(char *pcpBCData)
{
    int     ilRC, ilItemNo=1, ilLen;
    char    *pclBuff;
    char    clDel='-';
    char    clStart[15]="", clEnd[15]="", clRelLevels[11], *pclSelection;
    int     ilRC1, ilRC2, ilLevel1=0, ilLevel2=0;
    char    clSday1[9]="", clSday2[9]="", clUstf[11], clUdrr[11];
    long    llRowCount=0, llRowNum = ARR_FIRST, llFunc;
    char    *pclRow=0, pclOldRow=0;

    if ( !pcpBCData )
        return RC_FAIL;
    ilRC = RC_SUCCESS;
    dbg(DEBUG,"HandleRelDrr: Data <%s>", pcpBCData );

    pclBuff = (char*)calloc ( 1, strlen(pcpBCData)+1 ) ;
    ilLen = strlen(pcpBCData) * 3 / 2 + 130;
    pclSelection = (char*)calloc ( 1, ilLen ) ;
    if ( !pclBuff || !pclSelection )
    {
        dbg(TRACE,"HandleRelDrr: calloc of <%d> bytes failed", ilLen );
        if ( pclBuff )
            free ( pclBuff );
        return RC_NOMEM;
    }
    else
    {
        dbg (DEBUG, "HandleRelDrr: Data len <%d> now allocated <%d>", 
                    strlen(pcpBCData), ilLen );
    }

    /* Check release dates, maybe nothing to be done */
    if ( ilRC == RC_SUCCESS) 
    {
        if ( GetDataItem(pclBuff, pcpBCData, 1, clDel, "", "\0\0") > 0)
        {
            strncpy ( clStart, pclBuff, 8 );
            clStart[8] = '\0';
            strcpy ( clSday1, clStart );
        }
        if ( GetDataItem(pclBuff, pcpBCData, 2, clDel, "", "\0\0") > 0)
        {
            strncpy ( clEnd, pclBuff, 8 );
            clEnd[8] = '\0';
            strcpy ( clSday2, clEnd );
        }
        if ( (strlen(clEnd)<8) || (strlen (clStart)<8) )
            ilRC = RC_INIT_FAIL;
    }
    dbg(DEBUG,"HandleRelDrr: start  <%s> end <%s>",clStart,clEnd);

    if (ilRC == RC_SUCCESS && OVERLAP(clStart,clEnd,cgStartDay,cgEndDay))
    {
        char clStfuList[9000] = "";
        char *pclStfuList;
        int ilNoStfus = 0;

        pclStfuList = clStfuList;
        if ( GetDataItem(pclBuff, pcpBCData, 3, clDel, "", "\0\0") > 0)
        {
            char clStfu[24];
            char *pclRow;
            int ilItemNo;
            long llFunc = ARR_FIRST;

            ConvertDbStringToClient(pclBuff);

            dbg(DEBUG,"HandleRelDrr: Buff = <%s>",pclBuff);


            for (ilItemNo = 1; 
                    GetDataItem(clStfu,pclBuff,ilItemNo,',',"","") > 0;
                            ilItemNo++)
          {
                dbg(DEBUG,"HandleRelDrr: now searching shifts for: <%s>",clStfu);
                if (ilNoStfus > 0)
                {
                    *pclStfuList = ',';
                    pclStfuList++;
                }
                *pclStfuList = '\'';
                pclStfuList++;
                strcpy(pclStfuList,clStfu);
                pclStfuList += strlen(clStfu);
                *pclStfuList = '\'';
                pclStfuList++;
                *pclStfuList = 0;
                ilNoStfus++;

                llFunc = ARR_FIRST;
                while(CEDAArrayFindRowPointer( &(rgDrrArray.rrArrayHandle),
                                          rgDrrArray.crArrayName,
                                          &(rgDrrArray.rrIdx01Handle),
                                          rgDrrArray.crIdx01Name,
                                          clStfu, &llFunc, (void *) &pclRow ) == RC_SUCCESS )
                {
                    ilRC1 = AATArrayDeleteRow(&(rgDrrArray.rrArrayHandle),
                                      rgDrrArray.crArrayName, llFunc );
                    llFunc = ARR_FIRST;
                    dbg ( TRACE, "HandleRelDrr: DRR-record found for STFU=%s, delete from Array", clStfu ); 
                }

                while(CEDAArrayFindRowPointer( &(rgDrrArrayP.rrArrayHandle),
                                          rgDrrArrayP.crArrayName,
                                          &(rgDrrArrayP.rrIdx01Handle),
                                          rgDrrArrayP.crIdx01Name,
                                          clStfu, &llFunc, (void *) &pclRow ) == RC_SUCCESS )
                {
                    ilRC1 = AATArrayDeleteRow(&(rgDrrArrayP.rrArrayHandle),
                                  rgDrrArrayP.crArrayName, llFunc );
                    llFunc = ARR_FIRST;
                    dbg ( TRACE, "HandleRelDrr: DRR-record found for STFU=%s, delete from Array", clStfu ); 
                }
                if (ilNoStfus > 500)
                {
                    ReloadDrrArrays(clStfuList);
                    ilNoStfus = 0;
                    pclStfuList = clStfuList;
                }
            }
        }
        if (ilNoStfus > 0)
        {
            ReloadDrrArrays(clStfuList);
        }
    }       
    return ilRC;
}


static int StrToTime(char *pcpTime,time_t *plpTime)
{
    struct tm *_tm;
    time_t now;
    char   _tmpc[6];
    long llUtcDiff;

    dbg(0,"StrToTime, <%s>",pcpTime);
    if (strlen(pcpTime) < 12 )
    {
        *plpTime = time(0L);
        return RC_FAIL;
    } /* end if */




    now = time(0L);
    _tm = (struct tm *)gmtime(&now);

    _tmpc[2] = '\0';
/*
    strncpy(_tmpc,pcpTime+12,2);
    _tm -> tm_sec = atoi(_tmpc);
*/
    _tm -> tm_sec = 0;
    
    strncpy(_tmpc,pcpTime+10,2);
    _tm -> tm_min = atoi(_tmpc);
    strncpy(_tmpc,pcpTime+8,2);
    _tm -> tm_hour = atoi(_tmpc);
    strncpy(_tmpc,pcpTime+6,2);
    _tm -> tm_mday = atoi(_tmpc);
    strncpy(_tmpc,pcpTime+4,2);
    _tm -> tm_mon = atoi(_tmpc)-1;
    strncpy(_tmpc,pcpTime,4);
    _tmpc[4] = '\0';
    _tm ->tm_year = atoi(_tmpc)-1900;
    _tm ->tm_wday = 0;
    _tm ->tm_yday = 0;
    _tm->tm_isdst = -1; /* no adjustment of daylight saving time */
    now = mktime(_tm);
    dbg(0,"StrToTime: %02d.%02d.%04d %02d:%02d Now: %ld",_tm->tm_mday,
_tm->tm_mon+1,_tm->tm_year+1900,_tm->tm_hour,_tm->tm_min,now);
    if (now != (time_t) -1)
    {
    {
        time_t utc,local;

    _tm = (struct tm *)gmtime(&now);
    utc = mktime(_tm);
    _tm = (struct tm *)localtime(&now);
    local = mktime(_tm);
    dbg(TRACE,"utc %ld, local %ld, utc-diff %ld",
        utc,local,local-utc);
        llUtcDiff = local-utc;
    }
        now +=  + llUtcDiff;
        *plpTime = now;
        return RC_SUCCESS;
    }
    *plpTime = time(NULL);
    return RC_FAIL;
}


/**
dbg(TRACE,"%05d:",__LINE__);
***/



/*
 *     struct  tm {
 *             int     tm_sec;    seconds after the minute -- [0, 59]  
 *             int     tm_min;    minutes after the hour -- [0, 59] 
 *             int     tm_hour;   hour since midnight -- [0, 23] 
 *             int     tm_mday;   day of the month -- [1, 31] 
 *             int     tm_mon;    months since January -- [0, 11] 
 *             int     tm_year;   years since 1900 
 *             int     tm_wday;   days since Sunday -- [0, 6] 
 *             int     tm_yday;   days since January 1 -- [0, 365] 
 *             int     tm_isdst;  flag for daylight savings time 
 *             long    tm_tzadj;  seconds from GMT (east < 0) 
 *             char    tm_name[LTZNMAX];   name of timezone 
 *     };
 */




void    StrAddTime(char *pcpBase,char *pcpOffs,char *pcpDest)
{

    if (pcpBase != pcpDest)
    {
        strcpy(pcpDest,pcpBase);
    }
    AddSecondsToCEDATime(pcpDest,(time_t)atol(pcpOffs),1);

} /* end of StrAddTime */



int check_ret1(int ipRc,int ipLine)
{
    int ilOldDebugLevel;

    ilOldDebugLevel = debug_level;

    debug_level = TRACE;

    dbg(TRACE,"DB-ERROR in Line: %d",ipLine);
    debug_level  = ilOldDebugLevel;
    return check_ret(ipRc);
}

static void HandleQueues()
{
    int ilRc = RC_SUCCESS;          /* Return code */
    int ilBreakOut = FALSE;
    
    do
    {
        ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text; 

        if( ilRc == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRc != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRc);
            } /* fi */
        
            switch( prgEvent->command )
            {
            case    HSB_STANDBY :
                ctrl_sta = prgEvent->command;
                break;  
    
            case    HSB_COMING_UP   :
                ctrl_sta = prgEvent->command;
                break;  
    
            case    HSB_ACTIVE  :
                ctrl_sta = prgEvent->command;
                ilBreakOut = TRUE;
                break;  

            case    HSB_ACT_TO_SBY  :
                ctrl_sta = prgEvent->command;
                break;  
    
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate(1);
                break;  
    
            case    HSB_STANDALONE  :
                ctrl_sta = prgEvent->command;
                ResetDBCounter();
                ilBreakOut = TRUE;
                break;  

            case    REMOTE_DB :
                /* ctrl_sta is checked inside */
                HandleRemoteDB(prgEvent);
                break;

            case    SHUTDOWN    :
                Terminate(1);
                break;
                        
            case    RESET       :
                ilRc = Reset();
                break;
                        
            case    EVENT_DATA  :
                dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
                    
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;

            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;

            default         :
                dbg(TRACE,"HandleQueues: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        } else {
            /* Handle queuing errors */
            HandleQueErr(ilRc);
        } /* end else */
    } while (ilBreakOut == FALSE);

    if(igInitOK == FALSE)
    {
            ilRc = InitTaahdl();
            if(ilRc != RC_SUCCESS)
            {
                dbg(TRACE,"InitTaahdl failed!");
            } /* end of if */
    }/* end of if */

}

static int TriggerAction(char *pcpTableName,char *pcpFields)
{
  int           ilRc          = RC_SUCCESS ;    /* Return code */
  EVENT        *prlEvent      = NULL ;
  BC_HEAD      *prlBchead     = NULL ;
  CMDBLK       *prlCmdblk     = NULL ;
  /*ACTIONConfig *prlAction     = NULL ;*/
    ACTIONConfig rlAction;
  char         *pclSelection  = NULL ;
  char         *pclFields     = NULL ;
  char         *pclData       = NULL ;
  long          llActSize     = 0 ;
  int           ilAnswerQueue = 0 ;
  char          clQueueName[16] ;
    
  if(ilRc == RC_SUCCESS)
  {

  }/* end of if */
    

  if (ilRc == RC_SUCCESS)
  {
   sprintf (&clQueueName[0],"%s2", mod_name) ;

   ilRc = GetDynamicQueue(&ilAnswerQueue,&clQueueName[0]);
   if (ilRc != RC_SUCCESS)
   {
    dbg(TRACE,"TriggerAction: GetDynamicQueue failed <%d>",ilRc);
   }
   else
   {
    dbg(DEBUG,"TriggerAction: got <%d> for <%s>",ilAnswerQueue, &clQueueName[0]);
   }/* end of if */
  }/* end of if */
    
  if (ilRc == RC_SUCCESS)
  {
   llActSize = sizeof(EVENT) + sizeof(BC_HEAD) + 
            sizeof(CMDBLK) + sizeof(ACTIONConfig) + strlen(pcpFields) + 666;
   prgOutEvent = realloc(prgOutEvent,llActSize);
   if(prgOutEvent == NULL)
   {
    dbg(TRACE,"TriggerAction: realloc out event <%d> bytes failed",llActSize);
    ilRc = RC_FAIL ;
   } /* end of if */
  } /* end of if */

  if(ilRc == RC_SUCCESS)
  {
   prlBchead     = (BC_HEAD *) ((char *) prgOutEvent + sizeof(EVENT)) ;
   prlCmdblk     = (CMDBLK *)  ((char *) prlBchead->data) ;
   pclSelection  = prlCmdblk->data ;
   pclFields     = pclSelection + strlen (pclSelection) + 1 ;
     strcpy(pclFields,pcpFields);
   pclData       = pclFields + strlen (pclFields) + 1 ;
   strcpy (pclData, "DYN") ;
    memset(&rlAction,0,sizeof(ACTIONConfig));
   /*prlAction     = (ACTIONConfig *) (pclData + strlen(pclData) + 1) ;*/
    
    prlBchead->rc = RC_SUCCESS;
    
   prgOutEvent->type        = USR_EVENT ;
   prgOutEvent->command     = EVENT_DATA ;
   prgOutEvent->originator  = ilAnswerQueue ;
   prgOutEvent->retry_count = 0 ;
   prgOutEvent->data_offset = sizeof(EVENT) ;
   prgOutEvent->data_length = llActSize-sizeof(EVENT) ;

    rlAction.iEmptyFieldHandling = iDELETE_ALL_BLANKS ; 
    rlAction.iIgnoreEmptyFields = 0 ; 
    strcpy(rlAction.pcSndCmd, "") ; 
    sprintf(rlAction.pcSectionName,"%s_%s", mod_name,pcpTableName) ; 
    strcpy(rlAction.pcTableName, pcpTableName) ; 
    strcpy(rlAction.pcFields, pcpFields) ; 
  strcpy(rlAction.pcSectionCommands, "IRT,URT,DRT") ;
    rlAction.iModID = mod_id ;   

     /***
   prlAction->iEmptyFieldHandling = iDELETE_ALL_BLANKS ;
   prlAction->iIgnoreEmptyFields = 0 ;
   strcpy(prlAction->pcSndCmd, "") ;
   sprintf(prlAction->pcSectionName,"%s_%s", mod_name,pcpTableName) ;
   strcpy(prlAction->pcTableName, pcpTableName) ;
   strcpy(prlAction->pcFields, pcpFields) ;
   strcpy(prlAction->pcSectionCommands, "IRT,URT,DRT") ;
   prlAction->iModID = mod_id ;
     ***/
     /*********************** MCU TEST **************
   strcpy(prlAction->pcFields, "-") ;
   strcpy(prlAction->pcFields, "") ;
     *********************** MCU TEST ***************/
     /***********
     if (!strcmp(pcpTableName,"SPRTAB"))
        {
    strcpy(prlAction->pcFields, "PKNO,USTF,ACTI,FCOL") ;
     }
     ************/

   /****************************************
     DebugPrintEvent(DEBUG,prgOutEvent) ;
     DebugPrintBchead(DEBUG,prlBchead) ;
     DebugPrintCmdblk(DEBUG,prlCmdblk) ;
     dbg (DEBUG,"TriggerAction: selection <%s>",pclSelection) ;
     dbg (DEBUG,"TriggerAction: fields    <%s>",pclFields) ;
     dbg (DEBUG,"TriggerAction: data      <%s>",pclData) ;
     DebugPrintACTIONConfig(DEBUG,prlAction) ;
   ****************************************/

   if(ilRc == RC_SUCCESS)
   {
    rlAction.iADFlag = iDELETE_SECTION ;
     memcpy(pclData + strlen(pclData) + 1,&rlAction,sizeof(ACTIONConfig));

    ilRc = que(QUE_PUT, 7400, ilAnswerQueue, PRIORITY_3, llActSize, (char *) prgOutEvent) ; 
    if(ilRc != RC_SUCCESS)
    {
     dbg(TRACE,"TriggerAction: QUE_PUT failed <%d>",ilRc) ;
    }
    else
    {
     ilRc = WaitAndCheckQueue(100, ilAnswerQueue,&prgItem) ;
     if(ilRc != RC_SUCCESS)
     {
      dbg(TRACE,"TriggerAction: WaitAndCheckQueue failed <%d>",ilRc) ;
     }
     else
     {
      /* return value for delete does not matter !!! */
        
      /****************************************
      prlEvent     = (EVENT *)   ((char *)prgItem->text) ;
      prlBchead    = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT)) ;
      prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data) ;
      pclSelection = (char *)    prlCmdblk->data ;
      pclFields    = (char *)    pclSelection + strlen(pclSelection) + 1;
      pclData      = (char *)    pclFields + strlen(pclFields) + 1;
      DebugPrintItem(DEBUG, prgItem) ;
      DebugPrintEvent(DEBUG, prlEvent) ;
      DebugPrintBchead(DEBUG, prlBchead) ;
      DebugPrintCmdblk(DEBUG, prlCmdblk) ;
      dbg(DEBUG,"TriggerAction: selection <%s>",pclSelection) ;
      dbg(DEBUG,"TriggerAction: fields    <%s>",pclFields) ;
      dbg(DEBUG,"TriggerAction: data      <%s>",pclData) ;
      ****************************************/
    } /* end of if */
   }/* end of if */
  }/* end of if */

  if(ilRc == RC_SUCCESS)
  {
   rlAction.iADFlag = iADD_SECTION ;
     memcpy(pclData + strlen(pclData) + 1,&rlAction,sizeof(ACTIONConfig));

   ilRc = que(QUE_PUT, 7400, ilAnswerQueue, PRIORITY_3, llActSize,
                                              (char *) prgOutEvent ) ;
   if(ilRc != RC_SUCCESS)
   {
    dbg(TRACE,"TriggerAction: QUE_PUT failed <%d>",ilRc) ;
   }
   else
   {
    ilRc = WaitAndCheckQueue(100,ilAnswerQueue,&prgItem) ;
    if(ilRc != RC_SUCCESS)
    {
     dbg(TRACE,"TriggerAction: WaitAndCheckQueue failed <%d>",ilRc) ;
    }
    else
    {
     prlEvent     = (EVENT *)   ((char *)prgItem->text) ;
     prlBchead    = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT)) ;
     prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data) ;
     pclSelection = (char *)    prlCmdblk->data ;
     pclFields    = (char *)    pclSelection + strlen(pclSelection) + 1 ;
     pclData      = (char *)    pclFields + strlen(pclFields) + 1 ;

     if(strcmp(pclData,"SUCCESS") != 0)
     {
      dbg(TRACE,"TriggerAction: add dynamic action config failed") ;
      DebugPrintItem(DEBUG,prgItem) ;
      DebugPrintEvent(DEBUG,prlEvent) ;
      DebugPrintBchead(DEBUG,prlBchead) ;
      DebugPrintCmdblk(DEBUG,prlCmdblk) ;
      dbg (DEBUG,"TriggerAction: selection <%s>",pclSelection) ;
      dbg (DEBUG,"TriggerAction: fields    <%s>",pclFields) ;
      dbg(DEBUG,"TriggerAction: data      <%s>",pclData) ;
      ilRc = RC_FAIL ;
     }
     else
     {
      dbg(DEBUG,"TriggerAction: add dynamic action config OK") ;
     }/* end of if */
    }/* end of if */
   }/* end of if */
  }/* end of if */

  ilRc = que(QUE_DELETE,ilAnswerQueue,ilAnswerQueue,0,0,0) ;
  if(ilRc != RC_SUCCESS)
  {
   dbg(TRACE,"que QUE_DELETE <%d> failed <%d>",ilAnswerQueue, ilRc) ;
  }
  else
  {
   dbg(DEBUG,"TriggerAction: queue <%d> <%s> deleted", ilAnswerQueue, &clQueueName[0]) ;
  }/* end of if */
 }/* end of if */

 return (ilRc) ;
    
}
static int WaitAndCheckQueue(int ipTimeout, int ipModId, ITEM **prpItem)
{
    int ilRc          = RC_SUCCESS;                      /* Return code */
    int ilWaitCounter = 0;

    do
    {
        nap(50);
        ilWaitCounter += 1;

        ilRc = CheckQueue(ipModId,prpItem);
        if(ilRc != RC_SUCCESS)
        {
            if(ilRc != QUE_E_NOMSG)
            {
                dbg(TRACE,"WaitAndCheckQueue: CheckQueue <%d> failed <%d>",mod_id,ilRc);
            }/* end of if */
        }/* end of if */
    }while((ilRc == QUE_E_NOMSG) && (ilWaitCounter < ipTimeout));

    if(ilWaitCounter >= ipTimeout)
    {
        dbg(TRACE,"WaitAndCheckQueue: timeout reached <%d>",ipTimeout);
        ilRc = RC_FAIL;
    }/* end of if */

    return ilRc;
}


static int CheckQueue(int ipModId, ITEM **prpItem)
{
    int     ilRc       = RC_SUCCESS;                      /* Return code */
    int     ilItemSize = 0;
    EVENT *prlEvent    = NULL;
static  ITEM  *prlItem      = NULL;
    
    ilItemSize = I_SIZE;
    
    ilRc = que(QUE_GETBIGNW,ipModId,ipModId,PRIORITY_3,ilItemSize,(char *) &prlItem);
    if(ilRc == RC_SUCCESS)
    {
        dbg(TRACE,"DebugPrintItem with prlItem follows");
        DebugPrintItem(TRACE,prlItem);
        *prpItem = prlItem;
        dbg(TRACE,"DebugPrintItem with prpItem follows");
        DebugPrintItem(TRACE,*prpItem);
        prlEvent = (EVENT*) ((prlItem)->text);

        switch( prlEvent->command )
        {
            case    HSB_STANDBY :
                dbg(TRACE,"CheckQueue: HSB_STANDBY");
                HandleQueues();
                ctrl_sta = prlEvent->command;
                break;
    
            case    HSB_COMING_UP   :
                dbg(TRACE,"CheckQueue: HSB_COMING_UP");
                ctrl_sta = prlEvent->command;
                break;
    
            case    HSB_ACTIVE  :
                dbg(TRACE,"CheckQueue: HSB_ACTIVE");
                ctrl_sta = prlEvent->command;
                break;
    
            case    HSB_ACT_TO_SBY  :
                dbg(TRACE,"CheckQueue: HSB_ACT_TO_SBY");
                HandleQueues();
                ctrl_sta = prlEvent->command;
                break;
    
            case    HSB_DOWN    :
                dbg(TRACE,"CheckQueue: HSB_DOWN");
                ctrl_sta = prlEvent->command;
                /* Acknowledge the item 
                ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
                if( ilRc != RC_SUCCESS )
                {
                    HandleQueErr(ilRc);
                }  */
                Terminate(30);
                break;
        
        case    HSB_STANDALONE  :
                dbg(TRACE,"CheckQueue: HSB_STANDALONE");
                ctrl_sta = prlEvent->command;
                break;
    
            case    SHUTDOWN    :
                dbg(TRACE,"CheckQueue: SHUTDOWN");
                /* Acknowledge the item */
                ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
                if( ilRc != RC_SUCCESS )
                {
                    /* handle que_ack error */
                    HandleQueErr(ilRc);
                } /* fi */
                Terminate(30);
                break;
    
            case    RESET       :
                ilRc = Reset();
                break;
    
            case    EVENT_DATA  :
                /* DebugPrintItem(DEBUG,prgItem); */
                /* DebugPrintEvent(DEBUG,prlEvent); */
                break;
    
            case    TRACE_ON :
                dbg_handle_debug(prlEvent->command);
                break;
    
            case    TRACE_OFF :
                dbg_handle_debug(prlEvent->command);
                break;


            default         :
                dbg(TRACE,"CheckQueue: unknown event");
                DebugPrintItem(TRACE,prlItem);
                DebugPrintEvent(TRACE,prlEvent);
                break;
    
        } /* end switch */
    
        /* Acknowledge the item */
        ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
        if( ilRc != RC_SUCCESS )
        {
            /* handle que_ack error */
            HandleQueErr(ilRc);
        } /* fi */
    }else{
        if(ilRc != QUE_E_NOMSG)
        {
            dbg(TRACE,"CheckQueue: que(QUE_GETBIG,%d,%d,...) failed <%d>",ipModId,ipModId,ilRc);
            HandleQueErr(ilRc);
        }/* end of if */
    }/* end of if */
       
    return ilRc;
}/* end of CheckQueue */



/****************************************************************************
* Function: GetUrnoFromSelection
* Parameter:           
*            
* Return:
* Description: gets the Urno from the SQL Statement "WHERE URNO = 'xxxxxxx'"
*              
*****************************************************************************/
static int GetUrnoFromSelection(char *pcpSelection,char *pcpUrno)
{
    char *pclFirst;
    char *pclLast;
    char pclTmp[24];
    int ilLen;
    
    pclFirst = strchr(pcpSelection,'\'');
    if (pclFirst != NULL)
    {
        pclLast  = strrchr(pcpSelection,'\'');
        if (pclLast == NULL)
        {
            pclLast = pclFirst + strlen(pclFirst);
        }
        ilLen = pclLast-pclFirst < 22 ? pclLast - pclFirst : 22;
        strncpy(pclTmp,++pclFirst,ilLen);
        pclTmp[ilLen-1] = '\0';
        strcpy(pcpUrno,pclTmp);
    }
    else
        {
         pclFirst = pcpSelection;
         while (!isdigit(*pclFirst) && *pclFirst != '\0')
         {
            pclFirst++;
            }
        strcpy(pcpUrno,pclFirst);
        }
    return RC_SUCCESS;
}


static int SaveIndexInfo (ARRAYINFO *prpArrayInfo)
{
  int    ilRc       = RC_SUCCESS ;
  long   llRow      = 0 ;
  FILE  *prlFile    = NULL ;
  char   clDel      = ',' ;
  char   clFile[512] ;
  char  *pclTrimBuf = NULL ;

  dbg (TRACE,"SaveIndexInfo: <%s>",prpArrayInfo->crArrayName) ;

  if (ilRc == RC_SUCCESS)
  {
   sprintf (&clFile[0],"%s/%s_%s.info",getenv("DBG_PATH"),mod_name,prpArrayInfo->crArrayName) ;

   errno = 0 ;
   prlFile = fopen (&clFile[0], "w") ;
   if (prlFile == NULL)
   {
    dbg (TRACE, "SaveIndexInfo: fopen <%s> failed <%d-%s>", &clFile[0], errno, strerror(errno)) ;
    ilRc = RC_FAIL ;
   } /* end of if */
  } /* end of if */

  if (ilRc == RC_SUCCESS)
  {
   if (igTestOutput > 2) 
   {
    dbg (DEBUG, "ArrayName      <%s>\n", prpArrayInfo->crArrayName) ;      
    dbg (DEBUG, "ArrayFieldList <%s>\n", prpArrayInfo->crArrayFieldList) ;  
    dbg (DEBUG, "Idx01FieldList <%s>\n", prpArrayInfo->crIdx01FieldList) ;  
   }

   fprintf (prlFile, "ArrayName      <%s>\n", prpArrayInfo->crArrayName) ; fflush(prlFile) ;
   fprintf (prlFile, "ArrayFieldList <%s>\n", prpArrayInfo->crArrayFieldList) ; fflush(prlFile) ;
   fprintf (prlFile, "Idx01FieldList <%s>\n", prpArrayInfo->crIdx01FieldList) ; fflush(prlFile) ;

   if (igTestOutput > 1) 
   {
    dbg(DEBUG,"IndexData") ; 
   }
   fprintf(prlFile,"IndexData\n"); fflush(prlFile);

   llRow = ARR_FIRST ;
   do
   {
    ilRc = CEDAArrayFindKey (&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), &(prpArrayInfo->rrIdx01Handle), prpArrayInfo->crIdx01Name, &llRow, prpArrayInfo->lrIdx01RowLen, prpArrayInfo->pcrIdx01RowBuf);
    if (ilRc == RC_SUCCESS)
    {
     if (igTestOutput > 1) 
     {
      dbg(DEBUG, "SaveIndexInfo: Key <%ld> <%s>",llRow,prpArrayInfo->pcrIdx01RowBuf) ;
     }

     if (strlen (prpArrayInfo->pcrIdx01RowBuf) == 0)
     {
      ilRc = CEDAArrayGetRow(&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), llRow, clDel, prpArrayInfo->lrArrayRowLen, prpArrayInfo->pcrArrayRowBuf);
      if(ilRc == RC_SUCCESS)
      {
       if (igTestOutput > 1) 
       {
        dbg (DEBUG, "SaveIndexInfo: Row <%ld> <%s>", llRow,prpArrayInfo->pcrArrayRowBuf) ;
       }
      } /* end of if */
     } /* end of if */

     pclTrimBuf = strdup (prpArrayInfo->pcrIdx01RowBuf) ;
     if (pclTrimBuf != NULL)
     {
      ilRc = GetDataItem (pclTrimBuf,prpArrayInfo->pcrIdx01RowBuf, 1, clDel, "", "  ") ;
      if (ilRc > 0)
      {
       ilRc = RC_SUCCESS ;
       fprintf (prlFile,"Key            <%ld><%s>\n",llRow,pclTrimBuf) ; fflush(prlFile) ;
      } /* end of if */

      free (pclTrimBuf) ;
      pclTrimBuf = NULL ;
     } /* end of if */

     llRow = ARR_NEXT;
    }
    else
    {
     dbg (TRACE, "SaveIndexInfo: CEDAArrayFindKey failed <%d>", ilRc) ;
    } /* end of if */
   } while (ilRc == RC_SUCCESS) ;

   if (igTestOutput > 1) 
   {
    dbg (DEBUG,"ArrayData") ;
   }
   fprintf (prlFile,"ArrayData\n"); fflush(prlFile);

   llRow = ARR_FIRST ;
   do
   {
    ilRc = CEDAArrayFindKey (&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), &(prpArrayInfo->rrIdx01Handle), prpArrayInfo->crIdx01Name, &llRow, prpArrayInfo->lrIdx01RowLen, prpArrayInfo->pcrIdx01RowBuf);
    if (ilRc == RC_SUCCESS)
    {
     ilRc = CEDAArrayGetRow(&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), llRow, clDel, prpArrayInfo->lrArrayRowLen, prpArrayInfo->pcrArrayRowBuf);
     if (ilRc == RC_SUCCESS)
     {
      if (igTestOutput > 1) 
      {
       dbg(DEBUG,"SaveIndexInfo: Row <%ld> <%s>",llRow,prpArrayInfo->pcrArrayRowBuf) ;
      }
      fprintf (prlFile,"Row            <%ld><%s>\n",llRow,prpArrayInfo->pcrArrayRowBuf) ; fflush(prlFile) ;
     }
     else
     {
      dbg (TRACE, "SaveIndexInfo: CEDAArrayGetRow failed <%d>", ilRc) ;
     } /* end of if */

     llRow = ARR_NEXT ;
    } 
    else
    {
     dbg (TRACE, "SaveIndexInfo: CEDAArrayFindKey failed <%d>", ilRc) ;
    } /* end of if */
   } while (ilRc == RC_SUCCESS) ;

   if (ilRc == RC_NOTFOUND)
   {
    ilRc = RC_SUCCESS ;
   } /* end of if */
  } /* end of if */

  if (prlFile != NULL)
  {
   fclose(prlFile) ;
   prlFile = NULL ;
   if (igTestOutput > 1) 
   {
    dbg (TRACE, "SaveIndexInfo: event saved - file <%s>", &clFile[0]) ;
   }
  } /* end of if */

  return (ilRc) ;

}

        

static int GetDebugLevel(char *pcpMode, int *pipMode)
{
    int     ilRC = RC_SUCCESS;
    char    clCfgValue[64];

    ilRC = iGetConfigRow(cgConfigFile, "SYSTEM", pcpMode, CFG_STRING,
                            clCfgValue);

    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"SYSTEM %s not found in <%s>", pcpMode, cgConfigFile);
    }
    else
    {
        dbg(TRACE, "SYSTEM %s <%s>", pcpMode, clCfgValue);
        if (!strcmp(clCfgValue, "DEBUG"))
            *pipMode = DEBUG;
        else if (!strcmp(clCfgValue, "TRACE"))
            *pipMode = TRACE;
        else
            *pipMode = 0;
    }

    return ilRC;
}

static int ItemCnt(char *pcgData, char cpDelim)
{
    int ilItemCount=0;

    if (pcgData && strlen(pcgData)) 
    {
        ilItemCount = 1;
        while (*pcgData) 
        {
            if (*pcgData++ == cpDelim) 
            {
                ilItemCount++;
            } /* end if */
        } /* end while */
    } /* end if */
    return ilItemCount;
}

static int DumpRecord(ARRAYINFO *prpArray,char *pcpFields,char *pcpBuf)
{
    int ilRc;
    int ilRecord = 0;
    long llRowNum = ARR_FIRST;
    char *pclRowBuf;

    dbg(TRACE,"Recorddump: %s", prpArray->crArrayName);
    if (pcpFields != NULL)
    {
        dbg(TRACE,"Recorddump: Fields = %s", pcpFields);
    }
    else
    {
        dbg(TRACE,"Recorddump: Fields = %s",prpArray->crArrayFieldList);
    }

    {
            int ilFieldNo;
        
            for (ilFieldNo = 1; ilFieldNo <= prpArray->lrArrayFieldCnt; ilFieldNo++)
            {
                char pclFieldName[24];

                *pclFieldName = '\0';
                GetDataItem(pclFieldName,prpArray->crArrayFieldList,
                    ilFieldNo,',',"","\0\0");
                if (pcpFields != NULL)      
                {
                    if(strstr(pcpFields,pclFieldName) == NULL)
                    {
                        dbg(DEBUG,"Field <%s> not in field list",pclFieldName);
                        continue;
                    }
                }
                dbg(TRACE,"%s: <%s>",
                        pclFieldName,PFIELDVAL((prpArray),pcpBuf,ilFieldNo-1));
            }
    }

    return ilRc;

}
static int DumpArray(ARRAYINFO *prpArray,char *pcpFields)
{
    int ilRc;
    int ilRecord = 0;
    long llRowNum = ARR_FIRST;
    char *pclRowBuf;

    dbg(TRACE,"Arraydump: %s", prpArray->crArrayName);
    if (pcpFields != NULL)
    {
        dbg(TRACE,"Arraydump: Fields = %s", pcpFields);
    }
    else
    {
        dbg(TRACE,"Arraydump: Fields = %s",prpArray->crArrayFieldList);
    }

    while(CEDAArrayGetRowPointer(&prpArray->rrArrayHandle,
        prpArray->crArrayName,llRowNum,(void *)&pclRowBuf) == RC_SUCCESS)       
    {
            int ilFieldNo;
        
            ilRecord++;
            llRowNum = ARR_NEXT;

            dbg(TRACE,"Record No: %d",ilRecord);
            for (ilFieldNo = 1; ilFieldNo <= prpArray->lrArrayFieldCnt; ilFieldNo++)
            {
                char pclFieldName[24];

                *pclFieldName = '\0';
                GetDataItem(pclFieldName,prpArray->crArrayFieldList,
                    ilFieldNo,',',"","\0\0");
                if (pcpFields != NULL)      
                {
                    if(strstr(pcpFields,pclFieldName) == NULL)
                    {
                        dbg(DEBUG,"Field <%s> not in field list",pclFieldName);
                        continue;
                    }
                }
                dbg(TRACE,"%s: <%s>",
                        pclFieldName,PFIELDVAL((prpArray),pclRowBuf,ilFieldNo-1));
            }
    }

    return ilRc;

}

static BOOL ExportAbsence(char *pcpCode)
{
    int ilRc = RC_SUCCESS;
    char clKey[124];
    long llRow = ARR_FIRST;
    BOOL blRet = FALSE;

    strcpy(clKey,pcpCode);

    ilRc = CEDAArrayFindRowPointer(&rgOdaArray.rrArrayHandle,rgOdaArray.crArrayName,
        &rgOdaArray.rrIdx02Handle,rgOdaArray.crIdx02Name,clKey,
        &llRow,(void **)&rgOdaArray.pcrArrayRowBuf);

    if (ilRc >= 0)
    {
        char clOdaFree[3];
        char clOdaType[6];
        char clOdaSdac[10];

        strcpy(clOdaFree,PFIELDVAL((&rgOdaArray),rgOdaArray.pcrArrayRowBuf,igOdaFREE));
        strcpy(clOdaType,PFIELDVAL((&rgOdaArray),rgOdaArray.pcrArrayRowBuf,igOdaTYPE));
        strcpy(clOdaSdac,PFIELDVAL((&rgOdaArray),rgOdaArray.pcrArrayRowBuf,igOdaSDAC));

        dbg(TRACE,"IsAbsenceCode for key <%s> found absence <%s>",clKey,clOdaSdac);

        if (strstr(clOdaFree,"x") || strstr(clOdaType,"S"))
        {
            blRet = TRUE;
        }
    }

    dbg(DEBUG,"IsAbsenceCode for <%s> will return %d",pcpCode,blRet);
    return blRet;
}



