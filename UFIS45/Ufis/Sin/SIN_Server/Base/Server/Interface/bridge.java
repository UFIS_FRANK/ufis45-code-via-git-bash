import com.datalex.has.core.HostAccessServiceManager;
import com.datalex.has.core.HostAccessServiceNotFoundException;
import com.datalex.has.travelservices.terminalservice.TerminalSessionManager;
import com.datalex.has.travelservices.terminalservice.TerminalSession;
import com.datalex.has.travelservices.terminalservice.TerminalDevice;
import com.datalex.has.travelservices.terminalservice.TerminalSessionClosureEvent;
import com.datalex.has.travelservices.terminalservice.TerminalDeviceErrorEvent;
import com.datalex.has.travelservices.terminalservice.TerminalSessionValidationException;
import com.datalex.has.travelservices.terminalservice.TerminalDeviceStatusEvent;
import com.datalex.has.travelservices.terminalservice.TerminalEvent;
import com.datalex.has.travelservices.terminalservice.TerminalDeviceDataReceivedEvent;
import com.datalex.has.travelservices.terminalservice.TerminalDeviceDataSentEvent;
import com.datalex.has.travelservices.terminalservice.TerminalServiceFailoverEvent;
import com.datalex.has.travelservices.terminalservice.TerminalSessionAboutToCloseEvent;

import java.io.*;
import src.java.util.*;
import java.lang.*;
import java.util.*;
import java.net.*;
import java.sql.*;
       	 
public class bridge implements TerminalSessionManager.TerminalEventListener
{
//    private static String mks_version = "@(#) "UFIS_VERSION" $Id: Ufis/Sin/SIN_Server/Base/Server/Interface/bridge.java 1.23 2003/11/03 17:44:28SGT heb Exp  $";
    // IP oder DNS des CEDA Servers 
    private static final String CEDASERVER = "localhost";
    private static PrintStream out;
    private static InputStream in;
    private static int port;
    private static int pid;
    private static String dbgPath;
    private static String sgSelection;
    private static String sgFields;
    private static String sgData;
    private static String sgCommand;
  
    private static String sgReturnSelection;
    private static String sgReturnFields;
    private static String sgReturnData;
    private static String sgReturnCommand;
  
    private static String sgDataCommand;
    static String hostResponse = "no response";
    static String sgTmpHostResponse = "no response";
    static String logoutResponse = "no response";
    static String loginResponse = "no response";
    static String sgServiceName;
    static String sgMonitorPeriod;
    static String sgAgency;
    static String sgAppName;
    static String sgWorkstationId;
    static String sgPassword;
    static String sgFollowCommand;
    static String sgNextKey;
    static String sgNextRow;
    static String sgNextColumn;
    static String sgLastKey;
    static String sgLastRow;
    static String sgLastColumn;
    static String sgConfigFile = "/ceda/conf/kriscom.cfg";
    static String sgHeader = "NO";
    static String sgSendAndReceiveTimeOut = "default";
    static String sgGetHasInit_Timeout = "1000";
    static long llGetHasInit_Timeout = 1000;
    static long llTimeOut = 30000;
    static long LogFileSize = 5000000;
    private static transient TerminalSessionManager sessionManager=null;
    private static transient TerminalSession myTerminalSession;
    //Venka 15/07/03
    private static transient boolean bInit =false;
    static File LogFile;
    static File LogFile1;
    static File LogFileBak;
    static String LogFileName = "default";
    static String LogFileName1 = "default";
    static PrintStream origOut = System.out;
    static PrintStream cedaOut;
    static int LogFileCounter = 0;
    static String sgLogFileSize = "5000000";

    public bridge()
	{
	}
  
    public static void main(String[] args) 
	{
//	    dbg(mks_version);
	    port = Integer.parseInt(args[0]); // port - Erster �bergebener Parameter wird vom String zu einem Integer geparst - Port gibt den Port des CEDA Servers an
	    pid = Integer.parseInt(args[1]); // pid - Zweiter �bergebener Parameter wird vom String zu einem Integer geparst - PID ist der ProzessID des Vorgangs		          		
	    dbgPath = args[2]; // dbgPath - Dritter �bergebener Parameter - DebugPath gibt den Pfad mit Debuginformationen an

	    sgConfigFile = args[3];

	    LogFileName = args[2] + args[1] + ".log";
	    LogFileName1 = LogFileName;


	    try
	    {
		cedaOut = new PrintStream(new FileOutputStream(LogFileName));
		System.setOut(cedaOut);
	    }
	    catch (java.io.FileNotFoundException e)
	    {
		System.out.println("Exception : " + e.toString());
	    }



	    try
	    {
		dbg( "PORT: " + port); 			
		dbg( "PID: " + pid);			
		dbg( "DBG_PATH: " + dbgPath);
		LogFileName = dbgPath + ".log";
		dbg( "LogFileName: " + LogFileName);
      		dbg( "sgConfigFile: " +sgConfigFile);
		Socket socket = new Socket(CEDASERVER, port); // Clientsocket �ffnen
		dbg( "CEDAServer auf Port " + port + " gefunden");    
		dbg( "Verbindung wird hergestellt");
	  
		out = new PrintStream(socket.getOutputStream()); // OutputStream instanziieren
		in = socket.getInputStream();	// InputStream instanziieren

	    }
	    catch (IOException e) 
	    {
      		dbg("Bridge: " + e.toString() + " *** ERROR @ main(String[] args) ***");
      		dbg("Bridge: Verbindungsaufbau zum CEDAServer fehlgeschlagen...");
      		System.exit(1);
	    } 
	    catch (Exception exception) 
	    {
      		dbg("Bridge: " + exception.toString() + " *** ERROR @ main(String[] args) ***");
      		dbg("Bridge: Verbindungsaufbau zum RMIServer fehlgeschlagen...");
      		System.exit(1);
	    }

 
	    try 
	    {
	    	
		while(true)
		{	        		
		    readRequest();	// Ankommende Bytes auslesen und auswerten...
		    // socket.setSoTimeout(60000); // Timeout setzen in millisekunden
		}	
	    } 
	    catch (IOException e)
	    {
      		dbg("Bridge: " + e.toString() + " *** ERROR @ run() ***"); 
      		dbg("Bridge: Verbindung durch Fehler / Timeout geschlossen...");
      		System.exit(1);
	    } 	
	}

    /**
     * Liest den n�chsten Request ein
     */
    private static void readRequest() throws IOException
  	{
	    String trennzeichen, sizeString;
	    int c, i, j, k, l, tokenCounter1, tokenCounter2, sizeInt;
	    StringBuffer sb = new StringBuffer(999999);
	    StringBuffer sizeByte = new StringBuffer(15);
	    byte[] b = new byte[14];
    			
	    //erste 15 bytes auslesen - SIZE FORMAT: #SIZE#<123456>; 123456 wird weitergereicht
	    l = 0;
	    while ((c = in.read()) != -1) {
			
		if(l<=13) {
		    b[l] = (byte)c;
		    l++; 			
 		
		} else if (l>13) {
		    break;
		}
	    }	
	    		
	    for (l = 7; l <= 12; l++) {
     		sizeByte.append((char)b[l]);
	    }
     	      	     	
	    sizeString = sizeByte.toString();  
	    sizeInt = 0;
      	
	    try {
      		sizeInt = Integer.parseInt(sizeString);
  	    
  	    } catch (NumberFormatException numberE) {
		//dbg("Bridge: " + numberE.toString() + " *** ERROR @ readRequest() ***");
  	    } 		
 	      	     		      	  
	    //weiteren ByteStream auslesen, beschraenkt auf Groesse von sizeInt
	    l=1;
	    while (((c = in.read()) != -1) && (l < sizeInt)) {
    	
		sb.append((char)c);
		l++;	      	
	    }
	    		   	
	    try {
	      	dbg( "Empfangene Daten von bridge: <" + sb + ">");

		createResponse(sb.toString()); 
		dbg( "========== Request processed ==============");

	    } catch (Exception e) {
		dbg("Bridge: " + e.toString() + " *** ERROR @ readRequest() ***");
	    }
	        	     
	}

    /**
     * Anfrage auswerten und Antwort senden
     */
    private static void createResponse(String spDataString)
	{
	    int ilBegin = 0;
	    int ilEnd = 0;
	    int ilCount = 0;
	    int xy,xyz;
	    String returnFromRMI, returnDataToCEDA, zeros;
	    String slTmp = "KER";
	    StringBuffer sb;

	    try {        	
		ilBegin = spDataString.indexOf("#CMD#<");
		ilEnd = spDataString.indexOf(">",ilBegin);
		sgCommand = spDataString.substring(ilBegin +6 ,ilEnd);
		//dbg( "ilBegin: <" + ilBegin + ">");
		//dbg( "ilEnd: <" + ilEnd + ">");
		dbg( "sgCommand: <" + sgCommand + ">");

		ilBegin = spDataString.indexOf("#SEL#<");
		//dbg( "ilBegin: <" + ilBegin + ">");
		ilEnd = spDataString.indexOf(">",ilBegin);
		//dbg( "ilEnd: <" + ilEnd + ">");
		sgSelection = spDataString.substring(ilBegin +6 ,ilEnd);
		dbg( "sgSelection: <" + sgSelection + ">");
		
		if(sgCommand.equals("KRI"))
		{
		    /*get DataCommand*/
		    ilBegin = sgSelection.indexOf(";") + 1;
		    ilEnd = sgSelection.indexOf(";" ,ilBegin);
		    sgDataCommand = sgSelection.substring(ilBegin,ilEnd);
		    dbg( "sgDataCommand: <" + sgDataCommand + ">");
		
		    /*get FollowCommand*/
		    ilBegin = ilEnd +1;
		    ilEnd = sgSelection.indexOf(";" ,ilBegin);
		    sgFollowCommand = sgSelection.substring(ilBegin,ilEnd);
		    dbg( "sgFollowCommand: <" + sgFollowCommand + ">");
		
		    ilBegin = ilEnd +1;
		    ilEnd = sgSelection.indexOf(";" ,ilBegin);
		    sgNextKey = sgSelection.substring(ilBegin,ilEnd);
		    dbg( "sgNextKey: <" + sgNextKey + ">");
		    
		    ilBegin = ilEnd +1;
		    ilEnd = sgSelection.indexOf(";" ,ilBegin);
		    sgNextRow = sgSelection.substring(ilBegin,ilEnd);
		    dbg( "sgNextRow: <" + sgNextRow + ">");

		    ilBegin = ilEnd +1;
		    ilEnd = sgSelection.indexOf(";" ,ilBegin);
		    sgNextColumn = sgSelection.substring(ilBegin,ilEnd);
		    dbg( "sgNextColumn: <" + sgNextColumn + ">");

		    ilBegin = ilEnd +1;
		    ilEnd = sgSelection.indexOf(";" ,ilBegin);
		    sgLastKey = sgSelection.substring(ilBegin,ilEnd);
		    dbg( "sgLastKey: <" + sgLastKey + ">");

		    ilBegin = ilEnd +1;
		    ilEnd = sgSelection.indexOf(";" ,ilBegin);
		    sgLastRow = sgSelection.substring(ilBegin,ilEnd);
		    dbg( "sgLastRow: <" + sgLastRow + ">");

		    ilBegin = ilEnd +1;
		    ilEnd = sgSelection.indexOf(";" ,ilBegin);
		    sgLastColumn = sgSelection.substring(ilBegin);
		    dbg( "sgLastColumn: <" + sgLastColumn + ">");
		}
		ilBegin = spDataString.indexOf("#FLD#<");
		ilEnd = spDataString.indexOf(">",ilBegin);
		sgFields = spDataString.substring(ilBegin +6 ,ilEnd);
		dbg( "sgFields: <" + sgFields + ">");
        
		ilBegin = spDataString.indexOf("#DAT#<");
		ilEnd = spDataString.indexOf(">",ilBegin);
		sgData = spDataString.substring(ilBegin +6 ,ilEnd);
		dbg( "sgData: <" + sgData + ">");

        
		if (sgCommand.equalsIgnoreCase("PARACMD"))
		{
		    sgReturnCommand = "PARACMD";
		    sgReturnData = ";#DATA#<PARAOK>";
	          								
		} 
		else if (sgCommand.equalsIgnoreCase("PARALOG"))
		{
		    sgReturnCommand = "PARALOG";
		    sgReturnData = ";#DATA#<PARAOK>";
	        } 
		else if (sgCommand.equalsIgnoreCase("PARA"))
		{
		    sgReturnCommand = "PARA";
		    sgReturnData = ";#DATA#<PARAOK>";
		} 
		else if (sgCommand.equalsIgnoreCase("CHK")) 
		{
		    sgReturnCommand = "COC";
		    sgReturnData = "";

		} 
		else if (sgCommand.equalsIgnoreCase("SHUTDOWN")) 
		{
		    dbg( "Bridge: SHUTDOWN empfangen");
		    System.exit(1); 
		          					          				          													
		} 
		else if (sgCommand.equalsIgnoreCase("KRI")) 
		{
		    try
		    {
			hostResponse = "no response";
			ilCount = 1;
			while ((hostResponse.indexOf("no response") != -1) && (ilCount <= 3))
			{
			    doHASaccess();
			    //dbg("**************************************************************");
			    //dbg(hostResponse);
			    //dbg("**************************************************************");
			    ilCount++;
		       
			}
		    }
		    catch (Exception ex)
		    {
			dbg("Exception  " + ex.toString() );
		    }
		    /*sgReturnCommand,fiels,Seleciton aso is set in doHASaccess*/
		}

		sb = new StringBuffer(sgReturnData);
		//dbg("sb: <" + sb + ">");
		for( xy = 0; xy < sb.length(); xy++)
		{
		    xyz = sb.charAt(xy);
		    //dbg("xyz: " + xyz);
		    if (xyz == 0)
		    {
			sb.deleteCharAt(xy);
			xy--;
		    }
		}
		sgReturnData = sb.toString();

		returnDataToCEDA =   "#CMD#<" + sgReturnCommand + ">" + 
		    ";#DAT#<" + sgReturnData + ">" + 
		    ";#FLD#<" + sgReturnFields + ">" + 
		    ";#SEL#<" + sgReturnSelection +">";
		int returnDataToCEDALength = returnDataToCEDA.length();
		String lengthString = String.valueOf(returnDataToCEDALength);	
		int stringLengthDataToCEDA = lengthString.length();
		zeros = "";
	 				
		for (int i = stringLengthDataToCEDA; i <= 5; i++) {
		    zeros = zeros + "0";	
		}	
	 							 
		returnDataToCEDA = "#SIZE#<" + zeros + returnDataToCEDALength + ">;" + returnDataToCEDA;
		out.print(returnDataToCEDA);	//Anwort an CEDA senden
		if (returnDataToCEDA.length() < 2000)
		{
		    dbg( "Bridge: RETURN>>> " + returnDataToCEDA);

		    for( xy = 0; xy < sgReturnData.length() && xy < 10; xy++)
		    {
			xyz = sgReturnData.charAt(xy);
			//dbg("xyz: " + xyz);
		    }
		
		}
		else
		{
		    slTmp = sgReturnData.substring(0,1000);
		    dbg( "Bridge: RETURN>>> ***sorry, data too long, so truncated data (1000 characters) is shown: #CMD#<" 
			 + sgReturnCommand + ">" + 
			 ";#DAT#<" + slTmp + ">" + 
			 ";#FLD#<" + sgReturnFields + ">" + 
			 ";#SEL#<" + sgReturnSelection +">");
		}
				          	
          	 	 
	    } catch (Exception e) {
	      	dbg("Bridge: " + e.toString() + " *** ERROR @ createResponse() ***");
		dbg("Bridge: cannot recover from error. Terminate now. Restart will take about 30 seconds");
		System.exit(1);
	    }
    
	}

    
 
    //Venka 15/07/03  Try to connect for 20 seconds.  Ignore all exceptions
    public boolean getHasInit(String myServiceName)
	{

	    long lnow = System.currentTimeMillis();

	    long time = 20000;
	    while (lnow + time > System.currentTimeMillis())
	    {

		try
		{

		    Thread.sleep(llGetHasInit_Timeout);
		}
		catch(Exception e) {/* To be ignored*/}

		Object obj = null;
		try
		{
		    obj = HostAccessServiceManager.getService(myServiceName);
		}
		catch(Exception e) {/* To be ignored*/ }

		if (obj != null)
		{

		    long timetaken= System.currentTimeMillis() - lnow;
		    dbg("Time taken (msec) for service to be registered successfully: " + timetaken);

		    return true;
		}
	    }
	    return false;
	}


    public static void doHASaccess()
	{
	    sgReturnCommand = "KER";
	    sgReturnFields = "KER";
	    sgReturnSelection = "KER";
	    sgReturnData = "KER";
	    int ilBegin = 0;
	    int ilEnd = 0;
	    String slTmp = "KER";
	    String slTmpLH = "KER";
	    int flag = 0;
	    char clTmpChar = 'X';
	    int ilPos = 0;

	    try
	    {
		Object obj;
	    
		int ilPeriod = 60;

		bridge mybridge = new bridge();
		
		//TerminalEventListener EventListener = new TerminalEventListener();

		//Venka 15/07/03
		//Do this only once per start up of server
		if(!bInit)
		{
		    HostAccessServiceManager.startUp();
		    dbg("After HostAccessServiceManager.startUp()");
		    bInit = true;
		}

		getConfigEntry();

		try
		{
		    //Venka 15/07/03
		    //Shd be first thing done  Moved few lines up. To be done only once per start up
		    //HostAccessServiceManager.startUp();

		    //Venka 15/07/03
		    //Open new Terminal Session Manager object before asking Terminal service if such a TS exists
		    sessionManager = new TerminalSessionManager(sgServiceName,mybridge,30);

		    //Venka 15/07/03
		    dbg("After new TerminalSessionManager command");

		    //obj = HostAccessServiceManager.getService(sgServiceName);
		    //while (obj != null)
		    //{
		    //        dbg("in while");
		    //    obj = HostAccessServiceManager.getService(sgServiceName);
		    //}


		    //Venka 15/07/03  Try to conenct a total of 3 times.  Can be made configurable if neceesary
		    int retry = 3;
		    while (retry > 0)
		    {

			if (mybridge.getHasInit(sgServiceName))
			{
			    break;
			}

			if (retry > 0)
			{

			    retry--;
			    dbg("Timeout has occured. Retrying " + retry + " time(s) more ");
			}
			else
			{
			    String error = "Unable to start up service due to " + sgServiceName + " not found .." ;
			    dbg(error);
			    sgReturnCommand = "KER";
			    sgReturnSelection = "ERR";
			    sgReturnData = error;
			}
		    }

		}
		catch (Exception e)
		{
		    dbg("HostAccessServiceManager.startUp() -- HostAccessServiceNotFoundException: " + e.toString());
		    sgReturnCommand = "KER";
		    sgReturnSelection = "ERR";
		    sgReturnData = "HostAccessServiceManager.startUp() -- HostAccessServiceNotFoundException: ";

		}

		dbg("after startup");

		//Venka 15/07/03. 2nd step to be done b/f anything else.  Moved up a few lines
		//sessionManager = new TerminalSessionManager(sgServiceName,mykriscom,30);
		dbg("after new TerminalSessionManager");
		try
		{
		    dbg("sgAppname: "+ sgAppName +">");
		    dbg("sgWorkstationId: "+sgWorkstationId +">");
		    dbg("sgPassword: "+sgPassword +">");

		    myTerminalSession = sessionManager.openSession(sgAppName,sgWorkstationId,sgPassword);
		}
		catch (java.rmi.RemoteException e)
		{
		    dbg("sessionManager.openSession -- java.rmi.RemoteException: " + e.toString());
		    sgReturnCommand = "KER";
		    sgReturnSelection = "ERR";
		    sgReturnData = "sessionManager.openSession -- java.rmi.RemoteException: ";

		}
		catch (TerminalSessionValidationException e)
		{
		    dbg("sessionManager.openSession -- TerminalSessionValidationException: " + e.toString());
		    sgReturnCommand = "KER";
		    sgReturnSelection = "ERR";
		    sgReturnData = "sessionManager.openSession -- TerminalSessionValidationException: ";
		}
		catch (HostAccessServiceNotFoundException e)
		{
		    dbg("sessionManager.openSession -- HostAccessServiceNotFoundException: " + e.toString());
		    sgReturnCommand = "KER";
		    sgReturnSelection = "ERR";
		    sgReturnData = "sessionManager.openSession -- HostAccessServiceNotFoundException: ";
		}

		try
		{
		    String mySessionID =myTerminalSession.getSessionID();
		}
		catch (java.rmi.RemoteException e)
		{
		    dbg("myTerminalSession.getSessionID() -- java.rmi.RemoteException: " + e.toString());
		    sgReturnCommand = "KER";
		    sgReturnSelection = "ERR";
		    sgReturnData = "myTerminalSession.getSessionID() -- java.rmi.RemoteException: ";
		}
		catch (TerminalSessionValidationException e)
		{
		    dbg("myTerminalSession.getSessionID() -- TerminalSessionValidationException: "+ e.toString());
		    sgReturnCommand = "KER";
		    sgReturnSelection = "ERR";
		    sgReturnData = "myTerminalSession.getSessionID() -- TerminalSessionValidationException ";
		}

		TerminalDevice devices[] = null;
		devices = sessionManager.getDevices();
		//sessionManager.requestAllEventNotification();
		try
		{
		    sessionManager.requestDataReceivedEventNotification(devices[0]);
		    sessionManager.requestDataSentEventNotification(devices[0]);
		    sessionManager.requestErrorEventNotification(devices[0]);
		    sessionManager.requestSessionAboutToCloseEventNotification();
		    sessionManager.requestSessionClosureEventNotification();
		}
		catch (com.datalex.has.travelservices.terminalservice.TerminalSessionManager. UnknownTerminalDeviceException e)
		{
		    dbg("sessionManager.request -- UnknownTerminalDeviceException: "+ e.toString());
		    sgReturnCommand = "KER";
		    sgReturnSelection = "ERR";
		    sgReturnData = "sessionManager.request -- UnknownTerminalDeviceException ";
		}
		catch (java.rmi.RemoteException e)
		{
		    dbg("sessionManager.request -- java.rmi.RemoteException: " + e.toString());
		    sgReturnCommand = "KER";
		    sgReturnSelection = "ERR";
		    sgReturnData = "sessionManager.request -- java.rmi.RemoteException: ";
		}
		catch (TerminalSessionValidationException e)
		{
		    dbg("sessionManager.request -- TerminalSessionValidationException: "+ e.toString());
		}


		try 
		{        	
		    flag = 0;
		    ilBegin = 0;
		    ilEnd = sgData.indexOf("\n",ilBegin);
		    while (ilEnd != -1)
		    {
			flag = 1;
			if (ilEnd == -1)
			{
			    slTmp.substring(ilBegin);
			    ilBegin = 10; /*damit man aus dem while kommt*/
			}
			else
			{
			    slTmp = sgData.substring(ilBegin ,ilEnd);
			    ilBegin = ilEnd + 1;
			    ilEnd = sgData.indexOf("\n",ilBegin);
			}
			dbg("slTmp: <" + slTmp + ">");
			if(sgHeader.equalsIgnoreCase("YES"))
			{
			    slTmpLH = "\002" + slTmp + " \003";
			    slTmp = slTmpLH;
			    dbg("slTmpLH: <" + slTmpLH + ">");
				
			}
			loginResponse = devices[0].sendAndReceive(slTmp,llTimeOut);
			dbg("hostResponse for Login: <"+ loginResponse + ">");
		    }
		    if ((ilBegin > 0) || (flag == 0))
		    {
			slTmp = sgData.substring(ilBegin);
			if(sgHeader.equalsIgnoreCase("YES"))
			{
			    slTmpLH = "\002" + slTmp + " \003";
			    slTmp = slTmpLH;
			    dbg("slTmpLH: <" + slTmpLH + ">");
				
			}

			loginResponse = devices[0].sendAndReceive(slTmp,llTimeOut);
			dbg("hostResponse for Login: <"+ loginResponse + ">");
		    }

		    slTmp = sgDataCommand;
		    if(sgHeader.equalsIgnoreCase("YES"))
		    {
			slTmpLH = "\002" + sgDataCommand + " \003";
			slTmp = slTmpLH;
			dbg("sgDataCommand: <" + slTmpLH + ">");
				
		    }

		    hostResponse = devices[0].sendAndReceive(slTmp,llTimeOut);
		    dbg("hostResponse for Command: <"+ hostResponse + ">");

		    if(sgNextKey.charAt(0) != ' ')
		    {
			ilPos = hostResponse.length();
			ilPos--;
			while((hostResponse.charAt(ilPos) == '\r') || (hostResponse.charAt(ilPos) == '>') || (hostResponse.charAt(ilPos) == 0x1E))
			{
			    dbg("Char at Position <" + ilPos + "> = <" + hostResponse.charAt(ilPos) + ">");
			    ilPos--;
			}

			dbg("nextKey: <" + sgNextKey + ">");
			dbg("ilPos: <" + ilPos + ">");

			slTmp = sgFollowCommand;
			if(sgHeader.equalsIgnoreCase("YES"))
			{
			    slTmpLH = "\002" + sgFollowCommand + " \003";
		            slTmp = slTmpLH;
			    dbg("slTmpLH: <" + slTmpLH + ">");
			    
			}
			//while (hostResponse.lastIndexOf(sgNextKey, ilPos) != -1)
			while (hostResponse.charAt(ilPos) == sgNextKey.charAt(0))
			{
			    dbg("Position in While: <" + ilPos + ">");

			    sgTmpHostResponse = devices[0].sendAndReceive(slTmp,llTimeOut);
			    dbg("NEXT hostResponse: <" + sgTmpHostResponse + ">");

			    if(sgHeader.equalsIgnoreCase("YES"))
			    {
				hostResponse = hostResponse.substring(0,ilPos) + "\r" + sgTmpHostResponse;
			    }
			    else
			    {
				hostResponse = hostResponse.substring(0,ilPos) + sgTmpHostResponse;
			    }

			    //   dbg("hostResponse: <" + hostResponse + ">");
			
			    ilPos = hostResponse.length();
			    ilPos--;
			    while((hostResponse.charAt(ilPos) == '\r') || (hostResponse.charAt(ilPos) == '>') || (hostResponse.charAt(ilPos) == 0x1E))
			    {
				dbg("Char at Position <" + ilPos + "> = <" + hostResponse.charAt(ilPos) + ">");
				ilPos--;
			    }

			}
		    }  
			
			
		    sgReturnCommand = "HAS";
		    sgReturnData = hostResponse;
			
		    flag = 0;
		    ilBegin = 0;
		    ilEnd = sgFields.indexOf("\n",ilBegin);
		    while (ilEnd != -1)
		    {
			if (ilEnd == -1)
			{
			    slTmp.substring(ilBegin);
			    ilBegin = 10;  /*damit man aus dem while kommt*/
			}
			else
			{
			    slTmp = sgFields.substring(ilBegin ,ilEnd);
			    ilBegin = ilEnd + 1;
			    ilEnd = sgFields.indexOf("\n",ilBegin);
			}
			dbg("slTmp: <" + slTmp + ">");
			if(sgHeader.equalsIgnoreCase("YES"))
			{
			    slTmpLH = "\002" + slTmp + " \003";
			    slTmp = slTmpLH;
			    dbg("slTmpLH: <" + slTmpLH + ">");
				
			}
			logoutResponse = devices[0].sendAndReceive(slTmp,llTimeOut);
			dbg("hostResponse for Logoff: <"+ logoutResponse + ">");
		    }
		    if ((ilBegin > 0) || (flag == 0))
		    {
			slTmp = sgFields.substring(ilBegin);
			if(sgHeader.equalsIgnoreCase("YES"))
			{
			    slTmpLH = "\002" + slTmp + " \003";
			    slTmp = slTmpLH;
			    dbg("slTmpLH: <" + slTmpLH + ">");
				
			}
			logoutResponse = devices[0].sendAndReceive(slTmp,llTimeOut);
			dbg("hostResponse for Logoff: <"+ logoutResponse + ">");
		    }

		}
		catch (java.io.IOException e)
		{
		    dbg("devices[0].sendAndReceive -- java.io.IOException: "+ e.toString());
		    sgReturnCommand = "KER";
		    sgReturnSelection = "ERR";
		    sgReturnData = "devices[0].sendAndReceive -- java.io.IOException: ";
		}
		catch (TerminalSessionValidationException e)
		{
		    dbg("devices[0].sendAndReceive -- TerminalSessionValidationException: "+ e.toString());
		    sgReturnCommand = "KER";
		    sgReturnSelection = "ERR";
		    sgReturnData = "devices[0].sendAndReceive -- TerminalSessionValidationException: ";
		}

		try
		{
		    sessionManager.closeSession("The work is done");
		}
		catch (TerminalSessionValidationException e)
		{
		    dbg("sessionManager.closeSession -- TerminalSessionValidationException: "+ e.toString());
		    sgReturnCommand = "KER";
		    sgReturnSelection = "ERR";
		    sgReturnData = "sessionManager.closeSession -- TerminalSessionValidationException: ";
		}
		catch (java.rmi.RemoteException e)
		{
		    dbg("sessionManager.closeSession -- java.rmi.RemoteException: " + e.toString());
		    sgReturnCommand = "KER";
		    sgReturnSelection = "ERR";
		    sgReturnData = "sessionManager.closeSessiont -- java.rmi.RemoteException: ";
		}

	    }/*try*/
	    catch (java.lang.NoClassDefFoundError e)
	    {
		dbg("MAIN -- java.lang.NoClassDefFoundError: "+ e.toString());
		sgReturnCommand = "KER";
		sgReturnSelection = "ERR";
		sgReturnData = "MAIN -- java.lang.NoClassDefFoundError: " + e.toString();
		//e.printStackTrace(System.out);
	    }



	}/*doHASaccess*/


    private static void getConfigEntry()
	{

	    boolean eof = false;
	    int ilIndex = 0;

	    try
	    {
		/*FileReader ConfigFile = new FileReader("/ceda/conf/kriscom.cfg");*/
		FileReader ConfigFile = new FileReader(sgConfigFile);
		BufferedReader ConfigBuffer = new BufferedReader(ConfigFile);

		while (!eof)
		{
		    String ConfigLine = ConfigBuffer.readLine();
		    if (ConfigLine == null)
		    {
			eof = true;
		    }
		    else
		    {
			if (ConfigLine.startsWith("SERVICE_NAME"))
			{
			    ilIndex = ConfigLine.indexOf('=');
			    ConfigLine = ConfigLine.substring(ilIndex+1);
			    ConfigLine = ConfigLine.trim();
			    sgServiceName = ConfigLine;
			    dbg("ConfigEntry: SERVICE_NAME found. Value: <" + sgServiceName + ">");
			}
			if (ConfigLine.startsWith("MONITOR_PERIOD"))
			{
			    ilIndex = ConfigLine.indexOf('=');
			    ConfigLine = ConfigLine.substring(ilIndex+1);
			    ConfigLine = ConfigLine.trim();
			    sgMonitorPeriod = ConfigLine;
			    dbg("ConfigEntry: MONITOR_PERIOD found. Value: <" + sgMonitorPeriod + ">");
			}
			if (ConfigLine.startsWith("AGENCY"))
			{
			    ilIndex = ConfigLine.indexOf('=');
			    ConfigLine = ConfigLine.substring(ilIndex+1);
			    ConfigLine = ConfigLine.trim();
			    sgAgency = ConfigLine;
			    dbg("ConfigEntry: AGENCY found. Value: <" + sgAgency + ">");
			}
			if (ConfigLine.startsWith("APP_NAME"))
			{
			    ilIndex = ConfigLine.indexOf('=');
			    ConfigLine = ConfigLine.substring(ilIndex+1);
			    ConfigLine = ConfigLine.trim();
			    sgAppName = ConfigLine;
			    dbg("ConfigEntry: APP_NAME found. Value: <" + sgAppName + ">");
			}
			if (ConfigLine.startsWith("WORKSTATION_ID"))
			{
			    ilIndex = ConfigLine.indexOf('=');
			    ConfigLine = ConfigLine.substring(ilIndex+1);
			    ConfigLine = ConfigLine.trim();
			    sgWorkstationId = ConfigLine;
			    dbg("ConfigEntry: WORKSTATION_ID found. Value: <" + sgWorkstationId + ">");
			}
			if (ConfigLine.startsWith("PASSWORT"))
			{
			    ilIndex = ConfigLine.indexOf('=');
			    ConfigLine = ConfigLine.substring(ilIndex+1);
			    ConfigLine = ConfigLine.trim();
			    sgPassword = ConfigLine;
			    dbg("ConfigEntry: PASSWORT found. Value: <" + sgPassword + ">");
			}
			if (ConfigLine.startsWith("HEADER"))
			{
			    ilIndex = ConfigLine.indexOf('=');
			    ConfigLine = ConfigLine.substring(ilIndex+1);
			    ConfigLine = ConfigLine.trim();
			    sgHeader = ConfigLine;
			    dbg("ConfigEntry: HEADER found. Value: <" + sgHeader + ">");
			    				
			}
			if (ConfigLine.startsWith("SEND_AND_RECEIVE_TIME_OUT"))
			{
			    ilIndex = ConfigLine.indexOf('=');
			    ConfigLine = ConfigLine.substring(ilIndex+1);
			    ConfigLine = ConfigLine.trim();
			    sgSendAndReceiveTimeOut = ConfigLine;
			    llTimeOut = Long.parseLong(sgSendAndReceiveTimeOut);
			    dbg("ConfigEntry: SEND_AND_RECEIVE_TIME_OUT found. Value: <" + llTimeOut + ">");
			    				
			}
			if (ConfigLine.startsWith("LOG_FILE_SIZE"))
			{
			    ilIndex = ConfigLine.indexOf('=');
			    ConfigLine = ConfigLine.substring(ilIndex+1);
			    ConfigLine = ConfigLine.trim();
			    sgLogFileSize = ConfigLine;
			    LogFileSize = Long.parseLong(sgLogFileSize);
			    dbg("ConfigEntry: LOG_FILE_SIZE found. Value: <" + LogFileSize + ">");
			    				
			}
                        if (ConfigLine.startsWith("GETHASINIT_TIMEOUT"))
                        {
                            ilIndex = ConfigLine.indexOf('=');
                            ConfigLine = ConfigLine.substring(ilIndex+1);
                            ConfigLine = ConfigLine.trim();
                            sgGetHasInit_Timeout = ConfigLine;
                            llGetHasInit_Timeout = Long.parseLong(sgGetHasInit_Timeout);
                            if(llGetHasInit_Timeout <= 100)
                            {
                                 llGetHasInit_Timeout = 500;
                            }
                            dbg("ConfigEntry: GETHASINIT_TIMEOUT found. Value: <" + llGetHasInit_Timeout + ">");

                        }

		    }
		}
		ConfigBuffer.close();
	    }
	    catch (IOException e)
	    {
		dbg("Error -- " + e.toString());
	    }

	}/*end getConfigEntry*/

    private static void initKriscom()
	{

	}
    public synchronized void notify(TerminalEvent e)
	{
	    dbg("notify: Event received: " + e.toString());
	}

    public static void dbg(String spMessage)
	{
	    Timestamp ActualTime = new Timestamp(System.currentTimeMillis());
	    System.out.println(ActualTime.toString() + ": " + spMessage);
	    LogFile1 = new File(LogFileName1);
	    
	    if (LogFile1.length() > LogFileSize)
	    {
		System.setOut(origOut);
		cedaOut.close();
		LogFileBak = new File(LogFileName1 + ".bak" + "." + LogFileCounter);
		LogFile1.renameTo(LogFileBak);
		if (LogFileCounter > 10)
		{
		    LogFile1 = new File(LogFileName1 + ".bak" + "." + (LogFileCounter-10));
		    LogFile1.delete();
		}
		try
		{
		    cedaOut = new PrintStream(new FileOutputStream(LogFileName1));
		}
		catch (java.io.FileNotFoundException e)
		{
		    System.out.println("Exception : " + e.toString());
		}
		System.setOut(cedaOut);
		LogFileCounter++;
		
	    }
	}

}/* end of class bridge*/


    
    
