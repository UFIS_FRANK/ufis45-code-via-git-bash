#!/bin/bash
###
### Script to start a java bridge class 
###
### !! DO NOT START BY HAND !!!

BRIGDEENV="-Dbeanfactory.cfg=/prod/applc/datalex/jbookit/HostAccessServer/HASBeanpolicies.cfg -Dcom.datalex.has.core.HostAccessServiceManager.verbose=true -Djava.security.policy=policy.all -DhasGroup=JINI@AIXPROD"

LOGFILE=/ceda/debug/Alteabridge_BadMagickNo.log
if [ -f ${LOGFILE} ] ; then
  NUM_LINES=`cat ${LOGFILE} | wc -l`
  if [ `expr ${NUM_LINES} \> 30000` = 1  ] ; then
    mv ${LOGFILE} ${LOGFILE}.bak
  fi
fi
XIT=0
STAMP="$4 `date +%Y%m%m%d%H%M%S`"
echo "${STAMP}: ========================================="           >>${LOGFILE}
echo "${STAMP}: call \"javabridge.sh\" start"        >>${LOGFILE}


#/prod/applc/jdk1.3/bin/java ${BRIGDEENV} bridge $1 $2 $3 /ceda/conf/javabridge01.cfg 2>&1 | while [ "${XIT}" = "0" ] ; do
#ls -al 2>&1 | while [ "${XIT}" = "0" ] ; do
#java -jar /ceda/altea/2011-08-05/Bridge/$4 $1 $2 $3 2>&1 | while [ "${XIT}" = "0" ] ; do
#/prod/applc/jdk1.6/bin/java -jar /ceda/altea/Bridge/$4 $1 $2 $3 2>&1 | while [ "${XIT}" = "0" ] ; do
java -jar /ceda/altea/Bridge/$4 $1 $2 $3 2>&1 | while [ "${XIT}" = "0" ] ; do
  read line
  RC=$?
  if [ "!${RC}" = "0" ] || [ "${line}" == ' ' ] || [ -z "${line}" ]; then
    # read returns error (i.e EOF)
    echo "${STAMP}: call \"/prod/applc/jdk1.6/bin/java -jar /ceda/altea/Bridge/$4 $1 $2 $3\" finished" >>${LOGFILE}
    XIT=1
  else
    STAMP="$4 `date +%Y%m%m%d%H%M%S`"
    echo "${STAMP}: ${line}"                                         >>${LOGFILE}
    case "${line}" in
      *"BAD"*|*"bridge.java:56"*|*"Exception"*)  
       echo "${STAMP}: got bad magick number"                        >>${LOGFILE}
       # please note: the wildcard in grep avoids pid of grep:
       BRIDGE_PIDS=`ps -eo pid,args | grep "$4" | 
                       while read pid rest ; do echo $pid; done`
       if [ ! -z "${BRIDGE_PIDS}" ] ; then
          echo "${STAMP}: kill -9 ${BRIDGE_PIDS}"                    >>${LOGFILE}
          kill -9 ${BRIDGE_PIDS}                              2>&1   >>${LOGFILE}
       fi
       XIT=1
       ;;
    esac
  fi      
done
