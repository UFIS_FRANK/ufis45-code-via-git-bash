#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Sin/SIN_Server/Base/Server/Interface/saphdl.c 1.43 2009/02/19 09:51:25SGT lli Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :  MCU                                                      */
/* Date           :  03.03.2003                                               */
/* Description    :  SAPHDL import from and export to SAP                     */
/*                                                                            */
/* Update history :                                                           */
/* 07-JAN-09   LLI: Fix bug. Add checking for leaving date (dodm). If the leaving date is blank, then don't do the SetLeavingDate()             */
/* 12-FEB-09   LLI: Fix bug. Previously using URNO from STFTAB to update SCOTAB. Change to use URNO from SCOTAB            */
/*                                                                            */
/*                                                                            */
/******************************************************************************/

/******************************************************************************/

#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <dirent.h>
#include <fcntl.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "syslib.h"
#include "AATArray.h"
#include "db_if.h"
#include <time.h>
#include <cedatime.h>


/*#define TEST*/

#define MAXFILENAMES 128

#define BSDFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgBsdArray.plrArrayFieldOfs[ipFieldNo]])
#define ODAFIELD(pcpBuf,ipFieldNo) (&pcpBuf[rgOdaArray.plrArrayFieldOfs[ipFieldNo]])
#define FIELDVAL(rrArray,pcpBuf,ipFieldNo) (&pcpBuf[rrArray.plrArrayFieldOfs[ipFieldNo]])
#define PFIELDVAL(prArray,pcpBuf,ipFieldNo) (&pcpBuf[prArray->plrArrayFieldOfs[ipFieldNo]])

#define CLIENTTODB 0
#define DBTOCLIENT 1
#define NOCONVERT 2

#define CMD_SAP     (0)
#define CMD_DRD     (1)
#define CMD_LTR     (2)
#define CMD_EMP     (3)
#define CMD_ABS     (4)
#define CMD_ATT     (5)
#define CMD_URT     (6)
#define CMD_IRT     (7)
#define CMD_DRT		(8)

#define OVERLAP(start1,end1,start2,end2) strcmp(start1,end2) <= 0 && strcmp(start2,end1) <= 0
#define ISBETWEEN(start1,end1,time) strcmp(start1,time) <= 0 && strcmp(end1,time) >= 0

static char *prgCmdTxt[] = {"SAP","DRD","LTR","EMP","ABS","ATT","URT","IRT","DRT",""};
static int    rgCmdDef[] = {CMD_SAP,CMD_DRD,CMD_LTR,CMD_EMP,CMD_ABS,CMD_ATT,CMD_URT,CMD_IRT,CMD_DRT,0};

static char *cgWeekDays[8] = {
	"SO","MO","DI","MI","DO","FR","SA","SO" };
static char *cgWeekDaysEng[8] = {
	"SU","MO","TU","WE","TH","FR","SA","SU" };
static int igTestOutput = 9;

/******************************************************************************/
/***   Fields                                                             *****/
/******************************************************************************/


#define DRR_FIELDS "AVFR,AVTO,BKDP,BSDU,BUFU,CDAT,DRRN,DRS1,DRS2,DRS3,DRS4,"\
		   "DRSF,EXPF,FCTC,HOPO,LSTU,REMA,ROSL,ROSS,SBFR,SBLP,SBLU,"\
		   "SBTO,SCOD,SCOO,SDAY,STFU,URNO,USEC,USEU"

#define DRRPLANNED_FIELDS "URNO,AVFR,AVTO,SCOD,STFU"
static char cgDrrPlanFields[256] = DRRPLANNED_FIELDS;

#define JOB_FIELDS "ACFR,ACTO,UDSR"
#define SRE_FIELDS "SURN,VPFR,VPTO"
#define BSD_FIELDS "URNO,BSDC,ESBG,LSEN,BKD1"


#define DRRSTF_FIELDS "AVFR,AVTO,DRRN,SBFR,SBLU,SBTO,SCOD,SDAY,STFU,PENO,DRRTAB.URNO,DRS1,DRS3,DRS4,DRS2,ROSL"

static char cgDrrFields[512] = "";
static char cgStfFields[256] = "";

static char cgJobFields[256] = JOB_FIELDS;
static char cgBsdFields[256] = BSD_FIELDS;
static char cgDrrStfFields[256] = DRRSTF_FIELDS;
static char cgSreFields[256] = SRE_FIELDS;

static char cgDrrReloadHintRosl[256];
static char cgDrrReloadHintStfu[256];

#define ADD_DRR_FIELDS "USED,DUMY"

#define STF_INSERT_HOST ":VPENO,:VFINM,:VLANM,:VSHNM,:VGEBU,:VKIND,:VNATI,:VDOBK,:VDOEM,:VDODM,:VREMA,:VRELS" 
#define STF_FIELDS "PENO,FINM,LANM,SHNM,GEBU,KIND,NATI,DOBK,DOEM,DODM,REMA,TOHR,RELS"
#define STF_INSERT_FIELDS "PENO,FINM,LANM,SHNM,GEBU,KIND,NATI,DOBK,DOEM,DODM,REMA,RELS"
#define STF_UPDATE_FIELDS "PENO,FINM,LANM,SHNM,GEBU,KIND,NATI,DOBK,DOEM,DODM,REMA,RELS"
#define STF_UPDATE_HOST "PENO=:VPENO,FINM=:VFINM,LANM=:VLANM,SHNM=:VSHNM,GEBU=:VGEBU,KIND=:VKIND,NATI=:VNATI,DOBK=:VDOBK,DOEM=:VDOEM,DODM=:VDODM,REMA=:VREMA,RELS=:VRELS"

static char cgStfInsertFields[256] = STF_INSERT_FIELDS;
static char cgStfInsertHost[256] = STF_INSERT_HOST;
static char cgStfUpdateFields[256] = STF_UPDATE_FIELDS ;
static char cgStfUpdateHost[256] = STF_UPDATE_HOST;

#define SCO_INSERT_FIELDS "KOST,CODE,SURN,VPFR"
#define SCO_INSERT_HOST ":VKOST,:VCODE,:VSURN,:VVPFR"
#define SCO_UPDATE_FIELDS "KOST=:VKOST,CODE=:VCODE,VPFR=:VVPFR"
#define SCO_FIELDS "KOST,CODE,VPFR"

#define SOR_INSERT_FIELDS "CODE,SURN,VPFR"
#define SOR_INSERT_HOST ":VCODE,:VSURN,:VVPFR"
#define SOR_UPDATE_FIELDS "CODE=:VCODE,VPFR=:VVPFR"
#define SOR_FIELDS "CODE,SURN,VPFR"

#define ADR_INSERT_FIELDS "PENO,VAFR,VATO,STRA,ADRC,ZIPA,CITY,LANA,STFU"
#define ADR_INSERT_HOST ":VPENO,:VVAFR,:VVATO,:VSTRA,:VADRC,:VZIPA,:VCITY,:VLANA,:VSTFU"
#define ADR_UPDATE_FIELDS "PENO=:VPENO,VAFR=:VVAFR,VATO=:VVATO,STRA=:VSTRA,ADRC=:VADRC,ZIPA=:VZIPA,CITY=:VCITY,LANA=:VLANA"
#define ADR_FIELDS "PENO,VAFR,VATO,STRA,ADRC,ZIPA,CITY,LANA,STFU"

#define DRA_INSERT_FIELDS "BSDU,SDAC,SDAY,ABFR,ABTO,STFU,DRRN"
#define DRA_INSERT_HOST ":VBSDU,:VSDAC,:VSDAY,:VABFR,:VABTO,:VSTFU,:VDRRN"
#define DRA_UPDATE_FIELDS  "SDAC=:VSDAC,ABFR=:VABFR,ABTO=:VABTO,DRRN=:VDRRN"
#define DRA_FIELDS "SDAC,ABFR,ABTO,DRRN"

#define DRR_INSERT_FIELDS "BSDU,SCOD,SCOO,SDAY,AVFR,AVTO,STFU,DRRN,ROSS,ROSL,SBFR,SBTO,SBLU,DRS2"
#define DRR_INSERT_HOST ":VBSDU,:VSCOD,:VSCOO,:VSDAY,:VAVFR,:VAVTO,:VSTFU,:VDRRN,:VROSS,:VROSL,:VSBFR,:VSBTO,:VSBLU,:VDRS2"
#define DRR_UPDATE_FIELDS "BSDU=:VBSDU,SCOD=:VSCOD,AVFR=:VAVFR,AVTO=:VAVTO,SBLU=:VSBLU,SBFR=:VSBFR,SBTO=:VSBTO,FCTC=:VFCTC,DRS2=:VDRS2,SCOO=:VSCOO,DRS1=:VDRS1,DRS3=:VDRS3,DRS4=:VDRS4"
#define DRR_SEND_FIELDS "BSDU,SCOD,AVFR,AVTO,SBLU,SBFR,SBTO,FCTC,DRS2,SCOO,DRS1,DRS3,DRS4"
#define DRR_DELETE_FIELDS "BSDU=:VBSDU,SCOD=:VSCOD,SCOO=:VSCOO,SBFR=:VSBFR,SBTO=:VSBTO,SBLU=:VSBLU,AVFR=:VAVFR,AVTO=:VAVTO"
#define DRR_DEL_FIELDS "BSDU,SCOD,SCOO,SBFR,SBTO,SBLU,AVFR,AVTO"

#define COT_FIELDS "URNO,CTRC"
#define ORG_FIELDS "URNO,DPT1"
#define ODA_FIELDS "URNO,SDAC,FREE,SDAS,TYPE,ABFR,ABTO"

static char cgCotFields[256] = COT_FIELDS;
static char cgOrgFields[256] = ORG_FIELDS;
static char cgOdaFields[256] = ODA_FIELDS;

/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */

extern FILE *outp;
int  debug_level = TRACE;

extern errno;

/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static int   igLineNo     = 0;                   /* length of incoming item */
static EVENT *prgOutEvent  = NULL;
static int   igInitOK      = FALSE;
static long  lgEvtCnt      = 0;
static char  cgConfigFile[512];
static char  cgHopo[8];                         /* default home airport    */
static char  cgBrkUjty[24] = "";                         /* default home airport    */
static char  cgTabEnd[8];                       /* default table extension */
static	int  igStartUpMode = TRACE;
static	int  igRuntimeMode = 0;
static char  cgProcessName[80];                  /* name of executable */
static char  cgErrorString[1024];
static char cgUpdEmpSequence[12] = "Y";
static char cgUpdAbsSequence[12] = "Y";
static int igExportOffset = -1;
static int igSubChangesOffset = -1;
static char cgAlertName[124] = "SAP HR";
static char cgTohr[4] = "Y";
static char cgSetTohr[4] = "Y";

static char cgCmdBuf[240];
static char cgSelection[50*1024];
static char cgFldBuf[8196];
static char cgSqlBuf[5*1024];
static char cgTwStart[128];
static char cgTwEnd[128];
static char cgRecvName[128];
static char cgDestName[128];
static int igQueOut;

static int igBchdl = 1900;
static int igAction = 7400;
static int igLoghdl = 7700;

static char *pcgStfuList = NULL;
static long lgStfuListLength = 50000;
static long lgStfuListActualLength = 0;

static char cgDrrBuf[2048];
static char cgErrMsg[1024];
static char cgCfgBuffer[256];
static char cgDataArea[5*1024];

static BOOL bgBcActiv = TRUE;
static BOOL bgBreak = FALSE;
static BOOL bgSendBroadcast = TRUE;

static char cgNewRegi[6] = "N";
static char *pcgRegiDeleted = NULL;
static int  igRegiDeletedLength = 0;
static int  igRegiDeletedUsed = 0;

static char cgSapSubDef[6] = "2";
static char cgAbsSubDef[6] = "1";
static char cgShiftSubDef[6] = "1";

static char cgDrdFileId[6] = "DRDF";
static char cgLtrFileId[6] = "LTRF";
static char cgEmpFileId[6] = "EMPF";
static char cgAbsFileId[6] = "ABSF";
static char cgAttFileId[6] = "ATTF";

static char cgFileBase[256] = "/ceda/exco/SAP/";
static char cgFileNew[24] = "new/";
static char cgFileArc[24] = "arc/";
static char cgFileLog[24] = "log/";
static char cgFileError[24] = "error/";
static char cgTmpPath[1024] = "tmp/";
static char cgExportPath[1024] = "export/";
static char cgInputFilePath[312];
static char cgInputFileName[312];
static char cgLineBuf[1024];
static char cgArcFileName[312];
static char cgRegi[36];
static char cgLastLstu[16];

static long lgSuccessCount;
static long lgErrorCount;

static FILE *pfgInput = NULL;
static FILE *pfgError = NULL;
static FILE *pfgLog = NULL;
static FILE *pfgTmpFile = NULL;
static char cgTmpFilePath[512] = "";

struct _AbsenceStruct
{
	char Peno[32];
	char Code[12];
	char FullDayIndicator[12];
	char StartDate[24];
	char EndDate[24];
	char StartTime[24];
	char EndTime[24];
	char UpdateIndicator[4];
	char Start[32];
	char End[32];
} rgAbsenceData,rgAttendanceData;

typedef struct _AbsenceStruct ABSENCE;


/* static array informations */

#define ARR_NAME_LEN   (18)
#define ARR_FLDLST_LEN (256)

struct _arrayinfo {
	HANDLE   rrArrayHandle;
	char     crArrayTableName[ARR_NAME_LEN+1];
	char     crArrayName[ARR_NAME_LEN+1];
	char     crArrayFieldList[ARR_FLDLST_LEN+1];
	long     lrArrayFieldCnt;
	char   *pcrArrayRowBuf;
	long     lrArrayRowLen;
	long   *plrArrayFieldOfs;
	long   *plrArrayFieldLen;
	HANDLE   rrIdx01Handle;
	char     crIdx01Name[ARR_NAME_LEN+1];
	char     crIdx01FieldList[ARR_FLDLST_LEN+1];
	long     lrIdx01FieldCnt;
	char   *pcrIdx01RowBuf;
	long     lrIdx01RowLen;
	long   *plrIdx01FieldPos;
	long   *plrIdx01FieldOrd;

/*****************/
	HANDLE   rrIdx02Handle;
	char     crIdx02Name[ARR_NAME_LEN+1];
	char     crIdx02FieldList[ARR_FLDLST_LEN+1];
	long     lrIdx02FieldCnt;
	char   *pcrIdx02RowBuf;
	long     lrIdx02RowLen;
	long   *plrIdx02FieldPos;
	long   *plrIdx02FieldOrd;
	/****************************/
	HANDLE   rrIdx03Handle;
	char     crIdx03Name[ARR_NAME_LEN+1];
	char     crIdx03FieldList[ARR_FLDLST_LEN+1];
	long     lrIdx03FieldCnt;
	char   *pcrIdx03RowBuf;
	long     lrIdx03RowLen;
	long   *plrIdx03FieldPos;
	long   *plrIdx03FieldOrd;
};
typedef struct _arrayinfo ARRAYINFO;

static ARRAYINFO rgBsdArray;
static ARRAYINFO rgOdaArray;
static ARRAYINFO rgDrrArray;
static ARRAYINFO rgDrrPlanArray;
static ARRAYINFO rgCotArray;
static ARRAYINFO rgOrgArray;
static ARRAYINFO rgStfArray;
static ARRAYINFO rgJobArray;
static ARRAYINFO rgSreArray;


static int igDrrAVFR;
static int igDrrAVTO;
static int igDrrBKDP;
static int igDrrBSDU;
static int igDrrBUFU;
static int igDrrCDAT;
static int igDrrDRRN;
static int igDrrDRS1;
static int igDrrDRS2;
static int igDrrDRS3;
static int igDrrDRS4;
static int igDrrDRSF;
static int igDrrEXPF;
static int igDrrFCTC;
static int igDrrHOPO;
static int igDrrLSTU;
static int igDrrREMA;
static int igDrrROSL;
static int igDrrROSS;
static int igDrrSBFR;
static int igDrrSBLU;
static int igDrrSBLP;
static int igDrrSBTO;
static int igDrrSCOD;
static int igDrrSCOO;
static int igDrrSDAY;
static int igDrrSTFU;
static int igDrrURNO;
static int igDrrUSEC;
static int igDrrUSEU;
static int igDrrUSED;

static int igCotURNO;
static int igCotCTRC;

static int igOrgURNO;
static int igOrgDPT1;

static int igStfURNO;
static int igStfPENO;
static int igStfTOHR;

static int igOdaURNO;
static int igOdaABFR;
static int igOdaABTO;
static int igOdaSDAC;
static int igOdaFREE;
static int igOdaTYPE;
static int igOdaSDAS;


static int igDrrPlanURNO;
static int igDrrPlanAVFR;
static int igDrrPlanAVTO;
static int igDrrPlanSCOD;
static int igDrrPlanSTFU;


static int igJobACFR;
static int igJobACTO;
static int igJobUDSR;

static int igSreSURN;
static int igSreVPFR;
static int igSreVPTO;


static int igBsdBSDC;
static int igBsdESBG;
static int igBsdLSEN;
static int igBsdURNO;
static int igBsdBKD1;

/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
int check_ret1(int ipRc,int ipLine);
static int  InitSaphdl();
static long GetLengtOfMonth(int ipMonth,int ipYear);

static int SetIndexInfo(ARRAYINFO *prpArrayInfo,char *pcpFieldList,int ilIndex);
static int SetArrayInfo(char *pcpArrayName, char *pcpTableName, 
		char *pcpArrayFieldList,char *pcpAddFields,long *pcpAddFieldLens,
			ARRAYINFO *prpArrayInfo,char *pcpSelection,BOOL bpFill);
static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,
	char *pcpCfgBuffer);

static int GetItemNbr(char *Item, char *Data, char Delim, long Nbr);
static int ItemCnt(char *pcgData, char cpDelim);
static void StringPut(char *pcpDest,char *pcpSource,long lpLen);
static long GetSequence(char *pcpKey,char *pcpMonth);
static int GetFileNames(short spFunc,char *pcpFileId,char *pcpFileDir,char *pcpFileName,BOOL bpForExport);

static BOOL RelevantChange(char *pcpData,char *pcpOldData,char *pcpFieldList,char *pcpRelevantFields);
static BOOL RelevantFields(char *pcpFieldList,char *pcpRelevantFields);
static BOOL RelevantChangeJob(char *pcpData,char *pcpOldData,char *pcpFieldList);

static int SendAnswer(int ipDest, char *pcpAnswer);

extern int get_no_of_items(char *s);
extern int itemCount(char *);

static int	Reset(void);                       /* Reset program          */
static void	Terminate(int ipSleep);            /* Terminate program      */
/*static void	HandleSignal(int); */                /* Handles signals        */
static void	HandleErr(int);                    /* Handles general errors */
static void	HandleQueErr(int);                 /* Handles queuing errors */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/

static void TrimRight(char *pcpBuffer);
static int  GetFieldLength(char *pcpHopo, char *pcpTana, char *pcpFina,
							long *pipLen);
static int  GetRowLength(char *pcpHopo, char *pcpTana, char *pcpFieldList,
							long *pipLen);

static int  GetCommand(char *pcpCommand, int *pipCmd);

static int HandleData(EVENT *prpEvent);       /* Handles event data     */
static int SaveIndexInfo (ARRAYINFO *prpArrayInfo);
static int MySaveIndexInfo (ARRAYINFO *prpArrayInfo);

static void IncrementCount(int ipRc);
static void MoveToErrorFile(char *pcpLineBuf);
static int LogError(char *pcpFileName,char *pcpErrorString,BOOL bpAlert);
static int StrToTime(char *pcpTime,time_t *pclTime);
static void  StrAddTime(char *,char*,char *);
static int MyNewToolsSendSql(int ipRouteID,
		BC_HEAD *prpBchd,
		CMDBLK *prpCmdblk,
		char *pcpSelection,
		char *pcpFields,
		char *pcpData);
static int TriggerAction(char *pcpTableName,char *pcpFields);
static int  WaitAndCheckQueue(int ipTimeout, int ipModId, ITEM **prpItem);
static int  CheckQueue(int ipModId, ITEM **prpItem);
static int FindItemInArrayList(char *pcpInput, char *pcpPattern,char cpDel,int *ipLine,int *ipCol,int *ipPos);

static int GetDebugLevel(char *, int *);
static int GetBreakTimeDiff (char *pcpLocal1, char *pcpLocal2, time_t *plpDiff );
static int DumpArray(ARRAYINFO *prpArray,char *pcpFields);
static int DoSelect(short spFunction,short *pspCursor,char *pcpTable,char *pcpSelection,char *pcpFields,char *pcpDataArea);
static int DoSingleSelect(char *pcpTable,char *pcpSelection,char *pcpFields,char *pcpDataArea);

static int WriteTrailer(FILE *pflFile);
static int WriteHeader(FILE *pflFile,char *pcpFileId,char *pcpFileName,long lpLineCount);
static int MakeFileName(char *pcpFileId,long plSequence,char *pcpFileName);
static int ExportDailyRoster(FILE *pfpFile,char *pcpSday, char *pcpStaffUrno);
static int AddToUrnoList(char *pcpUrno,char *pcpList,long lpListLength,long *plpActualLength);

static void StringTrimRight(char *pcpBuffer);
static int CheckDateFormat(char *pcpDate);
static void PutSequence(char *pcpKey,long lpSequence,char *pcpMonth);

static void StringMid(char *pcpDest,char *pcpSource,long lpFrom,long lpLen,int ipConvert);
static int IsAbsenceCode(char *pcpCode, BOOL *bpOffRd);
static int CheckSre(char *pcpSurn,char *pcpSday);
static int CheckRegi(char *pcpSurn,char *pcpSday);
static BOOL CheckTohr(char *pcpSurn);
static BOOL DeleteStfRow(char *pcpSurn);
static int GetOvertime(char *pcpBsdc,char *pcpSday,char *pcpFrom,char *pcpTo,char *pcpDrs1,char *pcpDrs3,char *pcpDrs4);
static int GetSbluFromBSDTAB(char *pcpBsdc,char *pcpSblu);
static int DeleteDra(ABSENCE *prpAbsence,char *pcpStfUrno,char *pcpDrrn);
static int DeleteDrr(ABSENCE *prpAbsence,char *pcpStfUrno);
static int	DoUpdate(char *pcpTable,char *pcpSelection,char *pcpUpdateFields,
	char *pcpSendFields,char *pcpUrno,char *pcpDataArea,BOOL bpBroadcast);
static int DoInsert(char *pcpTable,char *pcpFields,
					char *pcpInsertFields,char *pcpDataArea,BOOL bpBroadcast);
static int DoInsert2(char *pcpTable,char *pcpFields,
					char *pcpInsertFields,char *pcpDataArea,BOOL bpBroadcast);
static int	DoUpdate2(char *pcpTable,char *pcpSelection,char *pcpUpdateFields,
	char *pcpSendFields,char *pcpUrno,char *pcpDataArea,BOOL bpBroadcast);
static int WriteSubsequentChanges(FILE *pfpFile,char *pcpDrrUrno);
static int ExportSubsequentChanges(char *pcpDrrUrno);
static int OpenTmpFile(BOOL bpRead);
static int GetItem(char* pcpTarget,char* pcpData,char* pcpFieldlist,char* pcpField,int piDeleteBlanks);

static int TimeToStr(char *pcpTime,time_t lpTime)
{
  struct tm *_tm;
 
  _tm = (struct tm *)localtime(&lpTime);
  sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
      _tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
      _tm->tm_min,_tm->tm_sec);
  return (_tm->tm_wday == 0 ? 7 : _tm->tm_wday) + '0';
}                                                                                

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
	int	ilRc = RC_SUCCESS;			/* Return code			*/
	int	ilCnt = 0;
	int ilOldDebugLevel;

	/************************
	FILE *prlErrFile = NULL;
	char clErrFile[512];
	************************/

	INITIALIZE;			/* General initialization	*/

	/****************************************************************************
	sprintf(&clErrFile[0],"%s/%s%5.5d.err",getenv("DBG_PATH"),mod_name,getpid());
	prlErrFile = freopen(&clErrFile[0],"w",stderr);
	fprintf(stderr,"HoHoHo\n");fflush(stderr);
	dbg(TRACE,"MAIN: error file <%s>",&clErrFile[0]);
	****************************************************************************/


	strcpy(cgProcessName,argv[0]);

	dbg(TRACE,"MAIN: version <%s>",mks_version);

	/* Attach to the MIKE queues */
	do
	{
		ilRc = init_que();
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		}/* end of if */
	}while((ilCnt < 10) && (ilRc != RC_SUCCESS));

	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
		sleep(60);
		exit(1);
	}else{
		dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
	}/* end of if */

	do
	{
		ilRc = init_db();
		if (ilRc != RC_SUCCESS)
		{
			check_ret(ilRc);
			dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		} /* end of if */
	} while((ilCnt < 10) && (ilRc != RC_SUCCESS));

	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
		sleep(60);
		exit(2);
	}else{
		dbg(TRACE,"MAIN: init_db() OK!");
	} /* end of if */

	/* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */

	sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
	dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
	ilRc = TransferFile(cgConfigFile);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */

	sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
	dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
	ilRc = TransferFile(cgConfigFile);
	if(ilRc != RC_SUCCESS)
	{ 
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */

	ilRc = SendRemoteShutdown(mod_id);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
	} /* end of if */

	if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
	{
		dbg(DEBUG,"MAIN: waiting for status switch ...");
		HandleQueues();
	}/* end of if */

	if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
	{
		dbg(TRACE,"MAIN: initializing ...");
		if(igInitOK == FALSE)
		{
			ilRc = InitSaphdl();
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitSaphdl: init failed!");
			} /* end of if */
		}/* end of if */
	} else {
		Terminate(30);
	}/* end of if */

/***
	dbg(TRACE,"MAIN: initializing OK sleep(60) ...");
	sleep(60);
***/

	dbg(TRACE,"=====================");
	dbg(TRACE,"MAIN: initializing OK");
	dbg(TRACE,"=====================");
	
	debug_level = igRuntimeMode;

	for(;;)
	{
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;
				
		if( ilRc == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRc != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRc);
			} /* fi */
			
			lgEvtCnt++;

			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				break;	
			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				/* CloseConnection(); */
				HandleQueues();
				break;	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate(1);
				break;	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				break;	
			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;
			case	SHUTDOWN	:
				/* process shutdown - maybe from uutil */
				Terminate(1);
				break;
					
			case	RESET		:
				ilRc = Reset();
				break;
					
			case	EVENT_DATA	:
				if((ctrl_sta == HSB_STANDALONE) || 
					(ctrl_sta == HSB_ACTIVE) || 
					(ctrl_sta == HSB_ACT_TO_SBY))
				{
					ilRc = HandleData(prgEvent);
					if(ilRc != RC_SUCCESS)
					{
						HandleErr(ilRc);
					}/* end of if */
				}else{
					dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
				}/* end of if */
				break;
				
			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;
			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;
			default			:
				dbg(TRACE,"MAIN: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		} /* end else */
		
	} /* end for */
	
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/

static int FindItemInArrayList(char *pcpInput, char *pcpPattern,char cpDel,int *ipLine,int *ipCol,int *ipPos)
{
	int ilRc = RC_SUCCESS;

	ilRc = FindItemInList(pcpInput,pcpPattern,cpDel,ipLine,ipCol,ipPos);
	*ipLine = (*ipLine) -1;
	
	return ilRc;
}



static int InitFieldIndex()
{
	int ilRc = RC_SUCCESS;
	int ilCol,ilPos;
	int ilDBFields;

	/*FindItemInArrayList(DEM_FIELDS,"ALID",',',&igDemALID,&ilCol,&ilPos);*/


	FindItemInArrayList(cgDrrFields,"AVFR",',',&igDrrAVFR,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"AVTO",',',&igDrrAVTO,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"BKDP",',',&igDrrBKDP,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"BSDU",',',&igDrrBSDU,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"BUFU",',',&igDrrBUFU,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"CDAT",',',&igDrrCDAT,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"DRRN",',',&igDrrDRRN,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"DRS1",',',&igDrrDRS1,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"DRS2",',',&igDrrDRS2,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"DRS3",',',&igDrrDRS3,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"DRS4",',',&igDrrDRS4,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"DRSF",',',&igDrrDRSF,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"EXPF",',',&igDrrEXPF,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"FCTC",',',&igDrrFCTC,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"HOPO",',',&igDrrHOPO,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"LSTU",',',&igDrrLSTU,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"REMA",',',&igDrrREMA,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"ROSL",',',&igDrrROSL,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"ROSS",',',&igDrrROSS,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"SBFR",',',&igDrrSBFR,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"SBLP",',',&igDrrSBLP,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"SBLU",',',&igDrrSBLU,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"SBTO",',',&igDrrSBTO,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"SCOD",',',&igDrrSCOD,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"SCOO",',',&igDrrSCOO,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"SDAY",',',&igDrrSDAY,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"STFU",',',&igDrrSTFU,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"URNO",',',&igDrrURNO,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"USEC",',',&igDrrUSEC,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrFields,"USEU",',',&igDrrUSEU,&ilCol,&ilPos);

	FindItemInArrayList(cgCotFields,"URNO",',',&igCotURNO,&ilCol,&ilPos);
	FindItemInArrayList(cgCotFields,"CTRC",',',&igCotCTRC,&ilCol,&ilPos);

	FindItemInArrayList(cgOrgFields,"URNO",',',&igOrgURNO,&ilCol,&ilPos);
	FindItemInArrayList(cgOrgFields,"DPT1",',',&igOrgDPT1,&ilCol,&ilPos);

	FindItemInArrayList(cgStfFields,"URNO",',',&igStfURNO,&ilCol,&ilPos);
	FindItemInArrayList(cgStfFields,"PENO",',',&igStfPENO,&ilCol,&ilPos);
	FindItemInArrayList(cgStfFields,"TOHR",',',&igStfTOHR,&ilCol,&ilPos);

	FindItemInArrayList(cgOdaFields,"URNO",',',&igOdaURNO,&ilCol,&ilPos);
	FindItemInArrayList(cgOdaFields,"SDAC",',',&igOdaSDAC,&ilCol,&ilPos);
	FindItemInArrayList(cgOdaFields,"FREE",',',&igOdaFREE,&ilCol,&ilPos);
	FindItemInArrayList(cgOdaFields,"SDAS",',',&igOdaSDAS,&ilCol,&ilPos);
	FindItemInArrayList(cgOdaFields,"TYPE",',',&igOdaTYPE,&ilCol,&ilPos);
	FindItemInArrayList(cgOdaFields,"ABFR",',',&igOdaABFR,&ilCol,&ilPos);
	FindItemInArrayList(cgOdaFields,"ABTO",',',&igOdaABTO,&ilCol,&ilPos);

	FindItemInArrayList(cgDrrPlanFields,"URNO",',',&igDrrPlanURNO,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrPlanFields,"AVFR",',',&igDrrPlanAVFR,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrPlanFields,"AVTO",',',&igDrrPlanAVTO,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrPlanFields,"SCOD",',',&igDrrPlanSCOD,&ilCol,&ilPos);
	FindItemInArrayList(cgDrrPlanFields,"STFU",',',&igDrrPlanSTFU,&ilCol,&ilPos);

	FindItemInArrayList(cgJobFields,"ACFR",',',&igJobACFR,&ilCol,&ilPos);
	FindItemInArrayList(cgJobFields,"ACTO",',',&igJobACTO,&ilCol,&ilPos);
	FindItemInArrayList(cgJobFields,"UDSR",',',&igJobUDSR,&ilCol,&ilPos);

	FindItemInArrayList(cgSreFields,"SURN",',',&igSreSURN,&ilCol,&ilPos);
	FindItemInArrayList(cgSreFields,"VPFR",',',&igSreVPFR,&ilCol,&ilPos);
	FindItemInArrayList(cgSreFields,"VPTO",',',&igSreVPTO,&ilCol,&ilPos);

	FindItemInArrayList(cgBsdFields,"URNO",',',&igBsdURNO,&ilCol,&ilPos);
	FindItemInArrayList(cgBsdFields,"BSDC",',',&igBsdBSDC,&ilCol,&ilPos);
	FindItemInArrayList(cgBsdFields,"ESBG",',',&igBsdESBG,&ilCol,&ilPos);
	FindItemInArrayList(cgBsdFields,"LSEN",',',&igBsdLSEN,&ilCol,&ilPos);
	FindItemInArrayList(cgBsdFields,"BKD1",',',&igBsdBKD1,&ilCol,&ilPos);

	return ilRc;

}


static int ReadConfigLine(char *pcpSection,char *pcpKeyword,
	char *pcpCfgBuffer)
{
	int ilRc = RC_SUCCESS;

	char clSection[124];
	char clKeyword[124];
	char clBuffer[256];
	char *pclEquals;

	strcpy(clSection,pcpSection);
	strcpy(clKeyword,pcpKeyword);

	ilRc = iGetConfigLine(cgConfigFile,clSection,clKeyword,clBuffer);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
	} 
	else
	{
			pclEquals = strchr(clBuffer,'=');
			if (pclEquals != NULL)
			{
				strcpy(pcpCfgBuffer,&pclEquals[2]);
			}
			else
			{
				strcpy(pcpCfgBuffer,clBuffer);
			}
			StringTrimRight(pcpCfgBuffer);
			dbg(DEBUG,"Config Line <%s>,<%s>:<%s> found in %s",
					&clSection[0],&clKeyword[0],pcpCfgBuffer,cgConfigFile);
	}/* end of if */
	return ilRc;
}

static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,
	char *pcpCfgBuffer)
{
	int ilRc = RC_SUCCESS;

	char clSection[124];
	char clKeyword[124];

	strcpy(clSection,pcpSection);
	strcpy(clKeyword,pcpKeyword);

	ilRc = iGetConfigEntry(cgConfigFile,clSection,clKeyword,
			CFG_STRING,pcpCfgBuffer);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
	} 
	else
	{
			dbg(DEBUG,"Config Entry <%s>,<%s>:<%s> found in %s",
					&clSection[0],&clKeyword[0],pcpCfgBuffer,cgConfigFile);
	}/* end of if */
	return ilRc;
}


static int InitSaphdl()
{
	int  ilRc = RC_SUCCESS;			/* Return code */
  char clSection[64];
  char clKeyword[64];
  char clNow[64];
	time_t tlNow;
	int ilOldDebugLevel;
	long pclAddFieldLens[12];
	char pclAddFields[256];
	char pclSqlBuf[2560];
	char pclSelection[1024];
 	short slCursor;
	short slSqlFunc;
	char  clBreak[24];
	char  clTmpStr[24];
	int ilLc;
	int ilTmpModId;

	tlNow = time(NULL);
	tlNow -= tlNow % 86400;

	TimeToStr(clNow,tlNow);

	dbg(TRACE,"Now = <%s>",clNow);

	/* now reading from configfile or from database */


	if ((ilTmpModId = tool_get_q_id("bchdl")) == RC_SUCCESS)
	{
		igBchdl = ilTmpModId;
		dbg(TRACE,"InitSaphdl: Mod-Id for <bchdl> set to %d",igBchdl);
	}
	else
	{
		dbg(TRACE,"InitSaphdl: Mod-Id for <bchdl> not found, set to default %d",igBchdl);
	}

	if ((ilTmpModId = tool_get_q_id("action")) == RC_SUCCESS)
	{
		igAction = ilTmpModId;
		dbg(TRACE,"InitSaphdl: Mod-Id for <action> set to %d",igAction);
	}
	else
	{
		dbg(TRACE,"InitSaphdl: Mod-Id for <action> not found, set to default %d",igAction);
	}


	if ((ilTmpModId = tool_get_q_id("loghdl")) == RC_SUCCESS)
	{
		igLoghdl = ilTmpModId;
		dbg(TRACE,"InitSaphdl: Mod-Id for <loghdl> set to %d",igLoghdl);
	}
	else
	{
		dbg(TRACE,"InitSaphdl: Mod-Id for <loghdl> not found, set to default %d",igLoghdl);
	}


	if(ilRc == RC_SUCCESS)
	{
		/* read HomeAirPort from SGS.TAB */
		memset((void*)&cgHopo[0], 0x00, 8);

		sprintf(&clSection[0],"SYS");
		sprintf(&clKeyword[0],"HOMEAP");

		ilRc = tool_search_exco_data(&clSection[0], &clKeyword[0], &cgHopo[0]);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"<InitSaphdl> EXTAB,%s,%s not found in SGS.TAB",&clSection[0], &clKeyword[0]);
			Terminate(30);
		} else {
			dbg(TRACE,"<InitSaphdl> home airport    <%s>",&cgHopo[0]);
		}/* end of if */
	}/* end of if */

	
	if(ilRc == RC_SUCCESS)
	{
		sprintf(&clSection[0],"ALL");
		sprintf(&clKeyword[0],"TABEND");

		ilRc = tool_search_exco_data(&clSection[0], &clKeyword[0], &cgTabEnd[0]);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"<InitSaphdl> EXTAB,%s,%s not found in SGS.TAB",&clSection[0], &clKeyword[0]);
			Terminate(30);
		} else {
			dbg(TRACE,"<InitSaphdl> table extension <%s>",&cgTabEnd[0]);
		}/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		 GetDebugLevel("STARTUP_MODE", &igStartUpMode);
		 GetDebugLevel("RUNTIME_MODE", &igRuntimeMode);
		 
		 debug_level = igStartUpMode;


		ilRc = ReadConfigEntry("SYSTEM","SAPSUBDEF",cgSapSubDef);
		if(ilRc == RC_SUCCESS)
		{
			dbg(TRACE,"SAPSUBDEF set to <%s>",cgSapSubDef);
		}
		else
		{
			dbg(TRACE,"SAPSUBDEF not found, set to <%s>",cgSapSubDef);
			ilRc = RC_SUCCESS;
		}

		ilRc = ReadConfigEntry("SYSTEM","ABSSUBDEF",cgAbsSubDef);
		if(ilRc == RC_SUCCESS)
		{
			dbg(TRACE,"ABSSUBDEF set to <%s>",cgAbsSubDef);
		}
		else
		{
			dbg(TRACE,"ABSSUBDEF not found, set to <%s>",cgAbsSubDef);
			ilRc = RC_SUCCESS;
		}

		ilRc = ReadConfigEntry("SYSTEM","SHIFTSUBDEF",cgShiftSubDef);
		if(ilRc == RC_SUCCESS)
		{
			dbg(TRACE,"SHIFTSUBDEF set to <%s>",cgShiftSubDef);
		}
		else
		{
			dbg(TRACE,"SHIFTSUBDEF not found, set to <%s>",cgShiftSubDef);
			ilRc = RC_SUCCESS;
		}

		ilRc = ReadConfigLine("SYSTEM","TOHR",cgTohr);
		if(ilRc == RC_SUCCESS)
		{
			dbg(TRACE,"TOHR set to <%s>",cgTohr);
		}
		else
		{
			strcpy(cgTohr,"Y");
			dbg(TRACE,"TOHR not found, set to <%s>",cgTohr);
			ilRc = RC_SUCCESS;
		}
 	 	ilRc = ReadConfigLine("MAIN","DrrReloadHintStfu",cgDrrReloadHintStfu);
   	if ( ilRc != RC_SUCCESS )
		{
      strcpy (cgDrrReloadHintStfu, "/*+ INDEX(DRRTAB DRRTAB_SDAY_ROSL) */");
		}
    dbg(TRACE,"InitPolhdl: Hint for reloading DRRTAB with ROSL <%s>", 
	 		cgDrrReloadHintStfu );
 	 	ilRc = ReadConfigLine("MAIN","DrrReloadHintRosl",cgDrrReloadHintRosl);
   	if ( ilRc != RC_SUCCESS )
		{
      strcpy (cgDrrReloadHintRosl, "/*+ INDEX(DRRTAB DRRTAB_SDAY_ROSL) */");
		}
    dbg(TRACE,"InitPolhdl: Hint for reloading DRRTAB with ROSL <%s>", 
	 		cgDrrReloadHintRosl );

	
		ilRc = ReadConfigLine("SYSTEM","SETTOHR",cgSetTohr);
		if(ilRc == RC_SUCCESS)
		{
			dbg(TRACE,"SETTOHR set to <%s>",cgSetTohr);
		}
		else
		{
			strcpy(cgSetTohr,"Y");
			dbg(TRACE,"SETTOHR not found, set to <%s>",cgSetTohr);
			ilRc = RC_SUCCESS;
		}

		if (*cgSetTohr == 'Y')
		{
			strcat(cgStfInsertFields,",TOHR");
			strcat(cgStfInsertHost,",:VTOHR");
			strcat(cgStfUpdateFields,",TOHR");
			strcat(cgStfUpdateHost,",TOHR=:VTOHR");
		}

		dbg(DEBUG,"StfInsertFields: <%s>",cgStfInsertFields);
		dbg(DEBUG,"StfInsertHost: <%s>",cgStfInsertHost);
		dbg(DEBUG,"StfUpdateFields: <%s>",cgStfUpdateFields);
		dbg(DEBUG,"StfUpdateHost: <%s>",cgStfUpdateHost);

		ilRc = ReadConfigEntry("FILEIDS","LTR",cgLtrFileId);
		ilRc = ReadConfigEntry("FILEIDS","LTR",cgLtrFileId);
		ilRc = ReadConfigEntry("FILEIDS","LTR",cgLtrFileId);
		ilRc = ReadConfigEntry("FILEIDS","DRD",cgDrdFileId);
		if(ilRc == RC_SUCCESS)
		{
			dbg(TRACE,"FILEID DRD set to <%s>",cgDrdFileId);
		}
		else
		{
			dbg(TRACE,"FILEID DRD not found, set to <%s>",cgDrdFileId);
			ilRc = RC_SUCCESS;
		}

		ilRc = ReadConfigEntry("FILEIDS","LTR",cgLtrFileId);
		if(ilRc == RC_SUCCESS)
		{
			dbg(TRACE,"FILEID LTR set to <%s>",cgLtrFileId);
		}
		else
		{
			dbg(TRACE,"FILEID LTR not found, set to <%s>",cgLtrFileId);
			ilRc = RC_SUCCESS;
		}
	
		ilRc = ReadConfigEntry("FILEIDS","EMP",cgEmpFileId);
		if(ilRc == RC_SUCCESS)
		{
			dbg(TRACE,"FILEID EMP set to <%s>",cgEmpFileId);
		}
		else
		{
			dbg(TRACE,"FILEID EMP not found, set to <%s>",cgEmpFileId);
			ilRc = RC_SUCCESS;
		}
	
		ilRc = ReadConfigEntry("FILEIDS","ABS",cgAbsFileId);
		if(ilRc == RC_SUCCESS)
		{
			dbg(TRACE,"FILEID ABS set to <%s>",cgAbsFileId);
		}
		else
		{
			dbg(TRACE,"FILEID ABS not found, set to <%s>",cgAbsFileId);
			ilRc = RC_SUCCESS;
		}

		ilRc = ReadConfigEntry("FILEIDS","ATT",cgAttFileId);
		if(ilRc == RC_SUCCESS)
		{
			dbg(TRACE,"FILEID ATT set to <%s>",cgAttFileId);
		}
		else
		{
			dbg(TRACE,"FILEID ATT not found, set to <%s>",cgAttFileId);
			ilRc = RC_SUCCESS;
		}

		ilRc = ReadConfigEntry("DIRECTORIES","FILEBASE",cgFileBase);
		if(ilRc == RC_SUCCESS)
		{
			dbg(TRACE,"FILEID FILEBASE set to <%s>",cgFileBase);
		}
		else
		{
			dbg(TRACE,"FILEID FILEBASE not found, set to <%s>",cgFileBase);
			ilRc = RC_SUCCESS;
		}

		ilRc = ReadConfigEntry("DIRECTORIES","NEW",cgFileNew);
		if(ilRc == RC_SUCCESS)
		{
			dbg(TRACE,"FILEID NEW set to <%s>",cgFileNew);
		}
		else
		{
			dbg(TRACE,"FILEID NEW not found, set to <%s>",cgFileNew);
			ilRc = RC_SUCCESS;
		}

		ilRc = ReadConfigEntry("MAIN","SHIFTINDICATOR",cgRegi);
		if(ilRc == RC_SUCCESS)
		{
			dbg(TRACE,"Shift indicator to send set to <%s>",cgRegi);
		}
		else
		{
			dbg(TRACE,"Shift indicator to send not found, set to <%s>",cgRegi);
			ilRc = RC_SUCCESS;
		}


		ilRc = ReadConfigEntry("DIRECTORIES","ARC",cgFileArc);
		if(ilRc == RC_SUCCESS)
		{
			dbg(TRACE,"FILEID ARC set to <%s>",cgFileArc);
		}
		else
		{
			dbg(TRACE,"FILEID ARC not found, set to <%s>",cgFileArc);
			ilRc = RC_SUCCESS;
		}

		ilRc = ReadConfigEntry("DIRECTORIES","LOG",cgFileLog);
		if(ilRc == RC_SUCCESS)
		{
			dbg(TRACE,"Directory for LOG files set to <%s>",cgFileLog);
		}
		else
		{
			dbg(TRACE,"Directory for LOG files not found, set to <%s>",cgFileLog);
			ilRc = RC_SUCCESS;
		}
		ilRc = ReadConfigEntry("DIRECTORIES","ERROR",cgFileError);
		if(ilRc == RC_SUCCESS)
		{
			dbg(TRACE,"Directory for ERROR files set to <%s>",cgFileError);
		}
		else
		{
			dbg(TRACE,"Directory for ERROR files not found, set to <%s>",cgFileError);
			ilRc = RC_SUCCESS;
		}
		ilRc = ReadConfigEntry("DIRECTORIES","TMP",cgTmpPath);
		if(ilRc == RC_SUCCESS)
		{
			dbg(TRACE,"Directory for temporary files set to <%s>",cgTmpPath);
		}
		else
		{
			dbg(TRACE,"Directory for temporary files not found, set to <%s>",cgTmpPath);
			ilRc = RC_SUCCESS;
		}
		ilRc = ReadConfigEntry("DIRECTORIES","EXPORT",cgExportPath);
		if(ilRc == RC_SUCCESS)
		{
			dbg(TRACE,"Directory for EXPORT files set to <%s>",cgExportPath);
		}
		else
		{
			dbg(TRACE,"Directory for temporary files not found, set to <%s>",cgExportPath);
			ilRc = RC_SUCCESS;
		}
		ilRc = ReadConfigEntry("TEST","EMPSEQUENCE",cgUpdEmpSequence);
		if(ilRc == RC_SUCCESS)
		{
			dbg(TRACE,"Update Emp-Import Sequence set to <%s>",cgUpdEmpSequence);
		}
		else
		{
			strcpy(cgUpdEmpSequence,"Y");
			dbg(TRACE,"Update Emp-Import Sequence not found, set to <%s>",cgUpdEmpSequence);
			ilRc = RC_SUCCESS;
		}
		ilRc = ReadConfigEntry("TEST","ABSSEQUENCE",cgUpdAbsSequence);
		if(ilRc == RC_SUCCESS)
		{
			dbg(TRACE,"Update Absence-Import Sequence set to <%s>",cgUpdAbsSequence);
		}
		else
		{
			strcpy(cgUpdAbsSequence,"Y");
			dbg(TRACE,"Update Absence-Import Sequence not found, set to <%s>",
						cgUpdAbsSequence);
			ilRc = RC_SUCCESS;
		}
		ilRc = ReadConfigEntry("TEST","EXPORTOFFSET",clTmpStr);
		if(ilRc == RC_SUCCESS)
		{
			igExportOffset = atoi(clTmpStr);
			dbg(TRACE,"Offset for daily roster export set to <%d>",igExportOffset);
		}
		else
		{
			igExportOffset = -1;
			dbg(TRACE,"Offset for daily roster export not found, set to <%d>",igExportOffset);
			ilRc = RC_SUCCESS;
		}

		ilRc = ReadConfigEntry("TEST","SUBCHANGESOFFSET",clTmpStr);
		if(ilRc == RC_SUCCESS)
		{
			igSubChangesOffset = atoi(clTmpStr);
			dbg(TRACE,"Offset for export subsequenct changes set to <%d>",igSubChangesOffset);
		}
		else
		{
			igSubChangesOffset = -1;
			dbg(TRACE,"Offset for export subsequenct changes not found, set to <%d>",
					igSubChangesOffset);
			ilRc = RC_SUCCESS;
		}


		ilRc = ReadConfigLine("SYSTEM","ALERT_NAME",cgAlertName);
		if(ilRc == RC_SUCCESS)
		{
			dbg(TRACE,"Alert Name set to <%s>",cgAlertName);
		}
		else
		{
			strcpy(cgAlertName,"Y");
			dbg(TRACE,"Alert Name not found, set to <%s>",
						cgAlertName);
			ilRc = RC_SUCCESS;
		}
		ilRc = ReadConfigLine("SYSTEM","NEWREGI",cgNewRegi);
		if(ilRc == RC_SUCCESS)
		{
			dbg(TRACE,"New Regularity Indicator handling set to <%s>",cgNewRegi);
		}
		else
		{
			dbg(TRACE,"New Regularity Indicator handling set to <%s>",cgNewRegi);
			ilRc = RC_SUCCESS;
		}
		if (*cgNewRegi == 'Y')
		{
				pcgRegiDeleted = malloc(1000+100);
				memset(pcgRegiDeleted,0,1100);
				igRegiDeletedLength = 1000;
				igRegiDeletedUsed = 50;
		}
		else
		{
				pcgRegiDeleted = malloc(100);
				memset(pcgRegiDeleted,0,100);
				igRegiDeletedLength = 100;
				igRegiDeletedUsed = 50;
		}
	}


	if(ilRc == RC_SUCCESS)
	{
		ilRc = CEDAArrayInitialize(20,20);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitSaphdl: CEDAArrayInitialize failed <%d>",ilRc);
		}/* end of if */
	}/* end of if */



	if(ilRc == RC_SUCCESS)
	{

		strcpy(cgSreFields,SRE_FIELDS);
		*pclSelection = '\0';
		dbg(TRACE,"InitSaphdl: SetArrayInfo SRETAB array");
		ilRc = SetArrayInfo("SRETAB","SRETAB",cgSreFields,
				NULL,NULL,&rgSreArray,pclSelection,FALSE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitSaphdl: SetArrayInfo SRETAB failed <%d>",ilRc);
		}
		else
		{
			dbg(DEBUG,"InitSaphdl: SetArrayInfo SRETAB OK");
			dbg(DEBUG,"<%s> %d",rgSreArray.crArrayName,rgSreArray.rrArrayHandle);

			dbg(DEBUG,"<%s> %d",rgSreArray.crArrayName,rgSreArray.rrArrayHandle);
			strcpy(rgSreArray.crIdx01Name,"SURN");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgSreArray.rrArrayHandle,
				rgSreArray.crArrayName,
				&rgSreArray.rrIdx01Handle,
				rgSreArray.crIdx01Name,"SURN");
				strcpy(rgSreArray.crIdx01FieldList,"SURN");
			SetIndexInfo(&rgSreArray,"SURN",1);
		}
	}



	if(ilRc == RC_SUCCESS)
	{

		strcpy(cgBsdFields,BSD_FIELDS);
		*pclSelection = '\0';
		dbg(TRACE,"InitSaphdl: SetArrayInfo BSDTAB array");	if(ilRc == RC_SUCCESS)
	{

		sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);
		dbg(TRACE,"InitSaphdl: SetArrayInfo BSDTAB array \n<%s>",pclSelection);
		ilRc = SetArrayInfo("BSDTAB","BSDTAB",BSD_FIELDS,NULL,NULL,&rgBsdArray,pclSelection,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitSaphdl: SetArrayInfo BSDTAB failed <%d>",ilRc);
		}
		else
		{
			dbg(DEBUG,"InitSaphdl: SetArrayInfo BSDTAB OK");
			dbg(DEBUG,"<%s> %d",rgBsdArray.crArrayName,rgBsdArray.rrArrayHandle);
			strcpy(rgBsdArray.crIdx01Name,"BSD_URNO");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgBsdArray.rrArrayHandle,
				rgBsdArray.crArrayName,
				&rgBsdArray.rrIdx01Handle,
				rgBsdArray.crIdx01Name,"URNO");
				strcpy(rgBsdArray.crIdx01FieldList,"URNO");
			SetIndexInfo(&rgBsdArray,"URNO",1);

			strcpy(rgBsdArray.crIdx02Name,"BSD_BSDC");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgBsdArray.rrArrayHandle,
				rgBsdArray.crArrayName,
				&rgBsdArray.rrIdx02Handle,
				rgBsdArray.crIdx02Name,"BSDC");
				strcpy(rgBsdArray.crIdx02FieldList,"BSDC");
			SetIndexInfo(&rgBsdArray,"BSDC",2);
		}
	}

		ilRc = SetArrayInfo("JOBTAB","JOBTAB",cgJobFields,
				NULL,NULL,&rgJobArray,pclSelection,FALSE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitSaphdl: SetArrayInfo JOBTAB failed <%d>",ilRc);
		}
		else
		{
			dbg(DEBUG,"InitSaphdl: SetArrayInfo JOBTAB OK");
			dbg(DEBUG,"<%s> %d",rgJobArray.crArrayName,rgJobArray.rrArrayHandle);

			dbg(DEBUG,"<%s> %d",rgJobArray.crArrayName,rgJobArray.rrArrayHandle);
			strcpy(rgJobArray.crIdx01Name,"JOBTAB_UDSR");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgJobArray.rrArrayHandle,
				rgJobArray.crArrayName,
				&rgJobArray.rrIdx01Handle,
				rgJobArray.crIdx01Name,"UDSR");
				strcpy(rgJobArray.crIdx01FieldList,"UDSR");
			SetIndexInfo(&rgJobArray,"UDSR",1);
		}
	}

	if(ilRc == RC_SUCCESS)
	{

		strcpy(cgDrrPlanFields,DRRPLANNED_FIELDS);
		*pclSelection = '\0';
		dbg(TRACE,"InitSaphdl: SetArrayInfo DRRTAB array");
		ilRc = SetArrayInfo("DRRPLAN","DRRTAB",cgDrrPlanFields,
				NULL,NULL,&rgDrrPlanArray,pclSelection,FALSE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitSaphdl: SetArrayInfo DRRPLAN failed <%d>",ilRc);
		}
		else
		{
			dbg(DEBUG,"InitSaphdl: SetArrayInfo DRRPLAN OK");
			dbg(DEBUG,"<%s> %d",rgDrrPlanArray.crArrayName,rgDrrPlanArray.rrArrayHandle);

			dbg(DEBUG,"<%s> %d",rgDrrPlanArray.crArrayName,rgDrrPlanArray.rrArrayHandle);
			strcpy(rgDrrPlanArray.crIdx01Name,"DRRPLAN_STFU");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgDrrPlanArray.rrArrayHandle,
				rgDrrPlanArray.crArrayName,
				&rgDrrPlanArray.rrIdx01Handle,
				rgDrrPlanArray.crIdx01Name,"STFU");
				strcpy(rgDrrPlanArray.crIdx01FieldList,"STFU");
			SetIndexInfo(&rgDrrPlanArray,"STFU",1);
		}
	}

	if(ilRc == RC_SUCCESS)
	{
		long pclAddFieldLens[12];  


		 pclAddFieldLens[0] = 2;  
 		 pclAddFieldLens[1] = 2;  
 		 pclAddFieldLens[1] = 0;  

		strcpy(cgDrrFields,DRR_FIELDS);
		*pclSelection = '\0';
		dbg(TRACE,"InitSaphdl: SetArrayInfo DRRTAB array");
		ilRc = SetArrayInfo("DRRTAB","DRRTAB",cgDrrFields,
				NULL,NULL,&rgDrrArray,pclSelection,FALSE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitSaphdl: SetArrayInfo DRRTAB failed <%d>",ilRc);
		}
		else
		{
			dbg(DEBUG,"InitSaphdl: SetArrayInfo DRRTAB OK");
			dbg(DEBUG,"<%s> %d",rgDrrArray.crArrayName,rgDrrArray.rrArrayHandle);

			strcpy(rgDrrArray.crIdx01Name,"DRR_STFU");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgDrrArray.rrArrayHandle,
				rgDrrArray.crArrayName,
				&rgDrrArray.rrIdx01Handle,
				rgDrrArray.crIdx01Name,"STFU");
				strcpy(rgDrrArray.crIdx01FieldList,"STFU");
			SetIndexInfo(&rgDrrArray,"STFU",1);
		}
	}

	if(ilRc == RC_SUCCESS)
	{

		sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);
		dbg(TRACE,"InitSaphdl: SetArrayInfo ODATAB array \n<%s>",pclSelection);
		ilRc = SetArrayInfo("ODATAB","ODATAB",ODA_FIELDS,NULL,NULL,&rgOdaArray,pclSelection,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitSaphdl: SetArrayInfo ODATAB failed <%d>",ilRc);
		}
		else
		{
			dbg(DEBUG,"InitSaphdl: SetArrayInfo ODATAB OK");
			dbg(DEBUG,"<%s> %d",rgOdaArray.crArrayName,rgOdaArray.rrArrayHandle);
			strcpy(rgOdaArray.crIdx01Name,"ODA_URNO");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgOdaArray.rrArrayHandle,
				rgOdaArray.crArrayName,
				&rgOdaArray.rrIdx01Handle,
				rgOdaArray.crIdx01Name,"URNO");
				strcpy(rgOdaArray.crIdx01FieldList,"URNO");
			SetIndexInfo(&rgOdaArray,"URNO",1);

			strcpy(rgOdaArray.crIdx02Name,"ODA_SDAC");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgOdaArray.rrArrayHandle,
				rgOdaArray.crArrayName,
				&rgOdaArray.rrIdx02Handle,
				rgOdaArray.crIdx02Name,"SDAC");
				strcpy(rgOdaArray.crIdx02FieldList,"SDAC");
			SetIndexInfo(&rgOdaArray,"SDAC",2);
		}

	}

	if(ilRc == RC_SUCCESS)
	{
		sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);
		dbg(TRACE,"InitSaphdl: SetArrayInfo COTTAB array \n<%s>",pclSelection);
		ilRc = SetArrayInfo("COTTAB","COTTAB",COT_FIELDS,NULL,NULL,&rgCotArray,pclSelection,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitSaphdl: SetArrayInfo COTTAB failed <%d>",ilRc);
		}
		else
		{
			dbg(DEBUG,"InitSaphdl: SetArrayInfo COTTAB OK");
			dbg(DEBUG,"<%s> %d",rgCotArray.crArrayName,rgCotArray.rrArrayHandle);
			strcpy(rgCotArray.crIdx01Name,"COT_URNO");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgCotArray.rrArrayHandle,
				rgCotArray.crArrayName,
				&rgCotArray.rrIdx01Handle,
				rgCotArray.crIdx01Name,"URNO");
				strcpy(rgCotArray.crIdx01FieldList,"URNO");
			SetIndexInfo(&rgCotArray,"URNO",1);

			strcpy(rgCotArray.crIdx02Name,"COT_CTRC");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgCotArray.rrArrayHandle,
				rgCotArray.crArrayName,
				&rgCotArray.rrIdx02Handle,
				rgCotArray.crIdx02Name,"CTRC");
				strcpy(rgCotArray.crIdx02FieldList,"CTRC");
			SetIndexInfo(&rgCotArray,"CTRC",2);
		}

	}

	if(ilRc == RC_SUCCESS)
	{
		sprintf(pclSelection,"WHERE HOPO = '%s'",cgHopo);
		dbg(TRACE,"InitSaphdl: SetArrayInfo ORGTAB array \n<%s>",pclSelection);
		ilRc = SetArrayInfo("ORGTAB","ORGTAB",ORG_FIELDS,NULL,NULL,&rgOrgArray,pclSelection,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitSaphdl: SetArrayInfo ORGTAB failed <%d>",ilRc);
		}
		else
		{
			dbg(DEBUG,"InitSaphdl: SetArrayInfo ORGTAB OK");
			dbg(DEBUG,"<%s> %d",rgOrgArray.crArrayName,rgOrgArray.rrArrayHandle);
			strcpy(rgOrgArray.crIdx01Name,"ORG_URNO");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgOrgArray.rrArrayHandle,
				rgOrgArray.crArrayName,
				&rgOrgArray.rrIdx01Handle,
				rgOrgArray.crIdx01Name,"URNO");
				strcpy(rgOrgArray.crIdx01FieldList,"URNO");
			SetIndexInfo(&rgOrgArray,"URNO",1);

			strcpy(rgOrgArray.crIdx02Name,"ORG_DPT1");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgOrgArray.rrArrayHandle,
				rgOrgArray.crArrayName,
				&rgOrgArray.rrIdx02Handle,
				rgOrgArray.crIdx02Name,"DPT1");
				strcpy(rgOrgArray.crIdx02FieldList,"DPT1");
			SetIndexInfo(&rgOrgArray,"DPT1",2);
		}
	}

	if(ilRc == RC_SUCCESS)
	{
		strcpy(cgStfFields,"URNO,PENO,TOHR");
		/*sprintf(pclSelection,"WHERE HOPO = '%s' AND TOHR='%s'",cgHopo,cgTohr);*/
    /* don't use TOHR for selection, load all and check TOHR field later */
		sprintf(pclSelection,"WHERE HOPO = '%s' ",cgHopo,cgTohr);
		dbg(TRACE,"InitSaphdl: SetArrayInfo STFTAB array \n<%s>",pclSelection);
		ilRc = SetArrayInfo("STFTAB","STFTAB",cgStfFields,NULL,NULL,&rgStfArray,pclSelection,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitSaphdl: SetArrayInfo STFTAB failed <%d>",ilRc);
		}
		else
		{
			dbg(DEBUG,"InitSaphdl: SetArrayInfo STFTAB OK");
			dbg(DEBUG,"<%s> %d",rgStfArray.crArrayName,rgStfArray.rrArrayHandle);
			strcpy(rgStfArray.crIdx01Name,"STF_URNO");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgStfArray.rrArrayHandle,
				rgStfArray.crArrayName,
				&rgStfArray.rrIdx01Handle,
				rgStfArray.crIdx01Name,"URNO");
				strcpy(rgStfArray.crIdx01FieldList,"URNO");
				SetIndexInfo(&rgStfArray,"URNO",1);
			strcpy(rgStfArray.crIdx02Name,"STF_PENO");
			ilRc = CEDAArrayCreateSimpleIndexUp(&rgStfArray.rrArrayHandle,
				rgStfArray.crArrayName,
				&rgStfArray.rrIdx02Handle,
				rgStfArray.crIdx02Name,"PENO");
				strcpy(rgStfArray.crIdx02FieldList,"PENO");
				SetIndexInfo(&rgStfArray,"PENO",2);
		}
	}

	if(ilRc == RC_SUCCESS)
	{
		ilRc = CEDAArrayReadAllHeader(outp);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"InitSaphdl: CEDAArrayReadAllHeader failed <%d>",ilRc);
		}else{
			dbg(TRACE,"InitSaphdl: CEDAArrayReadAllHeader OK");
		}/* end of if */
	}/* end of if */

	
	pcgStfuList = malloc(lgStfuListLength);
	if(pcgStfuList == NULL)
	{
		dbg(TRACE,"unable to allocate %ld Bytes for StfuList",lgStfuListLength);
		ilRc = RC_FAIL;
	}
	else
	{
		memset(pcgStfuList,0,lgStfuListLength);
	}

	if(ilRc == RC_SUCCESS)
	{
		ilRc = InitFieldIndex();
	}

	if(ilRc == RC_SUCCESS)
	{
		char clSelection[244];

		sprintf(clSelection,"WHERE NAME='BRK' AND HOPO='%s'",cgHopo);

		ilRc = DoSingleSelect("JTYTAB",clSelection,"URNO",cgDataArea);
		if (ilRc == RC_SUCCESS)
		{
			strcpy(cgBrkUjty,cgDataArea);
			StringTrimRight(cgBrkUjty);
			dbg(TRACE,"InitSaphdl: UJTY set to <%s>",cgBrkUjty);
		}
		else
		{
			dbg(TRACE,"InitSaphdl: <%s> not found in JTYTAB UJTY not set");
		}
	}


	TriggerAction("STFTAB","URNO!,PENO!,TOHR!");
	/*TriggerAction("DRRTAB","URNO!,SDAY!,DRS2!");*/
	/*TriggerAction("DRRTAB","URNO!,STFU,AVFR,AVTO,DRS2,SCOD!,SBLU,SDAY!");*/
	TriggerAction("JOBTAB","URNO!,ACFR!,ACTO!,UJTY!,UDSR!");

	if(ilRc == RC_SUCCESS)
	{
		igInitOK = TRUE;
		dbg(TRACE,"InitSaphdl: InitFieldIndex OK");
	}
	else
	{
		dbg(TRACE,"InitSaphdl: InitFieldIndex failed");
	}

	dbg(TRACE,"Debug Level is %d",debug_level);
	return (ilRc);	
} /* end of initialize */


static int SetIndexInfo(ARRAYINFO *prpArrayInfo,char *pcpFieldList,int ilIndex)
{
	int	ilRc = RC_SUCCESS;				/* Return code */
  int ilLc;
	char **ppclIdxRowBuf;
	long *pllIdxRowLen;

	dbg(DEBUG,"%05d:SetIndexInfo:",__LINE__);
	switch(ilIndex)
	{
		case 1:
			ppclIdxRowBuf = &prpArrayInfo->pcrIdx01RowBuf;
			pllIdxRowLen = &prpArrayInfo->lrIdx01RowLen;
			break;
		case 2:
			ppclIdxRowBuf = &prpArrayInfo->pcrIdx02RowBuf;
			pllIdxRowLen = &prpArrayInfo->lrIdx02RowLen;
			break;
		case 3:
			ppclIdxRowBuf = &prpArrayInfo->pcrIdx03RowBuf;
			pllIdxRowLen = &prpArrayInfo->lrIdx03RowLen;
			break;
		default:
			ilRc = RC_FAIL;
			dbg(DEBUG,"SetIndexInfo:ivalid index %d",ilIndex);
	}

	if (ilRc == RC_SUCCESS)
	{
  ilRc = GetRowLength(cgHopo,prpArrayInfo->crArrayTableName,pcpFieldList,
							pllIdxRowLen);
	*ppclIdxRowBuf = (char *) calloc(1,(*pllIdxRowLen+10));
	if(*ppclIdxRowBuf == NULL)
  {
    ilRc = RC_FAIL;
    dbg(TRACE,"SetArrayInfo: calloc failed");
   }/* end of if */
	}

	return ilRc;
}

/******************************************************************************/
/*                                                                            */
/******************************************************************************/
static int SetArrayInfo(char *pcpArrayName, char *pcpTableName, char *pcpArrayFieldList,char *pcpAddFields,long *pcpAddFieldLens,ARRAYINFO *prpArrayInfo,
	char *pcpSelection,BOOL bpFill)
{
	int	ilRc = RC_SUCCESS;				/* Return code */
  int ilLc;

	if(prpArrayInfo == NULL)
	{
		dbg(TRACE,"SetArrayInfo: invalid last parameter : null pointer not valid");
		ilRc = RC_FAIL;
	}else
	{
		memset(prpArrayInfo,0x00,sizeof(prpArrayInfo));

		prpArrayInfo->rrArrayHandle = -1;
		prpArrayInfo->rrIdx01Handle = -1;

		if(pcpArrayName != NULL)
		{
			if(strlen(pcpArrayName) <= ARR_NAME_LEN)
			{
				strcpy(prpArrayInfo->crArrayName,pcpArrayName);
			}/* end of if */
		}/* end of if */
	}/* end of if */

	strcpy(prpArrayInfo->crArrayTableName,pcpTableName);
	if(ilRc == RC_SUCCESS)
	{
		prpArrayInfo->lrArrayFieldCnt = get_no_of_items(pcpArrayFieldList);
		if (pcpAddFields != NULL)
		{
			prpArrayInfo->lrArrayFieldCnt += get_no_of_items(pcpAddFields);
		}

		prpArrayInfo->plrArrayFieldOfs = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
		if(prpArrayInfo->plrArrayFieldOfs == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrArrayFieldOfs calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}/* end of if */
	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		prpArrayInfo->plrArrayFieldLen = (long *) calloc(prpArrayInfo->lrArrayFieldCnt,sizeof(long));
		if(prpArrayInfo->plrArrayFieldLen == NULL)
		{
			dbg(TRACE,"SetArrayInfo: plrArrayFieldLen calloc(%d,%d) failed",prpArrayInfo->lrArrayFieldCnt,sizeof(long));
			ilRc = RC_FAIL;
		}else{
			*(prpArrayInfo->plrArrayFieldLen) = -1;
		}/* end of if */
	}/* end of if */

   	if(ilRc == RC_SUCCESS)
   	{
       	ilRc = GetRowLength(&cgHopo[0],pcpTableName,pcpArrayFieldList,
							&(prpArrayInfo->lrArrayRowLen));
       	if(ilRc != RC_SUCCESS)
       	{
           	dbg(TRACE,"SetArrayInfo: GetRowLength failed <%s> <%s>",pcpArrayName,pcpArrayFieldList);
      	}/* end of if */
   	}/* end of if */

   	if(ilRc == RC_SUCCESS)
   	{
		prpArrayInfo->lrArrayRowLen+=100;
		prpArrayInfo->pcrArrayRowBuf = (char *) calloc(1,(prpArrayInfo->lrArrayRowLen+10));
   		if(prpArrayInfo->pcrArrayRowBuf == NULL)
   		{
      			ilRc = RC_FAIL;
      			dbg(TRACE,"SetArrayInfo: calloc failed");
   		}/* end of if */
   	}/* end of if */

   	if(ilRc == RC_SUCCESS)
   	{
		prpArrayInfo->pcrIdx01RowBuf = (char *) calloc(1,(prpArrayInfo->lrIdx01RowLen+1));
   		if(prpArrayInfo->pcrIdx01RowBuf == NULL)
   		{
      			ilRc = RC_FAIL;
      			dbg(TRACE,"SetArrayInfo: calloc failed");
   		}/* end of if */
   	}/* end of if */

	if(ilRc == RC_SUCCESS)
	{
		dbg(DEBUG,"CEDAArrayCreate follows Array <%s> Table <%s>",pcpArrayName,pcpTableName);
		ilRc = CEDAArrayCreate(&(prpArrayInfo->rrArrayHandle),
				pcpArrayName,pcpTableName,pcpSelection,pcpAddFields,
				pcpAddFieldLens,pcpArrayFieldList,
				&(prpArrayInfo->plrArrayFieldLen[0]),
				&(prpArrayInfo->plrArrayFieldOfs[0]));

		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"SetArrayInfo: CEDAArrayInitialize failed <%d>",ilRc);
		}/* end of if */
	}/* end of if */

		if(pcpArrayFieldList != NULL)
		{
			if(strlen(pcpArrayFieldList) <= ARR_FLDLST_LEN)
			{
				strcpy(prpArrayInfo->crArrayFieldList,pcpArrayFieldList);
			}/* end of if */
		}/* end of if */
		if(pcpAddFields != NULL)
		{
			if(strlen(pcpAddFields) + 
				strlen(prpArrayInfo->crArrayFieldList) <= (size_t)ARR_FLDLST_LEN)
			{
				strcat(prpArrayInfo->crArrayFieldList,",");
				strcat(prpArrayInfo->crArrayFieldList,pcpAddFields);
				dbg(DEBUG,"<%s>",prpArrayInfo->crArrayFieldList);
			}/* end of if */
		}/* end of if */


	if(ilRc == RC_SUCCESS && bpFill)
	{
		ilRc = CEDAArrayFill(&(prpArrayInfo->rrArrayHandle),pcpArrayName,NULL);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"SetArrayInfo: CEDAArrayFill failed <%d>",ilRc);
		}/* end of if */
	}/* end of if */

   for(ilLc = 0; ilLc < prpArrayInfo->lrArrayFieldCnt; ilLc++)
   {
			char	pclFieldName[25];

			*pclFieldName  = '\0';

			GetDataItem(pclFieldName,prpArrayInfo->crArrayFieldList,
						ilLc+1,',',"","\0\0");
     dbg(DEBUG,"%02d %-10s Offset: %d",
				ilLc,pclFieldName,prpArrayInfo->plrArrayFieldOfs[ilLc]);
   }   

	return(ilRc);
}



/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
	int	ilRc = RC_SUCCESS;				/* Return code */
	
	dbg(TRACE,"Reset: now resetting");
	
	return ilRc;
	
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
	/* unset SIGCHLD ! DB-Child will terminate ! */

	dbg(TRACE,"Terminate: now DB logoff ...");

	signal(SIGCHLD,SIG_IGN);

	logoff();

	dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);


	dbg(TRACE,"Terminate: now leaving ...");

	fclose(outp);

	sleep(ipSleep < 1 ? 1 : ipSleep);
	
	exit(0);
	
} /* end of Terminate */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
	dbg(TRACE,"HandleErr: <%d> : unknown ERROR",pipErr);

	return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
	switch(pipErr) {
	case	QUE_E_FUNC	:	/* Unknown function */
		dbg(TRACE,"<%d> : unknown function",pipErr);
		break;
	case	QUE_E_MEMORY	:	/* Malloc reports no memory */
		dbg(TRACE,"<%d> : malloc failed",pipErr);
		break;
	case	QUE_E_SEND	:	/* Error using msgsnd */
		dbg(TRACE,"<%d> : msgsnd failed",pipErr);
		break;
	case	QUE_E_GET	:	/* Error using msgrcv */
		dbg(TRACE,"<%d> : msgrcv failed",pipErr);
		break;
	case	QUE_E_EXISTS	:
		dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
		break;
	case	QUE_E_NOFIND	:
		dbg(TRACE,"<%d> : route not found ",pipErr);
		break;
	case	QUE_E_ACKUNEX	:
		dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
		break;
	case	QUE_E_STATUS	:
		dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
		break;
	case	QUE_E_INACTIVE	:
		dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
		break;
	case	QUE_E_MISACK	:
		dbg(TRACE,"<%d> : missing ack ",pipErr);
		break;
	case	QUE_E_NOQUEUES	:
		dbg(TRACE,"<%d> : queue does not exist",pipErr);
		break;
	case	QUE_E_RESP	:	/* No response on CREATE */
		dbg(TRACE,"<%d> : no response on create",pipErr);
		break;
	case	QUE_E_FULL	:
		dbg(TRACE,"<%d> : too many route destinations",pipErr);
		break;
	case	QUE_E_NOMSG	:	/* No message on queue */
		dbg(TRACE,"<%d> : no messages on queue",pipErr);
		break;
	case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
		dbg(TRACE,"<%d> : invalid originator=0",pipErr);
		break;
	case	QUE_E_NOINIT	:	/* Queues is not initialized*/
		dbg(TRACE,"<%d> : queues are not initialized",pipErr);
		break;
	case	QUE_E_ITOBIG	:
		dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
		break;
	case	QUE_E_BUFSIZ	:
		dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
		break;
	default			:	/* Unknown queue error */
		dbg(TRACE,"<%d> : unknown error",pipErr);
		break;
	} /* end switch */
         
	return;

} /* end of HandleQueErr */


/******************************************************************************/
/******************************************************************************/
static int GetRowLength(char *pcpHopo, char *pcpTana, char *pcpFieldList,
						long *plpLen)
{
	int	 ilRc = RC_SUCCESS;			/* Return code */
	int  ilNoOfItems = 0;
	int  ilLoop = 0;
	long llFldLen = 0;
	long llRowLen = 0;
	char clFina[8];
	
	ilNoOfItems = get_no_of_items(pcpFieldList);
	ilLoop = 1;
	do
	{
		ilRc = get_real_item(&clFina[0],pcpFieldList,ilLoop);
		if(ilRc > 0)
		{
			ilRc = GetFieldLength(pcpHopo,pcpTana,&clFina[0],&llFldLen);
			if(ilRc == RC_SUCCESS)
			{
				llRowLen++;
				llRowLen += llFldLen;
			}/* end of if */
		}/* end of if */
		ilLoop++;
	}while((ilRc == RC_SUCCESS) && (ilLoop <= ilNoOfItems));

	if(ilRc == RC_SUCCESS)
	{
		*plpLen = llRowLen;
	}/* end of if */

	return(ilRc);
	
} /* end of GetRowLength */


/******************************************************************************/
/******************************************************************************/
static int GetFieldLength(char *pcpHopo, char *pcpTana, char *pcpFina,
							long *plpLen)
{
	int  ilRc = RC_SUCCESS;			/* Return code */
	int  ilCnt = 0;
	char clTaFi[32];
	char clFele[16];
	char clFldLst[16];

	ilCnt = 1;
	sprintf(&clTaFi[0],"%s,%s",pcpTana,pcpFina);
	sprintf(&clFldLst[0],"TANA,FINA"); /* wird von syslib zerst�rt */


	ilRc = syslibSearchSystabData(&cgTabEnd[0],&clFldLst[0],&clTaFi[0],"FELE",&clFele[0],&ilCnt,"");
	switch (ilRc)
	{
	case RC_SUCCESS :
		*plpLen = atoi(&clFele[0]);
		/* dbg(DEBUG,"GetFieldLength: <%s,%s,%s> <%d>",pcpHopo,pcpTana,pcpFina,*plpLen);*/
		break;

	case RC_NOT_FOUND :
		dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_NOT_FOUND <%s>",ilRc,&clTaFi[0]);
		break;

	case RC_FAIL :
		dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_FAIL",ilRc);
		break;

	default :
		dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> unexpected",ilRc);
		break;
	}/* end of switch */

	return(ilRc);
	
} /* end of GetFieldLength */


/******************************************************************************/
/******************************************************************************/
static int GetCommand(char *pcpCommand, int *pipCmd)
{
	int	ilRc = RC_SUCCESS;			/* Return code */
	int ilLoop = 0;
	char clCommand[48];
	
	memset(&clCommand[0],0x00,8);

	ilRc = get_real_item(&clCommand[0],pcpCommand,1);
	if(ilRc > 0)
	{
		ilRc = RC_FAIL;
	}/* end of if */

	while((ilRc != 0) && (prgCmdTxt[ilLoop] != NULL))
	{
		ilRc = strcmp(&clCommand[0],prgCmdTxt[ilLoop]);
		ilLoop++;
	}/* end of while */

	if(ilRc == 0)
	{
		ilLoop--;
		dbg(DEBUG,"GetCommand: <%s> <%d>",prgCmdTxt[ilLoop],rgCmdDef[ilLoop]);
		*pipCmd = rgCmdDef[ilLoop];
	}else{
		dbg(TRACE,"GetCommand: <%s> is not valid",&clCommand[0]);
		ilRc = RC_FAIL;
	}/* end of if */

	return(ilRc);
	
} /* end of GetCommand */

static int DailyRosterExport(char *pcpStaffUrno, char *pcpTime)
{
	int ilRc = RC_SUCCESS;
	char clFileName[34];
	char clFilePath[234];
	char clExportPath[234];
	long llSequence;
	FILE *pflFile = NULL;
	char clTimeStamp[24];
	int ilRecordCount = 0;
	char clMonth[32];

/*	if(strlen(pcpTime) == 0)
	{
		GetServerTimeStamp("LOC", 1,0,clTimeStamp);
	}
	else
	{
		strcpy(clTimeStamp,pcpTime);
	}*/
	GetServerTimeStamp("LOC", 1,0,clTimeStamp);
	dbg(TRACE,"clTimeStamp: <%s>",clTimeStamp);
	StringMid(clMonth,clTimeStamp,4,2,CLIENTTODB);
	llSequence = GetSequence("SAPDRD",clMonth);
	MakeFileName(cgDrdFileId,++llSequence,clFileName);
	sprintf(clFilePath,"%s%s%s",cgFileBase,cgTmpPath,clFileName);
	dbg(DEBUG,"%05d:  <%s> <%s> <%s> ",__LINE__,cgFileBase,cgTmpPath,clFileName);

	dbg(TRACE,"Export File for daily roster export <%s> will be created",clFilePath);
	dbg(DEBUG,"%05d:  <%s> ",__LINE__,clFileName);
	errno = 0;
	pflFile = fopen(clFilePath,"wt");
	if (pflFile == NULL)
	{
		dbg(TRACE,"DailyRoster File <%s> not created, reason: %d %s",
			clFilePath,errno,strerror(errno));
		ilRc = RC_FAIL;
	}
	else
	{
	char clSelection[124];

	/**** saving of LSTU is not needed anymore due to changes in ExportSubsequentChanges 
	sprintf(clSelection,"WHERE KEYS='%s' AND HOPO='%s'",
			"SAPLSTU",cgHopo);
	GetServerTimeStamp("LOC", 1,0,clTimeStamp);
	if ((ilRc = DoSingleSelect("NUMTAB",clSelection,"FLAG",cgDataArea)) == RC_SUCCESS)
	{
				strcpy(cgLastLstu,cgDataArea);
				StringTrimRight(cgLastLstu);
				DoUpdate2("NUMTAB",clSelection,"FLAG=:VFLAG","FLAG","0",clTimeStamp,TRUE);
	}
	else
	{
			sprintf(cgDataArea,"0,SAPLSTU,%s,%s",clTimeStamp,cgHopo);
			DoInsert2("NUMTAB","ACNU,KEYS,FLAG,HOPO",
						":VACNU,:VKEYS,:VFLAG,:VHOPO",cgDataArea,FALSE);
	}
	errno = 0;
	*/
		dbg(DEBUG,"daily roster export file created");
	dbg(DEBUG,"%05d:  <%s> ",__LINE__,clFileName);
	errno = 0;
		WriteHeader(pflFile,cgDrdFileId,clFileName,0);

		/* first write temporary file with Subsequent Changed to SUB1/SUB2 to export file */

		if (pfgTmpFile != NULL)
		{
			fclose(pfgTmpFile);
			pfgTmpFile = NULL;
		}

		OpenTmpFile(TRUE);
		if(pfgTmpFile != NULL)
		{
			memset(cgLineBuf,0,sizeof(cgLineBuf));
			while(fgets(cgLineBuf,sizeof(cgLineBuf),pfgTmpFile) != NULL)
			{
					int ilBytes;

					ilRecordCount++;
					errno = 0;
					ilBytes = strlen(cgLineBuf);
					if((ilRc  = fwrite(cgLineBuf,sizeof(char),ilBytes,pflFile)) != ilBytes)
					{
						dbg(TRACE,"could not write subsequent SUB1/SUB2 changes: <%s>, reason %d <%s>",
											cgLineBuf,errno,strerror(errno));
						sprintf(cgErrorString,
								"could not write subsequent SUB1/SUB2 changes: <%s>, reason %d <%s>",
										cgLineBuf,errno,strerror(errno));
						LogError(cgExportPath,cgErrorString,TRUE);
						ilRc = RC_FAIL;
						break;
					}
			}	

			fclose(pfgTmpFile);
			pfgTmpFile = NULL;
			remove(cgTmpFilePath);
		}
		dbg(TRACE,"ExportDailyRoster: %d  from temporary file written "
				" containing subsequent changes",ilRecordCount);
		/*GetServerTimeStamp("LOC", 1,0,clTimeStamp);*/
		if(strlen(pcpTime) == 0)
		{
			GetServerTimeStamp("LOC", 1,0,clTimeStamp);
		}
		else
		{
			strcpy(clTimeStamp,pcpTime);
		}
		/*AddSecondsToCEDATime(clTimeStamp,-86400,1);*/
		AddSecondsToCEDATime(clTimeStamp,(igExportOffset*86400),1);
		clTimeStamp[8] = '\0';
		ilRecordCount += ExportDailyRoster(pflFile,clTimeStamp,pcpStaffUrno);
		dbg(TRACE,"ExportDailyRoster: %d lines exported",ilRecordCount);
		WriteTrailer(pflFile);
		rewind(pflFile);
		WriteHeader(pflFile,cgDrdFileId,clFileName,ilRecordCount);
		sprintf(clExportPath,"%s%s%s",cgFileBase,cgExportPath,clFileName);
		fclose(pflFile);
		PutSequence("SAPDRD",llSequence,clMonth);
	  dbg(TRACE,"ExportDailyRoster: export file will be renamed from <%s> to <%s>",
				clFilePath,clExportPath);
		rename(clFilePath,clExportPath);
	}
  return ilRc;
}

static int GetOffRestIndicator(char *pcpIndicator,char *pcpScod)
{
	int ilRc = RC_SUCCESS;
	char clKey[24];
	long llRow;

	strcpy(clKey,pcpScod);
	llRow = ARR_FIRST;
	ilRc = CEDAArrayFindRowPointer(&rgOdaArray.rrArrayHandle,
			rgOdaArray.crArrayName,&rgOdaArray.rrIdx02Handle,
					rgOdaArray.crIdx02Name,clKey,&llRow,
							(void **)&rgOdaArray.pcrArrayRowBuf);
	if (ilRc == RC_SUCCESS)
	{
		strcpy(pcpIndicator,
				FIELDVAL(rgOdaArray,rgOdaArray.pcrArrayRowBuf,igOdaSDAS));
		dbg(DEBUG,"Off/Rest indicator got from ODATAB <%s> for SCOD <%s>",
						pcpIndicator,clKey);
	}
	else
	{
		strcpy(pcpIndicator,"0");
		dbg(DEBUG,"SCOD <%s> not found in ODATAB, Off/Rest indicator set to '0'",
					clKey);
	}

	return ilRc;
}

#if 0
{
	int ilRc = RC_SUCCESS;
	char clKey[24];
	long llRow;

	strcpy(clKey,pcpScod);
	llRow = ARR_FIRST;
	ilRc = CEDAArrayFindRowPointer(&rgOdaArray.rrArrayHandle,
			rgOdaArray.crArrayName,&rgOdaArray.rrIdx02Handle,
					rgOdaArray.crIdx02Name,clKey,&llRow,
							(void **)&rgOdaArray.pcrArrayRowBuf);
	if (ilRc == RC_SUCCESS)
	{
		strcpy(pcpIndicator,
				FIELDVAL(rgOdaArray,rgOdaArray.pcrArrayRowBuf,igOdaFREE));
		if (*pcpIndicator == 'x' || *pcpIndicator == 'X')
		{
			strcpy(pcpIndicator,"1");
		}
		else
		{
				strcpy(pcpIndicator,FIELDVAL(rgOdaArray,rgOdaArray.pcrArrayRowBuf
						,igOdaTYPE));
				if (*pcpIndicator == 'S')
				{
					strcpy(pcpIndicator,"2");
				}
				else
				{
					strcpy(pcpIndicator,"0");
				}
		}
		dbg(DEBUG,"Off/Rest indicator got from ODATAB <%s> for SCOD <%s>",
						pcpIndicator,clKey);
	}
	else
	{
		strcpy(pcpIndicator,"0");
		dbg(DEBUG,"SCOD <%s> not found in ODATAB, Off/Rest indicator set to '0'",
					clKey);
	}

	return ilRc;

}
#endif


static int 	WriteOneShiftLine(FILE *pfpFile,char *pcpPlfr,char *pcpPlto,
		char *pcpPlSCOD,char *pcpPeno,char *pcpRowBuf)
{
	int ilRc = RC_SUCCESS;
	char clLineBuffer[124];
	char clKey[24];
	char clOffRestIndicator[12];
	char clPlannedOffRestIndicator[12];
	char *pclLineBuffer;
	int ilBytes = 0;
	long llRow;
	char clJobSelection[244];
	char clSbfr[24],clSbto[24],clDrrUrno[24],clSday[22],clSblu[12],clTmpSblu[24];
	char clAvfr[24],clAvto[24],clScod[32];
	char clDrs1[4],clDrs2[28],clDrs3[4],clDrs4[4];
	char clRosl[2];
	char clSub[4];
	char clTmpStr[24];
	char *pclRowBuf;
	BOOL blExport;

	dbg(DEBUG,"WriteOneShiftLine: PLFR <%s> PLTO <%s> plSCOD <%s> PENO <%s> ",
		pcpPlfr,pcpPlto,pcpPlSCOD,pcpPeno);

	/*DRRSTF_FIELDS "AVFR,AVTO,DRRN,SBFR,SBLU,SBTO,SCOD,SDAY,STFU,PENO,DRRTAB.URNO,DRS1,DRS3,DRS4,DRS2,ROSL"*/

	GetDataItem(clTmpStr,pcpRowBuf,1,',',"","\0\0");
	strcpy(clAvfr,&clTmpStr[8]);
	GetDataItem(clTmpStr,pcpRowBuf,2,',',"","\0\0");
	strcpy(clAvto,&clTmpStr[8]);
	GetDataItem(clSday,pcpRowBuf,8,',',"","\0\0");
	GetDataItem(clTmpSblu,pcpRowBuf,5,',',"","\0\0");
	sprintf(clSblu,"%04d",atoi(clTmpSblu));
	GetDataItem(clScod,pcpRowBuf,7,',',"","\0\0");

	/* check for overtime */
	GetDataItem(clDrs1,pcpRowBuf,12,',',"","\0\0");
	GetDataItem(clDrs3,pcpRowBuf,13,',',"","\0\0");
	GetDataItem(clDrs4,pcpRowBuf,14,',',"","\0\0");
	GetDataItem(clDrs2,pcpRowBuf,15,',',"","\0\0");
	GetDataItem(clRosl,pcpRowBuf,16,',',"","\0\0");

	if (*clDrs4 == '1')
	{
		dbg(DEBUG,"don't export this record because compl. flag is set <%s>",clDrs4);
		return RC_NOTFOUND;
	}

	StringTrimRight(clDrs2);
	if(*clDrs1 == '1' || *clDrs3 == '1' || *clDrs4 == '1')
	{
				dbg(DEBUG,"%05d:GetOvertime follows: <%s>, <%s,%s,%s>",__LINE__,
						pcpRowBuf,clDrs1,clDrs3,clDrs4);
		GetDataItem(clTmpStr,pcpRowBuf,8,',',"","\0\0");
		GetOvertime(clScod,clTmpStr,clAvfr,clAvto,clDrs1,clDrs3,clDrs4);
	}
	/* get SBFR,SBTO from break job */
	/* JOB_FIELDS = "ACFR,ACTO,UDSR"*/

	GetDataItem(clDrrUrno,pcpRowBuf,11,',',"","\0\0");
	sprintf(clKey,"%s",clDrrUrno);
	llRow = ARR_FIRST;
	ilRc = CEDAArrayFindRowPointer(&rgJobArray.rrArrayHandle,
			rgJobArray.crArrayName,&rgJobArray.rrIdx01Handle,
					rgJobArray.crIdx01Name,clKey,&llRow,
							(void **)&pclRowBuf);

	if (ilRc == RC_SUCCESS)
	{
		char clBreakFrom[16];
		char clBreakTo[16];
		time_t llBreakDuration;

		strcpy(clBreakFrom,FIELDVAL(rgJobArray,pclRowBuf,igJobACFR));
		UtcToLocalTimeFixTZ(clBreakFrom);
		strcpy(clSbfr,&clBreakFrom[8]);

		strcpy(clBreakTo,FIELDVAL(rgJobArray,pclRowBuf,igJobACTO));
		UtcToLocalTimeFixTZ(clBreakTo);
		strcpy(clSbto,&clBreakTo[8]);

		GetBreakTimeDiff(clBreakFrom,clBreakTo,&llBreakDuration);
		sprintf(clSblu,"%04d",llBreakDuration/60);

		dbg(DEBUG,"Break job from:      <%s>",clSbfr);
		dbg(DEBUG,"Break job to :       <%s>",clSbto);
		dbg(DEBUG,"Break job duration : <%s>",clSblu);
	}
	else
	{
		if (atoi(clRosl) != 3)
		{
			GetDataItem(clTmpStr,pcpRowBuf,4,',',"","\0\0");
			StringTrimRight(clTmpStr);
			if ((*clTmpStr != '\0') || (*clTmpStr != ' '))
			{
				strcpy(clSbfr,&clTmpStr[8]);
				if(atol(clSblu) > 0)
				{
					AddSecondsToCEDATime(clTmpStr,(time_t)atol(clSblu)*60,1);
					strcpy(clSbto,&clTmpStr[8]);
				}
				else
				{
					strcpy(clSbfr,"0000");
					strcpy(clSbto,"0000");
					strcpy(clSblu,"0000");
				}
			}
			else
			{
				strcpy(clSbfr,"0000");
				strcpy(clSbto,"0000");
				strcpy(clSblu,"0000");
			}
			dbg(DEBUG,"Break job not found, using DRR: <%s> <%s><%s>",
					clSbfr,clSbto,pcpRowBuf);
		}
		else
		{
			strcpy(clSbfr,"0000");
			strcpy(clSbto,"0000");
			strcpy(clSblu,"0000");
			dbg(DEBUG,"Break job not found or deleted. Setting break times to 0000.");
		}
	}

	GetOffRestIndicator(clPlannedOffRestIndicator,pcpPlSCOD);
	if (strcmp(pcpPlSCOD,FIELDVAL(rgDrrArray,pcpRowBuf,igDrrSCOD)) == 0)
	{
			dbg(DEBUG,"active SCOD <%s> equals planned SCOD <%s>",
				FIELDVAL(rgDrrArray,pcpRowBuf,igDrrSCOD),pcpPlSCOD);
			strcpy(clOffRestIndicator,clPlannedOffRestIndicator);
	}
	else
	{
		GetOffRestIndicator(clOffRestIndicator,clScod);
	}

	if (*clDrs2 == '\0' || *clDrs2 == ' ')
	{
		if(*clOffRestIndicator == '0')
		{
			strcpy(clSub,cgAbsSubDef);
		}
		else
		{
			strcpy(clSub,cgShiftSubDef);
		}
	}
	else
	{
		strcpy(clSub,clDrs2);
	}
	pclLineBuffer = clLineBuffer;
	memset(clLineBuffer,0,sizeof(clLineBuffer));

	StringPut(pclLineBuffer,"10",2);
	pclLineBuffer += 2;
	ilBytes += 2;
	StringPut(pclLineBuffer,pcpPeno,8);
	pclLineBuffer += 8;
	ilBytes += 8;
	StringPut(pclLineBuffer,clSday,8);
	pclLineBuffer += 8;
	ilBytes += 8;

	if(IsAbsenceCode(clScod,&blExport) == RC_SUCCESS)
	{
		/** is absence, set start and end time to "000000000000" **/
		dbg(DEBUG,"shift is absence, set start <%> and end time <%> to 0",
			clAvfr,clAvto);
		strcpy(clAvfr,"0000");
		strcpy(clAvto,"0000");
	}

	if(IsAbsenceCode(pcpPlSCOD,&blExport) == RC_SUCCESS)
	{
		/** is absence, set start and end time to "000000000000" **/
		dbg(DEBUG,"Planned shift is absence, set start <%> and end time <%> to 0",
			pcpPlfr,pcpPlto);
		strcpy(pcpPlfr,"0000");
		strcpy(pcpPlto,"0000");
	}
	else
	{
		dbg(DEBUG,"%05d:shift <%s> is not absence",__LINE__,pcpPlSCOD);
	}
	GetSbluFromBSDTAB(clScod,clSblu);
	sprintf(clSblu,"%04d",atoi(clSblu));
	StringPut(pclLineBuffer,clAvfr,4);
	pclLineBuffer += 4;
	ilBytes += 4;
	StringPut(pclLineBuffer,clAvto,4);
	pclLineBuffer += 4;
	ilBytes += 4;
	StringPut(pclLineBuffer,pcpPlfr,4);
	pclLineBuffer += 4;
	ilBytes += 4;
	StringPut(pclLineBuffer,pcpPlto,4);
	pclLineBuffer += 4;
	ilBytes += 4;
	StringPut(pclLineBuffer,clSbfr,4);
	pclLineBuffer += 4;
	ilBytes += 4;
	StringPut(pclLineBuffer,clSbto,4);
	pclLineBuffer += 4;
	ilBytes += 4;
	StringPut(pclLineBuffer,clSblu,4);
	pclLineBuffer += 4;
	ilBytes += 4;
	StringPut(pclLineBuffer,clOffRestIndicator,1);
	pclLineBuffer += 1;
	ilBytes += 1;
	StringPut(pclLineBuffer,clPlannedOffRestIndicator,1);
	pclLineBuffer += 1;
	ilBytes += 1;
	StringPut(pclLineBuffer,clSub,1);
	pclLineBuffer += 1;
	ilBytes += 1;

	StringPut(pclLineBuffer,"\r\n",2);
	ilBytes += 2;

	/*if (bgARTest)*/
	{
		dbg(DEBUG,"AVFR: <%s>",clAvfr);
		dbg(DEBUG,"AVTO: <%s>",clAvto);
		dbg(DEBUG,"PLFR: <%s>",pcpPlfr);
		dbg(DEBUG,"PLTO: <%s>",pcpPlto);
		dbg(DEBUG,"SBFR: <%s>",clSbfr);
		dbg(DEBUG,"SBTO: <%s>",clSbto);
		dbg(DEBUG,"SBLU: <%s>",clSblu);
		dbg(DEBUG,"OFF/RD-I<%s>",clOffRestIndicator);
		dbg(DEBUG,"OFF/RD-I<%s>",clPlannedOffRestIndicator);
		dbg(DEBUG,"SUB:  <%s>",clSub);
		dbg(DEBUG,"line to write: <%s>",clLineBuffer);
	}


	if((ilRc  = fwrite(clLineBuffer,sizeof(char),ilBytes,pfpFile)) != ilBytes)
	{
		dbg(TRACE,"could not daily roster line: <%s>, reason %d <%s>",
				clLineBuffer,errno,strerror(errno));
		ilRc = RC_FAIL;
	}
	else
	{
		ilRc = RC_SUCCESS;
		fflush(pfpFile);
	}
	
	return(ilRc);
}

static int ExportLongTermRoster(FILE *pfpFile,char *pcpOrgUnit,char *pcpFrom,char *pcpTo)
{
	int ilRc = RC_SUCCESS;
	char clSreSelection[244];
	char clStfSelection[244];
	char clSorSelection[244];
	char clDrrSelection[262];
	char clStfUrno[22];
	char clPeno[24];
	char clVpfr[24];
	char clVpto[24];
	char clTohr[24];
	char clDodm[24];
	char clAvfr[24];
	char clAvto[24];
	char clTmpStr[24];
	char clSbfr[24];
	char clSbto[24];
	char clSblu[24];
	char clScod[24];
	char clTmpSday[24];
	char clSdayFrom[24];
	char clSdayTo[24];
	char clSday[24];
	char clPlfr[24],clPlto[24],clPlSCOD[24];
	long llAction = ARR_FIRST;
	long llDrrAction = ARR_FIRST;
	long llSreAction = ARR_FIRST;
	char *pclRowBuf;
	char *pclDrrRowBuf;
	short slSorCursor = 0;
	short slDrrCursor = 0;
	short slSorFunction = START;
	short slStfFunction = START;
	char *pclDataArea;
	int ilLc,ilItemCount;
	long llRowCount;
	char clLineBuffer[1024];
	char *pclLineBuffer;
	int  ilBytes = 0;
	char clOffRestIndicator[12];
	char clAcfr[24];
	char clActo[24];
	char clDrs1[4],clDrs3[4],clDrs4[4];
	char clDrrUrno[24];
	BOOL blExport;

	strncpy(clSdayFrom,pcpFrom,8);
	strncpy(clSdayTo,pcpTo,8);
	clSdayFrom[8] = '\0';
	clSdayTo[8] = '\0';

	sprintf(clAcfr,"%s000000",clSdayFrom);
	sprintf(clActo,"%s235959",clSdayTo);
	LocalTimeToUtcFixTZ(clAcfr);

	sprintf(clSreSelection,"WHERE REGI IN(%s) AND (VPTO >= '%s' OR VPTO = ' ') AND "
			" VPFR <= '%s' AND SURN != ' '",cgRegi,pcpFrom,pcpTo);
	CEDAArrayRefill(&rgSreArray.rrArrayHandle,rgSreArray.crArrayName,
			clSreSelection,NULL,llSreAction);
	CEDAArrayGetRowCount(&rgSreArray.rrArrayHandle,rgSreArray.crArrayName,&llRowCount);
	dbg(DEBUG,"%d SRETAB records loaded",llRowCount);

	memset(cgDataArea,0,sizeof(cgDataArea));
	sprintf(clSorSelection,"WHERE CODE='%s' AND (VPTO >= '%s' OR VPTO = ' ')"
		" AND VPFR <='%s' ORDER BY SURN",pcpOrgUnit,pcpFrom,pcpTo);
	dbg(DEBUG,"ExportLongTermRoster: Selection <%s>",clSorSelection);
	while(DoSelect(slSorFunction,&slSorCursor,"SORTAB",clSorSelection,
					"SURN,VPFR,VPTO",cgDataArea) == RC_SUCCESS)
	{
		slSorFunction = NEXT;

		dbg(DEBUG,"DATA-AREA <%s>",cgDataArea);
		GetDataItem(clStfUrno,cgDataArea,1,',',"","\0\0");
		dbg(DEBUG,"StfUrno <%s>",clStfUrno);
		GetDataItem(clVpfr,cgDataArea,2,',',"","\0\0");
		GetDataItem(clVpto,cgDataArea,3,',',"","\0\0");


		dbg(DEBUG,"DATA-AREA <%s> STF URNO <%s> VPFR <%s> VPTO <%s>",
				cgDataArea,clStfUrno,clVpfr,clVpto);

		sprintf(clStfSelection,"WHERE URNO='%s'",clStfUrno);
		if(DoSingleSelect("STFTAB",clStfSelection,"PENO,TOHR,DODM",cgDataArea) == RC_SUCCESS)
		{
			GetDataItem(clPeno,cgDataArea,1,',',"","\0\0");
			GetDataItem(clTohr,cgDataArea,2,',',"","\0\0");
			GetDataItem(clDodm,cgDataArea,3,',',"","\0\0");

			dbg(TRACE,"DATAAREA <%s> PENO <%s> TOHR <%s> DODM <%s>",
				cgDataArea,clPeno,clTohr,clDodm);
			if (*clTohr == *cgTohr) 
			{
				short slDrrFunction = START;

				sprintf(clDrrSelection,"WHERE STFU='%s' AND (SDAY "
					" BETWEEN '%s' AND '%s')  AND ROSL='1' AND "
					" DRRN='1' AND PRFL='1' ORDER BY SDAY",clStfUrno,clSdayFrom,clSdayTo);

				memset(cgDataArea,0,sizeof(cgDataArea));
				slDrrCursor = 0;
				while(DoSelect(slDrrFunction,&slDrrCursor,"DRRTAB",clDrrSelection,
								"SDAY,AVFR,AVTO,SBFR,SBTO,SBLU,SCOD,DRS1,DRS3,DRS4,URNO",cgDataArea) == RC_SUCCESS)
				{
					slDrrFunction = NEXT;
	
					GetDataItem(clScod,cgDataArea,7,',',"","\0\0");
					GetDataItem(clDrs4,cgDataArea,10,',',"","\0\0");
					GetDataItem(clDrrUrno,cgDataArea,11,',',"","\0\0");
					GetDataItem(clSday,cgDataArea,1,',',"","\0\0");
					if (*clDrs4 == '1')
					{
						dbg(DEBUG,"don't export this record because compl. flag is set <%s>"
						" STFU <%s> SDAY <%s> ",clDrs4,clStfUrno,clSday);
						continue;
					}
					if(IsAbsenceCode(clScod,&blExport) == RC_SUCCESS)
					{
						if (blExport == FALSE)
						{
							dbg(DEBUG,"%05d:not exported because is Absence <%s>",__LINE__,clScod);
							continue;
						}
					}
					if (CheckSre(clStfUrno,clSday) == RC_SUCCESS)
					{
						GetDataItem(clAvfr,cgDataArea,2,',',"","\0\0");
						GetDataItem(clAvto,cgDataArea,3,',',"","\0\0");
						GetDataItem(clSblu,cgDataArea,6,',',"","\0\0");
						GetDataItem(clTmpStr,cgDataArea,4,',',"","\0\0");
						StringTrimRight(clTmpStr);
						if (*clTmpStr != '\0')
						{
							strcpy(clSbfr,&clTmpStr[8]);
							if(atol(clSblu) > 0)
							{
								AddSecondsToCEDATime(clTmpStr,(time_t)atol(clSblu)*60,1);
								strcpy(clSbto,&clTmpStr[8]);
							}
							else
							{
								strcpy(clSbfr,"0000");
								strcpy(clSbto,"0000");
							}
						}
						else
						{
							strcpy(clSbfr,"0000");
							strcpy(clSbto,"0000");
						}

						strcpy(clTmpStr,&clAvfr[8]);
						strcpy(clAvfr,clTmpStr);
						strcpy(clTmpStr,&clAvto[8]);
						strcpy(clAvto,clTmpStr);

	dbg(DEBUG,"ExportLongTermRoster: <%s,%s,%s,%s,%s,%s",
			clPeno,clSday,clAvfr,clAvto,clSbfr,clSbto);
		/* check if this DRR is in VPFR,VPTO from SORTAB  and if Employee is still employed*/
						sprintf(clTmpSday,"%s000000",clSday);

						dbg(DEBUG,"SDAY <%s> VPFROM <%s> VPTO <%s> DODM <%s>",
								clTmpSday,clVpfr,clVpto,clDodm);



		dbg(DEBUG,"strcmp(clTmpSday,clVpfr) = %d",strcmp(clTmpSday,clVpfr));
		dbg(DEBUG,"strcmp(clTmpSday,clVpto)   = %d",strcmp(clTmpSday,clVpto)); 
		dbg(DEBUG,"strcmp(clTmpSday,clDodm)   = %d",strcmp(clTmpSday,clDodm));
						
						if ((strcmp(clTmpSday,clVpfr) >= 0) &&
								(strcmp(clTmpSday,clVpto) <= 0 || *clVpto == ' ') &&
								(strcmp(clTmpSday,clDodm) <= 0 || *clDodm == ' '))
						{
							/* check for overtime */
							GetDataItem(clDrs1,cgDataArea,8,',',"","\0\0");
							GetDataItem(clDrs3,cgDataArea,9,',',"","\0\0");
							if(*clDrs1 == '1' || *clDrs3 == '1' || *clDrs4 == '1')
							{
				dbg(DEBUG,"%05d:GetOvertime follows: <%s>, <%s,%s,%s>\nAVFR %s AVTO %s",__LINE__,
						cgDataArea,clDrs1,clDrs3,clDrs4,clAvfr,clAvto);
								GetOvertime(clScod,clTmpSday,clAvfr,clAvto,clDrs1,clDrs3,clDrs4);
							}

			if(IsAbsenceCode(clScod,&blExport) == RC_SUCCESS)
			{
				/** is absence, set start and end time to "000000000000" **/
				dbg(DEBUG,"shift is absence, set start <%> and end time <%> to 0",
					clAvfr,clAvto);
				strcpy(clAvfr,"0000");
				strcpy(clAvto,"0000");
			}						
			else
			{
				dbg(DEBUG,"shift <%s> is not absence",clScod);
			}
							dbg(DEBUG,"%05d:EmpX <%s> SDAY <%s> used for export range "
							"<%s/%s> VPFR %s VPTO %s\nAVFR %s AVTO %s",__LINE__,
										clPeno,clSday,pcpFrom,pcpTo,clVpfr,clVpto,clAvfr,clAvto);
							GetOffRestIndicator(clOffRestIndicator,clScod);

							pclLineBuffer = clLineBuffer;
							memset(clLineBuffer,0,sizeof(clLineBuffer));

							ilBytes = 0;
							StringPut(pclLineBuffer,"10",2);
							pclLineBuffer += 2;
							ilBytes += 2;
							StringPut(pclLineBuffer,clPeno,8);
							pclLineBuffer += 8;
							ilBytes += 8;
							StringPut(pclLineBuffer,clSday,8);
							pclLineBuffer += 8;
							ilBytes += 8;
							StringPut(pclLineBuffer,clAvfr,4);
							pclLineBuffer += 4;
							ilBytes += 4;
							StringPut(pclLineBuffer,clAvto,4);
							pclLineBuffer += 4;
							ilBytes += 4;
							StringPut(pclLineBuffer,clOffRestIndicator,1);
							pclLineBuffer += 1;
							ilBytes += 1;
							
							StringPut(pclLineBuffer,clSbfr,4);
							pclLineBuffer += 4;
							ilBytes += 4;
							StringPut(pclLineBuffer,clSbto,4);
							pclLineBuffer += 4;
							ilBytes += 4;
							GetSbluFromBSDTAB(clScod,clSblu);
							sprintf(clSblu,"%04d",atoi(clSblu));
							StringPut(pclLineBuffer,clSblu,4);
							pclLineBuffer += 4;
							ilBytes += 4;
#ifdef TEST
							StringPut(pclLineBuffer,clScod,8);
							pclLineBuffer += 8;
							ilBytes += 8;
#endif
							StringPut(pclLineBuffer,"\r\n",2);
							ilBytes += 2;

							dbg(DEBUG,"ExportLongTermRoster: <%s>",clLineBuffer);

							if((ilRc  = fwrite(clLineBuffer,sizeof(char),ilBytes,pfpFile)) != ilBytes)
							{
								dbg(TRACE,"could not write long term roster line: <%s>, reason %d <%s>",
										clLineBuffer,errno,strerror(errno));
								ilRc = RC_FAIL;
							}
							else
							{
								char clSqlBuf[244];
								char clDataArea[2440];
								short slCursor = 0;

								ilRc = RC_SUCCESS;
							
								sprintf(clSqlBuf,"UPDATE DRRTAB SET PRFL='0' "
								" WHERE URNO = '%s'",clDrrUrno);
								ilRc = sql_if(START,&slCursor,clSqlBuf,clDataArea);
								if(ilRc != RC_SUCCESS)
								{
									if(ilRc != RC_NOTFOUND)
									{ 
										/* there must be a database error */
										get_ora_err(ilRc, cgErrMsg);
									dbg(TRACE,"Update DRRTAB failed RC=%d <%s>",ilRc,cgErrMsg);
										dbg(TRACE,"SQL: <%s>",clSqlBuf);
										rollback();
									}
								}
								else
								{
									commit_work();
								}
								close_my_cursor(&slCursor);
							}
						}
						else
						{
							dbg(DEBUG,"Emp <%s> SDAY <%s> not used range <%s/%s> VPFR %s VPTO %s",

										clPeno,clSday,pcpFrom,pcpTo,clVpfr,clVpto);
						}	
					}
				}
			}
		}
		else
		{
			dbg(TRACE,"SORTAB SURN <%s> not found in STFTAB.URNO",clStfUrno);
		}
	}
	close_my_cursor(&slSorCursor);


	return RC_SUCCESS;
}

static int LongTermRosterExport()
{
	int ilRc = RC_SUCCESS;
	short slFunc = START;
	char clTmpFilePath[512];
	char clTmpTmpFilePath[512];
	char clExportFilePath[512];
	char clExportFile[32];
	char clFileMonth[32];
	char clSequence[32];
	char clOutputFilePath[512];
	char clMonth[12];
	char clTimeStamp[24];
	char *pclBaseName;
	long llSequence = 0;

	while(GetFileNames(slFunc,cgLtrFileId,cgTmpPath,clTmpTmpFilePath,TRUE) == RC_SUCCESS)
	{
		slFunc = NEXT;
		GetServerTimeStamp("LOC", 1,0,clTimeStamp);
		strncpy(clMonth,&clTimeStamp[4],2);
		clMonth[2] = '\0';
		pclBaseName = strrchr(clTmpTmpFilePath,'/') + 1;
		pclBaseName = pclBaseName == NULL ? clTmpTmpFilePath : pclBaseName;
		llSequence = GetSequence("SAPLTR",clMonth);
		MakeFileName(cgLtrFileId,++llSequence,clExportFile);
		sprintf(clExportFilePath,"%s%s%s.tmp",
				cgFileBase,cgExportPath,clExportFile);

		dbg(TRACE,"Long Term Roster File <%s> found to move to <%s>",
					clTmpTmpFilePath,clExportFilePath);

		if(rename(clTmpTmpFilePath,clExportFilePath) != RC_SUCCESS)
		{
			dbg(TRACE,"LongTermRosterExport: cannot rename file, reason: %d %s",
								errno,strerror(errno));
		}
		else
		{
				FILE *pflInput,*pflOutput;

				errno = 0;
				pflInput = fopen(clExportFilePath,"rt");
				if (pflInput == NULL)
				{
					dbg(TRACE,"LongTermRoster File <%s> not opened, reason: %d %s",
						clExportFilePath,errno,strerror(errno));
					ilRc = RC_FAIL;
				}
				else
				{
					sprintf(clOutputFilePath,"%s%s%s",cgFileBase,cgExportPath,clExportFile);
					pflOutput = fopen(clOutputFilePath,"wt");
					if (pflOutput == NULL)
					{
						dbg(TRACE,"LongTermRoster File <%s> not created, reason: %d %s",
							clExportFilePath,errno,strerror(errno));
						ilRc = RC_FAIL;
					}
					else
					{
						dbg(TRACE,"LongTermRoster File <%s> created",clOutputFilePath);
						WriteHeader(pflOutput,cgLtrFileId,clExportFile,0);
						igLineNo = 0;
  					memset(cgLineBuf,0,sizeof(cgLineBuf));
						while(fgets(cgLineBuf,sizeof(cgLineBuf),pflInput) != NULL)
						{
							int ilBytes;

							igLineNo++;
							errno = 0;
							ilBytes = strlen(cgLineBuf);
							if((ilRc  = fwrite(cgLineBuf,sizeof(char),ilBytes,pflOutput)) != ilBytes)
							{
								dbg(TRACE,"could not write long term roster line: <%s>, reason %d <%s>",
										cgLineBuf,errno,strerror(errno));
								sprintf(cgErrorString,"could not write long term roster "
									" reason %d <%s>",errno,strerror(errno));
								LogError(cgExportPath,cgErrorString,TRUE);
								ilRc = RC_FAIL;
								break;
							}
						}
						WriteTrailer(pflOutput);
						rewind(pflOutput);
						WriteHeader(pflOutput,cgLtrFileId,clExportFile,igLineNo);
						fclose(pflInput);
						fclose(pflOutput);
						PutSequence("SAPLTR",llSequence,clMonth);
						unlink(clExportFilePath);
					}
				}	
			}
		}
  return ilRc;
}

static int WriteTrailer(FILE *pflFile)
{
	int ilRc = RC_SUCCESS;
	char clTrailer[24];

	int ilBytes;

	sprintf(clTrailer,"99\r\n");
	ilBytes = strlen(clTrailer);
	errno = 0;
	if(fwrite(clTrailer,sizeof(char),ilBytes,pflFile) != ilBytes)
	{
		dbg(TRACE,"could not write Trailer: <%s>, reason %d <%s>",
				clTrailer,errno,strerror(errno));
		ilRc = RC_FAIL;
	}
	return ilRc;
}

static int WriteHeader(FILE *pflFile,char *pcpFileId,char *pcpFileName,long lpLineCount)
{
	int ilRc = RC_SUCCESS;
	char clTimeStamp[24];
	char clHeader[300];
	int ilBytes;
	
	GetServerTimeStamp("LOC", 1,0,clTimeStamp);
	sprintf(clHeader,"00%s%s%s%010d\r\n",pcpFileId,pcpFileName,clTimeStamp,lpLineCount);
	dbg(DEBUG,"WriteHeader: <%s> <%s> <%s> %ld",
					clHeader,pcpFileName,clTimeStamp,lpLineCount);
	ilBytes = strlen(clHeader);
	errno = 0;
	if(fwrite(clHeader,sizeof(char),ilBytes,pflFile) != ilBytes)
	{
		dbg(TRACE,"could not write header: <%s>, reason %d <%s>",
				clHeader,errno,strerror(errno));
		ilRc = RC_FAIL;
	}
	return ilRc;
}


static int MakeFileName(char *pcpFileId,long plSequence,char *pcpFileName)
{
	int ilRc = RC_SUCCESS;
	long llSequence;
	char clTimeStamp[24];
	
	GetServerTimeStamp("LOC", 1,0,clTimeStamp);
	clTimeStamp[6] = '\0';

	sprintf(pcpFileName,"%s%s%06ld.dat",pcpFileId,clTimeStamp,plSequence);

	return ilRc;
}

int compareForExport(const void * pcpStr1,const void * pcpStr2)
{
	char *pclStr1 = (char *)pcpStr1;
	char *pclStr2 = (char *)pcpStr2;
	return(strcmp(pclStr1,pclStr2));
}

int compare(const void * pcpStr1,const void * pcpStr2)
{
	char *pclStr1 = (char *)pcpStr1;
	char *pclStr2 = (char *)pcpStr2;
	return(strcmp(&pclStr1[10],&pclStr2[10]));
}

void DumpFileNames(char clFileName[MAXFILENAMES][512])
{
	int ilLc;

	dbg(TRACE,"\n\nFilenames found: \n");
	for(ilLc = 0; ilLc < MAXFILENAMES; ilLc++)
	{
		if (!*clFileName[ilLc])
			break;
		dbg(TRACE,"%03d: <%s>\n",ilLc,clFileName[ilLc]);
	}
}


static int GetFileNames(short spFunc,char *pcpFileId,char *pcpFileDir,char *pcpFileName,BOOL bpForExport)
{
	static DIR *prlNewFiles = NULL;
	static char clFileName[MAXFILENAMES][512];
	char clPath[512];
	static int ilFileNo = 0;
	static int ilActualFileNo = 0;
	int ilRc = RC_SUCCESS;
	int ilRcLocal;
	struct dirent *prlBuf;

	if (spFunc == START)
	{
		ilActualFileNo = 0;
		ilFileNo = 0;
		memset(clFileName,0,sizeof(clFileName));
		memset(clPath,0,sizeof(clPath));
		sprintf(clPath,"%s%s",cgFileBase,pcpFileDir);
		dbg(DEBUG,"GetFileNames: open directory <%s>\n",clPath);
		prlNewFiles = opendir(clPath);
		if (prlNewFiles == NULL)
		{
			dbg(TRACE,"GetFileNames: directory <%s> not opened\n",clPath);
			dbg(TRACE,"<%s>\n",strerror(errno));
		}
		if (prlNewFiles)
		{
	  	while(((prlBuf = readdir(prlNewFiles)) != NULL) && ilFileNo < MAXFILENAMES)
			{
				if (prlBuf == NULL)
				{
					dbg(DEBUG,"GetFileNames: no more file entry in <%s>\n",clFileName);
					closedir(prlNewFiles);
					prlNewFiles = NULL; 
					break;
				}
				else 
				{
					dbg(DEBUG,"GetFileNames: File <%s> %d found\n",prlBuf->d_name,ilFileNo);
					if (strncmp(prlBuf->d_name,pcpFileId,strlen(pcpFileId)) == 0)
					{
						sprintf(clFileName[ilFileNo++],"%s",prlBuf->d_name);
					}
				}
			}
		}
		if (bpForExport == TRUE)
		{
			qsort(clFileName,ilFileNo,512,compareForExport);
		}
		else
		{
			qsort(clFileName,ilFileNo,512,compareForExport);
		}
		DumpFileNames(clFileName);
	}
	dbg(DEBUG,"GetFileNames: Files %d, Actual File %d",ilFileNo,ilActualFileNo);
	if(ilActualFileNo < ilFileNo)
	{
		sprintf(pcpFileName,"%s%s%s",cgFileBase,pcpFileDir,clFileName[ilActualFileNo++]);
	}
	else
	{
		*pcpFileName = '\0';
		ilRc = RC_FAIL;
	}
	dbg(DEBUG,"end of GetFileNames:\n");
	return ilRc;
}

static void StringTrimRightLineEnd(char *pcpBuffer)
{
	if (*pcpBuffer != '\0')
	{
		char *pclBlank = &pcpBuffer[strlen(pcpBuffer)-1];
		while((*pclBlank == '\r' || *pclBlank == '\n') && pclBlank != pcpBuffer)
		{
			*pclBlank = '\0';
			pclBlank--;
		}
	}
}

static void StringTrimRight(char *pcpBuffer)
{
	if (*pcpBuffer != '\0')
	{
		char *pclBlank = &pcpBuffer[strlen(pcpBuffer)-1];
		while(isspace(*pclBlank) && pclBlank != pcpBuffer)
		{
			*pclBlank = '\0';
			pclBlank--;
		}
	}
}

static void StringPut(char *pcpDest,char *pcpSource,long lpLen)
{

	int i,ilSourceLen;

	ilSourceLen = strlen(pcpSource);

	for(i = 0; i < lpLen; i++)
	{
		if (i < ilSourceLen)
		{
			pcpDest[i] = pcpSource[i];
		}
		else
		{
			pcpDest[i] = ' ';
		}
	}
}


static int DaysOfMonth[12] = {31,28,31,30,31,30,31,31,30,31,30,31};

static int CheckDateOld(char *pcpDate)
{
	int ilRc = RC_SUCCESS;
	int ilLc;
	char clYear[6];
	char clMonth[6];
	char clDay[6];
	char clHour[6];
	char clMinute[6];
	char clSecond[6];
	int ilYear,ilMonth,ilDay,ilHour,ilMinute,ilSecond;

	/*dbg(DEBUG,"CheckDate14: <%s>",pcpDate);*/
	for (ilLc = 0; pcpDate[ilLc] != '\0'; ilLc++)
	{
			if(! (pcpDate[ilLc] >= '0' && pcpDate[ilLc] <= '9'))
			{
				strcpy(pcpDate," ");
				return (RC_FAIL);
			}
	}
	if (ilLc < 12 || ilLc > 14)
	{
		dbg(TRACE,"CheckDate: invalid length %d\n",ilLc);
		strcpy(pcpDate," ");
		return (RC_FAIL);
	}
	else
	{
		dbg(TRACE,"CheckDate: valid length %d\n",ilLc);
	}
		
	for (; ilLc < 14 ; ilLc++)
	{
			pcpDate[ilLc] = '0';
	}
	pcpDate[ilLc] = '\0';
	/*dbg(DEBUG,"CheckDate14: <%s>",pcpDate);*/

	strncpy(clYear,pcpDate,4);
	clYear[4] = '\0';
	strncpy(clMonth,&pcpDate[4],2);
	clMonth[2] = '\0';
	strncpy(clDay,&pcpDate[6],2);
	clDay[2] = '\0';
	strncpy(clHour,&pcpDate[8],2);
	clHour[2] = '\0';
	strncpy(clMinute,&pcpDate[10],2);
	clMinute[2] = '\0';
	strncpy(clSecond,&pcpDate[12],2);
	clSecond[2] = '\0';

	ilYear = atoi(clYear);
	ilMonth = atoi(clMonth);
	ilDay = atoi(clDay);
	ilHour = atoi(clHour);
	ilMinute = atoi(clMinute);
	ilSecond = atoi(clSecond);

	if (ilYear < 1900 || ilYear > 2099)
	{
		dbg(TRACE,"CheckDate: invalid year <%s>\n",clYear);
		strcpy(pcpDate," ");
		return (RC_FAIL);
	}

	if (ilMonth < 1 || ilMonth > 12)
	{
		dbg(TRACE,"CheckDate: invalid month <%s>\n",clMonth);
		strcpy(pcpDate," ");
		return (RC_FAIL);
	}

	if (ilMonth == 2)
	{
		if (!(ilYear % 4))
		{
			if(ilDay > 29)
			{
				dbg(TRACE,"CheckDate: invalid day in feb of leap year <%s>\n",clDay);
				strcpy(pcpDate," ");
				return (RC_FAIL);
			}
		}
		else
		{
			if (ilDay > DaysOfMonth[ilMonth-1])
			{
				dbg(TRACE,"CheckDate: invalid day <%s> for %d\n",clDay,ilMonth);
				strcpy(pcpDate," ");
				return (RC_FAIL);
			}
		}
	}
	else
	{
			if (ilDay > DaysOfMonth[ilMonth-1])
			{
				dbg(TRACE,"CheckDate: invalid day <%s> for %d\n",clDay,ilMonth);
				strcpy(pcpDate," ");
				return (RC_FAIL);
			}
		}
	if (ilHour > 24)
	{
		dbg(TRACE,"CheckDate: invalid hour <%s>\n",clHour);
		strcpy(pcpDate," ");
		return (RC_FAIL);
	}
	if (ilMinute > 60)
	{
		dbg(TRACE,"CheckDate: invalid minute <%s>\n",clMinute);
		strcpy(pcpDate," ");
		return (RC_FAIL);
	}
	if (ilSecond > 60)
	{
		dbg(TRACE,"CheckDate: invalid second <%s>\n",clSecond);
		strcpy(pcpDate," ");
		return (RC_FAIL);
	}
	return ilRc;
}	

static void StringMid(char *pcpDest,char *pcpSource,long lpFrom,long lpLen,int ipConvert)
{
	strncpy(pcpDest,&pcpSource[lpFrom],lpLen);
	pcpDest[lpLen] = '\0';
	switch(ipConvert)
	{
	case CLIENTTODB:
		ConvertClientStringToDb(pcpDest);
		break;
	case DBTOCLIENT:
		ConvertDbStringToClient(pcpDest);
		break;
	default:
			/* do nothing */
			;
	}
}

static char *ItemPut(char *pcpDest,char *pcpSource,char cpDelimiter)
{
	strcpy(pcpDest,pcpSource);
	pcpDest += strlen(pcpDest);
	if (cpDelimiter != '\0')
	{
		*pcpDest = cpDelimiter;
		pcpDest++;
	}
	return (pcpDest);
}

static int LogError(char *pcpFileName,char *pcpErrorString,BOOL bpAlert)
{
	int ilRc = RC_SUCCESS;

	if (pfgLog == NULL)
	{
		char clLogFileName[512];

		sprintf(clLogFileName,"%s%ssaphdl%ld",cgFileBase,cgFileLog,getpid());
		pfgLog = fopen(clLogFileName,"a");
		if (pfgLog != NULL)
		{
			dbg(DEBUG,"Logfile <%s> opened",clLogFileName);
		}
		else
		{
			dbg(DEBUG,"Logfile <%s> not opened",clLogFileName);
		}
	}
	if (pfgLog != NULL)
	{
		char clTimeStamp[24];

		GetServerTimeStamp("LOC", 1,0,clTimeStamp);
		fprintf(pfgLog,"%s: File %s Error: <%s> Line: <%s>\r\n",
				clTimeStamp,pcpFileName,pcpErrorString,cgLineBuf);
		dbg(DEBUG,"File %s %s <%s>",pcpFileName,pcpErrorString,cgLineBuf);
		fclose(pfgLog);
		pfgLog = NULL;
		/*
		AddAlert(cgAlertName,"R",clTimeStamp,"D",pcpErrorString,TRUE,TRUE,
				cgDestName,cgRecvName,cgTwStart,cgTwEnd);
				**/
		if (bpAlert == TRUE)
		{
			AddAlert2(cgAlertName,"R"," "," ",pcpErrorString,cgLineBuf,TRUE,FALSE,
				cgDestName,cgRecvName,cgTwStart,cgTwEnd);
		}
	}
	return ilRc;
}

static void PutSequence(char *pcpKey,long lpSequence,char *pcpMonth)
{
	int ilRc;

	short slCursor = 0;
	char clSqlBuf[200];

	sprintf(clSqlBuf,"UPDATE NUMTAB SET ACNU='%10d',FLAG='%s' WHERE KEYS='%s' "
		" AND HOPO='%s'",lpSequence,pcpMonth,pcpKey,cgHopo);
	ilRc = sql_if(START,&slCursor,clSqlBuf,cgDataArea);
	if(ilRc == RC_NOTFOUND)
	{
		/* sequence not yet written, create new one */
		sprintf(clSqlBuf,
			"INSERT INTO NUMTAB (ACNU,FLAG,HOPO,KEYS,MAXN,MINN) "
				"VALUES(%d,'%s','%s','%s',9999999999,1)",
			lpSequence,pcpMonth,cgHopo,pcpKey);
		close_my_cursor(&slCursor);
		slCursor = 0;
		ilRc = sql_if(START,&slCursor,clSqlBuf,cgDataArea);
		if (ilRc != RC_SUCCESS)
		{
			/* there must be a database error */
	 		get_ora_err (ilRc, cgErrMsg);
			dbg(TRACE,"Inserting Sequence <%s> failed RC=%d",pcpKey,ilRc);
		}
		close_my_cursor(&slCursor);
  }
	else if (ilRc != DB_SUCCESS)
	{ 
		/* there must be a database error */
	 	get_ora_err(ilRc, cgErrMsg);
		dbg(TRACE,"Reading Sequence <%s> failed RC=%d",pcpKey,ilRc);
	}
	commit_work();
	close_my_cursor(&slCursor);
	slCursor = 0;
}


static long GetSequence(char *pcpKey,char *pcpMonth)
{
	int ilRc;
	long llSequence = 0;
	char clSelection[200];
	char clSequence[24];
	char clTimeStamp[24];
	char clFlag[32];
	char clMonth[32];

	sprintf(clSelection,"WHERE KEYS='%s' AND HOPO='%s'",
			pcpKey,cgHopo);
	if ((ilRc = DoSingleSelect("NUMTAB",clSelection,"ACNU,FLAG",cgDataArea)) == RC_SUCCESS)
	{
		GetDataItem(clSequence,cgDataArea,1,',',"","\0\0");
		GetDataItem(clFlag,cgDataArea,2,',',"","\0\0");

		dbg(DEBUG,"GetSequence: Sequence <%s> Month <%s>",clSequence,clFlag);

		if(strncmp(pcpMonth,clFlag,2) == 0)
		{
			llSequence = atol(clSequence);
		}
		else
		{
			llSequence = 0;
			StringTrimRight(clFlag);
			dbg(DEBUG,"Month changed from <%s> to <%s> use new Sequence %d",
					clFlag,pcpMonth,llSequence);
		}
  }
	else if(ilRc == RC_NOTFOUND)
	{
		GetServerTimeStamp("LOC", 1,0,clTimeStamp);
		StringMid(clMonth,clTimeStamp,4,2,CLIENTTODB);
		PutSequence(pcpKey,0,clMonth);
		llSequence = 0;
	} 
	else
	{ 
		/* there must be a database error */
		dbg(TRACE,"Reading Sequence <%s> failed RC=%d",pcpKey,ilRc);
	}
	return llSequence;
}

static int DoSelect(short spFunction,short *pspCursor,char *pcpTable,char *pcpSelection,char *pcpFields,char *pcpDataArea)
{
	int ilRc = RC_SUCCESS;
	static char clSqlBuf[4096];

	char *pclDataArea;
	int ilLc,ilItemCount;

	sprintf(clSqlBuf,"SELECT %s FROM %s %s",pcpFields,pcpTable,pcpSelection);

	dbg(DEBUG,"DoSelect: clSqlBuf <%s>",clSqlBuf);
  *pcpDataArea = '\0';
	ilRc = sql_if(spFunction,pspCursor,clSqlBuf,pcpDataArea);
	if(ilRc != RC_SUCCESS && ilRc != NOTFOUND)
	{
		/* there must be a database error */
		get_ora_err(ilRc, cgErrMsg);
		dbg(TRACE,"DoSelect failed RC=%d <%s>",ilRc,cgErrMsg);
		dbg(TRACE,"SQL: <%s>",clSqlBuf);
	}
	if (ilRc != RC_SUCCESS)
	{
		close_my_cursor(pspCursor);
		*pspCursor = 0;
	}

	ilItemCount = get_no_of_items(pcpFields); 
	pclDataArea = pcpDataArea;
	for(ilLc =  1; ilLc < ilItemCount; ilLc++)
	{
		while(*pclDataArea != '\0')
		{
			pclDataArea++;
		}
			*pclDataArea = ',';
	}
  if (ilRc == RC_SUCCESS)
	{
		dbg(DEBUG,"DoSelect: RC=%d  items=%d <%s>",ilRc,ilItemCount,pcpDataArea);
	}
	else
	{
		dbg(DEBUG,"DoSelect: RC=%d",ilRc,pcpDataArea);
	}
	return(ilRc);
}

static int DoSingleSelect(char *pcpTable,char *pcpSelection,char *pcpFields,char *pcpDataArea)
{
	int ilRc = RC_SUCCESS;
	static char clSqlBuf[4096];

	short slCursor = 0;
	short slFunction;
	char *pclDataArea;
	int ilLc,ilItemCount;

	sprintf(clSqlBuf,"SELECT %s FROM %s %s",pcpFields,pcpTable,pcpSelection);

  *pcpDataArea = '\0';
	ilRc = sql_if(START,&slCursor,clSqlBuf,pcpDataArea);
	if(ilRc != RC_SUCCESS && ilRc != NOTFOUND)
	{
		/* there must be a database error */
		get_ora_err(ilRc, cgErrMsg);
		dbg(TRACE,"DoSingleSelect failed RC=%d <%s>",ilRc,cgErrMsg);
		dbg(TRACE,"SQL: <%s>",clSqlBuf);
	}
	close_my_cursor(&slCursor);
	slCursor = 0;

	ilItemCount = get_no_of_items(pcpFields); 
	pclDataArea = pcpDataArea;
	for(ilLc =  1; ilLc < ilItemCount; ilLc++)
	{
		while(*pclDataArea != '\0')
		{
			pclDataArea++;
		}
			*pclDataArea = ',';
		}
  if (ilRc == RC_SUCCESS)
	{
		dbg(DEBUG,"DoSingleSelect: RC=%d <%s>",ilRc,pcpDataArea);
	}
	else
	{
		dbg(DEBUG,"DoSingleSelect: RC=%d",ilRc);
	}
	return(ilRc);
}

static int	DoDelete(char *pcpTable,char *pcpSelection)
{
	int ilRc = RC_SUCCESS;
	static char clSqlBuf[4096];
	char clTimeStamp[24];

	short slCursor = 0;
	short slFunction;

	sprintf(clSqlBuf,"DELETE FROM %s %s",pcpTable,pcpSelection);

	if (debug_level == DEBUG)
	{
		dbg(DEBUG,"DoDelete: <%s>",clSqlBuf);
	}
	ilRc = sql_if(START,&slCursor,clSqlBuf,cgDataArea);
	if(ilRc != RC_SUCCESS)
	{
		if(ilRc != RC_NOTFOUND)
		{ 
			/* there must be a database error */
			get_ora_err(ilRc, cgErrMsg);
			dbg(TRACE,"DoDelete failed RC=%d <%s>",ilRc,cgErrMsg);
			dbg(TRACE,"SQL: <%s>",clSqlBuf);
		}
	}
	else
	{
		commit_work();
		/* send updates to loghdl and bchdl */
    SendCedaEvent(igLoghdl,0,cgDestName,cgAlertName,cgTwStart, cgTwEnd,
          "DRT",pcpTable,pcpSelection," "," ","",5,0);          
    SendCedaEvent(igBchdl,0,cgDestName,cgAlertName,cgTwStart, cgTwEnd,
          "DRT",pcpTable,pcpSelection," "," ","",3,0);          
    SendCedaEvent(igAction,0,cgDestName,cgAlertName,cgTwStart, cgTwEnd,
          "DRT",pcpTable,pcpSelection," "," ","",3,0);          
	}
	commit_work();
	close_my_cursor(&slCursor);
	slCursor = 0;
	return(ilRc);
}

static int	DoUpdate2(char *pcpTable,char *pcpSelection,char *pcpUpdateFields,
	char *pcpSendFields,char *pcpUrno,char *pcpDataArea,BOOL bpBroadcast)
{
	int ilRc = RC_SUCCESS;
	static char clSqlBuf[4096];
	static char clDataArea[40000];
	static char clFieldList[2000];
	static char clSendFieldList[2000];
	char clTimeStamp[24];
	char clSelection[204];

	short slCursor = 0;
	short slFunction;
	char *pclDataArea;
	char *pclData;

	sprintf(clSelection,"WHERE URNO = '%s'",pcpUrno);
	dbg(DEBUG,"DoUpdate: Selection <%s>",clSelection);
	strcpy(clDataArea,pcpDataArea);
	strcpy(clFieldList,pcpUpdateFields);
	sprintf(clSqlBuf,"UPDATE %s SET %s %s",
		pcpTable,clFieldList,pcpSelection);

	if (clDataArea[strlen(clDataArea)-1] != ',')
	{
		strcat(clDataArea,",");
	}

	if (debug_level == DEBUG)
	{
		dbg(DEBUG,"DoUpdate: <%s>",clSqlBuf);
		dbg(DEBUG,"DoUpdate: <%s>",clDataArea);
	}
	pclData = strdup(clDataArea);
	delton(clDataArea);
	ilRc = sql_if(START,&slCursor,clSqlBuf,clDataArea);
	if(ilRc != RC_SUCCESS)
	{
		if(ilRc != RC_NOTFOUND)
		{ 
			/* there must be a database error */
			get_ora_err(ilRc, cgErrMsg);
			dbg(TRACE,"DoUpdate failed RC=%d <%s>",ilRc,cgErrMsg);
			dbg(TRACE,"SQL: <%s>",clSqlBuf);
			rollback();
		}
	}
	else
	{
		commit_work();
		/* don't send updates to loghdl and bchdl */
	}
	free(pclData);
	close_my_cursor(&slCursor);
	slCursor = 0;
	return(ilRc);
}

static int	DoUpdate3(char *pcpTable,char *pcpSelection,char *pcpUpdateFields,
	char *pcpSendFields,char *pcpUrno,char *pcpDataArea,BOOL bpBroadcast)
{
	int ilRc = RC_SUCCESS;
	static char clSqlBuf[4096];
	static char clDataArea[40000];
	static char clFieldList[2000];
	static char clSendFieldList[2000];
	char clTimeStamp[24];
	char clSelection[204];

	short slCursor = 0;
	short slFunction;
	char *pclDataArea;
	char *pclData;

	sprintf(clSelection,"WHERE URNO = '%s'",pcpUrno);
	dbg(DEBUG,"DoUpdate3: Selection <%s>",clSelection);
	strcpy(clDataArea,pcpDataArea);
	sprintf(clFieldList,"%s",pcpUpdateFields);
	sprintf(clSendFieldList,"%s",pcpSendFields);
	sprintf(clSqlBuf,"UPDATE %s SET %s %s",
		pcpTable,clFieldList,pcpSelection);

	if (debug_level == DEBUG)
	{
		dbg(DEBUG,"DoUpdate3: <%s>",clSqlBuf);
		dbg(DEBUG,"DoUpdate3: <%s>",clDataArea);
	}
	pclData = strdup(clDataArea);
	delton(clDataArea);
	ilRc = sql_if(START,&slCursor,clSqlBuf,clDataArea);
	if(ilRc != RC_SUCCESS)
	{
		if(ilRc != RC_NOTFOUND)
		{ 
			/* there must be a database error */
			get_ora_err(ilRc, cgErrMsg);
			dbg(TRACE,"DoUpdate failed RC=%d <%s>",ilRc,cgErrMsg);
			dbg(TRACE,"SQL: <%s>",clSqlBuf);
			rollback();
		}
	}
	else
	{
		commit_work();
		/* send updates to loghdl and bchdl */
		if ((pclData != NULL) && (bpBroadcast))
		{
    	SendCedaEvent(igLoghdl,0,cgDestName,cgAlertName,cgTwStart, cgTwEnd,
          "URT",pcpTable,clSelection,clSendFieldList,pclData,"",5,0);          
    	SendCedaEvent(igBchdl,0,cgDestName,cgAlertName,cgTwStart, cgTwEnd,
          "URT",pcpTable,clSelection,clSendFieldList,pclData,"",3,0);          
    	SendCedaEvent(igAction,0,cgDestName,cgAlertName,cgTwStart, cgTwEnd,
          "URT",pcpTable,clSelection,clSendFieldList,pclData,"",3,0);          
		}
		else
		{
			dbg(TRACE,"DoUpdate: not enough memory to send data to loghdl");
			Terminate(20);
		}
	}
	free(pclData);
	close_my_cursor(&slCursor);
	slCursor = 0;
	return(ilRc);
}


static int	DoUpdate(char *pcpTable,char *pcpSelection,char *pcpUpdateFields,
	char *pcpSendFields,char *pcpUrno,char *pcpDataArea,BOOL bpBroadcast)
{
	int ilRc = RC_SUCCESS;
	static char clSqlBuf[4096];
	static char clDataArea[40000];
	static char clFieldList[2000];
	static char clSendFieldList[2000];
	char clTimeStamp[24];
	char clSelection[204];

	short slCursor = 0;
	short slFunction;
	char *pclDataArea;
	char *pclData;

	sprintf(clSelection,"WHERE URNO = '%s'",pcpUrno);
	dbg(DEBUG,"DoUpdate: Selection <%s>",clSelection);
	strcpy(clDataArea,pcpDataArea);
	sprintf(clFieldList,"%s,LSTU=:VLSTU,USEU=:VUSEU",pcpUpdateFields);
	sprintf(clSendFieldList,"%s,LSTU,USEU",pcpSendFields);
	sprintf(clSqlBuf,"UPDATE %s SET %s %s",
		pcpTable,clFieldList,pcpSelection);

	if (clDataArea[strlen(clDataArea)-1] != ',')
	{
		strcat(clDataArea,",");
	}
	pclDataArea = clDataArea + strlen(clDataArea);
	GetServerTimeStamp("UTC", 1,0,clTimeStamp);
	pclDataArea = ItemPut(pclDataArea,clTimeStamp,',');
	pclDataArea = ItemPut(pclDataArea,cgAlertName,'\0');

	if (debug_level == DEBUG)
	{
		dbg(DEBUG,"DoUpdate: <%s>",clSqlBuf);
		dbg(DEBUG,"DoUpdate: <%s>",clDataArea);
	}
	pclData = strdup(clDataArea);
	delton(clDataArea);
	ilRc = sql_if(START,&slCursor,clSqlBuf,clDataArea);
	if(ilRc != RC_SUCCESS)
	{
		if(ilRc != RC_NOTFOUND)
		{ 
			/* there must be a database error */
			get_ora_err(ilRc, cgErrMsg);
			dbg(TRACE,"DoUpdate failed RC=%d <%s>",ilRc,cgErrMsg);
			dbg(TRACE,"SQL: <%s>",clSqlBuf);
			rollback();
		}
	}
	else
	{
		commit_work();
		/* send updates to loghdl and bchdl */
		if ((pclData != NULL) && (bpBroadcast))
		{
    	SendCedaEvent(igLoghdl,0,cgDestName,cgAlertName,cgTwStart, cgTwEnd,
          "URT",pcpTable,clSelection,clSendFieldList,pclData,"",5,0);          
    	SendCedaEvent(igBchdl,0,cgDestName,cgAlertName,cgTwStart, cgTwEnd,
          "URT",pcpTable,clSelection,clSendFieldList,pclData,"",3,0);          
    	SendCedaEvent(igAction,0,cgDestName,cgAlertName,cgTwStart, cgTwEnd,
          "URT",pcpTable,clSelection,clSendFieldList,pclData,"",3,0);          
		}
		else
		{
			dbg(TRACE,"DoUpdate: not enough memory to send data to loghdl");
			Terminate(20);
		}
	}
	free(pclData);
	close_my_cursor(&slCursor);
	slCursor = 0;
	return(ilRc);
}

static int DoInsert2(char *pcpTable,char *pcpFields,
					char *pcpInsertFields,char *pcpDataArea,BOOL bpBroadcast)
{
	int ilRc = RC_SUCCESS;
	static char clSqlBuf[4096];
	static char clDataArea[40000];
	char clTimeStamp[24];
	static char clFieldList[2000];
	char clUrno[24];

	short slCursor = 0;
	short slFunction;
	char *pclDataArea;
	char *pclData;

	strcpy(clDataArea,pcpDataArea);
	dbg(DEBUG,"<%s>",clDataArea);
	sprintf(clFieldList,"%s",pcpFields);
	sprintf(clSqlBuf,"INSERT INTO %s (%s) VALUES(%s)",
		pcpTable,pcpFields,pcpInsertFields);

	if (clDataArea[strlen(clDataArea)-1] != ',')
	{
		strcat(clDataArea,",");
	}
	if (debug_level == DEBUG)
	{
		dbg(DEBUG,"DoInsert: <%s>",clSqlBuf);
		dbg(DEBUG,"DoInsert: <%s>",clDataArea);
	}
	pclData = strdup(clDataArea);
	delton(clDataArea);
	ilRc = sql_if(START,&slCursor,clSqlBuf,clDataArea);
	if(ilRc != RC_SUCCESS)
	{
		/* there must be a database error */
		get_ora_err(ilRc, cgErrMsg);
		dbg(TRACE,"DoInsert failed RC=%d <%s>",ilRc,cgErrMsg);
		dbg(TRACE,"SQL: <%s>",clSqlBuf);
		rollback();
	}
	else
	{
		commit_work();
		/* send inserts to loghdl */
		if ((pclData != NULL) && (bpBroadcast))
		{
    	SendCedaEvent(igLoghdl,0,cgDestName,cgAlertName,cgTwStart, cgTwEnd,
          "IRT",pcpTable," ",clFieldList,pclData,"",5,0);          
    	SendCedaEvent(igBchdl,0,cgDestName,cgAlertName,cgTwStart, cgTwEnd,
          "IRT",pcpTable," ",clFieldList,pclData,"",3,0);          
    	SendCedaEvent(igAction,0,cgDestName,cgAlertName,cgTwStart, cgTwEnd,
          "IRT",pcpTable," ",clFieldList,pclData,"",3,0);          
		}
		else
		{
			dbg(TRACE,"DoInsert: not enough memory to send data to loghdl");
			Terminate(20);
		}
	}

	free(pclData);
	strcpy(pcpDataArea,clUrno);
	close_my_cursor(&slCursor);
	slCursor = 0;
	return(ilRc);
}


static int DoInsert(char *pcpTable,char *pcpFields,
					char *pcpInsertFields,char *pcpDataArea,BOOL bpBroadcast)
{
	int ilRc = RC_SUCCESS;
	static char clSqlBuf[4096];
	static char clDataArea[40000];
	char clTimeStamp[24];
	static char clFieldList[2000];
	char clUrno[24];

	short slCursor = 0;
	short slFunction;
	char *pclDataArea;
	char *pclData;

	strcpy(clDataArea,pcpDataArea);
	dbg(DEBUG,"<%s>",clDataArea);
	sprintf(clFieldList,"%s,CDAT,USEC,URNO,HOPO",pcpFields);
	sprintf(clSqlBuf,"INSERT INTO %s (%s,CDAT,USEC,URNO,HOPO) VALUES(%s,"
	":VCDAT,:VUSEC,:VURNO,:VHOPO)",
		pcpTable,pcpFields,pcpInsertFields);

	if (clDataArea[strlen(clDataArea)-1] != ',')
	{
		strcat(clDataArea,",");
	}
	pclDataArea = clDataArea + strlen(clDataArea);
	GetServerTimeStamp("UTC", 1,0,clTimeStamp);
	pclDataArea = ItemPut(pclDataArea,clTimeStamp,',');
	pclDataArea = ItemPut(pclDataArea,cgAlertName,',');
	GetNextValues(clUrno,1);
	pclDataArea = ItemPut(pclDataArea,clUrno,',');
	pclDataArea = ItemPut(pclDataArea,cgHopo,'\0');
	if (debug_level == DEBUG)
	{
		dbg(DEBUG,"DoInsert: <%s>",clSqlBuf);
		dbg(DEBUG,"DoInsert: <%s>",clDataArea);
	}
	pclData = strdup(clDataArea);
	delton(clDataArea);
	ilRc = sql_if(START,&slCursor,clSqlBuf,clDataArea);
	if(ilRc != RC_SUCCESS)
	{
		/* there must be a database error */
		get_ora_err(ilRc, cgErrMsg);
		dbg(TRACE,"DoInsert failed RC=%d <%s>",ilRc,cgErrMsg);
		dbg(TRACE,"SQL: <%s>",clSqlBuf);
		rollback();
	}
	else
	{
		commit_work();
		/* send inserts to loghdl */
		if ((pclData != NULL) && (bpBroadcast))
		{
    	SendCedaEvent(igLoghdl,0,cgDestName,cgAlertName,cgTwStart, cgTwEnd,
          "IRT",pcpTable," ",clFieldList,pclData,"",5,0);          
    	SendCedaEvent(igBchdl,0,cgDestName,cgAlertName,cgTwStart, cgTwEnd,
          "IRT",pcpTable," ",clFieldList,pclData,"",3,0);          
    	SendCedaEvent(igAction,0,cgDestName,cgAlertName,cgTwStart, cgTwEnd,
          "IRT",pcpTable," ",clFieldList,pclData,"",3,0);          
		}
		else
		{
			dbg(TRACE,"DoInsert: not enough memory to send data to loghdl");
			Terminate(20);
		}
	}

	free(pclData);
	strcpy(pcpDataArea,clUrno);
	close_my_cursor(&slCursor);
	slCursor = 0;
	return(ilRc);
}

static int EmpContactDetails(char *pcpBuf)
{
	int ilRc;
	int ilActualPos = 0;
	char *pclDataArea;
	char clTmpString[512];
	char clSelection[512];
	char *pclBuf;

	char clPeno[32];
	char clDate[32];
	char clUrno[32];
	char clStfUrno[32] = "";
	char clStfTeld[120];
	char clAdrTel1[20];
	char clAdrTel2[20];

	pclBuf = pcpBuf;

	/* Employee Number */
	StringMid(clPeno,pclBuf,ilActualPos,8,CLIENTTODB);
	ilActualPos += 8;
	/* Effective Date  */
	StringMid(clDate,pclBuf,ilActualPos,8,CLIENTTODB);
	strcat(clDate,"000000");
	ilActualPos += 8;
	/* Phone Number Business (STF.TELD) */
	StringMid(clStfTeld,pclBuf,ilActualPos,14,CLIENTTODB);
	ilActualPos += 14;
	/* Phone Number Private 1 (ADR.TEL1) */
	StringMid(clAdrTel1,pclBuf,ilActualPos,14,CLIENTTODB);
	ilActualPos += 14;
	/* Phone Number Private 2 (ADR.TEL2) */
	StringMid(clAdrTel2,pclBuf,ilActualPos,14,CLIENTTODB);
	ilActualPos += 14;

	/* Update ADRTAB */
	sprintf(clSelection,"WHERE PENO='%s'",clPeno);
	if ((ilRc = DoSingleSelect("STFTAB",clSelection,"URNO",clStfUrno)) == RC_SUCCESS)
	{
		dbg(DEBUG,"EmpContactDetails:DoSingleSelect STFTAB <%s> successfull URNO=<%s>",
				clPeno,clStfUrno);
		strcpy(cgDataArea,clStfTeld);
		ilRc = DoUpdate("STFTAB",clSelection,"TELD=:VTELD","TELD",clStfUrno,cgDataArea,TRUE);
	}
	else
	{
		dbg(TRACE,"EmpContactDetails: STF Record <%s> not found",clPeno);
	}

	sprintf(clSelection,"WHERE PENO ='%s' AND ((VATO >= '%s' OR VATO = ' ') AND"
		" (VAFR <= '%s'))",clPeno,clDate,clDate);
	if ((ilRc = DoSingleSelect("ADRTAB",clSelection,"URNO",clUrno)) == RC_SUCCESS)
	{
		/* found at least one matching ADRTAB */

		dbg(DEBUG,"EmpContactDetails:DoSingleSelect successfull URNO=<%s>",clUrno);
		sprintf(cgDataArea,"%s,%s",clAdrTel1,clAdrTel2);
		ilRc = DoUpdate("ADRTAB",clSelection,"TEL1=:VTEL1,TEL2=:VTEL2","TEL1,TEL2",clUrno,cgDataArea,TRUE);
	}
	else
	{
		dbg(TRACE,"EmpContactDetails: Adr Record <%s> not found",clPeno);
	}

	return ilRc;
}

static int GetStfUrno(char *pcpStfUrno,char *pcpPeno)
{
	int ilRc;
	long llRow = ARR_FIRST;

	dbg(TRACE,"search URNO for PENO <%s> ",pcpPeno);
		/*StringTrimRight(clSurn);*/
		ilRc = CEDAArrayFindRowPointer(&rgStfArray.rrArrayHandle,
			rgStfArray.crArrayName,&rgStfArray.rrIdx02Handle,
					rgStfArray.crIdx02Name,pcpPeno,&llRow,
							(void **)&rgStfArray.pcrArrayRowBuf);
	if (ilRc == RC_SUCCESS)
	{
		strcpy(pcpStfUrno,FIELDVAL(rgStfArray,rgStfArray.pcrArrayRowBuf,igStfURNO));
	}
	else
	{
		*pcpStfUrno = '\0';
	}

	return ilRc;
}


static int EmpRegularityIndicator(char *pcpBuf)
{
	int ilRc;
	int ilActualPos = 0;
	char *pclDataArea;
	char clTmpString[512];
	char clSelection[512];
	char clRegi[12];
	char *pclBuf;

	char clPeno[32];
	char clStartDate[32];
	char clEndDate[32];
	char clUrno[32];
	char clStfUrno[32] = "";
	char clStfTeld[120];
	char clAdrTel1[20];
	char clAdrTel2[20];
	char clToday[24];

	pclBuf = pcpBuf;

	/* Employee Number */
	StringMid(clPeno,pclBuf,ilActualPos,8,CLIENTTODB);
	ilActualPos += 8;
	/* shift indicator REGI */
	StringMid(clRegi,pclBuf,ilActualPos,1,CLIENTTODB);
	ilActualPos += 1;
	/* Effective Start Date  */
	StringMid(clStartDate,pclBuf,ilActualPos,8,CLIENTTODB);
	strcat(clStartDate,"000000");
	ilActualPos += 8;
	/* Effective End Date  */
	StringMid(clEndDate,pclBuf,ilActualPos,8,CLIENTTODB);
	if (*clEndDate == ' ')
	{
		strcpy(clEndDate," ");
	}
	else
	{
		strcat(clEndDate,"000000");
	}
	ilActualPos += 8;

	if (GetStfUrno(clStfUrno,clPeno) != RC_SUCCESS)
	{
		dbg(TRACE,"URNO for PENO <%s> in Record type 04 not found",clPeno);
		sprintf(cgErrorString,"URNO for PENO <%s> not found",clPeno);
		LogError(cgInputFilePath,cgErrorString,TRUE);
		return(RC_FAIL);
	}

	dbg(TRACE,"URNO <%s> for PENO <%s> in Record type 04 found",clStfUrno,clPeno);
	if (strstr(pcgRegiDeleted,clPeno) == NULL)
	{
		/* first shift indicator for this employee, delete old SRETAB entries first */
		GetServerTimeStamp("LOC", 1,0,clToday);
		clToday[8] = '\0';

		/*sprintf(clSelection,"WHERE SURN ='%s' AND (VPTO >= '%s' OR VPTO = ' ')", 
				clStfUrno,clToday);*/
		/* MCU 20031023 PRF 280 now delete all SRE records for this employee */
		sprintf(clSelection,"WHERE SURN ='%s'",clStfUrno);
		ilRc = DoDelete("SRETAB",clSelection);
		sprintf(clTmpString,"%s,",clPeno);
		igRegiDeletedUsed += strlen(clTmpString)+1;
		if (igRegiDeletedUsed >= igRegiDeletedLength)
		{ 
			igRegiDeletedLength += 1000;
			igRegiDeletedUsed += 50;
			pcgRegiDeleted = realloc(pcgRegiDeleted,igRegiDeletedLength);
		}
		strcat(pcgRegiDeleted,clTmpString);
	}

	switch(clRegi[0])
	{
	case 'I' : *clRegi = '0';
		break;
	case 'R' : *clRegi = '1';
		break;
	case 'S' : *clRegi = '2';
		break;
	default: 
		dbg(TRACE,"%05d:EmpMasterImport: invalid shift indicator <%s>",__LINE__,
				clRegi); MoveToErrorFile(cgLineBuf); 
		sprintf(cgErrorString,"%05d:EmpMasterImport: invalid shift indicator <%s>",
				igLineNo,clRegi);
		LogError(cgInputFilePath,cgErrorString,TRUE);
		ilRc = RC_FAIL;
	}
	sprintf(cgDataArea,"%s,%s,%s,%s",clRegi,clStfUrno,clStartDate,clEndDate);
	ilRc = DoInsert("SRETAB","REGI,SURN,VPFR,VPTO",":VREGI,:VSURN,:VVPFR,:VVPTO",
				cgDataArea,TRUE);
	if (ilRc != RC_SUCCESS)
	{
		MoveToErrorFile(cgLineBuf);
	}
	return ilRc;
}

static int EmpAdressData(char *pcpBuf)
{
	int ilRc;
	int ilActualPos = 0;
	BOOL blUpdateDone = FALSE;
	char *pclDataArea;
	char clTmpString[512];
	char clSelection[512];
	char *pclBuf;

	char clVafr[50];
	char clVato[50];

	char clPeno[32];
	char clStfUrno[32];
	char clUrno[32];

	pclDataArea = cgDataArea;
	pclBuf = pcpBuf;

	/* Employee Number */
	StringMid(clTmpString,pclBuf,ilActualPos,8,CLIENTTODB);
	ilActualPos += 8;
	strcpy(clPeno,clTmpString);
	pclDataArea = ItemPut(pclDataArea,clTmpString,',');
	/* Effective Start (VAFR)*/
	StringMid(clTmpString,pclBuf,ilActualPos,8,CLIENTTODB);
	ilActualPos += 8;
	strcat(clTmpString,"000000");
	if(CheckDateFormat(clTmpString) != RC_SUCCESS)
	{
			sprintf(cgErrorString,"invalid date format in employee adress data import "
				" VAFR <%s>",clTmpString);
			LogError(cgInputFilePath,cgErrorString,TRUE);
			return(RC_FAIL);
	}
	strcpy(clVafr,clTmpString);
	pclDataArea = ItemPut(pclDataArea,clTmpString,',');
	/* Effective End (VATO)*/
	StringMid(clTmpString,pclBuf,ilActualPos,8,CLIENTTODB);
	ilActualPos += 8;
	strcat(clTmpString,"000000");
	if(CheckDateFormat(clTmpString) != RC_SUCCESS)
	{
			sprintf(cgErrorString,"invalid date format in employee adress data import "
				" VATO <%s>",clTmpString);
			LogError(cgInputFilePath,cgErrorString,TRUE);
			return(RC_FAIL);
	}
	strcpy(clVato,clTmpString);
	pclDataArea = ItemPut(pclDataArea,clTmpString,',');
	/* Street (STRA) */
	StringMid(clTmpString,pclBuf,ilActualPos,60,CLIENTTODB);
	ilActualPos += 60;
	pclDataArea = ItemPut(pclDataArea,clTmpString,',');
	/* Additional Information (ADRC) */
	StringMid(clTmpString,pclBuf,ilActualPos,40,CLIENTTODB);
	ilActualPos += 40;
	pclDataArea = ItemPut(pclDataArea,clTmpString,',');
	/* ZIP Code (ZIPA) */
	StringMid(clTmpString,pclBuf,ilActualPos,10,CLIENTTODB);
	ilActualPos += 10;
	pclDataArea = ItemPut(pclDataArea,clTmpString,',');
	/* City (CITY) */
	StringMid(clTmpString,pclBuf,ilActualPos,40,CLIENTTODB);
	ilActualPos += 40;
	pclDataArea = ItemPut(pclDataArea,clTmpString,',');
	/* Country (LANA) */
	StringMid(clTmpString,pclBuf,ilActualPos,3,CLIENTTODB);
	ilActualPos += 3;
	pclDataArea = ItemPut(pclDataArea,clTmpString,'\0');

	sprintf(clSelection,"WHERE PENO='%s' AND HOPO='%s'",clPeno,cgHopo);
	*clStfUrno = '\0';
	if ((ilRc = DoSingleSelect("STFTAB",clSelection,"URNO",clStfUrno)) == RC_SUCCESS)
	{
		dbg(DEBUG,"DoSingleSelect successfull URNO=<%s>",clStfUrno);
		/*sprintf(clSelection,"WHERE STFU ='%s' AND ((VATO > '%s' OR VATO = ' ') AND"
			" VAFR >= '%s')",clStfUrno,clVato,clVafr);*/
		sprintf(clSelection,"WHERE STFU ='%s' AND (VATO > '%s' OR VATO = ' ') ",
			clStfUrno,clVafr);
		if ((ilRc = DoSingleSelect("ADRTAB",clSelection,"URNO",clUrno)) == RC_SUCCESS)
		{
			/* set in all old ADRTAB record VATO to 'yesterday' */
			char clTimeStamp[24];

			if(*clVafr == ' ')
			{
				GetServerTimeStamp("LOC", 1,0,clTimeStamp);
			}
			else
			{
				strcpy(clTimeStamp,clVafr);
			}
			AddSecondsToCEDATime(clTimeStamp,(time_t)-86400,1);
			clTimeStamp[8] = '\0';
			strcat(clTimeStamp,"000000");
			dbg(DEBUG,"EmpAdressData:DoSingleSelect successfull URNO=<%s>",clUrno);
			ilRc = DoUpdate("ADRTAB",clSelection,"VATO=:VVATO","VATO",clUrno,clTimeStamp,TRUE);
		}
		/* check if one of the ADRTAB records match the new one */
		sprintf(clSelection,"WHERE STFU ='%s' AND VAFR = '%s'",
					clStfUrno,clVafr);
		dbg(DEBUG,"Searching for ADR record: <%s>",clSelection);
		if ((ilRc = DoSingleSelect("ADRTAB",clSelection,"URNO",clUrno)) == RC_SUCCESS)
		{
			sprintf(clSelection,"WHERE URNO = '%s'",clUrno);
			ilRc = DoUpdate("ADRTAB",clSelection,ADR_UPDATE_FIELDS,ADR_FIELDS,clUrno,cgDataArea,TRUE);
			blUpdateDone = TRUE;
			IncrementCount(ilRc);
		}
		
		if (! blUpdateDone )
		{
			sprintf(clTmpString,",%s",clStfUrno);
			pclDataArea = ItemPut(pclDataArea,clTmpString,'\0');
			ilRc = DoInsert("ADRTAB",ADR_INSERT_FIELDS,ADR_INSERT_HOST,cgDataArea,TRUE);
			IncrementCount(ilRc);
		}
	}
	else
	{
		dbg(TRACE,"EmpAdressData: Employee <%s> not found in STFTAB",clPeno);
		IncrementCount(RC_FAIL);
	}
	return ilRc;
}

static int CheckContractCode(char *pcpScoCode)
{
	int ilRc = RC_SUCCESS;
	char clKey[124];
	long llRow = ARR_FIRST;

	strcpy(clKey,pcpScoCode);

	ilRc = CEDAArrayFindRowPointer(&rgCotArray.rrArrayHandle,rgCotArray.crArrayName,
	&rgCotArray.rrIdx02Handle,rgCotArray.crIdx02Name,clKey,
		&llRow,(void **)&rgCotArray.pcrArrayRowBuf);

	dbg(DEBUG,"CheckContractCode: Key <%s> RC %d Code <%s>",pcpScoCode,ilRc,
				FIELDVAL(rgCotArray,rgCotArray.pcrArrayRowBuf,igCotCTRC));

	return ilRc;
}

static int CheckOrgUnit(char *pcpCotDpt1)
{
	int ilRc = RC_SUCCESS;
	char clKey[124];
	long llRow = ARR_FIRST;

	strcpy(clKey,pcpCotDpt1);

	ilRc = CEDAArrayFindKey(&rgOrgArray.rrArrayHandle,rgOrgArray.crArrayName,
	&rgOrgArray.rrIdx02Handle,rgOrgArray.crIdx02Name,
		&llRow,rgOrgArray.lrIdx02RowLen,clKey);

	return ilRc;
}

static int SetLeavingDate(char *pcpPeno,char *pcpDodm)
{
	int ilRc = RC_SUCCESS;
	char clSecSelection[256];
	char clSelection[256];
	char clLeavingDate[256];
	char clSecUrno[24];
	char clCalUrno[24];
	char clTimeStamp[24];
	char clStat[24];

	sprintf(clSecSelection,"WHERE USID='%s'",pcpPeno);
	if (DoSingleSelect("SECTAB",clSecSelection,"URNO,STAT",cgDataArea) == RC_SUCCESS)
	{
		GetDataItem(clSecUrno,cgDataArea,1,',',"","\0\0");
		GetDataItem(clStat,cgDataArea,2,',',"","\0\0");
		sprintf(clSelection,"WHERE FSEC = %s",clSecUrno);
		if (DoSingleSelect("CALTAB",clSelection,"URNO",cgDataArea) == RC_SUCCESS)
		{
			strcpy(clCalUrno,cgDataArea);
			sprintf(clSelection,"WHERE URNO = %s",clCalUrno);
			ilRc = DoUpdate3("CALTAB",clSelection,"VATO=:VVATO","VATO",
						clCalUrno,pcpDodm,TRUE);
			if (ilRc == RC_SUCCESS)
			{
				GetServerTimeStamp("LOC", 1,0,clTimeStamp);
				if (strncmp(clTimeStamp,pcpDodm,8) >= 0)
				{
					strcpy(clStat,"0");
				}
				sprintf(clLeavingDate,"Leaving date set to %c%c/%c%c/%c%c%c%c by SAPHDL,%s",
					pcpDodm[4],	pcpDodm[5],
					pcpDodm[6],	pcpDodm[7],
					pcpDodm[0],	pcpDodm[1],pcpDodm[2],pcpDodm[3],clStat);
				DoUpdate3("SECTAB",clSecSelection,"REMA=:VREMA,STAT=:VSTAT","REMA,STAT",
					clSecUrno,clLeavingDate,TRUE);
			}
		}
	}
	else
	{
		dbg(DEBUG,"Select urno from SECTAB selection <%s>",clSelection);
	}

	return ilRc;
}

static int EmpMasterBase(char *pcpBuf)
{
	int ilRc = RC_SUCCESS;
	int ilRcContract;
	int ilActualPos = 0;
	char *pclDataArea;
	char clTmpString[1024];
	char clSelection[512];
	char *pclBuf;

	char clScoKost[50];
	char clSreRegi[50];
	char clSreVpfr[50];
	char clScoCode[50];
	char clScoVpfr[50];
	char clSorCode[50];
	char clSorVpfr[50];
	char clDobk[24];
	char clDodm[24];
	char clDoem[24];
	char clNoKPhone[50];
	char clNoKAddress[50];
	char clNoKAdditionalAddress[50];
	char clNoKLastName[50];
	char clNoKFirstName[50];
	char clNoKRelationShip[50];
	char clTimeStamp[24];
	char clOldDoem[24] = "";
	char clOldDobk[24] = "";
	char clOldDodm[24] = "";
	char clOldUrno[24] = "";
	static char clDataArea[4096];

	char clPeno[32];
	char clStfUrno[32];

	char clRema[1024];
	char *pclRema;

  memset(clRema,0,sizeof(clRema));
  memset(cgDataArea,0,sizeof(cgDataArea));
  memset(clDataArea,0,sizeof(clDataArea));
	pclDataArea = clDataArea;

	pclBuf = pcpBuf;

	/* Employee Number */
	StringMid(clTmpString,pclBuf,ilActualPos,8,CLIENTTODB);
	ilActualPos += 8;
	strcpy(clPeno,clTmpString);


	sprintf(clSelection,"WHERE PENO='%s'",clPeno);

	if (DoSingleSelect("STFTAB",clSelection,"URNO,DOEM,DODM,DOBK",cgDataArea) == RC_SUCCESS)
	{
		GetDataItem(clOldUrno,cgDataArea,1,',',"","\0\0");
		GetDataItem(clOldDoem,cgDataArea,2,',',"","\0\0");
		GetDataItem(clOldDodm,cgDataArea,3,',',"","\0\0");
		GetDataItem(clOldDobk,cgDataArea,4,',',"","\0\0");
	}


	pclDataArea = ItemPut(pclDataArea,clTmpString,',');
	/* First Name */
	StringMid(clTmpString,pclBuf,ilActualPos,40,CLIENTTODB);
	ilActualPos += 40;
	pclDataArea = ItemPut(pclDataArea,clTmpString,',');
	/* Last Name */
	StringMid(clTmpString,pclBuf,ilActualPos,40,CLIENTTODB);
	ilActualPos += 40;
	pclDataArea = ItemPut(pclDataArea,clTmpString,',');
	/* Nick Name (in SHNM) */
	StringMid(clTmpString,pclBuf,ilActualPos,40,CLIENTTODB);
	ilActualPos += 40;
	pclDataArea = ItemPut(pclDataArea,clTmpString,',');
	/* Birthday */
	StringMid(clTmpString,pclBuf,ilActualPos,8,CLIENTTODB);
	ilActualPos += 8;
	pclDataArea = ItemPut(pclDataArea,clTmpString,',');
	/* Gender */
	StringMid(clTmpString,pclBuf,ilActualPos,1,CLIENTTODB);
	ilActualPos += 1;
	pclDataArea = ItemPut(pclDataArea,clTmpString,',');
	/* Nationality */
	StringMid(clTmpString,pclBuf,ilActualPos,3,CLIENTTODB);
	ilActualPos += 3;
	pclDataArea = ItemPut(pclDataArea,clTmpString,',');
	/* Re-Entrance Date */
	StringMid(clDobk,pclBuf,ilActualPos,8,CLIENTTODB);
	ilActualPos += 8;
	if (*clDobk == ' ')
	{
		clDobk[1] = '\0';
	}
	else
	{
		strcat(clDobk,"000000");
	}
	/* Entrance Date */
	StringMid(clDoem,pclBuf,ilActualPos,8,CLIENTTODB);
	ilActualPos += 8;
	if (*clDoem == ' ')
	{
		clDoem[1] = '\0';
	}
	else
	{
		strcat(clDoem,"000000");
	}
	/* Leaving Date */
	StringMid(clDodm,pclBuf,ilActualPos,8,CLIENTTODB);
	ilActualPos += 8;
	if (*clDodm == ' ')
	{
		clDodm[1] = '\0';
	}
	else
	{
		strcat(clDodm,"000000");
	}

	dbg(DEBUG,"Got from SAP: DOBK %s DODM %s DOEM %s",
							clDobk,clDodm,clDoem);
	if ((*clDobk != '\0' && (*clDobk != ' ')) && (strcmp(clDobk,clDodm) < 0))
	{
			sprintf(cgErrorString,"%05d:EmpMasterImport: reentrance date (%s) < leaving date (%s> ",igLineNo,clDobk,clDodm);
			LogError(cgInputFilePath,cgErrorString,TRUE);
			ilRc = RC_FAIL;
	}
	if (strcmp(clDodm,clOldDodm) != 0 && (*clDodm != '\0' && (*clDodm != ' ')))
	{
		/* leaving date has changed. set in SECTAB (CALTAB) also */
		SetLeavingDate(clPeno,clDodm);
	}
	if (ilRc == RC_SUCCESS && (*clDobk != '\0' && *clDobk != ' '  )&& (*clOldUrno != '\0'))
	{
		/* got reentrance get and record already exist */
		if ((strcmp(clDobk,clOldDobk) != 0) || (strcmp(clDoem,clOldDoem) != 0)
			|| (strcmp(clDoem,clOldDoem) != 0)) 
			{
				char clTmpDate[24];

				strcpy(clTmpDate,clOldDodm);
				AddSecondsToCEDATime(clTmpDate,30*86400,1);
				if (strcmp(clDobk,clTmpDate) >= 0)
				{
					dbg(DEBUG,"Reentrance date is >= (DODM+30days) DOBK %s DODM %s",
							clDobk,clOldDodm);
					strcpy(clDodm," ");
					strcpy(clDoem,clDobk);
					strcpy(clDobk," ");
				}
				else
				{
					dbg(DEBUG,"Reentrance date is < (DODM+30days) DOBK %s DODM %s",
							clDobk,clOldDodm);
					strcpy(clDodm,clOldDodm);
					strcpy(clDoem,clOldDoem);
					strcpy(clDobk,clOldDobk);
				}
			}
	}
	else if (ilRc == RC_SUCCESS && (*clDobk != '\0' && *clDobk != ' '   )&& (*clOldUrno == '\0'))
	{
		/* got reentrance get for new record */
		strcpy(clDodm," ");
		strcpy(clDoem,clDobk);
		strcpy(clDobk," ");
	}

	pclDataArea = ItemPut(pclDataArea,clDobk,',');
	pclDataArea = ItemPut(pclDataArea,clDoem,',');
	pclDataArea = ItemPut(pclDataArea,clDodm,',');
	/* Cost Center */
	StringMid(clScoKost,pclBuf,ilActualPos,10,CLIENTTODB);
	ilActualPos += 10;
	/* Indicator Shift Worker */
	StringMid(clSreRegi,pclBuf,ilActualPos,1,CLIENTTODB);
	ilActualPos += 1;
	if (*cgNewRegi != 'Y')
	{
		switch(clSreRegi[0])
		{
		case 'I' : *clSreRegi = '0';
			break;
		case 'R' : *clSreRegi = '1';
			break;
		case 'S' : *clSreRegi = '2';
			break;
		default: 
			dbg(TRACE,"%05d:EmpMasterImport: invalid shift indicator <%s>",__LINE__,
					clSreRegi); MoveToErrorFile(cgLineBuf); 
			sprintf(cgErrorString,"%05d:EmpMasterImport: invalid shift indicator <%s>",
					igLineNo,clSreRegi);
			LogError(cgInputFilePath,cgErrorString,TRUE);
			ilRc = RC_FAIL;
		}
	}

	StringMid(clSreVpfr,pclBuf,ilActualPos,8,CLIENTTODB);
	ilActualPos += 8;
	strcat(clSreVpfr,"000000");
	/* Contract Code */
	StringMid(clScoCode,pclBuf,ilActualPos,2,CLIENTTODB);
	ilActualPos += 2;
	StringMid(clScoVpfr,pclBuf,ilActualPos,8,CLIENTTODB);
	ilActualPos += 8;
	strcat(clScoVpfr,"000000");
	/* Organization Unit */
	StringMid(clSorCode,pclBuf,ilActualPos,8,CLIENTTODB);
	ilActualPos += 8;
	StringMid(clSorVpfr,pclBuf,ilActualPos,8,CLIENTTODB);
	ilActualPos += 8;
	strcat(clSorVpfr,"000000");
	StringMid(clNoKPhone,pclBuf,ilActualPos,14,CLIENTTODB);
	ilActualPos += 14;
	StringMid(clNoKAddress,pclBuf,ilActualPos,30,CLIENTTODB);
	ilActualPos += 30;
	StringMid(clNoKAdditionalAddress,pclBuf,ilActualPos,30,CLIENTTODB);
	ilActualPos += 30;
	StringMid(clNoKLastName,pclBuf,ilActualPos,40,CLIENTTODB);
	ilActualPos += 40;
	StringMid(clNoKFirstName,pclBuf,ilActualPos,40,CLIENTTODB);
	ilActualPos += 40;
	StringMid(clNoKRelationShip,pclBuf,ilActualPos,4,CLIENTTODB);
	ilActualPos += 4;


	StringTrimRight(clNoKPhone);
	StringTrimRight(clNoKAddress);
	StringTrimRight(clNoKRelationShip);
	StringTrimRight(clNoKFirstName);
	StringTrimRight(clNoKLastName);

	*clRema = '\0';
	pclRema = clRema;


	StringTrimRight(clNoKPhone);
	if(strcmp(clNoKPhone," ") != 0)
	{
		pclRema = ItemPut(pclRema,clNoKPhone,',');
	}
	StringTrimRight(clNoKAddress);
	if(strcmp(clNoKAddress," ") != 0)
	{
		pclRema = ItemPut(pclRema,clNoKAddress,',');
	}
	StringTrimRight(clNoKRelationShip);
	if(strcmp(clNoKRelationShip," ") != 0)
	{
		pclRema = ItemPut(pclRema,clNoKRelationShip,',');
	}
	StringTrimRight(clNoKFirstName);
	if(strcmp(clNoKFirstName," ") != 0)
	{
		pclRema = ItemPut(pclRema,clNoKFirstName,',');
	}
	StringTrimRight(clNoKLastName);
	if(strcmp(clNoKLastName," ") != 0)
	{
		pclRema = ItemPut(pclRema,clNoKLastName,'\0');
	}

	if(strlen(clRema) > 60)
	{
		clRema[60] = '\0';
	}


	ConvertClientStringToDb(clRema);

	dbg(TRACE,"REMA: <%s>",clRema);
	pclDataArea = ItemPut(pclDataArea,clRema,',');
	pclDataArea = ItemPut(pclDataArea,"1",',');
	if (*cgSetTohr == 'Y')
	{
		pclDataArea = ItemPut(pclDataArea,cgTohr,',');
	}

	dbg(DEBUG,"T1 <%s>",clDataArea);

	dbg(DEBUG,"KOST=<%s>,REGI=<%s>,SREVPFR=<%s>,SCOCODE=<%s>,"
	"SCOVPFR=<%s>,SORCODE=<%s>,SORVPFR=<%s>",clScoKost,clSreRegi, 
		clSreVpfr,clScoCode,clScoVpfr,clSorCode,clSorVpfr,clNoKPhone,
			clNoKAddress,clNoKAdditionalAddress,clNoKLastName,
				clNoKFirstName,clNoKRelationShip);

	sprintf(clSelection,"WHERE PENO='%s' AND HOPO='%s'",clPeno,cgHopo);
	*clStfUrno = '\0';
		
	if (((ilRcContract=CheckContractCode(clScoCode)) == RC_SUCCESS) && 
				ilRc == RC_SUCCESS)
	{
		if ((ilRc = DoSingleSelect("STFTAB",clSelection,"URNO",clStfUrno)) == RC_SUCCESS)
		{
			dbg(DEBUG,"EmpMasterBase:DoSingleSelect successfull URNO=<%s>",clStfUrno);
			ilRc = DoUpdate("STFTAB",clSelection,cgStfUpdateHost,cgStfUpdateFields,
						clStfUrno,clDataArea,TRUE);
			if (ilRc == RC_SUCCESS)
			{
				/*AddToUrnoList(clStfUrno,pcgStfuList,lgStfuListLength,&lgStfuListActualLength);*/
			}
			IncrementCount(ilRc);
		}
		else if (ilRc == NOTFOUND)
		{
			ilRc = DoInsert("STFTAB",cgStfInsertFields,cgStfInsertHost,clDataArea,TRUE);
			if (ilRc == RC_SUCCESS)
			{
				/*AddToUrnoList(clStfUrno,pcgStfuList,lgStfuListLength,&lgStfuListActualLength);*/
			}
			IncrementCount(ilRc);
			strcpy(clStfUrno,clDataArea);
		}
		else
		{
			/* there must be a database error */
			get_ora_err (ilRc, cgErrMsg);
			dbg(TRACE,"EmpMasterBase Select from STFTAB <%s> failed RC=%d",clSelection,ilRc);
			dbg(TRACE,"%s",cgErrMsg);
			IncrementCount(RC_FAIL);
		}
	}
	if (ilRc == RC_SUCCESS)
	{
		if (ilRcContract == RC_SUCCESS)
		{

			/*sprintf(clSelection,"WHERE CODE = '%s' and SURN = '%s'",clScoCode,clStfUrno);*/
			strcpy(clTimeStamp,clScoVpfr);
			sprintf(clSelection,"WHERE SURN = '%s' AND VPFR <= '%s' "
				" AND (VPTO >= '%s' OR VPTO = ' ')",clStfUrno,clScoVpfr,clTimeStamp);
			if (DoSingleSelect("SCOTAB",clSelection,"URNO,VPFR,VPTO,CODE,KOST",clDataArea) == NOTFOUND)
			{
			/*SCO_INSERT_FIELDS "KOST,CODE,SURN,VPFR"*/
				sprintf(cgDataArea,"%s,%s,%s,%s",clScoKost,clScoCode,
						clStfUrno,clScoVpfr);
				ilRc = DoInsert("SCOTAB",SCO_INSERT_FIELDS,SCO_INSERT_HOST,cgDataArea,TRUE);
				if (ilRc != RC_SUCCESS)
				{
					MoveToErrorFile(cgLineBuf);
				}
			}
			else
			{
				char clUrno[24];
				char clVpfr[24];
				char clVpto[24];
				char clCode[24];
				char clKost[24];
		

				GetDataItem(clUrno,clDataArea,1,',',"","\0\0");
				GetDataItem(clVpfr,clDataArea,2,',',"","\0\0");
				GetDataItem(clVpto,clDataArea,3,',',"","\0\0");
				GetDataItem(clCode,clDataArea,4,',',"","\0\0");
				GetDataItem(clKost,clDataArea,5,',',"","\0\0");
				dbg(DEBUG,"from DB:URNO <%s>,VPFR <%s>  CODE <%s> KOST <%s>",
						clUrno,clVpfr,clCode,clKost);

				dbg(DEBUG,"from File:        VPFR <%s>  CODE <%s> KOST <%s>",
						clScoVpfr,clScoCode,clScoKost);
				if ((strncmp(clCode,clScoCode,2) != 0) ||
					  (strncmp(clKost,clScoKost,10) != 0) ||
					  (strcmp(clVpfr,clScoVpfr) != 0))
				{

						strcpy(clTimeStamp,clScoVpfr);
						AddSecondsToCEDATime(clTimeStamp,-86400,1);
						clTimeStamp[8] = '\0';
						strcat(clTimeStamp,"000000");

						/* update VPTO in old SCO record */
						sprintf(clSelection,"WHERE URNO = '%s'",clUrno);
						DoUpdate("SCOTAB",clSelection,"VPTO=:VVPTO","VPTO",clUrno,clTimeStamp,TRUE);
						sprintf(cgDataArea,"%s,%s,%s,%s",clScoKost,clScoCode,
								clStfUrno,clScoVpfr);
						ilRc = DoInsert("SCOTAB",SCO_INSERT_FIELDS,SCO_INSERT_HOST,cgDataArea,TRUE);
				}
#if 0
				/*SCO_UPDATE_FIELDS "KOST=:VKOST,CODE=:VCODE,VPFR=:VVPFR"*/
				sprintf(cgDataArea,"%s,%s,%s",clScoKost,clScoCode,clScoVpfr);
				/*dbg(TRACE,"SCO_UPDATE_FIELDS <%s>",SCO_UPDATE_FIELDS);*/
				ilRc = DoUpdate("SCOTAB",clSelection,SCO_UPDATE_FIELDS,cgDataArea);
				if (ilRc != RC_SUCCESS)
				{
					MoveToErrorFile(cgLineBuf);
				}
#endif
			}
		}
	}
	if (ilRcContract != RC_SUCCESS)
	{
		dbg(TRACE,"%05d:EmpMasterImport: invalid contract code <%s>",__LINE__,clScoCode);
		MoveToErrorFile(cgLineBuf);

		sprintf(cgErrorString,"%05d:EmpMasterImport: invalid contract code <%s>",
				igLineNo,clScoCode);
		LogError(cgInputFilePath,cgErrorString,TRUE);
	}


	if ((ilRc == RC_SUCCESS) && (*cgNewRegi != 'Y'))
	{
			strcpy(clTimeStamp,clSreVpfr);
			GetServerTimeStamp("LOC", 1,0,clTimeStamp);
			sprintf(clSelection,"WHERE SURN = '%s' AND VPFR <= '%s' "
				" AND (VPTO >= '%s' OR VPTO = ' ')",clStfUrno,clSreVpfr,clTimeStamp);
	
		if (DoSingleSelect("SRETAB",clSelection,"URNO,VPFR,REGI",cgDataArea) == NOTFOUND)
		{
			sprintf(cgDataArea,"%s,%s,%s",clSreRegi,clStfUrno,clSreVpfr);
			ilRc = DoInsert("SRETAB","REGI,SURN,VPFR",":VREGI,:VSURN,:VVPFR",cgDataArea,TRUE);
			if (ilRc != RC_SUCCESS)
			{
				MoveToErrorFile(cgLineBuf);
			}
		}
		else
		{
				char clUrno[24];
				char clVpfr[24];
				char clVpto[24];
				char clRegi[24];
		

				GetDataItem(clUrno,cgDataArea,1,',',"","\0\0");
				GetDataItem(clVpfr,cgDataArea,2,',',"","\0\0");
				GetDataItem(clRegi,cgDataArea,3,',',"","\0\0");
				dbg(DEBUG,"from DB:URNO <%s>,VPFR <%s>  REGI <%s> ",
						clUrno,clVpfr,clRegi);

				dbg(DEBUG,"from File:        VPFR <%s>  REGI <%s> ",
						clSreVpfr,clSreRegi);
				if ((strncmp(clRegi,clSreRegi,1) != 0) ||
					  (strcmp(clVpfr,clSreVpfr) != 0))
				{

						strcpy(clTimeStamp,clSreVpfr);
						AddSecondsToCEDATime(clTimeStamp,-86400,1);
						clTimeStamp[8] = '\0';
						strcat(clTimeStamp,"235959");

						/* update VPTO in old SCO record */
						sprintf(clSelection,"WHERE URNO = '%s'",clUrno);
						DoUpdate("SRETAB",clSelection,"VPTO=:VVPTO","VPTO",clUrno,clTimeStamp,TRUE);
						sprintf(cgDataArea,"%s,%s,%s",clSreRegi,clStfUrno,clSreVpfr);
						ilRc = DoInsert("SRETAB","REGI,SURN,VPFR",":VREGI,:VSURN,:VVPFR",cgDataArea,TRUE);
						if (ilRc != RC_SUCCESS)
						{
							MoveToErrorFile(cgLineBuf);
						}
				}
		}
		ilRc = RC_SUCCESS;
	}
	return ilRc;
}




static int EmployeeMasterImport()
{
	int ilRc = RC_SUCCESS;
	char clSequence[12];
	char *pclBaseName;
	long llSequence,llOldSequence;
	long llLinesInFile;
	long llLineCount;
	FILE *pflInFile = NULL;
	short slFunc = START;
	BOOL blHeaderFound = FALSE;
	BOOL blHeaderAlerted = FALSE;
	BOOL blTrailerFound = FALSE;
	char clFileMonth[12];
	int  ilFilesFound = 0;

	while(GetFileNames(slFunc,cgEmpFileId,cgFileNew,cgInputFilePath,FALSE) == RC_SUCCESS)
	{
		*cgLineBuf = '\0';
		slFunc = NEXT;
		dbg(TRACE,"Employee Master File <%s> will be processed",cgInputFilePath);
		/* first check sequence number */
		pclBaseName = strrchr(cgInputFilePath,'/') + 1;
		pclBaseName = pclBaseName == NULL ? cgInputFilePath : pclBaseName;
		strcpy(cgInputFileName,pclBaseName);
		StringMid(clSequence,pclBaseName,10,6,CLIENTTODB);
		StringMid(clFileMonth,pclBaseName,8,2,CLIENTTODB);
		dbg(DEBUG,"Sequence is <%s> from <%s>",clSequence,pclBaseName);
		llSequence = atol(clSequence);
		llOldSequence = GetSequence("SAPEMP",clFileMonth);
		if (llSequence <= llOldSequence)
		{
			dbg(TRACE,"File already processed sequence %d sequence from DB %d",
					llSequence,llOldSequence);
			sprintf(cgErrorString,"File <%s> already processed sequence %d expected %d",
					pclBaseName,llSequence,llOldSequence+1);
			LogError(cgInputFilePath,cgErrorString,TRUE);
		}
		else
		{
			if (llSequence > (llOldSequence+1))
			{
				dbg(TRACE,"File out of sequence %d sequence from DB %d",
						llSequence,llOldSequence);
				sprintf(cgErrorString,"File out of sequence %d sequence from "
				" DB %d ",
						llSequence,llOldSequence);
				LogError(cgInputFilePath,cgErrorString,TRUE);
				continue;
			}
			dbg(DEBUG,"Import File <%s> will be opened",cgInputFilePath);
			ilFilesFound++;
			pflInFile = fopen(cgInputFilePath,"rt");
			if (pflInFile != NULL)
			{
				lgSuccessCount = 0;
				lgErrorCount = 0;
				dbg(DEBUG,"Import File <%s> opened",cgInputFilePath);
				/* 1.pass import only Employee Master Base */
				igLineNo = 0;
  			memset(cgLineBuf,0,sizeof(cgLineBuf));
				llLinesInFile = 0;
				while(fgets(cgLineBuf,sizeof(cgLineBuf),pflInFile) != NULL)
				{
					if(strncmp(cgLineBuf,"00",2) == 0)
					{
						/* header record */
						char clLineCount[24];
						strncpy(clLineCount,&cgLineBuf[40],10);
						clLineCount[10] = '\0';
						llLineCount = atol(clLineCount);
						dbg(DEBUG,"Employee Master Import, %d/<%s> records will be processed",
							llLineCount,clLineCount);
						blHeaderFound = TRUE;
					}
					if((strncmp(cgLineBuf,"99",2) == 0) && (blHeaderFound))
					{
						/* trailer record */
						blTrailerFound = TRUE;
					}
					StringTrimRightLineEnd(cgLineBuf);
					igLineNo++;
					dbg(DEBUG,"Buf: <%s>",cgLineBuf);
					if(strncmp(cgLineBuf,"01",2) == 0)
					{
						llLinesInFile++;
						/***********
						if (!blHeaderFound && (!blHeaderAlerted))
						{
							blHeaderAlerted = TRUE;
							dbg(TRACE,"No header found in File or header ist not first line");
							sprintf(cgErrorString,"No header found in File or header ist not "
							" first line in File <%s>",cgInputFileName);
							LogError(cgInputFilePath,cgErrorString,TRUE);
						}
						*****************/
						if (strlen(cgLineBuf) == 369)
						{
							EmpMasterBase((char *)cgLineBuf+2); /* skip record id */
						}
						else
						{
							StringTrimRight(cgLineBuf);
							ConvertClientStringToDb(cgLineBuf);
							sprintf(cgErrorString,"EmpMasterBase: Invalid line length %d "
							" line no: %d ",
										strlen(cgLineBuf),igLineNo);
							LogError(cgInputFilePath,cgErrorString,TRUE);
							IncrementCount(RC_FAIL);
						}
					}
				}
				rewind(pflInFile);
				igLineNo = 0;
  			memset(cgLineBuf,0,sizeof(cgLineBuf));
				while(fgets(cgLineBuf,sizeof(cgLineBuf),pflInFile) != NULL)
				{
					StringTrimRightLineEnd(cgLineBuf);
					igLineNo++;
					if(strncmp(cgLineBuf,"02",2) == 0)
					{
						llLinesInFile++;
						if (strlen(cgLineBuf) == 179)
						{
							EmpAdressData(cgLineBuf + 2); /* skip record id */
						}
						else
						{
							StringTrimRight(cgLineBuf);
							ConvertClientStringToDb(cgLineBuf);
							sprintf(cgErrorString,"EmpAdressData:Invalid line length %d line no: %d ",
										strlen(cgLineBuf),igLineNo);
							LogError(cgInputFilePath,cgErrorString,TRUE);
							IncrementCount(RC_FAIL);
						}
					}
				}
				rewind(pflInFile);
				igLineNo = 0;
  			memset(cgLineBuf,0,sizeof(cgLineBuf));
				while(fgets(cgLineBuf,sizeof(cgLineBuf),pflInFile) != NULL)
				{
					StringTrimRightLineEnd(cgLineBuf);
					igLineNo++;
					if(strncmp(cgLineBuf,"03",2) == 0)
					{
						llLinesInFile++;
						if (strlen(cgLineBuf) == 60)
						{
							EmpContactDetails(cgLineBuf + 2); /* skip record id */
						}
						else
						{
							StringTrimRight(cgLineBuf);
							ConvertClientStringToDb(cgLineBuf);
							sprintf(cgErrorString,"EmpContactDetails: Invalid line length %d line no: %d ",
										strlen(cgLineBuf),igLineNo);
							LogError(cgInputFilePath,cgErrorString,TRUE);
							IncrementCount(RC_FAIL);
						}
					}
				}
				if (*cgNewRegi == 'Y')
				{
					char clSelection[254];
					long llAction = ARR_FIRST;

					rewind(pflInFile);
					igLineNo = 0;
  				memset(cgLineBuf,0,sizeof(cgLineBuf));
					pcgRegiDeleted = realloc(pcgRegiDeleted,1000+100);
					igRegiDeletedLength = 1000;
					memset(pcgRegiDeleted,0,1100);
					igRegiDeletedUsed = 50;
		
		/** refill the STF-Array to ensure that the new imported STF records
		are available in memory **/
		sprintf(clSelection,"WHERE HOPO = '%s' ",cgHopo);
	CEDAArrayRefill(&rgStfArray.rrArrayHandle,rgStfArray.crArrayName,
			clSelection,NULL,llAction);
					while(fgets(cgLineBuf,sizeof(cgLineBuf),pflInFile) != NULL)
					{
						dbg(DEBUG,"Line: <%s>",cgLineBuf);
						StringTrimRightLineEnd(cgLineBuf);
						igLineNo++;
						if(strncmp(cgLineBuf,"04",2) == 0)
						{
							llLinesInFile++;
							if (strlen(cgLineBuf) == 27)
							{
								EmpRegularityIndicator(cgLineBuf + 2); /* skip record id */
							}
							else
							{
								StringTrimRight(cgLineBuf);
								ConvertClientStringToDb(cgLineBuf);
								sprintf(cgErrorString,"Emp Regularity Indicator: Invalid line length "
									"%d line no: %d ",strlen(cgLineBuf),igLineNo);
								LogError(cgInputFilePath,cgErrorString,TRUE);
								IncrementCount(RC_FAIL);
							}
						}
					}
				}
				rewind(pflInFile);
				igLineNo = 0;
  			memset(cgLineBuf,0,sizeof(cgLineBuf));

				while(fgets(cgLineBuf,sizeof(cgLineBuf),pflInFile) != NULL)
				{
					StringTrimRight(cgLineBuf);
					igLineNo++;

					if((strncmp(cgLineBuf,"01",2) != 0) &&
						 (strncmp(cgLineBuf,"02",2) != 0) &&
						 (strncmp(cgLineBuf,"03",2) != 0) &&
						 (strncmp(cgLineBuf,"04",2) != 0) &&
						 (strncmp(cgLineBuf,"00",2) != 0) &&
						 (strncmp(cgLineBuf,"99",2) != 0) &&
						 (strncmp(cgLineBuf,"*",1) != 0))
					{
						dbg(DEBUG,"Invalid Line: No.%d ",igLineNo);
						sprintf(cgErrorString,"Invalid Line: No.%d ",igLineNo);
						LogError(cgInputFilePath,cgErrorString,TRUE);
						IncrementCount(RC_FAIL);
					}
				}
				fclose(pflInFile);
				if (*cgUpdEmpSequence == 'Y')
				{
					sprintf(cgArcFileName,"%s%s%s",cgFileBase,cgFileArc,pclBaseName);
					dbg(DEBUG,"EmpMasterImport: Import File <%s> will be moved to <%s>",
							cgInputFilePath,cgArcFileName);
					if(rename(cgInputFilePath,cgArcFileName) != RC_SUCCESS)
					{
						dbg(TRACE,"EmpMasterImport: cannot rename file, reason: %d %s",
								errno,strerror(errno));
					}
					PutSequence("SAPEMP",llSequence,clFileMonth);
				}


				if (!blHeaderFound)
				{
						dbg(TRACE,"Employee Master Import: No header found in file <%s>"
						", could not determine line count",cgInputFileName);
						sprintf(cgErrorString,"Employee Master Import: No header found in"
						" file <%s>, could not determine line count",cgInputFileName);
						LogError(cgInputFilePath,cgErrorString,TRUE);
				}

				if (!blTrailerFound)
				{
						dbg(TRACE,"Employee Master Import: No trailer found in file <%s>"
						", %d lines from %d already imported",
							cgInputFileName,llLinesInFile,llLineCount);
						sprintf(cgErrorString,"Employee Master Import: No trailer found in file <%s>"
						", %d lines from %d already imported",
							cgInputFileName,llLinesInFile,llLineCount);
						LogError(cgInputFilePath,cgErrorString,TRUE);
				}
				else
				{
						dbg(TRACE,"Employee Master Import: "
						" %d lines from %d imported",llLinesInFile,llLineCount);
						if (llLinesInFile != llLineCount)
						{
							sprintf(cgErrorString,"Employee Master Import: invalid number of "
								" lines  in file <%s> %ld, should be %ld",
								cgInputFileName,llLinesInFile,llLineCount);
							LogError(cgInputFilePath,cgErrorString,TRUE);
						}
				}

				/***
				sprintf(cgErrorString,"Lines processed with success/error %d/%d",
							lgSuccessCount,lgErrorCount);
				LogError(cgInputFilePath,cgErrorString,FALSE);
				****/
			}
			else
			{
				dbg(TRACE,"EmployeeMasterImport: File <%s> not opened",cgInputFilePath);
			}
		}
	}
	if (ilFilesFound == 0)
	{
		sprintf(cgErrorString,"No employee import file found");
		*cgInputFilePath = '\0';
		LogError(cgInputFilePath,cgErrorString,TRUE);
	}
	return ilRc;
}

static int xx()
{
	return 0;
	}

static int InsertDra(ABSENCE *prpAbsence,char *pcpStfUrno,char *pcpDrrn)
{
	int ilRc = RC_SUCCESS;
	char *pclDataArea;
	char clSelection[244];
	char clStfUrno[24];

	/**************
	sprintf(clSelection,"WHERE STFU='%s' AND ABFR='%s' AND ABTO='%s' AND SDAC='%s'",
			pcpStfUrno,prpAbsence->Start,prpAbsence->End,prpAbsence->Code);
			*************/
			/** check for overlapping absences **/
	/*** overlapping is independent of SDAC **/
	sprintf(clSelection,"WHERE STFU='%s' AND ABFR <='%s' AND ABTO >='%s'",
			pcpStfUrno,prpAbsence->End,prpAbsence->Start);
	/*****
	sprintf(clSelection,"WHERE STFU='%s' AND ABFR <='%s' AND ABTO >='%s' AND SDAC='%s'",
			pcpStfUrno,prpAbsence->End,prpAbsence->Start,prpAbsence->Code);
			***********/
	ilRc = DoSingleSelect("DRATAB",clSelection,"URNO",clStfUrno);
	if(ilRc != RC_SUCCESS)
	{
		dbg(DEBUG,"InsertDra: no old DRA found, insert");

		pclDataArea = cgDataArea;

		pclDataArea = ItemPut(pclDataArea,"0",','); /* BSDU is always '0'*/
		pclDataArea = ItemPut(pclDataArea,prpAbsence->Code,',');
		pclDataArea = ItemPut(pclDataArea,prpAbsence->StartDate,',');
		pclDataArea = ItemPut(pclDataArea,prpAbsence->Start,',');
		pclDataArea = ItemPut(pclDataArea,prpAbsence->End,',');
		pclDataArea = ItemPut(pclDataArea,pcpStfUrno,',');
		pclDataArea = ItemPut(pclDataArea,pcpDrrn,'\0');
		ilRc = DoInsert("DRATAB",DRA_INSERT_FIELDS,DRA_INSERT_HOST,cgDataArea,TRUE);
		if (ilRc == RC_SUCCESS)
		{
			/*AddToUrnoList(pcpStfUrno,pcgStfuList,lgStfuListLength,&lgStfuListActualLength);*/
		}
	}
	else
	{
		dbg(TRACE,"InsertDra: old DRA found for <%s>, do nothing",clSelection);
	}
	return ilRc;
}

/*DRA_UPDATE_FIELDS  "SDAC=:VSDAC,ABFR=:VABFR,ABTO=:VABTO,DRRN=:VDRRN"*/

static int UpdateDra(ABSENCE *prpAbsence,char *pcpStfUrno,char *pcpDrrn)
{
	int ilRc = RC_SUCCESS;
	char clSelection[124];
	char clUrno[24];
	char *pclDataArea;
	long llRow;
	char clKey[24];
	char clSday[24];
	char clSdayM1[24];
	char clUstf[24];
	char clRosterStart[24];
	char clRosterEnd[24];
	pclDataArea = cgDataArea;

	pclDataArea = ItemPut(pclDataArea,prpAbsence->Code,',');
	pclDataArea = ItemPut(pclDataArea,prpAbsence->Start,',');
	pclDataArea = ItemPut(pclDataArea,prpAbsence->End,',');
	pclDataArea = ItemPut(pclDataArea,pcpDrrn,'\0');

/***********
	sprintf(clSelection,"WHERE STFU='%s' AND SDAY='%s' AND DRRN='%s'",
			nsertpcpStfUrno,prpAbsence->StartDate,pcpDrrn);
************/
	/*** first search the DRR for this DRA ***/

	dbg(DEBUG,"read shifts follows");
	strcpy(clSday,prpAbsence->Start);
	strcpy(clSdayM1,prpAbsence->Start);
	AddSecondsToCEDATime(clSdayM1,(time_t)-86400,1);
	clSday[8] = '\0';
	clSdayM1[8] = '\0';

	sprintf(clSelection,"WHERE SDAY in ('%s','%s') AND ROSS='A' AND "
	" STFU = '%s' AND HOPO='%s'",clSdayM1,clSday,pcpStfUrno,cgHopo);
	dbg(DEBUG,"UpdateDra: Select shifts <%s>",clSelection);
	CEDAArrayRefillHint(&rgDrrArray.rrArrayHandle,rgDrrArray.crArrayName,
			clSelection,NULL,ARR_FIRST,cgDrrReloadHintStfu);

	dbg(DEBUG,"read shifts finished");

				ilRc = RC_NOTFOUND;
				llRow = ARR_FIRST;
				strcpy(clKey,pcpStfUrno);
				*clSday = '\0';
				while(CEDAArrayFindRowPointer(&(rgDrrArray.rrArrayHandle),
																			&(rgDrrArray.crArrayName[0]),
																			&(rgDrrArray.rrIdx01Handle),
																			&(rgDrrArray.crIdx01Name[0]),
																			clUstf,&llRow,
																			(void **)&rgDrrArray.pcrArrayRowBuf) == RC_SUCCESS)
					{
						llRow = ARR_NEXT;
						strcpy(clRosterStart,FIELDVAL(rgDrrArray,rgDrrArray.pcrArrayRowBuf,igDrrAVFR));
						strcpy(clRosterEnd,FIELDVAL(rgDrrArray,rgDrrArray.pcrArrayRowBuf,igDrrAVTO));
					
						if (ISBETWEEN(clRosterStart,clRosterEnd,prpAbsence->Start))
						{
								dbg(DEBUG,"found Shift %s from %s to %s for absence beginning %s",
											FIELDVAL(rgDrrArray,rgDrrArray.pcrArrayRowBuf,igDrrURNO),
											clRosterStart,clRosterEnd,prpAbsence->Start);
								strcpy(clSday,FIELDVAL(rgDrrArray,rgDrrArray.pcrArrayRowBuf,igDrrSDAY));
								break;
						}
						else
						{
							 
								dbg(DEBUG,"Shift %s from %s to %s is not valid for absence beginning %s",
											FIELDVAL(rgDrrArray,rgDrrArray.pcrArrayRowBuf,igDrrURNO),
											clRosterStart,clRosterEnd,prpAbsence->Start);
						}
					}
		if (*clSday == '\0')
		{
			dbg(TRACE,"no valid shift found for absence from % to %s",
					prpAbsence->Start,prpAbsence->End);
			strcpy(clSday,prpAbsence->StartDate);
		}

		{
	sprintf(clSelection,"WHERE STFU='%s' AND SDAY='%s' AND SDAC='%s'",
			pcpStfUrno,clSday,prpAbsence->Code);
	if ((ilRc = DoSingleSelect("DRATAB",clSelection,"URNO",clUrno)) == RC_SUCCESS)
	{
		ilRc = DoUpdate("DRATAB",clSelection,DRA_UPDATE_FIELDS,DRA_FIELDS,clUrno,cgDataArea,TRUE);
		/*AddToUrnoList(pcpStfUrno,pcgStfuList,lgStfuListLength,&lgStfuListActualLength);*/
	}
	else if (ilRc == RC_NOTFOUND)
	{
		/*** we found no DRA record so we insert a new one and search DRR record to delete */
		ilRc = DeleteDrr(prpAbsence,pcpStfUrno);
		InsertDra(prpAbsence,pcpStfUrno,pcpDrrn);
	}
	}
	return ilRc;
}

static int DeleteDra(ABSENCE *prpAbsence,char *pcpStfUrno,char *pcpDrrn)
{
	int ilRc = RC_SUCCESS;
	char clSelection[128];

	/*** select exact start and end **/
	/**
	sprintf(clSelection,"WHERE STFU='%s' AND SDAY='%s' AND DRRN='%s'",
			pcpStfUrno,prpAbsence->StartDate,pcpDrrn);
			***/
	sprintf(clSelection,"WHERE STFU='%s' AND SDAY='%s' AND SDAC='%s'"
	" AND ABFR = '%s' AND ABTO = '%s'",
			pcpStfUrno,prpAbsence->StartDate,prpAbsence->Code,
			prpAbsence->Start,prpAbsence->End);
	ilRc = DoDelete("DRATAB",clSelection);

	return ilRc;
}

static int IsAbsenceCode(char *pcpCode,BOOL *bpOffRd)
{
	int ilRc = RC_SUCCESS;
	char clKey[124];
	long llRow = ARR_FIRST;

	*bpOffRd = FALSE;
	strcpy(clKey,pcpCode);

	ilRc = CEDAArrayFindRowPointer(&rgOdaArray.rrArrayHandle,rgOdaArray.crArrayName,
		&rgOdaArray.rrIdx02Handle,rgOdaArray.crIdx02Name,clKey,
		&llRow,(void **)&rgOdaArray.pcrArrayRowBuf);
	if (ilRc >= 0)
	{
		char clOdaFree[3];
		char clOdaType[6];

		strcpy(clOdaFree,PFIELDVAL((&rgOdaArray),rgOdaArray.pcrArrayRowBuf,igOdaFREE));
		strcpy(clOdaType,PFIELDVAL((&rgOdaArray),rgOdaArray.pcrArrayRowBuf,igOdaTYPE));

		if (strstr(clOdaFree,"x") || strstr(clOdaType,"S"))
		{
			*bpOffRd = TRUE;
			dbg(DEBUG,"Absence <%s> but will be exported. FREE <%s> TYPE <%s>",
				pcpCode,clOdaFree,clOdaType);
		}
	}

	dbg(DEBUG,"IsAbsenceCode for <%s> will return %d",pcpCode,ilRc);
	return ilRc;
}

static int CheckAbsenceCode(char *pcpCode,char *pcpAbfr,char *pcpAbto,char *pcpOdaUrno)
{
	int ilRc = RC_SUCCESS;
	char clKey[124];
	long llRow = ARR_FIRST;

	strcpy(clKey,pcpCode);

	ilRc = CEDAArrayFindRowPointer(&rgOdaArray.rrArrayHandle,rgOdaArray.crArrayName,
	&rgOdaArray.rrIdx02Handle,rgOdaArray.crIdx02Name,clKey,
		&llRow,(void **)&rgOdaArray.pcrArrayRowBuf);
	if (ilRc == RC_SUCCESS)
	{
		strcpy(pcpAbfr,PFIELDVAL((&rgOdaArray),rgOdaArray.pcrArrayRowBuf,igOdaABFR));
		strcpy(pcpAbto,PFIELDVAL((&rgOdaArray),rgOdaArray.pcrArrayRowBuf,igOdaABTO));
		strcpy(pcpOdaUrno,PFIELDVAL((&rgOdaArray),rgOdaArray.pcrArrayRowBuf,igOdaURNO));
	  dbg(DEBUG,"CheckAbsenceCode: OdaURNO <%s> %d ABFR <%s> ABTO <%s>",
					pcpOdaUrno,igOdaURNO,pcpAbfr,pcpAbto);
	}
	else
	{
		dbg(TRACE,"Absence Code <%s> in ODATAB not found",pcpCode);
		strcpy(pcpOdaUrno,"0");
	}

	return ilRc;
}


static int DraImport(ABSENCE *prpAbsence)
{
	int ilRc = RC_SUCCESS;
	char clSelection[1024];
	char clStfUrno[24];
	char clDrrUrno[24];
	char clDrrn[24];
	char clAbfr[24];
	char clAbto[24];
	char clOdaUrno[24];

	sprintf(clSelection,"WHERE PENO='%s'",prpAbsence->Peno);
	ilRc = DoSingleSelect("STFTAB",clSelection,"URNO",clStfUrno);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"DraImport: invalid Peno <%s>",prpAbsence->Peno);
	}
		/* search for absence code in ODATAB */
	if(ilRc == RC_SUCCESS)
	{
		ilRc = CheckAbsenceCode(prpAbsence->Code,clAbfr,clAbto,clOdaUrno);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"DraImport: invalid absence code <%s>",prpAbsence->Code);
			sprintf(cgErrorString,"DraImport: invalid absence code <%s>",prpAbsence->Code);
			LogError(cgInputFilePath,cgErrorString,TRUE);
		}
	}
	if(ilRc == RC_SUCCESS)
	{
		/* search for DRR record */
		sprintf(clSelection,"WHERE STFU='%s' and SDAY='%s' AND ROSS='A'",
				clStfUrno,prpAbsence->StartDate);
		ilRc = DoSingleSelect("DRRTAB",clSelection,"DRRN",clDrrn);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"DraImport: no matching DRR found <%s,%s,'A'>",
					clStfUrno,prpAbsence->StartDate);
			strcpy(clDrrn,"1");
		}
		switch(*prpAbsence->UpdateIndicator)
		{
			case 'I':
				InsertDra(prpAbsence,clStfUrno,clDrrn);
				break;
			case 'U':
				UpdateDra(prpAbsence,clStfUrno,clDrrn);
				break;
			case 'D':
				DeleteDra(prpAbsence,clStfUrno,clDrrn);
				break;
			default:
				dbg(TRACE,"ImportDra:invalid Update Indicator <%s>",prpAbsence->UpdateIndicator);
				sprintf(cgErrorString,"ImportDrr:invalid Update Indicator <%s>",
					prpAbsence->UpdateIndicator);
				LogError(cgInputFilePath,cgErrorString,TRUE);
				break;
		}
	}
	return ilRc;
}


static int AddToUrnoList(char *pcpUrno,char *pcpList,long lpListLength,long *plpActualLength)
{
	int ilRc = RC_SUCCESS;
	
  int ilNewLen;

	ilNewLen = strlen(pcpUrno) + 1;
	if (((*plpActualLength) + ilNewLen) > (lpListLength+50))
	{
		char *pclTmp;
		pclTmp = realloc(pcpList,lpListLength + 10000);
		if (pclTmp != NULL)
		{
			pcpList = pclTmp;
			lpListLength += 10000;
		}
		else
		{
			dbg(TRACE,"unable to reallocate %ld bytes for Urnolist",
					lpListLength+50000);
			Terminate(30);
		}
	}

	if (*pcpList != '\0')
	{
		sprintf(&pcpList[*plpActualLength],",'%s'",pcpUrno);
		*plpActualLength += ilNewLen;
	}
	else
	{
		sprintf(&pcpList[*plpActualLength],"'%s'",pcpUrno);
		*plpActualLength += ilNewLen-1;
	}

	/*dbg(DEBUG,"AddToUrnoList: <%s>",pcpList);*/

	return ilRc;
}

/* DRR_INSERT_FIELDS "BSDU,SCOD,SCOO,SDAY,AVFR,AVTO,STFU,DRRN,ROSS,ROSL,SBFR,SBTO,SBLU"*/
static int InsertDrr(ABSENCE *prpAbsence,char *pcpBsdu,char *pcpStfUrno,char *pcpAbfr,char *pcpAbto)
{
	int ilRc = RC_SUCCESS;
	char *pclDataArea;
	char clTmpDataArea[124];
	char clSelection[128];
	char clDrrUrno[24];
	char clDrrn[4];
	char clRosl[4];
	char clScod[24];
	BOOL blIsUpdated = FALSE;

	dbg(DEBUG,"InsertDrr: CODE <%s> BSDU <%s>, STFURNO <%s>",prpAbsence->Code,pcpBsdu,pcpStfUrno);

	memset(clTmpDataArea,0,sizeof(clTmpDataArea));
	sprintf(clSelection,"WHERE STFU='%s' AND SDAY='%s' AND ROSS='A'",
		pcpStfUrno,prpAbsence->StartDate);
	ilRc = DoSingleSelect("DRRTAB",clSelection,"URNO,DRRN,ROSL,SCOD",clTmpDataArea);

	if(ilRc == RC_SUCCESS)
	{
		/* set old DRR Ross to 'L' */
		GetDataItem(clDrrUrno,clTmpDataArea,1,',',"","\0\0");
		GetDataItem(clDrrn,clTmpDataArea,2,',',"","\0\0");
		GetDataItem(clRosl,clTmpDataArea,3,',',"","\0\0");
		GetDataItem(clScod,clTmpDataArea,4,',',"","\0\0");
		/*if (*clRosl == '2' && *clDrrn == '1')*/
		{
			dbg(DEBUG,"Old active DRR found, overwrite");
			memset(cgDataArea,0,sizeof(cgDataArea));
			pclDataArea = cgDataArea;

			pclDataArea = ItemPut(pclDataArea,pcpBsdu,','); 
			pclDataArea = ItemPut(pclDataArea,prpAbsence->Code,',');
			pclDataArea = ItemPut(pclDataArea,prpAbsence->Start,',');
			pclDataArea = ItemPut(pclDataArea,prpAbsence->End,',');
			pclDataArea = ItemPut(pclDataArea,"0000",',');
			pclDataArea = ItemPut(pclDataArea,"18991230000000",',');
			pclDataArea = ItemPut(pclDataArea,"18991230000000",',');
			pclDataArea = ItemPut(pclDataArea," ",',');
			pclDataArea = ItemPut(pclDataArea,cgSapSubDef,','); /* DRS2 set to SAPSUBDEF  */
			pclDataArea = ItemPut(pclDataArea,clScod,',');

dbg(DEBUG,"Update DRR follows <%s> Start <%s> End <%s>",cgDataArea,prpAbsence->Start,prpAbsence->End);

	pclDataArea = ItemPut(pclDataArea," ",','); /* DRS1 set to " "  */
	pclDataArea = ItemPut(pclDataArea," ",','); /* DRS3 set to " "  */
	pclDataArea = ItemPut(pclDataArea," ",','); /* DRS4 set to " "  */
			sprintf(clSelection,"WHERE URNO = '%s'",clDrrUrno);
			ilRc = DoUpdate("DRRTAB",clSelection,DRR_UPDATE_FIELDS,
					DRR_SEND_FIELDS,clDrrUrno,cgDataArea,TRUE);
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InsertDrr: DRR record <%s> to update not found",
					clSelection);
			}
			else
			{
				blIsUpdated = TRUE;
			}
		}
	}

	if (!blIsUpdated)
	{
		memset(cgDataArea,0,sizeof(cgDataArea));
		pclDataArea = cgDataArea;

		pclDataArea = ItemPut(pclDataArea,pcpBsdu,','); 
		pclDataArea = ItemPut(pclDataArea,prpAbsence->Code,',');
		/* copy shift code also in SCOO */
		pclDataArea = ItemPut(pclDataArea,prpAbsence->Code,',');
		pclDataArea = ItemPut(pclDataArea,prpAbsence->StartDate,',');
		pclDataArea = ItemPut(pclDataArea,prpAbsence->Start,',');
		pclDataArea = ItemPut(pclDataArea,prpAbsence->End,',');
		pclDataArea = ItemPut(pclDataArea,pcpStfUrno,',');
		pclDataArea = ItemPut(pclDataArea,"1",','); /* DRRN always '1' */
		pclDataArea = ItemPut(pclDataArea,"A",','); /* ROSS always 'A' */
		pclDataArea = ItemPut(pclDataArea,"2",','); /* ROSL always '2' */
		pclDataArea = ItemPut(pclDataArea,"18991230000000",','); /* SBFR always  */
		pclDataArea = ItemPut(pclDataArea,"18991230000000",','); /* SBTO always  */
		pclDataArea = ItemPut(pclDataArea,"0000",','); /* SBLU always  */
		pclDataArea = ItemPut(pclDataArea,cgSapSubDef,','); /* DRS2 set to SAPSUBDEF  */

		ilRc = DoInsert("DRRTAB",DRR_INSERT_FIELDS,DRR_INSERT_HOST,cgDataArea,TRUE);
	}

	if (ilRc == RC_SUCCESS)
	{
		/*AddToUrnoList(pcpStfUrno,pcgStfuList,lgStfuListLength,&lgStfuListActualLength);*/
	}
	return ilRc;
}

/*DRR_DELETE_FIELDS "BSDU=:VBSDU,SCOD=:VSCOD,SCOO=:VSCOO,SBFR=:VSBFR,SBTO=:VSBTO,SBLU=:VSBLU,AVFR=:VAVFR,AVTO=:VAVTO"*/

static int DeleteDrr(ABSENCE *prpAbsence,char *pcpStfUrno)
{
	int ilRc = RC_SUCCESS;
	char *pclDataArea;
	char clDrrUrno[24];
	char clScod[24];
	char clSelection[124];

	memset(cgDataArea,0,sizeof(cgDataArea));
	pclDataArea = cgDataArea;

	sprintf(clSelection,"WHERE STFU='%s' AND SDAY='%s' AND ROSS='A' AND SCOD = '%s'",
		pcpStfUrno,prpAbsence->StartDate,prpAbsence->Code);
	ilRc = DoSingleSelect("DRRTAB",clSelection,"URNO,SCOD",cgDataArea);
	if(ilRc == RC_SUCCESS)
	{
		GetDataItem(clDrrUrno,cgDataArea,1,',',"","\0\0");
		GetDataItem(clScod,cgDataArea,2,',',"","\0\0");

	  dbg(DEBUG,"DeleteDrr: URNO <%s> SCOD <%s>",clDrrUrno,prpAbsence->Code);

		memset(cgDataArea,0,sizeof(cgDataArea));
		pclDataArea = cgDataArea;

		/* BSDU */
		pclDataArea = ItemPut(pclDataArea," ",','); 
		/* SCOD */
		pclDataArea = ItemPut(pclDataArea," ",','); 
		/* SCOO */
		pclDataArea = ItemPut(pclDataArea,clScod,','); 
		/* SBFR */
		pclDataArea = ItemPut(pclDataArea," ",','); 
		/* SBTO */
		pclDataArea = ItemPut(pclDataArea," ",','); 
		/* SBLU */
		pclDataArea = ItemPut(pclDataArea," ",','); 
		/* AVFR */
		pclDataArea = ItemPut(pclDataArea," ",','); 
		/* AVTO */
		pclDataArea = ItemPut(pclDataArea," ",','); 

		ilRc = DoUpdate("DRRTAB",clSelection,DRR_DELETE_FIELDS,DRR_DEL_FIELDS,
				clDrrUrno,cgDataArea,TRUE);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"DeleteDrr: DRR record <%s> to delete not found",
				clSelection);
		}
		else
		{
			/*AddToUrnoList(pcpStfUrno,pcgStfuList,lgStfuListLength,&lgStfuListActualLength);*/
		}
	}
	else
	{
		sprintf(cgErrorString,"ImportDrr: Absence/Attendance not found "
		"for PKNO <%s> CODE <%s> DATE <%s>",prpAbsence->Peno,prpAbsence->Code,
		prpAbsence->StartDate);
		LogError(cgInputFilePath,cgErrorString,TRUE);
	}

	return ilRc;
}

/* DRR_UPDATE_FIELDS "BSDU=:VBSDU,SCOD=:VSCOD,AVFR=:VAVFR,AVTO=:VAVTO,SBLU=:VSBLU,SBFR=:VSBFR,SBTO=:VSBTO"*/
static int UpdateDrr(ABSENCE *prpAbsence,char *pcpBsdu,char *pcpStfUrno,char *pcpAbfr,char *pcpAbto)
{
	int ilRc = RC_SUCCESS;
	char *pclDataArea;
	char clDrrUrno[124];
	char clSelection[124];
	char clTmpDataArea[124];
	char clScod[24];

	dbg(DEBUG,"UpdateDrr: CODE <%s> BSDU <%s>, STFURNO <%s>",prpAbsence->Code,pcpBsdu,pcpStfUrno);
	memset(cgDataArea,0,sizeof(cgDataArea));
	pclDataArea = cgDataArea;

	pclDataArea = ItemPut(pclDataArea,pcpBsdu,','); 
	pclDataArea = ItemPut(pclDataArea,prpAbsence->Code,',');
	pclDataArea = ItemPut(pclDataArea,prpAbsence->Start,',');
	pclDataArea = ItemPut(pclDataArea,prpAbsence->End,',');
	pclDataArea = ItemPut(pclDataArea,"0000",',');
	pclDataArea = ItemPut(pclDataArea,"18991230000000",',');
	pclDataArea = ItemPut(pclDataArea,"18991230000000",',');
	pclDataArea = ItemPut(pclDataArea," ",',');
	pclDataArea = ItemPut(pclDataArea,cgSapSubDef,','); /* DRS2 set to SAPSUBDEF  */

	sprintf(clSelection,"WHERE STFU='%s' AND SDAY='%s' AND ROSS='A'",
		pcpStfUrno,prpAbsence->StartDate);
	ilRc = DoSingleSelect("DRRTAB",clSelection,"URNO,SCOD",clTmpDataArea);
	if (ilRc == RC_SUCCESS)
	{
		GetDataItem(clDrrUrno,clTmpDataArea,1,',',"","\0\0");
		GetDataItem(clScod,clTmpDataArea,2,',',"","\0\0");
		pclDataArea = ItemPut(pclDataArea,clScod,',');
	pclDataArea = ItemPut(pclDataArea," ",','); /* DRS1 set to " "  */
	pclDataArea = ItemPut(pclDataArea," ",','); /* DRS3 set to " "  */
	pclDataArea = ItemPut(pclDataArea," ",','); /* DRS4 set to " "  */
		ilRc = DoUpdate("DRRTAB",clSelection,DRR_UPDATE_FIELDS,DRR_SEND_FIELDS,
				clDrrUrno,cgDataArea,TRUE);
		if (ilRc == RC_SUCCESS)
		{
			/*AddToUrnoList(pcpStfUrno,pcgStfuList,lgStfuListLength,&lgStfuListActualLength);*/
		}
	}
	else
	{
		dbg(TRACE,"UpdateDrr: DRR record <%s> to update not found",
			clSelection);
		InsertDrr(prpAbsence,pcpBsdu,pcpStfUrno,pcpAbfr,pcpAbto);
		/*** delete the old invalid DRA-records **/
		sprintf(clSelection,"WHERE STFU='%s' AND SDAY='%s' ",
			pcpStfUrno,prpAbsence->StartDate);
		ilRc = DoDelete("DRATAB",clSelection);
	}

	/*** delete matching DRA record if it exist **/
	sprintf(clSelection,"WHERE STFU='%s' AND SDAY='%s' AND SDAC='%s'",
			pcpStfUrno,prpAbsence->StartDate,prpAbsence->Code);
			ilRc = DoDelete("DRATAB",clSelection);
	return ilRc;
}

static int DrrImport(ABSENCE *prpAbsence)
{
	int ilRc = RC_SUCCESS;
	char clSelection[1024];
	char clStfUrno[24];
	char clDrrUrno[24];
	char clDrrn[24];
	char clAbfr[24];
	char clAbto[24];
	char clOdaUrno[24];

	sprintf(clSelection,"WHERE PENO='%s'",prpAbsence->Peno);
	ilRc = DoSingleSelect("STFTAB",clSelection,"URNO",clStfUrno);
	if(ilRc == RC_SUCCESS)
	{
		/* search for absence code in ODATAB */
		ilRc = CheckAbsenceCode(prpAbsence->Code,clAbfr,clAbto,clOdaUrno);
		dbg(DEBUG,"DrrImport: RC from CheckAbsenceCode %d ODA-URNO <%s> ABFR <%s> ABTO <%s>",ilRc,clOdaUrno,clAbfr,clAbto);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"DrrImport: invalid absence code <%s>",prpAbsence->Code);
			sprintf(cgErrorString,"DraImport: invalid absence code <%s>",prpAbsence->Code);
			LogError(cgInputFilePath,cgErrorString,TRUE);
		}
	}
	else
	{
		dbg(TRACE,"DrrImport: invalid Peno <%s>",prpAbsence->Peno);
	}
	if(ilRc == RC_SUCCESS)
	{
#if 0
		/* search for DRR record */
		sprintf(clSelection,"WHERE STFU='%s' and SDAY='%s' AND ROSS='A'",
				clStfUrno,prpAbsence->StartDate);
		ilRc = DoSingleSelect("DRRTAB",clSelection,"DRRN",clDrrn);
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"DraImport: no matching DRR found <%s,%s,'A'>",
					clStfUrno,prpAbsence->StartDate);
			strcpy(clDrrn,"1");
		}
#endif
	
		if (strcmp(prpAbsence->End,"20301231") > 0)
		{
			dbg(TRACE,"ImportDrr:invalid absence end date <%s>",prpAbsence->End);
			sprintf(cgErrorString,"ImportDrr:invalid Absence End Date <%s>",
					prpAbsence->End);
			LogError(cgInputFilePath,cgErrorString,TRUE);
		}
		else
		{
		switch(*prpAbsence->UpdateIndicator)
		{
			case 'I':
				dbg(DEBUG,"Start <%s> End <%s>",prpAbsence->Start,prpAbsence->End);
				if (strncmp(prpAbsence->Start,prpAbsence->End,8) < 0)
				{
					/*** absence is more than one day **/
				  char clDay[24];
				  char clEnd[24];

					strcpy(clEnd,prpAbsence->End);
					strcpy(clDay,prpAbsence->Start);
					while (strncmp(clDay,clEnd,8) <= 0)
					{
						strncpy(prpAbsence->StartDate,clDay,8);
						prpAbsence->StartDate[8] = '\0';
						strcpy(prpAbsence->Start,clDay);
						strcpy(prpAbsence->End,clDay);
						dbg(DEBUG,"Start <%s> End <%s>",prpAbsence->Start,prpAbsence->End);
						InsertDrr(prpAbsence,clOdaUrno,clStfUrno,clDay,clDay);
						AddSecondsToCEDATime(clDay,(time_t)86400,1);
					}
				}
				else
				{
					InsertDrr(prpAbsence,clOdaUrno,clStfUrno,clAbfr,clAbto);
				}
				break;
			case 'U':
				dbg(DEBUG,"Start <%s> End <%s>",prpAbsence->Start,prpAbsence->End);
				if (strncmp(prpAbsence->Start,prpAbsence->End,8) < 0)
				{
					/*** absence is more than one day **/
				  char clDay[24];
				  char clEnd[24];

					strcpy(clEnd,prpAbsence->End);
					strcpy(clDay,prpAbsence->Start);
					while (strncmp(clDay,clEnd,8) <= 0)
					{
						strncpy(prpAbsence->StartDate,clDay,8);
						prpAbsence->StartDate[8] = '\0';
						strcpy(prpAbsence->Start,clDay);
						strcpy(prpAbsence->End,clDay);
						dbg(DEBUG,"Start <%s> End <%s>",prpAbsence->Start,prpAbsence->End);
						UpdateDrr(prpAbsence,clOdaUrno,clStfUrno,clDay,clDay);
						AddSecondsToCEDATime(clDay,(time_t)86400,1);
					}
				}
				else
				{
					UpdateDrr(prpAbsence,clOdaUrno,clStfUrno,clAbfr,clAbto);
				}
				break;
			case 'D':
				dbg(DEBUG,"Start <%s> End <%s>",prpAbsence->Start,prpAbsence->End);
				if (strncmp(prpAbsence->Start,prpAbsence->End,8) < 0)
				{
					/*** absence is more than one day **/
				  char clDay[24];
				  char clEnd[24];

					strcpy(clEnd,prpAbsence->End);
					strcpy(clDay,prpAbsence->Start);
					while (strncmp(clDay,clEnd,8) <= 0)
					{
						strncpy(prpAbsence->StartDate,clDay,8);
						prpAbsence->StartDate[8] = '\0';
						strcpy(prpAbsence->Start,clDay);
						strcpy(prpAbsence->End,clDay);
						dbg(DEBUG,"Start <%s> End <%s>",prpAbsence->Start,prpAbsence->End);
						DeleteDrr(prpAbsence,clStfUrno);
						AddSecondsToCEDATime(clDay,(time_t)86400,1);
					}
				}
				else
				{
					DeleteDrr(prpAbsence,clStfUrno);
				}
				break;
			default:
				dbg(TRACE,"ImportDrr:invalid Update Indicator <%s>",prpAbsence->UpdateIndicator);
				sprintf(cgErrorString,"ImportDrr:invalid Update Indicator <%s>",
					prpAbsence->UpdateIndicator);
				LogError(cgInputFilePath,cgErrorString,TRUE);
				break;
		}
		}
	}
	return ilRc;
}


static int FillAttendanceStruct(char *pcpBuf,ABSENCE *prpAbsence)
{
	int ilRc = RC_SUCCESS;
	int ilActualPos = 0;
	char *pclBuf;

	pclBuf = pcpBuf;

	/* Employee Number */
	StringMid(prpAbsence->Peno,pclBuf,ilActualPos,8,CLIENTTODB);
	ilActualPos += 8;
	/* Absence Code */
	StringMid(prpAbsence->Code,pclBuf,ilActualPos,4,CLIENTTODB);
	ilActualPos += 4;
	/* Full day indicator */
	StringMid(prpAbsence->FullDayIndicator,pclBuf,ilActualPos,1,CLIENTTODB);
	ilActualPos += 1;
	/* Start Date */
	StringMid(prpAbsence->StartDate,pclBuf,ilActualPos,8,CLIENTTODB);
	ilActualPos += 8;
	/* End Date */
	StringMid(prpAbsence->EndDate,pclBuf,ilActualPos,8,CLIENTTODB);
	ilActualPos += 8;
	/* Start Time */
	StringMid(prpAbsence->StartTime,pclBuf,ilActualPos,4,CLIENTTODB);
	ilActualPos += 4;
	if (strcmp(prpAbsence->StartTime,"    ") == 0)
	{
		strcpy(prpAbsence->StartTime,"0830");
	}
	/* End Time */
	StringMid(prpAbsence->EndTime,pclBuf,ilActualPos,4,CLIENTTODB);
	ilActualPos += 4;
	if (strcmp(prpAbsence->EndTime,"    ") == 0)
	{
		strcpy(prpAbsence->EndTime,"0830");
	}
	/* Update Indicator */
	StringMid(prpAbsence->UpdateIndicator,pclBuf,ilActualPos,1,CLIENTTODB);
	ilActualPos += 1;

	sprintf(prpAbsence->Start,"%s%s00",prpAbsence->StartDate,prpAbsence->StartTime);
	sprintf(prpAbsence->End,"%s%s00",prpAbsence->EndDate,prpAbsence->EndTime);

	if(CheckDateFormat(prpAbsence->Start) != RC_SUCCESS)
	{
			sprintf(cgErrorString,"invalid date format in absence"
				" Start <%s>",prpAbsence->Start);
			LogError(cgInputFilePath,cgErrorString,TRUE);
			return(RC_FAIL);
	}

	if(CheckDateFormat(prpAbsence->End) != RC_SUCCESS)
	{
			sprintf(cgErrorString,"invalid date format in absence"
				" End <%s>",prpAbsence->End);
			LogError(cgInputFilePath,cgErrorString,TRUE);
			return(RC_FAIL);
	}

	if(debug_level == DEBUG)
	{
		dbg(TRACE,"FillAttendanceStruct: PENO     <%s>",prpAbsence->Peno);
		dbg(TRACE,"FillAttendanceStruct: CODE     <%s>",prpAbsence->Code);
		dbg(TRACE,"FillAttendanceStruct: FULLDAY  <%s>",prpAbsence->FullDayIndicator);
		dbg(TRACE,"FillAttendanceStruct: STARTDATE<%s>",prpAbsence->StartDate);
		dbg(TRACE,"FillAttendanceStruct: ENDDATE  <%s>",prpAbsence->EndDate);
		dbg(TRACE,"FillAttendanceStruct: STARTTIME<%s>",prpAbsence->StartTime);
		dbg(TRACE,"FillAttendanceStruct: ENDTIME  <%s>",prpAbsence->EndTime);
		dbg(TRACE,"FillAttendanceStruct: UPDATE-I <%s>",prpAbsence->UpdateIndicator);
		dbg(TRACE,"FillAttendanceStruct: START    <%s>",prpAbsence->Start);
		dbg(TRACE,"FillAttendanceStruct: END      <%s>",prpAbsence->End);
	}


	return ilRc;
}

static int FillAbsenceStruct(char *pcpBuf,ABSENCE *prpAbsence)
{
	int ilRc = RC_SUCCESS;
	int ilActualPos = 0;
	char *pclBuf;

	pclBuf = pcpBuf;

	/* Employee Number */
	StringMid(prpAbsence->Peno,pclBuf,ilActualPos,8,CLIENTTODB);
	ilActualPos += 8;
	/* Absence Code */
	StringMid(prpAbsence->Code,pclBuf,ilActualPos,4,CLIENTTODB);
	ilActualPos += 4;
	/* Full day indicator */
	StringMid(prpAbsence->FullDayIndicator,pclBuf,ilActualPos,1,CLIENTTODB);
	ilActualPos += 1;
	/* Start Date */
	StringMid(prpAbsence->StartDate,pclBuf,ilActualPos,8,CLIENTTODB);
	ilActualPos += 8;
	/* End Date */
	StringMid(prpAbsence->EndDate,pclBuf,ilActualPos,8,CLIENTTODB);
	ilActualPos += 8;
	/* Start Time */
	StringMid(prpAbsence->StartTime,pclBuf,ilActualPos,4,CLIENTTODB);
	ilActualPos += 4;
	/* End Time */
	StringMid(prpAbsence->EndTime,pclBuf,ilActualPos,4,CLIENTTODB);
	ilActualPos += 4;
	/* Update Indicator */
	StringMid(prpAbsence->UpdateIndicator,pclBuf,ilActualPos,1,CLIENTTODB);
	ilActualPos += 1;

	if (*prpAbsence->FullDayIndicator == '1')
	{
		strcpy(prpAbsence->StartTime,"0830");
		strcpy(prpAbsence->EndTime,"0830");
	}
	sprintf(prpAbsence->Start,"%s%s00",prpAbsence->StartDate,prpAbsence->StartTime);
	sprintf(prpAbsence->End,"%s%s00",prpAbsence->EndDate,prpAbsence->EndTime);

	if(CheckDateFormat(prpAbsence->Start) != RC_SUCCESS)
	{
			sprintf(cgErrorString,"invalid date format in absence"
				" Start <%s>",prpAbsence->Start);
			LogError(cgInputFilePath,cgErrorString,TRUE);
			ilRc = RC_FAIL;
	}

	if(CheckDateFormat(prpAbsence->End) != RC_SUCCESS)
	{
			sprintf(cgErrorString,"invalid date format in absence"
				" End <%s>",prpAbsence->End);
			LogError(cgInputFilePath,cgErrorString,TRUE);
			ilRc = RC_FAIL;
	}

	if(debug_level == DEBUG)
	{
		dbg(TRACE,"FillAbsenceStruct: PENO     <%s>",prpAbsence->Peno);
		dbg(TRACE,"FillAbsenceStruct: CODE     <%s>",prpAbsence->Code);
		dbg(TRACE,"FillAbsenceStruct: FULLDAY  <%s>",prpAbsence->FullDayIndicator);
		dbg(TRACE,"FillAbsenceStruct: STARTDATE<%s>",prpAbsence->StartDate);
		dbg(TRACE,"FillAbsenceStruct: ENDDATE  <%s>",prpAbsence->EndDate);
		dbg(TRACE,"FillAbsenceStruct: STARTTIME<%s>",prpAbsence->StartTime);
		dbg(TRACE,"FillAbsenceStruct: ENDTIME  <%s>",prpAbsence->EndTime);
		dbg(TRACE,"FillAbsenceStruct: UPDATE-I <%s>",prpAbsence->UpdateIndicator);
		dbg(TRACE,"FillAbsenceStruct: START    <%s>",prpAbsence->Start);
		dbg(TRACE,"FillAbsenceStruct: END      <%s>",prpAbsence->End);
	}
	return ilRc;
}


static int AbsencesImport()
{
	int ilRc = RC_SUCCESS;
	char clBuf[1024];
	char clSequence[12];
	char *pclBaseName;
	long llSequence,llOldSequence;
	FILE *pflInFile = NULL;
	short slFunc = START;
	char clFileMonth[12];
	BOOL blHeaderFound = FALSE;
	BOOL blHeaderAlerted = FALSE;
	BOOL blTrailerFound = FALSE;
	long llLinesInFile;
	long llLineCount;
	int  ilFilesFound = 0;

	while(GetFileNames(slFunc,cgAbsFileId,cgFileNew,cgInputFilePath,FALSE) == RC_SUCCESS)
	{
		slFunc = NEXT;
		dbg(TRACE,"Absence File <%s> will be processed",cgInputFilePath);
		/* first check sequence number */
		pclBaseName = strrchr(cgInputFilePath,'/') + 1;
		pclBaseName = pclBaseName == NULL ? cgInputFilePath : pclBaseName;
		StringMid(clFileMonth,pclBaseName,8,2,CLIENTTODB);
		StringMid(clSequence,pclBaseName,10,6,CLIENTTODB);
		dbg(DEBUG,"Sequence is <%s> from <%s>",clSequence,pclBaseName);
		llSequence = atol(clSequence);
		llOldSequence = GetSequence("SAPABS",clFileMonth);
		if (llSequence <= llOldSequence)
		{
			dbg(TRACE,"File already processed sequence %d sequence from DB %d",
					llSequence,llOldSequence);
			sprintf(cgErrorString,"Absences Import: File <%s> out of sequence, "
			   "sequence %d expected",pclBaseName,llOldSequence+1);
			LogError(cgInputFilePath,cgErrorString,TRUE);
			continue;
		}
		else
		{
			dbg(DEBUG,"Import File <%s> will be opened",cgInputFilePath);
			ilFilesFound++;
			pflInFile = fopen(cgInputFilePath,"rt");
			if (pflInFile != NULL)
			{
				dbg(DEBUG,"Import File <%s> opened",cgInputFilePath);
  			memset(clBuf,0,sizeof(clBuf));
				llLinesInFile = 0;
				while(fgets(cgLineBuf,sizeof(cgLineBuf),pflInFile) != NULL)
				{
					dbg(DEBUG,"AbsencesImport: Buf <%s>",cgLineBuf);
					StringTrimRight(cgLineBuf);

					if(strncmp(cgLineBuf,"00",2) == 0)
					{
						/* header record */
						char clLineCount[24];
						strncpy(clLineCount,&cgLineBuf[40],10);
						clLineCount[10] = '\0';
						llLineCount = atol(clLineCount);
						dbg(DEBUG,"AbsencesImport, %d/<%s> records will be processed",
							llLineCount,clLineCount);
						blHeaderFound = TRUE;
					}
					if((strncmp(cgLineBuf,"99",2) == 0) && (blHeaderFound))
					{
						/* trailer record */
						blTrailerFound = TRUE;
					}


					if(strncmp(cgLineBuf,"10",2) == 0)
					{
						llLinesInFile++;
						if (FillAbsenceStruct((char *)cgLineBuf+2,&rgAbsenceData) == RC_SUCCESS)
						{
	dbg(TRACE,"AbsenceStruct: Start <%s> End <%s>", rgAbsenceData.Start,rgAbsenceData.End);
							if (rgAbsenceData.FullDayIndicator[0] == '1')
							{
								/* Full Day Absence, insert into DRRTAB */
								DrrImport(&rgAbsenceData);
							}
							else if (rgAbsenceData.FullDayIndicator[0] == '2')
							{
								/* Part Day Absence, insert into DRATAB */
								DraImport(&rgAbsenceData);
							}
							else 
							{
								dbg(TRACE,"Absences Import: invalid Full Day Indicator <%s>",
										rgAbsenceData.FullDayIndicator);
							}
						}
					}
				}
				fclose(pflInFile);
				if (*cgUpdAbsSequence == 'Y')
				{
					sprintf(cgArcFileName,"%s%s%s",cgFileBase,cgFileArc,pclBaseName);
					dbg(TRACE,"Absence Import: Import File <%s> will be moved to <%s>",
							cgInputFilePath,cgArcFileName);
					if(rename(cgInputFilePath,cgArcFileName) != RC_SUCCESS)
					{
						dbg(TRACE,"Absence Import: cannot rename file, reason: %d %s",
								errno,strerror(errno));
					}
					PutSequence("SAPABS",llSequence,clFileMonth);
				}
			}
			else
			{
				dbg(TRACE,"Absences Import: File <%s> not opened",cgInputFilePath);
			}

			if (!blHeaderFound)
			{
					dbg(TRACE,"Absences Import: No header found in file <%s>"
					", could not determine line count",cgInputFileName);
					sprintf(cgErrorString,"Absences Import: No header found in"
					" file <%s>, could not determine line count",cgInputFileName);
					LogError(pclBaseName,cgErrorString,TRUE);
			}

			if (!blTrailerFound)
			{
					dbg(TRACE,"Absences Import: No trailer found in file <%s>"
					", %d lines from %d already imported",
						cgInputFileName,llLinesInFile,llLineCount);
					sprintf(cgErrorString,"Absences Import: No trailer found in file <%s>"
					", %d lines from %d already imported",
						pclBaseName,llLinesInFile,llLineCount);
					LogError(cgInputFilePath,cgErrorString,TRUE);
			}
			else
			{
					dbg(TRACE,"Absences Import: "
					" %d lines from %d imported",llLinesInFile,llLineCount);
					if (llLinesInFile != llLineCount)
					{
						sprintf(cgErrorString,"Absences Import: invalid number of "
							" lines  in file <%s> %ld, should be %ld",
							pclBaseName,llLinesInFile,llLineCount);
						LogError(cgInputFilePath,cgErrorString,TRUE);
					}
			}

		}
	}
	if (ilFilesFound == 0)
	{
		sprintf(cgErrorString,"No absences import file found");
		*cgInputFilePath = '\0';
		LogError(cgInputFilePath,cgErrorString,TRUE);
	}
  return ilRc;
}

static int AttendancesImport()
{
	int ilRc = RC_SUCCESS;
	char clSequence[12];
	char *pclBaseName;
	long llSequence,llOldSequence;
	FILE *pflInFile = NULL;
	short slFunc = START;
	BOOL blHeaderFound = FALSE;
	BOOL blHeaderAlerted = FALSE;
	BOOL blTrailerFound = FALSE;
	char clFileMonth[12];
	long llLinesInFile;
	long llLineCount;
	int  ilFilesFound = 0;

	while(GetFileNames(slFunc,cgAttFileId,cgFileNew,cgInputFilePath,FALSE) == RC_SUCCESS)
	{
		*cgLineBuf = '\0';
		slFunc = NEXT;
		dbg(TRACE,"Attendance File <%s> will be processed",cgInputFilePath);
		/* first check sequence number */
		pclBaseName = strrchr(cgInputFilePath,'/') + 1;
		pclBaseName = pclBaseName == NULL ? cgInputFilePath : pclBaseName;
		StringMid(clFileMonth,pclBaseName,8,2,CLIENTTODB);
		StringMid(clSequence,pclBaseName,10,6,CLIENTTODB);
		dbg(DEBUG,"Sequence is <%s> from <%s>",clSequence,pclBaseName);
		llSequence = atol(clSequence);
		llOldSequence = GetSequence("SAPATT",clFileMonth);
		if (llSequence <= llOldSequence)
		{
			dbg(TRACE,"File already processed sequence %d sequence from DB %d",
					llSequence,llOldSequence);
			sprintf(cgErrorString,"Absences Import: File <%s> out of sequence, "
			   "sequence %d expected",pclBaseName,llOldSequence+1);
			LogError(cgInputFilePath,cgErrorString,TRUE);
			continue;
		}
		else
		{
			dbg(DEBUG,"Import File <%s> will be opened",cgInputFilePath);
			ilFilesFound++;
			pflInFile = fopen(cgInputFilePath,"rt");
			if (pflInFile != NULL)
			{
				dbg(DEBUG,"Import File <%s> opened",cgInputFilePath);
  			memset(cgLineBuf,0,sizeof(cgLineBuf));
				llLinesInFile = 0;
				while(fgets(cgLineBuf,sizeof(cgLineBuf),pflInFile) != NULL)
				{
					StringTrimRight(cgLineBuf);

					if(strncmp(cgLineBuf,"00",2) == 0)
					{
						/* header record */
						char clLineCount[24];
						strncpy(clLineCount,&cgLineBuf[40],10);
						clLineCount[10] = '\0';
						llLineCount = atol(clLineCount);
						dbg(DEBUG,"AttendancesImport, %d/<%s> records will be processed",
							llLineCount,clLineCount);
						blHeaderFound = TRUE;
					}
					if((strncmp(cgLineBuf,"99",2) == 0) && (blHeaderFound))
					{
						/* trailer record */
						blTrailerFound = TRUE;
					}

					if(strncmp(cgLineBuf,"10",2) == 0)
					{
						llLinesInFile++;
						if (FillAttendanceStruct((char *)cgLineBuf+2,&rgAttendanceData) == RC_SUCCESS)
						{ /* skip record id */
							if (rgAttendanceData.FullDayIndicator[0] == '1')
							{
								/* Full Day Attendance, insert into DRRTAB */
								DrrImport(&rgAttendanceData);
							}
							else if (rgAttendanceData.FullDayIndicator[0] == '2')
							{
								/* Part Day Attendance, insert into DRATAB */
								DraImport(&rgAttendanceData);
							}
							else 
							{
								dbg(TRACE,"Attendances Import: invalid Full Day Indicator <%s>",
										rgAttendanceData.FullDayIndicator);
								sprintf(cgErrorString,"Attendances Import: invalid Full "
								"Day Indicator <%s> ",pclBaseName);
								LogError(cgInputFilePath,cgErrorString,TRUE);
							}
						}
					}
				}
				fclose(pflInFile);
				if (*cgUpdAbsSequence == 'Y')
				{
					sprintf(cgArcFileName,"%s%s%s",cgFileBase,cgFileArc,pclBaseName);
					dbg(TRACE,"Attendance Import: Import File <%s> will be moved to <%s>",
							cgInputFilePath,cgArcFileName);
					if(rename(cgInputFilePath,cgArcFileName) != RC_SUCCESS)
					{
						dbg(TRACE,"Attendance Import: cannot rename file, reason: %d %s",
								errno,strerror(errno));
					}
					PutSequence("SAPATT",llSequence,clFileMonth);
				}

			}
			else
			{
				dbg(TRACE,"Attendances Import: File <%s> not opened",cgInputFilePath);
			}
		}

		if (!blHeaderFound)
		{
				dbg(TRACE,"Attendances Import: No header found in file <%s>"
				", could not determine line count",pclBaseName);
				sprintf(cgErrorString,"Attendances Import: No header found in"
				" file <%s>, could not determine line count",pclBaseName);
				LogError(cgInputFilePath,cgErrorString,TRUE);
		}

		if (!blTrailerFound)
		{
				dbg(TRACE,"Attendances Import: No trailer found in file <%s>"
				", %d lines from %d already imported",
					pclBaseName,llLinesInFile,llLineCount);
				sprintf(cgErrorString,"Attendances Import: No trailer found in file <%s>"
				", %d lines from %d already imported",
					pclBaseName,llLinesInFile,llLineCount);
				LogError(cgInputFilePath,cgErrorString,TRUE);
		}
		else
		{
				dbg(TRACE,"Attendances Import: "
				" %d lines from %d imported",llLinesInFile,llLineCount);
				if (llLinesInFile != llLineCount)
				{
					sprintf(cgErrorString,"Attendances Import: invalid number of "
						" lines  in file <%s> %ld, should be %ld",
						pclBaseName,llLinesInFile,llLineCount);
					LogError(cgInputFilePath,cgErrorString,TRUE);
				}
		}
	}
	if (ilFilesFound == 0)
	{
		sprintf(cgErrorString,"No attendances import file found");
		*cgInputFilePath = '\0';
		LogError(cgInputFilePath,cgErrorString,TRUE);
	}
  return ilRc;
}

static void MoveToErrorFile(char *pcpLineBuf)
{
	char clFileName[312];
	FILE *fplErrorFile = NULL;

	sprintf(clFileName,"%s%s%s.err",cgFileBase,cgFileError,cgInputFileName);
  fplErrorFile = fopen (clFileName, "a") ;
  if (fplErrorFile == NULL)
  {
    dbg (TRACE, "MoveToErrorFile: fopen <%s> failed <%d-%s>", clFileName, errno, strerror(errno)) ;
  } 
	else
	{
		fprintf(fplErrorFile,"%s\r\n",pcpLineBuf);
		fclose(fplErrorFile);
	}
}


static void IncrementCount(int ipRc)
{
		if (ipRc == RC_SUCCESS)
		{
			lgSuccessCount++;
		}
		else
		{
			lgErrorCount++;
			MoveToErrorFile(cgLineBuf);
		}
}

static int CheckRegi(char *pcpSurn,char *pcpSday)
{
	char clAcfr[24];
	char clActo[24];
	char clSelection[240];

	int ilRc = RC_SUCCESS;
	sprintf(clAcfr,"%s000000",pcpSday);
	sprintf(clActo,"%s235959",pcpSday);
	LocalTimeToUtcFixTZ(clAcfr);
	sprintf(clSelection,"WHERE REGI IN(%s) AND SURN = '%s' AND "
	" (VPTO >= '%s' OR VPTO = ' ') AND VPFR <= '%s'",
			cgRegi,pcpSurn,clAcfr,clActo);
	ilRc = DoSingleSelect("SRETAB",clSelection,"URNO",cgDataArea);
	if (ilRc != RC_SUCCESS)
	{
		dbg(DEBUG,"CheckRegi(): No regular shift worker: RC=%d <%s>",ilRc,clSelection);
	}
	else
	{
		dbg(DEBUG,"CheckRegi(): Is regular shift worker: RC=%d <%s>",ilRc,clSelection);
	}
	return ilRc;
}

static int CheckSre(char *pcpSurn,char *pcpSday)
{
	int ilRc = RC_FAIL;
	long llRow = ARR_FIRST;
	char clKey[124];
	char clVpfr[32],clVpto[32];

	strcpy(clKey,pcpSurn);

	while(CEDAArrayFindRowPointer(&rgSreArray.rrArrayHandle,rgSreArray.crArrayName,
	&rgSreArray.rrIdx01Handle,rgSreArray.crIdx01Name,clKey,
		&llRow,(void **)&rgSreArray.pcrArrayRowBuf) == RC_SUCCESS)
		{
			llRow = ARR_NEXT;
			strcpy(clVpfr,FIELDVAL(rgSreArray,rgSreArray.pcrArrayRowBuf,igSreVPFR));
			strcpy(clVpto,FIELDVAL(rgSreArray,rgSreArray.pcrArrayRowBuf,igSreVPTO));
			clVpfr[8] = '\0';

			if (*clVpto == ' ')
			{

				if (strcmp(pcpSday,clVpfr) >= 0)
				{
					ilRc = RC_SUCCESS;
					break;
				}
			}
			else
			{
				clVpto[8] = '\0';
				if ((strcmp(pcpSday,clVpfr) >= 0) && (strcmp(pcpSday,clVpto)  <= 0))
				{
					ilRc = RC_SUCCESS;
					break;
				}
			}
			dbg(DEBUG,"SRE record does not match: %s for SDAY <%s> VPFR <%s> VPTO <%s>",
				pcpSurn,pcpSday,clVpfr,clVpto);
		}

	if(ilRc == RC_SUCCESS)
	{
		dbg(DEBUG,"Shift indicator found for %s SDAY <%s> VPFR <%s> VPTO <%s>",
				pcpSurn,pcpSday,clVpfr,clVpto);
	}
	else
	{
		dbg(DEBUG,"no Shift indicator found for %s SDAY <%s>",
				pcpSurn,pcpSday);
	}
	return ilRc;
}

static int GetSbluFromBSDTAB(char *pcpBsdc,char *pcpSblu)
{
	int ilRc = RC_SUCCESS;
	char clBsdc[44];
	long llAction = ARR_FIRST;

	/* we need the basic shift times */
	/* BSD_FIELDS "URNO,BSDC,ESBG,LSEN,BKD1"*/

	strcpy(clBsdc,pcpBsdc);
	ilRc = CEDAArrayFindRowPointer(&rgBsdArray.rrArrayHandle,rgBsdArray.crArrayName,
			&rgBsdArray.rrIdx02Handle,rgBsdArray.crIdx02Name,clBsdc,
				&llAction,(void **)&rgBsdArray.pcrArrayRowBuf);

	if (ilRc == RC_SUCCESS)
	{
		sprintf(pcpSblu,"%s",
			FIELDVAL(rgBsdArray,rgBsdArray.pcrArrayRowBuf,igBsdBKD1));
	}
	else
	{
		dbg(TRACE,"BSD record with BSDC <%s> not found",clBsdc);
		strcpy(pcpSblu,"0000");
	}
	return ilRc;
}

static int GetOvertime(char *pcpBsdc,char *pcpSday,char *pcpFrom,char *pcpTo,char *pcpDrs1,char *pcpDrs3,char *pcpDrs4)
{
	int ilRc = RC_SUCCESS;
 	char clBsdStart[24];
 	char clTmpStr[24];
	char clBsdEnd[24];
	char clBsdc[44];
	BOOL blIsBsd = FALSE;
	long llAction = ARR_FIRST;

	/* we need the basic shift times */
	/* BSD_FIELDS "URNO,BSDC,ESBG,LSEN,BKD1"*/


	strcpy(clBsdc,pcpBsdc);
	ilRc = CEDAArrayFindRowPointer(&rgBsdArray.rrArrayHandle,rgBsdArray.crArrayName,
			&rgBsdArray.rrIdx02Handle,rgBsdArray.crIdx02Name,clBsdc,
				&llAction,(void **)&rgBsdArray.pcrArrayRowBuf);

	if (ilRc == RC_SUCCESS)
	{
		sprintf(clBsdStart,"%s",
			FIELDVAL(rgBsdArray,rgBsdArray.pcrArrayRowBuf,igBsdESBG));
		sprintf(clBsdEnd,"%s",
			FIELDVAL(rgBsdArray,rgBsdArray.pcrArrayRowBuf,igBsdLSEN));
		blIsBsd = TRUE;
	}
	else
	{
		dbg(TRACE,"BSD record with BSDC <%s> not found",clBsdc);
		blIsBsd = FALSE;
	}

	if (*pcpDrs4 == '1')
	{
			/* complete shift is overtime */
			dbg(DEBUG,"DRS4 = '1'");
			strcpy(pcpFrom,clBsdStart);
			strcpy(pcpTo,clBsdEnd);
	}
	else 
	{
		if (*pcpDrs1 == '1')
		{
			/* overtime before begin of shift */
			if (blIsBsd)
			{
				dbg(DEBUG,"DRS1 = '1'");
				strcpy(pcpFrom,clBsdStart);
			}
		}
		if (*pcpDrs3 == '1')
		{
			/* overtime after end of shift */
				dbg(DEBUG,"DRS3 = '1'");
			if (blIsBsd)
			{
				strcpy(pcpTo,clBsdEnd);
			}
		}
	}
	return ilRc;
}


static int OpenTmpFile(BOOL bpRead)
{
	int ilRc = RC_SUCCESS;
	char clMode[6];

	if (bpRead == TRUE)
	{
		strcpy(clMode,"rt");
	}
	else
	{
		strcpy(clMode,"at");
	}

	if (pfgTmpFile == NULL)
	{

		sprintf(cgTmpFilePath,"%s%s%s",cgFileBase,cgTmpPath,"SUB1CHANGES.TMP");
		dbg(DEBUG,"%05d:  <%s> <%s> <%s> ",__LINE__,cgFileBase,cgTmpPath,"SUB1CHANGES.TMP");

		errno = 0;
		pfgTmpFile = fopen(cgTmpFilePath,clMode);
		if (pfgTmpFile == NULL)
		{
			dbg(TRACE,"Temporary file <%s> not created/opened with mode <%s>, reason: %d %s",
				cgTmpFilePath,clMode,errno,strerror(errno));
			ilRc = RC_FAIL;
		}
	}

	return ilRc;
}


static int ExportSubsequentChanges(char *pcpDrrUrno)
{
	int ilRc = RC_SUCCESS;

	OpenTmpFile(FALSE);
	if (pfgTmpFile != NULL)
	{
		ilRc = WriteSubsequentChanges(pfgTmpFile,pcpDrrUrno);
	}

	return ilRc;
}


static int WriteSubsequentChanges(FILE *pfpFile,char *pcpDrrUrno)
{

	int ilRc = RC_SUCCESS;

	char clSelection[262];
	char clStfUrno[12];
	char clKey[22];
	char clPlfr[24],clPlto[24],clPlSCOD[24],clPeno[36];
	char clAcfr[24],clActo[24];
	long llAction = ARR_FIRST;
	long llDrrPlanAction = ARR_FIRST;
	long llJobAction = ARR_FIRST;
	long llCount = 0;
	long llRowCount = 0;
	long llRow = 0;
	char clDrrDataArea[1024];
	char clDrrPlanSelection[224];
	char clJobSelection[224];
	short slFunction,slCursor;
	char *pclRowBuf;
	char *pclDrrRowBuf;
	char clTmpStr[24];
	char clSday[24];
	char clDrrUrno[24];
	char clDrs1[4],clDrs3[4],clDrs4[4];
	BOOL blExport = FALSE;

	sprintf(clSelection,"WHERE DRRTAB.URNO = '%s' AND STFU=STFTAB.URNO",pcpDrrUrno);
	slFunction = START;
	slCursor = 0;

	if (DoSingleSelect("DRRTAB,STFTAB",clSelection,cgDrrStfFields,clDrrDataArea) == RC_SUCCESS)
	{
		char clTimeStamp[24];
		slFunction = NEXT;

		/* check the date, if it is in the export timeframe */
		GetDataItem(clSday,clDrrDataArea,8,',',"","\0\0");
		GetServerTimeStamp("LOC", 1,0,clTimeStamp);
		AddSecondsToCEDATime(clTimeStamp,((igSubChangesOffset)*86400),1);
		clTimeStamp[8] = '\0';
		if (strcmp(clSday,clTimeStamp) >= 0)
		{
			GetDataItem(clStfUrno,clDrrDataArea,9,',',"","\0\0");

			/* check regularity indicator (SRETAB) and Tohr-flag (STFTAB) */
			if (CheckTohr(clStfUrno) == TRUE && CheckRegi(clStfUrno,clSday) == RC_SUCCESS)
			{
				dbg(DEBUG,"DRR will be exported: sday <%s> Exportperiod starts <%s>",clSday,clTimeStamp);
	/*  DRRSTF_FIELDS "AVFR,AVTO,DRRN,SBFR,SBLU,SBTO,SCOD,SDAY,STFU,PENO,DRRTAB.URNO,DRS1,DRS3,DRS4,DRS2,ROSL"*/
				GetDataItem(clAcfr,clDrrDataArea,1,',',"","\0\0");
				strcpy(clPlfr,&clAcfr[8]);
				GetDataItem(clActo,clDrrDataArea,2,',',"","\0\0");
				strcpy(clPlto,&clActo[8]);
				GetDataItem(clStfUrno,clDrrDataArea,9,',',"","\0\0");
				GetDataItem(clPeno,clDrrDataArea,10,',',"","\0\0");
				GetDataItem(clDrrUrno,clDrrDataArea,11,',',"","\0\0");
				GetDataItem(clPlSCOD,clDrrDataArea,7,',',"","\0\0");
			 
				/* check for overtime */
				GetDataItem(clDrs1,clDrrDataArea,12,',',"","\0\0");
				GetDataItem(clDrs3,clDrrDataArea,13,',',"","\0\0");
				GetDataItem(clDrs4,clDrrDataArea,14,',',"","\0\0");

				if(IsAbsenceCode(clPlSCOD,&blExport) == RC_SUCCESS)
				{
						/* it is an absence - is it RD/OFF? */
						if (blExport == FALSE)
						{
								dbg(DEBUG,
									"don't export this record because shift <%s> is absence",clPlSCOD);
								return RC_NOTFOUND;
						}
				}

				if (*clDrs4 == '1')
				{
					dbg(DEBUG,
						"don't export this record because compl. flag is set <%s>",
									clDrs4);
					return RC_NOTFOUND;
				}


				if(*clDrs1 == '1' || *clDrs3 == '1' || *clDrs4 == '1')
				{
					dbg(DEBUG,"%05d:GetOvertime follows: <%s>, <%s,%s,%s>",__LINE__,
							clDrrDataArea,clDrs1,clDrs3,clDrs4);
					GetDataItem(clTmpStr,clDrrDataArea,8,',',"","\0\0");
					GetOvertime(clPlSCOD,clTmpStr,clPlfr,clPlto,clDrs1,clDrs3,clDrs4);
				}
				else
				{
					dbg(DEBUG,"%05d:no Overtime: <%s>, <%s,%s,%s>",__LINE__,
							clDrrDataArea,clDrs1,clDrs3,clDrs4);
				}
	
				/** now load all Arrays for this shift only */
				llRow = ARR_FIRST;
		sprintf(clDrrPlanSelection,"WHERE STFU = '%s' AND SDAY='%s' AND ROSL='1' "
				" AND DRRN='1' AND HOPO='%s'",clStfUrno,clSday,cgHopo);
		dbg(DEBUG,"ExportDailyRoster(subsequent changes): Select planned shift <%s>",
			clDrrPlanSelection);
		CEDAArrayRefillHint(&rgDrrPlanArray.rrArrayHandle,rgDrrPlanArray.crArrayName,
				clDrrPlanSelection,NULL,llDrrPlanAction,cgDrrReloadHintRosl);
		CEDAArrayGetRowCount(&rgDrrPlanArray.rrArrayHandle,
					rgDrrPlanArray.crArrayName,&llRowCount);
			dbg(DEBUG,"Planned shifts loaded: %d",llRowCount);
	
	
				llRow = ARR_FIRST;
		sprintf(clJobSelection,"WHERE UDSR = '%s' "
				" AND UJTY='%s' AND HOPO = '%s' ",
			clDrrUrno,cgBrkUjty,cgHopo);
		dbg(DEBUG,"ExportDailyRoster(subsequent changes): Select all breaks <%s>",
			clJobSelection);
		CEDAArrayRefill(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,
				clJobSelection,NULL,llJobAction);
		CEDAArrayGetRowCount(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,&llRowCount);
		dbg(DEBUG,"%d jobs loaded",llRowCount);
	
			/** all  arrays loaded, we continue normal processing */
	
				sprintf(clKey,"%s",clStfUrno);
				llRow = ARR_FIRST;
				ilRc = CEDAArrayFindRowPointer(&rgDrrPlanArray.rrArrayHandle,
					rgDrrPlanArray.crArrayName,&rgDrrPlanArray.rrIdx01Handle,
							rgDrrPlanArray.crIdx01Name,clKey,&llRow,
									(void **)&pclDrrRowBuf);
	
				if (ilRc == RC_SUCCESS)
				{
						char clTmpStr[24];
	
						strcpy(clTmpStr,FIELDVAL(rgDrrPlanArray,pclDrrRowBuf,igDrrPlanAVFR));
						strcpy(clPlfr,&clTmpStr[8]);
						strcpy(clTmpStr,FIELDVAL(rgDrrPlanArray,pclDrrRowBuf,igDrrPlanAVTO));
						strcpy(clPlto,&clTmpStr[8]);
						strcpy(clPlSCOD,FIELDVAL(rgDrrPlanArray,pclDrrRowBuf,igDrrPlanSCOD));
						strcpy(clTmpStr,FIELDVAL(rgDrrPlanArray,pclDrrRowBuf,igDrrPlanURNO));
						dbg(DEBUG,"Planned shift found: URNO <%s> SCOD <%s>,AVFR <%s>,AVTO<%s>",
								clTmpStr,clPlSCOD,clPlto,clPlfr);
				}
				else
				{
					*clPlSCOD = '\0';
					*clPlfr = '\0';
					*clPlto = '\0';
					dbg(DEBUG,"Planned shift for <%s> not found",clKey);
				}
	
				if(WriteOneShiftLine(pfpFile,clPlfr,clPlto,clPlSCOD,clPeno,clDrrDataArea) == RC_SUCCESS)
				{
					llCount++;
				}
			}
		}
		else
		{
			dbg(DEBUG,"not exported due to not in period: sday <%s> Exportperiod starts <%s>",clSday,clTimeStamp);
		}
	}
	close_my_cursor(&slCursor);
	return llCount;
}



static int ExportDailyRoster(FILE *pfpFile,char *pcpSday, char *pcpStaffUrno)
{
	int ilRc = RC_SUCCESS;
	char clSelection[262];
	char clStfUrno[12];
	char clKey[22];
	char clPlfr[24],clPlto[24],clPlSCOD[24],clPeno[36];
	char clAcfr[24],clActo[24];
	char clLocalAcfr[24];
	long llAction = ARR_FIRST;
	long llDrrPlanAction = ARR_FIRST;
	long llJobAction = ARR_FIRST;
  long llCount = 0;
  long llRowCount = 0;
  long llRow = 0;
	char clDrrDataArea[1024];
	char clDrrPlanSelection[224];
	char clJobSelection[224];
	short slFunction,slCursor;
	char *pclRowBuf;
	char *pclDrrRowBuf;
	char clTmpStr[24];
	char clDrs1[4],clDrs3[4],clDrs4[4];

	sprintf(clAcfr,"%s000000",pcpSday);
	strcpy(clLocalAcfr,clAcfr);
	sprintf(clActo,"%s235959",pcpSday);

	if(strlen(pcpStaffUrno) == 0)
	{
		sprintf(clDrrPlanSelection,"WHERE SDAY='%s' AND ROSL='1' "
				" AND DRRN='1' AND HOPO='%s'",
					pcpSday,cgHopo);
	}
	else
	{
		sprintf(clDrrPlanSelection,"WHERE SDAY='%s' AND ROSL='1' "
				" AND DRRN='1' AND HOPO='%s' AND STFU='%s'",
					pcpSday,cgHopo,pcpStaffUrno);
	}
	dbg(DEBUG,"ExportDailyRoster: Select all planned shifts <%s>",
		clDrrPlanSelection);
	CEDAArrayRefillHint(&rgDrrPlanArray.rrArrayHandle,rgDrrPlanArray.crArrayName,
			clDrrPlanSelection,NULL,llDrrPlanAction,cgDrrReloadHintRosl);

	AddSecondsToCEDATime(clActo,(time_t)86400,1);
	LocalTimeToUtcFixTZ(clAcfr);
	CEDAArrayGetRowCount(&rgDrrPlanArray.rrArrayHandle,
				rgDrrPlanArray.crArrayName,&llRowCount);
	dbg(DEBUG,"%d planned shifts loaded",llRowCount);

	if(strlen(pcpStaffUrno) == 0)
	{
		sprintf(clJobSelection,"WHERE (ACTO BETWEEN '%s' AND '%s') "
			" AND UJTY='%s' AND HOPO = '%s'",
			clAcfr,clActo,cgBrkUjty,cgHopo);
	}
	else
	{
		sprintf(clJobSelection,"WHERE (ACTO BETWEEN '%s' AND '%s') "
			" AND UJTY='%s' AND HOPO = '%s' AND USTF = '%s'",
			clAcfr,clActo,cgBrkUjty,cgHopo,pcpStaffUrno);

	}
	dbg(DEBUG,"ExportDailyRoster: Select all breaks <%s>",
		clJobSelection);
	CEDAArrayRefill(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,
			clJobSelection,NULL,llJobAction);
	CEDAArrayGetRowCount(&rgJobArray.rrArrayHandle,rgJobArray.crArrayName,&llRowCount);
	dbg(DEBUG,"%d jobs loaded",llRowCount);

	if(strlen(pcpStaffUrno) == 0)
	{
		sprintf(clSelection,"WHERE REGI IN(%s) AND (VPTO >= '%s' OR VPTO = ' ') AND "
				" VPFR <= '%s' AND SURN != ' '",cgRegi,clAcfr,clActo);
	}
	else
	{
		sprintf(clSelection,"WHERE REGI IN(%s) AND (VPTO >= '%s' OR VPTO = ' ') AND "
				" VPFR <= '%s' AND SURN = '%s'",cgRegi,clAcfr,clActo,pcpStaffUrno);
	}
	CEDAArrayRefill(&rgSreArray.rrArrayHandle,rgSreArray.crArrayName,
			clSelection,NULL,ARR_FIRST);
	CEDAArrayGetRowCount(&rgSreArray.rrArrayHandle,rgSreArray.crArrayName,&llRowCount);
	dbg(DEBUG,"%d SRETAB records loaded",llRowCount);

	/* MCU 20031022 change selection, don't export deleted shifts (SCOD empty) */
	sprintf(clSelection,"WHERE SDAY='%s' AND ROSS='A' AND DRRN='1' AND "
		" DRRTAB.SCOD != ' ' AND STFTAB.HOPO='%s' AND STFTAB.TOHR = '%s' "
			" AND (DODM >= '%s' OR DODM = ' ')"
		" AND STFU=STFTAB.URNO",
				pcpSday,cgHopo,cgTohr,clLocalAcfr);
	slFunction = START;
	slCursor = 0;
	while(DoSelect(slFunction,&slCursor,"DRRTAB,STFTAB",clSelection,
					cgDrrStfFields,clDrrDataArea) == RC_SUCCESS)
	{
		char clScod[34];
		BOOL blExport;

		slFunction = NEXT;

		GetDataItem(clScod,clDrrDataArea,7,',',"","\0\0");
		if(IsAbsenceCode(clScod,&blExport) == RC_SUCCESS)
		{
			/* it is an absence - is it RD/OFF? */
			if (blExport == FALSE)
			{
				dbg(DEBUG,"%05d:not exported because is Absence <%s>",__LINE__,clScod);
				continue;
			}
		}
		GetDataItem(clStfUrno,clDrrDataArea,9,',',"","\0\0");
		if (CheckSre(clStfUrno,pcpSday) == RC_SUCCESS)
		{
	/*DRRSTF_FIELDS "AVFR,AVTO,DRRN,SBFR,SBLU,SBTO,SCOD,SDAY,STFU,PENO,DRF.URNO,DRS1,DRS3,DRS4,DRS2,ROSL"*/
			GetDataItem(clTmpStr,clDrrDataArea,1,',',"","\0\0");
			strcpy(clPlfr,&clTmpStr[8]);
			GetDataItem(clTmpStr,clDrrDataArea,2,',',"","\0\0");
			strcpy(clPlto,&clTmpStr[8]);
			GetDataItem(clStfUrno,clDrrDataArea,9,',',"","\0\0");
			GetDataItem(clPeno,clDrrDataArea,10,',',"","\0\0");
			GetDataItem(clPlSCOD,clDrrDataArea,7,',',"","\0\0");
		 
			/* check for overtime */
			GetDataItem(clDrs1,clDrrDataArea,12,',',"","\0\0");
			GetDataItem(clDrs3,clDrrDataArea,13,',',"","\0\0");
			GetDataItem(clDrs4,clDrrDataArea,14,',',"","\0\0");


			if (*clDrs4 == '1')
			{
				dbg(DEBUG,"don't export this record because compl. flag is set <%s>"
						" STFU <%s> SDAY <%s> ",clDrs4,clStfUrno,pcpSday);
				continue;
			}

			if(*clDrs1 == '1' || *clDrs3 == '1' || *clDrs4 == '1')
			{
				dbg(DEBUG,"%05d:GetOvertime follows: <%s>, <%s,%s,%s>",__LINE__,
						clDrrDataArea,clDrs1,clDrs3,clDrs4);
				GetDataItem(clTmpStr,clDrrDataArea,8,',',"","\0\0");
				GetOvertime(clPlSCOD,clTmpStr,clPlfr,clPlto,clDrs1,clDrs3,clDrs4);
			}
			else
			{
				dbg(DEBUG,"%05d:no Overtime: <%s>, <%s,%s,%s>",__LINE__,
						clDrrDataArea,clDrs1,clDrs3,clDrs4);
			}

			sprintf(clKey,"%s",clStfUrno);
			llRow = ARR_FIRST;
			ilRc = CEDAArrayFindRowPointer(&rgDrrPlanArray.rrArrayHandle,
				rgDrrPlanArray.crArrayName,&rgDrrPlanArray.rrIdx01Handle,
						rgDrrPlanArray.crIdx01Name,clKey,&llRow,
								(void **)&pclDrrRowBuf);

			if (ilRc == RC_SUCCESS)
			{
					char clTmpStr[24];

					strcpy(clTmpStr,FIELDVAL(rgDrrPlanArray,pclDrrRowBuf,igDrrPlanAVFR));
					strcpy(clPlfr,&clTmpStr[8]);
					strcpy(clTmpStr,FIELDVAL(rgDrrPlanArray,pclDrrRowBuf,igDrrPlanAVTO));
					strcpy(clPlto,&clTmpStr[8]);
					strcpy(clPlSCOD,FIELDVAL(rgDrrPlanArray,pclDrrRowBuf,igDrrPlanSCOD));
					strcpy(clTmpStr,FIELDVAL(rgDrrPlanArray,pclDrrRowBuf,igDrrPlanURNO));
					dbg(DEBUG,"Planned shift found: URNO <%s> SCOD <%s>,AVFR <%s>,AVTO<%s>",
							clTmpStr,clPlSCOD,clPlto,clPlfr);
			}
			else
			{
				*clPlSCOD = '\0';
				*clPlfr = '\0';
				*clPlto = '\0';
				dbg(DEBUG,"Planned shift for <%s> not found",clKey);
			}

				if(WriteOneShiftLine(pfpFile,clPlfr,clPlto,clPlSCOD,clPeno,clDrrDataArea) == RC_SUCCESS)
				{
					llCount++;
				}
		}
	}	
	close_my_cursor(&slCursor);
  return llCount;
}


static int HandleLTR(char *pcpSelection)
{
	int ilRc = RC_SUCCESS;
	char clOrgUnit[24];
	char clFrom[24],clTo[24];
	char clFileName[34];
	char clFilePath[134];
	char clExportPath[1024];
	long llSequence;
	FILE *pflFile = NULL;
	char clTimeStamp[24];

	/*llSequence = GetSequence("SAPTMP",clFileMonth);*/

	/* use year, month & day instead of sequence*/
	GetServerTimeStamp("LOC", 1,0,clTimeStamp);
	clTimeStamp[8] = '\0';
	llSequence = atol(&clTimeStamp[2]); 

	dbg(DEBUG,"HandleLTR: Selection <%s>",pcpSelection);
  GetDataItem (clOrgUnit,pcpSelection, 1,',', "","\0\0") ;
  GetDataItem (clFrom,pcpSelection, 2,',', "","\0\0") ;
  GetDataItem (clTo,pcpSelection, 3,',', "","\0\0") ;

	dbg(TRACE,"Long Term Roster Export for <%s> from/to <%s/%s>",
			clOrgUnit,clFrom,clTo);
	
	MakeFileName(cgLtrFileId,llSequence,clFileName);
	sprintf(clFilePath,"%s%s%s",cgFileBase,cgTmpPath,clFileName);
	dbg(DEBUG,"%05d:Long Term Roster Export  <%s> <%s> <%s> ",__LINE__,
				cgFileBase,cgTmpPath,clFileName);

	dbg(TRACE,"Export File for long term roster export <%s> will be created",clFilePath);
	errno = 0;
	pflFile = fopen(clFilePath,"at");
	errno = 0;
	if (pflFile == NULL)
	{
		dbg(TRACE,"LongTErmRoster File <%s> not created, reason: %d %s",
			clFilePath,errno,strerror(errno));
		ilRc = RC_FAIL;
	}
	else
	{
		dbg(DEBUG,"Long term roster export file created");

		ExportLongTermRoster(pflFile,clOrgUnit,clFrom,clTo);
		fclose(pflFile);
	}

	return ilRc;
}


static int HandleSAPCommand(char *pcpCmdLine)
{
	int ilRc = RC_SUCCESS;
	char *pclSelection = NULL;

	if((pclSelection = strstr(pcpCmdLine,"[LTR]")) != NULL)
	{
		dbg(DEBUG,"HandleSAPCommand: HandleLTR follows");
		pclSelection += 6;
		HandleLTR(pclSelection);
	}
	else
	{
		dbg(TRACE,"HandleSAPCommand: invalid command line <%s>",pcpCmdLine);
		ilRc = RC_FAIL;
	}

	return ilRc;
}


static int GetItem(char* pcpTarget,char* pcpData,char* pcpFieldlist,char* pcpField,int piDeleteBlanks)
{
  int ilRc = RC_FAIL;
  int ilItemNo=0;

  ilItemNo=get_item_no(pcpFieldlist,pcpField,5);
  ilItemNo++;
  ilRc=GetDataItem(pcpTarget,pcpData,ilItemNo,',',"","");
	dbg(TRACE,"GetItem: found <%s> itemno %d RC %d",pcpTarget,ilItemNo,ilRc);
  if(piDeleteBlanks==TRUE)
    {
      DeleteCharacterInString(pcpTarget,cBLANK);
    }
  return ilRc;
}
/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(EVENT *prpEvent)
{
	int	   ilRc           = RC_SUCCESS;			/* Return code */
	int      ilCmd          = 0;

	BC_HEAD *prlBchd       = NULL;
	CMDBLK  *prlCmdblk       = NULL;
	char    *pclSelection    = NULL;
	char    *pclFields       = NULL;
	char    *pclData         = NULL;
	char    *pclRow          = NULL;
	char 	clTable[34];
	char    clTmpJob[12];
	char    clUrnoList[50000];
	char	*pclKeyPtr		= NULL;
	char	clPeno[20]	= "\0";
	char	clStaffUrno[20]	= "\0";
	char	clTime[20]	= "\0";
	long	llLen = 0;


	prlBchd    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
	prlCmdblk    = (CMDBLK *)  ((char *)prlBchd->data);
	pclSelection = prlCmdblk->data;
	pclFields    = pclSelection + strlen(pclSelection) + 1;
	pclData      = pclFields + strlen(pclFields) + 1;

	strcpy(clTable,prlCmdblk->obj_name);

	dbg(TRACE,"==========START <%10.10d> ==========",lgEvtCnt);

  igQueOut = prgEvent->originator;

  /* Save Global Elements of CMDBLK */
  strcpy(cgTwStart,prlCmdblk->tw_start);
  strcpy(cgTwEnd,prlCmdblk->tw_end);
  /* Save Global Elements of BC_HEAD */
  strcpy(cgRecvName,prlBchd->recv_name);
  strcpy(cgDestName,prlBchd->dest_name);
 
	/****************************************/
	DebugPrintBchead(DEBUG,prlBchd);
	DebugPrintCmdblk(DEBUG,prlCmdblk);
  dbg(DEBUG,"TwStart   <%s>",cgTwStart);
  dbg(DEBUG,"TwEnd     <%s>",cgTwEnd);
	dbg(DEBUG,"originator<%d>",prpEvent->originator);
	dbg(DEBUG,"selection <%s>",pclSelection);
	dbg(DEBUG,"fields    <%s>",pclFields);
	dbg(DEBUG,"data      <%s>",pclData);
	/****************************************/

  
	memset(pcgStfuList,0,lgStfuListLength);
	lgStfuListActualLength = 0;

	ilRc = GetCommand(&prlCmdblk->command[0],&ilCmd);
	dbg(DEBUG,"RC from GetCommand(%s) is %d",&ilCmd,ilRc);

	if (ilCmd != CMD_URT && ilCmd != CMD_DRT)
	{ /* delete old data from pclData */
		char *pclNewline = NULL;

		if ((pclNewline = strchr(pclData,'\n')) != NULL)
		{
			*pclNewline = '\0';
		}
	}

	if(ilRc == RC_SUCCESS)
	{
		switch (ilCmd)
		{
		case CMD_URT:
				dbg(TRACE,"Command is URT");
				if (strcmp(clTable,"DRRTAB") == 0)
				{
					char clDrrUrno[24];
					char clScod[24];
					char clSday[24];
					char *pclOldData;
					char clDrs2[12];
					char clOldDrs2[12];
					BOOL blDrrRelevantChange = TRUE;

					if ((pclOldData = strchr(pclData,'\n')) != NULL)
					{
						*pclOldData = '\0';
						pclOldData++;

						/* subsequent changes are only sent on change of shift code  and SUB1/2 indicator(RFC 25 Rev. 2.0)*/
						/*blDrrRelevantChange = RelevantChange(pclData,pclOldData,pclFields,"AVFR,AVTO,DRS2,SCOD,SBLU");*/
						blDrrRelevantChange = RelevantChange(pclData,pclOldData,pclFields,"DRS2,SCOD");
					}
					else
					{
						/*blDrrRelevantChange = RelevantFields(pclFields,"AVFR,AVTO,DRS2,SCOD,SBLU");*/
						/* subsequent changes are only sent on change of shift code and SUB1/2 indicator (RFC 25 Rev. 2.0)*/
						blDrrRelevantChange = RelevantFields(pclFields,"DRS2,SCOD");
					}

					if (blDrrRelevantChange == TRUE)
					{
						BOOL blExport;
						ilRc = GetItem(clDrrUrno,pclData,pclFields,"URNO",TRUE);
						if (ilRc > 0)
						{			
							ilRc = GetItem(clScod,pclData,pclFields,"SCOD",TRUE);
							if((ilRc > 0) && (IsAbsenceCode(clScod,&blExport) != RC_SUCCESS))
							{
								ExportSubsequentChanges(clDrrUrno);
							}
							else
							{
								if ((ilRc > 0) && (blExport == TRUE))
								{
									ExportSubsequentChanges(clDrrUrno);
								}
								else
								{
									dbg(DEBUG,"%0d:not exported because is Absence <%s>",__LINE__,clScod);
								}
							}
						}
						else 
						{ 
							dbg(TRACE,"no URNO in URT Event");
						}
					}
				}

				if (strcmp(clTable,"JOBTAB") == 0)
				{
					char clJobUjty[12];
					char *pclOldData;
					BOOL blJobChanged = FALSE;

					/* check if break job was changed */
					ilRc = GetItem(clJobUjty,pclData,pclFields,"UJTY",TRUE);
					if (ilRc > 0 && !strcmp(clJobUjty,cgBrkUjty))
					{
						/* check if break duration changed */
						if ((pclOldData = strchr(pclData,'\n')) != NULL)
						{
							*pclOldData = '\0';
							pclOldData++;

							if (RelevantChangeJob(pclData,pclOldData,pclFields) == TRUE)
							{
								blJobChanged = TRUE;
								dbg(DEBUG,"Job break duration changed");
							}
							else
							{
								blJobChanged = FALSE;
								dbg(DEBUG,"Job break duration not changed");
							}
						}
					}
					else
					{
						dbg(DEBUG,"JOBTAB-change was no break job or has no UJTY in field-/datalist!");
					}

					if (blJobChanged == TRUE)
					{
						char clJobUdsr[12];
						ilRc = GetItem(clJobUdsr,pclData,pclFields,"UDSR",TRUE);
						if (ilRc > 0)
						{
							ExportSubsequentChanges(clJobUdsr);
						}
						else
						{
							dbg(DEBUG,"JOBTAB-change has no UDSR in field-/datalist!");
						}
					}
				}
				
				if (strcmp(clTable,"STFTAB") == 0)
				{
					char *pclOldData;
					char clTohrOld[10];
					char clTohrNew[10];

					DebugPrintEvent(TRACE,prgEvent);
           /* make only EventUpdate */
					ilRc = CEDAArrayEventUpdate2(prgEvent,clUrnoList,clTable);

#if 0
/* we don't need this anymore because all records from stftab are loaded */
					if ((pclOldData = strchr(pclData,'\n')) != NULL)
					{
						int ilStfTohr;
						int ilCntFields;

						*pclOldData = '\0';
						pclOldData++;

						ilStfTohr = get_item_no(pclFields,"TOHR",4) + 1;
						ilCntFields = field_count(pclFields);

						if (ilStfTohr >= 0 && ilCntFields > 2)
						{
							get_real_item(clTohrNew, pclData, ilStfTohr);
							get_real_item(clTohrOld, pclOldData , ilStfTohr);

							if (strcmp(clTohrNew,clTohrOld) != NULL)
							{
								if (strstr(cgTohr,clTohrNew) != NULL)
								{
									/* update is like a insert, because STF.TOHR is now correct */
									strcpy(prlCmdblk->command,"IRT");
									ilRc = CEDAArrayEventUpdate2(prgEvent,clUrnoList,clTable);
									dbg(TRACE,"Command is URT resp. IRT: RC from EventUpdate=%d",ilRc);
								}
								else
								{
									/* update is like a delete, because STF.TOHR now is not correct */
									int ilStfUrno;
									char clStfUrno[12];

									ilStfUrno = get_item_no(pclFields,"URNO",4) + 1;
									if (ilStfUrno >= 0)
									{
										get_real_item(clStfUrno, pclData, ilStfUrno);
										if (DeleteStfRow(clStfUrno) != TRUE)
										{
											dbg(TRACE,"Failed to delete STF-record!");
										}
									}
								}
							}
						}
					}
#endif
				}
				break;
		case CMD_IRT:
				if (strcmp(clTable,"STFTAB") == 0)
				{
					ilRc = CEDAArrayEventUpdate2(prgEvent,clUrnoList,clTable);
				}
				break;
	case CMD_DRT :
				dbg(TRACE,"Command is URT");
				if (strcmp(clTable,"JOBTAB") == 0)
				{
					/* process only changes not coming from polhdl */
					if (strstr(cgTwEnd,"polhdl") == NULL)
					{
						char *pclOldData;
						/* we got a delete, so only the old data are relevant */
						if ((pclOldData = strchr(pclData,'\n')) != NULL)
						{
							char clJobUjty[12];

							*pclOldData = '\0';
							pclOldData++;

							/* check if a break job was deleted */
							ilRc = GetItem(clJobUjty,pclOldData,pclFields,"UJTY",TRUE);
							if (ilRc > 0 && !strcmp(clJobUjty,cgBrkUjty))
							{
								char clJobUdsr[12];
								ilRc = GetItem(clJobUdsr,pclOldData,pclFields,"UDSR",TRUE);
								if (ilRc > 0)
								{
									ExportSubsequentChanges(clJobUdsr);
								}
								else
								{
									dbg(DEBUG,"JOBTAB-delete has no UDSR in old field-/datalist!");
								}
							}
							else
							{
								dbg(DEBUG,"JOBTAB-delete was no break job or has no UJTY in old field-/datalist!");
							}
						}
						else
						{
							dbg(DEBUG,"JOBTAB-delete contains no old datalist!");
						}
					}
					else
					{
						dbg(TRACE,"JOBTAB-delete was from polhdl -> not processed.");
					}
				}
				break;
   	case CMD_SAP :
				dbg(TRACE,"Command is SAP");
				SendAnswer(1900, ""); 
				HandleSAPCommand(pclSelection);
				break;
   	case CMD_DRD :
				dbg(TRACE,"Command is DRD");
				pclKeyPtr = CedaGetKeyItem(clPeno,&llLen,pclSelection,"<PENO>","<\\PENO>", TRUE);
				pclKeyPtr = CedaGetKeyItem(clTime,&llLen,pclSelection,"<TIME>","<\\TIME>", TRUE);
				dbg(TRACE,"PENO:       <%s>",clPeno);
				ilRc = GetStaffUrno(&clStaffUrno,clPeno);
				dbg(TRACE,"STAFF_URNO: <%s>",clStaffUrno);
				dbg(TRACE,"TIME:       <%s>",clTime);
				DailyRosterExport(clStaffUrno,clTime);
			 break;
		case CMD_LTR :
				dbg(TRACE,"Command is LTR");
				LongTermRosterExport();
				break;
		case CMD_EMP :
				dbg(TRACE,"Command is EMP");
			  EmployeeMasterImport();
				break;
		case CMD_ABS:
				dbg(TRACE,"Command is ABS");
			  AbsencesImport();
				break;
		case CMD_ATT:
				dbg(TRACE,"Command is ATT");
			  AttendancesImport();
				break;
		default:
			  dbg(TRACE,"unknown command <%s>",prlCmdblk->command);
		}
	}

	dbg(TRACE,"==========END  <%10.10d> ==========",lgEvtCnt);

	return(RC_SUCCESS);
	
} /* end of HandleData */


static int StrToTime(char *pcpTime,time_t *plpTime)
{
	struct tm *_tm;
	time_t now;
	char   _tmpc[6];
	long llUtcDiff;

	dbg(0,"StrToTime, <%s>",pcpTime);
	if (strlen(pcpTime) < 12 )
	{
		*plpTime = time(0L);
		return RC_FAIL;
	} /* end if */




	now = time(0L);
	_tm = (struct tm *)gmtime(&now);

	_tmpc[2] = '\0';
/*
	strncpy(_tmpc,pcpTime+12,2);
	_tm -> tm_sec = atoi(_tmpc);
*/
	_tm -> tm_sec = 0;
	
	strncpy(_tmpc,pcpTime+10,2);
	_tm -> tm_min = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+8,2);
	_tm -> tm_hour = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+6,2);
	_tm -> tm_mday = atoi(_tmpc);
	strncpy(_tmpc,pcpTime+4,2);
	_tm -> tm_mon = atoi(_tmpc)-1;
	strncpy(_tmpc,pcpTime,4);
	_tmpc[4] = '\0';
	_tm ->tm_year = atoi(_tmpc)-1900;
	_tm ->tm_wday = 0;
	_tm ->tm_yday = 0;
	_tm->tm_isdst = -1; /* no adjustment of daylight saving time */
	now = mktime(_tm);
	dbg(0,"StrToTime: %02d.%02d.%04d %02d:%02d Now: %ld",_tm->tm_mday,
_tm->tm_mon+1,_tm->tm_year+1900,_tm->tm_hour,_tm->tm_min,now);
	if (now != (time_t) -1)
	{
	{
		time_t utc,local;

	_tm = (struct tm *)gmtime(&now);
	utc = mktime(_tm);
	_tm = (struct tm *)localtime(&now);
	local = mktime(_tm);
	dbg(TRACE,"utc %ld, local %ld, utc-diff %ld",
		utc,local,local-utc);
		llUtcDiff = local-utc;
	}
		now +=  + llUtcDiff;
		*plpTime = now;
		return RC_SUCCESS;
	}
	*plpTime = time(NULL);
	return RC_FAIL;
}


/**
dbg(TRACE,"%05d:",__LINE__);
***/



/*
 *     struct  tm {
 *             int     tm_sec;    seconds after the minute -- [0, 59]  
 *             int     tm_min;    minutes after the hour -- [0, 59] 
 *             int     tm_hour;   hour since midnight -- [0, 23] 
 *             int     tm_mday;   day of the month -- [1, 31] 
 *             int     tm_mon;    months since January -- [0, 11] 
 *             int     tm_year;   years since 1900 
 *             int     tm_wday;   days since Sunday -- [0, 6] 
 *             int     tm_yday;   days since January 1 -- [0, 365] 
 *             int     tm_isdst;  flag for daylight savings time 
 *             long    tm_tzadj;  seconds from GMT (east < 0) 
 *             char    tm_name[LTZNMAX];   name of timezone 
 *     };
 */




void	StrAddTime(char *pcpBase,char *pcpOffs,char *pcpDest)
{

	if (pcpBase != pcpDest)
	{
		strcpy(pcpDest,pcpBase);
	}
	AddSecondsToCEDATime(pcpDest,(time_t)atol(pcpOffs),1);

} /* end of StrAddTime */



int check_ret1(int ipRc,int ipLine)
{
	int ilOldDebugLevel;

	ilOldDebugLevel = debug_level;

	debug_level = TRACE;

	dbg(TRACE,"DB-ERROR in Line: %d",ipLine);
	debug_level  = ilOldDebugLevel;
	return check_ret(ipRc);
}

static void HandleQueues()
{
	int	ilRc = RC_SUCCESS;			/* Return code */
	int	ilBreakOut = FALSE;
	
	do
	{
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;	

		if( ilRc == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRc != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRc);
			} /* fi */
		
			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				ilBreakOut = TRUE;
				break;	

			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate(1);
				break;	
	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				ilBreakOut = TRUE;
				break;	

			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;

			case	SHUTDOWN	:
				Terminate(1);
				break;
						
			case	RESET		:
				ilRc = Reset();
				break;
						
			case	EVENT_DATA	:
				dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
					
			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;

			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;

			default			:
				dbg(TRACE,"HandleQueues: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		} /* end else */
	} while (ilBreakOut == FALSE);

	if(igInitOK == FALSE)
	{
			ilRc = InitSaphdl();
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitSaphdl failed!");
			} /* end of if */
	}/* end of if */

}

static int TriggerAction(char *pcpTableName,char *pcpFields)
{
  int           ilRc          = RC_SUCCESS ;    /* Return code */
  EVENT        *prlEvent      = NULL ;
  BC_HEAD      *prlBchead     = NULL ;
  CMDBLK       *prlCmdblk     = NULL ;
  /*ACTIONConfig *prlAction     = NULL ;*/
	ACTIONConfig rlAction;
  char         *pclSelection  = NULL ;
  char         *pclFields     = NULL ;
  char         *pclData       = NULL ;
  long          llActSize     = 0 ;
  int           ilAnswerQueue = 0 ;
  char          clQueueName[16] ;
	
  if(ilRc == RC_SUCCESS)
  {

  }/* end of if */
	

  if (ilRc == RC_SUCCESS)
  {
   sprintf (&clQueueName[0],"%s2", mod_name) ;

   ilRc = GetDynamicQueue(&ilAnswerQueue,&clQueueName[0]);
   if (ilRc != RC_SUCCESS)
   {
    dbg(TRACE,"TriggerAction: GetDynamicQueue failed <%d>",ilRc);
   }
   else
   {
    dbg(DEBUG,"TriggerAction: got <%d> for <%s>",ilAnswerQueue, &clQueueName[0]);
   }/* end of if */
  }/* end of if */
	
  if (ilRc == RC_SUCCESS)
  {
   llActSize = sizeof(EVENT) + sizeof(BC_HEAD) + 
	 		sizeof(CMDBLK) + sizeof(ACTIONConfig) + strlen(pcpFields) + 666;
   prgOutEvent = realloc(prgOutEvent,llActSize);
   if(prgOutEvent == NULL)
   {
    dbg(TRACE,"TriggerAction: realloc out event <%d> bytes failed",llActSize);
    ilRc = RC_FAIL ;
   } /* end of if */
  } /* end of if */

  if(ilRc == RC_SUCCESS)
  {
   prlBchead     = (BC_HEAD *) ((char *) prgOutEvent + sizeof(EVENT)) ;
   prlCmdblk     = (CMDBLK *)  ((char *) prlBchead->data) ;
   pclSelection  = prlCmdblk->data ;
   pclFields     = pclSelection + strlen (pclSelection) + 1 ;
	 strcpy(pclFields,pcpFields);
   pclData       = pclFields + strlen (pclFields) + 1 ;
   strcpy (pclData, "DYN") ;
	memset(&rlAction,0,sizeof(ACTIONConfig));
   /*prlAction     = (ACTIONConfig *) (pclData + strlen(pclData) + 1) ;*/
	
	prlBchead->rc = RC_SUCCESS;
	
   prgOutEvent->type        = USR_EVENT ;
   prgOutEvent->command     = EVENT_DATA ;
   prgOutEvent->originator  = ilAnswerQueue ;
   prgOutEvent->retry_count = 0 ;
   prgOutEvent->data_offset = sizeof(EVENT) ;
   prgOutEvent->data_length = llActSize-sizeof(EVENT) ;

	rlAction.iEmptyFieldHandling = iDELETE_ALL_BLANKS ; 
	rlAction.iIgnoreEmptyFields = 0 ; 
	strcpy(rlAction.pcSndCmd, "") ; 
	sprintf(rlAction.pcSectionName,"%s_%s", mod_name,pcpTableName) ; 
	strcpy(rlAction.pcTableName, pcpTableName) ; 
	strcpy(rlAction.pcFields, pcpFields) ; 
  strcpy(rlAction.pcSectionCommands, "IRT,URT,DRT") ;
	rlAction.iModID = mod_id ;   

	 /***
   prlAction->iEmptyFieldHandling = iDELETE_ALL_BLANKS ;
   prlAction->iIgnoreEmptyFields = 0 ;
   strcpy(prlAction->pcSndCmd, "") ;
   sprintf(prlAction->pcSectionName,"%s_%s", mod_name,pcpTableName) ;
   strcpy(prlAction->pcTableName, pcpTableName) ;
   strcpy(prlAction->pcFields, pcpFields) ;
   strcpy(prlAction->pcSectionCommands, "IRT,URT,DRT") ;
   prlAction->iModID = mod_id ;
	 ***/
	 /*********************** MCU TEST **************
   strcpy(prlAction->pcFields, "-") ;
   strcpy(prlAction->pcFields, "") ;
	 *********************** MCU TEST ***************/
	 /***********
	 if (!strcmp(pcpTableName,"SPRTAB"))
		{
   	strcpy(prlAction->pcFields, "PKNO,USTF,ACTI,FCOL") ;
	 }
	 ************/

   /****************************************
     DebugPrintEvent(DEBUG,prgOutEvent) ;
     DebugPrintBchead(DEBUG,prlBchead) ;
     DebugPrintCmdblk(DEBUG,prlCmdblk) ;
     dbg (DEBUG,"TriggerAction: selection <%s>",pclSelection) ;
     dbg (DEBUG,"TriggerAction: fields    <%s>",pclFields) ;
     dbg (DEBUG,"TriggerAction: data      <%s>",pclData) ;
     DebugPrintACTIONConfig(DEBUG,prlAction) ;
   ****************************************/

   if(ilRc == RC_SUCCESS)
   {
    rlAction.iADFlag = iDELETE_SECTION ;
	 memcpy(pclData + strlen(pclData) + 1,&rlAction,sizeof(ACTIONConfig));

    ilRc = que(QUE_PUT, 7400, ilAnswerQueue, PRIORITY_2, llActSize, (char *) prgOutEvent) ; 
    if(ilRc != RC_SUCCESS)
    {
     dbg(TRACE,"TriggerAction: QUE_PUT failed <%d>",ilRc) ;
    }
    else
    {
     ilRc = WaitAndCheckQueue(100, ilAnswerQueue,&prgItem) ;
     if(ilRc != RC_SUCCESS)
     {
      dbg(TRACE,"TriggerAction: WaitAndCheckQueue failed <%d>",ilRc) ;
     }
     else
     {
      /* return value for delete does not matter !!! */
		
      /****************************************
      prlEvent     = (EVENT *)   ((char *)prgItem->text) ;
      prlBchead    = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT)) ;
      prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data) ;
      pclSelection = (char *)    prlCmdblk->data ;
      pclFields    = (char *)    pclSelection + strlen(pclSelection) + 1;
      pclData      = (char *)    pclFields + strlen(pclFields) + 1;
      DebugPrintItem(DEBUG, prgItem) ;
      DebugPrintEvent(DEBUG, prlEvent) ;
      DebugPrintBchead(DEBUG, prlBchead) ;
      DebugPrintCmdblk(DEBUG, prlCmdblk) ;
      dbg(DEBUG,"TriggerAction: selection <%s>",pclSelection) ;
      dbg(DEBUG,"TriggerAction: fields    <%s>",pclFields) ;
      dbg(DEBUG,"TriggerAction: data      <%s>",pclData) ;
      ****************************************/
    } /* end of if */
   }/* end of if */
  }/* end of if */

  if(ilRc == RC_SUCCESS)
  {
   rlAction.iADFlag = iADD_SECTION ;
	 memcpy(pclData + strlen(pclData) + 1,&rlAction,sizeof(ACTIONConfig));

   ilRc = que(QUE_PUT, 7400, ilAnswerQueue, PRIORITY_3, llActSize,
                                              (char *) prgOutEvent ) ;
   if(ilRc != RC_SUCCESS)
   {
    dbg(TRACE,"TriggerAction: QUE_PUT failed <%d>",ilRc) ;
   }
   else
   {
    ilRc = WaitAndCheckQueue(100,ilAnswerQueue,&prgItem) ;
    if(ilRc != RC_SUCCESS)
    {
     dbg(TRACE,"TriggerAction: WaitAndCheckQueue failed <%d>",ilRc) ;
    }
    else
    {
     prlEvent     = (EVENT *)   ((char *)prgItem->text) ;
     prlBchead    = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT)) ;
     prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data) ;
     pclSelection = (char *)    prlCmdblk->data ;
     pclFields    = (char *)    pclSelection + strlen(pclSelection) + 1 ;
     pclData      = (char *)    pclFields + strlen(pclFields) + 1 ;

     if(strcmp(pclData,"SUCCESS") != 0)
     {
      dbg(TRACE,"TriggerAction: add dynamic action config failed") ;
      DebugPrintItem(DEBUG,prgItem) ;
      DebugPrintEvent(DEBUG,prlEvent) ;
      DebugPrintBchead(DEBUG,prlBchead) ;
      DebugPrintCmdblk(DEBUG,prlCmdblk) ;
      dbg (DEBUG,"TriggerAction: selection <%s>",pclSelection) ;
      dbg (DEBUG,"TriggerAction: fields    <%s>",pclFields) ;
      dbg(DEBUG,"TriggerAction: data      <%s>",pclData) ;
      ilRc = RC_FAIL ;
     }
     else
     {
      dbg(DEBUG,"TriggerAction: add dynamic action config OK") ;
     }/* end of if */
    }/* end of if */
   }/* end of if */
  }/* end of if */

  ilRc = que(QUE_DELETE,ilAnswerQueue,ilAnswerQueue,0,0,0) ;
  if(ilRc != RC_SUCCESS)
  {
   dbg(TRACE,"que QUE_DELETE <%d> failed <%d>",ilAnswerQueue, ilRc) ;
  }
  else
  {
   dbg(DEBUG,"TriggerAction: queue <%d> <%s> deleted", ilAnswerQueue, &clQueueName[0]) ;
  }/* end of if */
 }/* end of if */

 return (ilRc) ;
	
}


static int TriggerActionSAP(char *pcpTableName)
{
  int           ilRc          = RC_SUCCESS ;    /* Return code */
  EVENT        *prlEvent      = NULL ;
  BC_HEAD      *prlBchead     = NULL ;
  CMDBLK       *prlCmdblk     = NULL ;
  ACTIONConfig *prlAction     = NULL ;
  char         *pclSelection  = NULL ;
  char         *pclFields     = NULL ;
  char         *pclData       = NULL ;
  long          llActSize     = 0 ;
  int           ilAnswerQueue = 0 ;
  char          clQueueName[16] ;
	
  if(ilRc == RC_SUCCESS)
  {

  }/* end of if */
	

  if (ilRc == RC_SUCCESS)
  {
   sprintf (&clQueueName[0],"%s2", mod_name) ;

   ilRc = GetDynamicQueue(&ilAnswerQueue,&clQueueName[0]);
   if (ilRc != RC_SUCCESS)
   {
    dbg(TRACE,"TriggerAction: GetDynamicQueue failed <%d>",ilRc);
   }
   else
   {
    dbg(DEBUG,"TriggerAction: got <%d> for <%s>",ilAnswerQueue, &clQueueName[0]);
   }/* end of if */
  }/* end of if */
	
  if (ilRc == RC_SUCCESS)
  {
   llActSize = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + sizeof(ACTIONConfig) + 666;
   prgOutEvent = realloc(prgOutEvent,llActSize);
   if(prgOutEvent == NULL)
   {
    dbg(TRACE,"TriggerAction: realloc out event <%d> bytes failed",llActSize);
    ilRc = RC_FAIL ;
   } /* end of if */
  } /* end of if */

  if(ilRc == RC_SUCCESS)
  {
   prlBchead     = (BC_HEAD *) ((char *) prgOutEvent + sizeof(EVENT)) ;
   prlCmdblk     = (CMDBLK *)  ((char *) prlBchead->data) ;
   pclSelection  = prlCmdblk->data ;
   pclFields     = pclSelection + strlen (pclSelection) + 1 ;
   pclData       = pclFields + strlen (pclFields) + 1 ;
   strcpy (pclData, "DYN") ;
   prlAction     = (ACTIONConfig *) (pclData + strlen(pclData) + 1) ;
	
	prlBchead->rc = RC_SUCCESS;
	
   prgOutEvent->type        = USR_EVENT ;
   prgOutEvent->command     = EVENT_DATA ;
   prgOutEvent->originator  = ilAnswerQueue ;
   prgOutEvent->retry_count = 0 ;
   prgOutEvent->data_offset = sizeof(EVENT) ;
   prgOutEvent->data_length = llActSize-sizeof(EVENT) ;

   prlAction->iEmptyFieldHandling = iDELETE_ALL_BLANKS ;
   prlAction->iIgnoreEmptyFields = 0 ;
   strcpy(prlAction->pcSndCmd, "") ;
   sprintf(prlAction->pcSectionName,"%s_%s", mod_name,pcpTableName) ;
   strcpy(prlAction->pcTableName, pcpTableName) ;
   strcpy(prlAction->pcFields, "-") ;
	 /*********************** MCU TEST ***************/
   strcpy(prlAction->pcFields, "") ;
	 /*********************** MCU TEST ***************/
	 /************/
	 if (!strcmp(pcpTableName,"SPRTAB"))
		{
   	strcpy(prlAction->pcFields, "PKNO,USTF,ACTI,FCOL") ;
	 }
	 /************/
   strcpy(prlAction->pcSectionCommands, "IRT,URT,DRT") ;
   prlAction->iModID = mod_id ;

   /****************************************
     DebugPrintEvent(DEBUG,prgOutEvent) ;
     DebugPrintBchead(DEBUG,prlBchead) ;
     DebugPrintCmdblk(DEBUG,prlCmdblk) ;
     dbg (DEBUG,"TriggerAction: selection <%s>",pclSelection) ;
     dbg (DEBUG,"TriggerAction: fields    <%s>",pclFields) ;
     dbg (DEBUG,"TriggerAction: data      <%s>",pclData) ;
     DebugPrintACTIONConfig(DEBUG,prlAction) ;
   ****************************************/

   if(ilRc == RC_SUCCESS)
   {
    prlAction->iADFlag = iDELETE_SECTION ;

    ilRc = que(QUE_PUT, 7400, ilAnswerQueue, PRIORITY_3, llActSize, (char *) prgOutEvent) ; 
    if(ilRc != RC_SUCCESS)
    {
     dbg(TRACE,"TriggerAction: QUE_PUT failed <%d>",ilRc) ;
    }
    else
    {
     ilRc = WaitAndCheckQueue(10, ilAnswerQueue,&prgItem) ;
     if(ilRc != RC_SUCCESS)
     {
      dbg(TRACE,"TriggerAction: WaitAndCheckQueue failed <%d>",ilRc) ;
     }
     else
     {
      /* return value for delete does not matter !!! */
		
      /****************************************
      prlEvent     = (EVENT *)   ((char *)prgItem->text) ;
      prlBchead    = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT)) ;
      prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data) ;
      pclSelection = (char *)    prlCmdblk->data ;
      pclFields    = (char *)    pclSelection + strlen(pclSelection) + 1;
      pclData      = (char *)    pclFields + strlen(pclFields) + 1;
      DebugPrintItem(DEBUG, prgItem) ;
      DebugPrintEvent(DEBUG, prlEvent) ;
      DebugPrintBchead(DEBUG, prlBchead) ;
      DebugPrintCmdblk(DEBUG, prlCmdblk) ;
      dbg(DEBUG,"TriggerAction: selection <%s>",pclSelection) ;
      dbg(DEBUG,"TriggerAction: fields    <%s>",pclFields) ;
      dbg(DEBUG,"TriggerAction: data      <%s>",pclData) ;
      ****************************************/
    } /* end of if */
   }/* end of if */
  }/* end of if */

  if(ilRc == RC_SUCCESS)
  {
   prlAction->iADFlag = iADD_SECTION ;

   ilRc = que(QUE_PUT, 7400, ilAnswerQueue, PRIORITY_3, llActSize,
                                              (char *) prgOutEvent ) ;
   if(ilRc != RC_SUCCESS)
   {
    dbg(TRACE,"TriggerAction: QUE_PUT failed <%d>",ilRc) ;
   }
   else
   {
    ilRc = WaitAndCheckQueue(10,ilAnswerQueue,&prgItem) ;
    if(ilRc != RC_SUCCESS)
    {
     dbg(TRACE,"TriggerAction: WaitAndCheckQueue failed <%d>",ilRc) ;
    }
    else
    {
     prlEvent     = (EVENT *)   ((char *)prgItem->text) ;
     prlBchead    = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT)) ;
     prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data) ;
     pclSelection = (char *)    prlCmdblk->data ;
     pclFields    = (char *)    pclSelection + strlen(pclSelection) + 1 ;
     pclData      = (char *)    pclFields + strlen(pclFields) + 1 ;

     if(strcmp(pclData,"SUCCESS") != 0)
     {
      dbg(TRACE,"TriggerAction: add dynamic action config failed") ;
      DebugPrintItem(DEBUG,prgItem) ;
      DebugPrintEvent(DEBUG,prlEvent) ;
      DebugPrintBchead(DEBUG,prlBchead) ;
      DebugPrintCmdblk(DEBUG,prlCmdblk) ;
      dbg (DEBUG,"TriggerAction: selection <%s>",pclSelection) ;
      dbg (DEBUG,"TriggerAction: fields    <%s>",pclFields) ;
      dbg(DEBUG,"TriggerAction: data      <%s>",pclData) ;
      ilRc = RC_FAIL ;
     }
     else
     {
      dbg(DEBUG,"TriggerAction: add dynamic action config OK") ;
     }/* end of if */
    }/* end of if */
   }/* end of if */
  }/* end of if */

  ilRc = que(QUE_DELETE,ilAnswerQueue,ilAnswerQueue,0,0,0) ;
  if(ilRc != RC_SUCCESS)
  {
   dbg(TRACE,"que QUE_DELETE <%d> failed <%d>",ilAnswerQueue, ilRc) ;
  }
  else
  {
   dbg(DEBUG,"TriggerAction: queue <%d> <%s> deleted", ilAnswerQueue, &clQueueName[0]) ;
  }/* end of if */
 }/* end of if */

 return (ilRc) ;
	
}
static int WaitAndCheckQueue(int ipTimeout, int ipModId, ITEM **prpItem)
{
	int ilRc          = RC_SUCCESS;                      /* Return code */
	int ilWaitCounter = 0;

	do
	{
		sleep(1);
		ilWaitCounter += 1;

		ilRc = CheckQueue(ipModId,prpItem);
		if(ilRc != RC_SUCCESS)
		{
			if(ilRc != QUE_E_NOMSG)
			{
				dbg(TRACE,"WaitAndCheckQueue: CheckQueue <%d> failed <%d>",mod_id,ilRc);
			}/* end of if */
		}/* end of if */
	}while((ilRc == QUE_E_NOMSG) && (ilWaitCounter < ipTimeout));

	if(ilWaitCounter >= ipTimeout)
	{
		dbg(TRACE,"WaitAndCheckQueue: timeout reached <%d>",ipTimeout);
		ilRc = RC_FAIL;
	}/* end of if */

	return ilRc;
}


static int CheckQueue(int ipModId, ITEM **prpItem)
{
	int     ilRc       = RC_SUCCESS;                      /* Return code */
	int     ilItemSize = 0;
	EVENT *prlEvent    = NULL;
static	ITEM  *prlItem      = NULL;
	
	ilItemSize = I_SIZE;
	
	ilRc = que(QUE_GETBIGNW,ipModId,ipModId,PRIORITY_3,ilItemSize,(char *) &prlItem);
	if(ilRc == RC_SUCCESS)
	{
		dbg(TRACE,"DebugPrintItem with prlItem follows");
		DebugPrintItem(TRACE,prlItem);
		*prpItem = prlItem;
		dbg(TRACE,"DebugPrintItem with prpItem follows");
		DebugPrintItem(TRACE,*prpItem);
		prlEvent = (EVENT*) ((prlItem)->text);

		switch( prlEvent->command )
		{
			case    HSB_STANDBY :
				dbg(TRACE,"CheckQueue: HSB_STANDBY");
				HandleQueues();
				ctrl_sta = prlEvent->command;
				break;
	
			case    HSB_COMING_UP   :
				dbg(TRACE,"CheckQueue: HSB_COMING_UP");
				ctrl_sta = prlEvent->command;
				break;
	
			case    HSB_ACTIVE  :
				dbg(TRACE,"CheckQueue: HSB_ACTIVE");
				ctrl_sta = prlEvent->command;
				break;
	
			case    HSB_ACT_TO_SBY  :
				dbg(TRACE,"CheckQueue: HSB_ACT_TO_SBY");
				HandleQueues();
				ctrl_sta = prlEvent->command;
				break;
	
			case    HSB_DOWN    :
				dbg(TRACE,"CheckQueue: HSB_DOWN");
				ctrl_sta = prlEvent->command;
				/* Acknowledge the item 
				ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
				if( ilRc != RC_SUCCESS )
				{
					HandleQueErr(ilRc);
				}  */
				Terminate(30);
				break;
		
		case    HSB_STANDALONE  :
				dbg(TRACE,"CheckQueue: HSB_STANDALONE");
				ctrl_sta = prlEvent->command;
				break;
	
			case    SHUTDOWN    :
				dbg(TRACE,"CheckQueue: SHUTDOWN");
				/* Acknowledge the item */
				ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
				if( ilRc != RC_SUCCESS )
				{
					/* handle que_ack error */
					HandleQueErr(ilRc);
				} /* fi */
				Terminate(30);
				break;
	
			case    RESET       :
				ilRc = Reset();
				break;
	
			case    EVENT_DATA  :
				/* DebugPrintItem(DEBUG,prgItem); */
				/* DebugPrintEvent(DEBUG,prlEvent); */
				break;
	
			case    TRACE_ON :
				dbg_handle_debug(prlEvent->command);
				break;
	
			case    TRACE_OFF :
				dbg_handle_debug(prlEvent->command);
				break;


			default         :
				dbg(TRACE,"CheckQueue: unknown event");
				DebugPrintItem(TRACE,prlItem);
				DebugPrintEvent(TRACE,prlEvent);
				break;
	
		} /* end switch */
	
		/* Acknowledge the item */
		ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
		if( ilRc != RC_SUCCESS )
		{
			/* handle que_ack error */
			HandleQueErr(ilRc);
		} /* fi */
	}else{
		if(ilRc != QUE_E_NOMSG)
		{
			dbg(TRACE,"CheckQueue: que(QUE_GETBIG,%d,%d,...) failed <%d>",ipModId,ipModId,ilRc);
			HandleQueErr(ilRc);
		}/* end of if */
	}/* end of if */
       
	return ilRc;
}/* end of CheckQueue */



/****************************************************************************
* Function: GetUrnoFromSelection
* Parameter:           
*			 
* Return:
* Description: gets the Urno from the SQL Statement "WHERE URNO = 'xxxxxxx'"
*			   
*****************************************************************************/
static int GetUrnoFromSelection(char *pcpSelection,char *pcpUrno)
{
	char *pclFirst;
	char *pclLast;
	char pclTmp[24];
	int ilLen;
	
	pclFirst = strchr(pcpSelection,'\'');
	if (pclFirst != NULL)
	{
		pclLast  = strrchr(pcpSelection,'\'');
		if (pclLast == NULL)
		{
			pclLast = pclFirst + strlen(pclFirst);
		}
		ilLen = pclLast-pclFirst < 22 ? pclLast - pclFirst : 22;
		strncpy(pclTmp,++pclFirst,ilLen);
		pclTmp[ilLen-1] = '\0';
		strcpy(pcpUrno,pclTmp);
	}
	else
		{
		 pclFirst = pcpSelection;
		 while (!isdigit(*pclFirst) && *pclFirst != '\0')
		 {
			pclFirst++;
			}
		strcpy(pcpUrno,pclFirst);
		}
	return RC_SUCCESS;
}


static int SaveIndexInfo (ARRAYINFO *prpArrayInfo)
{
  int    ilRc       = RC_SUCCESS ;
  long   llRow      = 0 ;
  FILE  *prlFile    = NULL ;
  char   clDel      = ',' ;
  char   clFile[512] ;
  char  *pclTrimBuf = NULL ;

  dbg (TRACE,"SaveIndexInfo: <%s>",prpArrayInfo->crArrayName) ;

  if (ilRc == RC_SUCCESS)
  {
   sprintf (&clFile[0],"%s/%s_%s.info",getenv("DBG_PATH"),mod_name,prpArrayInfo->crArrayName) ;

   errno = 0 ;
   prlFile = fopen (&clFile[0], "w") ;
   if (prlFile == NULL)
   {
    dbg (TRACE, "SaveIndexInfo: fopen <%s> failed <%d-%s>", &clFile[0], errno, strerror(errno)) ;
    ilRc = RC_FAIL ;
   } /* end of if */
  } /* end of if */

  if (ilRc == RC_SUCCESS)
  {
   if (igTestOutput > 2) 
   {
    dbg (DEBUG, "ArrayName      <%s>\n", prpArrayInfo->crArrayName) ;      
    dbg (DEBUG, "ArrayFieldList <%s>\n", prpArrayInfo->crArrayFieldList) ;  
    dbg (DEBUG, "Idx01FieldList <%s>\n", prpArrayInfo->crIdx01FieldList) ;  
   }

   fprintf (prlFile, "ArrayName      <%s>\n", prpArrayInfo->crArrayName) ; fflush(prlFile) ;
   fprintf (prlFile, "ArrayFieldList <%s>\n", prpArrayInfo->crArrayFieldList) ; fflush(prlFile) ;
   fprintf (prlFile, "Idx01FieldList <%s>\n", prpArrayInfo->crIdx01FieldList) ; fflush(prlFile) ;

   if (igTestOutput > 1) 
   {
    dbg(DEBUG,"IndexData") ; 
   }
   fprintf(prlFile,"IndexData\n"); fflush(prlFile);

   llRow = ARR_FIRST ;
   do
   {
    ilRc = CEDAArrayFindKey (&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), &(prpArrayInfo->rrIdx01Handle), prpArrayInfo->crIdx01Name, &llRow, prpArrayInfo->lrIdx01RowLen, prpArrayInfo->pcrIdx01RowBuf);
    if (ilRc == RC_SUCCESS)
    {
     if (igTestOutput > 1) 
     {
      dbg(DEBUG, "SaveIndexInfo: Key <%ld> <%s>",llRow,prpArrayInfo->pcrIdx01RowBuf) ;
     }

     if (strlen (prpArrayInfo->pcrIdx01RowBuf) == 0)
     {
      ilRc = CEDAArrayGetRow(&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), llRow, clDel, prpArrayInfo->lrArrayRowLen, prpArrayInfo->pcrArrayRowBuf);
      if(ilRc == RC_SUCCESS)
      {
       if (igTestOutput > 1) 
       {
        dbg (DEBUG, "SaveIndexInfo: Row <%ld> <%s>", llRow,prpArrayInfo->pcrArrayRowBuf) ;
       }
      } /* end of if */
     } /* end of if */

     pclTrimBuf = strdup (prpArrayInfo->pcrIdx01RowBuf) ;
     if (pclTrimBuf != NULL)
     {
      ilRc = GetDataItem (pclTrimBuf,prpArrayInfo->pcrIdx01RowBuf, 1, clDel, "", "  ") ;
      if (ilRc > 0)
      {
       ilRc = RC_SUCCESS ;
       fprintf (prlFile,"Key            <%ld><%s>\n",llRow,pclTrimBuf) ; fflush(prlFile) ;
      } /* end of if */

      free (pclTrimBuf) ;
      pclTrimBuf = NULL ;
     } /* end of if */

     llRow = ARR_NEXT;
    }
    else
    {
     dbg (TRACE, "SaveIndexInfo: CEDAArrayFindKey failed <%d>", ilRc) ;
    } /* end of if */
   } while (ilRc == RC_SUCCESS) ;

   if (igTestOutput > 1) 
   {
    dbg (DEBUG,"ArrayData") ;
   }
   fprintf (prlFile,"ArrayData\n"); fflush(prlFile);

   llRow = ARR_FIRST ;
   do
   {
    ilRc = CEDAArrayFindKey (&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), &(prpArrayInfo->rrIdx01Handle), prpArrayInfo->crIdx01Name, &llRow, prpArrayInfo->lrIdx01RowLen, prpArrayInfo->pcrIdx01RowBuf);
    if (ilRc == RC_SUCCESS)
    {
     ilRc = CEDAArrayGetRow(&(prpArrayInfo->rrArrayHandle), &(prpArrayInfo->crArrayName[0]), llRow, clDel, prpArrayInfo->lrArrayRowLen, prpArrayInfo->pcrArrayRowBuf);
     if (ilRc == RC_SUCCESS)
     {
      if (igTestOutput > 1) 
      {
       dbg(DEBUG,"SaveIndexInfo: Row <%ld> <%s>",llRow,prpArrayInfo->pcrArrayRowBuf) ;
      }
      fprintf (prlFile,"Row            <%ld><%s>\n",llRow,prpArrayInfo->pcrArrayRowBuf) ; fflush(prlFile) ;
     }
     else
     {
      dbg (TRACE, "SaveIndexInfo: CEDAArrayGetRow failed <%d>", ilRc) ;
     } /* end of if */

     llRow = ARR_NEXT ;
    } 
    else
    {
     dbg (TRACE, "SaveIndexInfo: CEDAArrayFindKey failed <%d>", ilRc) ;
    } /* end of if */
   } while (ilRc == RC_SUCCESS) ;

   if (ilRc == RC_NOTFOUND)
   {
    ilRc = RC_SUCCESS ;
   } /* end of if */
  } /* end of if */

  if (prlFile != NULL)
  {
   fclose(prlFile) ;
   prlFile = NULL ;
   if (igTestOutput > 1) 
   {
    dbg (TRACE, "SaveIndexInfo: event saved - file <%s>", &clFile[0]) ;
   }
  } /* end of if */

  return (ilRc) ;

}

		
static int SendAnswer(int ipDest, char *pcpAnswer)
{
	int			ilRC;
	int			ilLen;
	EVENT			*prlOutEvent 	= NULL;
	BC_HEAD		*prlOutBCHead 	= NULL;
	CMDBLK		*prlOutCmdblk	= NULL;

    dbg(DEBUG,"SendAnswer: START-->");

	/* calculate size of memory we need */
	ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + strlen(pcpAnswer) + 32 ;

	/* get memory for out event */
	if ((prlOutEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
	{
		dbg(TRACE,"<SendAnswer> %05d malloc failed, can't allocate %d bytes", __LINE__, ilLen) ;
		dbg(DEBUG,"<SendAnswer> ----- END -----") ;
		return RC_FAIL ;
	}

	/* clear buffer */
	memset((void*)prlOutEvent, 0x00, ilLen) ;

	/* set structure members */
	prlOutEvent->type	   = SYS_EVENT ;
	prlOutEvent->command 	   = EVENT_DATA ;
	prlOutEvent->originator  = ipDest ;            /* mod_id ;   */ 
	prlOutEvent->retry_count = 0 ;
	prlOutEvent->data_offset = sizeof(EVENT) ;
	prlOutEvent->data_length = ilLen - sizeof(EVENT) ;

	/* BCHead members */
	prlOutBCHead = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT)) ;
	prlOutBCHead->rc = NETOUT_NO_ACK ;
	strncpy(prlOutBCHead->dest_name, "ACTION", 10) ;

	/* CMDBLK members */
	prlOutCmdblk = (CMDBLK*)((char*)prlOutBCHead->data) ;
	strcpy(prlOutCmdblk->command, "FIR") ;
	strcpy(prlOutCmdblk->tw_end, "ANS,WER,ACTION") ;

	/* data */
	strcpy(prlOutCmdblk->data+2, pcpAnswer) ;

	/* send this message */
	dbg(DEBUG,"<SendAnswer> sending to Mod-ID: %d", prgEvent->originator) ;
	if ((ilRC = que(QUE_PUT, prgEvent->originator, mod_id, PRIORITY_3, ilLen, (char*)prlOutEvent)) != RC_SUCCESS)
	{
		/* delete memory */
		free((void*)prlOutEvent) ;
		dbg(TRACE,"<SendAnswer> %05d QUE_PUT returns: %d", __LINE__, ilRC) ;
		dbg(DEBUG,"<SendAnswer> ----- END -----") ;
		return RC_FAIL ;
	}

/*  
	DebugPrintBchead (DEBUG, prlOutBCHead) ;
	DebugPrintCmdblk (DEBUG, prlOutCmdblk) ;
	DebugPrintEvent  (DEBUG, prlOutEvent) ;               
*/
	/* release memory */
	free((void*)prlOutEvent);
	
    dbg(DEBUG,"SendAnswer: <-- ENDE ");
	
	/* bye bye */
	return RC_SUCCESS;
}

static int GetDebugLevel(char *pcpMode, int *pipMode)
{
	int		ilRC = RC_SUCCESS;
	char	clCfgValue[64];

	ilRC = iGetConfigRow(cgConfigFile, "SYSTEM", pcpMode, CFG_STRING,
							clCfgValue);

	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"SYSTEM %s not found in <%s>", pcpMode, cgConfigFile);
	}
	else
	{
		dbg(TRACE, "SYSTEM %s <%s>", pcpMode, clCfgValue);
		if (!strcmp(clCfgValue, "DEBUG"))
			*pipMode = DEBUG;
		else if (!strcmp(clCfgValue, "TRACE"))
			*pipMode = TRACE;
		else
			*pipMode = 0;
	}

	return ilRC;
}

static int ItemCnt(char *pcgData, char cpDelim)
{
	int	ilItemCount=0;

	if (pcgData && strlen(pcgData)) 
	{
	    ilItemCount = 1;
	    while (*pcgData) 
		{
			if (*pcgData++ == cpDelim) 
			{
				ilItemCount++;
			} /* end if */
		} /* end while */
	} /* end if */
	return ilItemCount;
}


static int DumpArray(ARRAYINFO *prpArray,char *pcpFields)
{
	int ilRc;
	int ilRecord = 0;
	long llRowNum = ARR_FIRST;
	char *pclRowBuf;

	dbg(TRACE,"Arraydump: %s", prpArray->crArrayName);
	if (pcpFields != NULL)
	{
		dbg(TRACE,"Arraydump: Fields = %s", pcpFields);
	}
	else
	{
		dbg(TRACE,"Arraydump: Fields = %s",prpArray->crArrayFieldList);
	}

	while(CEDAArrayGetRowPointer(&prpArray->rrArrayHandle,
		prpArray->crArrayName,llRowNum,(void *)&pclRowBuf) == RC_SUCCESS)       
	{
			int ilFieldNo;
		
			ilRecord++;
			llRowNum = ARR_NEXT;

			dbg(TRACE,"Record No: %d",ilRecord);
			for (ilFieldNo = 1; ilFieldNo < prpArray->lrArrayFieldCnt; ilFieldNo++)
			{
				char pclFieldName[24];

				*pclFieldName = '\0';
				GetDataItem(pclFieldName,prpArray->crArrayFieldList,
					ilFieldNo,',',"","\0\0");
				if (pcpFields != NULL)      
				{
					if(strstr(pcpFields,pclFieldName) == NULL)
					{
						continue;
					}
				}
				dbg(TRACE,"%s: <%s>",
						pclFieldName,PFIELDVAL((prpArray),pclRowBuf,ilFieldNo-1));
			}
	}

	return ilRc;

}


static int CheckDateFormat(char *pcpDate)
{
	int ilRc = RC_SUCCESS;

	char clYear[8];
	char clMonth[4];
	char clDay[4];
	char clHour[4];
	char clMin[4];
	char clSec[4];
	char *pclDate;
	int ilActualPos = 0;
	int ilLc;
	static int igDaysPerMonth[12] = {31,28,31,30,31,30,31,31,30,31,30,31};

	int ilYear,ilMonth,ilDay,ilHour,ilMin,ilSec;

	for (ilLc = 0; pcpDate[ilLc] != '\0'; ilLc++)
	{
			if(! (pcpDate[ilLc] >= '0' && pcpDate[ilLc] <= '9'))
			{
				strcpy(pcpDate," ");
				return (RC_FAIL);
			}
	}

	pclDate = pcpDate;

	StringMid(clYear,pclDate,ilActualPos,4,NOCONVERT);
	ilActualPos += 4;
	StringMid(clMonth,pclDate,ilActualPos,2,NOCONVERT);
	ilActualPos += 2;
	StringMid(clDay,pclDate,ilActualPos,2,NOCONVERT);
	ilActualPos += 2;
	StringMid(clHour,pclDate,ilActualPos,2,NOCONVERT);
	ilActualPos += 2;
	StringMid(clMin,pclDate,ilActualPos,2,NOCONVERT);
	ilActualPos += 2;
	StringMid(clSec,pclDate,ilActualPos,2,NOCONVERT);
	ilActualPos += 2;

	dbg(TRACE,"<%s><%s><%s><%s><%s><%s>",clYear,clMonth,clDay,clHour,clMin,clSec);
	ilYear = atoi(clYear);
	ilMonth = atoi(clMonth);
	ilDay = atoi(clDay);
	ilHour = atoi(clHour);
	ilMin = atoi(clMin);
	ilSec = atoi(clSec);

	if (ilYear < 1900 || ilYear > 9999)
	{
		dbg(TRACE,"CheckDateFormat: invalid year %d <%s>",ilYear,pcpDate);
		ilRc = RC_FAIL;
	}
	else
	{
		if (ilMonth < 1 || ilMonth > 12)
		{
			dbg(TRACE,"CheckDateFormat: invalid month <%s>",pcpDate);
			ilRc = RC_FAIL;
		}
		else
		{
			if (ilMonth == 2)
			{
				if ( ((ilYear % 4) == 0) && (((ilYear % 100) != 0) || (ilYear % 400) == 0)) 
				{
					/* is leap year */
					if (ilDay > 29 || ilDay < 1)
					{
						dbg(TRACE,"CheckDateFormat: invalid day <%s> in leap year",pcpDate);
						ilRc = RC_FAIL;
					}
				}
				else if (ilDay < 1 || ilDay > igDaysPerMonth[ilMonth-1])
				{
					dbg(TRACE,"CheckDateFormat: invalid day <%s>",pcpDate);
					ilRc = RC_FAIL;
				}
			}
			else
			{
				if (ilDay < 1 || ilDay > igDaysPerMonth[ilMonth-1])
				{
					dbg(TRACE,"CheckDateFormat: invalid day <%s>",pcpDate);
					ilRc = RC_FAIL;
				}
			}
			if (ilHour < 0 || ilHour > 24)
			{
				dbg(TRACE,"CheckDateFormat: invalid hour <%s>",pcpDate);
				ilRc = RC_FAIL;
			}
			if (ilMin < 0 || ilMin > 60)
			{
				dbg(TRACE,"CheckDateFormat: invalid minute <%s>",pcpDate);
				ilRc = RC_FAIL;
			}
			if (ilSec < 0 || ilSec > 60)
			{
				dbg(TRACE,"CheckDateFormat: invalid second <%s>",pcpDate);
				ilRc = RC_FAIL;
			}
		}
	}

	return ilRc;
}

static BOOL RelevantFields(char *pcpFieldList,char *pcpRelevantFields)
{
	char clActualField[8];
	int ilCntFields;
	int ilItemNo;
	int i;
	BOOL blRelevantFieldFound = FALSE;

	ilCntFields = field_count(pcpRelevantFields);
	
	dbg(TRACE,"RelevantFields <%s>",pcpRelevantFields);
	for (i = 1; i < ilCntFields; i++)
	{
		dbg(TRACE,"I=%d",i);
		get_real_item(clActualField, pcpRelevantFields, i);

		dbg(TRACE,"I=%d Field: <%s>",i,clActualField);
		ilItemNo = get_item_no(pcpFieldList,clActualField,5);

		if (ilItemNo >= 0)
		{
			blRelevantFieldFound = TRUE;
			break;
		}
	}

	if (blRelevantFieldFound)
	{
	dbg(DEBUG,"RelevantFields: Relevant field <%s> found. Returning TRUE.",
			clActualField);
	}
	else
	{
	dbg(DEBUG,"RelevantFields: No relevant fields found <%s> <%s>. Returning FALSE.",
				pcpFieldList,pcpRelevantFields);
	}
	return blRelevantFieldFound;
}

static BOOL RelevantChange(char *pcpData,char *pcpOldData,char *pcpFieldList,char *pcpRelevantFields)
{
	char clOldValue[20];
	char clNewValue[20];
	char clActualField[8];
	int ilCntFields;
	int ilItemNo;
	int i;

	ilCntFields = field_count(pcpRelevantFields);
	/*dbg(DEBUG,"RelevantChange: Old Data: <%s> new data <%s>",pcpOldData,pcpData);*/

	for (i = 1; i <= ilCntFields; i++)
	{
		get_real_item(clActualField, pcpRelevantFields,i);
		ilItemNo = get_item_no(pcpFieldList,clActualField,5) + 1;

		if (ilItemNo > 0)
		{
			get_real_item(clNewValue, pcpData, ilItemNo);
			get_real_item(clOldValue, pcpOldData, ilItemNo);

			if (strcmp(clNewValue,clOldValue) != NULL)
			{
				dbg(DEBUG,"RelevantChange: Item <%s> - old value <%s> and new value <%s> are different. Returning TRUE.",clActualField,clOldValue,clNewValue);
				return TRUE;
			}
			else
			{
				dbg(DEBUG,"RelevantChange: Item <%s> - old value <%s> and new value <%s> are identical. Checking next one.",clActualField,clOldValue,clNewValue);
			}
		}
		else
		{
			dbg(TRACE,"RelevantChange: Item <%s> not found in fieldlist <%s>",clActualField,pcpFieldList);
		}
	}

	dbg(DEBUG,"RelevantChange: No relevant changes detected. Returning FALSE.");
	return FALSE;
}

static int GetBreakTimeDiff (char *pcpLocal1,char *pcpLocal2, time_t *plpDiff)
{
	int ilRC = RC_SUCCESS;
	char clTime1[16], clTime2[16];
	struct tm	rlTimeStruct1, rlTimeStruct2;
	time_t llT1, llT2;

	*plpDiff = (time_t)0;
	if ( !pcpLocal1 || ( strlen ( pcpLocal1 ) != 14 ) )
		ilRC = RC_FAIL;
	if ( !pcpLocal2 || ( strlen ( pcpLocal2 ) != 14 ) )
		ilRC = RC_FAIL;
	if ( ilRC != RC_SUCCESS )
		dbg (DEBUG, "GetBreakTimeDiff: At least one parameter invalid");
	else
	{
		strcpy ( clTime1, pcpLocal1 );
		strcpy ( clTime2, pcpLocal2 );
		ilRC = CedaDateToTimeStruct(&rlTimeStruct1, clTime1 );
		ilRC |= CedaDateToTimeStruct(&rlTimeStruct2, clTime2 );
	}
	if ( ilRC != RC_SUCCESS )
	{
		dbg ( TRACE, "GetBreakTimeDiff: At least one call to CedaDateToTimeStruct failed" );
	}
	else
	{
		llT1 = mktime ( &rlTimeStruct1 );
		llT2 = mktime ( &rlTimeStruct2 );
		*plpDiff = llT2 - llT1;
		dbg ( TRACE, "GetBreakTimeDiff: <%s> - <%s> = %ld seconds", pcpLocal2, pcpLocal1, *plpDiff );
	}
	return ilRC;
}
static BOOL RelevantChangeJob(char *pcpData,char *pcpOldData,char *pcpFieldList)
{
	time_t llOldBreakDuration;
	time_t llNewBreakDuration;
	char clBreakFrom[20];
	char clBreakTo[20];
	int ilRc;

	/* get new break duration */
	ilRc = GetItem(clBreakFrom,pcpData,pcpFieldList,"ACFR",TRUE);
	if (ilRc < 0)
	{
		return FALSE;
	}

	ilRc = GetItem(clBreakTo,pcpData,pcpFieldList,"ACTO",TRUE);
	if (ilRc < 0)
	{
		return FALSE;
	}

	GetBreakTimeDiff(clBreakFrom,clBreakTo,&llNewBreakDuration);

	/* get old break duration */
	ilRc = GetItem(clBreakFrom,pcpOldData,pcpFieldList,"ACFR",TRUE);
	if (ilRc < 0)
	{
		return FALSE;
	}

	ilRc = GetItem(clBreakTo,pcpOldData,pcpFieldList,"ACTO",TRUE);
	if (ilRc < 0)
	{
		return FALSE;
	}
	
	GetBreakTimeDiff(clBreakFrom,clBreakTo,&llOldBreakDuration);

	/* check, if break duration changed */
	if (llNewBreakDuration != llOldBreakDuration)
	{
		dbg(DEBUG,"RelevantChangeJob: Break duration changed. Returning TRUE.");
		return TRUE;
	}
	else
	{
		dbg(DEBUG,"RelevantChangeJob: Break duration not changed. Returning FALSE.");
		return FALSE;
	}
}

static BOOL CheckTohr(char *pcpSurn)
{
	int ilRc;
	long llRow = ARR_FIRST;

	dbg(DEBUG,"CheckTohr(): search TOHR for STFTAB.URNO <%s> ",pcpSurn);

	ilRc = CEDAArrayFindRowPointer(&rgStfArray.rrArrayHandle,
			rgStfArray.crArrayName,&rgStfArray.rrIdx01Handle,
			rgStfArray.crIdx01Name,pcpSurn,&llRow,
			(void **)&rgStfArray.pcrArrayRowBuf);

	if (ilRc == RC_SUCCESS)
	{
		char clTohr[2];
		strcpy(clTohr,FIELDVAL(rgStfArray,rgStfArray.pcrArrayRowBuf,igStfTOHR));
		if (strstr(cgTohr,clTohr) != NULL)
		{
			dbg(DEBUG,"CheckTohr(): Flag 'TOHR' of STF.URNO <%s> is OK.",pcpSurn);
			return TRUE;
		}
	}

	dbg(DEBUG,"CheckTohr(): Do NOT export STF.URNO <%s> to SAP HR.",pcpSurn);
	return FALSE;
}

static BOOL DeleteStfRow(char *pcpSurn)
{
	int ilRc;
	long llRow = ARR_FIRST;

	dbg(DEBUG,"DeleteStfRow(): search for STFTAB.URNO <%s> to delete row",pcpSurn);

	ilRc = CEDAArrayFindRowPointer(&rgStfArray.rrArrayHandle,
			rgStfArray.crArrayName,&rgStfArray.rrIdx01Handle,
			rgStfArray.crIdx01Name,pcpSurn,&llRow,
			(void **)&rgStfArray.pcrArrayRowBuf);

	if (ilRc == RC_SUCCESS)
	{
		dbg(DEBUG,"DeleteStfRow(): calling AATArrayDeleteRow for row <%ld> of 'STFTAB'",llRow);
		ilRc = AATArrayDeleteRow(&rgStfArray.rrArrayHandle,"STFTAB",llRow);
		if (ilRc != RC_SUCCESS)
		{
			dbg(DEBUG,"DeleteStfRow(): AATArrayDeleteRow failed for row %ld with RC = <%d>",llRow,ilRc);
			return FALSE;
		}
		return TRUE;
	}
	else
	{
		dbg(DEBUG,"DeleteStfRow(): STF.URNO <%s> not found!",pcpSurn);
		return FALSE;
	}
}


static int GetStaffUrno(char *pclStaffUrno, char *pcpPeno)
{
	short slFkt = 0;
    short slCursor = 0;
	int ilRC = 0;
	char clSqlBuf[200] = "\0";


	slFkt = START;
	slCursor = 0;
	sprintf(clSqlBuf,"SELECT URNO FROM STFTAB WHERE PENO = '%s'",pcpPeno);
	ilRC = sql_if (slFkt, &slCursor, clSqlBuf, pclStaffUrno);
	if (ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"sql_if failed###################");
	}
	commit_work();
	close_my_cursor (&slCursor);
	return RC_SUCCESS;

}
