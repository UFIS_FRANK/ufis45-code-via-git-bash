#ifndef _DEF_mks_version
#define _DEF_mks_version
#include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Sin/SIN_Server/Base/Server/Interface/coshdl.c 1.49 2008/07/11 15:52:19SGT fya Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program coshdl                                                  */
/*                                                                            */
/* Author         :                                                           */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history :                                                           */
/* 30.1.2004: HEB: FLTN change from 6 chars to 5 Chars                        */
/* 04.09.2006: TKU: Made changes to accomodate OK2LOAD messages handling      */
/* 16.04.2008: BLE: Delete BRS from LOATAB if CNX status is received          */
/* 23.04.2008: BLE: Disable OKL to iTrek web server                           */
/* 11.07.2008: BLE: Empty ULD number has 12 blanks, identify and throw        */
/* 16.10.2008: BLE: Forward flight delay info to coswmq                       */
/* 04.12.2008: BLE: Verify in DLYTAB before sending inserted DLY to COSYS     */
/* 13.09.2009: BLE: Include Number of Pieces for iTrek                        */
/* 17.07.2012: FYA: Check the field list for UFR command CheckFieldForUFRCommand()*/
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */

static char sccs_version[] ="@(#) UFIS45 (c) ABB AAT/I coshdl.c 45.1.0 / 11.12.2002 HEB";


#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "fditools.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "rmschkrul.h"
#include "syslib.h"
#include "db_if.h"
#include <time.h>
#include <cedatime.h>

#ifdef _HPUX_SOURCE
extern int daylight;
extern long timezone;
extern char *tzname[2];
#else
extern int _daylight;
extern long _timezone;
extern char *_tzname[2];
#endif

#define MaxRoute 3  /* Maximum number of via airports in routing list */
#define ROUTELEN 12 /* Length of ROUTE field */
#define RC_NODATA -129
/*
** URNO pool
*/
#define URNO_POOL_SIZE 100        /* number of URNOS fetched with each refill of pool */
#define URNO_SIZE 12              /* maximum size of URNO in string representation    */ 

/* BLE */
#define TYPE_INSERT 1
#define TYPE_UPDATE 2
#define TYPE_DELETE 3

/* Below structure is changed according to ICD, remarks changed from 15 to 25 characters. */
typedef struct 
{
  char uldNum[12];
  char alc[3];
  char fltn[6];
  char flns[1];
  char flda[8];
  char des3[3];
  char uldCont[10];
  char uldSource[2];
  char numOfPieces[3];
  char uldVolAvail[5];
  char remarks[15]; /* changed by Thanooj */
  char status[3];
} BRSInputType;

#define MAXCOUNT  20 /* maximum number of nonunique flights to log */

/*Created below structure for OK2LOAD message ... Guess can make use of BRSInputType for OK2LOAD message but below OK2LoadType was created to take care of first 3 characters of the message "OKL" ... Thanooj */
typedef struct 
{
    char msgName[3];
    char uldNum[12];
    char alc[3];
    char fltn[6];
    char flns[1];
    char flda[12];
    char des3[3];
}OK2LoadType;


int debug_level = TRACE;
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static EVENT *prgOutEvent  = NULL;

static char cgProcessName[20] = "\0";
static char  cgConfigFile[512] = "\0";
static char  cgHopo[8] = "\0";                         /* default home airport    */
static char  cgTabEnd[8] ="\0";                       /* default table extension */

static long lgEvtCnt = 0;
static char cgTdi1[24]; /* UTC-Loc7al Difference */
static char cgTdi2[24]; /* UTC-Local Difference */
static char cgTich[24]; /* UTC-Local Difference */

static char cgSendAFTModid[100] = "\0";
static int igSendULDModid = 0;
static int igSendOK2Modid = 0; /* Thanooj */
static int igSendCPMModid = 0; /* BLE */
static int igSendDLYModid[5]; /* BLE */
static char cgSendZeroUlds[10] = "\0";

/* BLE */
static int igOKLTimeDiff = 0;
static int igOKLForward = 0;

/******************************************************************************/
/* Function prototypes                                                        */
/******************************************************************************/
static int  Init_Process();
static int ReadConfigEntry(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer);

static int  Reset(void);                       /* Reset program          */
static void Terminate(int ipSleep);            /* Terminate program      */
static void HandleSignal(int);                 /* Handles signals        */
static void HandleErr(int);                    /* Handles general errors */
static void   HandleQueErr(int);                 /* Handles queuing errors */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/

static int  HandleData(EVENT *prpEvent);       /* Handles event data     */
static int  GetCommand(char *pcpCommand, int *pipCmd);
static int HandleAftData(int ipDelayInfo, char *pcpCmd,char *pcpFields,char *pcpData, char *pcpResult);
static int HandleUldData(char *pcpFields,char *pcpData, char *pcpCmd);
static int HandleCosData(char *pcpData);
static int getOrPutDBData(char *pcpSelection, char *pcpData, unsigned int ipMode);
static int appendUldList(long lpUrno, char *pcpCmd, char *pclAdid);
static int HandleDelayCode(char *pcpFields,char *pcpData, char *pcpCmd);

extern  char *getItem(char *,char *,char *);
static void TrimRight(char *);
static int UtcToLocal(char *);
static void getVialItems(int ,char* ,char*);
static int getFreeUrno(char *);
static int createFkey(char *,char *,char *,char *,char *,char *);
void BRSSending( int ipSendType, long lpUrno, long plFlnu, BRSInputType rpBRS );
int TimeDiff ( char *pclTimeStr );

static int CheckFieldForUFRCommand(char *pcpFieldList);

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
    int ilRc = RC_SUCCESS;          /* Return code          */
    int ilCnt = 0;
    int ilOldDebugLevel = 0;


    INITIALIZE;         /* General initialization   */



    debug_level = TRACE;

    strcpy(cgProcessName,argv[0]);

    dbg(TRACE,"MAIN: version <%s>",sccs_version);

    /* Attach to the MIKE queues */
    do
    {
    ilRc = init_que();
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
        sleep(6);
        ilCnt++;
    }/* end of if */
    }while((ilCnt < 10) && (ilRc != RC_SUCCESS));

    if(ilRc != RC_SUCCESS)
    {
    dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
    sleep(60);
    exit(1);
    }else{
    dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
    }/* end of if */

    do
    {
    ilRc = init_db();
    if (ilRc != RC_SUCCESS)
    {
        check_ret(ilRc);
        dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
        sleep(6);
        ilCnt++;
    } /* end of if */
    } while((ilCnt < 10) && (ilRc != RC_SUCCESS));

    if(ilRc != RC_SUCCESS)
    {
    dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
    sleep(60);
    exit(2);
    }else{
    dbg(TRACE,"MAIN: init_db() OK!");
    } /* end of if */

    /* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */

    sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
    dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
    ilRc = TransferFile(cgConfigFile);
    if(ilRc != RC_SUCCESS)
    {
    dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */

    sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
    dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
    ilRc = TransferFile(cgConfigFile);
    if(ilRc != RC_SUCCESS)
    { 
    dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */

    ilRc = SendRemoteShutdown(mod_id);
    if(ilRc != RC_SUCCESS)
    {
    dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    } /* end of if */

    if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
    dbg(DEBUG,"MAIN: waiting for status switch ...");
    HandleQueues();
    }/* end of if */

    if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
    dbg(TRACE,"MAIN: initializing ...");
    ilRc = Init_Process();
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"Init_Process: init failed!");
    } /* end of if */

    } else {
    Terminate(30);
    }/* end of if */



    dbg(TRACE,"=====================");
    dbg(TRACE,"MAIN: initializing OK");
    dbg(TRACE,"=====================");
    

    for(;;)
    {
    ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
    /* depending on the size of the received item  */
    /* a realloc could be made by the que function */
    /* so do never forget to set event pointer !!! */
    prgEvent = (EVENT *) prgItem->text;
    /* Acknowledge the item */
    ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
    if( ilRc != RC_SUCCESS ) 
    {
        /* handle que_ack error */
        HandleQueErr(ilRc);
    } /* fi */


    if( ilRc == RC_SUCCESS )
    {
            
        lgEvtCnt++;

        switch( prgEvent->command )
        {
        case    HSB_STANDBY :
            ctrl_sta = prgEvent->command;
            HandleQueues();
            break;  
        case    HSB_COMING_UP   :
            ctrl_sta = prgEvent->command;
            HandleQueues();
            break;  
        case    HSB_ACTIVE  :
            ctrl_sta = prgEvent->command;
            break;  
        case    HSB_ACT_TO_SBY  :
            ctrl_sta = prgEvent->command;
            /* CloseConnection(); */
            HandleQueues();
            break;  
        case    HSB_DOWN    :
            /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
            ctrl_sta = prgEvent->command;
            Terminate(1);
            break;  
        case    HSB_STANDALONE  :
            ctrl_sta = prgEvent->command;
            ResetDBCounter();
            break;  
        case    REMOTE_DB :
            /* ctrl_sta is checked inside */
            HandleRemoteDB(prgEvent);
            break;
        case    SHUTDOWN    :
            /* process shutdown - maybe from uutil */
            Terminate(1);
            break;
                    
        case    RESET       :
            ilRc = Reset();
            break;
                    
        case    EVENT_DATA  :
            if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
            {
            ilRc = HandleData(prgEvent);
            if(ilRc != RC_SUCCESS)
            {
                HandleErr(ilRc);
            }/* end of if */
            }else{
            dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
            DebugPrintItem(TRACE,prgItem);
            DebugPrintEvent(TRACE,prgEvent);
            }/* end of if */
            break;
                

        case    TRACE_ON :
            dbg_handle_debug(prgEvent->command);
            break;
        case    TRACE_OFF :
            dbg_handle_debug(prgEvent->command);
            break;
        default         :
            dbg(TRACE,"MAIN: unknown event");
            DebugPrintItem(TRACE,prgItem);
            DebugPrintEvent(TRACE,prgEvent);
            break;
        } /* end switch */



    } else {
        /* Handle queuing errors */
        HandleQueErr(ilRc);
    } /* end else */
        
    } /* end for */
    
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/



static int ReadConfigEntry(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer)
{
    int ilRc = RC_SUCCESS;

    char clSection[124] = "\0";
    char clKeyword[124] = "\0";

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRc = iGetConfigEntry(cgConfigFile,clSection,clKeyword,
               CFG_STRING,pcpCfgBuffer);
    if(ilRc != RC_SUCCESS)
    {
    dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
    } 
    else
    {
    dbg(TRACE,"Config Entry <%s>,<%s>:<%s> found in %s",
        clSection, clKeyword ,pcpCfgBuffer, cgConfigFile);
    }/* end of if */
    return ilRc;
}


static int Init_Process()
{
    int  ilRc = RC_SUCCESS;         /* Return code */
    int ili;
    char clSection[64] = "\0";
    char clKeyword[64] = "\0";
    int ilOldDebugLevel = 0;
    long pclAddFieldLens[12];
    char pclAddFields[256] = "\0";
    char pclSqlBuf[2560] = "\0";
    char pclSelection[1024] = "\0";
    short slCursor = 0;
    short slSqlFunc = 0;
    char  clBreak[24] = "\0";
    char pclApttab[10];
    char pclDataArea[2560];
    char pclErr[2560];
    char clTemp[200] = "\0";
    char *pclToken;
    char *pclFunc = "Init_Process";
    

    if(ilRc == RC_SUCCESS)
    {
    /* read HomeAirPort from SGS.TAB */
    ilRc = tool_search_exco_data("SYS", "HOMEAP", cgHopo);
    if (ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"<Init_Process> EXTAB,SYS,HOMEAP not found in SGS.TAB");
        Terminate(30);
    } else {
        dbg(TRACE,"<Init_Process> home airport <%s>",cgHopo);
    }
    }

    
    if(ilRc == RC_SUCCESS)
    {

    ilRc = tool_search_exco_data("ALL","TABEND", cgTabEnd);
    if (ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"<Init_Process> EXTAB,ALL,TABEND not found in SGS.TAB");
        Terminate(30);
    } else {
        dbg(TRACE,"<Init_Process> table extension <%s>",cgTabEnd);
    }
    }

    if(ilRc == RC_SUCCESS)
    {
    dbg(TRACE,"ACTUAL TIMEZONE-SETTING: <%s>",getenv("TZ"));
    }

    if(ilRc == RC_SUCCESS)
    {
        slSqlFunc = START;
        slCursor = 0;
        sprintf(pclSqlBuf,"SELECT TICH,TDI1,TDI2 FROM APTTAB WHERE APC3='%s'",cgHopo);
        ilRc = sql_if(slSqlFunc,&slCursor,pclSqlBuf,pclDataArea);
        if (ilRc != DB_SUCCESS)
        {
            get_ora_err(ilRc,pclErr);
            dbg(TRACE,"Init_Process Error reading APTTAB: ilRc <%d>\nORA-ERR <%s>\nSqlBuf <%s>",ilRc,pclErr,pclSqlBuf);
        sleep(120);
        close_my_cursor(&slCursor);
        commit_work();
        slCursor = 0;
        Terminate(1);
            ilRc = RC_FAIL;
        }
        close_my_cursor(&slCursor);
        commit_work();
        slCursor = 0;
 
    strcpy(cgTdi1,"60");
    strcpy(cgTdi2,"60");
    get_fld(pclDataArea,0,STR,14,cgTich);
    get_fld(pclDataArea,1,STR,14,cgTdi1);
    get_fld(pclDataArea,2,STR,14,cgTdi2);
    TrimRight(cgTich);
    if (*cgTich != '\0')
    {
        TrimRight(cgTdi1);
        TrimRight(cgTdi2);
        sprintf(cgTdi1,"%d",atoi(cgTdi1)*60);
        sprintf(cgTdi2,"%d",atoi(cgTdi2)*60);
    }
    dbg(TRACE,"InitProcess Read %s -> TICH: <%s> TDI1: <%s> TDI2 <%s>",pclApttab,cgTich,cgTdi1,cgTdi2);
    }

    ilRc = ReadConfigEntry("MAIN","SEND_AFT_TO_MODID",clTemp);
    if (ilRc == RC_SUCCESS)
    {
        if (strlen(clTemp) < 100)
        {
            strcpy(cgSendAFTModid,clTemp);
        }
        else
        {
            strcpy(cgSendAFTModid,"0");
        }
    }
    dbg( TRACE, "<%s> AFT Modid = <%s>", pclFunc, cgSendAFTModid );

    ilRc = ReadConfigEntry("MAIN","SEND_ULD_TO_MODID",clTemp);
    if (ilRc == RC_SUCCESS)
    {
    igSendULDModid = atoi(clTemp);
    }
    dbg( TRACE, "<%s> ULD Modid = <%d>", pclFunc, igSendULDModid );

    /* Added by Thanooj - to get the OK2 Load mod id*/
    ilRc = ReadConfigEntry("MAIN","SEND_OK2_TO_MODID",clTemp);
    if (ilRc == RC_SUCCESS)
    {
    igSendOK2Modid = atoi(clTemp);
    }
    /* Thanooj */
    dbg( TRACE, "<%s> OKL Modid = <%d>", pclFunc, igSendOK2Modid );

    ilRc = ReadConfigEntry("MAIN","SEND_ZERO_ULDS",cgSendZeroUlds);
    if (ilRc != RC_SUCCESS)
    {
    strcpy(cgSendZeroUlds,"YES");
    }

    /* BLE */
    igSendCPMModid = tool_get_q_id("itkcpm");
    if( igSendCPMModid <= 0 )
        igSendCPMModid = 9582;
    dbg( TRACE, "<%s> CPM Modid = <%d>", pclFunc, igSendCPMModid );

    /* BLE */
    memset( igSendDLYModid, 0, sizeof(igSendDLYModid) );
    ili = 0;
    ilRc = ReadConfigEntry("MAIN","SEND_DELAYCODE_TO_MODID",clTemp);
    if( ilRc == RC_SUCCESS )
    {
        pclToken = (char *)strtok(clTemp, ",");
        while( pclToken != NULL && ili < 5 )
        {
            igSendDLYModid[ili] = atoi(pclToken);
            pclToken = (char *)strtok( NULL, "," );
            dbg( TRACE, "<%s> DLY Modid[%d] = <%d>", pclFunc, ili, igSendDLYModid[ili] );
            ili++;
        }
    }


    ReadConfigEntry("MAIN","DEBUG_LEVEL",clTemp);
    debug_level = DEBUG;
    if( !strncmp(clTemp, "TRACE", 5) )
        debug_level = TRACE;

    /*** BLE 23-Apr-2008 ***/
    ilRc = ReadConfigEntry("OKL","TIME_DIFF",clTemp);
    igOKLTimeDiff = 20; /* min */
    if( ilRc == RC_SUCCESS )
        igOKLTimeDiff = atoi(clTemp);
    dbg( TRACE, "<Init_Process> igOKLTimeDiff <%d>", igOKLTimeDiff );

    igOKLForward = 0; 
    ilRc = ReadConfigEntry("OKL","FORWARD_ITREK",clTemp);
    if( ilRc == RC_SUCCESS && !strncmp( clTemp, "ON", 2 ) )
        igOKLForward = 1;
    dbg( TRACE, "<Init_Process> igOKLForward <%d> 1-ON 0-OFF", igOKLForward );



    if (ilRc != RC_SUCCESS)
    {
    dbg(TRACE,"Init_Process failed");
    Terminate(60);
    }

    return(ilRc);
    
} /* end of initialize */




/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
    int ilRc = RC_SUCCESS;              /* Return code */
    
    dbg(TRACE,"Reset: now resetting");
    
    return ilRc;
    
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
    /* unset SIGCHLD ! DB-Child will terminate ! */

    dbg(TRACE,"Terminate: now DB logoff ...");

    signal(SIGCHLD,SIG_IGN);

    logoff();

    dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);


    dbg(TRACE,"Terminate: now leaving ...");

    fclose(outp);

    sleep(ipSleep < 1 ? 1 : ipSleep);
    
    exit(0);
    
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/

static void HandleSignal(int pipSig)
{
    dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);

    switch(pipSig)
    {
    default :
        Terminate(1);
        break;
    } /* end of switch */

    exit(1);
    
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
    dbg(TRACE,"HandleErr: <%d> : unknown ERROR",pipErr);

    return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    switch(pipErr) {
    case    QUE_E_FUNC  :   /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :   /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;
    case    QUE_E_SEND  :   /* Error using msgsnd */
        dbg(TRACE,"<%d> : msgsnd failed",pipErr);
        break;
    case    QUE_E_GET   :   /* Error using msgrcv */
        dbg(TRACE,"<%d> : msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX   :
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE  :
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES  :
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        break;
    case    QUE_E_RESP  :   /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;
    case    QUE_E_FULL  :
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG :   /* No message on queue */
        dbg(TRACE,"<%d> : no messages on queue",pipErr);
        break;
    case    QUE_E_INVORG    :   /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :   /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;
    default         :   /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
    } /* end switch */
         
    return;

} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
    int ilRc = RC_SUCCESS;          /* Return code */
    int ilBreakOut = FALSE;
    
    do
    {
    ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
    /* depending on the size of the received item  */
    /* a realloc could be made by the que function */
    /* so do never forget to set event pointer !!! */
    prgEvent = (EVENT *) prgItem->text; 

    if( ilRc == RC_SUCCESS )
    {
        /* Acknowledge the item */
        ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
        if( ilRc != RC_SUCCESS ) 
        {
        /* handle que_ack error */
        HandleQueErr(ilRc);
        } /* fi */
        
        switch( prgEvent->command )
        {
        case    HSB_STANDBY :
            ctrl_sta = prgEvent->command;
            break;  
    
        case    HSB_COMING_UP   :
            ctrl_sta = prgEvent->command;
            break;  
    
        case    HSB_ACTIVE  :
            ctrl_sta = prgEvent->command;
            ilBreakOut = TRUE;
            break;  

        case    HSB_ACT_TO_SBY  :
            ctrl_sta = prgEvent->command;
            break;  
    
        case    HSB_DOWN    :
            /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
            ctrl_sta = prgEvent->command;
            Terminate(1);
            break;  
    
        case    HSB_STANDALONE  :
            ctrl_sta = prgEvent->command;
            ResetDBCounter();
            ilBreakOut = TRUE;
            break;  

        case    REMOTE_DB :
            /* ctrl_sta is checked inside */
            HandleRemoteDB(prgEvent);
            break;

        case    SHUTDOWN    :
            Terminate(1);
            break;
                        
        case    RESET       :
            ilRc = Reset();
            break;
                        
        case    EVENT_DATA  :
            dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
            DebugPrintItem(TRACE,prgItem);
            DebugPrintEvent(TRACE,prgEvent);
            break;
                    
        case    TRACE_ON :
            dbg_handle_debug(prgEvent->command);
            break;

        case    TRACE_OFF :
            dbg_handle_debug(prgEvent->command);
            break;

        default         :
            dbg(TRACE,"HandleQueues: unknown event");
            DebugPrintItem(TRACE,prgItem);
            DebugPrintEvent(TRACE,prgEvent);
            break;
        } /* end switch */
    } else {
        /* Handle queuing errors */
        HandleQueErr(ilRc);
    } /* end else */
    } while (ilBreakOut == FALSE);

    ilRc = Init_Process();
    if(ilRc != RC_SUCCESS)
    {
    dbg(TRACE,"InitDemhdl: init failed!");
    } 
     

} /* end of HandleQueues */
    
static int CheckFieldForUFRCommand(char *pcpFieldList)
{
	 dbg(DEBUG,"The length of pcpFieldList is <%d>",strlen(pcpFieldList));
	 
	 if( strlen(pcpFieldList) == 0 )
	 {
	 		return RC_FAIL;
	 }
	 
	 if( strstr(pcpFieldList,"URNO") != 0 && strlen(pcpFieldList) == 4 )
	 {
	 	 dbg(DEBUG,"The field list only contains URNO, do not send out messages");
	 	 return RC_FAIL;
	 }
	 else
	 {
	 	return RC_SUCCESS;
	 }
}


/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(EVENT *prpEvent)
{
    int    ilRc           = RC_SUCCESS;         /* Return code */
    int      ilCmd          = 0;

    BC_HEAD *prlBchead       = NULL;
    CMDBLK  *prlCmdblk       = NULL;
    char    *pclSelection    = NULL;
    char    *pclFields       = NULL;
    char    *pclData         = NULL;
    char    *pclRow          = NULL;
    char        clUrnoList[2400];
    char        clTable[34];
    int         ilUpdPoolJob = TRUE;

    prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
    prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
    pclSelection = prlCmdblk->data;
    pclFields    = pclSelection + strlen(pclSelection) + 1;
    pclData      = pclFields + strlen(pclFields) + 1;


    strcpy(clTable,prlCmdblk->obj_name);

    dbg(TRACE,"========== START <%10.10d> ==========",lgEvtCnt);

    /****************************************/
    DebugPrintBchead(TRACE,prlBchead);
    DebugPrintCmdblk(TRACE,prlCmdblk);
    /* 20030630 JIM: may core!: dbg(TRACE,"Command: <%s>,prlCmdblk->command"); */
    dbg(TRACE,"Command: <%s>",prlCmdblk->command);
    dbg(TRACE,"originator follows event = %p ",prpEvent);

    dbg(TRACE,"originator<%d>",prpEvent->originator);
    dbg(TRACE,"selection follows Selection = %p ",pclSelection);
    dbg(TRACE,"selection <%s>",pclSelection);
    dbg(TRACE,"fields    <%s>",pclFields);
    dbg(TRACE,"data      <%s>",pclData);
    /****************************************/

    /*Handle AFT-Data from action*/
    
    ilRc = CheckFieldForUFRCommand(pclFields);

    if ( ( (strcmp(prlCmdblk->command,"UFR") == 0) && (ilRc == RC_SUCCESS) ) ||
    		 (strcmp(prlCmdblk->command,"IFR") == 0) ||
    		 (strcmp(prlCmdblk->command,"DFR") == 0)) 
    {
        ilRc = HandleAftData(FALSE,prlCmdblk->command,pclFields,pclData, NULL);
    }

    /*Handle ULD-data from action*/
    if(strcmp(prlCmdblk->command,"TRIG") == 0)
    {
        ilRc = SendCedaEvent(prpEvent->originator, 0, " ", " ", " ", " ","COS", " ", " ",
                             " ", "Answer", "", 2, 0) ;
    }
    if (!strcmp(prlCmdblk->command,"TRIG") || !strcmp(prlCmdblk->command,"TLX"))
    {
        ilRc = HandleUldData(pclFields,pclData,prlCmdblk->command);
    }

    /*Handle ULD-Data from COSYS-Interface (wmqcdi) */
    if (strcmp(prlCmdblk->command,"COS") == 0)
    {
        ilRc = HandleCosData(pclData);
    }

    /** BLE 16-OCT-2008 **/
    if( !strcmp( clTable, "DLYTAB" ) )
        ilRc = HandleDelayCode(pclFields, pclData, prlCmdblk->command );
    
    dbg(TRACE,"==========  END  <%10.10d> ==========",lgEvtCnt);

    return(RC_SUCCESS);
    
} /* end of HandleData */



static int HandleAftData(int ipDelayInfo, char *pcpCmd, char *pcpFields,char *pcpData, char *pcpResult)
{
/* sample of fields received:
   ALC2,FLTN,DES3,STOA,STOD,ETOA,ETOD,ADID,REGN,ACT3,FTYP,REMP,PSTD,PSTA,GTD1,TIFA,TIFD,BLT1,BLT2,BAZ1,VIAL,VIAN
*/


    
    int ilRc = RC_SUCCESS;
    int ilLoop = 0;
    char clTemp[20] = "\0";
    char clTime[20] = "\0";
    char clResult[256] = "\0";

    char clAlc2[20] = "                   \0";
    char clFltn[20] = "                   \0";
    char clFlns[20] = "                   \0";
    char clDes3[20] = "                   \0";
    char clOrg3[20] = "                   \0";
    char clStoa[20] = "                   \0";
    char clStod[20] = "                   \0";
    char clEtai[20] = "                   \0";
    char clEtdi[20] = "                   \0";
    char clAdid[20] = "                   \0";
    char clRegn[20] = "                   \0";
    char clAct3[20] = "                   \0";
    char clFtyp[20] = "                   \0";
    char clRemp[20] = "                   \0";
    char clPstd[20] = "                   \0";
    char clPsta[20] = "                   \0";
    char clGtd1[20] = "                   \0";
    char clGta1[20] = "                   \0";
    char clTifa[20] = "                   \0";
    char clTifd[20] = "                   \0";
    char clBlt1[20] = "                   \0";
    char clBlt2[20] = "                   \0";
    char clBaz1[20] = "                   \0";
    char clOnbl[20] = "                   \0";
    char clOfbl[20] = "                   \0";
    char clVial[1024] = "\0";
    char clRoute[21] = "\0" ;

    char clFields[1024];
    char clSelection[2048];
    char clData[4096];

    int ilLen = 0;
    int ilLenBlt1 = 0;
    int ilLenBlt2 = 0;
    int ilVian = 0;
    int ilRouteLen;
    long llUrno = 0;

    char *pclItem;
    char *pclPointer;

    dbg(TRACE,"************* HandleAftData ***********");

    if (!strcmp(pcpCmd,"IFR") || !strcmp(pcpCmd,"UFR")) {
      /*
      ** For IFR/UFR commands we are provided an URNO
      ** fetch the required flight data from DB
      ** This is different from a DFR command, where data
      ** are no longer avaible from DB at this point!
      ** (See action.cfg COS sections!)
      */
      pclItem = getItem(pcpFields,pcpData,"URNO");
      if( pclItem != NULL ) {
    llUrno = atol(pclItem);
      }
      else {
    dbg(TRACE,"<HandleAftData> Fatal no URNO received, giving up");
    return RC_FAIL;
      }
      
      sprintf(clFields,
          "ALC2,FLTN,ORG3,DES3,STOA,STOD,ETAI,ETDI,ADID,REGN,ACT3,FTYP,REMP,PSTD,PSTA,GTA1,GTD1,TIFA,TIFD,BLT1,BLT2,BAZ1,VIAL,VIAN,ONBL,OFBL,FTYP,FLNS");
      sprintf(clSelection,"SELECT %s FROM AFTTAB WHERE URNO=%ld",clFields,llUrno);
      dbg(TRACE,"%05d: now read from DB: <%s>",__LINE__,clSelection);
      ilRc = getOrPutDBData(clSelection, clData, START);

      /** BLE: No error handling initially **/
      if( ilRc != DB_SUCCESS )
      {
          dbg( TRACE, "<HandleAftData> Failed to get AFT <%d> record from table", llUrno );
          return RC_FAIL;
      }
      BuildItemBuffer(clData, NULL, 28, ",");

      /* point to retrieved data  */
      pcpFields = clFields;
      pcpData = clData;
    }

    pclItem = getItem(pcpFields,pcpData,"ALC2");
    if( pclItem != NULL ) 
    {
    ilLen = strlen(pclItem);
    strncpy(clAlc2,pclItem,ilLen);
    dbg(TRACE,"clAlc2: <%s> len: %d",clAlc2,ilLen);
    }

    pclItem = getItem(pcpFields,pcpData,"FLTN");
    if( pclItem != NULL ) {
      TrimRight(pclItem);
      ilLen = strlen(pclItem);
      if (ilLen == 3){
    clFltn[0] = '0';
    strncpy(&clFltn[1],pclItem,ilLen);
    ilLen++;
      }
      else {
    strncpy(clFltn,pclItem,ilLen);
      }

    dbg(TRACE,"clFltn: <%s> len: %d",clFltn,ilLen);
    }

    pclItem = getItem(pcpFields,pcpData,"FLNS");
    if( pclItem != NULL ) 
    {
    ilLen = strlen(pclItem);
    strncpy(clFlns,pclItem,ilLen);
    dbg(TRACE,"clFlns: <%s> len: %d",clFlns,ilLen);
    }

    pclItem = getItem(pcpFields,pcpData,"ORG3");
    if( pclItem != NULL ) 
    {
    ilLen = strlen(pclItem);
    strncpy(clOrg3,pclItem,ilLen);
    dbg(TRACE,"clOrg3: <%s> len: %d",clOrg3,ilLen);
    }

    pclItem = getItem(pcpFields,pcpData,"DES3");
    if( pclItem != NULL ) 
    {
    ilLen = strlen(pclItem);
    strncpy(clDes3,pclItem,ilLen);
    dbg(TRACE,"clDes3: <%s> len: %d",clDes3,ilLen);
    }

    pclItem = getItem(pcpFields,pcpData,"ETAI");
    if( pclItem != NULL ) 
    {
    ilLen = strlen(pclItem);
    UtcToLocal(pclItem);
    strncpy(clEtai,pclItem,ilLen);
    dbg(TRACE,"clEtai: <%s> len: %d",clEtai,ilLen);
    }

    pclItem = getItem(pcpFields,pcpData,"ETDI");
    if( pclItem != NULL ) 
    {
    ilLen = strlen(pclItem);
    UtcToLocal(pclItem);
    strncpy(clEtdi,pclItem,ilLen);
    dbg(TRACE,"clEtdi: <%s> len: %d",clEtdi,ilLen);
    }

    pclItem = getItem(pcpFields,pcpData,"STOA");
    if( pclItem != NULL ) 
    {
    ilLen = strlen(pclItem);
    UtcToLocal(pclItem);
    strncpy(clStoa,pclItem,ilLen);
    dbg(TRACE,"clStoa: <%s> len: %d",clStoa,ilLen);
    }

    pclItem = getItem(pcpFields,pcpData,"STOD");
    if( pclItem != NULL ) 
    {
    ilLen = strlen(pclItem);
    UtcToLocal(pclItem);
    strncpy(clStod,pclItem,ilLen);
    dbg(TRACE,"clStod: <%s> len: %d",clStod,ilLen);
    }

    pclItem = getItem(pcpFields,pcpData,"ADID");
    if( pclItem != NULL ) 
    {
    ilLen = strlen(pclItem);
    strncpy(clAdid,pclItem,ilLen);
    dbg(TRACE,"clAdid: <%s> len: %d",clAdid,ilLen);
    }

    pclItem = getItem(pcpFields,pcpData,"REGN");
    if( pclItem != NULL ) 
    {
    ilLen = strlen(pclItem);
    strncpy(clRegn,pclItem,ilLen);
    dbg(TRACE,"clRegn: <%s> len: %d",clRegn,ilLen);
    }

    pclItem = getItem(pcpFields,pcpData,"ACT3");
    if( pclItem != NULL ) 
    {
    ilLen = strlen(pclItem);
    strncpy(clAct3,pclItem,ilLen);
    dbg(TRACE,"clAct3: <%s> len: %d",clAct3,ilLen);
    }

    pclItem = getItem(pcpFields,pcpData,"FTYP");
    if( pclItem != NULL ) 
    {
    ilLen = strlen(pclItem);
    strncpy(clFtyp,pclItem,ilLen);
    dbg(TRACE,"clFtyp: <%s> len: %d",clFtyp,ilLen);
    }

    pclItem = getItem(pcpFields,pcpData,"REMP");
    if( pclItem != NULL ) 
    {
    ilLen = strlen(pclItem);
    strncpy(clRemp,pclItem,ilLen);
    dbg(TRACE,"clRemp: <%s> len: %d",clRemp,ilLen);
    }

    pclItem = getItem(pcpFields,pcpData,"PSTD");
    if( pclItem != NULL ) 
    {
    ilLen = strlen(pclItem);
    strncpy(clPstd,pclItem,ilLen);
    dbg(TRACE,"clPstd: <%s> len: %d",clPstd,ilLen);
    }

    pclItem = getItem(pcpFields,pcpData,"PSTA");
    if( pclItem != NULL ) 
    {
    ilLen = strlen(pclItem);
    strncpy(clPsta,pclItem,ilLen);
    dbg(TRACE,"cgPsta: <%s> len: %d",clPsta,ilLen);
    }

    pclItem = getItem(pcpFields,pcpData,"GTD1");
    if( pclItem != NULL ) 
    {
    ilLen = strlen(pclItem);
    strncpy(clGtd1,pclItem,ilLen);
    dbg(TRACE,"clGtd1: <%s> len: %d",clGtd1,ilLen);
    }

    pclItem = getItem(pcpFields,pcpData,"GTA1");
    if( pclItem != NULL ) 
    {
    ilLen = strlen(pclItem);
    strncpy(clGta1,pclItem,ilLen);
    dbg(TRACE,"clGta1: <%s> len: %d",clGta1,ilLen);
    }

    pclItem = getItem(pcpFields,pcpData,"TIFA");
    if( pclItem != NULL ) 
    {
    ilLen = strlen(pclItem);
    UtcToLocal(pclItem);
    strncpy(clTifa,pclItem,ilLen);
    dbg(TRACE,"clTifa: <%s> len: %d",clTifa,ilLen);
    }

    pclItem = getItem(pcpFields,pcpData,"TIFD");
    if( pclItem != NULL ) 
    {
    ilLen = strlen(pclItem);
    UtcToLocal(pclItem);
    strncpy(clTifd,pclItem,ilLen);
    dbg(TRACE,"clTifd: <%s> len: %d",clTifd,ilLen);
    }

    pclItem = getItem(pcpFields,pcpData,"BLT1");
    if( pclItem != NULL ) 
    {
    strcpy(clBlt1,pclItem);
    TrimRight(clBlt1);
    dbg(TRACE,"clBlt1: <%s> ",clBlt1);
    }

    pclItem = getItem(pcpFields,pcpData,"BLT2");
    if( pclItem != NULL ) 
    {
    strcpy(clBlt2,pclItem);
    TrimRight(clBlt2);
    dbg(TRACE,"clBlt2: <%s>",clBlt2);
    }

    pclItem = getItem(pcpFields,pcpData,"BAZ1");
    if( pclItem != NULL ) 
    {
    ilLen = strlen(pclItem);
    strncpy(clBaz1,pclItem,ilLen);
    dbg(TRACE,"clBaz1: <%s> len: %d",clBaz1,ilLen);
    }

    pclItem = getItem(pcpFields,pcpData,"ONBL");
    if( pclItem != NULL ) 
    {
    ilLen = strlen(pclItem);
    UtcToLocal(pclItem);
    strncpy(clOnbl,pclItem,ilLen);
    dbg(TRACE,"clOnbl: <%s> len: %d",clOnbl,ilLen);
    }
    pclItem = getItem(pcpFields,pcpData,"OFBL");
    if( pclItem != NULL ) 
    {
    ilLen = strlen(pclItem);
    UtcToLocal(pclItem);
    strncpy(clOfbl,pclItem,ilLen);
    dbg(TRACE,"clOfbl: <%s> len: %d",clOfbl,ilLen);
    }

    pclItem = getItem(pcpFields,pcpData,"VIAN");
    if( pclItem != NULL ) {
      ilVian = atoi(pclItem);
      if (ilVian != 0) {
    pclItem = getItem(pcpFields,pcpData,"VIAL");
    if( pclItem != NULL ){
      if (ilVian > MaxRoute) ilVian = MaxRoute;
      getVialItems(ilVian,pclItem,clRoute);
      /* fill with ' ' to required length */
      /*
      ilRouteLen = ROUTELEN-strlen(clRoute);
      strncat(clRoute,"                    ",ilRouteLen);
      dbg(TRACE,"clRoute: <%s> len: %d",clRoute,ilRouteLen);
      */
    }
      }
      else {/* fill in blanks */
    strcpy(clRoute,"");
    dbg(TRACE,"clRoute: <%s> len: %d",clRoute,ROUTELEN);
      }
    }
    else {/* fill in blanks */
      strcpy(clRoute,"");
      dbg(TRACE,"clRoute: <%s> len: %d",clRoute,ROUTELEN);
    }
    if((clAdid[0] == 'A') || (clAdid[0] == 'B'))
    {
    pclPointer = clResult;
    memset(clResult,' ',sizeof(clResult));
    clResult[255] = '\0';
    strncpy(pclPointer,"FLT",3);
    pclPointer += 3;
    strncpy(pclPointer,clAlc2,3);
    pclPointer += 3;
    strncpy(pclPointer,clFltn,4);
    pclPointer += 4;
    strncpy(pclPointer,clFlns,1);
    pclPointer += 1;
    strncpy(pclPointer,clOrg3,3);
    pclPointer += 3;
    strncpy(pclPointer,clDes3,3);
    pclPointer += 3;
    strncpy(pclPointer,clStoa,8);
    pclPointer += 8;
    strncpy(pclPointer,"A",1);
    pclPointer += 1;
    strncpy(pclPointer,clPsta,3);
    pclPointer += 3;
    strncpy(pclPointer,clRegn,8);
    pclPointer += 8;
    strncpy(pclPointer,clAct3,3);
    pclPointer += 3;
    strncpy(pclPointer,clRemp,2);
    pclPointer += 2;
    strncpy(pclPointer,clGta1,4);
    pclPointer += 4;
    /* Status */
    if(strcmp(pcpCmd,"DFR") == 0)
    {
        strncpy(pclPointer,"DEL",3);
        pclPointer += 3;
    }
    else
    {
        if(clFtyp[0] == 'X')
        {
        strncpy(pclPointer,"CNC",3);
        pclPointer += 3;
        }
        else
        {
        {
            strncpy(pclPointer,"OP",2);
            pclPointer += 3;
        }
        }
    }
    /* Indicator */
    if(!strcmp(pcpCmd,"UFR")) {
      strncpy(pclPointer,"CHG",3);
    }
    else if(!strcmp(pcpCmd,"IFR")) {
      strncpy(pclPointer,"INS",3);
    }
    else if(!strcmp(pcpCmd,"DFR")) {
      strncpy(pclPointer,"DEL",3);
    }
    else {
      strncpy(pclPointer,"   ",3);
      dbg(TRACE,"<HandleAftData> Unknown command <%s> -- indicator not filled",pcpCmd);
    }
    pclPointer += 3;
    /* Flight Suffix */
    strcpy(pclPointer," "); /* PRF 6892 */
    pclPointer += 1;
    /* Route */
    if(strcmp(pcpCmd,"DFR") == 0) {
      /* If deleting a flight -> route has to be blank! */
      strncat(pclPointer,"                    ",20);
      pclPointer += 20;
    }
    else {
      strncpy(pclPointer,clOrg3,3);
      strcat(pclPointer," ");
      pclPointer += 4;
      strncpy(pclPointer,clRoute,strlen(clRoute));
      pclPointer += strlen(clRoute);
      strncpy(pclPointer,clDes3,3);
      ilRouteLen = ROUTELEN-strlen(clRoute);
      strncat(pclPointer,"            ",ilRouteLen);
      pclPointer += 4 + ilRouteLen;
    }
    strncpy(pclPointer,&clOnbl[8],4);
    pclPointer += 4;
    strncpy(pclPointer,clOnbl,8);
    pclPointer += 8;
    strncpy(pclPointer,&clEtai[8],4);
    pclPointer += 4;
    strncpy(pclPointer,clEtai,8);
    pclPointer += 8;
    strncpy(pclPointer,&clStoa[8],4);
    pclPointer += 4;
    strncpy(pclPointer,clStoa,8);
    pclPointer += 8;
    ilLenBlt1 = strlen(clBlt1);
    strncpy(pclPointer,clBlt1,ilLenBlt1);
    pclPointer += ilLenBlt1;
    ilLenBlt2 = strlen(clBlt2);
    if(ilLenBlt2 != 0)
    {
        pclPointer[0] = ',';
        pclPointer++;
        strncpy(pclPointer,clBlt2,ilLenBlt2);
    }
    else {
      /*
      ** add ',' if no belt
      */
      if (ilLenBlt1 == 0) {
        pclPointer[0] = ',';
        pclPointer++;
      }
    }
    clResult[120] = '\0';
    
    
    dbg(TRACE,"Arrival flight data: \n<%s>, size: %d ",clResult,strlen(clResult));
    ilLoop = 1;
    while (get_real_item(clTemp,cgSendAFTModid,ilLoop) > 0)
    {
        dbg(TRACE,"%05d: Now send online-flight-data to modid: <%s>",__LINE__,clTemp);
        ilRc = SendCedaEvent(atol(clTemp), 0, " ", " ", " ", " ","COS", " ", " ",
                           " ", clResult, "", 2, 0) ;
        ilLoop++;
    }
    }
    if((clAdid[0] == 'D') || (clAdid[0] == 'B'))
    {
    pclPointer = clResult;
    memset(clResult,' ',sizeof(clResult));
    clResult[255] = '\0';
    strncpy(pclPointer,"FLT",3);
    pclPointer += 3;
    strncpy(pclPointer,clAlc2,3);
    pclPointer += 3;
    strncpy(pclPointer,clFltn,4);
    pclPointer += 4;
    strncpy(pclPointer,clFlns,1);
    pclPointer += 1;
    strncpy(pclPointer,clOrg3,3);
    pclPointer += 3;
    strncpy(pclPointer,clDes3,3);
    pclPointer += 3;
    strncpy(pclPointer,clStod,8);
    pclPointer += 8;
    strncpy(pclPointer,"D",1);
    pclPointer += 1;
    strncpy(pclPointer,clPstd,3);
    pclPointer += 3;
    strncpy(pclPointer,clRegn,8);
    pclPointer += 8;
    strncpy(pclPointer,clAct3,3);
    pclPointer += 3;
    strncpy(pclPointer,clRemp,2);
    pclPointer += 2;
    strncpy(pclPointer,clGtd1,4);
    pclPointer += 4;
    if(strcmp(pcpCmd,"DFR") == 0)
    {
        strncpy(pclPointer,"DEL",3);
        pclPointer += 3;
    }
    else
    {
        if(clFtyp[0] == 'X')
        {
        strncpy(pclPointer,"CNC",3);
        pclPointer += 3;
        }
        else
        {
        {
            strncpy(pclPointer,"OP",2);
            pclPointer += 3;
        }
        }
    }
    /* Indicator */
    if(!strcmp(pcpCmd,"UFR")) {
      strncpy(pclPointer,"CHG",3);
    }
    else if(!strcmp(pcpCmd,"IFR")) {
      strncpy(pclPointer,"INS",3);
    }
    else if(!strcmp(pcpCmd,"DFR")) {
      strncpy(pclPointer,"DEL",3);
    }
    else {
      strncpy(pclPointer,"   ",3);
      dbg(TRACE,"<HandleAftData> Unknown command <%s> -- indicator not filled",pcpCmd);
    }
    pclPointer += 3;
    /* Flight Suffix */
    strcpy(pclPointer," "); /* PRF 6892 */
    pclPointer += 1;
    /* Route */
    if(strcmp(pcpCmd,"DFR") == 0) {
      /* If deleting a flight -> route has to be blank! */
      strncat(pclPointer,"                    ",20);
      pclPointer += 20;
    }
    else {
      strncpy(pclPointer,clOrg3,3);
      strcat(pclPointer," ");
      pclPointer += 4;
      strncpy(pclPointer,clRoute,strlen(clRoute));
      pclPointer += strlen(clRoute);
      strncpy(pclPointer,clDes3,3);
      ilRouteLen = ROUTELEN-strlen(clRoute);
      strncat(pclPointer,"            ",ilRouteLen);
      pclPointer += 4 + ilRouteLen;
    }
    strncpy(pclPointer,&clOfbl[8],4);
    pclPointer += 4;
    strncpy(pclPointer,clOfbl,8);
    pclPointer += 8;
    strncpy(pclPointer,&clEtdi[8],4);
    pclPointer += 4;
    strncpy(pclPointer,clEtdi,8);
    pclPointer += 8;
    strncpy(pclPointer,&clStod[8],4);
    pclPointer += 4;
    strncpy(pclPointer,clStod,8);
    pclPointer += 8;
    strncpy(pclPointer,clBaz1,4);
    pclPointer += 4;
    pclPointer[0] = '\0';
    dbg(TRACE,"Departure flight data: \n<%s>, size: %d ",clResult,strlen(clResult));

    if( ipDelayInfo == FALSE )
    {
        ilLoop = 1;
        while (get_real_item(clTemp,cgSendAFTModid,ilLoop) > 0)
        {
            dbg(TRACE,"%05d: Now send online-flight-data to modid: <%s>",__LINE__,clTemp);
            ilRc = SendCedaEvent(atol(clTemp), 0, " ", " ", " ", " ","COS", " ", " ",
                                 " ", clResult, "", 2, 0) ;
            ilLoop++;
        }
    }
    else
        strcpy( pcpResult, clResult );
    }
    return ilRc;
}


static int getOrPutDBData(char *pcpSelection, char *pcpData, unsigned int ipMode)
{
  int ilGetRc = DB_SUCCESS;
  short slFkt = 0;
  short slCursor = 0;
  char errBuff[128];

  slFkt = ipMode;
  slCursor = 0;
  ilGetRc = sql_if (slFkt, &slCursor, pcpSelection, pcpData);
  close_my_cursor (&slCursor);
  if (ilGetRc == DB_SUCCESS) {
    return RC_SUCCESS;
  }
  else {
    if (ilGetRc != NOTFOUND) {
      get_ora_err(ilGetRc, &errBuff[0]);
      dbg (TRACE, "<getOrPutDBData> Error getting Oracle data error <%s>", &errBuff[0]);
      return RC_FAIL;
    }
    else  return RC_NODATA;
  }
}

#define RESULTSIZE 512

typedef struct {
  char UldNum[11];
  char delim;
  char content;
  char delim1;
  char dest[3];
  char delim2;
  char offlInd;
  char delim3;
  char offlReason[10];
  char delim4;
} UldBlock;

typedef union {
  UldBlock structData;
  char flatData[19];
} uldVariant;

static char LOASelection [] = "ULDT,SSTP,APC3";
static char ULDSelection [] = "ULDN,CONT,REMA,DEST";
static char *pclResult = NULL;
static int ilExpandCount = 1;


static int appendUldList(long lpUrno, char *pcpCmd, char *pclAdid)
{
  int   ilGetRc = DB_SUCCESS;
  short slFkt = 0;
  short slCursor = 0;
  int   ilBufferSize;
  char  clSelect[512];
  char  clData[512];
  uldVariant clUldBlock;
  char  *pclItem;
  int   ilUldNum = 0;
  int   ilLen;
  int   ilItemCount=0;
  char  clULDNumItem[8];
  char  clContItem[8];
  char  clDestItem[8];
  char  clOfflReason[16];
  char  clSelection[128];
  
  if (!strcmp(pcpCmd, "TLX")) {
    sprintf(clSelect, "SELECT %s FROM LOATAB WHERE FLNU=%ld AND ULDT <> ' '", LOASelection, lpUrno);
    sprintf(clULDNumItem,"ULDT");
    sprintf(clContItem,"SSTP");
    sprintf(clDestItem,"APC3");
    sprintf(clSelection,"%s",LOASelection);
    ilItemCount=3;
  }
  else if (!strcmp(pcpCmd, "TRIG")) {
    sprintf(clSelect, "SELECT %s FROM ULDTAB WHERE FLNU=%ld AND CONF='Y' AND ULDN <> ' '", ULDSelection, lpUrno);
    sprintf(clULDNumItem,"ULDN");
    sprintf(clContItem,"CONT");
    sprintf(clDestItem,"DEST");
    sprintf(clSelection,"%s",ULDSelection);
    ilItemCount=4;
  }
  slFkt = START;
  dbg(TRACE,"%05d: uld-selection: <%s>",__LINE__,clSelect);
  while ((ilGetRc = sql_if (slFkt, &slCursor, clSelect, clData)) == DB_SUCCESS) {
    if (ilGetRc == DB_SUCCESS) {
      BuildItemBuffer(clData, NULL, ilItemCount, ",");
      /*
      ** check remaining buffersize first
      */
      ilBufferSize = strlen(pclResult) + sizeof(UldBlock) + 1;  
      if (ilBufferSize >= RESULTSIZE*ilExpandCount) {
    dbg(TRACE,"<appendUldList> Expanding result buffer");
    ilExpandCount++;
    pclResult = realloc(pclResult, ilExpandCount*RESULTSIZE);
      }
      if (ilUldNum > 0) strcat(pclResult, ","); /* separate list by , */
      pclItem = getItem(clSelection, clData, clULDNumItem);
      if (pclItem == NULL) {
    dbg(TRACE,"<appendUldList> Unable to fetch %s",clULDNumItem);
    return -1;
      }

      ilLen = strlen(pclItem);
      if (ilLen > 11) ilLen = 11;
      sprintf(clUldBlock.structData.UldNum,"%-11.11s",pclItem);
      clUldBlock.structData.delim = '/';
      pclItem = getItem(clSelection, clData, clContItem);
      if (pclItem == NULL) {
    dbg(TRACE,"<appendUldList> Unable to fetch %s",clContItem);
    return -1;
      }
      if (pclItem[0] == 'E')
      {
      clUldBlock.structData.content = 'X';
      }
      else
      {
      clUldBlock.structData.content = pclItem[0];
      }
      clUldBlock.structData.delim1 = '/';
      pclItem = getItem(clSelection, clData, clDestItem);
      if (pclItem == NULL) {
    dbg(TRACE,"<appendUldList> Unable to fetch %s",clDestItem);
    return -1;
      }
      sprintf(clUldBlock.structData.dest,"%-3.3s",pclItem);

      pclItem = getItem(clSelection, clData, "REMA");
      if (pclItem == NULL) {
    strcpy(clOfflReason,"          ");
      }
      else sprintf(clOfflReason,"%-10.10s",pclItem);

      if(pclAdid[0] == 'D')
      {
      clUldBlock.structData.delim2 = '/';
      clUldBlock.structData.offlInd = ' ';
      clUldBlock.structData.delim3 = '/';
      strcpy(clUldBlock.structData.offlReason, clOfflReason);
      clUldBlock.structData.delim4 = '\0';
      }
      else if (pclAdid[0] == 'A')
      {
      clUldBlock.structData.delim2 = '\0';
      clUldBlock.structData.offlInd = '\0';
      clUldBlock.structData.delim3 = '\0';
      clUldBlock.structData.offlReason[0] = '\0';
      clUldBlock.structData.delim4 = '\0';
      
      } 
      strcat(pclResult, clUldBlock.flatData);
      ilUldNum++;
    }
    slFkt = NEXT;
  }
  close_my_cursor (&slCursor);
  dbg(TRACE,"<appendUldList> <%d> Container counted for flight", ilUldNum);
  return ilUldNum;
}


static char AFTSelection [] =  "ALC2,FLTN,REGN,ORG3,DES3,TIFA,TIFD,ADID,PSTA,PSTD,ETOA,ETOD";

static int HandleUldData(char *pcpFields,char *pcpData, char *pcpCmd)
{
  char *pclItem;
  char *pclPointer;
  long llUrno=0;
  long llUldUrno=0;
  char clSelection[256];
  char clData[256];
  char clAdid[2];
  char clDate[9];
  char clTime[5];
  char clTIFX[5];
  char clETOX[5];
  char clPSTX[5];
  char clDes[4];
  char clBuf[32];
  int ilRc = RC_SUCCESS;
  
  dbg(TRACE,"************* HandleUldData ***********");

  if (pclResult == NULL) pclResult = malloc (RESULTSIZE);
  strcpy(pclResult,"");

  pclItem = getItem(pcpFields,pcpData,"FLNU");
  if( pclItem == NULL ) {
    dbg (TRACE, "<HandleUldData> Unable to get FLNU -- FATAL");
    return RC_FAIL;
  }  
  llUrno = atol(pclItem);

  pclItem = getItem(pcpFields,pcpData,"URNO");
  if (pclItem != NULL) llUldUrno = atol(pclItem);

  sprintf(clSelection, "SELECT %s FROM AFTTAB WHERE URNO=%d", AFTSelection, llUrno);
  dbg(TRACE,"%05d: now read from DB: <%s>",__LINE__,clSelection);
  ilRc = getOrPutDBData(clSelection, clData, START);

  if (ilRc != RC_SUCCESS) return ilRc; 

  BuildItemBuffer(clData, NULL, 12, ",");

  pclItem = getItem(AFTSelection, clData, "ADID");
  if (pclItem == NULL) {
    dbg (TRACE, "<HandleUldData> Unable to get ADID -- FATAL");
    return RC_FAIL;
  }
  strcpy(clAdid, pclItem);

  if (!strcmp(clAdid, "A")) {
    strcpy(clTIFX, "TIFA");
    strcpy(clETOX, "ETOA");
    strcpy(clPSTX, "PSTA");
  }
  else if (!strcmp(clAdid, "D")) {
    strcpy(clTIFX, "TIFD");
    strcpy(clETOX, "ETOD");
    strcpy(clPSTX, "PSTD");
  }

  pclItem = getItem(AFTSelection, clData, clTIFX);
  if (pclItem == NULL) {
    dbg (TRACE, "<HandleUldData> Unable to get %s -- FATAL", clTIFX);
    return RC_FAIL;
  }
  UtcToLocal(pclItem);
  sprintf(clDate, "%8.8s",pclItem);

  pclItem = getItem(AFTSelection, clData, clETOX);
  if (pclItem == NULL) {
    dbg (TRACE, "<HandleUldData> Unable to get %s -- FATAL", clETOX);
    return RC_FAIL;
  }
  UtcToLocal(pclItem);
  sprintf(clTime,"%4.4s",&pclItem[8]);
    
  strcpy(pclResult, "ULD");
  pclItem = getItem(AFTSelection, clData, "ALC2");
  if (pclItem == NULL) {
    dbg (TRACE, "<HandleUldData> Unable to get ALC2 -- FATAL");
    return RC_FAIL;
  }
  sprintf(clBuf,"%-3.3s", pclItem);
  strcat(pclResult, clBuf);

  pclItem = getItem(AFTSelection, clData, "FLTN");
  if (pclItem == NULL) {
    dbg (TRACE, "<HandleUldData> Unable to get FLTN -- FATAL");
    return RC_FAIL;
  }
  sprintf(clBuf,"%-5.5s", pclItem);
  strcat(pclResult, clBuf);
  
  pclItem = getItem(AFTSelection, clData, "REGN");
  if (pclItem == NULL) {
    dbg (TRACE, "<HandleUldData> Unable to get REGN -- FATAL");
    return RC_FAIL;
  }
  sprintf(clBuf,"%-8.8s", pclItem);
  strcat(pclResult, clBuf);

  pclItem = getItem(AFTSelection, clData, "ORG3");
  if (pclItem == NULL) {
    dbg (TRACE, "<HandleUldData> Unable to get ORG3 -- FATAL");
    return RC_FAIL;
  }
  strcat(pclResult, pclItem);

  pclItem = getItem(AFTSelection, clData, "DES3");
  if (pclItem == NULL) {
    dbg (TRACE, "<HandleUldData> Unable to get DES3 -- FATAL");
    return RC_FAIL;
  }
  strcat(pclResult, pclItem);
  strcpy(clDes, pclItem);

  strcat(pclResult, clDate);
  strcat(pclResult, clAdid);

  pclItem = getItem(AFTSelection, clData, clPSTX);
  if (pclItem == NULL) {
    dbg (TRACE, "<HandleUldData> Unable to get PSTA -- FATAL");
    return RC_FAIL;
  }
  pclItem[3] = '\0';
  strcat(pclResult, pclItem);
  
  strcat(pclResult, clTime);

  if (!strcmp(pcpCmd, "TRIG")) strcat(pclResult,"Y");
  else strcat(pclResult," ");
  if ((appendUldList(llUrno,pcpCmd,clAdid) > 0) || (strcmp(cgSendZeroUlds,"YES") == 0)) 
  {
      dbg(TRACE,"Load data: (first 1000 characters)\n<%.1000s>, size: %d ",pclResult,strlen(pclResult));
      ilRc = SendCedaEvent(igSendULDModid, 0, " ", " ", " ", " ","COS", " ", " ",
               " ", pclResult, "", 2, 0) ;
  }
  else
  {
      dbg(TRACE,"****************No ULDs found*********************");
  }

  return ilRc;
}


/* This function modified to handle OK2LOAD messages ... Thanooj */ 
/* This function was modified lot of places, consider its a new function all together */
static int HandleCosData(char *pcpData)
{
    int ilRc = RC_SUCCESS;
    long llMqrUrno = 0;
    long llURNO[MAXCOUNT];
    long llFlUrno = 0;
    long llLoaUrno = 0;
    int ilOKLTimeDiff = 0;
    int i;
    short slSqlFunc = 0;
    short slCursor = 0;
    char clSqlBuf[256] = "\0";
    char clData[200] = "\0";
    char clMessage[4000] = "\0";
    char clDataArea[4000] = "\0";
    char pclErr[2560] = "\0";
    char clFltn[16];
    char clAlc[8];
    char clFlns[8];
    char clFkey[32];
    char clFlda[16];
    char pclTIFD[20];
    char tempUld[16];
    BRSInputType *BRSdata;
    char clURNO[32];
    int ilFirstMatch = FALSE;
    int ilIsNotUnique = FALSE;
    int ilCount = 0;

    /* Added */
    OK2LoadType *OK2data;
    char msgName[4] = "\0";
    char xmlMsg[1000] = "\0";
    char *pclDataArea;
    char timeVal[20]    = "\0";

    dbg(TRACE,"%05d: ========== Start HandleCosData ==========",__LINE__);

    /* Get the URNO of MQRTAB */
    llMqrUrno = atol(pcpData);

    /* Below line will fetch the message(DATA) from MQRTAB for URNO */
    slSqlFunc = START;
    slCursor = 0;
    sprintf(clSqlBuf,"SELECT DATA,TPUT FROM MQRTAB WHERE URNO = %d",llMqrUrno);
    ilRc = sql_if(slSqlFunc,&slCursor,clSqlBuf,clDataArea);
    if (ilRc != DB_SUCCESS)
    {
        get_ora_err(ilRc,pclErr);
        dbg(TRACE,"<HandleCosData> Error reading MQRTAB: ilRc <%d>\nORA-ERR <%s>\nSqlBuf <%s>",ilRc,pclErr,clSqlBuf);
        sleep(120);
        close_my_cursor(&slCursor);
        commit_work();
        slCursor = 0;
        Terminate(1);
        ilRc = RC_FAIL;
    }
    close_my_cursor(&slCursor);
    commit_work();
    slCursor = 0; /* Make sure cursor points to zero, once DB access is over. */

    pclDataArea = clDataArea;
    strcpy(clMessage,pclDataArea);
    ConvertDbStringToClient(clMessage); /* check for any special characters available in the message */
    dbg(TRACE,"%05d: ------------- Message received: -------------- \n<%s>",__LINE__,clMessage);
    pclDataArea = pclDataArea + strlen(pclDataArea) + 1;
    strncpy(timeVal,pclDataArea,14);


    /* Check if the message is from BRS or OK2LOAD */
    strncpy(msgName,clMessage,3);
    msgName[3] = '\0';

    if(strcmp(msgName,"OKL")==0) 
    {
        /* Prepare the message & send to OK2 Message queue. */
        dbg(TRACE,"<HandleCosData> Its OK2LOAD Message "); 
        
        /* Do necessary checks, if flight is available then do send the ULD info to OK2LOAD MQ */

        if (strlen(clMessage)!=40)
        { 
            dbg(DEBUG,"<HandleCosData> Its OK2LOAD Message - Message format is wrong");
             
            /* acknowledge the Message in MQRTAB when message format is wrong. */
            slSqlFunc = START;
            slCursor = 0;
            sprintf(clSqlBuf,"UPDATE MQRTAB SET STAT = 'ERROR' WHERE URNO = %d",llMqrUrno);
            ilRc = sql_if(slSqlFunc,&slCursor,clSqlBuf,clDataArea);
            if (ilRc != DB_SUCCESS)
            {
                get_ora_err(ilRc,pclErr);
                dbg(TRACE,"<HandleCosData> Error reading MQRTAB: ilRc <%d>\nORA-ERR <%s>\nSqlBuf <%s>",ilRc,pclErr,clSqlBuf);
                sleep(120);
                close_my_cursor(&slCursor);
                commit_work();
                slCursor = 0;
                Terminate(1);
                ilRc = RC_FAIL;
            }
            close_my_cursor(&slCursor);
            commit_work();
            slCursor = 0;
        }
        else
        {
            OK2data = (OK2LoadType *)clMessage;

            dbg(DEBUG,"<HandleCosData> Ok2Data <%s>",OK2data);

            /*
            Check Flight is available & BRS message is available or not?

            Its a OKL message .. process the message, check the info available in DB, if not available then insert into DB.
            */

            for (i=0;i<MAXCOUNT;i++) llURNO[i]=0;
            
            strncpy(clAlc,OK2data->alc,3); /* ALC first */
            clAlc[3] = '\0';
            strncpy(clFltn,OK2data->fltn,6); /* FLNO */
            clFltn[6] = '\0';
            strncpy(clFlns,OK2data->flns,1); /* Suffix */
            clFlns[1] = '\0';
            strncpy(clFlda,OK2data->flda,8); /* Flight Day (local) */
            clFlda[8] = '\0';
            strncpy(tempUld,OK2data->uldNum,12);
            tempUld[12] = '\0';

            dbg(DEBUG,"<HandleCosData> FLDA: <%s>",clFlda);


            /*createFkey to query the DB for departure flight */
            createFkey(clAlc,clFltn,clFlns,clFlda,"D",&clFkey[0]); /* Got changed .. Thanooj */
 
            
            sprintf(clSqlBuf,"SELECT URNO, TIFD FROM AFTTAB WHERE FLDA = '%s' AND FKEY LIKE '%s'",clFlda, clFkey);
            dbg(DEBUG,"<HandleCosData> Searching flight: <%s>",clSqlBuf);

            slSqlFunc = START;
            slCursor = 0;
            while (((ilRc = sql_if(slSqlFunc,&slCursor,clSqlBuf,clData)) == DB_SUCCESS) && ilCount < MAXCOUNT)
            {
                if (ilRc == DB_SUCCESS)
                {
                    get_fld(clData,FIELD_1,STR,10,clURNO);
                    get_fld(clData,FIELD_2,STR,14,pclTIFD);

                    if (ilFirstMatch == FALSE) 
                    {
                        ilFirstMatch = TRUE;
                        llURNO[ilCount] = atol(clURNO);
                        ilCount++;
                    }
                    else 
                    {
                      llURNO[ilCount] = atol(clURNO);
                      ilCount++;
                      ilIsNotUnique = TRUE;
                    }
                }
                else 
                {
                    get_ora_err(ilRc,pclErr);
                    dbg(TRACE,"<HandleCosData> Error reading MQRTAB:16861686 ilRc <%d>\nORA-ERR <%s>\nSqlBuf <%s>",ilRc,pclErr,clSqlBuf);
                    sleep(120);
                    close_my_cursor(&slCursor);
                    commit_work();
                    slCursor = 0;
                    Terminate(1);
                    ilRc = RC_FAIL;
                }
                slSqlFunc = NEXT;
            }
        
            /* Close the cursor, commit work & make cursor points to zero */
            close_my_cursor(&slCursor);
            commit_work();
            slCursor = 0;

            /* More number of flights found. Error. */
            if (ilIsNotUnique)
            {
                dbg(TRACE,"<HandleCosData> Fatal error found %d flights -> selection not unique <%s>", ilCount,clSqlBuf);
            }

            /*
            ** now fill in the LOATAB fields of the associated flight
            */
            if (ilIsNotUnique == FALSE && ilCount != 0) 
            {
                /*
                ** Check if the requested container already existing ?
                */
                ilCount = 0;
                sprintf(clSqlBuf,"SELECT URNO FROM LOATAB WHERE FLNU='%ld' AND ULDT='%12.12s' AND DSSN='BRS'",
                                                                                  llURNO[0],OK2data->uldNum);
                slSqlFunc = START;
                slCursor = 0;
                llFlUrno = llURNO[0];

                while (((ilRc = sql_if(slSqlFunc,&slCursor,clSqlBuf,clURNO)) == DB_SUCCESS) && ilCount < MAXCOUNT)
                {
                    if (ilRc == DB_SUCCESS)
                    {
                        if (ilFirstMatch == FALSE) 
                        {
                            ilFirstMatch = TRUE;
                            llURNO[ilCount] = atol(clURNO);
                            ilCount++;
                        }
                        else 
                        {
                            llURNO[ilCount] = atol(clURNO);
                            ilCount++;
                            ilIsNotUnique = TRUE;
                        }
                    }
                    else 
                    {
                        get_ora_err(ilRc,pclErr);
                        dbg(TRACE,"<HandleCosData> Error reading MQRTAB:16861686 ilRc <%d>\nORA-ERR <%s>\nSqlBuf <%s>",ilRc,pclErr,clSqlBuf);
                        sleep(120);
                        close_my_cursor(&slCursor);
                        commit_work();
                        slCursor = 0;
                        Terminate(1);
                        ilRc = RC_FAIL;
                    }
                    slSqlFunc = NEXT;
                }

                close_my_cursor(&slCursor);
                commit_work();
                slCursor = 0;

                if (ilCount == 0) 
                {
                    /*
                     ** This container doesn't exist 
                     ** insert it now
                     */
                     getFreeUrno(clURNO);
                     sprintf(clSqlBuf,
                        "INSERT INTO LOATAB (ULDT,APC3,FLNO,FLNU,URNO,DSSN,DSSO,TYPE,TIME) "
                        "VALUES('%12.12s','%3.3s','%3.3s%5.5s%c','%ld','%s','BRS','OKL','ULD','%14.14s')",
                        OK2data->uldNum,OK2data->des3,OK2data->alc,OK2data->fltn,OK2data->flns[0],llFlUrno,clURNO,timeVal);
                     ilRc = getOrPutDBData(clSqlBuf,clData,START|COMMIT);

                     if (ilRc != RC_SUCCESS) 
                     {
                        if (ilRc != RC_NODATA) 
                        {
                            dbg(TRACE,"<HandleCosData> SQL didn't succeed <%s>", clSqlBuf);
                        }
                     }
                     else 
                        dbg(DEBUG,"<HandleCosData> SQL action performed: <%s>", clSqlBuf);
                }
                else if (ilCount == 1) 
                {
                    llLoaUrno = llURNO[0];
                    /*
                    ** we have exactly one container
                    ** perform the update
                    */
                    /*sprintf(clSqlBuf,"UPDATE LOATAB SET DSSO='OKL' WHERE URNO='%ld'", llURNO[0],BRSdata->uldNum);*/
                    sprintf(clSqlBuf,"UPDATE LOATAB SET DSSO = 'OKL', TIME='%14.14s' WHERE URNO='%ld'", timeVal, llURNO[0]);

                    ilRc = getOrPutDBData(clSqlBuf,clData,START|COMMIT);
                    if (ilRc != RC_SUCCESS) 
                    {
                        if (ilRc != RC_NODATA) 
                        {
                            dbg(TRACE,"<HandleCosData> SQL didn't succeed <%s>", clSqlBuf);
                        }
                    }
                    else dbg(DEBUG,"<HandleCosData> SQL action performed: <%s>", clSqlBuf);
                }
                else if (ilIsNotUnique) 
                {
                    /*
                     ** container not unique 
                     ** complain...
                     */
                    dbg(TRACE,"<HandleCosData> Found %d containers with ULD <%12.12s> no update performed", ilCount,OK2data->uldNum);
                }
            }
            else if (ilCount == 0) 
            {
                /*dbg(TRACE,"<HandleCosData> Departure flight not found FLNO=%9.9s FLDA=%8.8s -- no action performed",clFlno,OK2data->flda);*/
            }

            /* Prepare the message into xml format & push it to Ok2 MQ */
            /* Below message according to Sven's request */
            /*
            strcpy(xmlMsg,"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?><OK2L><ULDNumber>");
            strncat(xmlMsg,OK2data->uldNum,12);
            strcat(xmlMsg,"</ULDNumber><Airline>");
            strncat(xmlMsg,OK2data->alc,3);
            strcat(xmlMsg,"</Airline><FlightNo Suffix=\"");
            strncat(xmlMsg,OK2data->flns,1);
            strcat(xmlMsg,"\">");
            strncat(xmlMsg,OK2data->fltn,6);
            strcat(xmlMsg,"</FlightNo><Date>");
            strncat(xmlMsg,OK2data->flda,8);
            strcat(xmlMsg,"</Date><Destination>");
            strncat(xmlMsg,OK2data->des3,3);
            strcat(xmlMsg,"</Destination></OK2L>");
            */

            /*sprintf(xmlMsg,"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?><OK2L><ULDNUMBER>%s</ULDNUMBER><UAFT>%ld</UAFT></OK2L>",tempUld,llFlUrno); */

            /* BLE: Check whether need to forward */
            if( igOKLForward )
                ilOKLTimeDiff = TimeDiff( pclTIFD );
            if( igOKLForward && (ilOKLTimeDiff > igOKLTimeDiff) )
            {
                /* Below message according to Philip's request */
                sprintf(xmlMsg,"<OK2L><ULDNUMBER>%s</ULDNUMBER><UAFT>%ld</UAFT></OK2L>",tempUld,llFlUrno); 
    
                dbg(TRACE,"<HandleCosData> Put Message on COSOKL queue, XML message: %s",xmlMsg);
    
                /* Keep this message on COSOK2 process queue id. */
                ilRc = SendCedaEvent(igSendOK2Modid, 0, " ", " ", " ", " ","COS", " ", " "," ", xmlMsg, "", 4, 0) ;
            }
        
            /* Acknowledge the message in MQRTAB with status as 'OK' */
            slSqlFunc = START;
            slCursor = 0;
            sprintf(clSqlBuf,"UPDATE MQRTAB SET STAT = 'OK' WHERE URNO = %d",llMqrUrno);
            ilRc = sql_if(slSqlFunc,&slCursor,clSqlBuf,clDataArea);
            if (ilRc != DB_SUCCESS)
            {
                get_ora_err(ilRc,pclErr);
                dbg(TRACE,"<HandleCosData> Error reading MQRTAB: ilRc <%d>\nORA-ERR <%s>\nSqlBuf <%s>",ilRc,pclErr,clSqlBuf);
                sleep(120);
                close_my_cursor(&slCursor);
                commit_work();
                slCursor = 0;
                Terminate(1);
                ilRc = RC_FAIL;
            }
            close_my_cursor(&slCursor);
            commit_work();
            slCursor = 0;
        }

        dbg(TRACE,"%05d: ========== End HandleCosData ==========",__LINE__);
        return;
    }

    /* Its a BRS message .. process the message, check the info if all available then insert into DB.
    */

    BRSdata = (BRSInputType *)clMessage;
    for (i=0;i<MAXCOUNT;i++) llURNO[i]=0;
    /*
    ** first fetch airline,flight-number+suffix and date
    ** to retrieve the flight to work on -- has to be unique!
    */

    /*
    ** PRF 7137 remove first 2 leading 0 from number part as requested
    */
    strncpy(clAlc,BRSdata->alc,3); /* ALC first */
    clAlc[3] = '\0';
    strncpy(clFltn,BRSdata->fltn,6); /* FLNO */
    clFltn[6] = '\0';
    strncpy(clFlns,BRSdata->flns,1); /* Suffix */
    clFlns[1] = '\0';
    strncpy(clFlda,BRSdata->flda,8); /* Flight Day (local) */
    clFlda[8] = '\0';

    /*createFkey(clAlc,clFltn,clFlns,"%","D",&clFkey[0]); */
    createFkey(clAlc,clFltn,clFlns,clFlda,"D",&clFkey[0]); /* Got changed .. Thanooj */
        
    sprintf(clSqlBuf,"SELECT URNO FROM AFTTAB WHERE FLDA = '%s' AND FKEY LIKE '%s'", clFlda, clFkey );
    dbg(DEBUG,"<HandleCosData> Searching flight: <%s>",clSqlBuf);

    slSqlFunc = START;
    slCursor = 0;
    while (((ilRc = sql_if(slSqlFunc,&slCursor,clSqlBuf,clURNO)) == DB_SUCCESS) && ilCount < MAXCOUNT)
    {
        if (ilRc == DB_SUCCESS)
        {
            if (ilFirstMatch == FALSE) 
            {
                ilFirstMatch = TRUE;
                llURNO[ilCount] = atol(clURNO);
                ilCount++;
            }
            else 
            {
                llURNO[ilCount] = atol(clURNO);
                ilCount++;
                ilIsNotUnique = TRUE;
            }
        }
        else 
        {
            get_ora_err(ilRc,pclErr);
            dbg(TRACE,"<HandleCosData> Error reading MQRTAB:16861686 ilRc <%d>\nORA-ERR <%s>\nSqlBuf <%s>",ilRc,pclErr,clSqlBuf);
            sleep(120);
            close_my_cursor(&slCursor);
            commit_work();
            slCursor = 0;
            Terminate(1);
            ilRc = RC_FAIL;
        }
        slSqlFunc = NEXT;
    }
    
    /* Close the cursor, commit work & make cursor points to zero */
    close_my_cursor(&slCursor);
    commit_work();
    slCursor = 0;

    /* More number of flights found. Error. */
    if (ilIsNotUnique)
    {
        dbg(TRACE,"<HandleCosData> Fatal error found %d flights -> selection not unique <%s>", ilCount,clSqlBuf);
    }

    /*
    ** now fill in the LOATAB fields of the associated flight
    */
    if (ilIsNotUnique == FALSE && ilCount != 0) 
    {
        /*
        ** Check if the requested container already existing ?
        */
        ilCount = 0;
        sprintf(clSqlBuf,"SELECT URNO FROM LOATAB WHERE FLNU='%ld' AND ULDT='%12.12s' AND DSSN='BRS'",
                                                                           llURNO[0],BRSdata->uldNum);
        slSqlFunc = START;
        slCursor = 0;
        llFlUrno = llURNO[0];

        while (((ilRc = sql_if(slSqlFunc,&slCursor,clSqlBuf,clURNO)) == DB_SUCCESS) && ilCount < MAXCOUNT)
        {
            if (ilRc == DB_SUCCESS)
            {
                if (ilFirstMatch == FALSE) 
                {
                    ilFirstMatch = TRUE;
                    llURNO[ilCount] = atol(clURNO);
                    ilCount++;
                }
                else 
                {
                    llURNO[ilCount] = atol(clURNO);
                    ilCount++;
                    ilIsNotUnique = TRUE;
                }
            }
            else 
            {
                get_ora_err(ilRc,pclErr);
                dbg(TRACE,"<HandleCosData> Error reading MQRTAB:16861686 ilRc <%d>\nORA-ERR <%s>\nSqlBuf <%s>",
                                                                                          ilRc,pclErr,clSqlBuf);
                sleep(120);
                close_my_cursor(&slCursor);
                commit_work();
                slCursor = 0;
                Terminate(1);
                ilRc = RC_FAIL;
            }
            slSqlFunc = NEXT;
        }

        close_my_cursor(&slCursor);
        commit_work();
        slCursor = 0;

        if (ilCount == 0) 
        {
            if( !strncasecmp( BRSdata->status, "CNX", 3 ) )
            {
                dbg( TRACE, "<HandleCosData> CNX status. BRS throw" );
                return;
            }

            /*
            ** This container doesn't exist 
            ** insert it now
            */
            getFreeUrno(clURNO);
            sprintf(clSqlBuf,
                        "INSERT INTO LOATAB (ULDT,APC3,SSTP,ADDI,RMRK,FLNO,FLNU,URNO,DSSN,TYPE,TIME) VALUES "
                        "('%12.12s','%3.3s','%10.10s','%2.2s%3.3s%5.5s%3.3s','%15.15s','%3.3s%5.5s%c','%ld','%s','BRS','ULD','%14.14s')",
                        BRSdata->uldNum,BRSdata->des3,BRSdata->uldCont,BRSdata->uldSource,BRSdata->numOfPieces,
                        BRSdata->uldVolAvail,BRSdata->status,BRSdata->remarks,
                        BRSdata->alc,BRSdata->fltn,BRSdata->flns[0],llFlUrno,clURNO,timeVal);
            ilRc = getOrPutDBData(clSqlBuf,clData,START|COMMIT);

            if (ilRc != RC_SUCCESS) 
            {
                if (ilRc != RC_NODATA) 
                {
                    dbg(TRACE,"<HandleCosData> SQL didn't succeed <%s>", clSqlBuf);
                }
            }
            else 
            {
                dbg(DEBUG,"<HandleCosData> SQL action performed: <%s>", clSqlBuf);

                /* BLE */
                BRSSending( TYPE_INSERT, atol(clURNO), llFlUrno, *BRSdata );
            }
        }
        else if (ilCount == 1) 
        {
            llLoaUrno = llURNO[0];
                
            /*
             ** we have exactly one container
             ** perform the update
             */
            /*
                sprintf(clSqlBuf,"UPDATE LOATAB SET APC3='%3.3s',SSTP='%10.10s',ADDI='%2.2s%3.3s%5.5s%3.3s',RMRK='%15.15s',TIME='%14.14s' WHERE URNO='%ld'",
                                        BRSdata->des3,BRSdata->uldCont,BRSdata->uldSource,BRSdata->numOfPieces,
                                        BRSdata->uldVolAvail,BRSdata->status,BRSdata->remarks,timeVal,llURNO[0],BRSdata->uldNum);
            */

            /* BLE: check CNX status */
            if( !strncasecmp( BRSdata->status, "CNX", 3 ) )
            {
                sprintf(clSqlBuf,"DELETE FROM LOATAB WHERE URNO='%ld'", llURNO[0]);
                ilRc = getOrPutDBData(clSqlBuf,clData,START|COMMIT);
                if (ilRc != RC_SUCCESS) 
                {
                    if (ilRc != RC_NODATA) 
                    {
                        dbg(TRACE,"<HandleCosData> SQL didn't succeed <%s>", clSqlBuf);
                    }
                }
                else 
                {
                    dbg(DEBUG,"<HandleCosData> SQL action performed: <%s>", clSqlBuf);
    
                    /* BLE */
                    BRSSending( TYPE_DELETE, llURNO[0], llFlUrno, *BRSdata );
                }
            }
            else
            {
                sprintf(clSqlBuf,"UPDATE LOATAB SET APC3='%3.3s',SSTP='%10.10s',"
                                 "ADDI='%2.2s%3.3s%5.5s%3.3s',RMRK='%15.15s',"
                                 "TIME='%14.14s' WHERE URNO='%ld'", BRSdata->des3,BRSdata->uldCont,
                                 BRSdata->uldSource,BRSdata->numOfPieces,BRSdata->uldVolAvail,
                                 BRSdata->status,BRSdata->remarks,timeVal,llURNO[0]);

                ilRc = getOrPutDBData(clSqlBuf,clData,START|COMMIT);
                if (ilRc != RC_SUCCESS) 
                {
                    if (ilRc != RC_NODATA) 
                    {
                        dbg(TRACE,"<HandleCosData> SQL didn't succeed <%s>", clSqlBuf);
                    }
                }
                else 
                {
                    dbg(DEBUG,"<HandleCosData> SQL action performed: <%s>", clSqlBuf);

                    /* BLE */
                    BRSSending( TYPE_UPDATE, llURNO[0], llFlUrno, *BRSdata );
                }
            }
        }
        else if (ilIsNotUnique) 
        {
            /*
            ** container not unique 
            ** complain...
            */
            dbg(TRACE,"<HandleCosData> Found %d containers with ULD <%12.12s> no update performed",
                                                                           ilCount,BRSdata->uldNum);
        }
    }
    else if (ilCount == 0) 
    {
        /*
        ** flight not found
        ** complain...
        */
        /*
        dbg(TRACE,"<HandleCosData> Departure flight not found FLNO=%9.9s FLDA=%8.8s -- no action performed",
                                                                                       clFlno,BRSdata->flda);
        */
    }
                
    /* 
    ** acknowledge the Message in MQRTAB
    */
    slSqlFunc = START;
    slCursor = 0;
    sprintf(clSqlBuf,"UPDATE MQRTAB SET STAT = 'OK' WHERE URNO = %d",llMqrUrno);
    ilRc = sql_if(slSqlFunc,&slCursor,clSqlBuf,clMessage);
    if (ilRc != DB_SUCCESS)
    {
        get_ora_err(ilRc,pclErr);
        dbg(TRACE,"<HandleCosData> Error reading MQRTAB: ilRc <%d>\nORA-ERR <%s>\nSqlBuf <%s>",ilRc,pclErr,clSqlBuf);
        sleep(120);
        close_my_cursor(&slCursor);
        commit_work();
        slCursor = 0;
        Terminate(1);
        ilRc = RC_FAIL;
    }
    close_my_cursor(&slCursor);
    commit_work();
    slCursor = 0;
    dbg(TRACE,"%05d: ========== End HandleCosData ==========",__LINE__);

    return ilRc;
}

static int UtcToLocal(char *pcpTime)
{
    int ilRc = RC_SUCCESS;
    char *pclTdi;
    char clLocal[32];

    if(strlen(pcpTime) > 0)
    {
    strcpy(clLocal,pcpTime);
    pclTdi = (strcmp(pcpTime,cgTich) < 0) ? cgTdi1 : cgTdi2;
    AddSecondsToCEDATime(pcpTime,atol(pclTdi),1);
    }
    return ilRc;
}


static void TrimRight(char *s)
{
    int i = 0;    /* search for last non-space character */
    for (i = strlen(s) - 1; i >= 0 && isspace(s[i]); i--); /* trim off right spaces */
    s[++i] = '\0';
}

struct VialType {
    char display[1];
    char apc3[3];
    char apc4[4];
    char stoa[14];
    char etoa[14];
    char land[14];
    char onbl[14];
    char stod[14];
    char etod[14];
    char ofbl[14];
    char airb[14];
  };

typedef struct VialType *VialTypePtr;

static void getVialItems(int ipVian,char* pcpVial,char* pcpRoute)
{
  int ilCount;
  VialTypePtr clVialRecord;

  ilCount = sizeof (struct VialType);
  clVialRecord = (VialTypePtr) pcpVial;
  for (ilCount=0;ilCount<ipVian;ilCount++) {
    if (ilCount == 0) strncpy(pcpRoute,clVialRecord->apc3,3);
    else strncat(pcpRoute,clVialRecord->apc3,3);
    strcat(pcpRoute," ");
    clVialRecord = (VialTypePtr)(pcpVial + (ilCount+1)*sizeof (struct VialType));
  }
}

/******************************************************************************/
/* getFreeUrno                                                                */
/*                                                                            */
/* Maintains a pool of URNOS. If no more URNOS are available, the pool is     */
/* refilled with a set of URNO_POOL_SIZE.                                     */
/*                                                                            */
/* Input:                                                                     */
/*       Nothing                                                              */
/*                                                                            */
/* Return:                                                                    */
/*       cpUrno -- the buffer pointed to is filled with a new URNO            */
/*       RC_SUCCESS/RC_FAIL                                                   */
/*                                                                            */
/******************************************************************************/
static int getFreeUrno(char *cpUrno)
{
  static uint ilNumUrnos = 0;
  static uint ilFirstUrno = 0;
  static uint ilNextUrno = 0;
  char clNewUrno[URNO_SIZE];
  int ilRc;

  if (ilNumUrnos == 0) {
    ilRc = GetNextValues (clNewUrno, URNO_POOL_SIZE);
    if (ilRc == RC_SUCCESS) {
      ilFirstUrno = atoi(clNewUrno);
      ilNextUrno = ilFirstUrno;
      ilNumUrnos = 100;
    }
    else return RC_FAIL;
  }
  sprintf(cpUrno, "%d", ilNextUrno);
  ilNextUrno++;
  ilNumUrnos--;
  dbg(DEBUG,"<getFreeUrno> URNO First:<%d> Next:<%d> Num:<%d>", 
      ilFirstUrno, ilNextUrno, ilNumUrnos);
  return RC_SUCCESS;
}
/******************************************************************************/
/* createFkey                                                                 */
/*                                                                            */
/* Input:                                                                     */
/*       clAlc   -- Airline Code                                              */
/*       clFlno  -- Flight Number (6 digits with leading zeros)               */
/*       clFlns  -- Flight Suffix (optional)                                  */
/*       clFlda  -- Flight day (UTC)                                          */
/*       clAdid  -- Routing                                                   */
/*                                                                            */
/* Output:                                                                    */
/*       clFkey  -- generated FKEY                                            */
/*                                                                            */
/* Returns: RC_SUCCESS/RC_FAIL                                                */
/*                                                                            */
/******************************************************************************/
static int createFkey(char *pclAlc,char *pclFlno,
              char *pclFlns,char *pclFlda,
              char *pclAdid,char *pclFkey)
{
  char *FlnoPtr;
  char clFlns[8];
  char clSqlBuf[1024];
  char clData[64];
  int  lenALC3=0;
  int  ilRc;

  FlnoPtr = &pclFlno[1];
  if (!strcmp(pclFlns," ")) {
    strcpy(clFlns,"#");
  }
  else {
    strcpy(clFlns,pclFlns);
  }
  /*
  ** validate ALC from database
  */
  TrimRight(pclAlc);
  lenALC3 = strlen(pclAlc);
  if (lenALC3 == 2) {
    sprintf (clSqlBuf, "SELECT DISTINCT ALC3 FROM ALTTAB WHERE ALC2='%s'",pclAlc);
    ilRc = getOrPutDBData(clSqlBuf,clData,START);
    if (ilRc != RC_SUCCESS) {
      if (ilRc != RC_NODATA) {
    dbg(TRACE,"<createFkey> SQL didn't succeed <%s>", clSqlBuf);
      }
    }
    else {
      dbg(DEBUG,"<createFkey> SQL action performed: <%s>", clSqlBuf);  
      strcpy(pclAlc,clData);
    }
  }

  /* amended by BLE: 30-May-08. FLDA is in LOC time, wherelse FKEY is in UTC time */
  /* ignore flight date matching in FKEY */

  /* FFFFFAAA#YYYYDDMMR */
  /*sprintf(pclFkey,"%s%3.3s%s%s%s",
          FlnoPtr,pclAlc,clFlns,pclFlda,pclAdid);*/
  sprintf(pclFkey,"%s%3.3s%s%%%s",
          FlnoPtr,pclAlc,clFlns,pclAdid);

}

void BRSSending( int ipSendType, long lpUrno, long lpFlnu, BRSInputType rpBRS )
{
    int ilRc;
    char pclXmlStr[1024];
    char pclSqlBuf[1024];
    char pclTmpStr[512];
    char pclAlc2[8];
    char *pclFunc = "BRSSending";

    dbg( TRACE, "<%s> ipSendType <%d> lpUrno <%d> lpFlnu <%d>", pclFunc, ipSendType, lpUrno, lpFlnu );

    if( ipSendType == TYPE_DELETE )
    {
        sprintf( pclXmlStr, "<BRS>"
                            "<ACTION>DELETE</ACTION>"
                            "<URNO>%d</URNO>"
                            "</BRS>", lpUrno );
    }
    else
    {
        strncpy( pclTmpStr, rpBRS.uldNum, 12 );
        pclTmpStr[12] = '\0';
        TrimRight(pclTmpStr);
        if( strlen(pclTmpStr) <= 0 )
        {
            dbg( TRACE, "<%s> Empty ULD number. Throw", pclFunc );
            return;
        }
        sprintf( pclSqlBuf, "SELECT TRIM(ALC2) FROM AFTTAB WHERE URNO = %d", lpFlnu );
        dbg( TRACE, "<%s> SelAFT <%s>", pclFunc, pclSqlBuf );
        ilRc = getOrPutDBData(pclSqlBuf,pclAlc2,START);
        if( ilRc != DB_SUCCESS )
            strcpy( pclAlc2, "" );
        dbg( TRACE, "<%s> SelAFT ilRc <%d>", pclFunc, ilRc );

        sprintf( pclXmlStr,
                    "<BRSS>\n"
                    "<CPMDET>\n"
                    "<FLNU>%d</FLNU>\n"
                    "<ULDN>%12.12s</ULDN>\n" 
                    "<DSSN>BRS</DSSN>\n"
                    "<APC3>%3.3s</APC3>\n"
                    "<CONT>%10.10s</CONT>\n"
                    "<POSF></POSF>\n"
                    "<VALU>%5.5s</VALU>\n"
                    "<NOP>%3.3s</NOP>\n"
                    "<URNO>%d</URNO>\n"
                    "<ALC2>%2.2s</ALC2>\n"
                    "</CPMDET>\n"
                    "</BRSS>\n", lpFlnu, rpBRS.uldNum, rpBRS.des3, rpBRS.uldCont, 
                                 rpBRS.uldVolAvail, rpBRS.numOfPieces, 
                                 lpUrno, pclAlc2 );
    }

    ilRc = SendCedaEvent(igSendCPMModid,0, " ", " ", " ", " ","ITK", " ", " "," ", pclXmlStr, "", 4, 0) ;
    dbg (TRACE, "<%s> ilRc for SendCedaEvent = %d", pclFunc, ilRc );
    dbg (TRACE, "<%s> XML (%s)", pclFunc, pclXmlStr );
    return;
}

int TimeDiff ( char *pclTimeStr )
{
    struct tm rlTm;
    time_t tlNow, tlTimeComp;
    int ilMinDiff;
    char pclTmpStr[128];
    char *pclFunc = "TimeDiff";

    if( strlen( pclTimeStr ) != 14 )
        return -1;

    tlTimeComp = tlNow = time(0);
    gmtime_r( &tlTimeComp, &rlTm );

    sprintf( pclTmpStr, "%4.4s", pclTimeStr );     rlTm.tm_year = atoi(pclTmpStr) - 1900; 
    sprintf( pclTmpStr, "%2.2s", &pclTimeStr[4] ); rlTm.tm_mon  = atoi(pclTmpStr) - 1;
    sprintf( pclTmpStr, "%2.2s", &pclTimeStr[6] ); rlTm.tm_mday = atoi(pclTmpStr);
    sprintf( pclTmpStr, "%2.2s", &pclTimeStr[8] ); rlTm.tm_hour = atoi(pclTmpStr);
    sprintf( pclTmpStr, "%2.2s", &pclTimeStr[10] );rlTm.tm_min  = atoi(pclTmpStr);
    sprintf( pclTmpStr, "%2.2s", &pclTimeStr[12] );rlTm.tm_sec  = atoi(pclTmpStr);

    dbg( DEBUG, "<%s> TimeCompare <%4.4d-%2.2d-%2.2d %2.2d:%2.2d:%2.2d>", pclFunc, rlTm.tm_year+1900, 
                        rlTm.tm_mon+1, rlTm.tm_mday, rlTm.tm_hour, rlTm.tm_min, rlTm.tm_sec );

    tlTimeComp = mktime( (struct tm *)&rlTm );
    memset( &rlTm, 0, sizeof(struct tm) );
    gmtime_r( &tlNow, &rlTm );
    tlNow = mktime( (struct tm *)&rlTm );

    ilMinDiff = ( tlTimeComp - tlNow ) / SECONDS_PER_MINUTE;
    dbg( TRACE, "<%s> Diff Minutes TIFD<%d> - Now<%d> = <%d> min", pclFunc, tlTimeComp, tlNow, ilMinDiff );
    return ilMinDiff;
}

/** BLE 16-OCT-2008 **/
static int HandleDelayCode(char *pcpFields,char *pcpData, char *pcpCmd)
{
    int ilRc;
    int ilTotalCnt = 0;
    int ilCnt = 0;
    int ili;
    int isDelete;
    short slSqlCursor;
    short slSqlFunc;
    char pclUaft[20];
    char pclSqlBuf[1024];
    char pclSqlData[5120];
    char pclData[5120];
    char pclTIME_recv[16];
    char pclTIME_db[16];
    char pclTIME_field[8];
    char pclPackedAft[512];
    char pclFinalStr[5120];
    char pclTmpStr[512];
    char pclLINO[12];
    char pclTEXT[4004];
    char *pclToken;
    char *pclFunc = "HandleDelayCode";


    dbg(TRACE,"<%s> ************* START ***********", pclFunc);

    strcpy( pclData, pcpData );
    isDelete = FALSE;
    if( !strncmp( pcpCmd, "DRT", 3 ) )
    {
        isDelete = TRUE;
        pclToken = (char *)strstr( pclData, "\n" );
        if( pclToken != NULL )
        {
            *pclToken = '\0';
            pclToken++;
            strcpy( pclData, pclToken );
        }
    }
    pclToken = getItem(pcpFields,pclData,"FLNU");
    if( pclToken == NULL ) 
    {
        dbg(TRACE,"<%s> Fatal no URNO received, giving up", pclFunc);
        return RC_FAIL;
    }
    strcpy( pclUaft, pclToken );
    dbg( TRACE, "<%s> Flight URNO <%s>", pclFunc, pclUaft );

    /* To prevent sending too many sets of Delay Code */
    if( isDelete == FALSE )
    {
        strcpy( pclTIME_field, "LSTU" );
        if( !strncmp( pcpCmd, "IRT", 3 ) )
            strcpy( pclTIME_field, "CDAT" );
       
        ili = get_item_no(pcpFields,pclTIME_field,5);
        if( ili >= 0 )
            get_real_item(pclTIME_recv, pcpData, ili+1);
        if( strlen(pclTIME_recv) > 0 )
        {
        sprintf( pclSqlBuf, "SELECT MAX(%s) FROM DLYTAB WHERE FLNU = %s", pclTIME_field, pclUaft );
        dbg( TRACE, "<%s> Sel%s SqlBuf <%s>", pclFunc, pclTIME_field, pclSqlBuf );

        slSqlCursor = 0;
            ilRc = sql_if(START,&slSqlCursor,pclSqlBuf,pclTIME_db);
            close_my_cursor(&slSqlCursor);
            if( ilRc == DB_SUCCESS )
            {
                if( strncmp( pclTIME_recv, pclTIME_db, 14 ) )
                {
                    dbg( TRACE, "<%s> Not the last insert/update. Ignore.", pclFunc );
                    return RC_SUCCESS;
                }
            }
        }
    }

    ilRc = HandleAftData(TRUE, "UFR","URNO",pclUaft, pclPackedAft);
    if( ilRc != RC_SUCCESS )
        return ilRc;
    dbg( DEBUG, "<%s> Packed Aft Data <%s>", pclFunc, pclPackedAft );

    /* Preparing delay code information */
    sprintf( pclSqlBuf, "SELECT COUNT(*) FROM DLYTAB WHERE FLNU = %s", pclUaft );
    dbg( TRACE, "<%s> CntDLY SqlBuf <%s>", pclFunc, pclSqlBuf );
    ilRc = getOrPutDBData(pclSqlBuf, pclSqlData, START);
    if( ilRc != DB_SUCCESS )
        return ilRc;

    ilTotalCnt = atoi(pclSqlData);
    dbg(TRACE,"<%s> #DelayCode = %d", pclFunc, ilTotalCnt);

    if( ilTotalCnt <= 0 && !strcmp(pcpCmd, "DRT") )
    {
        sprintf( pclFinalStr, "%s%3.3d%3.3d", pclPackedAft,0,0 ); 
        dbg( TRACE, "<%s> Blanco Final Str <%-.300s>", pclFunc, pclFinalStr );
        ilRc = SendCedaEvent(8242, 0, " ", " ", " ", " ","COS", " ", " ",
                             " ", pclFinalStr, "", 2, 0) ;
        dbg( TRACE, "<%s> ilRc = %d", pclFunc, ilRc );
        return RC_SUCCESS;
    }
    ilCnt = 0;
    sprintf( pclSqlBuf, "SELECT TRIM(LINO), TRIM(TEXT) FROM DLYTAB WHERE FLNU = %s ORDER BY LINO", pclUaft );
    dbg( TRACE, "<%s> SelDLY SqlBuf <%s>", pclFunc, pclSqlBuf );

    slSqlCursor = 0;
    slSqlFunc = START;
    while( (ilRc = sql_if(slSqlFunc,&slSqlCursor,pclSqlBuf,pclSqlData)) == DB_SUCCESS )
    {
        ilCnt++;
        slSqlFunc = NEXT;
        if( ilCnt > ilTotalCnt )
            break;

        get_fld(pclSqlData,FIELD_1,STR,8,pclLINO);
        get_fld(pclSqlData,FIELD_2,STR,4000,pclTEXT);

        dbg( TRACE, "<%s> LINO <%s> TEXT <%-.40s>", pclFunc, pclLINO, pclTEXT );

        sprintf( pclFinalStr, "%s%3.3d%3.3d%3.3s%3.3s%s", pclPackedAft, ilTotalCnt, 
                                             ilCnt, &pclLINO[1], &pclLINO[5], pclTEXT ); 
        dbg( TRACE, "<%s> Final Str <%-.300s>", pclFunc, pclFinalStr );
        ili = 0;
        while( ili < 5 && igSendDLYModid[ili] > 0 )
            ilRc = SendCedaEvent(igSendDLYModid[ili++], 0, " ", " ", " ", " ","COS", " ", " ",
                                 " ", pclFinalStr, "", 2, 0) ;
        dbg( TRACE, "<%s> ilRc = %d", pclFunc, ilRc );
    }
    close_my_cursor(&slSqlCursor);

    dbg(TRACE,"<%s> ************* END ***********", pclFunc);
}
