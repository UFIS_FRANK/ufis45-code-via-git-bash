#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Sin/SIN_Server/Base/Server/Interface/iflhdl.c 1.4 2008/08/26 18:00:05SGT ble Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :  Mei, Lee Bee Suan                                        */
/* Date           :  21-Nov-2007                                              */
/* Description    :                                                           */
/*                                                                            */
/* Update history :                                                           */
/* 23-JUN-2008 BLE: Exclude FTYP configurable for Bulk/Online Flight data     */
/*                : When flight is split/join, RKEY is the only field changed */
/* 24-JUN-2008 BLE: Check for FTYP when DFR is received                       */
/* 01-JUL-2008 BLE: Merge code on ignoring Opsspm error                       */
/* 26-AUG-2008 BLE: Filled up TWE for BCHDL statistics to tabulate its usage  */
/* 26-AUG-2008 BLE: Vendor is pouring equip upd, block by chk only VALU change*/
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */

static char sccs_version[] ="@(#) UFIS44 (c) ABB AAT/I skeleton.c 44.1.0 / 11.12.2002 HEB";

/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */

#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "fditools.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "rmschkrul.h"
#include "syslib.h"
#include "itrek_fn.inc"
#include "db_if.h"
#include <time.h>
#include <cedatime.h>

#ifdef _HPUX_SOURCE
    extern long timezone;
    extern char *tzname[2];
    extern int _daylight;
    extern long _timezone;
    extern char *_tzname[2];
#endif

#ifndef BOOL
#define BOOL int
#endif

#define DBQRLEN         4000
#define NUM_TIME_FIELDS    8
#define MAX_FLIGHT_FIELDS 26
#define START_DATA_SIZE 6000
#define ONE_VIA_SIZE     120
#define FLIGHT  0
#define STAFF   1
#define EQUIP   2
#define TYPE_BULK    1
#define TYPE_INSERT  2
#define TYPE_UPDATE  3
#define TYPE_DELETE  4
#define TYPE_STAFF    0x01
#define TYPE_EQUIP    0x02
#define TYPE_BOTH     0x03
#define NOT_TURNAROUND 'N' 
#define IS_TURNAROUND  'Y' 

#define ONLINE_DELETE   "OD"
#define CHECK_DELETE    "CD"
#define SYNC_DELETE     "SD"
#define ONLINE_UPDATE   "OU"
#define CHECK_UPDATE    "CU"
#define SYNC_UPDATE     "SU"
#define ONLINE_INSERT   "OI"
#define CHECK_INSERT    "CI"
#define SYNC_INSERT     "SI"

#define ONE_FOUR    4
#define TWO_FOUR    8
#define THREE_FOUR  12
#define FOUR_FOUR   16
#define FIVE_FOUR   20
#define FIVE_FOUR   20
#define COTYLEN     ONE_FOUR
#define DATELEN     FIVE_FOUR
#define URNOLEN     THREE_FOUR
#define VIALLEN     1028  /* Since there are only 8 Vias */
#define NAMELEN     44
#define VALULEN     44

typedef struct
{
    char URNO[URNOLEN];
    char FLNO[THREE_FOUR];
    char ALC2[COTYLEN];
    char ALC3[COTYLEN];
    char FLTN[TWO_FOUR]; 
    char FLNS[ONE_FOUR]; 
    char STOA[DATELEN];
    char STOD[DATELEN];
    char ADID[ONE_FOUR];
    char RKEY[URNOLEN];
    char REGN[FOUR_FOUR];
    char CSGN[THREE_FOUR];
    char ETAI[DATELEN];
    char ETDI[DATELEN];
    char DES3[COTYLEN];
    char ORG3[COTYLEN];
    char FTYP[ONE_FOUR];
    char TTYP[TWO_FOUR];
    char ACT3[COTYLEN];
    char ACT5[TWO_FOUR];
    char LAND[DATELEN];
    char ONBL[DATELEN];
    char AIRB[DATELEN];
    char OFBL[DATELEN];
    char VIAL[VIALLEN];
    char PSTA[TWO_FOUR];
    char PSTD[TWO_FOUR];
    char TIFA[DATELEN];
    char TIFD[DATELEN];
} AFTREC;

typedef struct
{
    char URNO[URNOLEN];
    char WGPC[TWO_FOUR];
    char FCCO[THREE_FOUR];
    char USTF[URNOLEN];
    char UAFT[URNOLEN];
    char UEQU[URNOLEN];
    char STAT[ONE_FOUR];
    char UDSR[URNOLEN];
    char UTPL[URNOLEN];
    char DETY[ONE_FOUR];
    char RKEY[URNOLEN];
} JOBREC;

typedef struct
{
    char URNO[URNOLEN];
    char PENO[THREE_FOUR];
    char FINM[NAMELEN];
    char LANM[NAMELEN];
    char SHNM[NAMELEN];
} STFREC;

typedef struct
{
    char URNO[URNOLEN];
    char GCDE[TWO_FOUR];
    char ENAM[NAMELEN];
} EQUREC;

typedef struct
{
    char UEQA[URNOLEN];
    char UEQU[URNOLEN];
    char GCDE[TWO_FOUR];
    char EQPS[TWO_FOUR];
    char LSTU[DATELEN];
    char VALU[VALULEN];
    char NAME[NAMELEN];
} BAYREC;

/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
int debug_level = DEBUG;
static ITEM  *prgItem      = NULL;             /* The queue item pointer  */
static EVENT *prgEvent     = NULL;             /* The event pointer       */
static int   igItemLen     = 0;                /* length of incoming item */
static EVENT *prgOutEvent  = NULL;
static long lgEvtCnt = 0;

static int bgSyncData;
static int igFlightBulkStartHour;
static int igFlightBulkEndHour;
static int igFlightOnlineStartHour;
static int igFlightOnlineEndHour;
static int igSendPrio;
static int igRecipentID[3];
static int igSqlhdlID;
static int igBchdlID;
static char cgProcessName[20] = "\0";
static char cgConfigFile[512] = "\0";
static char cgHopo[8] = "\0";                 /* default home airport    */
static char cgTabEnd[8] ="\0";                /* default table extension */

char pcgXmlStr[START_DATA_SIZE];
char *pcgXmlHeader = "\n<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
char *pcgFlightDbFields = "TRIM(URNO),TRIM(FLNO),TRIM(ALC2),TRIM(ALC3),TRIM(FLTN),TRIM(FLNS),TRIM(STOA),TRIM(STOD),TRIM(ADID),TRIM(RKEY),TRIM(REGN),TRIM(CSGN),TRIM(ETAI),TRIM(ETDI),TRIM(DES3),TRIM(ORG3),TRIM(FTYP),TRIM(TTYP),TRIM(ACT3),TRIM(ACT5),TRIM(LAND),TRIM(ONBL),TRIM(AIRB),TRIM(OFBL),VIAL,TRIM(PSTA),TRIM(PSTD)";

#define NUM_ATTRIBUTES 4
char pcgEquXmlFields[NUM_ATTRIBUTES][8] = { "NAME","LSTU","PENO","EQPS" };
char pcgEquAttFields[NUM_ATTRIBUTES][NAMELEN];

char pcgApronTemplate[56];
char pcgMQRTable[12];
char pcgTimeStr[DATELEN];
char pcgExcludeFtyp[40];

/******************************************************************************/
/* Function prototypes                                                        */
/******************************************************************************/
static int Init_Process();
static int Init_Config();
static int ReadConfigEntry(char *pcpFname, char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer);
static int ReadConfigLine(char *pcpFname, char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer);
static int Reset(void);                       /* Reset program          */
static void Terminate(int ipSleep);            /* Terminate program      */
static void HandleSignal(int);                 /* Handles signals        */
static void HandleErr(int);                    /* Handles general errors */
static void HandleQueErr(int);                 /* Handles queuing errors */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/
static int HandleData(EVENT *prpEvent);       /* Handles event data     */
static int GetElementValue(char *pclDest, char *pclOrigData, char *pclBegin, char *pclEnd);
static int CheckQueue(int ipModId, ITEM **prpItem);
static int getOrPutDBData(char *pcpSelection, char *pcpData, unsigned int ipMode);
static void HandleFlightOnlineData(char *pcpFields, char *pcpData, char *pcpCmd, char *pcpSelection);
static void HandleJobOnlineData(char *pcpFields, char *pcpData, char *pcpCmd, char *pcpSelection);
static int PackFlightXml( AFTREC rpAftrec, int ipPackType );
static void SendBulkFlight();
int ReadDB( JOBREC *rpJobrec, STFREC *rpStfrec, EQUREC *rpEqurec, char cpJobType );
int InsertDB( JOBREC rpJobrec, STFREC rpStfrec, EQUREC rpEqurec );
int UpdateDB( JOBREC rpJobrec, STFREC rpStfrec, EQUREC rpEqurec );
int DeleteDB( char *pcpURNO, char *pcpUAFT );
int UpdateBay(char *pcpFields, char *pcpData, int ipAction);
int DeleteJob( JOBREC rpJobrec, STFREC rpStfrec, EQUREC rpEqurec, int ipDelType );
int UpdateJob( JOBREC rpJobrec, STFREC rpStfrec, EQUREC rpEqurec, int ipActType, int ipInsType );
int GetWGPC( char *pcpUDSR, char *pcpWGPC );
int GetStaff( STFREC *rpStfrec );
int GetEquip( EQUREC *rpEqurec );
int GetTurnaroundUAFT( char *pcpUAFT, char *pcpTurnArndUAFT, char *pcpRKEY );
int UpdateSyncStat( char *pcpUJOB, char *pcpSTAT );
int SyncData();

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
    int ilRc = RC_SUCCESS;          /* Return code          */
    int ilCnt = 0;
    int ilOldDebugLevel = 0;

    INITIALIZE;         /* General initialization   */

    strcpy(cgProcessName,argv[0]);
    dbg(TRACE,"MAIN: version <%s>",sccs_version);

    /* Attach to the MIKE queues */
    do
    {
        ilRc = init_que();
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        }/* end of if */
    }while((ilCnt < 10) && (ilRc != RC_SUCCESS));

    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
        sleep(60);
        exit(1);
    }else{
        dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
    }/* end of if */

    do
    {
        ilRc = init_db();
        if (ilRc != RC_SUCCESS)
        {
            check_ret(ilRc);
            dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        } /* end of if */
    } while((ilCnt < 10) && (ilRc != RC_SUCCESS));

    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
        sleep(60);
        exit(2);
    }else{
        dbg(TRACE,"MAIN: init_db() OK!");
    } /* end of if */

    /* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */
    sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
    dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
    ilRc = TransferFile(cgConfigFile);
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */

    sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
    dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
    ilRc = TransferFile(cgConfigFile);
    if(ilRc != RC_SUCCESS)
    { 
        dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */

    ilRc = SendRemoteShutdown(mod_id);
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    } /* end of if */

    if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
        dbg(DEBUG,"MAIN: waiting for status switch ...");
        HandleQueues();
    }/* end of if */

    if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
        dbg(TRACE,"MAIN: initializing ...");
            ilRc = Init_Process();
            if(ilRc != RC_SUCCESS)
            {
                dbg(TRACE,"Init_Process: init failed!");
            } /* end of if */

    } else {
        Terminate(30);
    }/* end of if */


    dbg(TRACE,"=====================");
    dbg(TRACE,"MAIN: initializing OK");
    dbg(TRACE,"=====================");
    
    dbg( TRACE, "MAIN: Synchronize FLIGHT/JOB data....." );
    bgSyncData = TRUE;
    /*SyncData();*/
    bgSyncData = FALSE;
    dbg( TRACE, "MAIN: Synchronize Completed!" );

    for(;;)
    {
        ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* Acknowledge the item */
        ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
        if( ilRc != RC_SUCCESS ) 
        {
            /* handle que_ack error */
            HandleQueErr(ilRc);
        } /* fi */

        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */

        prgEvent = (EVENT *) prgItem->text;
                
        if( ilRc == RC_SUCCESS )
        {
            lgEvtCnt++;
            switch( prgEvent->command )
            {
            case    HSB_STANDBY :
                ctrl_sta = prgEvent->command;
                HandleQueues();
                break;  
            case    HSB_COMING_UP   :
                ctrl_sta = prgEvent->command;
                HandleQueues();
                break;  
            case    HSB_ACTIVE  :
                ctrl_sta = prgEvent->command;
                break;  
            case    HSB_ACT_TO_SBY  :
                ctrl_sta = prgEvent->command;
                /* CloseConnection(); */
                HandleQueues();
                break;  
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate(1);
                break;  
            case    HSB_STANDALONE  :
                ctrl_sta = prgEvent->command;
                ResetDBCounter();
                break;  
            case    REMOTE_DB :
                /* ctrl_sta is checked inside */
                HandleRemoteDB(prgEvent);
                break;
            case    SHUTDOWN    :
                /* process shutdown - maybe from uutil */
                Terminate(1);
                break;
                    
            case    RESET       :
                ilRc = Reset();
                break;
                    
            case    EVENT_DATA  :
                if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
                {
                    ilRc = HandleData(prgEvent);
                    if(ilRc != RC_SUCCESS)
                    {
                        HandleErr(ilRc);
                    }/* end of if */
                }else{
                    dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
                    DebugPrintItem(TRACE,prgItem);
                    DebugPrintEvent(TRACE,prgEvent);
                }/* end of if */
                break;
                

            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;
            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;
            default         :
                dbg(TRACE,"MAIN: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */

        } else {
            /* Handle queuing errors */
            HandleQueErr(ilRc);
        } /* end else */
        
    } /* end for */
    
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int ReadConfigEntry(char *pcpFname, char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer)
{
    int ilRc = RC_SUCCESS;

    char clSection[124] = "\0";
    char clKeyword[124] = "\0";

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRc = iGetConfigEntry(pcpFname,clSection,clKeyword, CFG_STRING,pcpCfgBuffer);
    if(ilRc != RC_SUCCESS)
        dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
    else
        dbg(DEBUG,"Config Entry <%s>,<%s>:<%s> found in %s", clSection, clKeyword ,
                                               pcpCfgBuffer, cgConfigFile);
    return ilRc;
}

static int ReadConfigLine(char *pcpFname, char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer)
{
    FILE *rlFptr;
    BOOL ilFound = FALSE;
    BOOL ilStartFound = FALSE;
    char pclLine[2048];
    char pclStartSec[32];
    char pclEndSec[32];
    char *pclToken;

    rlFptr = fopen( pcpFname, "r" );
    if( rlFptr == NULL )
    {
        dbg(TRACE,"Error in reading Config Entry errno = %d<%s>", errno, strerror(errno) );
        return RC_FAIL;
    }
    sprintf( pclStartSec, "[%s]", pcpSection );
    sprintf( pclEndSec, "[%s_END]", pcpSection );

    while( !feof(rlFptr) )
    {
        if( fgets( pclLine, sizeof(pclLine), rlFptr ) == NULL )
            break;
        if( !strncmp(pclLine, pclEndSec, strlen(pclEndSec)) )
           break;
        if( !strncmp(pclLine, pclStartSec, strlen(pclStartSec)) )
        {
           ilStartFound = TRUE;
           continue;
        }
        if( ilStartFound == FALSE )
           continue;;
        pclToken = (char *)strtok( pclLine, " " );
        if( pclToken == NULL )
            continue;
        if( strncmp(pclToken, pcpKeyword, strlen(pcpKeyword)) )
            continue;

        pclToken = (char *)strtok( NULL, "'" );
        pclToken = (char *)strtok( NULL, "'" );
        memcpy( pcpCfgBuffer, pclToken, strlen(pclToken) );
        pcpCfgBuffer[strlen(pclToken)] = '\0';
        ilFound = TRUE;
        break;
    }
    fclose(rlFptr);
    if( ilFound == FALSE )
        return RC_FAIL;
    return RC_SUCCESS;
}

static int Init_Process()
{
    int  ilRc = RC_SUCCESS;         /* Return code */
    char *pclFunc = "Init_Process";


    /* read HomeAirPort from SGS.TAB */
    ilRc = tool_search_exco_data("SYS", "HOMEAP", cgHopo);
    if (ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"<%s> EXTAB,SYS,HOMEAP not found in SGS.TAB", pclFunc);
        Terminate(30);
    } 
    dbg(TRACE,"<%s> home airport <%s>", pclFunc,cgHopo);

    
    ilRc = tool_search_exco_data("ALL","TABEND", cgTabEnd);
    if (ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"<%s> EXTAB,ALL,TABEND not found in SGS.TAB", pclFunc);
        Terminate(30);
    } 
    dbg(TRACE,"<%s> table extension <%s>", pclFunc,cgTabEnd);

    Init_Config();
}


static int Init_Config()
{
    int ilRc = RC_SUCCESS;         /* Return code */
    int ilNum, ili;
    char pclTmpStr[100] = "\0";
    char pclSession[20] = "\0";
    char *pclFunc = "Init_Config";

    ilRc = ReadConfigEntry( cgConfigFile, "MAIN","DEBUG_LEVEL",pclTmpStr);
    debug_level = TRACE;
    if( !strncmp( pclTmpStr, "DEBUG", 5 ) )
        debug_level = DEBUG;
    dbg(TRACE,"<%s> debug_level <%s> -> <%d>",pclFunc, pclTmpStr, debug_level);

    ilRc = ReadConfigEntry( cgConfigFile, "MAIN","SEND_PRIO", pclTmpStr);
    if (ilRc == RC_SUCCESS)
    {
        igSendPrio = atoi(pclTmpStr);
        if( igSendPrio <= 0 )
            igSendPrio = 3;
        dbg(TRACE,"<%s> igSendPrio <%s> -> <%d>", pclFunc, pclTmpStr, igSendPrio );
    }

    strcpy( pcgApronTemplate, "'113682507','118289734'" );
    ilRc = ReadConfigEntry( cgConfigFile, "MAIN","APRON_TEMPLATE", pclTmpStr);
    if (ilRc == RC_SUCCESS)
        strcpy( pcgApronTemplate, pclTmpStr );
    dbg(TRACE,"<%s> ApronTemplate <%s>", pclFunc, pcgApronTemplate );

    strcpy( pcgMQRTable, "MR2TAB" );
    ilRc = ReadConfigEntry( cgConfigFile, "MAIN","MQR_TABLE", pclTmpStr);
    if (ilRc == RC_SUCCESS)
        strcpy( pcgMQRTable, pclTmpStr );
    dbg(TRACE,"<%s> MQ Receive Table <%s>", pclFunc, pcgMQRTable );

    /***** Initialize FLIGHT session *****/
    strcpy( pclSession, "FLIGHT_SESSION" );
    ilRc = ReadConfigEntry( cgConfigFile, pclSession,"BULK_START_HOUR",pclTmpStr);
    ilNum = 1;
    if( ilRc == RC_SUCCESS )
        ilNum = atoi(pclTmpStr);
    igFlightBulkStartHour = ilNum * SECONDS_PER_HOUR;         /* In seconds */
    dbg(TRACE,"<%s> igFlightBulkStartHour <%s> -> <%d>", pclFunc, pclTmpStr, ilNum);
    ilRc = ReadConfigEntry( cgConfigFile, pclSession,"BULK_END_HOUR",pclTmpStr);
    ilNum = 1;
    if( ilRc == RC_SUCCESS )
        ilNum = atoi(pclTmpStr);
    igFlightBulkEndHour = ilNum * SECONDS_PER_HOUR;           /* In seconds */
    dbg(TRACE,"<%s> igFlightBulkEndHour <%s> -> <%d>", pclFunc, pclTmpStr, ilNum);


    ilRc = ReadConfigEntry( cgConfigFile, pclSession,"ONLINE_START_HOUR",pclTmpStr);
    ilNum = 1;
    if( ilRc == RC_SUCCESS )
        ilNum = atoi(pclTmpStr);
    igFlightOnlineStartHour = ilNum * SECONDS_PER_HOUR;         /* In seconds */
    dbg(TRACE,"<%s> igFlightOnlineStartHour <%s> -> <%d>", pclFunc, pclTmpStr, ilNum);
    ilRc = ReadConfigEntry( cgConfigFile, pclSession,"ONLINE_END_HOUR",pclTmpStr);
    ilNum = 1;
    if( ilRc == RC_SUCCESS )
        ilNum = atoi(pclTmpStr);
    igFlightOnlineEndHour = ilNum * SECONDS_PER_HOUR;           /* In seconds */
    dbg(TRACE,"<%s> igFlightOnlineEndHour <%s> -> <%d>", pclFunc, pclTmpStr, ilNum);

    ilRc = ReadConfigEntry( cgConfigFile, pclSession,"RECEIVER",pclTmpStr);
    if( ilRc != RC_SUCCESS )
        strcpy( pclTmpStr, "iflwma" );
    igRecipentID[FLIGHT] = tool_get_q_id( pclTmpStr );
    dbg(TRACE,"<%s> FlightRecipent ID :<%s> -> <%d>", pclFunc, pclTmpStr, igRecipentID[FLIGHT] );

    ilRc = ReadConfigEntry( cgConfigFile, pclSession,"EXCLUDE_FTYP",pcgExcludeFtyp );
    if( ilRc != RC_SUCCESS )
        strcpy( pcgExcludeFtyp, "'T','G'" );
    dbg(TRACE,"<%s> FlightExcludeFtyp:<%s>", pclFunc, pcgExcludeFtyp );

    /***** Initialize STAFF session *****/
    strcpy( pclSession, "STAFF_SESSION" );
    ilRc = ReadConfigEntry( cgConfigFile, pclSession,"RECEIVER",pclTmpStr);
    if( ilRc != RC_SUCCESS )
        strcpy( pclTmpStr, "ifltwma" );
    igRecipentID[STAFF] = tool_get_q_id( pclTmpStr );
    dbg(TRACE,"<%s> StaffRecipent ID :<%s> -> <%d>", pclFunc, pclTmpStr, igRecipentID[STAFF] );


    /***** Initialize EQUIPMENT session *****/
    strcpy( pclSession, "EQUIP_SESSION" );
    ilRc = ReadConfigEntry( cgConfigFile, pclSession,"RECEIVER",pclTmpStr);
    if( ilRc != RC_SUCCESS )
        strcpy( pclTmpStr, "iflwmj" );
    igRecipentID[EQUIP] = tool_get_q_id( pclTmpStr );
    dbg(TRACE,"<%s> EquipRecipent ID :<%s> -> <%d>", pclFunc, pclTmpStr, igRecipentID[EQUIP] );

    /***** Get SQLHDL ID *****/
    ilRc = ReadConfigEntry( cgConfigFile, pclSession,"HANDLING_SQLHDL",pclTmpStr);
    if( ilRc != RC_SUCCESS )
        strcpy( pclTmpStr, "7150" );
    igSqlhdlID = atoi(pclTmpStr);
    dbg(TRACE,"<%s> SQLHDL ID :<%d>", pclFunc, igSqlhdlID );

    /***** Get BCHDL ID *****/
    igBchdlID = tool_get_q_id( "bchdl" );
    dbg(TRACE,"<%s> Broadcast Handler ID : <%d>", pclFunc, igBchdlID );

    
    /***** Initialize EQUIPMENT attribute names *****/
    for( ili = 0; ili < NUM_ATTRIBUTES; ili++ )
    {
        ilRc = ReadConfigLine(cgConfigFile, "ATTRIBUTE", pcgEquXmlFields[ili], pcgEquAttFields[ili] );
        if( ilRc != RC_SUCCESS )
            strcpy( pcgEquAttFields[ili], pcgEquXmlFields[ili] );
        dbg(TRACE,"<%s> EquipAttributesMatching XML<%s> -> ATTR<%s>", 
                                  pclFunc, pcgEquXmlFields[ili], pcgEquAttFields[ili] );
    }

    return(ilRc);
    
} /* end of initialize */

/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
    int ilRc = RC_SUCCESS;              /* Return code */
    
    dbg(TRACE,"Reset: now resetting");
    
    return ilRc;
    
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
    /* unset SIGCHLD ! DB-Child will terminate ! */

    dbg(TRACE,"Terminate: now DB logoff ...");

    signal(SIGCHLD,SIG_IGN);

    logoff();

    dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);


    dbg(TRACE,"Terminate: now leaving ...");

    fclose(outp);

    sleep(ipSleep < 1 ? 1 : ipSleep);
    
    exit(0);
    
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/

static void HandleSignal(int pipSig)
{
    dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);

    switch(pipSig)
    {
    default :
        Terminate(1);
        break;
    } /* end of switch */

    exit(1);
    
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
    dbg(TRACE,"HandleErr: <%d> : unknown ERROR",pipErr);

    return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    switch(pipErr) {
    case    QUE_E_FUNC  :   /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :   /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;
    case    QUE_E_SEND  :   /* Error using msgsnd */
        dbg(TRACE,"<%d> : msgsnd failed",pipErr);
        break;
    case    QUE_E_GET   :   /* Error using msgrcv */
        dbg(TRACE,"<%d> : msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX   :
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE  :
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES  :
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        break;
    case    QUE_E_RESP  :   /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;
    case    QUE_E_FULL  :
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG :   /* No message on queue */
        dbg(TRACE,"<%d> : no messages on queue",pipErr);
        break;
    case    QUE_E_INVORG    :   /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :   /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;
    default         :   /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
    } /* end switch */
         
    return;

} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
    int ilRc = RC_SUCCESS;          /* Return code */
    int ilBreakOut = FALSE;
    
    do
    {
        ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text; 

        if( ilRc == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRc != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRc);
            } /* fi */
        
            switch( prgEvent->command )
            {
            case    HSB_STANDBY :
                ctrl_sta = prgEvent->command;
                break;  
    
            case    HSB_COMING_UP   :
                ctrl_sta = prgEvent->command;
                break;  
    
            case    HSB_ACTIVE  :
                ctrl_sta = prgEvent->command;
                ilBreakOut = TRUE;
                break;  

            case    HSB_ACT_TO_SBY  :
                ctrl_sta = prgEvent->command;
                break;  
    
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate(1);
                break;  
    
            case    HSB_STANDALONE  :
                ctrl_sta = prgEvent->command;
                ResetDBCounter();
                ilBreakOut = TRUE;
                break;  

            case    REMOTE_DB :
                /* ctrl_sta is checked inside */
                HandleRemoteDB(prgEvent);
                break;

            case    SHUTDOWN    :
                Terminate(1);
                break;
                        
            case    RESET       :
                ilRc = Reset();
                break;
                        
            case    EVENT_DATA  :
                dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
                    
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;

            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;

            default         :
                dbg(TRACE,"HandleQueues: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        } else {
            /* Handle queuing errors */
            HandleQueErr(ilRc);
        } /* end else */
    } while (ilBreakOut == FALSE);

            ilRc = Init_Process();
            if(ilRc != RC_SUCCESS)
            {
                dbg(TRACE,"InitItkhdl: init failed!");
            } 
     

} /* end of HandleQueues */
    



/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(EVENT *prpEvent)
{
    BC_HEAD *prlBchead = NULL;
    CMDBLK  *prlCmdblk = NULL;

    int ilRc = RC_SUCCESS;         /* Return code */
    int ilCmd = 0;
    char *pclSelection = NULL;
    char *pclFields = NULL;
    char *pclData = NULL;
    char *pclRow = NULL;
    char pclTable[34];
    char *pclFunc = "HandleData";

    prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
    prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
    pclSelection = prlCmdblk->data;
    pclFields    = pclSelection + strlen(pclSelection) + 1;
    pclData      = pclFields + strlen(pclFields) + 1;


    strcpy(pclTable,prlCmdblk->obj_name);

    dbg(TRACE,"========== START <%10.10d> ==========",lgEvtCnt);

    /****************************************/
    DebugPrintBchead(TRACE,prlBchead);
    DebugPrintCmdblk(TRACE,prlCmdblk);
/*    dbg(TRACE,"Command: <%s>",prlCmdblk->command);
    dbg(TRACE,"tw_start: <%s>",prlCmdblk->tw_start);
    dbg(TRACE,"tw_end: <%s>",prlCmdblk->tw_end);
*/    dbg(TRACE,"originator follows event = %p ",prpEvent);

    dbg(TRACE,"originator<%d>",prpEvent->originator);
    dbg(TRACE,"selection follows Selection = %p ",pclSelection);
    dbg(TRACE,"selection <%s>",pclSelection);
    dbg(TRACE,"fields    <%s>",pclFields);
    dbg(TRACE,"data      <%s>",pclData);
    /****************************************/

    FormatTime( time(0), pcgTimeStr, "UTC" ); 
    strcat( pcgTimeStr, "000" );

    if( strlen(prlCmdblk->obj_name) > 0 )
    {
        if( strcmp(prlCmdblk->obj_name,pcgMQRTable) )  /* Assuming only IRT should be received */
        {
            if (strcmp(prlCmdblk->obj_name,"AFTTAB") == 0)
                HandleFlightOnlineData(pclFields, pclData, prlCmdblk->command, pclSelection);
            else if (strcmp(prlCmdblk->obj_name,"JOBTAB") == 0)
                HandleJobOnlineData(pclFields, pclData, prlCmdblk->command, pclSelection);
            /*else if (strcmp(prlCmdblk->obj_name,"EQATAB") == 0)  -- Assuming only IRT should be received
            {
                -- Delete old equipment attributes
                if( !strcmp( prlCmdblk->tw_start, mod_name ) &&
                    strlen(prlCmdblk->tw_end) > 0 )
                    -- UpdateBay(pclFields, prlCmdblk->tw_end, TYPE_DELETE);
                    UpdateBay(pclFields, pclData, TYPE_DELETE);
            } */
            return RC_SUCCESS;
        }
    }

    if( strlen(prlCmdblk->command) > 0 )
    {
        /* The ONLY message to be received from Proveo */
        if( !strncmp( prlCmdblk->command, "BAY", 3 ) )
            UpdateBay(pclFields, pclData, TYPE_UPDATE);
        else if( !strncmp( prlCmdblk->command, "BLK", 3 ) )
            SendBulkFlight();
        else if( !strncmp( prlCmdblk->command, "CFG", 3 ) )
            Init_Config();
        else if( !strncmp( prlCmdblk->command, "SYN", 3 ) )
        {
            bgSyncData = TRUE;
            SyncData();
            bgSyncData = FALSE;
        }
    }

    dbg(TRACE,"==========  END  <%10.10d> ==========",lgEvtCnt);

    return(RC_SUCCESS);
    
} /* end of HandleData */

static void SendBulkFlight()
{
    AFTREC rlAftrec;
    time_t ilNowTm, ilStartTm, ilEndTm;
    int ilRc = 0;
    int ilRecordCount = 0;
    short slSqlFunc = 0;
    short slSqlCursor = 0;
    char pclSqlData[4000] = "\0";
    char pclSqlQuery[4000] = "\0";
    char pclTmpStr[1000] = "\0";
    char pclStartTimer[DATELEN] = "\0";
    char pclEndTimer[DATELEN] = "\0";
    char *pclFunc = "SendBulkFlight";


    dbg(TRACE,"<%s> ####### START ##########", pclFunc );
    
    ilNowTm   = time(0);
    ilStartTm = ilNowTm + igFlightBulkStartHour;
    ilEndTm   = ilNowTm + igFlightBulkEndHour;
    FormatTime( ilStartTm, pclStartTimer, "UTC" );
    FormatTime( ilEndTm,   pclEndTimer,   "UTC" );

    sprintf( pclSqlQuery, "SELECT %s FROM AFTTAB WHERE FTYP NOT IN (%s) AND ((TIFD BETWEEN '%s' AND '%s') "
                          "AND (ORG3 = 'SIN')) OR ((TIFA BETWEEN '%s' AND '%s') AND (DES3='SIN')) ", 
                          pcgFlightDbFields, pcgExcludeFtyp, pclStartTimer, 
                          pclEndTimer, pclStartTimer, pclEndTimer );
    dbg(DEBUG,"<%s> Sql: <%s>", pclFunc, pclSqlQuery);


    slSqlFunc = START;
    slSqlCursor = 0;
    while((ilRc = sql_if(slSqlFunc,&slSqlCursor,pclSqlQuery,pclSqlData)) == DB_SUCCESS)
    {
        memset( &rlAftrec, 0, sizeof(rlAftrec) );
        ilRecordCount++;
        slSqlFunc = NEXT;

        get_fld(pclSqlData,FIELD_1,STR,URNOLEN,rlAftrec.URNO);
        dbg(DEBUG,"<%s> Rec No: <%d> URNO <%s> ---------",pclFunc,ilRecordCount, rlAftrec.URNO);
        get_fld(pclSqlData,FIELD_2,STR,THREE_FOUR,rlAftrec.FLNO); strip_special_char(rlAftrec.FLNO);
        get_fld(pclSqlData,FIELD_3,STR,COTYLEN,rlAftrec.ALC2);  strip_special_char(rlAftrec.ALC2);
        get_fld(pclSqlData,FIELD_4,STR,COTYLEN,rlAftrec.ALC3);  strip_special_char(rlAftrec.ALC3);
        get_fld(pclSqlData,FIELD_5,STR,TWO_FOUR,rlAftrec.FLTN); strip_special_char(rlAftrec.FLTN);
        get_fld(pclSqlData,FIELD_6,STR,ONE_FOUR,rlAftrec.FLNS); strip_special_char(rlAftrec.FLNS);
        get_fld(pclSqlData,FIELD_7,STR,DATELEN,rlAftrec.STOA);  /*UtcToLocalTimeFixTZ(rlAftrec.STOA); */
        get_fld(pclSqlData,FIELD_8,STR,DATELEN,rlAftrec.STOD);  /*UtcToLocalTimeFixTZ(rlAftrec.STOD); */
        get_fld(pclSqlData,FIELD_9,STR,ONE_FOUR,rlAftrec.ADID); strip_special_char(rlAftrec.ADID); 
        get_fld(pclSqlData,FIELD_10,STR,URNOLEN,rlAftrec.RKEY); strip_special_char(rlAftrec.RKEY);
        get_fld(pclSqlData,FIELD_11,STR,FOUR_FOUR,rlAftrec.REGN); strip_special_char(rlAftrec.REGN);
        get_fld(pclSqlData,FIELD_12,STR,THREE_FOUR,rlAftrec.CSGN); strip_special_char(rlAftrec.CSGN);
        get_fld(pclSqlData,FIELD_13,STR,DATELEN,rlAftrec.ETAI); /*UtcToLocalTimeFixTZ(rlAftrec.ETAI); */
        get_fld(pclSqlData,FIELD_14,STR,DATELEN,rlAftrec.ETDI); /*UtcToLocalTimeFixTZ(rlAftrec.ETDI); */
        get_fld(pclSqlData,FIELD_15,STR,COTYLEN,rlAftrec.DES3); strip_special_char(rlAftrec.DES3);
        get_fld(pclSqlData,FIELD_16,STR,COTYLEN,rlAftrec.ORG3); strip_special_char(rlAftrec.ORG3);
        get_fld(pclSqlData,FIELD_17,STR,ONE_FOUR,rlAftrec.FTYP);strip_special_char(rlAftrec.FTYP);
        get_fld(pclSqlData,FIELD_18,STR,TWO_FOUR,rlAftrec.TTYP);strip_special_char(rlAftrec.TTYP);
        get_fld(pclSqlData,FIELD_19,STR,COTYLEN,rlAftrec.ACT3); strip_special_char(rlAftrec.ACT3);
        get_fld(pclSqlData,FIELD_20,STR,TWO_FOUR,rlAftrec.ACT5);strip_special_char(rlAftrec.ACT5);
        get_fld(pclSqlData,FIELD_21,STR,DATELEN,rlAftrec.LAND); /*UtcToLocalTimeFixTZ(rlAftrec.LAND); */
        get_fld(pclSqlData,FIELD_22,STR,DATELEN,rlAftrec.ONBL); /*UtcToLocalTimeFixTZ(rlAftrec.ONBL); */
        get_fld(pclSqlData,FIELD_23,STR,DATELEN,rlAftrec.AIRB); /*UtcToLocalTimeFixTZ(rlAftrec.AIRB); */
        get_fld(pclSqlData,FIELD_24,STR,DATELEN,rlAftrec.OFBL); /*UtcToLocalTimeFixTZ(rlAftrec.OFBL); */
        get_fld(pclSqlData,FIELD_25,STR,VIALLEN,rlAftrec.VIAL); strip_special_char(rlAftrec.VIAL); 
        get_fld(pclSqlData,FIELD_26,STR,TWO_FOUR,rlAftrec.PSTA);strip_special_char(rlAftrec.PSTA); 
        get_fld(pclSqlData,FIELD_27,STR,TWO_FOUR,rlAftrec.PSTD);strip_special_char(rlAftrec.PSTD); 

        dbg( DEBUG, "<%s> URNO <%s>", pclFunc, rlAftrec.URNO );
        dbg( DEBUG, "<%s> FLNO <%s>", pclFunc, rlAftrec.FLNO );
        dbg( DEBUG, "<%s> ALC2 <%s>", pclFunc, rlAftrec.ALC2 );
        dbg( DEBUG, "<%s> ALC3 <%s>", pclFunc, rlAftrec.ALC3 );
        dbg( DEBUG, "<%s> FLTN <%s>", pclFunc, rlAftrec.FLTN );
        dbg( DEBUG, "<%s> FLNS <%s>", pclFunc, rlAftrec.FLNS );
        dbg( DEBUG, "<%s> STOA <%s>", pclFunc, rlAftrec.STOA );
        dbg( DEBUG, "<%s> STOD <%s>", pclFunc, rlAftrec.STOD );
        dbg( DEBUG, "<%s> ADID <%s>", pclFunc, rlAftrec.ADID );
        dbg( DEBUG, "<%s> RKEY <%s>", pclFunc, rlAftrec.RKEY );
        dbg( DEBUG, "<%s> REGN <%s>", pclFunc, rlAftrec.REGN );
        dbg( DEBUG, "<%s> CSGN <%s>", pclFunc, rlAftrec.CSGN );
        dbg( DEBUG, "<%s> ETAI <%s>", pclFunc, rlAftrec.ETAI );
        dbg( DEBUG, "<%s> ETDI <%s>", pclFunc, rlAftrec.ETDI );
        dbg( DEBUG, "<%s> DES3 <%s>", pclFunc, rlAftrec.DES3 );
        dbg( DEBUG, "<%s> ORG3 <%s>", pclFunc, rlAftrec.ORG3 );
        dbg( DEBUG, "<%s> FTYP <%s>", pclFunc, rlAftrec.FTYP );
        dbg( DEBUG, "<%s> TTYP <%s>", pclFunc, rlAftrec.TTYP );
        dbg( DEBUG, "<%s> ACT3 <%s>", pclFunc, rlAftrec.ACT3 );
        dbg( DEBUG, "<%s> ACT5 <%s>", pclFunc, rlAftrec.ACT5 );
        dbg( DEBUG, "<%s> LAND <%s>", pclFunc, rlAftrec.LAND );
        dbg( DEBUG, "<%s> ONBL <%s>", pclFunc, rlAftrec.ONBL );
        dbg( DEBUG, "<%s> AIRB <%s>", pclFunc, rlAftrec.AIRB );
        dbg( DEBUG, "<%s> OFBL <%s>", pclFunc, rlAftrec.OFBL );
        dbg( DEBUG, "<%s> VIAL <%s>", pclFunc, rlAftrec.VIAL );
        dbg( DEBUG, "<%s> PSTA <%s>", pclFunc, rlAftrec.PSTA );
        dbg( DEBUG, "<%s> PSTD <%s>", pclFunc, rlAftrec.PSTD );

        if( rlAftrec.ADID[0] == 'B' )
            strcpy( rlAftrec.ADID, "D" );

        PackFlightXml( rlAftrec, TYPE_BULK );
        dbg(DEBUG,"<%s> Flight Xml <%s>", pclFunc, pcgXmlStr);
        ilRc = SendCedaEvent(igRecipentID[FLIGHT],0,mod_name,"EXCO",mod_name,"","FLT","","","",pcgXmlStr,"",
                             igSendPrio,RC_SUCCESS);
        dbg( TRACE, "<%s> Send ilRc = %d", pclFunc, ilRc );
    }
    close_my_cursor(&slSqlCursor);
    dbg( TRACE, "<%s> Read %d flight records", pclFunc, ilRecordCount );
}


static void HandleFlightOnlineData( char *pcpFields, char *pcpData, char *pcpCmd, char *pcpSelection )
{
    BOOL blNewUpdate = FALSE;
    BOOL blSkip = FALSE;
    BOOL blReturnFlight = FALSE;
    BOOL blSinOrig = FALSE;
    time_t ilNowTm, ilStartTm, ilEndTm;
    int ilRc = RC_SUCCESS;
    int ilItemCount = 0;
    int ilItemPosition = 0;
    int ili, ilj;
    int ilLen;
    int ilActType;
    /* Base on pclFlightFields, hardcode the index of SEND ALWAYS fields */
    int ilSendAlwaysStart;
    int ilSendAlwaysEnd;
    int ilSpecFltFieldsIdx = 23; /* for pclFlightFields Indexing, VIAL onwards */
    int ilCsgnIdx = 0;           /* for pclFlightFields Indexing, CSGN */
    int ilFlnoIdx = 1;           /* for pclFlightFields Indexing, FLNO */
    int ilAdidIdx = 9;           /* for pclFlightFields Indexing, ADID */
    int ilAlc3Idx = 3;           /* for pclFlightFields Indexing, ALC3 */
    int ilFlnsIdx = 5;           /* for pclFlightFields Indexing, FLNS */
    int ilFtypIdx = 15;          /* for pclFlightFields Indexing, FTYP */
    int ilTimeFields[NUM_TIME_FIELDS] = { 6, 7, 11, 12, 19, 20, 21, 22 };
    char clActtype;
    char pclOneField[TWO_FOUR];
    char pclStartTimer[DATELEN] = "\0";
    char pclEndTimer[DATELEN] = "\0";
    char pclSqlBuf[DBQRLEN] = "\0";     
    char pclSqlData[DBQRLEN] = "\0";     
    char pclTmpStr[DBQRLEN] = "\0";     
    char pclData[2][DBQRLEN];
    char pclInfo[2][1200];
    char pclPstn[TWO_FOUR];
    char pclVial[VIALLEN];
    char pclFlns[ONE_FOUR];
    char pclAlc3[ONE_FOUR];
    char pclTif[DATELEN] = "\0";
    char pclUaft[URNOLEN];

    /* Upd Idx declare above */
    /* NOTE: CSGN and FLNO have to be in the front of the array             */
    /*       as the info for ALC3 and FLNS are derived from these 2 fields  */
    char pclFlightFields[MAX_FLIGHT_FIELDS][5] = { "CSGN", "FLNO", "ALC2", "ALC3", "FLTN", "FLNS", "STOA", 
                                                   "STOD", "RKEY", "ADID", "REGN", "ETAI", "ETDI", "DES3", 
                                                   "ORG3", "FTYP", "TTYP", "ACT3", "ACT5", "LAND", "ONBL", 
                                                   "AIRB", "OFBL", "VIAL", "PSTA", "PSTD" };  
    char *pclFunc = "HandleFlightOnlineData";
    char *pclToken;

    dbg(TRACE,"<%s> ---------- START ---------",pclFunc);

    memset( pclInfo, 0, sizeof(pclInfo) );
    memset( pclData, 0, sizeof(pclData) );


    strcpy( pclData[0], pcpData );
    pclToken = (char *)strstr( pclData[0], "\n" );
    if( pclToken != NULL )
    {
        *pclToken = '\0';
        pclToken++;
        strcpy( pclData[1], pclToken );
    }
    strip_special_char(pclData[0]);
    strip_special_char(pclData[1]);
    dbg(DEBUG,"<%s> Str1 <%s>",pclFunc, pclData[0] );
    dbg(DEBUG,"<%s> Str2 <%s>",pclFunc, pclData[1] );

    ilSendAlwaysStart = ilFlnoIdx;
    ilSendAlwaysEnd = ilAdidIdx;

    ilItemPosition = get_item_no(pcpFields,"URNO",5);
    if( ilItemPosition < 0 ) /* Item not found */
    {
        strcpy( pclTmpStr, pcpSelection );
        pclToken = (char *)strstr( pclTmpStr, "\n" );
        if( pclToken != NULL )
        {
            *pclToken = '\0';
            pclToken++;
            strcpy( pclUaft, pclToken );
        }

        if( strlen(pclUaft) <= 0 || atoi(pclUaft) <= 0 )
        {
            dbg(TRACE,"<%s> URNO not found <%s>",pclFunc, pcpFields );
            dbg(TRACE,"<%s> ---------- END ---------",pclFunc);
            return;
        }

        /* For RKEY field update, the rest of the SEND_ALWAYS fields have to be retrieved from DB */
        memset( pcpFields, 0, strlen(pcpFields) );
        sprintf( pclSqlBuf, "%s", "SELECT " );
        for( ili = ilSendAlwaysStart; ili <= ilSendAlwaysEnd; ili++ )
        {
            sprintf( pclTmpStr, "TRIM(%s),", pclFlightFields[ili] );
            strcat( pclSqlBuf, pclTmpStr );
            sprintf( pclTmpStr, "%s,", pclFlightFields[ili] );
            strcat( pcpFields, pclTmpStr );
        }
        strcat( pcpFields, "TIFA,TIFD" );
        dbg( TRACE, "<%s> Fields <%s>", pclFunc, pcpFields );

        sprintf( pclTmpStr, " TRIM(TIFA), TRIM(TIFD) FROM AFTTAB WHERE URNO = %s", pclUaft );
        strcat( pclSqlBuf, pclTmpStr );
        dbg( TRACE, "<%s> ReSelAFT <%s>", pclFunc, pclSqlBuf );
        
        ilRc = getOrPutDBData( pclSqlBuf, pclSqlData, START );
        if( ilRc != DB_SUCCESS )
        {
            dbg( TRACE, "<%s> No record found in AFTTAB - URNO <%s>", pclFunc, pclUaft );
            return;
        }
        memset( pclData, 0, sizeof(pclData) );
        ilj = (ilSendAlwaysEnd-ilSendAlwaysStart)+2;
        for( ili = 0; ili < ilj; ili++ )
        {
            get_fld(pclSqlData,ili,STR,100,pclTmpStr); 
            strcat( pclData[0], pclTmpStr );
            if( ili < ilj )
            {
                strcat( pclData[0], "," ); 
                strcat( pclData[1], "," );
            }
        }
        dbg( TRACE, "<%s> Data[0] <%s>", pclFunc, pclData[0] );
    }
    else
        get_real_item(pclUaft, pclData[0], ilItemPosition+1);

    clActtype = 'U'; 
    ilActType = TYPE_UPDATE;
    if( !strncmp( pcpCmd, "DFR", 3 ) )
    {
        ilActType = TYPE_DELETE;
        clActtype = 'D'; 
    }
    else if( !strncmp( pcpCmd, "IFR", 3 ) )
        ilActType = TYPE_INSERT;

    sprintf( pcgXmlStr, "%s\n" 
                        "<MSG>\n"
                        "<MSGSTREAM_OUT>\n"
                        "<INFOBJ_GENERIC_FLIGHT>\n"
                        "<TIMESTAMP>%s</TIMESTAMP>\n"
                        "<ACTIONTYPE>%c</ACTIONTYPE>\n"
                        "<UAFT>%s</UAFT>\n"
                        "</INFOBJ_GENERIC_FLIGHT>\n", pcgXmlHeader, pcgTimeStr, clActtype, pclUaft );

    /* Delete */
    if( ilActType == TYPE_DELETE )
    {
        ilItemPosition = get_item_no(pcpFields,"FTYP",5);
        if( ilItemPosition >= 0 ) 
        {
            get_real_item(pclTmpStr, pclData[0], ilItemPosition+1);
            pclToken = (char *)strstr( pcgExcludeFtyp, pclTmpStr );
            if( pclToken != NULL )
            {
                dbg( TRACE, "<%s> Del FTYP <%s> is not required. Throw", pclFunc, pclTmpStr );
                return;
            } 
        }
        sprintf( pclTmpStr, "%s", "</MSGSTREAM_OUT>\n"
                                  "</MSG>\n" );
        strcat( pcgXmlStr, pclTmpStr );
        dbg(TRACE,"<%s> DFR Xml <%s>",pclFunc, pcgXmlStr);
        ilRc = SendCedaEvent(igRecipentID[FLIGHT],0,mod_name,"EXCO",mod_name,"","FLT","","","",
                             pcgXmlStr,"",igSendPrio,RC_SUCCESS);
        dbg(TRACE,"<%s> SendCedaEvent-DFR ilRc = %d",pclFunc, ilRc );
        dbg(TRACE,"<%s> ---------- END ---------",pclFunc);
        return;
    }

    /* Check validity of time */
    ilItemPosition = get_item_no(pcpFields,"ORG3",5);
    if( ilItemPosition >= 0 ) 
    {
        get_real_item(pclTmpStr, pclData[0], ilItemPosition+1);
        if( !strncmp( pclTmpStr, cgHopo, 3 ) ) /* departure flight */
        {
            blSinOrig = TRUE;
            ilItemPosition = get_item_no(pcpFields,"TIFD",5);
            if( ilItemPosition >= 0 ) 
                get_real_item(pclTif, pclData[0], ilItemPosition+1);
        }
    }
    ilItemPosition = get_item_no(pcpFields,"DES3",5);
    if( ilItemPosition >= 0 )
    {
        get_real_item(pclTmpStr, pclData[0], ilItemPosition+1);
        if( !strncmp( pclTmpStr, cgHopo, 3 ) ) /* arrival flight */
        {
            if( blSinOrig == TRUE )
            {
                dbg( TRACE, "<%s> RETURN FLIGHT/TAXI", pclFunc );
                blReturnFlight = TRUE;
            }
            ilItemPosition = get_item_no(pcpFields,"TIFA",5);
            if( ilItemPosition >= 0 ) 
                get_real_item(pclTif, pclData[0], ilItemPosition+1);
        }
    }

    blSkip = FALSE;
    if( strlen(pclTif) > 0 )
    {
        ilNowTm   = time(0);
        ilStartTm = ilNowTm + igFlightOnlineStartHour;
        ilEndTm   = ilNowTm + igFlightOnlineEndHour;
        FormatTime( ilStartTm, pclStartTimer, "UTC" );
        FormatTime( ilEndTm,   pclEndTimer,   "UTC" );
        blSkip = FALSE;
        dbg( TRACE, "<%s> ChkTime Start <%s> Tif <%s> End <%s>", pclFunc, pclStartTimer, pclTif, pclEndTimer );
        if ((strcmp(pclTif,pclStartTimer) < 0) || (strcmp(pclTif,pclEndTimer) > 0))
            blSkip = TRUE;
    }

    if( blSkip == TRUE )
    {
        dbg( TRACE, "<%s> UAFT <%s> skip. Wrong time.", pclFunc, pclUaft ); 
        dbg(TRACE,"<%s> ---------- END ---------",pclFunc);
        return;
    }

    sprintf( pclTmpStr, "%s", "<MSGOBJECTS>\n"
                              "<INFOBJ_FLIGHT>\n" );
    strcat( pcgXmlStr, pclTmpStr );

    /* Update and Insert */
    blNewUpdate = FALSE;

    for( ili = 0; ili < MAX_FLIGHT_FIELDS; ili++ )
    {
        strcpy( pclOneField, pclFlightFields[ili] );
        ilItemPosition = get_item_no(pcpFields,pclOneField,5);
        if( ilItemPosition < 0 )
        {
            if( ili >= ilSendAlwaysStart && ili <= ilSendAlwaysEnd )
            {
                if( ili == ilAlc3Idx )
                    sprintf( pclTmpStr, "<%s>%s</%s>\n", pclOneField, pclAlc3, pclOneField );
                else if( ili == ilFlnsIdx )
                    sprintf( pclTmpStr, "<%s>%s</%s>\n", pclOneField, pclFlns, pclOneField );
                else
                    sprintf( pclTmpStr, "<%s></%s>\n", pclOneField, pclOneField );
                strcat( pcgXmlStr, pclTmpStr );
            }
            continue;
        }
        get_real_item(pclInfo[0], pclData[0], ilItemPosition+1);

        if( ili == ilCsgnIdx )
        {
            sprintf( pclAlc3, "%3.3s", pclInfo[0] );  
            dbg( DEBUG, "<%s> pclAlc3 <%s>", pclFunc, pclAlc3 );
        }
        else if( ili == ilFlnoIdx )
        {
            sprintf( pclTmpStr, "%9.9s", pclInfo[0] );
            sprintf( pclFlns, "%c", pclTmpStr[0] );
            dbg( DEBUG, "<%s> pclFlns <%s>", pclFunc, pclFlns );
        }
        else if( ili == ilFtypIdx )
        {
            pclToken = (char *)strstr( pcgExcludeFtyp, pclInfo[0] );
            if( pclToken != NULL )
            {
                dbg( TRACE, "<%s> FTYP <%s> is not required. Throw", pclFunc, pclInfo[0] );
                return;
            } 
        }

        if( ilActType == TYPE_UPDATE )
        {

            get_real_item(pclInfo[1], pclData[1], ilItemPosition+1);
            dbg(DEBUG,"<%s> Fields[%d] <%s> Info O<%s> N<%s>", pclFunc, ili, pclOneField, pclInfo[0], pclInfo[1] );

            if( !strcmp( pclInfo[0], pclInfo[1] ) )
            {
                if( ili < ilSendAlwaysStart || ili > ilSendAlwaysEnd )
                    continue;
            }
            else
                blNewUpdate = TRUE;

            if( ili == ilAdidIdx && blReturnFlight == TRUE )
                strcpy( pclInfo[0], "D" );
        }
        /*else
        {   
            if( strlen(pclInfo[0]) <= 0 )
                continue;
        }*/

        if( ili >= ilSpecFltFieldsIdx )
        {
            if( !strncmp(pclOneField, "PSTD", 4)) 
            {
                if( blSinOrig == TRUE )
                {
                    sprintf( pclTmpStr, "<PSTN>%s</PSTN>\n", pclInfo[0] );
                    strcat( pcgXmlStr, pclTmpStr );
                }
                continue;
            }
            else if( !strncmp(pclOneField, "PSTA", 4)) 
            {
                if( blSinOrig != TRUE )
                {
                    sprintf( pclTmpStr, "<PSTN>%s</PSTN>\n", pclInfo[0] );
                    strcat( pcgXmlStr, pclTmpStr );
                }
                continue;
            }
            else if( !strncmp(pclOneField, "VIAL", 4) )
            {
                sprintf( pclVial, "%-960.960s", pclInfo[0] );
                dbg( DEBUG, "<%s> VIAL <%s>", pclFunc, pclVial ); 
                for( ilj = 1; ilj <= 8; ilj++ )
                {
                    sprintf( pclTmpStr, "<VIA%d>%3.3s</VIA%d>\n", ilj, &pclVial[((ilj-1)*ONE_VIA_SIZE)+1], ilj );
                    dbg( DEBUG, "<%s> VIA%d: (%s)", pclFunc, ilj, pclTmpStr ); 
                    strcat( pcgXmlStr, pclTmpStr );
                }
                continue;
            }
        }

        for( ilj = 0; ilj < NUM_TIME_FIELDS; ilj++ )
        {
            if( ili == ilTimeFields[ilj] )
            {
                if( strlen(pclInfo[0]) > 0 )
                    sprintf( pclTmpStr, "<%s>%s000</%s>\n", pclOneField, pclInfo[0], pclOneField );
                else
                    sprintf( pclTmpStr, "<%s></%s>\n", pclOneField, pclOneField );
                break;
            }
        }
        if( ilj >= NUM_TIME_FIELDS )
            sprintf( pclTmpStr, "<%s>%s</%s>\n", pclOneField, pclInfo[0], pclOneField );
        strcat( pcgXmlStr, pclTmpStr );
    }

    strcpy( pclTmpStr, "</INFOBJ_FLIGHT>\n"
                       "</MSGOBJECTS>\n"
                       "</MSGSTREAM_OUT>\n"
                       "</MSG>\n" );
    strcat( pcgXmlStr, pclTmpStr );


    if( ilActType == TYPE_UPDATE )
    {
        if( blNewUpdate == FALSE )
        {
            dbg(TRACE,"<%s> Nothing to Update",pclFunc );
            dbg(TRACE,"<%s> ---------- END ---------",pclFunc);
            return;
        }
    }

    dbg(TRACE,"<%s> IFR-UFR Xml <%s>",pclFunc, pcgXmlStr);
    ilRc = SendCedaEvent(igRecipentID[FLIGHT],0,mod_name,"EXCO",mod_name,"","FLT","","","",
                         pcgXmlStr,"",igSendPrio,RC_SUCCESS);
    dbg(TRACE,"<%s> SendCedaEvent ilRc = %d",pclFunc, ilRc );
    dbg(TRACE,"<%s> ---------- END ---------",pclFunc);
    return;
                                    
} /* HandleFlightOnline Data */

static void HandleJobOnlineData(char *pcpFields, char *pcpData, char *pcpCmd, char *pcpSelection)
{
    BOOL blTurnArndInsert = FALSE;
    JOBREC rlJobrec, rlJobrec_o;
    STFREC rlStfrec;
    EQUREC rlEqurec;
    int ilRecordCount;
    int ilActType;
    int ilRc = RC_SUCCESS;
    int ilItemPosition = 0;
    int ilItemCount = 0;
    int ili, ilj;
    int ilLen;
    int ilFound;
    short slSqlCursor;
    short slSqlFunc;
    char pclTmpStr[DBQRLEN] = "\0";     
    char pclData[2][DBQRLEN];
    char pclInfo[2][1200];
    char pclSqlBuf[DBQRLEN];
    char *pclFunc = "HandleJobOnlineData";
    char *pclToken;

    dbg(TRACE,"<%s> ---------- START ---------",pclFunc);

    memset( pclInfo, 0, sizeof(pclInfo) );
    memset( pclData, 0, sizeof(pclData) );
    memset( &rlJobrec, 0, sizeof(JOBREC) );
    memset( &rlJobrec_o, 0, sizeof(JOBREC) );
    memset( &rlStfrec, 0, sizeof(STFREC) );
    memset( &rlEqurec, 0, sizeof(EQUREC) );

    strcpy( pclData[0], pcpData );
    pclToken = (char *)strstr( pclData[0], "\n" );
    if( pclToken != NULL )
    {
        *pclToken = '\0';
        pclToken++;
        strcpy( pclData[1], pclToken );
    }
    strip_special_char(pclData[0]);
    strip_special_char(pclData[1]);
    dbg(DEBUG,"<%s> Str1 <%s>",pclFunc, pclData[0] );
    dbg(DEBUG,"<%s> Str2 <%s>",pclFunc, pclData[1] );

    if( !strncmp( pcpCmd, "IRT", 3 ) )
        ilActType = TYPE_INSERT;
    else if( !strncmp( pcpCmd, "URT", 3 ) )
        ilActType = TYPE_UPDATE;
    else if( !strncmp( pcpCmd, "DRT", 3 ) )
        ilActType = TYPE_DELETE;
   
    /* Gets Job's URNO */
    sprintf( pclTmpStr, "%s", pcpSelection );
    if( strstr( pclTmpStr, "'" ) != NULL )
    {
        ilFound = GetElementValue(rlJobrec.URNO, pcpSelection, "'", "'");
        if( ilFound == FALSE || strlen(rlJobrec.URNO) <= 0 )
        {
            dbg(TRACE,"<%s> UJOB is empty <%s>",pclFunc, rlJobrec.URNO );
            dbg(TRACE,"<%s> ---------- END ---------",pclFunc);
            return;
        }
    }
    else
    {
        sprintf( rlJobrec.URNO, "%10.10s", pcpSelection );
    }
    dbg( DEBUG, "<%s> Sel UJOB <%s>", pclFunc, rlJobrec.URNO );
    if( atoi(rlJobrec.URNO) <= 0 )
    {
        ilItemPosition = get_item_no(pcpFields,"URNO",5);
        if( ilItemPosition < 0 ) 
        {
            dbg(TRACE,"<%s> URNO not found <%s>",pclFunc, pcpFields );
            dbg(TRACE,"<%s> ---------- END ---------",pclFunc);
            return;
        }
        get_real_item(rlJobrec.URNO, pclData[0], ilItemPosition+1);
        ilLen = strlen( rlJobrec.URNO );
        if( ilLen <= 0 || ilLen >= 10 || atoi(rlJobrec.URNO) <= 0 )
            get_real_item(rlJobrec.URNO, pclData[1], ilItemPosition+1);
    }
    
    dbg( DEBUG, "<%s> Fld UJOB <%s>", pclFunc, rlJobrec.URNO );
    if( atoi(rlJobrec.URNO) <= 0 )
    {
        dbg(TRACE,"<%s> Invalid URNO",pclFunc );
        dbg(TRACE,"<%s> ---------- END ---------",pclFunc);
        return;
    }

    ilItemPosition = get_item_no(pcpFields,"UAFT",5);
    if( ilItemPosition >= 0 )
        get_real_item(rlJobrec.UAFT, pclData[0], ilItemPosition+1);
    if( strlen(rlJobrec.UAFT) <= 0 && ilActType == TYPE_INSERT )
    {
        dbg(TRACE,"<%s> UAFT is missing <%s> Insert fail!",pclFunc, rlJobrec.URNO );
        dbg(TRACE,"<%s> ---------- END ---------",pclFunc);
        return;
    }
    dbg( TRACE, "<%s> UAFT <%s>", pclFunc, rlJobrec.UAFT );


    if( ilActType != TYPE_DELETE )
    {
        ilItemPosition = get_item_no(pcpFields,"UTPL",5);
        if( ilItemPosition >= 0 )
        {
            get_real_item(rlJobrec.UTPL, pclData[0], ilItemPosition+1);
            dbg( TRACE, "<%s> UTPL <%s>", pclFunc, rlJobrec.UTPL );

            sprintf( pclTmpStr, "'%s'", rlJobrec.UTPL );
            if( strstr(pcgApronTemplate, pclTmpStr) == NULL )
            {
                dbg( TRACE, "<%s> Not the required template. Return.", pclFunc );
                return;
            }
        }
    }

    ilItemPosition = get_item_no(pcpFields,"UDSR",5);
    if( ilItemPosition >= 0 )
        get_real_item(rlJobrec.UDSR, pclData[0], ilItemPosition+1);
    dbg( TRACE, "<%s> UDSR <%s>", pclFunc, rlJobrec.UDSR );


    /* Delete Job */
    if( ilActType == TYPE_DELETE )
    {
        ilRc = ReadDB( &rlJobrec, &rlStfrec, &rlEqurec, NOT_TURNAROUND );
        if( ilRc == RC_SUCCESS )
        {
            DeleteJob( rlJobrec, rlStfrec, rlEqurec, TYPE_BOTH );
            if( strlen(rlJobrec.RKEY) > 0 )
            {
                dbg( TRACE, "<%s> DelTurnArndDemand, delete arrival job as well", pclFunc );
                ilRc = ReadDB( &rlJobrec, &rlStfrec, &rlEqurec, IS_TURNAROUND );
                if( ilRc == RC_SUCCESS )
                    DeleteJob( rlJobrec, rlStfrec, rlEqurec, TYPE_BOTH );
            }
        }
        else
            dbg(TRACE,"<%s> UJOB <%s> not found in IFLJOB. Delete fail!", pclFunc, rlJobrec.URNO);
        dbg(TRACE,"<%s> ---------- END ---------",pclFunc);
        return;
    }


    /* Insert Job */
    /* Either job or staff will be inserted, will not insert both at same time */
    if( ilActType == TYPE_INSERT )
    {
        ilItemPosition = get_item_no(pcpFields,"DETY",5);
        if( ilItemPosition >= 0 )
        {
            get_real_item( rlJobrec.DETY, pclData[0], ilItemPosition+1 );
            if( rlJobrec.DETY[0] == '0' )
            {
                ilRc = GetTurnaroundUAFT( rlJobrec.UAFT, rlJobrec_o.UAFT, rlJobrec.RKEY );
                if( ilRc == DB_SUCCESS )
                    blTurnArndInsert = TRUE;
            }
        }


        /* Insert Equipment */
        ilItemPosition = get_item_no(pcpFields,"UEQU",5);
        if( ilItemPosition >= 0 )
        {
            get_real_item( rlEqurec.URNO, pclData[0], ilItemPosition+1 );
            if( atoi(rlEqurec.URNO) > 0 )
            {
                dbg( TRACE, "<%s> Insert equipt UEQU <%s>", pclFunc, rlEqurec.URNO );
                UpdateJob( rlJobrec, rlStfrec, rlEqurec, TYPE_INSERT, TYPE_EQUIP );
                if( blTurnArndInsert == TRUE )
                {
                    strcpy( rlJobrec.UAFT, rlJobrec_o.UAFT );
                    UpdateJob( rlJobrec, rlStfrec, rlEqurec, TYPE_INSERT, TYPE_EQUIP );
                }
            }
        }

        /* Insert Staff */
        ilItemPosition = get_item_no(pcpFields,"USTF",5);
        if( ilItemPosition >= 0 )
        {
            get_real_item( rlStfrec.URNO, pclData[0], ilItemPosition+1 );
            if( atoi(rlStfrec.URNO) > 0 )
            {
                dbg( TRACE, "<%s> Insert staff USTF <%s>", pclFunc, rlStfrec.URNO );
                ilItemPosition = get_item_no(pcpFields,"FCCO",5);
                if( ilItemPosition >= 0 )
                    get_real_item( rlJobrec.FCCO, pclData[0], ilItemPosition+1 );
                ilItemPosition = get_item_no(pcpFields,"WGPC",5);
                if( ilItemPosition >= 0 )
                    get_real_item( rlJobrec.WGPC, pclData[0], ilItemPosition+1 );
                UpdateJob( rlJobrec, rlStfrec, rlEqurec, TYPE_INSERT, TYPE_STAFF );
                if( blTurnArndInsert == TRUE )
                {
                    strcpy( rlJobrec.UAFT, rlJobrec_o.UAFT );
                    UpdateJob( rlJobrec, rlStfrec, rlEqurec, TYPE_INSERT, TYPE_STAFF );
                }
            }
        }
        dbg(TRACE,"<%s> ---------- END ---------",pclFunc);
        return;     
    }

    /* Update Equip Job */
    ilItemPosition = get_item_no(pcpFields,"UEQU",5);
    if( ilItemPosition >= 0 )
        get_real_item( pclInfo[0], pclData[0], ilItemPosition+1 );
    if( atoi(pclInfo[0]) > 0 )
    {
        get_real_item( pclInfo[1], pclData[1], ilItemPosition+1 );
        if( !strcmp( pclInfo[0], pclInfo[1] ) )
            dbg( TRACE, "<%s> No change in equip", pclFunc );
        else
        {
            ilRc = ReadDB( &rlJobrec, &rlStfrec, &rlEqurec, NOT_TURNAROUND );
            if( ilRc != DB_SUCCESS )
                dbg(TRACE,"<%s> UJOB <%s> not found in IFLJOB. Upd equip fail!", pclFunc, rlJobrec.URNO);
            else
            {
                dbg( TRACE, "<%s> Equip change. Old Equip GCDE <%s> New URNO <%s>", 
                                  pclFunc, rlEqurec.GCDE, pclInfo[0] );
                DeleteJob( rlJobrec, rlStfrec, rlEqurec, TYPE_EQUIP );
                memset( &rlEqurec, 0, sizeof(EQUREC) );
                strcpy( rlEqurec.URNO, pclInfo[0] );
                UpdateJob( rlJobrec, rlStfrec, rlEqurec, TYPE_INSERT, TYPE_EQUIP );

                /* Delete Turnaround Arrival Demand */
                if( strlen(rlJobrec.RKEY) > 0 )
                {
                    ilRc = ReadDB( &rlJobrec, &rlStfrec, &rlEqurec, IS_TURNAROUND );
                    if( ilRc == DB_SUCCESS )
                    {
                        DeleteJob( rlJobrec, rlStfrec, rlEqurec, TYPE_EQUIP );
                        memset( &rlEqurec, 0, sizeof(EQUREC) );
                        strcpy( rlEqurec.URNO, pclInfo[0] );
                        UpdateJob( rlJobrec, rlStfrec, rlEqurec, TYPE_INSERT, TYPE_EQUIP );
                    }
                }
            }
        }
        dbg(TRACE,"<%s> ---------- END ---------",pclFunc);
        return;             
    }

    /* Update Staff Job */ 
    ilRc = ReadDB( &rlJobrec, &rlStfrec, &rlEqurec, NOT_TURNAROUND );
    if( ilRc != DB_SUCCESS )
    {
        dbg( TRACE, "<%s> Rec not found in IFLJOB UJOB <%s>", pclFunc, rlJobrec.URNO );
        /* Check whether WGPC = workgroup is changed */
        ilItemPosition = get_item_no(pcpFields,"WGPC",5);
        if( ilItemPosition >= 0 )
        {
            get_real_item( pclInfo[0], pclData[0], ilItemPosition+1 );
            get_real_item( pclInfo[1], pclData[1], ilItemPosition+1 );
            if( strcmp(pclInfo[0], pclInfo[1]) )
            {
                slSqlCursor = 0;
                slSqlFunc = START;
                ilRecordCount = 0;
                sprintf( pclSqlBuf, "SELECT TRIM(URNO) FROM JOBTAB WHERE UDSR = '%s' "
                                    "AND UAFT <> 0", rlJobrec.UDSR );
                dbg( TRACE, "<%s> SelUAFT <%s>", pclFunc, pclSqlBuf );
                while((ilRc = sql_if(slSqlFunc,&slSqlCursor,pclSqlBuf,rlJobrec.URNO)) == DB_SUCCESS)
                {
                    slSqlFunc = NEXT;

                    if( ReadDB( &rlJobrec, &rlStfrec, &rlEqurec, NOT_TURNAROUND ) == RC_SUCCESS )
                    {
                        strcpy( rlJobrec.WGPC, pclInfo[0] );
                        if( strlen(rlJobrec.UAFT) > 0 )
                        {
                            ilRecordCount++;
                            UpdateJob( rlJobrec, rlStfrec, rlEqurec, TYPE_UPDATE, TYPE_STAFF );
                        }
                        if( strlen(rlJobrec.RKEY) > 0 )
                        {
                            if( ReadDB( &rlJobrec, &rlStfrec, &rlEqurec, IS_TURNAROUND ) == RC_SUCCESS )
                            {
                                strcpy( rlJobrec.WGPC, pclInfo[0] );
                                if( strlen(rlJobrec.UAFT) > 0 )
                                {
                                    ilRecordCount++;
                                    UpdateJob( rlJobrec, rlStfrec, rlEqurec, TYPE_UPDATE, TYPE_STAFF );
                                }
                            }
                        }   
                    }
                    memset( rlJobrec.URNO, 0, URNOLEN );
                }
                close_my_cursor(&slSqlCursor);
                dbg (TRACE,"<%s> No. of UAFT updated with WGPC = %d", pclFunc, ilRecordCount );
            }
        }
        else
            dbg( TRACE, "<%s> No record found to upd. Return", pclFunc );
        dbg(TRACE,"<%s> ---------- END ---------",pclFunc);
        return;
    }

    /* Changing of job assignment to another staff */
    ilItemPosition = get_item_no(pcpFields,"USTF",5);
    if( ilItemPosition >= 0 )
        get_real_item( pclInfo[0], pclData[0], ilItemPosition+1 );
    if( atoi(pclInfo[0]) > 0 )
    {
        get_real_item( pclInfo[0], pclData[0], ilItemPosition+1 );
        get_real_item( pclInfo[1], pclData[1], ilItemPosition+1 );
        if( strcmp( pclInfo[0], pclInfo[1] ) )
        {
            dbg( TRACE, "<%s> Staff change. Old Staff PENO <%s> New URNO <%s>", 
                              pclFunc, rlStfrec.PENO, pclInfo[0] );
            DeleteJob( rlJobrec, rlStfrec, rlEqurec, TYPE_STAFF );
            strcpy( rlStfrec.URNO, pclInfo[0] );
            ilItemPosition = get_item_no(pcpFields,"FCCO",5);
            if( ilItemPosition >= 0 )
                get_real_item( rlJobrec.FCCO, pclData[0], ilItemPosition+1 );
            ilItemPosition = get_item_no(pcpFields,"UDSR",5); /* New UDSR */
            if( ilItemPosition >= 0 )
                get_real_item( rlJobrec.UDSR, pclData[0], ilItemPosition+1 );
            memset( rlJobrec.WGPC, 0, sizeof(rlJobrec.WGPC) );
            UpdateJob( rlJobrec, rlStfrec, rlEqurec, TYPE_INSERT, TYPE_STAFF );

            if( strlen(rlJobrec.RKEY) > 0 )
            {
                memcpy( &rlJobrec_o, &rlJobrec, sizeof(JOBREC) );
                if( ReadDB( &rlJobrec, &rlStfrec, &rlEqurec, IS_TURNAROUND ) == RC_SUCCESS )
                {
                    DeleteJob( rlJobrec, rlStfrec, rlEqurec, TYPE_STAFF );
                    strcpy( rlStfrec.URNO, pclInfo[0] );
                    strcpy( rlJobrec.FCCO, rlJobrec_o.FCCO );
                    strcpy( rlJobrec.UDSR, rlJobrec_o.UDSR );
                    memset( rlJobrec.WGPC, 0, sizeof(rlJobrec.WGPC) );
                    UpdateJob( rlJobrec, rlStfrec, rlEqurec, TYPE_INSERT, TYPE_STAFF );
                }
            }
            dbg(TRACE,"<%s> ---------- END ---------",pclFunc);
            return;
        }
    }

    ilItemPosition = get_item_no(pcpFields,"FCCO",5);
    if( ilItemPosition >= 0 )
    {
        get_real_item( pclInfo[0], pclData[0], ilItemPosition+1 );
        strcpy( rlJobrec.FCCO, pclInfo[0] );
        UpdateJob( rlJobrec, rlStfrec, rlEqurec, TYPE_UPDATE, TYPE_STAFF );
        if( strlen(rlJobrec.RKEY) > 0 )
        {
            if( ReadDB( &rlJobrec, &rlStfrec, &rlEqurec, IS_TURNAROUND ) == RC_SUCCESS )
            {
                strcpy( rlJobrec.FCCO, pclInfo[0] );
                UpdateJob( rlJobrec, rlStfrec, rlEqurec, TYPE_UPDATE, TYPE_STAFF );
            }
        }
    } 

    dbg(TRACE,"<%s> ---------- END ---------",pclFunc);
                                    
} /* HandleJobOnline Data */

int UpdateBay(char *pcpFields, char *pcpData, int ipAction)
{
    BAYREC rlBayrec;
    BAYREC rlBayrec_db;
    int ilRc;
    int ili;
    int ilFound;
    int ilItemPosition;
    short slSqlCursor;
    short slSqlFunc;
    char pclStart[30];
    char pclEnd[30];
    char pclData[DBQRLEN];
    char pclSqlBuf[DBQRLEN];
    char pclSqlData[DBQRLEN];
    char pclSqlFields[DBQRLEN];
    char pclTw[64];
    char pclSqlSelection[100];
    char pclTmpStr[300] = "\0";
    char *pclFunc = "UpdateBay";

    dbg( TRACE, "<%s> Fields <%s>", pclFunc, pcpFields );
    dbg( TRACE, "<%s> Data <%s>", pclFunc, pcpData );
    dbg( TRACE, "<%s> ipAction <%d>", pclFunc, ipAction );

    memset( &rlBayrec, 0, sizeof(BAYREC) );
    memset( &rlBayrec_db, 0, sizeof(BAYREC) );

    if( ipAction == TYPE_DELETE )
    {   
        ilItemPosition = get_item_no(pcpFields,"URNO",5);
        if( ilItemPosition < 0 ) /* Item not found */
            return;
        get_real_item(rlBayrec.UEQA, pcpData, ilItemPosition+1); strip_special_char(rlBayrec.UEQA);
        dbg( TRACE, "<%s> DelEQA UEQA = %s", pclFunc, rlBayrec.UEQA );

        ilItemPosition = get_item_no(pcpFields,"NAME",5);
        if( ilItemPosition < 0 ) /* Item not found */
            return;
        get_real_item(rlBayrec.NAME, pcpData, ilItemPosition+1); strip_special_char(rlBayrec.NAME);
        dbg( TRACE, "<%s> DelEQA NAME = %s", pclFunc, rlBayrec.NAME );

        ilItemPosition = get_item_no(pcpFields,"UEQU",5);
        if( ilItemPosition < 0 ) /* Item not found */
            return;
        get_real_item(rlBayrec.UEQU, pcpData, ilItemPosition+1); strip_special_char(rlBayrec.UEQU);
        dbg( TRACE, "<%s> DelEQA UEQU = %s", pclFunc, rlBayrec.UEQU );

        sprintf( pclSqlBuf, "SELECT URNO FROM EQATAB WHERE UEQU = %s AND NAME = '%s' AND URNO <> %s",
                            rlBayrec.UEQU, rlBayrec.NAME, rlBayrec.UEQA );
        slSqlCursor = 0;
        slSqlFunc = START;
        while( sql_if(slSqlFunc,&slSqlCursor,pclSqlBuf,pclSqlData) == DB_SUCCESS )
        {
            slSqlFunc = NEXT;
            sprintf( pclSqlSelection, "WHERE URNO = %s", pclSqlData );
            dbg( TRACE, "<%s> pclSqlSelection = %s", pclFunc, pclSqlSelection );
            ilRc = SendCedaEvent(igSqlhdlID, 0, mod_name, "EXCO", mod_name, pclTw,"DRT", "EQATAB",
                                 pclSqlSelection,"", "", "", igSendPrio,NETOUT_NO_ACK) ;
            dbg( TRACE, "<%s> DelEQA ilRc = %d", pclFunc, ilRc );
        }
        close_my_cursor(&slSqlCursor);
        return;
    }

    strcpy( pclData, pcpData );

    ilFound = GetElementValue(pclTmpStr, pclData, "<TIMESTAMP>", "</TIMESTAMP>");
    if( ilFound == FALSE || (ilFound == TRUE && strlen(pclTmpStr) <= 0) )
    {
        dbg( TRACE, "<%s> Invalid TIMESTAMP", pclFunc );
        return;
    }
    ilFound = GetElementValue(pclTmpStr, pclData, "<ACTIONTYPE>", "</ACTIONTYPE>");
    if( ilFound == FALSE || (ilFound == TRUE && pclTmpStr[0] != 'U') )
    {
        dbg( TRACE, "<%s> Invalid ACTIONTYPE", pclFunc );
        return;
    }

    ilFound = GetElementValue(rlBayrec.GCDE, pclData, "<GCDE>", "</GCDE>"); 
    if( ilFound == FALSE || (ilFound == TRUE && strlen(rlBayrec.GCDE) <= 0) )
    {
        dbg( TRACE, "<%s> Invalid GCDE", pclFunc );
        return;
    }
    strip_special_char(rlBayrec.GCDE);

    ilFound = GetElementValue(rlBayrec.EQPS, pclData, "<EQPS>", "</EQPS>"); 
    if( ilFound == FALSE || (ilFound == TRUE && strlen(rlBayrec.EQPS) <= 0) )
    {
        dbg( TRACE, "<%s> No position to update", pclFunc );
        return;
    }
    strip_special_char(rlBayrec.EQPS);

    sprintf( pclSqlBuf, "SELECT URNO FROM EQUTAB WHERE GCDE = '%s'", rlBayrec.GCDE );
    dbg( TRACE, "<%s> SelEQU SqlBuf <%s>", pclFunc, pclSqlBuf );
    ilRc = getOrPutDBData( pclSqlBuf, rlBayrec.UEQU, START );
    if( ilRc != RC_SUCCESS )
    {
        dbg( TRACE, "<%s> Equip not available in EQUTAB for upd of pos. GCDE<s>", 
                                                         pclFunc, rlBayrec.GCDE );
        return;
    }
    dbg( TRACE, "<%s> UEQU <%s>", pclFunc, rlBayrec.UEQU );

    ilFound = GetElementValue(rlBayrec.LSTU, pclData, "<LSTU>", "</LSTU>"); strip_special_char(rlBayrec.LSTU);
   
    /** Commented by Mei, 26-AUG-2008 This is affecting SYSMON in reloading of shared memory **/
    /*sprintf( pclSqlFields, "%s", "EQPS,LSTU" ); 
    sprintf( pclSqlData, "%s,%14.14s", rlBayrec.EQPS, rlBayrec.LSTU );
    sprintf( pclSqlSelection, "WHERE URNO = %s", rlBayrec.UEQU );

    dbg( TRACE, "<%s> UpdEQU SqlFields <%s>", pclFunc, pclSqlFields );
    dbg( TRACE, "<%s> UpdEQU SqlData <%s>", pclFunc, pclSqlData );
    dbg( TRACE, "<%s> UpdEQU SqlSelection <%s>", pclFunc, pclSqlSelection );

    ilRc = SendCedaEvent(igSqlhdlID, 0, mod_name, "EXCO", mod_name, pclTw,"URT", "EQUTAB",
                         pclSqlSelection,pclSqlFields, pclSqlData, "",
                         igSendPrio,NETOUT_NO_ACK) ;
    dbg( TRACE, "<%s> UpdEQU ilRc = %d", pclFunc, ilRc );
    if( ilRc != RC_SUCCESS )
    {
        dbg( TRACE, "<%s> Error in upd Equip pos GCDE<%s>", pclFunc, rlBayrec.GCDE );
        return;
    }*/


    /* To facilitate BCHDL to generate statistic report */
    sprintf( pclTw, "%s,%s,%s", cgHopo, cgTabEnd, mod_name );

    memset( pclSqlSelection, 0, sizeof(pclSqlSelection) );

    for( ili = 0; ili < NUM_ATTRIBUTES; ili++ )
    {
        sprintf( pclStart, "<%s>", pcgEquXmlFields[ili] );
        sprintf( pclEnd, "</%s>", pcgEquXmlFields[ili] );
        ilFound = GetElementValue(rlBayrec.VALU, pclData, pclStart, pclEnd ); strip_special_char(rlBayrec.VALU);
        if( ilFound == FALSE )
            continue;
        if( ili == 1 ) /* LSTU, hardcoded, to format to what support team request */
        {
            /* DD.MM.YY HH:MI */
            /* Original is YYYYMMDDHHMISSsss */
            UtcToLocalTimeFixTZ(rlBayrec.VALU);
            sprintf( pclTmpStr, "%2.2s.%2.2s.%2.2s %2.2s:%2.2s", &rlBayrec.VALU[6], &rlBayrec.VALU[4], &rlBayrec.VALU[2],
                                                                 &rlBayrec.VALU[8], &rlBayrec.VALU[10] );
            strcpy( rlBayrec.VALU, pclTmpStr );
        }
        dbg( DEBUG, "<%s> Field <%s> Value <%s>", pclFunc, pcgEquXmlFields[ili], rlBayrec.VALU );

        /* Opsspm update on Equip Attributes contain bug, patch new opsspm before implementing iflhdl */
       
        sprintf( pclSqlBuf, "SELECT URNO, TRIM(VALU) FROM EQATAB WHERE NAME = '%s' AND UEQU = %s",
                                              pcgEquAttFields[ili], rlBayrec.UEQU );
        dbg( TRACE, "<%s> SelEQA SqlBuf <%s>", pclFunc, pclSqlBuf );
        ilRc = getOrPutDBData( pclSqlBuf, pclSqlData, START );

        if( ilRc == DB_SUCCESS )
        {
            dbg( TRACE, "<%s> Record exist in EQATAB Attribute <%s>", pclFunc, pcgEquAttFields[ili] );
            get_fld(pclSqlData,FIELD_1,STR,URNOLEN,rlBayrec_db.UEQA);
            get_fld(pclSqlData,FIELD_2,STR,VALULEN,rlBayrec_db.VALU);
    
            dbg( TRACE, "<%s> SelEQA UEQA <%s>", pclFunc, rlBayrec_db.UEQA );
            dbg( TRACE, "<%s> SelEQA VALU <%s>", pclFunc, rlBayrec_db.VALU );

            /* No change in VALU */
            if( !strcmp( rlBayrec_db.VALU, rlBayrec.VALU ) )
            {
                dbg( TRACE, "<%s> Ignore, value is the same", pclFunc );
                continue;
            }

            sprintf( pclSqlBuf, "UPDATE EQATAB SET VALU = '%s', LSTU = '%14.14s', USEU = '%s' WHERE URNO = %s", 
				                rlBayrec.VALU, pcgTimeStr, mod_name, rlBayrec_db.UEQA );
            dbg( TRACE, "<%s> UpdEQA SqlBuf <%s>", pclFunc, pclSqlBuf );

            ilRc = getOrPutDBData( pclSqlBuf, pclSqlData, START );
            if( ilRc != RC_SUCCESS )
            {
                dbg( TRACE, "<%s> Fail to update Equip Attributes - VALU in EQATAB", pclFunc );
                return;
            }

			sprintf( pclSqlFields, "%s", "VALU,LSTU,USEU" );
			sprintf( pclSqlData, "%s,%14.14s,%s", rlBayrec.VALU, pcgTimeStr, mod_name );
            /*  Send broadcast for update of EQA record in EQATAB */
            ilRc = tools_send_info_flag( igBchdlID, mod_id, "", "", "", "", "", mod_name, pclTw, 
                                         "URT", "EQATAB", rlBayrec_db.UEQA, pclSqlFields, pclSqlData, 0);
            continue;
        }

        /* Insertion */
        ilRc = GetNextValues( rlBayrec.UEQA, 1 );
        sprintf( pclSqlFields, "%s", "NAME,VALU,UEQU,CDAT,URNO,HOPO,USEC,USEU,LSTU" ); 
        sprintf( pclSqlData, "%s,%s,%s,%14.14s,%s,%s,%s,%s,%14.14s", 
                              pcgEquAttFields[ili], rlBayrec.VALU, rlBayrec.UEQU, 
                              pcgTimeStr, rlBayrec.UEQA, cgHopo, mod_name, mod_name, pcgTimeStr );

        dbg( TRACE, "<%s> InsEQA SqlFields <%s>", pclFunc, pclSqlFields );
        dbg( TRACE, "<%s> InsEQA SqlData <%s>", pclFunc, pclSqlData );

        sprintf( pclSqlBuf, "INSERT INTO EQATAB (%s) VALUES "
                            "('%s','%s',%s,'%14.14s',%s,'%s','%s','%s','%14.14s')", 
                            pclSqlFields, pcgEquAttFields[ili], rlBayrec.VALU, rlBayrec.UEQU, 
                            pcgTimeStr, rlBayrec.UEQA, cgHopo, mod_name, mod_name, pcgTimeStr );

        dbg( TRACE, "<%s> InsEQA SqlBuf <%s>", pclFunc, pclSqlBuf );

        ilRc = getOrPutDBData( pclSqlBuf, pclSqlData, START );
        if( ilRc != RC_SUCCESS )
        {
            dbg( TRACE, "<%s> Fail to insert Equip Attributes in EQATAB", pclFunc );
            return;
        }

        /*  Send broadcast for update of EQA record in EQATAB */
        ilRc = tools_send_info_flag( igBchdlID, mod_id, "", "", "", "", "", mod_name, pclTw, 
                                     "IRT", "EQATAB", rlBayrec.UEQA, pclSqlFields, pclSqlData, 0);
    }
    return ilRc;
}

static int GetElementValue(char *pcpDest, char *pcpOrigData, char *pcpBegin, char *pcpEnd)
{
    long llSize = 0;
    char pclTmpStr[DBQRLEN] = "\0";
    char pclEnd[100] = "\0";
    char *pclTemp = NULL;
    char *pclFunc = "GetElementValue";

    pclTemp = CedaGetKeyItem(pcpDest, &llSize, pcpOrigData, pcpBegin, pcpEnd, TRUE);
    if(pclTemp != NULL)
        return TRUE;
    else
    {
        strncpy( pclTmpStr, &pcpEnd[2], strlen(pcpEnd)-3 );    /* </INFOBJ_FLIGHT> */
        sprintf( pclEnd, "<%s/>", pclTmpStr );                 /* <INFOBJ_FLIGHT/> */
        /*dbg( DEBUG, "<%s> pclEnd <%s>", pclFunc, pclEnd );*/
        strcpy( pclTmpStr, pcpOrigData );
        pclTemp = (char *)strstr(pclTmpStr, pclEnd);
        if(pclTemp != NULL)
        {
            strcpy( pclTemp, " " );
            return TRUE;
        }
        else
            return FALSE;
    }
}


static int CheckQueue(int ipModId, ITEM **prpItem)
{
    int     ilRc       = RC_SUCCESS;                      /* Return code */
    int     ilItemSize = 0;
    EVENT *prlEvent    = NULL;
    static  ITEM  *prlItem      = NULL;
    
    ilItemSize = I_SIZE;
    
    ilRc = que(QUE_GETBIGNW,ipModId,ipModId,PRIORITY_3,ilItemSize,(char *) &prlItem);
    if(ilRc == RC_SUCCESS)
    {
        dbg(TRACE,"DebugPrintItem with prlItem follows");
        DebugPrintItem(TRACE,prlItem);
        *prpItem = prlItem;
        dbg(TRACE,"DebugPrintItem with prpItem follows");
        DebugPrintItem(TRACE,*prpItem);
        prlEvent = (EVENT*) ((prlItem)->text);

        switch( prlEvent->command )
        {
            case    HSB_STANDBY :
                dbg(TRACE,"CheckQueue: HSB_STANDBY");
                HandleQueues();
                ctrl_sta = prlEvent->command;
                break;
    
            case    HSB_COMING_UP   :
                dbg(TRACE,"CheckQueue: HSB_COMING_UP");
                ctrl_sta = prlEvent->command;
                break;
    
            case    HSB_ACTIVE  :
                dbg(TRACE,"CheckQueue: HSB_ACTIVE");
                ctrl_sta = prlEvent->command;
                break;
    
            case    HSB_ACT_TO_SBY  :
                dbg(TRACE,"CheckQueue: HSB_ACT_TO_SBY");
                HandleQueues();
                ctrl_sta = prlEvent->command;
                break;
    
            case    HSB_DOWN    :
                dbg(TRACE,"CheckQueue: HSB_DOWN");
                ctrl_sta = prlEvent->command;
                /* Acknowledge the item 
                ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
                if( ilRc != RC_SUCCESS )
                {
                    HandleQueErr(ilRc);
                }  */
                Terminate(30);
                break;
        
        case    HSB_STANDALONE  :
                dbg(TRACE,"CheckQueue: HSB_STANDALONE");
                ctrl_sta = prlEvent->command;
                break;
    
            case    SHUTDOWN    :
                dbg(TRACE,"CheckQueue: SHUTDOWN");
                /* Acknowledge the item */
                ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
                if( ilRc != RC_SUCCESS )
                {
                    /* handle que_ack error */
                    HandleQueErr(ilRc);
                } /* fi */
                Terminate(30);
                break;
    
            case    RESET       :
                ilRc = Reset();
                break;
    
            case    EVENT_DATA  :
                /* DebugPrintItem(DEBUG,prgItem); */
                /* DebugPrintEvent(DEBUG,prlEvent); */
                break;
    
            case    TRACE_ON :
                dbg_handle_debug(prlEvent->command);
                break;
    
            case    TRACE_OFF :
                dbg_handle_debug(prlEvent->command);
                break;


            default         :
                dbg(TRACE,"CheckQueue: unknown event");
                DebugPrintItem(TRACE,prlItem);
                DebugPrintEvent(TRACE,prlEvent);
                break;
    
        } /* end switch */
    
        /* Acknowledge the item */
        ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
        if( ilRc != RC_SUCCESS )
        {
            /* handle que_ack error */
            HandleQueErr(ilRc);
        } /* fi */
    }else{
        if(ilRc != QUE_E_NOMSG)
        {
            dbg(TRACE,"CheckQueue: que(QUE_GETBIG,%d,%d,...) failed <%d>",ipModId,ipModId,ilRc);
            HandleQueErr(ilRc);
        }/* end of if */
    }/* end of if */
       
    return ilRc;
}/* end of CheckQueue */

static int getOrPutDBData(char *pcpSelection, char *pcpData, unsigned int ipMode)
{
  int ilRc;
  short slFkt = 0;
  short slCursor = 0;
  char errBuff[128];

  slFkt = ipMode;
  slCursor = 0;
  ilRc = sql_if (slFkt, &slCursor, pcpSelection, pcpData);
  close_my_cursor (&slCursor);
  if (ilRc == DB_SUCCESS) 
    return ilRc;
  
  else {
    if (ilRc != NOTFOUND) {
      get_ora_err(ilRc, &errBuff[0]);
      dbg (TRACE, "<getOrPutDBData> Error getting Oracle data error <%s>", &errBuff[0]);
      return DB_ERROR;
    }
    else  return ilRc;
  }
}

/* Get current local time and format to YYYYMMDDHHMISS */
/********** move to itrek_fn.inc ************************
time_t FormatTime( time_t ipTime, char *pcpTimeStr, char *pcpTimeType )
{
    struct tm rlTmptr;
    time_t ilNow;
    char pclTimeStr[DATELEN];

    ilNow = ipTime;
    if( !strncmp(pcpTimeType, "UTC", 3 ) )
        gmtime_r( (time_t *)&ilNow, &rlTmptr );
    else
        localtime_r( (time_t *)&ilNow, &rlTmptr );
    sprintf( pcpTimeStr, "%4.4d%2.2d%2.2d%2.2d%2.2d%2.2d", rlTmptr.tm_year+1900, rlTmptr.tm_mon+1,
                                                           rlTmptr.tm_mday, rlTmptr.tm_hour, 
                                                           rlTmptr.tm_min,rlTmptr.tm_sec );
    return( ilNow );
}
***/

int PackFlightXml( AFTREC rpAftrec, int ipPackType )
{
    int ilLen;
    int ilNum;
    int ili;
    char pclTmpStr[300];
    char pclActType[2];
    char pclVias[8][ONE_FOUR];
    char pclPstn[TWO_FOUR];
    char *pclFunc = "PackFlightXml";


    /* ACTIONTYPE */
    strcpy( pclActType, "D" );
    if( ipPackType != TYPE_DELETE )
        strcpy( pclActType, "U" );
    
    /* BULK */
    strcpy( pclTmpStr, pcgXmlHeader );
    if( ipPackType == TYPE_BULK )
        strcat( pclTmpStr, "\n<BULK>" );

    /* VIAL */
    ilLen = strlen(rpAftrec.VIAL);
    ilNum = 0;
    memset( pclVias, 0, sizeof(pclVias) );
    for( ili = 0; ili < ilLen; ili+=ONE_VIA_SIZE )
    {
        sprintf( pclVias[ilNum++], "%3.3s", &rpAftrec.VIAL[ili+1] );
        if( ilNum >= 8 )
            break;
    }

    /* PSTN */
    strcpy( pclPstn, rpAftrec.PSTA );
    if( strlen(rpAftrec.PSTD) > 0 )
        strcpy( pclPstn, rpAftrec.PSTD );
    if( strlen(rpAftrec.STOA) > 0 )
        strcat( rpAftrec.STOA, "000" );
    if( strlen(rpAftrec.STOD) > 0 )
        strcat( rpAftrec.STOD, "000" );
    if( strlen(rpAftrec.ETAI) > 0 )
        strcat( rpAftrec.ETAI, "000" );
    if( strlen(rpAftrec.ETDI) > 0 )
        strcat( rpAftrec.ETDI, "000" );
    if( strlen(rpAftrec.LAND) > 0 )
        strcat( rpAftrec.LAND, "000" );
    if( strlen(rpAftrec.ONBL) > 0 )
        strcat( rpAftrec.ONBL, "000" );
    if( strlen(rpAftrec.AIRB) > 0 )
        strcat( rpAftrec.AIRB, "000" );
    if( strlen(rpAftrec.OFBL) > 0 )
        strcat( rpAftrec.OFBL, "000" );

    memset( pcgXmlStr, 0, sizeof(pcgXmlStr) );
    sprintf( pcgXmlStr, "%s\n"
                        "<MSG>\n"
                        "<MSGSTREAM_OUT>\n"
                        "<INFOBJ_GENERIC_FLIGHT>\n"
                        "<TIMESTAMP>%17.17s</TIMESTAMP>\n"
                        "<ACTIONTYPE>%c</ACTIONTYPE>\n"
                        "<UAFT>%s</UAFT>\n"
                        "</INFOBJ_GENERIC_FLIGHT>\n"
                        "<MSGOBJECTS>\n"
                        "<INFOBJ_FLIGHT>\n"
                        "<FLNO>%s</FLNO>\n"
                        "<ALC2>%s</ALC2>\n"
                        "<ALC3>%s</ALC3>\n"
                        "<FLTN>%s</FLTN>\n"
                        "<FLNS>%s</FLNS>\n"
                        "<STOA>%s</STOA>\n"
                        "<STOD>%s</STOD>\n"
                        "<ADID>%s</ADID>\n"
                        "<RKEY>%s</RKEY>\n"
                        "<REGN>%s</REGN>\n"
                        "<CSGN>%s</CSGN>\n"
                        "<ETAI>%s</ETAI>\n"
                        "<ETDI>%s</ETDI>\n"
                        "<DES3>%s</DES3>\n"
                        "<ORG3>%s</ORG3>\n"
                        "<FTYP>%s</FTYP>\n"
                        "<TTYP>%s</TTYP>\n"
                        "<ACT3>%s</ACT3>\n"
                        "<ACT5>%s</ACT5>\n"
                        "<LAND>%s</LAND>\n"
                        "<ONBL>%s</ONBL>\n"
                        "<AIRB>%s</AIRB>\n"
                        "<OFBL>%s</OFBL>\n"
                        "<VIA1>%s</VIA1>\n"
                        "<VIA2>%s</VIA2>\n"
                        "<VIA3>%s</VIA3>\n"
                        "<VIA4>%s</VIA4>\n"
                        "<VIA5>%s</VIA5>\n"
                        "<VIA6>%s</VIA6>\n"
                        "<VIA7>%s</VIA7>\n"
                        "<VIA8>%s</VIA8>\n"
                        "<PSTN>%s</PSTN>\n"
                        "</INFOBJ_FLIGHT>\n"
                        "</MSGOBJECTS>\n"
                        "</MSGSTREAM_OUT>\n"
                        "</MSG>\n", pclTmpStr, pcgTimeStr, pclActType[0], rpAftrec.URNO, 
                                    rpAftrec.FLNO, rpAftrec.ALC2, rpAftrec.ALC3,
                                    rpAftrec.FLTN, rpAftrec.FLNS, rpAftrec.STOA,
                                    rpAftrec.STOD, rpAftrec.ADID, rpAftrec.RKEY,
                                    rpAftrec.REGN, rpAftrec.CSGN, rpAftrec.ETAI,
                                    rpAftrec.ETDI, rpAftrec.DES3, rpAftrec.ORG3,
                                    rpAftrec.FTYP, rpAftrec.TTYP, rpAftrec.ACT3,
                                    rpAftrec.ACT5, rpAftrec.LAND, rpAftrec.ONBL, 
                                    rpAftrec.AIRB, rpAftrec.OFBL, 
                                    pclVias[0], pclVias[1], pclVias[2], pclVias[3],
                                    pclVias[4], pclVias[5], pclVias[6], pclVias[7],
                                    pclPstn );
    if( ipPackType == TYPE_BULK )
        strcat( pcgXmlStr, "</BULK>\n" );
}

int InsertDB( JOBREC rpJobrec, STFREC rpStfrec, EQUREC rpEqurec )
{
    int ilRc;
    char pclStatus[ONE_FOUR];
    char pclSqlBuf[DBQRLEN];
    char pclSqlData[DBQRLEN];
    char *pclFunc = "InsertDB";
    
    dbg( TRACE, "<%s> Job URNO <%s>", pclFunc, rpJobrec.URNO );

    strcpy( pclStatus, ONLINE_INSERT );
    if( bgSyncData == TRUE )
        strcpy( pclStatus, SYNC_INSERT );
    
    sprintf( pclSqlBuf, "INSERT INTO IFLJOB (UJOB,UAFT,WGPC,FCCO,PENO,GCDE,STAT,LSTU,RKEY) VALUES "
                        "(%s,%s,'%s','%s','%s','%s','%s', '%8.8s', '%s')", 
                        rpJobrec.URNO, rpJobrec.UAFT, rpJobrec.WGPC, rpJobrec.FCCO, 
                        rpStfrec.PENO, rpEqurec.GCDE, pclStatus, &pcgTimeStr[6], rpJobrec.RKEY );
                   
    dbg( TRACE, "<%s> SqlBuf <%s>", pclFunc, pclSqlBuf );
    
    ilRc = getOrPutDBData( pclSqlBuf, pclSqlData, START );
    return ilRc;
} /* InsertDB */

int UpdateDB( JOBREC rpJobrec, STFREC rpStfrec, EQUREC rpEqurec  )
{
    int ilRc;
    char pclStatus[ONE_FOUR];
    char pclSqlBuf[DBQRLEN];
    char pclSqlData[DBQRLEN];
    char *pclFunc = "UpdateDB";
    
    dbg( TRACE, "<%s> UJOB <%s> UAFT <%s>", pclFunc, rpJobrec.URNO, rpJobrec.UAFT );
    
    strcpy( pclStatus, ONLINE_UPDATE );
    if( bgSyncData == TRUE )
        strcpy( pclStatus, SYNC_UPDATE );

    sprintf( pclSqlBuf, "UPDATE IFLJOB SET UAFT = '%s',WGPC = '%s',"
                        "FCCO = '%s',PENO = '%s',GCDE = '%s',STAT = '%s',LSTU = '%8.8s' "
                        "WHERE UJOB = '%s' AND UAFT = '%s' AND STAT NOT IN ( '%s', '%s' )",
                        rpJobrec.UAFT, rpJobrec.WGPC, rpJobrec.FCCO, rpStfrec.PENO, 
                        rpEqurec.GCDE, pclStatus, &pcgTimeStr[6], rpJobrec.URNO, 
                        rpJobrec.UAFT, ONLINE_DELETE, SYNC_DELETE );
                   
    dbg( TRACE, "<%s> SqlBuf <%s>", pclFunc, pclSqlBuf );
    
    ilRc = getOrPutDBData( pclSqlBuf, pclSqlData, START );
    return ilRc;
} /* UpdateDB */

int ReadDB( JOBREC *rpJobrec, STFREC *rpStfrec, EQUREC *rpEqurec, char cpJobType )
{
    int ilRc;
    char pclSqlBuf[DBQRLEN];
    char pclSqlData[DBQRLEN];
    char *pclFunc = "ReadDB";
    
    dbg( TRACE, "<%s> Job URNO <%s> JobType <%c>", pclFunc, rpJobrec->URNO, cpJobType );
    
    if( cpJobType == IS_TURNAROUND )
    {
        sprintf( pclSqlBuf, "SELECT TRIM(UJOB), TRIM(UAFT),TRIM(WGPC),TRIM(FCCO),TRIM(PENO),"
                            "TRIM(GCDE),TRIM(RKEY),STAT FROM IFLJOB WHERE "
                            "UJOB = '%s' AND UAFT <> '%s' AND "
                            "RKEY = '%s' AND STAT NOT IN ( '%s', '%s' )", 
                            rpJobrec->URNO, rpJobrec->UAFT, rpJobrec->RKEY, ONLINE_DELETE, SYNC_DELETE );
        dbg( TRACE, "<%s> TurnArnd SqlBuf <%s>", pclFunc, pclSqlBuf );
    
        ilRc = getOrPutDBData( pclSqlBuf, pclSqlData, START );
        if( ilRc == RC_SUCCESS )
        {
            get_fld(pclSqlData,FIELD_1,STR,URNOLEN,rpJobrec->URNO);
            get_fld(pclSqlData,FIELD_2,STR,URNOLEN,rpJobrec->UAFT);
            get_fld(pclSqlData,FIELD_3,STR,TWO_FOUR,rpJobrec->WGPC);
            get_fld(pclSqlData,FIELD_4,STR,THREE_FOUR,rpJobrec->FCCO);
            get_fld(pclSqlData,FIELD_5,STR,NAMELEN,rpStfrec->PENO);
            get_fld(pclSqlData,FIELD_6,STR,TWO_FOUR,rpEqurec->GCDE);
            get_fld(pclSqlData,FIELD_7,STR,URNOLEN,rpJobrec->RKEY);
            get_fld(pclSqlData,FIELD_8,STR,ONE_FOUR,rpJobrec->STAT);
 
            dbg( DEBUG, "<%s> UJOB <%s>", pclFunc, rpJobrec->URNO );
            dbg( DEBUG, "<%s> UAFT <%s>", pclFunc, rpJobrec->UAFT );
            dbg( DEBUG, "<%s> WGPC <%s>", pclFunc, rpJobrec->WGPC );
            dbg( DEBUG, "<%s> FCCO <%s>", pclFunc, rpJobrec->FCCO );
            dbg( DEBUG, "<%s> PENO <%s>", pclFunc, rpStfrec->PENO );
            dbg( DEBUG, "<%s> GCDE <%s>", pclFunc, rpEqurec->GCDE );
            dbg( DEBUG, "<%s> RKEY <%s>", pclFunc, rpJobrec->RKEY );
            dbg( DEBUG, "<%s> STAT <%s>", pclFunc, rpJobrec->STAT );
        }
        return ilRc;
    }

    sprintf( pclSqlBuf, "SELECT TRIM(UAFT),TRIM(WGPC),TRIM(FCCO),TRIM(PENO),TRIM(GCDE),TRIM(RKEY),STAT "
                        "FROM IFLJOB WHERE UJOB = '%s' AND STAT NOT IN ( '%s', '%s' )", 
                        rpJobrec->URNO, ONLINE_DELETE, SYNC_DELETE );
    dbg( TRACE, "<%s> SqlBuf <%s>", pclFunc, pclSqlBuf );
    
    ilRc = getOrPutDBData( pclSqlBuf, pclSqlData, START );
    if( ilRc == RC_SUCCESS )
    {
        get_fld(pclSqlData,FIELD_1,STR,URNOLEN,rpJobrec->UAFT);
        get_fld(pclSqlData,FIELD_2,STR,TWO_FOUR,rpJobrec->WGPC);
        get_fld(pclSqlData,FIELD_3,STR,THREE_FOUR,rpJobrec->FCCO);
        get_fld(pclSqlData,FIELD_4,STR,NAMELEN,rpStfrec->PENO);
        get_fld(pclSqlData,FIELD_5,STR,TWO_FOUR,rpEqurec->GCDE);
        get_fld(pclSqlData,FIELD_6,STR,URNOLEN,rpJobrec->RKEY);
        get_fld(pclSqlData,FIELD_7,STR,URNOLEN,rpJobrec->STAT);
 
        dbg( DEBUG, "<%s> UAFT <%s>", pclFunc, rpJobrec->UAFT );
        dbg( DEBUG, "<%s> WGPC <%s>", pclFunc, rpJobrec->WGPC );
        dbg( DEBUG, "<%s> FCCO <%s>", pclFunc, rpJobrec->FCCO );
        dbg( DEBUG, "<%s> PENO <%s>", pclFunc, rpStfrec->PENO );
        dbg( DEBUG, "<%s> GCDE <%s>", pclFunc, rpEqurec->GCDE );
        dbg( DEBUG, "<%s> RKEY <%s>", pclFunc, rpJobrec->RKEY );
        dbg( DEBUG, "<%s> STAT <%s>", pclFunc, rpJobrec->STAT );
    }
    return ilRc;
} /* ReadDB */

int DeleteDB( char *pcpURNO, char *pcpUAFT )
{
    int ilRc;
    char pclStatus[ONE_FOUR];
    char pclSqlBuf[DBQRLEN];
    char pclSqlData[DBQRLEN];
    char *pclFunc = "DeleteDB";
    
    dbg( TRACE, "<%s> UJOB <%s> UAFT <%s>", pclFunc, pcpURNO, pcpUAFT );

    strcpy( pclStatus, ONLINE_DELETE );
    if( bgSyncData == TRUE )
        strcpy( pclStatus, SYNC_DELETE );
    
    sprintf( pclSqlBuf, "UPDATE IFLJOB SET STAT = '%s', LSTU = '%8.8s' WHERE UJOB = '%s' AND "
                        "UAFT = '%s' AND STAT NOT IN ( '%s', '%s' )", pclStatus,
                        &pcgTimeStr[6], pcpURNO, pcpUAFT, ONLINE_DELETE, SYNC_DELETE );
    dbg( TRACE, "<%s> SqlBuf <%s>", pclFunc, pclSqlBuf );
    
    ilRc = getOrPutDBData( pclSqlBuf, pclSqlData, START );
    return ilRc;
} /* DeleteDB */

int DeleteJob( JOBREC rpJobrec, STFREC rpStfrec, EQUREC rpEqurec, int ipDelType )
{
    int ilRc; 
    char *pclFunc = "DeleteJob";

    dbg( TRACE, "<%s> UJOB <%s>", pclFunc, rpJobrec.URNO );

    DeleteDB( rpJobrec.URNO, rpJobrec.UAFT );
    if( ipDelType == TYPE_STAFF || 
       (ipDelType == TYPE_BOTH && strlen(rpStfrec.PENO) > 0) )
    {
        sprintf( pcgXmlStr, "%s\n" 
                            "<MSG>\n"
                            "<MSGSTREAM_OUT>\n"
                            "<INFOBJ_GENERIC_STAFF>\n"
                            "<TIMESTAMP>%s</TIMESTAMP>\n"
                            "<ACTIONTYPE>D</ACTIONTYPE>\n"
                            "<PENO>%s</PENO>\n"
                            "</INFOBJ_GENERIC_STAFF>\n"
                            "<MSGOBJECTS>\n"
                            "<INFOBJ_STAFF>\n"
                            "<UAFT>%s</UAFT>\n"
                            "</INFOBJ_STAFF>\n"
                            "</MSGOBJECTS>\n"
                            "</MSGSTREAM_OUT>\n"
                            "</MSG>\n", pcgXmlHeader, pcgTimeStr, 
                                        rpStfrec.PENO, rpJobrec.UAFT );
        dbg( TRACE, "<%s> DeleteStaff (%s)", pclFunc, pcgXmlStr );
        ilRc = SendCedaEvent(igRecipentID[STAFF],0,mod_name,"EXCO",mod_name,"","AGN","","","",
                             pcgXmlStr,"",igSendPrio,RC_SUCCESS);
        dbg(TRACE,"<%s> SendCedaEvent-DeleteStaff ilRc = %d",pclFunc, ilRc );
    }
    if( ipDelType == TYPE_EQUIP ||
       (ipDelType == TYPE_BOTH && strlen(rpEqurec.GCDE) > 0) )
    {
        sprintf( pcgXmlStr, "%s\n" 
                            "<MSG>\n"
                            "<MSGSTREAM_OUT>\n"
                            "<INFOBJ_GENERIC_GSE>\n"
                            "<TIMESTAMP>%s</TIMESTAMP>\n"
                            "<ACTIONTYPE>D</ACTIONTYPE>\n"
                            "<GCDE>%s</GCDE>\n"
                            "</INFOBJ_GENERIC_GSE>\n"
                            "<MSGOBJECTS>\n"
                            "<INFOBJ_GSE>\n"
                            "<UAFT>%s</UAFT>\n"
                            "</INFOBJ_GSE>\n"
                            "</MSGOBJECTS>\n"
                            "</MSGSTREAM_OUT>\n"
                            "</MSG>\n", pcgXmlHeader, pcgTimeStr, 
                                        rpEqurec.GCDE, rpJobrec.UAFT );
        dbg( TRACE, "<%s> DeleteEquip (%s)", pclFunc, pcgXmlStr );
        ilRc = SendCedaEvent(igRecipentID[EQUIP],0,mod_name,"EXCO",mod_name,"","AGN","","","",
                             pcgXmlStr,"",igSendPrio,RC_SUCCESS);
        dbg(TRACE,"<%s> SendCedaEvent-DeleteEquip ilRc = %d",pclFunc, ilRc );
    }
}

int UpdateJob( JOBREC rpJobrec, STFREC rpStfrec, EQUREC rpEqurec, int ipActType, int ipInsType )
{
    int ilRc; 
    STFREC rlStfrec;
    EQUREC rlEqurec;
    BOOL blStaffInfo = FALSE;
    BOOL blEquipInfo = FALSE;
    char pclSqlBuf[DBQRLEN] = "\0";     
    char pclSqlData[DBQRLEN] = "\0";     
    char pclTmpStr[1000] = "\0";
    char *pclFunc = "UpdateJob";

    if( atoi(rpStfrec.URNO) > 0 )
        blStaffInfo = TRUE;
    if( atoi(rpEqurec.URNO) > 0 )
        blEquipInfo = TRUE;

    memset( &rlStfrec, 0, sizeof(STFREC) );
    memset( &rlEqurec, 0, sizeof(EQUREC) );

    dbg( DEBUG, "<%s> ActType = %d InsType = %d", pclFunc, ipActType, ipInsType );

    if( ipInsType == TYPE_STAFF || (ipInsType == TYPE_BOTH && blStaffInfo == TRUE) )
    {
        if( blStaffInfo == TRUE )
        {
            memcpy( &rlStfrec, &rpStfrec, sizeof(STFREC) );
            GetStaff( &rlStfrec );
            sprintf( pclTmpStr, "<FINM>%s</FINM>\n"
                                "<LANM>%s</LANM>\n"
                                "<SHNM>%s</SHNM>\n",
                                rlStfrec.FINM, rlStfrec.LANM, rlStfrec.SHNM );
        }
        if( strlen(rpJobrec.WGPC) <= 0 )
            GetWGPC( rpJobrec.UDSR, rpJobrec.WGPC );
        if( strlen(rlStfrec.PENO) <= 0 )
            strcpy( rlStfrec.PENO, rpStfrec.PENO );

        sprintf( pcgXmlStr, "%s\n" 
                            "<MSG>\n"
                            "<MSGSTREAM_OUT>\n"
                            "<INFOBJ_GENERIC_STAFF>\n"
                            "<TIMESTAMP>%s</TIMESTAMP>\n"
                            "<ACTIONTYPE>U</ACTIONTYPE>\n"
                            "<PENO>%s</PENO>\n"
                            "</INFOBJ_GENERIC_STAFF>\n"
                            "<MSGOBJECTS>\n"
                            "<INFOBJ_STAFF>\n"
                            "%s"
                            "<UAFT>%s</UAFT>\n"
                            "<WGPC>%s</WGPC>\n"
                            "<FCCO>%s</FCCO>\n"
                            "</INFOBJ_STAFF>\n"
                            "</MSGOBJECTS>\n"
                            "</MSGSTREAM_OUT>\n"
                            "</MSG>\n", pcgXmlHeader, pcgTimeStr, 
                                        rlStfrec.PENO, pclTmpStr,
                                        rpJobrec.UAFT, rpJobrec.WGPC,
                                        rpJobrec.FCCO );
        dbg( TRACE, "<%s> Staff Xml (%s)", pclFunc, pcgXmlStr );
        ilRc = SendCedaEvent(igRecipentID[STAFF],0,mod_name,"EXCO",mod_name,"","AGN","","","",
                             pcgXmlStr,"",igSendPrio,RC_SUCCESS);
        dbg(TRACE,"<%s> SendCedaEvent-Staff ilRc = %d",pclFunc, ilRc );
    }

    if( ipInsType == TYPE_EQUIP || (ipInsType == TYPE_BOTH && blEquipInfo == TRUE) )
    {
        if( blEquipInfo == TRUE )
        {
            memcpy( &rlEqurec, &rpEqurec, sizeof(EQUREC) );
            GetEquip( &rlEqurec );
        }

        sprintf( pcgXmlStr, "%s\n" 
                            "<MSG>\n"
                            "<MSGSTREAM_OUT>\n"
                            "<INFOBJ_GENERIC_GSE>\n"
                            "<TIMESTAMP>%s</TIMESTAMP>\n"
                            "<ACTIONTYPE>U</ACTIONTYPE>\n"
                            "<GCDE>%s</GCDE>\n"
                            "</INFOBJ_GENERIC_GSE>\n"
                            "<MSGOBJECTS>\n"
                            "<INFOBJ_GSE>\n"
                            "<ENAM>%s</ENAM>\n"
                            "<UAFT>%s</UAFT>\n"
                            "</INFOBJ_GSE>\n"
                            "</MSGOBJECTS>\n"
                            "</MSGSTREAM_OUT>\n"
                            "</MSG>\n", pcgXmlHeader, pcgTimeStr,
                            rlEqurec.GCDE, rlEqurec.ENAM, rpJobrec.UAFT );
            dbg( TRACE, "<%s> Equip Xml (%s)", pclFunc, pcgXmlStr );
            ilRc = SendCedaEvent(igRecipentID[EQUIP],0,mod_name,"EXCO",mod_name,"","AGN","","","",
                                 pcgXmlStr,"",igSendPrio,RC_SUCCESS);
            dbg(TRACE,"<%s> SendCedaEvent-Equip ilRc = %d",pclFunc, ilRc );
    }
    if( ipActType == TYPE_INSERT )
        InsertDB( rpJobrec, rlStfrec, rlEqurec );
    else
        UpdateDB( rpJobrec, rlStfrec, rlEqurec );
} /*UpdateJob */

/* This function update STAT in IFLJOB during Data Synchronization */
/* This is only meant for record which remained unchanged          */
int UpdateSyncStat( char *pcpUJOB, char *pcpSTAT )
{
    int ilRc;
    char pclStatus[ONE_FOUR];
    char pclSqlBuf[DBQRLEN];
    char pclSqlData[DBQRLEN];
    char *pclFunc = "UpdateSyncStat";
    
    dbg( TRACE, "<%s> UJOB <%s> STAT <%s>", pclFunc, pcpUJOB, pcpSTAT );
    
    strcpy( pclStatus, pcpSTAT );
    if( pcpSTAT[0] != 'P' )
    {
        if ( pcpSTAT[1] == 'I' )
            strcpy( pclStatus, CHECK_INSERT );
        else if ( pcpSTAT[1] == 'U' )
            strcpy( pclStatus, CHECK_UPDATE );
        else if ( pcpSTAT[1] == 'D' )
            strcpy( pclStatus, CHECK_DELETE );
    }

    sprintf( pclSqlBuf, "UPDATE IFLJOB SET STAT = '%s',LSTU = '%8.8s' "
                        "WHERE UJOB = '%s' AND STAT NOT IN ( '%s', '%s' )",
                        pclStatus, &pcgTimeStr[6], pcpUJOB, ONLINE_DELETE, SYNC_DELETE );
                   
    dbg( TRACE, "<%s> SqlBuf <%s>", pclFunc, pclSqlBuf );
    
    ilRc = getOrPutDBData( pclSqlBuf, pclSqlData, START );
    return ilRc;
} /* UpdateSyncStat */

int SyncData()
{
    JOBREC rlJobrec_job, rlJobrec_ifl;
    STFREC rlStfrec_job, rlStfrec_ifl;
    EQUREC rlEqurec_job, rlEqurec_ifl;
    BOOL blInsert;
    BOOL blUpdate;
    time_t ilNow;
    int ili;
    int ilRc;
    int ilIFLCnt = 0;
    short slSqlCursor;
    short slSqlFunc;
    char pclSqlBuf[DBQRLEN];
    char pclSqlBuf2[DBQRLEN];
    char pclSqlData[DBQRLEN];
    char pclSqlData2[DBQRLEN];
    char pclTmpStr[500];
    char pclStartTimer[DATELEN] = "\0";
    char pclEndTimer[DATELEN] = "\0";
    char *pclFunc = "SyncData";


    FormatTime( time(0), pcgTimeStr, "UTC" ); 
    strcat( pcgTimeStr, "000" );

/***** temp commented
    dbg( TRACE, "<%s> Flight Data............", pclFunc );
    SendBulkFlight();
    dbg( TRACE, "<%s> Flight Data Completed!", pclFunc );
*****/


    sprintf( pclSqlBuf, "SELECT COUNT(*) FROM IFLJOB WHERE STAT NOT IN ( '%s', '%s' )", ONLINE_DELETE, SYNC_DELETE );
    dbg( DEBUG, "<%s> SelIFLJob <%s>", pclFunc, pclSqlBuf );
    ilRc = getOrPutDBData( pclSqlBuf, pclTmpStr, START );
    if( ilRc != DB_SUCCESS )
    {
        dbg( TRACE, "<%s> Error in accessing IFLJOB. EXIT!", pclFunc );
        Terminate(30);
    }
    ilIFLCnt = atoi(pclTmpStr);
    dbg( TRACE, "<%s> Number of records in IFLJOB = %d", pclFunc, ilIFLCnt );


    ilNow = time(0); 
    FormatTime( (ilNow-(10*60)), pclStartTimer, "UTC" );              /* 10 min   */
    FormatTime( (ilNow+(12*SECONDS_PER_HOUR)), pclEndTimer, "UTC" ); /* 12 hours */


    dbg( TRACE, "<%s> Job Data............", pclFunc );
    
    sprintf( pclSqlBuf, "SELECT TRIM(URNO),TRIM(UAFT),TRIM(USTF),TRIM(UEQU),TRIM(WGPC),TRIM(FCCO),"
                        "TRIM(DETY),TRIM(UDSR) FROM JOBTAB WHERE ACFR >= '%s' AND ACTO <= '%s' "
                        "AND UTPL IN (%s) AND UAFT <> '0'", pclStartTimer, pclEndTimer, pcgApronTemplate );
    dbg( DEBUG, "<%s> SelJob <%s>", pclFunc, pclSqlBuf );
    slSqlCursor = 0;
    slSqlFunc = START;

    ili = 1;
    while( sql_if(slSqlFunc,&slSqlCursor,pclSqlBuf,pclSqlData) == DB_SUCCESS )
    {
        dbg( TRACE, "<%s> ++++++++++++++++ JOBTAB Record %d ++++++++++++++++", pclFunc, ili++ );

        memset( &rlJobrec_job, 0, sizeof(JOBREC) );
        memset( &rlStfrec_job, 0, sizeof(STFREC) );
        memset( &rlEqurec_job, 0, sizeof(EQUREC) );

        slSqlFunc = NEXT;
        blUpdate = FALSE;
        blInsert = FALSE;

        get_fld(pclSqlData,FIELD_1,STR,URNOLEN,rlJobrec_job.URNO);
        get_fld(pclSqlData,FIELD_2,STR,URNOLEN,rlJobrec_job.UAFT);
        get_fld(pclSqlData,FIELD_3,STR,URNOLEN,rlStfrec_job.URNO);
        get_fld(pclSqlData,FIELD_4,STR,URNOLEN,rlEqurec_job.URNO);
        get_fld(pclSqlData,FIELD_5,STR,TWO_FOUR,rlJobrec_job.WGPC);
        get_fld(pclSqlData,FIELD_6,STR,THREE_FOUR,rlJobrec_job.FCCO);
        get_fld(pclSqlData,FIELD_7,STR,ONE_FOUR,rlJobrec_job.DETY);
        get_fld(pclSqlData,FIELD_8,STR,URNOLEN,rlJobrec_job.UDSR);

        dbg( DEBUG, "<%s> JOBTAB-URNO <%s>", pclFunc, rlJobrec_job.URNO );
        dbg( DEBUG, "<%s> JOBTAB-UAFT <%s>", pclFunc, rlJobrec_job.UAFT );
        dbg( DEBUG, "<%s> JOBTAB-USTF <%s>", pclFunc, rlStfrec_job.URNO );
        dbg( DEBUG, "<%s> JOBTAB-UEQU <%s>", pclFunc, rlEqurec_job.URNO );
        dbg( DEBUG, "<%s> JOBTAB-WGPC <%s>", pclFunc, rlJobrec_job.WGPC );
        dbg( DEBUG, "<%s> JOBTAB-FCCO <%s>", pclFunc, rlJobrec_job.FCCO );
        dbg( DEBUG, "<%s> JOBTAB-DETY <%s>", pclFunc, rlJobrec_job.DETY );
        dbg( DEBUG, "<%s> JOBTAB-UDSR <%s>", pclFunc, rlJobrec_job.UDSR );

        if( ilIFLCnt <= 0 )
            blInsert = TRUE;
        else
        {
            memcpy( &rlJobrec_ifl, &rlJobrec_job, sizeof(JOBREC) );
            memcpy( &rlStfrec_ifl, &rlStfrec_job, sizeof(STFREC) );
            memcpy( &rlEqurec_ifl, &rlEqurec_job, sizeof(EQUREC) );
            ilRc = ReadDB( &rlJobrec_ifl, &rlStfrec_ifl, &rlEqurec_ifl, NOT_TURNAROUND );
            if( ilRc != DB_SUCCESS )
            {
                dbg( DEBUG, "<%s> No rec found in IFLJOB. Insert UJOB <%s>", pclFunc, rlJobrec_job.URNO );
                blInsert = TRUE;
            }
            else
            {
                if( atoi(rlStfrec_job.URNO) > 0 )
                {
                    GetStaff( &rlStfrec_job );
                    if( strcmp( rlStfrec_job.PENO, rlStfrec_ifl.PENO) )
                        blUpdate = TRUE;
                }

                if( blUpdate == FALSE && atoi(rlEqurec_job.URNO) > 0 )
                {
                    GetEquip( &rlEqurec_job );
                    if( strcmp( rlEqurec_job.GCDE, rlEqurec_ifl.GCDE) )
                        blUpdate = TRUE;
                }

                if( blUpdate == FALSE && strcmp( rlJobrec_job.FCCO, rlJobrec_ifl.FCCO ) )
                    blUpdate = TRUE;

                if( blUpdate == FALSE && atoi(rlJobrec_job.UDSR) > 0 )
                {
                    GetWGPC( rlJobrec_job.UDSR, rlJobrec_job.WGPC );
                    if( strcmp( rlJobrec_job.WGPC, rlJobrec_ifl.WGPC ) )
                        blUpdate = TRUE;
                }

                if( blUpdate == TRUE )
                {
                    DeleteJob( rlJobrec_ifl, rlStfrec_ifl, rlEqurec_ifl, TYPE_BOTH );
                    ilRc = ReadDB( &rlJobrec_ifl, &rlStfrec_ifl, &rlEqurec_ifl, IS_TURNAROUND );
                    if( ilRc == DB_SUCCESS )
                        DeleteJob( rlJobrec_ifl, rlStfrec_ifl, rlEqurec_ifl, TYPE_BOTH );
                    blInsert = TRUE;
                }
                else
                    UpdateSyncStat( rlJobrec_ifl.URNO, rlJobrec_ifl.STAT );
            }
        }

        if( blInsert == TRUE )
        {
            if( rlJobrec_job.DETY[0] == '0' )
            {
                ilRc = GetTurnaroundUAFT( rlJobrec_job.UAFT, rlJobrec_ifl.UAFT, rlJobrec_job.RKEY );
                UpdateJob( rlJobrec_job, rlStfrec_job, rlEqurec_job, TYPE_INSERT, TYPE_BOTH );
                if( ilRc == DB_SUCCESS )
                {
                    strcpy( rlJobrec_job.UAFT, rlJobrec_ifl.UAFT );
                    UpdateJob( rlJobrec_job, rlStfrec_job, rlEqurec_job, TYPE_INSERT, TYPE_BOTH );
                }
            }
            else
                UpdateJob( rlJobrec_job, rlStfrec_job, rlEqurec_job, TYPE_INSERT, TYPE_BOTH );
        }
    } /* while */
    close_my_cursor(&slSqlCursor);

    sprintf( pclSqlBuf, "SELECT COUNT(*) FROM IFLJOB WHERE STAT NOT LIKE '%%D' "
                        "AND STAT NOT LIKE 'P%%' AND LSTU <> '%8.8s'", &pcgTimeStr[6] );
    dbg( DEBUG, "<%s> SelIFLJob <%s>", pclFunc, pclSqlBuf );
    ilRc = getOrPutDBData( pclSqlBuf, pclTmpStr, START );
    if( ilRc != DB_SUCCESS )
    {
        dbg( TRACE, "<%s> Error in accessing IFLJOB. EXIT!", pclFunc );
        Terminate(30);
    }
    ilIFLCnt = atoi(pclTmpStr);
    dbg( TRACE, "<%s> New Number of records in IFLJOB = %d", pclFunc, ilIFLCnt );

    if( ilIFLCnt <= 0 )
        return;

    sprintf( pclSqlBuf, "SELECT UJOB, UAFT, STAT, RKEY FROM IFLJOB WHERE STAT NOT LIKE '%%D' "
                        "AND STAT NOT LIKE 'P%%' AND LSTU <> '%8.8s'", &pcgTimeStr[6] );
    dbg( DEBUG, "<%s> SelIFL <%s>", pclFunc, pclSqlBuf );
    slSqlCursor = 0;
    slSqlFunc = START;

    ili = 1;
    while( sql_if(slSqlFunc,&slSqlCursor,pclSqlBuf,pclSqlData) == DB_SUCCESS )
    {
        dbg( TRACE, "<%s> ............... IFLJOB Record %d ...............", pclFunc, ili++ );
        slSqlFunc = NEXT;
        
        memset( &rlJobrec_ifl, 0, sizeof(JOBREC) );
        memset( &rlStfrec_ifl, 0, sizeof(STFREC) );
        memset( &rlEqurec_ifl, 0, sizeof(EQUREC) );

        get_fld(pclSqlData,FIELD_1,STR,URNOLEN,rlJobrec_ifl.URNO);
        get_fld(pclSqlData,FIELD_2,STR,URNOLEN,rlJobrec_ifl.UAFT);
        get_fld(pclSqlData,FIELD_3,STR,ONE_FOUR,rlJobrec_ifl.STAT);
        get_fld(pclSqlData,FIELD_4,STR,URNOLEN,rlJobrec_ifl.RKEY);

        dbg( DEBUG, "<%s> IFLJOB-UJOB <%s>", pclFunc, rlJobrec_ifl.URNO );
        dbg( DEBUG, "<%s> IFLJOB-UAFT <%s>", pclFunc, rlJobrec_ifl.UAFT );
        dbg( DEBUG, "<%s> IFLJOB-STAT <%s>", pclFunc, rlJobrec_ifl.STAT );
        dbg( DEBUG, "<%s> IFLJOB-RKEY <%s>", pclFunc, rlJobrec_ifl.RKEY );

        if( rlJobrec_ifl.STAT[0] == 'P' )
            continue;

        sprintf( pclSqlBuf2, "SELECT URNO FROM JOBTAB WHERE URNO = '%s' AND UAFT = '%s'",
                             rlJobrec_ifl.URNO, rlJobrec_ifl.UAFT );
        dbg( DEBUG, "<%s> SelJob <%s>", pclFunc, pclSqlBuf2 );
        ilRc = getOrPutDBData( pclSqlBuf2, pclSqlData, START );
        if( ilRc != DB_SUCCESS )
        {
            ilRc = ReadDB( &rlJobrec_ifl, &rlStfrec_ifl, &rlEqurec_ifl, NOT_TURNAROUND );
            if( ilRc == DB_SUCCESS )
            {
                DeleteJob( rlJobrec_ifl, rlStfrec_ifl, rlEqurec_ifl, TYPE_BOTH );
                ilRc = ReadDB( &rlJobrec_ifl, &rlStfrec_ifl, &rlEqurec_ifl, IS_TURNAROUND );
                if( ilRc == DB_SUCCESS )
                    DeleteJob( rlJobrec_ifl, rlStfrec_ifl, rlEqurec_ifl, TYPE_BOTH );
            }
        }
        else
        {
            /* To specify outdated job to prevent re-sync of job */
            rlJobrec_ifl.STAT[0] = 'P';  /* Pass */
            UpdateSyncStat( rlJobrec_ifl.URNO, rlJobrec_ifl.STAT );
        }
    } /* while */
    close_my_cursor(&slSqlCursor);
    
    dbg( TRACE, "<%s> Job Data Completed!", pclFunc );

} /* SyncData */

int GetWGPC( char *pcpUDSR, char *pcpWGPC )
{
    int ilRc;
    char pclSqlBuf[DBQRLEN];
    char pclSqlData[DBQRLEN];
    char *pclFunc = "GetWGPC";

    sprintf(pclSqlBuf, "SELECT TRIM(WGPC) FROM JOBTAB WHERE UDSR = '%s' AND WGPC <> ' '", pcpUDSR );
    dbg( TRACE, "<%s> SelWGPC <%s>", pclFunc, pclSqlBuf );
    ilRc = getOrPutDBData( pclSqlBuf, pcpWGPC, START );
    dbg( TRACE, "<%s> WGPC <%s>", pclFunc, pcpWGPC );

    return ilRc;
}

int GetStaff( STFREC *rpStfrec )
{
    int ilRc;
    char pclSqlBuf[DBQRLEN];
    char pclSqlData[DBQRLEN];
    char *pclFunc = "GetStaff";

    sprintf( pclSqlBuf, "SELECT TRIM(PENO),TRIM(FINM),TRIM(LANM),TRIM(SHNM) "
                        "FROM STFTAB WHERE URNO = '%s'", rpStfrec->URNO );
    dbg( TRACE, "<%s> STFSql <%s>", pclFunc, pclSqlBuf );
    ilRc = getOrPutDBData( pclSqlBuf, pclSqlData, START );
    if( ilRc == RC_SUCCESS )
    {
        get_fld(pclSqlData,FIELD_1,STR,THREE_FOUR,rpStfrec->PENO);
        get_fld(pclSqlData,FIELD_2,STR,NAMELEN,rpStfrec->FINM);
        get_fld(pclSqlData,FIELD_3,STR,NAMELEN,rpStfrec->LANM);
        get_fld(pclSqlData,FIELD_4,STR,NAMELEN,rpStfrec->SHNM);

        dbg( DEBUG, "<%s> STF - PENO <%s>", pclFunc, rpStfrec->PENO );
        dbg( DEBUG, "<%s> STF - FINM <%s>", pclFunc, rpStfrec->FINM );
        dbg( DEBUG, "<%s> STF - LANM <%s>", pclFunc, rpStfrec->LANM );
        dbg( DEBUG, "<%s> STF - SHNM <%s>", pclFunc, rpStfrec->SHNM );
    }
    else
        dbg( TRACE, "<%s> STF URNO <%s> not found", pclFunc, rpStfrec->URNO );
    return ilRc;
}

int GetEquip( EQUREC *rpEqurec )
{
    int ilRc;
    char pclSqlBuf[DBQRLEN];
    char pclSqlData[DBQRLEN];
    char *pclFunc = "GetEquip";

    sprintf( pclSqlBuf, "SELECT TRIM(GCDE),TRIM(ENAM) "
                        "FROM EQUTAB WHERE URNO = '%s'", rpEqurec->URNO );
    dbg( TRACE, "<%s> EQUSql <%s>", pclFunc, pclSqlBuf );
    ilRc = getOrPutDBData( pclSqlBuf, pclSqlData, START );
    if( ilRc == RC_SUCCESS )
    {
        get_fld(pclSqlData,FIELD_1,STR,TWO_FOUR,rpEqurec->GCDE);
        get_fld(pclSqlData,FIELD_2,STR,NAMELEN,rpEqurec->ENAM);

        dbg( DEBUG, "<%s> EQU - GCDE <%s>", pclFunc, rpEqurec->GCDE );
        dbg( DEBUG, "<%s> EQU - ENAM <%s>", pclFunc, rpEqurec->ENAM );
    }
    else
        dbg( TRACE, "<%s> EQU URNO <%s> not found", pclFunc, rpEqurec->URNO );
    return ilRc;
}

int GetTurnaroundUAFT( char *pcpUAFT, char *pcpTurnArndUAFT, char *pcpRKEY )
{
    int ilRc;
    char pclSqlBuf[DBQRLEN];
    char pclSqlData[DBQRLEN];
    char *pclFunc = "GetTurnaroundUAFT";

    sprintf( pclSqlBuf, "SELECT TRIM(RKEY) FROM AFTTAB WHERE URNO = %s", pcpUAFT );
    dbg( DEBUG, "<%s> SelRkey <%s>", pclFunc, pclSqlBuf );
    ilRc = getOrPutDBData( pclSqlBuf, pcpRKEY, START );
    if( ilRc == DB_SUCCESS )
    {
        sprintf( pclSqlBuf, "SELECT TRIM(URNO) FROM AFTTAB WHERE RKEY = %s AND URNO <> %s", pcpRKEY, pcpUAFT );
        dbg( DEBUG, "<%s> SelUaft <%s>", pclFunc, pclSqlBuf );
        ilRc = getOrPutDBData( pclSqlBuf, pcpTurnArndUAFT, START );
    }

    return ilRc;
}
