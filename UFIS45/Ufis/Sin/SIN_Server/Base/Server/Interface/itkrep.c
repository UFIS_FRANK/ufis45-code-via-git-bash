#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Sin/SIN_Server/Base/Server/Interface/itkrep.c 1.3 2009/01/07 12:18:46SGT ble Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :  Mei                                                      */
/* Date           :  06-MAR-2007                                                         */
/* Description    :                                                           */
/*                                                                            */
/* Update history :                                                           */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */

static char sccs_version[] ="@(#) UFIS44 (c) ABB AAT/I skeleton.c 44.1.0 / 11.12.2002 HEB";

/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */

#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "fditools.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "rmschkrul.h"
#include "syslib.h"
#include "db_if.h"
#include "urno_fn.inc"
#include "itrek_fn.inc"
#include <time.h>
#include <cedatime.h>

#define MQRDLEN  30000   /* For Msg from the MQ */
#define DBQRLEN  30000   /* For DB SELECT query */
#define MQ_STATUS_OK  1    /* For MQ Status Update */
#define MQ_STATUS_ERR 2    /* For MQ Status Update */


/* Report Update Type */
#define NUM_UPD_TYPE  3
#define NUM_REP_TYPE  1
#define NUM_BPSR_UPD_FIELDS 11
#define NUM_BPTRPTS_FIELDS  18
#define NUM_BPTRDET_FIELDS   5
#define NUM_ASRPTS_FIELDS   74

#define BPSR_UPD 1
#define BPTRPTS  2
#define BPTRDET  3
#define ASRPTS   4

#define BPSR_REQ 5

#define START_DATA_SIZE  5000

#define URNOLEN 10
#define STATLEN 1
#define FLNOLEN 9
#define DATELEN 14

typedef struct
{
    char URNO[URNOLEN+1];
    char UAFT[URNOLEN+1];
    char ADID[STATLEN+1];
    char FLNO[FLNOLEN+1];
    char STDT[DATELEN+1];
    char ATDT[DATELEN+1];
    char TGID[STATLEN+1];
    char LDTY[STATLEN+1];
    char BFTM[STATLEN+1];
    char AFTM[STATLEN+1];
    char BLTM[STATLEN+1];
    char ALTM[STATLEN+1];
} IBUREC;

/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
int debug_level = TRACE;
static long lgEvtCnt = 0;
static ITEM  *prgItem      = NULL;             /* The queue item pointer  */
static EVENT *prgEvent     = NULL;             /* The event pointer       */
static int   igItemLen     = 0;                /* length of incoming item */
static EVENT *prgOutEvent  = NULL;

static char cgProcessName[20] = "\0";
static char cgConfigFile[512] = "\0";
static char cgHopo[8] = "\0";                 /* default home airport    */

static char pcgXmlStr[START_DATA_SIZE] = "\0";                 /* default home airport    */
static int igItkRptModid;

char *pcgBPSR_UPD_TagName[NUM_BPSR_UPD_FIELDS] = { "UAFT", "AD", "FLNO", "ST", 
                                                   "AT", "T", "LH", "BFT", 
                                                   "AFT", "BLT", "ALT" };

char *pcgBPSR_UPD_DBField[NUM_BPSR_UPD_FIELDS] = { "UAFT", "ADID", "FLNO", "STDT", 
                                                   "ATDT", "TGID", "LDTY", "BFTM", 
                                                   "AFTM", "BLTM", "ALTM" };

char *pcgBPTRPTS_TagName[NUM_BPTRPTS_FIELDS] = { "UAFT", "FLDT", "AT", "FLNU", 
                                                 "ACFT_TYPE", "BELT", "PARK_STAND", "AO_ID", 
                                                 "AO_FNAME", "TBS_UP", "FTRIP_B", "LTRIP_B", 
                                                 "FBAG", "LBAG", "FBAG_TIME_MET", "LBAG_TIME_MET", 
                                                 "AO_TRP_RMK", "BO_RMK" };

char *pcgBPTRPTS_DBField[NUM_BPTRPTS_FIELDS] = { "UAFT", "FLDT", "ATDT", "FLNU", 
                                                 "ACT3", "BELT", "PSTD", "AOID", 
                                                 "AONM", "TUDT", "FBDT", "LBDT", 
                                                 "FBAG", "LBAG", "FBOK", "LBOK", 
                                                 "ARMK", "BRMK" };

char *pcgBPTRDET_TagName[NUM_BPTRDET_FIELDS] = { "UAFT", "SR_NO", "ULDNO", "ULDTIME", 
                                                 "CLASS" };

char *pcgBPTRDET_DBField[NUM_BPTRDET_FIELDS] = { "UAFT", "SRNO", "ULDI", "ULDT", 
                                                 "CLAS" };

char *pcgASRPTS_TagName[NUM_ASRPTS_FIELDS] = { "UAFT",    "FLNO",    "AD",       "AOID", 
                                               "AOFNAME", "AOLNAME", "ACFTREGN", "ACFTTYPE",
                                               "FR", "TO", "ST", "AT",
                                               "BAY", "PLBA", "PLBB", "JCLFWD",
                                               "JCLAFT", "TRNO", "SLFWD", "SLAFT",
                                               "MODE", "LOAD", "FTRIP", "LTRIP",
                                               "UNLEND", "LDSTRT", "LDEND", "LSTDOR",
                                               "PLB", "CBRIEF", "CSAFE", "CCONE",
                                               "CLOW", "CEXT", "CBRTEST", "CDMGBDY",
                                               "CDMGDG", "CDG", "CULD", 
                                               "OICAO", "OICAO_D", 
                                               "OBRK", "OBRK_D", 
                                               "OCPM", "OCPM_D", 
                                               "OBG", "OBG_D", 
                                               "OWETH", "OWETH_D", 
                                               "OLGHT", "OLGHT_D", 
                                               "OBAY", "OBAY_D", 
                                               "OOVR", "OOVR_D", 
                                               "OFOD", "OFOD_D", 
                                               "OILS", "OILS_D", 
                                               "OJAM", "OJAM_D", 
                                               "OGSE", "OGSE_D", 
                                               "OWARP", "OWARP_D", 
                                               "ODNG", "ODNG_D",
                                               "OBLK", "OBLK_D",
                                               "ULDCMT", "GENCMT", "RSNBPT", 
                                               "DRO", "DM" };

char *pcgASRPTS_DBField[NUM_ASRPTS_FIELDS] = { "UAFT", "FLNO", "ADID", "AOID", 
                                               "AOFN", "AOLN", "REGN", "ACT3",
                                               "APC3", "DEST", "STDT", "ATDT",
                                               "PSTD", "PLBA", "PLBB", "JFNM",
                                               "JANM", "TRNO", "SFNM", "SANM",
                                               "MODN", "LOAD", "FTDT", "LTDT",
                                               "ULDT", "LSDT", "LEDT", "LDDT",
                                               "PLDT", "CBRF", "CSFE", "CCNE",
                                               "CLOW", "CEXT", "CBTS", "CDBG",
                                               "CDMG", "CDGY", "CULD", 
                                               "OICA", "OICD", 
                                               "OBRK", "OBRD", 
                                               "OCPM", "OCPD", 
                                               "OBGY", "OBGD", 
                                               "OWEH", "OWED", 
                                               "OLGT", "OLGD", 
                                               "OBAY", "OBAD", 
                                               "OOVR", "OOVD", 
                                               "OFOD", "OFDD", 
                                               "OILS", "OILD", 
                                               "OJAM", "OJAD", 
                                               "OGSE", "OGSD", 
                                               "OWAR", "OWAD", 
                                               "ODNG", "ODND",
                                               "OBLK", "OBLD",
                                               "ULRM", "GERM", "RBPT", 
                                               "DRNM", "DMNM" };


/******************************************************************************/
/* Function prototypes                                                        */
/******************************************************************************/
static int  Init_Process();
static int ReadConfigEntry(char *pcpFname, char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer);

static int Reset(void);                       /* Reset program          */
static void Terminate(int ipSleep);            /* Terminate program      */
static void HandleSignal(int);                 /* Handles signals        */
static void HandleErr(int);                    /* Handles general errors */
static void HandleQueErr(int);                 /* Handles queuing errors */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/

static int HandleData(EVENT *prpEvent);       /* Handles event data     */
static int GetElementValue(char *pclDest, char *pclOrigData, char *pclBegin, char *pclEnd);
static void HandleReportRequests(char *pclFields, char *pcpData);
static void HandleTripTiming(char *pcpFields, char *pcpData);

static int  TriggerAction(char *pcpTableName);
static int  WaitAndCheckQueue(int ipTimeout, int ipModId, ITEM **prpItem);
static int  CheckQueue(int ipModId, ITEM **prpItem);
static int getOrPutDBData(char *pcpSelection, char *pcpData, unsigned int ipMode);
static void UpdateTable(long lpMqrUrno, char *pcpData, int ipUpdType);
static void MQMsgUpd( long lpMqrUrno, int ipMQStatus );
static int StoreData( char *pcpData, int ipNumFields, char **pcpTagName, char **pcpDBField, 
               char *pcpTableName, char *pcpUrno );
static void GenerateReport(long lpMqrUrno, char *pcpData, int ipRepType);


/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
    int ilRc = RC_SUCCESS;
    int ilCnt = 0;
    char *pclFunc = "Main";

    INITIALIZE; 

    strcpy(cgProcessName,argv[0]);

    dbg(TRACE,"<%s> version <%s>", pclFunc, sccs_version);

    /* Attach to the MIKE queues */
    do
    {
        ilRc = init_que();
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"<%s> init_que() failed! waiting 6 sec ...", pclFunc);
            sleep(6);
            ilCnt++;
        }
    }while((ilCnt < 10) && (ilRc != RC_SUCCESS));

    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"<%s> init_que() failed! waiting 60 sec ...", pclFunc);
        sleep(60);
        exit(1);
    }
    else
    {
        dbg(TRACE,"<%s> init_que() OK! mod_id <%d>",pclFunc, mod_id);
    }/* end of if */

    ilCnt = 0;
    do
    {
        ilRc = init_db();
        if (ilRc != RC_SUCCESS)
        {
            check_ret(ilRc);
            dbg(TRACE,"<%s> init_db() failed! waiting 6 sec ...", pclFunc);
            sleep(6);
            ilCnt++;
        } /* end of if */
    } while((ilCnt < 10) && (ilRc != RC_SUCCESS));

    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"<%s> init_db() failed! waiting 60 sec ...", pclFunc);
        sleep(60);
        exit(2);
    }
    else
    {
        dbg(TRACE,"<%s> init_db() OK!", pclFunc);
    } /* end of if */

    /* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */
    sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
    dbg(TRACE,"<%s> ConfigFile <%s>",pclFunc, cgConfigFile);
    ilRc = TransferFile(cgConfigFile);
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"<%s> TransferFile(%s) failed!",pclFunc, cgConfigFile);
    } /* end of if */

    sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
    dbg(TRACE,"<%s> ConfigFile <%s>",pclFunc, cgConfigFile);
    ilRc = TransferFile(cgConfigFile);
    if(ilRc != RC_SUCCESS)
    { 
        dbg(TRACE,"<%s> TransferFile(%s) failed!",pclFunc, cgConfigFile);
    } /* end of if */

    ilRc = SendRemoteShutdown(mod_id);
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"<%s> SendRemoteShutdown(%d) failed!",pclFunc, mod_id);
    } /* end of if */

    if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
        dbg(DEBUG,"<%s> waiting for status switch ...", pclFunc);
        HandleQueues();
    }/* end of if */

    if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
        dbg(TRACE,"<%s> initializing ...", pclFunc);
        ilRc = Init_Process();
        if(ilRc != RC_SUCCESS)
            dbg(TRACE,"<%s> init failed!", pclFunc);
    } 
    else 
        Terminate(30);

    dbg(TRACE,"<%s> initializing OK", pclFunc);
    dbg(TRACE,"==========================");
    
    for(;;)
    {
        ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* Acknowledge the item */
        ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
        /* handle que_ack error */
        if( ilRc != RC_SUCCESS ) 
        HandleQueErr(ilRc);

        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */

        prgEvent = (EVENT *) prgItem->text;
                
        if( ilRc == RC_SUCCESS )
        {
            lgEvtCnt++;
            switch( prgEvent->command )
            {
                case HSB_STANDBY :
                     ctrl_sta = prgEvent->command;
                     HandleQueues();
                     break; 
                case HSB_COMING_UP :
                     ctrl_sta = prgEvent->command;
                     HandleQueues();
                     break; 
                case HSB_ACTIVE :
                     ctrl_sta = prgEvent->command;
                     break; 
                case HSB_ACT_TO_SBY :
                     ctrl_sta = prgEvent->command;
                     /* CloseConnection(); */
                     HandleQueues();
                     break; 
                case HSB_DOWN :
                     /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                     ctrl_sta = prgEvent->command;
                     Terminate(1);
                     break; 
                case HSB_STANDALONE :
                     ctrl_sta = prgEvent->command;
                     ResetDBCounter();
                     break; 
                case REMOTE_DB :
                     /* ctrl_sta is checked inside */
                     HandleRemoteDB(prgEvent);
                     break;
                case SHUTDOWN :
                     /* process shutdown - maybe from uutil */
                     Terminate(1);
                     break;
                case RESET :
                     ilRc = Reset();
                     break;
                case EVENT_DATA :
                     if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
                     {
                         ilRc = HandleData(prgEvent);
                         if(ilRc != RC_SUCCESS)
                         {
                             HandleErr(ilRc);
                         }/* end of if */
                     }
                     else
                     {
                         dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
                         DebugPrintItem(TRACE,prgItem);
                         DebugPrintEvent(TRACE,prgEvent);
                     }/* end of if */
                     break;
                case TRACE_ON :
                     dbg_handle_debug(prgEvent->command);
                     break;
                case TRACE_OFF :
                     dbg_handle_debug(prgEvent->command);
                     break;
                default :
                     dbg(TRACE,"<%s> unknown event", pclFunc);
                     DebugPrintItem(TRACE,prgItem);
                     DebugPrintEvent(TRACE,prgEvent);
                     break;
             } /* end switch */
         } 
         else 
         {
             /* Handle queuing errors */
             HandleQueErr(ilRc);
         } /* end else */
    } /* end for */
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int ReadConfigEntry(char *pcpFname, char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer)
{
    int ilRc = RC_SUCCESS;

    char clSection[124] = "\0";
    char clKeyword[124] = "\0";

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRc = iGetConfigEntry(pcpFname,clSection,clKeyword,
               CFG_STRING,pcpCfgBuffer);
    if(ilRc != RC_SUCCESS){
        dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
    } 
    else{
        dbg(DEBUG,"Config Entry <%s>,<%s>:<%s> found in %s",
        clSection, clKeyword ,pcpCfgBuffer, cgConfigFile);
    }/* end of if */
    return ilRc;
}

static int Init_Process()
{
    int  ilRc = RC_SUCCESS;         /* Return code */
    char pclTmpStr[500] = "\0";
    char *pclFunc = "Init_Process";

    if(ilRc == RC_SUCCESS)
    {
        /* read HomeAirPort from SGS.TAB */
        ilRc = tool_search_exco_data("SYS", "HOMEAP", cgHopo);
        if (ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"<%s> EXTAB,SYS,HOMEAP not found in SGS.TAB", pclFunc);
            Terminate(30);
        } 
        else 
        {
            dbg(TRACE,"<%s> home airport <%s>", pclFunc,cgHopo);
        }
        igItkRptModid = tool_get_q_id("itkrpt");
        dbg( TRACE, "<%s> igItkRptModid <%d>", pclFunc, igItkRptModid );
    }

    ilRc = ReadConfigEntry( cgConfigFile, "MAIN","DEBUG_LEVEL",pclTmpStr);
    debug_level = TRACE;
    if( !strncmp( pclTmpStr, "DEBUG", 5 ) )
        debug_level = DEBUG;
    dbg(TRACE,"<%s> debug_level <%s><%d>",pclFunc, pclTmpStr, debug_level);

    return(ilRc);
    
} /* end of initialize */

/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
    int ilRc = RC_SUCCESS;              /* Return code */
    
    dbg(TRACE,"Reset: now resetting");
    
    return ilRc;
    
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
    /* unset SIGCHLD ! DB-Child will terminate ! */

    dbg(TRACE,"Terminate: now DB logoff ...");

    signal(SIGCHLD,SIG_IGN);

    logoff();

    dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);


    dbg(TRACE,"Terminate: now leaving ...");

    fclose(outp);

    sleep(ipSleep < 1 ? 1 : ipSleep);
    
    exit(0);
    
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/

static void HandleSignal(int pipSig)
{
    dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);

    switch(pipSig)
    {
    default :
        Terminate(1);
        break;
    } /* end of switch */

    exit(1);
    
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
    dbg(TRACE,"HandleErr: <%d> : unknown ERROR",pipErr);

    return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    switch(pipErr) {
    case    QUE_E_FUNC  :   /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :   /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;
    case    QUE_E_SEND  :   /* Error using msgsnd */
        dbg(TRACE,"<%d> : msgsnd failed",pipErr);
        break;
    case    QUE_E_GET   :   /* Error using msgrcv */
        dbg(TRACE,"<%d> : msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX   :
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE  :
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES  :
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        break;
    case    QUE_E_RESP  :   /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;
    case    QUE_E_FULL  :
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG :   /* No message on queue */
        dbg(TRACE,"<%d> : no messages on queue",pipErr);
        break;
    case    QUE_E_INVORG    :   /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :   /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;
    default         :   /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
    } /* end switch */
         
    return;

} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
    int ilRc = RC_SUCCESS;          /* Return code */
    int ilBreakOut = FALSE;
    
    do
    {
        ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text; 

        if( ilRc == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRc != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRc);
            } /* fi */
        
            switch( prgEvent->command )
            {
            case    HSB_STANDBY :
                ctrl_sta = prgEvent->command;
                break;  
    
            case    HSB_COMING_UP   :
                ctrl_sta = prgEvent->command;
                break;  
    
            case    HSB_ACTIVE  :
                ctrl_sta = prgEvent->command;
                ilBreakOut = TRUE;
                break;  

            case    HSB_ACT_TO_SBY  :
                ctrl_sta = prgEvent->command;
                break;  
    
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate(1);
                break;  
    
            case    HSB_STANDALONE  :
                ctrl_sta = prgEvent->command;
                ResetDBCounter();
                ilBreakOut = TRUE;
                break;  

            case    REMOTE_DB :
                /* ctrl_sta is checked inside */
                HandleRemoteDB(prgEvent);
                break;

            case    SHUTDOWN    :
                Terminate(1);
                break;
                        
            case    RESET       :
                ilRc = Reset();
                break;
                        
            case    EVENT_DATA  :
                dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
                    
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;

            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;

            default         :
                dbg(TRACE,"HandleQueues: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        } else {
            /* Handle queuing errors */
            HandleQueErr(ilRc);
        } /* end else */
    } while (ilBreakOut == FALSE);

            ilRc = Init_Process();
            if(ilRc != RC_SUCCESS)
            {
                dbg(TRACE,"InitItkhdl: init failed!");
            } 
     

} /* end of HandleQueues */
    



/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(EVENT *prpEvent)
{
    int    ilRc           = RC_SUCCESS;         /* Return code */
    int      ilCmd          = 0;

    BC_HEAD *prlBchead       = NULL;
    CMDBLK  *prlCmdblk       = NULL;
    char    *pclSelection    = NULL;
    char    *pclFields       = NULL;
    char    *pclData         = NULL;
    char        clTable[34];

    prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
    prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
    pclSelection = prlCmdblk->data;
    pclFields    = pclSelection + strlen(pclSelection) + 1;
    pclData      = pclFields + strlen(pclFields) + 1;


    strcpy(clTable,prlCmdblk->obj_name);

    dbg(TRACE,"========== START <%10.10d> ==========",lgEvtCnt);

    /****************************************/
    DebugPrintBchead(TRACE,prlBchead);
    DebugPrintCmdblk(TRACE,prlCmdblk);
    dbg(TRACE,"Command: <%s>",prlCmdblk->command);
    dbg(TRACE,"originator follows event = %p ",prpEvent);

    dbg(TRACE,"originator<%d>",prpEvent->originator);
    dbg(TRACE,"selection follows Selection = %p ",pclSelection);
    dbg(TRACE,"selection <%s>",pclSelection);
    dbg(TRACE,"fields    <%s>",pclFields);
    dbg(TRACE,"data      <%s>",pclData);
    /****************************************/

    /* Send from iTrek Client via MQ handler(ITKWMU) */ 
    if (strcmp(prlCmdblk->command,"RPT") == 0)
        HandleReportRequests(pclFields, pclData);
    else if (strcmp(prlCmdblk->command,"TIM") == 0)
        HandleTripTiming(pclFields, pclData);

    dbg(TRACE,"==========  END  <%10.10d> ==========",lgEvtCnt);

    return(RC_SUCCESS);
    
} 

static void HandleReportRequests(char *pclFields, char *pcpData)
{
    int ilRc = RC_SUCCESS;
    long llMqrUrno = 0;
    short slSqlFunc = 0;
    char pclTmpStr[MQRDLEN] = "\0";
    char pclSqlBuf[DBQRLEN] = "\0";
    char pclData[MQRDLEN] = "\0";
    char *pclFunc = "HandleReportRequests";
    int ilFound;

    dbg(TRACE,"<%s> ========== Start ==========", pclFunc);

    /* Get the URNO of MQRTAB */
    /*llMqrUrno = atol(pcpData);*/

    /* Below line will fetch the message(DATA) from MQRTAB for URNO */
    /*slSqlFunc = START;
    sprintf(pclSqlBuf,"SELECT DATA FROM MQRTAB WHERE URNO = %d",llMqrUrno);
    dbg(DEBUG,"<%s> now read from DB: <%s>",pclFunc,pclSqlBuf);
    ilRc = getOrPutDBData(pclSqlBuf,pclData,slSqlFunc);*/
    
    strcpy( pclData, pcpData );
    /* check for any special characters available in the message */
    ConvertDbStringToClient(pclData); 
    dbg(TRACE,"<%s> ------------- Message received: -------------- <%s>",pclFunc,pclData);

    
    ilFound = GetElementValue(pclTmpStr, pclData, "<BPSR_UPD>", "</BPSR_UPD>");
    if( ilFound == TRUE )
    {   
        dbg(TRACE,"<%s> passing <BPSR_UPD> to UpdateTable",pclFunc);
        UpdateTable(llMqrUrno, pclData, BPSR_UPD);
        return;
    }
    ilFound = GetElementValue(pclTmpStr, pclData, "<BPTRPTS>", "</BPTRPTS>");
    if( ilFound == TRUE )
    {
        dbg(TRACE,"<%s> passing <BPTRPTS> to UpdateTable",pclFunc);
        UpdateTable(llMqrUrno, pclData, BPTRPTS);
        return;
    }
    ilFound = GetElementValue(pclTmpStr, pclData, "<ASRPTS>", "</ASRPTS>");
    if( ilFound == TRUE )
    {
        dbg(TRACE,"<%s> passing <ASRPTS> to UpdateTable",pclFunc);
        UpdateTable(llMqrUrno,pclData,ASRPTS);
        return;
    }
    ilFound = GetElementValue(pclTmpStr, pclData, "<BPSR_REQ>", "</BPSR_REQ>");
    if( ilFound == TRUE )
    {
        dbg(TRACE,"<%s> passing <BPSR_REQ> to GenerateReport",pclFunc);
        GenerateReport(llMqrUrno,pclData,BPSR_REQ);
        return;
    }
    dbg(TRACE,"<%s> ========== End ==========",pclFunc);
} /* HandleReportRequests */


static void HandleTripTiming(char *pcpFields, char *pcpData)
{
    int ilRc = RC_SUCCESS;
    char pclTmpStr[MQRDLEN] = "\0";
    char pclSqlBuf[DBQRLEN] = "\0";
    char pclData[1024] = "\0";
    char pclFields[1024] = "\0";
    char pclUAFT[12];
    char pclTripName[40];
    char pclTripValue[20];
    char *pclToken;
    char *pclFunc = "HandleTripTiming";

    dbg(TRACE,"<%s> ========== Start ==========", pclFunc);

    strcpy( pclData, pcpData );

    dbg(TRACE,"<%s> ------------- Message received: -------------- <%s>",pclFunc,pclData);

    pclToken = (char *)strtok( pclData, "," );
    if( pclToken != NULL )
        strcpy( pclTripName, pclToken );
    pclToken = (char *)strtok( NULL, "," );
    if( pclToken != NULL )
        strcpy( pclTripValue, pclToken );
    pclToken = (char *)strtok( NULL, "\0" );
    if( pclToken != NULL )
        strcpy( pclUAFT, pclToken );

    if( strlen(pclTripName) <= 0 || strlen(pclTripValue) <= 0 || strlen(pclUAFT) <= 0 )
    {
        dbg( TRACE, "<%s> Invalid Trip Information!", pclFunc );
        return;
    }
    if( strlen(pclTripValue) > DATELEN )
        pclTripValue[DATELEN] = '\0';

    dbg( TRACE, "<%s> UAFT <%s> TripName <%s> TripValue <%s>", pclFunc, pclUAFT, pclTripName, pclTripValue );

    if( !strncmp(pclTripName, "FTRIP_B", 7) )
        sprintf( pclSqlBuf, "UPDATE ITRTAB SET RCDT = '%s' WHERE UAFT = '%s' AND RTTY = 'F'", pclTripValue, pclUAFT );
    else if( !strncmp(pclTripName, "LTRIP_B", 7) )
        sprintf( pclSqlBuf, "UPDATE ITRTAB SET RCDT = '%s' WHERE UAFT = '%s' AND RTTY = 'L'", pclTripValue, pclUAFT );
    else if( !strncmp(pclTripName, "FTRIP", 5) )
        sprintf( pclSqlBuf, "UPDATE ITRTAB SET SDDT = '%s' WHERE UAFT = '%s' AND STTY = 'F'", pclTripValue, pclUAFT );
    else if( !strncmp(pclTripName, "LTRIP", 5) )
        sprintf( pclSqlBuf, "UPDATE ITRTAB SET SDDT = '%s' WHERE UAFT = '%s' AND STTY = 'L'", pclTripValue, pclUAFT );
    else
    {
        dbg( TRACE, "<%s> Invalie TripName received", pclFunc );
        return;
    }

    dbg( TRACE, "<%s> UpdITR <%s>", pclFunc, pclSqlBuf );
    ilRc = getOrPutDBData(pclSqlBuf,pclTmpStr,START);
    dbg( TRACE, "<%s> ilRc <%d>", pclFunc, ilRc );
     
} /* HandleTripTiming */

static void UpdateTable(long lpMqrUrno, char *pcpData, int ipUpdType)
{
    int ilRc;
    int ilUntagCnt = 0;
    char pclUrno[URNOLEN+1] = "\0";
    char pclUIBR[URNOLEN+1] = "\0";
    char pclData[MQRDLEN];
    char pclOneRec[MQRDLEN];
    char *pclFunc = "UpdateTable";

    dbg(TRACE,"<%s> ========== Start ==========",pclFunc);
    dbg(TRACE,"<%s> Update Type <%d>",pclFunc, ipUpdType);
    /*memcpy( pclData, pcpData, strlen(pcpData) );*/
    strcpy( pclData, pcpData );

    ilRc = RC_FAIL;
    if( ipUpdType == BPSR_UPD )
    {
        ilRc = StoreData ( pclData, NUM_BPSR_UPD_FIELDS, pcgBPSR_UPD_TagName, 
                           pcgBPSR_UPD_DBField, "IBUTAB", pclUrno );
        dbg( DEBUG, "<%s> IBUTAB ilRc <%d>", pclFunc, ilRc );
    }
    else if( ipUpdType == BPTRPTS )
    {
        ilRc = StoreData ( pclData, NUM_BPTRPTS_FIELDS, pcgBPTRPTS_TagName, 
                           pcgBPTRPTS_DBField, "IBRTAB", pclUIBR );
        dbg( DEBUG, "<%s> IBRTAB ilRc <%d>", pclFunc, ilRc );
        if( ilRc == RC_FAIL )
            return;
 
        ilUntagCnt = 1;
        memset( pclOneRec, 0, sizeof(pclOneRec) );
        while( xml_untag( pclOneRec, pclData, "BPTRDET", ilUntagCnt ) == TRUE )
        {
            dbg( DEBUG, "<%s> BPTRDET Cnt <%d> pclOneRec <%s>", pclFunc, ilUntagCnt, pclOneRec );
            ilUntagCnt++;
            ilRc = StoreData ( pclOneRec, NUM_BPTRDET_FIELDS, pcgBPTRDET_TagName, 
                               pcgBPTRDET_DBField, "IBDTAB", pclUIBR );
            dbg( DEBUG, "<%s> IBDTAB ilRc <%d>", pclFunc, ilRc );
            if( ilRc == RC_FAIL )
                return;
        }
    }
    else if( ipUpdType == ASRPTS )
    {
        ilUntagCnt = 1;
        memset( pclOneRec, 0, sizeof(pclOneRec) );
        while( xml_untag( pclOneRec, pclData, "ASR", ilUntagCnt ) == TRUE )
        {
            dbg( DEBUG, "<%s> ASR Cnt <%d> pclOneRec <%s>", pclFunc, ilUntagCnt, pclOneRec );
            ilUntagCnt++;
            ilRc = StoreData ( pclOneRec, NUM_ASRPTS_FIELDS, pcgASRPTS_TagName, 
                               pcgASRPTS_DBField, "IARTAB", pclUrno );
            dbg( DEBUG, "<%s> IARTAB ilRc <%d>", pclFunc, ilRc );
            if( ilRc == RC_FAIL )
                return;
        }
    }
    else
    {
        MQMsgUpd( lpMqrUrno, MQ_STATUS_ERR ); 
        return;
    }
    MQMsgUpd( lpMqrUrno, MQ_STATUS_OK ); 
}

int StoreData( char *pcpData, int ipNumFields, char **pcpTagName, char **pcpDBField, 
               char *pcpTableName, char *pcpUrno )
{
    IBUREC rlIburec;
    int ilFound = 0;
    int ili;
    int ilRc;
    int ilUAFTFound = FALSE;
    int ilULDIFound = FALSE;
    int ilTableType = 0;
    int ilUrno;
    short slSqlFunc = 0;
    char pclSqlBuf[DBQRLEN] = "\0";
    char pclSqlValues[DBQRLEN] = "\0";
    char pclSqlUpd[DBQRLEN] = "\0";
    char pclSqlFields[DBQRLEN] = "\0";
    char pclTmpStr[DBQRLEN] = "\0";
    char pclValueStr[DBQRLEN] = "\0"; /* Max value length is 1000 */
    char pclData[MQRDLEN] = "\0";
    char pclBegin[20] = "\0";
    char pclEnd[20] = "\0";
    char pclUrno[URNOLEN+1] = "\0";       
    char pclUAFT[URNOLEN+1] = "\0";       
    char pclULDI[100] = "\0";       
    char *pclFunc = "StoreData";
  
    dbg( TRACE, "<%s> NumFields <%d> TableName <%s>", pclFunc, ipNumFields, pcpTableName );
    memcpy( pclData, pcpData, strlen(pcpData) );

    if( !strncmp(pcpTableName, "IBDTAB", 6) )
        ilTableType = BPTRDET;

    for( ili = 0; ili < ipNumFields; ili++ )
    {
        dbg( DEBUG, "<%s> Loop <%d> pcpTagName <%s>", pclFunc, ili, pcpTagName[ili] );
        sprintf( pclBegin, "<%s>", pcpTagName[ili] );
        sprintf( pclEnd, "</%s>", pcpTagName[ili] );
        ilFound = GetElementValue( pclValueStr, pclData, pclBegin, pclEnd );
        if( ilFound == FALSE )
        {
            dbg( TRACE, "<%s> Tag <%s> is missing", pclFunc, pcpTagName[ili] );
            return RC_FAIL;
        }
        strip_special_char( pclValueStr );

        /* DB Fields */
        if( ili == (ipNumFields-1) )
            sprintf( pclTmpStr, "%s", pcpDBField[ili] );
        else
            sprintf( pclTmpStr, "%s,", pcpDBField[ili] );

        strcat( pclSqlFields, pclTmpStr );
        dbg( DEBUG, "<%s> SqlFields <%s>", pclFunc, pclSqlFields );


        /* Field Values */
        if( ili == (ipNumFields-1) )
            sprintf( pclTmpStr, "\'%s\'", pclValueStr );
        else
            sprintf( pclTmpStr, "\'%s\',", pclValueStr );
        strcat( pclSqlValues, pclTmpStr );
        dbg( DEBUG, "<%s> SqlValues <%s>", pclFunc, pclSqlValues );

         
        /* In case it is an update */
        if( ili < (ipNumFields-1) )
            sprintf( pclTmpStr, "%s = '%s',", pcpDBField[ili], pclValueStr );
        else
            sprintf( pclTmpStr, "%s = '%s'", pcpDBField[ili], pclValueStr );

        if( ili == 0 )
            strcpy( pclSqlUpd, pclTmpStr );
        else
            strcat( pclSqlUpd, pclTmpStr );


        /* Check whether record exist, base on UAFT only */
        if( ilUAFTFound == FALSE && !strncmp( pcpTagName[ili], "UAFT", 4 ) )
        {
            if( strlen(pclValueStr) >= URNOLEN )
                pclValueStr[URNOLEN] = '\0';
            strcpy( pclUAFT, pclValueStr );
            ilUAFTFound = TRUE;
            dbg( TRACE, "<%s> UAFT <%s>", pclFunc, pclUAFT );
        }
        
        if( ilTableType == BPTRDET && ilULDIFound == FALSE && 
            !strncmp( pcpTagName[ili], "ULDNO", 5 ) )
        {
            if( strlen(pclValueStr) >= 60 )
                pclValueStr[59] = '\0';
            strcpy( pclULDI, pclValueStr );
            ilULDIFound = TRUE;
            dbg( TRACE, "<%s> ULDI <%s>", pclFunc, pclULDI );
        }
    }

    /* This table requires validation on UAFT and ULDN */
    if( ilTableType == BPTRDET )
        sprintf( pclSqlBuf, "SELECT URNO FROM %s WHERE UAFT = '%s' AND ULDI = '%s'", pcpTableName, pclUAFT, pclULDI );
    else
        sprintf( pclSqlBuf, "SELECT URNO FROM %s WHERE UAFT = '%s'", pcpTableName, pclUAFT );
    dbg( TRACE, "<%s> Find rec SqlBuf <%s>", pclFunc, pclSqlBuf );
    slSqlFunc = START;
    ilRc = getOrPutDBData(pclSqlBuf,pclUrno,slSqlFunc);
     
    /* Update record */
    if( ilRc == DB_SUCCESS )
    {
        if( strlen(pclUrno) >= URNOLEN )
            pclUrno[URNOLEN] = '\0';
        dbg( TRACE,"<%s> URNO <%s>",pclFunc, pclUrno );

        sprintf( pclSqlBuf, "UPDATE %s SET %s WHERE URNO = '%s'", pcpTableName, pclSqlUpd, pclUrno );
        dbg( TRACE,"<%s> UPD SqlBuf <%s>",pclFunc, pclSqlBuf );
      
        slSqlFunc = START;
        ilRc = getOrPutDBData(pclSqlBuf,pclData,slSqlFunc);
        dbg( TRACE,"<%s> UPD ilRc <%d>",pclFunc, ilRc );
        return RC_SUCCESS;
    }
  
    /* Insert record */
    /*if( GetNextValues( pclUrno, 1 ) != RC_SUCCESS )*/
    ilUrno = NewUrnos( pcpTableName, 1 );
    if( ilUrno == DB_ERROR )
    {
        dbg(TRACE, "<%s> Fail to get URNO", pclFunc );
        return RC_FAIL;
    } 
    sprintf( pclUrno, "%d", ilUrno );
    dbg( TRACE, "<%s> URNO <%s>", pclFunc, pclUrno );
    if( ilTableType == BPTRDET )
        sprintf(pclSqlBuf,"INSERT INTO %s ( URNO, UIBR, %s) VALUES ( '%s', '%s', %s)", 
                       pcpTableName, pclSqlFields, pclUrno, pcpUrno, pclSqlValues ); 
    else
        sprintf(pclSqlBuf,"INSERT INTO %s ( URNO, %s) VALUES ( '%s', %s)", 
                       pcpTableName, pclSqlFields, pclUrno, pclSqlValues ); 
    
    if( ilTableType != BPTRDET )
        memcpy( pcpUrno, pclUrno, strlen(pclUrno) );
    dbg( TRACE, "<%s> pclSqlBuf <%s>", pclFunc, pclSqlBuf );

    slSqlFunc = START;
    ilRc = getOrPutDBData(pclSqlBuf,pclData,slSqlFunc);
    dbg( DEBUG,"<%s> INSERT ilRc = %d",pclFunc, ilRc );

    return ilRc;

} /*UpdateTable*/

static void GenerateReport(long lpMqrUrno, char *pcpData, int ipRepType)
{
    IBUREC rlIburec;
    int ilRc = RC_SUCCESS;
    int ilFound = 0;
    int ili;
    int ilNumFlight = 0;
    int ilNumLightLoader = 0;
    int ilNumHeavyLoader = 0;
    int ilNumBagFirstTimingMet = 0;
    int ilNumBagLastTimingMetLightLoader = 0;
    int ilNumBagLastTimingMetHeavyLoader = 0;
    int ilNumBagLastTimingMet = 0;
    short slSqlFunc = 0;
    short slSqlCursor = 0;
    char pclSqlBuf[DBQRLEN] = "\0";
    char pclWhere[DBQRLEN] = "\0";
    char pclTmpStr[DBQRLEN] = "\0";
    char pclData[MQRDLEN] = "\0";    
    char pclSessionID[51] = "\0";    
    char pclTerminalID[3] = "\0";    
    char pclFromDT[DATELEN+1] = "\0";    
    char pclToDT[DATELEN+1] = "\0";    
    char *pclFunc = "GenerateReport";
 
    memcpy( pclData, pcpData, strlen(pcpData) );

    if( ipRepType == BPSR_REQ )
    {
        ilFound = GetElementValue( pclSessionID, pclData, "<SID>", "</SID>" );
        if( ilFound == FALSE )
        {
            dbg(TRACE,"<%s> Tag <SID> Msg:%s",pclFunc, pclData);
            MQMsgUpd( lpMqrUrno, MQ_STATUS_ERR );
            return;
        }
        ilFound = GetElementValue( pclTerminalID, pclData, "<T>", "</T>" );
        if( ilFound == FALSE )
        {
            dbg(TRACE,"<%s> Tag <T> Msg:%s",pclFunc, pclData);
            MQMsgUpd( lpMqrUrno, MQ_STATUS_ERR );
            return;
        }
        ilFound = GetElementValue( pclFromDT, pclData, "<FR>", "</FR>" );
        if( ilFound == FALSE )
        {
            dbg(TRACE,"<%s> Tag <FR> Msg:%s",pclFunc, pclData);
            MQMsgUpd( lpMqrUrno, MQ_STATUS_ERR );
            return;
        }
        ilFound = GetElementValue( pclToDT, pclData, "<TO>", "</TO>" );
        if( ilFound == FALSE )
        {
            dbg(TRACE,"<%s> Tag <TO> Msg:%s",pclFunc, pclData);
            MQMsgUpd( lpMqrUrno, MQ_STATUS_ERR );
            return;
        }

        memset( pcgXmlStr, 0, sizeof(pcgXmlStr) );
        memset( &rlIburec, 0, sizeof(IBUREC) );

        sprintf( pclWhere, "WHERE ADID = 'A' AND STDT >= '%s' AND STDT <= '%s'", pclFromDT, pclToDT );
        if( pclTerminalID[0] != '*' )
        {
            sprintf( pclTmpStr, " AND TGID = '%s'", pclTerminalID );
            strcat( pclWhere, pclTmpStr );
        }
        dbg( TRACE, "<%s> pclWhere <%s>", pclFunc, pclWhere );
        sprintf( pclSqlBuf, "SELECT LDTY, BFTM, BLTM FROM IBUTAB %s", pclWhere );
        dbg( TRACE, "<%s> pclSqlBuf <%s>", pclFunc, pclSqlBuf );

        slSqlFunc = START;
        slSqlCursor = 0;
        memset( pclData, 0, sizeof(pclData) );
        while( sql_if(slSqlFunc, &slSqlCursor, pclSqlBuf, pclData ) == DB_SUCCESS )
        {
            slSqlFunc = NEXT;
            ilNumFlight++;
            get_fld( pclData, FIELD_1, STR, STATLEN, rlIburec.LDTY ); 
            get_fld( pclData, FIELD_2, STR, STATLEN, rlIburec.BFTM ); 
            get_fld( pclData, FIELD_3, STR, STATLEN, rlIburec.BLTM ); 
            if( rlIburec.LDTY[0] == 'L' )      ilNumLightLoader++;
            else if( rlIburec.LDTY[0] == 'H' ) ilNumHeavyLoader++;
            if( rlIburec.BFTM[0] == 'P' )      ilNumBagFirstTimingMet++;
            if( rlIburec.BLTM[0] == 'P' )      ilNumBagLastTimingMet++;
            if( rlIburec.LDTY[0] == 'L' && rlIburec.BLTM[0] == 'P' )      ilNumBagLastTimingMetLightLoader++;
            if( rlIburec.LDTY[0] == 'H' && rlIburec.BLTM[0] == 'P' )      ilNumBagLastTimingMetHeavyLoader++;
        }
        close_my_cursor( &slSqlCursor );
    }
    MQMsgUpd( lpMqrUrno, MQ_STATUS_OK );
    
    sprintf( pcgXmlStr, "<BPSR>"
                        "<SID>%s</SID>"
                        "<T>%s</T>"
                        "<FR>%s</FR>"
                        "<TO>%s</TO>"
                        "<TOT>%d</TOT>"
                        "<CLL>%d</CLL>"
                        "<CLH>%d</CLH>"
                        "<CBF>%d</CBF>"
                        "<CBLL>%d</CBLL>"
                        "<CBLH>%d</CBLH>"
                        "<CBL>%d</CBL>"
                        "</BPSR>", pclSessionID, pclTerminalID, pclFromDT, pclToDT, ilNumFlight,
                                   ilNumLightLoader, ilNumHeavyLoader, ilNumBagFirstTimingMet,
                                   ilNumBagLastTimingMetLightLoader, ilNumBagLastTimingMetHeavyLoader,
                                   ilNumBagLastTimingMet );

    dbg( TRACE, "<%s> XML <%s>", pclFunc, pcgXmlStr );

    ilRc = SendCedaEvent(igItkRptModid,0, " ", " ", " ", " ","ITK", " ", " "," ", pcgXmlStr, "", 4, 0) ;
    dbg (TRACE, "<%s> ilRc for SendCedaEvent = %d", pclFunc, ilRc );


} /*GenerateReport */

static int TriggerAction(char *pcpTableName)
{
  int           ilRc          = RC_SUCCESS ;    /* Return code */
  EVENT        *prlEvent      = NULL ;
  BC_HEAD      *prlBchead     = NULL ;
  CMDBLK       *prlCmdblk     = NULL ;
  /*ACTIONConfig *prlAction     = NULL ;*/
    ACTIONConfig rlAction;
    
  char         *pclSelection  = NULL ;
  char         *pclFields     = NULL ;
  char         *pclData       = NULL ;
  long          llActSize     = 0 ;
  int           ilAnswerQueue = 0 ;
  char          clQueueName[16] ;
    
  if(ilRc == RC_SUCCESS)
  {

  }/* end of if */
    

  if (ilRc == RC_SUCCESS)
  {
   sprintf (&clQueueName[0],"%s2", mod_name) ;

   ilRc = GetDynamicQueue(&ilAnswerQueue,&clQueueName[0]);
   if (ilRc != RC_SUCCESS)
   {
    dbg(TRACE,"TriggerAction: GetDynamicQueue failed <%d>",ilRc);
   }
   else
   {
    dbg(DEBUG,"TriggerAction: got <%d> for <%s>",ilAnswerQueue, &clQueueName[0]);
   }/* end of if */
  }/* end of if */
    
  if (ilRc == RC_SUCCESS)
  {
   llActSize = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + sizeof(ACTIONConfig) + 666;
   prgOutEvent = realloc(prgOutEvent,llActSize);
   if(prgOutEvent == NULL)
   {
    dbg(TRACE,"TriggerAction: realloc out event <%d> bytes failed",llActSize);
    ilRc = RC_FAIL ;
   } /* end of if */
  } /* end of if */

  if(ilRc == RC_SUCCESS)
  {
   prlBchead     = (BC_HEAD *) ((char *) prgOutEvent + sizeof(EVENT)) ;
   prlCmdblk     = (CMDBLK *)  ((char *) prlBchead->data) ;
   pclSelection  = prlCmdblk->data ;
   pclFields     = pclSelection + strlen (pclSelection) + 1 ;
   pclData       = pclFields + strlen (pclFields) + 1 ;
   strcpy (pclData, "DYN") ;
    
        memset(&rlAction,0,sizeof(ACTIONConfig));
        
    prlBchead->rc = RC_SUCCESS;
    
   prgOutEvent->type        = USR_EVENT ;
   prgOutEvent->command     = EVENT_DATA ;
   prgOutEvent->originator  = ilAnswerQueue ;
   prgOutEvent->retry_count = 0 ;
   prgOutEvent->data_offset = sizeof(EVENT) ;
   prgOutEvent->data_length = llActSize-sizeof(EVENT) ;

   rlAction.iEmptyFieldHandling = iDELETE_ALL_BLANKS ;
   rlAction.iIgnoreEmptyFields = 0 ;
   strcpy(rlAction.pcSndCmd, "") ;
   sprintf(rlAction.pcSectionName,"%s_%s", mod_name,pcpTableName) ;
   strcpy(rlAction.pcTableName, pcpTableName) ;
   strcpy(rlAction.pcFields, "-") ;
     /*********************** MCU TEST ***************/
   strcpy(rlAction.pcFields, "") ;
     /*********************** MCU TEST ***************/
     /************/
     if (!strcmp(pcpTableName,"SPRTAB"))
        {
    strcpy(rlAction.pcFields, "PKNO,USTF,ACTI,FCOL") ;
     }
     /************/
   strcpy(rlAction.pcSectionCommands, "IRT,URT,DRT") ;
   rlAction.iModID = mod_id ;

   /****************************************
     DebugPrintEvent(DEBUG,prgOutEvent) ;
     DebugPrintBchead(DEBUG,prlBchead) ;
     DebugPrintCmdblk(DEBUG,prlCmdblk) ;
     dbg (DEBUG,"TriggerAction: selection <%s>",pclSelection) ;
     dbg (DEBUG,"TriggerAction: fields    <%s>",pclFields) ;
     dbg (DEBUG,"TriggerAction: data      <%s>",pclData) ;
     DebugPrintACTIONConfig(DEBUG,&rlAction) ;
   ****************************************/

   if(ilRc == RC_SUCCESS)
   {
    rlAction.iADFlag = iDELETE_SECTION ;
             memcpy(pclData + strlen(pclData) + 1,&rlAction,sizeof(ACTIONConfig));

    ilRc = que(QUE_PUT, 7400, ilAnswerQueue, PRIORITY_3, llActSize, (char *) prgOutEvent) ; 
    if(ilRc != RC_SUCCESS)
    {
     dbg(TRACE,"TriggerAction: QUE_PUT failed <%d>",ilRc) ;
    }
    else
    {
     ilRc = WaitAndCheckQueue(10, ilAnswerQueue,&prgItem) ;
     if(ilRc != RC_SUCCESS)
     {
      dbg(TRACE,"TriggerAction: WaitAndCheckQueue failed <%d>",ilRc) ;
     }
     else
     {
      /* return value for delete does not matter !!! */
        
      /****************************************
      prlEvent     = (EVENT *)   ((char *)prgItem->text) ;
      prlBchead    = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT)) ;
      prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data) ;
      pclSelection = (char *)    prlCmdblk->data ;
      pclFields    = (char *)    pclSelection + strlen(pclSelection) + 1;
      pclData      = (char *)    pclFields + strlen(pclFields) + 1;
      DebugPrintItem(DEBUG, prgItem) ;
      DebugPrintEvent(DEBUG, prlEvent) ;
      DebugPrintBchead(DEBUG, prlBchead) ;
      DebugPrintCmdblk(DEBUG, prlCmdblk) ;
      dbg(DEBUG,"TriggerAction: selection <%s>",pclSelection) ;
      dbg(DEBUG,"TriggerAction: fields    <%s>",pclFields) ;
      dbg(DEBUG,"TriggerAction: data      <%s>",pclData) ;
      ****************************************/
    } /* end of if */
   }/* end of if */
  }/* end of if */

  if(ilRc == RC_SUCCESS)
  {
    rlAction.iADFlag = iADD_SECTION ;
      memcpy(pclData + strlen(pclData) + 1,&rlAction,sizeof(ACTIONConfig));
         
   ilRc = que(QUE_PUT, 7400, ilAnswerQueue, PRIORITY_3, llActSize,
                                              (char *) prgOutEvent ) ;
   if(ilRc != RC_SUCCESS)
   {
    dbg(TRACE,"TriggerAction: QUE_PUT failed <%d>",ilRc) ;
   }
   else
   {
    ilRc = WaitAndCheckQueue(10,ilAnswerQueue,&prgItem) ;
    if(ilRc != RC_SUCCESS)
    {
     dbg(TRACE,"TriggerAction: WaitAndCheckQueue failed <%d>",ilRc) ;
    }
    else
    {
     prlEvent     = (EVENT *)   ((char *)prgItem->text) ;
     prlBchead    = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT)) ;
     prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data) ;
     pclSelection = (char *)    prlCmdblk->data ;
     pclFields    = (char *)    pclSelection + strlen(pclSelection) + 1 ;
     pclData      = (char *)    pclFields + strlen(pclFields) + 1 ;

     if(strcmp(pclData,"SUCCESS") != 0)
     {
      dbg(TRACE,"TriggerAction: add dynamic action config failed") ;
      DebugPrintItem(DEBUG,prgItem) ;
      DebugPrintEvent(DEBUG,prlEvent) ;
      DebugPrintBchead(DEBUG,prlBchead) ;
      DebugPrintCmdblk(DEBUG,prlCmdblk) ;
      dbg (DEBUG,"TriggerAction: selection <%s>",pclSelection) ;
      dbg (DEBUG,"TriggerAction: fields    <%s>",pclFields) ;
      dbg(DEBUG,"TriggerAction: data      <%s>",pclData) ;
      ilRc = RC_FAIL ;
     }
     else
     {
      dbg(DEBUG,"TriggerAction: add dynamic action config OK") ;
     }/* end of if */
    }/* end of if */
   }/* end of if */
  }/* end of if */

  ilRc = que(QUE_DELETE,ilAnswerQueue,ilAnswerQueue,0,0,0) ;
  if(ilRc != RC_SUCCESS)
  {
   dbg(TRACE,"que QUE_DELETE <%d> failed <%d>",ilAnswerQueue, ilRc) ;
  }
  else
  {
   dbg(DEBUG,"TriggerAction: queue <%d> <%s> deleted", ilAnswerQueue, &clQueueName[0]) ;
  }/* end of if */
 }/* end of if */

 return (ilRc) ;
    
}
static int WaitAndCheckQueue(int ipTimeout, int ipModId, ITEM **prpItem)
{
    int ilRc          = RC_SUCCESS;                      /* Return code */
    int ilWaitCounter = 0;

    do
    {
        sleep(1);
        ilWaitCounter += 1;

        ilRc = CheckQueue(ipModId,prpItem);
        if(ilRc != RC_SUCCESS)
        {
            if(ilRc != QUE_E_NOMSG)
            {
                dbg(TRACE,"WaitAndCheckQueue: CheckQueue <%d> failed <%d>",mod_id,ilRc);
            }/* end of if */
        }/* end of if */
    }while((ilRc == QUE_E_NOMSG) && (ilWaitCounter < ipTimeout));

    if(ilWaitCounter >= ipTimeout)
    {
        dbg(TRACE,"WaitAndCheckQueue: timeout reached <%d>",ipTimeout);
        ilRc = RC_FAIL;
    }/* end of if */

    return ilRc;
}


static int CheckQueue(int ipModId, ITEM **prpItem)
{
    int     ilRc       = RC_SUCCESS;                      /* Return code */
    int     ilItemSize = 0;
    EVENT *prlEvent    = NULL;
static  ITEM  *prlItem      = NULL;
    
    ilItemSize = I_SIZE;
    
    ilRc = que(QUE_GETBIGNW,ipModId,ipModId,PRIORITY_3,ilItemSize,(char *) &prlItem);
    if(ilRc == RC_SUCCESS)
    {
        dbg(TRACE,"DebugPrintItem with prlItem follows");
        DebugPrintItem(TRACE,prlItem);
        *prpItem = prlItem;
        dbg(TRACE,"DebugPrintItem with prpItem follows");
        DebugPrintItem(TRACE,*prpItem);
        prlEvent = (EVENT*) ((prlItem)->text);

        switch( prlEvent->command )
        {
            case    HSB_STANDBY :
                dbg(TRACE,"CheckQueue: HSB_STANDBY");
                HandleQueues();
                ctrl_sta = prlEvent->command;
                break;
    
            case    HSB_COMING_UP   :
                dbg(TRACE,"CheckQueue: HSB_COMING_UP");
                ctrl_sta = prlEvent->command;
                break;
    
            case    HSB_ACTIVE  :
                dbg(TRACE,"CheckQueue: HSB_ACTIVE");
                ctrl_sta = prlEvent->command;
                break;
    
            case    HSB_ACT_TO_SBY  :
                dbg(TRACE,"CheckQueue: HSB_ACT_TO_SBY");
                HandleQueues();
                ctrl_sta = prlEvent->command;
                break;
    
            case    HSB_DOWN    :
                dbg(TRACE,"CheckQueue: HSB_DOWN");
                ctrl_sta = prlEvent->command;
                /* Acknowledge the item 
                ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
                if( ilRc != RC_SUCCESS )
                {
                    HandleQueErr(ilRc);
                }  */
                Terminate(30);
                break;
        
        case    HSB_STANDALONE  :
                dbg(TRACE,"CheckQueue: HSB_STANDALONE");
                ctrl_sta = prlEvent->command;
                break;
    
            case    SHUTDOWN    :
                dbg(TRACE,"CheckQueue: SHUTDOWN");
                /* Acknowledge the item */
                ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
                if( ilRc != RC_SUCCESS )
                {
                    /* handle que_ack error */
                    HandleQueErr(ilRc);
                } /* fi */
                Terminate(30);
                break;
    
            case    RESET       :
                ilRc = Reset();
                break;
    
            case    EVENT_DATA  :
                /* DebugPrintItem(DEBUG,prgItem); */
                /* DebugPrintEvent(DEBUG,prlEvent); */
                break;
    
            case    TRACE_ON :
                dbg_handle_debug(prlEvent->command);
                break;
    
            case    TRACE_OFF :
                dbg_handle_debug(prlEvent->command);
                break;


            default         :
                dbg(TRACE,"CheckQueue: unknown event");
                DebugPrintItem(TRACE,prlItem);
                DebugPrintEvent(TRACE,prlEvent);
                break;
    
        } /* end switch */
    
        /* Acknowledge the item */
        ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
        if( ilRc != RC_SUCCESS )
        {
            /* handle que_ack error */
            HandleQueErr(ilRc);
        } /* fi */
    }else{
        if(ilRc != QUE_E_NOMSG)
        {
            dbg(TRACE,"CheckQueue: que(QUE_GETBIG,%d,%d,...) failed <%d>",ipModId,ipModId,ilRc);
            HandleQueErr(ilRc);
        }/* end of if */
    }/* end of if */
       
    return ilRc;
}/* end of CheckQueue */

static int getOrPutDBData(char *pcpSelection, char *pcpData, unsigned int ipMode)
{
  int ilGetRc = DB_SUCCESS;
  short slFkt = 0;
  short slCursor = 0;
  char errBuff[128];

  slFkt = ipMode;
  slCursor = 0;
  ilGetRc = sql_if (slFkt, &slCursor, pcpSelection, pcpData);
  close_my_cursor (&slCursor);
  if (ilGetRc == DB_SUCCESS) {
    return RC_SUCCESS;
  }
  else {
    if (ilGetRc != NOTFOUND) {
      get_ora_err(ilGetRc, &errBuff[0]);
      dbg (TRACE, "<getOrPutDBData> Error getting Oracle data error <%s>", &errBuff[0]);
      return RC_FAIL;
    }
    else  return ilGetRc;
  }
}

/***********************************************************/
/* Implementation of Phase II iTrek                        */
/***********************************************************/

/* Update MQRTAB for error MQ messages */
static void MQMsgUpd( long lpMqrUrno, int ipMQStatus )
{
/*    int ilRc = RC_SUCCESS;
    short slSqlFunc = 0;
    char pclSqlBuf[DBQRLEN] = "\0";
    char pclData[MQRDLEN] = "\0";
    char pclStatus[11] = "\0";

    if( ipMQStatus == MQ_STATUS_OK )
        sprintf( pclStatus, "%s", "OK" );
    else if( ipMQStatus == MQ_STATUS_ERR )
        sprintf( pclStatus, "%s", "ERROR" );
    else
        sprintf( pclStatus, "%s", "UNKNOWN" );

    slSqlFunc = START;
    sprintf( pclSqlBuf,"UPDATE MQRTAB SET STAT='%s' WHERE URNO = %d", pclStatus, lpMqrUrno );
    ilRc = getOrPutDBData( pclSqlBuf,pclData,slSqlFunc );
    dbg( DEBUG,"%05d: upd DB ilRc = %d: <%s>",__LINE__, ilRc, pclSqlBuf );
*/
}

static int GetElementValue(char *pclDest, char *pclOrigData, char *pclBegin, char *pclEnd)
{
        char *pclTemp = NULL;
        long llSize = 0;
        char clBuffer[200] = "\0";

        pclTemp = CedaGetKeyItem(pclDest, &llSize, pclOrigData, pclBegin, pclEnd, TRUE);
        if(pclTemp != NULL)
        {
                return TRUE;
        }
        else
        {
                return FALSE;
        }
}

