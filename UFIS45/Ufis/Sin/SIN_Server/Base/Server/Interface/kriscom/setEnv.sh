# ######################## (c) DATALEX PLC #########################
# 
# setEnv.bat
#
# Configures Environment options for the Datalex Booking Engine.
#
# Author: RK.
#
# $Lock$
# $Log: Ufis/Sin/SIN_Server/Base/Server/Interface/kriscom/setEnv.sh  $
# Revision 1.1 2003/08/12 13:04:31SGT heb 
# Initial revision
# Member added to project /usr1/projects/UFIS4.5/SIN_Server.pj
# Revision 1.4  2001/08/02 10:43:25  maevel
# Added availopt.jar.
# Revision 1.3  2001/07/19 10:16:20  andrew
# changed to use classes12.zip oracle jdbc drive in classpath
# Revision 1.2  2001/05/21 10:00:23  ROBERTK
# generalised servername settings.
# Revision 1.1  2001/05/21 08:26:34  ROBERTK
# Initial revision
# Revision 1.3  2001/05/15 09:54:59  ROBERTK
# Updated classpaths for Avis
# Revision 1.2  2001/04/12 08:24:07  ROBERTK
# ##################################################################

# ##########   NB: THE FOLLOWING PARAMETERS MUST BE   ##########

# ########## HAS & Lookup parameters:
HOSTNAME='siocdev'
export HOSTNAME
JAVA_HOME=/prod/applc/jdk1.3
export JAVA_HOME

BIC_HOME=/prod/applc/datalex/jbookit
export BIC_HOME
HAS_HOME=$BIC_HOME/HostAccessServer
export HAS_HOME

ARG=`cat $HAS_HOME/has.properties | grep jiniGroups`

JINI_GROUP=`perl $HAS_HOME/getJINI.pl $ARG`
export JINI_GROUP
echo $JINI_GROUP

ADMIN_EMAIL=bernard_wai@singaporeair.com.sg
SMTP_MAIL=smtp.sq.com.sg
export ADMIN_EMAIL SMTP_MAIL

# ##########    ##########    ##########    ##########    ##########

#  ########## The parameters that follow are suitable for most installations. #

#  ########## PATHS:
JAVA_DIR=$JAVA_HOME/bin

HAS_LIBPATH=$HAS_HOME/lib
export HAS_LIBPATH

POLICYFILE=$HAS_HOME/policy.all
export POLICYFILE

PORT=8088
export PORT

#  ########## JAVA_OPTIONS:
# JAVA_OPTIONS:
#     Options that are passed to all java apps- web.bat, lookup.bat, runhas.bat
#     eg: For JINI 1.1 on NT with 2#nic require -Dnet.jini.discovery.interface=ip_of_nic_to_webserver
JAVA_OPTIONS="-Djava.security.policy=$POLICYFILE" 
export JAVA_OPTIONS

JAVA_HEAP="-ms256m -mx512m"
export JAVA_HEAP

#  ########## HAS_OPTIONS_DEFAULT:
# HAS_OPTIONS: HAS specific -D options
#HAS_OPTIONS_DEFAULT="-DhasGroup=$JINI_GROUP $HAS_DEBUG $HAS_FARES_OPTIONS $HAS_Threads $HAS_CacheFile"
#export HAS_OPTIONS_DEFAULT

#  ########## HAS_CLASSPATH:
HOSTS=$HAS_LIBPATH/travelservice.jar\
:$HAS_LIBPATH/pars.jar\
:$HAS_LIBPATH/bookitfares.jar\
:$HAS_LIBPATH/itinerary.jar\
:$HAS_LIBPATH/availopt.jar

JINI=$HAS_LIBPATH/jini-core.jar\
:$HAS_LIBPATH/jini-ext.jar\
:$HAS_LIBPATH/reggie-dl.jar\
:$HAS_LIBPATH/sun-util.jar

DATABASE=$HAS_LIBPATH/tds.jar\
:$HAS_LIBPATH/DEP.jar\
:$HAS_LIBPATH/mwi.jar\
:$HAS_LIBPATH/mwdas.jar\
:$HAS_LIBPATH/xml4j.jar\
:$HAS_LIBPATH/hasconfig.jar\
:$HAS_LIBPATH/HASConfigBean.jar\
:$HAS_LIBPATH/dasbeans.jar\
:$HAS_LIBPATH/hasstatistics.jar\
:$HAS_LIBPATH/dopeitineraries.jar\
:$HAS_LIBPATH/classes12.zip

HASCORE=$HAS_LIBPATH/hascore.jar\
:$HAS_LIBPATH/connectionframework.jar\
:$HAS_LIBPATH/hasconnectionservices.jar

CONNECTIONS=$HAS_LIBPATH/hasmatipconnectionserviceimpl.jar\
:$HAS_LIBPATH/japi.jar\
:$HAS_LIBPATH/hasbookitfaresconnectionserviceimpl.jar

OTHERS=$HAS_LIBPATH/DUP.jar\
:$HAS_LIBPATH/mail.jar\
:$HAS_LIBPATH/activation.jar\
:$HAS_LIBPATH/jsapi.jar\
:$HAS_LIBPATH/jsse.jar\
:$HAS_LIBPATH/jcert.jar\
:$HAS_LIBPATH/jnet.jar\
:$HAS_LIBPATH/AvisCarDataBean.jar\
:$HAS_LIBPATH/xerces.jar\
:$HAS_LIBPATH/hasadmin.jar

BIP=$HAS_LIBPATH/BookItPro.jar\
:$HAS_LIBPATH/terminalservice.jar\
:$HAS_LIBPATH/connectorservice.jar\
:$BIC_HOME/lib/acme.jar\
:$BIC_HOME/lib/dasstorefront.jar\
:$BIC_HOME/lib/GenStatsBean.jar\
:$BIC_HOME/lib/activation.jar\
:$BIC_HOME/lib/dastoursitin.jar\
:$BIC_HOME/lib/helper.jar\
:$BIC_HOME/lib/dasprofiles.jar\
:$BIC_HOME/lib/dastours.jar\
:$BIC_HOME/lib/dopegenstats.jar

DREAMSTREAM=$HAS_LIBPATH/rulesRuntime.jar\
:$HAS_LIBPATH/inputrules.jar\
:$HAS_LIBPATH/rulestravelservice.jar\
:$HAS_LIBPATH/compiler.jar\
:$HAS_LIBPATH/gnu-regexp-1.1.3.jar\
:$HAS_LIBPATH/xalan.jar\
:$HAS_LIBPATH/castor-Datalex-0.9.jar

LOGGING=$HAS_LIBPATH/logging.jar

HAS_CLASSPATH=$HOSTS\
:$JINI\
:$DATABASE\
:$HASCORE\
:$CONNECTIONS\
:$OTHERS\
:$BIP\
:$DREAMSTREAM\
:$LOGGING

export HAS_CLASSPATH
export CLASSPATH=$HAS_CLASSPATH:/prod/applc/ufis/kriscom:/prod/applc/jdk1.3/src.jar


