#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/Sin/SIN_Server/Base/Server/Kernel/ntisch.c 1.1 2008/03/27 16:51:06SGT fei Exp  $";
#endif /* _DEF_mks_version */
/*****************************************************************************/
/*                                                                           */
/* CCS Program Skeleton                                                      */
/*                                                                           */
/* Author         :                                                          */
/* Date           :                                                          */
/* Description    :                                                          */
/*                                                                           */
/* Update history : 05.03.98 Update der UserData:                            */
/*                           Falls UserData vorhanden sind werden nur noch   */
/*                           diese zum Ziel geschickt. Es wird keine eigene  */
/*                           Event-Structur davor aufgebaut!!!               */
/*                                                                           */
/* rkl 16.05.00     read initial debug_level from configfile                 */
/*                  init of eventpointer must be set after QUE_GETBIG        */
/* 20020628 JIM:    test for memory leak: allocation should be done in que   */
/* 20040810 jim:    removed sccs_ version string                             */
/* 20080319 MEI:    implement sec_o_min for type=rel                                                                          */
/*****************************************************************************/
/* This program is a CCS main program */
/*                                                                           */
/* source-code-control-system version string                                 */
/* be carefule with strftime or similar functions !!!                        */
/*****************************************************************************/
/******************************************************************************//* FORMAT OF BINARY-FILE...
|-------------------------|------------------|------------------|--------------
|       FileHeader        |   RecordHeader   |       Record     | Record Header
| Length, Counter, LastID | RecordID, Length |    (User Data)   |
|-------------------------|------------------|------------------|--------------

The FileHeader includes the
- Length of file without fileheader
- Counter, total number of items
- ID of next record

The RecordHeader includes the
- RecordID, a unique ID of this record
- Length of this record

The Record includes ths specific user data (e.g. Event-, Cmdblk-Structures 
sending to a other process).
*******************************************************************************/

#define     U_MAIN
#define     UGCCS_PRG
#define     STH_USE

/* The master header file */
#include  "ntisch.h"

/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE *outp       = NULL;*/
int  debug_level = TRACE;

/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */

/******************************************************************************/
/* my global variables                                                        */
/******************************************************************************/
static TNTMain          rgTM;
static char             pclCfgFile[iMIN_BUF_SIZE];
static char             pcgProcessName[iMIN];
static char             pcgBinaryFileName[iMIN_BUF_SIZE];
static char             pcgTmpFileName[iMIN_BUF_SIZE];
static char             pcgCfgFileName[iMIN_BUF_SIZE];
static char             pcgHomeAP[iMIN];
static char             pcgTABEnd[iMIN];
static int              igLastCtrlSta;
static TimerHandler fpTimerHandler[6];  

/******************************************************************************/
/* External functions                                                         */
/******************************************************************************/
extern int  init_que();                          /* Attach to CCS queues */
extern int  que(int,int,int,int,int,char*);      /* CCS queuing routine  */
extern int  send_message(int,int,int,int,char*); /* CCS msg-handler i/f  */
extern int  snap(char *, long, FILE *);
extern int  SendRemoteShutdown(int);

/******************************************************************************/
/* Function prototypes                                                        */
/******************************************************************************/
static int  init_ntisch();
static int  Reset(void);                       /* Reset program          */
static void Terminate(UINT);                   /* Terminate program      */
static void HandleSignal(int);                 /* Handles signals        */
static void HandleErr(int);                    /* Handles general errors */
static void HandleQueErr(int);                 /* Handles queuing errors */
static int  HandleData(void);                  /* Handles event data     */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/

/******************************************************************************/
/* my function prototypes                                                      */
/******************************************************************************/
static int  CheckQueStatus(void);
static int  TimeScheduler(void);
static int  CheckTimer(void);
static int  SendCommand(int, int, int, int, int, char *, int);
static int  AddTimer(FUNCTION *, int);  
static int  DeleteTimer(FUNCTION *, int);   
static int  UpdateTimer(FUNCTION *, int);   
static int  UPDBufTimer(FUNCTION *, int);   
static int  HSBAddTimer(FUNCTION *, int);   
static int  DelModIDTimer(FUNCTION *, int);
static void TransferAllFiles(void);
static void DeleteRemoteBinFile(void);
static void PrintNextEvents(void);
static void DumpBinFile(void);
static int CheckDay(UINT ipCfgDay, int ipCurDay);

/******************************************************************************/
/* something else                                                             */
/******************************************************************************/
static UINT pigDayFlags[8] = {iSUN,iMON,iTUE,iWED,iTHU,iFRI,iSAT,iSUN};
#define IsThisDay(a,b) (((a) & pigDayFlags[(b)]) ? 1 : 0)


/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
    int ilRC;       
    int ilCnt;
    char    pclFileName[512];
    
    /* global initialization */
    INITIALIZE;

    /* read something from sgs.tab */
    memset((void*)pcgTABEnd, 0x00, iMIN);
    if ((ilRC = tool_search_exco_data("ALL", "TABEND", pcgTABEnd)) != RC_SUCCESS)
    {
        dbg(TRACE,"<MAIN> cannot find entry \"TABEND\" in SGS.TAB");
        Terminate(30);
    }
    dbg(DEBUG,"<MAIN> found TABEND: <%s>", pcgTABEnd); 

    /* read HomeAirPort from SGS.TAB */
    memset((void*)pcgHomeAP, 0x00, iMIN);
    if ((ilRC = tool_search_exco_data("SYS", "HOMEAP", pcgHomeAP)) != RC_SUCCESS)
    {
        dbg(TRACE,"<MAIN> cannot find entry \"HOMEAP\" in SGS.TAB");
        Terminate(30);
    }
    dbg(DEBUG,"<MAIN> found HOME-AIRPORT: <%s>", pcgHomeAP); 
    
    /* save process name */
    strcpy(pcgProcessName, argv[0]);

    /* save binary file name and path... */
    pcgBinaryFileName[0] = 0x00;
    sprintf(pcgBinaryFileName, "%s/%s", getenv("CFG_PATH"), sDEFAULT_FNAME);
    dbg(TRACE,"<MAIN> %05d Binary-File: <%s>", __LINE__, pcgBinaryFileName);

    /* save tmp file name and path... */
    pcgTmpFileName[0] = 0x00;
    sprintf(pcgTmpFileName, "%s/%s", getenv("CFG_PATH"), sDEFAULT_FTMP);
    dbg(TRACE,"<MAIN> %05d Tmp-File: <%s>", __LINE__, pcgTmpFileName);

    /* save config file name and path... */
    pcgCfgFileName[0] = 0x00;
    sprintf(pcgCfgFileName, "%s/%s.cfg", getenv("CFG_PATH"), argv[0]);
    dbg(TRACE,"<MAIN> %05d Config-File: <%s>", __LINE__, pcgCfgFileName);

    /* for all signals */
    (void)SetSignals(HandleSignal);
    (void)UnsetSignals();

    /* Allocate a dynamic buffer for max event size */
        /* 20020628 JIM: test for memory leak: allocation should be done in que */
    /*if ((prgItem = (ITEM*)malloc(MAX_EVENT_SIZE)) == NULL)    */
    /*{     */
    /*  dbg(TRACE,"<MAIN> %05d malloc : <%s>",__LINE__, strerror(errno));   */
    /*  prgItem = NULL;     */
    /*  Terminate(30);      */
    /*}             */
    
    /* set event pointer */
    /* prgEvent = (EVENT*)prgItem->text; */
                
    /* Attach to the CCS queues */
    ilCnt = 0;
    do
    {
        ilRC = init_que();
        if(ilRC != RC_SUCCESS)
        {
            sleep(6);
            ilCnt++;
        }
    } while ((ilCnt < 10) && (ilRC != RC_SUCCESS));

    if (ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"<MAIN> %05d init_que failed!", __LINE__);
        Terminate(30);
    }

    /* transfer binary file to remote machine... */
    pclFileName[0] = 0x00;
    sprintf(pclFileName, "%s/%s", getenv("BIN_PATH"), argv[0]);
    dbg(TRACE,"<MAIN> %05d TransferFile: <%s>", __LINE__, pclFileName);
    if ((ilRC = TransferFile(pclFileName)) != RC_SUCCESS)
    {
        dbg(TRACE,"<MAIN> %05d TransferFile <%s> returns: %d", __LINE__, pclFileName, ilRC);
        set_system_state(HSB_STANDALONE);
    }

    /* to copy binary- and ascii configuration file... */
    TransferAllFiles();

    /* ask VBL about this!! */
    dbg(TRACE,"<MAIN> %05d SendRemoteShutdown: %d", __LINE__, mod_id);
    if ((ilRC = SendRemoteShutdown(mod_id)) != RC_SUCCESS)
    {
        dbg(TRACE,"<MAIN> %05d SendRemoteShutdown(%d) returns: %d", __LINE__, mod_id, ilRC);
        set_system_state(HSB_STANDALONE);
    }

    if ((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY) && (ctrl_sta != HSB_STANDBY))
    {
        dbg(TRACE,"<MAIN> %05d waiting for status switch ...", __LINE__);
        HandleQueues();
    }

    if ((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY) || (ctrl_sta == HSB_STANDBY))
    {
        dbg(TRACE,"<MAIN> %05d initializing ...", __LINE__);
        if ((ilRC = init_ntisch()) != RC_SUCCESS)
        {       
            dbg(TRACE,"<MAIN> %05d init_ntisch returns %d", __LINE__, ilRC);
            Terminate(30);
        }   
    } 
    else 
    {
        dbg(TRACE,"<MAIN> %05d wrong ctrl_sta: <%s>",__LINE__,CTRL_STA(ctrl_sta));
        Terminate(30);
    }

    /* only a message */
    dbg(TRACE,"<MAIN> %05d initializing OK", __LINE__);

    /* store control status... */
    igLastCtrlSta = ctrl_sta;

    /* forever */
    while(1)
    {
        switch (ctrl_sta)
        {
            case HSB_ACTIVE:
                /* to copy binary- and ascii configuration file... */
                TransferAllFiles();
            case HSB_STANDBY:
            case HSB_STANDALONE:
            case HSB_ACT_TO_SBY:
                dbg(TRACE,"<MAIN> %05d CTRL-STA: <%s>", __LINE__, CTRL_STA(ctrl_sta));
                TimeScheduler();
                break;

            default:
                dbg(TRACE,"<MAIN> %05d CTRL-STA: <%s>", __LINE__, CTRL_STA(ctrl_sta));
                HandleQueues();
                Reset();
                break;
        }
    }
}


/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int init_ntisch(void)
{
    struct tm rlNextTime;
    struct tm rlCurTime;
    time_t tlCurTime;
    time_t TimeDiff;
    time_t next;
    int i;
    int ilRC;
    int ilSL;
    int ilCurMin;
    int ilCurHour;
    int ilRecordNo;
    int ilNextHour;
    int ilNextMinute;
    int ilCurSection;
    int ilCurrentSection;
    int ilCurAbsDate;
    int ilMaxAbsDate;
    int ilBinaryFileIsCorrupt;
    char *pclS;
    char *pclCfgPath;
    char pclTmpBuf[iMIN_BUF_SIZE];
    char pclSection[iMAX_BUF_SIZE];
    char pclCurrentSection[iMIN_BUF_SIZE];
    char pclDataBuffer[iMAX_BUF_SIZE];
    char pclDay[iMIN];
    char pclMonth[iMIN];
    char pclAbsDate[iMAX_BUF_SIZE];
    char pcgSaveBinaryFileName[iMIN_BUF_SIZE];
    RecordHeader    rlRecordHeader;
    FileHeader rlFileHeader;
    struct tm *prlCurTime = NULL;
    struct tm *_tm = NULL;
    FILE *fh = NULL;
    char *pclRecord = NULL;
    FUNCTION  *prlFunction = NULL;
    DATA_PERIOD *prlDataPeriod = NULL;
    char *pclData = NULL;
    BC_HEAD *prlBCHead = NULL;
    CMDBLK *prlCmdblk = NULL;

    dbg(DEBUG,"<init_ntisch> ====== START ======");

    if ((pclCfgPath = getenv("CFG_PATH")) == NULL)
    {
        dbg(TRACE,"<init_ntisch> %05d cannot run without environment", __LINE__);
        Terminate(30);
    }
    else
    {
        /* copy path to buffer... */
        memset((void*)pclCfgFile, 0x00, iMIN_BUF_SIZE);
        strcpy(pclCfgFile, pclCfgPath);

        /* check last character */
        if (pclCfgFile[strlen(pclCfgFile)-1] != '/')
            strcat(pclCfgFile, "/");

        /* add filename */
        strcat(pclCfgFile, "ntisch.cfg");
        dbg(DEBUG,"<init_ntisch> %05d build filename <%s>", __LINE__, pclCfgFile);

        /* read file... */
        /* time the process should sleep */
        if ((ilRC = iGetConfigEntry(pclCfgFile, "MAIN", "stime", CFG_INT, 
                                    (char*)&rgTM.iSleepTime)) != RC_SUCCESS)
        {
             rgTM.iSleepTime = iDEFAULT_STIME;
        }
        /* -------------------------------------------------------------- */
        /* debug_level setting ------------------------------------------ */
        /* -------------------------------------------------------------- */
        pclTmpBuf[0] = 0x00;
        if ((ilRC = iGetConfigEntry(pclCfgFile, "MAIN", "debug_level", CFG_STRING, pclTmpBuf)) != RC_SUCCESS)
        {
            /* default */
            dbg(TRACE,"<init_ntisch> no debug_level in section <MAIN>");
            strcpy(pclTmpBuf, "OFF");
        }

        /* which debug_level is set in the Configfile ? */
        StringUPR((UCHAR*)pclTmpBuf);
        if (!strcmp(pclTmpBuf,"DEBUG"))
        {
            debug_level = DEBUG;
            dbg(TRACE,"<init_ntisch> debug_level set to DEBUG");
        }
        else if (!strcmp(pclTmpBuf,"TRACE"))
        {
            debug_level = TRACE;
            dbg(TRACE,"<init_ntisch> debug_level set to TRACE");
        }
        else if (!strcmp(pclTmpBuf,"OFF"))
        {
            debug_level = 0;
            dbg(TRACE,"<init_ntisch> debug_level set to OFF");
        }
        else
        {
            debug_level = 0;
            dbg(TRACE,"<init_ntisch> unknown debug_level set to default OFF");
        }

        /* -------------------------------------------------------------- */
        /* first the MAIN section... */
        memset((void*)pclSection, 0x00, iMAX_BUF_SIZE);
        if ((ilRC = iGetConfigEntry(pclCfgFile, "MAIN", "section",CFG_STRING, pclSection)) != RC_SUCCESS)
        {
            dbg(TRACE,"<init_ntisch> %05d cannot run without section in MAIN", __LINE__);
            Terminate(30);
        }

        /* read all sections... */
        rgTM.iNoOfSections = GetNoOfElements(pclSection, ',');
        dbg(DEBUG,"<init_ntisch> %05d found %d sections", __LINE__, rgTM.iNoOfSections);

        /* get memory for sections */
        if ((rgTM.prSection = (TNTSection*)malloc(rgTM.iNoOfSections*sizeof(TNTSection))) == NULL)
        {
            dbg(TRACE,"<init_ntisch> %05d cannot allocate memory for sections...", __LINE__);
            rgTM.prSection = NULL;
            Terminate(30);
        }

        /* for all sections */
        for( ilCurrentSection = 0; ilCurrentSection<rgTM.iNoOfSections; ilCurrentSection++ )
        {
            /* try to read next section... */
            if ((pclS = GetDataField(pclSection, ilCurrentSection, ',')) == NULL)
            {
                dbg(TRACE,"<init_ntisch> %05d error reading section %d...", __LINE__, ilCurrentSection);
                Terminate(30);
            }

            /* store current section name */
            memset((void*)pclCurrentSection, 0x00, iMIN_BUF_SIZE);
            strcpy(pclCurrentSection, pclS);

            dbg(DEBUG,"<init_ntisch> ------------ START <%s> (%d) -----------", pclCurrentSection,ilCurrentSection);
            
            /* set flag */
            rgTM.prSection[ilCurrentSection].iUseInternalEvent = 1;

            /* set valid flag = 1 at init so that section is valid on each startup */
            rgTM.prSection[ilCurrentSection].iValid = 1;

            /* read home-airport */
            if ((ilRC = iGetConfigEntry(pclCfgFile, pclCurrentSection, "home_airport", CFG_STRING, 
                                        rgTM.prSection[ilCurrentSection].pcHomeAirport)) != RC_SUCCESS)
            {
                strcpy(rgTM.prSection[ilCurrentSection].pcHomeAirport, pcgHomeAP);
            }
            dbg(DEBUG,"<init_ntisch> Home-Airport: <%s>", rgTM.prSection[ilCurrentSection].pcHomeAirport);

            /* read table-extension */
            if ((ilRC = iGetConfigEntry(pclCfgFile, pclCurrentSection, "table_extension", CFG_STRING, 
                                        rgTM.prSection[ilCurrentSection].pcTableExtension)) != RC_SUCCESS)
            {
                strcpy(rgTM.prSection[ilCurrentSection].pcTableExtension, pcgTABEnd);
            }
            dbg(DEBUG,"<init_ntisch> Table-Extension: <%s>", rgTM.prSection[ilCurrentSection].pcTableExtension);

            /* mod_id */
            if ((ilRC = iGetConfigEntry(pclCfgFile, pclCurrentSection, "mod_id", CFG_INT, 
                                       (char*)&rgTM.prSection[ilCurrentSection].iDestinationID)) != RC_SUCCESS)
            { 
                dbg(TRACE,"<init_ntisch> %05d cannot run without mod-id", __LINE__);
                Terminate(30);
            }
            /* parent id, only used if ntisch was programmed by other process */
            rgTM.prSection[ilCurrentSection].iParentID = mod_id;

            /* Event-ID */ 
            rgTM.prSection[ilCurrentSection].iEventID = ilCurrentSection;

            /* Function handle it like ADD */
            rgTM.prSection[ilCurrentSection].iFunction = iADD;

            /* Event Command */
            if ((ilRC = iGetConfigEntry(pclCfgFile, pclCurrentSection, "event_command", CFG_INT, 
                                        (char*)&rgTM.prSection[ilCurrentSection].iEventCommand)) != RC_SUCCESS)
            {
                rgTM.prSection[ilCurrentSection].iEventCommand = EVENT_DATA;
            }

            /* absolute date... */
            memset((void*)pclAbsDate, 0x00, iMAX_BUF_SIZE);
            if ((ilRC = iGetConfigEntry(pclCfgFile, pclCurrentSection, "abs_date", CFG_STRING, pclAbsDate)) != RC_SUCCESS)
            {
                rgTM.prSection[ilCurrentSection].iNoOfAbsDate = 0;
            }
            else
            {
                /* here we must handle this */
                if ((ilMaxAbsDate = GetNoOfElements(pclAbsDate, ',')) < 0)
                {
                    rgTM.prSection[ilCurrentSection].iNoOfAbsDate = 0;
                    dbg(TRACE,"<init_ntisch> %05d GetNoOfElements(AbsDate) returns: %d", __LINE__, ilMaxAbsDate);
                    Terminate(30);
                }
                else
                {
                    rgTM.prSection[ilCurrentSection].iNoOfAbsDate = ilMaxAbsDate;
                    dbg(DEBUG,"<init_ntisch> %05d NoOfAbsDate: %d", __LINE__, rgTM.prSection[ilCurrentSection].iNoOfAbsDate);

                    for (ilCurAbsDate=0; ilCurAbsDate<ilMaxAbsDate; ilCurAbsDate++)
                    {
                        if ((pclS = GetDataField(pclAbsDate, ilCurAbsDate, ',')) == NULL)
                        {
                            dbg(TRACE,"<init_ntisch> %05d GetDataField (pclAbsDate) returns NULL", __LINE__);
                            Terminate(30);
                        }

                        /* separate day and month */
                        memset((void*)pclDay, 0x00, iMIN);
                        memset((void*)pclMonth, 0x00, iMIN);
                        if ((ilRC = SeparateIt(pclS, pclDay, pclMonth, '.')) < 0)
                        {
                            dbg(TRACE,"<init_ntisch> %05d SeparateIt (AbsDate) returns: %d", __LINE__, ilRC);
                            Terminate(30);
                        }

                        /* store both */
                        rgTM.prSection[ilCurrentSection].rAbsDate[ilCurAbsDate].iDayOfMonth = atoi(pclDay);
                        rgTM.prSection[ilCurrentSection].rAbsDate[ilCurAbsDate].iMonthOfYear = atoi(pclMonth);

                        dbg(DEBUG,"<init_ntisch> %05d Date: %d <-> Month: %d", __LINE__, 
                                  rgTM.prSection[ilCurrentSection].rAbsDate[ilCurAbsDate].iDayOfMonth, 
                                  rgTM.prSection[ilCurrentSection].rAbsDate[ilCurAbsDate].iMonthOfYear);
                    }
                }
            }

            /* days of week */
            rgTM.prSection[ilCurrentSection].iDaysOfWeek = 0x0000;
            memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
            if ((ilRC = iGetConfigEntry(pclCfgFile, pclCurrentSection, "day_o_week", CFG_STRING, pclTmpBuf)) != RC_SUCCESS)
            {
                rgTM.prSection[ilCurrentSection].iNoOfDOW  = 0;
                dbg(DEBUG,"<init_ntisch> day_o_week: <->");
            }
            else
            {
                /* convert to uppercase */
                StringUPR((UCHAR*)pclTmpBuf);     

                /* count number of elements */
                rgTM.prSection[ilCurrentSection].iNoOfDOW = GetNoOfElements(pclTmpBuf, ',');

                /* for all days... */
                for (i=0; i<rgTM.prSection[ilCurrentSection].iNoOfDOW; i++)
                {
                    /* get day */
                    if ((pclS = GetDataField(pclTmpBuf, i, ',')) == NULL)
                    {
                        dbg(TRACE,"<init_ntisch> %05d error reading day of week %d...", __LINE__, i);
                        Terminate(30);
                    }

                    if (!strcmp(pclS, "SUN"))
                        rgTM.prSection[ilCurrentSection].iDaysOfWeek |= iSUN;
                    else if (!strcmp(pclS, "MON"))
                        rgTM.prSection[ilCurrentSection].iDaysOfWeek |= iMON;
                    else if (!strcmp(pclS, "TUE"))
                        rgTM.prSection[ilCurrentSection].iDaysOfWeek |= iTUE;
                    else if (!strcmp(pclS, "WED"))
                        rgTM.prSection[ilCurrentSection].iDaysOfWeek |= iWED;
                    else if (!strcmp(pclS, "THU"))
                        rgTM.prSection[ilCurrentSection].iDaysOfWeek |= iTHU;
                    else if (!strcmp(pclS, "FRI"))
                        rgTM.prSection[ilCurrentSection].iDaysOfWeek |= iFRI;
                    else if (!strcmp(pclS, "SAT"))
                        rgTM.prSection[ilCurrentSection].iDaysOfWeek |= iSAT;
                    else
                    {
                        /* hae? */
                        dbg(TRACE,"<init_ntisch> %05d unknown day of week <%s>", __LINE__, pclS);
                    }
                }
                dbg(DEBUG,"<init_ntisch> day_o_week: <%x>", rgTM.prSection[ilCurrentSection].iDaysOfWeek);
            }

            /* hour of day */
            memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
            if ((ilRC = iGetConfigEntry(pclCfgFile, pclCurrentSection, "hour_o_day", CFG_STRING, pclTmpBuf)) != RC_SUCCESS)
            {
                rgTM.prSection[ilCurrentSection].iNoOfHOD = 0;
                dbg(DEBUG,"<init_ntisch> hour_o_day: <->");
            }
            else
            {
                /* convert to uppercase */
                StringUPR((UCHAR*)pclTmpBuf);     

                /* count number of elements */
                rgTM.prSection[ilCurrentSection].iNoOfHOD = GetNoOfElements(pclTmpBuf, ',');

                /* for all days... */
                for (i=0; i<rgTM.prSection[ilCurrentSection].iNoOfHOD; i++)
                {
                    /* get hout */
                    if ((pclS = GetDataField(pclTmpBuf, i, ',')) == NULL)
                    {
                        dbg(TRACE,"<init_ntisch> %05d error reading hour of day %d...", __LINE__, i);
                        Terminate(30);
                    }
                    /* store it */
                    rgTM.prSection[ilCurrentSection].piHoursOfDay[i] = atoi(pclS);
                }
                dbg(DEBUG,"<init_ntisch> hour_o_day: <%d>", rgTM.prSection[ilCurrentSection].piHoursOfDay[0]);
            }

            /* min of hour */
            memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
            if ((ilRC = iGetConfigEntry(pclCfgFile, pclCurrentSection, "min_o_hour", CFG_STRING, pclTmpBuf)) != RC_SUCCESS)
            {
                rgTM.prSection[ilCurrentSection].iNoOfMOH = 0;
                dbg(DEBUG,"<init_ntisch> min_o_hour: <->");
            }
            else
            {
                /* convert to uppercase */
                StringUPR((UCHAR*)pclTmpBuf);     

                /* count number of elements */
                rgTM.prSection[ilCurrentSection].iNoOfMOH = GetNoOfElements(pclTmpBuf, ',');

                /* for all days... */
                for (i=0; i<rgTM.prSection[ilCurrentSection].iNoOfMOH; i++)
                {
                    /* get hour */
                    if ((pclS = GetDataField(pclTmpBuf, i, ',')) == NULL)
                    {
                        dbg(TRACE,"<init_ntisch> %05d error reading min of hour %d...", __LINE__, i);
                        Terminate(30);
                    }

                    /* store it */
                    rgTM.prSection[ilCurrentSection].piMinOfHour[i] = atoi(pclS);
                }
                dbg(DEBUG,"<init_ntisch> min_o_hour: <%d>", rgTM.prSection[ilCurrentSection].piMinOfHour[0]);
            }        

            /* reset flags for minute of hour */
            for (i=0; i<iMAX_MINUTES; i++)
                rgTM.prSection[ilCurrentSection].piMinIsSent[i] = 0;

            /* ... and for hours... */
            for (i=0; i<iMAX_HOURS; i++)
                rgTM.prSection[ilCurrentSection].piHourIsSent[i] = 0;

            /****************************************** Mei ***************************************************/
            /* sec of min */
            memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
            if ((ilRC = iGetConfigEntry(pclCfgFile, pclCurrentSection, "sec_o_min", CFG_STRING, pclTmpBuf)) != RC_SUCCESS)
            {
                rgTM.prSection[ilCurrentSection].iSecInterval = 0;
                dbg(DEBUG,"<init_ntisch> sec_o_min: <->");
            }
            else
            {
                rgTM.prSection[ilCurrentSection].iSecInterval = atoi(pclTmpBuf);
                rgTM.prSection[ilCurrentSection].tExecutedTime = time(0);
                dbg(DEBUG,"<init_ntisch> sec_o_min: <%d>", rgTM.prSection[ilCurrentSection].iSecInterval );
            }
            /*************************************************************************************************/


            /* user specific data */
            memset((void*)pclDataBuffer, 0x00, iMAX_BUF_SIZE);    
            if ((ilRC = iGetConfigRow(pclCfgFile, pclCurrentSection, "data", CFG_STRING, pclDataBuffer)) != RC_SUCCESS)
            {
                /* in this case we should check the send command entry */
                if ((ilRC = iGetConfigRow(pclCfgFile, pclCurrentSection, "snd_cmd", CFG_STRING, pclDataBuffer)) != RC_SUCCESS)
                {
                    rgTM.prSection[ilCurrentSection].pcData   = NULL;
                    rgTM.prSection[ilCurrentSection].iDataLen = 0;
                }
                else
                {
                    /* here we use BC_Head and Cmdblk - Structure */
                    ilSL = sizeof(BC_HEAD) + sizeof(CMDBLK);
                    if ((rgTM.prSection[ilCurrentSection].pcData = (char*)malloc(ilSL)) == NULL)
                    {
                        dbg(TRACE,"<init_ntisch> %05d memory failure...", __LINE__);
                        rgTM.prSection[ilCurrentSection].pcData   = NULL;
                        rgTM.prSection[ilCurrentSection].iDataLen = 0;
                    }
                    else
                    {
                        /* clear memory */
                        memset((void*)rgTM.prSection[ilCurrentSection].pcData, 0x00, (size_t)ilSL);

                        /* set pointer */ 
                        prlBCHead = (BC_HEAD*)(rgTM.prSection[ilCurrentSection].pcData);
                        prlCmdblk = (CMDBLK*)((char*)prlBCHead->data);
                        strcpy(prlCmdblk->command, pclDataBuffer);

                        /* set length */
                        rgTM.prSection[ilCurrentSection].iDataLen = ilSL;
                    }
               }
           }
           else
           {
              /* save data to stucture */
              if ((rgTM.prSection[ilCurrentSection].pcData = strdup(pclDataBuffer)) == NULL)
              {
                  dbg(TRACE,"<init_ntisch> %05d can't save data in structure...", __LINE__);
                  rgTM.prSection[ilCurrentSection].pcData   = NULL;
                  rgTM.prSection[ilCurrentSection].iDataLen = 0;
              }
              else
              {
                  rgTM.prSection[ilCurrentSection].iDataLen=strlen(pclDataBuffer);
              }
           }

           /* n_time */
           memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
           if ((ilRC = iGetConfigEntry(pclCfgFile, pclCurrentSection, "n_time", CFG_STRING, pclTmpBuf)) != RC_SUCCESS)
           {
               rgTM.prSection[ilCurrentSection].iNTime = 0;  
           }
           else
           {
               /* initialize counters */
               rgTM.prSection[ilCurrentSection].iNTime = atoi(pclTmpBuf);    
               for (i=0; i<iMAX_MINUTES; i++)
                   rgTM.prSection[ilCurrentSection].piMinCounter[i] = 0;
               for (i=0; i<iMAX_HOURS; i++)
                   rgTM.prSection[ilCurrentSection].piHourCounter[i] = 0;
           }

           /* type */
           memset((void*)pclTmpBuf, 0x00, iMIN_BUF_SIZE);
           if ((ilRC = iGetConfigEntry(pclCfgFile, pclCurrentSection, "type", CFG_STRING, pclTmpBuf)) != RC_SUCCESS)
           {
               rgTM.prSection[ilCurrentSection].iType = iABS;    
           }
           else
           {
               StringUPR((UCHAR*)pclTmpBuf);
               if (!strcmp(pclTmpBuf, "REL"))
               {
                   /* set flag */
                   rgTM.prSection[ilCurrentSection].iType = iREL;    
                    
                   /* if type is REL we must initialize StartTime, NextHour, NextMinutes...  */
                       
                   /* first get current time */
                   tlCurTime  = time(NULL);

                   /* convert to better format */
                   prlCurTime = (struct tm*)localtime(&tlCurTime);

                   /* save current timestamp */
                   memcpy((void*)&rgTM.prSection[ilCurrentSection].rStartTime,(const void*)prlCurTime, 
                          (size_t)sizeof(struct tm));
               }
               else if (!strcmp(pclTmpBuf, "ABS"))
                   rgTM.prSection[ilCurrentSection].iType = iABS;  
               else
               {
                   dbg(TRACE,"<init_ntisch> %05d unknown type", __LINE__);
                   rgTM.prSection[ilCurrentSection].iType = iABS;    
               }
           }
           dbg(DEBUG,"<init_ntisch> type: <%d>", rgTM.prSection[ilCurrentSection].iType);
           dbg(DEBUG,"<init_ntisch> ------------ END <%s> (%d) -----------", pclCurrentSection,ilCurrentSection);
        }
    }

    dbg(DEBUG,"<init_ntisch> === START INIT NEXT HOURS/MINUTES ===");

    /* init nextHour and nextMinute */
    for (ilCurrentSection=0; ilCurrentSection<rgTM.iNoOfSections; ilCurrentSection++)
    {
        if (rgTM.prSection[ilCurrentSection].iType == iREL)
        {
            for (ilCurHour=0; ilCurHour<rgTM.prSection[ilCurrentSection].iNoOfHOD; ilCurHour++)
            {
                /* calculate new relative time now */
                memcpy((void*)&rgTM.prSection[ilCurrentSection].rNextHours[ilCurHour], 
                       (const void*)&rgTM.prSection[ilCurrentSection].rStartTime, (size_t)sizeof(struct tm));

                /* add offset to hours... */
                rgTM.prSection[ilCurrentSection].rNextHours[ilCurHour].tm_hour += rgTM.prSection[ilCurrentSection].piHoursOfDay[ilCurHour];

                /* we have only 24 hours... */
                rgTM.prSection[ilCurrentSection].rNextHours[ilCurHour].tm_hour %= 24;
                dbg(DEBUG,"<init_ntisch> %05d Set NextHour To %d And Min To %d , Section %d", __LINE__, rgTM.prSection[ilCurrentSection].rNextHours[ilCurHour].tm_hour, rgTM.prSection[ilCurrentSection].rNextHours[ilCurHour].tm_min,ilCurrentSection);
            }

            for (ilCurMin=0; ilCurMin<rgTM.prSection[ilCurrentSection].iNoOfMOH; ilCurMin++)
            {
                /* calculate relative time now */
                memcpy((void*)&rgTM.prSection[ilCurrentSection].rNextMinutes[ilCurMin], 
                       (const void*)&rgTM.prSection[ilCurrentSection].rStartTime, (size_t)sizeof(struct tm));
                
                /* add offset to minutes... */
                rgTM.prSection[ilCurrentSection].rNextMinutes[ilCurMin].tm_min += rgTM.prSection[ilCurrentSection].piMinOfHour[ilCurMin];

                /* we have only 60 minutes */
                rgTM.prSection[ilCurrentSection].rNextMinutes[ilCurMin].tm_min %= 60;
                dbg(DEBUG,"<init_ntisch> %05d Set NextMinute To %d , Section %d", __LINE__, 
                           rgTM.prSection[ilCurrentSection].rNextMinutes[ilCurMin].tm_min,ilCurrentSection);
            }
        }
    }
    dbg(DEBUG,"<init_ntisch> === END INIT NEXT HOURS/MINUTES ===");

    /* read binary file - if it exist... */
    /* format of data in binary file is FUNCTION->DATA_PERIOD->USERDATA */
    ilBinaryFileIsCorrupt = FALSE;
    if ((fh = fopen(pcgBinaryFileName,"rb")) == NULL)
    {
        dbg(DEBUG,"<init_ntisch> %05d can't open binary file...", __LINE__);
    }
    else
    {
        dbg(DEBUG,"<init_ntisch> ------ START READING BINARY FILE ------");
        /* read file-header ... */
        fseek(fh, 0L, SEEK_SET);
        fread((void*)&rlFileHeader, (size_t)sizeof(FileHeader), (size_t)1, fh);
        dbg(DEBUG,"<init_ntisch> %05d binary header: Length: %d, Number: %d, FId: %d", __LINE__, 
                  (int)rlFileHeader.lLength, rlFileHeader.iNumber, rlFileHeader.iFId);

        /* is this a valid file header... */
        if (rlFileHeader.lLength < 0 || rlFileHeader.iNumber < 0 || rlFileHeader.iFId < 0)
        {
            /* corrupt binary file? */
            /* yes, maybe... */
            dbg(TRACE,"<init_ntisch> %05d maybe a corrupt binary file...", __LINE__);
            dbg(TRACE,"<init_ntisch> %05d ignore all binary records now...", __LINE__);
            dbg(TRACE,"<init_ntisch> %05d binary header: Length: %d, Number: %d, FId: %d", __LINE__, 
                       (int)rlFileHeader.lLength, rlFileHeader.iNumber, rlFileHeader.iFId);
            ilBinaryFileIsCorrupt = TRUE;
        }

        for (ilRecordNo=0; ilRecordNo<rlFileHeader.iNumber && ilBinaryFileIsCorrupt == FALSE; ilRecordNo++)
        {
            dbg(DEBUG,"<init_ntisch> --- START READING BINARY RECORD %d ---", ilRecordNo);

            /* now read record header... */
            fread((void*)&rlRecordHeader, (size_t)sizeof(RecordHeader), (size_t)1, fh);
            dbg(DEBUG,"<init_ntisch> %05d record header: ID: %d, from Event %d, Len: %d", __LINE__, 
                       rlRecordHeader.iRecordID,rlRecordHeader.iRecordEventID, (int)rlRecordHeader.lRecordLen);

            /* memory for whole record */
            if ((pclRecord = (char*)malloc(rlRecordHeader.lRecordLen)) == NULL)
            {
                dbg(TRACE,"<init_ntisch> %05d try to allocate %ld bytes", __LINE__, rlRecordHeader.lRecordLen);
                dbg(TRACE,"<init_ntisch> %05d can't allocate memory for record %d", __LINE__, i);
                pclRecord = NULL;

                /* this seems to be a corrupt binrary configuration file... */
                /* we have to ignore it!!! */
                dbg(TRACE,"<init_ntisch> %05d maybe a corrupt binary file...", __LINE__);
                dbg(TRACE,"<init_ntisch> %05d ignore all following binary records now...", __LINE__);
                ilRecordNo = rlFileHeader.iNumber + 1;
                ilBinaryFileIsCorrupt = TRUE;
                continue;
            }

            /* clear buffer */
            memset((void*)pclRecord, 0x00, (size_t)rlRecordHeader.lRecordLen);

            /* ...and the following record */
            fread((void*)pclRecord, (size_t)rlRecordHeader.lRecordLen, (size_t)1, fh);

            /* set pointer... */
            prlFunction   = (FUNCTION*)((char*)pclRecord);
            prlDataPeriod     = (DATA_PERIOD*)((char*)prlFunction->data);
            pclData           = (char*)prlDataPeriod->data;

            dbg(DEBUG,"<init_ntisch> -------------------------------");
            dbg(DEBUG,"<init_ntisch> read from binary file....");
            dbg(DEBUG,"<init_ntisch> Function->StartTime.tm_hour: %d", prlFunction->rStartTime.tm_hour);
            dbg(DEBUG,"<init_ntisch> Function->StartTime.tm_min: %d", prlFunction->rStartTime.tm_min);
            dbg(DEBUG,"<init_ntisch> Function->Function: %d", prlFunction->iFunction);
            dbg(DEBUG,"<init_ntisch> Function->EventID: %d", prlFunction->iEventID);
            dbg(DEBUG,"<init_ntisch> Function->ParentID: %d", prlFunction->iParentID);
            dbg(DEBUG,"<init_ntisch> Function->DestinID: %d", prlFunction->iDestinationID);
            dbg(DEBUG,"<init_ntisch> DataPeriod->day_o_week: %X", prlDataPeriod->rPeriod.day_o_week);
            dbg(DEBUG,"<init_ntisch> DataPeriod->hour_o_day: %d", prlDataPeriod->rPeriod.hour_o_day);
            dbg(DEBUG,"<init_ntisch> DataPeriod->min_o_hour: %d", prlDataPeriod->rPeriod.min_o_hour);
            dbg(DEBUG,"<init_ntisch> DataPeriod->sec_o_min: %d", prlDataPeriod->rPeriod.sec_o_min);
            dbg(DEBUG,"<init_ntisch> DataPeriod->n_times: %d", prlDataPeriod->rPeriod.n_times);
            dbg(DEBUG,"<init_ntisch> DataPeriod->count: %d", prlDataPeriod->rPeriod.count);
            dbg(DEBUG,"<init_ntisch> DataPeriod->period: %d", prlDataPeriod->rPeriod.period);
            dbg(DEBUG,"<init_ntisch> DataLen <%d>", (int)prlDataPeriod->lDataLen);
            dbg(DEBUG,"<init_ntisch> --------------------------------");

            /* now we must add new data to internal structure... */
            /* first add number of sections */
            rgTM.iNoOfSections++;
            ilCurSection = rgTM.iNoOfSections - 1;
            
            /* get memory for sections */
            if ((rgTM.prSection = (TNTSection*)realloc(rgTM.prSection, rgTM.iNoOfSections*sizeof(TNTSection))) == NULL)
            {
                dbg(TRACE,"<init_ntisch> %05d cannot allocate (realloc) memory for sections", __LINE__);
                Terminate(30);
            }

            /* set structure members */
            memset((void*)&rgTM.prSection[ilCurSection], 0x00, sizeof(TNTSection));

            /* all ID's */
            rgTM.prSection[ilCurSection].iFunction      = prlFunction->iFunction;
            rgTM.prSection[ilCurSection].iEventID       = prlFunction->iEventID;
            rgTM.prSection[ilCurSection].iParentID      = prlFunction->iParentID;
            rgTM.prSection[ilCurSection].iDestinationID = prlFunction->iDestinationID;
            rgTM.prSection[ilCurSection].iEventCommand  = EVENT_DATA;
            rgTM.prSection[ilCurrentSection].iUseInternalEvent = 0;
            rgTM.prSection[ilCurSection].iFID = rlRecordHeader.iRecordEventID; 

            /* all days */
            if (prlDataPeriod->rPeriod.day_o_week & SU)
            {
                rgTM.prSection[ilCurSection].iNoOfDOW++;
                rgTM.prSection[ilCurSection].iDaysOfWeek |= iSUN;
            }
            if (prlDataPeriod->rPeriod.day_o_week & MO)
            {
                rgTM.prSection[ilCurSection].iNoOfDOW++;
                rgTM.prSection[ilCurSection].iDaysOfWeek |= iMON;
            }
            if (prlDataPeriod->rPeriod.day_o_week & TU)
            {
                rgTM.prSection[ilCurSection].iNoOfDOW++;
                rgTM.prSection[ilCurSection].iDaysOfWeek |= iTUE;
            }
            if (prlDataPeriod->rPeriod.day_o_week & WE)
            {
                rgTM.prSection[ilCurSection].iNoOfDOW++;
                rgTM.prSection[ilCurSection].iDaysOfWeek |= iWED;
            }
            if (prlDataPeriod->rPeriod.day_o_week & TH)
            {
                rgTM.prSection[ilCurSection].iNoOfDOW++;
                rgTM.prSection[ilCurSection].iDaysOfWeek |= iTHU;
            }
           if (prlDataPeriod->rPeriod.day_o_week & FR)
           {
               rgTM.prSection[ilCurSection].iNoOfDOW++;
               rgTM.prSection[ilCurSection].iDaysOfWeek |= iFRI;
           }
           if (prlDataPeriod->rPeriod.day_o_week & SA)
           {
               rgTM.prSection[ilCurSection].iNoOfDOW++;
               rgTM.prSection[ilCurSection].iDaysOfWeek |= iSAT;
           }

           /* only one hour */
           if (prlDataPeriod->rPeriod.hour_o_day >= 0)
           {
               /* use only on hour */
               rgTM.prSection[ilCurSection].iNoOfHOD = 1;
               rgTM.prSection[ilCurSection].piHoursOfDay[0] = prlDataPeriod->rPeriod.hour_o_day;
           }
            
           /* only one minute */
           if (prlDataPeriod->rPeriod.min_o_hour >= 0)
           {
               rgTM.prSection[ilCurSection].iNoOfMOH = 1;
               rgTM.prSection[ilCurSection].piMinOfHour[0] = prlDataPeriod->rPeriod.min_o_hour;
           }

           /* something else */
           rgTM.prSection[ilCurSection].iNTime = prlDataPeriod->rPeriod.n_times;
           for (i=0; i<iMAX_MINUTES; i++)
               rgTM.prSection[ilCurSection].piMinCounter[i] = 0;
           for (i=0; i<iMAX_HOURS; i++)
               rgTM.prSection[ilCurSection].piHourCounter[i] = 0;
           rgTM.prSection[ilCurSection].iType = prlDataPeriod->rPeriod.period;

           if (rgTM.prSection[ilCurSection].iType == iREL)
           {
               /* save start-timestamp */
               memcpy((void*)&rgTM.prSection[ilCurSection].rStartTime, 
                      (const void*)&prlFunction->rStartTime, (size_t)sizeof(struct tm));

               /* this section isn't forever */
               if (rgTM.prSection[ilCurSection].iNTime > 0)
               {
                   /* calculate next event... */
                   TimeDiff = next = 0;
                   if (prlDataPeriod->rPeriod.hour_o_day > 0)
                       TimeDiff += prlDataPeriod->rPeriod.hour_o_day * 3600;
                   if (prlDataPeriod->rPeriod.min_o_hour > 0)
                       TimeDiff += prlDataPeriod->rPeriod.min_o_hour * 60;

                   next = mktime(&prlFunction->rStartTime) + TimeDiff;
                   _tm = (struct tm*)localtime(&next);
                   memcpy(&rlNextTime, _tm, sizeof(struct tm));

                   ilNextMinute = rlNextTime.tm_min;
                   ilNextHour = rlNextTime.tm_hour;

                   /* with an absolute date? */
                   if ((rlNextTime.tm_mon != prlFunction->rStartTime.tm_mon) ||
                       (rlNextTime.tm_mday != prlFunction->rStartTime.tm_mday))
                   {
                       /* set abs date */
                       rgTM.prSection[ilCurSection].iNoOfAbsDate = 1;
                       rgTM.prSection[ilCurSection].rAbsDate[0].iDayOfMonth = rlNextTime.tm_mday;
                       rgTM.prSection[ilCurSection].rAbsDate[0].iMonthOfYear = rlNextTime.tm_mon + 1;
                       dbg(DEBUG,"<init_ntisch> DayOfMonth: %d", rgTM.prSection[ilCurSection].rAbsDate[0].iDayOfMonth);
                       dbg(DEBUG,"<init_ntisch> MonthOfYear: %d", rgTM.prSection[ilCurSection].rAbsDate[0].iMonthOfYear);
                   }
                }
                else
                {
                    /* this section is forever */
                    /* first get current time */
                    tlCurTime  = time(NULL);

                    /* convert to better format */
                    prlCurTime = (struct tm*)localtime(&tlCurTime);
                    memcpy(&rlCurTime, prlCurTime, sizeof(struct tm));

                    /* init something */
                    TimeDiff = next = 0;
                    next = mktime(&prlFunction->rStartTime);
                    _tm = (struct tm*)localtime(&next);
                    memcpy(&rlNextTime, _tm, sizeof(struct tm));
                    while (rlNextTime.tm_hour < rlCurTime.tm_hour || 
                         (rlNextTime.tm_hour <= rlCurTime.tm_hour && rlNextTime.tm_min < rlCurTime.tm_min))
                    {
                        if (prlDataPeriod->rPeriod.hour_o_day > 0)
                            TimeDiff += prlDataPeriod->rPeriod.hour_o_day * 3600;
                        if (prlDataPeriod->rPeriod.min_o_hour > 0)
                            TimeDiff += prlDataPeriod->rPeriod.min_o_hour * 60;

                        next = mktime(&prlFunction->rStartTime) + TimeDiff;
                        _tm = (struct tm*)localtime(&next);
                        memcpy(&rlNextTime, _tm, sizeof(struct tm));
                    }

                   /* set next hours */
                   ilNextMinute = rlNextTime.tm_min;
                   ilNextHour = rlNextTime.tm_hour;
               }

               if (rgTM.prSection[ilCurSection].iNoOfHOD > 0)
               {
                   /* set hours... */
                   memcpy((void*)&rgTM.prSection[ilCurSection].rNextHours[0], 
                          (const void*)&prlFunction->rStartTime, (size_t)sizeof(struct tm));
                   rgTM.prSection[ilCurSection].rNextHours[0].tm_hour = ilNextHour;
               }

               /* for all minutes */
               if (rgTM.prSection[ilCurSection].iNoOfMOH > 0)
               {
                   /* set next minute... */
                   memcpy((void*)&rgTM.prSection[ilCurSection].rNextMinutes[0], 
                          (const void*)&prlFunction->rStartTime, (size_t)sizeof(struct tm));
                   rgTM.prSection[ilCurSection].rNextMinutes[0].tm_min = ilNextMinute;
               }

               if (rgTM.prSection[ilCurSection].iNoOfMOH > 0 && rgTM.prSection[ilCurSection].iNoOfHOD > 0)
               {
                   dbg(DEBUG,"<init_ntisch> %05d set nextHour to %d and min to %d", __LINE__, 
                              rgTM.prSection[ilCurSection].rNextHours[0].tm_hour, 
                              rgTM.prSection[ilCurSection].rNextMinutes[0].tm_min);
               }
               else if (rgTM.prSection[ilCurSection].iNoOfMOH > 0 && rgTM.prSection[ilCurSection].iNoOfHOD <= 0)
               {
                   dbg(DEBUG,"<init_ntisch> %05d set min to %d", __LINE__, rgTM.prSection[ilCurSection].rNextMinutes[0].tm_min);
               }
               else if (rgTM.prSection[ilCurSection].iNoOfMOH <= 0 && rgTM.prSection[ilCurSection].iNoOfHOD > 0)
               {
                   dbg(DEBUG,"<init_ntisch> %05d set nextHour to %d and min to %d", __LINE__, 
                              rgTM.prSection[ilCurSection].rNextHours[0].tm_hour, 
                              rgTM.prSection[ilCurSection].rNextHours[0].tm_min);
               }
           }

           /* user specific data */
           rgTM.prSection[ilCurSection].iDataLen = (UINT)prlDataPeriod->lDataLen; 

           if (prlDataPeriod->lDataLen > 0)
           {
               /* Ok if we are here, it seems to be so that the binary file is 
                  good. If we now get an malloc failure we must terminate 
                  the process
               */
               if ((rgTM.prSection[ilCurSection].pcData = (char*)malloc(prlDataPeriod->lDataLen)) == NULL)
               {
                   dbg(TRACE,"<init_ntisch> %05d cannot allocate memory for pcData", __LINE__);
                   rgTM.prSection[ilCurSection].pcData = NULL;
                   Terminate(30);
               }
               memcpy((void*)rgTM.prSection[ilCurSection].pcData,(const void*)pclData, prlDataPeriod->lDataLen);
           }

           /* free memory */
           if (pclRecord != NULL)
           {
               free ((void*)pclRecord);
               pclRecord = NULL;
           }
           dbg(DEBUG,"<init_ntisch> --- END READING BINARY RECORD %d ---", ilRecordNo);
       }

       /* close it */
       if (fh != NULL)
       {
           fclose(fh);
           fh = NULL;
       }
       dbg(DEBUG,"<init_ntisch> -------- END READING BINARY FILE --------");
    }

    /* save corrput binary file... */
    if (ilBinaryFileIsCorrupt == TRUE)
    {
        sprintf(pcgSaveBinaryFileName, "%s/%s.corrupt.%s", getenv("CFG_PATH"), sDEFAULT_FNAME, GetTimeStamp());
        dbg(TRACE,"<init_ntisch> %05d saving corrupt binary file <%s>", __LINE__, pcgSaveBinaryFileName); 
        rename(pcgBinaryFileName, pcgSaveBinaryFileName);
    }

    /* initialize timer-handler */
    fpTimerHandler[0] = DeleteTimer;
    fpTimerHandler[1] = AddTimer;
    fpTimerHandler[2] = UpdateTimer;
    fpTimerHandler[3] = UPDBufTimer;
    fpTimerHandler[4] = HSBAddTimer;
    fpTimerHandler[5] = DelModIDTimer;

    dbg(DEBUG,"<init_ntisch> ====== END ======");
    /* bye bye */
    return RC_SUCCESS;
}


/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset(void)
{
    int i;
    int ilRC;

    dbg(TRACE,"<Reset> %05d CTRL-STA: <%s>", __LINE__, CTRL_STA(ctrl_sta));
    
    dbg(DEBUG,"<Reset> %05d now resetting", __LINE__);
    if (prgItem != NULL)
    {
        dbg(DEBUG,"<Reset> %05d free Item...", __LINE__);
        free ((void*)prgItem);
        prgItem = NULL;
    }
    for (i=0; i<rgTM.iNoOfSections; i++)
    {
        if (rgTM.prSection[i].pcData != NULL)
        {
            dbg(DEBUG,"<Reset> %05d free pcData[%d] now...", __LINE__, i);
            free ((void*)rgTM.prSection[i].pcData);
            rgTM.prSection[i].pcData = NULL;
        }
    }
    if (rgTM.prSection != NULL)
    {
        dbg(DEBUG,"<Reset> %05d free section now...", __LINE__);
        free ((void*)rgTM.prSection);
        rgTM.prSection = NULL;
    }

        /* 20020628 JIM: test for memory leak: allocation should be done in que */
    /* Allocate a dynamic buffer for max event size */
    /*if ((prgItem = (ITEM*)malloc(MAX_EVENT_SIZE)) == NULL)        */
    /*{     */
    /*  dbg(TRACE,"<Reset> %05d malloc : <%s>", __LINE__, strerror(errno));*/
    /*  prgItem = NULL;     */
    /*  Terminate(0);       */
    /*}     */

    /* new init... */
    if ((ilRC = init_ntisch()) != RC_SUCCESS)
    {       
        dbg(TRACE,"<Reset> %05d init_ntisch returns %d", __LINE__, ilRC);
        Terminate(0);
    }   
    
    /* pointer to event */
        /* 20020628 JIM: test for memory leak: allocation should be done in que */
    /* prgEvent = (EVENT*)prgItem->text;*/

    dbg(TRACE,"<Reset> ...end of reset");   
    return ilRC;
}


/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(UINT ipSleepTime)
{
    int     i;

    if (ipSleepTime > 0)
    {
        dbg(DEBUG,"<Terminate> %05d sleeping %d seconds...", __LINE__, ipSleepTime);
        sleep(ipSleepTime);
    }

    /* ignore most signals... */
    (void)UnsetSignals();

    if (prgItem != NULL)
    {
        dbg(DEBUG,"<Terminate> %05d free Item...", __LINE__);
        free((void*)prgItem);
        prgItem = NULL;
    }
    for (i=0; i<rgTM.iNoOfSections; i++)
    {
        if (rgTM.prSection[i].pcData != NULL)
        {
            dbg(DEBUG,"<Terminate> %05d free pcData[%d] now...", __LINE__, i);
            free ((void*)rgTM.prSection[i].pcData);
            rgTM.prSection[i].pcData = NULL;
        }
    }
    if (rgTM.prSection != NULL)
    {
        dbg(DEBUG,"<Terminate> %05d free section now...", __LINE__);
        free ((void*)rgTM.prSection);
        rgTM.prSection = NULL;
    }

    /* only a message */
    dbg(TRACE,"<Terminate> now leaving ...");
    
    /* this is the end, the only end my friend....*/
    exit(0);
}


/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int ipSig)
{
    dbg(DEBUG,"<HandleSignal> %05d signal <%d> received", __LINE__, ipSig);

    switch(ipSig)
    {
        default:
            exit(0);
            break;
    } 
    return;
}


/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int ipErr)
{
    dbg(TRACE,"<HandleErr> %05d error %d received...", __LINE__, ipErr);
    return;
}


/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    switch(pipErr) 
    {
    case    QUE_E_FUNC  :   /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;

    case    QUE_E_MEMORY    :   /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;

    case    QUE_E_SEND  :   /* Error using msgsnd */
        dbg(TRACE,"<%d> : msgsnd failed",pipErr);
        break;

    case    QUE_E_GET   :   /* Error using msgrcv */
        dbg(TRACE,"<%d> : msgrcv failed",pipErr);
        break;

    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;

    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;

    case    QUE_E_ACKUNEX   :
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;

    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
        break;

    case    QUE_E_INACTIVE  :
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;

    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;

    case    QUE_E_NOQUEUES  :
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        break;

    case    QUE_E_RESP  :   /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;

    case    QUE_E_FULL  :
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;

    case    QUE_E_NOMSG :   /* No message on queue */
        /******
        dbg(TRACE,"<%d> : no messages on queue",pipErr);
        ******/
        break;

    case    QUE_E_INVORG    :   /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;

    case    QUE_E_NOINIT    :   /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;

    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;

    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;

    default         :   /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
    } /* end switch */
         
    return;

} /* end of HandleQueErr */


/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues(void)
{
    int ilRC = RC_SUCCESS;          /* Return code */
    int ilBreakOut = FALSE;
    int     len;    
    do
    {
        dbg(TRACE,"<HandleQueues> %05d CTRL-STA: <%s>", __LINE__, CTRL_STA(ctrl_sta));
        /*memset(prgItem, 0x00, MAX_EVENT_SIZE);*/
        /* 20020628 JIM: test for memory leak: allocation should be done in que */
    /*  prgItem=NULL;*/
        len=0;
        ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,len,(char*)&prgItem);
    
        if( ilRC == RC_SUCCESS )
        {
            ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
            if (ilRC != RC_SUCCESS) 
            {
                HandleQueErr(ilRC);
            } 
          prgEvent = (EVENT *) prgItem->text;
            switch (prgEvent->command)
            {
                case    HSB_COMING_UP:
                    ctrl_sta = prgEvent->command;
                    igLastCtrlSta = ctrl_sta;
                    break;  
        
                case    HSB_ACTIVE:
                    /* to copy binary- and ascii configuration file... */
                    TransferAllFiles();
                case    HSB_STANDBY:
                case    HSB_STANDALONE:
                case    HSB_ACT_TO_SBY:
                    ctrl_sta = prgEvent->command;
                    igLastCtrlSta = ctrl_sta;
                    ilBreakOut = TRUE;
                    break;  

                case    HSB_DOWN:
                    if ((ilRC = remove(pcgBinaryFileName)) != 0)
                    {
                        dbg(TRACE,"<HandleQueues> %05d can't remove file: <%s>", __LINE__, pcgBinaryFileName);
                    }
                    else
                    {
                        dbg(DEBUG,"<HandleQueues> %05d calling DeleteRemoteBinFile...", __LINE__);
                        DeleteRemoteBinFile();
                    }
                    ctrl_sta = prgEvent->command;
                    igLastCtrlSta = ctrl_sta;
                    Terminate(0);
                    break;  
        
                case    SHUTDOWN:
                    Terminate(0);
                    break;
                            
                case iDELETE_BIN_FILE:
                    /* break, return etc wurde nicht vergessen!!!, falls das bin-file
                    geloescht wurde muessen wir einen reset durchfuehren... */
                    if ((ilRC = remove(pcgBinaryFileName)) != 0)
                    {
                        dbg(TRACE,"<HandleQueues> %05d can't remove file: <%s>", __LINE__, pcgBinaryFileName);
                    }
                    else
                    {
                        dbg(DEBUG,"<CheckQueStatus> %05d calling DeleteRemoteBinFile...", __LINE__);
                        DeleteRemoteBinFile();
                    }
                    
                case    RESET:
                    ilRC = Reset();
                    break;
                            
                case    EVENT_DATA:
                    dbg(DEBUG,"<HandleQueues> %05d wrong hsb status <%d>",__LINE__, ctrl_sta);
                    DebugPrintItem(TRACE,prgItem);
                    DebugPrintEvent(TRACE,prgEvent);
                    break;
                        
                case TRACE_ON:
                    dbg_handle_debug(prgEvent->command);
                    break;

                case TRACE_OFF:
                    dbg_handle_debug(prgEvent->command);
                    break;

                default:
                    dbg(DEBUG,"<HandleQueues> unknown event");
                    DebugPrintItem(TRACE,prgItem);
                    DebugPrintEvent(TRACE,prgEvent);
                    break;
            } 
            
            /* Handle error conditions */
            if(ilRC != RC_SUCCESS)
            {
                HandleErr(ilRC);
            }
        } 
        else 
        {
            HandleQueErr(ilRC);
        } 
    } while (ilBreakOut == FALSE);
} /* end of HandleQueues */


/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(void)
{
    /* format of data is EVENT->FUNCTION->DATA_PERIOD->USERDATA */
    int             ilRC;
    FUNCTION            *prlFunction = NULL;

    dbg(DEBUG,"<HandleData> =====================================");
    dbg(DEBUG,"<HandleData> ----- START -----");
    
    /* set Pointer to structures */
    prlFunction = (FUNCTION*)((char*)prgEvent+sizeof(EVENT));

    /* Check Function that's necessary!! */
    if (prlFunction->iFunction < iDELETE || prlFunction->iFunction > iDEL_MODID)
    {
        /* ??? */
        dbg(TRACE,"<HandleData> %05d unknown function received (%d)", __LINE__, prlFunction->iFunction);
        dbg(DEBUG,"<HandleData> ----- END -----");
        dbg(DEBUG,"<HandleData> =====================================\n");
        return RC_FAIL;
    }

    /* debug info */
    DebugPrintEvent(DEBUG,prgEvent);

    /* check ctrl_sta */
    if ((ctrl_sta == HSB_ACT_TO_SBY || ctrl_sta == HSB_STANDBY) && prlFunction->iFunction != iHSBADD && prlFunction->iFunction != iDELETE && prlFunction->iFunction != iDEL_MODID)
    {
        dbg(TRACE,"<HandleData> -------------------------------------");
        dbg(TRACE,"<HandleData> CTRL-STA: <%s>",CTRL_STA(ctrl_sta));
        dbg(TRACE,"<HandleData> FUNCTION: <%s>",FUNCTION(prlFunction->iFunction));
        dbg(TRACE,"<HandleData> can't handle");
        dbg(TRACE,"<HandleData> -------------------------------------");
        dbg(DEBUG,"<HandleData> ----- END -----");
        dbg(DEBUG,"<HandleData> =====================================\n");
        return RC_FAIL;
    }
    else
    {
        /* --------------------------------------------------
       Possible Functions: DeleteTimer, AddTimer, UpdateTimer, 
                                  UPDBufTimer, HSBAddTimer, DelModIDTimer,
        -------------------------------------------------- */
        dbg(DEBUG,"<HandleData> %05d Function is <%s>", __LINE__, FUNCTION(prlFunction->iFunction));
        dbg(DEBUG,"<HandleData> %05d CTRL-STA: <%s>", __LINE__, CTRL_STA(ctrl_sta));
        ilRC = (*fpTimerHandler[prlFunction->iFunction])(prlFunction, iHANDLE_DATA);

        /* binary file has changed here, so send it to remote machine... */ 
        /* to copy binary- and ascii configuration file... */
        TransferAllFiles();

        dbg(DEBUG,"<HandleData> ----- END -----");
        dbg(DEBUG,"<HandleData> =====================================\n");

        /* bye bye */
        return ilRC;
    }
}

/******************************************************************************/
/* The check que status routine                                               */
/******************************************************************************/
static int CheckQueStatus(void)
{
    int     ilRC;
    int           len;
    /*memset(prgItem, 0x00, MAX_EVENT_SIZE);*/
    len=0;
        /* 20020628 JIM: test for memory leak: allocation should be done in que */
    /* prgItem=NULL; */
    if ((ilRC = que(QUE_GETBIGNW, 0, mod_id, PRIORITY_3,len, (char*)&prgItem)) == RC_SUCCESS)
    {
        /* Acknowledge the item */
        ilRC = que(QUE_ACK, 0, mod_id, 0, 0, NULL);
        if (ilRC != RC_SUCCESS) 
        {
            HandleQueErr(ilRC);
        } 
    prgEvent = (EVENT *) prgItem->text;
        switch (prgEvent->command)
        {
            case    HSB_COMING_UP:
                ctrl_sta = prgEvent->command;
                igLastCtrlSta = ctrl_sta;
                return -1;

            case    HSB_STANDBY:
                ctrl_sta = prgEvent->command;
                igLastCtrlSta = ctrl_sta;
                return 0;

            case    HSB_ACTIVE:
                /* to copy binary- and ascii configuration file... */
                TransferAllFiles();
            case    HSB_STANDALONE:
            case    HSB_ACT_TO_SBY:
                ctrl_sta = prgEvent->command;
                if (igLastCtrlSta == HSB_STANDBY)
                {
                    igLastCtrlSta = ctrl_sta;
                    Reset();
                }
                return 0;
    
            case    HSB_DOWN:
                if ((ilRC = remove(pcgBinaryFileName)) != 0)
                {
                    dbg(TRACE,"<CheckQueStatus> %05d can't remove file: <%s>", __LINE__, pcgBinaryFileName);
                }
                else
                {
                    dbg(DEBUG,"<CheckQueStatus> %05d calling DeleteRemoteBinFile...", __LINE__);
                    DeleteRemoteBinFile();
                }
                ctrl_sta = prgEvent->command;
                igLastCtrlSta = ctrl_sta;
                Terminate(0);
                break;  

            case    SHUTDOWN:
                Terminate(0);
                break;
                    
            case iDELETE_BIN_FILE:
                /* break, return etc wurde nicht vergessen!!!, falls das bin-file
                geloescht wurde muessen wir einen reset durchfuehren... */
                if ((ilRC = remove(pcgBinaryFileName)) != 0)
                {
                    dbg(TRACE,"<CheckQueStatus> %05d can't remove file: <%s>", __LINE__, pcgBinaryFileName);
                }
                else
                {
                    dbg(DEBUG,"<CheckQueStatus> %05d calling DeleteRemoteBinFile...", __LINE__);
                    DeleteRemoteBinFile();
                }
                
            case    RESET:
                Reset();
                return -2;
                    
            case    EVENT_DATA:
                HandleData();
                return 0;

            case iPRINT_NEXT_EVENT:
                PrintNextEvents();
                return 0;
                case iDUMP_BIN_FILE:
                    DumpBinFile();
                return 0;
            case TRACE_OFF:
            case TRACE_ON:
                dbg_handle_debug(prgEvent->command);
                return 0;

            default:
                dbg(DEBUG,"main unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                return 0;
        } 
    } 
    else
        HandleQueErr(ilRC);

    /* bye bye */
    return 0;
}

/******************************************************************************/
/* The time scheduler routine                                                 */
/******************************************************************************/
static int TimeScheduler(void)
{
    int         ilRC;

    while (1)
    {
        /*--------------------------------------------------------------------*/
        /*--------------------------------------------------------------------*/
        sleep(rgTM.iSleepTime);

        /*--------------------------------------------------------------------*/
        /*--------------------------------------------------------------------*/
        if ((ilRC = CheckTimer()) < 0)
            return ilRC;
    
        /*--------------------------------------------------------------------*/
        /*--------------------------------------------------------------------*/
        if ((ilRC = CheckQueStatus()) < 0)
            return ilRC;
    }
}

/******************************************************************************/
/* The check timer routine                                                    */
/******************************************************************************/
static int CheckTimer(void)
{
  int                       ilRC;
  int                       ilOK;
  int                       ilOp;
  int                       ilGoOn;
  int                       ilFound;
  int                       ilSection;
  int                       ilCurHour;
  int                       ilCurMin;
  int                       ilCurAbsDate;
  time_t                    tlCurTime;
  struct tm             *prlCurTime;
  struct tm             rlCurTime;
  FUNCTION                  rlFunction;

  /* get current system state... */
  ilRC = debug_level;
  debug_level = TRACE;
  ctrl_sta = get_system_state();
  debug_level = ilRC;

  /* get current time */
  tlCurTime  = time(NULL);

  /* convert to better format */
  prlCurTime = (struct tm*)localtime(&tlCurTime);

  /* save time */
  memcpy((void*)&rlCurTime, (const void*)prlCurTime, sizeof(struct tm));
    
  /* now check time... */
  for (ilSection=0; ilSection<rgTM.iNoOfSections; ilSection++)
  {
     /*------------------------------------------------------------------*/
     /*------------------------------------------------------------------*/
     /*------------------------------------------------------------------*/
     /* first check iType (rel or abs)..... */
     if (rgTM.prSection[ilSection].iType == iREL)
     {
        /* this is relative!!!!! */
        /*------------------------------------------------------------------
        bei einem relativem Ansatz beziehen wir uns auf die Zeit (rStartTime)
        in der Structur. Es finden lediglich die Stunden oder die Minuten 
        Verwendung. Tage und Sekunden machen keine (wenig) Sinn. Bsp:
        Montag relativ zu 9 Uhr??? Hae - kapier ich nicht...
        5 Sekunden spaeter relativ zu 9 Uhr - Na gut, geht vielleich, aber 
        ist so ein Eintrag sinnvoll. Sicher nicht oder...
        ------------------------------------------------------------------*/
        if (rgTM.prSection[ilSection].iNoOfHOD > 0)
        {
           if (rgTM.prSection[ilSection].iNoOfAbsDate > 0)
           {
              for (ilCurAbsDate=0,ilOK=0; ilCurAbsDate<rgTM.prSection[ilSection].iNoOfAbsDate; ilCurAbsDate++)
              {
                 if (rgTM.prSection[ilSection].rAbsDate[ilCurAbsDate].iDayOfMonth == rlCurTime.tm_mday && rgTM.prSection[ilSection].rAbsDate[ilCurAbsDate].iMonthOfYear == rlCurTime.tm_mon+1)  
                 {
                    /* found correct date */
                    ilOK = 1;
                    break;
                 }
              }
           }
           else
              ilOK = 1;

           /* search hour */
           for( ilCurHour=0; ilCurHour<rgTM.prSection[ilSection].iNoOfHOD && ilOK; ilCurHour++ )
           {
              /* is this correct time? */
              /* it must be initialized very carefully */
              if (rgTM.prSection[ilSection].rNextHours[ilCurHour].tm_hour == rlCurTime.tm_hour)
              {
                 ilFound = 0;
                 if (rgTM.prSection[ilSection].iNoOfMOH > 0)
                 {
                    /* and search for minute... */
                    for( ilCurMin=0; ilCurMin<rgTM.prSection[ilSection].iNoOfMOH; ilCurMin++ )
                    {
                       /* is this correct time? */
                       if (rgTM.prSection[ilSection].rNextMinutes[ilCurMin].tm_min == rlCurTime.tm_min)
                       {
                          ilFound = 1;
                       }
                    }
                 }
                 else
                 {
                    if (rgTM.prSection[ilSection].rNextHours[ilCurHour].tm_min == rlCurTime.tm_min)
                    {
                       ilFound = 2;
                    }
                 }

                 if (ilFound)
                 {
                    /* test n_time... */
                    if (ilFound == 1)
                    {
                       if (!rgTM.prSection[ilSection].iNTime ||
                           (rgTM.prSection[ilSection].iNTime && 
                           (rgTM.prSection[ilSection].piMinCounter[ilCurMin] < rgTM.prSection[ilSection].iNTime)))
                          ilGoOn = TRUE;
                       else
                          ilGoOn = FALSE; 
                    }
                    else
                    {
                       if (!rgTM.prSection[ilSection].iNTime ||
                           (rgTM.prSection[ilSection].iNTime && 
                           (rgTM.prSection[ilSection].piHourCounter[ilCurHour] < rgTM.prSection[ilSection].iNTime)))
                          ilGoOn = TRUE;
                       else
                          ilGoOn = FALSE; 
                    }

                    if (ilGoOn)
                    {
                       /* found correct hour and correct minute! */
                       /* in this case we should send */
                       /* send command now */
                       if (ilFound == 1)
                       {
                          if (!rgTM.prSection[ilSection].piMinIsSent[ilCurMin])
                             ilGoOn = TRUE;
                          else
                             ilGoOn = FALSE; 
                       }
                       else
                       {
                          if (!rgTM.prSection[ilSection].piHourIsSent[ilCurHour])
                             ilGoOn = TRUE;
                          else
                             ilGoOn = FALSE; 
                       }

                       if (ilGoOn)
                       {
                          /* check ctrl_sta and function... */
                          if (ctrl_sta == HSB_ACTIVE || ctrl_sta == HSB_STANDALONE || ((ctrl_sta == HSB_ACT_TO_SBY || ctrl_sta == HSB_STANDBY) && rgTM.prSection[ilSection].iFunction == iHSBADD))
                          {
                             if ((ilRC = SendCommand(
                                                     rgTM.prSection[ilSection].iUseInternalEvent,
                                                     rgTM.prSection[ilSection].iDestinationID, 
                                                     rgTM.prSection[ilSection].iParentID, 
                                                     rgTM.prSection[ilSection].iEventCommand, 
                                                     rgTM.prSection[ilSection].iDataLen, 
                                                     rgTM.prSection[ilSection].pcData,
                                                     ilSection)) == RC_SUCCESS)
                             {
                                /* calculate new relative time now */
                                rgTM.prSection[ilSection].rNextHours[ilCurHour].tm_hour += rgTM.prSection[ilSection].piHoursOfDay[ilCurHour];

                                /* we have only 24 hours... */
                                rgTM.prSection[ilSection].rNextHours[ilCurHour].tm_hour %= 24;

                                if (ilFound == 1)
                                {
                                   /* calculate relative time now */
                                   rgTM.prSection[ilSection].rNextMinutes[ilCurMin].tm_min += rgTM.prSection[ilSection].piMinOfHour[ilCurMin];

                                   /* check overflow */
                                   if (rgTM.prSection[ilSection].rNextMinutes[ilCurMin].tm_min > 60)
                                   {
                                      ilOp = (int)(rgTM.prSection[ilSection].rNextMinutes[ilCurMin].tm_min / 60);
                                      /* add offset to hours... */
                                      rgTM.prSection[ilSection].rNextHours[ilCurHour].tm_hour += ilOp;

                                      /* we have only 24 hours... */
                                      rgTM.prSection[ilSection].rNextHours[ilCurHour].tm_hour %= 24;
                                   }

                                   /* we have only 60 minutes */
                                   rgTM.prSection[ilSection].rNextMinutes[ilCurMin].tm_min %= 60;

                                   /* set flag, must be reseted */
                                   rgTM.prSection[ilSection].piMinIsSent[ilCurMin] = 1;
                                }
                                else
                                {
                                   /* set flag, must be reseted */
                                   rgTM.prSection[ilSection].piHourIsSent[ilCurHour] = 1;
                                }

                                /* count timer if necessary */
                                if (rgTM.prSection[ilSection].iNTime)
                                {       
                                   if (ilFound == 1)
                                      rgTM.prSection[ilSection].piMinCounter[ilCurMin]++;
                                   else
                                      rgTM.prSection[ilSection].piHourCounter[ilCurHour]++;

                                   if (ilFound == 1)
                                   {
                                      if (rgTM.prSection[ilSection].piMinCounter[ilCurMin] >= rgTM.prSection[ilSection].iNTime && !rgTM.prSection[ilSection].iUseInternalEvent)
                                         ilGoOn = TRUE;
                                      else
                                         ilGoOn = FALSE; 
                                   }
                                   else
                                   {
                                      if (rgTM.prSection[ilSection].piHourCounter[ilCurHour] >= rgTM.prSection[ilSection].iNTime && !rgTM.prSection[ilSection].iUseInternalEvent)
                                         ilGoOn = TRUE;
                                      else
                                         ilGoOn = FALSE; 
                                   }

                                   if (ilGoOn)
                                   {
                                      dbg(DEBUG,"<CheckTimer> %05d HOURS! calling DeleteTimer for section: %d (UseInternalEvent is: %d)", __LINE__, ilSection,  rgTM.prSection[ilSection].iUseInternalEvent);
                                      /* delete it now... */
                                      /* set members of FUNCTION... */
                                      rlFunction.iFunction = rgTM.prSection[ilSection].iFunction;
                                      rlFunction.iEventID = rgTM.prSection[ilSection].iEventID;
                                      rlFunction.iParentID = rgTM.prSection[ilSection].iParentID;
                                      rlFunction.iDestinationID = rgTM.prSection[ilSection].iDestinationID;
                                      if ((ilRC = DeleteTimer(&rlFunction, iCHECK_TIMER)) != RC_SUCCESS)
                                      {
                                         dbg(DEBUG,"<SendCommand> %05d DeleteTimer returns: %d", __LINE__, ilRC);
                                      }
 
                                      /* copy files to other machine */
                                      TransferAllFiles();
                                   }
                                }
                             }
                             else
                             {
                                /* debug message */
                                dbg(TRACE,"%05d SendCommand returns %d.", __LINE__, ilRC);
                             }
                          }
                       }
                    }
                 }
                 else
                 {
                    if (rgTM.prSection[ilSection].iNoOfMOH > 0)
                    {
                       /* reset this flag */
                       rgTM.prSection[ilSection].piMinIsSent[ilCurMin] = 0;
                    }
                    else
                    {
                       /* reset this flag */
                       rgTM.prSection[ilSection].piHourIsSent[ilCurHour] = 0;
                    }
                 }
              }
           }    
        } /* HOD */
        else
        {
           /* and search for minute... */
           for (ilCurMin=0;
                ilCurMin<rgTM.prSection[ilSection].iNoOfMOH;
                ilCurMin++)
           {
              /* is this correct time? */
              if (rgTM.prSection[ilSection].rNextMinutes[ilCurMin].tm_min == 
                  rlCurTime.tm_min)
              {
                 /* test n_time... */
                 if (!rgTM.prSection[ilSection].iNTime ||
                     (rgTM.prSection[ilSection].iNTime && 
                     (rgTM.prSection[ilSection].piMinCounter[ilCurMin] < 
                     rgTM.prSection[ilSection].iNTime)))
                 {
                    /* found correct hour */
                    /* send command now */
                    if (!rgTM.prSection[ilSection].piMinIsSent[ilCurMin])
                    {
                       /* check ctrl_sta and function... */
                       if (ctrl_sta == HSB_ACTIVE || ctrl_sta == HSB_STANDALONE || ((ctrl_sta == HSB_ACT_TO_SBY || ctrl_sta == HSB_STANDBY) && rgTM.prSection[ilSection].iFunction == iHSBADD))
                       {
                          if ((ilRC = SendCommand(
                                                  rgTM.prSection[ilSection].iUseInternalEvent,
                                                  rgTM.prSection[ilSection].iDestinationID, 
                                                  rgTM.prSection[ilSection].iParentID, 
                                                  rgTM.prSection[ilSection].iEventCommand, 
                                                  rgTM.prSection[ilSection].iDataLen, 
                                                  rgTM.prSection[ilSection].pcData,
                                                  ilSection)) == RC_SUCCESS)
                          {
                             /* calculate relative time now */
                             rgTM.prSection[ilSection].rNextMinutes[ilCurMin].tm_min += rgTM.prSection[ilSection].piMinOfHour[ilCurMin];

                             /* we have only 60 minutes */
                             rgTM.prSection[ilSection].rNextMinutes[ilCurMin].tm_min %= 60;

                             /* set flag, must be reseted */
                             rgTM.prSection[ilSection].piMinIsSent[ilCurMin] = 1;

                             /* count timer if necessary */
                             if (rgTM.prSection[ilSection].iNTime)
                             {
                                rgTM.prSection[ilSection].piMinCounter[ilCurMin]++;
                                if (rgTM.prSection[ilSection].piMinCounter[ilCurMin] >= rgTM.prSection[ilSection].iNTime && !rgTM.prSection[ilSection].iUseInternalEvent)
                                {
                                   dbg(DEBUG,"<CheckTimer> %05d MINUTES! calling DeleteTimer for section: %d (UseInternalEvent is: %d)", __LINE__, ilSection,  rgTM.prSection[ilSection].iUseInternalEvent);
                                   /* this must be a binary data type */
                                   /* delete it now... */
                                   /* set members of FUNCTION... */
                                   rlFunction.iFunction = rgTM.prSection[ilSection].iFunction;
                                   rlFunction.iEventID = rgTM.prSection[ilSection].iEventID;
                                   rlFunction.iParentID = rgTM.prSection[ilSection].iParentID;
                                   rlFunction.iDestinationID = rgTM.prSection[ilSection].iDestinationID;
                                   if ((ilRC = DeleteTimer(&rlFunction, iCHECK_TIMER)) != RC_SUCCESS)
                                   {
                                      dbg(DEBUG,"<SendCommand> %05d DeleteTimer returns: %d", __LINE__, ilRC);
                                   }

                                   /* copy files to other machine */
                                   TransferAllFiles();
                                }
                             }
                          }
                          else
                          {
                             /* debug message */
                             dbg(TRACE,"%05d SendCommand returns %d.", __LINE__, ilRC);
                          }
                       }
                    }
                 }
              }
              else
              {
                 /* reset this flag */
                 rgTM.prSection[ilSection].piMinIsSent[ilCurMin] = 0;
              }
           }
        } /* MOH */

        /**************************** Mei ********************************/
        if (rgTM.prSection[ilSection].iNoOfHOD <= 0 && rgTM.prSection[ilSection].iNoOfMOH <= 0 )
        {
            /* In case hour_o_min, min_o_hour and sec_o_min exist all at 1 time */
            if( rgTM.prSection[ilSection].iSecInterval > 0 )
            {
                if( (tlCurTime - rgTM.prSection[ilSection].tExecutedTime) >= rgTM.prSection[ilSection].iSecInterval )
                {
                    rgTM.prSection[ilSection].tExecutedTime = tlCurTime;

                    /* check ctrl_sta and function... */
                    if (ctrl_sta == HSB_ACTIVE || ctrl_sta == HSB_STANDALONE || ((ctrl_sta == HSB_ACT_TO_SBY || ctrl_sta == HSB_STANDBY) && rgTM.prSection[ilSection].iFunction == iHSBADD))
                    {
                        SendCommand( rgTM.prSection[ilSection].iUseInternalEvent,
                                     rgTM.prSection[ilSection].iDestinationID, 
                                     rgTM.prSection[ilSection].iParentID, 
                                     rgTM.prSection[ilSection].iEventCommand, 
                                     rgTM.prSection[ilSection].iDataLen, 
                                     rgTM.prSection[ilSection].pcData,
                                    ilSection);
                    }
                }
            }
        }
        /*****************************************************************/

     } /* REL */


     /*------------------------------------------------------------------*/
     /*------------------------------------------------------------------*/
     /*------------------------------------------------------------------*/
     else if (rgTM.prSection[ilSection].iType == iABS)
     {
        /* this is absolute !!!!! */
        ilOK = 0;
        if (rgTM.prSection[ilSection].iNoOfAbsDate > 0)
        {
           for (ilCurAbsDate=0,ilOK=0; ilCurAbsDate<rgTM.prSection[ilSection].iNoOfAbsDate; ilCurAbsDate++)
           {
              if (rgTM.prSection[ilSection].rAbsDate[ilCurAbsDate].iDayOfMonth == rlCurTime.tm_mday &&  rgTM.prSection[ilSection].rAbsDate[ilCurAbsDate].iMonthOfYear == rlCurTime.tm_mon+1)  
              {
                 /* found correct date */
                 ilOK = 1;
                 break;
              }
           }
        }
        else if(rgTM.prSection[ilSection].iNoOfDOW == 0) /* No Weekdays */
        {
           ilOK = 1;
        }

        /* search for day... */
        if ((CheckDay(rgTM.prSection[ilSection].iDaysOfWeek, rlCurTime.tm_wday) && !rgTM.prSection[ilSection].iNoOfAbsDate) || ilOK)
        {
           /* found correct day */
           /* search hour... */
           for (ilCurHour=0; 
                ilCurHour<rgTM.prSection[ilSection].iNoOfHOD;
                ilCurHour++)
           {
              if (rgTM.prSection[ilSection].piHoursOfDay[ilCurHour] ==
                  rlCurTime.tm_hour)
              {
                 /* found correct hour */
                 /* search minute */
                 for (ilCurMin=0;
                      ilCurMin<rgTM.prSection[ilSection].iNoOfMOH;
                      ilCurMin++)
                 {
                    if (rgTM.prSection[ilSection].piMinOfHour[ilCurMin] ==
                        rlCurTime.tm_min)
                    {
                       /* found correct minute */
                       /* send command now */
                       if (!rgTM.prSection[ilSection].piMinIsSent[ilCurMin])
                       {
                          /* check ctrl_sta and function... */
                          if (ctrl_sta == HSB_ACTIVE || ctrl_sta == HSB_STANDALONE || ((ctrl_sta == HSB_ACT_TO_SBY || ctrl_sta == HSB_STANDBY) && rgTM.prSection[ilSection].iFunction == iHSBADD))
                          {
                             if ((ilRC = SendCommand(
                                                     rgTM.prSection[ilSection].iUseInternalEvent,
                                                     rgTM.prSection[ilSection].iDestinationID, 
                                                     rgTM.prSection[ilSection].iParentID, 
                                                     rgTM.prSection[ilSection].iEventCommand, 
                                                     rgTM.prSection[ilSection].iDataLen, 
                                                     rgTM.prSection[ilSection].pcData,
                                                     ilSection)) == RC_SUCCESS)
                             {
                                /* set flag, must be reseted */
                                rgTM.prSection[ilSection].piMinIsSent[ilCurMin] = 1;
                             }
                             else
                             {
                                /* debug message */
                                dbg(TRACE,"%05d SendCommand returns %d.", __LINE__, ilRC);
                             }
                          }
                       }
                    }
                    else
                    {
                       /* reset flag */
                       rgTM.prSection[ilSection].piMinIsSent[ilCurMin] = 0;
                    }
                 }
              }
           }
        }
     }
     else
     {
        dbg(TRACE,"<CheckTimer> %05d unknown iType %d...", __LINE__, rgTM.prSection[ilSection].iType);
     }
  }

  /* bye bye */
  return RC_SUCCESS;
}

/******************************************************************************/
/* The send command routine                                                   */
/******************************************************************************/
static int SendCommand(int ipUseInternalEvent, int ipDestinationID, 
                              int ipParentID, int ipEventCommand, 
                              int ipDataLen, char *pcpData, int ipSection)
{
    /* it's a very simple structure type, Event->UserData that's all */
    int         ilRC = RC_FAIL;
    int         ilLen;
    EVENT           *prlEvent   = NULL;

    if (rgTM.prSection[ipSection].iValid == 1)
    {
        /* init */
        ilRC = RC_SUCCESS;

        /* calculate length */
        ilLen = sizeof(EVENT) + ipDataLen + 1;

        /* get memory */
        if ((prlEvent = (EVENT*)malloc(ilLen)) == NULL)
        {
            dbg(TRACE,"%05d cannot allocate memory for out event", __LINE__);
            prlEvent = NULL;
            return -1;
        }

        if (ipSection >= 0)
            dbg(DEBUG,"<SendCommand> Destination-ID %d from Section %d", ipDestinationID, ipSection);
        else
            dbg(DEBUG,"<SendCommand> Destination-ID %d", ipDestinationID);
        
        /* clear memory */
        memset((void*)prlEvent, 0x00, ilLen);

        /* set structure members */
        prlEvent->type          = SYS_EVENT;
        prlEvent->command       = ipEventCommand;
        prlEvent->originator    = ipParentID;
        prlEvent->retry_count   = 0;
        prlEvent->data_offset   = sizeof(EVENT);
        prlEvent->data_length   = ilLen - sizeof(EVENT);

        /* move user data to position... */
        if (ipUseInternalEvent)
        {
            /* copy behind event structure... */
            if (ipDataLen > 0)
            {
                memcpy((void*)((char*)prlEvent+sizeof(EVENT)),
                         (const void*)pcpData, (size_t)ipDataLen);
            }

            /* send it now */
            ilRC = que(QUE_PUT, ipDestinationID, mod_id, 3, ilLen, (char*)prlEvent);
            if (ilRC != RC_SUCCESS)
            {
                rgTM.prSection[ipSection].iValid = 0;
                dbg(TRACE,"<SendCommand> QUE_PUT returned <%d>. Setting section to INVALID!",ilRC);
            }
        }
        else if (ipDataLen > 0)
        {
            /* send only received data... */
            ilRC = que(QUE_PUT, ipDestinationID, mod_id, 3, ipDataLen, pcpData);
            if (ilRC != RC_SUCCESS)
            {
                rgTM.prSection[ipSection].iValid = 0;
                dbg(TRACE,"<SendCommand> QUE_PUT returned <%d>. Setting section to INVALID!",ilRC);
            }
        }
        else
        {
            /* don't know what to send */
            dbg(TRACE,"<SendCommand> %05d don't know what to send...", __LINE__);
            dbg(TRACE,"<SendCommand> %05d UseInternalEvent: %d, Dest-ID: %d, Parent-ID: %d, Event-Cmd: %d, DataLen: %d", __LINE__, ipUseInternalEvent, ipDestinationID, ipParentID, ipEventCommand, ipDataLen);
        }

        /* free memory */
        if (prlEvent != NULL)
        {
            free ((void*)prlEvent);
            prlEvent = NULL;
        }

        /* back */
    }
    return ilRC;
}

/******************************************************************************/
/* The add timer routine                                                      */
/******************************************************************************/
static int AddTimer(FUNCTION *prpFunction, int ipFlg)   
{
  /* simply we add to structure and config file... */
  /* format of data is FUNCTION->DATA_PERIOD->USERDATA */
  int               i;
  int               ilRC;
  int               ilAnswerRC;
  int               ilCurSection;
  int               ilNextHour;
  int ilEventIDBuff = 0;
  int               ilNextMinute;
  FileHeader        rlFileHeader;
  RecordHeader  rlRecordHeader;
  FUNCTION          rlAnswerFunction;
  DATA_PERIOD       *prlDataPeriod      = NULL;
  char              *pclData            = NULL;
  FILE              *fh             = NULL;
  time_t            tlCurTime;
  struct tm     *prlCurTime         = NULL;
  struct tm     rlCurTime;
  time_t            TimeDiff;
  time_t            next;
  struct tm     *_tm                    = NULL;
  struct tm     rlNextTime;

  dbg(DEBUG,"<AddTimer> ====== START ======");

  /* init something */
  ilAnswerRC = RC_SUCCESS;

  /* timestamp is necessary for new DATA... */
  if (ipFlg == iHANDLE_DATA || ipFlg == iUPDATE_TIMER)
    {
      /* first get current time and convert it */
      tlCurTime  = time(NULL);
      prlCurTime = (struct tm*)localtime(&tlCurTime);

      /* store time */
      memcpy((void*)&rlCurTime, (const void*)prlCurTime, sizeof(struct tm));

      /* save current timestamp */
      memcpy((void*)&prpFunction->rStartTime,
         (const void*)prlCurTime, (size_t)sizeof(struct tm));
    }

  /* set pointer */
  prlDataPeriod = (DATA_PERIOD*)((char*)prpFunction->data);
  pclData = (char*)prlDataPeriod->data;
    
  dbg(DEBUG,"<AddTimer> -------------------------------");
  dbg(DEBUG,"<AddTimer> received data..."); 
  dbg(DEBUG,"<AddTimer> Function->StartTime.tm_hour: %d", prpFunction->rStartTime.tm_hour);
  dbg(DEBUG,"<AddTimer> Function->StartTime.tm_min: %d", prpFunction->rStartTime.tm_min);
  dbg(DEBUG,"<AddTimer> Function->Function: %d", prpFunction->iFunction);
  dbg(DEBUG,"<AddTimer> Function->EventID: %d", prpFunction->iEventID);
  dbg(DEBUG,"<AddTimer> Function->ParentID: %d", prpFunction->iParentID);
  dbg(DEBUG,"<AddTimer> Function->DestinID: %d", prpFunction->iDestinationID);
  dbg(DEBUG,"<AddTimer> DataPeriod->day_o_week: %X", prlDataPeriod->rPeriod.day_o_week);
  dbg(DEBUG,"<AddTimer> DataPeriod->hour_o_day: %d", prlDataPeriod->rPeriod.hour_o_day);
  dbg(DEBUG,"<AddTimer> DataPeriod->min_o_hour: %d", prlDataPeriod->rPeriod.min_o_hour);
  dbg(DEBUG,"<AddTimer> DataPeriod->sec_o_min: %d", prlDataPeriod->rPeriod.sec_o_min);
  dbg(DEBUG,"<AddTimer> DataPeriod->n_times: %d", prlDataPeriod->rPeriod.n_times);
  dbg(DEBUG,"<AddTimer> DataPeriod->count: %d", prlDataPeriod->rPeriod.count);
  dbg(DEBUG,"<AddTimer> DataPeriod->period: %d", prlDataPeriod->rPeriod.period);
  dbg(DEBUG,"<AddTimer> DataLen <%d>", (int)prlDataPeriod->lDataLen);
  dbg(DEBUG,"<AddTimer> --------------------------------");
  /*save fuckin EventID here*/
   ilEventIDBuff = prpFunction->iEventID;

  /* try to open binary file for updating */
  dbg(DEBUG,"<AddTimer> %05d try to open <%s>", __LINE__, pcgBinaryFileName);
  if ((fh = fopen(pcgBinaryFileName, "r+b")) == NULL)
    {
      dbg(DEBUG,"<AddTimer> %05d can't open binary file <%s>, create it now", __LINE__, pcgBinaryFileName);
      if ((fh = fopen(pcgBinaryFileName, "a+b")) == NULL)
    {
      dbg(TRACE,"<AddTimer> %05d cannot create file <%s>...", __LINE__, pcgBinaryFileName);
      ilAnswerRC = RC_FAIL;
    }
    }

  if (ilAnswerRC == RC_SUCCESS)
    {
      /* File-Format is FileHeader, RecordHeader1, Record1, RecordHeader2,. */

      /* first read FileHeader */
      /* set to position 0L */
      fseek(fh, 0L, SEEK_SET);
      if ((ilRC = fread((void*)&rlFileHeader, 
            (size_t)sizeof(FileHeader), (size_t)1, fh)) == 0)
    {
      dbg(DEBUG,"<AddTimer> %05d fread returns: %d", __LINE__, ilRC);
      rlFileHeader.lLength = 0;
      rlFileHeader.iNumber = 0;
      rlFileHeader.iFId = rgTM.iNoOfSections + iSECTION_OFFSET;
    }

      /* now updating FileHeader */
      /* Length of file without fileheader */
      rlFileHeader.lLength += (sizeof(RecordHeader) + sizeof(FUNCTION) + 
                   sizeof(DATA_PERIOD) + prlDataPeriod->lDataLen); 

      /* total number of items */
      rlFileHeader.iNumber++; 

      /* id of next record, only for new Data */
      if (ipFlg == iHANDLE_DATA)
    rlFileHeader.iFId++;

      dbg(DEBUG,"<AddTimer> %05d FileHeader->Length: %d", __LINE__, (int)rlFileHeader.lLength);
      dbg(DEBUG,"<AddTimer> %05d FileHeader->Number: %d", __LINE__, rlFileHeader.iNumber);
      dbg(DEBUG,"<AddTimer> %05d FileHeader->FId: %d", __LINE__, rlFileHeader.iFId);

      /* here we set the members of answer-function... */
      if (ipFlg == iHANDLE_DATA)
    rlAnswerFunction.iEventID = rlFileHeader.iFId;

      /* write new fileheader to datafile */
      fseek(fh, 0L, SEEK_SET);
      if ((ilRC = fwrite((const void*)&rlFileHeader, (size_t)sizeof(FileHeader), (size_t)1, fh)) == 0)
    {
      dbg(DEBUG,"<AddTimer> %05d cannot write to %s", __LINE__, pcgBinaryFileName);

      /* can't live without new fileHeader */
      if (fh != NULL)
        {
          fclose(fh);
          fh = NULL;
        }

      /* massive error */
      ilAnswerRC = iTERMINATE;
    }

      if (ilAnswerRC == RC_SUCCESS)
    {
      /* close binary file and open it for appending... */
      if (fh != NULL)
        {
          fclose(fh);
          fh = NULL;
        }

      /* hope this is the correct modus */
      if ((fh = fopen(pcgBinaryFileName, "a+b")) == NULL)
        {
          dbg(DEBUG,"%05d cannot open file a+b <%s>...", __LINE__, pcgBinaryFileName);
          ilAnswerRC = RC_FAIL;
        }

      if (ilAnswerRC == RC_SUCCESS)
        {
                /* now add new record to file */
                /* set Record specifics */
          if (ipFlg == iHANDLE_DATA)
        rlRecordHeader.iRecordID = rlFileHeader.iFId;
          else
        rlRecordHeader.iRecordID = prpFunction->iEventID;
          rlRecordHeader.iRecordEventID = ilEventIDBuff;
          rlRecordHeader.lRecordLen = sizeof(FUNCTION) + 
        sizeof(DATA_PERIOD) + prlDataPeriod->lDataLen; 

          dbg(DEBUG,"%05d record len is: %d + %d + %d", __LINE__, sizeof(FUNCTION), sizeof(DATA_PERIOD), (int)prlDataPeriod->lDataLen); 
                
                /* first write record header */
          fseek(fh, 0L, SEEK_END);
          fwrite((const void*)&rlRecordHeader, 
             (size_t)sizeof(RecordHeader), (size_t)1, fh);

                /* then data */
                /* set Function specifics */
          if (ipFlg == iHANDLE_DATA)
        prpFunction->iEventID = rlFileHeader.iFId;
          fseek(fh, 0L, SEEK_END);
          fwrite((const void*)prpFunction, 
             (size_t)rlRecordHeader.lRecordLen, (size_t)1, fh);

                /* close binary file */
          if (fh != NULL)
        {
          fclose(fh);
          fh = NULL;
        }

                /* now we must add new data to internal structure... */
                /* first add number of sections */
          rgTM.iNoOfSections++;
          ilCurSection = rgTM.iNoOfSections - 1;
                
                /* get memory for sections */
          dbg(DEBUG,"%05d try to realloc %d * sizeof(TSection) bytes", __LINE__, rgTM.iNoOfSections);
          if ((rgTM.prSection = (TNTSection*)realloc(rgTM.prSection, rgTM.iNoOfSections*sizeof(TNTSection))) == NULL)
        {
          dbg(TRACE,"%05d cannot allocate (realloc) memory for sections (in AddTimer)", __LINE__);
          /* massive error */
          ilAnswerRC = iTERMINATE;
        }

          if (ilAnswerRC == RC_SUCCESS)
        {
          /* set structure members */
          memset((void*)&rgTM.prSection[ilCurSection], 0x00, sizeof(TNTSection));

          /* all ID's */
          rgTM.prSection[ilCurSection].iFunction      = prpFunction->iFunction;
          rgTM.prSection[ilCurSection].iEventID       = prpFunction->iEventID;
          rgTM.prSection[ilCurSection].iParentID      = prpFunction->iParentID;
          rgTM.prSection[ilCurSection].iDestinationID = prpFunction->iDestinationID;
          rgTM.prSection[ilCurSection].iEventCommand  = EVENT_DATA;
          rgTM.prSection[ilCurSection].iUseInternalEvent = 0;
          rgTM.prSection[ilCurSection].iFID =  ilEventIDBuff;
          /* all days */
          if (prlDataPeriod->rPeriod.day_o_week & SU)
            {
              rgTM.prSection[ilCurSection].iNoOfDOW++;
              rgTM.prSection[ilCurSection].iDaysOfWeek |= iSUN;
            }
          if (prlDataPeriod->rPeriod.day_o_week & MO)
            {
              rgTM.prSection[ilCurSection].iNoOfDOW++;
              rgTM.prSection[ilCurSection].iDaysOfWeek |= iMON;
            }
          if (prlDataPeriod->rPeriod.day_o_week & TU)
            {
              rgTM.prSection[ilCurSection].iNoOfDOW++;
              rgTM.prSection[ilCurSection].iDaysOfWeek |= iTUE;
            }
          if (prlDataPeriod->rPeriod.day_o_week & WE)
            {
              rgTM.prSection[ilCurSection].iNoOfDOW++;
              rgTM.prSection[ilCurSection].iDaysOfWeek |= iWED;
            }
          if (prlDataPeriod->rPeriod.day_o_week & TH)
            {
              rgTM.prSection[ilCurSection].iNoOfDOW++;
              rgTM.prSection[ilCurSection].iDaysOfWeek |= iTHU;
            }
          if (prlDataPeriod->rPeriod.day_o_week & FR)
            {
              rgTM.prSection[ilCurSection].iNoOfDOW++;
              rgTM.prSection[ilCurSection].iDaysOfWeek |= iFRI;
            }
          if (prlDataPeriod->rPeriod.day_o_week & SA)
            {
              rgTM.prSection[ilCurSection].iNoOfDOW++;
              rgTM.prSection[ilCurSection].iDaysOfWeek |= iSAT;
            }

          /* only one hour */
          if (prlDataPeriod->rPeriod.hour_o_day >= 0)
            {
              /* use only one hour */
              rgTM.prSection[ilCurSection].iNoOfHOD = 1;
              rgTM.prSection[ilCurSection].piHoursOfDay[0] = prlDataPeriod->rPeriod.hour_o_day;
            }
                    
          /* only one minute */
          if (prlDataPeriod->rPeriod.min_o_hour >= 0)
            {
              rgTM.prSection[ilCurSection].iNoOfMOH = 1;
              rgTM.prSection[ilCurSection].piMinOfHour[0] = prlDataPeriod->rPeriod.min_o_hour;
            }

          /* something else */
          rgTM.prSection[ilCurSection].iNTime = prlDataPeriod->rPeriod.n_times;
          for (i=0; i<iMAX_MINUTES; i++)
            {
              rgTM.prSection[ilCurSection].piMinCounter[i] = 0;
              rgTM.prSection[ilCurSection].piMinIsSent[i] = 0;
            }
          for (i=0; i<iMAX_HOURS; i++)
            {
              rgTM.prSection[ilCurSection].piHourCounter[i] = 0;
              rgTM.prSection[ilCurSection].piHourIsSent[i] = 0;
            }
          rgTM.prSection[ilCurSection].iType = prlDataPeriod->rPeriod.period;

          if (rgTM.prSection[ilCurSection].iType == iREL)
            {
              /* save current timestamp */
              memcpy((void*)&rgTM.prSection[ilCurSection].rStartTime,
                 (const void*)&rlCurTime, (size_t)sizeof(struct tm));

              /* this section isn't forever */
              if (rgTM.prSection[ilCurSection].iNTime > 0)
            {
              TimeDiff = next = 0;
              if (prlDataPeriod->rPeriod.hour_o_day > 0)
                TimeDiff += prlDataPeriod->rPeriod.hour_o_day * 3600;
              if (prlDataPeriod->rPeriod.min_o_hour > 0)
                TimeDiff += prlDataPeriod->rPeriod.min_o_hour * 60;

              next = mktime(&rlCurTime) + TimeDiff;
              _tm = (struct tm*)localtime(&next);
              memcpy(&rlNextTime, _tm, sizeof(struct tm));

              ilNextMinute = rlNextTime.tm_min;
              ilNextHour = rlNextTime.tm_hour;

              /* with an absolute date? */
              if ((rlNextTime.tm_mon != rlCurTime.tm_mon) ||
                  (rlNextTime.tm_mday != rlCurTime.tm_mday))
                {
                  /* set abs date */
                  rgTM.prSection[ilCurSection].iNoOfAbsDate = 1;
                  rgTM.prSection[ilCurSection].rAbsDate[0].iDayOfMonth = rlNextTime.tm_mday;
                  rgTM.prSection[ilCurSection].rAbsDate[0].iMonthOfYear = rlNextTime.tm_mon + 1;
                  dbg(DEBUG,"<AddTimer> DayOfMonth: %d", rgTM.prSection[ilCurSection].rAbsDate[0].iDayOfMonth);
                  dbg(DEBUG,"<AddTimer> MonthOfYear: %d", rgTM.prSection[ilCurSection].rAbsDate[0].iMonthOfYear);
                }
            }
              else
            {
              /* FOREVER */
              /* init something */
              TimeDiff = next = 0;
              next = mktime(&rlCurTime);
              _tm = (struct tm*)localtime(&next);
              memcpy(&rlNextTime, _tm, sizeof(struct tm));
              while (rlNextTime.tm_hour < rlCurTime.tm_hour || (rlNextTime.tm_hour <= rlCurTime.tm_hour && rlNextTime.tm_min < rlCurTime.tm_min))
                {
                  if (prlDataPeriod->rPeriod.hour_o_day > 0)
                TimeDiff += prlDataPeriod->rPeriod.hour_o_day * 3600;
                  if (prlDataPeriod->rPeriod.min_o_hour > 0)
                TimeDiff += prlDataPeriod->rPeriod.min_o_hour * 60;

                  next = mktime(&rlCurTime) + TimeDiff;
                  _tm = (struct tm*)localtime(&next);
                  memcpy(&rlNextTime, _tm, sizeof(struct tm));
                }

              /* set next hours */
              ilNextMinute = rlNextTime.tm_min;
              ilNextHour = rlNextTime.tm_hour;
            }

              /* for all hours */
              if (rgTM.prSection[ilCurSection].iNoOfHOD > 0)
            {
              /* calculate new relative time now */
              memcpy((void*)&rgTM.prSection[ilCurSection].rNextHours[0], (const void*)&rgTM.prSection[ilCurSection].rStartTime, (size_t)sizeof(struct tm));

              /* add offset to hours... */
              rgTM.prSection[ilCurSection].rNextHours[0].tm_hour = ilNextHour;
            }


              /* for all minutes */
              if (rgTM.prSection[ilCurSection].iNoOfMOH > 0)
            {
              /* calculate relative time now */
              memcpy((void*)&rgTM.prSection[ilCurSection].rNextMinutes[0], (const void*)&rgTM.prSection[ilCurSection].rStartTime, (size_t)sizeof(struct tm));
                            
              /* add offset to minutes... */
              rgTM.prSection[ilCurSection].rNextMinutes[0].tm_min = ilNextMinute;
            }
            }

          if (rgTM.prSection[ilCurSection].iNoOfMOH > 0 && rgTM.prSection[ilCurSection].iNoOfHOD > 0)
            {
              dbg(DEBUG,"<AddTimer> %05d set nextHour to %d and min to %d", __LINE__, rgTM.prSection[ilCurSection].rNextHours[0].tm_hour, rgTM.prSection[ilCurSection].rNextMinutes[0].tm_min);
            }
          else if (rgTM.prSection[ilCurSection].iNoOfMOH > 0 && rgTM.prSection[ilCurSection].iNoOfHOD <= 0)
            {
              dbg(DEBUG,"<AddTimer> %05d set min to %d", __LINE__, rgTM.prSection[ilCurSection].rNextMinutes[0].tm_min);
            }
          else if (rgTM.prSection[ilCurSection].iNoOfMOH <= 0 && rgTM.prSection[ilCurSection].iNoOfHOD > 0)
            {
              dbg(DEBUG,"<AddTimer> %05d set nextHour to %d and min to %d", __LINE__, rgTM.prSection[ilCurSection].rNextHours[0].tm_hour, rgTM.prSection[ilCurSection].rNextHours[0].tm_min);
            }

          /* user specific data */
          rgTM.prSection[ilCurSection].iDataLen = (UINT)prlDataPeriod->lDataLen; 

          if (prlDataPeriod->lDataLen > 0)
            {
              if ((rgTM.prSection[ilCurSection].pcData = (char*)malloc(prlDataPeriod->lDataLen)) == NULL)
            {
              dbg(TRACE,"%05d cannot allocate memory for pcData (AddTimer)", __LINE__);
              rgTM.prSection[ilCurSection].pcData = NULL;
              ilAnswerRC = iTERMINATE;
            }
              else
            {
              memcpy((void*)rgTM.prSection[ilCurSection].pcData, (const void*)pclData, prlDataPeriod->lDataLen);
            }
            }
        }
        }
    }
    }

  /* open file? */ 
  if (fh != NULL)
    {
      fclose(fh);
      fh = NULL;
    }

  if (ipFlg == iHANDLE_DATA)
    {
      if (ilAnswerRC != RC_SUCCESS)
    rlAnswerFunction.iEventID = ilAnswerRC;

      /* send answer to lib-function */
      if ((ilRC = SendCommand(1, prgEvent->originator, mod_id, EVENT_DATA, 
                  sizeof(FUNCTION), (char*)&rlAnswerFunction, -1)) != RC_SUCCESS)
    {
      dbg(TRACE,"%05d error sending command (%d)", __LINE__, ilRC);
      ilAnswerRC = iTERMINATE;
    }
    }

  /* in following cases we must terminatew the process */
  if (ilAnswerRC == iTERMINATE)
    {
      Terminate(0);
    }

  /* set flag */
  ipFlg = iADD;

  dbg(DEBUG,"<AddTimer> ====== END ======");

  /* bye bye */
  return ilAnswerRC;
}

/******************************************************************************/
/* The delete timer routine                                                   */
/******************************************************************************/
static int DeleteTimer(FUNCTION *prpFunction, int ipFlg)    
{
  /* delete from stucture and config file */
  /* format of data is FUNCTION->DATA_PERIOD->USERDATA */
  int   c;
  int   i;
  int   ilCurSec = 0;
  int   ilRC;
  int   ilAnswerRC; 
  long  l= 0;
  long  lLastPos= 0;
  long  lSourcePos= 0;
  long  lDestinationPos = 0;
  long  lNoOfBytes= 0;
  char  *pclTmpBuf= NULL;
  FILE  *fh= NULL;
  FILE  *tfh= NULL;
  FileHeader        rlFileHeader;
  RecordHeader  rlRecordHeader;
  FUNCTION          rlAnswerFunction;

  /* init something */
  ilAnswerRC = RC_SUCCESS;

  dbg(DEBUG,"<DeleteTimer> ====== START ======");

  dbg(DEBUG,"<DeleteTimer> -------------------------------");
  dbg(DEBUG,"<DeleteTimer> received data..."); 
  dbg(DEBUG,"<DeleteTimer> Function->StartTime.tm_hour: %d", prpFunction->rStartTime.tm_hour);
  dbg(DEBUG,"<DeleteTimer> Function->StartTime.tm_min: %d", prpFunction->rStartTime.tm_min);
  dbg(DEBUG,"<DeleteTimer> Function->Function: %d", prpFunction->iFunction);
  dbg(DEBUG,"<DeleteTimer> Function->EventID: %d", prpFunction->iEventID);
  dbg(DEBUG,"<DeleteTimer> Function->ParentID: %d", prpFunction->iParentID);
  dbg(DEBUG,"<DeleteTimer> Function->DestinID: %d", prpFunction->iDestinationID);
  dbg(DEBUG,"<DeleteTimer> -------------------------------");
    
  /* try to open file now */
  if ((fh = fopen(pcgBinaryFileName, "r+b")) == NULL)
    {
      dbg(DEBUG,"<DeleteTimer> can't open binary file <%s>...", pcgBinaryFileName);
      ilAnswerRC = RC_FAIL;
    }
    
  if (ilAnswerRC == RC_SUCCESS)
    {
      /* first read FileHeader */
      /* set to position 0L */
      fseek(fh, 0L, SEEK_SET);
      if ((ilRC = fread((void*)&rlFileHeader, (size_t)sizeof(FileHeader), (size_t)1, fh)) == 0)
    {
      dbg(DEBUG,"<DeleteTimer> %05d can't read fileHeader", __LINE__);
      if (fh != NULL)
        {
          fclose(fh);
          fh = NULL;
        }
      ilAnswerRC = RC_FAIL;
    }

      if (ilAnswerRC == RC_SUCCESS)
    {
      /* only if more than 1 occurrence */
      if (rlFileHeader.iNumber > 0)
        {
                /* for all records and all sections*/
         
          for (ilCurSec = 0;ilCurSec < rgTM.iNoOfSections ;ilCurSec++)
        {
          dbg(DEBUG,"<DeleteTimer> %05d found Event - ID %d ", __LINE__,rgTM.prSection[ilCurSec].iEventID);
          /*
          ** USC iFID doesn't seem to be maintained, try iEventID instead
          */
          if(rgTM.prSection[ilCurSec].iEventID == prpFunction->iEventID)
            {
              for (i=0; i<rlFileHeader.iNumber && ilAnswerRC == RC_SUCCESS; i++)      
            {
              /* read records */
              fread((void*)&rlRecordHeader, (size_t)sizeof(RecordHeader), (size_t)1, fh);
              dbg(DEBUG,"<DeleteTimer> %05d found Record-ID %d, Len %d", __LINE__, rlRecordHeader.iRecordID, (int)rlRecordHeader.lRecordLen);
              dbg(DEBUG,"<DeleteTimer> %05d found Event-ID %d, ", __LINE__, prpFunction->iEventID);
              
              if (rlRecordHeader.iRecordID ==  rgTM.prSection[ilCurSec].iEventID)
                {
                  /* found record to delete */
                  /* set position before record header... */
                  fseek(fh, -(long)(sizeof(RecordHeader)), SEEK_CUR);
                  
                  /* save destination position */
                  lDestinationPos = ftell(fh);
                  
                  /* calulate source position */
                  fseek(fh, (sizeof(RecordHeader)+rlRecordHeader.lRecordLen), SEEK_CUR);
                  
                  /* save soure position... */
                  lSourcePos = ftell(fh);
                  
                  /* seek to end */
                  fseek(fh, 0L, SEEK_END);
              
                  /* get last position */
                  lLastPos = ftell(fh);
                  
                  /* calculate number of bytes to move */
                  lNoOfBytes = lLastPos - lSourcePos;
                  
                  if (lNoOfBytes > 0)
                {
                  /* get memory for rest of file */
                  if ((pclTmpBuf = (char*)malloc(lNoOfBytes)) == NULL)
                    {
                      dbg(TRACE,"%05d can't allocate memory for delete record", __LINE__);
                      pclTmpBuf = NULL;
                      ilAnswerRC = iTERMINATE;
                    }else{
                      memset((void*)pclTmpBuf, 0x00, (size_t)lNoOfBytes);
                      fseek(fh, lSourcePos, SEEK_SET);
                      fread((void*)pclTmpBuf, (size_t)lNoOfBytes, (size_t)1, fh);
                      fseek(fh, lDestinationPos, SEEK_SET);
                      fwrite((const void*)pclTmpBuf, (size_t)lNoOfBytes, (size_t)1, fh);
                      dbg(DEBUG,"<DeleteTimer> %05d ", __LINE__);
                    }
                }
                  
                  if (ilAnswerRC == RC_SUCCESS)
                {
                  lLastPos -= (sizeof(RecordHeader) + rlRecordHeader.lRecordLen);
                  dbg(DEBUG,"<DeleteTimer> %05d write EOF at pos: %d", __LINE__, (int)lLastPos);
                  
                  /* now update FileHeader */
                  /* Length of file without fileheader */
                  rlFileHeader.lLength -= (sizeof(RecordHeader) + rlRecordHeader.lRecordLen); 
                  /* total number of items */
                  rlFileHeader.iNumber--;
                  
                  /* write new fileheader to datafile */
                  fseek(fh, 0L, SEEK_SET);
                  fwrite((const void*)&rlFileHeader, 
                     (size_t)sizeof(FileHeader), (size_t)1, fh);
                  
                  /* free this memory */
                  if (pclTmpBuf != NULL)
                    {
                      free ((void*)pclTmpBuf);
                      pclTmpBuf = NULL;
                    }
                }

                  /* break here */
                  break;
                }else{
                  /* set pos after this record */
                  fseek(fh, rlRecordHeader.lRecordLen, SEEK_CUR);
                }
            }/*end for*/
            }/*end if*/
        }/*end for*/ 

          if (ilAnswerRC == RC_SUCCESS)
        {
          /* check total number of items in file... */
          if (!rlFileHeader.iNumber)
            {
              /* this is only the FileHeader without any RecordData.
             we remove this file
              */
              if (fh != NULL)
            {
              fclose(fh);
              fh = NULL;
            }

              /* here we must delete this file... */
              if ((ilRC = remove(pcgBinaryFileName)) != 0)
            {
              dbg(TRACE,"<DeleteTimer> %05d cannot remove file %d", __LINE__, ilRC);
            }else{
              dbg(DEBUG,"<DeleteTimer> %05d calling DeleteRemoteBinFile...", __LINE__);
              DeleteRemoteBinFile();
            }
            }else{
              if (lLastPos > 0)
            {
              if ((tfh = fopen(pcgTmpFileName,"wb")) == NULL)
                {
                  dbg(TRACE,"<DeleteTimer> %05d cannot create temporary file...", __LINE__);
                  ilAnswerRC = iTERMINATE;
                }else{
                  dbg(DEBUG,"<DeleteTimer> %05d write tmp-file with %d bytes...", __LINE__, (int)lLastPos);
                  fseek(fh,  0L, SEEK_SET);
                  fseek(tfh, 0L, SEEK_SET);
                  for (l=0; l<lLastPos; l++)
                {
                  c = getc(fh);
                  putc(c, tfh);
                }
                }
            }

              if (ilAnswerRC == RC_SUCCESS)
            {
              /* close file */
              dbg(DEBUG,"<DeleteTimer> %05d close files now", __LINE__);
              if (fh != NULL)
                {
                  fclose(fh);
                  fh = NULL;
                }
              if (tfh != NULL)
                {
                  fclose(tfh);
                  tfh = NULL;
                }

              if (lLastPos > 0)
                {
                  /* delete old file... */
                  dbg(DEBUG,"%05d remove file <%s>", __LINE__, pcgBinaryFileName);
                  if ((ilRC = remove((const char*)pcgBinaryFileName)) != 0)
                {
                  dbg(DEBUG,"<DeleteTimer> %05d cannot remove file <%s>", __LINE__, pcgBinaryFileName);
                }

                  /* and create the new */
                  dbg(DEBUG,"%05d rename file now", __LINE__);
                  if ((ilRC = rename((const char*)pcgTmpFileName, (const char*)pcgBinaryFileName)) != 0)
                {
                  dbg(TRACE,"<DeleteTimer> %05d cannot rename file <%s> to <%s>", __LINE__, pcgTmpFileName, pcgBinaryFileName);
                }
                }
            }
            }
        }
        }else{
                /* this should be never reached!!! */
                /* close file */
          if (fh != NULL)
        {
          fclose(fh);
          fh = NULL;
        }

                /* here we must delete this file... */
          if ((ilRC = remove((const char *)pcgBinaryFileName)) != 0)
        {
          dbg(TRACE,"<DeleteTimer> %05d cannot remove file %d", __LINE__, ilRC);
        }
          else
        {
          dbg(DEBUG,"<DeleteTimer> %05d calling DeleteRemoteBinFile...", __LINE__);
          DeleteRemoteBinFile();
        }
        }

      if (ilAnswerRC == RC_SUCCESS)
        {
                /* delete it in memory */
          dbg(DEBUG,"<DeleteTimer> %05d delete it in memory now...",__LINE__);
          for (i=0; i<rgTM.iNoOfSections; i++)
        {
          dbg(DEBUG,"<DeleteTimer> %05d  i = %d, compare ID %d and %d", __LINE__, i, rgTM.prSection[i].iEventID, prpFunction->iEventID);
          if (rgTM.prSection[i].iFID == prpFunction->iEventID)
            {
              dbg(DEBUG," %05d found Event ** %d **", __LINE__, rgTM.prSection[i].iFID);
              dbg(DEBUG," %05d found Section Id ** %d **", __LINE__, prpFunction->iEventID);
              /* is this a dynamic section? */
              if (rgTM.prSection[i].iUseInternalEvent == 1)
            {
              /* no it's a section produces by configuration file */
              dbg(DEBUG,"<DeleteTimer> %05d ERROR ERROR ERROR.....", __LINE__);
              dbg(DEBUG,"<DeleteTimer> %05d can't delete sections from configuration file...", __LINE__);
              dbg(DEBUG,"<DeleteTimer> %05d so do nothing!!!", __LINE__);
            }else{
              /* this is a dynamic section... */
              /* first delete user data of this section */
              if (rgTM.prSection[i].pcData != NULL)
                {       
                  free ((void*)rgTM.prSection[i].pcData);
                  rgTM.prSection[i].pcData = NULL;
                }   

              /* is this last section? */
              dbg(DEBUG,"<DeleteTimer> %05d acually number of section: %d and counter i: %d", __LINE__, rgTM.iNoOfSections, i);
              if (i < rgTM.iNoOfSections - 1)
                {
                  /* move structure and delete last */
                  dbg(DEBUG,"<DeleteTimer> %05d move %d sections from pos %d to %d...", __LINE__, rgTM.iNoOfSections-i-1, i+1, i);
                  memmove((void*)&rgTM.prSection[i], (const void*)&rgTM.prSection[i+1], (size_t)((rgTM.iNoOfSections - i - 1)*sizeof(TNTSection)));
                }

              if (rgTM.iNoOfSections > 0)
                {
                  /* decr no of sections */
                  --rgTM.iNoOfSections;
                }

              dbg(DEBUG,"<DeleteTimer> %05d set new no. of sections. %d",__LINE__, rgTM.iNoOfSections);
            }       
            }
        }
        }
    }
    }

  /* open file? */
  if    (fh != NULL)
    {
      fclose(fh);
      fh = NULL;
    }
  if (tfh != NULL)
    {
      fclose(tfh);
      tfh = NULL;
    }
  if (pclTmpBuf != NULL)
    {
      free((void*)pclTmpBuf);
      pclTmpBuf = NULL;
    }

  /* must i send a answer to lib function? */
  if (ipFlg == iHANDLE_DATA)
    {
      /* set returncode */
      rlAnswerFunction.iEventID = ilAnswerRC;

      /* send answer to lib-function */
      if ((ilRC = SendCommand(1, prgEvent->originator, mod_id, EVENT_DATA, 
                  sizeof(FUNCTION), (char*)&rlAnswerFunction, -1)) != RC_SUCCESS)
    {
      dbg(TRACE,"%05d error sending command (%d)", __LINE__, ilRC);
      ilAnswerRC = iTERMINATE;
    }
    }

  /* in following cases we must terminatew the process */
  if (ilAnswerRC == iTERMINATE)
    {
      Terminate(0);
    }

  /* set flag */
  ipFlg = iDELETE_TIMER;

  dbg(DEBUG,"<DeleteTimer> ====== END ======");

  /* bye bye */
  return ilAnswerRC;
}

/******************************************************************************/
/* The change timer routine                                                   */
/******************************************************************************/
static int UpdateTimer(FUNCTION *prpFunction, int ipFlg)    
{
    /* change in structure and config file */
    /* format of data is FUNCTION->DATA_PERIOD->USERDATA */
    int         ilRC;
    int         ilAnswerRC;         
    FUNCTION        rlAnswerFunction;

    dbg(DEBUG,"<UpdateTimer> ===== START =====");

    /* init it */
    ilAnswerRC = ilRC = RC_SUCCESS;

    /* simply delete this entry...  but save old ID!!! */
    dbg(DEBUG,"<UpdateTimer> %05d calling DeleteTimer...", __LINE__);
    if ((ilRC = DeleteTimer(prpFunction, iUPDATE_TIMER)) == RC_SUCCESS)
    {
        dbg(DEBUG,"<UpdateTimer> %05d DeleteTimer returns RC_SUCCESS", __LINE__);
        dbg(DEBUG,"<UpdateTimer> %05d calling AddTimer...", __LINE__);
        if ((ilRC = AddTimer(prpFunction, iUPDATE_TIMER)) == RC_SUCCESS)
        {
            dbg(DEBUG,"%05d AddTimer in UpdateTimer returns RC_SUCCESS", __LINE__);
        }
        else    
        {
            dbg(TRACE,"%05d AddTimer in UpdateTimer returns: %d", __LINE__, ilRC);
            ilAnswerRC = iTERMINATE;
        }
    }
    else
    {
        dbg(TRACE,"%05d DeleteTimer in UpdateTimer returns: %d", __LINE__, ilRC);
        ilAnswerRC = iTERMINATE;
    }

    /* must i send a answer to lib function? */
    if (ipFlg == iHANDLE_DATA)
    {
        /* set returncode */
        rlAnswerFunction.iEventID = ilAnswerRC;

        /* send answer to lib-function */
        if ((ilRC = SendCommand(1, prgEvent->originator, mod_id, EVENT_DATA, 
                          sizeof(FUNCTION), (char*)&rlAnswerFunction, -1)) != RC_SUCCESS)
        {
            dbg(TRACE,"%05d error sending command (%d)", __LINE__, ilRC);
            ilAnswerRC = iTERMINATE;
        }
    }

    /* in following cases we must terminatew the process */
    if (ilAnswerRC == iTERMINATE)
    {
        Terminate(0);
    }

    /* set Flag */
    ipFlg = iUPDATE_TIMER;

    dbg(DEBUG,"<UpdateTimer> ===== END =====");

    /* everything looks fine */
    return ilAnswerRC;
}

/******************************************************************************/
/* The UPDBuf timer routine                                                   */
/******************************************************************************/
static int UPDBufTimer(FUNCTION *prpFunction, int ipFlg)    
{
    /* here we update only the buffer (USERDATA), not FUCTION, DATA_PERIOD */
    /* format of data is FUNCTION->DATA_PERIOD->USERDATA */
    int             ilRC;
    int             ilCurRecord;
    int             ilAnswerRC;
    RecordHeader    rlRecordHeader;
    RecordHeader    rlNewRecordHeader;
    FileHeader      rlFileHeader;
    char                *pclNewData         = NULL;
    char                *pclRecord          = NULL;
    char                *pclNewRecord       = NULL;
    FUNCTION            *prlFunction        = NULL;
    FUNCTION            *prlNewFunction = NULL;
    DATA_PERIOD     *prlDataPeriod      = NULL;
    DATA_PERIOD     *prlNewDataPeriod   = NULL;
    FILE                *fh                     = NULL;
    FUNCTION            rlAnswerFunction;

    dbg(DEBUG,"<UPDBufTimer> ===== START =====");

    /* init it */
    ilAnswerRC = RC_SUCCESS;

    /* open file... */
    if ((fh = fopen(pcgBinaryFileName,"r+b")) == NULL)
    {
        dbg(DEBUG,"%05d can't open binary file <%s>", __LINE__, pcgBinaryFileName);
        ilAnswerRC = RC_FAIL;
    }
    
    if (ilAnswerRC == RC_SUCCESS)
    {
        /* read fileheader to get informations about it... */
        fseek(fh, 0L, SEEK_SET);
        if ((ilRC = fread((void*)&rlFileHeader, (size_t)sizeof(FileHeader), (size_t)1, fh)) == 0)
        {
            dbg(DEBUG,"%05d can't read fileheader (UPDBufTimer)", __LINE__);
            if (fh != NULL)
            {
                fclose(fh);
                fh = NULL;
            }
            ilAnswerRC = RC_FAIL;
        }

        if (ilAnswerRC == RC_SUCCESS)
        {
            /* serach the record... */
            for (ilCurRecord=0; ilCurRecord<rlFileHeader.iNumber && ilAnswerRC == RC_SUCCESS; ilCurRecord++)
            {
                /* read record Header of current record... */
                fread((void*)&rlRecordHeader, 
                                (size_t)sizeof(RecordHeader), (size_t)1, fh);

                /* is this the record we serach? */
                if (rlRecordHeader.iRecordID == prpFunction->iEventID)
                {
                    /* found record */
                    /* save Data, first get memory for it */
                    if ((pclRecord = (char*)malloc(rlRecordHeader.lRecordLen)) == NULL)
                    {
                        dbg(TRACE,"%05d can't allocate memory for record (UPDBufTimer)", __LINE__);
                        pclRecord = NULL;
                        ilAnswerRC = iTERMINATE;
                    }
                    else
                    {
                        /* clear buffer */
                        memset((void*)pclRecord, 0x00, (size_t)rlRecordHeader.lRecordLen);

                        /* ...and the following record */
                        fread((void*)pclRecord, (size_t)rlRecordHeader.lRecordLen, (size_t)1, fh);

                        /* set pointer... */
                        prlFunction     = (FUNCTION*)((char*)pclRecord);
                        prlDataPeriod   = (DATA_PERIOD*)((char*)prlFunction->data);

                        /* close file */
                        if (fh != NULL)
                        {
                            fclose(fh);
                            fh = NULL;
                        }
                    }
                    
                    /* break here */
                    break;
                }
                else
                {
                    fseek(fh, rlRecordHeader.lRecordLen, SEEK_CUR);
                }
            }

            if (ilAnswerRC == RC_SUCCESS)
            {
                /* we found entry... */
                if (pclRecord != NULL)
                {
                    /* if we are here, we read the old data... */
                    /* calculate length and ID of new record... */
                    rlNewRecordHeader.iRecordID  = rlRecordHeader.iRecordID;
                    rlNewRecordHeader.lRecordLen = 
                                sizeof(FUNCTION)        +
                                sizeof(DATA_PERIOD)     + 
                                (long)(((DATA_PERIOD*)((char*)prpFunction->data))->lDataLen);

                    dbg(DEBUG,"%05d calculate new recordLen: %d", __LINE__, (int)rlNewRecordHeader.lRecordLen);

                    /* get memory for new record... */
                    if ((pclNewRecord = (char*)malloc(rlNewRecordHeader.lRecordLen)) == NULL)
                    {
                        dbg(TRACE,"%05d can't allocate memory for new record (UPDBufTimer)", __LINE__);
                        pclNewRecord = NULL;
                        ilAnswerRC = iTERMINATE;
                    }
                    else
                    {
                        /* clear buffer */
                        memset((void*)pclNewRecord, 0x00, (size_t)rlNewRecordHeader.lRecordLen);

                        /* set pointer */
                        prlNewFunction  = (FUNCTION*)((char*)pclNewRecord);
                        prlNewDataPeriod    = (DATA_PERIOD*)((char*)prlNewFunction->data);
                        pclNewData          = (char*)prlNewDataPeriod->data;

                        /* copy data to new record... */
                        /* first Function-Structure.... */
                        memcpy((void*)prlNewFunction, (const void*)prlFunction, (size_t)sizeof(FUNCTION));

                        /* ...then DataPeriod-Structure... */
                        memcpy((void*)prlNewDataPeriod, (const void*)prlDataPeriod, (size_t)sizeof(DATA_PERIOD));

                        /* ...and now the NEW!! User-Data... */
                        memcpy((void*)pclNewData, 
                            (const void*)(((DATA_PERIOD*)((char*)prpFunction->data))->data),
                            (size_t)(((DATA_PERIOD*)((char*)prpFunction->data))->lDataLen));

                        /* and the NEW! User-Data-Len */
                        prlNewDataPeriod->lDataLen = 
                            (long)(((DATA_PERIOD*)((char*)prpFunction->data))->lDataLen);

                        /* delete memory we don't need */
                        if (pclRecord != NULL)
                        {
                            free((void*)pclRecord);
                            pclRecord = NULL;
                        }
                    }
                }

                if (ilAnswerRC == RC_SUCCESS)
                {
                    /* now we can delete this entry in binary file and memory */
                    dbg(DEBUG,"<UPDBufTimer> %05d calling DeleteTimer...", __LINE__);
                    if ((ilRC = DeleteTimer(prpFunction, iUPDBUF_TIMER)) == RC_SUCCESS)
                    {
                        /* successfull delete timer... */
                        dbg(DEBUG,"%05d Delete Timer in UPDBuf returns with RC_SUCCESS", __LINE__);

                        /* now Add the new Entry and update it */
                        if ((ilRC = AddTimer(prlNewFunction, iUPDBUF_TIMER)) == RC_SUCCESS)
                        {
                            /* successfull add timer... */
                            dbg(DEBUG,"%05d Add Timer in UPDBuf returns with RC_SUCCESS", __LINE__);
                        }
                        else
                        {
                            dbg(TRACE,"%05d Can't add entry (int UPDBuf) %d", __LINE__, ilRC);
                            ilAnswerRC = iTERMINATE;
                        }
                    }
                    else
                    {
                        dbg(TRACE,"%05d Can't delete entry (int UPDBuf) %d", __LINE__, ilRC);
                        ilAnswerRC = iTERMINATE;
                    }
                }
            }
        }

        /* free memory */
        if (pclNewRecord != NULL)
        {
            free((void*)pclNewRecord);
            pclNewRecord = NULL;
        }
        if (pclRecord != NULL)
        {
            free((void*)pclRecord);
            pclRecord = NULL;
        }
    }

    /* open file ? */
    if (fh != NULL)
    {
        fclose(fh);
        fh = NULL;
    }

    /* must i send a answer to lib function? */
    if (ipFlg == iHANDLE_DATA)
    {
        /* set returncode */
        rlAnswerFunction.iEventID = ilAnswerRC;

        /* send answer to lib-function */
        if ((ilRC = SendCommand(1, prgEvent->originator, mod_id, EVENT_DATA, 
                          sizeof(FUNCTION), (char*)&rlAnswerFunction, -1)) != RC_SUCCESS)
        {
            dbg(TRACE,"%05d error sending command (%d)", __LINE__, ilRC);
            ilAnswerRC = iTERMINATE;
        }
    }

    /* in following cases we must terminatew the process */
    if (ilAnswerRC == iTERMINATE)
    {
        Terminate(0);
    }

    /* set Flag */
    ipFlg = iUPDBUF_TIMER;

    dbg(DEBUG,"<UPDBufTimer> ===== END =====");

    /* bye bye */
    return ilAnswerRC;
}

/******************************************************************************/
/* The HSBAdd timer routine                                                   */
/******************************************************************************/
static int HSBAddTimer(FUNCTION *prpFunction, int ipFlg)    
{
    int     ilRC;
    /* format of data is FUNCTION->DATA_PERIOD->USERDATA */
    /* so, the time scheduler should only work if 
        ctrl_sta = HSB_ACTIVE,HSB_STANDALONE and HSB_ACT_TO_SBY. 
    */
    dbg(DEBUG,"<HSBAddTimer> ===== START =====");
    ilRC = AddTimer(prpFunction, ipFlg);
    dbg(DEBUG,"<HSBAddTimer> ===== END =====");
    return ilRC;
}

/******************************************************************************/
/* The delete mod-id timer routine                                            */
/******************************************************************************/
static int DelModIDTimer(FUNCTION *prpFunction, int ipFlg)
{
  /* here event_id is set with mod_id!! */
  int               ilRC;
  int               ilAnswerRC;
  int               ilModID;
  int               ilFound;
  int               ilCurRecord;
  FileHeader        rlFileHeader;
  RecordHeader  rlRecordHeader;
  FUNCTION          *prlFunction        = NULL;
  char              *pclRecord          = NULL;
  FILE              *fh                     = NULL;
  FUNCTION          rlAnswerFunction;

  dbg(DEBUG,"<DelModIDTimer> ===== START =====");

  /* save mod_id */
  ilModID = prpFunction->iEventID;
  ilAnswerRC = RC_SUCCESS;

  /* serach all mod_id in file, get corresponding EventID and delete 
     all these entries... 
  */
  do
    {
      /* file isn't open, open it now */
      if ((fh = fopen(pcgBinaryFileName,"rb")) == NULL)
    {
      dbg(TRACE,"<DelModIDTimer> %05d cannot open binary file", __LINE__);
      ilAnswerRC = RC_FAIL;
    } 

      if (ilAnswerRC == RC_SUCCESS)
    {
      /* if we open file here, we must read fileHeader... */
      /* set pointer to first position */
      fseek(fh, 0L, SEEK_SET);
      if ((ilRC = fread((void*)&rlFileHeader, 
                (size_t)sizeof(FileHeader), (size_t)1, fh)) == 0)
        {
          dbg(TRACE,"<DelModIDTimer> %05d can't read fileHeader (%d)", __LINE__, ilRC);
          if (fh != NULL)
        {
          fclose(fh);
          fh = NULL;
        }
          ilAnswerRC = RC_FAIL;
        }
    }

      if (ilAnswerRC == RC_SUCCESS)
    {
      /* for all records */
      for (ilCurRecord=0, ilFound=0; ilCurRecord<rlFileHeader.iNumber && !ilFound && ilAnswerRC == RC_SUCCESS; ilCurRecord++)
        {
                /* read records */
                /* first header... */
          fread((void*)&rlRecordHeader, 
            (size_t)sizeof(RecordHeader), (size_t)1, fh);

                /* then record itself */
          if ((pclRecord = (char*)malloc(rlRecordHeader.lRecordLen)) == NULL)
        {
          dbg(DEBUG,"<DelModIDTimer> %05d Can't allocate memory for record", __LINE__);
          pclRecord = NULL;
          if (fh != NULL)
            {
              fclose(fh);
              fh = NULL;
            }
          ilAnswerRC = iTERMINATE;
        }
          else
        {
          /* clear buffer */
          memset((void*)pclRecord, 0x00, (size_t)rlRecordHeader.lRecordLen);

          /* ...and the following record */
          fread((void*)pclRecord, 
            (size_t)rlRecordHeader.lRecordLen, (size_t)1, fh);

          /* set pointer... */
          prlFunction   = (FUNCTION*)((char*)pclRecord);

          /* compare ID's */
          if (prlFunction->iDestinationID == ilModID)
            {
              /* found entry!! */
              /* set flag */
              ilFound = 1;

              /* close file */
              if (fh != NULL)
            {
              fclose(fh);
              fh = NULL;
            }

              /* call DeleteTimer */
              dbg(DEBUG,"<DelModIDTimer> %05d calling DeleteTimer...", __LINE__);
              if ((ilRC = DeleteTimer(prlFunction, iDELMODID_TIMER)) != RC_SUCCESS)
            {
              dbg(TRACE,"<DelModIDTimer> %05d DeleteTimer returns without RC_SUCCESS", __LINE__);
              ilAnswerRC = iTERMINATE;
            }
            }

          /* delete memory we don't need */
          if (pclRecord != NULL)
            {
              free((void*)pclRecord);
              pclRecord = NULL;
            }
        }
        }
    }   
    } while (ilFound && ilAnswerRC == RC_SUCCESS);
    
  /* close binary file */
  if (fh != NULL)
    {
      fclose(fh);
      fh = NULL;
    }

  /* must i send a answer to lib function? */
  if (ipFlg == iHANDLE_DATA)
    {
      /* set returncode */
      rlAnswerFunction.iEventID = ilAnswerRC;

      /* send answer to lib-function */
      if ((ilRC = SendCommand(1, prgEvent->originator, mod_id, EVENT_DATA, 
                  sizeof(FUNCTION), (char*)&rlAnswerFunction, -1)) != RC_SUCCESS)
    {
      dbg(TRACE,"%05d error sending command (%d)", __LINE__, ilRC);
      ilAnswerRC = iTERMINATE;
    }
    }

  /* in following cases we must terminatew the process */
  if (ilAnswerRC == iTERMINATE)
    {
      Terminate(0);
    }

  /* set flag */
  ipFlg = iDELMODID_TIMER;

  dbg(DEBUG,"<DelModIDTimer> ===== END =====");

  /* bye bye */
  return ilAnswerRC;
}

/******************************************************************************/
/* The  TransferAllFiles routine                                              */
/******************************************************************************/
static void TransferAllFiles(void)
{
    int     ilRC;

    /* exist this file? */
    if (FileExist(pcgBinaryFileName) == RC_SUCCESS)
    {
        /* transfer dat file to remote machine... */
        if ((ilRC = TransferFile(pcgBinaryFileName)) != RC_SUCCESS)
        {
            set_system_state(HSB_STANDALONE);
            dbg(TRACE,"<TAF> %05d TransferFile <%s> returns: %d", __LINE__, pcgBinaryFileName, ilRC);
        }
    }
    else
    {
        dbg(DEBUG,"<TAF> %05d File <%s> doesn't exist...", __LINE__, pcgBinaryFileName);
    }

    if (FileExist(pcgCfgFileName) == RC_SUCCESS)
    {
        /* transfer configuration file to remote machine... */
        if ((ilRC = TransferFile(pcgCfgFileName)) != RC_SUCCESS)
        {
            dbg(TRACE,"<TAF> %05d TransferFile <%s> returns: %d", __LINE__, pcgCfgFileName, ilRC);
            set_system_state(HSB_STANDALONE);
        }
    }
    else
    {
        dbg(DEBUG,"<TAF> %05d File <%s> doesn't exist...", __LINE__, pcgCfgFileName);
    }

    return;
}

/******************************************************************************/
/* The DeleteRemoteBinFile routine                                            */
/******************************************************************************/
static void DeleteRemoteBinFile(void)
{
    int     ilRC;
    EVENT       rlEvent;

    /* clear memory */
    memset((void*)&rlEvent, 0x00, sizeof(EVENT));

    /* set structure members */
    rlEvent.type            = SYS_EVENT;
    rlEvent.command         = iDELETE_BIN_FILE;
    rlEvent.originator  = mod_id;
    rlEvent.retry_count     = 0;
    rlEvent.data_offset     = sizeof(EVENT);
    rlEvent.data_length     = 0;

    dbg(DEBUG,"<DeleteRemoteBinFile> %05d calling RemoteQue...", __LINE__);

    /* HSB needs it */ 
    if ((ilRC = RemoteQue(QUE_PUT, 7850, 7850, PRIORITY_3, sizeof(EVENT), (char*)&rlEvent)) != RC_SUCCESS)
    {
        dbg(TRACE,"<CheckQueStatus> %05d RemoteQue returns: %d", __LINE__, ilRC);
    }

    /* bye bye */
    return;
}

/******************************************************************************/
/* The PrintNextEvent routine                                                 */
/******************************************************************************/
static void PrintNextEvents(void)
{
    int                     ilSection;
    int                     ilCurHour;
    int                     ilCurMin;
    int                     ilCurAbsDate;
    time_t                  tlCurTime;
    struct tm               *prlCurTime;
    struct tm               rlCurTime;

    dbg(TRACE,"<PrintNextEvents> -------- START ------");

    /* get current time */
    tlCurTime  = time(NULL);

    /* convert to better format */
    prlCurTime = (struct tm*)localtime(&tlCurTime);

    /* save time */
    memcpy((void*)&rlCurTime, (const void*)prlCurTime, sizeof(struct tm));

    for (ilSection=0; ilSection<rgTM.iNoOfSections; ilSection++)
    {
        dbg(TRACE,"<PrintNextEvents> -- START SECTION: %d --", ilSection);
        if (rgTM.prSection[ilSection].iType == iREL)
        {
            dbg(TRACE,"<PrintNextEvents> SECTION IS RELATIVE");
            if (rgTM.prSection[ilSection].iNoOfHOD > 0)
            {
                dbg(TRACE,"<PrintNextEvents> WITH HOUR");
                for (ilCurHour=0; 
                      ilCurHour<rgTM.prSection[ilSection].iNoOfHOD;
                      ilCurHour++)
                {
                    if (rgTM.prSection[ilSection].iNoOfMOH > 0)
                    {
                        /* and search for minute... */
                        for (ilCurMin=0;
                              ilCurMin<rgTM.prSection[ilSection].iNoOfMOH;
                              ilCurMin++)
                        {
                            dbg(TRACE,"<PrintNextEvents> Hour: %d - Minute: %d", rgTM.prSection[ilSection].rNextHours[ilCurHour].tm_hour, rgTM.prSection[ilSection].rNextMinutes[ilCurMin].tm_min);
                        }
                    }
                    else
                    {
                        dbg(TRACE,"<PrintNextEvents> Hour: %d - Minute: %d", rgTM.prSection[ilSection].rNextHours[ilCurHour].tm_hour, rgTM.prSection[ilSection].rNextHours[ilCurHour].tm_min);
                    }
                }
            }
            else
            {
                dbg(TRACE,"<PrintNextEvents> ONLY WITH MINUTES");
                for (ilCurMin=0;
                      ilCurMin<rgTM.prSection[ilSection].iNoOfMOH;
                      ilCurMin++)
                {
                    dbg(TRACE,"<PrintNextEvents> Minute: %d", rgTM.prSection[ilSection].rNextMinutes[ilCurMin].tm_min);
                }
            }
        }
        else if (rgTM.prSection[ilSection].iType == iABS)
        {
            dbg(TRACE,"<PrintNextEvents> SECTION IS ABSOLUTE");
            if (rgTM.prSection[ilSection].iNoOfAbsDate > 0)
            {
                for (ilCurAbsDate=0; ilCurAbsDate<rgTM.prSection[ilSection].iNoOfAbsDate; ilCurAbsDate++)
                {
                    dbg(TRACE,"<PrintNextEvents> Month: %d and Day: %d", rgTM.prSection[ilSection].rAbsDate[ilCurAbsDate].iMonthOfYear, rgTM.prSection[ilSection].rAbsDate[ilCurAbsDate].iDayOfMonth);
                }
            }

            for (ilCurHour=0; 
                  ilCurHour<rgTM.prSection[ilSection].iNoOfHOD;
                  ilCurHour++)
            {
                for (ilCurMin=0;
                      ilCurMin<rgTM.prSection[ilSection].iNoOfMOH;
                      ilCurMin++)
                {
                    dbg(TRACE,"<PrintNextEvents> Hour: %d - Minute: %d", rgTM.prSection[ilSection].piHoursOfDay[ilCurHour], rgTM.prSection[ilSection].piMinOfHour[ilCurMin]);
                }
            }
        }
        dbg(TRACE,"<PrintNextEvents> -- END SECTION: %d --\n", ilSection);
    }
    dbg(TRACE,"<PrintNextEvents> -------- END ------\n");

    return;
}

/******************************************************************************/
/* The DumpBinFile routine                                                 */
/******************************************************************************/
static void DumpBinFile(void)
{
  FileHeader   rlFileHeader;
  RecordHeader rlRecordHeader;
  FUNCTION     *prlFunction = NULL;
  DATA_PERIOD  *prlDataPeriod = NULL;
  char         *pclData = NULL;
  char         *pclRecord = NULL;
  int recordNum;
  FILE *fh = NULL;

  if ((fh = fopen(pcgBinaryFileName,"rb")) == NULL)
    {
      dbg(DEBUG,"<DumpBinFile> %05d can't open binary file...", __LINE__);
      /*
      ** nothing else to do
      */
      return;
    }
  fseek(fh, 0L, SEEK_SET);
  fread((void*)&rlFileHeader, (size_t)sizeof(FileHeader), (size_t)1, fh);
  dbg(TRACE,"Found %d entries",rlFileHeader.iNumber);

  for (recordNum=0;recordNum<rlFileHeader.iNumber;recordNum++){
    fread((void*)&rlRecordHeader, (size_t)sizeof(RecordHeader), (size_t)1, fh);
    pclRecord = (char*)malloc(rlRecordHeader.lRecordLen);
    memset((void*)pclRecord, 0x00, (size_t)rlRecordHeader.lRecordLen);

    dbg(TRACE,"**********Entry %d",recordNum+1);
    dbg(TRACE,"RecordEventID: %d RecordID: %d",rlRecordHeader.iRecordEventID,rlRecordHeader.iRecordID);

    fread((void*)pclRecord, (size_t)rlRecordHeader.lRecordLen, (size_t)1, fh);
    prlFunction     = (FUNCTION*)((char*)pclRecord);
    prlDataPeriod   = (DATA_PERIOD*)((char*)prlFunction->data);
    pclData             = (char*)prlDataPeriod->data;

    dbg(TRACE,"Function: %d Event: %d Parent: %d Dest: %d Time: %02d:%02d:%02d %02d-%02d",prlFunction->iFunction,prlFunction->iEventID,prlFunction->iParentID,
        prlFunction->iDestinationID,prlFunction->rStartTime.tm_hour,prlFunction->rStartTime.tm_min,prlFunction->rStartTime.tm_sec,
        prlFunction->rStartTime.tm_mday,prlFunction->rStartTime.tm_mon);
    dbg(TRACE,"Period-Info: dow: %d hod: %d moh: %d som: %d n_times: %d count: %d period: %d",prlDataPeriod->rPeriod.day_o_week,prlDataPeriod->rPeriod.hour_o_day,
        prlDataPeriod->rPeriod.min_o_hour,prlDataPeriod->rPeriod.sec_o_min,prlDataPeriod->rPeriod.n_times,prlDataPeriod->rPeriod.count,prlDataPeriod->rPeriod.period);
  }
  dbg(TRACE,"**********End of dump %d records dumped",rlFileHeader.iNumber);
}



static int CheckDay(UINT ipCfgDay, int ipCurDay)
{
  int ilRC = TRUE;

  if ((ipCfgDay & pigDayFlags[ipCurDay]) == 0)
     ilRC = FALSE;
  return ilRC;
} /* End of CheckDay */

