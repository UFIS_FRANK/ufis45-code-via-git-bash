#ifndef _DEF_mks_version_wrapElse
  #define _DEF_mks_version_wrapElse
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_wrapElse[] = "@(#) "UFIS_VERSION" $Id: Ufis/Sin/SIN_Server/Base/Server/Kernel/IMP/wrapElse.src.c 1.6 2012/03/05 12:37:11SGT dka Exp ble(2009/01/20 16:40:13SGT) $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program                                                         */
/*                                                                            */
/* Author         : JIM                                                       */
/* Date           : 28. Nov. 2005                                             */
/* Description    : ELSE to UFIS Inteface Include file                        */
/*                  This Include file reformats the ELSE updates received by  */
/*                  IMPHDL via an CEDA MQ Interface and handles requests from */
/*                  ELSE                                                      */
/*                                                                            */
/*	20080103 DKA	Read IGNORE_TERMINAL from imphdl.cfg. This defines	*/
/*			a terminal number that is ignored from ELSE. If cfg	*/
/*			does not have this entry, then accept all terminals	*/
/*	20080118 DKA	Use GATTAB to lookup the terminal, rather than the	*/
/*			ELSE message. PRF 8799.					*/
/*	20080128 DKA	PRF 8799 cont'd - if ELSE sends only TERM & no GATE	*/
/*			then ignore TERM if AFTTAB already has GATE. Reason	*/
/*			is that the lonely TERM is probably due to BELT change	*/
/*			in FCS. Cannot depend on presense of belt & term since	*/
/*			belt is not sent to SOCC...				*/
/*                                                                              */
/* 20120223 v.1.6 DKA   This is a jump over v.1.5 which was the non-sched.      */
/*                      Bug fix for UFIS-1519, clearing of fields via ELSE      */
/*                      did not work for <est/> because a space was not         */
/*                      appended. Other fields affected are calls_3, calls_4    */
/*                      ordest (why would ordest be deleted??)                  */
/*										*/
/* 20120305	 DKA	Leftover bug from original implementation - an empty	*/
/*			would be appended if changing stime was allowed and no  */
/*			stime was received.					*/ 
/*                                                                            */
/*                                                                            */
/* Update history :                                                           */
/*                                                                            */
/******************************************************************************/

/************************************************************************
 *	Function prototypes						*
 ************************************************************************/

static int GetADID(char *pclADID, char *pclOrigData);
static void Init_Else();
static void MapNature(char *pclTTYP);
static void InitMapNature();
static void ReadConfigDefault(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer, char *pcpDefault);
static int  LookupGateTerminal (char *gnam, char *term);
static void Add_Gate_n_Term (char cAdid,
			     int flagTerm, char *clElseTerm,
		             int flagGate, char *clElseGate,
			     char *pclFldOut,char *pclDatOut);
static int  Lookup_AFT (char cAdid, char *clFLNO, char *clFlda,
	                char *clField, char *clRes);

/************************************************************************
 *	Global variables						*
 ************************************************************************/

static char pcgNatureIn[128] = "";
static char pcgNatureOut[128] = "";
static char pcgIgnoreSqMi[500] = "\0";
static char pcgIgnoreEverOnline[500] = "\0";
static char pcgADD_MINUTES_TO_ELSE_ETA[20] = "\0";
static int igAddMinToETA = 0;
static int igQToElse = 0;
static char pcgIgnoreTerminal [4];
static char pcg_AFT_Gate [8];

/******************************************************************************/
/* The wrapElseData routine                                                   */
/* This is the interface routine called by IMPMAN                             */
/* In:  pclData:   pointer to the data record received from ELSE              */
/* Out: pclCmd:    pointer to store the command "UFR" or "DFR"                */
/*      pclSelOut: pointer to store the selection for IMPHDL                  */
/*      pclFldOut: pointer to store the field list for IMPHDL                 */
/*      pclDatOut: pointer to store the data list for IMPHDL                  */
/******************************************************************************/
int wrapElseData(char *pclData, char *pclCmd,
                 char *pclSelOut,char *pclFldOut,char *pclDatOut,
                 int ipPrioToElse, int ipQueueToElse)
{
	static int ElseCounter = 0;
    int ilRC = RC_SUCCESS;
	int ilRCTmp = RC_SUCCESS;
	int i = 0;
	char *pclTemp = NULL;
	long llSize = 0;
	char clTmp[500] = "\0";
	int  blTmpFound = FALSE;
	char clBegin[50] = "\0";
	char clEnd[50] = "\0";
	char clOrigData[4000]="\0";		/*orig data string from else*/
	char clADID[10] = "\0";			/*adid retrieved from attribute table in element update: DA = arrival, DD = departure*/
	int  blADIDFound = FALSE;
	char clFLDA[20] = "\0";			/*Local-Flight-Day given in attribute sdate of element KEY*/
	int  blFLDAFound = FALSE;
	char clFLNO[20] = "\0";			/*flight*/
	int  blFLNOFound = FALSE;
	char clSTOA[20] = "\0";			/*sdate if arrival*/
	int  blSTOAFound = FALSE;
	char clSTOD[20] = "\0";			/*sdate if departuer*/
	int  blSTODFound = FALSE;
	char clTGA1[20] = "\0";			/*terminal if arrival*/
	int  blTGA1Found = FALSE;
	char clTGD1[20] = "\0";			/*terminal if departure*/
	int  blTGD1Found = FALSE;
	char clACT5[20] = "\0";			/*actype if 5 char*/
	int  blACT5Found = FALSE;
	char clACT3[20] = "\0";			/*actype if 3 char*/
	int  blACT3Found = FALSE;
	char clTTYP[20] = "\0";			/*nature (to be mapped to configured value*/
	int  blTTYPFound = FALSE;
	char clJFLN[20] = "\0";
	int  blJFLNFound = FALSE;
	char clORG3[20] = "\0";			/*ordest if arrival*/
	int  blORG3Found = FALSE;
	char clDES3[20] = "\0";			/*ordest if departure*/
	int  blDES3Found = FALSE;
	char clVIA1[20] = "\0";			/*route_1*/
	char clVIA2[20] = "\0";			/*route_2*/
	char clVIA3[20] = "\0";			/*route_3*/
	char clVIA4[20] = "\0";			/*route_4*/
	char clVIA5[20] = "\0";			/*route_5*/
	char clVIA6[20] = "\0";			/*route_6*/
	char clVIA7[20] = "\0";			/*route_7*/
	char clROUT[100] = "\0";		/*ROUT will be send to flight*/
	char blROUTFound = FALSE;
	char clETOA[20] = "\0";			/*est if arrival*/
	int  blETOAFound = FALSE;
	char clETOD[20] = "\0";			/*est if departure*/
	int  blETODFound = FALSE;
	char clLAND[20] = "\0";			/*act if arrival*/
	int  blLANDFound = FALSE;
	char clAIRB[20] = "\0";			/*act if departure*/
	int  blAIRBFound = FALSE;
	char clGTA1[20] = "\0";			/*gate if arrival*/
	int  blGTA1Found = FALSE;
	char clGTD1[20] = "\0";			/*gate if departure*/
	int  blGTD1Found = FALSE;
	char clPSTA[20] = "\0";			/*park if arrival*/
	int  blPSTAFound = FALSE;
	char clPSTD[20] = "\0";			/*park if departure*/
	int  blPSTDFound = FALSE;
	char clREMP[20] = "\0";			/*rem_c*/
	int  blREMPFound = FALSE;
	char clREGN[20] = "\0";			/*registration*/
	int  blREGNFound = FALSE;
	char clMASTER[20] = "\0";		/*master for codeshare... this means that the FLNO is the code-share*/
	int  blMASTERFound = FALSE;
	char clCOSH[20] = "\0";			/*COSH-field will be used for Code-Share if this is a code-share*/
	int  blCOSHFound = TRUE;
	char clBLT1[20] = "\0";			/*belt_1 if arrival*/
	int  blBLT1Found = TRUE;
	char clBLT2[20] = "\0";			/*belt_2 if arrival*/
	int  blBLT2Found = TRUE;
	char clGD1X[20] = "\0";			/*calls_1, GateOpenTime*/
	int  blGD1XFound = TRUE;
	char clBOAO[20] = "\0";			/*calls_2, GateBoardingTime*/
	int  blBOAOFound = TRUE;
	char clFCAL[20] = "\0";			/* Final Call for AFTTAB, "latest final call"*/
	int  blFCALFound = TRUE;
	char clFCA1[20] = "\0";			/*calls_3, Final Call 1, stored in AF1TAB*/
	int  blFCA1Found = TRUE;
	char clFCA2[20] = "\0";			/*calls_4, final Call 2, stored in AF1TAB*/
	int  blFCA2Found = TRUE;
	char clGD1Y[20] = "\0";			/*calls_5, Gate Close Time*/
	int  blGD1YFound = TRUE;
	char clCKIF[20] = "\0";			/*checkin-first*/
	char clCKIT[20] = "\0";			/*checkin-last*/
	int  blIsSqMiFlight = FALSE;	/*will be true if it is an SQ/MI-Flight */
	char clInfo[1000] = "\0";
	int  blEchoFound = FALSE;
	char clTime[20] = "\0";
	int  blIsIntegrate = FALSE;
	char clFLNOOrig[20] = "\0";


	ElseCounter++;
	sprintf(pclSelOut,"IFNAME:ELSE,TIME:LOCAL,PRIO:4");
	strcpy(pclCmd,"UFR");
	strcpy(clOrigData,pclData);


	dbg(TRACE,"%05d: ############# wrapElseData - START <%d>###########",__LINE__,ElseCounter);
	if(ElseCounter == 1)
	{
		Init_Else();
	}

	if((strstr(clOrigData,"<info>") != NULL) || (strstr(clOrigData,"</info>") != NULL))
	{
		return RC_IGNORE;
	}

	if(strstr(clOrigData,"<query") != NULL)
	{
		SendCedaEvent(igQToElse,0,mod_name,"","","","QRY","","","",clOrigData,"",5,RC_SUCCESS);
		return RC_IGNORE;
	}


	if(GetElementValue(clTmp,clOrigData,"<echo>", "</echo>") == TRUE)
	{
		sprintf(clInfo,"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>\n<control>\n  <info>%s</info>\n</control>",clTmp);
		blEchoFound = TRUE;
	}
	else if(strstr(clOrigData,"<echo/>") != NULL)
	{
		strcpy(clInfo,"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>\n<control>\n  <info/>\n</control>");
		blEchoFound = TRUE;
	}
	else
	{
		blEchoFound = FALSE;
	}

	if(blEchoFound == TRUE)
	{
		SendCedaEvent(ipQueueToElse,0,mod_name,"","","","ELS","","","",clInfo,"",ipPrioToElse,RC_SUCCESS);
		dbg(TRACE,"%05d: Echo request processed. Info-message <%s> was send to ELSE",__LINE__,clInfo);
		return RC_IGNORE;
	}

	if(strstr(clOrigData,"<integrate>") != NULL)
	{
		blIsIntegrate = TRUE;
	}
	else
	{
		blIsIntegrate = FALSE;
	}

	blADIDFound		= GetElementValue(clADID,clOrigData,"<update table=\"D", "\"");
	if(blADIDFound == TRUE)
	{
		dbg(DEBUG,"%05d: UPDATE found. clADID = <%s>",__LINE__,clADID);
	}
	else if((blADIDFound = GetElementValue(clADID,clOrigData,"<delete table=\"D", "\"")) == TRUE)
	{
		strcpy(pclCmd,"DFR");
		dbg(DEBUG,"%05d: DELETE found. clADID = <%s>",__LINE__,clADID);
	}
	else
	{
		dbg(TRACE,"%05d: Update / Delete not found in Message",__LINE__);
		dbg(TRACE,"%05d: ############# wrapElseData - END  ###########",__LINE__);
		return RC_IGNORE;
	}

	blFLNOFound	= GetElementValue(clFLNO, clOrigData, "<key flight=\"", "\"");
	strcpy(clFLNOOrig,clFLNO);
	if(blFLNOFound == TRUE)
	{
		if(FormatFlnoStr(clFLNO) != RC_SUCCESS)
		{
			dbg(TRACE,"%05d: FormatFlnoStr failed",__LINE__);
			dbg(TRACE,"%05d: ############# wrapElseData - END  ###########",__LINE__);
			return RC_FAIL;
		}
		else
		{
			dbg(DEBUG,"%05d: clFLNO = <%s>",__LINE__,clFLNO);
		}
	}
	else
	{
		alert("Element <key flight=\"> not found");
		dbg(TRACE,"%05d: ############# wrapElseData - END  ###########",__LINE__);
		return RC_FAIL;
	}


	blFLDAFound		= GetElementValue(clFLDA, clOrigData, "sdate=\"", "\"");
	if(blFLDAFound == TRUE)
	{
		dbg(DEBUG,"%05d: clFLDA = <%s>",__LINE__,clFLDA);
		if(strcmp(pclCmd,"DFR") == 0)
		{
			GetServerTimeStamp("UTC",1,0,clTime);
			UtcToLocalTimeFixTZ(clTime); /* convert to local */
			clTime[8] = '\0';
			if(strcmp(clFLDA,clTime) < 0)
			{
				dbg(TRACE,"%05d: This flight is in the past: FLDA(%s) < Today(%s), so ignore this delete",__LINE__,clFLDA,clTime);
				dbg(TRACE,"%05d: ############# wrapElseData - END  ###########",__LINE__);
				return RC_IGNORE;
			}
		}
	}
	else
	{
		alert("Attribute \"sdate=\" not found");
		dbg(TRACE,"%05d: ############# wrapElseData - END  ###########",__LINE__);
		return RC_FAIL;
	}

	/*Build STOA/STOD from ADID, FLDA and stime*/
	pclTemp = CedaGetKeyItem(clTmp, &llSize, clOrigData, "<stime>", "</stime>", TRUE);
	if(pclTemp != NULL)
	{
		if (clADID[0] == 'A')
		{
			strcpy(clSTOA,clFLDA);
			strcat(clSTOA,clTmp);
			strcat(clSTOA,"00");
			blSTOAFound = TRUE;
			blSTODFound = FALSE;
			dbg(DEBUG,"%05d: clSTOA = <%s>",__LINE__,clSTOA);
		}
		if (clADID[0] == 'D')
		{
			strcpy(clSTOD,clFLDA);
			strcat(clSTOD,clTmp);
			strcat(clSTOD,"00");
			blSTODFound = TRUE;
			blSTOAFound = FALSE;
			dbg(DEBUG,"%05d: clSTOD = <%s>",__LINE__,clSTOD);
		}
	}
	else
	{
		if(strstr(clOrigData,"<stime/>") != NULL)
		{
			dbg(TRACE,"%05d: the TIME-Part of the scheduled-time can not be deleted",__LINE__);
		}
	}


	/*get find out if it is a code-share-flight */
	blMASTERFound		= GetElementValue(clMASTER, clOrigData, "<master>", "</master>");
	if(blMASTERFound == TRUE)
	{
		if(FormatFlnoStr(clMASTER) != RC_SUCCESS)
		{
			dbg(TRACE,"%05d: FormatFlnoStr failed for <%s>",__LINE__,clMASTER);
			dbg(TRACE,"%05d: ############# wrapElseData - END  ###########",__LINE__);
			return RC_FAIL;
		}
		else
		{
			dbg(DEBUG,"%05d: clMASTER = <%s>",__LINE__,clMASTER);
			dbg(DEBUG,"%05d: THIS IS A SLAVE-FLIGHT: now exchange FLNO and MASTER, because FLNO refers to a slave-flight",__LINE__);
			strcpy(clTmp,clFLNO);
			strcpy(clFLNO,clMASTER);
			if(strcmp(pclCmd,"DFR") == 0)
			{
				strcpy(clCOSH,"D;");
				strcpy(pclCmd,"UFR");
			}
			else
			{
				strcpy(clCOSH,"I;");
			}
			strcat(clCOSH,clTmp);
			blCOSHFound = TRUE;
			dbg(DEBUG,"%05d: now: clFLNO <%s>, clCOSH <%s>",__LINE__,clFLNO,clCOSH);

		}
	}
	else
	{
		if(strstr(clOrigData,"<master/>") != NULL)
		{
			dbg(TRACE,"%05d: <master/> found -> this is a codeshare but no master-flight is given. This message can not be handled ",__LINE__);
			return RC_FAIL;
		}
	}

	if((strstr(clFLNO,"SQ") != NULL) || (strstr(clFLNO,"MI") != NULL))
	{
		blIsSqMiFlight = TRUE;
		if(strcmp(pclCmd,"DFR") == 0) 
		{
			dbg(TRACE,"%05d: Delete-message for SQ/MI found. Ignore this message!",__LINE__);
			return RC_IGNORE;
		}
	}
	else
	{
		blIsSqMiFlight = FALSE;
	}

	/*now put first data to output-strings*/
	strcpy(pclFldOut,"ADID,FLNO,FLDA");
	sprintf(pclDatOut,"%s,%s,%s",clADID,clFLNO,clFLDA);

	if(((strstr(pcgIgnoreSqMi,"<stime>") == NULL) && (blIsSqMiFlight == TRUE)) ||  
		((blIsSqMiFlight == FALSE) && (strstr(pcgIgnoreEverOnline, "<stime>") == NULL)))
	{
		if((clADID[0] == 'A') && (blSTOAFound  == TRUE)) /* flag test added v.1.6 */
		{
			strcat(pclFldOut,",STOA");
			strcat(pclDatOut,",");
			strcat(pclDatOut,clSTOA);
		}
		if((clADID[0] == 'D') && (blSTODFound == TRUE)) /* flag test added v.1.6 */
		{
			strcat(pclFldOut,",STOD");
			strcat(pclDatOut,",");
			strcat(pclDatOut,clSTOD);
		}
	}

	if(((strstr(pcgIgnoreSqMi,"<master>") == NULL) && (blIsSqMiFlight == TRUE)) ||  
		((blIsSqMiFlight == FALSE) && (strstr(pcgIgnoreEverOnline, "<master>") == NULL)))
	{
		/*if this is a slave-flight, only update COSH, no other fields*/
		if(blMASTERFound == TRUE)
		{
			strcat(pclFldOut,",COSH");
			strcat(pclDatOut,",");
			strcat(pclDatOut,clCOSH);
			dbg(TRACE,"%05d: ############# wrapElseData - END  ###########",__LINE__);
			return RC_SUCCESS;
		}
	}


	if(((strstr(pcgIgnoreSqMi,"<terminal>") == NULL) && (blIsSqMiFlight == TRUE)) ||  
		((blIsSqMiFlight == FALSE) && (strstr(pcgIgnoreEverOnline, "<terminal>") == NULL)))
	{
		/*get TGA1/TGD1 from terminal*/
		if(GetElementValue(clTmp, clOrigData, "<terminal>", "</terminal>") == TRUE)
		{
			/*
				PRF 8799 - defer appending to field/data list in
			   	Add_Gate_n_Term ()
			*/ 

			if(clADID[0] == 'A')
			{
				strcpy(clTGA1,clTmp);
				dbg(DEBUG,"%05d: clTGA1 = <%s>",__LINE__,clTGA1);
				blTGA1Found = TRUE;

			}
			if(clADID[0] == 'D')
			{
				strcpy(clTGD1,clTmp);
				dbg(DEBUG,"%05d: clTGD1 = <%s>",__LINE__,clTGD1);
				    blTGD1Found = TRUE; 
		}
		}
		else
		{
			if(strstr(clOrigData,"<terminal/>") != NULL)
			{
				if(clADID[0] == 'A')
				{
					clTGA1[0] = '\0';
					dbg(DEBUG,"%05d: clTGA1 = <%s>",__LINE__,clTGA1);
					blTGA1Found = TRUE;
				}
				if(clADID[0] == 'D')
				{
					clTGD1[0] = '\0';
					dbg(DEBUG,"%05d: clTGD1 = <%s>",__LINE__,clTGD1);
					blTGD1Found = TRUE;
				}
			}
		}
	} /*end if(strstr(pcgIgnoreSqMi,"<terminal>") == NULL)*/

	if(((strstr(pcgIgnoreSqMi,"<actype>") == NULL) && (blIsSqMiFlight == TRUE)) ||  
		((blIsSqMiFlight == FALSE) && (strstr(pcgIgnoreEverOnline, "<actype>") == NULL)))
	{
		/* get aircraft-type 3 or 5 letter*/
		pclTemp = CedaGetKeyItem(clTmp, &llSize, clOrigData, "<actype>", "</actype>", TRUE);
		if(pclTemp != NULL)
		{
			if(llSize == 3)
			{
				strcpy(clACT3,clTmp);
				blACT3Found = TRUE;
				dbg(DEBUG,"%05d: clACT3 = <%s>",__LINE__,clACT3);
				strcat(pclFldOut,",ACT3");
				strcat(pclDatOut,",");
				strcat(pclDatOut,clACT3);
			}
			else
			{
				strcpy(clACT5,clTmp);
				blACT5Found = TRUE;
				dbg(DEBUG,"%05d: clACT5 = <%s>",__LINE__,clACT5);
				strcat(pclFldOut,",ACT5");
				strcat(pclDatOut,",");
				strcat(pclDatOut,clACT5);
			}
		}
		else
		{
			if(strstr(clOrigData,"<actype/>") != NULL)
			{
				strcat(pclFldOut,",ACT3,ACT5");
				strcat(pclDatOut,", , ");
				blACT3Found = TRUE;
				blACT5Found = TRUE;
			}
		}
	} /*end if(strstr(pcgIgnoreSqMi,"<actype>") == NULL)*/

	if(((strstr(pcgIgnoreSqMi,"<nature>") == NULL) && (blIsSqMiFlight == TRUE)) ||  
		((blIsSqMiFlight == FALSE) && (strstr(pcgIgnoreEverOnline, "<nature>") == NULL)))
	{
		/*get nature-code (TTYP) and map it as given in configuration*/
		blTTYPFound		= GetElementValue(clTTYP, clOrigData, "<nature>", "</nature>");
		if(blTTYPFound == TRUE)
		{
			MapNature(clTTYP);
			dbg(DEBUG,"%05d: clTTYP = <%s>",__LINE__,clTTYP);
			strcat(pclFldOut,",TTYP");
			strcat(pclDatOut,",");
			strcat(pclDatOut,clTTYP);
		}
		else
		{
			if(strstr(clOrigData,"<nature/>") != NULL)
			{
				strcat(pclFldOut,",TTYP");
				strcat(pclDatOut,", ");
				blTTYPFound = TRUE;
			}
		}
	} /*if(strstr(pcgIgnoreSqMi,"<nature>") == NULL)*/

	if(((strstr(pcgIgnoreSqMi,"<corrflight>") == NULL) && (blIsSqMiFlight == TRUE)) ||  
		((blIsSqMiFlight == FALSE) && (strstr(pcgIgnoreEverOnline, "<corrflight>") == NULL)))
	{
		/*Join-Flight-Number*/
		blJFLNFound		= GetElementValue(clJFLN, clOrigData, "<corrflight>", "</corrflight>");
		if(blJFLNFound == TRUE)
		{
			if(FormatFlnoStr(clJFLN) != RC_SUCCESS)
			{
				dbg(TRACE,"%05d: FormatFlnoStr failed for <%s>",__LINE__,clJFLN);
				dbg(TRACE,"%05d: ############# wrapElseData - END  ###########",__LINE__);
				return RC_FAIL;
			}
			else
			{
				dbg(DEBUG,"%05d: clJFLN = <%s>",__LINE__,clJFLN);
				strcat(pclFldOut,",JFLN");
				strcat(pclDatOut,",");
				strcat(pclDatOut,clJFLN);
			}
		}
		else
		{
			dbg(TRACE,"%05d: <corrflight/> found. No flight to be linked",__LINE__);
			blJFLNFound = TRUE;
		}
	} /*if(strstr(pcgIgnoreSqMi,"<corrflight>") == NULL)*/

	if(((strstr(pcgIgnoreSqMi,"<ordest>") == NULL) && (blIsSqMiFlight == TRUE)) || 
		((blIsSqMiFlight == FALSE) && (strstr(pcgIgnoreEverOnline, "<ordest>") == NULL)))
	{
		/* get origin / destination*/
		pclTemp = CedaGetKeyItem(clTmp, &llSize, clOrigData, "<ordest>", "</ordest>", TRUE);
		if(pclTemp != NULL)
		{
			if(clADID[0] == 'A')
			{
				strcpy(clORG3,clTmp);
				blORG3Found = TRUE;
				dbg(DEBUG,"%05d: clORG3 = <%s>",__LINE__,clORG3);
				strcat(pclFldOut,",ORG3");
				strcat(pclDatOut,",");
				strcat(pclDatOut,clORG3);
			}
			if(clADID[0] == 'D')
			{
				strcpy(clDES3,clTmp);
				blDES3Found = TRUE;
				dbg(DEBUG,"%05d: clDES3 = <%s>",__LINE__,clDES3);
				strcat(pclFldOut,",DES3");
				strcat(pclDatOut,",");
				strcat(pclDatOut,clDES3);
			}
		}
		else
		{
			if(strstr(clOrigData,"<ordest/>") != NULL)
			{
				if(clADID[0] == 'A')
				{
					blORG3Found = TRUE;
					dbg(DEBUG,"%05d: clORG3 = <%s>",__LINE__,clORG3);
					strcat(pclFldOut,",ORG3");
					strcat(pclDatOut,", ");
				}
				if(clADID[0] == 'D')
				{
					blDES3Found = TRUE;
					dbg(DEBUG,"%05d: clDES3 = <%s>",__LINE__,clDES3);
					strcat(pclFldOut,",DES3");
					strcat(pclDatOut,", ");
				}
			}
		}
	} /*if(strstr(pcgIgnoreSqMi,"<ordest>") == NULL)*/

	/* query for compelte via-list if rout is detected in the message and it is not a integrate-message*/
	/* only in integrate-messages the rout is complete, so whenever a normal message contains a route-element we*/
	/* have to discard the route-elements and query for an integrate message for this flight*/

	if(blIsIntegrate == TRUE)
	{
		if(((strstr(pcgIgnoreSqMi,"route") == NULL) && (blIsSqMiFlight == TRUE)) || 
			((blIsSqMiFlight == FALSE) && (strstr(pcgIgnoreEverOnline, "<route>") == NULL)))
		{
			/*get VIA1-VIA7 and build ROUT*/
			for (i=1 ; i<=7 ; i++)
			{
				sprintf(clBegin,"<route_%d>",i);
				sprintf(clEnd,"</route_%d>",i);
				blTmpFound		= GetElementValue(clTmp, clOrigData, clBegin, clEnd);
				if(blTmpFound == TRUE)
				{
					strcat(clROUT,clTmp);
					blROUTFound = TRUE;
				}
				else
				{
					sprintf(clBegin,"<route_%d/>",i);
					if(strstr(clOrigData,clBegin) != NULL)
					{
						strcat(clROUT," ");
						blROUTFound = TRUE;
					}
					else
					{
						strcat(clROUT,"?");
					}
				}
				strcat(clROUT,"|");
				dbg(DEBUG,"%05d: clROUT = <%s>",__LINE__,clROUT);
			}
			if(blROUTFound == TRUE)
			{
				clROUT[strlen(clROUT)-1] = '\0';
				dbg(DEBUG,"%05d: clROUT = <%s>",__LINE__,clROUT);
				strcat(pclFldOut,",ROUT");
				strcat(pclDatOut,",");
				strcat(pclDatOut,clROUT);
			}
		} /*if(strstr(pcgIgnoreSqMi,"route") == NULL)*/
	}/*end if((blIsIntegrate == FALSE))*/
	else
	{
		if((strstr(clOrigData,"<route") != NULL) || (strstr(clOrigData,"</route") != NULL))
		{
			sprintf(clTmp,
					"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\" ?>\n<query table=\"D%s\">\n<key flight=\"%s\" sdate=\"%s\"/>\n</query>",
					clADID,clFLNOOrig,clFLDA);
			SendCedaEvent(ipQueueToElse,0,mod_name,"","","","ELS","","","",clTmp,"",1,RC_SUCCESS);
		}
	}

	if(((strstr(pcgIgnoreSqMi,"<act>") == NULL) && (blIsSqMiFlight == TRUE)) ||  
		((blIsSqMiFlight == FALSE) && (strstr(pcgIgnoreEverOnline, "<act>") == NULL)))
	{
		/*get actual time depending of arrival/departure*/
		pclTemp = CedaGetKeyItem(clTmp, &llSize, clOrigData, "<act>", "</act>", TRUE);
		if(pclTemp != NULL)
		{
			if(clADID[0] == 'A')
			{
				strcpy(clLAND,clTmp);
				strcat(clLAND,"00");
				blLANDFound = TRUE;
				dbg(DEBUG,"%05d: clLAND = <%s>",__LINE__,clLAND);
				strcat(pclFldOut,",LAND");
				strcat(pclDatOut,",");
				strcat(pclDatOut,clLAND);
			}
			if(clADID[0] == 'D')
			{
				strcpy(clAIRB,clTmp);
				strcat(clAIRB,"00");
				blAIRBFound = TRUE;
				dbg(DEBUG,"%05d: clAIRB = <%s>",__LINE__,clAIRB);
				strcat(pclFldOut,",AIRB");
				strcat(pclDatOut,",");
				strcat(pclDatOut,clAIRB);
			}
		}
		else
		{
			if(strstr(clOrigData,"<act/>") != NULL)
			{
				if(clADID[0] == 'A')
				{
					blLANDFound = TRUE;
					dbg(DEBUG,"%05d: clLAND = <%s>",__LINE__,clLAND);
					strcat(pclFldOut,",LAND");
					strcat(pclDatOut,", ");
				}
				if(clADID[0] == 'D')
				{
					blAIRBFound = TRUE;
					dbg(DEBUG,"%05d: clAIRB = <%s>",__LINE__,clAIRB);
					strcat(pclFldOut,",AIRB");
					strcat(pclDatOut,", ");
				}
			}
		}
	} /*if(strstr(pcgIgnoreSqMi,"<act>") == NULL)*/

	if(((strstr(pcgIgnoreSqMi,"<gate>") == NULL) && (blIsSqMiFlight == TRUE)) || 
		((blIsSqMiFlight == FALSE) && (strstr(pcgIgnoreEverOnline, "<gate>") == NULL)))
	{
		/*get gate depending of arrival/departure*/
		pclTemp = CedaGetKeyItem(clTmp, &llSize, clOrigData, "<gate>", "</gate>", TRUE);
		if(pclTemp != NULL)
		{
			/*
				PRF 8799 - defer appending to field and data list
				into Add_Gate_n_Term ().  
			*/

			if(clADID[0] == 'A')
			{
				strcpy(clGTA1,clTmp);
				blGTA1Found = TRUE;
				dbg(DEBUG,"%05d: clGTA1 = <%s>",__LINE__,clGTA1);
			}
			if(clADID[0] == 'D')
			{
				strcpy(clGTD1,clTmp);
				blGTD1Found = TRUE;
				dbg(DEBUG,"%05d: clGTD1 = <%s>",__LINE__,clGTD1);			
			}
		}
		else
		{
			if(strstr(clOrigData,"<gate/>") != NULL)
			{
				if(clADID[0] == 'A')
				{
					blGTA1Found = TRUE;
					dbg(DEBUG,"%05d: clGTA1 = <%s>",__LINE__,clGTA1);
					/* Flwg added in PRF8799*/
					clGTA1 [0] = '\0'; 
				}
				if(clADID[0] == 'D')
				{
					blGTD1Found = TRUE;
					dbg(DEBUG,"%05d: clGTD1 = <%s>",__LINE__,clGTD1);			
					/* Flwg added in PRF8799*/
					clGTD1 [0] = '\0'; 
				}
			}
		}
	} /*if(strstr(pcgIgnoreSqMi,"<gate>") == NULL)*/

	if(((strstr(pcgIgnoreSqMi,"<park>") == NULL) && (blIsSqMiFlight == TRUE)) || 
		((blIsSqMiFlight == FALSE) && (strstr(pcgIgnoreEverOnline, "<park>") == NULL)))
	{
		/*get parkingstand depending of arrival/departure*/
		pclTemp = CedaGetKeyItem(clTmp, &llSize, clOrigData, "<park>", "</park>", TRUE);
		if(pclTemp != NULL)
		{
			if(clADID[0] == 'A')
			{
				strcpy(clPSTA,clTmp);
				blPSTAFound = TRUE;
				dbg(DEBUG,"%05d: clPSTA = <%s>",__LINE__,clPSTA);
				strcat(pclFldOut,",PSTA");
				strcat(pclDatOut,",");
				strcat(pclDatOut,clPSTA);
			}
			if(clADID[0] == 'D')
			{
				strcpy(clPSTD,clTmp);
				blPSTDFound = TRUE;
				dbg(DEBUG,"%05d: clPSTD = <%s>",__LINE__,clPSTD);
				strcat(pclFldOut,",PSTD");
				strcat(pclDatOut,",");
				strcat(pclDatOut,clPSTD);
			}
		}
		else
		{
			if(strstr(clOrigData,"<park/>") != NULL)
			{
				if(clADID[0] == 'A')
				{
					blPSTAFound = TRUE;
					dbg(DEBUG,"%05d: clPSTA = <%s>",__LINE__,clPSTA);
					strcat(pclFldOut,",PSTA");
					strcat(pclDatOut,", ");
				}
				if(clADID[0] == 'D')
				{
					blPSTDFound = TRUE;
					dbg(DEBUG,"%05d: clPSTD = <%s>",__LINE__,clPSTD);
					strcat(pclFldOut,",PSTD");
					strcat(pclDatOut,", ");
				}
			}
		}
	} /*if(strstr(pcgIgnoreSqMi,"<park>") == NULL)*/

	if(((strstr(pcgIgnoreSqMi,"<rem_c>") == NULL) && (blIsSqMiFlight == TRUE)) ||
		((blIsSqMiFlight == FALSE) && (strstr(pcgIgnoreEverOnline, "<rem_c>") == NULL)))
	{
		/*get remark-code */
		blREMPFound		= GetElementValue(clREMP, clOrigData, "<rem_c>", "</rem_c>");
		if(blREMPFound == TRUE)
		{
			dbg(DEBUG,"%05d: clREMP = <%s>",__LINE__,clREMP);
			strcat(pclFldOut,",REMP");
			strcat(pclDatOut,",");
			strcat(pclDatOut,clREMP);
			if(strcmp(clREMP,"CN") == 0)
			{
				strcat(pclFldOut,",FTYP");
				strcat(pclDatOut,",X");
			}
		}
		else
		{
			if(strstr(clOrigData,"<rem_c/>") != NULL)
			{
				strcat(pclFldOut,",REMP");
				strcat(pclDatOut,", ");
				blREMPFound = TRUE;
			}
		}
	}/*if(strstr(pcgIgnoreSqMi,"<rem_c>") == NULL)*/

	if(((strstr(pcgIgnoreSqMi,"<registration>") == NULL) && (blIsSqMiFlight == TRUE)) || 
		((blIsSqMiFlight == FALSE) && (strstr(pcgIgnoreEverOnline, "<registration>") == NULL)))
	{
		/*get registration */
		blREGNFound		= GetElementValue(clREGN, clOrigData, "<registration>", "</registration>");
		if(blREGNFound == TRUE)
		{
			dbg(DEBUG,"%05d: clREGN = <%s>",__LINE__,clREGN);
			strcat(pclFldOut,",REGN");
			strcat(pclDatOut,",");
			strcat(pclDatOut,clREGN);
		}
		else
		{
			if(strstr(clOrigData,"<registration/>") != NULL)
			{
				strcat(pclFldOut,",REGN");
				strcat(pclDatOut,", ");
				blREGNFound = TRUE;
			}
		}
	}/*if(strstr(pcgIgnoreSqMi,"<registration>") == NULL)*/


	if(((strstr(pcgIgnoreSqMi,"<belt_1>") == NULL) && (blIsSqMiFlight == TRUE)) || 
		((blIsSqMiFlight == FALSE) && (strstr(pcgIgnoreEverOnline, "<belt_1>") == NULL)))
	{
		/*get belt1 */
		blBLT1Found		= GetElementValue(clBLT1, clOrigData, "<belt_1>", "</belt_1>");
		if(blBLT1Found == TRUE)
		{
			dbg(DEBUG,"%05d: clBLT1 = <%s>",__LINE__,clBLT1);
			strcat(pclFldOut,",BLT1");
			strcat(pclDatOut,",");
			strcat(pclDatOut,clBLT1);
		}
		else
		{
			if(strstr(clOrigData,"<belt_1/>") != NULL)
			{
				strcat(pclFldOut,",BLT1");
				strcat(pclDatOut,", ");
				blBLT1Found = TRUE;
			}
		}
	}/*if(strstr(pcgIgnoreSqMi,"<belt_1>") == NULL)*/

	
	if(((strstr(pcgIgnoreSqMi,"<belt_2>") == NULL) && (blIsSqMiFlight == TRUE)) ||  
		((blIsSqMiFlight == FALSE) && (strstr(pcgIgnoreEverOnline, "<belt_2>") == NULL)))
	{
		/*get belt2 */
		blBLT2Found		= GetElementValue(clBLT2, clOrigData, "<belt_2>", "</belt_2>");
		if(blBLT2Found == TRUE)
		{
			dbg(DEBUG,"%05d: clBLT2 = <%s>",__LINE__,clBLT2);
			strcat(pclFldOut,",BLT2");
			strcat(pclDatOut,",");
			strcat(pclDatOut,clBLT2);
		}
		else
		{
			if(strstr(clOrigData,"<belt_2/>") != NULL)
			{
				strcat(pclFldOut,",BLT2");
				strcat(pclDatOut,", ");
				blBLT2Found = TRUE;
			}
		}
	}/*if(strstr(pcgIgnoreSqMi,"<belt_2>") == NULL)*/

	if(((strstr(pcgIgnoreSqMi,"<calls_1>") == NULL) && (blIsSqMiFlight == TRUE)) ||  
		((blIsSqMiFlight == FALSE) && (strstr(pcgIgnoreEverOnline, "<calls_1>") == NULL)))
	{
		/*get calls_1 / gate open time */
		blGD1XFound		= GetElementValue(clGD1X, clOrigData, "<calls_1>", "</calls_1>");
		if(blGD1XFound == TRUE)
		{
			strcat(clGD1X,"00");
			dbg(DEBUG,"%05d: clGD1X = <%s>",__LINE__,clGD1X);
			strcat(pclFldOut,",GD1X");
			strcat(pclDatOut,",");
			strcat(pclDatOut,clGD1X);
		}
		else
		{
			if(strstr(clOrigData,"<calls_1/>") != NULL)
			{
				strcat(pclFldOut,",GD1X");
				strcat(pclDatOut,", ");
				blGD1XFound = TRUE;
			}
		}
	} /*if(strstr(pcgIgnoreSqMi,"<calls_1>") == NULL)*/

	if(((strstr(pcgIgnoreSqMi,"<calls_2>") == NULL) && (blIsSqMiFlight == TRUE)) || 
		((blIsSqMiFlight == FALSE) && (strstr(pcgIgnoreEverOnline, "<calls_2>") == NULL)))
	{
		/*get calls_2  boarding time*/
		blBOAOFound		= GetElementValue(clBOAO, clOrigData, "<calls_2>", "</calls_2>");
		if(blBOAOFound == TRUE)
		{
			strcat(clBOAO,"00");
			dbg(DEBUG,"%05d: clBOAO = <%s>",__LINE__,clBOAO);
			strcat(pclFldOut,",BOAO");
			strcat(pclDatOut,",");
			strcat(pclDatOut,clBOAO);
		}
		else
		{
			if(strstr(clOrigData,"<calls_2/>") != NULL)
			{
				strcat(pclFldOut,",BOAO");
				strcat(pclDatOut,", ");
				blBOAOFound = TRUE;
			}
		}
	}/*if(strstr(pcgIgnoreSqMi,"<calls_2>") == NULL)*/
	
	if(((strstr(pcgIgnoreSqMi,"<calls_3>") == NULL) && (blIsSqMiFlight == TRUE)) || 
		((blIsSqMiFlight == FALSE) && (strstr(pcgIgnoreEverOnline, "<calls_3>") == NULL)))
	{
		/*get calls_3 last call 1 */
		blFCA1Found		= GetElementValue(clFCA1, clOrigData, "<calls_3>", "</calls_3>");
		if(blFCA1Found == TRUE)
		{
			strcat(clFCA1,"00");
			dbg(DEBUG,"%05d: clFCA1 = <%s>",__LINE__,clFCA1);
			strcat(pclFldOut,",FCA1");
			strcat(pclDatOut,",");
			strcat(pclDatOut,clFCA1);
		}
		else
		{
			if(strstr(clOrigData,"<calls_3/>") != NULL)
			{
				strcpy(clFCA1," ");
				blFCA1Found = TRUE;
				strcat(pclFldOut,",FCA1");
				strcat(pclDatOut,", ");
				strcat(pclDatOut,clFCA1);
			}
		}
	}/*if(strstr(pcgIgnoreSqMi,"<calls_3>") == NULL)*/
		
	if(((strstr(pcgIgnoreSqMi,"<calls_4>") == NULL) && (blIsSqMiFlight == TRUE)) || 
		((blIsSqMiFlight == FALSE) && (strstr(pcgIgnoreEverOnline, "<calls_4>") == NULL)))
	{
		/*get calls_4 lastcall 2 */
		blFCA2Found		= GetElementValue(clFCA2, clOrigData, "<calls_4>", "</calls_4>");
		if(blFCA2Found == TRUE)
		{	
			strcat(clFCA2,"00");
			dbg(DEBUG,"%05d: clFCA2 = <%s>",__LINE__,clFCA2);
			strcat(pclFldOut,",FCA2");
			strcat(pclDatOut,",");
			strcat(pclDatOut,clFCA2);
		}
		else
		{
			if(strstr(clOrigData,"<calls_4/>") != NULL)
			{
				blFCA2Found = TRUE;
				strcpy(clFCA2," ");
				strcat(pclFldOut,",FCA2");
				strcat(pclDatOut,", ");
				strcat(pclDatOut,clFCA2);
			}
		}
	}/*if(strstr(pcgIgnoreSqMi,"<calls_4>") == NULL)*/  

	if((blFCA1Found == TRUE) && (clFCA1[0] != ' ') && (blFCA2Found == TRUE) && (clFCA2[0] != ' '))
	{
		dbg(DEBUG,"%05d: Final-call1 and final-call-2 found in same message, so put final-call2 to FCAL",__LINE__);
		strcat(pclFldOut,",FCAL");
		strcat(pclDatOut,",");
		strcat(pclDatOut,clFCA2);
	}
	else if((blFCA1Found == TRUE) && (clFCA1[0] != ' '))
	{
		strcat(pclFldOut,",FCAL");
		strcat(pclDatOut,",");
		strcat(pclDatOut,clFCA1);
	}
	else if((blFCA2Found == TRUE) && (clFCA2[0] != ' '))
	{
		strcat(pclFldOut,",FCAL");
		strcat(pclDatOut,",");
		strcat(pclDatOut,clFCA2);
	}


	if(((strstr(pcgIgnoreSqMi,"<calls_5>") == NULL) && (blIsSqMiFlight == TRUE)) ||  
		((blIsSqMiFlight == FALSE) && (strstr(pcgIgnoreEverOnline, "<calls_5>") == NULL)))
	{
		/*get calls_5 / gate close time */
		blGD1YFound		= GetElementValue(clGD1Y, clOrigData, "<calls_5>", "</calls_5>");
		if(blGD1YFound == TRUE)
		{
			strcat(clGD1Y,"00");
			dbg(DEBUG,"%05d: clGD1Y = <%s>",__LINE__,clGD1Y);
			strcat(pclFldOut,",GD1Y");
			strcat(pclDatOut,",");
			strcat(pclDatOut,clGD1Y);
		}
		else
		{
			if(strstr(clOrigData, "<calls_5/>") != NULL)
			{
				blGD1YFound = TRUE;
				dbg(DEBUG,"%05d: clGD1Y = <%s>",__LINE__,clGD1Y);
				strcat(pclFldOut,",GD1Y");
				strcat(pclDatOut,", ");
			}
		}
	}/*if(strstr(pcgIgnoreSqMi,"<calls_5>") == NULL)*/


	if(((strstr(pcgIgnoreSqMi,"<est>") == NULL) && (blIsSqMiFlight == TRUE)) || 
		((blIsSqMiFlight == FALSE) && (strstr(pcgIgnoreEverOnline, "<est>") == NULL)))
	{
		/*get estimated time depending of arrival/departure*/
		pclTemp = CedaGetKeyItem(clTmp, &llSize, clOrigData, "<est>", "</est>", TRUE);
		if(pclTemp != NULL)
		{
			if(clADID[0] == 'A')
			{
				strcpy(clETOA,clTmp);
				strcat(clETOA,"00");
				blETOAFound = TRUE;
				dbg(DEBUG,"%05d: original clETOA = <%s>",__LINE__,clETOA);
				ilRC = AddSecondsToCEDATime(clETOA, igAddMinToETA * 60 ,1);
				dbg(DEBUG,"%05d: plus <%d> minutes to clETOA = <%s>",__LINE__,igAddMinToETA,clETOA);
					
				strcat(pclFldOut,",ETOA");
				strcat(pclDatOut,",");
				strcat(pclDatOut,clETOA);
			}
			if((clADID[0] == 'D') && (blIsSqMiFlight == FALSE))
			{
				strcpy(clETOD,clTmp);
				strcat(clETOD,"00");
				blETODFound = TRUE;
				dbg(DEBUG,"%05d: clETOD = <%s>",__LINE__,clETOD);
				strcat(pclFldOut,",ETOD");
				strcat(pclDatOut,",");
				strcat(pclDatOut,clETOD);
			}
		}
		else if(strstr(clOrigData,"<est/>") != NULL)
		{
			if(clADID[0] == 'A')
			{
				blETOAFound = TRUE;
				dbg(DEBUG,"%05d: clETOA = <%s>",__LINE__,clETOA);
				strcat(pclFldOut,",ETOA");
				strcat(pclDatOut,", ");
			}
			if((clADID[0] == 'D') && (blIsSqMiFlight == FALSE))
			{
				blETODFound = TRUE;
				dbg(DEBUG,"%05d: clETOD = <%s>",__LINE__,clETOD);
				strcat(pclFldOut,",ETOD");
				strcat(pclDatOut,", ");
			}
		}
	}/*if(strstr(pcgIgnoreSqMi,"<calls_3>") == NULL)*/

	if(strcmp(pclCmd,"UFR") == 0)	/* PRF8799 */
	{
	  if (clADID[0] == 'A')
	  { 
	    (void) Lookup_AFT (clADID[0], clFLNO, clFLDA,
	      "GTA1", pcg_AFT_Gate); 
	    Add_Gate_n_Term (clADID[0], blTGA1Found, clTGA1,
	        blGTA1Found, clGTA1, pclFldOut, pclDatOut);
	  }
	  else if (clADID[0] == 'D') 
	  { 
	    (void) Lookup_AFT (clADID[0], clFLNO, clFLDA,
	      "GTD1", pcg_AFT_Gate); 
	    Add_Gate_n_Term (clADID[0], blTGD1Found, clTGD1,
	        blGTD1Found, clGTD1, pclFldOut, pclDatOut); 
	} 
	} 

	dbg(TRACE,"%05d: ############# wrapElseData - END  ###########",__LINE__);

    return ilRC;
}




static int GetElementValue(char *pclDest, char *pclOrigData, char *pclBegin, char *pclEnd)
{
	char *pclTemp = NULL;
	long llSize = 0;
	char clBuffer[200] = "\0";
	
	pclTemp = CedaGetKeyItem(pclDest, &llSize, pclOrigData, pclBegin, pclEnd, TRUE);
	if(pclTemp != NULL)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}


static void Init_Else()
{
	ReadConfigDefault("ELSE","NATURE-IN",pcgNatureIn,"");
    ReadConfigDefault("ELSE","NATURE-OUT",pcgNatureOut,"");
	ReadConfigDefault("ELSE","IGNORE_SQMI",pcgIgnoreSqMi,"");
	ReadConfigDefault("ELSE","ADD_MINUTES_TO_ELSE_ETA",pcgADD_MINUTES_TO_ELSE_ETA,"0");
	ReadConfigDefault("ELSE","IGNORE_EVER_ONLINE",pcgIgnoreEverOnline,"");
	ReadConfigDefault("ELSE","IGNORE_TERMINAL",pcgIgnoreTerminal,"");
	igAddMinToETA = atoi(pcgADD_MINUTES_TO_ELSE_ETA);
	InitMapNature();
	igQToElse = tool_get_q_id ("toelse");
	dbg(TRACE,"%05d: <Init_Else> mod_id for toelse: <%d>",__LINE__,igQToElse);
	strcpy (pcg_AFT_Gate, " ");
}


/******************************************************************************/
/* The InitCheckNature routine                                                */
/* This routine reformats the comma seperated list to NULL seperated list     */
/******************************************************************************/
static void InitMapNature()
{
char *pclTmp1;
char *pclTmp2;
int ilCnt= 0;

      pclTmp1 = pcgNatureIn;
      while ((pclTmp1 = strchr(pclTmp1,',')) != NULL)
      {
         *(pclTmp1++) = 0;
         ilCnt++;
      }
      pclTmp1 = pcgNatureOut;
      while ((pclTmp1 = strchr(pclTmp1,',')) != NULL)
      {
         *(pclTmp1++) = 0;
         ilCnt--;
      }
      if (ilCnt != 0)
      {
         dbg(DEBUG,"Syntax error in mapping of nature code: different number of in and out");
         dbg(DEBUG,"=== Mapping of nature code disabled! === ");
         pcgNatureIn[0] = 0;
      }
      else
      {
         pclTmp1 = pcgNatureIn;
         pclTmp2 = pcgNatureOut;
         while (pclTmp1[0] != 0) 
         {
            dbg(DEBUG,"Init of nature code mapping <%s> to <%s> ...",pclTmp1,pclTmp2);
            pclTmp1= pclTmp1 + strlen(pclTmp1) +1 ;
            pclTmp2= pclTmp2 + strlen(pclTmp2) +1 ;
         }
      }
}

static void MapNature(char *pclTTYP)
{
	char *pclTmp1;
	char *pclTmp2;

   if (pclTTYP!=NULL && pclTTYP[0]!=0) 
   {
	  dbg(DEBUG,"%05d: checking nature code <%s> ...",__LINE__,pclTTYP);
      pclTmp1 = pcgNatureIn;
      pclTmp2 = pcgNatureOut;
      while ((pclTmp1[0] != 0) && (pclTTYP[0] != 0))
      {
         dbg(DEBUG,"checking map code <%s> ...",pclTmp1);
         if (strcmp(pclTmp1,pclTTYP) == 0)
         {
            dbg(DEBUG,"mapping nature code <%s> to <%s> ...",pclTTYP,pclTmp2);
            pclTTYP[0] = 0;
         }
         else
         {
            pclTmp1= pclTmp1 + strlen(pclTmp1) +1 ;
            pclTmp2= pclTmp2 + strlen(pclTmp2) +1 ;
         }
      }
/*      if (pclTTYP[0] == 0)
      {
         sgTamsToAft[igTtypNdx].AftValue = pclTmp2;
      }
	  */
	  if(pclTTYP[0] == 0)
	  {
		strcpy(pclTTYP,pclTmp2);
	  }
   }
}

/******************************************************************************/
/* Routine: ReadConfigDefault                                                 */
/*          read configuration, use default if keyword not found              */
/******************************************************************************/
static void ReadConfigDefault(char *pcpSection,char *pcpKeyword, 
                             char *pcpCfgBuffer, char *pcpDefault)
{
 int ilRc = RC_SUCCESS;

 char clSection[124] = "\0";
 char clKeyword[124] = "\0";

   strcpy(clSection,pcpSection);
   strcpy(clKeyword,pcpKeyword);

   ilRc = iGetConfigEntry(cgConfigFile,clSection,clKeyword,
	      CFG_STRING,pcpCfgBuffer);
   if(ilRc != RC_SUCCESS)
   {
     strcpy(pcpCfgBuffer, pcpDefault);
     dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
     dbg(TRACE,"using default <%s> for <%s>",pcpCfgBuffer,clKeyword);
   } 
   else
   {
      dbg(DEBUG,"Config Entry <%s>,<%s>:<%s> found in %s",
          clSection, clKeyword ,pcpCfgBuffer, cgConfigFile);
   }/* end of if */
   /*fd*/
}

int FormatFlnoStr(char *pcpFlnoIn)
{
   char pclFltn[16] = "               ";
   char pclAlc[16] = "               ";
   char clSuff = 0;
   char *ptr;
   int  ilPos;
   int  ilFltn;
	char clOrigFlno[20] = "\0";
	char clFlnoOut[20] = "\0";
	char clError[200] = "\0";

	strcpy(clOrigFlno,pcpFlnoIn);
   if (strlen(pcpFlnoIn)>2) /* avoid empty MFF */
   {
      ilPos= 0;
      ptr = pcpFlnoIn;
      strncpy(pclAlc, ptr, 2); /* first 2 are arbitrary */
      ptr++;ptr++; /* advance to next */
      ilPos++;ilPos++;
      if (isalpha((int) *ptr) && *ptr != '\0')
      {
        pclAlc[ilPos] = *ptr;
        ptr++;ilPos++;
      }
      else if (isdigit((int)*ptr)) {
        /* do nothing */
      }
      if (ilPos>2)
      {
         sprintf(clError,"Error while formatting <%s>. ALC2 <%s> to long",clOrigFlno,pclAlc);
		 dbg(TRACE,"%05d: %s",__LINE__,clError);
		 alert(clError);
		return RC_FAIL;		
      }
      else
      {   
         pclAlc[ilPos] = 0;
         ilPos=0;
         while (*ptr == ' ' && *ptr != '\0') ptr++; /* skip over possible ' ' */
         if (*ptr == '/' && strlen(ptr) > 3 )
         {  /* i.e. "SQ/LH4711" */
            ptr = ptr + 3 ; /* skip over "/<alc2>" of code share */
         }
         while (isdigit((int) *ptr) && *ptr != '\0')
         {
           pclFltn[ilPos] = *ptr;
           ptr++;ilPos++;
         }
         pclFltn[ilPos] = 0;
         clSuff= *ptr;
         if (ilPos>5)
         {
            sprintf(clError,"Error in <%s>: number <%s> to long",clOrigFlno,pclFltn);
            dbg(TRACE,"FormatFlnoStr: %s",clError);
            return RC_FAIL;
         }
         else if (ilPos==0)
         {
            sprintf(clError,"Error in <%s>: <%s>: number does not begin with digits",clOrigFlno,pclFltn);
            dbg(TRACE,"FormatFlnoStr: %s",clError);
            return RC_FAIL;
         }
         else
         {
            ilFltn= atoi(pclFltn);
            /* this will append the suffix directly behind the number :
             * sprintf(pcpFlnoOut,"%-3s%03d%c",pclAlc,ilFltn,clSuff); *
             * 20030822 JIM: write the suffix to 9 char of FLNO:      *
             */
            if (clSuff == 0)
            {
               sprintf(clFlnoOut,"%-3s%03d",pclAlc,ilFltn);
            }
            else
            {
               sprintf(clFlnoOut,"%-3s%03d   ",pclAlc,ilFltn);
               clFlnoOut[8]=clSuff;
               clFlnoOut[9]=0; /* if ilFltn has more than 3 digits, sprintf
                                   has written more than 9 chars */
			   
            }
         }
      }
   }
   else
   {
	   dbg(TRACE,"%05d: strlen of pcpFlnoIn <=2",__LINE__);
	return RC_FAIL;
   }

   strcpy(pcpFlnoIn,clFlnoOut);
   return RC_SUCCESS;
}

/************************************************************************
 * LookupGateTerminal - lookup GATTAB for TERM				*
 * Assumption : GNAM is unique in GATTAB. If it is not, we lookup only	*
 * the first record with the lowest URNO.				*
 ************************************************************************/

static int LookupGateTerminal (char *gnam, char *term)
{
  short slFkt = 0;
  short slCursor = 0;
  char clSqlBuf[2048] ="\0";
  char clDataArea [2048];
  int pos, ilRC;

  sprintf (clSqlBuf,
        "SELECT TERM FROM GATTAB WHERE "
        "GNAM = '%s' ORDER BY URNO", gnam); 

  dbg (TRACE, "LookupGateTerminal: [%s]", clSqlBuf); 

  slFkt = START;
  slCursor = 0;
  ilRC = RC_SUCCESS;

  ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clDataArea);

  if (ilRC == RC_SUCCESS)
  {
    BuildItemBuffer (clDataArea, "TERM", 1, ",");
    (void) get_real_item (term, clDataArea, 1); 
  } 
  else
  {
    if (ilRC == RC_FAIL)
    {
      dbg (TRACE, "LookupGateTerminal: ORACLE ERROR !!"); 
    }
  }

  /* slFkt = NEXT; -- FYI this is how multiple rec's are read */

  commit_work();
  close_my_cursor (&slCursor);
  return ilRC;
}

/************************************************************************
 *  Add_Gate_n_Term - Append gate and terminal based on rules 		*
 *  in PRF 8799								* 
 *  Important: Assume ADID is never blank				*
 ************************************************************************/

static void Add_Gate_n_Term (char cAdid,
		  	     int flagTerm, char *clElseTerm,
		             int flagGate, char *clElseGate,
			     char *pclFldOut,char *pclDatOut)
{
  int	ires;
  char  clUfisTerm [2];
  
  /* Get TERM from GATTAB */
  if (clElseGate [0] != '\0')
  {
    ires = LookupGateTerminal (clElseGate, clUfisTerm);
    dbg(TRACE,"Add_Gate_n_Term: From GATTAB: GNAM [%s]->TERM [%s]",
      clElseGate, clUfisTerm); 
  }

  /* Start testing various gate/term combinations */

  /********** Gate present & non-blanked. ***************************/
  if ((flagGate == TRUE) && (clElseGate [0] != '\0'))
  { 
    if (cAdid == 'A')
      strcat(pclFldOut,",GTA1");
    else
      strcat(pclFldOut,",GTD1");	/* Assume ADID never blank */
    strcat(pclDatOut,",");
    strcat(pclDatOut,clElseGate); 

    /*
	Allow the possibility of GNAM w/o TERM in GATTAB by adding terminal
	only if found, otherwise just send gate only to FLIGHT.
    */
    if (ires == RC_SUCCESS)
    {
      if (cAdid == 'A')
        strcat(pclFldOut,",TGA1");
      else
        strcat(pclFldOut,",TGD1");
      strcat(pclDatOut,",");
      strcat(pclDatOut,clUfisTerm); 
    } 

    /*
	A check for 'T0' is not needed above since we use GATTAB.TERM.  
	Reference code is:
	if ((pcgIgnoreTerminal [0] != '\0')
		&& (!(strcmp (clTGA1,pcgIgnoreTerminal)))) 
    */
  } 

  /************ Both absent *****************************************/ 
  else if ((flagGate == FALSE) && (flagTerm == FALSE))
     dbg(TRACE,"Add_Gate_n_Term: ELSE Gate & Term not present");

  /******** Gate absent, Term present *******************************/
  /******** Special - never blank out a terminal ********************/
  else if ((flagGate == FALSE) && (flagTerm == TRUE))
  {
    /* Check for 'T0' */
    if ((pcgIgnoreTerminal [0] != '\0')
		&& (!(strcmp (clElseTerm, pcgIgnoreTerminal)))) 
    {
      dbg(TRACE,
        "Add_Gate_n_Term:No ELSE gate; ELSE Term[%s] ignored", clElseTerm); 
    }
    else if (clElseTerm [0] == '\0')
    {
      dbg(TRACE,"Ignoring Terminal blanking from ELSE");
    } 
    else
    { 
      /*
	pcg_AFT_Gate is a space if this is a creation, or if gate is blank in
	AFTTAB. For either of these cases, the terminal must be accepted.
      */

      if (pcg_AFT_Gate [0] == ' ')
      {
      if (cAdid == 'A')
        strcat(pclFldOut,",TGA1");
      else
        strcat(pclFldOut,",TGD1");
      strcat(pclDatOut,",");
      strcat(pclDatOut,clElseTerm); 
      }
      else
      {
        dbg(TRACE,"AFTTAB gate [%s]; ignore lonely ELSE terminal", pcg_AFT_Gate);
      }
    }
  }

  /*********** Gate blanked, term present ***************************/ 
  /******** Special - never blank out a terminal ********************/
  else if ((flagGate == TRUE) && (clElseGate [0] == '\0') && (flagTerm == TRUE))
  { 
    if (cAdid == 'A')
      strcat(pclFldOut,",GTA1");
    else
      strcat(pclFldOut,",GTD1");
    strcat(pclDatOut,", ");
    
    /* Repeated code follows... */

    /* Check for 'T0' */
    if ((pcgIgnoreTerminal [0] != '\0')
		&& (!(strcmp (clElseTerm, pcgIgnoreTerminal)))) 
    {
      dbg(TRACE,
        "Add_Gate_n_Term: ELSE gate blanked; ELSE Term [%s] ignored",
        clElseTerm); 
    } 
    else if (clElseTerm [0] == '\0')
    {
      dbg(TRACE,"Ignoring Terminal blanking from ELSE");
    } 
    else
    { 
      if (cAdid == 'A')
        strcat(pclFldOut,",TGA1");
      else
        strcat(pclFldOut,",TGD1");
      strcat(pclDatOut,",");
      strcat(pclDatOut,clElseTerm); 
    } 
  }
  else
  {
    dbg(TRACE,
      "Add_Gate_n_Term: No matching condition; not adding gate or term"); 
  } 
} 

/************************************************************************
 * Lookup_AFT - general purpose lookup of a field in AFTTAB. 		*
 * clRes contains a space if field empty.				* 
 * Return value is the sql_if return code.				*
 ************************************************************************/

static int  Lookup_AFT (char cAdid, char *clFLNO, char *clFlda,
	                char *clField, char *clRes)
{
  short slFkt = 0;
  short slCursor = 0;
  char clSqlBuf[2048] ="\0";
  char clDataArea [2048];
  int pos, ilRC;

  sprintf (clSqlBuf,
        "SELECT %s FROM AFTTAB  WHERE "
        "ADID = '%c' AND  FLNO = '%s' AND FLDA = '%s'",
	clField, cAdid, clFLNO, clFlda); 

  dbg (TRACE, "LookupAFT: [%s]", clSqlBuf); 
  /*
	default value for no result. This will also be the
	value if an SQL error occurs.
  */
  strcpy (clRes, " ");

  slFkt = START;
  slCursor = 0;
  /* ilRC = RC_SUCCESS; */

  ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clDataArea);

  if (ilRC == RC_SUCCESS)
  {
    BuildItemBuffer (clDataArea, clField, 1, ",");
    (void) get_real_item (clRes, clDataArea, 1); 
    /* BuildItemBuffer nullifies blanks! */
    if (clRes [0] == '\0')
      strcpy (clRes, " ");
  } 
  else
  {
    if (ilRC == RC_FAIL)
    {
      dbg (TRACE, "Lookup_AFT: ORACLE ERROR !!"); 
    }
    /* And if we get here that means no record found ..? */
  }

  /* slFkt = NEXT; -- FYI this is how multiple rec's are read */

  commit_work();
  close_my_cursor (&slCursor);
  return ilRC;
}

