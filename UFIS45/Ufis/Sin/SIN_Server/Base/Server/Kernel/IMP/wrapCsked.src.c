#ifndef _DEF_mks_version
#define _DEF_mks_version
#include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Sin/SIN_Server/Base/Server/Kernel/IMP/wrapCsked.src.c 1.9a 2012/04/28 15:46:52SGT fya Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         : Hans-J�rgen Ebert                                         */
/* Date           : 19.03.2003                                                */
/* Description    :                                                           */
/*                                                                            */
/* Update history :                                                           */
/*	20071029	DKA	PR 442 - Add lookup function for aircraft   */
/*				type lookup.				    */
/*				Add reading of Mapping_Table in 	    */
/*				wrapCskedData as that appears to be the	    */
/*				only entry point from imphdl.c. Did not     */
/*				put this in imphdl.c since code structure   */
/*				of imphdl.c puts general parameters there   */
/*				and interface specific in wrapppers. 
/*				CRG - retain ufis ACT type		    */
/*				NFL,DFL,CFL,RIS,NOP - perform lookup.	    */
/*				If no lookup found, used CSKED data from    */
/*				their msg.				    */
/*	20071030	DKA	CRG messages sometimes miss the last CSKED  */
/*				IATA. Introduce config parameter to allow   */
/*				such a message to still be accepted.	    */
/*				Parameter is ALLOW_CRG_MISSING_IATA	    */
/*									    */
/* 20120428 FYA Modifying the length of FLDA*/
/* 20120524 FYA Fix the original return FLDA length as 8, and return another global varible whose length is 12*/
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */

/**********************************************************************
 * Function Prototypes
 **********************************************************************
 */

static int wrapCskedData(char *pcpData,char *pcpCommand, char *pcpSelection, char *pcpFields, char *pcpNewData);
static int CskedAircraftChange(char *pcpFields, char *pcpData);
static int CskedScheduleChange(char *pcpCommand, char *pcpFields, char *pcpData);
static int CskedOperationalUpdate(char *pcpFields, char *pcpData);
static void CskedTrimRight(char *s);
static void CskedWrapperInit (void);	/* Added for PR442 Oct07 */
static int CskedLookupMapTabAct (char *csked_icao, char *ufis_iata,
	char *ufis_icao);

/**********************************************************************
 * Static variables 
 **********************************************************************
 */

#define RC_DATA_ERR 2222		/* Per JIM..	*/

static int	CskedWrapperInited = 0;
static char	CskedMapTab [16];	/* Mapping table for ACT type */
static int	Csked_allow_CRG_missing_iata = 0;
/**********************************************************************/

static int wrapCskedData(char *pcpData,char *pcpCommand, char *pcpSelection, char *pcpFields, char *pcpNewData)
/* pcpData: The original Data that has to be transformed*/
/* Command, Selection,Fields,Data: the return-values of the function.... input for impman*/
{
    int ilRC = RC_SUCCESS;
    char clCommand[10] = "\0";
    char clSelection[50] = "\0";
    char clFields[500] = "\0";
    char clData[2002] = "\0";
    char *pclData = NULL;
    char clUpdCode[6] = "\0";
    int ilLen = 0;
    int ilDataLen = 0;

    if (CskedWrapperInited == 0)
	CskedWrapperInit ();
    
    ilDataLen = strlen(pcpData);

    if (ilDataLen < 2002)
    {
	strncpy(clData,pcpData,ilDataLen);
	clData[ilDataLen+1] = '\0';
    }


    strcpy(pcpSelection,"IFNAME:CSKED,TIME:UTC");

    dbg(TRACE,"%05d: Funktion Csked received: Data <%s>",__LINE__,clData);
    
    pclData = clData; /*Pointer auf beginn von Data*/
    pclData += 60;    /*Pointer hinter den Header verschieben (laut ICD 60 character)*/
    
    dbg(TRACE,"%05d: Data without Header: <%s>",__LINE__,pclData);
    strncpy(clUpdCode,pclData,3);
    clUpdCode[3] = '\0';
    strcpy(clData,pclData);
    dbg(TRACE,"%05d: Data without Header2: <%s>",__LINE__,pclData);
    dbg(TRACE," This is cldata to give to functions: <%s>",clData);
    dbg(TRACE," This is the update-Code: <%s>",clUpdCode);
    if(strcmp(clUpdCode,"CRG") == 0)
    {

	ilRC = CskedAircraftChange(pcpFields, clData);
	if(ilRC == RC_SUCCESS)
	{
	    strcpy(pcpData,pclData);
	    strcpy(pcpCommand,"UFR");
	}
	else
	{
	    strcpy(pcpData,"\0");
	    strcpy(pcpCommand,"ERR");
	    strcpy(pcpSelection,"Corrupted Data");
	    dbg(TRACE,"%05d: Corrupted Data");
	    ilRC = RC_FAIL;
	}
    }
    else
    {
	if((strcmp(clUpdCode,"NFL") == 0) ||
	   (strcmp(clUpdCode,"DFL") == 0) ||
	   (strcmp(clUpdCode,"CFL") == 0) ||
	   (strcmp(clUpdCode,"RIS") == 0) ||
	   (strcmp(clUpdCode,"NOP") == 0))
	{
	    ilRC = CskedScheduleChange(pcpCommand, pcpFields, clData);
	    if(ilRC != RC_SUCCESS)
	    {
		strcpy(pcpData,"\0");
		strcpy(pcpCommand,"ERR");
		strcpy(pcpSelection,"Corrupted Data");
		dbg(TRACE,"%05d: Corrupted Data");
		ilRC = RC_FAIL;
	    }
	}
	else
	{
	    if(strcmp(clUpdCode,"MVT") == 0)
	    {
		ilRC = CskedOperationalUpdate(pcpFields, clData);
		strcpy(pcpCommand,"UFR");
	    }
	    else
	    {
		strcpy(pcpData,"\0");
		strcpy(pcpCommand,"ERR");
		strcpy(pcpSelection,"Corrupted Data");
		dbg(TRACE,"%05d: Update-Code conforms not to CSKED-ICD");
		ilRC = RC_FAIL;
	    }
	}
    }
    strcpy(pcpNewData,clData);

    return ilRC;
}


static int CskedAircraftChange(char *pcpFields, char *pcpData)
{
    int ilRC = RC_SUCCESS;
    char *pclData = NULL;
    char *pclFields = NULL;
    //char clTemp[10] = "\0"; /* Frank v1.9 */
    char clTemp[16] = "\0";   /* Frank v1.9 */
    char clData[2002] = "\0";
    char cl_csked_icao [8];

    dbg(TRACE,"%05d: ----- CskedAircraftChange start -----",__LINE__);
    dbg(TRACE,"%05d: pcpFields: <%s>",__LINE__,pcpFields);
    dbg(TRACE,"%05d: pcpData  : <%s>",__LINE__,pcpData);
    
    pcpFields[0] = '\0';
    pclData = pcpData;
    pclData += 3; /* Update-Code ignorieren*/
/* Now get ALC2/3 */
    strncpy(clTemp,pclData,3);
    pclData += 3;
    clTemp[3] = '\0';
    CskedTrimRight(clTemp);
    if(strlen(clTemp) == 2)
    {
	strcat(pcpFields,"ALC2,");
    }
    else
    {
	strcat(pcpFields,"ALC3,");
    }
    if((strcmp(clTemp,"SQ") != 0) && (strcmp(clTemp,"MI") != 0))
    {
	return RC_FAIL;
    }
    strcat(clData,clTemp);
    strcat(clData,",");
    
/* Now get Flight-Number */
    strcat(clData,clTemp);
    strcat(clData," ");
    clTemp[0] = '\0';
    strncpy(clTemp,pclData,5);
    pclData += 5;
    clTemp[5] = '\0';
    CskedTrimRight(clTemp);
    strcat(pcpFields,"FLNO,");
    strcat(clData,clTemp);
    strcat(clData,",");

/* Now get scheduled time of deparutre */
/* Now the date*/
		/* Frank v1.9 */
    /*
    clTemp[0] = '\0';
    strncpy(clTemp,pclData,8);
    pclData += 8;
    clTemp[8] = '\0';
    strcat(pcpFields,"FLDA,");
    strcat(clData,clTemp);
    */
    clTemp[0] = '\0';

/*Frank v1.9a*/
    //strncpy(clTemp,pclData,12);
    strncpy(clTemp,pclData,8);
    memset(clFLDAFromWrapCsked,0,sizeof(clFLDAFromWrapCsked));
    strncpy(clFLDAFromWrapCsked,pclData,12);
/*Frank v1.9a*/
    
    pclData += 12;
    clTemp[12] = '\0';
    strcat(pcpFields,"FLDA,");
    strcat(clData,clTemp);
   /* Frank v1.9 */
/* Skipp the  time */    
    /* Frank v1.9 */
    /*
    pclData += 4;
    */
    strcat(clData,",");
		
		/* Frank v1.9 */
/* Now get sector Number*/
    clTemp[0] = '\0';
    strncpy(clTemp,pclData,1);
    pclData += 1;
    clTemp[1] = '\0';
    strcat(pcpFields,"SECN,");
    strcat(clData,clTemp);
    strcat(clData,",");

/* Now get Origin 3LC */
    clTemp[0] = '\0';
    strncpy(clTemp,pclData,3); /* 3LC but field is 5 char*/
    pclData += 5;
    clTemp[3] = '\0';
    strcat(pcpFields,"ORG3,");
    strcat(clData,clTemp);
    strcat(clData,",");
    
/* Now get Destination 3LC*/
    clTemp[0] = '\0';
    strncpy(clTemp,pclData,3); /* 3LC but field is 5 char*/
    pclData += 5;
    clTemp[3] = '\0';
    strcat(pcpFields,"DES3,");
    strcat(clData,clTemp);
    strcat(clData,",");
 
/* Now get Registration */
    clTemp[0] = '\0';
    strncpy(clTemp,pclData,8);
    pclData += 8;
    clTemp[8] = '\0';
    CskedTrimRight(clTemp);
    /* strcat(pcpFields,"REGN,"); */
    strcat(pcpFields,"REGN");
    strcat(clData,clTemp);
    /* strcat(clData,","); */

/* Now get Internal Aircraft-Type */
/* PR 442 - ignore CSKED a/c type but log it */
    clTemp[0] = '\0';
    strncpy(clTemp,pclData,5);
    pclData += 5;
    clTemp[5] = '\0';
    strcpy (cl_csked_icao, clTemp);
/*
    strcat(pcpFields,"ACT5,");
    strcat(clData,clTemp);
    strcat(clData,",");
*/
/* Now get Aircraft Type */
/* PR 442 - ignore CSKED a/c type but log it */
    clTemp[0] = '\0';
    strncpy(clTemp,pclData,5);
    pclData += 5;
    clTemp[5] = '\0';
    CskedTrimRight(clTemp);

    if(strlen(clTemp) == 0)	/* CSKED sent blank IATA */
    {
      if (Csked_allow_CRG_missing_iata == 0)
        return RC_FAIL;
    }

    /* strcat(pcpFields,"ACT3"); */ 
    /* strcat(clData,clTemp); */
    dbg (TRACE, "AircraftChange: Ignore CSKED ICAO [%s] IATA [%s]",
	    cl_csked_icao, clTemp); 

    strcat(clData,"\0");
    dbg(TRACE," end of Aircraftchange: clData: <%s>",clData);
    strcpy(pcpData,clData);
    dbg(TRACE," end of Aircraftchange: pcpData: <%s>",pcpData);

    return ilRC;
}


static int CskedScheduleChange(char *pcpCommand, char *pcpFields, char *pcpData)
{
    int ilRC = RC_SUCCESS;
    char *pclData = NULL;
    char *pclFields = NULL;
    /*char clTemp[10] = "\0";*/ /* Frank v1.9 */
    char clTemp[16] = "\0";			/* Frank v1.9 */
    char clData[2002] = "\0";
    char clUpdateCode[10] = "\0";
    char clAlc2[10] = "\0";
    char clFlno[10] = "\0";
    char clFlda[10] = "\0";
    char clNums[10] = "\0";
    char clActi[10] = "\0";
    char clAct[10] = "\0";
    char clData1[50] = "\0";
    char cl_ufis_iata [8], cl_ufis_icao [8];
    int ilNums = 0;
    int i = 0, resLU;;

    dbg(TRACE,"%05d: ----- CskedScheduleChange start -----",__LINE__);
    dbg(TRACE,"%05d: pcpFields: <%s>",__LINE__,pcpFields);
    dbg(TRACE,"%05d: pcpData  : <%s>",__LINE__,pcpData);
    
    pcpFields[0] = '\0';
    pclData = pcpData;
/*Now get update-code*/
    strncpy(clUpdateCode,pclData,3);
    pclData += 3;
    clUpdateCode[3] = '\0';

    if(strcmp(clUpdateCode,"NFL") == 0)
    {
	strcpy(pcpCommand,"IFR");
    }
    else
    {
	if(strcmp(clUpdateCode,"DFL") == 0)
	{
	    strcpy(pcpCommand,"DFR");
	}
	else
	{
	    strcpy(pcpCommand,"UFR");
	}
    }

/* Now set FTYP if necessary */
    if(strcmp(clUpdateCode,"RIS") == 0)
    {
	strcat(pcpFields,"FTYP,");
	strcat(clData,"S,");
    }
    
    if(strcmp(clUpdateCode,"NOP") == 0)
    {
	strcat(pcpFields,"FTYP,");
	strcat(clData,"X,");
    }
    
    
/* Now get ALC2/3 */
    strncpy(clTemp,pclData,3);
    pclData += 3;
    clTemp[3] = '\0';
    CskedTrimRight(clTemp);
    if((strcmp(clTemp,"MI") == 0) || (strcmp(clTemp,"SQ") == 0))
    {
	strcpy(clAlc2,clTemp);
	strcat(pcpFields,"ALC2,");
	strcat(clData,clTemp);
	strcat(clData,",");
    }
    else
    {
	dbg(TRACE,"%05d: Wrong Airline-code: not MI odr SQ, skipp this record",__LINE__);
	return RC_FAIL;
    }
    
/* Now get Flight-Number */
    strcat(clData,clTemp);
    strcat(clData," ");
    clTemp[0] = '\0';
    strncpy(clTemp,pclData,5);
    pclData += 5;
    clTemp[5] = '\0';
    CskedTrimRight(clTemp);
    strcpy(clFlno,clTemp);
    strcat(pcpFields,"FLNO,");
    strcat(clData,clTemp);
    strcat(clData,",");

/* Now get scheduled time of deparutre (first sector date)*/
/* Now the date*/
		/* Frank v1.9 */
		/*
    clTemp[0] = '\0';
    strncpy(clTemp,pclData,8);
    pclData += 8;
    clTemp[8] = '\0';
    strcpy(clFlda,clTemp);
    strcat(pcpFields,"FLDA,");
    strcat(clData,clTemp);
    */
    clTemp[0] = '\0';
    
 /*Frank v1.9a*/   
		//strncpy(clTemp,pclData,12);
    strncpy(clTemp,pclData,8);
    memset(clFLDAFromWrapCsked,0,sizeof(clFLDAFromWrapCsked));
    strncpy(clFLDAFromWrapCsked,pclData,12);
/*Frank v1.9a*/
    
    pclData += 12;
    clTemp[12] = '\0';
    strcpy(clFlda,clTemp);
    strcat(pcpFields,"FLDA,");
    strcat(clData,clTemp);
    /* Frank v1.9 */
/* Now skipp the time, we dont need it */    
    /*pclData += 4;*/
    strcat(clData,",");

/* Now get number of sectors*/
    clTemp[0] = '\0';
    strncpy(clTemp,pclData,1);
    pclData += 1;
    clTemp[1] = '\0';
    strcpy(clNums,clTemp);
    strcat(pcpFields,"NUMS,");
    strcat(clData,clTemp);
    strcat(clData,",");

/* Now get Internal Aircraft-Type */
    clTemp[0] = '\0';
    strncpy(clTemp,pclData,5);
    pclData += 5;
    clTemp[5] = '\0';
    CskedTrimRight(clTemp);

    /* PR 442 - perform lookup */

    memset (cl_ufis_iata, '\0', 8);
    memset (cl_ufis_icao, '\0', 8); 

    resLU = CskedLookupMapTabAct (clTemp, cl_ufis_iata, cl_ufis_icao);
    if (resLU == 0) /* Found lookup, so use UFIS ACTTAB */
    {
      strcat(pcpFields,"ACT3,");
      strcat(clData,cl_ufis_iata);
      strcat(clData,","); 

      strcat(pcpFields,"ACT5,");
      strcat(clData,cl_ufis_icao);
      strcat(clData,",");

      pclData += 5;     /* point to after CSKED iata */

    }
    else	/* Used CSKED data as before */
		/* This is the original pre-PR442 code */
    {
      strcpy(clActi,clTemp);
      strcat(pcpFields,"ACT5,");
      strcat(clData,clTemp);
      strcat(clData,",");

      /* Now get Aircraft Type */
      clTemp[0] = '\0';
      strncpy(clTemp,pclData,5);
      pclData += 5;
      clTemp[5] = '\0';
      CskedTrimRight(clTemp);
      if(strlen(clTemp) == 3)
      {
  	strcat(pcpFields,"ACT3,");
      }
      else
      {
  	return RC_FAIL;
      }
      strcpy(clAct,clTemp);
      strcat(clData,clTemp);
      strcat(clData,",");
    }
   
    if(strcmp(clUpdateCode,"NFL") == 0)
    {
	strcat(pcpFields,"TTYP,");
	clTemp[0] = '\0';
	strncpy(clTemp,pclData,3);
	pclData += 3;
	clTemp[3] = '\0';
	strcat(clData,clTemp);
	strcat(clData,",");
	
    }

    else
    {
	pclData += 3;
   }
    strcat(clData,"\0");
    strcpy(clData1,clData);
    
    ilNums = atoi(clNums);
    
    strcat(pcpFields,"SECN,ORG3,DES3,STOD,STOA");
    for (i=1 ; i<=ilNums ; i++)
    {
/* Now get Sector-number*/
	clTemp[0] = '\0';
	strncpy(clTemp,pclData,1);
	pclData += 1;
	clTemp[1] = '\0';
	strcat(clData,clTemp);
	strcat(clData,",");
	
/* Now get Org3 */
	clTemp[0] = '\0';
	strncpy(clTemp,pclData,5);
	pclData += 5;
	clTemp[5] = '\0';
	CskedTrimRight(clTemp);
	strcat(clData,clTemp);
	strcat(clData,",");
	
/* Now get Des3 */
	clTemp[0] = '\0';
	strncpy(clTemp,pclData,5);
	pclData += 5;
	clTemp[5] = '\0';
	CskedTrimRight(clTemp);
	strcat(clData,clTemp);
	strcat(clData,",");
	
/* Now get stod */
/*date*/
	clTemp[0] = '\0';
	strncpy(clTemp,pclData,8);
	pclData += 8;
	clTemp[8] = '\0';
	strcat(clData,clTemp);
/*time*/
	clTemp[0] = '\0';
	strncpy(clTemp,pclData,4);
	pclData += 4;
	clTemp[4] = '\0';
	strcat(clData,clTemp);
	strcat(clData,"00");
	strcat(clData,",");
	
/* Now get stoa */
/*date*/
	clTemp[0] = '\0';
	strncpy(clTemp,pclData,8);
	pclData += 8;
	clTemp[8] = '\0';
	strcat(clData,clTemp);
/*time*/
	clTemp[0] = '\0';
	strncpy(clTemp,pclData,4);
	pclData += 4;
	clTemp[4] = '\0';
	strcat(clData,clTemp);
	strcat(clData,"00");
	
	if((ilNums > 1) && (i != ilNums))
	{
	    strcat(clData,"\n");
	    strcat(clData,clData1);
	}

	
    }/*end of for*/

    strcpy(pcpData,clData);
    dbg(TRACE,"%05d: ----- CskedScheduleChange end -----",__LINE__);
 
    return RC_SUCCESS;
}


static int CskedOperationalUpdate(char *pcpFields, char *pcpData)
{
    int ilRC = RC_SUCCESS;
    char *pclData = NULL;
    char *pclFields = NULL;
    /*char clTemp[10] = "\0";*/ /* Frank v1.9*/
    char clTemp[16] = "\0";     /* Frank v1.9*/
    char clData[2002] = "\0";
    char clUpdateCode[10] = "\0";
    char clAlc2[10] = "\0";
    char clFlno[10] = "\0";
    char clFlda[10] = "\0";
    char clSecn[10] = "\0";
    char clActi[10] = "\0";
    char clAct[10] = "\0";
    char clOrg3[10] = "\0";
    char clDes3[10] = "\0";
    char clData1[50] = "\0";
    int ilBlocks = 0;
    int i = 0;
	char clTimeStamp[20] = "\0";

	int ilAddMinCSKEDToETA = 0;
	char clTemp2[20] = "\0";
	static int ilCSKED_COUNTER = 0;

    dbg(TRACE,"%05d: ----- CskedScheduleChange start -----",__LINE__);
    dbg(TRACE,"%05d: pcpFields: <%s>",__LINE__,pcpFields);
    dbg(TRACE,"%05d: pcpData  : <%s>",__LINE__,pcpData);
	ilCSKED_COUNTER++;
    
    pcpFields[0] = '\0';
    pclData = pcpData;
    ilBlocks = (strlen(pclData) - 34) / 14;
    dbg(TRACE,"%05d: Number of Movementblocks: <%d>",__LINE__,ilBlocks);

    pclData += 3;   /*Update-Code ueberspringen*/

/* Now get ALC2/3 */
    strncpy(clTemp,pclData,3);
    pclData += 3;
    clTemp[3] = '\0';
    CskedTrimRight(clTemp);
    if((strcmp(clTemp,"SQ") == 0) || (strcmp(clTemp,"MI") == 0))
    {
	strcpy(clAlc2,clTemp);
	strcat(pcpFields,"ALC2,");
	strcat(clData,clTemp);
	strcat(clData,",");
    }
    else
    {
	dbg(TRACE,"%05d: Wrong Airline-code: not MQ odr SI, skipp this record",__LINE__);
	return RC_FAIL;
    }
    
/* Now get Flight-Number */
    strcat(clData,clTemp);
    strcat(clData," ");
    clTemp[0] = '\0';
    strncpy(clTemp,pclData,5);
    pclData += 5;
    clTemp[5] = '\0';
    CskedTrimRight(clTemp);
    strcpy(clFlno,clTemp);
    strcat(pcpFields,"FLNO,");
    strcat(clData,clTemp);
    strcat(clData,",");

/* Now get scheduled time of deparutre (first sector date)*/
/* Now the date*/
		/* Frank v1.9*/
		/*
    clTemp[0] = '\0';
    strncpy(clTemp,pclData,8);
    pclData += 8;
    clTemp[8] = '\0';
    strcpy(clFlda,clTemp);
    strcat(pcpFields,"FLDA,");
    strcat(clData,clTemp);
    */
    clTemp[0] = '\0';
    
/*Frank v1.9a*/   
		//strncpy(clTemp,pclData,12);
    strncpy(clTemp,pclData,8);
    memset(clFLDAFromWrapCsked,0,sizeof(clFLDAFromWrapCsked));
    strncpy(clFLDAFromWrapCsked,pclData,12);
/*Frank v1.9a*/
    
    pclData += 12;
    clTemp[12] = '\0';
    strcpy(clFlda,clTemp);
    strcat(pcpFields,"FLDA,");
    strcat(clData,clTemp);
    /* Frank v1.9*/
/* Now skipp the time, we dont need it */    
    /*pclData += 4;*/ /* Frank v1.9*/
    strcat(clData,",");

/* Now get number of sectors*/
    clTemp[0] = '\0';
    strncpy(clTemp,pclData,1);
    pclData += 1;
    clTemp[1] = '\0';
    strcpy(clSecn,clTemp);
    strcat(pcpFields,"SECN,");
    strcat(clData,clTemp);
    strcat(clData,",");

/* Now get Origin 3LC */
    clTemp[0] = '\0';
    strncpy(clTemp,pclData,3); /* 3LC but field is 5 char*/
    pclData += 5;
    clTemp[3] = '\0';
    strcpy(clOrg3,clTemp);
    strcat(pcpFields,"ORG3,");
    strcat(clData,clTemp);
    strcat(clData,",");
    
/* Now get Destination 3LC*/
    clTemp[0] = '\0';
    strncpy(clTemp,pclData,3); /* 3LC but field is 5 char*/
    pclData += 5;
    clTemp[3] = '\0';
    strcpy(clDes3,clTemp);
    strcat(pcpFields,"DES3");
    strcat(clData,clTemp);


    for(i=1 ; i <= ilBlocks ; i++)
    {
	clTemp[0] = '\0';
	strncpy(clTemp,pclData,2);
	clTemp[2] = '\0';
	pclData += 2;
	
	/* check for ED */
	if(strcmp(clTemp,"EA") != 0)
	{

	    strcat(pcpFields,",");
	    strcat(clData,",");

	    if(strcmp(clTemp,"ED") == 0)
	    {
	    	    strcat(pcpFields,"ETDA");
	    } 
	    else
	    {
		if(strcmp(clTemp,"AD") == 0)
		{
	    	    strcat(pcpFields,"OFBS");
		} 
		else
		{
		    if(strcmp(clTemp,"AA") == 0)
		    {
			strcat(pcpFields,"ONBS");
		    } 
		    else
		    {
			if(strcmp(clTemp,"RD") == 0)
			{
			    strcat(pcpFields,"ETDC");
			} 
			else
			{
			    if(strcmp(clTemp,"RA") == 0)
			    {
				strcat(pcpFields,"ETAC");
			    } 
			    else
			    {
				if(strcmp(clTemp,"AN") == 0)
				{
				    strcat(pcpFields,"AIRA");
				} 
				else
				{
				    if(strcmp(clTemp,"LN") == 0)
				    {
					strcat(pcpFields,"LNDA");
				    } 
				    else
				    {
					dbg(TRACE,"%05d: MVT-Code <%s> does not correspond to ICD, skipp this record",__LINE__,clTemp);
					return RC_FAIL;
				    }
				}
			    }
			}
		    }
		}
	    }

	    
	    clTemp[0] = '\0';
	    strncpy(clTemp,pclData,8);
	    clTemp[8] = '\0';
	    pclData += 8;
	    strcat(clData,clTemp);
	    clTemp[0] = '\0';
	    strncpy(clTemp,pclData,4);
	    clTemp[4] = '\0';
	    pclData += 4;
	    strcat(clData,clTemp);
	    strcat(clData,"00");

	} /*end of "not EA"*/
	else
	{
		if(strcmp(clTemp,"EA") == 0)
		{
			/* get Estimated time of arrival */
		    clTemp[0] = '\0';
		    strncpy(clTemp,pclData,12);
		    clTemp[12] = '\0';
			strcat(clTemp,"00");
			if(ilCSKED_COUNTER == 1)
			{
				ReadConfigDefault("CSKED","ADD_MINUTES_TO_CSKED_ETA",clTemp2,"0");
				ilAddMinCSKEDToETA = atoi(clTemp2);
			}
			dbg(TRACE,"ETAA orig: <%s>",clTemp);
			ilRC = AddSecondsToCEDATime(clTemp, ilAddMinCSKEDToETA * 60 ,1);
			dbg(TRACE,"ETAA plus: <%s>",clTemp);
			strcat(pcpFields,",ETAA");
			strcat(clData,",");
			strcat(clData,clTemp);
		}
	}

    }

    strcpy(pcpData,clData);

    return RC_SUCCESS;
}


static void CskedTrimRight(char *s)
{
    int i = 0;    /* search for last non-space character */
    for (i = strlen(s) - 1; i >= 0 && isspace(s[i]); i--); /* trim off right spaces */
    s[++i] = '\0';
}

/**********************************************************************
 * CskedWrapperInit
 * 20071029 - read imphdl.cfg to see if mapping table for ACTYPE present.
 *
 **********************************************************************
 */
static void CskedWrapperInit (void)
{
    int ilRc;
    char clParm [32];

    if (CskedWrapperInited != 0)
	return;

    ilRc = ReadConfigEntry ("CSKED", "MAP_TABLE", CskedMapTab);
    if (ilRc != RC_SUCCESS)
	sprintf (CskedMapTab, "GDLTAB");	/* default */ 
 
    dbg(TRACE,"CskedWrapperInit: MAP_TABLE [%s]", CskedMapTab);
    dbg(TRACE,"FYI: Key for A/C type in [%s] is ACT", CskedMapTab); 

    Csked_allow_CRG_missing_iata = 0;	/* default */ 
    ilRc = ReadConfigEntry ("CSKED", "ALLOW_CRG_MISSING_IATA", clParm);
    if (ilRc == RC_SUCCESS) 
    {
	if ((!strcmp (clParm, "YES")) || (!strcmp (clParm, "yes"))
	    || (!strcmp (clParm, "Yes")))
	    Csked_allow_CRG_missing_iata = 1;

	if (Csked_allow_CRG_missing_iata == 1)
	    dbg(TRACE,"CskedWrapperInit: ALLOW_CRG_MISSING_IATA [YES]");
	else
	    dbg(TRACE,"CskedWrapperInit: ALLOW_CRG_MISSING_IATA [NO]");
    } 

    CskedWrapperInited = 1;
}

/**********************************************************************
 * CskedLookupMapTabAct
 * Check the mapping table for A/C types for a CSKED modified ICAO to
 * Ufis IATA conversion. If one found, return it in ufis_iata.
 * Success return 0, else return non zero.
 *
 **********************************************************************
 */ 
static int CskedLookupMapTabAct (char *pcl_csked_icao, char *pcl_ufis_iata,
    char *pcl_ufis_icao)
{ 
  int ilRC = RC_DATA_ERR, res, res2;
  short slFkt = 0;
  short slCursor = 0;
  char clSqlBuf[2048] ="\0";	/* probably too large */
  char clDataArea [2048];	/* but who knows.. */
  
  res = 0;			/* return 0 on success */ 

  sprintf(clSqlBuf,
    "SELECT RVAL FROM %s WHERE LKEY='ACT3' AND TYPE='CSK' AND LVAL='%s'", 
	CskedMapTab, pcl_csked_icao);

  dbg(TRACE,"CskedLookupMapTabAct: clSqlBuf: <%s>",clSqlBuf); 
  slFkt = START;
  slCursor = 0;
  /* ilRC = sql_if (slFkt, &slCursor, clSqlBuf, pcl_ufis_iata);  */
  ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clDataArea); 
  if (ilRC == RC_SUCCESS)
  { 
    BuildItemBuffer (clDataArea, "RVAL", 1, ",");
    get_real_item (pcl_ufis_iata, clDataArea, 1); 
    dbg(TRACE,"CskedLookupMapTabAct: CSKED ICAO [%s] -> Ufis IATA [%s]",
	pcl_csked_icao, pcl_ufis_iata); 
  }
  else if (ilRC == RC_NOTFOUND)
  {
      dbg(TRACE,"CskedLookupMapTabAct: No Ufis IATA for CSKED ICAO [%s]",
	pcl_csked_icao);
      res = 1; 
  }
  else 
  {
      dbg(TRACE,"CskedLookupMapTabAct: Lookup failed <%d> #############",ilRC);
      res = 2; 
  }

  commit_work();
  close_my_cursor (&slCursor);

  /* Then only get the ICAO from ACTTAB */

  if (res == 0)
  {
    sprintf(clSqlBuf,
	"SELECT ACT5 FROM ACTTAB WHERE ACT3='%s'", pcl_ufis_iata);
    dbg(DEBUG,"CskedLookupMapTabAct: clSqlBuf: <%s>",clSqlBuf); 
    slFkt = START;
    slCursor = 0;
    /* ilRC = sql_if (slFkt, &slCursor, clSqlBuf, pcl_ufis_icao);  */
    ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clDataArea); 

    if (ilRC == RC_SUCCESS)
    {
      BuildItemBuffer (clDataArea, "ACT5", 1, ",");
      get_real_item (pcl_ufis_icao, clDataArea, 1); 
      dbg(TRACE,"CskedLookupMapTabAct: Ufis ICAO [%s]", pcl_ufis_icao); 
    }
    else if (ilRC == RC_NOTFOUND)
    {
	/* This should never happen */
        dbg(TRACE,"CskedLookupMapTabAct: NO UFIS IATA [%s] in ACTTAB",
	    pcl_ufis_iata); 
	strcpy (pcl_ufis_icao, "     ");	/* Blank it */
    }
    else 
    {
        dbg(TRACE,"CskedLookupMapTabAct: Lookup failed <%d> #############",
	    ilRC); 
    }

    commit_work();
    close_my_cursor (&slCursor);

  }

  return res; 	/* This result is only for the Mapping lookup, not ACTTAB */
}
