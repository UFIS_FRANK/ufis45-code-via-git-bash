#ifndef _DEF_mks_version_wrapTams
  #define _DEF_mks_version_wrapTams
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version_wrapTams[] = "@(#) "UFIS_VERSION" $Id: Ufis/Sin/SIN_Server/Base/Server/Kernel/IMP/wrapTams.src.c 1.55 2006/02/14 16:25:26SGT heb Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program                                                         */
/*                                                                            */
/* Author         : JIM                                                       */
/* Date           : 11. Feb. 2003                                             */
/* Description    : TAMS to UFIS Inteface Include file                        */
/*                  This Include file reformats the TAMS updates received by  */
/*                  IMPHDL via an CEDA MQ Interface and handles requests from */
/*                  TAMS                                                      */
/*                                                                            */
/* Update history :                                                           */
/* 20030211 JIM: init                                                         */
/* 20030311 JIM: restructured as include file for IMPMAN (or IMPHDL...)       */
/* 20030218 JIM: modified some "dbg(TRACE,.." to "dbg(DEBUG,.."               */
/* 20030522 JIM: Racetrack / RTK for departure ignored (bag. belt behind CC)  */
/* 20030522 JIM: Ignore field lists in configuration                          */
/* 20030522 JIM: REM allowed for SQ/MI                                        */
/* 20030522 JIM: TAMS field name followed by '^' is ignored, SPACE resets UFIS*/
/* 20030523 JIM: SPACE resets UFIS value: also do not append seconds          */
/* 20030603 HEB: Changed  HandleCCARequest to send command REC to WMQCDI      */
/* 20030606 HEB: RTK,LKS,LKF added for processing                             */
/* 20030612 JIM: mapped TEP to TGA1 instead of TGA2, TGD1 instead of TGD2     */
/*               added CheckRemForCancel                                      */
/* 20030618 JIM: using strtok_r instead of strtok to avoid core               */
/*               no strcpy to sgTamsToAft[].AftValue                          */
/* 20030623 JIM: removed all strtok* to avoid core                            */
/* 20030623 JIM: using copy of sgDefTamsToAftSpec[..].Rule to write on (core!)*/
/* 20030624 JIM: CheckRemForCancel: ignoring ^REMCN^ for SQ/MI                */
/* 20030711 JIM: Updates fuer Codeshare mit leerem MFF (Master) abgelehnt     */
/* 20030711 JIM: Flugnummer ohne Ziffern abgelehnt                            */
/* 20030711 JIM: korrekte Fehlermeldung, wenn schedule data fuer SQ/MI        */
/* 20030715 JIM: added configuration if empty MFF shall result in error       */
/* 20030812 JIM: use TAMS Gate 'GAT' for POS if TAMS Position 'POS' is empty  */
/* 20030822 JIM: suffix of flno has to be on position 8 of string             */
/* 20031001 JIM: avoid update of other info than COSH for Master of code share*/
/* 20031001 JIM: simplified code for update on date part of ETA/ETD,avoid core*/
/* 20031107 JIM: ignore configured fields known for bad data                  */
/* 20031107 JIM: allow TEP for SQ/MI airlines                                 */
/* 20031113 JIM: ignore configured data types (i.e. 3U,4U known for bad data  */
/* 20040210 JIM: LC1 and LC2 mapped to FCA1,FCA2 (AF1TAB), both to FCAL (AFTTAB)*/
/* 20040216 JIM: accept FCAL, FCA1 and FCA2 even if date part (LD1/2) missing */
/* 20040407 JIM: PRF 6009: removed wrong check of sgTamsToAft[1].AftValue     */
/******************************************************************************/

char *pcgFreeForm;            /* some global pointers nearly every time in use */
char *pcgFreeFormCurr;
char *pcgFreeFormNext;
int   igFixMax= 0;            /* number of known fix format fields */
static char  pcgSeperator = '^'; /* seperator in TAMS record  */
char  pcgAdid[]= "A";         /* Arrival/Departure flag: A/D or B (both) */
int   igNumTamsToAft = 0;
int   igNumTamsToAftSpec = 0;
char *pcgTamsOutF;      /* global pointer to the pclFldOut */
char *pcgTamsOutD;      /* global pointer to the pclDatOut */
char *pcgTamsSel;       /* global pointer to the pclSelOut */
char *pcgTamsCmd;       /* global pointer to the pclCmdOut */
char *pcgTamsDat;       /* global pointer to the pclCmdOut */

char pcgTAMSAftFields[1024]; /* global array for field list */
char pcgTamsAftValues[1024]; /* global array for data  list */
char pcgAccepted[1024];
char pcgSkipped[1024];
char pcgUnknown[1024];
static char pcgNatureIn[128] = "";
static char pcgNatureOut[128] = "";
char pcgDataTyp[8];
char pcgIgnoreDataTypes[128] = "";

char pcgFlno[16];
char pcgJfno[16];
char pcgJfln[16];
char *pcgAct3;
char *pcgAct5;
int  igJfnoNdx;              /* global index for JFNO */
int  igJflnNdx;
int  igAct3Ndx;
int  igAct5Ndx;
int  igTtypNdx;
int  igRempNdx;
int  igPstaNdx;
int  igPstdNdx;
int  igGta1Ndx;
int  igGtd1Ndx;
int  igEtoADNdx;
int  igEtoa2Ndx;
int  igEtod2Ndx;
int  igFcal1Ndx;/* 20040210 JIM */
int  igFcal2Ndx;/* 20040210 JIM */
int  igFclA1Ndx;/* 20040210 JIM */
int  igFclA2Ndx;/* 20040210 JIM */
int  igFclB1Ndx;/* 20040210 JIM */
int  igFclB2Ndx;/* 20040210 JIM */

int  igAllowEmptyMFF= 0;  /* empty MFF will result in error */
int  igCoshEvaluateOther= 0; /* 20030930: if info is for Code share, do not evaluate other data (i.e. DEST) */

char cgAdhoc[10] = "\0";

/* by now we only know two rules: */
#define CONCAT (1)
#define CONCAT_SEP (2)

typedef struct _TamsToAftFix {/* structure for fixed field definitions */
   char Name[16];             /* UFIS name of field */
   char Value[16];            /* string copy of value (filled in GetFixField) */
   int Start;                 /* fixed value start in record */
   int Len;                   /* fixed value length in record */
} TAMS_TO_AFT_FIX;

/* the following array is used in "GetFixField" to fill ".Value" */
static TAMS_TO_AFT_FIX pcgFixFields[] = { /* array of fixed field definitions */
  {"FLNO",""  ,2,10 }, /* FLNO begins at pos.  2, length 10, default "" */
  {"FLDA",""  ,12,8 }, /* FLDA begins at pos. 12, length  8, default "" */
  {"ADID",""  ,0,1 },  /* ADID begins at pos. 0, length 1, (in pcgAdid !) */
  {"VPFR",""  ,12,8 }, /* like FLDA, but used with other command! */
  {"VPTO",""  ,20,8 },
  {"FREQ",""  ,28,7 },
  {"CKBS",""  , 2,14},
  {"CKES",""  ,16,14}
};

typedef struct _TamsToAft {/* structure for translation table */
   char *Tams;             /* Field name in TAMS */
   char *AftArr;           /* arrival field name in TAMS */
   char *AftDep;           /* departure field name in TAMS */
   char *AftValue;         /* pointer to the TAMS value in EventData */
   int  AddSeconds;        /* flag for HHMM fields to add seconds in data list */
} TAMS_TO_AFT;

/* the following array is used in "GetTamsToAft" to set the pointer ".Value" */
/* fileds marked with beginning "*" are symbolic fields, which are used to   */
/* combine one ore more TAMS values to one UFIS value */
static TAMS_TO_AFT sgTamsToAft[] = { /* array for translation table */
  {"ADT","*LAND1","*AIRB1", NULL, 0 }, /* Actual Date of Arrival/Departure (touchdown/airborne) */
  {"ATA","*LAND2"," "     , NULL, 1 }, /* Actual Time of Arrival (touchdown) */
  {"ATD"," "     ,"*AIRB2", NULL, 1 }, /* Actual Time of Departure (airborne) */
  {"BB1","BLT1"  ," "     , NULL, 0 }, /* First Belt for arrival (arrival updates only) */
  {"BB2","BLT2"  ," "     , NULL, 0 }, /* Second Belt for arrival (arrival updates only) */
  {"CFB"," "     ," "     , NULL, 0 }, /* Configured Seats buissiness (ignore) */
  {"CFE"," "     ," "     , NULL, 0 }, /* Configured Seats economy (ignore) */
  {"CFI"," "     ," "     , NULL, 0 }, /* Configured Seats first (ignore) */
  {"CFP"," "     ," "     , NULL, 0 }, /* ignore */
  {"DES","DES3"  ,"DES3"  , NULL, 0 }, /* IATA code of destination airport. */
  {"EDT","*ETOA1","*ETOD1", NULL, 0 }, /* Estimated Date of Arrival/Departure . */
  {"ETA","*ETOA2"," "     , NULL, 1 }, /* Estimated Time of Arrival. */
  {"ETD"," "     ,"*ETOD2", NULL, 1 }, /* Estimated Time of Departure. */
  {"GAT","GTA1"  ,"GTD1"  , NULL, 0 }, /* Gate id of Arrival */
  {"GBD"," "     ,"*BOAO1", NULL, 0 }, /* Gate boarding time */
  {"GBT"," "     ,"*BOAO2", NULL, 1 }, /* Gate boarding time */
  {"GCD"," "     ,"*GD1Y1", NULL, 0 }, /* Gate close date ? schedule ? (maybe actual GD1B) */
  {"GCT"," "     ,"*GD1Y2", NULL, 1 }, /* Gate close time ? schedule ? (maybe actual GD1B) */
  {"GOD"," "     ,"*GD1X1", NULL, 0 }, /* Gate open time ? schedule ? (maybe actual GD1A) */
  {"GOT"," "     ,"*GD1X2", NULL, 1 }, /* Gate open time ? schedule ? (maybe actual GD1A) */
  {"lcx"," "     ,"*FCAL1", NULL, 0 }, /* Gate last call 1 date, AFTTAB.FCAL */
  {"ldx"," "     ,"*FCAL2", NULL, 1 }, /* Gate last call 1 time, AFTTAB.FCAL */
  {"LD1"," "     ,"*FCLA1", NULL, 0 }, /* Gate last call 1 date, AF1TAB.FCA1 */
  {"LC1"," "     ,"*FCLA2", NULL, 1 }, /* Gate last call 1 time, AF1TAB.FCA1 */
  {"LD2"," "     ,"*FCLB1", NULL, 0 }, /* Gate last call 2 date, AF1TAB.FCA2 */
  {"LC2"," "     ,"*FCLB2", NULL, 1 }, /* Gate last call 2 time, AF1TAB.FCA2 */
  {"LKF","JFLN"  ,"JFLN"  , NULL, 0 }, /* Flight Identifier of linked departure flight */
  {"LKS","JDAY"  ,"JDAY"  , NULL, 0 }, /* Scheduled Date (SDT) of linked departure flight. */
  {"MFF","COSH"  ,"COSH"  , NULL, 0 }, /* Master code for share flights */
  {"NAT","TTYP"  ,"TTYP"  , NULL, 0 }, /* The flight nature code (passenger/cargo) */
  {"ORG","ORG3"  ,"ORG3"  , NULL, 0 }, /* IATA code of origin airport. */
  {"PAR","PSTA"  ,"PSTD"  , NULL, 0 }, /* Stand id of Arrival Flight */
  {"REG","REGN"  ,"REGN"  , NULL, 0 }, /* Aircraft Registration Number */
  {"REM","REMP"  ,"REMP"  , NULL, 0 }, /* Current Remarks Code */
  {"RWY","RWYA"  ,"RWYD"  , NULL, 0 }, /* Runway of arrival */
  {"RTK","BAZ1"  ,"BAZ1"  , NULL, 0 }, /* Baggage carussel */
  {"SST"," "     ," "     , NULL, 0 }, /* Code Share Status of this flight 00-99=Master, SL= Slave */
  {"STA","*STOA2"," "     , NULL, 1 }, /* Scheduled Time of Arrival. */
  {"STD"," "     ,"*STOD2", NULL, 1 }, /* Scheduled Time of Arrival. */
  {"TEP","TGA1"  ,"TGD1"  , NULL, 0 }, /* This is the current terminal for flight. */
  {"TYF","ACT5"  ,"ACT5"  , NULL, 0 }, /* IATA code for full aircraft type. (eg. 747-400) */
  {"TYP","ACT3"  ,"ACT3"  , NULL, 0 }, /* aircraft type like that passed by FCS to CGMS. (eg. 747) */
  {"VI1","*VIA1" ,"*VIA1" , NULL, 0 }, /* IATA code of 1st airport stopped at en route. */
  {"VI2","*VIA2" ,"*VIA2" , NULL, 0 }, /* IATA code of 2nd airport stopped at en route. */
  {"VI3","*VIA3" ,"*VIA3" , NULL, 0 }, /* IATA code of 3rd airport stopped at en route. */
  {"VI4","*VIA4" ,"*VIA4" , NULL, 0 }, /* IATA code of 4th airport stopped at en route. */
  {"VI5","*VIA5" ,"*VIA5" , NULL, 0 }, /* IATA code of 5st airport stopped at en route. */
  {"VI6","*VIA6" ,"*VIA6" , NULL, 0 }  /* IATA code of 6xt airport stopped at en route. */
};

typedef struct _DefTamsToAftSpec {/* structure for special translation table */
   char *Trigger;                 /* symbolic UFIS name on which to trigger */
   char *Target;                  /* translate to UFIS field name */
   char *Rule;                    /* rule, how to build UFIS value */
} DEF_TAMS_TO_AFT_SPEC;

/* the following array is used to combine TAMS values to one UFIS value */
static DEF_TAMS_TO_AFT_SPEC sgDefTamsToAftSpec[] = {/* array for spec. transl.*/
  {"*AIRB2","AIRB","|,*AIRB1,8,*AIRB2,4"},  /* Actual Time of Dparture (airborne) */
  {"*LAND2","LAND","|,*LAND1,8,*LAND2,4"},  /* Actual Time of Arrival (touchdown) */
  {"*ETOA2","ETOA","|,*ETOA1,8,*ETOA2,4"},  /* Estimated Time of Arrival. */
  {"*GD1Y2","GD1Y","|,*GD1Y1,8,*GD1Y2,4"},  /* Gate close date */
  {"*GD1X2","GD1X","|,*GD1X1,8,*GD1X2,4"},  /* Gate open date */
  {"*BOAO2","BOAO","|,*BOAO1,8,*BOAO2,4"},  /* Boarding time */
  {"*FCAL2","FCAL","|,*FCAL1,8,*FCAL2,4"},  /* Gate last call, AFTTAB.FCAL */
  {"*FCLA2","FCA1","|,*FCLA1,8,*FCLA2,4"},  /* Gate last call, AF1TAB.FCA1 */
  {"*FCLB2","FCA2","|,*FCLB1,8,*FCLB2,4"},  /* Gate last call, AF1TAB.FCA2 */
  {"*ETOD2","ETOD","|,*ETOD1,8,*ETOD2,4"},  /* Estimated Time of Arrival. */
  {"*STOA2","STOA","|,FLDA,8,*STOA2,4"},  /* Scheduled Time of Arrival. */
  {"*STOD2","STOD","|,FLDA,8,*STOD2,4"},  /* Scheduled Time of Arrival. */
  {"*VIA?" ,"ROUT",";,*VIA1,5,*VIA2,5,*VIA3,5,*VIA4,5,*VIA5,5,*VIA6,5"} /* List of VIAs */
};

typedef struct _WrkTamsToAftSpec {/* structure for indexes of special translation table */
   int Length[10]; /* length of max. 10 symbolic fields, i.e. *LAND2: [8,4,0,0,..]  */
   int Index[10];  /* indexes of max. 10 symbolic fields in array "sgTamsToAft" */
   int Rule;       /* all values of this */
   int iTrigger;   /* index of symbolic trigger field in array "sgTamsToAft" */
} WRK_TAMS_TO_AFT_SPEC;
WRK_TAMS_TO_AFT_SPEC *pcgWrkTamsToAftSpec;

/* depending on the TAMS record following fields are valid: */
/* 20030522 JIM: char pcgIgnOnline[] = ",CFB,CFE,CFI,CFP,LKF,LKS,SST,"; ignore RTK */
char pcgIgnOnline[256];
char pcgIgnSched[256];
char pcgIgnoreSQMI[256];
/* JIM 20031107: ignore configured fields known for bad data: */
char pcgEverIgnOnline[256];
char pcgEverIgnSched[256];
char pcgArrOnline[]=",ADT,ATA,BB1,BB2,EDT,ETA,GAT,MFF,NAT,ORG,PAR,REG,REM,RWY,STA,TEP,TYF,TYP,VI1,VI2,VI3,VI4,VI5,VI6,LKF,LKS,";
char pcgDepOnline[]=",ADT,ATD,DES,EDT,ETD,GAT,GBD,GBT,GCD,GCT,GOD,GOT,LC1,LD1,LC2,LD2,MFF,NAT,PAR,REG,REM,RWY,STD,TEP,TYF,TYP,VI1,VI2,VI3,VI4,VI5,VI6,RTK,LKF,LKS,";
char pcgArrSched[]=",MFF,NAT,ORG,STA,TYF,TYP,VI1,VI2,VI3,VI4,VI5,VI6,";
char pcgDepSched[]=",DES,MFF,NAT,STD,TYF,TYP,VI1,VI2,VI3,VI4,VI5,VI6,";
/* 20030522 JIM:  allow REM */
/* char pcgArrOnlSQMI[]=",ETA,EDT,GAT,PAR,BB1,BB2,RWY,"; */
/* char pcgDepOnlSQMI[]=",GAT,PAR,RWY,GBT,GCT,GOT,LC1,LC2,"; */
/* 20031107 JIM: added TEP for SQ/MI airlines: */
char pcgArrOnlSQMI[]=",ETA,EDT,GAT,PAR,BB1,BB2,REM,RWY,MFF,TEP,";
char pcgDepOnlSQMI[]=",GAT,PAR,RWY,GBT,GCT,GOT,LC1,LC2,REM,GOD,GCD,GBD,LD1,LD2,RTK,MFF,TEP,";
char pcgIgnoreAll[]="^";
/* the following pointer will be set to one of the field list above: */
char *pcgValidFields;
char *pcgIgnore;
/* JIM 20031107: ignore configured fields known for bad data: */
char *pcgIgnoreEver;

char pcgSpecAirlines[] = ",SQ,MI,"; /* restricted field for online data, no scheduled data */
int igToTams = 0;
int igToTamsPrio = 0;
char sgXXXX[] = "xxxx";  /* to handle update on date for ETOA/ETOD */


/******************************************************************************/
/* Routine: ReadConfigDefault                                                 */
/*          read configuration, use default if keyword not found              */
/******************************************************************************/
static void ReadConfigDefault(char *pcpSection,char *pcpKeyword, 
                             char *pcpCfgBuffer, char *pcpDefault)
{
 int ilRc = RC_SUCCESS;

 char clSection[124] = "\0";
 char clKeyword[124] = "\0";

   strcpy(clSection,pcpSection);
   strcpy(clKeyword,pcpKeyword);

   ilRc = iGetConfigEntry(cgConfigFile,clSection,clKeyword,
	      CFG_STRING,pcpCfgBuffer);
   if(ilRc != RC_SUCCESS)
   {
     strcpy(pcpCfgBuffer, pcpDefault);
     dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
     dbg(TRACE,"using default <%s> for <%s>",pcpCfgBuffer,clKeyword);
   } 
   else
   {
      dbg(DEBUG,"Config Entry <%s>,<%s>:<%s> found in %s",
          clSection, clKeyword ,pcpCfgBuffer, cgConfigFile);
   }/* end of if */
}

/******************************************************************************/
/* Routine: Init_Tams
   first initialization (a second initialization is done on first queue 
   event (for debugging)) 
*/
/******************************************************************************/
void Init_Tams(int ipPrioToTams, int ipQueueToTams)
{ 
    dbg(TRACE,"%s",mks_version_wrapTams);
    if (ipPrioToTams>0)
    {
       igToTamsPrio = ipPrioToTams;
    }
    else
    {
       igToTamsPrio = 4;
    }
    if (ipQueueToTams<0)
    {
      igToTams = tool_get_q_id ("totams");
    }
    else
    {
      igToTams = ipQueueToTams;
    }

    if (igToTams == 0)
    {
       dbg(TRACE,"Init_Tams: queue to 'totams' equal 0: will not process RQ's!");
    }
    igNumTamsToAft= sizeof(sgTamsToAft) / sizeof(TAMS_TO_AFT);
    igFixMax = sizeof(pcgFixFields) / sizeof(TAMS_TO_AFT_FIX);
    dbg(TRACE,"Init_Tams: number of TAMS fields: <%d>",igNumTamsToAft);
    ReadConfigDefault("TAMS","NATURE-IN",pcgNatureIn,"");
    ReadConfigDefault("TAMS","NATURE-OUT",pcgNatureOut,"");
    ReadConfigDefault("TAMS","IGNORE_ONLINE",
                      pcgIgnOnline,",CFB,CFE,CFI,CFP,LKF,LKS,RTK,SST,");
    ReadConfigDefault("TAMS","IGNORE_SCHED",
                      pcgIgnSched,",CFB,CFE,CFI,CFP,LKF,LKS,");
    ReadConfigDefault("TAMS","IGNORE_SQMI",
                      pcgIgnoreSQMI,",CFB,CFE,CFI,CFP,LKF,LKS,RTK,SST,");
    /* JIM 20031107: ignore configured fields known for bad data: */
    ReadConfigDefault("TAMS","IGNORE_EVER_ONLINE",pcgEverIgnOnline,"");
    ReadConfigDefault("TAMS","IGNORE_EVER_SCHED",pcgEverIgnSched,"");
    /* JIM 20031107: ignore configured data types known for bad data (i.e.3U):*/
    ReadConfigDefault("TAMS","IGNORE_DATA_TYPE",pcgIgnoreDataTypes,"");
    ReadConfigDefault("TAMS","SEND_ADHOC_DATA_TO",cgAdhoc,"");
    ReadConfigDefault("TAMS","ALLOW_EMPTY_MFF",pcgTAMSAftFields,"NO");
    if (strcmp(pcgTAMSAftFields,"YES")==0)
    {
       igAllowEmptyMFF= 1; /* empty MFF will be ignored */
    }
    ReadConfigDefault("TAMS","COSH_EVALUATE_OTHER",
                      pcgTAMSAftFields,"NO");
    if (strcmp(pcgTAMSAftFields,"YES")==0)
    {
       igCoshEvaluateOther= 1; /* 20030930: if info is for Code share, evaluate all data  */
    }
    pcgTAMSAftFields[0] = 0;
}

/******************************************************************************/
/* Routine: LocateSpecField
   lookup routine to find special field names of "sgDefTamsToAftSpec" (in 
   .Trigger or .Rule) and return the index in sgTamsToAft
   This routine is used in InitWrkTamsToAftSpec only and fills the index
   array "_WrkTamsToAftSpec"
*/
/******************************************************************************/
int LocateSpecField(char *pcpSpec)
{
  int ilCurr;
/*dbg(TRACE,"LocateSpecField <%s>",pcpSpec); */
  for (ilCurr= 0; ilCurr < igNumTamsToAft ; ilCurr++)
  {
/*   dbg(TRACE,"LocateSpecField <%s> <%s>",pcpSpec,sgTamsToAft[ilCurr].AftArr); */
     if (sgTamsToAft[ilCurr].AftArr != NULL)
     if (strcmp(sgTamsToAft[ilCurr].AftArr,pcpSpec) == 0)
     {
        return ilCurr;
     }
/*   dbg(TRACE,"LocateSpecField <%s> <%s>",pcpSpec,sgTamsToAft[ilCurr].AftDep); */
     if (sgTamsToAft[ilCurr].AftDep != NULL)
     if (strcmp(sgTamsToAft[ilCurr].AftDep,pcpSpec) == 0)
     {
        return ilCurr;
     }
  }
  return -1;
}

/******************************************************************************/
/* Routine: InitWrkTamsToAftSpec
   second initialization (is done on first queue event (for debugging)) 
   This routine uses InitWrkTamsToAftSpec to fills the index array 
   "_WrkTamsToAftSpec"
*/
/******************************************************************************/
void InitWrkTamsToAftSpec (void)
{
 int  ilRC= RC_SUCCESS;
 char *pclSpec;
 char *pclLen;
 char *pclRule;
 char *pclInRule;
 char pclRuleCpy[120];
 int ilCurr; /* index in defined rules */
 int ilUsed; /* index in valid Wrk rules (equal to ilCurr, if all rules valid)*/
 int ilFld;
 char *pclDummy;

  dbg(DEBUG,"InitWrkTamsToAftSpec: START");
  igNumTamsToAftSpec= sizeof(sgDefTamsToAftSpec) / sizeof(DEF_TAMS_TO_AFT_SPEC);
  dbg(DEBUG,"InitWrkTamsToAftSpec: igNumTamsToAftSpec <%d>",igNumTamsToAftSpec);
  pcgWrkTamsToAftSpec= (WRK_TAMS_TO_AFT_SPEC*) malloc(igNumTamsToAftSpec*sizeof(WRK_TAMS_TO_AFT_SPEC));
  ilUsed= 0;
  for (ilCurr=0 ; ilCurr < igNumTamsToAftSpec; ilCurr++)
  {
     ilRC= RC_SUCCESS;
     dbg(DEBUG,"InitWrkTamsToAftSpec: build rule No: <%d>",ilCurr); 
     dbg(DEBUG,"InitWrkTamsToAftSpec: build rule   : <%s>",sgDefTamsToAftSpec[ilCurr].Trigger); 
     dbg(DEBUG,"InitWrkTamsToAftSpec: build rule   : <%s>",sgDefTamsToAftSpec[ilCurr].Target); 
     dbg(DEBUG,"InitWrkTamsToAftSpec: build rule   : <%s>",sgDefTamsToAftSpec[ilCurr].Rule); 
     strcpy(pclRuleCpy,sgDefTamsToAftSpec[ilCurr].Rule);
     pclRule = pclRuleCpy;
     dbg(DEBUG,"InitWrkTamsToAftSpec: pclDummy   : <%s>",pclRule); 
     pclInRule = strchr(pclRule,',');
     *(pclInRule++) = 0; 
     dbg(DEBUG,"InitWrkTamsToAftSpec: pclRule <%s>",pclRule); 
     if (pclRule!=0)
     {
        switch (pclRule[0])
        {
          case '|' : pcgWrkTamsToAftSpec[ilUsed].Rule= CONCAT;
                     break;
          case ';' : pcgWrkTamsToAftSpec[ilUsed].Rule= CONCAT_SEP;
                     break;
          default  : pcgWrkTamsToAftSpec[ilUsed].Rule= 0;
                     break;
        }
     }
     else
     {
        pcgWrkTamsToAftSpec[ilUsed].Rule= 0;
     }
     pcgWrkTamsToAftSpec[ilUsed].iTrigger = LocateSpecField(sgDefTamsToAftSpec[ilCurr].Trigger);

     ilFld=0;
     while ((pclInRule != NULL) && (ilRC==RC_SUCCESS))
     {
/*        dbg(TRACE,"InitWrkTamsToAftSpec: field no <%d>",ilFld);    */
        pclSpec= pclInRule;                                       
        pclInRule = strchr(pclSpec,',');                          
        *(pclInRule++) = 0;                                          
/*        dbg(TRACE,"InitWrkTamsToAftSpec: pclSpec <%s>",pclSpec);   */
        pclLen = pclInRule;                                       
        pclInRule = strchr(pclLen,',');                           
        if (pclInRule != NULL)                                    
        {                                                          
           *(pclInRule++) = 0;                                       
        }                                                          
/*        dbg(TRACE,"InitWrkTamsToAftSpec: pclLen <%s>",pclLen);     */
        
        if (pclSpec !=NULL)
        {
           pcgWrkTamsToAftSpec[ilUsed].Index[ilFld]  = LocateSpecField(pclSpec);
           if (pcgWrkTamsToAftSpec[ilUsed].Index[ilFld] == -1)
           {
              pcgWrkTamsToAftSpec[ilUsed].Index[ilFld]  = 5000 + LocateFixField(pclSpec);
              if (pcgWrkTamsToAftSpec[ilUsed].Index[ilFld] == -1)
              {
                 ilRC= RC_FAIL;
                 dbg(TRACE,"InitWrkTamsToAftSpec: Field not found: <%s>",pclSpec); 
                 dbg(TRACE,"InitWrkTamsToAftSpec: please correct and rebuild",pclSpec); 
                 dbg(TRACE,"InitWrkTamsToAftSpec: deleting rule for <%s>",
                            sgDefTamsToAftSpec[ilCurr].Rule); 
              }   
           }
           pcgWrkTamsToAftSpec[ilUsed].Length[ilFld] = atoi(pclLen);
           ilFld++;
        }
        pclDummy= NULL;
     }
     while (ilFld < 10)
     {
        pcgWrkTamsToAftSpec[ilUsed].Index[ilFld]= -1;
        ilFld++;
     }
     if (ilRC == RC_FAIL)
     {
       ilUsed--;
     }
     ilUsed++;
  }
  igNumTamsToAftSpec= ilUsed;
  dbg(TRACE,"InitWrkTamsToAftSpec: igNumTamsToAftSpec used <%d>",igNumTamsToAftSpec);
}

/******************************************************************************/
/* The SkipBlanksLeftRight routine                                            */
/* This routine set the first trailing blank in pcpIn to '\0' and returns the */
/* non blank beginnig of the Strings                                          */
/* Very quick, but it modifies pcpIn and returns a pointer within pcpIn !     */
/******************************************************************************/
char *SkipBlanksLeftRight(char *pcpIn)
{
   int ilLen=0;
   char *pclDummy= pcpIn;

   /* dbg(TRACE,"SkipBlanksLeftRight: <%s>",pclDummy); */
   if (pclDummy != NULL)
   {
      /* set pointer to first non blank char: */
      for ( ;  (pclDummy[0] != 0 ) && (pclDummy[0] == ' ') ; pclDummy++ ) 
      {
         ;  
      }
      
      /* pointer now is first non blank char, may be \0 : */
      if ( pclDummy[0] != 0 )
      {
         /* not \0 , so ilLen must get now >= 0 : */
         ilLen=strlen(pclDummy)-1;
         for ( ; (ilLen > 0) && (pclDummy[ilLen] == ' ') ; ilLen-- )
         {
            ;  
         }
         pclDummy[ilLen+1]=0;
      }
   }
   /* dbg(TRACE,"SkipBlanksLeftRight: <%s>",pclDummy); */
   return pclDummy;
}

/******************************************************************************/
/* The SkipBlanksExceptOne routine                                            */
/* This routine set the first trailing blank in pcpIn to '\0' and returns the */
/* non blank beginnig of the Strings                                          */
/* Very quick, but it modifies pcpIn and returns a pointer within pcpIn !     */
/******************************************************************************/
char *SkipBlanksExceptOne(char *pcpIn)
{
   int ilLen=0;
   int ilLenOld;
   char *pclDummy= pcpIn;

   /* dbg(TRACE,"SkipBlanksLeftRight: <%s>",pclDummy); */
   if (pclDummy != NULL)
   {
      /* set pointer to first non blank char: */
      for ( ;  (pclDummy[0] != 0 ) && (pclDummy[0] == ' ') ; pclDummy++ ) 
      {
         ;  
      }
      
      /* pointer now is first non blank char, may be \0 : */
      if ( pclDummy[0] != 0 )
      {
         /* not \0 , so ilLen must get now >= 0 : */
         ilLen=strlen(pclDummy)-1;
         ilLenOld= ilLen;
         for ( ; (ilLen > 0) && (pclDummy[ilLen] == ' ') ; ilLen-- )
         {
            ;  
         }
         if ((ilLen==2) && /* 3 Chars: only TAMS field name without value given */
             (ilLenOld>2)) /*          but value was at least one space */
         {
            ilLen++;  /* use one space */
         }
         pclDummy[ilLen+1]=0;
      }
   }
   /* dbg(TRACE,"SkipBlanksLeftRight: <%s>",pclDummy); */
   return pclDummy;
}

/******************************************************************************/
/* The GetTamsToAft routine                                                   */
/* This routine locates the TAMS field name int the array sgTamsToAft and sets*/
/* the pointer "sgTamsToAft.Value" to the beginning of the TAMS value.        */
/* This routine is called from "HandleFreeForm" to set all pointers           */
/* "sgTamsToAft.Value"                                                        */
/******************************************************************************/
char *GetTamsToAft(char *pcTams)
{
  int ilCurr;
  for (ilCurr= 0; ilCurr < igNumTamsToAft ; ilCurr++)
  {
     if (strncmp(pcgFreeFormCurr,sgTamsToAft[ilCurr].Tams,3) == 0)
     {
        if (pcgAdid[0]=='A')
        {
           if (sgTamsToAft[ilCurr].AftArr != NULL) 
           {
             if (sgTamsToAft[ilCurr].AftArr[0] != ' ') 
             {
                sgTamsToAft[ilCurr].AftValue = &pcgFreeFormCurr[3];
                dbg(DEBUG,"GetTamsToAft: <%s> ==> <%s> (%s)",pcgFreeFormCurr,
                                                  sgTamsToAft[ilCurr].AftArr,
                                               sgTamsToAft[ilCurr].AftValue);
             }
             return sgTamsToAft[ilCurr].AftArr;
           }
        }
        else
        {
           if (sgTamsToAft[ilCurr].AftDep != NULL) 
           {
             if (sgTamsToAft[ilCurr].AftDep[0] != ' ') 
             {
                sgTamsToAft[ilCurr].AftValue = &pcgFreeFormCurr[3];
                dbg(DEBUG,"GetTamsToAft: <%s> ==> <%s> (%s)",pcgFreeFormCurr,
                                                  sgTamsToAft[ilCurr].AftDep,
                                               sgTamsToAft[ilCurr].AftValue);
             }
             return sgTamsToAft[ilCurr].AftDep;
           }
        }
     }
  }
  return NULL;
}

/******************************************************************************/
/* The EvalFreeSimpleAftValues routine                                        */
/* This routine collects all simple, free format UFIS fields and values set in*/
/* the array sgTamsToAft, which where set while processing the current queue  */
/* message                                                                    */
/******************************************************************************/
void *EvalFreeSimpleAftValues(void)
{
  int ilCurr;
  char* pclTmp;
  for (ilCurr= 0; ilCurr < igNumTamsToAft ; ilCurr++)
  {
     /* dbg(TRACE,"EvalFreeSimpleAftValues: <%d>",ilCurr); */
     if (sgTamsToAft[ilCurr].AftValue != NULL)
     {
        /* dbg(TRACE,"EvalFreeSimpleAftValues: <%d>",ilCurr); */
        if (pcgAdid[0]=='A')
        {
           /* dbg(TRACE,"EvalFreeSimpleAftValues: Arr"); */
           pclTmp=sgTamsToAft[ilCurr].AftArr;
        }
        else
        {
           /* dbg(TRACE,"EvalFreeSimpleAftValues: Dep"); */
           pclTmp=sgTamsToAft[ilCurr].AftDep;
        }
        if (pclTmp[0] != '*')
        {
           /* dbg(TRACE,"EvalFreeSimpleAftValues: Val"); */
           if (sgTamsToAft[ilCurr].AftValue[0] != 0)
           { /* no empty string: */
              strcat(pcgTAMSAftFields,pclTmp);
              strcat(pcgTAMSAftFields,",");
              strcat(pcgTamsAftValues,sgTamsToAft[ilCurr].AftValue);
              if ((sgTamsToAft[ilCurr].AddSeconds == 1) &&
                  (sgTamsToAft[ilCurr].AftValue[0] != ' ') )
              {
                 strcat(pcgTamsAftValues,"00");
              }
              strcat(pcgTamsAftValues,",");
           }
           else
           { 
              dbg(DEBUG,"EvalFreeSimpleAftValues: ignore NULL value..."); 
           }
           /* dbg(TRACE,"EvalFreeSimpleAftValues: done"); */
        }
     }
  }
}

/******************************************************************************/
/* The EvalConcat routine                                                     */
/* This routine concats the values of the fields specified in                 */
/* "sgDefTamsToAftSpec[].Rule" without any delimiter between them             */
/******************************************************************************/
void EvalConcat (int ipCurr)
{
  int ilFld;
  int ilSpecInd;
  int ilFldUsed;
  
       ilFld= 0;
       ilFldUsed= 0;
       while ((ilSpecInd= pcgWrkTamsToAftSpec[ipCurr].Index[ilFld]) >= 0)
       {
          if (ilSpecInd < 5000)
          {
            if (sgTamsToAft[ilSpecInd].AftValue != NULL)
            {
               /*strncat(pcgTamsAftValues,sgTamsToAft[ilSpecInd].AftValue,
                                    pcgWrkTamsToAftSpec[ipCurr].Length[ilFld]);
               */

              if (sgTamsToAft[ilSpecInd].AftValue[0] != 0)
               { 
                  /* no empty string: */
                  if (sgTamsToAft[ilSpecInd].AftValue[0] != ' ')
                  {
                     strcat(pcgTamsAftValues,sgTamsToAft[ilSpecInd].AftValue);
                     if (sgTamsToAft[ilSpecInd].AddSeconds == 1)
                     {
                        strcat(pcgTamsAftValues,"00");
                     }
                  }
                  else if (ilFldUsed == 0)
                  {
                     strcat(pcgTamsAftValues,sgTamsToAft[ilSpecInd].AftValue);
                  }
               }
               ilFldUsed= 1;
            }
          }
          else
          {
             ilSpecInd -= 5000;
             if (pcgFixFields[ilSpecInd].Value[0] != 0)
             {
             /* 20040407 JIM: removed check of sgTamsToAft[ilSpecInd].AftValue */
               if (pcgFixFields[ilSpecInd].Value[0] != ' ') 
               {
                 strcat(pcgTamsAftValues,pcgFixFields[ilSpecInd].Value);
               }
               ilFldUsed= 1;
               /* ilAddBlank= 0;  don't add a blank */
             }
          }
          ilFld++;
       }
       if (ilFldUsed == 1)
       {
          /* if (ilAddBlank == 1)
           * { * all values are empty => add one blank : *
           *   strcat(pcgTamsAftValues," "); 
           * }
          */
	       strcat(pcgTamsAftValues,",");
	       strcat(pcgTAMSAftFields,sgDefTamsToAftSpec[ipCurr].Target);
	       strcat(pcgTAMSAftFields,",");
       }
}

/******************************************************************************/
/* The EvalConcatWithSeperator routine                                        */
/* This routine concats the values of the fields specified in                 */
/* "sgDefTamsToAftSpec[].Rule" with the delimiter "|"                         */
/******************************************************************************/
void EvalConcatWithSeperator (int ipCurr)
{
    int ilFld, i;
    int ilSpecInd;
    int ilFldUsed;
    int bViaExists = FALSE;
    char *pclQuestionmark = "?";

    ilFld= 0;
    ilFldUsed= 0;

    while ((ilSpecInd= pcgWrkTamsToAftSpec[ipCurr].Index[ilFld]) >= 0)
    {
      if (sgTamsToAft[ilSpecInd].AftValue != NULL)
      {
	      if (sgTamsToAft[ilSpecInd].AftValue[0] != 0)
	      {
           bViaExists = TRUE;
        }
      }
	
      ilFld++;
    }
    

    ilFld = 0;

    if(bViaExists == TRUE)
    {
     	while ((ilSpecInd= pcgWrkTamsToAftSpec[ipCurr].Index[ilFld]) >= 0)
	   {
	    if (ilSpecInd < 5000)
	    {
        if (sgTamsToAft[ilSpecInd].AftValue == NULL)
        {
          strcat(pcgTamsAftValues,"?|");
          ilFldUsed = 1;
        }
        if (sgTamsToAft[ilSpecInd].AftValue != NULL)
        {
          if (sgTamsToAft[ilSpecInd].AftValue[0] == 0)
          {
             /* 20030618 JIM: set Pointer instead of copying to pointer: */
             /* strcpy(sgTamsToAft[ilSpecInd].AftValue , "?"); */
             sgTamsToAft[ilSpecInd].AftValue = pclQuestionmark;
          }
          if (sgTamsToAft[ilSpecInd].AftValue[0] != 0)
          {  
            /*     if (sgTamsToAft[ilSpecInd].AftValue[0] != ' ') */
            strcat(pcgTamsAftValues,sgTamsToAft[ilSpecInd].AftValue);
            if (sgTamsToAft[ilSpecInd].AddSeconds == 1) 
            {
               strcat(pcgTamsAftValues,"00");
            }
          }
          else if (ilFldUsed == 0)
          {
            strcat(pcgTamsAftValues,sgTamsToAft[ilSpecInd].AftValue);
          }
          strcat(pcgTamsAftValues,"|"); /* add delimiter inside of value */
          ilFldUsed= 1;
        }
      }
      else
      {
        ilSpecInd -= 5000;
        if (pcgFixFields[ilSpecInd].Value[0] != 0)
        {
          strcat(pcgTamsAftValues,pcgFixFields[ilSpecInd].Value);
          strcat(pcgTamsAftValues,"|"); /* add delimiter inside of value */
        }
        else
        {
          strcat(pcgTamsAftValues," |"); /* add blank and delimiter inside of value */
        }
        ilFldUsed= 1;
      }
      ilFld++;
	  }
    if (ilFldUsed == 1)
    {
      /* replace last delimiter inside of value by value delimiter ('|' => ',') */
      pcgTamsAftValues[strlen(pcgTamsAftValues)-1] = ','; 
      strcat(pcgTAMSAftFields,sgDefTamsToAftSpec[ipCurr].Target);
      strcat(pcgTAMSAftFields,",");
    }
  }/*end of if viaExists*/
}

/******************************************************************************/
/* The EvalFreeBuildAftValues routine                                         */
/* This routine collects all combined free format UFIS fields and values set  */
/* in the array sgTamsToAft, which where set while processing the current     */
/* queue message                                                              */
/******************************************************************************/
void *EvalFreeBuildAftValues(void)
{
  int ilCurr;
  int ilTrigger;
  int ilSpec;
  int ilConcat;
  int ilAftIndex;

  for (ilCurr=0 ; ilCurr < igNumTamsToAftSpec; ilCurr++)
  {
/*   dbg(TRACE,"EvalFreeBuildAftValues: Rule No : <%d>",ilCurr); */
   ilConcat = 0;
   ilTrigger= pcgWrkTamsToAftSpec[ilCurr].iTrigger;
   if (ilTrigger==-1)
   { /* Trigger field does not exist, i.e. VIA? */
     for (ilSpec=0 ; ilSpec < 10; ilSpec++)
     {
       ilAftIndex= pcgWrkTamsToAftSpec[ilCurr].Index[ilSpec];
       if (ilAftIndex < 0)
       {
         ilSpec = 10; /* all valid indexes checked, exit for loop*/
       }
       else
       {
         if (sgTamsToAft[ilAftIndex].AftValue != NULL)
         {
           ilConcat = 1;
           ilSpec = 10; /* trigger on any field, exit for loop*/
         }
       }
     }
   }
   else if (sgTamsToAft[ilTrigger].AftValue != NULL)
   {
     ilConcat = 1;
   }
   if (ilConcat == 1)
   {
     if (pcgWrkTamsToAftSpec[ilCurr].Rule == CONCAT)
     {
       EvalConcat(ilCurr);
     }
     else if (pcgWrkTamsToAftSpec[ilCurr].Rule == CONCAT_SEP)
     {
       EvalConcatWithSeperator(ilCurr);
     }
     else
     {
        dbg(TRACE,"EvalFreeBuildAftValues: Rule type not implemeted: <%d>",
                   pcgWrkTamsToAftSpec[ilCurr].Rule); 
     }
   }
  }
}

/******************************************************************************/
/* The EvalFixAftValues routine                                               */
/* This routine adds the fix format UFIS fields and values                    */
/******************************************************************************/
void *EvalFixAftValues(char *pcpField, char *pcpValue)
{
   strcat(pcgTAMSAftFields,pcpField);
   strcat(pcgTAMSAftFields,",");
   if (pcpValue[0] != 0)
   {
      strcat(pcgTamsAftValues,pcpValue);
      strcat(pcgTamsAftValues,",");
   }
   else
   {
      strcat(pcgTamsAftValues," ,");
   }
}


/******************************************************************************/
/* The LocateFixField routine                                                 */
/* This routine is used to initialize the indexes in the array                */
/* "pcgWrkTamsToAftSpec"                                                      */
/******************************************************************************/
int LocateFixField (char *pcpName)
{
int ilCurr;
  for (ilCurr= 0; ilCurr < igFixMax ; ilCurr++)
  {
    if (strcmp(pcgFixFields[ilCurr].Name,pcpName) == 0)
    {
       return ilCurr;
    }
  }
  return -1;
}

/******************************************************************************/
/* The GetFixField routine                                                    */
/* This routine is used to extract the fixed values according to the kind of  */
/* TAMS message and add them and their UFIS field name to the output buffers  */
/******************************************************************************/
void GetFixField (char *pcpName, char* pcpValue)
{
int ilLen;
int ilCurr;
char *pclTmp;

  for (ilCurr= 0; ilCurr < igFixMax ; ilCurr++)
  {
    if (strcmp(pcgFixFields[ilCurr].Name,pcpName) == 0)
    {
       ilLen= pcgFixFields[ilCurr].Len;
       strncpy(pcgFixFields[ilCurr].Value,
              &pcpValue[pcgFixFields[ilCurr].Start],ilLen);
       pcgFixFields[ilCurr].Value[ilLen]= 0;
       dbg(DEBUG,"GetFixField: <%s> = <%s>", pcgFixFields[ilCurr].Name,
                                             pcgFixFields[ilCurr].Value);
       if (strcmp(pcgFixFields[ilCurr].Name,"FLNO") != 0)
       { /* FLNO will be treated extra */
          strcat(pcgTAMSAftFields,pcgFixFields[ilCurr].Name);
          strcat(pcgTAMSAftFields,",");
          pclTmp = SkipBlanksLeftRight(pcgFixFields[ilCurr].Value);
          if (pclTmp[0] != 0)
          {
             strcat(pcgTamsAftValues,pclTmp);
             strcat(pcgTamsAftValues,",");
          }
          else
          {
             strcat(pcgTamsAftValues," ,");
          }
       }
       return;
    }
  }
  dbg(TRACE,"GetFixField: build error: <%s> not found", pcgFixFields[ilCurr].Name);
}

void FormatFlnoStr(char *pcpName, char *pcpFlnoOut, char *pcpFlnoIn)
{
   char pclFltn[16] = "               ";
   char pclAlc[16] = "               ";
   char clSuff = 0;
   char *ptr;
   int  ilPos;
   int  ilFltn;

   if (strlen(pcpFlnoIn)>2) /* avoid empty MFF */
   {
      ilPos= 0;
      ptr = pcpFlnoIn;
      strncpy(pclAlc, ptr, 2); /* first 2 are arbitrary */
      ptr++;ptr++; /* advance to next */
      ilPos++;ilPos++;
      if (isalpha((int) *ptr) && *ptr != '\0')
      {
        pclAlc[ilPos] = *ptr;
        ptr++;ilPos++;
      }
      else if (isdigit((int)*ptr)) {
        /* do nothing */
      }
      if (ilPos>2)
      {
         sprintf(pcgTamsSel,"Error in %s: ALC2 <%s> to long",pcpName,pclAlc);
         dbg(TRACE,"FormatFlnoStr: %s",pcgTamsSel);
         pcgFlno[0] = 0; /* global string is used as error flag! */
      }
      else
      {   
         pclAlc[ilPos] = 0;
         ilPos=0;
         while (*ptr == ' ' && *ptr != '\0') ptr++; /* skip over possible ' ' */
         if (*ptr == '/' && strlen(ptr) > 3 )
         {  /* i.e. "SQ/LH4711" */
            ptr = ptr + 3 ; /* skip over "/<alc2>" of code share */
         }
         while (isdigit((int) *ptr) && *ptr != '\0')
         {
           pclFltn[ilPos] = *ptr;
           ptr++;ilPos++;
         }
         pclFltn[ilPos] = 0;
         clSuff= *ptr;
         if (ilPos>5)
         {
            sprintf(pcgTamsSel,"Error in %s: number <%s> to long",pcpName,pclFltn);
            dbg(TRACE,"FormatFlnoStr: %s",pcgTamsSel);
            pcgFlno[0] = 0; /* global string is used as error flag! */
         }
         else if (ilPos==0)
         {
            sprintf(pcgTamsSel,"Error in %s: <%s>: number does not begin with digits",pcpName,pclFltn);
            dbg(TRACE,"FormatFlnoStr: %s",pcgTamsSel);
            pcgFlno[0] = 0; /* global string is used as error flag! */
         }
         else
         {
            ilFltn= atoi(pclFltn);
            /* this will append the suffix directly behind the number :
             * sprintf(pcpFlnoOut,"%-3s%03d%c",pclAlc,ilFltn,clSuff); *
             * 20030822 JIM: write the suffix to 9 char of FLNO:      *
             */
            if (clSuff == 0)
            {
               sprintf(pcpFlnoOut,"%-3s%03d",pclAlc,ilFltn);
            }
            else
            {
               sprintf(pcpFlnoOut,"%-3s%03d   ",pclAlc,ilFltn);
               pcpFlnoOut[8]=clSuff;
               pcpFlnoOut[9]=0; /* if ilFltn has more than 3 digits, sprintf
                                   has written more than 9 chars */
            }
         }
      }
   }
   else
   {
      pcpFlnoOut[0] = 0;
      /* i do not know, if this is wanted and implemented 
       * if ((strcmp(pcpName,"JFLN")==0) && (strcmp(pcpFlnoIn," ")==0))
       * {
       *   pcpFlnoOut[0] = ' ';
       *   pcpFlnoOut[1] = 0; * flag to delete *
       * }
       */
   }
}

/******************************************************************************/
/* The CheckFlno routine                                                      */
/* This routine evaluate FLNO and JFNO of slave flights                       */
/* If JFNO is set at entrance, FLNO and JFNO are exchanded                    */
/* If JFNO is not set at entrance, everything is ok                           */
/******************************************************************************/
void CheckFlno(char *pclActCmd)
{
   
   pcgFlno[0] = 0; /* global string is used as error flag! */
   /* if (sgTamsToAft[igJfnoNdx].AftValue != NULL && strcmp(sgTamsToAft[igJfnoNdx].AftValue, " ")) */
   if (sgTamsToAft[igJfnoNdx].AftValue != NULL)
   {
      dbg(TRACE,"CheckFlno: data for code share flight!");
      if (strcmp(pclActCmd,"DFR")==0)
      {
         strcpy(pclActCmd,"UFR"); /* delete of join flights are handled as */
         strcpy(pcgJfno,"D;");    /* update to the master flight */
         dbg(DEBUG,"CheckFlno: setting flag to remove code share...");
      }
      else
      {
         strcpy(pcgJfno,"I;");
         dbg(DEBUG,"CheckFlno: setting flag to insert code share...");
      }
         
      FormatFlnoStr("FLNO",pcgFlno, SkipBlanksLeftRight(sgTamsToAft[igJfnoNdx].AftValue));
      if (pcgFlno[0]>0)
      {
         FormatFlnoStr("COSH",&pcgJfno[2], SkipBlanksLeftRight(pcgFixFields[0].Value));
         sgTamsToAft[igJfnoNdx].AftValue= pcgJfno;
         dbg(TRACE,"CheckFlno: exchanging FLNO and COSH: <%s/%s>!",pcgFlno,pcgJfno);
      }
      else
      {
         sgTamsToAft[igJfnoNdx].AftValue= NULL;
         if (igAllowEmptyMFF == 1) /* 20030715 JIM */
         {  /* ignore empty MFF */
            dbg(TRACE,"CheckFlno: MFF empty, but further processing allowed!");
            FormatFlnoStr("FLNO",pcgFlno, SkipBlanksLeftRight(pcgFixFields[0].Value));
         }
         else
         { 
            dbg(TRACE,"CheckFlno: MFF empty, ignored: lookup of master flight not possible!");
         }
      }
   }
   else
   {
      FormatFlnoStr("FLNO",pcgFlno, SkipBlanksLeftRight(pcgFixFields[0].Value));
   }

   if (pcgFlno[0]>0) /* avoid overwriting error flag */
   {
      dbg(TRACE,"CheckFlno: using <%s> as FLNO....",pcgFlno);

      if (sgTamsToAft[igJflnNdx].AftValue != NULL )
      {
       
          FormatFlnoStr("JFLN",pcgJfln, SkipBlanksLeftRight(sgTamsToAft[igJflnNdx].AftValue));
          sgTamsToAft[igJflnNdx].AftValue= pcgJfln;
      }
   }

}

/**************************************************************************************/
/* The CheckCosh routine                                                              */
/* This routine checks, if COSH is set. If so, all other data is ignored by RC_FAIL   */
/* 20030930 JIM                                                                       */
/**************************************************************************************/
int CheckCosh(void)
{
 char *pclTmp;
   
   if ((sgTamsToAft[igJfnoNdx].AftValue != NULL) && (igCoshEvaluateOther == 0))
   {
      dbg(TRACE,"CheckCosh: skipping all data but COSH flight number!");
      if (pcgAdid[0]=='A')
      {
         /* dbg(TRACE,"EvalFreeSimpleAftValues: Arr"); */
         pclTmp=sgTamsToAft[igJfnoNdx].AftArr;
      }
      else
      {
         /* dbg(TRACE,"EvalFreeSimpleAftValues: Dep"); */
         pclTmp=sgTamsToAft[igJfnoNdx].AftDep;
      }
      strcat(pcgTAMSAftFields,pclTmp);
      strcat(pcgTamsAftValues,sgTamsToAft[igJfnoNdx].AftValue);
      strcat(pcgTAMSAftFields,",");
      strcat(pcgTamsAftValues,",");
      return RC_FAIL;
   }
   return RC_SUCCESS;
}

/******************************************************************************/
/* The CheckRemForCancel routine                                              */
/* This routine checks, if TAMS send REM CN, in this case FTYP is set to X    */
/******************************************************************************/
void CheckRemForCancel()
{
char *pclRemp;

   pclRemp = sgTamsToAft[igRempNdx].AftValue;
   if (pclRemp!=NULL && pclRemp[0]!=0) 
   {
      dbg(DEBUG,"CheckRemForCancel: checking remark code REM for CN: <%s> ...",pclRemp);
      if (strcmp(pclRemp,"CN") == 0)
      {
         if (strlen(pcgTamsDat)==2)
         { /* 20030624 JIM no MI/SQ, cancel allowed */
           dbg(DEBUG,"CheckRemForCancel: Cancel flight, setting FTYP to <X> !");
           strcat(pcgTAMSAftFields,"FTYP,");
           strcat(pcgTamsAftValues,"X,");
         }
         else
         { /* 20030624 JIM: MI/SQ, cancel ignored */
           dbg(DEBUG,"CheckRemForCancel: Cancel flight: NOT setting FTYP to <X> for <%s>!",pcgTamsDat);
         }
      }
   }
}

/******************************************************************************/
/* The CheckPosAndGate routine                                                */
/* This routine uses TAMS Gate 'GAT' for POS if TAMS Position 'POS' is empty  */
/******************************************************************************/
void CheckPosAndGate()
{
   if (pcgAdid[0]=='A')
   {
      if ((sgTamsToAft[igPstaNdx].AftValue == NULL) ||
          (sgTamsToAft[igPstaNdx].AftValue[0] == ' '))
      {
          if (sgTamsToAft[igGta1Ndx].AftValue != NULL)
          {
             sgTamsToAft[igPstaNdx].AftValue = sgTamsToAft[igGta1Ndx].AftValue;
          }
      }
   }
   else if (pcgAdid[0]=='D')
   {
      if ((sgTamsToAft[igPstdNdx].AftValue == NULL) ||
          (sgTamsToAft[igPstdNdx].AftValue[0] == ' '))
      {
          if (sgTamsToAft[igGtd1Ndx].AftValue != NULL)
          {
             sgTamsToAft[igPstdNdx].AftValue = sgTamsToAft[igGtd1Ndx].AftValue;
          }
      }
   }
} /* CheckPosAndGate */


/********************************************************************************/
/* The CheckEtoaEtod routine                                                    */
/* This routine if ETD comes without ETA/ETD, in this case us 'xxxx' for HH:MM  */
/********************************************************************************/
void CheckEtoaEtod()
{
  if ((sgTamsToAft[igEtoADNdx].AftValue != NULL) &&
      (sgTamsToAft[igEtoADNdx].AftValue[0] != ' '))
  {

   if (pcgAdid[0]=='A')
   {
      if ((sgTamsToAft[igEtoa2Ndx].AftValue == NULL) ||
          (sgTamsToAft[igEtoa2Ndx].AftValue[0] == ' ') ||
          (sgTamsToAft[igEtoa2Ndx].AftValue[0] == 0))
      {
           sgTamsToAft[igEtoa2Ndx].AftValue = sgXXXX;
           dbg(DEBUG,"setting time part of ETOA to 'xxxx'");
      }
   }
   else if (pcgAdid[0]=='D')
   {
      if ((sgTamsToAft[igEtod2Ndx].AftValue == NULL) ||
          (sgTamsToAft[igEtod2Ndx].AftValue[0] == ' ') ||
          (sgTamsToAft[igEtod2Ndx].AftValue[0] == 0))
      {
           sgTamsToAft[igEtod2Ndx].AftValue = sgXXXX;
           dbg(DEBUG,"setting time part of ETOD to xxxx");
      }
   }
  }
} /* CheckEtoaEtod */

/******************************************************************************/
/* The CheckNature routine                                                    */
/* This routine maps the SAP nature from TAMS nature                          */
/******************************************************************************/
void CheckNature()
{
char *pclTmp1;
char *pclTmp2;
char *pclTTyp;

   pclTTyp = sgTamsToAft[igTtypNdx].AftValue;
   if (pclTTyp!=NULL && pclTTyp[0]!=0) 
   {
      dbg(DEBUG,"checking nature code <%s> ...",pclTTyp);
      pclTmp1 = pcgNatureIn;
      pclTmp2 = pcgNatureOut;
      while ((pclTmp1[0] != 0) && (pclTTyp[0] != 0))
      {
         dbg(DEBUG,"checking map code <%s> ...",pclTmp1);
         if (strcmp(pclTmp1,pclTTyp) == 0)
         {
            dbg(DEBUG,"mapping nature code <%s> to <%s> ...",pclTTyp,pclTmp2);
            pclTTyp[0] = 0;
         }
         else
         {
            pclTmp1= pclTmp1 + strlen(pclTmp1) +1 ;
            pclTmp2= pclTmp2 + strlen(pclTmp2) +1 ;
         }
      }
      if (pclTTyp[0] == 0)
      {
         sgTamsToAft[igTtypNdx].AftValue = pclTmp2;
      }
   }
}

/******************************************************************************/
/* The InitCheckNature routine                                                */
/* This routine reformats the comma seperated list to NULL seperated list     */
/******************************************************************************/
void InitCheckNature ()
{
char *pclTmp1;
char *pclTmp2;
int ilCnt= 0;

      pclTmp1 = pcgNatureIn;
      while ((pclTmp1 = strchr(pclTmp1,',')) != NULL)
      {
         *(pclTmp1++) = 0;
         ilCnt++;
      }
      pclTmp1 = pcgNatureOut;
      while ((pclTmp1 = strchr(pclTmp1,',')) != NULL)
      {
         *(pclTmp1++) = 0;
         ilCnt--;
      }
      if (ilCnt != 0)
      {
         dbg(DEBUG,"Syntax error in mapping of nature code: different number of in and out");
         dbg(DEBUG,"=== Mapping of nature code disabled! === ");
         pcgNatureIn[0] = 0;
      }
      else
      {
         pclTmp1 = pcgNatureIn;
         pclTmp2 = pcgNatureOut;
         while (pclTmp1[0] != 0) 
         {
            dbg(DEBUG,"Init of nature code mapping <%s> to <%s> ...",pclTmp1,pclTmp2);
            pclTmp1= pclTmp1 + strlen(pclTmp1) +1 ;
            pclTmp2= pclTmp2 + strlen(pclTmp2) +1 ;
         }
      }
}


/******************************************************************************/
/* The CheckAct3_5 routine                                                    */
/* This routine evaluate Act3 and Act5                                        */
/* If len(Act3) > len(Act5), exchange the values                              */
/* if value of Act3 is equal to Act5, remove one of them: remove Act3 if      */
/* len>3 otherwise remove Act5                                                */
/******************************************************************************/
void CheckAct3_5()
{
   
   pcgAct3= sgTamsToAft[igAct3Ndx].AftValue; 
   pcgAct5= sgTamsToAft[igAct5Ndx].AftValue; 
   if (pcgAct3 != NULL)
   {
      if (pcgAct5 != NULL)
      {
         if (strcmp(pcgAct3,pcgAct5) == 0)
         {  /* same values, remove the not fitting */
            if (strlen(pcgAct3)>3)
            {
               sgTamsToAft[igAct3Ndx].AftValue = NULL;
               dbg(TRACE,"CheckAct3_5: ACT3==ACT5, removing ACT3");
            }
            else
            {
               sgTamsToAft[igAct5Ndx].AftValue = NULL;
               dbg(TRACE,"CheckAct3_5: ACT3==ACT5, removing ACT5");
            }
         }
         else
         {  /* different values: do we have to exchange them? */
            if(strlen(pcgAct3) > strlen(pcgAct5) && pcgAct5[0] != 0)
            {
               sgTamsToAft[igAct3Ndx].AftValue = pcgAct5;
               sgTamsToAft[igAct5Ndx].AftValue = pcgAct3;
               dbg(TRACE,"CheckAct3_5: ACT3 longer than ACT5, exchanging ....");
            }
         }
      }
      else
      {  /* no Act5, may be Act3 is a Act5 */
         if (strlen(pcgAct3)>3)
         {
            sgTamsToAft[igAct5Ndx].AftValue = pcgAct3;
            sgTamsToAft[igAct3Ndx].AftValue = NULL;
            dbg(TRACE,"CheckAct3_5: ACT3 to long, setting ACT5 ....");
         }
      }
   }
   else
   {  /* no Act3 */
      if (pcgAct5 != NULL)
      {  /* may be Act5 is a Act3 */
         if (strlen(pcgAct5)<4)
         {
            sgTamsToAft[igAct3Ndx].AftValue = pcgAct5;
            sgTamsToAft[igAct5Ndx].AftValue = NULL;
            dbg(TRACE,"CheckAct3_5: ACT5 to short, setting ACT3 ....");
         }
      }
   }
}


/* 20040210 JIM: CheckFcal_1_2 */
/********************************************************************************/
/* The CheckFcal_1_2 routine                                                    */
/* This routine copies LAST CALL 1 (LC1) or LAST CALL 2 (LC2) to FINAL CALL     */
/********************************************************************************/
void CheckFcal_1_2()
{
  /* check LAST CALL 1: LC1 */
  if ((sgTamsToAft[igFclA2Ndx].AftValue != NULL) &&
      (sgTamsToAft[igFclA2Ndx].AftValue[0] != ' '))
  {
     sgTamsToAft[igFcal1Ndx].AftValue = sgTamsToAft[igFclA1Ndx].AftValue;
     sgTamsToAft[igFcal2Ndx].AftValue = sgTamsToAft[igFclA2Ndx].AftValue;
     dbg(DEBUG,"FCAL values set to final call 1 values");
  }
  /* check LAST CALL 2: LC2 (wins, if both come in same update) */
  if ((sgTamsToAft[igFclB2Ndx].AftValue != NULL) &&
      (sgTamsToAft[igFclB2Ndx].AftValue[0] != ' '))
  {
     sgTamsToAft[igFcal1Ndx].AftValue = sgTamsToAft[igFclB1Ndx].AftValue;
     sgTamsToAft[igFcal2Ndx].AftValue = sgTamsToAft[igFclB2Ndx].AftValue;
     dbg(DEBUG,"FCAL values set to final call 2 values");
  }
} /* CheckFcal_1_2 */

/******************************************************************************/
/* The HandleFreeForm routine                                                 */
/* This routine uses "GetTamsToAft" to evaluate all free format fields and    */
/* values from the TAMS message and adds this collection to the output buffers*/
/* Finally the output buffers are used then to build and send the CEDA event  */
/******************************************************************************/
int HandleFreeForm(char *pclFreeFormStart,char *pclActCmd)
{
   char *pcAftName;
   char pclTamsTmp[8];
   char *pclStr_tok;
   int ilRC = RC_SUCCESS;

   strcpy(pcgTamsSel,"IFNAME:TAMS,TIME:LOCAL,PRIO:4");
   strcpy(pcgTamsCmd,pclActCmd);
   
   pcgAccepted[0]= 0;
   pcgSkipped[0]= 0;
   pcgUnknown[0]= 0;
/* dbg(TRACE,"HandleFreeForm: <%s>",pclFreeFormStart); */
   pcgFreeFormCurr=pclFreeFormStart;
   if ((pcgFreeFormCurr != NULL) && (pcgFreeFormCurr[0]==0))
   {
      pcgFreeFormCurr = NULL;
   }
   else
   {
      pcgFreeFormNext=strchr(pcgFreeFormCurr,pcgSeperator);
      if (pcgFreeFormNext!=NULL)
      {
         *(pcgFreeFormNext++) = 0;
      }
   }
   while   (pcgFreeFormCurr != NULL)
   {
      if (pcgFreeFormCurr!=NULL)
      {
        pcgFreeFormCurr= SkipBlanksExceptOne(pcgFreeFormCurr);
        strncpy(pclTamsTmp,pcgFreeFormCurr,3);
        pclTamsTmp[3] = 0;
        if (strstr(pcgIgnoreEver,pclTamsTmp) != NULL) 
        {  /* JIM 20031107: ignore configured fields known for bad data */
           strcat(pcgSkipped,pcgFreeFormCurr);
           strcat(pcgSkipped,",");
        }
        else if (strstr(pcgValidFields,pclTamsTmp) == NULL) 
        {
           if (strstr(pcgIgnore,pclTamsTmp) == NULL) 
           {
              strcat(pcgUnknown,pcgFreeFormCurr);
              strcat(pcgUnknown,",");
           }
           else
           {
              strcat(pcgSkipped,pcgFreeFormCurr);
              strcat(pcgSkipped,",");
           }
        }
        else
        {
           pcAftName=GetTamsToAft(pcgFreeFormCurr);
           if (pcAftName == NULL)
           {
             strcat(pcgUnknown,pcgFreeFormCurr);
             strcat(pcgUnknown,",");
           }
           else if (pcAftName[0] == ' ')
           {
             strcat(pcgSkipped,pcgFreeFormCurr);
             strcat(pcgSkipped,",");
           }
           else if (strcmp(pcAftName,"TTYP") == 0)
           {
              if(strchr(pcgFreeFormCurr,'Z') != NULL)
              {
                pcgTAMSAftFields[0]= 0;
                pcgTamsAftValues[0]= 0;
                pcgTamsSel[0] = 0;
                dbg(TRACE,"HandleFreeForm: <%s>:TAMS test flight detected, ignore",pcgTamsSel);
                ilRC= RC_IGNORE;/* nature 'Z' are TAMS test flights */
                return ilRC;  /* leave loop ! */
              }
              else
              {
                strcat(pcgAccepted,pcgFreeFormCurr);
                strcat(pcgAccepted,",");
              }
           }
           else
           {
             strcat(pcgAccepted,pcgFreeFormCurr);
             strcat(pcgAccepted,",");
           }
        }
      }
      pcgFreeFormCurr=pcgFreeFormNext;
      if (pcgFreeFormCurr!=NULL)
      {
         pcgFreeFormNext=strchr(pcgFreeFormCurr,pcgSeperator);
         if (pcgFreeFormNext!=NULL)
         {
            *(pcgFreeFormNext++) = 0;
         }
      }
   }
   if (pcgSkipped[0] != 0)
   {
     pcgSkipped[strlen(pcgSkipped)-1] = 0;
     dbg(DEBUG,"HandleFreeForm: skipped: <%s> ",pcgSkipped);
   }
   if (pcgUnknown[0] != 0)
   {
     pcgUnknown[strlen(pcgUnknown)-1] = 0;
     sprintf(pcgTamsSel,"fields and values not valid for <%s>: <%s>",pcgTamsDat,pcgUnknown);
     dbg(TRACE,"HandleFreeForm: %s",pcgTamsSel);
     ilRC= RC_FAIL;/* unknown fields may be a hint for corrupt data */
   }
   else
   {
     CheckFlno(pclActCmd);
     if (pcgFlno[0] == 0)
     {
        ilRC= RC_FAIL;/* error code already set */
     }
     else
     {
        if (CheckCosh() == RC_SUCCESS) /* 20030930: if info is for Code share, skip other data  */
        {
          CheckAct3_5();
          CheckNature();
          CheckEtoaEtod();
          CheckFcal_1_2(); /* 20040210 JIM: CheckFcal_1_2 */
          /* CheckPosAndGate(); USC: 5.9.2003 removed on request */
          EvalFreeSimpleAftValues();
          EvalFreeBuildAftValues();
        }
        if (pcgTAMSAftFields[0] != 0)
        {
          CheckRemForCancel();
          pcgTAMSAftFields[strlen(pcgTAMSAftFields)-1] = 0;
          pcgTamsAftValues[strlen(pcgTamsAftValues)-1] = 0;
          if (pcgAccepted[0] != 0)
          {
             pcgAccepted[strlen(pcgAccepted)-1] = 0;
          }
          dbg(DEBUG,"HandleFreeForm: accepted: <%s> ",pcgAccepted);
          dbg(TRACE,"HandleFreeForm: AFT Fields: <%s> ",pcgTAMSAftFields);
          dbg(TRACE,"HandleFreeForm: AFT Values: <%s> ",pcgTamsAftValues);
          dbg(TRACE,"HandleFreeForm: SEL <%s> ",pcgTamsSel);
          if ((strcmp(pcgTAMSAftFields,"FLDA,ADID") == 0) && 
              (strcmp(pclActCmd,"DFR") != 0)
             )
          {  /* only FLDA,ADID remaining, to less to insert, and update not
                allowed
                but delete of one flight is possible
              */
             pcgTAMSAftFields[0]= 0;
             pcgTamsAftValues[0]= 0;
             pcgTamsSel[0] = 0;
             dbg(TRACE,"HandleFreeForm: no changes of flight detected, ignore",pcgTamsSel);
             ilRC= RC_IGNORE;
          }
          else
          {
             sprintf(pcgTamsOutF,"FLNO,%s",pcgTAMSAftFields);
             sprintf(pcgTamsOutD,"%s,%s",pcgFlno,pcgTamsAftValues);
             ilRC= RC_SUCCESS;
          }
        }
     }
   }
   return ilRC;
}

/******************************************************************************/
/* The HandleOnline routine                                                   */
/* This routine extracts all the fixed field values and all free format fields*/
/* of TAMS online (in opposite to scheduled) messages                         */
/******************************************************************************/
int HandleOnline(char *pclData,char *pclActCmd,char *pclAdid)
{
   int ilRC = RC_SUCCESS;
   char pclAirline[8];


   dbg(TRACE,"HandleOnline: <%s>",pclData);
   pcgAdid[0] = pclAdid[0]; 
   GetFixField ("FLNO", pclData);
   GetFixField ("FLDA", pclData);
   GetFixField ("ADID", pclAdid);
   pcgFreeForm=&pclData[20];
   strncpy(pclAirline,pcgFixFields[0].Value,2);
   pclAirline[2]=0;
   if (strstr(pcgSpecAirlines,pclAirline) != NULL)
   {
     pcgTamsDat[4] = 0; /* delimit pcgTamsDat (and pclData also) to '1USQ' ... for dbg */
     dbg(DEBUG,"HandleOnline: restricted update for Airline <%s> ",pclAirline);
     if (pclAdid[0] == 'A')
     {
        pcgValidFields= pcgArrOnlSQMI;
     }
     else
     {
        pcgValidFields= pcgDepOnlSQMI;
     }
     pcgIgnore= pcgIgnoreSQMI;
   }
   else
   {
     pcgTamsDat[2] = 0; /* delimit pcgTamsDat (and pclData also) to '1U' ... for dbg */
   }
   ilRC= HandleFreeForm(pcgFreeForm,pclActCmd);
   return ilRC;
}

/******************************************************************************/
/* The HandleSchedule routine                                                 */
/* This routine extracts all the fixed field values and all free format fields*/
/* of TAMS scheduled (in opposite to online) messages                         */
/******************************************************************************/
int HandleSchedule(char *pclData,char *pclActCmd,char *pclAdid)
{
   char pclAirline[8];
   int ilRC = RC_SUCCESS;
   dbg(TRACE,"HandleSchedule: <%s>",pclData);
   pcgAdid[0] = pclAdid[0];
   GetFixField ("FLNO", pclData);
   strncpy(pclAirline,pcgFixFields[0].Value,2);
   pclAirline[2]=0;
   if (strstr(pcgSpecAirlines,pclAirline) != NULL)
   {
     pcgTAMSAftFields[0]= 0;
     pcgTamsAftValues[0]= 0;
     pcgTamsSel[0] = 0;
     sprintf(pcgTamsSel,"Airline <%s>: schedule data skipped ",pclAirline);
     dbg(DEBUG,"HandleSchedule: %s",pcgTamsSel);
     ilRC= RC_IGNORE;/* schedule data of SQ,MI not handled by TAMS */
   }
   else
   {
     GetFixField ("VPFR", pclData);
     GetFixField ("VPTO", pclData);
     GetFixField ("FREQ", pclData);
     GetFixField ("ADID", pclAdid);
     pcgFreeForm=&pclData[35];
     pcgTamsDat[2] = 0; /* delimit pcgTamsDat (and pclData also) for dbg */
     dbg(DEBUG,"HandleSchedule: Data   <%s>",pcgFreeForm);
     ilRC= HandleFreeForm(pcgFreeForm,pclActCmd);
   }
   return ilRC;
}

/******************************************************************************/
/* The HandleCCARequest routine !! FIRST APPROACH / no processing of values!! */
/* This routine extracts the fixed field values for the time range, from which*/
/* the TAMS want to reload the CheckIn Counter infos                          */
/******************************************************************************/
int HandleCCARequest(char *pclData,char *pclActCmd)
{
   int ilRC = RC_SUCCESS;
   char clStartTime[20] = "\0";

   dbg(TRACE,"HandleCCARequest: <%s>",pclData);
   strncpy(clStartTime,&pclData[2],14);
   clStartTime[14] = '\0';
   LocalTimeToUtcFixTZ(clStartTime);
   dbg(TRACE,"clStartTime: <%s>",clStartTime);
   
      SendCedaEvent(atoi(cgAdhoc),mod_name," ", " ",
                    " "," ","REC","AFTTAB",clStartTime,
                    " "," "," ", 5,RC_SUCCESS) ;

      ilRC= RC_IGNORE;/* RQ message shall not be processed by imphdl */
  return ilRC;
}

/******************************************************************************/
/* The ResetAftValues routine                                                 */
/* This routine resets all pointers and output buffers for online and         */
/* scheduled TAMS data                                                        */
/******************************************************************************/
void *ResetAftValues(void)
{
  int ilCurr;
  for (ilCurr= 0; ilCurr < igNumTamsToAft ; ilCurr++)
  {
     sgTamsToAft[ilCurr].AftValue = NULL;
  }
  for (ilCurr= 0; ilCurr < igFixMax ; ilCurr++)
  {
    pcgFixFields[ilCurr].Value[0] = 0;
  }
  pcgTAMSAftFields[0]= 0;
  pcgTamsAftValues[0]= 0;
  pcgTamsOutF[0]= 0;
  pcgTamsOutD[0]= 0;
  pcgTamsSel[0] = 0;
  strcpy(pcgDataTyp,",  ,");
  if (igNumTamsToAftSpec == 0)
  {
    InitWrkTamsToAftSpec();
  }
    
}

/******************************************************************************/
/* The wrapTamsData routine                                                   */
/* This is the interface routine called by IMPMAN                             */
/* In:  pclData:   pointer to the data record received from TAMS              */
/* Out: pclCmd:    pointer to store the command "UFR" or "DFR"                */
/*      pclSelOut: pointer to store the selection for IMPMAN                  */
/*      pclFldOut: pointer to store the field list for IMPMAN                 */
/*      pclDatOut: pointer to store the data list for IMPMAN                  */
/******************************************************************************/
int wrapTamsData(char *pclData, char *pclCmd,
                 char *pclSelOut,char *pclFldOut,char *pclDatOut,
                 int ipPrioToTams, int ipQueueToTams)
{
    int ilRC = RC_SUCCESS;
	char clOrigData[4000] = "\0";

	strcpy(clOrigData,pclData);

    if (igNumTamsToAft == 0)
    {
       Init_Tams(ipPrioToTams,ipQueueToTams);
       igJfnoNdx= LocateSpecField("COSH");
       igJflnNdx= LocateSpecField("JFLN");
       igAct3Ndx= LocateSpecField("ACT3");
       igAct5Ndx= LocateSpecField("ACT5");
       igTtypNdx= LocateSpecField("TTYP");
       igRempNdx= LocateSpecField("REMP");
       igPstaNdx= LocateSpecField("PSTA");
       igPstdNdx= LocateSpecField("PSTD");
       igGta1Ndx= LocateSpecField("GTA1");
       igGtd1Ndx= LocateSpecField("GTD1");
       igEtoADNdx= LocateSpecField("*ETOA1");
       igEtoa2Ndx= LocateSpecField("*ETOA2");
       igEtod2Ndx= LocateSpecField("*ETOD2");
       igFcal1Ndx= LocateSpecField("*FCAL1");/* 20040210 JIM */
       igFcal2Ndx= LocateSpecField("*FCAL2");/* 20040210 JIM */
       igFclA1Ndx= LocateSpecField("*FCLA1");/* 20040210 JIM */
       igFclA2Ndx= LocateSpecField("*FCLA2");/* 20040210 JIM */
       igFclB1Ndx= LocateSpecField("*FCLB1");/* 20040210 JIM */
       igFclB2Ndx= LocateSpecField("*FCLB2");/* 20040210 JIM */
       InitCheckNature();
    }

    pcgTamsSel= pclSelOut;
    pcgTamsOutF= pclFldOut;
    pcgTamsOutD= pclDatOut;
    pcgTamsDat= pclData;
    pcgTamsCmd= pclCmd;
    
    ResetAftValues();

    strncpy(&pcgDataTyp[1],pclData,2);
    if (strstr(pcgIgnoreDataTypes,pcgDataTyp) != NULL)
    { /* 20031113 JIM: ignore configured types of data: */
      sprintf(pcgTamsSel,"Schedule data skipped due to configuration");
      dbg(DEBUG,"HandleSchedule: %s",pcgTamsSel);
      ilRC= RC_IGNORE;
    }
    else if (strcmp(pcgDataTyp,",1U,") == 0)
    {
       pcgValidFields= pcgArrOnline;
       pcgIgnore= pcgIgnOnline;
       pcgIgnoreEver= pcgEverIgnOnline;
       ilRC = HandleOnline(pclData,"UFR","A");
    } 
    else if (strcmp(pcgDataTyp,",2U,") == 0)
    {
       pcgValidFields= pcgDepOnline;
       pcgIgnore= pcgIgnOnline;
       pcgIgnoreEver= pcgEverIgnOnline;
       ilRC = HandleOnline(pclData,"UFR","D");
    } 
    else if (strcmp(pcgDataTyp,",3U,") == 0)
    {
       pcgValidFields= pcgArrSched;
       pcgIgnore= pcgIgnSched;
       pcgIgnoreEver= pcgEverIgnSched;
       ilRC = HandleSchedule(pclData,"UFR","A");
    } 
    else if (strcmp(pcgDataTyp,",4U,") == 0)
    {
       pcgValidFields= pcgDepSched;
       pcgIgnore= pcgIgnSched;
       pcgIgnoreEver= pcgEverIgnSched;
       ilRC = HandleSchedule(pclData,"UFR","D");
    } 
    else if (strcmp(pcgDataTyp,",1D,") == 0)
    {
       pcgIgnore= pcgIgnoreAll;
       pcgIgnoreEver= pcgEverIgnOnline;
       pcgValidFields= pcgIgnore;
       ilRC = HandleOnline(pclData,"DFR","A");
    } 
    else if (strcmp(pcgDataTyp,",2D,") == 0)
    {
       pcgIgnore= pcgIgnoreAll;
       pcgIgnoreEver= pcgEverIgnOnline;
       pcgValidFields= pcgIgnore;
       ilRC = HandleOnline(pclData,"DFR","D");
    } 
    else if (strcmp(pcgDataTyp,",3D,") == 0)
    {
       pcgIgnore= pcgIgnoreAll;
       pcgIgnoreEver= pcgEverIgnSched;
       pcgValidFields= pcgIgnore;
       ilRC = HandleSchedule(pclData,"DFR","A");
    } 
    else if (strcmp(pcgDataTyp,",4D,") == 0)
    {
       pcgIgnore= pcgIgnoreAll;
       pcgIgnoreEver= pcgEverIgnSched;
       pcgValidFields= pcgIgnore;
       ilRC = HandleSchedule(pclData,"DFR","D");
    } 
    else if (strcmp(pcgDataTyp,",RQ,") == 0)
    {
       ilRC = HandleCCARequest(pclData,"RT");
    } 
    else
    {
       pclData[2] = 0; /* debug first two chars of data */
       sprintf(pcgTamsSel,"unknown message designator: <%s>",pclData);
       dbg(TRACE,"HandleData: %s, not processed!",pcgTamsSel);
       pcgTamsSel[0] = 0;
       pclData[0] = 0;
       ilRC = RC_FAIL;
    }


	dbg(TRACE,"result of wraptamsdata:\n");
	dbg(TRACE,"clOrigData:    <%s>",clOrigData);
	dbg(TRACE,"pclCmd:        <%s>",pclCmd);
	dbg(TRACE,"pclSelOut:     <%s>",pclSelOut);
	dbg(TRACE,"pclFldOut:     <%s>",pclFldOut);
	dbg(TRACE,"pclDatOut:     <%s>",pclDatOut);
	dbg(TRACE,"ipPrioToTams:  <%d>",ipPrioToTams);
	dbg(TRACE,"ipQueueToTams: <%d>",ipQueueToTams);

    return ilRC;
}

