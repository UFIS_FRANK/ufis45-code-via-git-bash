#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/Sin/SIN_Server/Base/Server/Kernel/hashdl.c 1.33 2007/12/12 20:58:50SGT akl Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :                                                           */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history :                                                           */
/* 20040810 JIM: removed sccs_  string, mark mks_version as possibly unused   */
/******************************************************************************/
/*                                                                            */
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */


#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "fditools.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "rmschkrul.h"
#include "syslib.h"
#include "db_if.h"
#include <time.h>
#include <cedatime.h>

#ifdef _HPUX_SOURCE
	extern int daylight;
	extern long timezone;
	extern char *tzname[2];
#else
	extern int _daylight;
	extern long _timezone;
	extern char *_tzname[2];
#endif

int debug_level = 0;
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static int   igDelTimeIntrvl;
static EVENT *prgOutEvent  = NULL;

static char cgProcessName[20] = "\0";
static char  cgConfigFile[512] = "\0";
static char  cgHopo[8] = "\0";                         /* default home airport    */
static char  cgTabEnd[8] ="\0";                       /* default table extension */

static long lgEvtCnt = 0;

enum TimerType {NONE,TIM,EVT};
static uint igDayTab []	= { 0,31,28,31,30,31,30,31,31,30,31,30,31 };

#define NumOfCommands 10
#define NumOfTimerEntries 32
#define MAX_INT_PARAMETER 32000
char cgpFtype [] = "(FTYP='O' OR FTYP='Z' OR FTYP='B')"; 
char *cgValidEvents [] = { "FC", "ATD", NULL };

typedef struct {
  char Group[6];
  char Command[NumOfCommands][12];
  int  Timer1[NumOfCommands][NumOfTimerEntries];
  int  Timer2[NumOfCommands][NumOfTimerEntries];
} KriscomConfigType;

struct KriscomList {
  char ALC2[3];
  struct List *next;
  struct List *prev;
  KriscomConfigType data; 
};

typedef struct KriscomList KriscomListElem;
typedef KriscomListElem *KriscomListPtr;

typedef struct {
  char *data;
  uint status;      /* various status information (bit-mask)    */ 
  uint len;         /* size of currently allocated data element */
  uint timeType;    /* time is UTC or timezone */
  char timeZone[3]; /* the 2 letter code of the timezone, if time is local */
} DataElementType;

#define MAX_NUM_LEGS 32

struct List {
  char *field;
  struct List *next;
  struct List *prev;
  DataElementType dobj[MAX_NUM_LEGS];    /* points to a DataElement   */
};

typedef struct {
  char *firstElement;
  char *lastElement;
  uint numEntries;
} ListHead;

typedef struct List ListElem;
typedef ListElem *ListPtr;

struct InDataType {
  struct InDataType *next;
  struct InDataType *prev;
  ListPtr ifData;
  uint serialNumber;
  uint stage;
};

typedef struct InDataType InDataElem;
typedef struct InDataType *InDataPtr;

/*
** Data status bits
*/
#define IS_VALID   0x01
#define IS_AFT     0x02           /* those fields will be send to the flight handler */ 
#define IS_INVALID 0x04
#define IS_UTCTIME 0x08           /* those fields have time values to be converted to UTC */

#define DATABLK_SIZE (256*1024)
#define RC_INVALID -128
#define RC_NODATA -129
#define RC_NOTFOUND -130
#define RC_ISPENDING -131
static char  pcgDataArea[DATABLK_SIZE];
static char  pcgSqlBuf[12 * 1024];

static int   igCEDAbridge = 0;
static int   igFdiId = 0;
static char  pcgDataArea[DATABLK_SIZE];
static char  pcgSqlBuf[12 * 1024];
static int   igTimeWindow = 10;         /* time window to fetch flights from -- should be read from config */
static char  pcgRecvName[64] ;          /* BC-Head used as WKS-Name */
static char  pcgDestName[64] ;          /* BC-Head used as USR-Name */
static char  pcgTwStart[64] ;           /* BC-Head used as Stamp */
static char  pcgTwEnd[64] ;             /* BC-Head used as Stamp */
static char  pcgChkWks[128] ; 
static char  pcgChkUsr[128] ; 
static char  pcgActCmd[16];
static char  cgErrMsg[128];
static ListPtr pcgItemList=NULL;
static ListPtr pcgGrp2Airlines=NULL;
static int   pcgGrp1Modid;
static int   pcgGrp2Modid;
static int   pcgGrp3Modid;
static int   test = 1;
static char cgErrorsToFdi[10] = "\0";

static char cgAlerts[100][512];
static char cgErrors[100][512];
static char cgAlertSections[1024] = "\0";
static int igAlertCount = 0;

static char pcgFdihdlName[16] = "fdihdl";
/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
static int  Init_Process();
static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,
			   char *pcpCfgBuffer);


static int	Reset(void);                       /* Reset program          */
static void	Terminate(int ipSleep);            /* Terminate program      */
static void	HandleSignal(int);                 /* Handles signals        */
static void	HandleErr(int);                    /* Handles general errors */
static void   HandleQueErr(int);                 /* Handles queuing errors */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/


static int	HandleData(EVENT *prpEvent);       /* Handles event data     */
static int  GetCommand(char *pcpCommand, int *pipCmd);

static int  HandleData_User(BC_HEAD *prlBchead,
			    CMDBLK *prlCmdblk,
			    char *pclSelection,
			    char *pclFields,
			    char *pclData);

static void  HandleShortTimerRequest();

static char *getNextItem(char *cpSource, char *cpDelim, uint *ipLen, uint *ipLast);
static int addItemToList(char *element, ListPtr *hook);
static char *getItemDataByName(ListPtr hook, char *name, uint legLine);
static int removeItemFromList (ListPtr hook);
static ListPtr getFirstFree(ListPtr hook); 
static char *getFirstTimerEntry(char *pcpEntries, int *ipType, char **pcpNextEntry);
static char *getFirstActTimerEntry(char *pcpEntries, int *ipType, char **pcpNextEntry, int ipHour, int ipMinAct, int ipMinStart);
static long GetSequence(char *pcpKey);
static void PutSequence(char *pcpKey,long lpSequence);
static void HandleKriscomResponse(char *pcpData, char *pcpCommand, char *pcpIdent);
static void HandleInsertFlightTimer(char *pcpFields, char *pcpData);
static void HandleDeleteFlightTimer(char *pcpFields, char *pcpData);
static void HandleUpdateFlightTimer(char *pcpFields, char *pcpData);
static int createItemList(char *list, ListPtr *hook);
static int addDataItemToList(char *list, ListPtr *hook, uint entry, char delimiter, uint defaultStatus);
static int checkCedaTime(char *pcpData);
static void decrypt(char *pcpPasswd);
static void selDataAndParam(char *pcpLogin, char *pcpParam, char *pcpBegSep, char *pcpEndSep);
static int get_int(char *Section, char *field, int *data);
static void HandleKriscomError(char *pcpLongError, char *pcpShortError, char *pcpIdent);
static int HandleLongTimerRequest(int ipInit);
static void alert(char *pcpMessage);
static void ReadAlertConfig();
static int setup_config();
static void HandleCleanUp();
/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
	int	ilRc = RC_SUCCESS;			/* Return code			*/
	int	ilCnt = 0;
	int ilOldDebugLevel = 0;


	INITIALIZE;			/* General initialization	*/



	debug_level = DEBUG;

	strcpy(cgProcessName,argv[0]);

	dbg(TRACE,"MAIN: version <%s>",mks_version);

	/* Attach to the MIKE queues */
	do
	{
		ilRc = init_que();
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		}/* end of if */
	}while((ilCnt < 10) && (ilRc != RC_SUCCESS));

	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
		sleep(60);
		exit(1);
	}else{
		dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
	}/* end of if */

	do
	{
		ilRc = init_db();
		if (ilRc != RC_SUCCESS)
		{
			check_ret(ilRc);
			dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		} /* end of if */
	} while((ilCnt < 10) && (ilRc != RC_SUCCESS));

	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
		sleep(60);
		exit(2);
	}else{
		dbg(TRACE,"MAIN: init_db() OK!");
	} /* end of if */

	/* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */

	sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
	dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
	ilRc = TransferFile(cgConfigFile);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */

	sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
	dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
	ilRc = TransferFile(cgConfigFile);
	if(ilRc != RC_SUCCESS)
	{ 
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */

	ilRc = SendRemoteShutdown(mod_id);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
	} /* end of if */

	if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
	{
		dbg(DEBUG,"MAIN: waiting for status switch ...");
		HandleQueues();
	}/* end of if */

	if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
	{
		dbg(TRACE,"MAIN: initializing ...");
			ilRc = Init_Process();
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"Init_Process: init failed!");
			} /* end of if */

	} else {
		Terminate(30);
	}/* end of if */



	dbg(TRACE,"=====================");
	dbg(TRACE,"MAIN: initializing OK");
	dbg(TRACE,"=====================");
	

	for(;;)
	{
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;
				
		if( ilRc == RC_SUCCESS )
		{
			
			lgEvtCnt++;

			/* Acknowledge the item */
			ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRc != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRc);
			} /* fi */
			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				break;	
			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				/* CloseConnection(); */
				HandleQueues();
				break;	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate(1);
				break;	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				break;	
			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;
			case	SHUTDOWN	:
				/* process shutdown - maybe from uutil */
				Terminate(1);
				break;
					
			case	RESET		:
				ilRc = Reset();
				break;
					
			case	EVENT_DATA	:
				if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
				{
					ilRc = HandleData(prgEvent);
					if(ilRc != RC_SUCCESS)
					{
						HandleErr(ilRc);
					}/* end of if */
				}else{
					dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
				}/* end of if */
				break;
				

			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;
			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;
			default			:
				dbg(TRACE,"MAIN: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */




		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		} /* end else */
		
	} /* end for */
	
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/



static int ReadConfigEntry(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer)
{
    int ilRc = RC_SUCCESS;

    char clSection[124] = "\0";
    char clKeyword[124] = "\0";

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRc = iGetConfigEntry(cgConfigFile,clSection,clKeyword,
			   CFG_STRING,pcpCfgBuffer);
    if(ilRc != RC_SUCCESS)
    {
	dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
    } 
    else
    {
	dbg(DEBUG,"Config Entry <%s>,<%s>:<%s> found in %s",
	    clSection, clKeyword ,pcpCfgBuffer, cgConfigFile);
    }/* end of if */
    return ilRc;
}


static int Init_Process()
{
  int  ilRc = RC_SUCCESS;			/* Return code */
  char clSection[64] = "\0";
  char clKeyword[64] = "\0";
  int ilOldDebugLevel = 0;
  long pclAddFieldLens[12];
  char pclAddFields[256] = "\0";
  char pclSqlBuf[2560] = "\0";
  char pclSelection[1024] = "\0";
  short slCursor = 0;
  short slSqlFunc = 0;
  char  clBreak[24] = "\0";
  short slWait = 1;

	if(ilRc == RC_SUCCESS)
	{
		/* read HomeAirPort from SGS.TAB */
		ilRc = tool_search_exco_data("SYS", "HOMEAP", cgHopo);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"<Init_Process> EXTAB,SYS,HOMEAP not found in SGS.TAB");
			Terminate(30);
		} else {
			dbg(TRACE,"<Init_Process> home airport <%s>",cgHopo);
		}
	}

	
	if(ilRc == RC_SUCCESS)
	{

		ilRc = tool_search_exco_data("ALL","TABEND", cgTabEnd);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"<Init_Process> EXTAB,ALL,TABEND not found in SGS.TAB");
			Terminate(30);
		} else {
			dbg(TRACE,"<Init_Process> table extension <%s>",cgTabEnd);
		}
	}

	if(ilRc == RC_SUCCESS) {
          ilRc = ReadConfigEntry("MAIN","FDIHDL_NAME",pcgFdihdlName);
	  igCEDAbridge = tool_get_q_id("bridge");
	  /*igFdiId = tool_get_q_id("fdihdl");*/
	  igFdiId = tool_get_q_id(pcgFdihdlName);
	  dbg(TRACE,"Mod-Id of %s = %d",pcgFdihdlName,igFdiId);
	}

	if(ilRc == RC_SUCCESS) {
	  ilRc = get_int("MAIN", "Grp1_Modid", &pcgGrp1Modid);
	}
	if(ilRc == RC_SUCCESS) {
	  ilRc = get_int("MAIN", "Grp2_Modid", &pcgGrp2Modid);
	}
	if(ilRc == RC_SUCCESS) {
	  ilRc = get_int("MAIN", "Grp3_Modid", &pcgGrp3Modid);
	}
	if(ilRc == RC_SUCCESS) {
	  ilRc = get_int("MAIN", "EXPIRED_TIMER_INTERVAL", &igDelTimeIntrvl);
	}
	if (ilRc != RC_SUCCESS){
	  igDelTimeIntrvl = 7;
	  ilRc = RC_SUCCESS;
	}
	if(ilRc == RC_SUCCESS) {
	  ilRc = setup_config();
	}
	
	/* while (slWait); */

	if(ilRc == RC_SUCCESS) {
	  HandleLongTimerRequest(TRUE); 
	}

	ReadAlertConfig();

	if (ilRc != RC_SUCCESS)
	{
	    dbg(TRACE,"Init_Process failed");
	}

	return(ilRc);
	
} /* end of initialize */




/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
	int	ilRc = RC_SUCCESS;				/* Return code */
	
	dbg(TRACE,"Reset: now resetting");
	Init_Process();
	
	return ilRc;
	
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
	/* unset SIGCHLD ! DB-Child will terminate ! */

	dbg(TRACE,"Terminate: now DB logoff ...");

	signal(SIGCHLD,SIG_IGN);

	logoff();

	dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);


	dbg(TRACE,"Terminate: now leaving ...");

	fclose(outp);

	sleep(ipSleep < 1 ? 1 : ipSleep);
	
	exit(0);
	
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/

static void HandleSignal(int pipSig)
{
	dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);

	switch(pipSig)
	{
	default	:
		Terminate(1);
		break;
	} /* end of switch */

	exit(1);
	
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
	dbg(TRACE,"HandleErr: <%d> : unknown ERROR",pipErr);

	return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
	switch(pipErr) {
	case	QUE_E_FUNC	:	/* Unknown function */
		dbg(TRACE,"<%d> : unknown function",pipErr);
		break;
	case	QUE_E_MEMORY	:	/* Malloc reports no memory */
		dbg(TRACE,"<%d> : malloc failed",pipErr);
		break;
	case	QUE_E_SEND	:	/* Error using msgsnd */
		dbg(TRACE,"<%d> : msgsnd failed",pipErr);
		break;
	case	QUE_E_GET	:	/* Error using msgrcv */
		dbg(TRACE,"<%d> : msgrcv failed",pipErr);
		break;
	case	QUE_E_EXISTS	:
		dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
		break;
	case	QUE_E_NOFIND	:
		dbg(TRACE,"<%d> : route not found ",pipErr);
		break;
	case	QUE_E_ACKUNEX	:
		dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
		break;
	case	QUE_E_STATUS	:
		dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
		break;
	case	QUE_E_INACTIVE	:
		dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
		break;
	case	QUE_E_MISACK	:
		dbg(TRACE,"<%d> : missing ack ",pipErr);
		break;
	case	QUE_E_NOQUEUES	:
		dbg(TRACE,"<%d> : queue does not exist",pipErr);
		break;
	case	QUE_E_RESP	:	/* No response on CREATE */
		dbg(TRACE,"<%d> : no response on create",pipErr);
		break;
	case	QUE_E_FULL	:
		dbg(TRACE,"<%d> : too many route destinations",pipErr);
		break;
	case	QUE_E_NOMSG	:	/* No message on queue */
		dbg(TRACE,"<%d> : no messages on queue",pipErr);
		break;
	case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
		dbg(TRACE,"<%d> : invalid originator=0",pipErr);
		break;
	case	QUE_E_NOINIT	:	/* Queues is not initialized*/
		dbg(TRACE,"<%d> : queues are not initialized",pipErr);
		break;
	case	QUE_E_ITOBIG	:
		dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
		break;
	case	QUE_E_BUFSIZ	:
		dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
		break;
	default			:	/* Unknown queue error */
		dbg(TRACE,"<%d> : unknown error",pipErr);
		break;
	} /* end switch */
         
	return;

} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
	int	ilRc = RC_SUCCESS;			/* Return code */
	int	ilBreakOut = FALSE;
	
	do
	{
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;	

		if( ilRc == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRc != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRc);
			} /* fi */
		
			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				ilBreakOut = TRUE;
				break;	

			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate(1);
				break;	
	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				ilBreakOut = TRUE;
				break;	

			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;

			case	SHUTDOWN	:
				Terminate(1);
				break;
						
			case	RESET		:
				ilRc = Reset();
				break;
						
			case	EVENT_DATA	:
				dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
					
			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;

			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;

			default			:
				dbg(TRACE,"HandleQueues: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		} /* end else */
	} while (ilBreakOut == FALSE);

			ilRc = Init_Process();
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitDemhdl: init failed!");
			} 
     

} /* end of HandleQueues */
	



/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(EVENT *prpEvent)
{
	int	   ilRc           = RC_SUCCESS;			/* Return code */
	int      ilCmd          = 0;

	BC_HEAD *prlBchead       = NULL;
	CMDBLK  *prlCmdblk       = NULL;
	char    *pclSelection    = NULL;
	char    *pclFields       = NULL;
	char    *pclData         = NULL;
	char    *pclRow          = NULL;
	char 		clUrnoList[2400];
	char 		clTable[34];
	int			ilUpdPoolJob = TRUE;

	prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
	prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
	pclSelection = prlCmdblk->data;
	pclFields    = pclSelection + strlen(pclSelection) + 1;
	pclData      = pclFields + strlen(pclFields) + 1;


	strcpy(clTable,prlCmdblk->obj_name);

	/*if (strcmp(prlCmdblk->command,"TIM"))*/ { /* supress info for timer message */
	  dbg(TRACE,"========== START <%10.10d> ==========",lgEvtCnt);
	  
	  /****************************************/
	  /*DebugPrintBchead(DEBUG,prlBchead);*/
	  /*DebugPrintCmdblk(DEBUG,prlCmdblk);*/
	  dbg(TRACE,"Command:   <%s>",prlCmdblk->command);
	  /*dbg(TRACE,"originator follows event = %p ",prpEvent);*/
	  
	  dbg(DEBUG,"Originator:<%d>",prpEvent->originator);
	  /*dbg(DEBUG,"selection follows Selection = %p ",pclSelection);*/
	  dbg(DEBUG,"Selection: <%s>",pclSelection);
	  dbg(DEBUG,"Fields:    <%s>",pclFields);
	  dbg(DEBUG,"Data:      <%.1000s>",pclData);
	  /****************************************/
	}
	
	HandleData_User(prlBchead,prlCmdblk,pclSelection,pclFields,pclData);


	/*if (strcmp(prlCmdblk->command,"TIM"))*/ { /* supress info for timer message */
	  dbg(TRACE,"==========  END  <%10.10d> ==========",lgEvtCnt);
	}

	return(RC_SUCCESS);
	
} /* end of HandleData */



/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
/******************************************************************************/
/* HandleData_User	                                                      */
/*									      */
/* Handle process specific data						      */
/*									      */
/******************************************************************************/
static int HandleData_User(BC_HEAD *prlBchead,
			   CMDBLK *prlCmdblk,
			   char *pclSelection,
			   char *pclFields,
			   char *pclData)
{
  uint ilDataID = 0;
  int  ilRC;

  strcpy  (pcgActCmd,prlCmdblk->command);
  memset  (pcgRecvName, '\0', (sizeof(prlBchead->recv_name) + 1)); 
  strncpy (pcgRecvName, prlBchead->recv_name, sizeof(prlBchead->recv_name));
    
  /* User */
  memset  (pcgDestName, '\0', sizeof(prlBchead->recv_name)) ; 
  strcpy  (pcgDestName, prlBchead->dest_name) ;   /* UserLoginName */
  sprintf (pcgChkWks, ",%s,", pcgRecvName) ; 
  sprintf (pcgChkUsr, ",%s,", pcgDestName) ; 
    
  /* SaveGlobal Info of CMDBLK */
  strcpy (pcgTwStart, prlCmdblk->tw_start) ; 
  strcpy (pcgTwEnd, prlCmdblk->tw_end) ; 
    
  strcpy (pcgActCmd,prlCmdblk->command);
 
  if (!strcmp(pcgActCmd, "TIM")) { /* 1 min timer, perform required actions */
    HandleShortTimerRequest();
  }
  else if (!strcmp(pcgActCmd, "TIML")) { /* 10 min timer */
    HandleLongTimerRequest(FALSE);
  }
  else if (!strcmp(pcgActCmd, "CLUP")) { /* daily cleanup */
    HandleCleanUp();
  }
  else if (!strcmp(pcgActCmd, "DFR")) { /* record deletion */
    HandleDeleteFlightTimer(pclFields, pclData);
  }
  else if (!strcmp(pcgActCmd, "UFR")) { /* update and possibly event */
    HandleUpdateFlightTimer(pclFields, pclData);
  }
  else if (!strcmp(pcgActCmd, "IFR")) { /* new flight */
    HandleInsertFlightTimer(pclFields, pclData);
  }
  else if (!strcmp(pcgActCmd, "KRI")) { /* kriscom response */
    HandleKriscomResponse(pclData, pclSelection, pcgTwStart);
  }
  else if (!strcmp(pcgActCmd, "KER")) { /* kriscom response */
    HandleKriscomError(pclData, pclSelection, pcgTwStart);
  }
  else if (!strcmp(pcgActCmd, "CAN")) { /* cancel timer */
    setTimerState(pclData, pclSelection,TRUE);
  }
  else if (!strcmp(pcgActCmd, "RES")) { /* resume timer */
    setTimerState(pclData, pclSelection,FALSE);
  }
  else if (!strcmp(pcgActCmd, "EXRQ")) { /* external request */
    HandleExternalRequest(pclData, pclSelection);
  }
  else {
    dbg(TRACE,"<HandleData_User> Unknown Command <%s>", pcgActCmd);
    return RC_FAIL;
  }
}
/******************************************************************************/
/* HandleCleanUp                                                              */
/*                                                                            */
/* Remove unused records from SEVTAB (stat='DELETE')                          */
/*                                                                            */
/******************************************************************************/
static void HandleCleanUp()
{
  int  ilRC;
  int  ilActDay,ilActMin,ilActWkDy;
  int  ilDelTime;
  char clActTim[15];

  GetServerTimeStamp("UTC", 1, 0, clActTim);
  ilRC = GetFullDay(clActTim, (int *)&ilActDay, (int *)&ilActMin, (int *)&ilActWkDy);
  ilDelTime = ilActDay - igDelTimeIntrvl;
  sprintf(pcgSqlBuf, "DELETE FROM SEVTAB WHERE STAT='DELETE' AND LSTU<>' ' AND FIN<'%d'", ilDelTime);
  ilRC = getOrPutDBData(START|COMMIT);
  if (ilRC  != RC_SUCCESS && ilRC != RC_NODATA) {
    dbg(TRACE, "<HandleCleanUp> Unable to delete SEVTAB entries with STAT='DELETE'");
  }
}
/******************************************************************************/
/* HandleExternalRequest                                                      */
/*                                                                            */
/* Handle external request command.                                           */ 
/*                                                                            */
/* Input:                                                                     */
/*       pcpCmd      -- Command to be sent                                    */
/*       pcpUrno -- URNO of requested flight                                  */
/*                                                                            */
/* Return:                                                                    */
/*       nothing                                                              */
/*                                                                            */
/******************************************************************************/
HandleExternalRequest(char *pcpCmd, char *pcpUrno)
{
  long llHASIDENT;
  long llUrno;
  long llCFUrno;
  int  ilRC;
  char *pclGrpTyp;
  char *pclCFUrno;
  char *pclALC2;
  char *pclFLNO;
  char *pclACTTOD;
  char clALC2[3];
  char clFLNO[10];
  char clACTTOD[15];

  dbg(DEBUG, "<HandleExternalRequest> Received command: <%s> Urno: <%s>", pcpCmd, pcpUrno);

  llUrno = atol(pcpUrno);

  sprintf(pcgSqlBuf, "SELECT ALC2,FLNO,TIFD FROM AFTTAB WHERE URNO=%ld", llUrno);
  ilRC = getOrPutDBData(START);
  if (ilRC == RC_FAIL) {
    dbg(TRACE, "<HandleExternalRequest> SQL-statement failed <%s>", pcgSqlBuf);
  }
  else {
    BuildItemBuffer(pcgDataArea, NULL, 3, ";");
    pclALC2 = strtok(pcgDataArea, ";");
    pclFLNO = strtok(NULL, ";");
    pclACTTOD = strtok(NULL, ";");
  }

  if (pclALC2 == NULL) {
    dbg(TRACE, "<HandleExternalRequest> Fatal -- AFTTAB:no ALC2 available");
    return RC_FAIL;
  }
  else if (pclFLNO == NULL) {
    dbg(TRACE, "<HandleExternalRequest> Fatal -- AFTTAB:no FLNO available");
    return RC_FAIL;
  }
  else if (pclACTTOD == NULL) {
    dbg(TRACE, "<HandleExternalRequest> Fatal -- AFTTAB:no TIFD available");
    return RC_FAIL;
  }

  strcpy(clALC2, pclALC2);
  strcpy(clFLNO, pclFLNO);
  strcpy(clACTTOD, pclACTTOD);

  sprintf(pcgSqlBuf, "SELECT GRPT,UHAT FROM HCFTAB WHERE ALC2='%s'", pclALC2);
  ilRC = getOrPutDBData(START);
  if (ilRC == RC_FAIL) {
    dbg(TRACE, "<HandleExternalRequest> SQL-statement failed <%s>", pcgSqlBuf);
    return RC_FAIL;
  }
  else {
    BuildItemBuffer(pcgDataArea, NULL, 2, ";");
    pclGrpTyp = strtok(pcgDataArea, ";");
    pclCFUrno = strtok(NULL, ";");
  }
  if (pclGrpTyp == NULL) {
    dbg(TRACE, "<HandleExternalRequest> Fatal -- HCFTAB:no GRPT available");
    return RC_FAIL;
  }
  else if (pclCFUrno == NULL) {
    dbg(TRACE, "<HandleExternalRequest> Fatal -- HCFTAB:no UHAT available");
    return RC_FAIL;
  }
    
  llCFUrno = atol(pclCFUrno);
  sendKriscomCommand(0, pcpCmd, pclGrpTyp, clFLNO, clACTTOD, pcpUrno, "HASCONF", llCFUrno);
}
/******************************************************************************/
/*  setTimerState                                                             */
/*                                                                            */
/* The status of the timer is set to 'CANCELLED' if ipSetMode is TRUE and     */
/* back to 'ACTIVE' if ipSetMode is FALSE                                      */
/*                                                                            */
/* Input:                                                                     */
/*       pcpFields -- ',' separated list of fields                            */
/*       pcpData   -- ',' separated list of data                              */
/*       ipSetMode -- set status to 'CANCELLED' if TRUE 'ACTIVE' else.         */
/*                                                                            */
/* Return:                                                                    */
/*       nothing                                                              */
/*                                                                            */
/******************************************************************************/
setTimerState(char *pcpFields, char *pcpData, int ipSetMode)
{
  long llUrno;
  int  ilRC;
  char *pclData;
  char clStatus[18];

  dbg(DEBUG, "<setTimerState> Received Fields: <%s> Data: <%s>",pcpFields, pcpData);
  /*
  ** build Key/Item list
  */
  createItemList(pcpFields, &pcgItemList);
  addDataItemToList(pcpData, &pcgItemList, 0, ',', IS_VALID);

  pclData = getItemDataByName(pcgItemList, "URNO", 0);
  if (pclData != NULL) {
    llUrno = atol(pclData);
  }

  if (ipSetMode) sprintf(clStatus, "CANCELLED");
  else sprintf(clStatus, "ACTIVE");

  sprintf(pcgSqlBuf, "UPDATE SEVTAB SET STAT='%s' WHERE UAFT='%ld'", clStatus, llUrno);

  ilRC = getOrPutDBData(START|COMMIT);
  if (ilRC  != RC_SUCCESS) {
    dbg(TRACE, "<setTimerState> Unable to update timer status to <%s> for URNO=%ld", clStatus, llUrno);
  }
  deleteItemList(pcgItemList);
  pcgItemList = NULL;
}
/******************************************************************************/
/* HandleDeleteFlightTimer                                                    */
/*                                                                            */
/* Receiving a DFR results in deleting the timer request from SEVTAB,         */
/* using the UAFT field.                                                      */
/*                                                                            */
/* Input:                                                                     */
/*       pcpFields -- ',' separated list of fields                            */
/*       pcpData   -- ',' separated list of data                              *
/*                                                                            */
/* Return:                                                                    */
/*       nothing                                                              */
/*                                                                            */
/******************************************************************************/
static void HandleDeleteFlightTimer(char *pcpFields, char *pcpData)
{
  char *pclData;
  long llURNO;
  int  ilRC;

  dbg(DEBUG, "<HandleDeleteFlightTimer> Received Fields: <%s> Data: <%s>",pcpFields, pcpData);
  /*
  ** build Key/Item list
  */
  createItemList(pcpFields, &pcgItemList);
  addDataItemToList(pcpData, &pcgItemList, 0, ',', IS_VALID);

  pclData = getItemDataByName(pcgItemList, "URNO", 0);
  if (pclData != NULL) {
    llURNO = atol(pclData);
  }
  else {
    dbg(TRACE, "<HandleDeleteFlightTimer> Failed to perform DELETE on record with missing URNO");
  }
  sprintf(pcgSqlBuf, "DELETE FROM SEVTAB WHERE UAFT=%ld", llURNO);
  ilRC = getOrPutDBData(START|COMMIT);
  if (ilRC  != RC_SUCCESS && ilRC != RC_NODATA) {
    dbg(TRACE, "<HandleFlightData> Unable to delete SEVTAB entry with URNO=%ld", llURNO);
  }
  deleteItemList(pcgItemList);
  pcgItemList = NULL;
}
/******************************************************************************/
/* HandleInsertFlightTimer                                                    */
/*                                                                            */
/* Receiving an IFR, we have to check if this flight needs to be inserted     */
/* into the timer tab.                                                        */
/* Input:                                                                     */
/*       pcpFields -- ',' separated list of fields                            */
/*       pcpData   -- ',' separated list of data                              */
/*                                                                            */
/* Return:                                                                    */
/*       nothing                                                              */
/*                                                                            */
/******************************************************************************/
static void HandleInsertFlightTimer(char *pcpFields, char *pcpData)
{
  char  *pclADID;
  char  *pclData;
  char  *pclALC2;
  char  *pclFLNO;
  int   ilTimer;
  int   ilRC = RC_SUCCESS;
  long  llURNO;
  int   ilDiffMin;
  char  clActTim[15];
  char  clNewTifd[15];
  char  clALC2[3];
  char  clFLNO[15];

  dbg(DEBUG, "<HandleInsertFlightTimer> Received Fields: <%s> Data: <%s>",pcpFields, pcpData);
  /*
  ** build Key/Item list
  */
  createItemList(pcpFields, &pcgItemList);
  addDataItemToList(pcpData, &pcgItemList, 0, ',', IS_VALID);

  pclADID = getItemDataByName(pcgItemList, "ADID", 0);
  if (pclADID != NULL) {
    if (strcmp(pclADID,"D")) {
      dbg(DEBUG, "<HandleInsertFlightTimer> Not a departure -- skipped");
      deleteItemList(pcgItemList);
      pcgItemList = NULL;
      return;
    }
  }
  pclData = getItemDataByName(pcgItemList, "URNO", 0);
  if (pclData != NULL) {
    llURNO = atol(pclData);
  }
  /*
  ** Check, if we need to insert a new timer
  */
  GetServerTimeStamp("UTC", 1, 0, clActTim);
  DateDiffToMin(clNewTifd, clActTim, &ilDiffMin);
  if (ilDiffMin <= 72*60) {
  }
  deleteItemList(pcgItemList);
  pcgItemList = NULL;
}
/******************************************************************************/
/* HandleUpdateFlightTimer                                                    */
/*                                                                            */
/* Receiving an UFR, we have to check if this flight needs to be inserted     */
/* into the timer tab. If we receive an AIRB or OFFBL, this will create an    */
/* 'flight closed' (FC) event. The timerlist is then scanned for commands     */
/* waiting for an FC event.                                                   */
/*                                                                            */
/* Input:                                                                     */
/*       pcpFields -- ',' separated list of fields                            */
/*       pcpData   -- ',' separated list of data                              *
/*                                                                            */
/* Return:                                                                    */
/*       nothing                                                              */
/*                                                                            */
/******************************************************************************/
static void HandleUpdateFlightTimer(char *pcpFields, char *pcpData)
{
  char  *pclData;
  char  *pclTimer;
  char  *pclACTOD;
  char  *pclADID;
  char  *pclALC2;
  char  *pclFLNO;
  int   ilTimer;
  long  llURNO;
  int   ilRC = RC_SUCCESS;
  int   ilGetRc;
  int   ilHaveActTod = FALSE;
  int   ilFlightClosed = FALSE;
  int   ilCntDayActTod, ilCntMinActTod, ilWkDy;
  int   ilCntDayNewTifd, ilCntMinNewTifd;
  int   ilTimerDiff;
  int   ilDiffMin;
  short slFkt,slCursor;
  char  clNewTifd[15];
  char  clACTOD[15];
  char  clActTim[15];
  char  clALC2[3];
  char  clFLNO[15];
  char  clWhereClause[256];
  char  pclSqlBuf[1024];
  char  pclDataArea[1024];

  dbg(DEBUG, "<HandleUpdateFlightTimer> Received Fields: <%s> Data: <%s>",pcpFields, pcpData);
  /*
  ** build Key/Item list
  */
  createItemList(pcpFields, &pcgItemList);
  addDataItemToList(pcpData, &pcgItemList, 0, ',', IS_VALID);

  pclADID = getItemDataByName(pcgItemList, "ADID", 0);
  if (pclADID != NULL) {
    if (strcmp(pclADID,"D")) {
      dbg(DEBUG, "<HandleUpdateFlightTimer> Not a departure -- skipped");
      deleteItemList(pcgItemList);
      pcgItemList = NULL;
      return;
    }
  }
  pclData = getItemDataByName(pcgItemList, "URNO", 0);
  if (pclData != NULL) {
    llURNO = atol(pclData);
  }
  /*
  ** check if this is a FC (Flight Close) event
  */
  pclData = getItemDataByName(pcgItemList, "AIRB", 0);
  if (pclData != 0) {
    if (checkCedaTime(pclData) == RC_SUCCESS) ilFlightClosed = TRUE;
  }
  pclData = getItemDataByName(pcgItemList, "OFBL", 0);
  if (pclData != 0) {
    if (checkCedaTime(pclData) == RC_SUCCESS) ilFlightClosed = TRUE;
  }
  if (ilFlightClosed) {
    deleteItemList(pcgItemList);
    pcgItemList = NULL;
    sprintf(clWhereClause, "(EVNT='FC' OR EVNT='ATD') AND UAFT=%ld ORDER BY PRIO", llURNO);
    ilRC =  performTimerOrEventAction(clWhereClause, TRUE);
    return;
  }
  /*
  ** Check, if we need to update an existant timer
  */
  pclData = getItemDataByName(pcgItemList, "TIFD", 0);
  if (pclData != 0) {
    sprintf(clNewTifd, "%s", pclData);
    sprintf(pclSqlBuf, "SELECT ATOD FROM SEVTAB WHERE UAFT='%ld'", llURNO);

    slFkt = START;
    slCursor = 0;
    while ((ilGetRc = sql_if (slFkt, &slCursor, pclSqlBuf, pclDataArea)) == DB_SUCCESS && ilHaveActTod == FALSE) {
      if (ilGetRc == DB_SUCCESS) {
	if (pclDataArea !=  NULL) {
	  sprintf(clACTOD, "%s", pclDataArea);
	  if (checkCedaTime(clACTOD) == RC_SUCCESS) {
	    ilHaveActTod = TRUE;
	    ilRC = GetFullDay(clACTOD, &ilCntDayActTod, &ilCntMinActTod, &ilWkDy);
	    ilRC = GetFullDay(clNewTifd, &ilCntDayNewTifd, &ilCntMinNewTifd, &ilWkDy);
	    ilTimerDiff = ilCntMinNewTifd - ilCntMinActTod;
	    dbg(DEBUG, "<HandleUpdateFlightTimer> Timer offset will be added <%d>", ilTimerDiff); 
	  }
	}
      }
      slFkt = NEXT;
    }
    close_my_cursor (&slCursor);
    if (ilHaveActTod && ilTimerDiff !=0) {
      sprintf(pcgSqlBuf, "UPDATE SEVTAB SET TIMR=TIMR+%d,ATOD='%s' WHERE UAFT='%ld'", ilTimerDiff, clNewTifd, llURNO);
      ilRC = getOrPutDBData(START|COMMIT);
      if (ilRC == RC_FAIL) {
	dbg(TRACE, "<HandleUpdateFlightTimer> SQL-statement failed <%s>", pcgSqlBuf);
      }
      /* negativ Timer values -> 0 */
      sprintf(pcgSqlBuf, "UPDATE SEVTAB SET TIMR=0 WHERE TIMR<0");
      ilRC = getOrPutDBData(START|COMMIT);
      if (ilRC == RC_FAIL) {
	dbg(TRACE, "<HandleUpdateFlightTimer> SQL-statement failed <%s>", pcgSqlBuf);
      }
    }
    if (ilHaveActTod) {
      deleteItemList(pcgItemList);
      pcgItemList = NULL;
      return;
    }
  
    /*
    ** Check, if we need to insert a new timer
    */
    GetServerTimeStamp("UTC", 1, 0, clActTim);
    DateDiffToMin(clNewTifd, clActTim, &ilDiffMin);
    if (ilDiffMin <= 72*60) {
      sprintf(pcgSqlBuf, "SELECT ALC2,FLNO FROM AFTTAB WHERE URNO=%ld", llURNO);
      ilRC = getOrPutDBData(START);
      if (ilRC == RC_FAIL) {
	dbg(TRACE, "<HandleUpdateFlightTimer> SQL-statement failed <%s>", pcgSqlBuf);
      }
      else {
	BuildItemBuffer(pcgDataArea, NULL, 2, ";");
	pclALC2 = strtok(pcgDataArea, ";");
	pclFLNO = strtok(NULL, ";");
	if (pclALC2 != NULL && pclFLNO != NULL) {
	  strcpy(clALC2, pclALC2);
	  strcpy(clFLNO, pclFLNO);
	  addTimerEntry(llURNO, clALC2, clNewTifd, clFLNO);
	}
	else dbg(TRACE, "<HandleUpdateFlightTimer> Unable to fetch ALC2 or FLNO");
      }
    }
    /*  ilRC = GetFullDay(clNewTifd, &ilCntDayNewTifd, &ilCntMinNewTifd, &ilWkDy); */
  }
  deleteItemList(pcgItemList);
  pcgItemList = NULL;
}
/******************************************************************************/
/* HandleKriscomError                                                         */
/*                                                                            */
/* Handle errors sent from the KRISCOM subsystem                              */
/*                                                                            */
/* Input:                                                                     */
/*       pcpLongError  -- long error message from KRISCOM                     */
/*       pcpshortError -- error code from KRISCOM                             */
/*       pcpIdent -- ID of the timer record                                   */
/*                                                                            */
/* Return:                                                                    */
/*       nothing                                                              */
/*                                                                            */
/******************************************************************************/
void HandleKriscomError(char *pcpLongError, char *pcpShortError, char *pcpIdent)
{
  char *clId;
  long llHasId=0;
  static char clMessage[128];

  dbg(DEBUG, "<HandleKriscomError> Error: <%s> / <%s> Ident: <%s>", pcpLongError, pcpShortError, pcpIdent);

  clId = strstr(pcpIdent, ",");
  if (clId != NULL) {
    clId++;
    llHasId = atol(clId);
  }
  
  if (!strcmp(pcpShortError, "NSF")) { /* No such flight -> alert */
    sprintf(clMessage, "No such flight <%s>", pcpLongError);
    alert(clMessage);
  }
  else if (!strcmp(pcpShortError, "FAF")) { /* Do Flight assignment first -> alert */
    sprintf(clMessage, "Do Flight assignment first <%s>", pcpLongError);
    alert(clMessage);
  }
  else if (!strcmp(pcpShortError, "UAT")) { /* Unauthorized to enter -> alert */
    sprintf(clMessage, "Unauthorized to enter <%s>", pcpLongError);
    alert(clMessage);
  }
  else if (!strcmp(pcpShortError, "LNC")) { /* Load plan not completed -> retry in 10 minutes */
  }
  else if (!strcmp(pcpShortError, "FNO")) { /* If flight departed -> continue w/o action, else retry once, then alert */
  }
  else if (!strcmp(pcpShortError, "NYB")) { /* No pax boarded -> retry in 10 minutes */
  }
  else if (!strcmp(pcpShortError, "NCD")) { /* Flight not departed from previous station */
  }
  else if (!strcmp(pcpShortError, "IRE")) { /* Flight cancelled -> alert */
    sprintf(clMessage, "Flight cancelled <%s>",pcpLongError);
    alert(clMessage); 
  }
  else if (!strcmp(pcpShortError, "FDA")) { /* Flight departed already -> silently ignore */
  }
  else if (!strcmp(pcpShortError, "NTD")) { /* No load to display -> continue w/o action */
  }
  else if (!strcmp(pcpShortError, "ERR")) { /* Unspecified error -> to be defined */
  }
 }

static void alert(char *pcpMessage)
{
    char clMyAlert[2000] = "\0";
    int ilRC = RC_SUCCESS;
    dbg(TRACE, "ALERT: *****%s*****", pcpMessage);
    ilRC = AddAlert2("KRISCOM"," "," ","E",pcpMessage," ",TRUE,FALSE,"ALERT",mod_name," "," ");
}
/******************************************************************************/
/* HandleKriscomResponse                                                      */
/*                                                                            */
/* Receives data from the KRISCOM system and prepares sending to FDIHDL.      */
/* Timer status is set to 'ACTIVE' if there are remaining request. If there    */
/* is not a remaining request, this record will be removed.                   */ 
/*                                                                            */
/* Input:                                                                     */
/*       pcpData  -- data from KRISCOM                                        */
/*       pcpIdent -- ID of the timer record                                   */
/*                                                                            */
/* Return:                                                                    */
/*       nothing                                                              */
/*                                                                            */
/******************************************************************************/
void HandleKriscomResponse(char *pcpData, char *pcpSelection, char *pcpIdent)
{
  char *clId;
  char *clUrno;
  char *clCommand;
  char *clPtr,*clPtr1;
  char *enId;
  char *pclTmp;
  char *pclRealAlertFlight;

  long ilHasId=0;
  char  pclFields[1024];
  char  pclValues[64000]; /* This is the buffer to contain the screen data */
  char  pclSelection[100];
  char  pclActCmd[16];
  char  pclTwStart[64];
  int   ilRC = RC_SUCCESS;
  int   ilPrio=4;
  long  ilEnId=-1;
  char  clOSRQ[129];
  char  clSearch[15];
  int   ilFound = FALSE;
  int   ilRightComma = FALSE;
  int   ilLeftComma = FALSE;
  int   ilEmbedComma = FALSE;
  int   i = 0;
  char clAlertFlight[100] = "\0";
  char clMyAlert[256] = "\0";

  dbg(DEBUG, "<HandleKriscomResponse> Received data: <%.1000s> Ident: <%s>",pcpData, pcpIdent);
  
  dbg(TRACE,"Now get FLNO, DATE, COMMAND for alert-message");
  GetDataItem(clAlertFlight,pcpSelection,10,';',"","");
  pclRealAlertFlight = strstr(pcpSelection,clAlertFlight);
  dbg(TRACE,"FLNO, DATE, COMMAND for alert-message found <%s>",pclRealAlertFlight);

  for (i=1 ; i<= igAlertCount; i++)
  {
      if (strstr(pcpData,cgErrors[i-1]) != NULL)
      {
	  sprintf(clMyAlert,"%s : %s",pclRealAlertFlight,cgAlerts[i-1]);
	  alert(clMyAlert);
	  dbg(TRACE,"<HandleKriscomResponse> Fatal -- Error received: <%s>, sended alert: <%s>",cgErrors[i-1],cgAlerts[i-1]);
	  if ((strcmp(cgErrorsToFdi,"NO") == 0) || (strcmp(cgErrorsToFdi,"no") == 0))
	  {
	      return;
	  }
      }
  }
  
  clId = strstr(pcpIdent, ",");
  if (clId != NULL) {
    clId++;
    ilHasId = atol(clId);
  }

  enId = strstr(clId, ",");
  if (enId != NULL) {
    enId++;
    ilEnId = atol(enId);
    dbg(DEBUG, "<HandleKriscomResponse> Timer request <%s>", enId);
  }
  dbg(DEBUG, "<HandleKriscomResponse> HasIdent: <%ld>", ilHasId);

  clUrno = strtok(pcpSelection, ";");
  if (clUrno == NULL) {
    dbg(TRACE, "<HandleKriscomResponse> Fatal -- no URNO, skipped response");
    return;
  }
  
  clCommand = strtok(NULL, ";");
  if (clCommand == NULL) {
    dbg(TRACE, "<HandleKriscomResponse> Fatal -- no Command, skipped response");
    return;
  }
  
  if (ilEnId != 0 && ilEnId != -1) {
    sprintf(pcgSqlBuf,"UPDATE SEVTAB SET STAT='ACTIVE' WHERE UHAT=%ld", clUrno);
    ilRC = getOrPutDBData(START|COMMIT);
  }
  /*
  ** Send received data to FDIHDL
  */
  strcpy(pclActCmd, "FDI");
  strcpy(pclSelection, "TELEX,7");
  sprintf(pcgTwEnd, "%s,%s,%s", cgHopo, cgTabEnd, cgProcessName);

  sprintf(pclFields, "");
  sprintf(pclValues, "<=SOURCE=>KRISCOM<=/SOURCE=>\n<=COMMAND=>%s<=/COMMAND=>\n<=URNO=>%s<=/URNO=>\n<=SCREENCONTENT=>\n%s\n<=/SCREENCONTENT=>",clCommand, clUrno, pcpData);
  ilRC = SendCedaEvent(igFdiId, 0, pcgDestName, pcgRecvName, pclTwStart, pcgTwEnd,
		       pclActCmd, "", pclSelection,
		       pclFields, pclValues, "", ilPrio, RC_SUCCESS) ;
}
/******************************************************************************/
/* HandleShortTimerRequest                                                    */
/*                                                                            */
/* Called every minute. Checks for some work: if a timer has expired, i.e = 0 */
/* then send the required command. If there are more commands to perform,     */
/* setup the new timer value, if not, remove that entry. Furthermore:         */
/* decrement all timers from the table that are <> 0                          */
/*                                                                            */
/******************************************************************************/
static void  HandleShortTimerRequest() 
{
  int ilRC;

  ilRC =  performTimerOrEventAction("TIMR<=0 AND ATIM<>' ' AND STAT='ACTIVE' ORDER BY PRIO", FALSE);
  sprintf(pcgSqlBuf,"UPDATE SEVTAB SET TIMR=TIMR-1 WHERE TIMR<>0");
  ilRC = getOrPutDBData(START|COMMIT);
}

/******************************************************************************/
/* performTimerOrEventAction                                                  */
/*                                                                            */
/* This function will figure out which commands are to be performed due to    */
/* expired timers or occured events.                                          */
/*                                                                            */
/* Input:                                                                     */
/*       pcpSelection -- search selection from caller                         */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/******************************************************************************/
int performTimerOrEventAction(char *pcpSelection, int ipIsEvent)
{
  int ilRC;
  int ilGetRc;
  short slFkt = 0;
  short slCursor = 0;
  char *pclURNO;
  char *pclCFURNO;
  long llCFURNO;
  long llURNO;
  char *pclHasId;
  long llHasId;
  char *pclActCmd;
  char *pclGrpTyp;
  char *pclFLNO;
  char *pclACTTOD;
  char *pclHRList;
  char *pclMinList;
  char *pclActTimer;
  char *pclEvent;
  char clErrBuff[128];
  char clDate[15];
  char pclSqlBuf[1024];
  char pclDataArea[1024];

  sprintf (pclSqlBuf,"SELECT UAFT,UHAT,HAID,ACMD,GRPT,FLNO,ATOD,TIHR,TIMI,ATIM,EVNT FROM SEVTAB WHERE %s", pcpSelection);
  slFkt = START;
  slCursor = 0;
  while ((ilGetRc = sql_if (slFkt, &slCursor, pclSqlBuf, pclDataArea)) == DB_SUCCESS) {
    if (ilGetRc == DB_SUCCESS) {
      BuildItemBuffer(pclDataArea, NULL, 11, ";");
      pclURNO = strtok(pclDataArea, ";");
      if (pclURNO == NULL) {
	dbg(TRACE, "<HandleShortTimerRequest> No URNO -- data skipped");
	slFkt = NEXT;
	continue;
      }
      else llURNO = atol(pclURNO);

      pclCFURNO = strtok(NULL, ";");
      if (pclCFURNO == NULL) {
	dbg(TRACE, "<HandleShortTimerRequest> No CFURNO -- data skipped");
	slFkt = NEXT;
	continue;
      }	
      else llCFURNO = atol(pclCFURNO);

      pclHasId = strtok(NULL, ";");
      if (pclHasId == NULL) {
	dbg(TRACE, "<HandleShortTimerRequest> No HasId -- data skipped");
	slFkt = NEXT;
	continue;
      }
      llHasId = atol(pclHasId);

      pclActCmd = strtok(NULL, ";");
      if (pclActCmd == NULL) {
	dbg(TRACE, "<HandleShortTimerRequest> No Command -- data skipped");
	slFkt = NEXT;
	continue;
      }
      pclGrpTyp = strtok(NULL, ";");
      if (pclGrpTyp  == NULL) {
	dbg(TRACE, "<HandleShortTimerRequest> No Group Type -- data skipped");
	slFkt = NEXT;
	continue;
      }
      pclFLNO = strtok(NULL, ";");
      if (pclFLNO  == NULL) {
	dbg(TRACE, "<HandleShortTimerRequest> No FLNO -- data skipped");
	slFkt = NEXT;	
	continue;
      }
      pclACTTOD = strtok(NULL, ";");
      if (pclACTTOD == NULL) {
	dbg(TRACE, "<HandleShortTimerRequest> No ACTTOD -- data skipped");
	slFkt = NEXT;
	continue;
      }
      pclHRList = strtok(NULL, ";");
      if (pclHRList == NULL) {
	dbg(TRACE, "<HandleShortTimerRequest> No Hour List -- data skipped");
	slFkt = NEXT;
	continue;
      }
      pclMinList = strtok(NULL, ";");
      if (pclMinList == NULL) {
	dbg(TRACE, "<HandleShortTimerRequest> No Minute List -- data skipped");
	slFkt = NEXT;
	continue;
      }
      pclActTimer = strtok(NULL, ";");
      if (pclActTimer == NULL) {
	dbg(TRACE, "<HandleShortTimerRequest> No ActTimer -- data skipped");
	slFkt = NEXT;
	continue;
      }
      pclEvent = strtok(NULL, ";");
      if (pclEvent  == NULL) {
	dbg(TRACE, "<HandleShortTimerRequest> No Event -- data skipped");
	slFkt = NEXT;
	continue;
      }
      /*
      ** send that command
      */
      
      sendKriscomCommand(llHasId, pclActCmd, pclGrpTyp, pclFLNO, pclACTTOD, pclURNO, pclActTimer, llCFURNO);

      ilRC = updateTimerEntry(pclHRList, pclMinList, pclACTTOD, llHasId, pclEvent, ipIsEvent);
    }
    else {
      if (ilGetRc != NOTFOUND) {
	get_ora_err(ilGetRc, &clErrBuff[0]);
	dbg (TRACE, "<HandleShortTimerRequest> Error getting Oracle data error <%s>", &clErrBuff[0]);
	ilRC = RC_FAIL;
	break;
      }
      else {
	ilRC = RC_NODATA;
	break;
      }
    }
    slFkt = NEXT;
  }
  close_my_cursor (&slCursor);
}
/******************************************************************************/
/*  updateTimerEntry                                                          */
/*                                                                            */
/* The timer record is updated to the next valid entry. If the record is      */
/* expired, it will be removed.                                               */
/*                                                                            */
/* Input:                                                                     */
/*       pcpHRList  -- list of valid timers (in hours)                        */
/*       pcpMinList -- list of valid timers (in minutes)                      */
/*       pcpACTTOD  -- actual time of day                                     */
/*       lpHasId    -- ID of the record we are working on                     */
/*                                                                            */
/*                                                                            */
/******************************************************************************/
int updateTimerEntry(char *pcpHRList, char *pcpMinList, char *pcpACTTOD, long lpHasId, char *pcpEvent, int ipIsEvent)
{
  int  ilHRListEmpty = FALSE;
  int  ilMinListEmpty = FALSE;
  int  ilTimeIsHour = FALSE;
  int  ilTimeOffset;
  int  ilTime;
  int  ilTimeFact;
  int  ilType;
  int  ilPrio;
  int  ilCntDayTIFD, ilCntMinTIFD;
  int  ilCntDayServEnd, ilCntMinServEnd;
  int  ilWkDy;
  int  ilDiffMin;
  int  ilRC;
  int  ilHaveEvent = FALSE;
  int  ilActDay,ilActMin,ilActWkDy;
  char clACTTIMER[11];
  char *clFirstTimer;
  char clEvent[11];
  char *pclHRTIMLST;
  char *pclNextTimLst;
  char clUtcTimeStart[15];
  char clOSRQ[129];
  char clTimerData[16];

  if (strcmp(pcpEvent, " ")) ilHaveEvent = TRUE;
  clFirstTimer = getFirstTimerEntry(pcpHRList, &ilType, &pclNextTimLst);
  if (ilType == TIM) {
    ilTime = atoi(clFirstTimer);
    strcat(clFirstTimer,"H");
    ilPrio = 1;
    ilTimeFact = 60;
    if (pclNextTimLst == NULL) strcpy(pcpHRList, " ");
    else pcpHRList = pclNextTimLst;
  }  
  if (ilType == NONE) {
    clFirstTimer = getFirstTimerEntry(pcpMinList, &ilType, &pclNextTimLst);
    if (ilType == TIM) {
      ilTime = atoi(clFirstTimer);
      strcat(clFirstTimer,"M");
      ilTimeFact = 1;
      ilPrio = 5;
      if (pclNextTimLst == NULL) strcpy(pcpMinList, " ");
      else pcpMinList = pclNextTimLst;
    }
    if (ilType == NONE && ilHaveEvent == FALSE) {
      /*
      ** we didn't find further entries 
      ** so we assume that this record has completed
      ** remove the acttimer  
      */
      sprintf(pcgSqlBuf,"UPDATE SEVTAB SET STAT='DELETE' WHERE HAID='%ld'", lpHasId);
      dbg(DEBUG, "<updateTimerEntry> Setting record <%ld> to 'DELETE'", lpHasId);
      ilRC = getOrPutDBData(START|COMMIT);
      return FALSE;
    }
  }

  clEvent[0] = '\0';
  if (ilType != TIM) {
    if (ilType == NONE && ilHaveEvent) strcpy(clEvent, pcpEvent);
    else if (ilType == EVT) strcpy(clEvent, clFirstTimer);
    if (ilType == EVT || ilHaveEvent) {
      ilPrio = 5;
      ilType = EVT;
    }
  }
  if (ilType == TIM) {
    ilTimeOffset = ilTime * ilTimeFact;
    GetServerTimeStamp("UTC", 1, 0, clUtcTimeStart);
    DateDiffToMin(pcpACTTOD, clUtcTimeStart, &ilDiffMin);
    ilTime = ilDiffMin-ilTimeOffset;
    if (ilTime < 0) ilTime = 0;
    if (clFirstTimer == NULL) {
      strcpy(clTimerData, " ");
      dbg(TRACE, "<updateTimerEntry> Warning setting ATIM to ' ' <%ld>, shouldn't happen", lpHasId);
    }
    else strcpy(clTimerData, clFirstTimer);

    sprintf(pcgSqlBuf, "UPDATE SEVTAB SET TIMR='%d',TIHR='%s',TIMI='%s',PRIO='%d',ATIM='%s' WHERE HAID='%d'",
	    ilTime, pcpHRList, pcpMinList, ilPrio, clTimerData, lpHasId);
    dbg(DEBUG, "<updateTimerEntry> Sql statement: <%s>", pcgSqlBuf);
    ilRC = getOrPutDBData(START|COMMIT);
  }
  else if (ilType == EVT && ipIsEvent == FALSE) {
    GetServerTimeStamp("UTC", 1, 0, clUtcTimeStart);
    ilRC = GetFullDay(clUtcTimeStart,(int *)&ilActDay, (int *)&ilActMin, (int *)&ilActWkDy);
    sprintf(pcgSqlBuf, "UPDATE SEVTAB SET TIMR='0',TIHR=' ',TIMI=' ',PRIO='5',ATIM=' ',EVNT='%s',LSTU='%s',FIN='%d' WHERE HAID='%d'",
	    clEvent, clUtcTimeStart, ilActDay, lpHasId);
  }
  else { /* seems to be completed --> remove this entry */
    sprintf(pcgSqlBuf, "UPDATE SEVTAB SET STAT='DELETE' WHERE HAID='%ld'",lpHasId);
    dbg(DEBUG, "<updateTimerEntry> Going to delete completed entry <%ld>", lpHasId);
  }
  ilRC = getOrPutDBData(START|COMMIT);
  if (ilRC != RC_SUCCESS) {
    dbg(TRACE, "<updateTimerEntry> Unable to perform <%s>", pcgSqlBuf);  
    return TRUE;
  }
  return TRUE;
}

static char *cgMonthList [] = { "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC" };
/******************************************************************************/
/* sendKriscomCommand                                                         */
/*                                                                            */
/* Send a request to the KRISCOM system. Mark the record as 'RQPENDING'. The  */
/* sending context depends on the type of the group the request is dedicated  */
/* to.                                                                        */
/*                                                                            */
/* Input:                                                                     */
/*       plHasId     -- the id of the timer entry                             */
/*       pcpActCmd   -- the command to be send                                */
/*       pcpGrpTyp   -- the group type, selects the type of login             */
/*       pcpFLNO     -- the flight number, is used for selection              */
/*       pcpACTTOD   -- actual time of day, used to build the selection       */
/*                                                                            */
/* Return:                                                                    */
/*       status of the send command                                           */
/*                                                                            */
/******************************************************************************/
int sendKriscomCommand(long plHasId, char *pcpActCmd, char *pcpGrpTyp, char *pcpFLNO, char *pcpACTTOD, char *pcpURNO, char *pclActTimer, long llCFURNO)
{
  char  *pclName;
  char  *pclPasswd;
  char  *pclDecPasswd;
  char  *pclLogin;
  char  *pclLogout;
  char  *pclParamStr;
  char  *pclEndFormat;
  char  *pclNumPar;
  char  *pclPtr;
  char  *pclData;
  char  *pclString;
  char  *pclNPCmd;
  char  *pclNPKey;
  char  *pclNPLine;
  char  *pclNPCol;
  char  *pclLPKey;
  char  *pclLPLine;
  char  *pclLPCol;
  char  *pclGRPT;
  char  *pclNPCM;
  char  *pclLPCM;
  char  *pclALC2;
  char  *pclParam[32];
  ListPtr pclItemList = NULL;
  char  clName[33];
  char  clPasswd[33];
  char  clLogin[129];
  char  clLogout[65];
  char  clFlno[16];
  char  pclFields[1024];
  char  pclValues[1024];
  char  pclSelection[100];
  char  clSelection[100];
  char  pclActCmd[16];
  char  pclTwStart[64];
  char  clDate[6];
  char  clUser[32];
  char  clDay[3];
  char  clMonth[3];
  char  clParam[129];
  int   ilNumPar;
  int   ilMonth;
  int   ilRC = RC_SUCCESS;
  int   ilPrio=4;
  int   ilEntry=0;
  long  llURNO=0;
  int   ilCount;
  int   ilReceiver=0;
  char  clItemList[128];
  char  clBuffer[1024];
  char  clMyDay[30] = "\0";
  char *pclTmp = NULL;
  char *pclReceiver;
  char  clMessage[128];
  char pclACTTOD[16];

  strcpy(pclACTTOD,pcpACTTOD);

  llURNO = atol(pcpURNO);
  strcpy(pclActCmd, "KRI");
  sprintf(pcgTwEnd, "%s,%s,%s", cgHopo, cgTabEnd, cgProcessName);
  strcpy(clUser, "SQ");

  UtcToLocalTimeFixTZ(pclACTTOD); /* convert to local */

  strncpy(clMonth, pclACTTOD+4, 2);
  clMonth[2] = '\0';
  strncpy(clDay, pclACTTOD+6, 2);
  clDay[2] = '\0';
  strcpy(clDate, clDay);
  ilMonth = atoi(clMonth);
  strcat(clDate, cgMonthList[ilMonth-1]);
  ConvertClientStringToDb(pcpActCmd);
  sprintf(pcgSqlBuf,"SELECT CMDN,NPGK,NPGL,NPGC,LPGK,LPGL,LPGC,GRPT,ALC2 FROM HCFTAB WHERE UHAT=%ld AND COMD='%s'", llCFURNO, pcpActCmd);
  ConvertDbStringToClient(pcpActCmd);
  ilRC = getOrPutDBData(START);
  if (ilRC != RC_SUCCESS) {
    dbg(TRACE, "<sendKriscomCommand> Unable to perform <%s>", pcgSqlBuf);
    return RC_FAIL;
  }

  BuildItemBuffer(pcgDataArea, NULL, 9, ";");

  strcpy(clBuffer, pcgDataArea);
  pclNPCmd = strtok(clBuffer, ";");
  pclNPKey = strtok(NULL, ";");
  pclNPLine = strtok(NULL, ";");
  pclNPCol = strtok(NULL, ";");
  pclLPKey = strtok(NULL, ";");
  pclLPLine = strtok(NULL, ";");
  pclLPCol = strtok(NULL, ";");
  pclGRPT  = strtok(NULL, ";");
  pclALC2  = strtok(NULL, ";");
  
  sprintf(pclSelection, "%ld;%s;%s;%s;%s;%s;%s;%s;%s;%s %s %s",
	  llURNO, pcpActCmd, pclNPCmd, pclNPKey, pclNPCol, pclNPLine, pclLPKey, pclLPCol, pclLPLine, pcpFLNO, pclACTTOD, pcpActCmd);
  
/*  strcpy(clItemList, "NAME,NUMPAR,PASW,LGIN,LGOU"); */
  strcpy(clItemList, "NAME,PASW,LGIN,LGOU");
  sprintf(pcgSqlBuf, "SELECT %s FROM HATTAB WHERE URNO=%ld", clItemList, llCFURNO);
  ilRC = getOrPutDBData(START);
  if (ilRC == RC_SUCCESS) {
    BuildItemBuffer(pcgDataArea, NULL, 4, ";");
    createItemList(clItemList, &pcgItemList);
    addDataItemToList(pcgDataArea, &pcgItemList, 0, ';', IS_VALID);

    pclName = getItemDataByName(pcgItemList, "NAME", 0);
    /*
    ** Mandatory user
    */
    if (pclName == NULL) {
      dbg(TRACE, "<sendKriscomCommand>  Unable to fetch user for login <%ld>", llCFURNO);
      deleteItemList(pcgItemList);
      pcgItemList = NULL;
      return RC_FAIL;
    }
    else {
      sprintf(clUser, "%s", pclName);
    }
    /*
    ** Mandatory parameter count
    */
/* Not used
    pclNumPar = getItemDataByName(pcgItemList, "NUMPAR", 0);
    if (pclNumPar == NULL) {
      dbg(TRACE, "<sendKriscomCommand>  Unable to fetch parameter count");
      return RC_FAIL;
    }
    else ilNumPar = atoi(pclNumPar);
*/
    /*
    ** Mandatory login string
    */
    pclLogin = getItemDataByName(pcgItemList, "LGIN", 0);
    if (pclLogin == NULL) {
      dbg(TRACE, "<sendKriscomCommand>  Unable to fetch login string <%ld>", llCFURNO);
      deleteItemList(pcgItemList);
      pcgItemList = NULL;
      return RC_FAIL;
    }
    else sprintf(clLogin, "%s", pclLogin);
    /*
    ** Mandatory logout string
    */
    pclLogout = getItemDataByName(pcgItemList, "LGOU", 0);
    if (pclLogout == NULL) {
      dbg(TRACE, "<sendKriscomCommand>  Unable to fetch logout string <%ld>", llCFURNO);
      deleteItemList(pcgItemList);
      pcgItemList = NULL;
      return RC_FAIL;
    }
    else sprintf(clLogout, "%s", pclLogout);
    if ((pclPtr = strstr(clLogin, "PASS")) != NULL) {
      pclPasswd = getItemDataByName(pcgItemList, "PASW", 0);
      if (pclPasswd == NULL) {
	dbg(TRACE, "<sendKriscomCommand>  Unable to fetch password <%ld>", llCFURNO);
	deleteItemList(pcgItemList);
	pcgItemList = NULL;
	return RC_FAIL;
      }
      else {
	strcpy (clPasswd, pclPasswd);
	decrypt(clPasswd);
      }
    }

    /* scan embedded params */
    ConvertDbStringToClient(clLogout);
    ConvertDbStringToClient(clLogin);
    selDataAndParam(clLogin, clParam, "{=", "=}");

    pclData = strtok(clParam, ",");

    ilEntry = 0;
    while (*pcpFLNO != '\0') {
      if (isalpha(*pcpFLNO) || isdigit(*pcpFLNO)) {
	clFlno[ilEntry] = *pcpFLNO;
	ilEntry++;
      }
      pcpFLNO++;
    }
    clFlno[ilEntry] = '\0';

    ilEntry = 0;
    while (pclData != NULL) {
      ilCount = TRUE;
      if (!strcmp(pclData, "USER")) pclParam[ilEntry] = clUser;
      else if (!strcmp(pclData, "FLNO")) pclParam[ilEntry] = clFlno;
      else if (!strcmp(pclData, "DATE")) pclParam[ilEntry] = clDate;
      else if (!strcmp(pclData, "PASS")) pclParam[ilEntry] = clPasswd;
      else {
	dbg(TRACE, "<sendKriscomCommand> Unknown parameter <%s>", pclData);
	ilCount = FALSE;
      }
      if (ilCount == TRUE) ilEntry++;
      pclData = strtok(NULL, ",");
    }

    switch (ilEntry) {
    case 1:
      sprintf(pclValues, clLogin, pclParam[0]);
      break;
    case 2:
      sprintf(pclValues, clLogin, pclParam[0], pclParam[1]);
      break;
    case 3:
      sprintf(pclValues, clLogin, pclParam[0], pclParam[1], pclParam[2]); 
      break;
    case 4:
      sprintf(pclValues, clLogin, pclParam[0], pclParam[1], pclParam[2], pclParam[3]);
      break;
    case 5:
      sprintf(pclValues, clLogin, pclParam[0], pclParam[1], pclParam[2], pclParam[3], pclParam[4]);
      break;
    default:
      dbg(TRACE, "<sendKriscomCommand> Number of parameters not supported <%d>", ilEntry);
    }
    /*  sprintf(pclValues, "BSIA%s/GS\nUC\n*%s/%s/%s\n", clUser, pcpFLNO, clDate, cgHopo);*/
    
    sprintf(pclFields, "%s", clLogout);
    sprintf(pclTwStart,"hashdl,%d,%s", plHasId, pclActTimer);
    
    deleteItemList(pcgItemList);
    pcgItemList = NULL;
    
    if (pclGRPT != NULL) {
      if (!strcmp(pclGRPT, "GRP1")) {
	ilReceiver = pcgGrp1Modid;
      }
      else if (!strcmp(pclGRPT, "GRP2")) {
	pclReceiver = getItemDataByName(pcgGrp2Airlines, pclALC2, 0);
	if (pclReceiver != NULL) ilReceiver = atoi(pclReceiver);
      }
      else if (!strcmp(pclGRPT, "GRP3")) {
	ilReceiver = pcgGrp3Modid;
      }
    }
    
    if (ilReceiver == 0) {
      sprintf(clMessage, "No receiver configured for airline <%s> in group <%s>", pclALC2, pclGRPT);
      alert(clMessage);
      return RC_FAIL;
    }

    pclTmp = strstr(pclSelection,"{=");
    if (pclTmp != NULL)
    {
      sprintf(clSelection, "%s", pclSelection);
      clParam[0]= '\0';
      selDataAndParam(clSelection, clParam, "{=", "=}");
      pclData = strtok(clParam, ",");
	
      strcpy(clMyDay,clDate);
      clMyDay[2] = '\0';
	
      ilEntry = 0;
      while (pclData != NULL) {
	ilCount = TRUE;
	if (!strcmp(pclData, "USER")) pclParam[ilEntry] = clUser;
	else if (!strcmp(pclData, "FLNO")) pclParam[ilEntry] = clFlno;
	else if (!strcmp(pclData, "DATE")) pclParam[ilEntry] = clDate;
	else if (!strcmp(pclData, "PASS")) pclParam[ilEntry] = clPasswd;
	else if (!strcmp(pclData, "DAY")) pclParam[ilEntry] = clDay;
	else {
	  dbg(TRACE, "<sendKriscomCommand> Unknown parameter <%s>", pclData);
	  ilCount = FALSE;
	}
	if (ilCount == TRUE) ilEntry++;
	pclData = strtok(NULL, ",");
      }
      
      switch (ilEntry) {
      case 1:
	sprintf(pclSelection, clSelection, pclParam[0]);
	break;
      case 2:
	sprintf(pclSelection, clSelection, pclParam[0], pclParam[1]);
	break;
      case 3:
	sprintf(pclSelection, clSelection, pclParam[0], pclParam[1], pclParam[2]); 
	break;
      case 4:
	sprintf(pclSelection, clSelection, pclParam[0], pclParam[1], pclParam[2], pclParam[3]);
	break;
      case 5:
	sprintf(pclSelection, clSelection, pclParam[0], pclParam[1], pclParam[2], pclParam[3], pclParam[4]);
	break;
      default:
	dbg(TRACE, "<sendKriscomCommand> Number of parameters not supported <%d>", ilEntry);
      }
    }

    dbg(TRACE,"pclValues <%s>",pclValues);

    /*    strcpy(pclSelection,pclValues); */

    dbg(TRACE,"pclSelection: <%s>",pclSelection);

    ilRC = SendCedaEvent(ilReceiver, 0, pcgDestName, pcgRecvName, pclTwStart, pcgTwEnd,
			 pclActCmd, "", pclSelection,
			 pclFields, pclValues, "", ilPrio, RC_SUCCESS) ;
    
    if (ilRC == RC_SUCCESS && plHasId != 0) { /* don't update for external commands */
      /*
      **sprintf(pcgSqlBuf, "UPDATE SEVTAB SET TIMR='1',STAT='RQPENDING' WHERE HAID='%d'", plHasId);
      **ilRC = getOrPutDBData(START|COMMIT);
      **if (ilRC != RC_SUCCESS) {
      **dbg(TRACE, "<sendKriscomCommand> Unable to perform <%s>", pcgSqlBuf);  
      **return TRUE;
      **}
      */
    }
  }
  else {
    dbg(TRACE, "<sendKriscomCommand> Unable to fetch user data for ident <%ld>", llURNO);
    return RC_FAIL;
  }
  dbg(DEBUG, "<sendKriscomCommand> \n ****** Send command report ****** \nSending command: <%s> to <%d>\nlogin: \n<%s> \nlogout: \n<%s> \n ****** End send command report ****** ", pclSelection, ilReceiver, pclValues, clLogout); 
}
/******************************************************************************/
/* selDataAndParam                                                            */
/*                                                                            */
/* Scans a login string and  moves {= XXX =} items to pcpParam (',' separated)*/
/* Substitutes  {= XXX =} with %s (for sprintf)                               */
/*                                                                            */
/* Input:                                                                     */
/*       pcpLogin   -- login string                                           */
/*       pcpParam   -- resulting parameter list                               */
/*       pcpBegSep  -- beginning separator                                    */
/*       pcpEndSep  -- ending separator                                       */
/*                                                                            */
/******************************************************************************/
static void  selDataAndParam(char *pcpLogin, char *pcpParam, char *pcpBegSep, char *pcpEndSep)
{
  char *pclStartData;
  char *pclEndData;
  char *pclStartParam;
  char *pclEndParam;
  int  ilFirst = TRUE;
  char clNewLogin[128];
  char clParam[128];

  strcpy(clNewLogin, pcpLogin);
  strcpy(clParam, "");

  pclStartData = clNewLogin;
  while ((pclStartParam = strstr(pclStartData, "{=")) != '\0') {
    pclEndData = pclStartParam;
    pclStartParam++;
    pclStartParam++;
    pclEndParam = strstr(pclStartData, "=}");
    *pclEndData = '\0';
    *pclEndParam = '\0';
    if (ilFirst == TRUE) {
      strcpy(clNewLogin,pclStartData);
      strcpy(clParam,pclStartParam);
      strcat(clNewLogin,"%s");
      ilFirst = FALSE;
    }
    else {
      strcat(clNewLogin,pclStartData);
      strcat(clParam,",");
      strcat(clParam,pclStartParam);
      strcat(clNewLogin,"%s");
    }
    pclStartData = pclEndParam + 2;
  }
  if (pclStartData != '\0') strcat(clNewLogin,pclStartData);

  strcpy(pcpLogin,clNewLogin);
  strcpy(pcpParam,clParam);
}
/******************************************************************************/
/* decrypt                                                                    */
/*                                                                            */
/* A high sophisticated password decrytion function.....                      */
/* encryption is done by the HAS Config Tool!                                 */
/*                                                                            */
/* Input:                                                                     */
/*       pcpPasswd -- password string to be "decrypted"                       */
/*                                                                            */
/******************************************************************************/
static void decrypt(char *pcpPasswd)
{
  int ilOffset = 42;
  char *clPtr;

  clPtr = pcpPasswd;
  while (*clPtr != NULL) {
    *clPtr = *clPtr - ilOffset;
    clPtr++;
  }
}
/******************************************************************************/
/* HandleLongTimerRequest                                                     */
/*                                                                            */
/* Triggered by ntisch every 10 minutes. Scan database for records with       */
/* ORG3 = HOPO and TIFD within a 10 minute time window 72 hours before TIFD.  */
/* If the ALC2 is configured, this function will make an entry in the timer   */
/* table for scheduling commands related to this record.                      */
/*                                                                            */
/******************************************************************************/
static int HandleLongTimerRequest(int ipInit) 
{
  char  clUtcTimeStart[15];
  char  clUtcTimeEnd[15];
  short slFkt = 0;
  short slCursor = 0;
  int   ilGetRc;
  int   ilRC = RC_SUCCESS;
  long  llURNO;
  char  *pclURNO;
  char  *pclALC2;
  char  *pclTIFD;
  char  *pclFLNO;
  char  clALC2[3];
  char  clTIFD[15];
  char  clFLNO[10];
  int   ilTimeOffset = 0;
  int   ilTimeOffsetEnd = 0;
  char  clErrBuff[128];
  char  pclSqlBuf[1024];

  if (ipInit == TRUE) {
    ilTimeOffset = 72 * 3600;
    GetServerTimeStamp("UTC", 1, ilTimeOffset, clUtcTimeEnd);
    GetServerTimeStamp("UTC", 1, 0, clUtcTimeStart);
  }
  else {
    ilTimeOffset = 72 * 3600 + 120;  /* give me 2 minutes of preparation please */ 
    ilTimeOffsetEnd = 60 * igTimeWindow;
    GetServerTimeStamp("UTC", 1, ilTimeOffset, clUtcTimeStart);
    ilTimeOffset = ilTimeOffset + ilTimeOffsetEnd;
    GetServerTimeStamp("UTC", 1, ilTimeOffset, clUtcTimeEnd);
  }

  sprintf (pclSqlBuf,"SELECT URNO,ALC2,TIFD,FLNO FROM AFTTAB WHERE TIFD BETWEEN '%s' AND '%s' AND ORG3='%s' AND ADID='D'" \
	   , clUtcTimeStart, clUtcTimeEnd, cgHopo);
  slFkt = START;
  slCursor = 0;
  while ((ilGetRc = sql_if (slFkt, &slCursor, pclSqlBuf, pcgDataArea)) == DB_SUCCESS) {
    if (ilGetRc == DB_SUCCESS) {
      BuildItemBuffer(pcgDataArea, NULL, 4, ",");
      pclURNO = strtok(pcgDataArea, ",");
      if (pclURNO == NULL) {
	dbg(TRACE, "<HandleLongTimerRequest> No URNO -- data skipped");
	continue;
      }
      llURNO = atol(pclURNO);
      /* ALC2 */
      pclALC2 = strtok(NULL, ",");
      if (pclALC2 == NULL) {
	dbg(TRACE, "<HandleLongTimerRequest> No ALC2 -- data skipped");
	continue;
      }
      strcpy(clALC2, pclALC2);
      /* TIFD */
      pclTIFD = strtok(NULL, ",");
      if (pclTIFD == NULL) {
	dbg(TRACE, "<HandleLongTimerRequest> No TIFD -- data skipped");
	continue;
      }
      strcpy(clTIFD, pclTIFD);
      /* FLNO */
      pclFLNO = strtok(NULL, ",");
      if (pclFLNO == NULL) {
	dbg(TRACE, "<HandleLongTimerRequest> No FLNO -- data skipped");
	continue;
      }
      strcpy(clFLNO, pclFLNO);
      addTimerEntry(llURNO, clALC2, clTIFD, clFLNO);
    }
    else {
      if (ilGetRc != NOTFOUND) {
	get_ora_err(ilGetRc, &clErrBuff[0]);
	dbg (TRACE, "<HandleLongTimerRequest> Error getting Oracle data error <%s>", &clErrBuff[0]);
	ilRC RC_FAIL;
	break;
      }
      else {
	ilRC = RC_NODATA;
	break;
      }
    }
    slFkt = NEXT;
  }
  close_my_cursor (&slCursor);
  return ilRC;
}

/******************************************************************************/
/* addTimerEntry                                                              */
/*                                                                            */
/* Add an entry to the timer table.                                           */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/******************************************************************************/
int addTimerEntry(long lpFLURNO, char *cpALC2, char *cpStartTime, char *cpFLNO)
{
  int  ilMinutes = 0;
  int  ilTime = 0;
  int  ilRC;
  int  ilGetRc;
  int  ilType;
  int  ilTimer;
  long llHASIDENT;
  int  ilCntDayTIFD, ilCntMinTIFD;
  int  ilCntDayAct, ilCntMinAct;
  int  ilWkDy; 
  int  ilHaveEvent = FALSE;
  char clActTime[15];
  char clValues [2048];
  char clFields [2048];
  char clACTTIMER[11];
  char *clFirstTimer;
  char clEvent[11];
  char *pclGRPTYP;
  char *pclCFURNO;
  char *pclACTCMD;
  char *pclHRTIMLST;
  char *pclMNTIMLST;
  char *pclNextTimLst;
  char *pclCFSERIAL;
  char *pclEvent;
  char clHRTIMLST[65];
  char clMNTIMLST[65];
  int   ilPrio=0;
  char  pclSqlBuf[1024];
  char  pclSqlBuf1[1024];
  short slFkt;
  short slCursor = 0;
  char  clNum[15];
  char  clTimerHR[65];
  char  clTimerMin[65];
  char  clActTimer[16];
  char  pclDataArea[DATABLK_SIZE];
  /*
  ** first check, if there is an entry for this flight already
  */
  sprintf(pcgSqlBuf,"SELECT UAFT FROM SEVTAB WHERE UAFT='%ld'", lpFLURNO);
  ilRC = getOrPutDBData(START);
  if (ilRC == RC_SUCCESS) {
    dbg(TRACE,"<addTimerEntry> This flight <%ld> already has an timer entry -- skipping", lpFLURNO);
    return RC_SUCCESS;
  }
  if (isValidALC2(cpALC2) != TRUE) {
    dbg(DEBUG,"<addTimerEntry> Airline <%s> not supported -- skipping", cpALC2);
    return RC_FAIL;
  }
  dbg(DEBUG,"<addTimerEntry> Adding timer for airline <%s>", cpALC2);
  /*
  ** initialize variables 
  */
  strcpy(clTimerHR, " ");
  strcpy(clTimerMin, " ");
  clEvent[0] = '\0';
  /*
  ** calculate minutes until first start
  */
  /* Add entry */
  ilRC = GetFullDay(cpStartTime, &ilCntDayTIFD, &ilCntMinTIFD, &ilWkDy);
  /*      ilRC = GetFullDay(clUtcTimeStart, &ilCntDayServ, &ilCntMinServ, &ilWkDy); */
  GetServerTimeStamp("UTC", 1, 0, clActTime);
  ilRC = GetFullDay(clActTime, &ilCntDayAct, &ilCntMinAct, &ilWkDy); 

  sprintf(pclSqlBuf,"SELECT GRPT,UHAT,COMD,TIHR,TIMI,SERL,EVNT FROM HCFTAB WHERE ALC2='%s' AND STAT='ACTIVE'", cpALC2);
  slFkt = START;
  slCursor = 0;
  while ((ilGetRc = sql_if (slFkt, &slCursor, pclSqlBuf, pclDataArea)) == DB_SUCCESS) {
    if (ilGetRc != RC_SUCCESS) {
      dbg(TRACE, "<addTimerEntry> Airline not configured <%s>", cpALC2);
      close_my_cursor (&slCursor);
      return RC_FAIL;
    }
    else {
      BuildItemBuffer(pclDataArea, NULL, 7, ";");
      pclGRPTYP = strtok(pclDataArea, ";");
      pclCFURNO = strtok(NULL, ";");
      pclACTCMD = strtok(NULL, ";");
      pclHRTIMLST = strtok(NULL, ";");
      pclMNTIMLST = strtok(NULL, ";");
      pclCFSERIAL = strtok(NULL, ";");
      pclEvent = strtok(NULL, ";");

      strcpy(clHRTIMLST, pclHRTIMLST);
      strcpy(clMNTIMLST, pclMNTIMLST);
      strcpy(clEvent, pclEvent);
      if (strcmp(clEvent, " ")) ilHaveEvent = TRUE;

      dbg(DEBUG, "<addTimerEntry>\nCommand: <%s>\nHR-Timer: <%s>\nMin-Timer: <%s>\nDeparture-Time: <%s>",
	  pclACTCMD, pclHRTIMLST, pclMNTIMLST, cpStartTime);
      clFirstTimer = getFirstActTimerEntry(pclHRTIMLST, &ilType, &pclNextTimLst, TRUE, ilCntMinAct, ilCntMinTIFD);
      if (ilType == TIM) {
	ilTime = atoi(clFirstTimer) * 60;
	strcat(clFirstTimer,"H");
	pclHRTIMLST = pclNextTimLst;
	ilPrio = 1;
      }  
      if (ilType == NONE) {
	clFirstTimer = getFirstActTimerEntry(pclMNTIMLST, &ilType, &pclNextTimLst, FALSE, ilCntMinAct, ilCntMinTIFD);
	if (ilType == TIM) {
	  ilTime = atoi(clFirstTimer);
	  strcat(clFirstTimer,"M");
	  ilPrio = 5;
	}
	if (ilType == NONE && ilHaveEvent == FALSE) {
	  dbg(TRACE, "<addTimerEntry> Fatal no valid timer specified");
	  slFkt = NEXT;
	  continue;
	}
	pclMNTIMLST = pclNextTimLst;
      }

      if (ilType == EVT) strcpy(clEvent, clFirstTimer);
      if (ilType == EVT || ilHaveEvent) {
	strcpy(clActTimer, " ");
	ilPrio = 5;
      }

      if (ilType == TIM) {
	if (pclHRTIMLST == NULL) strcpy(clTimerHR, " ");
	else strcpy(clTimerHR, pclHRTIMLST);
	
	if (pclMNTIMLST == NULL) strcpy(clTimerMin, " ");
	else strcpy(clTimerMin, pclMNTIMLST);

	if (clFirstTimer != NULL) strcpy(clActTimer, clFirstTimer);
	else strcpy(clActTimer, " ");

	ilMinutes = (ilCntMinTIFD - ilTime) - ilCntMinAct;
	if (ilMinutes<0) ilMinutes = 0; 
	dbg(DEBUG, "<addTimerEntry> <%s> Timer for request <%s> will start in <%d> minutes", clFirstTimer, cpStartTime, ilMinutes);
      }

      llHASIDENT = GetSequence("KRISCOM");
      PutSequence("KRISCOM", llHASIDENT+1);

      sprintf(clFields, "ALC2,GRPT,TIMT,ACMD,ATIM,TIMR,EVNT,UAFT,ATOD,STAT,WCMD,HAID,UHAT,CFSE,TIHR,TIMI,PRIO,FLNO,OSRQ");
      sprintf(clValues,"'%s','%s','%d','%s','%s','%d','%s','%d','%s','ACTIVE','0','%d','%s','%s','%s','%s','%d','%s',' '",
	      cpALC2, pclGRPTYP, ilType, pclACTCMD, clActTimer, ilMinutes, clEvent, lpFLURNO, cpStartTime, llHASIDENT,
	      pclCFURNO, pclCFSERIAL, clTimerHR, clTimerMin, ilPrio, cpFLNO);
      sprintf(pcgSqlBuf,"INSERT INTO SEVTAB (%s) VALUES(%s)", clFields, clValues);
      ilRC = getOrPutDBData(START|COMMIT);
      if (ilRC != RC_SUCCESS) {
	dbg(TRACE, "<addTimerEntry> Unable to activate timer SQL statement:\n<%s>", pcgSqlBuf);
	slFkt = NEXT;
	continue;
      }
    }
    slFkt = NEXT;
  }
  close_my_cursor (&slCursor);
}

/******************************************************************************/
/* getFirstActTimerEntry                                                      */
/*                                                                            */
/* Fetch the first entry from the list of timer entries. Try to figure out,   */
/* what kind of entry was found and advance to the next one if an entry is    */
/* already expired.                                                           */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/******************************************************************************/
static char *getFirstActTimerEntry(char *pcpEntries, int *ipType, char **pcpNextEntry, int ipHour, int ipMinAct, int ipMinStart)
{
  char *pclEntry;
  int  ilTime;
  int  ilFound=FALSE;
  char clEntryList[128];
  int  test = TRUE;

  /* while (test == TRUE); */

  pclEntry = getFirstTimerEntry(pcpEntries, ipType, pcpNextEntry);

  while (ilFound == FALSE) {
    if (!strcmp(pclEntry, "")) return "";
    else if (*ipType == EVT || *ipType == NONE) return pclEntry;
    
    /*
    ** check if timer entry is expired 
    */
    ilTime = atoi(pclEntry);
    if (ipHour) ilTime = ilTime * 60;
    if ((ipMinStart - ilTime) - ipMinAct > 0) {
      ilFound = TRUE;
      return pclEntry;
    }
    if (*pcpNextEntry == NULL) {
      if (ipHour == FALSE) return pclEntry;
      else {
	*ipType = NONE; /* force minute list scanning */
	return "";
      }
    }
    strcpy(clEntryList, *pcpNextEntry);
    pclEntry = getFirstTimerEntry(clEntryList, ipType, pcpNextEntry);
    if (*pcpNextEntry != NULL) {
      dbg(DEBUG, "<getFirstActTimerEntry> Next: <%s> Act: <%s> Remaining: <%s>", pclEntry, clEntryList, *pcpNextEntry); 
    }
    else {
      dbg(DEBUG, "<getFirstActTimerEntry> Next: <%s> Act: <%s> Remaining: <>", pclEntry, clEntryList);
    }
  }
}
/******************************************************************************/
/* getFirstTimerEntry                                                         */
/*                                                                            */
/* Fetch the first entry from the list of timer entries. Try to figure out,   */
/* what kind of entry was found.                                              */
/*                                                                            */
/*                                                                            */
/*                                                                            */
/******************************************************************************/
static char *getFirstTimerEntry(char *pcpEntries, int *ipType, char **pcpNextEntry)
{
  char *pclData;
  char *pclEnd;
  char *pclEndSav;
  static  char clSave[64];
  int  ilEventReq = FALSE;
  int  ilFirst = FALSE;
  int  entry = 0;

  *ipType = NONE;
  ConvertDbStringToClient(pcpEntries);

  if (!strcmp(pcpEntries, "") || !strcmp(pcpEntries, " ")) {
    *pcpNextEntry = NULL;
    return "";
  }

  pclData = pcpEntries;
  pclEnd = strchr (pcpEntries, ',');

  if (pclEnd != NULL) pclEnd++;

  if (pclData != NULL) {
    strcpy(clSave, pclData);
    pclEndSav = strchr (clSave, ',');
    if (pclEndSav != NULL) *pclEndSav = '\0';
  }

  *pcpNextEntry = pclEnd; /* point to next entry if any */

  entry = 0;
  while(cgValidEvents[entry] != NULL) {
    if (!strcmp(cgValidEvents[entry], clSave)) {
      *ipType = EVT;
      return clSave;
    }
    entry++;
  }
  entry = 0;
  while(clSave[entry] != '\0') {
    if (isalpha(clSave[entry])) {
      *ipType = NONE;
      break;
    }
    else *ipType = TIM;
    entry++;
  }
  return clSave;
}

int getMinutesToStart(char *cpActTime, char *cpStartTime)
{
  int ilStartMin = 0;
  int ilActMin = 0;
  int ilDummy = 0;

  GetFullDay(cpStartTime, &ilDummy,&ilStartMin,&ilDummy);
  GetFullDay(cpActTime, &ilDummy,&ilActMin,&ilDummy);
  return ilStartMin-ilActMin;
}
/******************************************************************************/
/* isValidALC2                                                                */
/*                                                                            */
/* Check if the airline is contained in the list of configured airlines.      */
/*                                                                            */
/* Input:                                                                     */
/*       cpALC2 -- points to the ALC2 to check                                */
/*                                                                            */
/* Returns:                                                                   */
/*       TRUE/FALSE                                                           */
/*                                                                            */
/******************************************************************************/
int isValidALC2(char *cpALC2)
{
  int ilRC;

  sprintf(pcgSqlBuf,"SELECT ALC2 FROM HCFTAB WHERE ALC2='%s'", cpALC2);
  ilRC = getOrPutDBData(START);
  if (ilRC == RC_SUCCESS) return TRUE;
  else return FALSE;
}


int addTimeOffset(char cpType, int ipUnits, char *pclTimeBuffer)
{
  char clNum[3];
  int  ilNum;
  int  ilPos = 0;

  if (cpType == 'H') ilPos = 8;
  else if (cpType == 'M') ilPos = 10;
  else if (cpType == 'S') ilPos = 12;
  else {
    dbg(TRACE, "<addTimeOffset> Invalid type <%c>", cpType);
    return RC_FAIL;
  }
  strncpy(clNum, &pclTimeBuffer[ilPos], 2);
  ilNum = atoi(clNum);
  ilNum = ipUnits + ilNum;
  sprintf(clNum, "%d", ilNum);
  strncpy(&pclTimeBuffer[ilPos], clNum, 2);
}
/******************************************************************************/
/* getOrPutDBData                                                             */
/*                                                                            */
/* Fetch data as described in the global pcgSqlBuf and return the result in   */
/* global buffer pcgDataArea.                                                 */
/*                                                                            */
/* Input:                                                                     */
/*        ipMode -- function mode mask like START|COMMIT                      */
/*                                                                            */
/******************************************************************************/
int getOrPutDBData(uint ipMode)
{
  int ilRC = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  short slFkt = 0;
  short slCursor = 0;
  char errBuff[128];

  slFkt = ipMode;
  slCursor = 0;
  ilGetRc = sql_if (slFkt, &slCursor, pcgSqlBuf, pcgDataArea);
  close_my_cursor (&slCursor);
  if (ilGetRc == DB_SUCCESS) {
    return RC_SUCCESS;
  }
  else {
    if (ilGetRc != NOTFOUND) {
      get_ora_err(ilGetRc, &errBuff[0]);
      dbg (TRACE, "<getOrPutDBData> Error getting Oracle data error <%s>", &errBuff[0]);
      return RC_FAIL;
    }
    else  return RC_NODATA;
  }
}
/******************************************************************************/
/* Get a unique identifier (MCU)                                              */
/******************************************************************************/
static void PutSequence(char *pcpKey,long lpSequence)
{
	int ilRc;

	short slCursor = 0;
	char clSqlBuf[200];
	char clDataArea[256];

	sprintf(clSqlBuf,"UPDATE NUMTAB SET ACNU='%10d' WHERE KEYS='%s' AND HOPO='%s'"
		,lpSequence,pcpKey,cgHopo);
	ilRc = sql_if(START,&slCursor,clSqlBuf,clDataArea);
	if(ilRc == RC_NOTFOUND) {
	  /* sequence not yet written, create new one */
	  sprintf(clSqlBuf,"INSERT INTO NUMTAB VALUES(%d,' ','%s','%s',9999999999,1)",
		  lpSequence,cgHopo,pcpKey);
	  close_my_cursor(&slCursor);
	  slCursor = 0;
	  ilRc = sql_if(START,&slCursor,clSqlBuf,clDataArea);
	  if (ilRc != RC_SUCCESS) {
	    /* there must be a database error */
	    get_ora_err (ilRc, cgErrMsg);
	    dbg(TRACE,"Inserting Sequence <%s> failed RC=%d",pcpKey,ilRc);
	  }
	}
	else if (ilRc != RC_SUCCESS) { 
	  /* there must be a database error */
	  get_ora_err(ilRc, cgErrMsg);
	  dbg(TRACE,"Inserting Sequence <%s> failed RC=%d",pcpKey,ilRc);
	}
	commit_work();
	close_my_cursor(&slCursor);
	slCursor = 0;
}


static long GetSequence(char *pcpKey)
{
	int ilRc;
	long llSequence = 0;
	short slCursor = 0;
	char clSqlBuf[200];
	char clDataArea[256];

	sprintf(clSqlBuf,"SELECT ACNU FROM NUMTAB WHERE KEYS='%s' AND HOPO='%s'",
			pcpKey,cgHopo);
	ilRc = sql_if(START,&slCursor,clSqlBuf,clDataArea);
	if(ilRc == RC_SUCCESS) {
	  llSequence = atol(clDataArea);
	}
	else if(ilRc == RC_NOTFOUND) {
	  PutSequence(pcpKey,0);
	  llSequence = 0;
	} 
	else { 
	  /* there must be a database error */
	  get_ora_err (ilRc, cgErrMsg);
	  dbg(TRACE,"Reading Sequence <%s> failed RC=%d",pcpKey,ilRc);
	}
	close_my_cursor(&slCursor);
	slCursor = 0;

	return llSequence;
}

/******************************************************************************/
/* Data Storage                                                               */
/******************************************************************************/

/******************************************************************************/
/* createItemList  	                                                      */
/*                                                                            */
/* Add new field items to the list, scan the item list and create a new entry */
/* for each item.                                                             */
/*                                                                            */
/* Input:                                                                     */
/*        list       -- pointer to data list containing the field items       */
/*        hook       -- pointer to list where the element is to add           */
/*                                                                            */
/* returns:                                                                   */
/*        nothing                                                             */
/*                                                                            */
/******************************************************************************/
static int createItemList(char *list, ListPtr *hook)
{
  char *pclElement;
  
  if (strlen(list) == 0) return RC_FAIL;
  if ((pclElement=strtok(list, ","))==NULL) {
    addItemToList(pclElement, hook);
    return;
  }
  addItemToList(pclElement, hook);
  while ((pclElement = strtok(NULL, ",")) != NULL) {
    addItemToList(pclElement, hook);
  }
}
/******************************************************************************/
/* addDataItemToList  	                                                      */
/*                                                                            */
/* Add new items to the list, allocate required memory, the field list needs  */
/* already exist. The list of data has to correspond to the field list        */
/*                                                                            */
/* Input:                                                                     */
/*        list       -- pointer to data list containing the data items        */
/*        hook       -- pointer to list where the element is to add           */
/*        entry      -- the number of the related leg                         */
/*                                                                            */
/* returns:                                                                   */
/*        nothing                                                             */
/*                                                                            */
/******************************************************************************/
static int addDataItemToList(char *list, ListPtr *hook, uint entry, char delimiter, uint defaultStatus)
{
  char *pclElement;      /* pointer to start of element */
  char *pclActElement;   /* pointer to actual element   */
  uint ilLenOfStr;
  uint ilMaxLen;
  uint ilLastElem = FALSE;
  uint ilNumItems = 0;
  ListPtr pclPtr;
  ListPtr pclRemPtr;

  pclElement = list;
  pclPtr = *hook;
  while (*pclElement != NULL && pclPtr != NULL) {
    pclActElement = getNextItem(pclElement, &delimiter, &ilLenOfStr, &ilLastElem);
    if (ilLastElem == FALSE) {
      pclElement = pclElement + ilLenOfStr + 1;
    }
    else {
      pclElement = pclElement + ilLenOfStr;
    }
    if (ilLenOfStr != 0) {
      ilMaxLen = ilLenOfStr + 1;
      pclPtr->dobj[entry].data = malloc(ilMaxLen);
      pclPtr->dobj[entry].len = ilMaxLen;
      pclPtr->dobj[entry].status = defaultStatus;
      strncpy(pclPtr->dobj[entry].data,pclActElement,ilMaxLen-1);
      pclPtr->dobj[entry].data[ilLenOfStr] = '\0';
      ilNumItems++;
    }
    else {
      pclRemPtr = pclPtr;
      removeItemFromList(pclRemPtr);
    }
    pclPtr = pclPtr->next;
  }
  dbg(DEBUG,"addDataItemToList: Added <%d> items to list", ilNumItems);
}  

/******************************************************************************/
/* getNextItem                                                                */
/*                                                                            */
/* Helper function to fetch the next item from a ',' separated list. If the   */
/* last item was fetched, ipLast will be set to TRUE.                         */
/*                                                                            */
/* Input:                                                                     */
/*      cpSource -- pointer to the item list                                  */
/*      cpDelim  -- separator character (something like ',')                  */
/*                                                                            */
/* Return:                                                                    */
/*      ipLen    -- len of the found item                                     */
/*      ipLast   -- TRUE, if the last item was fetched                        */
/*      returns a pointer to the item (isn't '\0' terminated, so use ipLen)   */
/*                                                                            */
/******************************************************************************/
static char *getNextItem(char *cpSource, char *cpDelim, uint *ipLen, uint *ipLast)
{
  char *clStart;

  if (cpSource == NULL) {
    *ipLen = 0;
    return NULL;
  }

  clStart = cpSource;
  while (*cpSource != NULL && *cpSource != *cpDelim) {
    cpSource++;
  }
  *ipLen = cpSource-clStart;
  if (*cpSource == NULL) *ipLast = TRUE;
  else *ipLast = FALSE;
  return clStart;
}

/******************************************************************************/
/* addItemToList  	                                                      */
/*                                                                            */
/* add a new item to the list, allocate required memory, initialize pointers  */
/*                                                                            */
/* Input:                                                                     */
/*        element    -- pointer to field name                                   */
/*        hook       -- pointer to list where the element is to add             */
/*        numOfLegs  -- number of legs to initialize                                                                  */
/*                                                                            */
/* returns:                                                                   */
/*        rtc   -- pointer to the first object with free next entry           */
/*                                                                            */
/******************************************************************************/
static int addItemToList(char *element, ListPtr *hook)
{
  ListPtr pclFreeEl;
  ListPtr pclInitEl;
  uint i;
  uint ilSize;

  ilSize = sizeof(ListElem);
  ilSize = sizeof(struct List);
  if (*hook == NULL) {
    *hook = malloc(sizeof(ListElem));
    (*hook)->next = NULL;
    (*hook)->prev = NULL;
    for (i=0; i<MAX_NUM_LEGS; i++) {
      (*hook)->dobj[i].data = NULL;
      (*hook)->dobj[i].status = 0;
      (*hook)->dobj[i].len = 0;
      (*hook)->dobj[i].timeType = 0;
      (*hook)->dobj[i].timeZone[0] = '\0';
      (*hook)->dobj[i].timeZone[1] = '\0';
      (*hook)->dobj[i].timeZone[2] = '\0';
    }
    (*hook)->field = malloc(strlen(element));
    strcpy((*hook)->field, element);
    return;
  }
  else {
    pclFreeEl = *hook;
    pclFreeEl = getFirstFree (pclFreeEl);
    pclFreeEl->next = malloc(sizeof(ListElem));
    pclFreeEl->next->next = NULL;
    pclInitEl = pclFreeEl->next;
    for (i=0; i<MAX_NUM_LEGS; i++) {
      pclInitEl->dobj[i].data = NULL;
      pclInitEl->dobj[i].status = 0;
      pclInitEl->dobj[i].len = 0;
      pclInitEl->dobj[i].timeType = 0;
      pclInitEl->dobj[i].timeZone[0] = '\0';
      pclInitEl->dobj[i].timeZone[1] = '\0';
      pclInitEl->dobj[i].timeZone[2] = '\0';
    }
    pclFreeEl->next->prev = pclFreeEl;
    pclFreeEl->next->field = malloc(strlen(element));
    strcpy(pclFreeEl->next->field, element);
  }
}

/******************************************************************************/
/* getFirstFree  	                                                      */
/*                                                                            */
/* return a pointer to a 'field' object with name 'name'                      */
/* note the recursive usage of that function!                                 */
/*                                                                            */
/* Input:                                                                     */
/*        hook  -- pointer to field list                                      */
/*                                                                            */
/* returns:                                                                   */
/*        rtc   -- pointer to the first object with free next entry           */
/*                                                                            */
/******************************************************************************/
ListPtr getFirstFree(ListPtr hook)
{
  ListPtr pclLis;

  if (hook->next == NULL) {
    pclLis = hook;
    return pclLis;
  }
  pclLis = getFirstFree(hook->next);
  return pclLis;
}

/******************************************************************************/
/* getItemByName	                                                      */
/*                                                                            */
/* return a pointer to a 'field' object with name 'name'                      */
/* note the recursive usage of that function!                                 */
/*                                                                            */
/* Input:                                                                     */
/*        hook  -- pointer to field element                                   */
/*        name  -- the name of the field                                      */
/*                                                                            */
/* returns:                                                                   */
/*        entry -- pointer to the element if rtc = RC_SUCCES                  */
/*        rtc   -- RC_SUCCESS: element was found                              */
/*                 RC_FAIL:    element was not found                          */
/*                                                                            */
/******************************************************************************/
int getItemByName(ListPtr hook, char *name, ListPtr *entry)                   
{
  int rtc;

  if (!strcmp(name,hook->field)) {
    *entry = hook;
    rtc = RC_SUCCESS;
    return rtc;
  }
  if (hook->next == NULL) return RC_FAIL;
  rtc = getItemByName(hook->next, name, entry);
  return rtc;
}
/******************************************************************************/
/* putItemDataByName	                                                      */
/*                                                                            */
/* Rewrite data of an item. If the data element is to short, the required     */
/* additional space is allocated.                                             */
/*                                                                            */
/* Input:                                                                     */
/*        hook     -- pointer to the start of the itemlist                    */
/*        name     -- the name of the field to get data from                  */
/*        newData  -- pointer to the new data to write                        */               
/*        legLine  -- the data entry to use                                   */
/*                                                                            */
/* Return:                                                                    */
/*        returns  SUCCESS or RC_FAIL                                         */
/*                                                                            */
/******************************************************************************/
int putItemDataByName(ListPtr hook, char *name, char *newData, uint legLine, uint ilStatus)
{
  char *data;
  uint ilnewDataLen = 0;
  uint iloldDataLen = 0;
  uint ilRC;
  ListPtr pclLis;

  ilnewDataLen = strlen(newData);
  /*if (checkDataValid(name, newData, legLine) != RC_SUCCESS) return RC_FAIL;*/

  ilRC = getItemByName(pcgItemList, name, &pclLis);
  if (ilRC != RC_SUCCESS) return RC_FAIL;

  if (pclLis->dobj[legLine].data == NULL) iloldDataLen = 0;
  else iloldDataLen = strlen(pclLis->dobj[legLine].data);
  if ((ilnewDataLen > iloldDataLen) && (pclLis->dobj[legLine].len < ilnewDataLen)) {
    if (iloldDataLen == 0) {
      pclLis->dobj[legLine].data = malloc(ilnewDataLen);
    }
    else {
      pclLis->dobj[legLine].data = realloc(pclLis->dobj[legLine].data, ilnewDataLen);
    }
    pclLis->dobj[legLine].len  = ilnewDataLen;
    if (ilStatus != 0) {
      pclLis->dobj[legLine].status = ilStatus;
    }
  }
  dbg(DEBUG,"putItemDataByName: writing <%s> to field <%s>", newData, name);
  strcpy(pclLis->dobj[legLine].data, newData);
  return RC_SUCCESS;
}
/******************************************************************************/
/* getItemDataByName	                                                      */
/*                                                                            */
/* Retrieve data from an item.                                                */
/*                                                                            */
/* Input:                                                                     */
/*        hook     -- pointer to the start of the itemlist                    */
/*        name     -- the name of the field to get data from                  */
/*        legLine  -- the data entry to use                                   */
/*                                                                            */
/* Return:                                                                    */
/*        returns a pointer to the data string or NULL if not found           */
/*        or invalid                                                          */
/*                                                                            */
/******************************************************************************/
static char *getItemDataByName(ListPtr hook, char *name, uint legLine)
{
  char *data;

  if (!strcmp(name,hook->field)) {
    if (hook->dobj[legLine].status & IS_VALID) return hook->dobj[legLine].data;
    else return NULL;
  }
  if (hook->next == NULL) return NULL;
  data = getItemDataByName(hook->next, name, legLine);
  return data;
}
/******************************************************************************/
/* removeItemFromList	                                                      */
/*                                                                            */
/* Remove an element from the list and free the allocated storage             */
/*                                                                            */
/*                                                                            */
/* Input:                                                                     */
/*        hook  -- pointer to field element                                   */
/*                                                                            */
/* returns:                                                                   */
/*        nothing                                                             */
/*                                                                            */
/******************************************************************************/
static int removeItemFromList (ListPtr hook)
{
  int i;

  if (hook->next != NULL) {
    hook->prev->next = hook->next;
  }
  else {
    hook->prev->next = NULL;
  }
  if (hook->next != NULL) {
    hook->next->prev = hook->prev;
  }
  dbg(TRACE,"removeItemFromList: removing %s",hook->field);
  free(hook->field);
  for (i=0; i<MAX_NUM_LEGS; i++) {
    if (hook->dobj[i].data != NULL) {
      free(hook->dobj[i].data);
    }
  }
  free(hook);
}
/******************************************************************************/
/* deleteItemList	                                                      */
/*                                                                            */
/* Delete the entire item list and free all storage.                          */
/* This function is called recursively!                                       */
/*                                                                            */
/* Input:                                                                     */
/*        hook  -- pointer to field element                                   */
/*                                                                            */
/* returns:                                                                   */
/*        nothing                                                              */
/*                                                                            */
/******************************************************************************/
int deleteItemList(ListPtr hook)
{
  int i;
  if (hook != NULL)
    {
      deleteItemList(hook->next);
      free(hook->field);
      for (i=0; i<MAX_NUM_LEGS; i++) {
	if (hook->dobj[i].data != NULL) {
	  free(hook->dobj[i].data);
	}
      }
      free(hook);
    }
}

/******************************************************************************/
/* checkCedaTime   	                                                      */
/*                                                                            */
/* Check if the provided time pattern is valid. It has to look like:          */
/* YYYYMMDDHHMM, DDHH.                                                        */
/*                                                                            */
/* Input:                                                                     */
/*        pcpData -- pointer to data we have to check                         */
/*                                                                            */
/* Returns:                                                                   */
/*        RC_SUCCESS if pattern is valid                                      */
/*        RC_FAIL else                                                        */
/*                                                                            */
/******************************************************************************/
static int checkCedaTime(char *pcpData) 
{
  uint ilTimStrLen;
  uint ilHour, ilMin, ilSec;
  uint ilYear, ilMonth, ilDay;
  uint ilTime = FALSE;
  uint ilDate = FALSE;
  char clHour[3], clMin[3], clSec[3];
  char clYear[5], clMonth[3], clDay[3];
  char *pclData;

  ilTimStrLen = strlen(pcpData);

  if (ilTimStrLen == 4 || ilTimStrLen ==6) ilTime = TRUE;
  else if (ilTimStrLen == 8) ilDate = TRUE;
  else if (ilTimStrLen == 12 || ilTimStrLen == 14) {
    ilTime = TRUE;
    ilDate = TRUE;
  }
  else {
    dbg(TRACE,"<checkCedaTime> Illegal time format <%s> valid is YYYYMMDD DDHH",pcpData); 
    return RC_FAIL;
  }

  pclData = pcpData;

  if (ilDate == TRUE) {
    strncpy(&clYear[0],pclData,4);
    clYear[4] = '\0';
    ilYear    = atoi(clYear);
    clMonth[0]= *(pclData+4);
    clMonth[1]= *(pclData+5);
    clMonth[2]= '\0';
    ilMonth   = atoi(clMonth);
    clDay[0]  = *(pclData+6);
    clDay[1]  = *(pclData+7);
    clDay[2]  = '\0';
    ilDay     = atoi(clDay);
    if (ilYear < 1970 || ilYear > 2100) {
      dbg(TRACE,"<checkCedaTime> Illegal year <%d>", ilYear);
      return RC_FAIL;
    }
    if (ilMonth < 1 || ilMonth > 12) {
      dbg(TRACE,"<checkCedaTime> Illegal month <%d>", ilMonth);
      return RC_FAIL;
    }
    /* check special case of 29. FEB in leap years! */
    if (LeapYear(ilYear) == 1 && ilMonth == 2) {
      if (ilDay < 1 || ilDay > 29) {
	dbg(TRACE,"<checkCedaTime> Illegal day <%d> in month <%d>", ilDay, ilMonth);
	return RC_FAIL;
      }
    }
    if (ilDay < 1 || ilDay > igDayTab[ilMonth]) {
      dbg(TRACE,"<checkCedaTime> Illegal day <%d> in month <%d>", ilDay, ilMonth);
      return RC_FAIL;
    } 
    /* proceed to date entry */
    pclData = pcpData+8;
  }
  if (ilTime == TRUE) {
    clHour[0] = *pclData;
    clHour[1] = *(pclData+1);
    clHour[2] = '\0';
    ilHour    = atoi(clHour);
    clMin[0]  = *(pclData+2);
    clMin[1]  = *(pclData+3);
    clMin[2]  = '\0';
    ilMin     = atoi(clMin);
    if (ilTimStrLen == 6 || ilTimStrLen== 14) {
      clSec[0] = *(pclData+4);
      clSec[1] = *(pclData+5);
      clSec[2] = '\0';
      ilSec    = atoi(clSec);
      if (ilSec > 59) {
	dbg(TRACE,"<checkCedaTime> Illegal Second <%d>", ilMin);
      return RC_FAIL;
      }
    }
    if(ilHour > 23) {
      dbg(TRACE,"<checkCedaTime> Illegal Hour <%d>", ilHour);
      return RC_FAIL;
    }
    if (ilMin > 59) {
      dbg(TRACE,"<checkCedaTime> Illegal Minute <%d>", ilMin);
      return RC_FAIL;
    }
  }
  return RC_SUCCESS;
}

/*
** Fetch an integer value from cfg
**
** Input:	Section -- name of the related section 
**		field	-- name of the entry
**		entry	-- entry to a specific interface table
**
** Output:	None
**
** Returns:	value contains the fetched value if rtc == RC_SUCCESS
**		rtc contains the message provided from the ReadConfigEntry
**		function, i.e E_SECTION or E_PARAM in case of error
**
** Description: 
**	Reads a value from impman.cfg. 
** 	If the requested section was not found an error message is printed. 
** 	If the requested field was not found, this is silently ignored.
**	If a value between 0 and 32000 is returned, an appropriate value is 
**	written to the config (the value is no longer treated as default,
**	even if we have read a value that equals the default value!).
**	If the value is not in range, an error message is printed
*/
static int get_int(char *Section, char *field, int *data)
{
  int rtc = RC_SUCCESS;
  char value[1024];
  int intval;

  rtc = ReadConfigEntry(Section, field, value);
  if (rtc<0) {
    /*
    ** no entry or missing section
    */
    if (rtc == E_PARAM) return rtc;	/* this parameter wasn't found in cfg -- keep default */
    else if (rtc == E_SECTION) {
      dbg(TRACE,"<get_int> Section %s not found in impman.cfg !", Section);
      return rtc;
    }
  }
  intval = atoi(value); /* Note: using atoi without prechecking the string maybe dangeroes! */
  if (intval > 0 && intval <MAX_INT_PARAMETER) { 
    *data = intval;
    return RC_SUCCESS;
  }
  else {
    *data = -1;
    dbg(TRACE,"<get_int> Illegal Value %s for field %s in Section %s !", value, field, Section);
    return RC_FAIL;
  }
}


static int ReadConfigEntryRow(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer)
{
    int ilRc = RC_SUCCESS;

    char clSection[124] = "\0";
    char clKeyword[124] = "\0";

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRc = iGetConfigRow(cgConfigFile,clSection,clKeyword,
			   CFG_STRING,pcpCfgBuffer);
    if(ilRc != RC_SUCCESS)
    {
	dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
    } 
    else
    {
	dbg(DEBUG,"Config Entry <%s>,<%s>:<%s> found in %s",
	    clSection, clKeyword ,pcpCfgBuffer, cgConfigFile);
    }/* end of if */
    return ilRc;
}





static void ReadAlertConfig()
{
    int ilRC = RC_SUCCESS;
    int i = 0;
    char clSectionName[20] = "\0";

    ilRC = ReadConfigEntryRow("MAIN","ALERT_SECTIONS",cgAlertSections);
    igAlertCount = GetNoOfElements(cgAlertSections,',');
    dbg(TRACE,"%05d: igAlertCount: <%d>",__LINE__,igAlertCount);
    for (i=1 ; i<= igAlertCount; i++)
    {
	GetDataItem(clSectionName,cgAlertSections,i,',',"","");
	dbg(TRACE,"clSectionName: <%s>",clSectionName);

	ilRC = ReadConfigEntryRow(clSectionName,"ERROR",cgErrors[i-1]);
	ilRC = ReadConfigEntryRow(clSectionName,"ALERT",cgAlerts[i-1]);
	dbg(DEBUG,"ERROR <%s>",cgErrors[i-1]);
	dbg(DEBUG,"ALERT <%s>",cgAlerts[i-1]);

    }
    ilRC = ReadConfigEntry("MAIN","SEND_ERRORS_TO_FDI",cgErrorsToFdi);
}

/******************************************************************************/
/* NOW read available entries and update config                            */
/******************************************************************************/

static int setup_config()
{
  int ilRc;
  char clValue[1024];
  char clSection[32];
  char clField[128];
  char clFieldList[1024];
  char clDataList[1024];

  if (pcgGrp2Airlines != NULL) {
    deleteItemList(pcgGrp2Airlines);
    pcgGrp2Airlines = NULL;
  }

  sprintf(clSection, "%s", "MAIN");
  sprintf(clField, "%s", "LIST_OF_AIRLINES");

  ilRc =  ReadConfigEntry(clSection,  clField, clValue);
  if (ilRc<0) {
    /*
    ** no entry or missing section
    */
    if (ilRc == E_PARAM) {
      dbg(TRACE,"<setup_config> Unknown Parameter <%s>", clField);
      return RC_FAIL;
    }
    else if (ilRc == E_SECTION) {
      dbg(TRACE,"<setup_config> Unknown Section <%s>", clSection);
      return RC_FAIL;
    }
  }
  strcpy(clFieldList,clValue);

  sprintf(clField, "%s", "LIST_OF_RECEIVERS");
  ilRc =  ReadConfigEntry(clSection,  clField, clValue);
  if (ilRc<0) {
    /*
    ** no entry or missing section
    */
    if (ilRc == E_PARAM) {
      dbg(TRACE,"<setup_config> Unknown Parameter <%s>", clField);
      return RC_FAIL;
    }
    else if (ilRc == E_SECTION) {
      dbg(TRACE,"<setup_config> Unknown Section <%s>", clSection);
      return RC_FAIL;
    }
  }
  strcpy(clDataList, clValue);
  createItemList(clFieldList, &pcgGrp2Airlines);
  addDataItemToList(clDataList, &pcgGrp2Airlines, 0, ',', IS_VALID);

  return ilRc;
}
