#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Sin/SIN_Server/Base/Server/Kernel/numhdl.c 1.3 2009/10/02 15:45:11SGT ble Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :  Mei, Lee Bee Suan                                        */
/* Date           :  15-JULY-2009                                             */
/* Description    :  Monitor FRATAB, if too few records, get more records from*/
/*                   FRAPOOL and insert into FRATAB                           */
/*                                                                            */
/* Update history :                                                           */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */

static char sccs_version[] ="@(#) UFIS44 (c) ABB AAT/I skeleton.c 44.1.0 / 11.12.2002 HEB";

/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */

#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "fditools.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "rmschkrul.h"
#include "syslib.h"
#include "db_if.h"
#include <time.h>
#include <cedatime.h>

#ifdef _HPUX_SOURCE
    extern long timezone;
    extern char *tzname[2];
    extern int _daylight;
    extern long _timezone;
    extern char *_tzname[2];
#endif

#ifndef BOOL
#define BOOL int
#endif

#define URNO_MAX 0x7FFFFFFF

/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
int debug_level = DEBUG;
static ITEM  *prgItem      = NULL;             /* The queue item pointer  */
static EVENT *prgEvent     = NULL;             /* The event pointer       */
static int   igItemLen     = 0;                /* length of incoming item */
static EVENT *prgOutEvent  = NULL;
static long lgEvtCnt = 0;

static char cgProcessName[20] = "\0";
static char cgConfigFile[512] = "\0";
static char cgHopo[8] = "\0";                 /* default home airport    */
static char cgTabEnd[8] ="\0";                /* default table extension */

/******************************************************************************/
/* Function prototypes                                                        */
/******************************************************************************/
static int Init_Process();
static int Init_Config();
static int ReadConfigEntry(char *pcpFname, char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer);
static int Reset(void);                       /* Reset program          */
static void Terminate(int ipSleep);            /* Terminate program      */
static void HandleSignal(int);                 /* Handles signals        */
static void HandleErr(int);                    /* Handles general errors */
static void HandleQueErr(int);                 /* Handles queuing errors */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/
static int HandleData(EVENT *prpEvent);       /* Handles event data     */
static int CheckQueue(int ipModId, ITEM **prpItem);
static int ReadCfg(char *, char *, char *, char *, char *);
static int CheckQueStatus(void);
static int UrnoMonitoring(void);
static int RunSql( char *, char * );
static void ReadUrnos();
/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
    int ilRc = RC_SUCCESS;          /* Return code          */
    int ilCnt = 0;
    int ilOldDebugLevel = 0;

    INITIALIZE;         /* General initialization   */

    strcpy(cgProcessName,argv[0]);
    dbg(TRACE,"MAIN: version <%s>",sccs_version);

    /* Attach to the MIKE queues */
    do
    {
        ilRc = init_que();
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        }/* end of if */
    }while((ilCnt < 10) && (ilRc != RC_SUCCESS));

    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
        sleep(60);
        exit(1);
    }else{
        dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
    }/* end of if */

    do
    {
        ilRc = init_db();
        if (ilRc != RC_SUCCESS)
        {
            check_ret(ilRc);
            dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        } /* end of if */
    } while((ilCnt < 10) && (ilRc != RC_SUCCESS));

    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
        sleep(60);
        exit(2);
    }else{
        dbg(TRACE,"MAIN: init_db() OK!");
    } /* end of if */

    /* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */
    sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
    dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
    ilRc = TransferFile(cgConfigFile);
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */

    sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
    dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
    ilRc = TransferFile(cgConfigFile);
    if(ilRc != RC_SUCCESS)
    { 
        dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */

    ilRc = SendRemoteShutdown(mod_id);
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    } /* end of if */

    if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
        dbg(DEBUG,"MAIN: waiting for status switch ...");
        HandleQueues();
    }/* end of if */

    if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
        dbg(TRACE,"MAIN: initializing ...");
            ilRc = Init_Process();
            if(ilRc != RC_SUCCESS)
            {
                dbg(TRACE,"Init_Process: init failed!");
            } /* end of if */

    } else {
        Terminate(30);
    }/* end of if */

    ReadUrnos();
    dbg(TRACE,"=====================");
    dbg(TRACE,"MAIN: initializing OK");
    dbg(TRACE,"=====================");
    
    for(;;)
    {
        switch (ctrl_sta)
        {
            case HSB_ACTIVE:
            case HSB_STANDBY:
            case HSB_STANDALONE:
            case HSB_ACT_TO_SBY:
                dbg(TRACE,"<MAIN> %05d CTRL-STA: <%s>", __LINE__, CTRL_STA(ctrl_sta));
                UrnoMonitoring();
                break;

            default:
                dbg(TRACE,"<MAIN> %05d CTRL-STA: <%s>", __LINE__, CTRL_STA(ctrl_sta));
                HandleQueues();
                Reset();
                break;
        }       
    } /* end for */
    
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int ReadCfg(char *pcpFile, char *pcpVar, char *pcpSection,
    char *pcpEntry, char *pcpDefVar)
{
    int ilRc = RC_SUCCESS;
    ilRc = iGetConfigEntry(pcpFile,pcpSection,pcpEntry,1,pcpVar);
    if( ilRc != RC_SUCCESS || strlen(pcpVar) <= 0 )
    {
        strcpy(pcpVar,pcpDefVar); /* copy the default value */
        ilRc = RC_FAIL;
    }
    return ilRc;
} /* end ReadCfg() */

static int Init_Process()
{
    int  ilRc = RC_SUCCESS;         /* Return code */
    char *pclFunc = "Init_Process";


    /* read HomeAirPort from SGS.TAB */
    ilRc = tool_search_exco_data("SYS", "HOMEAP", cgHopo);
    if (ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"<%s> EXTAB,SYS,HOMEAP not found in SGS.TAB", pclFunc);
        Terminate(30);
    } 
    dbg(TRACE,"<%s> home airport <%s>", pclFunc,cgHopo);

    
    ilRc = tool_search_exco_data("ALL","TABEND", cgTabEnd);
    if (ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"<%s> EXTAB,ALL,TABEND not found in SGS.TAB", pclFunc);
        Terminate(30);
    } 
    dbg(TRACE,"<%s> table extension <%s>", pclFunc,cgTabEnd);

    Init_Config();
    return ilRc;
}


static int Init_Config()
{
    int ilRc = RC_SUCCESS;         /* Return code */
    int ilNum, ili;
    char pclTmp[100] = "\0";
    char pclSession[20] = "\0";
    char *pclFunc = "Init_Config";

  ilRc = ReadCfg(cgConfigFile,pclTmp,"MAIN","DEBUG_LEVEL", "TRACE" );
    debug_level = TRACE;
    if( !strncmp( pclTmp, "DEBUG", 5 ) )
        debug_level = DEBUG;
    dbg(TRACE,"<%s> debug_level <%s> -> <%d>",pclFunc, pclTmp, debug_level);
   
} /* end of initialize */

/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
    int ilRc = RC_SUCCESS;              /* Return code */
    
    dbg(TRACE,"Reset: now resetting");
    
    return ilRc;
    
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
    /* unset SIGCHLD ! DB-Child will terminate ! */

    dbg(TRACE,"Terminate: now DB logoff ...");

    signal(SIGCHLD,SIG_IGN);

    logoff();

    dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);


    dbg(TRACE,"Terminate: now leaving ...");

    fclose(outp);

    sleep(ipSleep < 1 ? 1 : ipSleep);
    
    exit(0);
    
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/

static void HandleSignal(int pipSig)
{
    dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);

    switch(pipSig)
    {
    default :
        Terminate(1);
        break;
    } /* end of switch */

    exit(1);
    
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
    dbg(TRACE,"HandleErr: <%d> : unknown ERROR",pipErr);

    return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    switch(pipErr) {
    case    QUE_E_FUNC  :   /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :   /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;
    case    QUE_E_SEND  :   /* Error using msgsnd */
        dbg(TRACE,"<%d> : msgsnd failed",pipErr);
        break;
    case    QUE_E_GET   :   /* Error using msgrcv */
        dbg(TRACE,"<%d> : msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX   :
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE  :
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES  :
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        break;
    case    QUE_E_RESP  :   /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;
    case    QUE_E_FULL  :
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG :   /* No message on queue */
        dbg(DEBUG,"<%d> : no messages on queue",pipErr);
        break;
    case    QUE_E_INVORG    :   /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :   /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;
    default         :   /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
    } /* end switch */
         
    return;

} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
    int ilRc = RC_SUCCESS;          /* Return code */
    int ilBreakOut = FALSE;
    
    do
    {
        ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text; 

        if( ilRc == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRc != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRc);
            } /* fi */
        
            switch( prgEvent->command )
            {
            case    HSB_STANDBY :
                ctrl_sta = prgEvent->command;
                break;  
    
            case    HSB_COMING_UP   :
                ctrl_sta = prgEvent->command;
                break;  
    
            case    HSB_ACTIVE  :
                ctrl_sta = prgEvent->command;
                ilBreakOut = TRUE;
                break;  

            case    HSB_ACT_TO_SBY  :
                ctrl_sta = prgEvent->command;
                break;  
    
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate(1);
                break;  
    
            case    HSB_STANDALONE  :
                ctrl_sta = prgEvent->command;
                ResetDBCounter();
                ilBreakOut = TRUE;
                break;  

            case    REMOTE_DB :
                /* ctrl_sta is checked inside */
                HandleRemoteDB(prgEvent);
                break;

            case    SHUTDOWN    :
                Terminate(1);
                break;
                        
            case    RESET       :
                ilRc = Reset();
                break;
                        
            case    EVENT_DATA  :
                if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
                {
                    ilRc = HandleData(prgEvent);
                    if(ilRc != RC_SUCCESS)
                    {
                        HandleErr(ilRc);
                    }/* end of if */
                }else{
                    dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
                    DebugPrintItem(TRACE,prgItem);
                    DebugPrintEvent(TRACE,prgEvent);
                }/* end of if */
                break;
                    
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;

            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;

            default         :
                dbg(TRACE,"HandleQueues: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        } else {
            /* Handle queuing errors */
            HandleQueErr(ilRc);
        } /* end else */
    } while (ilBreakOut == FALSE);

/*            ilRc = Init_Process();
            if(ilRc != RC_SUCCESS)
            {
                dbg(TRACE,"Init: init failed!");
            } 
 */   

} /* end of HandleQueues */
    



/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(EVENT *prpEvent)
{
    BC_HEAD *prlBchead = NULL;
    CMDBLK  *prlCmdblk = NULL;

    int ilRc = RC_SUCCESS;         /* Return code */
    int ilCmd = 0;
    char *pclSelection = NULL;
    char *pclFields = NULL;
    char *pclData = NULL;
    char *pclRow = NULL;
    char pclTable[34];
    char *pclFunc = "HandleData";

    prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
    prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
    pclSelection = prlCmdblk->data;
    pclFields    = pclSelection + strlen(pclSelection) + 1;
    pclData      = pclFields + strlen(pclFields) + 1;


    strcpy(pclTable,prlCmdblk->obj_name);

    dbg(TRACE,"========== START <%10.10d> ==========",lgEvtCnt);

    /****************************************/
   /* DebugPrintBchead(TRACE,prlBchead);
    DebugPrintCmdblk(TRACE,prlCmdblk);
    */
    dbg(TRACE,"Command: <%s>",prlCmdblk->command);
    dbg(TRACE,"originator<%d>",prpEvent->originator);
    /*dbg(TRACE,"originator follows event = %p ",prpEvent);
    dbg(TRACE,"tw_start: <%s>",prlCmdblk->tw_start);
    dbg(TRACE,"tw_end: <%s>",prlCmdblk->tw_end);

    dbg(TRACE,"selection follows Selection = %p ",pclSelection);
    dbg(TRACE,"selection <%s>",pclSelection);
    dbg(TRACE,"fields    <%s>",pclFields);
    dbg(TRACE,"data      <%s>",pclData);*/
    /****************************************/

    if( !strncmp( prlCmdblk->command, "CFG", 3 ) )
        Init_Config();

	return(RC_SUCCESS);
    
} /* end of HandleData */


static int CheckQueue(int ipModId, ITEM **prpItem)
{
    int     ilRc       = RC_SUCCESS;                      /* Return code */
    int     ilItemSize = 0;
    EVENT *prlEvent    = NULL;
    static  ITEM  *prlItem      = NULL;
    
    ilItemSize = I_SIZE;
    
    ilRc = que(QUE_GETBIGNW,ipModId,ipModId,PRIORITY_3,ilItemSize,(char *) &prlItem);
    if(ilRc == RC_SUCCESS)
    {
        dbg(TRACE,"DebugPrintItem with prlItem follows");
        DebugPrintItem(TRACE,prlItem);
        *prpItem = prlItem;
        dbg(TRACE,"DebugPrintItem with prpItem follows");
        DebugPrintItem(TRACE,*prpItem);
        prlEvent = (EVENT*) ((prlItem)->text);

        switch( prlEvent->command )
        {
            case    HSB_STANDBY :
                dbg(TRACE,"CheckQueue: HSB_STANDBY");
                HandleQueues();
                ctrl_sta = prlEvent->command;
                break;
    
            case    HSB_COMING_UP   :
                dbg(TRACE,"CheckQueue: HSB_COMING_UP");
                ctrl_sta = prlEvent->command;
                break;
    
            case    HSB_ACTIVE  :
                dbg(TRACE,"CheckQueue: HSB_ACTIVE");
                ctrl_sta = prlEvent->command;
                break;
    
            case    HSB_ACT_TO_SBY  :
                dbg(TRACE,"CheckQueue: HSB_ACT_TO_SBY");
                HandleQueues();
                ctrl_sta = prlEvent->command;
                break;
    
            case    HSB_DOWN    :
                dbg(TRACE,"CheckQueue: HSB_DOWN");
                ctrl_sta = prlEvent->command;
                /* Acknowledge the item 
                ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
                if( ilRc != RC_SUCCESS )
                {
                    HandleQueErr(ilRc);
                }  */
                Terminate(30);
                break;
        
        case    HSB_STANDALONE  :
                dbg(TRACE,"CheckQueue: HSB_STANDALONE");
                ctrl_sta = prlEvent->command;
                break;
    
            case    SHUTDOWN    :
                dbg(TRACE,"CheckQueue: SHUTDOWN");
                /* Acknowledge the item */
                ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
                if( ilRc != RC_SUCCESS )
                {
                    /* handle que_ack error */
                    HandleQueErr(ilRc);
                } /* fi */
                Terminate(30);
                break;
    
            case    RESET       :
                ilRc = Reset();
                break;
    
            case    EVENT_DATA  :
                /* DebugPrintItem(DEBUG,prgItem); */
                /* DebugPrintEvent(DEBUG,prlEvent); */
                break;
    
            case    TRACE_ON :
                dbg_handle_debug(prlEvent->command);
                break;
    
            case    TRACE_OFF :
                dbg_handle_debug(prlEvent->command);
                break;


            default         :
                dbg(TRACE,"CheckQueue: unknown event");
                DebugPrintItem(TRACE,prlItem);
                DebugPrintEvent(TRACE,prlEvent);
                break;
    
        } /* end switch */
    
        /* Acknowledge the item */
        ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
        if( ilRc != RC_SUCCESS )
        {
            /* handle que_ack error */
            HandleQueErr(ilRc);
        } /* fi */
    }else{
        if(ilRc != QUE_E_NOMSG)
        {
            dbg(TRACE,"CheckQueue: que(QUE_GETBIG,%d,%d,...) failed <%d>",ipModId,ipModId,ilRc);
            HandleQueErr(ilRc);
        }/* end of if */
    }/* end of if */
       
    return ilRc;
}/* end of CheckQueue */

/******************************************************************************/
static int UrnoMonitoring(void)
{
    struct timeval tvptr;
    int ilRc;
    int ilNumUrno;
    int ilTotalTime = 0;
    char *pclFunc = "UrnoMonitoring";

    /* Set time to a higher precision of "sleep" */
    /*tvptr.tv_sec = tgCheckFRATAB;
    tvptr.tv_usec = 0L; */

    while (1)
    {
        /*--------------------------------------------------------------------*/
        /*--------------------------------------------------------------------*/
        /* Not supported in LINUX */
        /*ilRC = select( 0, NULL, NULL, NULL, &tvptr );
        if( ilRC == -1 )
        {
            dbg( TRACE, "<%s> Select ERROR (%s)", pclFunc, strerror( errno ) );
            sleep( 1 );
        }*/
        sleep( 1 );
        /*--------------------------------------------------------------------*/
        ReadUrnos();
        /*--------------------------------------------------------------------*/
        if ((ilRc = CheckQueStatus()) < 0)
            return ilRc;
    }
}

/******************************************************************************/

static void ReadUrnos()
{
    int ilRc;
    int ilMinUrno;
    int ilMaxUrno;
    int ilUrno;
    char pclTmpStr[128];
    char pclSqlBuf[512];
    char pclSqlData[512];
    char *pclFunc = "ReadUrnos";

    sprintf( pclSqlBuf, "%s", "SELECT MINN, MAXN, ACNU FROM NUMTAB WHERE KEYS = 'SNOTAB'" );
    ilRc = RunSql( pclSqlBuf, pclSqlData );
    if( ilRc != DB_SUCCESS )
    {
        dbg( TRACE, "<%s> MAJOR ERROR!!!! Cannot monitor URNO as NUMTAB cannot be accessed", pclFunc );
        return;
    }
    get_fld(pclSqlData,FIELD_1,STR,10,pclTmpStr); ilMinUrno = atoi(pclTmpStr);
    get_fld(pclSqlData,FIELD_2,STR,10,pclTmpStr); ilMaxUrno = atoi(pclTmpStr);
    if( !strncmp(pclTmpStr, "9999999999", 10) )
        ilMaxUrno = URNO_MAX;
    get_fld(pclSqlData,FIELD_3,STR,10,pclTmpStr); ilUrno = atoi(pclTmpStr);

    dbg( TRACE, "<%s> MINN <%d> MAXN <%d> URNO <%d> LEFT <%d>", pclFunc, 
                ilMinUrno, ilMaxUrno, ilUrno, (ilMaxUrno-ilUrno) );
}
/******************************************************************************/

static int CheckQueStatus(void)
{
    int     ilRC;
    int           len;

    /*memset(prgItem, 0x00, MAX_EVENT_SIZE);*/
    len=0;
        /* 20020628 JIM: test for memory leak: allocation should be done in que */
    /* prgItem=NULL; */
    if ((ilRC = que(QUE_GETBIGNW, 0, mod_id, PRIORITY_3,len, (char*)&prgItem)) == RC_SUCCESS)
    {
        /* Acknowledge the item */
        ilRC = que(QUE_ACK, 0, mod_id, 0, 0, NULL);
        if (ilRC != RC_SUCCESS) 
        {
            HandleQueErr(ilRC);
        } 
        prgEvent = (EVENT *) prgItem->text;
        switch (prgEvent->command)
        {
            case    HSB_COMING_UP:
                ctrl_sta = prgEvent->command;
/*                igLastCtrlSta = ctrl_sta; */
                return -1;

            case    HSB_STANDBY:
                ctrl_sta = prgEvent->command;
/*                igLastCtrlSta = ctrl_sta; */
                return 0;

            case    HSB_ACTIVE:
            case    HSB_STANDALONE:
            case    HSB_ACT_TO_SBY:
                ctrl_sta = prgEvent->command;
/*                if (igLastCtrlSta == HSB_STANDBY)
                {
                    igLastCtrlSta = ctrl_sta;
                    Reset();
                }
*/                return 0;
    
            case    HSB_DOWN:
                ctrl_sta = prgEvent->command;
/*                igLastCtrlSta = ctrl_sta;*/
                Terminate(0);
                break;  

            case    SHUTDOWN:
                Terminate(0);
                break;
                                 
            case    RESET:
                Reset();
                return -2;
                    
            case    EVENT_DATA:
                if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
                {
                    ilRC = HandleData(prgEvent);
                    if(ilRC != RC_SUCCESS)
                    {
                        HandleErr(ilRC);
                    }/* end of if */
                }else{
                    dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
                    DebugPrintItem(TRACE,prgItem);
                    DebugPrintEvent(TRACE,prgEvent);
                }/* end of if */                return 0;

            case TRACE_OFF:
            case TRACE_ON:
                dbg_handle_debug(prgEvent->command);
                return 0;

            default:
                dbg(DEBUG,"main unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                return 0;
        } 
    } 
    else
        HandleQueErr(ilRC);

    /* bye bye */
    return 0;
}


int RunSql( char *pcpQuery, char *pcpData )
{
  int ilRc = DB_SUCCESS;
  short slCursor = 0;
  char pclErrBuff[128];
  char *pclFunc = "RunSql";

  dbg ( TRACE, "<%s> SQL <%s>", pclFunc, pcpQuery );
  slCursor = 0;
  ilRc = sql_if ( START, &slCursor, pcpQuery, pcpData );
  close_my_cursor (&slCursor);

  if (ilRc == DB_ERROR )
  {
      get_ora_err(ilRc, &pclErrBuff[0]);
      dbg (TRACE, "<%s> Error getting Oracle data <%s>", pclFunc, &pclErrBuff[0]);
      ilRc = DB_ERROR;
  }
  return ilRc;
} /* RunSql */
