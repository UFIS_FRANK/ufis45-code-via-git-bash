#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Sin/SIN_Server/Base/Server/Kernel/bccom.c 1.1 2006/02/03 14:02:33SGT tho Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program bccom                                                   */
/*                                                                            */
/* Author         :                                                           */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history :                                                           */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* be carefule with strftime or similar functions !!!                          */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */

#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <stdarg.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "glbdef.h"
#include "debugrec.h"
#ifdef _SUN_UNIX
#include <thread.h>
#else
#include <pthread.h>
#endif
#include <fcntl.h>
#include <sys/ioctl.h>
/* #include <sys/filio.h>	*/
#include <sys/time.h>

#ifdef _HPUX_SOURCE
	extern int daylight;
	extern long timezone;
	extern char *tzname[2];
#else
	extern int _daylight;
	extern long _timezone;
	extern char *_tzname[2];
#endif

extern int debug_level;
static void get_time(char *s);

/* module multithreaded debug log file implementation	*/
typedef struct	MtDbgLogFile
{
#ifdef	_SUN_UNIX
	mutex_t			mutex;					/* the mutex for the debug log file		*/
#else
	pthread_mutex_t	mutex;					/* the mutex for the debug log file		*/
#endif	
	long	MaxDbgLines;
	long	ActDbgLines;
	int		MaxDbgFileNo;
	int		ActDbgFileNo;
	char	Mod_Name[256];
	char	Mod_Version[256];
	char	CedaLogFile[256];
	int		Debug_Level;
	FILE*	fp;	
}	MT_DBG_LOGFILE;

/* Initialize an instance of the threads logfile class	*/
/* log_file					MT_DBG_LOGFILE	*	the 'this' pointer of the instance	of this class	*/
/* mod_name					char			*	the module name of the process						*/
/* mod_version				char			*	the module version of the process					*/
/* cedaLogFile				char			*	the log file name									*/
static int MtDbgLogFileInit(MT_DBG_LOGFILE *log_file,char *mod_name,char *mod_version, char *cedaLogFile)
{
	if (log_file == NULL)
		return RC_FAIL;
		
	if (mod_name == NULL)
		return RC_FAIL;
		
	if (mod_version == NULL)
		return RC_FAIL;
		
	if (cedaLogFile == NULL)
		return RC_FAIL;
				
#ifdef	_SUN_UNIX
	mutex_init(&log_file->mutex,USYNC_THREAD,NULL);
#else
	pthread_mutex_init(&log_file->mutex,NULL);
#endif
						
	log_file->MaxDbgLines 	= 30000;
	log_file->ActDbgLines 	= 0;
	log_file->MaxDbgFileNo 	= 10;
	log_file->ActDbgFileNo 	= 0;
	
	strcpy(log_file->Mod_Name,mod_name);
	strcpy(log_file->Mod_Version,mod_version);
	strcpy(log_file->CedaLogFile,cedaLogFile);
	
	log_file->Debug_Level	= debug_level;
	log_file->fp			= NULL;
	
	return RC_SUCCESS;
	
}

/* Releae threads logfile instance	*/
/* log_file					MT_DBG_LOGFILE	*	the 'this' pointer of the instance	of this class	*/
static int MtDbgLogFileRelease(MT_DBG_LOGFILE *log_file)
{
	if (log_file == NULL)
		return RC_FAIL;

#ifdef	_SUN_UNIX
	mutex_destroy(&log_file->mutex);
#else
	pthread_mutex_destroy(&log_file->mutex);
#endif
		
	if (log_file->fp == NULL)
		return RC_SUCCESS;
		
	fclose(log_file->fp);
	
	return RC_SUCCESS;
}

/* switch the logfile automatically to a new file	*/
/* log_file					MT_DBG_LOGFILE	*	the 'this' pointer of the instance	of this class	*/
static int MtDbgLogFileSwitchAuto(MT_DBG_LOGFILE *log_file)
{
	char	pclCommand[256];
	char	pclLastDbgName[256];
	char	pclNewDbgName[256];
	FILE*	outp1;
			
	if (log_file == NULL)
		return RC_FAIL;
		
	if (log_file->fp == NULL)
		return RC_FAIL;

	outp1 = log_file->fp;
    fseek(outp1,0,SEEK_END);
    
	sprintf(pclLastDbgName,"%s.bak.%d",log_file->CedaLogFile,log_file->ActDbgFileNo);
	fprintf(outp1,"Renaming Old Debug File to '%s'\n",pclLastDbgName);
	fflush (outp1);	/* necessary, because if close is not done rename will result in a file with no contence */
	fclose (outp1);
	sprintf(pclNewDbgName,"%s.new",log_file->CedaLogFile);
	log_file->fp = NULL;
	
#ifdef _LINUX
	log_file->fp = fopen(pclNewDbgName,"w");
#else
	log_file->fp = freopen(pclNewDbgName,"w",outp1); /* reopen to avoid original and .bak mismatch 15.11.2001 2. */
#endif
	/* therefore following sleep and fopen loop no longer necessary (only one open) GRM 13.11.2001 1. */
	rename(log_file->CedaLogFile,pclLastDbgName);
	rename(pclNewDbgName,log_file->CedaLogFile);
	
	if (log_file->MaxDbgFileNo > 0 && log_file->ActDbgFileNo > log_file->MaxDbgFileNo)
  	{
		sprintf(pclLastDbgName,"%s.bak.%d",log_file->CedaLogFile,log_file->ActDbgFileNo - log_file->MaxDbgFileNo);
     	remove(pclLastDbgName);
  	}
  	
	++log_file->ActDbgFileNo;
	  	
	if (log_file->ActDbgFileNo < 0 ) /* turn around integer definition range */
		log_file->ActDbgFileNo = 1;
	
	return RC_SUCCESS;	
}

/* write to the logfile of an specific instance of the THREAD_MNGMNT class	*/
/* level					int					the debug level for this message	*/
/* log_file					MT_DBG_LOGFILE	*	the 'this' pointer of the instance	of this class	*/
/* pFormat					char			*	the format specification for this message (see fprintf)	*/
/* ...						va_list				variable argument list (see vfprinf)	*/
static int MtDbgLogFileDbg(int level,MT_DBG_LOGFILE *log_file,char *fmt,...)
{
	va_list args;
	char*	s;
	char	Errbuf[1000];
	int		i;
	
	if (log_file == NULL)
		return RC_FAIL;
		
#ifdef	_SUN_UNIX
	/* get exclusive access to the log file	*/
	mutex_lock(&log_file->mutex);
#else
	pthread_mutex_lock(&log_file->mutex);
#endif		
	if (log_file->fp == NULL)
	{
#ifdef	_SUN_UNIX
		/*	release exclusive access to the log file	*/
		mutex_unlock(&log_file->mutex);
#else
		/*	release exclusive access to the log file	*/
		pthread_mutex_unlock(&log_file->mutex);
#endif
		return RC_FAIL;
	}
			
	if (log_file->Debug_Level > 0)
	{
		if (level == TRACE)
		{
			fseek(log_file->fp,0,SEEK_END);
			if (log_file->ActDbgLines == 0)
			{
				fprintf(log_file->fp,"%s\n",log_file->Mod_Version);
				fflush(log_file->fp);
				++log_file->ActDbgLines;
				if (log_file->ActDbgFileNo > 0)
				{
					fprintf(log_file->fp,"=== Logfile continued ===\n");
					fflush(log_file->fp);
					++log_file->ActDbgLines;
				}
			}			
			
			va_start(args,fmt);
			(void) sprintf(Errbuf, "%s ",log_file->Mod_Name);
			s = Errbuf + strlen(Errbuf);
			get_time(s);
			s = Errbuf + strlen(Errbuf);
			strcat (s, fmt);
			strcat (s, "\n");
			vfprintf(log_file->fp,Errbuf, args); 
			fflush(log_file->fp);
      		++log_file->ActDbgLines;
			va_end(args);                        
		} 		
		
		if (level == DEBUG && log_file->Debug_Level > TRACE) 
		{
			fseek(log_file->fp,0,SEEK_END);
			if (log_file->ActDbgLines == 0)
			{
				fprintf(log_file->fp,"%s\n",log_file->Mod_Version);
				fflush(log_file->fp);
				++log_file->ActDbgLines;
				if (log_file->ActDbgFileNo > 0)
				{
					fprintf(log_file->fp,"=== Logfile continued ===\n");
					fflush(log_file->fp);
					++log_file->ActDbgLines;
				}
			}			
			
			va_start(args,fmt);
			(void) sprintf(Errbuf, "%s ",log_file->Mod_Name);
			s = Errbuf + strlen(Errbuf);
			get_time(s);
			s = Errbuf + strlen(Errbuf);
			strcat (s, fmt);
			strcat (s, "\n");
			vfprintf(log_file->fp,Errbuf, args); 
			fflush(log_file->fp);
      		++log_file->ActDbgLines;
			va_end(args);                        
		} 		
		
	    if (log_file->MaxDbgLines > 0 && log_file->ActDbgLines > log_file->MaxDbgLines)
	    {
			fseek(log_file->fp,0,SEEK_END);
			fprintf(log_file->fp,"(dbg) ActDbgLines: %ld, MaxDbgLines: %d\n",log_file->ActDbgLines,log_file->MaxDbgLines); 
	       	fflush(log_file->fp);
	       	MtDbgLogFileSwitchAuto(log_file);
			log_file->ActDbgLines = 0;
	    }

	}

#ifdef	_SUN_UNIX
		/*	release exclusive access to the log file	*/
	mutex_unlock(&log_file->mutex);
#else
		/*	release exclusive access to the log file	*/
	pthread_mutex_unlock(&log_file->mutex);
#endif
	
	return RC_SUCCESS;	
				
}



/* module bc buffer implementation	*/

typedef struct _BcBuffer
{
        char*	Value;			/* BC packed in key items				*/
		long  	AllocatedSize;	/* byte count from malloc or realloc 	*/
		long  	UsedLen;		/* calulated value from strlen 			*/

        char 	Table[8];		/* the table of the event (object)		*/
        char 	Cmd[12];		/* the command which happened			*/
        char	BcNum[12];		/* the broadcast number					*/
        char*	Status;			/* status, which thread has to send		*/
} BC_BUFFER;

/* initialize an instance of the BC_BUFFER class	*/
/* pBcBuffer	BC_BUFFER	*	the 'this' pointer of the instance of this class	*/
/* nClients		size_t			the huge count of connections that can be managed	*/
static	int	BcBufferInit(BC_BUFFER *pBcBuffer,size_t nClients)
{
	if (pBcBuffer == NULL)
		return RC_FAIL;
			
	pBcBuffer->Value 		= NULL;
	pBcBuffer->AllocatedSize= 0;
	pBcBuffer->UsedLen		= 0;
	pBcBuffer->Table[0]	  	= '\0';
	pBcBuffer->Cmd[0]		= '\0';
	pBcBuffer->BcNum[0]		= '\0';
	pBcBuffer->Status 		= (char *)malloc((nClients+2) * sizeof(char));
	pBcBuffer->Status[nClients] = '\0';
	
	return RC_SUCCESS;
}
										
/* releases an instance of the BC_BUFFER class	*/										
/* pBcBuffer	BC_BUFFER	*	the 'this' pointer of the instance of this class	*/
static	int	BcBufferRelease(BC_BUFFER *pBcBuffer)
{
	if (pBcBuffer == NULL)
		return RC_FAIL;
	
	if (pBcBuffer->Value != NULL)
	{
		free(pBcBuffer->Value);
		pBcBuffer->Value = NULL; 		
	}
	
	pBcBuffer->AllocatedSize= 0;
	pBcBuffer->UsedLen		= 0;
	pBcBuffer->Table[0]	  	= '\0';
	pBcBuffer->Cmd[0]		= '\0';
	pBcBuffer->BcNum[0]		= '\0';
	
	if (pBcBuffer->Status != NULL)
	{
		free(pBcBuffer->Status);
		pBcBuffer->Status = NULL; 		
	}

	return RC_SUCCESS;
}
										

/*	module	Queue implementation	*/
typedef struct _Queue
{
	int 		Head;		/* indicating the oldest BC in the ring buffer, which is not sent by all threads yet */
	int 		Tail;		/* indicating the latest BC in the ring buffer */
	int 		Wrap;		/* after Tail starts with 0 again, this flag is set till Head also starts again with 0 */
	size_t		Size;		/* the size of the queue	*/
	BC_BUFFER	*BcBuffer;	/* the queue implementation	*/
}	QUEUE;

/* initializes an instance of the QUEUE class	*/
/* pQueue		QUEUE	*	the 'this' pointer of the instance	of this class	*/
/* nElements	size_t		the huge count of events that can be stored within the ring buffer			*/
/* nClients		size_t		the huge count of connections that can be managed by this instance			*/
static int		QueueInit(QUEUE *myQueue,size_t nElements,size_t nClients)
{
	int i;
	int ilRc;
	
	if (myQueue == NULL)
		return RC_FAIL;
			
	myQueue->Head = 0;
	myQueue->Tail = 0;
	myQueue->Wrap = -1;
	myQueue->Size = nElements;
	myQueue->BcBuffer = (BC_BUFFER *)malloc(nElements * sizeof(BC_BUFFER));		
	for (i = 0; i < nElements; i++)
	{
		ilRc = BcBufferInit(&myQueue->BcBuffer[i],nClients);
		if (ilRc != RC_SUCCESS)
			return ilRc;
	}		
	
	return RC_SUCCESS;
}

/* releases an instance of the QUEUE class	*/
/* pQueue		QUEUE	*	the 'this' pointer of the instance	of this class	*/
static int			QueueRelease(QUEUE *pQueue)
{
	int i,ilRc;
	
	if (pQueue == NULL)
		return RC_FAIL;

	for (i = 0; i < pQueue->Size; i++)
	{
		ilRc = BcBufferRelease(&pQueue->BcBuffer[i]);
		if (ilRc != RC_SUCCESS)
			return ilRc;
	}		
	
	if (pQueue->BcBuffer != NULL)
	{
		free(pQueue->BcBuffer);
	}		
	
	return RC_SUCCESS;
}

/* returns the next free position in the ring buffer of an instance of the QUEUE class	*/
/* pQueue		QUEUE	*	the 'this' pointer of the instance	of this class	*/
/* bpIncrement	int			increment tail true or false	*/

static int QueueGetNextFreePosition(QUEUE *pQueue,int bpIncrement)
{
	if (pQueue == NULL)
		return -1;
			
	/* initialization state	? */
	if (pQueue->Wrap == -1)
	{
		/* set state to "not wrapped" around	*/
		pQueue->Wrap = 0;
	}
	else if (bpIncrement)
	{
		pQueue->Tail++;
	}
	
	/* end of buffer reached ? Yes -> restart at begin of buffer, but set the wrap around flag	*/
	if (pQueue->Wrap == 0)
	{
		if (pQueue->Tail >= pQueue->Size)
		{
			pQueue->Tail = 0;
			pQueue->Wrap = 1;
		}
	}
		
	/* check on buffer overrun	*/
	if (pQueue->Wrap == 1)
	{
		if (pQueue->Tail >= pQueue->Head)
		{
			return -1;
		}
	}

	/* last check */
	if (pQueue->Tail >= pQueue->Size)
		return -1;
			
	/* ok	*/
	return pQueue->Tail;	
}


/* returns the next read position in the ring buffer of an instance of the QUEUE class	*/
/* pQueue		QUEUE	*	the 'this' pointer of the instance	of this class	*/
/* ipCurPos		int 		the position last read					*/
static int	QueueGetNextReadPosition(QUEUE *pQueue,int ipCurPos)
{
	if (pQueue == NULL)
		return -1;
		
	if (ipCurPos == -1)	/* get first position	*/
	{		
		if (pQueue->BcBuffer[pQueue->Head].Value == NULL)
		{
			/* no BC arrived yet */
			return -1;
		}
		else
		{
			/* the first read position is the Head position of the ring buffer	*/
			return pQueue->Head;
		}
	}
	else
	{
		/* check on wrap around	*/
		if (pQueue->Tail < ipCurPos)	/* wrap around	*/
		{
			++ipCurPos;
			if (ipCurPos >= pQueue->Size)
			{
				ipCurPos = 0;										
				if (ipCurPos > pQueue->Tail)
				{
					return -1;
				}
			}
		}
		else
		{
			++ipCurPos;
			if (ipCurPos > pQueue->Tail)
			{
				return -1;
			}
		}

		/* additional check	*/		
		if (pQueue->BcBuffer[ipCurPos].Value == NULL)
		{
			/* no BC arrived yet */
			return -1;
		}
		else
		{
			return ipCurPos;
		}
	}
}

/* forward declaration	*/
extern size_t max_bc_clients;

/* set the status bit of a specific thread in the ring buffer of an instance of the QUEUE class	*/
/* pQueue		QUEUE	*	the 'this' pointer of the instance	of this class	*/
/* ilIndex		int			the index of the event within the ring buffer		*/
/* ipStatusIndexint			the index of the thread			*/
/* cpStatus		char		the state to be set : '1' -> not send, '0' -> send	*/
static int	QueueSetStatus(QUEUE *pQueue,int ilIndex,int ipStatusIndex,char cpStatus)
{
	if (pQueue == NULL)
		return RC_FAIL;
		
	if (ilIndex < 0 || ilIndex >= pQueue->Size)
		return RC_FAIL;
				
	if (ipStatusIndex >= max_bc_clients)
		return RC_FAIL;
						
	pQueue->BcBuffer[ilIndex].Status[ipStatusIndex] = cpStatus;
	
	return RC_SUCCESS;							
}

/* returns the status bit of a specific event in the ring buffer of an instance of the QUEUE class	*/
/* pQueue		QUEUE	*	the 'this' pointer of the instance	of this class	*/
/* ilIndex		int			the index of the event within the ring buffer		*/
static char *QueueGetStatus(QUEUE *pQueue,int ilIndex)
{
	if (pQueue == NULL)
		return NULL;

	if (ilIndex < 0 || ilIndex >= pQueue->Size)
		return NULL;

	return pQueue->BcBuffer[ilIndex].Status;	
}

/* initializes the event value of a specific event in the ring buffer of an instance of the QUEUE class	*/
/* pQueue		QUEUE	*	the 'this' pointer of the instance	of this class	*/
/* ilIndex		int			the index of the event within the ring buffer		*/
/* newSize		size_t		the new size of the event value to be stored for this thread	*/
/* pclKeyTbl	const char *the name of the table entry for this event				*/
/* pclKeyCmd	const char *the name of the command entry for this event			*/		
static int QueueValueInit(QUEUE *pQueue,int ilIndex,size_t newSize,const char *pclKeyTbl,const char *pclKeyCmd,const char *pclKeyBcn)
{
	if (pQueue == NULL)
		return RC_FAIL;

	if (ilIndex < 0 || ilIndex >= pQueue->Size)
		return RC_FAIL;
		
	if (pclKeyTbl == NULL)
		return RC_FAIL;
		
	if (pclKeyCmd == NULL)
		return RC_FAIL;				
		
	if (pclKeyBcn == NULL)
		return RC_FAIL;				
		
	if (newSize > pQueue->BcBuffer[ilIndex].AllocatedSize)
    {
    	if (pQueue->BcBuffer[ilIndex].AllocatedSize <= 0)
      	{
        	pQueue->BcBuffer[ilIndex].Value = (char *)malloc(newSize * sizeof(char));
        	if (pQueue->BcBuffer[ilIndex].Value == NULL)
        	{
        		return RC_FAIL;
        	} /* end if */
        	
        	pQueue->BcBuffer[ilIndex].AllocatedSize = newSize;
        } /* end if */
      	else
      	{
        	pQueue->BcBuffer[ilIndex].Value = (char*)realloc(pQueue->BcBuffer[ilIndex].Value, newSize);
        	if (pQueue->BcBuffer[ilIndex].Value == NULL)
        	{
        		return RC_FAIL;
        	} /* end if */
			pQueue->BcBuffer[ilIndex].AllocatedSize = newSize;
      	} /* end else */
    } /* end if */
    
	if (strcmp(pclKeyCmd,"BCOFF") == 0)
	{
		pQueue->BcBuffer[ilIndex].UsedLen = -1;
	} /* end if */
	else if (strcmp(pclKeyCmd,"BCKEY") == 0)
	{
		pQueue->BcBuffer[ilIndex].UsedLen = -2;
	} /* end if */
	else if (strcmp(pclKeyCmd,"BCGET") == 0)
	{
		pQueue->BcBuffer[ilIndex].UsedLen = -3;
	} /* end if */
	else if (strcmp(pclKeyCmd,"BCINF") == 0)
	{
		pQueue->BcBuffer[ilIndex].UsedLen = -4;
	} /* end if */
    else
    {
		pQueue->BcBuffer[ilIndex].UsedLen = 0;
	}
	
	/* set table and command values for filtering issue	*/
	strncpy(pQueue->BcBuffer[ilIndex].Table,pclKeyTbl,sizeof(pQueue->BcBuffer[ilIndex].Table));
	pQueue->BcBuffer[ilIndex].Table[sizeof(pQueue->BcBuffer[ilIndex].Table)-1] = '\0';
	 	
	strncpy(pQueue->BcBuffer[ilIndex].Cmd,pclKeyCmd,sizeof(pQueue->BcBuffer[ilIndex].Cmd));
	pQueue->BcBuffer[ilIndex].Cmd[sizeof(pQueue->BcBuffer[ilIndex].Cmd)-1] = '\0';
	
	strncpy(pQueue->BcBuffer[ilIndex].BcNum,pclKeyBcn,sizeof(pQueue->BcBuffer[ilIndex].BcNum));
	pQueue->BcBuffer[ilIndex].BcNum[sizeof(pQueue->BcBuffer[ilIndex].BcNum)-1] = '\0';
	
    return RC_SUCCESS;
}

/* initializes the event value of a specific event in the ring buffer of an instance of the QUEUE class	*/
/* pQueue		QUEUE	*	the 'this' pointer of the instance	of this class	*/
/* ilIndex		int			the index of the event within the ring buffer		*/
/* ipOutDatPos	int		*	the index of the value where the data will be inserted */
/* pcpKey		const char *the keyword to be written prior to the data			*/
/* pcpData		const char * the data to be appended to the value				*/	 		
static int QueueSetValue(QUEUE *pQueue,int ilIndex,int *ipOutDatPos,const char *pcpKey,const char *pcpData)
{
	if (pQueue == NULL)
		return RC_FAIL;

	if (ilIndex < 0 || ilIndex >= pQueue->Size)
		return RC_FAIL;

	if (ipOutDatPos == NULL)
		return RC_FAIL;
		
	if (pcpKey == NULL)
		return RC_FAIL;
		
	if (pcpData == NULL)
		return RC_FAIL;
	
	StrgPutStrg(pQueue->BcBuffer[ilIndex].Value,ipOutDatPos,pcpKey,0,-1,pcpData);
    
    /* check, if ilOutDatPos matches allocated size	*/
    if ((*ipOutDatPos) >= pQueue->BcBuffer[ilIndex].AllocatedSize)
    {
		return RC_FAIL;
	}
	
    return RC_SUCCESS;
						
}

/* sets the used length of the value of a specific event in the ring buffer of an instance of the QUEUE class	*/
/* pQueue		QUEUE	*	the 'this' pointer of the instance	of this class	*/
/* ilIndex		int			the index of the event within the ring buffer		*/
/* ipOutDatPos	int		*	the length of the value to be set */
static int QueueSetUsedLen(QUEUE *pQueue,int ilIndex,int ipOutDatPos)
{
	if (pQueue == NULL)
		return RC_FAIL;
		
	if (ilIndex < 0 || ilIndex >= pQueue->Size)
		return RC_FAIL;

    /* check, if ilOutDatPos matches allocated size	*/
    if (ipOutDatPos < 0 || ipOutDatPos >= pQueue->BcBuffer[ilIndex].AllocatedSize)
    {
		return RC_FAIL;
	}
	
   	pQueue->BcBuffer[ilIndex].Value[ipOutDatPos] = 0x00;
	pQueue->BcBuffer[ilIndex].UsedLen = ipOutDatPos;
   	
    return RC_SUCCESS;
}

/* returns the command of a specific event in the ring buffer of an instance of the QUEUE class	*/
/* pQueue		QUEUE	*	the 'this' pointer of the instance	of this class	*/
/* ilIndex		int			the index of the event within the ring buffer		*/
static char *QueueGetCmd(QUEUE *pQueue,int ilIndex)
{
	if (pQueue == NULL)
		return NULL;

	if (ilIndex < 0 || ilIndex >= pQueue->Size)
		return NULL;
		
	return 	pQueue->BcBuffer[ilIndex].Cmd;	
}

/* returns the table of a specific event in the ring buffer of an instance of the QUEUE class	*/
/* pQueue		QUEUE	*	the 'this' pointer of the instance	of this class	*/
/* ilIndex		int			the index of the event within the ring buffer		*/
static char *QueueGetTable(QUEUE *pQueue,int ilIndex)
{
	if (pQueue == NULL)
		return NULL;
		
	if (ilIndex < 0 || ilIndex >= pQueue->Size)
		return NULL;
		
	return 	pQueue->BcBuffer[ilIndex].Table;	
}

/* returns the value of a specific event in the ring buffer of an instance of the QUEUE class	*/
/* pQueue		QUEUE	*	the 'this' pointer of the instance	of this class	*/
/* ilIndex		int			the index of the event within the ring buffer		*/
static char *QueueGetValue(QUEUE *pQueue,int ilIndex)
{
	if (pQueue == NULL)
		return NULL;
		
	if (ilIndex < 0 || ilIndex >= pQueue->Size)
		return NULL;
		
	return 	pQueue->BcBuffer[ilIndex].Value;	
}

/* returns the broadcast number of a specific event in the ring buffer of an instance of the QUEUE class	*/
/* pQueue		QUEUE	*	the 'this' pointer of the instance	of this class	*/
/* ilIndex		int			the index of the event within the ring buffer		*/
static char *QueueGetBcNum(QUEUE *pQueue,int ilIndex)
{
	if (pQueue == NULL)
		return NULL;
		
	if (ilIndex < 0 || ilIndex >= pQueue->Size)
		return NULL;
		
	return 	pQueue->BcBuffer[ilIndex].BcNum;	
}

/* returns the used length of a specific event in the ring buffer of an instance of the QUEUE class	*/
/* pQueue		QUEUE	*	the 'this' pointer of the instance	of this class	*/
/* ilIndex		int			the index of the event within the ring buffer		*/
static int QueueGetUsedLen(QUEUE *pQueue,int ilIndex)
{
	if (pQueue == NULL)
		return -99;
		
	if (ilIndex < 0 || ilIndex >= pQueue->Size)
		return -100;
		
	return 	pQueue->BcBuffer[ilIndex].UsedLen;	
}

/* calculates the next "read" position for the ring buffer of an instance of the QUEUE class	*/
/* releases any allocated memory to prevent from growing process memory							*/
/* pQueue		QUEUE	*	the 'this' pointer of the instance	of this class	*/
static int	QueueUpdatePosition(QUEUE *pQueue)
{
	if (pQueue == NULL)
		return RC_FAIL;

	/* handle "wrapped around" flag at first	*/
	if (pQueue->Wrap == 1)
	{
		while ((pQueue->Head < pQueue->Size) && (strstr(pQueue->BcBuffer[pQueue->Head].Status,"1") == NULL))
		{
	    	if (pQueue->BcBuffer[pQueue->Head].AllocatedSize > 0)
	      	{
	        	free(pQueue->BcBuffer[pQueue->Head].Value);
	        	pQueue->BcBuffer[pQueue->Head].Value = NULL;
	        	pQueue->BcBuffer[pQueue->Head].AllocatedSize = 0;
	        } /* end if */
			pQueue->Head++;
		}

		/* end of buffer reached -> start at the beginning and reset wrap around flag	*/		
		if (pQueue->Head >= pQueue->Size)
		{
			pQueue->Head = 0;
			pQueue->Wrap = 0;
		}
	}
	
	/* handle "not wrapped around" state	*/
	if (pQueue->Wrap == 0)
	{
		while ((pQueue->Head < pQueue->Tail) && (strstr(pQueue->BcBuffer[pQueue->Head].Status,"1") == NULL))
		{
	    	if (pQueue->BcBuffer[pQueue->Head].AllocatedSize > 0)
	      	{
	        	free(pQueue->BcBuffer[pQueue->Head].Value);
	        	pQueue->BcBuffer[pQueue->Head].Value = NULL;
	        	pQueue->BcBuffer[pQueue->Head].AllocatedSize = 0;
	        } /* end if */
			pQueue->Head++;
		}
	}
	
	/* last check */
	if (pQueue->Head >= pQueue->Size)
		return RC_FAIL;
	
	return RC_SUCCESS;				
}

/* check, if the queue is empty	*/
/* pQueue		QUEUE	*	the 'this' pointer of the instance	of this class	*/
static int	QueueIsEmpty(QUEUE *pQueue)
{
	int i;
	
	if (pQueue == NULL)
		return RC_FAIL;

	for (i = 0; i < pQueue->Size; i++)
	{
		if (strstr(pQueue->BcBuffer[i].Status,"1") != NULL)		
			return FALSE;
	}

	return TRUE;				
}

/* module thread management implementation	*/
typedef struct _ThreadMngmnt
{
#ifdef	_SUN_UNIX
	mutex_t			mutex;					/* the mutex for the current thread		*/
	thread_t		ThreadID;				/* the thread ID						*/
#else
	pthread_mutex_t	mutex;					/* the mutex for the current thread		*/
	pthread_t		ThreadID;				/* the thread ID						*/
	pthread_attr_t	ThreadAttribute;		/* the thread attribute					*/
#endif	
	int				Exit;					/* the thread must be closed			*/
	int 			StatusPosition;			/* the status position (in the array)	*/
	int 			Socket;					/* the socket the thread writes			*/
	int				ItemsSend;				/* the count of items sent to the client*/
	int				ItemsTotal;				/* the count of all broadcasts 			*/ 
	char 			ClientIpAddr[32];		/* the IP adress of the client			*/
	char 			ClientHexAddr[32];		/* the hex address of the client		*/
	char*			BCFilter;				/* the broadcast filter					*/
} THREAD_MNGMNT;

/* initializes an instance of the THREAD_MNGMNT class	*/
/* popThreadMngmnt			THREAD_MNGMNT	*	the 'this' pointer of the instance	of this class	*/
/* bpFirst					int					first initialization or not	*/
static int ThreadMngmntInit(THREAD_MNGMNT *popThreadMngmnt,int bpFirst)
{
	if (popThreadMngmnt == NULL)
		return RC_FAIL;
		
	if (bpFirst == TRUE)
	{
#ifdef	_SUN_UNIX
		mutex_init(&popThreadMngmnt->mutex,USYNC_THREAD,NULL);
#else
		pthread_mutex_init(&popThreadMngmnt->mutex,NULL);
		pthread_attr_init(&popThreadMngmnt->ThreadAttribute);
		pthread_attr_setdetachstate(&popThreadMngmnt->ThreadAttribute,PTHREAD_CREATE_DETACHED);
#endif
	}		
	
	popThreadMngmnt->ThreadID = NULL;
	popThreadMngmnt->Exit = FALSE;
	popThreadMngmnt->StatusPosition = -1;
	popThreadMngmnt->Socket = -1;
	popThreadMngmnt->ItemsSend = 0;
	popThreadMngmnt->ItemsTotal = 0;
	popThreadMngmnt->ClientIpAddr[0] = '\0';
	strcpy(popThreadMngmnt->ClientHexAddr, "........");
	
	if (bpFirst == TRUE)
	{
		popThreadMngmnt->BCFilter = NULL;
	}
	else
	{
		if (popThreadMngmnt->BCFilter != NULL)
		{
			free(popThreadMngmnt->BCFilter);
		}
		
		popThreadMngmnt->BCFilter = NULL;
		
	}

	return RC_SUCCESS;
}

/* releases an instance of the THREAD_MNGMNT class	*/
/* popThreadMngmnt			THREAD_MNGMNT	*	the 'this' pointer of the instance	of this class	*/
static int ThreadMngmntRelease(THREAD_MNGMNT *popThreadMngmnt)
{
	if (popThreadMngmnt == NULL)
		return RC_FAIL;

#ifdef	_SUN_UNIX
	mutex_destroy(&popThreadMngmnt->mutex);
#else
	pthread_mutex_destroy(&popThreadMngmnt->mutex);

	pthread_attr_destroy(&popThreadMngmnt->ThreadAttribute);
#endif

	if (popThreadMngmnt->BCFilter != NULL)
	{
		free(popThreadMngmnt->BCFilter);
	}
		
	popThreadMngmnt->BCFilter = NULL;

	return RC_SUCCESS;
}

/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */

static char pcgMyProcName[20] = "\0";
static char cgConfigFile[512] = "\0";
static char pcgBcKeyFlag[4] = "A";

static long lgEvtCnt = 0;
static int igGotAlarm = FALSE;
static int igWaitForAck = FALSE;


/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
static int	ReadConfigEntry(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer);

static int	Reset(void);										/* Reset program          */
static void	Terminate();										/* Terminate program      */
static void	HandleErr(int);										/* Handles general errors */
static void HandleQueErr(int);									/* Handles queuing errors */
static void HandleQueues(void);									/* Waiting for Sts.-switch*/
static int	HandleData(EVENT *prpEvent);						/* Handles event data     */
static int	TranslateEvent(EVENT *prpEvent,int ipIndex, int ipTrimData);/* builds the key item	  */
static void TrimAndFilterCr(char *,char *, char *);
static void get_time(char *s);
static char* GetKeyItem(char *pcpResultBuff, long *plpResultSize,char *pcpTextBuff, char *pcpKeyWord, char *pcpItemEnd,int bpCopyData);
static int	CreateCLOEvent(EVENT **prpEvent,char *pcpAnswer);	/* creates a "CLO" event	*/

/******************************************************************************/
/* functions / variables                                                      */
/******************************************************************************/

/* function prototypes for threads */
void *client_bc_connector(void *);
void *con_request(void *);
void *signal_hand(void *);

void client_bc_cleanup(void *);

/* helper function prototypes */
static int Init_bccom();					/* Does the initialization		*/
static int ReadFromSocket(int socketID,void *vptr,size_t nBytes);
static int WriteToSocket(int socketID,void *vptr,size_t nBytes);
static int ReadKeywordFromSocket(int socketID,char *pcpKeyword,char pcpResult[256]);	/* read keyword from socket	*/
static int SetNonBlocking(int socketID);
static int SocketReadyForWrite(int socketID,struct timeval tv);
static int SocketShutdown(int socketID, int how);

static int CheckQueueStatus(QUEUE *pQueue,int bpCleanup);

/* the BC ring buffer */
static QUEUE argBcQueue;

/* the thread management buffer	*/
static THREAD_MNGMNT *argThreadMngmnt = NULL;

/* the listener thread management buffer	*/
static THREAD_MNGMNT listener_thr;

#ifdef	_SUN_UNIX
/* the	data arrival mutex	*/
mutex_t 			DataArrival;
/* the "wait for data" condition	*/
cond_t 				WaitNext;
/* the data access mutex	*/
rwlock_t			DataAccess;
/* the "CLO" command sent mutex	*/
rwlock_t			CLOSend;
#else
/* the	data arrival mutex	*/
pthread_mutex_t 	DataArrival;
/* the "wait for data" condition	*/
pthread_cond_t 		WaitNext;
/* the data access mutex	*/
pthread_rwlock_t	DataAccess;
/* the "CLO" command sent mutex	*/
pthread_rwlock_t	CLOSend;
#endif

int debug_level = TRACE;
int	tcp_port 	= 3359;
char not_filtered[512] = "SBC,CLO";	

static igStopQueGet = FALSE;
static igStopBcHdl = FALSE;

char log_file_name[256];
char signal_log_file_name[256];

/* the log file for all threads	except the main thread	*/
static	MT_DBG_LOGFILE	log_file;

static size_t max_bc_clients 	=  500;
static size_t max_bc_buffers 	= 1000;
static int	  client_handshake 	= FALSE;
static int	  reuse_address		= 0;
static struct timeval	mainsleep;
static char   pcgService[16]; /* the client service socket from config-file*/
static int	  reply_delay		= 0;			
static int	  last_head			= -1;
static int	  use_shutdown		= 1;
static int	  use_linger		= 0;
static int	  send_threadId		= 1;	
static int	  simulateReplyDelay= 0;
static int	  max_threadCount	= 0;
		
/******************************************************************************/
/*                                                                            */
/******************************************************************************/

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
	int	ilRc = RC_SUCCESS;			/* Return code			*/
	int	ilCnt = 0;
	int	index;
	int ilClientCount = 0;
	sigset_t set;

	INITIALIZE;			/* General initialization	*/

#ifdef	_SUN_UNIX
	sigfillset(&set);
	thr_sigsetmask(SIG_SETMASK, &set, NULL);
	thr_create(NULL, 0, signal_hand, NULL,0,NULL);
#else	
	/* block all signals in main thread.  Any other threads that are
	created after this will also block all signals */
	sigfillset(&set);
	pthread_sigmask(SIG_SETMASK, &set, NULL);

	/* create a signal handler thread.  This thread will catch all
	signals and decide what to do with them.  This will only
	catch nondirected signals.  (I.e., if a thread causes a SIGFPE 
	then that thread will get that signal. */
	pthread_create(NULL, 0, signal_hand, NULL);
#endif

	debug_level = TRACE;

	strcpy(pcgMyProcName,argv[0]);

	dbg(TRACE,"MAIN: version <%s>",mks_version);

	/* Attach to the MIKE queues */
	do
	{
		ilRc = init_que();
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		}/* end of if */
	}while((ilCnt < 10) && (ilRc != RC_SUCCESS));

	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
		sleep(60);
		exit(1);
	}
	else
	{
		dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
	}/* end of if */

	sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
	dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
	ilRc = TransferFile(cgConfigFile);
	if(ilRc != RC_SUCCESS)
	{ 
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */

	ilRc = SendRemoteShutdown(mod_id);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
	} /* end of if */

	if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
	{
		dbg(DEBUG,"MAIN: waiting for status switch ...");
		HandleQueues();
	}/* end of if */

	if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
	{
		dbg(TRACE,"MAIN: initializing ...");
		ilRc = Init_bccom();

#ifdef	_SUN_UNIX
		/* get exclusive access to listener thread management area	*/
		mutex_lock(&listener_thr.mutex);

		/* start the thread for listening to the port */
		dbg(TRACE,"MAIN: Starting listining thread (port %d)",tcp_port);
		ilRc = thr_create(NULL, 0,con_request, NULL,THR_DETACHED,&listener_thr.ThreadID);
		dbg(TRACE,"MAIN: ilRc from pthread_create = %d,%s", ilRc,strerror(errno));
	
		if (listener_thr.ThreadID != NULL)
		{
			dbg(TRACE,"MAIN: listener Thread ID is valid!");
		}
		else
		{
			dbg(TRACE,"MAIN: listener Thread ID is NULL!");
		}

		/* release exclusive access to listener thread management area	*/
		mutex_unlock(&listener_thr.mutex);
#else
		/* get exclusive access to listener thread management area	*/
		pthread_mutex_lock(&listener_thr.mutex);

		/* start the thread for listening to the port */
		dbg(TRACE,"MAIN: Starting listining thread (port %d)",tcp_port);
		ilRc = pthread_create(&listener_thr.ThreadID, &listener_thr.ThreadAttribute, con_request, NULL);
		dbg(TRACE,"MAIN: ilRc from pthread_create = %d,%s", ilRc,strerror(errno));
	
		if (listener_thr.ThreadID != NULL)
		{
			dbg(TRACE,"MAIN: listener Thread ID is valid!");
		}
		else
		{
			dbg(TRACE,"MAIN: listener Thread ID is NULL!");
		}

		/* release exclusive access to listener thread management area	*/
		pthread_mutex_unlock(&listener_thr.mutex);
#endif

		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"bccom: init failed!");
		} /* end of if */

	}
	else
	{
		Terminate();
	}/* end of if */

	dbg(TRACE,"=====================");
	dbg(TRACE,"MAIN: initializing OK");
	dbg(TRACE,"=====================");

	for(;;)
	{
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);

		/* Acknowledge the item */
		ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);

		if( ilRc != RC_SUCCESS ) 
		{
			/* handle que_ack error */
			HandleQueErr(ilRc);
		} /* fi */

		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */

		prgEvent = (EVENT *) prgItem->text;

		if( ilRc == RC_SUCCESS )
		{
			lgEvtCnt++;

			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				break;	
			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				/* CloseConnection(); */
				HandleQueues();
				break;	
			case	HSB_DOWN	:
				/* whole system SocketShutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate();
				break;	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				break;	
			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;
			case	SHUTDOWN	:
				/* process SocketShutdown - maybe from uutil */
				Terminate();
				break;
			case	RESET		:
				ilRc = Reset();
				break;
			case	EVENT_DATA	:
				if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
				{
					ilRc = HandleData(prgEvent);
					if(ilRc != RC_SUCCESS)
					{
						HandleErr(ilRc);
					}/* end of if */
				}
				else
				{
					dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
				}/* end of if */
				break;
			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				
				/* initialize debug information for the threads log file	*/
#ifdef	_SUN_UNIX			  		
				/* get exclusive access to threads log file	*/
				mutex_lock(&log_file.mutex);
#else
				/* get exclusive access to threads log file	*/
				pthread_mutex_lock(&log_file.mutex);
#endif					
			  	log_file.Debug_Level = debug_level;
			  		
#ifdef	_SUN_UNIX			  		
				/* release exclusive access to threads log file	*/
				mutex_unlock(&log_file.mutex);
#else
				/* release exclusive access to threads log file	*/
				pthread_mutex_unlock(&log_file.mutex);
#endif
				break;
			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				
				/* initialize debug information for the threads log file	*/
#ifdef	_SUN_UNIX			  		
				/* get exclusive access to threads log file	*/
				mutex_lock(&log_file.mutex);
#else
				/* get exclusive access to threads log file	*/
				pthread_mutex_lock(&log_file.mutex);
#endif					
			  	log_file.Debug_Level = debug_level;
			  		
#ifdef	_SUN_UNIX			  		
				/* release exclusive access to threads log file	*/
				mutex_unlock(&log_file.mutex);
#else
				/* release exclusive access to threads log file	*/
				pthread_mutex_unlock(&log_file.mutex);
#endif
				break;
			case	-101:	/*	enable/disable ring buffer overflow scenario	*/
				simulateReplyDelay = !simulateReplyDelay;
				if (simulateReplyDelay != 0)
				{
					dbg(TRACE,"MAIN: SIMULATION - ring buffer too small scenario is enabled");
				}
				else
				{
					dbg(TRACE,"MAIN: SIMULATION - ring buffer too small scenario is disabled");
				}
				break;
			case	102	:	/*	calculate count of connected clients	*/
				dbg(TRACE,"MAIN: Display currently connected client thread information ...");
				dbg(TRACE,"%s,%s,%s,%s,%s,%s,%s\n","Queue index","Thread Id","Socket Id","Client IP Address","Client Hex Address","Items sent","Items total");
				ilClientCount = 0;
				
#ifdef	_SUN_UNIX			  		
				for (index = 0; index < max_bc_clients; index++)
				{
					/* get exclusive access to thread management area	*/
					mutex_lock(&argThreadMngmnt[index].mutex);
					
					if (argThreadMngmnt[index].ThreadID != NULL)
					{
						++ilClientCount;
						dbg(TRACE,"%d,%d,%d,%s,%s,%d,%d\n",index,argThreadMngmnt[index].ThreadID,argThreadMngmnt[index].Socket,argThreadMngmnt[index].ClientIpAddr,argThreadMngmnt[index].ClientHexAddr,argThreadMngmnt[index].ItemsSend,argThreadMngmnt[index].ItemsTotal);
						
					}
					
					/* release exclusive access to thread management area	*/
					mutex_unlock(&argThreadMngmnt[index].mutex);
				}
#else
				for (index = 0; index < max_bc_clients; index++)
				{
					/* get exclusive access to thread management area	*/
					pthread_mutex_lock(&argThreadMngmnt[index].mutex);
					
					if (argThreadMngmnt[index].ThreadID != NULL)
					{
						++ilClientCount;
						dbg(TRACE,"%d,%d,%d,%s,%s,%d,%d\n",index,argThreadMngmnt[index].ThreadID,argThreadMngmnt[index].Socket,argThreadMngmnt[index].ClientIpAddr,argThreadMngmnt[index].ClientHexAddr,argThreadMngmnt[index].ItemsSend,argThreadMngmnt[index].ItemsTotal);
					}
					
					/* release exclusive access to thread management area	*/
					pthread_mutex_unlock(&argThreadMngmnt[index].mutex);
				}

#endif
				dbg(TRACE,"MAIN: %d client threads are currently connected...",ilClientCount);
				break;
				
			default			:
				dbg(TRACE,"MAIN: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		}
		else
		{
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		} /* end else */
	} /* end for */
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/

static int ReadConfigEntry(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer)
{
    int ilRc = RC_SUCCESS;

    char clSection[124] = "\0";
    char clKeyword[124] = "\0";

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRc = iGetConfigEntry(cgConfigFile,clSection,clKeyword,
			   CFG_STRING,pcpCfgBuffer);
    if(ilRc != RC_SUCCESS)
    {
	dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
    } 
    else
    {
	dbg(DEBUG,"Config Entry <%s>,<%s>:<%s> found in %s",
	    clSection, clKeyword ,pcpCfgBuffer, cgConfigFile);
    }/* end of if */
    return ilRc;
}

static int GetDebugLevel(int *pipMode)
{
	int		ilRc = RC_SUCCESS;
	char	clCfgValue[64];

	ilRc = iGetConfigRow(cgConfigFile, "MAIN","debug_level", CFG_STRING,clCfgValue);

	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN %s not found in <%s>", "debug_level", cgConfigFile);
	}
	else
	{
		dbg(TRACE, "MAIN %s <%s>", "debug_level", clCfgValue);
		if (!strcmp(clCfgValue, "DEBUG"))
			*pipMode = DEBUG;
		else if (!strcmp(clCfgValue, "TRACE"))
			*pipMode = TRACE;
		else
			*pipMode = 0;
	}

	return ilRc;
}


/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
	int	ilRc = RC_SUCCESS;				/* Return code */
	
	dbg(TRACE,"Reset: now resetting");
	
	return ilRc;
	
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate()
{
	int i;
	int rc;
	int blEmpty = FALSE;
	EVENT	*prlCLOEvent = NULL;

	/* unset SIGCHLD ! DB-Child will terminate ! */

	dbg(TRACE,"Terminate: now DB logoff ...");

	signal(SIGCHLD,SIG_IGN);

	logoff();

#ifdef	_SUN_UNIX
	/* get exclusive access to listener thread management area	*/
	mutex_lock(&listener_thr.mutex);
	
	/* Signal listener thread at first since we can't accept further connections */
	if (listener_thr.ThreadID != NULL)
	{
		listener_thr.Exit = TRUE;
	}
	
	/* SocketShutdown listener port	*/
	if (listener_thr.Socket != -1)
	{
		dbg(TRACE,"Terminate: Shutdown socket %d (listener)",listener_thr.Socket);
		SocketShutdown(listener_thr.Socket,SHUT_RDWR);
		dbg(TRACE,"Terminate: Closing socket %d (listener)",listener_thr.Socket);
		close(listener_thr.Socket);
	}
	
	/* release exclusive access to listener thread management area	*/
	mutex_unlock(&listener_thr.mutex);
#else
	/* get exclusive access to listener thread management area	*/
	pthread_mutex_lock(&listener_thr.mutex);
	
	/* Signal listener thread at first since we can't accept further connections */
	if (listener_thr.ThreadID != NULL)
	{
		listener_thr.Exit = TRUE;
		rc = pthread_cancel(listener_thr.ThreadID);
		dbg(TRACE,"Listener thread canceled with rc = %d",rc);
		
		rc = pthread_detach(listener_thr.ThreadID);
		dbg(TRACE,"Listener thread detached with rc = %d",rc);
	}

	/* SocketShutdown listener port	*/
	if (listener_thr.Socket != -1)
	{
		dbg(TRACE,"Terminate: Shutdown socket %d (listener)",listener_thr.Socket);
		SocketShutdown(listener_thr.Socket,SHUT_RDWR);
		dbg(TRACE,"Terminate: Closing socket %d (listener)",listener_thr.Socket);
		close(listener_thr.Socket);
	}
	
	/* release exclusive access to listener thread management area	*/
	pthread_mutex_unlock(&listener_thr.mutex);
#endif
	/* create a "CLO" event		*/
	rc = CreateCLOEvent(&prlCLOEvent,"");	
	if (rc != RC_SUCCESS)
	{
		dbg(TRACE,"CreateCLOEvent failed with rc = %d",rc);
	}
	else
	{
		/* send "CLO" event to clients	*/
		rc = HandleData(prlCLOEvent);
		dbg(TRACE,"Terminate event sent to clients with rc = %d",rc);
	}
	
	/* release memory	*/
	free(prlCLOEvent);

	sleep(1);

	dbg(TRACE,"Waiting for write lock");

#ifdef	_SUN_UNIX
	/* wait until all readers are ready	*/
	rw_wrlock(&CLOSend);

	/* now terminate all client connection threads	*/
	dbg(TRACE,"Terminate: Sending exit signal to all connected client threads ...");
	for (i = 0; i < max_bc_clients; i++)
	{
		/* get exclusive access to thread management area	*/
		mutex_lock(&argThreadMngmnt[i].mutex);
		
		/* detach the working thread	*/		
		if (argThreadMngmnt[i].ThreadID != NULL)
		{
			argThreadMngmnt[i].Exit = TRUE;
		}
		
		if (argThreadMngmnt[i].Socket != -1)
		{
			/* SocketShutdown socket connection	*/
			dbg(TRACE,"Terminate: Shutdown socket %d (connection with client %s)", argThreadMngmnt[i].Socket, argThreadMngmnt[i].ClientIpAddr);
			SocketShutdown(argThreadMngmnt[i].Socket,SHUT_RDWR);
			
			/* closing open sockets */
			dbg(TRACE,"Terminate: Closing socket %d (connection with client %s)", argThreadMngmnt[i].Socket, argThreadMngmnt[i].ClientIpAddr);
			close(argThreadMngmnt[i].Socket);
		}
		
		/* release exclusive access to thread management area	*/
		mutex_unlock(&argThreadMngmnt[i].mutex);
		
	}

	dbg(TRACE,"Releasing allocated resources");
	
	/* release queue	*/
	QueueRelease(&argBcQueue);
	
	/* release thread management	*/
  	for (i = 0; i < max_bc_clients; i++)
  	{
  		rc = ThreadMngmntRelease(&argThreadMngmnt[i]);
	}
	
	free(argThreadMngmnt);

	/* release listener thread management	*/
	rc = ThreadMngmntRelease(&listener_thr);
	
	/* release threads log file	*/
	rc = MtDbgLogFileRelease(&log_file);
	
	/* release condition variables	*/
	rc = cond_destroy(&WaitNext);
			
	/* release mutexes	*/
	rc = mutex_destroy(&DataArrival);
	rc = rwlock_destroy(&DataAccess);			
	rc = rwlock_destroy(&CLOSend);			
#else
	/* wait until all readers are ready	*/
	pthread_rwlock_wrlock(&CLOSend);

	/* now terminate all client connection threads	*/
	dbg(TRACE,"Terminate: Closing open sockets ...");
	for (i = 0; i < max_bc_clients; i++)
	{
		/* get exclusive access to thread management area	*/
		pthread_mutex_lock(&argThreadMngmnt[i].mutex);
		
		/* detach the working thread	*/		
		if (argThreadMngmnt[i].ThreadID != NULL)
		{
			argThreadMngmnt[i].Exit = TRUE;
			rc = pthread_cancel(argThreadMngmnt[i].ThreadID);
			dbg(TRACE,"Thread %d cancelled with rc = %d",argThreadMngmnt[i].ThreadID,rc);
			
			rc = pthread_detach(argThreadMngmnt[i].ThreadID);
			dbg(TRACE,"Thread %d detached with rc = %d",argThreadMngmnt[i].ThreadID,rc);
		}

		if (argThreadMngmnt[i].Socket != -1)
		{
			/* SocketShutdown socket connection	*/
			dbg(TRACE,"Terminate: Shutdown socket %d (connection with client %s)", argThreadMngmnt[i].Socket, argThreadMngmnt[i].ClientIpAddr);
			SocketShutdown(argThreadMngmnt[i].Socket,SHUT_RDWR);
			
			/* closing open sockets */
			dbg(TRACE,"Terminate: Closing socket %d (connection with client %s)", argThreadMngmnt[i].Socket, argThreadMngmnt[i].ClientIpAddr);
			close(argThreadMngmnt[i].Socket);
		}
		
		/* release exclusive access to thread management area	*/
		pthread_mutex_unlock(&argThreadMngmnt[i].mutex);
		
	}

	dbg(TRACE,"Releasing allocated resources");
	
	/* release queue	*/
	QueueRelease(&argBcQueue);
	
	/* release thread management	*/
  	for (i = 0; i < max_bc_clients; i++)
  	{
  		rc = ThreadMngmntRelease(&argThreadMngmnt[i]);
	}
	
	free(argThreadMngmnt);

	/* release listener thread management	*/
	rc = ThreadMngmntRelease(&listener_thr);
	
	/* release condition variables	*/
	rc = pthread_cond_destroy(&WaitNext);
			
	/* release mutexes	*/
	rc = pthread_mutex_destroy(&DataArrival);
	rc = pthread_rwlock_destroy(&DataAccess);			
	rc = pthread_rwlock_destroy(&CLOSend);			
#endif
			
	dbg(TRACE,"Terminate: now leaving ...");

	fclose(outp);

	exit(0);

} /* end of Terminate */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
	dbg(TRACE,"HandleErr: <%d> : unknown ERROR",pipErr);

	return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
	switch(pipErr) {
	case	QUE_E_FUNC	:	/* Unknown function */
		dbg(TRACE,"<%d> : unknown function",pipErr);
		break;
	case	QUE_E_MEMORY	:	/* Malloc reports no memory */
		dbg(TRACE,"<%d> : malloc failed",pipErr);
		break;
	case	QUE_E_SEND	:	/* Error using msgsnd */
		dbg(TRACE,"<%d> : msgsnd failed",pipErr);
		break;
	case	QUE_E_GET	:	/* Error using msgrcv */
		dbg(TRACE,"<%d> : msgrcv failed",pipErr);
		break;
	case	QUE_E_EXISTS	:
		dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
		break;
	case	QUE_E_NOFIND	:
		dbg(TRACE,"<%d> : route not found ",pipErr);
		break;
	case	QUE_E_ACKUNEX	:
		dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
		break;
	case	QUE_E_STATUS	:
		dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
		break;
	case	QUE_E_INACTIVE	:
		dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
		break;
	case	QUE_E_MISACK	:
		dbg(TRACE,"<%d> : missing ack ",pipErr);
		break;
	case	QUE_E_NOQUEUES	:
		dbg(TRACE,"<%d> : queue does not exist",pipErr);
		break;
	case	QUE_E_RESP	:	/* No response on CREATE */
		dbg(TRACE,"<%d> : no response on create",pipErr);
		break;
	case	QUE_E_FULL	:
		dbg(TRACE,"<%d> : too many route destinations",pipErr);
		break;
	case	QUE_E_NOMSG	:	/* No message on queue */
		dbg(TRACE,"<%d> : no messages on queue",pipErr);
		break;
	case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
		dbg(TRACE,"<%d> : invalid originator=0",pipErr);
		break;
	case	QUE_E_NOINIT	:	/* Queues is not initialized*/
		dbg(TRACE,"<%d> : queues are not initialized",pipErr);
		break;
	case	QUE_E_ITOBIG	:
		dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
		break;
	case	QUE_E_BUFSIZ	:
		dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
		break;
	default			:	/* Unknown queue error */
		dbg(TRACE,"<%d> : unknown error",pipErr);
		break;
	} /* end switch */
         
	return;

} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
	int	ilRc = RC_SUCCESS;			/* Return code */
	int	ilBreakOut = FALSE;
	int	i;
	
	do
	{
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;	

		if( ilRc == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRc != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRc);
			} /* fi */

			switch( prgEvent->command )
			{
			case	HSB_STANDBY:
				ctrl_sta = prgEvent->command;
				break;	

			case	HSB_COMING_UP:
				ctrl_sta = prgEvent->command;
				break;	

			case	HSB_ACTIVE:
				ctrl_sta = prgEvent->command;
				ilBreakOut = TRUE;
				break;	

			case	HSB_ACT_TO_SBY:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_DOWN:
				/* whole system SocketShutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate();
				break;	
	
			case	HSB_STANDALONE:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				ilBreakOut = TRUE;
				break;	

			case	REMOTE_DB:
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;

			case	SHUTDOWN:
				Terminate();
				break;

			case	RESET:
				ilRc = Reset();
				break;

			case	EVENT_DATA:
				dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;

			case	TRACE_ON:
				dbg_handle_debug(prgEvent->command);
		
				/* initialize debug information for the threads log file	*/
#ifdef	_SUN_UNIX			  		
				/* get exclusive access to threads log file	*/
				mutex_lock(&log_file.mutex);
#else
				/* get exclusive access to threads log file	*/
				pthread_mutex_lock(&log_file.mutex);
#endif					
			  	log_file.Debug_Level = debug_level;
			  		
#ifdef	_SUN_UNIX			  		
				/* release exclusive access to threads log file	*/
				mutex_unlock(&log_file.mutex);
#else
				/* release exclusive access to threads log file	*/
				pthread_mutex_unlock(&log_file.mutex);
#endif
				break;

			case	TRACE_OFF:
				dbg_handle_debug(prgEvent->command);
				
				/* initialize debug information for the threads log file	*/
#ifdef	_SUN_UNIX			  		
				/* get exclusive access to threads log file	*/
				mutex_lock(&log_file.mutex);
#else
				/* get exclusive access to threads log file	*/
				pthread_mutex_lock(&log_file.mutex);
#endif					
			  	log_file.Debug_Level = debug_level;
			  		
#ifdef	_SUN_UNIX			  		
				/* release exclusive access to threads log file	*/
				mutex_unlock(&log_file.mutex);
#else
				/* release exclusive access to threads log file	*/
				pthread_mutex_unlock(&log_file.mutex);
#endif
				break;

			default:
				dbg(TRACE,"HandleQueues: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		} /* end else */
	} while (ilBreakOut == FALSE);

} /* end of HandleQueues */

/***************************/
/* The handle data routine */
/***************************/
static int HandleData(EVENT *prpEvent)
{
	int		ilRc           = RC_SUCCESS;			/* Return code */
	int		i;
	int		ilTail;
	int		ilUsedLen;

	BC_HEAD *prlBchead       = NULL;
	CMDBLK  *prlCmdblk       = NULL;
	char    *pclSelection    = NULL;
	char    *pclFields       = NULL;
	char    *pclData         = NULL;


	prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
	prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
	pclSelection = prlCmdblk->data;
	pclFields    = pclSelection + strlen(pclSelection) + 1;
	pclData      = pclFields + strlen(pclFields) + 1;

	dbg(TRACE,"========== START <%10.10d> ==========",lgEvtCnt);

	/****************************************/
	DebugPrintBchead(DEBUG,prlBchead);
	DebugPrintCmdblk(DEBUG,prlCmdblk);
	dbg(TRACE,"Command: <%s>",prlCmdblk->command);
	dbg(TRACE,"originator follows event = %p ",prpEvent);
	dbg(TRACE,"originator<%d>",prpEvent->originator);
	dbg(TRACE,"selection follows Selection = %p ",pclSelection);
	dbg(TRACE,"selection <%s>",pclSelection);
	dbg(TRACE,"fields    <%s>",pclFields);
	dbg(TRACE,"data      <%s>",pclData);
	/****************************************/
#ifdef	_SUN_UNIX
	/* now we lock the mutex and insert the BC in the bc buffer*/
	rw_wrlock(&DataAccess);

	ilTail = QueueGetNextFreePosition(&argBcQueue,TRUE);
	if (ilTail < 0)	
	{
		/* TODO: message to alerter */
		dbg(TRACE,"QueueGetNextFreePosition: ERROR - ring buffer too small,cleaning up not sending threads!");
		ilRc = CheckQueueStatus(&argBcQueue,TRUE);
		dbg(TRACE,"HandleData : Update Pos after cleaning at head = %d,tail = %d,wrap = %d",argBcQueue.Head,argBcQueue.Tail,argBcQueue.Wrap);
		
		if (ilRc == RC_SUCCESS)
		{
			ilTail = QueueGetNextFreePosition(&argBcQueue,FALSE);
			dbg(TRACE,"HandleData : Update Pos after 2nd QueueGetNextFreePosition at head = %d,tail = %d,wrap = %d",argBcQueue.Head,argBcQueue.Tail,argBcQueue.Wrap);
		}
		
		if (ilRc != RC_SUCCESS || ilTail < 0)
		{
			dbg(TRACE,"QueueGetNextFreePosition: ERROR - ring buffer too small, cleaning not successfull, ilRc = %d, ilTail = %d!",ilRc,ilTail);
			/* unlock the mutex */
			rw_unlock(&DataAccess);
			return RC_FAIL;
		}
	}

	/* check current queue and thread state	*/
	if (reply_delay > 10)
	{
		/* first time check ?	*/
		if (last_head == -1)
		{
			last_head = argBcQueue.Head;
		}
		else if (last_head == argBcQueue.Head)	/* head position not changed ?	*/
		{
			int ilDiff = 0;
			if (argBcQueue.Wrap == 0)
			{
				ilDiff = argBcQueue.Tail - argBcQueue.Head;
			}
			else
			{
				ilDiff = argBcQueue.Size - argBcQueue.Head + argBcQueue.Tail;				
			}
			
			dbg(TRACE,"HandleData: diff between Tail and head = %d",ilDiff);
			/* difference exceeds reply delay ?	*/
			if (abs(ilDiff) > reply_delay)
			{
				/* TODO: message to alerter */
				dbg(TRACE,"HandleData: ERROR - reply delay exceeded,cleaning up not sending threads!");
				ilRc = CheckQueueStatus(&argBcQueue,TRUE);
				dbg(TRACE,"HandleData : Update Pos after cleaning at head = %d,tail = %d,wrap = %d",argBcQueue.Head,argBcQueue.Tail,argBcQueue.Wrap);
			}
			
			last_head = argBcQueue.Head;
		}
		else
		{
			last_head = argBcQueue.Head;
		}
	}
	else
	{
		CheckQueueStatus(&argBcQueue,FALSE);
	}


	dbg(TRACE,"HandleData : Inserting BC with BCNum = %d at head = %d,tail = %d,wrap = %d",prlBchead->bc_num,argBcQueue.Head,argBcQueue.Tail,argBcQueue.Wrap);
	
	/* build the status string */
	for (i = 0; i < max_bc_clients; i++)
	{
		/* get exclusive access to thread management area	*/
		mutex_lock(&argThreadMngmnt[i].mutex);		
		if (argThreadMngmnt[i].ThreadID != NULL && argThreadMngmnt[i].Socket != -1)
		{
			ilRc = QueueSetStatus(&argBcQueue,ilTail,i,'1');
		}
		else
		{
			ilRc = QueueSetStatus(&argBcQueue,ilTail,i,'0');
		}
		/* release exclusive access to thread management area	*/
		mutex_unlock(&argThreadMngmnt[i].mutex);	
		
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"QueueSetStatus failed with ec = %d",ilRc);
			/* unlock the mutex */
			rw_unlock(&DataAccess);
			return ilRc;	
		}
	}

	ilRc = QueueSetStatus(&argBcQueue,ilTail,max_bc_clients,'\0');
	
	dbg(DEBUG,"Status string for item %d: %s", ilTail,QueueGetStatus(&argBcQueue,ilTail));

	/* building key item string of the event */
	ilRc = TranslateEvent(prpEvent,ilTail,TRUE);
	if (ilRc != RC_SUCCESS)
	{
		/* unlock the mutex */
		rw_unlock(&DataAccess);
		/* ignore invalid event	*/
		return ilRc;		
	}

	ilUsedLen = QueueGetUsedLen(&argBcQueue,ilTail);
	dbg(TRACE,"QueueGetUsedLen for item %d: %d", ilTail,ilUsedLen);

	if (ilUsedLen > 0)
	{
		if (QueueGetValue(&argBcQueue,ilTail) != NULL)
		{
			/* wake up call will be queued, but since we do have the mutex, the threads are not active yet	*/
			cond_broadcast(&WaitNext);
		}
		else
		{
			dbg(TRACE,"HandleData(): OUT LIST IS NULL !!");
		}
	}
	else if (ilUsedLen == 0)
	{
		dbg(TRACE,"HandleData(): EVENT WITHOUT DATA");
	}
	else
	{
		switch (ilUsedLen)
		{
			case -1:
				dbg(TRACE,"HandleData(): UNEXPECTED <BCOFF> RECEIVED");
				break;
			case -2:
				dbg(TRACE,"HandleData(): UNEXPECTED <BCKEY> RECEIVED");
				break;
			case -4:
				dbg(TRACE,"HandleData(): UNEXPECTED <BCINF> RECEIVED");
				break;
			default:
				break;
		}
	}

	ilRc = QueueUpdatePosition(&argBcQueue);
	dbg(DEBUG,"HandleData : Update Pos after BC at head = %d,tail = %d,wrap = %d",argBcQueue.Head,argBcQueue.Tail,argBcQueue.Wrap);
	
	if (ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"QueueUpdatePosition: ERROR - ring buffer too small,cleaning up not sending threads!");
		ilRc = CheckQueueStatus(&argBcQueue,TRUE);
		if (ilRc == RC_SUCCESS)
		{
			dbg(DEBUG,"HandleData : Update Pos after BC at head = %d,tail = %d,wrap = %d",argBcQueue.Head,argBcQueue.Tail,argBcQueue.Wrap);
		}
	}

	if (simulateReplyDelay != 0)
	{
		dbg(TRACE,"QueueUpdatePosition: SIMULATION - ring buffer too small scenario,cleaning up not sending threads!");
		ilRc = CheckQueueStatus(&argBcQueue,TRUE);
		if (ilRc == RC_SUCCESS)
		{
			dbg(DEBUG,"HandleData : Update Pos after BC at head = %d,tail = %d,wrap = %d",argBcQueue.Head,argBcQueue.Tail,argBcQueue.Wrap);
		}
				
	}

	/* unlock the mutex */
	rw_unlock(&DataAccess);
	/* at this point the bc client connections will be waked up !	*/
#else
	/* now we lock the mutex and insert the BC in the bc buffer*/
	pthread_rwlock_wrlock(&DataAccess);

	ilTail = QueueGetNextFreePosition(&argBcQueue,TRUE);
	if (ilTail < 0)	
	{
		/* TODO: message to alerter */
		dbg(TRACE,"QueueGetNextFreePosition: ERROR - ring buffer too small,cleaning up not sending threads!");
		ilRc = CheckQueueStatus(&argBcQueue,TRUE);
		dbg(TRACE,"HandleData : Update Pos after cleaning at head = %d,tail = %d,wrap = %d",argBcQueue.Head,argBcQueue.Tail,argBcQueue.Wrap);
		
		if (ilRc == RC_SUCCESS)
		{
			ilTail = QueueGetNextFreePosition(&argBcQueue,FALSE);
			dbg(TRACE,"HandleData : Update Pos after 2nd QueueGetNextFreePosition at head = %d,tail = %d,wrap = %d",argBcQueue.Head,argBcQueue.Tail,argBcQueue.Wrap);
		}
		
		if (ilRc != RC_SUCCESS || ilTail < 0)
		{
			dbg(TRACE,"QueueGetNextFreePosition: ERROR - ring buffer too small, cleaning not successfull, ilRc = %d, ilTail = %d!",ilRc,ilTail);
			/* unlock the mutex */
			pthread_rwlock_unlock(&DataAccess);
			return RC_FAIL;
		}
	}

	/* check current queue and thread state	*/
	if (reply_delay > 10)
	{
		/* first time check ?	*/
		if (last_head == -1)
		{
			last_head = argBcQueue.Head;
		}
		else if (last_head == argBcQueue.Head)	/* head position not changed ?	*/
		{
			int ilDiff = 0;
			if (argBcQueue.Wrap == 0)
			{
				ilDiff = argBcQueue.Tail - argBcQueue.Head;
			}
			else
			{
				ilDiff = argBcQueue.Size - argBcQueue.Head + argBcQueue.Tail;				
			}
			
			dbg(TRACE,"HandleData: diff between Tail and head = %d",ilDiff);
			/* difference exceeds reply delay ?	*/
			if (abs(ilDiff) > reply_delay)
			{
				/* TODO: message to alerter */
				dbg(TRACE,"HandleData: ERROR - reply delay exceeded,cleaning up not sending threads!");
				ilRc = CheckQueueStatus(&argBcQueue,TRUE);
				dbg(TRACE,"HandleData : Update Pos after cleaning at head = %d,tail = %d,wrap = %d",argBcQueue.Head,argBcQueue.Tail,argBcQueue.Wrap);
			}
			
			last_head = argBcQueue.Head;
		}
		else
		{
			last_head = argBcQueue.Head;
		}
	}
	else
	{
		CheckQueueStatus(&argBcQueue,FALSE);
	}


	dbg(TRACE,"HandleData : Inserting BC with BCNum = %d at head = %d,tail = %d,wrap = %d",prlBchead->bc_num,argBcQueue.Head,argBcQueue.Tail,argBcQueue.Wrap);
	
	/* build the status string */
	for (i = 0; i < max_bc_clients; i++)
	{
		/* get exclusive access to thread management area	*/
		pthread_mutex_lock(&argThreadMngmnt[i].mutex);		
		if (argThreadMngmnt[i].ThreadID != NULL && argThreadMngmnt[i].Socket != -1)
		{
			ilRc = QueueSetStatus(&argBcQueue,ilTail,i,'1');
		}
		else
		{
			ilRc = QueueSetStatus(&argBcQueue,ilTail,i,'0');
		}
		/* release exclusive access to thread management area	*/
		pthread_mutex_unlock(&argThreadMngmnt[i].mutex);	
		
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"QueueSetStatus failed with ec = %d",ilRc);
			/* unlock the mutex */
			pthread_rwlock_unlock(&DataAccess);
			return ilRc;	
		}
	}

	ilRc = QueueSetStatus(&argBcQueue,ilTail,max_bc_clients,'\0');
	
	dbg(DEBUG,"Status string for item %d: %s", ilTail,QueueGetStatus(&argBcQueue,ilTail));

	/* building key item string of the event */
	ilRc = TranslateEvent(prpEvent,ilTail,TRUE);
	if (ilRc != RC_SUCCESS)
	{
		/* unlock the mutex */
		pthread_rwlock_unlock(&DataAccess);
		/* ignore invalid event	*/
		return ilRc;		
	}

	ilUsedLen = QueueGetUsedLen(&argBcQueue,ilTail);
	dbg(TRACE,"QueueGetUsedLen for item %d: %d", ilTail,ilUsedLen);

	if (ilUsedLen > 0)
	{
		if (QueueGetValue(&argBcQueue,ilTail) != NULL)
		{
			/* wake up call will be queued, but since we do have the mutex, the threads are not active yet	*/
			pthread_cond_broadcast(&WaitNext);
		}
		else
		{
			dbg(TRACE,"HandleData(): OUT LIST IS NULL !!");
		}
	}
	else if (ilUsedLen == 0)
	{
		dbg(TRACE,"HandleData(): EVENT WITHOUT DATA");
	}
	else
	{
		switch (ilUsedLen)
		{
			case -1:
				dbg(TRACE,"HandleData(): UNEXPECTED <BCOFF> RECEIVED");
				break;
			case -2:
				dbg(TRACE,"HandleData(): UNEXPECTED <BCKEY> RECEIVED");
				break;
			case -4:
				dbg(TRACE,"HandleData(): UNEXPECTED <BCINF> RECEIVED");
				break;
			default:
				break;
		}
	}

	ilRc = QueueUpdatePosition(&argBcQueue);
	dbg(DEBUG,"HandleData : Update Pos after BC at head = %d,tail = %d,wrap = %d",argBcQueue.Head,argBcQueue.Tail,argBcQueue.Wrap);
	
	if (ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"QueueUpdatePosition: ERROR - ring buffer too small,cleaning up not sending threads!");
		ilRc = CheckQueueStatus(&argBcQueue,TRUE);
		if (ilRc == RC_SUCCESS)
		{
			dbg(DEBUG,"HandleData : Update Pos after BC at head = %d,tail = %d,wrap = %d",argBcQueue.Head,argBcQueue.Tail,argBcQueue.Wrap);
		}
	}

	if (simulateReplyDelay != 0)
	{
		dbg(TRACE,"QueueUpdatePosition: SIMULATION - ring buffer too small scenario,cleaning up not sending threads!");
		ilRc = CheckQueueStatus(&argBcQueue,TRUE);
		if (ilRc == RC_SUCCESS)
		{
			dbg(DEBUG,"HandleData : Update Pos after BC at head = %d,tail = %d,wrap = %d",argBcQueue.Head,argBcQueue.Tail,argBcQueue.Wrap);
		}
				
	}

	/* unlock the mutex */
	pthread_rwlock_unlock(&DataAccess);
	/* at this point the bc client connections will be waked up !	*/
#endif

	dbg(TRACE,"==========  END  <%10.10d> ==========",lgEvtCnt);

	/* wait before getting the next event	*/
	nap(mainsleep.tv_usec);
	
	return ilRc;
} /* end of HandleData */


/*********************************************************************/
/* This is the routine that is executed when a thread is cancelled   */
/*********************************************************************/
void client_bc_cleanup1(void *arg)
{
#ifdef	_SUN_UNIX
	mutex_unlock(&DataArrival);
#else	
	pthread_mutex_unlock(&DataArrival);
#endif
}

/*********************************************************************/
/* This is the routine that is executed when a thread is cancelled   */
/*********************************************************************/
void client_bc_cleanup2(void *arg)
{
#ifdef	_SUN_UNIX
	mutex_unlock(&DataArrival);
	
	/* release exclusive lock	*/
	rw_unlock(&CLOSend);
#else
	pthread_mutex_unlock(&DataArrival);
	
	/* release exclusive lock	*/
	pthread_rwlock_unlock(&CLOSend);
#endif
}

/*********************************************************************/
/* This is the routine that is executed when a thread is cancelled   */
/*********************************************************************/
void client_bc_cleanup3(void *arg)
{
	THREAD_MNGMNT* polThreadMngmnt = (THREAD_MNGMNT*) arg;

#ifdef	_SUN_UNIX
	mutex_unlock(&DataArrival);
	
	/* release exclusive lock	*/
	rw_unlock(&CLOSend);
	
	/* release exclusive access to thread management area	*/
	if (polThreadMngmnt != NULL)
	{
		mutex_unlock(&polThreadMngmnt->mutex);
	}
#else
	pthread_mutex_unlock(&DataArrival);
	
	/* release exclusive lock	*/
	pthread_rwlock_unlock(&CLOSend);
	
	/* release exclusive access to thread management area	*/
	if (polThreadMngmnt != NULL)
	{
		pthread_mutex_unlock(&polThreadMngmnt->mutex);
	}
#endif	
}

/*********************************************************************/
/* This is the routine that is executed when a thread is cancelled   */
/*********************************************************************/
void client_bc_cleanup4(void *arg)
{
	THREAD_MNGMNT* polThreadMngmnt = (THREAD_MNGMNT*) arg;

#ifdef	_SUN_UNIX
	mutex_unlock(&DataArrival);
	
	/* release exclusive lock	*/
	rw_unlock(&CLOSend);
	
	/* release exclusive lock	*/
	rw_unlock(&DataAccess);
#else
	pthread_mutex_unlock(&DataArrival);
	
	/* release exclusive lock	*/
	pthread_rwlock_unlock(&CLOSend);
	
	/* release exclusive lock	*/
	pthread_rwlock_unlock(&DataAccess);
#endif
}

/*************************************************************/
/* This is the routine that is executed from a new thread    */
/*************************************************************/
void *client_bc_connector(void *arg)
{
	THREAD_MNGMNT* polThreadMngmnt = (THREAD_MNGMNT*) arg;
#ifdef	_SUN_UNIX	
	thread_t	thread_id = (thread_t)NULL;
#else
	pthread_t	thread_id = (pthread_t)NULL;
#endif
	int			thread_socket = -1;
	int			ilStatusPosition = -1;
	int			ilSentThisTime = 0;
	int			ilBytesToSend = 0;
	int			ilBytesSent = 0;
	int			ilBcIdx = 0;
	int			i;
	int			ilRc;
	int			ilOldBcIdx;
	int			oldState;
	int			oldType;

	char*		pclOutPtr = NULL;
	char		pclResult[256];
	long 		llLine;
	int			blMatchFilter;
	int			blStopThread = FALSE;
	int			blSendEvent;
	char		clClientIpAddr[32];


	/* this code will be called only once at creation time of the thread	*/
	
#ifdef	_SUN_UNIX	
	/* get exclusive access to thread management area	*/
	mutex_lock(&polThreadMngmnt->mutex);

	/* complete thread management information */
	polThreadMngmnt->ThreadID = thr_self();

	thread_id 		= polThreadMngmnt->ThreadID;
	thread_socket 	= polThreadMngmnt->Socket;
	ilStatusPosition = polThreadMngmnt->StatusPosition;
	strcpy(clClientIpAddr,polThreadMngmnt->ClientIpAddr);

	MtDbgLogFileDbg(TRACE,&log_file,"Thread [%d]: Socket number: %d\n",polThreadMngmnt->ThreadID, thread_socket);
	MtDbgLogFileDbg(TRACE,&log_file,"Thread [%d]: Client IP: %s\n",polThreadMngmnt->ThreadID, polThreadMngmnt->ClientIpAddr);
	MtDbgLogFileDbg(TRACE,&log_file,"Thread [%d]: Client hex address: %s\n",polThreadMngmnt->ThreadID, polThreadMngmnt->ClientHexAddr);

	MtDbgLogFileDbg(DEBUG,&log_file,"Thread [%d]: Status position: %d\n",thread_id, ilStatusPosition);
	
	/*	release exclusive access to thread management area	*/
	mutex_unlock(&polThreadMngmnt->mutex);

#else
	/* get exclusive access to thread management area	*/
	pthread_mutex_lock(&polThreadMngmnt->mutex);

	/* complete thread management information */
	polThreadMngmnt->ThreadID = pthread_self();

	thread_id 		= polThreadMngmnt->ThreadID;
	thread_socket 	= polThreadMngmnt->Socket;
	ilStatusPosition = polThreadMngmnt->StatusPosition;
	strcpy(clClientIpAddr,polThreadMngmnt->ClientIpAddr);

	MtDbgLogFileDbg(TRACE,&log_file,"Thread [%d]: Socket number: %d\n",polThreadMngmnt->ThreadID, thread_socket);
	MtDbgLogFileDbg(TRACE,&log_file,"Thread [%d]: Client IP: %s\n", polThreadMngmnt->ThreadID, polThreadMngmnt->ClientIpAddr);
	MtDbgLogFileDbg(TRACE,&log_file,"Thread [%d]: Client hex address: %s\n", polThreadMngmnt->ThreadID, polThreadMngmnt->ClientHexAddr);

	MtDbgLogFileDbg(DEBUG,&log_file,"Thread [%d]: Status position: %d\n", thread_id, ilStatusPosition);
	
	/*	release exclusive access to thread management area	*/
	pthread_mutex_unlock(&polThreadMngmnt->mutex);
#endif
	/* wait on condition variable */
	while (1)
	{
#ifdef	_SUN_UNIX		
		/* wait to get the data */
		mutex_lock(&DataArrival);
    	ilRc = cond_wait(&WaitNext, &DataArrival);
		mutex_unlock(&DataArrival);

		/* get exclusive read lock	*/
		rw_rdlock(&CLOSend);


		/* get exclusive access to thread management area	*/
		mutex_lock(&polThreadMngmnt->mutex);
		
		blStopThread = polThreadMngmnt->Exit;

		/*	release exclusive access to thread management area	*/
		mutex_unlock(&polThreadMngmnt->mutex);

		MtDbgLogFileDbg(DEBUG,&log_file,"Thread [%d,%s]: Exit status : %d\n", thread_id,clClientIpAddr,blStopThread);

		if (!blStopThread)
		{
			/* get exclusive read lock	*/
			rw_rdlock(&DataAccess);
	
			ilBcIdx = QueueGetNextReadPosition(&argBcQueue,-1);
	
			/* release exclusive lock	*/
			rw_unlock(&DataAccess);
			
			MtDbgLogFileDbg(DEBUG,&log_file,"Thread [%d,%s]: Start position in ring buffer: %d\n", thread_id,clClientIpAddr, ilBcIdx);
		}
		
#else
		/* wait to get the data */
		pthread_mutex_lock(&DataArrival);
    	ilRc = pthread_cond_wait(&WaitNext, &DataArrival);
		pthread_mutex_unlock(&DataArrival);

		/* get exclusive read lock	*/
		pthread_rwlock_rdlock(&CLOSend);

		/* get exclusive access to thread management area	*/
		pthread_mutex_lock(&polThreadMngmnt->mutex);
		blStopThread = polThreadMngmnt->Exit;

		/*	release exclusive access to thread management area	*/
		pthread_mutex_unlock(&polThreadMngmnt->mutex);

		MtDbgLogFileDbg(DEBUG,&log_file,"Thread [%d,%s]: Exit status : %d\n", thread_id,clClientIpAddr,blStopThread);

		if (!blStopThread)
		{
			/* get exclusive read lock	*/
			pthread_rwlock_rdlock(&DataAccess);
	
			ilBcIdx = QueueGetNextReadPosition(&argBcQueue,-1);
	
			/* release exclusive lock	*/
			pthread_rwlock_unlock(&DataAccess);
			
			MtDbgLogFileDbg(DEBUG,&log_file,"Thread [%d,%s]: Start position in ring buffer: %d\n", thread_id,clClientIpAddr, ilBcIdx);
		}
	
#endif
		/* sending all BCs not send yet */
		while (ilBcIdx != -1 || blStopThread == TRUE)
		{
#ifdef	_SUN_UNIX			

			if (blStopThread == TRUE)
			{
				blSendEvent = FALSE;
			}
			else
			{
				/* get exclusive read lock	*/
				rw_rdlock(&DataAccess);
	
				blSendEvent = (QueueGetStatus(&argBcQueue,ilBcIdx)[ilStatusPosition] == '1');
	
				/* release exclusive lock	*/
				rw_unlock(&DataAccess);
			}
#else
			pthread_testcancel();
			
			if (blStopThread == TRUE)
			{
				blSendEvent = FALSE;
			}
			else
			{
				/* get exclusive read lock	*/
				pthread_rwlock_rdlock(&DataAccess);
	
				blSendEvent = (QueueGetStatus(&argBcQueue,ilBcIdx)[ilStatusPosition] == '1');
	
				/* release exclusive lock	*/
				pthread_rwlock_unlock(&DataAccess);
			}
#endif
			if (blStopThread  || blSendEvent)
			{
				blMatchFilter = FALSE;
#ifdef	_SUN_UNIX

				if (!blStopThread)
				{
					/* get exclusive access to thread management area	*/
					mutex_lock(&polThreadMngmnt->mutex);
	
					MtDbgLogFileDbg(TRACE,&log_file,"Thread [%d,%s]: Sending item %d, BcNum %s\n", thread_id,clClientIpAddr, ilBcIdx,QueueGetBcNum(&argBcQueue,ilBcIdx));
	
					/* check on non filtered command(s)	*/
					if (strstr(not_filtered,QueueGetCmd(&argBcQueue,ilBcIdx)) != NULL)
					{
						MtDbgLogFileDbg(DEBUG,&log_file,"Thread [%d,%s]: %s command accepted\n", thread_id,clClientIpAddr,QueueGetCmd(&argBcQueue,ilBcIdx));
						blMatchFilter = TRUE;
					}
					/* check, if event matches the filter criteria	*/
					else if (polThreadMngmnt->BCFilter == NULL || strcmp(polThreadMngmnt->BCFilter,".") == 0)
					{
						MtDbgLogFileDbg(DEBUG,&log_file,"Thread [%d,%s]: No BCFilter defined\n", thread_id,clClientIpAddr);
						blMatchFilter = TRUE;
					}
					else if (strstr(polThreadMngmnt->BCFilter,QueueGetTable(&argBcQueue,ilBcIdx)) != NULL)
					{
						MtDbgLogFileDbg(DEBUG,&log_file,"Thread [%d,%s]: Table %s matches BCFilter %s\n", thread_id,clClientIpAddr,QueueGetTable(&argBcQueue,ilBcIdx),polThreadMngmnt->BCFilter);
						blMatchFilter = TRUE;
					}
					else
					{
						MtDbgLogFileDbg(DEBUG,&log_file,"Thread [%d,%s]: Table %s doesn't match BCFilter %s\n", thread_id,clClientIpAddr,QueueGetTable(&argBcQueue,ilBcIdx),polThreadMngmnt->BCFilter);
					}
	
					if (blStopThread == FALSE && blMatchFilter == TRUE)
					{
						++polThreadMngmnt->ItemsSend;
					}
					
					++polThreadMngmnt->ItemsTotal;
					
					/* release exclusive access to thread management area	*/
					mutex_unlock(&polThreadMngmnt->mutex);
				}
				
				if (blStopThread == TRUE)
				{
					int rc = 0;
					
					/* get exclusive read lock	*/
					rw_rdlock(&DataAccess);
					
					for (i = 0; i < max_bc_buffers; i++)
					{
						QueueSetStatus(&argBcQueue,i,ilStatusPosition,'0');
					}

					/* release exclusive lock	*/
					rw_unlock(&DataAccess);

					/* release exclusive lock	*/
					rw_unlock(&CLOSend);
					
					rc = SocketShutdown(thread_socket,SHUT_RDWR);
					if (rc != 0)
					{
						MtDbgLogFileDbg(TRACE,&log_file,"Thread [%d,%s]: ERROR - SocketShutdown socket %d failed with rc = %d,%s!\n", thread_id,clClientIpAddr,thread_socket,errno,strerror(errno));
					}
					
					rc = close(thread_socket);
					if (rc != 0)
					{
						MtDbgLogFileDbg(TRACE,&log_file,"Thread [%d,%s]: ERROR - Closing socket %d, rc = %d,%s!\n", thread_id,clClientIpAddr,thread_socket,errno,strerror(errno));
					}

					/* get exclusive access to thread management area	*/
					mutex_lock(&polThreadMngmnt->mutex);
							
					MtDbgLogFileDbg(TRACE,&log_file,"Thread [%d,%s]: Stop broadcasting and calling pthread_exit((void *)0)\n", thread_id,clClientIpAddr);
					ThreadMngmntInit(&argThreadMngmnt[ilStatusPosition],FALSE);
							
					/* release exclusive access to thread management area	*/
					mutex_unlock(&polThreadMngmnt->mutex);

					thr_exit((void *)0);					
				}
				else if (blMatchFilter == TRUE)
				{
					
					/* sending the data to the client */
					ilBytesToSend = QueueGetUsedLen(&argBcQueue,ilBcIdx);
					pclOutPtr = QueueGetValue(&argBcQueue,ilBcIdx);
					ilBytesSent = 0;
	
					do
					{
						/* prior to send, check if client is able to get it	*/
						ilSentThisTime = write(thread_socket, pclOutPtr, ilBytesToSend);
						
						if (ilSentThisTime <= 0)
						{
							/* get exclusive access to thread management area	*/
							mutex_lock(&polThreadMngmnt->mutex);
								
							MtDbgLogFileDbg(TRACE,&log_file,"Thread [%d,%s]: ERROR - writing to socket %d, rc = %d,%s at line %d!\n", thread_id,clClientIpAddr,thread_socket,errno,strerror(errno),1907);
								
							SocketShutdown(thread_socket,SHUT_RDWR);
							MtDbgLogFileDbg(DEBUG,&log_file,"Thread [%d,%s]: ERROR - SocketShutdown socket %d, rc = %d,%s!\n", thread_id,clClientIpAddr,thread_socket,errno,strerror(errno));
								
							close(thread_socket);
							MtDbgLogFileDbg(DEBUG,&log_file,"Thread [%d,%s]: ERROR - closing socket %d, rc = %d,%s!\n", thread_id,clClientIpAddr,thread_socket,errno,strerror(errno));
	
							/* release exclusive access to thread management area	*/
							mutex_unlock(&polThreadMngmnt->mutex);
	
							/* get exclusive read lock	*/
							rw_rdlock(&DataAccess);
	
							for (i = 0; i < max_bc_buffers; i++)
							{
								QueueSetStatus(&argBcQueue,i,ilStatusPosition,'0');
							}
	
							/* release exclusive lock	*/
							rw_unlock(&DataAccess);
	
							/* release exclusive lock	*/
							rw_unlock(&CLOSend);
	
							/* get exclusive access to thread management area	*/
							mutex_lock(&polThreadMngmnt->mutex);
									
							MtDbgLogFileDbg(TRACE,&log_file,"Thread [%d,%s]: calling pthread_exit((void *)0) at line %d\n", thread_id,clClientIpAddr,1936);
								
							ThreadMngmntInit(polThreadMngmnt,FALSE);
									
							/* release exclusive access to thread management area	*/
							mutex_unlock(&polThreadMngmnt->mutex);
	
							thr_exit((void *)0);
						}
			
						pclOutPtr += ilSentThisTime;
						ilBytesSent += ilSentThisTime;
						ilBytesToSend -= ilSentThisTime;
							
					} 
					while (ilBytesToSend > 0);
						
					if (client_handshake)
					{
						ilRc = ReadKeywordFromSocket(thread_socket,"{=ACK=}",pclResult);
						if (ilRc != RC_SUCCESS)
						{
							/* get exclusive access to thread management area	*/
							mutex_lock(&polThreadMngmnt->mutex);
								
							MtDbgLogFileDbg(TRACE,&log_file,pclResult);
								
							/* release exclusive access to thread management area	*/
							mutex_unlock(&polThreadMngmnt->mutex);
						}
					}					
				}
				else
				{
					/* get exclusive access to thread management area	*/
					mutex_lock(&polThreadMngmnt->mutex);
					
					MtDbgLogFileDbg(DEBUG,&log_file,"Thread [%d,%s]: Not sending item %d because filter criteria do not match\n", thread_id,clClientIpAddr, ilBcIdx);
					
					/* release exclusive access to thread management area	*/
					mutex_unlock(&polThreadMngmnt->mutex);
				
				}
	
				if (blSendEvent)
				{
				
					/* get exclusive read lock	*/
					rw_rdlock(&DataAccess);

					/* resetting status information*/
					QueueSetStatus(&argBcQueue,ilBcIdx,ilStatusPosition,'0');
					
					/* release exclusive lock	*/
					rw_unlock(&DataAccess);
				
				}
#else
				
				if (!blStopThread)
				{
					/* get exclusive access to thread management area	*/
					pthread_mutex_lock(&polThreadMngmnt->mutex);
	
					MtDbgLogFileDbg(TRACE,&log_file,"Thread [%d,%s]: Sending item %d, BcNum %s\n", thread_id,clClientIpAddr, ilBcIdx,QueueGetBcNum(&argBcQueue,ilBcIdx));
	
					/* check on non filtered command(s)	*/
					if (strstr(not_filtered,QueueGetCmd(&argBcQueue,ilBcIdx)) != NULL)
					{
						MtDbgLogFileDbg(DEBUG,&log_file,"Thread [%d,%s]: %s command accepted\n", thread_id,clClientIpAddr,QueueGetCmd(&argBcQueue,ilBcIdx));
						blMatchFilter = TRUE;
					}
					/* check, if event matches the filter criteria	*/
					else if (polThreadMngmnt->BCFilter == NULL || strcmp(polThreadMngmnt->BCFilter,".") == 0)
					{
						MtDbgLogFileDbg(DEBUG,&log_file,"Thread [%d,%s]: No BCFilter defined\n", thread_id,clClientIpAddr);
						blMatchFilter = TRUE;
					}
					else if (strstr(polThreadMngmnt->BCFilter,QueueGetTable(&argBcQueue,ilBcIdx)) != NULL)
					{
						MtDbgLogFileDbg(DEBUG,&log_file,"Thread [%d,%s]: Table %s matches BCFilter %s\n", thread_id,clClientIpAddr,QueueGetTable(&argBcQueue,ilBcIdx),polThreadMngmnt->BCFilter);
						blMatchFilter = TRUE;
					}
					else
					{
						MtDbgLogFileDbg(DEBUG,&log_file,"Thread [%d,%s]: Table %s doesn't match BCFilter %s\n", thread_id,clClientIpAddr,QueueGetTable(&argBcQueue,ilBcIdx),polThreadMngmnt->BCFilter);
					}
	
					if (blStopThread == FALSE && blMatchFilter == TRUE)
					{
						++polThreadMngmnt->ItemsSend;
					}
					
					++polThreadMngmnt->ItemsTotal;
					
					/* release exclusive access to thread management area	*/
					pthread_mutex_unlock(&polThreadMngmnt->mutex);
				}
				
				if (blStopThread == TRUE)
				{
					int rc = 0;
					
					/* get exclusive read lock	*/
					pthread_rwlock_rdlock(&DataAccess);
					
					for (i = 0; i < max_bc_buffers; i++)
					{
						QueueSetStatus(&argBcQueue,i,ilStatusPosition,'0');
					}

					/* release exclusive lock	*/
					pthread_rwlock_unlock(&DataAccess);

					/* release exclusive lock	*/
					pthread_rwlock_unlock(&CLOSend);
					
					rc = SocketShutdown(thread_socket,SHUT_RDWR);
					if (rc != 0)
					{
						MtDbgLogFileDbg(TRACE,&log_file,"Thread [%d,%s]: SocketShutdown socket %d, rc = %d,%s!\n", thread_id,clClientIpAddr,thread_socket,errno,strerror(errno));
					}
					
					rc = close(thread_socket);
					if (rc != 0)
					{
						MtDbgLogFileDbg(TRACE,&log_file,"Thread [%d,%s]: ERROR - closing socket %d, rc = %d,%s!\n", thread_id,clClientIpAddr,thread_socket,errno,strerror(errno));
					}

					/* get exclusive access to thread management area	*/
					pthread_mutex_lock(&polThreadMngmnt->mutex);
							
					MtDbgLogFileDbg(TRACE,&log_file,"Thread [%d,%s]: Stop broadcasting and calling pthread_exit((void *)0)\n", thread_id,clClientIpAddr);
					ThreadMngmntInit(&argThreadMngmnt[ilStatusPosition],FALSE);
							
					/* release exclusive access to thread management area	*/
					pthread_mutex_unlock(&polThreadMngmnt->mutex);

					pthread_exit((void *)0);					
				}
				else if (blMatchFilter == TRUE)
				{
					
					/* sending the data to the client */
					ilBytesToSend = QueueGetUsedLen(&argBcQueue,ilBcIdx);
					pclOutPtr = QueueGetValue(&argBcQueue,ilBcIdx);
					ilBytesSent = 0;
	
					do
					{
						pthread_testcancel();
						/* prior to send, check if client is able to get it	*/
						ilSentThisTime = write(thread_socket, pclOutPtr, ilBytesToSend);
						pthread_testcancel();
						
						if (ilSentThisTime <= 0)
						{
							/* get exclusive access to thread management area	*/
							pthread_mutex_lock(&polThreadMngmnt->mutex);
								
							MtDbgLogFileDbg(TRACE,&log_file,"Thread [%d,%s]: ERROR - writing to socket %d, rc = %d,%s at line %d!\n", thread_id,clClientIpAddr,thread_socket,errno,strerror(errno),1907);
								
							SocketShutdown(thread_socket,SHUT_RDWR);
							MtDbgLogFileDbg(DEBUG,&log_file,"Thread [%d,%s]: ERROR - SocketShutdown socket %d, rc = %d,%s!\n", thread_id,clClientIpAddr,thread_socket,errno,strerror(errno));
								
							close(thread_socket);
							MtDbgLogFileDbg(DEBUG,&log_file,"Thread [%d,%s]: ERROR - closing socket %d, rc = %d,%s!\n", thread_id,clClientIpAddr,thread_socket,errno,strerror(errno));
	
							/* release exclusive access to thread management area	*/
							pthread_mutex_unlock(&polThreadMngmnt->mutex);
	
							/* get exclusive read lock	*/
							pthread_rwlock_rdlock(&DataAccess);
	
							for (i = 0; i < max_bc_buffers; i++)
							{
								QueueSetStatus(&argBcQueue,i,ilStatusPosition,'0');
							}
	
							/* release exclusive lock	*/
							pthread_rwlock_unlock(&DataAccess);
	
							/* release exclusive lock	*/
							pthread_rwlock_unlock(&CLOSend);
	
							/* get exclusive access to thread management area	*/
							pthread_mutex_lock(&polThreadMngmnt->mutex);
									
							MtDbgLogFileDbg(TRACE,&log_file,"Thread [%d,%s]: calling pthread_exit((void *)0) at line %d\n", thread_id,clClientIpAddr,1936);
								
							ThreadMngmntInit(polThreadMngmnt,FALSE);
									
							/* release exclusive access to thread management area	*/
							pthread_mutex_unlock(&polThreadMngmnt->mutex);
	
							pthread_exit((void *)0);
						}
			
						pclOutPtr += ilSentThisTime;
						ilBytesSent += ilSentThisTime;
						ilBytesToSend -= ilSentThisTime;
							
					} 
					while (ilBytesToSend > 0);
						
					if (client_handshake)
					{
						ilRc = ReadKeywordFromSocket(thread_socket,"{=ACK=}",pclResult);
						if (ilRc != RC_SUCCESS)
						{
							/* get exclusive access to thread management area	*/
							pthread_mutex_lock(&polThreadMngmnt->mutex);
								
							MtDbgLogFileDbg(TRACE,&log_file,pclResult);
								
							/* release exclusive access to thread management area	*/
							pthread_mutex_unlock(&polThreadMngmnt->mutex);
						}
					}					
				}
				else
				{
		
					/* get exclusive access to thread management area	*/
					pthread_mutex_lock(&polThreadMngmnt->mutex);
					
					MtDbgLogFileDbg(DEBUG,&log_file,"Thread [%d,%s]: Not sending item %d because filter criteria do not match\n", thread_id,clClientIpAddr, ilBcIdx);
					
					/* release exclusive access to thread management area	*/
					pthread_mutex_unlock(&polThreadMngmnt->mutex);
					
				}
	
				if (blSendEvent)
				{
					/* get exclusive read lock	*/
					pthread_rwlock_rdlock(&DataAccess);

					/* resetting status information*/
					QueueSetStatus(&argBcQueue,ilBcIdx,ilStatusPosition,'0');
					
					/* release exclusive lock	*/
					pthread_rwlock_unlock(&DataAccess);
					
				}
#endif	
			}
			else
			{
#ifdef	_SUN_UNIX
				/* get exclusive access to thread management area	*/
				mutex_lock(&polThreadMngmnt->mutex);
				
				MtDbgLogFileDbg(DEBUG,&log_file,"Thread [%d,%s]: Not sending item %d, status byte was not set.\n", thread_id,clClientIpAddr, ilBcIdx);
				
				/* release exclusive access to thread management area	*/
				mutex_unlock(&polThreadMngmnt->mutex);
#else
				/* get exclusive access to thread management area	*/
				pthread_mutex_lock(&polThreadMngmnt->mutex);
				
				MtDbgLogFileDbg(DEBUG,&log_file,"Thread [%d,%s]: Not sending item %d, status byte was not set.\n", thread_id,clClientIpAddr, ilBcIdx);
				/* release exclusive access to thread management area	*/
				pthread_mutex_unlock(&polThreadMngmnt->mutex);
#endif				
			}

			ilOldBcIdx = ilBcIdx;

#ifdef	_SUN_UNIX
			/* get exclusive read lock	*/
			rw_rdlock(&DataAccess);
			
			/* calculate next read position	*/
			ilBcIdx = QueueGetNextReadPosition(&argBcQueue,ilBcIdx);
			
			/* release exclusive lock	*/
			rw_unlock(&DataAccess);

			/* get exclusive access to thread management area	*/
			mutex_lock(&polThreadMngmnt->mutex);
			
			MtDbgLogFileDbg(DEBUG,&log_file,"Thread [%d,%s]: Previous position in ring buffer(head=%d,tail=%d,wrap=%d): %d\n", thread_id,clClientIpAddr,argBcQueue.Head,argBcQueue.Tail,argBcQueue.Wrap,ilOldBcIdx);
			MtDbgLogFileDbg(DEBUG,&log_file,"Thread [%d,%s]: Next position in ring buffer: %d\n", thread_id,clClientIpAddr, ilBcIdx);
			
			/* release exclusive access to thread management area	*/
			mutex_unlock(&polThreadMngmnt->mutex);
			
#else
			/* get exclusive read lock	*/
			pthread_rwlock_rdlock(&DataAccess);
			
			/* calculate next read position	*/
			ilBcIdx = QueueGetNextReadPosition(&argBcQueue,ilBcIdx);
			
			/* release exclusive lock	*/
			pthread_rwlock_unlock(&DataAccess);

			/* get exclusive access to thread management area	*/
			pthread_mutex_lock(&polThreadMngmnt->mutex);
			
			MtDbgLogFileDbg(DEBUG,&log_file,"Thread [%d,%s]: Previous position in ring buffer(head=%d,tail=%d,wrap=%d): %d\n", thread_id,clClientIpAddr,argBcQueue.Head,argBcQueue.Tail,argBcQueue.Wrap,ilOldBcIdx);
			MtDbgLogFileDbg(DEBUG,&log_file,"Thread [%d,%s]: Next position in ring buffer: %d\n", thread_id,clClientIpAddr, ilBcIdx);
			
			/* release exclusive access to thread management area	*/
			pthread_mutex_unlock(&polThreadMngmnt->mutex);
			
#endif
		}
	
#ifdef	_SUN_UNIX		
		/* release exclusive lock	*/
		rw_unlock(&CLOSend);
#else
		/* release exclusive lock	*/
		pthread_rwlock_unlock(&CLOSend);
		
#endif
	}
}

/*************************************************************/
/*	the connection request thread handler					 */
/*************************************************************/
void *con_request(void *arg)
{

#ifdef	_SUN_UNIX	
	int newsockfd, clilen, rc, ilCnt, i;
	struct sockaddr_in cli_addr, serv_addr;
	struct in_addr* prlInAddr = NULL;
	thread_t chld_thr = NULL;
	int		oldState;
	int		oldType;
	int		blExit = FALSE;
	char	clClientIpAddr[32];
	
	/* get exclusive access to thread management area	*/
	mutex_lock(&listener_thr.mutex);

	/* create the logging file	*/
	log_file.fp = fopen(log_file_name,"w");

	MtDbgLogFileDbg(TRACE,&log_file,"------------------------------------------------\n");
	MtDbgLogFileDbg(TRACE,&log_file,"-  con_request - new process                   -\n");
	MtDbgLogFileDbg(TRACE,&log_file,"------------------------------------------------\n");

#if	1	
	ilCnt = 0;

	MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Creating socket ...\n");
	if((listener_thr.Socket = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	{
		MtDbgLogFileDbg(TRACE,&log_file,"con_request: Can't open stream socket with rc = %d,%s\n",errno,strerror(errno));
		
		/* release exclusive access to listener thread management area	*/
		mutex_unlock(&listener_thr.mutex);
		
		exit(0);
	}

	/* so that we can re-bind to it without TIME_WAIT problems		*/
	if (reuse_address)
	{
		setsockopt(listener_thr.Socket,SOL_SOCKET,SO_REUSEADDR,&reuse_address,sizeof(reuse_address));
	}
	
	/* release exclusive access to listener thread management area	*/
	mutex_unlock(&listener_thr.mutex);

	
	memset((char *) &serv_addr, 0, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	serv_addr.sin_port = htons(tcp_port);


	do
	{
		ilCnt++;
		
		/* get exclusive access to thread management area	*/
		mutex_lock(&listener_thr.mutex);
		
		MtDbgLogFileDbg(TRACE,&log_file,"con_request: Binding socket (attempt %d)\n",ilCnt);

		rc = bind(listener_thr.Socket, (struct sockaddr *) &serv_addr, sizeof(serv_addr));

		if (rc != RC_SUCCESS)
		{
			/* the operating system closes the connection after a while */
			MtDbgLogFileDbg(TRACE,&log_file,"con_request: Binding socket failed with ec = %d. Sleeping 30s.\n",rc);
			/* release exclusive access to listener thread management area	*/
			mutex_unlock(&listener_thr.mutex);
			
			sleep(30);
		}/* end of if */
		else
		{
			/* release exclusive access to listener thread management area	*/
			mutex_unlock(&listener_thr.mutex);
		}
	}
	while ((ilCnt < 20) && (rc != RC_SUCCESS));

	if (rc != RC_SUCCESS)
	{
		/* get exclusive access to thread management area	*/
		mutex_lock(&listener_thr.mutex);
		
		MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR - can't bind local address!\n");
		
		/* release exclusive access to listener thread management area	*/
		mutex_unlock(&listener_thr.mutex);
		
		exit(0);
	}
	
#else
	MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Creating socket ...\n");
	listener_thr.Socket = tcp_create_socket(SOCK_STREAM, &pcgService);
  	if (listener_thr.Socket < 0)
	{
		MtDbgLogFileDbg(TRACE,&log_file,"con_request: Can't open stream socket with rc = %d,%s\n",errno,strerror(errno));
		
		/* release exclusive access to listener thread management area	*/
		mutex_unlock(&listener_thr.mutex);
		
		exit(0);
	}
	
	MtDbgLogFileDbg(TRACE,&log_file,"con_request: Listen to socket\n");
	
#endif	

	/* set the level of thread concurrency we desire */
	rc = thr_setconcurrency(max_threadCount);
	if (rc != 0)
	{
		MtDbgLogFileDbg(TRACE,&log_file,"con_request: set thread concurrency fails with ec = %d,%s\n",errno,strerror(errno));
	}
	
	/* get exclusive access to thread management area	*/
	mutex_lock(&listener_thr.mutex);
	
	listen(listener_thr.Socket, 5);
	
	/* release exclusive access to listener thread management area	*/
	mutex_unlock(&listener_thr.mutex);
	
	while(!blExit)
	{
		/* Waiting for connection request */
		clilen = sizeof(cli_addr);
		
		MtDbgLogFileDbg(TRACE,&log_file,"Waiting for connection request ...\n");

		newsockfd = accept(listener_thr.Socket, (struct sockaddr *) &cli_addr, &clilen);

		/* get exclusive access to listener thread management area	*/
		mutex_lock(&listener_thr.mutex);
		
		blExit = listener_thr.Exit;
		
		/* release exclusive access to listener thread management area	*/
		mutex_unlock(&listener_thr.mutex);
		
		if (blExit)
		{
			MtDbgLogFileDbg(TRACE,&log_file,"Shutting down listener thread ...\n");
			
			/* get exclusive access to listener thread management area	*/
			mutex_lock(&listener_thr.mutex);

			/* SocketShutdown socket connection	*/
			MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Shutdown socket %d\n", newsockfd);
				
			SocketShutdown(newsockfd,SHUT_RDWR);
						
			/* closing open sockets */
			MtDbgLogFileDbg(DEBUG,&log_file,"Terminate: Closing socket %d \n", newsockfd);
			close(newsockfd);
				
			/* SocketShutdown socket connection	*/
			MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Shutdown listener socket %d\n", listener_thr.Socket);
				
			SocketShutdown(listener_thr.Socket,SHUT_RDWR);
						
			/* closing open sockets */
			MtDbgLogFileDbg(DEBUG,&log_file,"Terminate: Closing listener socket %d \n", listener_thr.Socket);
			close(listener_thr.Socket);

			/* release exclusive access to listener thread management area	*/
			mutex_unlock(&listener_thr.mutex);

			thr_exit((void *)0);			
		}
		else if (newsockfd < 0)
		{
			/* get exclusive access to listener thread management area	*/
			mutex_lock(&listener_thr.mutex);
			
			MtDbgLogFileDbg(TRACE,&log_file,"con_request: Accept error, errno = %d,%s\n",errno,strerror(errno)); 
			
			/* release exclusive access to listener thread management area	*/
			mutex_unlock(&listener_thr.mutex);
			continue;
		}
		else
		{
			/* read the command from socket	*/
			char	*data = NULL;
			char	cmd[100];
			char	total[18];
			long	size;
			char*	pclDataBegin;
			
			/* set linger socket option	*/
			if (use_linger)
			{
				int				blDontLinger;
				struct linger 	olLinger;
				int		ilLen;
				
#if	0				
				ilLen = sizeof(int);
				rc = getsockopt(newsockfd,SOL_SOCKET,SO_DONTLINGER,&blDontLinger,&ilLen);
				if (rc < 0)
				{
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR - getsockopt failed for socket %d, ec = %d,%s!\n",newsockfd,errno,strerror(errno));
				}
				else
				{
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: getsockopt dont linger set to (%d)!\n",blDontLinger);
				}				
#endif
				
				olLinger.l_onoff  = -1;
				olLinger.l_linger = -1;
				ilLen = sizeof(olLinger);
				
				rc = getsockopt(newsockfd,SOL_SOCKET,SO_LINGER,(char *)&olLinger,&ilLen);
				if (rc < 0)
				{
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR - getsockopt failed for socket %d, ec = %d,%s!\n",newsockfd,errno,strerror(errno));
				}
				else
				{
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: getsockopt linger set to (%d,%d)!\n",olLinger.l_onoff,olLinger.l_linger);
				}				

				olLinger.l_onoff  = 1;
				olLinger.l_linger = 0;
				ilLen = sizeof(olLinger);
				
				rc = setsockopt(newsockfd,SOL_SOCKET,SO_LINGER,(char *)&olLinger,ilLen);
				if (rc < 0)
				{
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR - setsockopt failed for socket %d, ec = %d,%s!\n",newsockfd,errno,strerror(errno));
				}
				else
				{
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: setsockopt linger set to (%d,%d)!\n",olLinger.l_onoff,olLinger.l_linger);
				}				
			}
								
			/* read {=TOT=} total size of command from socket	*/
			rc = ReadFromSocket(newsockfd,cmd,16); 
			if (rc <= 0)
			{
				/* get exclusive access to listener thread management area	*/
				mutex_lock(&listener_thr.mutex);
				
				MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR - error reading socket %d, ec = %d,%s!\n",newsockfd,errno,strerror(errno));
				
				/* SocketShutdown socket connection	*/
				MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Shutdown socket %d\n", newsockfd);
				
				SocketShutdown(newsockfd,SHUT_RDWR);
						
				/* closing open sockets */
				MtDbgLogFileDbg(DEBUG,&log_file,"Terminate: Closing socket %d \n", newsockfd);
				close(newsockfd);

				/* release exclusive access to listener thread management area	*/
				mutex_unlock(&listener_thr.mutex);
												
				continue;								
			}
			else if (rc != 16)
			{
				/* get exclusive access to listener thread management area	*/
				mutex_lock(&listener_thr.mutex);
				
				MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR - error reading socket %d, invalid format!\n",newsockfd);
				
				/* SocketShutdown socket connection	*/
				MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Shutdown socket %d\n", newsockfd);
				
				SocketShutdown(newsockfd,SHUT_RDWR);
						
				/* closing open sockets */
				MtDbgLogFileDbg(DEBUG,&log_file,"Terminate: Closing socket %d \n", newsockfd);
				close(newsockfd);

				/* release exclusive access to listener thread management area	*/
				mutex_unlock(&listener_thr.mutex);
												
				continue;								
			}
			
			pclDataBegin = GetKeyItem(total,&size,cmd,"{=TOT=}", "{=",TRUE);
			if (pclDataBegin == NULL)
			{
				/* get exclusive access to listener thread management area	*/
				mutex_lock(&listener_thr.mutex);
				
				MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR - can't locate {=TOT=} keyword!\n");
				
				/* SocketShutdown socket connection	*/
				MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Shutdown socket %d\n", newsockfd);
				SocketShutdown(newsockfd,SHUT_RDWR);
						
				/* closing open sockets */
				MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Closing socket %d \n", newsockfd);
				close(newsockfd);

				/* release exclusive access to listener thread management area	*/
				mutex_unlock(&listener_thr.mutex);
												
				continue;								
			}
				
			/* get exclusive access to listener thread management area	*/
			mutex_lock(&listener_thr.mutex);
			
			MtDbgLogFileDbg(DEBUG,&log_file,"con_request: bytes read from socket %d,total size from socket = %s\n", rc,total);
					
			size = atol(total);
			data = (char *)malloc((size + 2) * sizeof(char));							
		
			MtDbgLogFileDbg(DEBUG,&log_file,"con_request: data allocated at %ld with size = %ld \n", data,(size + 2) * sizeof(char));

			/* release exclusive access to listener thread management area	*/
			mutex_unlock(&listener_thr.mutex);
		
			memmove(data,cmd,16);
								
			if (size > 16)
			{
				rc = ReadFromSocket(newsockfd,&data[16],size - 16);
				if (rc <= 0)
				{
					/* get exclusive access to listener thread management area	*/
					mutex_lock(&listener_thr.mutex);
					
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR - error reading socket %d, ec = %d,%s!\n",newsockfd,errno,strerror(errno));
					
					/* SocketShutdown socket connection	*/
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Shutdown socket %d\n", newsockfd);
					SocketShutdown(newsockfd,SHUT_RDWR);
							
					/* closing open sockets */
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Closing socket %d \n", newsockfd);
					close(newsockfd);

					/* release exclusive access to listener thread management area	*/
					mutex_unlock(&listener_thr.mutex);
					
					free(data);
					
					continue;					
				}
				else if (rc != (size - 16))
				{
					/* get exclusive access to listener thread management area	*/
					mutex_lock(&listener_thr.mutex);
					
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR - error reading socket %d, invalid format!\n",newsockfd);
					
					/* SocketShutdown socket connection	*/
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Shutdown socket %d\n", newsockfd);
					SocketShutdown(newsockfd,SHUT_RDWR);
							
					/* closing open sockets */
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Closing socket %d \n", newsockfd);
					close(newsockfd);

					/* release exclusive access to listener thread management area	*/
					mutex_unlock(&listener_thr.mutex);
					
					free(data);
					
					continue;					
										
				}
			}
					
			data[size] = '\0';
			
			/* get exclusive access to listener thread management area	*/
			mutex_lock(&listener_thr.mutex);
			
			MtDbgLogFileDbg(DEBUG,&log_file,"con_request: data from socket = %s\n", data);

			/* release exclusive access to listener thread management area	*/
			mutex_unlock(&listener_thr.mutex);
						
			pclDataBegin = GetKeyItem(cmd,&size,data,"{=CMD=}", "{=",TRUE);
			if (pclDataBegin == NULL)
			{
				/* get exclusive access to listener thread management area	*/
				mutex_lock(&listener_thr.mutex);
				
				MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR - can't locate {=CMD=} keyword!\n");
				
				/* SocketShutdown socket connection	*/
				MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Shutdown socket %d\n", newsockfd);
				SocketShutdown(newsockfd,SHUT_RDWR);
							
				/* closing open sockets */
				MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Closing socket %d \n", newsockfd);
				close(newsockfd);

				/* release exclusive access to listener thread management area	*/
				mutex_unlock(&listener_thr.mutex);
					
				free(data);
					
				continue;					
			}
			
			/* get exclusive access to listener thread management area	*/
			mutex_lock(&listener_thr.mutex);
			
			MtDbgLogFileDbg(TRACE,&log_file,"con_request: executing command = %s from socket = %d\n", cmd,newsockfd);

			/* release exclusive access to listener thread management area	*/
			mutex_unlock(&listener_thr.mutex);
			
			/* check command types	*/						
			if (strcmp(cmd,"BCOUT") == 0)
			{									
				/* search for a position in the thread management */
				for (i = 0; i < max_bc_clients; i++)
				{
					/* get exclusive access to management area	*/
					mutex_lock(&argThreadMngmnt[i].mutex);
					if (argThreadMngmnt[i].ThreadID == NULL)
					{
						if (argThreadMngmnt[i].Socket == -1)
						{
							/* setting the thread management information */
							argThreadMngmnt[i].Socket = newsockfd;
							argThreadMngmnt[i].StatusPosition = i;
							prlInAddr = (struct in_addr *) &cli_addr.sin_addr.s_addr;
						    sprintf(argThreadMngmnt[i].ClientIpAddr,"%s",inet_ntoa(*prlInAddr));
						    sprintf(argThreadMngmnt[i].ClientHexAddr,"%08x",ntohl(cli_addr.sin_addr.s_addr));
						    if (argThreadMngmnt[i].BCFilter != NULL)
						    {
								free(argThreadMngmnt[i].BCFilter);						    	
						    }
						    argThreadMngmnt[i].BCFilter = NULL;

							strcpy(clClientIpAddr,argThreadMngmnt[i].ClientIpAddr);
													    
							/* release exclusive access to management area	*/
							mutex_unlock(&argThreadMngmnt[i].mutex);
							break;
						}
					}
					/* release exclusive access to management area	*/
					mutex_unlock(&argThreadMngmnt[i].mutex);
				}
				
				/* check on overflow	*/
				if (i == max_bc_clients)
				{
					/* get exclusive access to listener thread management area	*/
					mutex_lock(&listener_thr.mutex);
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: max_bc_clients connected!\n");
					
					/* SocketShutdown socket connection	*/
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Shutdown socket %d\n", newsockfd);
					SocketShutdown(newsockfd,SHUT_RDWR);
								
					/* closing open sockets */
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Closing socket %d\n", newsockfd);
					close(newsockfd);

					/* release exclusive access to listener thread management area	*/
					mutex_unlock(&listener_thr.mutex);
						
					free(data);
						
					continue;					
				}

				/* create a new thread to process the incomming request */
				chld_thr = NULL;
				rc = thr_create(NULL, 0,client_bc_connector, (void *) &argThreadMngmnt[i],THR_DETACHED,&chld_thr);
				if (rc != 0)
				{
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: rc from pthread_create = %d\n", rc);
				}
			
				if (rc == 0 && chld_thr != NULL)
				{
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: Thread ID [%d,%s] for queue %d is valid!\n",chld_thr,clClientIpAddr,i);
				}
				else
				{
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: Thread ID for queue %d is NULL, error = %d,%s!\n",i,errno,strerror(errno));
					
					/* get exclusive access to management area	*/
					mutex_lock(&argThreadMngmnt[i].mutex);
					
					argThreadMngmnt[i].Socket = -1;
					argThreadMngmnt[i].StatusPosition = -1;
					
					/* release exclusive access to management area	*/
					mutex_unlock(&argThreadMngmnt[i].mutex);

					/* get exclusive access to listener thread management area	*/
					mutex_lock(&listener_thr.mutex);
					
					/* SocketShutdown socket connection	*/
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Shutdown socket %d\n", newsockfd);
					SocketShutdown(newsockfd,SHUT_RDWR);
								
					/* closing open sockets */
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Closing socket %d \n", newsockfd);
					close(newsockfd);
					
					/* release exclusive access to listener thread management area	*/
					mutex_unlock(&listener_thr.mutex);
						
					free(data);
						
					continue;					
					
				}

				/* send queue id aka thread management index to client	*/
				if (send_threadId)
				{
					sprintf(data,"{=TOT=}%-9d{=QUE=}%-4d,%-9d",37,i,chld_thr);
					rc = write(newsockfd,data,37);								
				}
				else
				{
					sprintf(data,"{=TOT=}%-9d{=QUE=}%-4d",27,i);
					rc = write(newsockfd,data,27);								
				}
				
				if (rc <= 0)
				{
					/* get exclusive access to listener thread management area	*/
					mutex_lock(&listener_thr.mutex);
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR - error writing socket %d, ec = %d,%s!\n",newsockfd,errno,strerror(errno));
					
					/* SocketShutdown socket connection	*/
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Shutdown socket %d\n", newsockfd);
					SocketShutdown(newsockfd,SHUT_RDWR);
								
					/* closing open sockets */
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Closing socket %d \n", newsockfd);
					close(newsockfd);

					/* release exclusive access to listener thread management area	*/
					mutex_unlock(&listener_thr.mutex);
						
					free(data);
						
					continue;					
				}


				/* the server is now free to accept another socket request */
				MtDbgLogFileDbg(DEBUG,&log_file,"Waiting for next client.\n");
				
			}
			else if (strcmp(cmd,"BCKEY") == 0)
			{
				char queue[100];
				int index 	 = -1;
				int threadId =  0;
				
				/* get exclusive access to listener thread management area	*/
				mutex_lock(&listener_thr.mutex);
				
				MtDbgLogFileDbg(TRACE,&log_file,"con_request: BCKEY command received !\n");

				/* release exclusive access to listener thread management area	*/
				mutex_unlock(&listener_thr.mutex);
				
				/* get queue number from command	*/
				pclDataBegin = GetKeyItem(queue,&size,data,"{=QUE=}", "{=",TRUE);
				if (pclDataBegin == NULL)
				{
					/* get exclusive access to listener thread management area	*/
					mutex_lock(&listener_thr.mutex);
					
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR -{=QUE=} keyword not found within BCKEY command!\n");

					/* SocketShutdown socket connection	*/
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Shutdown socket %d\n", newsockfd);
					SocketShutdown(newsockfd,SHUT_RDWR);
								
					/* closing open sockets */
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Closing socket %d \n", newsockfd);
					close(newsockfd);
					
					/* release exclusive access to listener thread management area	*/
					mutex_unlock(&listener_thr.mutex);

					free(data);					
					continue;
				}

				if (send_threadId)
				{
					index = atoi(queue);
					threadId = atoi(&queue[5]);
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: INFO queue = [%s] index = %d,threadId = %d\n",queue,index,threadId);
				}
				else
				{
					index = atoi(queue);
				}
				
				/* check queue id validity	*/
				if (index < 0 || index >= max_bc_clients)
				{
					/* get exclusive access to listener thread management area	*/
					mutex_lock(&listener_thr.mutex);
					
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR queue index %d invalid!\n",index);
					
					/* SocketShutdown socket connection	*/
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Shutdown socket %d\n", newsockfd);
					SocketShutdown(newsockfd,SHUT_RDWR);
								
					/* closing open sockets */
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Closing socket %d \n", newsockfd);
					close(newsockfd);
					
					/* release exclusive access to listener thread management area	*/
					mutex_unlock(&listener_thr.mutex);

					free(data);					
					
					continue;
				}

				/* extract broadcast filter data from command	*/												
				pclDataBegin = GetKeyItem("",&size,data,"{=DAT=}", "{=",FALSE);
				if (pclDataBegin == NULL)	/* no data means no filter in this case	*/
				{
					size = 0;
					
					/* get exclusive access to listener thread management area	*/
					mutex_lock(&listener_thr.mutex);
					
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: INFO -{=DAT=} keyword not found within BCKEY command -> assuming NO BC filter!\n");
					
					/* release exclusive access to listener thread management area	*/
					mutex_unlock(&listener_thr.mutex);
				}

				/* get exclusive access to management area	*/
				mutex_lock(&argThreadMngmnt[index].mutex);

				strcpy(clClientIpAddr,argThreadMngmnt[index].ClientIpAddr);
				
				/* check, if corresponding thread is active	*/			
				if (send_threadId && (argThreadMngmnt[index].ThreadID != threadId))
				{
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR Thread id for queue index %d,%s invalid!\n",index,clClientIpAddr);
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: ERROR Thread id for queue index %d = %d doesn't match %d!\n",index,argThreadMngmnt[index].ThreadID,threadId);
				}
				else if (argThreadMngmnt[index].ThreadID == NULL)
				{
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR Thread id for queue index %d,%s invalid!\n",index,clClientIpAddr);
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: ERROR Thread id for queue index %d = %d doesn't match %d!\n",index,argThreadMngmnt[index].ThreadID,threadId);
				}
				else
				{				
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: allocate memory!\n");
					
					/* release current filter	*/
					if (argThreadMngmnt[index].BCFilter != NULL)
					{
						free(argThreadMngmnt[index].BCFilter);						    	
					}
					
					argThreadMngmnt[index].BCFilter = NULL;
					
					/* allocate broadcast filter, if necessary	*/
					if (size > 0)
					{
						argThreadMngmnt[index].BCFilter = (char *)malloc((size + 2) * sizeof(char));
						strncpy(argThreadMngmnt[index].BCFilter,pclDataBegin,size);
						argThreadMngmnt[index].BCFilter[size] = '\0';
						
						MtDbgLogFileDbg(DEBUG,&log_file,"con_request: BC Filter for queue index %d,%s = %s!\n",index,clClientIpAddr,argThreadMngmnt[index].BCFilter);
					}
					else
					{
						MtDbgLogFileDbg(DEBUG,&log_file,"con_request: BC Filter for queue index %d,%s = %s!\n",index,clClientIpAddr,".");
					}
				}
								
				/*	release exclusive access to management area	*/
				mutex_unlock(&argThreadMngmnt[index].mutex);

				/* get exclusive access to listener thread management area	*/
				mutex_lock(&listener_thr.mutex);

				/* SocketShutdown socket connection	*/
				MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Shutdown socket %d\n", newsockfd);
				SocketShutdown(newsockfd,SHUT_RDWR);
						
				/* closing open sockets */
				MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Closing socket %d\n", newsockfd);
				close(newsockfd);
				
				/* the server is now free to accept another socket request */
				MtDbgLogFileDbg(DEBUG,&log_file,"con_request : Waiting for next client.\n");
				
				/* release exclusive access to listener thread management area	*/
				mutex_unlock(&listener_thr.mutex);
				
			}
			else if (strcmp(cmd,"BCOFF") == 0)
			{
				char queue[100];
				int index 	 = -1;
				int threadId =  0;
				
				/* get exclusive access to listener thread management area	*/
				mutex_lock(&listener_thr.mutex);
				
				MtDbgLogFileDbg(TRACE,&log_file,"con_request: BCOFF command received at socket = %d!\n",newsockfd);

				/* release exclusive access to listener thread management area	*/
				mutex_unlock(&listener_thr.mutex);
				
				/* get queue number from command	*/
				pclDataBegin = GetKeyItem(queue,&size,data,"{=QUE=}", "{=",TRUE);
				if (pclDataBegin == NULL)
				{
					/* get exclusive access to listener thread management area	*/
					mutex_lock(&listener_thr.mutex);
					
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR -{=QUE=} keyword not found within BCOFF command!\n");

					/* SocketShutdown socket connection	*/
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Shutdown socket %d\n", newsockfd);
					SocketShutdown(newsockfd,SHUT_RDWR);
								
					/* closing open sockets */
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Closing socket %d \n", newsockfd);
					close(newsockfd);
					
					/* release exclusive access to listener thread management area	*/
					mutex_unlock(&listener_thr.mutex);

					free(data);					
	
					continue;
				}
				
				if (send_threadId)
				{
					index = atoi(queue);
					threadId = atoi(&queue[5]);
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: INFO queue = [%s] index = %d,threadId = %d\n",queue,index,threadId);
				}
				else
				{
					index = atoi(queue);
				}
				
				/* check queue id validity	*/
				if (index < 0 || index >= max_bc_clients)
				{
					/* get exclusive access to listener thread management area	*/
					mutex_lock(&listener_thr.mutex);
					
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR queue index %d invalid!\n",index);
					
					/* SocketShutdown socket connection	*/
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Shutdown socket %d\n", newsockfd);
					SocketShutdown(newsockfd,SHUT_RDWR);
								
					/* closing open sockets */
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Closing socket %d \n", newsockfd);
					close(newsockfd);
					
					/* release exclusive access to listener thread management area	*/
					mutex_unlock(&listener_thr.mutex);

					free(data);					
					
					continue;
				}

				/* get exclusive access to management area	*/
				mutex_lock(&argThreadMngmnt[index].mutex);

				strcpy(clClientIpAddr,argThreadMngmnt[index].ClientIpAddr);

				/* check, if corresponding thread is active	*/			
				if (send_threadId && (argThreadMngmnt[index].ThreadID != threadId))
				{
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR Thread id for queue index %d,%s invalid!\n",index,clClientIpAddr);
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: ERROR Thread id for queue index %d = %d doesn't match %d!\n",index,argThreadMngmnt[index].ThreadID,threadId);
				}
				else if (argThreadMngmnt[index].ThreadID == NULL)
				{
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR Thread id for queue index %d,%s invalid!\n",index,clClientIpAddr);
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: ERROR Thread id for queue index %d = %d doesn't match %d!\n",index,argThreadMngmnt[index].ThreadID,threadId);
				}
				else
				{				
					/*	stop broadcasting	*/
					thread_t threadID;					
					void*	  exitArg;
					threadID = argThreadMngmnt[index].ThreadID;

#if	1
					/* get exclusive access to listener thread management area	*/
					mutex_lock(&listener_thr.mutex);

					/* set thread status to exit	*/
					argThreadMngmnt[index].Exit = TRUE;

#if	0				/* due to accept error = 9		*/
	
					/* close the sockets at first	*/					
					if (argThreadMngmnt[index].Socket != -1)
					{
						/* SocketShutdown socket connection	*/
						MtDbgLogFileDbg(TRACE,&log_file,"con_request: Shutdown socket %d (connection with client %s)\n", argThreadMngmnt[index].Socket, argThreadMngmnt[index].ClientIpAddr);
						SocketShutdown(argThreadMngmnt[index].Socket,SHUT_RDWR);
						
						/* closing open sockets */
						MtDbgLogFileDbg(TRACE,&log_file,"con_request: Closing socket %d (connection with client %s)\n", argThreadMngmnt[index].Socket, argThreadMngmnt[index].ClientIpAddr);
						close(argThreadMngmnt[index].Socket);
					}
#endif					
					/* now wakeup the client connection threads	*/
					cond_broadcast(&WaitNext);																				

					/* release exclusive access to listener thread management area	*/
					mutex_unlock(&listener_thr.mutex);

#else					
					/* get exclusive access to listener thread management area	*/
					mutex_lock(&listener_thr.mutex);
					
					rc = thr_kill(argThreadMngmnt[index].ThreadID,SIGCANCEL);
					MtDbgLogFileDbg(TRACE,&listener_thr,"con_request : Thread %d,%s for queue index %d cancelled with rc = %d\n",threadID,clClientIpAddr,index,rc);

					if (argThreadMngmnt[index].Socket != -1)
					{
						/* SocketShutdown socket connection	*/
						MtDbgLogFileDbg(TRACE,&log_file,"con_request: Shutdown socket %d (connection with client %s)\n", argThreadMngmnt[index].Socket, argThreadMngmnt[index].ClientIpAddr);
						SocketShutdown(argThreadMngmnt[index].Socket,SHUT_RDWR);
						
						/* closing open sockets */
						MtDbgLogFileDbg(TRACE,&log_file,"con_request: Closing socket %d (connection with client %s)\n", argThreadMngmnt[index].Socket, argThreadMngmnt[index].ClientIpAddr);
						close(argThreadMngmnt[index].Socket);
					}

					/* release exclusive access to listener thread management area	*/
					mutex_unlock(&listener_thr.mutex);
					
					ThreadMngmntInit(&argThreadMngmnt[index],FALSE);
#endif
				}
								
				/*	release exclusive access to management area	*/
				mutex_unlock(&argThreadMngmnt[index].mutex);
				
				/* get exclusive access to listener thread management area	*/
				mutex_lock(&listener_thr.mutex);

				/* SocketShutdown socket connection	*/
				MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Shutdown socket %d\n", newsockfd);
				SocketShutdown(newsockfd,SHUT_RDWR);
						
				/* closing open sockets */
				MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Closing socket %d\n", newsockfd);
				close(newsockfd);
				
				/* the server is now free to accept another socket request */
				MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Waiting for next client.\n");

				/* release exclusive access to listener thread management area	*/
				mutex_unlock(&listener_thr.mutex);
			
			}
			else if (strcmp(cmd,"BCINF") == 0)	/* send connection information to client monitor...	*/
			{
				int 	index = -1;
				int 	count   = 0;
				char*	pclData = (char *)malloc((max_bc_clients + 1) * 256);	
				char	pclLine[256];

				sprintf(pclData,"{=TOT=}%-9d{=COUNT=}%-4d{\\=COUNT=}{=DATA=}",16,0);
									
				/* get connection information from thread management	*/
				for (index = 0; index < max_bc_clients; index++)
				{
					/* get exclusive access to management area	*/
					mutex_lock(&argThreadMngmnt[index].mutex);
					
					if (argThreadMngmnt[index].ThreadID != NULL)
					{
						++count;
						sprintf(pclLine,"%d,%d,%d,%s,%s,%d,%d\n",index,argThreadMngmnt[index].ThreadID,argThreadMngmnt[index].Socket,argThreadMngmnt[index].ClientIpAddr,argThreadMngmnt[index].ClientHexAddr,argThreadMngmnt[index].ItemsSend,argThreadMngmnt[index].ItemsTotal);
						strcat(pclData,pclLine);
					}
												
					/*	release exclusive access to management area	*/
					mutex_unlock(&argThreadMngmnt[index].mutex);
				}				
				
				strcat(pclData,"{\\=DATA=}");
				
				/* send thread management information to monitor client */
				sprintf(pclLine,"{=TOT=}%-9d{=COUNT=}%-4d",strlen(pclData),count);
				/* overwrite this part of the data	*/
				memmove(pclData,pclLine,strlen(pclLine));

				/* get exclusive access to listener thread management area	*/
				mutex_lock(&listener_thr.mutex);
				
				MtDbgLogFileDbg(DEBUG,&log_file,"con_request: BCINF - sending connection info = %s!\n",pclData);

				/* release exclusive access to listener thread management area	*/
				mutex_unlock(&listener_thr.mutex);
				
				rc = write(newsockfd,pclData,strlen(pclData));								
				if (rc <= 0)
				{
					/* get exclusive access to listener thread management area	*/
					mutex_lock(&listener_thr.mutex);
					
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR - error writing socket %d, ec = %d,%s!\n",newsockfd,errno,strerror(errno));
					
					/* release exclusive access to listener thread management area	*/
					mutex_unlock(&listener_thr.mutex);
				
				}
				else
				{					
					rc = ReadKeywordFromSocket(newsockfd,"{=ACK=}",pclLine);
					
					/* get exclusive access to listener thread management area	*/
					mutex_lock(&listener_thr.mutex);
				
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Read ACK from socket %d, ec = %d,%s\n", newsockfd,errno,strerror(errno));

					/* release exclusive access to listener thread management area	*/
					mutex_unlock(&listener_thr.mutex);
				
				}

				/* get exclusive access to listener thread management area	*/
				mutex_lock(&listener_thr.mutex);

				/* SocketShutdown socket connection	*/
				MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Shutdown socket %d\n", newsockfd);
				SocketShutdown(newsockfd,SHUT_RDWR);
						
				/* closing open sockets */
				MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Closing socket %d\n", newsockfd);
				close(newsockfd);

				/* release exclusive access to listener thread management area	*/
				mutex_unlock(&listener_thr.mutex);
						
				free(pclData);
	
			}
			else
			{
				/* get exclusive access to listener thread management area	*/
				mutex_lock(&listener_thr.mutex);
				
				MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR - unsupported command %s ignored!\n",cmd);
				
				/* release exclusive access to listener thread management area	*/
				mutex_unlock(&listener_thr.mutex);
			}
			
			if (data != NULL)
			{
				free(data);
				data = NULL;
			}
		}
	}
#else
	int newsockfd, clilen, rc, ilCnt, i;
	struct sockaddr_in cli_addr, serv_addr;
	struct in_addr* prlInAddr = NULL;
	pthread_t chld_thr = NULL;
	int		oldState;
	int		oldType;
	int		blExit = FALSE;
	char	clClientIpAddr[32];
	
	/* get exclusive access to thread management area	*/
	pthread_mutex_lock(&listener_thr.mutex);

	/* create the logging file	*/
	log_file.fp = fopen(log_file_name,"w");

	MtDbgLogFileDbg(TRACE,&log_file,"------------------------------------------------\n");
	MtDbgLogFileDbg(TRACE,&log_file,"-  con_request - new process                   -\n");
	MtDbgLogFileDbg(TRACE,&log_file,"------------------------------------------------\n");

	rc = pthread_setcancelstate(PTHREAD_CANCEL_ENABLE,&oldState);
	MtDbgLogFileDbg(DEBUG,&log_file,"con_request: set cancel state with rc = %d\n",rc);
	
	rc = pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS,&oldType);  
	MtDbgLogFileDbg(DEBUG,&log_file,"con_request: set cancel type with rc = %d\n",rc);
	
#if	1	
	ilCnt = 0;

	MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Creating socket ...\n");
	if((listener_thr.Socket = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	{
		MtDbgLogFileDbg(TRACE,&log_file,"con_request: Can't open stream socket with rc = %d,%s\n",errno,strerror(errno));
		
		/* release exclusive access to listener thread management area	*/
		pthread_mutex_unlock(&listener_thr.mutex);
		
		exit(0);
	}

	/* so that we can re-bind to it without TIME_WAIT problems		*/
	if (reuse_address)
	{
		setsockopt(listener_thr.Socket,SOL_SOCKET,SO_REUSEADDR,&reuse_address,sizeof(reuse_address));
	}
	
	/* release exclusive access to listener thread management area	*/
	pthread_mutex_unlock(&listener_thr.mutex);

	
	memset((char *) &serv_addr, 0, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	serv_addr.sin_port = htons(tcp_port);


	do
	{
		ilCnt++;
		
		/* get exclusive access to thread management area	*/
		pthread_mutex_lock(&listener_thr.mutex);
		
		MtDbgLogFileDbg(TRACE,&log_file,"con_request: Binding socket (attempt %d)\n",ilCnt);

		rc = bind(listener_thr.Socket, (struct sockaddr *) &serv_addr, sizeof(serv_addr));

		if (rc != RC_SUCCESS)
		{
			/* the operating system closes the connection after a while */
			MtDbgLogFileDbg(TRACE,&log_file,"con_request: Binding socket failed with ec = %d. Sleeping 30s.\n",rc);
			/* release exclusive access to listener thread management area	*/
			pthread_mutex_unlock(&listener_thr.mutex);
			
			sleep(30);
		}/* end of if */
		else
		{
			/* release exclusive access to listener thread management area	*/
			pthread_mutex_unlock(&listener_thr.mutex);
		}
	}
	while ((ilCnt < 20) && (rc != RC_SUCCESS));

	if (rc != RC_SUCCESS)
	{
		/* get exclusive access to thread management area	*/
		pthread_mutex_lock(&listener_thr.mutex);
		
		MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR - can't bind local address!\n");
		
		/* release exclusive access to listener thread management area	*/
		pthread_mutex_unlock(&listener_thr.mutex);
		
		exit(0);
	}
	
#else
	MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Creating socket ...\n");
	listener_thr.Socket = tcp_create_socket(SOCK_STREAM, &pcgService);
  	if (listener_thr.Socket < 0)
	{
		MtDbgLogFileDbg(TRACE,&log_file,"con_request: Can't open stream socket with rc = %d,%s\n",errno,strerror(errno));
		
		/* release exclusive access to listener thread management area	*/
		pthread_mutex_unlock(&listener_thr.mutex);
		
		exit(0);
	}
	
	MtDbgLogFileDbg(TRACE,&log_file"con_request: Listen to socket\n");
	
#endif	

	/* set the level of thread concurrency we desire */
	rc = pthread_setconcurrency(max_threadCount);
	if (rc != 0)
	{
		MtDbgLogFileDbg(TRACE,&log_file,"con_request: set thread concurrency fails with ec = %d,%s\n",errno,strerror(errno));
	}
	
	/* get exclusive access to thread management area	*/
	pthread_mutex_lock(&listener_thr.mutex);
	
	listen(listener_thr.Socket, 5);
	
	/* release exclusive access to listener thread management area	*/
	pthread_mutex_unlock(&listener_thr.mutex);
	
	while(!blExit)
	{
		/* Waiting for connection request */
		clilen = sizeof(cli_addr);
		
		MtDbgLogFileDbg(TRACE,&log_file,"Waiting for connection request ...\n");

		newsockfd = accept(listener_thr.Socket, (struct sockaddr *) &cli_addr, &clilen);

		/* get exclusive access to listener thread management area	*/
		pthread_mutex_lock(&listener_thr.mutex);
		
		blExit = listener_thr.Exit;
		
		/* release exclusive access to listener thread management area	*/
		pthread_mutex_unlock(&listener_thr.mutex);

		if (blExit)
		{
			/* get exclusive access to listener thread management area	*/
			pthread_mutex_lock(&listener_thr.mutex);
			
			MtDbgLogFileDbg(TRACE,&log_file,"con_request: Shutting down listener thread!\n"); 

			/* SocketShutdown socket connection	*/
			MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Shutdown socket %d\n", newsockfd);
				
			SocketShutdown(newsockfd,SHUT_RDWR);
						
			/* closing open sockets */
			MtDbgLogFileDbg(DEBUG,&log_file,"Terminate: Closing socket %d \n", newsockfd);
			close(newsockfd);

			/* SocketShutdown socket connection	*/
			MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Shutdown listener socket %d\n",listener_thr.Socket);
				
			SocketShutdown(listener_thr.Socket,SHUT_RDWR);
						
			/* closing open sockets */
			MtDbgLogFileDbg(DEBUG,&log_file,"Terminate: Closing socket %d \n", listener_thr.Socket);
			close(listener_thr.Socket);

			/* release exclusive access to listener thread management area	*/
			pthread_mutex_unlock(&listener_thr.mutex);

			pthread_exit((void *)0);			
		}
		else if (newsockfd < 0)
		{
			/* get exclusive access to listener thread management area	*/
			pthread_mutex_lock(&listener_thr.mutex);
			
			MtDbgLogFileDbg(TRACE,&log_file,"con_request: Accept error, errno = %d,%s\n",errno,strerror(errno)); 
			
			/* release exclusive access to listener thread management area	*/
			pthread_mutex_unlock(&listener_thr.mutex);
			continue;
		}
		else
		{
			/* read the command from socket	*/
			char	*data = NULL;
			char	cmd[100];
			char	total[18];
			long	size;
			char*	pclDataBegin;
			
			/* read {=TOT=} total size of command from socket	*/
			rc = ReadFromSocket(newsockfd,cmd,16); 
			if (rc <= 0)
			{
				/* get exclusive access to listener thread management area	*/
				pthread_mutex_lock(&listener_thr.mutex);
				
				MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR - error reading socket %d, ec = %d,%s!\n",newsockfd,errno,strerror(errno));
				
				/* SocketShutdown socket connection	*/
				MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Shutdown socket %d\n", newsockfd);
				
				SocketShutdown(newsockfd,SHUT_RDWR);
						
				/* closing open sockets */
				MtDbgLogFileDbg(DEBUG,&log_file,"Terminate: Closing socket %d \n", newsockfd);
				close(newsockfd);

				/* release exclusive access to listener thread management area	*/
				pthread_mutex_unlock(&listener_thr.mutex);
												
				continue;								
			}
			else if (rc != 16)
			{
				/* get exclusive access to listener thread management area	*/
				pthread_mutex_lock(&listener_thr.mutex);
				
				MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR - error reading socket %d, invalid format!\n",newsockfd);
				
				/* SocketShutdown socket connection	*/
				MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Shutdown socket %d\n", newsockfd);
				
				SocketShutdown(newsockfd,SHUT_RDWR);
						
				/* closing open sockets */
				MtDbgLogFileDbg(DEBUG,&log_file,"Terminate: Closing socket %d \n", newsockfd);
				close(newsockfd);

				/* release exclusive access to listener thread management area	*/
				pthread_mutex_unlock(&listener_thr.mutex);
												
				continue;								
			}
			
			pclDataBegin = GetKeyItem(total,&size,cmd,"{=TOT=}", "{=",TRUE);
			if (pclDataBegin == NULL)
			{
				/* get exclusive access to listener thread management area	*/
				pthread_mutex_lock(&listener_thr.mutex);
				
				MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR - can't locate {=TOT=} keyword!\n");
				
				/* SocketShutdown socket connection	*/
				MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Shutdown socket %d\n", newsockfd);
				SocketShutdown(newsockfd,SHUT_RDWR);
						
				/* closing open sockets */
				MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Closing socket %d \n", newsockfd);
				close(newsockfd);

				/* release exclusive access to listener thread management area	*/
				pthread_mutex_unlock(&listener_thr.mutex);
												
				continue;								
			}
				
			/* get exclusive access to listener thread management area	*/
			pthread_mutex_lock(&listener_thr.mutex);
			
			MtDbgLogFileDbg(DEBUG,&log_file,"con_request: bytes read from socket %d,total size from socket = %s\n", rc,total);
					
			size = atol(total);
			data = (char *)malloc((size + 2) * sizeof(char));							
		
			MtDbgLogFileDbg(DEBUG,&log_file,"con_request: data allocated at %ld with size = %ld \n", data,(size + 2) * sizeof(char));

			/* release exclusive access to listener thread management area	*/
			pthread_mutex_unlock(&listener_thr.mutex);
		
			memmove(data,cmd,16);
								
			if (size > 16)
			{
				rc = ReadFromSocket(newsockfd,&data[16],size - 16);
				if (rc <= 0)
				{
					/* get exclusive access to listener thread management area	*/
					pthread_mutex_lock(&listener_thr.mutex);
					
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR - error reading socket %d, ec = %d,%s!\n",newsockfd,errno,strerror(errno));
					
					/* SocketShutdown socket connection	*/
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Shutdown socket %d\n", newsockfd);
					SocketShutdown(newsockfd,SHUT_RDWR);
							
					/* closing open sockets */
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Closing socket %d \n", newsockfd);
					close(newsockfd);

					/* release exclusive access to listener thread management area	*/
					pthread_mutex_unlock(&listener_thr.mutex);
					
					free(data);
					
					continue;					
				}
				else if (rc != (size - 16))
				{
					/* get exclusive access to listener thread management area	*/
					pthread_mutex_lock(&listener_thr.mutex);
					
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR - error reading socket %d, invalid format!\n",newsockfd);
					
					/* SocketShutdown socket connection	*/
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Shutdown socket %d\n", newsockfd);
					SocketShutdown(newsockfd,SHUT_RDWR);
							
					/* closing open sockets */
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Closing socket %d \n", newsockfd);
					close(newsockfd);

					/* release exclusive access to listener thread management area	*/
					pthread_mutex_unlock(&listener_thr.mutex);
					
					free(data);
					
					continue;					
										
				}
			}
					
			data[size] = '\0';
			
			/* get exclusive access to listener thread management area	*/
			pthread_mutex_lock(&listener_thr.mutex);
			
			MtDbgLogFileDbg(DEBUG,&log_file,"con_request: data from socket = %s\n", data);

			/* release exclusive access to listener thread management area	*/
			pthread_mutex_unlock(&listener_thr.mutex);
						
			pclDataBegin = GetKeyItem(cmd,&size,data,"{=CMD=}", "{=",TRUE);
			if (pclDataBegin == NULL)
			{
				/* get exclusive access to listener thread management area	*/
				pthread_mutex_lock(&listener_thr.mutex);
				
				MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR - can't locate {=CMD=} keyword!\n");
				
				/* SocketShutdown socket connection	*/
				MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Shutdown socket %d\n", newsockfd);
				SocketShutdown(newsockfd,SHUT_RDWR);
							
				/* closing open sockets */
				MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Closing socket %d \n", newsockfd);
				close(newsockfd);

				/* release exclusive access to listener thread management area	*/
				pthread_mutex_unlock(&listener_thr.mutex);
					
				free(data);
					
				continue;					
			}
			
			/* get exclusive access to listener thread management area	*/
			pthread_mutex_lock(&listener_thr.mutex);
			
			MtDbgLogFileDbg(TRACE,&log_file,"con_request: executing command = %s from socket = %d\n", cmd,newsockfd);

			/* release exclusive access to listener thread management area	*/
			pthread_mutex_unlock(&listener_thr.mutex);
			
			/* check command types	*/						
			if (strcmp(cmd,"BCOUT") == 0)
			{									
				/* search for a position in the thread management */
				for (i = 0; i < max_bc_clients; i++)
				{
					/* get exclusive access to management area	*/
					pthread_mutex_lock(&argThreadMngmnt[i].mutex);
					if (argThreadMngmnt[i].ThreadID == NULL)
					{
						if (argThreadMngmnt[i].Socket == -1)
						{
							/* setting the thread management information */
							argThreadMngmnt[i].Socket = newsockfd;
							argThreadMngmnt[i].StatusPosition = i;
							prlInAddr = (struct in_addr *) &cli_addr.sin_addr.s_addr;
						    sprintf(argThreadMngmnt[i].ClientIpAddr,"%s",inet_ntoa(*prlInAddr));
						    sprintf(argThreadMngmnt[i].ClientHexAddr,"%08x",ntohl(cli_addr.sin_addr.s_addr));
						    if (argThreadMngmnt[i].BCFilter != NULL)
						    {
								free(argThreadMngmnt[i].BCFilter);						    	
						    }
						    argThreadMngmnt[i].BCFilter = NULL;

							strcpy(clClientIpAddr,argThreadMngmnt[i].ClientIpAddr);
						    
							/* release exclusive access to management area	*/
							pthread_mutex_unlock(&argThreadMngmnt[i].mutex);
							break;
						}
					}
					/* release exclusive access to management area	*/
					pthread_mutex_unlock(&argThreadMngmnt[i].mutex);
				}
				
				/* check on overflow	*/
				if (i == max_bc_clients)
				{
					/* get exclusive access to listener thread management area	*/
					pthread_mutex_lock(&listener_thr.mutex);
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: max_bc_clients connected!\n");
					
					/* SocketShutdown socket connection	*/
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Shutdown socket %d\n", newsockfd);
					SocketShutdown(newsockfd,SHUT_RDWR);
								
					/* closing open sockets */
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Closing socket %d\n", newsockfd);
					close(newsockfd);

					/* release exclusive access to listener thread management area	*/
					pthread_mutex_unlock(&listener_thr.mutex);
						
					free(data);
						
					continue;					
				}

				/* create a new thread to process the incomming request */
				chld_thr = NULL;
				rc = pthread_create(&chld_thr,&argThreadMngmnt[i].ThreadAttribute, client_bc_connector, (void *) &argThreadMngmnt[i]);
				if (rc != 0)
				{
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR - pthread_create failed with rc = %d\n", rc);
				}
				
				if (rc == 0 && chld_thr != NULL)
				{
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: Thread ID %d,%s for queue %d is valid!\n",chld_thr,clClientIpAddr,i);
				}
				else
				{
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR - Thread ID for queue %d is NULL, error = %d,%s!\n",i,errno,strerror(errno));
					
					/* get exclusive access to management area	*/
					pthread_mutex_lock(&argThreadMngmnt[i].mutex);
					
					argThreadMngmnt[i].Socket = -1;
					argThreadMngmnt[i].StatusPosition = -1;
					
					/* release exclusive access to management area	*/
					pthread_mutex_unlock(&argThreadMngmnt[i].mutex);

					/* get exclusive access to listener thread management area	*/
					pthread_mutex_lock(&listener_thr.mutex);
					
					/* SocketShutdown socket connection	*/
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Shutdown socket %d\n", newsockfd);
					SocketShutdown(newsockfd,SHUT_RDWR);
								
					/* closing open sockets */
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Closing socket %d \n", newsockfd);
					close(newsockfd);
					
					/* release exclusive access to listener thread management area	*/
					pthread_mutex_unlock(&listener_thr.mutex);
						
					free(data);
						
					continue;					
					
				}

				/* send queue id aka thread management index to client	*/
				if (send_threadId)
				{
					sprintf(data,"{=TOT=}%-9d{=QUE=}%-4d,%-9d",37,i,chld_thr);
					rc = write(newsockfd,data,37);								
				}
				else
				{
					sprintf(data,"{=TOT=}%-9d{=QUE=}%-4d",27,i);
					rc = write(newsockfd,data,27);								
				}
				if (rc <= 0)
				{
					/* get exclusive access to listener thread management area	*/
					pthread_mutex_lock(&listener_thr.mutex);
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR - error writing socket %d, ec = %d,%s!\n",newsockfd,errno,strerror(errno));
					
					/* SocketShutdown socket connection	*/
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Shutdown socket %d\n", newsockfd);
					SocketShutdown(newsockfd,SHUT_RDWR);
								
					/* closing open sockets */
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Closing socket %d \n", newsockfd);
					close(newsockfd);

					/* release exclusive access to listener thread management area	*/
					pthread_mutex_unlock(&listener_thr.mutex);
						
					free(data);
						
					continue;					
				}
	

				/* the server is now free to accept another socket request */
				MtDbgLogFileDbg(DEBUG,&log_file,"Waiting for next client.\n");
				
			}
			else if (strcmp(cmd,"BCKEY") == 0)
			{
				char queue[100];
				int index 	 = -1;
				int threadId =  0;
				
				/* get exclusive access to listener thread management area	*/
				pthread_mutex_lock(&listener_thr.mutex);
				
				MtDbgLogFileDbg(TRACE,&log_file,"con_request: BCKEY command received !\n");

				/* release exclusive access to listener thread management area	*/
				pthread_mutex_unlock(&listener_thr.mutex);
				
				/* get queue number from command	*/
				pclDataBegin = GetKeyItem(queue,&size,data,"{=QUE=}", "{=",TRUE);
				if (pclDataBegin == NULL)
				{
					/* get exclusive access to listener thread management area	*/
					pthread_mutex_lock(&listener_thr.mutex);
					
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR -{=QUE=} keyword not found within BCKEY command!\n");

					/* SocketShutdown socket connection	*/
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Shutdown socket %d\n", newsockfd);
					SocketShutdown(newsockfd,SHUT_RDWR);
								
					/* closing open sockets */
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Closing socket %d \n", newsockfd);
					close(newsockfd);
					
					/* release exclusive access to listener thread management area	*/
					pthread_mutex_unlock(&listener_thr.mutex);

					free(data);					
					continue;
				}
				
				if (send_threadId)
				{
					index = atoi(queue);
					threadId = atoi(&queue[5]);
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: INFO queue = [%s] index = %d,threadId = %d\n",queue,index,threadId);
				}
				else
				{
					index = atoi(queue);
				}
				
				/* check queue id validity	*/
				if (index < 0 || index >= max_bc_clients)
				{
					/* get exclusive access to listener thread management area	*/
					pthread_mutex_lock(&listener_thr.mutex);
					
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR queue index %d invalid!\n",index);
					
					/* SocketShutdown socket connection	*/
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Shutdown socket %d\n", newsockfd);
					SocketShutdown(newsockfd,SHUT_RDWR);
								
					/* closing open sockets */
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Closing socket %d \n", newsockfd);
					close(newsockfd);
					
					/* release exclusive access to listener thread management area	*/
					pthread_mutex_unlock(&listener_thr.mutex);

					free(data);					
					
					continue;
				}

				/* extract broadcast filter data from command	*/												
				pclDataBegin = GetKeyItem("",&size,data,"{=DAT=}", "{=",FALSE);
				if (pclDataBegin == NULL)	/* no data means no filter in this case	*/
				{
					size = 0;
					
					/* get exclusive access to listener thread management area	*/
					pthread_mutex_lock(&listener_thr.mutex);
					
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: INFO -{=DAT=} keyword not found within BCKEY command -> assuming NO BC filter!\n");
					
					/* release exclusive access to listener thread management area	*/
					pthread_mutex_unlock(&listener_thr.mutex);
				}

				/* get exclusive access to management area	*/
				pthread_mutex_lock(&argThreadMngmnt[index].mutex);

				strcpy(clClientIpAddr,argThreadMngmnt[index].ClientIpAddr);

				/* check, if corresponding thread is active	*/			
				if (send_threadId && (argThreadMngmnt[index].ThreadID != threadId))
				{
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR Thread id for queue index %d,%s invalid!\n",index,clClientIpAddr);
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: ERROR Thread id for queue index %d = %d doesn't match %d!\n",index,argThreadMngmnt[index].ThreadID,threadId);
				}
				else if (argThreadMngmnt[index].ThreadID == NULL)
				{
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR Thread id for queue index %d,%s invalid!\n",index,clClientIpAddr);
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: ERROR Thread id for queue index %d = %d doesn't match %d!\n",index,argThreadMngmnt[index].ThreadID,threadId);
				}
				else
				{				
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: allocate memory!\n");
					
					/* release current filter	*/
					if (argThreadMngmnt[index].BCFilter != NULL)
					{
						free(argThreadMngmnt[index].BCFilter);						    	
					}
					
					argThreadMngmnt[index].BCFilter = NULL;
					
					/* allocate broadcast filter, if necessary	*/
					if (size > 0)
					{
						argThreadMngmnt[index].BCFilter = (char *)malloc((size + 2) * sizeof(char));
						strncpy(argThreadMngmnt[index].BCFilter,pclDataBegin,size);
						argThreadMngmnt[index].BCFilter[size] = '\0';
						
						MtDbgLogFileDbg(DEBUG,&log_file,"con_request: BC Filter for queue index %d,%s = %s!\n",index,clClientIpAddr,argThreadMngmnt[index].BCFilter);
					}
					else
					{
						MtDbgLogFileDbg(DEBUG,&log_file,"con_request: BC Filter for queue index %d,%s = %s!\n",index,clClientIpAddr,".");
					}
				}
								
				/*	release exclusive access to management area	*/
				pthread_mutex_unlock(&argThreadMngmnt[index].mutex);

				/* get exclusive access to listener thread management area	*/
				pthread_mutex_lock(&listener_thr.mutex);

				/* SocketShutdown socket connection	*/
				MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Shutdown socket %d\n", newsockfd);
				SocketShutdown(newsockfd,SHUT_RDWR);
						
				/* closing open sockets */
				MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Closing socket %d\n", newsockfd);
				close(newsockfd);
				
				/* the server is now free to accept another socket request */
				MtDbgLogFileDbg(DEBUG,&log_file,"con_request : Waiting for next client.\n");
				
				/* release exclusive access to listener thread management area	*/
				pthread_mutex_unlock(&listener_thr.mutex);
				
			}
			else if (strcmp(cmd,"BCOFF") == 0)
			{
				char queue[100];
				int index 	 = -1;
				int threadId =  0;
				
				/* get exclusive access to listener thread management area	*/
				pthread_mutex_lock(&listener_thr.mutex);
				
				MtDbgLogFileDbg(TRACE,&log_file,"con_request: BCOFF command received at socket = %d!\n",newsockfd);

				/* release exclusive access to listener thread management area	*/
				pthread_mutex_unlock(&listener_thr.mutex);
				
				/* get queue number from command	*/
				pclDataBegin = GetKeyItem(queue,&size,data,"{=QUE=}", "{=",TRUE);
				if (pclDataBegin == NULL)
				{
					/* get exclusive access to listener thread management area	*/
					pthread_mutex_lock(&listener_thr.mutex);
					
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR -{=QUE=} keyword not found within BCOFF command!\n");

					/* SocketShutdown socket connection	*/
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Shutdown socket %d\n", newsockfd);
					SocketShutdown(newsockfd,SHUT_RDWR);
								
					/* closing open sockets */
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Closing socket %d \n", newsockfd);
					close(newsockfd);
					
					/* release exclusive access to listener thread management area	*/
					pthread_mutex_unlock(&listener_thr.mutex);

					free(data);					
	
					continue;
				}
				
				if (send_threadId)
				{
					index = atoi(queue);
					threadId = atoi(&queue[5]);
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: INFO queue = [%s] index = %d,threadId = %d\n",queue,index,threadId);
				}
				else
				{
					index = atoi(queue);
				}
				
				/* check queue id validity	*/
				if (index < 0 || index >= max_bc_clients)
				{
					/* get exclusive access to listener thread management area	*/
					pthread_mutex_lock(&listener_thr.mutex);
					
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR queue index %d invalid!\n",index);
					
					/* SocketShutdown socket connection	*/
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Shutdown socket %d\n", newsockfd);
					SocketShutdown(newsockfd,SHUT_RDWR);
								
					/* closing open sockets */
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Closing socket %d \n", newsockfd);
					close(newsockfd);
					
					/* release exclusive access to listener thread management area	*/
					pthread_mutex_unlock(&listener_thr.mutex);

					free(data);					
					
					continue;
				}

				/* get exclusive access to management area	*/
				pthread_mutex_lock(&argThreadMngmnt[index].mutex);

				strcpy(clClientIpAddr,argThreadMngmnt[index].ClientIpAddr);

				/* check, if corresponding thread is active	*/			
				if (send_threadId && (argThreadMngmnt[index].ThreadID != threadId))
				{
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR Thread id for queue index %d,%s invalid!\n",index,clClientIpAddr);
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: ERROR Thread id for queue index %d = %d doesn't match %d!\n",index,argThreadMngmnt[index].ThreadID,threadId);
				}
				else if (argThreadMngmnt[index].ThreadID == NULL)
				{
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR Thread id for queue index %d,%s invalid!\n",index,clClientIpAddr);
					MtDbgLogFileDbg(DEBUG,&log_file,"con_request: ERROR Thread id for queue index %d = %d doesn't match %d!\n",index,argThreadMngmnt[index].ThreadID,threadId);
				}
				else
				{				
					/*	stop broadcasting	*/
					pthread_t threadID;					
					void*	  exitArg;
					threadID = argThreadMngmnt[index].ThreadID;

#if	1
					/* get exclusive access to listener thread management area	*/
					pthread_mutex_lock(&listener_thr.mutex);

					/* set thread exit status	*/
					argThreadMngmnt[index].Exit = TRUE;
					
#if	0				/* due to accept error = 9	*/
					
					/* close sockets	*/
					if (argThreadMngmnt[index].Socket != -1)
					{
						/* SocketShutdown socket connection	*/
						MtDbgLogFileDbg(TRACE,&log_file,"con_request: Shutdown socket %d (connection with client %s)\n", argThreadMngmnt[index].Socket, argThreadMngmnt[index].ClientIpAddr);
						SocketShutdown(argThreadMngmnt[index].Socket,SHUT_RDWR);
						
						/* closing open sockets */
						MtDbgLogFileDbg(TRACE,&log_file,"con_request: Closing socket %d (connection with client %s)\n", argThreadMngmnt[index].Socket, argThreadMngmnt[index].ClientIpAddr);
						close(argThreadMngmnt[index].Socket);
					}
#endif
					
					/* wakeup the threads	*/
					pthread_cond_broadcast(&WaitNext);
					
					/* release exclusive access to listener thread management area	*/
					pthread_mutex_unlock(&listener_thr.mutex);
					
#else
					/* get exclusive access to listener thread management area	*/
					pthread_mutex_lock(&listener_thr.mutex);
					/* cancel this thread	*/
					rc = pthread_cancel(argThreadMngmnt[index].ThreadID);
					MtDbgLogFileDbg(TRACE,&log_file,"con_request : Thread %d,%s for queue index %d cancelled with rc = %d\n",threadID,clClientIpAddr,index,rc);
					/* detach this thread from memory	*/
					rc = pthread_detach(argThreadMngmnt[index].ThreadID);
					MtDbgLogFileDbg(TRACE,&log_file,"con_request : Thread %d,%s for queue index %d detached with rc = %d\n",threadID,clClientIpAddr,index,rc);

					if (argThreadMngmnt[index].Socket != -1)
					{
						/* SocketShutdown socket connection	*/
						MtDbgLogFileDbg(TRACE,&log_file,"con_request: Shutdown socket %d (connection with client %s)\n", argThreadMngmnt[index].Socket, argThreadMngmnt[index].ClientIpAddr);
						SocketShutdown(argThreadMngmnt[index].Socket,SHUT_RDWR);
						
						/* closing open sockets */
						MtDbgLogFileDbg(TRACE,&log_file,"con_request: Closing socket %d (connection with client %s)\n", argThreadMngmnt[index].Socket, argThreadMngmnt[index].ClientIpAddr);
						close(argThreadMngmnt[index].Socket);
					}

					/* release exclusive access to listener thread management area	*/
					pthread_mutex_unlock(&listener_thr.mutex);
					
					ThreadMngmntInit(&argThreadMngmnt[index],FALSE);
#endif
				}
								
				/*	release exclusive access to management area	*/
				pthread_mutex_unlock(&argThreadMngmnt[index].mutex);
				
				/* get exclusive access to listener thread management area	*/
				pthread_mutex_lock(&listener_thr.mutex);

				/* SocketShutdown socket connection	*/
				MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Shutdown socket %d\n", newsockfd);
				SocketShutdown(newsockfd,SHUT_RDWR);
						
				/* closing open sockets */
				MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Closing socket %d\n", newsockfd);
				close(newsockfd);
				
				/* the server is now free to accept another socket request */
				MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Waiting for next client.\n");

				/* release exclusive access to listener thread management area	*/
				pthread_mutex_unlock(&listener_thr.mutex);
				
			}
			else if (strcmp(cmd,"BCINF") == 0)	/* send connection information to client monitor...	*/
			{
				int 	index = -1;
				int 	count   = 0;
				char*	pclData = (char *)malloc((max_bc_clients + 1) * 256);	
				char	pclLine[256];

				sprintf(pclData,"{=TOT=}%-9d{=COUNT=}%-4d{\\=COUNT=}{=DATA=}",16,0);
									
				/* get connection information from thread management	*/
				for (index = 0; index < max_bc_clients; index++)
				{
					/* get exclusive access to management area	*/
					pthread_mutex_lock(&argThreadMngmnt[index].mutex);
					
					if (argThreadMngmnt[index].ThreadID != NULL)
					{
						++count;
						sprintf(pclLine,"%d,%d,%d,%s,%s,%d,%d\n",index,argThreadMngmnt[index].ThreadID,argThreadMngmnt[index].Socket,argThreadMngmnt[index].ClientIpAddr,argThreadMngmnt[index].ClientHexAddr,argThreadMngmnt[index].ItemsSend,argThreadMngmnt[index].ItemsTotal);
						strcat(pclData,pclLine);
					}
												
					/*	release exclusive access to management area	*/
					pthread_mutex_unlock(&argThreadMngmnt[index].mutex);
				}				
				
				strcat(pclData,"{\\=DATA=}");
				
				/* send thread management information to monitor client */
				sprintf(pclLine,"{=TOT=}%-9d{=COUNT=}%-4d",strlen(pclData),count);
				/* overwrite this part of the data	*/
				memmove(pclData,pclLine,strlen(pclLine));

				/* get exclusive access to listener thread management area	*/
				pthread_mutex_lock(&listener_thr.mutex);
				
				MtDbgLogFileDbg(DEBUG,&log_file,"con_request: BCINF - sending connection info = %s!\n",pclData);

				/* release exclusive access to listener thread management area	*/
				pthread_mutex_unlock(&listener_thr.mutex);
				
				rc = write(newsockfd,pclData,strlen(pclData));								
				if (rc <= 0)
				{
					/* get exclusive access to listener thread management area	*/
					pthread_mutex_lock(&listener_thr.mutex);
					
					MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR - error writing socket %d, ec = %d,%s!\n",newsockfd,errno,strerror(errno));
					
					/* release exclusive access to listener thread management area	*/
					pthread_mutex_unlock(&listener_thr.mutex);
				
				}
				else
				{					
					rc = ReadKeywordFromSocket(newsockfd,"{=ACK=}",pclLine);
					
					/* get exclusive access to listener thread management area	*/
					pthread_mutex_lock(&listener_thr.mutex);
				
					if (rc <= 0)
					{
						MtDbgLogFileDbg(TRACE,&log_file,"con_request: Error reading ACK from socket %d, ec = %d,%s\n", newsockfd,errno,strerror(errno));
					}

					/* release exclusive access to listener thread management area	*/
					pthread_mutex_unlock(&listener_thr.mutex);
				
				}

				/* get exclusive access to listener thread management area	*/
				pthread_mutex_lock(&listener_thr.mutex);

				/* SocketShutdown socket connection	*/
				MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Shutdown socket %d\n", newsockfd);
				SocketShutdown(newsockfd,SHUT_RDWR);
						
				/* closing open sockets */
				MtDbgLogFileDbg(DEBUG,&log_file,"con_request: Closing socket %d\n", newsockfd);
				close(newsockfd);

				/* release exclusive access to listener thread management area	*/
				pthread_mutex_unlock(&listener_thr.mutex);
						
				free(pclData);
	
			}
			else
			{
				/* get exclusive access to listener thread management area	*/
				pthread_mutex_lock(&listener_thr.mutex);
				
				MtDbgLogFileDbg(TRACE,&log_file,"con_request: ERROR - unsupported command %s ignored!\n",cmd);
				
				/* release exclusive access to listener thread management area	*/
				pthread_mutex_unlock(&listener_thr.mutex);
			}
			
			if (data != NULL)
			{
				free(data);
				data = NULL;
			}
		}
	}
#endif
}

/*******************************/
/* initializing of the process */
/*******************************/
static int Init_bccom()
{
	int i = 0;
	char clTmpBuf[512];
	int ilLevel;
	int	ilRc;

	/* initializing the log files for threads and signals */
    sprintf(log_file_name,"%s/%s%.5ld.txt",getenv("DBG_PATH"),mod_name,getpid());
    sprintf(signal_log_file_name,"%s/%s_signal.log",getenv("DBG_PATH"),mod_name);

	/* initialize configuration information	*/
	if (GetDebugLevel(&ilLevel) == RC_SUCCESS)
	{
		debug_level = ilLevel;
	}
	
	/* read config file entries	*/
	if (ReadConfigEntry("MAIN","tcp_port",clTmpBuf) == RC_SUCCESS)
	{
		tcp_port = atoi(clTmpBuf);
	}

	if (ReadConfigEntry("MAIN","not_filtered",clTmpBuf) == RC_SUCCESS)
	{
		strcpy(not_filtered,clTmpBuf);
	}
	
	if (ReadConfigEntry("MAIN","max_bc_clients",clTmpBuf) == RC_SUCCESS)
	{
		max_bc_clients = atol(clTmpBuf);
	}
	
	if (ReadConfigEntry("MAIN","max_bc_buffers",clTmpBuf) == RC_SUCCESS)
	{
		max_bc_buffers = atol(clTmpBuf);
	}
	
	if (ReadConfigEntry("MAIN","reuse_address",clTmpBuf) == RC_SUCCESS)
	{
		if (strcmp(clTmpBuf,"TRUE") == 0)
			reuse_address = 1;
		else
			reuse_address = 0;
	}
	
	if (ReadConfigEntry("MAIN","mainsleep",clTmpBuf) == RC_SUCCESS)
	{
		mainsleep.tv_usec = atoi(clTmpBuf);
		mainsleep.tv_sec = 0;		
	}
	else
	{
		mainsleep.tv_sec  = 0;
		mainsleep.tv_usec = 10;		
	}
	
	if (ReadConfigEntry("MAIN","reply_delay",clTmpBuf) == RC_SUCCESS)
	{
		reply_delay = atoi(clTmpBuf);
	}
	else
	{
		reply_delay = 0;
	}
	
	if (ReadConfigEntry("MAIN","client_handshake",clTmpBuf) == RC_SUCCESS)
	{
		if (strcmp(clTmpBuf,"TRUE") == 0)		
			client_handshake = TRUE;
		else
			client_handshake = FALSE;
	}

	if (ReadConfigEntry("MAIN","use_shutdown",clTmpBuf) == RC_SUCCESS)
	{
		if (strcmp(clTmpBuf,"TRUE") == 0)		
			use_shutdown = TRUE;
		else
			use_shutdown = FALSE;
	}

	if (ReadConfigEntry("MAIN","use_linger",clTmpBuf) == RC_SUCCESS)
	{
		if (strcmp(clTmpBuf,"TRUE") == 0)		
			use_linger = TRUE;
		else
			use_linger = FALSE;
	}
	
	if (ReadConfigEntry("MAIN","send_threadid",clTmpBuf) == RC_SUCCESS)
	{
		if (strcmp(clTmpBuf,"TRUE") == 0)		
			send_threadId = TRUE;
		else
			send_threadId = FALSE;
	}

	if (ReadConfigEntry("MAIN","max_threadcount",clTmpBuf) == RC_SUCCESS)
	{
		max_threadCount = atoi(clTmpBuf);
	}
	else
	{
		max_threadCount = 0;
	}


	/* initialize the service	*/	
    strcpy(pcgService,"UFIS_BCC");
	
	/* dump current configuration	*/
	dbg(TRACE,"Init_bccom() : debug_level = %d",debug_level);
	dbg(TRACE,"Init_bccom() : tcp_port = %d",tcp_port);
	dbg(TRACE,"Init_bccom() : not_filtered = %s",not_filtered);
	dbg(TRACE,"Init_bccom() : max_bc_clients = %d",max_bc_clients);
	dbg(TRACE,"Init_bccom() : max_bc_buffers = %d",max_bc_buffers);
	dbg(TRACE,"Init_bccom() : reuse_address  = %d",reuse_address);
	dbg(TRACE,"Init_bccom() : mainsleep = %d milliseconds",mainsleep.tv_usec);
	dbg(TRACE,"Init_bccom() : reply_delay  = %d",reply_delay);
	dbg(TRACE,"Init_bccom() : client_handshake = %d",client_handshake);
	dbg(TRACE,"Init_bccom() : use_shutdown  = %d",use_shutdown);
	dbg(TRACE,"Init_bccom() : use_linger  	= %d",use_linger);
	dbg(TRACE,"Init_bccom() : send_threadid  = %d",send_threadId);
	dbg(TRACE,"Init_bccom() : max_threadcount  = %d",max_threadCount);
		
#ifdef	_SUN_UNIX		
	/* initialize mutex variable	*/
	dbg(TRACE,"Init_bccom(): Initializing mutex variable.");
	mutex_init(&DataArrival,USYNC_THREAD,NULL);			
	
	dbg(TRACE,"Init_bccom(): Initializing rwlock variable.");
	rwlock_init(&DataAccess,USYNC_THREAD,NULL);			
	
	dbg(TRACE,"Init_bccom(): Initializing rwlock variable.");
	rwlock_init(&CLOSend,USYNC_THREAD,NULL);			
	
	dbg(TRACE,"Init_bccom(): Initializing condition variable.");
	cond_init(&WaitNext,USYNC_THREAD,NULL);
#else
	/* initialize mutex variable	*/
	dbg(TRACE,"Init_bccom(): Initializing mutex variable.");
	pthread_mutex_init(&DataArrival,NULL);			
	
	dbg(TRACE,"Init_bccom(): Initializing rwlock variable.");
	pthread_rwlock_init(&DataAccess,NULL);			
	
	dbg(TRACE,"Init_bccom(): Initializing rwlock variable.");
	pthread_rwlock_init(&CLOSend,NULL);			
	
	dbg(TRACE,"Init_bccom(): Initializing condition variable.");
	pthread_cond_init(&WaitNext,NULL);
#endif	
	
	
	/* init the threads log file	*/
	dbg(TRACE,"Init_bccom(): Initializing the threads log file.");
	ilRc = MtDbgLogFileInit(&log_file,mod_name,mod_version,log_file_name);
	if (ilRc != RC_SUCCESS)
		return ilRc;
	
	/* init the BC buffer queue*/
	dbg(TRACE,"Init_bccom(): Initializing the BC-buffer.");
	ilRc = QueueInit(&argBcQueue,max_bc_buffers,max_bc_clients);	
	if (ilRc != RC_SUCCESS)
		return ilRc;

	/* init the array to identify which status position which thread has got */
	dbg(TRACE,"Init_bccom(): Initializing the thread management array.");
	argThreadMngmnt = (THREAD_MNGMNT *)malloc(max_bc_clients * sizeof(THREAD_MNGMNT));
	
	/* initialize thread members the first time	*/
  	for (i = 0; i < max_bc_clients; i++)
  	{
  		ilRc = ThreadMngmntInit(&argThreadMngmnt[i],TRUE);
  		if (ilRc != RC_SUCCESS)
  			return ilRc;
	}

	dbg(TRACE,"Init_bccom(): Initializing the listener thread.");
	ilRc = ThreadMngmntInit(&listener_thr,TRUE);
 	if (ilRc != RC_SUCCESS)
  		return ilRc;
	
	dbg(TRACE,"Init_bccom(): Initializing completed successfully");
	return RC_SUCCESS;
}

/******************************************************************************/
/* The handle signals routine/thread                                          */
/* The  letters  in  the  "Action"  column have the following meanings:       */
/*       A      Default action is to terminate the process.                   */
/*       B      Default action is to ignore the signal.                       */
/*       C      Default action is to dump core.                               */
/*       D      Default action is to stop the process.                        */
/*       E      Signal cannot be caught.                                      */
/*       F      Signal cannot be ignored.                                     */
/*       G      Not a POSIX.1 conformant signal.                              */
/******************************************************************************/
void *signal_hand(void *arg)
{
	sigset_t set;
	int err;
	int sig;
	char s[22];
	FILE *fp = NULL;

	sigfillset(&set); /* catch all signals */

	/* open the logfile*/
	fp = fopen(signal_log_file_name,"a+");

	while (1)
	{
#ifdef	_SUN_UNIX		
#ifdef	_POSIX_PTHREAD_SEMANTICS		
		/* wait for a signal to arrive */
		err = sigwait(&set,&sig);
#else
		err = 0;
		nap(100);
		sig = sigwait(&set);
#endif
#else
		/* wait for a signal to arrive */
		err = sigwait(&set,&sig);
#endif
		get_time (s);
		if (err)
		{
			fprintf(fp,"%s sigwait returned with error %d\n", s,err); 
			fflush(fp);
		}
		else
		{
			switch (sig)
			{
				/* here you would add whatever signal you needed to catch */
				
				case SIGHUP:
					fprintf(fp,"%s Received signal %d A: SIGHUP. Hangup detected on controlling terminal or death of controlling process.\n", s, sig); 
					fflush(fp); break;
	
				case SIGINT :
					fprintf(fp,"%s Received signal %d A: SIGINT. Interrupt from keyboard\n", s, sig);
					fflush(fp); break;
	
				case SIGQUIT:
					fprintf(fp,"%s Received signal %d A: SIGQUIT. Quit from keyboard\n", s, sig);
					fflush(fp); break;
	
				case SIGILL:
					fprintf(fp,"%s Received signal %d A: SIGILL. Illegal Instruction\n", s, sig);
					fflush(fp); break;
	
				case SIGABRT:
					fprintf(fp,"%s Received signal %d C: SIGABRT. Abort signal from abort\n", s, sig);
					fflush(fp); break;
	
				case SIGFPE:
					fprintf(fp,"%s Received signal %d C: SIGFPE. Floating point exception\n", s, sig);
					fflush(fp); break;
	
				case SIGKILL:
					fprintf(fp,"%s Received signal %d AEF: SIGKILL. Kill signal\n", s, sig);
					fflush(fp); break;
	
				case SIGSEGV:
					fprintf(fp,"%s Received signal %d C: SIGSEGV. Invalid memory reference\n", s, sig);
					fflush(fp); break;
	
				case SIGPIPE:
					fprintf(fp,"%s Received signal %d A: SIGPIPE. Broken pipe: write to pipe with no readers\n", s, sig);
					fflush(fp); break;
	
				case SIGALRM:
					fprintf(fp,"%s Received signal %d A: SIGALRM. Timer signal from alarm\n", s, sig);
					fflush(fp); break;
	
				case SIGTERM:
					fprintf(fp,"%s Received signal %d A: SIGTERM. Termination signal\n", s, sig);
					fflush(fp); break;
	
				/* TODO: to be continued if necessary */
	
				default :
					fprintf(fp,"%s Received signal %d.\n", s, sig);
					fflush(fp);
					break;
	
			} /* end of switch */
		}	/* end of if	*/
	} /* end of while */

	return((void *)0);
} /* end of signal_hand */

/******************************************************************/
/******************************************************************/
static int TranslateEvent(EVENT *prlEvent,int ipTail, int ipTrimData)
{
	int 	ilRc = RC_SUCCESS;
	int 	ilOutDatPos = 0;
	int		ilOutDatLen = 0;
	char 	pclKeyTot[64];
	char 	pclKeyCmd[64];
	char 	pclKeyTbl[64];
	char 	pclKeyTws[64];
	char 	pclKeyTwe[64];
	char 	pclKeyUsr[64];
	char 	pclKeyWks[64];
	char 	pclKeyBcn[64];

  	BC_HEAD*prlBchead     = NULL;
  	CMDBLK*	prlCmdblk     = NULL;
  	char*	pclSelection  = NULL;
  	char*	pclFields     = NULL;
  	char*	pclData       = NULL;
  	char*	pclError      = NULL;
  	char*	pclNoError    = "\0";
  	char*	pclOutData	   = NULL;
  	char*	pclAttachment = NULL;	

  	dbg(DEBUG,"%s GOT CEDA EVENT",pcgMyProcName);
  	igStopQueGet = FALSE;
  	igStopBcHdl = FALSE;

  	if (prlEvent->command == EVENT_DATA)
  	{
    	prlBchead    = (BC_HEAD *) ((char *) prlEvent + sizeof(EVENT)) ;
    	if (prlBchead->rc != RC_FAIL)
    	{
      		pclError = pclNoError;
      		prlCmdblk    = (CMDBLK *)  ((char *) prlBchead->data) ;
      		pclSelection = (char *)    prlCmdblk->data ;
      		pclFields    = (char *)    pclSelection + strlen(pclSelection) + 1 ;
      		pclData      = (char *)    pclFields + strlen(pclFields) + 1 ;
      		pclAttachment= strchr(pclData,'\n');
      		if (pclAttachment != NULL)
	  			++pclAttachment;	  		      
      		TrimAndFilterCr(pclData, ",","\n");
		} /* end if */
    	else
    	{
      		pclError   = prlBchead->data;
      		prlCmdblk    = (CMDBLK *)((char *) pclError + strlen(pclError) + 1) ;
      		pclSelection = (char *)    prlCmdblk->data ;
      		pclFields    = (char *)    pclSelection + strlen(pclSelection) + 1 ;
      		pclData      = (char *)    pclFields + strlen(pclFields) + 1 ;
      		pclData 	   = pclNoError;
    	} /* end else */

	    memset (pclKeyUsr, '\0', sizeof(prlBchead->dest_name) + 1) ;
	    strncpy (pclKeyUsr, prlBchead->dest_name, sizeof(prlBchead->dest_name)) ;
	    memset (pclKeyWks, '\0', sizeof(prlBchead->recv_name) + 1) ;
	    strncpy (pclKeyWks, prlBchead->recv_name, sizeof(prlBchead->recv_name)) ;
	    memset (pclKeyCmd, '\0', sizeof(prlCmdblk->command) + 1) ;
	    strncpy (pclKeyCmd, prlCmdblk->command, sizeof(prlCmdblk->command)) ;
	    memset (pclKeyTws, '\0', sizeof(prlCmdblk->tw_start) + 1) ;
	    strncpy (pclKeyTws, prlCmdblk->tw_start, sizeof(prlCmdblk->tw_start)) ;
	    memset (pclKeyTwe, '\0', sizeof(prlCmdblk->tw_end) + 1) ;
	    strncpy (pclKeyTwe, prlCmdblk->tw_end, sizeof(prlCmdblk->tw_end)) ;
	    memset (pclKeyTbl, '\0', sizeof(prlCmdblk->obj_name) + 1) ;
	    strncpy (pclKeyTbl, prlCmdblk->obj_name, sizeof(prlCmdblk->obj_name)) ;
	    sprintf(pclKeyBcn,"%d",prlBchead->bc_num);

		ilOutDatLen = prlEvent->data_length;
		ilOutDatLen+= 100;	/* the keywords	*/
    	if (pclAttachment != NULL && pclAttachment[0] != '\0')
    		ilOutDatLen += strlen(pclAttachment);
    		
		ilRc = QueueValueInit(&argBcQueue,ipTail,ilOutDatLen,pclKeyTbl,pclKeyCmd,pclKeyBcn);
		if (ilRc != RC_SUCCESS)
			return ilRc;

	    ilOutDatPos = 16;

	    if (pclKeyBcn[0] != '\0')
			ilRc = QueueSetValue(&argBcQueue,ipTail,&ilOutDatPos,"{=BCNUM=}",pclKeyBcn);
    	if (pclKeyCmd[0] != '\0')
      		ilRc = QueueSetValue(&argBcQueue,ipTail,&ilOutDatPos,"{=CMD=}",pclKeyCmd);
    	if (pclKeyTbl[0] != '\0')
      		ilRc = QueueSetValue(&argBcQueue,ipTail,&ilOutDatPos,"{=TBL=}",pclKeyTbl);
    	if (pclKeyUsr[0] != '\0')
      		ilRc = QueueSetValue(&argBcQueue,ipTail,&ilOutDatPos,"{=USR=}",pclKeyUsr);
    	if (pclKeyWks[0] != '\0')
      		ilRc = QueueSetValue(&argBcQueue,ipTail,&ilOutDatPos,"{=WKS=}",pclKeyWks);
    	if (pclKeyTws[0] != '\0')
      		ilRc = QueueSetValue(&argBcQueue,ipTail,&ilOutDatPos,"{=TWS=}",pclKeyTws);
    	if (pclKeyTwe[0] != '\0')
      		ilRc = QueueSetValue(&argBcQueue,ipTail,&ilOutDatPos,"{=TWE=}",pclKeyTwe);
    	if (prlBchead->rc == RC_FAIL)
      		ilRc = QueueSetValue(&argBcQueue,ipTail,&ilOutDatPos,"{=ERR=}",pclError);
    	if (pclSelection[0] != '\0')
      		ilRc = QueueSetValue(&argBcQueue,ipTail,&ilOutDatPos,"{=WHE=}",pclSelection);
    	if (pclFields[0] != '\0')
      		ilRc = QueueSetValue(&argBcQueue,ipTail,&ilOutDatPos,"{=FLD=}",pclFields);
    	if (pclData[0] != '\0')
      		ilRc = QueueSetValue(&argBcQueue,ipTail,&ilOutDatPos,"{=DAT=}",pclData);
    	if (pclAttachment != NULL && pclAttachment[0] != '\0')
    	{
      		ilRc = QueueSetValue(&argBcQueue,ipTail,&ilOutDatPos,"",pclAttachment);
    	}

		if (pclAttachment == NULL)
		{
	  		dbg(DEBUG,"Attachment == NULL");      
		}
		else
		{
	  		dbg(DEBUG,"Attachment=[%s]",pclAttachment);      
		}

		if (ilRc != RC_SUCCESS)	
    	{
    		dbg(TRACE,"ilOutDatPos = %d >= ilOutDatLen = %d!",ilOutDatPos,ilOutDatLen);
    		return ilRc;
		}

		ilRc = QueueSetUsedLen(&argBcQueue,ipTail,ilOutDatPos);
	    if (ilRc != RC_SUCCESS)
	    	return ilRc;
		
	    sprintf(pclKeyTot,"%09d",ilOutDatPos);
	    ilOutDatPos = 0;
	    ilRc = QueueSetValue(&argBcQueue,ipTail,&ilOutDatPos,"{=TOT=}",pclKeyTot);
	    if (ilRc != RC_SUCCESS)
	    	return ilRc;
	 
	 	dbg(DEBUG,"Event used len = %d and value = [%s]",QueueGetUsedLen(&argBcQueue,ipTail),QueueGetValue(&argBcQueue,ipTail));
	 	
		if (strcmp(pclKeyCmd,"BCKEY") == 0)
	    {
	      if (pclSelection[0] != '\0')
	      {
	        strcpy(pcgBcKeyFlag,"F");
	      }
	      else
	      {
	        strcpy(pcgBcKeyFlag,"E");
	      }
	    } /* end if */
	    else if (strcmp(pclKeyCmd,"BCGET") == 0)
	    {
	    } /* end if */
	    else if (strcmp(pclKeyCmd,"CLO") == 0)
	    {
	      dbg(TRACE,"%s GOT <CLO> COMMAND (BCHDL CLOSED)",pcgMyProcName);
	      igStopBcHdl = TRUE;
	    } /* end if */
	    else if (strcmp(pclKeyCmd,"QCPC") == 0)
	    {
	      dbg(TRACE,"%s GOT <QCPC> COMMAND (RESET FROM MAIN)",pcgMyProcName);
	      igStopQueGet = TRUE;
	    } /* end if */
	
	    ilRc = RC_SUCCESS;
	} /* end if */
	else
	{
		dbg(TRACE,"TRANSLATE: EVENT WITHOUT DATA (TYPE=%d)",prlEvent->command);
	    ilRc = RC_FAIL;
	} /* end else */
	
	return ilRc; 
} /* end TranslateEvent */


static void TrimAndFilterCr(char *pclTextBuff,char *pcpFldSep, char *pcpRecSep)
{
	char *pclDest = NULL;
	char *pclSrc = NULL;
	char *pclLast = NULL; /* last non blank byte position */
	pclDest = pclTextBuff;
	pclSrc  = pclTextBuff;
	pclLast  = pclTextBuff - 1;
    dbg(DEBUG,"%s TRIMMING DATA",pcgMyProcName);
	while (*pclSrc != '\0')
	{
		if (*pclSrc != '\r')
		{
			if ((*pclSrc == pcpFldSep[0]) || (*pclSrc == pcpRecSep[0]))
			{
				pclDest = pclLast + 1;
			}
			*pclDest = *pclSrc;
			if (*pclDest != ' ')
			{
				pclLast = pclDest;
			}
			pclDest++;
		}
		pclSrc++;
	}
	pclDest = pclLast + 1;
	*pclDest = '\0';
	return;
} /* end TrimAndFilterCr */

static void get_time(char *s)
{
	struct timeb tp;
	time_t _CurTime;
	struct tm *CurTime;
	struct tm rlTm;
	long secVal, minVal, hourVal;
	char tmp[10];

	if (s != NULL)
	{
		_CurTime = time(0L);
		CurTime = (struct tm *)localtime(&_CurTime);
		rlTm = *CurTime;
		strftime(s,20,"%" "Y%" "m%" "d%" "H%" "M%" "S",&rlTm);

		ftime(&tp);
		minVal = tp.time / 60;
		secVal = tp.time % 60;
		hourVal= minVal / 60;
		minVal = minVal % 60;
		sprintf(tmp,":%3.3d:",tp.millitm);
		strcat(s,tmp);
     }
}

static char* GetKeyItem(char *pcpResultBuff, long *plpResultSize,char *pcpTextBuff, char *pcpKeyWord,char *pcpItemEnd,int bpCopyData) 
{ 
	long llDataSize = 0L; 
	char *pclDataBegin = NULL; 
	char *pclDataEnd = NULL; 
	pclDataBegin = strstr(pcpTextBuff, pcpKeyWord); 
	/* Search the keyword */ 
	if (pclDataBegin != NULL) 
	{ 
		/* Did we find it? Yes. */ 
		pclDataBegin += strlen(pcpKeyWord); 
		/* Skip behind the keyword */ 
		pclDataEnd = strstr(pclDataBegin, pcpItemEnd); 
		/* Search end of data */ 
		if (pclDataEnd == NULL) 
		{ 
			/* End not found? */ 
			pclDataEnd = pclDataBegin + strlen(pclDataBegin); 
			/* Take the whole string */ 
		} /* end if */ 
		llDataSize = pclDataEnd - pclDataBegin; 
		/* Now calculate the length */ 
		if (bpCopyData == TRUE) 
		{ 
			/* Shall we copy? */ 
			strncpy(pcpResultBuff, pclDataBegin, llDataSize); 
			/* Yes, strip out the data */ 
		} /* end if */ 
	} /* end if */ 
	if (bpCopyData == TRUE) 
	{ 
		/* Allowed to set EOS? */ 
		pcpResultBuff[llDataSize] = 0x00; 
		/* Yes, terminate string */ 
	} /* end if */ 
	*plpResultSize = llDataSize; 
	/* Pass the length back */ 
	return pclDataBegin; 
	/* Return the data's begin */ 
} /* end GetKeyItem */ 

/* ******************************************************************** */
static int CreateCLOEvent(EVENT **prpEvent,char *pcpAnswer)
{
  	EVENT	*prlOutEvent	= NULL;
	BC_HEAD *prlBchead      = NULL;
	CMDBLK  *prlCmdblk      = NULL;
	int		ilLen;
	
	if (prpEvent == NULL)
		return RC_FAIL;

	/* initialization	*/
	*prpEvent = NULL;
	
	/* calculate size of memory we need */
  	ilLen = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + strlen(pcpAnswer) + 32; 

	/* get memory for out event */
  	if ((prlOutEvent = (EVENT*)malloc((size_t)ilLen)) == NULL)
    {
		dbg(TRACE,"<CreateCLOEvent> malloc failed, can't allocate %d bytes", ilLen);
		dbg(DEBUG,"<CreateCLOEvent> ----- END -----");
      	return RC_FAIL;
    }

	/* clear buffer */
  	memset((void*)prlOutEvent, 0x00, ilLen);

	/* set structure members */
  	prlOutEvent->type	 	 = SYS_EVENT;
  	prlOutEvent->command 	 = EVENT_DATA;
  	prlOutEvent->originator  = mod_id;
  	prlOutEvent->retry_count = 0;
  	prlOutEvent->data_offset = sizeof(EVENT);
  	prlOutEvent->data_length = ilLen - sizeof(EVENT);

	prlBchead    = (BC_HEAD*)((char*)prlOutEvent+sizeof(EVENT));
	prlBchead->rc = RC_SUCCESS;
	

    strcpy(prlBchead->orig_name,"BCCOM");
 	strcpy(prlBchead->dest_name,"") ;
	strcpy(prlBchead->recv_name,"") ;

	prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
    strcpy(prlCmdblk->command,"CLO") ;
    strcpy(prlCmdblk->obj_name,"CLOTAB") ;

	/* data */
  	strcpy(prlCmdblk->data+2, pcpAnswer);
		
	*prpEvent = prlOutEvent;

	return RC_SUCCESS;		
}

static int ReadFromSocket(int socketID,void *vptr,size_t nBytes)
{
	size_t nleft;
	size_t nread;
	char * ptr;
	ptr = vptr;
	nleft = nBytes;
	
	while(nleft > 0)
	{
		if ((nread = read(socketID,ptr,nleft)) < 0)
		{
			return nread;				
		}
		else if (nread == 0)
		{
			break;			
		}		
		
		nleft -= nread;
		ptr	  += nread;
	}
	
	return (nBytes - nleft);				
}

static int ReadKeywordFromSocket(int socketID,char *pcpKeyword,char pcpResult[256])
{
	int 	ilRc;
	char	cmd[20];
	char	data[256];
	char	total[20];
	char*	pclDataBegin;
	long	size;
				
	/* ASSERTS	*/
	if (pcpKeyword == NULL)
		return RC_FAIL;
	
	ilRc = ReadFromSocket(socketID,pcpResult,16);
	if (ilRc <= 0)
	{
		sprintf(pcpResult,"ReadKeywordFromSocket: ERROR - read from socket failed with ec = %d,%s!\n",errno,strerror(errno));
		return RC_FAIL;
	}
	else if (ilRc != 16)
	{
		sprintf(pcpResult,"ReadKeywordFromSocket: ERROR - read from socket failed : invalid format!\n");
		return RC_FAIL;
	}
		
	pclDataBegin = GetKeyItem(total,&size,cmd,"{=TOT=}", "{=",TRUE);
	if (pclDataBegin == NULL)
	{
		strcpy(pcpResult,"ReadKeywordFromSocket: ERROR - can't locate {=TOT=} keyword!\n");
		return RC_FAIL;
	}
			
	size = atol(total);
	if (size <= 0 || size >= sizeof(data))
	{
		sprintf(pcpResult,"ReadKeywordFromSocket: ERROR - size %d exceeds limitation %d!\n",size,sizeof(data));
		return RC_FAIL;
	}
		
	memmove(data,cmd,16);
						
	if (size > 16)
	{
		ilRc = ReadFromSocket(socketID,&data[16],size - 16);
		if (ilRc <= 0)
		{
			sprintf(pcpResult,"ReadKeywordFromSocket: ERROR - read from socket failed with ec = %d,%s!\n",errno,strerror(errno));
			return RC_FAIL;
		}
		else if (ilRc != (size - 16))
		{
			sprintf(pcpResult,"ReadKeywordFromSocket: ERROR - read from socket failed : invalid format!\n");
			return RC_FAIL;
		}
	}
			
	data[size] = '\0';
			
	pclDataBegin = GetKeyItem(pcpResult,&size,data,pcpKeyword, "{=",TRUE);
	if (pclDataBegin == NULL)
	{
		sprintf(pcpResult,"ReadKeywordFromSocket: ERROR - can't locate %s keyword!\n",pcpKeyword);
		return RC_FAIL;
	}

	return RC_SUCCESS;
}

static int SetNonBlocking(int socketID)
{
	int opts;
	opts = fcntl(socketID,F_GETFL);
	if (opts < 0)
		return RC_FAIL;
		
	opts = opts | O_NONBLOCK;
	if (fcntl(socketID,F_SETFL,opts) < 0)
		return RC_FAIL;
		
	return RC_SUCCESS;						
}

static int SocketReadyForWrite(int socketID,struct timeval tv)
{
	int rc;
	fd_set	fds;
	
	FD_ZERO(&fds);
	FD_SET(socketID,&fds);
	
	rc = select(socketID+1,NULL,&fds,NULL,&tv);
	if (rc < 0)
		return RC_FAIL;
	
	return FD_ISSET(socketID,&fds) ? RC_SUCCESS : RC_FAIL;			
}

static int SocketShutdown(int socketID, int how)
{
	int rc = 0;
	
	if (use_shutdown)
	{
		rc = shutdown(socketID,how);		
	}		
	
	return rc;
}

static int CheckQueueStatus(QUEUE *pQueue,int bpCleanup)
{
	int i;
	int rc;
	int j;
	
	if (pQueue == NULL)
		return RC_FAIL;
		
	/* check state of current head position	*/
	if (strstr(pQueue->BcBuffer[pQueue->Head].Status,"1") == NULL)
		return RC_SUCCESS;
		
	dbg(DEBUG,"Some queue items not sent,checking threads ...");
		
#ifdef	_SUN_UNIX
	for (i = 0; i < max_bc_clients; i++)
	{
		if (QueueGetStatus(pQueue,pQueue->Head)[i] == '1')
		{
			dbg(DEBUG,"CheckQueueStatus: Thread with Queue id = %d didn\'t sent item %d with BcNum = %s...",i,pQueue->Head,QueueGetBcNum(pQueue,pQueue->Head));
			
			mutex_lock(&argThreadMngmnt[i].mutex);
			if (argThreadMngmnt[i].ThreadID == NULL)
			{
				dbg(TRACE,"CheckQueueStatus: Thread with Queue id = %d didn\'t sent item %d with BcNum = %s...",i,pQueue->Head,QueueGetBcNum(pQueue,pQueue->Head));
				for (j = 0; j < max_bc_buffers; j++)
				{
					QueueSetStatus(pQueue,j,i,'0');						
				}
			}
			else
			{
				if (bpCleanup)
				{
					dbg(TRACE,"Cleaning up Thread with Queue id = %d,Thread id = %d,socket id = %d,client IP address = %s didn\'t sent ...",i,argThreadMngmnt[i].ThreadID,argThreadMngmnt[i].Socket,argThreadMngmnt[i].ClientIpAddr);
			
					argThreadMngmnt[i].Exit = TRUE;
					
					if (argThreadMngmnt[i].Socket != -1)
					{
						/* SocketShutdown socket connection	*/
						dbg(TRACE,"CheckQueueStatus: Shutdown socket %d (connection with client %s)", argThreadMngmnt[i].Socket, argThreadMngmnt[i].ClientIpAddr);
						SocketShutdown(argThreadMngmnt[i].Socket,SHUT_RDWR);
			
						/* closing open sockets */
						dbg(TRACE,"CheckQueueStatus: Closing socket %d (connection with client %s)", argThreadMngmnt[i].Socket, argThreadMngmnt[i].ClientIpAddr);
						close(argThreadMngmnt[i].Socket);
					}
					
					for (j = 0; j < max_bc_buffers; j++)
					{
						QueueSetStatus(pQueue,j,i,'0');						
					}
				}								
				else
				{
					dbg(DEBUG,"CheckQueueStatus:Thread with Queue id = %d,Thread id = %d,socket id = %d,client IP address = %s didn\'t sent item %d with BcNum = %s...",i,argThreadMngmnt[i].ThreadID,argThreadMngmnt[i].Socket,argThreadMngmnt[i].ClientIpAddr,pQueue->Head,QueueGetBcNum(pQueue,pQueue->Head));
				}
			}
		
			mutex_unlock(&argThreadMngmnt[i].mutex);
									
		}		
	}											
#else			
	for (i = 0; i < max_bc_clients; i++)
	{
		if (QueueGetStatus(pQueue,pQueue->Head)[i] == '1')
		{
			dbg(DEBUG,"CheckQueueStatus: Thread with Queue id = %d didn\'t sent item %d with BcNum = %s...",i,pQueue->Head,QueueGetBcNum(pQueue,pQueue->Head));
			
			pthread_mutex_lock(&argThreadMngmnt[i].mutex);
			if (argThreadMngmnt[i].ThreadID == NULL)
			{
				dbg(TRACE,"CheckQueueStatus: Thread with Queue id = %d didn\'t sent item %d with BcNum = %s...",i,pQueue->Head,QueueGetBcNum(pQueue,pQueue->Head));
				for (j = 0; j < max_bc_buffers; j++)
				{
					QueueSetStatus(pQueue,j,i,'0');						
				}
			}
			else
			{
				if (bpCleanup)
				{
					dbg(TRACE,"Cleaning up Thread with Queue id = %d,Thread id = %d,socket id = %d,client IP address = %s didn\'t sent ...",i,argThreadMngmnt[i].ThreadID,argThreadMngmnt[i].Socket,argThreadMngmnt[i].ClientIpAddr);
					
					argThreadMngmnt[i].Exit = TRUE;

					if (argThreadMngmnt[i].Socket != -1)
					{
						/* SocketShutdown socket connection	*/
						dbg(TRACE,"CheckQueueStatus: Shutdown socket %d (connection with client %s)", argThreadMngmnt[i].Socket, argThreadMngmnt[i].ClientIpAddr);
						SocketShutdown(argThreadMngmnt[i].Socket,SHUT_RDWR);
			
						/* closing open sockets */
						dbg(TRACE,"CheckQueueStatus: Closing socket %d (connection with client %s)", argThreadMngmnt[i].Socket, argThreadMngmnt[i].ClientIpAddr);
						close(argThreadMngmnt[i].Socket);
					}
					
					for (j = 0; j < max_bc_buffers; j++)
					{
						QueueSetStatus(pQueue,j,i,'0');						
					}
				}								
				else
				{
					dbg(DEBUG,"CheckQueueStatus:Thread with Queue id = %d,Thread id = %d,socket id = %d,client IP address = %s didn\'t sent item %d with BcNum = %s...",i,argThreadMngmnt[i].ThreadID,argThreadMngmnt[i].Socket,argThreadMngmnt[i].ClientIpAddr,pQueue->Head,QueueGetBcNum(pQueue,pQueue->Head));
				}
			}
		
			pthread_mutex_unlock(&argThreadMngmnt[i].mutex);
									
		}		
	}											
#endif

	if (bpCleanup)
	{
		if (strstr(pQueue->BcBuffer[pQueue->Head].Status,"1") == NULL)
		{
			dbg(TRACE,"CheckQueueStatus: checks successfully completed, updating position ...");
			return QueueUpdatePosition(pQueue);
		}
		else
		{
			dbg(TRACE,"CheckQueueStatus: checks not successfull!");
			return RC_FAIL;
		}
	}
	else
	{
		dbg(DEBUG,"CheckQueueStatus: checks successfully completed ...");
		return RC_SUCCESS;					
	}
}

