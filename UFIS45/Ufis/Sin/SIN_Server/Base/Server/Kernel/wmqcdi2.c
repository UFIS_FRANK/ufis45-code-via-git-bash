#ifndef _DEF_mks_version
#define _DEF_mks_version
#include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/Sin/SIN_Server/Base/Server/Kernel/wmqcdi2.c 1.1 2008/05/15 16:06:26SGT jku Exp  $";
#endif /* _DEF_mks_version */
/*****************************************************************************************/
/*                                                                                       */
/* ABB ACE/FC Program Skeleton                                                           */
/*                                                                                       */
/* Author         : Mei                                                                  */
/* Date           : 07 Jun 2007                                                          */
/* Description    :                                                                      */
/*                                                                                       */
/* Update history :                                                                      */
/* 18 Sep 2007    : 1. To prevent zombie CHILD, config of NTISCH has to be added for     */
/*                     timer chk to PARENT                                               */
/*                  2. To prevent orphan child(parent die before child), CHILD will only */
/*                     blocked at MQ for x seconds, followed by checking on status of    */
/*                     PARENT                                                            */
/*****************************************************************************************/
/*                                                                                       */
/* source-code-control-system version string                                             */

static char sccs_version[] ="@(#) UFIS45 (c) ABB AAT/I wmqcdi2.c 45.3 / 07.06.2007 HEB";


/* be carefule with strftime or similar functions !!!                                    */
/*                                                                                       */
/*****************************************************************************************/
/* This program is a MIKE main program */


#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "fditools.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "rmschkrul.h"
#include "syslib.h"
#include "db_if.h"
#include "urno_fn.inc"
#include <time.h>
#include <cedatime.h>
/* includes for WebsphereMQ  */
#include <cmqc.h>

#ifdef _HPUX_SOURCE
extern int daylight;
extern long timezone;
extern char *tzname[2];
#else
extern int _daylight;
extern long _timezone;
extern char *_tzname[2];
#endif

/* Mei */
#define MAX_SEND_QUEUES 20
#define MQPKTLEN         30001
#define MAX_NUM_RETRIES  1000
#define HYBERNATE_TIMING  900
#define MQTAB_DATALEN    4000  /* Change to 4000 when production */
#define COMM_RECV        1   
#define COMM_SEND        2    
#define COMM_BOTH        3  
#define ROLE_RECEIVER    8    
#define ROLE_SENDER      -8
#define PKT_HEAD         1
#define PKT_TAIL         999999999

int debug_level = 0;
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static EVENT *prgOutEvent  = NULL;


static long lgEvtCnt = 0;
static int igSendCedaFail = FALSE;
static int igSendPrio = 0;
static int igCommType;
static int igConnected = FALSE;
static int igQueueCount = 0;
static int igLogging = FALSE;   
static int igRecovery = FALSE;   
static int igPlayRole;
static int igWaitInterval;
static int igSendUrno = FALSE;
static time_t tgCleanMr2 = 0;
static time_t tgCleanMs2 = 0;
static long lgCount = -1;

static char cgConfigFile[512] = "\0";
static char cgHopo[8] = "\0";                        /* default home airport    */
static char cgTabEnd[8] ="\0";                       /* default table extension */
static char cgUseAlerter[100] = "\0";
static char cgSelection[200] = "\0";
static char cgNameAlert[1024] = "\0";
static char cgContextAlert[2048] = "\0";
static char cgMQServer[100] = "\0";
static char cgQMgrReceive[50] = "\0";
static char cgQMgrSend[50] = "\0";
static char cgMqReceive[50] = "\0";
static char cgMqSend[1024] = "\0";
static char cgMqSendPrio[110] = "\0";
static char cgCedaReceive[10] = "\0";
static char cgCedaReceiveCommand[10] = "\0";
static char cgCedaSend[10] = "\0";
static char cgCedaSendCommand[10] = "\0";
static char cgSimulation[10] = "\0";
static char cgInterval[10] = "\0";
static char cgReconnectMq[10] = "\0";
static char cgFirstFailedUrno[20] = "\0";
static char cgFirstFailedData[4002] = "\0";
static char cgUseQName[10] = "\0";
static char igUseQName = FALSE;
static char cgWaitInterval[410] = "\0";

typedef struct
{
    MQHOBJ  HobjSend;
    char    cgQName[50];
    int     igQPrio;
    int     igQDynPrio;
    int     bgQConnected;
    int     igPutFailed;
    int     igPutFailedOld;
    char    cgFirstFailedData[20];
    char    cgFirstFailedUrno[20];
    char    cgUseAlerter[10];
    char    cgNameAlert[50];
    char    cgContextAlert[100];
    char    cgWaitInterval[20];
} _MyQueues;

_MyQueues rgMyQueues[20];

#define DATALEN 4004
#define THREE_FOUR 12
#define FIVE_FOUR  20
#define SIX_FOUR   24

typedef struct 
{
    char COUN[THREE_FOUR];
    char DATA[DATALEN];
    char INAM[THREE_FOUR];
    char PNUM[SIX_FOUR];
    char PSEQ[THREE_FOUR];
    char STAT[THREE_FOUR];
    char TPUT[FIVE_FOUR];
    char URNO[THREE_FOUR];

} MQDBLOG;

pid_t gChildPid;
pid_t gParentPid;

/*Handles for Sending*/
MQHCONN  HconSend;                   /* connection handle             */
MQHOBJ   HobjSend;                   /* object handle                 */

/*Handles for Receiving */
MQHCONN  HconRecBrowse;              /* connection handle             */
MQHCONN  HconRecGet;                 /* connection handle             */
MQHOBJ   HobjRecBrowse;              /* object handle                 */
MQHOBJ   HobjRecGet;

/******************************************************************************/
/* Function prototypes                                                        */
/******************************************************************************/
static int Init_Process();
static int ReadConfigEntry(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer, char *pcpDefault);


static int Reset(void);                       /* Reset program          */
static void Terminate(int ipSleep);            /* Terminate program      */
static void HandleSignal(int);                 /* Handles signals        */
static void HandleErr(int);                    /* Handles general errors */
static void HandleQueErr(int);                 /* Handles queuing errors */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/

static int HandleData(EVENT *prpEvent);       /* Handles event data     */
static int ConnectMQ(char *pcpQManager, MQHCONN *Hcon);
static int ReceiveFromMQ();
static int SendToCeda( char *pcpBuffer, char *pcpBegUrno, char *pcpEndUrno, int ipCount ); 
static int SendToMq( char *pcpBuffer, char *pcpBegUrno, char *pcpEndUrno, int ipCount, int ipQnum ); 
static int SendData( int ipRole, char *pcpData, char *pcpBeginUrno, char *pcpEndUrno, int ipCount, int ipQnum );
static int TimeToStr(char *pcpTime,time_t lpTime);
static int DoDataRecovery( int ipRole, int ipQnum );
static int LogMsg( char *pcpData, char *pcpMQtime, char *pcpBeginUrno, char *pcpEndUrno, 
                   char *pcpStatus, int ipRole, int ipMsglen );
static int OpenSendMQ(int ipQueueNumber);
void myinitialize(int argc,char *argv[]);
static int MarkRecord( char *pcpTabName, char *pcpBeginUrno, char *pcpEndUrno, char *pcpStatus, int ipCount );
void CleanDb();
static int GetQueueNumber(char *pclQueueName);
static int isParentAlive();
/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
    int ilRC = RC_SUCCESS;          /* Return code          */
    int ilCnt = 0;
    int ilOldDebugLevel = 0;
    int ili = 0;
    int ilChildStatus;


    /* INITIALIZE; */
    myinitialize(argc,argv);

    /* Initial debug statement shall be log here */
    debug_level = DEBUG;
    sprintf(__CedaLogFile,"%s/%s%.5ld.log",getenv("DBG_PATH"),mod_name,getpid());
    outp = fopen(__CedaLogFile,"w");


    (void)SetSignals(HandleSignal);

    dbg(TRACE,"MAIN: version <%s>",sccs_version);


    sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
    dbg(TRACE,"Binary_File <%s>",cgConfigFile);
    ilRC = TransferFile(cgConfigFile);
    if(ilRC != RC_SUCCESS)
        dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
 
    sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
    dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
    ilRC = TransferFile(cgConfigFile);
    if(ilRC != RC_SUCCESS)
        dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);

    ilRC = SendRemoteShutdown(mod_id);
    if(ilRC != RC_SUCCESS)
        dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);

    ilRC = Init_Process();

 
    /* Check whether fork is required */
    if(strlen(cgQMgrReceive) > 0 )
    {
        igCommType = COMM_RECV;
        if( strlen(cgQMgrSend) > 0 )
            igCommType = COMM_BOTH;
    }
    else
    {
        if( strlen(cgQMgrSend) <= 0 )
        {
            dbg( TRACE, "MAIN: Error in Q setup, no info for send/recv. EXIT!!" );
            sleep( 2 );
            exit( 1 );
        }
        igCommType = COMM_SEND;
    }

    if( igCommType == COMM_BOTH )
        dbg( TRACE, "This is a BI-DIRECTIONAL MQ process" );
    else if( igCommType == COMM_RECV )
        dbg( TRACE, "This is a one-directional RECEIVING MQ process" );
    else if( igCommType == COMM_SEND )
        dbg( TRACE, "This is a one-directional SENDING MQ process" );

    ilCnt = 0;
    gChildPid = fork();
    while(gChildPid == -1)
    {
        ilCnt++;
        if( ilCnt > 10 )
        {
            dbg( TRACE, "MAIN: Exceed 10 tries in fork() process EXIT!" );
            exit( 1 );
        }
        dbg(TRACE,"********* fork failed, waiting 60 Seconds **********");
        sleep( 60 );
        gChildPid = fork();
     }
     fclose(outp);
     if(gChildPid == 0)
        sprintf(__CedaLogFile,"%s/%sc%.5ld.log",getenv("DBG_PATH"),mod_name,getpid());
     else
        sprintf(__CedaLogFile,"%s/%sp%.5ld.log",getenv("DBG_PATH"),mod_name,getpid());
     outp = fopen(__CedaLogFile,"w");

    /* Attach to the MIKE queues */
    ilCnt = 0;
    do
    {
        ilRC = init_que();
        if(ilRC != RC_SUCCESS)
        {
            dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        }/* end of if */
    }while((ilCnt < 10) && (ilRC != RC_SUCCESS));


    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
        sleep(60);
        exit(1);
    }
    else
        dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);

    ilCnt = 0;
    do
    {
        ilRC = init_db();
        if (ilRC != RC_SUCCESS)
        {
            check_ret(ilRC);
            dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        } /* end of if */
    } while((ilCnt < 10) && (ilRC != RC_SUCCESS));


    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
        sleep(60);
        exit(2);
    }
    else
        dbg(TRACE,"MAIN: init_db() OK!");
    

    dbg(TRACE,"=====================");
    dbg(TRACE,"MAIN: initializing OK");
    dbg(TRACE,"=====================");
    

    if (gChildPid == 0)
    {
        dbg(TRACE,"******* I am the child (RECEIVER) *******");
        gParentPid = getppid();
        dbg(TRACE,"******* My Parent PID = %d *******", gParentPid);
        igPlayRole = ROLE_RECEIVER;
    }
    else
    {
        dbg(TRACE,"******* I am the parent (SENDER) *******");
        igPlayRole = ROLE_SENDER;
    }

    if( igPlayRole == ROLE_SENDER && ((igCommType & COMM_SEND) > 0) ) 
    {
        dbg(TRACE,"%05d: connect for sending to MQ",__LINE__);
        if( igConnected == FALSE)
        {
            ilRC = ConnectMQ(cgQMgrSend, &HconSend);
            for(ili = 0 ; ili < igQueueCount ; ili++)
                ilRC = OpenSendMQ(ili); 
        }
        else
            dbg(TRACE,"%05d: Send-Qmanager already connected",__LINE__);
    }

    dbg(TRACE," PID: <%d>",getpid());


    if( igPlayRole == ROLE_RECEIVER )
    {
        if( !(igCommType & COMM_RECV) )
        {
            dbg( TRACE, "%05d: dun need RECEIVER! TERMINATE", __LINE__ );
            Terminate(1);
        }

        if( igRecovery == TRUE )
        {
            if( DoDataRecovery(ROLE_RECEIVER, -1) == RC_FAIL )
                Terminate(5);
        }
        ReceiveFromMQ();  /* Loop */
        Terminate(1);
    }
    else
    {
        if( igRecovery == TRUE )
        {
            for (ili = 0 ; ili < 20 ; ili++)
            {
                if(strlen(rgMyQueues[ili].cgQName) > 0)
                {
                    if( DoDataRecovery(ROLE_SENDER, ili) == RC_FAIL )
                        Terminate( 5 );
                }
            }
        }
    }

    if( igPlayRole == ROLE_SENDER )
    {
        for(;;)
        {
            /* the child should not listen to this CEDA-queue*/
            ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
            /* depending on the size of the received item  */
            /* a realloc could be made by the que function */
            /* so do never forget to set event pointer !!! */
            prgEvent = (EVENT *) prgItem->text;
                
            if( ilRC != RC_SUCCESS )
            {
                /* Handle queuing errors */
                HandleQueErr(ilRC);
            }
            else
            {
                /* Acknowledge the item */
                ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
                if( ilRC != RC_SUCCESS ) 
                {
                    /* handle que_ack error */
                    HandleQueErr(ilRC);
                } /* fi */

                lgEvtCnt++;
    
                switch( prgEvent->command )
                {
                    case REMOTE_DB :
                        /* ctrl_sta is checked inside */
                        HandleRemoteDB(prgEvent);
                        break;
                    case SHUTDOWN :
                        /* process shutdown - maybe from uutil */
                        Terminate(1);
                        break;
                    case RESET :
                        ilRC = Reset();
                        break;
                    case EVENT_DATA :
                        ilRC = HandleData(prgEvent);
                        if(ilRC != RC_SUCCESS)
                        {
                            HandleErr(ilRC);
                        }/* end of if */
                        break;
                    case TRACE_ON :
                        dbg_handle_debug(prgEvent->command);
                        break;
                    case TRACE_OFF :
                        dbg_handle_debug(prgEvent->command);
                        break;
                    default :
                        dbg(TRACE,"MAIN: unknown event");
                        DebugPrintItem(TRACE,prgItem);
                        DebugPrintEvent(TRACE,prgEvent);
                        break;
                } /* end switch */
            }
            /* Only check if there are process required for MQ reading */
            if( gChildPid <= 0 )
                continue;
            dbg( TRACE, "%d: Chk child status", __LINE__ );
            ilRC = waitpid( gChildPid, &ilChildStatus, WNOHANG );
            if( ilRC == gChildPid || ilRC == -1 )
            {
                dbg( TRACE, "MAIN: Child is dead!" );
                if( (igCommType & COMM_RECV) > 0 ) 
                {
                    dbg( TRACE, "MAIN: Parent EXIT!" );
                    Terminate(1);
                }
                else
                    dbg( TRACE, "MAIN: Is ok, since is SENDING Q" );
                gChildPid = 0;
            }
        } /* end for */
    } /* ROLE_SENDER */
} /* end of MAIN */

static int ReadConfigEntry(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer, char *pcpDefault)
{
    int ilRC = RC_SUCCESS;
    char pclSection[124] = "\0";
    char pclKeyword[124] = "\0";

    strcpy(pclSection,pcpSection);
    strcpy(pclKeyword,pcpKeyword);

    ilRC = iGetConfigEntry(cgConfigFile,pclSection,pclKeyword,CFG_STRING,pcpCfgBuffer);
    if(ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"Not found in %s: [%s] <%s>",cgConfigFile,pclSection,pclKeyword);
        dbg(TRACE,"use default-value: <%s>",pcpDefault);
        strcpy(pcpCfgBuffer,pcpDefault);
    } 
    else
        dbg(DEBUG,"Config Entry [%s],<%s>:<%s> found in %s", pclSection, pclKeyword ,pcpCfgBuffer, cgConfigFile);
    return ilRC;
}


/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/

static int Init_Process()
{
    int  ilRC = RC_SUCCESS;         /* Return code */
    int ili = 0;
    int blMoreQueues = TRUE;
    char pclTemp[100] = "\0";
    char *pclFunc = "Init_Process";

    debug_level = DEBUG;

    /* read HomeAirPort from SGS.TAB */
    ilRC = tool_search_exco_data("SYS", "HOMEAP", cgHopo);
    if (ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"<%s> EXTAB,SYS,HOMEAP not found in SGS.TAB", pclFunc);
        Terminate(30);
    } 
    else
        dbg(TRACE,"<%s> home airport <%s>",pclFunc, cgHopo);

    
    ilRC = tool_search_exco_data("ALL","TABEND", cgTabEnd);
    if (ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"<%s> EXTAB,ALL,TABEND not found in SGS.TAB", pclFunc);
        Terminate(30);
    } 
    else
        dbg(TRACE,"<%s> table extension <%s>",pclFunc, cgTabEnd);

    /*** Mei ***/
    igLogging = FALSE;
    ilRC = ReadConfigEntry("MAIN","DB_LOGGING",pclTemp,"ON");
    if( !strncmp(pclTemp, "ON", 2) )
    {
        igLogging = TRUE;
        dbg( TRACE, "<%s> LOG Data", pclFunc);
    }
    debug_level = TRACE;
    ilRC = ReadConfigEntry("MAIN","DEBUG_LEVEL",pclTemp,"TRACE");
    if( !strncmp( pclTemp, "DEBUG", 5 ) )
    {
        debug_level = DEBUG;
        dbg( TRACE, "<%s> DEBUG level", pclFunc );
    }

    igSendUrno  = FALSE;
    ilRC = ReadConfigEntry("MAIN","SEND_URNO_TO_CEDA",pclTemp,"FALSE");
    if( !strncmp(pclTemp,"TRUE", 4) )
    {
        igSendUrno = TRUE;
        dbg( TRACE, "<%s> Send CEDA Process URNO", pclFunc );
    }   
    else
        dbg( TRACE, "<%s> Send CEDA Process FULL DATA", pclFunc );

    igRecovery = FALSE;
    ilRC = ReadConfigEntry("MAIN","DATA_RECOVERY",pclTemp,"ON");
    if( !strncmp( pclTemp, "ON", 2 ) )
    {
        igRecovery = TRUE;
        dbg( TRACE, "<%s> RECOVERY ON", pclFunc );
    }
    
    if( igLogging == FALSE )
    {
        igRecovery = FALSE;
        dbg(TRACE,"%05d: RECOVERY switched to <OFF> because DB-logging is <OFF>",__LINE__);
    }

    ilRC = ReadConfigEntry("MAIN","SELECTION",cgSelection,"");
    ilRC = ReadConfigEntry("MAIN","USE_QUEUENAME_FOR_INAM",cgUseQName,"NO");
    ilRC = ReadConfigEntry("MAIN","USE_ALERTER",cgUseAlerter,"YES");
    ilRC = ReadConfigEntry("MAIN","NAME_SEND_TO_ALERTER",cgNameAlert,"");
    ilRC = ReadConfigEntry("MAIN","CONTEXT_FOR_ALERT",cgContextAlert,"");
    ilRC = ReadConfigEntry("MAIN","WAIT_INTERVAL",cgWaitInterval,"60000"); igWaitInterval = atoi(cgWaitInterval);
    ilRC = ReadConfigEntry("QUEUE","MQSERVER",cgMQServer,"");
    ilRC = ReadConfigEntry("QUEUE","MQ_MGR_RECEIVE",cgQMgrReceive,"");
    ilRC = ReadConfigEntry("QUEUE","MQ_MGR_SEND",cgQMgrSend,"");
    ilRC = ReadConfigEntry("QUEUE","MQ_RECEIVE",cgMqReceive,"");
    ilRC = ReadConfigEntry("QUEUE","MQ_SEND",cgMqSend,"");
    ilRC = ReadConfigEntry("QUEUE","MQ_SEND_PRIO",cgMqSendPrio,"0");
    ilRC = ReadConfigEntry("QUEUE","CEDA_RECEIVE_COMMAND",cgCedaReceiveCommand,"");
    ilRC = ReadConfigEntry("QUEUE","CEDA_SEND",cgCedaSend,"");
    ilRC = ReadConfigEntry("QUEUE","CEDA_SEND_COMMAND",cgCedaSendCommand,"");
    ilRC = ReadConfigEntry("RECONNECT","RECONNECT_MQ",cgReconnectMq,"10");
    
    igSendPrio = 5;
    ilRC = ReadConfigEntry("QUEUE","CEDA_SEND_PRIO",pclTemp,"5");
    if( ilRC == RC_SUCCESS )
        igSendPrio = atoi(pclTemp);
    dbg( TRACE, "<%s> Send Priority <%d>", pclFunc, igSendPrio );
 
    ilRC = ReadConfigEntry("CLEANUP","CLEAN_MQR",pclTemp,"30");
    if( ilRC == RC_SUCCESS )
        tgCleanMr2 = atoi(pclTemp);
    if( tgCleanMr2 <= 0 || tgCleanMr2 >= 365 )
        tgCleanMr2 = 10;
    dbg( TRACE, "<%s> Clean MR2 <%d> days", pclFunc, tgCleanMr2 );
    tgCleanMr2 *= SECONDS_PER_DAY;


    ilRC = ReadConfigEntry("CLEANUP","CLEAN_MQS",pclTemp,"30");
    if( ilRC == RC_SUCCESS )
        tgCleanMs2 = atoi(pclTemp);
    if( tgCleanMs2 <= 0 || tgCleanMs2 >= 365 )
        tgCleanMs2 = 10;
    dbg( TRACE, "<%s> Clean MS2 <%d> days", pclFunc, tgCleanMs2 );
    tgCleanMs2 *= SECONDS_PER_DAY;


    igUseQName = TRUE;
    if((strcmp(cgUseQName,"NO") == 0) && (strstr(cgMqSend,",") == NULL))
        igUseQName = FALSE;


    ili = 0;
    while(blMoreQueues == TRUE)
    {
        ilRC = get_item(ili+1,cgMqSend,rgMyQueues[ili].cgQName,0,",","\0","\0");
        if(ilRC == FALSE) blMoreQueues = FALSE;

        ilRC = get_item(ili+1,cgMqSendPrio,pclTemp,0,",","\0","\0");
        if(ilRC == FALSE) blMoreQueues = FALSE;

        rgMyQueues[ili].igQPrio = atoi(pclTemp);
        rgMyQueues[ili].igQDynPrio = -1;

        ilRC = get_item(ili+1,cgUseAlerter,rgMyQueues[ili].cgUseAlerter,0,",","\0","\0");
        if(ilRC == FALSE) blMoreQueues = FALSE;

        ilRC = get_item(ili+1,cgNameAlert,rgMyQueues[ili].cgNameAlert,0,",","\0","\0");
        if(ilRC == FALSE) blMoreQueues = FALSE;
            
        ilRC = get_item(ili+1,cgContextAlert,rgMyQueues[ili].cgContextAlert,0,",","\0","\0");
        if(ilRC == FALSE) blMoreQueues = FALSE;
            
        ilRC = get_item(ili+1,cgWaitInterval,rgMyQueues[ili].cgWaitInterval,0,",","\0","\0");
        if(ilRC == FALSE) blMoreQueues = FALSE;
            
        rgMyQueues[ili].bgQConnected = FALSE;
        rgMyQueues[ili].igPutFailed = FALSE;
        rgMyQueues[ili].igPutFailedOld = FALSE;
        rgMyQueues[ili].cgFirstFailedUrno[0] = '\0';
        rgMyQueues[ili].cgFirstFailedData[0] = '\0';
        ili++;
    }
    igQueueCount = ili -1;

    for(ili = 0 ; ili < igQueueCount ; ili++)
    {
        dbg(TRACE,"%05d: ......... QUEUE <%d> .......... found",__LINE__,ili);
        dbg(TRACE,"%05d: QName: <%s>",__LINE__,rgMyQueues[ili].cgQName);
        dbg(TRACE,"%05d: QPrio: <%d>",__LINE__,rgMyQueues[ili].igQPrio);
        dbg(TRACE,"%05d: QUseAlerter: <%s>",__LINE__,rgMyQueues[ili].cgUseAlerter);     
        dbg(TRACE,"%05d: QNameAlert: <%s>",__LINE__,rgMyQueues[ili].cgNameAlert);
        dbg(TRACE,"%05d: QContextAlert: <%s>",__LINE__,rgMyQueues[ili].cgContextAlert);
        dbg(TRACE,"%05d: QWaitInterval: <%s>",__LINE__,rgMyQueues[ili].cgWaitInterval);
        dbg(TRACE,"%05d: QConnected: <%d>",__LINE__,rgMyQueues[ili].bgQConnected);
        dbg(TRACE,"%05d: QFirstFailedUrno: <%s>",__LINE__,rgMyQueues[ili].cgFirstFailedUrno);
        dbg(TRACE,"%05d: QFirstFailedData: <%s>",__LINE__,rgMyQueues[ili].cgFirstFailedData);
    }

    if (ilRC != RC_SUCCESS)
        dbg(TRACE,"Init_Process failed");
    else
        dbg(DEBUG,"Init_Process successful");

    return(ilRC);
    
} /* end of initialize */




/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
    int ilRC = RC_SUCCESS;              /* Return code */
    
    dbg(TRACE,"Reset: now resetting");
    
    return ilRC;
    
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
    int ilRC = RC_SUCCESS;
    int ili = 0;

    MQLONG   C_options;              /* MQCLOSE options                 */
    MQLONG   CompCode;               /* completion code                 */
    MQLONG   Reason;                 /* reason code                     */

    C_options = MQCO_NONE;           /* no close options             */

    if(gChildPid == 0)   
    {
        MQCLOSE (HconRecBrowse, &HobjRecBrowse, C_options, &CompCode, &Reason);
        dbg(TRACE,"Terminate: Closed Browse Queue ended with Reasoncod: <%d>, CompletionCode: <%d>",Reason,CompCode);
        MQCLOSE (HconRecGet, &HobjRecGet, C_options, &CompCode, &Reason);
        dbg(TRACE,"Terminate: Closed Get Queue ended with Reasoncod: <%d>, CompletionCode: <%d>",Reason,CompCode);
        MQDISC(&HconRecBrowse,&CompCode,&Reason);
        dbg(TRACE,"Terminate: Closed Receive Browse connection ended with Reasoncod: <%d>, CompletionCode: <%d>",Reason,CompCode);
    
        /* MQDISC Disconnects from QueueManager  */
        /* So the second call is not necessary.  */
        /* MQDISC(&HconRecGet,&CompCode,&Reason); */
        /* dbg(TRACE,"Terminate: Closed Receive Get connection ended with Reasoncod: <%d>, CompletionCode: <%d>",Reason,CompCode);*/

    }
    else
    {
        if( !(igCommType & COMM_RECV) )
            return;
        for(ili = 0 ; ili < igQueueCount ; ili++)
        {
            MQCLOSE (HconSend, &rgMyQueues[ili].HobjSend, C_options, &CompCode, &Reason);   
        }

        MQDISC(&HconSend,&CompCode,&Reason);
        dbg(TRACE,"Terminate: Closed Send connection ended with Reasoncod: <%d>, CompletionCode: <%d>",Reason,CompCode);
        dbg(TRACE,"Due to SIGTERM Kill Child now");
    }

    if(gChildPid != 0)
        kill(gChildPid,SIGTERM);

    /* unset SIGCHLD ! DB-Child will terminate ! */

    dbg(TRACE,"Terminate: now DB logoff ...");
    
    logoff();

    dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);


    dbg(TRACE,"Terminate: now leaving ...");

    fclose(outp);

    sleep(ipSleep < 1 ? 1 : ipSleep);
    
    exit(0);
    
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/

static void HandleSignal(int pipSig)
{
    dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);

    switch(pipSig)
    {
        case SIGCHLD:
            /*dbg(TRACE,"HandleSignal:SIGHCLD received, Terminate now");
            Terminate(1);*/
            break;
        case SIGTERM:
            dbg(TRACE,"HandleSignal:SIGTERM received !");
            if(gChildPid != 0)   /* then its a parent */
            {
                dbg(TRACE,"Due to SIGTERM Kill Child now");
                kill(gChildPid,SIGTERM);
            }
            dbg(TRACE,"Due to SIGTERM Terminate now");
            Terminate(1);
            break;

        default :
            Terminate(1);
            break;
    } /* end of switch */
    /*exit(1);*/
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
    dbg(TRACE,"HandleErr: <%d> : unknown ERROR",pipErr);
    return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    switch(pipErr) {
    case    QUE_E_FUNC  :   /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :   /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;
    case    QUE_E_SEND  :   /* Error using msgsnd */
        dbg(TRACE,"<%d> : msgsnd failed",pipErr);
        break;
    case    QUE_E_GET   :   /* Error using msgrcv */
        dbg(TRACE,"%05d: <%d> : msgrcv failed",__LINE__,pipErr);
        Terminate(1);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX   :
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE  :
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES  :
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        break;
    case    QUE_E_RESP  :   /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;
    case    QUE_E_FULL  :
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG :   /* No message on queue */
        dbg(TRACE,"<%d> : no messages on queue",pipErr);
        break;
    case    QUE_E_INVORG    :   /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :   /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;
    default         :   /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
    } /* end switch */
         
    return;

} /* end of HandleQueErr */
    



/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(EVENT *prpEvent)
{
    int    ilRC           = RC_SUCCESS;         /* Return code */
    int      ilCmd          = 0;
    int     ilUpdPoolJob = TRUE;
    int     ilQueueNumber = 0;
    int     ili = 0;
    long    llQueueNameLen = 0;

    BC_HEAD *prlBchead       = NULL;
    CMDBLK  *prlCmdblk       = NULL;
    char    *pclSelection    = NULL;
    char    *pclFields       = NULL;
    char    *pclData         = NULL;
    char    *pclRow          = NULL;
    char    pclUrnoList[2400];
    char    pclTable[34];
    char    pclQueueName[50] = "\0";
    char    *pclKeyPtr = NULL;
    char    pclQDynPrio[10] = "\0";
/* char pclTmpStr[6000]; */

    prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
    prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
    pclSelection = prlCmdblk->data;
    pclFields    = pclSelection + strlen(pclSelection) + 1;
    pclData      = pclFields + strlen(pclFields) + 1;


    strcpy(pclTable,prlCmdblk->obj_name);

    dbg(TRACE,"========== START <%10.10d> ==========",lgEvtCnt);

    /****************************************/
    dbg(TRACE,"Command:   <%s>",prlCmdblk->command);
    dbg(TRACE,"tw_start:  <%s>",prlCmdblk->tw_start);
    dbg(TRACE,"tw_end:    <%s>",prlCmdblk->tw_end);
    dbg(TRACE,"originator <%d>",prpEvent->originator);
    dbg(TRACE,"selection  <%s>",pclSelection);
    dbg(TRACE,"fields     <%s>",pclFields);
    dbg(TRACE,"data       <%s>",pclData);
    /****************************************/

    dbg(TRACE,"%05d: prlCmdblk->command: <%s>, cgCedaReceiveCommand: <%s>",__LINE__,prlCmdblk->command,cgCedaReceiveCommand);
    if (strcmp(prlCmdblk->command,cgCedaReceiveCommand) == 0)
    {
        ilQueueNumber = 0; 
        if(strlen(pclFields) != 0)
        {
            pclKeyPtr = CedaGetKeyItem(pclQueueName,&llQueueNameLen,pclFields,"<QUEUE>","<\\QUEUE>", TRUE);
            if(strlen(pclQueueName) > 0)
            {
                ilQueueNumber = GetQueueNumber(pclQueueName);
                pclKeyPtr = CedaGetKeyItem(pclQDynPrio,&llQueueNameLen,pclFields,"<QPRIO>","<\\QPRIO>", TRUE);
                rgMyQueues[ilQueueNumber].igQDynPrio = -1;
                if(strlen(pclQDynPrio) > 0)
                    rgMyQueues[ilQueueNumber].igQDynPrio = atoi(pclQDynPrio);
            }
            dbg(TRACE,"%05d: QueueNumber: <%d>",__LINE__,ilQueueNumber);
        }


        if((strlen(pclData) >=0) && (ilQueueNumber >= 0))
        {
            if(igConnected == FALSE)
            {
                ilRC = ConnectMQ(cgQMgrSend, &HconSend);
                ilRC = OpenSendMQ(ilQueueNumber);
            }
            
            if( igLogging == TRUE )
            {
                if(rgMyQueues[ilQueueNumber].igPutFailed == TRUE && igRecovery == TRUE )
                {
                   ilRC = DoDataRecovery(ROLE_SENDER, ilQueueNumber);
                   if( ilRC == RC_FAIL )
                       Terminate( 5 );
                }
            }
/*sprintf( pclTmpStr, "%s-%s-%s-%s-%s-%s", pclData, pclData, pclData, pclData, pclData, pclData );
            ilRC = SendToMq( pclTmpStr, NULL, NULL, 1, ilQueueNumber ); */
            ilRC = SendToMq( pclData, NULL, NULL, 1, ilQueueNumber ); 
        }
    }

    if (strcmp(prlCmdblk->command,"SFM") == 0)
    {
        if( igLogging == TRUE && igRecovery == TRUE )
        {
            for (ili = 0 ; ili < 20 ; ili++)
            {
                if(strlen(rgMyQueues[ili].cgQName) > 0)
                {
                    if( DoDataRecovery(ROLE_SENDER, ili) == RC_FAIL )
                        Terminate( 5 );
                }
            }
        }
    }

    if (strcmp(prlCmdblk->command,"CDB") == 0)
    {
        if( igLogging == TRUE )
            CleanDb();
    }

    if (strcmp(prlCmdblk->command,"28674") == 0)
        Terminate(1);

    dbg(TRACE,"==========  END  <%10.10d> ==========",lgEvtCnt);

    return(RC_SUCCESS);
    
} /* end of HandleData */



static int ConnectMQ(char *pcpQManager, MQHCONN *Hcon)
{
    static int ilRC = RC_SUCCESS;
 
    MQLONG   CompCode;               /* completion code               */
    MQLONG   CReason;                /* reason code for MQCONN        */

    static char pclMyMQServer[100] = "\0";
    static char pclNowTime[20] = "\0";
    static char pclAlert[256] = "\0";

    /* SET ENVIRONMENT TO CONNECT TO THE RIGHT QMANAGER */
    sprintf(pclMyMQServer,"MQSERVER=%s",cgMQServer);
    ilRC=putenv(pclMyMQServer);
   
    /* CONNECT TO GIVEN QMANAGER */
    CompCode = MQCC_FAILED;
    
    MQCONN(pcpQManager,    /* queue manager                  */
           Hcon,           /* connection handle              */
           &CompCode,      /* completion code                */
           &CReason);      /* reason code                    */

   /* report reason and stop if it failed     */
   igConnected = FALSE;    
   if (CompCode == MQCC_FAILED)
   {
       dbg(TRACE,"%05d: MQCONN ended with reason code <%d>, now waiting <%s> seconds and try to reconnect",
                 __LINE__, CReason, cgReconnectMq);
       return RC_FAIL;
   }
   else
   {
       igConnected = TRUE;
       dbg(TRACE,"%05d: MQCONN connected with QManager <%s>",__LINE__, pcpQManager);
       dbg(TRACE,"%05d: on Server: <%s>",__LINE__,cgMQServer); 
       return RC_SUCCESS;
   }
}


static int ReceiveFromMQ()
{
    int ilRC = RC_SUCCESS;
    int ilMaxErrorTries = 0;
    int ili;
    MQLONG   OpenBrowseOptions;                 /* MQOPEN options                */
    MQLONG   QueueBrowseOpenCode;               /* MQOPEN completion code        */
    MQLONG   QueueBrowseCompCode;               /* MQOPEN completion code        */
    MQLONG   QueueBrowseReason;                 /* reason code                   */
    MQLONG   OpenOptions;                       /* MQOPEN options                */
    MQLONG   QueueOpenCode;                     /* MQOPEN completion code        */
    MQLONG   QueueCompCode;                     /* MQOPEN completion code        */
    MQLONG   QueueReason;                       /* reason code                   */
    MQLONG   RecBuffLen;                        /* buffer length                 */
    MQLONG   MessLen;                           /* message length received       */
    /*MQHOBJ   QueueBrowseHobj;                 *//* object handle                 */
    /*MQHOBJ   QueueHobj;                       *//* object handle                 */
    MQOD     QueueBrowseOd = {MQOD_DEFAULT};    /* Object Descriptor             */
    MQOD     QueueOd = {MQOD_DEFAULT};          /* Object Descriptor             */
    MQGMO    QueueGmo = {MQGMO_DEFAULT};        /* get message options           */
    MQGMO    QueueBrowseGmo = {MQGMO_DEFAULT};  /* get message options           */
    MQMD     ReceiveBrowseMd = {MQMD_DEFAULT};  /* Message Descriptor            */
    MQMD     ReceiveMd = {MQMD_DEFAULT};        /* Message Descriptor            */
    MQBYTE   RecBuffer[MQPKTLEN];               /* message buffer                */
    char pclMyBuffer[MQPKTLEN] = "\0";
    char pclBeginUrno[15] = "\0";
    char pclEndUrno[15] = "\0";
    char pclMyTime[20] = "\0";
    static char pclAlert[256] = "\0";
    char *pclFunc = "ReceiveFromMQ";


    /* CONNECT TO QMANAGER */
    ilMaxErrorTries = 0;
    while(igConnected == FALSE)
    {
        ilRC = ConnectMQ(cgQMgrReceive, &HconRecBrowse);
        if( ilRC == RC_SUCCESS )
            ilRC = ConnectMQ(cgQMgrReceive, &HconRecGet);
        if( ilRC == RC_SUCCESS )
            break;

        dbg(TRACE,"<%s> !!!!!!!!!! ERROR WHILE connecting to Qmanager !!!!!!!!!!!!!!!", pclFunc);
        ilMaxErrorTries++;
        if( ilMaxErrorTries >= MAX_NUM_RETRIES )
        {
            if(strcmp(cgUseAlerter,"YES") == 0)
            {
                sprintf(pclAlert,"<%s>: MQCONN failed while connecting to QManager <%s>",
                                cgContextAlert,cgQMgrReceive);
                ilRC = AddAlert2(cgNameAlert," "," ","E",pclAlert," ",TRUE,FALSE,"ALERT",mod_name," "," ");
            }
            dbg(TRACE,"<%s> Hybernating the process for %d seconds!!!!", pclFunc, HYBERNATE_TIMING );
            sleep(HYBERNATE_TIMING);
            ilMaxErrorTries = 0;
        }
        dbg(TRACE,"<%s> waiting <%s> seconds before try to reconnect",pclFunc,cgReconnectMq);
        sleep(atoi(cgReconnectMq));
    }

    
    /* OPEN QUEUE FOR BROWSING */
    OpenBrowseOptions = MQOO_INPUT_AS_Q_DEF           /* open queue for input      */
                        + MQOO_BROWSE                 /* open for browsing */
                        + MQOO_FAIL_IF_QUIESCING;     /* but not if MQM stopping   */

    strcpy(QueueBrowseOd.ObjectName,cgMqReceive);        
    
    QueueBrowseReason = 1111; /* != MQRC_NONE */
    QueueBrowseOpenCode = MQCC_FAILED;
    
    ilMaxErrorTries = 0;
    while ((QueueBrowseReason != MQRC_NONE) || (QueueBrowseOpenCode == MQCC_FAILED))
    {
        MQOPEN(HconRecBrowse,            /* connection handle            */
               &QueueBrowseOd,           /* object descriptor for queue  */
               OpenBrowseOptions,        /* open options                 */
               &HobjRecBrowse,           /* object handle                */
               &QueueBrowseOpenCode,     /* completion code              */
               &QueueBrowseReason);      /* reason code                  */

        if( QueueBrowseReason == MQRC_NONE && QueueBrowseOpenCode != MQCC_FAILED )
        {
            dbg(TRACE,"<%s> Queue <%s> opened for browsing",pclFunc,cgMqReceive);
            break;
        }
        dbg(TRACE,"<%s> !!!!!!!!!! ERROR WHILE opening queue !!!!!!!!!!!!!!!",pclFunc);

        if (QueueBrowseReason != MQRC_NONE)
            dbg(TRACE,"<%s> MQOPEN ended with reason code <%d>",pclFunc, QueueBrowseReason);
        if (QueueBrowseOpenCode == MQCC_FAILED)
            dbg(TRACE,"<%s> unable to open receive-queue",pclFunc);

        ilMaxErrorTries++;
        if( ilMaxErrorTries >= MAX_NUM_RETRIES )
        {
            if(strcmp(cgUseAlerter,"YES") == 0)
            {
                sprintf(pclAlert,"<%s>: MQOPEN ended with reason code <%d> while connecting to Queue <%s>",
                                cgContextAlert,QueueBrowseReason,cgMqReceive);
                ilRC = AddAlert2(cgNameAlert," "," ","E",pclAlert," ",TRUE,FALSE,"ALERT",mod_name," "," ");
            }

            dbg(TRACE,"<%s> Hybernating the process for %d seconds!!!!", pclFunc, HYBERNATE_TIMING );
            sleep(HYBERNATE_TIMING);
            ilMaxErrorTries = 0;
            dbg(TRACE,"<%s> EXIT!", pclFunc, HYBERNATE_TIMING );
            exit( -1 );
        }
        dbg(TRACE,"<%s> waiting <%s> seconds before try to reopen",pclFunc,cgReconnectMq);
        sleep(atoi(cgReconnectMq));
    } /* end of while (QueueBrowseReason.......*/

    /* OPEN QUEUE FOR Receiving with GET (and delete item on queue) */
    OpenOptions = MQOO_INPUT_AS_Q_DEF            /* open queue for input      */
                  +  MQOO_FAIL_IF_QUIESCING;     /* but not if MQM stopping   */

    strcpy(QueueOd.ObjectName,cgMqReceive);          
    
    QueueReason = 1111; /* != MQRC_NONE */
    QueueOpenCode = MQCC_FAILED;
    
    ilMaxErrorTries = 0;
    while ((QueueReason != MQRC_NONE) || (QueueOpenCode == MQCC_FAILED))
    {
        MQOPEN(HconRecGet,              /* connection handle            */
               &QueueOd,                /* object descriptor for queue  */
               OpenOptions,             /* open options                 */
               &HobjRecGet,              /* object handle                */
               &QueueOpenCode,          /* completion code              */
               &QueueReason);           /* reason code                  */

        if (QueueReason == MQRC_NONE && QueueOpenCode != MQCC_FAILED)
        {
            dbg(TRACE,"<%s> Queue <%s> is opened for receive and delete item",pclFunc,cgMqReceive);
            break;
        }

        dbg(TRACE,"<%s> !!!!!!!!!! ERROR WHILE opening queue !!!!!!!!!!!!!!!",pclFunc);

        if (QueueReason != MQRC_NONE)
            dbg(TRACE,"<%s> MQOPEN ended with reason code <%d>",pclFunc, QueueReason);
        if (QueueOpenCode == MQCC_FAILED)
            dbg(TRACE,"<%s> unable to open receive-queue",pclFunc);

        ilMaxErrorTries++;
        if( ilMaxErrorTries >= MAX_NUM_RETRIES )
        {
            if(strcmp(cgUseAlerter,"YES") == 0)
            {
                sprintf(pclAlert,"<%s>: MQOPEN ended with reason code <%d> while connecting to Queue <%s>",
                                cgContextAlert,QueueReason,cgMqReceive);
                ilRC = AddAlert2(cgNameAlert," "," ","E",pclAlert," ",TRUE,FALSE,"ALERT",mod_name," "," ");
            }
            dbg(TRACE,"<%s> Hybernating the process for %d seconds!!!!", pclFunc, HYBERNATE_TIMING );
            sleep(HYBERNATE_TIMING);
            ilMaxErrorTries = 0;
        }
        dbg(TRACE,"<%s> waiting <%s> seconds before try to reopen",pclFunc,cgReconnectMq);
        sleep(atoi(cgReconnectMq));
    } /* end of while (QueueReason.......*/


    /* Main program logic */
    /* NOW LOOP FOR RECEIVING AND SENDING THE DATA FROM THE MQ-QUEUE */
    for(;;)
    {

        /* NOW GET FIRST ITEM ON QUEUE WITH BROWSE, SO THE ITEM WILL NOT BE DELETED FROM THE QUEUE */
        dbg(TRACE,"<%s> =================Wait for next message on Queue <%s>==============",pclFunc,cgMqReceive);

        QueueBrowseGmo.Options = MQGMO_WAIT
                                 + MQGMO_FAIL_IF_QUIESCING
                                 + MQGMO_BROWSE_FIRST
                                 + MQGMO_ACCEPT_TRUNCATED_MSG;

        /*QueueBrowseGmo.WaitInterval = MQWI_UNLIMITED;*/       /* no limit for waiting     */
        QueueBrowseGmo.WaitInterval = igWaitInterval;
        if(igWaitInterval == 0)
        {
            dbg(TRACE,"<%s> MQGET-WaitInterval = 0 ----> set WaitInterval to 60000", pclFunc);
            QueueBrowseGmo.WaitInterval = 60000;
        }
        dbg(TRACE,"<%s> MQGET-WaitInterval = <%d>",pclFunc, QueueBrowseGmo.WaitInterval );

        RecBuffLen = sizeof(RecBuffer) - 1;
        memcpy(ReceiveBrowseMd.MsgId, MQMI_NONE, sizeof(ReceiveBrowseMd.MsgId));
        memcpy(ReceiveBrowseMd.CorrelId, MQCI_NONE, sizeof(ReceiveBrowseMd.CorrelId));
    
        do
        {
            do 
            {
                 MQGET(HconRecBrowse,         /* connection handle                 */
                       HobjRecBrowse,         /* object handle                     */
                       &ReceiveBrowseMd,      /* message descriptor                */
                       &QueueBrowseGmo,       /* get message options               */
                       RecBuffLen,            /* buffer length                     */
                       RecBuffer,             /* message buffer                    */
                       &MessLen,              /* message length                    */
                       &QueueBrowseCompCode,  /* completion code                   */
                       &QueueBrowseReason);   /* reason code                       */

                 if(QueueBrowseReason == 2033) /* MQRC_NO_MSG_AVAILABLE */
                 {
                     /*dbg(DEBUG,"<%s> No msg. Check parent status",pclFunc );*/
                     if( isParentAlive() == FALSE )
                         Terminate(3);
                 }
            } while (QueueBrowseReason == 2033); /*timout-error*/

            if( QueueBrowseReason != MQRC_NONE )
            {
                dbg(TRACE,"%05d: !!!!!!!!!! ERROR WHILE RECEIVING MESSAGE !!!!!!!!!!!!!!!",__LINE__);
                dbg(TRACE,"<%s> MQGET ended with Reasoncode: <%d>",pclFunc,QueueBrowseReason);
            }

            switch(QueueBrowseReason)
            {
                case (MQRC_NONE):
                    dbg(TRACE,"<%s> MQGET (browse) Size of Msg <%d>",pclFunc, MessLen );
                    strcpy(pclMyTime,ReceiveBrowseMd.PutDate);
                    pclMyTime[16] = '\0';

                    /* Replace '\0' in the message by ' ' because the message will be handled as string in further processing */
                    for(ili = 0; ili < MessLen; ili++)
                    {
                        if (RecBuffer[ili] == '\0')
                            RecBuffer[ili] = ' ';
                    }
                    RecBuffer[MessLen] = '\0';
                    dbg(TRACE,"<%s> MQGET (browse) received Message: <%s> Time: <%s>, Reason: <%d>",pclFunc,
                                    RecBuffer,pclMyTime,QueueBrowseReason);

                    sprintf(pclMyBuffer,"%s",RecBuffer);

                    if( atoi(cgCedaSend) != 0 )
                    {
                        if( igSendCedaFail == TRUE && igRecovery == TRUE )
                        {
                            dbg( TRACE, "<%s> Previous sending fail. Do data recovery", pclFunc );
                            if( DoDataRecovery( ROLE_RECEIVER, -1 ) == RC_FAIL )
                                Terminate( 5 );
                        }
        
                        if( igLogging == TRUE )
                        {
                            /* INSERT MESSAGE INTO MR2TAB */
                            LogMsg( pclMyBuffer, pclMyTime, pclBeginUrno, pclEndUrno, "RCV", ROLE_RECEIVER, MessLen );
                        }
                        dbg( TRACE, "<%s> URNO range <%s> - <%s>", pclFunc, pclBeginUrno, pclEndUrno );
                        ilRC = SendToCeda(pclMyBuffer,pclBeginUrno,pclEndUrno,1);
                        dbg( TRACE, "<%s> ilRC for SendCeda <%d>", pclFunc, ilRC );
                    }
                    break;
                

                case (2009): /* MQRC_CONNECTION_BROKEN */
                    dbg( TRACE, "(MQGET) CONNECTION IS BROKEN!!! TERMINATE!!!" );
                    Terminate(10);

                case (2079): /* MQRC_TRUNCATED_MSG_ACCEPTED */
                    dbg(TRACE,"%05d: Msg was truncated. Remove msg",__LINE__ );

                    /* INSERT MESSAGE INTO MR2TAB */
                    strncpy(pclMyBuffer, RecBuffer, RecBuffLen);
                    LogMsg( pclMyBuffer, pclMyTime, pclBeginUrno, pclEndUrno, "TRUN", ROLE_RECEIVER, MessLen );
                    break;

                case (2016): /* MQRC_GET_INHIBITED */
                    ilMaxErrorTries++;
                    if( ilMaxErrorTries >= MAX_NUM_RETRIES )
                    {
                        if(strcmp(cgUseAlerter,"YES") == 0)
                        {
                            sprintf(pclAlert,"<%s>: MQGET ended with Reasoncode: <%d> ",cgContextAlert,QueueBrowseReason);
                            ilRC = AddAlert2(cgNameAlert," "," ","E",pclAlert," ",TRUE,FALSE,"ALERT",mod_name," "," ");
                        }
                        dbg(TRACE,"<%s> Hybernating the process for %d seconds!!!!", pclFunc, HYBERNATE_TIMING );
                        sleep(HYBERNATE_TIMING);
                        ilMaxErrorTries = 0;
                    }
                    dbg(TRACE,"<%s> waiting 120 seconds for retry",pclFunc);
                    sleep(120);
                    break;
                    
                default:
                    dbg(TRACE,"%05d: MAJOR ERROR GOING TO TERMINATE",__LINE__ );
                    Terminate(10);
            } /* switch */

        } while (QueueBrowseReason != MQRC_NONE && QueueBrowseReason != 2079);

    
        /* NOW GET FIRST ITEM ON QUEUE AND DELETE IT FROM THE QUEUE */
        QueueGmo.Options = MQGMO_NO_WAIT
                           + MQGMO_FAIL_IF_QUIESCING
                           + MQGMO_ACCEPT_TRUNCATED_MSG;
    
        RecBuffLen = sizeof(RecBuffer) - 1;
    
        memcpy(ReceiveMd.MsgId, MQMI_NONE, sizeof(ReceiveMd.MsgId));
        memcpy(ReceiveMd.CorrelId, MQCI_NONE, sizeof(ReceiveMd.CorrelId));
        ilMaxErrorTries = 0;
        do
        {
            MQGET(HconRecGet,     /* connection handle                 */
                  HobjRecGet,      /* object handle                     */
                  &ReceiveMd,     /* message descriptor                */
                  &QueueGmo,      /* get message options               */
                  RecBuffLen,     /* buffer length                     */
                  RecBuffer,      /* message buffer                    */
                  &MessLen,       /* message length                    */
                  &QueueCompCode, /* completion code                   */
                  &QueueReason);  /* reason code                       */

            /*dbg( TRACE, "<%s> MQGET - completion code = <%d>", pclFunc, QueueCompCode );*/
            if (QueueReason != MQRC_NONE)
            {
               dbg(TRACE,"<%s> !!!!!!!!!! ERROR WHILE RECEIVING MESSAGE !!!!!!!!!!!!!!!",pclFunc);
               dbg(TRACE,"<%s> MQGET ended with Reasoncode: <%d>",pclFunc,QueueReason);
            }

            switch( QueueReason )
            {
                case (MQRC_NONE):
                    RecBuffer[MessLen] = '\0';
                    dbg(DEBUG,"<%s> deleted Msg: <%s>",pclFunc,RecBuffer);
                    dbg(TRACE,"<%s> MQGET received and deleted Message from Queue",pclFunc);
                    break;

                case (2009):    /* MQRC_CONNECTION_BROKEN */
                    dbg( TRACE, "<%s> (MQGET2) CONNECTION IS BROKEN!!! TERMINATE!!!", pclFunc );
                    Terminate(10);
            
                case (2079):    /* MQRC_TRUNCATED_MSG_ACCEPTED */
                    dbg(TRACE,"<%s> Removing truncated msg",pclFunc,QueueReason);
                    break;

                case (2033):
                    break;

                case (2016):
                    dbg(TRACE,"<%s> GetInhibited, waiting 120 seconds for retry", pclFunc);
                    sleep(120);
                    ilMaxErrorTries++;
                    if( ilMaxErrorTries >= MAX_NUM_RETRIES )
                    {
                        if(strcmp(cgUseAlerter,"YES") == 0 )
                        {
                           sprintf(pclAlert,"<%s>: MQGET ended with Reasoncode: <%d> ",cgContextAlert,QueueReason);
                           ilRC = AddAlert2(cgNameAlert," "," ","E",pclAlert," ",TRUE,FALSE,"ALERT",mod_name," "," ");
                        }
                       dbg(TRACE,"<%s> Hybernating the process for %d seconds!!!!", pclFunc, HYBERNATE_TIMING );
                       sleep(HYBERNATE_TIMING);
                       ilMaxErrorTries = 0;
                   }

                default:
                    dbg( TRACE, "<%s> MAJOR ERROR!!! GOING TO TERMINATE", pclFunc );
                    sleep( 120 );
                    Terminate(1);

            } /* switch */
        } while (QueueBrowseReason != MQRC_NONE && QueueBrowseReason != 2033 && QueueBrowseReason != 2079);

        dbg(TRACE,"<%s> Check parent status",pclFunc );
        if( isParentAlive() == FALSE )
            Terminate(3);
    } /* end of for(;;) */

    return ilRC;

} /* ReceiveFromMQ */


static int SendToCeda( char *pcpBuffer, char *pcpBeginUrno, char *pcpEndUrno, int ipCount ) 
{
static int ilTest = 1;
    int ili = 0;
    int ilRC = RC_SUCCESS;
    int ilLen = 0;
    char pclBeginUrno[12] = "\0";
    char pclEndUrno[12] = "\0";
    char pclSqlBuf[4500] ="\0";
    char pclTwStart[30] = "\0";
    char pclTwEnd[30] = "\0";
    char pclSelection[200] = "\0";
    char *pclFunc = "SendToCeda";

    dbg(TRACE,"<%s> ----- START -----", pclFunc);

    strcpy( pclBeginUrno, pcpBeginUrno );
    strcpy( pclEndUrno, pcpEndUrno );
    dbg(TRACE,"<%s> Range of URNO <%s> - <%s>", pclFunc, pclBeginUrno, pclEndUrno );

    ilLen = strlen(pcpBuffer);
    dbg(TRACE,"<%s> Len of Data <%d>", pclFunc, ilLen );

    /* NOW SEND EVENT TO CEDA */
    sprintf( pclTwStart,"%s,%s,%s",mod_name,pclBeginUrno,pclEndUrno );
    sprintf( pclTwEnd,"%s,%s,%s",cgHopo,cgTabEnd,mod_name );
    if( strlen(cgSelection) > 0 )
        strcpy( pclSelection, cgSelection );

    dbg(TRACE,"<%s> Now Send Event to <%d>", pclFunc, atoi(cgCedaSend));
      
    if( igSendUrno == FALSE )
        ilRC = SendCedaEvent(atoi(cgCedaSend),0,mod_name,"TOOL",pclTwStart,pclTwEnd,cgCedaSendCommand,"MR2TAB",
                             pclSelection, "", pcpBuffer,"",igSendPrio,RC_SUCCESS);
    else
        ilRC = SendCedaEvent(atoi(cgCedaSend),0,mod_name,"TOOL",pclTwStart,pclTwEnd, cgCedaSendCommand,"MR2TAB",
                             pclSelection, "","URNO", pclBeginUrno,"",igSendPrio,RC_SUCCESS);

/* Owl */
    /*if( ilTest == 0 )
    {
        ilRC = RC_FAIL;
        ilTest = 1 ;
    }
    else
        ilTest = 0; */

    if( igLogging == TRUE )
    {
        if (ilRC != RC_SUCCESS)
        {
            dbg(TRACE,"<%s> ERROR in sending to CEDA process",pclFunc );
            MarkRecord( "MR2TAB", pclBeginUrno, pclEndUrno, "ERR", ipCount );
            igSendCedaFail = TRUE;
        }
        else
        {
            dbg(TRACE,"<%s> OK in sending to CEDA process",pclFunc );
            MarkRecord( "MR2TAB", pclBeginUrno, pclEndUrno, "OK", ipCount );
            igSendCedaFail = FALSE;
        }
    }
    dbg(TRACE,"<%s> ----- END -----",pclFunc);
    return ilRC;
}



static int TimeToStr(char *pcpTime,time_t lpTime)
{
    struct tm *_tm;
    char   _tmpc[6];

    /*_tm = (struct tm *)localtime(&lpTime); */
    _tm = (struct tm *)gmtime(&lpTime);
    /*      strftime(pcpTime,15,"%" "Y%" "m%" "d%" "H%" "M%" "S",_tm); */


    sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
            _tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
            _tm->tm_min,_tm->tm_sec);

    return RC_SUCCESS;

}     /* end of TimeToStr */


/* For Data Recovery */
/* MQID will be make up of HHMISSMMMXXXX, 13 characters, where XXX specifies the packet index between 1-9999 */
/* REAS will contain the packet number, with 9999 to mark the end of packet                                  */
/* The purpose of having MQID is to facilitate sorting, and REAS to specify the actual pkt number            */

static int DoDataRecovery( int ipRole, int ipQnum )
{
    MQDBLOG rlMqDbLog;
    MQDBLOG rlMqDbLog_Begin;
    MQDBLOG rlMqDbLog_End;
    int blBigPackStart = FALSE;
    int blNewPacket = TRUE;
    int ili = 0;
    int ilj = 0;
    int ilTries = 0;
    int ilRC;
    int ilPktNumber = 0;
    int ilPktTrace = 0;
    int ilCount;
    short slSqlFunc = 0;
    short slSqlCursor = 0;
    char pclSqlBuf[1000] = "\0";
    char pclData[4500] = "\0";
    char pclExpectedPNUM[32] = "\0";
    char pclTabName[10] = "\0";
    char *pclBigPack = NULL;
    char *pclFunc = "DoDataRecovery";

    if( igRecovery == FALSE )
        return;

    dbg(TRACE,"<%s> ----- START -----", pclFunc);

    strcpy( pclTabName, "MS2TAB" );
    if( ipRole == ROLE_RECEIVER )
        strcpy( pclTabName, "MR2TAB" );
 
    sprintf(pclSqlBuf,"SELECT TRIM(COUN), TRIM(DATA), TRIM(PNUM), TRIM(PSEQ), TRIM(STAT), "
                      "TRIM(TPUT), TRIM(URNO) FROM %s "
                      "WHERE STAT = 'RCV' OR STAT LIKE 'E%%' AND INAM='%s' ORDER BY TPUT, PNUM ASC", pclTabName, mod_name);
    dbg(TRACE,"<%s> SqlBuf <%s>", pclFunc, pclSqlBuf);

    slSqlFunc = START;
    slSqlCursor = 0;
    blBigPackStart = FALSE;
    blNewPacket = TRUE;
    ili = ilj = 1;
    ilRC = RC_SUCCESS;
    while( sql_if (slSqlFunc, &slSqlCursor, pclSqlBuf, pclData) == RC_SUCCESS)
    {
        if( blNewPacket == TRUE )
        {
            if( pclBigPack != NULL )
            {
                free( (char *)pclBigPack );
                pclBigPack = NULL;
            }
            ilj = 1;
            blBigPackStart = FALSE;
            memset( &rlMqDbLog_Begin, 0, sizeof(MQDBLOG) );
            memset( &rlMqDbLog_End,   0, sizeof(MQDBLOG) );
        }

        dbg( TRACE, "<%s> .... Recovering Pkt %4d .....", pclFunc, ili++ );

        slSqlFunc = NEXT;
        ConvertDbStringToClient(pclData);
        memset( &rlMqDbLog, 0, sizeof(rlMqDbLog) );
        get_fld(pclData, FIELD_1, STR, THREE_FOUR, rlMqDbLog.COUN);
        get_fld(pclData, FIELD_2, STR, DATALEN,    rlMqDbLog.DATA);
        get_fld(pclData, FIELD_3, STR, SIX_FOUR,   rlMqDbLog.PNUM);
        get_fld(pclData, FIELD_4, STR, THREE_FOUR, rlMqDbLog.PSEQ);
        get_fld(pclData, FIELD_5, STR, THREE_FOUR, rlMqDbLog.STAT);
        get_fld(pclData, FIELD_6, STR, FIVE_FOUR,  rlMqDbLog.TPUT);
        get_fld(pclData, FIELD_7, STR, THREE_FOUR, rlMqDbLog.URNO);
        dbg( TRACE, "<%s> URNO <%s> TPUT <%s>", pclFunc, rlMqDbLog.URNO, rlMqDbLog.TPUT );
        dbg( TRACE, "<%s> PSEQ <%s> STAT <%s>", pclFunc, rlMqDbLog.PSEQ, rlMqDbLog.STAT );
        dbg( TRACE, "<%s> COUN <%s> PNUM <%s>", pclFunc, rlMqDbLog.COUN, rlMqDbLog.PNUM );
        dbg( TRACE, "<%s> DATA <%s>", pclFunc, rlMqDbLog.DATA );
        ilCount = atoi(rlMqDbLog.COUN) + 1;

        ilPktTrace = atoi(rlMqDbLog.PSEQ);
        ilPktNumber = atoi(&rlMqDbLog.PNUM[16]);
        dbg( DEBUG, "<%s> Pkt trace <%d> number <%d>", pclFunc, ilPktTrace, ilPktNumber );
        /* Contain more then 1 pkt                                               */
        /* Assuming pkt has TPUT arranged in ascending order                     */
        /* If PSEQ = 999999999 and PNUM = YYYYMMDDHHMISSMMM000001, this mean only 1 pkt to send */
        if( ilPktTrace != PKT_TAIL || ilPktNumber != PKT_HEAD ) 
        {
            blNewPacket = FALSE;
            if( (pclBigPack = (char *)realloc( pclBigPack, (ilj*DATALEN)+1 )) == NULL )
            {
                dbg( TRACE, "<%s> Realloc failed! for URNO <%s>", pclFunc, rlMqDbLog.URNO );
                Terminate( 5 );
            }
            if( ilPktTrace == PKT_HEAD || strlen(rlMqDbLog_Begin.URNO) <= 0 )
            {
                memcpy( &rlMqDbLog_Begin, &rlMqDbLog, sizeof(MQDBLOG) );
                blBigPackStart = TRUE;
            }
            memcpy( &rlMqDbLog_End, &rlMqDbLog, sizeof(MQDBLOG) );
            if( blBigPackStart == FALSE )
            {
                dbg( TRACE, "<%s> Broken packet(No first pkt). Mark as FAIL", pclFunc );
                MarkRecord( pclTabName, rlMqDbLog_Begin.URNO, rlMqDbLog_End.URNO, "FAIL", ilCount );
                blNewPacket = TRUE;
                continue;
            }

            sprintf( pclExpectedPNUM, "%16.16s%6.6d", &rlMqDbLog_Begin.TPUT, ilj++ );
            dbg( TRACE, "<%s> Expect PNUM <%s>", pclFunc, pclExpectedPNUM );

            if( strncmp( pclExpectedPNUM, rlMqDbLog.PNUM, 22 ) )
            {
                dbg( TRACE, "<%s> Broken packet. Mark as FAIL", pclFunc );
                MarkRecord( pclTabName, rlMqDbLog_Begin.URNO, rlMqDbLog_End.URNO, "FAIL", ilCount );
                blNewPacket = TRUE;
                continue;
            }

            strcat( pclBigPack, rlMqDbLog.DATA );
            dbg( TRACE, "<%s> Append Pkt <%d> URNO <%s>", pclFunc, (ilj-1), rlMqDbLog.URNO );
            if( ilPktTrace == PKT_TAIL )
            {
                dbg( TRACE, "<%s> DATA <%s>", pclFunc, pclBigPack );
                ilRC = SendData(ipRole, pclBigPack,rlMqDbLog_Begin.URNO, rlMqDbLog_End.URNO, ilCount, ipQnum );
                dbg( TRACE, "<%s> Completed BIG PACK! ilRC <%d>", pclFunc, ilRC );

                if( ipRole == ROLE_SENDER )
                {
                    ilTries = 1;
                    while( ilRC != RC_SUCCESS )
                    {
                        if( ilRC != 2053 ) /* MQRC_Q_FULL */
                        {
                            ilRC = RC_FAIL;
                            break;
                        }
                        sleep( 3 );
                        ilRC = SendData(ipRole, pclBigPack,rlMqDbLog_Begin.URNO, rlMqDbLog_End.URNO, ilCount++, ipQnum );
                        dbg( TRACE, "<%s> Tries <%d>", pclFunc, ilTries++ );
                    }
                }
                if( ilRC == RC_FAIL )
                {
                    commit_work();
                    close_my_cursor (&slSqlCursor);
                    return ilRC;
                }
                blNewPacket = TRUE;
                continue;
            }
        } /* Multiple packs */
        else
        {
            /* Broken pkt, esp for BIG PACK with only the 1st pkt to be recovered. Not really possible for this to happen */
            if( blNewPacket == FALSE )
                MarkRecord( pclTabName, rlMqDbLog_Begin.URNO, rlMqDbLog_End.URNO, "FAIL", ilCount );

            dbg( TRACE, "<%s> DATA <%s>", pclFunc, rlMqDbLog.DATA );
            ilRC = SendData( ipRole, rlMqDbLog.DATA, rlMqDbLog.URNO, rlMqDbLog.URNO, ilCount, ipQnum);
            dbg( TRACE, "<%s> Completed SMALL PACK! ilRC <%d>", pclFunc, ilRC );
            if( ipRole == ROLE_SENDER )
            {
                ilTries = 1;
                while( ilRC != RC_SUCCESS )
                {
                    if( ilRC != 2053 ) /* MQRC_Q_FULL */
                    {
                        ilRC = RC_FAIL;
                        break;
                    }
                    sleep( 3 );
                    ilRC = SendData( ipRole, rlMqDbLog.DATA, rlMqDbLog.URNO, rlMqDbLog.URNO, ilCount++, ipQnum);
                    dbg( TRACE, "<%s> Tries <%d>", pclFunc, ilTries++ );
                }
            }
            if( ilRC == RC_FAIL )
            {
                commit_work();
                close_my_cursor (&slSqlCursor);
                return ilRC;
            }
            blNewPacket = TRUE;
        }
        /* Broken pkt, esp for BIG PACK with only the 1st pkt to be recovered. Not really possible for this to happen */
        /*if( blNewPacket == FALSE )
            MarkRecord( pclTabName, rlMqDbLog_Begin.URNO, rlMqDbLog_End.URNO, "FAIL", atoi(rlMqDbLog.COUN) ); */
    } /* while */
    commit_work();
    close_my_cursor (&slSqlCursor);

    dbg(TRACE,"<%s> ----- END -----", pclFunc );
    return ilRC;
} /*DoDataRecovery*/

static int SendData( int ipRole, char *pcpData, char *pcpBeginUrno, char *pcpEndUrno, int ipCount, int ipQnum )
{
    int ilRC;
    char *pclFunc = "SendData";

    dbg( TRACE, "<%s> ------ START --------", pclFunc );
    if( ipRole == ROLE_RECEIVER )
        ilRC = SendToCeda(pcpData, pcpBeginUrno, pcpEndUrno, ipCount);
    else
        ilRC= SendToMq(pcpData, pcpBeginUrno, pcpEndUrno, ipCount, ipQnum);
    dbg( TRACE, "<%s> ilRC = <%d>", pclFunc, ilRC );
    dbg( TRACE, "<%s> ------ END --------", pclFunc );
    return ilRC;
}

static int LogMsg( char *pcpData, char *pcpMQtime, char *pcpBeginUrno, char *pcpEndUrno, 
                   char *pcpStatus, int ipRole, int ipMsglen )
{
static ilTest = 1;
    int ilRC = RC_SUCCESS;
    int ili;
    int ilNumPkt = 0;
    int ilPktNumber = 0;
    int ilBaseUrno = 0;
    int ilLastUrno = 0;
    short slSqlFunc = START;
    short slSqlCursor = 0;
    char pclMyBuffer[MQPKTLEN] = "\0";
    char pclDBText[MQTAB_DATALEN+1] = "\0";
    char pclMyTime[20] = "\0";
    char pclData[100] = "\0";
    char pclUrno[20] = "\0";
    char pclNowTime[20] = "\0";
    char pclSqlBuf[4500] ="\0";
    char pclTmpStr[1000] ="\0";
    char pclTabName[10] = "\0";
    char *pclFunc = "LogMsg";


    dbg( TRACE, "<%s> ----- START ------", pclFunc );
    if( igLogging == FALSE )
        return;

    strcpy( pclTabName, "MS2TAB" );
    if( ipRole == ROLE_RECEIVER )
        strcpy( pclTabName, "MR2TAB" );
    dbg(TRACE,"<%s> ipRole <%d> Table name <%s>", pclFunc, ipRole, pclTabName);

    ilNumPkt = 1;
    if( ipMsglen > MQTAB_DATALEN )
    {
        ilNumPkt = (ipMsglen/MQTAB_DATALEN);
        if( (ipMsglen%MQTAB_DATALEN) > 0 )
            ilNumPkt++;
    }
    dbg(TRACE,"<%s> MsgLen <%d> NumPkt <%d>", pclFunc, ipMsglen, ilNumPkt);

    ilBaseUrno = NewUrnos( pclTabName, ilNumPkt );
    if( ilBaseUrno <= 0 )
    {
        dbg( TRACE, "<%s> Error <%d> in getting New URNO!!! Record not inserted into %s", 
                                                                pclFunc, ilBaseUrno, pclTabName );
        return;
    }
    ilLastUrno = ilBaseUrno + ilNumPkt - 1;

    /* INSERT MESSAGE INTO Oracle Table */
    dbg( DEBUG, "<%s> Base <%d> Last <%d> URNO", pclFunc, ilBaseUrno, ilLastUrno );

    sprintf( pcpBeginUrno, "%d", ilBaseUrno );
    sprintf( pcpEndUrno, "%d", ilLastUrno );
    dbg( TRACE, "<%s> URNO range <%s>-<%s>", pclFunc, pcpBeginUrno, pcpEndUrno );
  
    ConvertClientStringToDb(pcpData);
    TimeToStr(pclNowTime,time(NULL));

    for( ili = 1; ili <= ilNumPkt; ili++ )
    {
        ilPktNumber = ili;
        if( ili == ilNumPkt )
            ilPktNumber = PKT_TAIL;
        ilLastUrno = ilBaseUrno + ili - 1;
        strncpy( pclDBText, &pcpData[(ili-1)*MQTAB_DATALEN], MQTAB_DATALEN );
        if( strlen(pcpMQtime) <= 0 )
            sprintf( pcpMQtime, "%s00", pclNowTime );
        sprintf( pclTmpStr, "%16.16s%6.6d", pcpMQtime, ili );
        sprintf(pclSqlBuf,"INSERT INTO %s (URNO,INAM,TPUT,DATA,COUN,STAT,PSEQ,LSTU,PNUM) VALUES "
                          "(%d,'%s','%s','%s',%d,'%s',%d,'%s','%s')", pclTabName,
                           ilLastUrno, mod_name, pcpMQtime, pclDBText, 1, 
                           pcpStatus, ilPktNumber, pclNowTime, pclTmpStr);
        dbg(TRACE,"<%s> pclSqlBuf: <%s>",pclFunc,pclSqlBuf);
        slSqlFunc = START;
        slSqlCursor = 0;
        ilRC = sql_if (slSqlFunc, &slSqlCursor, pclSqlBuf, pclData);
/* Owl */
/*if( ilTest == 1 )
{
    ilRC = RC_SUCCESS;
    ilTest = 2;
}
else
    ilRC = RC_FAIL;
*/
        if (ilRC != DB_SUCCESS)
        {
            dbg(TRACE,"<%s> sql_if failed###################", pclFunc);
			rollback();
            Terminate( 10 );
        }
        commit_work();   /* No choice have to commit for every record. As cursor is limited */
        close_my_cursor (&slSqlCursor);
    }
    ConvertDbStringToClient(pcpData);
    dbg( TRACE, "<%s> ----- END ------", pclFunc );
    return ilRC;

} /* LogMsg */

static int OpenSendMQ(int ipQueueNumber)
{
    MQOD od = {MQOD_DEFAULT};
    MQLONG   O_options;              
    MQLONG   QueueOpenCode;               /* completion code                 */
    MQLONG   QueueReason;                 /* reason code                     */
    int ilRC = RC_SUCCESS;
    int ilMaxErrorTries = 0;
    static char pclAlert[256] = "\0";

    ilMaxErrorTries = 0;
    while(igConnected == FALSE)
    {
        ilRC = ConnectMQ(cgQMgrSend, &HconSend);
        if( ilRC == RC_SUCCESS )
            break;

        dbg(TRACE,"%05d: !!!!!!!!!! ERROR WHILE connecting to Qmanager !!!!!!!!!!!!!!!",__LINE__);
        ilMaxErrorTries++;
        if( ilMaxErrorTries >= MAX_NUM_RETRIES )
        {
            if(strcmp(cgUseAlerter,"YES") == 0)
            {
                sprintf(pclAlert,"<%s>: MQCONN failed while connecting to QManager <%s>",
                                cgContextAlert,cgQMgrSend);
                ilRC = AddAlert2(cgNameAlert," "," ","E",pclAlert," ",TRUE,FALSE,"ALERT",mod_name," "," ");
            }
            dbg(TRACE,"%5d: Hybernating the process for %d seconds!!!!", __LINE__, HYBERNATE_TIMING );
            sleep(HYBERNATE_TIMING);
            ilMaxErrorTries = 0;
        }
        dbg(TRACE,"%05d: waiting <%s> seconds before try to reconnect",__LINE__,cgReconnectMq);
        sleep(atoi(cgReconnectMq));
    }

    strcpy(od.ObjectName, rgMyQueues[ipQueueNumber].cgQName);
    
    O_options = MQOO_OUTPUT         /* open queue for output          */
                + MQOO_FAIL_IF_QUIESCING; /* but not if MQM stopping    */
    QueueReason = 1111; /* != MQRC_NONE */
    QueueOpenCode = MQCC_FAILED;

    ilMaxErrorTries = 0;
    while( QueueReason != MQRC_NONE ) 
    {
        MQOPEN (HconSend, &od, O_options, &rgMyQueues[ipQueueNumber].HobjSend, &QueueOpenCode, &QueueReason);

        if (QueueReason == MQRC_NONE && QueueOpenCode != MQCC_FAILED)
        {
            dbg(TRACE,"%05d: Queue <%s> opened for sending to MQ",__LINE__,rgMyQueues[ipQueueNumber].cgQName);
            return RC_SUCCESS;
        }
        dbg(TRACE,"%05d: !!!!!!!!!! ERROR WHILE Open Queue <%s> !!!!!!!!!!!!!!!",__LINE__,rgMyQueues[ipQueueNumber].cgQName);
        dbg(TRACE,"%05d: MQOPEN ended with reason code <%d>",__LINE__, QueueReason);

        ilMaxErrorTries++;
        ilRC = RC_FAIL;
        if (QueueReason ==  MQRC_CONNECTION_BROKEN)
        {
            dbg(TRACE,"Connection broken, try to reconnect at next message");
            igConnected = FALSE;
            break;
        }
        if (QueueOpenCode == MQCC_FAILED)
            dbg(TRACE,"%05d: unable to open send-queue <%s>",__LINE__,rgMyQueues[ipQueueNumber].cgQName);
        if( ilMaxErrorTries >= MAX_NUM_RETRIES )
        {
            if(strcmp(cgUseAlerter,"YES") == 0 )
            {
                sprintf(pclAlert,"<%s>: MQOPEN ended with reason code <%d> while connecting to Queue <%s>",
                                cgContextAlert,QueueReason,rgMyQueues[ipQueueNumber].cgQName);
                ilRC = AddAlert2(cgNameAlert," "," ","E",pclAlert," ",TRUE,FALSE,"ALERT",mod_name," "," ");
            }
            break;
        }
    }
    return ilRC;
}

void myinitialize(int argc,char *argv[])
{
    mod_name = argv[0];
    /* 20020822 JIM: if argv[0] contains path (i.e. started by ddd), skip path */
    if (strrchr(mod_name,'/')!= NULL)
    {
        mod_name= strrchr(mod_name,'/');
        mod_name++;
    }
    /*if(gChildPid == 0)
    {
        sprintf(__CedaLogFile,"%s/%sc%.5ld.log",getenv("DBG_PATH"),mod_name,getpid());
    }
    else
    {
        sprintf(__CedaLogFile,"%s/%sp%.5ld.log",getenv("DBG_PATH"),mod_name,getpid());
    }
    outp = fopen(__CedaLogFile,"w");*/
    mod_id = 0;
    ctrl_sta = 1;
    if (argc > 1)
    {
        mod_id = atoi(argv[1]);
        ctrl_sta = atoi(argv[2]);
    } 
    glbrc = init();
    if(glbrc)
    {
        fprintf(outp,"\n<%s><%ld>STHINIT failed: ret = %d\n",mod_name,getpid(),glbrc);
        exit(1);
    }
}



static int SendToMq(char *pcpData, char *pcpBeginUrno, char *pcpEndUrno, int ipCount, int ipQnum )
{
static int ilTest = 1;
    int ilRC = RC_SUCCESS;
    int ilDataSize = 0;
    char pclMyTime[30] = "\0";
    char pclNowTime[30] = "\0";
    char pclData[4500] = "\0";
    char pclAlert[256] = "\0";
    char pclTmpStr[200] = "\0";
    char pclBeginUrno[15] = "\0";
    char pclEndUrno[15] = "\0";
    char *pclFunc = "SendToMq";


    MQMD     md = {MQMD_DEFAULT};    /* Message Descriptor              */
    MQLONG   CompCode;               /* completion code                 */
    MQLONG   Reason;                 /* reason code                     */
    MQPMO    pmo = {MQPMO_DEFAULT};   /* put message options             */

    dbg(TRACE,"<%s> ----- START -----", pclFunc);
    ilDataSize = strlen(pcpData);
    dbg(TRACE,"<%s> Data Size <%d>", pclFunc, ilDataSize );

    if( pcpBeginUrno != NULL )
        strcpy( pclBeginUrno, pcpBeginUrno );
    if( pcpEndUrno != NULL )
        strcpy( pclEndUrno, pcpEndUrno );
    
    dbg(TRACE,"<%s> URNO range <%s>-<%s>", pclFunc, pclBeginUrno, pclEndUrno );
    dbg(TRACE,"<%s> Count <%d>", pclFunc, ipCount );

    /* PUT MESSAGE TO MQ QUEUE */
    memcpy(md.Format, MQFMT_STRING, MQ_FORMAT_LENGTH);
    memcpy(md.MsgId, MQMI_NONE, sizeof(md.MsgId));
    if(rgMyQueues[ipQnum].igQDynPrio == -1)
        md.Priority = rgMyQueues[ipQnum].igQPrio;
    else
        md.Priority = rgMyQueues[ipQnum].igQDynPrio;

    pmo.Options = MQPMO_NEW_MSG_ID
                + MQPMO_FAIL_IF_QUIESCING;

    dbg(TRACE,"<%s> Send to Queue: <%s> pcpData <%s>, length <%d>", pclFunc,rgMyQueues[ipQnum].cgQName,pcpData,ilDataSize);

    ilRC = RC_SUCCESS;
    MQPUT(HconSend,                       /* connection handle               */
          rgMyQueues[ipQnum].HobjSend,    /* object handle                   */
          &md,                            /* message descriptor              */
          &pmo,                           /* default options                 */
          ilDataSize,                     /* buffer length                   */
          pcpData,                        /* message buffer                  */
          &CompCode,                      /* completion code                 */
          &Reason);                       /* reason code                     */

    strcpy(pclMyTime,md.PutDate);
    pclMyTime[16] = '\0';
    /* report reason, if any */
    dbg(TRACE,"MQPUT ended with reason code <%d>, Time: <%s> ", Reason, pclMyTime);

    if (Reason == 2009) /* MQRC_CONNECTION_BROKEN */
    {
        dbg( TRACE, "(MQPUT) CONNECTION IS BROKEN!!! TERMINATE!!!" );
        Terminate(10);
    }

/* Owl */
    /*if( ilTest == 0 )
    {
        Reason = 2026;
        ilTest = 1 ;
    }
    else
        ilTest = 0; 
*/

    if( Reason == MQRC_NONE)
    {
        if( igLogging == TRUE )
        {
            if( pcpBeginUrno == NULL || pcpEndUrno == NULL )
                LogMsg( pcpData, pclMyTime, pclBeginUrno, pclEndUrno, "OK", ROLE_SENDER, ilDataSize );
            else
                MarkRecord( "MS2TAB", pclBeginUrno, pclEndUrno, "OK", ipCount );
        }
        if( igRecovery == TRUE )
            rgMyQueues[ipQnum].igPutFailed = FALSE;
        ilRC = RC_SUCCESS;
    }
    else
    {
        dbg(TRACE,"%05d: !!!!!!!!!! ERROR WHILE SENDING MESSAGE !!!!!!!!!!!!!!!",__LINE__);
        if(strcmp(rgMyQueues[ipQnum].cgUseAlerter,"YES") == 0 )
        {
            sprintf(pclAlert,"<%s>: MQPUT ended with reason code <%d> ",rgMyQueues[ipQnum].cgContextAlert,Reason);
            ilRC = AddAlert2(rgMyQueues[ipQnum].cgNameAlert," "," ","E",pclAlert,pcpData,
                             TRUE,FALSE,"ALERT",rgMyQueues[ipQnum].cgQName," "," ");
        }
        if( igLogging == TRUE )
        {
            sprintf( pclTmpStr, "E%4.4d", Reason );
            if( pcpBeginUrno == NULL || pcpEndUrno == NULL )
                LogMsg( pcpData, pclMyTime, pclBeginUrno, pclEndUrno, pclTmpStr, ROLE_SENDER, ilDataSize );
            else
                MarkRecord( "MS2TAB", pclBeginUrno, pclEndUrno, pclTmpStr, ipCount );
        }

        if( igRecovery == TRUE )
            rgMyQueues[ipQnum].igPutFailed = TRUE;
        rgMyQueues[ipQnum].bgQConnected = FALSE;
        if(rgMyQueues[ipQnum].igPutFailedOld == FALSE)
        {
            strcpy(rgMyQueues[ipQnum].cgFirstFailedUrno,pclBeginUrno);
            strcpy(rgMyQueues[ipQnum].cgFirstFailedData,pcpData);
        }
        /*ilRC = RC_FAIL;*/
        ilRC = Reason;
    }
    dbg(TRACE,"<%s> ----- END -----", pclFunc );
    return ilRC;
}

static int MarkRecord( char *pcpTabName, char *pcpBeginUrno, char *pcpEndUrno, char *pcpStatus, int ipCount )
{
    int ilRC = RC_SUCCESS;
    int ilBeginUrno, ilEndUrno;
    short slSqlFunc = 0;
    short slSqlCursor = 0;
    char pclNowTime[30] = "\0";
    char pclTmpStr[500] = "\0";
    char pclSqlBuf[4500] = "\0";
    char pclData[50] = "\0";
    char *pclFunc = "MarkRecord";

    dbg(TRACE,"<%s> ----- START -----", pclFunc);

    if( igLogging == FALSE )
           return;
    if( pcpBeginUrno == NULL || pcpEndUrno == NULL )
    {
        dbg( TRACE, "<%s> Invalid Urno range", pclFunc );
        return;
    }

    TimeToStr(pclNowTime,time(NULL));

    sprintf(pclSqlBuf,"UPDATE %s SET COUN = %d, LSTU = '%s'", pcpTabName, ipCount, pclNowTime );

    if( pcpStatus != NULL )
    {
        sprintf( pclTmpStr, ", STAT = '%s'", pcpStatus );
        strcat( pclSqlBuf, pclTmpStr );
    }
   /* if( pcpMQTime != NULL )
    {
        sprintf( pclTmpStr, ", TPUT = '%s'", pcpMQTime );
        strcat( pclSqlBuf, pclTmpStr );
    }*/

    ilBeginUrno = atoi(pcpBeginUrno);
    ilEndUrno = atoi(pcpEndUrno);

    /* This is play by assumption that the turn-around wont be so soon, thus base on URNO and INAM, record 
       should be unique */
    if( ilBeginUrno == ilEndUrno )
        sprintf(pclTmpStr," WHERE URNO = %s AND INAM = '%s'", pcpBeginUrno, mod_name );
    else if( ilEndUrno > ilBeginUrno )
        sprintf(pclTmpStr," WHERE URNO >= %s AND URNO <= %s AND INAM = '%s'", pcpBeginUrno, pcpEndUrno, mod_name );
    else
    {
        dbg( TRACE, "<%s> Invalid URNO values Begin <%d> End <%d>", pclFunc, ilBeginUrno, ilEndUrno );
        return;
    }
    strcat( pclSqlBuf, pclTmpStr );

    dbg(TRACE,"<%s> Mark Message pclSqlBuf: <%s>",pclFunc,pclSqlBuf);

    slSqlFunc = START;
    slSqlCursor = 0;
    ilRC = sql_if (slSqlFunc, &slSqlCursor, pclSqlBuf, pclData);
    if (ilRC != RC_SUCCESS)
    {
        dbg(TRACE,"<%s> sql_if failed###################", pclFunc);
        Terminate( 3 );
    }
    commit_work();
    close_my_cursor (&slSqlCursor);
    dbg(TRACE,"<%s> ----- END -----", pclFunc);

    return ilRC;
}


void CleanDb()
{
    int ilRC = RC_SUCCESS;
    int ili = 0;
    time_t tlNow;
    time_t tlTime;
    short slSqlFunc;
    short slSqlCursor;
    char pclSqlBuf[200] = "\0";
    char pclSqlData[50] = "\0";
    char pclTimeStr[50] = "\0";
    char *pclFunc = "CleanDb";

    dbg(TRACE,"<%s> ----- Start -----", pclFunc);
    tlNow = time(0);

    /* NOW CLEAN UP MR2TAB*/
    tlTime = tlNow - tgCleanMr2;
    TimeToStr( pclTimeStr, tlTime );
    strcat( pclTimeStr, "00" );
    dbg( TRACE,"<%s> Now Delete Records older than <%s> from MR2TAB for INAM = <%s>", pclFunc, pclTimeStr, mod_name );

    slSqlFunc = START;
    slSqlCursor = 0;
    sprintf( pclSqlBuf,"DELETE FROM MR2TAB WHERE TPUT < '%s' AND INAM = '%s'", pclTimeStr, mod_name );
    ilRC = sql_if( slSqlFunc, &slSqlCursor, pclSqlBuf, pclSqlData );
    if( ilRC != RC_SUCCESS )
        dbg(TRACE,"<%s> MQR clean failed or nothing to delete in CleanDb", pclFunc);
    commit_work();
    close_my_cursor (&slSqlCursor);



    /* NOW CLEAN UP MS2TAB */  
    if( tgCleanMs2 != tgCleanMr2 )
    {
        tlTime = tlNow - tgCleanMs2;
        TimeToStr( pclTimeStr, tlTime );
        strcat( pclTimeStr, "00" );
    }
    
    slSqlFunc = START;
    slSqlCursor = 0;

    if(igUseQName == TRUE)
    {
        for(ili = 0 ; ili < 20 ; ili++)
        {
            if(strlen(rgMyQueues[ili].cgQName) > 0)
            {
                dbg( TRACE,"<%s> Now Delete Records older than <%s> from MS2TAB for INAM = <%s>", pclFunc, pclTimeStr, rgMyQueues[ili].cgQName );

                sprintf(pclSqlBuf,"DELETE FROM MS2TAB WHERE TPUT < '%s' AND INAM = '%s'",pclTimeStr,rgMyQueues[ili].cgQName);
                ilRC = sql_if (slSqlFunc, &slSqlCursor, pclSqlBuf, pclSqlData);
                if (ilRC != RC_SUCCESS)
                    dbg(TRACE,"<%s> MQS clean failed or nothing to delete in CleanDb Qname <%s>", pclFunc, rgMyQueues[ili].cgQName);
                commit_work();
                close_my_cursor (&slSqlCursor);
            }
        }
    }
    else
    {
        dbg( TRACE,"<%s> Now Delete Records older than <%s> from MS2TAB for INAM = <%s>", pclFunc, pclTimeStr, mod_name );
        sprintf(pclSqlBuf,"DELETE FROM MS2TAB WHERE TPUT < '%s' AND INAM = '%s'",pclTimeStr,mod_name);
        ilRC = sql_if (slSqlFunc, &slSqlCursor, pclSqlBuf, pclSqlData);
        if (ilRC != RC_SUCCESS)
            dbg(TRACE,"<%s> MQS clean failed or nothing to delete in CleanDb", pclFunc);
        commit_work();
        close_my_cursor (&slSqlCursor);
    }
    dbg(TRACE,"<%s> ----- End -----", pclFunc);
}


static int GetQueueNumber(char *pclQueueName)
{
    int ili = 0;
    int ilResult = -1;

    for(ili = 0 ; ili < 20 ; ili++)
    {
        if(strcmp(rgMyQueues[ili].cgQName,pclQueueName) == 0)
        {
            ilResult = ili;
        }
    }
    return ilResult;
}


static int isParentAlive()
{
    struct stat rlFileStat;
    char pclTmpStr[100] = "\0";
    char pclPathName[200] = "\0";
    char *pclFunc = "isParentAlive";

    sprintf( pclPathName, "%s/%s.tmp", getenv("DBG_PATH"), mod_name );
    /*sprintf( pclTmpStr, "ps -ef | grep -w %s | grep -w \'%d\' | grep -v \'%d\' > %s", 
                         mod_name, gParentPid, getpid(), pclPathName );*/
    sprintf( pclTmpStr, "ps -ef | grep -w %s | awk \'{ if ($2 == %d) print $0}\' > %s",
                         mod_name, gParentPid, pclPathName );
    dbg( DEBUG, "<%s> system cmd <%s>", pclFunc, pclTmpStr );
    system( pclTmpStr );

    memset( &rlFileStat, 0, sizeof( struct stat ) );
    if( stat( pclPathName, &rlFileStat ) == -1 )
    {
        dbg( TRACE, "<%s> Unable to stat file <%s> error <%s>-<%d>", pclFunc, pclPathName, errno, strerror(errno) );
        return FALSE;
    }
    if( rlFileStat.st_size > 0 )
    {
        dbg( TRACE, "<%s> Parent is alive.", pclFunc, pclPathName );
        return TRUE;
    }
    dbg( TRACE, "<%s> Parent is dead.", pclFunc, pclPathName );
    return FALSE;
}
