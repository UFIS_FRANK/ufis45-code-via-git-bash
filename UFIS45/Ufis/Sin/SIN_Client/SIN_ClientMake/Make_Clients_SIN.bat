Rem BEGIN
date /T
time /T 

Echo Build Project SIN > con

Rem ---Check for folder c:\ufis\system---
IF EXIST c:\ufis\system\nul GOTO cont1
Echo Create folder C:\ufis\system > con
md c:\ufis\system
:cont1

Rem ---Check for folder c:\ufis_Bin\Runtimedlls---
IF EXIST C:\Ufis_Bin\Runtimedlls\nul GOTO cont2
Echo Create folder C:\ufis_Bin\Runtimedlls > con
md C:\Ufis_Bin\Runtimedlls
:cont2

Rem ---Check for folder c:\ufis_Bin\templates---
IF EXIST c:\ufis_Bin\templates\nul GOTO cont3
Echo Create folder C:\ufis_Bin\templates > con
md c:\ufis_Bin\templates
:cont3


IF EXIST C:\tmp\nul goto cont4
Echo Create folder C:\tmp > con
md C:\tmp
:cont4

ECHO Project SINGAPORE4.5 > C:\Ufis_Bin\SIN45.txt

mkdir c:\Ufis_Bin\ClassLib
mkdir c:\Ufis_Bin\ClassLib\Include
mkdir c:\Ufis_Bin\ClassLib\Include\jm
mkdir C:\Ufis_Bin\ForaignLibs
mkdir C:\Ufis_Bin\Help

copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Regularexpressionlib\lib\*.lib C:\Ufis_Bin\ForaignLibs
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Regularexpressionlib\lib\*.pdb C:\Ufis_Bin\ForaignLibs
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Regularexpressionlib\lib\*.dll C:\Ufis_Bin\Runtimedlls
copy ..\..\..\_Standard\_Standard_client\_Standard_configuration\Clientruntime\*.* C:\Ufis_Bin\Runtimedlls
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Regularexpressionlib\include\*.h c:\Ufis_Bin\ClassLib\Include
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Regularexpressionlib\include\jm\*.h c:\Ufis_Bin\ClassLib\Include\jm
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_ClassLib\*.h c:\Ufis_Bin\ClassLib\Include
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Wrapper\*.h c:\Ufis_Bin\ClassLib\Include

copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Classlib\*.h c:\Ufis_Bin\ClassLib\Include
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_Classlib\Classlib.rc c:\Ufis_Bin\ClassLib\Include

copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_HtmlHelp\lib\*.lib C:\Ufis_Bin\ForaignLibs
copy ..\..\..\_Standard\_Standard_client\_Standard_share\_Standard_HtmlHelp\include\*.h c:\Ufis_Bin\ClassLib\Include
copy ..\..\..\_Standard\_Standard_client\_Standard_help\*.chm c:\Ufis_Bin\help

copy ..\..\..\_Standard\_Standard_client\_Standard_dutyroster\_Standard_RosteringPrint\RosteringPrint.ulf c:\Ufis_Bin


copy ..\SIN_Share\SIN_HASConfigTool\templates\*.* c:\Ufis_Bin\templates

copy ..\SIN_configuration\ceda.ini c:\Ufis_Bin
copy ..\SIN_configuration\Bdps_sec.cfg c:\Ufis_Bin
copy ..\SIN_configuration\Satslogo.bmp c:\Ufis_Bin
copy ..\SIN_configuration\notvalid*.bmp c:\Ufis_Bin
copy ..\SIN_configuration\grp.ini c:\Ufis_Bin
copy ..\SIN_configuration\Telexpool.ini c:\Ufis_Bin
copy ..\SIN_configuration\ImportTool.ini c:\Ufis_Bin
copy ..\SIN_configuration\LoaTabCfg.cfg c:\Ufis_Bin
copy ..\SIN_configuration\Loatabviewer.ini c:\Ufis_Bin
copy ..\SIN_configuration\HASConfigTool.cfg c:\Ufis_Bin
copy ..\SIN_configuration\daco3tool.ini c:\Ufis_Bin
copy ..\SIN_configuration\InfoPC.cfg c:\Ufis_Bin
copy ..\SIN_configuration\TextTable.txt c:\Ufis_Bin

copy copy_for_install.bat c:\Ufis_Bin


REM ------------------Cients build from _Standard-Project-----------------------------------

REM ------------------------Share-----------------------------------------------------------

cd ..\..\..\_Standard\_Standard_client

ECHO Build Ufis32.dll... > con
cd _Standard_share\_Standard_Ufis32dll
msdev Ufis32.dsp /MAKE "Ufis32 - Debug" /REBUILD
msdev Ufis32.dsp /MAKE "Ufis32 - Release" /REBUILD
If exist c:\Ufis_Bin\Debug\Ufis32.ilk del c:\Ufis_Bin\Debug\Ufis32.ilk
If exist c:\Ufis_Bin\Debug\Ufis32.pdb del c:\Ufis_Bin\Debug\Ufis32.pdb
If exist c:\Ufis_Bin\Debug\Ufis32.pdb del c:\Ufis_Bin\Debug\Ufis32.exp
If exist c:\Ufis_Bin\Release\Ufis32.ilk del c:\Ufis_Bin\Release\Ufis32.ilk
If exist c:\Ufis_Bin\Release\Ufis32.pdb del c:\Ufis_Bin\Release\Ufis32.pdb
If exist c:\Ufis_Bin\Release\Ufis32.pdb del c:\Ufis_Bin\Release\Ufis32.exp
copy c:\ufis_bin\release\Ufis32.dll c:\ufis_bin\debug\*.*
copy c:\ufis_bin\release\Ufis32.lib c:\ufis_bin\debug\*.*
cd ..\..

ECHO Build Classlib... > con
cd _Standard_share\_Standard_ClassLib
msdev CCSClass.dsp /MAKE "CCSClass - Win32 Debug" /REBUILD
msdev CCSClass.dsp /MAKE "CCSClass - Win32 Release" /REBUILD
msdev CCSClass.dsp /MAKE "CCSClass - Win32 Dll Debug" /REBUILD
msdev CCSClass.dsp /MAKE "CCSClass - Win32 Dll Release" /REBUILD
cd ..\..

ECHO Build BcServ... > con
cd _Standard_share\_Standard_BcServ
msdev BcServ32.dsp /MAKE "BcServ - Release" /REBUILD
If exist c:\Ufis_Bin\Release\BcServ32.ilk del c:\Ufis_Bin\Release\BcServ32.ilk
If exist c:\Ufis_Bin\Release\BcServ32.pdb del c:\Ufis_Bin\Release\BcServ32.pdb
cd ..\..

ECHO Build BcProxy... > con
cd _Standard_share\_Standard_BcProxy
Rem ---remove read-only state---
attrib *.* -r
msdev BcProxy.dsp /MAKE "BcProxy - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Release\BcProxy.ilk del c:\Ufis_Bin\Release\BcProxy.ilk
If exist c:\Ufis_Bin\Release\BcProxy.pdb del c:\Ufis_Bin\Release\BcProxy.pdb
cd ..\..

ECHO Build Tabocx... > con
cd _Standard_share\_Standard_TABocx
msdev TAB.dsp /MAKE "TAB - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Release\TAB.ilk del c:\Ufis_Bin\Release\TAB.ilk
If exist c:\Ufis_Bin\Release\TAB.pdb del c:\Ufis_Bin\Release\TAB.pdb
cd ..\..

ECHO Build UfisCedaocx... > con
cd _Standard_share\_Standard_Ufiscedaocx
msdev UfisCom.dsp /MAKE "UfisCom - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Release\UfisCom.ilk del c:\Ufis_Bin\Release\UfisCom.ilk
If exist c:\Ufis_Bin\Release\UfisCom.pdb del c:\Ufis_Bin\Release\UfisCom.pdb
cd ..\..

ECHO Build UComocx... > con
cd _Standard_Share\_Standard_UCom
rem msdev UCom.dsp /MAKE "UCom - Win32 Debug" /REBUILD
msdev UCom.dsp /MAKE "UCom - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Release\UCom.ilk del c:\Ufis_Bin\Release\UCom.ilk
If exist c:\Ufis_Bin\Release\UCom.pdb del c:\Ufis_Bin\Release\UCom.pdb
cd ..\..

ECHO Build TrafficLightocx... > con
cd _Standard_share\_Standard_TrafficLight
msdev TrafficLight.dsp /MAKE "TrafficLight - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Release\TrafficLight.ilk del c:\Ufis_Bin\Release\TrafficLight.ilk
If exist c:\Ufis_Bin\Release\TrafficLight.pdb del c:\Ufis_Bin\Release\TrafficLight.pdb
cd ..\..

ECHO Build Ganttocx... > con
cd _Standard_share\_Standard_Ganttocx
msdev UGantt.dsp /MAKE "UGantt - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Release\UGantt.ilk del c:\Ufis_Bin\Release\UGantt.ilk
If exist c:\Ufis_Bin\Release\UGantt.pdb del c:\Ufis_Bin\Release\UGantt.pdb
cd ..\..

ECHO Build BCComClientocx... > con
cd _Standard_share\_Standard_BCComClient
msdev BCComClient.dsp /MAKE "BCComClient - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Release\BCComClient.ilk del c:\Ufis_Bin\Release\BCComClient.ilk
If exist c:\Ufis_Bin\Release\BCComClient.pdb del c:\Ufis_Bin\Release\BCComClient.pdb
cd ..\..

ECHO Build BCComServer... > con
cd _Standard_share\_Standard_BCComServer
Rem ---remove read-only state---
attrib *.* -r
msdev BCComServer.dsp /MAKE "BCComServer - Win32 Release" /REBUILD
copy BCComServer.tlb c:\Ufis_Bin\Release
If exist c:\Ufis_Bin\Release\BCComServer.ilk del c:\Ufis_Bin\Release\BCComServer.ilk
If exist c:\Ufis_Bin\Release\BCComServer.pdb del c:\Ufis_Bin\Release\BCComServer.pdb
cd ..\..

ECHO Build UfisAppMng... > con
cd _Standard_share\_Standard_UfisApManager
Rem ---remove read-only state---
attrib *.* -r
msdev UfisAppMng.dsp /MAKE "UfisAppMng - Win32 Release" /REBUILD
copy C:\Ufis_Bin\Release\UfisAppMng.tlb c:\ufis\system
If exist c:\Ufis_Bin\Release\UfisAppMng.ilk del c:\Ufis_Bin\Release\UfisAppMng.ilk
If exist c:\Ufis_Bin\Release\UfisAppMng.pdb del c:\Ufis_Bin\Release\UfisAppMng.pdb
cd ..\..

ECHO Build UfisApplMgr... > con
cd _Standard_share\_Standard_UfisApplMgr
Rem ---remove read-only state---
attrib *.* -r
msdev UfisApplMgr.dsp /MAKE "UfisApplMgr - Win32 Release" /REBUILD
copy C:\Ufis_Bin\Release\UFISApplMgr.tlb c:\ufis\system
If exist c:\Ufis_Bin\Release\UfisApplMgr.ilk del c:\Ufis_Bin\Release\UfisApplMgr.ilk
If exist c:\Ufis_Bin\Release\UfisApplMgr.pdb del c:\Ufis_Bin\Release\UfisApplMgr.pdb
cd ..\..

ECHO Build AatLoginocx... > con
cd _Standard_Share\_Standard_Login
rem msdev AatLogin.dsp /MAKE "AatLogin - Win32 Debug" /REBUILD
msdev AatLogin.dsp /MAKE "AatLogin - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Release\AatLogin.ilk del c:\Ufis_Bin\Release\AatLogin.ilk
If exist c:\Ufis_Bin\Release\AatLogin.pdb del c:\Ufis_Bin\Release\AatLogin.pdb
cd ..\..

ECHO Build BdpsPass... > con
cd _Standard_share\_Standard_Bdpspass
msdev Bdpspass.dsp /MAKE "Bdpspass - Win32 Debug" /REBUILD
If exist c:\Ufis_Bin\Debug\Bdpspass.ilk del c:\Ufis_Bin\Debug\Bdpspass.ilk
If exist c:\Ufis_Bin\Debug\Bdpspass.pdb del c:\Ufis_Bin\Debug\Bdpspass.pdb
cd ..\..

ECHO Build BdpsSec... > con
cd _Standard_share\_Standard_Bdpssec
msdev Bdps_sec.dsp /MAKE "Bdps_sec - Win32 Debug" /REBUILD
If exist c:\Ufis_Bin\Debug\Bdps_sec.ilk del c:\Ufis_Bin\Debug\Bdps_sec.ilk
If exist c:\Ufis_Bin\Debug\Bdps_sec.pdb del c:\Ufis_Bin\Debug\Bdps_sec.pdb
cd ..\..

ECHO Build BdpsUif... > con
cd _Standard_share\_Standard_Bdpsuif
rem msdev Bdpsuif.dsp /MAKE "Bdpsuif - Win32 Debug" /REBUILD
msdev Bdpsuif.dsp /MAKE "Bdpsuif - Win32 WatsonDebug" /REBUILD
If exist c:\Ufis_Bin\Debug\Bdpsuif.ilk del c:\Ufis_Bin\Debug\Bdpsuif.ilk
If exist c:\Ufis_Bin\Debug\Bdpsuif.pdb del c:\Ufis_Bin\Debug\Bdpsuif.pdb
cd ..\..

REM ------------------------Flight--------------------------------------

ECHO Build Fips... > con
cd _Standard_flight\_Standard_Fips
msdev FPMS.dsp /MAKE "FPMS - Win32 Debug" /REBUILD
If exist c:\Ufis_Bin\Debug\FIPS.ilk del c:\Ufis_Bin\Debug\FIPS.ilk
If exist c:\Ufis_Bin\Debug\FIPS.pdb del c:\Ufis_Bin\Debug\FIPS.pdb
cd ..\..

ECHO Build RulesFips... > con
cd _Standard_flight\_Standard_Ruleseditorflight
msdev Rules.dsp /MAKE "Rules - Win32 Debug" /REBUILD
If exist c:\Ufis_Bin\Debug\Rules.ilk del c:\Ufis_Bin\Debug\Rules.ilk
If exist c:\Ufis_Bin\Debug\Rules.pdb del c:\Ufis_Bin\Debug\Rules.pdb
cd ..\..

REM -----------------------Dutyroster-------------------------------------

ECHO Build Coverage... > con
cd _Standard_Dutyroster\_Standard_Coverage
rem msdev Coverage.dsp /MAKE "Coverage - Win32 AutoCoverageDebug" /REBUILD
msdev Coverage.dsp /MAKE "Coverage - Win32 WatsonDebug" /REBUILD
If exist c:\Ufis_Bin\Debug\AutoCoverage.ilk del c:\Ufis_Bin\Debug\AutoCoverage.ilk
If exist c:\Ufis_Bin\Debug\AutoCoverage.pdb del c:\Ufis_Bin\Debug\AutoCoverage.pdb
cd ..\..

ECHO Build Rostering... > con
cd _Standard_Dutyroster\_Standard_Rostering
msdev Rostering.dsp /MAKE "Rostering - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Release\Rostering.ilk del c:\Ufis_Bin\Release\Rostering.ilk
If exist c:\Ufis_Bin\Release\Rostering.pdb del c:\Ufis_Bin\Release\Rostering.pdb
cd ..\..

REM ---------------------------RMS---------------------------------------------------

ECHO Build StaticGroupEditor... > con
cd _Standard_RMS\_Standard_Staticgroupeditor
msdev Grp.dsp /MAKE "Grp - Win32 Debug" /REBUILD
If exist c:\Ufis_Bin\Debug\Grp.ilk del c:\Ufis_Bin\Debug\Grp.ilk
If exist c:\Ufis_Bin\Debug\Grp.pdb del c:\Ufis_Bin\Debug\Grp.pdb
cd ..\..

ECHO Build ServiceCatalog... > con
cd _Standard_RMS\_Standard_Servicecatalog
rem msdev ServiceCatalog.dsp /MAKE "ServiceCatalog - Win32 Debug" /REBUILD
msdev ServiceCatalog.dsp /MAKE "ServiceCatalog - Win32 WatsonDebug" /REBUILD
If exist c:\Ufis_Bin\Debug\ServiceCatalog.ilk del c:\Ufis_Bin\Debug\ServiceCatalog.ilk
If exist c:\Ufis_Bin\Debug\ServiceCatalog.pdb del c:\Ufis_Bin\Debug\ServiceCatalog.pdb
cd ..\..

ECHO Build RulesRms... > con
cd _Standard_RMS\_Standard_RulesEditorRms
rem msdev Regelwerk.dsp /MAKE "Regelwerk - Win32 Debug" /REBUILD
msdev Regelwerk.dsp /MAKE "Regelwerk - Win32 WatsonDebug" /REBUILD
If exist c:\Ufis_Bin\Debug\Regelwerk.ilk del c:\Ufis_Bin\Debug\Regelwerk.ilk
If exist c:\Ufis_Bin\Debug\Regelwerk.pdb del c:\Ufis_Bin\Debug\Regelwerk.pdb
cd ..\..

ECHO Build OpssPm... > con
cd _Standard_RMS\_Standard_Opsspm
msdev OpssPm.dsp /MAKE "OpssPm - Win32 Debug" /REBUILD
rem msdev OpssPm.dsp /MAKE "OpssPm - Win32 WatsonDebug" /REBUILD
If exist c:\Ufis_Bin\Debug\OpssPm.ilk del c:\Ufis_Bin\Debug\OpssPm.ilk
If exist c:\Ufis_Bin\Debug\OpssPm.pdb del c:\Ufis_Bin\Debug\OpssPm.pdb
cd ..\..

ECHO Build ProfileEditor... > con
cd _Standard_RMS\_Standard_ProfileEditor
msdev ProfileEditor.dsp /MAKE "ProfileEditor - Win32 Debug" /REBUILD
If exist c:\Ufis_Bin\Debug\ProfileEditor.ilk del c:\Ufis_Bin\Debug\ProfileEditor.ilk
If exist c:\Ufis_Bin\Debug\ProfileEditor.pdb del c:\Ufis_Bin\Debug\ProfileEditor.pdb
cd ..\..

ECHO Build PoolAllocation... > con
cd _Standard_RMS\_Standard_PoolAloc
msdev PoolAlloc.dsp /MAKE "PoolAlloc - Win32 Debug" /REBUILD
If exist c:\Ufis_Bin\Debug\PoolAlloc.ilk del c:\Ufis_Bin\Debug\PoolAlloc.ilk
If exist c:\Ufis_Bin\Debug\PoolAlloc.pdb del c:\Ufis_Bin\Debug\PoolAlloc.pdb
cd ..\..

ECHO Build WgrTool... > con
cd _Standard_Rms\_Standard_Wgrtool
msdev Wgrtool.dsp /MAKE "Wgrtool - Win32 Debug" /REBUILD
If exist c:\Ufis_Bin\Debug\Wgrtool.ilk del c:\Ufis_Bin\Debug\Wgrtool.ilk
If exist c:\Ufis_Bin\Debug\Wgrtool.pdb del c:\Ufis_Bin\Debug\Wgrtool.pdb
cd ..\..


REM Visal Bacic Clients have to be build
REM ------------------------------------

ECHO Build Telexpool... > con
cd _Standard_Flight\_Standard_Telexpool
vb6 /make Telexpool.vbp /out C:\Ufis_Bin\vbappl.log
cd ..\..

ECHO Build LoadTabViewer... > con
cd _Standard_Flight\_Standard_LoaTabViewer
vb6 /make LoadTabViewer.vbp /out C:\Ufis_Bin\vbappl.log
cd ..\..

ECHO Build ImportFlights... > con
cd _Standard_Flight\_Standard_ImportExportTool
vb6 /make ImportFlights.vbp /out C:\Ufis_Bin\vbappl.log
cd ..\..

ECHO Build ExportTool... > con
cd _Standard_Flight\_Standard_ImportExportTool\VB6_ExportTool
vb6 /make Export.vbp /out C:\Ufis_Bin\vbappl.log
cd ..\..\..

ECHO Build DaCo3Tool... > con
cd _Standard_Flight\_Standard_DaCo3Tool
vb6 /make DaCo3Tool.vbp /out C:\Ufis_Bin\vbappl.log
cd ..\..

ECHO Build RosteringPrint... > con
cd _Standard_Dutyroster\_Standard_RosteringPrint
vb6 /make RosteringPrint.vbp /out C:\Ufis_Bin\vbappl.log
cd ..\..

ECHO Build AbsencePlanning... > con
cd _Standard_Dutyroster\_Standard_AbsencePlanning
vb6 /make AbsencePlanning.vbp /out C:\Ufis_Bin\vbappl.log
cd ..\..

ECHO Build LoggingTableViewer... > con
cd _Standard_Share\_Standard_Loggingtableviewer
vb6 /make LogTab_Viewer.vbp /out C:\Ufis_Bin\vbappl.log
cd ..\..

ECHO Build HUB-Manager... > con
cd _Standard_UfisAddOn\_Standard_ConnectionManager
vb6 /make HUB_Manager.vbp /out C:\Ufis_Bin\vbappl.log
cd ..\..

ECHO Build ULD-Manager... > con
cd _Standard_UfisAddOn\_Standard_UldManager
vb6 /make ULD_Manager.vbp /out C:\Ufis_Bin\vbappl.log
cd ..\..

ECHO Build StaffInfo... > con
cd _Standard_UfisAddOn\_Standard_StaffInfo
vb6 /make StaffInfo.vbp /out C:\Ufis_Bin\vbappl.log
cd ..\..

REM C# Clients have to be build
REM ---------------------------

ECHO Build Ufis.Utils.dll... > con
cd _Standard_UfisAddOn\_Standard_Utilities
devenv Utilities.sln /Rebuild Release /project ufis.utils
cd ..\..

ECHO Build CheckListProcessing... > con
cd _Standard_UfisAddOn\_Standard_CheckListProcessing\CheckListProcessing
devenv CheckListProcessing.sln /Rebuild Release /project CheckListProcessing
cd ..\..\..

ECHO Build StatusManager... > con
cd _Standard_UfisAddOn\_Standard_StatusManager
devenv Status_Manager.sln /Rebuild Release /project Status_Manager
cd ..\..

REM ------------------Cients build from Customer-Project------------------------------------

cd ..\..\SIN\SIN_Client

ECHO Build Alerter... > con
cd SIN_Share\SIN_Alerter
msdev Alerter.dsp /MAKE "Alerter - Win32 Release" /REBUILD
If exist c:\Ufis_Bin\Release\Alerter.ilk del c:\Ufis_Bin\Release\Alerter.ilk
If exist c:\Ufis_Bin\Release\Alerter.pdb del c:\Ufis_Bin\Release\Alerter.pdb
cd ..\..

ECHO Build HAS-ConfigTool... > con
cd SIN_Share\SIN_HasConfigTool
vb6 /make HASConfigTool.vbp /out C:\Ufis_Bin\vbappl.log
cd ..\..

ECHO Build WKS Admin-Tool... > con
cd SIN_UfisAddOn\WKSAdmin
vb6 /make WKSAdmin.vbp /out C:\Ufis_Bin\vbappl.log
cd ..\..

ECHO ...Done > con

cd SIN_ClientMake 

:end
Rem END
time/T

