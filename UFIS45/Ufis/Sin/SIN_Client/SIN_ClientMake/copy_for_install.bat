rem Copies all generated files from the SCM-Structue into the Install-Shield directory.
rem 

rem set drive to J: (IS 6.2) or K: (IS 8.0) 
set drive=C:\InstallShield

rem Copy ini- and config-files
copy c:\ufis_bin\ceda.ini %drive%\sin_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\*.bmp %drive%\sin_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\grp.ini %drive%\sin_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\bdps_sec.cfg %drive%\sin_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\importtool.ini %drive%\sin_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\telexpool.ini %drive%\sin_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\rosteringprint.ulf %drive%\sin_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\loatabcfg.cfg %drive%\sin_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\loatabviewer.ini %drive%\sin_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\hasconfigtool.cfg %drive%\sin_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\daco3tool.ini %drive%\sin_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\infopc.cfg %drive%\sin_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\texttable.txt %drive%\sin_build_msi\ufis_client_appl\system\*.*

rem copy applications
copy c:\ufis_bin\debug\bdps_sec.exe %drive%\sin_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\bdpspass.exe %drive%\sin_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\bdpsuif.exe %drive%\sin_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\autocoverage.exe %drive%\sin_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\fips.exe %drive%\sin_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\grp.exe %drive%\sin_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\opsspm.exe %drive%\sin_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\regelwerk.exe %drive%\sin_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\rules.exe %drive%\sin_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\servicecatalog.exe %drive%\sin_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\poolalloc.exe %drive%\sin_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\profileeditor.exe %drive%\sin_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\debug\wgrtool.exe %drive%\sin_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\alerter.exe %drive%\sin_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\rostering.exe %drive%\sin_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\rosteringprint.exe %drive%\sin_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\telexpool.exe %drive%\sin_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\loadtabviewer.exe %drive%\sin_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\logtab_viewer.exe %drive%\sin_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\absenceplanning.exe %drive%\sin_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\importflights.exe %drive%\sin_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\hub_manager.exe %drive%\sin_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\hasconfigtool.exe %drive%\sin_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\daco3tool.exe %drive%\sin_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\uld_manager.exe %drive%\sin_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\infopc.exe %drive%\sin_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\export.exe %drive%\sin_build_msi\ufis_client_appl\applications\*.*
copy c:\ufis_bin\release\wksadmin.exe %drive%\sin_build_msi\ufis_client_appl\applications\*.*

rem copy System
copy c:\ufis_bin\release\bcproxy.exe %drive%\sin_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcproxy.tlb %drive%\sin_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bcserv32.exe %drive%\sin_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufis32.dll %drive%\sin_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisappmng.exe %drive%\sin_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisapplmgr.exe %drive%\sin_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisappmng.tlb %drive%\sin_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufisapplmgr.tlb %drive%\sin_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\tab.ocx %drive%\sin_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\tab.tlb %drive%\sin_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufiscom.ocx %drive%\sin_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ufiscom.tlb %drive%\sin_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ucom.ocx %drive%\sin_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ucom.tlb %drive%\sin_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\trafficlight.ocx %drive%\sin_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\trafficlight.tlb %drive%\sin_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ugantt.ocx %drive%\sin_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\ugantt.tlb %drive%\sin_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\aatlogin.ocx %drive%\sin_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\aatlogin.tlb %drive%\sin_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bccomserver.exe %drive%\sin_build_msi\ufis_client_appl\system\*.*
copy c:\ufis_bin\release\bccomserver.tlb %drive%\sin_build_msi\ufis_client_appl\system\*.*
rem copy c:\ufis_bin\release\bccomclient.ocx %drive%\sin_build_msi\ufis_client_appl\system\*.*
rem copy c:\ufis_bin\release\bccomclient.tlb %drive%\sin_build_msi\ufis_client_appl\system\*.*


rem Copy Help-Files
rem copy c:\ufis_bin\Help\*.chm %drive%\sin_build_msi\ufis_client_appl\help\*.*


rem copy files for Status Manager
copy c:\ufis_bin\release\*lib.dll %drive%\sin_build_sm_msi\ufis_client_appl\applications\statusmanager\*.*
copy c:\ufis_bin\release\ufis.utils.dll %drive%\sin_build_sm_msi\ufis_client_appl\applications\statusmanager\*.*
copy c:\ufis_bin\release\Status_Manager.exe %drive%\sin_build_sm_msi\ufis_client_appl\applications\statusmanager\*.*

rem copy files for Check List Processing
copy c:\ufis_bin\release\*lib.dll %drive%\sin_build_sm_msi\ufis_client_appl\applications\checklistproc\*.*
copy c:\ufis_bin\release\ufis.utils.dll %drive%\sin_build_sm_msi\ufis_client_appl\applications\checklistproc\*.*
copy c:\ufis_bin\release\CheckListProcessing.exe %drive%\sin_build_sm_msi\ufis_client_appl\applications\checklistproc\*.*
