VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{A2F31E92-C74F-11D3-A251-00500437F607}#1.0#0"; "UfisCom.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{7BDE1363-DF9A-4D96-A2A0-4E85E0191F0F}#1.0#0"; "AatLogin.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Begin VB.Form frmMain 
   Caption         =   "Lounge FIDS Admin"
   ClientHeight    =   11010
   ClientLeft      =   165
   ClientTop       =   450
   ClientWidth     =   13725
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   11010
   ScaleWidth      =   13725
   StartUpPosition =   2  'CenterScreen
   Tag             =   "s"
   Visible         =   0   'False
   Begin TabDlg.SSTab SSTab1 
      Height          =   10545
      Left            =   105
      TabIndex        =   3
      Top             =   105
      Width           =   13545
      _ExtentX        =   23892
      _ExtentY        =   18600
      _Version        =   393216
      Tab             =   1
      TabHeight       =   520
      TabCaption(0)   =   "Configuration"
      TabPicture(0)   =   "frmMain.frx":030A
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "cmdSave"
      Tab(0).Control(1)=   "cmdExit(0)"
      Tab(0).Control(2)=   "AATLoginControl1"
      Tab(0).ControlCount=   3
      TabCaption(1)   =   "Lounge FIDS Administrator"
      TabPicture(1)   =   "frmMain.frx":0326
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "frmDisplay"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "Frame2"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "frmAddFlight"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "cmdAddFlight"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).Control(4)=   "cmdQuit"
      Tab(1).Control(4).Enabled=   0   'False
      Tab(1).Control(5)=   "Command4"
      Tab(1).Control(5).Enabled=   0   'False
      Tab(1).ControlCount=   6
      TabCaption(2)   =   "Workstations"
      TabPicture(2)   =   "frmMain.frx":0342
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Frame5"
      Tab(2).Control(1)=   "Frame6"
      Tab(2).Control(2)=   "cmdExcel"
      Tab(2).Control(3)=   "dlgSave"
      Tab(2).Control(4)=   "txtSaveFile"
      Tab(2).Control(5)=   "cmdSaveFile"
      Tab(2).ControlCount=   6
      Begin VB.CommandButton Command4 
         Caption         =   "Release updates"
         Enabled         =   0   'False
         Height          =   375
         Left            =   11865
         TabIndex        =   43
         Top             =   9345
         Width           =   1335
      End
      Begin VB.CommandButton cmdQuit 
         Caption         =   "Quit"
         Height          =   375
         Left            =   11865
         TabIndex        =   42
         Top             =   9765
         Width           =   1335
      End
      Begin VB.CommandButton cmdAddFlight 
         Caption         =   "&Add Flight"
         Height          =   375
         Left            =   10500
         TabIndex        =   41
         Top             =   9345
         Width           =   1335
      End
      Begin VB.Frame frmAddFlight 
         Caption         =   "Add Flight"
         Height          =   5475
         Left            =   945
         TabIndex        =   37
         Top             =   3675
         Visible         =   0   'False
         Width           =   8205
         Begin VB.ComboBox cmbFT 
            Height          =   315
            ItemData        =   "frmMain.frx":035E
            Left            =   4560
            List            =   "frmMain.frx":0360
            TabIndex        =   46
            Text            =   "Select Lounge"
            Top             =   5040
            Width           =   1455
         End
         Begin VB.CommandButton cmdCancelAddFlight 
            Caption         =   "&Cancel"
            Enabled         =   0   'False
            Height          =   375
            Left            =   6240
            TabIndex        =   40
            Top             =   4935
            Width           =   855
         End
         Begin VB.CommandButton Command1 
            Caption         =   "&Add Flight"
            Enabled         =   0   'False
            Height          =   375
            Left            =   7080
            TabIndex        =   39
            Top             =   4935
            Width           =   855
         End
         Begin TABLib.TAB tab_AFTTABADD 
            Height          =   4500
            Left            =   240
            TabIndex        =   38
            Tag             =   "{=TABLE=}AFTTAB{=FIELDS=}URNO,FLNO,ADID,STOA,STOD,TIFD,TIFA,GTD1,ORG3,DES3,ALC2"
            Top             =   315
            Width           =   7740
            _Version        =   65536
            _ExtentX        =   13652
            _ExtentY        =   7937
            _StockProps     =   64
         End
         Begin VB.Label Label4 
            Caption         =   "Lounge :"
            Height          =   255
            Left            =   3840
            TabIndex        =   49
            Top             =   5040
            Width           =   735
         End
         Begin VB.Label Label3 
            Caption         =   "You can manually add a flight here. It does not affect the airline default settings."
            Height          =   450
            Left            =   210
            TabIndex        =   44
            Top             =   4935
            Width           =   3285
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Configured Airlines"
         Height          =   8580
         Left            =   9660
         TabIndex        =   32
         Top             =   525
         Width           =   3615
         Begin TABLib.TAB tab_ALTTAB 
            Height          =   3945
            Left            =   180
            TabIndex        =   33
            Tag             =   "{=TABLE=}ALTTAB{=FIELDS=}ALC2,ALC3,ALFN"
            Top             =   720
            Width           =   3225
            _Version        =   65536
            _ExtentX        =   5689
            _ExtentY        =   6959
            _StockProps     =   64
         End
         Begin TABLib.TAB tab_ALTTAB_FT3 
            Height          =   2625
            Left            =   180
            TabIndex        =   45
            Tag             =   "{=TABLE=}ALTTAB{=FIELDS=}ALC2,ALC3,ALFN"
            Top             =   5160
            Width           =   3225
            _Version        =   65536
            _ExtentX        =   5689
            _ExtentY        =   4630
            _StockProps     =   64
         End
         Begin VB.Label lbl2 
            Caption         =   "Lounge 3"
            Height          =   255
            Left            =   240
            TabIndex        =   48
            Top             =   4920
            Width           =   1455
         End
         Begin VB.Label lbl1 
            Caption         =   "Lounge 1"
            Height          =   255
            Left            =   240
            TabIndex        =   47
            Top             =   480
            Width           =   1455
         End
         Begin VB.Label Label1 
            Caption         =   "Read-only. Airlines are configured by UFIS Admin user in BDPS-UIF."
            Height          =   495
            Left            =   285
            TabIndex        =   34
            Top             =   7980
            Width           =   3045
         End
      End
      Begin VB.Frame frmDisplay 
         Caption         =   "Displayed Flights"
         Height          =   9810
         Left            =   210
         TabIndex        =   31
         Top             =   525
         Width           =   9165
         Begin TABLib.TAB tab_AFTTAB 
            Height          =   8700
            Left            =   210
            TabIndex        =   35
            Tag             =   "{=TABLE=}AFTTAB{=FIELDS=}URNO,FLNO,ADID,STOA,STOD,TIFD,TIFA,GTD1,ORG3,DES3,ALC2,PRFL"
            Top             =   240
            Width           =   8685
            _Version        =   65536
            _ExtentX        =   15319
            _ExtentY        =   15346
            _StockProps     =   64
         End
         Begin VB.Label Label2 
            Caption         =   $"frmMain.frx":0362
            Height          =   495
            Left            =   210
            TabIndex        =   36
            Top             =   9240
            Width           =   8610
         End
      End
      Begin VB.CommandButton cmdSaveFile 
         Caption         =   "..."
         Height          =   255
         Left            =   -66255
         TabIndex        =   30
         Top             =   10005
         Width           =   255
      End
      Begin VB.TextBox txtSaveFile 
         Enabled         =   0   'False
         Height          =   285
         Left            =   -73485
         TabIndex        =   29
         Top             =   9990
         Width           =   7140
      End
      Begin MSComDlg.CommonDialog dlgSave 
         Left            =   -70455
         Top             =   9720
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.CommandButton cmdExcel 
         Caption         =   "Export to Excel"
         Enabled         =   0   'False
         Height          =   375
         Left            =   -74880
         TabIndex        =   21
         Top             =   9945
         Width           =   1335
      End
      Begin VB.Frame Frame6 
         Caption         =   "Workstation Settings"
         Height          =   2775
         Left            =   -74880
         TabIndex        =   6
         Top             =   7065
         Width           =   13215
         Begin VB.CommandButton cmdCancel 
            Caption         =   "&Cancel"
            Enabled         =   0   'False
            Height          =   375
            Left            =   11640
            TabIndex        =   24
            Top             =   480
            Width           =   1335
         End
         Begin VB.TextBox txtTADR 
            BackColor       =   &H0000FFFF&
            Height          =   645
            Left            =   1440
            MaxLength       =   255
            TabIndex        =   19
            Top             =   1680
            Width           =   9975
         End
         Begin VB.TextBox txtREMA 
            BackColor       =   &H00FFFFFF&
            Height          =   645
            Left            =   5520
            MaxLength       =   250
            TabIndex        =   18
            Top             =   840
            Width           =   5895
         End
         Begin VB.TextBox txtLOCT 
            BackColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   1440
            MaxLength       =   50
            TabIndex        =   16
            Top             =   840
            Width           =   2895
         End
         Begin VB.TextBox txtDEPT 
            BackColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   1440
            MaxLength       =   50
            TabIndex        =   15
            Top             =   1200
            Width           =   2895
         End
         Begin VB.CommandButton cmdNew 
            Caption         =   "&New"
            Height          =   375
            Left            =   11640
            TabIndex        =   11
            Top             =   1920
            Width           =   1335
         End
         Begin VB.CommandButton cmdSaveWKST 
            Caption         =   "&Save"
            Enabled         =   0   'False
            Height          =   375
            Left            =   11640
            TabIndex        =   10
            Top             =   1440
            Width           =   1335
         End
         Begin VB.CommandButton cmdDelete 
            Caption         =   "&Delete"
            Enabled         =   0   'False
            Height          =   375
            Left            =   11640
            TabIndex        =   9
            Top             =   945
            Width           =   1335
         End
         Begin VB.TextBox txtWKST 
            BackColor       =   &H0000FFFF&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1440
            MaxLength       =   32
            TabIndex        =   8
            Top             =   480
            Width           =   2895
         End
         Begin VB.Label Label27 
            Caption         =   "Addresses"
            Height          =   255
            Left            =   360
            TabIndex        =   20
            Top             =   1680
            Width           =   1215
         End
         Begin VB.Label Label26 
            Caption         =   "Remark"
            Height          =   255
            Left            =   4680
            TabIndex        =   17
            Top             =   840
            Width           =   1215
         End
         Begin VB.Label Label25 
            Caption         =   "Location"
            Height          =   255
            Left            =   360
            TabIndex        =   14
            Top             =   840
            Width           =   1215
         End
         Begin VB.Label Label24 
            Caption         =   "Department"
            Height          =   255
            Left            =   360
            TabIndex        =   13
            Top             =   1200
            Width           =   1215
         End
         Begin VB.Label Label20 
            Caption         =   "Workstation"
            Height          =   255
            Left            =   360
            TabIndex        =   12
            Top             =   480
            Width           =   1215
         End
      End
      Begin AATLOGINLib.AatLogin AATLoginControl1 
         Height          =   1095
         Left            =   -74625
         TabIndex        =   4
         Top             =   390
         Visible         =   0   'False
         Width           =   1575
         _Version        =   65536
         _ExtentX        =   2778
         _ExtentY        =   1931
         _StockProps     =   0
      End
      Begin VB.CommandButton cmdExit 
         Caption         =   "Exit"
         Height          =   405
         Index           =   0
         Left            =   -69795
         TabIndex        =   2
         Top             =   10545
         Width           =   1440
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "Save"
         Height          =   405
         Left            =   -71415
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   10545
         Width           =   1440
      End
      Begin VB.Frame Frame5 
         Caption         =   "List of workstations"
         Height          =   6495
         Left            =   -74880
         TabIndex        =   5
         Top             =   480
         Width           =   13215
         Begin VB.CommandButton cmdResetFilter 
            Caption         =   "&Reset Filter"
            Height          =   375
            Left            =   5400
            TabIndex        =   28
            Top             =   6000
            Width           =   1335
         End
         Begin VB.CommandButton cmdFilter 
            Caption         =   "&Apply Filter"
            Height          =   375
            Left            =   3960
            TabIndex        =   26
            Top             =   6000
            Width           =   1335
         End
         Begin VB.TextBox txtFilter 
            Height          =   285
            Left            =   1440
            TabIndex        =   25
            Top             =   6000
            Width           =   2415
         End
         Begin VB.Frame frmRecordset 
            BorderStyle     =   0  'None
            Height          =   735
            Left            =   1200
            TabIndex        =   22
            Top             =   2760
            Visible         =   0   'False
            Width           =   10815
            Begin VB.Label Label28 
               BackColor       =   &H00FFFFFF&
               BackStyle       =   0  'Transparent
               Caption         =   "Record changed."
               Height          =   255
               Left            =   4680
               TabIndex        =   23
               Top             =   240
               Width           =   1695
            End
         End
         Begin TABLib.TAB tab_WKSTAB 
            Height          =   5535
            Left            =   105
            TabIndex        =   7
            Tag             =   "{=TABLE=}WKSTAB{=FIELDS=}WKST,TADR,DEPT,LOCT,REMA,URNO,CDAT,USEC,LSTU,USEU"
            Top             =   345
            Width           =   12885
            _Version        =   65536
            _ExtentX        =   22728
            _ExtentY        =   9763
            _StockProps     =   64
         End
         Begin VB.Label Label29 
            Caption         =   "Workstation Filter"
            Height          =   255
            Left            =   120
            TabIndex        =   27
            Top             =   6000
            Width           =   1935
         End
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   300
      Left            =   0
      TabIndex        =   0
      Top             =   10710
      Width           =   13725
      _ExtentX        =   24209
      _ExtentY        =   529
      SimpleText      =   "Ready."
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   18547
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            TextSave        =   "04-Jun-08"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   2
            TextSave        =   "3:15 AM"
         EndProperty
      EndProperty
   End
   Begin UFISCOMLib.UfisCom UfisCom1 
      Left            =   0
      Top             =   0
      _Version        =   65536
      _ExtentX        =   1111
      _ExtentY        =   1164
      _StockProps     =   0
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'#######################################################################
'# Main form
'# WKSADMIN TOOL
'#  based on HASConfigTool Sources
'#
'#  adapted by SHA 20060324
'#
'#######################################################################


Option Explicit

Private gSaved As Boolean
Private gNewRecord As Boolean
Private gOrgWKST As String
Private gOrgDEPT As String
Private gOrgREMA As String
Private gOrgLOCT As String
Private gOrgTADR As String
Private gOrgURNO As String
Private gFilter As String
Private gExcelFile As String

'#######################################################################
'### To show the flights which will not be displayed in FIDS
'#######################################################################

Private Sub cmdAddFlight_Click()
    frmAddFlight.Visible = True
    
    Dim fromDate As String
    Dim toDate As String
    Dim strWhere As String
    
    'Preparing date, according to current date&time-12 & current date&time+12
    fromDate = DateAdd("h", -12, Now)
    fromDate = Format(fromDate, "yyyymmddhhmmss")
    'MsgBox ("from Date: " & fromDate)
    
    toDate = DateAdd("h", 12, Now)
    toDate = Format(toDate, "yyyymmddhhmmss")
    'MsgBox ("To Date: " & toDate)
    
    'To show flights .. preparing query to AFTTABADD
    strWhere = "WHERE PRFL=' ' AND ADID='D' AND OFBL=' ' AND STOD BETWEEN '" & fromDate & " ' AND '" & toDate & "' AND URNO>0"
    'MsgBox ("Query" + strWhere)
    
    tab_AFTTABADD.ResetContent
    tab_AFTTABADD.Refresh
    frmData.LoadData tab_AFTTABADD, strWhere
    tab_AFTTABADD.HeaderFontSize = 14
    tab_AFTTABADD.FontSize = 14
    If tab_AFTTABADD.GetLineCount = 0 Then MsgBox "No flights which are not to be displayed in FIDS found."
    
End Sub

'#######################################################################
'### When user clicks cancel button in Addflight window
'#######################################################################

Private Sub cmdCancelAddFlight_Click()
    frmAddFlight.Visible = False
End Sub




'#######################################################################
'### QUIT APPLICATION
'#######################################################################
Private Sub cmdQuit_Click()
    If gSaved = False Then
        Dim ilRet As Integer
        ilRet = MsgBox("Changes not saved. Quit?", vbYesNo)
        If ilRet = vbNo Then
            Exit Sub
        End If
    End If
    
    Unload frmData
    End

End Sub



'#######################################################################
'### This function works at Form load & Refreshing the form.
'#######################################################################

Public Sub RefreshView()
    
    'Initiliazing required DB Tabs
    InitTabs
    
    Dim toDate As String
    Dim fromDate As String
    Dim strWhere As String
    
    
    SSTab1.TabVisible(0) = False
    SSTab1.TabVisible(2) = False
    
    'To show airline codes
    strWhere = "WHERE ADMD='FT1'"
    frmData.LoadData tab_ALTTAB, strWhere
    tab_ALTTAB.AutoSizeColumns
    tab_ALTTAB.Sort 0, True, False
    
    'kkh on 26/05/2008 RFC 423
    'To show airline codes
    strWhere = "WHERE ADMD='FT3'"
    frmData.LoadData tab_ALTTAB_FT3, strWhere
    tab_ALTTAB_FT3.AutoSizeColumns
    tab_ALTTAB_FT3.Sort 0, True, False
    
    'Preparing date, according to current date&time-12 & current date&time+12
    fromDate = DateAdd("h", -12, Now)
    fromDate = Format(fromDate, "yyyymmddhhmmss")
    'MsgBox ("from Date: " & fromDate)
    
    toDate = DateAdd("h", 12, Now)
    toDate = Format(toDate, "yyyymmddhhmmss")
    'MsgBox ("To Date: " & toDate)
        
    'To show flights .. preparing query to AFTTAB
    'kkh on 26/05/2008 RFC 423
    
    'strWhere = "WHERE PRFL='1' AND ADID='D' AND STOD BETWEEN '" & fromDate & " ' AND '" & toDate & "' AND URNO>0"
    strWhere = "WHERE PRFL='1' AND ADID='D' AND STOD BETWEEN '" & fromDate & " ' AND '" & toDate & "' AND URNO>0 OR PRFL='3' AND ADID='D' AND STOD BETWEEN '" & fromDate & " ' AND '" & toDate & "' AND URNO>0"
    
    tab_AFTTAB.ResetContent
    tab_AFTTAB.Refresh
    frmData.LoadData tab_AFTTAB, strWhere
    tab_AFTTAB.HeaderFontSize = 14
    tab_AFTTAB.FontSize = 14
    If tab_AFTTAB.GetLineCount = 0 Then MsgBox "No flights found."
    
End Sub

'#######################################################################
'###
'#######################################################################

Private Sub Form_Unload(Cancel As Integer)
Unload frmData
End Sub


'#######################################################################
'### When user double clicks the flight, it shouldnt be displayed in FIDS screens.
'#######################################################################

Private Sub tab_AFTTAB_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)

    Dim strFields As String
    Dim strData As String
    Dim strWhere As String
    Dim l As Long
    Dim urno As String
    
    l = tab_AFTTAB.GetCurrentSelected
    
    If l <> -1 Then
        strFields = "PRFL"
        strData = " "
        urno = tab_AFTTAB.GetColumnValues(l, 0)
        strWhere = "WHERE URNO='" & urno & "'"
        If frmMain.UfisCom1.CallServer("URT", "AFTTAB", strFields, strData, strWhere, "360") <> 0 Then
            MsgBox frmMain.UfisCom1.LastErrorMessage, vbCritical
            Exit Sub
        End If
    End If
    
    RefreshView
    
End Sub

'#######################################################################
'### When user clicks the flight, then it has to be displayed in FIDS screen
'#######################################################################

Private Sub tab_AFTTABADD_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)

    Dim strFields As String
    Dim strData As String
    Dim strWhere As String
    Dim l As Long
    Dim urno As String
    
    'kkh on 26/05/2008 RFC 423
    
    Dim strFT As String
    strFT = cmbFT.ListIndex
    
    Select Case strFT
        Case "0":  strData = "1" 'Lounge @ Terminal 1
        Case "1":  strData = "3" 'Lounge @ Terminal 3
        Case Else
            MsgBox "Please select Lounge"
            cmbFT.SetFocus
            'tab_AFTTABADD
            cmdAddFlight_Click
            Exit Sub
    End Select
    
    l = tab_AFTTABADD.GetCurrentSelected
    
    If l <> -1 Then
        strFields = "PRFL"
        'kkh on 26/05/2008 RFC 423
        'strData = "1"
        urno = tab_AFTTABADD.GetColumnValues(l, 0)
        strWhere = "WHERE URNO='" & urno & "'"
        If frmMain.UfisCom1.CallServer("URT", "AFTTAB", strFields, strData, strWhere, "360") <> 0 Then
            MsgBox frmMain.UfisCom1.LastErrorMessage, vbCritical
            Exit Sub
        End If
    End If
    
    cmdCancelAddFlight_Click
    RefreshView

End Sub

'#######################################################################
'###
'#######################################################################

Private Sub tab_WKSTAB_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)

    
End Sub

'#######################################################################
'###
'#######################################################################

Sub CheckChanges()



End Sub

'#######################################################################
'### INIT DB TABLES
'#######################################################################
Sub InitTabs()
    
    frmData.InitTabGeneral tab_ALTTAB
    frmData.InitTabForCedaConnection tab_ALTTAB
    tab_ALTTAB.AutoSizeColumns
    
    'kkh on 26/05/2008 RFC 423
    frmData.InitTabGeneral tab_ALTTAB_FT3
    frmData.InitTabForCedaConnection tab_ALTTAB_FT3
    tab_ALTTAB_FT3.AutoSizeColumns
    
    frmData.InitTabGeneral tab_AFTTAB
    frmData.InitTabForCedaConnection tab_AFTTAB
    'kkh on 26/05/2008 RFC 423 add lounge
    tab_AFTTAB.HeaderString = "URNO,Flight,A/D,STOA,STOD,Best TD,Best TA,Gate,Origin,Destination,Airline Code,Lounge"
    tab_AFTTAB.HeaderLengthString = "0,50,20,110,110,110,110,25,25,25,30,25"
    tab_AFTTAB.HeaderFontSize = 14
    tab_AFTTAB.FontSize = 14
    tab_AFTTAB.Refresh
    
    frmData.InitTabGeneral tab_AFTTABADD
    frmData.InitTabForCedaConnection tab_AFTTABADD
    'kkh on 26/05/2008 RFC 423 add lounge
    tab_AFTTABADD.HeaderString = "URNO,Flight,A/D,STOA,STOD,Best TD,Best TA,Gate,Origin,Destination,Airline Code,Lounge"
    tab_AFTTABADD.HeaderLengthString = "0,50,20,110,110,110,110,25,25,25,30,25"
    tab_AFTTABADD.HeaderFontSize = 14
    tab_AFTTABADD.FontSize = 14
    tab_AFTTABADD.Refresh
    
    'kkh on 26/05/2008 RFC 423
    cmbFT.Clear
    cmbFT.AddItem "FT1", 0
    cmbFT.AddItem "FT3", 1
    
End Sub
