using System;
using System.Collections.Generic;
using System.Text;
using IBM.WMQ;
using iTrekXML;
//using System.Timers;
using System.Collections;
//using System.Configuration;
using iTrekUtils;
using iTrekWMQ;
using NUnit.Framework;
using System.Security.Policy;   //for  evidence  object

namespace iTrekWMQTests
{
    [TestFixture]
    public class iTrekWMQTests
    {
        //currently Ok2LWMQ tests only
        iTXMLcl iTXML;
        iTrekWMQClass mqObj;
        iTXMLPathscl iTPaths;
        //[SetUp]
        public iTrekWMQTests()
        {
            iTPaths = new iTXMLPathscl();

            //As we want to add multiple assemblies to the NUnit project
            //at PushServ directory level 
            //we need to point NUNit at the relevant ApplicationBase
            //when instantiating classes
            //This one is for iTrekOk2L (points to config xml file)

            //Create  the  application  domain  setup  information.
            AppDomainSetup domaininfo = new AppDomainSetup();
            domaininfo.ApplicationBase = System.Environment.CurrentDirectory;
            //Create  evidence  for  new  appdomain.
            Evidence adevidence = AppDomain.CurrentDomain.Evidence;
            //  Create  the  new  application  domain  using  setup  information.
            AppDomain domain = AppDomain.CreateDomain("iTrekOk2L", adevidence, domaininfo);

            iTXML = new iTXMLcl(domain.SetupInformation.ApplicationBase);
            //mqObj = new iTrekOk2L.WMQClass(iTPaths.GetTOPUSHOK2LOADQueueName());
            //mqObj = new iTrekWMQClass(iTPaths.GetTOPUSHOK2LOADQueueName());
            mqObj = new iTrekWMQClass(iTPaths.GetTOPUSHOK2LOADQueueName());
        }

        //TODO: Coverage high
        [Test]
        public void PutGetMsgOnTOPUSHOK2LOADQueue()
        {
            mqObj.putTheMessageIntoQueue("Test message");
            Assert.AreEqual("Test message", mqObj.GetMessageFromTheQueue());
        }

        //TODO: Coverage high
        [Test]
        public void PurgeMessageQueue()
        {
            mqObj.putTheMessageIntoQueue("Test message");
            mqObj.PurgeQueue();
            Assert.AreEqual("MQRC_NO_MSG_AVAILABLE", mqObj.GetMessageFromTheQueue());
        }

        //TODO: Coverage high
        [Test]
        public void Put3MsgOnQueueGet3MsgBackInSameOrder()
        { 
            mqObj.putTheMessageIntoQueue("First Test message");
            mqObj.putTheMessageIntoQueue("Second Test message");
            mqObj.putTheMessageIntoQueue("Third Test message");
            Assert.AreEqual("First Test message", mqObj.GetMessageFromTheQueue());
            Assert.AreEqual("Second Test message", mqObj.GetMessageFromTheQueue());
            Assert.AreEqual("Third Test message", mqObj.GetMessageFromTheQueue());
        }

        //TODO: Coverage medium
        //Only testing that message going onto queue
        //not that appropriate result being returned in 
        //TO.ITREK.READ_RES
        [Test]
        public void PutGetMessageIntoFROMITREKAUTHQueueWithSessionID()
        {
            iTrekWMQClass FROMITREKAUTHQueue = new iTrekWMQClass(iTPaths.GetFROMITREKAUTHQueueName());
            FROMITREKAUTHQueue.putMessageIntoFROMITREKAUTHQueueWithSessionID("Authenticated", "dt2xbz23qk4vbs45sfvqhljh");
            Assert.AreEqual("<Msg><SessionId>dt2xbz23qk4vbs45sfvqhljh</SessionId><CurrentMsg>Authenticated</CurrentMsg></Msg>", FROMITREKAUTHQueue.GetMessageFromTheQueue());
        }

        //TODO: Coverage medium
        //Only testing that message going onto queue
        [Test]
        public void PutGetMessageFromTOITREKREAD_RESQueueTest()
        {
            iTrekWMQClass TOITREKREAD_RESQueue = new iTrekWMQClass(iTPaths.GetTOITREKREAD_RESQueueName());
            TOITREKREAD_RESQueue.PurgeQueue();
            TOITREKREAD_RESQueue.putTheMessageIntoQueue("Test message from GetMessageFromTOITREKREAD_RESQueue");
            Assert.AreEqual("Test message from GetMessageFromTOITREKREAD_RESQueue", TOITREKREAD_RESQueue.GetMessageFromTheQueue());
        }

        //TODO: Coverage medium
        //Only testing that message going onto queue
        [Test]
        public void PutGetMessageFROMITREKUPDATEQueueTest()
        {
            iTrekWMQClass FROMITREKUPDATEQueue = new iTrekWMQClass(iTPaths.GetFROMITREKUPDATEQueueName());
            FROMITREKUPDATEQueue.PurgeQueue();
            FROMITREKUPDATEQueue.putTheMessageIntoQueue("Test message from PutGetMessageFROMITREKUPDATEQueue");
            Assert.AreEqual("Test message from PutGetMessageFROMITREKUPDATEQueue", FROMITREKUPDATEQueue.GetMessageFromTheQueue());
        }

        //TODO: Coverage medium
        //Only testing that message going onto queue
        [Test]
        public void PutGetMessageTOITREKREAD_AFTQueueTest()
        {
            iTrekWMQClass TOITREKREAD_AFTQueue = new iTrekWMQClass(iTPaths.GetTOITREKREAD_AFTQueueName());
            TOITREKREAD_AFTQueue.PurgeQueue();
            TOITREKREAD_AFTQueue.putTheMessageIntoQueue("Test message from PutGetMessageTOITREKREAD_AFTQueue");
            Assert.AreEqual("Test message from PutGetMessageTOITREKREAD_AFTQueue", TOITREKREAD_AFTQueue.GetMessageFromTheQueue());
        }

        //TODO: Coverage medium
        //Only testing that message going onto queue
        [Test]
        public void PutGetMessageTOITREKREAD_JOBQueueTest()
        {
            iTrekWMQClass TOITREKREAD_JOBQueue = new iTrekWMQClass(iTPaths.GetTOITREKREAD_JOBQueueName());
            TOITREKREAD_JOBQueue.PurgeQueue();
            TOITREKREAD_JOBQueue.putTheMessageIntoQueue("Test message from PutGetMessageTOITREKREAD_JOBQueueTest");
            Assert.AreEqual("Test message from PutGetMessageTOITREKREAD_JOBQueueTest", TOITREKREAD_JOBQueue.GetMessageFromTheQueue());
        }

        //TODO: Coverage medium
        //Only testing that message going onto queue
        //[Test]
        //public void PutGetMessageFROMITREKAUTHQueueTest()
        //{
        //    iTrekWMQClass FROMITREKAUTHQueue = new iTrekWMQClass(iTPaths.GetFROMITREKAUTHQueueName());
        //    FROMITREKAUTHQueue.PurgeQueue();
        //    FROMITREKAUTHQueue.putTheMessageIntoQueue("Test message from PutGetMessageFROMITREKAUTHQueue");
        //    Assert.AreEqual("Test message from PutGetMessageFROMITREKAUTHQueue", FROMITREKAUTHQueue.GetMessageFromTheQueue());
        //}

        //TODO: Coverage low
        //Only testing that message going onto queue
        [Test]
        public void PutGetMessageTOITREKAUTHQueueTest()
        {
            iTrekWMQClass TOITREKAUTHQueue = new iTrekWMQClass(iTPaths.GetTOITREKAUTHQueueName());
            TOITREKAUTHQueue.PurgeQueue();
            TOITREKAUTHQueue.putTheMessageIntoQueue("Test message from PutGetMessageTOITREKAUTHQueue");
            Assert.AreEqual("Test message from PutGetMessageTOITREKAUTHQueue", TOITREKAUTHQueue.GetMessageFromTheQueue());
        }

        //TODO: Coverage medium
        //Only testing that message going onto queue
        [Test]
        public void PutGetMessageFROMITREKSELECTQueueTest()
        {
            iTrekWMQClass FROMITREKSELECTQueue = new iTrekWMQClass(iTPaths.GetFROMITREKSELECTQueueName());
            FROMITREKSELECTQueue.PurgeQueue();
            FROMITREKSELECTQueue.putTheMessageIntoQueue("Test message from PutGetMessageFROMITREKSELECTQueue");
            Assert.AreEqual("Test message from PutGetMessageFROMITREKSELECTQueue", FROMITREKSELECTQueue.GetMessageFromTheQueue());
        } 

    }
}



namespace iTrekWMQ
{
    /// <summary>
    /// Manages iTrek queues
    /// </summary>
    public class iTrekWMQClass
    {
        MQQueueManager queueManager;
        MQQueue queue;
        MQMessage queueMessage;
        MQPutMessageOptions queuePutMessageOptions;
        MQGetMessageOptions queueGetMessageOptions;

        static string QueueName;
        static string QueueManagerName;
       //static string ChannelInfo;

        string channelName;
        string transportType;
        string connectionName;

        string message;
        byte[] MessageID;

        iTXMLPathscl iTPaths;

        public iTrekWMQClass(string QueueNm)
        {

            iTPaths = new iTXMLPathscl();
            //TODO: Don't use queue name at this level
            //can be instatiated twice and change relevant queue
            QueueName = QueueNm;
            QueueManagerName = iTPaths.GetQueueManagerName();

            channelName = iTPaths.GetChannelName();
            transportType = iTPaths.GetChannelTransportType();

            connectionName = iTPaths.GetChannelServer();
            connectionName += iTPaths.GetChannelPortNumber();

           
            string serverAndPort = iTPaths.GetChannelServer();
            serverAndPort += iTPaths.GetChannelPortNumber();
            queueManager = new MQQueueManager(QueueManagerName, iTPaths.GetChannelName(), serverAndPort);
        }

        public void PurgeQueue()
        {
            while (GetMessageFromTheQueue() != "MQRC_NO_MSG_AVAILABLE")
            {
                GetMessageFromTheQueue();
            }
        }

        //only used by console application
        public string putTheMessageIntoQueue()
        {
            try
            {
                queue = queueManager.AccessQueue(QueueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);

                return "Enter the message to put in MQSeries server:";
               //message = System.Console.ReadLine();
                int msgLen = message.Length;

                while (msgLen == 0)
                {
                    if (msgLen == 0)
                        return "Please reenter the message:";
                    message = System.Console.ReadLine();
                    msgLen = message.Length;
                }

                queueMessage = new MQMessage();
                queueMessage.WriteString(message);
                queueMessage.Format = MQC.MQFMT_STRING;
                queuePutMessageOptions = new MQPutMessageOptions();

                //putting the message into the queue
                queue.Put(queueMessage, queuePutMessageOptions);
                MessageID = queueMessage.MessageId;

                return "Success fully entered the message into the queue";
            }
            catch (Exception mqexp)
            {
                return "MQSeries Exception: " + mqexp.Message;
            }
        }

        public string GetMessageFromTheQueue()
        {

            queue = queueManager.AccessQueue(QueueName, MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
            queueMessage = new MQMessage();
            queueMessage.Format = MQC.MQFMT_STRING;
            if(MessageID != null)
             queueMessage.MessageId = MessageID;
            
            queueGetMessageOptions = new MQGetMessageOptions();

            try
            {
                queue.Get(queueMessage, queueGetMessageOptions);
                // Received Message.
                //return "MessageID:" + MessageID + " Message:" + queueMessage.ReadString(queueMessage.MessageLength);
                return queueMessage.ReadString(queueMessage.MessageLength);
            }
            catch (Exception MQExp)
            {
                //HACK: Need to stop using exception to control queue purge
                return MQExp.Message;
            }
        }

        public string GetMessageFromTheQueueAnyOrder()
        {
            channelName = iTPaths.GetChannelName();
            transportType = iTPaths.GetChannelTransportType();

            connectionName = iTPaths.GetChannelServer();
            connectionName += iTPaths.GetChannelPortNumber();

            queueManager = new MQQueueManager(QueueManagerName, channelName, connectionName);
            //In office
            //queueManager = new MQQueueManager(QueueManagerName, "CHANNEL1", "192.168.1.8");
            //At home
            //queueManager = new MQQueueManager(QueueManagerName, "CHANNEL1", "http://www.ufissin.homelinux.net/");

            queue = queueManager.AccessQueue(QueueName, MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
            queueMessage = new MQMessage();
            queueMessage.Format = MQC.MQFMT_STRING;
            //queueMessage.MessageId = MessageID;
            queueGetMessageOptions = new MQGetMessageOptions();

            try
            {
                queue.Get(queueMessage, queueGetMessageOptions);

                // Received Message.
                //System.Console.WriteLine("Received Message: {0}", queueMessage.ReadString(queueMessage.MessageLength));
                return queueMessage.ReadString(queueMessage.MessageLength);

            }
            catch (Exception MQExp)
            {
                // report the error
                //System.Console.WriteLine("MQQueue::Get ended with " + MQExp.Message);
                return "MQQueue::Get ended with " + MQExp.Message;
            }
        }

        public void GetConsoleMessageFromTheQueueAnyOrder()
        {

            channelName = iTPaths.GetChannelName();
            transportType = iTPaths.GetChannelTransportType();

            connectionName = iTPaths.GetChannelServer();
            connectionName += iTPaths.GetChannelPortNumber();

            //QueueName = iTPaths.GetTOPUSHOK2LOADQueueName();
            //QueueManagerName = "venus.queue.manager";
            //ChannelInfo = "CHANNEL1/TCP/192.168.1.8";

            queueManager = new MQQueueManager(QueueManagerName, channelName, connectionName);
            //In office
            //queueManager = new MQQueueManager(QueueManagerName, "CHANNEL1", "192.168.1.8");
            //At home
            //queueManager = new MQQueueManager(QueueManagerName, "CHANNEL1", "http://www.ufissin.homelinux.net/");

            queue = queueManager.AccessQueue(QueueName, MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
            queueMessage = new MQMessage();
            queueMessage.Format = MQC.MQFMT_STRING;
            //queueMessage.MessageId = MessageID;
            queueGetMessageOptions = new MQGetMessageOptions();

            try
            {
                queue.Get(queueMessage, queueGetMessageOptions);

                // Received Message.
                System.Console.WriteLine("Received Message: {0}", queueMessage.ReadString(queueMessage.MessageLength));
                //return queueMessage.ReadString(queueMessage.MessageLength);

            }
            catch (Exception MQExp)
            {
                // report the error
                //System.Console.WriteLine("MQQueue::Get ended with " + MQExp.Message);
                System.Console.WriteLine("MQQueue::Get ended with " + MQExp.Message);
            }
        }

        public string GetMessageFromTheQueueReturnString()
        {
            channelName = iTPaths.GetChannelName();
            transportType = iTPaths.GetChannelTransportType();

            connectionName = iTPaths.GetChannelServer();
            connectionName += iTPaths.GetChannelPortNumber();

            queueManager = new MQQueueManager(QueueManagerName, channelName, connectionName);

            queue = queueManager.AccessQueue(QueueName, MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
            queueMessage = new MQMessage();
            queueMessage.Format = MQC.MQFMT_STRING;
            //queueMessage.MessageId = MessageID;
            queueGetMessageOptions = new MQGetMessageOptions();

            try
            {
                queue.Get(queueMessage, queueGetMessageOptions);

                // Received Message.
                return queueMessage.ReadString(queueMessage.MessageLength);
                //System.Console.WriteLine("Received Message: {0}", queueMessage.MessageId[0].ToString());

            }
            catch (MQException MQExp)
            {
                // report the error
                return MQExp.Message;
            }
        }

        public void putXMLMessageIntoQueue()
        {
            try
            {
                channelName = iTPaths.GetChannelName();
                transportType = iTPaths.GetChannelTransportType();

                connectionName = iTPaths.GetChannelServer();
                connectionName += iTPaths.GetChannelPortNumber();

                queueManager = new MQQueueManager(QueueManagerName, channelName, connectionName);
                queue = queueManager.AccessQueue(QueueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);


                message = iTUtils.ReadFromFile(@"C:\temp\tests\bigfile.xml");

                int msgLen = message.Length;

                while (msgLen == 0)
                {
                    if (msgLen == 0)
                        Console.WriteLine("Please reenter the message:");
                    message = System.Console.ReadLine();
                    msgLen = message.Length;
                }

                queueMessage = new MQMessage();
                queueMessage.WriteString(message);
                queueMessage.Format = MQC.MQFMT_STRING;
                queuePutMessageOptions = new MQPutMessageOptions();

                //putting the message into the queue
                queue.Put(queueMessage, queuePutMessageOptions);
                MessageID = queueMessage.MessageId;

                Console.WriteLine("Success fully entered the message into the queue");
            }
            catch (MQException mqexp)
            {
                Console.WriteLine("MQSeries Exception: " + mqexp.Message);
            }
        }

        public string putTheMessageIntoQueue(string message)
        {
            try
            {
                queueManager = new MQQueueManager(QueueManagerName, channelName, connectionName);
                queue = queueManager.AccessQueue(QueueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);


                int msgLen = message.Length;

                while (msgLen == 0)
                {
                    if (msgLen == 0)
                        return "Add a message parameter";
                    msgLen = message.Length;
                }

                queueMessage = new MQMessage();
                queueMessage.WriteString(message);
                queueMessage.Format = MQC.MQFMT_STRING;
                queuePutMessageOptions = new MQPutMessageOptions();

                //putting the message into the queue
                queue.Put(queueMessage, queuePutMessageOptions);
                //add the new message id to the next messageid byte array element
                //to retain earlier messages message ids
                MessageID[MessageID.Length + 1] = queueMessage.MessageId[MessageID.Length];

                return MessageID[MessageID.Length + 1].ToString();

                //return queueMessage.MessageId.ToString();
            }
            catch (Exception mqexp)
            {
                return "MQSeries Exception: " + mqexp.Message;
            }
        }

        public string putConsoleMessageIntoQueue(string message)
        {
            try
            {
                queueManager = new MQQueueManager(QueueManagerName, channelName, connectionName);
                queue = queueManager.AccessQueue(QueueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);

                //Console.WriteLine("Enter the message to put in MQSeries server:");
                //message = System.Console.ReadLine();
                int msgLen = message.Length;

                while (msgLen == 0)
                {
                    if (msgLen == 0)
                        return "Add a message parameter";
                    msgLen = message.Length;
                }

                queueMessage = new MQMessage();
                queueMessage.WriteString(message);
                queueMessage.Format = MQC.MQFMT_STRING;
                queuePutMessageOptions = new MQPutMessageOptions();

                //putting the message into the queue
                queue.Put(queueMessage, queuePutMessageOptions);
                //add the new message id to the next messageid byte array element
                //to retain earlier messages message ids
                //MessageID[MessageID.Length +1] = queueMessage.MessageId[MessageID.Length];

                //return MessageID[MessageID.Length + 1].ToString();
                MessageID = queueMessage.MessageId;
                //return MessageID[MessageID.Length - 1].ToString(); ;
                //return queueMessage.MessageId.ToString();
                return message;
            }
            catch (Exception mqexp)
            {
                return "MQSeries Exception: " + mqexp.Message;
            }
        }

        public void putMessageIntoQueueForMultipleMessages(string msg)
        {
            try
            {
                
                channelName = iTPaths.GetChannelName();
                transportType = iTPaths.GetChannelTransportType();

                connectionName = iTPaths.GetChannelServer();
                connectionName += iTPaths.GetChannelPortNumber();

                queueManager = new MQQueueManager(QueueManagerName, channelName, connectionName);
                queue = queueManager.AccessQueue(QueueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);
               
                message = msg;

                queueMessage = new MQMessage();
                queueMessage.WriteString(message);
                queueMessage.Format = MQC.MQFMT_STRING;
                queuePutMessageOptions = new MQPutMessageOptions();

                //putting the message into the queue
                queue.Put(queueMessage, queuePutMessageOptions);
                MessageID = queueMessage.MessageId;

                Console.WriteLine("Success fully entered the message into the queue");
            }
            catch (MQException mqexp)
            {
                Console.WriteLine("MQSeries Exception: " + mqexp.Message);
            }
        }

        public string putMessageIntoFROMITREKAUTHQueueWithSessionID(string message, string sessID)
        {
            try
            {
                QueueName = iTPaths.GetFROMITREKAUTHQueueName();
                queueManager = new MQQueueManager(QueueManagerName, channelName, connectionName);
                queue = queueManager.AccessQueue(QueueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);

                //message = "<Msg><SessionId>" + sessID + "</SessionId><CurrentMsg>" + message + "</CurrentMsg></Msg>";
                message = "<AUTH><PENO>" + iTPaths.GetTestPENO() + "</PENO><PASS>" + iTPaths.GetTestPassword() + "</PASS><SESSIONID>" + iTPaths.GetTestSessionId() + "</SESSIONID></AUTH>";
                int msgLen = message.Length;

                while (msgLen == 0)
                {
                    if (msgLen == 0)
                        return "Add a message parameter";
                    msgLen = message.Length;
                }

                queueMessage = new MQMessage();
                queueMessage.WriteString(message);
                queueMessage.Format = MQC.MQFMT_STRING;
                queuePutMessageOptions = new MQPutMessageOptions();

                //putting the message into the queue
                queue.Put(queueMessage, queuePutMessageOptions);
                //add the new message id to the next messageid byte array element
                //to retain earlier messages message ids
                //MessageID[MessageID.Length + 1] = queueMessage.MessageId[MessageID.Length];

                //return MessageID[MessageID.Length + 1].ToString();
                //TODO: Check relevance of MessageID here
                return "success";
            }
            catch (Exception mqexp)
            {
                return "MQSeries Exception: " + mqexp.Message;
            }
        }

        public string putAllOK2LoadMessageIntoFROMITREKSELECTQueueWithSessionID(string flightno, string URNO, string sessID)
        {
            try
            {
                QueueName = iTPaths.GetFROMITREKSELECTQueueName();
                queueManager = new MQQueueManager(QueueManagerName, channelName, connectionName);
                queue = queueManager.AccessQueue(QueueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);

                //message = "<ALLOK2L><SessionId>" + sessID + "</SessionId><URNO>" + URNO + "</URNO><FLIGHTNO>" + flightno + "</FLIGHTNO></ALLOK2L>";
                message = "<ALLOK2L><URNO>" + URNO + "</URNO><SESSIONID>" + sessID + "</SESSIONID><FLIGHTNO>" + flightno + "</FLIGHTNO></ALLOK2L>";

                int msgLen = message.Length;

                while (msgLen == 0)
                {
                    if (msgLen == 0)
                        return "Add a message parameter";
                    msgLen = message.Length;
                }

                queueMessage = new MQMessage();
                queueMessage.WriteString(message);
                queueMessage.Format = MQC.MQFMT_STRING;
                queuePutMessageOptions = new MQPutMessageOptions();

                //putting the message into the queue
                queue.Put(queueMessage, queuePutMessageOptions);
                //add the new message id to the next messageid byte array element
                //to retain earlier messages message ids
                //MessageID[MessageID.Length + 1] = queueMessage.MessageId[MessageID.Length];

                //return MessageID[MessageID.Length + 1].ToString();
                //TODO: Check relevance of MessageID here
                return "success";
            }
            catch (Exception mqexp)
            {
                return "MQSeries Exception: " + mqexp.Message;
            }
        }

        public string putShowDLSMessageIntoFROMITREKSELECTQueueWithSessionID(string flightno, string URNO, string sessID)
        {
            try
            {
                QueueName = iTPaths.GetFROMITREKSELECTQueueName();
                queueManager = new MQQueueManager(QueueManagerName, channelName, connectionName);
                queue = queueManager.AccessQueue(QueueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);
  
                message = "<SHOWDLS><SessionId>" + sessID + "</SessionId><URNO>" + URNO + "</URNO></SHOWDLS>";

                int msgLen = message.Length;

                while (msgLen == 0)
                {
                    if (msgLen == 0)
                        return "Add a message parameter";
                    msgLen = message.Length;
                }

                queueMessage = new MQMessage();
                queueMessage.WriteString(message);
                queueMessage.Format = MQC.MQFMT_STRING;
                queuePutMessageOptions = new MQPutMessageOptions();

                //putting the message into the queue
                queue.Put(queueMessage, queuePutMessageOptions);
                //add the new message id to the next messageid byte array element
                //to retain earlier messages message ids
                //MessageID[MessageID.Length + 1] = queueMessage.MessageId[MessageID.Length];
                
                //return MessageID[MessageID.Length + 1].ToString();
                //TODO: Check relevance of MessageID here
                return "success";
            }
            catch (Exception mqexp)
            {
                return "MQSeries Exception: " + mqexp.Message;
            }
        }

        public void PutATDTimingOnFROMITREKUPDATEQueue(string URNO, string flightno, string ATDTime)
        {
            string message = "<ATDTiming><URNO>" + URNO + "</URNO><FLIGHTNO>" + flightno + "</FLIGHTNO><DATETIME>" + ATDTime + "</DATETIME></ATDTiming>";
            //set to the current queue?
            putTheMessageIntoQueue(message);
        }

    }
}
