//#define TestsOn
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Xml;
using System.Xml.XPath;
using System.IO;
#if TestsOn
using NUnit.Framework;
#endif
using iTrekXML;
using System.Web;

#if TestsOn
namespace iTrekXMLPathsTests
{
    [TestFixture]
    public class iTXMLPathsclTests
    {
        iTXMLPathscl iTPaths;

        //[SetUp]
        public iTXMLPathsclTests()
        {
            //TODO: - Change for deployment
            iTPaths = new iTXMLPathscl();
        }

        //TODO: - Coverage - High
        [Test]
        public void ReadPathsXMLFileTest()
        {
            //OkToLoad path is used as test
            if (iTPaths.ApplicationMode == "Debug")
                //Assert.AreEqual(@"C:\LocalSandboxes\Ufis4.5\Ufis\Sin\SIN_Client\SIN_UfisAddOn\SIN_iTrek\PushServ\iTrekOk2L\bin\Debug\", iTPaths.GetOK2LoadPath());
                Assert.AreEqual(@"C:\Program Files\Ufis\iTrekOK2Load\bin\", iTPaths.GetOK2LoadPath());
            else if (iTPaths.ApplicationMode == "Test")
                Assert.AreEqual(@"F:\Program Files\Ufis\iTrekOK2Load\bin\", iTPaths.GetOK2LoadPath());
            else
                Assert.AreEqual(@"F:\Program Files\Ufis\iTrekOK2Load\bin\", iTPaths.GetOK2LoadPath());

        }

        //TODO: Coverage high
        [Test]
        public void ReadMultipleValuesFromAppConfigFile()
        {
            Assert.AreEqual(iTPaths.GetTOPUSHOK2LOADQueueName(), iTPaths.GetTOPUSHOK2LOADQueueName());
            Assert.AreEqual("http://203.126.249.148:9002/pap", iTPaths.GetPapURL());
        }

        //TODO: Coverage high
        [Test]
        public void GetETDSERVICESTIMERTest()
        {
            Assert.AreEqual("4000", iTPaths.GetETDSERVICESTIMER());
            
        }
        

    }
}
#endif

namespace iTrekXML
{
    /// <summary>
    /// Manages accessing the path data retained in the 
    /// Paths.xml file
    /// </summary>
    public class iTXMLPathscl
    {
        public string ApplicationMode = "";
        string PathsXMLFilePath = "";
        public string applicationBasePath = "";
        

        string OK2LoadPath = "";
        string SessionManagerPath = "";
        string AllXMLPath = "";


        public iTXMLPathscl()
        {
            //Only one ApplicationMode can be active at a time
            //ApplicationMode = "Debug";
            ApplicationMode = "Test";
            //ApplicationMode = "live";

            if (ApplicationMode == "Debug")
            {
                //PathsXMLFilePaths have to be hard coded
                //check Server.MapPath or equiv
                AllXMLPath = @"C:\Program Files\Ufis\iTrekOK2Load\XML\";
                
                PathsXMLFilePath = AllXMLPath + "Paths.xml";
                applicationBasePath = GetApplicationBasePath(PathsXMLFilePath, "//Paths/applicationBasePathDebug");
                OK2LoadPath = GetOK2LoadPath();
                SessionManagerPath = GetSessionManagerPath();
            }
            else if (ApplicationMode == "Test")
            {
                AllXMLPath = @"F:\Program Files\Ufis\iTrekOK2Load\XML\";
                PathsXMLFilePath = AllXMLPath + "Paths.xml";
                applicationBasePath = GetApplicationBasePath(PathsXMLFilePath, "//Paths/applicationBasePathTest");
                OK2LoadPath = GetConfigElmt("//Paths/TestReleasePaths/iTrekOk2LPath");
                SessionManagerPath = GetConfigElmt("//Paths/TestReleasePaths/iTrekSessionManagerPath");
            }
            else
            //live
            {
                AllXMLPath = @"F:\Program Files\Ufis\iTrekOK2Load\XML\";
                PathsXMLFilePath = AllXMLPath + "Paths.xml";
                applicationBasePath = GetApplicationBasePath(PathsXMLFilePath, "//Paths/applicationBasePathLive");
                OK2LoadPath = GetConfigElmt("//Paths/LiveReleasePaths/iTrekOk2LPath");
                SessionManagerPath = GetConfigElmt("//Paths/LiveReleasePaths/iTrekSessionManagerPath");
                //XMLPath = GetConfigElmt("//Paths/LiveReleasePaths/iTrekXMLPath");
            }
        }

        string GetApplicationBasePath(string XMLFilePathbyMode, string expression)
        {
            XPathDocument doc = new XPathDocument(XMLFilePathbyMode);
            XPathNavigator nav = doc.CreateNavigator();
            return nav.SelectSingleNode(expression).ToString();
        }


        public string GetConfigElmt(string expression)
        {
            XPathDocument doc = new XPathDocument(PathsXMLFilePath);
            XPathNavigator nav = doc.CreateNavigator();
            return nav.SelectSingleNode(expression).ToString();
        }


        public string GetOK2LoadPath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekOk2LPath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekOk2LPath");
        }


        public string GetFlightsFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekFlightsFilePath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekFlightsFilePath");
        }

    

        public string GetJobsFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekJobsFilePath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekJobsFilePath");
        }

        public string GetFlightsTestFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekFlightsTestFilePath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekFlightsTestFilePath");
        }

        public string GetJobsTestFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekJobsTestFilePath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekJobsTestFilePath");
        }

        public string GetSessionManagerPath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekSessionManagerPath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekSessionManagerPath");
        }

        public string GetXMLPath()
        {
            if (ApplicationMode == "Debug")
                //return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekXMLPath");
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekXMLPath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekXMLPath");
        }

        public string GetDeviceIDFile()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/DeviceID");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/DeviceID");
        }

        public string GetUpdateableFlightsFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekUpdateableFlightsFilePath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekUpdateableFlightsFilePath");
        }

        public string GetUpdateableJobsFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekUpdateableJobsFilePath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekUpdateableJobsFilePath");
        }

        public string GetTOPUSHOK2LOADQueueName()
        {
            return GetConfigElmt("//Paths/TOPUSHOK2LOAD");
        }

        public string GetFROMITREKSELECTQueueName()
        {
            return GetConfigElmt("//Paths/FROMITREKSELECT");
        }

        public string GetTOITREKREAD_RESQueueName()
        {
            return GetConfigElmt("//Paths/TOITREKREAD_RES");
        }

        public string GetTOITREKAUTHQueueName()
        {
            return GetConfigElmt("//Paths/TOITREKAUTH");
        }

        public string GetFROMITREKUPDATEQueueName()
        {
            return GetConfigElmt("//Paths/FROMITREKUPDATE");
        }

        public string GetSERVICESTIMER()
        {
            return GetConfigElmt("//Paths/SERVICESTIMER");
        }


        public string GetETDSERVICESTIMER()
        {
            return GetConfigElmt("//Paths/ETDSERVICESTIMER");
        }

        public string GetDELSESSDIRTIMER()
        {
            return GetConfigElmt("//Paths/DELSESSDIRTIMER");
        }

        public string GetDELSESSDIRSERVICETIMER()
        {
            return GetConfigElmt("//Paths/DELSESSDIRSERVICETIMER");
        }

        
        public string GetJOB_AFT_SERVICESTIMER()
        {
            return GetConfigElmt("//Paths/JOB_AFT_SERVICESTIMER");
        }

        public string GetTOITREKREAD_AFTQueueName()
        {
            return GetConfigElmt("//Paths/TOITREKREAD_AFT");
        }

        public string GetTOITREKREAD_JOBQueueName()
        {
            return GetConfigElmt("//Paths/TOITREKREAD_JOB");
        }

        public string GetFROMITREKAUTHQueueName()
        {
            return GetConfigElmt("//Paths/FROMITREKAUTH");
        }

        //public string GetTOITREKAUTHQueueName()
        //{
        //    return GetConfigElmt("//Paths/TOITREKAUTH");
        //}


        public string GetPapURL()
        {
            return GetConfigElmt("//Paths/papURL/@href");
        }

        public string GetQueueManagerName()
        {
            return GetConfigElmt("//Paths/QUEUEMANAGERNAME");
        }

        public string GetChannelName()
        {
            return GetConfigElmt("//Paths/CHANNELNAME");
        }

        public string GetChannelInfo()
        {
            return GetConfigElmt("//Paths/CHANNELINFO");
        }

        public string GetChannelTransportType()
        {
            return GetConfigElmt("//Paths/CHANNELTRANSPORTTYPE");
        }

        public string GetChannelServer()
        {
            return GetConfigElmt("//Paths/CHANNELSERVER");
        }

        public string GetChannelPortNumber()
        {
            return GetConfigElmt("//Paths/CHANNELPORTNO");
        }

        public string GetTestSessionId()
        {
            return GetConfigElmt("//Paths/TestSessionId");
        }

        public string GetTestSID()
        {
            return GetConfigElmt("//Paths/TestSID");
        }

        public string GetTestPENO()
        {
            return GetConfigElmt("//Paths/TestPENO");
        }

        public string GetTestPENO2()
        {
            return GetConfigElmt("//Paths/TestPENO2");
        }

        public string GetTestFlightURNO()
        {
            return GetConfigElmt("//Paths/TestFlightURNO");
        }

        public string GetTestFlightURNO2()
        {
            return GetConfigElmt("//Paths/TestFlightURNO2");
        }

        public string GetTestJobURNO()
        {
            return GetConfigElmt("//Paths/TestJobURNO");
        }

        public string GetTestJobURNO2()
        {
            return GetConfigElmt("//Paths/TestJobURNO2");
        }

        public string GetTestUAFT()
        {
            return GetConfigElmt("//Paths/TestUAFT");
        }

        public string GetTestUAFT2()
        {
            return GetConfigElmt("//Paths/TestUAFT2");
        }

        public string GetFileMgrFlushSetting()
        {
            return GetConfigElmt("//Paths/FILEMGRFLUSH");
        }

        public string GetMsgFromQueueDelaySetting()
        {
            return GetConfigElmt("//Paths/GETMSGFROMQUEUEDELAY");
        }

        public string GetLoginExpiryTime()
        {
            return GetConfigElmt("//Paths/LOGINEXPIRYTIME");
        }

        public string GetTestPassword()
        {
            return GetConfigElmt("//Paths/TestPassword");
        }

        public string GetTestSTOA()
        {
            return GetConfigElmt("//Paths/TestSTOA");
        }

        public string GetTestSTOA2()
        {
            return GetConfigElmt("//Paths/TestSTOA2");
        }

        public string GetTestSTOD()
        {
            return GetConfigElmt("//Paths/TestSTOD");
        }

        public string GetTestSTOD2()
        {
            return GetConfigElmt("//Paths/TestSTOD2");
        }

        public string GetETDALERTTIME()
        {
            return GetConfigElmt("//Paths/ETDALERTTIME");
        }

        public string GetTestFLNO()
        {
            return GetConfigElmt("//Paths/TestFLNO");
        }


        public string GetTestULDNo2()
        {
            return GetConfigElmt("//Paths/TestULDNo2");
        }


        public string GetAllOK2LoadTestFlURNO()
        {
            return GetConfigElmt("//Paths/AllOK2LoadTestFlURNO");
        }

        public string GetSHOWDLSTestFlURNO()
        {
            return GetConfigElmt("//Paths/SHOWDLSTestFlURNO");
        }

        public string GetSHOWDLSTestFLNO()
        {
            return GetConfigElmt("//Paths/SHOWDLSTestFLNO");
        }

        public string GetTestAllOK2LoadFLNO()
        {
            return GetConfigElmt("//Paths/TestAllOK2LoadFLNO");
        }

        public string GetOK2LSvcTestUAFT()
        {
            return GetConfigElmt("//Paths/OK2LSvcTestUAFT");
        }

      
        
    }
}
