#define UsingQueues
//#define TestsOn
using System;
using System.Data;
using System.Configuration;
#if TestsOn
using NUnit.Framework;
#endif
using System.Collections.Specialized;
using iTrekXML;
using iTrekWMQ;
using iTrekUtils;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System.Collections;
using System.Text;

#if TestsOn
namespace iTrekSessionMgrTests
{
    [TestFixture]
    public class SessionMgrTests
    {
        iTXMLcl iTXML;
        iTrekSessionMgr.SessionManager sessMan;
        iTXMLPathscl iTPaths;
       
        public SessionMgrTests()
        {
            iTPaths = new iTXMLPathscl();
            iTXML = new iTXMLcl(iTPaths.GetXMLPath());
            sessMan = new iTrekSessionMgr.SessionManager();
        }

        //TODO: Coverage medium
        [Test]
        public void GetPENOBySessionIDTest()
        {
            iTXML.WriteStaffSessionandDeviceIDXmlToDeviceIDFile(iTPaths.GetTestSessionId(), iTPaths.GetTestSID(), iTPaths.GetTestPENO());
            Assert.AreEqual(iTPaths.GetTestPENO(), sessMan.GetPENOBySessionID(iTPaths.GetTestSessionId()));
            iTXML.DeleteDeviceIDEntryByDeviceID(iTPaths.GetTestSID());
        }   


        //TODO: Coverage high
        [Test]
        public void GetStaffNameFromAuthFileInSessionDirTest()
        {
            //setup
            string theXML = "<AUTH><AUTHENTICATED>TRUE</AUTHENTICATED><SESSIONID>" + iTPaths.GetTestSessionId() + "</SESSIONID><FNAME>John</FNAME><LNAME>Smith</LNAME></AUTH>";
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(theXML);
            string fileName = "AUTH.xml";
            sessMan.CreateXMLFileInSessionDirectory(iTPaths.GetTestSessionId(), xmldoc, fileName);
            Assert.AreEqual("John Smith", sessMan.GetStaffNameFromAuthFileInSessionDir(iTPaths.GetTestSessionId()));
            //tear down
            sessMan.DeleteAuthFileInSessionDir(iTPaths.GetTestSessionId());
        }

        //TODO: Coverage high with queues on
        //medium with queues off
        //Implement test when FROMITREKAUTHQueue operational
        //as it depends on it
#if UsingQueues
        [Test]
        public void IsValidLoginTest()
        { 
            Assert.IsTrue(sessMan.IsValidLogin(iTPaths.GetTestPENO(), iTPaths.GetTestPassword(), iTPaths.GetTestSessionId()),"Change TestPENO and-or TestPassword");
            //clean up
            DeleteCurrentMsgFileInSessionDir(iTPaths.GetTestSessionId(), "AUTH.xml");
        }
#endif
        
        //TODO: Coverage high
        [Test]
        public void OrderFlightsByTimeTest()
        {
            //Test that flights are correctly ordered by time
            //regardless of order that they appear in XML

            //Setup
            //SetupTestFlightAndJobEntriesInMainFiles();
            //the first flight is the later flight (STOD2)
            //the al needs data to be set up in the session directory
            string message = @"<FLIGHT><URNO>" + iTPaths.GetTestFlightURNO() + "</URNO><ACTION>INSERT</ACTION><FLNO>QF 999</FLNO><STOD>" + iTPaths.GetTestSTOD2() + "</STOD><STOA>" + iTPaths.GetTestSTOA2() + "</STOA><REGN></REGN><DES3>SIN</DES3><ORG3>LHR</ORG3><ETDI></ETDI><ETOA></ETOA><GTA1></GTA1><GTD1></GTD1><ADID>D</ADID><PSTA></PSTA><PSTD></PSTD></FLIGHT>";
            message = iTXML.DeleteActionFromFlightXML(message);  
            string message2 = @"<FLIGHT><URNO>" + iTPaths.GetTestFlightURNO2() + "</URNO><ACTION>INSERT</ACTION><FLNO>QF 998</FLNO><STOD>" + iTPaths.GetTestSTOD() + "</STOD><STOA>" + iTPaths.GetTestSTOA() + "</STOA><REGN></REGN><DES3>SIN</DES3><ORG3>LHR</ORG3><ETDI></ETDI><ETOA></ETOA><GTA1></GTA1><GTD1></GTD1><ADID>D</ADID><PSTA></PSTA><PSTD></PSTD></FLIGHT>";
            message2 = iTXML.DeleteActionFromFlightXML(message2);
            message += message2;
            message = "<FLIGHTS>" + message + "</FLIGHTS>";
            XmlDocument flightsxmlDoc = iTXML.TransformStringToXmlDoc(message);

            sessMan.WriteXMLToStaffDirectoryBySession(iTPaths.GetTestSessionId(), flightsxmlDoc);
            
            ArrayList al = iTXML.UserSessionReadElementsForArrayListFromCurrentFlightXMLFile(iTPaths.GetTestSessionId(), "//FLIGHTS/FLIGHT", "flights.xml");
            al = iTXML.ReorderFlightsEarliestOnTop(al);
            Flight fl1 = (Flight)al[0];
            Flight fl2 = (Flight)al[1];
            //the second flight had the earlier time - so 
            //we expect it now to be the first in the arraylist
            //Test
            Assert.AreEqual(iTPaths.GetTestFlightURNO2(), fl1.URNO);
            Assert.AreEqual(iTPaths.GetTestFlightURNO(), fl2.URNO);
            //Tear down
            iTXML.DeleteSessionFile(iTPaths.GetTestSessionId(), "flights.xml");
        }

        //TODO: Coverage high
        [Test]
        public void CreateSessionDirectoryTest()
        {
            //Test
            sessMan.CreateSessionDirectory(iTPaths.GetTestSessionId());
            DirectoryInfo di = new DirectoryInfo(iTPaths.GetXMLPath() + iTPaths.GetTestSessionId());
            Assert.IsTrue(di.Exists);
            //Tear down
            sessMan.DeleteSessionDirectory(iTPaths.GetTestSessionId());
            DirectoryInfo di2 = new DirectoryInfo(iTPaths.GetXMLPath() + iTPaths.GetTestSessionId());
            Assert.IsFalse(di2.Exists);
        }

       

        //TODO: Coverage medium - real data will have different format
        //There must be a matching flight no - otherwise get objref error
        [Test]
        public void GetFlightsByStaffIDTest()
        {  
            //staffid (PENO) in Jobs.xml used to identify UAFT (which corresponds to 
            //URNO in Flights.xml
            //setup - write 2 flights to main Flights.xml 
            //with unlikely (but realistic data)
            //the UAFT in the jobs file will then be used as a key to identify the 
            //relevant flight URNO in the main Flights.xml file
            //have to ensure that there are no other flights already assigned to each 
            //of the 2 PENOs for this test to work

            
            //Setup
            SetupTestFlightAndJobEntriesInMainFiles();
            
            //Test
            XmlDocument xmldoc = sessMan.GetFlightsByStaffID(iTPaths.GetTestPENO(), iTPaths.GetJobsTestFilePath(), iTPaths.GetFlightsTestFilePath());
            
            XmlNodeList nlxml = xmldoc.SelectNodes("//FLIGHTS/FLIGHT");
            XmlNode ndxml = nlxml.Item(0);
            
            Assert.AreEqual("QF 999", ndxml.ChildNodes[1].InnerXml);

            XmlDocument xmldoc2 = sessMan.GetFlightsByStaffID(iTPaths.GetTestPENO2(), iTPaths.GetJobsTestFilePath(), iTPaths.GetFlightsTestFilePath());
            XmlNodeList nlxml2 = xmldoc2.SelectNodes("//FLIGHTS/FLIGHT");
            XmlNode ndxml2 = nlxml2.Item(0);
            Assert.AreEqual("QF 998", ndxml2.ChildNodes[1].InnerXml);
            
            //Tear down - remove file entries           
            DeleteSetupTestFlightAndJobEntriesInMainFiles();
        }

        //GetFlightsByStaffIDTest Helper function
        //Always use together with DeleteSetupTestFlightAndJobEntriesInMainFiles
        //Sets up 2 jobs and 2 flights
        //NB. setup the STOD2 and STOA2 times in the first flight
        //so that re-ordering can be tested

        //NB. ETDI set to NOW for GetTestFlightURNO flight
        //and an old date for GetTestFlightURNO2 flight
        //for GetFlightURNOsForLoadAllAlertTest
        public void SetupTestFlightAndJobEntriesInMainFiles()
        {
            string message = @"<FLIGHT><URNO>" + iTPaths.GetTestFlightURNO() + "</URNO><ACTION>INSERT</ACTION><FLNO>QF 999</FLNO><STOD>" + iTPaths.GetTestSTOD2() + "</STOD><STOA>" + iTPaths.GetTestSTOA2() + "</STOA><REGN></REGN><DES3>SIN</DES3><ORG3>LHR</ORG3><ETDI>" + iTUtils.convertStandardDateTimeToOracleFormat(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")) + "</ETDI><ETOA></ETOA><GTA1></GTA1><GTD1></GTD1><ADID>D</ADID><PSTA></PSTA><PSTD></PSTD></FLIGHT>";
            WriteFlightToFile(message);
            message = @"<FLIGHT><URNO>" + iTPaths.GetTestFlightURNO2() + "</URNO><ACTION>INSERT</ACTION><FLNO>QF 998</FLNO><STOD>" + iTPaths.GetTestSTOD() + "</STOD><STOA>" + iTPaths.GetTestSTOA() + "</STOA><REGN></REGN><DES3>SIN</DES3><ORG3>LHR</ORG3><ETDI>20060901122000</ETDI><ETOA></ETOA><GTA1></GTA1><GTD1></GTD1><ADID>D</ADID><PSTA></PSTA><PSTD></PSTD></FLIGHT>";
            WriteFlightToFile(message);
            //jobs - write 2 (with improbable data) jobs to jobs file with 
            //different PENO's assigned to the different flights
            //Nb. The FLIGHT TestFlightURNO will be the JOB UAFT
            //don't use testUAFT values
            //TODO: Change the JOB URNOs?
            message = @"<JOB><URNO>" + iTPaths.GetTestFlightURNO() + "</URNO><ACTION>INSERT</ACTION><PENO>" + iTPaths.GetTestPENO() + "</PENO><UAFT>" + iTPaths.GetTestFlightURNO() + "</UAFT></JOB>";
            WriteJobToFile(message);
            message = @"<JOB><URNO>" + iTPaths.GetTestFlightURNO2() + "</URNO><ACTION>INSERT</ACTION><PENO>" + iTPaths.GetTestPENO2() + "</PENO><UAFT>" + iTPaths.GetTestFlightURNO2() + "</UAFT></JOB>";
            WriteJobToFile(message);
        }

        //NB. ETDI set to NOW + 5 min for GetTestFlightURNO flight
        //and an old date for GetTestFlightURNO2 flight
        //for GetFlightURNOsForLoadAllAlertTest
        //only diff between this test and SetupTestFlightAndJobEntriesInMainFiles
        //is the 5 min added to NOW for ETDI field
        public void SetupTestFlightAndJobEntriesForGetFlightURNOsForLoadAllAlertTest()
        {
            string message = @"<FLIGHT><URNO>" + iTPaths.GetTestFlightURNO() + "</URNO><ACTION>INSERT</ACTION><FLNO>QF 999</FLNO><STOD>" + iTPaths.GetTestSTOD2() + "</STOD><STOA>" + iTPaths.GetTestSTOA2() + "</STOA><REGN></REGN><DES3>SIN</DES3><ORG3>LHR</ORG3><ETDI>" + iTUtils.convertStandardDateTimeToOracleFormat(DateTime.Now.AddMinutes(5.0).ToString("dd/MM/yyyy HH:mm:ss")) + "</ETDI><ETOA></ETOA><GTA1></GTA1><GTD1></GTD1><ADID>D</ADID><PSTA></PSTA><PSTD></PSTD></FLIGHT>";
            WriteFlightToFile(message);
            message = @"<FLIGHT><URNO>" + iTPaths.GetTestFlightURNO2() + "</URNO><ACTION>INSERT</ACTION><FLNO>QF 998</FLNO><STOD>" + iTPaths.GetTestSTOD() + "</STOD><STOA>" + iTPaths.GetTestSTOA() + "</STOA><REGN></REGN><DES3>SIN</DES3><ORG3>LHR</ORG3><ETDI>20060901122000</ETDI><ETOA></ETOA><GTA1></GTA1><GTD1></GTD1><ADID>D</ADID><PSTA></PSTA><PSTD></PSTD></FLIGHT>";
            WriteFlightToFile(message);
            //jobs - write 2 (with improbable data) jobs to jobs file with 
            //different PENO's assigned to the different flights
            //Nb. The FLIGHT TestFlightURNO will be the JOB UAFT
            //don't use testUAFT values
            //TODO: Change the JOB URNOs?
            message = @"<JOB><URNO>" + iTPaths.GetTestFlightURNO() + "</URNO><ACTION>INSERT</ACTION><PENO>" + iTPaths.GetTestPENO() + "</PENO><UAFT>" + iTPaths.GetTestFlightURNO() + "</UAFT></JOB>";
            WriteJobToFile(message);
            message = @"<JOB><URNO>" + iTPaths.GetTestFlightURNO2() + "</URNO><ACTION>INSERT</ACTION><PENO>" + iTPaths.GetTestPENO2() + "</PENO><UAFT>" + iTPaths.GetTestFlightURNO2() + "</UAFT></JOB>";
            WriteJobToFile(message);
        }

        //Deletes 2 jobs and 2 flights
        public void DeleteSetupTestFlightAndJobEntriesInMainFiles()
        {
            XmlDocument xmlDocFlight = new XmlDocument();
            XmlDocument xmlDocJob = new XmlDocument();
           
            xmlDocFlight.Load(iTPaths.GetFlightsTestFilePath());
            xmlDocJob.Load(iTPaths.GetJobsTestFilePath());
            //DeleteFlight and Job FromXMLFile works with a FLIGHT string message
            //First job/flight
            string Deletemessage = @"<FLIGHT><URNO>" + iTPaths.GetTestFlightURNO() + "</URNO><ACTION>DELETE</ACTION></FLIGHT>";
            xmlDocFlight = iTXML.DeleteFlightFromXMLFile(xmlDocFlight, Deletemessage);
            
            xmlDocJob = iTXML.DeleteJobFromXMLFileForGetFlightsByStaffID(xmlDocJob, Deletemessage);
            xmlDocFlight.Save(iTPaths.GetFlightsTestFilePath());
            xmlDocJob.Save(iTPaths.GetJobsTestFilePath());
            //Second job/flight
            xmlDocFlight.Load(iTPaths.GetFlightsTestFilePath());
            xmlDocJob.Load(iTPaths.GetJobsTestFilePath());
            Deletemessage = @"<FLIGHT><URNO>" + iTPaths.GetTestFlightURNO2() + "</URNO><ACTION>DELETE</ACTION></FLIGHT>";
            xmlDocFlight = iTXML.DeleteFlightFromXMLFile(xmlDocFlight, Deletemessage);
            
            xmlDocJob = iTXML.DeleteJobFromXMLFileForGetFlightsByStaffID(xmlDocJob, Deletemessage);
            xmlDocFlight.Save(iTPaths.GetFlightsTestFilePath());
            xmlDocJob.Save(iTPaths.GetJobsTestFilePath());
        }

        public void WriteFlightToFile(string message)
        {
            message = iTXML.DeleteActionFromFlightXML(message);

            XmlDocument doc = new XmlDocument();
            DataSet ds = new DataSet();
            ds.ReadXml(iTPaths.GetFlightsTestFilePath());
            doc.LoadXml(ds.GetXml());
            doc = iTXML.WriteNewFlightToXMLFile(doc, message);
            doc.Save(iTPaths.GetFlightsTestFilePath());
        }

        //Helper function
        public void WriteJobToFile(string message)
        {
            message = iTXML.DeleteActionFromJobXMLFile(message);
            XmlDocument doc = new XmlDocument();
            doc.Load(iTPaths.GetJobsTestFilePath());
            doc = iTXML.WriteNewJobToXMLFile(doc, message);
            doc.Save(iTPaths.GetJobsTestFilePath());
        }

        


        //TODO: Coverage medium - real data will have different format
        //Create setup and cleanup for this test
        //be careful that the writing tests don't impact on this (and similar tests)
        [Test]
        public void GetDeviceIDByStaffIDTest()
        {
            //setup
            //TODO: Ensure that these are non-existent SIDs
            string testSID1 = "SID-0000178192";
            string testSID2 = "SID-0000178198";
            //string TestPENO1 = "123456";
            string TestPENO2 = "123458";
            iTXML.WriteStaffSessionandDeviceIDXmlToDeviceIDFile(iTPaths.GetTestSessionId(), testSID1, iTPaths.GetTestPENO());
            iTXML.WriteStaffSessionandDeviceIDXmlToDeviceIDFile(iTPaths.GetTestSessionId(), testSID2, TestPENO2);
            //test
            Assert.AreEqual(testSID1, sessMan.GetDeviceIDByStaffID(iTPaths.GetTestPENO()));
            Assert.AreEqual(testSID2, sessMan.GetDeviceIDByStaffID(TestPENO2));
            //tear down
            iTXML.DeleteDeviceIDEntryByDeviceID(testSID1);
            iTXML.DeleteDeviceIDEntryByDeviceID(testSID2);
        }

        //TODO: Coverage medium - real data will have different format
        //Create setup and cleanup for this test
        //be careful that the writing tests don't impact on this (and similar tests)
        //If re-factoring this function/test may replace GetDeviceIDByStaffIDTest (above)
        [Test]
        public void GetDeviceIDByPENOTest()
        {
            //setup
            //TODO: Ensure that these are non-existent SIDs
            string testSID1 = "SID-0000178192";
            string testSID2 = "SID-0000178198";
            string testSID3 = "SID-0000178888";
            
            string TestPENO2 = "123458";
            
            iTXML.WriteStaffSessionandDeviceIDXmlToDeviceIDFile(iTPaths.GetTestSessionId(), testSID1, iTPaths.GetTestPENO());
            iTXML.WriteStaffSessionandDeviceIDXmlToDeviceIDFile(iTPaths.GetTestSessionId(), testSID2, TestPENO2);
            
            //test
            Assert.AreEqual(testSID1, sessMan.GetDeviceIDByPENO(iTPaths.GetTestPENO()), "testSID1");
            Assert.AreEqual(testSID2, sessMan.GetDeviceIDByPENO(TestPENO2), "testSID2");
            //this entry has the same PENO (but different SID) as the first entry 
            //(to test that the last entry is one retrieved)
            iTXML.WriteStaffSessionandDeviceIDXmlToDeviceIDFile(iTPaths.GetTestSessionId(), testSID3, iTPaths.GetTestPENO());
            Assert.AreEqual(testSID3, sessMan.GetDeviceIDByPENO(iTPaths.GetTestPENO()), "testSID3");
            //tear down
            iTXML.DeleteDeviceIDEntryByDeviceID(testSID1);
            iTXML.DeleteDeviceIDEntryByDeviceID(testSID2);
            iTXML.DeleteDeviceIDEntryByDeviceID(testSID3);
        }
 
        //TODO: Coverage medium - use to refactor out WriteXMLToStaffDirectoryBySessionTest
        //check if replaced by more realistic tests
        //remove - better tests in place?
        [Test]
        public void CreateXMLFileInSessionDirectoryTest()
        {
            //sessMan.SetupSessionDirectory("123456abc", "22", "0000178198");
            string theXML = "<SessFlights><testxml></testxml></SessFlights>";
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(theXML);
            string fileName = "currentMsg.xml";
            sessMan.CreateXMLFileInSessionDirectory(iTPaths.GetTestSessionId(), xmldoc, fileName);
            //sessMan.CreateSessionIDFileAndInsertText(iTPaths.GetTestSessionId(), xmldoc.InnerText, fileName);
            //get the current session directory and file to test
            DirectoryInfo di = new DirectoryInfo(iTPaths.GetXMLPath());
            DirectoryInfo[] diArr = di.GetDirectories(iTPaths.GetTestSessionId());
            FileInfo[] rgFiles = diArr[0].GetFiles(fileName);
            Assert.AreEqual(fileName, rgFiles[0].Name);
            //clean up - delete file
            for (int i=0; i < rgFiles.Length; i++)
            {
                if (rgFiles[i].Name == fileName)
                    rgFiles[i].Delete();
            }
            //diArr[0].Delete();
        }

        

        //TODO: Coverage low - setting up a directory to delete 
        //- real data will have different format
        //not using real xml
        [Test]
        public void DeleteSessionDirectoryTest()
        {
            //sessMan.SetupSessionDirectory("123456abc", "22", "0000178198");
            string theXML = "<SessFlights><testxml></testxml></SessFlights>";
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(theXML);
            //sessMan.WriteXMLToStaffDirectoryBySession("123456abc", xmldoc);
            sessMan.CreateXMLFileInSessionDirectory(iTPaths.GetTestSessionId(), xmldoc, "flights.xml");
            //get the current session directory and file to test
            DirectoryInfo di = new DirectoryInfo(iTPaths.GetXMLPath());
            //DirectoryInfo[] diArr = di.GetDirectories("*123456abc*");
            DirectoryInfo[] diArr = di.GetDirectories("*" + iTPaths.GetTestSessionId() + "*");
            FileInfo[] rgFiles = diArr[0].GetFiles("*.xml"); 
            //sessMan.DeleteSessionDirectory("123456abc");
            sessMan.DeleteSessionDirectory(iTPaths.GetTestSessionId());
            Assert.IsFalse(diArr[0].Exists);
        }

        //TODO: Coverage medium
        [Test]
        public void GetDeviceIDByUAFTTest()
        {
            //We must at least have a device id 
            
            //Setup
            SetupTestFlightAndJobEntriesInMainFiles();
 
            //Test
            //iTPaths.GetTestPENO() relates to the first flight
            iTXML.WriteStaffSessionandDeviceIDXmlToDeviceIDFile(iTPaths.GetTestSessionId(), iTPaths.GetTestSID(), iTPaths.GetTestPENO());
            //NB. we use the TestFlightURNO as the UAFT here because SetupTestFlightAndJobEntriesInMainFiles
            //sets up the jobs file with the flight URNO - therefore don't use TestUAFT
            //Assert.AreEqual(iTPaths.GetTestSID(), sessMan.GetDeviceIDByUAFT(iTPaths.GetTestUAFT()));
            Assert.AreEqual(iTPaths.GetTestSID(), sessMan.GetDeviceIDByUAFT(iTPaths.GetTestFlightURNO(), iTPaths.GetJobsTestFilePath()));
            //Although the second flight has been written to the flights file
            //no matching device exists in DeviceId.xml
            //OK to use TestUAFT for this test as we are not expecting to find a device for the staffid
            Assert.AreEqual("There are no devices allocated to this staff id (PENO)", sessMan.GetDeviceIDByUAFT(iTPaths.GetTestUAFT2(), iTPaths.GetJobsTestFilePath()));
            
            //Tear down
            iTXML.DeleteDeviceIDEntryByDeviceID(iTPaths.GetTestSID());
            DeleteSetupTestFlightAndJobEntriesInMainFiles();

        }

        //TODO: Coverage medium
        [Test]
        public void GetFlightURNOsForLoadAllAlertTest()
        {
            //Setup - SetupTestFlightAndJobEntriesInMainFiles
            //is setup to have a ETD set to NOW for GetTestFlightURNO
            //entry (but not for GetTestFlightURNO2)
            
            //SetupTestFlightAndJobEntriesInMainFiles();
            SetupTestFlightAndJobEntriesForGetFlightURNOsForLoadAllAlertTest();

            //Test
            bool testBool = false;
            ArrayList FlightAL = sessMan.GetFlightURNOsForLoadAllAlert(iTPaths.GetFlightsTestFilePath());
            foreach (string FlURNO in FlightAL)
            {
                if(FlURNO == iTPaths.GetTestFlightURNO())
                    testBool = true;
            }
            Assert.IsTrue(testBool);
            //Although the second flight has been written to the flights file
            //it has an ETDI that is set to an old date
            //Test
            bool testBool2 = false;
            foreach (string FlURNO in sessMan.GetFlightURNOsForLoadAllAlert(iTPaths.GetFlightsTestFilePath()))
            {
                if (FlURNO == iTPaths.GetTestFlightURNO2())
                    testBool2 = true;
            }
            Assert.IsFalse(testBool2);

            //Tear down
            //iTXML.DeleteDeviceIDEntryByDeviceID(iTPaths.GetTestSID());
            DeleteSetupTestFlightAndJobEntriesInMainFiles();

        }

        //TODO: Coverage medium
        //puts message on queue and gets off same queue
        [Test]
        public void GetAuthenticationMsgFromSessionDirTest()
        {
            //string responseMessage = "<AUTH><AUTHENTICATED>TRUE</AUTHENTICATED><SESSIONID>" + iTPaths.GetTestSessionId() + "</SESSIONID></AUTH>";
            string responseMessage = "<AUTH><AUTHENTICATED>TRUE</AUTHENTICATED><SESSIONID>" + iTPaths.GetTestSessionId() + "</SESSIONID><FNAME>John</FNAME><LNAME>Smith</LNAME></AUTH>";
            sessMan.CreateXMLFileInSessionDirectory(iTXML.GetSessionIdFromMsg(responseMessage), iTXML.TransformStringToXmlDoc(responseMessage), "AUTH.xml");
            Assert.AreEqual("TRUE", sessMan.GetAuthenticationMsgFromSessionDir(iTPaths.GetTestSessionId()));
            //clean up
            sessMan.DeleteAuthFileInSessionDir(iTPaths.GetTestSessionId());
        }

       

        //TODO: Coverage high
        [Test]
        public void GetOK2LoadALFromSessionDirTest()
        {
            //put a message on the queue and receive a response
            //in session dir
            //depends on GetRead_ResSvc
            //TODO: Currently not working with other tests
            //but does alone
#if UsingQueues
            string msgId = "";
            //put the session id on the queue
            ITrekWMQClass1 FROMITREKSELECTQueue = new ITrekWMQClass1();
            //iTPaths.GetFROMITREKSELECTQueueName()
            //TODO: Change to test values?
           msgId = FROMITREKSELECTQueue.putAllOK2LoadMessageIntoFROMITREKSELECTQueueWithSessionID(iTPaths.GetTestAllOK2LoadFLNO(), iTPaths.GetAllOK2LoadTestFlURNO(), iTPaths.GetTestSessionId(),iTPaths.GetFROMITREKSELECTQueueName());
            //msgId = ITrekWMQClass1.putAllOK2LoadMessageIntoFROMITREKSELECTQueueWithSessionID(iTPaths.GetTestAllOK2LoadFLNO(), iTPaths.GetAllOK2LoadTestFlURNO(), iTPaths.GetTestSessionId(), iTPaths.GetFROMITREKSELECTQueueName());
#else
            //simulate - by just writing the result xml to file
            string message = "<ALLOK2L><URNO>1234567890</URNO><SESSIONID>dt2xbz23qk4vbs45sfvqhljh</SESSIONID><ULD1>AKE22960SQ-SIN-620-B</ULD1><ULD2>" + iTPaths.GetTestULDNo2() +"</ULD2><ULD3>AKE22960SQ-SIN-620-B</ULD3><ULD4>AKE22960SQ-SIN-620-B</ULD4></ALLOK2L>";
            sessMan.CreateXMLFileInSessionDirectory(iTXML.GetSessionIdFromMsg(message), iTXML.TransformStringToXmlDoc(message), "ALLOK2L.xml");
#endif
            //we will know the URNO, FLNO and sessid from the UI
            //Nb. if not UsingQueues - we don't test GetAllOk2LoadAL which actually polls the queue
            ArrayList al = sessMan.GetOK2LoadALFromSessionDir(iTPaths.GetTestSessionId());
            if (al != null && al.Count > 0)
                Assert.AreEqual(iTPaths.GetTestULDNo2(), al[1].ToString());
            else
                Assert.Fail("GetOK2LoadAL was empty - change TestFLNO and/or AllOK2LoadTestFlURNO and/or TestULDNo2");
            
            //clean up
            DeleteCurrentMsgFileInSessionDir(iTPaths.GetTestSessionId(), "ALLOK2L.xml");
        }

        //TODO: Coverage high (re: process and queue usage) - but only tests that
        //text greater than 20 chars returned
        [Test]
        public void GetDLSFromSessionDirTest()
        {
            //put a SHOWDLS message on the queue and receive a response
            //in session dir
            //depends on GetRead_ResSvc
            //Must have a Flight Number in SHOWDLSTestFLNO
            //Must have a valid Flight URNO in SHOWDLSTestFlURNO
            //TODO: Currently not working with other tests
            //but does alone
#if UsingQueues
            string msgId = "";
            //put the session id on the queue
            ITrekWMQClass1 FROMITREKSELECTQueue = new ITrekWMQClass1();
             //iTPaths.GetFROMITREKSELECTQueueName()
            msgId = FROMITREKSELECTQueue.putShowDLSMessageIntoFROMITREKSELECTQueueWithSessionID(iTPaths.GetSHOWDLSTestFLNO(), iTPaths.GetSHOWDLSTestFlURNO(), iTPaths.GetTestSessionId(), iTPaths.GetFROMITREKSELECTQueueName());
           // msgId = ITrekWMQClass1.putShowDLSMessageIntoFROMITREKSELECTQueueWithSessionID(iTPaths.GetSHOWDLSTestFLNO(), iTPaths.GetSHOWDLSTestFlURNO(), iTPaths.GetTestSessionId(), iTPaths.GetFROMITREKSELECTQueueName());
#else
            //simulate - by just writing the result xml to file
            
            string message = "<SHOWDLS><URNO>1234567890</URNO><SESSIONID>dt2xbz23qk4vbs45sfvqhljh</SESSIONID><DLS>aaaaaaaaaaaaaaaaaaaaaaa   aaaaaaaaaaaaaaaaaaaaaaaaaaaa   aaaaaaaaaaaa</DLS></SHOWDLS>";
            sessMan.CreateXMLFileInSessionDirectory(iTXML.GetSessionIdFromMsg(message), iTXML.TransformStringToXmlDoc(message), "SHOWDLS.xml");
#endif
            //we will know the URNO, FLNO and sessid from the UI
            //Nb. if not UsingQueues - we don't test GetShowDLS which actually polls the queue 
            string DLS = sessMan.GetShowDLS(iTPaths.GetSHOWDLSTestFLNO(), iTPaths.GetSHOWDLSTestFlURNO(), iTPaths.GetTestSessionId());
            //if (al != null && al.Count > 0)
            if (DLS != "")
                Assert.IsTrue(DLS.Length > 20);
            else
                Assert.Fail("GetShowDLS was empty - change SHOWDLSTestFLNO and/or SHOWDLSTestFlURNO");
            //Tear down
            DeleteCurrentMsgFileInSessionDir(iTPaths.GetTestSessionId(), "SHOWDLS.xml");
        }

        //TODO: Coverage medium
        [Test]
        public void GetFLNOByURNOTest()
        {
            Assert.AreEqual("SQ 7377", sessMan.GetFLNOByURNO("1213617830", iTPaths.GetFlightsTestFilePath()), iTPaths.GetFlightsTestFilePath());
        }

        
        
            //TODO: Coverage medium
        [Test]
        public void GetPENOByUAFTTest()
        {
            //iTXML.WriteStaffSessionandDeviceIDXmlToDeviceIDFile(iTPaths.GetTestSessionId(), iTPaths.GetTestSID(), iTPaths.GetTestPENO());
            SetupTestFlightAndJobEntriesInMainFiles();
            //we use the flight URNO because it corresponds to what is setup in SetupTestFlightAndJobEntriesInMainFiles
            Assert.AreEqual(iTPaths.GetTestPENO(), iTXML.GetPENOByUAFTFromJobsXML(iTPaths.GetTestFlightURNO(), iTPaths.GetJobsTestFilePath()));
            DeleteSetupTestFlightAndJobEntriesInMainFiles();
        }

        //TODO: Coverage medium
        [Test]
        public void GetNoOfStaffAssignedToFlightTest()
        {
            //iTXML.WriteStaffSessionandDeviceIDXmlToDeviceIDFile(iTPaths.GetTestSessionId(), iTPaths.GetTestSID(), iTPaths.GetTestPENO());
            //SetupTestFlightAndJobEntriesInMainFiles();
            string message = @"<JOB><URNO>" + iTPaths.GetTestJobURNO() + "</URNO><ACTION>INSERT</ACTION><PENO>" + iTPaths.GetTestPENO() + "</PENO><UAFT>" + iTPaths.GetTestFlightURNO() + "</UAFT></JOB>";
            WriteJobToFile(message);
            message = @"<JOB><URNO>" + iTPaths.GetTestJobURNO2() + "</URNO><ACTION>INSERT</ACTION><PENO>" + iTPaths.GetTestPENO() + "</PENO><UAFT>" + iTPaths.GetTestFlightURNO() + "</UAFT></JOB>";
            WriteJobToFile(message);
            message = @"<JOB><URNO>2222222222</URNO><ACTION>INSERT</ACTION><PENO>" + iTPaths.GetTestPENO() + "</PENO><UAFT>" + iTPaths.GetTestFlightURNO() + "</UAFT></JOB>";
            WriteJobToFile(message);
            //we use the flight URNO because it corresponds to what is setup in SetupTestFlightAndJobEntriesInMainFiles
            //TODO: Test against higher number
            Assert.AreEqual("3", sessMan.GetNoOfStaffAssignedToFlight(iTPaths.GetTestFlightURNO(), iTPaths.GetJobsTestFilePath()));
            //DeleteSetupTestFlightAndJobEntriesInMainFiles();
            //string Deletemessage = @"<FLIGHT><URNO>" + iTPaths.GetTestFlightURNO() + "</URNO><ACTION>DELETE</ACTION></FLIGHT>";
            XmlDocument xmlDocJob = new XmlDocument();
            xmlDocJob.Load(iTPaths.GetJobsTestFilePath());
            //xmlDocJob = iTXML.(xmlDocJob, Deletemessage);
            //xmlDocJob.Save(iTPaths.GetJobsTestFilePath());
            //Deletemessage = @"<FLIGHT><URNO>" + iTPaths.GetTestFlightURNO() + "</URNO><ACTION>DELETE</ACTION></FLIGHT>";
            string Deletemessage = @"<JOB><URNO>" + iTPaths.GetTestJobURNO() + "</URNO><ACTION>DELETE</ACTION></JOB>";
            xmlDocJob = iTXML.DeleteJobFromXMLFile(xmlDocJob, Deletemessage);
            xmlDocJob.Save(iTPaths.GetJobsTestFilePath());
            Deletemessage = @"<JOB><URNO>" + iTPaths.GetTestJobURNO2() + "</URNO><ACTION>DELETE</ACTION></JOB>";
            xmlDocJob = iTXML.DeleteJobFromXMLFile(xmlDocJob, Deletemessage);
            xmlDocJob.Save(iTPaths.GetJobsTestFilePath());
            Assert.AreEqual("1", sessMan.GetNoOfStaffAssignedToFlight(iTPaths.GetTestFlightURNO(), iTPaths.GetJobsTestFilePath()));
            Deletemessage = @"<JOB><URNO>2222222222</URNO><ACTION>DELETE</ACTION></JOB>";
            xmlDocJob = iTXML.DeleteJobFromXMLFile(xmlDocJob, Deletemessage);
            xmlDocJob.Save(iTPaths.GetJobsTestFilePath());
        }

       
        public void DeleteCurrentMsgFileInSessionDir(string sessID, string fileName)
        {
            iTXML.DeleteSessionFile(sessID, fileName);
        }

        //TODO: Coverage medium
        [Test]
        public void GetUserLoginTimeTest()
        {
            iTXML.WriteStaffSessionandDeviceIDXmlToDeviceIDFile(iTPaths.GetTestSessionId(), iTPaths.GetTestSID(), iTPaths.GetTestPENO());
            Assert.AreEqual(DateTime.Now.ToShortTimeString(), sessMan.GetUserLoginTime(iTPaths.GetTestSessionId()));
            iTXML.DeleteDeviceIDEntryBySessID(iTPaths.GetTestSessionId());
        }
    }
}
#endif

/// <summary>
/// Manages the iTrek application sessions
/// </summary>
namespace iTrekSessionMgr
{
    public class SessionManager
    {
        //string sessionID;
        string deviceID;
        string staffID;
        string password;
        DateTime loginTime;
        iTXMLcl iTXML;
        iTXMLPathscl iTPaths;
        string errorMsgFromMQ;


        //General use constructor
        public SessionManager()
        {
            iTPaths = new iTXMLPathscl();
            iTXML = new iTXMLcl(iTPaths.GetXMLPath());
        }

        //Initial login constructor (check device, staff ids and password)
        public SessionManager(string devID, string stID, DateTime lgtmTime, string pword)
        {
            deviceID = devID;
            staffID = stID;
            loginTime = lgtmTime;
            password = pword;
            iTPaths = new iTXMLPathscl();
            iTXML = new iTXMLcl(iTPaths.GetXMLPath());
        }

        //Initial login constructor (check device, staff ids)
        //without password
        public SessionManager(string devID, string stID, DateTime lgtmTime)
        {
            deviceID = devID;
            staffID = stID;
            loginTime = lgtmTime;
            iTPaths = new iTXMLPathscl();
            iTXML = new iTXMLcl(iTPaths.GetXMLPath());
        }

       

        public string GetPENOBySessionID(string sessID)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(iTPaths.GetDeviceIDFile());
            XmlNodeList DeviceNodeLst = doc.SelectNodes("//deviceIDs/devices");

            string deviceIdAssignedToPENO = "";
            if (DeviceNodeLst.Count > 0)
            {
                foreach (XmlNode DeviceNode in DeviceNodeLst)
                {
                    if (DeviceNode.Attributes.Item(0).Value == sessID)
                    {
                        deviceIdAssignedToPENO = DeviceNode.Attributes.Item(2).Value;
                    }
                }
            }
            return deviceIdAssignedToPENO;
        }


        public bool IsValidSessionWithPassword(string sessionId)
        {
            //if (IsValidDevice(deviceID) && IsValidLogin(staffID, password, sessionId) && IsValidLoginTime(loginTime))
            //check relevance of IsValidLoginTime here
            if (IsValidLogin(staffID, password, sessionId) && IsValidLoginTime(loginTime))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string geterrMsgFromMQ
        {
            get { return errorMsgFromMQ; }
        
        
        }

        public bool IsValidLogin(string staffId, string pword, string sessionId)
        {
            string msgId = "";
            //put the session id on the queue
#if UsingQueues
             ITrekWMQClass1 FROMITREKAUTHQueue = new ITrekWMQClass1();
            //iTPaths.GetFROMITREKAUTHQueueName()
             msgId = FROMITREKAUTHQueue.putMessageIntoFROMITREKAUTHQueueWithSessionID(staffId, pword, sessionId, iTPaths.GetFROMITREKAUTHQueueName());
             
                 errorMsgFromMQ = msgId;
             
             
           // msgId = ITrekWMQClass1.putMessageIntoFROMITREKAUTHQueueWithSessionID(staffId, pword, sessionId, iTPaths.GetFROMITREKAUTHQueueName());
#else
            string responseMessage = "<AUTH><AUTHENTICATED>TRUE</AUTHENTICATED><SESSIONID>" + iTPaths.GetTestSessionId() + "</SESSIONID><FNAME>Charlie</FNAME><LNAME>Drake</LNAME></AUTH>";
                CreateXMLFileInSessionDirectory(iTPaths.GetTestSessionId(), iTXML.TransformStringToXmlDoc(responseMessage), "AUTH.xml");
#endif 

            string returnVal = "";
            //we need to wait for n seconds for iTrekAuthSvc process to get the message from the queue
           
            System.Threading.Thread.Sleep(Convert.ToInt32(iTPaths.GetMsgFromQueueDelaySetting()));
                    returnVal = GetAuthenticationMsgFromSessionDir(sessionId);

                    if (returnVal == "TRUE")
                    {
                        return true;
                    }
                    else
                        if (returnVal == "FALSE")
                        {
                            return false;
                        }
                        else
                        {
                            errorMsgFromMQ = returnVal;
                            return false;
                           
                        
                        }
               
        }

        public ArrayList GetAllOk2LoadAL(string flightno, string URNO, string sessionId)
        {

            //get rid of the previous file if in session dir
            //commented out for prod release before regression tests
            //- code is working and should be uncommented
            string CurrentMsgFile = iTPaths.GetXMLPath() + sessionId + @"\ALLOK2L.xml";
            FileInfo fi = new FileInfo(CurrentMsgFile);
            if (fi.Exists)
                fi.Delete();
           
#if UsingQueues
                string msgId = "";
                //put the session id on the queue
                ITrekWMQClass1 FROMITREKSELECTQueue = new ITrekWMQClass1();
                msgId = FROMITREKSELECTQueue.putAllOK2LoadMessageIntoFROMITREKSELECTQueueWithSessionID(flightno, URNO, sessionId, iTPaths.GetFROMITREKSELECTQueueName());
                //msgId = ITrekWMQClass1.putAllOK2LoadMessageIntoFROMITREKSELECTQueueWithSessionID(flightno, URNO, sessionId, iTPaths.GetFROMITREKSELECTQueueName());
            
#else
            string message = "<ALLOK2L><URNO>1234567890</URNO><SESSIONID>dt2xbz23qk4vbs45sfvqhljh</SESSIONID><ULD1>AKE22960SQ-SIN-620-B</ULD1><ULD2>AKE22960SQ-SIN-620-B</ULD2><ULD3>AKE22960SQ-SIN-620-B</ULD3><ULD4>AKE22960SQ-SIN-620-B</ULD4></ALLOK2L>";
            CreateXMLFileInSessionDirectory(iTXML.GetSessionIdFromMsg(message), iTXML.TransformStringToXmlDoc(message), "ALLOK2L.xml");
#endif
                ArrayList al = null;
            //we need to wait for n seconds for iTrekFileMgr process to get the message from the queue
            
            System.Threading.Thread.Sleep(Convert.ToInt32(iTPaths.GetMsgFromQueueDelaySetting()));
            al = GetOK2LoadALFromSessionDir(sessionId);
            return al;
        }

        public string GetShowDLS(string flightno, string URNO, string sessionId)
        {
            //get rid of the previous file if in session dir
            //commented out for prod release before regression tests
            //- code is working and should be uncommented
            string CurrentMsgFile = iTPaths.GetXMLPath() + sessionId + @"\SHOWDLS.xml";
            FileInfo fi = new FileInfo(CurrentMsgFile);
            if (fi.Exists)
                fi.Delete();

#if UsingQueues
                string msgId = "";
                //put the session id on the queue
                ITrekWMQClass1 FROMITREKSELECTQueue = new ITrekWMQClass1();
                msgId = FROMITREKSELECTQueue.putShowDLSMessageIntoFROMITREKSELECTQueueWithSessionID(flightno, URNO, sessionId, iTPaths.GetFROMITREKSELECTQueueName());
                //msgId = ITrekWMQClass1.putShowDLSMessageIntoFROMITREKSELECTQueueWithSessionID(flightno, URNO, sessionId, iTPaths.GetFROMITREKSELECTQueueName());
#else
                string message = "<SHOWDLS><URNO>1234567890</URNO><SESSIONID>dt2xbz23qk4vbs45sfvqhljh</SESSIONID><DLS>  aaaaaaaaaaaaaaaaaaaaaaa   aaaaaaaaaaaaaaaaaaaaaaaaaaaa   aaaaaaaaaaaa</DLS></SHOWDLS>";
                CreateXMLFileInSessionDirectory(iTXML.GetSessionIdFromMsg(message), iTXML.TransformStringToXmlDoc(message), "SHOWDLS.xml");
#endif

                string DLS = "";
            //we need to wait for n seconds for iTrekFileMgr process to get the message from the queue
            //use configurable time param
 
                System.Threading.Thread.Sleep(Convert.ToInt32(iTPaths.GetMsgFromQueueDelaySetting()));
                DLS = GetDLSFromSessionDir(sessionId);
            return DLS;
        }
        
        public void CreateFileAndInsertText(string text, string filename)
        {
            //TODO: check if directory exists
            FileInfo f = new FileInfo(iTPaths.GetXMLPath() + filename);
            StreamWriter w = f.CreateText();
            //put the message into the file
            w.WriteLine(text);
            w.Close();
        }

        public string GetAuthenticationMsgFromSessionDir(string SessId)
        {
            string CurrentMsgFile = iTPaths.GetXMLPath() + SessId + @"\AUTH.xml";
            FileInfo fi = new FileInfo(CurrentMsgFile);
            
            if (fi.Exists)
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(CurrentMsgFile);
                XmlNode xmln = doc.SelectSingleNode("//AUTH/AUTHENTICATED");
                return xmln.FirstChild.InnerText;  
            }
            else
            {
                return "";
            }
        }

        public ArrayList GetOK2LoadALFromSessionDir(string SessId)
        {
            string CurrentMsgFile = iTPaths.GetXMLPath() + SessId + @"\ALLOK2L.xml";
            FileInfo fi = new FileInfo(CurrentMsgFile);
           
            if (fi.Exists)
            {
                XmlDocument doc = new XmlDocument();
                DataSet ds = new DataSet();
                ds.ReadXml(CurrentMsgFile);
                doc.LoadXml(ds.GetXml());

                XmlNode CurrentOK2LNode = doc.SelectSingleNode("//ALLOK2L");
                XmlNodeList CurrentNodeLst = CurrentOK2LNode.ChildNodes;
                ArrayList al = new ArrayList();
                if (CurrentNodeLst.Count > 0)
                {
                    foreach (XmlNode CurrentNode in CurrentNodeLst)
                    {
                        if (CurrentNode.Name.Substring(0, 3) == "ULD")
                        {
                            if(CurrentNode.InnerXml != "")
                                al.Add(CurrentNode.InnerXml);
                        }
                    }
                }
                return al;
            }
            else
            {
                return null;
            }
        }


        public string GetDLSFromSessionDir(string SessId)
        {
            string DLS = "";
            string CurrentMsgFile = iTPaths.GetXMLPath() + SessId + @"\SHOWDLS.xml";
            
            FileInfo fi = new FileInfo(CurrentMsgFile);
            if (fi.Exists)
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(CurrentMsgFile);
                XmlNode CurrentDLSNode = doc.SelectSingleNode("//SHOWDLS/DLS");
                DLS = CurrentDLSNode.InnerXml;
            }
            return DLS;
        }


        public string GetStaffNameFromAuthFileInSessionDir(string SessId)
        {
            string CurrentMsgFile = iTPaths.GetXMLPath() + SessId + @"\AUTH.xml";
            FileInfo fi = new FileInfo(CurrentMsgFile);
            if (fi.Exists)
            {
                XmlDocument doc = new XmlDocument();
                DataSet ds = new DataSet();
                ds.ReadXml(CurrentMsgFile);
                doc.LoadXml(ds.GetXml());
                string StaffName = "";
                XmlNode xmln = doc.SelectSingleNode("//AUTH/FNAME");
                StaffName = xmln.FirstChild.InnerText;
                //xmln = doc.SelectSingleNode("//AUTH/LNAME");
                //StaffName += " " + xmln.FirstChild.InnerText;
                return StaffName;
            }
            else
            {
                return "";
            }
        }

        public bool IsValidLoginTime(DateTime lgTm)
        {
            //Time set in LOGINEXPIRYTIME
            lgTm = lgTm.AddHours(Convert.ToDouble(iTPaths.GetLoginExpiryTime()));
            if (lgTm > DateTime.Now)
                return true;
            else
                return false;
        }

        public XmlDocument GetFlightsByStaffID(string staffID, string jobsMainFilePath, string flightsMainFilePath)
        {
            //Open jobs.xml and get all flights for this staffid
            ArrayList lstJobs = iTXML.UsePENOToGetMatchingFlightsFromMainJobsXMLFile("//JOBS/JOB", staffID, jobsMainFilePath);
            //Open flights.xml and get all flights
            XmlNodeList lstFlights = iTXML.GetBatchFlightsNodeList("//FLIGHTS/FLIGHT", flightsMainFilePath);
            //match the two sets of data
            //and create an XML string (with no root element - which is created by newdoc)
            //XmlNodeList listofSessionFlights;
            string theXML = "";
            foreach (string job in lstJobs)
            { 
                foreach (XmlNode FlightNode in lstFlights)
                {
                    if (FlightNode.ChildNodes[0].InnerXml == job)
                    { 
                        //HACK: XML Writer?
                        theXML += "<FLIGHT>";
                        theXML += "<URNO>" + FlightNode.ChildNodes[0].InnerXml + "</URNO>";
                        theXML += "<FLNO>" + FlightNode.ChildNodes[1].InnerXml + "</FLNO>";
                        theXML += "<STOD>" + FlightNode.ChildNodes[2].InnerXml + "</STOD>";
                        theXML += "<STOA>" + FlightNode.ChildNodes[3].InnerXml + "</STOA>";
                        theXML += "<REGN>" + FlightNode.ChildNodes[4].InnerXml + "</REGN>";
                        theXML += "<DES3>" + FlightNode.ChildNodes[5].InnerXml + "</DES3>";
                        theXML += "<ORG3>" + FlightNode.ChildNodes[6].InnerXml + "</ORG3>";
                        theXML += "<ETDI>" + FlightNode.ChildNodes[7].InnerXml + "</ETDI>";
                        theXML += "<ETOA>" + FlightNode.ChildNodes[8].InnerXml + "</ETOA>";
                        theXML += "<GTA1>" + FlightNode.ChildNodes[9].InnerXml + "</GTA1>";
                        theXML += "<GTD1>" + FlightNode.ChildNodes[10].InnerXml + "</GTD1>";
                        theXML += "<ADID>" + FlightNode.ChildNodes[11].InnerXml + "</ADID>";
                        theXML += "<PSTA>" + FlightNode.ChildNodes[12].InnerXml + "</PSTA>";
                        theXML += "<PSTD>" + FlightNode.ChildNodes[13].InnerXml + "</PSTD>";      
                        theXML += "</FLIGHT>";                   
                    }
                }
            }
            theXML = "<FLIGHTS>" + theXML + "</FLIGHTS>";
            XmlDocument newdoc = new XmlDocument();
           
            newdoc.LoadXml(theXML);
               
            return newdoc;
        }

        public void WriteXMLToStaffDirectoryBySession(string sessID, XmlDocument xmlDoc)
        {
            //get the current session directory
            DirectoryInfo di = new DirectoryInfo(iTPaths.GetXMLPath());
            //Identify the directory for this session id
            DirectoryInfo[] diArr = di.GetDirectories("*" + sessID + "*"); 
            string directoryName = diArr[0].FullName;
            // Open the writer
            XmlTextWriter myWriter = new XmlTextWriter(directoryName + @"\flights.xml", Encoding.UTF8);
            // Indented for easy reading
            myWriter.Formatting = Formatting.Indented;
            // Write the file to it's directory
            xmlDoc.WriteTo(myWriter);
            myWriter.Close();
        }

        //TODO: This can be used to refactor WriteXMLToStaffDirectoryBySession
        public void CreateXMLFileInSessionDirectory(string sessID, XmlDocument xmlDoc, string fileName)
        {
            //get the current session directory
            DirectoryInfo di = new DirectoryInfo(iTPaths.GetXMLPath());
            di.CreateSubdirectory(sessID);
            //Identify the directory for this session id
            DirectoryInfo[] diArr = di.GetDirectories("*" + sessID + "*");
            string directoryName = diArr[0].FullName;
            iTUtils.WriteXmlDocToFile(xmlDoc, directoryName + @"\" + fileName);      
        }


        public void DeleteSessionDirectory(string sessID)
        {
            DirectoryInfo di = new DirectoryInfo(iTPaths.GetXMLPath() + sessID);
            di.Delete(true);
        }

        public void CreateSessionDirectory(string sessID)
        {
            DirectoryInfo di = new DirectoryInfo(iTPaths.GetXMLPath());
            di.CreateSubdirectory(sessID);
        }

        
        public void DeleteAuthFileInSessionDir(string sessID)
        {
            iTXML.DeleteSessionFile(sessID, "AUTH.xml");
        }

        //TODO: this will probably replace GetStaffIDByFlights
        //public string GetPENOByUAFT(string UAFT, string jobsMainFilePath)
        //{
        //    //string JobsFile = iTPaths.GetXMLPath() + @"Jobs\Jobs.xml";
        //    XmlDocument doc = new XmlDocument();
        //    //DataSet ds = new DataSet();
        //    //ds.ReadXml(JobsFile);
        //    //doc.LoadXml(ds.GetXml());
        //    doc.Load(jobsMainFilePath);
        //    XmlNodeList JobsNodeLst = doc.SelectNodes("//JOBS/JOB");

        //    string PENOMatchedToFlight = "";
        //    if (JobsNodeLst.Count > 0)
        //    {
        //        foreach (XmlNode JobsNode in JobsNodeLst)
        //        {
        //            //IF UAFT == flightNo
        //            if (JobsNode.ChildNodes.Item(2).InnerXml == UAFT)
        //            {
        //                //match to PENO
        //                PENOMatchedToFlight = JobsNode.ChildNodes.Item(1).InnerXml;
        //            }
        //        }
        //    }
        //    return PENOMatchedToFlight;
        //}

        public string GetFLNOByURNO(string URNO, string flightMainFilePath)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(flightMainFilePath);
            XmlNodeList FlightsNodeLst = doc.SelectNodes("//FLIGHTS/FLIGHT");

            string FLNOMatchedToFlight = "";
            if (FlightsNodeLst.Count > 0)
            {
                foreach (XmlNode FlightsNode in FlightsNodeLst)
                {
                    if (FlightsNode.ChildNodes.Item(0).InnerXml == URNO)
                    {
                        //match to URNO
                        FLNOMatchedToFlight = FlightsNode.ChildNodes.Item(1).InnerXml;
                    }
                }
            }
            return FLNOMatchedToFlight;
        }


        public ArrayList GetFlightURNOsForLoadAllAlert(string flightsFilePath)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(flightsFilePath);
            XmlNodeList FlightsNodeLst = doc.SelectNodes("//FLIGHTS/FLIGHT");
            ArrayList alFlightsTriggerLoadAllAlert = new ArrayList();
            //setup an old date as default
            DateTime ETD = Convert.ToDateTime(iTUtils.ConvertDateFromOracleFormat("20050901122000"));
            //DateTime ETDAddTenMin = Convert.ToDateTime(iTUtils.ConvertDateFromOracleFormat("20050901122000"));
            int ETDAlertTime = Convert.ToInt32(iTPaths.GetETDALERTTIME());
            TimeSpan TenMinuteTimeSpan = new TimeSpan(0, ETDAlertTime, 0);
            TimeSpan ETDMinusCurrentTime = new TimeSpan();
            if (FlightsNodeLst.Count > 0)
            {
                foreach (XmlNode FlightsNode in FlightsNodeLst)
                {
                    //get the ETDI
                    if (FlightsNode.ChildNodes.Item(7).InnerXml != "")
                    {
                        //iTUtils.LogToEventLog("etd:" + FlightsNode.ChildNodes.Item(7).InnerXml);
                        //iTUtils.LogToEventLog("convetd:" + iTrekUtils.iTUtils.ConvertDateFromOracleFormat(FlightsNode.ChildNodes.Item(7).InnerXml));
                        ETD = Convert.ToDateTime(iTrekUtils.iTUtils.ConvertDateFromOracleFormat(FlightsNode.ChildNodes.Item(7).InnerXml));
                        ETDMinusCurrentTime = ETD - DateTime.Now;
                        if ((ETDMinusCurrentTime < TenMinuteTimeSpan) && (ETD > DateTime.Now))
                        {
                            //add the URNO
                            //iTUtils.LogToEventLog("inetd:");
                            alFlightsTriggerLoadAllAlert.Add(FlightsNode.ChildNodes.Item(0).InnerXml);
                        }

                    }
                    else
                    
                    {
                        /*Added by Alphy on 06 Dec 2006 to send Net Alert for n minutes before STOD if no ETDI*/
                        if (FlightsNode.ChildNodes.Item(2).InnerXml != "")
                        {
                            //iTUtils.LogToEventLog("std:" + FlightsNode.ChildNodes.Item(2).InnerXml);
                            ETD = Convert.ToDateTime(iTrekUtils.iTUtils.ConvertDateFromOracleFormat(FlightsNode.ChildNodes.Item(2).InnerXml));
                            ETDMinusCurrentTime = ETD - DateTime.Now;
                            if ((ETDMinusCurrentTime < TenMinuteTimeSpan) && (ETD > DateTime.Now))
                            {
                                //add the URNO
                                //iTUtils.LogToEventLog("std:");
                                alFlightsTriggerLoadAllAlert.Add(FlightsNode.ChildNodes.Item(0).InnerXml);
                            }
                        }
                    
                    
                    }
                    
                    
                }
            }
            return alFlightsTriggerLoadAllAlert;
        }

        //TODO: This will probably become obsolete by GetDeviceIDByPENO
        public string GetDeviceIDByStaffID(string staffid)
        {
            string DeviceFile = iTPaths.GetXMLPath() + "DeviceID.xml";
            XmlDocument doc = new XmlDocument();
            DataSet ds = new DataSet();
            ds.ReadXml(DeviceFile);
            doc.LoadXml(ds.GetXml());
            XmlNodeList DeviceNodeLst = doc.SelectNodes("//deviceIDs/devices");

            string deviceIdAssignedToStaffID = "";
            if (DeviceNodeLst.Count > 0)
            {
                foreach (XmlNode DeviceNode in DeviceNodeLst)
                {
                    if (DeviceNode.Attributes.Item(2).Value == staffid)
                    {
                        deviceIdAssignedToStaffID = DeviceNode.Attributes.Item(1).Value;
                    }
                }
            }
            return deviceIdAssignedToStaffID;
        }

        //This will probably replace GetDeviceIDByStaffID
        public string GetDeviceIDByPENO(string PENO)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(iTPaths.GetDeviceIDFile());
            XmlNodeList DeviceNodeLst = doc.SelectNodes("//deviceIDs/devices");

            string deviceIdAssignedToPENO = "";
            if (DeviceNodeLst.Count > 0)
            {
                foreach (XmlNode DeviceNode in DeviceNodeLst)
                {
                    if (DeviceNode.Attributes.Item(2).Value == PENO)
                    {
                        deviceIdAssignedToPENO = DeviceNode.Attributes.Item(1).Value;
                    }
                }
            }
            if (deviceIdAssignedToPENO == "")
            {
                return "There are no devices allocated to this staff id (PENO)";
            }
            else
            {
                return deviceIdAssignedToPENO;
            }
        }

        //public string GetPENOBySessionID(string sessID)
        //{
        //    string DeviceFile = iTPaths.GetXMLPath() + "DeviceID.xml";
        //    XmlDocument doc = new XmlDocument();
        //    DataSet ds = new DataSet();
        //    //TODO: Refactor?
        //    ds.ReadXml(DeviceFile);
        //    doc.LoadXml(ds.GetXml());
        //    XmlNodeList DeviceNodeLst = doc.SelectNodes("//deviceIDs/devices");

        //    string deviceIdAssignedToPENO = "";
        //    if (DeviceNodeLst.Count > 0)
        //    {
        //        foreach (XmlNode DeviceNode in DeviceNodeLst)
        //        {
        //            if (DeviceNode.Attributes.Item(0).Value == sessID)
        //            {
        //                deviceIdAssignedToPENO = DeviceNode.Attributes.Item(2).Value;
        //            }
        //        }
        //    }
        //    return deviceIdAssignedToPENO;
        //}


        public string GetNoOfStaffAssignedToFlight(string UAFT, string jobsFilePath)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(jobsFilePath);
            XmlNodeList JobsNodeLst = doc.SelectNodes("//JOBS/JOB[UAFT = '" + UAFT + "']");
            return JobsNodeLst.Count.ToString();
        }

        public string GetDeviceIDByUAFT(string UAFT, string jobsMainFilePath)
        {
            string PENO = iTXML.GetPENOByUAFTFromJobsXML(UAFT, jobsMainFilePath);
            if (PENO == "")
            {
                return "There are no flights allocated to this staff id";
            }
            else
            {
                return GetDeviceIDByPENO(PENO);
            }
        }

        /* Added by Alphy to get the list of DeviceIDS for sending OK2LOAD Messages*/

        public ArrayList GetDeviceIDSByUAFT(string UAFT, string jobsMainFilePath)
        {
            ArrayList penoList = iTXML.GetPENOSByUAFTFromJobsXML(UAFT, jobsMainFilePath);
            ArrayList deviceIDList = new ArrayList();
            deviceIDList.Clear();
            if(penoList.Count!=0)        
            {
                foreach (string peno in penoList)
                {
                    string deviceId = GetDeviceIDByPENO(peno);
                    if (deviceId != "There are no devices allocated to this staff id (PENO)")
                    {
                        deviceIDList.Add(deviceId);
                    }
                    
                }
            }
            return deviceIDList;
        }


        public Hashtable GetDeviceIDSinHTByUAFT(string UAFT, string jobsMainFilePath)
        {
            ArrayList penoList = iTXML.GetPENOSByUAFTFromJobsXML(UAFT, jobsMainFilePath);
            Hashtable htdeviceID = new Hashtable();
            if (penoList.Count != 0)
            {
                foreach (string peno in penoList)
                {
                    string deviceId = GetDeviceIDByPENO(peno);
                    if (deviceId != "There are no devices allocated to this staff id (PENO)")
                    {
                        htdeviceID.Add(peno, deviceId);
                    }
                       
                }
            }
            return htdeviceID;
        }

        

        //public string GetDeviceIDBySession(string sessid)
        //{
        //    string DeviceFile = iTPaths.GetXMLPath() + "DeviceID.xml";
        //    XmlDocument doc = new XmlDocument();
        //    DataSet ds = new DataSet();
        //    ds.ReadXml(DeviceFile);
        //    doc.LoadXml(ds.GetXml());
        //    XmlNodeList DeviceNodeLst = doc.SelectNodes("//deviceIDs/devices");

        //    string deviceIdAssignedToSession = "";
        //    if (DeviceNodeLst.Count > 0)
        //    {
        //        foreach (XmlNode DeviceNode in DeviceNodeLst)
        //        {
        //            if (DeviceNode.Attributes.Item(0).Value == sessid)
        //            {
        //                deviceIdAssignedToSession = DeviceNode.Attributes.Item(1).Value;
        //            }
        //        }
        //    }
        //    return deviceIdAssignedToSession;
        //}

        public string GetUserLoginTime(string sessID)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(iTPaths.GetDeviceIDFile());
            XmlNode deviceNode = doc.SelectSingleNode("//deviceIDs/devices[@sessionid = '" + sessID + "']");
            //Format a short time string
            return iTUtils.GetShortTimeFromDateTimeStr(deviceNode.Attributes.Item(3).Value);
        }

    }
}
