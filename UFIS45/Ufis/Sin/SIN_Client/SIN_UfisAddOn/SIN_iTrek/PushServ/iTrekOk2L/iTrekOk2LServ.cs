#define UsingQueues
#define LoggingOn

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using IBM.WMQ;
using System.Timers;
using System.Configuration;
using iTrekUtils;
using iTrekXML;
using iTrekWMQ;
//using System.Security.Policy;   //for  evidence  object
using System.Xml;
using iTrekSessionMgr;



namespace iTrekOk2L
{
    /// <summary>
    /// Service to get OKToLoad messages off the queue and send a Push message
    /// to the PPG server via http.
    /// Requires that there is a PENO in the jobs.xml file
    /// which the UAFT in the message can be matched to.
    /// The recipient would have to be logged in because the device
    /// SID is recorded in the deviceid.xml file at login time
    /// </summary>
    
    public partial class iTrekOk2L : ServiceBase
    {
        iTXMLcl iTXML; 
        iTXMLPathscl iTPaths;
        MQQueue queue;
        
        ITrekWMQClass1 wmq1;

        public iTrekOk2L()
        {
            InitializeComponent();
            iTXML = new iTXMLcl();
            iTPaths = new iTXMLPathscl();
#if LoggingOn        
            iTTimerLog.WriteEntry("OK2L Initialisation"); 
#endif
        }

        protected override void OnStart(string[] args)
        {
            wmq1 = new ITrekWMQClass1();
           // queue = wmq1.getQueueConnection(iTPaths.GetTOPUSHOK2LOADQueueName(), MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
#if LoggingOn
            iTTimerLog.WriteEntry("In OnStart");
#endif
            MessageQueueTimer();
#if LoggingOn
            //iTTimerLog.WriteEntry("Finished in MessageQueueTimer");
#endif
        }

        protected override void OnStop()
        {
#if LoggingOn
            iTTimerLog.WriteEntry("In onStop.");
            //queue.Close();
            //wmq1.getQManager.Close();
           // wmq1.getQManager.Disconnect();
#endif
        }

        public void MessageQueueTimer()
        {  
#if LoggingOn
            iTTimerLog.WriteEntry("In MessageQueueTimer");
#endif
            System.Timers.Timer aTimer = new System.Timers.Timer();
            aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            aTimer.Interval = Convert.ToInt32(iTPaths.GetSERVICESTIMER());
            aTimer.Enabled = true;
        }

        // Specify what you want to happen when the Elapsed event is raised.
        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            string message = "";
            
#if LoggingOn
            iTTimerLog.WriteEntry("In OnTimedEvent");
#endif

#if UsingQueues
            //iTrekWMQClass mqObj = new iTrekWMQClass(iTPaths.GetTOPUSHOK2LOADQueueName());
           // message = mqObj.GetMessageFromTheQueue();
            if (wmq1 != null)
            {
                queue = wmq1.getQueueConnection(iTPaths.GetTOPUSHOK2LOADQueueName(), MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
            }
            else
            {
                wmq1 = new ITrekWMQClass1(); 
                queue = wmq1.getQueueConnection(iTPaths.GetTOPUSHOK2LOADQueueName(), MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);

            
            }
            message = wmq1.GetMessageFromTheQueue(queue);
            wmq1.closeQueueConnection(queue, wmq1.getQManager);
#else
            message = "<OK2L><ULDNUMBER>AKE123456CX</ULDNUMBER><UAFT>" + iTPaths.GetOK2LSvcTestUAFT() + "</UAFT></OK2L>";
#endif
            //need to convert string to byte array
            //and back to remove unwanted chars
            message = iTUtils.FromUnicodeByteArrayToString(iTUtils.ConvertStringToByteArray(message));


            //iTTimerLog.WriteEntry(message);
            //no point adding push info - user would have to re-login to 
            //read net alert - better to just obtain OK2load listing
            //add push info to querystring (no blank spaces)
            //currently unsure of exact char limit
            //string info = "OK2LOAD-FlightSQ020/AKE21304SQ/SIN/620/B";
            //info just here for standby - not currently used
            string info = "";

            //XmlPushClient PushClient = new XmlPushClient("http://203.126.249.148:9002/pap");
            //Change due to move to UAT server
            XmlPushClient PushClient = new XmlPushClient("http://57.228.106.159:9002/pap");

            if (message == "MQRC_NO_MSG_AVAILABLE")
            {
#if LoggingOn
                iTTimerLog.WriteEntry("MQRC_NO_MSG_AVAILABLE");
#endif
            }
            else
            {
                SessionManager sessMan = new SessionManager();
                //NB. the UAFT must be matched to a PENO in the jobs.xml file
                //the PENO must also be entered into deviceid.xml
                //the SID in device.xml must be valid for a handset
                string UAFTFromMsg = iTXML.GetUAFTFromMsg(message);
#if LoggingOn
                iTTimerLog.WriteEntry("UAFTFromMsg");
#endif
              //  string DeviceIDByUAFT = sessMan.GetDeviceIDByUAFT(UAFTFromMsg, iTPaths.GetJobsFilePath());
                //ArrayList deviceIDSbyUAFTlst = sessMan.GetDeviceIDSByUAFT(UAFTFromMsg, iTPaths.GetJobsFilePath());
                Hashtable htOkToLoad = sessMan.GetDeviceIDSinHTByUAFT(UAFTFromMsg, iTPaths.GetJobsFilePath());
                
#if LoggingOn
                iTTimerLog.WriteEntry("DeviceIDByUAFT");
#endif
                string OK2LULDNo = iTXML.OK2LULDNo(message);
#if LoggingOn
                iTTimerLog.WriteEntry("OK2LULDNo");
#endif
                string FLNOfromURNO = sessMan.GetFLNOByURNO(iTXML.GetUAFTFromMsg(message), iTPaths.GetFlightsFilePath());
#if LoggingOn
                iTTimerLog.WriteEntry("FLNOfromURNO");
#endif
                ArrayList csvFltList = new ArrayList();
                
                //Send the Service Indication
                IDictionaryEnumerator oktoenum = htOkToLoad.GetEnumerator();
                while (oktoenum.MoveNext())
                {


                    string PID = PushClient.PushServiceIndication(oktoenum.Value.ToString(), FLNOfromURNO + "-" + OK2LULDNo, "http://172.31.246.41/itrek/login.aspx");
                    
                    
                    
                    
                    //string PID = PushClient.PushServiceIndication(DeviceIDByUAFT, FLNOfromURNO + "-" + OK2LULDNo, "");
                    //string PID = PushClient.PushServiceIndication(DeviceIDByUAFT, FLNOfromURNO + "-" + OK2LULDNo, "http://57.228.106.35/itrek/loggedin.aspx?info=" + info);
                    //string PID = PushClient.PushServiceIndication("SID-0000178198", "SQ 020 - AKE123456CX", "http://ufissin.homelinux.net/itrek/loggedin.aspx?info=" + info); 


#if LoggingOn
                    iTTimerLog.WriteEntry("PID: " + PID + "msg: " + message);
                    iTTimerLog.WriteEntry("UAFTFromMsg: " + UAFTFromMsg);
                    iTTimerLog.WriteEntry("DeviceIDByUAFT: " + oktoenum.Value.ToString());
                    iTTimerLog.WriteEntry("OK2LULDNo: " + OK2LULDNo);
                    iTTimerLog.WriteEntry("FLNO: " + FLNOfromURNO);

                    csvFltList.Add(FLNOfromURNO);
                    csvFltList.Add(UAFTFromMsg);
                    csvFltList.Add(DateTime.Now.ToString());
                    csvFltList.Add(OK2LULDNo);
                    csvFltList.Add(oktoenum.Key.ToString());
                    createCSVFile(csvFltList);


#endif

                }
                csvFltList.Clear();

                htOkToLoad.Clear();
        

//              foreach (string deviceID in deviceIDSbyUAFTlst)
//                {
                    
//                    string PID = PushClient.PushServiceIndication(deviceID, FLNOfromURNO + "-" + OK2LULDNo, "");
//                    //string PID = PushClient.PushServiceIndication(DeviceIDByUAFT, FLNOfromURNO + "-" + OK2LULDNo, "");
//                    //string PID = PushClient.PushServiceIndication(DeviceIDByUAFT, FLNOfromURNO + "-" + OK2LULDNo, "http://57.228.106.35/itrek/loggedin.aspx?info=" + info);
//                    //string PID = PushClient.PushServiceIndication("SID-0000178198", "SQ 020 - AKE123456CX", "http://ufissin.homelinux.net/itrek/loggedin.aspx?info=" + info); 
                    
                   
//#if LoggingOn
//                    iTTimerLog.WriteEntry("PID: " + PID + "msg: " + message);
//                    iTTimerLog.WriteEntry("UAFTFromMsg: " + UAFTFromMsg);
//                    iTTimerLog.WriteEntry("DeviceIDByUAFT: " + deviceID);
//                    iTTimerLog.WriteEntry("OK2LULDNo: " + OK2LULDNo);
//                    iTTimerLog.WriteEntry("FLNO: " + FLNOfromURNO);

//                    csvFltList.Add(FLNOfromURNO);
//                    csvFltList.Add(UAFTFromMsg);
//                    csvFltList.Add(DateTime.Now.ToLongDateString());
//                    csvFltList.Add(OK2LULDNo);
//                    csvFltList.Add(deviceID);
//                    createCSVFile(csvFltList);
                    
                    
//#endif
//                }
          
                

//#if LoggingOn
//                iTTimerLog.WriteEntry("PID: " + PID + "msg: " + message);
//                iTTimerLog.WriteEntry("UAFTFromMsg: " + UAFTFromMsg);
//                iTTimerLog.WriteEntry("DeviceIDByUAFT: " + DeviceIDByUAFT);
//                iTTimerLog.WriteEntry("OK2LULDNo: " + OK2LULDNo);
//                iTTimerLog.WriteEntry("FLNO: " + FLNOfromURNO);
//#endif
            }
        }

        private void createCSVFile(ArrayList lstFlts)
        {

            try
            {
                System.IO.StreamWriter csvFile = new System.IO.StreamWriter(@"F:\Program Files\Ufis\iTrekOK2Load\okToLoadCSV.csv", true);
                string sep = ",";
                csvFile.Write(csvFile.NewLine);
                foreach (string lst in lstFlts)
                {
                    csvFile.Write(lst);
                    csvFile.Write(sep);

                }
                csvFile.Close();
                lstFlts.Clear();
            }

            catch (Exception ex)
            {

                iTTimerLog.WriteEntry("error: " + ex.Message);
            }


        }
    
    }
}