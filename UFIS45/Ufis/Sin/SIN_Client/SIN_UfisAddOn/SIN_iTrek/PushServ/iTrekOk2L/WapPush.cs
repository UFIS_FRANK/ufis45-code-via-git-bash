using System.IO;
using System.Net;
using System.Xml;
using System;
namespace iTrekOk2L
{
    public class XmlPushClient
    {
        public enum PushStatus
        {
            Aborted,
            Cancelled,
            Delivered,
            Expired,
            Pending,
            Rejected,
            Timeout,
            Undeliverable,
            Unknown
        }
        public struct StatusQueryResult
        {
            public PushStatus MessageState;
            public string Code;
            public string Description;
        }
        private string papSvr;

        public XmlPushClient(string papServer)
        {
            this.papSvr = papServer;
        }

        public static string GeneratePushId()
        {
            string pid;
            //int[] myValue = new int[3];
            //Randomize();
            //myValue(0) = System.Convert.ToInt32(Int((99999 * Rnd()) + 1));
            //myValue(1) = System.Convert.ToInt32(Int((999 * Rnd()) + 1));
            //myValue(2) = System.Convert.ToInt32(Int((99999 * Rnd()) + 1));
            //myValue(3) = System.Convert.ToInt32(Int((999 * Rnd()) + 1));
            
            Random randomNumber = new Random();
            int rndNum1 = System.Convert.ToInt32((randomNumber.Next(1, 99999) + 1));
            int rndNum2 = System.Convert.ToInt32((randomNumber.Next(1, 999) + 1));
            //pid = myValue(0) + "/" + myValue(1) + "/Openwave Push";
            pid = rndNum1 + "/" + rndNum2 + "/Openwave Push";
            
            return pid;
        }

        public static string GenerateServiceIndicationId()
        {
            string siid;
            //int[] myValue = new int[2];
            //Randomize();
            //myValue(0) = System.Convert.ToInt32(Int((99999 * Rnd()) + 1));
            //myValue(1) = System.Convert.ToInt32(Int((999 * Rnd()) + 1));
            //siid = "SI/" + myValue(0) + "/" + myValue(1);
            Random randomNumber = new Random();
            int rndNum1 = System.Convert.ToInt32((randomNumber.Next(1, 99999) + 1));
            int rndNum2 = System.Convert.ToInt32((randomNumber.Next(1, 999) + 1));
            siid = "SI/" + rndNum1 + "/" + rndNum2;
            return siid;
        }

        public static PushStatus ParseMessageState(string messageState)
        {
            PushStatus result;
            if (messageState.Equals("aborted"))
            {
                result = PushStatus.Aborted;
            }
            else if (messageState.Equals("cancelled"))
            {
                result = PushStatus.Cancelled;
            }
            else if (messageState.Equals("delivered"))
            {
                result = PushStatus.Delivered;
            }
            else if (messageState.Equals("expired"))
            {
                result = PushStatus.Expired;
            }
            else if (messageState.Equals("pending"))
            {
                result = PushStatus.Pending;
            }
            else if (messageState.Equals("rejected"))
            {
                result = PushStatus.Rejected;
            }
            else if (messageState.Equals("timeout"))
            {
                result = PushStatus.Timeout;
            }
            else if (messageState.Equals("undeliverable"))
            {
                result = PushStatus.Undeliverable;
            }
            else
            {
                result = PushStatus.Unknown;
            }
            return result;
        }

        public static string MessageStateToString(PushStatus messageState)
        {
            string result = "unknown";
            if (messageState == PushStatus.Aborted)
            {
                result = "aborted";
            }
            else if (messageState == PushStatus.Cancelled)
            {
                result = "cancelled";
            }
            else if (messageState == PushStatus.Delivered)
            {
                result = "delivered";
            }
            else if (messageState == PushStatus.Expired)
            {
                result = "expired";
            }
            else if (messageState == PushStatus.Pending)
            {
                result = "pending";
            }
            else if (messageState == PushStatus.Rejected)
            {
                result = "rejected";
            }
            else if (messageState == PushStatus.Timeout)
            {
                result = "timeout";
            }
            else if (messageState == PushStatus.Undeliverable)
            {
                result = "undeliverable";
            }
            return result;
        }

        private string CreateServiceIndicationRequest(Stream requestStream, string recipient, string alertMsg, string alertUrl)
        {
            string pid = GeneratePushId();
            string siid = GenerateServiceIndicationId();
            string targetSid = "WAPPUSH=" + recipient + "_WAPGW.grid.net.sg/TYPE=USER@www.openwave.com";
            StreamWriter sw = new StreamWriter(requestStream);
            //sw.WriteLine(vbCrLf);
            sw.WriteLine(Environment.NewLine);
            sw.WriteLine("--WPL17woVbhESdfalYevGqpdzLCs");
            sw.WriteLine("Content-Type: application/xml; charset=UTF-8");
            sw.WriteLine("");
            sw.WriteLine("<?xml version=\"1.0\"?>");
            sw.WriteLine("<!DOCTYPE pap PUBLIC \"-//WAPFORUM//DTD PAP 2.0//EN\" \"http://www.wapforum.org/DTD/pap_2.0.dtd\">");
            sw.WriteLine("<pap>");
            sw.WriteLine("<push-message push-id=\"" + pid + "\">");
            sw.WriteLine("<address address-value=\"" + targetSid + "\"/>");
            //sw.WriteLine("<quality-of-service delivery-method=\"preferconfirmed\" network=\"\" network-required=\"false\" bearer=\"\" bearer-required=\"false\"/>");
            sw.WriteLine("<quality-of-service delivery-method=\"unconfirmed\" network=\"\" network-required=\"false\" bearer=\"\" bearer-required=\"false\"/>");
            sw.WriteLine("</push-message>");
            sw.WriteLine("</pap>");
            sw.WriteLine("--WPL17woVbhESdfalYevGqpdzLCs");
            sw.WriteLine("Content-Type: text/vnd.wap.si; charset=UTF-8");
            sw.WriteLine("");
            sw.WriteLine("<?xml version=\"1.0\"?>");
            sw.Write("<!DOCTYPE si PUBLIC \"-//WAPFORUM//DTD SI 1.0//EN\"");
            sw.Write(" ");
            sw.WriteLine("\"http://www.wapforum.org/DTD/si.dtd\">");
            sw.WriteLine("<si>");
            sw.WriteLine("<indication href=\"" + alertUrl + "\"" + " si-id=\"" + siid + "\" action=\"signal-high\">");
            sw.WriteLine(alertMsg);
            sw.WriteLine("</indication>");
            sw.WriteLine("</si>");
            sw.WriteLine("--WPL17woVbhESdfalYevGqpdzLCs--");
            sw.Close();
            return pid;
        }

        public string PushServiceIndication(string recipient, string alertMsg, string alertUrl)
        {
            HttpWebRequest myRequest = ((HttpWebRequest)(WebRequest.Create(papSvr)));
            myRequest.Method = "POST";
            myRequest.ContentType = "multipart/related; type=application/xml; boundary=WPL17woVbhESdfalYevGqpdzLCs";
            string pid = CreateServiceIndicationRequest(myRequest.GetRequestStream(), recipient, alertMsg, alertUrl);
            HttpWebResponse objResponse = (HttpWebResponse)myRequest.GetResponse();
            objResponse.Close();
            return pid;
        }

        private void CreateStatusQueryRequest(Stream requestStream, string pushId)
        {
            XmlTextWriter myXmlTextWriter = null;
            myXmlTextWriter = new XmlTextWriter(requestStream, null);
            myXmlTextWriter.Formatting = System.Xml.Formatting.Indented;
            myXmlTextWriter.WriteStartDocument();
            myXmlTextWriter.WriteDocType("pap", "-//WAPFORUM//DTD PAP 2.0//EN", "http://www.wapforum.org/DTD/pap_2.0.dtd", null);
            myXmlTextWriter.WriteStartElement("pap");
            myXmlTextWriter.WriteStartElement("statusquery-message");
            myXmlTextWriter.WriteAttributeString("push-id", pushId);
            myXmlTextWriter.WriteEndElement();
            myXmlTextWriter.WriteEndElement();
            myXmlTextWriter.Flush();
            myXmlTextWriter.Close();
        }

        private StatusQueryResult ParseStatusQueryResponse(XmlDocument statusQueryResponse)
        {
            XmlNodeReader xmlNodeRdr = new XmlNodeReader(statusQueryResponse);
            StatusQueryResult result = new StatusQueryResult();
            //StatusQueryResult result2;
            xmlNodeRdr.MoveToContent();
            while (xmlNodeRdr.Read())
            {
                if (xmlNodeRdr.NodeType == XmlNodeType.Element)
                {
                    if (xmlNodeRdr.Name == "badmessage-response")
                    {
                        result.Code = xmlNodeRdr.GetAttribute("code");
                        result.Description = xmlNodeRdr.GetAttribute("desc");
                    }
                    else if (xmlNodeRdr.Name == "statusquery-result")
                    {
                        result.Code = xmlNodeRdr.GetAttribute("code");
                        result.Description = xmlNodeRdr.GetAttribute("desc");
                        result.MessageState = ParseMessageState(xmlNodeRdr.GetAttribute("message-state"));
                    }
                }
            }
           
            xmlNodeRdr.Close();
            return result;
        }

        public StatusQueryResult StatusQuery(string pushId)
        {
            HttpWebRequest myReq = ((HttpWebRequest)(WebRequest.Create(papSvr)));
            myReq.Method = "POST";
            myReq.ContentType = "application/xml; charset=UTF-8";
            CreateStatusQueryRequest(myReq.GetRequestStream(), pushId);
            HttpWebResponse objResponse = (HttpWebResponse)myReq.GetResponse();
            StreamReader sr = new StreamReader(objResponse.GetResponseStream());
            string responseContent = sr.ReadToEnd();
            int pos = responseContent.IndexOf("<pap");
            responseContent = responseContent.Substring(pos);
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(responseContent);
            objResponse.Close();
            return ParseStatusQueryResponse(xmldoc);
        }
    }
}