pause code - use for loop

'Generic utilities that can be used.
Imports System
Imports System.Net
Imports System.Globalization
Imports System.IO
Imports System.Text

Class SysUtils

    Public Shared Function GetRandomNumber(ByVal iMin As Integer, ByVal iMax As Integer) As Integer
        Dim rand As New Random()
        Try
            GetRandomNumber = rand.Next(iMin, iMax)
        Finally
            rand = Nothing
        End Try
    End Function

    Public Shared Sub WriteOutFile(ByVal DIAGNOSTICS_REQUIRED As Boolean, ByVal strFileName As String, ByVal strContents As String)
        If DIAGNOSTICS_REQUIRED = True Then
            Dim lengthDirectory As Integer
            lengthDirectory = InStrRev(strFileName, "\") - 1
            Try
                Dim outputDirectory As System.IO.Directory
                outputDirectory.CreateDirectory(Mid(strFileName, 1, lengthDirectory))
            Catch
                'swallow the error...
            End Try
            Try
                Dim writer As System.IO.TextWriter
                writer = New System.IO.StreamWriter(strFileName)

                writer.Write(strContents)
                writer.Close()

            Catch e As Exception
                'swallow the error...
            End Try
        End If
    End Sub

    '*****************************************************************************************************
    '* Change Log
    '*
    '* DateTime			Developer			Description
    '* 2004.04.20		Philip.Gasaatura	Modification of WriteOutFile, writing to one log file created eachday
    '                                       every log is created into this file Log(dd-mm-yyy).xml
    '                                       logs from devices are created dependant on whether their corresponding
    '                                       node in the config file is set to true/false
    '*****************************************************************************************************
    Public Shared Sub WriteXMLLog(ByVal DIAGNOSTICS_REQUIRED As Boolean, _
                                    ByVal Level As String, _
                                    ByVal Type As String, _
                                    ByVal TypeDetail As String, _
                                    ByVal Device As String, _
                                    ByVal LogDetail As String, _
                                    Optional ByVal DEVICE_DIAGNOSTICS_REQUIRED As Boolean = True)



        If DIAGNOSTICS_REQUIRED = True And DEVICE_DIAGNOSTICS_REQUIRED = True Then
            Dim lengthDirectory As Integer
            Dim strFileName As String = "c:\temp\qacaf\Log" + Format(DateTime.Today, "dd-MM-yyyy") + ".xml"
            lengthDirectory = InStrRev(strFileName, "\") - 1
            LogDetail = LogDetail.Replace("<![CDATA[", "")
            LogDetail = LogDetail.Replace("]]>", "")

            Dim strContents = "<log><datetime>" + System.DateTime.Now + "</datetime><level>" + Level + "</level><type>" + Type + "</type><typedetail>" + TypeDetail + "</typedetail><device>" + Device + "</device><logdetail><![CDATA[" + LogDetail + "]]></logdetail></log>"
            Try
                Dim outputDirectory As System.IO.Directory
                outputDirectory.CreateDirectory(Mid(strFileName, 1, lengthDirectory))
            Catch e As Exception
                'swallow the error...
            End Try
            Try
                If (File.Exists(strFileName)) Then

                    Dim writer As System.IO.StreamWriter = File.AppendText(strFileName)
                     writer.Write(strContents)
                    writer.Close()
                Else
                    Dim writer As System.IO.TextWriter
                    writer = New System.IO.StreamWriter(strFileName)
                    writer.Write(strContents)
                    writer.Close()
                End If


            Catch e As Exception
                'swallow the error...
            End Try
        End If
    End Sub


    Public Shared Function ReadFromFile(ByVal strFileName As String) As String
        Try

            Dim reader As System.IO.TextReader
            reader = New System.IO.StreamReader(strFileName)

            ReadFromFile = reader.ReadToEnd()
            reader.Close()
        Catch
            'swallow the error...
        End Try
    End Function

    Public Shared Function GetMachineName() As String
        GetMachineName = System.Environment.MachineName
    End Function

    Public Shared Function TransformXML(ByVal DIAGNOSTICS_REQUIRED As Boolean, ByVal xmlDoc As Xml.XmlDocument, ByVal strXSLURL As String) As String
        Dim i As String = SysUtils.GetRandomNumber(1, 1000).ToString
        Dim xslDoc As New System.Xml.Xsl.XslTransform()
        Try
            Dim txtwriterTemp As New System.IO.StringWriter()
            Try
                xslDoc.Load(strXSLURL)
                xslDoc.Transform(xmlDoc, Nothing, txtwriterTemp, Nothing)
                'SysUtils.WriteOutFile(DIAGNOSTICS_REQUIRED, "c:\temp\QACAF\Transform\" + i + "-xml.xml", xmlDoc.OuterXml)
                'SysUtils.WriteOutFile(DIAGNOSTICS_REQUIRED, "c:\temp\QACAF\Transform\" + i + "-xslfile.txt", strXSLURL)
                'SysUtils.WriteOutFile(DIAGNOSTICS_REQUIRED, "c:\temp\QACAF\Transform\" + i + "-output.xml", txtwriterTemp.ToString)
                TransformXML = txtwriterTemp.ToString
            Finally
                txtwriterTemp = Nothing
            End Try
        Finally
            xslDoc = Nothing
        End Try
    End Function

    Public Shared Function TransformXML(ByVal DIAGNOSTICS_REQUIRED As Boolean, ByVal strXMLInput As String, ByVal strXSLURL As String) As String
        Try
            Dim xmlDoc As New System.Xml.XmlDocument()
            Try
                xmlDoc.LoadXml(strXMLInput)
                TransformXML = TransformXML(DIAGNOSTICS_REQUIRED, xmlDoc, strXSLURL)
            Finally
                xmlDoc = Nothing
            End Try
        Catch e As Exception
            SysUtils.WriteXMLLog(DIAGNOSTICS_REQUIRED, "Transform", "ERROR", "", "", e.ToString(), True)
            'Throw e
        End Try
    End Function

    Public Shared Function ConvertStreamToString(ByVal theStream As System.IO.Stream) As String
        Dim srReader As System.IO.StreamReader
        srReader = New System.IO.StreamReader(theStream, System.Text.Encoding.ASCII)
        Return srReader.ReadToEnd()
    End Function

    Public Shared Function GetXMLFrom(ByVal strURL As String, Optional ByVal strProxy As String = "", Optional ByVal iPort As Integer = 0) As Xml.XmlDocument
        Dim myRequest As Net.WebRequest
        Dim strmResponse As System.IO.Stream
        myRequest = Net.WebRequest.Create(strURL)
        If strProxy = "" Then
            myRequest.Proxy = New Net.WebProxy(strProxy, iPort)
        End If
        ' Return the response. 
        strmResponse = myRequest.GetResponse().GetResponseStream()
        Dim strResponse As String = ConvertStreamToString(strmResponse)
        GetXMLFrom = New Xml.XmlDocument()
        GetXMLFrom.LoadXml(strResponse)
        myRequest.GetResponse().Close()
    End Function

    '*****************************************************************************************************
    '* Change Log
    '*
    '* DateTime			Developer			Description
    '* 2004.04.20		Philip.Gasaatura	Modification from GetHTML to take device and type params, making calls to WriteXMLLog
    '* 2004.04.20		Philip.Gasaatura	Strip off XML Header being returned
    '*****************************************************************************************************
    Public Shared Function GetHTMLFrom(ByVal DIAGNOSTICS_REQUIRED As Boolean, _
                                              ByVal strURL As String, ByVal device As String, _
                                              ByVal type As String, _
                                              Optional ByVal Token1 As String = "", _
                                              Optional ByVal Token2 As String = "", _
                                              Optional ByVal Token3 As String = "") As String

        Try
            Dim myRequest As Net.WebRequest
            Dim strmResponse As System.IO.Stream
            Dim i As String = SysUtils.GetRandomNumber(0, 1000).ToString
            Dim strResponse As String

            'setup the request's URI, proxy and credentials and collect the response

            myRequest = Net.WebRequest.Create(strURL)
            myRequest.Proxy = Net.WebProxy.GetDefaultProxy()

            ' If a Token is set/present then use stored credentials
            If Len(Trim(Token1)) > 0 Then
                Dim myCredentialCache As New CredentialCache()
                myCredentialCache.Add(myRequest.RequestUri, "Basic", New NetworkCredential(Token1, Token2, Token3))
                Dim myCredential As NetworkCredential = myCredentialCache.GetCredential(myRequest.RequestUri, "Basic")
                myRequest.Proxy.Credentials = myCredential 'Associating only our credentials            
                strmResponse = myRequest.GetResponse().GetResponseStream()
                strResponse = ConvertStreamToString(strmResponse)
            Else
                myRequest.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials
                'SysUtils.WriteOutFile(DIAGNOSTICS_REQUIRED, "c:\temp\QACAF\tf-html\HTML-request" + i + ".txt", "URL:(" + strURL + ")")
                strmResponse = myRequest.GetResponse().GetResponseStream()
                strResponse = ConvertStreamToString(strmResponse)
            End If

            SysUtils.WriteXMLLog(DIAGNOSTICS_REQUIRED, "GetHTML", "REQUEST", type, device, "URL:( " + strURL + " )")
            'Tidy up, write to log file and return the respomse
            myRequest.GetResponse().Close()

            strResponse = strResponse.Replace("<?xml version=""1.0"" encoding=""ISO-8859-1""?>", "")
            SysUtils.WriteXMLLog(DIAGNOSTICS_REQUIRED, "GetHTML", "RESPONSE", type, device, strResponse)
            Return strResponse
        Catch exp As Exception
            Throw exp
        End Try
    End Function


    Public Shared Sub LogToEventLog(ByVal strMessage As String)
        Try
            Dim strLogSrc As String = "QACAF"
            Dim strLogName As String = "QACAFLogs"
            Dim EventLog1 As New System.Diagnostics.EventLog()
            If Not EventLog.SourceExists(strLogSrc) Then
                EventLog.CreateEventSource(strLogSrc, strLogName)
            End If
            EventLog1.Source = strLogSrc
            EventLog1.Log = strLogName
            EventLog1.WriteEntry(strMessage & "(Time:" & Now.ToString & ":" & Now.Millisecond.ToString & ")")
        Catch
        End Try
    End Sub
    Public Shared Function BuildINumber(ByVal dInputString As String) As String
        Dim result As String
        Dim holder As Array
        Dim iInputChar As String
        If InStr(dInputString, ",") Then
            iInputChar = ","
        Else
            iInputChar = "."
        End If
        holder = Split(dInputString, iInputChar)
        If holder.Length = 2 Then
            result = holder(0) & NumberFormatInfo.CurrentInfo.NumberDecimalSeparator & holder(1)
        Else
            result = dInputString
        End If
        Return result
    End Function

End Class

namespace iTrekOk2LTests
{
    [TestFixture]
    public class iTrekWMQTests
    {
        //currently Ok2LWMQ tests only
        iTXMLcl iTXML;
        iTrekWMQClass mqObj;
        iTXMLPathscl iTPaths;

        //[SetUp]
        public iTrekWMQTests()
        {
            iTPaths = new iTXMLPathscl();

            //As we want to add multiple assemblies to the NUnit project
            //at PushServ directory level 
            //we need to point NUNit at the relevant ApplicationBase
            //when instantiating classes
            //This one is for iTrekOk2L (points to config xml file)

            //Create  the  application  domain  setup  information.
            AppDomainSetup domaininfo = new AppDomainSetup();
            domaininfo.ApplicationBase = System.Environment.CurrentDirectory;
            //Create  evidence  for  new  appdomain.
            Evidence adevidence = AppDomain.CurrentDomain.Evidence;
            //  Create  the  new  application  domain  using  setup  information.
            AppDomain domain = AppDomain.CreateDomain("iTrekOk2L", adevidence, domaininfo);

            iTXML = new iTXMLcl(domain.SetupInformation.ApplicationBase);
            //mqObj = new iTrekOk2L.WMQClass(iTPaths.GetTOPUSHOK2LOADQueueName());
            mqObj = new iTrekWMQClass(iTPaths.GetTOPUSHOK2LOADQueueName());
        }
        //TODO: Coverage high
        //If fail - check service already running
        //We also test the OK2Load service from this project
        [Test]
        public void CanOk2LServiceRun()
        {
            string message = "";
            System.ServiceProcess.ServiceController sc = new System.ServiceProcess.ServiceController("iTrekOk2L");
            sc.Start();
            for (int i = 1; i <= 6000000; i++)
            {
                //do nothing, delay for service to start
                //increase number if test failing
            }
            message = sc.Status.ToString();
            sc.Stop();
            sc.Dispose();
            Assert.AreEqual("Running", message);
        }
    }
}

