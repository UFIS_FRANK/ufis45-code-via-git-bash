namespace iTrekOk2L
{
    partial class iTrekOk2L
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.iTTimerLog = new System.Diagnostics.EventLog();
            ((System.ComponentModel.ISupportInitialize)(this.iTTimerLog)).BeginInit();
            // 
            // iTTimerLog
            // 
            this.iTTimerLog.Log = "iTreckOk2LLog";
            this.iTTimerLog.Source = "iTreckOk2LSource";
            // 
            // iTrekOk2L
            // 
            this.ServiceName = "iTrekOk2L";
            ((System.ComponentModel.ISupportInitialize)(this.iTTimerLog)).EndInit();

        }

        #endregion

        private System.Diagnostics.EventLog iTTimerLog;

    }
}
