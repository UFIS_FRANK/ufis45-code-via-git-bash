using System.Collections.Generic;
using System.ServiceProcess;
using System.Text;

namespace iTrekOk2L
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            //#if (!DEBUG)
            ServiceBase[] ServicesToRun;

            // More than one user Service may run within the same process. To add
            // another service to this process, change the following line to
            // create a second service object. For example,
            //
            //   ServicesToRun = new ServiceBase[] {new Service1(), new MySecondUserService()};
            //
            ServicesToRun = new ServiceBase[] { new iTrekOk2L() };

            ServiceBase.Run(ServicesToRun);

            //#else
            //// debug code: allows the process to run as a non-service
            //// will kick off the service start point, but never kill it
            //// shut down the debugger to exit
            ////iTrekOk2L iTrekOk2Lservice = new iTrekOk2L();
            ////iTrekOk2Lservice.
            
            //iTrekOk2L.MessageQueueTimer();
            //System.Threading.Thread.Sleep(System.Threading.Timeout.Infinite);
            //#endif 
        }
    }
}