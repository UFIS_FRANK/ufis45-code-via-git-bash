using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Xml;
using System.Xml.XPath;
using System.IO;
using NUnit.Framework;
using iTrekXML;

namespace iTrekXMLPathsTests
{
    [TestFixture]
    public class iTXMLPathsclTests
    {
        iTXMLPathscl iTPaths;
       
        //iTrekSessionMgr.SessionManager sessMan;

        //[SetUp]
        public iTXMLPathsclTests()
        {
            //TODO: - Change for deployment
            //iTXMLPaths = new iTXMLPaths(@"C:\LocalSandboxes\Ufis4.5\Ufis\Sin\SIN_Client\SIN_UfisAddOn\SIN_iTrek\PushServ\iTrekXML");
            iTPaths = new iTXMLPathscl();
            //sessMan = new iTrekSessionMgr.SessionManager();
        }

        //TODO: - Coverage - High
        [Test]
        public void ReadPathsXMLFileTest()
        {
            //OkToLoad path is used as test
            if (iTPaths.ApplicationMode == "Debug")
                Assert.AreEqual(@"C:\LocalSandboxes\Ufis4.5\Ufis\Sin\SIN_Client\SIN_UfisAddOn\SIN_iTrek\PushServ\iTrekOk2L\bin\Debug\", iTPaths.GetOK2LoadPath());
            else if (iTPaths.ApplicationMode == "Test")
                Assert.AreEqual(@"C:\Program Files\Ufis\iTrekOK2Load\bin\", iTPaths.GetOK2LoadPath());
            else
                Assert.AreEqual(@"C:\Program Files\Ufis\iTrekOK2Load\bin\", iTPaths.GetOK2LoadPath());

        }

        //TODO: Coverage high
        [Test]
        public void ReadMultipleValuesFromAppConfigFile()
        {
            //Assert.AreEqual(iTPaths.GetTOPUSHOK2LOADQueueName(), iTXML.GetConfigElmt("//configuration/appSettings/TOPUSHOK2LOAD"));
            Assert.AreEqual(iTPaths.GetTOPUSHOK2LOADQueueName(), iTPaths.GetTOPUSHOK2LOADQueueName());
            //Assert.AreEqual("http://203.126.249.148:9002/pap", iTXML.GetConfigElmt("//configuration/appSettings/papURL/@href"));
            Assert.AreEqual("http://203.126.249.148:9002/pap", iTPaths.GetPapURL());
        }
        
    }
}

namespace iTrekXML
{
    /// <summary>
    /// Manages accessing the path data retained in the 
    /// Paths.xml file
    /// </summary>
    public class iTXMLPathscl
    {
        public string ApplicationMode = "";
        string PathsXMLFilePath = "";
        public string applicationBasePath = "";

        string OK2LoadPath = "";
        string SessionManagerPath = "";
        //string XMLPath = "";
        string AllXMLPath = "";
 

        public iTXMLPathscl()
        {
            //Only one ApplicationMode can be active at a time
            ApplicationMode = "Debug";
            //ApplicationMode = "Test";
           //ApplicationMode = "live";
            
            if (ApplicationMode == "Debug")
            {
                //PathsXMLFilePaths have to be hard coded
                //AllXMLPath = @"C:\LocalSandboxes\Ufis4.5\Ufis\Sin\SIN_Client\SIN_UfisAddOn\SIN_iTrek\PushServ\iTrekXML\XML\";
                AllXMLPath = @"C:\Program Files\Ufis\iTrekOK2Load\XML\";
                PathsXMLFilePath = AllXMLPath + "Paths.xml";
                applicationBasePath = GetApplicationBasePath(PathsXMLFilePath, "//Paths/applicationBasePathDebug");
                OK2LoadPath = GetOK2LoadPath();
                SessionManagerPath = GetSessionManagerPath();
                //XMLPath = GetConfigElmt("//Paths/DebugPaths/iTrekXMLPath");
            }
            else if (ApplicationMode == "Test")
            {
                AllXMLPath = @"C:\Program Files\Ufis\iTrekOK2Load\XML\";
                PathsXMLFilePath = AllXMLPath + "Paths.xml";
                //applicationBasePath = GetConfigElmt("//Paths/applicationBasePathTest");
                applicationBasePath = GetApplicationBasePath(PathsXMLFilePath, "//Paths/applicationBasePathTest");
                OK2LoadPath = GetConfigElmt("//Paths/TestReleasePaths/iTrekOk2LPath");
                SessionManagerPath = GetConfigElmt("//Paths/TestReleasePaths/iTrekSessionManagerPath");
                //XMLPath = GetConfigElmt("//Paths/TestReleasePaths/iTrekXMLPath");
            }
            else
                //live
            {
                AllXMLPath = @"C:\Program Files\Ufis\iTrekOK2Load\XML\";
                PathsXMLFilePath = AllXMLPath + "Paths.xml";
                applicationBasePath = GetApplicationBasePath(PathsXMLFilePath, "//Paths/applicationBasePathLive");
                OK2LoadPath = GetConfigElmt("//Paths/LiveReleasePaths/iTrekOk2LPath");
                SessionManagerPath = GetConfigElmt("//Paths/LiveReleasePaths/iTrekSessionManagerPath");
                //XMLPath = GetConfigElmt("//Paths/LiveReleasePaths/iTrekXMLPath");
            }
        }

        string GetApplicationBasePath(string XMLFilePathbyMode, string expression)
        {
            XPathDocument doc = new XPathDocument(XMLFilePathbyMode);
            XPathNavigator nav = doc.CreateNavigator();
            return nav.SelectSingleNode(expression).ToString();
        }


		public string GetConfigElmt(string expression)
		{
            XPathDocument doc = new XPathDocument(PathsXMLFilePath);
			XPathNavigator nav = doc.CreateNavigator();
            return nav.SelectSingleNode(expression).ToString();
		}


        public string GetOK2LoadPath()
        {
            if(ApplicationMode == "Debug")
            return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekOk2LPath");
            else
            return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekOk2LPath");
        }


        public string GetFlightsFilePath()
        {
            if(ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekFlightsFilePath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekFlightsFilePath");
        }

        public string GetJobsFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekJobsFilePath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekJobsFilePath");
        }

        //TODO: Check that these are used
        //public string FlightsTestFilePath()
        //{
        //    if (ApplicationMode == "Debug")
        //        return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekFlightsTestFilePath");
        //    else
        //        return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekFlightsTestFilePath");
        //}

        //public string JobsTestFilePath()
        //{
        //    if (ApplicationMode == "Debug")
        //        return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekJobsTestFilePath");
        //    else
        //        return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekJobsTestFilePath");
        //}

        public string GetSessionManagerPath()
        {
            if (ApplicationMode == "Debug")
            return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekSessionManagerPath");
            else
            return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekSessionManagerPath");
        }

        public string GetXMLPath()
        {
            if (ApplicationMode == "Debug")
            return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekXMLPath");
            else
            return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekXMLPath");
        }

        public string GetTOPUSHOK2LOADQueueName()
        {
                return GetConfigElmt("//Paths/TOPUSHOK2LOAD");
        }

        public string GetFROMITREKSELECTQueueName()
        {
            return GetConfigElmt("//Paths/FROMITREKSELECT");
        }

        public string GetTOITREKREAD_RESQueueName()
        {
            return GetConfigElmt("//Paths/TOITREKREAD_RES");
        }

        public string GetFROMITREKUPDATEQueueName()
        {
            return GetConfigElmt("//Paths/FROMITREKUPDATE");
        }

        public string GetSERVICESTIMER()
        {
            return GetConfigElmt("//Paths/SERVICESTIMER");
        }

        public string GetTOITREKREAD_AFTQueueName()
        {
            return GetConfigElmt("//Paths/TOITREKREAD_AFT");
        }

        public string GetTOITREKREAD_JOBQueueName()
        {
            return GetConfigElmt("//Paths/TOITREKREAD_JOB");
        }

        public string GetFROMITREKAUTHQueueName()
        {
            return GetConfigElmt("//Paths/FROMITREKAUTH");
        }

        public string GetTOITREKAUTHQueueName()
        {
            return GetConfigElmt("//Paths/TOITREKAUTH");
        }


        public string GetPapURL()
        {
            return GetConfigElmt("//Paths/papURL/@href");
        }

        public string GetQueueManagerName()
        {
            return GetConfigElmt("//Paths/QUEUEMANAGERNAME");
        }

        public string GetChannelName()
        {
            return GetConfigElmt("//Paths/CHANNELNAME");
        }

        public string GetChannelInfo()
        {
            return GetConfigElmt("//Paths/CHANNELINFO");
        }

        public string GetChannelTransportType()
        {
            return GetConfigElmt("//Paths/CHANNELTRANSPORTTYPE");
        }

        public string GetChannelServer()
        {
            return GetConfigElmt("//Paths/CHANNELSERVER");
        }

        public string GetChannelPortNumber()
        {
            return GetConfigElmt("//Paths/CHANNELPORTNO");
        }

        public string GetTestSessionId()
        {
            return GetConfigElmt("//Paths/TestSessionId");
        }

        public string GetTestSID()
        {
            return GetConfigElmt("//Paths/TestSID");
        }

        public string GetTestStaffId()
        {
            return GetConfigElmt("//Paths/TestStaffId");
        }

        public string GetFileMgrFlushSetting()
        {
            return GetConfigElmt("//Paths/FILEMGRFLUSH");
        }

        public string GetMsgFromQueueDelaySetting()
        {
            return GetConfigElmt("//Paths/GETMSGFROMQUEUEDELAY");
        }

        //public string GetREADFLT_JOB_RESQueue()
        //{
        //    return GetConfigElmt("//Paths/TOITREKREADFLT_JOB_RES");
        //}
    }
}
