using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Xml;
using System.Xml.XPath;
using System.IO;
using NUnit.Framework;
using iTrekXML;
using System.Collections;
using System.Globalization;
using System.Threading;


namespace iTrekXMLTests
{
    [TestFixture]
    public class iTXMLclTests
    {
        iTrekXML.iTXMLcl iTXML;
        iTXMLPathscl iTPaths;
      

        //[SetUp]
        public iTXMLclTests()
        {
            iTPaths = new iTXMLPathscl();
            //iTXML = new iTXMLcl(iTPaths.applicationBasePath + @"Sin\SIN_Client\SIN_UfisAddOn\SIN_iTrek\PushServ\iTrekXML");
            iTXML = new iTXMLcl();

            //sessMan = new iTrekSessionMgr.SessionManager();
        }

        //TODO: Coverage medium
        //Just compares 2 file sizes to ensure slight increase
        //for added element
        //[Test]
        //public void WriteXmlToATDTimingsFileTest()
        //{
        //    //TODO: Add appdomain
        //    long origVal, newVal;
        //    //FileInfo fi = new FileInfo(@"C:\LocalSandboxes\Ufis4.5\Ufis\Sin\SIN_Client\SIN_UfisAddOn\SIN_iTrek\PushServ\iTrekXML\XML\ATDTimings.xml");
        //    FileInfo fi = new FileInfo(iTPaths.applicationBasePath + @"SIN_iTrek\PushServ\iTrekXML\XML\ATDTimings.xml");
        //    origVal = fi.Length;
        //    iTXML.WriteXmlToATDTimingsFile("SID-0000178198", "SQ020", "12:00");
        //    FileInfo fi2 = new FileInfo(iTPaths.applicationBasePath + @"SIN_iTrek\PushServ\iTrekXML\XML\ATDTimings.xml");
        //    newVal = fi2.Length;
        //    Assert.AreNotEqual((decimal)origVal, (decimal)newVal);
        //}

        //TODO: Coverage medium
        //Just compares 2 file sizes to ensure slight increase
        //for added element
        [Test]
        public void WriteStaffSessionandDeviceIDXmlToDeviceIDFileTest()
        { 
            long origVal, newVal;
            FileInfo fi = new FileInfo(iTPaths.GetXMLPath() + "DeviceID.xml");
            origVal = fi.Length;
            iTXML.WriteStaffSessionandDeviceIDXmlToDeviceIDFile(iTPaths.GetTestSessionId(), iTPaths.GetTestSID(), "123456");
            FileInfo fi2 = new FileInfo(iTPaths.GetXMLPath() + "DeviceID.xml");
            newVal = fi2.Length;
            Assert.AreNotEqual((decimal)origVal, (decimal)newVal);
            //iTXML.DeleteDeviceIDEntryByDeviceID(testSID);
        }

        
        //[Test]
        //public void ReadFromJobsXMLFileTest()
        //{
        //    Assert.AreEqual("123456", iTXML.ReadFromJobsXMLFile("//Jobs/Job/@StaffID"));
        //}

        //[Test]
        //public void ReadFromStaffIDXMLFileTest()
        //{
        //    Assert.AreEqual("123456", iTXML.ReadFromStaffIDXMLFile("//staffIDs/staff/@id"));
        //}
        //[Test]
        //public void ReadFromCurrentFlightXMLFileTest()
        //{
        //    //Nb. value below can change if new files are added
        //    //HACK: Improve below
        //    Assert.AreEqual("20060615083", iTXML.ReadFromCurrentFlightXMLFile("//FLIGHT/update/blocks"));
        //}

        //TODO: Coverage medium
        //[Test]
        //public void ReadAttributesForArrayListFromCurrentFlightXMLFileTest()
        //{
        //    ArrayList arrl = iTXML.ReadAttributesForArrayListFromCurrentFlightXMLFile("//FLIGHT");
        //    //Object firstItem = arrl[0].ToString();
        //    //Nb. value below can change if new files are added
        //    //HACK: Improve below
        //    Assert.AreEqual("SQ021", arrl[0].ToString());
        //}

        //TODO: Coverage medium
        [Test]
        public void GetSessionIdFromMsgTest()
        {
            //string message = @"<Msg><SessionId>dt2xbz23qk4vbs45sfvqhljh</SessionId><CurrentMsg>Authenticated</CurrentMsg></Msg>";
            //TODO: This code will need to be made more generic
            string responseMessage = "<AUTH><AUTHENTICATED>true</AUTHENTICATED><SESSIONID>" + iTPaths.GetTestSessionId() + "</SESSIONID></AUTH>";
            Assert.AreEqual(iTPaths.GetTestSessionId(), iTXML.GetSessionIdFromMsg(responseMessage));
        }

        //TODO: Coverage medium
        [Test]
        public void GetUAFTFromMsgTest()
        {
            //string message = @"<Msg><SessionId>dt2xbz23qk4vbs45sfvqhljh</SessionId><CurrentMsg>Authenticated</CurrentMsg></Msg>";
            //TODO: This code will need to be made more generic
            string responseMessage = "<OK2L><ULDNUMBER>AKE123456CX</ULDNUMBER><UAFT>1234567899</UAFT></OK2L>";
            Assert.AreEqual("1234567899", iTXML.GetUAFTFromMsg(responseMessage));
        }

        //TODO: Coverage medium
        [Test]
        public void GetCurrentMsgFromMsgTest()
        { 
            string message = @"<Msg><SessionId>dt2xbz23qk4vbs45sfvqhljh</SessionId><CurrentMsg>Authenticated</CurrentMsg></Msg>";
            Assert.AreEqual("Authenticated", iTXML.GetCurrentMsgFromMsg(message));
        }

        //TODO: Coverage medium
        [Test]
        public void GetMsgTypeFromMsgTest()
        {
            string message = @"<FLIGHTS><FLIGHT><FLNO>SQ020</FLNO></FLIGHT></FLIGHTS>";
            Assert.AreEqual("FLIGHTS", iTXML.GetMsgTypeFromMsg(message));
        }

        //TODO: Coverage medium
        //Nb. test depends on realistic xml - change to generic xml from file
        [Test]
        public void OK2LULDNoTest()
        {
            string message = @"<OK2L><ULDNUMBER>AKE123456CX</ULDNUMBER><UAFT>1234567899</UAFT></OK2L>";
            Assert.AreEqual("AKE123456CX", iTXML.OK2LULDNo(message));
        }

        
         //TODO: Coverage medium
        [Test]
        public void GetBatchFlightsNodeListTest()
        {
            //string message = @"<OK2L><ULDNUMBER>AKE123456CX</ULDNUMBER><UAFT>1234567899</UAFT></OK2L>";
            XmlNodeList lstFlights = iTXML.GetBatchFlightsNodeList("//FLIGHTS/FLIGHT");
            //XmlNode xmln = xmlNodeL.Item[0];
            //Assert.AreEqual("AKE123456CX", xmln.FirstChild.InnerXml);
            foreach (XmlNode FlightNode in lstFlights)
            {
                if (FlightNode.ChildNodes[0].InnerXml == "123456")
                {

                }
            }
            //Assert.AreEqual("AKE123456CX", xmln.FirstChild.InnerXml);
        }

        //TODO: Coverage high
        //This test incorporates delete test
        [Test]
        public void WriteNewFlightToXMLFileTest()
        {
            //TODO: Find a URNO that would never exist for this test
            string message = @"<FLIGHT><URNO>9999999999</URNO><FLNO>QF 011</FLNO><STOD>20060927042000</STOD><STOA>20060927175500</STOA><REGN></REGN><DES3>SIN</DES3><ORG3>LHR</ORG3><ETOD></ETOD><ETOA></ETOA><GTA1></GTA1><GTD1></GTD1><ADID>A</ADID><PSTA></PSTA><PSTD></PSTD></FLIGHT>";
            XmlDocument doc = new XmlDocument();
            DataSet ds = new DataSet();
            ds.ReadXml(iTPaths.GetFlightsFilePath());
            doc.LoadXml(ds.GetXml());
            doc = iTXML.WriteNewFlightToXMLFile(doc, message);
            doc.Save(iTPaths.GetFlightsFilePath());
            //ArrayList al = iTXML.ReadElementsForArrayListFromCurrentFlightXMLFile("//FLIGHTS/FLIGHT");
            Assert.AreEqual("QF 011", iTXML.GetFLNOElmtFromFlightsXML("//FLIGHTS/FLIGHT[URNO='9999999999']/FLNO"));
            //code to remove new entry here
            doc = iTXML.DeleteFlightFromXMLFile(doc, "//FLIGHTS/FLIGHT[URNO='9999999999']");
            doc.Save(iTPaths.GetFlightsFilePath());
            Assert.AreNotEqual("QF 011", iTXML.GetFLNOElmtFromFlightsXML("//FLIGHTS/FLIGHT[URNO='9999999999']/FLNO"));
        } 

        //TODO: Coverage medium
        //add and remove data to make this test more dynamic
        [Test]
        public void GetFLNOElmtFromFlightsXMLTest()
        {
            Assert.AreEqual("QF 010", iTXML.GetFLNOElmtFromFlightsXML("//FLIGHTS/FLIGHT[URNO='1233798753']/FLNO"));
        }
    }
}

namespace iTrekXML
{
    //TODO: Move Flight class?
    public class Flight
    {
        //  Private  Fields
        private String _FLNO;
        private String _STOD;
        private String _STOA;
        private String _REGN;
        private String _DES3;
        private String _ETDI;
        private String _ETOA;
        private String _EQPS;
        private String _ADID;
        private String _POS1;
        private String _URNO;
        private String _DisplayOpenFlights;
        private String _DisplayOpenFlightsMenu;
        

        //  Constructor
        public Flight(string FLNO, string STOD, string STOA, string REGN, string DES3, string ETDI, string ETOA, string EQPS, string ADID, string POS1, string URNO)
        {
            this._FLNO = FLNO;
            this._STOD = STOD;
            this._STOA = STOA;
            this._REGN = REGN;
            this._DES3 = DES3;
            this._ETDI = ETDI;
            this._ETOA = ETOA;
            this._EQPS = EQPS;
            this._ADID = ADID;
            this._POS1 = POS1;
            this._URNO = URNO;

            DateTime dtSTOA = new DateTime();
            DateTime dtSTOD = new DateTime();
            DateTime dtETDI = new DateTime();
            DateTime dtETOA = new DateTime();

            //Determine if departure or arrival flight
            if (STOD == "")
            {
                dtSTOA = Convert.ToDateTime(STOA);
                dtETOA = Convert.ToDateTime(ETOA);
                this._DisplayOpenFlights = FLNO + "-A-" + dtSTOA.ToShortTimeString() + "-" + EQPS;
                this._DisplayOpenFlightsMenu = FLNO + "-STA" + dtSTOA.ToShortTimeString() + "</br>" + REGN + " " + DES3 + " " + "ETA" + " " + dtETOA.ToShortTimeString() + " " + EQPS;
            }
            else
            {
                dtSTOD = Convert.ToDateTime(STOD);
                dtETDI = Convert.ToDateTime(ETDI);
                this._DisplayOpenFlights = FLNO + "-D-" + dtSTOD.ToShortTimeString() + "-" + EQPS;
                this._DisplayOpenFlightsMenu = FLNO + "-STD" + dtSTOD.ToShortTimeString() + " " + REGN + " " + DES3 + " " + "ETD" + " " + dtETDI.ToShortTimeString() + " " + EQPS;
            }
        }
        //  Public  Properties
        public String FLNO { get { return _FLNO; } }
        public String STOD { get { return _STOD; } }
        public String STOA { get { return _STOA; } }
        public String REGN { get { return _REGN; } }
        public String DES3 { get { return _DES3; } }
        public String ETDI { get { return _ETDI; } }
        public String ETOA { get { return _ETOA; } }
        public String EQPS { get { return _EQPS; } }
        public String ADID { get { return _ADID; } }
        public String POS1 { get { return _POS1; } }
        public String URNO { get { return _URNO; } }
        public String DISPLAYOPENFLIGHTS { get { return _DisplayOpenFlights; } }
        public String DISPLAYFLIGHTSMENU { get { return _DisplayOpenFlightsMenu; } }
    }
    /// <summary>
    /// iTrek XML manipulations
    /// </summary>
    public class iTXMLcl
    {
        iTXMLPathscl iTPaths;
        //string sAppDomain = "";
        public iTXMLcl(string AppDomain)
        {
            //sAppDomain = AppDomain;
            iTPaths = new iTXMLPathscl();
        }

        public iTXMLcl()
        {
            iTPaths = new iTXMLPathscl();
           // sAppDomain = iTPaths.applicationBasePath;
        }


        //public string GetConfigElmt(string expression)
        //{
        //    string fileName = iTPaths.applicationBasePath + @"\SIN_iTrek\PushServ\iTrekOk2L\iTrekOk2L.exe.config";
        //    //iTPaths.GetXMLPath()
        //    XPathDocument doc = new XPathDocument(fileName);
        //    XPathNavigator nav = doc.CreateNavigator();
        //    return nav.SelectSingleNode(expression).ToString();
        //}

        //TODO: Check relevance
        //public string GetDeviceIDElmt(string expression)
        //{
        //    //string fileName = iTPaths.applicationBasePath + @"\XML\DeviceID.xml";
        //    string fileName = iTPaths.GetXMLPath() + "DeviceID.xml";
        //    XPathDocument doc = new XPathDocument(fileName);
        //    XPathNavigator nav = doc.CreateNavigator();
        //    return nav.SelectSingleNode(expression).ToString();
        //}

        public XmlDocument TransformStringToXmlDoc(string xmlString)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlString);
            return xmlDoc;
        }

        public string GetFLNOElmtFromFlightsXML(string expression)
        {
            try
            {
                string fileName = iTPaths.GetFlightsFilePath();
                XPathDocument doc = new XPathDocument(fileName);
                XPathNavigator nav = doc.CreateNavigator();
                return nav.SelectSingleNode(expression).ToString();
            }
            catch(Exception e)
            {
                return e.Message;
            }
        }

        //TODO: Used?
        public string GetStaffIDElmt(string expression)
        {
            string fileName = iTPaths.GetXMLPath() + "StaffID.xml";
            XPathDocument doc = new XPathDocument(fileName);
            XPathNavigator nav = doc.CreateNavigator();
            return nav.SelectSingleNode(expression).ToString();
        }

        //TODO: Used?
        public string GetPasswordIDElmt(string expression)
        {
            string fileName = iTPaths.GetXMLPath() + "Password.xml";
            XPathDocument doc = new XPathDocument(fileName);
            XPathNavigator nav = doc.CreateNavigator();
            return nav.SelectSingleNode(expression).ToString();
        }

        public XmlDocument WriteNewFlightToXMLFile(XmlDocument flightXMLdoc, string flightXML)
        {
            XmlDocument newFlightXML = new XmlDocument();
            newFlightXML.LoadXml(flightXML);
            XmlElement newFlightXMLElement = newFlightXML.DocumentElement;
            XmlNode xmlNode = flightXMLdoc.DocumentElement;
            xmlNode.AppendChild(flightXMLdoc.ImportNode(newFlightXMLElement,true));
            return flightXMLdoc;
        }

        public XmlDocument DeleteFlightFromXMLFile(XmlDocument flightXMLdoc, string expression)
        {
            XmlNode xmlNode = flightXMLdoc.SelectSingleNode(expression);
            flightXMLdoc.DocumentElement.RemoveChild(xmlNode);
            return flightXMLdoc;
        } 

        //public void WriteXmlToOK2LoadsFile(string deviceID, string flightno, string OK2LoadAlertTime)
        public void WriteXmlToOK2LoadsFile(string deviceID, string flightno, string ULDid)
        {
            string OK2LoadsXMLFile = iTPaths.GetXMLPath() + "OK2Loads.xml";
            XmlDocument doc = new XmlDocument();
            DataSet ds = new DataSet();
            ds.ReadXml(OK2LoadsXMLFile);
            doc.LoadXml(ds.GetXml());
            //XmlElement root = doc.DocumentElement;
            XmlNode xmlNode = doc.DocumentElement;
            //Create  a  new  node.
            XmlElement elem = doc.CreateElement("OK2LoadAlert");
            //elem.InnerText="19.95";
            xmlNode.AppendChild(elem);
            // Add a new attribute.
            elem.SetAttribute("ULD", ULDid);
            elem.SetAttribute("FlightNo", flightno);
            elem.SetAttribute("OK2LoadTime", DateTime.Now.ToString());
            //root.SetAttribute("Timing", "12:00");
            doc.Save(OK2LoadsXMLFile);

        }

        //public string ReadFromJobsXMLFile(string expression)
        //{
        //    //TODO: appDomain
        //    //string fileName = iTPaths.applicationBasePath + @"SIN_iTrek\PushServ\iTrekXML\XML\Jobs.xml";
        //    string fileName = iTPaths.GetXMLPath() + @"\Jobs\Jobs.xml";
        //    XPathDocument doc = new XPathDocument(fileName);
        //    XPathNavigator nav = doc.CreateNavigator();
        //    return nav.SelectSingleNode(expression).ToString();

        //}

        //public string ReadFromStaffIDXMLFile(string expression)
        //{
        //    //TODO: appDomain
        //    string fileName = iTPaths.applicationBasePath + @"SIN_iTrek\PushServ\iTrekXML\XML\StaffID.xml";
        //    XPathDocument doc = new XPathDocument(fileName);
        //    XPathNavigator nav = doc.CreateNavigator();
        //    return nav.SelectSingleNode(expression).ToString();

        //}

        //public string ReadFromCurrentFlightXMLFile(string expression)
        //{
        //    //TODO: appDomain
        //    DirectoryInfo di = new DirectoryInfo("c:/temp/tests");
        //    FileInfo[] rgFiles = di.GetFiles("*.xml");
        //    XPathDocument doc = new XPathDocument(rgFiles[rgFiles.Length - 1].FullName);
        //    XPathNavigator nav = doc.CreateNavigator();
        //    return nav.SelectSingleNode(expression).ToString();

        //}

        public ArrayList ReadAssignedFlightElementByStaffIDFromCurrentJobsXMLFile(string expression, string staffid)
        {
            
            //TODO: appDomain
            DirectoryInfo di = new DirectoryInfo(iTPaths.GetXMLPath() + @"\Jobs\");
            FileInfo[] rgFiles = di.GetFiles("*.xml");
            string fileName = rgFiles[rgFiles.Length - 1].FullName;

            ArrayList al = new ArrayList();

           // XmlNodeList FlightList;
            XmlNodeList JobList;

            XmlDocument myXmlDocument = new XmlDocument();
            myXmlDocument.Load(fileName);
            //FlightList = myXmlDocument.SelectNodes(expression);
            JobList = myXmlDocument.SelectNodes(expression);

            if (JobList.Count > 0)
            {
                foreach (XmlNode JobNode in JobList)
                {
                    if (JobNode.ChildNodes[1].InnerXml == staffid)
                    { 
                        XmlNode flightAssign = JobNode.ChildNodes[2];
                        al.Add(flightAssign.InnerXml);
                    }
                }
            }
            //only fetch last 3 flights
            //al.RemoveRange(2, al.Count - 3);
            return al;
        }

        //public ArrayList ReadAttributesForArrayListFromCurrentFlightXMLFile(string expression)
        //{
        //    //TODO: appDomain
        //    DirectoryInfo di = new DirectoryInfo("c:/temp/tests");
        //    FileInfo[] rgFiles = di.GetFiles("*.xml");
        //    string fileName = rgFiles[rgFiles.Length - 1].FullName;

        //    ArrayList al = new ArrayList();

        //    XmlNodeList DeviceList;

        //    XmlDocument myXmlDocument = new XmlDocument();
        //    myXmlDocument.Load(fileName);
        //    DeviceList = myXmlDocument.SelectNodes(expression);

        //    if (DeviceList.Count > 0)
        //    {
        //        foreach (XmlNode DeviceNode in DeviceList)
        //        {
        //               al.Add(DeviceNode.Attributes.Item(0).Value);
        //               //al.Add(fileName); 
        //        }
        //    }
        //    //only fetch last 3 flights
        //    al.RemoveRange(2, al.Count - 3);
        //    return al;
        //}

        public ArrayList ReadElementsForArrayListFromCurrentFlightXMLFile(string expression)
        {
            //TODO: appDomain
            //DirectoryInfo di = new DirectoryInfo("c:/temp/tests");
            DirectoryInfo di = new DirectoryInfo(iTPaths.GetXMLPath() + @"\FLIGHTS\");
            //wildcard used to obtain latest in array of files if that becomes necessary
            FileInfo[] rgFiles = di.GetFiles("*.xml");
            string fileName = rgFiles[rgFiles.Length - 1].FullName;

            ArrayList al = new ArrayList();

            XmlNodeList FlightList;

            XmlDocument myXmlDocument = new XmlDocument();
            myXmlDocument.Load(fileName);
            FlightList = myXmlDocument.SelectNodes(expression);
           
            if (FlightList.Count > 0)
            {
                foreach (XmlNode FlightNode in FlightList)
                {
                    //"SQ020","stod", "stoa","regn","des3","etdi", "ETOA", "EQPS", ADID, POS1, "urno"));
                    al.Add(new Flight(FlightNode.ChildNodes[0].InnerXml, FlightNode.ChildNodes[1].InnerXml, FlightNode.ChildNodes[2].InnerXml, FlightNode.ChildNodes[3].InnerXml, FlightNode.ChildNodes[4].InnerXml, FlightNode.ChildNodes[5].InnerXml, FlightNode.ChildNodes[6].InnerXml, FlightNode.ChildNodes[7].InnerXml, FlightNode.ChildNodes[8].InnerXml, FlightNode.ChildNodes[9].InnerXml, FlightNode.ChildNodes[10].InnerXml));                 
                }
            }
            //only fetch last 3 flights
            if (al.Count != 0 && al.Count < 4)
                return al;
            else
            {
                al.RemoveRange(2, al.Count - 3);
                return al;
            }
        }

        public ArrayList UserSessionReadElementsForArrayListFromCurrentFlightXMLFile(string sessid, string expression)
        {
            //TODO: appDomain
            DirectoryInfo di = new DirectoryInfo(iTPaths.GetXMLPath() + sessid + @"\");
            FileInfo[] rgFiles = di.GetFiles("Flights.xml");
            string fileName = rgFiles[rgFiles.Length - 1].FullName;

            ArrayList al = new ArrayList();

            XmlNodeList FlightList;

            XmlDocument myXmlDocument = new XmlDocument();
            myXmlDocument.Load(fileName);
            FlightList = myXmlDocument.SelectNodes(expression);

            if (FlightList.Count > 0)
            {
                foreach (XmlNode FlightNode in FlightList)
                {
                    //"SQ020","stod", "stoa","regn","des3","etdi", "ETOA", "EQPS", ADID, POS1, "urno"));
                    al.Add(new Flight(FlightNode.ChildNodes[0].InnerXml, FlightNode.ChildNodes[1].InnerXml, FlightNode.ChildNodes[2].InnerXml, FlightNode.ChildNodes[3].InnerXml, FlightNode.ChildNodes[4].InnerXml, FlightNode.ChildNodes[5].InnerXml, FlightNode.ChildNodes[6].InnerXml, FlightNode.ChildNodes[7].InnerXml, FlightNode.ChildNodes[8].InnerXml, FlightNode.ChildNodes[9].InnerXml, FlightNode.ChildNodes[10].InnerXml));
                }
            }
            //only fetch last 3 flights
            if (al.Count != 0 && al.Count < 4)
                return al;
            else
            {
                al.RemoveRange(2, al.Count - 3);
                return al;
            }
        }

        public XmlNodeList GetBatchFlightsNodeList(string expression)
        {
            //TODO: appDomain
            //DirectoryInfo di = new DirectoryInfo("c:/temp/tests");
            DirectoryInfo di = new DirectoryInfo(iTPaths.GetXMLPath() + @"\FLIGHTS\");
            FileInfo[] rgFiles = di.GetFiles("*.xml");
            string fileName = rgFiles[rgFiles.Length - 1].FullName;

            ArrayList al = new ArrayList();

            XmlNodeList FlightList;

            XmlDocument myXmlDocument = new XmlDocument();
            myXmlDocument.Load(fileName);
            FlightList = myXmlDocument.SelectNodes(expression);
            return FlightList;
        }



        public DataSet GetFlightDataSetFromCurrentFlightXMLFile()
        {
            //TODO: appDomain
            //DirectoryInfo di = new DirectoryInfo("c:/temp/tests");
            DirectoryInfo di = new DirectoryInfo(iTPaths.GetXMLPath() + @"/FLIGHTS/");
            FileInfo[] rgFiles = di.GetFiles("*.xml");
            string fileName = rgFiles[rgFiles.Length - 1].FullName;

            DataSet ds = new DataSet();

            ds.ReadXml(fileName);
     
            //only fetch last 3 flights
            for (int i = 3; i < ds.Tables[0].Rows.Count; i++)
                ds.Tables[0].Rows[i].Delete();
            ds.Tables[0].AcceptChanges();
          
            return ds;
        }

        public ArrayList ReadAttributesForArrayListFromCurrentOK2LoadXMLFile()
        {
            //TODO: appDomain
            //will need to point this at relevant session directory
            //DirectoryInfo di = new DirectoryInfo(iTPaths.GetXMLPath());
            //FileInfo[] rgFiles = di.GetFiles("OK2Loads.xml");
            //string fileName = rgFiles[rgFiles.Length - 1].FullName;

            ArrayList al = new ArrayList();

            XmlNodeList DeviceList;

            XmlDocument myXmlDocument = new XmlDocument();
            myXmlDocument.Load(iTPaths.GetXMLPath() + "OK2Loads.xml");
            DeviceList = myXmlDocument.SelectNodes("//OK2Loads/OK2LoadAlert");

            if (DeviceList.Count > 0)
            {
                foreach (XmlNode DeviceNode in DeviceList)
                {
                    if (DeviceNode.Attributes.Item(0).Value != null
                        && DeviceNode.Attributes.Item(1).Value != null)
                    {
                        al.Add(DeviceNode.Attributes.Item(0).Value);
                        al.Add(DeviceNode.Attributes.Item(1).Value);
                    }
                  
                    //al.Add(fileName); 
                }
            }

            //al.RemoveRange(2, al.Count - 3);
            return al;
        }

        public string OK2LULDNo(string message)
        {
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(message);
            //XmlNode xmln = xmldoc.ChildNodes[1];
            XmlNode xmln = xmldoc.SelectSingleNode("//OK2L/ULDNUMBER");
            //ChildNodes[0] = ULD
            //XmlNode xmlnULD = xmln.ChildNodes[0];
            //return xmlnULD.InnerXml;
            return xmln.InnerXml;
        }

        public string OK2LAirline(string message)
        {
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(message);
            XmlNode xmln = xmldoc.ChildNodes[1];
            //ChildNodes[1] = Airline
            XmlNode xmlnULD = xmln.ChildNodes[1];
            return xmlnULD.InnerXml;
        }

        public string OK2LFlightNo(string message)
        {
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(message);
            XmlNode xmln = xmldoc.ChildNodes[1];
            XmlNode xmlnULD = xmln.ChildNodes[2];
            return xmlnULD.InnerXml;
        }

        public string OK2LDate(string message)
        {
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(message);
            XmlNode xmln = xmldoc.ChildNodes[1];
            XmlNode xmlnULD = xmln.ChildNodes[3];
            return xmlnULD.InnerXml;
        }

        public string OK2LDestination(string message)
        {
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(message);
            XmlNode xmln = xmldoc.ChildNodes[1];
            XmlNode xmlnULD = xmln.ChildNodes[4];
            return xmlnULD.InnerXml;
        }

        public string GetSessionIdFromMsg(string message)
        {
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(message);
            XmlNodeList xmlnl = xmldoc.FirstChild.ChildNodes;
            string sessid = "";
            foreach (XmlNode infonode in xmlnl)
            {
                if (infonode.Name == "SESSIONID")
                    sessid = infonode.InnerXml;
            }  
            return sessid;
        }

        public string GetUAFTFromMsg(string message)
        {
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(message);
            XmlNodeList xmlnl = xmldoc.FirstChild.ChildNodes;
            string sessid = "";
            foreach (XmlNode infonode in xmlnl)
            {
                if (infonode.Name == "UAFT")
                    sessid = infonode.InnerXml;
            }
            return sessid;
        }

        public string GetCurrentMsgFromMsg(string message)
        {
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(message);
            XmlNode xmln = xmldoc.ChildNodes[0];
            XmlNode xmlnCurrentMsg = xmln.ChildNodes[1];
            return xmlnCurrentMsg.InnerXml;
        }

        public string GetMsgTypeFromMsg(string message)
        {
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(message);
            //XmlNode xmln = xmldoc.DocumentElement;
            //XmlNode xmlnCurrentMsg = xmln.ChildNodes[1];
            
            //return xmldoc.DocumentElement.Value.ToString();
            return xmldoc.DocumentElement.Name;
        }

        //TODO: Refactor this method to write to a session.xml file which more 
        //accurately describes it's purpose
        public void WriteStaffSessionandDeviceIDXmlToDeviceIDFile(string sessid, string deviceID, string staffid)
        {
            string DeviceIDFile = iTPaths.GetXMLPath() + "DeviceID.xml";
            XmlDocument doc = new XmlDocument();
            DataSet ds = new DataSet();
            ds.ReadXml(DeviceIDFile);
            doc.LoadXml(ds.GetXml());
            //XmlElement root = doc.DocumentElement;
            XmlNode xmlNode = doc.DocumentElement;
            //Create  a  new  node.
            XmlElement elem = doc.CreateElement("devices");
            //elem.InnerText="19.95";
            xmlNode.AppendChild(elem);
            // Add a new attribute.
            elem.SetAttribute("sessionid", sessid);
            elem.SetAttribute("deviceid", deviceID);
            elem.SetAttribute("staffid", staffid);
            elem.SetAttribute("time", DateTime.Now.ToString());
            //root.SetAttribute("Timing", "12:00");
            doc.Save(DeviceIDFile);
           
        }


        public void DeleteDeviceIDEntryByDeviceID(string deviceid)
        {
            string DeviceIDFile = iTPaths.GetXMLPath() + "DeviceID.xml";
            XmlDocument doc = new XmlDocument();
            DataSet ds = new DataSet();
            ds.ReadXml(DeviceIDFile);
            doc.LoadXml(ds.GetXml());
            XmlNodeList DeviceIDNodeLst = doc.SelectNodes("//deviceIDs/devices");

            if (DeviceIDNodeLst.Count > 0)
            {
                foreach (XmlNode DeviceNode in DeviceIDNodeLst)
                {
                    if (DeviceNode.Attributes.Item(1).Value == deviceid)
                    {
                        DeviceNode.ParentNode.RemoveChild(DeviceNode);
                    }
                }
            }
            doc.Save(DeviceIDFile);
        }

        public void DeleteDeviceIDEntryBySessID(string sessid)
        {
            string DeviceIDFile = iTPaths.GetXMLPath() + "DeviceID.xml";
            XmlDocument doc = new XmlDocument();
            DataSet ds = new DataSet();
            ds.ReadXml(DeviceIDFile);
            doc.LoadXml(ds.GetXml());
            XmlNodeList DeviceIDNodeLst = doc.SelectNodes("//deviceIDs/devices");

            if (DeviceIDNodeLst.Count > 0)
            {
                foreach (XmlNode DeviceNode in DeviceIDNodeLst)
                {
                    if (DeviceNode.Attributes.Item(0).Value == sessid)
                    {
                        DeviceNode.ParentNode.RemoveChild(DeviceNode);
                    }
                }
            }
            doc.Save(DeviceIDFile);
        }
    }
}
