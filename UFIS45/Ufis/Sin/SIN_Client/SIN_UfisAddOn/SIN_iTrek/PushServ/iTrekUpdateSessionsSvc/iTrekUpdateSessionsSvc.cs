#define LoggingOn
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using IBM.WMQ;
using System.Timers;
using System.Configuration;
using iTrekWMQ;
using System.IO;
using iTrekXML;
//using NUnit.Framework;
using System.Xml;
using iTrekSessionMgr;



//NB. Tests at bottom of file
namespace iTrekUpdateSessionsSvc
{
    /// <summary>
    /// The flight and job xml files are constantly being updated.
    /// These updates must be reflected in the session directories
    /// that the users are actually using to obtain their data.
    /// This service constantly reviews the current session directories
    /// and reflects changes from the latest xml files in them.
    /// </summary>

    public partial class iTrekUpdateSessionsSvc : ServiceBase
    {
        iTXMLcl iTXML;
        iTXMLPathscl iTPaths;
        SessionManager sessMan;
       

        public iTrekUpdateSessionsSvc()
        {
            InitializeComponent();
#if LoggingOn
            iTrekUpdateSessionsSvcLog.WriteEntry("Initialisation iTrekUpdateSessionsSvc");
#endif
            iTXML = new iTXMLcl();
            iTPaths = new iTXMLPathscl();
            sessMan = new SessionManager();
            //#endif
        }

        protected override void OnStart(string[] args)
        {
#if LoggingOn
            iTrekUpdateSessionsSvcLog.WriteEntry("In OnStart");
#endif
            MessageQueueTimer();
        }

        protected override void OnStop()
        {
#if LoggingOn
            iTrekUpdateSessionsSvcLog.WriteEntry("In onStop.");
#endif
        }

        public void MessageQueueTimer()
        {
            System.Timers.Timer aTimer = new System.Timers.Timer();
            aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            aTimer.Interval = Convert.ToInt32(iTPaths.GetSERVICESTIMER());
            aTimer.Enabled = true;
        }

        // Specify what you want to happen when the Elapsed event is raised.
        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            iTXMLPathscl iTPaths = new iTXMLPathscl();
            //Nb. directories must have been created by users for
            //anything to happen here
            DirectoryInfo di = new DirectoryInfo(iTPaths.GetXMLPath());
            DirectoryInfo[] diArr = di.GetDirectories();
            for (int i = 0; i < diArr.Length; i++)
            {
                XmlDocument Flightsxmldoc = new XmlDocument();
                //ensure it's a session dir
                if (diArr[i].Name.Length > 20)
                {
                    try
                    {
                        //There must be a session dir listed in deviceid file
                        //string PENO = iTXML.GetPENOBySessionID(diArr[i].Name);
#if LoggingOn
                        iTrekUpdateSessionsSvcLog.WriteEntry(diArr[i].Name);
#endif
                        string PENO = sessMan.GetPENOBySessionID(diArr[i].Name);
#if LoggingOn
                        iTrekUpdateSessionsSvcLog.WriteEntry("PENO: " + PENO);
#endif
                        Flightsxmldoc = sessMan.GetFlightsByStaffID(PENO, iTPaths.GetJobsFilePath(), iTPaths.GetFlightsFilePath());
                        if(Flightsxmldoc != null)
                         sessMan.CreateXMLFileInSessionDirectory(diArr[i].Name, Flightsxmldoc, "Flights.xml");
                    }
                    catch(Exception ex)
                    {
                        
#if LoggingOn
                        iTrekUpdateSessionsSvcLog.WriteEntry(ex.Message + "" + diArr[i].Name);
#endif
                        }
                }
            }
        }
    }
}
