using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Globalization;
using System.IO;
using System.Diagnostics;
using iTrekXML;

namespace iTrekUtils
{
    /// <summary>
    /// iTrek utilities
    /// </summary>
    public class iTUtils
    {
        iTXMLPathscl iTPaths;

        public static int GetRandomNumber(int iMin, int iMax)
        {
            Random rand = new Random();
            try
            {
                return rand.Next(iMin, iMax);
            }
            finally
            {
                rand = null;
            }
        }

       

        //public static void WriteXMLLog(bool DIAGNOSTICS_REQUIRED, string Level, string Type, string TypeDetail, string Device, string LogDetail, bool DEVICE_DIAGNOSTICS_REQUIRED)
        //{
        //    if (DIAGNOSTICS_REQUIRED == true & DEVICE_DIAGNOSTICS_REQUIRED == true)
        //    {
        //        int lengthDirectory;
        //        string strFileName = "c:\\temp\\qacaf\\Log" + Format(DateTime.Today, "dd-MM-yyyy") + ".xml";
        //        lengthDirectory = InStrRev(strFileName, "\\") - 1;
        //        LogDetail = LogDetail.Replace("<![CDATA[", "");
        //        LogDetail = LogDetail.Replace("]]>", "");
        //        object strContents = "<log><datetime>" + System.DateTime.Now + "</datetime><level>" + Level + "</level><type>" + Type + "</type><typedetail>" + TypeDetail + "</typedetail><device>" + Device + "</device><logdetail><![CDATA[" + LogDetail + "]]></logdetail></log>";
        //        try
        //        {
        //            System.IO.Directory outputDirectory;
        //            outputDirectory.CreateDirectory(Mid(strFileName, 1, lengthDirectory));
        //        }
        //        catch (Exception e)
        //        {
        //        }
        //        try
        //        {
        //            if ((File.Exists(strFileName)))
        //            {
        //                System.IO.StreamWriter writer = File.AppendText(strFileName);
        //                writer.Write(strContents);
        //                writer.Close();
        //            }
        //            else
        //            {
        //                System.IO.TextWriter writer;
        //                writer = new System.IO.StreamWriter(strFileName);
        //                writer.Write(strContents);
        //                writer.Close();
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //        }
        //    }
        //}

        public static string ReadFromFile(string strFileName)
        {
            try
            {
                System.IO.TextReader reader;
                reader = new System.IO.StreamReader(strFileName);
                string sreaderData = reader.ReadToEnd();
                reader.Close();
                return sreaderData;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string GetMachineName()
        {
            return System.Environment.MachineName;
        }


        public static string ConvertStreamToString(System.IO.Stream theStream)
        {
            System.IO.StreamReader srReader;
            srReader = new System.IO.StreamReader(theStream, System.Text.Encoding.ASCII);
            return srReader.ReadToEnd();
        }

        public static void LogToEventLog(string strMessage)
        {
            try
            {
                string strLogSrc = "QACAF";
                string strLogName = "QACAFLogs";
                System.Diagnostics.EventLog EventLog1 = new System.Diagnostics.EventLog();
                if (!(EventLog.SourceExists(strLogSrc)))
                {
                    EventLog.CreateEventSource(strLogSrc, strLogName);
                }
                EventLog1.Source = strLogSrc;
                EventLog1.Log = strLogName;
                EventLog1.WriteEntry(strMessage + "(Time:" + DateTime.Now.ToString() + ":" + DateTime.Now.Millisecond.ToString() + ")");
            }
            catch
            {
            }
        }

    }
}
