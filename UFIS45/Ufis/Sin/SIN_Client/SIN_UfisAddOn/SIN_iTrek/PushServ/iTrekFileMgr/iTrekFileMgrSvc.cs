using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using IBM.WMQ;
using System.Timers;
using System.Configuration;
using iTrekWMQ;
using System.IO;
using iTrekXML;
using NUnit.Framework;
using System.Xml;
using iTrekSessionMgr;

//Tests at bottom of page to work with designer
namespace iTrekFileMgr
{
    /// <summary>
    /// Gets flight and job xml data from queues and writes it to 
    /// flight and job xml files on disk
    /// </summary>
    
    public partial class iTrekFileMgrSvc : ServiceBase
    {
        iTXMLcl iTXML;
        iTXMLPathscl iTPaths;
        SessionManager sessMan;
        iTrekWMQClass TOITREKREAD_RESQ;
        int UpdateFlushCounter = 0;
        XmlDocument InMemoryFlightsXml = new XmlDocument();
        XmlDocument InMemoryJobsXml = new XmlDocument();

        public iTrekFileMgrSvc()
        {
            InitializeComponent();
            //#if DEBUG
            iTFileMgrLog.WriteEntry("Initialisation File Mgr");
            iTXML = new iTXMLcl();
            iTPaths = new iTXMLPathscl();
            //sessMan = new SessionManager();
            //TOITREKREAD_RESQ = new iTrekWMQClass(iTPaths.GetTOITREKREAD_RESQueueName());
//#endif
        }

        protected override void OnStart(string[] args)
        {
//#if DEBUG
            iTFileMgrLog.WriteEntry("In OnStart");
//#endif
            MessageQueueTimer();
//#if DEBUG
            iTFileMgrLog.WriteEntry("Finished in MessageQueueTimer");
//#endif
        }

        protected override void OnStop()
        {
//#if DEBUG
            iTFileMgrLog.WriteEntry("In onStop.");
//#endif
        }

        public void MessageQueueTimer()
        {  
           //#if DEBUG
            iTFileMgrLog.WriteEntry("In MessageQueueTimer");
//#endif
            System.Timers.Timer aTimer = new System.Timers.Timer();
            aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            // Set the Interval to 5 seconds.
            aTimer.Interval = 5000;  
            aTimer.Enabled = true;
        }

        // Specify what you want to happen when the Elapsed event is raised.
        private void OnTimedEvent(object source, ElapsedEventArgs e)
        { 
            string message = "";
            iTFileMgrLog.WriteEntry("In OnTimedEvent");
            
            iTXMLPathscl iTPaths = new iTXMLPathscl();
            iTrekWMQClass TOITREKREADFLT_JOB_RES = new iTrekWMQClass(iTPaths.GetTOITREKREADFLT_JOB_RESQueueName());
            message = TOITREKREADFLT_JOB_RES.GetMessageFromTheQueue();

            string messageType = "";
            messageType = iTXML.GetMsgTypeFromMsg(message);

            UpdateFlushCounter++;

            //Convert.toInt?
            if (UpdateFlushCounter == Convert.ToInt32(iTPaths.GetFileMgrFlushSetting()))
            {
                InMemoryFlightsXml.Save(iTPaths.GetXMLPath() + @"/Flights/Flights.xml");
                InMemoryJobsXml.Save(iTPaths.GetXMLPath() + @"/Jobs/Jobs.xml");
            }
            
            if (message == "MQRC_NO_MSG_AVAILABLE")
            {
                //Do nothing
            }
            else if(messageType == "FlightsBatch")
            {
                InMemoryFlightsXml.LoadXml(message);
                InMemoryFlightsXml.Save(iTPaths.GetXMLPath() + @"/Flights/Flights.xml");

//#if DEBUG
                iTFileMgrLog.WriteEntry(message);
//#endif
            }
            else if (messageType == "JobsBatch")
            {
                InMemoryJobsXml.LoadXml(message);
                InMemoryJobsXml.Save(iTPaths.GetXMLPath() + @"/Jobs/Jobs.xml");
            }

            else if (messageType == "FLTDEL")
            {
                //do FLTDEL update on InMemoryFlightsXml
            }

            else if (messageType == "FLTINS")
            {
                //do FLTINS update on InMemoryFlightsXml
                //
            }

            else if (messageType == "FLTUPD")
            {
                //do FLTUPD update on InMemoryFlightsXml
            }

            else if (messageType == "JOBDEL")
            {
                //do JOBDEL update on InMemoryJobsXml
            }
            else if (messageType == "JOBINS")
            {
                //do JOBINS update on InMemoryJobsXml
            }
            else if (messageType == "JOBUPD")
            {
                //do JOBUPD update on InMemoryJobsXml
            }
            else
            {
                //do NOTHING
            }
        }
    }
}

namespace iTrekFlightMgrSvcTests
{
    [TestFixture]
    public class iTrekFileMgrTests
    {

        //[SetUp]
        public iTrekFileMgrTests()
        {
            
        }
        //TODO: Coverage high
        //If fail - check service already running
        [Test]
        public void CaniTrekFileMgrSvcRunTest()
        {
            string message = "";
            System.ServiceProcess.ServiceController sc = new System.ServiceProcess.ServiceController("iTrekFileMgrSvc");
            sc.Start();
            for (int i = 1; i <= 6000000; i++)
            {
                //do nothing, delay for service to start
                //increase number if test failing
            }
            message = sc.Status.ToString();
            sc.Stop();
            sc.Dispose();
            Assert.AreEqual("Running", message);
        }
    }
}


