using System;
using System.Collections.Generic;
using System.Text;
//using System.Data;
using System.Xml;
using System.Xml.XPath;
using System.IO;
using NUnit.Framework;
using iTrekXML;
using System.Collections;
using iTrekFileMgr;

//namespace iTrekFileMgrTests
//{
//    [TestFixture]
//    public class iTrekFileMgrTests
//    {
//        iTXMLPathscl iTPaths;
//        DirectoryInfo di;
//        iTrekFileMgrcl iTFm;

//        //[SetUp]
//        public iTrekFileMgrTests()
//        {
//            iTPaths = new iTXMLPathscl();
//            iTFm = new iTrekFileMgrcl();
//        }

//        //TODO: - Coverage - High
//        [Test]
//        public void CreatNewDirectoryBySessionStaffAndDeviceIDTest()
//        {
//            string newdirPath = iTFm.CreatNewDirectoryBySessionStaffAndDeviceID(iTPaths.GetXMLPath(), "123456abc", "22", "0000178198");
//            di = new DirectoryInfo(newdirPath);
//            Assert.AreEqual(iTPaths.GetXMLPath() + @"123456abc-22-0000178198", di.FullName);
//            //clean up
//            di.Delete();
//        }
//    }
//}

namespace iTrekFileMgr
{
   
    public class iTrekFileMgrcl
    {
        DirectoryInfo di;
        iTXMLPathscl iTPaths;
            //sessMan = new iTrekSessionMgr.SessionManager();
            //HACK: change from hardcode


        public iTrekFileMgrcl()
        {
              iTPaths = new iTXMLPathscl();
         }

        public string CreatNewDirectoryBySessionStaffAndDeviceID(string dirPath, string sessID, string staffID, string devID)
        {
             di = new DirectoryInfo(dirPath);
             string subdirPath = sessID + "-" + staffID + "-" + devID;
             di.CreateSubdirectory(subdirPath);
             DirectoryInfo[] diArr = di.GetDirectories(subdirPath);
             return diArr[0].FullName;
        }
    
    }
       
}
