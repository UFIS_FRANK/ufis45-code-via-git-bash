using System;
using System.Collections.Generic;
using System.Text;
using IBM.WMQ;
//using NUnit.Framework;
using System.Configuration;
using System.Collections.Specialized;
using iTrekXML;
//using iTrekXML;
//using iTrekOk2L;
//using System.Security.Policy;   //for  evidence  object
//TODO: consolidate this class with the other WMQs
namespace iTrekFileMgr
{
    public class iTFileMgrWMQ
    {
        MQQueueManager queueManager;
        MQQueue queue;
        MQMessage queueMessage;
        MQPutMessageOptions queuePutMessageOptions;
        MQGetMessageOptions queueGetMessageOptions;

        static string QueueName;
        static string QueueManagerName;
        static string ChannelInfo;

        string channelName;
        string transportType;
        string connectionName;

        byte[] MessageID;

        iTXMLPathscl iTPaths;

        public iTFileMgrWMQ(string qname)
        {
            iTPaths = new iTXMLPathscl();
            QueueName = qname;
            QueueManagerName = "venus.queue.manager";
            ChannelInfo = "CHANNEL1/TCP/192.168.1.8";

            char[] separator = { '/' };
            string[] ChannelParams;
            ChannelParams = ChannelInfo.Split(separator);
            channelName = ChannelParams[0];
            transportType = ChannelParams[1];
            connectionName = ChannelParams[2];
        }

        public void PurgeQueue()
        {
            iTFileMgrWMQ mqObj = new iTFileMgrWMQ("ORANGE.QUEUE");
            while (mqObj.GetMessageFromTheQueue() != "MQRC_NO_MSG_AVAILABLE")
            {
                mqObj.GetMessageFromTheQueue();
            }
        }

        public string putTheMessageIntoQueue(string message)
        {
            try
            {
                queueManager = new MQQueueManager(QueueManagerName, channelName, connectionName);
                queue = queueManager.AccessQueue(QueueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);

                
                int msgLen = message.Length;

                while (msgLen == 0)
                {
                    if (msgLen == 0)
                        return "Add a message parameter";   
                    msgLen = message.Length;
                }

                queueMessage = new MQMessage();
                queueMessage.WriteString(message);
                queueMessage.Format = MQC.MQFMT_STRING;
                queuePutMessageOptions = new MQPutMessageOptions();

                //putting the message into the queue
                queue.Put(queueMessage, queuePutMessageOptions);
                MessageID = queueMessage.MessageId;

                return "Success";
            }
            catch (Exception mqexp)
            {
                return "MQSeries Exception: " + mqexp.Message;
            }
        }

        public string GetMessageFromTheQueue()
        {
            queueManager = new MQQueueManager(QueueManagerName, channelName, connectionName);

            queue = queueManager.AccessQueue(QueueName, MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
            queueMessage = new MQMessage();
            queueMessage.Format = MQC.MQFMT_STRING;
            //we can disable message id for now
            //but will need it later
            //queueMessage.MessageId = MessageID;
            queueGetMessageOptions = new MQGetMessageOptions(); 
            try
            {
                queue.Get(queueMessage, queueGetMessageOptions);
                return queueMessage.ReadString(queueMessage.MessageLength);  
            }
            catch (Exception MQExp)
            {
                return MQExp.Message;
            }
        }
    }
}

