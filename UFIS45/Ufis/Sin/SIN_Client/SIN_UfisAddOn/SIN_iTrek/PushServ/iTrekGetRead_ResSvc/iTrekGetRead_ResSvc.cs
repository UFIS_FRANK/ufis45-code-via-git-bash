#define LoggingOn
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using IBM.WMQ;
using System.Timers;
using System.Configuration;
using iTrekWMQ;
using System.IO;
using iTrekXML;
using System.Xml;
using iTrekSessionMgr;

//NB. Tests at bottom of file
namespace iTrekGetRead_ResSvc
{
    /// <summary>
    /// When a request for data has been made by iTrek and put on the 
    /// FROM.ITREK.SELECT queue this service picks up the reply from
    /// TO.ITREK.READ_RES queue and puts it in ALLOK2L/SHOWDLS.xml in the 
    /// relevant users session directory
    /// According to timing set in SERVICESTIMER
    /// </summary>

    public partial class iTrekGetRead_ResSvc : ServiceBase
    {
        iTXMLcl iTXML;
        iTXMLPathscl iTPaths;
        MQQueue queue;

        ITrekWMQClass1 wmq1;
        //SessionManager sessMan;
        //iTrekWMQClass TOITREKREAD_RESQ;

        public iTrekGetRead_ResSvc()
        {
            InitializeComponent();
#if LoggingOn
            iTrekGetRead_ResSvcLog.WriteEntry("Initialisation iTrekGetRead_ResSvc");
#endif
            iTXML = new iTXMLcl();
            iTPaths = new iTXMLPathscl();
          
        }

        protected override void OnStart(string[] args)
        {
#if LoggingOn
            iTrekGetRead_ResSvcLog.WriteEntry("In OnStart");
#endif
            MessageQueueTimer();
#if LoggingOn
            //iTrekGetRead_ResSvcLog.WriteEntry("Finished in MessageQueueTimer");
#endif
            wmq1 = new ITrekWMQClass1();
           //queue = wmq1.getQueueConnection(iTPaths.GetTOITREKREAD_RESQueueName(), MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
        }

        protected override void OnStop()
        {
#if LoggingOn
            iTrekGetRead_ResSvcLog.WriteEntry("In onStop.");
#endif

            //queue.Close();
            //wmq1.getQManager.Close();
            //wmq1.getQManager.Disconnect();
        }

        public void MessageQueueTimer()
        {
#if LoggingOn
            iTrekGetRead_ResSvcLog.WriteEntry("In MessageQueueTimer");
#endif
            System.Timers.Timer aTimer = new System.Timers.Timer();
            aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            // Set the Interval to 5 seconds.
            aTimer.Interval = Convert.ToInt32(iTPaths.GetSERVICESTIMER());
            aTimer.Enabled = true;
        }

        // Specify what you want to happen when the Elapsed event is raised.
        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            string message = "";
#if LoggingOn   
            //iTrekGetRead_ResSvcLog.WriteEntry("In OnTimedEvent");
#endif
            iTXMLPathscl iTPaths = new iTXMLPathscl();
            //TODO: Check how this is instantiated - hard coded path?
            iTXML = new iTXMLcl();
            //ITrekWMQClass1 TOITREKREAD_RESQ = new ITrekWMQClass1();
            if (wmq1 != null)
            {
                queue = wmq1.getQueueConnection(iTPaths.GetTOITREKREAD_RESQueueName(), MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
            }
            else
            {
                wmq1 = new ITrekWMQClass1();
                queue = wmq1.getQueueConnection(iTPaths.GetTOITREKREAD_RESQueueName(), MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
            
            }
            message = wmq1.GetMessageFromTheQueue(queue);
            wmq1.closeQueueConnection(queue, wmq1.getQManager);
           // message = ITrekWMQClass1.GetMessageFromTheQueue(iTPaths.GetTOITREKREAD_RESQueueName());
           
#if LoggingOn
            //message = "<SHOWDLS><URNO>1234567890</URNO><SESSIONID>dt2xbz23qk4vbs45sfvqhljh</SESSIONID><DLS>aaaaaaaaaaaaaaaaaaaaaaa   aaaaaaaaaaaaaaaaaaaaaaaaaaaa   aaaaaaaaaaaa</DLS></SHOWDLS>";
            iTrekGetRead_ResSvcLog.WriteEntry("In initial msg: " + message);
#endif
           
            //add push info to querystring (no blank spaces)
            string info = "OK2LOAD-FlightSQ020/AKE21304SQ/SIN/620/B";
            string messageType = "";
            if (message != "MQRC_NO_MSG_AVAILABLE")
            {  
                messageType = iTXML.GetMsgTypeFromMsg(message);
            }

#if LoggingOn
            iTrekGetRead_ResSvcLog.WriteEntry("messageType : " + messageType);
#endif
            
            if(messageType == "ALLOK2L")
            {
                //TODO: add the path
                SessionManager sessMan = new SessionManager();
                sessMan.CreateXMLFileInSessionDirectory(iTXML.GetSessionIdFromMsg(message), iTXML.TransformStringToXmlDoc(message), "ALLOK2L.xml");
            }
            else if (messageType == "SHOWDLS")
            {
                message = iTXML.RemoveBadCharsFromSHOWDLS(message);
               
#if LoggingOn
                iTrekGetRead_ResSvcLog.WriteEntry("In SHOWDLS msg: " + message);
#endif
                //TODO: add the path
                SessionManager sessMan = new SessionManager();
                sessMan.CreateXMLFileInSessionDirectory(iTXML.GetSessionIdFromMsg(message), iTXML.TransformStringToXmlDoc(message), "SHOWDLS.xml");
            }
            else
            {
                //do nothing
            }
#if LoggingOn
            iTrekGetRead_ResSvcLog.WriteEntry("In final msg: " + message);
#endif
        }
    }
}


