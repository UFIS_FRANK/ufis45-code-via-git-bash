#define UsingQueues
#define LoggingOn
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using IBM.WMQ;
using System.Timers;
using System.Configuration;
using iTrekWMQ;
using System.IO;
using iTrekXML;
using System.Xml;
using iTrekSessionMgr;


namespace iTrekFlightMgrSvc
{
    /// <summary>
    /// Gets flight xml batch and update data from queue and writes it to 
    /// flight xml file on disk
    /// According to timing set in JOB_AFT_SERVICESTIMER
    /// </summary>

    public partial class iTrekFlightMgrSvc : ServiceBase
    {
        iTXMLcl iTXML;
        iTXMLPathscl iTPaths;
        int UpdateFlushCounter = 0;
     
        MQQueue queue;
        
        ITrekWMQClass1 wmq1;
        public iTrekFlightMgrSvc()
        {
            InitializeComponent();

            iTFlightMgrLog.WriteEntry("Initialisation Flight Mgr");
            iTXML = new iTXMLcl();
            iTPaths = new iTXMLPathscl();
        }

        protected override void OnStart(string[] args)
        {
#if LoggingOn
            iTFlightMgrLog.WriteEntry("In OnStart iTFlightMgr");
#endif
            wmq1 = new ITrekWMQClass1();

            //Create an updateable flights file if one doesn't already exist
            FileInfo fiFlightsUpdateable = new FileInfo(iTPaths.GetUpdateableFlightsFilePath());
            FileInfo fiFlightsMain = new FileInfo(iTPaths.GetFlightsFilePath());
            if (!fiFlightsUpdateable.Exists)
                fiFlightsMain.CopyTo(iTPaths.GetUpdateableFlightsFilePath());

            MessageQueueTimer();
#if LoggingOn
            //iTFlightMgrLog.WriteEntry("Finished in MessageQueueTimer");
#endif
        }

        protected override void OnStop()
        {
#if LoggingOn
            iTFlightMgrLog.WriteEntry("In onStop.");
#endif          
        }

        public void MessageQueueTimer()
        {
#if LoggingOn
            iTFlightMgrLog.WriteEntry("In MessageQueueTimer");
#endif
            System.Timers.Timer aTimer = new System.Timers.Timer();
            aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            aTimer.Interval = Convert.ToInt32(iTPaths.GetJOB_AFT_SERVICESTIMER());
            aTimer.Enabled = true;
        }

        // Specify what you want to happen when the Elapsed event is raised.
        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            XmlDocument UpdateableFlightsXml = new XmlDocument();
            XmlDocument MainFlightsXml = new XmlDocument();
            
            if (iTPaths == null)
                iTPaths = new iTXMLPathscl();
            if (iTXML == null)
                iTXML = new iTXMLcl();
#if LoggingOn
            iTFlightMgrLog.WriteEntry("Declarations");
#endif
            try
            {
                UpdateableFlightsXml.Load(iTPaths.GetUpdateableFlightsFilePath());
            }
            catch (Exception ex)
            {
#if LoggingOn
                iTFlightMgrLog.WriteEntry("Load XML GetUpdateableFlightsFilePath:" + ex.Message);
#endif
            }
            
            try
            {
                MainFlightsXml.Load(iTPaths.GetFlightsFilePath());
            }
            catch (Exception ex)
            {
#if LoggingOn
                iTFlightMgrLog.WriteEntry("Load XML GetFlightsFilePath:" + ex.Message);
#endif
            }
            
            string message = "";


#if UsingQueues
            try{
            if (wmq1 != null)
            {
                queue = wmq1.getQueueConnection(iTPaths.GetTOITREKREAD_AFTQueueName(), MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
            }
            else
            {
                wmq1 = new ITrekWMQClass1();
                queue = wmq1.getQueueConnection(iTPaths.GetTOITREKREAD_AFTQueueName(), MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);

            }
            message=wmq1.GetMessageFromTheQueue(queue);
            wmq1.closeQueueConnection(queue, wmq1.getQManager);
            }
            catch (Exception ex)
            {
#if LoggingOn
                iTFlightMgrLog.WriteEntry("Setup Queues:" + ex.Message);
#endif
            }
#else
            //message = @"<FLIGHT><URNO>" + iTPaths.GetTestFlightURNO() + "</URNO><ACTION>UPDATE</ACTION><FLNO>ABC 123</FLNO><STOD>20060927042000</STOD><STOA>20060927175500</STOA><REGN></REGN><DES3>SIN</DES3><ORG3>LHR</ORG3><ETDI></ETDI><ETOA></ETOA><GTA1></GTA1><GTD1></GTD1><ADID>A</ADID><PSTA></PSTA><PSTD></PSTD></FLIGHT>";        
            message = @"<FLIGHTS><FLIGHT><URNO>" + iTPaths.GetTestFlightURNO() + "</URNO><ACTION>INSERT</ACTION><FLNO>QF 999</FLNO><STOD>" + iTPaths.GetTestSTOD2() + "</STOD><STOA>" + iTPaths.GetTestSTOA2() + "</STOA><REGN></REGN><DES3>SIN</DES3><ORG3>LHR</ORG3><ETDI>" + iTrekUtils.iTUtils.convertStandardDateTimeToOracleFormat(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")) + "</ETDI><ETOA></ETOA><GTA1></GTA1><GTD1></GTD1><ADID>D</ADID><PSTA></PSTA><PSTD></PSTD></FLIGHT></FLIGHTS>";
#endif
           
#if LoggingOn
             string shortMsg = message.Remove(20);
             iTFlightMgrLog.WriteEntry("Short Message:" + shortMsg);
#endif
            string messageType = "";
            UpdateFlushCounter++;
            

            if (UpdateFlushCounter > Convert.ToInt32(iTPaths.GetFileMgrFlushSetting()))
            {
#if LoggingOn
                    iTFlightMgrLog.WriteEntry("UpdateFlushCounter : " + UpdateFlushCounter);
    #endif
                    UpdateFlushCounter = 0;
                try{
                    //Copy the save the UpdateableFlightsXml file to the MAIN Flights.xml
                    FileInfo fiFlightsUpdateable = new FileInfo(iTPaths.GetUpdateableFlightsFilePath());
                    FileInfo fiFlightsMain = new FileInfo(iTPaths.GetFlightsFilePath());
                    if (fiFlightsMain.Exists && fiFlightsUpdateable.Exists)
                        fiFlightsUpdateable.CopyTo(iTPaths.GetFlightsFilePath(), true);
                    }
                catch (Exception ex)
                {
    #if LoggingOn
                    iTFlightMgrLog.WriteEntry("Instatniate FileInfo:" + ex.Message);
    #endif
                }        
            }

            if (message != "MQRC_NO_MSG_AVAILABLE")
            {
                messageType = iTXML.GetFlightMsgActionFromMsg(message);
            }

#if LoggingOn
            iTFlightMgrLog.WriteEntry("messageType : " + messageType);
#endif

            //FLIGHTS is flights batch
            if (messageType == "FLIGHTS")
            {
                //Create new xml docs to process batch FLIGHTS
                //to avoid confusion with the update processing
                try
                {
                    XmlDocument MainBatchFlightsXml = new XmlDocument();
               
                    MainBatchFlightsXml.LoadXml(message);

                MainBatchFlightsXml.Save(iTPaths.GetFlightsFilePath());
                //Take a copy for the update file
                MainBatchFlightsXml.Save(iTPaths.GetUpdateableFlightsFilePath());
                
                }

                 catch (Exception ex)
                {
                     iTFlightMgrLog.WriteEntry("IN FLIGHTS: " + ex.Message);
                }

            }
            else if (messageType == "DELETE")
            {
                //DeleteFlightFromXMLFile ignores the ACTION tag
                //so don't need DeleteActionFromFlightXML
                try{
                    UpdateableFlightsXml = iTXML.DeleteFlightFromXMLFile(UpdateableFlightsXml, message);
                }
                catch (Exception ex)
                {
    #if LoggingOn
                    iTFlightMgrLog.WriteEntry("DELETE DeleteFlightFromXMLFile:" + ex.Message);
    #endif
                }
                try{
                    UpdateableFlightsXml.Save(iTPaths.GetUpdateableFlightsFilePath());
                }
                catch (Exception ex)
                {
    #if LoggingOn
                    iTFlightMgrLog.WriteEntry("DELETE GetUpdateableFlightsFilePath:" + ex.Message);
    #endif
                }
            }

            else if (messageType == "INSERT")
            {
                try{
                    message = iTXML.DeleteActionFromFlightXML(message);
                }
                catch (Exception ex)
                {
    #if LoggingOn
                    iTFlightMgrLog.WriteEntry("INSERT DeleteActionFromFlightXML:" + ex.Message);
    #endif
                }
                try{
                    UpdateableFlightsXml = iTXML.WriteNewFlightToXMLFile(UpdateableFlightsXml, message);
                }
                catch (Exception ex)
                {
    #if LoggingOn
                    iTFlightMgrLog.WriteEntry("INSERT WriteNewFlightToXMLFile:" + ex.Message);
    #endif
                }
                try{
                    UpdateableFlightsXml.Save(iTPaths.GetUpdateableFlightsFilePath());
                }
                catch (Exception ex)
                {
    #if LoggingOn
                    iTFlightMgrLog.WriteEntry("INSERT GetUpdateableFlightsFilePath:" + ex.Message);
    #endif
                }
            }
            else if (messageType == "UPDATE")
            {
                try{
                    message = iTXML.DeleteActionFromFlightXML(message);
                 }
                catch (Exception ex)
                {
    #if LoggingOn
                    iTFlightMgrLog.WriteEntry("UPDATE DeleteActionFromFlightXML:" + ex.Message);
    #endif
                }
                try{
                    UpdateableFlightsXml = iTXML.UpdateFlightInXMLFile(UpdateableFlightsXml, message);
                 }
                catch (Exception ex)
                {
    #if LoggingOn
                    iTFlightMgrLog.WriteEntry("UPDATE UpdateFlightInXMLFile:" + ex.Message);
    #endif
                }
                try{
                    UpdateableFlightsXml.Save(iTPaths.GetUpdateableFlightsFilePath());
                 }
                catch (Exception ex)
                {
    #if LoggingOn
                    iTFlightMgrLog.WriteEntry("UPDATE GetUpdateableFlightsFilePath:" + ex.Message);
    #endif
                }
            }

            else
            {
                //do nothing
            }

//               }catch (Exception ex)
//                {
//#if LoggingOn
//                    iTFlightMgrLog.WriteEntry(ex.Message);
                   
//#endif
//                }
        
        }
    }
}
