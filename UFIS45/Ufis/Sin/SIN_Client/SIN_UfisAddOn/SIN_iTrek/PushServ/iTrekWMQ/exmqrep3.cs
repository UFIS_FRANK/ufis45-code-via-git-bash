//      program name:  exmqrep3 - example of MQGET to receive a message from a queue
//                                and MQPUT to write a reply to a dynamic queue
//                                supplied in the ReplyToQueue field of the request 
//                
//                exmqrep3 has 2 parameters:
//                - the name of a request queue (required)
//                - the name of a queue manager (required)
//
//    example command line:   exmqrep3 LEXAQ1 EXQMAN

using System;
using IBM.WMQ;

namespace exmqrep3
{
  /// <summary>
  /// Summary description for exmqrep3
  /// </summary>
  class exmqrep3
  {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [STAThread]
    static void Main(String[] args)
    {
      MQQueueManager      mqQMgr;           // MQQueueManager instance
      MQQueue             requestQueue;     // MQQueue instance
      MQQueue             responseQueue;    // MQQueue instance
      MQMessage           requestMessage;   // MQMessage instance
      MQGetMessageOptions gmo;              // MQGetMessageOptions instance
      MQRCText            mqrcText;         // MQRCText instance
      String              requestQueueName; // Name of request queue 
      String              line;

      if (args.Length == 0) 
      {
        System.Console.WriteLine("Required parameters missing - 'queue name' 'queue manager name'" );
        return;
      }
      else 
      {
        requestQueueName = args[0];
      }

      ///
      /// create an MQRCText instance
      /// 
       
      mqrcText = new MQRCText();
       
      ///
      /// Try to create an MQQueueManager instance 
      /// 
      try 
      {
        if (args.Length > 1)
        {
          // queue name, queue manager name provided
          mqQMgr = new MQQueueManager( args[1] );
        }
        else 
        {
          if (args[0] == "?")
          {
           System.Console.WriteLine("execute exmqrep3 as follows: exmqrep3 'request queue name' 'queue manager name'" );
           System.Console.WriteLine("                         eg. exmqrep3 LEXAQ1 EXQMAN " );
           return;
          }
          else
          {
            System.Console.WriteLine("Required parameter missing - queue manager name" );
            return;
          }
        }
      }
      catch (MQException mqe) 
      {
        // stop if failed
        String mText = mqrcText.getMQRCText(mqe.Reason);     // get the message text
        System.Console.WriteLine( "create of MQQueueManager ended with " + mqe.Message + " Text: " + mText );
        return;
      }

      ///
      /// Try to open the queue
      ///
      try 
      {
        requestQueue = mqQMgr.AccessQueue( requestQueueName, 
                                           MQC.MQOO_INPUT_AS_Q_DEF           // open queue for input
                                         + MQC.MQOO_FAIL_IF_QUIESCING );     // but not if MQM stopping
      }
      catch (MQException mqe) 
      {
        // stop if failed
        String mText = mqrcText.getMQRCText(mqe.Reason);      // get the message text
        System.Console.WriteLine( "Open request queue ended with " + mqe.Message + " Text: " + mText );
        return;
      }

      ///
      /// Get messages from the message queueand send
      /// the response back to the queue defined in the
      /// ReplyToQueue/ReplyToQueueManagerName fields 
      /// of request message.                         
      /// 
      try
      {
        Console.WriteLine("Example exmqrep3 started.");
        requestMessage = new MQMessage();
        gmo = new MQGetMessageOptions();
        gmo.Options = MQC.MQGMO_WAIT;                 // must be specified if you use wait interval
        gmo.WaitInterval = MQC.MQWI_UNLIMITED;        // wait forever
        gmo.WaitInterval = 60000;                     // wait 60 seconds
        gmo.MatchOptions = MQC.MQMO_NONE;             // no matching required
        requestQueue.Get(requestMessage, gmo);
        line = requestMessage.ReadString(requestMessage.MessageLength);
        Console.WriteLine("Received: " + line);
      }
      catch (MQException mqe) 
      {
        // stop if failed
        String mText = mqrcText.getMQRCText(mqe.Reason);      // get the message text
        System.Console.WriteLine( "Get request message ended with " + mqe.Message + " Text: " + mText );
        return;
      }

      ///
      /// Open the dynamic response queue    
      ///
      try
      {
        Console.WriteLine("ReplyToQueueName: {0}", requestMessage.ReplyToQueueName);
        Console.WriteLine("ReplyToQueueManagerName: {0}", requestMessage.ReplyToQueueManagerName);
        responseQueue = mqQMgr.AccessQueue(requestMessage.ReplyToQueueName,
                                           MQC.MQOO_OUTPUT,
                                           requestMessage.ReplyToQueueManagerName,
                                           null, null);
      }
      catch (MQException mqe) 
      {
        // stop if failed
        String mText = mqrcText.getMQRCText(mqe.Reason);      // get the message text
        System.Console.WriteLine( "Open response queue ended with " + mqe.Message + " Text: " + mText );
        return;
      }

      ///
      /// Send the response message.
      ///
      try
      {
        MQMessage responseMessage = new MQMessage();
        MQPutMessageOptions pmo = new MQPutMessageOptions();
        pmo.Options = MQC.MQPMO_NONE;

        ///
        /// A large number of MQSeries based applications are
        /// hard coded to process the MsgId and CorrelId in
        /// request and reply processing.  This section of code
        /// honors the request message Report field and the
        /// options contained within.
        ///
        if ((requestMessage.Report & MQC.MQRO_PASS_MSG_ID) == MQC.MQRO_PASS_MSG_ID)
        {
          responseMessage.MessageId = requestMessage.CorrelationId;
        }
        else // Assume MQRO_NEW_MSG_ID
        {
          pmo.Options = MQC.MQPMO_NEW_MSG_ID;
        }

        if ((requestMessage.Report & MQC.MQRO_PASS_CORREL_ID) == MQC.MQRO_PASS_CORREL_ID)
        {
          responseMessage.CorrelationId = requestMessage.CorrelationId;
        }
        else // Assume MQRO_COPY_MSG_ID_TO_CORREL_ID
        {
          responseMessage.CorrelationId = requestMessage.MessageId;
        }

        responseMessage.MessageType = MQC.MQMT_REPLY;
        responseMessage.WriteString("This is the response: " + line);

        responseQueue.Put(responseMessage);
        responseQueue.Close();
        requestQueue.Close();
      }
      catch(MQException mqe)
      {
        String mText = mqrcText.getMQRCText(mqe.Reason);      // get the message text
        System.Console.WriteLine( "Put message to response queue ended with " + mqe.Message + " Text: " + mText );
      }

      mqQMgr.Disconnect();
      System.Console.WriteLine( "Example exmqrep3 end" );
      return;
    }
  }
}
