using System;
using IBM.WMQ;
using System.Timers;
using System.Collections.Specialized;
using System.Configuration;
using iTrekUtils;
using iTrekXML;
using iTrekWMQ;


namespace iTrekWMQConsole
{
    /// <summary>
    /// Used for testing queues
    /// </summary>
    class iTrekWMQRunClass
    {
        MQQueueManager queueManager;
        MQQueue queue;
        MQMessage queueMessage;
        MQPutMessageOptions queuePutMessageOptions;
        MQGetMessageOptions queueGetMessageOptions;

        static string QueueName;
        static string QueueManagerName;
        static string ChannelInfo;

        string channelName;
        string transportType;
        string connectionName;

        string message;
        byte[] MessageID;

        //iTXMLPathscl iTPaths;
 

        static void Main(string[] args)
        {
                iTXMLPathscl iTPaths = new iTXMLPathscl();

                string choice = "1";
                Console.WriteLine("What do you want to do?:");
                Console.WriteLine("Add/test messages to TOPUSHOK2LOAD queue: Press 1");
                Console.WriteLine("Add/test messages to FROMITREKSELECT queue: Press 2");
                Console.WriteLine("Add/test messages to TOITREKREAD_RES queue: Press 3");
                Console.WriteLine("Add/test messages to FROMITREKUPDATE queue: Press 4");
                Console.WriteLine("Add/test messages to TOITREKREAD_AFT queue: Press 5");
                Console.WriteLine("Add/test messages to TOITREKREAD_JOB queue: Press 6");
                Console.WriteLine("Add/test messages to FROMITREKAUTH queue: Press 7");
                Console.WriteLine("Add/test messages to TOITREKAUTH queue: Press 8");
                Console.WriteLine("Test polling and getting messages from queue: Press 9");
                Console.WriteLine("Quit: Press 10");
                choice = Console.ReadLine();

             while (choice != "10"){

                    if (choice == "1")
                    {
                        iTrekWMQClass TOPUSHOK2LOAD = new iTrekWMQClass(iTPaths.GetTOPUSHOK2LOADQueueName());
                        SimpleTest(TOPUSHOK2LOAD); 
                    }
                    if (choice == "2")
                    {
                        iTrekWMQClass FROMITREKSELECT = new iTrekWMQClass(iTPaths.GetFROMITREKSELECTQueueName());
                        SimpleTest(FROMITREKSELECT);
                    }
                    if (choice == "3")
                    {
                        iTrekWMQClass TOITREKREAD_RES = new iTrekWMQClass(iTPaths.GetTOITREKREAD_RESQueueName());
                        SimpleTest(TOITREKREAD_RES);
                    }
                    if (choice == "4")
                    {
                        iTrekWMQClass FROMITREKUPDATE = new iTrekWMQClass(iTPaths.GetFROMITREKUPDATEQueueName());
                        SimpleTest(FROMITREKUPDATE);
                    }
                    if (choice == "5")
                    {
                        //iTrekWMQClass TOITREKREAD_AFT = new iTrekWMQClass(iTPaths.GetTOITREKREADFLT_JOB_RESQueueName());
                        iTrekWMQClass TOITREKREAD_AFT = new iTrekWMQClass(iTPaths.GetTOITREKREAD_AFTQueueName());
                        SimpleTest(TOITREKREAD_AFT);
                    }
                    if (choice == "6")
                    {
                        iTrekWMQClass TOITREKREAD_JOB = new iTrekWMQClass(iTPaths.GetTOITREKREAD_JOBQueueName());
                        SimpleTest(TOITREKREAD_JOB);
                    }
                    if (choice == "7")
                    {
                        iTrekWMQClass FROMITREKAUTH = new iTrekWMQClass(iTPaths.GetFROMITREKAUTHQueueName());
                        SimpleTest(FROMITREKAUTH);
                    }
                    if (choice == "8")
                    {
                        iTrekWMQClass TOITREKAUTH = new iTrekWMQClass(iTPaths.GetTOITREKAUTHQueueName());
                        SimpleTest(TOITREKAUTH);
                    }
                    else if (choice == "9")
                    {
                        MessageQueueTimer();    
                    }

                    choice = Console.ReadLine();
                }
        }
            
            
      private static void SimpleTest(iTrekWMQClass WMQobj){
            try
            {
                iTXMLPathscl iTPaths = new iTXMLPathscl();
                //TODO: Change to iTrekWMQClass
                //iTrekWMQRunClass mqSeries = new iTrekWMQRunClass();
                //iTrekWMQClass TOITREKREAD_RES = new iTrekWMQClass(iTPaths.GetTOITREKREAD_RESQueueName());

                string choice = "1";
                Console.WriteLine("What do you want to do?:");
                Console.WriteLine("Put message: Press 1");
                Console.WriteLine("Get message: Press 2");
                Console.WriteLine("Purge messages: Press 3");
                Console.WriteLine("Put and Get message: Press 4");
                Console.WriteLine("Put 15 messages: Press 5");
                Console.WriteLine("Put XML message onto queue: Press 6");
                Console.WriteLine("Put 15 XML messages onto queue: Press 7");          
                Console.WriteLine("Put single pre-formatted XML message onto current queue: Press 8");
                Console.WriteLine("Quit: Press 9");
                choice = Console.ReadLine();
               
                while (choice != "9"){

                    if (choice == "1")
                    {
                        Console.WriteLine("Enter the message to put in MQSeries server:");
                        string message = Console.ReadLine();
                        WMQobj.putTheMessageIntoQueue(message);
                        
                    }
                    else if (choice == "2")
                    {
                        Console.WriteLine("Message: " + WMQobj.GetMessageFromTheQueue() + " successfully entered (ensure message in this output)");   
                    }
                    else if (choice == "3")
                    {

                        WMQobj.PurgeQueue();
                    }
                    else if (choice == "4")
                    {
                        Console.WriteLine("Enter message:");
                        string message = System.Console.ReadLine();
                        WMQobj.putTheMessageIntoQueue(message);
                        Console.WriteLine("Message: " + WMQobj.GetMessageFromTheQueue() + " successfully entered (ensure message in this output)");
                    }
                    else if (choice == "5")
                    {
                        for (int i = 0;i < 16; i++ )
                        {
                            string temp4 = "AKE2130" + i.ToString() + "SQ";
                            WMQobj.putMessageIntoQueueForMultipleMessages(temp4);
                        }
                    }
                    else if (choice == "6")
                    {
                        WMQobj.putXMLMessageIntoQueue();
                    }
                    else if (choice == "7")
                    {
                        WMQobj.putXMLMessageIntoQueue();
                    }
                    //Put single pre-formatted XML message onto current queue
                    else if (choice == "8")
                    {
                        string choice2 = "1";
                        Console.WriteLine("What do you want to do?:");
                        Console.WriteLine("Add/test messages to TOPUSHOK2LOAD queue: Press 1");
                        Console.WriteLine("Add/test messages to FROMITREKSELECT queue: Press 2");
                        Console.WriteLine("Add/test messages to TOITREKREAD_RES queue: Press 3");
                        Console.WriteLine("Add/test messages to FROMITREKUPDATE queue: Press 4");
                        Console.WriteLine("Add/test messages to TOITREKREAD_AFT queue: Press 5");
                        Console.WriteLine("Add/test messages to TOITREKREAD_JOB queue: Press 6");
                        Console.WriteLine("Add/test messages to FROMITREKAUTH queue: Press 7");
                        Console.WriteLine("Add/test messages to TOITREKAUTH queue: Press 8");
                        choice2 = Console.ReadLine();
                        if(choice2 == "1")
                            WMQobj.putTheMessageIntoQueue("<OK2L><ULDNUMBER>AKE123456CX</ULDNUMBER><UAFT>" + iTPaths.GetOK2LSvcTestUAFT() + "</UAFT></OK2L>");
                        else if(choice2 == "2")
                            WMQobj.putTheMessageIntoQueue("");
                        else if (choice2 == "3")
                            WMQobj.putTheMessageIntoQueue("<SHOWDLS><URNO>1234567890</URNO><SESSIONID>dt2xbz23qk4vbs45sfvqhljh</SESSIONID><DLS>aaaaaaaaaaaaaaaaaaaaaaa   aaaaaaaaaaaaaaaaaaaaaaaaaaaa   aaaaaaaaaaaa</DLS></SHOWDLS>");
                        else if (choice2 == "4")
                            WMQobj.putTheMessageIntoQueue("");
                        else if (choice2 == "5")
                        {
                            //NB. unrealistic data
                           string xmlstr = "<FLIGHTS><FLIGHT><URNO>1234636137</URNO>";
                            xmlstr += "<FLNO>QF 051</FLNO><STOD>20060927200000</STOD>";
                            xmlstr += "<STOA>20060927195000</STOA><REGN /><DES3>SIN</DES3>";
                            xmlstr += "<ORG3>BNE</ORG3><ETDI /><ETOA /><GTA1 /> <GTD1 />";
                            xmlstr += "<ADID>A</ADID><PSTA /><PSTD /></FLIGHT>";
                            xmlstr += "<FLIGHT><URNO>1234636137</URNO>";
                            xmlstr += "<FLNO>QF 051</FLNO><STOD>20060927200000</STOD>";
                            xmlstr += "<STOA>20060927195000</STOA><REGN /><DES3>SIN</DES3>";
                            xmlstr += "<ORG3>BNE</ORG3><ETDI /><ETOA /><GTA1 /> <GTD1 />";
                            xmlstr += "<ADID>A</ADID><PSTA /><PSTD /></FLIGHT></FLIGHTS>";

                            WMQobj.putTheMessageIntoQueue(xmlstr);
                        }
                        else if (choice2 == "6")
                        {
                            //NB. unrealistic data
                            string xmlstr = "<JOBS>";
                            xmlstr += "<JOB><URNO>1234567890</URNO><PENO>123456</PENO><UAFT>1234884973</UAFT></JOB>";
                            xmlstr += "<JOB><URNO>1234567891</URNO><PENO>123456</PENO><UAFT>1234916054</UAFT></JOB>";
                            xmlstr += "</JOBS>";

                            WMQobj.putTheMessageIntoQueue(xmlstr);
                        }
                        else if (choice2 == "7")
                            WMQobj.putTheMessageIntoQueue("<AUTH><PENO>1234567890</PENO><PASS>aabbc123</PASS><SESSIONID>dt2xbz23qk4vbs45sfvqhljh</SESSIONID></AUTH>");
                        else if (choice2 == "8")
                        {
                            string xmlstr = "<AUTH><AUTHENTICATED>TRUE</AUTHENTICATED>";
                            xmlstr += "<SESSIONID>dt2xbz23qk4vbs45sfvqhljh</SESSIONID>";
                            xmlstr += "<FNAME>John</FNAME><LNAME>Smith</LNAME>";
                            xmlstr += "</AUTH>";
                      
                        WMQobj.putTheMessageIntoQueue(xmlstr);
                        }
                    }
                    
                    choice = Console.ReadLine();
                }

                }
                catch (MQException ex)
                {
                    Console.WriteLine(ex.Message);
                }
           }


            public static void MessageQueueTimer()
            {
                //iTrekWMQClass mqSeries = new iTrekWMQClass();
                iTXMLPathscl iTPaths = new iTXMLPathscl();
                System.Timers.Timer aTimer = new System.Timers.Timer();
                aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
                // Set the Interval to 5 seconds.
                aTimer.Interval = Convert.ToInt32(iTPaths.GetJOB_AFT_SERVICESTIMER());
                aTimer.Enabled = true;

                Console.WriteLine("Press \'q + Enter\' to quit the sample.");
                while (Console.Read() != 'q') ;
            }

            // Specify what you want to happen when the Elapsed event is raised.
            private static void OnTimedEvent(object source, ElapsedEventArgs e)
            {
                iTXMLPathscl iTPaths = new iTXMLPathscl();
                iTrekWMQClass mqObj = new iTrekWMQClass(iTPaths.GetTOPUSHOK2LOADQueueName());
                string test = mqObj.GetMessageFromTheQueueAnyOrder();
                if (test == "")
                {
                    Console.WriteLine("No messages on the queue");
                }
                else
                {
                    Console.WriteLine(test);
                }
            }

        public void PurgeQueue()
        {
            //iTrekWMQClass mqObj = new iTrekWMQClass();
            //while (mqObj.GetMessageFromTheQueue() != "MQRC_NO_MSG_AVAILABLE")
            while (GetMessageFromTheQueueReturnString() != "MQRC_NO_MSG_AVAILABLE")
            {
                GetMessageFromTheQueueForPurging();
            }
        }


        public void putTheMessageIntoQueue()
        {
            try
            {
              
            char[] separator = { '/' };
            string[] ChannelParams;
            ChannelParams = ChannelInfo.Split(separator);
            channelName = ChannelParams[0];
            transportType = ChannelParams[1];
            connectionName = ChannelParams[2];
            
                queueManager = new MQQueueManager(QueueManagerName, channelName, connectionName);
                queue = queueManager.AccessQueue(QueueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);
               //queue.QueueType = 1;
                Console.WriteLine("Enter the message to put in MQSeries server:");
                message = System.Console.ReadLine();
                int msgLen = message.Length;

                while (msgLen == 0)
                {
                    if (msgLen == 0)
                        Console.WriteLine("Please reenter the message:");
                    message = System.Console.ReadLine();
                    msgLen = message.Length;
                }

                queueMessage = new MQMessage();
                queueMessage.WriteBytes(message);
                queueMessage.Format = MQC.MQFMT_STRING;
                queuePutMessageOptions = new MQPutMessageOptions();

                //putting the message into the queue
                queue.Put(queueMessage, queuePutMessageOptions);
                MessageID = queueMessage.MessageId;

                Console.WriteLine("Success fully entered the message into the queue");
            }
            catch (MQException mqexp)
            {
                Console.WriteLine("MQSeries Exception: " + mqexp.Message);
            }
        }

        public void putXMLMessageIntoQueue()
        {
            try
            {
                char[] separator = { '/' };
                string[] ChannelParams;
                ChannelParams = ChannelInfo.Split(separator);
                channelName = ChannelParams[0];
                transportType = ChannelParams[1];
                connectionName = ChannelParams[2];

                queueManager = new MQQueueManager(QueueManagerName, channelName, connectionName);
                queue = queueManager.AccessQueue(QueueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);


                //message = iTUtils.ReadFromFile(@"C:\temp\tests\bigfile.xml");
                //message = @"<?xml version='1.0' encoding='UTF-8' standalone='no' ?><OK2L><ULDNumber>AKE123456CX</ULDNumber><Airline>SQ</Airline><FlightNo Suffix='D'>000001</FlightNo><Date>20040405</Date><Destination>FRA</Destination></OK2L>";
                message = @"<OK2L><ULDNUMBER>AKE123456CX</ULDNUMBER><UAFT>1234567899</UAFT></OK2L>";

                int msgLen = message.Length;

                while (msgLen == 0)
                {
                    if (msgLen == 0)
                        Console.WriteLine("Please reenter the message:");
                    message = System.Console.ReadLine();
                    msgLen = message.Length;
                }

                queueMessage = new MQMessage();
                queueMessage.WriteBytes(message);
                queueMessage.Format = MQC.MQFMT_STRING;
                queuePutMessageOptions = new MQPutMessageOptions();

                //putting the message into the queue
                queue.Put(queueMessage, queuePutMessageOptions);
                MessageID = queueMessage.MessageId;

                Console.WriteLine("Success fully entered the message into the queue");
            }
            catch (MQException mqexp)
            {
                Console.WriteLine("MQSeries Exception: " + mqexp.Message);
            }
        }

        public void putXMLMessageIntoQueue(string xmlMessage)
        {
            try
            {
                char[] separator = { '/' };
                string[] ChannelParams;
                ChannelParams = ChannelInfo.Split(separator);
                channelName = ChannelParams[0];
                transportType = ChannelParams[1];
                connectionName = ChannelParams[2];

                queueManager = new MQQueueManager(QueueManagerName, channelName, connectionName);
                queue = queueManager.AccessQueue(QueueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);


                //message = iTUtils.ReadFromFile(@"C:\temp\tests\bigfile.xml");
                message = xmlMessage;

                int msgLen = message.Length;

                while (msgLen == 0)
                {
                    if (msgLen == 0)
                        Console.WriteLine("Please reenter the message:");
                    message = System.Console.ReadLine();
                    msgLen = message.Length;
                }

                queueMessage = new MQMessage();
                queueMessage.WriteBytes(message);
                queueMessage.Format = MQC.MQFMT_STRING;
                queuePutMessageOptions = new MQPutMessageOptions();

                //putting the message into the queue
                queue.Put(queueMessage, queuePutMessageOptions);
                MessageID = queueMessage.MessageId;

                Console.WriteLine("Success fully entered the message into the queue");
            }
            catch (MQException mqexp)
            {
                Console.WriteLine("MQSeries Exception: " + mqexp.Message);
            }
        }

        //public static string ReadFromFile(string strFileName)
        //{
        //    try
        //    {
        //        System.IO.TextReader reader;
        //        reader = new System.IO.StreamReader(strFileName);
        //        string temp = reader.ReadToEnd();
        //        reader.Close();
        //        return temp;
        //    }
        //    catch (Exception e)
        //    {
        //        return e.Message;
        //    }
        //}

        
        //public void putMessageIntoQueueForMultipleMessages(string msg)
        //{
        //    try
        //    {
        //        char[] separator = { '/' };
        //        string[] ChannelParams;
        //        ChannelParams = ChannelInfo.Split(separator);
        //        channelName = ChannelParams[0];
        //        transportType = ChannelParams[1];
        //        connectionName = ChannelParams[2];

        //        queueManager = new MQQueueManager(QueueManagerName, channelName, connectionName);
        //        queue = queueManager.AccessQueue(QueueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);
                
        //        message = msg;

        //        queueMessage = new MQMessage();
        //        queueMessage.WriteBytes(message);
        //        queueMessage.Format = MQC.MQFMT_STRING;
        //        queuePutMessageOptions = new MQPutMessageOptions();

        //        //putting the message into the queue
        //        queue.Put(queueMessage, queuePutMessageOptions);
        //        MessageID = queueMessage.MessageId;

        //        Console.WriteLine("Success fully entered the message into the queue");
        //    }
        //    catch (MQException mqexp)
        //    {
        //        Console.WriteLine("MQSeries Exception: " + mqexp.Message);
        //    }
        //}


        public void GetMessageFromTheQueue()
        {

            queue = queueManager.AccessQueue(QueueName, MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
            queueMessage = new MQMessage();
            queueMessage.Format = MQC.MQFMT_STRING;
            queueMessage.MessageId = MessageID;
            queueGetMessageOptions = new MQGetMessageOptions();

            try
            {
                queue.Get(queueMessage, queueGetMessageOptions);

                // Received Message.
                System.Console.WriteLine("Received Message: {0}", queueMessage.ReadString(queueMessage.MessageLength));
                //System.Console.WriteLine("Received Message: {0}", queueMessage.MessageId[0].ToString());
                
            }
            catch (MQException MQExp)
            {
                // report the error
                System.Console.WriteLine("MQQueue::Get ended with " + MQExp.Message);
            }
        }

        public void GetMessageFromTheQueueForPurging()
        {

            queue = queueManager.AccessQueue(QueueName, MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
            queueMessage = new MQMessage();
            queueMessage.Format = MQC.MQFMT_STRING;
            //queueMessage.MessageId = MessageID;
            queueGetMessageOptions = new MQGetMessageOptions();

            try
            {
                queue.Get(queueMessage, queueGetMessageOptions);

                // Received Message.
                System.Console.WriteLine("Received Message: {0}", queueMessage.ReadString(queueMessage.MessageLength));
                //System.Console.WriteLine("Received Message: {0}", queueMessage.MessageId[0].ToString());

            }
            catch (MQException MQExp)
            {
                // report the error
                System.Console.WriteLine("MQQueue::Get ended with " + MQExp.Message);
            }
        }

        public string GetMessageFromTheQueueReturnString()
        {
            char[] separator = { '/' };
            string[] ChannelParams;
            ChannelParams = ChannelInfo.Split(separator);
            channelName = ChannelParams[0];
            transportType = ChannelParams[1];
            connectionName = ChannelParams[2];

            queueManager = new MQQueueManager(QueueManagerName, channelName, connectionName);

            queue = queueManager.AccessQueue(QueueName, MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
            queueMessage = new MQMessage();
            queueMessage.Format = MQC.MQFMT_STRING;
            //queueMessage.MessageId = MessageID;
            queueGetMessageOptions = new MQGetMessageOptions();

            try
            {
                queue.Get(queueMessage, queueGetMessageOptions);

                // Received Message.
                return queueMessage.ReadString(queueMessage.MessageLength);
                //System.Console.WriteLine("Received Message: {0}", queueMessage.MessageId[0].ToString());

            }
            catch (MQException MQExp)
            {
                // report the error
                return MQExp.Message;
            }
        }

        public void GetMessageFromTheQueueAnyOrder()
        {
            char[] separator = { '/' };
            string[] ChannelParams;
            ChannelParams = ChannelInfo.Split(separator);
            channelName = ChannelParams[0];
            transportType = ChannelParams[1];
            connectionName = ChannelParams[2];
            
            queueManager = new MQQueueManager(QueueManagerName, channelName, connectionName);
            queue = queueManager.AccessQueue(QueueName, MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING); 
            queueMessage = new MQMessage();
            queueMessage.Format = MQC.MQFMT_STRING;
            //queueMessage.MessageId = MessageID;
            queueGetMessageOptions = new MQGetMessageOptions();

            try
            {
                queue.Get(queueMessage, queueGetMessageOptions);

                // Received Message.
                System.Console.WriteLine("Received Message: {0}", queueMessage.ReadString(queueMessage.MessageLength));
                //System.Console.WriteLine("Received Message: {0}", queueMessage.MessageId[0].ToString());

            }
            catch (MQException MQExp)
            {
                // report the error
                System.Console.WriteLine("MQQueue::Get ended with " + MQExp.Message);
            }
        }
    }
}