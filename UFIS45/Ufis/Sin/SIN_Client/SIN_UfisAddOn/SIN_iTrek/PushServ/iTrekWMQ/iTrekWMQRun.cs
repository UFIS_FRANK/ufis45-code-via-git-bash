using System;
using IBM.WMQ;
using System.Timers;

namespace iTrekWMQ
{
    class iTrekWMQRunClass
    {
        MQQueueManager queueManager;
        MQQueue queue;
        MQMessage queueMessage;
        MQPutMessageOptions queuePutMessageOptions;
        MQGetMessageOptions queueGetMessageOptions;

        static string QueueName;
        static string QueueManagerName;
        static string ChannelInfo;

        string channelName;
        string transportType;
        string connectionName;

        string message;
        byte[] MessageID;
        //iTrekWMQClass mqSeries;

        static void Main(string[] args)
        {
  
                QueueName = "ORANGE.QUEUE";
                QueueManagerName = "venus.queue.manager";
                ChannelInfo = "CHANNEL1/TCP/192.168.1.8";

                string choice = "1";
                Console.WriteLine("What do you want to do?:");
                Console.WriteLine("Add/test messages to queue: Press 1");
                Console.WriteLine("Test polling and getting messages from queue: Press 2");
                Console.WriteLine("Quit: Press 4");
                choice = Console.ReadLine();

             while (choice != "4"){

                    if (choice == "1")
                    {
                        SimpleTest(); 
                    }
                    else if (choice == "2")
                    {
                        MessageQueueTimer();    
                    }
                    choice = Console.ReadLine();
                }
        }
            
            
      private static void SimpleTest(){
            try
            {
                iTrekWMQRunClass mqSeries = new iTrekWMQRunClass();

                string choice = "1";
                Console.WriteLine("What do you want to do?:");
                Console.WriteLine("Put message: Press 1");
                Console.WriteLine("Get message: Press 2");
                Console.WriteLine("Purge messages: Press 3");
                Console.WriteLine("Put and Get message: Press 4");
                Console.WriteLine("Quit: Press 5");
                choice = Console.ReadLine();
               
                while (choice != "5"){

                    if (choice == "1")
                    {
                        mqSeries.putTheMessageIntoQueue();
                        
                    }
                    else if (choice == "2")
                    {
                        
                        mqSeries.GetMessageFromTheQueueAnyOrder();
                        
                    }
                    else if (choice == "3")
                    {

                        mqSeries.PurgeQueue();
                    }
                    else
                    {
                        mqSeries.putTheMessageIntoQueue();

                        mqSeries.GetMessageFromTheQueue();
                        
                    }
                    
                    choice = Console.ReadLine();
                }

                }
                catch (MQException ex)
                {
                    Console.WriteLine(ex.Message);
                }
           }


            public static void MessageQueueTimer()
            {
                //iTrekWMQClass mqSeries = new iTrekWMQClass();

                System.Timers.Timer aTimer = new System.Timers.Timer();
                aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
                // Set the Interval to 5 seconds.
                aTimer.Interval = 5000;
                aTimer.Enabled = true;

                Console.WriteLine("Press \'q + Enter\' to quit the sample.");
                while (Console.Read() != 'q') ;
            }

            // Specify what you want to happen when the Elapsed event is raised.
            private static void OnTimedEvent(object source, ElapsedEventArgs e)
            {
                iTrekWMQClass mqObj = new iTrekWMQClass();
                string test = mqObj.GetMessageFromTheQueueAnyOrder();
                if (test == "")
                {
                    Console.WriteLine("No messages on the queue");
                }
                else
                {
                    Console.WriteLine(test);
                }
            }

        public void PurgeQueue()
        {
            //iTrekWMQClass mqObj = new iTrekWMQClass();
            //while (mqObj.GetMessageFromTheQueue() != "MQRC_NO_MSG_AVAILABLE")
            while (GetMessageFromTheQueueReturnString() != "MQRC_NO_MSG_AVAILABLE")
            {
                GetMessageFromTheQueue();
            }
        }


        public void putTheMessageIntoQueue()
        {
            try
            {
              
            char[] separator = { '/' };
            string[] ChannelParams;
            ChannelParams = ChannelInfo.Split(separator);
            channelName = ChannelParams[0];
            transportType = ChannelParams[1];
            connectionName = ChannelParams[2];
            
                queueManager = new MQQueueManager(QueueManagerName, channelName, connectionName);
                queue = queueManager.AccessQueue(QueueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);
               //queue.QueueType = 1;
                Console.WriteLine("Enter the message to put in MQSeries server:");
                message = System.Console.ReadLine();
                int msgLen = message.Length;

                while (msgLen == 0)
                {
                    if (msgLen == 0)
                        Console.WriteLine("Please reenter the message:");
                    message = System.Console.ReadLine();
                    msgLen = message.Length;
                }

                queueMessage = new MQMessage();
                queueMessage.WriteString(message);
                queueMessage.Format = MQC.MQFMT_STRING;
                queuePutMessageOptions = new MQPutMessageOptions();

                //putting the message into the queue
                queue.Put(queueMessage, queuePutMessageOptions);
                MessageID = queueMessage.MessageId;

                Console.WriteLine("Success fully entered the message into the queue");
            }
            catch (MQException mqexp)
            {
                Console.WriteLine("MQSeries Exception: " + mqexp.Message);
            }
        }

        public void GetMessageFromTheQueue()
        {

            queue = queueManager.AccessQueue(QueueName, MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
            queueMessage = new MQMessage();
            queueMessage.Format = MQC.MQFMT_STRING;
            queueMessage.MessageId = MessageID;
            queueGetMessageOptions = new MQGetMessageOptions();

            try
            {
                queue.Get(queueMessage, queueGetMessageOptions);

                // Received Message.
                System.Console.WriteLine("Received Message: {0}", queueMessage.ReadString(queueMessage.MessageLength));
                //System.Console.WriteLine("Received Message: {0}", queueMessage.MessageId[0].ToString());
                
            }
            catch (MQException MQExp)
            {
                // report the error
                System.Console.WriteLine("MQQueue::Get ended with " + MQExp.Message);
            }
        }

        public string GetMessageFromTheQueueReturnString()
        {
            char[] separator = { '/' };
            string[] ChannelParams;
            ChannelParams = ChannelInfo.Split(separator);
            channelName = ChannelParams[0];
            transportType = ChannelParams[1];
            connectionName = ChannelParams[2];

            queueManager = new MQQueueManager(QueueManagerName, channelName, connectionName);

            queue = queueManager.AccessQueue(QueueName, MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
            queueMessage = new MQMessage();
            queueMessage.Format = MQC.MQFMT_STRING;
            //queueMessage.MessageId = MessageID;
            queueGetMessageOptions = new MQGetMessageOptions();

            try
            {
                queue.Get(queueMessage, queueGetMessageOptions);

                // Received Message.
                return queueMessage.ReadString(queueMessage.MessageLength);
                //System.Console.WriteLine("Received Message: {0}", queueMessage.MessageId[0].ToString());

            }
            catch (MQException MQExp)
            {
                // report the error
                return MQExp.Message;
            }
        }

        public void GetMessageFromTheQueueAnyOrder()
        {
            char[] separator = { '/' };
            string[] ChannelParams;
            ChannelParams = ChannelInfo.Split(separator);
            channelName = ChannelParams[0];
            transportType = ChannelParams[1];
            connectionName = ChannelParams[2];
            
            queueManager = new MQQueueManager(QueueManagerName, channelName, connectionName);
            queue = queueManager.AccessQueue(QueueName, MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING); 
            queueMessage = new MQMessage();
            queueMessage.Format = MQC.MQFMT_STRING;
            //queueMessage.MessageId = MessageID;
            queueGetMessageOptions = new MQGetMessageOptions();

            try
            {
                queue.Get(queueMessage, queueGetMessageOptions);

                // Received Message.
                System.Console.WriteLine("Received Message: {0}", queueMessage.ReadString(queueMessage.MessageLength));
                //System.Console.WriteLine("Received Message: {0}", queueMessage.MessageId[0].ToString());

            }
            catch (MQException MQExp)
            {
                // report the error
                System.Console.WriteLine("MQQueue::Get ended with " + MQExp.Message);
            }
        }
    }
}