using System;
using System.Collections.Generic;
using System.Text;
using IBM.WMQ;
using iTrekXML;

namespace iTrekWMQConsole
{
    public class iTrekWMQConsoleClass
    {
        MQQueueManager queueManager;
        MQQueue queue;
        MQMessage queueMessage;
        MQPutMessageOptions queuePutMessageOptions;
        MQGetMessageOptions queueGetMessageOptions;

        static string QueueName;
        static string QueueManagerName;
        static string ChannelInfo;

        string channelName;
        string transportType;
        string connectionName;

        string message;
        byte[] MessageID;

        iTXMLPathscl iTPaths;

        public iTrekWMQConsoleClass(string QueueNm)
        {
            iTPaths = new iTXMLPathscl();
            //QueueName = "ORANGE.QUEUE";
            QueueName = QueueNm;
            QueueManagerName = "venus.queue.manager";
            ChannelInfo = "CHANNEL1/TCP/192.168.1.8";
            char[] separator = { '/' };
            string[] ChannelParams;
            ChannelParams = ChannelInfo.Split(separator);
            channelName = ChannelParams[0];
            transportType = ChannelParams[1];
            connectionName = ChannelParams[2];
            queueManager = new MQQueueManager(QueueManagerName, channelName, connectionName);
        }

        public void PurgeQueue()
        {
            while (GetMessageFromTheQueue() != "MQRC_NO_MSG_AVAILABLE")
            {
                GetMessageFromTheQueue();
            }
        }

        public string putTheMessageIntoQueue()
        {
            try
            {
                queue = queueManager.AccessQueue(QueueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);

                return "Enter the message to put in MQSeries server:";
                message = System.Console.ReadLine();
                int msgLen = message.Length;

                while (msgLen == 0)
                {
                    if (msgLen == 0)
                        return "Please reenter the message:";
                    message = System.Console.ReadLine();
                    msgLen = message.Length;
                }

                queueMessage = new MQMessage();
                queueMessage.WriteString(message);
                queueMessage.Format = MQC.MQFMT_STRING;
                queuePutMessageOptions = new MQPutMessageOptions();

                //putting the message into the queue
                queue.Put(queueMessage, queuePutMessageOptions);
                MessageID = queueMessage.MessageId;

                return "Success fully entered the message into the queue";
            }
            catch (Exception mqexp)
            {
                return "MQSeries Exception: " + mqexp.Message;
            }
        }

        public string GetMessageFromTheQueue()
        {

            queue = queueManager.AccessQueue(QueueName, MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
            queueMessage = new MQMessage();
            queueMessage.Format = MQC.MQFMT_STRING;
            queueMessage.MessageId = MessageID;
            queueGetMessageOptions = new MQGetMessageOptions();

            try
            {
                queue.Get(queueMessage, queueGetMessageOptions);

                // Received Message.
                //System.Console.WriteLine("Received Message: {0}", queueMessage.ReadString(queueMessage.MessageLength));
                return queueMessage.ReadString(queueMessage.MessageLength);
                //System.Console.WriteLine("Received Message: {0}", queueMessage.MessageId[0].ToString());

            }
            catch (Exception MQExp)
            {
                // report the error
                System.Console.WriteLine("MQQueue::Get ended with " + MQExp.Message);
                return "MQQueue::Get ended with " + MQExp.Message;
            }
        }

        public string GetMessageFromTheQueueAnyOrder()
        {
            char[] separator = { '/' };
            string[] ChannelParams;
            ChannelParams = ChannelInfo.Split(separator);
            channelName = ChannelParams[0];
            transportType = ChannelParams[1];
            connectionName = ChannelParams[2];

            QueueName = "ORANGE.QUEUE";
            QueueManagerName = "venus.queue.manager";
            ChannelInfo = "CHANNEL1/TCP/192.168.1.8";

            //queueManager = new MQQueueManager(QueueManagerName, channelName, connectionName);
            //In office
            queueManager = new MQQueueManager(QueueManagerName, "CHANNEL1", "192.168.1.8");
            //At home
            //queueManager = new MQQueueManager(QueueManagerName, "CHANNEL1", "http://www.ufissin.homelinux.net/");

            queue = queueManager.AccessQueue(QueueName, MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
            queueMessage = new MQMessage();
            queueMessage.Format = MQC.MQFMT_STRING;
            //queueMessage.MessageId = MessageID;
            queueGetMessageOptions = new MQGetMessageOptions();

            try
            {
                queue.Get(queueMessage, queueGetMessageOptions);

                // Received Message.
                //System.Console.WriteLine("Received Message: {0}", queueMessage.ReadString(queueMessage.MessageLength));
                return queueMessage.ReadString(queueMessage.MessageLength);

            }
            catch (Exception MQExp)
            {
                // report the error
                //System.Console.WriteLine("MQQueue::Get ended with " + MQExp.Message);
                return "MQQueue::Get ended with " + MQExp.Message;
            }
        }

    }
}
