#define LoggingOn
#define UsingQueues
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using IBM.WMQ;
using System.Timers;
using System.Configuration;
using iTrekWMQ;
using System.IO;
using iTrekXML;
using System.Xml;
using iTrekSessionMgr;
using iTrekUtils;


namespace iTrekJobMgrSvc
{
    /// <summary>
    /// Gets job xml batch and update data from queue and writes it to 
    /// job xml file on disk
    /// According to timing set in JOB_AFT_SERVICESTIMER
    /// </summary>
    
    public partial class iTrekJobMgrSvc : ServiceBase
    {
        iTXMLcl iTXML;
        iTXMLPathscl iTPaths ;
        int UpdateFlushCounter = 0;
        MQQueue queue;
        ITrekWMQClass1 wmq1;

        public iTrekJobMgrSvc()
        {
            InitializeComponent();
#if LoggingOn
            iTJobMgrLog.WriteEntry("Initialisation File Mgr");
#endif
            iTXML = new iTXMLcl();
            iTPaths = new iTXMLPathscl();
        }

        protected override void OnStart(string[] args)
        {
#if LoggingOn
            iTJobMgrLog.WriteEntry("In OnStart");
#endif
            wmq1 = new ITrekWMQClass1();
            //queue = wmq1.getQueueConnection(iTPaths.GetTOITREKREAD_JOBQueueName(), MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
            //Create an updateable jobs file if one doesn't already exist
            FileInfo fiJobsUpdateable = new FileInfo(iTPaths.GetUpdateableJobsFilePath());
            FileInfo fiJobsMain = new FileInfo(iTPaths.GetJobsFilePath());
            if(!fiJobsUpdateable.Exists)
                fiJobsMain.CopyTo(iTPaths.GetUpdateableJobsFilePath());
                
            MessageQueueTimer();
#if LoggingOn
            //iTJobMgrLog.WriteEntry("Finished in MessageQueueTimer");
#endif
        }

        protected override void OnStop()
        {
#if LoggingOn
            iTJobMgrLog.WriteEntry("In onStop.");
#endif
        }

        public void MessageQueueTimer()
        {
            
#if LoggingOn
            iTJobMgrLog.WriteEntry("In MessageQueueTimer");
#endif
            System.Timers.Timer aTimer = new System.Timers.Timer();
            aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            // Interval set in JOB_AFT_SERVICESTIMER
            aTimer.Interval = Convert.ToInt32(iTPaths.GetJOB_AFT_SERVICESTIMER());  
            aTimer.Enabled = true;
            //test = "test2";
        }

        // Specify what you want to happen when the Elapsed event is raised.
        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
           
                XmlDocument UpdateableJobsXml = new XmlDocument();
                XmlDocument MainJobsXml = new XmlDocument();
                if(iTPaths == null)
                    iTPaths = new iTXMLPathscl();
                if(iTXML == null)
                    iTXML = new iTXMLcl();
  
            try
            {
                UpdateableJobsXml.Load(iTPaths.GetUpdateableJobsFilePath());
                MainJobsXml.Load(iTPaths.GetJobsFilePath());
            }
            catch (Exception ex)
            {
#if LoggingOn
                iTJobMgrLog.WriteEntry("Load XMLs :" + ex.Message);
#endif
            }

            string message = "";         
            
#if UsingQueues

            try
            {
                if (wmq1 != null)
                {
                    queue = wmq1.getQueueConnection(iTPaths.GetTOITREKREAD_JOBQueueName(), MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
                }
                else
                {
                    wmq1 = new ITrekWMQClass1();
                    queue = wmq1.getQueueConnection(iTPaths.GetTOITREKREAD_JOBQueueName(), MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);

                }
                message = wmq1.GetMessageFromTheQueue(queue);
                wmq1.closeQueueConnection(queue, wmq1.getQManager);

            }
            catch (Exception ex)
            {
#if LoggingOn
                iTJobMgrLog.WriteEntry("Setup the queues :" + ex.Message);
#endif
            }
           
#else
            message = @"<JOB><URNO>" + iTPaths.GetTestJobURNO() + "</URNO><ACTION>INSERT</ACTION><PENO>" + iTPaths.GetTestPENO() + "</PENO><UAFT>" + iTPaths.GetTestUAFT() + "</UAFT></JOB>";
#endif
            string messageType = "";
            
            UpdateFlushCounter++;

            if (UpdateFlushCounter > Convert.ToInt32(iTPaths.GetFileMgrFlushSetting()))
            {
#if LoggingOn
                iTJobMgrLog.WriteEntry("UpdateFlushCounter : " + UpdateFlushCounter);
#endif
                UpdateFlushCounter = 0;

                //save the UpdateableJobsXml file to the MAIN Jobs.xml 
                try
                {
                    FileInfo fiJobsUpdateable = new FileInfo(iTPaths.GetUpdateableJobsFilePath());
                    FileInfo fiJobsMain = new FileInfo(iTPaths.GetJobsFilePath());
                    if (fiJobsMain.Exists && fiJobsUpdateable.Exists)
                        fiJobsUpdateable.CopyTo(iTPaths.GetJobsFilePath(), true);
                }catch (Exception ex)
                {
#if LoggingOn
                    iTJobMgrLog.WriteEntry("Instantiate FileInfo:" + ex.Message);
#endif
                }
            }

#if LoggingOn
            string shortMsg = message.Remove(20);
            iTJobMgrLog.WriteEntry("Short Message:" + shortMsg);
#endif

            if (message != "MQRC_NO_MSG_AVAILABLE")
            {
                messageType = iTXML.GetJobMsgActionFromMsg(message);
            }

#if LoggingOn
            iTJobMgrLog.WriteEntry("messageType: " + messageType);
#endif
              //try
              //  {
            //JOBS is jobs batch
            if (messageType == "JOBS")
            {
                try{
                    XmlDocument MainBatchJobsXml = new XmlDocument();
                    MainBatchJobsXml.LoadXml(message);
                    MainBatchJobsXml.Save(iTPaths.GetJobsFilePath());
                    MainBatchJobsXml.Save(iTPaths.GetUpdateableJobsFilePath());
                }catch (Exception ex)
                {
#if LoggingOn
                    iTJobMgrLog.WriteEntry("Instantiate MainBatchJobsXml:" + ex.Message);
#endif
                }
            }

            else if (messageType == "DELETE")
            {
                try{
                    //DeleteJobFromXMLFile ignores the ACTION tag
                    //handle UpdateableJobsXml becomes null
                   
                    UpdateableJobsXml = iTXML.DeleteJobFromXMLFile(UpdateableJobsXml, message);
                  
                }catch (Exception ex)
                {
#if LoggingOn
                    iTJobMgrLog.WriteEntry("DELETE DeleteJobFromXMLFile:" + ex.Message);
#endif
                }
                try
                {
                    UpdateableJobsXml.Save(iTPaths.GetUpdateableJobsFilePath());
                }catch (Exception ex)
                {
#if LoggingOn
                    iTJobMgrLog.WriteEntry("DELETE GetUpdateableJobsFilePath:" + ex.Message);
#endif
                }
            }
            else if (messageType == "INSERT")
            {
                try{
                    message = iTXML.DeleteActionFromJobXMLFile(message);
                }catch (Exception ex)
                {
#if LoggingOn
                    iTJobMgrLog.WriteEntry("INSERT DeleteActionFromJobXMLFile:" + ex.Message);
#endif
                }
                try{
                    UpdateableJobsXml = iTXML.WriteNewJobToXMLFile(UpdateableJobsXml, message);
                }catch (Exception ex)
                {
#if LoggingOn
                    iTJobMgrLog.WriteEntry("INSERT WriteNewJobToXMLFile:" + ex.Message);
#endif
                }
                try{
                    UpdateableJobsXml.Save(iTPaths.GetUpdateableJobsFilePath());
                }catch (Exception ex)
                {
#if LoggingOn
                    iTJobMgrLog.WriteEntry("INSERT GetUpdateableJobsFilePath:" + ex.Message);
#endif
                }
            }
            else if (messageType == "UPDATE")
            {
                try{
                    message = iTXML.DeleteActionFromJobXMLFile(message);
                }catch (Exception ex)
                {
#if LoggingOn
                    iTJobMgrLog.WriteEntry("UPDATE DeleteActionFromJobXMLFile:" + ex.Message);
#endif
                }
              try{
                    UpdateableJobsXml = iTXML.UpdateJobInXMLFile(UpdateableJobsXml, message);
                }catch (Exception ex)
                {
#if LoggingOn
                    iTJobMgrLog.WriteEntry("UPDATE UpdateJobInXMLFile:" + ex.Message);
#endif
                }
                try{
                    UpdateableJobsXml.Save(iTPaths.GetUpdateableJobsFilePath());
               }catch (Exception ex)
                {
#if LoggingOn
                    iTJobMgrLog.WriteEntry("UPDATE GetUpdateableJobsFilePath:" + ex.Message);
#endif
                }
                
            }
            else
            {
                //do NOTHING
            }
//        }catch (Exception ex)
//                {
//#if LoggingOn
//            iTJobMgrLog.WriteEntry(ex.Message);
//#endif
           
//                }
        }
    }
}
