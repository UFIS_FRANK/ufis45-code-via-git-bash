//#define UseTestData
#define Production
using System;
using System.Xml;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using System.Web.Mobile;
using iTrekSessionMgr;
using iTrekXML;

public partial class Login : System.Web.UI.MobileControls.MobilePage
{

    iTXMLPathscl iTPaths = new iTXMLPathscl();
    iTXMLcl iTXML = new iTXMLcl();
    SessionManager sessMan;
    string SID = "";
    string sessionid = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                if (Session.Count > 0)
                    MobileFormsAuthentication.RedirectFromLoginPage(Session["staffid"].ToString(), false);
            }
            else
            {
#if UseTestData

        sessionid = iTPaths.GetTestSessionId();
        Session["sessionid"] = sessionid;
        SID = iTPaths.GetTestSID();

#else
                sessionid = Page.Session.SessionID;
                Session["sessionid"] = sessionid;
                //Device sends a Request.Headers["x-up-subno"];
                //when appl live
                SID = Request.Headers["x-up-subno"].ToString();
                SID = SID.Replace("_WAPGW.grid.net.sg", "");
#endif

            }
        }
        catch(Exception LoginEx)
        
        {
            ActiveForm = frmLoginError;
            lblLoginErrorMsg.Text = "Some unexpected error occured. Please try again.";
        
        
        
        }
    }

    protected void Login_Click(Object sender, EventArgs e)
    {
#if Production
        if (txtbxstaffid.Text.Length == 0 || txtbxpword.Text.Length == 0)
#else
         if (txtbxstaffid.Text.Length != 6 || txtbxpword.Text.Length != 6)
#endif
       
        {
            message.Text = "Staff ID does not exist.  Please try again or contact supervisor or administrator";
            message.Visible = true;
        }
        //else if (iTXML.PENOExist("00" + txtbxstaffid.Text))
        //{
        //    message.Text = "Staff ID currently in use. Please try again or contact supervisor or administrator";
        //    message.Visible = true;
        //}
        else
        {
            txtbxstaffid.Text = "00" + txtbxstaffid.Text;
            Session["staffid"] = txtbxstaffid.Text;
#if Production
            txtbxpword.Text = txtbxpword.Text;
#else
            txtbxpword.Text = "00" + txtbxpword.Text;
#endif

            
            //TODO: Configure DateTime.Now?
            sessMan = new SessionManager(SID, txtbxstaffid.Text, DateTime.Now, txtbxpword.Text);
            sessMan.CreateSessionDirectory(sessionid);
            if (sessMan.IsValidSessionWithPassword(sessionid))
            {
                //when login use Session Manager constructor that checks time and 
                //also checks device and staffid s
                iTXML.WriteStaffSessionandDeviceIDXmlToDeviceIDFile(sessionid, SID, txtbxstaffid.Text);
                
                Session["loginTime"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
               
                XmlDocument xmldoc = sessMan.GetFlightsByStaffID(txtbxstaffid.Text, iTPaths.GetJobsFilePath(), iTPaths.GetFlightsFilePath());
                sessMan.CreateXMLFileInSessionDirectory(sessionid, xmldoc, "flights.xml");
                

                MobileFormsAuthentication.RedirectFromLoginPage(txtbxstaffid.Text, false);
            }
            else
            {
                if (sessMan.geterrMsgFromMQ != "success")
                {
                    lblstaffid.Visible = true;
                    txtbxstaffid.Visible = true;
                    cmdLogin.Visible = true;
                    message.Visible = true;
                    txtbxstaffid.Text = "";
                    message.Text = "Sorry,we are unable to process your request.Please try again after some time";
                
                }
                else
                {
                    lblstaffid.Visible = true;
                    txtbxstaffid.Visible = true;
                    cmdLogin.Visible = true;
                    message.Visible = true;
                    txtbxstaffid.Text = "";
                    message.Text = "Staff ID does not exist.  Please try again or contact supervisor or administrator";
                }
            }
        }

    }

    protected override void OnViewStateExpire(EventArgs e)
    {
        ActiveForm = frmLoginError;
        lblLoginErrorMsg.Text = "Timeout error occured. Please try again.";
    }

    protected void cmdLoginError_Click(object sender, EventArgs e)
    {
        ActiveForm = formA;
    }
}
