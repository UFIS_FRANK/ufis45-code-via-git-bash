<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Page Inherits="System.Web.UI.MobileControls.MobilePage" Language="C#" %>
<%@ Import Namespace="System.Web.Mobile" %>
<%@ Import Namespace="System.Web.Security" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="iTrekData" %>


<script runat="Server">
public void Page_Load(Object o, EventArgs e)
{
    //Response.AddHeader("Refresh", "5");
    OraDb ordb = new OraDb();
    ObjectList1.DataSource = ordb.GetFlightData();
    ObjectList1.DataBind();
}

public void Logout_Click(Object o, EventArgs e)
{
    ActiveForm = formB;
    MobileFormsAuthentication.SignOut();
}
</script>
<html>
<body>
<mobile:Form id="Form1" runat="server" paginate="False">
    <mobile:Label id="label1" runat="server"></mobile:Label> 
    <mobile:ObjectList id="ObjectList1" runat="server" CommandStyle-StyleReference="subcommand" LabelStyle-StyleReference="title" ></mobile:ObjectList> 
    <mobile:Command id="Command1" onclick="Logout_Click" runat="server" softkeyLabel="go">Logout</mobile:Command>
</mobile:Form>
<mobile:Form id="formB" runat="server">
	<mobile:Label id="Label2" runat="server">Good Bye</mobile:Label>
	<mobile:Link id="Link1" runat="server" navigateURL="Login.aspx">Log back in</mobile:Link>
</mobile:Form>
</body>
</html>
