<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Menu.aspx.cs" Inherits="Menu" EnableSessionState="True"%>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmMenu" runat="server"><mobile:Label ID="lblTitle" Runat="server"
        Alignment="Center" EnableViewState="False">SATS iTrek</mobile:Label><mobile:Label ID="lblMenu" Runat="server"
            Alignment="Center" EnableViewState="False">MAIN MENU</mobile:Label>
        <mobile:Label ID="lblUserIdName" Runat="server" Alignment="Center" Visible="False">
        </mobile:Label>     
        <mobile:Label ID="lblAlertMsg" Runat="server" Alignment="Center" Visible="False" EnableViewState="False">
        </mobile:Label>
        <mobile:List ID="lstMainMenu" Runat="server" OnItemCommand="lstMainMenu_ItemCommand" >
            <Item Text="Open Flights" />
            <Item Text="Logout" />
        </mobile:List>        
    </mobile:Form>
    <mobile:Form ID="frmOpenFlights" Runat="server">
        <mobile:Label ID="lblTitle1" Runat="server" Alignment="Center" EnableViewState="False">SATS iTrek</mobile:Label>
        <mobile:Label ID="lblOpenFlights" Runat="server" Alignment="Center"></mobile:Label>
        <mobile:List ID="lstOpenFlights" Runat="server" OnItemCommand="lstOpenFlights_ItemCommand"  BreakAfter="True" >
        </mobile:List>
         <br />
        <mobile:Command ID="cmdBack" Runat="server" OnClick="cmdBack_Click" EnableViewState="False">Back</mobile:Command>
       </mobile:Form>
    <mobile:Form ID="frmLogout" Runat="server">
        <mobile:Label ID="lblTitle2" Runat="server" Alignment="Center">SATS iTrek</mobile:Label>
        <mobile:Label ID="lblLogout" Runat="server" Alignment="Center">
        </mobile:Label>
          <mobile:TextView ID="txVLogout" Runat="server">
        </mobile:TextView>
         <mobile:Command ID="cmdLogin" Runat="server" OnClick="cmdLogin_Click" SoftkeyLabel="Go">
        </mobile:Command>
    </mobile:Form><mobile:Form ID="frmOpenFlightsMenu" Runat="server"><mobile:Label ID="lblTitle3"
        Runat="server" Alignment="Center" EnableViewState="False">SATS iTrek</mobile:Label>
        <mobile:TextView ID="txtVFlightDetailsOpenFlightMenu" Runat="server">
        </mobile:TextView>
      
        <mobile:Label ID="lblStaffNo" Runat="server">
        </mobile:Label>
     
        <mobile:List ID="lstSelFlights" Runat="server" OnItemCommand="lstSelFlights_ItemCommand">
            <Item Text="ATD        " />
            <Item Text="ALL OK2LOAD" />
            <Item Text="SHOW DLS" />
        </mobile:List>
        <mobile:Command ID="cmdBack1" Runat="server" OnClick="cmdBack1_Click">Back</mobile:Command>
        
    </mobile:Form><mobile:Form ID="frmTiming" Runat="server"><mobile:Label ID="lblTitle4" Runat="server" Alignment="Center">SATS iTrek</mobile:Label>
        <mobile:TextView ID="txtVFlightDetailsTiming" Runat="server"></mobile:TextView>
      
        <mobile:Label ID="lblSelectedOption" Runat="server">
        </mobile:Label>
    
        <mobile:Label ID="lblTimingAlertMsg" Runat="server" EnableViewState="False" Visible="False" BreakAfter="True" >
        </mobile:Label>        
        <mobile:List ID="lstTime" Runat="server" OnItemCommand="lstTime_ItemCommand">
         </mobile:List>        
        <mobile:Label ID="lblATD" Runat="server"  >ATD </mobile:Label> 
        <br />                   
        <mobile:TextBox ID="txtBxGetTime" Runat="server" EnableViewState="False" MaxLength="4"
            Numeric="True" Size="4">
        </mobile:TextBox>
       
        <br />
   <mobile:Command ID="cmdTime" Runat="server" OnClick="cmdTime_Click" >Go </mobile:Command>
       
        <mobile:Command ID="cmdBackfrmTime" Runat="server" OnClick="cmdBackfrmTime_Click">Back</mobile:Command>
      
    </mobile:Form>
   <mobile:Form ID="frmOK2Load" Runat="server"><mobile:Label ID="lblTitle6" Runat="server"
        Alignment="Center" EnableViewState="False">SATS iTrek</mobile:Label>
        <br />
        <mobile:TextView ID="txtVFlightDetailsOK2L" Runat="server">
        </mobile:TextView>
        <mobile:Label ID="lblOk2Load" Runat="server">
        </mobile:Label>
        <br />
        <mobile:List ID="lstAllOK2Load" Runat="server">
        </mobile:List>
        <mobile:Command ID="cmdBackFromOK2L" Runat="server" OnClick="cmdBackFromOK2L_Click"
            SoftkeyLabel="Back">
        </mobile:Command>
    </mobile:Form>
    <mobile:Form ID="frmDLS" Runat="server">
        <mobile:Label ID="lblTitle5" Runat="server" Alignment="Center" EnableViewState="False">SATS iTrek</mobile:Label>
        <mobile:TextView ID="txtVFlightDetailsDLS" Runat="server">
        </mobile:TextView>
        <mobile:Label ID="lblDLS" Runat="server">
        </mobile:Label>
        <br />
        <mobile:TextView ID="txtVShowDLS" Runat="server">
        </mobile:TextView>
        <br />
        <mobile:Command ID="cmdBackFromDLS" Runat="server" OnClick="cmdBackFromDLS_Click"
            SoftkeyLabel="Back">
        </mobile:Command>
    </mobile:Form>
    <mobile:Form ID="frmError" Runat="server">    
        <mobile:Label ID="lblErrTitle" Runat="server" Alignment="Center" EnableViewState="False">SATS iTrek</mobile:Label>
        <br /><mobile:Label ID="lblErrorMsg" Runat="server" EnableViewState="False"></mobile:Label>
        <mobile:Command ID="cmdError" Runat="server" OnClick="cmdError_Click">Go</mobile:Command>
    </mobile:Form>

</body>
</html>
