<%@ Import Namespace="System.Web.Security" %>
<%@ Import Namespace="System.Web.Mobile" %>
<%@ Import Namespace="System.Web.UI" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="iTrekSessionMgr" %>
<%@ Import Namespace="iTrekXML" %>
<%@ Import Namespace="iTrekWMQ" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>

<%@ Page Inherits="System.Web.UI.MobileControls.MobilePage" Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="Server">
    string FlightNo = "";
    string selectedTime = "";
    //string selectedFlight = "";
    //TODO: Change to use without session vars?
    //we currently have (check for when assigned!)
    //Session["StaffID"]
    //Session["sessionid"]
    //Session["SelectedFlight"]
    //Session["loginTime"]
    //Session["staffid"]
    //Session["StaffID"]
    //Session["FlightURNO"]
    //in flight class (itxmlcl) we have:
    //    this._FLNO = FLNO;
    //    this._STOD = STOD;
    //    this._STOA = STOA;
    //    this._REGN = REGN;
    //    this._DES3 = DES3;
    //    this._ETDI = ETDI;
    //    this._ETOA = ETOA;
    //    this._EQPS = EQPS;
    //    this._ADID = ADID;
    //    this._POS1 = POS1;
    //    this._URNO = URNO;

    public void Page_Load(Object o, EventArgs e)
    {
       
        SessionManager sessMan = new SessionManager();
        if (!(sessMan.IsValidLoginTime(Convert.ToDateTime(Session["loginTime"]))))
        {
            lblAlertMsg.Visible = true;
            lblAlertMsg.Text = "Your session has expired. Please login again.";
            selLstMainMenu.Visible = false;
            cmdSubmit.Visible = false;
            cmdLoginFromMainMenu.Visible = true;
        }
        else
        {

            lblUserIdName.Visible = true;
            //lblUserIdName.Text = Session["staffid"] + "-" + sessMan.ReadStaffNameFromStaffIDFile(Session["staffid"].ToString());
            //GetStaffNameFromcurrentMsgInSessionDir uses the staff name set by IsValidLogin
            lblUserIdName.Text = Session["staffid"] + "-" + sessMan.GetStaffNameFromAuthFileInSessionDir(Session["sessionid"].ToString());
            
            try
            {
                if (Request.QueryString["info"].ToString() != null)
                {
                    lblAlertMsg.Visible = true;
                    lblAlertMsg.Text = Request.QueryString["info"].ToString();
                    cmdRefresh.Visible = true;
                    cmdSubmit.Visible = false;
                    selLstMainMenu.Visible = false;
                }
            }
            catch (Exception ex)
            {
                //swallow error - ex.Message;
            }
        }
    }

    protected void cmdSubmitTime_Click(object sender, EventArgs e)
    {
        iTXMLPathscl iTPaths = new iTXMLPathscl();
        iTXMLcl iTXML = new iTXMLcl();
        //process the time input box if there's anything in it
        if (txtBxGetTime.Text == "")
            selectedTime = selLstTimes.Items[selLstTimes.SelectedIndex].ToString();
        else
            selectedTime = txtBxGetTime.Text;

        selectedTime = selectedTime.Replace("NOW ", "");
        if (lblSelectedOption.Text == "ATD")
        {
            if (iTPaths.ApplicationMode != "Debug")
            {
                iTrekWMQClass iTWMQ = new iTrekWMQClass(iTPaths.GetFROMITREKUPDATEQueueName());
                iTWMQ.PutATDTimingOnFROMITREKUPDATEQueue(Convert.ToString(Session["FlightURNO"]), lblSelectedOption.Text, selectedTime);

            }//HACK: Improve
            //uses message format to delete the flight
            //from the session and the main files
            XmlDocument xmlDoc = new XmlDocument();
            string xmlDocFileName = iTPaths.GetXMLPath() + Convert.ToString(Session["sessionid"]) + @"\flights.xml";
            XmlDocument xmlDoc2 = new XmlDocument();
            //TODO: iTPaths.GetFlightsFilePath()?
            string xmlDoc2FileName = iTPaths.GetXMLPath() + @"\Flights\Flights.xml";
            xmlDoc.Load(xmlDocFileName);
            //DeleteFlightFromXMLFile works with a string message
            string message = @"<FLIGHT><URNO>" + Convert.ToString(Session["FlightURNO"]) + "</URNO><ACTION>DELETE</ACTION></FLIGHT>";
            xmlDoc = iTXML.DeleteFlightFromXMLFile(xmlDoc, message);
            xmlDoc.Save(xmlDocFileName);

            xmlDoc2.Load(xmlDoc2FileName);
            iTXML.DeleteFlightFromXMLFile(xmlDoc2, message);
            xmlDoc2.Save(xmlDoc2FileName);
            ActiveForm = frmMainMenu;
            //ActiveForm = frmOpenFlights;
        }
    }



    protected void cmdSubmitFromMainMenu_Click(object sender, EventArgs e)
    {
        iTXMLPathscl itpaths = new iTXMLPathscl();
        iTXMLcl itxml = new iTXMLcl(itpaths.applicationBasePath);
        SessionManager sessMan = new SessionManager();
        if (selLstMainMenu.Selection.Value == "Open Flights")
        {
            ArrayList lstFlights = itxml.UserSessionReadElementsForArrayListFromCurrentFlightXMLFile(Session["sessionid"].ToString(), "//FLIGHTS/FLIGHT");
            if (lstFlights != null && lstFlights.Count != 0)
            {
                objListOpenFlights.DataSource = lstFlights;
                objListOpenFlights.LabelField = "DISPLAYOPENFLIGHTS";

                objListOpenFlights.DataBind();
                ActiveForm = frmOpenFlights;
               
                //ActiveForm = frmOpenFlights;
                lblOpenFlights.Text = selLstMainMenu.Selection.Value;
            }
            else
            {
                lblAlertMsg.Visible = true;
                lblAlertMsg.Text = "There are currently no departure flights assigned to your Staff ID";
                //Reload the flights on the page
                //MobilePage
                //ActiveForm = frmMainMenu;
                //ActiveForm = frmOpenFlights;
                //frmOpenFlights.Form.r
                //RedirectToMobilePage("LoggedIn.aspx");
            }
        }
        else
        {
            ActiveForm = LogoutForm;
            lblLogout.Text = selLstMainMenu.Selection.Value;
            txVLogout.Text = "You logged out at " + DateTime.Now.Hour + ":" + DateTime.Now.Minute + "<br>";
            txVLogout.Text += "Press 'Login' to re-loggin";
            //sessMan.DeleteSessionDirectory(Convert.ToString(Session["staffid"]));
            sessMan.DeleteSessionDirectory(Convert.ToString(Session["sessionid"]));
        }
    }

    protected void cmdSubmitFromfrmOpenFlightsMenu_Click(object sender, EventArgs e)
    {
        iTXMLPathscl itpaths = new iTXMLPathscl();
        iTXMLcl itxml = new iTXMLcl(itpaths.applicationBasePath);
        SessionManager sessMan = new SessionManager();
        //FlightNo = lblOpenFlightsMenu.Text;

        if (selLstOpenFlightsMenu.Selection.Value == "ATD")
        {
            ActiveForm = frmTiming;
            txtVFlightDetailsTiming.Text = Session["SelectedFlight"].ToString();
            lblSelectedOption.Text = "ATD";
            //lblFlightNo.Text = FlightNo;
            DateTime currentTime = DateTime.Now;
            selLstTimes.Items[0].Text = "NOW " + currentTime.ToString("HH:mm");
            selLstTimes.Items[1].Text = currentTime.AddMinutes(-1.0).ToString("HH:mm");
            selLstTimes.Items[2].Text = currentTime.AddMinutes(-2.0).ToString("HH:mm");
            selLstTimes.Items[3].Text = currentTime.AddMinutes(-3.0).ToString("HH:mm");
            selLstTimes.Items[4].Text = currentTime.AddMinutes(-4.0).ToString("HH:mm");
        }
        else if (selLstOpenFlightsMenu.Selection.Value == "AllOK2Load")
        {
            //TODO;Remove?
            ActiveForm = frmOK2Load;
            lblOk2Load.Text = selLstOpenFlightsMenu.Selection.Value;
            txtVFlightDetailsOK2L.Text = Session["SelectedFlight"].ToString();

            //ArrayList lstOK2Loads = itxml.ReadAttributesForArrayListFromCurrentOK2LoadXMLFile();
            //string temp = Session["FlightNO"].ToString();
            //Have to go through GetAllOk2LoadAL (not GetAllOk2LoadALFromSessionDir) to
            //implement delay timing
            ArrayList lstOK2Loads = sessMan.GetAllOk2LoadAL(Session["FlightNO"].ToString(), Session["FlightURNO"].ToString(), Session["sessionid"].ToString());
           
            lstAllOK2Load.Visible = true;
            lstAllOK2Load.DataSource = lstOK2Loads;
            lstAllOK2Load.DataBind();
            ActiveForm = frmOK2Load;
            lblOk2Load.Text = selLstOpenFlightsMenu.Selection.Value;
        }
        else //if (selLstMainMenu.Selection.Value == "DLS")
        {
            //TODO: Change GetShowDLSAL to return a string
            ArrayList lstShowDLS = sessMan.GetShowDLSAL(Session["FlightNO"].ToString(), Session["FlightURNO"].ToString(), Session["sessionid"].ToString());
            string[] strArr = (string[])lstShowDLS.ToArray((typeof(string)));
            txtVShowDLS.Visible = true;
            txtVShowDLS.Text = strArr[0].ToString();
            //txtVShowDLS.DataBind();
            ActiveForm = DLSForm;
            lblDLS.Text = selLstOpenFlightsMenu.Selection.Value;
            txtVFlightDetailsDLS.Text = Session["SelectedFlight"].ToString();
        }
    }


    protected void objListOpenFlights_ItemCommand(object sender, ObjectListCommandEventArgs e)
    {
        //Below works due to class Flight objects added to flight arraylist
        //selectedFlight = e.ListItem["DISPLAYFLIGHTSMENU"].ToString();
        Session["SelectedFlight"] = e.ListItem["DISPLAYFLIGHTSMENU"].ToString();
        Session["FlightURNO"] = e.ListItem["URNO"].ToString();
        Session["FlightNO"] = e.ListItem["FLNO"].ToString();
 
        ActiveForm = frmOpenFlightsMenu;
        txtVFlightDetailsOpenFlightMenu.Text = Session["SelectedFlight"].ToString();

    }

    protected void cmdBackFromTiming_Click(object sender, EventArgs e)
    {
        ActiveForm = frmOpenFlightsMenu;
    }


    protected void cmdBackFromDLS_Click(object sender, EventArgs e)
    {
        ActiveForm = frmOpenFlightsMenu;
    }

    protected void cmdBackFromOK2L_Click(object sender, EventArgs e)
    {
        ActiveForm = frmOpenFlightsMenu;
    }


    protected void cmdRefresh_Click(object sender, EventArgs e)
    {
        RedirectToMobilePage("LoggedIn.aspx");
    }

    protected void cmdLogin_Click(object sender, EventArgs e)
    {
        RedirectToMobilePage("Login.aspx");
    }

    protected void frmOpenFlightsBack_Click(object sender, EventArgs e)
    {
        ActiveForm = frmMainMenu;
    }

    protected void cmdOpenFlightsMenuBack_Click(object sender, EventArgs e)
    {
        ActiveForm = frmOpenFlights;
    }

   
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<body>
    <mobile:Form ID="frmMainMenu" Runat="server" Paginate="True"><mobile:Label ID="lblSATSHeader"
        Visible="true" Runat="server" Alignment="Center">SATS iTrek</mobile:Label><br /><mobile:Label
            ID="Label1" Runat="server" Alignment="Center">MAIN MENU</mobile:Label> <mobile:Label
                ID="lblUserIdName" Runat="server" Visible="False" Alignment="Center">
            </mobile:Label><br /><mobile:Label ID="lblAlertMsg" Runat="server" Alignment="Center"
                Visible="False">
            </mobile:Label>&nbsp;<br /><mobile:SelectionList ID="selLstMainMenu" Runat="server"
                SelectType="Radio">
                <Item Selected="True" Text="Open Flights" Value="Open Flights" />
                <Item Text="Logout" Value="Logout" />
            </mobile:SelectionList> <mobile:Command ID="cmdLoginFromMainMenu" Runat="server" OnClick="cmdLogin_Click" SoftkeyLabel="Login" Visible="False">Login</mobile:Command><br /><mobile:Command ID="cmdSubmit" Runat="server" OnClick="cmdSubmitFromMainMenu_Click"
                SoftkeyLabel="Submit">Submit</mobile:Command><br /><mobile:Command ID="cmdRefresh"
                    Runat="server" OnClick="cmdRefresh_Click" SoftkeyLabel="Refresh" Visible="False">Refresh</mobile:Command></mobile:Form>
    <mobile:Form ID="frmOpenFlights" Runat="server" Paginate="True"><mobile:Label ID="Label2"
        Visible="true" Runat="server" Alignment="Center">SATS iTrek</mobile:Label><br /><mobile:Label
            ID="lblOpenFlights" Runat="server" Alignment="Center">
        </mobile:Label>&nbsp;&nbsp;<br /><mobile:ObjectList ID="objListOpenFlights" Runat="server"
            CommandStyle-StyleReference="subcommand" DefaultCommand="objListShowFlight" LabelStyle-StyleReference="title"
            OnItemCommand="objListOpenFlights_ItemCommand">
            <Command Name="cmdDisplayFlightOptions" Text="DisplayFlightOptions" />
</mobile:ObjectList><br /><br /><mobile:Command ID="cmdBack" Runat="server" OnClick="frmOpenFlightsBack_Click"
            SoftkeyLabel="Back">Back</mobile:Command></mobile:Form><br>
</br>
    <mobile:Form ID="frmTiming" Runat="server"><mobile:Label ID="lblSatsiTrek3" Visible="true"
        Runat="server" Alignment="Center">SATS iTrek</mobile:Label>&nbsp;<mobile:TextView
            ID="txtVFlightDetailsTiming" Runat="server">
        </mobile:TextView><br /><mobile:Label ID="lblSelectedOption" Runat="server">
        </mobile:Label> <br /><mobile:SelectionList ID="selLstTimes" Runat="server" Rows="5"
            SelectType="Radio">
            <Item Selected="True" />
            <Item />
            <Item />
            <Item />
            <Item />
        </mobile:SelectionList> <br /><mobile:TextBox ID="txtBxGetTime" Runat="server">
        </mobile:TextBox> <br /><mobile:Command ID="cmdSendTime" Runat="server" OnClick="cmdSubmitTime_Click"
            SoftkeyLabel="Send">Send</mobile:Command> <br /><mobile:Command ID="cmdBackFromTiming"
                Runat="server" OnClick="cmdBackFromTiming_Click" SoftkeyLabel="Back">Back</mobile:Command></mobile:Form><br>
</br>
    <mobile:Form ID="DLSForm" Runat="server"><mobile:Label ID="lblSatsiTrek4" Visible="true"
        Runat="server" Alignment="Center">SATS iTrek</mobile:Label>&nbsp;<br /><mobile:TextView
            ID="txtVFlightDetailsDLS" Runat="server">
        </mobile:TextView> <br /><mobile:Label ID="lblDLS" Runat="server">
        </mobile:Label> <mobile:TextView ID="txtVShowDLS" Runat="server">
        </mobile:TextView><br /><br /><mobile:Command ID="cmdBackFromDLS" Runat="server" OnClick="cmdBackFromDLS_Click"
            SoftkeyLabel="Back">Back</mobile:Command></mobile:Form><br>
</br>
    <mobile:Form ID="frmOK2Load" Runat="server"><mobile:Label ID="lblSatsiTrek5" Visible="true"
        Runat="server" Alignment="Center">SATS iTrek</mobile:Label>&nbsp;<br /><mobile:TextView
            ID="txtVFlightDetailsOK2L" Runat="server">
        </mobile:TextView> <br /><mobile:Label ID="lblOk2Load" Runat="server">
        </mobile:Label> <mobile:List ID="lstAllOK2Load" Runat="server">
        </mobile:List><br /><br /><mobile:Command ID="cmdBackFromOK2L" Runat="server" OnClick="cmdBackFromOK2L_Click"
            SoftkeyLabel="Back">Back</mobile:Command></mobile:Form><br>
</br>
    <mobile:Form ID="LogoutForm" Runat="server">
        <mobile:Label ID="lblSatsiTrek6" Visible="true" Runat="server" Alignment="Center">SATS iTrek</mobile:Label>
        <br />
        <mobile:Label ID="lblLogout" Runat="server" Alignment="Center">
        </mobile:Label>
        <mobile:TextView ID="txVLogout" Runat="server">
        </mobile:TextView>
        <br />
        <mobile:Command ID="cmdLogout" Runat="server" OnClick="cmdLogin_Click" SoftkeyLabel="Logout">Login</mobile:Command>
    </mobile:Form><br>
</br>
    <br>
</br>
    <mobile:Form ID="frmOpenFlightsMenu" Runat="server" Paginate="True"><mobile:Label
        ID="Label4" Visible="true" Runat="server" Alignment="Center">SATS iTrek</mobile:Label> <mobile:TextView
            ID="txtVFlightDetailsOpenFlightMenu" Runat="server">
        </mobile:TextView>&nbsp;<br /><br /><mobile:SelectionList ID="selLstOpenFlightsMenu"
            Runat="server" SelectType="Radio">
            <Item Selected="True" Text="ATD" Value="ATD" />
            <Item Text="ALL OK2LOAD" Value="AllOK2Load" />
            <Item Text="SHOW DLS" Value="SHOWDLS" />
</mobile:SelectionList><mobile:Command ID="cmdOpenFlightsMenu" Runat="server" OnClick="cmdSubmitFromfrmOpenFlightsMenu_Click"
            SoftkeyLabel="Submit">Submit</mobile:Command><br /><mobile:Command ID="cmdOpenFlightsMenuBack"
                Runat="server" OnClick="cmdOpenFlightsMenuBack_Click" SoftkeyLabel="Back">Back</mobile:Command></mobile:Form><br>
</br>
    &nbsp;
</body>
</html>
