
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Page  Inherits="Login" Language="C#" CodeFile="Login.aspx.cs" EnableSessionState="True" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script runat="Server">
   
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
<mobile:Form id="formA" runat="server" Paginate="True"  >
    
    
    <mobile:Label runat="server" id="heading" Alignment="Center" EnableViewState="False">SATS iTrek</mobile:Label>
   
    <mobile:Label runat="server" id="lblstaffid" EnableViewState="False">Login Staff ID</mobile:Label>
    <mobile:TextBox id="txtbxstaffid" runat="Server"/>
    <mobile:Label runat="server" id="lblpword" EnableViewState="False">Enter password</mobile:Label>
    <mobile:TextBox id="txtbxpword" runat="Server" Password="True" />
    <mobile:Command runat="Server" OnClick="Login_Click" SoftkeyLabel="Go" ID="cmdLogin">Go</mobile:Command>
    <mobile:Label id="message" visible="false" runat="server" EnableViewState="False"/>
</mobile:Form>
<mobile:Form ID="frmLoginError" Runat="server">    
        <mobile:Label ID="lblErrTitle" Runat="server" Alignment="Center" EnableViewState="False">SATS iTrek</mobile:Label>
        <br /><mobile:Label ID="lblLoginErrorMsg" Runat="server" EnableViewState="False"></mobile:Label>
        <mobile:Command ID="cmdLoginError" Runat="server" OnClick="cmdLoginError_Click" >Go</mobile:Command>
    </mobile:Form>
</body>
</html>