using System;
using System.Data;
using System.Web.Mobile;
using System.Web.Security;
using System.Xml;
using iTrekData;

public partial class UpdateFlightData : System.Web.UI.MobileControls.MobilePage
{
    dsUFIS.AFMTABDataTable afmttab = new dsUFIS.AFMTABDataTable();
    //dsUFIS dsufis = new dsUFIS();

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack){
        OraDb ordb = new OraDb();
        //afmttab = ordb.GetFlightData();
        //afmttab = dsufis.AFMTAB;
        ObjectList1.DataSource = afmttab;
        //ObjectList1.DataSource = ordb.UpdateFlightDataValue("jh8", "22D");
        //ObjectList1.DataSource = ordb.InsertFlightDataValue("jh9");
        ObjectList1.DataBind();
        }
    }

    protected void Logout_Click(object sender, EventArgs e)
    {
        MobileFormsAuthentication.SignOut();
        Response.Redirect("Login.aspx");
    }
    protected void ObjectList1_ItemCommand(object sender, System.Web.UI.MobileControls.ObjectListCommandEventArgs e)
    {
        //label1.Text = e.ListItem[0];
        DataRow dr = afmttab.Rows[0];
        ObjectList1.DataSource = afmttab;
        ObjectList1.DataBind();
    }
}
