#define UsingQueues
using System;
using System.Configuration;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using System.IO;
using System.Net;
using System.Xml;
using iTrekSessionMgr;
using iTrekXML;
using iTrekWMQ;
using iTrekUtils;

public partial class Menu : System.Web.UI.MobileControls.MobilePage
{
    //TODO: Change to use without these vars?
    string FlightNo = "";
    string selectedTime = "";
    //TODO: Change to use without session vars?

    protected void Page_Load(object sender, EventArgs e)
    {


        try
        {
            SessionManager sessMan = new SessionManager();
            //Avoid null obj ref error

            if (Context.Session.Count == 0)
            {
                ActiveForm = frmError;
                lblErrorMsg.Text = "Your session has expired. Please login again.";
                //RedirectToMobilePage("Login.aspx");
            }
            else
            {
             
                string sessid = Session["sessionid"].ToString();
                //if (!(sessMan.IsValidLoginTime(Convert.ToDateTime(sessMan.GetUserLoginTime(Session["sessionid"].ToString())))))
                if (!(sessMan.IsValidLoginTime(Convert.ToDateTime(sessMan.GetUserLoginTime(Convert.ToString(sessid))))))
                {
                    lblAlertMsg.Visible = true;
                    lblAlertMsg.Text = "Your session has expired. Please login again.";
                    lstMainMenu.Visible = false;

                }
                else
                {
                    lblUserIdName.Visible = true;

                    //GetStaffNameFromcurrentMsgInSessionDir uses the staff name set by IsValidLogin
                    lblUserIdName.Text = Session["staffid"] + "-" + sessMan.GetStaffNameFromAuthFileInSessionDir(Session["sessionid"].ToString());
                    //Test t = new Test();
                    //t.GetPENOSByUAFTFromJobsXML("1464882112");


                    try
                    {
                        if (Request.QueryString["info"].ToString() != null)
                        {
                            lblAlertMsg.Visible = true;
                            lblAlertMsg.Text = Request.QueryString["info"].ToString();
                            lstMainMenu.Visible = false;
                        }
                    }
                    catch (Exception ex)
                    {//Form.PagerStyle.NextPageText 

                        //swallow error - ex.Message;
                    }
                }
            }
        }
        catch (Exception genEx)
        {

            ActiveForm = frmError;
            lblErrorMsg.Text = "Some unexpected error occured. Please try again.";
        
        }
    }
    protected void lstMainMenu_ItemCommand(object sender, ListCommandEventArgs e)
    {
       
        int selIndex = e.ListItem.Index;
         if (e.ListItem.Text == "Open Flights")
        {
            getOpenFlights(); 

        }
        else
        {
            ActiveForm = frmLogout;
            lblLogout.Text = e.ListItem.Text;
            DateTime curTime = DateTime.Now;
            string curStr = curTime.ToShortTimeString();
            txVLogout.Text = "You logged out at " +curStr+"<br/>";
            txVLogout.Text += "Press Go to Login again.";
        }
    }



    protected void getOpenFlights()
    {
        iTXMLPathscl itpaths = new iTXMLPathscl();
        iTXMLcl iTXML = new iTXMLcl(itpaths.applicationBasePath);
        SessionManager sessMan = new SessionManager();
        ArrayList lstFlights = new ArrayList();
        lstFlights.Clear();
        lstFlights = iTXML.UserSessionReadElementsForArrayListFromCurrentFlightXMLFile(Session["sessionid"].ToString(), "//FLIGHTS/FLIGHT", "flights.xml");

        lstFlights = iTXML.ReorderFlightsEarliestOnTop(lstFlights);
        Session["FlightsList"] = null;

        int i = 0;
        lstOpenFlights.Items.Clear();


        if (lstFlights != null && lstFlights.Count != 0)
        {
            Hashtable ht = new Hashtable(lstFlights.Count);
            ht.Clear();
            // lstOpenFlights.ItemCount = lstFlights.Count;
            foreach (Flight flts in lstFlights)
            {
                ht.Add(i, flts);
                string lstItem = flts.DISPLAYOPENFLIGHTS;

                MobileListItem mb = new MobileListItem();
                if (flts.etdStatus)
                {
                    mb.ForeColor = Color.SeaGreen;
                    mb.Text = lstItem + "*";

                }
                else
                {

                    mb.Text = lstItem;

                }

                lstOpenFlights.Items.Add(mb);

                i++;
            }

            ActiveForm = frmOpenFlights;
            lstOpenFlights.Visible = true;
            lblOpenFlights.Text = "Open Flights";
            Session["FlightsList"] = ht;
        }
        else
        {
            lblAlertMsg.Visible = true;
            lblAlertMsg.Text = "There are currently no departure flights assigned to your Staff ID";
        }

    
    
    
    }
    protected void lstOpenFlights_ItemCommand(object sender, ListCommandEventArgs e)
    {

        try
        {
            int selIndex = e.ListItem.Index;
            Flight flt = ((Flight)((Hashtable)(Session["FlightsList"]))[selIndex]);



            Session["SelectedFlight"] = flt.DISPLAYFLIGHTSMENU;
            Session["FlightURNO"] = flt.URNO;
            Session["FlightNO"] = flt.FLNO;

            ActiveForm = frmOpenFlightsMenu;

            iTrekSessionMgr.SessionManager sessMan = new iTrekSessionMgr.SessionManager();
            iTrekXML.iTXMLPathscl iTPaths = new iTrekXML.iTXMLPathscl();
            lblStaffNo.Text = "Staff: " + sessMan.GetNoOfStaffAssignedToFlight(Session["FlightURNO"].ToString(), iTPaths.GetJobsFilePath());
            txtVFlightDetailsOpenFlightMenu.Text = Session["SelectedFlight"].ToString();
        }
        catch (Exception arrExcep)
        {


            ActiveForm = frmError;
            lblErrorMsg.Text = "Timeout error occured. Please try again";
        
        }
    }
    protected void cmdLogin_Click(object sender, EventArgs e)
    {
        iTXMLcl iTXML = new iTXMLcl();
        SessionManager sessMan = new SessionManager();
        try
        {
            if (Session["sessionid"].Equals(null))
            {
                RedirectToMobilePage("Login.aspx");
            }
            else
            {
                //TODO: Re-check how this is used
                //sessMan.IsValidLoginTime(Convert.ToDateTime(sessMan.GetUserLoginTime(Session["sessionid"].ToString())));

                //iTXML.DeleteDeviceIDEntryBySessID(sessID);
                string PENO = Session["staffid"].ToString();
                iTXML.DeleteDeviceIDEntryByStaffId(PENO);
                Session.Clear();
                Session.Abandon();

                RedirectToMobilePage("Login.aspx");
            }
        }
        catch (Exception logExc)
        {
            ActiveForm = frmError;
            lblErrorMsg.Text = "Timeout error occured. Please try again";
        
        }
    }
    protected void cmdBack1_Click(object sender, EventArgs e)
    {
        getOpenFlights();
        //ActiveForm = frmOpenFlights;
    }
    protected void lstSelFlights_ItemCommand(object sender, ListCommandEventArgs e)
    {
        iTXMLPathscl itpaths = new iTXMLPathscl();
        iTXMLcl itxml = new iTXMLcl(itpaths.applicationBasePath);
        SessionManager sessMan = new SessionManager();
        if (e.ListItem.Text.Trim() == "ATD")
        {
            lstTime.Items.Clear();
            ActiveForm = frmTiming;

            txtVFlightDetailsTiming.Text = Session["SelectedFlight"].ToString();
            lblSelectedOption.Text = "ATD";
            
            DateTime currentTime = DateTime.Now;
            lstTime.Items.Add("NOW " + currentTime.ToString("HH:mm"));
            for (double d = -1.0; d >= -9.0; d--)
            {
                lstTime.Items.Add(currentTime.AddMinutes(d).ToString("HH:mm"));
            }



        }
        else if (e.ListItem.Text == "ALL OK2LOAD")
        {
            //TODO;Remove?
            ActiveForm = frmOK2Load;
            lblOk2Load.Text = e.ListItem.Text;
            txtVFlightDetailsOK2L.Text = Session["SelectedFlight"].ToString();

            //Have to go through GetAllOk2LoadAL (not GetAllOk2LoadALFromSessionDir) to
            //implement delay timing
            ArrayList lstOK2Loads = sessMan.GetAllOk2LoadAL(Session["FlightNO"].ToString(), Session["FlightURNO"].ToString(), Session["sessionid"].ToString());

            lstAllOK2Load.Visible = true;
            lstAllOK2Load.DataSource = lstOK2Loads;
            lstAllOK2Load.DataBind();
            ActiveForm = frmOK2Load;
            lblOk2Load.Text = e.ListItem.Text;
        }
        else //if (selLstMainMenu.Selection.Value == "DLS")
        {

            //ArrayList lstShowDLS = sessMan.GetShowDLS(Session["FlightNO"].ToString(), Session["FlightURNO"].ToString(), Session["sessionid"].ToString());
            string DLS = sessMan.GetShowDLS(Session["FlightNO"].ToString(), Session["FlightURNO"].ToString(), Session["sessionid"].ToString());
            //if (lstShowDLS != null && lstShowDLS.Count > 0)
            if (DLS != "")
            {
                //string[] strArr = (string[])lstShowDLS.ToArray((typeof(string)));
                txtVShowDLS.Visible = true;
                //txtVShowDLS.Text = strArr[0].ToString();
                txtVShowDLS.Text = DLS;
                //txtVShowDLS.DataBind();
                ActiveForm = frmDLS;
                lblDLS.Text = e.ListItem.Text;
                txtVFlightDetailsDLS.Text = Session["SelectedFlight"].ToString();
            }
            else
            {
                ActiveForm = frmDLS;
                txtVFlightDetailsDLS.Text = Session["SelectedFlight"].ToString();
                lblDLS.Text = "No DLS data available for this flight";
            }
        }

    }


    protected void cmdTime_Click(object sender, EventArgs e)
    {
        iTXMLPathscl iTPaths = new iTXMLPathscl();
        iTXMLcl iTXML = new iTXMLcl();
        DateTime dtselectedTime = DateTime.Now;
        bool dtStatus = true;
        bool prevDayStatus = false;


        try
        {
            selectedTime = txtBxGetTime.Text.Substring(0, 2) + ":" + txtBxGetTime.Text.Substring(2, 2);
        }
        catch (ArgumentOutOfRangeException exc)
        {

            lblTimingAlertMsg.Visible = true;
            lblTimingAlertMsg.Text = "Invalid Time.Please enter time between 0000 - 2359";
            dtStatus = false;
        }
        try
        {
            dtselectedTime = Convert.ToDateTime(selectedTime);
        }
        catch (FormatException ex)
        {
            lblTimingAlertMsg.Visible = true;
            lblTimingAlertMsg.Text = "Invalid Time.Please enter time between 0000 - 2359";
            dtStatus = false;
        }

        if(dtStatus)
        {
            DateTime curTime = DateTime.Now;
            if (dtselectedTime > curTime)
            {
                if (checkTimeforMidNight(dtselectedTime, curTime))
                {
                    prevDayStatus = true;
                    
                }
                else 
                {
                    lblTimingAlertMsg.Visible = true;
                    lblTimingAlertMsg.Text = "Time entered must be earlier than current time - please re-enter";
                    dtStatus=false;
                }
            }
       }



                if (lblSelectedOption.Text == "ATD" && dtStatus)
                {

                    string ATDTimeInOracleFormat = iTUtils.convertStandardDateTimeToOracleFormat(iTXML.ProcessSubmittedTime(selectedTime, prevDayStatus));
                    string FlightURNO = Session["FlightURNO"].ToString();
                    string StaffID = Session["StaffID"].ToString();
                    //iTWMQ.PutATDTimingOnFROMITREKUPDATEQueue(FlightURNO, lblSelectedOption.Text, selectedTime, StaffID);
                    
#if UsingQueues
                   ITrekWMQClass1 iTWMQ = new ITrekWMQClass1();
                   //line below temporarily removed until business decision
                   //regarding iTrek updates to ATD has been made
                   //iTWMQ.PutATDTimingOnFROMITREKUPDATEQueue(FlightURNO, lblSelectedOption.Text, ATDTimeInOracleFormat, StaffID,iTPaths.GetFROMITREKUPDATEQueueName());             
#endif


                    //HACK: Improve
                    //uses message format to delete the flight
                    //from the session and the main files
                    XmlDocument xmlDoc = new XmlDocument();
                    //string xmlDocFileName = iTPaths.GetXMLPath() + Convert.ToString(Session["sessionid"]) + @"\flights.xml";
                    //string xmlDocFileName = iTPaths.GetXMLPath() + Convert.ToString(Session["sessionid"].ToString()) + @"\flights.xml";
                    string sessID = Session["sessionid"].ToString();
                    string xmlDocFileName = iTPaths.GetXMLPath() + sessID + @"\flights.xml";

                    //TODO: iTPaths.GetFlightsFilePath()?
                    //string xmlDoc2FileName = iTPaths.GetXMLPath() + @"\Flights\Flights.xml";
                    xmlDoc.Load(xmlDocFileName);
                    //DeleteFlightFromXMLFile works with a string message
                    //string message = @"<FLIGHT><URNO>" + Convert.ToString(Session["FlightURNO"]) + "</URNO><ACTION>DELETE</ACTION></FLIGHT>";
                    string flightURNO = Session["FlightURNO"].ToString();
                    string message = @"<FLIGHT><URNO>" + flightURNO + "</URNO><ACTION>DELETE</ACTION></FLIGHT>";
                    xmlDoc = iTXML.DeleteFlightFromXMLFile(xmlDoc, message);
                    xmlDoc.Save(xmlDocFileName);

                    //do same for Main/updateable flights.xml files
                    XmlDocument MainandUpdateableFlightsXML = new XmlDocument();
                    //xmlDoc2.Load(xmlDoc2FileName);
                    MainandUpdateableFlightsXML.Load(iTPaths.GetUpdateableFlightsFilePath());
                    //iTXML.DeleteFlightFromXMLFile(MainandUpdateableFlightsXML, message);
                    //xmlDoc2.Save(xmlDoc2FileName);
                    MainandUpdateableFlightsXML = iTXML.DeleteFlightFromXMLFile(MainandUpdateableFlightsXML, message);
                    //additional code to handle change for UpdateableFlights
                    MainandUpdateableFlightsXML.Save(iTPaths.GetUpdateableFlightsFilePath());
                    //MainandUpdateableFlightsXML.Save(iTPaths.GetFlightsFilePath());
                    //unable to use xmldocument to save to main flights file
                    //so use FileInfo instead
                    FileInfo fiFlightsUpdateable = new FileInfo(iTPaths.GetUpdateableFlightsFilePath());
                    FileInfo fiFlightsMain = new FileInfo(iTPaths.GetFlightsFilePath());
                    if (fiFlightsMain.Exists && fiFlightsUpdateable.Exists)
                        fiFlightsUpdateable.CopyTo(iTPaths.GetFlightsFilePath(), true);
                    updateFlightList();
                    ActiveForm = frmOpenFlights;
                }
            }

        
   

    

    protected void cmdBackfrmTime_Click(object sender, EventArgs e)
    {
        ActiveForm = frmOpenFlightsMenu;
    }
    protected void cmdBackFromDLS_Click(object sender, EventArgs e)
    {
        ActiveForm = frmOpenFlightsMenu;
    }

    protected void cmdBackFromOK2L_Click(object sender, EventArgs e)
    {
        ActiveForm = frmOpenFlightsMenu;
    }
    protected void lstTime_ItemCommand(object sender, ListCommandEventArgs e)
    {
        iTXMLPathscl iTPaths = new iTXMLPathscl();
        iTXMLcl iTXML = new iTXMLcl();
        DateTime dtselectedTime;
        bool prevDayStatus = false;

        //process the time input box if there's anything in it
        // if (txtBxGetTime.Text == "")
        selectedTime = e.ListItem.Text;

        /*  else
          {
              selectedTime = txtBxGetTime.Text + ":" + txtBxGetMin.Text;

          }*/

        selectedTime = selectedTime.Replace("NOW ", "");

        dtselectedTime = Convert.ToDateTime(selectedTime);

         DateTime curTime = DateTime.Now;
        if (dtselectedTime > curTime)

        {
            if (checkTimeforMidNight(dtselectedTime, curTime))
            {
                prevDayStatus = true;
            }
           

        }

            if (lblSelectedOption.Text == "ATD")
            {
                //iTWMQ.PutATDTimingOnFROMITREKUPDATEQueue(Convert.ToString(Session["FlightURNO"]), lblSelectedOption.Text, selectedTime, Convert.ToString(Session["StaffID"]));
                string FlightURNO = Session["FlightURNO"].ToString();
                string StaffID = Session["StaffID"].ToString();
                //int selectedTimeHour = Convert.ToInt32(selectedTime.Remove(2));
                //int selectedTimeMinute = Convert.ToInt32(selectedTime.Substring(3));
                //DateTime dtATDSubmit = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, selectedTimeHour, selectedTimeMinute, 00);
                //string ATDTimeInOracleFormat = iTUtils.convertStandardDateTimeToOracleFormat(dtATDSubmit.ToString());
                string ATDTimeInOracleFormat = iTUtils.convertStandardDateTimeToOracleFormat(iTXML.ProcessSubmittedTime(selectedTime, prevDayStatus));
                //iTWMQ.PutATDTimingOnFROMITREKUPDATEQueue(FlightURNO, lblSelectedOption.Text, selectedTime, StaffID);
#if UsingQueues
                ITrekWMQClass1 iTWMQ = new ITrekWMQClass1();
                    //iTWMQ.PutATDTimingOnFROMITREKUPDATEQueue(Convert.ToString(Session["FlightURNO"]), lblSelectedOption.Text, selectedTime, Convert.ToString(Session["StaffID"]),iTPaths.GetFROMITREKUPDATEQueueName());
                   iTWMQ.PutATDTimingOnFROMITREKUPDATEQueue(FlightURNO, lblSelectedOption.Text, ATDTimeInOracleFormat, StaffID,iTPaths.GetFROMITREKUPDATEQueueName());
                   // ITrekWMQClass1.PutATDTimingOnFROMITREKUPDATEQueue(FlightURNO, lblSelectedOption.Text, ATDTimeInOracleFormat, StaffID, iTPaths.GetFROMITREKUPDATEQueueName());
#endif


                //HACK: Improve
                //uses message format to delete the flight
                //from the session and the main files
                XmlDocument xmlDoc = new XmlDocument();
                //string xmlDocFileName = iTPaths.GetXMLPath() + Convert.ToString(Session["sessionid"]) + @"\flights.xml";
                //string xmlDocFileName = iTPaths.GetXMLPath() + Convert.ToString(Session["sessionid"].ToString()) + @"\flights.xml";
                string sessID = Session["sessionid"].ToString();
                string xmlDocFileName = iTPaths.GetXMLPath() + sessID + @"\flights.xml";
                XmlDocument xmlDoc2 = new XmlDocument();
                //TODO: iTPaths.GetFlightsFilePath()?
                string xmlDoc2FileName = iTPaths.GetXMLPath() + @"\Flights\Flights.xml";
                xmlDoc.Load(xmlDocFileName);
                //DeleteFlightFromXMLFile works with a string message
                //string message = @"<FLIGHT><URNO>" + Convert.ToString(Session["FlightURNO"]) + "</URNO><ACTION>DELETE</ACTION></FLIGHT>";
                //string message = @"<FLIGHT><URNO>" + Convert.ToString(Session["FlightURNO"].ToString()) + "</URNO><ACTION>DELETE</ACTION></FLIGHT>";

                string message = @"<FLIGHT><URNO>" + FlightURNO + "</URNO><ACTION>DELETE</ACTION></FLIGHT>";
                xmlDoc = iTXML.DeleteFlightFromXMLFile(xmlDoc, message);
                xmlDoc.Save(xmlDocFileName);

                //xmlDoc2.Load(xmlDoc2FileName);
                //iTXML.DeleteFlightFromXMLFile(xmlDoc2, message);
                //xmlDoc2.Save(xmlDoc2FileName);

                //do same for Main/updateable flights.xml files
                XmlDocument MainandUpdateableFlightsXML = new XmlDocument();
                //xmlDoc2.Load(xmlDoc2FileName);
                MainandUpdateableFlightsXML.Load(iTPaths.GetUpdateableFlightsFilePath());
                //iTXML.DeleteFlightFromXMLFile(MainandUpdateableFlightsXML, message);
                //xmlDoc2.Save(xmlDoc2FileName);
                MainandUpdateableFlightsXML = iTXML.DeleteFlightFromXMLFile(MainandUpdateableFlightsXML, message);
                //additional code to handle change for UpdateableFlights
                MainandUpdateableFlightsXML.Save(iTPaths.GetUpdateableFlightsFilePath());
                //MainandUpdateableFlightsXML.Save(iTPaths.GetFlightsFilePath());
                //unable to use xmldocument to save to main flights file
                //so use FileInfo instead
                FileInfo fiFlightsUpdateable = new FileInfo(iTPaths.GetUpdateableFlightsFilePath());
                FileInfo fiFlightsMain = new FileInfo(iTPaths.GetFlightsFilePath());
                if (fiFlightsMain.Exists && fiFlightsUpdateable.Exists)
                    fiFlightsUpdateable.CopyTo(iTPaths.GetFlightsFilePath(), true);

                updateFlightList();
                ActiveForm = frmOpenFlights;
            }
        }
    
    protected void cmdBack_Click(object sender, EventArgs e)
    {
        ActiveForm = frmMenu;
    }

    private void updateFlightList()
    {
        iTXMLPathscl itpaths = new iTXMLPathscl();
        iTXMLcl iTXML = new iTXMLcl(itpaths.applicationBasePath);
        SessionManager sessMan = new SessionManager();
        ArrayList lstFlights = iTXML.UserSessionReadElementsForArrayListFromCurrentFlightXMLFile(Session["sessionid"].ToString(), "//FLIGHTS/FLIGHT", "flights.xml");

        lstFlights = iTXML.ReorderFlightsEarliestOnTop(lstFlights);
        Session["FlightsList"] = null;

        int i = 0;
        lstOpenFlights.Items.Clear();


        if (lstFlights != null && lstFlights.Count != 0)
        {
            Hashtable ht = new Hashtable(lstFlights.Count);
            ht.Clear();
            // lstOpenFlights.ItemCount = lstFlights.Count;
            foreach (Flight flts in lstFlights)
            {
                ht.Add(i, flts);
                string lstItem = flts.DISPLAYOPENFLIGHTS;
                lstOpenFlights.Items.Add(lstItem);
                i++;
            }

            ActiveForm = frmOpenFlights;
            lstOpenFlights.Visible = true;
            lblOpenFlights.Text = "Open Flights";
            Session["FlightsList"] = ht;

        }
    }

    private bool checkTimeforMidNight(DateTime selTime, DateTime curTime)
    {
        if (curTime.Hour == 0 && selTime.Hour == 23)
        {
            return true;
        }
        return false;
    }

    protected override void OnViewStateExpire(EventArgs e)
    {
        //IEnumerator en=Session.Keys.GetEnumerator();
        //string test = "";
        //while (en.MoveNext())
        //{ 
        
        //test+=en.Current.ToString();
        
        //}
        
        ActiveForm = frmError;
        lblErrorMsg.Text = "Timeout error occured. Please try again";
    }




    protected void cmdError_Click(object sender, EventArgs e)
    {
        
            RedirectToMobilePage("Login.aspx");
        
    }
}
