package wodspush;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;
import com.openwave.wappush.*;
import java.net.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003 Fujitsu Asia Pte Ltd</p>
 * <p>Company: Fujitsu Asia Pte Ltd</p>
 * @author not attributable
 * @version 1.0
 */
public class PushServ
	extends HttpServlet {
	private static final String CONTENT_TYPE = "text/vnd.wap.wml";
	private static final String DOC_TYPE =
		"<!DOCTYPE wml PUBLIC \"-//WAPFORUM//DTD WML 1.2//EN\"\n" +
		"  \"http://www.wapforum.org/DTD/wml12.dtd\">";

//	String ppgString = "http://edevgate.openwave.com:9001/pap";
//	String ppgIPaddr = "devgate2.openwave.com";
//	String ppgIPaddr = "204.163.167.193";
	String ppgIPaddr = "203.126.249.148";
	String ppgString = "http://"+ppgIPaddr+":9002/pap";

	//Initialize global variables
	public void init() throws ServletException {
	}

	//Process the HTTP Get request
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws
		ServletException, IOException {
		response.setContentType(CONTENT_TYPE);
		PrintWriter out = response.getWriter();
		out.println("<?xml version=\"1.0\"?>");
		out.println(DOC_TYPE);
		out.println("<wml>");
		out.println("<card>");
		out.println("<p>The servlet has received a " + request.getMethod() +
					". This is the reply.</p>");
		out.println("</card></wml>");

		String alertURL   = request.getParameter("alertURL");
		String alertText  = request.getParameter("alertText");
		String clientAddr = request.getParameter("clientAddr");

		// === for TESTING only ========================================
//		alertURL = "http://203.125.208.138:8080/IPCentrexWeb/login.jsp";
		alertURL = "http://203.125.208.138:8080/WodsWAP/login.jsp";
		alertText = "Your Order is confirmed: 181818 ... Orange:10 Banana:20 ToiletPaper:100 Breakfast:99 XO:77 NewWater:10 Coke:98";
//		clientAddr =
//			"1058523817-44998_devgate2.uplanet.com/TYPE=USER@www.openwave.com"; // subscriber ID
		clientAddr = "108089_WAPGW.grid.net.sg/TYPE=USER@www.openwave.com"; // subscriber ID
//		clientAddr = "1081034_WAPGW.grid.net.sg/TYPE=USER@www.openwave.com"; // subscriber ID		// === for TESTING only ========================================

//		String subID = request.getHeader("X-Up-Subno");

		// Testing pushServiceLoading
//		System.out.println("Calling pushServiceLoading");
//		pushServiceLoading(clientAddr, alertURL);

		// Testing pushServiceIndication
		System.out.println("Calling pushServiceIndication");
		pushServiceIndication(clientAddr,alertText,alertURL);

		/*
		  // Testing pushCustomSL
		  System.out.println("Calling pushCustomSL");
		  String message="<?xml version=\"1.0\"?> "+DOC_TYPE;
		  message+="<wml><card>";
		  message+="<p>The servlet has received a Get. This is the reply.</p>";
		  message+="</card></wml>";
		  pushCustomSL(clientAddress, message);
		 */
	}

	//Process the HTTP Post request
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws
		ServletException, IOException {
		doGet(request, response);
	}

	//Clean up resources
	public void destroy() {
	}

	private void pushCustomSL(String clientAddress, String message) {
		String pushID = "wijayam@sg.fujitsu.com";
		URL ppgURL = null;
		try {
			ppgURL = new URL(ppgString);
			Pusher ppg = new Pusher(ppgURL);
			ppg.initialize();
			CustomContent cc = new CustomContent();
			cc.setContentData(message, CONTENT_TYPE); //CONTENT_TYPE="text/vnd.wap.wml"
            //Instantiate the push message object.
			PushMessage pushMessage = new PushMessage(pushID, clientAddress);
            //Instantiate a MimeEntity object and add the PushMessage and
            //CustomContent objects.
			MimeEntity me = new MimeEntity();
			me.addEntity(pushMessage);
			me.addEntity(cc);
            //Send the push message contained in the MimeEntity.
			PushResponse pushResponse = (PushResponse) ppg.send(me);
            //Read some information from the response.
			System.out.println("reply-Time = " +
							   pushResponse.getReplyTime());
			System.out.println("response-result-code = " +
							   pushResponse.getResultCode());
			System.out.println("response-result-desc = " +
							   pushResponse.getResultDesc());
		}
		catch (MalformedURLException e) {
			System.out.println("Exiting - Malformed URL");
			System.out.println(e.getMessage());
		}
		catch (Exception e) {
			System.out.println("Exiting - Exception -- cancellation ");
			System.out.println("Reason: " + e.getMessage());
			//myLog.close();
			// ITS 269413 -- remove System commands to ensure that the servlet does not exit tomcat.
			//System.err.println(e.toString());
			//System.exit(-1);
		}
	}

	private synchronized void pushServiceLoading(String clientAddress, String alertURL) {
		URL ppgURL = null;
		System.out.println("pushServiceLoading::Creating URL: " + ppgString);
		try {
			ppgURL = new URL(ppgString);
		}
		catch (MalformedURLException e) {
			System.out.println("Exiting - Malformed URL");
			System.out.println(e.getMessage());
			return;
		}
		System.out.println("Creating Simple Push");
		SimplePush sp = new SimplePush(ppgURL, "WODS", "/Openwave Push");
		System.out.println("setQualityOfService");
		sp.setQualityOfService(null, DeliveryMethod.confirmed, null, false, null, false);
		PushResponse pushResponse = null;
		System.out.println("AlertURL: " + alertURL);
		try {
			System.out.println("clientAddress: " + clientAddress);
			pushResponse = sp.pushServiceLoading(new String[] {clientAddress}
												 ,
												 alertURL,
												 ServiceLoadingAction.
												 executeHigh);
			System.out.println(
				"After pushServiceLoading. Executing printResults (QueryStatus)");
			printResults(pushResponse);
		}
		catch (WapPushException e) {
			System.out.println("Exiting - WapPushException -- cancellation");
			System.out.println("Reason: " + e.getMessage());
			//myLog.close();
			//System.err.println(e.toString());
			//System.exit(-1);
		}
		catch (IOException e) {
			System.out.println("Exiting - IO Exception -- cancellation ");
			System.out.println("Reason: " + e.getMessage());
		}
	} // end of pushServiceLoading

	private synchronized void pushServiceIndication(String clientAddress,
		String alertText,
		String alertURL) {

		URL ppgURL = null;
		System.out.println("pushServiceIndication::Creating URL: " + ppgString);
		try {
			ppgURL = new URL(ppgString);
		}
		catch (MalformedURLException e) {
			System.out.println("Exiting - Malformed URL");
			System.out.println(e.getMessage());
			return;
		}
		System.out.println("Creating Simple Push");
//		SimplePush sp = new SimplePush(ppgURL, "WODS", "/wods");
		SimplePush sp = new SimplePush(ppgURL, "WODS", "/Openwave Push");
//		System.out.println("setQualityOfService");
//		sp.setQualityOfService(null, DeliveryMethod.NOTSPECIFIED, null, false, null, false);
		PushResponse pushResponse = null;
		System.out.println("AlertURL: " + alertURL);
		try {
			System.out.println("clientAddress: " + clientAddress);
			pushResponse = sp.pushServiceIndication(new String[] {clientAddress},
										 alertText, alertURL,
										 ServiceIndicationAction.signalHigh);
			System.out.println(
				"After pushServiceIndication. Executing printResults (QueryStatus)");
			printResults(pushResponse);
		}
		catch (WapPushException e) {
			System.out.println("Exiting - WapPushException -- cancellation");
			System.out.println("Reason: " + e.getMessage());
			//myLog.close();
			//System.err.println(e.toString());
			//System.exit(-1);
		}
		catch (IOException e) {
			System.out.println("Exiting - IO Exception -- cancellation ");
			System.out.println("Reason: " + e.getMessage());
		}
	} // end of pushServiceIndication

	private synchronized void printResults(PushResponse pushResponse) throws
		WapPushException, MalformedURLException, IOException {
//Read the response to find out if the Push Submission succeded.
//1001 = "Accepted for processing"
		if (pushResponse.getResultCode() == 1001) {
			try {
				String pushID = pushResponse.getPushID();
				SimplePush sp = new SimplePush(new URL(ppgString), "WODS", "/wods");
				StatusQueryResponse queryResponse =
					sp.queryStatus(pushID, null);
				StatusQueryResult queryResult =
					queryResponse.getResult(0);
				System.out.println("Message status: " +
								   queryResult.getMessageState());
			}
			catch (WapPushException exception) {
				System.out.println("*** ERROR - WapPushException (" +
								   exception.getMessage() + ")");
			}
			catch (MalformedURLException exception) {
				System.out.println("*** ERROR - MalformedURLException ("
								   + exception.getMessage() + ")");
			}
			catch (IOException exception) {
				System.out.println("*** ERROR - IOException ("
								   + exception.getMessage() + ")");
			}
		}
		else {
			System.out.println("push-id: " + pushResponse.getPushID());
			System.out.println("Message failed: " + pushResponse.getResultDesc());
		}
	} //end of printResults
}
