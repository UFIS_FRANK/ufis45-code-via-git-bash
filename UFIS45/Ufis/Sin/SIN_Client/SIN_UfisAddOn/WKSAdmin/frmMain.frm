VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{A2F31E92-C74F-11D3-A251-00500437F607}#1.0#0"; "UfisCom.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{7BDE1363-DF9A-4D96-A2A0-4E85E0191F0F}#1.0#0"; "AatLogin.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Begin VB.Form frmMain 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Telexpool-Workstation Administration"
   ClientHeight    =   11025
   ClientLeft      =   150
   ClientTop       =   435
   ClientWidth     =   13725
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   11025
   ScaleWidth      =   13725
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Tag             =   "s"
   Visible         =   0   'False
   Begin TabDlg.SSTab SSTab1 
      Height          =   10545
      Left            =   90
      TabIndex        =   3
      Top             =   120
      Width           =   13545
      _ExtentX        =   23892
      _ExtentY        =   18600
      _Version        =   393216
      Tab             =   2
      TabHeight       =   520
      TabCaption(0)   =   "Configuration"
      TabPicture(0)   =   "frmMain.frx":030A
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "cmdSave"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "cmdExit(0)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "AATLoginControl1"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).ControlCount=   3
      TabCaption(1)   =   "Manual Input"
      TabPicture(1)   =   "frmMain.frx":0326
      Tab(1).ControlEnabled=   0   'False
      Tab(1).ControlCount=   0
      TabCaption(2)   =   "Workstations"
      TabPicture(2)   =   "frmMain.frx":0342
      Tab(2).ControlEnabled=   -1  'True
      Tab(2).Control(0)=   "Frame5"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).Control(1)=   "Frame6"
      Tab(2).Control(1).Enabled=   0   'False
      Tab(2).Control(2)=   "cmdQuit"
      Tab(2).Control(2).Enabled=   0   'False
      Tab(2).Control(3)=   "cmdExcel"
      Tab(2).Control(3).Enabled=   0   'False
      Tab(2).Control(4)=   "dlgSave"
      Tab(2).Control(4).Enabled=   0   'False
      Tab(2).Control(5)=   "txtSaveFile"
      Tab(2).Control(5).Enabled=   0   'False
      Tab(2).Control(6)=   "cmdSaveFile"
      Tab(2).Control(6).Enabled=   0   'False
      Tab(2).ControlCount=   7
      Begin VB.CommandButton cmdSaveFile 
         Caption         =   "..."
         Height          =   255
         Left            =   8745
         TabIndex        =   31
         Top             =   10005
         Width           =   255
      End
      Begin VB.TextBox txtSaveFile 
         Enabled         =   0   'False
         Height          =   285
         Left            =   1515
         TabIndex        =   30
         Top             =   9990
         Width           =   7140
      End
      Begin MSComDlg.CommonDialog dlgSave 
         Left            =   9120
         Top             =   9720
         _ExtentX        =   847
         _ExtentY        =   847
         _Version        =   393216
      End
      Begin VB.CommandButton cmdExcel 
         Caption         =   "Export to Excel"
         Enabled         =   0   'False
         Height          =   375
         Left            =   120
         TabIndex        =   22
         Top             =   9945
         Width           =   1335
      End
      Begin VB.CommandButton cmdQuit 
         Caption         =   "Quit"
         Height          =   375
         Left            =   11760
         TabIndex        =   7
         Top             =   9945
         Width           =   1335
      End
      Begin VB.Frame Frame6 
         Caption         =   "Workstation Settings"
         Height          =   2775
         Left            =   120
         TabIndex        =   6
         Top             =   7065
         Width           =   13215
         Begin VB.CommandButton cmdCancel 
            Caption         =   "&Cancel"
            Enabled         =   0   'False
            Height          =   375
            Left            =   11640
            TabIndex        =   25
            Top             =   480
            Width           =   1335
         End
         Begin VB.TextBox txtTADR 
            BackColor       =   &H0000FFFF&
            Height          =   645
            Left            =   1440
            MaxLength       =   255
            TabIndex        =   20
            Top             =   1680
            Width           =   9975
         End
         Begin VB.TextBox txtREMA 
            BackColor       =   &H00FFFFFF&
            Height          =   645
            Left            =   5520
            MaxLength       =   250
            TabIndex        =   19
            Top             =   840
            Width           =   5895
         End
         Begin VB.TextBox txtLOCT 
            BackColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   1440
            MaxLength       =   50
            TabIndex        =   17
            Top             =   840
            Width           =   2895
         End
         Begin VB.TextBox txtDEPT 
            BackColor       =   &H00FFFFFF&
            Height          =   285
            Left            =   1440
            MaxLength       =   50
            TabIndex        =   16
            Top             =   1200
            Width           =   2895
         End
         Begin VB.CommandButton cmdNew 
            Caption         =   "&New"
            Height          =   375
            Left            =   11640
            TabIndex        =   12
            Top             =   1920
            Width           =   1335
         End
         Begin VB.CommandButton cmdSaveWKST 
            Caption         =   "&Save"
            Enabled         =   0   'False
            Height          =   375
            Left            =   11640
            TabIndex        =   11
            Top             =   1440
            Width           =   1335
         End
         Begin VB.CommandButton cmdDelete 
            Caption         =   "&Delete"
            Enabled         =   0   'False
            Height          =   375
            Left            =   11640
            TabIndex        =   10
            Top             =   945
            Width           =   1335
         End
         Begin VB.TextBox txtWKST 
            BackColor       =   &H0000FFFF&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1440
            MaxLength       =   32
            TabIndex        =   9
            Top             =   480
            Width           =   2895
         End
         Begin VB.Label Label27 
            Caption         =   "Addresses"
            Height          =   255
            Left            =   360
            TabIndex        =   21
            Top             =   1680
            Width           =   1215
         End
         Begin VB.Label Label26 
            Caption         =   "Remark"
            Height          =   255
            Left            =   4680
            TabIndex        =   18
            Top             =   840
            Width           =   1215
         End
         Begin VB.Label Label25 
            Caption         =   "Location"
            Height          =   255
            Left            =   360
            TabIndex        =   15
            Top             =   840
            Width           =   1215
         End
         Begin VB.Label Label24 
            Caption         =   "Department"
            Height          =   255
            Left            =   360
            TabIndex        =   14
            Top             =   1200
            Width           =   1215
         End
         Begin VB.Label Label20 
            Caption         =   "Workstation"
            Height          =   255
            Left            =   360
            TabIndex        =   13
            Top             =   480
            Width           =   1215
         End
      End
      Begin AATLOGINLib.AatLogin AATLoginControl1 
         Height          =   1095
         Left            =   -74625
         TabIndex        =   4
         Top             =   390
         Visible         =   0   'False
         Width           =   1575
         _Version        =   65536
         _ExtentX        =   2778
         _ExtentY        =   1931
         _StockProps     =   0
      End
      Begin VB.CommandButton cmdExit 
         Caption         =   "Exit"
         Height          =   405
         Index           =   0
         Left            =   -69795
         TabIndex        =   2
         Top             =   10545
         Width           =   1440
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "Save"
         Height          =   405
         Left            =   -71415
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   10545
         Width           =   1440
      End
      Begin VB.Frame Frame5 
         Caption         =   "List of workstations"
         Height          =   6495
         Left            =   120
         TabIndex        =   5
         Top             =   480
         Width           =   13215
         Begin VB.CommandButton cmdResetFilter 
            Caption         =   "&Reset Filter"
            Height          =   375
            Left            =   5400
            TabIndex        =   29
            Top             =   6000
            Width           =   1335
         End
         Begin VB.CommandButton cmdFilter 
            Caption         =   "&Apply Filter"
            Height          =   375
            Left            =   3960
            TabIndex        =   27
            Top             =   6000
            Width           =   1335
         End
         Begin VB.TextBox txtFilter 
            Height          =   285
            Left            =   1440
            TabIndex        =   26
            Top             =   6000
            Width           =   2415
         End
         Begin VB.Frame frmRecordset 
            BorderStyle     =   0  'None
            Height          =   735
            Left            =   1200
            TabIndex        =   23
            Top             =   2760
            Visible         =   0   'False
            Width           =   10815
            Begin VB.Label Label28 
               BackColor       =   &H00FFFFFF&
               BackStyle       =   0  'Transparent
               Caption         =   "Record changed."
               Height          =   255
               Left            =   4680
               TabIndex        =   24
               Top             =   240
               Width           =   1695
            End
         End
         Begin TABLib.TAB tab_WKSTAB 
            Height          =   5535
            Left            =   105
            TabIndex        =   8
            Tag             =   "{=TABLE=}WKSTAB{=FIELDS=}WKST,TADR,DEPT,LOCT,REMA,URNO,CDAT,USEC,LSTU,USEU"
            Top             =   360
            Width           =   12885
            _Version        =   65536
            _ExtentX        =   22728
            _ExtentY        =   9763
            _StockProps     =   64
         End
         Begin VB.Label Label29 
            Caption         =   "Workstation Filter"
            Height          =   255
            Left            =   120
            TabIndex        =   28
            Top             =   6000
            Width           =   1935
         End
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   300
      Left            =   0
      TabIndex        =   0
      Top             =   10725
      Width           =   13725
      _ExtentX        =   24209
      _ExtentY        =   529
      SimpleText      =   "Ready."
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   19024
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            TextSave        =   "29-Mar-06"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   2
            TextSave        =   "3:59 PM"
         EndProperty
      EndProperty
   End
   Begin UFISCOMLib.UfisCom UfisCom1 
      Left            =   0
      Top             =   0
      _Version        =   65536
      _ExtentX        =   1111
      _ExtentY        =   1164
      _StockProps     =   0
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'#######################################################################
'# Main form
'# WKSADMIN TOOL
'#  based on HASConfigTool Sources
'#
'#  adapted by SHA 20060324
'#
'#######################################################################


Option Explicit

Private gSaved As Boolean
Private gNewRecord As Boolean
Private gOrgWKST As String
Private gOrgDEPT As String
Private gOrgREMA As String
Private gOrgLOCT As String
Private gOrgTADR As String
Private gOrgURNO As String
Private gFilter As String
Private gExcelFile As String




'#######################################################################
'### Cancel changes to recordset and undo
'#######################################################################
Private Sub cmdCancel_Click()
    
    txtWKST.Text = gOrgWKST
    txtTADR.Text = gOrgTADR
    txtDEPT.Text = gOrgDEPT
    txtLOCT.Text = gOrgLOCT
    txtREMA.Text = gOrgREMA
            
    gSaved = True
    cmdSaveWKST.Enabled = False
    tab_WKSTAB.Enabled = True
    frmRecordset.Visible = False
    cmdCancel.Enabled = False
    cmdNew.Enabled = True
    
End Sub

'#######################################################################
'###
'#######################################################################
Private Sub cmdDelete_Click()

    Dim strWhere As String
    Dim strTemp As String

    strTemp = "Delete the record for workstation " & gOrgWKST & ". Continue?"
    Dim ilRet As Integer
    ilRet = MsgBox(strTemp, vbYesNo, "Delete Record")
    If ilRet = vbNo Then
        Exit Sub
    End If

    strWhere = "WHERE URNO='" & gOrgURNO & "'"
    
    If frmMain.UfisCom1.CallServer("DRT", "WKSTAB", "", "", strWhere, "360") <> 0 Then
        MsgBox frmMain.UfisCom1.LastErrorMessage, vbCritical
        Exit Sub
    End If
    
    tab_WKSTAB.ResetContent
    frmData.LoadData tab_WKSTAB, ""

End Sub

'#######################################################################
'###
'#######################################################################
Private Sub cmdExcel_Click()

    Dim l As Integer
    Dim llLineCount As Integer
    Dim strTemp As String
    Dim FileNumber
    
    l = tab_WKSTAB.GetLineCount
    
    If l < 1 Then
        MsgBox "No Data to export.", vbExclamation
        Exit Sub
    End If
    
    If gFilter <> "" Then MsgBox "Export with active filter.", vbInformation
    
    
    FileNumber = FreeFile   ' Get unused file number.
    Open gExcelFile For Output As #FileNumber   ' Create file name.
    
    
    llLineCount = tab_WKSTAB.GetLineCount

    Print #FileNumber, "Workstation,Department,Location,Adresses,Remarks"

    For l = 0 To llLineCount Step 1
        strTemp = tab_WKSTAB.GetColumnValues(l, 0) & "," & tab_WKSTAB.GetColumnValues(l, 2) & "," & tab_WKSTAB.GetColumnValues(l, 3) & "," & tab_WKSTAB.GetColumnValues(l, 1) & "," & tab_WKSTAB.GetColumnValues(l, 4)
        Print #FileNumber, strTemp
    Next l
    
    Close #FileNumber
    
    strTemp = "Data exported to " & gExcelFile
    MsgBox strTemp, vbInformation
    
    
End Sub

'#######################################################################
'###
'#######################################################################
Private Sub cmdFilter_Click()
    
    Dim strWhere As String
    
    txtFilter.Text = UCase(txtFilter.Text)
    
    gFilter = "WHERE WKST LIKE '%" & txtFilter.Text & "%'"
    
    MousePointer = vbHourglass
    tab_WKSTAB.ResetContent
    frmData.LoadData tab_WKSTAB, gFilter
    tab_WKSTAB.RedrawTab
    MousePointer = vbDefault
    
End Sub

'#######################################################################
'### PREPARE FOR NEW RECORD
'#######################################################################
Private Sub cmdNew_Click()
    
    txtTADR.Text = ""
    txtWKST.Text = ""
    txtDEPT.Text = ""
    txtLOCT.Text = ""
    txtREMA.Text = ""
    
    gOrgWKST = txtWKST.Text
    gOrgTADR = txtTADR.Text
    gOrgDEPT = txtDEPT.Text
    gOrgLOCT = txtLOCT.Text
    gOrgREMA = txtREMA.Text
    
    gNewRecord = True
    
End Sub

'#######################################################################
'### QUIT APPLICATION
'#######################################################################
Private Sub cmdQuit_Click()
    If gSaved = False Then
        Dim ilRet As Integer
        ilRet = MsgBox("Changes not saved. Quit?", vbYesNo)
        If ilRet = vbNo Then
            Exit Sub
        End If
    End If
    
    Unload frmData
    End

End Sub

'#######################################################################
'### RESET FILTER CONDITION
'#######################################################################

Private Sub cmdResetFilter_Click()
   
    MousePointer = vbHourglass
    gFilter = ""
    tab_WKSTAB.ResetContent
    frmData.LoadData tab_WKSTAB, gFilter
    MousePointer = vbDefault
    tab_WKSTAB.RedrawTab
    
End Sub

'#######################################################################
'### SELECT FILENAME FOR EXCEL EXPORT
'#######################################################################
Private Sub cmdSaveFile_Click()
        
    dlgSave.DialogTitle = "Save as Excel"
    dlgSave.Filter = "Excel (CSV)|*.csv"
    dlgSave.ShowSave
    
    If dlgSave.FileName <> "" Then
        txtSaveFile.Text = dlgSave.FileName ' + ".csv"
        gExcelFile = txtSaveFile.Text
        cmdExcel.Enabled = True
    End If
End Sub

'#######################################################################
'### UPDATE OR INSERT RECORD
'#######################################################################

Private Sub cmdSaveWKST_Click()

    Dim strWhere As String
    Dim strTemp As String
    Dim strCMD As String
    Dim strData As String
    Dim strFields As String
    
    strFields = "WKST,TADR,DEPT,LOCT,REMA"
    
    '### REPLACE , WITH | FOR CEDA DB
    strTemp = Replace(txtTADR.Text, ",", "|")
    strData = txtWKST.Text & "," & strTemp & "," & txtDEPT.Text & "," & txtLOCT.Text & "," & txtREMA.Text

    '### CHECK MANDATORY FIELDS
    'If Trim(txtWKST.Text) = "" Or Trim(txtTADR.Text) = "" Then
    If Trim(txtWKST.Text) = "" Then
        'MsgBox "Workstation and/or Telexaddresses not entered.", vbCritical
        MsgBox "Workstation not entered.", vbCritical
        Exit Sub
    End If

    If gNewRecord = True Then
    
        If frmMain.UfisCom1.CallServer("IRT", "WKSTAB", strFields, strData, "", "360") <> 0 Then
            MsgBox frmMain.UfisCom1.LastErrorMessage, vbCritical
            Exit Sub
        End If
    
    Else
        strWhere = "WHERE URNO='" & gOrgURNO & "'"
        If frmMain.UfisCom1.CallServer("URT", "WKSTAB", strFields, strData, strWhere, "360") <> 0 Then
            MsgBox frmMain.UfisCom1.LastErrorMessage, vbCritical
            Exit Sub
        End If
    End If

    cmdCancel_Click
    cmdFilter_Click

    cmdSaveWKST.Enabled = False
    
End Sub

'#######################################################################
'###
'#######################################################################

Private Sub Form_Load()
    
    '### INITIAL FILTER: LOAD ALL
    gFilter = ""
    
    SSTab1.TabVisible(1) = False
    SSTab1.TabVisible(0) = False

End Sub

'#######################################################################
'###
'#######################################################################

Public Sub RefreshView()
    
    
    frmData.InitTabGeneral tab_WKSTAB
    frmData.InitTabForCedaConnection tab_WKSTAB
    tab_WKSTAB.AutoSizeColumns
    
    tab_WKSTAB.HeaderLengthString = "150,300,150,150,150,0,0,0,0,0"
    tab_WKSTAB.HeaderString = "Workstation,Telex Addresses,Department,Location,Remark,"","","","","")"
    
    'frmData.LoadData tab_WKSTAB, ""
    frmData.LoadData tab_WKSTAB, gFilter
    
    gSaved = True
    gNewRecord = True
    
    
End Sub

'#######################################################################
'###
'#######################################################################

Private Sub Form_Unload(Cancel As Integer)
    Unload frmData
End Sub



'#######################################################################
'###
'#######################################################################

Private Sub tab_WKSTAB_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim strkey1 As String
    Dim strWhere As String
    Dim strTemp As String
    Dim strFields As String
    Dim strValues As String
    Dim l As Long
    Dim llLineCount As Long
    Dim INlist As String
    
    l = tab_WKSTAB.GetCurrentSelected
    
    If gSaved = True Then
        If l <> -1 Then
        
            MousePointer = vbHourglass
            
            txtWKST.Text = tab_WKSTAB.GetColumnValues(l, 0)
            gOrgWKST = txtWKST.Text
            
            strTemp = Replace(tab_WKSTAB.GetColumnValues(l, 1), "|", ",")
            txtTADR.Text = strTemp
            gOrgTADR = txtTADR.Text
            
            txtDEPT.Text = tab_WKSTAB.GetColumnValues(l, 2)
            gOrgDEPT = txtDEPT.Text
            
            txtLOCT.Text = tab_WKSTAB.GetColumnValues(l, 3)
            gOrgLOCT = txtLOCT.Text
            
            txtREMA.Text = tab_WKSTAB.GetColumnValues(l, 4)
            gOrgREMA = txtREMA.Text
            
            gOrgURNO = tab_WKSTAB.GetColumnValues(l, 5)
           
            gNewRecord = False
            
            cmdDelete.Enabled = True
    
            MousePointer = vbDefault
    
        End If
    Else
        MsgBox "Changes not saved.", vbCritical
    End If
    
    If LineNo = -1 Then
        tab_WKSTAB.Sort ColNo, True, False
        tab_WKSTAB.Refresh
    End If
    
End Sub

'#######################################################################
'###
'#######################################################################

Sub CheckChanges()

    If ((txtWKST.Text = gOrgWKST) _
        And (Trim(txtTADR.Text) = Trim(gOrgTADR)) _
        And (Trim(txtDEPT.Text) = Trim(gOrgDEPT)) _
        And (Trim(txtLOCT.Text) = Trim(gOrgLOCT)) _
        And (Trim(txtREMA.Text) = Trim(gOrgREMA))) Then
        
        '### NO CHANGES
        gSaved = True
        cmdSaveWKST.Enabled = False
        tab_WKSTAB.Enabled = True
        frmRecordset.Visible = False
        cmdCancel.Enabled = False
        cmdNew.Enabled = True
        
    Else
        '###CHANGED, NEED TO SAVE
        gSaved = False
        cmdSaveWKST.Enabled = True
        tab_WKSTAB.Enabled = False
        frmRecordset.Visible = True
        cmdCancel.Enabled = True
        cmdNew.Enabled = False
    End If

End Sub


Private Sub txtDEPT_KeyPress(KeyAscii As Integer)
    cmdSaveWKST.Enabled = True
    cmdCancel.Enabled = True
End Sub

'#######################################################################
'###
'#######################################################################

Private Sub txtDEPT_LostFocus()
    CheckChanges
End Sub

'#######################################################################
'###
'#######################################################################

Private Sub txtLOCT_KeyPress(KeyAscii As Integer)
    cmdSaveWKST.Enabled = True
    cmdCancel.Enabled = True
End Sub

'#######################################################################
'###
'#######################################################################

Private Sub txtLOCT_LostFocus()
    CheckChanges
End Sub

'#######################################################################
'###
'#######################################################################

Private Sub txtREMA_KeyPress(KeyAscii As Integer)
    cmdSaveWKST.Enabled = True
    cmdCancel.Enabled = True
End Sub

'#######################################################################
'###
'#######################################################################

Private Sub txtREMA_LostFocus()
    CheckChanges
End Sub

'#######################################################################
'###
'#######################################################################

Private Sub txtTADR_KeyPress(KeyAscii As Integer)
    cmdSaveWKST.Enabled = True
    cmdCancel.Enabled = True
End Sub

'#######################################################################
'###
'#######################################################################

Private Sub txtTADR_LostFocus()
    CheckChanges
End Sub

'#######################################################################
'###
'#######################################################################

Private Sub txtWKST_KeyPress(KeyAscii As Integer)
    cmdSaveWKST.Enabled = True
    cmdCancel.Enabled = True
End Sub

'#######################################################################
'###
'#######################################################################

Private Sub txtWKST_LostFocus()
    txtWKST.Text = UCase(txtWKST.Text)
    
    CheckChanges
End Sub
