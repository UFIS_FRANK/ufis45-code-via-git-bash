#define LoggingOn
#define UsingQueues
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
//using IBM.WMQ;
using System.Timers;
using System.Configuration;
using iTrekWMQ;
using System.IO;
using iTrekXML;
using System.Xml;
using iTrekSessionMgr;
using iTrekUtils;
using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DB;
using UFIS.ITrekLib.Util;
using System.Collections;
using UFIS.ITrekLib.CommMsg;
using IBM.WMQ;


namespace iTrekJobMgrSvc
{
	/// <summary>
	/// Gets job xml batch and update data from queue and writes it to 
	/// job xml file on disk
	/// Also used to capture any changes within N minutes from the time of Dep or Arrival
	/// to send an alert to the AO for change of Job.
	/// 
	/// According to timing set in JOB_AFT_SERVICESTIMER
	/// </summary>

	public partial class iTrekJobMgrSvc : ServiceBase
	{
		MQQueue queue;
		ITrekWMQClass1 wmq1;
		iTXMLPathscl iTPaths;
		private static string _queueName = null;
		public iTrekJobMgrSvc()
		{
			InitializeComponent();
			iTPaths = new iTXMLPathscl();
		}

		protected override void OnStart(string[] args)
		{
			// TODO: Add code here to start your service.
			MessageQueueTimer();
		}

		protected override void OnStop()
		{
			// TODO: Add code here to perform any tear-down necessary to stop your service.
			if (wmq1 != null)
			{
				wmq1.closeQueueConnection(queue, wmq1.getQManager);

			}
		}

		private static int cnt = 0;

		public void MessageQueueTimer()
		{
#if LoggingOn
			cnt++;
			if (cnt > 100)
			{
				LogMsg("In MessageQueueTimer");
				cnt = 0;
			}
#endif
			System.Timers.Timer aTimer = new System.Timers.Timer();
			aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
			// Interval set in JOB_AFT_SERVICESTIMER
			aTimer.Interval = Convert.ToInt32(iTPaths.GetJOB_AFT_SERVICESTIMER());
			aTimer.Enabled = true;
		}

		private static bool _IsProcessing = false;
		private static object _lockObj = new object();
		private void OnTimedEvent(object source, ElapsedEventArgs e)
		{
			if (!_IsProcessing)
			{
				lock (_lockObj)
				{
					if (!_IsProcessing)
					{
						try
						{
							_IsProcessing = true;

							DoProcess();
						}
						catch (Exception ex)
						{
							LogMsg("OnTimedEvent:Err:" + ex.Message);
						}
						finally
						{
							_IsProcessing = false;
						}
					}
				}
			}
		}


		private void DoProcess()
		{
			try
			{
				for (int i = 0; i < 200; i++)
				{
					string message = "";
					byte[] messageId = new byte[100];
					message = GetMessageFromQueue(out messageId);
					if (message == "MQRC_NO_MSG_AVAILABLE" || message == "")
					{
						System.Threading.Thread.Sleep(100);
						continue;
					}
					else
					{
						if (DoProcessAMsg(message))
						{
							wmq1.DeleteMessages(queue, messageId);
						}
					}
				}
			}
			catch (Exception ex)
			{
				LogMsg("DoProcess:Err:" + ex.Message);
			}
		}

		private string QueueName
		{
			get
			{
				if ((_queueName == null) || (_queueName == ""))
				{
					try
					{
						_queueName = iTPaths.GetTOITREKREAD_JOBQueueName();
					}
					catch (Exception ex)
					{
						LogMsg("QueueName:Err:" + ex.Message);
					}
				}
				return _queueName;
			}
		}

		private string GetMessageFromQueue(out byte[] messageId)
		{
			if (iTPaths == null)
				iTPaths = new iTXMLPathscl();

			messageId = new byte[100];
			string message = "";
			messageId = new byte[100];
			try
			{
				if (wmq1 == null)
				{
					wmq1 = new ITrekWMQClass1();

					//  queue = wmq1.getQueueConnection(iTPaths.GetTOITREKREAD_JOBQueueName(), MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
					queue = wmq1.getQueueConnection(QueueName, MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING + MQC.MQOO_BROWSE);
					message = wmq1.BrowseFirstMessageFromTheQueueForOpenConnection(queue, out messageId);
					//iTJobMgrLog.WriteEntry("message open connection" + message);
					//iTJobMgrLog.WriteEntry("messageID open connection" + Convert.ToBase64String(messageId));
					// wmq1.closeQueueConnectionForServices(queue, wmq1.getQManager, iTPaths.GetMQStatus());
				}
				else
				{
					//queue = wmq1.getQueueConnectionForServices(iTPaths.GetTOITREKREAD_JOBQueueName(), MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING, iTPaths.GetMQStatus());
					message = wmq1.BrowseFirstMessageFromTheQueueForOpenConnection(queue, out messageId);
					//iTJobMgrLog.WriteEntry("message else connection" + message);
					//iTJobMgrLog.WriteEntry("messageID else connection" + Convert.ToBase64String(messageId));
					// wmq1.closeQueueConnectionForServices(queue, wmq1.getQManager, iTPaths.GetMQStatus());
				}
			}
			catch (MQException mEx)
			{
				if (mEx.Message == "MQRC_NO_MSG_AVAILABLE")
				{
					return "";
				}				
				else if (mEx.ReasonCode == 2033)
				{
					return mEx.Message;
				}
				else
				{
					LogMsg("GetMessageFromQueue:MqErr:" + mEx.Message);
					if (wmq1 != null)
					{
						try
						{
							wmq1.closeQueueConnection(queue, wmq1.getQManager);
						}
						catch (Exception)
						{
							wmq1 = null;
						}
						wmq1 = null;
					}
				}
			}
			catch (Exception ex)
			{
				LogMsg("GetMessageFromQueue:Err:" + ex.Message);
				if (wmq1 != null)
				{
					try
					{
						wmq1.closeQueueConnection(queue, wmq1.getQManager);
					}
					catch (Exception)
					{
						wmq1 = null;
					}
					wmq1 = null;
				}
			}

			return message;
		}

		private bool DoProcessAMsg(string message)
		{
			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			IDbCommand cmd = null;
			string replyFrom = "";
			string urno = "";
			try
			{
				cmd = db.GetCommand(true);
				if (message.Length > 4000)
				{
					urno = CMsgIn.NewBatchMessageToProcess(cmd, QueueName, message, "Jsvc", "JOB", out replyFrom);
				}
				else
				{
					urno = CMsgIn.NewMessageToProcess(cmd, QueueName, message, "Jsvc", "JOB", out replyFrom);
				}
				commit = true;
			}
			catch (Exception ex)
			{
				LogTraceMsg("DoProcessAMsg:Err:" + ex.Message + Environment.NewLine + ex.Source);	
				throw;
			}
			finally
			{
				db.CloseCommand(cmd, commit);
			}
			return commit;
		}

		private void LogTraceMsg(string msg)
		{
			try
			{
				if (msg != null)
				{
					int len = msg.Length;
					if (len > 200) len = 200;
					UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("iTrekJobMgrSvc", msg.Substring(0, len));
				}
			}
			catch (Exception ex)
			{
				LogMsg("LogTraceMsg:Err:" + ex.Message);
			}
		}

		private void LogMsg(string msg)
		{
			iTJobMgrLog.WriteEntry(msg);
		}
	}
}