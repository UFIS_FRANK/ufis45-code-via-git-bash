#define UsingQueues
#define LoggingOn
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using IBM.WMQ;
using System.Timers;
using System.Configuration;
using iTrekWMQ;
using System.IO;
using iTrekXML;
using System.Xml;
using iTrekSessionMgr;
using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DB;
using UFIS.ITrekLib.Util;
using UFIS.ITrekLib.CommMsg;

namespace iTrekFlightMgrSvc
{
	/// <summary>
	/// Gets flight xml batch and update data from queue and writes it to 
	/// flight xml file on disk
	/// Also when a change for GTD1,GTA1,PSTA,PSTD,ETDI,ETOA occurs within n minute 
	/// which is configured in Path.xml, an alert will be send to AO who is 
	/// in duty of this flight. 
	/// According to timing set in JOB_AFT_SERVICESTIMER
	/// </summary>

	public partial class iTrekFlightMgrSvc : ServiceBase
	{
		//iTXMLcl iTXML;
		iTXMLPathscl iTPaths;
		int UpdateFlushCounter = 0;

		MQQueue queue;

		ITrekWMQClass1 wmq1;
		private static Object _lockObj = new Object();//to use in synchronisation of single access
		private static bool _IsProcessing = false;
		XmlDocument UpdateableFlightsXml;
		public iTrekFlightMgrSvc()
		{
			InitializeComponent();

			//iTFlightMgrLog.WriteEntry("Initialisation Flight Mgr");
			//iTXML = new iTXMLcl();
			iTPaths = new iTXMLPathscl();
		}

		protected override void OnStart(string[] args)
		{
			MessageQueueTimer();
		}

		protected override void OnStop()
		{
			if (wmq1 != null)
			{
				wmq1.closeQueueConnection(queue, wmq1.getQManager);
			}
		}

		public void MessageQueueTimer()
		{
			System.Timers.Timer aTimer = new System.Timers.Timer();
			aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
			aTimer.Interval = Convert.ToInt32(iTPaths.GetJOB_AFT_SERVICESTIMER());
			aTimer.Enabled = true;
		}

		// Specify what you want to happen when the Elapsed event is raised.
		private void OnTimedEvent(object source, ElapsedEventArgs e)
		{
			if (iTPaths == null)
				iTPaths = new iTXMLPathscl();

			if (!_IsProcessing)
			{
				lock (_lockObj)
				{
					if (!_IsProcessing)
					{
						try
						{
							_IsProcessing = true;
							DoProcess();
						}
						catch (Exception ex)
						{
							LogMsg(ex.Message);
						}
						finally
						{
							_IsProcessing = false;
						}
					}
				}
			}
		}


		private void LogTraceMsg(string msg)
		{
			try
			{
				if (msg != null)
				{
					int len = msg.Length;
					if (len > 200) len = 200;
					UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("iTrekFlightMgrSvc", msg.Substring(0, len));
				}
			}
			catch (Exception ex)
			{
				iTFlightMgrLog.WriteEntry(ex.Message);
			}
		}

		private void DoProcess()
		{
			try
			{
				for (int i = 0; i < 400; i++)
				{
					string message = "";

					byte[] messageId = new byte[100];
					message = GetMsgFromMQ(out messageId);

					if (message == "MQRC_NO_MSG_AVAILABLE" || message == "")
					{
						System.Threading.Thread.Sleep(100);
						continue;
						//break;//go out from the FOR Loop
					}
					else
					{
						try
						{
							if (DoProcessAMsg(message))
							{
								wmq1.DeleteMessages(queue, messageId);
							}
						}
						catch (Exception ex)
						{
							LogMsg("DoProcess:Err:" + ex.Message);
						}
					}
				}
			}
			catch (Exception ex)
			{
				LogMsg("DoProcess:Err1:" + ex.Message);
			}
		}

		private string _qName = "";


		/* Browse the messages from MQ*/
		private string GetMsgFromMQ(out byte[] messageId)
		{
			string message = "";
			messageId = new byte[100];
			try
			{
				if (wmq1 == null)
				{
					wmq1 = new ITrekWMQClass1();
					_qName = iTPaths.GetTOITREKREAD_AFTQueueName();
					queue = wmq1.getQueueConnection(iTPaths.GetTOITREKREAD_AFTQueueName(), MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING + MQC.MQOO_BROWSE);
					message = wmq1.BrowseFirstMessageFromTheQueueForOpenConnection(queue, out messageId);
					//queue = wmq1.getQueueConnection(iTPaths.GetTOITREKREAD_AFTQueueName(), MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
					//message = wmq1.GetMessageFromTheQueueForOpenConnection(queue);
				}
				else
				{
					message = wmq1.BrowseFirstMessageFromTheQueueForOpenConnection(queue, out messageId);
				}
			}
			catch (MQException mEx)
			{
				if (mEx.ReasonCode == 2033)
				{
					return mEx.Message;
				}
				else
				{
					iTFlightMgrLog.WriteEntry("MQ ReasonCode" + mEx.ReasonCode);
					iTFlightMgrLog.WriteEntry("MQ CompletionCode" + mEx.CompletionCode);
					iTFlightMgrLog.WriteEntry("MQ Message" + mEx.Message);
					if (wmq1 != null)
					{
						try
						{
							wmq1.closeQueueConnection(queue, wmq1.getQManager);
						}
						catch (Exception ex)
						{
							iTFlightMgrLog.WriteEntry("MQ Excep Message" + ex.Message);
							wmq1 = null;
						}
						wmq1 = null;
					}
				}
			}
			catch (Exception ex)
			{
#if LoggingOn
				iTFlightMgrLog.WriteEntry("Setup Queues:" + ex.Message);
#endif
				if (wmq1 != null)
				{
					try
					{
						wmq1.closeQueueConnection(queue, wmq1.getQManager);
					}
					catch (Exception ex1)
					{
						iTFlightMgrLog.WriteEntry("Excep Message" + ex1.Message);
						wmq1 = null;
					}
					wmq1 = null;

				}
			}

			return message;
		}

		private bool DoProcessAMsg(string message)
		{
			bool commit = false;

			if (message != "")
			{
				if (message != "MQRC_NO_MSG_AVAILABLE")
				{
					UtilDb db = UtilDb.GetInstance();

					IDbCommand cmd = null;
					string replyFrom = "";
					string urno = "";
					try
					{
						cmd = db.GetCommand(true);
						//LogTraceMsg("DoProcessAMsg" + message);
						//LogTraceMsg("DoProcessAMsg" + message.Length);
						if (message.Length > 4000)
						{
							urno = CMsgIn.NewBatchMessageToProcess(cmd, _qName, message, "Fsvc", "FLIGHT", out replyFrom);
						}
						else
						{
							urno = CMsgIn.NewMessageToProcess(cmd, _qName, message, "Fsvc", "FLIGHT", out replyFrom);
						}
						commit = true;
					}
					catch (Exception)
					{
						throw;
					}
					finally
					{
						db.CloseCommand(cmd, commit);
					}
				}
			}
			return commit;
		}


		private void LogMsg(string msg)
		{
			iTFlightMgrLog.WriteEntry(msg);
		}

	}
}
