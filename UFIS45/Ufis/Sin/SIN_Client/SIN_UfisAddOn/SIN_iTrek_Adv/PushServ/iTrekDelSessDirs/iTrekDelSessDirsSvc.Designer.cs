namespace iTrekDelSessDirsSvc
{
    partial class iTrekDelSessDirsSvc
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.iTrekDelSessDirsSvcLog = new System.Diagnostics.EventLog();
            ((System.ComponentModel.ISupportInitialize)(this.iTrekDelSessDirsSvcLog)).BeginInit();
            // 
            // iTrekDelSessDirsSvcLog
            // 
            this.iTrekDelSessDirsSvcLog.Log = "iTrekDelSessDirsSvcLog";
            this.iTrekDelSessDirsSvcLog.Source = "iTrekDelSessDirsSvcLogSrc";
            // 
            // iTrekDelSessDirsSvc
            // 
            this.ServiceName = "iTrekDelSessDirsSvc";
            ((System.ComponentModel.ISupportInitialize)(this.iTrekDelSessDirsSvcLog)).EndInit();

        }

        #endregion

        private System.Diagnostics.EventLog iTrekDelSessDirsSvcLog;
    }
}
