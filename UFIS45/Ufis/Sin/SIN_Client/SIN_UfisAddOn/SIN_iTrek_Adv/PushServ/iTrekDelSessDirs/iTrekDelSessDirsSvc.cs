#define LoggingOn
#define UsingQueues
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using System.Timers;
using System.Configuration;
using System.IO;
using iTrekXML;
using System.Xml;
using iTrekSessionMgr;
using UFIS.ITrekLib.Ctrl;
using iTrekWMQ;
using IBM.WMQ;

using MM.UtilLib;


namespace iTrekDelSessDirsSvc
{
	/// <summary>
	/// This service was added late in the project lifecycle and
	/// so the logic was not merely added to UpdateSessionsSvc (although
	/// it could relatively easily be if the number of services used
	/// by iTrek is a problem).
	/// This service scans the session directories and if they were last 
	/// written to later than the time set in Paths.xml 
	/// (normally about 1 hour) they will be deleted.
	/// Also for clearing the LOADALL.xml file which is entering the urno
	/// of flight to which a loadall alert is send. 
	/// In Second Phase this service is modified to do the 
	/// housekeeping jobs for iTrek.The downtime is 
	/// taken from Paths.XML and the housekeeping jobs will be done at that time.
	/// Only once a day.
	/// 
	/// 
	/// </summary>

	partial class iTrekDelSessDirsSvc : ServiceBase
	{
		iTXMLcl iTXML;
		iTXMLPathscl iTPaths;
		ITrekWMQClass1 wmq1;
		string msgId = "";
		int houseKeepDay = 0;
		int startDay = 0;
		int backUpHour;
		DateTime traceMin;

		public iTrekDelSessDirsSvc()
		{
			InitializeComponent();
			LogMsg("Init DelSvc");
			iTXML = new iTXMLcl();
			iTPaths = new iTXMLPathscl();
			startDay = 0;
			backUpHour = 0;
			traceMin = DateTime.Now;
		}

		protected override void OnStart(string[] args)
		{
			LogMsg("In OnStart");
			MessageQueueTimer();
		}

		protected override void OnStop()
		{
			LogMsg("In onStop.");
		}

		public void MessageQueueTimer()
		{
			LogMsg("In MessageQueueTimer: GetSERVICESTIMER " + iTPaths.GetSERVICESTIMER());
			System.Timers.Timer aTimer = new System.Timers.Timer();
			aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
			// Set the Interval by SERVICESTIMER
			//aTimer.Interval = Convert.ToInt32(iTPaths.GetSERVICESTIMER());
			aTimer.Interval = Convert.ToInt32(iTPaths.GetDELSESSDIRSERVICETIMER());

			aTimer.Enabled = true;
		}


		private void RestartSvc(iTXMLPathscl itPaths)
		{
			DateTime curDT = DateTime.Now;
			// FileInfo fiFlightsSumm = new FileInfo(iTPaths.GetSummFlightsFilePath());
			// FileInfo fiJobsSumm = new FileInfo(iTPaths.GetJobsSummFilePath());
			string logDir = iTPaths.GetLogDirPath().Trim();
			DateTime dt = System.DateTime.Now;
			string fName = logDir + string.Format(@"Job_{0:MMdd}.txt", dt);
			FileInfo fiJobsTrace = new FileInfo(fName);
			//   if (fiFlightsSumm.LastWriteTime.AddMinutes(3).CompareTo(curDT) <= 0 && fiJobsSumm.LastWriteTime.AddMinutes(3).CompareTo(curDT) <= 0 && fiJobsTrace.LastWriteTime.AddMinutes(3).CompareTo(curDT) <= 0)
			if (fiJobsTrace.LastWriteTime.AddMinutes(3).CompareTo(curDT) <= 0)
			{
				try
				{
					System.Diagnostics.Process proc = new System.Diagnostics.Process();
					proc.EnableRaisingEvents = false;

					proc.StartInfo.FileName = iTPaths.GetLogDirPath() + "SVCREFRESH.BAT";
					proc.StartInfo.Arguments = "iTrekJobMgrSvc";
					proc.Start();
					LogTraceMsg("Called the BatchFile");
				}
				catch (Exception ex)
				{
					LogMsg("JobSvcRef:Err:" + ex.Message);
				}
			}
		}

		private void CleanUpSessDir(iTXMLPathscl itPaths)
		{
			try
			{
				//get all the current directories in XML directory
				DirectoryInfo di = new DirectoryInfo(iTPaths.GetSessionXMLPath());
				DirectoryInfo[] diArr = di.GetDirectories();
				//filter out the currently active directories
				//inc  Jobs and Flights
				//TODO: Put time val in paths.xml
				DateTime dtCurrentTimeMinusOneHour = DateTime.Now.AddHours(Convert.ToDouble(iTPaths.GetDELSESSDIRTIMER()));

				foreach (DirectoryInfo dir in diArr)
				{
					if (dir.LastAccessTime < dtCurrentTimeMinusOneHour)
					{
						//remove the remaining directories
						dir.Delete(true);
					}
				}
				LogTraceMsg("deleting directory");
			}
			catch (Exception ex)
			{
				LogMsg("CleanUpSessDir:Err:" + ex.Message);
			}
		}

		private void CleanUpLoadAll(iTXMLPathscl itPaths)
		{
			/* Added by Alphy on 06 Dec 2006 to implement deleting flight nodes in LoadAll.xml after 1 day*/
			try
			{
				string loadallxmlPath = iTPaths.GetXMLPath() + @"\LOADALL.XML";
				XmlDocument myXmlDocument = iTXML.DeleteFlightFromLoadAllXMLFile(loadallxmlPath);
				myXmlDocument.Save(loadallxmlPath);
				LogTraceMsg("CleanUpLoadAll");
			}
			catch (Exception genEx)
			{
				LogMsg("CleanUpLoadAll:Err:" + genEx.Message);
			}

		}

		private const String SVC_NAMES_TO_STOP_ON_HK = "iTrekFlightMgrSvc,iTrekJobMgrSvc";

		private void StopSvcs()
		{
			string[] svcNamesToStop = SVC_NAMES_TO_STOP_ON_HK.Split(',');
			foreach (string svcName in svcNamesToStop)
			{
				try
				{
					UtilSvc.StopASvc(svcName);
				}
				catch (Exception ex)
				{
					LogMsg(svcName + " Stop:Err:" + ex.Message);
				}
			}
		}

		private void StartSvcs()
		{
			string[] svcNamesToStop = SVC_NAMES_TO_STOP_ON_HK.Split(',');
			foreach (string svcName in svcNamesToStop)
			{
				try
				{
					UtilSvc.StartASvc(svcName);
				}
				catch (Exception ex)
				{
					LogMsg(svcName + " Start:Err:" + ex.Message);
				}
			}
		}

		//private void DoHousekeeping()
		//{
		//    try
		//    {
		//        LogTraceMsg("HKStart:" + DateTime.Now.ToLongDateString());
		//        StopSvcs();
		//        System.Threading.Thread.Sleep(60000);//Sleep 1 Minutes to stop all the services
		//        string cntStatus = CtrlFlight.deleteOldFlights();
		//        string msgStatus = getPastMessagesForDuration();
		//        string taskStatus = RequestTasks();
		//        LogTraceMsg("HKFinish:" + DateTime.Now.ToLongDateString());
		//        LogTraceMsg("HKCnt:" + cntStatus);
		//        LogTraceMsg("msgStatus" + msgStatus);
		//        LogTraceMsg("taskStatus" + taskStatus);
		//    }
		//    catch (Exception houseKeepEx)
		//    {
		//        LogMsg("HK:Err:" + houseKeepEx.Message);
		//    }
		//    finally
		//    {
		//        StartSvcs();
		//    }
		//}

		private void SaveEventLog()
		{
			try
			{
				LogTraceMsg("Before getting Log");
				CtrlHK.GetInstance().GetITrekEventLog();
				LogTraceMsg("After getting Log");
			}
			catch (Exception hkExcep)
			{
				LogMsg("GetLog:Err:" + hkExcep.Message);
			}
		}

		private static bool _IsProcessing = false;
		private static object _lockObj = new object();
		private int _cntProc = 0;

		//private static object objLock = new object();

		// Specify what you want to happen when the Elapsed event is raised.
		private void OnTimedEvent(object source, ElapsedEventArgs e)
		{
			if (!_IsProcessing)
			{
				lock (_lockObj)
				{
					if (!_IsProcessing)
					{
						try
						{
							_IsProcessing = true;
							DoProcess();
						}
						catch (Exception ex)
						{
							LogMsg("Err:" + ex.Message);
						}
						finally
						{
							_IsProcessing = false;
						}
					}
				}
			}
		}

		private void DoProcess()
		{
			try
			{
				iTXMLPathscl iTPaths = new iTXMLPathscl();

				//try
				//{
				//RestartSvc(iTPaths);

				//}
				//catch (Exception ex)
				//{
				//    LogMsg("CleanUp:Err:" + ex.Message);
				//}

				int testDay = DateTime.Now.Day;
				int testHour = DateTime.Now.Hour;
				DateTime testMin = DateTime.Now;
				if (startDay == 0)
				{
					try
					{
						RequestTasks();
						startDay = testDay;
					}
					catch (Exception ex)
					{
						LogMsg("HK:Err:" + ex.Message);
					}
				}

				if ((DateTime.Now.Hour == Convert.ToInt32(iTPaths.GetDownTime()) && houseKeepDay != testDay))
				{
					LogTraceMsg("Time:" + DateTime.Now.ToShortDateString());
					houseKeepDay = testDay;

					try
					{
						RequestTasks();
						CleanUpSessDir(iTPaths);
						CleanUpLoadAll(iTPaths);
						SaveEventLog();
					}
					catch (Exception ex)
					{
						LogMsg("HK:Err:" + ex.Message);
					}
				}
				//if (backUpHour != testHour)
				//{
				//    LogTraceMsg("backup Time:" + DateTime.Now.ToShortDateString());
				//    backUpHour = DateTime.Now.Hour;

				//    try
				//    {
				//        CtrlHK.GetInstance().BackupData(CtrlHK.EnumBackupDataOption.Hourly, "");
				//    }
				//    catch (Exception ex)
				//    {
				//        LogTraceMsg("HK:Err: while backup Data" + ex.Message);
				//    }
				//}
				if ((traceMin.AddMinutes(2).CompareTo(testMin)) < 0)
				{
					LogTraceMsg("trace Time:" + DateTime.Now.ToShortDateString());
					traceMin = DateTime.Now;

					try
					{
						UFIS.ITrekLib.Util.ProcMonitor.GetInstance().RecordProcessesInfo();
					}
					catch (Exception ex)
					{
						LogTraceMsg("HK:Err: while writing proc trace Data" + ex.Message);
					}
				}
			}
			catch (Exception ex)
			{
				LogMsg("OnTime:Err:" + ex.Message);
			}
		}

		
	


		//        private string getPastMessagesForDuration()
		//        {
		//            string msgStatus = "";
		//#if UsingQueues

		//            try
		//            {
		//                wmq1 = new ITrekWMQClass1();
		//                msgId = wmq1.putShowMessageIntoFROMITREKSELECTQueue("msg", Convert.ToInt32(iTPaths.GetMessageDuration()), iTPaths.GetFROMITREKMSGQueueName());
		//                msgStatus = "MSG";
		//            }
		//            catch (Exception ex)
		//            {
		//                LogMsg("Exception while Service start" + ex.Message);
		//                msgStatus = "Error";

		//            }

		//            if (msgId == "success")
		//            {
		//                System.Threading.Thread.Sleep(Convert.ToInt32(iTPaths.GetMsgFromQueueDelaySetting()));

		//                try
		//                {
		//                    if (wmq1 != null)
		//                    {
		//                        msgId = wmq1.putShowMessageIntoFROMITREKSELECTQueue("msgto", Convert.ToInt32(iTPaths.GetMessageDuration()), iTPaths.GetFROMITREKMSGQueueName());
		//                    }
		//                    else
		//                    {
		//                        wmq1 = new ITrekWMQClass1();
		//                        msgId = wmq1.putShowMessageIntoFROMITREKSELECTQueue("msgto", Convert.ToInt32(iTPaths.GetMessageDuration()), iTPaths.GetFROMITREKMSGQueueName());
		//                    }
		//                }
		//                catch (Exception msgToEx)
		//                {

		//                    LogMsg("Exception while Service start for MsgTo" + msgToEx.Message);
		//                    msgStatus = "Error";
		//                }
		//                msgStatus = msgId + "MSGTO";
		//                msgId = "";
		//            }
		//#endif
		//            return msgStatus;

		//        }


		private string RequestTasks()
		{
			string msgId = "";
			try
			{
				string msg = "<HAG>APRON;BAGGAGE</HAG>";
				if (wmq1 != null)
				{
					msgId = wmq1.putShowMessageIntoFROMITREKSELECTQueueForTasks(msg, iTPaths.GetFROMITREKSELECTQueueName());
				}
				else
				{
					wmq1 = new ITrekWMQClass1();
					msgId = wmq1.putShowMessageIntoFROMITREKSELECTQueueForTasks(msg, iTPaths.GetFROMITREKSELECTQueueName());
				}
			}
			catch (Exception ex)
			{
				LogMsg("Exception while Service start" + ex.Message);
			}
			return msgId;
		}

		private void LogMsg(string msg)
		{
			iTrekDelSessDirsSvcLog.WriteEntry(msg);
		}

		private void LogTraceMsg(string msg)
		{
			iTrekDelSessDirsSvcLog.WriteEntry(msg);
		}
	}
}
