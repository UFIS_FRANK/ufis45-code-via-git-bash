using System;
using System.Collections.Generic;
using System.Text;

using IBM.WMQ;
using System.Collections;
//using iTrekUtils;
//using iTrekWMQ;

namespace MM.UtilLib
{

    public class MQFactory
    {
        private Dictionary<string, MQQueueManager> _dictQueManager = null;
        private List<MQQueueManager> _arrQueueManager = null;
        private MQFactory _this = null;
        private static object _lockThis = new object();

        private MQFactory()
        {
            _arrQueueManager = new List<MQQueueManager>();
            _dictQueManager = new Dictionary<string, MQQueueManager>();
        }

        public MQFactory GetInstance()
        {
            if (_this == null)
            {
                lock (_lockThis)
                {
                    if (_this == null)
                        _this = new MQFactory();
                }
            }
            return _this;
        }

        /// <summary>
        /// Get the Queue Manager with given data
        /// </summary>
        /// <param name="queName">QDMSD00</param>
        /// <param name="channelName">QDMSD00.CLIENT.ITREK</param>
        /// <param name="channelServerName">GREEN</param>
        /// <param name="portName">(1414)</param>
        /// <returns></returns>
        public MQQueueManager GetQueueManager(string queMgrName, string channelName,
            string channelServerName, string portName)
        {

            string id = GetQMgrHashCode(queMgrName, channelName, channelServerName, portName);

            MQQueueManager qMgr = null;

            if (_dictQueManager.ContainsKey(id))
            {
                qMgr = _dictQueManager[id];
            }
            else
            {
                qMgr = new MQQueueManager(queMgrName, channelName, channelServerName + portName);
            }
            return qMgr;
        }

        private string GetQMgrHashCode(string queMgrName, string channelName,
            string channelServerName, string portName)
        {
            long hashCode = queMgrName.GetHashCode() * 1000 +
                channelName.GetHashCode() * 100 +
                channelServerName.GetHashCode() * 10 +
                portName.GetHashCode();
            return string.Format("{0}|{1}|{2}|{3}|{4}",
                hashCode.ToString(),
                queMgrName,
                channelName,
                channelServerName,
                portName);
        }

        public void RemoveQueueManager(string queMgrName, string channelName,
            string channelServerName, string portName)
        {
            string id = GetQMgrHashCode(queMgrName, channelName, channelServerName, portName);

            if (_dictQueManager.ContainsKey(id))
            {
                _dictQueManager.Remove(id);
            }
        }

        public string GetMessageFromQueue(MQQueueManager qMgr, string qName)
        {
            MQQueue q = qMgr.AccessQueue(qName, MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
            string returnMessage = "";

            MQMessage queueMessage = new MQMessage();
            queueMessage.Format = MQC.MQFMT_STRING;
            //if (MessageID != null)
            //    queueMessage.MessageId = MessageID;

            MQGetMessageOptions queueGetMessageOptions = new MQGetMessageOptions();
            q.Get(queueMessage, queueGetMessageOptions);

            returnMessage = queueMessage.ReadString(queueMessage.MessageLength);  

            return returnMessage;
        }

        public string BrowseMessageFromQueue(MQQueueManager qMgr, string qName, byte[] correlationId)
        {
            MQQueue q = qMgr.AccessQueue(qName, MQC.MQGMO_SYNCPOINT + MQC.MQOO_FAIL_IF_QUIESCING);
            string returnMessage = "";

            MQMessage queueMessage = new MQMessage();
            queueMessage.Format = MQC.MQFMT_STRING;
            if (correlationId != null)
            {
                queueMessage.CorrelationId = correlationId;
            }
            //if (MessageID != null)
            //    queueMessage.MessageId = MessageID;

            MQGetMessageOptions queueGetMessageOptions = new MQGetMessageOptions();
            queueGetMessageOptions.Options = MQC.MQGMO_BROWSE_FIRST +
                MQC.MQGMO_FAIL_IF_QUIESCING +
                MQC.MQGMO_NO_WAIT;
            q.Get(queueMessage, queueGetMessageOptions);

            returnMessage = queueMessage.ReadString(queueMessage.MessageLength);
            
            return returnMessage;
        }

    }

    public class UtilMQ
    {
        private MQQueueManager _qManager;
        private MQQueue _q;
        private MQMessage _qMsg;

        private string _qManagerName;
        private string _channelName;
        private string _transportType;
        private string _connectionName;
        private string _stHashId;
        private string _qName;

        private MQQueueManager MyQMgr
        {
            get
            {
                if (_qManager == null)
                {
                    _qManager = new MQQueueManager(_qManagerName, _channelName, _connectionName);
                }
                return _qManager;
            }
        }

        private MQQueue MyQueue
        {
            get
            {
                if (_q == null) _q = MyQMgr.AccessQueue(_qName,
                      MQC.MQOO_BROWSE +
                      MQC.MQOO_FAIL_IF_QUIESCING +
                      MQC.MQOO_INPUT_AS_Q_DEF +
                      MQC.MQOO_OUTPUT
                  );
                return _q;
            }
        }

        public string GetMessage(byte[] correlationId)
        {
            string returnMessage = "";

            MQMessage queueMessage = new MQMessage();
            queueMessage.Format = MQC.MQFMT_STRING;
            queueMessage.CorrelationId = correlationId;
            //if (MessageID != null)
            //    queueMessage.MessageId = MessageID;

            MQGetMessageOptions queueGetMessageOptions = new MQGetMessageOptions();
            MyQueue.Get(queueMessage, queueGetMessageOptions);

            returnMessage = queueMessage.ReadString(queueMessage.MessageLength);
            return returnMessage;
        }

        public string GetMessageBrowse(byte[] correlationId)
        {
            string returnMessage = "";
            MQMessage queueMessage = new MQMessage();
            queueMessage.Format = MQC.MQFMT_STRING;
            queueMessage.CorrelationId = correlationId;
            //if (MessageID != null)
            //    queueMessage.MessageId = MessageID;

            MQGetMessageOptions queueGetMessageOptions = new MQGetMessageOptions();
            queueGetMessageOptions.Options = MQC.MQGMO_BROWSE_FIRST +
                MQC.MQGMO_FAIL_IF_QUIESCING +
                MQC.MQGMO_NO_WAIT;
            MyQueue.Get(queueMessage, queueGetMessageOptions);

            returnMessage = queueMessage.ReadString(queueMessage.MessageLength);
            return returnMessage;
        }

        public List<string> GetMessagesBrowse(int cntToFetchMsg, out int cntOfFetchedMsg)
        {
            List<string> arrMsg = new List<string>();
            cntOfFetchedMsg = 0;

            MQMessage qMsg = new MQMessage();
            qMsg.Format = MQC.MQFMT_STRING;
            MQGetMessageOptions queueGetMessageOptions = new MQGetMessageOptions();
            queueGetMessageOptions.Options = MQC.MQGMO_BROWSE_FIRST +
                MQC.MQGMO_FAIL_IF_QUIESCING +
                MQC.MQGMO_NO_WAIT;

            for (int i = 0; i < cntToFetchMsg; i++)
            {
                MyQueue.Get(qMsg, queueGetMessageOptions);
                string msg = qMsg.ReadString(qMsg.MessageLength);
                if (IsNoMsg(msg))
                {
                    break;
                }
                if (cntOfFetchedMsg == 0)
                {
                    queueGetMessageOptions.Options = MQC.MQGMO_BROWSE_NEXT +
                        MQC.MQGMO_FAIL_IF_QUIESCING +
                        MQC.MQGMO_NO_WAIT;
                }

                arrMsg.Add(msg);
            }

            cntOfFetchedMsg = arrMsg.Count;
            return arrMsg;
        }

        private bool IsNoMsg(string msg)
        {
            return msg == "MQRC_NO_MSG_AVAILABLE";
        }

        public void PurgeMessage(int cnt)
        {
            MQMessage qMsg = new MQMessage();
            qMsg.Format = MQC.MQFMT_STRING;
            for (int i = 0; i < cnt; i++)
                MyQueue.Get(qMsg);
        }

        public string PutMessage(string msg, string correlatedId)
        {
            string returnMessage = "";

            try
            {
                msg = removeBadCharacters(msg);
                int msgLen = msg.Length;

                if (msgLen == 0)
                    return "Add a message parameter";

                MQMessage qMsg = new MQMessage();
                qMsg.Format = MQC.MQFMT_STRING;
                //qMsg.CorrelationId = correlatedId;

                qMsg.WriteBytes(msg);
                MQPutMessageOptions queuePutMessageOptions = new MQPutMessageOptions();

                //putting the message into the queue
                MyQueue.Put(qMsg, queuePutMessageOptions);
                returnMessage = "success";
            }
            catch (Exception)
            {
            }

            return returnMessage;
        }

        private static string removeBadCharacters(string message)
        {
            if (message == null) message = "";
            else if (message != "")
            {
                message = message.Replace("'", "");
            }

            return message;
        }
    }


//    class UtilMQOld
//    {
//        private MQQueueManager queueManager;
//        private MQQueue queue;
//        private MQMessage queueMessage;
//        private MQPutMessageOptions queuePutMessageOptions;
//        private MQGetMessageOptions queueGetMessageOptions;

//        string QueueManagerName;

//        private string channelName;
//        private string transportType;
//        private string connectionName;

//        private string message = "";
//        private byte[] MessageID;

//        private static iTXMLPathscl iTPaths;
//        /*
//         * 
//         * Create a queue connection for the WMQ
//         * 
//         * 
//         * 
//         * */
//        private void createQueueConnection()
//        {
//#if LoggingOn
//           // iTrekUtils.iTUtils.LogToEventLog("Inside createQueueConnection: " );
//#endif

//            iTPaths = new iTXMLPathscl();
//            //TODO: Don't use queue name at this level
//            //can be instatiated twice and change relevant queue
//            //QueueName = queueNm;
//            QueueManagerName = iTPaths.GetQueueManagerName();

//            channelName = iTPaths.GetChannelName();
//            transportType = iTPaths.GetChannelTransportType();

//            connectionName = iTPaths.GetChannelServer();
//            connectionName += iTPaths.GetChannelPortNumber();
//            string serverAndPort = iTPaths.GetChannelServer();
//            serverAndPort += iTPaths.GetChannelPortNumber();
//            queueManager = new MQQueueManager(QueueManagerName, iTPaths.GetChannelName(), serverAndPort);

//        }

//        public MQQueue getQueueConnection(string qName, int qType)
//        {
//#if LoggingOn
//            //iTrekUtils.iTUtils.LogToEventLog("Inside getQueueConnection: " + qName);
//#endif
//            iTPaths = new iTXMLPathscl();
//            //TODO: Don't use queue name at this level
//            //can be instatiated twice and change relevant queue
//            //QueueName = queueNm;
//            QueueManagerName = iTPaths.GetQueueManagerName();

//            channelName = iTPaths.GetChannelName();
//            transportType = iTPaths.GetChannelTransportType();

//            connectionName = iTPaths.GetChannelServer();
//            connectionName += iTPaths.GetChannelPortNumber();
//            string serverAndPort = iTPaths.GetChannelServer();
//            serverAndPort += iTPaths.GetChannelPortNumber();
//            queueManager = new MQQueueManager(QueueManagerName, iTPaths.GetChannelName(), serverAndPort);
//            //queueManager.AlternateUserId = "mqitrek";

//            queue = queueManager.AccessQueue(qName, qType);
//#if LoggingOn
//          //  iTrekUtils.iTUtils.LogToEventLog("getQueueConnection: " + queue.Name);
//#endif

//            return queue;

//        }
//        public MQQueueManager getQManager
//        {
//            get
//            {
//                return queueManager;

//            }


//        }
//        private void closeQueueConnection()
//        {
//#if LoggingOn
//           // iTrekUtils.iTUtils.LogToEventLog("Inside closeQueueConnection: " + queue.Name);
//#endif

//            if (queue.IsOpen)
//            {
//#if LoggingOn
//                  //  iTrekUtils.iTUtils.LogToEventLog("Inside Going To Close queue: " + queue.Name);
//#endif
//                queue.Close();
//                queue.CloseOptions = MQC.MQCO_DELETE;


//            }
//            if (queueManager.IsConnected)
//            {
//#if LoggingOn
//                //iTrekUtils.iTUtils.LogToEventLog("Inside Going To Close queueManager: " +queueManager.Name);
//#endif
//                queueManager.Close();
//                queueManager.Disconnect();

//            }
//            // iTrekUtils.iTUtils.LogToEventLog("End of closequeueconnection: " + queue.);
//#if LoggingOn
//            //iTrekUtils.iTUtils.LogToEventLog("End of closequeueconnection: " + queueManager.IsConnected);
//#endif
//        }

//        /* Closing queue connection for services*/
//        public void closeQueueConnection(MQQueue locque, MQQueueManager locqueueManager)
//        {
//#if LoggingOn
//           // iTrekUtils.iTUtils.LogToEventLog("Inside servicecloseQueueConnection: " + locque.Name);
//#endif
//            if (locque.IsOpen)
//            {
//#if LoggingOn
//               // iTrekUtils.iTUtils.LogToEventLog("Inside Going To service Close queue: " + locque.Name);
//#endif
//                locque.Close();
//                locque.CloseOptions = MQC.MQCO_DELETE;


//            }
//            if (locqueueManager.IsConnected)
//            {
//#if LoggingOn
//             //   iTrekUtils.iTUtils.LogToEventLog("Inside Going To service Close queueManager: " + locqueueManager.Name);
//#endif
//                locqueueManager.Close();
//                locqueueManager.Disconnect();

//            }

//        }
//        public void PurgeQueue(string queueName)
//        {
//            while (GetMessageFromTheQueue(queueName) != "MQRC_NO_MSG_AVAILABLE")
//            {
//                GetMessageFromTheQueue(queueName);
//            }
//        }

//        public string putTheMessageIntoQueue(string queueName)
//        {
//            string status = "";
//            try
//            {

//                createQueueConnection();
//                queue = queueManager.AccessQueue(queueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);

//                return "Enter the message to put in MQSeries server:";
//                //message = System.Console.ReadLine();
//                int msgLen = message.Length;

//                while (msgLen == 0)
//                {
//                    if (msgLen == 0)
//                        return "Please reenter the message:";
//                    message = System.Console.ReadLine();
//                    msgLen = message.Length;
//                }

//                queueMessage = new MQMessage();
//                queueMessage.WriteBytes(message);
//                queueMessage.Format = MQC.MQFMT_STRING;
//                queuePutMessageOptions = new MQPutMessageOptions();

//                //putting the message into the queue
//                queue.Put(queueMessage, queuePutMessageOptions);
//                // MessageID = queueMessage.MessageId;

//                status = "Success fully entered the message into the queue";

//            }
//            catch (Exception mqexp)
//            {
//                status = "MQSeries Exception: " + mqexp.Message;
//                //iTrekUtils.iTUtils.LogToEventLog("Exception in putTheMessageIntoQueue: " + mqexp.Message);


//            }
//            finally
//            {

//                closeQueueConnection();

//            }
//#if LoggingOn
//           //iTrekUtils.iTUtils.LogToEventLog("End of putTheMessageIntoQueue: " + status);
//#endif
//            return status;
//        }


//        public string GetMessageFromTheQueue(string queueName)
//        {
//            string returnMessage = "";
//            try
//            {

//                createQueueConnection();
//                queue = queueManager.AccessQueue(queueName, MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
//                queueMessage = new MQMessage();
//                queueMessage.Format = MQC.MQFMT_STRING;
//                if (MessageID != null)
//                    queueMessage.MessageId = MessageID;

//                queueGetMessageOptions = new MQGetMessageOptions();


//                queue.Get(queueMessage, queueGetMessageOptions);
//                // Received Message.
//                //return "MessageID:" + MessageID + " Message:" + queueMessage.ReadString(queueMessage.MessageLength);

//                returnMessage = queueMessage.ReadString(queueMessage.MessageLength);

//            }
//            catch (Exception MQExp)
//            {
//                //HACK: Need to stop using exception to control queue purge
//                returnMessage = MQExp.Message;
//            }
//            finally
//            {
//                closeQueueConnection();

//            }
//#if LoggingOn
//           // iTrekUtils.iTUtils.LogToEventLog("GetMessageFromTheQueue: " + returnMessage);
//#endif
//            return returnMessage;
//        }




//        public string GetMessageFromTheQueue(MQQueue queue1)
//        {
//            string returnMessage = "";
//            try
//            {
//#if LoggingOn
//               // iTrekUtils.iTUtils.LogToEventLog("Inside GetMessageFromTheQueue: " + queue1.Name);
//#endif
//                //createQueueConnection();
//                //queue = queueManager.AccessQueue(queueName, MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
//                queueMessage = new MQMessage();
//                queueMessage.Format = MQC.MQFMT_STRING;
//                if (MessageID != null)
//                    queueMessage.MessageId = MessageID;

//                queueGetMessageOptions = new MQGetMessageOptions();


//                queue1.Get(queueMessage, queueGetMessageOptions);
//                // Received Message.
//                //return "MessageID:" + MessageID + " Message:" + queueMessage.ReadString(queueMessage.MessageLength);

//                returnMessage = queueMessage.ReadString(queueMessage.MessageLength);
//                //iTrekUtils.iTUtils.LogToEventLog("Inside GetMessageFromTheQueue: " + returnMessage);

//            }
//            catch (Exception MQExp)
//            {
//                //HACK: Need to stop using exception to control queue purge
//                returnMessage = MQExp.Message;
//            }
//            finally
//            {
//                //closeQueueConnection(queue1);

//            }
//#if LoggingOn
//           // iTrekUtils.iTUtils.LogToEventLog("End of GetMessageFromTheQueue: " + returnMessage);
//#endif
//            return returnMessage;
//        }
//        public string GetMessageFromTheQueueForOpenConnection(MQQueue queue1)
//        {
//            string returnMessage = "";



//            queueMessage = new MQMessage();
//            queueMessage.Format = MQC.MQFMT_STRING;
//            if (MessageID != null)
//                queueMessage.MessageId = MessageID;

//            queueGetMessageOptions = new MQGetMessageOptions();

//            queueGetMessageOptions.Options = MQC.MQGMO_BROWSE_FIRST + MQC.MQGMO_NO_WAIT;
//            queue1.Get(queueMessage, queueGetMessageOptions);

//            returnMessage = queueMessage.ReadString(queueMessage.MessageLength);


//            return returnMessage;
//        }




//        public string GetMessageFromTheQueueAnyOrder(string queueName)
//        {
//            string returnMessage = "";
//            try
//            {
//                createQueueConnection();
//                //In office
//                //queueManager = new MQQueueManager(QueueManagerName, "CHANNEL1", "192.168.1.8");
//                //At home
//                //queueManager = new MQQueueManager(QueueManagerName, "CHANNEL1", "http://www.ufissin.homelinux.net/");

//                queue = queueManager.AccessQueue(queueName, MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
//                queueMessage = new MQMessage();
//                queueMessage.Format = MQC.MQFMT_STRING;
//                //queueMessage.MessageId = MessageID;
//                queueGetMessageOptions = new MQGetMessageOptions();

//                queue.Get(queueMessage, queueGetMessageOptions);

//                // Received Message.
//                //System.Console.WriteLine("Received Message: {0}", queueMessage.ReadString(queueMessage.MessageLength));

//                returnMessage = queueMessage.ReadString(queueMessage.MessageLength);

//            }
//            catch (Exception MQExp)
//            {

//                returnMessage = "MQQueue::Get ended with " + MQExp.Message;
//            }
//            finally
//            {
//                closeQueueConnection();

//            }
//            return returnMessage;
//        }


//        public void GetConsoleMessageFromTheQueueAnyOrder(string queueName)
//        {


//            try
//            {


//                //QueueName = iTPaths.GetTOPUSHOK2LOADQueueName();
//                //QueueManagerName = "venus.queue.manager";
//                //ChannelInfo = "CHANNEL1/TCP/192.168.1.8";


//                //In office
//                //queueManager = new MQQueueManager(QueueManagerName, "CHANNEL1", "192.168.1.8");
//                //At home
//                //queueManager = new MQQueueManager(QueueManagerName, "CHANNEL1", "http://www.ufissin.homelinux.net/");
//                createQueueConnection();
//                queue = queueManager.AccessQueue(queueName, MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
//                queueMessage = new MQMessage();
//                queueMessage.Format = MQC.MQFMT_STRING;
//                //queueMessage.MessageId = MessageID;
//                queueGetMessageOptions = new MQGetMessageOptions();


//                queue.Get(queueMessage, queueGetMessageOptions);

//                // Received Message.
//                System.Console.WriteLine("Received Message: {0}", queueMessage.ReadString(queueMessage.MessageLength));
//                //return queueMessage.ReadString(queueMessage.MessageLength);


//            }
//            catch (Exception MQExp)
//            {
//                // report the error
//                //System.Console.WriteLine("MQQueue::Get ended with " + MQExp.Message);
//                System.Console.WriteLine("MQQueue::Get ended with " + MQExp.Message);

//            }

//            finally
//            {
//                closeQueueConnection();

//            }

//        }

//        public string GetMessageFromTheQueueReturnString(string queueName)
//        {
//            string returnMessage = "";

//            try
//            {
//                createQueueConnection();
//                queue = queueManager.AccessQueue(queueName, MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
//                queueMessage = new MQMessage();
//                queueMessage.Format = MQC.MQFMT_STRING;
//                //queueMessage.MessageId = MessageID;
//                queueGetMessageOptions = new MQGetMessageOptions();


//                queue.Get(queueMessage, queueGetMessageOptions);

//                // Received Message.
//                returnMessage = queueMessage.ReadString(queueMessage.MessageLength);
//                //System.Console.WriteLine("Received Message: {0}", queueMessage.MessageId[0].ToString());

//            }
//            catch (MQException MQExp)
//            {
//                // report the error

//                returnMessage = MQExp.Message;
//            }
//            finally
//            {
//                closeQueueConnection();

//            }
//            return returnMessage;
//        }
//        public void putXMLMessageIntoQueue(string queueName)
//        {
//            try
//            {
//                createQueueConnection();
//                queue = queueManager.AccessQueue(queueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);


//                message = iTUtils.ReadFromFile(@"C:\temp\tests\bigfile.xml");

//                int msgLen = message.Length;

//                while (msgLen == 0)
//                {
//                    if (msgLen == 0)
//                        Console.WriteLine("Please reenter the message:");
//                    message = System.Console.ReadLine();
//                    msgLen = message.Length;
//                }

//                queueMessage = new MQMessage();
//                queueMessage.WriteBytes(message);
//                queueMessage.Format = MQC.MQFMT_STRING;
//                queuePutMessageOptions = new MQPutMessageOptions();

//                //putting the message into the queue
//                queue.Put(queueMessage, queuePutMessageOptions);
//                MessageID = queueMessage.MessageId;

//                Console.WriteLine("Success fully entered the message into the queue");
//            }
//            catch (MQException mqexp)
//            {

//                Console.WriteLine("MQSeries Exception: " + mqexp.Message);
//            }
//            finally
//            {
//                closeQueueConnection();

//            }
//        }

//        public string putTheMessageIntoQueue(string message, string queueName)
//        {
//            string returnMessage = "";
//            try
//            {
//                createQueueConnection();
//                queue = queueManager.AccessQueue(queueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);

//                message = removeBadCharacters(message);
//                int msgLen = message.Length;

//                while (msgLen == 0)
//                {
//                    if (msgLen == 0)
//                        return "Add a message parameter";
//                    msgLen = message.Length;
//                }

//                queueMessage = new MQMessage();
//                queueMessage.WriteBytes(message);
//                queueMessage.Format = MQC.MQFMT_STRING;
//                queuePutMessageOptions = new MQPutMessageOptions();

//                //putting the message into the queue
//                queue.Put(queueMessage, queuePutMessageOptions);
//                queueMessage.
//                returnMessage = "success";
//                //add the new message id to the next messageid byte array element
//                //to retain earlier messages message ids
//                // MessageID[MessageID.Length + 1] = queueMessage.MessageId[MessageID.Length];

//                //returnMessage = MessageID[MessageID.Length + 1].ToString();

//                //return queueMessage.MessageId.ToString();
//            }
//            catch (Exception mqexp)
//            {

//                returnMessage = "MQSeries Exception: " + mqexp.Message;
//                // iTrekUtils.iTUtils.LogToEventLog("putTheMessageIntoQueue: " + message + ":" + returnMessage);
//                // TextWriter tw = new StreamWriter("C://MQTest.txt");

//                // write a line of text to the file
//                // tw.WriteLine(queueName + "|" + message);

//                // close the stream
//                //tw.Close();
//            }
//            finally
//            {
//                closeQueueConnection();

//            }
//#if LoggingOn
//            //iTrekUtils.iTUtils.LogToEventLog("putTheMessageIntoQueue: " +message+":"+ returnMessage);
//#endif
//            return returnMessage;
//        }
//        public string putConsoleMessageIntoQueue(string message, string queueName)
//        {
//            string returnMessage = "";
//            try
//            {
//                createQueueConnection();
//                queue = queueManager.AccessQueue(queueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);

//                int msgLen = message.Length;

//                while (msgLen == 0)
//                {
//                    if (msgLen == 0)
//                        return "Add a message parameter";
//                    msgLen = message.Length;
//                }

//                queueMessage = new MQMessage();
//                queueMessage.WriteBytes(message);
//                queueMessage.Format = MQC.MQFMT_STRING;
//                queuePutMessageOptions = new MQPutMessageOptions();

//                //putting the message into the queue
//                queue.Put(queueMessage, queuePutMessageOptions);
//                //add the new message id to the next messageid byte array element
//                //to retain earlier messages message ids
//                //MessageID[MessageID.Length +1] = queueMessage.MessageId[MessageID.Length];

//                //return MessageID[MessageID.Length + 1].ToString();
//                MessageID = queueMessage.MessageId;
//                //return MessageID[MessageID.Length - 1].ToString(); ;
//                //return queueMessage.MessageId.ToString();

//                returnMessage = message;
//            }
//            catch (Exception mqexp)
//            {

//                returnMessage = "MQSeries Exception: " + mqexp.Message;
//            }
//            finally
//            {
//                closeQueueConnection();

//            }
//            return returnMessage;
//        }
//        public void putMessageIntoQueueForMultipleMessages(string msg, string queueName)
//        {

//            try
//            {
//                createQueueConnection();
//                queue = queueManager.AccessQueue(queueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);

//                message = msg;

//                queueMessage = new MQMessage();
//                queueMessage.WriteBytes(message);
//                queueMessage.Format = MQC.MQFMT_STRING;
//                queuePutMessageOptions = new MQPutMessageOptions();

//                //putting the message into the queue
//                queue.Put(queueMessage, queuePutMessageOptions);
//                MessageID = queueMessage.MessageId;

//                Console.WriteLine("Success fully entered the message into the queue");
//            }
//            catch (MQException mqexp)
//            {

//                Console.WriteLine("MQSeries Exception: " + mqexp.Message);
//            }
//            finally
//            {
//                closeQueueConnection();


//            }
//        }

//        public string putMessageIntoFROMITREKAUTHQueueWithSessionID(string PENO, string pword, string sessID, string queueName, string src)
//        {
//            string returnMessage = "";
//            try
//            {
//                createQueueConnection();
//                queue = queueManager.AccessQueue(queueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);

//                message = @"<AUTH><PENO>" + PENO + "</PENO><PASS>" + pword + "</PASS><SESSIONID>" + sessID + "</SESSIONID><SRC>" + src + "</SRC></AUTH>";

//#if LoggingOn
//                //iTrekUtils.iTUtils.LogToEventLog("Inside putAuth: " + message);
//#endif
//                queueMessage = new MQMessage();
//                //NB. use WriteBytes here not Write
//                //the message must be converted to UTF-8
//                //understandable by UNIX from UTF-16
//                //(Windows default)
//                queueMessage.WriteBytes(message);
//                queueMessage.Format = MQC.MQFMT_STRING;
//                queuePutMessageOptions = new MQPutMessageOptions();

//                //putting the message into the queue
//                queue.Put(queueMessage, queuePutMessageOptions);
//                //add the new message id to the next messageid byte array element
//                //to retain earlier messages message ids
//                //MessageID[MessageID.Length + 1] = queueMessage.MessageId[MessageID.Length];

//                //return MessageID[MessageID.Length + 1].ToString();
//                //TODO: Check relevance of MessageID here

//                returnMessage = "success";

//            }
//            catch (Exception mqexp)
//            {

//                returnMessage = "MQSeries Exception: " + mqexp.Message;
//            }
//            finally
//            {
//                closeQueueConnection();

//            }
//            //  iTrekUtils.iTUtils.LogToEventLog("Inside putMessageIntoFROMITREKAUTHQueueWithSessionID: " + returnMessage);
//#if LoggingOn
//          //  iTrekUtils.iTUtils.LogToEventLog("putMessageIntoFROMITREKAUTHQueueWithSessionID: " + returnMessage);
//#endif
//            return returnMessage;
//        }
//        public string putAllOK2LoadMessageIntoFROMITREKSELECTQueueWithSessionID(string flightno, string URNO, string sessID, string queueName)
//        {
//            string returnMessage = "";
//            try
//            {
//                createQueueConnection();

//                queue = queueManager.AccessQueue(queueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);
//                message = "<ALLOK2L><URNO>" + URNO + "</URNO><SESSIONID>" + sessID + "</SESSIONID><FLIGHTNO>" + flightno + "</FLIGHTNO></ALLOK2L>";

//                int msgLen = message.Length;

//                while (msgLen == 0)
//                {
//                    if (msgLen == 0)
//                        return "Add a message parameter";
//                    msgLen = message.Length;
//                }
//                //message = message.Trim();
//                queueMessage = new MQMessage();
//                queueMessage.WriteBytes(message);
//                queueMessage.Format = MQC.MQFMT_STRING;
//                queuePutMessageOptions = new MQPutMessageOptions();

//                //putting the message into the queue
//                queue.Put(queueMessage, queuePutMessageOptions);
//                //add the new message id to the next messageid byte array element
//                //to retain earlier messages message ids
//                //MessageID[MessageID.Length + 1] = queueMessage.MessageId[MessageID.Length];

//                //return MessageID[MessageID.Length + 1].ToString();
//                //TODO: Check relevance of MessageID here

//                returnMessage = "success";
//            }
//            catch (Exception mqexp)
//            {

//                returnMessage = "MQSeries Exception: " + mqexp.Message;
//            }
//            finally
//            {
//                closeQueueConnection();

//            }
//#if LoggingOn
//           // iTrekUtils.iTUtils.LogToEventLog("putAllOK2LoadMessageIntoFROMITREKSELECTQueueWithSessionID: " + returnMessage);
//#endif
//            return returnMessage;
//        }

//        public string putShowDLSMessageIntoFROMITREKSELECTQueueWithSessionID(string flightno, string URNO, string sessID, string queueName)
//        {
//            string returnMessage = "";
//            try
//            {
//                createQueueConnection();
//                queue = queueManager.AccessQueue(queueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);

//                message = "<SHOWDLS><URNO>" + URNO + "</URNO><SESSIONID>" + sessID + "</SESSIONID></SHOWDLS>";
//                int msgLen = message.Length;
//                message = message.Trim();
//                while (msgLen == 0)
//                {
//                    if (msgLen == 0)
//                        return "Add a message parameter";
//                    msgLen = message.Length;
//                }

//                queueMessage = new MQMessage();
//                queueMessage.WriteBytes(message);
//                queueMessage.Format = MQC.MQFMT_STRING;


//                //putting the message into the queue
//                queue.Put(queueMessage);
//                //add the new message id to the next messageid byte array element
//                //to retain earlier messages message ids
//                //MessageID[MessageID.Length + 1] = queueMessage.MessageId[MessageID.Length];

//                //return MessageID[MessageID.Length + 1].ToString();
//                //TODO: Check relevance of MessageID here

//                returnMessage = "success";
//            }
//            catch (Exception mqexp)
//            {

//                returnMessage = "MQSeries Exception: " + mqexp.Message;
//            }
//            finally
//            {
//                closeQueueConnection();

//            }
//#if LoggingOn
//           // iTrekUtils.iTUtils.LogToEventLog("putShowDLSMessageIntoFROMITREKSELECTQueueWithSessionID: " + returnMessage);
//#endif
//            return returnMessage;
//        }

//        public string putShowMessageIntoFROMITREKSELECTQueue(string messageType, int messageDuration, string queueName)
//        {
//            string returnMessage = "";
//            try
//            {
//                createQueueConnection();
//                queue = queueManager.AccessQueue(queueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);
//                if (messageType == "msg")
//                {
//                    message = "<MESSAGE><DURATION>" + messageDuration + "</DURATION></MESSAGE>";
//                }
//                else
//                {
//                    message = "<MESSAGETO><DURATION>" + messageDuration + "</DURATION></MESSAGETO>";


//                }
//                int msgLen = message.Length;
//                message = message.Trim();
//                while (msgLen == 0)
//                {
//                    if (msgLen == 0)
//                        return "Add a message parameter";
//                    msgLen = message.Length;
//                }

//                queueMessage = new MQMessage();
//                queueMessage.WriteBytes(message);
//                queueMessage.Format = MQC.MQFMT_STRING;


//                //putting the message into the queue
//                queue.Put(queueMessage);
//                //add the new message id to the next messageid byte array element
//                //to retain earlier messages message ids
//                //MessageID[MessageID.Length + 1] = queueMessage.MessageId[MessageID.Length];

//                //return MessageID[MessageID.Length + 1].ToString();
//                //TODO: Check relevance of MessageID here

//                returnMessage = "success";
//            }
//            catch (Exception mqexp)
//            {

//                returnMessage = "MQSeries Exception: " + mqexp.Message;
//            }
//            finally
//            {
//                closeQueueConnection();

//            }
//#if LoggingOn
//           // iTrekUtils.iTUtils.LogToEventLog("putShowMessageIntoFROMITREKSELECTQueue " + returnMessage);
//#endif
//            return returnMessage;
//        }
//        public void PutATDTimingOnFROMITREKUPDATEQueue(string URNO, string flightno, string ATDTime, string PENO, string queueName)
//        {
//            string message = "<ATDTIME><URNO>" + URNO + "</URNO><FLIGHTNO>" + flightno + "</FLIGHTNO><ATD>" + ATDTime + "</ATD><PENO>" + PENO + "</PENO></ATDTIME>";
//            //set to the current queue?
//            putTheMessageIntoQueue(message, queueName);

//        }
//        public string PutTimingOnFROMITREKUPDATEQueue(string URNO, string fncName, string time, string fltType, string queueName, string ustf)
//        {
//            Hashtable orgTable = new Hashtable();
//            orgTable.Clear();
//            orgTable.Add("MAB", "AO");
//            orgTable.Add("LOAD-START", "AO");
//            orgTable.Add("LOAD-END", "AO");
//            orgTable.Add("LAST-DOOR", "AO");
//            orgTable.Add("PLB", "AO");
//            orgTable.Add("ATD", "AO");
//            orgTable.Add("ATA", "AO");
//            orgTable.Add("TBS-UP", "AO");
//            orgTable.Add("UNLOAD-START", "AO");
//            orgTable.Add("UNLOAD-END", "AO");
//            orgTable.Add("FBAG", "BO");
//            orgTable.Add("LBAG", "BO");
//            orgTable.Add("FTRIP-B", "BO");
//            orgTable.Add("LTRIP-B", "BO");
//            if (fltType == "D")
//            {
//                orgTable.Add("FTRIP", "BO");
//                orgTable.Add("LTRIP", "BO");
//            }
//            else
//            {
//                orgTable.Add("FTRIP", "AO");
//                orgTable.Add("LTRIP", "AO");

//            }
//            string message = "";
//            if (fltType == "D")
//            {
//                message = "<DFLIGHT><URNO>" + URNO + "</URNO><" + fncName + ">" + time + "</" + fncName + "><ORG>" + orgTable[fncName].ToString() + "</ORG><PENO>" + ustf + "</PENO></DFLIGHT>";
//            }
//            else
//            {
//                message = "<AFLIGHT><URNO>" + URNO + "</URNO><" + fncName + ">" + time + "</" + fncName + "><ORG>" + orgTable[fncName].ToString() + "</ORG><PENO>" + ustf + "</PENO></AFLIGHT>";


//            }
//            //set to the current queue?
//            return putTheMessageIntoQueue(message, queueName);

//        }
//        public void PutLOAOnFROMITREKQueue(string urno, string queueName)
//        {
//            string message = "";

//            message = "<LOA>" + urno + "</LOA>";

//            //set to the current queue?
//            putTheMessageIntoQueue(message, queueName);

//        }

//        public string putMessageIntoFROMITREKMSGQueue(string PENO, string sName, string msgFromUser, ArrayList lstReceipients, string queueName)
//        {
//            string returnMessage = "";
//            try
//            {
//                createQueueConnection();
//                queue = queueManager.AccessQueue(queueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);

//                string recList = "<MSGTO>";
//                foreach (string receipient in lstReceipients)
//                {
//                    recList += "<MSG_TO_ID>" + receipient + "</MSG_TO_ID>";
//                }
//                recList += "</MSGTO>";
//                // message = "<MSG><SENT_BY_ID>00127364</SENT_BY_ID><SENT_BY_NAME>CHARLES</SENT_BY_NAME><MSG_BODY>Testing 123</MSG_BODY><MSGTO><MSG_TO_ID>00127364</MSG_TO_ID></MSGTO></MSG>";

//                message = "<MSG><SENT_BY_ID>" + PENO + "</SENT_BY_ID><SENT_BY_NAME>" + sName + "</SENT_BY_NAME><MSG_BODY>" + msgFromUser + "</MSG_BODY>" + recList + "</MSG>";
//                message = removeBadCharacters(message);
//#if LoggingOn
//               iTrekUtils.iTUtils.LogToEventLog("Inside Message: " + message);
//#endif
//                queueMessage = new MQMessage();
//                ////NB. use WriteBytes here not Write
//                ////the message must be converted to UTF-8
//                ////understandable by UNIX from UTF-16
//                ////(Windows default)
//                queueMessage.WriteBytes(message);
//                queueMessage.Format = MQC.MQFMT_STRING;
//                queuePutMessageOptions = new MQPutMessageOptions();

//                ////putting the message into the queue
//                queue.Put(queueMessage, queuePutMessageOptions);
//                ////add the new message id to the next messageid byte array element
//                ////to retain earlier messages message ids
//                ////MessageID[MessageID.Length + 1] = queueMessage.MessageId[MessageID.Length];

//                ////return MessageID[MessageID.Length + 1].ToString();
//                ////TODO: Check relevance of MessageID here

//                returnMessage = "success";

//            }
//            catch (Exception mqexp)
//            {

//                returnMessage = "MQSeries Exception: " + mqexp.Message;
//            }
//            finally
//            {
//                closeQueueConnection();

//            }
//            //  iTrekUtils.iTUtils.LogToEventLog("Inside putMessageIntoFROMITREKAUTHQueueWithSessionID: " + returnMessage);
//#if LoggingOn
//           // iTrekUtils.iTUtils.LogToEventLog("putMessageIntoFROMITREKMSGQueue" + returnMessage);
//#endif
//            return returnMessage;
//        }


//        public string putMessageIntoFROMITREKMSGQueue(string urno, string staffId, string queueName)
//        {
//            string returnMessage = "";
//            try
//            {
//                createQueueConnection();
//                queue = queueManager.AccessQueue(queueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);


//                message = "<MSGTO><URNO>" + urno + "</URNO><RCV_BY>" + staffId + "</RCV_BY><RCV_DT></RCV_DT></MSGTO>";
//#if LoggingOn
//                //iTrekUtils.iTUtils.LogToEventLog("Inside putAuth: " + message);
//#endif
//                queueMessage = new MQMessage();
//                ////NB. use WriteBytes here not Write
//                ////the message must be converted to UTF-8
//                ////understandable by UNIX from UTF-16
//                ////(Windows default)
//                queueMessage.WriteBytes(message);
//                queueMessage.Format = MQC.MQFMT_STRING;
//                queuePutMessageOptions = new MQPutMessageOptions();

//                ////putting the message into the queue
//                queue.Put(queueMessage, queuePutMessageOptions);
//                ////add the new message id to the next messageid byte array element
//                ////to retain earlier messages message ids
//                ////MessageID[MessageID.Length + 1] = queueMessage.MessageId[MessageID.Length];

//                ////return MessageID[MessageID.Length + 1].ToString();
//                ////TODO: Check relevance of MessageID here

//                returnMessage = "success";

//            }
//            catch (Exception mqexp)
//            {

//                returnMessage = "MQSeries Exception: " + mqexp.Message;
//            }
//            finally
//            {
//                closeQueueConnection();

//            }
//#if LoggingOn
//           // iTrekUtils.iTUtils.LogToEventLog("putMessageIntoFROMITREKMSGQueue" + returnMessage);
//#endif
//            //  iTrekUtils.iTUtils.LogToEventLog("Inside putMessageIntoFROMITREKAUTHQueueWithSessionID: " + returnMessage);
//            return returnMessage;
//        }


//        private string removeBadCharacters(string message)
//        {

//            if (message != "")
//            {
//                message = message.Replace("'", "");
//            }


//            return message;

//        }
//        public MQQueue getQueueConnectionForServices(string qName, int qType, string mqStatus)
//        {


//            if (mqStatus == "SINGLE")
//            {

//                queue = getQManager.AccessQueue(qName, qType);
//            }
//            else
//            {
//                getQueueConnection(qName, qType);

//            }
//#if LoggingOn
//           // iTrekUtils.iTUtils.LogToEventLog("getQueueConnectionForServices: " + queue.Name);
//#endif
//            return queue;

//        }

//        public void closeQueueConnectionForServices(MQQueue locque, MQQueueManager locqueueManager, string mqStatus)
//        {

//            if (mqStatus == "SINGLE")
//            {

//                if (locque.IsOpen)
//                {
//#if LoggingOn
//                    //    iTrekUtils.iTUtils.LogToEventLog("closeQueueConnectionForServices: " + locque.Name);
//#endif
//                    locque.Close();
//                    locque.CloseOptions = MQC.MQCO_DELETE;


//                }
//            }
//            else
//            {
//                closeQueueConnection(locque, locqueueManager);

//            }


//        }

//        //public void ConnectToQueue( 
//    }
}