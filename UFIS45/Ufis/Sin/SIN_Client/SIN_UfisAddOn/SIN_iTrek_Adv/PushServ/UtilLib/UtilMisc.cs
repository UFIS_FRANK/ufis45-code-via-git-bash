using System;
using System.Collections.Generic;
using System.Text;

using System.IO;
using System.IO.Compression;
using System.Management;

using System.Collections;

namespace MM.UtilLib
{
	public class UtilMisc
	{
		/// <summary>
		/// Get the Selection String for a given data in string in hashtable key
		/// </summary>
		/// <param name="htDataInSt">Data Hashtable in Key as string</param>
		/// <param name="noOfCntInASelection">Number of Count to put in each selection string between 10 and 200</param>
		/// <returns>string array of selection string (e.g. [0] ==> '23453','345345','456546'; [1] ==> '62345','74613','63245'</returns>
		public static string[] GetDataForSelection(Hashtable htDataInSt, int noOfCntInASelection)
		{
			string[] stArr = null;

			if (noOfCntInASelection < 10) noOfCntInASelection = 10;
			else if (noOfCntInASelection > 200) noOfCntInASelection = 200;
			int cntArr = 1;
			if (htDataInSt != null)
			{
				cntArr = (int)(Math.Ceiling((decimal)htDataInSt.Count / (decimal)noOfCntInASelection));
				if (cntArr < 1) cntArr = 1;
				stArr = new string[cntArr];
				if (htDataInSt != null)
				{
					StringBuilder sb = new StringBuilder();
					int cnt = 0;
					int arrIdx = 0;
					foreach (DictionaryEntry de in htDataInSt.Keys)
					{
						string id = (string)de.Key;
						if (cnt > 0) sb.Append(",");
						sb.Append("'" + id + "'");
						cnt++;
						if (cnt == noOfCntInASelection)
						{
							stArr[arrIdx] = sb.ToString();
							cnt = 0;
							arrIdx++;
							sb = new StringBuilder();
						}
					}

					if (cnt > 0) stArr[arrIdx] = sb.ToString();
				}
			}

			return stArr;
		}

		const char FILE_SEP = '\\';

		/// <summary>
		/// Get File Name from given file name consists of paths
		/// e.g.
		///    fullFileName ==> C:\TMP\TEST.TXT
		///    return       ==> TEXT.TXT
		/// </summary>
		/// <param name="fullFileName">File Name with full path info</param>
		/// <returns>File Name only</returns>
		public static string GetFileNameOnly(string fullFileName)
		{
			string fName = "";
			if (!string.IsNullOrEmpty(fullFileName))
			{
				//char[] fileSep = { FILE_SEP };
				//string[] stArr = fullFileName.Split(fileSep);
				//int cnt = stArr.Length - 1;
				//if (cnt >= 0)
				//{
				//    fName = stArr[cnt];
				//}
				fName = System.IO.Path.GetFileName(fullFileName);
			}
			return fName;
		}

		/// <summary>
		/// Compose full file name consists with path using prefix and susfix
		/// e.g.
		///     path       ==> C:\TMP\
		///     filePrefix ==> PRE_
		///     fileName   ==> TEST.TXT
		///     fileSuffix ==> _SUF
		///     result     ==> C:\TMP\PRE_TEST_SUF.TXT
		/// </summary>
		/// <param name="path">File Path</param>
		/// <param name="filePrefix">Prefix of the file</param>
		/// <param name="fileName">File Name</param>
		/// <param name="fileSuffix">Suffix for the file</param>
		/// <returns>Full File Name</returns>
		public static string GetFullFileName(string path, string filePrefix, string fileName, string fileSuffix)
		{
			return Path.Combine(path,
				filePrefix +
				Path.GetFileNameWithoutExtension(fileName) +
				fileSuffix +
				Path.GetExtension(fileName));
		}

		/// <summary>
		/// Combine the path and file name
		/// </summary>
		/// <param name="path">File Path</param>
		/// <param name="fileName">File Name</param>
		/// <returns>Full File Name</returns>
		public static string Combine(string path, string fileName)
		{
			return System.IO.Path.Combine(path, fileName);
		}

		/// <summary>
		/// Convert string arraylist into delimited string
		/// </summary>
		/// <param name="arr">arraylist string</param>
		/// <param name="delimiter">delimiter for result string (e.g. ',')</param>
		/// <returns>delimited list of string (e.g. a,e,b,n)</returns>
		public static string ConvArrListToString(ArrayList arr, string delimiter)
		{
			StringBuilder result = new StringBuilder();
			if ((arr != null) && (arr.Count > 0))
			{
				int cnt = arr.Count;
				result.Append((string)arr[0]);
				for (int i = 1; i < cnt; i++)
				{
					result.Append(delimiter + (string)arr[i]);
				}
			}
			return result.ToString();
		}

		/// <summary>
		/// Convert Delimited String to Array List
		/// </summary>
		/// <param name="delimitedString">Delimited String (e.g. a,e,b,n)</param>
		/// <param name="delimiter">Delimiter of the string (e.g. ',')</param>
		/// <returns>ArrayList</returns>
		public static ArrayList ConvDelimitedStringToArrayList(string delimitedString, char delimiter)
		{
			ArrayList arr = new ArrayList();
			char[] del = { delimiter };
			string[] stArr = delimitedString.Split(del);
			int cnt = stArr.Length;
			for (int i = 0; i < cnt; i++)
			{
				arr.Add(stArr[i]);
			}
			return arr;
		}

		public static string ConvListToString(List<string> arr, string delimiter)
		{
			StringBuilder result = new StringBuilder();
			if ((arr != null) && (arr.Count > 0))
			{
				int cnt = arr.Count;
				result.Append((string)arr[0]);
				for (int i = 1; i < cnt; i++)
				{
					result.Append(delimiter + (string)arr[i]);
				}
			}
			return result.ToString();
		}

		public static string ConvListToString(List<string> arr, string delimiter, string pfix, string sfix, int frIdx, int toIdx)
		{
			StringBuilder result = new StringBuilder();
			if (frIdx > toIdx) throw new ApplicationException("UtilMisc:ConvListToString:Invalid From and To.");
			if ((arr != null) && (arr.Count > 0))
			{
				int cnt = arr.Count;
				if (frIdx < 0) frIdx = 0;
				else if (frIdx >= cnt) frIdx = cnt - 1;

				if (toIdx < 0) toIdx = 0;
				else if (toIdx >= cnt) toIdx = cnt - 1;


				result.Append(pfix + (string)arr[frIdx] + sfix);
				for (int i = frIdx + 1; i <= toIdx; i++)
				{
					result.Append(delimiter + pfix + (string)arr[i] + sfix);
				}
			}
			return result.ToString();
		}

		/// <summary>
		/// Convert ArrayList into hashtable
		/// </summary>
		/// <param name="arr"></param>
		/// <returns></returns>
		public static Hashtable ConvArrayListToHashTable(ArrayList arr)
		{
			Hashtable ht = new Hashtable();
			if (arr != null)
			{
				int cnt = arr.Count;
				for (int i = 0; i < cnt; i++)
				{
					try
					{
						if (!ht.ContainsKey(arr[i]))
						{
							ht.Add(arr[i], arr[i]);
						}
					}
					catch (Exception)
					{
					}
				}
			}
			return ht;
		}

		/// <summary>
		/// Convert ArrayList into generic list of string
		/// </summary>
		/// <param name="arr"></param>
		/// <returns></returns>
		public static List<string> ConvArrayListToStringList(ArrayList arr)
		{
			List<string> result = null;
			if (arr != null)
			{
				result = new List<string>();
				int cnt = arr.Count;
				for (int i = 0; i < cnt; i++)
				{
					try
					{
						result.Add((string)arr[i]);
					}
					catch (Exception)
					{
					}
				}
			}
			return result;
		}



		/// <summary>
		/// Get the Drive Info (size and freespace) for given drive
		/// </summary>
		/// <param name="driveName">Drive Name (e.g. C)</param>
		/// <param name="size">Size of the drive</param>
		/// <param name="freeSpace">Free space of the drive</param>
		public static void DriveInfo(string driveName, out string size, out string freeSpace)
		{
			size = "-1";
			freeSpace = "-1";
			try
			{
				ManagementObject disk = new ManagementObject("win32_logicaldisk.deviceid='" + driveName + ":'");
				disk.Get();
				size = disk["Size"].ToString();
				freeSpace = disk["FreeSpace"].ToString();
			}
			catch (Exception)
			{
			}
		}

		/// <summary>
		/// Convert string to byte array
		/// </summary>
		/// <param name="str">string to convert to byte array</param>
		/// <returns>byte array</returns>
		public static byte[] ConvertStrToByteArray(string str)
		{
			System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
			return encoding.GetBytes(str);
		}

		/// <summary>
		/// Convert Byte Array to String
		/// </summary>
		/// <param name="arr">Byte Array</param>
		/// <returns>string</returns>
		public static string ConvertByteArrayToStr(byte[] arr)
		{
			System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
			return encoding.GetString(arr);
		}

		public static byte[] Zip(byte[] unZipData)
		{
			MemoryStream ms = new MemoryStream();
			// Use the newly created memory stream for the compressed data.
			GZipStream compressedzipStream = new GZipStream(ms, CompressionMode.Compress, true);
			//Console.WriteLine("Compression");
			compressedzipStream.Write(unZipData, 0, unZipData.Length);

			// Close the stream.
			compressedzipStream.Close();
			byte[] zipData = new Byte[ms.Length + 100];
			//ms.Write(zipData, 0, (int)ms.Length);
			//WriteStreamToByteArr(ms, zipData);
			zipData = ms.GetBuffer();
			ms.Close();
			return zipData;
		}

		public static byte[] UnZip(byte[] zipData)
		{
			MemoryStream ms = new MemoryStream(zipData);
			//ms.Read(zipData, 0, zipData.Length);
			ms.Position = 0;
			GZipStream zipStream = new GZipStream(ms, CompressionMode.Decompress);
			//Console.WriteLine("Decompression");
			byte[] unZipData = new byte[zipData.Length * 30];
			// Use the ReadAllBytesFromStream to read the stream.
			int totalCount = ReadAllBytesFromStream(zipStream, unZipData);

			zipStream.Close();
			ms.Close();
			return unZipData;
		}

		public static void WriteStreamToByteArr(Stream stream, byte[] buffer)
		{
			long offset = 0;
			long totCnt = stream.Length;
			long cnt = 1000;
			int res = (int)(totCnt % cnt);
			int loopCnt = (int)(totCnt / cnt);

			for (int i = 0; i < loopCnt; i++)
			{
				stream.Write(buffer, (int)offset, (int)cnt);
				offset += cnt;
			}
			stream.Write(buffer, (int)offset, res);
		}

		public static int ReadAllBytesFromStream(Stream stream, byte[] buffer)
		{
			// Use this method is used to read all bytes from a stream.
			int offset = 0;
			int totalCount = 0;

			while (true)
			{
				int bytesRead = stream.Read(buffer, offset, 100);
				if (bytesRead == 0)
				{
					break;
				}
				offset += bytesRead;
				totalCount += bytesRead;
			}
			return totalCount;
		}

		public static byte[] AsciiStrToByteArray(string str)
		{
			System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
			return encoding.GetBytes(str);
		}

		public static string ByteArrayToAsciiString(byte[] byteArr)
		{
			System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
			return enc.GetString(byteArr);
		}

		/// <summary>
		/// Add Key, Value pair to the hashtable.
		/// </summary>
		/// <param name="ht"></param>
		/// <param name="key"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public static void AddToHash(ref Hashtable ht, object key, object value, out bool hasDuplicate)
		{
			hasDuplicate = false;
			if (ht.ContainsKey(key))
			{
				hasDuplicate = true;
				ht[key] = value;
			}
			else ht.Add(key, value);
		}

		/// <summary>
		/// Get string from hash table for given key. Return true if it has keys.
		/// </summary>
		/// <param name="ht">Key, Value pair table</param>
		/// <param name="key">Key for which to get the value</param>
		/// <param name="stValue">value in string</param>
		/// <returns>True-If table contains given key</returns>
		public static bool GetStringFromHash(Hashtable ht, object key, out string stValue)
		{
			bool exist = false;
			stValue = "";
			if (ht.ContainsKey(key))
			{
				exist = true;
				object obj = ht[key];
                if (obj != null) stValue = obj.ToString();
			}
			return exist;
		}
	}
}