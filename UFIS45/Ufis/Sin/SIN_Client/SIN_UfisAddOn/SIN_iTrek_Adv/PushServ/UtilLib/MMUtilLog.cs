using System;
using System.Collections.Generic;
using System.Text;

namespace MM.UtilLib
{
    public class MMUtilLog
    {
        //private static string _logDir = @"C:\TEMP\";
        Queue<string> _q = new Queue<string>();

        private static object _lockTraceMsg = new object();

        public static void LogToTraceFile(string logDir, string logSource, string strMessage)
        {
            //if (logDir[logDir.Length - 1].ToString() != @"\")
            //{
            //    logDir += @"\";
            //}

            //lock (_lockTraceMsg)
            //{
            //    DateTime dt = System.DateTime.Now;
            //    string msg = string.Format("{0:ddHHmmss}: {1} : {2}",
            //        dt,
            //        logSource,
            //        strMessage);
            //    string fName = logDir + string.Format(@"Trc_{0:MMdd}.txt", dt);
                
            //    UtilFileIO.WriteToTextFile(fName, msg);
            //}
            LogToTraceFile(logDir, null, logSource, strMessage);
        }

        public static void LogToTraceFile(string logDir, string logFileName, string logSource, string strMessage)
        {
            if (logDir[logDir.Length - 1].ToString() != @"\")
            {
                logDir += @"\";
            }

            lock (_lockTraceMsg)
            {
                DateTime dt = System.DateTime.Now;
                
                string msg = string.Format("{0:ddHHmmss}{1:000}:{2}:{3}",
                    dt,
                    dt.Millisecond,
                    logSource,
                    strMessage);

                if (logFileName == null) logFileName = "Trc";
                else
                {
                    logFileName = logFileName.Trim();
                    if (logFileName == "") logFileName = "Trc";
                }

                string fName = logDir + string.Format(@"{0}_{1:MMdd}.txt", logFileName, dt);
                UtilFileIO.WriteToTextFile(fName, msg);
            }
        }
    }
}
