using System;
using System.Collections.Generic;
using System.Text;

using System.ServiceProcess;

namespace MM.UtilLib
{
    public class UtilSvc
    {
        public static void StopASvc(string svcName)
        {
            //System.Diagnostics.Process proc = new System.Diagnostics.Process();
            //proc.EnableRaisingEvents = false;

            //proc.StartInfo.FileName = "NET";
            //proc.StartInfo.Arguments = "STOP " + svcName;
            //proc.Start();

            ServiceController svcCtrl = null;
            try
            {
                svcCtrl = new ServiceController(svcName);
                svcCtrl.Stop();
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            finally
            {
                if (svcCtrl != null)
                    svcCtrl.Close();
            }
        }

        public static void PauseASvc(string svcName)
        {

            ServiceController svcCtrl = null;
            try
            {
                svcCtrl = new ServiceController(svcName);
                if (svcCtrl.CanPauseAndContinue)
                {
                    svcCtrl.Pause();
                }
                else
                {
                    throw new ApplicationException(svcName + " service can not pause");
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            finally
            {
                if (svcCtrl != null)
                    svcCtrl.Close();
            }
        }

        public static void ResumePausedSvc(string svcName)
        {
            ServiceController svcCtrl = null;
            try
            {
                svcCtrl = new ServiceController(svcName);
                if (svcCtrl.CanPauseAndContinue)
                {
                    svcCtrl.Continue();
                }
                else
                {
                    StartASvc(svcName);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            finally
            {
                if (svcCtrl != null)
                    svcCtrl.Close();
            }

        }

        public static void StartASvc(string svcName)
        {
            //System.Diagnostics.Process proc = new System.Diagnostics.Process();
            //proc.EnableRaisingEvents = false;

            //proc.StartInfo.FileName = "NET";
            //proc.StartInfo.Arguments = "START " + svcName;
            //proc.Start();

            ServiceController svcCtrl = null;
            try
            {
                svcCtrl = new ServiceController(svcName);
                svcCtrl.Start();
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            finally
            {
                if (svcCtrl != null)
                    svcCtrl.Close();
            }
        }


        public static string SvcStatus(string svcName)
        {
            //              Service     Service
            //              Status      Status String
            //              ======      =============
            //              Start   ==> Running
            //              Stop    ==> Stopped
            //              Pause   ==> Paused
            string stat = "";
            ServiceController svcCtrl = null;
            try
            {
                svcCtrl = new ServiceController(svcName);
                stat = svcCtrl.Status.ToString();
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            finally
            {
                if (svcCtrl != null)
                    svcCtrl.Close();
            }
            return stat;
        }

    }
}
