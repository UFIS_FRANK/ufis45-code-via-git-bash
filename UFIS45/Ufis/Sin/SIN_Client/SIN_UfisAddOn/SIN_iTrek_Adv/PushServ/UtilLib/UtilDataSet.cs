using System;
using System.Collections.Generic;
using System.Text;

using System.Data;
using System.IO;
using System.IO.Compression;

using System.Collections;
using System.Xml;

namespace MM.UtilLib
{
    public class UtilDataSet
    {
        private static UtilTraceLog _log = UtilTraceLog.GetInstance();
        private static void LogTraceMsg( string msg )
        {
            _log.LogTraceMsg("UtilDataSet", msg);
        }

        public static void LoadDataSetFromXmlString(DataSet ds, String st)
        {
            if (ds == null) throw new ApplicationException("Null DataSet.");
            if (st == null) throw new ApplicationException("No Data.");
            StringReader sr = new StringReader(st);
            ds.ReadXml(sr);
            sr.Close();
            sr.Dispose();
        }

        public static string GetDataSetString(DataSet ds)
        {
            if (ds == null) throw new ApplicationException("Null DataSet.");
            StringWriter sw = new StringWriter();
            ds.WriteXml(sw);
            string st = sw.ToString();
            sw.Close();
            sw.Dispose();

            return st;
        }

        public static void LoadDataTableFromXmlString(DataTable dt, String st)
        {
            if (dt == null) throw new ApplicationException("Null DataTable.");
            if (st == null) throw new ApplicationException("No Data.");
            StringReader sr = new StringReader(st);
            dt.ReadXml(sr);
            sr.Close();
            sr.Dispose();
        }

        public static string GetDataTableString(DataTable dt)
        {
            if (dt == null) throw new ApplicationException("Null DataTable.");
            StringWriter sw = new StringWriter();
            dt.WriteXml(sw);
            string st = sw.ToString();
            sw.Close();
            sw.Dispose();

            return st;
        }

       /// <summary>
        /// Get Data in [ColumnName]:[Value],[ColumnName]:[Value],... of the given row.
       /// </summary>
       /// <param name="row"></param>
       /// <returns></returns>
       public static string GetDataRowString(DataRow row)
       {
          if (row == null) throw new ApplicationException("Null Data Row.");
          StringBuilder sb = new StringBuilder();
          foreach (DataColumn col in row.Table.Columns)
          {
             sb.AppendFormat("{0}:{1}, ", col.ColumnName, row[col].ToString());
          }
          return sb.ToString();
       }

		public static string GetDataString(DataRow row, string colName)
		{
			if (!IsNullOrEmptyValue(row, colName)) return Convert.ToString(row[colName]);
			else return "";
		}

		public static string GetDataString(DataRow row, string colName, DataRowVersion rowVersion)
		{
			if (!IsNullOrEmptyValue(row, colName, rowVersion)) return Convert.ToString(row[colName, rowVersion]);
			else return "";
		}

        /// <summary>
        /// Prepare where clause for giving values in string separated by valSeperator for the given column 
        /// with 'OR' conjuction
        /// e.g.
        ///     values       ==> AB,AF,CE
        ///     valSeparator ==> ,
        ///     colName      ==> ID
        ///     valQuote     ==> '
        ///     return       ==> (ID='AB' OR ID='AF' OR ID='CE')
        /// </summary>
        /// <param name="values">Values (e.g. AB,AF,CE)</param>
        /// <param name="valSeperator">Seperator for value (e.g. , )</param>
        /// <param name="colName">Column Name (e.g. ID)</param>
        /// <param name="valQuote">Quotation for value in where clause</param>
        /// <returns>where clause</returns>
        public static string PrepareWhereClause(string values, char valSeperator, string colName, string valQuote)
        {
            StringBuilder sb = new StringBuilder();
            char[] del = { valSeperator };
            if (!string.IsNullOrEmpty(values))
            {
                string ids = values.Trim();
                string[] idArr = ids.Split(del);
                string conj = "(";
                for (int i = 0; i < idArr.Length; i++)
                {
                    string st = idArr[i].Trim();
                    if (st != "")
                    {
                        sb.AppendFormat("{0}{1}={2}{3}{2}",
                            conj, colName, valQuote, st);
                        conj = " OR ";
                    }
                }
                if (sb.Length > 1) sb.Append(")");
            }
            return sb.ToString();
        }

        /// <summary>
        /// Remove the Computed columns from datatable
        /// </summary>
        /// <param name="dt">Datatable from which to remove the computed column</param>
        public static void RemoveComputedColumns(DataTable dt)
        {
            //LogTraceMsg("Removing from " + dt.TableName);
            foreach (DataColumn col in dt.Columns)
            {
                if (IsExpressionColumn(col))
                {
                    col.ColumnMapping = MappingType.Hidden;
                }
            }
        }

        /// <summary>
        /// Save Data in given dataset to the xml file after removing the computed columns
        /// </summary>
        /// <param name="dsOrg">original dataset to save</param>
        /// <param name="fileName">XML File Name to save</param>
        /// <returns>true --> Successfully Save</returns>
        public static bool WriteXml(DataSet dsOrg, string fileName)
        {
            DataSet ds = dsOrg.Copy();
            foreach (DataTable dt in ds.Tables)
            {
                RemoveComputedColumns(dt);
            }

            return WriteDataToXmlFile(ds, fileName);
        }

        /// <summary>
        /// Save Data in given datatable to the xml file after removing the computed columns
        /// </summary>
        /// <param name="dsOrg">original datatable to save</param>
        /// <param name="fileName">XML File Name to save</param>
        /// <returns>true --> Successfully Save</returns>
        public static bool WriteXml(DataTable dtOrg, string fileName)
        {
            DataTable dt = dtOrg.Copy();
            RemoveComputedColumns(dt);
            return WriteDataToXmlFile(dt, fileName);
        }

        /// <summary>
        /// Load Data from XML file with locking read.
        /// </summary>
        /// <param name="ds">Dataset to load to (must not be null)</param>
        /// <param name="fileName">XML Data File Name</param>
        /// <returns>true - successfully loaded</returns>
        public static bool LoadDataFromXmlFile(DataSet ds, string fileName)
        {
            bool success = false;
            FileStream file = null;
            long fileLength = 0;
            string fileNameOnly = UtilMisc.GetFileNameOnly(fileName);
            try
            {
                LogTraceMsg("   LoadDataFromXmlFile:dt:trying to access file:" + fileNameOnly);
                for (int i = 0; i < 50; i++)
                {
                    if (HasWaitingWrite) System.Threading.Thread.Sleep(10);
                    else break;
                }
                if (HasWaitingWrite) throw new ApplicationException("Abort Reading due to wait for Writing " + UtilMisc.GetFileNameOnly(fileName));

                file = UtilFileIO.GetFileStreamForReading(fileName, 50);
                if (file == null)
                {
                    LogTraceMsg("LoadDataFromXmlFile:dt:Unable to read " + fileNameOnly);
                    throw new ApplicationException("Unable to read " + fileNameOnly);
                }

                fileLength = file.Length;
                file.Lock(0, fileLength);
                // Create a new stream to read from a file
                using (StreamReader sr = new StreamReader(file))
                {
                    ds.ReadXml(sr);
                    success = true;
                }
                LogTraceMsg("LoadDataFromXmlFile:after read " + fileNameOnly);
            }
            catch (Exception ex)
            {
                LogTraceMsg("LoadDataFromXmlFile:" + fileNameOnly + ":Err:" + ex.Message);
                throw ex;
            }
            finally
            {
                if (file != null)
                {
                    try
                    {
                        file.Unlock(0, fileLength);
                    }
                    catch (Exception)
                    {
                    }
                    //LogTraceMsg("LoadDataFromXmlFile:before close file");
                    file.Dispose();
                    file.Close();
                    LogTraceMsg("LoadDataFromXmlFile:after close file " + fileNameOnly);
                }
            }
            return success;
        }

        /// <summary>
        /// Load Data from XML file with locking read. And remove the invalid data from XML file
        /// </summary>
        /// <param name="ds">Datatable to load to (must not be null)</param>
        /// <param name="fileName">XML Data File Name</param>
        /// <returns>true - successfully loaded</returns>
        public static bool LoadDataFromXmlFile(DataTable dt, string fileName)
        {
            bool success = false;
            FileStream file = null;
            long fileLength = 0;
            string fileNameOnly = UtilMisc.GetFileNameOnly(fileName);
            try
            {
                LogTraceMsg("   LoadDataFromXmlFile:dt:trying to access file:" + fileNameOnly);
                for (int i = 0; i < 50; i++)
                {
                    if (HasWaitingWrite) System.Threading.Thread.Sleep(10);
                    else break;
                }
                if (HasWaitingWrite) throw new ApplicationException("Abort Reading due to wait for Writing " + UtilMisc.GetFileNameOnly(fileName));

                file = UtilFileIO.GetFileStreamForReading(fileName, 50);
                if (file == null)
                {
                    LogTraceMsg("LoadDataFromXmlFile:dt:Unable to read " + fileNameOnly);
                    throw new ApplicationException("Unable to read " + fileNameOnly);
                }

                fileLength = file.Length;
                file.Lock(0, fileLength);
                // Create a new stream to read from a file
                try
                {
                    using (StreamReader sr = new StreamReader(file))
                    {
                        dt.BeginLoadData();
                        dt.ReadXml(sr);
                        try
                        {
                            dt.EndLoadData();
                        }
                        catch (Exception)
                        {
                            if (dt.HasErrors)
                            {
                                DataRow[] rows = dt.GetErrors();
                                int errCnt = rows.Length;
                                LogTraceMsg("LoadDataFromXmlFile:dt:" + fileNameOnly + ":rowErrCnt-" + errCnt);
                                for (int i = 0; i < errCnt; i++)
                                {
                                    LogTraceMsg("LoadDataFromXmlFile:dt:rowErr:" + i + ":" + rows[i].RowError);
                                    rows[i].Delete();
                                }
                                dt.AcceptChanges();
                            }
                        }
                        success = true;
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    try
                    {
                        file.Unlock(0, fileLength);
                    }
                    catch { }
                }
                LogTraceMsg("LoadDataFromXmlFile:dt:after read:" + fileNameOnly);
            }
            catch (Exception ex)
            {
                LogTraceMsg("LoadDataFromXmlFile:dt:" + fileNameOnly + ":Err:" + ex.Message);
                throw ex;
            }
            finally
            {
                if (file != null)
                {
                    //LogTraceMsg("LoadDataFromXmlFile:before close file");
                    file.Dispose();
                    file.Close();
                    LogTraceMsg("LoadDataFromXmlFile:dt:after close file:" + fileNameOnly);
                }
            }
            return success;
        }

        private const int FILE_WRITE_SLEEP = 10; //Wait for this milisecond before next attempt to open the file to write.
        private const int FILE_READ_SLEEP = 30;//Wait for this milisecond before next attepmt to open the file to read.
        private static bool HasWaitingWrite = false;
        private const int WRITE_RETRY = 50;//Number of retry to write

        /// <summary>
        /// Write Data in Dataset to Xml File without removing anything
        /// </summary>
        /// <param name="ds">Dataset to save</param>
        /// <param name="fileName">xml file name to save</param>
        /// <returns>true- Successfully saved</returns>
        private static bool WriteDataToXmlFile(DataSet ds, string fileName)
        {
            bool success = false;
            FileStream file = null;
            XmlTextWriter xtw = null;
            string fileNameOnly = UtilMisc.GetFileNameOnly(fileName);
            try
            {
                HasWaitingWrite = true;
                LogTraceMsg("  WriteDataToXmlFile:Trying to get file access:" + fileNameOnly);

                file = UtilFileIO.GetFileStreamForWriting(fileName, WRITE_RETRY);
                //LogTraceMsg("WriteDataToXmlFile:get file access");
                if (file != null)
                {
                    xtw = new XmlTextWriter(file, Encoding.ASCII);
                    xtw.Formatting = Formatting.None;
                    ds.WriteXml(xtw);
                    xtw.Flush();
                    success = true;
                    LogTraceMsg("WriteDataToXmlFile:after write:" + UtilMisc.GetFileNameOnly(fileName));
                }
                else
                {
                    LogTraceMsg("WriteDataToXmlFile:Err:Unable to writexml:" + fileNameOnly);
                    throw new ApplicationException("Unable to write " + fileNameOnly);
                }
            }
            catch (Exception ex)
            {
                LogTraceMsg("WriteDataToXmlFile" + fileNameOnly + ":Err:" + ex.Message + "\n" + ex.StackTrace);
                throw ex;
            }
            finally
            {
                HasWaitingWrite = false;
                if (xtw != null) xtw.Close();
                // Close file
                if (file != null)
                {
                    //LogTraceMsg("  WriteDataToXmlFile:before close file");  
                    file.Dispose();
                    file.Close();
                    LogTraceMsg("  WriteDataToXmlFile:after close file:" + fileNameOnly);
                }
            }
            return success;
        }

        /// <summary>
        /// Write Data in Datatable to Xml File without removing anything
        /// </summary>
        /// <param name="ds">Datatable to save</param>
        /// <param name="fileName">xml file name to save</param>
        /// <returns>true- Successfully saved</returns>
        private static bool WriteDataToXmlFile(DataTable dt, string fileName)
        {
            bool success = false;
            FileStream file = null;
            XmlTextWriter xtw = null;
            string fileNameOnly = UtilMisc.GetFileNameOnly(fileName);
            try
            {
                HasWaitingWrite = true;
                LogTraceMsg("  WriteDataToXmlFile:Trying to get file access:" + fileNameOnly);

                file = UtilFileIO.GetFileStreamForWriting(fileName, WRITE_RETRY);
                if (file != null)
                {
                    LogTraceMsg("WriteDataToXmlFile:dt:get file access");
                    xtw = new XmlTextWriter(file, Encoding.ASCII);
                    xtw.Formatting = Formatting.None;
                    dt.WriteXml(xtw);
                    xtw.Flush();
                    success = true;
                    LogTraceMsg("  WriteDataToXmlFile:dt:after write");
                }
                else
                {
                    LogTraceMsg("WriteDataToXmlFile:dt:Err:Unable to writexml");
                    throw new ApplicationException("Unable to Write " + fileNameOnly);
                }
            }
            catch (Exception ex)
            {
                LogTraceMsg("WriteDataToXmlFile:dt:Err:" + ex.Message + "\n" + ex.StackTrace);
                throw ex;
            }
            finally
            {
                HasWaitingWrite = false;
                if (xtw != null) xtw.Close();
                // Close file
                if (file != null)
                {
                    //LogTraceMsg("  WriteDataToXmlFile:before close file"); 
                    file.Dispose();
                    file.Close();
                    LogTraceMsg("  WriteDataToXmlFile:dt:after close file:" + fileNameOnly);
                }
            }
            return success;
        }

        /// <summary>
        /// Is Expression (Virtual) Column
        /// </summary>
        /// <param name="col">column</param>
        /// <returns>true - if it is expression column</returns>
        public static bool IsExpressionColumn(DataColumn col)
        {
            return (col.ReadOnly) && (!((col.Expression == null) || (col.Expression.Trim() == "")));
        }

       public static List<string> GetNonExpressionColumnList(DataTable dt)
       {
          List<string> ls = new List<string>();
          foreach (DataColumn col in dt.Columns)
          {
             if (!IsExpressionColumn(col))
             {
                ls.Add(col.ColumnName);
             }
          }
          return ls;
       }

       public static DataSet RemoveExpressionAndRelations(DataSet ds)
       {
          foreach (DataTable dt in ds.Tables)
          {
             int cntCol = dt.Columns.Count;
             for (int i = cntCol - 1; i >= 0; i--)
             {
                if (UtilDataSet.IsExpressionColumn(dt.Columns[i]))
                {
                   dt.Columns.Remove(dt.Columns[i]);
                }
             }
          }
          ds.Relations.Clear();
          return ds;
       }

       public static bool AssignValue(DataRow row, DataColumn col, IDataReader dr, string dbColName)
       {
          bool success = false;
          try
          {
             object data = dr[dbColName];
             row[col] = Convert.ChangeType(data, col.DataType);
             success = true;
          }
          catch (Exception)
          {
          }

          return success;
       }

        /// <summary>
        /// Get changes by comparing 2 datatables.
        /// 1. With data changes (same keys in org and new table with different data)
        /// 2. New data (no such record in original table but exist in new table)
        /// </summary>
        /// <param name="dtOrg">Original Datatable</param>
        /// <param name="dtNew">New Datatable</param>
        /// <param name="keyColumns">Key Columns seperated by comma</param>
        /// <param name="dtChg">Table with data changes result</param>
        /// <returns>true --> Has changes</returns>
        public static bool GetChanges(DataTable dtOrg, DataTable dtNew, string keyColumns, DataTable dtChg)
        {
            bool hasChanges = false;
            DataRow[] rowsOrg = dtOrg.Select("", keyColumns);//Sort by key columns
            DataRow[] rowsNew = dtNew.Select("", keyColumns);//Sort by key columns

            int idxOrg = 0;
            int idxNew = 0;

            int cntOrg = rowsOrg.Length;
            int cntNew = rowsNew.Length;
            string[] keys = keyColumns.Split(_KEY_SEPERATOR);
            DataColumnCollection cols = dtNew.Columns;
            ArrayList arrColNames = new ArrayList();

            //Get the columns to check
            foreach (DataColumn col in dtNew.Columns)
            {
                if (!IsExpressionColumn(col))
                {//No need to compare the expression column
                    arrColNames.Add(col.ColumnName);
                }
            }

            //Convert columns from arraylist to string array
            int colCnt = arrColNames.Count;
            string[] stColNames = new string[colCnt];
            for (int i = 0; i < colCnt; i++)
            {
                stColNames[i] = arrColNames[i].ToString();
            }

            for (idxNew = 0; idxNew < cntNew; idxNew++)
            {
                DataRow rowNew = rowsNew[idxNew];
                if (idxOrg < cntOrg)
                {//Check against org row
                    DataRow rowOrg = rowsOrg[idxOrg];

                    if (IsSameKey(rowOrg, rowNew, keys))
                    {
                        idxOrg++;
                        if (IsDiff(rowOrg, rowNew, stColNames, keys))
                        {//Different Data with same key. Has changes
                            dtChg.Rows.Add(rowNew.ItemArray);
                        }
                    }
                    else
                    {//Not same key.
                        bool hasSameKey = false;
                        //try to search ahead in org table.
                        for (int idxTmpOrg = idxOrg + 1; idxTmpOrg < cntOrg; idxTmpOrg++)
                        {
                            rowOrg = rowsOrg[idxTmpOrg];
                            if (IsSameKey(rowOrg, rowNew, keys))
                            {//Same Key. Move the original data array pointer.
                                idxOrg = idxTmpOrg + 1;
                                hasSameKey = true;
                                break;
                            }
                        }

                        if (hasSameKey)
                        {
                            if (IsDiff(rowOrg, rowNew, stColNames, keys))
                            {//Different Data with same key. Have changes
                                dtChg.Rows.Add(rowNew.ItemArray);
                            }
                        }
                        else
                        {//No same key was found. No such record in original data. New Data
                            dtChg.Rows.Add(rowNew.ItemArray);
                        }
                    }
                }
                else
                {//All the rows in org was checked. New Data
                    dtChg.Rows.Add(rowNew.ItemArray);
                }
            }
            if (dtChg.Rows.Count > 0) hasChanges = true;
            return hasChanges;
        }

        /// <summary>
        /// Is the data different for given original and new datarow
        /// </summary>
        /// <param name="rowOrg">Original Datarow</param>
        /// <param name="rowNew">New Datarow</param>
        /// <param name="stColNames">data column names to check</param>
        /// <param name="keys">keys</param>
        /// <returns>true --> Is different data</returns>
        private static bool IsDiff(DataRow rowOrg, DataRow rowNew, string[] stColNames, string[] keys)
        {
            bool diff = false;
            int cnt = stColNames.Length;
            for (int i = 0; i < cnt; i++)
            {
                try
                {
                    if (!(rowOrg[stColNames[i]].Equals(rowNew[stColNames[i]])))
                    {
                        diff = true;
                        break;
                    }
                }
                catch (Exception)
                {
                    diff = true;
                    break;
                }
            }
            return diff;
        }

        private static readonly char[] _KEY_SEPERATOR = { ',' };

        /// <summary>
        /// Is same key for given original and new data row
        /// </summary>
        /// <param name="rowOrg">Original datarow</param>
        /// <param name="rowNew">New Datarow</param>
        /// <param name="keys">Keys</param>
        /// <returns>true --> Same Key</returns>
        private static bool IsSameKey(DataRow rowOrg, DataRow rowNew, string[] keys)
        {
            bool sameKey = true;

            for (int i = 0; i < keys.Length; i++)
            {
                if (!(rowOrg[keys[i]].Equals(rowNew[keys[i]])))
                {
                    sameKey = false;
                    break;
                }
            }
            return sameKey;
        }


        public static DataSet LoadDataFromXmlString(string xmlData)
        {
            DataSet ds = new DataSet();
            if (!string.IsNullOrEmpty(xmlData))
            {
               try
               {
                  //Method 1
                  //--------
                  //XmlDocument xmlDoc = new XmlDocument();
                  //xmlDoc.LoadXml(xmlData);

                  //XmlNodeReader reader = new XmlNodeReader(xmlDoc);
                  //ds.ReadXml(reader);

                  XmlDocument xmlDoc = new XmlDocument();
                  xmlDoc.LoadXml(xmlData);

                  using (XmlNodeReader reader = new XmlNodeReader(xmlDoc))
                  {
                     ds.ReadXml(reader);
                  }                  

                  //Method 2
                  //--------
                  //using (StringReader sr = new StringReader(xmlData))
                  //{
                  //    //Create XmlReader and load reader from StringReader
                  //    using (XmlReader xmlReader = XmlReader.Create(sr))
                  //    {
                  //        ds.ReadXml(xmlReader);
                  //    }
                  //}
               }
               catch (Exception)
               {

                  ds = null;
               }
            }
            return ds;
        }

        public static void AssignData(string stData, DataRow row, string colName)
        {
           if ((row[colName] == null) || (row[colName] == DBNull.Value))
           {
              if ((stData == null) || (stData.Trim() == "")) { }
              else { row[colName] = stData; }
           }
           else
           {
              if (string.Compare((string)row[colName], stData) != 0) row[colName] = stData;
           }
        }

       /// <summary>
       /// Assign Data (if any difference) of rowSource to rowDestination. 
       /// Note: Both rows must be same DataTable type.
       /// </summary>
       /// <param name="rowSource"></param>
       /// <param name="rowDestination"></param>
       /// <param name="colCnt"></param>
       public static void AssignData(DataRow rowSource, DataRow rowDestination, int colCnt)
       {
          for (int i = 0; i < colCnt; i++)
          {
             if ( Convert.ToString( rowDestination[i]) != Convert.ToString( rowSource[i]))
             {
                rowDestination[i] = rowSource[i];
             }
          }
       }

       public static void AssignData(DataRow rowSource, DataRow rowDestination, List<string> lsColNameToCopyData)
       {
          int cnt = lsColNameToCopyData.Count;
          for (int i = 0; i < cnt; i++)
          {
			  string colName = lsColNameToCopyData[i];
             if (Convert.ToString(rowDestination[colName]) != Convert.ToString(rowSource[colName]))
             {
                rowDestination[colName] = rowSource[colName];
             }
          }
       }

       public static void AssignDateTimeData(string stDateTime, DataRow row, string colName)
        {
           if (string.IsNullOrEmpty(stDateTime))
           {
              if ((row[colName] == DBNull.Value) || (row[colName] == null)) { }
              else
              {
                 row[colName] = DBNull.Value;
              }
           }
           else
           {
              DateTime dt = UtilTime.ConvUfisTimeStringToDateTime(stDateTime);
              if ((row[colName] == DBNull.Value) || (row[colName] == null)) { row[colName] = dt; }
              else
              {
                 DateTime dt1 = (DateTime)row[colName];
                 if (dt1.CompareTo(dt) != 0) row[colName] = dt;
              }
           }
        }

		public static bool IsNullValue(DataRow row, string colName)
		{
			if (row.IsNull(colName)) return true;
			else if (row[colName] == null) return true;
			else if (Convert.ToString(row[colName]) == DBNull.Value.ToString()) return true;
			return false;
		}

		public static bool IsNullValue(DataRow row, string colName, DataRowVersion rowVersion)
		{
			DataColumn dCol = null;
			foreach (DataColumn col in row.Table.Columns)
			{
				if (col.ColumnName == colName)
				{
					dCol = col;
					break;
				}
			}
			if (dCol == null) return true;

			if (row.IsNull(dCol, rowVersion)) return true;
			else if (row[colName, rowVersion] == null) return true;
			else if (Convert.ToString(row[colName, rowVersion]) == DBNull.Value.ToString()) return true;
			return false;
		}

		public static bool IsNullOrEmptyValue(DataRow row, string colName)
		{
			if (IsNullValue(row, colName)) return true;
			else if (Convert.ToString(row[colName]).Trim() == "") return true;
			else return false;
		}

		public static bool IsNullOrEmptyValue(DataRow row, string colName, DataRowVersion rowVersion)
		{
			if (IsNullValue(row, colName, rowVersion)) return true;
			else if (Convert.ToString(row[colName, rowVersion]).Trim() == "") return true;
			else return false;
		}

		public static DateTime? GetDateTimeNullable(DataRow row, string colName)
		{
			DateTime? data = null;
			if (!IsNullValue(row, colName)) data = (DateTime?) row[colName];
			return data;
		}

		public static object GetNullValue()
		{
			return global::System.Convert.DBNull;
		}

		public static void SetNullValue(DataRow row, string colName)
		{
			row[colName] = GetNullValue();
		}

		public static void SetData(DataRow row, string colName, DateTime? data)
		{
			if (data.HasValue && !IsNullValue(row, colName))
			{//Both current and new value have data.
				if (data.Value.CompareTo((DateTime)row[colName]) != 0)
				{
					row[colName] = data.Value;
				}
			}
			else
			{
				row[colName] = data.HasValue ? data : GetNullValue();
			}
		}

		public static void SetData(DataRow row, string colName, string st)
		{
			if (st == null) row[colName] = GetNullValue();
			else row[colName] = st;
		}

		public static void SetData(DataRow row, string colName, int? data)
		{
			if (data.HasValue && !IsNullValue(row, colName))
			{//Both current and new value have data.
				if (data.Value!=((int)row[colName]))
				{
					row[colName] = data.Value;
				}
			}
			else
			{
				row[colName] = data.HasValue ? data : GetNullValue();
			}
		}

    }
}


