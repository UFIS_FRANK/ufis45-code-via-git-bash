using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Win32;

namespace MM.UtilLib
{
   public class UtilRegistry
   {
      //Most software packages will have a registry key inside
      //HKEY_LOCAL_MACHINE->SOFTWARE
      //These keys are available no matter who is logged in and is a good place to stick general application values. Let's put a key in this folder called "My Registry Key".      

      /// <summary>
      /// Set Data 'Value' in 'Key' under Registry HKEY_LOCAL_MACHINE->SOFTWARE->'registryName'
      /// </summary>
      /// <param name="registryName">Registry Name unser HKEY_LOCAL_MACHINE->SOFTWARE. Can create sub registry by using '/' (e.g. UFIS Airport Solutions/ITrek)</param>
      /// <param name="key">Key to create under the given registry name</param>
      /// <param name="value">Value</param>
      /// <param name="createRegistryFlag">Create the Registry if not found it. True - To create. False - Not to create and throw error</param>
      public static void SetData(string registryName, string key, string value, bool createRegistryFlag)
      {
         if (string.IsNullOrEmpty(registryName)) throw new ApplicationException("No registry information.");
         RegistryKey regKey = GetRegistry(registryName, true);
         if (regKey == null)
         {
            if (createRegistryFlag)
            {
               CreateRegistry(registryName);
               regKey = GetRegistry(registryName, true);
            }           
         }

         if (regKey == null)
         {
            throw new ApplicationException("No Registry found for " + registryName);
         }

         string st = (string)regKey.GetValue(key);
         regKey.SetValue(key, value);
         regKey.Close();        
      }

      /// <summary>
      /// Get Data for 'Key' under Registry HKEY_LOCAL_MACHINE->SOFTWARE->'registryName'
      /// </summary>
      /// <param name="registryName">Registry Name unser HKEY_LOCAL_MACHINE->SOFTWARE. 
      /// Can use sub registry by using '/' (e.g. UFIS Airport Solutions/ITrek)</param>
      /// <param name="key">Key to create under the given registry name</param>
      /// <returns>Value for the giving Key</returns>
      public static string GetData(string registryName, string key)
      {
         if (string.IsNullOrEmpty(registryName)) throw new ApplicationException("No registry information.");
         RegistryKey regKey = GetRegistry(registryName,false);
         object data = regKey.GetValue(key);
         regKey.Close();
         if (data != null)
         {
            return (string)data;
         }
         else return null;
      }

      /// <summary>
      /// Has Data for 'Key'
      /// </summary>
      /// <param name="registryName">Registry Name unser HKEY_LOCAL_MACHINE->SOFTWARE. 
      /// Can use sub registry by using '/' (e.g. UFIS Airport Solutions/ITrek)</param>
      /// <param name="key">Key to create under the given registry name</param>
      /// <param name="value">Value for the given key</param>
      /// <returns>Trye==>Has Data</returns>
      public static bool HasData(string registryName, string key, out string value)
      {
         if (string.IsNullOrEmpty(registryName)) throw new ApplicationException("No registry information.");
         bool has = false;
         value = null;
         RegistryKey regKey = GetRegistry(registryName, false);
         object data = regKey.GetValue(key);
         regKey.Close();
         if (data != null)
         {
            has = true;
            value = (string)data;
         }

         return has;
      }

      /// <summary>
      /// Get Registry Key for giving Registry Name
      /// </summary>
      /// <param name="registryName">Registry Name unser HKEY_LOCAL_MACHINE->SOFTWARE. 
      /// Can use sub registry by using '/' (e.g. UFIS Airport Solutions/ITrek)</param>
      /// <returns></returns>
      private static RegistryKey GetRegistry(String registryName, bool writable)
      {
         if (string.IsNullOrEmpty(registryName)) throw new ApplicationException("No registry information.");
         return Registry.LocalMachine.OpenSubKey("SOFTWARE\\" + registryName, writable);
      }

      /// <summary>
      /// Has the Registry for given RegistryName
      /// </summary>
      /// <param name="registryName">Registry Name unser HKEY_LOCAL_MACHINE->SOFTWARE. 
      /// Can use sub registry by using '/' (e.g. UFIS Airport Solutions/ITrek)</param>
      /// <returns></returns>
      public static bool HasRegistry(string registryName)
      {
         if (string.IsNullOrEmpty(registryName)) throw new ApplicationException("No registry information.");
         bool has = false;
         RegistryKey regKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\" + registryName);
         if (regKey != null) has = true;
         regKey.Close();
         return has;
      }

      /// <summary>
      /// Create the Registry for given RegistryName
      /// </summary>
      /// <param name="registryName">Registry Name unser HKEY_LOCAL_MACHINE->SOFTWARE. 
      /// Can use sub registry by using '/' (e.g. UFIS Airport Solutions/ITrek)</param>
      /// <returns></returns>
      public static void CreateRegistry(String registryName)
      {
         if (string.IsNullOrEmpty(registryName)) throw new ApplicationException("No registry information.");
         RegistryKey regKey = null;

         try
         {
            regKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\" + registryName);
         }
         catch (Exception ex)
         {
            throw new ApplicationException( "Registry [" + registryName + "] Error due to " + ex.Message ) ;
         }

         if (regKey == null) regKey = Registry.LocalMachine.CreateSubKey("SOFTWARE\\" + registryName);
         regKey.Close();
      }

      public static void RemoveRegistry(String registryName)
      {
         if (string.IsNullOrEmpty(registryName)) throw new ApplicationException("No registry information.");
         Registry.LocalMachine.DeleteSubKeyTree("SOFTWARE\\" + registryName);
      }
   }
}
