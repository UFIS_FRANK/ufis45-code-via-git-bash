using System;
using System.Collections.Generic;
using System.Text;

using IBM.WMQ;


namespace MM.UtilLib
{
    public class UtilWSMQ
    {

        private MQQueueManager _qManager;
        private MQQueue _q;
        private MQMessage _qMessage;
        private MQPutMessageOptions _qPutMessageOptions;
        private MQGetMessageOptions _qGetMessageOptions;

        private static string _qName;
        private static string _qMgrName;
        //static string ChannelInfo;

        private string _channelName;
        private string _transportType;
        private string _connectionName;

        private string _message = "";
        private byte[] _messageID;

        public UtilWSMQ(string qMgrName, string qName, string transportType,
            string channelName, string channelServer, string channelPort)
        {
            _qMgrName = qMgrName;//e.g. QDMSD00
            _qName = qName;//e.g. ITRE.ITREK.SOCC.MSG
            _transportType = transportType;//e.g. TCP
            _channelName = channelName;//e.g. QDMSD00.CLIENT.ITREK
            _connectionName = channelServer + channelPort;// e.g. GREEN(1414)
            _qManager = new MQQueueManager(_qMgrName, _channelName, _connectionName);
        }

        //public string GetMessageFromTheQueue(string qName, byte[] msgId)
        //{

        //    //_q = _qManager.AccessQueue(qName, MQC.MQOO_BROWSE );
        //    //if (_q.
        //    //MQMessage queueMessage = new MQMessage();
        //    //queueMessage.Format = MQC.MQFMT_STRING;
        //    //if (msgId != null)
        //    //    queueMessage.MessageId = msgId;

        //    //MQGetMessageOptions qGetMessageOptions = new MQGetMessageOptions();

        //    //try
        //    //{
        //    //    queue.Get(qMessage, qGetMessageOptions);
        //    //    // Received Message.
        //    //    //return "MessageID:" + MessageID + " Message:" + queueMessage.ReadString(queueMessage.MessageLength);
        //    //    queue.Close();
        //    //    queueManager.Close();
        //    //    return queueMessage.ReadString(queueMessage.MessageLength);
        //    //}
        //    //catch (Exception MQExp)
        //    //{
        //    //    //HACK: Need to stop using exception to control queue purge
        //    //    queue.Close();
        //    //    queueManager.Close();
        //    //    return MQExp.Message;
        //    //}
        //}
    }
}
