//#define USE_ORA_CONN

using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;

#if USE_ORA_CONN
#region USING Oracle ODP
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
#endregion
#else
#region USING Microsoft Oracle Library
using System.Data.OracleClient;
#endregion
#endif

using System.Data;
//using System.Data.Common;

namespace MM.UtilLib
{
	public class UtilOraDb : IUtilDb, IDisposable
	{
		#region Singleton
		private static UtilOraDb _this = null;
		private static object _lockObj = new object();
		private UtilOraDb()
		{
		}

		public static UtilOraDb GetInstance()
		{
			if (_this == null)
			{
				lock (_lockObj)
				{
					if (_this == null)
					{
						_this = new UtilOraDb();
					}
				}
			}
			return _this;
		}
		#endregion

		#region Dispose the object
		// Implement IDisposable.
		// Do not make this method virtual.
		// A derived class should not be able to override this method.
		public void Dispose()
		{
			Dispose(true);
			// This object will be cleaned up by the Dispose method.
			// Therefore, you should call GC.SupressFinalize to
			// take this object off the finalization queue 
			// and prevent finalization code for this object
			// from executing a second time.
			GC.SuppressFinalize(this);
		}

		// Track whether Dispose has been called.
		private bool disposed = false;

		// Dispose(bool disposing) executes in two distinct scenarios.
		// If disposing equals true, the method has been called directly
		// or indirectly by a user's code. Managed and unmanaged resources
		// can be disposed.
		// If disposing equals false, the method has been called by the 
		// runtime from inside the finalizer and you should not reference 
		// other objects. Only unmanaged resources can be disposed.
		private void Dispose(bool disposing)
		{
			if (!this.disposed)
			{
				lock (_lockObj)
				{
					// Check to see if Dispose has already been called.
					if (!this.disposed)
					{
						// If disposing equals true, dispose all managed 
						// and unmanaged resources.
						if (disposing)
						{
							// Dispose managed resources.
							//component.Dispose();
						}
					}
					disposed = true;
				}
			}
		}

		// Use C# destructor syntax for finalization code.
		// This destructor will run only if the Dispose method 
		// does not get called.
		// It gives your base class the opportunity to finalize.
		// Do not provide destructors in types derived from this class.
		~UtilOraDb()
		{
			// Do not re-create Dispose clean-up code here.
			// Calling Dispose(false) is optimal in terms of
			// readability and maintainability.
			Dispose(false);
		}
		#endregion

		#region public : DB Helper methods

		public static string PARA_NAME_PREFIX = ":";

		public static string FormatParamName(string nameToFormat)
		{
			return PARA_NAME_PREFIX + nameToFormat;
		}

		public static string GetSampleUatTnsConnString()
		{
			string connString = "";
			connString = @"(DESCRIPTION =
    (ADDRESS_LIST =
      (ADDRESS = (PROTOCOL = TCP)(HOST = eapsxd07)(PORT = 40062))
    )
    (CONNECT_DATA =
      (SID = SASIOC3)
      (SERVER = DEDICATED)
    )
  )";
			return connString;
		}

		public static string GetSampleProdTnsConnString()
		{
			string connString = "";
			connString = @"(DESCRIPTION =
    (ADDRESS_LIST =
      (ADDRESS = (PROTOCOL = TCP)(HOST = SASIOC1.sq.com.sg)(PORT = 40062))
    )
    (CONNECT_DATA =
      (SID = SASIOC1)
    )
  )";
			return connString;
		}

		public static string GetConnString(string stTnsConn,
		   string userId, string pwd, string proxyUserId)
		{
			return "DATA SOURCE=" + stTnsConn + ";Persist Security Info=true;User Id=" + userId + ";Password=" + pwd + ";";
		}

		public static string GetSampleConnString()
		{
			string connString = "";

#if USE_ORA_CONN
			#region using ODP
         connString = @"DATA SOURCE=GREEN11;PASSWORD=ceda;PERSIST SECURITY INFO=False;USER ID=CEDA";
         connString = @"DATA SOURCE=
         (DESCRIPTION =
             (ADDRESS_LIST =
               (ADDRESS = (PROTOCOL = TCP)(HOST = yellow)(PORT = 1521))
             )
             (CONNECT_DATA =
               (SERVICE_NAME = UFIS.yellow)
             )
           );
         Persist Security Info=true;User Id=ceda;Password=ceda;
         ";
		 #endregion
#else
			#region using Microsoft Oracle Connection Library
			connString = @"Server=
(DESCRIPTION =
    (ADDRESS_LIST =
      (ADDRESS = (PROTOCOL = TCP)(HOST = green)(PORT = 1521))
    )
    (CONNECT_DATA =
      (SERVICE_NAME = UFIS.green)
    )
  );
Persist Security Info=true;User Id=ceda;Password=ceda;
";

			connString = @"Server=
(DESCRIPTION =
    (ADDRESS_LIST =
      (ADDRESS = (PROTOCOL = TCP)(HOST = yellow)(PORT = 1521))
    )
    (CONNECT_DATA =
      (SERVICE_NAME = UFIS.yellow)
    )
  );
Persist Security Info=true;User Id=ceda;Password=ceda;
";
			#endregion
#endif

			return connString;
		}

		/// <summary>
		/// Get Data Adapter for given command
		/// </summary>
		/// <param name="cmd"></param>
		/// <returns></returns>
		public IDataAdapter GetDataAdapter(IDbCommand cmd)
		{
			return (IDataAdapter)new OracleDataAdapter((OracleCommand)cmd);
		}

		/// <summary>
		/// Get Database DataAdapter to generate Insert,Update,Delete Command.
		/// </summary>
		/// <returns></returns>
		public IDbDataAdapter GetDbDataAdapter()
		{
			return (IDbDataAdapter)new OracleDataAdapter();
		}

		/// <summary>
		/// Create Command Builder for given Data Adapter (which has SelectCommand with Select "CommandText", connection and transaction )
		/// </summary>
		/// <param name="da">Data Adapter to create 'Insert', 'Update', 'Delete' Command</param>
		public void CreateCommandBuilder(IDbDataAdapter da)
		{
			OracleDataAdapter da1 = (OracleDataAdapter)da;
			da1.FillSchema(new DataTable(), SchemaType.Source);
			using (OracleCommandBuilder cb = new OracleCommandBuilder(da1))
			{
				da1.InsertCommand = (OracleCommand)CloneCommand(cb.GetInsertCommand(), da.SelectCommand.Connection, da.SelectCommand.Transaction);
				da1.UpdateCommand = (OracleCommand)CloneCommand(cb.GetUpdateCommand(), da.SelectCommand.Connection, da.SelectCommand.Transaction);
				da1.DeleteCommand = (OracleCommand)CloneCommand(cb.GetDeleteCommand(), da.SelectCommand.Connection, da.SelectCommand.Transaction);
			}
		}

		private IDbCommand CloneCommand(IDbCommand srcCmd, IDbConnection conn, IDbTransaction trx)
		{
			if (srcCmd == null) throw new ApplicationException("Null Command to Clone.");
			IDbCommand cmd = GetCommand(conn, trx);

			cmd.CommandText = srcCmd.CommandText;
			cmd.CommandType = srcCmd.CommandType;
			cmd.Parameters.Clear();

			foreach (IDataParameter para in srcCmd.Parameters)
			{
				AddParameter(cmd, para);
			}
			cmd.UpdatedRowSource = srcCmd.UpdatedRowSource;
			return cmd;
		}

		public void AddParameter(IDbCommand cmd, IDataParameter para)
		{
			OracleParameter oPara = (OracleParameter)para;
			OracleParameter newPara = new OracleParameter();
			newPara.ParameterName = oPara.ParameterName;
			newPara.DbType = oPara.DbType;
			newPara.Direction = oPara.Direction;
			newPara.IsNullable = oPara.IsNullable;
			newPara.Offset = oPara.Offset;
			newPara.OracleType = oPara.OracleType;
			newPara.Size = oPara.Size;
			newPara.SourceColumn = oPara.SourceColumn;
			newPara.SourceColumnNullMapping = oPara.SourceColumnNullMapping;
			newPara.SourceVersion = oPara.SourceVersion;
			cmd.Parameters.Add(newPara);
		}

		public void Update(IDbDataAdapter da, DataTable dt)
		{
			((OracleDataAdapter)da).Update(dt);
		}

		/// <summary>
		/// Get Data Adapter for given command
		/// </summary>
		/// <param name="cmd"></param>
		/// <returns></returns>
		public IDbDataAdapter GetDbDataAdapter(IDbCommand cmd)
		{
			return (IDbDataAdapter)new OracleDataAdapter((OracleCommand)cmd);
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="da"></param>
		/// <returns></returns>
		public DbCommandBuilder GetDbCommandBuilder(IDbDataAdapter da)
		{
			return (DbCommandBuilder)new OracleCommandBuilder((OracleDataAdapter)da);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="ds"></param>
		/// <returns></returns>
		public int FillDataSet(IDbCommand cmd, DataSet ds)
		{
			if (ds == null) throw new ApplicationException("No DataTable");
			int cnt = -1;

			using (OracleDataAdapter da = new OracleDataAdapter((OracleCommand)cmd))
			{
				cnt = da.Fill(ds, ds.Tables[0].TableName);
			}
			return cnt;
		}

		public int FillDataTable(IDbCommand cmd, DataTable dt)
		{
			if (dt == null) throw new ApplicationException("No DataTable");
			int cnt = -1;

			using (OracleDataAdapter da = new OracleDataAdapter((OracleCommand)cmd))
			{
				cnt = da.Fill(dt);
			}
			return cnt;
		}

		public IDbCommand GetCommand(string connString)
		{
			DateTime dt = DateTime.Now;
			IDbConnection conn = GetConnection(connString, true);
			IDbCommand cmd = null;
			DateTime dt2 = DateTime.Now;

			try
			{
				cmd = conn.CreateCommand();
			}
			catch (Exception)
			{
				CloseConnection(conn);
			}
			DateTime dt3 = DateTime.Now;
			//LogMsg("Connection Open Time:" + ElapsedTime(dt, dt2) + Environment.NewLine +
			//   "Create Command Time:" + ElapsedTime(dt2, dt3));
			return cmd;
		}

		public IDbCommand GetCommandWithTrx(string connString)
		{
			//LogMsg("GetCommandWithTrx:Begin");
			IDbConnection conn = GetConnection(connString, true);
			//LogMsg("GetCommandWithTrx:After GetConnection");
			IDbCommand cmd = null;
			try
			{
				cmd = conn.CreateCommand();
				//LogMsg("GetCommandWithTrx:CreatedCommand");
				cmd.Transaction = conn.BeginTransaction(IsolationLevel.ReadCommitted);
				//LogMsg("GetCommandWithTrx:BeginTrx");
			}
			catch (Exception ex)
			{
				LogMsg("GetCommandWithTrx:Err:" + ex.Message);
				CloseConnection(conn);
				cmd = null;

				throw;
			}
			//LogMsg("GetCommandWithTrx:End:");
			return cmd;
		}

		public IDbCommand GetCommand(IDbConnection conn, IDbTransaction trx)
		{
			IDbCommand cmd = new OracleCommand();
			cmd.UpdatedRowSource = UpdateRowSource.None;
			cmd.Connection = conn;
			cmd.Transaction = trx;
			return cmd;
		}

		public IDbCommand GetCommandWithTrx(IDbConnection conn)
		{
			IDbCommand cmd = null;
			try
			{
				cmd = conn.CreateCommand();
				//LogMsg("GetCommandWithTrx:CreatedCommand");
				cmd.Transaction = conn.BeginTransaction(IsolationLevel.ReadCommitted);
				//LogMsg("GetCommandWithTrx:BeginTrx");
			}
			catch (Exception ex)
			{
				LogMsg("GetCommandWithTrx:Err:" + ex.Message);
				cmd = null;

				throw;
			}
			//LogMsg("GetCommandWithTrx:End:");
			return cmd;
		}

		public void CloseCommand(IDbCommand cmd, bool commit)
		{
			CloseCommand(cmd, commit, true);
		}

		/// <summary>
		/// Close Command. Commit if any transaction when commit==True.
		/// Close the connection when closeConnectionInd==True
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="commit"></param>
		/// <param name="closeConnectionInd"></param>
		public void CloseCommand(IDbCommand cmd, bool commit, bool closeConnectionInd)
		{
			if (cmd != null)
			{
				try
				{
					if (cmd.Transaction != null)
					{
						if (commit) cmd.Transaction.Commit();
						else cmd.Transaction.Rollback();
					}
				}
				catch (Exception ex)
				{
					LogMsg("CloseCommand:Err:" + ex.Message);
					throw;
				}
				finally
				{
					if (closeConnectionInd)
					{
						CloseConnection(cmd.Connection);
					}
					cmd.Dispose();
					cmd = null;
				}
			}
		}

		public void CommitTransaction(IDbCommand cmd, bool commit)
		{
			if (cmd != null)
			{
				try
				{
					if (cmd.Transaction != null)
					{
						if (commit) cmd.Transaction.Commit();
						else cmd.Transaction.Rollback();
					}
				}
				catch (Exception ex)
				{
					LogMsg("CloseCommand:Err:" + ex.Message);
					throw;
				}

			}
		}

		public void CancelCommand(IDbCommand cmd)
		{
			cmd.Cancel();
		}

		public IDbCommand AddParam(IDbCommand cmd, string paramName, object paramValue)
		{
			if (paramValue == null) paramValue = DBNull.Value;
			if (cmd.Parameters.Contains(paramName))
			{
				((OracleParameter)cmd.Parameters[paramName]).Value = paramValue;
			}
			else
			{
				OracleParameter op = ((OracleCommand)cmd).Parameters.Add(new OracleParameter(paramName, paramValue));
			}
			return cmd;
		}

		public IDbCommand AddParamDateTime(IDbCommand cmd, string paramName, object paramValue)
		{
			if (paramValue == null) paramValue = DBNull.Value;
			if (cmd.Parameters.Contains(paramName))
			{
				((OracleParameter)cmd.Parameters[paramName]).Value = paramValue;
			}
			else
			{
				OracleParameter op = ((OracleCommand)cmd).Parameters.Add(new OracleParameter(paramName, OracleType.DateTime));
				op.Value = paramValue;
			}
			return cmd;
		}



		#endregion

		#region Private Section
		//-------------------Private Section -Begin ----------------------
		private static void CloseConnection(IDbConnection conn)
		{
			if (conn != null)
			{
				try
				{
					try
					{
						if (conn.State != ConnectionState.Closed)
						{
							conn.Close();
						}
					}
					catch (Exception ex)
					{
						if ("ORA-03113,ORA-12541,ORA-12514".Contains(ex.Message))
						{
							try
							{
								ClearPool(conn);
							}
							catch { }
						}
						//throw;
					}
					finally
					{
						conn.Dispose();
						conn = null;
					}
				}
				catch (Exception ex)
				{
					LogMsg("CloseConnection:Err:" + ex.Message);
				}
			}
		}

		/// <summary>
		/// Empties the connection pool
		/// </summary>
		public static void ClearAllPools()
		{
			LogMsg("ClearAllPools");
			OracleConnection.ClearAllPools();
		}

		/// <summary>
		/// Empties the connection pool associated with the given connection.
		/// </summary>
		/// <param name="con"></param>
		public static void ClearPool(IDbConnection con)
		{
			LogMsg("ClearPool");
			OracleConnection.ClearPool((OracleConnection)con);
		}

		public IDbConnection GetConnection(string connString, bool openConnection)
		{
			DateTime dt = DateTime.Now;
			IDbConnection conn = null;
			//LogMsg("GetConnection:Begin.");
			try
			{
				conn = new OracleConnection();
				conn.ConnectionString = connString;

				if (openConnection)
				{
					try
					{
						conn.Open();
					}
					catch (Exception ex)
					{
						if ("ORA-03113,ORA-12541,ORA-12514".Contains(ex.Message))
						{
							try
							{
								ClearAllPools();
							}
							catch (Exception)
							{
							}
						}
						throw;
					}
				}

				//string timeTaken = "GetConnection:Time Taken:" + ElapsedTime(dt, DateTime.Now);
				//LogMsg(timeTaken);
			}
			catch (Exception)
			{
				if (conn != null) { CloseConnection(conn); conn = null; }
				throw;
			}
			//LogMsg("GetConnection:End.");
			return conn;
		}

		private static IDbCommand GetCommand(IDbConnection conn)
		{
			IDbCommand cmd = null;
			cmd = conn.CreateCommand();
			return cmd;
		}

		private static string ElapsedTime(DateTime startTime, DateTime endTime)
		{
			return MM.UtilLib.UtilTime.ElapsedTime(startTime, endTime);
		}

		private static void LogMsg(string msg)
		{
			try
			{
				MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("UtilOraDb", msg);
			}
			catch (Exception)
			{				
			}
		}

		//-------------------Private Section - End ----------------------
		#endregion
	}

}