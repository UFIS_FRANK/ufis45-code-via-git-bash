using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using System.Threading;
using MM.UtilLib;
using System.Messaging;

namespace MM.UtilLib
{
    public class UtilFileIO
    {
        /// <summary>
        /// Delete a folder
        /// </summary>
        /// <param name="fullFolderName">folder name to delete</param>
        public static void DeleteAFolder(string fullFolderName)
        {
            try
            {
                Directory.Delete(fullFolderName, true);
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Delete a file
        /// </summary>
        /// <param name="fullFileName">File Name to delete</param>
        public static void DeleteAFile(string fullFileName)
        {
            try
            {
                //FileInfo file = new FileInfo(fullFileName);
                //if (file.Exists)
                if (File.Exists(fullFileName))
                {
                    File.Delete(fullFileName);
                }
            }
            catch { }
        }

        /// <summary>
        /// Is File Exist
        /// </summary>
        /// <param name="fullFileName">Full File Name</param>
        /// <returns>true-Exist</returns>
        public static bool IsFileExist(string fullFileName)
        {
            bool exist = false;
            try
            {
                //FileInfo file = new FileInfo(fullFileName);
                //exist = (file.Exists);
                exist = File.Exists(fullFileName);
            }
            catch { }
            return exist;
        }

        /// <summary>
        /// Create a sub directory under the parent directory
        /// </summary>
        /// <param name="parentDir">parent Directory</param>
        /// <param name="newDirName">new directory name to create under the parent directory</param>
        public static void CreateSubDirectory(string parentDir, string newDirName)
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo(parentDir);
                di.CreateSubdirectory(newDirName);
            }
            catch { }
        }

        /// <summary>
        /// Create a file if it is not existed
        /// </summary>
        /// <param name="fullFileName">File Name to create</param>
        public static void CreateAFile(string fullFileName)
        {
            try
            {
                if (File.Exists(fullFileName))
                {
                    StreamWriter sw = File.CreateText(fullFileName);
                    sw.AutoFlush = true;
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.Message);
            }
        }

        private static TextWriter ErrorLog = null;
        private static object _lockTextFile = new object();
//        private static EventWaitHandle wh = new EventWaitHandle(false, EventResetMode.AutoReset,
//"UFIS.ITREK.LOG");
        private static Mutex _mt = null;
        const string MT_TRACELOG_NAME = "MT_TRACELOG_NAME";
        private static Mutex GetTraceLogMutex()
        {
            if (_mt == null)
            {
                try
                {
                    _mt = new Mutex(false, MT_TRACELOG_NAME);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("MutexCreate:Err:" + ex.Message);
                }

                if (_mt == null)
                {
                    try
                    {
                        _mt = Mutex.OpenExisting(MT_TRACELOG_NAME);
                    }
                    catch (WaitHandleCannotBeOpenedException ex)
                    {
                        Console.WriteLine("MutexOpenExisting:Err:" + ex.Message);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("MutexOpen:Err:" + ex.Message);
                    }
                }
                if (_mt == null) _mt = new Mutex(false, MT_TRACELOG_NAME);
            }
            return _mt;
        }

        private static bool _isMsMqExist = false;
        private static bool IsMsMqExist()
        {
            if (!_isMsMqExist)
            {
                _isMsMqExist = MessageQueue.Exists(@".\Private$\am-test");
                if (!_isMsMqExist)
                {
                    MessageQueue.Create(@".\Private$\am-test");
                    _isMsMqExist = MessageQueue.Exists(@".\Private$\am-test");
                }
            }
            return _isMsMqExist;
        }

        private static MessageQueue _qSend = null;

        /// <summary>
        /// Write to text file by appending the given string at the bottom of the file
        /// </summary>
        /// <param name="fullFileName">Full File Name to append the text</param>
        /// <param name="stToWrite">string to write</param>
        public static bool WriteToTextFile(string fullFileName, String stToWrite)
        {
            bool success = false;
            try
            {
               File.AppendAllText(fullFileName, stToWrite + Environment.NewLine);
               success = true;
            }
            catch (Exception ex)
            {
                //Console.WriteLine("TraceLogErr:" + ex.Message);
            }
            return success;
        }

        public static void WriteToTraceLog()
        {
            MessageQueue q = null;
            try
            {
                q = new MessageQueue(@".\Private$\am-test");
                ((XmlMessageFormatter)q.Formatter).TargetTypeNames = new string[] { "System.String" };

            }
            catch (Exception)
            {
            }

            if (q != null)
            {
                bool doAgain = true;
                int blankCount = 0;
                StringBuilder sb = new StringBuilder();
                
                do
                {
                    string fullFileName = @"C:\TEMP\shdata_1006.txt";
                    if (blankCount > 100) doAgain = false;
                    try
                    {
                        for (int i = 0; i < 100; i++)
                        {
                            Message msg = q.Receive(new TimeSpan(0, 0, 0, 0, 10));
                            if (msg == null) blankCount++;
                            //if ((fullFileName != "") && (fullFileName != msg.Label))
                            //{
                            //    File.AppendAllText(fullFileName, sb.ToString());
                            //    sb = new StringBuilder();
                            //}
                            //fullFileName = msg.Label;
                            sb.AppendLine(msg.Body.ToString()); 
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Ex:" + ex.Message);
                        blankCount++;
                    }
                    finally
                    {
                        if ((sb.Length > 0) && fullFileName!="")
                        {
                            File.AppendAllText(fullFileName, sb.ToString());
                            sb = new StringBuilder();
                        }
                    }

                } while (doAgain);

                //if (q != null) q.Close();
            }
        }

        /// <summary>
        /// Get the File Stream for given file name for reading lock
        /// </summary>
        /// <param name="fileName">File Name</param>
        /// <param name="retryCount">Retry Count in case of any error reading file</param>
        /// <returns>FileStream of given file</returns>
        public static FileStream GetFileStreamForReading(string fileName, int retryCount)
        {
            FileStream file = null;
            if (retryCount == -1) retryCount = 1000;
            else if (retryCount < 10) retryCount = 10;

            for (int i = 0; i < retryCount; i++)
            {
                try
                {
                    file = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read);
                    if (file != null) break;
                }
                catch (System.IO.IOException)
                {//Might be caused by file lock
                    Random rdm = new Random();
                    System.Threading.Thread.Sleep(rdm.Next(10, 50));
                }
                catch (Exception)
                {
                    throw;
                }
            }
            if (file == null) throw new ApplicationException("Unable to read " + UtilMisc.GetFileNameOnly(fileName));
            return file;
        }

        /// <summary>
        /// Get FileStream for file name for Writing lock
        /// </summary>
        /// <param name="fileName">File Name</param>
        /// <param name="retryCount">Retry Count in case of error opening file</param>
        /// <returns>FileStream</returns>
        public static FileStream GetFileStreamForWriting(string fileName, int retryCount)
        {
            FileStream file = null;
            if (retryCount == -1) retryCount = 1000;
            else if (retryCount < 50) retryCount = 50;

            for (int i = 0; i < retryCount; i++)
            {
                try
                {
                    file = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.None);
                    if (file != null)
                    {
                        break;
                    }
                }
                catch (System.IO.IOException)
                {
                    Random rdm = new Random();
                    System.Threading.Thread.Sleep(rdm.Next(2, 30));
                }
                catch (Exception ex)
                {
                    LogTraceMsg("GetFileStreamForWriting:" + UtilMisc.GetFileNameOnly(fileName) + ":Err:" + ex.StackTrace + "\n" + ex.ToString());
                    throw ex;
                }
            }
            return file;
        }

        /// <summary>
        /// Copy the file with locking the source file and target file
        /// </summary>
        /// <param name="srcFile">Source File</param>
        /// <param name="destFile">Destination File</param>
        /// <param name="overWrite">overwrite the destination file if it is already existed</param>
        /// <param name="isSrcFileExist">Is the Source File existed?</param>
        /// <returns>true - successfully copied. Else - Not success to copy</returns>
        public static bool CopyFileWithLock(string srcFile, string destFile, bool overWrite, out bool isSrcFileExist)
        {
            bool copied = false;
            isSrcFileExist = File.Exists(srcFile);

            if (!string.IsNullOrEmpty(srcFile) && isSrcFileExist)
            {
                using (FileStream fsSrc = GetFileStreamForReading(srcFile, 10))
                {
                    if (fsSrc != null)
                    {
                        long fileLength = fsSrc.Length;
                        try
                        {
                            fsSrc.Lock(0, fileLength);
                            using (FileStream fsDest = GetFileStreamForWriting(destFile, 100))
                            {
                                if (fsDest != null)
                                {
                                    try
                                    {
                                        for (long i = 0; i < fileLength; i++)
                                        {
                                            fsDest.WriteByte((byte)fsSrc.ReadByte());
                                        }
                                        copied = true;
                                    }
                                    catch (Exception)
                                    {
                                    }
                                    finally
                                    {

                                        try
                                        {
                                            fsDest.Dispose();
                                            fsDest.Close();
                                        }
                                        catch (Exception)
                                        {
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            string st = ex.Message;
                            LogTraceMsg(ex.Message);
                        }
                        finally
                        {
                            try
                            {
                                fsSrc.Unlock(0, fileLength);
                            }
                            catch (Exception ex)
                            {
                                LogTraceMsg(ex.Message);
                            }
                            fsSrc.Dispose();
                            fsSrc.Close();
                        }
                    }
                }
            }
            return copied;
        }

        /// <summary>
        /// Log to trace File
        /// </summary>
        /// <param name="msg">message to log</param>
        private static void LogTraceMsg(string msg)
        {
            //UtilLog.LogToTraceFile("UtilFileIO", msg);
        }

    }
}
