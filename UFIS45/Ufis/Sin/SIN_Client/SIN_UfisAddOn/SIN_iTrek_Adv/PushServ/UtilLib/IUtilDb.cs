using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;

using System.Data;

namespace MM.UtilLib
{
	public interface IUtilDb
	{
		/// <summary>
		/// Get Command for given connection string
		/// </summary>
		/// <param name="connString"></param>
		/// <returns></returns>
		IDbCommand GetCommand(string connString);

		/// <summary>
		/// Get Command with transaction for given connection string
		/// </summary>
		/// <param name="connString"></param>
		/// <returns></returns>
		IDbCommand GetCommandWithTrx(string connString);

		/// <summary>
		/// Get Command for given connection and transacton
		/// </summary>
		/// <param name="conn"></param>
		/// <param name="trx"></param>
		/// <returns></returns>
		IDbCommand GetCommand(IDbConnection conn, IDbTransaction trx);

		/// <summary>
		/// Get Command for given connection with transaction
		/// </summary>
		/// <param name="conn"></param>
		/// <returns></returns>
		IDbCommand GetCommandWithTrx(IDbConnection conn);

		/// <summary>
		/// Close Command. It will commit accordingly. It will close and dispose the connection as well.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="commit"></param>
		void CloseCommand(IDbCommand cmd, bool commit);

		/// <summary>
		/// Close command without closing the connection.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="commit"></param>
		void CommitTransaction(IDbCommand cmd, bool commit);

		/// <summary>
		/// Close Command. Commit if any transaction when commit==True.
		/// Close the connection when closeConnectionInd==True
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="commit"></param>
		/// <param name="closeConnectionInd"></param>
		void CloseCommand(IDbCommand cmd, bool commit, bool closeConnectionInd);

		/// <summary>
		/// Get Connection for given Connection string. Connection will be opened according to parameter 'openCnnection'
		/// </summary>
		/// <param name="connString"></param>
		/// <param name="openConnection"></param>
		/// <returns></returns>
		IDbConnection GetConnection(string connString, bool openConnection);

		/// <summary>
		/// Get Data Adapter for given Command. Note: command text and parameters must be already assigned before calling this method.
		/// </summary>
		/// <param name="cmd"></param>
		/// <returns></returns>
		IDataAdapter GetDataAdapter(IDbCommand cmd);

		/// <summary>
		/// Get Data Adapter for given Command. Note: command text and parameters must be already assigned before calling this method.
		/// </summary>
		/// <param name="cmd"></param>
		/// <returns></returns>
		IDbDataAdapter GetDbDataAdapter(IDbCommand cmd);
		IDbDataAdapter GetDbDataAdapter();
		/// <summary>
		/// 
		/// </summary>
		/// <param name="cmd"></param>
		/// <returns></returns>
		DbCommandBuilder GetDbCommandBuilder(IDbDataAdapter da);
		void CreateCommandBuilder(IDbDataAdapter da);

		void Update(IDbDataAdapter da, DataTable dt);
		void AddParameter(IDbCommand cmd, IDataParameter para);

		/// <summary>
		/// Fill Data Set "ds" for given command "cmd". Note: command text and parameters must be already assigned before calling this method.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="ds"></param>
		/// <returns></returns>
		int FillDataSet(IDbCommand cmd, DataSet ds);

		/// <summary>
		/// Fill Data Table "dt" for given command "cmd". Note: command text and parameters must be already assigned before calling this method.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="dt"></param>
		/// <returns></returns>
		int FillDataTable(IDbCommand cmd, DataTable dt);

		/// <summary>
		/// Add Parameter to the command.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="paramName"></param>
		/// <param name="paramValue"></param>
		/// <returns></returns>
		IDbCommand AddParam(IDbCommand cmd, string paramName, object paramValue);

		/// <summary>
		/// Add DateTime Parameter to the command.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="paramName"></param>
		/// <param name="paramValue"></param>
		/// <returns></returns>
		IDbCommand AddParamDateTime(IDbCommand cmd, string paramName, object paramValue);
	}
}