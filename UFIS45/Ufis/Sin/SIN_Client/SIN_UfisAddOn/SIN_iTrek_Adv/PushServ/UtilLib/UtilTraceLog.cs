using System;
using System.Collections.Generic;
using System.Text;

namespace MM.UtilLib
{
    public class UtilTraceLog
    {
        private static UtilTraceLog _this = null;
        private bool _traceOn = false;
        private static object _lockObj = new object();
        private const string DEFAULT_LOG_DIR = @"C:\TEMP\";
        private const string DEFAULT_LOG_FNAME = "Trc";
        private static string _logDir = DEFAULT_LOG_DIR;
        private static string _logFileName = DEFAULT_LOG_FNAME;
        private static string _logSource = "Unk";

        public static UtilTraceLog GetInstance()
        {
            if (_this == null)
            {
                lock (_lockObj)
                {
                    if (_this == null)
                    {
                        _this = new UtilTraceLog();
                    }
                }
            }
            return _this;
        }

       /// <summary>
       /// Set Directory of Trace Log File (set only one time at application start)
       /// </summary>
       /// <param name="logDir">Log Directory</param>
        public void SetLogDir(string logDir)
        {
            if (string.IsNullOrEmpty(logDir)) _logDir = DEFAULT_LOG_DIR;
            else if (logDir.Trim() == "") _logDir = DEFAULT_LOG_DIR;
            else _logDir = logDir;
        }

       /// <summary>
        /// Set File Name of Trace Log File  (set only one time at application start)
       /// </summary>
       /// <param name="logFileName">Log File Name</param>
        public void SetLogFileName(string logFileName)
        {
            if (string.IsNullOrEmpty(logFileName)) _logFileName = DEFAULT_LOG_FNAME;
            else if (logFileName.Trim() == "") _logFileName = DEFAULT_LOG_FNAME;
            else _logFileName = logFileName;
        }

        //public static void SetLogSource(string logSource)
        //{
        //    if (string.IsNullOrEmpty(logSource)) _logSource = "Unk";
        //    else _logSource = logSource;
        //}

       /// <summary>
       /// Set the Trace ON/OFF
       /// </summary>
       /// <param name="traceOn">True==> Trace ON, Otherwise ==> Trace OFF</param>
        public void SetTrace(bool traceOn)
        {
            _traceOn = traceOn;
        }

        /// <summary>
        /// Log the message to trace file
        /// Trace file need to be defined by 'SetLogDir' and 'SetLogFileName'
        /// and need to turn on the trace by 'SetTrace'
        /// </summary>
        /// <param name="msg">message to log</param>
        public void LogTraceMsg(string msg)
        {
            if (_traceOn)
            {
                MMUtilLog.LogToTraceFile(_logDir, _logFileName, _logSource, msg);
            }
        }

       /// <summary>
       /// Log the message for given Log Source to trace file.
        /// Trace file need to be defined by 'SetLogDir' and 'SetLogFileName'
        /// and need to turn on the trace by 'SetTrace'
       /// </summary>
       /// <param name="logSource"></param>
       /// <param name="msg"></param>
        public void LogTraceMsg(string logSource, string msg)
        {
            if (_traceOn)
            {
                MMUtilLog.LogToTraceFile(_logDir, _logFileName, logSource, msg);
            }
        }
    }
}
