//#define TestsOn
using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Globalization;
using System.IO;
using System.Diagnostics;
using iTrekXML;
using System.Xml;
#if TestsOn
using NUnit.Framework;
#endif

#if TestsOn
namespace iTrekUtilsTests
{
    [TestFixture]
    public class iTrekUtilsTests
    {
        public iTrekUtilsTests()
        {

        }

         [Test]
        public void convertStandardDateTimeToOracleFormatTest()
        {
            Assert.AreEqual("20060927124019", iTrekUtils.iTUtils.convertStandardDateTimeToOracleFormat("27/09/2006 12:40:19"));
        }
    }
}
#endif

namespace iTrekUtils
{
    /// <summary>
    /// iTrek utilities
    /// </summary>
    public class iTUtils
    {
        public static int GetRandomNumber(int iMin, int iMax)
        {
            Random rand = new Random();
            try
            {
                return rand.Next(iMin, iMax);
            }
            finally
            {
                rand = null;
            }
        }

        //TODO: Refactor - assumes standard date format
        public static string GetShortTimeFromDateTimeStr(string DateTimeStr)
        {
            
            string ShortTimeStr = DateTimeStr;
            if (ShortTimeStr != "")
            {
                ShortTimeStr = ShortTimeStr.Remove(0, 11);
                ShortTimeStr = ShortTimeStr.Remove(5, 3);
            }
            return ShortTimeStr;
        }

        public static void WriteXmlDocToFile(XmlDocument xmlDoc, string filePath)
        {
            XmlTextWriter myWriter = new XmlTextWriter(filePath, Encoding.UTF8);
            // Indented for easy reading
            myWriter.Formatting = Formatting.Indented;
            // Write the file to it's directory
            xmlDoc.WriteTo(myWriter);
            myWriter.Close();
        }

        //TODO: Create a test
        public static string ConvertDateFromOracleFormat(string dateStr)
        {
            //Eg. From 20060927124019
            //To "27/09/2006 12:40:19"
            string ConvertedString = "";
            if (dateStr != "")
            {
                string Year = dateStr.Substring(0, 4);
                string Month = dateStr.Substring(4, 2);
                string Day = dateStr.Substring(6, 2);
                string Hour = dateStr.Substring(8, 2);
                string Minute = dateStr.Substring(10, 2);
                string Seconds = dateStr.Substring(12, 2);
                ConvertedString = Day;
                ConvertedString += @"/" + Month;
                ConvertedString += @"/" + Year;
                ConvertedString += @" " + Hour;
                ConvertedString += @":" + Minute;
                ConvertedString += @":" + Seconds;
            }
            return ConvertedString;
        }


        public static string convertStandardDateTimeToOracleFormat(string standardDatetime)
        {//Eg. From  "27/09/2006 12:40:19"
            //To      20060927124019
            string ConvertedString = "";
            if (standardDatetime != "")
            {
                string Year = standardDatetime.Substring(6, 4);
                string Month = standardDatetime.Substring(3, 2);
                string Day = standardDatetime.Substring(0, 2);
                string Hour = standardDatetime.Substring(11, 2);
                string Minute = standardDatetime.Substring(14, 2);
                string Seconds = standardDatetime.Substring(17, 2);
                ConvertedString += Year;
                ConvertedString += Month;
                ConvertedString += Day;
                ConvertedString += Hour;
                ConvertedString += Minute;
                ConvertedString += Seconds;
            }
            return ConvertedString;
        }

        

        public static string ReadFromFile(string strFileName)
        {
            try
            {
                System.IO.TextReader reader;
                reader = new System.IO.StreamReader(strFileName);
                string sreaderData = reader.ReadToEnd();
                reader.Close();
                return sreaderData;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string GetMachineName()
        {
            return System.Environment.MachineName;
        }


        public static string ConvertStreamToString(System.IO.Stream theStream)
        {
            System.IO.StreamReader srReader;
            srReader = new System.IO.StreamReader(theStream, System.Text.Encoding.ASCII);
            return srReader.ReadToEnd();
        }

        public static byte[] ConvertStringToByteArray(string stringToConvert)
        {
            return (new UnicodeEncoding()).GetBytes(stringToConvert);
            

        }

        public static string FromUnicodeByteArrayToString(byte[] characters)
        {
            string constructedString = "";
            try
            {
                UnicodeEncoding encoding = new UnicodeEncoding();
                constructedString = encoding.GetString(characters);
            }
            catch (Exception ex)
            {
                LogToEventLog("exception message"+ex.Message, "iTrekError"); 
               
            }
           
            return constructedString;
        }

        public static void LogToEventLog(string strMessage)
        {
            try
            {
                string strLogSrc = "iTrekGeneric";
                string strLogName = "iTrekGenericLogs";
                System.Diagnostics.EventLog EventLog1 = new System.Diagnostics.EventLog();
                if (!(EventLog.SourceExists(strLogSrc)))
                {
                    EventLog.CreateEventSource(strLogSrc, strLogName);
                }
                EventLog1.Source = strLogSrc;
                EventLog1.Log = strLogName;
                EventLog1.WriteEntry(strMessage + "(Time:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + ":" + DateTime.Now.Millisecond.ToString() + ")");
            }
            catch
            {
            }
        }

        public static void LogToEventLog(string strMessage, string eventLogName)
        {
            try
            {
                string strLogSrc = eventLogName;
                string strLogName = eventLogName + "Logs";
                System.Diagnostics.EventLog EventLog1 = new System.Diagnostics.EventLog();
                if (!(EventLog.SourceExists(strLogSrc)))
                {
                    EventLog.CreateEventSource(strLogSrc, strLogName);
                }
                EventLog1.Source = strLogSrc;
                EventLog1.Log = strLogName;
                EventLog1.WriteEntry(strMessage + "(Time:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + ":" + DateTime.Now.Millisecond.ToString() + ")");
            }
            catch
            {
                LogToEventLog("Exception caught while creating new log");

            }
        }

        public static DateTime ConvUfisTimeStringToDateTime(string st)
        {
            DateTime dt;
            if (!IsValidUfisTimeString(st, out dt))
            {
                throw new ApplicationException("Invalid Date and Time format");
            }
            return dt;
        }

        public static bool IsValidUfisTimeString(string st, out DateTime dt)
        {
            bool valid = false;
            dt = new DateTime(1, 1, 1);
            if (st == "") { valid = true; }
            else if (st.Length == 14)
            {
                try
                {
                    if (Convert.ToInt64(st).ToString() == st)
                    {
                        dt = new DateTime(Convert.ToInt16(st.Substring(0, 4)),
                            Convert.ToInt16(st.Substring(4, 2)),
                            Convert.ToInt16(st.Substring(6, 2)),
                            Convert.ToInt16(st.Substring(8, 2)),
                            Convert.ToInt16(st.Substring(10, 2)),
                            Convert.ToInt16(st.Substring(12, 2)));
                        valid = true;
                    }
                }
                catch (Exception)
                {
                    //throw;
                }
            }
            return valid;
        }


        public static string ConvDateTimeToUfisFullDTString(DateTime dt)
        {
            string st = String.Format("{0:yyyyMMddHHmmss}", dt);
            return st;
        }


        public static string removeBadCharacters(string message)
        {
            if ((message!=null) && (message != ""))
            {
                message = message.Replace("'", "");
                message = message.Replace("&", "");
                message = message.Replace("<", "");
            }            
            return message;

        }
 public static string RemoveTroublesomeCharacters(string inString)
        {
           //if (inString == null) return null;

           // StringBuilder newString = new StringBuilder();
           // char ch;
           // if ((inString != null) && (inString != ""))
           // {
           //     inString = inString.Replace("'", "");
           //     inString = inString.Replace("&", "");
           //     inString = inString.Replace("<", "");
           // }            
           // for (int i = 0; i < inString.Length; i++)
           // {

           //     ch = inString[i];
           //     // remove any characters outside the valid UTF-8 range as well as all control characters
           //     // except tabs and new lines
           //     if ((ch < 0x00FD && ch > 0x001F) || ch == '\t' || ch == '\n' || ch == '\r')
           //     {
           //         newString.Append(ch);
           //     }
           // }
           // return newString.ToString();
            return inString;
        }

    }
}