//#define TestsOn
#define LoggingOn
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Xml;
using System.Xml.XPath;
using System.IO;
#if TestsOn
using NUnit.Framework;
#endif
using iTrekXML;
using System.Collections;
using System.Globalization;
using System.Threading;
using iTrekUtils;
using iTrekSessionMgr;
//using UFIS.iTrekLib.DS;

#if TestsOn
namespace iTrekXMLTests
{
    [TestFixture]
    public class iTXMLclTests
    {
        iTrekXML.iTXMLcl iTXML;
        iTXMLPathscl iTPaths;

        //[SetUp]
        public iTXMLclTests()
        {
            iTPaths = new iTXMLPathscl();
            iTXML = new iTXMLcl();
        }

        
        //TODO: Coverage high
        //Test not testing for midnight changeover issue
        [Test]
        public void ProcessSubmittedTimeTest()
        {
            string processedTime = iTXML.ProcessSubmittedTime("23:55", true);
            string Day = "";
                Day = DateTime.Now.AddDays(-1.0).Day.ToString() + "/";
            string Month = DateTime.Now.Month.ToString() + "/";
            string Year = DateTime.Now.Year.ToString() + " ";
            string DayMonthYear = Day + Month + Year;
            Assert.AreEqual(DayMonthYear + "23:55:00", processedTime);
            processedTime = iTXML.ProcessSubmittedTime("23:55", false);
            Day = DateTime.Now.Day.ToString() + "/";
            DayMonthYear = Day + Month + Year;
            Assert.AreEqual(DayMonthYear + "23:55:00", processedTime);
        }

        //TODO: Coverage medium
        //create two device entries to test
        //that will delete them all for a 
        //given staffid
        [Test]
        public void GetDeviceIDForSessionTest()
        {
            //TODO: Code for these assumptions
            //Assumes relevant flight data available
            //Assumes relevant jobs data available
            //We must at least have a device id
            string nonexistentSessionId = "abcxbz23qk4vbs45sfvqhabc";

            iTXML.WriteStaffSessionandDeviceIDXmlToDeviceIDFile(iTPaths.GetTestSessionId(), iTPaths.GetTestSID(), iTPaths.GetTestPENO());
            iTXML.WriteStaffSessionandDeviceIDXmlToDeviceIDFile(iTPaths.GetTestSessionId(), iTPaths.GetTestSID(), iTPaths.GetTestPENO());
            Assert.AreEqual("", iTXML.GetDeviceIDBySession(nonexistentSessionId));
            Assert.AreEqual("SID-0000178198", iTXML.GetDeviceIDBySession(iTPaths.GetTestSessionId()));
            //Tear down and test
            //iTXML.DeleteDeviceIDEntryBySessID(iTPaths.GetTestSessionId());
            iTXML.DeleteDeviceIDEntryByDeviceID(iTPaths.GetTestSID());
            //test using GetDeviceIDByStaffID when have re-factored move to iTXML
            //Assert.AreEqual("", iTXML.g(nonexistentSessionId));
        }

        //TODO: Coverage medium - continue to refactor
        [Test]
        public void FlightObjectCompleteTest()
        {
            //we use ReadElementsForArrayListFromCurrentFlightXMLFile
            //because it uses the Flight obj (and therefore tests it)
            ArrayList al = iTXML.ReadElementsForArrayListFromCurrentFlightXMLFile("//FLIGHTS/FLIGHT", iTPaths.GetFlightsTestFilePath());
            foreach (Flight fl in al)
            {
                Assert.AreEqual(10, fl.URNO.Length);
                Assert.IsTrue(fl.FLNO.Length > 0 && fl.FLNO.Length < 9);
                Assert.AreEqual(3, fl.DES3.Length);
                Assert.AreEqual(1, fl.ADID.Length);
            }
        }


        //TODO: Coverage medium
        //uses static PENO value in main Jobs.xml file
        [Test]
        public void UsePENOToGetMatchingFlightsFromMainJobsXMLFileTest()
        {
            ArrayList al = iTXML.UsePENOToGetMatchingFlightsFromMainJobsXMLFile("//JOBS/JOB", "00223344", iTPaths.GetJobsTestFilePath());

            Assert.AreEqual(20, al.Count);
            Assert.AreEqual("1234893873", al[0].ToString());
            Assert.AreEqual("1215015183", al[19].ToString());
        }

        //TODO: Coverage medium - continue to refactor
        [Test]
        public void UserSessionReadElementsForArrayListFromCurrentFlightXMLFileTest()
        {
            //setup
            XmlDocument xmlDoc = new XmlDocument();
            //load test file, but write to Flights.xml file
            xmlDoc.Load(iTPaths.GetXMLPath() + @"Test\UserSessionFlightsTest.xml");
            iTUtils.WriteXmlDocToFile(xmlDoc, iTPaths.GetXMLPath() + iTPaths.GetTestSessionId() + @"\FlightsTest.xml");
            //test
            ArrayList al = iTXML.UserSessionReadElementsForArrayListFromCurrentFlightXMLFile(iTPaths.GetTestSessionId(), "//FLIGHTS/FLIGHT", "FlightsTest.xml");
            foreach (Flight fl in al)
            {
                Assert.AreEqual(10, fl.URNO.Length);
                Assert.IsTrue(fl.FLNO.Length > 0 && fl.FLNO.Length < 9);
                Assert.AreEqual(3, fl.DES3.Length);
                Assert.AreEqual(1, fl.ADID.Length);
            }
            //Teardown
            iTXML.DeleteSessionFile(iTPaths.GetTestSessionId(), "FlightsTest.xml");
        }



        //TODO: Coverage medium
        [Test]
        public void PENOExistTest()
        {
            iTXML.WriteStaffSessionandDeviceIDXmlToDeviceIDFile(iTPaths.GetTestSessionId(), iTPaths.GetTestSID(), iTPaths.GetTestPENO());
            Assert.IsTrue(iTXML.PENOExist(iTPaths.GetTestPENO()));
            iTXML.DeleteDeviceIDEntryByDeviceID(iTPaths.GetTestSID());
            Assert.IsFalse(iTXML.PENOExist(iTPaths.GetTestPENO()));
        }

        //TODO: Coverage medium
        //Just compares 2 file sizes to ensure slight increase
        //for added element
        [Test]
        public void WriteStaffSessionandDeviceIDXmlToDeviceIDFileTest()
        {
            long origVal, newVal;
            //FileInfo fi = new FileInfo(iTPaths.GetXMLPath() + "DeviceID.xml");
            FileInfo fi = new FileInfo(iTPaths.GetDeviceIDFile());
            origVal = fi.Length;
            iTXML.WriteStaffSessionandDeviceIDXmlToDeviceIDFile(iTPaths.GetTestSessionId(), iTPaths.GetTestSID(), iTPaths.GetTestPENO());
            FileInfo fi2 = new FileInfo(iTPaths.GetDeviceIDFile());
            newVal = fi2.Length;
            Assert.AreNotEqual((decimal)origVal, (decimal)newVal);
            iTXML.DeleteDeviceIDEntryByDeviceID(iTPaths.GetTestSID());
        }

        //TODO: Complete this test Coverage medium
        [Test]
        public void DeleteDeviceIDEntryBySessIDTest()
        {
            iTXML.WriteStaffSessionandDeviceIDXmlToDeviceIDFile(iTPaths.GetTestSessionId(), iTPaths.GetTestSID(), iTPaths.GetTestPENO());
            iTXML.DeleteDeviceIDEntryBySessID(iTPaths.GetTestSessionId());
            Assert.AreEqual("", iTXML.GetDeviceIDBySession(iTPaths.GetTestSessionId()));
        }

        //TODO: Coverage high
        [Test]
        public void DeleteDeviceIDEntryByStaffIdTest()
        {
            iTXML.WriteStaffSessionandDeviceIDXmlToDeviceIDFile(iTPaths.GetTestSessionId(), iTPaths.GetTestSID(), iTPaths.GetTestPENO());
            //add some extra entrie to ensure that they all get deleted
            iTXML.WriteStaffSessionandDeviceIDXmlToDeviceIDFile("1234567890", "SID-0000178888", iTPaths.GetTestPENO());
            iTXML.WriteStaffSessionandDeviceIDXmlToDeviceIDFile("1234567777", "SID-0000179999", iTPaths.GetTestPENO());
            iTXML.DeleteDeviceIDEntryByStaffId(iTPaths.GetTestPENO());
            Assert.AreEqual("", iTXML.GetDeviceIDBySession(iTPaths.GetTestSessionId()));
            Assert.AreEqual("", iTXML.GetDeviceIDBySession("1234567890"));
            Assert.AreEqual("", iTXML.GetDeviceIDBySession("1234567777"));
        }

        //TODO: Coverage medium
        [Test]
        public void GetSessionIdFromMsgTest()
        {
            //TODO: This code will need to be made more generic
            string responseMessage = "<AUTH><AUTHENTICATED>TRUE</AUTHENTICATED><SESSIONID>" + iTPaths.GetTestSessionId() + "</SESSIONID></AUTH>";
            Assert.AreEqual(iTPaths.GetTestSessionId(), iTXML.GetSessionIdFromMsg(responseMessage));
        }

        //TODO: Coverage medium
        [Test]
        public void GetUAFTFromMsgTest()
        {
            //TODO: This code will need to be made more generic
            string responseMessage = "<OK2L><ULDNUMBER>AKE123456CX</ULDNUMBER><UAFT>1234567899</UAFT></OK2L>";
            Assert.AreEqual("1234567899", iTXML.GetUAFTFromMsg(responseMessage));
        }


        //TODO: Coverage medium
        [Test]
        public void GetFlightMsgActionFromMsg()
        {
            //Code only looks at root node
            string message = @"<FLIGHTS><FLIGHT><FLNO>SQ020</FLNO></FLIGHT></FLIGHTS>";
            Assert.AreEqual("FLIGHTS", iTXML.GetFlightMsgActionFromMsg(message));
            string message2 = @"<FLIGHT><URNO>1234916054</URNO><ACTION>INSERT</ACTION><FLNO>VF 532</FLNO><STOD /><STOA>20060927125000</STOA><REGN /><DES3>SIN</DES3><ORG3>SUB</ORG3><ETDI /><ETOA /><GTA1 /><GTD1 /><ADID>D</ADID><PSTA /><PSTD /></FLIGHT>";
            Assert.AreEqual("INSERT", iTXML.GetFlightMsgActionFromMsg(message2));
        }

        //TODO: Coverage medium
        //Nb. test depends on realistic xml - change to generic xml from file
        [Test]
        public void OK2LULDNoTest()
        {
            string message = @"<OK2L><ULDNUMBER>AKE123456CX</ULDNUMBER><UAFT>1234567899</UAFT></OK2L>";
            Assert.AreEqual("AKE123456CX", iTXML.OK2LULDNo(message));
        }

        //TODO: Coverage high
        //This test incorporates delete test
        [Test]
        public void WriteNewFlightToXMLFileTest()
        {
            //Setup
            string message = @"<FLIGHT><URNO>" + iTPaths.GetTestFlightURNO() + "</URNO><ACTION>INSERT</ACTION><FLNO>QF 011</FLNO><STOD>20060927042000</STOD><STOA>20060927175500</STOA><REGN></REGN><DES3>SIN</DES3><ORG3>LHR</ORG3><ETDI></ETDI><ETOA></ETOA><GTA1></GTA1><GTD1></GTD1><ADID>D</ADID><PSTA></PSTA><PSTD></PSTD></FLIGHT>";
            message = iTXML.DeleteActionFromFlightXML(message);
            XmlDocument doc = new XmlDocument();
            doc.Load(iTPaths.GetFlightsTestFilePath());
            doc = iTXML.WriteNewFlightToXMLFile(doc, message);
            doc.Save(iTPaths.GetFlightsTestFilePath());

            //Test
            Assert.AreEqual("QF 011", iTXML.SelectSingleNodeFromFlightsXML("//FLIGHTS/FLIGHT[URNO='" + iTPaths.GetTestFlightURNO() + "']/FLNO"));
            //Tear down
            string deleteMessage = @"<FLIGHT><URNO>" + iTPaths.GetTestFlightURNO() + "</URNO><ACTION>DELETE</ACTION></FLIGHT>";
            doc = iTXML.DeleteFlightFromXMLFile(doc, deleteMessage);
            doc.Save(iTPaths.GetFlightsTestFilePath());
            //Test tear down
            Assert.AreNotEqual("QF 011", iTXML.SelectSingleNodeFromFlightsXML("//FLIGHTS/FLIGHT[URNO='" + iTPaths.GetTestFlightURNO() + "']/FLNO"));
        }


        //TODO: Coverage high
        //This test incorporates delete test
        [Test]
        public void DeleteETDIFromFlightsXMLFileTest()
        {
            //Setup - important element is the ETDI setting
            string CurrentDateTime = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            CurrentDateTime = iTrekUtils.iTUtils.convertStandardDateTimeToOracleFormat(CurrentDateTime);
            string message = @"<FLIGHT><URNO>" + iTPaths.GetTestFlightURNO() + "</URNO><ACTION>INSERT</ACTION><FLNO>QF 011</FLNO><STOD>20060927042000</STOD><STOA>20060927175500</STOA><REGN></REGN><DES3>SIN</DES3><ORG3>LHR</ORG3><ETDI>" + CurrentDateTime + "</ETDI><ETOA></ETOA><GTA1></GTA1><GTD1></GTD1><ADID>D</ADID><PSTA></PSTA><PSTD></PSTD></FLIGHT>";
            message = iTXML.DeleteActionFromFlightXML(message);
            XmlDocument doc = new XmlDocument();
            doc.Load(iTPaths.GetFlightsTestFilePath());
            doc = iTXML.WriteNewFlightToXMLFile(doc, message);
            doc.Save(iTPaths.GetFlightsTestFilePath());

            //Test
            Assert.AreEqual(CurrentDateTime, iTXML.SelectSingleNodeFromFlightsXML("//FLIGHTS/FLIGHT[URNO='" + iTPaths.GetTestFlightURNO() + "']/ETDI"));
            //Save twice to GetFlightsTestFilePath for testing purposes (in reality save to updateablefligthsfile)
            doc = iTXML.DeleteETDIFromFlightsXMLFile(doc, iTPaths.GetTestFlightURNO(), iTPaths.GetFlightsTestFilePath(), iTPaths.GetFlightsTestFilePath());
            Assert.AreNotEqual(CurrentDateTime, iTXML.SelectSingleNodeFromFlightsXML("//FLIGHTS/FLIGHT[URNO='" + iTPaths.GetTestFlightURNO() + "']/ETDI"));


            //Tear down
            string deleteMessage = @"<FLIGHT><URNO>" + iTPaths.GetTestFlightURNO() + "</URNO><ACTION>DELETE</ACTION></FLIGHT>";
            doc = iTXML.DeleteFlightFromXMLFile(doc, deleteMessage);
            doc.Save(iTPaths.GetFlightsTestFilePath());
            //Test tear down
            Assert.AreNotEqual("QF 011", iTXML.SelectSingleNodeFromFlightsXML("//FLIGHTS/FLIGHT[URNO='" + iTPaths.GetTestFlightURNO() + "']/FLNO"));
        }

        //TODO: Coverage high
        [Test]
        public void UpdateFlightInXMLFileTest()
        {
            //Setup
            string message = @"<FLIGHT><URNO>" + iTPaths.GetTestFlightURNO() + "</URNO><ACTION>UPDATE</ACTION><FLNO>QF 011</FLNO><STOD>20060927042000</STOD><STOA>20060927175500</STOA><REGN></REGN><DES3>SIN</DES3><ORG3>LHR</ORG3><ETDI></ETDI><ETOA></ETOA><GTA1></GTA1><GTD1></GTD1><ADID>D</ADID><PSTA></PSTA><PSTD></PSTD></FLIGHT>";
            message = iTXML.DeleteActionFromFlightXML(message);
            XmlDocument doc = new XmlDocument();
            doc.Load(iTPaths.GetFlightsTestFilePath());
            doc = iTXML.WriteNewFlightToXMLFile(doc, message);
            doc.Save(iTPaths.GetFlightsTestFilePath());

            //Test
            //doc is the main flight xml file
            //message is the 
            string messageWithUpdates = @"<FLIGHT><URNO>" + iTPaths.GetTestFlightURNO() + "</URNO><ACTION>UPDATE</ACTION><FLNO>QF 022</FLNO><STOD>20060927042111</STOD><STOA>20060927175522</STOA><REGN></REGN><DES3>SIN</DES3><ORG3>BKK</ORG3><ETDI></ETDI><ETOA></ETOA><GTA1></GTA1><GTD1></GTD1><ADID>D</ADID><PSTA></PSTA><PSTD></PSTD></FLIGHT>";
            doc = iTXML.UpdateFlightInXMLFile(doc, messageWithUpdates);
            doc.Save(iTPaths.GetFlightsTestFilePath());
            Assert.AreEqual("QF 022", iTXML.SelectSingleNodeFromFlightsXML("//FLIGHTS/FLIGHT[URNO='" + iTPaths.GetTestFlightURNO() + "']/FLNO"));
            Assert.AreEqual("20060927042111", iTXML.SelectSingleNodeFromFlightsXML("//FLIGHTS/FLIGHT[URNO='" + iTPaths.GetTestFlightURNO() + "']/STOD"));
            Assert.AreEqual("20060927175522", iTXML.SelectSingleNodeFromFlightsXML("//FLIGHTS/FLIGHT[URNO='" + iTPaths.GetTestFlightURNO() + "']/STOA"));
            Assert.AreEqual("BKK", iTXML.SelectSingleNodeFromFlightsXML("//FLIGHTS/FLIGHT[URNO='" + iTPaths.GetTestFlightURNO() + "']/ORG3"));
            //Tear down
            string deleteMessage = @"<FLIGHT><URNO>" + iTPaths.GetTestFlightURNO() + "</URNO><ACTION>DELETE</ACTION></FLIGHT>";
            doc = iTXML.DeleteFlightFromXMLFile(doc, deleteMessage);
            doc.Save(iTPaths.GetFlightsTestFilePath());
            //Test tear down
            Assert.AreNotEqual("QF 022", iTXML.SelectSingleNodeFromFlightsXML("//FLIGHTS/FLIGHT[URNO='" + iTPaths.GetTestFlightURNO() + "']/FLNO"));
        }

        //TODO: Coverage high
        [Test]
        public void UpdateJobInXMLFileTest()
        {
            //Setup
            string message = @"<JOB><URNO>" + iTPaths.GetTestJobURNO() + "</URNO><ACTION>INSERT</ACTION><PENO>" + iTPaths.GetTestPENO() + "</PENO><UAFT>" + iTPaths.GetTestUAFT() + "</UAFT></JOB>";
            message = iTXML.DeleteActionFromJobXMLFile(message);
            XmlDocument doc = new XmlDocument();
            doc.Load(iTPaths.GetJobsTestFilePath());
            doc = iTXML.WriteNewJobToXMLFile(doc, message);
            doc.Save(iTPaths.GetJobsTestFilePath());

            //Test
            //doc is the main Job xml file
            //message is the 
            string messageWithUpdates = @"<JOB><URNO>" + iTPaths.GetTestJobURNO() + "</URNO><ACTION>UPDATE</ACTION><PENO>" + iTPaths.GetTestPENO() + "</PENO><UAFT>" + iTPaths.GetTestUAFT() + "</UAFT></JOB>";
            doc = iTXML.UpdateJobInXMLFile(doc, messageWithUpdates);
            doc.Save(iTPaths.GetJobsTestFilePath());
            Assert.AreEqual(iTPaths.GetTestPENO(), iTXML.SelectSingleNodeFromJobsXML("//JOBS/JOB[URNO='" + iTPaths.GetTestJobURNO() + "']/PENO", iTPaths.GetJobsTestFilePath()));
            Assert.AreEqual(iTPaths.GetTestUAFT(), iTXML.SelectSingleNodeFromJobsXML("//JOBS/JOB[URNO='" + iTPaths.GetTestJobURNO() + "']/UAFT", iTPaths.GetJobsTestFilePath()));
            //Tear down
            string deleteMessage = @"<JOB><URNO>" + iTPaths.GetTestJobURNO() + "</URNO><ACTION>DELETE</ACTION></JOB>";
            doc = iTXML.DeleteJobFromXMLFile(doc, deleteMessage);
            doc.Save(iTPaths.GetJobsTestFilePath());
            //Test tear down
            Assert.AreNotEqual(iTPaths.GetTestPENO(), iTXML.SelectSingleNodeFromJobsXML("//JOBS/JOB[URNO='" + iTPaths.GetTestJobURNO() + "']/PENO", iTPaths.GetJobsTestFilePath()));
        }

        //TODO: Coverage high
        //This test incorporates delete test
        [Test]
        public void WriteNewJobToXMLFileTest()
        {
            //Setup
            string message = @"<JOB><URNO>" + iTPaths.GetTestJobURNO() + "</URNO><ACTION>INSERT</ACTION><PENO>" + iTPaths.GetTestPENO() + "</PENO><UAFT>" + iTPaths.GetTestUAFT() + "</UAFT></JOB>";
            message = iTXML.DeleteActionFromJobXMLFile(message);
            XmlDocument doc = new XmlDocument();
            doc.Load(iTPaths.GetJobsTestFilePath());
            doc = iTXML.WriteNewJobToXMLFile(doc, message);
            doc.Save(iTPaths.GetJobsTestFilePath());
            //Test
            Assert.AreEqual(iTPaths.GetTestPENO(), iTXML.GetPENOByUAFTFromJobsXML(iTPaths.GetTestUAFT(), iTPaths.GetJobsTestFilePath()));
            //Tear down
            string deleteMessage = @"<JOB><URNO>" + iTPaths.GetTestJobURNO() + "</URNO><ACTION>DELETE</ACTION></JOB>";
            doc = iTXML.DeleteJobFromXMLFile(doc, deleteMessage);
            doc.Save(iTPaths.GetJobsTestFilePath());
            //Test Tear down
            Assert.AreNotEqual(iTPaths.GetTestPENO(), iTXML.GetPENOByUAFTFromJobsXML(iTPaths.GetTestUAFT(), iTPaths.GetJobsTestFilePath()));
        }

        //TODO: Coverage medium
        //add and remove data to make this test more dynamic
        [Test]
        public void SelectSingleNodeFromFlightsXMLTest()
        {
            Assert.AreEqual("QF 010", iTXML.SelectSingleNodeFromFlightsXML("//FLIGHTS/FLIGHT[URNO='1233798753']/FLNO"));
        }


        //TODO: Coverage low
        //add and remove data to make this test more dynamic
        [Test]
        public void GetMsgTypeFromMsgTest()
        {
            string message = @"<AUTH><AUTHENTICATED>TRUE</AUTHENTICATED><SESSIONID>" + iTPaths.GetTestSessionId() + "</SESSIONID><FNAME>John</FNAME><LNAME>Smith</LNAME></AUTH>";
            Assert.AreEqual("AUTH", iTXML.GetMsgTypeFromMsg(message));
            message = @"<SHOWDLS><URNO>1234567890</URNO><SESSIONID>dt2xbz23qk4vbs45sfvqhljh</SESSIONID><DLS>aaaaaaaaaaaaaaaaaaaaaaa   aaaaaaaaaaaaaaaaaaaaaaaaaaaa   aaaaaaaaaaaa</DLS></SHOWDLS>";
            Assert.AreEqual("SHOWDLS", iTXML.GetMsgTypeFromMsg(message));
            message = @"<ALLOK2L><URNO>1234567890</URNO><SESSIONID>dt2xbz23qk4vbs45sfvqhljh</SESSIONID><ULD1>AKE22960SQ-SIN-620-B</ULD1></ALLOK2L>";
            Assert.AreEqual("ALLOK2L", iTXML.GetMsgTypeFromMsg(message));
        }

        //TODO: Coverage low
        //Re-enable test at later date
        //[Test]
        public void RemoveBadCharsFromSHOWDLSTest()
        {
            string condensedDLSWithBadChars = "<SHOWDLS><URNO>1214060084</URNO><SESSIONID>dt2xbz23qk4vbs45sfvqhljh</SESSIONID><DLS>QU SISQ .SIQ 327 /1/FF/CG/DL FLT/S2/CT  AGO/MENT SQ0802/10OCT.SIN C LDDIN DIKR - SI: PGY ULD-SIEK-PLBSQ-PLQ PN PEK-BT4 BT SRY-B44 </DLS></SHOWDLS>";

            //string condensedDLSWithBadChars = "<SHOWDLS><URNO>1214060084</URNO><SESSIONID>dt2xbz23qk4vbs45sfvqhljh</SESSIONID><DLS>QU SISQ .SIQ 327 /1/FF/CG/DL FLT/S2/CT  AGO/MENT SQ0802/10OCT.SIN C LDDIN DIKR - SI: PGY ULD-SIEK-PLBSQ-PLQ PN PEK-BT4 BT SRY-B44 </DLS></SHOWDLS>";
            //Assert.AreEqual("<SHOWDLS><URNO>1214060084</URNO><SESSIONID>dt2xbz23qk4vbs45sfvqhljh</SESSIONID><DLS>QU SISQ .SIQ 327 -1-FF-CG-DL FLT-S2-CT  AGO-MENT SQ0802-10OCT.SIN C LDDIN DIKR - SI: PGY ULD-SIEK-PLBSQ-PLQ PN PEK-BT4 BT SRY-B44 </DLS></SHOWDLS>", iTXML.RemoveBadCharsFromSHOWDLS(condensedDLSWithBadChars));
            //Assert.AreEqual("<SHOWDLS><URNO>1214060084</URNO><SESSIONID>dt2xbz23qk4vbs45sfvqhljh</SESSIONID><DLS>QUSISQ.SIQ327-1-FF-CG-DL FLT-S2-CTAGO-MENTSQ0802-10OCT.SINCLDDINDIKR-SI:PGYULD-SIEK-PLBSQ-PLQPNPEK-BT4BTSRY-B44</DLS></SHOWDLS>", iTXML.RemoveBadCharsFromSHOWDLS(condensedDLSWithBadChars));

            Assert.AreEqual("<SHOWDLS><URNO>1214060084</URNO><SESSIONID>dt2xbz23qk4vbs45sfvqhljh</SESSIONID><DLS>QU SISQ -SIQ 327 -1-FF-CG-DL FLT-S2-CT  AGO-MENT SQ0802-10OCT SIN C LDDIN DIKR - SI  PGY ULD-SIEK-PLBSQ-PLQ PN PEK-BT4 BT SRY-B44 </DLS></SHOWDLS>", iTXML.RemoveBadCharsFromSHOWDLS(condensedDLSWithBadChars));

        }

        //TODO: Coverage medium
        [Test]
        public void ConvertDateFromOracleFormatTest()
        {
            string message = "20060927124019";
            //Flight fl = new Flight();
            Assert.AreEqual("27/09/2006 12:40:19", iTUtils.ConvertDateFromOracleFormat(message));
        }

        //TODO: Coverage medium
        [Test]
        public void DeleteActionFromFlightXMLTest()
        {
            string messageBefore = @"<FLIGHT><URNO>9999999999</URNO><ACTION>UPDATE</ACTION><FLNO>QF 011</FLNO><STOD>20060927042000</STOD><STOA>20060927175500</STOA><REGN></REGN><DES3>SIN</DES3><ORG3>LHR</ORG3><ETDI></ETDI><ETOA></ETOA><GTA1></GTA1><GTD1></GTD1><ADID>D</ADID><PSTA></PSTA><PSTD></PSTD></FLIGHT>";
            string messageAfter = @"<FLIGHT><URNO>9999999999</URNO><FLNO>QF 011</FLNO><STOD>20060927042000</STOD><STOA>20060927175500</STOA><REGN></REGN><DES3>SIN</DES3><ORG3>LHR</ORG3><ETDI></ETDI><ETOA></ETOA><GTA1></GTA1><GTD1></GTD1><ADID>D</ADID><PSTA></PSTA><PSTD></PSTD></FLIGHT>";
            Assert.AreEqual(messageAfter, iTXML.DeleteActionFromFlightXML(messageBefore));
        }
    }
}
#endif
namespace iTrekXML
{
    //TODO: Move Flight class?
    public class Flight : IComparable
    {
        //  Private  Fields
        private String _URNO;
        private String _FLNO;
        private String _STOD;
        private String _STOA;
        private String _REGN;
        private String _DES3;
        private String _ORG3;
        private String _ETDI;
        private String _ETOA;
        private String _GTA1;
        private String _GTD1;
        private String _ADID;
        private String _PSTA;
        private String _PSTD;
        private String _BLT1;
        private String _OFBL;
        private String _ONBL;
        private String _TGA1;
        private String _TGD1;
        private String _TTYP;
        private String _ACT3;
        private String _TURN;
        private String _UAFT;

        private String _DisplayOpenFlights;
        private String _DisplayOpenFlightsMenu;
        private String _DisplayFlightForBO;
        private String _DisplayFlightForBOMenu;
        private bool _etdStatus = false;
        private bool _lastDoorStatus = false;
        private String _flightDate;

        //defaultDateTime used to manage
        //blank times
        //string defaultDateTime = "20060101124019";

        //DateTime dtSTOD = new DateTime();
        DateTime dtSTOA = new DateTime();
        //DateTime dtETDI = new DateTime();
        DateTime dtETOA = new DateTime();
        string posToDisplay = "";


        //  Constructors
        public Flight()
        {

        }

        public Flight(string URNO, string FLNO, string STOD, string STOA, string REGN, string DES3, string ORG3, string ETDI, string ETOA, string GTA1, string GTD1, string ADID, string PSTA, string PSTD, string BLT1, string OFBL, string ONBL, string TGA1, string TGD1, string TTYP, string ACT3,string TURN,string UAFT)
        {
            this._URNO = URNO;
            this._FLNO = FLNO;
            this._STOD = STOD;
            this._STOA = STOA;
            this._REGN = REGN;
            this._DES3 = DES3;
            this._ORG3 = ORG3;
            this._ETDI = ETDI;
            this._ETOA = ETOA;
            this._GTA1 = GTA1;
            this._GTD1 = GTD1;
            this._ADID = ADID;
            this._PSTA = PSTA;
            this._PSTD = PSTD;
            this._BLT1 = BLT1;
            this._OFBL = OFBL;
            this._ONBL = ONBL;
            this._TGA1 = TGA1;
            this._TGD1 = TGD1;
            this._TTYP = TTYP;
            this._ACT3 = ACT3;
            this._TURN = TURN;
            this._UAFT = UAFT;
            String flDatestoa = STOA;
            string flDatestod = STOD;
            string flDateetoa = ETOA;
            string flDateetdi = ETDI;


            //Determine if departure or arrival flight
            if (TURN != "" && ADID=="A")
            {
                _ADID = "T";

                if (PSTA != "")
                {
                    posToDisplay = PSTA;
                }
                else
                {
                    posToDisplay = GTA1;

                }
                STOA = iTUtils.GetShortTimeFromDateTimeStr(iTUtils.ConvertDateFromOracleFormat(STOA));
                ETOA = iTUtils.GetShortTimeFromDateTimeStr(iTUtils.ConvertDateFromOracleFormat(ETOA));
                STOD = iTUtils.GetShortTimeFromDateTimeStr(iTUtils.ConvertDateFromOracleFormat(STOD));
                ETDI = iTUtils.GetShortTimeFromDateTimeStr(iTUtils.ConvertDateFromOracleFormat(ETDI));
                this._DisplayOpenFlightsMenu = FLNO +"-"+ REGN +"-"+ posToDisplay +"-"+ BLT1 +"-"+ ORG3 + "-STA" + STOA + "-" +  "ETA" +ETOA +"-"+DES3+"-STD"+STOD+"-ETD"+ETDI;
                
                if (ETOA != "")
                {
                    this._flightDate = flDateetoa;
                    this._DisplayOpenFlights = FLNO + "-T-" + ETOA + "-" + posToDisplay + "-" + BLT1  +"*";
                    
                }
                else
                {
                    this._flightDate = flDatestoa;
                    this._DisplayOpenFlights = FLNO + "-T-" + STOA + "-" + posToDisplay + "-" + BLT1;
                    
                }
            }
            else

            if (ADID == "A")
            {
                //if (STOA == "")
                //    STOA = defaultDateTime;
                //if (ETOA == "")
                //    ETOA = defaultDateTime;
                if (PSTA != "")
                {
                    posToDisplay = PSTA;
                }
                else
                {
                    posToDisplay = GTA1;

                }
                STOA = iTUtils.GetShortTimeFromDateTimeStr(iTUtils.ConvertDateFromOracleFormat(STOA));
                ETOA = iTUtils.GetShortTimeFromDateTimeStr(iTUtils.ConvertDateFromOracleFormat(ETOA));
                ONBL = iTUtils.GetShortTimeFromDateTimeStr(iTUtils.ConvertDateFromOracleFormat(ONBL));

                this._DisplayOpenFlightsMenu = FLNO + "-STA" + STOA + "-</br>" + REGN + "-" + ORG3 + "-" + "-<b>ETA" + " " + ETOA + " </b>-" + posToDisplay + "-" + BLT1;
                this._DisplayFlightForBOMenu = FLNO + "-STA" + STOA + "-ATA" + ONBL + "-" + posToDisplay + "-" + BLT1;
                if (ETOA != "")
                {
                    this._flightDate = flDateetoa;
                    this._DisplayOpenFlights = FLNO + "-A-" + ETOA + "-" + posToDisplay + "-" + BLT1 +"*";
                    this._DisplayFlightForBO = FLNO + "-" + ETOA + "-" + ONBL + "-" + posToDisplay + "-" + BLT1 + "*";
                }
                else
                {
                    this._flightDate = flDatestoa;
                    this._DisplayOpenFlights = FLNO + "-A-" + STOA + "-" + posToDisplay + "-" + BLT1;
                    this._DisplayFlightForBO = FLNO + "-" + STOA + "-" + ONBL + "-" + posToDisplay + "-" + BLT1;
                }



            }
            else if(ADID=="D")
            {
                //if (STOD == "")
                //    STOD = defaultDateTime;
                //if (ETDI == "")
                //    ETDI = defaultDateTime;
                //dtSTOD = Convert.ToDateTime(iTUtils.ConvertDateFromOracleFormat(STOD));
                //dtETDI = Convert.ToDateTime(iTUtils.ConvertDateFromOracleFormat(ETDI));
                //this._DisplayOpenFlights = FLNO + "-D-" + dtSTOD.ToShortTimeString() + "-" + GTD1;
                //this._DisplayOpenFlightsMenu = FLNO + "-STD" + dtSTOD.ToShortTimeString() + " " + REGN + " " + DES3 + " " + "<b>ETD</b>" + " " + dtETDI.ToShortTimeString() + " " + GTD1;

                //dtSTOD = Convert.ToDateTime(iTUtils.ConvertDateFromOracleFormat(STOD));
                STOD = iTUtils.GetShortTimeFromDateTimeStr(iTUtils.ConvertDateFromOracleFormat(STOD));
                //dtETDI = Convert.ToDateTime(iTUtils.ConvertDateFromOracleFormat(ETDI));
                ETDI = iTUtils.GetShortTimeFromDateTimeStr(iTUtils.ConvertDateFromOracleFormat(ETDI));
                /* Added by Alphy for Changing ETD to STD if ETD is available*/
                /* Added by Alphy to diplay gate if no pstd is available*/

                if (PSTD != "")
                {
                    posToDisplay = PSTD;
                }
                else
                {
                    posToDisplay = GTD1;

                }
                if (ETDI != "")
                {
                    this._DisplayOpenFlights = FLNO + "-D-" + ETDI + "-" + posToDisplay + "*"; ;
                    this._DisplayFlightForBO = FLNO + "-" + ETDI + "-" + posToDisplay + "*";
                    this._etdStatus = true;
                    this._flightDate = flDateetdi;

                }
                else
                {
                    this._DisplayOpenFlights = FLNO + "-D-" + STOD + "-" + posToDisplay;
                    this._DisplayFlightForBO = FLNO + "-" + STOD + "-" + posToDisplay;
                    this._flightDate = flDatestod;
                }
                this._DisplayOpenFlightsMenu = FLNO + "-STD" + STOD + "-" + REGN + "-" + DES3 + "-" + "<b>ETD</b>" + ETDI + "-" + posToDisplay;
                this._DisplayFlightForBOMenu = FLNO + "-STD" + STOD + "-ETD" + ETDI + "-" + posToDisplay;
                //Alphys changes:
                //this._DisplayOpenFlights = FLNO + "-D-" + dtSTOD.ToShortTimeString() + "-" + EQPS;
                //this._DisplayOpenFlightsMenu = FLNO + "-STD" + dtSTOD.ToShortTimeString() + "-" + REGN + "-" + DES3 + "-" + "ETD" + dtETDI.ToShortTimeString() + "-" + EQPS;
            }
        }

        public bool etdStatus { get { return _etdStatus; } }
        public bool lastDoorStatus { get { return _lastDoorStatus; } }

        //  Public  Properties
        public string URNO { get { return _URNO; } }
        public string FLNO { get { return _FLNO; } }
        public string STOD { get { return _STOD; } }
        public string STOA { get { return _STOA; } }
        public string REGN { get { return _REGN; } }
        public string DES3 { get { return _DES3; } }
        public string ORG3 { get { return _ORG3; } }
        public string ETDI { get { return _ETDI; } }
        public string ETOA { get { return _ETOA; } }
        public string GTA1 { get { return _GTA1; } }
        public string GTD1 { get { return _GTD1; } }
        public string ADID { get { return _ADID; } set { _ADID = value; } }
        public string PSTA { get { return _PSTA; } }
        public string PSTD { get { return _PSTD; } }
        public string BLT1 { get { return _BLT1; } }
        public string OFBL { get { return _OFBL; } }
        public string ONBL { get { return _ONBL; } }
        public string TGA1 { get { return _TGA1; } }
        public string TGD1 { get { return _TGD1; } }
        public string TTYP { get { return _TTYP; } }
        public string ACT3 { get { return _ACT3; } }
        public string TURN { get { return _TURN; } }
        public string FLIGHTDATE { get { return _flightDate; } }
        public string UAFT { get { return _UAFT; } }
        public string DISPLAYOPENFLIGHTS { get { return _DisplayOpenFlights; } }
        public string DISPLAYFLIGHTSMENU { get { return _DisplayOpenFlightsMenu; } }
        public string DISPLAYBOFLIGHT { get { return _DisplayFlightForBO; } set { _DisplayFlightForBO = value; } }
        public string DISPLAYBOFLIGHTMENU { get { return _DisplayFlightForBOMenu; } set { _DisplayFlightForBOMenu = value; _lastDoorStatus = true; } }

        //Comparator to sort flights according to date
        public int CompareTo(object obj)
        {
            int result = 0;
            Flight Compare = (Flight)obj;
            result = string.Compare(this.FLIGHTDATE, Compare.FLIGHTDATE);

            return result;

        }


    }

   
    /// <summary>
    /// iTrek XML manipulations
    /// </summary>
    public class iTXMLcl
    {
        iTXMLPathscl iTPaths;
        private static Object _lockObj = new Object();
        XmlDocument _myXmlDocument=new XmlDocument();
        public iTXMLcl(string AppDomain)
        {
            iTPaths = new iTXMLPathscl();
        }

        public iTXMLcl()
        {
            iTPaths = new iTXMLPathscl();
        }


        public XmlDocument TransformStringToXmlDoc(string xmlString)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlString);
            return xmlDoc;
        }

        //convert the hour:minute submitted time standard DateTime
        public string ProcessSubmittedTime(string selectedTime, bool TimeFromPreviousDay)
        {
            int selectedTimeHour = Convert.ToInt32(selectedTime.Remove(2));
            int selectedTimeMinute = Convert.ToInt32(selectedTime.Substring(3));
            DateTime dtATDSubmit = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, selectedTimeHour, selectedTimeMinute, 00);

            //for one hour after midnight the application
            //will handle time entries relating to the previous
            //day - after the one hour extension period the users
            //will receive the standard - 'unable to process later
            //times than current time' err msg
            //if (IsMidnightTimeChange(dtATDSubmit))
            if (TimeFromPreviousDay)
                dtATDSubmit = dtATDSubmit.AddDays(-1.0);

            return dtATDSubmit.ToString("dd/MM/yyyy HH:mm:ss");
        }

        //move to tests?
        public string SelectSingleNodeFromFlightsXML(string expression)
        {
            try
            {
                //string fileName = iTPaths.GetFlightsFilePath();
                //XPathDocument doc = new XPathDocument(fileName);
                XPathDocument doc = new XPathDocument(iTPaths.GetFlightsTestFilePath());
                XPathNavigator nav = doc.CreateNavigator();
                return nav.SelectSingleNode(expression).ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string SelectSingleNodeFromJobsXML(string expression, string jobsFilePath)
        {
            try
            {
                //string fileName = iTPaths.GetJobsFilePath();
                //XPathDocument doc = new XPathDocument(fileName);
                XPathDocument doc = new XPathDocument(jobsFilePath);
                XPathNavigator nav = doc.CreateNavigator();
                return nav.SelectSingleNode(expression).ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string GetPENOByUAFTFromJobsXML(string UAFT, string jobsFilePath)
        {
            try
            {
                string expression = "//JOBS/JOB[UAFT=" + UAFT + "]/PENO";
                //string fileName = iTPaths.GetJobsFilePath();
                //XPathDocument doc = new XPathDocument(fileName);
                XPathDocument doc = new XPathDocument(jobsFilePath);
                XPathNavigator nav = doc.CreateNavigator();
                return nav.SelectSingleNode(expression).ToString();

            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        /* Added by Alphy to getPenos for pushing OK2LOAD Message*/

        public ArrayList GetPENOSByUAFTFromJobsXML(string UAFT, string jobsFilePath)
        {
            ArrayList penoList = new ArrayList();
            try
            {

                string expression = "//JOBS/JOB[UAFT=" + UAFT + "]/PENO";
                //string fileName = iTPaths.GetJobsFilePath();
                //XPathDocument doc = new XPathDocument(fileName);                
                //XPathDocument doc = new XPathDocument(jobsFilePath);
                XPathDocument doc = LoadJobForAlert(jobsFilePath);
                XPathNavigator nav = doc.CreateNavigator();
                //ArrayList penoList = new ArrayList();
                //int cnt=nav.Select(expression).Count;
                XPathNodeIterator iter = nav.Select(expression);
                penoList.Clear();
                while (iter.MoveNext())
                {
                    penoList.Add(iter.Current.Value);

                }
            }
            catch (Exception )
            {
                //return e.Message;
            }
            return penoList;
        }

        //TODO: Used?
        public string GetStaffIDElmt(string expression)
        {
            string fileName = iTPaths.GetXMLPath() + "StaffID.xml";
            XPathDocument doc = new XPathDocument(fileName);
            XPathNavigator nav = doc.CreateNavigator();
            return nav.SelectSingleNode(expression).ToString();
        }

        //TODO: Used?
        public string GetPasswordIDElmt(string expression)
        {
            string fileName = iTPaths.GetXMLPath() + "Password.xml";
            XPathDocument doc = new XPathDocument(fileName);
            XPathNavigator nav = doc.CreateNavigator();
            return nav.SelectSingleNode(expression).ToString();
        }

   /* public XmlDocument UpdateFlightInXMLFile(XmlDocument mainflightXMLdoc, string flightXML)
        {

            XmlDocument newFlightData = new XmlDocument();
            XmlNode oldFlightData;
            XmlElement root;
            XmlNode oldTurnFlightData;
            //XmlElement turnroot;
            string URNO = "";
            string turnUrno = "";
            XmlElement updatedFlightData;
            XmlElement updatedTurnFlightData;
            try
            {
                newFlightData.LoadXml(flightXML);
            }
            catch (Exception ex)
            {
#if LoggingOn
                iTUtils.LogToEventLog("ITXML: UpdateFlightInXMLFile - newFlightData Load:" + ex.Message);
#endif
            }
            XmlElement newFlightXMLElement = newFlightData.DocumentElement;
            try
            {
                
                URNO = newFlightXMLElement.FirstChild.InnerXml.Trim();
            }
            catch (Exception ex)
            {
 #if LoggingOn
                iTUtils.LogToEventLog("ITXML: Getting Urno:" + ex.Message);
#endif 
                
            }
            root = mainflightXMLdoc.DocumentElement;
            oldFlightData = root.SelectSingleNode("//FLIGHTS/FLIGHT[URNO='" + URNO + "']");
            
                
                try
                {

                    if (newFlightXMLElement.ChildNodes[1].Name.Trim() == "TURN" && newFlightXMLElement.ChildNodes[1].InnerXml.Trim() == "")
                    {
                        turnUrno = oldFlightData.ChildNodes[21].InnerXml;
#if LoggingOn
                        iTUtils.LogToEventLog("turnUrno" + turnUrno);
#endif
                    }
                }
                catch (Exception)
                {
                    
    #if LoggingOn
                iTUtils.LogToEventLog("ITXML: UpdateFlightInXMLFile - newTurnFlightFlightData Load:" );
#endif   
                }
            
            
            
            
            //XmlNodeList newFlightDataNodes = newFlightData.ChildNodes;
            if (oldFlightData != null)
            {
                string newFlightXml = "";
                //build a new xml string for the flight based on 
                //the old data adding the new data where it is specified
                foreach (XmlNode node in oldFlightData.ChildNodes)
                {
                    try
                    {
                        
                        if (newFlightData.SelectSingleNode("//FLIGHT/" + node.Name) == null)
                        {
                            newFlightXml += "<" + node.Name + ">" + node.InnerXml + "</" + node.Name + ">";

                        }
                        else
                        {
                            
                            newFlightXml += "<" + node.Name + ">" + newFlightData.SelectSingleNode("//FLIGHT/" + node.Name).InnerXml + "</" + node.Name + ">";

                        }
                    }
                    catch (Exception ex)
                    {
#if LoggingOn
                        iTUtils.LogToEventLog("ITXML: UpdateFlightInXMLFile - foreach: Load:" + ex.Message);
#endif
                    }
                }


                try
                {

                    updatedFlightData = mainflightXMLdoc.CreateElement("FLIGHT");
                    updatedFlightData.InnerXml = newFlightXml;
                    root.ReplaceChild(updatedFlightData, oldFlightData);
                }
                catch (Exception ex)
                {
#if LoggingOn
                    iTUtils.LogToEventLog("ITXML: UpdateFlightInXMLFile - ReplaceChild:" + ex.Message);
#endif
                }
            }
            if (turnUrno != "")
            {
             
                oldTurnFlightData = root.SelectSingleNode("//FLIGHTS/FLIGHT[URNO='" + turnUrno + "']");

                if (oldTurnFlightData != null)
                {
                    string newFlightXml = "";
                    //build a new xml string for the flight based on 
                    //the old data adding the new data where it is specified
                    foreach (XmlNode node in oldTurnFlightData.ChildNodes)
                    {
                        try
                        {

                            if (node.Name == "TURN")
                            {

                                newFlightXml += "<" + node.Name + "></" + node.Name + ">";
                            }
                            else
                            {
                                newFlightXml += "<" + node.Name + ">" + node.InnerXml + "</" + node.Name + ">";
                            }
                        }
                        catch (Exception ex)
                        {
#if LoggingOn
                            iTUtils.LogToEventLog("ITXML: UpdateFlightInXMLFile - foreach: Load:" + ex.Message);
#endif
                        }
                    }


                    try
                    {

                        updatedTurnFlightData = mainflightXMLdoc.CreateElement("FLIGHT");
                        updatedTurnFlightData.InnerXml = newFlightXml;
                        root.ReplaceChild(updatedTurnFlightData, oldTurnFlightData);
                    }
                    catch (Exception ex)
                    {
#if LoggingOn
                        iTUtils.LogToEventLog("ITXML: UpdateFlightInXMLFile - ReplaceChild:" + ex.Message);
#endif
                    }
                }
            }
            return mainflightXMLdoc;
        }
*/

        public XmlDocument UpdateFlightInXMLFile(XmlDocument mainflightXMLdoc, string flightXML)
        {

            XmlDocument newFlightData = new XmlDocument();
            XmlNode oldFlightData;
            XmlElement root;
            string URNO = "";
            XmlElement updatedFlightData;
            try
            {
                newFlightData.LoadXml(flightXML);
            }
            catch (Exception ex)
            {
#if LoggingOn
                iTUtils.LogToEventLog("ITXML: UpdateFlightInXMLFile - newFlightData Load:" + ex.Message);
#endif
            }
            try
            {
                XmlElement newFlightXMLElement = newFlightData.DocumentElement;
                URNO = newFlightXMLElement.FirstChild.InnerXml.Trim();
            }
            catch (Exception ex)
            {
#if LoggingOn
                iTUtils.LogToEventLog("ITXML: UpdateFlightInXMLFile - newFlightData Load:" + ex.Message);
#endif
            }

            root = mainflightXMLdoc.DocumentElement;
            oldFlightData = root.SelectSingleNode("//FLIGHTS/FLIGHT[URNO='" + URNO + "']");

            //XmlNodeList newFlightDataNodes = newFlightData.ChildNodes;
            if (oldFlightData != null)
            {
                string newFlightXml = "";
                //build a new xml string for the flight based on 
                //the old data adding the new data where it is specified
                foreach (XmlNode node in oldFlightData.ChildNodes)
                {
                    try
                    {
                        if (newFlightData.SelectSingleNode("//FLIGHT/" + node.Name) == null)
                        {
                            newFlightXml += "<" + node.Name + ">" + node.InnerXml + "</" + node.Name + ">";
                        }
                        else
                        {
                            newFlightXml += "<" + node.Name + ">" + newFlightData.SelectSingleNode("//FLIGHT/" + node.Name).InnerXml + "</" + node.Name + ">";
                        }
                    }
                    catch (Exception ex)
                    {
#if LoggingOn
                        iTUtils.LogToEventLog("ITXML: UpdateFlightInXMLFile - foreach: Load:" + ex.Message);
#endif
                    }
                }
                try
                {
                    updatedFlightData = mainflightXMLdoc.CreateElement("FLIGHT");
                    updatedFlightData.InnerXml = newFlightXml;
                    root.ReplaceChild(updatedFlightData, oldFlightData);
                }
                catch (Exception ex)
                {
#if LoggingOn
                    iTUtils.LogToEventLog("ITXML: UpdateFlightInXMLFile - ReplaceChild:" + ex.Message);
#endif
                }
            }
            return mainflightXMLdoc;
        }

        public string UpdateJobInXMLFile(ref XmlDocument mainJobXMLdoc, string JobXML, out string updatedTurnAroundNodes, out string turnUaft, out string uaft)
        {
            XmlDocument newJobData = new XmlDocument();
            string URNO = "";
            XmlNode oldJobData;
            XmlNode oldJobDataForTurnAround;
            XmlElement root;
            XmlElement updatedJobData;
            bool turnaroundStatus = false;
            string updatedNodes = "";
            updatedTurnAroundNodes = "";
            string netalertstatus = "yes";

            try
            {
                newJobData.LoadXml(JobXML);
            }
            catch (Exception ex)
            {
#if LoggingOn
                iTUtils.LogToEventLog("ITXML: UpdateJobInXMLFile - newJobData Load:" + ex.Message);
#endif
            }
            try
            {
                XmlElement newJobXMLElement = newJobData.DocumentElement;
                URNO = newJobXMLElement.FirstChild.InnerXml.Trim();
            }
            catch (Exception ex)
            {
#if LoggingOn
                iTUtils.LogToEventLog("ITXML: UpdateJobInXMLFile - newJobXMLElement:" + ex.Message);
#endif
            }

            //XmlNode oldJobData;
            root = mainJobXMLdoc.DocumentElement;
            oldJobData = root.SelectSingleNode("//JOBS/JOB[URNO='" + URNO + "']");
            oldJobDataForTurnAround = root.SelectSingleNode("//JOBS/JOB[URNO='A" + URNO + "']");
            if (oldJobDataForTurnAround != null)
            {
                turnaroundStatus = true;

            }
            //change
            //XmlNodeList newJobDataNodes = newJobData.ChildNodes;

            string newJobXml = "";
            string newTurnJobXml = "";
            turnUaft = "";
            uaft = "";
            //build a new xml string for the Job based on 
            //the old data adding the new data where it is specified
            if (oldJobData != null)
            {
                foreach (XmlNode node in oldJobData.ChildNodes)
                {
                    try
                    {
                        if (node.Name != "URNO" && node.Name != "UAFT")
                        {
                            if (newJobData.SelectSingleNode("//JOB/" + node.Name) == null)
                            {
                                newJobXml += "<" + node.Name + ">" + node.InnerXml + "</" + node.Name + ">";
                                if (turnaroundStatus)
                                {
                                    newTurnJobXml += "<" + node.Name + ">" + node.InnerXml + "</" + node.Name + ">";
                                }
                            }
                            else
                            {
                                newJobXml += "<" + node.Name + ">" + newJobData.SelectSingleNode("//JOB/" + node.Name).InnerXml + "</" + node.Name + ">";
                                if (turnaroundStatus)
                                {
                                    newTurnJobXml += "<" + node.Name + ">" + newJobData.SelectSingleNode("//JOB/" + node.Name).InnerXml + "</" + node.Name + ">";
                                }
                            }
                        }
                        else
                        {


                            if (turnaroundStatus)
                            {
                                if (node.Name == "URNO")
                                {
                                    newTurnJobXml += oldJobDataForTurnAround.FirstChild.OuterXml;
                                }
                                else
                                {

                                    newTurnJobXml += oldJobDataForTurnAround.ChildNodes[4].OuterXml;

                                }

                            }
                            if (newJobData.SelectSingleNode("//JOB/" + node.Name) == null)
                            {

                                newJobXml += "<" + node.Name + ">" + node.InnerXml + "</" + node.Name + ">";
                            }
                            else
                            {
                                if (node.Name == "UAFT")
                                {
                                    netalertstatus = "no";
                                }
                                newJobXml += "<" + node.Name + ">" + newJobData.SelectSingleNode("//JOB/" + node.Name).InnerXml + "</" + node.Name + ">";
                            }


                        }


                    }
                    catch (Exception ex)
                    {
#if LoggingOn
                        iTUtils.LogToEventLog("ITXML: UpdateJobInXMLFile - foreach:" + ex.Message);
#endif
                    }
                }

                try
                {
                    updatedJobData = mainJobXMLdoc.CreateElement("JOB");
                    updatedJobData.InnerXml = newJobXml;

                    root.ReplaceChild(updatedJobData, oldJobData);
                    if (turnaroundStatus)
                    {
                        updatedJobData = mainJobXMLdoc.CreateElement("JOB");
                        updatedJobData.InnerXml = newTurnJobXml;
                        root.ReplaceChild(updatedJobData, oldJobDataForTurnAround);
                    }
                    //mainJobXMLdoc.Save(iTPaths.GetUpdateableJobsFilePath());
                    if (turnaroundStatus && netalertstatus == "yes")
                    {
                        updatedTurnAroundNodes = "<JOB>" + newTurnJobXml + "</JOB>";
                    }

                    updatedNodes = "<JOB>" + newJobXml + "</JOB>";


                }
                catch (Exception ex)
                {
#if LoggingOn
                    iTUtils.LogToEventLog("ITXML: UpdateJobInXMLFile - ReplaceChild:" + ex.Message);
#endif
                }

                uaft = oldJobData.ChildNodes[4].InnerXml;
            }


            if (turnaroundStatus)
            {
                turnUaft = oldJobDataForTurnAround.ChildNodes[4].InnerXml;
            }
            return updatedNodes;
        }


     /*  public string UpdateJobInXMLFile(ref XmlDocument mainJobXMLdoc, string JobXML, out string updatedTurnAroundNodes)
        {
            XmlDocument newJobData = new XmlDocument();
            string URNO = "";
            XmlNode oldJobData;
            XmlNode oldJobDataForTurnAround;
            XmlElement root;
            XmlElement updatedJobData;
            bool turnaroundStatus = false;
            string updatedNodes = "";
            updatedTurnAroundNodes = "";
            string netalertstatus = "yes";
           
            try
            {
                newJobData.LoadXml(JobXML);
            }
            catch (Exception ex)
            {
#if LoggingOn
                iTUtils.LogToEventLog("ITXML: UpdateJobInXMLFile - newJobData Load:" + ex.Message);
#endif
            }
            try
            {
                XmlElement newJobXMLElement = newJobData.DocumentElement;
                URNO = newJobXMLElement.FirstChild.InnerXml.Trim();
            }
            catch (Exception ex)
            {
#if LoggingOn
                iTUtils.LogToEventLog("ITXML: UpdateJobInXMLFile - newJobXMLElement:" + ex.Message);
#endif
            }

            //XmlNode oldJobData;
            root = mainJobXMLdoc.DocumentElement;
            oldJobData = root.SelectSingleNode("//JOBS/JOB[URNO='" + URNO + "']");
            oldJobDataForTurnAround = root.SelectSingleNode("//JOBS/JOB[URNO='A" + URNO + "']");
            if (oldJobDataForTurnAround != null)
            {
                turnaroundStatus = true;

            }
            //change
            //XmlNodeList newJobDataNodes = newJobData.ChildNodes;

            string newJobXml = "";
            string newTurnJobXml = "";
            //build a new xml string for the Job based on 
            //the old data adding the new data where it is specified
            if (oldJobData != null)
            {
                foreach (XmlNode node in oldJobData.ChildNodes)
                {
                    try
                    {
                        if (node.Name != "URNO" && node.Name != "UAFT")
                        {
                            if (newJobData.SelectSingleNode("//JOB/" + node.Name) == null)
                            {
                                newJobXml += "<" + node.Name + ">" + node.InnerXml + "</" + node.Name + ">";
                                if (turnaroundStatus)
                                {
                                    newTurnJobXml += "<" + node.Name + ">" + node.InnerXml + "</" + node.Name + ">";
                                }
                            }
                            else
                            {
                                newJobXml += "<" + node.Name + ">" + newJobData.SelectSingleNode("//JOB/" + node.Name).InnerXml + "</" + node.Name + ">";
                                if (turnaroundStatus)
                                {
                                    newTurnJobXml += "<" + node.Name + ">" + newJobData.SelectSingleNode("//JOB/" + node.Name).InnerXml + "</" + node.Name + ">";
                                }
                            }
                        }
                        else
                        {


                            if (turnaroundStatus)
                            {
                                if (node.Name == "URNO")
                                {
                                    newTurnJobXml += oldJobDataForTurnAround.FirstChild.OuterXml;
                                }
                                else
                                {

                                    newTurnJobXml += oldJobDataForTurnAround.ChildNodes[4].OuterXml;
                                }
                            }
                            if (newJobData.SelectSingleNode("//JOB/" + node.Name) == null)
                            {

                                newJobXml += "<" + node.Name + ">" + node.InnerXml + "</" + node.Name + ">";
                            }
                            else
                            {
                                if (node.Name == "UAFT")
                                {
                                    netalertstatus = "no";
                                }
                                newJobXml += "<" + node.Name + ">" + newJobData.SelectSingleNode("//JOB/" + node.Name).InnerXml + "</" + node.Name + ">";
                            }
                            
                           
                        }


                    }
                    catch (Exception ex)
                    {
#if LoggingOn
                        iTUtils.LogToEventLog("ITXML: UpdateJobInXMLFile - foreach:" + ex.Message);
#endif
                    }
                }

                try
                {
                    updatedJobData = mainJobXMLdoc.CreateElement("JOB");
                    updatedJobData.InnerXml = newJobXml;

                    root.ReplaceChild(updatedJobData, oldJobData);
                    if (turnaroundStatus)
                    {
                        updatedJobData = mainJobXMLdoc.CreateElement("JOB");
                        updatedJobData.InnerXml = newTurnJobXml;
                        root.ReplaceChild(updatedJobData, oldJobDataForTurnAround);
                    }
                   //mainJobXMLdoc.Save(iTPaths.GetUpdateableJobsFilePath());
                    if (turnaroundStatus && netalertstatus=="yes")
                    {
                        updatedTurnAroundNodes = "<JOB>"+newTurnJobXml+"</JOB>";
                    }
                    
                         updatedNodes = "<JOB>" + newJobXml + "</JOB>";
                   

                }
                catch (Exception ex)
                {
#if LoggingOn
                    iTUtils.LogToEventLog("ITXML: UpdateJobInXMLFile - ReplaceChild:" + ex.Message);
#endif
                }
               

            }


            return updatedNodes;
        }
        */
        //this is used by the MainFlightsXml (FlightMgr Process)
        //(as well as SessionMgr.cs)
        //which is why we return xmldoc rather than save in function itself
        public XmlDocument WriteNewFlightToXMLFile(XmlDocument flightXMLdoc, string flightXML)
        {
            XmlDocument newFlightXML = new XmlDocument();
            newFlightXML.LoadXml(flightXML);
            XmlElement newFlightXMLElement = newFlightXML.DocumentElement;
            XmlNode xmlNode = flightXMLdoc.DocumentElement;
            xmlNode.AppendChild(flightXMLdoc.ImportNode(newFlightXMLElement, true));
            return flightXMLdoc;
        }



        public XmlDocument WriteNewJobToXMLFile(XmlDocument jobXMLdoc, string jobXML)
        {
            XmlDocument newJobXML = new XmlDocument();
            newJobXML.LoadXml(jobXML);
            XmlElement newJobXMLElement = newJobXML.DocumentElement;
            XmlNode xmlNode = jobXMLdoc.DocumentElement;
            xmlNode.AppendChild(jobXMLdoc.ImportNode(newJobXMLElement, true));
            return jobXMLdoc;
        }

        public XmlDocument DeleteETDIFromFlightsXMLFile(XmlDocument flightXMLdoc, string URNO, string FlightsFilePath, string UpdateableFlightsFilePath)
        {
            string expression = "//FLIGHTS/FLIGHT[URNO='" + URNO + "']/ETDI";
            XmlNode xmlNewNode = flightXMLdoc.SelectSingleNode(expression);
            xmlNewNode.InnerXml = "";
            flightXMLdoc.Save(FlightsFilePath);
            flightXMLdoc.Save(UpdateableFlightsFilePath);
            return flightXMLdoc;
        }
        /* Added by Alphy on 06 Dec2006 to delete flight from loadallxml.used by deletesessionservice*/

        public XmlDocument DeleteFlightFromLoadAllXMLFile(string loadallflightsFilePath)
        {
            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(loadallflightsFilePath);
                XmlNodeList FlightsNodeLst = doc.SelectNodes("//LOADALL/FLIGHT");

                DateTime curTime = DateTime.Now;
                TimeSpan minuteTimeSpan = new TimeSpan(1, 0, 0, 0);



                if (FlightsNodeLst.Count > 0)
                {
                    foreach (XmlNode FlightsNode in FlightsNodeLst)
                    {
                        TimeSpan testspan = curTime - Convert.ToDateTime(FlightsNode.ChildNodes.Item(1).InnerXml);
                        if (testspan > minuteTimeSpan)
                        {
#if LoggingOn
                            // iTUtils.LogToEventLog("ITXML: testspan > minuteTimeSpan:");
#endif


                            doc.DocumentElement.RemoveChild(FlightsNode);

                        }


                    }
                }
            }
            catch (Exception genEx)
            {

#if LoggingOn
                iTUtils.LogToEventLog("ITXML: DeleteFlightFromLoadAllXMLFile:" + genEx.Message);
#endif

            }
            return doc;

        }


        public XmlDocument DeleteFlightFromXMLFile(XmlDocument flightXMLdoc, string message)
        {
            //Get URNO from msg
            XmlDocument xmldoc = new XmlDocument();
            try
            {
                xmldoc.LoadXml(message);
            }
            catch (Exception ex)
            {
#if LoggingOn
                iTUtils.LogToEventLog("ITXML: DeleteFlightFromXMLFile - LoadXml:" + ex.Message);
                iTUtils.LogToEventLog("ITXML: DeleteFlightFromXMLFile - LoadXml  message text:" + message);
#endif
            }
            try
            {
                XmlNode xmlnURNO = xmldoc.FirstChild.FirstChild;
                string expression = "//FLIGHTS/FLIGHT[URNO='" + xmlnURNO.InnerXml.Trim() + "']";
                XmlNode xmlNode = flightXMLdoc.SelectSingleNode(expression);
                if (xmlNode != null)
                    flightXMLdoc.DocumentElement.RemoveChild(xmlNode);
            }
            catch (Exception ex)
            {
#if LoggingOn
                iTUtils.LogToEventLog("ITXML: DeleteFlightFromXMLFile - RemoveChild:" + ex.Message);
#endif
            }
            //TODO: Save xmldoc?
            return flightXMLdoc;
        }

        //NB. DeleteJobFromXMLFileForGetFlightsByStaffID doesn't save the deletion
        //TODO: Clarify use of this function - uses flight messages
        //because only needs to check URNO?
        public XmlDocument DeleteJobFromXMLFileForGetFlightsByStaffID(XmlDocument jobXMLdoc, string message)
        {
            XmlDocument xmldoc = new XmlDocument();
            try
            {
                xmldoc.LoadXml(message);
            }
            catch (Exception ex)
            {
#if LoggingOn
                iTUtils.LogToEventLog("ITXML: DeleteJobFromXMLFileForGetFlightsByStaffID - LoadXml:" + ex.Message);
#endif
            }
            try
            {
                XmlNode xmlnURNO = xmldoc.FirstChild.FirstChild;
                string expression = "//JOBS/JOB[URNO='" + xmlnURNO.InnerXml + "']";
                XmlNode xmlNode = jobXMLdoc.SelectSingleNode(expression);
                jobXMLdoc.DocumentElement.RemoveChild(xmlNode);
            }
            catch (Exception ex)
            {
#if LoggingOn
                iTUtils.LogToEventLog("ITXML: DeleteJobFromXMLFileForGetFlightsByStaffID - RemoveChild:" + ex.Message);
#endif
            }
            return jobXMLdoc;
        }

        public XmlDocument DeleteJobFromXMLFile(XmlDocument jobXMLdoc, string message)
        {
            XmlDocument xmldoc = new XmlDocument();
            XmlNode xmlnURNO = null;
            string expression = "";
            XmlNode xmlNode = null;

            if (jobXMLdoc == null)
            {
#if LoggingOn
                iTUtils.LogToEventLog("ITXML: DeleteJobFromXMLFile - jobXMLdoc == null");
#endif
                jobXMLdoc = new XmlDocument();
                jobXMLdoc.Load(iTPaths.GetUpdateableJobsFilePath());
            }


            try
            {
                xmldoc.LoadXml(message);
            }
            catch (Exception ex)
            {
#if LoggingOn
                iTUtils.LogToEventLog("ITXML: DeleteJobFromXMLFile - LoadXml message text:" + message);
                iTUtils.LogToEventLog("ITXML: DeleteJobFromXMLFile - LoadXml:" + ex.Message);
#endif
            }

            try
            {
                xmlnURNO = xmldoc.FirstChild.FirstChild;

                expression = "//JOBS/JOB[URNO='" + xmlnURNO.InnerXml.Trim() + "']";
            }
            catch (Exception ex)
            {
#if LoggingOn
               // iTUtils.LogToEventLog("ITXML: DeleteJobFromXMLFile - expression:" + ex.Message);
             //   iTUtils.LogToEventLog("ITXML: DeleteJobFromXMLFile - xmlnURNO:" + xmlnURNO.InnerXml);
#endif
            }

            try
            {
#if LoggingOn
                //if (jobXMLdoc == null)
                //    iTUtils.LogToEventLog("ITXML: DeleteJobFromXMLFile - jobXMLdoc == null");
                //else
                //    iTUtils.LogToEventLog("ITXML: DeleteJobFromXMLFile - jobXMLdoc:" + jobXMLdoc.FirstChild.InnerXml);
#endif
                xmlNode = jobXMLdoc.SelectSingleNode(expression);
#if LoggingOn
                if (xmlNode == null)
                {
                  //  iTUtils.LogToEventLog("ITXML: DeleteJobFromXMLFile - xmlNode == null");
                }
                else
                {
                   // iTUtils.LogToEventLog("ITXML: DeleteJobFromXMLFile - xmlNode:" + xmlNode.InnerXml);
                    jobXMLdoc.DocumentElement.RemoveChild(xmlNode);
                    expression = "//JOBS/JOB[URNO='A" + xmlnURNO.InnerXml.Trim() + "']";
                    xmlNode = jobXMLdoc.SelectSingleNode(expression);
                    if (xmlNode == null)
                    {
                        //iTUtils.LogToEventLog("ITXML: DeleteJobFromXMLFilefor turnaround - xmlNode == null");
                    }
                    else
                    {
                     //  iTUtils.LogToEventLog("ITXML: DeleteJobFromXMLFilefor turnaround - xmlNode:" + xmlNode.InnerXml);
                        jobXMLdoc.DocumentElement.RemoveChild(xmlNode);
                    }
                }
#endif

            }
            catch (Exception ex)
            {
#if LoggingOn
                iTUtils.LogToEventLog("ITXML: DeleteJobFromXMLFile - LoadXml message text:" + message);
                iTUtils.LogToEventLog("ITXML: DeleteJobFromXMLFile - jobXMLdoc:" + jobXMLdoc.FirstChild.InnerXml);
                iTUtils.LogToEventLog("ITXML: DeleteJobFromXMLFile - ex.Message:" + ex.Message);
                iTUtils.LogToEventLog("ITXML: DeleteJobFromXMLFile - RemoveChild expression:" + expression);
                iTUtils.LogToEventLog("ITXML: DeleteJobFromXMLFile - RemoveChild xmlNode:" + xmlNode.InnerXml);
#endif
            }
            return jobXMLdoc;
        }

        //TODO: Probably improve - see DeleteAuthFileInSessionDir code
        public void DeleteSessionFile(string sessid, string fileName)
        {

            DirectoryInfo di = new DirectoryInfo(iTPaths.GetSessionXMLPath());
            //DirectoryInfo[] diArr = di.GetDirectories(iTPaths.GetTestSessionId());
            DirectoryInfo[] diArr = di.GetDirectories(sessid);
            FileInfo[] rgFiles = diArr[0].GetFiles(fileName);
            //Assert.AreEqual(fileName, rgFiles[0].Name);
            //clean up - delete file
            for (int i = 0; i < rgFiles.Length; i++)
            {
                if (rgFiles[i].Name == fileName)
                    rgFiles[i].Delete();
            }
        }



        public string DeleteActionFromFlightXML(string flightXML)
        {
            XmlDocument FlightXMLdoc = TransformStringToXmlDoc(flightXML);
            string expression = "//FLIGHT/ACTION";
            XmlNode xmlNode = FlightXMLdoc.SelectSingleNode(expression);
            FlightXMLdoc.DocumentElement.RemoveChild(xmlNode);
            return FlightXMLdoc.OuterXml;
        }

        public string DeleteActionFromJobXMLFile(string jobXML)
        {
            XmlDocument jobXMLdoc = TransformStringToXmlDoc(jobXML);
            string expression = "//JOB/ACTION";
            XmlNode xmlNode = jobXMLdoc.SelectSingleNode(expression);
            jobXMLdoc.DocumentElement.RemoveChild(xmlNode);
            return jobXMLdoc.OuterXml;
        }

        //public void WriteXmlToOK2LoadsFile(string deviceID, string flightno, string OK2LoadAlertTime)
        public void WriteXmlToOK2LoadsFile(string deviceID, string flightno, string ULDid)
        {
            string OK2LoadsXMLFile = iTPaths.GetXMLPath() + "OK2Loads.xml";
            XmlDocument doc = new XmlDocument();
            doc.Load(OK2LoadsXMLFile);
            //XmlElement root = doc.DocumentElement;
            XmlNode xmlNode = doc.DocumentElement;
            //Create  a  new  node.
            XmlElement elem = doc.CreateElement("OK2LoadAlert");
            //elem.InnerText="19.95";
            xmlNode.AppendChild(elem);
            // Add a new attribute.
            elem.SetAttribute("ULD", ULDid);
            elem.SetAttribute("FlightNo", flightno);
            elem.SetAttribute("OK2LoadTime", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            //root.SetAttribute("Timing", "12:00");
            doc.Save(OK2LoadsXMLFile);

        }


        /* Added on 01/11/2007 by Alphy*/


        public static int DiffInSec(DateTime dt1, DateTime dt2)
        {
            int diff = (dt1.Hour - dt2.Hour) * 3600 +
                (dt1.Minute - dt2.Minute) * 60 +
                (dt1.Second - dt2.Second);
            return diff;
        }

        private readonly static DateTime _defaultDT = new DateTime(1, 1, 1);
        private static DateTime _lastJobWriteTime = _defaultDT;
        private static DateTime _lastJobRefreshTime = _defaultDT;
        private static XmlDocument _myJobXmlDocument = new XmlDocument();

        public static XmlDocument LoadJob(string filePath,out string status)
        {
            status = "NOK";
            //int timeElapse = Math.Abs(DiffInSec(DateTime.Now, _lastJobRefreshTime));
            //if (timeElapse > 3)
            //{
                FileInfo fiXml = new FileInfo(filePath);
                DateTime curDT = fiXml.LastWriteTime;

                if (curDT.CompareTo(_lastJobWriteTime) != 0)
                {
                    _myJobXmlDocument.Load(filePath);
                    _lastJobWriteTime = curDT;
                    status = "OK";
                }
            //    _lastJobRefreshTime = DateTime.Now;
            //}
            return _myJobXmlDocument;
        }
        private static DateTime _lastFlightWriteTime = _defaultDT;
        private static DateTime _lastFlightRefreshTime = _defaultDT;
        private static XmlDocument _myFlightXmlDocument = new XmlDocument();

        public static XmlDocument LoadFlight(string filePath, out string status)
        {
            status = "NOK";
            // int timeElapse = Math.Abs(DiffInSec(DateTime.Now, _lastFlightRefreshTime));
            //if (timeElapse > 3)
            // {
            FileInfo fiXml = new FileInfo(filePath);
            DateTime curDT = fiXml.LastWriteTime;

            if (curDT.CompareTo(_lastFlightWriteTime) != 0)
            {
                _myFlightXmlDocument.Load(filePath);
                _lastFlightWriteTime = curDT;
                status = "OK";
            }
            //  _lastFlightRefreshTime = DateTime.Now;
            //}
            return _myFlightXmlDocument;

        }
       
        private static DateTime _lastJobforAlertWriteTime = _defaultDT;
        private static DateTime _lastJobforAlertRefreshTime = _defaultDT;
        private static XPathDocument _myJobXmlPathDocument = null;

        public static XPathDocument LoadJobForAlert(string filePath)
        {
            if (_myJobXmlPathDocument == null)
            {
                _myJobXmlPathDocument = new XPathDocument(filePath);
                _lastJobforAlertRefreshTime = DateTime.Now;
            }
            else
            {
                int timeElapse = Math.Abs(DiffInSec(DateTime.Now, _lastJobforAlertRefreshTime));
                if (timeElapse > 3)
                {
                    FileInfo fiXml = new FileInfo(filePath);
                    DateTime curDT = fiXml.LastWriteTime;

                    if (curDT.CompareTo(_lastJobforAlertWriteTime) != 0)
                    {
                        _myJobXmlPathDocument = new XPathDocument(filePath);

                        _lastJobforAlertWriteTime = curDT;
                    }
                    _lastJobforAlertRefreshTime = DateTime.Now;
                }
            }
            return _myJobXmlPathDocument;
        }

        public ArrayList UsePENOToGetMatchingFlightsFromMainJobsXMLFile(string expression, string staffid, string filePath,out string jobStatus)
        {
            ArrayList al = new ArrayList();
            jobStatus = "NOK";
            FileInfo fi = new FileInfo(filePath);
            if (fi.Exists)
            {
                XmlNodeList JobList;
                string status = "NOK";
                XmlDocument myXmlDocument = LoadJob(filePath,out status);
                jobStatus = status;
               // XmlDocument myXmlDocument = new XmlDocument();
               // myXmlDocument.Load(filePath);
                JobList = myXmlDocument.SelectNodes(expression);

                if (JobList.Count > 0)
                {
                    foreach (XmlNode JobNode in JobList)
                    {
                        if (JobNode.ChildNodes[1].InnerXml == staffid)
                        {
                            XmlNode flightAssign = JobNode.ChildNodes[4];
                            al.Add(flightAssign.InnerXml);
                        }
                    }
                }

            }
            return al;

        }

        //public ArrayList ReadElementsForArrayListFromCurrentFlightXMLFile(string expression, string filePath)
        //{
        //    ArrayList al = new ArrayList();
        //    XmlNodeList FlightList;
        //    XmlDocument myXmlDocument = new XmlDocument();
        //    myXmlDocument.Load(filePath);
        //    FlightList = myXmlDocument.SelectNodes(expression);

        //    if (FlightList.Count > 0)
        //    {
        //        foreach (XmlNode FlightNode in FlightList)
        //        {
        //            //"SQ020","stod", "stoa","regn","des3","ETDI", "ETOA", "EQPS", ADID, POS1, "urno"));
        //            al.Add(new Flight(FlightNode.ChildNodes[0].InnerXml, FlightNode.ChildNodes[1].InnerXml, FlightNode.ChildNodes[2].InnerXml, FlightNode.ChildNodes[3].InnerXml, FlightNode.ChildNodes[4].InnerXml, FlightNode.ChildNodes[5].InnerXml, FlightNode.ChildNodes[6].InnerXml, FlightNode.ChildNodes[7].InnerXml, FlightNode.ChildNodes[8].InnerXml, FlightNode.ChildNodes[9].InnerXml, FlightNode.ChildNodes[10].InnerXml, FlightNode.ChildNodes[11].InnerXml, FlightNode.ChildNodes[12].InnerXml, FlightNode.ChildNodes[13].InnerXml, FlightNode.ChildNodes[14].InnerXml, FlightNode.ChildNodes[15].InnerXml, FlightNode.ChildNodes[16].InnerXml, FlightNode.ChildNodes[17].InnerXml, FlightNode.ChildNodes[18].InnerXml, FlightNode.ChildNodes[19].InnerXml, FlightNode.ChildNodes[20].InnerXml));
        //        }
        //    }
        //    //only fetch last 3 flights
        //    al.Sort();
        //    if (al.Count != 0 && al.Count < 4)
        //        return al;
        //    else
        //    {
        //        al.RemoveRange(2, al.Count - 3);
        //        return al;
        //    }
        //}


        /* Get the list of flights from Session directory */

        //NB. Third parameter is fileName NOT filePath
        //public ArrayList UserSessionReadElementsForArrayListFromCurrentFlightXMLFile(string sessid, string expression, string fileName)
        //{
        //    string filePath = iTPaths.GetSessionXMLPath() + sessid + @"\" + fileName;
           
        //    ArrayList al = new ArrayList();
        //    try
        //    {
        //        XmlNodeList FlightList;
        //        XmlNode transitNode;
        //        XmlDocument myXmlDocument = new XmlDocument();
        //        myXmlDocument.Load(filePath);
        //        FlightList = myXmlDocument.SelectNodes(expression);
        //        if (FlightList.Count > 0)
        //        {
        //            foreach (XmlNode FlightNode in FlightList)
        //            {
        //                //currently only process departure flights
        //                //  if (FlightNode.ChildNodes[11].InnerXml == "D")
        //                if (FlightNode.ChildNodes[21].InnerXml == "")
        //                {
        //                    al.Add(new Flight(FlightNode.ChildNodes[0].InnerXml, FlightNode.ChildNodes[1].InnerXml, FlightNode.ChildNodes[2].InnerXml, FlightNode.ChildNodes[3].InnerXml, FlightNode.ChildNodes[4].InnerXml, FlightNode.ChildNodes[5].InnerXml, FlightNode.ChildNodes[6].InnerXml, FlightNode.ChildNodes[7].InnerXml, FlightNode.ChildNodes[8].InnerXml, FlightNode.ChildNodes[9].InnerXml, FlightNode.ChildNodes[10].InnerXml, FlightNode.ChildNodes[11].InnerXml, FlightNode.ChildNodes[12].InnerXml, FlightNode.ChildNodes[13].InnerXml, FlightNode.ChildNodes[14].InnerXml, FlightNode.ChildNodes[15].InnerXml, FlightNode.ChildNodes[16].InnerXml, FlightNode.ChildNodes[17].InnerXml, FlightNode.ChildNodes[18].InnerXml, FlightNode.ChildNodes[19].InnerXml, FlightNode.ChildNodes[20].InnerXml, FlightNode.ChildNodes[21].InnerXml));
        //                }
        //                else if(FlightNode.ChildNodes[11].InnerXml == "A")
        //                {
                           
        //                    transitNode = myXmlDocument.SelectSingleNode(expression + "[URNO=" + FlightNode.ChildNodes[21].InnerXml + "]");
        //                    if (transitNode != null)
        //                    {
                               
        //                        al.Add(new Flight(FlightNode.ChildNodes[0].InnerXml, FlightNode.ChildNodes[1].InnerXml, transitNode.ChildNodes[2].InnerXml, FlightNode.ChildNodes[3].InnerXml, FlightNode.ChildNodes[4].InnerXml, transitNode.ChildNodes[5].InnerXml, FlightNode.ChildNodes[6].InnerXml, transitNode.ChildNodes[7].InnerXml, FlightNode.ChildNodes[8].InnerXml, FlightNode.ChildNodes[9].InnerXml, FlightNode.ChildNodes[10].InnerXml, FlightNode.ChildNodes[11].InnerXml, FlightNode.ChildNodes[12].InnerXml, FlightNode.ChildNodes[13].InnerXml, FlightNode.ChildNodes[14].InnerXml, FlightNode.ChildNodes[15].InnerXml, FlightNode.ChildNodes[16].InnerXml, FlightNode.ChildNodes[17].InnerXml, FlightNode.ChildNodes[18].InnerXml, FlightNode.ChildNodes[19].InnerXml, FlightNode.ChildNodes[20].InnerXml, FlightNode.ChildNodes[21].InnerXml));
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        iTrekUtils.iTUtils.LogToEventLog("Exception in getting flights from session directory" + ex.Message);
        //    }
        //    if (al != null && al.Count != 0)
        //    {

        //        return al;
        //    }
        //    else
        //    {
        //        return null;

        //    }
           
        //}
        //public Flight UserSessionReadFlightFromCurrentFlightXMLFile(string sessid, string expression, string fileName)
        //{
        //    string filePath = iTPaths.GetSessionXMLPath() + sessid + @"\" + fileName;

        //   Flight flight=null;
        //   try
        //   {
        //       XmlNode FlightNode;
        //       XmlNode transitNode;
        //       XmlDocument myXmlDocument = new XmlDocument();
        //       myXmlDocument.Load(filePath);
        //       FlightNode = myXmlDocument.SelectSingleNode(expression);
        //       if (FlightNode.ChildNodes.Count > 0)
        //       {

        //           if (FlightNode.ChildNodes[21].InnerXml == "")
        //           {
        //               flight = new Flight(FlightNode.ChildNodes[0].InnerXml, FlightNode.ChildNodes[1].InnerXml, FlightNode.ChildNodes[2].InnerXml, FlightNode.ChildNodes[3].InnerXml, FlightNode.ChildNodes[4].InnerXml, FlightNode.ChildNodes[5].InnerXml, FlightNode.ChildNodes[6].InnerXml, FlightNode.ChildNodes[7].InnerXml, FlightNode.ChildNodes[8].InnerXml, FlightNode.ChildNodes[9].InnerXml, FlightNode.ChildNodes[10].InnerXml, FlightNode.ChildNodes[11].InnerXml, FlightNode.ChildNodes[12].InnerXml, FlightNode.ChildNodes[13].InnerXml, FlightNode.ChildNodes[14].InnerXml, FlightNode.ChildNodes[15].InnerXml, FlightNode.ChildNodes[16].InnerXml, FlightNode.ChildNodes[17].InnerXml, FlightNode.ChildNodes[18].InnerXml, FlightNode.ChildNodes[19].InnerXml, FlightNode.ChildNodes[20].InnerXml, FlightNode.ChildNodes[21].InnerXml);
        //           }
        //           else if (FlightNode.ChildNodes[11].InnerXml == "A")
        //           {

        //               transitNode = myXmlDocument.SelectSingleNode("//FLIGHTS/FLIGHT[URNO='" + FlightNode.ChildNodes[21].InnerXml + "']");
        //               if (transitNode != null)
        //               {

        //                   flight = new Flight(FlightNode.ChildNodes[0].InnerXml, FlightNode.ChildNodes[1].InnerXml, transitNode.ChildNodes[2].InnerXml, FlightNode.ChildNodes[3].InnerXml, FlightNode.ChildNodes[4].InnerXml, transitNode.ChildNodes[5].InnerXml, FlightNode.ChildNodes[6].InnerXml, transitNode.ChildNodes[7].InnerXml, FlightNode.ChildNodes[8].InnerXml, FlightNode.ChildNodes[9].InnerXml, FlightNode.ChildNodes[10].InnerXml, FlightNode.ChildNodes[11].InnerXml, FlightNode.ChildNodes[12].InnerXml, FlightNode.ChildNodes[13].InnerXml, FlightNode.ChildNodes[14].InnerXml, FlightNode.ChildNodes[15].InnerXml, FlightNode.ChildNodes[16].InnerXml, FlightNode.ChildNodes[17].InnerXml, FlightNode.ChildNodes[18].InnerXml, FlightNode.ChildNodes[19].InnerXml, FlightNode.ChildNodes[20].InnerXml, FlightNode.ChildNodes[21].InnerXml);
        //               }
        //           }
        //       }
        //   }

        //   catch (Exception ex)
        //   {

        //       iTrekUtils.iTUtils.LogToEventLog("Exception in getting flights from session directory" + ex.Message);
        //   }
        //   return flight;

        //}
       

        public ArrayList ReorderFlightsEarliestOnTop(ArrayList al)
        {
            if (al == null)
                return al;
            else
            {
                al.Sort();
                return al;
            }
        }

        public XmlNodeList GetBatchFlightsNodeList(string expression, string flightsMainFilePath,out string flightStatus)
        {
            ArrayList al = new ArrayList();

            XmlNodeList FlightList;
            flightStatus = "NOK";
           string status = "NOK";
            XmlDocument myXmlDocument = LoadFlight(flightsMainFilePath,out status);
            flightStatus=status;
            //XmlDocument myXmlDocument = new XmlDocument();
           // myXmlDocument.Load(flightsMainFilePath);
            FlightList = myXmlDocument.SelectNodes(expression);
            return FlightList;
        }



        public DataSet GetFlightDataSetFromCurrentFlightXMLFile()
        {
            //TODO: appDomain
            DirectoryInfo di = new DirectoryInfo(iTPaths.GetXMLPath() + @"/FLIGHTS/");
            FileInfo[] rgFiles = di.GetFiles("*.xml");
            string fileName = rgFiles[rgFiles.Length - 1].FullName;

            DataSet ds = new DataSet();

            ds.ReadXml(fileName);

            //only fetch last 3 flights
            for (int i = 3; i < ds.Tables[0].Rows.Count; i++)
                ds.Tables[0].Rows[i].Delete();
            ds.Tables[0].AcceptChanges();

            return ds;
        }


        public string OK2LULDNo(string message)
        {
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(message);
            XmlNode xmln = xmldoc.SelectSingleNode("//OK2L/ULDNUMBER");
            return xmln.InnerXml;
        }

        public string OK2LAirline(string message)
        {
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(message);
            XmlNode xmln = xmldoc.ChildNodes[1];
            //ChildNodes[1] = Airline
            XmlNode xmlnULD = xmln.ChildNodes[1];
            return xmlnULD.InnerXml;
        }

        public string OK2LFlightNo(string message)
        {
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(message);
            XmlNode xmln = xmldoc.ChildNodes[1];
            XmlNode xmlnULD = xmln.ChildNodes[2];
            return xmlnULD.InnerXml;
        }

        public string OK2LDate(string message)
        {
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(message);
            XmlNode xmln = xmldoc.ChildNodes[1];
            XmlNode xmlnULD = xmln.ChildNodes[3];
            return xmlnULD.InnerXml;
        }

        public string OK2LDestination(string message)
        {
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(message);
            XmlNode xmln = xmldoc.ChildNodes[1];
            XmlNode xmlnULD = xmln.ChildNodes[4];
            return xmlnULD.InnerXml;
        }

        public string GetSessionIdFromMsg(string message)
        {
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(message);
            XmlNodeList xmlnl = xmldoc.FirstChild.ChildNodes;
            string sessid = "";
            foreach (XmlNode infonode in xmlnl)
            {
                if (infonode.Name == "SESSIONID")
                    sessid = infonode.InnerXml;
            }
            return sessid;
        }
        public string GetSessionIdFromMsgSibling(string message)
        {
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(message);
            string s1 = xmldoc.FirstChild.InnerXml;
            xmldoc.LoadXml(s1);
            XmlNodeList xmlnl = xmldoc.FirstChild.ChildNodes;
            string sessid = "";
            foreach (XmlNode infonode in xmlnl)
            {
                if (infonode.Name == "SESSIONID")
                    sessid = infonode.InnerXml;
            }
            return sessid;
        }


        public string GetUAFTFromMsg(string message)
        {
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(message);
            XmlNodeList xmlnl = xmldoc.FirstChild.ChildNodes;
            string sessid = "";
            foreach (XmlNode infonode in xmlnl)
            {
                if (infonode.Name == "UAFT")
                    sessid = infonode.InnerXml;
            }
            return sessid;
        }

        //TODO: Refactor for further error handling
        public string GetMsgTypeFromMsg(string message)
        {
            if (message == "MQRC_NO_MSG_AVAILABLE" || message == "")
                return "NO_MSG";
            else
            {
                //special code for DLS to avoid use of XmlDoc
                if (message.IndexOf("<SHOWDLS>") != -1)
                {
                    return "SHOWDLS";
                }
                else
                    if (message.IndexOf("<MSG>") != -1)
                    {
                        return "MSG";


                    }
                    else
                        if (message.IndexOf("<MSGTO>") != -1)
                        {
                            return "MSGTO";


                        }

                        else
                        {
                            XmlDocument xmldoc = new XmlDocument();
                            xmldoc.LoadXml(message);
                            return xmldoc.DocumentElement.Name;
                        }
            }
        }

        public string GetFlightMsgActionFromMsg(string message)
        {
            try
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(message);

                if (xmldoc.DocumentElement.Name == "FLIGHTS")
                    return "FLIGHTS";
                else
                    return xmldoc.SelectSingleNode("//FLIGHT/ACTION").InnerXml;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }


        public string GetJobMsgActionFromMsg(string message)
        {
            try
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(message);

                if (xmldoc.DocumentElement.Name == "JOBS")
                    return "JOBS";
                else
                    return xmldoc.SelectSingleNode("//JOB/ACTION").InnerXml;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        //TODO: Refactor this method to write to a session.xml file which more 
        //accurately describes it's purpose
        //public void WriteStaffSessionandDeviceIDXmlToDeviceIDFile(string sessid, string deviceID, string staffid, string jobCategory)
        //{
        //    DeleteDeviceIDEntryByStaffId(staffid);
        //    DeleteDeviceIDEntryByDeviceID(deviceID);
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load(iTPaths.GetDeviceIDFile());

        //    XmlNode xmlNode = doc.DocumentElement;
        //    //Create  a  new  node.
        //    XmlElement elem = doc.CreateElement("devices");
        //    //elem.InnerText="19.95";
        //    xmlNode.AppendChild(elem);
        //    // Add a new attribute.
        //    elem.SetAttribute("sessionid", sessid);
        //    elem.SetAttribute("deviceid", deviceID);
        //    elem.SetAttribute("staffid", staffid);
        //    //29/10/2006 10:19:59
        //    elem.SetAttribute("time", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
        //    elem.SetAttribute("org", jobCategory);
        //    doc.Save(iTPaths.GetDeviceIDFile());
        //    iTrekUtils.iTUtils.LogToEventLog("DeviceiD=" + deviceID + ";Staffid=" + staffid + ";SessionId=" + sessid + ";Org=" + jobCategory, "iTrekDevice");

        //}

        //public void WriteAuthorisationResponseToFile(string sessid, string deviceID, string staffid)
        //{
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load(iTPaths.GetDeviceIDFile());
        //    //XmlElement root = doc.DocumentElement;
        //    XmlNode xmlNode = doc.DocumentElement;
        //    //Create  a  new  node.
        //    XmlElement elem = doc.CreateElement("devices");
        //    //elem.InnerText="19.95";
        //    xmlNode.AppendChild(elem);
        //    // Add a new attribute.
        //    elem.SetAttribute("sessionid", sessid);
        //    elem.SetAttribute("deviceid", deviceID);
        //    elem.SetAttribute("staffid", staffid);
        //    elem.SetAttribute("time", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
        //    //root.SetAttribute("Timing", "12:00");
        //    doc.Save(iTPaths.GetDeviceIDFile());
        //}

        //TODO: create test to include multiple device id entries?
        //public void DeleteDeviceIDEntryBySessID(string sessid)
        //{
        //    //use sessid to identify PENO
        //    //so we can eliminate all entries for 
        //    //the relevant device
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load(iTPaths.GetDeviceIDFile());

        //    XmlNodeList DeviceIDNodeLst = doc.SelectNodes("//deviceIDs/devices");
        //    XmlNode PENONode = doc.SelectSingleNode("//deviceIDs/devices[@sessionid='" + sessid + "']/@staffid");

        //    if (DeviceIDNodeLst.Count > 0)
        //    {
        //        foreach (XmlNode DeviceNode in DeviceIDNodeLst)
        //        {
        //            if (DeviceNode.Attributes.Item(2).Value == PENONode.InnerXml)
        //            {
        //                DeviceNode.ParentNode.RemoveChild(DeviceNode);
        //            }
        //        }
        //    }
        //    doc.Save(iTPaths.GetDeviceIDFile());
        //}

        //Following removes ALL entries for staffid (PENO)
        //including earlier logins (which hadn't logged out correctly)
        //public void DeleteDeviceIDEntryByStaffId(string staffid)
        //{
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load(iTPaths.GetDeviceIDFile());
        //    XmlNodeList DeviceIDNodeLst = doc.SelectNodes("//deviceIDs/devices");

        //    if (DeviceIDNodeLst.Count > 0)
        //    {
        //        foreach (XmlNode DeviceNode in DeviceIDNodeLst)
        //        {
        //            if (DeviceNode.Attributes.Item(2).Value == staffid)
        //            {
        //                DeviceNode.ParentNode.RemoveChild(DeviceNode);
        //            }
        //        }
        //    }
        //    doc.Save(iTPaths.GetDeviceIDFile());
        //}


        //public void DeleteDeviceIDEntryByDeviceID(string deviceid)
        //{
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load(iTPaths.GetDeviceIDFile());
        //    XmlNodeList DeviceIDNodeLst = doc.SelectNodes("//deviceIDs/devices");

        //    if (DeviceIDNodeLst.Count > 0)
        //    {
        //        foreach (XmlNode DeviceNode in DeviceIDNodeLst)
        //        {
        //            if (DeviceNode.Attributes.Item(1).Value == deviceid)
        //            {
        //                DeviceNode.ParentNode.RemoveChild(DeviceNode);
        //            }
        //        }
        //    }
        //    doc.Save(iTPaths.GetDeviceIDFile());
        //}




        //public bool PENOExist(string PENO)
        //{
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load(iTPaths.GetDeviceIDFile());
        //    XmlNodeList DeviceNodeLst = doc.SelectNodes("//deviceIDs/devices[@staffid = '" + PENO + "']");
        //    if (DeviceNodeLst.Count > 0)
        //        return true;
        //    else
        //        return false;
        //}


        //public string GetDeviceIDBySession(string sessid)
        //{
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load(iTPaths.GetDeviceIDFile());
        //    XmlNodeList DeviceNodeLst = doc.SelectNodes("//deviceIDs/devices");

        //    string deviceIdAssignedToSession = "";
        //    if (DeviceNodeLst.Count > 0)
        //    {
        //        foreach (XmlNode DeviceNode in DeviceNodeLst)
        //        {
        //            if (DeviceNode.Attributes.Item(0).Value == sessid)
        //            {
        //                deviceIdAssignedToSession = DeviceNode.Attributes.Item(1).Value;
        //            }
        //        }
        //    }
        //    return deviceIdAssignedToSession;
        //}

        public string RemoveBadCharsFromSHOWDLS(string BadCharsString)
        {
            int startOfBadStr = BadCharsString.IndexOf("<DLS>");
            int endOfBadStr = BadCharsString.IndexOf("</DLS>");
            int lengthOfSubStr = endOfBadStr - startOfBadStr;
            string newString = BadCharsString.Substring(startOfBadStr, lengthOfSubStr);

            newString = convStr(newString);
            BadCharsString = BadCharsString.Remove(startOfBadStr, lengthOfSubStr);
            BadCharsString = BadCharsString.Insert(startOfBadStr, newString);

            return BadCharsString;
        }



        private string convStr(string Origstr)
        {
            char[] charAr = Origstr.ToCharArray();
            string charSet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890<> -.,";
            string completeStr = "";
            int len = Origstr.Length;

            for (int i = 0; i < len; i++)
            {
                if (charSet.IndexOf(charAr[i]) != -1)
                {
                    completeStr = completeStr + charAr[i];
                }
                else
                {
                    completeStr = completeStr + " ";
                }
            }
            return completeStr;
        }


        /*  Get the timings for a particular flight and returns a Dataset*/

        //public DataSet getArrivalFlightTimingsFromXML(string flightUrno)
        //{

        //    DataSet ds = new DataSet("AFLIGHT");
        //    try
        //    {

        //        DataTable dt = ds.Tables.Add("AFLIGHT");
        //        dt.Columns.Add("Key");
        //        dt.Columns.Add("Value");
        //        DataRow dr;
        //        Hashtable timingHt = new Hashtable();
        //        XmlDocument myXmlDocument = new XmlDocument();
        //        myXmlDocument.Load(iTPaths.GetArrFlightsTimingFilePath());
        //        XmlNode timeNode = myXmlDocument.SelectSingleNode("//ArrivalFlight/AFLIGHT[URNO=" + flightUrno + "]");


        //        if (timeNode != null)
        //        {
        //            int count = timeNode.ChildNodes.Count;
        //            for (int i = 1; i < count; i++)
        //            {

        //                dr = dt.NewRow();
        //                dr["Key"] = timeNode.ChildNodes[i].Name;
        //                dr["Value"] = timeNode.ChildNodes[i].InnerXml;
        //                ds.Tables["AFLIGHT"].Rows.Add(dr);
        //                ds.Tables["AFLIGHT"].AcceptChanges();
        //                // timingHt.Add(timeNode.ChildNodes[i].Name, timeNode.ChildNodes[i].InnerXml);
        //            }


        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        iTUtils.LogToEventLog("Exception in getArrivalFlightTimingsFromXML" + ex.Message);
        //    }
        //    //return timingHt;
        //    return ds;
        //}


        ///*  Get the timings for a particular flight and returns a Dataset*/

        //public DataSet getDepartureFlightTimingsFromXML(string flightUrno)
        //{
        //    DataSet ds = new DataSet("DFLIGHT");

        //    try
        //    {
        //        DataTable dt = ds.Tables.Add("DFLIGHT");
        //        dt.Columns.Add("Key");
        //        dt.Columns.Add("Value");
        //        DataRow dr;
        //        //ds.
        //        //Hashtable timingHt = new Hashtable();
        //        SortedList st = new SortedList();
        //        XmlDocument myXmlDocument = new XmlDocument();
        //        myXmlDocument.Load(iTPaths.GetDepFlightsTimingFilePath());
        //        XmlNode timeNode = myXmlDocument.SelectSingleNode("//DEPARTUREFLIGHT/DFLIGHT[URNO=" + flightUrno + "]");
        //        // ds.ReadXml("C:\\Program Files\\Ufis\\iTrekOk2Load\\XML\\DepartureFlightTimings.XML",XmlReadMode.ReadSchema);
        //        //int k= ds.Tables.Count;
        //        if (timeNode != null)
        //        {
        //            int count = timeNode.ChildNodes.Count;
        //            for (int i = 0; i < count; i++)
        //            {
        //                dr = dt.NewRow();
        //                dr["Key"] = timeNode.ChildNodes[i].Name;
        //                dr["Value"] = timeNode.ChildNodes[i].InnerXml;
        //                ds.Tables["DFLIGHT"].Rows.Add(dr);
        //                ds.Tables["DFLIGHT"].AcceptChanges();
        //                // st.Add(timeNode.ChildNodes[i].Name, timeNode.ChildNodes[i].InnerXml);
        //                // timingHt.Add(timeNode.ChildNodes[i].Name, timeNode.ChildNodes[i].InnerXml);
        //            }


        //        }
        //        string test = ds.GetXml();
        //    }
        //    catch (Exception ex)
        //    {
        //        iTUtils.LogToEventLog("Exception in getDepartureFlightTimingsFromXML" + ex.Message); 
                
        //    }
        //    return ds;

        //}
        /* Update Arrival timing received from backend.Called by GetReadService*/

        //public void updateArrivalTimings(string flightURNO, string node, string time)
        //{

        //    XmlDocument myXmlDocument = new XmlDocument();
        //    myXmlDocument.Load(iTPaths.GetArrFlightsTimingFilePath());
        //    XmlNode timeNode = myXmlDocument.SelectSingleNode("//ArrivalFlight/AFLIGHT[URNO=" + flightURNO + "]");
        //    if (timeNode != null)
        //    {
        //        int count = timeNode.ChildNodes.Count;
        //        for (int i = 1; i < count; i++)
        //        {
        //            if (timeNode.ChildNodes[i].Name == node)
        //            {

        //                timeNode.ChildNodes[i].InnerXml = time;
        //            }
        //        }


        //    }
        //    else
        //    {
        //        XmlElement elem = myXmlDocument.CreateElement("AFLIGHT");
        //        elem.InnerXml = "<URNO></URNO><MAB></MAB><ATA></ATA><TBS-UP></TBS-UP><PLB></PLB><UNLOAD-START></UNLOAD-START><FTRIP></FTRIP><NEXT-TRIP></NEXT-TRIP><LTRIP></LTRIP><UNLOAD-END></UNLOAD-END><FTRIP-B></FTRIP-B><FBAG></FBAG><NEXT-TRIP-B></NEXT-TRIP-B><LTRIP-B></LTRIP-B><LBAG></LBAG>";
        //        elem["URNO"].InnerText = flightURNO;
        //        elem[node].InnerText = time;

        //        myXmlDocument.DocumentElement.AppendChild(elem);


        //    }
        //    myXmlDocument.Save(iTPaths.GetArrFlightsTimingFilePath());


        //}


        /* Update Departue timing received from backend.Called by GetReadService*/


        //public void updateDepartureTimings(string flightURNO, string node, string time)
        //{

        //    XmlDocument myXmlDocument = new XmlDocument();
        //    myXmlDocument.Load(iTPaths.GetDepFlightsTimingFilePath());
        //    XmlNode timeNode = myXmlDocument.SelectSingleNode("//DEPARTUREFLIGHT/DFLIGHT[URNO=" + flightURNO + "]");
        //    if (timeNode != null)
        //    {
        //        int count = timeNode.ChildNodes.Count;
        //        for (int i = 1; i < count; i++)
        //        {
        //            if (timeNode.ChildNodes[i].Name == node)
        //            {

        //                timeNode.ChildNodes[i].InnerXml = time;
        //            }
        //        }


        //    }
        //    else
        //    {
        //        XmlElement elem = myXmlDocument.CreateElement("DFLIGHT");
        //        elem.InnerXml = "<URNO></URNO><MAB></MAB><LOAD-START></LOAD-START><LOAD-END></LOAD-END><TRIPS></TRIPS><LAST-DOOR></LAST-DOOR><PLB></PLB><ATD></ATD><FTRIP></FTRIP><NEXT-TRIP></NEXT-TRIP><LTRIP></LTRIP>";
        //        elem["URNO"].InnerText = flightURNO;
        //        elem[node].InnerText = time;

        //        myXmlDocument.DocumentElement.AppendChild(elem);


        //    }
        //    myXmlDocument.Save(iTPaths.GetDepFlightsTimingFilePath());


        //}


        

      
       

       
        public string GetMsgActionFromMsg(string message)
        {
            string messageType = "";
            try
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(message);
                string eleName = xmldoc.DocumentElement.Name;
                switch (xmldoc.DocumentElement.Name)
                {
                    case "MSGS":
                        messageType = "MSGS";
                        break;
                    case "MSGSTO":
                        messageType = "MSGSTO";
                        break;
                    case "MSGLOGS":
                        messageType = "MSGLOGS";
                        break;
                    case "MSGTO":
                        messageType = "UPDATE";
                        break;
                    case "SMSGD":
                        messageType = "SMSGD";
                        break; 
                    default:
                        messageType = xmldoc.SelectSingleNode("//MSG/ACTION").InnerXml;
                        break;
                }
            }
            catch (Exception ex)
            {
                messageType = ex.Message;
            }
            return messageType;
        }

        //public string createNewMsgIntoMsgXml(string message)
        //{
        //    string status = "";
        //    string messageType = "";
        //    try
        //    {
        //        XmlDocument xmldoc = new XmlDocument();
        //        xmldoc.LoadXml(message);

        //        if (xmldoc.DocumentElement.Name == "MSGS")
        //        {
        //            message = xmldoc.DocumentElement.InnerXml;
        //            messageType = "MSG";
        //        }
        //        else
        //            if (xmldoc.DocumentElement.Name == "MSGSTO")
        //            {
        //                message = xmldoc.DocumentElement.InnerXml;
        //                messageType = "MSGTO";
        //            }



        //        string strToPutInXml = "<?xml version=\"1.0\" standalone=\"yes\"?><DSMsg xmlns=\"http://tempuri.org/DSMsg.xsd\">" + message + "</DSMsg>";

        //        XmlDocument MainBatchMsgsXml = new XmlDocument();
        //        MainBatchMsgsXml.LoadXml(strToPutInXml);
        //        lock (_lockObj)
        //        {
        //            if (messageType == "MSG")
        //            {
        //                MainBatchMsgsXml.Save(iTPaths.GetMsgFile());
        //            }
        //            else
        //            {

        //                MainBatchMsgsXml.Save(iTPaths.GetMsgToFile());

        //            } 
        //        }
        //        status = "success";


        //    }
        //    catch (Exception ex)
        //    {

        //        status = ex.Message;

        //    }

        //    return status;

        //}
        /* Read Next ten Flights for a Baggage Officer*/

//        public ArrayList ReadNextTenFlightElementsFromCurrentFlightXMLFile(string expression, string filePath, string fltType, string orgcode)
//        {
//            ArrayList al = new ArrayList();
//            XmlNodeList FlightList;
//            XmlDocument myXmlDocument = new XmlDocument();
//            myXmlDocument.Load(filePath);
//            FlightList = myXmlDocument.SelectNodes(expression);
//            string et = "";
//            if (FlightList.Count > 0)
//            {
//                foreach (XmlNode FlightNode in FlightList)
//                {
//                    et = "";
//                    //"SQ020","stod", "stoa","regn","des3","ETDI", "ETOA", "EQPS", ADID, POS1, "urno"));
//                    if (FlightNode.ChildNodes[11].InnerXml == fltType)
//                    {
//                        if (fltType == "D")
//                        {
//                            if (FlightNode.ChildNodes[18].InnerXml == orgcode)
//                            {
//                                if (FlightNode.ChildNodes[7].InnerXml != "")
//                                {
//                                    et = FlightNode.ChildNodes[7].InnerXml;
//                                }
//                                else
//                                {
//                                    et = FlightNode.ChildNodes[2].InnerXml;
//                                }
//                                //if ((FlightNode.ChildNodes[2].InnerXml).CompareTo(iTrekUtils.iTUtils.convertStandardDateTimeToOracleFormat(DateTime.Now.ToString())) == 1)
//                              //  if (string.Compare(FlightNode.ChildNodes[2].InnerXml, iTrekUtils.iTUtils.ConvDateTimeToUfisFullDTString(DateTime.Now)) == 1)
//                                if (string.Compare(et, iTrekUtils.iTUtils.ConvDateTimeToUfisFullDTString(DateTime.Now)) == 1)
//                                {
//                                    al.Add(new Flight(FlightNode.ChildNodes[0].InnerXml, FlightNode.ChildNodes[1].InnerXml, FlightNode.ChildNodes[2].InnerXml, FlightNode.ChildNodes[3].InnerXml, FlightNode.ChildNodes[4].InnerXml, FlightNode.ChildNodes[5].InnerXml, FlightNode.ChildNodes[6].InnerXml, FlightNode.ChildNodes[7].InnerXml, FlightNode.ChildNodes[8].InnerXml, FlightNode.ChildNodes[9].InnerXml, FlightNode.ChildNodes[10].InnerXml, FlightNode.ChildNodes[11].InnerXml, FlightNode.ChildNodes[12].InnerXml, FlightNode.ChildNodes[13].InnerXml, FlightNode.ChildNodes[14].InnerXml, FlightNode.ChildNodes[15].InnerXml, FlightNode.ChildNodes[16].InnerXml, FlightNode.ChildNodes[17].InnerXml, FlightNode.ChildNodes[18].InnerXml, FlightNode.ChildNodes[19].InnerXml, FlightNode.ChildNodes[20].InnerXml,""));
//                                }
//#if LoggingOn
//                                //iTUtils.LogToEventLog("In Next Ten Dep" + FlightNode.ChildNodes[1].InnerXml);
//                                //iTUtils.LogToEventLog("In Next Ten Dep" + FlightNode.ChildNodes[2].InnerXml);
//                                //iTUtils.LogToEventLog("In Next Ten Dep" + iTrekUtils.iTUtils.ConvDateTimeToUfisFullDTString(DateTime.Now));
//                                //iTUtils.LogToEventLog("In Next Ten Dep" + string.Compare(FlightNode.ChildNodes[2].InnerXml, iTrekUtils.iTUtils.ConvDateTimeToUfisFullDTString(DateTime.Now)));
//#endif
//                            }
//                        }
//                        else
//                        {
//                            if (FlightNode.ChildNodes[17].InnerXml == orgcode)
//                            {
//                                if (FlightNode.ChildNodes[8].InnerXml != "")
//                                {
//                                    et = FlightNode.ChildNodes[8].InnerXml;
//                                }
//                                else
//                                {
//                                    et = FlightNode.ChildNodes[3].InnerXml;
//                                }
//                                //if ((FlightNode.ChildNodes[3].InnerXml).CompareTo(iTrekUtils.iTUtils.convertStandardDateTimeToOracleFormat(DateTime.Now.ToString())) == 1)
//                                //if (string.Compare(FlightNode.ChildNodes[3].InnerXml, iTrekUtils.iTUtils.ConvDateTimeToUfisFullDTString(DateTime.Now)) == 1)
//                                if (string.Compare(et, iTrekUtils.iTUtils.ConvDateTimeToUfisFullDTString(DateTime.Now)) == 1)
//                                {
//                                    al.Add(new Flight(FlightNode.ChildNodes[0].InnerXml, FlightNode.ChildNodes[1].InnerXml, FlightNode.ChildNodes[2].InnerXml, FlightNode.ChildNodes[3].InnerXml, FlightNode.ChildNodes[4].InnerXml, FlightNode.ChildNodes[5].InnerXml, FlightNode.ChildNodes[6].InnerXml, FlightNode.ChildNodes[7].InnerXml, FlightNode.ChildNodes[8].InnerXml, FlightNode.ChildNodes[9].InnerXml, FlightNode.ChildNodes[10].InnerXml, FlightNode.ChildNodes[11].InnerXml, FlightNode.ChildNodes[12].InnerXml, FlightNode.ChildNodes[13].InnerXml, FlightNode.ChildNodes[14].InnerXml, FlightNode.ChildNodes[15].InnerXml, FlightNode.ChildNodes[16].InnerXml, FlightNode.ChildNodes[17].InnerXml, FlightNode.ChildNodes[18].InnerXml, FlightNode.ChildNodes[19].InnerXml, FlightNode.ChildNodes[20].InnerXml,""));
//                                }
//#if LoggingOn
//                                //iTUtils.LogToEventLog("In Next Ten Arr" + FlightNode.ChildNodes[1].InnerXml);
//                                //iTUtils.LogToEventLog("In Next Ten Arr" + FlightNode.ChildNodes[3].InnerXml);
//                                //iTUtils.LogToEventLog("In Next Ten Dep" + iTrekUtils.iTUtils.ConvDateTimeToUfisFullDTString(DateTime.Now));
//                                //iTUtils.LogToEventLog("In Next Ten Arr" + string.Compare(FlightNode.ChildNodes[3].InnerXml,iTrekUtils.iTUtils.ConvDateTimeToUfisFullDTString(DateTime.Now)));
//#endif
//                            }

//                        }
//                    }
//                }
//            }
//            //only fetch last 3 flights

//           /* if (al.Count != 0)
//            {
//                al.Sort();
//                if (al.Count < 11)
//                    return al;
//                else
//                {
//                    al.RemoveRange(10, al.Count - 10);
//                    return al;
//                }
//            }*/
//            al.Sort();
//            return al;
//        }



        /* Read Previous ten Flights for a Baggage Officer*/


//        public ArrayList ReadPreviousTenFlightElementsFromCurrentFlightXMLFile(string expression, string filePath, string fltType, string orgcode)
//        {
//            ArrayList al = new ArrayList();
//            XmlNodeList FlightList;
//            XmlDocument myXmlDocument = new XmlDocument();
//            myXmlDocument.Load(filePath);
//            FlightList = myXmlDocument.SelectNodes(expression);
//            string et = "";
//            if (FlightList.Count > 0)
//            {
//                foreach (XmlNode FlightNode in FlightList)
//                {
//                    et = "";
//                    //"SQ020","stod", "stoa","regn","des3","ETDI", "ETOA", "EQPS", ADID, POS1, "urno"));
//                    if (FlightNode.ChildNodes[11].InnerXml == fltType)
//                    {
//                        if (fltType == "D")
//                        {
//                            if (FlightNode.ChildNodes[18].InnerXml == orgcode)
//                            {
//                                if (FlightNode.ChildNodes[7].InnerXml != "")
//                                {
//                                    et = FlightNode.ChildNodes[7].InnerXml;
//                                }
//                                else
//                                {
//                                    et = FlightNode.ChildNodes[2].InnerXml;
//                                }
//                                // if ((FlightNode.ChildNodes[2].InnerXml).CompareTo(iTrekUtils.iTUtils.convertStandardDateTimeToOracleFormat(DateTime.Now.ToString())) < 1)
//                              //  if (string.Compare(FlightNode.ChildNodes[2].InnerXml, iTrekUtils.iTUtils.ConvDateTimeToUfisFullDTString(DateTime.Now)) < 1)
//                                if (string.Compare(et, iTrekUtils.iTUtils.ConvDateTimeToUfisFullDTString(DateTime.Now)) < 1)
//                                {
//                                    al.Add(new Flight(FlightNode.ChildNodes[0].InnerXml, FlightNode.ChildNodes[1].InnerXml, FlightNode.ChildNodes[2].InnerXml, FlightNode.ChildNodes[3].InnerXml, FlightNode.ChildNodes[4].InnerXml, FlightNode.ChildNodes[5].InnerXml, FlightNode.ChildNodes[6].InnerXml, FlightNode.ChildNodes[7].InnerXml, FlightNode.ChildNodes[8].InnerXml, FlightNode.ChildNodes[9].InnerXml, FlightNode.ChildNodes[10].InnerXml, FlightNode.ChildNodes[11].InnerXml, FlightNode.ChildNodes[12].InnerXml, FlightNode.ChildNodes[13].InnerXml, FlightNode.ChildNodes[14].InnerXml, FlightNode.ChildNodes[15].InnerXml, FlightNode.ChildNodes[16].InnerXml, FlightNode.ChildNodes[17].InnerXml, FlightNode.ChildNodes[18].InnerXml, FlightNode.ChildNodes[19].InnerXml, FlightNode.ChildNodes[20].InnerXml,""));

//                                }
//#if LoggingOn
//                                //iTUtils.LogToEventLog("In Prev Ten Dep" + FlightNode.ChildNodes[1].InnerXml);
//                                //iTUtils.LogToEventLog("In Prev Ten Dep" + FlightNode.ChildNodes[2].InnerXml);
//                                //iTUtils.LogToEventLog("In Next Ten Dep" + iTrekUtils.iTUtils.ConvDateTimeToUfisFullDTString(DateTime.Now));
//                                //iTUtils.LogToEventLog("In Prev Ten Dep" + string.Compare(FlightNode.ChildNodes[2].InnerXml,iTrekUtils.iTUtils.ConvDateTimeToUfisFullDTString(DateTime.Now)));
//#endif


//                            }
//                        }

//                        else
//                        {
//                            if (FlightNode.ChildNodes[17].InnerXml == orgcode)
//                            {
//                                if (FlightNode.ChildNodes[8].InnerXml != "")
//                                {
//                                    et = FlightNode.ChildNodes[8].InnerXml;
//                                }
//                                else
//                                {
//                                    et = FlightNode.ChildNodes[3].InnerXml;
//                                }
//                                // if ((FlightNode.ChildNodes[3].InnerXml).CompareTo(iTrekUtils.iTUtils.convertStandardDateTimeToOracleFormat(DateTime.Now.ToString())) < 1)
//                              //  if (string.Compare(FlightNode.ChildNodes[3].InnerXml, iTrekUtils.iTUtils.ConvDateTimeToUfisFullDTString(DateTime.Now)) < 1)
//                                if (string.Compare(et, iTrekUtils.iTUtils.ConvDateTimeToUfisFullDTString(DateTime.Now)) < 1)
//                                {
//                                    al.Add(new Flight(FlightNode.ChildNodes[0].InnerXml, FlightNode.ChildNodes[1].InnerXml, FlightNode.ChildNodes[2].InnerXml, FlightNode.ChildNodes[3].InnerXml, FlightNode.ChildNodes[4].InnerXml, FlightNode.ChildNodes[5].InnerXml, FlightNode.ChildNodes[6].InnerXml, FlightNode.ChildNodes[7].InnerXml, FlightNode.ChildNodes[8].InnerXml, FlightNode.ChildNodes[9].InnerXml, FlightNode.ChildNodes[10].InnerXml, FlightNode.ChildNodes[11].InnerXml, FlightNode.ChildNodes[12].InnerXml, FlightNode.ChildNodes[13].InnerXml, FlightNode.ChildNodes[14].InnerXml, FlightNode.ChildNodes[15].InnerXml, FlightNode.ChildNodes[16].InnerXml, FlightNode.ChildNodes[17].InnerXml, FlightNode.ChildNodes[18].InnerXml, FlightNode.ChildNodes[19].InnerXml, FlightNode.ChildNodes[20].InnerXml,""));
//                                }
//#if LoggingOn
//                                //iTUtils.LogToEventLog("In Prev Ten Arr" + FlightNode.ChildNodes[1].InnerXml);
//                                //iTUtils.LogToEventLog("In Prev Ten Arr" + FlightNode.ChildNodes[3].InnerXml);
//                                //iTUtils.LogToEventLog("In Next Ten Dep" + iTrekUtils.iTUtils.ConvDateTimeToUfisFullDTString(DateTime.Now));
//                                //iTUtils.LogToEventLog("In Prev Ten Arr" + string.Compare(FlightNode.ChildNodes[3].InnerXml,iTrekUtils.iTUtils.ConvDateTimeToUfisFullDTString(DateTime.Now)));
//#endif


//                            }
//                        }
//                    }
//                }
//            }
//            //only fetch last 3 flights

//           /* if (al.Count != 0)
//            {
//                al.Sort();
//                if (al.Count < 11)
//                    return al;
//                else
//                {
//                    al.RemoveRange(0, al.Count - 10);
//                    return al;
//                }
//            }*/
//            al.Sort();
//            return al;
//        }
//        /*
         //* Get the count of containers for preparing the container 
         //* 
         //*
         //**/

        //public void getCountofContainerInContainerXml(string fltUrno)
        //{
        //    XmlDocument myXmlDocument = new XmlDocument();
        //    myXmlDocument.Load(iTPaths.GetContainerFile());
        //    XmlNode containerNode = myXmlDocument.SelectSingleNode("//CONTAINERS/CONTAINER[UAFT=" + fltUrno + "]");



        //}
        /*
         * Add Node to Container.xml
         * 
         * 
         * */
        //public void createContainerXmlForFlight(string fltUrno, ArrayList alContainer)
        //{
        //    int containerCnt = alContainer.Count;


        //    XmlDocument myXmlDocument = new XmlDocument();
        //    myXmlDocument.Load(iTPaths.GetContainerFile());
        //    XmlNode containerNode = myXmlDocument.SelectSingleNode("//CONTAINERS/CONTAINER[UAFT=" + fltUrno + "]");
        //    XmlNode txt;
        //    bool isUldPresent = false;

        //    if (containerNode != null)
        //    {
        //        for (int i = 0; i < containerCnt; i++)
        //        {
        //            XmlNodeList testNode = containerNode.LastChild.ChildNodes;
        //            foreach (XmlNode ulNode in testNode)
        //            {
        //                if (!isUldPresent)
        //                {
        //                    if (ulNode.InnerText== alContainer[i].ToString())
        //                    {
        //                        isUldPresent = true;

        //                    }
        //                }

        //            }
        //           if(!isUldPresent)
        //           {
        //            txt = myXmlDocument.CreateNode(XmlNodeType.Element, "ULDNO", "");
        //            txt.InnerText = alContainer[i].ToString();                    
        //            containerNode["ULD"].AppendChild(txt);
        //           }


        //        }


        //    }
        //    else
        //    {
        //        string uldNode = "";
        //        for (int i = 0; i < containerCnt; i++)
        //        {
        //            uldNode += "<ULDNO>" + alContainer[i].ToString() + "</ULDNO>";
        //        }
        //        XmlElement elem = myXmlDocument.CreateElement("CONTAINER");           
        //        elem.InnerXml = "<UAFT></UAFT><ULD>" + uldNode.Trim() + "</ULD>";
        //        elem["UAFT"].InnerText = fltUrno;
        //       // elem["ULD"].AppendChild(txt);
        //        //XmlNodeList uldNodeList = elem["ULD"].ChildNodes;
        //        //int j = 0;
        //        //foreach (XmlNode uldxmlNode in uldNodeList)
        //        //{

        //        //    if (alContainer[j].ToString() != "")
        //        //    {
        //        //        uldxmlNode.InnerText = alContainer[j].ToString();
        //        //    }
        //        //    j++;

        //        //}

        //        myXmlDocument.DocumentElement.AppendChild(elem);


        //    }
        //    myXmlDocument.Save(iTPaths.GetContainerFile());
        //}

        /* Create Container info for a Particular Flight*/


        //public void createContainerXmlForFlight(string fltUrno, ArrayList alContainer)
        //{
        //    int containerCnt = alContainer.Count;


        //    XmlDocument myXmlDocument = new XmlDocument();
        //    myXmlDocument.Load(iTPaths.GetContainerFile());
        //    XmlNode containerNode = myXmlDocument.SelectSingleNode("//CONTAINERS/CONTAINER[UAFT=" + fltUrno + "]");
        //    XmlNode txt;
        //    bool isUldPresent = false;

        //    if (containerNode != null)
        //    {
        //        for (int i = 0; i < containerCnt; i++)
        //        {
        //            XmlNodeList testNode = containerNode.ChildNodes[1].ChildNodes;

        //            foreach (XmlNode uldNode in testNode)
        //            {
        //                if (!isUldPresent)
        //                {
        //                    if (uldNode.ChildNodes[0].InnerText == alContainer[i].ToString())
        //                    {
        //                        isUldPresent = true;

        //                    }
        //                }
        //            }



        //            if (!isUldPresent)
        //            {
        //                txt = myXmlDocument.CreateNode(XmlNodeType.Element, "ULDI", "");
        //                txt.InnerXml = "<ULDNO>" + iTrekUtils.iTUtils.removeBadCharacters(alContainer[i].ToString()) + "</ULDNO><SEND_IND>N</SEND_IND>";
        //                containerNode["ULD"].AppendChild(txt);
        //            }
        //            isUldPresent = false;

        //        }


        //    }
        //    else
        //    {
        //        string uldNode = "";
        //        for (int i = 0; i < containerCnt; i++)
        //        {
        //            uldNode += "<ULDI><ULDNO>" + iTrekUtils.iTUtils.removeBadCharacters(alContainer[i].ToString()) + "</ULDNO><SEND_IND>N</SEND_IND></ULDI>";
        //        }
        //        XmlElement elem = myXmlDocument.CreateElement("CONTAINER");
        //        elem.InnerXml = "<UAFT></UAFT><ULD>" + uldNode.Trim() + "</ULD>";
        //        elem["UAFT"].InnerText = fltUrno;
        //        // elem["ULD"].AppendChild(txt);
        //        //XmlNodeList uldNodeList = elem["ULD"].ChildNodes;
        //        //int j = 0;
        //        //foreach (XmlNode uldxmlNode in uldNodeList)
        //        //{

        //        //    if (alContainer[j].ToString() != "")
        //        //    {
        //        //        uldxmlNode.InnerText = alContainer[j].ToString();
        //        //    }
        //        //    j++;

        //        //}

        //        myXmlDocument.DocumentElement.AppendChild(elem);


        //    }
        //    myXmlDocument.Save(iTPaths.GetContainerFile());
        //}

        /* Get the list of Containers for a particular flightUrno*/


        //public DataSet getContainerDetailsFromXML(string flightUrno)
        //{
        //    DataSet ds = new DataSet("CONTAINER");
        //    DataTable dt = ds.Tables.Add("CONTAINER");
        //    dt.Columns.Add("ULDNO");
        //    DataRow dr;
        //    //ds.
        //    //Hashtable timingHt = new Hashtable();
        //    SortedList st = new SortedList();
        //    XmlDocument myXmlDocument = new XmlDocument();
        //    myXmlDocument.Load(iTPaths.GetContainerFile());
        //    XmlNode uldNode = myXmlDocument.SelectSingleNode("//CONTAINERS/CONTAINER[UAFT=" + flightUrno + "]");
        //    // ds.ReadXml("C:\\Program Files\\Ufis\\iTrekOk2Load\\XML\\DepartureFlightTimings.XML",XmlReadMode.ReadSchema);
        //    //int k= ds.Tables.Count;
        //    int maxCnt = 20;
        //    if (uldNode != null)
        //    {
        //        int count = uldNode["ULD"].ChildNodes.Count;
        //        for (int i = 0; i < count; i++)
        //        {

        //            if (uldNode["ULD"].ChildNodes[i].LastChild.InnerXml == "N")
        //            {
        //                dr = dt.NewRow();
        //                dr["ULDNO"] = uldNode["ULD"].ChildNodes[i].FirstChild.InnerXml;
        //                ds.Tables["CONTAINER"].Rows.Add(dr);
        //                ds.Tables["CONTAINER"].AcceptChanges();
        //                maxCnt--;
                        
        //            }
        //            if (maxCnt <= 0) break;
        //            // st.Add(timeNode.ChildNodes[i].Name, timeNode.ChildNodes[i].InnerXml);
        //            // timingHt.Add(timeNode.ChildNodes[i].Name, timeNode.ChildNodes[i].InnerXml);
        //        }


        //    }
        //    string test = ds.GetXml();
        //    return ds;

        //}



        //public DataSet getContainerDetailsFromXML(string flightUrno)
        //{
        //    DataSet ds = new DataSet("CONTAINER");
        //    DataTable dt = ds.Tables.Add("CONTAINER");
        //    dt.Columns.Add("ULDNO");          
        //    DataRow dr;
        //    //ds.
        //    //Hashtable timingHt = new Hashtable();
        //    SortedList st = new SortedList();
        //    XmlDocument myXmlDocument = new XmlDocument();
        //    myXmlDocument.Load(iTPaths.GetContainerFile());
        //    XmlNode uldNode = myXmlDocument.SelectSingleNode("//CONTAINERS/CONTAINER[UAFT=" + flightUrno + "]");
        //    // ds.ReadXml("C:\\Program Files\\Ufis\\iTrekOk2Load\\XML\\DepartureFlightTimings.XML",XmlReadMode.ReadSchema);
        //    //int k= ds.Tables.Count;
        //    if (uldNode != null)
        //    {
        //        int count = uldNode["ULD"].ChildNodes.Count;
        //        for (int i = 0; i < count; i++)
        //        {
        //            dr = dt.NewRow();

        //            dr["ULDNO"] = uldNode["ULD"].ChildNodes[i].InnerXml;
        //            ds.Tables["CONTAINER"].Rows.Add(dr);
        //        ds.Tables["CONTAINER"].AcceptChanges();
        //            // st.Add(timeNode.ChildNodes[i].Name, timeNode.ChildNodes[i].InnerXml);
        //            // timingHt.Add(timeNode.ChildNodes[i].Name, timeNode.ChildNodes[i].InnerXml);
        //        }


        //    }
        //    string test = ds.GetXml();
        //    return ds;

        //}


        //public void deleteUldsFromContainerXml(string fltUrno, ArrayList alContainer)
        //{
        //    lock (_lockObj)
        //    {
        //        int containerCnt = alContainer.Count;
        //        XmlDocument myXmlDocument = new XmlDocument();
        //        myXmlDocument.Load(iTPaths.GetContainerFile());
        //        XmlNode containerNode = myXmlDocument.SelectSingleNode("//CONTAINERS/CONTAINER[UAFT=" + fltUrno + "]");
        //        XmlNodeList uldNodeList = containerNode["ULD"].ChildNodes;
        //        foreach (string uld in alContainer)
        //        {
        //            foreach (XmlNode uldxmlNode in uldNodeList)
        //            {

        //                if (uldxmlNode.FirstChild.InnerText == uld)
        //                {
        //                    uldxmlNode.LastChild.InnerText = "Y";
        //                    // containerNode["ULD"].RemoveChild(uldxmlNode);

        //                }
        //            }

        //        }

        //        myXmlDocument.Save(iTPaths.GetContainerFile()); 
        //    }
        //}
        /* Update send indicator in CONTAINER.xml once this container is selected and send to backend*/

        //public void updateUldsFromContainerXml(string fltUrno, ArrayList alContainer)
        //{
        //    int containerCnt = alContainer.Count;
        //    XmlDocument myXmlDocument = new XmlDocument();
        //    myXmlDocument.Load(iTPaths.GetContainerFile());
        //    XmlNode containerNode = myXmlDocument.SelectSingleNode("//CONTAINERS/CONTAINER[UAFT=" + fltUrno + "]");
        //    if (containerNode != null)
        //    {
        //        XmlNodeList uldNodeList = containerNode["ULD"].ChildNodes;
        //        foreach (string uld in alContainer)
        //        {
        //            foreach (XmlNode uldxmlNode in uldNodeList)
        //            {

        //                if (uldxmlNode.FirstChild.InnerText == uld)
        //                {
        //                    uldxmlNode.LastChild.InnerText = "Y";
        //                    // containerNode["ULD"].RemoveChild(uldxmlNode);

        //                }
        //            }

        //        }
        //        myXmlDocument.Save(iTPaths.GetContainerFile());
        //    }

            
        //}


        //public void deleteUldsFromContainerXml(string fltUrno, ArrayList alContainer)
        //{
        //    int containerCnt = alContainer.Count;
        //    XmlDocument myXmlDocument = new XmlDocument();
        //    myXmlDocument.Load(iTPaths.GetContainerFile());
        //    XmlNode containerNode = myXmlDocument.SelectSingleNode("//CONTAINERS/CONTAINER[UAFT=" + fltUrno + "]");
        //    if (containerNode != null)
        //    {
        //        XmlNodeList uldNodeList = containerNode["ULD"].ChildNodes;
        //        foreach (string uld in alContainer)
        //        {
        //            foreach (XmlNode uldxmlNode in uldNodeList)
        //            {

        //                if (uldxmlNode.FirstChild.InnerText == uld)
        //                {
        //                    containerNode["ULD"].RemoveChild(uldxmlNode);
        //                    // containerNode["ULD"].RemoveChild(uldxmlNode);

        //                }
        //            }

        //        }
        //        myXmlDocument.Save(iTPaths.GetContainerFile());
        //    }


        //}

        public string DeleteActionFromMsgXMLFile(string MsgXML)
        {
            XmlDocument msgXMLdoc = TransformStringToXmlDoc(MsgXML);
            string expression = "//MSG/ACTION";
            XmlNode xmlNode = msgXMLdoc.SelectSingleNode(expression);
            msgXMLdoc.DocumentElement.RemoveChild(xmlNode);
            return msgXMLdoc.OuterXml;
        }

        public string DeleteActionFromMsgToXMLFile(string MsgXML)
        {
            XmlDocument msgXMLdoc = TransformStringToXmlDoc(MsgXML);
            string expression = "//MSGTO/ACTION";
            XmlNode xmlNode = msgXMLdoc.SelectSingleNode(expression);
            msgXMLdoc.DocumentElement.RemoveChild(xmlNode);
            return msgXMLdoc.OuterXml;
        }

        public bool getUpdateStatus(string processedTime, string depTime)
        {
            bool status = true;
            try
            {
                string convertedDepTime = "";
                int ETDAlertTime = Convert.ToInt32(iTPaths.GetETDALERTTIME());
                TimeSpan TenMinuteTimeSpan = new TimeSpan(0, ETDAlertTime, 0);
                TimeSpan ETDMinusCurrentTime = new TimeSpan();
                convertedDepTime = iTrekUtils.iTUtils.ConvertDateFromOracleFormat(depTime);

                ETDMinusCurrentTime = Convert.ToDateTime(convertedDepTime) - Convert.ToDateTime(processedTime);
                if (ETDMinusCurrentTime > TenMinuteTimeSpan)
                {
                    status = false;

                }
                else
                {
                    status = true;

                }
            }
            catch (Exception statEx)
            {
                status = false;
                iTrekUtils.iTUtils.LogToEventLog("getUpdateStatus" + statEx.Message, "iTrekError");


            }

            return status;
        }


        public bool getUpdateStatus(DateTime processedTime, DateTime depTime)
        {
            bool status = true;
            try
            {
                string convertedDepTime = "";
                int ETDAlertTime = Convert.ToInt32(iTPaths.GetETDALERTTIME());
                TimeSpan TenMinuteTimeSpan = new TimeSpan(0, ETDAlertTime, 0);
                TimeSpan ETDMinusCurrentTime = new TimeSpan();
                //convertedDepTime = iTrekUtils.iTUtils.ConvertDateFromOracleFormat(depTime);

                ETDMinusCurrentTime =depTime - processedTime;
                if (ETDMinusCurrentTime > TenMinuteTimeSpan)
                {
                    status = false;

                }
                else
                {
                    status = true;

                }
            }
            catch (Exception statEx)
            {
                status = false;
                iTrekUtils.iTUtils.LogToEventLog("getUpdateStatus" + statEx.Message, "iTrekError");
               //iTrekUtils.iTUtils.LogToEventLog("getUpdateStatus" + statEx.StackTrace, "iTrekError");

            }

            return status;
        }

        public ArrayList GetMatchingFlightsFromMainJobsXMLFile(string expression, string staffid, XmlDocument jobXml)
        {
            ArrayList al = new ArrayList();


            XmlNodeList JobList;
            string status = "NOK";

            // XmlDocument myXmlDocument = new XmlDocument();
            // myXmlDocument.Load(filePath);
            JobList = jobXml.SelectNodes(expression);

            if (JobList.Count > 0)
            {
                foreach (XmlNode JobNode in JobList)
                {
                    if (JobNode.ChildNodes[1].InnerXml == staffid)
                    {
                        XmlNode flightAssign = JobNode.ChildNodes[4];
                        al.Add(flightAssign.InnerXml);
                    }
                }
            }


            return al;

        }
        /* Housekeeping of CONTAINER.xml.Called by iTrekDelSessionSvc*/


        //public void deleteOldUldsFromContainerXml(ArrayList alflUrno)
        //{

        //    XmlDocument myXmlDocument = new XmlDocument();
        //    myXmlDocument.Load(iTPaths.GetContainerFile());
        //    foreach (string urno in alflUrno)
        //    {
        //        XmlNode containerNode = myXmlDocument.SelectSingleNode("//CONTAINERS/CONTAINER[UAFT=" + urno + "]");
        //        if (containerNode != null)
        //        {

        //            myXmlDocument.DocumentElement.RemoveChild(containerNode);
        //        }



        //    }


        //    myXmlDocument.Save(iTPaths.GetContainerFile());
        //}


        /* Housekeeping of DEPARTUREFLIGHTTIMING.xml.Called by iTrekDelSessionSvc*/

        //public void deleteOldFlightsFromDepTimings(ArrayList alflUrno)
        //{

        //    lock (_lockObj)
        //    {
        //        XmlDocument myXmlDocument = new XmlDocument();
        //        myXmlDocument.Load(iTPaths.GetDepFlightsTimingFilePath());
        //        foreach (string urno in alflUrno)
        //        {
        //            XmlNode timeNode = myXmlDocument.SelectSingleNode("//DEPARTUREFLIGHT/DFLIGHT[URNO=" + urno + "]");
        //            if (timeNode != null)
        //            {
        //                myXmlDocument.DocumentElement.RemoveChild(timeNode);
        //            }



        //        }


        //        myXmlDocument.Save(iTPaths.GetDepFlightsTimingFilePath());
        //    }
        //}



        /* Housekeeping of ARRIVALFLIGHTTIMING.xml.Called by iTrekDelSessionSvc*/

        //public void deleteOldFlightsFromArrTimings(ArrayList alflUrno)
        //{

        //    lock (_lockObj)
        //    {
        //        XmlDocument myXmlDocument = new XmlDocument();
        //        myXmlDocument.Load(iTPaths.GetArrFlightsTimingFilePath());
        //        foreach (string urno in alflUrno)
        //        {
        //            XmlNode timeNode = myXmlDocument.SelectSingleNode("//ArrivalFlight/AFLIGHT[URNO=" + urno + "]");

        //            if (timeNode != null)
        //            {
        //                myXmlDocument.DocumentElement.RemoveChild(timeNode);
        //            }


        //        }


        //        myXmlDocument.Save(iTPaths.GetArrFlightsTimingFilePath());
        //    }
        //}
        

        public void updateMessageDetails(string msgToXml)
        {


            //message = iTXML.DeleteActionFromMsgXMLFile(msgToXml);
            //        XmlDocument updMsgXML = new XmlDocument();
            //        updMsgXML.LoadXml(msgToXml);
            //        if (dsMsgTo == null)
            //        {
            //            dsMsgTo = CtrlMsg.returnDSMessage("MSGTO");

            //        }
            //       string msgUrno = updMsgXML.ChildNodes[0].InnerText;
            //        foreach(DSMsg.MSGTORow msgtorow in ds.MSGTO.Select("URNO='" + msgUrno + "'"))
            //        {

            //            dsMsgTo.MSGTO.RemoveMSGTORow(msgtorow);
            //            dsMsgTo.MSGTO.AcceptChanges();
            //            DSMsg.MSGTORow msgTorow = dsMsgTo.MSGTO.NewMSGTORow();
            //            int msgTorowcnt =updMsgXML.ChildNodes.Count;

            //            for (int j = 0; j < msgTorowcnt; j++)
            //            {

            //                msgTorow[j] = updMsgXML.ChildNodes[j].InnerText;

            //            }
            //            dsMsgTo.MSGTO.Rows.Add(msgTorow);
            //            dsMsgTo.MSGTO.AcceptChanges();
            //        }



        }

    }
}
