//#define TestsOn
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Xml;
using System.Xml.XPath;
using System.IO;
#if TestsOn
using NUnit.Framework;
#endif
using iTrekXML;
using System.Web;

#if TestsOn
namespace iTrekXMLPathsTests
{
    [TestFixture]
    public class iTXMLPathsclTests
    {
        iTXMLPathscl iTPaths;

        //[SetUp]
        public iTXMLPathsclTests()
        {
            //TODO: - Change for deployment
            iTPaths = new iTXMLPathscl();
        }

        //TODO: - Coverage - High
        [Test]
        public void ReadPathsXMLFileTest()
        {
            //OkToLoad path is used as test
            if (iTPaths.ApplicationMode == "Debug")
                //Assert.AreEqual(@"C:\LocalSandboxes\Ufis4.5\Ufis\Sin\SIN_Client\SIN_UfisAddOn\SIN_iTrek\PushServ\iTrekOk2L\bin\Debug\", iTPaths.GetOK2LoadPath());
                Assert.AreEqual(@"C:\Program Files\Ufis\iTrekOK2Load\bin\", iTPaths.GetOK2LoadPath());
            else if (iTPaths.ApplicationMode == "Test")
                Assert.AreEqual(@"F:\Program Files\Ufis\iTrekOK2Load\bin\", iTPaths.GetOK2LoadPath());
            else
                Assert.AreEqual(@"F:\Program Files\Ufis\iTrekOK2Load\bin\", iTPaths.GetOK2LoadPath());

        }

        //TODO: Coverage high
        [Test]
        public void ReadMultipleValuesFromAppConfigFile()
        {
            Assert.AreEqual(iTPaths.GetTOPUSHOK2LOADQueueName(), iTPaths.GetTOPUSHOK2LOADQueueName());
            Assert.AreEqual("http://203.126.249.148:9002/pap", iTPaths.GetPapURL());
        }

        //TODO: Coverage high
        [Test]
        public void GetETDSERVICESTIMERTest()
        {
            Assert.AreEqual("4000", iTPaths.GetETDSERVICESTIMER());
            
        }
        

    }
}
#endif

namespace iTrekXML
{
    /// <summary>
    /// Manages accessing the path data retained in the 
    /// Paths.xml file
    /// </summary>
    public class iTXMLPathscl
    {
        public string ApplicationMode = "";
        string PathsXMLFilePath = "";
        public string applicationBasePath = "";       

        string OK2LoadPath = "";
        string SessionManagerPath = "";
        string AllXMLPath = "";


        public iTXMLPathscl()
        {
            //Only one ApplicationMode can be active at a time
			ApplicationMode = "Debug";//DEV Server Environment
			//ApplicationMode = "Test";//UAT Server Environment
          // ApplicationMode = "live";//PROD Server Environment

            if (ApplicationMode == "Debug")
            {
                //PathsXMLFilePaths have to be hard coded
                //check Server.MapPath or equiv
                AllXMLPath = @"C:\Program Files\Ufis\iTrekOK2Load\XML\";
                
                PathsXMLFilePath = AllXMLPath + "Paths.xml";
                applicationBasePath = GetApplicationBasePath(PathsXMLFilePath, "//Paths/applicationBasePathDebug");
                OK2LoadPath = GetOK2LoadPath();
                SessionManagerPath = GetSessionManagerPath();
            }
            else if (ApplicationMode == "Test")
            {
                AllXMLPath = @"F:\Program Files\Ufis\iTrekOK2Load\XML\";
                PathsXMLFilePath = AllXMLPath + "Paths.xml";
                applicationBasePath = GetApplicationBasePath(PathsXMLFilePath, "//Paths/applicationBasePathTest");
                OK2LoadPath = GetConfigElmt("//Paths/TestReleasePaths/iTrekOk2LPath");
                SessionManagerPath = GetConfigElmt("//Paths/TestReleasePaths/iTrekSessionManagerPath");
            }
            else
            //live
            {
                AllXMLPath = @"F:\Program Files\Ufis\iTrekOK2Load\XML\";
                PathsXMLFilePath = AllXMLPath + "Paths.xml";
                applicationBasePath = GetApplicationBasePath(PathsXMLFilePath, "//Paths/applicationBasePathLive");
                OK2LoadPath = GetConfigElmt("//Paths/LiveReleasePaths/iTrekOk2LPath");
                SessionManagerPath = GetConfigElmt("//Paths/LiveReleasePaths/iTrekSessionManagerPath");
                //XMLPath = GetConfigElmt("//Paths/LiveReleasePaths/iTrekXMLPath");
            }
        }

        string GetApplicationBasePath(string XMLFilePathbyMode, string expression)
        {
            XPathDocument doc = new XPathDocument(XMLFilePathbyMode);
            XPathNavigator nav = doc.CreateNavigator();
            return nav.SelectSingleNode(expression).ToString();
        }


        public string GetConfigElmt(string expression)
        {
            XPathDocument doc = new XPathDocument(PathsXMLFilePath);
            XPathNavigator nav = doc.CreateNavigator();
            return nav.SelectSingleNode(expression).ToString();
        }


        public string GetOK2LoadPath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekOk2LPath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekOk2LPath");
        }


        public string GetFlightsFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekFlightsFilePath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekFlightsFilePath");
        }

    

        public string GetJobsFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekJobsFilePath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekJobsFilePath");
        }

        public string GetFlightsTestFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekFlightsTestFilePath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekFlightsTestFilePath");
        }

        public string GetJobsTestFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekJobsTestFilePath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekJobsTestFilePath");
        }

        public string GetSessionManagerPath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekSessionManagerPath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekSessionManagerPath");
        }

        public string GetXMLPath()
        {
            if (ApplicationMode == "Debug")
                //return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekXMLPath");
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekXMLPath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekXMLPath");
        }
        public string GetSessionXMLPath()
        {
            if (ApplicationMode == "Debug")
                //return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekXMLPath");
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekSessionXMLPath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekSessionXMLPath");
        }

        public string GetDeviceIDFile()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/DeviceID");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/DeviceID");
        }

        public string GetUpdateableFlightsFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekUpdateableFlightsFilePath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekUpdateableFlightsFilePath");
        }

        public string GetUpdateableJobsFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekUpdateableJobsFilePath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekUpdateableJobsFilePath");
        }

        
        public string GetPredefinedMsgFile()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/PRE_DEFINED_MSG_FILE");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/PRE_DEFINED_MSG_FILE");
        }

        //public string GetMsgFile()
        //{
        //    if (ApplicationMode == "Debug")
        //        return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/MSG_FILE");
        //    else
        //        return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/MSG_FILE");
        
        
        
        //}
        //public string GetMsgToFile()
        //{
        //    if (ApplicationMode == "Debug")
        //        return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/MSG_TO_FILE");
        //    else
        //        return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/MSG_TO_FILE");



        //}
        public string GetContainerFile()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/CONTAINER");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/CONTAINER");



        }
        public string GetTOPUSHOK2LOADQueueName()
        {
            return GetConfigElmt("//Paths/TOPUSHOK2LOAD");
        }

        public string GetFROMITREKSELECTQueueName()
        {
            return GetConfigElmt("//Paths/FROMITREKSELECT");
        }

        public string GetTOITREKREAD_RESQueueName()
        {
            return GetConfigElmt("//Paths/TOITREKREAD_RES");
        }

        public string GetTOITREKAUTHQueueName()
        {
            return GetConfigElmt("//Paths/TOITREKAUTH");
        }

        public string GetFROMITREKUPDATEQueueName()
        {
            return GetConfigElmt("//Paths/FROMITREKUPDATE");
        }

        public string GetSERVICESTIMER()
        {
            return GetConfigElmt("//Paths/SERVICESTIMER");
        }


        public string GetETDSERVICESTIMER()
        {
            return GetConfigElmt("//Paths/ETDSERVICESTIMER");
        }

        public string GetDELSESSDIRTIMER()
        {
            return GetConfigElmt("//Paths/DELSESSDIRTIMER");
        }

        public string GetDELSESSDIRSERVICETIMER()
        {
            return GetConfigElmt("//Paths/DELSESSDIRSERVICETIMER");
        }

        
        public string GetJOB_AFT_SERVICESTIMER()
        {
            return GetConfigElmt("//Paths/JOB_AFT_SERVICESTIMER");
        }

        public string GetTOITREKREAD_AFTQueueName()
        {
            return GetConfigElmt("//Paths/TOITREKREAD_AFT");
        }

        public string GetTOITREKREAD_JOBQueueName()
        {
            return GetConfigElmt("//Paths/TOITREKREAD_JOB");
        }

        public string GetFROMITREKAUTHQueueName()
        {
            return GetConfigElmt("//Paths/FROMITREKAUTH");
        }
        public string GetFROMITREKMSGQueueName()
        {
            return GetConfigElmt("//Paths/FROMITREKMSG");
        }
        public string GetTOITREKMSGQueueName()
        {
            return GetConfigElmt("//Paths/TOITREKMSG");
        }
        public string GetFROMITREKCPMQueueName()
        {

            return GetConfigElmt("//Paths/FROMITREKCPM");
        }
        public string GetTOITREKCPMQueueName()
        {
            return GetConfigElmt("//Paths/TOITREKCPM");
        }

       

        //public string GetTOITREKAUTHQueueName()
        //{
        //    return GetConfigElmt("//Paths/TOITREKAUTH");
        //}


        public string GetPapURL()
        {
            return GetConfigElmt("//Paths/papURL/@href");
        }
        public string GetURLForNetAlert()
        {
            if (ApplicationMode == "Debug")
            return GetConfigElmt("//Paths/DEBUGURL/@href");
            else
            return GetConfigElmt("//Paths/URL/@href");
        }

        public string GetQueueManagerName()
        {
            return GetConfigElmt("//Paths/QUEUEMANAGERNAME");
        }

        public string GetChannelName()
        {
            return GetConfigElmt("//Paths/CHANNELNAME");
        }

        public string GetChannelInfo()
        {
            return GetConfigElmt("//Paths/CHANNELINFO");
        }

        public string GetChannelTransportType()
        {
            return GetConfigElmt("//Paths/CHANNELTRANSPORTTYPE");
        }

        public string GetChannelServer()
        {
            return GetConfigElmt("//Paths/CHANNELSERVER");
        }

        public string GetChannelPortNumber()
        {
            return GetConfigElmt("//Paths/CHANNELPORTNO");
        }

        public string GetTestSessionId()
        {
            return GetConfigElmt("//Paths/TestSessionId");
        }

        public string GetTestSID()
        {
            return GetConfigElmt("//Paths/TestSID");
        }

        public string GetTestPENO()
        {
            return GetConfigElmt("//Paths/TestPENO");
        }

        public string GetTestPENO2()
        {
            return GetConfigElmt("//Paths/TestPENO2");
        }

        public string GetTestFlightURNO()
        {
            return GetConfigElmt("//Paths/TestFlightURNO");
        }

        public string GetTestFlightURNO2()
        {
            return GetConfigElmt("//Paths/TestFlightURNO2");
        }

        public string GetTestJobURNO()
        {
            return GetConfigElmt("//Paths/TestJobURNO");
        }

        public string GetTestJobURNO2()
        {
            return GetConfigElmt("//Paths/TestJobURNO2");
        }

        public string GetTestUAFT()
        {
            return GetConfigElmt("//Paths/TestUAFT");
        }

        public string GetTestUAFT2()
        {
            return GetConfigElmt("//Paths/TestUAFT2");
        }

        private static string _fileMgrFlush = null;

        public string GetFileMgrFlushSetting()
        {
            if (_fileMgrFlush == null)
            {
                
                _fileMgrFlush = GetConfigElmt("//Paths/FILEMGRFLUSH");
                try
                {
                    int count = Convert.ToInt32(_fileMgrFlush);
                }
                catch (Exception)
                {
                    _fileMgrFlush = "1"; 
                    
                }
                
            }
            
        return _fileMgrFlush;
        }
        private static string _flightFileMgrFlush = null;

        public string GetFlightFileMgrFlushSetting()
        {
            if (_flightFileMgrFlush == null)
            {

                _flightFileMgrFlush = GetConfigElmt("//Paths/FLIGHTFILEMGRFLUSH");
                try
                {
                    int count = Convert.ToInt32(_flightFileMgrFlush);
                }
                catch (Exception)
                {
                    _flightFileMgrFlush = "1";

                }

            }

            return _flightFileMgrFlush;
        }
        private static string _jobFileMgrFlush = null;

        public string GetJobFileMgrFlushSetting()
        {
            if (_jobFileMgrFlush == null)
            {

                _jobFileMgrFlush = GetConfigElmt("//Paths/JOBFILEMGRFLUSH");
                try
                {
                    int count = Convert.ToInt32(_jobFileMgrFlush);
                }
                catch (Exception)
                {
                    _jobFileMgrFlush = "1";

                }

            }

            return _jobFileMgrFlush;
        }
        public string GetCountForJobMgrLoop()
        {
            return GetConfigElmt("//Paths/JOBMGRLOOPCOUNT");
        }

        public string GetMsgFromQueueDelaySetting()
        {
            return GetConfigElmt("//Paths/GETMSGFROMQUEUEDELAY");
        }
        public string GetLoginDelaySetting()
        {
            return GetConfigElmt("//Paths/LOGINDELAY");
        }
        public string GetDelayForPush()
        {

            return GetConfigElmt("//Paths/DELAYFORPUSH");
        }

        public string GetDelayForClosingFlight()
        {
            return GetConfigElmt("//Paths/CLOSEDFLIGHTDELAY");
        }

        public string GetLoginExpiryTime()
        {
            return GetConfigElmt("//Paths/LOGINEXPIRYTIME");
        }

        public string GetTestPassword()
        {
            return GetConfigElmt("//Paths/TestPassword");
        }

        public string GetTestSTOA()
        {
            return GetConfigElmt("//Paths/TestSTOA");
        }

        public string GetTestSTOA2()
        {
            return GetConfigElmt("//Paths/TestSTOA2");
        }

        public string GetTestSTOD()
        {
            return GetConfigElmt("//Paths/TestSTOD");
        }

        public string GetTestSTOD2()
        {
            return GetConfigElmt("//Paths/TestSTOD2");
        }

        public string GetETDALERTTIME()
        {
            return GetConfigElmt("//Paths/ETDALERTTIME");
        }

        public string GetTestFLNO()
        {
            return GetConfigElmt("//Paths/TestFLNO");
        }


        public string GetTestULDNo2()
        {
            return GetConfigElmt("//Paths/TestULDNo2");
        }


        public string GetAllOK2LoadTestFlURNO()
        {
            return GetConfigElmt("//Paths/AllOK2LoadTestFlURNO");
        }

        public string GetSHOWDLSTestFlURNO()
        {
            return GetConfigElmt("//Paths/SHOWDLSTestFlURNO");
        }

        public string GetSHOWDLSTestFLNO()
        {
            return GetConfigElmt("//Paths/SHOWDLSTestFLNO");
        }

        public string GetTestAllOK2LoadFLNO()
        {
            return GetConfigElmt("//Paths/TestAllOK2LoadFLNO");
        }

        public string GetOK2LSvcTestUAFT()
        {
            return GetConfigElmt("//Paths/OK2LSvcTestUAFT");
        }
        public string GetFuncMapXmlFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekFuncMapFilePath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekFuncMapFilePath");
        }

        public string GetPFCsFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekPFCsFilePath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekPFCsFilePath");
        }
        //public string GetBM_MAB()
        //{
        //    return GetConfigElmt("//Paths/BM_MAB");
        //}

        //public string GetBMApronFirstBag()
        //{
        //    return GetConfigElmt("//Paths/BM_AP_FST_BAG");
        //}

        //public string GetBMBaggageFirstBag()
        //{
        //    return GetConfigElmt("//Paths/BM_BG_FST_BAG");
        //}

        //public string GetBMApronLastBagLightLoad()
        //{
        //    return GetConfigElmt("//Paths/BM_AP_LST_BAG_LGT_LOAD");
        //}

        //public string GetBMBaggageLastBagLightLoad()
        //{
        //    return GetConfigElmt("//Paths/BM_BG_LST_BAG_LGT_LOAD");
        //}

        //public string GetBMApronLastBagHeavyLoad()
        //{
        //    return GetConfigElmt("//Paths/BM_AP_LST_BAG_HVY_LOAD");
        //}

        //public string GetBMBaggageLastBagHeavyLoad()
        //{
        //    return GetConfigElmt("//Paths/BM_BG_LST_BAG_HVY_LOAD");
        //}

        //public string GetWebFLINFO_TO_MINUTES()
        //{
        //    return GetConfigElmt("//Paths/WEB_FLINFO_TO_MINUTES");
        //}

        //public string GetWebFLINFO_FROM_MINUTES()
        //{
        //    return GetConfigElmt("//Paths/WEB_FLINFO_FROM_MINUTES");
        //}

        //public int GetWapJobAssignAlertMinutes()
        //{
        //    return Convert.ToInt32(GetConfigElmt("//Paths/WAP_JOBALERT_MINUTES"));
        //}

        public string GetPredefinedMsgFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/PRE_DEFINED_MSG_FILE");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/PRE_DEFINED_MSG_FILE");
        }

        public string GetPredefinedBagConRmkFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/PRE_DEFINED_BAG_CON_RMK_FILE");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/PRE_DEFINED_BAG_CON_RMK_FILE");
        }

        //public string GetPFCsFilePath()
        //{
        //    if (ApplicationMode == "Debug")
        //        return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekPFCsFilePath");
        //    else
        //        return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekPFCsFilePath");
        //}

        public string GetCpmFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekCPMFilePath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekCPMFilePath");
        }
        public string GetCpmDetFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekCPMDetFilePath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekCPMDetFilePath");
        }
        public string GetCpmDetHistFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekCPMDetHistFilePath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekCPMDetHistFilePath");
        }
        public string GetMsgLogFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekMsgLogFilePath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekMsgLogFilePath");
        }

        public string GetDLSTabFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekDLSTabFilePath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekDLSTabFilePath");
        }

        public string GetTripFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekTripFilePath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekTripFilePath");
        }
        public string GetTripHistFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekTripHistFilePath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekTripHistFilePath");
        }
        //public string GetFuncMapXmlFilePath()
        //{
        //    if (ApplicationMode == "Debug")
        //        return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekFuncMapFilePath");
        //    else
        //        return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekFuncMapFilePath");
        //}

        public string GetMsgToFile()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekMsgToFilePath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekMsgToFilePath");
        }

        public string GetMsgFile()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekMsgFilePath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekMsgFilePath");
        }


        public string GetDepFlightsTimingFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekDepFlightTimingFilePath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekDepFlightTimingFilePath");
        }

        public string GetArrFlightsTimingFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekArrFlightTimingFilePath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekArrFlightTimingFilePath");
        }


        public string GetSummFlightsFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekSummFlightFilePath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekSummFlightFilePath");
        }

        public string GetTerminalFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekTerminalFilePath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekTerminalFilePath");
        }

        public string GetFROMITREKRPTQueueName()
        {
            return GetConfigElmt("//Paths/FROMITREKRPT");
        }

        public string GetTOITREKRPTQueueName()
        {
            return GetConfigElmt("//Paths/TOITREKRPT");
        }

        public string GetBagPresentTimingReportHdrFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekBagPresentTimeRptHdrFilePath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekBagPresentTimeRptHdrFilePath");
        }

        public string GetBagPresentTimingReportDetFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekBagPresentTimeRptDetFilePath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekBagPresentTimeRptDetFilePath");
        }
        public string GetJobsSummFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekJobsSummFilePath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekJobsSummFilePath");
        }

        public string GetJobsSummHistFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekJobsSummHistFilePath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekJobsSummHistFilePath");
        }
        public string GetApronServiceReportFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/iTrekApronServiceReportFilePath");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/iTrekApronServiceReportFilePath");
        }

        public string GetCONFIGFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/ConfigFile");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/ConfigFile");
        }

        
        public string GetMessageDuration()
        {
            return GetConfigElmt("//Paths/MESSAGEDURATION");           
        }

        public string GetDownTime()
        {
            return GetConfigElmt("//Paths/DOWNTIME");
        }
        private static string _mqStatus = null;
        public string GetMQStatus()
        {
            if (_mqStatus == null)
            {

                _mqStatus = GetConfigElmt("//Paths/MQSTATUS");
            }

            return _mqStatus;
        }

        public string GetLogDirPath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath + GetConfigElmt("//Paths/DebugPaths/LogDir");
            else
                return applicationBasePath + GetConfigElmt("//Paths/TestReleasePaths/LogDir");
        }
        public string GetCpmPriorityFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath +
                GetConfigElmt("//Paths/DebugPaths/iTrekCpmPriorityFilePath");
            else
                return applicationBasePath +
                GetConfigElmt("//Paths/TestReleasePaths/iTrekCpmPriorityFilePath");
        }
        public string GetTaskFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath +
                GetConfigElmt("//Paths/DebugPaths/iTrekTaskFilePath");
            else
                return applicationBasePath +
                GetConfigElmt("//Paths/TestReleasePaths/iTrekTaskFilePath");
        }
        public string GetMsgLogDetFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath +
                GetConfigElmt("//Paths/DebugPaths/iTrekMsgLogDetFilePath");
            else
                return applicationBasePath +
                GetConfigElmt("//Paths/TestReleasePaths/iTrekMsgLogDetFilePath");
        }
        public string GetUfisCONFIGFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath +
                GetConfigElmt("//Paths/DebugPaths/iTrekUfisConfigFile");
            else
                return applicationBasePath +
                GetConfigElmt("//Paths/TestReleasePaths/iTrekUfisConfigFile");
        }


        public string GetBeltFilePath()
        {
            if (ApplicationMode == "Debug")
                return applicationBasePath +
                GetConfigElmt("//Paths/DebugPaths/iTrekBeltFilePath");
            else
                return applicationBasePath +
                GetConfigElmt("//Paths/TestReleasePaths/iTrekBeltFilePath");
        } 

    }
}
