//#define TestsOn
#define LoggingOn
using System;
using System.Collections.Generic;
using System.Text;
using IBM.WMQ;
using iTrekXML;
using System.Collections;
using iTrekUtils;
using iTrekWMQ;
#if TestsOn
using NUnit.Framework;
#endif
using System.Security.Policy;   //for  evidence  object
using iTrekSessionMgr;
using System.IO;

#if TestsOn
namespace iTrekWMQTests
{
    
   [TestFixture]
    public class iTrekWMQTests1
    {
        //currently Ok2LWMQ tests only
        iTXMLcl iTXML;
        ITrekWMQClass1 mqObj;
        iTXMLPathscl iTPaths;
        SessionManager sessMan;

        //[SetUp]
        public iTrekWMQTests1()
        {
            iTPaths = new iTXMLPathscl();
            sessMan = new SessionManager();

            //As we want to add multiple assemblies to the NUnit project
            //at PushServ directory level 
            //we need to point NUNit at the relevant ApplicationBase
            //when instantiating classes
            //This one is for iTrekOk2L (points to config xml file)

            //Create  the  application  domain  setup  information.
            AppDomainSetup domaininfo = new AppDomainSetup();
            domaininfo.ApplicationBase = System.Environment.CurrentDirectory;
            //Create  evidence  for  new  appdomain.
            Evidence adevidence = AppDomain.CurrentDomain.Evidence;
            //  Create  the  new  application  domain  using  setup  information.
            AppDomain domain = AppDomain.CreateDomain("iTrekOk2L", adevidence, domaininfo);

            iTXML = new iTXMLcl(domain.SetupInformation.ApplicationBase);
            //mqObj = new iTrekOk2L.WMQClass(iTPaths.GetTOPUSHOK2LOADQueueName());
            //mqObj = new iTrekWMQClass(iTPaths.GetTOPUSHOK2LOADQueueName());
            mqObj = new ITrekWMQClass1();
           // iTPaths.GetTOPUSHOK2LOADQueueName());
        }

        //TODO: Coverage high
        //[Test]
        //public void TOPUSHOK2LOADQueue()
        //{
        //    mqObj.putTheMessageIntoQueue("Test message");
        //    Assert.AreEqual("Test message", mqObj.GetMessageFromTheQueue());
        //}

        //TODO: Coverage high
        [Test]
        public void PurgeMessageQueue()
        {
            mqObj.putTheMessageIntoQueue("Test message", iTPaths.GetTOPUSHOK2LOADQueueName());
            mqObj.PurgeQueue(iTPaths.GetTOPUSHOK2LOADQueueName());
            Assert.AreEqual("MQRC_NO_MSG_AVAILABLE", mqObj.GetMessageFromTheQueue(iTPaths.GetTOPUSHOK2LOADQueueName()));
        }

        //TODO: Coverage high
        [Test]
        public void Put3MsgOnQueueGet3MsgBackInSameOrder()
        {
            mqObj.PurgeQueue(iTPaths.GetTOPUSHOK2LOADQueueName());
            mqObj.putTheMessageIntoQueue("First Test message", iTPaths.GetTOPUSHOK2LOADQueueName());
            mqObj.putTheMessageIntoQueue("Second Test message");
            mqObj.putTheMessageIntoQueue("Third Test message", iTPaths.GetTOPUSHOK2LOADQueueName());
            Assert.AreEqual("First Test message", mqObj.GetMessageFromTheQueue(iTPaths.GetTOPUSHOK2LOADQueueName()));
            Assert.AreEqual("Second Test message", mqObj.GetMessageFromTheQueue(iTPaths.GetTOPUSHOK2LOADQueueName()));
            Assert.AreEqual("Third Test message", mqObj.GetMessageFromTheQueue(iTPaths.GetTOPUSHOK2LOADQueueName()));
        }

        //TODO: Coverage medium
        //Only testing that message going onto queue
        //not that appropriate result being returned in 
        //TO.ITREK.READ_RES
        [Test]
        public void IntoFROMITREKAUTHQueueWithSessionIDTest()
        {
            ITrekWMQClass1 FROMITREKAUTHQueue = new ITrekWMQClass1();
            FROMITREKAUTHQueue.putMessageIntoFROMITREKAUTHQueueWithSessionID(iTPaths.GetTestPENO(), iTPaths.GetTestPassword(), iTPaths.GetTestSessionId(), iTPaths.GetFROMITREKAUTHQueueName());
            Assert.AreEqual("<AUTH><PENO>" + iTPaths.GetTestPENO() + "</PENO><PASS>" + iTPaths.GetTestPassword() + "</PASS><SESSIONID>" + iTPaths.GetTestSessionId() + "</SESSIONID></AUTH>", FROMITREKAUTHQueue.GetMessageFromTheQueue(iTPaths.GetFROMITREKAUTHQueueName()));
        }

        //TODO: Coverage medium
        //Only testing that message going onto queue
        [Test]
        public void FromTOITREKREAD_RESQueueTest()
        {
            ITrekWMQClass1 TOITREKREAD_RESQueue = new ITrekWMQClass1();
            TOITREKREAD_RESQueue.PurgeQueue(iTPaths.GetTOITREKREAD_RESQueueName());
            TOITREKREAD_RESQueue.putTheMessageIntoQueue("Test message from GetMessageFromTOITREKREAD_RESQueue", iTPaths.GetTOITREKREAD_RESQueueName());
            Assert.AreEqual("Test message from GetMessageFromTOITREKREAD_RESQueue", TOITREKREAD_RESQueue.GetMessageFromTheQueue(iTPaths.GetTOITREKREAD_RESQueueName()));
        }

        //TODO: Coverage medium
        //Only testing that message going onto queue
        [Test]
        public void FROMITREKUPDATEQueueTest()
        {
            ITrekWMQClass1 FROMITREKUPDATEQueue = new ITrekWMQClass1();
            FROMITREKUPDATEQueue.PurgeQueue(iTPaths.GetFROMITREKUPDATEQueueName());
            FROMITREKUPDATEQueue.putTheMessageIntoQueue("Test message from FROMITREKUPDATEQueue", iTPaths.GetFROMITREKUPDATEQueueName());
            Assert.AreEqual("Test message from FROMITREKUPDATEQueue", FROMITREKUPDATEQueue.GetMessageFromTheQueue(iTPaths.GetFROMITREKUPDATEQueueName()));
        }

        //TODO: Coverage medium
        //Only testing that message going onto queue
        [Test]
        public void TOITREKREAD_AFTQueueTest()
        {
            ITrekWMQClass1 TOITREKREAD_AFTQueue = new ITrekWMQClass1();
            TOITREKREAD_AFTQueue.PurgeQueue(iTPaths.GetTOITREKREAD_AFTQueueName());
            TOITREKREAD_AFTQueue.putTheMessageIntoQueue("Test message from TOITREKREAD_AFTQueue", iTPaths.GetTOITREKREAD_AFTQueueName());

            Assert.AreEqual("Test message from TOITREKREAD_AFTQueue", TOITREKREAD_AFTQueue.GetMessageFromTheQueue(iTPaths.GetTOITREKREAD_AFTQueueName()));
        }

        //TODO: Coverage medium
        //Only testing that message going onto queue
        [Test]
        public void TOITREKREAD_JOBQueueTest()
        {
            ITrekWMQClass1 TOITREKREAD_JOBQueue = new ITrekWMQClass1();
            TOITREKREAD_JOBQueue.PurgeQueue(iTPaths.GetTOITREKREAD_JOBQueueName());
            TOITREKREAD_JOBQueue.putTheMessageIntoQueue("Test message from TOITREKREAD_JOBQueueTest", iTPaths.GetTOITREKREAD_JOBQueueName());
            Assert.AreEqual("Test message from TOITREKREAD_JOBQueueTest", TOITREKREAD_JOBQueue.GetMessageFromTheQueue(iTPaths.GetTOITREKREAD_JOBQueueName()));
        }

        //TODO: Coverage medium
        //Only testing that message going onto queue
        [Test]
        public void FROMITREKAUTHQueueTest()
        {
            ITrekWMQClass1 FROMITREKAUTHQueue = new ITrekWMQClass1();
            FROMITREKAUTHQueue.PurgeQueue(iTPaths.GetFROMITREKAUTHQueueName());
            FROMITREKAUTHQueue.putTheMessageIntoQueue("Test message from FROMITREKAUTHQueue", iTPaths.GetFROMITREKAUTHQueueName());
            Assert.AreEqual("Test message from FROMITREKAUTHQueue", FROMITREKAUTHQueue.GetMessageFromTheQueue(iTPaths.GetFROMITREKAUTHQueueName()));
        }


        //TODO: Coverage low
        //Only testing that message going onto queue
        [Test]
        public void TOITREKAUTHQueueTest()
        {
            ITrekWMQClass1 TOITREKAUTHQueue = new ITrekWMQClass1();
            TOITREKAUTHQueue.PurgeQueue(iTPaths.GetTOITREKAUTHQueueName());
            TOITREKAUTHQueue.putTheMessageIntoQueue("Test message from TOITREKAUTHQueue", iTPaths.GetTOITREKAUTHQueueName());
            Assert.AreEqual("Test message from TOITREKAUTHQueue", TOITREKAUTHQueue.GetMessageFromTheQueue(iTPaths.GetTOITREKAUTHQueueName()));

        }

        //TODO: Coverage medium       
        //Need to see result on handset - Alert Msg
        //Needs OK2LSvc to be running
        [Test]
        public void TOPUSHOK2LOADQueue()
        {
            ITrekWMQClass1 TOPUSHOK2LOADQueue = new ITrekWMQClass1();
            TOPUSHOK2LOADQueue.PurgeQueue(iTPaths.GetTOPUSHOK2LOADQueueName());
            TOPUSHOK2LOADQueue.putTheMessageIntoQueue("<OK2L><ULDNUMBER>AKE123456CX</ULDNUMBER><UAFT>" + iTPaths.GetOK2LSvcTestUAFT() + "</UAFT></OK2L>", iTPaths.GetTOPUSHOK2LOADQueueName());
            //Assert.AreEqual("Test message from TOITREKAUTHQueue", TOITREKAUTHQueue.GetMessageFromTheQueue());
            Assert.AreEqual("", "", "Setup valid device in DeviceID.xml?");

        }
        //TODO: Coverage medium
        //Only testing that message going onto queue
        [Test]
        public void FROMITREKSELECTQueueTest()
        {
            ITrekWMQClass1 FROMITREKSELECTQueue = new ITrekWMQClass1();
            FROMITREKSELECTQueue.PurgeQueue(iTPaths.GetFROMITREKSELECTQueueName());
            FROMITREKSELECTQueue.putTheMessageIntoQueue("Test message from FROMITREKSELECTQueue", iTPaths.GetFROMITREKSELECTQueueName());
            Assert.AreEqual("Test message from FROMITREKSELECTQueue", FROMITREKSELECTQueue.GetMessageFromTheQueue(iTPaths.GetFROMITREKSELECTQueueName()));
        }

    }
}

#endif


namespace iTrekWMQ
{
    public  class ITrekWMQClass1
    {

        private  MQQueueManager queueManager;
        private  MQQueue queue;
        private  MQMessage queueMessage;
        private  MQPutMessageOptions queuePutMessageOptions;
        private  MQGetMessageOptions queueGetMessageOptions;

        //static string QueueName;
         string QueueManagerName;
        //static string ChannelInfo;

        private  string channelName;
        private  string transportType;
        private  string connectionName;

        private  string message = "";
        private  byte[] MessageID;

        private static iTXMLPathscl iTPaths;
        /*
         * 
         * Create a queue connection for the WMQ
         * 
         * 
         * 
         * */
        private  void createQueueConnection()
        {
#if LoggingOn
           // iTrekUtils.iTUtils.LogToEventLog("Inside createQueueConnection: " );
#endif
            
            iTPaths = new iTXMLPathscl();
            //TODO: Don't use queue name at this level
            //can be instatiated twice and change relevant queue
            //QueueName = queueNm;
            QueueManagerName = iTPaths.GetQueueManagerName();

            channelName = iTPaths.GetChannelName();
            transportType = iTPaths.GetChannelTransportType();

            connectionName = iTPaths.GetChannelServer();
            connectionName += iTPaths.GetChannelPortNumber();
            string serverAndPort = iTPaths.GetChannelServer();
            serverAndPort += iTPaths.GetChannelPortNumber();
            queueManager = new MQQueueManager(QueueManagerName, iTPaths.GetChannelName(), serverAndPort);

        }

        public  MQQueue getQueueConnection(string qName, int qType)
        {
            #if LoggingOn
            //iTrekUtils.iTUtils.LogToEventLog("Inside getQueueConnection: " + qName);
#endif
            iTPaths = new iTXMLPathscl();
            //TODO: Don't use queue name at this level
            //can be instatiated twice and change relevant queue
            //QueueName = queueNm;
            QueueManagerName = iTPaths.GetQueueManagerName();

            channelName = iTPaths.GetChannelName();
            transportType = iTPaths.GetChannelTransportType();

            connectionName = iTPaths.GetChannelServer();
            connectionName += iTPaths.GetChannelPortNumber();
            string serverAndPort = iTPaths.GetChannelServer();
            serverAndPort += iTPaths.GetChannelPortNumber();
            queueManager = new MQQueueManager(QueueManagerName, iTPaths.GetChannelName(), serverAndPort);
            //queueManager.AlternateUserId = "mqitrek";
           
            queue = queueManager.AccessQueue(qName,qType);
#if LoggingOn
          //  iTrekUtils.iTUtils.LogToEventLog("getQueueConnection: " + queue.Name);
#endif
            
            return queue;

        }
        public MQQueueManager getQManager
        {
            get
            {
                return queueManager;
            
            }
        
        
        }
        private  void closeQueueConnection()
        {
            #if LoggingOn
           // iTrekUtils.iTUtils.LogToEventLog("Inside closeQueueConnection: " + queue.Name);
#endif
            
                if (queue.IsOpen)
                {
#if LoggingOn
                  //  iTrekUtils.iTUtils.LogToEventLog("Inside Going To Close queue: " + queue.Name);
#endif
                    queue.Close();
                    queue.CloseOptions = MQC.MQCO_DELETE;
                                    
                
                  }
            if (queueManager.IsConnected)
            {
                #if LoggingOn
                //iTrekUtils.iTUtils.LogToEventLog("Inside Going To Close queueManager: " +queueManager.Name);
#endif
                queueManager.Close();
                queueManager.Disconnect();
                
            }
           // iTrekUtils.iTUtils.LogToEventLog("End of closequeueconnection: " + queue.);
            #if LoggingOn
            //iTrekUtils.iTUtils.LogToEventLog("End of closequeueconnection: " + queueManager.IsConnected);
#endif
        }

        /* Closing queue connection for services*/
        public void closeQueueConnection(MQQueue locque,MQQueueManager locqueueManager)
        {
            #if LoggingOn
           // iTrekUtils.iTUtils.LogToEventLog("Inside servicecloseQueueConnection: " + locque.Name);
#endif
            if (locque.IsOpen)
            {
                #if LoggingOn
               // iTrekUtils.iTUtils.LogToEventLog("Inside Going To service Close queue: " + locque.Name);
#endif
                locque.Close();
                locque.CloseOptions = MQC.MQCO_DELETE;
               

            }
            if (locqueueManager.IsConnected)
            {
                #if LoggingOn
             //   iTrekUtils.iTUtils.LogToEventLog("Inside Going To service Close queueManager: " + locqueueManager.Name);
#endif
                locqueueManager.Close();
                locqueueManager.Disconnect();

            }
           
        }
        public  void PurgeQueue(string queueName)
        {
            while (GetMessageFromTheQueue(queueName) != "MQRC_NO_MSG_AVAILABLE")
            {
                GetMessageFromTheQueue(queueName);
            }
        }

        public  string putTheMessageIntoQueue(string queueName)
        {
            string status = "";
            try
            {
                
                createQueueConnection();
                queue = queueManager.AccessQueue(queueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);

                return "Enter the message to put in MQSeries server:";
                //message = System.Console.ReadLine();
                int msgLen = message.Length;

                while (msgLen == 0)
                {
                    if (msgLen == 0)
                        return "Please reenter the message:";
                    message = System.Console.ReadLine();
                    msgLen = message.Length;
                }

                queueMessage = new MQMessage();
                queueMessage.WriteBytes(message);
                queueMessage.Format = MQC.MQFMT_STRING;
                queuePutMessageOptions = new MQPutMessageOptions();

                //putting the message into the queue
                queue.Put(queueMessage, queuePutMessageOptions);
               // MessageID = queueMessage.MessageId;

                status = "Success fully entered the message into the queue";

            }
            catch (Exception mqexp)
            {
                status = "MQSeries Exception: " + mqexp.Message;
                //iTrekUtils.iTUtils.LogToEventLog("Exception in putTheMessageIntoQueue: " + mqexp.Message);
                

            }
            finally
            {

                closeQueueConnection();

            }
#if LoggingOn
           //iTrekUtils.iTUtils.LogToEventLog("End of putTheMessageIntoQueue: " + status);
#endif
            return status;
        }


        public  string GetMessageFromTheQueue(string queueName)
        {
            string returnMessage = "";
            try
            {
                
                createQueueConnection();
                queue = queueManager.AccessQueue(queueName, MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
                queueMessage = new MQMessage();
                queueMessage.Format = MQC.MQFMT_STRING;
                if (MessageID != null)
                    queueMessage.MessageId = MessageID;

                queueGetMessageOptions = new MQGetMessageOptions();


                queue.Get(queueMessage, queueGetMessageOptions);
                // Received Message.
                //return "MessageID:" + MessageID + " Message:" + queueMessage.ReadString(queueMessage.MessageLength);

                returnMessage = queueMessage.ReadString(queueMessage.MessageLength);

            }
            catch (Exception MQExp)
            {
                //HACK: Need to stop using exception to control queue purge
                returnMessage = MQExp.Message;
            }
            finally
            {
                closeQueueConnection();

            }
#if LoggingOn
           // iTrekUtils.iTUtils.LogToEventLog("GetMessageFromTheQueue: " + returnMessage);
#endif
            return returnMessage;
        }




        public  string GetMessageFromTheQueue(MQQueue queue1)
        {
            string returnMessage = "";
            try
            {
#if LoggingOn
               // iTrekUtils.iTUtils.LogToEventLog("Inside GetMessageFromTheQueue: " + queue1.Name);
#endif
                //createQueueConnection();
                //queue = queueManager.AccessQueue(queueName, MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
                queueMessage = new MQMessage();
                queueMessage.Format = MQC.MQFMT_STRING;
                if (MessageID != null)
                    queueMessage.MessageId = MessageID;

                queueGetMessageOptions = new MQGetMessageOptions();


                queue1.Get(queueMessage, queueGetMessageOptions);
                // Received Message.
                //return "MessageID:" + MessageID + " Message:" + queueMessage.ReadString(queueMessage.MessageLength);

                returnMessage = queueMessage.ReadString(queueMessage.MessageLength);
                //iTrekUtils.iTUtils.LogToEventLog("Inside GetMessageFromTheQueue: " + returnMessage);

            }
            catch (Exception MQExp)
            {
                //HACK: Need to stop using exception to control queue purge
                returnMessage = MQExp.Message;
            }
            finally
            {
                //closeQueueConnection(queue1);

            }
            #if LoggingOn
           // iTrekUtils.iTUtils.LogToEventLog("End of GetMessageFromTheQueue: " + returnMessage);
#endif
            return returnMessage;
        }


        public string GetMessageFromTheQueueForInvalidChars(MQQueue queue1)
        {
            string returnMessage = "";
            try
            {
#if LoggingOn
                // iTrekUtils.iTUtils.LogToEventLog("Inside GetMessageFromTheQueue: " + queue1.Name);
#endif
                //createQueueConnection();
                //queue = queueManager.AccessQueue(queueName, MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
                queueMessage = new MQMessage();
                queueMessage.Format = MQC.MQFMT_NONE;
                if (MessageID != null)
                    queueMessage.MessageId = MessageID;

                queueGetMessageOptions = new MQGetMessageOptions();


                queue1.Get(queueMessage, queueGetMessageOptions);
                // Received Message.
                //return "MessageID:" + MessageID + " Message:" + queueMessage.ReadString(queueMessage.MessageLength);

                returnMessage = queueMessage.ReadString(queueMessage.MessageLength);
                //iTrekUtils.iTUtils.LogToEventLog("Inside GetMessageFromTheQueue: " + returnMessage);

            }
            catch (Exception MQExp)
            {
                //HACK: Need to stop using exception to control queue purge
                returnMessage = MQExp.Message;
            }
            finally
            {
                //closeQueueConnection(queue1);

            }
#if LoggingOn
            // iTrekUtils.iTUtils.LogToEventLog("End of GetMessageFromTheQueue: " + returnMessage);
#endif
            return returnMessage;
        }


        public string GetMessageFromTheQueueForOpenConnection(MQQueue queue1)
        {
            string returnMessage = "";
            

                
                queueMessage = new MQMessage();
                queueMessage.Format = MQC.MQFMT_STRING;
                if (MessageID != null)
                    queueMessage.MessageId = MessageID;

                queueGetMessageOptions = new MQGetMessageOptions();


                queue1.Get(queueMessage, queueGetMessageOptions);
                
                returnMessage = queueMessage.ReadString(queueMessage.MessageLength);      


            return returnMessage;
        }

        public string BrowseFirstMessageFromTheQueueForOpenConnection(MQQueue queue1, out byte[] messageId)
        {
            messageId = new byte[100];
            string returnMessage = "";
            queueMessage = new MQMessage(); // object to receive the message
            queueMessage.Format = MQC.MQFMT_STRING;
            if (MessageID != null)
                queueMessage.MessageId = MessageID;
            queueGetMessageOptions = new MQGetMessageOptions();
            queueGetMessageOptions.Options = MQC.MQGMO_FAIL_IF_QUIESCING + MQC.MQGMO_NO_WAIT + MQC.MQGMO_BROWSE_FIRST; // browse with no wait
            queueGetMessageOptions.MatchOptions = MQC.MQMO_NONE; // no matching required      

          

            // browse all messages on the queue

            // browse message - exception will be thrown when no more messages
            queue1.Get(queueMessage, queueGetMessageOptions);

            if (queueMessage.Format.CompareTo(MQC.MQFMT_STRING) == 0)
            {
                returnMessage = queueMessage.ReadString(queueMessage.MessageLength);
                messageId = queueMessage.MessageId;
            }
           


            return returnMessage;
        }



        public string DeleteMessages(MQQueue queue1, byte[] messageId)
        {
            // browse (i.e. get without removing) a message on the message queue
            string message = "";
            byte[] newmessageId = new byte[100];

           
                // get message options
             queueGetMessageOptions = new MQGetMessageOptions();
             queueGetMessageOptions.Options = MQC.MQGMO_FAIL_IF_QUIESCING + MQC.MQGMO_NO_WAIT + MQC.MQGMO_BROWSE_FIRST; // browse with no wait
             queueGetMessageOptions.MatchOptions = MQC.MQMO_NONE; // no matching required

             queueMessage = new MQMessage(); // object to receive the message

                // browse all messages on the queue

                // browse message - exception will be thrown when no more messages
             queue1.Get(queueMessage, queueGetMessageOptions);

                // get message text from the message object
                //message = mqMsg.ReadString(mqMsg.MessageLength);
             newmessageId = queueMessage.MessageId;
                if (Convert.ToBase64String(messageId) == Convert.ToBase64String(newmessageId))
                // if (messageId .Equals(newmessageId))
                {
                    queueGetMessageOptions.Options = MQC.MQGMO_MSG_UNDER_CURSOR;
                    queue1.Get(queueMessage, queueGetMessageOptions);

                }
            

            return message;
        }
        public  string GetMessageFromTheQueueAnyOrder(string queueName)
        {
            string returnMessage = "";
            try
            {
                createQueueConnection();
                //In office
                //queueManager = new MQQueueManager(QueueManagerName, "CHANNEL1", "192.168.1.8");
                //At home
                //queueManager = new MQQueueManager(QueueManagerName, "CHANNEL1", "http://www.ufissin.homelinux.net/");

                queue = queueManager.AccessQueue(queueName, MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
                queueMessage = new MQMessage();
                queueMessage.Format = MQC.MQFMT_STRING;
                //queueMessage.MessageId = MessageID;
                queueGetMessageOptions = new MQGetMessageOptions();

                queue.Get(queueMessage, queueGetMessageOptions);

                // Received Message.
                //System.Console.WriteLine("Received Message: {0}", queueMessage.ReadString(queueMessage.MessageLength));

                returnMessage = queueMessage.ReadString(queueMessage.MessageLength);

            }
            catch (Exception MQExp)
            {

                returnMessage = "MQQueue::Get ended with " + MQExp.Message;
            }
            finally
            {
                closeQueueConnection();

            }
            return returnMessage;
        }


        public  void GetConsoleMessageFromTheQueueAnyOrder(string queueName)
        {


            try
            {


                //QueueName = iTPaths.GetTOPUSHOK2LOADQueueName();
                //QueueManagerName = "venus.queue.manager";
                //ChannelInfo = "CHANNEL1/TCP/192.168.1.8";


                //In office
                //queueManager = new MQQueueManager(QueueManagerName, "CHANNEL1", "192.168.1.8");
                //At home
                //queueManager = new MQQueueManager(QueueManagerName, "CHANNEL1", "http://www.ufissin.homelinux.net/");
                createQueueConnection();
                queue = queueManager.AccessQueue(queueName, MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
                queueMessage = new MQMessage();
                queueMessage.Format = MQC.MQFMT_STRING;
                //queueMessage.MessageId = MessageID;
                queueGetMessageOptions = new MQGetMessageOptions();


                queue.Get(queueMessage, queueGetMessageOptions);

                // Received Message.
                System.Console.WriteLine("Received Message: {0}", queueMessage.ReadString(queueMessage.MessageLength));
                //return queueMessage.ReadString(queueMessage.MessageLength);


            }
            catch (Exception MQExp)
            {
                // report the error
                //System.Console.WriteLine("MQQueue::Get ended with " + MQExp.Message);
                System.Console.WriteLine("MQQueue::Get ended with " + MQExp.Message);

            }

            finally
            {
                closeQueueConnection();

            }

        }

        public  string GetMessageFromTheQueueReturnString(string queueName)
        {
            string returnMessage = "";

            try
            {
                createQueueConnection();
                queue = queueManager.AccessQueue(queueName, MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
                queueMessage = new MQMessage();
                queueMessage.Format = MQC.MQFMT_STRING;
                //queueMessage.MessageId = MessageID;
                queueGetMessageOptions = new MQGetMessageOptions();


                queue.Get(queueMessage, queueGetMessageOptions);

                // Received Message.
                returnMessage = queueMessage.ReadString(queueMessage.MessageLength);
                //System.Console.WriteLine("Received Message: {0}", queueMessage.MessageId[0].ToString());

            }
            catch (MQException MQExp)
            {
                // report the error

                returnMessage = MQExp.Message;
            }
            finally
            {
                closeQueueConnection();

            }
            return returnMessage;
        }
        public  void putXMLMessageIntoQueue(string queueName)
        {
            try
            {
                createQueueConnection();
                queue = queueManager.AccessQueue(queueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);


                message = iTUtils.ReadFromFile(@"C:\temp\tests\bigfile.xml");

                int msgLen = message.Length;

                while (msgLen == 0)
                {
                    if (msgLen == 0)
                        //Console.WriteLine("Please reenter the message:");
                    message = System.Console.ReadLine();
                    msgLen = message.Length;
                }

                queueMessage = new MQMessage();
                queueMessage.WriteBytes(message);
                queueMessage.Format = MQC.MQFMT_STRING;
                queuePutMessageOptions = new MQPutMessageOptions();

                //putting the message into the queue
                queue.Put(queueMessage, queuePutMessageOptions);
                MessageID = queueMessage.MessageId;

               // Console.WriteLine("Success fully entered the message into the queue");
            }
            catch (MQException mqexp)
            {

              //  Console.WriteLine("MQSeries Exception: " + mqexp.Message);
            }
            finally
            {
                closeQueueConnection();

            }
        }

        public  string putTheMessageIntoQueue(string message, string queueName)
        {
            string returnMessage = "";
            try
            {
                createQueueConnection();
                queue = queueManager.AccessQueue(queueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);

                message = removeBadCharacters(message);
                int msgLen = message.Length;

                while (msgLen == 0)
                {
                    if (msgLen == 0)
                        return "Add a message parameter";
                    msgLen = message.Length;
                }

                queueMessage = new MQMessage();
                queueMessage.WriteBytes(message);
                queueMessage.Format = MQC.MQFMT_STRING;
                queuePutMessageOptions = new MQPutMessageOptions();

                //putting the message into the queue
                queue.Put(queueMessage, queuePutMessageOptions);
                returnMessage = "success";
                //add the new message id to the next messageid byte array element
                //to retain earlier messages message ids
               // MessageID[MessageID.Length + 1] = queueMessage.MessageId[MessageID.Length];

                //returnMessage = MessageID[MessageID.Length + 1].ToString();

                //return queueMessage.MessageId.ToString();
            }
            catch (Exception mqexp)
            {

                returnMessage = "MQSeries Exception: " + mqexp.Message;
               // iTrekUtils.iTUtils.LogToEventLog("putTheMessageIntoQueue: " + message + ":" + returnMessage);
               // TextWriter tw = new StreamWriter("C://MQTest.txt");

                // write a line of text to the file
               // tw.WriteLine(queueName + "|" + message);

                // close the stream
                //tw.Close();
            }
            finally
            {
                closeQueueConnection();

            }
#if LoggingOn
            //iTrekUtils.iTUtils.LogToEventLog("putTheMessageIntoQueue: " +message+":"+ returnMessage);
#endif
            return returnMessage;
        }
        public  string putConsoleMessageIntoQueue(string message, string queueName)
        {
            string returnMessage = "";
            try
            {
                createQueueConnection();
                queue = queueManager.AccessQueue(queueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);

                int msgLen = message.Length;

                while (msgLen == 0)
                {
                    if (msgLen == 0)
                        return "Add a message parameter";
                    msgLen = message.Length;
                }

                queueMessage = new MQMessage();
                queueMessage.WriteBytes(message);
                queueMessage.Format = MQC.MQFMT_STRING;
                queuePutMessageOptions = new MQPutMessageOptions();

                //putting the message into the queue
                queue.Put(queueMessage, queuePutMessageOptions);
                //add the new message id to the next messageid byte array element
                //to retain earlier messages message ids
                //MessageID[MessageID.Length +1] = queueMessage.MessageId[MessageID.Length];

                //return MessageID[MessageID.Length + 1].ToString();
                MessageID = queueMessage.MessageId;
                //return MessageID[MessageID.Length - 1].ToString(); ;
                //return queueMessage.MessageId.ToString();

                returnMessage = message;
            }
            catch (Exception mqexp)
            {

                returnMessage = "MQSeries Exception: " + mqexp.Message;
            }
            finally
            {
                closeQueueConnection();

            }
            return returnMessage;
        }
        public  void putMessageIntoQueueForMultipleMessages(string msg, string queueName)
        {
            
            try
            {
                createQueueConnection();
                queue = queueManager.AccessQueue(queueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);

                message = msg;

                queueMessage = new MQMessage();
                queueMessage.WriteBytes(message);
                queueMessage.Format = MQC.MQFMT_STRING;
                queuePutMessageOptions = new MQPutMessageOptions();

                //putting the message into the queue
                queue.Put(queueMessage, queuePutMessageOptions);
                MessageID = queueMessage.MessageId;

              //  Console.WriteLine("Success fully entered the message into the queue");
            }
            catch (MQException mqexp)
            {

               // Console.WriteLine("MQSeries Exception: " + mqexp.Message);
            }
            finally
            {
                closeQueueConnection();


            }
        }



        public string putShowMessageIntoFROMITREKSELECTQueueForTasks(string message, string queueName)
        {
            string returnMessage = "";
            try
            {
                createQueueConnection();
                queue = queueManager.AccessQueue(queueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);
                
                int msgLen = message.Length;
                message = message.Trim();
                while (msgLen == 0)
                {
                    if (msgLen == 0)
                        return "Add a message parameter";
                    msgLen = message.Length;
                }

                queueMessage = new MQMessage();
                queueMessage.WriteBytes(message);
                queueMessage.Format = MQC.MQFMT_STRING;


                //putting the message into the queue
                queue.Put(queueMessage);
                //add the new message id to the next messageid byte array element
                //to retain earlier messages message ids
                //MessageID[MessageID.Length + 1] = queueMessage.MessageId[MessageID.Length];

                //return MessageID[MessageID.Length + 1].ToString();
                //TODO: Check relevance of MessageID here

                returnMessage = "success";
            }
            catch (Exception mqexp)
            {

                returnMessage = "MQSeries Exception: " + mqexp.Message;
            }
            finally
            {
                closeQueueConnection();

            }
#if LoggingOn
            // iTrekUtils.iTUtils.LogToEventLog("putShowMessageIntoFROMITREKSELECTQueue " + returnMessage);
#endif
            return returnMessage;
        }
        public  string putMessageIntoFROMITREKAUTHQueueWithSessionID(string PENO, string pword, string sessID, string queueName,string src)
        {
            string returnMessage = "";
            try
            {
                createQueueConnection();
                queue = queueManager.AccessQueue(queueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);
                
                message = @"<AUTH><PENO>" + PENO + "</PENO><PASS>" + pword + "</PASS><SESSIONID>" + sessID + "</SESSIONID><SRC>"+src+"</SRC></AUTH>";
                
#if LoggingOn
                //iTrekUtils.iTUtils.LogToEventLog("Inside putAuth: " + message);
#endif
                queueMessage = new MQMessage();
                //NB. use WriteBytes here not Write
                //the message must be converted to UTF-8
                //understandable by UNIX from UTF-16
                //(Windows default)
                queueMessage.WriteBytes(message);
                queueMessage.Format = MQC.MQFMT_STRING;
                queuePutMessageOptions = new MQPutMessageOptions();

                //putting the message into the queue
                queue.Put(queueMessage, queuePutMessageOptions);
                //add the new message id to the next messageid byte array element
                //to retain earlier messages message ids
                //MessageID[MessageID.Length + 1] = queueMessage.MessageId[MessageID.Length];

                //return MessageID[MessageID.Length + 1].ToString();
                //TODO: Check relevance of MessageID here
                
                returnMessage= "success";

            }
            catch (Exception mqexp)
            {
                
              returnMessage= "MQSeries Exception: " + mqexp.Message;
            }
            finally
            {
                closeQueueConnection();

            }
          //  iTrekUtils.iTUtils.LogToEventLog("Inside putMessageIntoFROMITREKAUTHQueueWithSessionID: " + returnMessage);
#if LoggingOn
          //  iTrekUtils.iTUtils.LogToEventLog("putMessageIntoFROMITREKAUTHQueueWithSessionID: " + returnMessage);
#endif
            return returnMessage;
        }
        public  string putAllOK2LoadMessageIntoFROMITREKSELECTQueueWithSessionID(string flightno, string URNO, string sessID, string queueName)
        {
            string returnMessage = "";
            try
            {
                createQueueConnection();
                
                queue = queueManager.AccessQueue(queueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);
                message = "<ALLOK2L><URNO>" + URNO + "</URNO><SESSIONID>" + sessID + "</SESSIONID><FLIGHTNO>" + flightno + "</FLIGHTNO></ALLOK2L>";

                int msgLen = message.Length;

                while (msgLen == 0)
                {
                    if (msgLen == 0)
                        return "Add a message parameter";
                    msgLen = message.Length;
                }
                //message = message.Trim();
                queueMessage = new MQMessage();
                queueMessage.WriteBytes(message);
                queueMessage.Format = MQC.MQFMT_STRING;
                queuePutMessageOptions = new MQPutMessageOptions();

                //putting the message into the queue
                queue.Put(queueMessage, queuePutMessageOptions);
                //add the new message id to the next messageid byte array element
                //to retain earlier messages message ids
                //MessageID[MessageID.Length + 1] = queueMessage.MessageId[MessageID.Length];

                //return MessageID[MessageID.Length + 1].ToString();
                //TODO: Check relevance of MessageID here
                
                returnMessage="success";
            }
            catch (Exception mqexp)
            {
                
                returnMessage= "MQSeries Exception: " + mqexp.Message;
            }
            finally
            {
                closeQueueConnection();

            }
#if LoggingOn
           // iTrekUtils.iTUtils.LogToEventLog("putAllOK2LoadMessageIntoFROMITREKSELECTQueueWithSessionID: " + returnMessage);
#endif
            return returnMessage;
        }

        public  string putShowDLSMessageIntoFROMITREKSELECTQueueWithSessionID(string flightno, string URNO, string sessID, string queueName)
        {
            string returnMessage = "";
            try
            {
                createQueueConnection();
                queue = queueManager.AccessQueue(queueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);

                message = "<SHOWDLS><URNO>" + URNO + "</URNO><SESSIONID>" + sessID + "</SESSIONID></SHOWDLS>";
                int msgLen = message.Length;
                message = message.Trim();
                while (msgLen == 0)
                {
                    if (msgLen == 0)
                        return "Add a message parameter";
                    msgLen = message.Length;
                }

                queueMessage = new MQMessage();
                queueMessage.WriteBytes(message);
                queueMessage.Format = MQC.MQFMT_STRING;


                //putting the message into the queue
                queue.Put(queueMessage);
                //add the new message id to the next messageid byte array element
                //to retain earlier messages message ids
                //MessageID[MessageID.Length + 1] = queueMessage.MessageId[MessageID.Length];

                //return MessageID[MessageID.Length + 1].ToString();
                //TODO: Check relevance of MessageID here
                
                returnMessage= "success";
            }
            catch (Exception mqexp)
            {
                
                returnMessage= "MQSeries Exception: " + mqexp.Message;
            }
            finally
            {
                closeQueueConnection();

            }
#if LoggingOn
           // iTrekUtils.iTUtils.LogToEventLog("putShowDLSMessageIntoFROMITREKSELECTQueueWithSessionID: " + returnMessage);
#endif
            return returnMessage;
        }

        public string putShowMessageIntoFROMITREKSELECTQueue(string messageType, int messageDuration, string queueName)
        {
            string returnMessage = "";
            try
            {
                createQueueConnection();
                queue = queueManager.AccessQueue(queueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);
                if (messageType == "msg")
                {
                    message = "<MESSAGE><DURATION>" + messageDuration + "</DURATION></MESSAGE>";
                }
                else
                {
                    message = "<MESSAGETO><DURATION>" + messageDuration + "</DURATION></MESSAGETO>";

                
                }
                int msgLen = message.Length;
                message = message.Trim();
                while (msgLen == 0)
                {
                    if (msgLen == 0)
                        return "Add a message parameter";
                    msgLen = message.Length;
                }

                queueMessage = new MQMessage();
                queueMessage.WriteBytes(message);
                queueMessage.Format = MQC.MQFMT_STRING;


                //putting the message into the queue
                queue.Put(queueMessage);
                //add the new message id to the next messageid byte array element
                //to retain earlier messages message ids
                //MessageID[MessageID.Length + 1] = queueMessage.MessageId[MessageID.Length];

                //return MessageID[MessageID.Length + 1].ToString();
                //TODO: Check relevance of MessageID here

                returnMessage = "success";
            }
            catch (Exception mqexp)
            {

                returnMessage = "MQSeries Exception: " + mqexp.Message;
            }
            finally
            {
                closeQueueConnection();

            }
#if LoggingOn
           // iTrekUtils.iTUtils.LogToEventLog("putShowMessageIntoFROMITREKSELECTQueue " + returnMessage);
#endif
            return returnMessage;
        }
        public  void PutATDTimingOnFROMITREKUPDATEQueue(string URNO, string flightno, string ATDTime, string PENO, string queueName)
        {
            string message = "<ATDTIME><URNO>" + URNO + "</URNO><FLIGHTNO>" + flightno + "</FLIGHTNO><ATD>" + ATDTime + "</ATD><PENO>" + PENO + "</PENO></ATDTIME>";
            //set to the current queue?
            putTheMessageIntoQueue(message,queueName);
            
        }
        public string PutTimingOnFROMITREKUPDATEQueue(string URNO, string fncName, string time, string fltType, string queueName,string ustf)
        {
            Hashtable orgTable = new Hashtable();
            orgTable.Clear();
            orgTable.Add("MAB", "AO");
            orgTable.Add("LOAD-START", "AO");
            orgTable.Add("LOAD-END", "AO");
            orgTable.Add("LAST-DOOR", "AO");
            orgTable.Add("PLB", "AO");
            orgTable.Add("ATD", "AO");            
            orgTable.Add("ATA", "AO");
            orgTable.Add("TBS-UP", "AO");
            orgTable.Add("UNLOAD-START", "AO");
            orgTable.Add("UNLOAD-END", "AO");
            orgTable.Add("FBAG", "BO");
            orgTable.Add("LBAG", "BO");
            orgTable.Add("FTRIP-B", "BO");
            orgTable.Add("LTRIP-B", "BO");
            if (fltType == "D")
            {
                orgTable.Add("FTRIP", "BO");
                orgTable.Add("LTRIP", "BO");
            }
            else
            {
                orgTable.Add("FTRIP", "AO");
                orgTable.Add("LTRIP", "AO");
            
            }
            string message = "";
            if (fltType == "D")
            {
                message = "<DFLIGHT><URNO>" + URNO + "</URNO><" + fncName + ">" + time + "</" + fncName + "><ORG>"+orgTable[fncName].ToString()+"</ORG><PENO>"+ustf+"</PENO></DFLIGHT>";
            }
            else
            {
                message = "<AFLIGHT><URNO>" + URNO + "</URNO><" + fncName + ">" + time + "</" + fncName + "><ORG>" + orgTable[fncName].ToString() + "</ORG><PENO>" + ustf + "</PENO></AFLIGHT>";

            
            }
            //set to the current queue?
          return  putTheMessageIntoQueue(message, queueName);       
        
        }
        public void PutLOAOnFROMITREKQueue(string urno,string queueName)
            {
            string message = "";
            
                message = "<LOA>"+urno+"</LOA>";
            
            //set to the current queue?
            putTheMessageIntoQueue(message, queueName);       
        
        }

        public string putMessageIntoFROMITREKMSGQueue(string PENO,string sName, string msgFromUser,ArrayList lstReceipients, string queueName)
        {
            string returnMessage = "";
            try
            {
                createQueueConnection();
                queue = queueManager.AccessQueue(queueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);
              
                string recList = "<MSGTO>";
                foreach (string receipient in lstReceipients)
                {
                    recList += "<MSG_TO_ID>" + receipient + "</MSG_TO_ID>";
                }
                recList += "</MSGTO>";
               // message = "<MSG><SENT_BY_ID>00127364</SENT_BY_ID><SENT_BY_NAME>CHARLES</SENT_BY_NAME><MSG_BODY>Testing 123</MSG_BODY><MSGTO><MSG_TO_ID>00127364</MSG_TO_ID></MSGTO></MSG>";

                message = "<MSG><SENT_BY_ID>" + PENO + "</SENT_BY_ID><SENT_BY_NAME>" + sName + "</SENT_BY_NAME><MSG_BODY>" + msgFromUser + "</MSG_BODY>" + recList + "</MSG>";
                message = removeBadCharacters(message);
#if LoggingOn
               iTrekUtils.iTUtils.LogToEventLog("Inside Message: " + message);
#endif
                queueMessage = new MQMessage();
                ////NB. use WriteBytes here not Write
                ////the message must be converted to UTF-8
                ////understandable by UNIX from UTF-16
                ////(Windows default)
                queueMessage.WriteBytes(message);
                queueMessage.Format = MQC.MQFMT_STRING;
                queuePutMessageOptions = new MQPutMessageOptions();

                ////putting the message into the queue
                queue.Put(queueMessage, queuePutMessageOptions);
                ////add the new message id to the next messageid byte array element
                ////to retain earlier messages message ids
                ////MessageID[MessageID.Length + 1] = queueMessage.MessageId[MessageID.Length];

                ////return MessageID[MessageID.Length + 1].ToString();
                ////TODO: Check relevance of MessageID here

                returnMessage = "success";

            }
            catch (Exception mqexp)
            {

                returnMessage = "MQSeries Exception: " + mqexp.Message;
            }
            finally
            {
                closeQueueConnection();

            }
            //  iTrekUtils.iTUtils.LogToEventLog("Inside putMessageIntoFROMITREKAUTHQueueWithSessionID: " + returnMessage);
#if LoggingOn
           // iTrekUtils.iTUtils.LogToEventLog("putMessageIntoFROMITREKMSGQueue" + returnMessage);
#endif
            return returnMessage;
        }


        public string putMessageIntoFROMITREKMSGQueue(string urno, string staffId, string queueName)
        {
            string returnMessage = "";
            try
            {
                createQueueConnection();
                queue = queueManager.AccessQueue(queueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);


                message = "<MSGTO><URNO>" + urno + "</URNO><RCV_BY>" + staffId + "</RCV_BY><RCV_DT></RCV_DT></MSGTO>";
#if LoggingOn
                //iTrekUtils.iTUtils.LogToEventLog("Inside putAuth: " + message);
#endif
                queueMessage = new MQMessage();
                ////NB. use WriteBytes here not Write
                ////the message must be converted to UTF-8
                ////understandable by UNIX from UTF-16
                ////(Windows default)
                queueMessage.WriteBytes(message);
                queueMessage.Format = MQC.MQFMT_STRING;
                queuePutMessageOptions = new MQPutMessageOptions();

                ////putting the message into the queue
                queue.Put(queueMessage, queuePutMessageOptions);
                ////add the new message id to the next messageid byte array element
                ////to retain earlier messages message ids
                ////MessageID[MessageID.Length + 1] = queueMessage.MessageId[MessageID.Length];

                ////return MessageID[MessageID.Length + 1].ToString();
                ////TODO: Check relevance of MessageID here

                returnMessage = "success";

            }
            catch (Exception mqexp)
            {

                returnMessage = "MQSeries Exception: " + mqexp.Message;
            }
            finally
            {
                closeQueueConnection();

            }
#if LoggingOn
           // iTrekUtils.iTUtils.LogToEventLog("putMessageIntoFROMITREKMSGQueue" + returnMessage);
#endif
            //  iTrekUtils.iTUtils.LogToEventLog("Inside putMessageIntoFROMITREKAUTHQueueWithSessionID: " + returnMessage);
            return returnMessage;
        }


        private string removeBadCharacters(string message)
        {

            if (message != "")
            {
                message = message.Replace("'", "");
            }

            
            return message;

        }
        public MQQueue getQueueConnectionForServices(string qName, int qType,string mqStatus)
        {
           

            if (mqStatus == "SINGLE")
            {
               
                queue = getQManager.AccessQueue(qName, qType);
            }
            else
            {
                getQueueConnection(qName, qType);
            
            }
#if LoggingOn
           // iTrekUtils.iTUtils.LogToEventLog("getQueueConnectionForServices: " + queue.Name);
#endif
            return queue;

        }

        public void closeQueueConnectionForServices(MQQueue locque, MQQueueManager locqueueManager, string mqStatus)
        {
            
            if (mqStatus == "SINGLE")
            {
              
                if (locque.IsOpen)
                {
#if LoggingOn
                    //    iTrekUtils.iTUtils.LogToEventLog("closeQueueConnectionForServices: " + locque.Name);
#endif
                    locque.Close();
                    locque.CloseOptions = MQC.MQCO_DELETE;


                }
            }
            else
            {
                closeQueueConnection(locque, locqueueManager);
            
            }

           
        }
    }
}
