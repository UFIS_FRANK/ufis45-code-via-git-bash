//#define TestsOn
using System;
using System.Collections.Generic;
using System.Text;
using IBM.WMQ;
using iTrekXML;
using System.Collections;
using iTrekUtils;
using iTrekWMQ;
#if TestsOn
using NUnit.Framework;
#endif
using System.Security.Policy;   //for  evidence  object
using iTrekSessionMgr;


#if TestsOn
namespace iTrekWMQTests
{
    [TestFixture]
    public class iTrekWMQTests
    {
        //currently Ok2LWMQ tests only
        iTXMLcl iTXML;
        iTrekWMQClass mqObj;
        iTXMLPathscl iTPaths;
        SessionManager sessMan;

        //[SetUp]
        public iTrekWMQTests()
        {
            iTPaths = new iTXMLPathscl();
            sessMan = new SessionManager();

            //As we want to add multiple assemblies to the NUnit project
            //at PushServ directory level 
            //we need to point NUNit at the relevant ApplicationBase
            //when instantiating classes
            //This one is for iTrekOk2L (points to config xml file)

            //Create  the  application  domain  setup  information.
            AppDomainSetup domaininfo = new AppDomainSetup();
            domaininfo.ApplicationBase = System.Environment.CurrentDirectory;
            //Create  evidence  for  new  appdomain.
            Evidence adevidence = AppDomain.CurrentDomain.Evidence;
            //  Create  the  new  application  domain  using  setup  information.
            AppDomain domain = AppDomain.CreateDomain("iTrekOk2L", adevidence, domaininfo);

            iTXML = new iTXMLcl(domain.SetupInformation.ApplicationBase);
            //mqObj = new iTrekOk2L.WMQClass(iTPaths.GetTOPUSHOK2LOADQueueName());
            //mqObj = new iTrekWMQClass(iTPaths.GetTOPUSHOK2LOADQueueName());
            mqObj = new iTrekWMQClass(iTPaths.GetTOPUSHOK2LOADQueueName());
        }

        //TODO: Coverage high
        //[Test]
        //public void TOPUSHOK2LOADQueue()
        //{
        //    mqObj.putTheMessageIntoQueue("Test message");
        //    Assert.AreEqual("Test message", mqObj.GetMessageFromTheQueue());
        //}

        //TODO: Coverage high
        [Test]
        public void PurgeMessageQueue()
        {
            mqObj.putTheMessageIntoQueue("Test message");
            mqObj.PurgeQueue();
            Assert.AreEqual("MQRC_NO_MSG_AVAILABLE", mqObj.GetMessageFromTheQueue());
        }

        //TODO: Coverage high
        [Test]
        public void Put3MsgOnQueueGet3MsgBackInSameOrder()
        {
            mqObj.PurgeQueue();
            mqObj.putTheMessageIntoQueue("First Test message");
            mqObj.putTheMessageIntoQueue("Second Test message");
            mqObj.putTheMessageIntoQueue("Third Test message");
            Assert.AreEqual("First Test message", mqObj.GetMessageFromTheQueue());
            Assert.AreEqual("Second Test message", mqObj.GetMessageFromTheQueue());
            Assert.AreEqual("Third Test message", mqObj.GetMessageFromTheQueue());
        }

        //TODO: Coverage medium
        //Only testing that message going onto queue
        //not that appropriate result being returned in 
        //TO.ITREK.READ_RES
        [Test]
        public void IntoFROMITREKAUTHQueueWithSessionIDTest()
        {
            iTrekWMQClass FROMITREKAUTHQueue = new iTrekWMQClass(iTPaths.GetFROMITREKAUTHQueueName());
            FROMITREKAUTHQueue.putMessageIntoFROMITREKAUTHQueueWithSessionID(iTPaths.GetTestPENO(), iTPaths.GetTestPassword(), iTPaths.GetTestSessionId());
            Assert.AreEqual("<AUTH><PENO>" + iTPaths.GetTestPENO() + "</PENO><PASS>" + iTPaths.GetTestPassword() + "</PASS><SESSIONID>" + iTPaths.GetTestSessionId() + "</SESSIONID></AUTH>", FROMITREKAUTHQueue.GetMessageFromTheQueue());
        }

        //TODO: Coverage medium
        //Only testing that message going onto queue
        [Test]
        public void FromTOITREKREAD_RESQueueTest()
        {
            iTrekWMQClass TOITREKREAD_RESQueue = new iTrekWMQClass(iTPaths.GetTOITREKREAD_RESQueueName());
            TOITREKREAD_RESQueue.PurgeQueue();
            TOITREKREAD_RESQueue.putTheMessageIntoQueue("Test message from GetMessageFromTOITREKREAD_RESQueue");
            Assert.AreEqual("Test message from GetMessageFromTOITREKREAD_RESQueue", TOITREKREAD_RESQueue.GetMessageFromTheQueue());
        }

        //TODO: Coverage medium
        //Only testing that message going onto queue
        [Test]
        public void FROMITREKUPDATEQueueTest()
        {
            iTrekWMQClass FROMITREKUPDATEQueue = new iTrekWMQClass(iTPaths.GetFROMITREKUPDATEQueueName());
            FROMITREKUPDATEQueue.PurgeQueue();
            FROMITREKUPDATEQueue.putTheMessageIntoQueue("Test message from FROMITREKUPDATEQueue");
            Assert.AreEqual("Test message from FROMITREKUPDATEQueue", FROMITREKUPDATEQueue.GetMessageFromTheQueue());
        }

        //TODO: Coverage medium
        //Only testing that message going onto queue
        [Test]
        public void TOITREKREAD_AFTQueueTest()
        {
            iTrekWMQClass TOITREKREAD_AFTQueue = new iTrekWMQClass(iTPaths.GetTOITREKREAD_AFTQueueName());
            TOITREKREAD_AFTQueue.PurgeQueue();
            TOITREKREAD_AFTQueue.putTheMessageIntoQueue("Test message from TOITREKREAD_AFTQueue");

            Assert.AreEqual("Test message from TOITREKREAD_AFTQueue", TOITREKREAD_AFTQueue.GetMessageFromTheQueue());
        }

        //TODO: Coverage medium
        //Only testing that message going onto queue
        [Test]
        public void TOITREKREAD_JOBQueueTest()
        {
            iTrekWMQClass TOITREKREAD_JOBQueue = new iTrekWMQClass(iTPaths.GetTOITREKREAD_JOBQueueName());
            TOITREKREAD_JOBQueue.PurgeQueue();
            TOITREKREAD_JOBQueue.putTheMessageIntoQueue("Test message from TOITREKREAD_JOBQueueTest");
            Assert.AreEqual("Test message from TOITREKREAD_JOBQueueTest", TOITREKREAD_JOBQueue.GetMessageFromTheQueue());
        }

        //TODO: Coverage medium
        //Only testing that message going onto queue
        [Test]
        public void FROMITREKAUTHQueueTest()
        {
            iTrekWMQClass FROMITREKAUTHQueue = new iTrekWMQClass(iTPaths.GetFROMITREKAUTHQueueName());
            FROMITREKAUTHQueue.PurgeQueue();
            FROMITREKAUTHQueue.putTheMessageIntoQueue("Test message from FROMITREKAUTHQueue");
            Assert.AreEqual("Test message from FROMITREKAUTHQueue", FROMITREKAUTHQueue.GetMessageFromTheQueue());
        }


        //TODO: Coverage low
        //Only testing that message going onto queue
        [Test]
        public void TOITREKAUTHQueueTest()
        {
            iTrekWMQClass TOITREKAUTHQueue = new iTrekWMQClass(iTPaths.GetTOITREKAUTHQueueName());
            TOITREKAUTHQueue.PurgeQueue();
            TOITREKAUTHQueue.putTheMessageIntoQueue("Test message from TOITREKAUTHQueue");
            Assert.AreEqual("Test message from TOITREKAUTHQueue", TOITREKAUTHQueue.GetMessageFromTheQueue());

        }

        //TODO: Coverage medium       
        //Need to see result on handset - Alert Msg
        //Needs OK2LSvc to be running
        [Test]
        public void TOPUSHOK2LOADQueue()
        {
            iTrekWMQClass TOPUSHOK2LOADQueue = new iTrekWMQClass(iTPaths.GetTOPUSHOK2LOADQueueName());
            TOPUSHOK2LOADQueue.PurgeQueue();
            TOPUSHOK2LOADQueue.putTheMessageIntoQueue("<OK2L><ULDNUMBER>AKE123456CX</ULDNUMBER><UAFT>" + iTPaths.GetOK2LSvcTestUAFT() + "</UAFT></OK2L>");
            //Assert.AreEqual("Test message from TOITREKAUTHQueue", TOITREKAUTHQueue.GetMessageFromTheQueue());
            Assert.AreEqual("", "", "Setup valid device in DeviceID.xml?");

        }
        //TODO: Coverage medium
        //Only testing that message going onto queue
        [Test]
        public void FROMITREKSELECTQueueTest()
        {
            iTrekWMQClass FROMITREKSELECTQueue = new iTrekWMQClass(iTPaths.GetFROMITREKSELECTQueueName());
            FROMITREKSELECTQueue.PurgeQueue();
            FROMITREKSELECTQueue.putTheMessageIntoQueue("Test message from FROMITREKSELECTQueue");
            Assert.AreEqual("Test message from FROMITREKSELECTQueue", FROMITREKSELECTQueue.GetMessageFromTheQueue());
        }

    }
}

#endif

namespace iTrekWMQ
{
    /// <summary>
    /// Manages iTrek queues
    /// </summary>
    public class iTrekWMQClass
    {
        MQQueueManager queueManager;
        MQQueue queue;
        MQMessage queueMessage;
        MQPutMessageOptions queuePutMessageOptions;
        MQGetMessageOptions queueGetMessageOptions;

        static string QueueName;
        static string QueueManagerName;
        //static string ChannelInfo;

        string channelName;
        string transportType;
        string connectionName;

        string message = "";
        byte[] MessageID;

        iTXMLPathscl iTPaths;

        public iTrekWMQClass(string QueueNm)
        {

            iTPaths = new iTXMLPathscl();
            //TODO: Don't use queue name at this level
            //can be instatiated twice and change relevant queue
            QueueName = QueueNm;
            QueueManagerName = iTPaths.GetQueueManagerName();

            channelName = iTPaths.GetChannelName();
            transportType = iTPaths.GetChannelTransportType();

            connectionName = iTPaths.GetChannelServer();
            connectionName += iTPaths.GetChannelPortNumber();


            string serverAndPort = iTPaths.GetChannelServer();
            serverAndPort += iTPaths.GetChannelPortNumber();
            queueManager = new MQQueueManager(QueueManagerName, iTPaths.GetChannelName(), serverAndPort);

        }

        public void PurgeQueue()
        {
            while (GetMessageFromTheQueue() != "MQRC_NO_MSG_AVAILABLE")
            {
                GetMessageFromTheQueue();
            }
        }

        //only used by console application
        public string putTheMessageIntoQueue()
        {
            try
            {
                
                queue = queueManager.AccessQueue(QueueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);

                return "Enter the message to put in MQSeries server:";
                //message = System.Console.ReadLine();
                int msgLen = message.Length;

                while (msgLen == 0)
                {
                    if (msgLen == 0)
                        return "Please reenter the message:";
                    message = System.Console.ReadLine();
                    msgLen = message.Length;
                }

                queueMessage = new MQMessage();
                queueMessage.WriteBytes(message);
                queueMessage.Format = MQC.MQFMT_STRING;
                queuePutMessageOptions = new MQPutMessageOptions();

                //putting the message into the queue
                queue.Put(queueMessage, queuePutMessageOptions);
                MessageID = queueMessage.MessageId;
                queue.Close();
                queueManager.Close();
                return "Success fully entered the message into the queue";
            }
            catch (Exception mqexp)
            {
                queue.Close();
                queueManager.Close();
                return "MQSeries Exception: " + mqexp.Message;
            }
        }

        public string GetMessageFromTheQueue()
        {

            queue = queueManager.AccessQueue(QueueName, MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
            queueMessage = new MQMessage();
            queueMessage.Format = MQC.MQFMT_STRING;
            if (MessageID != null)
                queueMessage.MessageId = MessageID;

            queueGetMessageOptions = new MQGetMessageOptions();

            try
            {
                queue.Get(queueMessage, queueGetMessageOptions);
                // Received Message.
                //return "MessageID:" + MessageID + " Message:" + queueMessage.ReadString(queueMessage.MessageLength);
                queue.Close();
                queueManager.Close();
                return queueMessage.ReadString(queueMessage.MessageLength);
            }
            catch (Exception MQExp)
            {
                //HACK: Need to stop using exception to control queue purge
                queue.Close();
                queueManager.Close();
                return MQExp.Message;
            }
        }

        public string GetMessageFromTheQueueAnyOrder()
        {
            channelName = iTPaths.GetChannelName();
            transportType = iTPaths.GetChannelTransportType();

            connectionName = iTPaths.GetChannelServer();
            connectionName += iTPaths.GetChannelPortNumber();

            queueManager = new MQQueueManager(QueueManagerName, channelName, connectionName);
            //In office
            //queueManager = new MQQueueManager(QueueManagerName, "CHANNEL1", "192.168.1.8");
            //At home
            //queueManager = new MQQueueManager(QueueManagerName, "CHANNEL1", "http://www.ufissin.homelinux.net/");

            queue = queueManager.AccessQueue(QueueName, MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
            queueMessage = new MQMessage();
            queueMessage.Format = MQC.MQFMT_STRING;
            //queueMessage.MessageId = MessageID;
            queueGetMessageOptions = new MQGetMessageOptions();

            try
            {
                queue.Get(queueMessage, queueGetMessageOptions);

                // Received Message.
                //System.Console.WriteLine("Received Message: {0}", queueMessage.ReadString(queueMessage.MessageLength));
                queue.Close();
                queueManager.Close();
                return queueMessage.ReadString(queueMessage.MessageLength);

            }
            catch (Exception MQExp)
            {
                queue.Close();
                queueManager.Close();
                return "MQQueue::Get ended with " + MQExp.Message;
            }
        }

        public void GetConsoleMessageFromTheQueueAnyOrder()
        {

            channelName = iTPaths.GetChannelName();
            transportType = iTPaths.GetChannelTransportType();

            connectionName = iTPaths.GetChannelServer();
            connectionName += iTPaths.GetChannelPortNumber();

            //QueueName = iTPaths.GetTOPUSHOK2LOADQueueName();
            //QueueManagerName = "venus.queue.manager";
            //ChannelInfo = "CHANNEL1/TCP/192.168.1.8";

            queueManager = new MQQueueManager(QueueManagerName, channelName, connectionName);
            //In office
            //queueManager = new MQQueueManager(QueueManagerName, "CHANNEL1", "192.168.1.8");
            //At home
            //queueManager = new MQQueueManager(QueueManagerName, "CHANNEL1", "http://www.ufissin.homelinux.net/");

            queue = queueManager.AccessQueue(QueueName, MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
            queueMessage = new MQMessage();
            queueMessage.Format = MQC.MQFMT_STRING;
            //queueMessage.MessageId = MessageID;
            queueGetMessageOptions = new MQGetMessageOptions();

            try
            {
                queue.Get(queueMessage, queueGetMessageOptions);

                // Received Message.
                System.Console.WriteLine("Received Message: {0}", queueMessage.ReadString(queueMessage.MessageLength));
                //return queueMessage.ReadString(queueMessage.MessageLength);
                queue.Close();
                queueManager.Close();

            }
            catch (Exception MQExp)
            {
                // report the error
                //System.Console.WriteLine("MQQueue::Get ended with " + MQExp.Message);
                System.Console.WriteLine("MQQueue::Get ended with " + MQExp.Message);
                queue.Close();
                queueManager.Close();
            }
        }

        public string GetMessageFromTheQueueReturnString()
        {
            channelName = iTPaths.GetChannelName();
            transportType = iTPaths.GetChannelTransportType();

            connectionName = iTPaths.GetChannelServer();
            connectionName += iTPaths.GetChannelPortNumber();

            queueManager = new MQQueueManager(QueueManagerName, channelName, connectionName);

            queue = queueManager.AccessQueue(QueueName, MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
            queueMessage = new MQMessage();
            queueMessage.Format = MQC.MQFMT_STRING;
            //queueMessage.MessageId = MessageID;
            queueGetMessageOptions = new MQGetMessageOptions();

            try
            {
                queue.Get(queueMessage, queueGetMessageOptions);
                queue.Close();
                queueManager.Close();
                // Received Message.
                return queueMessage.ReadString(queueMessage.MessageLength);
                //System.Console.WriteLine("Received Message: {0}", queueMessage.MessageId[0].ToString());

            }
            catch (MQException MQExp)
            {
                // report the error
                queue.Close();
                queueManager.Close();
                return MQExp.Message;
            }
        }

        public void putXMLMessageIntoQueue()
        {
            try
            {
                channelName = iTPaths.GetChannelName();
                transportType = iTPaths.GetChannelTransportType();

                connectionName = iTPaths.GetChannelServer();
                connectionName += iTPaths.GetChannelPortNumber();

                queueManager = new MQQueueManager(QueueManagerName, channelName, connectionName);
                queue = queueManager.AccessQueue(QueueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);


                message = iTUtils.ReadFromFile(@"C:\temp\tests\bigfile.xml");

                int msgLen = message.Length;

                while (msgLen == 0)
                {
                    if (msgLen == 0)
                        Console.WriteLine("Please reenter the message:");
                    message = System.Console.ReadLine();
                    msgLen = message.Length;
                }

                queueMessage = new MQMessage();
                queueMessage.WriteBytes(message);
                queueMessage.Format = MQC.MQFMT_STRING;
                queuePutMessageOptions = new MQPutMessageOptions();

                //putting the message into the queue
                queue.Put(queueMessage, queuePutMessageOptions);
                MessageID = queueMessage.MessageId;
                queue.Close();
                queueManager.Close();
                Console.WriteLine("Success fully entered the message into the queue");
            }
            catch (MQException mqexp)
            {
                queue.Close();
                queueManager.Close();
                Console.WriteLine("MQSeries Exception: " + mqexp.Message);
            }
        }

        public string putTheMessageIntoQueue(string message)
        {
            try
            {
                queueManager = new MQQueueManager(QueueManagerName, channelName, connectionName);
                queue = queueManager.AccessQueue(QueueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);


                int msgLen = message.Length;

                while (msgLen == 0)
                {
                    if (msgLen == 0)
                        return "Add a message parameter";
                    msgLen = message.Length;
                }

                queueMessage = new MQMessage();
                queueMessage.WriteBytes(message);
                queueMessage.Format = MQC.MQFMT_STRING;
                queuePutMessageOptions = new MQPutMessageOptions();

                //putting the message into the queue
                queue.Put(queueMessage, queuePutMessageOptions);
                //add the new message id to the next messageid byte array element
                //to retain earlier messages message ids
                MessageID[MessageID.Length + 1] = queueMessage.MessageId[MessageID.Length];
                queue.Close();
                queueManager.Close();
                queueManager.Disconnect();
                return MessageID[MessageID.Length + 1].ToString();

                //return queueMessage.MessageId.ToString();
            }
            catch (Exception mqexp)
            {
                queue.Close();
                queueManager.Close();
                queueManager.Disconnect();
                return "MQSeries Exception: " + mqexp.Message;
            }
        }

        public string putConsoleMessageIntoQueue(string message)
        {
            try
            {
                queueManager = new MQQueueManager(QueueManagerName, channelName, connectionName);
                queue = queueManager.AccessQueue(QueueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);

                int msgLen = message.Length;

                while (msgLen == 0)
                {
                    if (msgLen == 0)
                        return "Add a message parameter";
                    msgLen = message.Length;
                }

                queueMessage = new MQMessage();
                queueMessage.WriteBytes(message);
                queueMessage.Format = MQC.MQFMT_STRING;
                queuePutMessageOptions = new MQPutMessageOptions();

                //putting the message into the queue
                queue.Put(queueMessage, queuePutMessageOptions);
                //add the new message id to the next messageid byte array element
                //to retain earlier messages message ids
                //MessageID[MessageID.Length +1] = queueMessage.MessageId[MessageID.Length];

                //return MessageID[MessageID.Length + 1].ToString();
                MessageID = queueMessage.MessageId;
                //return MessageID[MessageID.Length - 1].ToString(); ;
                //return queueMessage.MessageId.ToString();
                queue.Close();
                queueManager.Close();
                return message;
            }
            catch (Exception mqexp)
            {
                queue.Close();
                queueManager.Close();
                return "MQSeries Exception: " + mqexp.Message;
            }
        }

        public void putMessageIntoQueueForMultipleMessages(string msg)
        {
            try
            {
                channelName = iTPaths.GetChannelName();
                transportType = iTPaths.GetChannelTransportType();

                connectionName = iTPaths.GetChannelServer();
                connectionName += iTPaths.GetChannelPortNumber();

                queueManager = new MQQueueManager(QueueManagerName, channelName, connectionName);
                queue = queueManager.AccessQueue(QueueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);

                message = msg;

                queueMessage = new MQMessage();
                queueMessage.WriteBytes(message);
                queueMessage.Format = MQC.MQFMT_STRING;
                queuePutMessageOptions = new MQPutMessageOptions();

                //putting the message into the queue
                queue.Put(queueMessage, queuePutMessageOptions);
                MessageID = queueMessage.MessageId;
                queue.Close();
                queueManager.Close();
                Console.WriteLine("Success fully entered the message into the queue");
            }
            catch (MQException mqexp)
            {
                queue.Close();
                queueManager.Close();
                Console.WriteLine("MQSeries Exception: " + mqexp.Message);
            }
        }

        public string putMessageIntoFROMITREKAUTHQueueWithSessionID(string PENO, string pword, string sessID)
        {
            try
            {
                QueueName = iTPaths.GetFROMITREKAUTHQueueName();
                queueManager = new MQQueueManager(QueueManagerName, channelName, connectionName);
                queue = queueManager.AccessQueue(QueueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);

                message = @"<AUTH><PENO>" + PENO + "</PENO><PASS>" + pword + "</PASS><SESSIONID>" + sessID + "</SESSIONID></AUTH>";

                queueMessage = new MQMessage();
                //NB. use WriteBytes here not Write
                //the message must be converted to UTF-8
                //understandable by UNIX from UTF-16
                //(Windows default)
                queueMessage.WriteBytes(message);
                queueMessage.Format = MQC.MQFMT_STRING;
                queuePutMessageOptions = new MQPutMessageOptions();

                //putting the message into the queue
                queue.Put(queueMessage, queuePutMessageOptions);
                //add the new message id to the next messageid byte array element
                //to retain earlier messages message ids
                //MessageID[MessageID.Length + 1] = queueMessage.MessageId[MessageID.Length];

                //return MessageID[MessageID.Length + 1].ToString();
                //TODO: Check relevance of MessageID here
                queue.Close();
                queueManager.Close();
                return "success";
            }
            catch (Exception mqexp)
            {
                queue.Close();
                queueManager.Close();
                return "MQSeries Exception: " + mqexp.Message;
            }
        }

        public string putAllOK2LoadMessageIntoFROMITREKSELECTQueueWithSessionID(string flightno, string URNO, string sessID)
        {
            try
            {
                QueueName = iTPaths.GetFROMITREKSELECTQueueName();
                queueManager = new MQQueueManager(QueueManagerName, channelName, connectionName);
                queue = queueManager.AccessQueue(QueueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);
                message = "<ALLOK2L><URNO>" + URNO + "</URNO><SESSIONID>" + sessID + "</SESSIONID><FLIGHTNO>" + flightno + "</FLIGHTNO></ALLOK2L>";

                int msgLen = message.Length;

                while (msgLen == 0)
                {
                    if (msgLen == 0)
                        return "Add a message parameter";
                    msgLen = message.Length;
                }
                //message = message.Trim();
                queueMessage = new MQMessage();
                queueMessage.WriteBytes(message);
                queueMessage.Format = MQC.MQFMT_STRING;
                queuePutMessageOptions = new MQPutMessageOptions();

                //putting the message into the queue
                queue.Put(queueMessage, queuePutMessageOptions);
                //add the new message id to the next messageid byte array element
                //to retain earlier messages message ids
                //MessageID[MessageID.Length + 1] = queueMessage.MessageId[MessageID.Length];

                //return MessageID[MessageID.Length + 1].ToString();
                //TODO: Check relevance of MessageID here
                queue.Close();
                queueManager.Close();
                return "success";
            }
            catch (Exception mqexp)
            {
                queue.Close();
                queueManager.Close();
                return "MQSeries Exception: " + mqexp.Message;
            }
        }

        public string putShowDLSMessageIntoFROMITREKSELECTQueueWithSessionID(string flightno, string URNO, string sessID)
        {
            try
            {
                QueueName = iTPaths.GetFROMITREKSELECTQueueName();
                queueManager = new MQQueueManager(QueueManagerName, channelName, connectionName);
                queue = queueManager.AccessQueue(QueueName, MQC.MQOO_OUTPUT + MQC.MQOO_FAIL_IF_QUIESCING);

                message = "<SHOWDLS><URNO>" + URNO + "</URNO><SESSIONID>" + sessID + "</SESSIONID></SHOWDLS>";
                int msgLen = message.Length;
                message = message.Trim();
                while (msgLen == 0)
                {
                    if (msgLen == 0)
                        return "Add a message parameter";
                    msgLen = message.Length;
                }

                queueMessage = new MQMessage();
                queueMessage.WriteBytes(message);
                queueMessage.Format = MQC.MQFMT_STRING;


                //putting the message into the queue
                queue.Put(queueMessage);
                //add the new message id to the next messageid byte array element
                //to retain earlier messages message ids
                //MessageID[MessageID.Length + 1] = queueMessage.MessageId[MessageID.Length];

                //return MessageID[MessageID.Length + 1].ToString();
                //TODO: Check relevance of MessageID here
                queue.Close();
                queueManager.Close();
                return "success";
            }
            catch (Exception mqexp)
            {
                queue.Close();
                queueManager.Close();
                return "MQSeries Exception: " + mqexp.Message;
            }
        }

        //TODO: Relevance?
        public void PutATDTimingOnFROMITREKUPDATEQueue(string URNO, string flightno, string ATDTime, string PENO)
        {
            string message = "<ATDTIME><URNO>" + URNO + "</URNO><FLIGHTNO>" + flightno + "</FLIGHTNO><ATD>" + ATDTime + "</ATD><PENO>" + PENO + "</PENO></ATDTIME>";
            //set to the current queue?
            putTheMessageIntoQueue(message);
            queue.Close();
            queueManager.Close();
        }
    }
}