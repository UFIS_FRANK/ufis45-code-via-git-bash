using System;
using System.Text;
using System.Collections;

namespace iTrekSessionMgr
{
    public class Message:IComparable
    {
        private string _msgUrno;  
        private string _fromId;
        private string _fromName;
        private string _sendDateTime;
        private string _msgBody;
        private ArrayList _lstReceipients;
       
        
        public Message()
        { 
              
        
        }

        public Message(string msgUrno,string fromId,string fromName,string sendDateTime,string msgBody,ArrayList lstReceipients)
        {
            this._msgUrno = msgUrno;
            this._fromId = fromId;
            this._fromName = fromName;
            this._msgBody = msgBody;
            this._sendDateTime = sendDateTime;
            this._lstReceipients = lstReceipients;
        
        
        }
        public string MsgUrno
        {
            get { return _msgUrno; }
        
        }
        public string FromId
        {
            get { return _fromId; }
        
        }
        public string FromName
        {
            get { return _fromName; }

        }
        public string MsgBody
        {
            get { return _msgBody; }

        }
        public string SendDateTime
        {
            get { return _sendDateTime; }

        }
        public ArrayList LstReceipients
        {
            get
            {
                return _lstReceipients; 
            
            }
        }

        public int CompareTo(object obj)
        {
            Message Compare = (Message)obj;
            int result = this.SendDateTime.CompareTo(Compare.SendDateTime);
            //if (result == 0)
            //    result = this.SendDateTime.CompareTo(Compare.SendDateTime);
            return result;
        }








    }
}
