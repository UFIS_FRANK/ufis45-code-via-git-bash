#define LoggingOn
#define UsingQueues
//#define GET_MSGLOG_FROM_RPT_QUEUE
#define TRACING_ON

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using System.IO;
using iTrekXML;
using System.Xml;
using iTrekSessionMgr;
using iTrekWMQ;
using IBM.WMQ;
using System.Timers;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.Ctrl;
using System.Collections;
using iTrekUtils;
using UFIS.ITrekLib.Util;
using UFIS.ITrekLib.CommMsg;
using MM.UtilLib;

namespace iTrekMgiSvc
{
	/// <summary>
	/// All insert,update and Batch Messages for n hours of duration is 
	/// handled by this service.Alert will be send when a new message to any logged in 
	/// user to device.
	/// Actions-Insert-when new message comes
	/// Update-when a receipient reads the message,the RCVBY field will
	/// be updated.
	/// MSGS and MSGSTO files will be send when a request for 12 hours duration is
	/// requested.
	/// MSGLOG-When user request for a message log.
	/// </summary>



	public partial class iTrekMgiSvc : ServiceBase
	{
		iTXMLcl iTXML;
		iTXMLPathscl iTPaths;

		IDbCommand cmd = null;
		UtilDb db = UtilDb.GetInstance();
		DateTime _lastRefreshTime = DateTime.Now;
		IDbConnection conn = null;
		private static UtilTraceLog _traceLog = UtilTraceLog.GetInstance();
		public iTrekMgiSvc()
		{
			InitializeComponent();

			iTXML = new iTXMLcl();

			iTPaths = new iTXMLPathscl();

			_lastRefreshTime.AddHours(-1.0);
		}

		protected override void OnStart(string[] args)
		{
			string baseDir = AppDomain.CurrentDomain.BaseDirectory;
			DirectoryInfo directoryInfo =
						 Directory.GetParent(baseDir);
			baseDir = directoryInfo.FullName;
			directoryInfo =
						 Directory.GetParent(baseDir);
			baseDir = directoryInfo.FullName;

			string logDir = Path.Combine(Path.Combine(baseDir, "XML"), "LOG");
			if (logDir[logDir.Length - 1] != '\\') logDir += "\\";
			LogTraceMsg(logDir);
			_traceLog.SetTrace(true);
			_traceLog.SetLogDir(logDir);
			_traceLog.SetLogFileName("CMsgIn");

			MessageQueueTimer();
		}

		private void LogTraceMsg(string msg)
		{
			try
			{
				UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("CMsgIn", msg);
			}
			catch (Exception)
			{
			}
		}

		protected override void OnStop()
		{
			CloseConnection();
			LogMsg("OnStop");
		}

		private void CloseConnection()
		{
			if (conn != null)
			{
				UtilDb.CloseConnection(conn);
				LogTraceMsg("CloseConnection");
			}
		}

		public void MessageQueueTimer()
		{
			System.Timers.Timer aTimer = new System.Timers.Timer();
			aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
			// Interval set in JOB_AFT_SERVICESTIMER
			aTimer.Interval = Convert.ToInt32(iTPaths.GetJOB_AFT_SERVICESTIMER());
			aTimer.Enabled = true;
		}

		private static bool _IsProcessing = false;
		private static object _lockObj = new object();

		private int _cntProc = 0;

		// Specify what you want to happen when the Elapsed event is raised.
		private void OnTimedEvent(object source, ElapsedEventArgs e)
		{
			try
			{
				if (!_IsProcessing)
				{
					lock (_lockObj)
					{
						if (!_IsProcessing)
						{
							try
							{
								_IsProcessing = true;
								_cntProc++;
								DoProcess();
							}
							catch (Exception ex)
							{
								CloseConnection();
								LogMsg("OnTimedEvent:ProcErr:" + ex.Message);
							}
							finally
							{
								if (_cntProc > 50)
								{
									LogMsg("Active");
									_cntProc = 0;
									CloseConnection();
								}
								_IsProcessing = false;
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				LogMsg("OnTimedEvent:Err:" + ex.Message);
			}
		}

		private void LogMsg(string msg)
		{
			try
			{
				if (msg != null)
				{
					int len = msg.Length;
					LogTraceMsg(msg);
					if (len > 200) len = 200;
					iTrekMgiLog.WriteEntry(msg.Substring(0,len));
				}
			}
			catch (Exception)
			{
			}
		}

		private void DoProcess()
		{
			try
			{
				for (int i = 0; i < 30; i++)
				{
					if (GetMessageFromDB() == 0)
					{
						System.Threading.Thread.Sleep(200);//Nothing to process. Sleep
					}
					else
					{
						UpdateAoId();
						System.Threading.Thread.Sleep(100);//Processed all the Incoming Messages. Sleep
					}
				}
				UpdateAoId();				
			}
			catch (Exception ex)
			{
				CloseConnection();
				LogMsg("DoProcess:Err:" + ex.Message);
			}
		}

		private void UpdateAoId()
		{
			try
			{
				if (CtrlFlight.UpdateAoIds(conn, CtrlJob.RetrieveAOIDsForAFlight(_lastRefreshTime), "ITKHDL"))
				{
					_lastRefreshTime = DateTime.Now.AddMinutes(-5);
				}
			}
			catch (Exception ex)
			{
				LogMsg("UpdateAoId:Err:" + ex.Message);
			}
		}

		//private IDbCommand GetCommand()
		//{
		//    if (cmd == null)
		//    {
		//        cmd = db.GetCommand(true);
		//    }
		//    return cmd;
		//}

		private IDbConnection GetConnection()
		{
			if (conn == null || conn.State == ConnectionState.Closed || conn.State==ConnectionState.Broken)
			{
				CloseConnection();
				try
				{
					if (db == null) db = UtilDb.GetInstance();
					conn = db.GetConnection(true);
				}
				catch (Exception)
				{
					conn = null;
					db = null;
					throw;
				}
				if (conn != null)
					LogMsg("Get Connection");
			}
			return conn;
		}

		//private void CommitTransaction(bool commit, string urno)
		//{
		//    try
		//    {
		//        if (commit)
		//        {
		//            CMsgIn.UpdProcIndForInMessage(db, GetCommand(), urno, "", "");
		//        }
		//    }
		//    catch (Exception)
		//    {
		//        throw;
		//    }
		//    finally
		//    {
		//        db.CommitTransaction(cmd, commit);
		//    }
		//}

		private int GetMessageFromDB()
		{
			int updCount = 0;
			try
			{
				IDbConnection conn = GetConnection();
				if (conn != null)
				{
					updCount = CMsgIn.ProcessAll(conn, "mgiSvc");
				}
			}
			catch (Exception ex)
			{
				iTrekMgiLog.WriteEntry("GetMessageFromDB" + ex.Message);
				throw ex;
			}

			return updCount;
		}
	}
}