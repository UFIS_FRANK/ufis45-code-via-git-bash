#define LoggingOn
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using System.Timers;
using System.Configuration;
using iTrekUtils;
using iTrekXML;
using iTrekWMQ;
using System.Security.Policy;   //for  evidence  object
using System.Xml;
using iTrekSessionMgr;
using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;
using MM.UtilLib;



namespace iTrekETDSvc
{
	/// <summary>
	/// Service to check the ETDs of the currently assigned flights
	/// and send out LoadAll alert to relevant devices
	/// if flight n mins(ETDALERTTIME) before ETD
	/// The recipient would have to be logged in because the device
	/// SID is recorded in the deviceid.xml file at login time
	/// To Test:
	/// Need a job in main Jobs.xml file with a corresponding (UAFT)
	/// flight in main Flights.xml file with a date/time set to NOW.
	/// When testing with handset also need an active device listed
	/// in DeviceID.xml
	/// 
	/// ETD Alert is send only to SQ Flights ,Departure and Passenger.
	/// </summary>

	public partial class iTrekETDSvc : ServiceBase
	{
		iTXMLcl iTXML;
		iTXMLPathscl iTPaths;

		public iTrekETDSvc()
		{
			InitializeComponent();
			iTXML = new iTXMLcl();
			iTPaths = new iTXMLPathscl();
#if LoggingOn
			LogMsg("iTrekETDSvc Initialisation");
#endif
		}

		protected override void OnStart(string[] args)
		{
#if LoggingOn
			//LogMsg("In OnStart");
#endif
			MessageQueueTimer();
		}

		protected override void OnStop()
		{
#if LoggingOn
			//LogMsg("In onStop.");
#endif
		}

		public void MessageQueueTimer()
		{
			System.Timers.Timer aTimer = new System.Timers.Timer();
			aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
			aTimer.Interval = Convert.ToInt32(iTPaths.GetETDSERVICESTIMER());
#if LoggingOn
			//LogMsg("GetETDSERVICESTIMER: " + iTPaths.GetETDSERVICESTIMER());
#endif
			aTimer.Enabled = true;
		}

		private static bool _IsProcessing = false;
		private static object _lockObj = new object();

		private int _cntProc = 0;

		// Specify what you want to happen when the Elapsed event is raised.
		private void OnTimedEvent(object source, ElapsedEventArgs e)
		{
			try
			{
				if (!_IsProcessing)
				{
					lock (_lockObj)
					{
						if (!_IsProcessing)
						{
							try
							{
								_IsProcessing = true;
								_cntProc++;
								DoProcess();
							}
							catch (Exception ex)
							{
								LogMsg("Err:" + ex.Message);
							}
							finally
							{
								if (_cntProc > 50)
								{
									LogMsg("Active");
									_cntProc = 0;
								}
								_IsProcessing = false;
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				LogMsg("OnTimedEvent:Err:" + ex.Message);
			}
		}

		private void DoProcess()
		{
			//LogMsg(message);
			//no point adding push info - user would have to re-login to 
			//read net alert - better to just obtain OK2load listing
			//add push info to querystring (no blank spaces)
			//currently unsure of exact char limit
			//string info = "OK2LOAD-FlightSQ020/AKE21304SQ/SIN/620/B";
			//info just here for standby - not currently used
			// string info = "";

			//XmlPushClient PushClient = new XmlPushClient("http://203.126.249.148:9002/pap");
			//NB. remains the same for production
			//use getpapURL function?
			// XmlPushClient PushClient = new XmlPushClient("http://57.228.106.159:9002/pap");
			XmlPushClient PushClient = new XmlPushClient(GetPapUrl());
			SessionManager sessMan = new SessionManager();

			string temp = "Push Message sent early";
			// string temp2 = "";
			string exception = "";


			/*
		* Gets the list of Flight URNOS -Condition SQ Flight,Departure Flight, Passenger Flight & n minutes
		*  before STD/ETD .This list is used by ETDService to send LOADALL alert to logged in devices who is 
		* having this flight job.
		* 
		* 
		* 
		* */
			ArrayList lstFlight = new ArrayList();
			try
			{

				int ETDAlertTime = Convert.ToInt32(iTPaths.GetETDALERTTIME());
				TimeSpan TenMinuteTimeSpan = new TimeSpan(0, ETDAlertTime, 0);
				TimeSpan ETDMinusCurrentTime = new TimeSpan();
				string term = "";
				//foreach(DSTerminal.TERMINALRow row in CtrlTerminal.RetrieveTerminal().TERMINAL.Select())
				//{

				//term=term+row.TERM_ID+";";

				//}


				//LogMsg("term: " + term);
				DSFlight flight = CtrlFlight.RetrieveFlights(term, DateTime.Now.AddHours(-1), DateTime.Now.AddHours(1));
				//foreach (DSFlight.FLIGHTRow row in flight.FLIGHT.Select("ADID='D' AND TTYP='PAX'"))
				foreach (DSFlight.FLIGHTRow row in flight.FLIGHT.Select("A_D='D' AND TTYP='PAX'"))
				{
					if (row.FLNO.Substring(0, 2) == "SQ")
					{
						DateTime flightDate = UtilTime.ConvUfisTimeStringToDateTime(row.FLIGHT_DT);

						ETDMinusCurrentTime = flightDate - DateTime.Now;
						if ((ETDMinusCurrentTime < TenMinuteTimeSpan) && (flightDate > DateTime.Now))
						{
							lstFlight.Add(row);
						}
					}
				}
			}
			catch (Exception ex)
			{
				exception = ex.Message;
				LogMsg("DoProcess:Err:" + exception);
			}

			/*Added by Alphy on 06 Dec 2006 to send loadall alert to all devices and keeping the urno in LoadAll.xml*/

			XmlDocument loadAllXmlDocument = new XmlDocument();
			string loadXmlFilePath = iTPaths.GetXMLPath() + @"\LOADALL.XML";

			loadAllXmlDocument.Load(loadXmlFilePath);
			ArrayList csvFltList = new ArrayList();
			string flNo = "";
			string flightURNO = "";
			if (lstFlight.Count != 0)
			{
				foreach (DSFlight.FLIGHTRow row in lstFlight)
				{
					flNo = "";
					flightURNO = "";
					try
					{
						flightURNO = row.URNO;
						flNo = row.FLNO;
					}
					catch (Exception)
					{
					}

					XmlNode flUrnoNode = loadAllXmlDocument.SelectSingleNode("//LOADALL/FLIGHT[URNO=" + flightURNO + "]");
					if (flUrnoNode == null)
					{
						string deviceId = "";
						string peno = "";

						try
						{
							peno = row.AOID;
						}
						catch (Exception)
						{
							throw;
						}
						try
						{
							if (peno != "")
							{
								deviceId = CtrlCurUser.RetrieveCurUserForStaffId(peno).LOGINUSER[0].DEVICE_ID;
							}
						}
						catch (Exception)
						{
						}

						if (deviceId == "")
						{
							temp = "No Devices";
						}
						else
						{
							string url = GetUrlForNetAlert() + "login.aspx";
							temp = PushClient.PushServiceIndication(deviceId, "LOAD ALL " + flNo, url);

#if LoggingOn
							LogMsg("GetDeviceIDByUAFT" + deviceId);
#endif
							csvFltList.Add(temp);
							csvFltList.Add(flNo);
							csvFltList.Add(DateTime.Now.ToString());
							csvFltList.Add("OK");
							createCSVFile(csvFltList);
						}
					}
					if (temp != "No Devices")
					{
						XmlElement elem = loadAllXmlDocument.CreateElement("FLIGHT");
						elem.InnerXml = "<URNO></URNO><TIME></TIME>";
						elem["URNO"].InnerText = flightURNO;
						elem["TIME"].InnerText = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
						loadAllXmlDocument.DocumentElement.AppendChild(elem);
						loadAllXmlDocument.Save(loadXmlFilePath);
					}
				}
#if LoggingOn
				LogMsg("PID: " + temp + "urno: " + flightURNO);
#endif
			}
			csvFltList.Clear();
		}
		private void createCSVFile(ArrayList lstFlts)
		{
			try
			{
				//System.IO.StreamWriter csvFile = new System.IO.StreamWriter(@"F:\Program Files\Ufis\iTrekOK2Load\LoadAll.csv", true);
				//string sep = ",";
				//csvFile.Write(csvFile.NewLine);
				//foreach (string lst in lstFlts)
				//{
				//    csvFile.Write(lst);
				//    csvFile.Write(sep);

				//}
				//csvFile.Close();
				string logToWrite = "";
				foreach (string lst in lstFlts)
				{
					logToWrite += lst + ",";
				}

				iTrekLoadAllLog.WriteEntry(logToWrite);

				lstFlts.Clear();
			}
			catch (Exception ex)
			{
				LogMsg("createCSVFile:Err:" + ex.Message);
			}
		}

		private string GetPapUrl()
		{
			iTXMLPathscl paths = new iTXMLPathscl();
			return paths.GetPapURL();
		}

		private string GetUrlForNetAlert()
		{
			iTXMLPathscl paths = new iTXMLPathscl();
			return paths.GetURLForNetAlert();
		}

		private void LogMsg(string msg)
		{
			try
			{
				iTrekETDSvcLog.WriteEntry(msg);
			}
			catch { }
		}
	}
}