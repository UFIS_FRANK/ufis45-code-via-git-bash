namespace iTrekETDSvc
{
    partial class iTrekETDSvc
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.iTrekETDSvcLog = new System.Diagnostics.EventLog();
            this.iTrekLoadAllLog = new System.Diagnostics.EventLog();
            ((System.ComponentModel.ISupportInitialize)(this.iTrekETDSvcLog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iTrekLoadAllLog)).BeginInit();
            // 
            // iTrekETDSvcLog
            // 
            this.iTrekETDSvcLog.Log = "iTrekETDSvcLog";
            this.iTrekETDSvcLog.Source = "iTrekETDSvcLogSrc";
            // 
            // iTrekLoadAllLog
            // 
            this.iTrekLoadAllLog.Log = "iTrekLoadAllLog";
            this.iTrekLoadAllLog.Source = "iTrekLoadAllLog";
            // 
            // iTrekETDSvc
            // 
            this.ServiceName = "iTrekETDSvc";
            ((System.ComponentModel.ISupportInitialize)(this.iTrekETDSvcLog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iTrekLoadAllLog)).EndInit();

        }

        #endregion

        private System.Diagnostics.EventLog iTrekETDSvcLog;
        private System.Diagnostics.EventLog iTrekLoadAllLog;
    }
}
