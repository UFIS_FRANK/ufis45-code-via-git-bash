using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

using System.Data;
using System.IO;

using UFIS.ITrekLib.DS;
//using iTrekXML;
using System.Collections;
using UFIS.ITrekLib.Util;
using UFIS.ITrekLib.CustomException;
using UFIS.ITrekLib.Config;
using MM.UtilLib;


namespace UFIS.ITrekLib.DB
{
	public class DBDTrip
	{
		private static Object _lockObj = new Object();//to use in synchronisation of single access
		private static Object _lockObjHist = new Object();
		private static DBDTrip _dbTrip = null;
		private static DateTime lastWriteDT = new System.DateTime(1, 1, 1);
		private static DateTime lastTripHistWriteDT = new System.DateTime(1, 1, 1);
		private static DSTrip _dsTrip = null;

		private static readonly DateTime DEFAULT_DT = new System.DateTime(1, 1, 1);
		/// <summary>
		/// Singleton Pattern.
		/// Not allowed to create the object from other class.
		/// To get the object, user "GetInstance()" method.
		/// </summary>
		private DBDTrip() { }

		public void Init()
		{//Initialize all the information, to get the latest data when fetching them again.
			_dsTrip = null;

			lastWriteDT = new System.DateTime(1, 1, 1);
			lastTripHistWriteDT = new System.DateTime(1, 1, 1);
		}

		/// <summary>
		/// Get the Instance of DBTrip. There will be only one reference for all the clients
		/// </summary>
		/// <returns></returns>
		public static DBDTrip GetInstance()
		{
			if (_dbTrip == null)
			{
				_dbTrip = new DBDTrip();
			}
			return _dbTrip;
		}
        /// <summary>
        /// Retrieve Trip for a Flight
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="flightUrNo">Flight Urno</param>
        /// <returns></returns>
		public static DSTrip RetrieveTripForAFlight(IDbCommand cmd, string flightUrNo)
		{
			//DSTrip ds = new DSTrip();
			DSTrip tempDs = new DSTrip();
			if (!LoadData(cmd, ref tempDs, flightUrNo))
			{
				tempDs = new DSTrip();
			}
			return tempDs;
			//foreach (DSTrip.TRIPRow row in tempDs.TRIP.Select("UAFT='" + flightUrNo + "'"))
			//{

			//    ds.TRIP.LoadDataRow(row.ItemArray, true);

			//}

			//return ds;
		}
        /// <summary>
        /// Retrieve Trip for a Flight with Max Count 20.
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="flightUrNo">Flight Urno</param>
        /// <returns></returns>
		public DSTrip RetrieveTripForAFlightWithMaxCnt(IDbCommand cmd, string flightUrNo)
		{
			DSTrip ds = new DSTrip();
			DSTrip tempDs = new DSTrip();
			LoadData(cmd, ref tempDs, flightUrNo);
			int maxCnt = 20;
			foreach (DSTrip.TRIPRow row in tempDs.TRIP.Select("UAFT='" + flightUrNo + "' AND (RCV_DT IS NULL OR RCV_DT='' OR RCV_DT='" + DBNull.Value.ToString() + "')", "SENT_DT ASC"))
			{
				ds.TRIP.LoadDataRow(row.ItemArray, true);
				maxCnt--;
				if (maxCnt <= 0) break;
			}

			return ds;
		}

        /// <summary>
        /// Retrieve Trip for a Flight Received
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="flightUrNo">Flight Urno</param>
        /// <returns></returns>
		public DSTrip RetrieveTripForAFlightReceived(IDbCommand cmd, string flightUrNo)
		{
			DSTrip ds = new DSTrip();
			DSTrip tempDs = new DSTrip();
			LoadData(cmd, ref tempDs, flightUrNo);

			foreach (DSTrip.TRIPRow row in tempDs.TRIP.Select("UAFT='" + flightUrNo + "' AND (RCV_DT IS NOT NULL OR RCV_DT<>'')"))
			{

				ds.TRIP.LoadDataRow(row.ItemArray, true);
			}

			return ds;


		}
        /// <summary>
        /// Retrieve Trip for a Flight Not Received
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="flightUrNo">Flight Urno</param>
        /// <returns></returns>
		public DSTrip RetrieveTripForAFlightNotReceived(IDbCommand cmd, string flightUrNo)
		{
			DSTrip ds = new DSTrip();
			DSTrip tempDs = new DSTrip();
			LoadData(cmd, ref tempDs, flightUrNo);

			foreach (DSTrip.TRIPRow row in tempDs.TRIP.Select("UAFT='" + flightUrNo + "' AND (RCV_DT IS NULL OR RCV_DT='')"))
			{
				ds.TRIP.LoadDataRow(row.ItemArray, true);
			}

			return ds;
		}

        /// <summary>
        /// Retrieve Trip for a Flight
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="flightUrNo">Flight Urno</param>
        /// <param name="uldn"></param>
        /// <returns></returns>
		public DSTrip RetrieveTripForAFlight(IDbCommand cmd, string flightUrNo, string uldn)
		{
			DSTrip ds = new DSTrip();
			DSTrip tempDs = new DSTrip();
			LoadData(cmd, ref tempDs, flightUrNo);

			foreach (DSTrip.TRIPRow row in tempDs.TRIP.Select("UAFT='" + flightUrNo + "'AND ULDN='" + uldn + "'"))
			{
				ds.TRIP.LoadDataRow(row.ItemArray, true);
			}

			return ds;
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="flightUrNo"></param>
        /// <param name="urno"></param>
        /// <returns></returns>
		public static DSTrip RetrieveTripForAFlightWithTripUrno(IDbCommand cmd, string flightUrNo, string urno)
		{
			DSTrip ds = new DSTrip();
			DSTrip tempDs = new DSTrip();
			LoadData(cmd, ref tempDs, flightUrNo);

			foreach (DSTrip.TRIPRow row in tempDs.TRIP.Select("UAFT='" + flightUrNo + "'AND URNO='" + urno + "'"))
			{
				ds.TRIP.LoadDataRow(row.ItemArray, true);
			}

			return ds;
		}

		//private DSTrip dsTrip
		//{
		//    get
		//    {
		//        //if (_dsTrip == null) { LoadDSTrip(); }
		//        // LoadDSTrip();
		//        DSTrip ds = (DSTrip)_dsTrip.Copy();

		//        return ds;
		//    }
		//}


		/// <summary>
		/// Find the record for getting the latest trip number.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="flnu">Urno of the flight</param>
		/// <returns></returns>The Trip Number for the flight
		public int FindRecordforTripNumber(IDbCommand cmd, string flnu, string sendBy, DateTime curDt,out string lTripId)
		{
			int sTripNo = -1;
			try
			{
				UtilDb udb = UtilDb.GetInstance();

				string pfix = UtilDb.GetParaNamePrefix();
				string pUrno = pfix + "URNO";

				//cmd.CommandText = "SELECT COUNT(*) from " + GetDb_Tb_TrpMst() + " WHERE URNO=" + pUrno;
				//cmd.Parameters.Clear();
				//udb.AddParam(cmd, pUrno, flnu);

				//int cnt = Int32.Parse(cmd.ExecuteScalar().ToString());
                 lTripId = "";
				//if (cnt==1)              
				if (UtilDb.IsRecordExist( cmd, DB_Info.DB_T_TRPMST, flnu ))
                {
                    sTripNo = FindLatestSendTripNumberAndLTripId(cmd, flnu, out lTripId);
                   // sTripNo = Int32.Parse(val.ToString());
                    sTripNo = sTripNo + 1;
                    LogTraceMsg(" sTripNo" + sTripNo);
                    UpdateSTripNumberInTrpMst(cmd, flnu, sTripNo);
                }
                else
                {
                    sTripNo = InsertNewRecordForFlightIntoTrpMst(cmd, flnu, sendBy, curDt);
                    LogTraceMsg(" sTripNo else" + sTripNo);
                }
			}
			catch (Exception)
			{
				throw;
			}
			return sTripNo;
		}

        /// <summary>
        /// Find Latest Send Trip Number
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="flnu"></param>
        /// <returns></returns>
		public int FindLatestSendTripNumber(IDbCommand cmd, string flnu)
		{
			return UtilDb.GetDataInInt(cmd, DB_Info.DB_T_TRPMST, flnu, "SLTRPNO");
			//int sTripNo = -1;
			//try
			//{
			//    UtilDb udb = UtilDb.GetInstance();

			//    string pfix = UtilDb.GetParaNamePrefix();
			//    string pTabn = pfix + "URNO";

			//    cmd.CommandText = "SELECT SLTRPNO from " + GetDb_Tb_TrpMst() + " WHERE URNO=" + pTabn;
			//    cmd.Parameters.Clear();
			//    udb.AddParam(cmd, pTabn, flnu);

			//    Object val = cmd.ExecuteScalar();
			//    if (val != null)
			//    {
			//        sTripNo = Int32.Parse(val.ToString());

			//    }

			//}
			//catch (Exception)
			//{
			//    throw;
			//}
			//return sTripNo;
		}
        /// <summary>
        /// Find the latest Send Trip Number and Last Trip ID
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="flnu">Flight Urni</param>
        /// <param name="lTripId">Last Trip ID</param>
        /// <returns>Trip Number</returns>
		public int FindLatestSendTripNumberAndLTripId(IDbCommand cmd, string flnu, out string lTripId)
		{
			//If it is null, create and close the command cmd from this method.
			int sTripNo = -1;
			lTripId = "";
			try
			{
				UtilDb udb = UtilDb.GetInstance();

				string pfix = UtilDb.GetParaNamePrefix();
				string pTabn = pfix + "URNO";

                cmd.CommandText = "SELECT SLTRPNO,SLTRPID from " + GetDb_Tb_TrpMst() + " WHERE URNO=" + pTabn;
				cmd.Parameters.Clear();
				udb.AddParam(cmd, pTabn, flnu);

				using (IDataReader dr = cmd.ExecuteReader())
				{
					while (dr.Read())
					{
						try
						{
							try
							{
								sTripNo = UtilDb.GetDataInt(dr, "SLTRPNO");
							}
							catch (Exception ex)
							{
                                LogTraceMsg("FindLatestSendTripNumberAndLTripId" + ex.Message);                                
							}
                            try
                            {
                                lTripId = UtilDb.GetData(dr, "SLTRPID");
                            }
                            catch (Exception)
                            {
                                lTripId = "";
                            }
							LogTraceMsg("sTripNo:" + sTripNo + ", lTripId" + lTripId);
						}
						catch (Exception ex)
						{

                            LogTraceMsg("Exception" +ex.Message);
						}
					}
				}
			}
			catch (Exception)
			{
				throw;
			}
			return sTripNo;
		}



		public int FindLatestReceiveTripNumber(IDbCommand cmd, string flnu)
		{
			return UtilDb.GetDataInInt(cmd, DB_Info.DB_T_TRPMST, flnu, "RLTRPNO");

			//int rTripNo = -1;
			//try
			//{
			//    UtilDb udb = UtilDb.GetInstance();

			//    string pfix = UtilDb.GetParaNamePrefix();
			//    string pTabn = pfix + "URNO";

			//    cmd.CommandText = "SELECT RLTRPNO from " + GetDb_Tb_TrpMst() + " WHERE URNO=" + pTabn;
			//    cmd.Parameters.Clear();
			//    udb.AddParam(cmd, pTabn, flnu);

			//    Object val = cmd.ExecuteScalar();
			//    if (val != null)
			//    {
			//        rTripNo = Int32.Parse(val.ToString());

			//    }

			//}
			//catch (Exception)
			//{
			//    throw;
			//}

			//return rTripNo;
		}

		public string GetTripIdFromTrpForATripNumber(IDbCommand cmd, string flnu, int trNo, out string trDt, string sendOrRec)
		{
			//string sTripId = "";
			string urno = "";
			try
			{
				UtilDb udb = UtilDb.GetInstance();

				string pfix = UtilDb.GetParaNamePrefix();
				string pTabn = pfix + "FLID";
				string pTrNo = pfix + "TRNO";
				string psendOrRec = pfix + "SORR";

                cmd.CommandText = "SELECT URNO,TRDT from " + GetDb_Tb_Trp() + " WHERE FLID=" + pTabn + " AND TRNO=" + pTrNo + " AND S_OR_R=" + psendOrRec;
				cmd.Parameters.Clear();
				udb.AddParam(cmd, pTabn, flnu);
				udb.AddParam(cmd, pTrNo, trNo);
				udb.AddParam(cmd, psendOrRec, sendOrRec);

				using (IDataReader dr = cmd.ExecuteReader())
				{
					trDt = "";

					while (dr.Read())
					{
						try
						{
							urno = UtilDb.GetData(dr, "URNO");
                            DateTime dt;
                            if(DateTime.TryParse(dr["TRDT"].ToString(), out dt))
                            {
                                if (dt != null)
                                {
                                    trDt = UtilTime.ConvDateTimeToUfisFullDTString(dt);
                                }
                            }
							LogTraceMsg("urno:" + urno + ",TrDt:" + trDt);
						}
						catch (Exception ex)
						{
							LogTraceMsg("GetTripIdFromTrpForATripNumber:Err:" + ex.Message);
						}
					}
				}

			}
			catch (Exception)
			{
				throw;
			}

			return urno;
		}

		public const string SFTRPID = "SFTRPID";
		public const string SLTRPID = "SLTRPID";
		public const string RFTRPID = "RFTPID";
		public const string RLTRPID = "RLTRPID";

        /// <summary>
        /// Find TripId from Trip Master
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="flnu">Flight Urno</param>
        /// <returns>HashTable of field and ID</returns>

		public Hashtable FindMasterTripforFlight(IDbCommand cmd, string flnu)
		{

			Hashtable htValues = new Hashtable();
			//string sTripId = "";
			try
			{
				UtilDb udb = UtilDb.GetInstance();

				string pfix = UtilDb.GetParaNamePrefix();
				string pTabn = pfix + "URNO";

                cmd.CommandText = "SELECT SFTRPID,SLTRPID,RFTRPID,RLTRPID from " + GetDb_Tb_TrpMst() + " WHERE URNO=" + pTabn;

				cmd.Parameters.Clear();
				udb.AddParam(cmd, pTabn, flnu);

				using (IDataReader dr = cmd.ExecuteReader())
				{
					string sFtrpId = "";
					string sLtrpId = "";
					string rFtrpId = "";
					string rLtrpId = "";

					while (dr.Read())
					{
						try
						{
							try
							{
								sFtrpId = UtilDb.GetData(dr, SFTRPID);
                                if (sFtrpId != null || sFtrpId != "")
                                {
                                    htValues.Add(ENT.EnTripTypeSendReceive.SFTRPID, sFtrpId);
                                }
								LogTraceMsg("sFtrpId" + sFtrpId);
							}
							catch (Exception)
							{
							}
							try
							{
								sLtrpId = UtilDb.GetData(dr, SLTRPID);
                                if (sLtrpId != null || sLtrpId != "")
                                {
                                    htValues.Add(ENT.EnTripTypeSendReceive.SLTRPID, sLtrpId);
                                }
								LogTraceMsg("sLtrpId" + sLtrpId);
							}
							catch (Exception)
							{
							}
							try
							{
								rFtrpId = UtilDb.GetData(dr, RFTRPID);
							
                                if (rFtrpId != null || rFtrpId != "")
                                {
                                    htValues.Add(ENT.EnTripTypeSendReceive.RFTRPID, rFtrpId);
                                }
								LogTraceMsg("rFtrpId" + rFtrpId);
							}
							catch (Exception)
							{
							}
							try
							{
								rLtrpId = UtilDb.GetData(dr, RLTRPID);
                                if (rLtrpId != null || rLtrpId != "")
                                {
                                    htValues.Add(ENT.EnTripTypeSendReceive.RLTRPID, rLtrpId);
                                }
								LogTraceMsg("rLtrpId" + rLtrpId);
							}
							catch (Exception)
							{			
							}
						}
						catch (Exception)
						{
						}
					}
				}
			}
			catch (Exception)
			{
				throw;
			}
			return htValues;
		}

		public static string GetSendFirstTripId(IDbCommand cmd, string flightId)
		{
			return UtilDb.GetDataInString(cmd, DB_Info.DB_T_TRPMST, flightId, "SFTRPID");
		}

		public static string GetSendLastTripId(IDbCommand cmd, string flightId)
		{
			return UtilDb.GetDataInString(cmd, DB_Info.DB_T_TRPMST, flightId, "SLTRPID");
		}

		public static string GetReceiveirstTripId(IDbCommand cmd, string flightId)
		{
			return UtilDb.GetDataInString(cmd, DB_Info.DB_T_TRPMST, flightId, "RFTRPID");
		}

		public static string GetReceiveLastTripId(IDbCommand cmd, string flightId)
		{
			return UtilDb.GetDataInString(cmd, DB_Info.DB_T_TRPMST, flightId, "RLTRPID");
		}

		///// <summary>
		///// Find TripId from Trip Master
		///// </summary>
		///// <param name="cmd"></param>
		///// <param name="flnu"></param>
		///// <param name="tripType"></param>
		///// <returns>TripId</returns>
		//public string FindSendTripIdforFlight(IDbCommand cmd, string flnu, string tripType)
		//{
		//    string sTripId = "";
		//    try
		//    {
		//        UtilDb udb = UtilDb.GetInstance();

		//        string pfix = UtilDb.GetParaNamePrefix();
		//        string pTabn = pfix + "URNO";
		//        if (tripType == "F")
		//        {
		//            cmd.CommandText = "SELECT SFTRPID from " + GetDb_Tb_TrpMst() + " WHERE URNO=" + pTabn;
		//        }
		//        else if (tripType == "L")
		//        {
		//            cmd.CommandText = "SELECT SLTRPID from " + GetDb_Tb_TrpMst() + " WHERE URNO=" + pTabn;
		//        }
		//        else
		//        {
		//        }
		//        cmd.Parameters.Clear();
		//        udb.AddParam(cmd, pTabn, flnu);

		//        Object val = cmd.ExecuteScalar();
		//        if (val != null)
		//        {
		//            sTripId = val.ToString();
		//        }
		//    }
		//    catch (Exception)
		//    {
		//        throw;
		//    }
		//    return sTripId;
		//}


		//public string FindRcvTripIdforFlight(IDbCommand cmd, string flnu, string tripType)
		//{
		//    string rTripId = "";
		//    try
		//    {
		//        UtilDb udb = UtilDb.GetInstance();

		//        string pfix = UtilDb.GetParaNamePrefix();
		//        string pTabn = pfix + "URNO";
		//        if (tripType == "F")
		//        {
		//            cmd.CommandText = "SELECT RFTRPID from " + GetDb_Tb_TrpMst() + " WHERE URNO=" + pTabn;
		//        }
		//        else if (tripType == "L")
		//        {
		//            cmd.CommandText = "SELECT RLTRPID from " + GetDb_Tb_TrpMst() + " WHERE URNO=" + pTabn;
		//        }
		//        cmd.Parameters.Clear();
		//        udb.AddParam(cmd, pTabn, flnu);

		//        Object val = cmd.ExecuteScalar();
		//        if (val != null)
		//        {
		//            rTripId = val.ToString();

		//        }

		//    }
		//    catch (Exception)
		//    {
		//        throw;
		//    }
		//    finally
		//    {

		//    }
		//    return rTripId;
		//}

        /// <summary>
        /// FindUnreceived Trip Count
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="flnu">Flight Urno</param>
        /// <returns>Count of Unreceived Trip</returns>
        public static int FindUnRcvTripCountforFlight(IDbCommand cmd, string flnu)
        {
			UtilDb udb = UtilDb.GetInstance();
			cmd.Parameters.Clear();
			return UtilDb.GetRecordCount(cmd, DB_Info.DB_T_TRPDET,
				string.Format(" WHERE FLID={0} AND RTID IS NULL",
				udb.PrepareParam(cmd, "FLID", flnu)));

			//int rowCnt = -1;
			//try
			//{
			//    UtilDb udb = UtilDb.GetInstance();

			//    string pfix = UtilDb.GetParaNamePrefix();
			//    string pFlid = pfix + "FLID";

			//    cmd.CommandText = "SELECT COUNT(*) from " + GetDb_Tb_TrpDet() + " WHERE FLID=" + pFlid + " AND RTID IS NULL";
                
			//    cmd.Parameters.Clear();
			//    udb.AddParam(cmd, pFlid, flnu);

			//    Object val = cmd.ExecuteScalar();
			//    if (val != null)
			//    {
			//        rowCnt = Int32.Parse(val.ToString());
			//    }
			//}
			//catch (Exception)
			//{
			//    throw;
			//}
			//return rowCnt;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="flnu"></param>
        /// <returns></returns>
        public int  FindUldCountForFlight(IDbCommand cmd, string flnu)
        {
			cmd.Parameters.Clear();
			return UtilDb.GetRecordCount(cmd, DB_Info.DB_T_TRPDET, "WHERE FLID=" +
				UtilDb.GetInstance().PrepareParam(cmd, "FLID", flnu));
			//int rowCnt = -1;
			//try
			//{
			//    UtilDb udb = UtilDb.GetInstance();

			//    string pfix = UtilDb.GetParaNamePrefix();
			//    string pFlid = pfix + "FLID";

			//    cmd.CommandText = "SELECT COUNT(*) from " + GetDb_Tb_TrpDet() + " WHERE FLID=" + pFlid ;
                
			//    cmd.Parameters.Clear();
			//    udb.AddParam(cmd, pFlid, flnu);

			//    Object val = cmd.ExecuteScalar();
			//    if (val != null)
			//    {
			//        rowCnt = Int32.Parse(val.ToString());
			//    }
			//}
			//catch (Exception)
			//{
			//    throw;
			//}
			//finally
			//{
			//}
			//return rowCnt;
        }

        /// <summary>
        /// Get the Receive Trip Number from Master
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="flnu">Flight Urno</param>
        /// <param name="sendBy">Received By</param>
        /// <param name="curDt"></param>
        /// <returns>Record Number</returns>
		public int FindRecordforRecTripNumber(IDbCommand cmd, string flnu, string sendBy, DateTime curDt)
		{
			int rTripNo = -1;
			try
			{
				UtilDb udb = UtilDb.GetInstance();

				string pfix = UtilDb.GetParaNamePrefix();
				string pUrno = pfix + "URNO";
				cmd.Parameters.Clear();
				cmd.CommandText = "SELECT RLTRPNO from " + GetDb_Tb_TrpMst() + " WHERE URNO=" + udb.PrepareParam(cmd, "URNO", flnu );

				Object val = cmd.ExecuteScalar();
				if (val != null)
				{
					rTripNo = Int32.Parse(val.ToString());
					rTripNo = rTripNo + 1;
					UpdateRTripNumberInTrpMst(cmd, flnu, rTripNo);
				}
				else
				{
					rTripNo = InsertNewRecordForFlightIntoTrpMstForTripRcv(cmd, flnu, sendBy, curDt);
				}
			}
			catch (Exception)
			{
				throw;
			}
			finally
			{

			}
			return rTripNo;
		}
		/// <summary>
		/// Insert new record into Trip Master if no record found.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="flnu">Urno of the flight</param>
		/// <returns></returns>


		public int InsertNewRecordForFlightIntoTrpMst(IDbCommand cmd, string flnu, string sendBy, DateTime curDt)
		{
			string paramPrefix = UtilDb.GetParaNamePrefix();
			string pUrno = paramPrefix + "URNO";
			string pCdat = paramPrefix + "CDAT";
			string pUdat = paramPrefix + "LSTU";
			string pUsec = paramPrefix + "USEC";
			string pUseu = paramPrefix + "USEU";


			UtilDb udb = UtilDb.GetInstance();
			DateTime dt1 = DateTime.Now;

			bool success = false;
			for (int i = 0; i < 100; i++)
			{
				try
				{

                    cmd.CommandText = String.Format(@"INSERT INTO {0}(URNO,SLTRPNO,CDAT,LSTU,USEC,USEU) VALUES ({1},{2},{3},{4},{5},{6})", GetDb_Tb_TrpMst(), pUrno, 1, pCdat, pUdat, pUsec, pUseu);
					cmd.CommandType = CommandType.Text;
					cmd.Parameters.Clear();
					udb.AddParam(cmd, pUrno, flnu);
					udb.AddParam(cmd, pCdat, curDt);
					udb.AddParam(cmd, pUdat, curDt);
					udb.AddParam(cmd, pUsec, sendBy);
					udb.AddParam(cmd, pUseu, sendBy);


					int cnt = cmd.ExecuteNonQuery();
					LogTraceMsg(" InsertNewRecordForFlightIntoTrpMst cnt" + cnt);
					if (cnt == 1)
					{
						success = true;
						break;
					}
				}
				catch (NoCtrlSettingException)
				{
					throw;
				}
				catch (Exception)
				{
					continue;
				}
			}
			if (!success) throw new ApplicationException("Unable to create new message to send.");
			return 1;
		}
        /// <summary>
        /// Insert new record in Master.
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="flnu"></param>
        /// <param name="sendBy"></param>
        /// <param name="curDt"></param>
        /// <returns></returns>
		public int InsertNewRecordForFlightIntoTrpMstForTripRcv(IDbCommand cmd, string flnu, string sendBy, DateTime curDt)
		{
			string paramPrefix = UtilDb.GetParaNamePrefix();
			string pUrno = paramPrefix + "URNO";
			string pCdat = paramPrefix + "CDAT";
			string pUdat = paramPrefix + "LSTU";
			string pUsec = paramPrefix + "USEC";
			string pUseu = paramPrefix + "USEU";

			UtilDb udb = UtilDb.GetInstance();
			DateTime dt1 = DateTime.Now;

			bool success = false;
			for (int i = 0; i < 100; i++)
			{
				try
				{

                    cmd.CommandText = String.Format(@"INSERT INTO {0}(URNO,RLTRPNO) VALUES ({1},{2},{3},{4},{5},{6})", GetDb_Tb_TrpMst(), pUrno, 1, pCdat, pUdat, pUsec, pUseu);
					cmd.CommandType = CommandType.Text;
					cmd.Parameters.Clear();
					udb.AddParam(cmd, pUrno, flnu);
					udb.AddParam(cmd, pCdat, curDt);
					udb.AddParam(cmd, pUdat, curDt);
					udb.AddParam(cmd, pUsec, sendBy);
					udb.AddParam(cmd, pUseu, sendBy);

					int cnt = cmd.ExecuteNonQuery();
					if (cnt == 1)
					{
						success = true;
						break;
					}
				}
				catch (NoCtrlSettingException)
				{
					throw;
				}
				catch (Exception)
				{
					continue;
				}
			}
			if (!success) throw new ApplicationException("Unable to create new message to send.");
			return 1;
		}

		public int UpdateSTripNumberInTrpMst(IDbCommand cmd, string flnu, int sTripNo)
		{
			string paramPrefix = UtilDb.GetParaNamePrefix();
			string pUrno = paramPrefix + "URNO";
			string pSTrpNo = paramPrefix + "RNO";


			UtilDb udb = UtilDb.GetInstance();
			DateTime dt1 = DateTime.Now;

			bool success = false;
			for (int i = 0; i < 100; i++)
			{
				try
				{

                    cmd.CommandText = String.Format(@"UPDATE {0} SET SLTRPNO = {1} WHERE URNO= {2}", GetDb_Tb_TrpMst(), pSTrpNo, pUrno);
					cmd.CommandType = CommandType.Text;
					cmd.Parameters.Clear();
					udb.AddParam(cmd, pUrno, flnu);
					udb.AddParam(cmd, pSTrpNo, sTripNo);

					int cnt = cmd.ExecuteNonQuery();
					if (cnt == 1)
					{
						success = true;
						break;
					}
				}
				catch (NoCtrlSettingException)
				{
					throw;
				}
				catch (Exception)
				{
					continue;
				}
			}
			if (!success) throw new ApplicationException("Unable to create new message to send.");
			return sTripNo;



		}

        /// <summary>
        /// Update RLTRPNO in Master
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="flnu">Flight Urno</param>
        /// <param name="rTripNo">Trip Number</param>
        /// <returns>Trip Number</returns>
		public int UpdateRTripNumberInTrpMst(IDbCommand cmd, string flnu, int rTripNo)
		{
			string paramPrefix = UtilDb.GetParaNamePrefix();
			string pUrno = paramPrefix + "URNO";
			string pRTrpNo = paramPrefix + "RNO";


			UtilDb udb = UtilDb.GetInstance();
			DateTime dt1 = DateTime.Now;

			bool success = false;
			for (int i = 0; i < 100; i++)
			{
				try
				{

                    cmd.CommandText = String.Format(@"UPDATE {0} SET RLTRPNO = {1} WHERE URNO= {2}", GetDb_Tb_TrpMst(), pRTrpNo, pUrno);
					cmd.CommandType = CommandType.Text;
					cmd.Parameters.Clear();
					udb.AddParam(cmd, pUrno, flnu);
					udb.AddParam(cmd, pRTrpNo, rTripNo);

					int cnt = cmd.ExecuteNonQuery();
					if (cnt == 1)
					{
						success = true;
						break;
					}
				}
				catch (NoCtrlSettingException)
				{
					throw;
				}
				catch (Exception)
				{
					continue;
				}
			}
			if (!success) throw new ApplicationException("Unable to create new message to send.");
			return rTripNo;




		}
        /// <summary>
        /// Update Received ID in Trip Detail
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="rTripId">TripId</param>
        /// <param name="urno">Urno</param>

		public void UpdateTripRecToTrpDet(IDbCommand cmd, string rTripId, string urno)
		{
			string paramPrefix = UtilDb.GetParaNamePrefix();
			string pUrno = paramPrefix + "URNO";
			string pRTrpId = paramPrefix + "RID";


			UtilDb udb = UtilDb.GetInstance();
			DateTime dt1 = DateTime.Now;

			bool success = false;
			for (int i = 0; i < 100; i++)
			{
				try
				{

                    cmd.CommandText = String.Format(@"UPDATE {0} SET RTID = {1} WHERE URNO= {2}", GetDb_Tb_TrpDet(), pRTrpId, pUrno);
					cmd.CommandType = CommandType.Text;
					cmd.Parameters.Clear();
					udb.AddParam(cmd, pUrno, urno);
					udb.AddParam(cmd, pRTrpId, rTripId);

					int cnt = cmd.ExecuteNonQuery();
					if (cnt == 1)
					{
						LogTraceMsg("UpdateTripRecToTrpDet " + urno);
						success = true;
						break;
					}
				}
				catch (NoCtrlSettingException)
				{
					throw;
				}
				catch (Exception)
				{
					continue;
				}
			}
			if (!success) throw new ApplicationException("Unable to create new message to send.");

		}
        /// <summary>
        /// Update SendTripId To Master Trip
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="sTripId">SendTripId from Trp</param>
        /// <param name="urno">Urno of TripMaster</param>
        /// <param name="tripType">TripType'F' or 'L'</param>
        /// <param name="userId">Id of prson sending Trips</param>
        /// <param name="curDt">Current Date</param>
        /// <param name="unSelectedCnt">Any balance container(If 0 and tripType='F'-Only one Trip.</param>
		public void UpdateSendFirstTripIdToTrpMst(IDbCommand cmd, string sTripId, string urno, ref string tripType, string userId, DateTime curDt, int unSelectedCnt)
		{
			string paramPrefix = UtilDb.GetParaNamePrefix();
			string pUrno = paramPrefix + "URNO";
			string pSTrpId = paramPrefix + "RID";
			string pSendBy = paramPrefix + "USEU";
			string pUdat = paramPrefix + "LSTU";


			UtilDb udb = UtilDb.GetInstance();
			DateTime dt1 = DateTime.Now;

			bool success = false;
			for (int i = 0; i < 100; i++)
			{
				try
				{
					if (tripType == "F")
					{
						if (unSelectedCnt == 0)
						{
							tripType = "FL";
                            cmd.CommandText = String.Format(@"UPDATE {0} SET SFTRPID = {1},SLTRPID = {1},USEU={2},LSTU={3} WHERE URNO= {4}", GetDb_Tb_TrpMst(), pSTrpId, pSendBy, pUdat, pUrno);

						}
						else
						{
                            cmd.CommandText = String.Format(@"UPDATE {0} SET SFTRPID = {1},USEU={2},LSTU={3} WHERE URNO= {4}", GetDb_Tb_TrpMst(), pSTrpId, pSendBy, pUdat, pUrno);
						}
					}
					else if (tripType == "L")
					{
                        cmd.CommandText = String.Format(@"UPDATE {0} SET SLTRPID = {1},USEU={2},LSTU={3} WHERE URNO= {4}", GetDb_Tb_TrpMst(), pSTrpId, pSendBy, pUdat, pUrno);
					}
					cmd.CommandType = CommandType.Text;
					cmd.Parameters.Clear();
					udb.AddParam(cmd, pUrno, urno);
					udb.AddParam(cmd, pSTrpId, sTripId);
					udb.AddParam(cmd, pSendBy, userId);
					udb.AddParam(cmd, pUdat, curDt);

					int cnt = cmd.ExecuteNonQuery();
					LogTraceMsg("cnt UpdateSendFirstTripIdToTrpMst" + cnt);
					if (cnt == 1)
					{
						success = true;
						break;
					}
				}
				catch (NoCtrlSettingException)
				{
					throw;
				}
				catch (Exception)
				{
					continue;
				}
			}
			if (!success) throw new ApplicationException("Unable to create new message to send.");

		}

		public void UpdateRecTripIdToTrpMst(IDbCommand cmd, string rTripId, string urno, string tripType, string userId, DateTime curDt)
		{
			if (string.IsNullOrEmpty(tripType)) throw new ApplicationException("No Trip Type.");
			if (!("F,L,FL".Contains(tripType))) throw new ApplicationException("Invalid Trip Type:" + tripType);

			string paramPrefix = UtilDb.GetParaNamePrefix();
			string pUrno = paramPrefix + "URNO";
			string pRTrpId = paramPrefix + "RID";
			string pSendBy = paramPrefix + "USEU";
			string pUdat = paramPrefix + "LSTU";

			UtilDb udb = UtilDb.GetInstance();
			DateTime dt1 = DateTime.Now;
			LogTraceMsg("UpdateRecTripIdToTrpMst,Urno:" + urno + ", rTripId:" + rTripId + ", tripType:" + tripType);
			bool success = false;
			for (int i = 0; i < 50; i++)
			{
				try
				{
					if (tripType == "F")
					{
						cmd.CommandText = String.Format(@"UPDATE {0} SET RFTRPID = {1},USEU={2},LSTU={3} WHERE URNO= {4}", 
							GetDb_Tb_TrpMst(), pRTrpId, pSendBy, pUdat, pUrno);
					}
					else if (tripType == "L")
					{
						cmd.CommandText = String.Format(@"UPDATE {0} SET RLTRPID = {1},USEU={2},LSTU={3} WHERE URNO= {4}", 
							GetDb_Tb_TrpMst(), pRTrpId, pSendBy, pUdat, pUrno);
					}
					else if (tripType == "FL")
					{
						cmd.CommandText = String.Format(@"UPDATE {0} SET RFTRPID = {1},RLTRPID = {1},USEU={2},LSTU={3} WHERE URNO= {4}", 
							GetDb_Tb_TrpMst(), pRTrpId, pSendBy, pUdat, pUrno);
					}

					cmd.CommandType = CommandType.Text;
					cmd.Parameters.Clear();
					udb.AddParam(cmd, pUrno, urno);
					udb.AddParam(cmd, pRTrpId, rTripId);
					udb.AddParam(cmd, pSendBy, userId);
					udb.AddParam(cmd, pUdat, curDt);
					LogTraceMsg("UpdateRecTripIdToTrpMst:cmd:" + cmd.CommandText);
					int cnt = cmd.ExecuteNonQuery();
					LogTraceMsg("UpdateRecTripIdToTrpMst:Updcnt:" + cnt);
					if (cnt == 1)
					{
						success = true;
						break;
					}
				}
				catch (NoCtrlSettingException)
				{
					throw;
				}
				catch (Exception ex)
				{
					LogTraceMsg("UpdateRecTripIdToTrpMst:Err:" + ex.Message);
					continue;
				}
			}
			if (!success) throw new ApplicationException("Unable to update.");

		}


		/// <summary>
		/// Inserting a new Trip into the Trp
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="sendBy">The sender's ID</param>
		/// <param name="sendDt">The send Date as entered by user</param>
		/// <param name="strNo">send trip number as got from trip master</param>
		/// <param name="sInd">Indiacator 'S' Send ,'R' Receive</param>
		/// <returns></returns>Urno for this record for updating Master and Detail
		public string InsertNewRecordForFlightIntoTrp(IDbCommand cmd, string flnu, string sendBy, string sendDt, int strNo, string sInd, DateTime curDt)
		{
			string paramPrefix = UtilDb.GetParaNamePrefix();
			string pInd = paramPrefix + "IND";
			string pTrNo = paramPrefix + "TRNO";
			string pTrDt = paramPrefix + "TRDT";
			string pTrBy = paramPrefix + "TRBY";
			string pUrno = paramPrefix + "URNO";
			string pFlnu = paramPrefix + "FLNU";
			string pCdat = paramPrefix + "CDAT";
			string pUdat = paramPrefix + "LSTU";
			string pUsec = paramPrefix + "USEC";
			string pUseu = paramPrefix + "USEU";
			string urno = "";
			UtilDb udb = UtilDb.GetInstance();
			DateTime dt1 = DateTime.Now;

			bool success = false;
			for (int i = 0; i < 100; i++)
			{
				try
				{
					urno = UtilDb.GenUrid(cmd, ProcIds.TRP, false, dt1);
                    cmd.CommandText = String.Format(@"INSERT INTO {0}(URNO,FLID,S_OR_R,TRNO,TRDT,TRBY,CDAT,LSTU,USEC,USEU) VALUES ({1},{2},{3},{4},{5},{6},{7},{8},{9},{10})", GetDb_Tb_Trp(), pUrno, pFlnu, pInd, pTrNo, pTrDt, pTrBy, pCdat, pUdat, pUsec, pUseu);
					cmd.CommandType = CommandType.Text;
					cmd.Parameters.Clear();
					udb.AddParam(cmd, pUrno, urno);
					udb.AddParam(cmd, pInd, sInd);
					udb.AddParam(cmd, pTrNo, strNo);
					udb.AddParam(cmd, pTrDt, UtilTime.ConvUfisTimeStringToDateTime(sendDt));
					udb.AddParam(cmd, pTrBy, sendBy);
					udb.AddParam(cmd, pFlnu, flnu);
					udb.AddParam(cmd, pCdat, curDt);
					udb.AddParam(cmd, pUdat, curDt);
					udb.AddParam(cmd, pUsec, sendBy);
					udb.AddParam(cmd, pUseu, sendBy);


					int cnt = cmd.ExecuteNonQuery();
					LogTraceMsg(" InsertNewRecordForFlightIntoTrp cnt" + cnt);
					if (cnt == 1)
					{
						success = true;
						break;
					}
				}
				catch (NoCtrlSettingException)
				{
					throw;
				}
				catch (Exception)
				{
					continue;
				}
			}
			if (!success) throw new ApplicationException("Unable to create new message to send.");
			return urno;
		}
		/// <summary>
		/// Insert new record into the Trip Detail 
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="flId">Flight Urno</param>
		/// <param name="uldn">UldNumber</param>
		/// <param name="des">Destination</param>
		/// <param name="rmk">Remark</param>
		/// <param name="isCpm">Indicate whether source is from CPM</param>
		/// <param name="stripId">Trip ID from Trp</param>
		/// <returns></returns>
		public string InsertNewRecordForFlightIntoTrpDet(IDbCommand cmd, string flId, string uldn, string des, string rmk, string isCpm, string stripId, string sendBy, DateTime curDt)
		{
			string paramPrefix = UtilDb.GetParaNamePrefix();
			string pUldn = paramPrefix + "ULDN";
			string pFlid = paramPrefix + "FLID";
			string pDes = paramPrefix + "DES";
			string pRmk = paramPrefix + "RMK";
			string pIcpm = paramPrefix + "ICPM";
			string pStrId = paramPrefix + "STRID";
			string pUrno = paramPrefix + "URNO";
			string pCdat = paramPrefix + "CDAT";
			string pUdat = paramPrefix + "LSTU";
			string pUsec = paramPrefix + "USEC";
			string pUseu = paramPrefix + "USEU";
			string urno = "";
			UtilDb udb = UtilDb.GetInstance();
			DateTime dt1 = DateTime.Now;

			bool success = false;
			for (int i = 0; i < 100; i++)
			{
				try
				{
					urno = UtilDb.GenUrid(cmd, ProcIds.TRPDET, false, dt1);
                    cmd.CommandText = String.Format(@"INSERT INTO {0}(URNO,FLID,ULDN,DEST,STID,SRMK,ICPM,CDAT,LSTU,USEC,USEU) VALUES ({1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11})", GetDb_Tb_TrpDet(), pUrno, pFlid, pUldn, pDes, pStrId, pRmk, pIcpm, pCdat, pUdat, pUsec, pUseu);
					cmd.CommandType = CommandType.Text;
					cmd.Parameters.Clear();
					udb.AddParam(cmd, pUrno, urno);
					udb.AddParam(cmd, pFlid, flId);
					udb.AddParam(cmd, pDes, des);
					udb.AddParam(cmd, pRmk, rmk);
					udb.AddParam(cmd, pIcpm, isCpm);
					udb.AddParam(cmd, pStrId, stripId);
					udb.AddParam(cmd, pUldn, uldn);
					udb.AddParam(cmd, pCdat, curDt);
					udb.AddParam(cmd, pUdat, curDt);
					udb.AddParam(cmd, pUsec, sendBy);
					udb.AddParam(cmd, pUseu, sendBy);

					int cnt = cmd.ExecuteNonQuery();
					LogTraceMsg("cnt InsertNewRecordForFlightIntoTrpDet" + cnt);
					if (cnt == 1)
					{
						success = true;
						break;
					}
				}
				catch (NoCtrlSettingException)
				{
					throw;
				}
				catch (Exception)
				{
					continue;
				}
			}
			if (!success) throw new ApplicationException("Unable to create new message to send.");
			return urno;
		}

		/// <summary>
		/// Insert New Trips Info for a flight
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="xmlDoc"></param>
		public bool InsertTripsInfo(IDbCommand cmd, XmlDocument xmlDoc, out string tripType, DateTime curDt, out string tripId, int unSelectedCnt,out int totalUldCount)
		{
			bool isOk = false;
			tripType = "";
			tripId = "";
            totalUldCount = -1;
			lock (_lockObj)
			{
				try
				{


					DataSet dsTripMst = new DataSet();
					XmlNodeReader reader = new XmlNodeReader(xmlDoc);
					dsTripMst.ReadXml(reader);
					string flightUrno = "";
					string sendBy = "";
					string sendDt = "";
					int strNo = -1;
					string stty = "";

					string srInd = "";
					string uldNo = "";
					string des = "";
					string rmk = "";
					string isCpm = "";

					DataRow newRow = dsTripMst.Tables["TRIP"].Rows[0];
					flightUrno = newRow["UAFT"].ToString();
					sendBy = newRow["SRBY"].ToString();
					sendDt = newRow["SRDT"].ToString();
					srInd = newRow["SR"].ToString();
					stty = newRow["TRPTYPE"].ToString();
                    string lTripId = "";
                    /*Find the TripNumber and LtripId for inserting new record.If LTRIPId is not empty 
                     new trip will be last trip.*/
					int trpNo = FindRecordforTripNumber(cmd, flightUrno, sendBy, curDt,out lTripId);
					strNo = trpNo;
					DataSet dsNewTrip = new DataSet();


                    /* Insert new record into Trp Table*/
					tripId = InsertNewRecordForFlightIntoTrp(cmd, flightUrno, sendBy, sendDt, strNo, srInd, curDt);
					LogTraceMsg("tripId" + tripId);
                    LogTraceMsg("lTripId" + lTripId);
					XmlNodeList uldList = xmlDoc.SelectSingleNode("//TRIP/CONT").ChildNodes;
					foreach (XmlNode uldNode in uldList)
					{
						uldNo = uldNode.SelectSingleNode(".//ULDN").InnerText;
						des = uldNode.SelectSingleNode(".//DES").InnerText;
						rmk = uldNode.SelectSingleNode(".//RMK").InnerText;
						isCpm = uldNode.SelectSingleNode(".//ISCPM").InnerText;
                        /* Insert new record into TripDet*/
						InsertNewRecordForFlightIntoTrpDet(cmd, flightUrno, uldNo, des, rmk, isCpm, tripId, sendBy, curDt);

					}

                    totalUldCount = FindUldCountForFlight(cmd, flightUrno);
                    if (lTripId != null && lTripId!="")
                    {
                        LogTraceMsg("lTripId != ");
                        stty = "L";
                    }
                    LogTraceMsg("trpNo" + trpNo);
                    LogTraceMsg("stty" + stty);
					if (trpNo == 1)
					{
						tripType = "F";
                        /*Update SendTrip ID to Master*/
						UpdateSendFirstTripIdToTrpMst(cmd, tripId, flightUrno, ref tripType, sendBy, curDt, unSelectedCnt);
					}
					else if (stty == "L")
					{
						tripType = stty;
                        /*Update SendTrip ID to Master*/
						UpdateSendFirstTripIdToTrpMst(cmd, tripId, flightUrno, ref tripType, sendBy, curDt, unSelectedCnt);

					}

					isOk = true;

				}
				catch (Exception ex)
				{
					LogTraceMsg("UpdateTripsInfoFromBackendToTripXmlFile:Err:" + ex.Message);
				}
				finally
				{
					LogTraceMsg("UpdateTripsInfoFromBackendToTripXmlFile:Err:" + isOk);
				}
				return isOk;

			}
		}

		/// <summary>
		/// Is Receiving the Last Trip? True - Yes, it is the last trip.
		/// NOTE: Call this only after updating the receiving the ulds for the trip.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="flightId"></param>
		/// <returns></returns>
		public static bool IsRecvLastTrip(IDbCommand cmd, string flightId)
		{
			return ((GetSendLastTripId(cmd, flightId) != "") && //Has Send Last Trip
						(FindUnRcvTripCountforFlight(cmd, flightId) == 0)); //No more uld container to receive
		}

		/// <summary>
		/// Update trip received
		/// </summary>
		/// <param name="cpmList">Urno List of TripDet to be updated</param>
		/// <param name="datetime">Trip date time</param>
		/// <param name="sentBy">Trip Receiver</param>
		/// <param name="stTripType">F,N,L,FL Trip</param>
		/// <param name="flnu">Urno of flight</param>
		/// <returns></returns>
        public bool UpdateTripsReceived(IDbCommand cmd, List<string> cpmList, string datetime, string sentBy, string stTripType, string flnu, out string tripType, DateTime curDt)
		{
			bool isOk = false;
			tripType = "";
			lock (_lockObj)
			{
				try
				{
                    /* Get Trip Number*/
					int trpNo = FindRecordforRecTripNumber(cmd, flnu, sentBy, curDt);

					LogTraceMsg("UpdateTripsReceived trpNo" + trpNo);
					string srInd = "R";
					string tripId = InsertNewRecordForFlightIntoTrp(cmd, flnu, sentBy, datetime, trpNo, srInd, curDt);
					// int uldCnt = uldList.Count;
					foreach (string urno in cpmList)
					{
						UpdateTripRecToTrpDet(cmd, tripId, urno);
					}

					if (trpNo == 1)
					{
						tripType = "F";//Receiving the first trip
					}
					else
					{
						if ((stTripType == "L") || (stTripType == "FL"))
						{
							tripType = "L";//Receiving the last trip
						}
					}

					if (IsRecvLastTrip(cmd,flnu))
                    {
						if (trpNo == 1) tripType = "FL";//Receive the First Trip and it is also the Last Trip
						else tripType = "L";
                    }

					//if (string.IsNullOrEmpty(tripType))
					//{
					//    //UpdateRecTripIdToTrpMst(cmd, tripId, flnu, tripType, sentBy, curDt);
					//}

                    /*To be done when automation of Trips is implemented===>Alphy*/
                 /*   else
                        if (needToUpdate)
                        {
                            string sendId = "";
                            sendId = FindSendTripIdforFlight(cmd, flnu, "L");
                            if (sendId != "")
                            {
                                tripType = "L";
                                UpdateRecFirstTripIdToTrpMst(cmd, tripId, flnu, tripType, sentBy, curDt);
                            }
                        }
                  * */

					isOk = true;
				}
				catch (Exception ex)
				{
					LogTraceMsg("UpdateTripsInfoFromBackendToTripXmlFile:Err:" + ex.Message);
				}
				return isOk;
			}
		}

		//public bool UpdateTripTimingForSend(IDbCommand cmd,string flnu,string dateTime,string tripType)
		//{ 
		///*Get FirstTrip Id from Trp and update the date in TrpMst*/
		//    bool isOk = false;
		//    lock (_lockObj)
		//    {
		//        try
		//        {



		//            string sFtrpNo = FindSendTripIdforFlight(cmd, flnu, tripType);



		//          UpdateTripTimingToTrp(cmd, sFtrpNo, dateTime);                    


		//            isOk = true;
		//        }
		//        catch (Exception ex)
		//        {
		//            LogTraceMsg("UpdateTripsInfoFromBackendToTripXmlFile:Err:" + ex.Message);
		//        }
		//        finally
		//        {

		//        }
		//        return isOk;

		//    }



		//}
        /// <summary>
        /// Update Trip Timing in Trip
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="sFtrpNo">Urno of Trip Table</param>
        /// <param name="datetime">New Time</param>
        /// <param name="userId">User Updated</param>
        /// <param name="curDt"></param>

		public void UpdateTripTimingToTrp(IDbCommand cmd, string sFtrpNo, string datetime, string userId, DateTime curDt)
		{
			string paramPrefix = UtilDb.GetParaNamePrefix();
			string pUrno = paramPrefix + "URNO";
			string pSDtTime = paramPrefix + "DT";
			string pUseu = paramPrefix + "USEU";
			string pUdat = paramPrefix + "LSTU";


			UtilDb udb = UtilDb.GetInstance();
			DateTime dt1 = DateTime.Now;

			bool success = false;
			for (int i = 0; i < 100; i++)
			{
				try
				{

                    cmd.CommandText = String.Format(@"UPDATE {0} SET TRDT = {1},USEU={2},LSTU={3} WHERE URNO= {4}", GetDb_Tb_Trp(), pSDtTime, pUseu, pUdat, pUrno);
					cmd.CommandType = CommandType.Text;
					cmd.Parameters.Clear();
					udb.AddParam(cmd, pUrno, sFtrpNo);
					udb.AddParam(cmd, pSDtTime, UtilTime.ConvUfisTimeStringToDateTime(datetime));
					udb.AddParam(cmd, pUseu, userId);
					udb.AddParam(cmd, pUdat, curDt);
					LogTraceMsg("  cmd.CommandText" + cmd.CommandText);
					int cnt = cmd.ExecuteNonQuery();
					if (cnt == 1)
					{
						success = true;
						LogTraceMsg("success" + success);
						break;
					}
				}
				catch (NoCtrlSettingException)
				{
					throw;
				}
				catch (Exception)
				{
					continue;
				}
			}
			if (!success) throw new ApplicationException("Unable to create new message to send.");

		}

        /// <summary>
        /// Update Trip Timing
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="flnu">Flight Urno</param>
        /// <param name="htTrip">TripInfo with Time</param>
        /// <param name="userId">User Updated</param>
        /// <param name="curDt"></param>
        /// <returns></returns>
		public bool UpdateTripTiming(IDbCommand cmd, string flnu, Dictionary<ENT.EnTripTypeSendReceive, string> htTrip, string userId, DateTime curDt)
		{
			/*Get FirstTrip Id from Trp and update the date in TrpMst*/
			bool isOk = false;
			lock (_lockObj)
			{
				try
				{
					Hashtable htTripMst = FindMasterTripforFlight(cmd, flnu);
					LogTraceMsg("htTripMst.Count" + htTripMst.Count);
					foreach (ENT.EnTripTypeSendReceive trip in htTrip.Keys)
					{
						try
						{
							//string rtrpNo = htTripMst[trip].ToString();	
							string rtrpNo = "";
							if (UtilMisc.GetStringFromHash(htTripMst, trip, out rtrpNo))
							{
								LogTraceMsg("rtrpNo" + rtrpNo);
								if (string.IsNullOrEmpty(rtrpNo)) continue;
							}
							else continue;

							string dateTime = htTrip[trip].ToString();
							LogTraceMsg("dateTime" + dateTime);

							UpdateTripTimingToTrp(cmd, rtrpNo, dateTime, userId, curDt);
						}
						catch (Exception ex)
						{
							LogMsg("UpdateTripTiming:Err:" + flnu + "," + trip.ToString() + "," + ex.Message);
							continue;
						}
					}

					isOk = true;
				}
				catch (Exception ex)
				{
					LogTraceMsg("UpdateTripsInfoFromBackendToTripXmlFile:Err:" + ex.Message);
				}
				finally
				{

				}
				return isOk;

			}
		}

        /// <summary>
        /// Update Send Last Trip Id
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="flnu">Flight Urno</param>
        /// <param name="userId">User Updated</param>
        /// <param name="curDt"></param>
        /// <param name="tripType">Trip Type</param>
        /// <param name="trDt"></param>
        /// <returns></returns>

		public bool UpdateSendLastTripIdForTrpMst(IDbCommand cmd, string flnu, string userId, DateTime curDt, out string tripType, out string trDt)
		{
			/*Get FirstTrip Id from Trp and update the date in TrpMst*/
			bool isOk = false;
			tripType = "L";
			trDt = "";
			lock (_lockObj)
			{
				try
				{
					string lTripId = "";


					int sTrpNo = FindLatestSendTripNumberAndLTripId(cmd, flnu, out lTripId);
                    if (lTripId == null ||lTripId == ""  )
					{
                        LogTraceMsg("lTripId == ");
						if (sTrpNo > 0)
						{
                            LogTraceMsg("sTrpNo > 0");

							string sTrpId = GetTripIdFromTrpForATripNumber(cmd, flnu, sTrpNo, out trDt, "S");
                            LogTraceMsg("sTrpId" + sTrpId);
                            LogTraceMsg("trDt" + trDt);
							if (sTrpId != "")
							{
								UpdateSendFirstTripIdToTrpMst(cmd, sTrpId, flnu, ref tripType, userId, curDt, -1);
							}
						}
					} 

                    /* Update Receive Trip if all send trips are acknowledged and update receive Last Trip time*/

					isOk = true;
				}
				catch (Exception ex)
				{
					LogTraceMsg("UpdateTripsInfoFromBackendToTripXmlFile:Err:" + ex.Message);
				}
				finally
				{

				}
				return isOk;

			}




		}
        /// <summary>
        /// Update Receive Last Trip ID in Trip Master
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="flnu">Flight Urno</param>
        /// <param name="userId">user Id</param>
        /// <param name="curDt"></param>
        /// <param name="tripType">'F','L'</param>
        /// <param name="trDt">TripDate</param>
        /// <returns></returns>

		public bool UpdateReceiveLastTripIdForTrpMst(IDbCommand cmd, string flnu, string userId, DateTime curDt, out string tripType, out string trDt)
		{
			/*Get FirstTrip Id from Trp and update the date in TrpMst*/
			bool isOk = false;
			tripType = "L";
			trDt = "";
			lock (_lockObj)
			{
				try
				{
					int rTrpNo = FindLatestReceiveTripNumber(cmd, flnu);

					string rTrpId = GetTripIdFromTrpForATripNumber(cmd, flnu, rTrpNo, out trDt, "R");
					if (string.IsNullOrEmpty(rTrpId))
					{
						LogTraceMsg("UpdateReceiveLastTripIdForTrpMst:NoRTrpId:" + flnu + "," + rTrpNo);
					}
					else
					{
						UpdateRecTripIdToTrpMst(cmd, rTrpId, flnu, tripType, userId, curDt);
					}


					isOk = true;
				}
				catch (Exception ex)
				{
					LogTraceMsg("UpdateReceiveLastTripIdForTrpMst:Err:" + ex.Message);
				}
				finally
				{
				}
				return isOk;

			}
		}


		private void LogMsg(string msg)
		{
			UFIS.ITrekLib.Util.UtilLog.LogToGenericEventLog("DBTrip", msg);
		}

		private static void LogTraceMsg(string msg)
		{

            try
            {
                MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("DBDTrip", msg);
            }
            catch (Exception)
            {
                UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DBDTrip", msg);
            }
			
		}




		private void LockTripForWriting()
		{
			try
			{
				UtilLock.GetInstance().LockTrip();
			}
			catch (Exception ex)
			{
				LogMsg("LockTripForWriting:Err:" + ex.Message);
				if (ex.InnerException != null)
				{
					LogMsg("LockTripForWriting:err:Inner:" + ex.InnerException.Message);
				}
			}
		}

		private void ReleaseTripAfterWrite()
		{
			try
			{
				UtilLock.GetInstance().ReleaseTrip();
			}
			catch (Exception ex)
			{
				LogMsg("ReleaseTripAfterWrite:Err:" + ex.Message);
				if (ex.InnerException != null)
				{
					LogMsg("ReleaseTripAfterWrite:err:Inner:" + ex.InnerException.Message);
				}
			}
		}

		/// <summary>
		/// Selection Command Text to fill the DSCpm
		/// </summary>
		private const string _cmdSelTrpMst = @"SELECT * FROM ";
		private const string _cmdSelTrp = @"SELECT * FROM ";
		private const string _cmdSelTrpDet = @"SELECT * FROM ";
		private const string _cmdSelTrpDetView = @"SELECT URNO,UAFT,ULDN,DES,STRPNO,SENT_DT,SENT_BY,SENT_RMK,RTRPNO,RCV_DT,RCV_BY,RCV_RMK,ISCPM,CLASS,STTY,RTTY FROM ";
		/// <summary>
		/// Load Trip Master Data for given flightId. It will return True when loading success.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="ds"></param>
		/// <param name="flightId">Flight Id to fetch the data</param>
		/// <returns></returns>
		public bool LoadTrpMstDataSet(IDbCommand cmd, ref DataSet ds, string flnu)
		{
			bool loadSuccess = false;
			UtilDb db = UtilDb.GetInstance();
			bool isCmdNull = false;
			try
			{
				if (cmd == null)
				{
					isCmdNull = true;
					cmd = db.GetCommand(false);
				}
				DateTime dt1 = DateTime.Now;

				if (ds == null) ds = new DataSet();

				string paramNamePrefix = UtilDb.GetParaNamePrefix();
				string pnFlnu = paramNamePrefix + "FLID";

				cmd.CommandText = _cmdSelTrpMst +GetDb_Tb_TrpMst()+ " WHERE URNO= '" + flnu + "'";
				cmd.CommandType = CommandType.Text;
				cmd.Parameters.Clear();

				// db.AddParam(cmd, pnFlnu, flnu);

				db.FillDataSet(cmd, ds);

				if ((ds != null) && (ds.Tables["TRPMST"].Rows.Count > 0))
				{
					loadSuccess = true;
				}
				else
				{
					LogMsg("No Trip Data for " + flnu);
				}
			}
			catch (Exception)
			{
				throw;
			}
			finally
			{
				if (isCmdNull)
				{
					db.CloseCommand(cmd, false);
					cmd = null;
				}
			}

			return loadSuccess;
		}

		/// <summary>
		/// Load Trip Data for given flightId. It will return True when loading success.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="ds"></param>
		/// <param name="flightId">Flight Id to fetch the data</param>
		/// <returns></returns>
		public bool LoadTrpDataSet(IDbCommand cmd, ref DataSet ds, string flnu)
		{
			bool loadSuccess = false;
			UtilDb db = UtilDb.GetInstance();
			bool isCmdNull = false;
			try
			{
				if (cmd == null)
				{
					isCmdNull = true;
					cmd = db.GetCommand(false);
				}
				DateTime dt1 = DateTime.Now;

				if (ds == null) ds = new DataSet();

				string paramNamePrefix = UtilDb.GetParaNamePrefix();
				string pnFlnu = paramNamePrefix + "FLID";

				cmd.CommandText = _cmdSelTrp +GetDb_Tb_Trp()+ " WHERE FLNU= '" + flnu + "'";
				cmd.CommandType = CommandType.Text;
				cmd.Parameters.Clear();

				// db.AddParam(cmd, pnFlnu, flnu);

				db.FillDataSet(cmd, ds);

				if ((ds != null) && (ds.Tables["CPMDET"].Rows.Count > 0))
				{
					loadSuccess = true;
				}
				else
				{
					LogMsg("No Cpm Data for " + flnu);
				}
			}
			catch (Exception)
			{
				throw;
			}
			finally
			{
				if (isCmdNull)
				{
					db.CloseCommand(cmd, false);
					cmd = null;
				}
			}

			return loadSuccess;
		}
		/// <summary>
		/// Load TripDet Data for given flightId. It will return True when loading success.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="ds"></param>
		/// <param name="flightId">Flight Id to fetch the data</param>
		/// <returns></returns>
		public bool LoadTrpDetDataSet(IDbCommand cmd, ref DataSet ds, string flnu)
		{
			bool loadSuccess = false;
			UtilDb db = UtilDb.GetInstance();
			bool isCmdNull = false;
			try
			{
				if (cmd == null)
				{
					isCmdNull = true;
					cmd = db.GetCommand(false);
				}
				DateTime dt1 = DateTime.Now;

				if (ds == null) ds = new DataSet();

				string paramNamePrefix = UtilDb.GetParaNamePrefix();
				string pnFlnu = paramNamePrefix + "FLID";

				cmd.CommandText = _cmdSelTrpDet +GetDb_Tb_TrpDet()+ " WHERE FLID= '" + flnu + "'";
				cmd.CommandType = CommandType.Text;
				cmd.Parameters.Clear();

				// db.AddParam(cmd, pnFlnu, flnu);

				db.FillDataSet(cmd, ds);

				if ((ds != null) && (ds.Tables["TRPDET"].Rows.Count > 0))
				{
					loadSuccess = true;
				}
				else
				{
					LogMsg("No TripDet Data for " + flnu);
				}
			}
			catch (Exception)
			{
				throw;
			}
			finally
			{
				if (isCmdNull)
				{
					db.CloseCommand(cmd, false);
					cmd = null;
				}
			}

			return loadSuccess;
		}
		/// <summary>
		/// Load Cpm Information for 3 days (From back 2days to Today). Return True when successfully loaded.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="ds"></param>
		/// <returns>True-Load Successful.</returns>
		private bool LoadData(IDbCommand cmd, ref DSTrip ds)
		{
			bool loadSuccess = false;
			UtilDb db = UtilDb.GetInstance();
			bool isCmdNull = false;
			try
			{
				if (cmd == null)
				{
					isCmdNull = true;
					cmd = db.GetCommand(false);
				}
				DateTime dt1 = DateTime.Now;

				if (ds == null) ds = new DSTrip();

				string paramNamePrefix = UtilDb.GetParaNamePrefix();
				string pnFrDate = paramNamePrefix + "FR_FL_DATE";
				string pnToDate = paramNamePrefix + "TO_FL_DATE";

                cmd.CommandText = _cmdSelTrp + GetDb_Tb_Trp() + " WHERE BFLT BETWEEN " + pnFrDate + " AND " + pnToDate;
				cmd.CommandType = CommandType.Text;
				cmd.Parameters.Clear();

				DateTime now = DateTime.Now.Date;
				db.AddParam(cmd, pnFrDate, now.AddDays(-2));
				db.AddParam(cmd, pnToDate, now.AddDays(+1).AddMilliseconds(1));

				db.FillDataTable(cmd, ds.TRIP);

				DateTime dt2 = DateTime.Now;
				// LogMsg("Load Job Data Time:" + UtilTime.ElapsedTime(dt1, dt2));

				if ((ds != null) && (ds.TRIP.Rows.Count > 0))
				{
					loadSuccess = true;
				}
				else
				{
					LogMsg("No Trip Data.");
				}
			}
			catch (Exception)
			{
				throw;
			}
			finally
			{
				if (isCmdNull)
				{
					db.CloseCommand(cmd, false);
					cmd = null;
				}
			}
			return loadSuccess;
		}

		/// <summary>
		/// Load Job Data for given flightId. It will return True when loading success.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="ds"></param>
		/// <param name="flightId">Flight Id to fetch the data</param>
		/// <returns></returns>
		public static bool LoadData(IDbCommand cmd, ref DSTrip ds, string flnu)
		{
			bool loadSuccess = false;
			UtilDb db = UtilDb.GetInstance();
			bool isCmdNull = false;
			try
			{
				if (cmd == null)
				{
					isCmdNull = true;
					cmd = db.GetCommand(false);
				}
				DateTime dt1 = DateTime.Now;

				if (ds == null) ds = new DSTrip();

				string paramNamePrefix = UtilDb.GetParaNamePrefix();
				string pnFlnu = paramNamePrefix + "FLID";

				cmd.CommandText = _cmdSelTrpDetView +GetDb_Tb_TrpDetView()+ " WHERE UAFT = " + pnFlnu;
				cmd.CommandType = CommandType.Text;
				cmd.Parameters.Clear();

				db.AddParam(cmd, pnFlnu, flnu);
				//LogTraceMsg(cmd.CommandText);
				db.FillDataTable(cmd, ds.TRIP);

				if ((ds != null) && (ds.TRIP.Rows.Count > 0))
				{
					loadSuccess = true;
					//LogTraceMsg("loadSuccess" + loadSuccess + "," + flnu + "," + ds.TRIP.Rows.Count);
				}
				else
				{
					LogTraceMsg("No Trip Data for " + flnu);
				}
			}
			catch (Exception ex)
			{
				LogTraceMsg("LoadData:Err:" + flnu + "," + ex.Message);
				throw;
			}
			finally
			{
				if (isCmdNull)
				{
					db.CloseCommand(cmd, false);
					cmd = null;
				}
			}

			return loadSuccess;
		}


		/// <summary>
		/// Load Trip Data for given flightId. It will return True when loading success.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="ds"></param>
		/// <param name="flightId">Flight Id to fetch the data</param>
		/// <returns></returns>
		public bool LoadDataSet(IDbCommand cmd, ref DataSet ds, string flnu)
		{
			bool loadSuccess = false;
			UtilDb db = UtilDb.GetInstance();
			bool isCmdNull = false;
			try
			{
				if (cmd == null)
				{
					isCmdNull = true;
					cmd = db.GetCommand(false);
				}
				DateTime dt1 = DateTime.Now;

				if (ds == null) ds = new DataSet();

				string paramNamePrefix = UtilDb.GetParaNamePrefix();
				string pnFlnu = paramNamePrefix + "FLID";

                cmd.CommandText = _cmdSelTrp + GetDb_Tb_Trp() + " WHERE FLNU= '" + flnu + "'";
				cmd.CommandType = CommandType.Text;
				cmd.Parameters.Clear();

				// db.AddParam(cmd, pnFlnu, flnu);

				db.FillDataSet(cmd, ds);

				if ((ds != null) && (ds.Tables["CPMDET"].Rows.Count > 0))
				{
					loadSuccess = true;
				}
				else
				{
					LogMsg("No Cpm Data for " + flnu);
				}
			}
			catch (Exception)
			{
				throw;
			}
			finally
			{
				if (isCmdNull)
				{
					db.CloseCommand(cmd, false);
					cmd = null;
				}
			}

			return loadSuccess;
		}

		/// <summary>
		/// Add a row of the DsCpm Dataset to given DsCpm Dataset. 
		/// If is there any row with same id will be deleted from dataset before adding the new row.
		/// </summary>
		/// <param name="ds"></param>
		/// <param name="row"></param>
		/// <returns></returns>
		private bool AddDsTripRow(DSTrip ds, DSTrip.TRIPRow row)
		{
			bool ok = false;
			if (ds == null) throw new ApplicationException("UpdateDsCpm:Null Data.");
			string tripId = row.URNO;
			foreach (DSTrip.TRIPRow tRow in ds.TRIP.Select("URNO='" + tripId + "'"))
			{
				tRow.Delete();
			}
			ds.TRIP.AddTRIPRow(row);
			ok = true;
			return ok;
		}
		//const string DB_TAB_TRPMST = "ITK_TRPMST";
		//const string DB_TAB_TRP = "ITK_TRP";
		//const string DB_TAB_TRPDET = "ITK_TRPDET";

        private static string GetDb_Tb_TrpMst()
        {
            return DB_Info.GetDbFullName(DB_Info.DB_T_TRPMST);
        }
        private static string GetDb_Tb_Trp()
        {
            return DB_Info.GetDbFullName(DB_Info.DB_T_TRP);
        }
        private static string GetDb_Tb_TrpDet()
        {
            return DB_Info.GetDbFullName(DB_Info.DB_T_TRPDET);
        }

        private static string GetDb_Tb_TrpDetView()
        {
            return DB_Info.GetDbFullName(DB_Info.DB_V_TRP);
        }
		public bool HasChanges(DateTime lastChgDt, out DateTime curChgDt)
		{
			bool changed = true;
			bool isExist = false;
            curChgDt = UtilDb.GetLastUpdateDateTimeOfTable(null, DB_Info.DB_T_TRP, out isExist);

			if (!isExist)
			{
				throw new ApplicationException("Not able to get the Trip Last Update Time information.");
			}

			if (curChgDt.CompareTo(lastChgDt) > 0) changed = true;
			else changed = false;

			return changed;
		}

		//private static bool _isFetchingTripInfo = false;

		///// <summary>
		///// Load the TRIP information from database to common static variable dsTrip.
		///// </summary>
		///// <param name="loadSuccess">True - Loading Success</param>
		///// <param name="returnACopyWhenSuccess">True-To return the copy of dsTrip when success</param>
		///// <returns>Latest job Information</returns>
		//public DSTrip LoadDSTrip(out bool loadSuccess, bool returnACopyWhenSuccess)
		//{
		//    loadSuccess = false;

		//    //LogMsg("Summarise the flight Infor");
		//    DateTime curChgDt = DEFAULT_DT;
		//    if (!_isFetchingTripInfo)
		//    {
		//        if ((_dsTrip == null) || HasChanges(lastWriteDT, out curChgDt))
		//        {
		//            lock (_lockObj)
		//            {
		//                _isFetchingTripInfo = true;
		//                try
		//                {
		//                    if ((_dsTrip == null) || HasChanges(lastWriteDT, out curChgDt))
		//                    {
		//                        try
		//                        {
		//                            DSTrip tempDs = new DSTrip();

		//                            if (LoadData(null, ref tempDs))
		//                            {
		//                                if ((tempDs != null) && (tempDs.TRIP.Rows.Count > 0))
		//                                {
		//                                    loadSuccess = true;
		//                                    SetLatestInfoToDataset(tempDs, curChgDt);
		//                                    //LogTraceMsg("Get new flight info.");
		//                                }
		//                                else
		//                                {
		//                                    LogMsg("No Cpm Data");
		//                                }
		//                            }



		//                        }
		//                        catch (Exception ex)
		//                        {
		//                            LogMsg("LoadDSTrip:Err:" + ex.Message);
		//                            throw new ApplicationException("Error Accessing Trip Information.");
		//                        }
		//                    }

		//                }
		//                catch (Exception ex)
		//                {
		//                    LogMsg("LoadDSTrip:Err:" + ex.Message);
		//                }
		//                finally
		//                {
		//                    _isFetchingTripInfo = false;
		//                }
		//            }
		//        }
		//        else
		//        {
		//            loadSuccess = true;
		//        }
		//    }

		//    if ((loadSuccess) && (returnACopyWhenSuccess))
		//    {
		//        return (DSTrip)_dsTrip.Copy();
		//    }
		//    return _dsTrip;
		//}

		//private void SetLatestInfoToDataset(DSTrip ds, DateTime curChgDt)
		//{
		//    _dsTrip = ds;
		//    lastWriteDT = curChgDt;
		//}

		const string FIRST_TRIP_IND = "'F'";
		const string LAST_TRIP_IND = "'L'";
		const string TRIP_COL_NAME_SENT = "SENT_DT";
		const string TRIP_COL_NAME_RCV = "RCV_DT";

		const string FL_COL_NAME_FTRIP_SENT = "FTRIP";
		const string FL_COL_NAME_FTRIP_RCV = "FTRIP-B";
		const string FL_COL_NAME_LTRIP_SENT = "LTRIP";
		const string FL_COL_NAME_LTRIP_RCV = "LTRIP-B";

	}
}