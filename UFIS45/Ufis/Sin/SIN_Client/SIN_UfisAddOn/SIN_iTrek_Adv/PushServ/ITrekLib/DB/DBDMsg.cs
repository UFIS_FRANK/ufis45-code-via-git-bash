#define TestingOn
#define TRACING_ON

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data;
using System.Diagnostics;

using iTrekXML;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.Util;
using UFIS.ITrekLib.ENT;
using MM.UtilLib;
using UFIS.ITrekLib.Misc;
using UFIS.ITrekLib.Ctrl;
using iTrekSessionMgr;
using UFIS.ITrekLib.Config;

namespace UFIS.ITrekLib.DB
{
	public class DBDMsg
	{
		private static Object _lockObj = new Object();//to use in synchronisation of single access
		private static DBDMsg _dbMsg = null;

		private static DSMsg _dsMSG = null;
		private static string _fnMSGXml = null;//file name of pdm XML in full path.
		private static string _fnMSGTOXml = null;//file name of pdm XML in full path.


		/// <summary>
		/// Single Pattern.
		/// Not allowed to create the object from other class.
		/// To get the object, user "GetInstance()" method.
		/// </summary>
		private DBDMsg() { }

		public void Init()
		{//Initialize all the information, to get the latest data when fetching them again.
			_dsMSG = null;
			_fnMSGXml = null;
			_fnMSGTOXml = null;
			_lastLoadAllDT = DEFAULT_DT;
			_lastRefreshDT = DEFAULT_DT;
			_lastMsgRefreshDT = CurrentDateTime().AddDays(-10);
			_lastMsgToRefreshDT = _lastMsgRefreshDT;
		}

		/// <summary>
		/// Get the Instance of DBCpm. There will be only one reference for all the clients
		/// </summary>
		/// <returns></returns>
		public static DBDMsg GetInstance()
		{

			if (_dbMsg == null)
			{
				_dbMsg = new DBDMsg();
			}
			return _dbMsg;
		}

		public DSMsg RetrieveOutMsg(string staffId)
		{
			DSMsg ds = new DSMsg();
			DSMsg tempDs = (DSMsg)dsMSG.Copy();
			foreach (DSMsg.MSGRow row in tempDs.MSG.Select("SENT_BY_ID='" + staffId + "'", "SENT_DT DESC"))
			{
				ds.MSG.LoadDataRow(row.ItemArray, true);
				//int len=row.GetChildRows("MSG_MSG_TO").Length;
				foreach (System.Data.DataRow childRow in row.GetChildRows("MSG_MSG_TO"))
				{
					ds.MSGTO.LoadDataRow(childRow.ItemArray, true);
				}
			}

			return ds;
		}

		private string GetFromTime(int hour)
		{
			if (hour > 0) hour = 0 - hour;
			if (hour == 0) hour = -12;
			return UtilTime.ConvDateTimeToUfisFullDTString(DateTime.Now.AddHours(hour));
		}

		public DSMsg RetrieveOutMsg(string staffId, int maxCnt, int hour)
		{
			DSMsg ds = new DSMsg();
			DSMsg tempDs = (DSMsg)dsMSG.Copy();
			int cnt = 0;
			if (tempDs != null)
			{
				string frTime = GetFromTime(hour);
				foreach (DSMsg.MSGRow row in tempDs.MSG.Select("SENT_DT>=" + frTime + " AND SENT_BY_ID='" + staffId + "'", "SENT_DT DESC"))
				{
					ds.MSG.LoadDataRow(row.ItemArray, true);
					//int len=row.GetChildRows("MSG_MSG_TO").Length;
					if (tempDs.MSGTO != null)
					{
						foreach (System.Data.DataRow childRow in row.GetChildRows("MSG_MSG_TO"))
						{
							ds.MSGTO.LoadDataRow(childRow.ItemArray, true);
							cnt++;
							if (cnt >= maxCnt) break;
						}
						if (cnt >= maxCnt) break;
					}
				}
			}
			return ds;
		}

		public DSMsg RetrieveOutMessageDetailsForUrno(string urno)
		{
			DSMsg ds = new DSMsg();
			DSMsg tempDs = (DSMsg)dsMSG.Copy();
			if (tempDs != null)
			{
				if (tempDs.MSGTO != null)
				{
					foreach (DSMsg.MSGRow row in tempDs.MSG.Select("URNO='" + urno + "'"))
					{
						ds.MSG.LoadDataRow(row.ItemArray, true);
						//int len=row.GetChildRows("MSG_MSG_TO").Length;
						foreach (System.Data.DataRow childRow in row.GetChildRows("MSG_MSG_TO"))
						{
							ds.MSGTO.LoadDataRow(childRow.ItemArray, true);
						}
					}
				}
			}
			return ds;

		}
		public DSMsg RetrieveInMsg(string staffId)
		{
			DSMsg ds = new DSMsg();
			DSMsg tempDs = (DSMsg)dsMSG.Copy();

			foreach (DSMsg.MSGTORow row in tempDs.MSGTO.Select("MSG_TO_ID='" + staffId + "'", "SENT_DT DESC"))
			{
				ds.MSG.LoadDataRow(row.GetParentRow("MSG_MSG_TO").ItemArray, true);
				ds.MSGTO.LoadDataRow(row.ItemArray, true);
			}

			return ds;
		}

		public DSMsg RetrieveInMsgForFlight(string urno, string turnUrno)
		{
			DSMsg ds = new DSMsg();
			DSMsg tempDs = dsMSG;
			int cnt = 0;

			foreach (DSMsg.MSGTORow row in tempDs.MSGTO.Select("(MSG_TO_ID='" + urno + "' OR MSG_TO_ID='" + turnUrno + "')", "SENT_DT DESC"))
			{
				ds.MSG.LoadDataRow(row.GetParentRow("MSG_MSG_TO").ItemArray, true);
				ds.MSGTO.LoadDataRow(row.ItemArray, true);
				cnt++;
				if (cnt >= 50) break;
			}

			return ds;
		}


		public DSMsg RetrieveInMsg(string staffId, string jobCat)
		{
			DSMsg ds = new DSMsg();
			DSMsg tempDs = (DSMsg)dsMSG.Copy();

			foreach (DSMsg.MSGTORow row in tempDs.MSGTO.Select("MSG_TO_ID='" + staffId + "' OR MSG_TO_ID='" + jobCat + "' ", "SENT_DT DESC"))
			{
				ds.MSG.LoadDataRow(row.GetParentRow("MSG_MSG_TO").ItemArray, true);
				ds.MSGTO.LoadDataRow(row.ItemArray, true);
			}

			return ds;
		}



		private string GetMsgToIdFilter(string staffId, ArrayList groupList)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append("(MSG_TO_ID='" + staffId + "'");

			if (groupList != null)
			{
				int cnt = groupList.Count;
				for (int i = 0; i < cnt; i++)
				{
					string groupId = groupList[i].ToString().Trim();
					if (groupId == "") continue;
					sb.Append(" OR MSG_TO_ID='" + groupId + "'");
				}
			}
			sb.Append(")");
			return sb.ToString();
		}

		public DSMsg RetrieveInMsg(string staffId, ArrayList groupList)
		{
			DSMsg ds = new DSMsg();
			DSMsg tempDs = (DSMsg)dsMSG.Copy();
			if (tempDs != null)
			{
				string stSelect = "MSG_TO_ID='" + staffId + "'";
				if (groupList != null)
				{
					int cnt = groupList.Count;
					for (int i = 0; i < cnt; i++)
					{
						string groupId = groupList[i].ToString().Trim();
						if (groupId == "") continue;
						stSelect += " OR MSG_TO_ID='" + groupId + "'";
					}
				}
				//LogTraceMsg("RetrieveInMsg : " + stSelect);
				if (tempDs.MSGTO != null)
				{
					foreach (DSMsg.MSGTORow row in tempDs.MSGTO.Select(stSelect, "SENT_DT DESC"))
					{
						ds.MSG.LoadDataRow(row.GetParentRow("MSG_MSG_TO").ItemArray, true);
						ds.MSGTO.LoadDataRow(row.ItemArray, true);
					}
				}
			}

			return ds;
		}

		public DSMsg RetrieveInMsg(string staffId, ArrayList groupList, int maxCnt, int hour)
		{
			return RetrieveInMsg(staffId, groupList, maxCnt, hour, EnComposeMsgType.Undefined);
			//DSMsg ds = new DSMsg();
			//DSMsg tempDs = (DSMsg)dsMSG.Copy();
			//if (tempDs != null)
			//{
			//    //string stSelect = "MSG_TO_ID='" + staffId + "'";
			//    string fromTime = GetFromTime(hour);
			//    string stSelect = "SENT_DT>=" + fromTime + " AND (MSG_TO_ID='" + staffId + "'";
			//    if (groupList != null)
			//    {
			//        int gcnt = groupList.Count;
			//        for (int i = 0; i < gcnt; i++)
			//        {
			//            string groupId = groupList[i].ToString().Trim();
			//            if (groupId == "") continue;
			//            stSelect += " OR MSG_TO_ID='" + groupId + "'";
			//        }
			//    }
			//    stSelect += ")";
			//    int cnt = 0;
			//    //LogTraceMsg("RetrieveInMsg : " + stSelect);
			//    if (tempDs.MSGTO != null)
			//    {
			//        DSMsg.MSGTORow[] rows = (DSMsg.MSGTORow[])tempDs.MSGTO.Select(stSelect, "SENT_DT DESC");

			//        foreach (DSMsg.MSGTORow row in tempDs.MSGTO.Select(stSelect, "SENT_DT DESC"))
			//        {
			//            ds.MSG.LoadDataRow(row.GetParentRow("MSG_MSG_TO").ItemArray, true);
			//            ds.MSGTO.LoadDataRow(row.ItemArray, true);
			//            cnt++;
			//            if (cnt >= maxCnt) break;
			//        }
			//    }
			//}
			//return ds;
		}

		public DSMsg RetrieveInMsg(string staffId, ArrayList groupList, int maxCnt, int hour, EnComposeMsgType msgType)
		{
			DSMsg ds = new DSMsg();
			DSMsg tempDs = (DSMsg)dsMSG.Copy();
			if (tempDs != null)
			{
				//string stSelect = "MSG_TO_ID='" + staffId + "'";
				string fromTime = GetFromTime(hour);
				string stSelect = "SENT_DT>=" + fromTime + " AND (MSG_TO_ID='" + staffId + "'";
				if (groupList != null)
				{
					int gcnt = groupList.Count;
					for (int i = 0; i < gcnt; i++)
					{
						string groupId = groupList[i].ToString().Trim();
						if (groupId == "") continue;
						stSelect += " OR MSG_TO_ID='" + groupId + "'";
					}
				}
				stSelect += ")";
				switch (msgType)
				{
					case EnComposeMsgType.Personal:
						stSelect += " AND TP<>'F'";
						break;
					case EnComposeMsgType.Flight:
						stSelect += " AND TP='F'";
						break;
					case EnComposeMsgType.Group:
						stSelect += " AND TP<>'F'";
						break;
					case EnComposeMsgType.Undefined:
						break;
					default:
						break;
				}

				int cnt = 0;
				//LogTraceMsg("RetrieveInMsg : " + stSelect);
				if (tempDs.MSGTO != null)
				{
					DSMsg.MSGTORow[] rows = (DSMsg.MSGTORow[])tempDs.MSGTO.Select(stSelect, "SENT_DT DESC");

					foreach (DSMsg.MSGTORow row in tempDs.MSGTO.Select(stSelect, "SENT_DT DESC"))
					{
						ds.MSG.LoadDataRow(row.GetParentRow("MSG_MSG_TO").ItemArray, true);
						ds.MSGTO.LoadDataRow(row.ItemArray, true);
						cnt++;
						if (cnt >= maxCnt) break;
					}
				}
			}
			return ds;
		}


		public DSMsg RetrieveInMessageDetailsForUrno(string msgToUrNo)
		{
			DSMsg ds = new DSMsg();
			DSMsg tempDs = (DSMsg)dsMSG.Copy();
			if (tempDs != null)
			{
				if (tempDs.MSGTO != null)
				{
					foreach (DSMsg.MSGTORow row in tempDs.MSGTO.Select("URNO='" + msgToUrNo + "'"))
					{
						ds.MSGTO.LoadDataRow(row.ItemArray, true);
						ds.MSG.LoadDataRow(row.GetParentRow("MSG_MSG_TO").ItemArray, true);
					}
				}
			}

			return ds;
		}

		private string fnMSGXml
		{
			get
			{
				if ((_fnMSGXml == null) || (_fnMSGXml == ""))
				{
					iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
					_fnMSGXml = path.GetMsgFile();
				}

				return _fnMSGXml;
			}
		}
		private string fnMSGTOXml
		{
			get
			{
				if ((_fnMSGTOXml == null) || (_fnMSGTOXml == ""))
				{
					iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
					_fnMSGTOXml = path.GetMsgToFile();
				}

				return _fnMSGTOXml;
			}
		}

		private DateTime CurrentDateTime() { return ITrekTime.GetCurrentDateTime(); }

		private bool IsNeedToReloadAllData()
		{
			if ((_dsMSG == null) || (UtilTime.DiffInSec(CurrentDateTime(), _lastLoadAllDT) > 300))
				return true;
			else return false;
		}

		public void RefreshData()
		{
			UtilDb udb = UtilDb.GetInstance();
			IDbCommand cmd = null;

			try
			{
				cmd = udb.GetCommand(false);

				DSMsg dsTemp = new DSMsg();
				DateTime lastUpdDateTime;
				if (LoadDsMsgFromDbToUpdNewAndChangesData(cmd,
					CurrentDateTime().AddDays(-1),
					_lastMsgRefreshDT, _lastMsgToRefreshDT,
					_dsMSG, out dsTemp, out lastUpdDateTime))
				{
					_dsMSG = dsTemp;
					_lastMsgToRefreshDT = lastUpdDateTime;
					_lastMsgRefreshDT = lastUpdDateTime;
					_lastRefreshDT = CurrentDateTime();
				}

			}
			catch (Exception)
			{
				throw;
			}
			finally
			{
				udb.CloseCommand(cmd, false);
			}

		}

		public void LoadData()
		{
			DSMsg dsTemp = new DSMsg();
			DateTime lastUpdDateTime;
			if (LoadDsMsgFromDb(null,
				CurrentDateTime().AddDays(-1),
				dsTemp, out lastUpdDateTime))
			{
				_dsMSG = dsTemp;
				_lastLoadAllDT = lastUpdDateTime;
				_lastMsgToRefreshDT = lastUpdDateTime;
				_lastMsgRefreshDT = lastUpdDateTime;
				_lastRefreshDT = CurrentDateTime();
			}
			else
			{
				if (_dsMSG == null) _dsMSG = new DSMsg();
			}
		}

		private DSMsg dsMSG
		{
			get
			{
				List<DateTime> lsDt = new List<DateTime>();
				lsDt.Add(DateTime.Now);
				//if (_dsMSG == null) { LoadDSMsg(); }
				//bool loadSuccess = false;
				//DateTime lastUpdDateTime;

				if (IsNeedToReloadAllData())
				{
					LoadData();
					lsDt.Add(DateTime.Now);
				}
				else
				{
					if (UtilTime.DiffInSec(CurrentDateTime(), _lastRefreshDT) > 2)
					{
						RefreshData();
						lsDt.Add(DateTime.Now);
					}
				}
				if (_dsMSG == null) _dsMSG = new DSMsg();

				#region Display the Processing Timing Info in Log
				//lsDt.Add(DateTime.Now);

				//int cntDt = lsDt.Count;
				//if (cntDt > 1)
				//{
				//    string st = "dsMsg:";
				//    for (int i = 1; i < cntDt; i++)
				//    {
				//        st += string.Format(",{0}:{1}",
				//            i, UtilTime.ElapsedTime(lsDt[i - 1], lsDt[i]));
				//    }
				//    st += string.Format(",Tot:{0}",
				//            UtilTime.ElapsedTime(lsDt[1], lsDt[cntDt - 1]));
				//    LogTraceMsg(st);
				//}
				#endregion

				return _dsMSG;
			}
		}

		private static readonly DateTime DEFAULT_DT = new System.DateTime(1, 1, 1);
		private static DateTime lastMsgWriteDT = DEFAULT_DT;
		private static DateTime lastMsgToWriteDT = DEFAULT_DT;
		private static DateTime _lastRefreshDT = DEFAULT_DT;
		private static DateTime _lastLoadAllDT = DEFAULT_DT;
		private static DateTime _lastMsgRefreshDT = DEFAULT_DT;
		private static DateTime _lastMsgToRefreshDT = DEFAULT_DT;

		private void LoadDSMsg()
		{
			FileInfo fiMsgToXml = new FileInfo(fnMSGTOXml);
			FileInfo fiMsgXml = new FileInfo(fnMSGXml);
			DateTime curMsgDT = fiMsgXml.LastWriteTime;
			DateTime curMsgToDT = fiMsgToXml.LastWriteTime;
			if (!fiMsgXml.Exists) throw new ApplicationException("Message information missing. Please inform to administrator.");
			if (!fiMsgToXml.Exists) throw new ApplicationException("Message Inbox information missing. Please inform to administrator.");

			if ((_dsMSG == null) || (curMsgDT != lastMsgWriteDT) || (curMsgToDT != lastMsgToWriteDT))
			{
				lock (_lockObj)
				{
					try
					{
						fiMsgToXml = new FileInfo(fnMSGTOXml);
						fiMsgXml = new FileInfo(fnMSGXml);
						curMsgDT = fiMsgXml.LastWriteTime;
						curMsgToDT = fiMsgToXml.LastWriteTime;
						if ((_dsMSG == null) || (curMsgDT != lastMsgWriteDT) || (curMsgToDT != lastMsgToWriteDT))
						{
							if (_dsMSG == null)
							{
								_dsMSG = new DSMsg();
							}
							DSMsg tempDsMsg = new DSMsg();
							try
							{
								tempDsMsg.MSG.ReadXml(fnMSGXml);
								tempDsMsg.MSGTO.ReadXml(fnMSGTOXml);
								_dsMSG.MSG.Clear();
								_dsMSG.MSGTO.Clear();
								_dsMSG.MSG.Merge(tempDsMsg.MSG);
								_dsMSG.MSGTO.Merge(tempDsMsg.MSGTO);
								//_dsMSG.MSG.Clear();
								//_dsMSG.MSG.ReadXml(fnMSGXml);
								//_dsMSG.MSGTO.Clear();
								//_dsMSG.MSGTO.ReadXml(fnMSGTOXml);
							}
							catch (Exception ex)
							{
								LogMsg("LoadDSMsg : Error : " + ex.Message);
							}
						}
					}
					catch (Exception ex)
					{
						LogMsg("LoadDSMsg:Err:" + ex.Message);
					}
				}
			}
		}


		//[Conditional("TRACING_ON")]
		private void LogTraceMsg(string msg)
		{
			try
			{
				MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("DBDMsg", msg);
			}
			catch (Exception)
			{
				UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DBDMsg", msg);
			}
		}

		private void LogMsg(string msg)
		{
			//UtilLog.LogToGenericEventLog("DBDMsg", msg);
			try
			{
				MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("DBDMsg", msg);
			}
			catch (Exception)
			{
				UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DBDMsg", msg);
			}
		}


		private void TestWriteXml()
		{
			//DSMsg ds1 = new DSMsg();
			//DSMsg.MSGRow msgRow = ds1.MSG.NewMSGRow();
			//DSMsg.MSGTORow msgToRow = ds1.MSGTO.NewMSGTORow();

			//msgRow.MSG_BODY = "msgBody";
			//msgRow.SENT_BY_ID = "sentBy";
			//msgRow.SENT_BY_NAME = "sentByName";
			//msgRow.SENT_DT = "20070122230011";
			//msgRow.URNO = "123";
			//ds1.MSG.AddMSGRow(msgRow);

			//msgToRow.MSG_TO_ID = "to";
			//msgToRow.MSG_URNO = "msgUrno";
			//msgToRow.RCV_BY = "";
			//msgToRow.RCV_DT = "20070122231111";
			//msgToRow.URNO = "msgToUrno";
			//ds1.MSGTO.AddMSGTORow(msgToRow);

			//ds1.AcceptChanges();
			//ds1.MSG.WriteXml(fnMSGXml);
			//ds1.MSGTO.WriteXml(fnMSGTOXml);
		}


		public int GetUnReadInBoxMsgCount(string staffId, ArrayList groupList, int hour)
		{
			int msgCnt = 0;
			DSMsg ds = new DSMsg();
			DSMsg tempDs = (DSMsg)dsMSG.Copy();
			string stSelect = "MSG_TO_ID='" + staffId + "'";
			string stUnReadSelect = "(RCV_DT IS NULL OR RCV_DT='')";
			if (groupList != null)
			{
				int cnt = groupList.Count;
				for (int i = 0; i < cnt; i++)
				{
					string groupId = groupList[i].ToString().Trim();
					if (groupId == "") continue;
					stSelect += " OR MSG_TO_ID='" + groupId + "'";
				}
			}
			stSelect = "SENT_DT>=" + GetFromTime(hour) + " AND (" + stSelect + ") AND " + stUnReadSelect;

			try
			{
				msgCnt = tempDs.MSGTO.Select(stSelect).Length;
			}
			catch (Exception ex)
			{
				LogMsg("GetUnReadInBoxMsgCount:Error:" + stSelect + ":" + ex.Message);
			}

			return msgCnt;
		}

        public string SendMessage(IDbCommand cmd, EntMsg msgToSend, string userId, DateTime updateDateTime,out  ArrayList lstReceipients)
		{
			string msgId = DbMsg_Insert(cmd, msgToSend, userId, updateDateTime);
			int msgToCnt = DbMsgTo_Insert(cmd, msgToSend, msgId, userId, updateDateTime,out lstReceipients);
			return msgId;
		}

		public void ReceiveMessage(IDbCommand cmd, string msgToId, string receivedById, DateTime receiveDateTime,
		   string userId, DateTime updateDateTime)
		{
			DBMsgTo_Update(cmd, msgToId, receivedById, receiveDateTime, userId, updateDateTime);
		}

		//private const string DB_MSG = "ITK_MSG";
		//private const string DB_MSG_TO = "ITK_MSGTO";

		//private const string DB_V_MSG = "V_ITK_MSG";
		//private const string DB_V_MSGTO = "V_ITK_MSGTO";

		private const string ID_TYPE_FOR_MSG = "ITK_MSG";
		private const string ID_TYPE_FOR_MSG_TO = "ITK_MSGTO";


        private static string GetDb_Tb_Msg()
        {
            return DB_Info.GetDbFullName(DB_Info.DB_T_MSG);
        }
        private static string GetDb_Vw_Msg()
        {
            return DB_Info.GetDbFullName(DB_Info.DB_V_MSG);
        }
        private static string GetDb_Tb_MsgTo()
        {
            return DB_Info.GetDbFullName(DB_Info.DB_T_MSGTO);
        }
        private static string GetDb_Vw_MsgTo()
        {
            return DB_Info.GetDbFullName(DB_Info.DB_V_MSGTO);
        }

		private string GenNewMsgId(IDbCommand cmd, string stDateTime)
		{
			//return UtilDb.GetInstance().GenUrid(cmd, ID_TYPE_FOR_MSG, false, DateTime.Now);
			return UtilDb.GenUrid(cmd, ID_TYPE_FOR_MSG, false, DateTime.Now);
		}

		private string GenNewMsgToId(IDbCommand cmd, string stDateTime)
		{
			//return UtilDb.GetInstance().GenUrid(cmd, ID_TYPE_FOR_MSG_TO, false, DateTime.Now);
			return UtilDb.GenUrid(cmd, ID_TYPE_FOR_MSG_TO, false, DateTime.Now);
		}

		#region DB - MSG Table Operation
		private string DbMsg_Insert(IDbCommand cmd, EntMsg msgToSend, string userId, DateTime updateDateTime)
		{
			StringBuilder sb = new StringBuilder();
			StringBuilder sbDbColNames = new StringBuilder();
			UtilDb udb = UtilDb.GetInstance();
			string paramPrefix = UtilDb.GetParaNamePrefix();

			sbDbColNames.Append("URNO,SBID,MBDY,SNDT,CDAT,USEC,LSTU,USEU");
            LogTraceMsg("sbDbColNames" + sbDbColNames.ToString());
			string msgId = GenNewMsgId(cmd, "");
            LogTraceMsg("msgId" + msgId);
			cmd.Parameters.Clear();
			sb.Append(udb.PrepareParam(cmd, "URNO", msgId));
			sb.Append("," + udb.PrepareParam(cmd, "SBID", msgToSend.SentById));
			sb.Append("," + udb.PrepareParam(cmd, "MBDY", msgToSend.MsgBody));
			sb.Append("," + udb.PrepareParam(cmd, "SNDT", msgToSend.SendDateTime));
			sb.Append("," + udb.PrepareParam(cmd, "CDAT", updateDateTime));
			sb.Append("," + udb.PrepareParam(cmd, "USEC", userId));
			sb.Append("," + udb.PrepareParam(cmd, "LSTU", updateDateTime));
			sb.Append("," + udb.PrepareParam(cmd, "USEU", userId));

			string stCmd = string.Format(@"INSERT INTO {0} ({1}) VALUES ({2})",
               GetDb_Tb_Msg(), sbDbColNames.ToString(), sb.ToString());
            LogTraceMsg("stCmd" + stCmd);
			if (stCmd != cmd.CommandText)
			{
				cmd.CommandText = stCmd;
				cmd.CommandType = CommandType.Text;
			}

			int updCnt = cmd.ExecuteNonQuery();
			if (updCnt < 1) throw new ApplicationException("Update Fail!!!");
			return msgId;
		}

		private const string SEL_CMD_MSG = "SELECT URNO, SENT_BY_ID, SENT_BY_NAME, MSG_BODY, SENT_DT FROM ";
		private const string SEL_CMD_MSGTO = "SELECT URNO, MSG_URNO, MSG_TO_ID, TONM, RCV_BY, RCNM, RCV_DT, TP FROM ";

		private bool LoadDsMsg_MSG(IDbCommand cmd, DateTime dtFrSentDt, DSMsg ds,
		   bool changedDataOnly, DateTime dtLastUpd)
		{
			bool loadSuccess = false;
			StringBuilder sb = new StringBuilder();
			StringBuilder sbWhere = new StringBuilder();
			UtilDb udb = UtilDb.GetInstance();

			try
			{
				cmd.Parameters.Clear();
				cmd.CommandText = SEL_CMD_MSG +GetDb_Vw_Msg()+ " WHERE SNDT>=" + udb.PrepareParamForTime(cmd, "FR_SNDT", dtFrSentDt);

				if (changedDataOnly)
				{//Get the data with newly insert or changes on or after the given date and time
					cmd.CommandText += " AND LSTU>=" + udb.PrepareParamForTime(cmd, "FR_DATE", dtLastUpd);
				}
				else
				{//Get the data with Send date time is on or after the given date and time.
				}
				cmd.CommandType = CommandType.Text;

				udb.FillDataTable(cmd, ds.MSG);
				loadSuccess = true;
			}
			catch (Exception ex)
			{
				LogMsg("LoadDsMsg_MSG:Err:" + ex.Message);
			}

			return loadSuccess;
		}

		private bool LoadDsMsg_MSGTO(IDbCommand cmd, DateTime dtFrSentDt, DataTable dt, bool changedDataOnly, DateTime dtLastUpd)
		{
			bool loadSuccess = false;
			StringBuilder sb = new StringBuilder();
			StringBuilder sbWhere = new StringBuilder();
			UtilDb udb = UtilDb.GetInstance();

			try
			{
				cmd.Parameters.Clear();
				cmd.CommandText = SEL_CMD_MSGTO + GetDb_Vw_MsgTo()+" WHERE SNDT>=" + udb.PrepareParamForTime(cmd, "FR_SNDT", dtFrSentDt);

				if (changedDataOnly)
				{//Get the data with newly insert or changes on or after the given date and time
					cmd.CommandText += " AND LSTU>=" + udb.PrepareParamForTime(cmd, "FR_DATE", dtLastUpd);
				}
				else
				{//Get the data with Send date time is on or after the given date and time.

				}
				cmd.CommandType = CommandType.Text;

				udb.FillDataTable(cmd, dt);
				loadSuccess = true;
			}
			catch (Exception ex)
			{
				LogMsg("LoadDsMsg_MSG:Err:" + ex.Message);
			}

			return loadSuccess;
		}

		#endregion

		#region DB - MSGTO Table Operation

		/// <summary>
		/// Insert New "Message To" information records into iTrek DB (Use in sending the message)
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="msgToSend">message to send</param>
		/// <param name="msgId">message id</param>
		/// <param name="userId"></param>
		/// <param name="updateDateTime"></param>
		/// <returns>number of new "MSGTO" records</returns>
        private int DbMsgTo_Insert(IDbCommand cmd, EntMsg msgToSend, string msgId, string userId, DateTime updateDateTime, out ArrayList lstReceipients)
		{
			StringBuilder sb = new StringBuilder();
			StringBuilder sbDbColNames = new StringBuilder();
			UtilDb udb = UtilDb.GetInstance();
			List<string> lsColNames = new List<string>();
			string paramPrefix = UtilDb.GetParaNamePrefix();
            lstReceipients = new ArrayList();
            lstReceipients.Clear();
			sbDbColNames.Append("URNO,MSID,TOID,TOTP,CDAT,USEC,LSTU,USEU");
			cmd.Parameters.Clear();
			sb.Append(udb.PrepareParam(cmd, "URNO", null));
			sb.Append("," + udb.PrepareParam(cmd, "MSID", null));
			sb.Append("," + udb.PrepareParam(cmd, "TOID", null));
			sb.Append("," + udb.PrepareParam(cmd, "TOTP", null));
			sb.Append("," + udb.PrepareParam(cmd, "CDAT", updateDateTime));
			sb.Append("," + udb.PrepareParam(cmd, "USEC", userId));
			sb.Append("," + udb.PrepareParam(cmd, "LSTU", updateDateTime));
			sb.Append("," + udb.PrepareParam(cmd, "USEU", userId));

			string stCmd = string.Format(@"INSERT INTO {0} ({1}) VALUES ({2})",
               GetDb_Tb_MsgTo(), sbDbColNames.ToString(), sb.ToString());
            LogTraceMsg("stCmd" + stCmd);
			if (stCmd != cmd.CommandText)
			{
				cmd.CommandText = stCmd;
				cmd.CommandType = CommandType.Text;
				cmd.Prepare();
			}

			int totUpdCnt = 0;
			string pnUrno = paramPrefix + "URNO";
			string pnMsid = paramPrefix + "MSID";
			string pnToid = paramPrefix + "TOID";
			string pnToty = paramPrefix + "TOTP";
			List<EntMsgTo> lsReceipients = msgToSend.LsReceipients;
			IDbCommand cmdForGenNewId = udb.CloneCommand(cmd, cmd.Connection, cmd.Transaction);

			foreach (EntMsgTo msgTo in lsReceipients)
			{
				udb.PrepareParam(cmd, "URNO", GenNewMsgToId(cmdForGenNewId, ""));
				udb.PrepareParam(cmd, "MSID", msgId);
				udb.PrepareParam(cmd, "TOID", msgTo.ReceipientId);
				udb.PrepareParam(cmd, "TOTP", msgTo.StReceipientType);
                if (msgTo.StReceipientType == "F")
                {
                    string aoId = "";
                    try
                    {
                        aoId = CtrlFlight.GetAOIdForFlight(msgTo.ReceipientId);
                    }
                    catch (Exception)
                    {


                    }
                    if (aoId != "")
                    {
                        lstReceipients.Add(aoId);
                    }
                    
                }

                else 
                {
                    lstReceipients.Add(msgTo.ReceipientId);
                }
				int updCnt = cmd.ExecuteNonQuery();
				if (updCnt < 1) throw new ApplicationException("Update Fail!!!");
				totUpdCnt++;
			}
            //DoProcessForPushNewMessage(lstReceipients,msgToSend.MsgBody);
            
			return totUpdCnt;
		}

		/// <summary>
		/// Update the "MSGTO" in iTrek DB for acknowledge receive of the message.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="msgToId">message to id</param>
		/// <param name="receivedById">received by id</param>
		/// <param name="receivedDateTime">receive date time</param>
		/// <param name="userId">user id who update this information</param>
		/// <param name="updateDateTime">update date and time</param>
		private void DBMsgTo_Update(IDbCommand cmd, string msgToId, string receivedById, DateTime receivedDateTime, string userId, DateTime updateDateTime)
		{
			StringBuilder sb = new StringBuilder();
			StringBuilder sbDbColNames = new StringBuilder();
			UtilDb udb = UtilDb.GetInstance();
			List<string> lsColNames = new List<string>();
			string paramPrefix = UtilDb.GetParaNamePrefix();

			cmd.Parameters.Clear();
			sb.Append(udb.PrepareUpdCmdToUpdDb(cmd, "RCBY", receivedById));
			sb.Append("," + udb.PrepareUpdCmdToUpdDb(cmd, "RCDT", receivedDateTime));
			sb.Append("," + udb.PrepareUpdCmdToUpdDb(cmd, "LSTU", updateDateTime));
			sb.Append("," + udb.PrepareUpdCmdToUpdDb(cmd, "USEU", userId));

			string stWhere = "URNO=" + udb.PrepareParam(cmd, "URNO", msgToId);

			string stCmd = string.Format("UPDATE {0} SET {1} WHERE {2}",
               GetDb_Tb_MsgTo(), sb.ToString(), stWhere);

			if (stCmd != cmd.CommandText)
			{
				cmd.CommandText = stCmd;
				cmd.CommandType = CommandType.Text;
				cmd.Prepare();
			}

			int updCnt = cmd.ExecuteNonQuery();
			if (updCnt < 1) throw new ApplicationException("Update Fail!!!");
		}
		#endregion

		private bool LoadDsMsgFromDb(IDbCommand cmd, DateTime dtFrSentDt, DSMsg ds, out DateTime dtUploadDateTime)
		{
			if (ds == null) throw new ApplicationException("Null Dataset.");
			dtUploadDateTime = ITrekTime.GetCurrentDateTime();
			bool loadSuccess = false;
			StringBuilder sb = new StringBuilder();
			StringBuilder sbWhere = new StringBuilder();
			UtilDb udb = UtilDb.GetInstance();

			bool commit = false;
			bool isNewCmd = false;//Is command 'cmd' created from this method?

			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = udb.GetCommand(false);
				}

				if (LoadDsMsg_MSG(cmd, dtFrSentDt, ds, false, dtFrSentDt) &&
					LoadDsMsg_MSGTO(cmd, dtFrSentDt, ds.MSGTO, false, dtFrSentDt))
				{
					if ((ds.MSG != null) && (ds.MSG.Rows.Count > 0) &&
					   (ds.MSGTO != null) && (ds.MSGTO.Rows.Count > 0))
					{
						loadSuccess = true;
					}
				}
				commit = true;
			}
			catch (Exception ex)
			{
				LogMsg("LoadDsMsg_MSG:Err:" + ex.Message);
			}
			finally
			{
				if (isNewCmd)
				{
					udb.CloseCommand(cmd, commit);
				}
			}

			return loadSuccess;

		}

		private bool LoadDsMsgFromDbToUpdNewAndChangesData(IDbCommand cmd,
			DateTime dtFrSentDt,
		   DateTime dtMsgLastUpd, DateTime dtMsgtoLastUpd,
			DSMsg dsCur,
		   out DSMsg dsUpdated, out DateTime dtUploadDateTime)
		{
			bool loadSuccess = false;
			dsUpdated = new DSMsg();

			dtUploadDateTime = CurrentDateTime();
			if (dsCur == null)
			{
				loadSuccess = LoadDsMsgFromDb(cmd, dtFrSentDt, dsUpdated, out dtUploadDateTime);
			}
			else
			{
				dtUploadDateTime = ITrekTime.GetCurrentDateTime();
				StringBuilder sb = new StringBuilder();
				StringBuilder sbWhere = new StringBuilder();
				UtilDb udb = UtilDb.GetInstance();

				DSMsg dsTemp = new DSMsg();
				DataTable dt = new DataTable();

				bool commit = false;
				bool isNewCmd = false;//Is command 'cmd' created from this method?

				try
				{
					if (cmd == null)
					{
						isNewCmd = true;
						cmd = udb.GetCommand(false);
					}

					if (LoadDsMsg_MSG(cmd,
						dtFrSentDt, dsTemp, true,
						dtMsgLastUpd.AddMinutes(-10))
						&&
						LoadDsMsg_MSGTO(cmd, dtFrSentDt, dt, true,
						dtMsgtoLastUpd.AddMinutes(-10)))
					{
						dsUpdated = (DSMsg)dsCur.Copy();
						foreach (DSMsg.MSGRow row in dsTemp.MSG.Rows)
						{
							dsUpdated.MSG.LoadDataRow(row.ItemArray, true);
						}

						//foreach (DSMsg.MSGTORow row in dsTemp.MSGTO.Rows)
						//{
						//   dsUpdated.MSGTO.LoadDataRow(row.ItemArray, true);
						//}
						foreach (DataRow row in dt.Rows)
						{
							try
							{
								dsUpdated.MSGTO.LoadDataRow(row.ItemArray, true);
							}
							catch (Exception ex)
							{
								LogMsg("LoadDsMsgFromDbToUpdNewAndChangesData:MSGTO:Err:" + ex.Message);
							}
						}

						if ((dsUpdated.MSG != null) && (dsUpdated.MSG.Rows.Count > 0) &&
						   (dsUpdated.MSGTO != null) && (dsUpdated.MSGTO.Rows.Count > 0))
						{
							loadSuccess = true;
						}
					}
					commit = true;
				}
				catch (Exception ex)
				{
					LogMsg("LoadDsMsg_MSG:Err:" + ex.Message);
				}
				finally
				{
					if (isNewCmd)
					{
						udb.CloseCommand(cmd, commit);
					}
				}
			}

			return loadSuccess;
		}

		private const string FIELD_NAMES_MSGLOG_LOG = "URNO,SBY,SON,STO,RBY,RON,MSG";
		//private const string DB_MSGLOG_LOG = "V_ITK_MSGLOG_LOG";
		private static string GetDb_Vw_MsgLog()
		{
			return DB_Info.GetDbFullName(DB_Info.DB_V_MSGLOG);
		}

		public DSMsgLog RetrieveMsgLog(IDbCommand cmd, string sessionId,
			  string sentBy, string frUfisDateTime, string toUfisDateTime,
			  int startingIndex, int cnt, string orderBy)
		{
			DSMsgLog ds = new DSMsgLog();

			StringBuilder sb = new StringBuilder();
			StringBuilder sbWhere = new StringBuilder();
			UtilDb udb = UtilDb.GetInstance();
			string conj = " WHERE ";

			#region Prepare for "Order by" clause
			string ad = "DESC"; //Ascending or Descending order. Default to Descending
			if (orderBy == null) { orderBy = "SON"; }
			orderBy = orderBy.Trim().ToUpper();
			if ((orderBy != "SBY") && (orderBy != "SON") &&
				(orderBy != "STO") && (orderBy != "RBY") &&
				(orderBy != "RON")) orderBy = "SON";

			if ((orderBy == "SBY") || (orderBy == "STO") || (orderBy == "RBY"))
			{
				ad = "ASC";//String sort by ascending
			}
			else
			{
				ad = "DESC"; //Date sort by descending
			}

			if (cnt < 1) { cnt = 10; }
			else if (cnt > 100) { cnt = 100; }
			if (startingIndex < 1) startingIndex = 1;
			#endregion

			try
			{
				cmd.Parameters.Clear();
				//////           SELECT * FROM
				//////              WHERE [(UPPER(SDNM) LIKE '%<SENTBY>%' OR 
				//////              UPPER(SDID) LIKE '%<SENTBY>%') ]
				//////              [ AND ]
				//////              [ SDDT >= <FromDateTime> ]
				//////              [ AND ]
				//////              [ SDDT <= <ToDateTime> ]
				//////              ORDER BY <SortOrder> <AscDesc>

				if (!string.IsNullOrEmpty(sentBy))
				{
					sb.AppendFormat("{1}(UPPER(SBY) LIKE '%{0}%')", sentBy.Trim().ToUpper(), conj);
					conj = " AND ";
				}

				if (!string.IsNullOrEmpty(frUfisDateTime))
				{
					//DateTime dt = UtilTime.ConvUfisTimeStringToDateTime(frUfisDateTime);
					//sb.AppendFormat("{0}(SON>={1})", conj, udb.PrepareParam(cmd, "FR_SNDT", dt));
					sb.AppendFormat("{0}(SON>={1})", conj, udb.PrepareParam(cmd, "FR_SNDT", frUfisDateTime));
					conj = " AND ";
				}

				if (!string.IsNullOrEmpty(toUfisDateTime))
				{
					//DateTime dt = UtilTime.ConvUfisTimeStringToDateTime(toUfisDateTime);
					//sb.AppendFormat("{0}(SON<={1})", conj, udb.PrepareParam(cmd, "TO_SNDT", dt));
					sb.AppendFormat("{0}(SON<={1})", conj, udb.PrepareParam(cmd, "TO_SNDT", toUfisDateTime));
					conj = " AND ";
				}

				//sb.AppendFormat(" ORDER BY {0} {1}", orderBy.Trim().ToUpper(), ad);

				#region Oracle Select to fetch the given number of rows start from given row number
				//            SELECT * FROM (
				//  SELECT
				//    ROW_NUMBER() OVER (ORDER BY key ASC) AS rn,
				//    columns
				//  FROM tablename
				//)
				//WHERE rn >= startIndex AND rn < (n+startIndex)
				string stSelCmd = string.Format(
				   "SELECT /*+ FIRST_ROWS({6})*/ {0} FROM (SELECT ROW_NUMBER() OVER (ORDER BY {1} {2}, URNO DESC) AS tempRn,{0} FROM {3} {4}) WHERE tempRn >= {5} AND tempRn<{6}",
				   FIELD_NAMES_MSGLOG_LOG,
				   orderBy.Trim().ToUpper(), ad,
				   GetDb_Vw_MsgLog(), sb.ToString(),
				   startingIndex, startingIndex + cnt);
				string stSelCmdForTotCnt = string.Format("SELECT COUNT(*) FROM {0} {1}", GetDb_Vw_MsgLog(), sb.ToString());
				#endregion
				LogTraceMsg("RetrieveMsgLog:Sel: " + stSelCmd + Environment.NewLine +
					"   SelCnt:" + stSelCmdForTotCnt);
				cmd.CommandText = stSelCmd;
				cmd.CommandType = CommandType.Text;

				udb.FillDataTable(cmd, ds.LOG);

				cmd.CommandText = stSelCmdForTotCnt;
				string stTotCnt = cmd.ExecuteScalar().ToString();
				if (ds.HDR.Rows.Count < 1)
				{
					DSMsgLog.HDRRow nrow = ds.HDR.NewHDRRow();
					nrow.SESSID = sessionId;
					ds.HDR.AddHDRRow(nrow);
				}
				DSMsgLog.HDRRow row = (DSMsgLog.HDRRow)ds.HDR.Rows[0];
				row.TOT_CNT = stTotCnt;
				row.SESSID = sessionId;
				row.SID = startingIndex.ToString();
			}
			catch (Exception ex)
			{
				LogMsg("RetrieveMsgLog:Err:" + ex.Message);
				throw new ApplicationException("Error getting the Message Log.");
			}


			return ds;
		}


        //private void DoProcessForPushNewMessage(ArrayList alRec, string msgBody)
        //{
        //    string deviceId = "";
        //    SessionManager sessMan = new SessionManager();
        //    if ((alRec != null) && (alRec.Count != 0))
        //    {
        //        foreach (string rec in alRec)
        //        {
        //            try
        //            {
        //                if (rec == "AO")
        //                {
        //                    DSCurUser dsCur = CtrlApp.GetInstance().GetCurrentLoginedAOs();
        //                    foreach (DSCurUser.LOGINUSERRow row in dsCur.LOGINUSER.Select(""))
        //                    {
        //                        deviceId = row.DEVICE_ID.Trim();
        //                        pushMessage(deviceId, msgBody);
        //                    }
        //                }
        //                else
        //                    if (rec == "BO")
        //                    {
        //                        DSCurUser dsCur = CtrlApp.GetInstance().GetCurrentLoginedBOs();
        //                        foreach (DSCurUser.LOGINUSERRow row in dsCur.LOGINUSER.Select(""))
        //                        {
        //                            deviceId = row.DEVICE_ID.Trim();
        //                            pushMessage(deviceId, msgBody);
        //                        }
        //                    }
        //                    else
        //                    {
        //                        DSCurUser dsCur = CtrlCurUser.RetrieveCurUserForStaffId(rec);
        //                        //deviceId = sessMan.GetDeviceIDByPENO(rec);
        //                        try
        //                        {
        //                            deviceId = dsCur.LOGINUSER[0].DEVICE_ID;
        //                            pushMessage(deviceId, msgBody);
        //                        }
        //                        catch (Exception)
        //                        {

        //                            LogMsg("Excption while getting DeviceId:");
        //                        }
        //                    }
        //            }
        //            catch (Exception pidEx)
        //            {
        //                LogMsg("Push New Message exception:" + pidEx.Message);
        //            }
        //        }
        //    }
        //}

        //private void pushMessage(string deviceId, string msgBody)
        //{
        //    XmlPushClient PushClient = new XmlPushClient(GetPapUrl());
        //    try
        //    {
        //        string PID = "";
        //        string url = GetUrlForNetAlert() + "(S(" + GetSessionIdForDeviceId(deviceId) + "))/Messages.aspx?newmessage=alert";
        //        LogTraceMsg("url:" + url);

        //        if (deviceId != "")
        //        {
        //            // PID = PushClient.PushServiceIndication(deviceId, "New Msg-" + msgBody, "http://172.31.246.42/itrek/Messages.aspx?newmessage=alert");
        //            PID = PushClient.PushServiceIndication(deviceId, "New Msg-" + msgBody, url);
        //        }
        //        LogTraceMsg("PushId :" + PID + "-devicedId:" + deviceId);
        //    }
        //    catch (Exception ex)
        //    {
        //        LogMsg("pushMessage exception:" + ex.Message);
        //    }
        //}
        //private string GetPapUrl()
        //{
        //    iTXMLPathscl paths = new iTXMLPathscl();
        //    return paths.GetPapURL();
        //}

        //private string GetUrlForNetAlert()
        //{
        //    iTXMLPathscl paths = new iTXMLPathscl();
        //    return paths.GetURLForNetAlert();
        //}

        //private string GetSessionIdForDeviceId(string deviceId)
        //{
        //    string sessionId = "";
        //    try
        //    {
        //        sessionId = CtrlCurUser.RetrieveCurUserForDeviceId(deviceId).LOGINUSER[0].SESS_ID;
        //    }
        //    catch (Exception)
        //    {


        //    }

        //    return sessionId;

        //}


	}
}