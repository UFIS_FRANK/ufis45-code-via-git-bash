using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.IO;

using UFIS.ITrekLib.DS;

namespace UFIS.ITrekLib.ENT
{
    public class EntDb:MarshalByRefObject
    {
        private int _cnt = 0;
        private static object _ObjDsLock = new object();
        private static DSFlight _dsFlight = null;
        //private static string _flightSummXml = null;
        private static DateTime DEFAULT_DT = new DateTime(1, 1, 1);
        private static DateTime _dtLUpdDsFlight = DEFAULT_DT;
        private static DataSet _dsDummy = null;
        private static String _st;
        private static MemoryStream _ms;

        public DataSet DsDummy
        {
            get { lock (_ObjDsLock) { return _dsDummy; } }
            set { lock (_ObjDsLock) { /*_dsDummy = value.Copy();*/ _dsDummy = value; } }
        }

        public String MySt
        {
            get { return _st; }
            set { _st = value; }
        }

        public MemoryStream MyMemSt
        {
            get{ return _ms; }
            set{ _ms = value; }
        }

        public EntDb()
        {
            _dsDummy = new DataSet();
            _dsDummy.RemotingFormat = SerializationFormat.Binary;
            _ms = new MemoryStream();
        }

        public int GetCnt()
        {
            return _cnt;
        }

        public void IncCnt()
        {
            //System.Threading.Thread.Sleep(10000);
            _cnt++;
        }

        public void DecCnt()
        {
            //System.Threading.Thread.Sleep(10000);
            _cnt--;
        }

        public DSFlight GetDSFlight(ref DSFlight dsFlight, ref DateTime lastUpdDateTime)
        {
            if (_dsFlight == null) throw new ApplicationException("Flight Information was not intialised yet.");
            if (_dtLUpdDsFlight.Equals(DEFAULT_DT)) throw new ApplicationException("Flight Information was not updated yet.");
            if (lastUpdDateTime.CompareTo(_dtLUpdDsFlight) < 0)
            {
                dsFlight = (DSFlight) _dsFlight.Copy();
            }
            if (dsFlight == null)
            {
                dsFlight = new DSFlight();
            }
            return dsFlight;
        }

        public void SetDSFlight(DSFlight dsFlight)
        {
            if (_dsFlight == null) _dsFlight = new DSFlight();
            else
            {
                _dsFlight.Clear();
                _dsFlight.AcceptChanges();
            }
            _dsFlight.Merge(dsFlight);
        }

        public void SetFlightSummXml(string flightSummXml)
        {
            if (flightSummXml == null) return;
            DSFlight dsFlight = new DSFlight();
            //System.Xml.XmlReader xmlReader = new System.Xml.XmlReader();
            
        }
    }
}
