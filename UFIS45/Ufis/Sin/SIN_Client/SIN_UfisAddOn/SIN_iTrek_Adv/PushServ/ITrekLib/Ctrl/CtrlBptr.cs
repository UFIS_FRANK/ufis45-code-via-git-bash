using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

using System.Data;

using UFIS.ITrekLib.DB;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.DSDB;
using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.Util;
using UFIS.ITrekLib.Misc;

namespace UFIS.ITrekLib.Ctrl
{
	public class CtrlBptr
	{
		public static CtrlBptr GetInstance()
		{
			return new CtrlBptr();
		}

		public DSBagPresentTimingRpt PopulateBagPresentTimingReport( string flightUrno)
		{
			DSBagPresentTimingRpt dsBPTR = new DSBagPresentTimingRpt();
			UtilDb db = UtilDb.GetInstance();
			IDbCommand cmd = null;
			bool commit = false;
			bool isNewCmd = false;//Is command 'cmd' created from this method?

			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = db.GetCommand(false);
				}

				dsBPTR = PopulateExistingData(cmd, flightUrno);

				//if (IsBagPresentTimingReportExist(cmd, flightUrno))
				//{
				//    //LogMsg("bptr exist");
				//    dsBPTR = RetrieveBagPresentTimingReport(cmd, flightUrno);
				//}
				//else
				//{
				//    //LogMsg("bptr not exist");
				//    dsBPTR = PopulateExistingData(cmd, flightUrno);
				//}
			}
			catch (Exception ex)
			{
				LogMsg("PopulateBagPresentTimingReport:Error:" + ex.Message);
				throw new ApplicationException("Fail to get the report information.");
			}
			finally
			{
				if (isNewCmd)
				{
					db.CloseCommand(cmd, commit);
				}
			}
			return dsBPTR;
		}

		private DSBagPresentTimingRpt PopulateExistingData(IDbCommand cmd, string flightUrno)
		{
			LogTraceMsg("PopulateExistingData:" + flightUrno);
			DSBagPresentTimingRpt dsBptr = new DSBagPresentTimingRpt();
			DSBagPresentTimingRpt.BPTRHDRRow bptrHdrRow = dsBptr.BPTRHDR.NewBPTRHDRRow();
			bptrHdrRow.URNO = flightUrno;			
			dsBptr.BPTRHDR.AddBPTRHDRRow(bptrHdrRow);

			PopulateFlightInfo(cmd, flightUrno, dsBptr);
			PopulateTripInfoForArrivalFlight(cmd, flightUrno, dsBptr);
			PopulateAORemarks(cmd, flightUrno, dsBptr);
			PopulateBptrInfo(cmd, flightUrno, dsBptr);

			dsBptr.AcceptChanges();
			return dsBptr;
		}

		private void PopulateAORemarks(IDbCommand cmd, string flightUrno, DSBagPresentTimingRpt dsBptr)
		{
			try
			{
				LogTraceMsg("PopulateAORemarks:" + flightUrno);
				if (CtrlAsr.IsConfirmedReportExist( cmd, flightUrno))
				{
					DSApronServiceRpt dsASR = CtrlAsr.RetrieveApronServiceReport(cmd, flightUrno);
					LogTraceMsg("PopulateAORemarks:" + flightUrno + ", asr exist");
					if ((dsASR != null) && (dsASR.ASR != null))
					{
						if (dsASR.ASR.Rows.Count > 0)
						{
							LogTraceMsg("PopulateAORemarks:" + flightUrno + ". Exist");
							bool createNewRow = false;
							DSBagPresentTimingRpt.BPTRHDRRow row = null;
							if (dsBptr.BPTRHDR.Rows.Count < 1)
							{
								row = dsBptr.BPTRHDR.NewBPTRHDRRow();
							}
							else
							{
								row = (DSBagPresentTimingRpt.BPTRHDRRow)dsBptr.BPTRHDR.Rows[0];
							}
							row.AO_TIME_RMK = ((DSApronServiceRpt.ASRRow)dsASR.ASR.Rows[0]).RSNBPT;
							LogTraceMsg("PopulateAORemarks:" + flightUrno + ",AO TIME REMK:" + row.AO_TIME_RMK);
							if (createNewRow)
							{
								dsBptr.BPTRHDR.AddBPTRHDRRow(row);
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				LogMsg("PopulateAORemarks:Error:" + flightUrno + ":" + ex.Message);
				throw new ApplicationException("Unable to get the AO Timing Remark.");
			}
		}

		/// <summary>
		/// Populate Trip Information for Arrival Flight in BPTRDET table.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="flightUrno">Flight Id</param>
		/// <param name="dsBptr"></param>
		private void PopulateTripInfoForArrivalFlight(IDbCommand cmd, string flightUrno, DSBagPresentTimingRpt dsBptr)
		{
			try
			{
				DSTrip dsTrip = CtrlTrip.RetrieveTrip(flightUrno);
				int cnt = 0;
				string aoUldRmk = "";
				const string NEWLINE = "\n\r";
				if (dsTrip != null)
				{
					foreach (DSTrip.TRIPRow tripRow in dsTrip.TRIP.Select("", "RCV_DT, ULDN"))
					{
						DSBagPresentTimingRpt.BPTRDETRow bptrDetRow = dsBptr.BPTRDET.NewBPTRDETRow();
						bptrDetRow.CLASS = tripRow.CLASS;
						bptrDetRow.SR_NO = Convert.ToString(++cnt);
						bptrDetRow.URNO = flightUrno;
						bptrDetRow.ULDNO = tripRow.ULDN;

						bptrDetRow.ULDTIME = tripRow.RCV_DT;
						if (tripRow.SENT_RMK != "")
						{
							aoUldRmk += tripRow.SENT_RMK + NEWLINE;
						}
						dsBptr.BPTRDET.AddBPTRDETRow(bptrDetRow);
					}
				}

				((DSBagPresentTimingRpt.BPTRHDRRow)(dsBptr.BPTRHDR.Rows[0])).AO_TRP_RMK = aoUldRmk;
			}
			catch (Exception ex)
			{
				LogMsg("PopulateTripInfoForArrivalFlight:Error:" + flightUrno + ":" + ex.Message);
				throw new ApplicationException("Unable to get the AO Trip remarks.");
			}
		}

		/// <summary>
		/// Populate the Flight Information for BPTR for given flight Id.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="flightUrno">Flight Id</param>
		/// <param name="dsBptr"></param>
		private void PopulateFlightInfo(IDbCommand cmd, string flightUrno, DSBagPresentTimingRpt dsBptr)
		{
			try
			{
				DSFlight dsFlight = CtrlFlight.RetrieveFlightsByFlightId(flightUrno);
				bool createNewRow = false;
				DSBagPresentTimingRpt.BPTRHDRRow bptrHdrRow = null;
				if (dsBptr.BPTRHDR.Rows.Count < 1)
				{
					bptrHdrRow = dsBptr.BPTRHDR.NewBPTRHDRRow();
				}
				else
				{
					bptrHdrRow = (DSBagPresentTimingRpt.BPTRHDRRow)dsBptr.BPTRHDR.Rows[0];
				}

				if (dsFlight.FLIGHT.Rows.Count > 0)
				{
					DSFlight.FLIGHTRow flightRow = (DSFlight.FLIGHTRow)dsFlight.FLIGHT.Rows[0];
					bptrHdrRow.ACFT_TYPE = flightRow.AIRCRAFT_TYPE;
					bptrHdrRow.AO_ID = flightRow.AOID;
					bptrHdrRow.AO_FNAME = flightRow.AOFNAME;
					//bptrHdrRow.AO_LNAME = flightRow.AO_LNAME;
					bptrHdrRow.AT = flightRow.AT;

					bptrHdrRow.BELT = flightRow.BELT;
					//bptrHdrRow.BO_ID = flightRow.bo
					bptrHdrRow.FBAG = flightRow.FST_BAG;
					bptrHdrRow.FBAG_TIME_MET = flightRow.BG_FST_BM_MET;
					bptrHdrRow.FLDT = flightRow.ST;
					bptrHdrRow.FLNU = flightRow.FLNO;
					bptrHdrRow.FTRIP_B = flightRow.BG_FST_TRP;
					bptrHdrRow.LBAG = flightRow.LST_BAG;
					bptrHdrRow.LBAG_TIME_MET = flightRow.BG_LST_BM_MET;
					bptrHdrRow.LTRIP_B = flightRow.BG_LST_TRP;
					bptrHdrRow.PARK_STAND = flightRow.PARK_STAND;
					bptrHdrRow.TBS_UP = flightRow.TBS_UP;
					bptrHdrRow.URNO = flightUrno;
				}

				if (createNewRow)
				{
					dsBptr.BPTRHDR.AddBPTRHDRRow(bptrHdrRow);
				}

			}
			catch (Exception ex)
			{
				LogMsg("PopulateFlightInfo:Error:" + flightUrno + ":" + ex.Message);
				throw new ApplicationException("Unable to get the flight informatin.");
			}
		}

		private void PopulateBptrInfo(IDbCommand cmd, string flightId, DSBagPresentTimingRpt dsBPTR)
		{
			DsDbBptr dsDb = DBDBptr.LoadDBBptr(cmd, flightId);
			if (dsDb == null) throw new ApplicationException("Unable to load BPTR info.");

			DsDbBptr.ITK_BPTRRow[] dbRows = (DsDbBptr.ITK_BPTRRow[])dsDb.ITK_BPTR.Select("URNO='" + flightId + "'");
			if ((dbRows == null) || (dbRows.Length < 1))
			{
				LogTraceMsg("No BPTR for " + flightId);
			}
			else
			{
				DSBagPresentTimingRpt.BPTRHDRRow[] dsRows = (DSBagPresentTimingRpt.BPTRHDRRow[])
					dsBPTR.BPTRHDR.Select("URNO='" + flightId + "'");
				if ((dsRows == null) || (dsRows.Length < 1)) throw new ApplicationException("No BPTR Header for " + flightId);
				dsRows[0].BO_ID = dbRows[0].BOID;
				dsRows[0].BO_FNAME = dbRows[0].BOFN;
				dsRows[0].BO_LNAME = dbRows[0].BOLN;
				dsRows[0].BO_RMK = dbRows[0].BORM;
			}
		}

		private DSBagPresentTimingRpt RetrieveBagPresentTimingReport(IDbCommand cmd, string flightUrNo)
		{
			DSBagPresentTimingRpt dsBptr = DBDBptr.GetInstance().RetrieveBagPresentTimingReportForAFlight(flightUrNo);
			if (dsBptr.BPTRHDR.Rows.Count > 0)
			{
				DSBagPresentTimingRpt.BPTRHDRRow hdrRow = (DSBagPresentTimingRpt.BPTRHDRRow)dsBptr.BPTRHDR.Rows[0];
				PopulateAORemarks(cmd, hdrRow.URNO, dsBptr);
			}

			return dsBptr;
		}

		/// <summary>
		/// Check whether baggage Presentation Timing Report Existed for given flight id.
		/// Return True - If existed.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="flightUrNo">Flight Id</param>
		/// <returns></returns>
		public bool IsBagPresentTimingReportExist(IDbCommand cmd, string flightUrNo)
		{
			bool exist = false;
			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			bool isNewCmd = false;//Is command 'cmd' created from this method?

			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = db.GetCommand(false);
				}

				exist = DBDBptr.GetInstance().IsBptrExist(cmd, flightUrNo);
			}
			catch (Exception ex)
			{
				LogMsg("IsBagPresentTimingReportExist:Err:" + ex.Message);
				throw;
			}
			finally
			{
				if (isNewCmd)
				{
					db.CloseCommand(cmd, commit);
				}
			}

			return exist;
		}

		private static DBDBptr dbBptr = DBDBptr.GetInstance();

		/// <summary>
		/// Save Bptr Report for give flight Id.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="flightUrno">Flight Id</param>
		/// <param name="boRemark">BO Remark</param>
		/// <param name="user">User who update this BPTR report 
		/// (Assume it is BO who handle this flight, if BPTR is not existed)</param>
		public void SaveReport( string flightUrno, string boRemark, EntUser user)
		{
			IDbCommand cmd = null;
			DSBagPresentTimingRpt dsBptr = PopulateBagPresentTimingReport(flightUrno);
			DSBagPresentTimingRpt.BPTRHDRRow hdrRow = ((DSBagPresentTimingRpt.BPTRHDRRow)(dsBptr.BPTRHDR.Rows[0]));
			if (hdrRow.BO_ID.Trim() == "")
			{
				hdrRow.BO_ID = user.UserId;
				hdrRow.BO_FNAME = user.FirstName;
				hdrRow.BO_LNAME = user.LastName;
			}
			hdrRow.BO_RMK = boRemark;

			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			bool isNewCmd = false;//Is command 'cmd' created from this method?

			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = db.GetCommand(true);
				}

				dbBptr.SaveRpt(cmd, flightUrno, dsBptr,
				   user.UserId, ITrekTime.GetCurrentDateTime());

				commit = true;
			}
			catch (Exception ex)
			{
				LogMsg("SaveReport:Err:" + ex.Message);
				throw;
			}
			finally
			{
				if (isNewCmd)
				{
					//db.CloseCommand(cmd, commit);
					CtrlFlight.CloseCmdAndRefreshFlightAfterUpdFlightInfo(cmd, commit, flightUrno);
				}
			}
		}

		public static DataTable RetrieveBptrSumm(List<string> terminalIds, string frDateTime, string toDateTime, string userId, 
			out ENT.BPTRSumm.EntBptrSummCond condUse)
		{
			condUse = null;
			IDbCommand cmd = null;
			UtilDb udb = UtilDb.GetInstance();
			bool commit = false;
			bool isNewCmd = false;//Is command 'cmd' created from this method?

			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = udb.GetCommand(true);
				}

				ENT.BPTRSumm.EntBptrSumm summ = null;
				summ = DBDBptr.RetrieveBptrSummary(cmd,
					terminalIds, frDateTime, toDateTime, userId, out condUse);

				if (summ == null)
				{
					throw new ApplicationException("Fail to generate the Summary Report. Null");
				}

				#region prepare datatable 
				DataTable dtRpt = new DataTable();
				dtRpt.Columns.Add("REPORT STATISTIC");
				dtRpt.Columns.Add("NUMBER");
				dtRpt.Columns.Add("PERCENTAGE");
				dtRpt.Rows.Add("Total Flights", summ.FlightCount, "N/A");
				dtRpt.Rows.Add("Number of Light Loader", summ.LightLoaderFlightCount, "N/A");
				dtRpt.Rows.Add("Number of Heavy Loader", summ.HeavyLoaderFlightCount, "N/A");
				AddAnRow(dtRpt, "Absolute First Bags Meeting BPT",
					summ.FirstBagBMMetFlightCount, summ.FlightCount);
				AddAnRow(dtRpt, "Absolute Last Bags Meeting BPT - Light Loader",
					summ.LastBagBMMetLightLoaderFlightCount, summ.LightLoaderFlightCount);
				AddAnRow(dtRpt, "Absolute Last Bags Meeting BPT - Heavy Loader",
					summ.LastBagBMMetHeavyLoaderFlightCount, summ.HeavyLoaderFlightCount);
				AddAnRow(dtRpt, "Absolute Last Bags Meeting BPT - Total",
					(summ.LastBagBMMetHeavyLoaderFlightCount + summ.LastBagBMMetLightLoaderFlightCount)
				, summ.FlightCount);
				return dtRpt;
				#endregion
			}
			catch (Exception ex)
			{
				LogMsg("RetrieveBptrSumm:Err:" + ex.Message);
				throw new ApplicationException("Fail to generate the Summary Report.");
			}
			finally
			{
				if (isNewCmd)
				{
					udb.CloseCommand(cmd, commit);
				}
			}
		}

		private static void AddAnRow(DataTable dt, string lbl, Int64 num, Int64 totNum)
		{
			if (totNum > 0)
			{
				dt.Rows.Add(lbl, num, string.Format("{0:#0.00}%", num * 100 / totNum));
			}
			else
			{
				dt.Rows.Add(lbl, num, "-");
			}
		}

		public static void HkReport(ArrayList arrList)
		{
			DBDBptr.GetInstance().HkReport(arrList);
		}

		//public static DSBagPresentTimingRpt GetBagPresentTimingRpt_Test(string flightId)
		//{
		//    DSBagPresentTimingRpt ds = new DSBagPresentTimingRpt();
		//    IDbCommand cmd = null;
		//    UtilDb db = UtilDb.GetInstance();
		//    bool commit = false;
		//    bool isNewCmd = false;//Is command 'cmd' created from this method?

		//    try
		//    {
		//        if (cmd == null)
		//        {
		//            isNewCmd = true;
		//            cmd = db.GetCommand(false);
		//        }

		//        dbBptr.DBLoadDsBptrHdr(cmd, flightId, ds);

		//        //commit = true;
		//    }
		//    catch (Exception ex)
		//    {
		//        LogMsg("UpdateMessageReceivedDetails:Err:" + ex.Message);
		//        throw;
		//    }
		//    finally
		//    {
		//        if (isNewCmd)
		//        {
		//            db.CloseCommand(cmd, commit);
		//        }
		//    }
		//    return ds;
		//}

		#region Logging
		private static void LogTraceMsg(string msg)
		{
			LogMsg(msg);
		}

		private static void LogMsg(string msg)
		{
			try
			{
				MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("CtrlBptr", msg);
			}
			catch (Exception)
			{
				UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("CtrlBptr", msg);
			}
		}
		#endregion
	}
}