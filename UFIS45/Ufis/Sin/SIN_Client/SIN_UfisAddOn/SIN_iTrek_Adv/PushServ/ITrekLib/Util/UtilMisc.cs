using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Management;

using System.Collections;

namespace UFIS.ITrekLib.Util
{
    public class UtilMisc
    {
        /// <summary>
        /// Get the Selection String for a given data in string in hashtable key
        /// </summary>
        /// <param name="htDataInSt">Data Hashtable in Key as string</param>
        /// <param name="noOfCntInASelection">Number of Count to put in each selection string between 10 and 200</param>
        /// <returns>string array of selection string (e.g. [0] ==> '23453','345345','456546'; [1] ==> '62345','74613','63245'</returns>
        public static string[] GetDataForSelection(Hashtable htDataInSt, int noOfCntInASelection)
        {
            string[] stArr = null;

            if (noOfCntInASelection < 10) noOfCntInASelection = 10;
            else if (noOfCntInASelection > 200) noOfCntInASelection = 200;
            int cntArr = 1;
            if (htDataInSt != null)
            {
                cntArr = (int)( Math.Ceiling((decimal)htDataInSt.Count / (decimal)noOfCntInASelection));
                if (cntArr < 1) cntArr = 1;
                stArr = new string[cntArr];
                if (htDataInSt != null)
                {
                    StringBuilder sb = new StringBuilder();
                    int cnt = 0;
                    int arrIdx = 0;
                    foreach (DictionaryEntry de in htDataInSt.Keys)
                    {
                        string id = (string)de.Key;
                        if (cnt > 0) sb.Append(",");
                        sb.Append("'" + id + "'");
                        cnt++;
                        if (cnt == noOfCntInASelection)
                        {
                            stArr[arrIdx] = sb.ToString();
                            cnt = 0;
                            arrIdx++;
                            sb = new StringBuilder();
                        }
                    }

                    if (cnt > 0) stArr[arrIdx] = sb.ToString();
                }
            }

            return stArr;
        }

        const char FILE_SEP = '\\';

        /// <summary>
        /// Get File Name from given file name consists of paths
        /// e.g.
        ///    fullFileName ==> C:\TMP\TEST.TXT
        ///    return       ==> TEXT.TXT
        /// </summary>
        /// <param name="fullFileName">File Name with full path info</param>
        /// <returns>File Name only</returns>
        public static string GetFileNameOnly(string fullFileName)
        {
            string fName = "";
            if (!string.IsNullOrEmpty(fullFileName))
            {
                //char[] fileSep = { FILE_SEP };
                //string[] stArr = fullFileName.Split(fileSep);
                //int cnt = stArr.Length - 1;
                //if (cnt >= 0)
                //{
                //    fName = stArr[cnt];
                //}
                fName = System.IO.Path.GetFileName(fullFileName);
            }
            return fName;
        }

        /// <summary>
        /// Compose full file name consists with path using prefix and susfix
        /// e.g.
        ///     path       ==> C:\TMP\
        ///     filePrefix ==> PRE_
        ///     fileName   ==> TEST.TXT
        ///     fileSuffix ==> _SUF
        ///     result     ==> C:\TMP\PRE_TEST_SUF.TXT
        /// </summary>
        /// <param name="path">File Path</param>
        /// <param name="filePrefix">Prefix of the file</param>
        /// <param name="fileName">File Name</param>
        /// <param name="fileSuffix">Suffix for the file</param>
        /// <returns>Full File Name</returns>
        public static string GetFullFileName(string path, string filePrefix, string fileName, string fileSuffix)
        {
            return Path.Combine( path,
                filePrefix + 
                Path.GetFileNameWithoutExtension( fileName ) + 
                fileSuffix + 
                Path.GetExtension( fileName ));
        }

        /// <summary>
        /// Combine the path and file name
        /// </summary>
        /// <param name="path">File Path</param>
        /// <param name="fileName">File Name</param>
        /// <returns>Full File Name</returns>
        public static string Combine(string path, string fileName)
        {
            return System.IO.Path.Combine(path, fileName);
        }

        /// <summary>
        /// Convert string arraylist into delimited string
        /// </summary>
        /// <param name="arr">arraylist string</param>
        /// <param name="delimiter">delimiter for result string (e.g. ',')</param>
        /// <returns>delimited list of string (e.g. a,e,b,n)</returns>
        public static string ConvArrListToString(ArrayList arr, string delimiter)
        {
            StringBuilder result = new StringBuilder();
            if ((arr != null) && (arr.Count > 0))
            {
                int cnt = arr.Count;
                result.Append((string)arr[0]);
                for (int i = 1; i < cnt; i++)
                {
                    result.Append(delimiter + (string)arr[i]);
                }
            }
            return result.ToString();
        }

        /// <summary>
        /// Convert Delimited String to Array List
        /// </summary>
        /// <param name="delimitedString">Delimited String (e.g. a,e,b,n)</param>
        /// <param name="delimiter">Delimiter of the string (e.g. ',')</param>
        /// <returns>ArrayList</returns>
        public static ArrayList ConvDelimitedStringToArrayList(string delimitedString, char delimiter)
        {
            ArrayList arr = new ArrayList();
            char[] del = { delimiter };
            string[] stArr = delimitedString.Split(del);
            int cnt = stArr.Length;
            for (int i = 0; i < cnt; i++)
            {
                arr.Add(stArr[i]);
            }
            return arr;
        }

        /// <summary>
        /// Convert ArrayList into hashtable
        /// </summary>
        /// <param name="arr"></param>
        /// <returns></returns>
        public static Hashtable ConvArrayListToHashTable(ArrayList arr)
        {
            Hashtable ht = new Hashtable();
            if (arr != null)
            {
                int cnt = arr.Count;
                for (int i = 0; i < cnt; i++)
                {
                    try
                    {
                        if (!ht.ContainsKey(arr[i]))
                        {
                            ht.Add(arr[i], arr[i]);
                        }
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            return ht;
        }

        /// <summary>
        /// Convert ArrayList into generic list of string
        /// </summary>
        /// <param name="arr"></param>
        /// <returns></returns>
        public static List<string> ConvArrayListToStringList(ArrayList arr)
        {
            List<string> result = null;
            if (arr != null)
            {
                result = new List<string>();
                int cnt = arr.Count;
                for (int i = 0; i < cnt; i++)
                {
                    try
                    {
                        result.Add((string)arr[i]);
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Get the Drive Info (size and freespace) for given drive
        /// </summary>
        /// <param name="driveName">Drive Name (e.g. C)</param>
        /// <param name="size">Size of the drive</param>
        /// <param name="freeSpace">Free space of the drive</param>
        public static void DriveInfo(string driveName, out string size, out string freeSpace)
        {
            size = "-1";
            freeSpace = "-1";
            try
            {
                ManagementObject disk = new ManagementObject("win32_logicaldisk.deviceid='" + driveName + ":'");
                disk.Get();
                size = disk["Size"].ToString();
                freeSpace = disk["FreeSpace"].ToString();
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Convert string to byte array
        /// </summary>
        /// <param name="str">string to convert to byte array</param>
        /// <returns>byte array</returns>
        public static byte[] ConvertStrToByteArray(string str)
        {
            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            return encoding.GetBytes(str);
        }

        /// <summary>
        /// Convert Byte Array to String
        /// </summary>
        /// <param name="arr">Byte Array</param>
        /// <returns>string</returns>
        public static string ConvertByteArrayToStr(byte[] arr)
        {
            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            return encoding.GetString(arr);
        }
    }
}
