using System;
using System.Collections.Generic;
using System.Text;

using System.Collections;
using System.Data;

using UFIS.ITrekLib.Util;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.DB;
using UFIS.ITrekLib.ENT;

namespace UFIS.ITrekLib.Ctrl
{
   public class CtrlDAsr
   {
      public static DSApronServiceRpt PopulateApronServiceReport(string flightUrno)
      {
         DSApronServiceRpt ds = null;
         bool isDraft = true;
         if (IsReportExist(flightUrno, out isDraft))
         {
            ds = RetrieveApronServiceReport(flightUrno);
         }
         else
         {
            ds = PopulateData(flightUrno);
         }

         return ds;
      }

      private static DBDAsr _dbAsr = DBDAsr.GetInstance();


      private static bool IsReportExist(string flightUrno, out bool isDraft)
      {        
         return _dbAsr.IsApronServiceRptExist(flightUrno, out isDraft);
      }

      public static bool IsConfirmedReportExist(string flightUrno)
      {
         bool isDraft = true;
         bool exist = _dbAsr.IsApronServiceRptExist(flightUrno, out isDraft);
         if (exist == true)
         {
            if (isDraft == true) { exist = false; }
         }
         return exist;
      }

      public static bool IsDraft(string flightUrno)
      {
         return _dbAsr.IsApronServiceRptExist(flightUrno);
      }

      public static DSApronServiceRpt PopulateData(string flightUrno)
      {
         //Populate data from Flight
         DSApronServiceRpt dsASR = new DSApronServiceRpt();
         DSFlight dsFlight = CtrlFlight.RetrieveFlightsByFlightId(flightUrno);
         string stTURN = "";
         string stADID = "";
         DSApronServiceRpt.ASRRow row = dsASR.ASR.NewASRRow();
         if (dsFlight.FLIGHT.Rows.Count > 0)
         {
            DSFlight.FLIGHTRow fRow = (DSFlight.FLIGHTRow)(dsFlight.FLIGHT.Rows[0]);
            stTURN = fRow.TURN;
            stADID = fRow.A_D;
            if (stTURN == null) stTURN = "";
            else stTURN = stTURN.Trim();

            row.UAFT = fRow.URNO;
            row.AD = fRow.A_D;
            row.ACFTREGN = fRow.REGN;
            row.ACFTTYPE = fRow.AIRCRAFT_TYPE;

            row.AOID = fRow.AOID;
            row.AOFNAME = fRow.AOFNAME;
            row.AOLNAME = fRow.AOLNAME;
            row.FLNO = fRow.FLNO;
            row.FR = fRow.FR;
            row.TO = fRow.TO;

            row.BAY = fRow.BAY;

            row.FTRIP = fRow.AP_FST_TRP;
            row.LTRIP = fRow.AP_LST_TRP;
            row.UNLEND = fRow.UNL_END;
            row.LDSTRT = fRow.LD_ST;
            row.LDEND = fRow.LD_END;
            row.LSTDOR = fRow.LST_DOOR;
            row.PLB = fRow.PLB;
            row.ST = fRow.ST;
            row.AT = fRow.AT;
            if (stADID == "A")
            {//Arrival Flight
               row.STA = fRow.ST;
               row.ATA = fRow.AT;
               row.STD = "";
               row.ATD = "";
            }
            else if (stADID == "D")
            {//Departure Flight
               row.STA = "";
               row.ATA = "";
               row.STD = fRow.ST;
               row.ATD = fRow.AT;
            }

            if (stTURN == "")
            {//Not a Transit Flight
               //Populate ULD Comments
               row.ULDCMT = CtrlTrip.GetArrFlightUldAOComments(flightUrno);
            }
            else
            {//Transit Flight
               //Populate ULD Comments
               row.ULDCMT = CtrlTrip.GetArrFlightUldAOComments(flightUrno) +
                   "\n" + CtrlTrip.GetArrFlightUldAOComments(stTURN);
               row.TURN = stTURN;
               DSFlight dsFlightTransit = CtrlFlight.RetrieveFlightsByFlightId(stTURN);
               if (dsFlightTransit.FLIGHT.Rows.Count > 0)
               {
                  DSFlight.FLIGHTRow fRowTransit = (DSFlight.FLIGHTRow)(dsFlightTransit.FLIGHT.Rows[0]);
                  string stADIDTransit = fRowTransit.A_D;
                  if (stADIDTransit == "A")
                  {
                     row.FR = fRowTransit.FR;
                     row.FTRIP = fRowTransit.AP_FST_TRP;
                     row.LTRIP = fRowTransit.AP_LST_TRP;
                     row.UNLEND = fRowTransit.UNL_END;

                     row.ST = fRowTransit.ST;
                     row.STA = fRowTransit.ST;
                     row.ATA = fRowTransit.AT;
                  }
                  else if (stADIDTransit == "D")
                  {
                     row.TO = fRowTransit.TO;
                     row.LDSTRT = fRowTransit.LD_ST;
                     row.LDEND = fRowTransit.LD_END;
                     row.LSTDOR = fRowTransit.LST_DOOR;
                     row.PLB = fRowTransit.PLB;

                     row.STD = fRowTransit.ST;
                     row.ATD = fRowTransit.AT;
                  }
               }
            }
         }

         dsASR.ASR.AddASRRow(row);

         return dsASR;
      }

      //public static DSApronServiceRpt PopulateData(string flightUrno, DSApronServiceRpt dsASR)
      //{
      //    //Populate data from Flight
      //    DSFlight dsFlight = CtrlFlight.RetrieveFlightsByFlightId(flightUrno);
      //    string stTURN = "";
      //    string stADID = "";
      //    bool isNew = false;
      //    DSApronServiceRpt.ASRRow row = null;

      //    row = (DSApronServiceRpt.ASRRow)dsASR.ASR.Select("UAFT='" + flightUrno + "'")[0];
      //    if (row == null)
      //    {
      //        isNew = true;
      //        row = dsASR.ASR.NewASRRow();
      //    }
      //    if (dsFlight.FLIGHT.Rows.Count > 0)
      //    {
      //        DSFlight.FLIGHTRow fRow = (DSFlight.FLIGHTRow)(dsFlight.FLIGHT.Rows[0]);
      //        stTURN = fRow.TURN;
      //        stADID = fRow.A_D;
      //        row.UAFT = fRow.URNO;
      //        row.AD = fRow.A_D;
      //        row.ACFTREGN = fRow.REGN;
      //        row.ACFTTYPE = fRow.AIRCRAFT_TYPE;
      //        row.AOID = fRow.AOID;
      //        row.AOFNAME = fRow.AOFNAME;
      //        row.AOLNAME = fRow.AOLNAME;
      //        row.FLNO = fRow.FLNO;
      //        row.FR = fRow.FR;
      //        row.TO = fRow.TO;
      //        row.ST = fRow.ST;
      //        row.AT = fRow.AT;
      //        row.BAY = fRow.BAY;

      //        row.FTRIP = fRow.AP_FST_TRP;
      //        row.LTRIP = fRow.AP_LST_TRP;
      //        row.UNLEND = fRow.UNL_END;
      //        row.LDSTRT = fRow.LD_ST;
      //        row.LDEND = fRow.LD_END;
      //        row.LSTDOR = fRow.LST_DOOR;
      //        row.PLB = fRow.PLB;
      //    }

      //    //Populate ULD Comments
      //    if (stADID == "D")
      //    {//Departure flight of transit, use arrival flight to get the comment
      //        row.ULDCMT = CtrlTrip.GetArrFlightUldAOComments(stTURN);
      //    }
      //    else
      //    {
      //        row.ULDCMT = CtrlTrip.GetArrFlightUldAOComments(flightUrno);
      //    }
      //    if (isNew)
      //    {
      //        dsASR.ASR.AddASRRow(row);
      //    }
      //    return dsASR;
      //}


      public static DSApronServiceRpt RetrieveApronServiceReport(string flightUrno)
      {
         DSApronServiceRpt dsASR = new DSApronServiceRpt();
         dsASR = _dbAsr.RetrieveApronServiceReportForAFlight(flightUrno);
         return dsASR;
	 }

	 
	 public static void SaveApronServiceReport(IDbCommand cmd, string flightUrno, DSApronServiceRpt dsASR, EntUser curUser)
      {
         try
         {
            string stTrsitFltUrno = "";
            stTrsitFltUrno = CtrlFlight.RetrieveRelatedTransitFlightUrnoForFlight(cmd, flightUrno);
            if ((stTrsitFltUrno == null) || (stTrsitFltUrno == ""))
            {
               _dbAsr.SaveRpt(flightUrno, dsASR, curUser.UserId);
               if (IsConfirmedReportExist(flightUrno))
			   {
#warning //TODO - To Update the Baggage Presentation Timing Report Exist
				   //CtrlFlight.GetInstance().UpdateApronServiceReportExist(flightUrno, true);
               }
            }
            else
            {
               SaveRptForRelatedTransitFlight(flightUrno, stTrsitFltUrno, dsASR, curUser);
            }
         }
         catch (Exception ex)
         {
            LogMsg("SaveApronServiceReport:Err:" + ex.Message);
            throw new ApplicationException("Fail to save");
         }
      }

      public static void SaveRptForRelatedTransitFlight(string flightUrno,
          string transitFltUrno, DSApronServiceRpt dsASR, EntUser curUser)
      {
         try
         {
            if ((transitFltUrno != null) && (transitFltUrno != ""))
            {
               LogTraceMsg("Turn:" + flightUrno + "," + transitFltUrno);
               DSApronServiceRpt dsTemp = PopulateApronServiceReport(transitFltUrno);
               if (dsTemp != null)
               {
                  foreach (DSApronServiceRpt.ASRRow row in dsTemp.ASR.Select("UAFT='" + transitFltUrno + "'"))
                  {
                     //LogTraceMsg("has info for turn" + dsASR.ASR.Rows.Count);
                     DSApronServiceRpt dsNew = (DSApronServiceRpt)dsASR.Copy();
                     DSApronServiceRpt.ASRRow newRow = (DSApronServiceRpt.ASRRow)dsNew.ASR.Rows[0];
                     //LogTraceMsg("before assign," + newRow.UAFT + "<==" + row.UAFT);
                     newRow.UAFT = row.UAFT;
                     if (row.AD == "A")
                     {
                        newRow.AD = "D";
                     }
                     else if (row.AD == "D")
                     {
                        newRow.AD = "A";
                     }

                     newRow.ACFTREGN = row.ACFTREGN;
                     newRow.ACFTTYPE = row.ACFTTYPE;
                     newRow.AOID = row.AOID;
                     newRow.AOFNAME = row.AOFNAME;
                     newRow.AOLNAME = row.AOLNAME;
                     newRow.FLNO = row.FLNO;
                     newRow.FR = row.FR;
                     newRow.TO = row.TO;
                     newRow.STA = row.STA;
                     newRow.ATA = row.ATA;
                     newRow.STD = row.STD;
                     newRow.ATD = row.ATD;
                     newRow.BAY = row.BAY;

                     newRow.FTRIP = row.FTRIP;
                     newRow.LTRIP = row.LTRIP;
                     newRow.UNLEND = row.UNLEND;
                     newRow.LDSTRT = row.LDSTRT;
                     newRow.LDEND = row.LDEND;
                     newRow.LSTDOR = row.LSTDOR;
                     newRow.PLB = row.PLB;
                     newRow.TURN = flightUrno;
                     //LogTraceMsg("after assign");
                     foreach (DSApronServiceRpt.ASRRow tRow in dsASR.ASR.Select("UAFT='" + transitFltUrno + "'"))
                     {
                        tRow.Delete();
                     }
                     //LogTraceMsg("after del");
                     dsASR.ASR.ImportRow(newRow);
                     //dsASR.ASR.Rows.Add(newRow);
                     //LogTraceMsg("after import," + dsASR.ASR.Rows.Count);
                     break;
                  }
               }
            }
            if ((transitFltUrno == null) || (transitFltUrno == ""))
            {
               _dbAsr.SaveRpt(flightUrno, dsASR, curUser.UserId);
            }
            else
            {
               _dbAsr.SaveRpt(flightUrno, transitFltUrno, dsASR, curUser.UserId);
            }
            if (IsConfirmedReportExist(flightUrno))
			{
#warning //TODO - To Update the Baggage Presentation Timing Report Exist
				//CtrlFlight.GetInstance().UpdateApronServiceReportExist(flightUrno, true);
            }
            if (IsConfirmedReportExist(transitFltUrno))
			{
#warning //TODO - To Update the Baggage Presentation Timing Report Exist
				//CtrlFlight.GetInstance().UpdateApronServiceReportExist(transitFltUrno, true);
            }
         }
         catch (Exception ex)
         {
            LogMsg("SaveRptForRelatedTransitFlight:Err:" + ex.Message);
            throw new ApplicationException("Fail to Save.");
         }
      }

      public static void HkReport(ArrayList arrList)
      {
         _dbAsr.HkReport(arrList);
      }

      private static void LogMsg(string msg)
      {
         Util.UtilLog.LogToGenericEventLog("CtrlASR", msg);
      }

      private static void LogTraceMsg(string msg)
      {
         Util.UtilLog.LogToTraceFile("CtrlASR", msg);
      }
   }
}
