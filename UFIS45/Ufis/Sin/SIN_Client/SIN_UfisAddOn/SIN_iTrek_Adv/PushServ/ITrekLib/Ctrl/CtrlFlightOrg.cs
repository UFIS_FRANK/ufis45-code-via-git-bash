using System;
using System.Collections.Generic;
using System.Text;

using System.Data;
using System.Xml;
using System.Xml.XPath;

using MM.UtilLib;

namespace UFIS.ITrekLib.Ctrl
{
	/// <summary>
	/// Controller Class for Processing (updating to iTrek database) the Original Flight Information (coming from back-end)
	/// </summary>
	public class CtrlFlightOrg
	{
		private static object _objLock = new object();
		private static CtrlFlightOrg _this = null;

		private CtrlFlightOrg() { }

		/// <summary>
		/// Get the instance of CtrlFlightOrg.
		/// </summary>
		/// <returns></returns>
		public static CtrlFlightOrg GetInstance()
		{
			if (_this == null)
			{
				lock (_objLock)
				{
					if (_this == null)
					{
						_this = new CtrlFlightOrg();
					}
				}
			}
			return _this;
		}

		/// <summary>
		/// Process (Update to iTrek DB) Original Flight Info Message.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="msg">Message getting from MQ</param>
		/// <param name="userId">user id who update this information in iTrek</param>
		/// <returns></returns>
		public void ProcessOrgFlightInfo(IDbCommand cmd, string msg, string userId, out string flightMsgType, out string msgWoCmd, out string flId)
		{
			//if (cmd == null) throw new ApplicationException("Null Command.");
			msgWoCmd = "";
			flightMsgType = "";
			flId = "";
			lock (_objLock)
			{
				string uaft = "";
				string flightId = "";
				bool hasData = false;
				string adid = "";

				flightMsgType = GetType(msg, out msgWoCmd, out uaft);
				switch (flightMsgType)
				{
					case "FLIGHTS":
						CtrlFlight.UpdateOriginalFlightInfo(cmd, UtilDataSet.LoadDataFromXmlString(msgWoCmd), userId);
						break;
					case "INSERT":
						CtrlFlight.UpdateOriginalFlightInfoIns(cmd, msgWoCmd, userId);
						break;
					case "UPDATE":
						CtrlFlight.UpdateOriginalFlightInfoUpd(cmd, msgWoCmd, userId, out hasData, out flId);
						if (!hasData)
						{
							LogTraceMsg("ProcessOrgFlightInfo:U:No Data:" + uaft);
						}
						break;
					case "DELETE":
						flightId = CtrlFlight.GetFlightIdForUaft(cmd, uaft, "", out hasData, out adid);
						if (hasData)
						{
							CtrlFlight.UpdateOriginalFlightInfoDel(cmd, flightId, userId);
						}
						else
						{
							LogTraceMsg("ProcessOrgFlightInfo:D:No Data:" + uaft);
						}
						break;
					default:
						throw new ApplicationException("Unknown Message Type." + flightMsgType);
					//break;
				}
			}
		}

		private string GetType(string stXml, out string msgWoCmd, out string uaft)
		{
			msgWoCmd = stXml;
			uaft = "";
			string action = "";
			try
			{
				XmlDocument xmldoc = new XmlDocument();
				xmldoc.LoadXml(stXml);

				if (xmldoc.DocumentElement.Name == "FLIGHTS")
				{
					action = "FLIGHTS";
				}
				else
				{
					XmlNode xmlNode = xmldoc.SelectSingleNode("//FLIGHT/ACTION");
					action = xmlNode.InnerXml.Trim().ToUpper();
					xmldoc.DocumentElement.RemoveChild(xmlNode);
					msgWoCmd = xmldoc.OuterXml;
					uaft = xmldoc.SelectSingleNode("//FLIGHT/URNO").InnerXml;
				}
			}
			catch (Exception)
			{
				throw;
			}
			return action;
		}

		#region Logging
		private static void LogTraceMsg(string msg)
		{
			try
			{
				MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("CtrlFlight", msg);
			}
			catch (Exception)
			{
				UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("CtrlFlight", msg);
			}
		}

		private static void LogMsg(string msg)
		{
			//UFIS.ITrekLib.Util.UtilLog.LogToGenericEventLog("CtrlFlight", msg);
			try
			{
				MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("CtrlFlight", msg);
			}
			catch (Exception)
			{
				UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("CtrlFlight", msg);
			}
		}
		#endregion
	}
}