using System;
using System.Collections.Generic;
using System.Text;

using System.Data;

using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.DB;
using iTrekXML;


namespace UFIS.ITrekLib.Ctrl
{
    public class CtrlPreDefMsg
    {

        public static void Init()
        {
            DBPreDefMsg.GetInstance().Init();
        }

        //private DSPreDefMsg dsPredefMsg
        //{
        //    get
        //    {
        //        LoadPredefinedMsgToDs();
        //        return _dsPredefMsg;
        //    }
        //}

        //private string GetMsg(string msgUrNo)
        //{
        //    string msg = "";
        //    msg = ((DSPreDefMsg.PREDEFMSGRow)dsPredefMsg.PREDEFMSG.FindByURNO(msgUrNo)).MSGTEXT;
        //    return msg;
        //}

        //private string GetUrNoForAMsg(string msg)
        //{
        //    string urNo = "";
        //    urNo = ((DSPreDefMsg.PREDEFMSGRow[])dsPredefMsg.PREDEFMSG.Select("MSGTEXT='" + msg + "'"))[0].URNO;
        //    return urNo;
        //}

        //private string GetPreDefMsgFilePath()
        //{
        //    if ((_preDefMsgXmlPath == null) || (_preDefMsgXmlPath == ""))
        //    {
        //        iTrekXML.iTXMLPathscl path = new iTXMLPathscl();

        //        _preDefMsgXmlPath = path.GetPredefinedMsgFilePath();
        //        //string st = path.GetJobsFilePath();
        //    }

        //    return _preDefMsgXmlPath;
        //}

        //private void LoadPredefinedMsgToDs()
        //{
        //    if (_dsPredefMsg == null)
        //    {
        //        lock (lockObj)
        //        {
        //            if (_dsPredefMsg == null)
        //            {
        //                //PREDEFMSGTableAdapter ta = new PREDEFMSGTableAdapter();
        //                //ta.Fill(ds.PREDEFMSG);
        //                try
        //                {
        //                    _dsPredefMsg = new DSPreDefMsg();
        //                    _dsPredefMsg.ReadXml(GetPreDefMsgFilePath());
        //                }
        //                catch (Exception)
        //                {
        //                    _dsPredefMsg.WriteXml(GetPreDefMsgFilePath());
        //                    _dsPredefMsg.WriteXmlSchema(GetPreDefMsgFilePath());
        //                }
        //            }
        //        }
        //    }
        //}

        //public bool IsExistingMsg(string msg)
        //{
        //    return (dsPredefMsg.PREDEFMSG.Select("MSGTEXT='" + msg + "'").Length) > 0;
        //}

        //private bool IsValidStatus(string stat)
        //{
        //    bool validStat = false;
        //    if ((stat == "A") || (stat == "I")) validStat = true;
        //    return validStat;
        //}

        //private void UpdatePredefinedMsg(string urNo, string msg, string stat)
        //{
        //    if (!IsValidStatus(stat))
        //    {
        //        throw new ApplicationException("Invalid Predefined Message status " + stat);
        //    }

        //    DSPreDefMsg.PREDEFMSGRow row = dsPredefMsg.PREDEFMSG.FindByURNO(urNo);
        //    row.MSGTEXT = msg;
        //    row.STAT = stat;
        //    dsPredefMsg.PREDEFMSG.AcceptChanges();
        //    dsPredefMsg.PREDEFMSG.WriteXml(GetPreDefMsgFilePath(), XmlWriteMode.IgnoreSchema, false);
        //}

        //public static void UpdatePredefinedMsg(string urNo, string msg)
        //{
            //CtrlPreDefMsg ctrl = new CtrlPreDefMsg();
            //DSPreDefMsg.PREDEFMSGRow row = ctrl.dsPredefMsg.PREDEFMSG.FindByURNO(urNo);
            //row.MSGTEXT = msg;
            //ctrl.dsPredefMsg.PREDEFMSG.AcceptChanges();
            //ctrl.dsPredefMsg.PREDEFMSG.WriteXml(ctrl.GetPreDefMsgFilePath(), XmlWriteMode.IgnoreSchema, false);
        //}

        //public static void CreatePredefinedMsg(string msg)
        //{
        //    CtrlPreDefMsg ctrl = new CtrlPreDefMsg();
        //    if (ctrl.IsExistingMsg(msg))
        //    {
        //        string urNo = ctrl.GetUrNoForAMsg(msg);

        //        ctrl.UpdatePredefinedMsg(urNo, msg, "A");
        //        throw new ApplicationException("Duplicate Message!");
        //    }
        //    else
        //    {
        //        string urNo = Convert.ToString(ctrl.dsPredefMsg.PREDEFMSG.Rows.Count + 1);

        //        DSPreDefMsg.PREDEFMSGRow row = (DSPreDefMsg.PREDEFMSGRow) ctrl.dsPredefMsg.PREDEFMSG.NewRow();
        //        row.MSGTEXT = msg;
        //        row.URNO = urNo;
        //        ctrl.dsPredefMsg.PREDEFMSG.AddPREDEFMSGRow(row);
        //        ctrl.dsPredefMsg.PREDEFMSG.AcceptChanges();
        //        ctrl.dsPredefMsg.PREDEFMSG.WriteXml(ctrl.GetPreDefMsgFilePath(), XmlWriteMode.IgnoreSchema, false);
        //    }
        //}

        //public static void DeletePredefinedMsg(string msg)
        //{
        //    try
        //    {
        //        CtrlPreDefMsg ctrl = new CtrlPreDefMsg();

        //        DSPreDefMsg.PREDEFMSGRow row = ctrl.dsPredefMsg.PREDEFMSG.FindByURNO(ctrl.GetUrNoForAMsg(msg));
        //        row.STAT = "I";
        //        ctrl.dsPredefMsg.PREDEFMSG.AcceptChanges();
        //        ctrl.dsPredefMsg.PREDEFMSG.WriteXml(ctrl.GetPreDefMsgFilePath(), XmlWriteMode.IgnoreSchema, false);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new ApplicationException("Unable to delete predefined message");
        //    }
        //}

        //public static void DeletePredefinedMsgById(string urNo)
        //{
        //    try
        //    {
        //        CtrlPreDefMsg ctrl = new CtrlPreDefMsg();

        //        DSPreDefMsg.PREDEFMSGRow row = ctrl.dsPredefMsg.PREDEFMSG.FindByURNO(urNo);
        //        row.STAT = "I";
        //        ctrl.dsPredefMsg.PREDEFMSG.AcceptChanges();
        //        ctrl.dsPredefMsg.PREDEFMSG.WriteXml(ctrl.GetPreDefMsgFilePath(), XmlWriteMode.IgnoreSchema, false);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new ApplicationException("Unable to delete predefined message");
        //    }
        //}

        //public static DSPreDefMsg RetrieveActivePredifinedMsg()
        //{
        //    CtrlPreDefMsg ctrl = new CtrlPreDefMsg();
        //    DSPreDefMsg ds = null;
        //    ds = (DSPreDefMsg)ctrl.dsPredefMsg.Copy();
        //    ds.PREDEFMSG.Clear();
        //    foreach (DSPreDefMsg.PREDEFMSGRow row in ctrl.dsPredefMsg.PREDEFMSG.Select("STAT='A'"))
        //    {
        //        ds.PREDEFMSG.LoadDataRow(row.ItemArray, true);
        //    }

        //    return ds;
        //}

        //public static DSPreDefMsg RetrieveAllPredefinedMsg()
        //{
        //    CtrlPreDefMsg ctrl = new CtrlPreDefMsg();
        //    DSPreDefMsg ds = (DSPreDefMsg)ctrl.dsPredefMsg.Clone();

        //    return ds;
        //}
        private const int MAX_MSG_LEN = 250;

        public static DSPreDefMsg RetrieveActivePredifinedMsg()
        {
            return DBPreDefMsg.GetInstance().RetrieveActivePredifinedMsg();
        }

        public static void CreatePredefinedMsg(string msg)
        {
            if (msg.Length > MAX_MSG_LEN) msg = msg.Substring(0, MAX_MSG_LEN);
            DBPreDefMsg.GetInstance().CreatePredefinedMsg(msg);
        }

        public static void DeletePredefinedMsg(string msgId)
        {
            DBPreDefMsg.GetInstance().DeletePredefinedMsgById(msgId);
        }

        public static void UpdatePredefinedMsg(string msgId, string msg)
        {
            if (msg.Length > MAX_MSG_LEN) msg = msg.Substring(0, MAX_MSG_LEN);
            DBPreDefMsg.GetInstance().UpdatePredefinedMsg(msgId, msg);
        }
    }
}
