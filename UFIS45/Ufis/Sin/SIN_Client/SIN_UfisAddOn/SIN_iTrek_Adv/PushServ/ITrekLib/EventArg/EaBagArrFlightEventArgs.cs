using System;
using System.Collections.Generic;
using System.Text;

using System.Collections;

using UFIS.ITrekLib.ENT;

namespace UFIS.ITrekLib.EventArg
{
    public class EaBagArrFlightEventArgs
    {
        private EnBagArrFlightActionType _action = EnBagArrFlightActionType.None;
        private string _flightId = null;
        private List<string> _arrSelectedUldIds = null;
        private string _time = null;

        public EaBagArrFlightEventArgs(string flightId, EnBagArrFlightActionType action
            , List<string> arrSelectedUldIds, string time)
        {
            _flightId = flightId;
            _action = action;
            _arrSelectedUldIds = arrSelectedUldIds;
            _time = time;
        }

        public EnBagArrFlightActionType Action
        {
            get { return _action; }
        }

        public string FlightId
        {
            get { return _flightId; }
        }

        public List<string> SelectedUlds
        {
            get { return _arrSelectedUldIds; }
        }

        public string Time
        {
            get { return _time; }
        }
    }
}
