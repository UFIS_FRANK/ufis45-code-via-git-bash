﻿namespace UFIS.ITrekLib.DS
{


   partial class DSMsg
   {
      partial class MSGTODataTable
      {
      }

      partial class MSGDataTable
      {
      }

      public string GetSentTo(string msgId)
      {
         string stTo = "";
         string seperator = "";
         foreach (MSGTORow row in this.MSGTO.Select("MSG_URNO='" + msgId + "'"))
         {
            string tp = row.TP;
            if ((!string.IsNullOrEmpty(tp)) && (tp == "F"))
            {//Flight Recepient
               if (string.IsNullOrEmpty(row.TONM))
                  stTo += seperator + row.MSG_TO_ID;
               else
                  stTo += seperator + row.TONM;
            }
            else
            {
               stTo += seperator + row.MSG_TO_ID;
            }
            seperator = ",";
         }
         return stTo;
      }
   }
}