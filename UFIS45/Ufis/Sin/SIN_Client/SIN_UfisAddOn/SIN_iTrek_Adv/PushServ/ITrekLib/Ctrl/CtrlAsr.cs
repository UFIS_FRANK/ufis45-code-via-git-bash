using System;
using System.Collections.Generic;
using System.Text;

using System.Collections;
using System.Data;

using UFIS.ITrekLib.Util;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.DB;
using UFIS.ITrekLib.ENT;

namespace UFIS.ITrekLib.Ctrl
{
	public class CtrlAsr
	{
		public static DSApronServiceRpt PopulateApronServiceReport(IDbCommand cmd, string flightUrno)
		{
			LogTraceMsg("PopulateApronServiceReport:" + flightUrno);
			DSApronServiceRpt ds = null;
			UtilDb udb = UtilDb.GetInstance();
			bool isNewCmd = false;
			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = udb.GetCommand(false);
				}

				DBDAsr.RetrieveDsApronServiceRpt(cmd, flightUrno, ref ds);
				if (!IsConfirmedReportExist(cmd, flightUrno))
				{
					LogTraceMsg("PopulateApronServiceReport:" + flightUrno + ",Not exist");
					DSApronServiceRpt.ASRRow row = (DSApronServiceRpt.ASRRow)ds.ASR.Select("URNO='" + flightUrno + "'")[0];
					string stADID = row.AD;
					string stTurn = row.TURN;
					LogTraceMsg("PopulateApronServiceReport:" + flightUrno + "," + stADID + "," + stTurn);
					//Populate ULD Comments					
					if (stADID == "D")
					{//Departure flight of transit, use arrival flight to get the comment
						if (!string.IsNullOrEmpty(stTurn))
							row.ULDCMT = CtrlTrip.GetArrFlightUldAOComments(cmd, stTurn);						
					}
					else
					{
						row.ULDCMT = CtrlTrip.GetArrFlightUldAOComments(cmd, flightUrno);
					}
					if (row.ULDCMT.Length > 4000) row.ULDCMT = row.ULDCMT.Substring(0, 3999);
					LogTraceMsg("PopulateApronServiceReport:" + flightUrno + "," + row.ULDCMT);
				}
				else
				{
					LogTraceMsg("PopulateApronServiceReport:" + flightUrno + ",Not exist");
				}
			}
			catch (Exception ex)
			{
				LogMsg("PopulateApronServiceReport:Err:" + ex.Message);
				throw new ApplicationException("Unable to retrieve the ASR data.");
			}
			finally
			{
				if (isNewCmd)
				{
					udb.CloseCommand(cmd, false);
				}
			}

			return ds;
		}



		public static DSApronServiceRpt PopulateApronServiceReport2(IDbCommand cmd, string flightUrno, string txtSelCmd)
		{
			LogTraceMsg("PopulateApronServiceReport:" + flightUrno);
			DSApronServiceRpt ds = null;
			UtilDb udb = UtilDb.GetInstance();
			bool isNewCmd = false;
			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = udb.GetCommand(false);
				}

				DBDAsr.RetrieveDsApronServiceRpt2(cmd, flightUrno, txtSelCmd, ref ds);
				if (!IsConfirmedReportExist(cmd, flightUrno))
				{
					LogTraceMsg("PopulateApronServiceReport:" + flightUrno + ",Not exist");
					DSApronServiceRpt.ASRRow row = (DSApronServiceRpt.ASRRow)ds.ASR.Select("URNO='" + flightUrno + "'")[0];
					string stADID = row.AD;
					string stTurn = row.TURN;
					LogTraceMsg("PopulateApronServiceReport:" + flightUrno + "," + stADID + "," + stTurn);
					//Populate ULD Comments					
					if (stADID == "D")
					{//Departure flight of transit, use arrival flight to get the comment
						if (!string.IsNullOrEmpty(stTurn))
							row.ULDCMT = CtrlTrip.GetArrFlightUldAOComments(cmd, stTurn);
					}
					else
					{
						row.ULDCMT = CtrlTrip.GetArrFlightUldAOComments(cmd, flightUrno);
					}
					if (row.ULDCMT.Length > 4000) row.ULDCMT = row.ULDCMT.Substring(0, 3999);
					LogTraceMsg("PopulateApronServiceReport:" + flightUrno + "," + row.ULDCMT);
				}
				else
				{
					LogTraceMsg("PopulateApronServiceReport:" + flightUrno + ",Not exist");
				}
			}
			catch (Exception ex)
			{
				LogMsg("PopulateApronServiceReport:Err:" + ex.Message);
				throw new ApplicationException("Unable to retrieve the ASR data.");
			}
			finally
			{
				if (isNewCmd)
				{
					udb.CloseCommand(cmd, false);
				}
			}

			return ds;
		}

		private static DBDAsr _dbAsr = DBDAsr.GetInstance();

		private static string RetrieveAsrGenComment(IDbCommand cmd, string flightUrno)
		{
			string stGenComment = "";
			UtilDb udb = UtilDb.GetInstance();
			bool isNewCmd = false;

			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = udb.GetCommand(false);
				}
				Object objGenComment = UtilDb.GetDataForAColumn(cmd, 
					UFIS.ITrekLib.Config.DB_Info.DB_T_ASR, flightUrno, "RGEN");
				if ((objGenComment == null) || (objGenComment.ToString() == DBNull.Value.ToString()))
				{
					stGenComment = "";
				}
				else
				{
					stGenComment = (string)objGenComment;
				}
			}
			catch (Exception ex)
			{
				LogMsg("RetrieveAsrGenComment:Err:" + ex.Message);
				throw;
			}
			finally
			{
				if (isNewCmd)
				{
					udb.CloseCommand(cmd, false);
				}
			}
			return stGenComment;
		}

		private static bool IsReportExist(IDbCommand cmd, string flightUrno, out bool isDraft)
		{
			DBDAsr.EnumAsrStatus stat = DBDAsr.EnumAsrStatus.Unknown;
			//return _dbAsr.IsApronServiceRptExist(flightUrno, out isDraft);
			bool exist = DBDAsr.IsAsrExist(cmd, flightUrno, out stat);
			isDraft = true;
			switch (stat)
			{
				case DBDAsr.EnumAsrStatus.NoAsr:
					break;
				case DBDAsr.EnumAsrStatus.Draft:
					break;
				case DBDAsr.EnumAsrStatus.Submit:
					isDraft = false;
					break;
				case DBDAsr.EnumAsrStatus.Lock:
					isDraft = false;
					break;
				case DBDAsr.EnumAsrStatus.Unknown:
					break;
				default:
					break;
			}

			return exist;
		}

		/// <summary>
		/// Is Confirmed ASR report exist. 
		/// </summary>
		/// <param name="cmd">Command to use if any. Pass 'null' if does not have any DB connection</param>
		/// <param name="flightUrno">Flight Id</param>
		/// <returns></returns>
		public static bool IsConfirmedReportExist(IDbCommand cmd, string flightUrno)
		{
			//DBDAsr.EnumAsrStatus stat = DBDAsr.EnumAsrStatus.Unknown;
			////return _dbAsr.IsApronServiceRptExist(flightUrno, out isDraft);
			//bool exist = DBDAsr.IsAsrExist(null, flightUrno, out stat);
			//bool isDraft = true;
			//switch (stat)
			//{
			//    case DBDAsr.EnumAsrStatus.NoAsr:
			//        break;
			//    case DBDAsr.EnumAsrStatus.Draft:
			//        break;
			//    case DBDAsr.EnumAsrStatus.Submit:
			//        isDraft = false;
			//        break;
			//    case DBDAsr.EnumAsrStatus.Lock:
			//        isDraft = false;
			//        break;
			//    case DBDAsr.EnumAsrStatus.Unknown:
			//        break;
			//    default:
			//        break;
			//}

			//if (isDraft == true) { exist = false; }

			//return exist;
			return _dbAsr.IsConfirmASRExist(cmd, flightUrno);
		}

		public static bool IsDraft(IDbCommand cmd, string flightUrno)
		{
			DBDAsr.EnumAsrStatus stat = DBDAsr.EnumAsrStatus.Unknown;
			//return _dbAsr.IsApronServiceRptExist(flightUrno, out isDraft);
			bool exist = DBDAsr.IsAsrExist(cmd, flightUrno, out stat);
			bool isDraft = true;
			switch (stat)
			{
				case DBDAsr.EnumAsrStatus.NoAsr:
					break;
				case DBDAsr.EnumAsrStatus.Draft:
					break;
				case DBDAsr.EnumAsrStatus.Submit:
					isDraft = false;
					break;
				case DBDAsr.EnumAsrStatus.Lock:
					isDraft = false;
					break;
				case DBDAsr.EnumAsrStatus.Unknown:
					break;
				default:
					break;
			}

			return isDraft;
		}

		#region Populate Data from Flight (Not used)
		//public static DSApronServiceRpt PopulateData(string flightUrno)
		//{
		//    //Populate data from Flight
		//    DSApronServiceRpt dsASR = new DSApronServiceRpt();
		//    DSFlight dsFlight = CtrlFlight.RetrieveFlightsByFlightId(flightUrno);
		//    string stTURN = "";
		//    string stADID = "";
		//    DSApronServiceRpt.ASRRow row = dsASR.ASR.NewASRRow();
		//    if (dsFlight.FLIGHT.Rows.Count > 0)
		//    {
		//        DSFlight.FLIGHTRow fRow = (DSFlight.FLIGHTRow)(dsFlight.FLIGHT.Rows[0]);
		//        stTURN = fRow.TURN;
		//        stADID = fRow.A_D;
		//        if (stTURN == null) stTURN = "";
		//        else stTURN = stTURN.Trim();

		//        row.UAFT = fRow.URNO;
		//        row.AD = fRow.A_D;
		//        row.ACFTREGN = fRow.REGN;
		//        row.ACFTTYPE = fRow.AIRCRAFT_TYPE;

		//        row.AOID = fRow.AOID;
		//        row.AOFNAME = fRow.AOFNAME;
		//        row.AOLNAME = fRow.AOLNAME;
		//        row.FLNO = fRow.FLNO;
		//        row.FR = fRow.FR;
		//        row.TO = fRow.TO;

		//        row.BAY = fRow.BAY;

		//        row.FTRIP = fRow.AP_FST_TRP;
		//        row.LTRIP = fRow.AP_LST_TRP;
		//        row.UNLEND = fRow.UNL_END;
		//        row.LDSTRT = fRow.LD_ST;
		//        row.LDEND = fRow.LD_END;
		//        row.LSTDOR = fRow.LST_DOOR;
		//        row.PLB = fRow.PLB;
		//        row.ST = fRow.ST;
		//        row.AT = fRow.AT;
		//        if (stADID == "A")
		//        {//Arrival Flight
		//            row.STA = fRow.ST;
		//            row.ATA = fRow.AT;
		//            row.STD = "";
		//            row.ATD = "";
		//        }
		//        else if (stADID == "D")
		//        {//Departure Flight
		//            row.STA = "";
		//            row.ATA = "";
		//            row.STD = fRow.ST;
		//            row.ATD = fRow.AT;
		//        }

		//        if (stTURN == "")
		//        {//Not a Transit Flight
		//            //Populate ULD Comments
		//            row.ULDCMT = CtrlTrip.GetArrFlightUldAOComments(flightUrno);
		//        }
		//        else
		//        {//Transit Flight
		//            //Populate ULD Comments
		//            row.ULDCMT = CtrlTrip.GetArrFlightUldAOComments(flightUrno) +
		//                "\n" + CtrlTrip.GetArrFlightUldAOComments(stTURN);
		//            row.TURN = stTURN;
		//            DSFlight dsFlightTransit = CtrlFlight.RetrieveFlightsByFlightId(stTURN);
		//            if (dsFlightTransit.FLIGHT.Rows.Count > 0)
		//            {
		//                DSFlight.FLIGHTRow fRowTransit = (DSFlight.FLIGHTRow)(dsFlightTransit.FLIGHT.Rows[0]);
		//                string stADIDTransit = fRowTransit.A_D;
		//                if (stADIDTransit == "A")
		//                {
		//                    row.FR = fRowTransit.FR;
		//                    row.FTRIP = fRowTransit.AP_FST_TRP;
		//                    row.LTRIP = fRowTransit.AP_LST_TRP;
		//                    row.UNLEND = fRowTransit.UNL_END;

		//                    row.ST = fRowTransit.ST;
		//                    row.STA = fRowTransit.ST;
		//                    row.ATA = fRowTransit.AT;
		//                }
		//                else if (stADIDTransit == "D")
		//                {
		//                    row.TO = fRowTransit.TO;
		//                    row.LDSTRT = fRowTransit.LD_ST;
		//                    row.LDEND = fRowTransit.LD_END;
		//                    row.LSTDOR = fRowTransit.LST_DOOR;
		//                    row.PLB = fRowTransit.PLB;

		//                    row.STD = fRowTransit.ST;
		//                    row.ATD = fRowTransit.AT;
		//                }
		//            }
		//        }
		//    }

		//    dsASR.ASR.AddASRRow(row);

		//    return dsASR;
		//}
		#endregion

		#region Populate Data for a Flight (not used)
		//public static DSApronServiceRpt PopulateData(string flightUrno, DSApronServiceRpt dsASR)
		//{
		//    //Populate data from Flight
		//    DSFlight dsFlight = CtrlFlight.RetrieveFlightsByFlightId(flightUrno);
		//    string stTURN = "";
		//    string stADID = "";
		//    bool isNew = false;
		//    DSApronServiceRpt.ASRRow row = null;

		//    row = (DSApronServiceRpt.ASRRow)dsASR.ASR.Select("UAFT='" + flightUrno + "'")[0];
		//    if (row == null)
		//    {
		//        isNew = true;
		//        row = dsASR.ASR.NewASRRow();
		//    }
		//    if (dsFlight.FLIGHT.Rows.Count > 0)
		//    {
		//        DSFlight.FLIGHTRow fRow = (DSFlight.FLIGHTRow)(dsFlight.FLIGHT.Rows[0]);
		//        stTURN = fRow.TURN;
		//        stADID = fRow.A_D;
		//        row.UAFT = fRow.URNO;
		//        row.AD = fRow.A_D;
		//        row.ACFTREGN = fRow.REGN;
		//        row.ACFTTYPE = fRow.AIRCRAFT_TYPE;
		//        row.AOID = fRow.AOID;
		//        row.AOFNAME = fRow.AOFNAME;
		//        row.AOLNAME = fRow.AOLNAME;
		//        row.FLNO = fRow.FLNO;
		//        row.FR = fRow.FR;
		//        row.TO = fRow.TO;
		//        row.ST = fRow.ST;
		//        row.AT = fRow.AT;
		//        row.BAY = fRow.BAY;

		//        row.FTRIP = fRow.AP_FST_TRP;
		//        row.LTRIP = fRow.AP_LST_TRP;
		//        row.UNLEND = fRow.UNL_END;
		//        row.LDSTRT = fRow.LD_ST;
		//        row.LDEND = fRow.LD_END;
		//        row.LSTDOR = fRow.LST_DOOR;
		//        row.PLB = fRow.PLB;
		//    }

		//    //Populate ULD Comments
		//    if (stADID == "D")
		//    {//Departure flight of transit, use arrival flight to get the comment
		//        row.ULDCMT = CtrlTrip.GetArrFlightUldAOComments(stTURN);
		//    }
		//    else
		//    {
		//        row.ULDCMT = CtrlTrip.GetArrFlightUldAOComments(flightUrno);
		//    }
		//    if (isNew)
		//    {
		//        dsASR.ASR.AddASRRow(row);
		//    }
		//    return dsASR;
		//}
		#endregion

		#region RetrieveApronServiceReport
		public static DSApronServiceRpt RetrieveApronServiceReport(IDbCommand cmd, string flightUrno)
		{
			//DSApronServiceRpt dsASR = new DSApronServiceRpt();
			//dsASR = _dbAsr.RetrieveApronServiceReportForAFlight(flightUrno);
			//return dsASR;
			return PopulateApronServiceReport(cmd, flightUrno);
		}
		#endregion


		public static void SaveApronServiceReport(IDbCommand cmd, string flightId, DSApronServiceRpt dsASR, EntUser curUser)
		{
			UtilDb udb = UtilDb.GetInstance();
			bool commit = false;
			bool isNewCmd = false;//Is command 'cmd' created from this method?
			LogMsg("SaveApronServiceReport:Start:" + flightId);
			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = udb.GetCommand(true);
				}

				string stTrsitFltUrno = "";
				stTrsitFltUrno = CtrlFlight.RetrieveRelatedTransitFlightUrnoForFlight(cmd, flightId);
				DBDAsr.SaveRpt(cmd, flightId, dsASR, curUser.UserId);
				//dsASR.WriteXml( "C:\\TEMP\\ASR.XML");
				LogTraceMsg("SaveApronServiceReport:Save:ASR:" + flightId);
				if (!string.IsNullOrEmpty(stTrsitFltUrno))
				{
					DSApronServiceRpt dsAsrTurn = new DSApronServiceRpt();
					DSApronServiceRpt.ASRRow[] tRows = (DSApronServiceRpt.ASRRow[])dsASR.ASR.Select("URNO='" + flightId + "'");
					if ((tRows != null) || (tRows.Length > 0))
					{
						LogTraceMsg("SaveApronServiceReport:Save:TurnAsr:" + stTrsitFltUrno);
						DSApronServiceRpt.ASRRow tRow = tRows[0];
						tRow.URNO = stTrsitFltUrno;
						tRow.TURN = flightId;
						foreach (DataColumn col1 in dsASR.ASR.Columns)
						{
							if (!dsAsrTurn.ASR.Columns.Contains(col1.ColumnName))
							{
								LogTraceMsg("ExtraCol:" + col1.ColumnName);
								dsASR.ASR.Columns.Remove(col1);
							}
						}

						dsAsrTurn.ASR.LoadDataRow(tRow.ItemArray, true);
						LogTraceMsg("SaveApronServiceReport:Save:TurnAsr:AfLoad:" + stTrsitFltUrno);
						dsASR.ASR.RejectChanges();
						DBDAsr.SaveRpt(cmd, stTrsitFltUrno, dsAsrTurn, curUser.UserId);
						LogTraceMsg("SaveApronServiceReport:Save:TurnAsr:AfUpd:" + stTrsitFltUrno);
					}					
				}

				commit = true;
			}
			catch (Exception ex)
			{
				LogMsg("SaveApronServiceReport:Err:" + ex.Message);
				throw new ApplicationException("Fail to save");
			}
			finally
			{
				if (isNewCmd)
				{//Command 'cmd' is created from this method. So, close the command from this method.
					LogMsg("SaveApronServiceReport:Commit:" + commit);
					udb.CloseCommand(cmd, commit);
				}
			}
		}

		//        public static void SaveRptForRelatedTransitFlight(string flightUrno,
		//            string transitFltUrno, DSApronServiceRpt dsASR, EntUser curUser)
		//        {
		//            try
		//            {
		//                if ((transitFltUrno != null) && (transitFltUrno != ""))
		//                {
		//                    LogTraceMsg("Turn:" + flightUrno + "," + transitFltUrno);
		//                    DSApronServiceRpt dsTemp = PopulateApronServiceReport(transitFltUrno);
		//                    if (dsTemp != null)
		//                    {
		//                        foreach (DSApronServiceRpt.ASRRow row in dsTemp.ASR.Select("UAFT='" + transitFltUrno + "'"))
		//                        {
		//                            //LogTraceMsg("has info for turn" + dsASR.ASR.Rows.Count);
		//                            DSApronServiceRpt dsNew = (DSApronServiceRpt)dsASR.Copy();
		//                            DSApronServiceRpt.ASRRow newRow = (DSApronServiceRpt.ASRRow)dsNew.ASR.Rows[0];
		//                            //LogTraceMsg("before assign," + newRow.UAFT + "<==" + row.UAFT);
		//                            newRow.UAFT = row.UAFT;
		//                            if (row.AD == "A")
		//                            {
		//                                newRow.AD = "D";
		//                            }
		//                            else if (row.AD == "D")
		//                            {
		//                                newRow.AD = "A";
		//                            }

		//                            newRow.ACFTREGN = row.ACFTREGN;
		//                            newRow.ACFTTYPE = row.ACFTTYPE;
		//                            newRow.AOID = row.AOID;
		//                            newRow.AOFNAME = row.AOFNAME;
		//                            newRow.AOLNAME = row.AOLNAME;
		//                            newRow.FLNO = row.FLNO;
		//                            newRow.FR = row.FR;
		//                            newRow.TO = row.TO;
		//                            newRow.STA = row.STA;
		//                            newRow.ATA = row.ATA;
		//                            newRow.STD = row.STD;
		//                            newRow.ATD = row.ATD;
		//                            newRow.BAY = row.BAY;

		//                            newRow.FTRIP = row.FTRIP;
		//                            newRow.LTRIP = row.LTRIP;
		//                            newRow.UNLEND = row.UNLEND;
		//                            newRow.LDSTRT = row.LDSTRT;
		//                            newRow.LDEND = row.LDEND;
		//                            newRow.LSTDOR = row.LSTDOR;
		//                            newRow.PLB = row.PLB;
		//                            newRow.TURN = flightUrno;
		//                            //LogTraceMsg("after assign");
		//                            foreach (DSApronServiceRpt.ASRRow tRow in dsASR.ASR.Select("UAFT='" + transitFltUrno + "'"))
		//                            {
		//                                tRow.Delete();
		//                            }
		//                            //LogTraceMsg("after del");
		//                            dsASR.ASR.ImportRow(newRow);
		//                            //dsASR.ASR.Rows.Add(newRow);
		//                            //LogTraceMsg("after import," + dsASR.ASR.Rows.Count);
		//                            break;
		//                        }
		//                    }
		//                }
		//                if ((transitFltUrno == null) || (transitFltUrno == ""))
		//                {
		//                    _dbAsr.SaveRpt(flightUrno, dsASR, curUser.UserId);
		//                }
		//                else
		//                {
		//                    _dbAsr.SaveRpt(flightUrno, transitFltUrno, dsASR, curUser.UserId);
		//                }
		//                if (IsConfirmedReportExist(flightUrno))
		//                {
		//#warning //TODO - To Update the Baggage Presentation Timing Report Exist
		//                    //CtrlFlight.GetInstance().UpdateApronServiceReportExist(flightUrno, true);
		//                }
		//                if (IsConfirmedReportExist(transitFltUrno))
		//                {
		//#warning //TODO - To Update the Baggage Presentation Timing Report Exist
		//                    //CtrlFlight.GetInstance().UpdateApronServiceReportExist(transitFltUrno, true);
		//                }
		//            }
		//            catch (Exception ex)
		//            {
		//                LogMsg("SaveRptForRelatedTransitFlight:Err:" + ex.Message);
		//                throw new ApplicationException("Fail to Save.");
		//            }
		//        }

		//public static void HkReport(ArrayList arrList)
		//{
		//    _dbAsr.HkReport(arrList);
		//}

		#region Logging
		private static void LogMsg(string msg)
		{
			try
			{
				MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("CtrlASR", msg);
			}
			catch (Exception)
			{
				UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("CtrlASR", msg);
			}

		}

		private static void LogTraceMsg(string msg)
		{
			LogMsg(msg);
		}
		#endregion

		public static void DoXmlToDbConv()
		{
			UtilDb udb = UtilDb.GetInstance();
			bool isNewCmd = false;
			bool commit = false;
			IDbCommand cmd = null;

			try
			{
				if (cmd == null)
				{
					cmd = udb.GetCommand(false);
					isNewCmd = true;
				}
				string stAsrXmlPath = @"C:\TEMP\Asr.XML";
				//DSApronServiceRpt ds = new DSApronServiceRpt();
				//ds.ReadXml(stAsrXmlPath);

				DataSet ds = new DataSet();
				ds.ReadXml(stAsrXmlPath);

				foreach (DataRow oldRow in ds.Tables[0].Rows)
				{
					DSApronServiceRpt tmp = new DSApronServiceRpt();
					DSApronServiceRpt.ASRRow newRow = tmp.ASR.NewASRRow();
					foreach (DataColumn oldCol in ds.Tables[0].Columns)
					{
						if (oldCol.ColumnName == "UAFT")
						{
							AssignAsrOldToNewData(oldRow, oldCol.ColumnName, newRow, "URNO");
						}
						else
						{
							AssignAsrOldToNewData(oldRow, oldCol.ColumnName, newRow, oldCol.ColumnName);
						}
					}
					tmp.ASR.AddASRRow(newRow);
					tmp.ASR.AcceptChanges();
					DBDAsr.SaveRpt(cmd, newRow.URNO, tmp, "conv");
				}
				commit = true;
			}
			catch (Exception ex)
			{
				LogMsg("RetrieveFlidsForUafts:Err:" + ex.Message);
				throw;
			}
			finally
			{
				if (isNewCmd)
				{
					udb.CloseCommand(cmd, commit);
				}
			}
		}

		private static void AssignAsrOldToNewData(DataRow oldRow, string oldColName, DataRow newRow, string newColName)
		{
			try
			{
				newRow[newColName] = oldRow[oldColName];
			}
			catch (Exception)
			{
			}
		}
	}
}