#define TESTING_ON

using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Data;

using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.DB;
using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.Util;
using iTrekWMQ;
using iTrekXML;
using System.Xml;
using System.Collections;
using MM.UtilLib;

namespace UFIS.ITrekLib.Ctrl
{
   public class CtrlTrip
   {
      public static DSTrip RetrieveTrip(string flightId)
      {
         DSTrip ds = null;
         DBTrip dbTrip = DBTrip.GetInstance();
         ds = dbTrip.RetrieveTripForAFlight(flightId);
         return ds;
      }

      public static DSTrip RetrieveReceivedTrip(string flightId)
      {
         return DBTrip.GetInstance().RetrieveTripForAFlight(flightId, EnBagArrRecvCategory.Recv, EnBagArrType.All);
      }

      //public static DSTrip RetrieveTripNotReceived(string flightUrno)
      //{
      //    DSTrip ds = null;
      //    DBTrip dbTrip = DBTrip.GetInstance();
      //    ds = dbTrip.RetrieveTripForAFlightNotReceived(flightUrno);
      //    return ds;
      //}

      //public static DSTrip RetrieveTrip(string flightUrno,string uldn)
      //{
      //    DSTrip ds = null;
      //    DBTrip dbTrip = DBTrip.GetInstance();
      //    ds = dbTrip.RetrieveTripForAFlight(flightUrno,uldn);
      //    return ds;
      //}

      public static DSTrip RetrieveTripWithMaxCnt(string flightUrno)
      {
         DSTrip ds = null;
         DBTrip dbTrip = DBTrip.GetInstance();
         ds = dbTrip.RetrieveTripForAFlightWithMaxCnt(flightUrno);
         return ds;
      }


      public static DSTrip RetrieveTripNotReceived(string flightUrno)
      {
         DSTrip ds = null;
         DBTrip dbTrip = DBTrip.GetInstance();
         ds = dbTrip.RetrieveTripForAFlightNotReceived(flightUrno);
         return ds;
      }

      public static DSTrip RetrieveTrip(string flightUrno, string uldn)
      {
         DSTrip ds = null;
         DBTrip dbTrip = DBTrip.GetInstance();
         ds = dbTrip.RetrieveTripForAFlight(flightUrno, uldn);
         return ds;
      }

      public static DSTrip RetrieveTripforTripUrno(string flightUrno, string urno)
      {
         DSTrip ds = null;
         DBTrip dbTrip = DBTrip.GetInstance();
         ds = dbTrip.RetrieveTripForAFlightWithTripUrno(flightUrno, urno);
         return ds;
      }

      public static DataTable RetrieveUnSentCPMandTripsInfo(string flightUrno)
      {
         DataTable dt = new DataTable();
         dt.Columns.Add("ULDNO");
         dt.Columns.Add("ULDINFO");

         DSCpm dsCpm = CtrlCPM.RetrieveUnSentCPMUldForAFlight(flightUrno);
         foreach (DSCpm.CPMDETRow row in dsCpm.CPMDET.Select("", "POSF,DSSN,ULDN"))
         {
            dt.Rows.Add(row.ULDN, row.ULDN);
         }

         DSTrip dsTrip = RetrieveTrip(flightUrno);
         foreach (DSTrip.TRIPRow row in dsTrip.TRIP.Select("", "ISCPM_FORSORT,STRPNO,RTRPNO,ULDN"))
         {
            string st = row.ULDN;
            //string sentDt = GetShortTime(row.SENT_DT);
            //string rcvDt = GetShortTime(row.RCV_DT);
            //if (sentDt != "") st += "(Sent:" + sentDt + ")";
            //if (rcvDt != "") st += "(Received:" + rcvDt + ")";
            dt.Rows.Add(row.ULDN, row.CPM_INFO);
         }
         dt.AcceptChanges();
         return dt;
      }

      public static DataTable RetrieveUnSentCPMandTripsInfo(string flightUrno, int count)
      {
         DataTable dt = new DataTable();
         dt.Columns.Add("ULDNO");
         dt.Columns.Add("ULDINFO");

         DSCpm dsCpm = CtrlCPM.RetrieveUnSentCPMUldForAFlight(flightUrno);
         foreach (DSCpm.CPMDETRow row in dsCpm.CPMDET.Select("", "POSF,DSSN,ULDN"))
         {
            dt.Rows.Add(row.ULDN, row.ULDN);

         }

         DSTrip dsTrip = RetrieveTrip(flightUrno);
         foreach (DSTrip.TRIPRow row in dsTrip.TRIP.Select("", "ISCPM_FORSORT,STRPNO,RTRPNO,ULDN"))
         {
            string st = row.ULDN;
            //string sentDt = GetShortTime(row.SENT_DT);
            //string rcvDt = GetShortTime(row.RCV_DT);
            //if (sentDt != "") st += "(Sent:" + sentDt + ")";
            //if (rcvDt != "") st += "(Received:" + rcvDt + ")";
            dt.Rows.Add(row.ULDN, row.CPM_INFO);
         }
         dt.AcceptChanges();
         return dt;
      }

      private static string GetShortTime(string ufisDateTime)
      {
         string st = "";
         if ((ufisDateTime != null) && (ufisDateTime != ""))
         {
            st = UtilTime.ConvUfisTimeStringToITrekShortTimeString(ufisDateTime);
         }
         return st;
      }

      public const string FIRST_TRIP_IND = "F";
      public const string LAST_TRIP_IND = "L";
      public const string NEXT_TRIP_IND = "N";

      public static void SaveTrip(string flightId,
          EntCPMList cpmList, DateTime dtSentDateTime, string sentBy, EnTripType tripType, EnTripSendRecv sendRecv)
      {
         EntCPMList ls = null;
         string sentDateTime = UtilTime.ConvDateTimeToUfisFullDTString(dtSentDateTime);
         string stTripType = "";
         ls = cpmList.GetSelectedList(ls);
         switch (tripType)
         {
            case EnTripType.FirstTrip:
               stTripType = FIRST_TRIP_IND;
               break;
            case EnTripType.LastTrip:
               stTripType = LAST_TRIP_IND;
               break;
            case EnTripType.NextTrip:
               stTripType = NEXT_TRIP_IND;
               break;
         }

         string stSendRecv = "";

         switch (sendRecv)
         {
            case EnTripSendRecv.Receive:
               stSendRecv = "R";
               break;
            case EnTripSendRecv.Send:
               stSendRecv = "S";
               break;
            default:
               break;
         }

         StringBuilder xml = new StringBuilder();
         xml.Append("<TRIP>");
         xml.Append("<UAFT>" + flightId + "</UAFT>");//Flight UrNo
         xml.Append("<TRPTYPE>" + stTripType + "</TRPTYPE>");//(Trip Type, F  First trip, N  Next Trip, L Last Trip)
         xml.Append("<SR>" + stSendRecv + "</SR>");//(Send or Receive, S Send, R  Receive)
         xml.Append("<SRDT>" + sentDateTime + "</SRDT>");//(Send or Receive Date and Time)
         xml.Append("<SRBY>" + sentBy + "</SRBY>");//(Sent or Received by User Id)
         xml.Append(GenXmlForUldsForSendingToBackend(cpmList));
         xml.Append("</TRIP>");
         ITrekWMQClass1 wmq = new ITrekWMQClass1();
         iTXMLPathscl path = new iTXMLPathscl();
         wmq.putTheMessageIntoQueue(xml.ToString(), path.GetFROMITREKUPDATEQueueName());
      }

      public static void SaveTrip(string flightId,
          EntCPMList cpmList, string sentDateTime, string sentBy, EnTripType tripType, EnTripSendRecv sendRecv)
      {
         EntCPMList ls = null;
         //string sentDateTime = Util.UtilTime.ConvDateTimeToUfisFullDTString(dtSentDateTime);
         string stTripType = "";
         ls = cpmList.GetSelectedList(ls);
         switch (tripType)
         {
            case EnTripType.FirstTrip:
               stTripType = FIRST_TRIP_IND;
               break;
            case EnTripType.LastTrip:
               stTripType = LAST_TRIP_IND;
               break;
            case EnTripType.NextTrip:
               stTripType = NEXT_TRIP_IND;
               break;
         }

         string stSendRecv = "";

         switch (sendRecv)
         {
            case EnTripSendRecv.Receive:
               stSendRecv = "R";
               break;
            case EnTripSendRecv.Send:
               stSendRecv = "S";
               break;
            default:
               break;
         }

         StringBuilder xml = new StringBuilder();
         xml.Append("<TRIP>");
         xml.Append("<UAFT>" + flightId + "</UAFT>");//Flight UrNo
         xml.Append("<TRPTYPE>" + stTripType + "</TRPTYPE>");//(Trip Type, F  First trip, N  Next Trip, L Last Trip)
         xml.Append("<SR>" + stSendRecv + "</SR>");//(Send or Receive, S Send, R  Receive)
         xml.Append("<SRDT>" + sentDateTime + "</SRDT>");//(Send or Receive Date and Time)
         xml.Append("<SRBY>" + sentBy + "</SRBY>");//(Sent or Received by User Id)
         xml.Append(GenXmlForUldsForSendingToBackend(cpmList));
         xml.Append("</TRIP>");
         ITrekWMQClass1 wmq = new ITrekWMQClass1();
         iTXMLPathscl path = new iTXMLPathscl();
         wmq.putTheMessageIntoQueue(xml.ToString(), path.GetFROMITREKUPDATEQueueName());
      }

      public static void SaveTripAndUpdateIndicator(string flightId,
 EntCPMList cpmList, string sentDateTime, string sentBy, EnTripType tripType, EnTripSendRecv sendRecv)
      {
         EntCPMList ls = null;
         //string sentDateTime = Util.UtilTime.ConvDateTimeToUfisFullDTString(dtSentDateTime);
         string stTripType = "";
         ls = cpmList.GetSelectedList(ls);
         switch (tripType)
         {
            case EnTripType.FirstTrip:
               stTripType = FIRST_TRIP_IND;
               break;
            case EnTripType.LastTrip:
               stTripType = LAST_TRIP_IND;
               break;
            case EnTripType.NextTrip:
               stTripType = NEXT_TRIP_IND;
               break;
         }

         string stSendRecv = "";

         switch (sendRecv)
         {
            case EnTripSendRecv.Receive:
               stSendRecv = "R";
               break;
            case EnTripSendRecv.Send:
               stSendRecv = "S";
               break;
            default:
               break;
         }

         StringBuilder xml = new StringBuilder();
         xml.Append("<TRIP>");
         xml.Append("<UAFT>" + flightId + "</UAFT>");//Flight UrNo
         xml.Append("<TRPTYPE>" + stTripType + "</TRPTYPE>");//(Trip Type, F  First trip, N  Next Trip, L Last Trip)
         xml.Append("<SR>" + stSendRecv + "</SR>");//(Send or Receive, S Send, R  Receive)
         xml.Append("<SRDT>" + sentDateTime + "</SRDT>");//(Send or Receive Date and Time)
         xml.Append("<SRBY>" + sentBy + "</SRBY>");//(Sent or Received by User Id)
         xml.Append(GenXmlForUldsandUpdateIndicator(cpmList, flightId));
         xml.Append("</TRIP>");
         ITrekWMQClass1 wmq = new ITrekWMQClass1();
         iTXMLPathscl path = new iTXMLPathscl();
         wmq.putTheMessageIntoQueue(xml.ToString(), path.GetFROMITREKUPDATEQueueName());
      }

      public static void SaveTripAndUpdateIndicatorForBrs(string flightId,
         EntCPMList cpmList, string sentDateTime, string sentBy, EnTripType tripType, EnTripSendRecv sendRecv)
      {
         EntCPMList ls = null;
         //string sentDateTime = Util.UtilTime.ConvDateTimeToUfisFullDTString(dtSentDateTime);
         string stTripType = "";
         ls = cpmList.GetSelectedList(ls);
         switch (tripType)
         {
            case EnTripType.FirstTrip:
               stTripType = FIRST_TRIP_IND;
               break;
            case EnTripType.LastTrip:
               stTripType = LAST_TRIP_IND;
               break;
            case EnTripType.NextTrip:
               stTripType = NEXT_TRIP_IND;
               break;
         }

         string stSendRecv = "";

         switch (sendRecv)
         {
            case EnTripSendRecv.Receive:
               stSendRecv = "R";
               break;
            case EnTripSendRecv.Send:
               stSendRecv = "S";
               break;
            default:
               break;
         }

         StringBuilder xml = new StringBuilder();
         xml.Append("<TRIP>");
         xml.Append("<UAFT>" + flightId + "</UAFT>");//Flight UrNo
         xml.Append("<TRPTYPE>" + stTripType + "</TRPTYPE>");//(Trip Type, F  First trip, N  Next Trip, L Last Trip)
         xml.Append("<SR>" + stSendRecv + "</SR>");//(Send or Receive, S Send, R  Receive)
         xml.Append("<SRDT>" + sentDateTime + "</SRDT>");//(Send or Receive Date and Time)
         xml.Append("<SRBY>" + sentBy + "</SRBY>");//(Sent or Received by User Id)
         xml.Append(GenXmlForUldsandUpdateIndicator(cpmList, flightId));
         xml.Append("</TRIP>");
         ITrekWMQClass1 wmq = new ITrekWMQClass1();
         iTXMLPathscl path = new iTXMLPathscl();
         wmq.putTheMessageIntoQueue(xml.ToString(), path.GetFROMITREKUPDATEQueueName());
      }

      //private static string GenXmlForUldsForSendingToBackend(EntCPMList cpmList)
      //{
      //    StringBuilder xml = new StringBuilder();
      //    int cnt = cpmList.Count;
      //    for(int i=0; i<cnt; i++)
      //    {
      //        EntCPM cpm = cpmList[i];
      //        xml.Append("<ULD>");//(one ULD)
      //        xml.Append("<ULDN>" + cpm.ContainerNo + "</ULDN>");//(ULD Number)
      //        xml.Append("<RMK>" + cpm.RemarkText +"</RMK>");//(Remark)
      //        xml.Append("<DES>" + cpm.Destination +"</DES>");//(Destination)
      //        if (cpm.AllowToChangeContainerNo)
      //        {
      //            xml.Append("<ISCPM>N</ISCPM>");//(Is CPM/Free, Y  CPM, Otherwise  Free)
      //        }
      //        else
      //        {
      //            xml.Append("<ISCPM>Y</ISCPM>");//(Is CPM/Free, Y  CPM, Otherwise  Free)
      //        }
      //        xml.Append("</ULD>");
      //    }

      //    return xml.ToString();
      //}

      public static void SaveTrip(string flightId,
          ArrayList cpmList, string sentDateTime, string sentBy, EnTripType tripType, EnTripSendRecv sendRecv)
      {
         //EntCPMList ls = null;
         //string sentDateTime = Util.UtilTime.ConvDateTimeToUfisFullDTString(dtSentDateTime);
         string stTripType = "";
         // ls = cpmList.GetSelectedList(ls);
         switch (tripType)
         {
            case EnTripType.FirstTrip:
               stTripType = FIRST_TRIP_IND;
               break;
            case EnTripType.LastTrip:
               stTripType = LAST_TRIP_IND;
               break;
            case EnTripType.NextTrip:
               stTripType = NEXT_TRIP_IND;
               break;
         }

         string stSendRecv = "";

         switch (sendRecv)
         {
            case EnTripSendRecv.Receive:
               stSendRecv = "R";
               break;
            case EnTripSendRecv.Send:
               stSendRecv = "S";
               break;
            default:
               break;
         }

         StringBuilder xml = new StringBuilder();
         xml.Append("<TRIP>");
         xml.Append("<UAFT>" + flightId + "</UAFT>");//Flight UrNo
         xml.Append("<TRPTYPE>" + stTripType + "</TRPTYPE>");//(Trip Type, F  First trip, N  Next Trip, L Last Trip)
         xml.Append("<SR>" + stSendRecv + "</SR>");//(Send or Receive, S Send, R  Receive)
         xml.Append("<SRDT>" + sentDateTime + "</SRDT>");//(Send or Receive Date and Time)
         xml.Append("<SRBY>" + sentBy + "</SRBY>");//(Sent or Received by User Id)
         xml.Append(GenXmlForUldsForSendingToBackend(cpmList));
         xml.Append("</TRIP>");
         ITrekWMQClass1 wmq = new ITrekWMQClass1();
         iTXMLPathscl path = new iTXMLPathscl();
         wmq.putTheMessageIntoQueue(xml.ToString(), path.GetFROMITREKUPDATEQueueName());
      }

      private static string GenXmlForUldsForSendingToBackend(EntCPMList cpmList)
      {
         StringBuilder xml = new StringBuilder();
         int cnt = cpmList.Count;
         for (int i = 0; i < cnt; i++)
         {
            EntCPM cpm = cpmList[i];
            xml.Append("<ULD>");//(one ULD)
            xml.Append("<ULDN>" + iTrekUtils.iTUtils.removeBadCharacters(cpm.ContainerNo) + "</ULDN>");//(ULD Number)
            xml.Append("<RMK>" + removeExcessStringInRemarks(iTrekUtils.iTUtils.removeBadCharacters(cpm.RemarkText)) + "</RMK>");//(Remark)
            xml.Append("<DES>" + cpm.Destination + "</DES>");//(Destination)
            if (cpm.AllowToChangeContainerNo)
            {
               xml.Append("<ISCPM>N</ISCPM>");//(Is CPM/Free, Y  CPM, Otherwise  Free)
            }
            else
            {
               xml.Append("<ISCPM>Y</ISCPM>");//(Is CPM/Free, Y  CPM, Otherwise  Free)
            }
            xml.Append("</ULD>");
         }

         return xml.ToString();
      }
      private static string GenXmlForUldsForSendingToBackend(ArrayList cpmList)
      {
         StringBuilder xml = new StringBuilder();
         int cnt = cpmList.Count;
         for (int i = 0; i < cnt; i++)
         {
            string cpm = cpmList[i].ToString();
            xml.Append("<ULD>");//(one ULD)
            xml.Append("<ULDN>" + cpm + "</ULDN>");//(ULD Number)
            xml.Append("<RMK></RMK>");//(Remark)
            xml.Append("<DES></DES>");//(Destination)

            xml.Append("<ISCPM>N</ISCPM>");//(Is CPM/Free, Y  CPM, Otherwise  Free)

            xml.Append("</ULD>");
         }

         return xml.ToString();
      }

      private static string GenXmlForUldsandUpdateIndicator(EntCPMList cpmList, string flightId)
      {
         StringBuilder xml = new StringBuilder();
         int cnt = cpmList.Count;

         ArrayList urnoList = new ArrayList();
         for (int i = 0; i < cnt; i++)
         {
            EntCPM cpm = cpmList[i];
            xml.Append("<ULD>");//(one ULD)
            xml.Append("<ULDN>" + iTrekUtils.iTUtils.removeBadCharacters(cpm.ContainerNo) + "</ULDN>");//(ULD Number)
            xml.Append("<RMK>" + removeExcessStringInRemarks(iTrekUtils.iTUtils.removeBadCharacters(cpm.RemarkText)) + "</RMK>");//(Remark)
            xml.Append("<DES>" + cpm.Destination + "</DES>");//(Destination)
            if (cpm.AllowToChangeContainerNo)
            {
               xml.Append("<ISCPM>N</ISCPM>");//(Is CPM/Free, Y  CPM, Otherwise  Free)
            }
            else
            {
               xml.Append("<ISCPM>Y</ISCPM>");//(Is CPM/Free, Y  CPM, Otherwise  Free)
            }
            xml.Append("</ULD>");

            urnoList.Add(cpm.Urno);

         }

         DBCpm.GetInstance().UpdateSentIndicatorbyUrno(urnoList, flightId);
         return xml.ToString();
      }

      private static string GenXmlForUldsandUpdateIndicatorInContainer(EntCPMList cpmList, string flightId)
      {
         StringBuilder xml = new StringBuilder();
         int cnt = cpmList.Count;

         ArrayList urnoList = new ArrayList();
         for (int i = 0; i < cnt; i++)
         {
            EntCPM cpm = cpmList[i];
            xml.Append("<ULD>");//(one ULD)
            xml.Append("<ULDN>" + iTrekUtils.iTUtils.removeBadCharacters(cpm.ContainerNo) + "</ULDN>");//(ULD Number)
            xml.Append("<RMK>" + removeExcessStringInRemarks(iTrekUtils.iTUtils.removeBadCharacters(cpm.RemarkText)) + "</RMK>");//(Remark)
            xml.Append("<DES>" + cpm.Destination + "</DES>");//(Destination)
            if (cpm.AllowToChangeContainerNo)
            {
               xml.Append("<ISCPM>N</ISCPM>");//(Is CPM/Free, Y  CPM, Otherwise  Free)
            }
            else
            {
               xml.Append("<ISCPM>Y</ISCPM>");//(Is CPM/Free, Y  CPM, Otherwise  Free)
            }
            xml.Append("</ULD>");
            urnoList.Add(iTrekUtils.iTUtils.removeBadCharacters(cpm.ContainerNo));
         }
         DBContainer.GetInstance().UpdateUldSendIndicator(flightId, urnoList);
         return xml.ToString();
      }
      //public static void ReceiveTrip(string flightId, ArrayList urnoList, 
      //   string sentDateTime, string sentBy, EnTripType tripType, EnTripSendRecv sendRecv)
      //{
      //   //EntCPMList ls = null;
      //   //string sentDateTime = Util.UtilTime.ConvDateTimeToUfisFullDTString(dtSentDateTime);
      //   string stTripType = "";
      //   // ls = cpmList.GetSelectedList(ls);
      //   switch (tripType)
      //   {
      //      case EnTripType.FirstTrip:
      //         stTripType = FIRST_TRIP_IND;
      //         break;
      //      case EnTripType.LastTrip:
      //         stTripType = LAST_TRIP_IND;
      //         break;
      //      case EnTripType.NextTrip:
      //         stTripType = NEXT_TRIP_IND;
      //         break;
      //   }

      //   string stSendRecv = "";

      //   switch (sendRecv)
      //   {
      //      case EnTripSendRecv.Receive:
      //         stSendRecv = "R";
      //         break;
      //      case EnTripSendRecv.Send:
      //         stSendRecv = "S";
      //         break;
      //      default:
      //         break;
      //   }

      //   StringBuilder xml = new StringBuilder();
      //   xml.Append("<TRIP>");
      //   xml.Append("<UAFT>" + flightId + "</UAFT>");//Flight UrNo
      //   xml.Append("<TRPTYPE>" + stTripType + "</TRPTYPE>");//(Trip Type, F  First trip, N  Next Trip, L Last Trip)
      //   xml.Append("<SR>" + stSendRecv + "</SR>");//(Send or Receive, S Send, R  Receive)
      //   xml.Append("<SRDT>" + sentDateTime + "</SRDT>");//(Send or Receive Date and Time)
      //   xml.Append("<SRBY>" + sentBy + "</SRBY>");//(Sent or Received by User Id)
      //   xml.Append(GenXmlForUrnoForSendingToBackend(urnoList, sentDateTime, sentBy, stTripType));
      //   xml.Append("</TRIP>");
      //   ITrekWMQClass1 wmq = new ITrekWMQClass1();
      //   iTXMLPathscl path = new iTXMLPathscl();
      //   wmq.putTheMessageIntoQueue(xml.ToString(), path.GetFROMITREKUPDATEQueueName());
      //}

      private static string GenXmlForUrnoForSendingToBackend(List<string> cpmList, string datetime, string sentBy, string stTripType)
      {
         StringBuilder xml = new StringBuilder();
         int cnt = cpmList.Count;
         for (int i = 0; i < cnt; i++)
         {
            //string cpm = cpmList[i].ToString();
            string cpm = cpmList[i];
            xml.Append("<URNO>" + cpm + "</URNO>");//(ULD Number)                

         }
         UpdateTripsReceivedToTripXmlFile(cpmList, datetime, sentBy, stTripType);
         return xml.ToString();
      }

      public static void UpdateTripsReceivedToTripXmlFile(ArrayList cpmList, string datetime, string sentBy, string stTripType)
      {
         DBTrip.GetInstance().UpdateTripsReceivedToTripXmlFile(cpmList, datetime, sentBy, stTripType);
         // DBCpm.GetInstance().UpdateSentIndicator(xmlDoc);
      }

      public static void UpdateTripsReceivedToTripXmlFile(List<string> cpmList, string datetime, string sentBy, string stTripType)
      {
         ArrayList arr = new ArrayList(cpmList);
         DBTrip.GetInstance().UpdateTripsReceivedToTripXmlFile(arr, datetime, sentBy, stTripType);
         // DBCpm.GetInstance().UpdateSentIndicator(xmlDoc);
      }

      public static void UpdateTripsInfoFromBackendToTripXmlFile(XmlDocument xmlDoc)
      {
         DBTrip.GetInstance().UpdateTripsInfoFromBackendToTripXmlFile(xmlDoc);
         DBCpm.GetInstance().UpdateSentIndicator(xmlDoc);
      }

      public static void UpdateTripsFieldsToTripXmlFile(XmlDocument xmlDoc)
      {
         DBTrip.GetInstance().UpdateTripsFieldsToTripXmlFile(xmlDoc);
         // DBCpm.GetInstance().UpdateSentIndicator(xmlDoc);
      }

      //[Conditional("TESTING_ON")]
      public static void TestUpdateXmlFile()
      {
         //string stXml = "<TRIPS>" +
         //        "<TRIP>" + //					(Trip)
         //            "<URNO>1</URNO>" + //			(ULD Trip number)
         //            "<UAFT>1234</UAFT >" + //			(Flight Urno)
         //            "<ULDN>11L/1/SIN/B</ULDN>" + //			(ULD Number)
         //            "<DES>SIN</DES>" + //			(Destination)
         //            "<STRPNO>1</STRPNO>" + //		(Sent Trip Number)
         //            "<SEND_DT>20070212105000</SEND_DT>" + //		(Sent Date Time)
         //            "<SEND_BY>ao1</SEND_BY>" +
         //            "<SEND_RMK>RMK1 FOR SENDING</SEND_RMK>" + //	(Sent Remarks)
         //            "<RTRPNO></RTRPNO>" + //		(Receive Trip Number)
         //            "<RCV_DT></RCV_DT>" + //		(Receive Date Time)
         //            "<RCV_BY></RCV_BY>" +
         //            "<RCV_RMK></RCV_RMK>" + //	(Receive Remark)
         //            "<ISCPM>Y</ISCPM>" + //			(Is CPM, Y  CPM, Otherwise - Free)
         //        "</TRIP>" +
         //    "</TRIPS>";
         //XmlDocument xmlDoc = new XmlDocument();
         //xmlDoc.LoadXml(stXml);
         //UpdateTripsInfoFromBackendToTripXmlFile(xmlDoc);
      }

      public static int GetContainerCountForAFlight(string flightUrNo)
      {
         return RetrieveTrip(flightUrNo).TRIP.Rows.Count;
      }

      public static String GetArrFlightUldAOComments(string flightUrno)
      {
         DSTrip ds = null;
         DBTrip dbTrip = DBTrip.GetInstance();
         ds = dbTrip.RetrieveTripForAFlight(flightUrno);
         ArrayList arr = new ArrayList();
         string uldComments = "";
         //foreach (DSTrip.TRIPRow row in ds.TRIP.Select("SENT_RMK<>''", "SENT_DT,ULDN"))
         //foreach (DSTrip.TRIPRow row in ds.TRIP.Select("", "SENT_DT,ULDN"))
         foreach (DSTrip.TRIPRow row in ds.TRIP.Select("", "STRPNO ASC,ULDN"))
         {
            string uldNo = "";
            string sentRmk = "";
            string sTripNo = "";
            string des = "";

            uldNo = (string)row.ULDN;
            try
            {
               sentRmk = (string)row.SENT_RMK;
               sTripNo = (string)row.STRPNO;
               des = (string)row.DES;
               if (des.Trim() != "") des = des.Trim() + ",";
            }
            catch { }
            //if (sentRmk != "")
            //{
            //    //arr.Add(uldNo + ", " + sentRmk);
            //    uldComments += sTripNo+ ". " + des + ", " + uldNo + ", " + sentRmk + "\n";
            //}
            //Add des (INTER/LOCAL) by PRF 56(A)
            //User want to see the trip information even though no remark. PRF 56(B)
            uldComments += sTripNo + ". " + des + uldNo + ", " + sentRmk + "\n";
         }
         //return arr;
         return uldComments;
      }

      private static string removeExcessStringInRemarks(string rmkMessage)
      {
         if (rmkMessage.Length > 255)
         {
            rmkMessage = rmkMessage.Substring(0, 255);
         }
         return rmkMessage;

      }

      public const string TRIP_RECEIVE_IND = "R";
      public const string TRIP_SEND_IND = "S";

      ///// <summary>
      ///// Compute the Current Receive Trip Type
      ///// </summary>
      ///// <param name="flightId"></param>
      ///// <param name="hasUlds"></param>
      ///// <param name="hasFirstTrip"></param>
      ///// <param name="hasLastTrip"></param>
      ///// <param name="hasUnSentUld"></param>
      ///// <param name="hasUnRecvUld"></param>
      ///// <returns></returns>
      //public static string ComputeCurrentReceiveTripType(string flightId,
      //    out bool hasUlds,
      //    out bool hasFirstTrip, out bool hasLastTrip,
      //    out bool hasUnSentUld, out bool hasUnRecvUld)
      //{
      //   string result = "";
      //   hasUlds = false;
      //   hasFirstTrip = false;
      //   hasLastTrip = false;
      //   hasUnSentUld = true;
      //   hasUnRecvUld = true;

      //   return result;
      //}

      /// <summary>
      /// Convert the Trip Type to String
      /// </summary>
      /// <param name="tripType">Trip Type</param>
      /// <returns>Trip Type in String</returns>
      public static string ConvTripTypeToString(EnTripType tripType)
      {
         string stTripType = "";
         switch (tripType)
         {
            case EnTripType.FirstTrip:
               stTripType = FIRST_TRIP_IND;
               break;
            case EnTripType.LastTrip:
               stTripType = LAST_TRIP_IND;
               break;
            case EnTripType.NextTrip:
               stTripType = NEXT_TRIP_IND;
               break;
            case EnTripType.ToAutoDefine:
               //To auto define the trip type.
               throw new ApplicationException("Undefined Trip Type.");
               //break;
            default:
               throw new ApplicationException("Invalid Trip Type.");
            //break;
         }
         return stTripType;
      }

      /// <summary>
      /// Define the Arrival Flight Container Receive Trip Type automatically
      /// </summary>
      /// <param name="flightId">Flight Id</param>
      /// <param name="tripIdList">Uld Ids (currently going to acknowledge as received)</param>
      /// <returns>Trip Type (First/Last/Next Trip)</returns>
      public static EnTripType AutoDefineArrRecvTripType(string flightId, List<string> tripIdList)
      {
         //1. if no first received trip for the flight it is first trip
         //2. else if no sent container left for the flight, it is last trip
         //3. else it is next trip.
         EnTripType tripType = EnTripType.None;
         if (IsArrRecvFirstTrip(flightId, tripIdList)) tripType = EnTripType.FirstTrip;
         else if (IsArrRecvLastTrip(flightId, tripIdList)) tripType = EnTripType.LastTrip;
         else tripType = EnTripType.NextTrip;
         return tripType;
      }


      /// <summary>
      /// Get Received Trip Type String
      /// </summary>
      /// <param name="flightId">Flight Id</param>
      /// <param name="urnoList">Urno List of Containers in Trip</param>
      /// <param name="tripType">Trip Type</param>
      /// <returns>Trip Type in String</returns>
      private static string GetRecvTripTypeString(string flightId, List<string> urnoList, EnTripType tripType)
      {
         if (tripType == EnTripType.ToAutoDefine)
         {
            tripType = AutoDefineArrRecvTripType(flightId, urnoList);
            LogTraceMsg( "AutoDefine Trip:" + flightId + ",[" + 
               MM.UtilLib.UtilMisc.ConvListToString( urnoList, "," ) + "]," + tripType);
         }

         return ConvTripTypeToString(tripType); 
      }

      private static void LogTraceMsg( string msg )
      {
         try
         {
            MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("CtrlTrip", msg);
         }
         catch (Exception)
         {
            UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("CtrlTrip", msg);
         }
      }

      /// <summary>
      /// Is First Trip for Receiving Container
      /// </summary>
      /// <param name="flightId">Flight Id</param>
      /// <param name="urnoList">Container List To send</param>
      /// <returns></returns>
      private static bool IsArrRecvFirstTrip(string flightId, List<string> tripIdList)
      {
         bool firstTrip = false;
         DateTime dtTime ;
         firstTrip = !CtrlFlight.IsArrFlightTimeExist(flightId, CtrlFlight.EnmArrFlightTimeType.ArrBagFirstTripTime, out dtTime);
         return firstTrip;
      }

      /// <summary>
      /// Is it the Last Trip for Receiving Container
      /// </summary>
      /// <param name="flightId">Flight Id</param>
      /// <param name="urnoList">Container List To send</param>
      /// <returns></returns>
      private static bool IsArrRecvLastTrip(string flightId, List<string> tripIdList)
      {
         bool lastTrip = false;
         DateTime dtTime;
         bool isAprSentLastTripExist = CtrlFlight.IsArrFlightTimeExist(flightId, CtrlFlight.EnmArrFlightTimeType.ArrAprLastTripTime, out dtTime);
         if (isAprSentLastTripExist)
         {
            //Check any container left out to receive?
            EntBagArrContainer entBAContainer = CtrlBagArrival.GetTripInfo(flightId, EnBagArrType.All);
            Hashtable ht = entBAContainer.UnReceivedTripList.GetUnRecvUlds();
            if (ht == null) throw new ApplicationException("No unreceived containers.");

            //Filter the duplicate in Receive Urno List - Start
            Hashtable htRecv = new Hashtable();
            int cntRecv = tripIdList.Count;
            for (int i = cntRecv - 1; i >= 0; i--)
            {
               if (htRecv.ContainsKey(tripIdList[i])) tripIdList.RemoveAt(i);
               else htRecv.Add(tripIdList[i], tripIdList[i]);
            }
            //Filter the duplicate in Receive Urno List - End

            cntRecv = tripIdList.Count;

            int cntUnRecv = ht.Count;
            if (cntRecv==cntUnRecv) 
            {
               lastTrip = true;
               for (int i = 0; i < cntRecv; i++)
               {
                  string id = tripIdList[i];
                  if (!ht.ContainsKey(id))
                  {
                     lastTrip = false;
                     break;
                  }
               }
            }
         }

         return lastTrip;
      }

      /// <summary>
      /// Receiving the Containers as a trip for a flight
      /// </summary>
      /// <param name="flightId">Flight Id</param>
      /// <param name="tripIdList">List of containers</param>
      /// <param name="dtRecvDateTime">Received Date Time</param>
      /// <param name="recvBy">Received by</param>
      /// <param name="tripType">Trip Type</param>
      public static void ReceiveTrip(string flightId, List<string> tripIdList,
          DateTime dtRecvDateTime, string recvBy, EnTripType tripType)
      {
         try
         {
            string stTripType = "";
            stTripType = GetRecvTripTypeString(flightId, tripIdList, tripType);
            string sentDateTime = UtilTime.ConvDateTimeToUfisFullDTString(dtRecvDateTime);

            string stSendRecv = TRIP_RECEIVE_IND;

            StringBuilder xml = new StringBuilder();
            xml.Append("<TRIP>");
            xml.Append("<UAFT>" + flightId + "</UAFT>");//Flight UrNo
            xml.Append("<TRPTYPE>" + stTripType + "</TRPTYPE>");//(Trip Type, F  First trip, N  Next Trip, L Last Trip)
            xml.Append("<SR>" + stSendRecv + "</SR>");//(Send or Receive, S Send, R  Receive)
            xml.Append("<SRDT>" + sentDateTime + "</SRDT>");//(Send or Receive Date and Time)
            xml.Append("<SRBY>" + recvBy + "</SRBY>");//(Sent or Received by User Id)
            xml.Append(GenXmlForUrnoForSendingToBackend(tripIdList, sentDateTime, recvBy, stTripType));
            xml.Append("</TRIP>");
            ITrekWMQClass1 wmq = new ITrekWMQClass1();
            iTXMLPathscl path = new iTXMLPathscl();
            if (wmq.putTheMessageIntoQueue(xml.ToString(), path.GetFROMITREKUPDATEQueueName()).ToUpper().Trim() == "SUCCESS")
            {//Update to the arrival flight timing of First Trip of Receiving
               if (stTripType == FIRST_TRIP_IND)
               {
                  CtrlFlight.SaveArrBagTimingByTag(flightId, recvBy, sentDateTime, CtrlFlight.BA_FRIST_TRIP_TIME_TAG);
               }
               else if (stTripType == LAST_TRIP_IND )
               {
                  CtrlFlight.SaveArrBagTimingByTag(flightId, recvBy, sentDateTime, CtrlFlight.BA_LAST_TRIP_TIME_TAG);
               }
            }
         }
         catch (Exception ex)
         {
            LogTraceMsg("ReceiveTrip:Err:" + ex.Message);
            throw;
         }
      }

      public static void UpdateLastTrip(string FlightURNO, string StaffID)
      {
         try
         {
            if (CtrlTrip.RetrieveTrip(FlightURNO).TRIP.Select("RTTY <> 'F'").Length == 0 && CtrlTrip.RetrieveTripNotReceived(FlightURNO).TRIP.Count == 0)
            {
               DSArrivalFlightTimings dsArrFlightTiming = DBFlight.GetInstance().RetrieveArrivalFlightTimings(FlightURNO);

               DSArrivalFlightTimings.AFLIGHTRow row = null;
               if (dsArrFlightTiming.AFLIGHT.Rows.Count > 0)
               {
                  row = ((DSArrivalFlightTimings.AFLIGHTRow)dsArrFlightTiming.AFLIGHT.Rows[0]);
               }
               string ltrip_new = "";
               string ltrip_old = "";
               try
               {
                  ltrip_old = row._LTRIP_B;
               }
               catch (Exception)
               {
               }
               try
               {
                  if (ltrip_old == "" || ltrip_old == "-")
                  {
                     ltrip_new = row._FTRIP_B;
                  }
               }
               catch (Exception)
               {
               }

               if (ltrip_new != "")
               {
                 // CtrlFlight.UpdateArrivalFlightTiming(FlightURNO, "LTRIP-B", ltrip_new, StaffID);
               }
            }

         }
         catch (Exception ex)
         {
            throw new ApplicationException("Unable to Update Baggage Last Trip for Arrival Flight.");
         }
      }
   }
}