using System;
using System.Collections.Generic;
using System.Text;

using UFIS.ITrekLib.DB;
using UFIS.ITrekLib.DS;

namespace UFIS.ITrekLib.Ctrl
{
    public class CtrlPreDefBagConRmk
    {
        private const int MAX_MSG_LEN = 250;

        public static void Init()
        {
            DBPreDefBagConRmk.GetInstance().Init();
        }

        public static DSPreDefBagConRmk RetrieveActiveRmk()
        {
            return DBPreDefBagConRmk.GetInstance().RetrieveActiveRmk();
        }

        public static void CreatePreDefBagConRmk(string msg)
        {
            DBPreDefBagConRmk db = DBPreDefBagConRmk.GetInstance();
            if (msg.Length > MAX_MSG_LEN) msg = msg.Substring(0, MAX_MSG_LEN);
            db.CreatePreDefBagConRmk(msg);
        }

        public static void DeletePreDefBagConRmk(string msgId)
        {
            DBPreDefBagConRmk.GetInstance().DeletePreDefBagConRmkById(msgId);
        }

        public static void UpdatePreDefBagConRmk(string msgId, string msg)
        {
            if (msg.Length > MAX_MSG_LEN) msg = msg.Substring(0, MAX_MSG_LEN);
            DBPreDefBagConRmk.GetInstance().UpdatePreDefBagConRmk(msgId, msg);
        }
    }
}
