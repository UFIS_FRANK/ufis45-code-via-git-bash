using System;
using System.Collections.Generic;
using System.Text;

using UFIS.ITrekLib.Util;

namespace UFIS.ITrekLib.ENT
{
    public class EntFlightTiming
    {
        private string xmlTagNameInFile = "";
        private string ufisTiming = "";

        public string ColumnName
        {
            get
            {
                if (xmlTagNameInFile == null) xmlTagNameInFile = "";
                return xmlTagNameInFile;
            }
            set
            {
                if (value == null) value = "";
                xmlTagNameInFile = value;
            }
        }

        public string TimingUfis
        {
            get
            {
                return ufisTiming;
            }
            set
            {
                if (value == null) value = "";
                ufisTiming = value;
            }
        }
    }
}
