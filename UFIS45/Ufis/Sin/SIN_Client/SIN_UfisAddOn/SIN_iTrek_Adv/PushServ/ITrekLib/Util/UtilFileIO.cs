using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using MM.UtilLib;

namespace UFIS.ITrekLib.Util
{
    public class UtilFileIO
    {
        /// <summary>
        /// Delete a folder
        /// </summary>
        /// <param name="fullFolderName">folder name to delete</param>
        public static void DeleteAFolder(string fullFolderName)
        {
            try
            {
                Directory.Delete(fullFolderName, true);
            }
            catch (Exception )
            {
            }
        }

        /// <summary>
        /// Delete a file
        /// </summary>
        /// <param name="fullFileName">File Name to delete</param>
        public static void DeleteAFile(string fullFileName)
        {
            try
            {
                //FileInfo file = new FileInfo(fullFileName);
                //if (file.Exists)
                if(File.Exists(fullFileName))
                {
                    File.Delete(fullFileName);
                }
            }catch{}
        }

        /// <summary>
        /// Is File Exist
        /// </summary>
        /// <param name="fullFileName">Full File Name</param>
        /// <returns>true-Exist</returns>
        public static bool IsFileExist(string fullFileName)
        {
            bool exist = false;
            try
            {
                //FileInfo file = new FileInfo(fullFileName);
                //exist = (file.Exists);
                exist = File.Exists(fullFileName);
            }
            catch { }
            return exist;
        }

        /// <summary>
        /// Create a sub directory under the parent directory
        /// </summary>
        /// <param name="parentDir">parent Directory</param>
        /// <param name="newDirName">new directory name to create under the parent directory</param>
        public static void CreateSubDirectory(string parentDir, string newDirName)
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo(parentDir);
                di.CreateSubdirectory(newDirName);
            }
            catch { }
        }

        /// <summary>
        /// Create a file if it is not existed
        /// </summary>
        /// <param name="fullFileName">File Name to create</param>
        public static void CreateAFile(string fullFileName)
        {
            try
            {
                
                //FileInfo file = new FileInfo(fullFileName);
                //if (!file.Exists)
                if (File.Exists( fullFileName ))
                {
                    StreamWriter sw = File.CreateText(fullFileName);
                    sw.AutoFlush= true;
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Write to text file by appending the given string at the bottom of the file
        /// </summary>
        /// <param name="fullFileName">Full File Name to append the text</param>
        /// <param name="stToWrite">string to write</param>
        public static void WriteToTextFile(string fullFileName, string stToWrite)
        {
            //CreateAFile(fullFileName);
            //StreamWriter sw = File.AppendText(fullFileName);
            try
            {
                File.AppendAllText(fullFileName, stToWrite + Environment.NewLine);
                //StreamWriter sw = File.CreateText(fullFileName);
                //sw.WriteLine(stToWrite);
                //sw.AutoFlush();
                //sw.Flush();
                //sw.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }          
        }

        /// <summary>
        /// Get the File Stream for given file name for reading lock
        /// </summary>
        /// <param name="fileName">File Name</param>
        /// <param name="retryCount">Retry Count in case of any error reading file</param>
        /// <returns>FileStream of given file</returns>
        public static FileStream GetFileStreamForReading(string fileName, int retryCount)
        {
            FileStream file = null;
            if (retryCount == -1) retryCount = 1000;
            else if (retryCount < 10) retryCount = 10;

            for (int i = 0; i < retryCount; i++)
            {
                try
                {
                    file = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read);
                    if (file != null) break;
                }
                catch (System.IO.IOException)
                {//Might be caused by file lock
                    Random rdm = new Random();
                    System.Threading.Thread.Sleep(rdm.Next(10, 50));
                }
                catch (Exception)
                {
                    throw;
                }
            }
            if (file == null) throw new ApplicationException("Unable to read " + UtilMisc.GetFileNameOnly(fileName));
            return file;
        }

        /// <summary>
        /// Get FileStream for file name for Writing lock
        /// </summary>
        /// <param name="fileName">File Name</param>
        /// <param name="retryCount">Retry Count in case of error opening file</param>
        /// <returns>FileStream</returns>
        public static FileStream GetFileStreamForWriting(string fileName, int retryCount)
        {
            FileStream file = null;
            if (retryCount == -1) retryCount = 1000;
            else if (retryCount < 50) retryCount = 50;

            for (int i = 0; i < retryCount; i++)
            {
                try
                {
                    file = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.None);
                    if (file != null)
                    {
                        break;
                    }
                }
                catch (System.IO.IOException)
                {
                    Random rdm = new Random();
                    System.Threading.Thread.Sleep(rdm.Next(2, 30));
                }
                catch (Exception ex)
                {
                    LogTraceMsg("GetFileStreamForWriting:" + UtilMisc.GetFileNameOnly(fileName) + ":Err:" + ex.StackTrace + "\n" + ex.ToString());
                    throw ex;
                }
            }
            return file;
        }

        /// <summary>
        /// Copy the file with locking the source file and target file
        /// </summary>
        /// <param name="srcFile">Source File</param>
        /// <param name="destFile">Destination File</param>
        /// <param name="overWrite">overwrite the destination file if it is already existed</param>
        /// <param name="isSrcFileExist">Is the Source File existed?</param>
        /// <returns>true - successfully copied. Else - Not success to copy</returns>
        public static bool CopyFileWithLock(string srcFile, string destFile, bool overWrite, out bool isSrcFileExist)
        {
            bool copied = false;
            isSrcFileExist = File.Exists(srcFile);

            if (!string.IsNullOrEmpty(srcFile) && isSrcFileExist)
            {
                using (FileStream fsSrc = GetFileStreamForReading(srcFile, 10))
                {
                    if (fsSrc != null)
                    {
                        long fileLength = fsSrc.Length;
                        try
                        {
                            fsSrc.Lock(0, fileLength);
                            using (FileStream fsDest = GetFileStreamForWriting(destFile, 100))
                            {
                                if (fsDest != null)
                                {
                                    try
                                    {
                                        for (long i = 0; i < fileLength; i++)
                                        {
                                            fsDest.WriteByte((byte)fsSrc.ReadByte());
                                        }
                                        copied = true;
                                    }
                                    catch (Exception)
                                    {
                                    }
                                    finally
                                    {

                                        try
                                        {
                                            fsDest.Dispose();
                                            fsDest.Close();
                                        }
                                        catch (Exception)
                                        {
                                        }
                                    }                                    
                                }                                
                            }                            
                        }
                        catch (Exception ex)
                        {
                            string st = ex.Message;
                            LogTraceMsg(ex.Message);
                        }
                        finally
                        {
                            try
                            {
                                fsSrc.Unlock(0, fileLength);
                            }
                            catch (Exception ex)
                            {
                                LogTraceMsg(ex.Message);
                            }
                            fsSrc.Dispose();
                            fsSrc.Close();
                        }
                    }
                }
            }
            return copied;
        }

        /// <summary>
        /// Log to trace File
        /// </summary>
        /// <param name="msg">message to log</param>
        private static void LogTraceMsg(string msg)
        {
            UtilLog.LogToTraceFile("UtilFileIO", msg);
        }

    }
}
