using System;
using System.Collections.Generic;
using System.Text;

using System.Collections;
using System.Xml.XPath;

using UFIS.ITrekLib.Util;

using MM.UtilLib;

namespace UFIS.ITrekLib.Config
{
	public class ITrekPath
	{
		private static Hashtable _htKeyValue = new Hashtable();
		private static DateTime _dtLastUpdDateTime = UtilTime.DEFAULT_DATE_TIME;
		private static ITrekPath _this = null;
		private static object _lockThis = new object();

		#region MQ Queuq Name constants
		private const string MQ_TOITREK_READ_RES = "MQ_TOITREK_READ_RES";
		private const string MQ_TOITREK_JOB = "MQ_TOITREK_JOB";
		private const string MQ_TOITREK_AFT = "MQ_TOITREK_AFT";
		private const string MQ_TOITREK_AUTH = "MQ_TOITREK_AUTH";

		private const string MQ_FRITREK_SELECT = "MQ_FRITREK_SELECT";
		private const string MQ_FRITREK_UPDATE = "MQ_FRITREK_UPDATE";

		private const string MQ_FRITREK_AUTH = "MQ_FRITREK_AUTH";
		private const string MQ_FRITREK_CPM = "MQ_FRITREK_CPM";
		#endregion

		#region Timer Time constants
		private const string TIMER_TIME_MGO_SVC = "TIMER_TIME_MGO_SVC";
		#endregion

		private ITrekPath()
		{
			PopulateInfo();
		}

		public static ITrekPath GetInstance()
		{
			if (_this == null)
			{
				lock (_lockThis)
				{
					if (_this == null)
					{
						_this = new ITrekPath();
					}
				}
			}
			return _this;
		}

		public void Init()
		{
			_this = null;
			_dtLastUpdDateTime = UtilTime.DEFAULT_DATE_TIME;
		}

		private XPathDocument _xmlDoc = null;
		private XPathNavigator _xmlNav = null;

		private string GetConfigElmt(string expression)
		{
			if ((_xmlDoc == null) || (_xmlNav == null))
			{
				_xmlDoc = new XPathDocument(GetPathsXmlFilePath());
				_xmlNav = _xmlDoc.CreateNavigator();
			}

			return _xmlNav.SelectSingleNode(expression).ToString();
		}

		private string GetPathsXmlFilePath()
		{
			return ITrekConfig.GetPathsXmlFilePath();
		}

		private void PopulateInfo()
		{
			_xmlDoc = new XPathDocument(GetPathsXmlFilePath());
			_xmlNav = _xmlDoc.CreateNavigator();

			GetInfoForIdAndAddToLookupTable(MQ_TOITREK_READ_RES);
			GetInfoForIdAndAddToLookupTable(MQ_TOITREK_JOB);

		}

		private string GetInfoForIdAndAddToLookupTable(string infoId)
		{
			string value = "";
			infoId = infoId.Trim().ToUpper();

			switch (infoId)
			{
				case MQ_TOITREK_READ_RES:
					value = GetConfigElmt("//Paths/TOITREKREAD_RES");
					break;
				case MQ_TOITREK_JOB:
					value = GetConfigElmt("//Paths/TOITREKREAD_JOB");
					break;
				case TIMER_TIME_MGO_SVC:
					value = GetConfigElmt("//Paths/MGO_SERVICESTIMER");
					break;
				case MQ_FRITREK_UPDATE:
					value = GetConfigElmt("//Paths/FROMITREKUPDATE");
					break;
				default:
					throw new ApplicationException("Unknown Info Id [" + infoId + "]");
			}
			if (_htKeyValue.ContainsKey(infoId))
			{
				_htKeyValue[infoId] = value;
			}
			else _htKeyValue.Add(infoId, value);

			return value;
		}

		private string GetValueForId(string infoId)
		{
			string value = "";
			if (_htKeyValue.ContainsKey(infoId))
			{
				value = _htKeyValue[infoId].ToString();
			}
			else
			{
				value = GetInfoForIdAndAddToLookupTable(infoId);
			}

			return value;
		}

		#region Get Itrek MQ Info
		public string GetTOITREKREAD_JOBQueueName()
		{
			return GetValueForId(MQ_TOITREK_JOB);
		}

		public string GetTOITREKREAD_RESQueueName()
		{
			return GetValueForId(MQ_TOITREK_READ_RES);
		}

		public string GetFROMITREKUPDATEQueueName()
		{
			return GetValueForId(MQ_FRITREK_UPDATE);
		}

		#endregion

		#region Get ITrek Services Timer Info
		public static int GetTimerForMgoSvc()
		{
			string stTime = ITrekPath.GetInstance().GetValueForId(TIMER_TIME_MGO_SVC);
			int time = 1;//Default to 1 sec
			try
			{
				time = Convert.ToInt32(stTime);
			}
			catch (Exception)
			{
			}
			if (time <= 0) time = 1;
			return time;
		}
		#endregion

	}
}