# define TestingOn

using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using iTrekXML;
using UFIS.ITrekLib.DS;
using System.IO;
using UFIS.ITrekLib.Util;
using System.Xml;
using System.Xml.XPath;
using System.Data;

namespace UFIS.ITrekLib.DB
{
    public class DBContainer
    {
        private static Object _lockObj = new Object();//to use in synchronisation of single access
        private static DBContainer _dbContainer = null;

        private static DSContainer _dsContainer = null;
        private static string _fnContainerXml = null;//file name of pdm XML in full path.
     
         private static readonly DateTime DEFAULT_DT = new System.DateTime(1,1,1);
        private static DateTime _lastContainerWriteDT = DEFAULT_DT;  
     

        /// <summary>
        /// Single Pattern.
        /// Not allowed to create the object from other class.
        /// To get the object, user "GetInstance()" method.
        /// </summary>
        private DBContainer() { }

        public void Init()
        {//Initialize all the information, to get the latest data when fetching them again.
            _dsContainer = null;
           _fnContainerXml = null; 
           _lastContainerWriteDT = DEFAULT_DT;
        
        }

        /// <summary>
        /// Get the Instance of DBCpm. There will be only one reference for all the clients
        /// </summary>
        /// <returns></returns>
        public static DBContainer GetInstance()
        {

            if (_dbContainer == null)
            {
                _dbContainer = new DBContainer();
            }
            return _dbContainer;
        }

        public DSContainer RetrieveUldNoNotSend(string uaft)
        {
            DSContainer ds = new DSContainer();
            DSContainer tempDs = dsContainer;
           
            foreach (DSContainer.ContainerRow row in tempDs.Container.Select("UAFT='" + uaft + "' AND SEND_IND<>'Y'"))
            {
               
                    ds.Container.LoadDataRow(row.ItemArray, true);
                    //int len=row.GetChildRows("MSG_MSG_TO").Length;
                    
                  
            }
                
                
            

            return ds;
        }

        public DSContainer RetrieveUldNos(string uaft)
        {
            DSContainer ds = new DSContainer();
            DSContainer tempDs = dsContainer;

            foreach (DSContainer.ContainerRow row in tempDs.Container.Select("UAFT='" + uaft + "'"))
            {

                ds.Container.LoadDataRow(row.ItemArray, true);
                //int len=row.GetChildRows("MSG_MSG_TO").Length;


            }




            return ds;
        }


        public void UpdateUldSendIndicator(string uaft,ArrayList lstUldNo)
        {
            lock (_lockObj)
            {
                try
                {
                    LockContainerForWriting();
                    DSContainer ds = null;
                    int updCnt = 0;
                    try
                    {
                        ds = new DSContainer();
                        //LogTraceMsg("TripPath:" + fnTripXml);
                        ds.Container.ReadXml(fnContainerXml);
                        if (ds == null)
                        {
                            throw new ApplicationException("Unable to get the Container Information.");
                        }
                    }
                    catch (ApplicationException ex) { throw ex; }
                    catch (Exception ex)
                    {
                        throw new ApplicationException("Unable to get the Container Information due to " + ex.Message);
                    }

                    foreach (string uldNo in lstUldNo)
                    {
                        foreach (DSContainer.ContainerRow row in ds.Container.Select("UAFT='" + uaft + "' AND ULDNO='" + uldNo + "'"))
                        {
                            row.SEND_IND = "Y";
                            updCnt++;
                            //int len=row.GetChildRows("MSG_MSG_TO").Length;

                        }

                    }
                    if (updCnt > 0)
                    {
                        ds.Container.AcceptChanges();
                        ds.Container.WriteXml(fnContainerXml);
                        _dsContainer = ds;
                    }
                }
                catch (Exception ex)
                {
                    LogMsg("UpdateTripsInfoFromBackendToTripXmlFile:Err:" + ex.Message);
                }
                finally
                {
                    ReleaseContainerAfterWrite();
                }
            }
        
        }
        //public void deleteAContainer(string uaft, string uldNo)
        //{
        //    lock (_lockObj)
        //    {
        //        try
        //        {
        //            LockContainerForWriting();
        //            DSContainer ds = null;
        //            int updCnt = 0;
        //            try
        //            {
        //                ds = new DSContainer();
                        
        //                //LogTraceMsg("TripPath:" + fnTripXml);
        //                ds.Container.ReadXml(fnContainerXml);
        //                if (ds == null)
        //                {
        //                    throw new ApplicationException("Unable to get the Container Information.");
        //                }
        //            }
        //            catch (ApplicationException ex) { throw ex; }
        //            catch (Exception ex)
        //            {
        //                throw new ApplicationException("Unable to get the Container Information due to " + ex.Message);
        //            }


        //            foreach (DSContainer.ContainerRow row in ds.Container.Select("UAFT='" + uaft + "' AND ULDNO='" + uldNo + "'"))
        //            {
        //                row.Delete();
        //                updCnt++;
                       
        //                //int len=row.GetChildRows("MSG_MSG_TO").Length;

        //            }
        //            if (updCnt > 0)
        //            {
        //                ds.Container.AcceptChanges();
        //                ds.Container.WriteXml(fnContainerXml);
        //                _dsContainer = ds;
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            LogMsg("UpdateTripsInfoFromBackendToTripXmlFile:Err:" + ex.Message);
        //        }
        //        finally
        //        {
        //            ReleaseContainerAfterWrite();
        //        }
        //    }
        //}

      

        private string fnContainerXml
        {
            get
            {
                if ((_fnContainerXml == null) || (_fnContainerXml == ""))
                {
                    iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
                    _fnContainerXml = path.GetContainerFile();
                }

                return _fnContainerXml;
            }
        }
        

        private DSContainer dsContainer
        {
            get
            {
                 LoadDSContainer(); 
                DSContainer ds = (DSContainer)_dsContainer.Copy();

                return ds;
            }
        }

        private void LoadDSContainer()
        {
            FileInfo fiXml = new FileInfo(fnContainerXml);
       
            DateTime curDT = fiXml.LastWriteTime;
           
            if (!fiXml.Exists ) throw new ApplicationException("Container information missing. Please inform to administrator.");

            if ((_dsContainer == null) || (curDT.CompareTo(_lastContainerWriteDT) != 0)  )
            {

                lock (_lockObj)
                {
                    try
                    {
                        curDT = fiXml.LastWriteTime;

                        if (_dsContainer == null || (curDT.CompareTo(_lastContainerWriteDT) != 0) )
                        {

                            try
                            {
                                DSContainer tempDs = new DSContainer();
                                tempDs.Container.ReadXml(fnContainerXml);

                                _dsContainer = tempDs;
                               
                                //_dsFlight = (DSFlight)tempDs.Copy();
                                _lastContainerWriteDT = curDT;
                                
                            }
                            catch (Exception ex)
                            {
                                LogMsg(ex.Message);
                                // TestWriteCPM();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogMsg("LoadDSContainer:Err:" + ex.Message);
                    }
                }
            }
        }

        private void LogMsg(string msg)
        {
            //Util.UtilLog.LogToGenericEventLog("DBContainer", msg);
            try
            {
               MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("DBContainer", msg);
            }
            catch (Exception)
            {
               UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DBContainer", msg);
            }
        }
       
      

        public void deleteOldContainers(ArrayList lstFlights)
        {
            lock (_lockObj)
            {
                try
                {
                    LockContainerForWriting();
                    DSContainer ds = null;
                    int updCnt = 0;
                    try
                    {
                        ds = new DSContainer();

                        //LogTraceMsg("TripPath:" + fnTripXml);
                        ds.Container.ReadXml(fnContainerXml);
                        if (ds == null)
                        {
                            throw new ApplicationException("Unable to get the Container Information.");
                        }
                    }
                    catch (ApplicationException ex) { throw ex; }
                    catch (Exception ex)
                    {
                        throw new ApplicationException("Unable to get the Container Information due to " + ex.Message);
                    }

                    foreach (string uaft in lstFlights)
                    {
                        foreach (DSContainer.ContainerRow row in ds.Container.Select("UAFT='" + uaft + "'"))
                        {
                            row.Delete();
                            updCnt++;

                            //int len=row.GetChildRows("MSG_MSG_TO").Length;

                        }
                    }
                    if (updCnt > 0)
                    {
                        ds.Container.AcceptChanges();
                        ds.Container.WriteXml(fnContainerXml);
                        _dsContainer = ds;
                    }
                }
                catch (Exception ex)
                {
                    LogMsg("UpdateTripsInfoFromBackendToTripXmlFile:Err:" + ex.Message);
                }
                finally
                {
                    ReleaseContainerAfterWrite();
                }
            }
        }

        public void createNewContainerIntoContainerXml(string uaft, ArrayList lstUldNo)
        {
            lock (_lockObj)
            {
                try
                {
                    LockContainerForWriting();
                    DSContainer ds = null;
                    int updCnt = 0;
                    try
                    {
                        ds = new DSContainer();
                        //LogTraceMsg("TripPath:" + fnTripXml);
                        ds.Container.ReadXml(fnContainerXml);
                        if (ds == null)
                        {
                            throw new ApplicationException("Unable to get the Container Information.");
                        }
                    }
                    catch (ApplicationException ex) { throw ex; }
                    catch (Exception ex)
                    {
                        throw new ApplicationException("Unable to get the Container Information due to " + ex.Message);
                    }
                    foreach (string uld in lstUldNo)
                    {
                        DSContainer.ContainerRow[] tempRowArr = (DSContainer.ContainerRow[])ds.Container.Select("UAFT='" + uaft + "' AND ULDNO='" + uld + "'");
                        if (tempRowArr.Length == 0 || tempRowArr==null)
                        {
                            DSContainer.ContainerRow updRow = ds.Container.NewContainerRow();
                            updRow.UAFT = uaft;
                            updRow.ULDNO = uld;
                            updRow.SEND_IND = "N";
                            ds.Container.AddContainerRow(updRow);
                            updCnt++;

                        }
                    }
                    if (updCnt > 0)
                    {
                        ds.Container.AcceptChanges();
                        ds.WriteXml(fnContainerXml);
                        _dsContainer = ds;
                    }
                }
                catch (Exception ex)
                {
                    LogMsg("UpdateTripsInfoFromBackendToTripXmlFile:Err:" + ex.Message);
                }
                finally
                {
                    ReleaseContainerAfterWrite();
                }


            }
            
        
            }

        public void UpdateBRSInfoFromBackendToContainerXmlFile(XmlDocument xmlDoc)
        {
            lock (_lockObj)
            {
                try
                {

                    LockContainerForWriting();
                    DSContainer ds = null;
                    int updCnt = 0;
                    try
                    {
                        ds = new DSContainer();
                        //LogTraceMsg("TripPath:" + fnTripXml);
                        ds.Container.ReadXml(fnContainerXml);
                        if (ds == null)
                        {
                            throw new ApplicationException("Unable to get the Container Information.");
                        }
                    }
                    catch (ApplicationException ex) { throw ex; }
                    catch (Exception ex)
                    {
                        throw new ApplicationException("Unable to get the Container Information due to " + ex.Message);
                    }

                    DataSet dsNewCpm = new DataSet();
                    XmlNodeReader reader = new XmlNodeReader(xmlDoc);
                    dsNewCpm.ReadXml(reader);
                    
                    iTXMLPathscl itpaths = new iTXMLPathscl();
                    iTXMLcl iTXML = new iTXMLcl(itpaths.applicationBasePath);
                    int cnt = dsNewCpm.Tables["CPMDET"].Rows.Count;
                    string flightUrno = "";
                    string loaurno = "";
                    for (int i = 0; i < cnt; i++)
                    {
                        DataRow newRow = dsNewCpm.Tables["CPMDET"].Rows[i];
                        loaurno = newRow["URNO"].ToString();
                        flightUrno = newRow["FLNU"].ToString();
                        string uldNo = newRow["ULDN"].ToString();
                        string apc3 = "";
                        string cont = "";
                        string posf = "";
                        string valu = "";
                        string dssn = "";
                        try
                        {
                            apc3 = newRow["APC3"].ToString();
                            cont = newRow["CONT"].ToString().ToUpper();
                            posf = newRow["POSF"].ToString();
                            valu = newRow["VALU"].ToString();
                            dssn = newRow["DSSN"].ToString();
                        }
                        catch (Exception ex)
                        {
                            LogMsg("UpdateCPMInfoFromBackendToCPMXmlFile:Error:" + ex.Message);
                        }
                        string uldText = posf + "/" + uldNo + "/" + apc3 + "/" + valu + "/" + cont;


                        if (dssn == "BRS")
                        {
                            DSContainer.ContainerRow[] tempRowArr = (DSContainer.ContainerRow[])ds.Container.Select("UAFT='" + flightUrno + "' AND ULDNO='" + uldText + "'");
                            if (tempRowArr.Length == 0 || tempRowArr == null)
                            {
                                DSContainer.ContainerRow updRow = ds.Container.NewContainerRow();
                                updRow.URNO = loaurno;
                                updRow.UAFT = flightUrno;
                                updRow.ULDNO = iTrekUtils.iTUtils.removeBadCharacters(uldText);
                                updRow.SEND_IND = "N";
                                ds.Container.AddContainerRow(updRow);
                                updCnt++;
                                LogMsg("Adding to Containerxml" + flightUrno + ":" + uldText);

                            }
                            //if (!alContainer.Contains(iTrekUtils.iTUtils.removeBadCharacters(uldText)))
                            //{
                            //    alContainer.Add(iTrekUtils.iTUtils.removeBadCharacters(uldText));
                            //    LogMsg("Adding to Containerxml" + flightUrno + ":" + uldText);
                            //}
                        }

                    }

                    if (updCnt > 0)
                    {
                        ds.Container.AcceptChanges();
                        ds.WriteXml(fnContainerXml);
                        _dsContainer = ds;
                    }
                    //if (alContainer.Count > 0)
                    //{
                    //    createNewContainerIntoContainerXml(flightUrno, alContainer);
                    //    // iTXML.createContainerXmlForFlight(flightUrno, alContainer);
                    //}
                }
                catch (Exception ex)
                {
                    LogMsg("UpdateBRSInfoFromBackendToContainerXmlFile:Err:" + ex.Message);
                }
                finally
                {
                    ReleaseContainerAfterWrite();
                }

            }
        }
        public void UpdateBRSFieldsToContainerXmlFile(XmlDocument brsFieldXml)
        {


            string action = "";
            string urno = "";
            //string uldn = "";
            //string flightUrno = "";

            lock (_lockObj)
            {
                try
                {
                    LockContainerForWriting();

                    DSContainer ds = null;
                    int updCnt = 0;
                    try
                    {
                        ds = new DSContainer();

                        //LogTraceMsg("TripPath:" + fnTripXml);
                        ds.Container.ReadXml(fnContainerXml);
                        if (ds == null)
                        {
                            throw new ApplicationException("Unable to get the Container Information.");
                        }
                    }
                    catch (ApplicationException ex) { throw ex; }
                    catch (Exception ex)
                    {
                        throw new ApplicationException("Unable to get the Container Information due to " + ex.Message);
                    }
                    try
                    {
                        XmlElement xmlElement = brsFieldXml.DocumentElement;
                        action = xmlElement.FirstChild.InnerXml.Trim();
                        // LogMsg("action" + action);
                        if (action == "DELETE")
                        {

                            urno = xmlElement.LastChild.InnerXml.Trim();

                            foreach (DSContainer.ContainerRow row in ds.Container.Select("URNO='" + urno + "'"))
                            {
                                row.Delete();
                                updCnt++;

                            }
                            if (updCnt > 0)
                            {
                                ds.Container.AcceptChanges();
                                ds.Container.WriteXml(fnContainerXml);
                                _dsContainer = ds;
                            } 
                        }

                    }
                    catch (Exception ex1)
                    {

                        LogMsg("UpdateBRSFieldsToContainerXmlFile:Err:" + ex1.Message);

                    }


                }
                catch (Exception ex)
                {
                    LogMsg("UpdateBRSFieldsToContainerXmlFile:Err:" + ex.Message);
                }
                finally
                {
                    ReleaseContainerAfterWrite();
                }
            }


        }


        private void LockContainerForWriting()
        {
            try
            {
                UtilLock.GetInstance().LockContainer();
            }
            catch (Exception ex)
            {
                LogMsg("LockContainerForWriting:Err:" + ex.Message);
                if (ex.InnerException != null)
                {
                    LogMsg("LockContainerForWriting:err:Inner:" + ex.InnerException.Message);
                }
            }
        }

        private void ReleaseContainerAfterWrite()
        {
            try
            {
                UtilLock.GetInstance().ReleaseContainer();
            }
            catch (Exception ex)
            {
                LogMsg("ReleaseContainerAfterWrite:Err:" + ex.Message);
                if (ex.InnerException != null)
                {
                    LogMsg("ReleaseContainerAfterWrite:err:Inner:" + ex.InnerException.Message);
                }
            }
        }


        //private void TestWriteCPM()
        //{
        //    {
        //        DSCpm ds = new DSCpm();
        //        DSCpm.CPMRow row = ds.CPM.NewCPMRow();
        //        row.URNO = "CPMUrno123";
        //        row.UAFT = "UAFT1324";
        //        row.MSG = "Msg123 afd";
        //        ds.CPM.Rows.Add(row);
        //        ds.CPM.AcceptChanges();
        //        ds.CPM.WriteXml(fnCpmXml);
        //    }
        //}

    }
}