//#define TESTING_ON
#define LOG_ON
//#define USER_GRP_HARDCODE

using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.IO;

using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.CustomException;
using iTrekXML;
using UFIS.ITrekLib.Util;
using MM.UtilLib;

namespace UFIS.ITrekLib.DB
{
    public class DBUser
    {
        public DBUser()
        {
        }

        public static DBUser GetInstance()
        {
            return new DBUser();
        }

        public EntUser RetrieveUserInfo(string userId, DataSet ds)
        {
            DataTable dt = ds.Tables["USER"];
            //DataRow[] rows = dt.Select("UID='" + userId.Trim() + "'");
            DataRow[] rows = dt.Select("PENO='" + userId.Trim() + "'");
            int cnt = rows.Length;
            if (cnt < 1) throw new NoRecordException();

            EntUser user = new EntUser();
            user.UserId = userId;
            try
            {
                string auth = rows[0]["AUTH"].ToString();
                if (auth.Trim().ToUpper() == "TRUE")
                {
                    user.AuthenticateUser = true;
                }
                else
                {
                    user.AuthenticateUser = false;
                }
                user.AuthError = rows[0]["ERROR"].ToString();
                //user.Dept = rows[0]["DEPT"].ToString();
                user.Dept = rows[0]["ORG_CODE"].ToString();
                user.FirstName = rows[0]["FNAME"].ToString();
                user.LastName = rows[0]["LNAME"].ToString();
                try
                {
                    string grpId = rows[0]["GRP"].ToString().Trim();
                    if (grpId != "")
                        user.AddGroups(grpId);
                }
                catch (Exception)
                {
                }
            }
            catch (Exception ex)
            {
                LogMsg("RetrieveUserInfo:Err:" + ex.Message);
            }

            if (user.AuthenticateUser)
            {
                string rowId = rows[0]["USER_ID"].ToString();//Auto assigned by Dataset
                DataTable dtPriv = ds.Tables["PRIV"];
                if (dtPriv != null)
                {
                    foreach (DataRow row in dtPriv.Select("USER_ID='" + rowId + "'"))
                    {
                        user.AddPageAccess(row["FNC"].ToString(), row["ACC"].ToString());

                    }
                    //LogMsg("Set Priviledges");
                }
                else
                {
                    LogMsg("No Priviledges");
                }
               
                DataTable dtGrp = ds.Tables["GRP"];
                if (dtGrp != null)
                {
                    foreach (DataRow row in dtGrp.Select("USER_ID='" + rowId + "'"))
                    {
                        string grpId = row["GRP_Text"].ToString().Trim();
                        if (grpId != "")
                            user.AddGroups(grpId);
                    }
                }
            }
            
            return user;
        }

        public string GetAuthFileName(string sessionId)
        {
            iTXMLPathscl iTPaths = new iTXMLPathscl();
            string fName = "";
#if TESTING_ON
                fName = iTPaths.GetXMLPath() + @"Auth2.xml";
#else
            fName = iTPaths.GetSessionXMLPath() + sessionId + @"\AUTH2.xml";
#endif
            return fName;

        }

        public void DeleteAuthFile(string sessionId)
        {
#if !TESTING_ON
            UtilFileIO.DeleteAFile(GetAuthFileName(sessionId));
#endif
        }

        public EntUser RetrieveUserInfo(string userId, string sessionId)
        {
            //LogMsg("Retrieving User Info " + userId);
            string fName = "";

            fName = GetAuthFileName(sessionId);

            //LogTraceMsg("Auth File Name : " + fName);
            bool hasReply = false;
            DataSet ds = new DataSet();
            for (int i = 0; i < 30; i++)
            {
                try
                {
                    //FileInfo fi = new FileInfo(fName);
                    //if (fi.Exists)
                    if (UtilFileIO.IsFileExist(fName))
                    {
                        //LogMsg("Auth file exist");
                        ds.ReadXml(fName);
                        hasReply = true;
                        break;
                    }
                }
                catch (Exception)
                {
                }
                System.Threading.Thread.Sleep(500);//wait for .5 seconds to re-read the file again
            }
            //LogTraceMsg("Has Reply : " + hasReply);
            if (!hasReply) throw new ApplicationException("Authentication Timeout.");
            return RetrieveUserInfo(userId, ds);
        }

        public EntUser RetrieveUserInfo(string userId, System.IO.Stream xmlString)
        {
            DataSet ds = new DataSet();
            ds.ReadXml(xmlString);

            return RetrieveUserInfo(userId, ds);
        }

        //[Conditional("LOG_ON")]
        private void LogMsg(string msg)
        {
            //UtilLog.LogToEventLog("ITrekGlobalLogs", "dbuser", msg);
            //UtilLog.LogToGenericEventLog("DBUser", msg);
           try
           {
              MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("DBUser", msg);
           }
           catch (Exception)
           {
              UtilLog.LogToTraceFile("DBUser", msg);
           }
        }

        private void LogTraceMsg(string msg)
        {
           try
           {
              MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("DBUser", msg);
           }
           catch (Exception)
           {
              UtilLog.LogToTraceFile("DBUser", msg);
           }
        }

    }
}
