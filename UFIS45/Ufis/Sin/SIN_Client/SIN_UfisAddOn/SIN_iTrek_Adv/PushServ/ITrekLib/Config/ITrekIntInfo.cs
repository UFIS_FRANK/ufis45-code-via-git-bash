using System;
using System.Collections.Generic;
using System.Text;

using System.Data;
using System.Collections;

using UFIS.ITrekLib.Util;
using MM.UtilLib;


namespace UFIS.ITrekLib.Config
{
	/// <summary>
	/// To get the Interface Related Info according to the Environment.
	/// MQ Connection Information
	/// iDen Handheld network information
	/// </summary>
	public class ITrekIntInfo
	{
		private static Hashtable _htKeyValue = new Hashtable();
		private static DateTime _dtLastUpdDateTime = UtilTime.DEFAULT_DATE_TIME;
		private static ITrekIntInfo _this = null;
		private static object _lockThis = new object();

		private static DataSet _dsIntInfo = null;

		public const string MQ_MANAGER_NAME = "MQ_MANAGER";
		public const string MQ_CHANNEL_NAME = "MQ_CHANNEL";
		public const string MQ_CHANNEL_TRANSPORT_TYPE = "MQ_CHANNEL_TRANSPORT";
		public const string MQ_CHANNEL_SERVER = "MQ_CHANNEL_SERVER";
		public const string MQ_CHANNEL_PORTNO = "MQ_CHANNEL_PORT_NO";

		public const string HH_PAPURL = "PAPURL";

		public static ITrekIntInfo GetInstance()
		{
			if (_this == null)
			{
				lock (_lockThis)
				{
					if (_this == null)
					{
						_this = new ITrekIntInfo();
					}
				}
			}
			return _this;
		}

		private ITrekIntInfo()
		{
			_dsIntInfo = new DataSet();
		}

		private void LoadInfo()
		{
			DataSet ds = new DataSet();
			try
			{
				ds.ReadXml(GetInfoFilePath());
			}
			catch (Exception ex)
			{
				LogMsg("LoadInfo:Err:" + ex.Message);
			}
		}

		private string GetInfoFilePath()
		{
			return ITrekConfig.GetIntInfoFilePath();
		}

		private static void LogMsg(string msg)
		{
			try
			{
				MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("ITrekIntInfo", msg);
			}
			catch (Exception)
			{
				UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("ITrekIntInfo", msg);
			}
		}

		private static void LogTraceMsg(string msg)
		{
			LogMsg(msg);
		}
	}
}
