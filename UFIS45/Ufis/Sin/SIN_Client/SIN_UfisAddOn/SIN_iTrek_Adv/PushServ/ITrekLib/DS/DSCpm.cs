﻿using System.Data;
using MM.UtilLib;

namespace UFIS.ITrekLib.DS {
    public partial class DSCpm
    {
        public new void ReadXml(string fileName)
        {
            try
            {
                //LogTraceMsg("MyReadXml for CPM");
                this.EnforceConstraints = false;
                UtilDataSet.LoadDataFromXmlFile(this, fileName);
                try
                {
                    this.EnforceConstraints = true;
                }
                catch (System.Exception)
                {
                    if (this != null)
                    {
                        if (this.CPMDET.HasErrors)
                        {//if any error due to the constraints, remove from the dataset
                            DataRow[] rowArr = this.CPMDET.GetErrors();
                            foreach (DataRow row in rowArr)
                            {
                                try
                                {
                                    LogTraceMsg("DSCpm Delete Error Rec Id[" + row["URNO"] + "]");
                                }
                                catch { }
                                row.Delete();
                            }
                            //for (int i1 = 0; i1 < rowArr.Length; i1++)
                            //{
                            //    string st1 = rowArr[i1]["URNO"].ToString();
                            //    LogTraceMsg("DSCpm Delete Error Rec Id[" + st1 + "]");
                            //    rowArr[i1].Delete();
                            //}
                            this.CPMDET.AcceptChanges();
                        }
                    }
                }
            }
            catch (System.Exception)
            {
            }
            finally
            {
                this.EnforceConstraints = true;
            }
        }

        private static void LogTraceMsg(string msg)
        {
            Util.UtilLog.LogToTraceFile("CtrlCPM", msg);
        }

        partial class CPMDETDataTable
        {
        }
    }
}
