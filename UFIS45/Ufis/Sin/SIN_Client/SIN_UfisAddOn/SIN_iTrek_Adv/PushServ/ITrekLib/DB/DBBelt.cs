using System;
using System.Collections.Generic;
using System.Text;

using System.Data;
using System.IO;
using System.Collections;

using UFIS.ITrekLib.DS;
using iTrekXML;
using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.Util;
using MM.UtilLib;

namespace UFIS.ITrekLib.DB
{
    public class DBBelt
    {
        private static Object _lockObj = new Object();
        private static readonly Object _lockObjInstance = new Object();
        //to use in synchronisation of single access
        private static DBBelt _DBBelt = null;
        private static readonly DateTime DEFAULT_DT = new System.DateTime(1, 1, 1);
        private static DateTime _lastWriteDT = DEFAULT_DT;
        private static DateTime _lastRefreshDT = DEFAULT_DT;

        private static DSBelt _dsOBelt = null;
        private static string _fnXml = null;//file name of XML in full path.
        private static readonly object _lockObjUpd = new object();

        ///// <summary>
        ///// Singleton Pattern.
        ///// Not allowed to create the object from other class.
        ///// To get the object, user "GetInstance()" method.
        ///// </summary>
        private DBBelt() { }

        public void Init()
        {//Initialize all the information, to get the latest data when fetching them again.
            _dsOBelt = null;
            _fnXml = null;
            _lastWriteDT = DEFAULT_DT;
            _lastRefreshDT = DEFAULT_DT;
        }

        public void RefreshData()
        {
            _lastWriteDT = DEFAULT_DT;
            _lastRefreshDT = DEFAULT_DT;
        }

        //  /// <summary>
        //  /// Get the Instance of DBBelt. There will be only one reference for all the clients
        //  /// </summary>
        //  /// <returns></returns>
        public static DBBelt GetInstance()
        {
            if (_DBBelt == null)
            {
                lock (_lockObjInstance)
                {
                    try
                    {
                        if (_DBBelt == null)
                            _DBBelt = new DBBelt();
                    }
                    catch (Exception ex)
                    {
                        LogMsg("GetInstance:Err:" + ex.Message);
                    }
                }
            }
            return _DBBelt;
        }

        private string FnXml
        {
            get
            {
                if ((_fnXml == null) || (_fnXml == ""))
                {
                    iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
                    _fnXml = path.GetBeltFilePath();
                }

                return _fnXml;
            }
        }

        /// <summary>
        /// Getting the original dataset. 
        /// Do not make any changes to the original dataset. 
        /// It will affect other methods. 
        /// Note : Return new Dataset from other methods to caller.
        /// </summary>
        private DSBelt DsOBelt
        {
            get
            {
                int timeElapse = Math.Abs(UtilTime.DiffInSec(DateTime.Now, _lastRefreshDT));
                if ((_dsOBelt == null) || (timeElapse > 6))
                {
                    LoadDSBelt();                    
                    _lastRefreshDT = DateTime.Now;
                }
                return _dsOBelt;
            }
        }


        private void LoadDSBelt()
        {
            FileInfo fiXml = new FileInfo(FnXml);
            DateTime curDT = fiXml.LastWriteTime;

            if ((_dsOBelt == null) || (curDT.CompareTo(_lastWriteDT) != 0))
            {
                lock (_lockObj)
                {
                    try
                    {
                        curDT = fiXml.LastWriteTime;//check the time again, in case of lock
                        if ((_dsOBelt == null) || (curDT.CompareTo(_lastWriteDT) != 0))
                        {
                            if (_dsOBelt == null) _dsOBelt = new DSBelt();
                            try
                            {
                                //DSBelt tempDs = new DSBelt();
                                
                                //tempDs.BLT.ReadXml(FnXml);
                                //_dsOBelt = tempDs;
                                _dsOBelt = ReadFromFile();
                                _lastWriteDT = curDT;
                            }
                            catch (Exception ex)
                            {
                                LogMsg("LoadDSBelt : Error : " + ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogMsg("LoadDSBelt:Err:" + ex.Message);
                    }
                }
            }
        }

        private static void LogMsg(string msg)
        {
            //UtilLog.LogToGenericEventLog("DBBelt", msg);
           try
           {
              MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("DBBelt", msg);
           }
           catch (Exception)
           {
              UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DBBelt", msg);
           }
        }

        public bool IsExist(string terminalId, string section)
        {
            bool exist = false;

            DSBelt.BLTRow[] rows = (DSBelt.BLTRow[])
                DsOBelt.BLT.Select(GetSelectStatement(terminalId, section));

            if ((rows != null) && (rows.Length > 0)) exist = true;
            return exist;
        }

        private string GetSelectStatement(string terminalId, string section)
        {
            return "TMNL='" + terminalId + "' AND SECT='" + section + "'";
        }

        public void Update(string terminalId, string section, string belts)
        {
            lock (_lockObjUpd)
            {
                DSBelt ds = ReadFromFile();

                DSBelt.BLTRow[] rows = (DSBelt.BLTRow[])ds.BLT.Select(GetSelectStatement(terminalId, section));
                if ((rows == null) || (rows.Length < 1))
                {
                    Create(terminalId, section, belts);
                }
                else if (rows.Length == 1)
                {
                    rows[0].BLTS = belts;
                    ds.BLT.AcceptChanges();
                    WriteToFile(ds);
                }
                else if (rows.Length > 1) throw new ApplicationException("Duplicate Entry");
            }
        }

        public void Update(string terminalId, string section, int frBelt, int toBelt)
        {
            lock (_lockObjUpd)
            {
                DSBelt ds = ReadFromFile();

                DSBelt.BLTRow[] rows = (DSBelt.BLTRow[])ds.BLT.Select(GetSelectStatement(terminalId, section));
                if ((rows == null) || (rows.Length < 1))
                {
                    Create(terminalId, section, frBelt, toBelt);
                }
                else if (rows.Length == 1)
                {
                    rows[0].FR = frBelt;
                    rows[0].TO = toBelt;
                    rows[0].BLTS = frBelt + "-" + toBelt;
                    ds.BLT.AcceptChanges();
                    WriteToFile(ds);
                }
                else if (rows.Length > 1) throw new ApplicationException("Duplicate Entry");
            }
        }

        private DSBelt ReadFromFile()
        {
            DSBelt ds = new DSBelt();
            ds.BLT.ReadXml(FnXml);
            return ds;
        }

        private void WriteToFile(DSBelt ds)
        {
            ds.BLT.AcceptChanges();
            ds.BLT.WriteXml(FnXml, XmlWriteMode.IgnoreSchema, false);
            _dsOBelt = ds;
            _lastWriteDT = DEFAULT_DT;
        }

        public void Create(string terminalId, string section, string belts)
        {
            lock (_lockObjUpd)
            {
                if (!File.Exists(FnXml))
                {
                    DSBelt ds = new DSBelt();
                    WriteToFile(ds);
                }
                if (IsExist(terminalId, section))
                {
                    Update(terminalId, section, belts);
                }
                else
                {
                    DSBelt ds = ReadFromFile();
                    DSBelt.BLTRow row = (DSBelt.BLTRow)ds.BLT.NewRow();
                    row.TMNL = terminalId;
                    row.SECT = section;
                    row.BLTS = belts;
                    ds.BLT.AddBLTRow(row);
                    ds.BLT.AcceptChanges();
                    WriteToFile(ds);
                }
            }
        }

        public void Create(string terminalId, string section, int frBelt, int toBelt)
        {
            lock (_lockObjUpd)
            {
                if (!File.Exists(FnXml))
                {
                    DSBelt ds = new DSBelt();
                    WriteToFile(ds);
                }
                if (IsExist(terminalId, section))
                {
                    Update(terminalId, section, frBelt, toBelt);
                }
                else
                {
                    DSBelt ds = ReadFromFile();
                    DSBelt.BLTRow row = (DSBelt.BLTRow)ds.BLT.NewRow();
                    row.TMNL = terminalId;
                    row.SECT = section;
                    row.FR = frBelt;
                    row.TO = toBelt;
                    row.BLTS = row.FR + "-" + row.TO;
                    ds.BLT.AddBLTRow(row);
                    ds.BLT.AcceptChanges();
                    WriteToFile(ds);
                }
            }
        }

        public void Delete(string terminalId, string sectionId)
        {
            lock (_lockObjUpd)
            {
                try
                {
                    DSBelt ds = ReadFromFile();

                    DSBelt.BLTRow[] rows = (DSBelt.BLTRow[])ds.BLT.Select(GetSelectStatement(terminalId, sectionId));
                    int cnt = rows.Length;

                    for (int i = 0; i < cnt; i++)
                    {
                        rows[i].Delete();
                    }
                    ds.BLT.AcceptChanges();

                    WriteToFile(ds);
                }
                catch (Exception ex)
                {
                    LogMsg("Delete:Err: " + ex.Message);
                    throw new ApplicationException("Unable to delete due to : " + ex.Message);
                }
            }
        }

        public DSBelt RetrieveAll()
        {
            DSBelt ds = null;
            ds = (DSBelt)DsOBelt.Copy();

            return ds;
        }

        public DSBelt RetrieveBelts(string terminalId)
        {
            DSBelt ds = new DSBelt();
            DSBelt dsTemp = this.DsOBelt;
            foreach (DSBelt.BLTRow row in dsTemp.BLT.Select("TMNL='" + terminalId + "'"))
            {
                ds.BLT.LoadDataRow(row.ItemArray, true);
            }
            return ds;
        }

        public DSBelt RetrieveBelts(string terminalId, string sectionId)
        {
            DSBelt ds = null;
            if (sectionId == null) ds = RetrieveBelts(terminalId);
            else
            {
                terminalId = terminalId.Trim();
                if (terminalId == "") ds = RetrieveBelts(terminalId);
                else
                {
                    foreach (DSBelt.BLTRow row in ds.BLT.Select(GetSelectStatement(terminalId, sectionId)))
                    {
                        ds.BLT.LoadDataRow(row.ItemArray, true);
                    }
                }
            }

            return ds;
        }
    }
}

