////#define TESTING_ON

using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections;
using System.Diagnostics;


namespace UFIS.ITrekLib.ENT
{

   /// <summary>
   /// Summary description for User
   /// </summary>
   public class EntUser
   {
      private string _userId = "";
      private string _pwd = "";
      private string _dept = "";
      private string _role = "";
      private Hashtable htPageAccess = null;
      private ArrayList _groupList = null;
      private bool _auth = false;
      private string _authError = "";
      private string _FName = "";
      private string _LName = "";

      public const string ROLE_AO = "AO";
      public const string ROLE_BO = "BO";
      public const string ROLE_BAG_DM = "BDM";
      public const string ROLE_APR_DM = "ADM";

      public bool IsBO()
      {
         if (Role == ROLE_BO) return true;
         else return false;
      }

      public string FirstName
      {
         get { return _FName; }
         set { if (value == null) { _FName = ""; } else { _FName = value.Trim(); } }
      }

      public string LastName
      {
         get { return _LName; }
         set { if (value == null) { _LName = ""; } else { _LName = value.Trim(); } }
      }

      public bool AuthenticateUser
      {
         get { return _auth; }
         set { _auth = value; }
      }

      public EntUser()
      {
         UserId = "";
         Password = "";
         Dept = "";
         Role = "";
         htPageAccess = new Hashtable();
         _groupList = new ArrayList();
      }

      public EntUser(string userId, string pwd)
         : this()
      {
         UserId = userId;
         Password = pwd;
      }

      public string AuthError
      {
         get { return _authError; }
         set { _authError = value; }
      }

      public void AddGroups(string groupName)
      {
         if (groupName == null) return;
         groupName = groupName.Trim().ToUpper();
         if (groupName == "") return;
         if (!IsExistGroup(groupName))
         {
            _groupList.Add(groupName);
         }
      }

      public bool IsExistGroup(string groupName)
      {
         int cnt = _groupList.Count;
         bool exist = false;

         for (int i = 0; i < cnt; i++)
         {
            if (_groupList[i].ToString() == groupName)
            {
               exist = true;
               break;
            }
         }
         return exist;
      }


      public string GetComaSeperatedGroupNames()
      {
         int cnt = _groupList.Count;
         string groupNames = "";
         string coma = "";

         for (int i = 0; i < cnt; i++)
         {
            groupNames += coma + _groupList[i].ToString();
            coma = ",";
         }
         return groupNames;
      }

      public ArrayList GetGroupList()
      {
         return _groupList;
      }


      public EntUser(string userId, string pwd, string dept, string role)
         : this(userId, pwd)
      {
         Dept = dept;
         Role = role;
      }

      public bool IsValidUser()
      {
         if ((_userId == null) || (_userId == "")) return false;
         else return _auth;
      }

      public bool HasPageAccess(string pageName)
      {
         bool accessPage = false;
         if (htPageAccess.ContainsKey(pageName))
         {
            string st = htPageAccess[pageName].ToString();
            switch (st)
            {
               case "1":
               case "0":
                  accessPage = true;
                  break;
               default:
                  accessPage = false;
                  break;
            }
            //accessPage = true;
         }
         return accessPage;
      }

      public EnUserSecurityAccess GetPageAccess(string pageName)
      {
         EnUserSecurityAccess access = EnUserSecurityAccess.NoAccess;
         if (htPageAccess.ContainsKey(pageName))
         {
            string st = htPageAccess[pageName].ToString();
            switch (st)
            {
               case "1":
                  access = EnUserSecurityAccess.Edit;
                  break;
               case "0":
                  access = EnUserSecurityAccess.View;
                  break;
               default:
                  access = EnUserSecurityAccess.NoAccess;
                  break;
            }
         }
         return access;
      }

      public void AddPageAccess(string pageName, string right)
      {
         if (htPageAccess.ContainsKey(pageName))
         {
            string st = htPageAccess[pageName].ToString();
            if (st == "1")
            {//no need to overwrite
            }
            else if (st == "0")
            {
               if (right == "1")
               {//more access right, need to overwrite
                  htPageAccess[pageName] = right;
               }
            }
            else
            {//overwrite
               htPageAccess[pageName] = right;
            }
         }
         else
         {
            htPageAccess.Add(pageName, right);
         }
      }

      #region Properties
      public string UserId
      {
         get { return _userId; }
         set { _userId = value.Trim(); }
      }

      public string Password
      {
         get { return _pwd; }
         set { _pwd = value.Trim(); }
      }

      public string Role
      {
         get { return _role; }
         set { _role = value.Trim().ToUpper(); }
      }

      //public bool IsBaggageUser()
      //{
      //   bool isBgUser = false;
      //   if ((!string.IsNullOrEmpty(_dept)) && (
      //      _dept.ToUpper().Substring(0, 1) == "B"))
      //      isBgUser = true;
      //   return isBgUser;
      //}

      //public bool IsApronUser()
      //{
      //   return Dept == "A";
      //}

      /// <summary>
      /// Department Type
      /// </summary>
      public EnUserDept UserDept
      {
         get
         {
            EnUserDept dept = EnUserDept.Undefined;
            string stDept = _dept;
            if (!string.IsNullOrEmpty(stDept))
            {
               if (stDept.Length>1) stDept = stDept.Substring(0,1);
               stDept = stDept.ToUpper();
               switch (stDept)
               {
                  case "A":
                     dept = EnUserDept.Apron;
                     break;
                  case "B":
                     dept = EnUserDept.Baggage;
                     break;
               }
            }
            return dept;
         }
      }

      /// <summary>
      /// Name of Department
      /// </summary>
      public string Dept
      {
         get
         {
            string deptName = "";
            switch (_dept.ToUpper())
            {
               case "A":
                  deptName = "Apron";
                  break;
               case "B":
                  deptName = "Baggage";
                  break;
               case "S":
                  deptName = "Socc";
                  break;
               case "L":
                  deptName = "Lcc";
                  break;
               default:
                  deptName = _dept;
                  break;
            }
            return deptName;
         }
         set
         {
            if (value == null) value = "";
            else
            {
               value = value.ToUpper().Trim();
               if (value.Length > 2) value = value.Substring(0, 2);
               if ((value == "A1") || (value == "A2")) value = "A";//Apron
               else if ((value == "B1") || (value == "B2")) value = "B";//Baggage
               else if (value == "S1") value = "S"; //Socc
               else if (value == "LC") value = "L"; //Lcc
               else if (value.Length > 0) value = value.Substring(0, 1);
            }
            _dept = value;
         }
      }
      #endregion

   }
}