using System;
using System.Collections.Generic;
using System.Text;

namespace UFIS.ITrekLib.CustomException
{
    public class NoRecordException: ApplicationException
    {
        private const string DEFAULT_MSG = "No record was selected.";

        public NoRecordException()
            : base(DEFAULT_MSG)
        {
        }

        public NoRecordException(string msg)
            : base(msg)
        {
        }
    }
}
