#define TRACE_ON

using System;
using System.Collections.Generic;
using System.Text;

using System.IO;
using System.Diagnostics;
using System.Data;

using System.Collections;
using System.Xml;

using UFIS.ITrekLib.ENT;
using MM.UtilLib;

using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.DSDB;
using UFIS.ITrekLib.Util;
using UFIS.ITrekLib.Config;
using UFIS.ITrekLib.CommMsg;
using UFIS.ITrekLib.Misc;
using UFIS.ITrekLib.CustomException;
using System.Threading;


namespace UFIS.ITrekLib.DB
{
	public class DBDFlight : IDisposable
	{
		private static Object _lockObj = new Object();//to use in synchronisation of single access
		private static Object _lockObjArrivalTime = new Object();//to use in synchronisation of single access
		private static Object _lockObjDepartureTime = new Object();//to use in synchronisation of single access
		private static DBDFlight _DBDFlight = null;


		private static readonly DateTime DEFAULT_DT = new System.DateTime(1, 1, 1);
		private static DateTime _lastFLIGHTWriteDT = DEFAULT_DT;
		private static DateTime _lastFULLRefreshDT = DEFAULT_DT;
		private static DateTime _lastRefreshDT = DEFAULT_DT;
		private static DateTime _lastRefreshDTArr = DEFAULT_DT;
		private static DateTime _lastRefreshDTDep = DEFAULT_DT;
		private static DateTime _lastArrivalTimeWriteDT = DEFAULT_DT;
		private static DateTime _lastDepartureTimeWriteDT = DEFAULT_DT;

		private static DSFlight _dsFlight = null;
		private static DSArrivalFlightTimings _dsArrivalFlightTimings = null;
		private static DSDepartureFlightTimings _dsDepartureFlightTimings = null;

		private const int FLIGHT_REFRESH_SEC = 1;
		private const int ARR_FLT_REFRESH_SEC = 1;
		private const int DEP_FLT_REFRESH_SEC = 1;

		public enum EnumFlightType { Arrival, Departure };

		/// <summary>
		/// Singleton Pattern.
		/// Not allowed to create the object from other class.
		/// To get the object, user "GetInstance()" method.
		/// </summary>
		private DBDFlight()
		{
			if (_dsFlight == null)
			{
				Init();
				_dsFlight = new DSFlight();
			}
			//DE.FlightSummUpdated += new FlightSummUpdEventHandler(DE_FlightSummUpdated);
			LogTraceMsg("Subscribe Flight Summ Update");
			DE.FlightSummUpdated += DE_FlightSummUpdated;
		}

		public void Dispose()
		{
			LogTraceMsg("UnSubscribe Flight Summ Update");
			DE.FlightSummUpdated -= DE_FlightSummUpdated;
		}

		void DE_FlightSummUpdated(object sender, DSFlight dsFlight, DateTime dtLastUpd)
		{
			LogTraceMsg("DE_FlightSummUpdated");
			lock (_lockUpdDsFlight)
			{
				_dsFlight = dsFlight;
				_lastFLIGHTWriteDT = dtLastUpd;
				_lastRefreshDT = Misc.ITrekTime.GetCurrentDateTime();
			}
		}

		public void Init()
		{//Initialize all the information, to get the latest data when fetching them again.
			_lastFLIGHTWriteDT = DEFAULT_DT;
			_lastArrivalTimeWriteDT = DEFAULT_DT;
			_lastDepartureTimeWriteDT = DEFAULT_DT;
			_lastFULLRefreshDT = DEFAULT_DT;
			_lastRefreshDT = DEFAULT_DT;
			_lastRefreshDTArr = DEFAULT_DT;
			_lastRefreshDTDep = DEFAULT_DT;
			_refreshing = false;
		}

		public void InitToRefreshArrTiming()
		{
			_lastRefreshDTArr = DEFAULT_DT;
            _lastArrivalTimeWriteDT = Misc.ITrekTime.GetCurrentDateTime().AddDays(-1);
		}

		public void InitToRefreshDepTiming()
		{
            _lastRefreshDTDep = DEFAULT_DT;
			_lastDepartureTimeWriteDT = Misc.ITrekTime.GetCurrentDateTime().AddDays(-1);
		}

		private static bool _refreshing = false;
		private static object _lockRefresh = new object();

		/// <summary>
		/// Refresh the Data without waiting for refresh to finish.
		/// </summary>
		public void Refresh()
		{
			if (!_refreshing)
			{
				lock (_lockRefresh)
				{
					_refreshing = true;
					try
					{
						int timeElapse = Math.Abs(UtilTime.DiffInSec(ITrekTime.GetCurrentDateTime(), _lastRefreshDT));
						if ((_dsFlight == null) || (timeElapse > FLIGHT_REFRESH_SEC))
						{
							Thread trd = new Thread(new ThreadStart(this.ThRefresh));
							trd.IsBackground = true;
							trd.Start();
						}
					}
					catch (Exception)
					{
						throw;
					}
					finally
					{
						_refreshing = false;
					}
				}
			}
		}

		/// <summary>
		/// Refresh the data for a given flight Id and return to caller after refreshing finished.
		/// </summary>
		/// <param name="flightId"></param>
		public void Refresh(IDbConnection conn, string flightId)
		{
			DateTime dt1 = DateTime.Now;
			DateTime dt2, dt3, dt4, dt5;
			dt2 = dt3 = dt4 = dt5 = DateTime.Now;
			bool isNewConn = false;
			IDbCommand cmd = null;
			UtilDb udb = UtilDb.GetInstance();
			try
			{
				if ((conn == null) || (conn.State == ConnectionState.Closed) || (conn.State == ConnectionState.Broken))
				{
					cmd = udb.GetCommand(false);
					isNewConn = true;
				}
				else
				{
					cmd = udb.GetCommand(conn, null);
				}
				dt2 = DateTime.Now;

				DSFlight ds = new DSFlight();
				if (LoadData(cmd, ref ds, flightId))
				{					
					dt3 = DateTime.Now;
					lock (_lockUpdDsFlight)
					{
						DSFlight dsCur = dsFlight;
						DSFlight dsNew = UpdateDsFlight(dsCur, ds);
						dt4 = DateTime.Now;
                        LogTraceMsg("dsCur.FLIGHT.Count:" + dsCur.FLIGHT.Count + ", dsNew.FLIGHT.Count:" + dsNew.FLIGHT.Count);
                        
						if (dsCur.FLIGHT.Count <= dsNew.FLIGHT.Count)
						{
							_dsFlight = dsNew;
							try
							{
								string adid = ((DSFlight.FLIGHTRow)ds.FLIGHT.Rows[0]).A_D;
								if (adid == "A")
								{
                                    LogTraceMsg("InitToRefreshArrTiming");
									InitToRefreshArrTiming();
								}
								else if (adid == "D")
								{
                                    LogTraceMsg("InitToRefreshDepTiming");
									InitToRefreshDepTiming();
								}
								else
								{
                                    LogTraceMsg("InitToRefreshArrTiming  InitToRefreshDepTiming");
									InitToRefreshArrTiming();
									InitToRefreshDepTiming();
								}
							}
							catch (Exception ex)
							{
								LogMsg("Refresh Arr/Dep Timing:Err:" + ex.Message);
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				LogMsg("Refresh:Err:" + flightId + "," + ex.Message);
			}
			finally
			{
				if (isNewConn)
				{
					udb.CloseCommand(cmd, false);
				}
				else
				{
					udb.CloseCommand(cmd, false, false);
				}
			}
			dt5 = DateTime.Now;
			LogTraceMsg(string.Format(
				"Refresh:{0},Conn:{1},Load:{2},Upd:{3},Close:{4},Tot:{5}",
				flightId,
				UtilTime.ElapsedTime(dt1, dt2),
				UtilTime.ElapsedTime(dt2, dt3),
				UtilTime.ElapsedTime(dt3, dt4),
			UtilTime.ElapsedTime(dt4, dt5),
			UtilTime.ElapsedTime(dt1, dt5)));

		}
		
		private void ThRefresh()
		{
			//_lastRefreshDT = DEFAULT_DT;
			//DSFlight ds = dsFlight;
			DateTime now = ITrekTime.GetCurrentDateTime();

			int timeElapse = Math.Abs(UtilTime.DiffInSec(now, _lastRefreshDT));
			if ((_dsFlight == null) || (timeElapse > FLIGHT_REFRESH_SEC))
			{
				//LogTraceMsg("TimeElapse:" + timeElapse);
				bool loadingSuccess = false;
				LoadDSFlight(out loadingSuccess, false);
				if (loadingSuccess)
				{
					_lastRefreshDT = now;
					LoadDSDepartureFlightTimings();
					LoadDSArrivalFlightTimings();
					LogTraceMsg("Refresh" + _lastRefreshDT.ToLongTimeString());
				}
			}

			if (_dsFlight == null)
			{
				Init();
				_dsFlight = new DSFlight();
			}
		}

		/// <summary>
		/// Get the Instance of  DBDFlight. There will be only one reference for all the clients
		/// </summary>
		/// <returns></returns>
		public static DBDFlight GetInstance()
		{
			if (_DBDFlight == null)
			{
				_DBDFlight = new DBDFlight();
			}
			return _DBDFlight;
		}

		public string GetFlightInfoSumm(string flightId)
		{
			string info = "";
			try
			{
				DSFlight ds = RetrieveFlightInfoForAFlight(flightId);
				DSFlight.FLIGHTRow row = (DSFlight.FLIGHTRow)ds.FLIGHT.Rows[0];
				//info = row.FL_SUMM;
				string flDate = string.Format("{0:dd/MMM/yyyy}", UtilTime.ConvUfisTimeStringToDateTime(row.ST)).Replace("-", "/");
				info = string.Format("{0}-{1}-{2}",
				   row.FLNO, flDate, row.A_D);
			}
			catch (Exception)
			{
			}
			return info;
		}

		/// <summary>
		/// Retrieve the flights 
		/// based on the filter criteria in 'filter'
		/// </summary>
		/// <param name="filter">filter criteria object</param>
		/// <param name="userId">User Id to Filter (if blank or null, no need to filter with user Id)</param>
		/// <param name="maxNoOfFlightsToFetch">Maximum No. of Flights To Fetch (if 0 or -1 then Unlimited')</param>
		/// <param name="sortByTime">Sort by Ascending or Descending or None</param>
		/// <returns>DSFlight</returns>
		public DSFlight RetrieveFlights(EntWebFlightFilter filter,
		   string userId, int maxNoOfFlightsToFetch, EnSort sortByTime)
		{
			DSFlight ds = new DSFlight();
			DSFlight tempDs = dsFlight;
			//Assign to local variable
			// to avoid calling "property get method" in foreach loop
			//LogTraceMsg("RetrieveFlights:" + filter.UseFrToFixedDateTime + ", " + 
			//    filter.FrFixedDateTime.ToLongTimeString() + ", " + filter.FromDateTime.ToLongTimeString());
			string stFr = UtilTime.ConvDateTimeToUfisFullDTString(filter.FromDateTime);
			string stTo = UtilTime.ConvDateTimeToUfisFullDTString(filter.ToDateTime);
			//LogMsg("Retrieving Flight for terminal " + terminal + ", " + stFr + ", " + stTo);
			string selString = "";
			string conj = "";

			if (!string.IsNullOrEmpty(filter.FlightType))
			{
				selString += conj + " (A_D='" + filter.FlightType + "')";
				conj = " AND ";
			}

			if (!string.IsNullOrEmpty(userId))
			{
				selString += conj + " (AOID='" + userId + "' OR BOID='" + userId + "')";
				conj = " AND ";
			}

			if (!string.IsNullOrEmpty(filter.Regn))
			{
				selString += conj + UtilDataSet.PrepareWhereClause(
					filter.Regn, EntWebFlightFilter.REGN_SEPERATOR,
					"REGN", "'");
				conj = " AND ";
			}
			if (!((filter.TerminalIds == null) || (filter.TerminalIds == "")))
			{
				selString += conj + UtilDataSet.PrepareWhereClause(
					filter.TerminalIds, EntWebFlightFilter.TERMINAL_SEPERATOR,
					"TERMINAL", "'");
				conj = " AND ";
			}

			selString += string.Format("{0} ({1}>='{2}' AND {1}<='{3}')",
			   conj, filter.StDateFilterType, stFr, stTo);
			//selString += conj +" FLIGHT_DT>='" + stFr + "' ";
			//selString += " AND FLIGHT_DT<='" + stTo + "'";

			string stSortBy = "";
			if (sortByTime == EnSort.Asc) stSortBy = filter.StDateFilterType + " ASC";
			else if (sortByTime == EnSort.Desc) stSortBy = filter.StDateFilterType + " DESC";

			//LogTraceMsg("RetrieveFlights selString==>" + selString);
			int cnt = 0;
			bool chkCnt = false;
			if (maxNoOfFlightsToFetch > 0) chkCnt = true;

			foreach (DSFlight.FLIGHTRow row in tempDs.FLIGHT.Select(selString, stSortBy))
			{
				string airlineCode = "";
				try
				{
					if (!string.IsNullOrEmpty(row.FLNO))
					{
						if (row.FLNO.Length > 2)
							airlineCode = row.FLNO.Substring(0, 3).Trim();
						else
							airlineCode = row.FLNO.Substring(0, 2).Trim();
					}
				}
				catch (Exception)
				{
				}

				if (filter.IsMeetCriteria(airlineCode, row.REGN))
				{
					//LogTraceMsg("RetrieveFlights:IN:" + row.URNO + "," + row.FLNO + "," + row.ST + "," + airlineCode + "," + row.REGN + "," + row.ST_TIME);
					ds.FLIGHT.LoadDataRow(row.ItemArray, true);
					if (chkCnt)
					{//Check the Count of row
						cnt++;
						if (cnt >= maxNoOfFlightsToFetch) break;
					}
				}
				else
				{
					//LogTraceMsg("RetrieveFlights:OUT:" + row.FLNO + "," + row.ST + "," + airlineCode + "," + row.REGN);
				}
			}

			return ds;
		}

		public DSFlight RetrieveFlightInfoForAFlight(string flightUrNo)
		{
			//LogTraceMsg(" DBDFlight,RetrieveFlightInfoForAFlight," + flightUrNo);
			DSFlight ds = new DSFlight();
			DSFlight tempDs = dsFlight;
			//Assign to local variable
			// to avoid calling "property get method" in foreach loop

			foreach (DSFlight.FLIGHTRow row in tempDs.FLIGHT.Select("URNO='" + flightUrNo + "'"))
			{
				ds.FLIGHT.LoadDataRow(row.ItemArray, true);
				break;
			}

			return ds;
		}

		public string RetrieveRelatedTransitFlightUrnoForFlight(string flightUrNo)
		{
			string stTransitFltUrno = "";

			DSFlight tempDs = dsFlight;
			//Assign to local variable
			// to avoid calling "property get method" in foreach loop
			foreach (DSFlight.FLIGHTRow row in tempDs.FLIGHT.Select("URNO='" + flightUrNo + "'"))
			{
				stTransitFltUrno = row.TURN;
				break;
			}

			if (stTransitFltUrno == null) stTransitFltUrno = "";
			stTransitFltUrno = stTransitFltUrno.Trim();
			return stTransitFltUrno;
		}

		public DSFlight RetrieveFlightInfoForAFlightWithDoorClosed(string flightUrNo)
		{
			DSFlight ds = new DSFlight();
			DSFlight tempDs = dsFlight;
			//Assign to local variable
			// to avoid calling "property get method" in foreach loop

			foreach (DSFlight.FLIGHTRow row in tempDs.FLIGHT.Select("URNO='" + flightUrNo + "' AND LST_DOOR<>''"))
			{
				ds.FLIGHT.LoadDataRow(row.ItemArray, true);
			}

			return ds;
		}

		public enum EnSort { Desc = -1, None = 0, Asc = 1 }

		public DSFlight RetrieveAssignedFlights(string terminal, string userId, int maxNoOfFlightsToFetch, EnSort sortByTime,
			DateTime fr, DateTime to)
		{
			DSFlight ds = new DSFlight();
			DSFlight tempDs = dsFlight;
			int cnt = 0;
			string stSortBy = "";
			string terminalSel = "";
			if (!((terminal == null) || (terminal == ""))) terminalSel = " AND TERMINAL='" + terminal + "'";
			if (sortByTime == EnSort.Asc) stSortBy = "FLIGHT_DT ASC";
			else if (sortByTime == EnSort.Desc) stSortBy = "FLIGHT_DT DESC";

			string stFr = UtilTime.ConvDateTimeToUfisFullDTString(fr);
			string stTo = UtilTime.ConvDateTimeToUfisFullDTString(to);
			string timeSelString = " AND FLIGHT_DT>='" + stFr + "' ";
			timeSelString += " AND FLIGHT_DT<='" + stTo + "'";
			string selString = "(AOID='" + userId + "' OR BOID='" + userId + "') " + terminalSel + timeSelString;
			//LogTraceMsg("RetrieveAssignedFlights selstring==>" + selString + ", " + stSortBy);
			foreach (DSFlight.FLIGHTRow row in tempDs.FLIGHT.Select(selString, stSortBy))
			{
				ds.FLIGHT.LoadDataRow(row.ItemArray, true);
				cnt++;
				if (cnt >= maxNoOfFlightsToFetch) break;
			}
			return ds;
		}

		public DSFlight RetrieveAssignedFlights(EntWebFlightFilter filter, string adId, string userId, int maxNoOfFlightsToFetch, EnSort sortByTime)
		{
			if (string.IsNullOrEmpty(userId)) throw new ApplicationException("No User Information.");

			DSFlight ds = new DSFlight();
			DSFlight tempDs = dsFlight;
			int cnt = 0;

			//-----------
			string stFr = UtilTime.ConvDateTimeToUfisFullDTString(filter.FromDateTime);
			string stTo = UtilTime.ConvDateTimeToUfisFullDTString(filter.ToDateTime);
			string selString = "";
			string conj = " AND ";

			selString = "(AOID='" + userId + "' OR BOID='" + userId + "') ";

			if (!string.IsNullOrEmpty(adId))
			{
				selString += conj + " (A_D='" + adId.Trim().ToUpper() + "')";
				conj = " AND ";
			}

			if (!string.IsNullOrEmpty(filter.Regn))
			{
				selString += conj + UtilDataSet.PrepareWhereClause(
					filter.Regn, EntWebFlightFilter.REGN_SEPERATOR,
					"REGN", "'");
				conj = " AND ";
			}
			if (!((filter.TerminalIds == null) || (filter.TerminalIds == "")))
			{
				selString += conj + UtilDataSet.PrepareWhereClause(
					filter.TerminalIds, EntWebFlightFilter.TERMINAL_SEPERATOR,
					"TERMINAL", "'");
				conj = " AND ";
			}

			selString += conj + " FLIGHT_DT>='" + stFr + "' ";
			selString += " AND FLIGHT_DT<='" + stTo + "'";
			string stSortBy = "";
			if (sortByTime == EnSort.Asc) stSortBy = "FLIGHT_DT ASC";
			else stSortBy = "FLIGHT_DT DESC";

			//LogTraceMsg("RetrieveFlights selString==>" + selString);
			foreach (DSFlight.FLIGHTRow row in tempDs.FLIGHT.Select(selString, stSortBy))
			{
				string airlineCode = "";
				try
				{
					airlineCode = row.FLNO.Substring(0, 3).Trim();
				}
				catch (Exception)
				{
				}
				if (filter.IsMeetCriteria(airlineCode, row.REGN))
				{
					//LogTraceMsg("RetrieveFlights:IN:" + row.FLNO + "," + row.ST + "," + airlineCode + "," + row.REGN);
					ds.FLIGHT.LoadDataRow(row.ItemArray, true);
					cnt++;
					if (cnt >= maxNoOfFlightsToFetch) break;
				}
				else
				{
					//LogTraceMsg("RetrieveFlights:OUT:" + row.FLNO + "," + row.ST + "," + airlineCode + "," + row.REGN);
				}
			}
			return ds;
		}

		public DSFlight RetrieveAssignedFlights(string terminal, string userId, int maxNoOfFlightsToFetch, EnSort sortByTime)
		{
			DSFlight ds = new DSFlight();
			DSFlight tempDs = dsFlight;
			int cnt = 0;
			string stSortBy;
			string terminalSel = "";
			if (!((terminal == null) || (terminal == ""))) terminalSel = " AND TERMINAL='" + terminal + "'";
			if (sortByTime == EnSort.Asc) stSortBy = "FLIGHT_DT ASC";
			else stSortBy = "FLIGHT_DT DESC";
			foreach (DSFlight.FLIGHTRow row in tempDs.FLIGHT.Select("(AOID='" + userId + "' OR BOID='" + userId + "') " + terminalSel, stSortBy))
			{
				ds.FLIGHT.LoadDataRow(row.ItemArray, true);
				cnt++;
				if (cnt >= maxNoOfFlightsToFetch) break;
			}
			return ds;
		}

		//public DSFlight RetrieveClosedFlightsForAO(string aoId, int maxNoOfFlightsToFetch, EnSort sortByTime)
		//{
		//    DSFlight ds = new DSFlight();
		//    DSFlight tempDs = dsFlight;
		//    int cnt = 0;
		//    string stSortBy;
		//    if (sortByTime == EnSort.Asc) stSortBy = "FLIGHT_DT ASC";
		//    else stSortBy = "FLIGHT_DT DESC";
		//    foreach (DSFlight.FLIGHTRow row in tempDs.FLIGHT.Select("AOID='" + aoId + "' AND CLOSEDFLIGHTS='Y'", stSortBy))
		//    {
		//        ds.FLIGHT.LoadDataRow(row.ItemArray, true);
		//        cnt++;
		//        if (cnt >= maxNoOfFlightsToFetch) break;
		//    }
		//    return ds;
		//}
		public DSFlight RetrieveClosedFlightsForAO(string aoId, int maxNoOfFlightsToFetch, EnSort sortByTime)
		{
			DSFlight ds = new DSFlight();
			DSFlight tempDs = dsFlight;
			DSFlight tempDs2 = tempDs;
			int cnt = 0;
			string stSortBy;
			if (sortByTime == EnSort.Asc) stSortBy = "FLIGHT_DT ASC";
			else stSortBy = "FLIGHT_DT DESC";
			foreach (DSFlight.FLIGHTRow row in tempDs.FLIGHT.Select("AOID='" + aoId + "'", stSortBy))
			{
				bool isClosed = false;
				//if (row.CLOSEDFLIGHTS == "Y") isClosed = true;
				//else
				//{
				//    if (!UtilDataSet.IsNullOrEmptyValue(row, "TURN"))
				//    {
				//        String stTurnFlid = row.TURN;

				//        if (!string.IsNullOrEmpty(stTurnFlid))
				//        {
				//            foreach (DSFlight.FLIGHTRow row2 in tempDs.FLIGHT.Select(
				//                "URNO='" + stTurnFlid + "' AND AOID='" + aoId + "' AND CLOSEDFLIGHTS='Y'"))
				//            {
				//                isClosed = true;
				//                break;
				//            }
				//        }
				//    }
				//}
				string flightId = "";
				string adid = "";
				string turnFlightId = "";
				if (!UtilDataSet.IsNullOrEmptyValue(row, "TURN"))
				{
					turnFlightId = row.TURN;
					if (!string.IsNullOrEmpty(turnFlightId))
					{
						DataRow[] rows = ds.FLIGHT.Select("URNO='" + turnFlightId + "'");
						if ((rows!=null) && (rows.Length>0)) continue;
					}
				}

				isClosed = IsClosedFlight(row, out flightId, out adid, out turnFlightId);
				if (isClosed)
				{
					ds.FLIGHT.LoadDataRow(row.ItemArray, true);
					cnt++;
				}
				if (cnt >= maxNoOfFlightsToFetch) break;
			}
			return ds;
		}

		public bool IsClosedFlight(DSFlight.FLIGHTRow row,
			out string flightId, out string adid,
			out string turnFlightId)
		{
			bool isClosed = false;
			flightId = "";
			adid = "";
			turnFlightId = "";

			if (row == null) throw new ApplicationException("Empty data.");
			flightId = row.URNO;
			adid = row.A_D;
			DSFlight tempDs = dsFlight;

			if (row.CLOSEDFLIGHTS == "Y") isClosed = true;
			else
			{
				if (!UtilDataSet.IsNullOrEmptyValue(row, "TURN"))
				{
					turnFlightId = row.TURN;

					if (!string.IsNullOrEmpty(turnFlightId))
					{
						foreach (DSFlight.FLIGHTRow row2 in tempDs.FLIGHT.Select(
							"URNO='" + turnFlightId + "' AND CLOSEDFLIGHTS='Y'"))
						{
							isClosed = true;
							break;
						}
					}
				}
			}
			return isClosed;
		}

		public DSFlight RetrieveOpenFlightsForAO(string aoId, int maxNoOfFlightsToFetch)
		{
			DSFlight ds = new DSFlight();
			DSFlight tempDs = dsFlight;
			int cnt = 0;
			foreach (DSFlight.FLIGHTRow row in tempDs.FLIGHT.Select("AOID='" + aoId + "' AND CLOSEDFLIGHTS<>'Y'", "FLIGHT_DT ASC"))
			{
				ds.FLIGHT.LoadDataRow(row.ItemArray, true);
				cnt++;
				if (cnt >= maxNoOfFlightsToFetch) break;
			}
			return ds;
		}

		public DSFlight RetrieveOpenFlightsForAO(string aoId)
		{
			DSFlight ds = new DSFlight();
			DSFlight tempDs = dsFlight;

			foreach (DSFlight.FLIGHTRow row in tempDs.FLIGHT.Select("AOID='" + aoId + "' AND CLOSEDFLIGHTS<>'Y'", "FLIGHT_DT ASC"))
			{
				ds.FLIGHT.LoadDataRow(row.ItemArray, true);
			}
			return ds;
		}
		/*Added by Alphy for testing purpose*/
		//public DSFlight RetrieveOpenFlightsForAO()
		//{
		//    DSFlight ds = new DSFlight();
		//    DSFlight tempDs = dsFlight;

		//    foreach (DSFlight.FLIGHTRow row in tempDs.FLIGHT.Select("CLOSEDFLIGHTS<>'Y'", "FLIGHT_DT ASC"))
		//    {
		//        ds.FLIGHT.LoadDataRow(row.ItemArray, true);
		//    }
		//    return ds;
		//}

		//public DSFlight RetrieveClosedFlightsForAO(int maxNoOfFlightsToFetch, EnSort sortByTime)
		//{
		//    DSFlight ds = new DSFlight();
		//    DSFlight tempDs = dsFlight;
		//    int cnt = 0;
		//    string stSortBy;
		//    if (sortByTime == EnSort.Asc) stSortBy = "FLIGHT_DT ASC";
		//    else stSortBy = "FLIGHT_DT DESC";
		//    foreach (DSFlight.FLIGHTRow row in tempDs.FLIGHT.Select("CLOSEDFLIGHTS='Y'", stSortBy))
		//    {
		//        ds.FLIGHT.LoadDataRow(row.ItemArray, true);
		//        cnt++;
		//        if (cnt >= maxNoOfFlightsToFetch) break;
		//    }
		//    return ds;
		//}

		public DSFlight dsFlight
		{
			get
			{
				if ((_dsFlight == null) || (_lastRefreshDT.CompareTo(DEFAULT_DT) == 0))
				{
					bool loadingSuccess = false;
					LoadDSFlight(out loadingSuccess, false);
					if (loadingSuccess)
					{
						_lastRefreshDT = ITrekTime.GetCurrentDateTime();
						LogTraceMsg("Refresh" + _lastRefreshDT.ToLongTimeString());
					}
				}
				else
				{
					Refresh();
				}

				if (_dsFlight == null)
				{
					Init();
					_dsFlight = new DSFlight();
				}

				return (DSFlight)_dsFlight;
			}
		}

		private DSArrivalFlightTimings dsArrivalFlightTiming
		{
			get
			{
                LogTraceMsg("_lastRefreshDTArr:" + _lastRefreshDTArr);
                int timeElapse = Math.Abs(UtilTime.DiffInSec(ITrekTime.GetCurrentDateTime(), _lastRefreshDTArr));
                LogTraceMsg("timeElapse:" + timeElapse);
				if ((_dsArrivalFlightTimings == null) || (timeElapse > ARR_FLT_REFRESH_SEC))
				{
					LoadDSArrivalFlightTimings();
					_lastRefreshDTArr = ITrekTime.GetCurrentDateTime();
				}
				if (_dsArrivalFlightTimings == null) _dsArrivalFlightTimings = new DSArrivalFlightTimings();

				return _dsArrivalFlightTimings;
			}
		}

		private DSDepartureFlightTimings dsDepartureFlightTiming
		{
			get
			{
				int timeElapse = Math.Abs(UtilTime.DiffInSec(ITrekTime.GetCurrentDateTime(), _lastRefreshDTDep));
				if ((_dsDepartureFlightTimings == null) || (timeElapse > DEP_FLT_REFRESH_SEC))
				{
					LoadDSDepartureFlightTimings();
					_lastRefreshDTDep = ITrekTime.GetCurrentDateTime();
				}
				if (_dsDepartureFlightTimings == null) _dsDepartureFlightTimings = new DSDepartureFlightTimings();
				//DSDepartureFlightTimings ds = (DSDepartureFlightTimings)_dsDepartureFlightTimings.Copy();

				return _dsDepartureFlightTimings;
			}
		}

		private const string DB_FLIGHT_URNO_COL_NAME = "URNO";

		/// <summary>
		/// Has Flight Changes after the given 'lastChgDt'. True - Has Changes. Will return the Latest Change Date Time for Flight table in "curChgDt".
		/// </summary>
		/// <param name="lastChgDt">Last Change Date Time known by iTrek</param>
		/// <param name="curChgDt">Latest Change Date Time of Flight in Database</param>
		/// <returns>True-Has changes after 'lastChgDt'</returns>
		public bool HasChanges(DateTime lastChgDt, out DateTime curChgDt)
		{
			bool changed = true;
			bool isExist = false;
            curChgDt = UtilDb.GetLastUpdateDateTimeOfTable(null, DB_Info.DB_T_FLIGHT, out isExist);

			if (!isExist)
			{
				throw new ApplicationException("Not able to get the Flight Last Update Time information.");
			}

			if (curChgDt.CompareTo(lastChgDt) > 0)
			{
				changed = true;
			}
			else
			{
				curChgDt = UtilDb.GetLastUpdateDateTimeOfTable(null, DB_Info.DB_T_ASR, out isExist);
				if (curChgDt.CompareTo(lastChgDt) > 0) changed = true;
				else
				{
					curChgDt = UtilDb.GetLastUpdateDateTimeOfTable(null, DB_Info.DB_T_BPTR, out isExist);
					if (curChgDt.CompareTo(lastChgDt) > 0) changed = true;
					else changed = false;
				}			
			}

			return changed;
		}

		/// <summary>
		/// Selection Command Text to fill the DSFlight
		/// </summary>
		private const string _cmdSelFlight = @"SELECT URNO,UAFT,FLNO,A_D,L_H,REGN,AIRCRAFT_TYPE,TTYP," +
  "FR,\"TO\",PARK_STAND,BELT,AOFNAME,AOLNAME,AO,BO," +
  @" MAB,TBS_UP,LD_ST,FST_CRGO,LD_END,LST_DOOR,PLB,UNL_STRT,AP_FST_TRP,BG_FST_TRP,AP_LST_TRP,BG_LST_TRP,FST_BAG,LST_BAG, UNL_END, TURN, 
  AP_FST_BM_MET,AP_LST_BM_MET,BG_FST_BM_MET,BG_LST_BM_MET,
  AP_FST_BM,AP_LST_BM,BG_FST_BM,BG_LST_BM,
  AP_FST_BAG_TIMING,AP_LST_BAG_TIMING,BG_FST_BAG_TIMING,BG_LST_BAG_TIMING,
  HAS_AO_RPT,TERMINAL,HAS_BO_RPT,
  ST,ET,OBL,AT,GATE,AOID,BOID,
  AO_ULD_RMK,AO_TIM_RMK
  FROM ";


        private static string GetDb_Tb_Flight()
        {
            return DB_Info.GetDbFullName(DB_Info.DB_T_FLIGHT);
        }
        private static string GetDb_Vw_Flight()
        {
            return DB_Info.GetDbFullName(DB_Info.DB_V_FLIGHT);
        }
		private static DateTime GetFrFlightDateTimeToLoad()
		{
			return ITrekTime.GetCurrentDateTime().Date.AddDays(-2);//Upto back 2 day.
		}

		private static DateTime GetToFlightDateTimeToLoad()
		{
			DateTime now = ITrekTime.GetCurrentDateTime();
			DateTime dt1 = now.Date.AddDays(1);
			DateTime dt2 = now.AddHours(+8);
			if (dt2.CompareTo(dt1) >= 0) return dt2;//Return the later date time.
			else return dt1;
		}

		/// <summary>
		/// Load Flight Information for 3 days (From back 2days to Today). Return True when successfully loaded.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="ds"></param>
		/// <returns>True-Load Successful.</returns>
		private bool LoadData(IDbCommand cmd, ref DSFlight ds, bool getChgDataOnly, DateTime frChgDateTime)
		{
			bool loadSuccess = false;
			UtilDb db = UtilDb.GetInstance();
			bool isCmdNull = false;
			try
			{
				if (cmd == null)
				{
					isCmdNull = true;
					cmd = db.GetCommand(false);
				}
				DateTime dt1 = DateTime.Now;

				if (ds == null) ds = new DSFlight();

				string paramNamePrefix = UtilDb.GetParaNamePrefix();
				string pnFrDate = paramNamePrefix + "FR_FL_DATE";
				string pnToDate = paramNamePrefix + "TO_FL_DATE";

				cmd.Parameters.Clear();

				DateTime now = Misc.ITrekTime.GetCurrentDateTime().Date;
				cmd.CommandText = string.Format("{0} WHERE (BFLT BETWEEN {1} AND {2}) ",
                    _cmdSelFlight + GetDb_Vw_Flight(),
					db.PrepareParamForTime(cmd, "FR_FL_DATE", GetFrFlightDateTimeToLoad()),
					db.PrepareParamForTime(cmd, "TO_FL_DATE", GetToFlightDateTimeToLoad()));
				if (getChgDataOnly)
				{
					cmd.CommandText += " AND LSTU>=" + db.PrepareParamForTime(cmd, "FR_LSTU", frChgDateTime);
				}
				//LogTraceMsg(String.Format("Cmd:{0}, Fr:{1:dd MMM yyyy hh:mm:ss}, To:{2:dd MMM yyyy hh:mm:ss}, getChg:{3}, Chg:{4:dd MMM yyyy hh:mm:ss}",
				//    cmd.CommandText, GetFrFlightDateTimeToLoad(), GetToFlightDateTimeToLoad(), getChgDataOnly, frChgDateTime));
				LogTraceMsg(String.Format("Cmd:{0}, Fr:{1:dd MMM yyyy hh:mm:ss}, To:{2:dd MMM yyyy hh:mm:ss}, getChg:{3}, Chg:{4:dd MMM yyyy hh:mm:ss}",
					"...", GetFrFlightDateTimeToLoad(), GetToFlightDateTimeToLoad(), getChgDataOnly, frChgDateTime));
				db.FillDataTable(cmd, ds.FLIGHT);

				DateTime dt2 = DateTime.Now;
				LogTraceMsg("Load Flight Data Time:" + UtilTime.ElapsedTime(dt1, dt2));

				if ((ds != null) && (ds.FLIGHT.Rows.Count > 0))
				{
					loadSuccess = true;
				}
				else
				{
					LogTraceMsg("No Flight Data.");
				}
			}
			catch (Exception ex)
			{
				LogTraceMsg("LoadData:Err:" + ex.Message);
				throw;
			}
			finally
			{
				if (isCmdNull)
				{
					db.CloseCommand(cmd, false);
					cmd = null;
				}
			}
			return loadSuccess;
		}

		/// <summary>
		/// Load Flight Data for given flightId. It will return True when loading success.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="ds"></param>
		/// <param name="flightId">Flight Id to fetch the data</param>
		/// <returns></returns>
		public bool LoadData(IDbCommand cmd, ref DSFlight ds, string flightId)
		{
			bool loadSuccess = false;
			UtilDb db = UtilDb.GetInstance();
			bool isCmdNull = false;
			try
			{
				if (cmd == null)
				{
					isCmdNull = true;
					cmd = db.GetCommand(false);
				}
				DateTime dt1 = DateTime.Now;

				if (ds == null) ds = new DSFlight();
				cmd.Parameters.Clear();

				cmd.CommandText = _cmdSelFlight +GetDb_Vw_Flight()+ " WHERE " + db.PrepareParam(cmd, "URNO", "=", "FLID", flightId);
				cmd.CommandType = CommandType.Text;

				db.FillDataTable(cmd, ds.FLIGHT);

				if ((ds != null) && (ds.FLIGHT.Rows.Count > 0))
				{
					loadSuccess = true;
                    LogTraceMsg("LoadData loadSuccess" + loadSuccess);
				}
				else
				{
					LogMsg("No Flight Data for " + flightId);
				}
			}
			catch (Exception)
			{
				throw;
			}
			finally
			{
				if (isCmdNull)
				{
					db.CloseCommand(cmd, false);
					cmd = null;
				}
			}

			return loadSuccess;
		}

		private void AddDsArrFlightRow(DSArrivalFlightTimings dsArrFl, DSFlight.FLIGHTRow fRow)
		{
			if (dsArrFl == null) dsArrFl = new DSArrivalFlightTimings();

			DSArrivalFlightTimings.AFLIGHTRow row = dsArrFl.AFLIGHT.NewAFLIGHTRow();
			row._FTRIP_B = fRow.BG_FST_TRP;
			row._LTRIP_B = fRow.BG_LST_TRP;
			//row._NEXT_TRIP = ?;
			//row._NEXT_TRIP_B = ?;
			row._TBS_UP = fRow.TBS_UP;
			row._UNLOAD_END = fRow.UNL_END;
			row._UNLOAD_START = fRow.UNL_STRT;
			row.ATA = fRow.AT;
			row.FBAG = fRow.FST_BAG;
			row.FTRIP = fRow.AP_FST_TRP;
			row.LBAG = fRow.LST_BAG;
			row.LTRIP = fRow.AP_LST_TRP;
			row.MAB = fRow.MAB;
			row.PLB = fRow.PLB;
			row.URNO = fRow.URNO;
			dsArrFl.AFLIGHT.AddAFLIGHTRow(row);
		}

		private void AddDsDepFlightRow(DSDepartureFlightTimings ds, DSFlight.FLIGHTRow fRow)
		{
			if (ds == null) ds = new DSDepartureFlightTimings();

			DSDepartureFlightTimings.DFLIGHTRow row = ds.DFLIGHT.NewDFLIGHTRow();
			row._LAST_DOOR = fRow.LST_DOOR;
			row._LOAD_END = fRow.LD_END;
			row._LOAD_START = fRow.LD_ST;
			//row._NEXT_TRIP = fRow.???;
			row.ATD = fRow.AT;
			row.FTRIP = fRow.BG_FST_TRP;
			row.LTRIP = fRow.BG_LST_TRP;
			row.MAB = fRow.MAB;
			row.PLB = fRow.PLB;
			//row.TRIPS = ???;
			row.URNO = fRow.URNO;
			ds.DFLIGHT.AddDFLIGHTRow(row);
		}

		private bool LoadDSArrivalFlightTimings()
		{
			bool loaded = true;
			DateTime curDt = _lastFLIGHTWriteDT;
            LogTraceMsg("LoadDSArrivalFlightTimings start");
			if ((_dsArrivalFlightTimings == null) || (_lastArrivalTimeWriteDT.CompareTo(curDt) != 0))
			{
				lock (_lockObjArrivalTime)
				{
					try
					{
						curDt = _lastFLIGHTWriteDT;
						if ((_dsArrivalFlightTimings == null) || (_lastArrivalTimeWriteDT.CompareTo(curDt) != 0))
						{
							try
							{
								loaded = false;
								DSArrivalFlightTimings tempDs = new DSArrivalFlightTimings();
								DSFlight tempDsFlight = dsFlight;
								DateTime now = Misc.ITrekTime.GetCurrentDateTime();
								//Keep the Arrival Flight Dataset for -24Hr to +8Hr from Current Time.
								string frFlightDt = UtilTime.ConvDateTimeToUfisFullDTString(now.AddDays(-1));
								string toFlightDt = UtilTime.ConvDateTimeToUfisFullDTString(now.AddHours(+8));

								string stFilter = string.Format("FLIGHT_DT>='{0}' AND FLIGHT_DT<='{1}'", frFlightDt, toFlightDt);

								foreach (DSFlight.FLIGHTRow fRow in tempDsFlight.FLIGHT.Select(stFilter))
								{
									AddDsArrFlightRow(tempDs, fRow);
								}

								_dsArrivalFlightTimings = tempDs;
								_lastArrivalTimeWriteDT = curDt;
								loaded = true;
							}
							catch (Exception ex)
							{
								LogMsg(ex.Message);
							}
						}
					}
					catch (Exception ex)
					{
						LogMsg("LoadDSArrivalFlightTimings:Err" + ex.Message);
					}
				}
			}
			return loaded;
		}

		private bool LoadDSDepartureFlightTimings()
		{
			bool loaded = true;
			DateTime curDT = _lastFLIGHTWriteDT;
			if ((_dsDepartureFlightTimings == null) || (curDT.CompareTo(_lastDepartureTimeWriteDT) != 0))
			{
				lock (_lockObjArrivalTime)
				{
					try
					{
						curDT = _lastFLIGHTWriteDT;//check the time again, in case of lock
						if ((_dsDepartureFlightTimings == null) || (curDT.CompareTo(_lastDepartureTimeWriteDT) != 0))
						{
							try
							{
								loaded = false;
								DSDepartureFlightTimings tempDs = new DSDepartureFlightTimings();
								DSFlight tempDsFlight = dsFlight;
								DateTime now = Misc.ITrekTime.GetCurrentDateTime();
								string frFlightDt = UtilTime.ConvDateTimeToUfisFullDTString(now.AddDays(-1));
								string toFlightDt = UtilTime.ConvDateTimeToUfisFullDTString(now.AddHours(+8));

								string stFilter = string.Format("FLIGHT_DT>='{0}' AND FLIGHT_DT<='{1}'", frFlightDt, toFlightDt);

								foreach (DSFlight.FLIGHTRow fRow in tempDsFlight.FLIGHT.Select(stFilter))
								{
									AddDsDepFlightRow(tempDs, fRow);
								}

								_dsDepartureFlightTimings = tempDs;
								_lastDepartureTimeWriteDT = curDT;
								loaded = true;
							}
							catch (Exception ex)
							{
								LogMsg(ex.Message);
							}
						}
					}
					catch (Exception ex)
					{
						LogMsg("LoadDSDepartureFlightTimings:Err:" + ex.Message);
					}
				}
			}
			return loaded;
		}

		private bool IsFullRefreshNeeded()
		{
			bool needFullRefresh = false;
			DateTime now = ITrekTime.GetCurrentDateTime();
			int hh = now.Hour;
			if ((hh >= 2) && (hh <= 10))
			{
				if (_lastFULLRefreshDT.CompareTo(DEFAULT_DT) == 0)
				{
					needFullRefresh = true;
				}
				else if (_lastFULLRefreshDT.CompareTo(now.AddHours(-2)) < 0)
				{
					needFullRefresh = true;
				}
			}

			return needFullRefresh;
		}

		private static bool _isFetchingFlightInfo = false;

		/// <summary>
		/// Load the flight information from database to common static variable dsFlight.
		/// </summary>
		/// <param name="loadSuccess">True - Loading Success</param>
		/// <param name="returnACopyWhenSuccess">True-To return the copy of dsFlight when success</param>
		/// <returns>Latest flight Information</returns>
		public DSFlight LoadDSFlight(out bool loadSuccess, bool returnACopyWhenSuccess)
		{
			loadSuccess = false;

			//LogMsg("Summarise the flight Infor");
			DateTime curChgDt = DEFAULT_DT;
			if (!_isFetchingFlightInfo)
			{
				if ((_dsFlight == null) || HasChanges(_lastFLIGHTWriteDT, out curChgDt))
				{
					lock (_lockObj)
					{
						_isFetchingFlightInfo = true;
						try
						{
							if ((_dsFlight == null) || HasChanges(_lastFLIGHTWriteDT, out curChgDt))
							{
								try
								{
									DSFlight tempDs = new DSFlight();
									DateTime frDateTime = _lastFLIGHTWriteDT;
									DateTime earliestDateTime = GetFrFlightDateTimeToLoad();
									bool loadChgDataOnly = true;

									if (frDateTime.CompareTo(earliestDateTime) < 0)
									{
										frDateTime = earliestDateTime;
										loadChgDataOnly = false;
									}
									else if (IsFullRefreshNeeded())
									{
										loadChgDataOnly = false;
									}

									if (LoadData(null, ref tempDs, loadChgDataOnly, frDateTime))
									{
										if ((tempDs != null) && (tempDs.FLIGHT.Rows.Count > 0))
										{
											lock (_lockUpdDsFlight)
											{
												if (loadChgDataOnly)
												{
													tempDs = UpdateDsFlight(_dsFlight, tempDs);
													loadSuccess = true;
													SetLatestInfoToDataset(tempDs, curChgDt);
												}
												else
												{
													loadSuccess = true;
													SetLatestInfoToDataset(tempDs, curChgDt);
													_lastFULLRefreshDT = ITrekTime.GetCurrentDateTime();
													LogTraceMsg(string.Format("Full Refresh {0:dd MMM yyyy HH}", frDateTime));
												}
											}
										}
										else
										{
											LogMsg("No Flight Data.");
										}
									}
								}
								catch (Exception ex)
								{
									LogMsg("LoadDSFlight:Err:" + ex.Message);
									throw new ApplicationException("Error Accessing Flight Information.");
								}
							}
						}
						catch (Exception ex)
						{
							LogMsg("LoadDSFlight:Err:" + ex.Message);
						}
						finally
						{
							_isFetchingFlightInfo = false;
						}
					}
				}
				else
				{
					loadSuccess = true;
				}
			}

			if ((loadSuccess) && (returnACopyWhenSuccess))
			{
				return (DSFlight)_dsFlight.Copy();
			}
			return _dsFlight;
		}

		private static object _lockUpdDsFlight = new object();
		private DSFlight UpdateDsFlight(DSFlight curData, DSFlight updData)
		{
			DSFlight dsNew = (DSFlight)curData.Copy();
			foreach (DSFlight.FLIGHTRow row in updData.FLIGHT.Rows)
			{
				dsNew.FLIGHT.LoadDataRow(row.ItemArray, true);
			}
			return dsNew;
		}

		private void SetLatestInfoToDataset(DSFlight ds, DateTime curChgDt)
		{
			lock (_lockUpdDsFlight)
			{
				_dsFlight = ds;
				if (curChgDt.CompareTo(DEFAULT_DT) <= 0) _lastFLIGHTWriteDT = Misc.ITrekTime.GetCurrentDateTime().AddDays(-1);
				else _lastFLIGHTWriteDT = curChgDt.AddMinutes(-30);
			}
		}

		//[Conditional("TRACE_ON")]
		private static void LogTraceMsg(string msg)
		{
			try
			{
				MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("DBDFlight", msg);
			}
			catch (Exception)
			{
				UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DBDFlight", msg);
			}
		}

		private static void LogMsg(string msg)
		{
			//Util.UtilLog.LogToGenericEventLog(" DBDFlight", msg);
			//UFIS.ITrekLib.Util.UtilLog.LogToTraceFile(" DBDFlight", msg);
			try
			{
				MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("DBDFlight", msg);
			}
			catch (Exception)
			{
				UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DBDFlight", msg);
			}
		}

		public bool UpdateFlightTiming(IDbCommand cmd, 
			string flightId, string fieldName, string fieldValue, string staffId, EnumFlightType flType)
		{
			Hashtable ht = new Hashtable();
			ht.Add(fieldName, fieldValue);
			return UpdateFlightTiming(cmd, flightId, ht, staffId, flType);
		}

		/// <summary>
		/// Update Flight timing. Return true when success
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="flightId"></param>
		/// <param name="htFieldValues">Field Name (ITK_FLIGHT Col Name) as 'Key' and Field Value as 'Value'</param>
		/// <param name="staffId"></param>
		/// <param name="flType"></param>
		/// <returns></returns>
		public bool UpdateFlightTiming(IDbCommand cmd, 
			string flightId, Hashtable htFieldValues, string staffId, EnumFlightType flType)
		{
			bool resultOk = false;
			string stMsg = "";
			if (flightId == "")
			{
				throw new ApplicationException("UpdateFlightTiming:Err:Not enough info.");
			}

			lock (_lockObj)
			{
				try
				{
					//LockFlightSummForWriting();

					UtilDb udb = UtilDb.GetInstance();
					bool hasMapping = false;
					ITrekColMapping colMap = ITrekColMapping.GetInstance();
					string qName = GetFromItrekUpdateQueueName();
					List<string> msgTimingUpdToBackend = new List<string>();


					int chgCnt = 0;

					//Get the latest data
					DSFlight tDsFlight = new DSFlight();
					if (!LoadData(cmd, ref tDsFlight, flightId))
					{
						throw new ApplicationException("Unable to get the flight info for " + flightId);
					}

					stMsg += "UpdateFlightTiming:" + flightId + "," + staffId;
					DSFlight.FLIGHTRow fRow = (DSFlight.FLIGHTRow)tDsFlight.FLIGHT.Select("URNO='" + flightId + "'")[0];
					string uaft = "";
					if (fRow["UAFT"] != null)
					{
						uaft = fRow["UAFT"].ToString().Trim();
					}

					DateTime curDt = ITrekTime.GetCurrentDateTime();
					StringBuilder sb = new StringBuilder();

					cmd.Parameters.Clear();
					string paramFlightId = udb.PrepareParam(cmd, "URNO", flightId);

					sb.Append(udb.PrepareUpdCmdToUpdDb(cmd, "USEU", staffId));
					sb.Append("," + udb.PrepareUpdCmdToUpdDb(cmd, "LSTU", curDt));

					foreach (string stFieldName in htFieldValues.Keys)
					{
						string newTime = htFieldValues[stFieldName].ToString();
						if (newTime == null) newTime = "";
						else newTime = newTime.Trim();

						hasMapping = false;
						string flFieldName = null;
						string dbfieldName = null;
						string stFlightType = "U";//Undefined.
						switch (flType)
						{
							case EnumFlightType.Arrival:
								flFieldName = colMap.GetDsFlightColForDsArrFlight(stFieldName, out hasMapping);
								dbfieldName = colMap.GetDBT_FlightColForDsArrFlight(stFieldName, out hasMapping);
								stFlightType = "A";
								break;
							case EnumFlightType.Departure:
								flFieldName = colMap.GetDsFlightColForDsDepFlight(stFieldName, out hasMapping);
								dbfieldName = colMap.GetDBT_FlightColForDsDepFlight(stFieldName, out hasMapping);
								stFlightType = "D";
								break;
							default:
								throw new ApplicationException("Invalid Flight Type");
							//break;
						}

						string orgTime = "$#%@!";//Impossible Time
						try
						{
							orgTime = fRow[flFieldName].ToString();
						}
						catch (Exception ex) { LogTraceMsg("UpdateFlightTiming:Err:" + flFieldName + "," + ex.Message); }

						if (newTime.CompareTo(orgTime) != 0)
						{

							if (string.IsNullOrEmpty(newTime))
							{
								newTime = "";
								sb.Append("," + udb.PrepareUpdCmdToUpdDbForTime(cmd, dbfieldName, null));
							}
							else
							{
								//DateTime dtNewTime = UtilTime.ConvUfisTimeStringToDateTime(newTime);
								sb.Append("," + udb.PrepareUpdCmdToUpdDbForTime(cmd, dbfieldName, newTime));
							}

							string msgToBackEnd = CMsgOut.MsgForFlightTiming(uaft, stFlightType, staffId, stFieldName, newTime);
							msgTimingUpdToBackend.Add(msgToBackEnd);

							stMsg += "," + stFieldName + "," + orgTime + "==>" + newTime;
							fRow[flFieldName] = newTime;
							chgCnt++;
						}
					}

					if (chgCnt > 0)
					{
						#region Do Update
						cmd.CommandText = string.Format("UPDATE {0} SET {1} WHERE URNO={2}",
                         GetDb_Tb_Flight(), sb.ToString(), paramFlightId);
						cmd.CommandType = CommandType.Text;
						//LogTraceMsg("UpdateFlightTiming:Cmd:" + cmd.CommandText);
						int updCnt = cmd.ExecuteNonQuery();
						if (updCnt < 1) throw new ApplicationException("Update Fail!!!");
						foreach (string msgToBackEnd in msgTimingUpdToBackend)
						{
							string stReplyTo = "";
							string msgOutUrno = CMsgOut.NewMessageToSend(cmd,
							   qName, msgToBackEnd, staffId,
							   CMsgOut.MSG_TYPE_FLIGHT_TIMING_UPD,
							   out stReplyTo);
						}

						List<string> flightIds = new List<string>();
						flightIds.Add(flightId);
						UpdateBMTimingInDb(cmd, flightIds, staffId, curDt);
						#endregion
					}
					else
					{
						LogMsg("UpdateArrivalTime:No changes to update.");
					}

					resultOk = true;
				}
				catch (Exception ex)
				{
					LogMsg("UpdateArrivalTime:Err:" + ex.Message);
					throw;
				}
				finally
				{
					if (stMsg != "")
					{
						LogTraceMsg(stMsg);
					}
					//ReleaseFlightSummAfterWrite();
				}
			}
			return resultOk;
		}

		public static void UpdateBMTimingInDb(IDbCommand cmd, List<string> flightIds, string userId, DateTime updDateTime)
		{
			DsDbFlight dsDbCur = RetrieveDsDbFlightFromDb(cmd, flightIds);
			if (dsDbCur.ITK_FLIGHT.Rows.Count > 0)
			{
				foreach (DsDbFlight.ITK_FLIGHTRow row in dsDbCur.ITK_FLIGHT.Rows)
				{
					ComputeTimingForBM(row);
				}
				int insCnt, updCnt, delCnt;
                UtilDb.InsertUpdateDataToDb(cmd, dsDbCur.ITK_FLIGHT, DB_Info.DB_T_FLIGHT,
					ProcIds.FLIGHT, userId, updDateTime, out insCnt, out updCnt, out delCnt);
			}
		}


		private const string ADID_DEPARTURE_IND = "D";
		private const string ADID_ARRIVAL_IND = "A";

		/// <summary>
		/// Update Original Flight Info (comming from interface) into iTrek Database.
		/// If flight record not existed in iTrek Database, it will not update anything.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="flightXML"></param>
		/// <param name="userId"></param>
		/// <param name="updDateTime"></param>
		public void UpdateOrgFlightInfoUpd(IDbCommand cmd, string flightXML, string userId, DateTime updDateTime, bool hasRecordInItrek,out string flightId)
		{
			//<FLIGHT><URNO>1966482260</URNO><ACTION>UPDATE</ACTION><ETAI>20081031222300</ETAI></FLIGHT>
			hasRecordInItrek = false;
			ITrekColMapping colMapping = ITrekColMapping.GetInstance();
			XmlDocument newFlightData = new XmlDocument();
			//XmlNode oldFlightData;
			XmlElement root;
			//string URNO = "";
			//XmlElement updatedFlightData;
			try
			{
				newFlightData.LoadXml(flightXML);
			}
			catch (Exception ex)
			{
				LogMsg("UpdateOrgFlightInfo:Err:" + ex.Message);
			}
			bool hasMapping = false;
			root = newFlightData.DocumentElement;
			XmlNode nodeUrno;
			string uaft = "";
			flightId = "";
			string adid = "";
			UtilDb udb = UtilDb.GetInstance();

			if ((nodeUrno = newFlightData.SelectSingleNode("//FLIGHT/URNO")) != null)
			{
				uaft = nodeUrno.InnerXml;

				flightId = GetITrekFlightIdForUaft(cmd, uaft, "", out hasRecordInItrek, out adid);
			}

			if (hasRecordInItrek)
			{//Flight information is existed in iTrek DB.
				cmd.Parameters.Clear();
				StringBuilder sb = new StringBuilder();
				sb.Append(udb.PrepareUpdCmdToUpdDb(cmd, "USEU", userId));
				sb.Append("," + udb.PrepareUpdCmdToUpdDb(cmd, "LSTU", updDateTime));

				bool hasChg = false;
				bool hasTurnChg = false;
				string stOldITurn = "";
				string stNewITurn = "";
				foreach (XmlNode node in root.ChildNodes)
				{
					string orgColName = node.Name;
					if (orgColName == "URNO") continue;
					string data = node.InnerXml;

					string dbColName = colMapping.GetDBT_FlightColForOrgFlight(orgColName, out hasMapping);
					if ((hasMapping) && (!string.IsNullOrEmpty(dbColName)))
					{
						if ("STOA,STOD,ETDI,ETAI,ONBL,OFBL".Contains(dbColName))
						{//Time Field
							sb.Append("," + udb.PrepareUpdCmdToUpdDbForTime(cmd, dbColName, data));
						}
						else
						{
							if (dbColName == "ITURN")
							{
								data = data.Trim().ToUpper();
								bool hasRec = false;
								string stAdid = "";
								IDbCommand cmdSel = null;
								try
								{
									cmdSel = udb.GetCommand(cmd.Connection, cmd.Transaction);
									stOldITurn = UtilDb.GetDataInString(cmdSel, DB_Info.DB_T_FLIGHT, flightId, "ITURN").Trim().ToUpper();
									if (stOldITurn != data)
									{
										hasTurnChg = true;
										stNewITurn = data;
									}
								}
								catch (Exception)
								{
									throw;
								}
								finally
								{
									if (cmdSel != null)
									{
										cmdSel.Dispose();
										cmdSel = null;
									}
								}
							}
							sb.Append("," + udb.PrepareUpdCmdToUpdDb(cmd, dbColName, data));
						}
						hasChg = true;
					}
				}
				if (hasChg)
				{
					cmd.CommandText = string.Format("UPDATE {0} SET {1} WHERE URNO='{2}'",
                       GetDb_Tb_Flight(), sb.ToString(), flightId);
					cmd.CommandType = CommandType.Text;

					int updCnt = cmd.ExecuteNonQuery();
					if (updCnt < 1) throw new ApplicationException("Update Fail!!!");

					if (hasTurnChg)
					{//Turnaround information was changed
						string stOldTurnFlightId = "";
						string stNewTurnFlightId = "";
						bool hasRec = false;
						string stAdid = "";
						if (!string.IsNullOrEmpty(stOldITurn))
						{
							stOldTurnFlightId = GetITrekFlightIdForUaft( cmd, stOldITurn, "", out hasRec, out stAdid );
							if (!hasRec)
							{
								stOldTurnFlightId = "";
							}
							if (!string.IsNullOrEmpty(stOldTurnFlightId))
							{
								UpdateAFlightWithTurnInfo(cmd, stOldTurnFlightId, "", "", updDateTime, userId, false);
							}
						}

						if (!string.IsNullOrEmpty(stNewITurn))
						{
							stNewTurnFlightId = GetITrekFlightIdForUaft(cmd, stNewITurn, "", out hasRec, out stAdid);
							if (!hasRec)
							{
								stNewTurnFlightId = "";
							}
							if (!string.IsNullOrEmpty(stNewTurnFlightId))
							{
								UpdateAFlightWithTurnInfo(cmd, stNewTurnFlightId, uaft, flightId, updDateTime, userId, false);
							}
						}
						UpdateAFlightWithTurnInfo(cmd, flightId, stNewITurn, stNewTurnFlightId, updDateTime, userId, false);
					}
				}
				
			}
			else
			{//The flight information is not existed in iTrek DB.
				LogMsg("UpdateOrgFlightInfoUpd:Warn:Record Not found for " + flightXML);
			}
		}

		public void UpdateOrgFlightInfoDelete(IDbCommand cmd, string flightId, string userId, DateTime updDateTime)
		{
			cmd.Parameters.Clear();
			StringBuilder sb = new StringBuilder();
			UtilDb udb = UtilDb.GetInstance();
			sb.Append(udb.PrepareUpdCmdToUpdDb(cmd, "USEU", userId));
			sb.Append("," + udb.PrepareUpdCmdToUpdDb(cmd, "LSTU", updDateTime));
			sb.Append("," + udb.PrepareUpdCmdToUpdDb(cmd, "STAT", "X"));//Deleted Status

			cmd.CommandText = string.Format("UPDATE {0} SET {1} WHERE URNO='{2}'",
              GetDb_Tb_Flight(), sb.ToString(), flightId);
			cmd.CommandType = CommandType.Text;

			int updCnt = cmd.ExecuteNonQuery();
			if (updCnt < 1) throw new ApplicationException("Fail to delete!!!");
		}

		private string GetInfoForColumn(DataRow row, string colName, bool allowNoColumn)
		{
			string st = "";
			try
			{
				st = (string)row[colName];
			}
			catch (Exception)
			{
				if (!allowNoColumn) throw;
			}
			return st;
		}

		#region InsertUpdDbOrgFlightInfoToDb

		/// <summary>
		/// Update the Original Flight Info data (Datatable are same type as FLIGHT Table) to the itrek database
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="dt"></param>
		/// <param name="userId"></param>
		/// <param name="curDt"></param>
		private int UpdateOrgFlightInfoDataToDb(IDbCommand cmd, DataTable dt, string userId, DateTime curDt)
		{
			cmd.Parameters.Clear();
			StringBuilder sb = new StringBuilder();
			UtilDb udb = UtilDb.GetInstance();
			List<string> lsColNames = new List<string>();
			string seperator = "";
			string paramPrefix = UtilDb.GetParaNamePrefix();

			foreach (DataColumn col in dt.Columns)
			{
				if ("URNO,CDAT,USEC".Contains(col.ColumnName)) continue;
				lsColNames.Add(col.ColumnName);
				if (col.DataType == typeof(DateTime))
				{
					sb.Append(seperator + udb.PrepareUpdCmdToUpdDbForTime(cmd, col.ColumnName, ""));
				}
				else
				{
					sb.Append(seperator + udb.PrepareUpdCmdToUpdDb(cmd, col.ColumnName, null));
				}
				seperator = ",";
			}

			string stWhere = "URNO=" + udb.PrepareParam(cmd, "URNO", null);
			lsColNames.Add("URNO");

			string stCmd = string.Format("UPDATE {0} SET {1} WHERE {2}",
              GetDb_Tb_Flight(), sb.ToString(), stWhere);
			if (stCmd != cmd.CommandText)
			{
				cmd.CommandText = stCmd;
				cmd.CommandType = CommandType.Text;
				cmd.Prepare();
			}

			int cntCol = lsColNames.Count;
			int totUpdCnt = 0;
			foreach (DataRow row in dt.Rows)
			{
				for (int i = 0; i < cntCol; i++)
				{
					((IDataParameter)cmd.Parameters[paramPrefix + lsColNames[i]]).Value = row[lsColNames[i]];
				}
				int updCnt = cmd.ExecuteNonQuery();
				if (updCnt < 1) throw new ApplicationException("Update Fail!!!");
				totUpdCnt++;
			}
			return totUpdCnt;
		}

		/// <summary>
		/// Insert the Original Flight Info data (Datatable are same type as FLIGHT Table) to the itrek database
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="dt"></param>
		/// <param name="userId"></param>
		/// <param name="curDt"></param>
		/// <returns></returns>
		private int InsertOrgFlightInfoDataToDb(IDbCommand cmd, DataTable dt, string userId, DateTime curDt)
		{
			UtilDb udb = UtilDb.GetInstance();
			int totUpdCnt = 0;
			List<string> stIds = UtilDb.GenUrids(cmd, ProcIds.FLIGHT,
			   false, DateTime.Now, dt.Rows.Count);//Generate the Urnos.

			cmd.Parameters.Clear();
			StringBuilder sb = new StringBuilder();
			StringBuilder sbDbColNames = new StringBuilder();

			List<string> lsColNamesToAssignValue = new List<string>();//Name of the column to assign the value
			string seperator = "";
			string paramPrefix = UtilDb.GetParaNamePrefix();

			#region Prepare the parameters with names only (without value)
			foreach (DataColumn col in dt.Columns)
			{
				lsColNamesToAssignValue.Add(col.ColumnName);
				if (col.DataType == typeof(DateTime))
				{
					sb.Append(seperator + udb.PrepareParamForTime(cmd, col.ColumnName, ""));
				}
				else
				{
					sb.Append(seperator + udb.PrepareParam(cmd, col.ColumnName, null));
				}
				sbDbColNames.Append(seperator + col.ColumnName);
				seperator = ",";
			}
			#endregion

			string stCmd = string.Format(@"INSERT INTO {0} ({1}) VALUES ({2})",
            GetDb_Tb_Flight(), sbDbColNames.ToString(), sb.ToString());

			if (stCmd != cmd.CommandText)
			{
				cmd.CommandText = stCmd;
				cmd.CommandType = CommandType.Text;
				cmd.Prepare();
			}

			int cntCol = lsColNamesToAssignValue.Count;

			int idxRowIdx = -1;
			foreach (DataRow row in dt.Rows)
			{
				idxRowIdx++;
				try
				{
					#region Assign Value for the parameter and Update to the database
					if (row[DB_FLIGHT_URNO_COL_NAME].ToString().StartsWith(UtilDb.PFIX_VIRTUAL_NEW_ID))
					{//It is virtual Urno
						row[DB_FLIGHT_URNO_COL_NAME] = stIds[idxRowIdx];//Assign the Actual Urno.
					}
					
					for (int i = 0; i < cntCol; i++)
					{
						string colName = lsColNamesToAssignValue[i];
						try
						{
							((IDataParameter)cmd.Parameters[paramPrefix + colName]).Value = row[colName];
						}
						catch (Exception ex)
						{
							LogMsg("InsertOrgFlightInfoDataToDb:Err:AssignVal:" + colName + ":" + ex.Message + Environment.NewLine + UtilDataSet.GetDataRowString(row));
							//throw new ApplicationException(ex.Message + Environment.NewLine + UtilDataSet.GetDataRowString(row));
						}
					}
					int updCnt = cmd.ExecuteNonQuery();
					if (updCnt < 1) throw new ApplicationException("Update Fail!!!");
					totUpdCnt++;
					row.AcceptChanges();
					#endregion
				}
				catch (Exception ex)
				{
					string st = ex.Message + Environment.NewLine + UtilDataSet.GetDataRowString(row);
					LogMsg("InsertOrgFlightInfoDataToDb:Err:" + ex.Message);
					throw;//Throw error when one record was not able to update.
				}
			}
			return totUpdCnt;
		}
		#endregion

		private DataSet RetrieveDataFromDb(IDbCommand cmd, List<string> lsUaft, DateTime frDate, DateTime toDate)
		{
			cmd.Parameters.Clear();
			DataSet dsNew = new DataSet();
			dsNew.Tables.Add();
			UtilDb udb = UtilDb.GetInstance();

			//string stUafts = FormDbString(lsUaft);
			string stUafts = UtilMisc.ConvListToString(lsUaft, ",", "'", "'", 0, lsUaft.Count);
			if (!string.IsNullOrEmpty(stUafts))
			{
				string whereClause = string.Format(" WHERE UAFT IN ({0}) AND ((STOA BETWEEN {1} AND {2}) OR (STOD BETWEEN {1} AND {2}))",
				   stUafts,
				   udb.PrepareParam(cmd, "FR_DT", frDate.AddDays(-7)),
				   udb.PrepareParam(cmd, "TO_DT", toDate.AddDays(7)));
                cmd.CommandText = "SELECT URNO,UAFT,FLNO,ADID,REGN,TTYP,ACT3,ORG3,DES3,BLT1,TURN,ITURN,PSTA,TGA1,STOA,ETAI,ONBL,GTA1,PSTD,TGD1,STOD,ETDI,OFBL,GTD1,CDAT,USEC,LSTU,USEU FROM " + GetDb_Tb_Flight() +
				   whereClause;

				cmd.CommandType = CommandType.Text;

				udb.FillDataTable(cmd, dsNew.Tables[0]);
			}
			dsNew.AcceptChanges();
			return dsNew;
		}

		/// <summary>
		/// Get New Flight Id (Flight Id in iTrek) for given back-end flight id and schedule data and time of flight
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="uaft">Backend flight id</param>
		/// <param name="stDateTime">Scheduled Flight Date Time in yyyymmddHHMMSS (e.g. 20090130235645)</param>
		/// <returns>newly generated Flight Id</returns>
		public string GenNewFlightId(IDbCommand cmd, string uaft, string stDateTime)
		{
			return UtilDb.GenUrid(cmd, ProcIds.FLIGHT, false, DateTime.Now);
		}


		/// <summary>
		/// Update Original Flight Data (batch data) from backend to iTrek Database.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="dsOrgFlight">Dataset of Original Flight Data from backend</param>
		/// <param name="userId"></param>
		/// <param name="updateDateTime"></param>
		/// <param name="cntInsert">Insert Record Count to iTrek Database</param>
		/// <param name="cntUpdate">Update Record Count to iTrek Database</param>
		/// <param name="cntDelete">Delete Record Count to iTrek Database</param>
		/// <param name="cntWithRecordInDb">Existing Record (backend data which was already existed in iTrek) Count in iTrek Database</param>
		public void UploadOrgFlightDataToDb(IDbCommand cmd, DataSet dsOrgFlight,
		   string userId, DateTime updateDateTime,
		   out int cntInsert, out int cntUpdate, out int cntDelete, out int cntWithRecordInDb)
		{
			UtilDb udb = UtilDb.GetInstance();
			List<string> lsUaft = new List<string>();
			string frDate = UtilTime.ConvDateTimeToUfisFullDTString(DateTime.Now);
			string toDate = UtilTime.ConvDateTimeToUfisFullDTString(DateTime.Now);
			int cntNewId = 0;
			DateTime dt1 = DateTime.Now;
			List<String> lsUaftWithTurn = new List<string>();
			if (!dsOrgFlight.Tables["FLIGHT"].Columns.Contains("ITURN"))
			{
				dsOrgFlight.Tables["FLIGHT"].Columns.Add("ITURN");
			}
			
			#region Get all Uaft and From and To DateTime range
			foreach (DataRow row in dsOrgFlight.Tables["FLIGHT"].Rows)
			{
				string uaft = (string)row["URNO"];
				if (string.IsNullOrEmpty(uaft)) continue;
				if (!UtilDataSet.IsNullOrEmptyValue(row, "TURN"))
				{
					string stTurn = Convert.ToString(row["TURN"]);
					lsUaftWithTurn.Add(uaft);
					lsUaft.Add(stTurn);
				}
				row["ITURN"] = row["TURN"];//BackEnd TURN will be assign to iTrek 'ITURN'
				lsUaft.Add(uaft);

				string adid = (string)row["ADID"];
				string stDt = "";
				if (adid == "D")
				{
					stDt = (string)row["STOD"];
				}
				else
				{
					stDt = (string)row["STOA"];
				}
				if (!string.IsNullOrEmpty(stDt) && stDt.Length == 14)
				{
					if (string.Compare(stDt, frDate) < 0) frDate = stDt;
					else if (string.Compare(stDt, toDate) > 0) toDate = stDt;
				}
			}

			#endregion
			DateTime dt2 = DateTime.Now;

			#region Get the Current Data for all the Uaft from Database
			DataSet dsCur = RetrieveDataFromDb(cmd, lsUaft, UtilTime.ConvUfisTimeStringToDateTime(frDate),
			   UtilTime.ConvUfisTimeStringToDateTime(toDate));
			//int cnt1 = dsCur.Tables[0].Rows.Count;
			cntWithRecordInDb = dsCur.Tables[0].Rows.Count;
			#endregion
			DateTime dt3 = DateTime.Now;

			#region Update/Insert the Information in DB Dataset
			foreach (DataRow row in dsOrgFlight.Tables["FLIGHT"].Rows)
			{
				string uaft = (string)row["URNO"];
				if (string.IsNullOrEmpty(uaft)) continue;

				string adid = (string)row["ADID"];
				string stColName = "STOA";
				if (adid == "D") stColName = "STOD";
				string stOrg = (string)row[stColName];//STOA/STOD from original flight info

				DataRow[] rowCurs = dsCur.Tables[0].Select("UAFT='" + uaft + "'");
				if (rowCurs == null || rowCurs.Length < 1)
				{//No match. New Data
					//string flightId = GenNewFlightId(cmd, uaft, stOrg);
					string flightId = UtilDb.PFIX_VIRTUAL_NEW_ID + cntNewId;
					cntNewId++;
					AssignNewData(row, flightId, dsCur, userId, updateDateTime);
				}
				else if (rowCurs.Length == 1)
				{
					string flightId = (string)rowCurs[0]["URNO"];
					AssignExistingData(row, flightId, rowCurs[0], userId, updateDateTime);
				}
				else
				{//Multiple Record with same uaft.
					int cnt = rowCurs.Length;

					for (int i = 0; i < cnt; i++)
					{
						if (stOrg == GetTimeString(rowCurs[i], stColName))
						{//Found Matched record
							string flightId = (string)rowCurs[i]["URNO"];
							AssignExistingData(row, flightId, rowCurs[i], userId, updateDateTime);
							break;
						}
					}
				}
			}
			#endregion
			DateTime dt4 = DateTime.Now;
			#region get the date changes (New Record and Updated Records)
			DataTable dtChgIns = dsCur.Tables[0].GetChanges(DataRowState.Added);
			DataTable dtChgUpd = dsCur.Tables[0].GetChanges(DataRowState.Modified);

			int cnt1 = dsCur.Tables[0].Rows.Count;
			//IDbDataAdapter da = GetOrgFlightDataAdapter(cmd);
			cntInsert = 0;
			cntUpdate = 0;
			cntDelete = 0;
			#endregion
			DateTime dt5 = DateTime.Now;
			#region Insert the New Records
			if (dtChgIns != null)
			{
				cntInsert = dtChgIns.Rows.Count;
				//udb.Update(da, dtChgIns);
				InsertOrgFlightInfoDataToDb(cmd, dtChgIns, userId, updateDateTime);
			}
			#endregion
			DateTime dt6 = DateTime.Now;

			Dictionary<string, FlightTurnInfo> deFlightTurnInfo = new Dictionary<string, FlightTurnInfo>();

			#region Update the Changed Records
			if (dtChgUpd != null)
			{
				cntUpdate = dtChgUpd.Rows.Count;
				//udb.Update(da, dtChgUpd);
				foreach (DataRow row in dtChgUpd.Rows)
				{
					string stOldITurn = UtilDataSet.GetDataString(row, "ITURN", DataRowVersion.Original).Trim().ToUpper();
					string stNewITurn = UtilDataSet.GetDataString(row, "ITURN", DataRowVersion.Current).Trim().ToUpper();
					
					if (stOldITurn != stNewITurn)
					{
						string stOldTurn = UtilDataSet.GetDataString(row, "TURN", DataRowVersion.Original);
						string stNewTurn = UtilDataSet.GetDataString(row, "TURN", DataRowVersion.Current);
						string stUaft = UtilDataSet.GetDataString(row, "UAFT");
						string stUrno = UtilDataSet.GetDataString(row, "URNO");
						string stAdid = (string)row["ADID"];
						string stFldt = "";
						if (stAdid == "A")
						{
							stFldt = UtilTime.ConvDateTimeToUfisFullDTString((DateTime)row["STOA"]);
						}
						else
						{
							stFldt = UtilTime.ConvDateTimeToUfisFullDTString((DateTime)row["STOD"]);
						}

						if (string.IsNullOrEmpty(stNewITurn))
						{//No ITurn in New Data
							if (string.IsNullOrEmpty(stOldITurn))
							{//No ITurn in Old Data, No ITurn in New Data
								//Nothing to do.
							}
							else
							{//Has ITurn in Old Data, No ITurn in New Data
								//Update the Turn Info in Turn Flight Data which is related to this flight
#warning TODO Update the Turn Info in Turn Flight Data which is related to this flight
							    
								if (string.IsNullOrEmpty(stOldTurn))
								{
									bool hasRec = false;
									string stAdidTurn = "";
									stOldTurn = GetITrekFlightIdForUaft(cmd, stOldITurn, stFldt, out hasRec, out stAdidTurn);
								}

								if (!string.IsNullOrEmpty(stOldTurn))
								{
									if (!deFlightTurnInfo.ContainsKey(stUrno))
										deFlightTurnInfo.Add(stUrno, new FlightTurnInfo(stUrno, stUaft, "", ""));
									if (!deFlightTurnInfo.ContainsKey(stOldTurn))
										deFlightTurnInfo.Add(stOldTurn, new FlightTurnInfo(stOldTurn, stOldITurn, "", ""));
								}
								row["TURN"] = "";
							}
						}
						else
						{//Has ITrun in New Data
							if (string.IsNullOrEmpty(stOldITurn))
							{//No ITurn in Old Data, Has ITurn in New Data
								//Update the Turn Info in Turn Flight Data which is related to this flight
#warning TODO Update the Turn Info in Turn Flight Data which is related to this flight.
								if (string.IsNullOrEmpty(stNewTurn))
								{
									bool hasRec = false;
									string stAdidTurn = "";
									stNewTurn = GetITrekFlightIdForUaft(cmd, stNewITurn, stFldt, out hasRec, out stAdidTurn);
								}
								if (!string.IsNullOrEmpty(stNewTurn))
								{
									if (!deFlightTurnInfo.ContainsKey(stUrno))
										deFlightTurnInfo.Add(stUrno, new FlightTurnInfo(stUrno, stUaft, stNewTurn, stNewITurn));
									if (!deFlightTurnInfo.ContainsKey(stNewTurn))
										deFlightTurnInfo.Add(stNewTurn, new FlightTurnInfo(stNewTurn, stNewITurn, stUrno, stUaft));
								}
							}
							else
							{//Has ITurn in Old Data, Has ITurn in New Data
								if (stOldITurn != stNewITurn)
								{//New and Old ITrun has different info.
									LogTraceMsg(string.Format("TurnChg:Id{0},uaft{1},Fr{2},To{3}", stUrno, stUaft, stOldITurn, stNewITurn));
									if (string.IsNullOrEmpty(stNewTurn))
									{
										bool hasRec = false;
										string stAdidTurn = "";
										stNewTurn = GetITrekFlightIdForUaft(cmd, stNewITurn, stFldt, out hasRec, out stAdidTurn);
									}
									if (string.IsNullOrEmpty(stOldTurn))
									{
										bool hasRec = false;
										string stAdidTurn = "";
										stOldTurn = GetITrekFlightIdForUaft(cmd, stOldITurn, stFldt, out hasRec, out stAdidTurn);
									}

									if (!string.IsNullOrEmpty(stOldTurn))
									{
										if (!deFlightTurnInfo.ContainsKey(stOldTurn))
											deFlightTurnInfo.Add(stOldTurn, new FlightTurnInfo(stOldTurn, stOldITurn, "", ""));
									}
									if (!string.IsNullOrEmpty(stNewTurn))
									{										
										if (!deFlightTurnInfo.ContainsKey(stOldTurn))
											deFlightTurnInfo.Add(stOldTurn, new FlightTurnInfo(stNewTurn, stNewITurn, stUrno, stUaft));
									}
									if (!deFlightTurnInfo.ContainsKey(stUrno))
										deFlightTurnInfo.Add(stUrno, new FlightTurnInfo(stUrno, stUaft, stNewTurn, stNewITurn));
								}
							}
						}
					}
				}
				UpdateOrgFlightInfoDataToDb(cmd, dtChgUpd, userId, updateDateTime);
			}
			#endregion
			DateTime dt7 = DateTime.Now;

			#region Update the Turn Info
			foreach (string key in deFlightTurnInfo.Keys)
			{
				FlightTurnInfo fInfo = deFlightTurnInfo[key];
				UpdateAFlightWithTurnInfo(cmd, 
					fInfo._flightId, fInfo._turnUaft, fInfo._turnFlightId, 
					updateDateTime, userId, false);
			}

			UpdateAllTurnInfo(cmd, updateDateTime, userId, false);
			#endregion
			DateTime dt8 = DateTime.Now;

			LogTraceMsg(string.Format(@"UploadOrgFlightDataToDb:CntXml={0},Existing={1},Insert={2}, Update={3}, Delete={4},
    PrepareInfo   :{5}, GetCur:{6}, PrepareData:{7},
    GetChangedInfo:{8}, Insert:{9}, Update     :{10},
    UpdTurnInfo   :{11},
    Total Time    :{12}",
			   dsOrgFlight.Tables[0].Rows.Count,
			   cntWithRecordInDb,
			   cntInsert, cntUpdate, cntDelete,
			   UtilTime.ElapsedTime(dt1, dt2),//PrepareInfo Time
			   UtilTime.ElapsedTime(dt2, dt3),//GetCurrent Info Time
			   UtilTime.ElapsedTime(dt3, dt4),//Prepare Data Time
			   UtilTime.ElapsedTime(dt4, dt5),//Get Changed Info (New/Changed)
			   UtilTime.ElapsedTime(dt5, dt6),//Insert New Info
			   UtilTime.ElapsedTime(dt6, dt7),//Update Changed Info
			   UtilTime.ElapsedTime(dt7, dt8),//Update Turn Info
			   UtilTime.ElapsedTime(dt1, dt8)//Total Time
			   ));
		}

		private class FlightTurnInfo
		{
			internal string _flightId = null;
			internal string _uaft = null;
			internal string _turnUaft = null;
			internal string _turnFlightId = null;

			public FlightTurnInfo(string flightId, string uaft, string turnFlightId, string turnUaft)
			{
				_flightId = flightId;
				_uaft = uaft;
				_turnUaft = turnUaft;
				_turnFlightId = turnFlightId;
			}
		}

		/// <summary>
		/// Update the TurnAround Information.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="updDateTime"></param>
		/// <param name="stUpdBy"></param>
		/// <param name="skipUnsuccess"></param>
		/// <returns></returns>
		private bool UpdateAllTurnInfo(IDbCommand cmd, DateTime updDateTime, string stUpdBy, bool skipUnsuccess)
		{
			bool success = true;
			UtilDb udb = UtilDb.GetInstance();
			IDbCommand cmdSel = null;
			Dictionary<string, string> deUpdList = new Dictionary<string, string>();
			
			try
			{
				cmdSel = udb.GetCommand(cmd.Connection, cmd.Transaction);
				cmdSel.Parameters.Clear();
				string stDbFn = DB_Info.GetDbFullName("ORCLDATETIME2UFISDATETIME");
				cmdSel.CommandText = string.Format("SELECT URNO,UAFT,TURN,ITURN,ADID, " +
					stDbFn + "(DECODE(ADID,'A',STOA,STOD)) V_FLDT,LSTU FROM {0} " +
				" WHERE ITURN IS NOT NULL AND TURN IS NULL ORDER BY LSTU DESC",
					GetDb_Tb_Flight());
				cmd.CommandType = CommandType.Text;
				//LogTraceMsg("UpdateAllTurnInfo:Cmd:" + cmdSel.CommandText);
				int cnt = 0;

				using (IDataReader dr = cmdSel.ExecuteReader())
				{
					while (dr.Read())
					{
						cnt++;
						LogTraceMsg("cnt:" + cnt);
						string stUrno = "";
						string stUaft = "";

						try
						{
							stUrno = UtilDb.GetData(dr, "URNO");
							if (string.IsNullOrEmpty(stUrno)) continue;
							if (deUpdList.ContainsKey(stUrno)) continue;

							stUaft = UtilDb.GetData(dr, "UAFT");
							string stITurn = UtilDb.GetData(dr, "ITURN");
							if (string.IsNullOrEmpty(stITurn)) continue;
							//string stAdid = UtilDb.GetData(dr, "ADID");
							string stFlDt = UtilDb.GetData(dr, "V_FLDT");

							bool hasRec = false;
							string stTurnAdid = "";
							string stTrunFlid = GetITrekFlightIdForUaft(cmd, stITurn,
								stFlDt, out hasRec, out stTurnAdid);
							LogTraceMsg("UpdateAllTurnInfo:" + stUrno + "," +
								stUaft + "," + stITurn + "," +
								stFlDt + "," + stTrunFlid + "," +
								hasRec);
							if (hasRec)
							{
								//Update the Turn Info for current flight.
								UpdateAFlightWithTurnInfo(cmd, stUrno, stITurn, stTrunFlid, updDateTime, stUpdBy, skipUnsuccess);

								//Update the Turn Info for TurnAround Flight related to current flight
								UpdateAFlightWithTurnInfo(cmd, stTrunFlid, stUaft, stUrno, updDateTime, stUpdBy, skipUnsuccess);

								deUpdList.Add(stUrno, stTrunFlid);
								deUpdList.Add(stTrunFlid, stUrno);
							}
						}
						catch (Exception ex)
						{
							LogMsg("UpdateAllTurnInfo:Err:" + stUaft + "," + stUrno + "," + ex.Message);
							throw;
						}
					}
				}
			}
			catch (Exception ex)
			{
				LogMsg("UpdateAllTurnInfo:Err:" + ex.Message );
				throw;
			}
			finally
			{
				//udb.CloseCommand(cmdSel, false, false);
				if (cmdSel != null)
				{
					cmdSel.Dispose();
					cmdSel = null;
				}
			}
			return success;
		}

		private bool UpdateAFlightWithTurnInfo(IDbCommand cmd, 
			string flightId, string turnUaft, string turnFlightId, DateTime dtUpdDateTime, string stUpdBy,
			bool skipUnsuccess)
		{
			bool success = true;
			UtilDb udb = UtilDb.GetInstance();
			cmd.Parameters.Clear();
			cmd.CommandText = string.Format("UPDATE {0} SET TURN={1},ITURN={2},LSTU={3},USEU={4} WHERE URNO={5}",
				GetDb_Tb_Flight(),
				udb.PrepareParam(cmd, "TurnFlid", turnFlightId),
				udb.PrepareParam(cmd, "ITurn", turnUaft),
				udb.PrepareParam(cmd, "LSTU", dtUpdDateTime),
				udb.PrepareParam(cmd, "UpdBy", stUpdBy),
				udb.PrepareParam(cmd, "Urno", flightId));

			int updCnt = cmd.ExecuteNonQuery();
			if (updCnt < 1)
			{
				success = false;
				if (!skipUnsuccess)
					throw new ApplicationException("Fail to update Turnaround info");
			}
			return success;
		}

		private string GetTimeString(DataRow row, string colName)
		{
			String st = "";
			object obj = row[colName];
			if ((obj != null) && (obj != DBNull.Value))
			{
				st = UtilTime.ConvDateTimeToUfisFullDTString((DateTime)obj);
			}
			return st;
		}

		/// <summary>
		/// Assign "New" Original Flight Data from backend to iTrek Flight Data
		/// </summary>
		/// <param name="rowOrgFlight">Original Flight Data row from Backend</param>
		/// <param name="flightId">flight id for new data</param>
		/// <param name="dsCurDb">iTrek Flight Data Table</param>
		/// <param name="userId">user id who update this info</param>
		/// <param name="updateDateTime">update date and time</param>
		private void AssignNewData(DataRow rowOrgFlight, string flightId, DataSet dsCurDb,
		   string userId, DateTime updateDateTime)
		{
			DataRow row = dsCurDb.Tables[0].NewRow();
			row["URNO"] = flightId;
			row["UAFT"] = rowOrgFlight["URNO"];
			row["FLNO"] = rowOrgFlight["FLNO"];
			row["ADID"] = rowOrgFlight["ADID"];
			row["REGN"] = rowOrgFlight["REGN"];
			row["TTYP"] = rowOrgFlight["TTYP"];
			row["ACT3"] = rowOrgFlight["ACT3"];
			row["ORG3"] = rowOrgFlight["ORG3"];
			row["DES3"] = rowOrgFlight["DES3"];
			row["BLT1"] = rowOrgFlight["BLT1"];

			//row["TURN"] = rowOrgFlight["TURN"];
			row["ITURN"] = rowOrgFlight["ITURN"];

			row["PSTA"] = rowOrgFlight["PSTA"];
			row["PSTD"] = rowOrgFlight["PSTD"];
			row["TGA1"] = rowOrgFlight["TGA1"];
			row["TGD1"] = rowOrgFlight["TGD1"];
			UtilDataSet.AssignDateTimeData((string)rowOrgFlight["STOA"], row, "STOA");
			UtilDataSet.AssignDateTimeData((string)rowOrgFlight["STOD"], row, "STOD");
			UtilDataSet.AssignDateTimeData((string)rowOrgFlight["ETAI"], row, "ETAI");
			UtilDataSet.AssignDateTimeData((string)rowOrgFlight["ETDI"], row, "ETDI");
			UtilDataSet.AssignDateTimeData((string)rowOrgFlight["ONBL"], row, "ONBL");
			UtilDataSet.AssignDateTimeData((string)rowOrgFlight["OFBL"], row, "OFBL");
			row["GTA1"] = rowOrgFlight["GTA1"];
			row["GTD1"] = rowOrgFlight["GTD1"];

			row["LSTU"] = updateDateTime;
			row["USEU"] = userId;
			row["CDAT"] = updateDateTime;
			row["USEC"] = userId;

			dsCurDb.Tables[0].LoadDataRow(row.ItemArray, LoadOption.Upsert);
		}

		/// <summary>
		/// Assign Existing Original Flight Data from backend to iTrek Flight Data
		/// </summary>
		/// <param name="rowOrgFlight">Original Flight Data row from Backend</param>
		/// <param name="flightId">flight id</param>
		/// <param name="row">iTrek Flight data table row</param>
		/// <param name="userId">user id who update this info</param>
		/// <param name="updateDateTime">Update date and time</param>
		private void AssignExistingData(DataRow rowOrgFlight, string flightId, DataRow row,
		   string userId, DateTime updateDateTime)
		{
			UtilDataSet.AssignData((string)rowOrgFlight["FLNO"], row, "FLNO");
			UtilDataSet.AssignData((string)rowOrgFlight["ADID"], row, "ADID");
			UtilDataSet.AssignData((string)rowOrgFlight["REGN"], row, "REGN");
			UtilDataSet.AssignData((string)rowOrgFlight["TTYP"], row, "TTYP");
			UtilDataSet.AssignData((string)rowOrgFlight["ACT3"], row, "ACT3");
			UtilDataSet.AssignData((string)rowOrgFlight["ORG3"], row, "ORG3");
			UtilDataSet.AssignData((string)rowOrgFlight["DES3"], row, "DES3");
			UtilDataSet.AssignData((string)rowOrgFlight["BLT1"], row, "BLT1");
			//UtilDataSet.AssignData((string)rowOrgFlight["TURN"], row, "TURN");
			UtilDataSet.AssignData((string)rowOrgFlight["ITURN"], row, "ITURN");
			UtilDataSet.AssignData((string)rowOrgFlight["PSTA"], row, "PSTA");
			UtilDataSet.AssignData((string)rowOrgFlight["PSTD"], row, "PSTD");
			UtilDataSet.AssignData((string)rowOrgFlight["TGA1"], row, "TGA1");
			UtilDataSet.AssignData((string)rowOrgFlight["TGD1"], row, "TGD1");
			UtilDataSet.AssignDateTimeData((string)rowOrgFlight["STOA"], row, "STOA");
			UtilDataSet.AssignDateTimeData((string)rowOrgFlight["STOD"], row, "STOD");
			UtilDataSet.AssignDateTimeData((string)rowOrgFlight["ETAI"], row, "ETAI");
			UtilDataSet.AssignDateTimeData((string)rowOrgFlight["ETDI"], row, "ETDI");
			UtilDataSet.AssignDateTimeData((string)rowOrgFlight["ONBL"], row, "ONBL");
			UtilDataSet.AssignDateTimeData((string)rowOrgFlight["OFBL"], row, "OFBL");
			UtilDataSet.AssignData((string)rowOrgFlight["GTA1"], row, "GTA1");
			UtilDataSet.AssignData((string)rowOrgFlight["GTD1"], row, "GTD1");
			if (row.RowState == DataRowState.Modified)
			{
				row["LSTU"] = updateDateTime;
				row["USEU"] = userId;
			}
		}

		#region OrgFlightDataInsert
		//private void OrgFlightDataInsert(IDbCommand cmd,
		//    DataRow row, List<string> cols, string insCmd, string userId, DateTime updateDateTime)
		// {
		//    int cnt = cols.Count;
		//    cmd.Parameters.Clear();
		//    for (int i = 0; i < cnt; i++)
		//    {
		//       cmd.Parameters.Add(row[cols[i]]);
		//    }

		//    cmd.CommandText = insCmd;
		//    cmd.CommandType = CommandType.Text;
		//    int updCnt = cmd.ExecuteNonQuery();
		//    if (updCnt < 1) throw new ApplicationException("Fail to Insert.");
		//}
		#endregion

		#region OrgFlightDataUpdate
		//private void OrgFlightDataUpdate(IDbCommand cmd,
		//    DataRow row, List<string> cols, string updCmd, string userId, DateTime updateDateTime)
		// {
		//    int cnt = cols.Count;
		//    cmd.Parameters.Clear();
		//    for (int i = 0; i < cnt; i++)
		//    {
		//       if ("CDAT,USEC".Contains(cols[i])) continue; 
		//       cmd.Parameters.Add(row[cols[i]]);
		//    }

		//    cmd.CommandText = updCmd;
		//    cmd.CommandType = CommandType.Text;
		//    int updCnt = cmd.ExecuteNonQuery();
		//    if (updCnt < 1) throw new ApplicationException("Fail to Insert.");
		//}
		#endregion

		/// <summary>
		/// Has record in iTrek Database for given Uaft with given Flight Schedule Date.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="uaft">Flight Id from Back End UFIS System</param>
		/// <param name="stFlightStDt">Flight Schedule (Arrival/Departure) Date and Time</param>
		/// <param name="flightId">Flight Id in iTrek Database</param>
		/// <returns>True - has record</returns>
		public bool HasITrekDbRecordForUaft(IDbCommand cmd, string uaft, string stFlightStDt, out string flightId)
		{
			bool has = false;
			flightId = "";
			DateTime flDt = DateTime.Now;
			if (stFlightStDt != null)
			{
				stFlightStDt = stFlightStDt.Trim();
				if (stFlightStDt != "")
				{
					flDt = UtilTime.ConvUfisTimeStringToDateTime(stFlightStDt);
				}
			}

			cmd.Parameters.Clear();
			string whereClause = " WHERE ";
			UtilDb udb = UtilDb.GetInstance();

			whereClause = string.Format(" WHERE UAFT={0} AND ((STOA BETWEEN {1} AND {2}) OR (STOD BETWEEN {1} AND {2}))",
			   udb.PrepareParam(cmd, "UAFT", uaft),
			   udb.PrepareParam(cmd, "FR_DT", flDt.AddDays(-14)),
			   udb.PrepareParam(cmd, "TO_DT", flDt.AddDays(+14)));

            cmd.CommandText = string.Format("SELECT COUNT(*) FROM {0} {1}", GetDb_Tb_Flight(), whereClause);
			cmd.CommandType = CommandType.Text;

			int cnt = Int32.Parse(cmd.ExecuteScalar().ToString());
			if (cnt == 1) has = true;
			else if (cnt > 1)
			{
				LogTraceMsg("HasITrekDbRecordForUaft:Dup:" + uaft + "," + stFlightStDt);
				throw new CEMultipleRecordsException();
			}

			if (has)
			{
                cmd.CommandText = string.Format("SELECT URNO FROM {0} {1}", GetDb_Tb_Flight(), whereClause);
				cmd.CommandType = CommandType.Text;

				try
				{
					flightId = cmd.ExecuteScalar().ToString();
					has = true;
				}
				catch (Exception)
				{
					throw;
				}
			}

			return has;
		}

		/// <summary>
		/// Get iTrek Flight Id for given UAFT (Flight Id from other system such as SOCC)
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="uaft"></param>
		/// <param name="flightDt"></param>
		/// <param name="hasRecord"></param>
		/// <returns></returns>
		public string GetITrekFlightIdForUaft(IDbCommand cmd, string uaft, string flightDt, out bool hasRecord, out string adid)
		{
			string flightId = "";
			UtilDb udb = UtilDb.GetInstance();

			string pnUaft = UtilDb.GetFormattedParaName("URNO");
			string whereClause = "WHERE URNO=" + pnUaft;
			hasRecord = false;
			adid = "";

			hasRecord = HasITrekDbRecordForUaft(cmd, uaft, flightDt, out flightId);
			if (hasRecord)
			{
				try
				{
                    cmd.CommandText = string.Format("SELECT ADID FROM {0} {1}", GetDb_Tb_Flight(), whereClause);
					cmd.CommandType = CommandType.Text;
					cmd.Parameters.Clear();
					udb.AddParam(cmd, pnUaft, flightId);

					adid = cmd.ExecuteScalar().ToString();
					hasRecord = true;
				}
				catch (Exception)
				{
					throw;
				}
			}
			else
			{
				flightId = "";
			}

			return flightId;
		}

		/// <summary>
		/// Get UAFT for given flight Id (itrek flight id)
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="flightId"></param>
		/// <returns></returns>
		public string GetUaftForITrekFlightId(IDbCommand cmd, string flightId)
		{
			string uaft = "";
			try
			{
                uaft = UtilDb.GetDataForAColumn(cmd, DB_Info.DB_T_FLIGHT, flightId, "UAFT").ToString();
			}
			catch (Exception ex)
			{
				LogMsg("GetUaftForITrekFlightId:Err:" + ex.Message);
				throw new ApplicationException("Unable to get the Flight Id.");
			}
			return uaft;
		}

		#region Lock and Release Semaphore
		private void LockFlightSummForWriting()
		{
			try
			{
				UtilLock.GetInstance().LockFlightSumm();
			}
			catch (Exception ex)
			{
				LogMsg("LockFlightSummForWriting:Err:" + ex.Message);
				if (ex.InnerException != null)
				{
					LogMsg("LockFlightSummForWriting:err:Inner:" + ex.InnerException.Message);
				}
			}
		}

		private void ReleaseFlightSummAfterWrite()
		{
			try
			{
				UtilLock.GetInstance().ReleaseFlightSumm();
			}
			catch (Exception ex)
			{
				LogMsg("ReleaseFlightSummAfterWrite:Err:" + ex.Message);
				if (ex.InnerException != null)
				{
					LogMsg("ReleaseFlightSummAfterWrite:err:Inner:" + ex.InnerException.Message);
				}
			}
		}

		private void LockArrFlightTimeForWriting()
		{
			try
			{
				UtilLock.GetInstance().LockArrTime();
			}
			catch (Exception ex)
			{
				LogMsg("LockArrFlightTimeForWriting:Err:" + ex.Message);
				if (ex.InnerException != null)
				{
					LogMsg("LockArrFlightTimeForWriting:err:Inner:" + ex.InnerException.Message);
				}
			}
		}

		private void ReleaseArrFlightTimeAfterWrite()
		{
			try
			{
				UtilLock.GetInstance().ReleaseArrTime();
			}
			catch (Exception ex)
			{
				LogMsg("ReleaseArrFlightTimeAfterWrite:Err:" + ex.Message);
				if (ex.InnerException != null)
				{
					LogMsg("ReleaseArrFlightTimeAfterWrite:err:Inner:" + ex.InnerException.Message);
				}
			}
		}

		private void LockDepFlightTimeForWriting()
		{
			try
			{
				UtilLock.GetInstance().LockDepTime();
			}
			catch (Exception ex)
			{
				LogMsg("LockDepFlightTimeForWriting:Err:" + ex.Message);
				if (ex.InnerException != null)
				{
					LogMsg("LockDepFlightTimeForWriting:err:Inner:" + ex.InnerException.Message);
				}
			}
		}

		private void ReleaseDepFlightTimeAfterWrite()
		{
			try
			{
				UtilLock.GetInstance().ReleaseDepTime();
			}
			catch (Exception ex)
			{
				LogMsg("ReleaseDepFlightTimeAfterWrite:Err:" + ex.Message);
				if (ex.InnerException != null)
				{
					LogMsg("ReleaseDepFlightTimeAfterWrite:err:Inner:" + ex.InnerException.Message);
				}
			}
		}
		#endregion

		/// <summary>
		/// Retrieve Departure Flight Timings for a flight
		/// </summary>
		/// <param name="urno">flight id</param>
		/// <returns></returns>
		public DSDepartureFlightTimings RetrieveDepartureFlightTimings(string urno)
		{
			DSDepartureFlightTimings ds = new DSDepartureFlightTimings();
			DSDepartureFlightTimings tempDs = dsDepartureFlightTiming;
			//Assign to local variable
			// to avoid calling "property get method" in foreach loop
			int flightCnt = 0;

			foreach (DSDepartureFlightTimings.DFLIGHTRow row in tempDs.DFLIGHT.Select(("URNO='" + urno + "'")))
			{
				flightCnt++;
				ds.DFLIGHT.LoadDataRow(row.ItemArray, true);
			}
			return ds;
		}

		/// <summary>
		/// Retrieve Arrival Flight Timings for a Flight
		/// </summary>
		/// <param name="urno">flight id</param>
		/// <returns></returns>
		public DSArrivalFlightTimings RetrieveArrivalFlightTimings(string urno)
		{
			DSArrivalFlightTimings ds = new DSArrivalFlightTimings();
			DSArrivalFlightTimings tempDs = (DSArrivalFlightTimings)dsArrivalFlightTiming;
			//Assign to local variable
			// to avoid calling "property get method" in foreach loop
			int flightCnt = 0;

			foreach (DSArrivalFlightTimings.AFLIGHTRow row in tempDs.AFLIGHT.Select(("URNO='" + urno + "'")))
			{
				flightCnt++;
				ds.AFLIGHT.LoadDataRow(row.ItemArray, true);
			}

			return ds;
		}

		private string GetFromItrekUpdateQueueName()
		{
			return ITrekPath.GetInstance().GetFROMITREKUPDATEQueueName();
		}

		/// <summary>
		/// Update Ao Id for the flight in giving hashtable 
		/// with flightId (URNO of ITK_FLIGHT) as Key 
		/// and AoId (StaffId - URNO of ITK_STAFF) as Value.
		/// </summary>
		/// <param name="conn">connection to use</param>
		/// <param name="htFlightAoId">HashTable of FlightId,Aoid</param>
		/// <param name="userId">user/process Id who update this info</param>
		/// <returns>true-Successfull update all</returns>
        public bool UpdateFlightAoIds(IDbConnection conn, Hashtable htFlightAoids, string userId, out int errCnt)
        {
            UtilDb udb = UtilDb.GetInstance();
            bool successAll = true;
            errCnt = 0;

            Hashtable htChgFlightAoids = GetChgFlightAoids(conn, htFlightAoids, out errCnt);

            IDbCommand cmd = null;

            try
            {
                cmd = udb.GetCommand(conn, null);
                cmd.Parameters.Clear();
                string pLstu = udb.PrepareParamForTime(cmd, "LSTU", Misc.ITrekTime.GetCurrentDateTime());
                string pUseu = udb.PrepareParam(cmd, "USEU", userId);
                bool firstTime = true;

                foreach (String flightId in htChgFlightAoids.Keys)
                {
                    cmd.Transaction = conn.BeginTransaction();
                    string pAoid = udb.PrepareParam(cmd, "AIOD", GetValue(htChgFlightAoids, flightId));
                    string pUrno = udb.PrepareParam(cmd, "URNO", flightId);

                    if (firstTime)
                    {
                        cmd.CommandText = string.Format("UPDATE {0} SET AOID={1},LSTU={2},USEU={3} WHERE URNO={4}",
                            GetDb_Tb_Flight(),
                            pAoid, pLstu, pUseu,
                            pUrno);
                        LogTraceMsg("UpdateFlightAoIds cmd.CommandText" + cmd.CommandText);
                        cmd.CommandType = CommandType.Text;
                        firstTime = false;
                    }

                    try
                    {
                        int updCnt = cmd.ExecuteNonQuery();
                        if (updCnt < 1)
                        {//Not able to update
                            successAll = false;
                            cmd.Transaction.Rollback();
                        }
                        else
                        {
                            cmd.Transaction.Commit();
                        }
                    }
                    catch (Exception)
                    {
                        successAll = false;
                        cmd.Transaction.Rollback();
                        LogMsg("UpdateFlightAoIds:Err:UpdFlight:" + flightId);
                    }
                }

            }
            catch (Exception ex)
            {
                successAll = false;
                LogMsg("UpdateFlightAoIds:Err:" + ex.Message);
            }
            finally
            {
                udb.CloseCommand(cmd, true, false);
            }

            if (errCnt > 0) successAll = false;

            return successAll;
        }

		/// <summary>
		/// Get the New Aoid for the flight which are different from current AoId in database.
		/// </summary>
		/// <param name="conn"></param>
		/// <param name="htFlightAoids"></param>
		/// <returns></returns>
		public Hashtable GetChgFlightAoids(IDbConnection conn, Hashtable htFlightAoids, out int errCnt)
		{
			Hashtable htChgFlightIds = new Hashtable();

			UtilDb udb = UtilDb.GetInstance();
			IDbCommand cmd = null;
			StringBuilder sbErrMsg = new StringBuilder();
			errCnt = 0;

			StringBuilder sb = new StringBuilder();
			if (htFlightAoids.Keys.Count > 0)
			{
				#region Prepare Flight Ids for selection in database
				bool firstTime = true;
				foreach (string flightId in htFlightAoids.Keys)
				{
					if (firstTime)
					{
						sb.Append("'" + flightId + "'");
						firstTime = false;
					}
					else
					{
						sb.Append(",'" + flightId + "'");
					}
				}
				#endregion

				#region Select and add into the hashtable
				try
				{
					cmd = udb.GetCommand(conn, null);
					cmd.CommandText = string.Format("SELECT URNO,AOID FROM {0} WHERE URNO IN ({1})",
                            GetDb_Tb_Flight(), sb.ToString());
					cmd.CommandType = CommandType.Text;
					using (IDataReader dr = cmd.ExecuteReader())
					{
						string flightId = "";
						string curAoId = "";
						while (dr.Read())
						{
							try
							{
								flightId = UtilDb.GetData(dr, "URNO");
								curAoId = UtilDb.GetData(dr, "AOID");
								if (htFlightAoids.ContainsKey(flightId))
								{
									string newAoId = GetValue(htFlightAoids, flightId);
									if (!IsSameAoId(newAoId, curAoId))
									{
										htChgFlightIds.Add(flightId, newAoId);
										htFlightAoids.Remove(flightId);
									}
								}
								else
								{
									errCnt++;
									sbErrMsg.AppendLine("Could not find Flight Id:" + flightId);
								}
							}
							catch (Exception ex)
							{
								errCnt++;
								sbErrMsg.AppendLine("RetrieveFlightAoids:Err:Ao:" + UtilDb.GetData(dr, "URNO") + "," + ex.Message);
							}
						}
					}
				}
				catch (Exception ex)
				{
					LogMsg("RetrieveFlightAoids:Err:" + ex.Message);
					throw;
				}
				finally
				{
					udb.CloseCommand(cmd, false, false);
				}
				#endregion
			}
			return htChgFlightIds;
		}

		private static bool IsSameAoId(string aoId1, string aoId2)
		{
			bool same = false;
			if ((string.IsNullOrEmpty(aoId1)) && (string.IsNullOrEmpty(aoId2))) same = true;
			else if (aoId1 == aoId2) same = true;
			return same;
		}

		private static string GetValue(Hashtable ht, string key)
		{
			string st = "";
			object obj = ht[key];
			if (obj != null)
			{
				st = (string)obj;
			}
			return st;
		}

		public static DsDbFlight RetrieveDsDbFlightFromDb(IDbCommand cmd, List<string> flightIds)
		{
			cmd.Parameters.Clear();
			UtilDb udb = UtilDb.GetInstance();
			int totCnt = flightIds.Count;
			const int MAX_CNT = 500;
			int noOfIter = (totCnt / MAX_CNT) + 1;
			DsDbFlight dsAll = new DsDbFlight();
			int frIdx = 0;
			int toIdx = totCnt;
			for (int i = 0; i < noOfIter; i++)
			{
				if (toIdx < frIdx) break;
				if ((toIdx - frIdx) > MAX_CNT)
				{
					toIdx = frIdx + MAX_CNT;
				}
				cmd.CommandText = string.Format("SELECT * FROM {0} WHERE URNO IN ({1})",
					GetDb_Tb_Flight(),
					UtilMisc.ConvListToString(flightIds, ",", "'", "'", frIdx, toIdx));
				cmd.CommandType = CommandType.Text;

				if (i == 0)
				{
					int cnt = udb.FillDataSet(cmd, dsAll);
				}
				else
				{
					DsDbFlight ds = new DsDbFlight();
					int cnt = udb.FillDataSet(cmd, ds);
					foreach (DataRow row in ds.Tables[0].Rows)
					{
						dsAll.Tables[0].LoadDataRow(row.ItemArray, true);
					}
				}
				frIdx = toIdx;
				toIdx = totCnt;
			}
			return dsAll;
		}

		public void SaveDsDbFlight(IDbCommand cmd, DsDbFlight dsToSave, string userId, DateTime updDateTime)
		{
			List<string> flightIds = new List<string>();
			foreach (DsDbFlight.ITK_FLIGHTRow row in dsToSave.ITK_FLIGHT.Rows)
			{
				flightIds.Add(row.URNO);
			}

			DsDbFlight dsCur = RetrieveDsDbFlightFromDb(cmd, flightIds);
			dsCur.AcceptChanges();

			List<string> lsCol = UtilDataSet.GetNonExpressionColumnList(dsCur.ITK_FLIGHT);
			lsCol.Remove("URNO");
			lsCol.Remove("UAFT");

			foreach (DsDbFlight.ITK_FLIGHTRow row in dsToSave.ITK_FLIGHT.Rows)
			{
				DsDbFlight.ITK_FLIGHTRow[] curRows = (DsDbFlight.ITK_FLIGHTRow[])
					dsCur.ITK_FLIGHT.Select("URNO='" + row.URNO + "'");

				DsDbFlight.ITK_FLIGHTRow chgRow;
				if ((curRows == null) || (curRows.Length < 1))
				{//New record
					chgRow = dsCur.ITK_FLIGHT.NewITK_FLIGHTRow();
					chgRow.URNO = row.URNO;
					chgRow.UAFT = row.UAFT;
					dsCur.ITK_FLIGHT.AddITK_FLIGHTRow(chgRow);
				}
				else
				{
					chgRow = curRows[0];
					if (chgRow.UAFT != row.UAFT)
					{
						throw new ApplicationException(
							string.Format("Flight Id {0} has Different Uaft Cur:{1}, New:{2}",
						 row.URNO, chgRow.UAFT, row.UAFT));
					}
				}

				UtilDataSet.AssignData(row, chgRow, lsCol);
				if ((chgRow.RowState == DataRowState.Added) || (chgRow.RowState == DataRowState.Modified))
				{
					ComputeTimingForBM(chgRow);
				}

			}

			DataTable dtIns = dsCur.ITK_FLIGHT.GetChanges(DataRowState.Added);
			DataTable dtUpd = dsCur.ITK_FLIGHT.GetChanges(DataRowState.Modified);

			if ((dtIns != null) && (dtIns.Rows.Count > 0))
			{
				UtilDb.InsertDataToDb(cmd, dtIns, DB_Info.DB_T_FLIGHT, ProcIds.FLIGHT, userId, updDateTime);
			}

			if ((dtUpd != null) && (dtUpd.Rows.Count > 0))
			{
				int totRowCntToUpd = 0;
				int successRowCnt = UtilDb.UpdateDataToDb(cmd, dtUpd, DB_Info.DB_T_FLIGHT, userId, updDateTime, false, out totRowCntToUpd);
			}
		}

		/// <summary>
		/// Compute the Benchmark Timing for given data. Return true if successfully computed.
		/// </summary>
		/// <param name="row"></param>
		/// <returns></returns>
		public static bool ComputeTimingForBM_org(DsDbFlight.ITK_FLIGHTRow row)
		{
			bool computed = false;
			try
			{
				//NOTE: If any changes of PASS/FAIL computation formula, need to change the "BMPassFail.aspx" accordingly
				DateTime fBagTime = UtilDb.DEFAULT_DT;
				DateTime lBagTime = UtilDb.DEFAULT_DT;
				DateTime tbsUpTime = UtilDb.DEFAULT_DT;
				if (!row.IsTBS_UPNull()) { tbsUpTime = row.TBS_UP; }
				if (!row.IsFBAGNull()) { fBagTime = row.FBAG; }
				if (!row.IsLBAGNull()) { lBagTime = row.LBAG; }

				DateTime bgRefTime = tbsUpTime;
				DateTime bgFstBagTime = fBagTime;
				DateTime bgLstBagTime = lBagTime;

				DateTime apRefTime = tbsUpTime;
				DateTime apFstBagTime = fBagTime;
				DateTime apLstBagTime = lBagTime;

				row.AP_FST_BM_TIMING = GetTiming(apRefTime, apFstBagTime);
				row.AP_LST_BM_TIMING = GetTiming(apRefTime, apLstBagTime);
				row.BG_FST_BM_TIMING = GetTiming(bgRefTime, bgFstBagTime);
				row.BG_LST_BM_TIMING = GetTiming(bgRefTime, bgLstBagTime);
				computed = true;
			}
			catch (Exception ex)
			{
				LogMsg("ComputeTimingForBM:Err:" + ex.Message);
			}
			return computed;
		}

		public static bool ComputeTimingForBM(DsDbFlight.ITK_FLIGHTRow row)
		{
			bool computed = false;
			try
			{
				//NOTE: If any changes of PASS/FAIL computation formula, need to change the "BMPassFail.aspx" accordingly
				DateTime? fBagTime = UtilDataSet.GetDateTimeNullable(row, "FBAG");
				DateTime? lBagTime = UtilDataSet.GetDateTimeNullable(row, "LBAG");
				DateTime? tbsUpTime = UtilDataSet.GetDateTimeNullable(row, "TBS_UP");

				DateTime? bgRefTime = tbsUpTime;
				DateTime? bgFstBagTime = fBagTime;
				DateTime? bgLstBagTime = lBagTime;

				DateTime? apRefTime = tbsUpTime;
				DateTime? apFstBagTime = fBagTime;
				DateTime? apLstBagTime = lBagTime;

				UtilDataSet.AssignData(GetTiming(apRefTime, apFstBagTime), row, "AP_FST_BM_TIMING");
				UtilDataSet.AssignData(GetTiming(apRefTime, apLstBagTime), row, "AP_LST_BM_TIMING");
				UtilDataSet.AssignData(GetTiming(apRefTime, bgFstBagTime), row, "BG_FST_BM_TIMING");
				UtilDataSet.AssignData(GetTiming(apRefTime, bgLstBagTime), row, "BG_LST_BM_TIMING");

				//row.AP_FST_BM_TIMING = GetTiming(apRefTime, apFstBagTime);
				//row.AP_LST_BM_TIMING = GetTiming(apRefTime, apLstBagTime);
				//row.BG_FST_BM_TIMING = GetTiming(bgRefTime, bgFstBagTime);
				//row.BG_LST_BM_TIMING = GetTiming(bgRefTime, bgLstBagTime);
				computed = true;
			}
			catch (Exception ex)
			{
				LogMsg("ComputeTimingForBM:Err:" + ex.Message);
			}
			return computed;
		}

		#region MetTiming
		//private static string MetTiming(int bmTiming, DateTime refUfisTime, DateTime ufisTimeToMeasure, out int timeDiff)
		//{
		//string timingMet = "";
		//int timing = GetTiming(refUfisTime, ufisTimeToMeasure);
		//if (timing != "" && (bmTiming.Trim() != ""))
		//{
		//    if (bmTiming >= timing) timingMet = "P";//Meet the timing
		//    else timingMet = "F";
		//}
		//timeDiff = timing;
		//return timingMet;
		//}
		#endregion

		/// <summary>
		/// Get Timing in Minutes (toTime - frTime).
		/// </summary>
		/// <param name="frTime"></param>
		/// <param name="toTime"></param>
		/// <returns></returns>
		private static string GetTiming(DateTime? frTime, DateTime? toTime)
		{
			string timing = null;
			if ((!UtilDb.IsNullDateTime(frTime)) && (!UtilDb.IsNullDateTime(toTime)))
			{
				TimeSpan ts = toTime.Value.Subtract(frTime.Value);
				if (ts.TotalMinutes > Int32.MaxValue)
				{
					timing = Convert.ToString(Int32.MaxValue);
				}
				else if (ts.TotalMinutes < Int32.MinValue)
				{
					timing = Convert.ToString(Int32.MinValue);
				}
				else
				{
					timing = Convert.ToString(ts.TotalMinutes).Replace(",", "");
					int idx = timing.LastIndexOf(".");
					if (idx == 0)
					{
						timing = "0";
					}
					else if (idx > 0)
					{
						timing = timing.Substring(0, idx);
					}
				}
			}
			return timing;
		}

		/// <summary>
		/// Retrieve Flight Ids for given List of Uafts. 
		/// Return Dictionary Map with uaft as Key and Flight as value.
		/// </summary>
		/// <param name="cmd">Command to connect to database</param>
		/// <param name="lsUaft">List of Uaft</param>
		/// <returns></returns>
		public Dictionary<String, String> RetrieveFlidsForUafts(IDbCommand cmd, List<string> lsUaft)
		{
			Dictionary<string, string> mapUaftToFlid = new Dictionary<string, string>();
			DSFlight ds = dsFlight;
			foreach (string uaft in lsUaft)
			{
				if (mapUaftToFlid.ContainsKey(uaft)) continue;
				DSFlight.FLIGHTRow[] rows = (DSFlight.FLIGHTRow[])ds.FLIGHT.Select("UAFT='" + uaft + "'");
				if ((rows == null) || (rows.Length < 1))
				{
					string flightId = "";
					if (HasITrekDbRecordForUaft(cmd, uaft, "", out flightId))
					{
						mapUaftToFlid.Add(uaft, flightId);
					}
					else mapUaftToFlid.Add(uaft, "");
				}
				else
				{
					mapUaftToFlid.Add(uaft, rows[0].URNO);
				}
			}
			return mapUaftToFlid;
		}

		public DsDbFlight ConvDsFlightToDsDbFlight(IDbCommand cmd, DSFlight dsOld)
		{
			DsDbFlight dsNew = new DsDbFlight();
			//int newFlCnt = 0;
			LogTraceMsg("ConvDsFlightToDsDbFlight:Start.");
			foreach (DSFlight.FLIGHTRow rowOld in dsOld.FLIGHT.Rows)
			{
				DsDbFlight.ITK_FLIGHTRow rowNew = dsNew.ITK_FLIGHT.NewITK_FLIGHTRow();
				string stUaft = rowOld.URNO;
				string stFlid = stUaft;
				DSFlight dsTemp = new DSFlight();
				bool hasRecord = false;
				string adid = "";
				stFlid = GetITrekFlightIdForUaft(cmd, stUaft, rowOld.ST, out hasRecord, out adid);
				if (!hasRecord)
				{
					stFlid = stUaft;
					//newFlCnt++;
					//stFlid = UtilDb.PFIX_VIRTUAL_NEW_ID + newFlCnt;
				}

				//---
				rowNew.URNO = stFlid;
				rowNew.FLNO = rowOld.FLNO;
				rowNew.ADID = rowOld.A_D;
				rowNew.REGN = rowOld.REGN;
				rowNew.ACT3 = rowOld.AIRCRAFT_TYPE;
				rowNew.ORG3 = rowOld.FR;
				rowNew.DES3 = rowOld.TO;
				rowNew.BLT1 = rowOld.BELT;

				//rowNew.TTYP=rowOld.;
				//rowNew.RKEY=rowOld.;
				rowNew.UAFT = stUaft;
				rowNew.TURN = rowOld.TURN;
				rowNew.L_H = rowOld.L_H;
				rowNew.AOID = rowOld.AOID;
				rowNew.BOID = rowOld.BOID;

				AssignData(rowOld, "AP_FST_BM", rowNew, "AP_FST_BM", false);
				AssignData(rowOld, "AP_LST_BM", rowNew, "AP_LST_BM", false);
				AssignData(rowOld, "BG_FST_BM", rowNew, "BG_FST_BM", false);
				AssignData(rowOld, "BG_LST_BM", rowNew, "BG_LST_BM", false);

				rowNew.AP_FST_BM_TIMING = rowOld.AP_FST_BAG_TIMING;
				rowNew.AP_LST_BM_TIMING = rowOld.AP_LST_BAG_TIMING;
				rowNew.BG_FST_BM_TIMING = rowOld.BG_FST_BAG_TIMING;
				rowNew.BG_LST_BM_TIMING = rowOld.BG_LST_BAG_TIMING;

				UtilDataSet.AssignDateTimeData(rowOld.MAB, rowNew, "MAB");
				UtilDataSet.AssignDateTimeData(rowOld.TBS_UP, rowNew, "TBS_UP");
				UtilDataSet.AssignDateTimeData(rowOld.LD_ST, rowNew, "LD_ST");
				UtilDataSet.AssignDateTimeData(rowOld.LD_END, rowNew, "LD_END");
				UtilDataSet.AssignDateTimeData(rowOld.LST_DOOR, rowNew, "LST_DOOR");
				UtilDataSet.AssignDateTimeData(rowOld.PLB, rowNew, "PLB");
				UtilDataSet.AssignDateTimeData(rowOld.UNL_STRT, rowNew, "UNL_STRT");
				UtilDataSet.AssignDateTimeData(rowOld.UNL_END, rowNew, "UNL_END");

				UtilDataSet.AssignDateTimeData(rowOld.FST_CRGO, rowNew, "FST_CRGO");
				UtilDataSet.AssignDateTimeData(rowOld.FST_BAG, rowNew, "FBAG");
				UtilDataSet.AssignDateTimeData(rowOld.LST_BAG, rowNew, "LBAG");

				UtilDataSet.AssignDateTimeData(rowOld.AP_FST_TRP, rowNew, "FTRIP");
				UtilDataSet.AssignDateTimeData(rowOld.BG_FST_TRP, rowNew, "FTRIPB");
				UtilDataSet.AssignDateTimeData(rowOld.AP_LST_TRP, rowNew, "LTRIP");
				UtilDataSet.AssignDateTimeData(rowOld.BG_LST_TRP, rowNew, "LTRIPB");

				//---
				if (rowOld.A_D == "A")
				{
					UtilDataSet.AssignData(rowOld.PARK_STAND, rowNew, "PSTA");
					UtilDataSet.AssignData(rowOld.TERMINAL, rowNew, "TGA1");
					UtilDataSet.AssignDateTimeData(rowOld.ST, rowNew, "STOA");
					UtilDataSet.AssignDateTimeData(rowOld.ET, rowNew, "ETAI");
					UtilDataSet.AssignDateTimeData(rowOld.OBL, rowNew, "ONBL");
					UtilDataSet.AssignData(rowOld.GATE, rowNew, "GTA1");
					UtilDataSet.AssignDateTimeData(rowOld.AT, rowNew, "ATA");
				}
				else
				{
					UtilDataSet.AssignData(rowOld.PARK_STAND, rowNew, "PSTD");
					UtilDataSet.AssignData(rowOld.TERMINAL, rowNew, "TGD1");
					UtilDataSet.AssignDateTimeData(rowOld.ST, rowNew, "STOD");
					UtilDataSet.AssignDateTimeData(rowOld.ET, rowNew, "ETDI");
					UtilDataSet.AssignDateTimeData(rowOld.OBL, rowNew, "OFBL");
					UtilDataSet.AssignData(rowOld.GATE, rowNew, "GTD1");
					UtilDataSet.AssignDateTimeData(rowOld.AT, rowNew, "ATD");
				}
				dsNew.ITK_FLIGHT.AddITK_FLIGHTRow(rowNew);
			}
			LogTraceMsg("ConvDsFlightToDsDbFlight:Completed.");
			dsNew.AcceptChanges();
			return dsNew;
		}

		private static void AssignData(DataRow frRow, string frCol, DataRow toRow, string toCol, bool isTime)
		{
			try
			{
				if (isTime)
				{
					UtilDataSet.AssignDateTimeData(frRow[frCol].ToString(), toRow, toCol);
				}
				else
				{
					toRow[toCol] = frRow[frCol].ToString();
				}
			}
			catch (Exception ex)
			{
				LogMsg("AssignData:Err:" + ex.Message);
			}
		}
	}
}
