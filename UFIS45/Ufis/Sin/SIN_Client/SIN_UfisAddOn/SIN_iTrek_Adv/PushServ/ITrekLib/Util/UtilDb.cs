using System;
using System.Collections.Generic;
using System.Text;

using System.Data;
using System.Data.Common;

using UFIS.ITrekLib.Config;
using MM.UtilLib;
using UFIS.ITrekLib.CustomException;
using UFIS.ITrekLib.Config;

namespace UFIS.ITrekLib.Util
{
	public class UtilDb
	{
		#region Singleton
		private static UtilDb _this = null;
		private static object _lockObj = new object();

		private static IUtilDb _theUtilDb = null;
		private static string _paramPrefix = null;
		private static readonly DateTime NULL_DATE_TIME = new DateTime(1, 1, 1);

		private UtilDb()
		{
		}

		public static UtilDb GetInstance()
		{
			if (_this == null)
			{
				lock (_lockObj)
				{
					if (_this == null)
					{
						_this = new UtilDb();
					}
				}
			}
			return _this;
		}
		#endregion

		/// <summary>
		/// Clear all the connections from pool
		/// </summary>
		public static void ClearAllPools()
		{
			string errMsg = "";
			try
			{
				string db = ITrekConfig.GetDbType().Trim().ToUpper();
				//string dbVersion = ITrekConfig.GetDbVersion();
				switch (db)
				{
					case DB_ORACLE:
						UtilOraDb.ClearAllPools();
						LogTraceMsg("ClearAllPool:Ora.");
						break;
					default:
						errMsg = "UnSupported Database";
						LogTraceMsg("ClearAllPool:" + errMsg);
						//throw new ApplicationException("UnSupported Database");
						break;
				}
			}
			catch (Exception)
			{
			}
		}

		/// <summary>
		/// Clear the given connection from pool
		/// </summary>
		/// <param name="conn"></param>
		public static void ClearPool(IDbConnection conn)
		{
			string errMsg = "";
			try
			{
				string db = ITrekConfig.GetDbType().Trim().ToUpper();
				//string dbVersion = ITrekConfig.GetDbVersion();
				switch (db)
				{
					case DB_ORACLE:
						UtilOraDb.ClearPool(conn);
						LogTraceMsg("ClearPool:Ora.");
						break;
					default:						
						errMsg = "UnSupported Database";
						LogTraceMsg("ClearPool:" + errMsg);
						break;
				}
			}
			catch (Exception)
			{
			}

			//if (!string.IsNullOrEmpty(errMsg)) throw new ApplicationException(errMsg);
		}

		private IUtilDb CurrentUtilDb
		{
			get
			{
				if (_theUtilDb == null)
				{
					lock (_lockObj)
					{
						if (_theUtilDb == null)
						{
							string db = ITrekConfig.GetDbType().Trim().ToUpper();
							string dbVersion = ITrekConfig.GetDbVersion();
							switch (db)
							{
								case DB_ORACLE:
									_theUtilDb = UtilOraDb.GetInstance();
									break;
								default:
									throw new ApplicationException("UnSupported Database");
							}
						}
					}
				}
				return _theUtilDb;
			}
		}

		#region const
		private const string DB_ORACLE = "ORACLE";
		#endregion

		#region DB Helper Methods : public
		public string GetConnString()
		{
			return ITrekConfig.GetDbConnString();
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Get Prefix for Parameter Name to use in Command.Parameters.Add Method.
		/// </summary>
		/// <returns></returns>
		public static string GetParaNamePrefix()
		{
			string prefix = ":";

			if (_paramPrefix == null)
			{
				string db = ITrekConfig.GetDbType();
				string dbVersion = ITrekConfig.GetDbVersion();
				switch (db)
				{
					case DB_ORACLE:
						prefix = UtilOraDb.PARA_NAME_PREFIX;
						break;
					default:
						throw new ApplicationException("UnSupported Database");
				}
				_paramPrefix = prefix;
			}
			else
			{
				prefix = _paramPrefix;
			}

			return prefix;
		}

		/// <summary>
		/// Get the Formatted Parameter Name for give original parameter name.
		/// (e.g orgParaName ==>MSGT, Formatted Parameter Name ==>:MSGT
		/// </summary>
		/// <param name="orgParaName"></param>
		/// <returns></returns>
		public static string GetFormattedParaName(string orgParaName)
		{
			return GetParaNamePrefix() + orgParaName;
		}

		/// <summary>
		/// Get Connection
		/// </summary>
		/// <param name="openConnection">True-Open the Connection. Otherwise-Not to open the connection (calling method will take care to open.</param>
		/// <returns>Connection</returns>
		public IDbConnection GetConnection(bool openConnection)
		{
			String connString = ITrekConfig.GetDbConnString();
			return CurrentUtilDb.GetConnection(connString, openConnection);

		}

		/// <summary>
		/// Get Command for Database.
		/// Use CloseCommand to Dispoe the command. It will take care of commit/rollback of transaction and Disposing of connection as well.
		/// </summary>
		/// <param name="withTrx">True-To get the command with Begin Transaction. Otherwise-No Transaction</param>
		/// <returns>Command</returns>
		public IDbCommand GetCommand(bool withTrx)
		{
			IDbCommand cmd = null;
			String connString = ITrekConfig.GetDbConnString();
			if (withTrx)
			{
				cmd = CurrentUtilDb.GetCommandWithTrx(connString);
			}
			else
			{
				cmd = CurrentUtilDb.GetCommand(connString);
			}
			return cmd;
		}

		/// <summary>
		/// Get Command for Database for given connection and transaction.
		/// Take care of Closing and Disposing the Command, connection and commit/rollback of transaction.
		/// </summary>
		/// <param name="conn"></param>
		/// <param name="trx"></param>
		/// <returns></returns>
		public IDbCommand GetCommand(IDbConnection conn, IDbTransaction trx)
		{
			return CurrentUtilDb.GetCommand(conn, trx);
		}

		public IDbCommand GetCommandWithTrx(IDbConnection conn)
		{
			return CurrentUtilDb.GetCommandWithTrx(conn);
		}

		/// <summary>
		/// Close the command. Commit/Rollback the transaction (if any). Dispose the connection.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="commit">True-To Commit any transaction. Otherwise - Rollback</param>
		public void CloseCommand(IDbCommand cmd, bool commit)
		{
			CurrentUtilDb.CloseCommand(cmd, commit);
		}

		/// <summary>
		/// Close the connection
		/// </summary>
		/// <param name="conn">Connection to be closed</param>
		public static void CloseConnection(IDbConnection conn)
		{
			if (conn != null)
			{
				try
				{
					try
					{
						if (conn.State != ConnectionState.Closed)
						{
							conn.Close();
						}
					}
					catch (Exception)
					{
						//throw;
					}
					finally
					{
						try
						{
							conn.Dispose();
						}
						catch (Exception)
						{
						}
						conn = null;
					}
				}
				catch (Exception ex)
				{
					LogMsg("CloseConnection:Err:" + ex.Message);
				}
				finally
				{
					conn = null;
				}
			}
		}

		/// <summary>
		/// Close Command. Commit if any transaction when commit==True.
		/// Close the connection when closeConnectionInd==True
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="commit"></param>
		/// <param name="closeConnectionInd"></param>
		public void CloseCommand(IDbCommand cmd, bool commit, bool closeConnection)
		{
			CurrentUtilDb.CloseCommand(cmd, commit, closeConnection);
		}

		/// <summary>
		/// Close command without closing the connection.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="commit"></param>
		public void CommitTransaction(IDbCommand cmd, bool commit)
		{
			//string db = ITrekConfig.GetDbType();
			//string dbVersion = ITrekConfig.GetDbVersion();
			//switch (db)
			//{
			//   case DB_ORACLE:
			//      CloseOracleCommand(cmd, commit);
			//      break;
			//   default:
			//      CloseUnkCommand(cmd, commit);
			//      throw new ApplicationException("UnSupported Database");
			//}
			CurrentUtilDb.CommitTransaction(cmd, commit);
		}

		/// <summary>
		/// Get Data Adapter to use to fill the Dataset. Prepare the command before calling this method.
		/// </summary>
		/// <param name="cmd">Select Command to fill the dataset</param>
		/// <returns></returns>
		public IDataAdapter GetDataAdapter(IDbCommand cmd)
		{
			return CurrentUtilDb.GetDataAdapter(cmd);
		}

		public IDbDataAdapter GetDbDataAdapter()
		{
			return CurrentUtilDb.GetDbDataAdapter();
		}

		public void CreateCommandBuilder(IDbDataAdapter da)
		{
			CurrentUtilDb.CreateCommandBuilder(da);
		}

		public void Update(IDbDataAdapter da, DataTable dt)
		{
			CurrentUtilDb.Update(da, dt);
		}

		/// <summary>
		/// Get Data Adapter to use to fill the Dataset. Prepare the command before calling this method.
		/// </summary>
		/// <param name="cmd">Select Command to fill the dataset</param>
		/// <returns></returns>
		public IDbDataAdapter GetDbDataAdapter(IDbCommand cmd)
		{
			return CurrentUtilDb.GetDbDataAdapter(cmd);
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="cmd"></param>
		/// <returns></returns>	  
		public DbCommandBuilder GetDbCommandBuilder(IDbDataAdapter da)
		{
			return CurrentUtilDb.GetDbCommandBuilder(da);
		}

		/// <summary>
		/// Fill Data Table using the given command. Note: Select command text and parameters must already be assigned before passing to this method.
		/// </summary>
		/// <param name="cmd">command with command text and parameters</param>
		/// <param name="dt"></param>
		/// <returns></returns>
		public int FillDataTable(IDbCommand cmd, DataTable dt)
		{
			if (cmd == null) throw new ApplicationException("Null Command.");
			try
			{
				return CurrentUtilDb.FillDataTable(cmd, dt);
			}
			catch (Exception)
			{
				ClearPool(cmd.Connection);
				throw;
			}
		}

		/// <summary>
		/// Fill DataSet using the given command. Note: command text and parameters must already be assigned before passing to this method.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="ds"></param>
		/// <returns></returns>
		public int FillDataSet(IDbCommand cmd, DataSet ds)
		{
			if (cmd == null) throw new ApplicationException("Null Command.");
			try
			{
				return CurrentUtilDb.FillDataSet(cmd, ds);
			}
			catch (Exception)
			{
				ClearPool(cmd.Connection);
				throw;
			}
		}

		public IDbCommand CloneCommand(IDbCommand srcCmd, IDbConnection conn, IDbTransaction trx)
		{
			if (srcCmd == null) throw new ApplicationException("Null Command to Clone.");
			IDbCommand cmd = CurrentUtilDb.GetCommand(conn, trx);

			cmd.CommandText = srcCmd.CommandText;
			cmd.CommandType = srcCmd.CommandType;
			cmd.Parameters.Clear();

			foreach (IDataParameter para in srcCmd.Parameters)
			{
				CurrentUtilDb.AddParameter(cmd, para);
			}
			cmd.UpdatedRowSource = srcCmd.UpdatedRowSource;
			return cmd;
		}

		/// <summary>
		/// Add the parameter with given name and value to the command
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="paramName"></param>
		/// <param name="paramValue"></param>
		/// <returns></returns>
		public IDbCommand AddParam(IDbCommand cmd, string paramName, object paramValue)
		{
			return CurrentUtilDb.AddParam(cmd, paramName, paramValue);
		}

		/// <summary>
		/// Prepare Update command by adding parameter to the command returing the "set" assing for the paraname and value.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="dbFieldName">Database Table Field Name (e.g. "PSTU")</param>
		/// <param name="paramValue">value (e.g. "Testing for Update."</param>
		/// <returns>string to use in SET of Update Command (e.g. PSTU=:PSTU)</returns>
		public string PrepareUpdCmdToUpdDb(IDbCommand cmd, string dbFieldName, object paramValue)
		{
			string paramName = GetParaNamePrefix() + dbFieldName;
			CurrentUtilDb.AddParam(cmd, paramName, paramValue);
			return dbFieldName + "=" + paramName;
		}

		/// <summary>
		/// Prepare Update command by adding parameter to the command returing the "set" assing for the paraname and value.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="dbFieldName">Database Table Field Name (e.g. "STDT")</param>
		/// <param name="ufisTime">"" - To update DBNull. Otherwise convert to date time and update</param>
		/// <returns>string to use in SET of Update Command (e.g. STDT=:STDT)</returns>
		public string PrepareUpdCmdToUpdDbForTime(IDbCommand cmd, string dbFieldName, string ufisTime)
		{
			string paramName = GetParaNamePrefix() + dbFieldName;
			if (string.IsNullOrEmpty(ufisTime)) CurrentUtilDb.AddParamDateTime(cmd, paramName, null);
			else
			{
				DateTime dt = UtilTime.ConvUfisTimeStringToDateTime(ufisTime);
				CurrentUtilDb.AddParamDateTime(cmd, paramName, dt);
			}
			return dbFieldName + "=" + paramName;
		}

		/// <summary>
		/// Prepare the parameter and return the command string based on the operand
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="dbFieldName">Database field name (e.g. 'CNTI')</param>
		/// <param name="operand">Operand to use with db FieldName and ParamValue (e.g. '>=')</param>
		/// <param name="paramName">Parameter Name to use to add into param (e.g. CNT2)</param>
		/// <param name="paramValue">Parameter Value (e.g. '534')</param>
		/// <returns>dbFieldName + Operand + ParaPrefix + paramName (e.g. 'CNTI>=:CNT2')</returns>
		public string PrepareParam(IDbCommand cmd, string dbFieldName, string operand, string paramName, object paramValue)
		{
			string pnName = GetParaNamePrefix() + paramName;
			CurrentUtilDb.AddParam(cmd, pnName, paramValue);
			return dbFieldName + operand + pnName;
		}

		/// <summary>
		/// Prepare Parameter. Add the paramater name and value to the Command.Parameters. And return the formatted parameter name.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="paramName"></param>
		/// <param name="paramValue"></param>
		/// <returns></returns>
		public string PrepareParam(IDbCommand cmd, string paramName, object paramValue)
		{
			string pnName = GetParaNamePrefix() + paramName;
			CurrentUtilDb.AddParam(cmd, pnName, paramValue);
			return pnName;
		}

		/// <summary>
		/// Prepare the parameter for the Date Time data
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="paramName">parameter name</param>
		/// <param name="ufisTime">time in yyyymmddHHMMSS format (e.g. 20090130214500). If it is null or blank, it will set to DBNull</param>
		/// <returns></returns>
		public string PrepareParamForTime(IDbCommand cmd, string paramName, string ufisTime)
		{
			string pnName = GetParaNamePrefix() + paramName;
			if (string.IsNullOrEmpty(ufisTime)) CurrentUtilDb.AddParamDateTime(cmd, pnName, null);
			else
			{
				DateTime dt = UtilTime.ConvUfisTimeStringToDateTime(ufisTime);
				CurrentUtilDb.AddParamDateTime(cmd, pnName, dt);
			}
			return pnName;
		}

		public string PrepareParamForTime(IDbCommand cmd, string paramName, DateTime dtData)
		{
			string pnName = GetParaNamePrefix() + paramName;
			if (dtData.CompareTo(NULL_DATE_TIME) == 0) CurrentUtilDb.AddParamDateTime(cmd, pnName, null);
			else
			{
				CurrentUtilDb.AddParamDateTime(cmd, pnName, dtData);
			}
			return pnName;
		}

		/// <summary>
		/// Is record exist for given record id for the given table (by checking the table where URNO=recordId)
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="tableName"></param>
		/// <param name="recordId"></param>
		/// <returns>TRUE==>Exist, Otherwise==>Not Exist</returns>
		public static bool IsRecordExist(IDbCommand cmd, string tableName, string recordId)
		{
			//         tableName = "RUNTAB";
			//#warning Temporary hardcode of tableName for testing. To remove the above line.
			UtilDb udb = GetInstance();
			bool exist = false;
			string paraName = GetFormattedParaName("URNO");
			cmd.CommandText = "SELECT COUNT(*) FROM " + DB_Info.GetDbFullName(tableName) + " WHERE URNO=" + paraName;
			cmd.CommandType = CommandType.Text;
			cmd.Parameters.Clear();
			udb.AddParam(cmd, paraName, recordId);

			int cnt = -1;
			//LogTraceMsg("IsRecordExist:Cmd:" + cmd.CommandText);
			if (int.TryParse(cmd.ExecuteScalar().ToString(), out cnt))
			{
				if (cnt > 0) exist = true;
			}
			return exist;
		}

		#region Get Data for a column of a record
		/// <summary>
		/// Get Data for the given column in the given tableName and record id.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="tableName"></param>
		/// <param name="recordId"></param>
		/// <param name="colName"></param>
		/// <returns></returns>
		public static Object GetDataForAColumn(IDbCommand cmd, string tableName, string recordId, string colName)
		{
			object colVal = null;

			string pfix = GetParaNamePrefix();
			string pUrno = pfix + "URNO";

			cmd.CommandText = string.Format("SELECT {0} from {1} WHERE URNO={2} ",
			   colName, DB_Info.GetDbFullName( tableName ), pUrno);
			cmd.CommandType = CommandType.Text;
           //LogTraceMsg("cmd.CommandText" + cmd.CommandText);
			cmd.Parameters.Clear();
			GetInstance().AddParam(cmd, pUrno, recordId);
			colVal = cmd.ExecuteScalar();
			return colVal;
		}

		/// <summary>
		/// Get data for the given record id for a given column in string. If it is null, return "";
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="tableName">Table Name to get the data</param>
		/// <param name="recordId">Record Id (URNO)</param>
		/// <param name="colName">column name in the database table</param>
		/// <returns></returns>
		public static String GetDataInString(IDbCommand cmd, string tableName, string recordId, string colName)
		{
			object obj = GetDataForAColumn(cmd, tableName, recordId, colName);
			if ((obj == null) || (obj == DBNull.Value)) return "";
			else
			{
				return Convert.ToString(obj);
			}
		}

		/// <summary>
		/// Get integer for the given record id for a given column. If it is null, return 0.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="tableName">Table Name to get the data</param>
		/// <param name="recordId">Record Id (URNO)</param>
		/// <param name="colName">column name in the database table</param>
		/// <returns></returns>
		public static int GetDataInInt(IDbCommand cmd, string tableName, string recordId, string colName)
		{
			object obj = GetDataForAColumn(cmd, tableName, recordId, colName);
			if ((obj == null) || (obj == DBNull.Value)) return 0;
			else
			{
				int num = 0;
				if (int.TryParse((string)obj, out num)) return num;
				else return 0;
			}
		}
		#endregion

		public static int GetRecordCount(IDbCommand cmd, string tableName, string whereClause)
		{
			int cnt = 0;
			cmd.CommandText = string.Format("SELECT COUNT(*) FROM {0} {1}",
				DB_Info.GetDbFullName(tableName), whereClause);
			cmd.CommandType = CommandType.Text;
			if (!(int.TryParse(cmd.ExecuteScalar().ToString(), out cnt)))
			{
				cnt = 0;
			}
			return cnt;
		}

		/// <summary>
		/// Generate Unique Record Id for given processId
		/// </summary>
		/// <param name="cmd">command with connection open</param>
		/// <param name="procId">process id</param>
		/// <returns>new id for the process</returns>
		public static string GenUrid(IDbCommand cmd, string procId, bool useGivenDt, DateTime dtToUseInId)
		{
			UtilDb udb = GetInstance();
			StringBuilder sb = new StringBuilder();
			DateTime dt1 = DateTime.Now;
			DateTime dt2, dt3, dt4, dt5 = dt1, dt6;
			if (cmd == null) throw new ApplicationException("No Database Command.");

			#region Prepare DbCommand to get the current info of running no.
			string paraName = GetFormattedParaName("PROCID");
			cmd.CommandText = "SELECT URNO,TABN,RUNC,YYCN,USEM,USED,PFIX,SFIX,LANO,LSTU,SYSDATE CDT FROM " + GetDb_Vw_Urno() + " WHERE PROC=" + paraName;
			cmd.CommandType = CommandType.Text;
			cmd.Parameters.Clear();
			udb.AddParam(cmd, paraName, procId);
			cmd.CommandTimeout = 10;
			#endregion

			#region init variable
			string stProcUrno = "";
			string stTabName = "";
			int runCnt = -1;
			int yearCnt = -1;
			string useMonth = "";
			string useDay = "";
			string pfix = "";
			string sfix = "";
			int lastRunNo = -1;
			//DateTime lastRunNoDateTime = DateTime.Now ;
			DateTime dtCurDbDateTime = DateTime.Now;
			bool found = false;
			string newUrid = "";
			#endregion
			#region get current info of running no.
			using (IDataReader dr = cmd.ExecuteReader())
			{
				dt2 = DateTime.Now;
				while (dr.Read())
				{
					stProcUrno = Convert.ToString(dr["URNO"]);
					stTabName = Convert.ToString(dr["TABN"]);

					runCnt = Convert.ToInt32(dr["RUNC"]);
					yearCnt = Convert.ToInt32(dr["YYCN"]);
					useMonth = Convert.ToString(dr["USEM"]);
					useDay = Convert.ToString(dr["USED"]);
					pfix = Convert.ToString(dr["PFIX"]);
					sfix = Convert.ToString(dr["SFIX"]);
					lastRunNo = Convert.ToInt32(dr["LANO"]);
					//lastRunNoDateTime = Convert.ToDateTime(dr["LSTU"]);
					dtCurDbDateTime = Convert.ToDateTime(dr["CDT"]);
					found = true;
					if (lastRunNo <= 0)
					{
						try
						{
							CreateRunTabRecord(cmd, stProcUrno, procId, 0, "sys", Misc.ITrekTime.GetCurrentDateTime());
						}
						catch (Exception ex)
						{
							LogMsg("GenUrid:Err:CreateRunTabRecord:" + stProcUrno + ":" + procId + ":" + ex.Message);
						}
					}
					break;
				}
				dt3 = DateTime.Now;
			}
			#endregion

			if (found)
			{
				#region generate new Urid
				if (yearCnt > 4) yearCnt = 4;
				else if (yearCnt < 0) yearCnt = 0;

				if (useGivenDt)
				{//Use with Given Date and Time. Overwrite the database date time with given date time.
					dtCurDbDateTime = dtToUseInId;
				}

				string yyyy = dtCurDbDateTime.Year.ToString().Substring(4 - yearCnt);
				string mm = "";
				string dd = "";
				if (useDay.Trim().ToUpper() == "Y") dd = dtCurDbDateTime.Day.ToString().PadLeft(2, '0');
				if (useMonth.Trim().ToUpper() == "Y") mm = dtCurDbDateTime.Month.ToString().PadLeft(2, '0');

				int maxRunNo = Convert.ToInt32("".PadLeft(runCnt, '9'));
				dt4 = DateTime.Now;
				for (int i = 0; i < 100; i++)
				{
					if (lastRunNo >= maxRunNo) lastRunNo = 0;
					lastRunNo++;

					string stRunNo = Convert.ToString(lastRunNo).PadLeft(runCnt, '0');
					newUrid = pfix + yyyy + mm + dd + stRunNo + sfix;
					if (IsRecordExist(cmd, stTabName, newUrid)) continue;
					dt5 = DateTime.Now;
					UpdLastRunNo(cmd, stProcUrno, procId, lastRunNo);
					break;
				}
				dt6 = DateTime.Now;
				#endregion
			}
			else
			{
				throw new NoCtrlSettingException();
			}

			//LogTraceMsg(string.Format("GetSetting-Db:{0}, GetSeettingData:{1},PrepareFormat:{2},ChkRecExist:{3},GetId&UPd:{4}",
			//   UtilTime.ElapsedTime(dt1, dt2),
			//   UtilTime.ElapsedTime(dt2, dt3),
			//   UtilTime.ElapsedTime(dt3, dt4),
			//   UtilTime.ElapsedTime(dt4, dt5),
			//   UtilTime.ElapsedTime(dt5, dt6)));

			if (string.IsNullOrEmpty(newUrid)) throw new ApplicationException("Unable to generate new record id for Proc<" + procId + ">");
			return newUrid;
		}

		//private const string V_GEN_URNO = "V_ITK_GEN_URNO";

		private static string GetDb_Vw_Urno()
		{
			return DB_Info.GetDbFullName(DB_Info.DB_V_GEN_URNO);
		}

		/// <summary>
		/// Generate cntOfIdToGen Number of Urno (Record Id) for give process Id.      
		/// </summary>
		/// <param name="cmd">Command</param>
		/// <param name="procId">Process Id to generate the Urno</param>
		/// <param name="useGivenDt">Use the given date and time to format the Urno (True/False)</param>
		/// <param name="dtToUseInId">If "useGevenDt" is True, use this date and time to format the Urno</param>
		/// <param name="cntOfIdToGen">Number of Id to be generated</param>
		/// <returns></returns>
		public static List<string> GenUrids(IDbCommand cmd, string procId, bool useGivenDt, DateTime dtToUseInId, int cntOfIdToGen)
		{
			if (cntOfIdToGen < 1) throw new ApplicationException("Invalid Number of id to generate.");
			//else if (cntOfIdToGen < 2) throw new ApplicationException("Please use 'GenUrid' method to generate the id");
			else if (cntOfIdToGen > 500) throw new ApplicationException("Unable to generate more than 500 ids.");

			List<string> lsIds = new List<string>();
			StringBuilder sb = new StringBuilder();
			DateTime dt1 = DateTime.Now;
			DateTime dt2, dt3, dt4, dt5 = dt1, dt6;
			if (cmd == null) throw new ApplicationException("No Database Command.");

			#region Prepare DbCommand to get the current info of running no.
			string paraName = GetFormattedParaName("PROCID");
			cmd.CommandText = "SELECT URNO,TABN,RUNC,YYCN,USEM,USED,PFIX,SFIX,LANO,LSTU,SYSDATE CDT FROM " + GetDb_Vw_Urno() + " WHERE PROC=" + paraName;
			cmd.CommandType = CommandType.Text;
			cmd.Parameters.Clear();
			GetInstance().AddParam(cmd, paraName, procId);
			cmd.CommandTimeout = 10;
			#endregion

			#region init variable
			string stProcUrno = "";
			string stTabName = "";
			int runCnt = -1;
			int yearCnt = -1;
			string useMonth = "";
			string useDay = "";
			string pfix = "";
			string sfix = "";
			int lastRunNo = -1;
			//DateTime lastRunNoDateTime = DateTime.Now ;
			DateTime dtCurDbDateTime = DateTime.Now;
			bool found = false;
			string newUrid = "";
			#endregion
			#region get current info of running no.
			using (IDataReader dr = cmd.ExecuteReader())
			{
				dt2 = DateTime.Now;
				while (dr.Read())
				{
					stProcUrno = Convert.ToString(dr["URNO"]);
					stTabName = Convert.ToString(dr["TABN"]);

					runCnt = Convert.ToInt32(dr["RUNC"]);
					yearCnt = Convert.ToInt32(dr["YYCN"]);
					useMonth = Convert.ToString(dr["USEM"]);
					useDay = Convert.ToString(dr["USED"]);
					pfix = Convert.ToString(dr["PFIX"]);
					sfix = Convert.ToString(dr["SFIX"]);
					lastRunNo = Convert.ToInt32(dr["LANO"]);
					//lastRunNoDateTime = Convert.ToDateTime(dr["LSTU"]);
					dtCurDbDateTime = Convert.ToDateTime(dr["CDT"]);
					found = true;
					if (lastRunNo <= 0)
					{//It might be new record.
						#region Create New RunTab Record
						try
						{
							CreateRunTabRecord(cmd, stProcUrno, procId, 0, "sys", Misc.ITrekTime.GetCurrentDateTime());
						}
						catch (Exception ex)
						{
							LogMsg("GenUrids:Err:CreateRunTabRecord:" + stProcUrno + ":" + procId + ":" + ex.Message);
						}
						#endregion
					}
					break;
				}
				dt3 = DateTime.Now;
			}
			#endregion

			if (found)
			{
				#region generate new Urid and add to the list
				if (yearCnt > 4) yearCnt = 4;
				else if (yearCnt < 0) yearCnt = 0;

				if (useGivenDt)
				{//Use with Given Date and Time. Overwrite the database date time with given date time.
					dtCurDbDateTime = dtToUseInId;
				}

				string yyyy = dtCurDbDateTime.Year.ToString().Substring(4 - yearCnt);
				string mm = "";
				string dd = "";
				if (useDay.Trim().ToUpper() == "Y") dd = dtCurDbDateTime.Day.ToString().PadLeft(2, '0');
				if (useMonth.Trim().ToUpper() == "Y") mm = dtCurDbDateTime.Month.ToString().PadLeft(2, '0');

				int maxRunNo = Convert.ToInt32("".PadLeft(runCnt, '9'));
				dt4 = DateTime.Now;
				int initialRunNo = lastRunNo;
				bool exceedMax = false;

				do
				{
					int curCnt = lsIds.Count;
					for (int i = curCnt; i < cntOfIdToGen; i++)
					{
						if (lastRunNo >= maxRunNo)
						{
							lastRunNo = 0;
							exceedMax = true;
						}
						lastRunNo++;

						string stRunNo = Convert.ToString(lastRunNo).PadLeft(runCnt, '0');
						newUrid = pfix + yyyy + mm + dd + stRunNo + sfix;
						if (lsIds.Contains(newUrid)) continue;//Avoid duplicate id.
						lsIds.Add(newUrid);
					}
					lsIds = RemoveExistingIds(cmd, stTabName, lsIds, curCnt);//Remove the id which are already in the table
					if (exceedMax)
					{//Last Run No was restart from 0
						if (lastRunNo > initialRunNo) throw new ApplicationException("Not exough id to generate.");
					}
				} while (lsIds.Count < cntOfIdToGen);//Loop until getting the enough number of ids.

				dt5 = DateTime.Now;
				UpdLastRunNo(cmd, stProcUrno, procId, lastRunNo);//Update the last running no. into table
				dt6 = DateTime.Now;
				#endregion
			}
			else
			{
				throw new NoCtrlSettingException();
			}

//            LogTraceMsg(string.Format(@"GenUrids:Cnt:{0},
//            GetDbSetting:{1}, GetSettingData :{2}, Format:{3}, 
//            GenIds      :{4}, UPdLastNo      :{5}, Tot   :{6}",
//               cntOfIdToGen,
//               UtilTime.ElapsedTime(dt1, dt2),//GetDbSetting
//               UtilTime.ElapsedTime(dt2, dt3),//GetData
//               UtilTime.ElapsedTime(dt3, dt4),//Formatting
//               UtilTime.ElapsedTime(dt4, dt5),//Generate Ids
//               UtilTime.ElapsedTime(dt5, dt6),//Update Last Running No.
//               UtilTime.ElapsedTime(dt1, dt6)) + //Total Time
//               Environment.NewLine +
//               UtilMisc.ConvListToString(lsIds, ","));

			if (string.IsNullOrEmpty(newUrid)) throw new ApplicationException("Unable to generate new record id for Proc<" + procId + ">");
			return lsIds;
		}

		/// <summary>
		/// Remove item from List if it is already existed in the given table
		/// </summary>
		/// <param name="cmd">Command</param>
		/// <param name="stTabName">Table Name to check the existance of id</param>
		/// <param name="lsIdsToCheck">List of Ids to check</param>
		/// <param name="frIdx">Start checking from this index and onward</param>
		/// <returns>List of Ids which are not existed in the given table</returns>
		private static List<string> RemoveExistingIds(IDbCommand cmd, string stTabName, List<string> lsIdsToCheck, int frIdx)
		{
			cmd.CommandText = "SELECT URNO FROM " + DB_Info.GetDbFullName(stTabName) +
			   " WHERE URNO IN (" +
			   UtilMisc.ConvListToString(lsIdsToCheck,
			   ",", "'", "'", frIdx, lsIdsToCheck.Count - 1) +
				  ")";
			cmd.Parameters.Clear();
			using (IDataReader dr = cmd.ExecuteReader())
			{
				while (dr.Read())
				{
					string stUrno = Convert.ToString(dr["URNO"]).Trim();
					if (lsIdsToCheck.Contains(stUrno)) lsIdsToCheck.Remove(stUrno);
				}
			}
			return lsIdsToCheck;
		}

		/// <summary>
		/// Get Current Date Time of Database Server
		/// </summary>
		/// <param name="cmd"></param>
		/// <returns></returns>
		public static DateTime GetCurDateTimeOfDbServer(IDbCommand cmd)
		{
			cmd.CommandText = "SELECT SYSDATE FROM DUAL";
			cmd.CommandType = CommandType.Text;

			DateTime dt = Convert.ToDateTime(cmd.ExecuteScalar());

			return dt;
		}

		/// <summary>
		/// Default Date Time
		/// </summary>
		public static DateTime DEFAULT_DT = new DateTime(1, 1, 1);

		/// <summary>
		/// Get Last Updated Date and Time of given table.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="tableName">Table Name to check</param>
		/// <param name="isExist">Is the information exist</param>
		/// <returns>Last Updated Date Time of the Table</returns>
		public static DateTime GetLastUpdateDateTimeOfTable(IDbCommand cmd, string tableName, out bool isExist)
		{
			UtilDb udb = GetInstance();
			DateTime dt;
			isExist = false;
			bool isCmdNull = false;//Is Passing in Parameter 'cmd' null? 
			//If it is null, create and close the command cmd from this method.
			try
			{
				if (cmd == null)
				{
					isCmdNull = true;
					cmd = udb.GetCommand(false);
				}

				dt = GetLastUpdateDateTimeOfTableFromTRKTAB(cmd, tableName, out isExist);
				if (!isExist)
				{
					//cmd.CommandText = "SELECT LSTU FROM " + tableName + " ORDER BY LSTU DESC";
					cmd.CommandText = string.Format("SELECT LSTU FROM (SELECT LSTU FROM {0} WHERE LSTU>TO_DATE('{1:yyyyMMdd}','YYYYMMDD') AND LSTU IS NOT NULL ORDER BY LSTU DESC) WHERE ROWNUM<2",
                    DB_Info.GetDbFullName(tableName), Misc.ITrekTime.GetCurrentDateTime().AddDays(-1));
//#warning //TODO - To implement to update the last update date time in TRKTAB for each TABLE.
					//string pfix = GetParaNamePrefix();
					//string pTabn = pfix + "TABN";
					//cmd.CommandText = "SELECT LSTU from TRKTAB WHERE TABN=" + pTabn;
					//cmd.Parameters.Clear();
					//udb.AddParam(cmd, pTabn, tableName);
                   // LogTraceMsg(" GetLastUpdateDateTimeOfTable cmd.CommandText" + cmd.CommandText);
					try
					{
						Object val = cmd.ExecuteScalar();
						if ((val != null) && (DateTime.TryParse(val.ToString(), out dt)))
						{
							isExist = true;
						}
						else
						{
							dt = DEFAULT_DT;
						}

					}
					catch (Exception ex)
					{
						dt = DEFAULT_DT;
						LogMsg("GetLastUpdateDateTimeOfTable:" + tableName + ":Err:" + ex.Message);
					}
				}
			}
			catch (Exception)
			{
				throw;
			}
			finally
			{
				if (isCmdNull)
				{
					udb.CloseCommand(cmd, false);
				}
			}
			return dt;
		}

		private static DateTime GetLastUpdateDateTimeOfTableFromTRKTAB(IDbCommand cmd, string tableName, out bool isExist)
		{
			UtilDb udb = GetInstance();
			DateTime dt;
			isExist = false;

			//cmd.CommandText = "SELECT LSTU FROM " + tableName + " ORDER BY LSTU DESC";
			string pfix = GetParaNamePrefix();
			string pTabn = pfix + "TABN";
            cmd.CommandText = "SELECT LUDT from " + GetDb_T_TRKTAB() + " WHERE TABN=" + pTabn;
            //LogTraceMsg("GetLastUpdateDateTimeOfTableFromTRKTAB cmd.CommandText" + cmd.CommandText);
			cmd.Parameters.Clear();
			udb.AddParam(cmd, pTabn, tableName);
			try
			{
				Object val = cmd.ExecuteScalar();
				if ((val != null) && (DateTime.TryParse(val.ToString(), out dt)))
				{
					isExist = true;
					//cmd.CommandText = string.Format("INSERT INTO ITK_TRKTAB (URNO,TABN,LSTU) VALUES ({0},{0},SYSDATE)",
					//    tableName);
					//cmd.Parameters.Clear();
					//cmd.ExecuteNonQuery();
				}
				else
				{
					dt = DEFAULT_DT;
				}
			}
			catch (Exception ex)
			{
				dt = DEFAULT_DT;
				LogMsg("GetLastUpdateDateTimeOfTable:" + tableName + ":Err:" + ex.Message);
			}
			return dt;
		}

		#endregion

		/// <summary>
		/// Update the Last Running No after generating the unique id for the process.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="recId"></param>
		/// <param name="procId"></param>
		/// <param name="newRunNo"></param>
		private static void UpdLastRunNo(IDbCommand cmd, string recId, string procId, int newRunNo)
		{
			UtilDb udb = GetInstance();
			string pLano = GetFormattedParaName("LANO");
			string pUrno = GetFormattedParaName("URNO");
			cmd.CommandText = "UPDATE " + GetDb_T_RUNTAB() + " SET LANO=" + pLano + " WHERE URNO=" + pUrno;
			cmd.Parameters.Clear();
			udb.AddParam(cmd, pLano, newRunNo);
			udb.AddParam(cmd, pUrno, recId);
			int cnt = cmd.ExecuteNonQuery();
			if (cnt < 1) throw new ApplicationException("Fail to update the new running no.");
		}

		//public const string DB_RUN_NO_TAB = "ITK_RUNTAB";
		private static string GetDb_T_RUNTAB()
		{
			return DB_Info.GetDbFullName(DB_Info.DB_T_RUNTAB);
		}

        private static string GetDb_T_TRKTAB()
        {
            return DB_Info.GetDbFullName(DB_Info.DB_T_TRKTAB);
        }

		private const string FIELDS_RUNTAB_INSERT = "URNO,LANO,USEC,CDAT,USEU,LSTU";

		private static void CreateRunTabRecord(IDbCommand cmd,
		   string recId, string procId, int newRunNo,
		   string userId, DateTime dtUpdDateTime)
		{
			UtilDb udb = GetInstance();
			if (UtilDb.IsRecordExist(cmd, DB_Info.DB_T_RUNTAB, recId)) return;

			StringBuilder sb = new StringBuilder();
			cmd.Parameters.Clear();
			sb.Append(udb.PrepareParam(cmd, "URNO", recId));
			sb.Append("," + udb.PrepareParam(cmd, "LANO", newRunNo));
			sb.Append("," + udb.PrepareParam(cmd, "USEC", userId));
			sb.Append("," + udb.PrepareParam(cmd, "CDAT", dtUpdDateTime));
			sb.Append("," + udb.PrepareParam(cmd, "USEU", userId));
			sb.Append("," + udb.PrepareParam(cmd, "LSTU", dtUpdDateTime));

			cmd.CommandText = string.Format("INSERT INTO {0} ({1}) VALUES ({2})",
			   GetDb_T_RUNTAB(), FIELDS_RUNTAB_INSERT, sb.ToString());
			//LogTraceMsg("CreateRunTabRecord:Cmd:" + cmd.CommandText);
			int cnt = cmd.ExecuteNonQuery();
			if (cnt < 1) throw new ApplicationException("Fail to create the new running no.");
		}

		/// <summary>
		/// Close Command with Unknown Type
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="commit"></param>
		private static void CloseUnkCommand(IDbCommand cmd, bool commit)
		{
			try
			{
				if (cmd != null)
				{
					if (cmd.Connection != null)
					{
						if (cmd.Connection.State != ConnectionState.Closed)
						{
							cmd.Connection.Close();
						}
						cmd.Connection.Dispose();
						cmd.Connection = null;
					}
					cmd.Dispose();
					cmd = null;
				}
			}
			catch (Exception)
			{
			}
		}

		#region Oracle Db related Methods
		private static IDbCommand GetOracleCommand(bool withTrx, string connString, string dbVersion)
		{
			IDbCommand cmd = null;
			if (withTrx)
			{
				cmd = UtilOraDb.GetInstance().GetCommandWithTrx(connString);
			}
			else
			{
				cmd = UtilOraDb.GetInstance().GetCommand(connString);
			}
			return cmd;
		}

		private static void CloseOracleCommand(IDbCommand cmd, bool commit)
		{
			UtilOraDb.GetInstance().CloseCommand(cmd, commit);
		}
		#endregion

		//#region Dispose the object
		//// Implement IDisposable.
		//// Do not make this method virtual.
		//// A derived class should not be able to override this method.
		//public void Dispose()
		//{
		//   Dispose(true);
		//   // This object will be cleaned up by the Dispose method.
		//   // Therefore, you should call GC.SupressFinalize to
		//   // take this object off the finalization queue 
		//   // and prevent finalization code for this object
		//   // from executing a second time.
		//   GC.SuppressFinalize(this);
		//}

		//// Track whether Dispose has been called.
		//private bool disposed = false;

		//// Dispose(bool disposing) executes in two distinct scenarios.
		//// If disposing equals true, the method has been called directly
		//// or indirectly by a user's code. Managed and unmanaged resources
		//// can be disposed.
		//// If disposing equals false, the method has been called by the 
		//// runtime from inside the finalizer and you should not reference 
		//// other objects. Only unmanaged resources can be disposed.
		//private void Dispose(bool disposing)
		//{
		//   if (!this.disposed)
		//   {
		//      lock (_lockObj)
		//      {
		//         // Check to see if Dispose has already been called.
		//         if (!this.disposed)
		//         {
		//            // If disposing equals true, dispose all managed 
		//            // and unmanaged resources.
		//            if (disposing)
		//            {
		//               // Dispose managed resources.
		//               //component.Dispose();
		//            }
		//         }
		//         disposed = true;
		//      }
		//   }
		//}

		//// Use C# destructor syntax for finalization code.
		//// This destructor will run only if the Dispose method 
		//// does not get called.
		//// It gives your base class the opportunity to finalize.
		//// Do not provide destructors in types derived from this class.
		//~UtilDb()
		//{
		//   // Do not re-create Dispose clean-up code here.
		//   // Calling Dispose(false) is optimal in terms of
		//   // readability and maintainability.
		//   Dispose(false);
		//}
		//#endregion


		public static String GetData(IDataReader dr, string colName)
		{
			string st = null;
			object obj = dr[colName];
			if (obj != null)
			{
				if (obj != DBNull.Value)
					st = (string)obj;
			}
			return st;
		}

		/// <summary>
		/// Get Integer data from Datareader with given column name. If null data, return 0.
		/// </summary>
		/// <param name="dr"></param>
		/// <param name="colName"></param>
		/// <returns></returns>
		public static int GetDataInt(IDataReader dr, string colName)
		{
			int num = 0;
			bool hasValidData = false;
			object obj = dr[colName];
			if (obj != null)
			{
				if (obj != DBNull.Value)
				{
					hasValidData = Int32.TryParse(obj.ToString(), out num);
				}
			}
			return num;
		}

		/// <summary>
		/// Try to get data from datareader with given column name. 
		/// If null or invalid number, return false. Otherwise return true.
		/// </summary>
		/// <param name="dr"></param>
		/// <param name="colName"></param>
		/// <param name="num">integer data. It will be 0 if not a valid number</param>
		/// <returns></returns>
		public static bool TryGetDataInt(IDataReader dr, string colName, out int num)
		{
			num = 0;
			bool hasValidData = false;
			object obj = dr[colName];
			if (obj != null)
			{
				if (obj != DBNull.Value)
				{
					hasValidData = Int32.TryParse(obj.ToString(), out num);
				}
			}
			return hasValidData;
		}

		#region Db Insert/Update Operation

		public const string PFIX_VIRTUAL_NEW_ID = "NEW_";
		public const string DB_TABLE_COL_URNO = "URNO";

		public static int InsertDataToDb(IDbCommand cmd, DataTable dt,
		   string dbTableName, string processIdToGenId,
		   string userId, DateTime curDt)
		{
			List<string> stIds = null;
			if (!string.IsNullOrEmpty(processIdToGenId))
			{
				stIds = GenUrids(cmd, processIdToGenId,
				  false, DateTime.Now, dt.Rows.Count);//Generate the Urnos.
			}

			return InsertDataToDb(cmd, dt, dbTableName, userId, curDt, stIds);
		}

		/// <summary>
		/// Insert the Data for the row with URNO start with <see cref="PFIX_VIRTUAL_NEW_ID"/> in the datatable into database table
		/// </summary>
		/// <param name="cmd">command with connection</param>
		/// <param name="dt">DataTable contains data to insert into database (Column Name must be same as Database Table)</param>
		/// <param name="dbTableName">Database TableName</param>
		/// <param name="processIdToGenId">Process Id to generate the URNO</param>
		/// <param name="userId">User Id who update the information</param>
		/// <param name="curDt">Current Date and Time</param>
		/// <returns>total Insert Count</returns>
		public static int InsertDataToDb(IDbCommand cmd, DataTable dt,
		   string dbTableName,
		   string userId, DateTime curDt, List<String> stIds)
		{
			UtilDb udb = GetInstance();
			int totUpdCnt = 0;
			cmd.Parameters.Clear();
			StringBuilder sb = new StringBuilder();
			StringBuilder sbDbColNames = new StringBuilder();

			List<string> lsColNamesToAssignValue = new List<string>();//Name of the column to assign the value
			string seperator = "";
			string paramPrefix = UtilDb.GetParaNamePrefix();

			#region Prepare the parameters with names only (without value)
			foreach (DataColumn col in dt.Columns)
			{
				bool toAssignCol = true;

				if (col.DataType == typeof(DateTime))
				{
					if ("CDAT,LSTU".Contains(col.ColumnName))
					{
						sb.Append(seperator + udb.PrepareParamForTime(cmd, col.ColumnName, curDt));
						toAssignCol = false;
					}
					else
					{
						sb.Append(seperator + udb.PrepareParamForTime(cmd, col.ColumnName, ""));
					}
				}
				else
				{
					if ("USEC,USEU".Contains(col.ColumnName))
					{
						sb.Append(seperator + udb.PrepareParam(cmd, col.ColumnName, userId));
						toAssignCol = false;
					}
					else
					{
						sb.Append(seperator + udb.PrepareParam(cmd, col.ColumnName, null));
					}
				}
				sbDbColNames.Append(seperator + col.ColumnName);
				seperator = ",";
				if (toAssignCol)
				{
					lsColNamesToAssignValue.Add(col.ColumnName);
				}
			}
			#endregion

			string stCmd = string.Format(@"INSERT INTO {0} ({1}) VALUES ({2})",
			   DB_Info.GetDbFullName( dbTableName ), sbDbColNames.ToString(), sb.ToString());

			if (stCmd != cmd.CommandText)
			{
                //LogTraceMsg("InsertDataToDb stCmd" + stCmd);
				cmd.CommandText = stCmd;
				cmd.CommandType = CommandType.Text;
				cmd.Prepare();
			}

			int cntCol = lsColNamesToAssignValue.Count;

			int idxRowIdx = -1;
			foreach (DataRow row in dt.Rows)
			{
				idxRowIdx++;
				try
				{
					#region Assign Value for the parameter and Update to the database
					if (row[DB_TABLE_COL_URNO].ToString().StartsWith(PFIX_VIRTUAL_NEW_ID))
					{//It is virtual Urno
						row[DB_TABLE_COL_URNO] = stIds[idxRowIdx];//Assign the Actual Urno.
					}

					for (int i = 0; i < cntCol; i++)
					{
						string colName = lsColNamesToAssignValue[i];
						try
						{
							string st = ((IDataParameter)cmd.Parameters[paramPrefix + colName]).DbType.ToString();
							((IDataParameter)cmd.Parameters[paramPrefix + colName]).Value = row[colName];
						}
						catch (Exception ex)
						{
							LogMsg("InsertDataToDb:Err:AssignVal:" + colName + ":" + ex.Message + Environment.NewLine + UtilDataSet.GetDataRowString(row));
							//throw new ApplicationException(ex.Message + Environment.NewLine + UtilDataSet.GetDataRowString(row));
						}
					}
					int updCnt = cmd.ExecuteNonQuery();
					if (updCnt < 1) throw new ApplicationException("Update Fail!!!");
					totUpdCnt++;
					#endregion
				}
				catch (Exception ex)
				{
					string st = ex.Message + Environment.NewLine + UtilDataSet.GetDataRowString(row);
					LogMsg("InsertDataToDb:Err:" + ex.Message);
					throw;//Throw error when one record was not able to update.
				}
			}
			return totUpdCnt;
		}


		/// <summary>
		/// Update the data in the datatable into database table. Return the success row count.
		/// </summary>
		/// <param name="cmd">command with connection</param>
		/// <param name="dt">DataTable contains data to insert into database (Column Name must be same as Database Table)</param>
		/// <param name="dbTableName">Database TableName</param>
		/// <param name="userId">User Id who update the information</param>
		/// <param name="curDt">Current Date and Time</param>
		/// <param name="allowUnsuccessUpd">Allow Unsuccessful Update (True/False). If not allowed and fail to update, the exception will be raised.</param>
		/// <param name="totRowCntToUpd">total number of row to update (without the row with URNO start with <see cref="PFIX_VIRTUAL_NEW_ID"/>) </param>
		/// <returns>total number of rows which are succcessfully updated</returns>
		public static int UpdateDataToDb(IDbCommand cmd, DataTable dt,
		   string dbTableName,
		   string userId, DateTime curDt,
		   bool allowUnsuccessUpd,
		   out int totRowCntToUpd)
		{
			UtilDb udb = GetInstance();
			cmd.Parameters.Clear();
			totRowCntToUpd = 0;
			int totUpdatedRowCnt = 0;
			StringBuilder sb = new StringBuilder();

			List<string> lsColNames = new List<string>();
			string seperator = "";
			string paramPrefix = UtilDb.GetParaNamePrefix();

			List<MyParam> lsParam = new List<MyParam>();

			foreach (DataColumn col in dt.Columns)
			{
				if ("URNO,CDAT,USEC".Contains(col.ColumnName))
				{
					continue;
				}

				if (col.ColumnName == "LSTU")
				{
					sb.Append(seperator + udb.PrepareUpdCmdToUpdDbForTime(cmd, col.ColumnName, UtilTime.ConvDateTimeToUfisFullDTString(curDt)));
				}
				else if (col.ColumnName == "USEU")
				{
					sb.Append(seperator + udb.PrepareUpdCmdToUpdDb(cmd, col.ColumnName, userId));
				}
				else
				{
					if (col.DataType == typeof(DateTime))
					{
						sb.Append(seperator + udb.PrepareUpdCmdToUpdDbForTime(cmd, col.ColumnName, null));
						lsParam.Add(new MyParam(col.ColumnName, EnumParamType.DateTime));
					}
					else
					{
						sb.Append(seperator + udb.PrepareUpdCmdToUpdDb(cmd, col.ColumnName, null));
						lsParam.Add(new MyParam(col.ColumnName, EnumParamType.Other));
					}

					lsColNames.Add(col.ColumnName);
				}

				seperator = ",";
			}

			string stWhere = "URNO=" + udb.PrepareParam(cmd, "URNO", null);
			lsColNames.Add("URNO");
			lsParam.Add(new MyParam("URNO", EnumParamType.Other));

			string stCmd = string.Format("UPDATE {0} SET {1} WHERE {2}",
			  DB_Info.GetDbFullName( dbTableName ), sb.ToString(), stWhere);
            //LogTraceMsg("UpdateDataToDb stCmd" + stCmd);
			if (stCmd != cmd.CommandText)
			{
				cmd.CommandText = stCmd;
				cmd.CommandType = CommandType.Text;
				cmd.Prepare();
			}

			int cntCol = lsColNames.Count;

			foreach (DataRow row in dt.Rows)
			{
				if (row[DB_TABLE_COL_URNO].ToString().StartsWith(PFIX_VIRTUAL_NEW_ID))
				{//It is virtual Urno
					continue;
				}
				totRowCntToUpd++;

				for (int i = 0; i < cntCol; i++)
				{
					string st = ((IDataParameter)cmd.Parameters[paramPrefix + lsColNames[i]]).DbType.ToString();
					//((IDataParameter)cmd.Parameters[paramPrefix + lsColNames[i]]).Value = row[lsColNames[i]];
					lsParam[i].AssignParamData(cmd.Parameters, row);
				}
				int updCnt = cmd.ExecuteNonQuery();
				if ((updCnt < 1) && (!allowUnsuccessUpd)) throw new ApplicationException("Update Fail!!!");
				totUpdatedRowCnt++;
			}
			return totUpdatedRowCnt;
		}

		/// <summary>
		/// Insert/Update the Data in DataTable according to row state in the datatable. Return true when success.
		/// Before calling this method, 
		/// 1. get current data from db into datatable
		/// 2. update/insert new data into datatable (For update, check any difference before update)
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="dt"></param>
		/// <param name="dbTableName"></param>
		/// <param name="procId"></param>
		/// <param name="userId"></param>
		/// <param name="updDateTime"></param>
		/// <param name="insCnt"></param>
		/// <param name="updCnt"></param>
		/// <param name="delCnt"></param>
		/// <returns></returns>
		public static bool InsertUpdateDataToDb(IDbCommand cmd, DataTable dt,
			string dbTableName, string procId,
			string userId, DateTime updDateTime, out int insCnt, out int updCnt, out int delCnt)
		{
			bool success = true;
			insCnt = 0;
			updCnt = 0;
			delCnt = 0;
			DataTable dtIns = dt.GetChanges(DataRowState.Added);
			DataTable dtUpd = dt.GetChanges(DataRowState.Modified);

			if ((dtIns != null) && (dtIns.Rows.Count > 0))
			{
				UtilDb.InsertDataToDb(cmd, dtIns, dbTableName, procId, userId, updDateTime);
			}

			if ((dtUpd != null) && (dtUpd.Rows.Count > 0))
			{
				int totRowCntToUpd = 0;
				updCnt = UtilDb.UpdateDataToDb(cmd, dtUpd, dbTableName, userId, updDateTime, false, out totRowCntToUpd);
				if (updCnt != totRowCntToUpd) success = false;
			}
			return success;
		}

		public static bool IsNullDateTime(DateTime? dt)
		{
			bool isNull = false;

			if ((dt == null) || (dt.Value.CompareTo(DEFAULT_DT) == 0)) isNull = true;

			return isNull;
		}

		/// <summary>
		/// Convert String to DateTime to be stored in Database.
		/// </summary>
		/// <param name="st">date time string in yyyymmddhhmmss format</param>
		/// <returns></returns>
		public static DateTime ConvString2DBDateTime(string st)
		{
			DateTime dt = UtilDb.DEFAULT_DT;
			if (!string.IsNullOrEmpty(st))
			{
				dt = UtilTime.ConvUfisTimeStringToDateTime(st);
			}

			return dt;
		}

		enum EnumParamType { DateTime, Other };

		class MyParam
		{
			static readonly string PARAM_PREFIX = GetParaNamePrefix();
			private string _paraName;
			private string _colName;
			private EnumParamType _type;
			internal MyParam(string colName, EnumParamType pType)
			{
				_colName = colName;
				_paraName = PARAM_PREFIX + _colName;
				_type = pType;
			}

			internal void AssignParamData(IDataParameterCollection paraColl, DataRow row)
			{
				object data = row[_colName];
				if ((_type == EnumParamType.DateTime) &&
					(data != DBNull.Value) &&
				   (data != null) &&
				   (((DateTime)data).CompareTo(UtilTime.DEFAULT_DATE_TIME) == 0))
				{
					((IDataParameter)paraColl[_paraName]).Value = DBNull.Value;
				}
				else
				{
					((IDataParameter)paraColl[_paraName]).Value = data;
				}
			}
		}

		#endregion

		#region Logging
		private static void LogTraceMsg(string msg)
		{
			try
			{
				MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("UtilDb", msg);
			}
			catch (Exception)
			{
				UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("UtilDb", msg);
			}
		}

		private static void LogMsg(string msg)
		{
			try
			{
				MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("UtilDb", msg);
			}
			catch (Exception)
			{
				UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("UtilDb", msg);
			}
		}
		#endregion

		public IDbCommand GetCommand(string stConnString, bool withTrx)
		{
			IDbCommand cmd = null;

			if (withTrx)
			{
				cmd = CurrentUtilDb.GetCommandWithTrx(stConnString);
			}
			else
			{
				cmd = CurrentUtilDb.GetCommand(stConnString);
			}
			return cmd;
		}

	}
}