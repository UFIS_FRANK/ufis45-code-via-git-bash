using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;

using iTrekWMQ;
using IBM.WMQ;

using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DB;
using UFIS.ITrekLib.ENT;
using System.Xml;
using iTrekXML;

namespace UFIS.ITrekLib.Ctrl
{
    public class CtrlCPM
    {
        public static string RetrieveCPMForAFlight(string flightUrNo, string flightNo)
        {
            DBCpm dbCpm = DBCpm.GetInstance();
            //string st = "";
            StringBuilder sb = new StringBuilder("");
            try
            {
                DSCpm ds = RetrieveCPMUldForAFlight(flightUrNo);
                foreach (DSCpm.CPMDETRow row in ds.CPMDET.Select("","POSF,DSSN,ULDN"))
                {
                    //st += row.ULD_TXT + "\n";
                    sb.Append(row.ULD_TXT + "\n");
                }
            }catch (ApplicationException ex)
            {
                throw new ApplicationException(ex.Message);
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Unable to get the CPM Message for flight " + flightNo + ", " + flightUrNo + " due to " + ex.Message );
            }
            return sb.ToString();
            //return st;
        }

        public static DSCpm RetrieveCPMUldForAFlight(string flightUrNo)
        {
            CheckExistCPM(flightUrNo);
            return DBCpm.GetInstance().RetrieveCpmDetForAFlight(flightUrNo);
        }

        private static void CheckExistCPM(string flightUrNo)
        {
            DBCpm db = DBCpm.GetInstance();
            if (!db.IsCPMExist(flightUrNo))
            {
                RequestCPM(flightUrNo);
                System.Threading.Thread.Sleep(2000);
            }
        }

        public static void CheckExistCPMForAFlight(string flightUrNo)
        {
            DBCpm db = DBCpm.GetInstance();
            if (!db.IsCPMExist(flightUrNo))
            {
                RequestCPM(flightUrNo);

            }
        }

        public static DSCpm RetrieveUnSentCPMUldForAFlight(string flightUrNo)
        {
            CheckExistCPM(flightUrNo);
            return DBCpm.GetInstance().RetrieveUnSentCPMUldForAFlight(flightUrNo);
        }

        public static DSCpm RetrieveUnSentCPMUldForAFlight(string flightUrNo, bool checkCPMExist)
        {
            if (checkCPMExist)
                CheckExistCPM(flightUrNo);
            return DBCpm.GetInstance().RetrieveUnSentCPMUldForAFlight(flightUrNo);
        }

        public static EntDsCPM RetrieveUnSentCPMUldForAFlightForAO(string flightUrNo)
        {
            EntDsCPM ent = new EntDsCPM();
            CheckExistCPM(flightUrNo);
            DSCpm ds = DBCpm.GetInstance().RetrieveUnSentCPMUldForAFlightForTrips(flightUrNo);
            if (ds == null)
            {
                ent.DsCPM = new DSCpm();
            }
            else
            {
                ent.DsCPM = (DSCpm)ds.Copy();
            }
            return ent;
        }

        public static EntDsCPM RetrieveUnSentCPMUldForAFlightForTrips(string flightUrNo)
        {
            EntDsCPM ent = new EntDsCPM();
            //CheckExistCPM(flightUrNo);
            ent.DsCPM = DBCpm.GetInstance().RetrieveUnSentCPMUldForAFlight(flightUrNo);
            return ent;
        }

        public static DataTable RetrieveUnSentCPMandTripsInfo(string flightUrno)
        {
            CheckExistCPM(flightUrno);
            return CtrlTrip.RetrieveUnSentCPMandTripsInfo(flightUrno);
        }


        public static EntDsCPM RetrieveEntDsCpmForAFlight(string flightUrNo)
        {
            EntDsCPM ent = new EntDsCPM();
            CheckExistCPM(flightUrNo);
            ent.DsCPM = DBCpm.GetInstance().RetrieveCpmDetForAFlight(flightUrNo);
            return ent;
        }

        public static string GetUldInfoForAFlight(string flightUrNo, out int uldCntForSIN, out int uldCntForInter)
        {
            string st = "";
            uldCntForInter = 0;
            uldCntForSIN = 0;
            CheckExistCPM(flightUrNo);

            DSCpm ds = DBCpm.GetInstance().RetrieveCpmDetForAFlight(flightUrNo);
            foreach (DSCpm.CPMDETRow row in ds.CPMDET)
            {
                if (row.APC3.Trim().ToUpper() == "SIN")
                {
                    uldCntForSIN++;
                }
                else
                {
                    uldCntForInter++;
                }
            }
            st = "ULD SIN:" + uldCntForSIN + " INTER:" + uldCntForInter;
            return st;
        }

        public static string GetUldInfoForAFlight(string flightUrNo, out int uldCntForSIN, out int uldCntForInter, bool checkCPMExist)
        {
            string st = "";
            uldCntForInter = 0;
            uldCntForSIN = 0;
            if (checkCPMExist)
                CheckExistCPM(flightUrNo);

            //DSCpm ds = DBCpm.GetInstance().RetrieveCpmDetForAFlight(flightUrNo);
             DSCpm ds = DBCpm.GetInstance().RetrieveCpmDetForAFlightWithoutCargo(flightUrNo);
            foreach (DSCpm.CPMDETRow row in ds.CPMDET)
            {
               if (!String.IsNullOrEmpty(row.CONT))
               {
                  string stCont = row.CONT.Trim().ToUpper();
                  if (stCont.StartsWith("BT") || stCont.StartsWith("T"))
                  {
                     uldCntForInter++;
                  }
                  else
                  {
                     uldCntForSIN++;
                  }
               }
               else
               {
                  uldCntForSIN++;
               }
            }
            if (ds.CPMDET.Rows.Count > 0)
            {
               st = "ULD SIN:" + uldCntForSIN + " INTER:" + uldCntForInter;
            }
            return st;
        }

        private static string FromItrekToSoccQueueName
        {
            get
            {
                iTXMLPathscl iTPaths = new iTXMLPathscl();
                return iTPaths.GetFROMITREKCPMQueueName();
            }
        }

        private static string FromSoccToItrekQueueName
        {
            get
            {
                iTXMLPathscl iTPaths = new iTXMLPathscl();
                return iTPaths.GetTOITREKCPMQueueName();
            }
        }

        public static string RequestCPM(string flightId)
        {
            string iTrekToSoccQueueName = FromItrekToSoccQueueName;

            ITrekWMQClass1 iTWMQ = new ITrekWMQClass1();
            string msg = "<LOA>" + flightId + "</LOA>";
            LogTraceMsg("RequestCPM:" + msg);
            string msgId = iTWMQ.putTheMessageIntoQueue(msg, iTrekToSoccQueueName);
            LogTraceMsg("Requested");

            return msgId;
        }

        public static void UpdateCPMInfoFromBackendToCPMXmlFile(XmlDocument xmlDoc)
        {
            DBCpm.GetInstance().UpdateCPMInfoFromBackendToCPMXmlFile(xmlDoc);
        }

        private static void LogMsg(string msg)
        {
            Util.UtilLog.LogToTraceFile("CtrlCPM", msg);
        }

        private static void LogTraceMsg(string msg)
        {
            Util.UtilLog.LogToTraceFile("CtrlCPM", msg);
        }
    }

}
