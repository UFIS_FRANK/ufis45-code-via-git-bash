using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using UFIS.ITrekLib.DB;
using UFIS.ITrekLib.Util;
using System.Data;
using System.Collections;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.ENT;
using iTrekWMQ;
using iTrekXML;
using UFIS.ITrekLib.Misc;

namespace UFIS.ITrekLib.Ctrl
{
	public class CtrlCPM
	{

		public static string GetUldInfoForAFlight(string flightUrNo, out int uldCntForSIN, out int uldCntForInter, bool checkCPMExist)
		{
			if (checkCPMExist)
				CheckExistCPM(flightUrNo);

			return GetUldInfoForAFlight(flightUrNo, out uldCntForSIN, out uldCntForInter);
		}

		public static string GetUldInfoForAFlight(string flightUrNo, out int uldCntForSIN, out int uldCntForInter)
		{
			string st = "";
			uldCntForInter = 0;
			uldCntForSIN = 0;
			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			IDbCommand cmd = null;
			DSCpm ds = new DSCpm();
			try
			{
				cmd = db.GetCommand(false);
				ds = DBDCpm.GetInstance().RetrieveCpmDetForAFlightForTrips(cmd, flightUrNo);
				/*Changed by Alphy to get interline count according to container code BT & TB*/
				foreach (DSCpm.CPMDETRow row in ds.CPMDET)
				{
					if (row.CONT != "")
					{
						if (row.CONT.Trim().ToUpper().StartsWith("BT") || row.CONT.Trim().ToUpper().StartsWith("T"))
						{

							uldCntForInter++;
						}

						else
						{
							uldCntForSIN++;
						}

					}
					else
					{
						uldCntForSIN++;
					}

				}
				if (ds.CPMDET.Rows.Count > 0)
				{
					st = "ULD SIN:" + uldCntForSIN + " INTER:" + uldCntForInter;
				}

			}
			catch (Exception)
			{

				throw;
			}
			finally
			{
				db.CloseCommand(cmd, commit);
			}
			return st;
		}



		public static DSCpm RetrieveUnSentCPMUldForAFlight(string flightUrNo)
		{

			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			IDbCommand cmd = null;
			DSCpm ds = new DSCpm();
			try
			{
				cmd = db.GetCommand(false);
				// CheckExistCPM(cmd,flightUrNo);

				ds = DBDCpm.GetInstance().RetrieveUnSentCPMUldForAFlight(cmd, flightUrNo);
			}
			catch (Exception)
			{

				throw;
			}
			finally
			{
				db.CloseCommand(cmd, commit);
			}
			return ds;

		}
		public static DSCpm RetrieveUldNos(string flightUrNo)
		{
			UtilDb db = UtilDb.GetInstance();

			IDbCommand cmd = null;
			DSCpm ds = new DSCpm();

			try
			{
				cmd = db.GetCommand(false);

				ds = ((DSCpm)DBDCpm.GetInstance().RetrieveCpmDetForAFlight(cmd, flightUrNo));
			}
			catch (Exception)
			{
				//throw new ApplicationException("Unable to get the  Messages" + ex.Message);
			}
			return ds;
		}



		public static EntDsCPM RetrieveUnSentCPMUldForAFlightForAO(string flightUrNo)
		{
			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			IDbCommand cmd = null;
			DSCpm ds = new DSCpm();
			EntDsCPM ent = new EntDsCPM();
			try
			{
				cmd = db.GetCommand(false);
				//CheckExistCPM(cmd,flightUrNo);
				ds = DBDCpm.GetInstance().RetrieveUnSentCPMUldForAFlightForTrips(cmd, flightUrNo);
				if (ds == null)
				{
					ent.DsCPM = new DSCpm();
				}
				else
				{
					ent.DsCPM = (DSCpm)ds.Copy();
				}
			}
			catch (Exception)
			{

				throw;
			}
			finally
			{
				db.CloseCommand(cmd, commit);
			}
			return ent;
		}

		public static EntDsCPM RetrieveUnSentCPMUldForAFlightForTrips(string flightUrNo)
		{
			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			IDbCommand cmd = null;
			DSCpm ds = new DSCpm();
			EntDsCPM ent = new EntDsCPM();
			try
			{
				cmd = db.GetCommand(false);
				//CheckExistCPM(flightUrNo);
				ent.DsCPM = DBDCpm.GetInstance().RetrieveUnSentCPMUldForAFlightForTrips(cmd, flightUrNo);
			}
			catch (Exception)
			{

				throw;
			}
			finally
			{
				db.CloseCommand(cmd, commit);
			}
			return ent;
		}

        //public static DSCpm RetrieveSentCPMUldForAFlightForTrips(string flightUrNo)
        //{
        //    UtilDb db = UtilDb.GetInstance();
        //    bool commit = false;
        //    IDbCommand cmd = null;
        //    DSCpm ds = new DSCpm();
			
        //    try
        //    {
        //        cmd = db.GetCommand(false);
        //        //CheckExistCPM(flightUrNo);
        //        ds = DBDCpm.GetInstance().RetrieveSentCPMUldForAFlightForTrips(cmd, flightUrNo);
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //    finally
        //    {
        //        db.CloseCommand(cmd, commit);
        //    }
            
        //    return ent;
        //}

       

		public static EntDsCPM RetrieveUnSentCPMUldForAFlightForTripsForBrs(string flightUrNo)
		{
			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			IDbCommand cmd = null;
			DSCpm ds = new DSCpm();
			EntDsCPM ent = new EntDsCPM();
			try
			{
				cmd = db.GetCommand(false);
				//CheckExistCPM(flightUrNo);
				ent.DsCPM = DBDCpm.GetInstance().RetrieveUnSentCPMUldForAFlightForTripsForBrs(cmd, flightUrNo);
			}
			catch (Exception)
			{

				throw;
			}
			finally
			{
				db.CloseCommand(cmd, commit);
			}
			return ent;
		}

		//public static DataTable RetrieveUnSentCPMandTripsInfo(string flightUrno)
		//{
		//    UtilDb db = UtilDb.GetInstance();
		//    bool commit = false;
		//    IDbCommand cmd = null;
		//    DSCpm ds = new DSCpm();
		//    EntDsCPM ent = new EntDsCPM();
		//    try
		//    {
		//       cmd = db.GetCommand(true);  
		//    CheckExistCPM(flightUrno);
		//    return CtrlDTrip.RetrieveUnSentCPMandTripsInfo(flightUrno);
		//}


		public static EntDsCPM RetrieveEntDsCpmForAFlight(string flightUrNo)
		{
			EntDsCPM ent = new EntDsCPM();
			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			IDbCommand cmd = null;


			try
			{
				cmd = db.GetCommand(false);
				//ent.DsCPM = CheckExistCPM(cmd, flightUrNo);
				ent.DsCPM = DBDCpm.GetInstance().RetrieveCpmDetForAFlight(cmd, flightUrNo);
			}
			catch (Exception)
			{

				throw;
			}
			finally
			{
				db.CloseCommand(cmd, commit);
			}

			return ent;
		}

		public static DSCpm RetrieveUldNoNotSend(string uaft)
		{
			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			IDbCommand cmd = null;
			DSCpm ds = new DSCpm();
			try
			{
				cmd = db.GetCommand(false);
				ds = ((DSCpm)DBDCpm.GetInstance().RetrieveUnSentCPMUldForAFlight(cmd, uaft));
			}
			catch (Exception)
			{
				//throw new ApplicationException("Unable to get the  Messages" + ex.Message);
			}
			finally
			{
				db.CloseCommand(cmd, commit);
			}
			return ds;
		}


		public static string RequestCPM(string flightId)
		{
			string iTrekToSoccQueueName = FromItrekToSoccQueueName;

			ITrekWMQClass1 iTWMQ = new ITrekWMQClass1();
			string msg = "<LOA>" + flightId + "</LOA>";
			string msgId = iTWMQ.putTheMessageIntoQueue(msg, iTrekToSoccQueueName);
			return msgId;
		}

		public static DSCpm CheckExistCPMForAFlight(IDbCommand cmd, string flightUrNo)
		{
			DBDCpm db = DBDCpm.GetInstance();
			DSCpm tempDs = new DSCpm();
			if (!db.IsCPMExist(cmd, flightUrNo, out tempDs))
			{
				RequestCPM(CtrlFlight.GetUaftForFlightId(cmd, flightUrNo));

			}
			return tempDs;
		}


		public static void CheckExistCPM(string flightUrNo)
		{
			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			IDbCommand cmd = null;
			try
			{
				cmd = db.GetCommand(false);
				CheckExistCPMForAFlight(cmd, flightUrNo);
			}
			catch (Exception)
			{
				//throw new ApplicationException("Unable to get the  Messages" + ex.Message);
			}
			finally
			{
				db.CloseCommand(cmd, commit);
			}
		}
		private static string FromItrekToSoccQueueName
		{
			get
			{
				iTXMLPathscl iTPaths = new iTXMLPathscl();
				return iTPaths.GetFROMITREKCPMQueueName();
			}
		}

		private static string FromSoccToItrekQueueName
		{
			get
			{
				iTXMLPathscl iTPaths = new iTXMLPathscl();
				return iTPaths.GetTOITREKCPMQueueName();
			}
		}


		public static void InsertNewContainerIntoDB(string flightUrno, ArrayList lstUld, string userId)
		{

			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			IDbCommand cmd = null;
			try
			{
				cmd = db.GetCommand(true);
				DateTime curDt = ITrekTime.GetCurrentDateTime();
				if (!DBDCpm.GetInstance().InsertNewContainerIntoDB(cmd, flightUrno, lstUld, userId, curDt))
				{
					throw new ApplicationException("Fail to Save the Timing.");
				}
				commit = true;
			}
			catch (Exception)
			{

				throw;
			}
			finally
			{
				db.CloseCommand(cmd, commit);
			}
		}
		private static bool DoProcessInsertCpm(IDbCommand cmd, XmlDocument xmlDoc)
		{
			bool isOk = false;

			try
			{
				DateTime curDt = ITrekTime.GetCurrentDateTime();
				isOk = DBDCpm.GetInstance().InsertACpm(cmd, xmlDoc, curDt);
			}
			catch (Exception ex)
			{
				throw new ApplicationException("Fail to Insert the CPM." + ex.Message);
			}
			return isOk;
		}

		private static bool DoProcessInsertBrs(IDbCommand cmd, XmlDocument xmlDoc)
		{


			bool isOk = false;

			try
			{
				DateTime curDt = ITrekTime.GetCurrentDateTime();
				isOk = DBDCpm.GetInstance().InsertABrs(cmd, xmlDoc, curDt);

			}
			catch (Exception ex)
			{
				throw new ApplicationException("Fail to Insert the BRS." + ex.Message);
			}
			return isOk;
		}


		private static bool DoProcessUpdateBrs(IDbCommand cmd, XmlDocument xmlDoc)
		{


			bool isOk = false;

			try
			{
				DateTime curDt = ITrekTime.GetCurrentDateTime();
				isOk = DBDCpm.GetInstance().UpdateBRSFieldsToDB(cmd, xmlDoc, curDt);

			}
			catch (Exception ex)
			{

				throw new ApplicationException("Fail to Update the BRS." + ex.Message);
			}
			return isOk;
		}



		public static void DoProcessUpdateSendIndicator(IDbCommand cmd, ArrayList lstUld, string flnu, string sentBy)
		{


			UtilDb db = UtilDb.GetInstance();


			try
			{

				DateTime curDt = ITrekTime.GetCurrentDateTime();

				if (!DBDCpm.GetInstance().UpdateSendIndicator(cmd, lstUld, flnu, sentBy, curDt))
				{
					throw new ApplicationException("Fail to Save the Timing.");
				}

			}
			catch (Exception)
			{

				throw;
			}



		}
		public static bool ProcessCPMMsg(IDbCommand cmd, string msg)
		{
			bool isOk = false;
			try
			{
				XmlDocument xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(msg);
				isOk = DoProcessInsertCpm(cmd, xmlDoc);

			}
			catch (Exception ex)
			{
				throw new ApplicationException("Fail to ProcessCPMMsg" + ex.Message);
			}
			return isOk;
		}
		public static bool ProcessBRSMsg(IDbCommand cmd, string msg)
		{
			bool isOk = false;
			try
			{

				XmlDocument xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(msg);
				isOk = DoProcessInsertBrs(cmd, xmlDoc);

			}
			catch (Exception ex)
			{
				throw new ApplicationException("Fail to ProcessBRSMsg" + ex.Message);
			}
			return isOk;
		}

		public static bool ProcessBRSUpdateMsg(IDbCommand cmd, string msg)
		{
			bool isOk = false;
			try
			{

				XmlDocument xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(msg);
				isOk = DoProcessUpdateBrs(cmd, xmlDoc);

			}
			catch (Exception ex)
			{
				throw new ApplicationException("Fail to ProcessBRSUpdateMsg" + ex.Message);
			}
			return isOk;
		}

		public static bool ProcessInMsgs(IDbCommand cmd, string message, string messageType)
		{
			bool isOk = false;

			try
			{
				if (message != "")
				{

					if (messageType == "CPMS")
					{
						isOk = ProcessCPMMsg(cmd, message);
					}
					else if (messageType == "BRSS")
					{
						isOk = ProcessBRSMsg(cmd, message);
					}
					else if (messageType == "BRS")
					{
						isOk = ProcessBRSUpdateMsg(cmd, message);
					}
					else
					{
						// iTJobMgrLog.WriteEntry("Invalid Message Type:" + messageType);
						//do NOTHING
					}
					



				}

			}
			catch (Exception ex)
			{

				throw new ApplicationException("Fail to ProcessInMsgs" + ex.Message);
			}

			return isOk;

		}

		public static DSCpm RetrieveUnSentCPMUldForAFlight(string flightUrNo, bool checkCPMExist)
		{
			if (checkCPMExist)
				CheckExistCPM(flightUrNo);
			return RetrieveUnSentCPMUldForAFlight(flightUrNo);
		}

		public static string RetrieveCPMForAFlight(string flightUrNo, string flightNo)
		{
			DBDCpm dbCpm = DBDCpm.GetInstance();
			//string st = "";
			StringBuilder sb = new StringBuilder("");
			try
			{
				DSCpm ds = RetrieveCPMUldForAFlight(null, flightUrNo);
				foreach (DSCpm.CPMDETRow row in ds.CPMDET.Select("", "POSF,DSSN,ULDN"))
				{
					//st += row.ULD_TXT + "\n";
					sb.Append(row.ULD_TXT + "\n");
				}
			}
			catch (ApplicationException ex)
			{
				throw;
			}
			catch (Exception ex)
			{
				throw new ApplicationException("Unable to get the CPM Message for flight " + flightNo + ", " + flightUrNo + " due to " + ex.Message);
			}
			return sb.ToString();
			//return st;
		}

		public static DSCpm RetrieveCPMUldForAFlight(IDbCommand cmd, string flightUrNo)
		{
			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			bool isNewCmd = false;
			DSCpm ds = null;
			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = db.GetCommand(false);
				}
				CheckExistCPMForAFlight(cmd, flightUrNo);
				ds = DBDCpm.GetInstance().RetrieveCpmDetForAFlight(cmd, flightUrNo);
			}
			catch (Exception ex)
			{
				LogMsg("RetrieveCPMUldForAFlight:Err:" + flightUrNo + "," + ex.Message);
			}
			finally
			{
				if (isNewCmd)
				{
				db.CloseCommand(cmd, commit);
				}
			}
			if (ds == null) ds = new DSCpm();
			return ds;
		}

		#region Logging
		private static void LogTraceMsg(string msg)
		{
			LogMsg(msg);
		}

		private static void LogMsg(string msg)
		{
			//UFIS.ITrekLib.Util.UtilLog.LogToGenericEventLog("CtrlFlight", msg);
			try
			{
				MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("CtrlFlight", msg);
			}
			catch (Exception)
			{
				UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("CtrlFlight", msg);
			}
		}
		#endregion

	}
}