#define USING_MQ
#define TESTING_ON
#define TRACING_ON


using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Data;

using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.Util;
using iTrekXML;
using iTrekWMQ;


namespace UFIS.ITrekLib.DB
{
    public class DBBagPresentTimeSummaryReport
    {
        private static Object _lockObj = new Object();//to use in synchronisation of single access
        private static DBBagPresentTimeSummaryReport _dbBagPresentTimeSummaryReport = null;
        private static DateTime _lastWriteDT = new System.DateTime(1, 1, 1);

        private static string _fnFlightSummXml = null;

        /// <summary>
        /// Singleton Pattern.
        /// Not allowed to create the object from other class.
        /// To get the object, user "GetInstance()" method.
        /// </summary>
        private DBBagPresentTimeSummaryReport() { }

        public void Init()
        {//Initialize all the information, to get the latest data when fetching them again.
            _fnFlightSummXml = null;
            _lastWriteDT = new System.DateTime(1, 1, 1);
        }

        /// <summary>
        /// Get the Instance of DBBagPresentTimeSummaryReport. There will be only one reference for all the clients
        /// </summary>
        /// <returns></returns>
        public static DBBagPresentTimeSummaryReport GetInstance()
        {
            if (_dbBagPresentTimeSummaryReport == null)
            {
                _dbBagPresentTimeSummaryReport = new DBBagPresentTimeSummaryReport();
            }
            return _dbBagPresentTimeSummaryReport;
        }

        private string fnFlightSummXml
        {
            get
            {
                if ((_fnFlightSummXml == null) || (_fnFlightSummXml == ""))
                {
                    iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
                    _fnFlightSummXml = path.GetSummFlightsFilePath();
                }

                return _fnFlightSummXml;
            }
        }

        private string GetFromITrekToSoccReportQueueName()
        {
            iTXMLPathscl paths = new iTXMLPathscl();
            return paths.GetFROMITREKRPTQueueName();
        }

        private static string GetFromSoccToITrekReportQueueName()
        {
            iTXMLPathscl paths = new iTXMLPathscl();
            return paths.GetTOITREKRPTQueueName();
        }

        public void UpdateAllFlightBMToBackend(int keepDays)
        {

            FileInfo fiXml = new FileInfo(fnFlightSummXml);
            DateTime curDT = fiXml.LastWriteTime;

            if (_lastWriteDT != curDT)
            {
                lock (_lockObj)
                {
                    try
                    {
                        curDT = fiXml.LastWriteTime;
                        if (_lastWriteDT != curDT)
                        {
                            LogTraceMsg("UpdateAllFlightBMToBackend");
                            DateTime to = DateTime.Now;
                            DateTime fr;
                            if (keepDays > 0) keepDays = 0 - keepDays;
                            else if (keepDays == 0) keepDays = -3;
                            fr = to.AddDays(keepDays);
                            DSFlight ds = CtrlFlight.RetrieveFlights("", fr, to);
                            foreach (DSFlight.FLIGHTRow row in ds.FLIGHT.Select(""))
                            {
                                string flightUrno = row.URNO;
                                try
                                {
                                    string flightNo = row.FLNO;

                                    UpdateFlightInfoToBackend(flightUrno,
                                        flightNo, row.A_D,
                                        row.ST, row.AT,
                                        row.TERMINAL, row.L_H,
                                        row.BG_FST_BM_MET, row.AP_FST_BM_MET,
                                        row.BG_LST_BM_MET, row.AP_LST_BM_MET);
                                }
                                catch (Exception ex)
                                {
                                    LogMsg("UpdateAllFlightBMToBackend:Error:Flight Urno:" + flightUrno + "==>" + ex.Message);
                                }
                            }
                            _lastWriteDT = curDT;
                        }

                    }
                    catch (Exception ex)
                    {
                        LogMsg("UpdateAllFlightBMToBackend:Err:" + ex.Message);
                    }
                }
            }
        }

        public void UpdateFlightInfoToBackend(string flightUrno,
            string flightNo, string ad,
            string stUfisTime, string atUfisTime,
            string terminalId, string lightOrHeavyLoader,
            string baggageFirstTimingMet, string apronFirstTimingMet,
            string baggageLastTimingMet, string apronLastTimingMet
            )
        {
            //         1. Save the data to backend
            //through MQ Queue ITREK.ITREK.SOCC.RPT
            //<BPSR_UPD>
            //   <UAFT></UAFT>		//Flight URNO
            //   <AD><AD>			//Arrival - 'A', 
            //             //Departure - 'D'
            //   <FLNO></FLNO>		//Flight Number
            //   <ST></ST>			//Schedule Time
            //   <AT></AT>			//Actual Time
            //   <T></T>			//Terminal Id
            //   <LH></LH>			//Light Loader - 'L'
            //                             // or Heavy Loader - 'H'
            //   <BFT></BFT>               //Baggage First Timing Met. 
            //   <AFT></AFT>               //Apron First Timing Met
            //   <BLT></BLT>               //Baggage Last Timing Met
            //   <ALT></ALT>               //Apron Last Timing Met
            //</BPSRS_UPD>

            //Note :  Timing Met ==> 'P'
            //        Timing Not Met ==> Other
            string xmlMsg = "<BPSR_UPD>" +
                "<UAFT>" + flightUrno + "</UAFT>" +
                "<AD>" + ad + "</AD>" +
                "<FLNO>" + flightNo + "</FLNO>" +
                "<ST>" + stUfisTime + "</ST>" +
                "<AT>" + atUfisTime + "</AT>" +
                "<T>" + terminalId + "</T>" +
                "<LH>" + lightOrHeavyLoader + "</LH>" +
                "<BFT>" + baggageFirstTimingMet + "</BFT>" +
                "<AFT>" + apronFirstTimingMet + "</AFT>" +
                "<BLT>" + baggageLastTimingMet + "</BLT>" +
                "<ALT>" + apronLastTimingMet + "</ALT>" +
                "</BPSR_UPD>";
            LogTraceMsg(xmlMsg);
            SendXmlMsgToBackend(xmlMsg);
        }

        [Conditional("USING_MQ")]
        public void SendXmlMsgToBackend(string xmlMsg)
        {
            ITrekWMQClass1 mq = new ITrekWMQClass1();
            mq.putTheMessageIntoQueue(xmlMsg, GetFromITrekToSoccReportQueueName());
        }

        //[Conditional("TRACING_ON")]
        private void LogTraceMsg(string msg)
        {
           try
           {
              MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("DBBagPresentTimeSummaryReport", msg);
           }
           catch (Exception)
           {
              UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DBBagPresentTimeSummaryReport", msg);
           }
        }

        [Conditional("TESTING_ON")]
        private void LogMsg(string msg)
        {
            //UtilLog.LogToGenericEventLog("DBBagPresentTimeSummaryReport", msg);
           LogTraceMsg(msg);
        }

    }
}