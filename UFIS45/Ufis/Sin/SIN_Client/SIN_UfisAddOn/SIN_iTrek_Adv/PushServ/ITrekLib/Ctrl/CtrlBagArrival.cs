using System;
using System.Collections.Generic;
using System.Text;

using System.Data;
using System.Collections;

using UFIS.ITrekLib.DB;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.Util;
using MM.UtilLib;
using UFIS.ITrekLib.Misc;

namespace UFIS.ITrekLib.Ctrl
{
	public class CtrlBagArrival
	{
		public const string CSS_HIGHLIGHT = "Highlight";
		private static readonly DateTime DEFAULT_DT = new DateTime(1, 1, 1);

		#region Get x Minutes to Highlight
		public static int GetXMinutesToHighlightTrip()
		{
			//#warning TO DO - To get from config file.
			int xMinutes = 10;
			xMinutes = CtrlConfig.GetWebBAHighlightTripMINUTES();
			if (xMinutes < 0) xMinutes = Math.Abs(xMinutes);
			return xMinutes;
		}

		/// <summary>
		/// Time in Minutes to highlight the expected containers (Unsent Ulds)
		/// </summary>
		/// <returns>Time in Minutes</returns>
		public static int GetXMinutesToHighlightUnsentUld()
		{
			//#warning TO DO - To get from config file.
			int xMinutes = 8;
			xMinutes = CtrlConfig.GetWebBAHighlightUnsentUldMINUTES();
			if (xMinutes < 0) xMinutes = Math.Abs(xMinutes);
			return xMinutes;
		}

		/// <summary>
		/// Time in Minutes to Highlight First Bag
		/// </summary>
		/// <returns>Time in Minutes</returns>
		public static int GetXMinutesToHighlightFirstBag()
		{//TO DO - To get the time from config.
			int xMinutes = 1;
			xMinutes = CtrlConfig.GetWebBAHighlightFirstBagMINUTES();
			if (xMinutes < 0) xMinutes = Math.Abs(xMinutes);
			return xMinutes;
		}

		public static int GetXMinutesToHighlightLastBag()
		{
			//#warning TO DO - To get the information from config file.
			int xMinutes = 3;
			xMinutes = CtrlConfig.GetWebBAHighlightLastBagMINUTES();
			if (xMinutes < 0) xMinutes = Math.Abs(xMinutes);
			return xMinutes;
		}

		#endregion

		public static void HighlightData(string flightId, EnBagArrType bagArrType,
			ref DSFlight rdsFlight, ref DSTrip rdsTrip,
			out bool highlightUnsentUld,
			out bool highlightFirstBag, out bool highlightLastBag)
		{
			highlightFirstBag = false;
			highlightLastBag = false;
			highlightUnsentUld = false;

			rdsFlight = CtrlFlight.RetrieveFlightsByFlightId(flightId);
			DSCpm dsCpm = null;
			string st = GetTripNContainerInfo(flightId, bagArrType, ref rdsTrip, ref dsCpm);
		}

		private static bool HasToAddAsUnSentContainer(DSCpm.CPMDETRow row, EnBagArrType bagArrType)
		{
			bool add = false;
			if (bagArrType == EnBagArrType.All) add = true;
			else if (!String.IsNullOrEmpty(row.CONT))
			{
				string stCont = row.CONT.Trim().ToUpper();
				if (stCont.StartsWith("BT") || stCont.StartsWith("T"))
				{
					if (bagArrType == EnBagArrType.Inter) add = true;
				}
				else
				{
					if (bagArrType == EnBagArrType.Local) add = true;
				}
			}
			else
			{
				if (bagArrType == EnBagArrType.Local) add = true;
			}
			return add;
		}

		public static void GetTripInfo(string flightId, EnBagArrType bagArrType, ref DSTrip dsTripResult)
		{
			dsTripResult = CtrlTrip.RetrieveTripForAFlight(null, flightId, EnBagArrRecvCategory.All, bagArrType);
		}

		/// <summary>
		/// Get Trip and Container Information for giving FlightId, baggage arrival type
		/// </summary>
		/// <param name="flightId">Flight Id</param>
		/// <param name="bagArrType">Baggage Arrival Type (1.Local, 2.InterLine, 3.All)</param>
		/// <param name="dsTripResult">DSTrip result</param>
		/// <param name="dsCpmResult">DSCpm result</param>
		/// <returns>Message String. If Everything is ok, return "".</returns>
		public static string GetTripNContainerInfo(string flightId, EnBagArrType bagArrType, ref DSTrip dsTripResult, ref DSCpm dsCpmResult)
		{
			string returnMsg = "";
			bool hasTrip = false;

			dsTripResult = CtrlTrip.RetrieveTripForAFlight(null, flightId, EnBagArrRecvCategory.All, bagArrType);

			//dsTripResult = DBDTrip.GetInstance().RetrieveTripForAFlight(flightId, EnBagArrRecvCategory.All, bagArrType);
			if ((dsTripResult != null) && (dsTripResult.TRIP != null) && (dsTripResult.TRIP.Rows != null) && (dsTripResult.TRIP.Rows.Count > 0))
				hasTrip = true;
			//string stSel = "";
			string stSelCpm = "";
			//switch (bagArrType)
			//{
			//   case EnBagArrType.Inter:
			//      //stSel = "DES='I'";//INTER
			//      //stSelCpm = "(APC3 IS NULL OR APC3<>'SIN')";
			//      break;
			//   case EnBagArrType.Local:
			//      //stSel = "DES='L'";//LOCAL
			//      //stSelCpm = "APC3='SIN'";
			//      break;
			//   case EnBagArrType.None:
			//      break;
			//   default:
			//      break;
			//}

            DSCpm dsCpm = CtrlCPM.RetrieveUnSentCPMUldForAFlight(flightId);
			dsCpmResult = new DSCpm();
			if (dsCpm != null)
			{
				foreach (DSCpm.CPMDETRow row in dsCpm.CPMDET.Select(stSelCpm))
				{
					string uldn = row.ULDN;
					if ((uldn == null) || (uldn.Trim() == "")) continue;
					bool add = false;
					//if (bagArrType == EnBagArrType.All)
					//{
					//   if (!String.IsNullOrEmpty(row.CONT))
					//   {
					//      string stCont = row.CONT.Trim().ToUpper();
					//      if (stCont.StartsWith("BT") || stCont.StartsWith("T"))
					//      {
					//         if (bagArrType != EnBagArrType.Inter) add = false;
					//      }
					//      else
					//      {
					//         if (bagArrType != EnBagArrType.Local) add = false;
					//      }
					//   }
					//   else
					//   {
					//      if (bagArrType != EnBagArrType.Local) add = false;
					//   }
					//}
					add = HasToAddAsUnSentContainer(row, bagArrType);
					//LogTraceMsg("GetTripNContainerInfo: " + bagArrType + ",add:" + add + ", " + row.CONT);
					if (add)
					{
						bool addCpmDet = false;
						if (hasTrip)
						{
							DSTrip.TRIPRow[] tripRows = (DSTrip.TRIPRow[])dsTripResult.TRIP.Select("ULDN='" + uldn + "'");
							if ((tripRows == null) || (tripRows.Length < 1))
							{//this uld is not in the trip yet.
								addCpmDet = true;
							}
						}
						else
						{
							addCpmDet = true;
						}
						if (addCpmDet)
							dsCpmResult.CPMDET.ImportRow(row);
					}
				}
			}
			return returnMsg;
		}

		private static readonly char[] ULD_DELIMITER = { '/' };

		public static EntBagArrContainer GetTripInfo(string flightUrno, EnBagArrType bagArrType)
		{
			//LogTraceMsg("GetTripInfo:" + flightUrno + ", " + bagArrType);
			EntBagArrContainer entBagArr = new EntBagArrContainer();
			//DSTrip dsTrip = CtrlTrip.RetrieveTripNotReceived(flightUrno);
			DSTrip dsTrip = CtrlTrip.RetrieveTrip(flightUrno);
			if (dsTrip == null)
			{
				LogTraceMsg("GetTripInfo:NoTrip:" + flightUrno);
				dsTrip = new DSTrip();
			}
			string stSel = "";
			string stSelCpm = "CG='N'";
			switch (bagArrType)
			{
				case EnBagArrType.Inter:
					stSel = "DES='I'";
					//stSelCpm += " AND (APC3 IS NULL OR APC3<>'SIN')";
					break;
				case EnBagArrType.Local:
					stSel = "DES='L'";
					//stSelCpm += " AND APC3='SIN'";
					break;
				case EnBagArrType.None:
					break;
				default:
					break;
			}
			//LogTraceMsg("GetTripInfo:Has Trip.Cnt:" + dsTrip.TRIP.Rows.Count + ",SelCnt:" + 
			//    dsTrip.TRIP.Select(stSel).Length +"," + stSelCpm);
			foreach (DSTrip.TRIPRow row in dsTrip.TRIP.Select(stSel))
			{
				//entBagArr.AddTrip(row.STRPNO, row.URNO, row.ULDN, row.SENT_RMK, row.SENT_DT, row.RCV_RMK, row.RCV_DT);
				entBagArr.AddTrip(row.STRPNO, row.URNO, GetUldNo(row.ULDN), row.SENT_RMK, row.SENT_DT, row.RCV_RMK, row.RCV_DT);
			}

			DSCpm dsCpm = CtrlCPM.RetrieveUnSentCPMUldForAFlight(flightUrno);
			if ((dsCpm != null) && (dsCpm.CPMDET != null) && (dsCpm.CPMDET.Rows != null) && (dsCpm.CPMDET.Rows.Count > 0))
			{
				//LogTraceMsg("GetTripInfo:Has Unsent CPM." + dsCpm.CPMDET.Rows.Count + "," + stSelCpm);
				foreach (DSCpm.CPMDETRow row in dsCpm.CPMDET.Select(stSelCpm))
				{
					bool add = false;
					//if (bagArrType == EnBagArrType.All) add = true;
					//else if (!String.IsNullOrEmpty(row.CONT))
					//{
					//   string stCont = row.CONT.Trim().ToUpper();
					//   if (stCont.StartsWith("BT") || stCont.StartsWith("T"))
					//   {
					//      if (bagArrType == EnBagArrType.Inter) add = true;
					//   }
					//   else
					//   {
					//      if (bagArrType == EnBagArrType.Local) add = true;
					//   }
					//}
					//else
					//{
					//   if (bagArrType == EnBagArrType.Local) add = true;
					//}
					add = HasToAddAsUnSentContainer(row, bagArrType);

					//LogTraceMsg("GetTripInfo: " + bagArrType + ",add:" + add + ", " + row.CONT);
					if (add)
					{
						entBagArr.AddUnSentUld(row.URNO, GetUldNo(row.ULDN));
					}
				}
			}
			else
			{
				//LogTraceMsg("GetTripInfo:No Unsent CPM.");
			}
			return entBagArr;
		}

		public static string GetUldNo(string uldText)
		{
			string uldNo = uldText + "";
			if (uldNo.Contains("/"))
			{
				char[] del = { '/' };
				string[] stArr = uldNo.Split(del, 3);
				if ((stArr != null) && (stArr.Length > 1))
				{
					uldNo = stArr[1];
					if ((string.IsNullOrEmpty(uldNo)) || (uldNo.Trim() == ""))
					{
						uldNo = uldText;
					}
				}
			}
			return uldNo;
		}

		public static void GetFlightTimeFrame(ref DateTime frDateTime, ref DateTime toDateTime)
		{
			DateTime dtNow = DateTime.Now;
			//#warning TO DO - to place the -2 and +2 in Settings
			//frDateTime = dtNow.AddHours(-2);
			//toDateTime = dtNow.AddHours(+2);

			frDateTime = dtNow.AddMinutes(CtrlConfig.GetWebBAFromMinutes());
			toDateTime = dtNow.AddMinutes(CtrlConfig.GetWebBAToMinutes());
		}

		public static DSFlight GetArrivalFlights(string terminalId)
		{
			DateTime frDateTime = DateTime.Now;
			DateTime toDateTime = DateTime.Now;
			//GetFlightTimeFrame(ref frDateTime, ref toDateTime);

			EntWebFlightFilter filter = new EntWebFlightFilter();
			filter.DateFilterType = EnFlightDateFilterType.UseOBLETST;
			filter.Dept = EnUserDept.Baggage;
			filter.FlightType = "A";
			filter.TerminalIds = terminalId;
			//filter.UseFrToFixedDateTime = true;
			//filter.FrFixedDateTime = frDateTime;
			//filter.ToFixedDateTime = toDateTime;
			filter.FrMinute = CtrlConfig.GetWebBAFromMinutes();
			filter.ToMinute = CtrlConfig.GetWebBAToMinutes();
			DSFlight ds = CtrlFlight.RetrieveFlightsForWeb(filter);

			//DSFlight ds = CtrlFlight.RetrieveArrivalFlights(terminalId, frDateTime, toDateTime, EnFlightDateFilterType.UseOBLETST);
			//LogTraceMsg(string.Format("GetArrivalFlights:Fr{0:yyMMddHHmm}, To{1:yyMMddHHmm}, Terminal-{2}, Cnt-{3}",
			//   frDateTime, toDateTime, terminalId, ds.FLIGHT.Rows.Count));
			return ds;
		}

		private static void LogMsg(string msg)
		{
			try
			{
				MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("CtrlBagArrival", msg);
			}
			catch (Exception)
			{
				UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("CtrlBagArrival", msg);
			}
		}

		private static void LogTraceMsg(string msg)
		{
			LogMsg(msg);
		}

		/// <summary>
		/// To Highlight Expected Containers when Apron fails to send its first trip X minutes after thumbs up.
		/// </summary>
		/// <param name="flightId">Flight Id</param>
		/// <param name="pdsFlight">Flight Dataset</param>
		/// <param name="pdsTrip">Trip Dataset for the flight</param>
		/// <returns>True-Highlight, Else-Not to highlight</returns>
		public static bool HighlightExpectedContainers(string flightId,
			ref DSFlight pdsFlight, ref DSTrip pdsTrip)
		{
			bool highlight = false;
			if (!HasApronSentTrip(flightId, ref pdsTrip))
			{
				DateTime refTime;
				GetFlightInfo(flightId, ref pdsFlight);
				foreach (DSFlight.FLIGHTRow row in pdsFlight.FLIGHT.Select("URNO='" + flightId + "'"))
				{
					string stRefTime = row.TBS_UP;
					if (!string.IsNullOrEmpty(stRefTime))
					{
						refTime = UtilTime.ConvUfisTimeStringToDateTime(stRefTime);
						if (refTime.AddMinutes(GetXMinutesToHighlightUnsentUld()).CompareTo(DateTime.Now) < 0) highlight = true;
						break;
					}
				}
			}

			return highlight;
		}

		/// <summary>
		/// Highlight UnReceived Trip when Baggage fails to acknowledged received
		/// X Minutes after Apron Sent Trip Time.
		/// </summary>
		/// <param name="entBAUnRecvTrip">Entity of BagArrUnReceivedTrip</param>
		/// <param name="errMsg">error message (if any)</param>
		/// <returns>true==>highlight</returns>
		public static bool HighlightUnRecvTrip(EntBagArrUnReceivedTrip entBAUnRecvTrip, out string errMsg)
		{
			bool highlight = false;
			int xMinutes = 10;
			errMsg = "";
			try
			{
				xMinutes = CtrlConfig.GetWebBAHighlightTripMINUTES();
			}
			catch (Exception)
			{
				errMsg += "Trip Highlight Time was not set. Please inform to administrator.";
			}

			if (entBAUnRecvTrip.IsMoreThanXMinutes(xMinutes))
			{
				highlight = true;
			}
			return highlight;
		}

		/// <summary>
		/// Highlight First Trip, when it fails to be triggered X Minutes after acknowledged receive of first container
		/// </summary>
		/// <param name="flightId">Flight Id</param>
		/// <param name="pdsFlight">Flight Dataset</param>
		/// <param name="pdsTrip">Trip Dataset</param>
		/// <returns>True==>Highlight</returns>
		public static bool HighlightFirstBag(string flightId,
			ref DSFlight pdsFlight, ref DSTrip pdsTrip)
		{
			bool highlight = false;
			GetFlightInfo(flightId, ref pdsFlight);
			foreach (DSFlight.FLIGHTRow row in pdsFlight.FLIGHT.Select("URNO='" + flightId + "'"))
			{
				bool hasFirstBagTime = true;
				string stFirstBag = row.FST_BAG;
				if (string.IsNullOrEmpty(stFirstBag))
				{
					hasFirstBagTime = false;
				}
				else if (stFirstBag.Trim() == "") hasFirstBagTime = false;
				else if (stFirstBag == DBNull.Value.ToString()) hasFirstBagTime = false;

				if (!hasFirstBagTime)
				{
					DateTime firstRecvDateTime;
					if (HasRecvTrip(flightId, ref pdsTrip, out firstRecvDateTime))
					{
						LogTraceMsg("HighlightFirstBag:Has RecvTrip:" + flightId + string.Format("{0:dd MMM yyyy HH:mm:ss}", firstRecvDateTime));
						if (firstRecvDateTime.AddMinutes(GetXMinutesToHighlightFirstBag()).CompareTo(DateTime.Now) < 0)
						{
							highlight = true;
						}
						break;
					}
					else
					{
						LogTraceMsg("HighlightFirstBag:NoRecvTrip:" + flightId);
					}
				}
			}
			//LogTraceMsg("HighlightFirstBag:Highlight:" + flightId + "," + highlight);
			return highlight;
		}

		/// <summary>
		/// Highlight Last Trip, when it fails to be triggered X Minutes after acknowledged receive of last container
		/// </summary>
		/// <param name="flightId">Flight Id</param>
		/// <param name="pdsFlight">Flight Dataset</param>
		/// <param name="pdsTrip">Trip Dataset</param>
		/// <returns>True ==> Highlight</returns>
		public static bool HighlightLastBag(string flightId,
			ref DSFlight pdsFlight, ref DSTrip pdsTrip)
		{
			bool highlight = false;
			GetFlightInfo(flightId, ref pdsFlight);
			foreach (DSFlight.FLIGHTRow row in pdsFlight.FLIGHT.Select("URNO='" + flightId + "'"))
			{
				bool hasTime = true;
				string stTime = row.LST_BAG;
				if (string.IsNullOrEmpty(stTime))
				{
					hasTime = false;
				}
				else if (stTime.Trim() == "") hasTime = false;
				else if (stTime == DBNull.Value.ToString()) hasTime = false;

				if (!hasTime)
				{
					//LogTraceMsg("HighlightLastBag:No Time.");
					DateTime lastContainerRecvDt;
					GetTrips(flightId, ref pdsTrip);//To make sure to have the data
					foreach (DSTrip.TRIPRow tripRow in pdsTrip.TRIP.Select("UAFT='" + flightId + "' AND RTTY='L'"))
					{
						string stRecvTime = tripRow.RCV_DT;
						//LogTraceMsg("HighlightLastBag:RcvTime:" + stRecvTime);
						if ((stRecvTime != null) && (stRecvTime.Trim() != ""))
						{
							lastContainerRecvDt = UtilTime.ConvUfisTimeStringToDateTime(stRecvTime);
							if (lastContainerRecvDt.AddMinutes(GetXMinutesToHighlightLastBag()).CompareTo(DateTime.Now) < 0)
							{
								highlight = true;
							}
							break;
						}
					}
				}
				else
				{
					//LogTraceMsg("HighlightLastBag:HasTime " + stTime + "," + flightId);
				}

			}
			return highlight;
		}


		public static bool HasRecvTrip(string flightId, ref DSTrip pdsTrip, out DateTime firstRecvDateTime)
		{
			bool has = false;
			firstRecvDateTime = DEFAULT_DT;
			GetTrips(flightId, ref pdsTrip);
			if ((pdsTrip != null) && (pdsTrip.TRIP != null))
			{
				//foreach (DSTrip.TRIPRow row in pdsTrip.TRIP.Select("UAFT='" + flightId + "' AND RTRPNO='1'"))
				foreach (DSTrip.TRIPRow row in pdsTrip.TRIP.Select("UAFT='" + flightId + "'", "RCV_DT ASC"))
				{
					string stDateTime = row.RCV_DT;
					if ((stDateTime != null) && (stDateTime.Trim() != ""))
					{
						has = true;
						firstRecvDateTime = UtilTime.ConvUfisTimeStringToDateTime(stDateTime);
						break;
					}
				}
			}
			return has;
		}


		/// <summary>
		/// Has Apron Sent the Trips for Arrival Flight?
		/// </summary>
		/// <param name="flightId">flight Id</param>
		/// <param name="pdsTrip">Trip Dataset</param>
		/// <returns>True-Apron Sent a Trip</returns>
		private static bool HasApronSentTrip(string flightId, ref DSTrip pdsTrip)
		{
			bool has = false;
			GetTrips(flightId, ref pdsTrip);
			if ((pdsTrip != null) && (pdsTrip.TRIP != null))
			{
				DataRow[] rows = pdsTrip.TRIP.Select("UAFT='" + flightId + "'");
				if ((rows != null) && (rows.Length > 0)) has = true;
			}
			return has;
		}

		/// <summary>
		/// Get Flight Info for giving flight Id (To make sure info is there)
		/// </summary>
		/// <param name="flightId">Flight Id</param>
		/// <param name="pdsFlight">Flight Dataset</param>
		public static void GetFlightInfo(string flightId, ref DSFlight pdsFlight)
		{
			if (pdsFlight == null)
			{
				pdsFlight = CtrlFlight.RetrieveFlightsByFlightId(flightId);
			}
			else
			{
				DSFlight.FLIGHTRow[] rows = (DSFlight.FLIGHTRow[])pdsFlight.FLIGHT.Select("URNO='" + flightId + "'");
				if ((rows == null) || (rows.Length < 1))
				{
					DSFlight ds = CtrlFlight.RetrieveFlightsByFlightId(flightId);
					foreach (DSFlight.FLIGHTRow row in ds.FLIGHT.Rows)
					{
						pdsFlight.FLIGHT.LoadDataRow(row.ItemArray, true);
					}
				}
			}
		}

		public static void GetTrips(string flightId, ref DSTrip pdsTrip)
		{
			if (pdsTrip == null)
			{
				pdsTrip = CtrlTrip.RetrieveTrip(flightId);
			}
			else
			{
				DSTrip.TRIPRow[] rows = (DSTrip.TRIPRow[])pdsTrip.TRIP.Select("UAFT='" + flightId + "'");
				if ((rows == null) || (rows.Length < 1))
				{
					DSTrip ds = CtrlTrip.RetrieveTrip(flightId);
					foreach (DSTrip.TRIPRow row in ds.TRIP.Rows)
					{
						pdsTrip.TRIP.LoadDataRow(row.ItemArray, true);
					}
				}
			}
		}


		#region HasFirstBag
		public static bool HasFirstBag(string flightId,
			ref DSFlight pDsFlight,
			ref DSArrivalFlightTimings pDsArrFlTimings,
			out string firstBagTime)
		{
			bool has = false;
			firstBagTime = "";

			if (!has)
			{
				has = HasTimeInFlightTiming(flightId, ref pDsArrFlTimings, out firstBagTime, "FBAG");
			}

			if (!has)
			{
				has = HasTimeInFlight(flightId, ref pDsFlight, out firstBagTime, "FST_BAG");
			}

			return has;
		}
		#endregion

		#region HasLastBag
		public static bool HasLastBag(string flightId,
			ref DSFlight pDsFlight,
			ref DSArrivalFlightTimings pDsArrFlTimings,
			out string lastBagTime)
		{
			bool has = false;
			lastBagTime = "";

			if (!has)
			{
				has = HasTimeInFlightTiming(flightId, ref pDsArrFlTimings, out lastBagTime, "LBAG");
			}

			if (!has)
			{
				has = HasTimeInFlight(flightId, ref pDsFlight, out lastBagTime, "LST_BAG");
			}

			return has;
		}
		#endregion


		private static bool HasTimeInFlight(string flightId,
			ref DSFlight pDsFlight, out string time, string timeTag)
		{
			bool has = false;
			time = "";

			if ((flightId == null) || (flightId.Trim() == "")) throw new ApplicationException("No Flight Id.");

			DSFlight.FLIGHTRow[] fRows = null;
			if (pDsFlight == null)
			{
				pDsFlight = CtrlFlight.RetrieveFlightsByFlightId(flightId);
				fRows = (DSFlight.FLIGHTRow[])pDsFlight.FLIGHT.Select("URNO='" + flightId + "'");
			}
			else
			{
				fRows = (DSFlight.FLIGHTRow[])pDsFlight.FLIGHT.Select("URNO='" + flightId + "'");
				if ((fRows == null) || (fRows.Length < 1))
				{
					pDsFlight = CtrlFlight.RetrieveFlightsByFlightId(flightId);
					fRows = (DSFlight.FLIGHTRow[])pDsFlight.FLIGHT.Select("URNO='" + flightId + "'");
				}
			}

			if ((fRows == null) || (fRows.Length < 1)) throw new ApplicationException("Not able to get the Flight Information.");
			if (!(fRows[0][timeTag] is System.DBNull))
			{
				time = (string)(fRows[0][timeTag]);
			}
			if (string.IsNullOrEmpty(time))
			{
				has = false;
			}
			else has = true;

			return has;
		}

		public static bool HasTimeInFlightTiming(string flightId,
			ref DSArrivalFlightTimings pDsArrFlTimings,
			out string time, string timeTag)
		{
			bool has = false;
			time = "";

			if ((flightId == null) || (flightId.Trim() == "")) throw new ApplicationException("No Flight Id.");

			DSArrivalFlightTimings.AFLIGHTRow[] rows = null;
			if (pDsArrFlTimings == null)
			{
				pDsArrFlTimings = DBDFlight.GetInstance().RetrieveArrivalFlightTimings(flightId);
				rows = (DSArrivalFlightTimings.AFLIGHTRow[])
					pDsArrFlTimings.AFLIGHT.Select("URNO='" + flightId + "'");
			}
			else
			{
				rows = (DSArrivalFlightTimings.AFLIGHTRow[])
					pDsArrFlTimings.AFLIGHT.Select("URNO='" + flightId + "'");
				if ((rows == null) || (rows.Length < 1))
				{
					pDsArrFlTimings = DBDFlight.GetInstance().RetrieveArrivalFlightTimings(flightId);
					rows = (DSArrivalFlightTimings.AFLIGHTRow[])
					pDsArrFlTimings.AFLIGHT.Select("URNO='" + flightId + "'");
				}
			}
			if (((rows != null) && (rows.Length > 0)) && (!(rows[0][timeTag] is System.DBNull)))
			{
				time = (string)(rows[0][timeTag]);
				if (string.IsNullOrEmpty(time))
				{
					has = false;
				}
				else has = true;
			}

			return has;
		}

		/// <summary>
		/// Send Last Bag Timing
		/// </summary>
		/// <param name="flightId">Flight Id</param>
		/// <param name="stDateTime">Last Bag Timing in ufis date time format</param>
		public static void SendLastBagTiming(IDbCommand cmd, string flightId, string stDateTime, string userId)
		{

			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			bool isNewCmd = false;
			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = db.GetCommand(true);
				}
				DateTime curDt = ITrekTime.GetCurrentDateTime();

				//Dictionary<EnTripTypeSendReceive, string> de = new Dictionary<EnTripTypeSendReceive, string>();
				//de.Add(EnTripTypeSendReceive.RLTRPID, stDateTime);
				//CtrlTrip.UpdateTripTiming(cmd, flightId, de, userId);
				Hashtable ht = new Hashtable();
				string stLastTripTime = "";
                //if (NeedToUpdateLastTrip(flightId, userId, out stLastTripTime ))
                //{
                //    CtrlTrip.UpdateReceiveLastTripIdForTrpMst(cmd, flightId, userId);
                //    ht.Add(CtrlFlight.BA_LAST_TRIP_TIME_TAG, stLastTripTime);
                //}
				
				ht.Add(CtrlFlight.BA_LAST_BAG_TIMG_TAG, stDateTime);
				//CtrlFlight.SaveArrBagTimingByTag(cmd, flightId, userId, stDateTime, CtrlFlight.BA_LAST_TRIP_TIME_TAG);
				//CtrlFlight.SaveArrBagTimingByTag(flightId, LoginedId, stDateTime, CtrlFlight.BA_LAST_BAG_TIMG_TAG);
				CtrlFlight.UpdateArrivalFlightTiming(cmd, flightId, ht, userId);

				commit = true;
			}
			catch (Exception)
			{
				throw;
			}
			finally
			{
				if (isNewCmd)
				{
//					db.CloseCommand(cmd, commit);
					CtrlFlight.CloseCmdAndRefreshFlightAfterUpdFlightInfo(cmd, commit, flightId);
				}
			}
		}

        //private static bool NeedToUpdateLastTrip(string flightId, string userId, out string lstTripDateTime)
        //{
        //    lstTripDateTime = "";
        //    return CtrlTrip.NeedToUpdateLastTrip(flightId, userId, out lstTripDateTime);
        //    //bool needToUpdate = false;
        //    //lstTripDateTime = "";
            
        //    //try
        //    //{
        //    //    if (CtrlDTrip.RetrieveTrip(flightId).TRIP.Select("RTRPNO <> '1'").Length == 0 && CtrlDTrip.RetrieveTripNotReceived(flightId).TRIP.Count == 0)
        //    //    {
        //    //        DSArrivalFlightTimings dsArrFlightTiming = DBDFlight.GetInstance().RetrieveArrivalFlightTimings(flightId);

        //    //        DSArrivalFlightTimings.AFLIGHTRow row = null;
        //    //        if (dsArrFlightTiming.AFLIGHT.Rows.Count > 0)
        //    //        {
        //    //            row = ((DSArrivalFlightTimings.AFLIGHTRow)dsArrFlightTiming.AFLIGHT.Rows[0]);
        //    //        }
        //    //        string ltrip_new = "";
        //    //        string ltrip_old = "";
        //    //        try
        //    //        {
        //    //            ltrip_old = row._LTRIP_B;
        //    //        }
        //    //        catch (Exception)
        //    //        {
        //    //        }
        //    //        try
        //    //        {
        //    //            if (ltrip_old == "" || ltrip_old == "-")
        //    //            {
        //    //                ltrip_new = row._FTRIP_B;
        //    //            }
        //    //        }
        //    //        catch (Exception ex)
        //    //        {
        //    //            LogMsg("NeedToUpdateLastTrip:Err:" + flightId + "," + ex.Message);
        //    //            throw;
        //    //        }

        //    //        if (ltrip_new != "")
        //    //        {
        //    //            lstTripDateTime = ltrip_new;
        //    //            needToUpdate = true;
        //    //            //CtrlFlight.UpdateArrivalFlightTiming(null, flightId, "LTRIP-B", ltrip_new, StaffID);
        //    //        }
        //    //    }

        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    throw new ApplicationException("Unable to Update Baggage Last Trip for Arrival Flight." + ex.Message);
        //    //}

        //    //return needToUpdate;
        //}
	}
}