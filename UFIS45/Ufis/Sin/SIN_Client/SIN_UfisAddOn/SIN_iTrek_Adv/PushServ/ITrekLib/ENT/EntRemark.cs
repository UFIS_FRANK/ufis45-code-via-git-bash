using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

using UFIS.ITrekLib.DS;

namespace UFIS.ITrekLib.ENT
{
    public class EntRemark
    {
        ArrayList _rmkList;
        bool _selfEntrySelected = false;
        string _selfEntry;
        //DSPreDefBagConRmk _dsRmk;

        private string RMK_SEPERATOR = ",";

        public EntRemark()
        {
            _rmkList = new ArrayList();
            _selfEntrySelected = false;
            _selfEntry = "";
        }

        public int GetPreDefRmkCount()
        {
            int cnt = 0;
            if (_rmkList != null) cnt = _rmkList.Count;
            return cnt;
        }

        public void AddSelfEntryText(string selfEntryRemarkText)
        {
            _selfEntrySelected = true;
            _selfEntry = selfEntryRemarkText;
        }
        public bool HasSelfEntry()
        {
            return _selfEntrySelected;
        }
        public string GetSelfEntryText()
        {
            string st = "";
            if (_selfEntry!=null) st = _selfEntry;
            return st;
        }
        public void RemoveSelfEntry()
        {
            _selfEntrySelected = false;
            _selfEntry = null;
        }

        public string GetPreDefRmkValueAt(int idx)
        {//Get the value/id of Predefined Remark at index 'idx'
            return ((PreDefRmk)_rmkList[idx])._urno;
        }

        public string GetPreDefRmkTextAt(int idx)
        {//Get the value/id of Predefined Remark at index 'idx'
            return ((PreDefRmk)_rmkList[idx])._rmk;
        }

        public void AddPreDefRmk( PreDefRmk rmk )
        {
            if (rmk==null) throw new ApplicationException("Invalid Remark");
            if ((rmk._urno == null) || (rmk._rmk == null))
            {
                throw new ApplicationException("Invalid Remark.");
            }
            if (_rmkList == null) _rmkList = new ArrayList();
            _rmkList.Add(rmk);
        }

        public void RemoveAllPredefineRemarks()
        {
            _rmkList = null;
        }

        public void ClearAll()
        {
            RemoveAllPredefineRemarks();
            RemoveSelfEntry();
        }

        public String GetRemarksString()
        {
            string seperator = "";
            string rmk = "";
            if (_rmkList!=null)
            {
                int cnt = 0;
                cnt = _rmkList.Count;
                for (int i = 0; i < cnt; i++)
                {
                    rmk += seperator + ((PreDefRmk)_rmkList[i])._rmk;
                    seperator = RMK_SEPERATOR;
                }
            }
            if (_selfEntrySelected){
                rmk += _selfEntry;
            }
            return rmk;
        }
    }

    public class PreDefRmk
    {
        internal string _urno;
        internal string _rmk;

        private PreDefRmk()
        {
            _urno = null;
            _rmk = null;
        }

        public PreDefRmk(string remarkId, string remarkText)
        {
            if ((remarkId == null) || (remarkId == "")) throw new ApplicationException("Invalid Remark Id");
            if ((remarkText == null) || (remarkText == "")) throw new ApplicationException("Invalid Remark Text");
            _urno = remarkId;
            _rmk = remarkText;
        }
    }
}
