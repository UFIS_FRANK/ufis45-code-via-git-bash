using System;
using System.Collections.Generic;
using System.Text;

namespace UFIS.ITrekLib.CustomException
{
    public class NoResponseException:ApplicationException
    {
        private const string DEFAULT_MSG = "No response";

        public NoResponseException(): this(DEFAULT_MSG)
        {
        }

        public NoResponseException(string msg)
            : base(msg)
        {
        }
    }
}
