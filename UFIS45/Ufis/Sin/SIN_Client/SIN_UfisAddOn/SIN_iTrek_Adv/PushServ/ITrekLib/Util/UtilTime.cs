using System;
using System.Collections.Generic;
using System.Text;

namespace UFIS.ITrekLib.Util
{
    public class UtilTime
    {
        public static string ConvDateTimeToUfisFullDTString(DateTime dt)
        {
            string st = String.Format("{0:yyyyMMddHHmmss}", dt);
            return st;
        }

        public static string ConvDateTimeToITrekShortTimeString(DateTime dt)
        {
            string st = String.Format("{0:HHmm}", dt);
            return st;
        }

        public static DateTime ConvUfisTimeStringToDateTime(string st)
        {
            DateTime dt;
            if (!IsValidUfisTimeString( st, out dt ))
            {
                throw new ApplicationException("Invalid Date and Time format");
            }
            return dt;
        }

        public static String ConvUfisTimeStringToITrekShortTimeString(string stUfisTime)
        {
            return ConvDateTimeToITrekShortTimeString(ConvUfisTimeStringToDateTime(stUfisTime));
        }

        public static bool IsValidUfisTimeString( string st, out DateTime dt)
        {
            bool valid = false;
            dt = new DateTime(1, 1, 1);
            if (st == "") { valid = true; }
            else if (st.Length == 14)
            {
                try
                {
                    if (Convert.ToInt64(st).ToString() == st)
                    {
                        dt = new DateTime(Convert.ToInt16(st.Substring(0, 4)),
                            Convert.ToInt16(st.Substring(4, 2)),
                            Convert.ToInt16(st.Substring(6, 2)),
                            Convert.ToInt16(st.Substring(8, 2)),
                            Convert.ToInt16(st.Substring(10, 2)),
                            Convert.ToInt16(st.Substring(12, 2)));
                        valid = true;
                    }
                }
                catch (Exception )
                {                   
                    //throw;
                }
            }
            return valid;
        }

        public static DateTime CompDateTimeFromRef(DateTime dt, string hhmm)
        {
            ////To compute date and time for the "hhmm"
            ////  based on the "dt" datetime
            ////  take the nearest time to the base date time.
            DateTime result;
            //if (hhmm.Length != 4) throw new ApplicationException("Invalid time.");
            //int hour = Convert.ToInt32(hhmm.Substring(0, 2));
            //int min = Convert.ToInt32(hhmm.Substring(2, 2));
            //if ((hour < 0) || (hour > 23)) throw new ApplicationException("Invalid Hour.");
            //if ((min < 0) || (min > 59)) throw new ApplicationException("Invalid Minute.");
            //int refHour = dt.Hour;
            //int refMin = dt.Minute;
            //int incDay = 0;
            ////result = new DateTime(dt.Year, dt.Month, dt.Day, hour, min, 0);
            //incDay = ComputeIncDays(refHour, hour);
           
            ////result.AddDays(1);
            //result = new DateTime(dt.Year, dt.Month, dt.Day, hour, min, 0).AddDays(incDay);
            ////result.AddDays( incDay );
            result = CompDateTimeFromRef(dt, hhmm, 12);
            return result;
        }

        public static DateTime CompDateTimeFromRef(DateTime dt, string hhmm, int hourRange)
        {
            //To compute date and time for the "hhmm"
            //  based on the "dt" datetime
            //  take the nearest time to the base date time.
            DateTime result;
            if (hhmm.Length != 4) throw new ApplicationException("Invalid time.");
            int hour = Convert.ToInt32(hhmm.Substring(0, 2));
            int min = Convert.ToInt32(hhmm.Substring(2, 2));
            if ((hour < 0) || (hour > 23)) throw new ApplicationException("Invalid Hour.");
            if ((min < 0) || (min > 59)) throw new ApplicationException("Invalid Minute.");
            int refHour = dt.Hour;
            int refMin = dt.Minute;
            int incDay = 0;
            //result = new DateTime(dt.Year, dt.Month, dt.Day, hour, min, 0);
            incDay = ComputeIncDays(refHour, hour, hourRange);

            //result.AddDays(1);
            result = new DateTime(dt.Year, dt.Month, dt.Day, hour, min, 0).AddDays(incDay);
            //result.AddDays( incDay );
            return result;
        }

        private static int ComputeIncDays(int refHour, int hour, int hourRange)
        {
            int incDay = 0;
            int h1 = hour - refHour; 
            int h2 = h1 + 24;
            int h3 = refHour - hour;
            int h4 = h3 + 24;

            if ((h1 >= 0) && (h1 <= hourRange)) incDay = 0;
            else if ((h2 >= 0) && (h2 <= hourRange)) incDay = +1;
            else if ((h3 >= 0) && (h3 <= hourRange)) incDay = 0;
            else if ((h4 >= 0) && (h4 <= hourRange)) incDay = -1;
            else throw new ApplicationException("Invalid Time. Time is not within " + hourRange + " hour.");
            return incDay;
        }

        public static DateTime ConvStringToDate(string stDate)
        {
            return ConvStringToDate(stDate, "000000");
            //string[] sep ={ " ", "/", "-" };
            //DateTime dt;
            //stDate = stDate.Replace( "  "," ");
            //string[] dtArr = stDate.Split(sep, StringSplitOptions.RemoveEmptyEntries);
            //if (dtArr.Length != 3) throw new ApplicationException("Invalid Date Formate.");
            //string mm = dtArr[1].Trim().ToUpper(); ;
            //switch (mm)
            //{
            //    case "JAN": 
            //        mm = "1"; break;
            //    case "FEB":
            //        mm = "2"; break;
            //    case "MAR":
            //        mm = "3"; break;
            //    case "APR":
            //        mm = "4"; break;
            //    case "MAY":
            //        mm = "5"; break;
            //    case "JUN":
            //        mm = "6"; break;
            //    case "JUL":
            //        mm = "7"; break;
            //    case "AUG":
            //        mm = "8"; break;
            //    case "SEP":
            //        mm = "9"; break;
            //    case "OCT":
            //        mm = "10"; break;
            //    case "NOV":
            //        mm = "11"; break;
            //    case "DEC":
            //        mm = "12"; break;
            //}
            //try
            //{
            //     dt = new DateTime(Convert.ToInt16(dtArr[2]),
            //           Convert.ToInt16(mm),
            //           Convert.ToInt16(dtArr[0]));
            //}
            //catch (Exception)
            //{
            //    throw new ApplicationException("Invalid Date.");
            //}
            //return dt;
        }

        public static DateTime ConvStringToDate(string stDate, string hhmmss)
        {  //hhmmss ==> 155230 (3:52:30pm)
            string[] sep ={ " ", "/", "-" };
            DateTime dt;
            stDate = stDate.Replace("  ", " ");
            string[] dtArr = stDate.Split(sep, StringSplitOptions.RemoveEmptyEntries);
            if (dtArr.Length != 3) throw new ApplicationException("Invalid Date Formate.");
            string mm = dtArr[1].Trim().ToUpper();
            if ((hhmmss == null) || (hhmmss == "")){ hhmmss = "000000"; }
            hhmmss = hhmmss.Replace(" ", "");
            hhmmss = hhmmss.Trim().PadRight(6, '0');

            switch (mm)
            {
                case "JAN":
                    mm = "1"; break;
                case "FEB":
                    mm = "2"; break;
                case "MAR":
                    mm = "3"; break;
                case "APR":
                    mm = "4"; break;
                case "MAY":
                    mm = "5"; break;
                case "JUN":
                    mm = "6"; break;
                case "JUL":
                    mm = "7"; break;
                case "AUG":
                    mm = "8"; break;
                case "SEP":
                    mm = "9"; break;
                case "OCT":
                    mm = "10"; break;
                case "NOV":
                    mm = "11"; break;
                case "DEC":
                    mm = "12"; break;
            }
            try
            {
                dt = new DateTime(Convert.ToInt16(dtArr[2]),//year
                      Convert.ToInt16(mm), //month
                      Convert.ToInt16(dtArr[0]), //day
                      Convert.ToInt16(hhmmss.Substring(0, 2)), //hour
                      Convert.ToInt16(hhmmss.Substring(2, 2)), //minute
                      Convert.ToInt16(hhmmss.Substring(4, 2))  //second
                      );
            }
            catch (Exception)
            {
                throw new ApplicationException("Invalid Date.");
            }
            return dt;
        }

        public static int TimeDiffInMinute(string ufisTime1, string ufisTime2)
        {
            return TimeDiff(ConvUfisTimeStringToDateTime(ufisTime1),
                ConvUfisTimeStringToDateTime(ufisTime2),
                "M");
        }

        public static string TimeDiffString(string ufisDt1, string ufisDt2, string dtUnitInd)
        {
            string stTimeDiff = "";
            if (ufisDt1==null) ufisDt1 = "";
            if (ufisDt2==null) ufisDt2= "";
            if ((ufisDt1 != "") && (ufisDt2 != ""))
            {
                stTimeDiff = Convert.ToString( TimeDiff( ConvUfisTimeStringToDateTime(ufisDt1),
                    ConvUfisTimeStringToDateTime(ufisDt2), 
                    dtUnitInd ));
            }
            return stTimeDiff;
        }

        public static int TimeDiff(DateTime dt1, DateTime dt2, string dtUnitInd)
        {
            int diff = 0;

            switch (dtUnitInd)
            {
                case "S": //Time Difference in Second
                    diff += dt1.Second - dt2.Second + (dt1.Minute - dt2.Minute) * 60 +
                        (dt1.Hour - dt2.Hour) * 3600 + (dt1.DayOfYear - dt2.DayOfYear) * 3600 * 24;
                    break;
                case "M": //Time Difference in Minute
                    diff += (dt1.Minute - dt2.Minute) +
                        (dt1.Hour - dt2.Hour) * 60 + (dt1.DayOfYear - dt2.DayOfYear) * 60 * 24;
                    break;
                case "H":
                    diff += (dt1.Hour - dt2.Hour) + (dt1.DayOfYear - dt2.DayOfYear) * 24;
                    break;
                default:
                    throw new ApplicationException("Invalid Unit to Compute the Time Difference.");
                    //break;
            }
            return diff;
        }

        public static string ConvHHmmToUfisTimeAfterRefTime(string refUfisDateTime, string hhmm)
        {
            string newUfisTime = "";
            if ((hhmm != null) && (hhmm.Trim() != ""))
            {
                hhmm = hhmm.Trim().PadLeft(4, '0');
                int yyyy = Convert.ToInt32( refUfisDateTime.Substring(0, 4));
                int mth = Convert.ToInt32(refUfisDateTime.Substring(4, 2));
                int dd = Convert.ToInt32(refUfisDateTime.Substring(6, 2));
                int hh = Convert.ToInt32(refUfisDateTime.Substring(8, 2));
                int mm = Convert.ToInt32(refUfisDateTime.Substring(10, 2));
                int refHHMM = Convert.ToInt32(refUfisDateTime.Substring(8, 4));

                int nHH = Convert.ToInt32(hhmm.Substring(0, 2));
                int nMM = Convert.ToInt32(hhmm.Substring(2, 2));
                int nHHMM = Convert.ToInt32(hhmm);

                DateTime newDt = new DateTime(yyyy, mth, dd, nHH, nMM, 0);
                if (refHHMM > nHHMM)
                {
                    newDt.AddDays(1);
                }
                newUfisTime = ConvDateTimeToUfisFullDTString(newDt);
            }
            return newUfisTime;

        }

        public static string ConvHHmmToUfisTimeBeforeRefTime(string refUfisDateTime, string hhmm)
        {
            string newUfisTime = "";
            if ((hhmm != null) && (hhmm.Trim() != ""))
            {
                hhmm = hhmm.Trim().PadLeft(4, '0');
                int yyyy = Convert.ToInt32(refUfisDateTime.Substring(0, 4));
                int mth = Convert.ToInt32(refUfisDateTime.Substring(4, 2));
                int dd = Convert.ToInt32(refUfisDateTime.Substring(6, 2));
                int hh = Convert.ToInt32(refUfisDateTime.Substring(8, 2));
                int mm = Convert.ToInt32(refUfisDateTime.Substring(10, 2));
                int refHHMM = Convert.ToInt32(refUfisDateTime.Substring(8, 4));

                int nHH = Convert.ToInt32(hhmm.Substring(0, 2));
                int nMM = Convert.ToInt32(hhmm.Substring(2, 2));
                int nHHMM = Convert.ToInt32(hhmm);

                DateTime newDt = new DateTime(yyyy, mth, dd, nHH, nMM, 0);
                if (refHHMM < nHHMM)
                {
                    newDt.AddDays(-1);
                }
                newUfisTime = ConvDateTimeToUfisFullDTString(newDt);
            }
            return newUfisTime;
        }

        public static bool IsValidHour(string stHour, out int hour)
        {
            bool valid = false;
            hour = 0;
            try
            {
                hour = Convert.ToInt16(stHour);
                if ((hour >= 0) && (hour < 24)) valid = true;
            }
            catch (Exception)
            {
            }
            return valid;
        }

        public static bool IsValidMinute(string stMinute, out int minute)
        {
            bool valid = false;
            minute = 0;
            try
            {
                minute = Convert.ToInt16(stMinute);
                if ((minute >= 0) && (minute < 60)) valid = true;
            }
            catch (Exception)
            {
            }
            return valid;
        }

        public static string ConvUfisDateTimeStringToDisplayDateTime(string stUfisDateTime)
        {
            string st = "";
            stUfisDateTime += "";
            if (stUfisDateTime != "")
            {
                try
                {
                    st = String.Format("{0:dd MMM yyyy  HH:mm}", ConvUfisTimeStringToDateTime(stUfisDateTime));
                }
                catch (Exception)
                {
                }
            }
            return st;
        }

        public static string ConvDateTimeToDisplayDateTime(DateTime dt)
        {
            string st = "";
            try
            {
                st = String.Format("{0:dd MMM yyyy  HH:mm}", dt);
            }
            catch (Exception)
            {
            }
            return st;
        }

        public static string ConvUfisDateTimeStringToDisplayTime(string stUfisDateTime)
        {
            string st = "";
            stUfisDateTime += "";
            if (stUfisDateTime != "")
            {
                try
                {
                    st = String.Format("{0:HH:mm}", ConvUfisTimeStringToDateTime(stUfisDateTime));
                }
                catch (Exception)
                {
                }
            }
            return st;
        }

        public static int DiffInSec(DateTime dt1, DateTime dt2)
        {
            int diff = (dt1.Hour - dt2.Hour) * 3600 + 
                (dt1.Minute - dt2.Minute) * 60 + 
                (dt1.Second - dt2.Second);
            return diff;
        }

        public static bool IsValidHHmm(string hhmm)
        {
            bool valid = false;
            if (hhmm.Length == 4)
            {
                if (IsNumeric(hhmm))
                {
                    int nHH = Convert.ToInt32(hhmm.Substring(0, 2));
                    int nMM = Convert.ToInt32(hhmm.Substring(2, 2));
                    if ((nHH >= 0) && (nHH < 24) && (nMM >= 0) && (nMM < 60)) valid = true;
                }
            }
            return valid;
        }

        public static bool IsNumeric(string st)
        {
            //const int ascii0 = 48;
            //const int ascii9 = 57;
            bool isNum=true;

            int cnt = st.Length;
            for (int i = 0; i < cnt; i++)
            {
                char ch = st[i];
                if ((ch < '0') || (ch > '9'))
                {
                    isNum = false;
                    break;
                }
            }
            return isNum;
        }

        public static String ConvDateTimeToUfisTimeWithoutYear(DateTime dt)
        {
            return String.Format("{0:MMddHHmmss}", dt);
        }


        /// <summary>
        /// Check whether given 'ufisDateTime' is within the time range 'frHhMm' to 'toHhMm'
        /// </summary>
        /// <param name="ufisDateTime">Ufis Date Time format</param>
        /// <param name="frHhMm">From Time in HHMM Format (e.g. 0921). Assume it is valid time within (0000-2359)</param>
        /// <param name="toHhMm">To Time in HHMM Format (e.g. 2359). Assume it is valid time within (0000-2359)</param>
        /// <returns></returns>
        public static bool IsWithinTime( string ufisDateTime, string frHhMm, string toHhMm )
        {
            bool isWithin = false;
            //UtilLog.LogToTraceFile("UtilTime", "IsWithinTime:" + ufisDateTime + "," + frHhMm + "," + toHhMm);
            if (String.IsNullOrEmpty(frHhMm) && String.IsNullOrEmpty(toHhMm)) { isWithin = true; }
            else
            {
                if (frHhMm == "") frHhMm = "0000";
                if (toHhMm == "") toHhMm = "2359";                
                string t = ufisDateTime.Substring(8, 4);
                int tTime = Int32.Parse(t);
                int frTime = Int32.Parse(frHhMm);
                int toTime = Int32.Parse(toHhMm);
                if (frTime <= toTime)
                {//e.g. From 0930, To 2130
                    // (0930<=t<=2130)
                    if ((frTime <= tTime) && (tTime <= toTime)) isWithin = true;
                }
                else
                {//e.g. From 2235, To 0835
                    // NOT (0835<t<2235)
                    if (!((toTime < tTime) && (tTime < frTime))) isWithin = true;
                }
            }

            //UtilLog.LogToTraceFile("UtilTime", "IsWithinTime:" + isWithin);
            return isWithin;
        }

        /// <summary>
        /// Set Time for given Time for given date
        /// </summary>
        /// <param name="orgDateTime"></param>
        /// <param name="time"></param>
        /// <returns></returns>
        public static DateTime SetTime(DateTime orgDateTime, int hh, int mm, int ss)
        {
            return (new DateTime(orgDateTime.Year, orgDateTime.Month, orgDateTime.Day,
                hh, mm, ss));
        }

    }
}
