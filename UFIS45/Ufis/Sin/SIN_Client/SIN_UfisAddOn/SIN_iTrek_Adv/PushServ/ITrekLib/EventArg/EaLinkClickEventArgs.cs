using System;
using System.Collections.Generic;
using System.Text;

namespace UFIS.ITrekLib.EventArg
{
    public class EaLinkClickEventArgs : EventArgs
    {
        int _idx = -1;
        public EaLinkClickEventArgs(int idx)
        {
            _idx = idx;
        }

        public int idx
        {
            get { return _idx; }
        }
    }
}
