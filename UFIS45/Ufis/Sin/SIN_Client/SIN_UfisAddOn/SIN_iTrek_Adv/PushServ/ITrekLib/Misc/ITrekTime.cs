using System;
using System.Collections.Generic;
using System.Text;

namespace UFIS.ITrekLib.Misc
{
    public class ITrekTime
    {
        private ITrekTime() { }

        public static DateTime GetCurrentDateTime()
        {
            return DateTime.Now;
        }

        public static string GetCurrentDateTimeInUfisDateTimeString()
        {
            return MM.UtilLib.UtilTime.ConvDateTimeToUfisFullDTString(GetCurrentDateTime());
        }
    }
}
