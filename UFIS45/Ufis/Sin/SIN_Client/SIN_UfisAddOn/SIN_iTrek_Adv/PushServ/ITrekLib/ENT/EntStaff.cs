using System;
using System.Collections.Generic;
using System.Text;

namespace UFIS.ITrekLib.ENT
{
    public class EntStaff
    {
        private string _staffId = "";
        private string _fName = "";
        private string _lName = "";
        private bool _isAO = false;
        private DateTime _lastAccessTime = new DateTime();
        private DateTime _lastUpdTime = new DateTime();

        public EntStaff(string staffId, string fName, string lName, bool isAO)
        {
            _staffId = staffId;
            if (fName == null) fName = "";
            if (lName == null) lName = "";
            _fName = fName.Trim();
            _lName = lName.Trim();
            _isAO = isAO;
            _lastAccessTime = DateTime.Now;
            _lastUpdTime = DateTime.Now;
        }

        public string StaffId
        {
            get
            {
                _lastAccessTime = DateTime.Now;
                if (_staffId == null) _staffId = "";
                return _staffId;
            }
            //set
            //{
            //    if (value == null) _staffId = "";
            //    else _staffId = value.Trim();
            //}
        }

        public DateTime LastAccessTime
        {
            get
            {
                return _lastAccessTime;
            }
        }

        public DateTime LastUpdTime
        {
            get
            {
                return _lastUpdTime;
            }
        }

        public string FirstName
        {
            get
            {
                if (_fName == null) _fName = "";
                return _fName;
            }
            //set
            //{
            //    if (value == null) _fName = "";
            //    else _fName = value.Trim();
            //}
        }

        public string LastName
        {
            get
            {
                if (_lName == null) _lName = "";
                return _lName;
            }
            //set
            //{
            //    if (value == null) _lName = "";
            //    else _lName = value.Trim();
            //}
        }

        public bool IsAO
        {
            get
            {
                return _isAO;
            }
            //set
            //{
            //    _isAO = value;
            //}
        }

    }
}
