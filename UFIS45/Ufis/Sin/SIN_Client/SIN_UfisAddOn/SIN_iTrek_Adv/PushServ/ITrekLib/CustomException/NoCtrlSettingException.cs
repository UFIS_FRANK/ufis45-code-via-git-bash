using System;
using System.Collections.Generic;
using System.Text;

namespace UFIS.ITrekLib.CustomException
{
   public class NoCtrlSettingException : ApplicationException
   {
      private const string DEFAULT_MSG = "No Setting to generate the id.";

        public NoCtrlSettingException()
            : base(DEFAULT_MSG)
        {
        }

      public NoCtrlSettingException(string msg)
            : base(msg)
        {
        }
   }
}
