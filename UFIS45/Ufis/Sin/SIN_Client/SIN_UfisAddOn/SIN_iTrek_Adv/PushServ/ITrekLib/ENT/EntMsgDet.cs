using System;
using System.Collections.Generic;
using System.Text;

namespace UFIS.ITrekLib.ENT
{
	[Serializable]	
    public class EntMsgDet
    {
        private string _urno;
        private string _sby;
        private string _son;
        private string _sto;
        private string _rby;
        private string _ron;
        private string _msg;

        public string MsgId
        {
            get { return _urno; }
            set { _urno = value; }
        }

        public string SentBy
        {
            get { return _sby; }
            set { _sby = value; }
        }

        public string SentOn
        {
            get { return _son; }
            set { _son = value; }
        }

        public string SentTo
        {
            get { return _sto; }
            set { _sto = value; }
        }

        public string RecvBy
        {
            get { return _rby; }
            set { _rby = value; }
        }

        public string RecvOn
        {
            get { return _ron; }
            set { _ron = value; }
        }

        public string Msg
        {
            get { return _msg; }
            set { _msg = value; }
        }

    }

   public class EntMsg
   {
      private string _SentById = null;
      private string _SentByUserName = null;
      private string _MsgBody = null;
      private DateTime _SendDateTime;
      private List<EntMsgTo> _LsReceipients = null;

      public EntMsg(string sentById, string sentByUserName, string msgBody, List<EntMsgTo> lsReceipients, DateTime sendDateTime)
      {
         _SentById = sentById;
         _SentByUserName = sentByUserName;
         _MsgBody = msgBody;
         _SendDateTime = sendDateTime;
         _LsReceipients = new List<EntMsgTo>(lsReceipients);
      }

      public List<EntMsgTo> LsReceipients
      {
         get { return _LsReceipients; }
      }

      public string SentById
      {
         get { return _SentById; }
      }

      public string MsgBody
      {
         get { return _MsgBody; }
      }

      public DateTime SendDateTime
      {
         get { return _SendDateTime; }
      }

   }

   public enum EnumReceipientType { Flight, Person, Group, Unknown };

   public class EntMsgTo
   {
      private string _receipientId = null;
      private EnumReceipientType _receipientType = EnumReceipientType.Unknown;
      private string _receivedById = null;
      private DateTime _receivedDateTime;

      public EntMsgTo(string receipientId, EnumReceipientType receipientType)
      {
         _receipientId = receipientId;
         _receipientType = receipientType;
      }

      public bool IsReceived()
      {
         if (string.IsNullOrEmpty(_receivedById)) return false;
         else return true;
      }

      public string ReceipientId
      {
         get { return _receipientId; }
      }

      public EnumReceipientType ReceipientType
      {
         get { return _receipientType; }
      }

      public string StReceipientType
      {
         get
         {
            return (GetReceipientTypeString(_receipientType ));
         }
      }

      public static String GetReceipientTypeString(EnumReceipientType receipientType)
      {
         string st = "";
         switch (receipientType)
         {
            case EnumReceipientType.Flight:
               st = "F";
               break;
            case EnumReceipientType.Person:
               st = "P";
               break;
            case EnumReceipientType.Group:
               st = "G";
               break;
            case EnumReceipientType.Unknown:
               break;
            default:
               break;
         }
         return st;
      }

      public string ReceivedById
      {
         get { return _receivedById; }
         set { _receivedById = value; }
      }

      public DateTime ReceivedDateTime
      {
         get { return _receivedDateTime; }
         set { _receivedDateTime = value; }
      }
   }
}
