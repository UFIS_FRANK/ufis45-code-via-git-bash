using System;
using System.Collections.Generic;
using System.Text;

namespace UFIS.ITrekLib.ENT
{
    public enum EnTripType { FirstTrip, NextTrip, LastTrip, FirstNLastTrip, ToAutoDefine, None };
    public enum EnTripSendRecv { Send, Receive };
    public enum EnUserSecurityAccess { NoAccess, View, Edit };
    public enum EnBagArrType { Inter, Local, All, None };//To choose the trip uld with Destination is InterLine/Local/All
    public enum EnBagArrRecvCategory { Recv, UnReceive, All, None };//To choose the trip uld which are Received/UnReceived/All
    public enum EnBagArrFlightActionType { Receive, FirstBag, LastBag, ShowCntr, FlMsg, ShowRecvdCntr, None };
    public enum EnTripTypeSendReceive { SFTRPID , SLTRPID, RFTRPID, RLTRPID};

    public enum EnComposeMsgType { Personal, Flight, Group, Undefined };
    //To differentitate the Compose Message Type to show the receipient accordingly

    public enum EnFlightDateFilterType { UseATETST, UseOBLETST };
    //To filter by the flight time
    //  UseATETST ==> Filter using 'FLIGHT_DT' (AT,ET,ST)
    //  UseOBLETST ==> Filter using 'VBA_FLDT' (OBL,ET,ST)

   /// <summary>
   /// User Department.
   /// </summary>
   public enum EnUserDept { Apron, Baggage, Undefined };

}
