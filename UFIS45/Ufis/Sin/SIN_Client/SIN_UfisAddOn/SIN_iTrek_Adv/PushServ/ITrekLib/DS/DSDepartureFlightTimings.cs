﻿using System.Data;
using UFIS.ITrekLib.Util;
using MM.UtilLib;

namespace UFIS.ITrekLib.DS {
    public partial class DSDepartureFlightTimings
    {

        //public new void ReadXml(string fileName)
        //{
        //    try
        //    {
        //        // LogTraceMsg("ReadXml");
        //        this.DFLIGHT.BeginLoadData();
        //        this.EnforceConstraints = false;
        //        UFIS.ITrekLib.Util.UtilDataSet.LoadDataFromXmlFile(this, fileName);
        //        try
        //        {
        //            this.EnforceConstraints = true;
        //        }
        //        catch (System.Exception)
        //        {
        //            if (this != null)
        //            {
        //                if (this.DFLIGHT.HasErrors)
        //                {//if any error due to the constraints, remove from the dataset
        //                    DataRow[] rowArr = this.DFLIGHT.GetErrors();
        //                    LogTraceMsg("Got error in aflight file");
        //                    for (int i1 = 0; i1 < rowArr.Length; i1++)
        //                    {

        //                        //string st1 = rowArr[i1]["URNO"].ToString();
        //                        rowArr[i1].Delete();
        //                        LogTraceMsg("after deleting the row with error");

        //                    }
        //                    this.DFLIGHT.AcceptChanges();
        //                }
        //            }
        //        }
        //    }
        //    catch (System.Exception)
        //    {
        //        LogTraceMsg("Exception caught");
        //    }
        //    finally
        //    {
        //        this.EnforceConstraints = true;
        //        this.DFLIGHT.EndLoadData();
        //    }
        //}

        private void LogTraceMsg(string msg)
        {
            UtilLog.LogToTraceFile("DSDepartureFlightTimings", msg);
        }
    }
}
