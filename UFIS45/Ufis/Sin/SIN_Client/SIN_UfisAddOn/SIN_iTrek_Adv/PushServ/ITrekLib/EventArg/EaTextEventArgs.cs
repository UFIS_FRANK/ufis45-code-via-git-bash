using System;
using System.Collections.Generic;
using System.Text;

namespace UFIS.ITrekLib.EventArg
{
    public class EaTextEventArgs:EventArgs 
    {
        private string _text;

        public EaTextEventArgs(string text)
        {
            if (text==null){ _text = "";}
            else _text = text;
        }

        public string Text
        {
            get { return _text; }
        }
    }
}
