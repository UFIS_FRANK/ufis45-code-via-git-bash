using System;
using System.Collections.Generic;
using System.Text;

using System.Data;
using System.Collections;

using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.DSDB;
using UFIS.ITrekLib.DB;
using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.Util;
using UFIS.ITrekLib.Misc;
using iTrekXML;

using MM.UtilLib;
using UFIS.ITrekLib.CommMsg;
using UFIS.ITrekLib.Config;

namespace UFIS.ITrekLib.Ctrl
{
	public class CtrlFlight
	{

		public const string DAYS_OPTION_TODAY = "T";
		public const string DAYS_OPTION_BACK1DAY = "T_1";
		public const string DAYS_OPTION_BACK2DAY = "T_2";

		public readonly static string TERMINAL_SEPERATOR = EntWebFlightFilter.TERMINAL_SEPERATOR.ToString();
		public readonly static string AIRLINECODE_SEPERATOR = EntWebFlightFilter.AIRLINE_CODE_SEPERATOR.ToString();
		public readonly static string REGN_SEPERATOR = EntWebFlightFilter.REGN_SEPERATOR.ToString();

		private static DBDFlight dbFlight = DBDFlight.GetInstance();

		/// <summary>
		/// Restrict the From Date Time (up to -60 Hours)
		/// </summary>
		/// <param name="dtNew"></param>
		/// <returns></returns>
		public static DateTime RestrictFrDateTime(DateTime dtNew)
		{
			//DateTime dtFrDateTimeLimit = DateTime.Now.AddHours(-60);
			//if (dtNew.CompareTo(dtFrDateTimeLimit) < 0) dtNew = dtFrDateTimeLimit;
			return dtNew;
		}

		/// <summary>
		/// Restrict the To Date Time (up to +12 Hours)
		/// </summary>
		/// <param name="dtNew"></param>
		/// <returns></returns>
		public static DateTime RestrictToDateTime(DateTime dtNew)
		{
			DateTime dtToDateTimeLimit = DateTime.Now.AddHours(+12);
			if (dtNew.CompareTo(dtToDateTimeLimit) > 0) dtNew = dtToDateTimeLimit;
			return dtNew;
		}

		private static void SetFromAndToDateTimeForFilter(EntWebFlightFilter filter)
		{
			DateTime dtNow = DateTime.Now;
			DateTime dtDate = new DateTime(dtNow.Year, dtNow.Month, dtNow.Day, 0, 0, 0, 0);

			if (filter == null) throw new ApplicationException("No Filter.");
			if (string.IsNullOrEmpty(filter.DayOption)) return;

			switch (filter.DayOption)
			{
				case DAYS_OPTION_TODAY:
					if ((filter.FrTime == "") && (filter.ToTime == ""))
					{
						filter.UseFrToFixedDateTime = false;
					}
					else
					{
						filter.UseFrToFixedDateTime = true;
						filter.FrFixedDateTime = UtilTime.SetTime(dtDate, filter.FrTimeHH, filter.FrTimeMM, 0);
						filter.ToFixedDateTime = UtilTime.SetTime(dtDate, filter.ToTimeHH, filter.ToTimeMM, 59);
					}
					break;
				case DAYS_OPTION_BACK1DAY:
					filter.UseFrToFixedDateTime = true;
					dtDate = dtDate.AddDays(-1);
					filter.FrFixedDateTime = UtilTime.SetTime(dtDate, filter.FrTimeHH, filter.FrTimeMM, 0);
					filter.ToFixedDateTime = UtilTime.SetTime(dtDate, filter.ToTimeHH, filter.ToTimeMM, 59);
					break;
				case DAYS_OPTION_BACK2DAY:
					filter.UseFrToFixedDateTime = true;
					dtDate = dtDate.AddDays(-2);
					filter.FrFixedDateTime = UtilTime.SetTime(dtDate, filter.FrTimeHH, filter.FrTimeMM, 0);
					filter.ToFixedDateTime = UtilTime.SetTime(dtDate, filter.ToTimeHH, filter.ToTimeMM, 59);
					break;
				default:
					throw new ApplicationException("Invalid Days Option.");
			}
			if (filter.UseFrToFixedDateTime)
			{
				filter.FrFixedDateTime = RestrictFrDateTime(filter.FrFixedDateTime);
				filter.ToFixedDateTime = RestrictToDateTime(filter.ToFixedDateTime);
			}
			//LogTraceMsg("SetFromAndToDateTimeForFilter:" + filter.UseFrToFixedDateTime + ", " +
			//    filter.FrFixedDateTime.ToLongTimeString() + ", " + filter.FromDateTime.ToLongTimeString());
		}

		public static DSFlight RetrieveArrivalFlights(string terminal, DateTime fr, DateTime to)
		{
			DSFlight ds = null;
			//ds = dbFlight.RetrieveArrivalFlights(terminal, fr, to);
			EntWebFlightFilter filter = new EntWebFlightFilter();
			filter.FlightType = "A";
			filter.TerminalIds = terminal;
			filter.UseFrToFixedDateTime = true;
			filter.FrFixedDateTime = fr;
			filter.ToFixedDateTime = to;
			SetAirlineCodesByDept(filter);
			ds = dbFlight.RetrieveFlights(filter, "", -1, DBDFlight.EnSort.None);
			return ds;
		}

		public static DSFlight RetrieveDepartureFlights(string terminal, DateTime fr, DateTime to)
		{
			DSFlight ds = null;
			//ds = dbFlight.RetrieveArrivalFlights(terminal, fr, to);
			EntWebFlightFilter filter = new EntWebFlightFilter();
			filter.FlightType = "D";
			filter.TerminalIds = terminal;
			filter.UseFrToFixedDateTime = true;
			filter.FrFixedDateTime = fr;
			filter.ToFixedDateTime = to;
			SetAirlineCodesByDept(filter);
			ds = dbFlight.RetrieveFlights(filter, "", -1, DBDFlight.EnSort.None);
			return ds;
		}

		public static DSFlight RetrieveFlights(string terminal, DateTime fr, DateTime to)
		{
			//DSFlight ds = null;

			//EntWebFlightFilter filter = new EntWebFlightFilter();
			//filter.TerminalIds = terminal;
			//filter.UseFrToFixedDateTime = true;
			//filter.FrFixedDateTime = fr;
			//filter.ToFixedDateTime = to;
			//SetAirlineCodesByDept(filter);
			//ds = dbFlight.RetrieveFlights(filter, "", -1, DBDFlight.EnSort.None);
			//return ds;

			return RetrieveFlights(terminal, fr, to, "");
		}

		public static DSFlight RetrieveFlights(string terminal, DateTime fr, DateTime to, string userIdToFilter)
		{
			DSFlight ds = null;

			EntWebFlightFilter filter = new EntWebFlightFilter();
			filter.TerminalIds = terminal;
			filter.UseFrToFixedDateTime = true;
			filter.FrFixedDateTime = fr;
			filter.ToFixedDateTime = to;
			SetAirlineCodesByDept(filter);
			ds = dbFlight.RetrieveFlights(filter, userIdToFilter, -1, DBDFlight.EnSort.None);
            ds=RetrieveOpenFlights(ds);
			return ds;
		}

        public static DSFlight RetrieveOpenFlights(DSFlight dsFlight)
        {
            DSFlight ds = new DSFlight();
            DSFlight tempDs = dsFlight;
            foreach (DSFlight.FLIGHTRow row in tempDs.FLIGHT.Select("CLOSEDFLIGHTS<>'Y'", "FLIGHT_DT ASC"))
            {
                ds.FLIGHT.LoadDataRow(row.ItemArray, true);
            }
            return ds;
        
        
        }


		//[Conditional("TRACE_ON")]
		private static void LogTraceMsg(string msg)
		{
			try
			{
				MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("CtrlFlight", msg);
			}
			catch (Exception)
			{
				UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("CtrlFlight", msg);
			}
		}

		private static void LogMsg(string msg)
		{
			//UFIS.ITrekLib.Util.UtilLog.LogToGenericEventLog("CtrlFlight", msg);
			try
			{
				MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("CtrlFlight", msg);
			}
			catch (Exception)
			{
				UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("CtrlFlight", msg);
			}
		}

		private static EntWebFlightFilter SetAirlineCodesByDept(EntWebFlightFilter entFilter)
		{
			//LogTraceMsg("SetAirlineCodesByDept:" + entFilter.Dept.ToString());
			switch (entFilter.Dept)
			{
				case EnUserDept.Apron:
					entFilter.AirlineCodesByDept = CtrlTasks.GetAirlineCodeInString(CtrlTasks.EnTaskDept.Apron);
					break;
				case EnUserDept.Baggage:
					entFilter.AirlineCodesByDept = CtrlTasks.GetAirlineCodeInString(CtrlTasks.EnTaskDept.Baggage);
					break;
				case EnUserDept.Undefined://No filter
					entFilter.AirlineCodesByDept = "";
					break;
			}
			return entFilter;
		}

		public static DSFlight RetrieveFlightsForWeb(EntWebFlightFilter entFilter)
		{
			SetFromAndToDateTimeForFilter(entFilter);
			SetAirlineCodesByDept(entFilter);
			return dbFlight.RetrieveFlights(entFilter, "", -1, DBDFlight.EnSort.None);
		}

		public static DSFlight RetrieveFlightsForWeb(string terminal, int frMinute, int toMinute)
		{
			DateTime fr, to;
			if (frMinute >= toMinute) throw new ApplicationException("'From Time' must be before 'To Time'");
			fr = DateTime.Now.AddMinutes(frMinute);
			to = DateTime.Now.AddMinutes(toMinute);
			return (RetrieveFlights(terminal, fr, to));
		}

		public static DSFlight RetrieveAssignedFlightsForWeb(string terminal, string userId)
		{// Get the Assinged Flights for this user only.
			DSFlight ds = new DSFlight();
			int frMinute = Convert.ToInt32(CtrlConfig.GetWebFLINFO_FROM_MINUTES());
			int toMinute = Convert.ToInt32(CtrlConfig.GetWebFLINFO_TO_MINUTES());
			if (frMinute > 0) frMinute = 0 - frMinute;
			if (toMinute < 0) toMinute = Math.Abs(toMinute);
			DateTime fr, to;
			fr = DateTime.Now.AddMinutes(frMinute);
			to = DateTime.Now.AddMinutes(toMinute);
			//LogTraceMsg("Retrieve Assign Flight for " + terminal + ", " + 
			//    userId + ", 1000, " + 
			//    DBFlight.EnSort.Asc + ", " + 
			//    Util.UtilTime.ConvDateTimeToUfisFullDTString(fr) + ", " + 
			//    Util.UtilTime.ConvDateTimeToUfisFullDTString(to)); 
			ds = dbFlight.RetrieveAssignedFlights(terminal, userId, 1000, DBDFlight.EnSort.Asc, fr, to);
			return ds;
		}

		public static DSFlight RetrieveAssignedFlightsForWeb(EntWebFlightFilter entFilter, string adId, string userId)
		{// Get the Assinged Flights for this user only.
			DSFlight ds = new DSFlight();

			if (entFilter.FrMinute >= entFilter.ToMinute) throw new ApplicationException("'From Time' must be before 'To Time'");
			SetFromAndToDateTimeForFilter(entFilter);
			entFilter.FlightType = adId;
			SetAirlineCodesByDept(entFilter);
			ds = dbFlight.RetrieveAssignedFlights(entFilter, adId, userId, 1000, DBDFlight.EnSort.Asc);
			return ds;
		}

		public static DSFlight RetrieveAssignedFlights(string terminal, string userId)
		{// Get the Assinged Flights for this user only.
			DSFlight ds = new DSFlight();
			ds = dbFlight.RetrieveAssignedFlights(terminal, userId, 1000, DBDFlight.EnSort.Asc);
			return ds;
		}

		public static DSFlight RetrieveFlightsByFlightIdWithDoorClosed(string flightUrNo)
		{
			return dbFlight.RetrieveFlightInfoForAFlightWithDoorClosed(flightUrNo);
		}

		public static DSFlight RetrieveFlightsByFlightId(string flightId)
		{
			return dbFlight.RetrieveFlightInfoForAFlight(flightId);
		}

		public static void UpdateArrivalFlightTiming(IDbCommand cmd, string flightId, string field, string time, string userId)
		{
			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			bool isNewCmd = false;//Is command 'cmd' created from this method?

			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = db.GetCommand(true);
				}
				Hashtable ht = new Hashtable();
				ht.Add(field, time);
				UpdateArrivalFlightTiming(cmd, flightId, ht, userId);
				commit = true;
			}
			catch (Exception ex)
			{
				LogMsg("UpdateArrivalFlightTiming:Err:" + ex.Message);
				throw;
			}
			finally
			{
				if (isNewCmd)
				{//Command 'cmd' is created from this method. So, close the command from this method.

					CloseCmdAndRefreshFlightAfterUpdFlightInfo(cmd, commit, flightId);
                    LogTraceMsg("After CloseCmdAndRefreshFlightAfterUpdFlightInfo");
				}
			}
		}

		public static void CloseCmdAndRefreshFlightAfterUpdFlightInfo(IDbCommand cmd, bool commit, string flightId)
		{
			UtilDb udb = UtilDb.GetInstance();
			if (commit)
			{
				IDbConnection conn = cmd.Connection;
				udb.CloseCommand(cmd, commit, false);
				try
				{
					dbFlight.Refresh(conn, flightId);
					if (conn != null)
					{
						conn.Close();
						conn.Dispose();
					}
				}
				catch (Exception)
				{
				}
			}
			else
			{
				udb.CloseCommand(cmd, commit);
			}
		}

		public static void UpdateDepartureFlightTiming(IDbCommand cmd, 
			string flightId, string field, string time, string userId)
		{
			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			bool isNewCmd = false;//Is command 'cmd' created from this method?

			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = db.GetCommand(true);
				}
				Hashtable ht = new Hashtable();
				ht.Add(field, time);
				UpdateDepartureFlightTiming(cmd, flightId, ht, userId);
				commit = true;
			}
			catch (Exception ex)
			{
				LogMsg("UpdateArrivalFlightTiming:Err:" + ex.Message);
				throw;
			}
			finally
			{
				if (isNewCmd)
				{//Command 'cmd' is created from this method. So, close the command from this method.
					CloseCmdAndRefreshFlightAfterUpdFlightInfo(cmd, commit, flightId);
				}
			}
		}

		/// <summary>
		/// Update Arrival Flight Timing
		/// </summary>
		/// <param name="cmd">Command to use. Pass 'null' if this is the only transaction. Otherwise pass the command</param>
		/// <param name="flightUrno">Flight Id</param>
		/// <param name="htFieldValues">Field - Value pair of flight timings</param>
		/// <param name="userId">staff Id</param>
		public static void UpdateArrivalFlightTiming(IDbCommand cmd, 
			string flightId, Hashtable htFieldValues, string userId)
		{
			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			bool isNewCmd = false;//Is command 'cmd' created from this method?
			DateTime dtCurDateTime = ITrekTime.GetCurrentDateTime();

			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = db.GetCommand(true);
				}
				if (!dbFlight.UpdateFlightTiming(cmd, flightId, htFieldValues, userId, DBDFlight.EnumFlightType.Arrival))
				{
					throw new ApplicationException("Fail to Save the Timing.");
				}

				UpdateArrivalFlightTripTiming(cmd, flightId, htFieldValues, userId, dtCurDateTime);

				commit = true;

			}
			catch (Exception ex)
			{
				LogMsg("UpdateArrivalFlightTiming:Err:" + ex.Message);
				throw;
			}
			finally
			{
				if (isNewCmd)
				{//Command 'cmd' is created from this method. So, close the command from this method.
					CloseCmdAndRefreshFlightAfterUpdFlightInfo(cmd, commit, flightId);
				}
			}
		}

		/// <summary>
		/// Update Arrival Flight Timing in Trip for send and receive FTRIP/LTRIP 
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="flightId"></param>
		/// <param name="htFieldValues"></param>
		/// <param name="userId"></param>
		/// <param name="dtUpdDateTime"></param>
		/// 
		private static void UpdateArrivalFlightTripTiming(IDbCommand cmd, 
			string flightId, Hashtable htFieldValues, string userId, DateTime dtUpdDateTime)
		{
			Dictionary<ENT.EnTripTypeSendReceive, string> dTrip = new Dictionary<EnTripTypeSendReceive, string>();
			foreach (string stTimeTag in htFieldValues.Keys)
			{
				string stTime = (string) htFieldValues[stTimeTag];
				switch (stTimeTag)
				{
					case BA_FRIST_TRIP_TIME_TAG:
						dTrip.Add(EnTripTypeSendReceive.RFTRPID, stTime);
						break;
					case BA_LAST_TRIP_TIME_TAG:
						dTrip.Add(EnTripTypeSendReceive.RLTRPID, stTime);
						break;
					case AA_FIRST_TRIP_TIME_TAG:
						dTrip.Add(EnTripTypeSendReceive.SFTRPID, stTime);
						break;
					case AA_LAST_TRIP_TIME_TAG:
						dTrip.Add(EnTripTypeSendReceive.SLTRPID, stTime);
						break;
				}
			}

			if (dTrip.Count > 0)
			{
				if (!DBDTrip.GetInstance().UpdateTripTiming(cmd, flightId, dTrip, userId, dtUpdDateTime))
				{
					LogTraceMsg("UpdateArrivalFlightTripTiming:Unable To Update the Trip Timing." +
						flightId);
					throw new ApplicationException("Unable to save the Trip Timing.");
				}
			}

		}

		/// <summary>
		/// Update Departure Flight Timing in Trip Table
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="flightId">flightId</param>
		/// <param name="htFieldValues">Flight Timing tag and value pair</param>
		/// <param name="userId">user Id who update this information</param>
		/// <param name="dtUpdDateTime">update date and time</param>
		private static void UpdateDepartureFlightTripTiming(IDbCommand cmd,
			string flightId, Hashtable htFieldValues, string userId, DateTime dtUpdDateTime)
		{
			Dictionary<ENT.EnTripTypeSendReceive, string> dTrip = new Dictionary<EnTripTypeSendReceive, string>();
			foreach (string stTimeTag in htFieldValues.Keys)
			{
				string stTime = (string)htFieldValues[stTimeTag];
				switch (stTimeTag)
				{
					case BD_FIRST_TRIP_TIME_TAG://Baggage Departure First Trip
						dTrip.Add(EnTripTypeSendReceive.SFTRPID, stTime);
						break;
					case BD_LAST_TRIP_TIME_TAG://Baggage Departure Last Trip
						dTrip.Add(EnTripTypeSendReceive.SLTRPID, stTime);
						break;
				}
			}

			if (dTrip.Count > 0)
			{
				if (!DBDTrip.GetInstance().UpdateTripTiming(cmd, flightId, dTrip, userId, dtUpdDateTime))
				{
					LogTraceMsg("UpdateArrivalFlightTripTiming:Unable To Update the Trip Timing." +
						flightId);
					throw new ApplicationException("Unable to save the Trip Timing.");
				}
			}

		}

		public static void UpdateDepartureFlightTiming(IDbCommand cmd, 
			string flightId, Hashtable htFieldValues, string userId)
		{
			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			bool isNewCmd = false;//Is command 'cmd' created from this method?

			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = db.GetCommand(true);
				}

				if (!dbFlight.UpdateFlightTiming(cmd, flightId, htFieldValues, userId, DBDFlight.EnumFlightType.Departure))
				{
					throw new ApplicationException("Fail to Save the Timing.");
				}

				UpdateDepartureFlightTripTiming(cmd, flightId, htFieldValues, userId, ITrekTime.GetCurrentDateTime());
				commit = true;
			}
			catch (Exception ex)
			{
				LogMsg("UpdateArrivalFlightTiming:Err:" + ex.Message);
				throw;
			}
			finally
			{
				if (isNewCmd)
				{//Command 'cmd' is created from this method. So, close the command from this method.
					CloseCmdAndRefreshFlightAfterUpdFlightInfo(cmd, commit, flightId);
				}
			}
		}

		public const string BA_FRIST_TRIP_TIME_TAG = "FTRIP-B";
		public const string BA_LAST_TRIP_TIME_TAG = "LTRIP-B";
		public const string BA_FIRST_BAG_TIME_TAG = "FBAG";
		public const string BA_LAST_BAG_TIMG_TAG = "LBAG";

		public const string AA_FIRST_TRIP_TIME_TAG = "FTRIP";
		public const string AA_LAST_TRIP_TIME_TAG = "LTRIP";

		public const string BD_FIRST_TRIP_TIME_TAG = "FTRIP";
		public const string BD_LAST_TRIP_TIME_TAG = "LTRIP";

		public static string SaveArrBagTimingByTag(IDbCommand cmd, 
			string flightId, string userId, string stTime, string stTimeTag)
		{
			LogTraceMsg("SaveArrBagTiming " + flightId + ", " + userId + ", " + stTimeTag + "=" + stTime);
			string updMsg = "";
			string result = "";

			bool isNewCmd = false;//Is command 'cmd' created from this method?
			bool commit = false;
			UtilDb db = UtilDb.GetInstance();

			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = db.GetCommand(true);
				}

				DateTime dtUpdDateTime = ITrekTime.GetCurrentDateTime();

				Hashtable ht = new Hashtable();
				ht.Add(stTimeTag, stTime);
				UpdateArrivalFlightTiming(cmd, flightId, ht, userId);
				
				commit = true;
			}
			catch (Exception ex)
			{
				LogMsg("SaveArrBagTiming:Err:" + ex.Message);
				throw new ApplicationException("Fail to save.");
			}
			finally
			{
				if (isNewCmd)
				{//Command 'cmd' is created from this method. So, close the command from this method.
					CloseCmdAndRefreshFlightAfterUpdFlightInfo(cmd, commit, flightId);
				}
			}
			if (updMsg != "") throw new ApplicationException(updMsg);
			return result;
		}

		public static string SaveArrBagTimingInfo(string flightId, string flightNo,
			string stFirstTripTime, string stLastTripTime, string stFirstBagTime, string stLastBagTime, 
			string userId)
		{
			LogTraceMsg("SaveArrBagTimingInfo " + flightId + ", " + flightNo + ", " + stFirstTripTime + ", " + stLastTripTime + ", " + stFirstBagTime + ", " + stLastBagTime);
			string updMsg = "";
			string result = "";

			bool isNewCmd = false;//Is command 'cmd' created from this method?
			IDbCommand cmd = null;
			bool commit = false;
			UtilDb db = UtilDb.GetInstance();

			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = db.GetCommand(true);
				}

				Hashtable ht = new Hashtable();
				//ht.Add("FTRIP-B", stFirstTripTime);
				//ht.Add("LTRIP-B", stLastTripTime);
				//ht.Add("FBAG", stFirstBagTime);
				//ht.Add("LBAG", stLastBagTime);

				ht.Add(BA_FRIST_TRIP_TIME_TAG, stFirstTripTime);
				ht.Add(BA_LAST_TRIP_TIME_TAG, stLastTripTime);
				ht.Add(BA_FIRST_BAG_TIME_TAG, stFirstBagTime);
				ht.Add(BA_LAST_BAG_TIMG_TAG, stLastBagTime);

				//iTXMLcl itXml = new iTXMLcl();
				DateTime dtUpdDateTime = ITrekTime.GetCurrentDateTime();
				UpdateArrivalFlightTiming(cmd, flightId, ht, userId);
				////result = dbFlight.UpdateArrivalFlightTiming(flightId, ht, userStaffNo);
				////DBDTrip.GetInstance().UpdateTripTiming(flightId, "A", ht);
				//Dictionary<ENT.EnTripTypeSendReceive, string> dTrip = new Dictionary<EnTripTypeSendReceive, string>();
				//dTrip.Add(EnTripTypeSendReceive.RFTRPID, stFirstTripTime);
				//dTrip.Add(EnTripTypeSendReceive.RLTRPID, stLastTripTime);
				//if (!DBDTrip.GetInstance().UpdateTripTiming(cmd, flightId, dTrip, userId, dtUpdDateTime))
				//{
				//    LogTraceMsg("SaveArrBagTimingInfo:Unable To Update the Trip Timing." +
				//        flightId + "," + stFirstTripTime + "," + stLastTripTime);
				//}

				commit = true;
			}
			catch (Exception ex)
			{
				LogMsg("SaveArrBagTimingInfo:Error:" + ex.Message);
				throw new ApplicationException("Fail to save.");
			}
			finally
			{
				if (isNewCmd)
				{//Command 'cmd' is created from this method. So, close the command from this method.
					CloseCmdAndRefreshFlightAfterUpdFlightInfo(cmd, commit, flightId);
				}
			}
			if (updMsg != "") throw new ApplicationException(updMsg);
			return result;
		}

		//private const string COL_MAB = "MAB";
		//private const string COL_TBS_UP = "TBS_UP";

		//private const string COL_LD_ST = "LD_ST";
		//private const string COL_LD_END = "LD_END";
		//private const string COL_LST_DOOR = "LST_DOOR";
		//private const string COL_PLB = "PLB";
		//private const string COL_UNL_STRT = "UNL_STRT";
		//private const string COL_UNL_END = "UNL_END";

		//private const string COL_FST_CRGO = "FST_CRGO";
		//private const string COL_FBAG = "FBAG";
		//private const string COL_LBAG = "LBAG";


		public static void SaveArrAprTimingInfo(string flightId, string flightNo,
			string stMABTime, string stTbsUpTime, string stPlbTime,
			string stUnlStartTime, string stFirstTripTime,
			string stLastTripTime, string stUnlEndTime, string userId)
		{
			List<DateTime> lsDt = new List<DateTime>();
			lsDt.Add(DateTime.Now);
			string stTrsitFltUrno = "";
			string updMsg = "";

			bool isNewCmd = false;//Is command 'cmd' created from this method?
			IDbCommand cmd = null;
			bool commit = false;
			UtilDb db = UtilDb.GetInstance();

			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = db.GetCommand(true);
				}
			}
			catch
			{
			}
			if (cmd == null)
			{
				LogMsg("SaveArrAprTimingInfo:Err:Can not get database connection.");
				throw new ApplicationException("Database Error.");
			}
			lsDt.Add(DateTime.Now);

			stTrsitFltUrno = RetrieveRelatedTransitFlightUrnoForFlight(cmd, flightId);
			lsDt.Add(DateTime.Now);
			LogTraceMsg("SaveArrAprTimingInfo," + flightId + ", " + flightNo + ", " +
				stMABTime + ", " +
				stTbsUpTime + ", " + stPlbTime + ", " +
				stUnlStartTime + ", " + stFirstTripTime + ", " +
				stLastTripTime + ", " + stUnlEndTime + "," + stTrsitFltUrno);
			try
			{
				Hashtable ht = new Hashtable();

				ht.Add("MAB", "" + stMABTime);
				ht.Add("TBS-UP", "" + stTbsUpTime);
				ht.Add("PLB", "" + stPlbTime);
				ht.Add("UNLOAD-START", "" + stUnlStartTime);
				ht.Add("FTRIP", "" + stFirstTripTime);
				ht.Add("LTRIP", stLastTripTime);
				ht.Add("UNLOAD-END", "" + stUnlEndTime);

				//iTXMLcl itXml = new iTXMLcl();
				lsDt.Add(DateTime.Now);
				DateTime dtUpdDateTime = ITrekTime.GetCurrentDateTime();
				UpdateArrivalFlightTiming(cmd, flightId, ht, userId);
				lsDt.Add(DateTime.Now);
				if ((updMsg == "") && (stTrsitFltUrno != ""))
				{
					Hashtable htTr = new Hashtable();
					htTr.Add("MAB", stMABTime);
					UpdateArrivalFlightTiming(cmd, flightId, htTr, userId);
					lsDt.Add(DateTime.Now);
				}
				commit = true;
			}
			catch (Exception ex)
			{
				LogMsg("SaveArrAprTimingInfo:Err:" + ex.Message);
				throw new ApplicationException("Fail to save data.");
			}
			finally
			{
				if (isNewCmd)
				{//Command 'cmd' is created from this method. So, close the command from this method.
					CloseCmdAndRefreshFlightAfterUpdFlightInfo(cmd, commit, flightId);
				}
			}
			lsDt.Add(DateTime.Now);
			string st = "SaveArrAprTimingInfo:" + flightId;

			int cntDt = lsDt.Count;
			if (cntDt > 1)
			{
				for (int i = 1; i < cntDt; i++)
				{
					st += string.Format(",{0}:{1}",
						i, UtilTime.ElapsedTime(lsDt[i - 1], lsDt[i]));
				}
				st += string.Format(",Tot:{0}",
						UtilTime.ElapsedTime(lsDt[1], lsDt[cntDt - 1]));
				LogTraceMsg(st);
			}
			if (updMsg != "") throw new ApplicationException(updMsg);
		}

		public static void SaveDepAprTimingInfo(string flightId, string flightNo,
	string stMABTime, string stPlbTime, string stLoadStartTime,
	string stLoadEndTime, string stLastDoorTime, string userId)
		{
			string stTrsitFltUrno = "";
			string updMsg = "";

			bool isNewCmd = false;//Is command 'cmd' created from this method?
			IDbCommand cmd = null;
			bool commit = false;
			UtilDb db = UtilDb.GetInstance();

			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = db.GetCommand(true);
				}
			}
			catch { }
			if (cmd == null)
			{
				LogMsg("SaveDepAprTimingInfo:Err:Can not get database connection.");
				throw new ApplicationException("Database Error.");
			}
			stTrsitFltUrno = RetrieveRelatedTransitFlightUrnoForFlight(cmd, flightId);

			LogTraceMsg("SaveDepAprTimingInfo, " + flightId + ", " + flightNo + ", " +
				stMABTime + ", " + stPlbTime + ", " +
				stLoadStartTime + ", " +
				stLoadEndTime + ", " +
				stLastDoorTime + "," + stTrsitFltUrno);

			try
			{
				Hashtable ht = new Hashtable();
				ht.Add("MAB", stMABTime);
				ht.Add("PLB", stPlbTime);
				ht.Add("LOAD-START", stLoadStartTime);
				ht.Add("LOAD-END", stLoadEndTime);
				ht.Add("LAST-DOOR", stLastDoorTime);

				iTXMLcl itXml = new iTXMLcl();
				UpdateDepartureFlightTiming(cmd, flightId, ht, userId);
				if (stTrsitFltUrno != "")
				{
					Hashtable htTr = new Hashtable();
					htTr.Add("MAB", stMABTime);
					UpdateDepartureFlightTiming(cmd, flightId, htTr, userId);
				}
				commit = true;

			}
			catch (Exception ex)
			{
				LogMsg("SaveDepAprTimingInfo:Error:" + ex.Message);
				throw new ApplicationException("Fail to save.");
			}
			finally
			{
				if (isNewCmd)
				{//Command 'cmd' is created from this method. So, close the command from this method.
					CloseCmdAndRefreshFlightAfterUpdFlightInfo(cmd, commit, flightId);
				}
			}
			if (updMsg != "") throw new ApplicationException(updMsg);
		}

		public static void SaveDepBagTimingInfo(string flightId, string flightNo,
			string firstTripTime, string lastTripTime, string userId)
		{
			string updMsg = "";
			LogTraceMsg("SaveDepBagTimingInfo, " + flightId + ", " + flightNo + ", " +
				firstTripTime + ", " + lastTripTime);

			bool isNewCmd = false;//Is command 'cmd' created from this method?
			IDbCommand cmd = null;
			bool commit = false;
			UtilDb db = UtilDb.GetInstance();

			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = db.GetCommand(true);
				}

				Hashtable ht = new Hashtable();
				ht.Add("FTRIP", firstTripTime);
				ht.Add("LTRIP", lastTripTime);
				//iTXMLcl itXml = new iTXMLcl();
				DateTime dtUpdDateTime = ITrekTime.GetCurrentDateTime();
				UpdateDepartureFlightTiming(cmd, flightId, ht, userId);
				commit = true;
			}
			catch (Exception ex)
			{
				LogMsg("SaveDepBagTimingInfo:Error:" + ex.Message);
				throw new ApplicationException("Fail to save.");
			}
			finally
			{
				if (isNewCmd)
				{//Command 'cmd' is created from this method. So, close the command from this method.
					CloseCmdAndRefreshFlightAfterUpdFlightInfo(cmd, commit, flightId);
				}
			}
			if (updMsg != "") throw new ApplicationException(updMsg);
		}

		public static DSFlight RetrieveOpenFlightsForAO(string userId, int count)
		{
			return dbFlight.RetrieveOpenFlightsForAO(userId, count);
		}

		public static DSFlight RetrieveOpenFlightsForAO(string userId)
		{
			return dbFlight.RetrieveOpenFlightsForAO(userId, 3);
		}

		public static DSArrivalFlightTimings RetrieveArrivalFlightTimings(string urno)
		{
			return dbFlight.RetrieveArrivalFlightTimings(urno);

		}
		public static DSDepartureFlightTimings RetrieveDepartureFlightTimings(string urno)
		{
			return dbFlight.RetrieveDepartureFlightTimings(urno);

		}

		/* Added by Alphy for testing*/
		//public static DSFlight RetrieveOpenFlightsForAO()
		//{
		//    return dbFlight.RetrieveOpenFlightsForAO();
		//}
		//public static DSFlight RetrieveClosedFlightsForAO(int maxNumberOfFlights)
		//{
		//    return dbFlight.RetrieveClosedFlightsForAO(maxNumberOfFlights, DBDFlight.EnSort.Desc);
		//}

		public static DSFlight RetrieveClosedFlightsForAO(string userId, int maxNumberOfFlights)
		{
			return dbFlight.RetrieveClosedFlightsForAO(userId, maxNumberOfFlights, DBDFlight.EnSort.Desc);
		}

		public static bool IsClosedFlight(DSFlight.FLIGHTRow row,
			out string flightId, out string adid,
			out string turnFlightId)
		{
			bool isClosed = false;
			flightId = "";
			adid = "";
			turnFlightId = "";

			try
			{
				isClosed = DBDFlight.GetInstance().IsClosedFlight(row, out flightId, out adid,out turnFlightId);
			}
			catch (Exception ex)
			{
				LogMsg("IsClosedFlight:Err:" + ex.Message);
				throw;
			}
			return isClosed;
		}

		public static string GetFlightSummInfo(string flightId)
		{
			return dbFlight.GetFlightInfoSumm(flightId);
		}


		public static string GetAOIdForFlight(string flightId)
		{
			string aoId = "";
			DSFlight dsFlight = RetrieveFlightsByFlightId(flightId);
			if ((dsFlight == null) || (dsFlight.FLIGHT.Rows.Count < 1))
			{
				//throw new ApplicationException("Can not get the Flight Information.");
			}
			else
			{
				try
				{
					aoId = ((DSFlight.FLIGHTRow)dsFlight.FLIGHT.Rows[0]).AOID;
				}
				catch { }
			}
			return aoId;
		}

		/// <summary>
		/// Has Ao Information for given flight
		/// </summary>
		/// <param name="flightId">Flight Id</param>
		/// <param name="aoId">AO Id</param>
		/// <param name="aoFName">AO First Name</param>
		/// <param name="aoLName">AO Last Name</param>
		/// <returns>true-has AO Info, false-otherwise</returns>
		public static bool HasAoInfo(string flightId,
			out string aoId, out string aoFName, out string aoLName)
		{
			bool has = false;
			aoId = "";
			aoFName = "";
			aoLName = "";
			DSFlight dsFlight = RetrieveFlightsByFlightId(flightId);
			if ((dsFlight == null) || (dsFlight.FLIGHT.Rows.Count < 1))
			{
				//throw new ApplicationException("Can not get the Flight Information.");
			}
			else
			{
				try
				{
					DSFlight.FLIGHTRow row = (DSFlight.FLIGHTRow)dsFlight.FLIGHT.Rows[0];
					aoId = row.AOID;
					aoFName = row.AOFNAME;
					aoLName = row.AOLNAME;
					if ((!string.IsNullOrEmpty(aoId)) || (aoId.Trim() != ""))
					{
						has = true;
						LogTraceMsg(string.Format("HasAoInfo:{0},<{1}>,<{2}>", aoId, aoFName, aoLName));
					}
				}
				catch { }
			}
			return has;
		}

		/// <summary>
		/// Get Best Flight Date and Time
		/// 1. AT,  2. ET,  3. ST
		/// </summary>
		/// <param name="flightId">Flight Id</param>
		/// <returns>UFIS Full Date Time string (e.g. 20080829154600 for 2008 Aug 29, 15:46:00)</returns>
		public static string GetFlightDateTime(string flightId)
		{
			string stFlightDt = "";
			DSFlight dsFlight = RetrieveFlightsByFlightId(flightId);
			if ((dsFlight == null) || (dsFlight.FLIGHT.Rows.Count < 1))
			{
			}
			else
			{
				try
				{
					stFlightDt = ((DSFlight.FLIGHTRow)dsFlight.FLIGHT.Rows[0]).AT;
					if (stFlightDt == "") stFlightDt = ((DSFlight.FLIGHTRow)dsFlight.FLIGHT.Rows[0]).ET;
					if (stFlightDt == "") stFlightDt = ((DSFlight.FLIGHTRow)dsFlight.FLIGHT.Rows[0]).ST;
				}
				catch { }
			}
			return stFlightDt;
		}

		//public void UpdateApronServiceReportExist(string flightId, bool exist)
		//{
		//   try
		//   {
		//      dbFlight.UpdateApronServiceRportExist(flightId, exist);
		//      dbFlight.Refresh(flightId);
		//   }
		//   catch (Exception ex)
		//   {
		//      LogMsg("UpdateApronServiceReportExist:Error:" + flightId + ", " + exist + ", " + ex.Message);
		//   }
		//}

		//public void UpdateBagPreTimeReportExist(string flightId, bool exist)
		//{
		//   try
		//   {
		//      dbFlight.UpdateBagPreTimeReportExist(flightId, exist);
		//      dbFlight.Refresh(flightId);
		//   }
		//   catch (Exception ex)
		//   {
		//      LogMsg("UpdateBagPreTimeReportExist:Error:" + flightUrNo + ", " + exist + ", " + ex.Message);
		//   }
		//}

		public static CtrlFlight GetInstance()
		{
			CtrlFlight ctrl = new CtrlFlight();
			return ctrl;
		}

		public string GetDefaultTimeInd()
		{
			return "T";
		}

		public DataTable GetTimeIndTable()
		{
			DataTable dt = new DataTable();

			dt.Columns.Add("ID", Type.GetType("System.String"));
			dt.Columns.Add("VAL", Type.GetType("System.String"));
			DataRow row;

			row = dt.NewRow();
			row["ID"] = "T";
			row["VAL"] = "Today";
			dt.Rows.Add(row);

			row = dt.NewRow();
			row["ID"] = "T_1";
			row["VAL"] = "Back 1 Day";
			dt.Rows.Add(row);

			row = dt.NewRow();
			row["ID"] = "T_2";
			row["VAL"] = "Back 2 Days";
			dt.Rows.Add(row);

			dt.AcceptChanges();
			return dt;
		}

		public static int GetWebFLINFO_TO_MINUTESByTimeInd(string timeInd)
		{
			int minutes = 0;
			//switch (timeInd)
			//{
			//    case "T": //Today
			//        minutes = +120; //+2 Hours
			//        break;
			//    case "T_1": //Back 1 day
			//        minutes = +120; //+2 Hours
			//        break;
			//    case "T_2": //Back 2 day
			//        minutes = +120; //+2 Hours
			//        break;
			//    default:
			//        throw new ApplicationException("Unknown Time Indicator.");
			//        break;
			//}
			minutes = Convert.ToInt32(CtrlConfig.GetWebFLINFO_TO_MINUTES());
			return minutes;
		}

		public static int GetWebFLINFO_FROM_MINUTESByTimeInd(string timeInd)
		{
			int minutes = Convert.ToInt32(CtrlConfig.GetWebFLINFO_FROM_MINUTES());
			switch (timeInd)
			{
				case "T": //Today
					//minutes = -720; //-6 Hours
					break;
				case "T_1": //Back 1 day
					minutes -= 1440; //-36 Hours
					break;
				case "T_2": //Back 2 day
					minutes -= 2880; //-60 Hours
					break;
				default:
					throw new ApplicationException("Unknown Time Indicator.");
				//break;
			}
			return minutes;
		}

		public static int GetWebFLINFO_AO_TO_MINUTESByTimeInd(string timeInd)
		{
			int minutes = 0;
			//switch (timeInd)
			//{
			//    case "T": //Today
			//        minutes = +120; //+2 Hours
			//        break;
			//    case "T_1": //Back 1 day
			//        minutes = +120; //+2 Hours
			//        break;
			//    case "T_2": //Back 2 day
			//        minutes = +120; //+2 Hours
			//        break;
			//    default:
			//        throw new ApplicationException("Unknown Time Indicator.");
			//        break;
			//}
			minutes = Convert.ToInt32(CtrlConfig.GetWebFLINFO_AO_TO_MINUTES());
			return minutes;
		}

		public static int GetWebFLINFO_AO_FROM_MINUTESByTimeInd(string timeInd)
		{
			int minutes = Convert.ToInt32(CtrlConfig.GetWebFLINFO_AO_FROM_MINUTES());
			switch (timeInd)
			{
				case "T": //Today
					//minutes = -720; //-6 Hours
					break;
				case "T_1": //Back 1 day
					minutes -= 1440; //-36 Hours
					break;
				case "T_2": //Back 2 day
					minutes -= 2880; //-60 Hours
					break;
				default:
					throw new ApplicationException("Unknown Time Indicator.");
				//break;
			}
			return minutes;
		}

		public static string RetrieveRelatedTransitFlightUrnoForFlight(IDbCommand cmd, string flightId)
		{
			string stTrsitFltUrno = "";
			Object objTurn = UtilDb.GetDataForAColumn(cmd, DB_Info.DB_T_FLIGHT, flightId, "TURN");
			if ((objTurn == null) || (objTurn == DBNull.Value)) stTrsitFltUrno = "";
			else stTrsitFltUrno = ((string)objTurn).Trim();
			return stTrsitFltUrno;
			//return dbFlight.RetrieveRelatedTransitFlightUrnoForFlight(flightId);
		}

		//private static Hashtable _ArrivalOrgTable = null;
		//private static Hashtable _DepartureOrgTable = null;

		//private static Hashtable GetArrivalOrgTable()
		//{
		//    if (_ArrivalOrgTable == null)
		//    {
		//        _ArrivalOrgTable = new Hashtable();
		//        _ArrivalOrgTable.Clear();
		//        _ArrivalOrgTable.Add("MAB", "AO");
		//        _ArrivalOrgTable.Add("LOAD-START", "AO");
		//        _ArrivalOrgTable.Add("LOAD-END", "AO");
		//        _ArrivalOrgTable.Add("LAST-DOOR", "AO");
		//        _ArrivalOrgTable.Add("PLB", "AO");
		//        _ArrivalOrgTable.Add("ATD", "AO");
		//        _ArrivalOrgTable.Add("ATA", "AO");
		//        _ArrivalOrgTable.Add("TBS-UP", "AO");
		//        _ArrivalOrgTable.Add("UNLOAD-START", "AO");
		//        _ArrivalOrgTable.Add("UNLOAD-END", "AO");
		//        _ArrivalOrgTable.Add("FBAG", "BO");
		//        _ArrivalOrgTable.Add("LBAG", "BO");
		//        _ArrivalOrgTable.Add("FTRIP-B", "BO");
		//        _ArrivalOrgTable.Add("LTRIP-B", "BO");
		//        _ArrivalOrgTable.Add("FTRIP", "AO");
		//        _ArrivalOrgTable.Add("LTRIP", "AO");
		//    }
		//    return _ArrivalOrgTable;
		//}

		//private static Hashtable GetDepartureOrgTable()
		//{
		//    if (_DepartureOrgTable == null)
		//    {
		//        _DepartureOrgTable = new Hashtable();
		//        _DepartureOrgTable.Clear();
		//        _DepartureOrgTable.Add("MAB", "AO");
		//        _DepartureOrgTable.Add("LOAD-START", "AO");
		//        _DepartureOrgTable.Add("LOAD-END", "AO");
		//        _DepartureOrgTable.Add("LAST-DOOR", "AO");
		//        _DepartureOrgTable.Add("PLB", "AO");
		//        _DepartureOrgTable.Add("ATD", "AO");
		//        _DepartureOrgTable.Add("ATA", "AO");
		//        _DepartureOrgTable.Add("TBS-UP", "AO");
		//        _DepartureOrgTable.Add("UNLOAD-START", "AO");
		//        _DepartureOrgTable.Add("UNLOAD-END", "AO");
		//        _DepartureOrgTable.Add("FBAG", "BO");
		//        _DepartureOrgTable.Add("LBAG", "BO");
		//        _DepartureOrgTable.Add("FTRIP-B", "BO");
		//        _DepartureOrgTable.Add("LTRIP-B", "BO");
		//        _DepartureOrgTable.Add("FTRIP", "BO");
		//        _DepartureOrgTable.Add("LTRIP", "BO");
		//    }
		//    return _DepartureOrgTable;
		//}

		/// <summary>
		/// Compute Date Time for given Time in (HHMM format) for given flight Id using Flight Best Date Time.
		/// </summary>
		/// <param name="flightId">Flight Id</param>
		/// <param name="hhmm">Time in HHMM format (e.g. 2156)</param>
		/// <returns>DateTime for given HHMM with reference to Flight Best Date Time</returns>
		public static DateTime ComputeFlightRelDateTime(string flightId, string hhmm)
		{
			DateTime dtSelectedTime = DateTime.Now;
			DateTime dtFlightDateTime = UtilTime.ConvUfisTimeStringToDateTime(GetFlightDateTime(flightId));
			dtSelectedTime = UtilTime.CompDateTimeFromRef(dtFlightDateTime,
				hhmm, 10);
			return dtSelectedTime;
		}

		public static bool IsEmptyString(string st)
		{
			return (string.IsNullOrEmpty(st) || (st.Trim() == ""));
		}

		public enum EnmArrFlightTimeType { ArrBagFirstTripTime, ArrAprLastTripTime, ArrBagLastTripTime, None };
		public static bool IsArrFlightTimeExist(IDbCommand cmd, string flightId, 
			EnmArrFlightTimeType timeType, out DateTime dtTime)
		{
			bool exist = false;
			UtilDb udb = UtilDb.GetInstance();
			bool isNewCmd = false;
			DBDFlight dbFlight = DBDFlight.GetInstance();
			object objTime = null;
			dtTime = DateTime.Now;
			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = udb.GetCommand(false);
					switch (timeType)
					{
						case EnmArrFlightTimeType.ArrBagFirstTripTime:
							objTime = UtilDb.GetDataForAColumn(cmd, DB_Info.DB_T_FLIGHT, flightId, "FTRIPB");
							break;
						case EnmArrFlightTimeType.ArrAprLastTripTime:
							objTime = UtilDb.GetDataForAColumn(cmd, DB_Info.DB_T_FLIGHT, flightId, "LTRIPB");
							break;
						case EnmArrFlightTimeType.ArrBagLastTripTime:
							objTime = UtilDb.GetDataForAColumn(cmd, DB_Info.DB_T_FLIGHT, flightId, "FBAG");
							break;
						case EnmArrFlightTimeType.None:
							throw new ApplicationException("Invalid Time Type requested.");
							break;
						default:
							throw new ApplicationException("Invalid Time Type requested.");
							break;
					}
					if ((objTime != null) && (objTime != DBNull.Value))
					{
						exist = true;
						dtTime = Convert.ToDateTime(objTime);
					}
				}
			}
			catch (Exception ex)
			{
				LogMsg("IsArrFlightTimeExist:Err:" + flightId + "," + ex.Message);
				throw;
			}
			finally
			{
				if (isNewCmd)
				{
					udb.CloseCommand(cmd, false);
				}
			}

			return exist;
		}
		//public static bool IsArrFlightTimeExist(string flightId, EnmArrFlightTimeType timeType, out DateTime dtTime)
		//{
		//    bool exist = false;
		//    DSFlight ds = null;
		//    dtTime = new DateTime(1, 1, 1);
		//    DBDFlight dbFlight = DBDFlight.GetInstance();
		//    ds = dbFlight.RetrieveFlightInfoForAFlight(flightId);
		//    if ((ds == null) || (ds.FLIGHT == null) || (ds.FLIGHT.Rows.Count < 1)) throw new ApplicationException("Unable to get the flight information.");
		//    if (ds.FLIGHT.Rows.Count != 1) throw new ApplicationException("Duplicate Flight Information.");

		//    DSFlight.FLIGHTRow row = (DSFlight.FLIGHTRow)ds.FLIGHT.Rows[0];
		//    String stTime = "";
		//    if (row.A_D == "A")
		//    {
		//        switch (timeType)
		//        {
		//            case EnmArrFlightTimeType.ArrAprLastTripTime:
		//                stTime = row.AP_LST_TRP;
		//                break;
		//            case EnmArrFlightTimeType.ArrBagFirstTripTime:
		//                stTime = row.BG_FST_TRP;
		//                break;
		//            case EnmArrFlightTimeType.ArrBagLastTripTime:
		//                stTime = row.BG_LST_TRP;
		//                break;
		//            default:
		//                throw new ApplicationException("Invalid Arrival Flight Time Type.");
		//        }

		//        if (IsEmptyString(stTime))
		//        {
		//            try
		//            {
		//                DSArrivalFlightTimings dsArr = dbFlight.RetrieveArrivalFlightTimings(flightId);
		//                if ((dsArr != null) && (dsArr.AFLIGHT != null) && (dsArr.AFLIGHT.Rows.Count == 1))
		//                {
		//                    DSArrivalFlightTimings.AFLIGHTRow rowArr = (DSArrivalFlightTimings.AFLIGHTRow)dsArr.AFLIGHT.Rows[0];
		//                    stTime = rowArr._FTRIP_B;
		//                    switch (timeType)
		//                    {
		//                        case EnmArrFlightTimeType.ArrAprLastTripTime:
		//                            stTime = rowArr.LTRIP;
		//                            break;
		//                        case EnmArrFlightTimeType.ArrBagFirstTripTime:
		//                            stTime = rowArr._FTRIP_B; ;
		//                            break;
		//                        case EnmArrFlightTimeType.ArrBagLastTripTime:
		//                            stTime = rowArr._LTRIP_B;
		//                            break;
		//                        default:
		//                            throw new ApplicationException("Invalid Arrival Flight Time Type.");
		//                    }
		//                }
		//            }
		//            catch (Exception)
		//            {
		//            }
		//        }
		//    }
		//    else
		//    {
		//        throw new ApplicationException("It is not an Arrival Flight.");
		//    }

		//    if (IsEmptyString(stTime)) exist = false;
		//    else
		//    {
		//        dtTime = UtilTime.ConvUfisTimeStringToDateTime(stTime);
		//        exist = true;
		//    }

		//    return exist;
		//}

		/// <summary>
		/// Convert the Baggage Arrival Time in (HHMM) to Date Time with reference to flight best date time.
		/// </summary>
		/// <param name="flightId">flight id in iTrek database</param>
		/// <param name="hhmm">Time in HHMM format</param>
		/// <param name="dsFlight">flight dataset for the given flight id</param>
		/// <returns>Timing in Date Time format</returns>
		public static DateTime BagArrConvTime(string flightId, string hhmm, ref DSFlight dsFlight)
		{
			DateTime dt = new DateTime(1, 1, 1);
			try
			{
				if (hhmm != "")
				{
					if (UtilTime.IsValidHHmm(hhmm))
					{
						dsFlight = RetrieveFlightsByFlightId(flightId);
						string refUfisDt = "";
						if ((dsFlight != null) && (dsFlight.FLIGHT != null) && (dsFlight.FLIGHT.Rows.Count == 1))
						{
							DSFlight.FLIGHTRow row = (DSFlight.FLIGHTRow)dsFlight.FLIGHT.Rows[0];
							refUfisDt = row.FLIGHT_DT;
						}
						else
						{
							throw new ApplicationException("No flight information.");
						}

						if (string.IsNullOrEmpty(refUfisDt)) throw new ApplicationException("Unable to get flight date time.");

						DateTime refDt = UtilTime.ConvUfisTimeStringToDateTime(refUfisDt);
						//stDt = UtilTime.ConvDateTimeToUfisFullDTString(UtilTime.CompDateTimeFromRef(refDt, hhmm, 6));
						dt = UtilTime.CompDateTimeFromRef(refDt, hhmm, 6);
					}
					else
					{
						throw new ApplicationException("Invalid Time Format.");
					}
				}
			}
			catch (ApplicationException)
			{
				throw;
			}
			catch (Exception)
			{
				throw new ApplicationException("Time Format Error.");
			}
			return dt;
		}

		#region Update Original Flight Info

		/// <summary>
		/// Update Original Flight Information (Bulk/Batch) coming from UFIS Back-end to iTrek Database
		/// </summary>
		/// <param name="cmd">Command. Pass Command, if other transaction(s) to be done together. Otherwise, pass 'null'</param>
		/// <param name="dsOrgFlightXml">Original Flight XML dataset</param>
		/// <param name="userId">user id who update the info</param>
		public static void UpdateOriginalFlightInfo(IDbCommand cmd, DataSet dsOrgFlightXml, string userId)
		{
			DateTime dt1 = DateTime.Now;
			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			bool isNewCmd = false;//Is command 'cmd' created from this method?
			int cntUpd, cntIns, cntDel, cntExistingRecords;

			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = db.GetCommand(true);
				}
				//dbFlight.UpdateOrgFlightInfo(cmd, dsOrgFlightXml, userId, ITrekTime.GetCurrentDateTime());
				dbFlight.UploadOrgFlightDataToDb(cmd, dsOrgFlightXml,
				   userId, ITrekTime.GetCurrentDateTime(),
				   out cntIns, out cntUpd, out cntDel, out cntExistingRecords);
				commit = true;
			}
			catch (Exception ex)
			{
				LogMsg("UpdateOriginalFlightInfo:Err:" + ex.Message);
				throw;
			}
			finally
			{
				if (isNewCmd)
				{
					db.CloseCommand(cmd, commit);
				}
			}
			DateTime dt2 = DateTime.Now;
			string st = string.Format("UpdateOriginalFlightInfo:CntXml={0},Existing={1},Insert={2}, Update={3}, Delete={4},Elapsed Time:{5}",
			   dsOrgFlightXml.Tables[0].Rows.Count,
			   cntExistingRecords,
			   cntIns, cntUpd, cntDel,
			   UtilTime.ElapsedTime(dt1, dt2));
			LogTraceMsg(st);
		}

		/// <summary>
		/// Generate Given Number of Flight Id.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="cnt">Flight Id Count to create</param>
		/// <returns>Generated flight Ids</returns>
		private static string GenFlightIds(IDbCommand cmd, int cnt)
		{
			DateTime dt1 = DateTime.Now;
			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			bool isNewCmd = false;//Is command 'cmd' created from this method?

			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = db.GetCommand(true);
				}
				//dbFlight.UpdateOrgFlightInfo(cmd, dsOrgFlightXml, userId, ITrekTime.GetCurrentDateTime());
				//for (int i = 0; i < cnt; i++)
				//   dbFlight.GenNewFlightId(cmd, "1243234", "");
				UtilDb.GenUrids(cmd, "ITK_FLIGHT", false, DateTime.Now, cnt);
				commit = true;
			}
			catch (Exception ex)
			{
				LogMsg("UpdateOriginalFlightInfo:Err:" + ex.Message);
				throw;
			}
			finally
			{
				if (isNewCmd)
				{
					db.CloseCommand(cmd, commit);
				}
			}
			DateTime dt2 = DateTime.Now;
			string st = string.Format("GenFlightIds:cnt:{0},Elapsed Time:{1}",
			   cnt,
			   UtilTime.ElapsedTime(dt1, dt2));
			LogTraceMsg(st);

			return st;
		}

		/// <summary>
		/// Update Original Flight Info (Update a record) coming from back-end to iTrek Database.
		/// If flight record is not existed in iTrek, it will not update.
		/// </summary>
		/// <param name="cmd">Command. Pass Command, if other transaction(s) to be done together. Otherwise, pass 'null'</param>
		/// <param name="updFlightXml">Update Flight Xml for a record</param>
		/// <param name="userId">user id who update this info</param>
		public static void UpdateOriginalFlightInfoUpd(IDbCommand cmd, string updFlightXml, string userId, out bool hasRecordInItrek,out string flightId)
		{
			////string xml = "<FLIGHT><URNO>71473768</URNO><ACTION>UPDATE</ACTION><ETAI>20081031222300</ETAI></FLIGHT>";
			////updFlightXml = xml;
			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			bool isNewCmd = false;//Is command 'cmd' created from this method?
			hasRecordInItrek = false;
            flightId = "";
			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = db.GetCommand(true);
				}
				dbFlight.UpdateOrgFlightInfoUpd(cmd, updFlightXml, userId, ITrekTime.GetCurrentDateTime(), hasRecordInItrek,out flightId);
				commit = true;
			}
			catch (Exception ex)
			{
				LogMsg("UpdateOriginalFlightInfo:Err:" + ex.Message);
				throw;
			}
			finally
			{
				if (isNewCmd)
				{
					db.CloseCommand(cmd, commit);
				}
                //if (commit)
                //{
                //    CtrlWAPPush.PushFlightXMLChangesMsgAlertToAO(updFlightXml,flightId);
                //}
			}
		}

		/// <summary>
		/// Update Original Flight Info (Insert a record) coming from back-end to iTrek Database
		/// </summary>
		/// <param name="cmd">Command. Pass Command, if other transaction(s) to be done together. Otherwise, pass 'null'</param>
		/// <param name="insFlightXml">Insert Flight Xml for a record</param>
		/// <param name="userId">user id who update this info</param>
		public static void UpdateOriginalFlightInfoIns(IDbCommand cmd, string insFlightXml, string userId)
		{
			////string xml = "<FLIGHT><URNO>315759899</URNO><ACTION>INSERT</ACTION><FLNO></FLNO><STOD></STOD><STOA>20081101214500</STOA><REGN>VPBIP</REGN><DES3>SIN</DES3><ORG3>CGK</ORG3><GTA1></GTA1><GTD1></GTD1><ADID>A</ADID><PSTA></PSTA><PSTD></PSTD><BLT1></BLT1><OFBL></OFBL><ONBL></ONBL><TTYP>EXE</TTYP><ACT3>LF5</ACT3><TURN></TURN></FLIGHT>";
			////insFlightXml = xml;
			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			bool isNewCmd = false;//Is command 'cmd' created from this method?
			DataSet ds = UtilDataSet.LoadDataFromXmlString(insFlightXml);

			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = db.GetCommand(true);
				}
				//dbFlight.UpdateOrgFlightInfo(cmd, ds, userId, ITrekTime.GetCurrentDateTime());20090311
				UpdateOriginalFlightInfo(cmd, ds, userId);
				commit = true;
			}
			catch (Exception ex)
			{
				LogMsg("UpdateOriginalFlightInfo:Err:" + ex.Message);
				throw;
			}
			finally
			{
				if (isNewCmd)
				{
					db.CloseCommand(cmd, commit);
				}
			}
		}

		/// <summary>
		/// Update Original Flight Info (Delete a record) coming from back-end to iTrek Database
		/// </summary>
		/// <param name="cmd">Command. Pass Command, if other transaction(s) to be done together. Otherwise, pass 'null'</param>
		/// <param name="flightId">flight id in iTrek Database</param>
		/// <param name="userId">user id who update this info</param>
		public static void UpdateOriginalFlightInfoDel(IDbCommand cmd, string flightId, string userId)
		{
			bool isNewCmd = false;//Is command 'cmd' created from this method?
			bool commit = false;
			UtilDb db = UtilDb.GetInstance();

			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = db.GetCommand(true);
				}
				dbFlight.UpdateOrgFlightInfoDelete(cmd, flightId, userId, ITrekTime.GetCurrentDateTime());
				commit = true;
			}
			catch (Exception ex)
			{
				LogMsg("UpdateOriginalFlightInfo:Err:" + ex.Message);
				throw;
			}
			finally
			{
				if (isNewCmd)
				{
					db.CloseCommand(cmd, commit);
				}
			}
		}
		#endregion

		#region Conversion for UAFT (back-end flight id) and Flight id (iTrek flight id)
		/// <summary>
		/// Get Flight Id (iTrek Flight Id) for given UAFT (back-end Flight Id) with given flight datetime (in yyyymmddHHMMSS e.g. 20090130213200)
		/// </summary>
		/// <param name="cmd">Command. Pass Command, if other transaction(s) to be done together. Otherwise, pass 'null'</param>
		/// <param name="stUaft">UAFT (flight id from back-end)</param>
		/// <param name="flightDt">Flight Date Time in yyyymmddHHMMSS (e.g. 20090130214500)</param>
		/// <param name="hasData">Has this flight info in iTrek Database</param>
		/// <param name="adid">Arrival or Departure Indicator</param>
		/// <returns>Flight Id in iTrek Database</returns>
		public static string GetFlightIdForUaft(IDbCommand cmd, string stUaft, string flightDt, out bool hasData, out string adid)
		{
			string flightId = "";
			hasData = false;
			adid = "";

			bool isNewCmd = false;//Is command 'cmd' created from this method?
			bool commit = false;
			UtilDb db = UtilDb.GetInstance();

			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = db.GetCommand(false);
				}
				flightId = dbFlight.GetITrekFlightIdForUaft(cmd, stUaft, flightDt, out hasData, out adid);
                cmd.Parameters.Clear();
				commit = true;
			}
			catch (Exception ex)
			{
				LogMsg("UpdateOriginalFlightInfo:Err:" + ex.Message);
				throw;
			}
			finally
			{
				if (isNewCmd)
				{
					db.CloseCommand(cmd, commit);
				}
			}
			return flightId;
		}
		#endregion

		/// <summary>
		/// Get UFIS Urno for ITREK Urno
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="flightId">ITREK Urno for Flight</param>
		/// <returns></returns>
		public static string GetUaftForFlightId(IDbCommand cmd, string flightId)
		{


			bool isNewCmd = false;//Is command 'cmd' created from this method?
			bool commit = false;
			UtilDb db = UtilDb.GetInstance();
			string uaft = "";
			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = db.GetCommand(false);
				}
				uaft = dbFlight.GetUaftForITrekFlightId(cmd, flightId);
				commit = true;
			}
			catch (Exception ex)
			{
				LogMsg("UpdateOriginalFlightInfo:Err:" + ex.Message);
				throw;
			}
			finally
			{
				if (isNewCmd)
				{
					db.CloseCommand(cmd, commit);
				}
			}
			return uaft;
		}


		/// <summary>
		/// Load flight data in DSFlight for given iTrek FlightId
		/// </summary>
		/// <param name="flightId"></param>
		/// <returns></returns>
		public static DSFlight LoadFlightData(string flightId)
		{
			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			IDbCommand cmd = null;
			DSFlight ds = new DSFlight();

			try
			{
				cmd = db.GetCommand(false);
				dbFlight.LoadData(cmd, ref ds, flightId);
				commit = true;
			}
			catch (Exception ex)
			{
				LogMsg("UpdateOriginalFlightInfo:Err:" + ex.Message);
				throw;
			}
			finally
			{
				db.CloseCommand(cmd, commit);
			}
			return ds;
		}

		/// <summary>
		/// Update Ao Id for the flight in giving hashtable 
		/// with flightId (URNO of ITK_FLIGHT) as Key 
		/// and AoId (StaffId - URNO of ITK_STAFF) as Value.
		/// </summary>
		/// <param name="conn">connection to use if the caller has open connection. null ==> get new connection</param>
		/// <param name="htFlightAoId">HashTable of FlightId,Aoid</param>
		/// <param name="userId">user/process Id who update this info</param>
		/// <returns>true-Successfull update all</returns>
        //public static bool UpdateAoIds(IDbConnection conn, Hashtable htFlightAoId, string userId)
        public static bool UpdateAoIds(IDbConnection conn, Hashtable htFlightAoId, string userId)
        {
            bool successAll = false;
            UtilDb udb = UtilDb.GetInstance();
            bool isNewConn = false;
            if (conn == null)
            {
                conn = udb.GetConnection(true);
                isNewConn = true;
            }

            if ((conn.State == ConnectionState.Broken) || (conn.State == ConnectionState.Closed))
            {
                conn.Open();
            }

            try
            {
                int errCnt = 0;
                successAll = DBDFlight.GetInstance().UpdateFlightAoIds(conn, htFlightAoId, userId, out errCnt);
                if (errCnt > 0) successAll = false;
            }
            catch (Exception ex)
            {
                LogMsg("UpdateOriginalFlightInfo:Err:" + ex.Message);
                throw;
            }
            finally
            {
                if (isNewConn)
                {
                    conn.Close();
                    conn.Dispose();
                }
            }
            return successAll;
        }

        private static string GetFromItrekUpdateQueueName()
        {
            return ITrekPath.GetInstance().GetFROMITREKUPDATEQueueName();
        }

		/// <summary>
		/// Update Trip Related Information in Flight Table.
		/// </summary>
		/// <param name="cmd">Command</param>
		/// <param name="flightId">Flight Id</param>
		/// <param name="tripInfo">Trip Info in Trip Type and Timing in yyymmddhhmmss as key,value pair</param>
		/// <param name="uldCnt">Total Number of Uld. for the flight. Pass 'null' if no changes in total number of uld</param>
		/// <param name="userId">User Id who update this info</param>
		/// <param name="updateDateTime">Update Date and Time</param>
		/// <returns></returns>
		public static bool UpdateFlightInfoForTrip(IDbCommand cmd, string flightId, 
			Dictionary<ENT.EnTripTypeSendReceive, string> tripInfo, int? uldCnt,
			string userId, DateTime updateDateTime)
		{
			bool success = false;

			if ((tripInfo.Count < 1) && (uldCnt == null))
			{
				LogTraceMsg("UpdateFlightInfoForTrip:" + flightId + ", Nothing to update.");
				success = true;
			}
			else
			{
				UtilDb db = UtilDb.GetInstance();
				bool commit = false;
				bool isNewCmd = false;//Is command 'cmd' created from this method?

				try
				{
					if (cmd == null)
					{
						isNewCmd = true;
						cmd = db.GetCommand(true);
					}
					List<string> flightIds = new List<string>();
					flightIds.Add(flightId);
					DsDbFlight ds = DBDFlight.RetrieveDsDbFlightFromDb(cmd, flightIds);
					ds.AcceptChanges();

					DsDbFlight.ITK_FLIGHTRow[] rows = (DsDbFlight.ITK_FLIGHTRow[])ds.ITK_FLIGHT.Select("URNO='" + flightId + "'");
					if ((rows == null) || (rows.Length < 1)) throw new ApplicationException("No flight record for id " + flightId);
					DsDbFlight.ITK_FLIGHTRow row = rows[0];
					string adid = row.ADID.Trim().ToUpper();               
                    string stFlightType = adid;
                    string uaft = "";
                    try
                    {
                        uaft = row.UAFT.Trim();
                    }
                    catch (Exception)
                    {
                    }
                    string stFieldName = "";
                    string qName = GetFromItrekUpdateQueueName();

                    if ((tripInfo != null) && (tripInfo.Count > 0))
                    {
                        if (adid == "A")
                        {//Arrival Flight
                            foreach (ENT.EnTripTypeSendReceive id in tripInfo.Keys)
                            {
                                string stVal = "";
                                stFieldName = "";
                                if (tripInfo[id] != null) stVal = (string)tripInfo[id];
                                switch (id)
                                {
                                    case EnTripTypeSendReceive.SFTRPID:
                                        row.FTRIP = UtilDb.ConvString2DBDateTime(stVal);
                                        stFieldName = AA_FIRST_TRIP_TIME_TAG;
                                        break;
                                    case EnTripTypeSendReceive.SLTRPID:
                                        row.LTRIP = UtilDb.ConvString2DBDateTime(stVal);
                                        stFieldName = AA_LAST_TRIP_TIME_TAG;
                                        break;
                                    case EnTripTypeSendReceive.RFTRPID:
                                        row.FTRIPB = UtilDb.ConvString2DBDateTime(stVal);
                                        stFieldName = BA_FRIST_TRIP_TIME_TAG;
                                        break;
                                    case EnTripTypeSendReceive.RLTRPID:
                                        row.LTRIPB = UtilDb.ConvString2DBDateTime(stVal);
                                        stFieldName = BA_LAST_TRIP_TIME_TAG;
                                        break;
                                    default:
                                        break;
                                }

                                if (!string.IsNullOrEmpty(stFieldName))
                                {
                                    string msgToBackEnd = CMsgOut.MsgForFlightTiming(uaft, stFlightType, userId, stFieldName, stVal);
                                    string stReplyTo = "";
                                    string msgOutUrno = CMsgOut.NewMessageToSend(cmd,
                                       qName, msgToBackEnd, userId,
                                       CMsgOut.MSG_TYPE_FLIGHT_TIMING_UPD,
                                       out stReplyTo);
                                }
                            }
                        }
                        else
                        {//Departure Flight
                            foreach (ENT.EnTripTypeSendReceive id in tripInfo.Keys)
                            {
                                string stVal = "";
                                stFieldName = "";
                                if (tripInfo[id] != null) stVal = (string)tripInfo[id];
                                switch (id)
                                {
                                    case EnTripTypeSendReceive.SFTRPID:
                                        row.FTRIPB = UtilDb.ConvString2DBDateTime(stVal);
                                        stFieldName = BD_FIRST_TRIP_TIME_TAG;
                                        break;
                                    case EnTripTypeSendReceive.SLTRPID:
                                        row.LTRIPB = UtilDb.ConvString2DBDateTime(stVal);
                                        stFieldName = BD_LAST_TRIP_TIME_TAG;
                                        break;
                                    case EnTripTypeSendReceive.RFTRPID:
                                        row.FTRIP = UtilDb.ConvString2DBDateTime(stVal);
                                        break;
                                    case EnTripTypeSendReceive.RLTRPID:
                                        row.LTRIP = UtilDb.ConvString2DBDateTime(stVal);
                                        break;
                                    default:
                                        break;
                                }

                                if (!string.IsNullOrEmpty(stFieldName))
                                {
                                    string msgToBackEnd = CMsgOut.MsgForFlightTiming(uaft, stFlightType, userId, stFieldName, stVal);
                                    string stReplyTo = "";
                                    string msgOutUrno = CMsgOut.NewMessageToSend(cmd,
                                       qName, msgToBackEnd, userId,
                                       CMsgOut.MSG_TYPE_FLIGHT_TIMING_UPD,
                                       out stReplyTo);
                                }

                            }
                        }
                    }
					if (uldCnt != null)
					{
						SetBMTiming(row, uldCnt.Value);
					}
					DataTable dt = (ds.ITK_FLIGHT.GetChanges());
					if (dt != null)
					{
						dbFlight.SaveDsDbFlight(cmd, ds, userId, updateDateTime);
					}
					commit = true;
				}
				catch (Exception ex)
				{
					LogMsg("UpdateFlightInfoForTrip:Err:" + ex.Message);
					throw;
				}
				finally
				{
					if (isNewCmd)
					{
						//db.CloseCommand(cmd, commit);
						CloseCmdAndRefreshFlightAfterUpdFlightInfo(cmd, commit, flightId);
					}
				}
				success = commit;
			}

			return success;
		}

		/// <summary>
		/// Set Benchmark Timing and Light/Heavy Loader for flight in given row.
		/// </summary>
		/// <param name="row">row of the flight for which Timing and indicator will be updated.</param>
		/// <param name="cntContainer">total number of container for this flight</param>
		private static void SetBMTiming(DsDbFlight.ITK_FLIGHTRow row, int cntContainer)
		{		
			row.AP_FST_BM = CtrlConfig.GetBmApronFirstBag();
			row.BG_FST_BM = CtrlConfig.GetBmBaggageFirstBag();

			//Light Loader ==> 6 containers and below
			//Heavy Loader ==> 7 containers and above
			if (cntContainer <= 6)
			{ //Light Loader 
				//row.AP_LST_BM = Convert.ToInt32(ITPath.GetBMApronLastBagLightLoad());
				row.AP_LST_BM = CtrlConfig.GetBMApronLastBagLightLoad();
				//row.BG_LST_BM = Convert.ToInt32(ITPath.GetBMBaggageLastBagLightLoad());
				row.BG_LST_BM = CtrlConfig.GetBMBaggageLastBagLightLoad();
				row.L_H = "L";
			}
			else
			{ //Heavy Loader
				//row.AP_LST_BM = Convert.ToInt32(ITPath.GetBMApronLastBagHeavyLoad());
				row.AP_LST_BM = CtrlConfig.GetBMApronLastBagHeavyLoad();
				//row.BG_LST_BM = Convert.ToInt32(ITPath.GetBMBaggageLastBagHeavyLoad());
				row.BG_LST_BM = CtrlConfig.GetBMBaggageLastBagHeavyLoad();
				row.L_H = "H";
			}
		}

		/// <summary>
		/// Retrieve Flight Ids for given List of Uafts. 
		/// Return Dictionary Map with uaft as Key and Flight as value.
		/// </summary>
		/// <param name="cmd">Command to connect to database</param>
		/// <param name="lsUaft">List of Uaft</param>
		/// <returns></returns>
		public static Dictionary<String, String> RetrieveFlidsForUafts(IDbCommand cmd, List<string> lsUafts)
		{
			Dictionary<String, String> mapUaftsToFlids = null;
			if (lsUafts != null)
			{
				UtilDb udb = UtilDb.GetInstance();
				bool isNewCmd = false;
				bool commit = false;

				try
				{
					if (cmd == null)
					{
						cmd = udb.GetCommand(false);
						isNewCmd = true;
					}

					mapUaftsToFlids = DBDFlight.GetInstance().RetrieveFlidsForUafts(cmd, lsUafts);
				}
				catch (Exception ex)
				{
					LogMsg("RetrieveFlidsForUafts:Err:" + ex.Message);
					throw;
				}
				finally
				{
					if (isNewCmd)
					{
						udb.CloseCommand(cmd, commit);
					}
				}
			}
			return mapUaftsToFlids;
		}

		/// <summary>
		/// Do Conversion of Data from SummFlight.XML to iTrek Database
		/// </summary>
		public static void DoXmlToDbConv()
		{
			UtilDb udb = UtilDb.GetInstance();
			bool isNewCmd = false;
			bool commit = false;
			IDbCommand cmd = null;

			try
			{
				if (cmd == null)
				{
					cmd = udb.GetCommand(false);
					isNewCmd = true;
				}
				string stSummFlightXmlPath = @"C:\TEMP\SummFlight.XML";
				//iTXMLPathscl path = new iTXMLPathscl();
				//stSummFlightXmlPath = path.GetSummFlightsFilePath();

				DBDFlight dbFlight = DBDFlight.GetInstance();

				DSFlight dsOld = new DSFlight();

				dsOld.ReadXml(stSummFlightXmlPath);
				DsDbFlight dsNew = dbFlight.ConvDsFlightToDsDbFlight(cmd, dsOld);
				dbFlight.SaveDsDbFlight(cmd, dsNew, "conv", ITrekTime.GetCurrentDateTime());
			}
			catch (Exception ex)
			{
				LogMsg("RetrieveFlidsForUafts:Err:" + ex.Message);
				throw;
			}
			finally
			{
				if (isNewCmd)
				{
					udb.CloseCommand(cmd, commit);
				}
			}
		}
	}
}