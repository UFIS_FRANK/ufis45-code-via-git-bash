#define TESTING_ON
#define TRACING_ON
#define LOGGING_ON
#define USING_MQ


using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.IO;
using System.Xml;
using System.Data;
using System.Diagnostics;
using System.Collections;

using iTrekXML;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.Util;
using iTrekWMQ;

namespace UFIS.ITrekLib.DB
{
    public class DBBagPresentTimingReport
    {
        private static Object _lockObj = new Object();//to use in synchronisation of single access
        private static Object _lockObjBagPresentTimingReportHdr = new Object();//to use in synchronisation of single access
        private static Object _lockObjBagPresentTimingReportDet = new Object();//to use in synchronisation of single access
        private static DBBagPresentTimingReport _dbBagPresentTimingReport = null;

        private static readonly DateTime DEFAULT_DT = new System.DateTime(1, 1, 1);
        private static DateTime _lastBagPresentTimingReportHdrWriteDT = DEFAULT_DT;
        private static DateTime _lastBagPresentTimingReportDetWriteDT = DEFAULT_DT;

        private static DSBagPresentTimingRpt _dsBagPresentTimingReport = null;
        private static string _fnBagPresentTimingReportHdrXml = null;//file name of BagPresentTimingReport XML in full path.
        private static string _fnBagPresentTimingReportDetXml = null;//file name of BagPresentTimingReport Details XML in full path.

        /// <summary>
        /// Singleton Pattern.
        /// Not allowed to create the object from other class.
        /// To get the object, user "GetInstance()" method.
        /// </summary>
        private DBBagPresentTimingReport() { }

        public void Init()
        {//Initialize all the information, to get the latest data when fetching them again.
            _dsBagPresentTimingReport = null;
            _fnBagPresentTimingReportHdrXml = null;
            _fnBagPresentTimingReportDetXml = null;
            _lastBagPresentTimingReportHdrWriteDT = DEFAULT_DT;
            _lastBagPresentTimingReportDetWriteDT = DEFAULT_DT;
        }

        /// <summary>
        /// Get the Instance of DBBagPresentTimingReport. There will be only one reference for all the clients
        /// </summary>
        /// <returns></returns>
        public static DBBagPresentTimingReport GetInstance()
        {
            if (_dbBagPresentTimingReport == null)
            {
                _dbBagPresentTimingReport = new DBBagPresentTimingReport();
            }
            return _dbBagPresentTimingReport;
        }

        public DSBagPresentTimingRpt RetrieveBagPresentTimingReportForAFlight(string flightUrno)
        {
            DSBagPresentTimingRpt ds = new DSBagPresentTimingRpt();
            DSBagPresentTimingRpt tempDs = dsBagPresentTimingReport;
            foreach (DSBagPresentTimingRpt.BPTRHDRRow row in tempDs.BPTRHDR.Select("UAFT='" + flightUrno + "'"))
            {
                ds.BPTRHDR.LoadDataRow(row.ItemArray, true);
            }
            try
            {
                if (tempDs.BPTRDET != null)
                {
                    foreach (DSBagPresentTimingRpt.BPTRDETRow row in tempDs.BPTRDET.Select("UAFT='" + flightUrno + "'"))
                    {
                        ds.BPTRDET.LoadDataRow(row.ItemArray, true);
                    }
                }

            }
            catch (Exception ex)
            {
                LogMsg("RetrieveBagPresentTimingReportForAFlight:Error:" + flightUrno + ":" + ex.Message);
            }
            return ds;
        }

        private string fnBagPresentTimingReportHdrXml
        {
            get
            {
                if ((_fnBagPresentTimingReportHdrXml == null) || (_fnBagPresentTimingReportHdrXml == ""))
                {
                    iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
                    _fnBagPresentTimingReportHdrXml = path.GetBagPresentTimingReportHdrFilePath();
                }

                return _fnBagPresentTimingReportHdrXml;
            }
        }

        private string fnBagPresentTimingReportDetXml
        {
            get
            {
                if ((_fnBagPresentTimingReportDetXml == null) || (_fnBagPresentTimingReportDetXml == ""))
                {
                    iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
                    _fnBagPresentTimingReportDetXml = path.GetBagPresentTimingReportDetFilePath();
                }

                return _fnBagPresentTimingReportDetXml;
            }
        }

        private DSBagPresentTimingRpt dsBagPresentTimingReport
        {
            get
            {
                //if (_dsBagPresentTimingReport == null) { LoadDSBagPresentTimingRpt(); }
                LoadDSBagPresentTimingRpt();
                DSBagPresentTimingRpt ds = (DSBagPresentTimingRpt)_dsBagPresentTimingReport.Copy();

                return ds;
            }
        }

        private void LoadDSBagPresentTimingRpt()
        {
            if (IsNeedToUpdateData())
            {
                lock (_lockObj)
                {
                    try
                    {
                        if (IsNeedToUpdateData())
                        {
                            if (_dsBagPresentTimingReport == null) _dsBagPresentTimingReport = new DSBagPresentTimingRpt();
                            LoadDSBagPresentTimingRpt_BagPresentTimingReportHeaderTable();
                            LoadDSBagPresentTimingRpt_BagPresentTimingReportDetailTable();
                        }
                    }
                    catch (Exception ex)
                    {
                        LogMsg("LoadDSBagPresentTimingRpt:Err:" + ex.Message);
                    }
                }
            }
        }

        private bool IsNeedToUpdateData()
        {
            bool needToUpdate = true;
            if ((_dsBagPresentTimingReport == null) ||
                (IsNeedToUpdateBagPresentTimingReportHdrTable()) ||
                (IsNeedToUpdateBagPresentTimingReportDetTable())) needToUpdate = true;
            else needToUpdate = false;

            return needToUpdate;
        }

        private bool IsNeedToUpdateBagPresentTimingReportHdrTable()
        {
            bool needToUpdate = false;
            FileInfo fiXml = new FileInfo(fnBagPresentTimingReportHdrXml);
            DateTime curDT = fiXml.LastWriteTime;
            if (curDT.CompareTo(_lastBagPresentTimingReportHdrWriteDT) != 0) needToUpdate = true;
            return needToUpdate;
        }

        private bool IsNeedToUpdateBagPresentTimingReportDetTable()
        {
            bool needToUpdate = false;
            FileInfo fiXml = new FileInfo(fnBagPresentTimingReportDetXml);
            DateTime curDT = fiXml.LastWriteTime;
            if (curDT.CompareTo(_lastBagPresentTimingReportDetWriteDT) != 0) needToUpdate = true;
            return needToUpdate;
        }


        private void LoadDSBagPresentTimingRpt_BagPresentTimingReportHeaderTable()
        {
            FileInfo fiXml = new FileInfo(fnBagPresentTimingReportHdrXml);
            DateTime curDT = fiXml.LastWriteTime;
            if (!fiXml.Exists)
            {
                //throw new ApplicationException("BagPresentTimingReport Header information missing. Please inform to administrator.");
                if (_dsBagPresentTimingReport == null)
                {
                    _dsBagPresentTimingReport = new DSBagPresentTimingRpt();
                }
                return;
            }

            if ((_dsBagPresentTimingReport == null) || (curDT.CompareTo(_lastBagPresentTimingReportHdrWriteDT) != 0))
            {
                lock (_lockObjBagPresentTimingReportHdr)
                {
                    try
                    {
                        curDT = fiXml.LastWriteTime;//check the time again, in case of lock
                        if ((_dsBagPresentTimingReport == null) || (curDT.CompareTo(_lastBagPresentTimingReportHdrWriteDT) != 0))
                        {
                            try
                            {
                                DSBagPresentTimingRpt tempDs = new DSBagPresentTimingRpt();
                                tempDs.BPTRHDR.ReadXml(fnBagPresentTimingReportHdrXml);
                                if (_dsBagPresentTimingReport == null)
                                {
                                    _dsBagPresentTimingReport = new DSBagPresentTimingRpt();
                                }
                                else
                                {
                                    _dsBagPresentTimingReport.BPTRHDR.Clear();
                                }
                                _dsBagPresentTimingReport.BPTRHDR.Merge(tempDs.BPTRHDR);
                                _lastBagPresentTimingReportHdrWriteDT = curDT;
                                LogMsg("Load Hdr");
                            }
                            catch (Exception ex)
                            {
                                LogMsg("LoadDSBagPresentTimingRpt_BagPresentTimingReportHeaderTable : Error : " + ex.Message);
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        LogMsg("LoadDSBagPresentTimingRpt:Err:" + ex.Message);
                    }
                }
            }
        }

        private void LoadDSBagPresentTimingRpt_BagPresentTimingReportDetailTable()
        {
            FileInfo fiXml = new FileInfo(fnBagPresentTimingReportDetXml);
            DateTime curDT = fiXml.LastWriteTime;
            if (!fiXml.Exists)
            {
                //throw new ApplicationException("BagPresentTimingReport Detail information missing. Please inform to administrator.");
                if (_dsBagPresentTimingReport == null)
                {
                    _dsBagPresentTimingReport = new DSBagPresentTimingRpt();
                }
                return;
            }

            if ((_dsBagPresentTimingReport == null) || (curDT.CompareTo(_lastBagPresentTimingReportDetWriteDT) != 0))
            {
                lock (_lockObjBagPresentTimingReportDet)
                {
                    try
                    {
                        curDT = fiXml.LastWriteTime;//check the time again, in case of lock
                        if ((_dsBagPresentTimingReport == null) || (curDT.CompareTo(_lastBagPresentTimingReportDetWriteDT) != 0))
                        {
                            try
                            {
                                DSBagPresentTimingRpt tempDs = new DSBagPresentTimingRpt();
                                tempDs.BPTRDET.ReadXml(fnBagPresentTimingReportDetXml);
                                if (_dsBagPresentTimingReport == null)
                                {
                                    _dsBagPresentTimingReport = new DSBagPresentTimingRpt();
                                }
                                else
                                {
                                    _dsBagPresentTimingReport.BPTRDET.Clear();
                                }
                                _dsBagPresentTimingReport.BPTRDET.Merge(tempDs.BPTRDET);
                                _lastBagPresentTimingReportDetWriteDT = curDT;
                            }
                            catch (Exception ex)
                            {
                                LogMsg("LoadDSBagPresentTimingRpt_BagPresentTimingReportDetailTable : Error : " + ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogMsg("LoadDSBagPresentTimingRpt_BagPresentTimingReportDetailTable:Err:" + ex.Message);
                    }
                }
            }
        }

        public void SaveRpt(string flightUrno, DSBagPresentTimingRpt dsToSave)
        {
            lock (_lockObj)
            {
                try
                {
                    DSBagPresentTimingRpt dsTemp = dsBagPresentTimingReport;
                    SaveHdr(flightUrno, dsToSave, dsTemp);
                    SaveDet(flightUrno, dsToSave, dsTemp);
					//SaveDataToBackend(flightUrno, dsToSave);
                }
                catch (Exception ex)
                {
                    LogMsg("SaveRpt:Err:" + ex.Message);
                }
            }
        }

		//private void SaveDataToBackend(string flightUrno, DSBagPresentTimingRpt dsToSave)
		//{
		//    StringBuilder sbXml = new StringBuilder();
		//    sbXml.Append("<BPTRPTS>");
		//    foreach (DSBagPresentTimingRpt.BPTRHDRRow row in dsToSave.BPTRHDR.Select("UAFT='" + flightUrno + "'"))
		//    {
		//        sbXml.Append("<BPTR>");
		//        sbXml.Append("  <UAFT>" + row.URNO + "</UAFT>");
		//        sbXml.Append("  <FLDT>" + row.FLDT + "</FLDT>");
		//        sbXml.Append("  <AT>" + row.AT + "</AT>");
		//        sbXml.Append("  <FLNU>" + row.FLNU + "</FLNU>");
		//        sbXml.Append("  <ACFT_TYPE>" + row.ACFT_TYPE + "</ACFT_TYPE>");
		//        sbXml.Append("  <BELT>" + row.BELT + "</BELT>");
		//        sbXml.Append("  <PARK_STAND>" + row.PARK_STAND + "</PARK_STAND>");
		//        sbXml.Append("  <AO_ID>" + row.AO_ID + "</AO_ID>");
		//        sbXml.Append("  <AO_FNAME>" + row.AO_FNAME + "</AO_FNAME>");
		//        sbXml.Append("  <TBS_UP>" + row.TBS_UP + "</TBS_UP>");
		//        sbXml.Append("  <FTRIP_B>" + row.FTRIP_B + "</FTRIP_B>");
		//        sbXml.Append("  <LTRIP_B>" + row.LTRIP_B + "</LTRIP_B>");
		//        sbXml.Append("  <FBAG>" + row.FBAG + "</FBAG>");
		//        sbXml.Append("  <LBAG>" + row.LBAG + "</LBAG>");
		//        sbXml.Append("  <FBAG_TIME_MET>" + row.FBAG_TIME_MET + "</FBAG_TIME_MET>");
		//        sbXml.Append("  <LBAG_TIME_MET>" + row.LBAG_TIME_MET + "</LBAG_TIME_MET>");
		//        sbXml.Append("  <AO_TRP_RMK>" + row.AO_TRP_RMK + "</AO_TRP_RMK>");
		//        sbXml.Append("  <BO_RMK>" + row.BO_RMK + "</BO_RMK>");
		//        //sbXml.Append("  <AT_TIME>" + row.AT_TIME + "</AT_TIME>");

		//        foreach (DSBagPresentTimingRpt.BPTRDETRow detRow in dsToSave.BPTRDET.Select("UAFT='" + flightUrno + "'"))
		//        {
		//            sbXml.Append("<BPTRDET>");
		//            sbXml.Append("<UAFT>" + detRow.URNO + "</UAFT>");
		//            sbXml.Append("<SR_NO>" + detRow.SR_NO + "</SR_NO>");
		//            sbXml.Append("<ULDNO>" + detRow.ULDNO + "</ULDNO>");
		//            sbXml.Append("<ULDTIME>" + detRow.ULDTIME + "</ULDTIME>");
		//            sbXml.Append("<CLASS>" + detRow.CLASS + "</CLASS>");
		//            sbXml.Append("</BPTRDET>");
		//        }
		//        sbXml.Append("<BPTR>");
		//        sbXml.Append("<BPTRPT>");
		//        SendXmlMsgToBackend(sbXml.ToString());
		//        break;
		//    }
		//}

        private void SaveHdr(string flightUrno, DSBagPresentTimingRpt dsToSave, DSBagPresentTimingRpt dsCur)
        {
            lock (_lockObjBagPresentTimingReportHdr)
            {
                try
                {
                    //foreach (DSBagPresentTimingRpt.BPTRHDRRow row in dsCur.BPTRHDR.Select("UAFT='" + flightUrno + "'"))
                    //{
                    //    row.Delete();
                    //}
                    foreach (DSBagPresentTimingRpt.BPTRHDRRow row in dsToSave.BPTRHDR.Select("UAFT='" + flightUrno + "'"))
                    {
                        dsCur.BPTRHDR.LoadDataRow(row.ItemArray, LoadOption.Upsert);
                        break;
                    }
                    dsCur.BPTRHDR.WriteXml(fnBagPresentTimingReportHdrXml);
                }
                catch (Exception ex)
                {
                    LogMsg("SaveHdr:Err:" + ex.Message);
                }
            }
        }

        private void SaveDet(string flightUrno, DSBagPresentTimingRpt dsToSave, DSBagPresentTimingRpt dsCur)
        {
            lock (_lockObjBagPresentTimingReportDet)
            {
                try
                {
                    foreach (DSBagPresentTimingRpt.BPTRDETRow row in dsCur.BPTRDET.Select("UAFT='" + flightUrno + "'"))
                    {
                        row.Delete();
                    }
                    foreach (DSBagPresentTimingRpt.BPTRDETRow row in dsToSave.BPTRDET.Select("UAFT='" + flightUrno + "'"))
                    {
                        dsCur.BPTRDET.LoadDataRow(row.ItemArray, LoadOption.Upsert);
                    }
                    dsCur.BPTRDET.WriteXml(fnBagPresentTimingReportDetXml);
                }
                catch (Exception ex)
                {
                    LogMsg("SaveDet:Err:" + ex.Message);
                }
            }
        }

        private string GetFromITrekToSoccReportQueueName()
        {
            iTXMLPathscl paths = new iTXMLPathscl();
            return paths.GetFROMITREKRPTQueueName();
        }

        [Conditional("USING_MQ")]
        private void SendXmlMsgToBackend(string xmlMsg)
        {
            ITrekWMQClass1 mq = new ITrekWMQClass1();
            mq.putTheMessageIntoQueue(xmlMsg, GetFromITrekToSoccReportQueueName());
        }

        public void HkReport(ArrayList arrList)
        {
            int cnt = arrList.Count;
            for (int i = 0; i < cnt; i++)
            {
                DelAReport(arrList[i].ToString().Trim());
            }
        }

        private void DelAReport(string flightUrno)
        {
            if (flightUrno == "") return;

            lock (_lockObj)
            {
                try
                {
                    DSBagPresentTimingRpt dsCur = dsBagPresentTimingReport;
                    foreach (DSBagPresentTimingRpt.BPTRDETRow row in dsCur.BPTRDET.Select("UAFT='" + flightUrno + "'"))
                    {
                        row.Delete();
                    }
                    foreach (DSBagPresentTimingRpt.BPTRHDRRow row in dsCur.BPTRHDR.Select("UAFT='" + flightUrno + "'"))
                    {
                        row.Delete();
                    }
                    dsCur.AcceptChanges();
                    dsCur.BPTRDET.WriteXml(fnBagPresentTimingReportDetXml);
                    dsCur.BPTRHDR.WriteXml(fnBagPresentTimingReportHdrXml);
                }
                catch (Exception ex)
                {
                    LogMsg("DelAReport:Error:" + flightUrno + ":" + ex.Message);
                }
            }
        }

        //[Conditional("TRACING_ON")]
        private void LogTraceMsg(string msg)
        {
           try
           {
              MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("DBBagPresentTimingReport", msg);
           }
           catch (Exception)
           {
              UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DBBagPresentTimingReport", msg);
           }
        }

        //[Conditional("LOGGING_ON")]
        private void LogMsg(string msg)
        {
            //UtilLog.LogToGenericEventLog("DBBagPresentTimingReport", msg);
           LogTraceMsg(msg);
        }
    }
}
