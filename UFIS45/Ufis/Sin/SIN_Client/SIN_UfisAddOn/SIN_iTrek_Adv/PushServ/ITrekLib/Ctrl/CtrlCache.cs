using System;
using System.Collections.Generic;
using System.Text;

using System.Data;

using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.DB;
//using UFIS.GlobalCache;
using UFIS.ITrekLib.Util;
using UFIS.ITrekLib.CustomException;


namespace UFIS.ITrekLib.Ctrl
{
    public class CtrlCache
    {
        private const string CACHE_KEY_FOR_FLIGHT_SUMM = "CACHE_KEY_FOR_FLIGHT_SUMM";
        //private const string CACHE_KEY_FOR_FLIGHT_SUMM_HIST = "CACHE_KEY_FOR_FLIGHT_SUMM_HIST";
        private const string CACHE_KEY_FOR_JOB_SUMM = "CACHE_KEY_FOR_JOB_SUMM";
        private const string CACHE_KEY_FOR_JOB_SUMM_HIST = "CACHE_KEY_FOR_JOB_SUMM_HIST";
        private static readonly DateTime DEFAULT_DATETIME = new DateTime(1,1,1);

        #region //FlightSumm
        //-------------------------------------------------
        //public static void SaveCacheFlightSumm(DSFlight ds)
        //{
        //    try
        //    {
                
        //        if ((ds != null) && (ds.FLIGHT.Rows.Count > 0))
        //        {
        //            TimeSpan lifeTime = TimeSpan.FromMinutes(2);
        //            LogMsg("SaveCacheFlightSumm:" + lifeTime.ToString());
        //            GlobalCacheData.GetInstance().SetData(CACHE_KEY_FOR_FLIGHT_SUMM,
        //                MM.UtilLib.UtilDataSet.GetDataSetString(ds), lifeTime);
        //        }
        //        else
        //        {
        //            LogMsg("SaveCacheFlightSumm:No Data.");
        //            throw new NoDataException();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LogMsg("SaveCacheFlightSumm:Err:" + ex.Message);
        //    }
        //}


        //public static DSFlight RetrieveCacheFlightSumm( out DateTime lastUpdDateTime)
        //{
        //    lastUpdDateTime = DEFAULT_DATETIME;
        //    DSFlight ds = null;
        //    try
        //    {
        //        LogMsg("RetrieveCacheFlightSumm");

        //        ds = new DSFlight();
        //        MM.UtilLib.UtilDataSet.LoadDataSetFromXmlString((DataSet)ds,
        //            (string)GlobalCacheData.GetInstance().GetData(CACHE_KEY_FOR_FLIGHT_SUMM, out lastUpdDateTime));
        //    }
        //    catch (CacheException ex)
        //    {
        //        LogMsg("RetrieveCacheFlightSumm:CacheErr:" + ex.Message);
        //        ds = null;
        //        throw new CacheException();
        //    }
        //    catch (Exception ex)
        //    {
        //        LogMsg("RetrieveCacheFlightSumm:Err:" + ex.Message);
        //    }

        //    if (ds == null)
        //    {
        //        LogMsg("RetrieveCacheFlightSumm:GetDataFromFile");
        //        ds = RetrieveFlightSummFromFile();
        //        SaveCacheFlightSumm(ds);
        //        lastUpdDateTime = GetFlightSummLastUpdDateTime();
        //    }

        //    if ((ds == null) || ds.FLIGHT.Rows.Count<1)
        //    {
        //        LogMsg("RetrieveCacheFlightSumm:No Data.");
        //        throw new NoDataException();
        //    }

        //    return ds;
        //}

        //public static DateTime GetFlightSummLastUpdDateTime()
        //{
        //    return GlobalCacheData.GetInstance().GetLastUpdDateTime(CACHE_KEY_FOR_FLIGHT_SUMM);
        //}

        private static DSFlight RetrieveFlightSummFromFile()
        {
            DSFlight ds = DBFlight.GetInstance().LoadDSFlight();            
            return ds;
        }

        public static bool HasUpdFlightSummAfter(DateTime lastUpdDateTime, out DateTime newUpdDateTime, out bool hasData)
        {
            bool hasUpd = false;
            hasData = false;
            newUpdDateTime = GetFlightSummLastUpdDateTime();
            if (newUpdDateTime != DEFAULT_DATETIME)
            {
                hasData = true;
            }
            if (newUpdDateTime.CompareTo(lastUpdDateTime) > 0)
            {
                hasUpd = true;
            }
            return hasUpd;
        }

        public static bool HasUpdFlightSummAfter(DateTime lastUpdDateTime, out DateTime newUpdDateTime, out DSFlight ds, out bool hasData)
        {
            bool hasUpd = false;
            hasData = false;
            ds = null;
            LogMsg("HasUpdFlightSummAfter:In:" + UtilTime.ConvDateTimeToUfisFullDTString(lastUpdDateTime));
            hasUpd = HasUpdFlightSummAfter(lastUpdDateTime,out newUpdDateTime, out hasData);
            if (!hasData)
            {
                LogMsg("HasUpdFlightSummAfter:No Data. Retrieve Data.");
                ds = RetrieveFlightSummFromFile();
                LogMsg("HasUpdFlightSummAfter:No Data. After Retrieve Data.");
                SaveCacheFlightSumm(ds);
                LogMsg("HasUpdFlightSummAfter:No Data. After Save Data.");
                newUpdDateTime = GetFlightSummLastUpdDateTime();
                if ((ds != null) && (ds.FLIGHT.Rows.Count > 0)) hasData = true;
                if (hasData && (newUpdDateTime.CompareTo(lastUpdDateTime) >= 0))
                {
                    hasUpd = true;
                }
            }
            else if (hasUpd)
            {
                ds = RetrieveCacheFlightSumm(out newUpdDateTime);                
            }
            LogMsg("HasUpdFlightSummAfter:" + UtilTime.ConvDateTimeToUfisFullDTString(lastUpdDateTime) + ":" +
                UtilTime.ConvDateTimeToUfisFullDTString(newUpdDateTime) + ":Data," + 
                hasData.ToString() + ":Upd," + hasUpd.ToString());
            return hasUpd;
        }

        //-------------------------------------------------
        #endregion


        #region //JobSumm
        //-------------------------------------------------
        public static void SaveCacheJobSumm(DSJob ds)
        {
            try
            {
                if ((ds != null) && (ds.JOB.Rows.Count > 0))
                {
                    TimeSpan lifeTime = TimeSpan.FromMinutes(2);
                    LogMsg("SaveCacheJobSumm:" + lifeTime.ToString());
                    GlobalCacheData.GetInstance().SetData(CACHE_KEY_FOR_JOB_SUMM,
                        MM.UtilLib.UtilDataSet.GetDataSetString(ds), lifeTime);
                }
                else
                {
                    LogMsg("SaveCacheJobSumm:No Data.");
                    throw new NoDataException();
                }
            }
            catch (Exception ex)
            {
                LogMsg("SaveCacheJobSumm:Err:" + ex.Message);
            }
        }

        public static DSJob RetrieveCacheJobSumm(out DateTime lastUpdDateTime)
        {
            lastUpdDateTime = DEFAULT_DATETIME;
            DSJob ds = null;
            try
            {
                LogMsg("RetrieveCacheJobSumm");
                ds = new DSJob();
                MM.UtilLib.UtilDataSet.LoadDataSetFromXmlString(ds,
                    (string)GlobalCacheData.GetInstance().GetData(CACHE_KEY_FOR_JOB_SUMM, out lastUpdDateTime));
            }
            catch (CacheException ex)
            {
                LogMsg("RetrieveCacheJobSumm:CacheErr:" + ex.Message);
                throw new CacheException();
            }
            catch (Exception ex)
            {
                LogMsg("RetrieveCacheJobSumm:Err:" + ex.Message);
            }

            if ((ds == null) || (ds.JOB.Rows.Count<1))
            {
                LogMsg("RetrieveCacheJobSumm:GetDataFromFile.");
                ds = RetrieveJobSummFromFile();
                LogMsg("RetrieveCacheJobSumm:Saving");
                SaveCacheJobSumm(ds);
                LogMsg("RetrieveCacheJobSumm:Saved");
                lastUpdDateTime = GetJobSummLastUpdDateTime();
            }
            if ((ds == null) || (ds.JOB.Rows.Count < 1))
            {
                LogMsg("RetrieveCacheJobSumm:NoDataErr.");
                throw new NoDataException();
            }

            return ds;
        }

        public static DateTime GetJobSummLastUpdDateTime()
        {
            return GlobalCacheData.GetInstance().GetLastUpdDateTime(CACHE_KEY_FOR_JOB_SUMM);
        }

        private static DSJob RetrieveJobSummFromFile()
        {
            DSJob ds = DBJob.GetInstance().LoadDsJob_JobTable();
            return ds;
        }

        public static bool HasUpdJobSummAfter(DateTime lastUpdDateTime, out DateTime newUpdDateTime, out bool hasData)
        {
            bool hasUpd = false;
            hasData = false;
            newUpdDateTime = GetJobSummLastUpdDateTime();
            if (newUpdDateTime != DEFAULT_DATETIME)
            {
                hasData = true;
            }
            if (newUpdDateTime.CompareTo(lastUpdDateTime) > 0)
            {
                hasUpd = true;
            }
            return hasUpd;
        }

        public static bool HasUpdJobSummAfter(DateTime lastUpdDateTime, out DateTime newUpdDateTime, out DSJob ds, out bool hasData)
        {
            bool hasUpd = false;
            ds = null;

            LogMsg("HasUpdJobSummAfter:In:" + UtilTime.ConvDateTimeToUfisFullDTString(lastUpdDateTime));
            hasUpd = HasUpdJobSummAfter(lastUpdDateTime, out newUpdDateTime, out hasData);
            if (!hasData)
            {
                LogMsg("HasUpdJobSummAfter:No Data. Retrieve Data.");
                ds = RetrieveJobSummFromFile();
                LogMsg("HasUpdJobSummAfter:No Data. After Retrieve Data.");
                SaveCacheJobSumm(ds);
                LogMsg("HasUpdJobSummAfter:No Data. After Save Data.");
                newUpdDateTime = GetJobSummLastUpdDateTime();
                if ((ds != null) && (ds.JOB.Rows.Count > 0)) hasData = true;
                if (hasData && (newUpdDateTime.CompareTo(lastUpdDateTime) >= 0))
                {
                    hasUpd = true;
                }
            }
            else if (hasUpd)
            {
                ds = RetrieveCacheJobSumm(out newUpdDateTime);
            }
            LogMsg("HasUpdJobSummAfter:" + UtilTime.ConvDateTimeToUfisFullDTString(lastUpdDateTime) + ":" +
                UtilTime.ConvDateTimeToUfisFullDTString(newUpdDateTime) + ":Data," +
                hasData.ToString() + ":Upd," + hasUpd.ToString());
            return hasUpd;
        }


        //public static void SaveCacheJobSumm(DSJob.JOBDataTable dt)
        //{
        //    try
        //    {
        //        if ((dt != null) && (dt.Rows.Count > 0))
        //        {
        //            TimeSpan lifeTime = TimeSpan.FromMinutes(2);
        //            LogMsg("SaveCacheJobSumm:" + lifeTime.ToString());
        //            DSJob ds = new DSJob();
        //            ds.JOB.LoadDataRow(dt.Select(), true);
        //            GlobalCacheData.GetInstance().SetData(CACHE_KEY_FOR_JOB_SUMM,
        //                MM.UtilLib.UtilDataSet.GetDataSetString(ds), lifeTime);
        //        }
        //        else
        //        {
        //            LogMsg("SaveCacheJobSumm:No Data.");
        //            throw new NoDataException();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LogMsg("SaveCacheJobSumm:Err:" + ex.Message);
        //    }
        //}


        //public static DSJob.JOBDataTable RetrieveCacheJobSumm(out DateTime lastUpdDateTime)
        //{
        //    lastUpdDateTime = DEFAULT_DATETIME;
        //    DSJob.JOBDataTable dt = null;
        //    try
        //    {
        //        LogMsg("RetrieveCacheJobSumm");
        //        DSJob ds = new DSJob();
        //        dt = ds.JOB;
        //        MM.UtilLib.UtilDataSet.LoadDataSetFromXmlString(ds,
        //            (string)GlobalCacheData.GetInstance().GetData(CACHE_KEY_FOR_JOB_SUMM, out lastUpdDateTime));
        //    }
        //    catch (CacheException ex)
        //    {
        //        LogMsg("RetrieveCacheJobSumm:CacheErr:" + ex.Message);
        //        throw new CacheException();
        //    }
        //    catch (Exception ex)
        //    {
        //        LogMsg("RetrieveCacheJobSumm:Err:" + ex.Message);
        //    }

        //    if (dt == null)
        //    {
        //        LogMsg("RetrieveCacheJobSumm:GetDataFromFile." );
        //        dt = RetrieveJobSummFromFile();
        //        LogMsg("RetrieveCacheJobSumm:Saving");
        //        SaveCacheJobSumm(dt);
        //        LogMsg("RetrieveCacheJobSumm:Saved");
        //        lastUpdDateTime = GetJobSummLastUpdDateTime();
        //    }

        //    if ((dt == null)|| (dt.Rows.Count<1))
        //    {
        //        LogMsg("RetrieveCacheJobSumm:NoDataErr.");
        //        throw new NoDataException();
        //    }

        //    return dt;
        //}

        //public static DateTime GetJobSummLastUpdDateTime()
        //{
        //    return GlobalCacheData.GetInstance().GetLastUpdDateTime(CACHE_KEY_FOR_JOB_SUMM);
        //}

        //private static DSJob.JOBDataTable RetrieveJobSummFromFile()
        //{
        //    DSJob.JOBDataTable dt = DBJob.GetInstance().LoadDsJob_JobTable();
        //    return dt;
        //}

        //public static bool HasUpdJobSummAfter(DateTime lastUpdDateTime, out DateTime newUpdDateTime, out bool hasData)
        //{
        //    bool hasUpd = false;
        //    hasData = false;
        //    newUpdDateTime = GetJobSummLastUpdDateTime();
        //    if (newUpdDateTime != DEFAULT_DATETIME)
        //    {
        //        hasData = true;
        //    }
        //    if (newUpdDateTime.CompareTo(lastUpdDateTime) > 0)
        //    {
        //        hasUpd = true;
        //    }
        //    return hasUpd;
        //}

        //public static bool HasUpdJobSummAfter(DateTime lastUpdDateTime, out DateTime newUpdDateTime, out DSJob.JOBDataTable dt, out bool hasData)
        //{
        //    bool hasUpd = false;
        //    dt = null;

        //    LogMsg("HasUpdJobSummAfter:In:" + UtilTime.ConvDateTimeToUfisFullDTString(lastUpdDateTime));
        //    hasUpd = HasUpdJobSummAfter(lastUpdDateTime, out newUpdDateTime, out hasData);
        //    if (!hasData)
        //    {
        //        LogMsg("HasUpdJobSummAfter:No Data. Retrieve Data.");
        //        dt = RetrieveJobSummFromFile();
        //        LogMsg("HasUpdJobSummAfter:No Data. After Retrieve Data.");
        //        SaveCacheJobSumm(dt);
        //        LogMsg("HasUpdJobSummAfter:No Data. After Save Data.");
        //        newUpdDateTime = GetJobSummLastUpdDateTime();
        //        if ((dt != null) && (dt.Rows.Count > 0)) hasData = true;
        //        if (hasData && (newUpdDateTime.CompareTo(lastUpdDateTime) >= 0))
        //        {
        //            hasUpd = true;
        //        }
        //    }
        //    else if (hasUpd)
        //    {
        //        dt = RetrieveCacheJobSumm(out newUpdDateTime);
        //    }
        //    LogMsg("HasUpdJobSummAfter:" + UtilTime.ConvDateTimeToUfisFullDTString(lastUpdDateTime) + ":" +
        //        UtilTime.ConvDateTimeToUfisFullDTString(newUpdDateTime) + ":Data," +
        //        hasData.ToString() + ":Upd," + hasUpd.ToString());
        //    return hasUpd;
        //}

        //-------------------------------------------------
        #endregion



        private static void LogMsg(string msg)
        {
            UtilLog.LogToTraceFileWithFileName("CtrlCache", msg, "Gcc");
        }
    }
}
