using System;
using System.Collections.Generic;
using System.Text;

namespace UFIS.ITrekLib.ENT
{
	[Serializable]	
    public class EntCPM
    {
        private bool _selected;
        private string _containerNo;
        private string _urno;

        public string Urno
        {
            get { return _urno; }
            set { _urno = value; }
        }
        //private string _remk;
        private EntRemark _remk;
        private string _dest;
        private bool _allowToChangeContainerNo;

        public bool AllowToChangeContainerNo
        {
            get { return _allowToChangeContainerNo; }
            set { _allowToChangeContainerNo = value; }
        }

        public bool Selected
        {
            get { return _selected; }
            set { _selected = value; }
        }

        public void ClearData()
        {
            Selected = false;
            ContainerNo = null;
            Remark = null;
            Destination = null;
            Urno = null;
        }

        public string ContainerNo
        {
            get { return _containerNo; }
            set { if (value == null) _containerNo = ""; else _containerNo = value; }
        }

        public string RemarkText
        {
            get { if (_remk == null) return ""; else return _remk.GetRemarksString(); }
        }

        public EntRemark Remark
        {
            get { return _remk; }
            set { _remk = value; }
        }

        public string Destination
        {
            get { return _dest; }
            set { if (value == null) _dest = ""; else _dest = value; }
        }

        public EntCPM()
        {
            Selected = false;
            AllowToChangeContainerNo = true;
            ContainerNo = "";
            Remark = null;
            Destination = "";
            Urno = "";
        }

        public EntCPM(string containerNo):this()
        {
            ContainerNo = containerNo;
        }

        public void Copy(EntCPM cpm)
        {
            this._allowToChangeContainerNo = cpm._allowToChangeContainerNo;
            this._containerNo = cpm._containerNo;
            this._dest = cpm._dest;
            this._remk = cpm._remk;
            this._selected = cpm._selected;
            this._urno = cpm._urno;
        }
    }
}
