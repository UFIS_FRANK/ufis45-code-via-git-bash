using System;
using System.Collections.Generic;
using System.Text;

using UFIS.ITrekLib.Util;
using MM.UtilLib;

namespace UFIS.ITrekLib.ENT
{
    public class EntUld
    {
        private string _uldNo = null;
        private string _sentDt = null;
        private string _sentBy = null;
        private string _rcvDt = null;
        private string _rcvBy = null;

        public EntUld(string uldNo, 
            string sentDt, string sentBy,
            string receivedDt, string receivedBy)
        {
            _uldNo = uldNo;
            _sentDt = sentDt;
            _sentBy = sentBy;
            _rcvBy = receivedDt;
            _rcvDt = receivedBy;
            if (_sentDt == null) _sentDt = "";
            if (_sentBy == null) _sentBy = "";
            if (_rcvDt == null) _rcvDt = "";
            if (_rcvBy == null) _rcvBy = "";
        }

        public string UldNo
        {
            get
            {
                return _uldNo;
            }
        }

        public string SentDt
        {
            get
            {
                return _sentDt;
            }
        }

        public string UldInfo
        {
            get
            {
                string uld = _uldNo;
                if (IsSent()) uld += " (Sent:" + UtilTime.ConvUfisTimeStringToITrekShortTimeString(_sentDt) + ")";
                if (IsReceived()) uld += " (Received:" + UtilTime.ConvUfisTimeStringToITrekShortTimeString(_rcvDt) + ")";
                return uld;
            }
        }

        private bool IsSent()
        {
            bool sent = false;
            if ((_sentDt != "") || (_sentBy != "")) 
            {
                sent = true;
            }
            return sent;
        }

        private bool IsReceived()
        {
            bool rcv = false;
            if ((_rcvBy != "") || (_rcvDt != ""))
            {
                rcv = true;
            }
            return rcv;
        }
    }
}
