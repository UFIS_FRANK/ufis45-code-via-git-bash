using System;
using System.Collections.Generic;
using System.Text;
using UFIS.ITrekLib.DB;
using UFIS.ITrekLib.Util;
using System.Data;
using iTrekXML;
using System.Xml;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.Misc;
using System.Collections;

using MM.UtilLib;
using UFIS.ITrekLib.Misc;

namespace UFIS.ITrekLib.Ctrl
{
	public class CtrlJob
	{

		public static bool DeleteAJob(IDbCommand cmd, string jobUrno)
		{
			bool isOk = false;
			string userId = "";

			try
			{
				isOk = DBDJob.GetInstance().DeleteAJob(cmd, jobUrno, ITrekTime.GetCurrentDateTime(), userId);
			}
			catch (Exception ex)
			{
				LogMsg("DeleteAJob:Err:" + ex.Message);
				throw;
			}
			return isOk;

		}
        public static bool UpdateAJob(IDbCommand cmd, DataSet ds, out string staffId, out  string fliId, out string turnFlid)
		{
			bool isOk = false;
			string userId = "ITKHDL";
            staffId = "";
            fliId = "";
            turnFlid = "";
            bool hasJobForFlight = false;
            string oldFlId = "";
			try
			{
                isOk = DBDJob.GetInstance().UpdateAJob(cmd, ds, ITrekTime.GetCurrentDateTime(), userId, out staffId, out fliId, out turnFlid, out hasJobForFlight, out oldFlId);
                if (hasJobForFlight)
                { 
                Hashtable htFlight=new Hashtable();
                htFlight.Add(oldFlId, "");
                    try
                    {
                        CtrlFlight.UpdateAoIds(null, htFlight, "auto");
                    }
                    catch (Exception)
                    {
                        
                       
                    }
                
                }
			}
			catch (Exception ex)
			{
				LogTraceMsg("UpdateAJob:Err:" + ex.Message);
				throw ex;
			}
			return isOk;

		}
		public static bool InsertAJob(IDbCommand cmd, DataSet ds, out string flightId)
		{
			bool isOk = false;
			string userId = "";
			flightId = "";
			try
			{
				isOk = DBDJob.GetInstance().InsertAJob(cmd, ds, ITrekTime.GetCurrentDateTime(), userId, out flightId);
			}
			catch (Exception ex)
			{
				LogMsg("InsertAJob:Err:" + ex.Message);
				throw;
			}
			return isOk;

		}


		public static String RetrieveStaffsForAFlight(string urNo)
		{
			string st = "";
			/*Changed by Alphy on 13 May 2007*/
			DSJob dsJob = RetrieveFlightStaff(urNo);

			foreach (DSJob.JOBRow row in dsJob.JOB.Rows)
			{
				if ((row.FINM == null) || (row.FINM == ""))
				{
					st += row.PENO + ", " + row.FCTN + "\n";
				}
				else
				{
					st += row.FINM + ", " + row.FCTN + "\n";
				}
			}
			return st;
		}


		//public static string GetStaffName(string staffId, out string fName, out string lName)
		//{
		//    string staffName = "";
		//    fName = "";
		//    lName = "";
		//    UtilDb db = UtilDb.GetInstance();
		//    bool commit = false;
		//    IDbCommand cmd = null;
		//    try
		//    {
		//        cmd = db.GetCommand(false);
		//        DSJob ds = DBDJob.GetInstance().RetrieveStaffInfoForAStaff(staffId);
		//        if (ds.JOB.Rows.Count > 0)
		//        {
		//            DSJob.JOBRow row = (DSJob.JOBRow)ds.JOB.Rows[0];
		//            fName = row.FINM;
		//            lName = row.LANM;
		//            staffName = fName + " " + lName;
		//        }
		//    }
		//    catch (Exception ex)
		//    {

		//        throw;
		//    }
		//    finally
		//    {
		//        db.CloseCommand(cmd, commit);
		//    }
		//    return staffName;
		//}


		public static int GetStaffCount(string flightId)
		{
			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			IDbCommand cmd = null;
			int staffCnt = -1;
			try
			{
				cmd = db.GetCommand(false);
				staffCnt = DBDJob.GetInstance().GetStaffCount(cmd, flightId);
			}
			catch (Exception ex)
			{
				LogMsg("GetStaffCount:Err:" + ex.Message);
				throw;
			}
			finally
			{
				db.CloseCommand(cmd, commit);
			}
			return staffCnt;
		}


		public static Hashtable RetrieveAOIDsForAFlight(DateTime curTime)
		{

			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			IDbCommand cmd = null;
			Hashtable htAOId = new Hashtable();
			try
			{
				cmd = db.GetCommand(false);
				htAOId = DBDJob.GetInstance().RetrieveAOIDsForAFlight(cmd, curTime);
			}
			catch (Exception ex)
			{
				LogMsg("RetrieveAOIDsForAFlight:Err:" + ex.Message);
				throw;
			}
			finally
			{
				db.CloseCommand(cmd, commit);
			}
			return htAOId;
		}

		public static string GetAOId(string flightId, out string lastName, out string firstName)
		{
			string userId = "";
			lastName = "";
			firstName = "";

			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			IDbCommand cmd = null;
			try
			{
				cmd = db.GetCommand(false);
				DSJob dsJob = DBDJob.GetInstance().RetrieveStaffsForAFlight(cmd, flightId);
				int cnt = dsJob.JOB.Rows.Count;
				foreach (DSJob.JOBRow row in dsJob.JOB.Select("EMPTYPE='AO'"))
				{
					userId = row.PENO;
					lastName = row.LANM;
					firstName = row.FINM;
				}

			}
			catch (Exception ex)
			{
				LogMsg("GetAOId:Err:" + ex.Message);
				throw;
			}
			finally
			{
				db.CloseCommand(cmd, commit);
			}
			return userId;

		}
		public static DSJob RetrieveFlightStaff(string flightUrno)
		{
			DSJob ds = null;
			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			IDbCommand cmd = null;
			try
			{
				cmd = db.GetCommand(false);
				ds = DBDJob.GetInstance().RetrieveStaffsForAFlight(cmd, flightUrno);
			}
			catch (Exception ex)
			{
				LogMsg("RetrieveFlightStaff:Err:" + ex.Message);
				throw;
			}
			finally
			{
				db.CloseCommand(cmd, commit);
			}

			return ds;
		}

		public static DSJob RetrieveJobForAlert(string jobUrno, string turnJobUrno)
		{
			DSJob ds = null;
			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			IDbCommand cmd = null;
			try
			{
				cmd = db.GetCommand(false);
				ds = DBDJob.GetInstance().RetrieveJobForAlert(cmd, jobUrno, turnJobUrno);
			}
			catch (Exception ex)
			{
				LogMsg("RetrieveJobForAlert:Err:" + ex.Message);
				throw;
			}
			finally
			{
				db.CloseCommand(cmd, commit);
			}

			return ds;
		}

		private static void LogTraceMsg(string msg)
		{
			LogMsg(msg);
		}

		private static void LogMsg(string msg)
		{
			try
			{
				MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("CtrlJob", msg);

			}
			catch (Exception)
			{
				UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("CtrlJob", msg);
			}
		}

        public static bool ProcessInMsgs(IDbCommand cmd, string message, out string staffId, out string flId, out string turnFlId, out string messageType,out string msgToSend)
		{
			bool isOk = false;

			iTXMLPathscl iTPaths = new iTXMLPathscl();
			iTXMLcl iTXML = new iTXMLcl();
			string userId = "auto";
			DateTime updDateTime = ITrekTime.GetCurrentDateTime();
            staffId = "";
             flId = "";
             turnFlId = "";
             msgToSend = "";
            messageType = iTXML.GetJobMsgActionFromMsg(message);

			try
			{
				if (message != "")
				{
					
					LogTraceMsg("messageType" + messageType);
					if (messageType == "JOBS")
					{
						DoProcessJobBatch(cmd, message, userId, updDateTime);
					}
					else if (messageType == "DELETE")
					{
						DoProcessDeleteJob(cmd, message);
					}
					else if (messageType == "INSERT")
					{
						DoProcessInsertJob(cmd, message, iTXML,out flId,out msgToSend);
					}

					else if (messageType == "UPDATE")
					{
						DoProcessUpdateJob(cmd, message, iTXML,out staffId,out flId,out turnFlId);
					}
					else
					{
						// iTJobMgrLog.WriteEntry("Invalid Message Type:" + messageType);
						//do NOTHING
					}
					isOk = true;
				}
			}
			catch (Exception ex)
			{
				LogMsg("Fail to process:" + ex.Message);
				 throw new ApplicationException("Fail to ProcessInMsgs" + ex.Message);
			}

			return isOk;

		}

		/// <summary>
		/// Process the Job Batch Message.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="message"></param>
		/// <param name="userId"></param>
		/// <param name="updateDateTime"></param>
		public static void DoProcessJobBatch(IDbCommand cmd, string message,
			string userId, DateTime updateDateTime)
		{
			int cntInsert = 0; int cntUpdate = 0;
			int cntDelete = 0;
			int cntWithRecordInDb = 0;
			UtilDb udb = UtilDb.GetInstance();
			bool isNewCmd = false;
			bool commit = false;

			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = udb.GetCommand(false);
				}

				DataSet dsBatchJob = UtilDataSet.LoadDataFromXmlString(message);
				if (dsBatchJob == null || dsBatchJob.Tables.Count < 1 || dsBatchJob.Tables[0].TableName != "JOB")
				{
					LogTraceMsg("DoProcessJobBatch:Skip:" + message);
				}
				else
				{
					if (!dsBatchJob.Tables["JOB"].Columns.Contains("FLID"))
					{
						dsBatchJob.Tables["JOB"].Columns.Add("FLID", typeof(string));
					}
					List<string> lsUaft = new List<string>();
					Dictionary<string, string> mapUaftToFlid = new Dictionary<string, string>();

					#region Get all Uaft
					foreach (DataRow row in dsBatchJob.Tables["JOB"].Rows)
					{
						string uaft = (string)row["UAFT"];
						if (string.IsNullOrEmpty(uaft)) continue;
						if (lsUaft.Contains(uaft)) continue;
						lsUaft.Add(uaft);
					}
					#endregion
					mapUaftToFlid = CtrlFlight.RetrieveFlidsForUafts(cmd, lsUaft);

					DBDJob db = DBDJob.GetInstance();
					db.UploadBatchJobDataToDb(cmd, dsBatchJob, mapUaftToFlid, userId, updateDateTime, out cntInsert, out cntUpdate, out cntDelete, out cntWithRecordInDb);
					commit = true;
				}
			}
			catch (Exception ex)
			{
				LogMsg("DoProcessJobBatch:Err:" + ex.Message);
				throw;
			}
			finally
			{
				if (isNewCmd)
				{
					udb.CloseCommand(cmd, commit);
				}
			}
		}

		private static bool DoProcessDeleteJob(IDbCommand cmd, string message)
		{
			bool isOk = false;
			XmlDocument tempxmldoc = new XmlDocument();
			XmlNode xmlnURNO = null;
			try
			{
				tempxmldoc.LoadXml(message);
				xmlnURNO = tempxmldoc.FirstChild.FirstChild;

				isOk = DeleteAJob(cmd, xmlnURNO.InnerXml);
			}
			catch (Exception ex)
			{
				LogMsg("DoProcessDeleteJob:Err:" + ex.Message);
			}
			return isOk;
		}

        private static bool DoProcessInsertJob(IDbCommand cmd, string message, iTXMLcl iTXML, out string flightId,out string msgToSend)
		{
			bool isOk = false;
           flightId = "";
           msgToSend = "";
			try
			{
				message = iTXML.DeleteActionFromJobXMLFile(message);
                msgToSend = message;
			}
			catch (Exception ex)
			{
				LogMsg("DoProcessInsertJob:Act:Err:" + ex.Message);
			}
			try
			{
				XmlDocument tempxmldoc = new XmlDocument();

				tempxmldoc.LoadXml(message);
				DataSet ds = new DataSet();
				ds.ReadXml(new XmlNodeReader(tempxmldoc));
				
				isOk = InsertAJob(cmd, ds, out flightId);
				
			}
			catch (Exception ex)
			{
				LogMsg("DoProcessInsertJob:Err:" + ex.Message);
			}
			return isOk;
		}

		private static bool DoProcessUpdateJob(IDbCommand cmd, string message, iTXMLcl iTXML,out string staffId, out string flId, out string turnFlId)
		{
			bool isOk = false;
			try
			{
				message = iTXML.DeleteActionFromJobXMLFile(message);
			}
			catch (Exception ex)
			{
				LogMsg("DoProcessUpdateJob:Act:Err:" + ex.Message);
			}
			try
			{
				XmlDocument tempxmldoc = new XmlDocument();

				tempxmldoc.LoadXml(message);
				DataSet ds = new DataSet();
				ds.ReadXml(new XmlNodeReader(tempxmldoc));
				 staffId = "";
				 flId = "";
                 turnFlId = "";
                isOk = UpdateAJob(cmd, ds, out staffId, out flId, out turnFlId);
               
				
			}
			catch (Exception ex)
			{
				LogMsg("DoProcessUpdateJob:Err:" + ex.Message);
				throw;
			}
			return isOk;
		}
	}
}