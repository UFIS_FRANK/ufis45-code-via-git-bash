using System;
using System.Collections.Generic;
using System.Text;

using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.DB;
using UFIS.ITrekLib.ENT;
using iTrekSessionMgr;
using System.Collections;
using UFIS.ITrekLib.Util;
using iTrekXML;

using MM.UtilLib;

namespace UFIS.ITrekLib.Ctrl
{
    public class CtrlHK
    {
        private CtrlHK() { }

        public static CtrlHK GetInstance()
        {
            return new CtrlHK();
        }

        /// <summary>
        /// Generate the Itrek Adhoc Event Log (To get the UpToDate Event Information)
        /// </summary>
        /// <returns></returns>
        public string GetITrekAdhocEventLog()
        {
            string folderName = "EL_MANUAL";
            try
            {
				LogTraceMsg("GetITrekAdhocEventLog:");
                CleanUpEventLog();
                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.EnableRaisingEvents = false;
                iTXMLPathscl paths = new iTXMLPathscl();

                proc.StartInfo.FileName = GetLogDirPath() + "LogITrekEvents.bat";
                proc.StartInfo.Arguments = "V " + folderName;
                proc.Start();
            }
            catch (Exception ex)
            {
                UtilLog.LogToGenericEventLog("CtrlHK", "GetITrekEventLog:Error:" + ex.Message);
            }
            return folderName;
        }

        /// <summary>
        /// Get ITrek Event Log
        /// </summary>
        public void GetITrekEventLog()
        {
            try
            {
				LogTraceMsg("GetITrekEventLog:");
                CleanUpEventLog();
                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.EnableRaisingEvents = false;
                iTXMLPathscl paths = new iTXMLPathscl();

                proc.StartInfo.FileName = GetLogDirPath() + "LogITrekEvents.bat";
                proc.StartInfo.Arguments = "V " + GetEventLogFolderName(DateTime.Now);
                proc.Start();
            }
            catch (Exception ex)
            {
                UtilLog.LogToGenericEventLog("CtrlHK", "GetITrekEventLog:Error:" + ex.Message);
            }
        }

        /// <summary>
        /// Get Event Log Folder Name only (Not in Full Path)
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public string GetEventLogFolderName(DateTime dt)
        {
            return "EL_" + String.Format("{0:yyyyMMdd}", dt);
        }

        /// <summary>
        /// Get XML Data Backup Folder Name Only (Not in Full Path)
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public string GetXMLDataBackupFolderName(DateTime dt)
        {
            string fName = "DBK_";
            try
            {
                fName = "DBK_" + String.Format("{0:yyyyMMdd}", dt);
            }
            catch (Exception)
            {
            }
            return fName;
        }

        /// <summary>
        /// Get XML Data Backup Folder Full Path Name for given date
        /// </summary>
        /// <param name="dt">Date of the Backup Data</param>
        /// <returns>Full Path Name of the XML Backup data</returns>
        public string GetXMLDataBackupFolderFullPath(DateTime dt)
        {
            string logPath = GetLogDirPath();
            string fName = logPath + "DBK_";
            try
            {
                fName = logPath + "DBK_" + String.Format("{0:yyyyMMdd}", dt);
            }
            catch (Exception)
            {
            }
            return fName;
        }

        private string GetTraceLogFileName(DateTime dt)
        {
            return String.Format("Trc_{0:MMdd}.txt", dt);
        }

        private string GetTextLogFileName(DateTime dt)
        {
            return String.Format("Log_{0:MMdd}.txt", dt);
        }

		/// <summary>
		/// Return Trace File Suffix (e.g. _0315.txt)
		/// </summary>
		/// <param name="dt">Date Time to prepare the suffix</param>
		/// <returns></returns>
		private string GetTraceFileSuffix(DateTime dt)
		{
			return String.Format("_{0:MMdd}.txt", dt);
		}

		private static void LogTraceMsg(string stMsg)
		{
			UtilLog.LogToGenericEventLog("CtrlHK", stMsg);
		}

        /// <summary>
        /// Clean Up Event Log and data backup
        /// </summary>
        public void CleanUpEventLog()
        {
            try
            {
				LogTraceMsg("CleanUpEventLog:Start.");
                iTXMLPathscl paths = new iTXMLPathscl();
                string logDir = paths.GetLogDirPath();
				LogTraceMsg("CleanUpEventLog:d" + logDir );
                DateTime curDt = DateTime.Now;

                int keepLogDays = 7;
                keepLogDays = 0 - Math.Abs(keepLogDays);

                int keepDataDays = -4;
                DateTime dtDataHk = curDt.AddDays(keepDataDays);

				string logDirPath = GetLogDirPath();

                for (int i = 0; i < 7; i++)
                {
                    DateTime dtHk = curDt.AddDays(keepLogDays - i);
					string stFileSuffix = GetTraceFileSuffix(dtHk );
					LogTraceMsg( "CleanUpEventLog:" + stFileSuffix);
					UtilFileIO.DeleteAFolder(logDirPath + GetEventLogFolderName(dtHk));
					UtilFileIO.DeleteAFile(logDirPath + GetTextLogFileName(dtHk));
					UtilFileIO.DeleteAFile(logDirPath + GetTraceLogFileName(dtHk));

					UtilFileIO.DeleteAFile(logDirPath + "Web" + stFileSuffix);
					UtilFileIO.DeleteAFile(logDirPath + "Handheld" + stFileSuffix);
					UtilFileIO.DeleteAFile(logDirPath + "Proc" + stFileSuffix);
					UtilFileIO.DeleteAFile(logDirPath + "CMsgOut" + stFileSuffix);
					UtilFileIO.DeleteAFile(logDirPath + "CMsgIn" + stFileSuffix);

					UtilFileIO.DeleteAFolder(logDirPath + GetXMLDataBackupFolderName(dtDataHk));
                    dtDataHk.AddDays(-1);
                }
				LogTraceMsg("CleanUpEventLog:Finish.");
            }
            catch (Exception ex)
            {
                UtilLog.LogToGenericEventLog("CtrlHK", "CleanUpEventLog:Error:" + ex.Message);
            }
        }

        private string _logDir = null;
        /// <summary>
        /// Get Log Directory Path
        /// </summary>
        /// <returns></returns>
        private string GetLogDirPath()
        {
            if (_logDir == null)
            {
                iTXMLPathscl paths = new iTXMLPathscl();
                _logDir = paths.GetLogDirPath();
            }
            return _logDir + "";
        }

        private string _xmlDir = null;

        /// <summary>
        /// Get XML Directory path
        /// </summary>
        /// <returns></returns>
        private string GetXmlDirPath()
        {
            if (_xmlDir == null)
            {
                iTXMLPathscl paths = new iTXMLPathscl();
                _xmlDir = paths.GetXMLPath();
            }
            return _xmlDir;
        }

        /// <summary>
        /// Restore the previously backed up data
        /// </summary>
        /// <param name="bkOption">Back Option to restore back. Restore back from daily back or hourly backup</param>
        /// <param name="bkFileSuffix">Backup File Suffix</param>
        /// <param name="dtToRestore">Date Time of the backup to be restored</param>
        /// <param name="orgFileNameToRestore">Original File Name to Restore (with Full Path)</param>
        /// <param name="bkFileExist">Is the Backup File Exist</param>
        /// <returns>True - Successfully restored. Otherwise - Not able to restore</returns>
        public bool RestoreData(EnumBackupDataOption bkOption, string bkFileSuffix, DateTime dtToRestore, string orgFileNameToRestore, out bool bkFileExist)
        {
            if (string.IsNullOrEmpty(orgFileNameToRestore)) throw new ApplicationException("No File Name to Restore.");

            iTXMLPathscl paths = new iTXMLPathscl();
            string dataBackupDir = GetXMLDataBackupFolderName(dtToRestore);
            string logPath = GetLogDirPath();

            dataBackupDir = logPath + dataBackupDir + @"\";

            string stPrefix = "";
            switch (bkOption)
            {
                case EnumBackupDataOption.Daily:
                    break;
                case EnumBackupDataOption.Hourly:
                    stPrefix = string.Format("{0:HH}", dtToRestore);
                    break;
                default:
                    throw new ApplicationException("Invalid Backup Option.");
                //break;
            }

            if (bkFileSuffix == null) bkFileSuffix = "";

            return DoRestore(dataBackupDir, stPrefix, bkFileSuffix, true, orgFileNameToRestore, out bkFileExist);
        }

        /// <summary>
        /// Backup the Data for the following file.
        /// 1. SummFlights.xml
        /// 2. JobSumm.xml
        /// 3. JobSummHist.xml
        /// </summary>
        /// <param name="bkOption">Backup Option</param>
        /// <param name="destFileSuffix">Suffix for Backup file</param>
        public void BackupData(EnumBackupDataOption bkOption, string destFileSuffix)
        {
            DateTime dtNow = DateTime.Now;
            iTXMLPathscl paths = new iTXMLPathscl();
            string dataBackupDir = GetXMLDataBackupFolderName(dtNow);
            //string xmlDir = GetXmlDirPath();
            string logPath = GetLogDirPath();

            UtilFileIO.CreateSubDirectory(logPath, dataBackupDir);

            dataBackupDir = logPath + dataBackupDir + @"\";

            string stPrefix = "";
            switch (bkOption)
            {
                case EnumBackupDataOption.Daily:
                    stPrefix = string.Format("{0:yyMMdd}___", dtNow);
                    break;
                case EnumBackupDataOption.Hourly:
                    stPrefix = string.Format("{0:yyMMddHH}_", dtNow);
                    break;
                default:
                    throw new ApplicationException("Invalid Backup Option.");
                    //break;
            }

            if (destFileSuffix == null) destFileSuffix = "";
            bool isFileExist = false;

            DoBackup(paths.GetSummFlightsFilePath(), dataBackupDir, stPrefix, destFileSuffix, true, out isFileExist);
            DoBackup(paths.GetJobsSummFilePath(), dataBackupDir, stPrefix, destFileSuffix, true, out isFileExist);
            DoBackup(paths.GetJobsSummHistFilePath(), dataBackupDir, stPrefix, destFileSuffix, true, out isFileExist);
        }

        /// <summary>
        /// Do the backup
        /// </summary>
        /// <param name="srcFileName">Source File Name (with Full Path)</param>
        /// <param name="desDir">Destination Directory</param>
        /// <param name="destFilePrefix">Destination File Prefix</param>
        /// <param name="destFileSuffix">Destination File Suffix</param>
        /// <param name="overwrite">overwrite the destination file if it is already existed</param>
        /// <param name="fileExist">Is Source File Exist</param>
        /// <returns>True - Successfully Backed-up</returns>
        private bool DoBackup(string srcFileName, string destDir, string destFilePrefix, string destFileSuffix, bool overwrite, out bool fileExist)
        {
            string destFileName = UtilMisc.GetFullFileName(destDir, 
                destFilePrefix,
                srcFileName,
                destFileSuffix);
            return UtilFileIO.CopyFileWithLock(srcFileName, destFileName,
                overwrite, out fileExist);
        }

        
        /// <summary>
        /// Do the restoring of data
        /// </summary>
        /// <param name="bkDir">Backup Directory</param>
        /// <param name="bkPrefix">Backup Prefix</param>
        /// <param name="bkSuffix">Backup Suffix</param>
        /// <param name="overwrite">overwite the original file with backup if original file was existed</param>
        /// <param name="fileNameToRestore">File Name to restore</param>
        /// <param name="isBkFileExist">Is backup file exist</param>
        /// <returns>true - successfully restored</returns>
        private bool DoRestore(string bkDir, string bkPrefix, string bkSuffix, bool overwrite, string fileNameToRestore, out bool isBkFileExist)
        {
            string bkFileName = UtilMisc.GetFullFileName(bkDir,
                bkPrefix,
                fileNameToRestore ,
                bkSuffix);
            return UtilFileIO.CopyFileWithLock(bkFileName, fileNameToRestore, 
                overwrite, out isBkFileExist);
        }
    }

    /// <summary>
    /// Backup data options
    /// </summary>
    public enum EnumBackupDataOption { Daily, Hourly };
}
