#define UsingQueues
//#define TESTING_ON

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Diagnostics;

using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.DB;
using iTrekSessionMgr;
using iTrekXML;
using iTrekWMQ;
using MM.UtilLib;

namespace UFIS.ITrekLib.Ctrl
{
	public class CtrlDLS
	{

		public static string RetrieveDLSForAFlight(string flightUrNo, string flightNo, string sessionId)
		{
			string st = "";
			try
			{
				SessionManager sessMgr = new SessionManager();
				string stUaft = CtrlFlight.GetUaftForFlightId(null, flightUrNo);
				st = sessMgr.GetShowDLS(flightNo, stUaft, sessionId);
				st = sessMgr.GetDLSFromSessionDir(sessionId, stUaft).Trim();
				if (st == "") st = "No DLS data available for this flight.";
			}
			catch (ApplicationException ex)
			{
				LogMsg("RetrieveDLSForAFlight:AppError:" + flightUrNo + "," + flightNo + "," + sessionId + "," + ex.Message);
				throw new ApplicationException("Unable to get the DLS Message for flight no. " + flightNo + ", " + flightUrNo + " due to " + ex.Message);
			}
			catch (Exception ex)
			{
				LogMsg("RetrieveDLSForAFlight:Error:" + flightUrNo + "," + flightNo + "," + sessionId + "," + ex.Message);
				throw new ApplicationException("Unable to get the DLS Message for flight no. " + flightNo + ", " + flightUrNo);
			}
			return st;
		}


		//public static string RetrieveDLSForAFlight(string flightUrNo, string flightNo, string sessionId)
		//{
		//    string st = "";
		//    try
		//    {
		//        DBDls dbDls = DBDls.GetInstance();
		//        DSDls dsDls = null;
		//        try
		//        {
		//            dsDls = dbDls.RetrieveDlsForAFlight(flightUrNo, flightNo);
		//        }
		//        catch (Exception ex)
		//        {
		//            LogMsg("RetrieveDLSForAFlight:GetDls:Error:" + ex.Message);
		//        }
		//        if ((dsDls == null) || (dsDls.DLSTAB.Rows.Count < 1))
		//        {
		//            //SessionManager sessMgr = new SessionManager();
		//            //st = sessMgr.GetShowDLS(flightNo, flightUrNo, sessionId);
		//            st = GetShowDLS(flightNo, flightUrNo, sessionId);
		//        }
		//        else
		//        {
		//            st = ((DSDls.DLSTABRow)dsDls.DLSTAB.Rows[0]).DLS;
		//        }
		//    }
		//    catch (ApplicationException ex)
		//    {
		//        LogMsg("RetrieveDLSForAFlight:AppError:", ex.Message);
		//        throw new ApplicationException("Unable to get the DLS Message for flight no. " + flightNo + ", " + flightUrNo + " due to " + ex.Message);
		//    }
		//    catch (Exception ex)
		//    {
		//        LogMsg("RetrieveDLSForAFlight:Error:", ex.Message);
		//        throw new ApplicationException("Unable to get the DLS Message for flight no. " + flightNo + ", " + flightUrNo );
		//    }
		//    return st;
		//}

		private static void LogMsg(string msg)
		{
			Util.UtilLog.LogToGenericEventLog("CtrlDls", msg);
		}

		public static string GetDLSFromSessionDir(string msgFile)
		{
			string DLS = "";
			for (int i = 0; i < 10; i++)
			{
				//string msgFile = @"D:\AungMoe\Am\iTrek\xml\xml\Sessions\1" + @"\SHOWDLS.xml";

				//FileInfo fi = new FileInfo(msgFile);
				//if (fi.Exists)
				System.Threading.Thread.Sleep(1000);
				if (UtilFileIO.IsFileExist(msgFile))
				{
					XmlDocument doc = new XmlDocument();
					doc.Load(msgFile);
					XmlNode CurrentDLSNode = doc.SelectSingleNode("//SHOWDLS/DLS");
					DLS = CurrentDLSNode.InnerXml;
					break;
				}
			}
			return DLS;
		}

		//[Conditional("TESTING_ON")]
		private static void DeleteFile(string fileName)
		{
			try
			{
				File.Delete(fileName);
			}
			catch (Exception)
			{
			}
		}

		public static string GetShowDLS(string flightno, string flightUrno, string sessionId)
		{
			//get rid of the previous file if in session dir
			//commented out for prod release before regression tests
			//- code is working and should be uncommented
			iTXMLPathscl iTPaths = new iTXMLPathscl();
			string msgFile = "";
#if TESTING_ON
            msgFile = @"D:\AungMoe\Am\iTrek\xml\xml\Sessions\1" + @"\SHOWDLS.xml";
#else
			msgFile = iTPaths.GetXMLPath() + sessionId + @"\SHOWDLS.xml";
			DeleteFile(msgFile);
#endif

#if UsingQueues
			string msgId = "";
			//put the session id on the queue
			ITrekWMQClass1 FROMITREKSELECTQueue = new ITrekWMQClass1();
			msgId = FROMITREKSELECTQueue.putShowDLSMessageIntoFROMITREKSELECTQueueWithSessionID(flightno, flightUrno, sessionId, iTPaths.GetFROMITREKSELECTQueueName());
			//msgId = ITrekWMQClass1.putShowDLSMessageIntoFROMITREKSELECTQueueWithSessionID(flightno, flightUrno, sessionId, iTPaths.GetFROMITREKSELECTQueueName());
#endif

			string DLS = "";
			//we need to wait for n seconds for iTrekFileMgr process to get the message from the queue
			//use configurable time param
			int waitingTime = 0;
			try
			{
				waitingTime = Convert.ToInt32(iTPaths.GetMsgFromQueueDelaySetting()) / 1000;
			}
			catch { }
			if (waitingTime < 5) waitingTime = 5;//Force to wait atleast 5 seconds
			if (waitingTime > 20) waitingTime = 20;//Force to wait maximum 20 seconds
			for (int i = 0; i < waitingTime; i++)
			{
				if (File.Exists(msgFile))
				{
					string msgFlightUrno = "";
					string dlsMsg = "";

					try
					{
						XmlDocument doc = new XmlDocument();
						doc.Load(msgFile);

						UpdateDls(doc, out msgFlightUrno, out dlsMsg);
					}
					catch (Exception ex)
					{
						LogMsg("GetShowDLS:Error:" + ex.Message);
					}

					if (msgFlightUrno == flightUrno)
					{
						DLS = dlsMsg;
					}
					break;
				}
				else
				{
					System.Threading.Thread.Sleep(1000);//wait for 1 seconds
				}
			}
			return DLS;
		}

		public static void UpdateDls(XmlDocument doc, out string msgFlightUrno, out string dlsMsg)
		{//Catch error by calling program.
			XmlNode msgFlightUrnoNode = doc.SelectSingleNode("//SHOWDLS/URNO");
			msgFlightUrno = msgFlightUrnoNode.InnerXml;
			XmlNode CurrentDLSNode = doc.SelectSingleNode("//SHOWDLS/DLS");
			dlsMsg = CurrentDLSNode.InnerXml;

			if (dlsMsg != "")
			{
				DBDls.GetInstance().UpdateDls(msgFlightUrno, dlsMsg);
			}
		}
	}
}