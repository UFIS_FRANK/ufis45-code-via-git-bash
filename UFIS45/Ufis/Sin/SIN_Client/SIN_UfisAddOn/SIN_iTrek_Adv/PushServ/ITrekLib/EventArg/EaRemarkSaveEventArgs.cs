using System;
using System.Collections.Generic;
using System.Text;

using UFIS.ITrekLib.ENT;

namespace UFIS.ITrekLib.EventArg
{
    public class EaRemarkSaveEventArgs
    {
        EntRemark _ent = null;
        public EaRemarkSaveEventArgs(EntRemark ent)
        {
            if (ent == null) throw new ApplicationException("Null Remark");
            _ent = ent;
        }

        public EntRemark Remk
        {
            get { return _ent; }
        }
    }
}
