using System;
using System.Collections.Generic;
using System.Text;

using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.DB;
using UFIS.ITrekLib.Util;

namespace UFIS.ITrekLib.Ctrl
{
    public class CtrlCurUser
    {
        private const string sessionid = "sessionid";
        private const string deviceid = "deviceid";
        private const string staffid = "staffid";
        private const string time = "time";
        private const string org = "org";
        private const string name = "name";

        public static DSCurUser RetrieveCurUserForSessionId(string sessionid)
        {
            return DBCurUser.GetInstance().RetrieveCurUserForSessionId(sessionid);
        }

        public static DSCurUser RetrieveAllCurUsers()
        {
            return DBCurUser.GetInstance().RetrieveCurUser();
        }

        public static DSCurUser RetrieveAllAOS()
        {
            return DBCurUser.GetInstance().GetCurrentLoginedUsers("A");
        }

		public static DSCurUser RetrieveAllBOs()
		{
			return DBCurUser.GetInstance().GetCurrentLoginedUsers("B");
		}



        //    public static string GetValueFor(string configFor, out string desc)
        //    {
        //        string val = "";
        //        val = DBConfig.GetInstance().GetConfigValueFor(configFor, out desc);
        //        //DSConfig ds = GetConfigFor(configFor);

        //        //if ((ds != null) && (ds.CONFIG.Rows.Count > 0))
        //        //{
        //        //    val = ((DSConfig.CONFIGRow)ds.CONFIG.Rows[0]).VAL;
        //        //}
        //        return val;
        //    }

        public static void CreateCurUser(string sessid, string loginType, string deviceid, string staffid, string time, string org, string name)
        {
            DBCurUser.GetInstance().CreateNewUser(sessid, loginType, deviceid, staffid, time, org, name);
        }


        public static DSCurUser RetrieveCurUserForStaffId(string staffid)
        {
            return DBCurUser.GetInstance().RetrieveCurUserForStaffId(staffid);

        }

        public static DSCurUser RetrieveCurUserForDeviceId(string deviceid)
        {
            return DBCurUser.GetInstance().RetrieveCurUserForDeviceId(deviceid);

        }

        public static void DeleteDeviceIdEntryBySessID(string sessionid)
        {

            DBCurUser.GetInstance().DeleteDeviceIdEntryBySessID(sessionid);
        }

        public static void DeleteDeviceIdEntryByStaffID(string staffid)
        {

            DBCurUser.GetInstance().DeleteDeviceIdEntryByStaffID(staffid);
        }

        //    public static void CreateConfigForWeb(string configFor, string value, string desc)
        //    {
        //        CreateConfigFor(configFor, value, "WEB", desc);
        //    }

        //    public static void UpdateConfigValueFroWeb(string configFor, string value)
        //    {
        //        DBConfig.GetInstance().UpdateConfig(configFor, value);
        //    }

        //    public static string GetBmMAB()
        //    {
        //        string desc="";
        //        return DBConfig.GetInstance().GetConfigValueFor(BM_MAB, out desc);
        //    }


        //    public static string GetBmPLB()
        //    {
        //        string desc="";
        //        return DBConfig.GetInstance().GetConfigValueFor(BM_PLB, out desc);
        //    }

        //    public static string GetBmApronFirstBag()
        //    {
        //        string desc = "";
        //        return DBConfig.GetInstance().GetConfigValueFor(BM_AP_FST_BAG, out desc);
        //    }

        //    public static string GetBmBaggageFirstBag()
        //    {
        //        string desc = "";
        //        return DBConfig.GetInstance().GetConfigValueFor(BM_BG_FST_BAG, out desc);
        //    }

        //    public static string GetBMApronLastBagLightLoad()
        //    {
        //        string desc = "";
        //        return DBConfig.GetInstance().GetConfigValueFor(BM_AP_LST_BAG_LGT_LOAD, out desc);
        //    }

        //    public static string GetBMBaggageLastBagLightLoad()
        //    {
        //        string desc = "";
        //        return DBConfig.GetInstance().GetConfigValueFor(BM_BG_LST_BAG_LGT_LOAD, out desc);
        //    }

        //    public static string GetBMApronLastBagHeavyLoad()
        //    {
        //        string desc = "";
        //        return DBConfig.GetInstance().GetConfigValueFor(BM_AP_LST_BAG_HVY_LOAD, out desc);
        //    }

        //    public static string GetBMBaggageLastBagHeavyLoad()
        //    {
        //        string desc = "";
        //        return DBConfig.GetInstance().GetConfigValueFor(BM_BG_LST_BAG_HVY_LOAD, out desc);
        //    }

        //    public static string GetWebFLINFO_TO_MINUTES()
        //    {
        //        string desc = "";
        //        return DBConfig.GetInstance().GetConfigValueFor(WEB_FLINFO_TO_MINUTES, out desc);
        //    }

        //    public static string GetWebFLINFO_FROM_MINUTES()
        //    {
        //        string desc = "";
        //        return DBConfig.GetInstance().GetConfigValueFor(WEB_FLINFO_FROM_MINUTES, out desc);
        //    }

        //    public static string GetWapJobAssignAlertMinutes()
        //    {
        //        string desc = "";
        //        return DBConfig.GetInstance().GetConfigValueFor(WAP_JOBALERT_MINUTES, out desc);
        //    }

        //    public static int GetKeepDataDays()
        //    {
        //        string desc = "";
        //        string keepHours = DBConfig.GetInstance().GetConfigValueFor(KEEP_DAY, out desc);
        //        int days = -60;
        //        try
        //        {
        //            days = Convert.ToInt32(keepHours);
        //            if (days > 0) days = 0 - days;
        //        }
        //        catch (Exception ex)
        //        {
        //            LogMsg("GetKeepDataDays:Error:" + ex.Message);
        //        }
        //        if (days > 0) days = 0 - days;
        //        else if (days == 0) days = -60;
        //        return days;
        //    }

        //    public static int GetKeepShortLifeSpanDataDays()
        //    {
        //        string desc = "";
        //        string keepDay = DBConfig.GetInstance().GetConfigValueFor(KEEP_DAY_SH, out desc);
        //        int days = -1;
        //        try
        //        {
        //            days = Convert.ToInt32(keepDay);
        //            if (days > 0) days = 0 - days;
        //        }
        //        catch (Exception ex)
        //        {
        //            LogMsg("GetKeepShortLifeSpanDataDays:Error:" + ex.Message);
        //        }
        //        if (days > 0) days = 0 - days;
        //        else if (days == 0) days = -1;
        //        return days;
        //    }

        //    private static void LogMsg(string msg)
        //    {
        //        UtilLog.LogToGenericEventLog("CtrlConfig", msg);
        //    }

        //    public static bool IsTraceModeOn()
        //    {
        //        string desc = "";
        //        bool traceMode = false;
        //        string stTraceMode = DBConfig.GetInstance().GetConfigValueFor(TRACE_MODE, out desc);
        //        if (stTraceMode == "1") traceMode = true;
        //        return traceMode;
        //    }

        //    public static int GetWebRefreshTiming()
        //    {
        //        const int DEF_REFRESH_TIMING = 300;
        //        int refreshTiming = DEF_REFRESH_TIMING; //in seconds

        //        string desc = "";
        //        string stTiming = DBConfig.GetInstance().GetConfigValueFor(WEB_REFRESH_TIMING, out desc);
        //        if ((stTiming != null) && (stTiming != ""))
        //        {
        //            try
        //            {
        //                refreshTiming = Convert.ToInt32(stTiming);
        //                refreshTiming = Math.Abs(refreshTiming);
        //                if (refreshTiming == 0)
        //                {
        //                    refreshTiming = DEF_REFRESH_TIMING;
        //                }
        //            }
        //            catch (Exception)
        //            {
        //                refreshTiming = DEF_REFRESH_TIMING;
        //            }
        //        }
        //        return refreshTiming;
        //    }

    }
}
