#define TESTING_ON
#define TRACING_ON
#define LOGGING_ON
#define USING_MQ


using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.IO;
using System.Xml;
using System.Data;
using System.Diagnostics;
using System.Collections;

using iTrekXML;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.DSDB;
using UFIS.ITrekLib.Util;
//using iTrekWMQ;
using MM.UtilLib;
using UFIS.ITrekLib.ENT.BPTRSumm;
using UFIS.ITrekLib.Config;

namespace UFIS.ITrekLib.DB
{
	public class DBDBptr
	{
		private static Object _lockObj = new Object();//to use in synchronisation of single access
		private static Object _lockObjBagPresentTimingReportHdr = new Object();//to use in synchronisation of single access
		private static Object _lockObjBagPresentTimingReportDet = new Object();//to use in synchronisation of single access
		private static DBDBptr _DBDBptr = null;

		private static readonly DateTime DEFAULT_DT = new System.DateTime(1, 1, 1);
		private static DateTime _lastBagPresentTimingReportHdrWriteDT = DEFAULT_DT;
		private static DateTime _lastBagPresentTimingReportDetWriteDT = DEFAULT_DT;

		private static DSBagPresentTimingRpt _dsBagPresentTimingReport = null;
		private static string _fnBagPresentTimingReportHdrXml = null;//file name of BagPresentTimingReport XML in full path.
		private static string _fnBagPresentTimingReportDetXml = null;//file name of BagPresentTimingReport Details XML in full path.

		/// <summary>
		/// Singleton Pattern.
		/// Not allowed to create the object from other class.
		/// To get the object, user "GetInstance()" method.
		/// </summary>
		private DBDBptr() { }

		public void Init()
		{//Initialize all the information, to get the latest data when fetching them again.
			_dsBagPresentTimingReport = null;
			_fnBagPresentTimingReportHdrXml = null;
			_fnBagPresentTimingReportDetXml = null;
			_lastBagPresentTimingReportHdrWriteDT = DEFAULT_DT;
			_lastBagPresentTimingReportDetWriteDT = DEFAULT_DT;
		}

		/// <summary>
		/// Get the Instance of DBDBptr. There will be only one reference for all the clients
		/// </summary>
		/// <returns></returns>
		public static DBDBptr GetInstance()
		{
			if (_DBDBptr == null)
			{
				_DBDBptr = new DBDBptr();
			}
			return _DBDBptr;
		}

		public DSBagPresentTimingRpt RetrieveBagPresentTimingReportForAFlight(string flightUrno)
		{
			DSBagPresentTimingRpt ds = new DSBagPresentTimingRpt();
			DSBagPresentTimingRpt tempDs = dsBagPresentTimingReport;
			foreach (DSBagPresentTimingRpt.BPTRHDRRow row in tempDs.BPTRHDR.Select("URNO='" + flightUrno + "'"))
			{
				ds.BPTRHDR.LoadDataRow(row.ItemArray, true);
			}
			try
			{
				if (tempDs.BPTRDET != null)
				{
					foreach (DSBagPresentTimingRpt.BPTRDETRow row in tempDs.BPTRDET.Select("URNO='" + flightUrno + "'"))
					{
						ds.BPTRDET.LoadDataRow(row.ItemArray, true);
					}
				}

			}
			catch (Exception ex)
			{
				LogMsg("RetrieveBagPresentTimingReportForAFlight:Error:" + flightUrno + ":" + ex.Message);
			}
			return ds;
		}

		private string fnBagPresentTimingReportHdrXml
		{
			get
			{
				if ((_fnBagPresentTimingReportHdrXml == null) || (_fnBagPresentTimingReportHdrXml == ""))
				{
					iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
					_fnBagPresentTimingReportHdrXml = path.GetBagPresentTimingReportHdrFilePath();
				}

				return _fnBagPresentTimingReportHdrXml;
			}
		}

		private string fnBagPresentTimingReportDetXml
		{
			get
			{
				if ((_fnBagPresentTimingReportDetXml == null) || (_fnBagPresentTimingReportDetXml == ""))
				{
					iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
					_fnBagPresentTimingReportDetXml = path.GetBagPresentTimingReportDetFilePath();
				}

				return _fnBagPresentTimingReportDetXml;
			}
		}

		private DSBagPresentTimingRpt dsBagPresentTimingReport
		{
			get
			{
				//if (_dsBagPresentTimingReport == null) { LoadDSBagPresentTimingRpt(); }
				LoadDSBagPresentTimingRpt();
				DSBagPresentTimingRpt ds = (DSBagPresentTimingRpt)_dsBagPresentTimingReport.Copy();

				return ds;
			}
		}

		private void LoadDSBagPresentTimingRpt()
		{
			if (IsNeedToUpdateData())
			{
				lock (_lockObj)
				{
					try
					{
						if (IsNeedToUpdateData())
						{
							if (_dsBagPresentTimingReport == null) _dsBagPresentTimingReport = new DSBagPresentTimingRpt();
							LoadDSBagPresentTimingRpt_BagPresentTimingReportHeaderTable();
							LoadDSBagPresentTimingRpt_BagPresentTimingReportDetailTable();
						}
					}
					catch (Exception ex)
					{
						LogMsg("LoadDSBagPresentTimingRpt:Err:" + ex.Message);
					}
				}
			}
		}

		private bool IsNeedToUpdateData()
		{
			bool needToUpdate = true;
			if ((_dsBagPresentTimingReport == null) ||
				(IsNeedToUpdateBagPresentTimingReportHdrTable()) ||
				(IsNeedToUpdateBagPresentTimingReportDetTable())) needToUpdate = true;
			else needToUpdate = false;

			return needToUpdate;
		}

		private bool IsNeedToUpdateBagPresentTimingReportHdrTable()
		{
			bool needToUpdate = false;
			FileInfo fiXml = new FileInfo(fnBagPresentTimingReportHdrXml);
			DateTime curDT = fiXml.LastWriteTime;
			if (curDT.CompareTo(_lastBagPresentTimingReportHdrWriteDT) != 0) needToUpdate = true;
			return needToUpdate;
		}

		private bool IsNeedToUpdateBagPresentTimingReportDetTable()
		{
			bool needToUpdate = false;
			FileInfo fiXml = new FileInfo(fnBagPresentTimingReportDetXml);
			DateTime curDT = fiXml.LastWriteTime;
			if (curDT.CompareTo(_lastBagPresentTimingReportDetWriteDT) != 0) needToUpdate = true;
			return needToUpdate;
		}


		private void LoadDSBagPresentTimingRpt_BagPresentTimingReportHeaderTable()
		{
			FileInfo fiXml = new FileInfo(fnBagPresentTimingReportHdrXml);
			DateTime curDT = fiXml.LastWriteTime;
			if (!fiXml.Exists)
			{
				//throw new ApplicationException("BagPresentTimingReport Header information missing. Please inform to administrator.");
				if (_dsBagPresentTimingReport == null)
				{
					_dsBagPresentTimingReport = new DSBagPresentTimingRpt();
				}
				return;
			}

			if ((_dsBagPresentTimingReport == null) || (curDT.CompareTo(_lastBagPresentTimingReportHdrWriteDT) != 0))
			{
				lock (_lockObjBagPresentTimingReportHdr)
				{
					try
					{
						curDT = fiXml.LastWriteTime;//check the time again, in case of lock
						if ((_dsBagPresentTimingReport == null) || (curDT.CompareTo(_lastBagPresentTimingReportHdrWriteDT) != 0))
						{
							try
							{
								DSBagPresentTimingRpt tempDs = new DSBagPresentTimingRpt();
								tempDs.BPTRHDR.ReadXml(fnBagPresentTimingReportHdrXml);
								if (_dsBagPresentTimingReport == null)
								{
									_dsBagPresentTimingReport = new DSBagPresentTimingRpt();
								}
								else
								{
									_dsBagPresentTimingReport.BPTRHDR.Clear();
								}
								_dsBagPresentTimingReport.BPTRHDR.Merge(tempDs.BPTRHDR);
								_lastBagPresentTimingReportHdrWriteDT = curDT;
								LogMsg("Load Hdr");
							}
							catch (Exception ex)
							{
								LogMsg("LoadDSBagPresentTimingRpt_BagPresentTimingReportHeaderTable : Error : " + ex.Message);
							}
						}

					}
					catch (Exception ex)
					{
						LogMsg("LoadDSBagPresentTimingRpt:Err:" + ex.Message);
					}
				}
			}
		}

		private void LoadDSBagPresentTimingRpt_BagPresentTimingReportDetailTable()
		{
			FileInfo fiXml = new FileInfo(fnBagPresentTimingReportDetXml);
			DateTime curDT = fiXml.LastWriteTime;
			if (!fiXml.Exists)
			{
				//throw new ApplicationException("BagPresentTimingReport Detail information missing. Please inform to administrator.");
				if (_dsBagPresentTimingReport == null)
				{
					_dsBagPresentTimingReport = new DSBagPresentTimingRpt();
				}
				return;
			}

			if ((_dsBagPresentTimingReport == null) || (curDT.CompareTo(_lastBagPresentTimingReportDetWriteDT) != 0))
			{
				lock (_lockObjBagPresentTimingReportDet)
				{
					try
					{
						curDT = fiXml.LastWriteTime;//check the time again, in case of lock
						if ((_dsBagPresentTimingReport == null) || (curDT.CompareTo(_lastBagPresentTimingReportDetWriteDT) != 0))
						{
							try
							{
								DSBagPresentTimingRpt tempDs = new DSBagPresentTimingRpt();
								tempDs.BPTRDET.ReadXml(fnBagPresentTimingReportDetXml);
								if (_dsBagPresentTimingReport == null)
								{
									_dsBagPresentTimingReport = new DSBagPresentTimingRpt();
								}
								else
								{
									_dsBagPresentTimingReport.BPTRDET.Clear();
								}
								_dsBagPresentTimingReport.BPTRDET.Merge(tempDs.BPTRDET);
								_lastBagPresentTimingReportDetWriteDT = curDT;
							}
							catch (Exception ex)
							{
								LogMsg("LoadDSBagPresentTimingRpt_BagPresentTimingReportDetailTable : Error : " + ex.Message);
							}
						}
					}
					catch (Exception ex)
					{
						LogMsg("LoadDSBagPresentTimingRpt_BagPresentTimingReportDetailTable:Err:" + ex.Message);
					}
				}
			}
		}

		public void SaveRpt(IDbCommand cmd, string flightId, DSBagPresentTimingRpt dsToSave, string userId, DateTime updateDateTime)
		{
			lock (_lockObj)
			{
				try
				{
					DSBagPresentTimingRpt.BPTRHDRRow[] rows = (DSBagPresentTimingRpt.BPTRHDRRow[])dsToSave.BPTRHDR.Select("URNO='" + flightId + "'");
					if ((rows == null) || (rows.Length < 1))
					{
						throw new ApplicationException("No BPTR information to update for the flight.");
					}

					if (IsBptrExist(cmd, flightId))
					{
						DBBptr_Update(cmd, flightId, rows[0], userId, updateDateTime);
					}
					else
					{
						DbBptr_Insert(cmd, flightId, rows[0], userId, updateDateTime);
					}
				}
				catch (Exception ex)
				{
					LogMsg("SaveRpt:Err:" + ex.Message);
				}
			}
		}

		private static string GetDb_Tb_Bptr()
		{
			return DB_Info.GetDbFullName(DB_Info.DB_T_BPTR);
		}

		private static string GetDb_Vw_BptrHdr()
		{
			return DB_Info.GetDbFullName(DB_Info.DB_V_BPTR_HDR);
		}

		private static string GetDb_Vw_BptrDet()
		{
			return DB_Info.GetDbFullName(DB_Info.DB_V_BPTR_DET);
		}

		private static string GetDb_Vw_BptrSumm()
		{
			return DB_Info.GetDbFullName(DB_Info.DB_V_BPTR_SUMM);
		}

		public const string FIELDS_V_BPTR_HDR = //"URNO,FLDT,AT,FLNU,BOID,BOFN,BOLN,AIRCRAFT_TYPE,BELT,PARK_STAND,AOID,AOFNAME,AOLNAME,TBS_UP,BG_FST_TRP,BG_LST_TRP,FST_BAG,LST_BAG, BG_FST_BM_MET, BG_LST_BM_MET, AO_TRP_RMK, BORM";
		"URNO,FLDT,AT,FLNU,BO_ID,BO_FNAME,BO_LNAME,ACFT_TYPE,BELT, PARK_STAND, AO_ID, AO_FNAME, AO_LNAME, TBS_UP, FTRIP_B, LTRIP_B, FBAG, LBAG, FBAG_TIME_MET, LBAG_TIME_MET, AO_TRP_RMK, AO_TIME_RMK, BO_RMK";
		public const string FIELDS_V_BPTR_DET = "URNO,SR_NO,ULDNO,ULDTIME,CLASS";

		public bool IsBptrExist(IDbCommand cmd, string flightId)
		{
			return UtilDb.IsRecordExist(cmd, DB_Info.DB_T_BPTR, flightId);
		}

		public bool DBLoadDsBptr(IDbCommand cmd, string flightId,
		   DSBagPresentTimingRpt ds)
		{
			bool success = false;
			DateTime loadDateTime = ITrekLib.Misc.ITrekTime.GetCurrentDateTime();
			if (DBLoadDsBptrHdr(cmd, flightId, ds))
			{
				if (DBLoadDsBptrDet(cmd, flightId, ds)) success = true;
			}
			return success;
		}

		public bool DBLoadDsBptrHdr(IDbCommand cmd, string flightId,
		   DSBagPresentTimingRpt ds)
		{
			bool loadSuccess = false;
			StringBuilder sbWhere = new StringBuilder();
			UtilDb udb = UtilDb.GetInstance();

			try
			{
				cmd.Parameters.Clear();
				sbWhere.Append("URNO=" + udb.PrepareParam(cmd, "URNO", flightId));
				cmd.CommandText = string.Format("SELECT {0} FROM {1} WHERE {2}",
				   FIELDS_V_BPTR_HDR, GetDb_Vw_BptrHdr(), sbWhere.ToString());

				cmd.CommandType = CommandType.Text;

				udb.FillDataTable(cmd, ds.BPTRHDR);
				loadSuccess = true;
			}
			catch (Exception ex)
			{
				LogMsg("LoadDsMsg_MSG:Err:" + ex.Message);
			}

			return loadSuccess;
		}

		//public bool DBLoadDsBptrHdr(IDbCommand cmd, DateTime dtFrSentDt, DSBagPresentTimingRpt ds,
		//   bool changedDataOnly, DateTime dtLastUpd)
		//{
		//   bool loadSuccess = false;
		//   StringBuilder sbWhere = new StringBuilder();
		//   UtilDb udb = UtilDb.GetInstance();

		//   try
		//   {
		//      cmd.Parameters.Clear();
		//      if (changedDataOnly)
		//      {//Get the data with newly insert or changes on or after the given date and time
		//         cmd.CommandText += " AND LSTU>=" + udb.PrepareParam(cmd, "FR_DATE", dtLastUpd);
		//      }
		//      else
		//      {//Get the data with Send date time is on or after the given date and time.
		//      }
		//      cmd.CommandText = string.Format("SELECT {0} FROM {1} WHERE {2}",
		//         FIELDS_V_BPTR_HDR, GetDb_Vw_BptrHdr(), sbWhere.ToString());

		//      cmd.CommandType = CommandType.Text;

		//      udb.FillDataTable(cmd, ds.BPTRHDR);
		//      loadSuccess = true;
		//   }
		//   catch (Exception ex)
		//   {
		//      LogMsg("LoadDsMsg_MSG:Err:" + ex.Message);
		//   }

		//   return loadSuccess;
		//}

		public bool DBLoadDsBptrDet(IDbCommand cmd, string flightId,
		   DSBagPresentTimingRpt ds)
		{
			bool loadSuccess = false;
			StringBuilder sbWhere = new StringBuilder();
			UtilDb udb = UtilDb.GetInstance();

			try
			{
				cmd.Parameters.Clear();
				sbWhere.Append("URNO=" + udb.PrepareParam(cmd, "FLID", flightId));
				cmd.CommandText = string.Format("SELECT {0} FROM {1} WHERE {2}",
				   FIELDS_V_BPTR_DET, GetDb_Vw_BptrDet(), sbWhere.ToString());

				cmd.CommandType = CommandType.Text;

				udb.FillDataTable(cmd, ds.BPTRDET);
				loadSuccess = true;
			}
			catch (Exception ex)
			{
				LogMsg("LoadDsMsg_MSG:Err:" + ex.Message);
			}

			return loadSuccess;
		}

		public bool DBLoadDsBptrDet(IDbCommand cmd, DateTime dtFrDt,
		   DSBagPresentTimingRpt ds,
		   bool changedDataOnly, DateTime dtLastUpd)
		{
			bool loadSuccess = false;
			StringBuilder sbWhere = new StringBuilder();
			UtilDb udb = UtilDb.GetInstance();

			try
			{
				cmd.Parameters.Clear();
				if (changedDataOnly)
				{//Get the data with newly insert or changes on or after the given date and time
					cmd.CommandText += " AND LSTU>=" + udb.PrepareParam(cmd, "FR_DATE", dtLastUpd);
				}
				else
				{//Get the data with Send date time is on or after the given date and time.
				}
				cmd.CommandText = string.Format("SELECT {0} FROM {1} WHERE {2}",
				   FIELDS_V_BPTR_HDR, GetDb_Vw_BptrHdr(), sbWhere.ToString());

				cmd.CommandType = CommandType.Text;

				udb.FillDataTable(cmd, ds.BPTRHDR);
				loadSuccess = true;
			}
			catch (Exception ex)
			{
				LogMsg("LoadDsMsg_MSG:Err:" + ex.Message);
			}

			return loadSuccess;
		}


		private void DbBptr_Insert(IDbCommand cmd, string flightId,
		   DSBagPresentTimingRpt.BPTRHDRRow row,
		   string userId, DateTime updateDateTime)
		{
			StringBuilder sb = new StringBuilder();
			StringBuilder sbDbColNames = new StringBuilder();
			UtilDb udb = UtilDb.GetInstance();

			sbDbColNames.Append("URNO,BOID,BOFN,BOLN,BORM,CDAT,USEC,LSTU,USEU");
			cmd.Parameters.Clear();

			sb.Append(udb.PrepareParam(cmd, "URNO", flightId));
			sb.Append("," + udb.PrepareParam(cmd, "BOID", row.BO_ID));
			sb.Append("," + udb.PrepareParam(cmd, "BOFN", row.BO_FNAME));
			sb.Append("," + udb.PrepareParam(cmd, "BOLN", row.BO_LNAME));
			sb.Append("," + udb.PrepareParam(cmd, "BORM", row.BO_RMK));
			sb.Append("," + udb.PrepareParam(cmd, "CDAT", updateDateTime));
			sb.Append("," + udb.PrepareParam(cmd, "USEC", userId));
			sb.Append("," + udb.PrepareParam(cmd, "LSTU", updateDateTime));
			sb.Append("," + udb.PrepareParam(cmd, "USEU", userId));

			string stCmd = string.Format(@"INSERT INTO {0} ({1}) VALUES ({2})",
			   GetDb_Tb_Bptr(), sbDbColNames.ToString(), sb.ToString());

			if (stCmd != cmd.CommandText)
			{
				cmd.CommandText = stCmd;
				cmd.CommandType = CommandType.Text;
			}

			int updCnt = cmd.ExecuteNonQuery();
			if (updCnt < 1) throw new ApplicationException("Update Fail!!!");
		}

		private void DBBptr_Update(IDbCommand cmd, string flightId,
		   DSBagPresentTimingRpt.BPTRHDRRow row,
		   string userId, DateTime updateDateTime)
		{
			StringBuilder sb = new StringBuilder();
			StringBuilder sbDbColNames = new StringBuilder();
			UtilDb udb = UtilDb.GetInstance();
			List<string> lsColNames = new List<string>();
			string paramPrefix = UtilDb.GetParaNamePrefix();

			cmd.Parameters.Clear();
			string stWhere = "URNO=" + udb.PrepareParam(cmd, "URNO", flightId);
			cmd.CommandText = string.Format("SELECT BORM FROM {0} WHERE {1}", GetDb_Tb_Bptr(), stWhere);
			cmd.CommandType = CommandType.Text;
			string stBorm = cmd.ExecuteScalar().ToString();

			if (stBorm.Equals(row.BO_RMK))
			{
				//throw new ApplicationException("No changes to update for the flight.");
			}
			else
			{
				sb.Append(udb.PrepareUpdCmdToUpdDb(cmd, "BORM", row.BO_RMK));
				sb.Append("," + udb.PrepareUpdCmdToUpdDb(cmd, "LSTU", updateDateTime));
				sb.Append("," + udb.PrepareUpdCmdToUpdDb(cmd, "USEU", userId));

				string stCmd = string.Format("UPDATE {0} SET {1} WHERE {2}",
				   GetDb_Tb_Bptr(), sb.ToString(), stWhere);

				cmd.CommandText = stCmd;
				cmd.CommandType = CommandType.Text;

				int updCnt = cmd.ExecuteNonQuery();
				if (updCnt < 1) throw new ApplicationException("BPTR Update Fail!!!");
			}
		}

		public static DsDbBptr LoadDBBptr(IDbCommand cmd, string flightId)
		{
			DsDbBptr ds = new DsDbBptr();
			cmd.Parameters.Clear();
			cmd.CommandText = string.Format("SELECT * FROM {0} WHERE URNO='{1}'",
				GetDb_Tb_Bptr(), flightId);

			UtilDb.GetInstance().FillDataTable(cmd, ds.ITK_BPTR);
			return ds;
		}


		public void HkReport(ArrayList arrList)
		{
			int cnt = arrList.Count;
			for (int i = 0; i < cnt; i++)
			{
				//DelAReport(arrList[i].ToString().Trim());
			}
		}

		public static EntBptrSumm RetrieveBptrSummary(IDbCommand cmd,
			List<string> terminalIds, string stFrDateTime, string stToDateTime, string userId, out EntBptrSummCond cond)
		{
			if (
				((terminalIds == null) || (terminalIds.Count < 1)) //No Terminal for selection
				&& (string.IsNullOrEmpty(stFrDateTime) || (stFrDateTime.Trim() == ""))
				&& (string.IsNullOrEmpty(stToDateTime) || (stToDateTime.Trim() == ""))
			)
			{
				throw new ApplicationException("No selection criteria.");
			}

			DateTime frDateTime;
			DateTime toDateTime;
			EntBptrSumm summ = null;

			DataTable dt = new DataTable();
			string stWhere = " WHERE ADID='A'";
		    const string stConj = " AND ";
			if (terminalIds != null && terminalIds.Count > 0)
			{
				if ((terminalIds.Count == 1) && (string.IsNullOrEmpty(terminalIds[0]))) { }
				else
				{
					//string stTerminals = MM.UtilLib.UtilMisc.ConvListToString(terminalIds, ",", "'", "'", 0, terminalIds.Count);
					//LogTraceMsg("RetrieveBptrSummary:Terminals:" + stTerminals);
					//stWhere += String.Format("{0} TERMINAL IN ({1})",
					//    stConj,
					//    stTerminals);

					stWhere += String.Format("{0} TERMINAL IN ({1})",
						stConj,
						MM.UtilLib.UtilMisc.ConvListToString(terminalIds, ",", "'", "'", 0, terminalIds.Count));
				}
			}

			toDateTime = Misc.ITrekTime.GetCurrentDateTime();
			frDateTime = toDateTime.Date.AddMonths(-6);

			if (string.IsNullOrEmpty(stFrDateTime) || (stFrDateTime.Trim() == "")) { }
			else
			{
				DateTime dtTemp = UtilTime.ConvUfisTimeStringToDateTime(stFrDateTime);
				if (dtTemp.CompareTo(frDateTime) > 0) frDateTime = dtTemp;
			}

			if (string.IsNullOrEmpty(stToDateTime) || (stToDateTime.Trim() == "")) { }
			else
			{
				DateTime dtTemp = UtilTime.ConvUfisTimeStringToDateTime(stToDateTime);
				if (dtTemp.CompareTo(toDateTime) < 0) toDateTime = dtTemp;
			}

			cond = new EntBptrSummCond(terminalIds, frDateTime, toDateTime);

			UtilDb udb = UtilDb.GetInstance();

			stWhere += string.Format(" AND (BFLT BETWEEN {0} AND {1})",
				udb.PrepareParamForTime(cmd, "FRDT", frDateTime),
				udb.PrepareParamForTime(cmd, "TODT", toDateTime));

			string stSelCmd = @"SELECT 
  COUNT(*) totFlightCnt,
  SUM(FBMETCNT) totFbMetCnt,
  SUM(CASE WHEN L_H='L' THEN LBMETCNT ELSE 0 END) totLbMetLightLoaderCnt,
  SUM(CASE WHEN L_H='H' THEN LBMETCNT ELSE 0 END) totLbMetHeavyLoaderCnt,
  SUM(CASE WHEN L_H='L' THEN 1 ELSE 0 END) totLightLoaderCnt,
  SUM(CASE WHEN L_H='H' THEN 1 ELSE 0 END) totHeavyLoaderCnt
FROM " + GetDb_Vw_BptrSumm();
			cmd.CommandText = stSelCmd + stWhere;
			LogTraceMsg("RetrieveBptrSummary:" + userId + "," + cmd.CommandText);

			cmd.CommandType = CommandType.Text;
			using (IDataReader dr = cmd.ExecuteReader())
			{
				while (dr.Read())
				{
					int totFlightCnt = 0;
					UtilDb.TryGetDataInt(dr, "totFlightCnt", out totFlightCnt);
					int totFbMetCnt = UtilDb.GetDataInt(dr, "totFbMetCnt");
					summ = new EntBptrSumm(
						totFlightCnt,
						UtilDb.GetDataInt(dr, "totLightLoaderCnt"),
						UtilDb.GetDataInt(dr, "totHeavyLoaderCnt"),
						UtilDb.GetDataInt(dr, "totFbMetCnt"),
						UtilDb.GetDataInt(dr, "totLbMetLightLoaderCnt"),
						UtilDb.GetDataInt(dr, "totLbMetHeavyLoaderCnt"));
					break;
				}
			}

			return summ;
		}

		#region Logging
		//[Conditional("TRACING_ON")]
		private static void LogTraceMsg(string msg)
		{
			try
			{
				MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("DBDBptr", msg);
			}
			catch (Exception)
			{
				UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DBDBptr", msg);
			}
		}

		//[Conditional("LOGGING_ON")]
		private static void LogMsg(string msg)
		{
			LogTraceMsg(msg);
		}
		#endregion
	}
}