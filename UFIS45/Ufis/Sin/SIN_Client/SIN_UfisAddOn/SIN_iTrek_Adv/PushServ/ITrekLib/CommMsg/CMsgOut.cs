using System;
using System.Collections.Generic;
using System.Text;

using System.Collections;
using System.Data;

using UFIS.ITrekLib.Util;
using MM.UtilLib;
using UFIS.ITrekLib.Config;
using UFIS.ITrekLib.CustomException;
using UFIS.ITrekLib.Ctrl;
using iTrekXML;

namespace UFIS.ITrekLib.CommMsg
{
	public class CMsgOut
	{
		#region Flight Timing to send to Backend UFIS
		public const string MSG_TYPE_FLIGHT_TIMING_UPD = "FT";
		/// <summary>
		/// Prepare Message for Flight Timing to send to backend UFIS
		/// </summary>
		/// <param name="uaft">Urno of AFTTAB</param>
		/// <param name="fltType">Flight Type (A/D)</param>
		/// <param name="peno">Employee's PENO</param>
		/// <param name="fncName">Function Name (Timing Name)</param>
		/// <param name="time">Date Time in ufis format yyyymmddhhmiss</param>
		/// <returns>Formatted message string</returns>
		public static string MsgForFlightTiming(string uaft, string fltType, string peno, string fncName, string time)
		{
			Hashtable orgTable = new Hashtable();
			orgTable.Clear();
			orgTable.Add("MAB", "AO");
			orgTable.Add("LOAD-START", "AO");
			orgTable.Add("LOAD-END", "AO");
			orgTable.Add("LAST-DOOR", "AO");
			orgTable.Add("PLB", "AO");
			orgTable.Add("ATD", "AO");
			orgTable.Add("ATA", "AO");
			orgTable.Add("TBS-UP", "AO");
			orgTable.Add("UNLOAD-START", "AO");
			orgTable.Add("UNLOAD-END", "AO");
			orgTable.Add("FBAG", "BO");
			orgTable.Add("LBAG", "BO");
			orgTable.Add("FTRIP-B", "BO");
			orgTable.Add("LTRIP-B", "BO");
			if (fltType == "D")
			{
				orgTable.Add("FTRIP", "BO");
				orgTable.Add("LTRIP", "BO");
			}
			else
			{
				orgTable.Add("FTRIP", "AO");
				orgTable.Add("LTRIP", "AO");

			}
			string message = "";
			if (fltType == "D")
			{
				message = "<DFLIGHT><URNO>" + uaft + "</URNO><" + fncName + ">" + time + "</" + fncName + "><ORG>" + orgTable[fncName].ToString() + "</ORG><PENO>" + peno + "</PENO></DFLIGHT>";
			}
			else
			{
				message = "<AFLIGHT><URNO>" + uaft + "</URNO><" + fncName + ">" + time + "</" + fncName + "><ORG>" + orgTable[fncName].ToString() + "</ORG><PENO>" + peno + "</PENO></AFLIGHT>";
			}
			return message;
		}

		#endregion

		#region Message to request the Staff Info. To send to backend UFIS
		public static string MsgForRequestStaffInfo(List<string> lsPenos)
		{
			//<STFREQ>
			//   <PNO>1234533</PNO> //PENO
			//   <PNO>3453134</PNO> //PENO
			//</STFREQ>

			StringBuilder sb = new StringBuilder();
			sb.Append("<STFREQ>");
			foreach (string peno in lsPenos)
			{
				if (!string.IsNullOrEmpty(peno))
				{
					sb.Append(string.Format("<PNO>{0}</PNO>", peno));
				}
			}
			sb.Append("</STFREQ>");
			return sb.ToString();
		}

		public const string MSG_TYPE_REQUEST_STAFF_INFO = "STFREQ";
        public const string MSG_TYPE_SEND_ALERT = "ALERT";

		private static string GetMqNameToRequestStaffInfo()
		{
			return ITrekPath.GetInstance().GetTOITREKREAD_RESQueueName();
		}

		/// <summary>
		/// Request the Staff Info from Backend. Return the id of the message.
		/// </summary>
		/// <param name="cmd">Command to use. Must not be null</param>
		/// <param name="lsPenos">List of Penos</param>
		/// <param name="sentByUserId">user id who send the request</param>
		/// <param name="replyTo">Reply to</param>
		/// <returns>id of the message</returns>
		public static string RequestStaffInfo(IDbCommand cmd, List<string> lsPenos, string sentByUserId, out string replyTo)
		{
			return NewMessageToSend(cmd, GetMqNameToRequestStaffInfo(), MsgForRequestStaffInfo(lsPenos),
			   sentByUserId, MSG_TYPE_REQUEST_STAFF_INFO, out replyTo);
		}

		#endregion

		#region MSGOUT Methods
		const string DB_TAB_MSG_OUT = "ITK_MGOTAB";
		const string MSG_OUT_STAT_PROCESSED = "P"; //Processed (Put into MQ)
		const string MSG_OUT_STAT_OUT = "O"; //Not processed yet. (Not yet put into MQ)
        private static string GetDb_Tb_MsgOut()
        {
            return DB_Info.GetDbFullName(DB_Info.DB_T_MGOTAB);
        }
		/// <summary>
		/// Update the Processed Indicator for Out Message
		/// To show that this message is already put into the communication channel (MQ)
		/// Return True when success
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="outMsgId">outMsgId- Id of ITK_MGOTAB to update</param>
		/// <param name="procBy">user id who process it</param>
		/// <param name="procDt">Processed Date Time. "" - To use the current date time</param>
		/// <returns>True-Successfully Updated the status.</returns>
		public static bool UpdProcIndForOutMessage(IDbCommand cmd,
		   string outMsgId, string procBy, string procDt)
		{
			UtilDb udb = UtilDb.GetInstance();

			bool success = false;

			if (string.IsNullOrEmpty(procDt))
			{
				procDt = UFIS.ITrekLib.Misc.ITrekTime.GetCurrentDateTimeInUfisDateTimeString();
			}

			cmd.Parameters.Clear();
            cmd.CommandText = "UPDATE " + GetDb_Tb_MsgOut()
			   + " SET " + udb.PrepareUpdCmdToUpdDb(cmd, "PRBY", procBy)
			   + "," + udb.PrepareUpdCmdToUpdDbForTime(cmd, "PRDT", procDt)
			   + "," + udb.PrepareUpdCmdToUpdDb(cmd, "STAT", MSG_OUT_STAT_PROCESSED)
			   + " WHERE URNO=" + udb.PrepareParam(cmd, "URNO", outMsgId);
			cmd.CommandType = CommandType.Text;

			int cnt = cmd.ExecuteNonQuery();
			if (cnt == 1) success = true;
			else throw new ApplicationException("Fail to update the Processed Indicator.");

			return success;
		}

		/// <summary>
		/// Update the processing Count for Unsuccessfull Attempt
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="outMsgId"></param>
		/// <param name="procBy"></param>
		/// <param name="procDt"></param>
		/// <returns></returns>
		public static bool UpdProcCntForOutMessage(IDbCommand cmd,
		   string outMsgId, string procBy, string procDt)
		{
			UtilDb udb = UtilDb.GetInstance();

			bool success = false;

			if (string.IsNullOrEmpty(procDt))
			{
				procDt = UFIS.ITrekLib.Misc.ITrekTime.GetCurrentDateTimeInUfisDateTimeString();
			}

			cmd.Parameters.Clear();
            cmd.CommandText = "UPDATE " + GetDb_Tb_MsgOut()
			   + " SET " + udb.PrepareUpdCmdToUpdDb(cmd, "PRBY", procBy)
			   + "," + udb.PrepareUpdCmdToUpdDbForTime(cmd, "PRDT", procDt)
			   + ",CNT=(CNT+1)"
			   + " WHERE URNO=" + udb.PrepareParam(cmd, "URNO", outMsgId);
			cmd.CommandType = CommandType.Text;

			int cnt = cmd.ExecuteNonQuery();
			if (cnt == 1) success = true;
			else throw new ApplicationException("Fail to update the Processing Count.");

			return success;
		}

		/// <summary>
		/// Send the new message by inserting a record into "MGOTAB".
		/// Insert a record (of message to put in MQ) into "MGOTAB". 
		/// Message will be sent accordingly by other windows service process.
		/// And return the Urno of the ITK_MGOTAB
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="sendTo">Send To (destination of message. It might be MQ Channel)</param>
		/// <param name="msg">Message Text to send</param>
		/// <param name="sentBy">Send By User Id</param>
		/// <param name="msgType">Message Type</param>
		/// <param name="replyTo">Id to reply to (same as urno in MGOTAB)</param>
		/// <returns>URNO of MGOTAB for this record.</returns>
		public static string NewMessageToSend(IDbCommand cmd,
		   string sendTo, string msg, string sentBy, string msgType, out string replyTo)
		{
			string paramPrefix = UtilDb.GetParaNamePrefix();
			string pSendTo = paramPrefix + "TODE";
			string pMsg = paramPrefix + "MSGT";
			string pSendBy = paramPrefix + "SNBY";
			string pSendDt = paramPrefix + "SNDT";
			string pStat = paramPrefix + "STAT";
			string pRpyt = paramPrefix + "RPYT";
			string pMsgType = paramPrefix + "MTYP";
			string pUrno = paramPrefix + "URNO";

			replyTo = "";
			UtilDb udb = UtilDb.GetInstance();
			DateTime dt1 = DateTime.Now;
			string urno = "";
			bool success = false;
            bool isNewCmd = false;//Is command 'cmd' created from this method?
            try
            {
                for (int i = 0; i < 50; i++)
                {
                    try
                    {
                        if (cmd == null)
                        {
                            isNewCmd = true;
                            cmd = udb.GetCommand(true);
                        }
                        urno = UtilDb.GenUrid(cmd, ProcIds.MGO, false, dt1);
                        cmd.CommandText = String.Format(@"INSERT INTO {0} 
            (URNO,TODE,MSGT,SNBY,SNDT,STAT,RPYT,MTYP) 
            VALUES ({1},{2},{3},{4},SYSDATE,{5},{6},{7})",
                           GetDb_Tb_MsgOut(), pUrno, pSendTo, pMsg, pSendBy, pStat, pRpyt, pMsgType);
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.Clear();
                        udb.AddParam(cmd, pUrno, urno);
                        udb.AddParam(cmd, pSendTo, sendTo);
                        udb.AddParam(cmd, pMsg, msg);
                        udb.AddParam(cmd, pSendBy, sentBy);
                        udb.AddParam(cmd, pStat, MSG_OUT_STAT_OUT);
                        udb.AddParam(cmd, pRpyt, urno);
                        udb.AddParam(cmd, pMsgType, msgType);
						//LogTraceMsg("NewMessageToSend:Cmd:" + cmd.CommandText);
                        int cnt = cmd.ExecuteNonQuery();
						//LogTraceMsg("cnt" + cnt);
                        if (cnt == 1)
                        {
                            success = true;
                            break;
                        }
                    }
                    catch (NoCtrlSettingException)
                    {
                        throw;
                    }
                    catch (Exception)
                    {
						System.Threading.Thread.Sleep(10);
                        continue;
                    }
                }
                if (!success) throw new ApplicationException("Unable to create new message to send.");

            }
            catch (Exception)
            {
                throw;                  
            }

            finally
			{
				if (isNewCmd)
				{
                    udb.CloseCommand(cmd, success);
				}
			}
			return urno;
		}
		#endregion

		#region Process Mgo Records to send the information to other system via MQ/iDen Network
		private static DateTime dtLastRefresh = DateTime.Now;
		/// <summary>
		/// Process All Out Messages (Put into MQ and Update the stat indicator to show that it was processed.)
		/// </summary>
		/// <param name="conn">Connection</param>
		/// <param name="procById">User/Process Id who process this</param>
		/// <param name="errorMsgType">List of Message Types which has error in processing</param>
		/// <returns>True - All processed successfully. False - Has some error.</returns>
		public static bool ProcessAll(IDbConnection conn, string procById, out List<String> errorMsgType)
		{
			errorMsgType = new List<string>();
			bool successAll = true;
			if (UtilTime.DiffInSec(DateTime.Now, dtLastRefresh)>30)
			{
				LogTraceMsg("ProcessAll");
				dtLastRefresh = DateTime.Now;
			}

			//lock (_lockPutMsgToMq)
			{
				UtilDb udb = UtilDb.GetInstance();
				#region Open Connection
				bool isNewConn = false;

				if (conn == null)
				{
					conn = udb.GetConnection(true);
					isNewConn = true;
				}

				if ((conn.State == ConnectionState.Closed) || (conn.State == ConnectionState.Broken))
				{
					conn.Open();
				}
				#endregion

				string msgType = "";
				try
				{
					#region Process 'Flight Timing Update'
					msgType = MSG_TYPE_FLIGHT_TIMING_UPD;
					//Sequence is import for Flight Timing Update. Do not proceed if one message fail.
					if (!ProcessByMsgType(conn, msgType, false, procById))
					{
						successAll = false;
						errorMsgType.Add(msgType);
					}
					#endregion

					#region Process 'Request Staff Information.'
					msgType = MSG_TYPE_REQUEST_STAFF_INFO;
					//Sequence is not important for Staff Info request. Proceed if one message fail.
					if (!ProcessByMsgType(conn, msgType, true, procById))
					{
						successAll = false;
						errorMsgType.Add(msgType);
					}
					#endregion
					#region Process 'Send Alert to Devices.'
					msgType = MSG_TYPE_SEND_ALERT;
					//Sequence is not important for Staff Info request. Proceed if one message fail.
					if (!ProcessAlert(conn, msgType, true, procById))
					{
						successAll = false;
						errorMsgType.Add(msgType);
					}
					#endregion
				}
				catch (Exception ex)
				{
					successAll = false;
					LogTraceMsg("ProcessAll:Err:" + ex.Message);
				}
				finally
				{
					#region Close Connection, if it is opened by this method
					if (isNewConn) UtilDb.CloseConnection(conn);
					#endregion
				}
			}

			return successAll;
		}
		#endregion

		#region Put the message into MQ

		public static object _lockPutMsgToMq = new object();

		/// <summary>
		/// Put the message into MQ for given outMsgId of ITK_MGOTAB 
		/// and it will be updated in ITK_MGOTAB to show that this message was already put into MQ
		/// and Commit it.
		/// </summary>
		/// <param name="conn">DB Connection</param>
		/// <param name="outMsgId">outMessageId</param>
		/// <param name="procById">user id who is doing the processing</param>
		/// <returns></returns>
		public static bool PutIntoMQ(IDbConnection conn, string outMsgId, string procById)
		{
			bool isNewConn = false;
			UtilDb udb = UtilDb.GetInstance();

			if (conn == null)
			{
				conn = udb.GetConnection(true);
				isNewConn = true;
			}

			if ((conn.State == ConnectionState.Closed) || (conn.State == ConnectionState.Broken))
			{
				conn.Open();
			}

			bool success = false;
			string message = "";
			string replyTo = "";
			string queueName = "";
			string curStat = "";

			IDbCommand cmd = null;
			bool commit = false;

			try
			{
				cmd = udb.GetCommandWithTrx(conn);

				#region Get Message Info for the given id
				cmd.Parameters.Clear();
				cmd.CommandText = string.Format("SELECT TODE,MSGT,RPYT,STAT FROM {0} WHERE URNO={1}",
                   GetDb_Tb_MsgOut(),
				   udb.PrepareParam(cmd, "URNO", outMsgId));
				cmd.CommandType = CommandType.Text;

				using (IDataReader dr = cmd.ExecuteReader())
				{
					while (dr.Read())
					{
						queueName = Convert.ToString(dr["TODE"]);
						message = Convert.ToString(dr["MSGT"]);
						replyTo = Convert.ToString(dr["RPYT"]);
						curStat = Convert.ToString(dr["STAT"]);
						break;
					}
				}
				#endregion

				if (curStat == MSG_OUT_STAT_PROCESSED)
				{
					success = true;
				}
				else
				{
					bool hasPutIntoMq = false;
					if (!string.IsNullOrEmpty(message) && !string.IsNullOrEmpty(queueName))
					{
						hasPutIntoMq = PutTheMsgIntoMq(message, queueName, replyTo);
					}
					else
					{//No queue Name or message. Nothing to put into MQ
						hasPutIntoMq = true;
					}

					if (hasPutIntoMq)
					{
						bool updInd = UpdProcIndForOutMessage(cmd, outMsgId, procById, "");
						success = true;
						commit = updInd;
					}
				}
			}
			catch (Exception)
			{
				throw;
			}
			finally
			{
				bool closeConnection = false;
				if (isNewConn) closeConnection = true;
				udb.CloseCommand(cmd, commit, closeConnection);
			}

			return success;
		}

		/// <summary>
		/// Scan all the unProcessed (Not yet put into MQ) message of given message type 
		/// in Message send Date Time order and put them into MQ.
		/// After successfully place the message into MQ, update the status as Processed for that message
		/// </summary>
		/// <param name="conn">Connection to use. If null, it will create a new connection.</param>
		/// <param name="msgType">Message Type to send the messages</param>
		/// <param name="skipError">True - Skip when unsuccessful/Error. False - Not to skip on error and throw exception</param>
		/// <param name="procById">User/Process Id who process this</param>
		/// <returns></returns>
		public static bool ProcessByMsgType(IDbConnection conn, string msgType, bool skipError, string procById)
		{
			bool successAll = true;
			//LogTraceMsg("ProcessByMsgType:" + msgType);
			//lock (_lockPutMsgToMq)
			{
				#region Get Open Connection
				bool isNewConn = false;
				UtilDb udb = UtilDb.GetInstance();

				if (conn == null)
				{
					conn = udb.GetConnection(true);
					isNewConn = true;
				}

				if ((conn.State == ConnectionState.Closed) || (conn.State == ConnectionState.Broken))
				{
					conn.Open();
				}
				#endregion

				IDbCommand cmd = null;
				bool commit = false;

				try
				{
					cmd = udb.GetCommand(conn, null);

					#region Select records
					cmd.Parameters.Clear();
					cmd.CommandText = string.Format("SELECT URNO,TODE,MSGT,RPYT,STAT FROM {0} WHERE MTYP={1} AND STAT='O' ORDER BY SNDT",
                       GetDb_Tb_MsgOut(),
					   udb.PrepareParam(cmd, "MTYP", msgType));
					cmd.CommandType = CommandType.Text;
					#endregion

					#region Process Records
					using (IDataReader dr = cmd.ExecuteReader())
					{
						while (dr.Read())
						{
							IDbCommand cmdUpd = null;

							#region init var
							bool success = false;
							string message = "";
							string replyTo = "";
							string queueName = "";
							string curStat = "";
							string outMsgId = "";
							bool commitUpd = false;
							#endregion

							try
							{
								cmdUpd = udb.GetCommandWithTrx(conn);

								#region Get Message Info for the given id
								outMsgId = Convert.ToString(dr["URNO"]);
								queueName = Convert.ToString(dr["TODE"]);
								message = Convert.ToString(dr["MSGT"]);
								replyTo = Convert.ToString(dr["RPYT"]);
								curStat = Convert.ToString(dr["STAT"]);
								#endregion

								#region Put the message into Mq and Update the status
								if (curStat == MSG_OUT_STAT_PROCESSED)
								{
									success = true;
								}
								else
								{
									bool hasPutIntoMq = false;
									if (!string.IsNullOrEmpty(message) && !string.IsNullOrEmpty(queueName))
									{
										hasPutIntoMq = PutTheMsgIntoMq(message, queueName, replyTo);
										if (hasPutIntoMq)
										{
											if (UpdProcIndForOutMessage(cmdUpd, outMsgId, procById, ""))
											{
												success = true;
												commitUpd = true;
											}
										}
										else
										{
											if (UpdProcCntForOutMessage(cmdUpd, outMsgId, procById, ""))
											{
												success = false;
												commitUpd = true;
											}
										}
									}
									else
									{//No queue Name or message. Nothing to put into MQ
										hasPutIntoMq = true;
										if (UpdProcIndForOutMessage(cmdUpd, outMsgId, procById, ""))
										{//Don't care. It is not import  
											commitUpd = true;
										}

										success = true;
									}
								}
								#endregion
							}
							catch (Exception ex)
							{
								commitUpd = false;
								LogMsg("ProcessByMsgType:Err:" + msgType + ":" + outMsgId + ":" + ex.Message);
								success = false;
							}

							udb.CloseCommand(cmdUpd, commitUpd, false);

							if ((!success) && (!skipError))
							{
								successAll = false;
								break;
							}
						}
					}
					#endregion
				}
				catch (Exception)
				{
					successAll = false;
					throw;
				}
				finally
				{
					bool closeConnection = false;
					if (isNewConn) closeConnection = true;
					udb.CloseCommand(cmd, commit, closeConnection);
				}
			}

			return successAll;
		}

		/// <summary>
		/// Put a message into MQ under given queueName 
		/// </summary>
		/// <param name="message">Message to put into MQ</param>
		/// <param name="queueName">Queue Name</param>
		/// <param name="replyTo">Reply To Id (for future use)</param>
		/// <returns></returns>
		private static bool PutTheMsgIntoMq(string message, string queueName, string replyTo)
		{
			bool success = false;
			iTrekWMQ.ITrekWMQClass1 mq = new iTrekWMQ.ITrekWMQClass1();
			if ("success" == mq.putTheMessageIntoQueue(message, queueName))
			{
				success = true;
			}
			return success;
		}
		#endregion


		#region Send Net Alert to Handheld
		/// <summary>
		/// Scan all the unProcessed alerts message 
		/// in Message send Date Time order 
		/// After successfully sending alert, update the status as Processed and update the PID for that message
		/// </summary>
		/// <param name="conn">Connection to use. If null, it will create a new connection.</param>
		/// <param name="msgType">Message Type to send the messages</param>
		/// <param name="skipError">True - Skip when unsuccessful/Error. False - Not to skip on error and throw exception</param>
		/// <param name="procById">User/Process Id who process this</param>
		/// <returns></returns>
		public static bool ProcessAlert(IDbConnection conn, string msgType, bool skipError, string procById)
		{
			bool successAll = true;

			//lock (_lockPutMsgToMq)
			{
				#region Get Open Connection
				bool isNewConn = false;
				UtilDb udb = UtilDb.GetInstance();

				if (conn == null)
				{
					conn = udb.GetConnection(true);
					isNewConn = true;
				}

				if ((conn.State == ConnectionState.Closed) || (conn.State == ConnectionState.Broken))
				{
					conn.Open();
				}
				#endregion

				IDbCommand cmd = null;
				bool commit = false;

				try
				{
					cmd = udb.GetCommand(conn, null);

					#region Select records
					cmd.Parameters.Clear();
					cmd.CommandText = string.Format("SELECT URNO,TODE,MSGT,RPYT,STAT FROM {0} WHERE MTYP={1} AND STAT='O' ORDER BY SNDT",
					  GetDb_Tb_MsgOut(),
					   udb.PrepareParam(cmd, "MTYP", msgType));
					cmd.CommandType = CommandType.Text;
					#endregion

					#region Process Records
					using (IDataReader dr = cmd.ExecuteReader())
					{
						while (dr.Read())
						{
							IDbCommand cmdUpd = null;

							#region init var
							bool success = false;
							string message = "";
							string replyTo = "";
							string deviceId = "";
							string curStat = "";
							string outMsgId = "";
							bool commitUpd = false;
							#endregion

							try
							{
								cmdUpd = udb.GetCommandWithTrx(conn);

								#region Get Message Info for the given id
								outMsgId = Convert.ToString(dr["URNO"]);
								deviceId = Convert.ToString(dr["TODE"]);
								message = Convert.ToString(dr["MSGT"]);
								replyTo = Convert.ToString(dr["RPYT"]);
								curStat = Convert.ToString(dr["STAT"]);
								#endregion

								#region Send the message to Handheld via iDen network and Update the status
								if (curStat == MSG_OUT_STAT_PROCESSED)
								{
									success = true;
								}
								else
								{
									string pid = "";
									if (!string.IsNullOrEmpty(message) && !string.IsNullOrEmpty(deviceId))
									{
										deviceId = deviceId.Substring(0, deviceId.IndexOf('|'));
										LogTraceMsg(deviceId);
										pid = pushMessage(deviceId, message);
										if (pid != "")
										{
											if (UpdProcIndForOutMessage(cmdUpd, outMsgId, pid, ""))
											{
												success = true;
												commitUpd = true;
											}
										}
										else
										{
											if (UpdProcCntForOutMessage(cmdUpd, outMsgId, pid, ""))
											{
												success = false;
												commitUpd = true;
											}
										}
									}
									//else
									//{//No queue Name or message. Nothing to put into MQ

									//    if (UpdProcIndForOutMessage(cmdUpd, outMsgId, procById, ""))
									//    {//Don't care. It is not import  
									//        commitUpd = true;
									//    }

									//    success = true;
									//}
								}
								#endregion
							}
							catch (Exception ex)
							{
								commitUpd = false;
								LogTraceMsg("ProcessAlert:Err:" + msgType + ":" + outMsgId + ":" + ex.Message);
								success = false;
							}

							udb.CloseCommand(cmdUpd, commitUpd, false);

							if ((!success) && (!skipError))
							{
								successAll = false;
								break;
							}
						}
					}
					#endregion
				}
				catch (Exception)
				{
					successAll = false;
					throw;
				}
				finally
				{
					bool closeConnection = false;
					if (isNewConn) closeConnection = true;
					udb.CloseCommand(cmd, commit, closeConnection);
				}
			}

			return successAll;
		}

		/// <summary>
		/// Get the session id for given device id
		/// </summary>
		/// <param name="deviceId">device id</param>
		/// <returns></returns>
		private static string GetSessionIdForDeviceId(string deviceId)
		{
			string sessionId = "";
			try
			{
				sessionId = CtrlCurUser.RetrieveCurUserForDeviceId(deviceId).LOGINUSER[0].SESS_ID;
			}
			catch (Exception)
			{
			}

			return sessionId;
			//  SessionManager sessMan = new SessionManager();
			//  return sessMan.GetSessionIdByDeviceId(deviceId);
		}

		/// <summary>
		/// Push the message 'msgBody' to Handhdle for the 'deviceId' via iDen network. Return the PID.
		/// </summary>
		/// <param name="deviceId">device id</param>
		/// <param name="msgBody">message to send</param>
		/// <returns></returns>
		private static string pushMessage(string deviceId, string msgBody)
		{
			if (string.IsNullOrEmpty(deviceId)) return "";

			XmlPushClient pushClient = new XmlPushClient(GetPapUrl());
			string PID = "";
			try
			{

				//string url = GetUrlForNetAlert();//Commented out by AM@20090421
				string url = GetUrlForNetAlert() + "(S(" + GetSessionIdForDeviceId(deviceId) + "))/Messages.aspx?newmessage=alert";
				LogTraceMsg("url:" + url);

				if (deviceId != "")
				{
					// PID = PushClient.PushServiceIndication(deviceId, "New Msg-" + msgBody, "http://172.31.246.42/itrek/Messages.aspx?newmessage=alert");
					PID = pushClient.PushServiceIndication(deviceId, msgBody, url);
				}
				LogTraceMsg("PushId :" + PID + "-devicedId:" + deviceId);
			}
			catch (Exception ex)
			{
				LogTraceMsg("pushMessage exception:" + ex.Message);
			}
			return PID;
		}

		private static string GetPapUrl()
		{
			iTXMLPathscl paths = new iTXMLPathscl();
			return paths.GetPapURL();
		}

		private static string GetUrlForNetAlert()
		{
			iTXMLPathscl paths = new iTXMLPathscl();
			return paths.GetURLForNetAlert();
		}
		#endregion

		#region Logging
		private static void LogTraceMsg(string msg)
		{
			try
			{
				MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("CMsgOut", msg);
			}
			catch (Exception)
			{
				UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("CMsgOut", msg);
			}
		}

		private static void LogMsg(string msg)
		{
			try
			{
				MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("CMsgOut", msg);
			}
			catch (Exception)
			{
				UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("CMsgOut", msg);
			}
		}
		#endregion
	}
}
