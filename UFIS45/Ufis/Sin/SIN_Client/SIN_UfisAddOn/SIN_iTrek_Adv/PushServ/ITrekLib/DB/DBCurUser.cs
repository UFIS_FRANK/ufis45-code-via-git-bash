using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;

using UFIS.ITrekLib.DS;
using iTrekXML;
using UFIS.ITrekLib.Util;
using UFIS.ITrekLib.ENT;

namespace UFIS.ITrekLib.DB
{
    public class DBCurUser
    {
        private static Object _lockObj = new Object();
        private static DBCurUser _dbCurUser = null;
        private static DateTime _lastWriteDT = new System.DateTime(1, 1, 1);

        private static DSCurUser _dsCurUser = null;
        private static string _fnDeviceIdXml = null;


        private DBCurUser() { }

        public void Init()
        {
            _dsCurUser = null;
            _fnDeviceIdXml = null;
            _lastWriteDT = new System.DateTime(1, 1, 1);
        }

        public static DBCurUser GetInstance()
        {
            if (_dbCurUser == null)
            {
                _dbCurUser = new DBCurUser();
            }
            return _dbCurUser;
        }

        public string GetCurUserValueFor(string CurUserFor, out string desc)
        {
            string value = "";
            desc = "";
            DSCurUser ds = new DSCurUser();
            DSCurUser tempDs = dsCurUser;
            DSCurUser.LOGINUSERRow[] rowArr = (DSCurUser.LOGINUSERRow[])tempDs.LOGINUSER.Select("ID='" + CurUserFor + "'");
            if (rowArr.Length > 0)
            {
                //value = rowArr[0].VAL;
                //desc = rowArr[0].ID_DESC;
            }

            return value;
        }

        public DSCurUser RetrieveCurUser()
        {
            DSCurUser ds = new DSCurUser();
            ds.LOGINUSER.Merge(dsCurUser.LOGINUSER);
            return ds;
        }

        public DSCurUser RetrieveCurUserForSessionId(string sessionid)
        {
            DSCurUser ds = new DSCurUser();
            DSCurUser tempDs = dsCurUser;
            foreach (DSCurUser.LOGINUSERRow row in tempDs.LOGINUSER.Select("SESS_ID='" + sessionid + "'"))
            {
                ds.LOGINUSER.LoadDataRow(row.ItemArray, true);
            }
            return ds;
        }


        public DSCurUser RetrieveCurUserForStaffId(string staffid)
        {
            DSCurUser ds = new DSCurUser();
            DSCurUser tempDs = dsCurUser;
            foreach (DSCurUser.LOGINUSERRow row in tempDs.LOGINUSER.Select("UID='" + staffid + "'"))
            {
                ds.LOGINUSER.LoadDataRow(row.ItemArray, true);
            }
            return ds;
        }

        public DSCurUser RetrieveCurUserForDeviceId(string deviceid)
        {
            DSCurUser ds = new DSCurUser();
            DSCurUser tempDs = dsCurUser;
            foreach (DSCurUser.LOGINUSERRow row in tempDs.LOGINUSER.Select("DEVICE_ID='" + deviceid + "'"))
            {
                ds.LOGINUSER.LoadDataRow(row.ItemArray, true);
            }
            return ds;
        }



        public void DeleteDeviceIdEntryBySessID(string sessionid)
        {
            lock (_lockObj)
            {
                DSCurUser ds = new DSCurUser();
                ds.ReadXml(fnDeviceIdXml);
                foreach (DSCurUser.LOGINUSERRow oldRow in ds.LOGINUSER.Select("SESS_ID='" + sessionid + "'"))
                {
                    ds.LOGINUSER.RemoveLOGINUSERRow(oldRow);
                    // throw new ApplicationException("record Id " + id + " already existed.");
                    //oldRow.Delete();
                }
                ds.LOGINUSER.AcceptChanges();
                ds.WriteXml(fnDeviceIdXml);

            }

        }

        public void DeleteDeviceIdEntryByStaffID(string staffid)
        {
            lock (_lockObj)
            {
                DSCurUser ds = new DSCurUser();
                ds.ReadXml(_fnDeviceIdXml);
                foreach (DSCurUser.LOGINUSERRow oldRow in ds.LOGINUSER.Select("UID='" + staffid + "'"))
                {
                    ds.LOGINUSER.RemoveLOGINUSERRow(oldRow);
                    // throw new ApplicationException("record Id " + id + " already existed.");
                    //oldRow.Delete();
                }
                ds.LOGINUSER.AcceptChanges();
                ds.WriteXml(fnDeviceIdXml);

            }


        }
        public void CreateNewUser(string sessid, string loginType, string deviceid, string staffid, string time, string org, string name)
        {
            lock (_lockObj)
            {
                DSCurUser ds = new DSCurUser();
                ds.ReadXml(fnDeviceIdXml);
                foreach (DSCurUser.LOGINUSERRow oldRow in ds.LOGINUSER.Select("UID='" + staffid + "'"))
                {
                    ds.LOGINUSER.RemoveLOGINUSERRow(oldRow);
                    // throw new ApplicationException("record Id " + id + " already existed.");
                    //oldRow.Delete();
                }
                foreach (DSCurUser.LOGINUSERRow oldRow in ds.LOGINUSER.Select("DEVICE_ID='" + deviceid + "'"))
                {
                    ds.LOGINUSER.RemoveLOGINUSERRow(oldRow);
                    // throw new ApplicationException("record Id " + id + " already existed.");
                    //oldRow.Delete();
                }
                DSCurUser.LOGINUSERRow row = ds.LOGINUSER.NewLOGINUSERRow();
                row.UID = staffid.Trim().ToUpper();
                row.SESS_ID = sessid.Trim();
                row.LGTYPE = loginType;
                row.DEVICE_ID = deviceid.Trim();
                //row.LGTYPE = desc;
                row.NAME = name;
                row.ROLE = org;
                ds.LOGINUSER.AddLOGINUSERRow(row);
                ds.LOGINUSER.AcceptChanges();

                ds.WriteXml(fnDeviceIdXml);
            }
        }

        public void UpdateCurUser(string id, string value)
        {
            lock (_lockObj)
            {
                DSCurUser ds = new DSCurUser();
                ds.ReadXml(fnDeviceIdXml);
                foreach (DSCurUser.LOGINUSERRow row in ds.LOGINUSER.Select("ID='" + id + "'"))
                {
                    // row.VAL = value.Trim();
                }
                ds.LOGINUSER.AcceptChanges();
                ds.WriteXml(fnDeviceIdXml);
            }
        }

        private string fnDeviceIdXml
        {
            get
            {
                if ((_fnDeviceIdXml == null) || (_fnDeviceIdXml == ""))
                {
                    iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
                    _fnDeviceIdXml = path.GetDeviceIDFile();
                }

                return _fnDeviceIdXml;
            }
        }

        private DSCurUser dsCurUser
        {
            get
            {
                LoadDSCurUser();
                DSCurUser ds = (DSCurUser)_dsCurUser.Copy();

                return ds;
            }
        }

        private void LoadDSCurUser()
        {
            FileInfo fiXml = new FileInfo(fnDeviceIdXml);
            DateTime curDT = fiXml.LastWriteTime;

            if ((_dsCurUser == null) || (curDT.CompareTo(_lastWriteDT) != 0))
            {
                lock (_lockObj)
                {
                    try
                    {
                        curDT = fiXml.LastWriteTime;//check the time again, in case of lock
                        if ((_dsCurUser == null) || (curDT.CompareTo(_lastWriteDT) != 0))
                        {
                            try
                            {
                                DSCurUser tempDs = new DSCurUser();
                                tempDs.LOGINUSER.ReadXml(fnDeviceIdXml);
                                if (_dsCurUser == null) _dsCurUser = new DSCurUser();
                                else
                                {
                                    _dsCurUser.LOGINUSER.Clear();
                                }
                                _dsCurUser.LOGINUSER.Merge(tempDs.LOGINUSER);
                                _lastWriteDT = curDT;
                            }
                            catch (Exception ex)
                            {
                                LogMsg("LoadDSCurUser : Error : " + ex.Message);
                                //TestWriteCurUser();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogMsg("LoadDSCurUser:Err:" + ex.Message);
                    }
                }
            }
        }

        private void LogMsg(string msg)
        {
            //UtilLog.LogToGenericEventLog("DBCurUser", msg);
           try
           {
              MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("DBCurUser", msg);
           }
           catch (Exception)
           {
              UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DBCurUser", msg);
           }
        }

       

        private void TestWriteCurUser()
        {
            {
                //DSCurUser ds = new DSCurUser();
                //DSConfig.CONFIGRow row = ds.CONFIG.NewCONFIGRow();
                //row.ID = "BM_MAB";
                //row.ID_DESC = "MAB Alert Timing";
                //row.VAL = "30";
                //row.MODULE = "WEB";
                ////row.BM_AP_FST_BAG = "30";
                ////row.BM_AP_LST_BAG_HVY_LOAD = "60";
                ////row.BM_AP_LST_BAG_LGT_LOAD = "50";
                ////row.BM_BG_FST_BAG = "45";
                ////row.BM_BG_LST_BAG_HVY_LOAD = "70";
                ////row.BM_BG_LST_BAG_LGT_LOAD = "60";
                ////row.BM_MAB = "30";
                ////row.CONFIG_FOR = "WEB";
                ////row.WAP_JOBALERT_MINUTES = "30";
                ////row.WEB_FLINFO_FROM_MINUTES = "-720";
                ////row.WEB_FLINFO_TO_MINUTES = "120";
                //ds.CONFIG.Rows.Add(row);
                //ds.CONFIG.AcceptChanges();
                //ds.CONFIG.WriteXml(fnConfigXml);
            }
        }

        /// <summary>
        /// To get current login user(s)
        /// </summary>
        /// <param name="indAoBo">user type. "A" - AOs, "B" - BOs, "*" - All
        ///Logined users</param>
        /// <returns>DSCurUser</returns>
        public DSCurUser GetCurrentLoginedUsers(string indAoBo)
        {//indAoBo ==> "A" - AOs, "B" - BOs, "*" - All Logined users

            ///to do to read the information from DEVICEID.XML file.
            LogMsg("indAoBo" + indAoBo);

            DSCurUser ds = new DSCurUser();
            DSCurUser tempDs = dsCurUser;
            foreach (DSCurUser.LOGINUSERRow row in tempDs.LOGINUSER.Select())
            {
                if (indAoBo == "*")
                {
                    ds.LOGINUSER.LoadDataRow(row.ItemArray, true);
                }
                else if (row.ROLE.Substring(0, 1) == indAoBo)
                {
                    ds.LOGINUSER.LoadDataRow(row.ItemArray, true);
                }

            }
            return ds;


        }
    }
}
