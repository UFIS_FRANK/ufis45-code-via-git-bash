using System;
using System.Collections.Generic;
using System.Text;

namespace UFIS.ITrekLib.ENT
{
    public class EntConfig
    {
        private string _bmMAB="";
        private string _bmApFirstBag = "";
        private string _bmBgFirstBag = "";
        private string _bmApLastBagLightLoader = "";
        private string _bmBgLastBagLightLoader = "";
        private string _bmApLastBagHeavyLoader = "";
        private string _bmBgLastBagHeavyLoader = "";
        private string _webFlightInfoFromMinutes = "";
        private string _webFlightInfoToMinutes = "";
        private string _wapJobAlertMinutes = "";
    }
}
