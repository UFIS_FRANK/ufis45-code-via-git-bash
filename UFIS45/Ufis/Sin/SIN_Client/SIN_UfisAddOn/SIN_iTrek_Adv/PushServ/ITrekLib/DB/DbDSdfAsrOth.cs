using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Diagnostics;
using System.Collections;

using System.Data;
using System.IO;

using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.DSDB;
using iTrekXML;
using UFIS.ITrekLib.Util;
using iTrekWMQ;
using MM.UtilLib;
using UFIS.ITrekLib.Config;

namespace UFIS.ITrekLib.DB
{
   public class DbDSdfAsrOth
   {
      private static readonly object _lockObj = new object();
      private static DsDbAsr _dsDbAsr = null;
      private static DateTime _dtLastRefresh = UtilTime.DEFAULT_DATE_TIME;
      //private static DateTime _dtLastUpdDate = UtilTime.DEFAULT_DATE_TIME;//Last Update Date Time of Records for ITK_SFD_ASR_OTH Table

      private DbDSdfAsrOth() { }

      /// <summary>
      /// Get the Instance of DbDSdfAsrOth. There will be only one reference for all the clients
      /// </summary>
      /// <returns></returns>
      public static DbDSdfAsrOth GetInstance()
      {
         return Nested.instance;
      }

      class Nested
      {
         // Explicit static constructor to tell C# compiler
         // not to mark type as beforefieldinit
         static Nested() { }

         internal static readonly DbDSdfAsrOth instance = new DbDSdfAsrOth();
      }

      public void Init()
      {
         //_dsDbAsr = null;
         _dtLastRefresh = UtilTime.DEFAULT_DATE_TIME;
      }

      public bool NeedToRefreshDsDbAsr()
      {
         bool needToRefresh = false;
         if (_dsDbAsr == null) { needToRefresh = true; }
         else if (UtilTime.DiffInSec(DateTime.Now, _dtLastRefresh) > 60)//More than 60 seconds after last refresh
         {
            needToRefresh = true;
         }
         return needToRefresh;
      }

      /// <summary>
      /// Load/Refresh the Data and return the data.
      /// Note: Do not operate (update/insert/delete) directly onto this data.
      /// </summary>
      /// <param name="cmd"></param>
      /// <returns></returns>
      private DsDbAsr GetDsDbAsrWithRefresh(IDbCommand cmd)
      {
         if (NeedToRefreshDsDbAsr())
         {
            lock (_lockObj)
            {
               if (NeedToRefreshDsDbAsr())
               {
                  DsDbAsr temp = new DsDbAsr();
                  bool loadSuccess = false;
                  int cnt = DbLoadData_SdfAsrOth(cmd, temp, out loadSuccess);

                  if (loadSuccess)
                  {
                     _dsDbAsr = temp;
                     _dtLastRefresh = DateTime.Now;
                     PrepareMapping(_dsDbAsr);
                  }
                  else if (_dsDbAsr == null)
                  {
                     _dsDbAsr = temp;
                  }

                  if (_dsDbAsr == null) _dsDbAsr = new DsDbAsr();
               }
            }
         }
         return _dsDbAsr;
      }


      /// <summary>
      /// Get the DsDbAsr Copy with data in SdfAsrOth.
      /// </summary>
      /// <param name="cmd"></param>
      /// <returns></returns>
      public DsDbAsr GetDsDbAsr(IDbCommand cmd)
      {
         return (DsDbAsr) GetDsDbAsrWithRefresh(cmd).Copy();
      }

      /// <summary>
      /// Load the SdfAsrOth Data into given DsDbAsr
      /// </summary>
      /// <param name="cmd"></param>
      /// <param name="dsAsr"></param>
      public void LoadSdfAsrOth(IDbCommand cmd, DsDbAsr dsAsr)
      {
         DsDbAsr dsTemp = GetDsDbAsrWithRefresh(cmd);
         dsAsr.ITK_SDF_ASR_OTH.BeginLoadData();
         dsAsr.ITK_SDF_ASR_OTH.Clear();
         foreach (DataRow row in dsTemp.ITK_SDF_ASR_OTH.Rows)
         {
            dsAsr.ITK_SDF_ASR_OTH.LoadDataRow(row.ItemArray, true);
         }
         dsAsr.ITK_SDF_ASR_OTH.EndLoadData();

         dsAsr.ITK_SDF_ASR_OTH.AcceptChanges();
      }

      #region Database Operation for ITK_SDF_ASR_OTH
	   private static string GetDb_Tb_SdfAsrOth()
	   {
		   return DB_Info.GetDbFullName(DB_Info.DB_T_SDF_ASR_OTH);
	   }

      //private const string DB_T_SDF_ASR_OTH = "ITK_SDF_ASR_OTH";
      private const string PROC_ID_FOR_SDF_ASR_OTH = "ITK_SDF_ASR_OTH";

      private static int DbLoadData_SdfAsrOth(IDbCommand cmd, DsDbAsr dsAsr, out bool success)
      {
         int cnt = 0;
         success = false;
         bool cmdCreated = false;
         UtilDb udb = UtilDb.GetInstance();

         try
         {
            if (cmd == null)
            {
				cmdCreated = true;
               cmd = udb.GetCommand(false);
            }

			cmd.CommandText = "SELECT URNO, NAME, STAT, STDT, LSNO, OCLN, USEC, CDAT, USEU, LSTU FROM " + GetDb_Tb_SdfAsrOth();
            cmd.CommandType = CommandType.Text;
			cmd.Parameters.Clear();

            dsAsr.ITK_SDF_ASR_OTH.Clear();
            cnt = udb.FillDataTable(cmd, dsAsr.ITK_SDF_ASR_OTH);
            if (cnt > 0) success = true;
         }
         catch (Exception ex)
         {
            LogMsg("DbLoadData_SdfAsrOth:Err:" + ex.Message);
         }
         finally
         {
            if (cmdCreated)
            {
               udb.CloseCommand(cmd, false);
            }
         }

         return cnt;
      }

      public static int DbSaveData_SdfAsrOth(IDbCommand cmd, DsDbAsr dsAsr,
         string userId, DateTime updateDateTime)
      {
         DsDbAsr dsCurSdfAsrOth = new DsDbAsr();
         int newRowCnt = 0;
         bool loadSuccess = false;

         int cnt = DbLoadData_SdfAsrOth(cmd, dsCurSdfAsrOth, out loadSuccess);
         if (!loadSuccess)
         {
            LogMsg("DbSaveData_SdfAsrOth:Unable To Load the current data.");
            throw new ApplicationException("Fail to update.");
         }

         #region Update the information in the current table
         int colCnt = dsAsr.ITK_SDF_ASR_OTH.Columns.Count;
         foreach (DsDbAsr.ITK_SDF_ASR_OTHRow row in dsAsr.ITK_SDF_ASR_OTH.Rows)
         {
            if (string.IsNullOrEmpty(row.URNO))
            {
               row.URNO = UtilDb.PFIX_VIRTUAL_NEW_ID + newRowCnt;
               newRowCnt++;
            }
            DsDbAsr.ITK_SDF_ASR_OTHRow[] curRows = (DsDbAsr.ITK_SDF_ASR_OTHRow[])dsCurSdfAsrOth.ITK_SDF_ASR_OTH.Select("URNO='" + row.URNO + "'");
            if ((curRows == null) || (curRows.Length < 1))
            {//New Data
               dsCurSdfAsrOth.ITK_SDF_ASR_OTH.LoadDataRow(row.ItemArray, false);
            }
            else
            {//Existing Data
               if (curRows[0].STAT != row.STAT)
               {
                  row.STDT = updateDateTime;
               }
               UtilDataSet.AssignData(row, curRows[0], colCnt);
            }
         }
         #endregion

         DsDbAsr.ITK_SDF_ASR_OTHDataTable dtIns = (DsDbAsr.ITK_SDF_ASR_OTHDataTable)dsCurSdfAsrOth.ITK_SDF_ASR_OTH.GetChanges(DataRowState.Added);
         DsDbAsr.ITK_SDF_ASR_OTHDataTable dtUpd = (DsDbAsr.ITK_SDF_ASR_OTHDataTable)dsCurSdfAsrOth.ITK_SDF_ASR_OTH.GetChanges(DataRowState.Modified);

         if ((dtIns != null) && (dtIns.Rows.Count > 0))
         {
            DbInsertData_SdfAsrOth(cmd, dtIns, userId, updateDateTime);
         }

         if ((dtUpd != null) && (dtUpd.Rows.Count > 0))
         {
            int totRowCntToUpdate = 0;
            DbUpdateData_SdfAsrOth(cmd, dtUpd, userId, updateDateTime, out totRowCntToUpdate);
         }

         return newRowCnt;
      }

      private static int DbInsertData_SdfAsrOth(IDbCommand cmd,
         DsDbAsr.ITK_SDF_ASR_OTHDataTable dt,
         string userId, DateTime updateDateTime)
      {
		  return UtilDb.InsertDataToDb(cmd, dt, DB_Info.DB_T_SDF_ASR_OTH, PROC_ID_FOR_SDF_ASR_OTH, userId, updateDateTime);
      }

      private static int DbUpdateData_SdfAsrOth(IDbCommand cmd,
         DsDbAsr.ITK_SDF_ASR_OTHDataTable dt,
         string userId, DateTime updateDateTime, out int totRowCntToUpdate)
      {
		  return UtilDb.UpdateDataToDb(cmd, dt, DB_Info.DB_T_SDF_ASR_OTH, userId, updateDateTime, false, out totRowCntToUpdate);
      }

      #endregion

      #region Logging
      //[Conditional("LOGGING_ON")]
      private static void LogMsg(string msg)
      {
		  try
         {
            MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("DBDSdfAsrOth", msg);
         }
         catch (Exception)
         {
            UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DBDSdfAsrOth", msg);
         }
      }

      //[Conditional("TRACING_ON")]
      private static void LogTraceMsg(string msg)
      {
		  LogMsg(msg);
      }
      #endregion

      /// <summary>
      /// Get SdfOth Id for given column name related to DsApronServiceRpt.
      /// </summary>
      /// <param name="colName">Column Name to get the id</param>
      /// <param name="sdfOthId">SdfOth Id</param>
      /// <returns>True = Has Column</returns>
      public static bool GetIdForDsApronServiceReportCol(string colName, out string sdfOthId)
      {
         bool hasCol = false;
         sdfOthId = "";
         if (_htMappingForAsrOthColToId.ContainsKey(colName))
         {
            hasCol = true;
            sdfOthId = (string)_htMappingForAsrOthColToId[colName];
         }
         return hasCol;
      }

      private static Hashtable _htMappingForAsrOthColToId = new Hashtable();//DsApronService Column Name as Key, Oth Id in ITK_SDF_ASR_OTH Table as value
      private static Hashtable _htMappingForOthIdToAsrOthCol = new Hashtable();

      private static bool PrepareMapping(DsDbAsr ds)
      {
         bool hasErr = false;
         Hashtable htAsrToDb = new Hashtable();
		 Hashtable htDbToAsr = new Hashtable();
         foreach (DsDbAsr.ITK_SDF_ASR_OTHRow row in ds.ITK_SDF_ASR_OTH)
         {
            if (!string.IsNullOrEmpty(row.OCLN))
            {
               string colName = row.OCLN.Trim().ToUpper();
			   if (htAsrToDb.ContainsKey(colName))
               {
                  LogMsg("PrepareMappingForAsrOthColToId:Err:Duplicate OCLN Column:" + colName);
                  hasErr = true;
               }
               else
               {
				   htAsrToDb.Add(colName, row.URNO);
               }

               if (htDbToAsr.ContainsKey(row.URNO))
               {
                  LogMsg("PrepareMappingForAsrOthColToId:Err:Duplicate urno:" + row.URNO);
                  hasErr = true;
               }
               else
               {
				   htDbToAsr.Add(row.URNO, colName);
               }
            }
         }
		 if (!hasErr)
		 {
			 _htMappingForAsrOthColToId = htAsrToDb;
			 _htMappingForOthIdToAsrOthCol = htDbToAsr;
		 }
		 else
		 {
			 if (_htMappingForAsrOthColToId == null)
			 {
				 _htMappingForAsrOthColToId = htAsrToDb;
			 }

			 if (_htMappingForOthIdToAsrOthCol == null) _htMappingForOthIdToAsrOthCol = htDbToAsr;
		 }
         return hasErr;
      }

   }
}

