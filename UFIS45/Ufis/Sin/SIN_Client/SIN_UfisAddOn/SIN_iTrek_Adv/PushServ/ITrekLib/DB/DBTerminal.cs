//#define TESTING_ON

using System;
using System.Collections.Generic;
using System.Text;

using System.Diagnostics;
using System.IO;

using UFIS.ITrekLib.DS;
using iTrekXML;
using UFIS.ITrekLib.Util;
using MM.UtilLib;

namespace UFIS.ITrekLib.DB
{
    public class DBTerminal
    {
        private static Object _lockObj = new Object();//to use in synchronisation of single access
        private static DBTerminal _dbTerminal = null;

        private static readonly DateTime DEFAULT_DT = new System.DateTime(1, 1, 1);
        private static DateTime _lastTERMINALWriteDT = DEFAULT_DT;
        private static DateTime _lastRefreshDT = DEFAULT_DT;

        private static DSTerminal _dsTerminal = null;
        private static string _fnTerminalXml = null;//file name of TERMINAL XML in full path.

        /// <summary>
        /// Singleton Pattern.
        /// Not allowed to create the object from other class.
        /// To get the object, user "GetInstance()" method.
        /// </summary>
        private DBTerminal() { }

        public void Init()
        {//Initialize all the information, to get the latest data when fetching them again.
            _dsTerminal = null;
            _fnTerminalXml = null;
            _lastTERMINALWriteDT = DEFAULT_DT;
            _lastRefreshDT = DEFAULT_DT;
        }

        /// <summary>
        /// Get the Instance of DBTerminal. There will be only one reference for all the clients
        /// </summary>
        /// <returns></returns>
        public static DBTerminal GetInstance()
        {
            if (_dbTerminal == null)
            {
                _dbTerminal = new DBTerminal();
            }
            return _dbTerminal;
        }

        public DSTerminal RetrieveTerminals()
        {
            DSTerminal ds = null;
            ds = dsTerminal;
            return ds;
        }

        public void RetrieveTerminals(DSBelt.TMNDataTable dt)
        {
            if (dt == null) dt = new DSBelt.TMNDataTable();
            dt.Clear();
            DSTerminal ds = dsTerminal;
            foreach (DSTerminal.TERMINALRow row in ds.TERMINAL.Rows)
            {
                DSBelt.TMNRow beltTmnRow = dt.NewTMNRow();
                dt.AddTMNRow(row.TERM_ID, row.TERM_NAME);
            }
        }

        private string fnTerminalXml
        {
            get
            {
                if ((_fnTerminalXml == null) || (_fnTerminalXml == ""))
                {
                    iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
                    _fnTerminalXml = path.GetTerminalFilePath();
                }

                return _fnTerminalXml;
            }
        }

        private DSTerminal dsTerminal
        {
            get
            {
                if (Math.Abs(UtilTime.DiffInSec(DateTime.Now, _lastRefreshDT)) > 600)
                {
                    LoadDSTerminal();
                    _lastRefreshDT = DateTime.Now;
                }
                DSTerminal ds = (DSTerminal)_dsTerminal.Copy();

                return ds;
            }
        }

        private void LoadDSTerminal()
        {
            FileInfo fiXml = new FileInfo(fnTerminalXml);
            DateTime curDT = fiXml.LastWriteTime;
            if (!fiXml.Exists) throw new ApplicationException("TERMINAL information missing. Please inform to administrator.");

            if ((_dsTerminal == null) || (curDT.CompareTo(_lastTERMINALWriteDT) != 0))
            {
                lock (_lockObj)
                {
                    try
                    {
                        curDT = fiXml.LastWriteTime;//check the time again, in case of lock
                        if ((_dsTerminal == null) || (curDT.CompareTo(_lastTERMINALWriteDT) != 0))
                        {
                            try
                            {
                                DSTerminal tempDs = new DSTerminal();
                                tempDs.TERMINAL.ReadXml(fnTerminalXml);
                                if (_dsTerminal == null)
                                {
                                    _dsTerminal = new DSTerminal();
                                }
                                else
                                {
                                    _dsTerminal.TERMINAL.Clear();
                                }
                                _dsTerminal.TERMINAL.Merge(tempDs.TERMINAL);
                                _lastTERMINALWriteDT = curDT;
                            }
                            catch (Exception ex)
                            {
                                LogMsg("LoadDSTerminal : Error : " + ex.Message);
                                //TestWriteTERMINAL();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogMsg("LoadDSTerminal:Err:" + ex.Message);
                    }
                }
            }
        }

        private void LogMsg(string msg)
        {
            //UtilLog.LogToGenericEventLog("DBTerminal", msg);
           try
           {
              MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("DBTerminal", msg);
           }
           catch (Exception)
           {
              UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DBTerminal", msg);
           }
        }

        //[Conditional("TESTING_ON")]
        //private void TestWriteTERMINAL()
        //{
        //    {
        //        //DSTerminal ds = new DSTerminal();
        //        //DSTerminal.TERMINALRow row = ds.TERMINAL.NewTERMINALRow();
        //        //row.TERM_ID = "1";
        //        //row.TERM_NAME = "Terminal 1";
        //        //ds.TERMINAL.Rows.Add(row);
        //        //ds.TERMINAL.AcceptChanges();
        //        //ds.TERMINAL.WriteXml(fnTerminalXml);
        //    }
        //}


    }
}
