using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

using System.Data;
using System.IO;
using System.Collections;

using UFIS.ITrekLib.DS;
using iTrekXML;
using UFIS.ITrekLib.Util;
using UFIS.ITrekLib.ENT;
using MM.UtilLib;

namespace UFIS.ITrekLib.DB
{
    public class DBTrip
    {
        private static Object _lockObj = new Object();//to use in synchronisation of single access
        private static Object _lockObjHist = new Object();//to use in synchronisation of single access
        private static DBTrip _dbTrip = null;
        private static readonly DateTime DEFAULT_DT = new System.DateTime(1, 1, 1);
        private static DateTime lastWriteDT = DEFAULT_DT;
        private static DateTime lastHistWriteDT = DEFAULT_DT;

        private static DSTrip _dsTrip = null;
        private static string _fnTripXml = null;//file name of TRIP XML in full path.
        private static DSTrip _dsTripHist = null;
        private static string _fnTripHistXml = null;//file name of TRIP XML in full path.

        private static DateTime _lastRefreshDT = DEFAULT_DT;
        private static DateTime _lastHistRefreshDT = DEFAULT_DT;

        /// <summary>
        /// Singleton Pattern.
        /// Not allowed to create the object from other class.
        /// To get the object, user "GetInstance()" method.
        /// </summary>
        private DBTrip() { }

        public void Init()
        {//Initialize all the information, to get the latest data when fetching them again.
            _dsTrip = null;
            _fnTripXml = null;
            lastWriteDT = DEFAULT_DT;
            _dsTripHist = null;
            _fnTripHistXml = null;
            lastHistWriteDT = DEFAULT_DT;
            _lastRefreshDT = DEFAULT_DT;
            _lastHistRefreshDT = DEFAULT_DT;
        }

        /// <summary>
        /// Get the Instance of DBTrip. There will be only one reference for all the clients
        /// </summary>
        /// <returns></returns>
        public static DBTrip GetInstance()
        {
            if (_dbTrip == null)
            {
                _dbTrip = new DBTrip();
            }
            return _dbTrip;
        }

        public DSTrip RetrieveTripForAFlight(string flightUrNo)
        {
            DSTrip ds = new DSTrip();
            DSTrip tempDs = dsTrip;
            bool found = false;

            foreach (DSTrip.TRIPRow row in tempDs.TRIP.Select("UAFT='" + flightUrNo + "'"))
            {
                ds.TRIP.LoadDataRow(row.ItemArray, true);
                found = true;
            }

            if (!found)
            {
                tempDs = dsTripHist;
                foreach (DSTrip.TRIPRow row in tempDs.TRIP.Select("UAFT='" + flightUrNo + "'"))
                {
                    ds.TRIP.LoadDataRow(row.ItemArray, true);
                    found = true;
                }
            }

            return ds;
        }

        /// <summary>
        /// Retrieve Trip for a flight with given criteria
        /// </summary>
        /// <param name="flightId">Flight Id</param>
        /// <param name="bagArrRecvCat">Baggage Arrival Receive Category</param>
        /// <param name="bagArrType">Bagage Arrival Destination Type</param>
        /// <returns>Dataset of the result trip</returns>
        public DSTrip RetrieveTripForAFlight(string flightId,
            EnBagArrRecvCategory bagArrRecvCat, EnBagArrType bagArrType)
        {//AM 20080829
            DSTrip ds = new DSTrip();
            DSTrip tempDs = dsTrip;
            bool found = false;
            string stSel = "(UAFT='" + flightId + "')";

            switch (bagArrRecvCat)
            {
                case EnBagArrRecvCategory.Recv:
                    stSel += " AND ((RCV_DT IS NOT NULL) OR (RCV_DT<>''))";
                    break;
                case EnBagArrRecvCategory.UnReceive:
                    stSel += " AND ((RCV_DT IS NULL) OR (RCV_DT=''))";
                    break;
                case EnBagArrRecvCategory.All:
                    //No filter required.
                    break;
                case EnBagArrRecvCategory.None:
                    throw new ApplicationException("Nothing to select. Choose either Received or UnReceived, or All.");
                    //break;
                default:
                    throw new ApplicationException("Nothing to select. Choose either Received or UnReceived, or All.");
                    //break;
            }

            switch (bagArrType)
            {
                case EnBagArrType.Inter:
                    stSel += " AND (DES='I')";//Inter
                    break;
                case EnBagArrType.Local:
                    stSel += " AND (DES='L')";//LOCAL
                    break;
                case EnBagArrType.All:
                    //No filter required.
                    break;
                case EnBagArrType.None:
                    throw new ApplicationException("Nothing to select. Choose either Inter or Local, or all.");
                    //break;
                default:
                    throw new ApplicationException("Nothing to select. Choose either Inter or Local, or all.");
                    //break;
            }

            foreach (DSTrip.TRIPRow row in tempDs.TRIP.Select(stSel))
            {
                ds.TRIP.LoadDataRow(row.ItemArray, true);
                found = true;
            }

            if (!found)
            {
                tempDs = dsTripHist;
                foreach (DSTrip.TRIPRow row in tempDs.TRIP.Select(stSel))
                {
                    ds.TRIP.LoadDataRow(row.ItemArray, true);
                    found = true;
                }
            }

            return ds;
        }

        public DSTrip RetrieveTripForAFlightWithMaxCnt(string flightUrNo)
        {
            DSTrip ds = new DSTrip();
            int maxCnt = 20;
            DSTrip tempDs = RetrieveTripForAFlight(flightUrNo);
            foreach (DSTrip.TRIPRow row in tempDs.TRIP.Select("UAFT='" + flightUrNo + "'AND RCV_DT=''", "SENT_DT ASC"))
            {
                ds.TRIP.LoadDataRow(row.ItemArray, true);
                maxCnt--;
                if (maxCnt <= 0) break;
            }

            return ds;
        }

        public DSTrip RetrieveTripForAFlightNotReceived(string flightUrNo)
        {
            DSTrip ds = new DSTrip();
            DSTrip tempDs = RetrieveTripForAFlight(flightUrNo);
            foreach (DSTrip.TRIPRow row in tempDs.TRIP.Select("UAFT='" + flightUrNo + "' AND RCV_DT=''"))
            {
                ds.TRIP.LoadDataRow(row.ItemArray, true);
            }

            return ds;
        }

        public DSTrip RetrieveTripForAFlight(string flightUrNo, string uldn)
        {
            DSTrip ds = new DSTrip();
            DSTrip tempDs = RetrieveTripForAFlight(flightUrNo);
            foreach (DSTrip.TRIPRow row in tempDs.TRIP.Select("UAFT='" + flightUrNo + "'AND ULDN='" + uldn + "'"))
            {
                ds.TRIP.LoadDataRow(row.ItemArray, true);
            }

            return ds;
        }

        public DSTrip RetrieveTripForAFlightWithTripUrno(string flightUrNo, string urno)
        {
            DSTrip ds = new DSTrip();
            DSTrip tempDs = RetrieveTripForAFlight(flightUrNo);
            foreach (DSTrip.TRIPRow row in tempDs.TRIP.Select("UAFT='" + flightUrNo + "'AND URNO='" + urno + "'"))
            {
                ds.TRIP.LoadDataRow(row.ItemArray, true);
            }

            return ds;
        }

        private string fnTripXml
        {
            get
            {
                if ((_fnTripXml == null) || (_fnTripXml == ""))
                {
                    iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
                    _fnTripXml = path.GetTripFilePath();
                }

                return _fnTripXml;
            }
        }

        private string fnTripHistXml
        {
            get
            {
                if ((_fnTripHistXml == null) || (_fnTripHistXml == ""))
                {
                    iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
                    _fnTripHistXml = path.GetTripHistFilePath();
                }

                return _fnTripHistXml;
            }
        }

        private DSTrip dsTrip
        {
            get
            {
                int timeElapse = Math.Abs(UtilTime.DiffInSec(DateTime.Now, _lastRefreshDT));
                if ((_dsTrip == null) || (timeElapse > 3))
                {
                    LoadDSTrip();
                    _lastRefreshDT = DateTime.Now;
                    LogTraceMsg("Trip Refresh:" + _lastRefreshDT.ToLongTimeString());
                }
                
                return (DSTrip)(_dsTrip.Copy());
            }
        }

        private DSTrip dsTripHist
        {
            get
            {
                int timeElapse = Math.Abs(UtilTime.DiffInSec(DateTime.Now, _lastHistRefreshDT));
                if ((_dsTripHist == null) || (timeElapse > 30))
                {
                    LoadDSTripHist();
                    _lastHistRefreshDT = DateTime.Now;
                    LogTraceMsg("Trip Refresh:" + _lastHistRefreshDT.ToLongTimeString());
                }

                return (DSTrip)(_dsTripHist.Copy());
            }
        }

        private void LoadDSTrip()
        {
            FileInfo fiXml = new FileInfo(fnTripXml);
            DateTime curDT = fiXml.LastWriteTime;

            if ((_dsTrip == null) || (curDT.CompareTo(lastWriteDT) != 0))
            {
                lock (_lockObj)
                {
                    try
                    {
                        fiXml = new FileInfo(fnTripXml);
                        curDT = fiXml.LastWriteTime;//check the time again, in case of lock
                        if ((_dsTrip == null) || (curDT.CompareTo(lastWriteDT) != 0))
                        {
                            try
                            {
                                DSTrip tempDs = new DSTrip();
                                tempDs.ReadXml(fnTripXml);
                                _dsTrip = tempDs;
                                lastWriteDT = curDT;
                            }
                            catch (Exception ex)
                            {
                                LogMsg("LoadDSTrip : Error : " + ex.Message);
                                //TestWriteTRIP();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogMsg("LoadDSTrip:Err:" + ex.Message);
                    }
                }
            }
        }

        private void LoadDSTripHist()
        {
            FileInfo fiXml = new FileInfo(fnTripHistXml);
            DateTime curDT = fiXml.LastWriteTime;

            if ((_dsTripHist == null) || (curDT.CompareTo(lastHistWriteDT) != 0))
            {
                lock (_lockObjHist)
                {
                    try
                    {
                        fiXml = new FileInfo(fnTripHistXml);
                        curDT = fiXml.LastWriteTime;//check the time again, in case of lock
                        if ((_dsTripHist == null) || (curDT.CompareTo(lastHistWriteDT) != 0))
                        {
                            try
                            {
                                DSTrip tempDs = new DSTrip();
                                tempDs.ReadXml(fnTripHistXml);
                                _dsTripHist = tempDs;
                                lastHistWriteDT = curDT;
                            }
                            catch (Exception ex)
                            {
                                LogMsg("LoadDSTrip : Error : " + ex.Message);
                                //TestWriteTRIP();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogMsg("LoadDSTripHist:Err:" + ex.Message);
                    }
                }
            }
        }

        public void UpdateTripsInfoFromBackendToTripXmlFile(XmlDocument xmlDoc)
        {
            lock (_lockObj)
            {
                try
                {
                    LockTripForWriting();
                    FileInfo fi = new FileInfo(fnTripXml);
                    if (!fi.Exists)
                    {
                        DSTrip temp = new DSTrip();
                        temp.WriteXml(fnTripXml);
                    }

                    DSTrip dsOldTrip = new DSTrip();
                    dsOldTrip.ReadXml(fnTripXml);

                    DataSet dsNewTrip = new DataSet();
                    XmlNodeReader reader = new XmlNodeReader(xmlDoc);
                    dsNewTrip.ReadXml(reader);

                    int cnt = dsNewTrip.Tables["TRIP"].Rows.Count;
                    for (int i = 0; i < cnt; i++)
                    {
                        DataRow newRow = dsNewTrip.Tables["TRIP"].Rows[i];
                        string urno = newRow["URNO"].ToString();
                        string flightUrno = newRow["UAFT"].ToString();
                        string uldNo = newRow["ULDN"].ToString();
                        DSTrip.TRIPRow[] tempRowArr = (DSTrip.TRIPRow[])dsOldTrip.TRIP.Select("URNO='" + urno + "'");
                        if (tempRowArr.Length > 0)
                        {//Existing record - to update
                            tempRowArr[0].Delete();

                        }
                        else
                        {//New Record - Insert

                        }
                        DSTrip.TRIPRow updRow = dsOldTrip.TRIP.NewTRIPRow();
                        updRow.DES = newRow["DES"].ToString();
                        updRow.ISCPM = newRow["ISCPM"].ToString();
                        updRow.RCV_BY = newRow["RCV_BY"].ToString();
                        updRow.RCV_DT = newRow["RCV_DT"].ToString();
                        updRow.RCV_RMK = newRow["RCV_RMK"].ToString();
                        updRow.RTRPNO = newRow["RTRPNO"].ToString();
                        updRow.SENT_BY = newRow["SEND_BY"].ToString();
                        updRow.SENT_DT = newRow["SEND_DT"].ToString();
                        updRow.SENT_RMK = newRow["SEND_RMK"].ToString();
                        updRow.STRPNO = newRow["STRPNO"].ToString();
                        updRow.UAFT = newRow["UAFT"].ToString();
                        updRow.ULDN = newRow["ULDN"].ToString();
                        updRow.URNO = newRow["URNO"].ToString();
                        dsOldTrip.TRIP.AddTRIPRow(updRow);
                        //dsOldTrip.TRIP.AddTRIPRow( (DSTrip.TRIPRow) newRow );
                    }
                    dsOldTrip.TRIP.AcceptChanges();
                    dsOldTrip.WriteXml(fnTripXml);
                }
                catch (Exception ex)
                {
                    LogMsg("DBTrip:Err:" + ex.Message);
                }
                finally
                {
                    ReleaseTripAfterWrite();
                }
            }
        }

        public void UpdateTripsReceivedToTripXmlFile(ArrayList cpmList, string datetime, string sentBy, string stTripType)
        {
            lock (_lockObj)
            {
                try
                {
                    LockTripForWriting();
                    DSTrip ds = null;
                    int updCnt = 0;
                    try
                    {
                        ds = new DSTrip();
                        //LogTraceMsg("TripPath:" + fnTripXml);
                        ds.ReadXml(fnTripXml);
                        if (ds == null)
                        {
                            throw new ApplicationException("Unable to get the Trip Information.");
                        }
                    }
                    catch (ApplicationException ex) { throw ex; }
                    catch (Exception ex)
                    {
                        throw new ApplicationException("Unable to get the Trip Information due to " + ex.Message);
                    }

                    foreach (string urno in cpmList)
                    {
                        DSTrip.TRIPRow[] tempRowArr = (DSTrip.TRIPRow[])ds.TRIP.Select("URNO='" + urno + "'");
                        if (tempRowArr.Length > 0)
                        {//Existing record - to updat
                            tempRowArr[0].RCV_DT = datetime;
                            tempRowArr[0].RCV_BY = sentBy;
                            tempRowArr[0].RTTY = stTripType;
                            updCnt++;
                        }
                    }
                    if (updCnt > 0)
                    {
                        ds.TRIP.AcceptChanges();
                        ds.WriteXml(fnTripXml);
                        _dsTrip = ds;
                    }
                }
                catch (Exception ex)
                {
                    LogMsg("UpdateTripsInfoFromBackendToTripXmlFile:Err:" + ex.Message);
                }
                finally
                {
                    ReleaseTripAfterWrite();
                }
            }
        }

        public void UpdateTripsFieldsToTripXmlFile(XmlDocument tripFieldXml)
        {
            lock (_lockObj)
            {
                try
                {
                    LockTripForWriting();

                    string urno = "";
                    string tripField = "";
                    string tripFieldValue = "";
                    string urnoValue = "";
                    DSTrip ds = null;
                    int updCnt = 0;
                    try
                    {
                        ds = new DSTrip();
                        ds.ReadXml(fnTripXml);
                        if (ds == null)
                        {
                            throw new ApplicationException("Unable to get the Trip Information.");
                        }
                    }
                    catch (Exception)
                    {
                        throw new ApplicationException("Unable to get the Trip Information.");
                    }

                    try
                    {
                        XmlElement newTripXMLElement = tripFieldXml.DocumentElement;
                        tripField = newTripXMLElement.FirstChild.Name.Trim();
                        // LogMsg("tripField" + tripField);
                        tripFieldValue = newTripXMLElement.FirstChild.InnerXml.Trim();
                        // LogMsg("tripFieldValue" + tripFieldValue);
                        int count = tripFieldXml.DocumentElement.ChildNodes.Count;
                        // LogMsg("count" + count);

                        foreach (XmlNode node in newTripXMLElement.ChildNodes)
                        {
                            urno = node.Name.Trim();
                            // LogMsg("urno" + urno);
                            if (urno == "URNO")
                            {
                                urnoValue = node.InnerXml.Trim();
                                //   LogMsg("urnoValue" + urnoValue);
                                DSTrip.TRIPRow[] tempRowArr = (DSTrip.TRIPRow[])ds.TRIP.Select("URNO='" + urnoValue + "'");
                                if (tempRowArr.Length > 0)
                                {//Existing record - to updat
                                    if (tripField == "RTTY")
                                    {
                                        tempRowArr[0].RTTY = tripFieldValue;
                                        //   LogMsg("RTTY" + tripFieldValue);

                                        updCnt++;
                                    }
                                    else
                                        if (tripField == "STTY")
                                        {
                                            tempRowArr[0].STTY = tripFieldValue;
                                            //        LogMsg("STTY" + tripFieldValue);
                                            updCnt++;
                                        }
                                }
                            }

                        }
                    }
                    catch (Exception ex1)
                    {
                        LogMsg("UpdateTripsFieldsToTripXmlFile:Err:" + ex1.Message);
                    }

                    if (updCnt > 0)
                    {
                        ds.TRIP.AcceptChanges();
                        ds.WriteXml(fnTripXml);
                        _dsTrip = ds;
                    }
                }
                catch (Exception ex)
                {
                    LogMsg("UpdateTripsInfoFromBackendToTripXmlFile:Err:" + ex.Message);
                }
                finally
                {
                    ReleaseTripAfterWrite();
                }
            }
        }

        private void LogMsg(string msg)
        {
            //UtilLog.LogToGenericEventLog("DBTrip", msg);
           try
           {
              MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("DBTrip", msg);
           }
           catch (Exception)
           {
              UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DBTrip", msg);
           }
        }

        private static void LogTraceMsg(string msg)
        {
            //UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DBTrip", msg);
           try
           {
              MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("DBTrip", msg);
           }
           catch (Exception)
           {
              UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DBTrip", msg);
           }
        }

        private void TestWriteTRIP()
        {
            //{
            //    DSTrip ds = new DSTrip();
            //    DSTrip.TRIPRow row = ds.TRIP.NewTRIPRow();
            //    row.URNO = "TRIPUrno123";
            //    row.UAFT = "UAFT1324";
            //    row.CONTAINERNO = "Container No. 1";
            //    row.DES = "BKK";
            //    row.SENT_DT = DateTime.Now.AddMinutes(-20);
            //    row.RCV_DT = DateTime.Now.AddMinutes(-10);
            //    row.RMKS = "Remark";
            //    ds.TRIP.Rows.Add(row);
            //    ds.TRIP.AcceptChanges();
            //    ds.TRIP.WriteXml(fnTripXml);
            //}
        }

        public void DeleteOldTrips(ArrayList alFlightUrno)
        {
            lock (_lockObj)
            {
                try
                {
                    LockTripForWriting();
                    DSTrip ds = null;
                    try
                    {
                        ds = new DSTrip();
                        ds.ReadXml(fnTripXml);
                        if (ds == null)
                        {
                            throw new ApplicationException("Unable to get the Trip Information.");
                        }
                    }
                    catch (Exception)
                    {
                        throw new ApplicationException("Unable to get the Trip Information.");
                    }
                    foreach (string urno in alFlightUrno)
                    {
                        foreach (DSTrip.TRIPRow row in ds.TRIP.Select("UAFT='" + urno + "'"))
                        {
                            ds.TRIP.RemoveTRIPRow(row);
                        }
                        ds.AcceptChanges();
                    }
                    ds.WriteXml(fnTripXml);
                    _dsTrip = ds;
                }
                catch (Exception ex)
                {
                    LogMsg("DeleteOldTrips:Err:" + ex.Message);
                }
                finally
                {
                    ReleaseTripAfterWrite();
                }
            }
        }

        public void UpdateTripTiming(string flightURNO, EnumTripType tripType, string tripTimeInUfisFormat)
        {
            if ((tripTimeInUfisFormat == null) || (tripTimeInUfisFormat.Trim() == "")) return;
            lock (_lockObj)
            {
                try
                {
                    LockTripForWriting();
                    DSTrip ds = null;
                    try
                    {
                        ds = new DSTrip();
                        ds.ReadXml(fnTripXml);
                        if (ds == null)
                        {
                            throw new ApplicationException("Unable to get the Trip Information.");
                        }
                    }
                    catch (Exception)
                    {
                        throw new ApplicationException("Unable to get the Trip Information.");
                    }

                    string stFilter = null;
                    string colName = null;

                    switch (tripType)
                    {
                        case EnumTripType.ArrAprFirstTrip:
                            stFilter = "UAFT='" + flightURNO + "' AND STTY=" + FIRST_TRIP_IND;
                            colName = TRIP_COL_NAME_SENT;
                            break;
                        case EnumTripType.ArrAprLastTrip:
                            stFilter = "UAFT='" + flightURNO + "' AND STTY=" + LAST_TRIP_IND;
                            colName = TRIP_COL_NAME_SENT;
                            break;
                        case EnumTripType.ArrBaggFirstTrip:
                            stFilter = "UAFT='" + flightURNO + "' AND RTTY=" + FIRST_TRIP_IND;
                            colName = TRIP_COL_NAME_RCV;
                            break;
                        case EnumTripType.ArrBaggLastTrip:
                            stFilter = "UAFT='" + flightURNO + "' AND RTTY=" + LAST_TRIP_IND;
                            colName = TRIP_COL_NAME_RCV;
                            break;

                        case EnumTripType.DepAprFirstTrip:
                            stFilter = "UAFT='" + flightURNO + "' AND RTTY=" + FIRST_TRIP_IND;
                            colName = TRIP_COL_NAME_RCV;
                            break;
                        case EnumTripType.DepAprLastTrip:
                            stFilter = "UAFT='" + flightURNO + "' AND RTTY=" + LAST_TRIP_IND;
                            colName = TRIP_COL_NAME_RCV;
                            break;
                        case EnumTripType.DepBaggFirstTrip:
                            stFilter = "UAFT='" + flightURNO + "' AND STTY=" + FIRST_TRIP_IND;
                            colName = TRIP_COL_NAME_SENT;
                            break;
                        case EnumTripType.DepBaggLastTrip:
                            stFilter = "UAFT='" + flightURNO + "' AND STTY=" + LAST_TRIP_IND;
                            colName = TRIP_COL_NAME_SENT;
                            break;
                        default:
                            break;
                    }

                    foreach (DSTrip.TRIPRow row in ds.TRIP.Select(stFilter))
                    {
                        row[colName] = tripTimeInUfisFormat;
                    }
                    ds.TRIP.AcceptChanges();
                    ds.WriteXml(fnTripXml);
                    _dsTrip = ds;
                }
                catch (Exception ex)
                {
                    LogMsg("UpdateTripTiming:error:" + ex.Message);
                }
                finally
                {
                    ReleaseTripAfterWrite();
                }
            }
        }

        public void UpdateTripTiming(string flightURNO, string arrDepInd, Hashtable ht)
        {
            if ((ht == null) || (ht.Count<1)) throw new ApplicationException("No data to update.");
            lock (_lockObj)
            {
                try
                {
                    LockTripForWriting();
                    DSTrip ds = null;
                    try
                    {
                        ds = new DSTrip();
                        //LogTraceMsg("TripPath:" + fnTripXml);
                        ds.ReadXml(fnTripXml);
                        if (ds == null)
                        {
                            throw new ApplicationException("Unable to get the Trip Information.");
                        }
                    }
                    catch (ApplicationException ex) { throw ex; }
                    catch (Exception ex)
                    {
                        throw new ApplicationException("Unable to get the Trip Information due to " + ex.Message);
                    }

                    string stFilter = "";
                    string stArrDepInd = arrDepInd.Trim().ToUpper();
                    string tripColName = "";
                    int updCnt = 0;
                    foreach (string colName in ht.Keys)
                    {
                        if ((colName == null) || (colName == "")) continue;
                        string time = ht[colName].ToString().Trim();
                        if (time == "") continue;
                        tripColName = "";

                        switch (colName.Trim().ToUpper())
                        {
                            case FL_COL_NAME_FTRIP_SENT :
                                tripColName = TRIP_COL_NAME_SENT;
                                stFilter = "UAFT='" + flightURNO + "' AND STTY=" + FIRST_TRIP_IND;
                                break;
                            case FL_COL_NAME_FTRIP_RCV :
                                tripColName = TRIP_COL_NAME_RCV;
                                stFilter = "UAFT='" + flightURNO + "' AND RTTY=" + FIRST_TRIP_IND;
                                break;
                            case FL_COL_NAME_LTRIP_SENT:
                                tripColName = TRIP_COL_NAME_SENT;
                                stFilter = "UAFT='" + flightURNO + "' AND STTY=" + LAST_TRIP_IND;
                                break;
                            case FL_COL_NAME_LTRIP_RCV:
                                tripColName = TRIP_COL_NAME_RCV;
                                stFilter = "UAFT='" + flightURNO + "' AND RTTY=" + LAST_TRIP_IND;
                                break;

                        }

                        if ((tripColName != "") && (stFilter != ""))
                        {
                            updCnt++;
                            foreach (DSTrip.TRIPRow row in ds.TRIP.Select(stFilter))
                            {
                                row[tripColName] = time;

                                LogTraceMsg("UpdateTripTiming:ColName<" + colName + ">, tripCol<" + tripColName + ">,filter<" + stFilter + ">," +
                                    row.URNO + "," + row.UAFT + "," + row.ULDN + "," +
                                    row.STTY + "," + row.RTTY + "," +
                                    row.SENT_DT + "," + row.RCV_DT);
                            }
                        }
                    }//end of For loop - processing each Timing in Flight
                    if (updCnt > 0)
                    {
                        ds.TRIP.AcceptChanges();
                        ds.WriteXml(fnTripXml);
                        _dsTrip = ds;
                    }
                }
                catch (Exception ex)
                {
                    LogTraceMsg("UpdateTripTiming:err:" + ex.Message);
                    LogMsg("UpdateTripTiming:err:" + ex.Message);
                }
                finally
                {
                    ReleaseTripAfterWrite();
                }
            }
        }

        //private int DoArrUpdateTimingInDs(DSTrip ds, string flightTimingColName, string time)
        //{
        //    int updCnt = 0;

        //    return updCnt;
        //}

        private void LockTripForWriting()
        {
            try
            {
                UtilLock.GetInstance().LockTrip();
            }
            catch (Exception ex)
            {
                LogMsg("LockTripForWriting:Err:" + ex.Message);
                if (ex.InnerException != null)
                {
                    LogMsg("LockTripForWriting:err:Inner:" + ex.InnerException.Message);
                }
            }
        }

        private void ReleaseTripAfterWrite()
        {
            try
            {
                UtilLock.GetInstance().ReleaseTrip();
            }
            catch (Exception ex)
            {
                LogMsg("ReleaseTripAfterWrite:Err:" + ex.Message);
                if (ex.InnerException != null)
                {
                    LogMsg("ReleaseTripAfterWrite:err:Inner:" + ex.InnerException.Message);
                }
            }
        }

        const string FIRST_TRIP_IND = "'F'";
        const string LAST_TRIP_IND = "'L'";
        const string TRIP_COL_NAME_SENT = "SENT_DT";
        const string TRIP_COL_NAME_RCV = "RCV_DT";

        const string FL_COL_NAME_FTRIP_SENT = "FTRIP";
        const string FL_COL_NAME_FTRIP_RCV = "FTRIP-B";
        const string FL_COL_NAME_LTRIP_SENT = "LTRIP";
        const string FL_COL_NAME_LTRIP_RCV = "LTRIP-B";
    }

    public enum EnumTripType { ArrAprFirstTrip, ArrAprLastTrip, ArrBaggFirstTrip, ArrBaggLastTrip,
        DepAprFirstTrip, DepAprLastTrip, DepBaggFirstTrip, DepBaggLastTrip };


}
