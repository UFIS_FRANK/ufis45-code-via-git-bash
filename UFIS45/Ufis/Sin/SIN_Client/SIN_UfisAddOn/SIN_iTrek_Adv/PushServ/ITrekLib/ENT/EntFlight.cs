using System;
using System.Collections.Generic;
using System.Text;

namespace UFIS.ITrekLib.ENT
{
    public class EntFlight
    {
        private EntFlightTiming _entFlightTiming = null;
        private string _flightUrno = null; //Flight Unique Record No. (Primary Key)
        private string _flightNo = null; //Flight N.
        private string _arrDep = null; //'A' - Arrival, 'D' - Departure
        private string _loaderType = null; //'L' - Light Loader, 'H' - Heavy Loader
        private string _acftType = null; //Aircraft Type
        private string _terminal = null; //Airport Terminal
        private string _acftRegn = null; //Aircraft Registration.
        private string _from = null; //Flight From
        private string _to = null; //Fly to
        private string _parkingStand = null; //Parking Stand
        private string _gate = null;//Gate
        private string _flightTime = null;

        public string bay
        {
            get
            {
                string st = parkingStand;
                if (st == "") st = gate;
                return st;
            }
        }

        public string parkingStand
        {
            get
            {
                if (_parkingStand == null) _parkingStand = "";
                return _parkingStand;
            }
            set
            {
                if (value == null) value = "";
                _parkingStand = value;
            }
        }

        public string gate
        {
            get
            {
                if (_gate == null) _gate = "";
                return _gate;
            }
            set
            {
                if (value == null) value = "";
                _gate = value;
            }
        }

        public string AO
        {
            get
            {
                string st = "";
                return st;
            }

        }

        public DateTime flightTime
        {
            get
            {
                //string st = "";
                //DateTime dt = DateTime.Now;

                return DateTime.Now;
            }

        }


    }
}
