using System;
using System.Collections.Generic;
using System.Text;

using UFIS.ITrekLib.DB;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.Util;

namespace UFIS.ITrekLib.Ctrl
{
    public class CtrlCpmPriority
    {
        private CtrlCpmPriority() { }

        private static void CheckData(ref string airlineCode, ref int priority, ref string baggageTypes)
        {
            if (string.IsNullOrEmpty(airlineCode)) throw new ApplicationException("No Airline Code.");
            if (string.IsNullOrEmpty(baggageTypes)) throw new ApplicationException("No Baggage Type.");
            if (priority < 1) throw new ApplicationException("Invalid Priority");
            airlineCode = airlineCode.Trim().ToUpper();
            baggageTypes = baggageTypes.Trim().ToUpper();
            if (airlineCode == "") throw new ApplicationException("No Airline Code.");
            if (baggageTypes == "") throw new ApplicationException("No baggage type.");
        }

        public static void Create(string airlineCode, int priority, string baggageTypes)
        {
            CheckData(ref airlineCode, ref priority, ref baggageTypes);

            if (IsExist(airlineCode, priority))
            {
                throw new ApplicationException("Records already existed.");
            }
            DBCpmPriority.GetInstance().Create(airlineCode, priority, baggageTypes);
        }

        public static void Update(string airlineCode, int priority, string baggageTypes)
        {
            CheckData(ref airlineCode, ref priority, ref baggageTypes);

            if (!IsExist(airlineCode, priority))
            {
                throw new ApplicationException("Records not existed.");
            }
            DBCpmPriority.GetInstance().Update(airlineCode, priority, baggageTypes);
        }

        public static void Delete(string airlineCode, int priority)
        {
            airlineCode = airlineCode.Trim().ToUpper();
            DBCpmPriority.GetInstance().Delete(airlineCode, priority);
        }

        public static bool IsExist(string airlineCode, int priority)
        {
            return DBCpmPriority.GetInstance().IsExist(airlineCode, priority);
        }

        public static DSCpmPriority RetrieveAll()
        {
            return DBCpmPriority.GetInstance().RetrieveAll();
        }

        public static int GetPriority(string airlineCode, string baggageType)
        {
            int dummy = 1;
            string alCode = airlineCode;
            string bagType = baggageType;
            CheckData(ref alCode, ref dummy, ref bagType);

            DSCpmPriority ds = RetrieveAll();
            char[] del = {';'};
            int priority = -1;
            foreach (DSCpmPriority.TPRRow row in ds.TPR.Select("ALC='" + airlineCode + "'", "PR ASC"))
            {
                if (row.ALC == alCode)
                {
                    string[] btArr = row.BT.Split(del);
                    int cnt = btArr.Length;
                    for (int i=0; i<cnt; i++ )
                    {
                        if (btArr[i]==bagType) 
                        {
                            priority = row.PR;
                            break;
                        }
                    }
                    if (priority > 0) break;
                }
            }
            return priority;
        }
    }
}
