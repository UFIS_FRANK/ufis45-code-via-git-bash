using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace UFIS.ITrekLib.Util
{
    public class UtilLock
    {
        const string MT_JOBSUMM_NAME = "iTrek_JobSumm";
        const string MT_FLIGHTSUMM_NAME = "iTrek_FlightSumm";
        const string MT_TRIP_NAME = "iTrek_Trip";
        const string MT_CPM_NAME = "iTrek_Cpm";
        const string MT_ARRTIME_NAME = "iTrek_ArrTime";
        const string MT_DEPTIME_NAME = "iTrek_DepTime";
        const string MT_CONTAINER_NAME = "iTrek_Container";
        const string MT_CONFIG_NAME = "iTrek_Config";
        const string MT_UFIS_CONFIG_NAME = "iTrek_UfisConfig";

        private static Mutex _mtJobSumm = null;
        private static Mutex _mtFlightSumm = null;
        private static Mutex _mtTrip = null;
        private static Mutex _mtCpm = null;
        private static Mutex _mtArrTime = null;
        private static Mutex _mtDepTime = null;
        private static Mutex _mtContainer = null;
        private static UtilLock _utilLock = null;
        private static Mutex _mtConfig = null;
        private static Mutex _mtUfisConfig = null;

        #region Mutex Getter
        private Mutex MutexJobSumm
        {
            get
            {
                if (_mtJobSumm == null)
                {
                    try
                    {
                        _mtJobSumm = Mutex.OpenExisting(MT_JOBSUMM_NAME);
                    }
                    catch (Exception)
                    {
                    }
                    if (_mtJobSumm == null) _mtJobSumm = new Mutex(false, MT_JOBSUMM_NAME);
                }
                if (_mtJobSumm == null) throw new ApplicationException("Unable To get Mutex for JobSumm");
                return _mtJobSumm;
            }
        }

        private Mutex MutexFlightSumm
        {
            get
            {
                if (_mtFlightSumm == null)
                {
                    try
                    {
                        _mtFlightSumm = Mutex.OpenExisting(MT_FLIGHTSUMM_NAME);
                    }
                    catch (Exception)
                    {
                    }
                    if (_mtFlightSumm == null) _mtFlightSumm = new Mutex(false, MT_FLIGHTSUMM_NAME);
                }
                if (_mtFlightSumm == null) throw new ApplicationException("Unable to get Mutex for FlightSumm");
                return _mtFlightSumm;
            }
        }

        private Mutex MutexTrip
        {
            get
            {
                if (_mtTrip == null)
                {
                    try
                    {
                        _mtTrip = Mutex.OpenExisting(MT_TRIP_NAME);
                    }
                    catch (Exception)
                    {
                    }
                    if (_mtTrip == null) _mtTrip = new Mutex(false, MT_TRIP_NAME);
                }
                if (_mtTrip == null) throw new ApplicationException("Unable to get Mutex for Trip.");
                return _mtTrip;
            }
        }

        private Mutex MutexCpm
        {
            get
            {
                if (_mtCpm == null)
                {
                    try
                    {
                        _mtCpm = Mutex.OpenExisting(MT_CPM_NAME);
                    }
                    catch (Exception)
                    {
                    }
                    if (_mtCpm == null) _mtCpm = new Mutex(false, MT_CPM_NAME);
                }
                if (_mtCpm == null) throw new ApplicationException("Unable to get the Mutex for CPM.");
                return _mtCpm;
            }
        }

        private Mutex MutexArrTime
        {
            get
            {
                if (_mtArrTime == null)
                {
                    try
                    {
                        _mtArrTime = Mutex.OpenExisting(MT_ARRTIME_NAME);
                    }
                    catch (Exception)
                    {
                    }
                    if (_mtArrTime == null) _mtArrTime = new Mutex(false, MT_ARRTIME_NAME);
                }
                if (_mtArrTime == null) throw new ApplicationException("Unable to get the Mutex for ArrTime.");
                return _mtArrTime;
            }
        }
        private Mutex MutexDepTime
        {
            get
            {
                if (_mtDepTime == null)
                {
                    try
                    {
                        _mtDepTime = Mutex.OpenExisting(MT_DEPTIME_NAME);
                    }
                    catch (Exception)
                    {
                    }
                    if (_mtDepTime == null) _mtDepTime = new Mutex(false, MT_DEPTIME_NAME);
                }
                if (_mtDepTime == null) throw new ApplicationException("Unable to get the Mutex for DepTime.");
                return _mtDepTime;
            }
        }
        private Mutex MutexContainer
        {
            get
            {
                if (_mtContainer == null)
                {
                    try
                    {
                        _mtContainer = Mutex.OpenExisting(MT_CONTAINER_NAME);
                    }
                    catch (Exception)
                    {
                    }
                    if (_mtContainer == null) _mtContainer = new Mutex(false, MT_CONTAINER_NAME);
                }
                if (_mtContainer == null) throw new ApplicationException("Unable to get the Mutex for Container.");
                return _mtContainer;
            }
        }
        #endregion

        private UtilLock()
        {
        }

        public static UtilLock GetInstance()
        {
            if (_utilLock == null)
            {
                _utilLock = new UtilLock();
            }
            return _utilLock;
            // return new UtilLock();
        }

        #region Mutex for Job Summary
        public void LockJobSumm()
        {
            //mtJobSumm.WaitOne(5000, true);
            try
            {
                MutexJobSumm.WaitOne();
            }
            catch (Exception ex)
            {
                LogMsg("LockJobSumm:Err:" + ex.Message);
            }
        }

        public void ReleaseJobSumm()
        {
            try
            {
                MutexJobSumm.ReleaseMutex();
            }
            catch (Exception ex)
            {
                LogMsg("ReleaseJobSumm:Err:" + ex.Message);
            }
        }
        #endregion

        #region Mutex for Flight Summary
        public void LockFlightSumm()
        {
            //mtFlightSumm.WaitOne(5000, true);
            try
            {
                MutexFlightSumm.WaitOne();
            }
            catch (Exception ex)
            {
                LogMsg("LockFlightSumm:Err:" + ex.Message);
            }
        }

        public void ReleaseFlightSumm()
        {
            try
            {
                MutexFlightSumm.ReleaseMutex();
            }
            catch (Exception ex)
            {
                LogMsg("ReleaseFlightSumm:Err:" + ex.Message);
            }
        }
        #endregion

        #region Mutex for Trip
        public void LockTrip()
        {
            try
            {
                MutexTrip.WaitOne();
            }
            catch (Exception ex)
            {
                LogMsg("LockTrip:Err:" + ex.Message);
            }
        }

        public void ReleaseTrip()
        {
            try
            {
                MutexTrip.ReleaseMutex();
            }
            catch (Exception ex)
            {
                LogMsg("ReleaseTrip:Err:" + ex.Message);
            }
        }
        #endregion

        #region Mutex for CPM
        public void LockCpm()
        {
            try
            {
                MutexCpm.WaitOne();
            }
            catch (Exception ex)
            {
                LogMsg("LockCpm:Err:" + ex.Message);
            }
        }

        public void ReleaseCpm()
        {
            try
            {
                MutexCpm.ReleaseMutex();
            }
            catch (Exception ex)
            {
                LogMsg("ReleaseCpm:Err:" + ex.Message);
            }
        }
        #endregion

        #region Mutex for Arrival Timing
        public void LockArrTime()
        {
            try
            {
                MutexArrTime.WaitOne();
            }
            catch (Exception ex)
            {
                LogMsg("LockArrTime:Err:" + ex.Message);
            }
        }

        public void ReleaseArrTime()
        {
            try
            {
                MutexArrTime.ReleaseMutex();
            }
            catch (Exception ex)
            {
                LogMsg("ReleaseArrTime:Err:" + ex.Message);
            }
        }
        #endregion

        #region Mutex for Departure Timing
        public void LockDepTime()
        {
            try
            {
                MutexDepTime.WaitOne();
            }
            catch (Exception ex)
            {
                LogMsg("LockDepTime:Err:" + ex.Message);
            }
        }

        public void ReleaseDepTime()
        {
            try
            {
                MutexDepTime.ReleaseMutex();
            }
            catch (Exception ex)
            {
                LogMsg("ReleaseDepTime:Err:" + ex.Message);
            }
        }
        #endregion

        #region Mutex for Container
        public void LockContainer()
        {
            try
            {
                MutexContainer.WaitOne();
            }
            catch (Exception ex)
            {
                LogMsg("LockContainer:Err:" + ex.Message);
            }
        }

        public void ReleaseContainer()
        {
            try
            {
                MutexContainer.ReleaseMutex();
            }
            catch (Exception ex)
            {
                LogMsg("ReleaseContainer:Err:" + ex.Message);
            }
        }
        #endregion

        #region Mutex for Config
        private Mutex MutexConfig
        {
            get
            {
                if (_mtConfig == null)
                {
                    try
                    {
                        _mtConfig = Mutex.OpenExisting(MT_CONFIG_NAME);
                    }
                    catch (Exception)
                    {
                    }
                    if (_mtConfig == null) _mtConfig = new Mutex(false, MT_CONFIG_NAME);
                }
                if (_mtConfig == null) throw new ApplicationException("Unable to get Mutex for Config");
                return _mtConfig;
            }
        }

        public void LockConfig()
        {
            try
            {
                MutexConfig.WaitOne();
            }
            catch (Exception ex)
            {
                LogMsg("LockConfig:Err:" + ex.Message);
            }
        }

        public void ReleaseConfig()
        {
            try
            {
                MutexConfig.ReleaseMutex();
            }
            catch (Exception ex)
            {
                LogMsg("ReleaseConfig:Err:" + ex.Message);
            }
        }
        #endregion

        #region Mutex for UfisConfig
        private Mutex MutexUfisConfig
        {
            get
            {
                if (_mtUfisConfig == null)
                {
                    try
                    {
                        _mtUfisConfig = Mutex.OpenExisting(MT_UFIS_CONFIG_NAME);
                    }
                    catch (Exception)
                    {
                    }
                    if (_mtUfisConfig == null) _mtUfisConfig = new Mutex(false, MT_UFIS_CONFIG_NAME);
                }
                if (_mtUfisConfig == null) throw new ApplicationException("Unable to get Mutex for UfisConfig");
                return _mtUfisConfig;
            }
        }

        public void LockUfisConfig()
        {
            try
            {
                MutexUfisConfig.WaitOne();
            }
            catch (Exception ex)
            {
                LogMsg("LockUfisConfig:Err:" + ex.Message);
            }
        }

        public void ReleaseUfisConfig()
        {
            try
            {
                MutexUfisConfig.ReleaseMutex();
            }
            catch (Exception ex)
            {
                LogMsg("ReleaseUfisConfig:Err:" + ex.Message);
            }
        }
        #endregion

        #region Log/Trace Message
        private void LogMsg(string msg)
        {
            UtilLog.LogToTraceFile("UtilLock", msg);
        }

        private void LogTraceMsg(string msg)
        {
            UtilLog.LogToTraceFile("UtilLock", msg);
        }
        #endregion
    }
}
