using System;
using System.Collections.Generic;
using System.Text;

using UFIS.ITrekLib.DS;

namespace UFIS.ITrekLib.ENT
{
	[Serializable]
    public class EntDsCPM
    {
        private DSCpm _dsCpm = null;

        public EntDsCPM()
        {
            _dsCpm = null;
        }

        public DSCpm DsCPM
        {
			get
			{
				if (_dsCpm == null) _dsCpm = new DSCpm();
				return _dsCpm;
			}
            set { _dsCpm = value; }
        }

        public String GetUldTextAt(int idx)
        {
            string txt = "";
            txt = ((DSCpm.CPMDETRow)_dsCpm.CPMDET.Rows[idx]).ULD_TXT;
            return txt;
        }

        public int ULDCntForSin( string flightId )
        {
            int cnt = 0;
            if (_dsCpm != null)
            {
                cnt = _dsCpm.CPMDET.Select("FLNU='" + flightId + "' AND APC3='SIN'").Length;
            }
            return cnt;
        }

        public int ULDCntForInter(string flightId)
        {
            int cnt = 0;
            if (_dsCpm != null)
            {
                cnt = _dsCpm.CPMDET.Select("FLNU='" + flightId + "' AND APC3<>'SIN'").Length;
            }
            return cnt;
        }

        public int UldCount(string flightId)
        {
            int cnt = 0;
            if (_dsCpm != null)
            {
                cnt = _dsCpm.CPMDET.Select("FLNU='" + flightId + "'").Length;
            }
            return cnt;
        }

    }
}
