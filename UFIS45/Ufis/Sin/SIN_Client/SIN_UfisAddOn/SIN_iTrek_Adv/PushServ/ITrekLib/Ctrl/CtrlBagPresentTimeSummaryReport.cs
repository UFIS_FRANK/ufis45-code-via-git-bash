#define USING_MQ

using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Data;
using System.Xml;

using iTrekSessionMgr;
using iTrekWMQ;
using iTrekXML;
using UFIS.ITrekLib.Util;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.DB;
using MM.UtilLib;

namespace UFIS.ITrekLib.Ctrl
{
    public class CtrlBagPresentTimeSummaryReport
    {
        public void UpdateFlightBatchInfoToBackend()
        {
            int keepDays = CtrlConfig.GetKeepDataDays();
            if (keepDays > 0) keepDays = 0 - keepDays;
            if (keepDays < -2) keepDays = -2;
            else if (keepDays == 0) keepDays = -2;
            //DBBagPresentTimeSummaryReport.GetInstance().UpdateAllFlightBMToBackend(keepDays);
            UpdateBPTRToBackEnd(keepDays);
        }

        public static void UpdateFlightInfoToBackend(string flightUrno,
            string flightNo, string ad,
            string stUfisTime, string atUfisTime,
            string terminalId, string lightOrHeavyLoader,
            string baggageFirstTimingMet, string apronFirstTimingMet,
            string baggageLastTimingMet, string apronLastTimingMet
            )
        {
   //         1. Save the data to backend
   //through MQ Queue ITREK.ITREK.SOCC.RPT
   //<BPSR_UPD>
   //   <UAFT></UAFT>		//Flight URNO
   //   <AD><AD>			//Arrival - 'A', 
   //             //Departure - 'D'
   //   <FLNO></FLNO>		//Flight Number
   //   <ST></ST>			//Schedule Time
   //   <AT></AT>			//Actual Time
   //   <T></T>			//Terminal Id
   //   <LH></LH>			//Light Loader - 'L'
   //                             // or Heavy Loader - 'H'
   //   <BFT></BFT>               //Baggage First Timing Met. 
   //   <AFT></AFT>               //Apron First Timing Met
   //   <BLT></BLT>               //Baggage Last Timing Met
   //   <ALT></ALT>               //Apron Last Timing Met
   //</BPSRS_UPD>

   //Note :  Timing Met ==> 'P'
   //        Timing Not Met ==> Other
            //string xmlMsg = "<BPSR_UPD>" +
            //    "<UAFT>" + flightUrno + "</UAFT>" +
            //    "<AD>" + ad + "</AD>" +
            //    "<FLNO>" + flightNo + "</FLNO>" +
            //    "<ST>" + stUfisTime + "</ST>" +
            //    "<AT>" + atUfisTime + "</AT>" +
            //    "<T>" + terminalId + "</T>" +
            //    "<LH>" + lightOrHeavyLoader + "</LH>" +
            //    "<BFT>" + baggageFirstTimingMet + "</BFT>" + 
            //    "<AFT>" + apronFirstTimingMet + "</AFT>" +
            //    "<BLT>" + baggageLastTimingMet + "</BLT>" +
            //    "<ALT>" + apronLastTimingMet + "</ALT>" +
            //    "</BPSR_UPD>";
            //SendXmlMsgToBackend( xmlMsg );
            DBBagPresentTimeSummaryReport.GetInstance().UpdateFlightInfoToBackend(flightUrno,
                flightNo, ad,
                stUfisTime, atUfisTime,
                terminalId, lightOrHeavyLoader,
                baggageFirstTimingMet, apronFirstTimingMet,
                baggageLastTimingMet, apronLastTimingMet);
        }

        public static DataTable RetrieveReport(string sessionId, string terminalId,
             string frUfisTime, string toUfisTime )
        {
   //         1. Request the information
   //through MQ Queue ITREK.ITREK.SOCC.RPT

   //<BPSR_REQ>
   //   <SID><SID>		//Session Id
   //   <T></T>			//Teminal Id
   //   <FR></FR>			//From Date Time in UFIS format
   //   <TO></TO>			//To Date Time in UFIS format
   //</BPSR_REQ>
            if (terminalId == "") terminalId = "*";
            string xmlReplyFileName = GetRptFileName(sessionId);
            string xmlRequestMsg = "<BPSR_REQ>" +
                   "<SID>" + sessionId + "</SID>" +		    //Session Id
                   "<T>" + terminalId + "</T>" + 			//Teminal Id
                   "<FR>" + frUfisTime + "</FR>" +			//From Date Time in UFIS format
                   "<TO>" + toUfisTime + "</TO>" +			//To Date Time in UFIS format
                "</BPSR_REQ>";
            UtilFileIO.DeleteAFile(xmlReplyFileName);
            DBBagPresentTimeSummaryReport.GetInstance().SendXmlMsgToBackend(xmlRequestMsg);
            DataTable dt = null;
            bool hasReply = false;
            for (int i = 0; i < 30; i++)
            {
                if (File.Exists(xmlReplyFileName))
                {
                    CtrlBagPresentTimeSummaryReport ctrlRpt = new CtrlBagPresentTimeSummaryReport();
                    dt = ctrlRpt.RetrieveReplyMsg(xmlReplyFileName);
                    hasReply = true;
                    break;
                }
                System.Threading.Thread.Sleep(1000);//sleep for 1 second
            }
            if (!hasReply) throw new ApplicationException("Request Timeout.");
            return dt;
        }

        private static string GetRptFileName(string sessionId)
        {
            iTXMLPathscl paths = new iTXMLPathscl();
            string stXmlSessPath = paths.GetSessionXMLPath().Trim();
            if (stXmlSessPath.Substring(stXmlSessPath.Length - 1, 1) != @"\") stXmlSessPath += @"\";
            return  stXmlSessPath + sessionId + @"\BagPreTimeSummRpt.xml";
        }

        private static string GetFromSoccToITrekReportQueueName()
        {
            iTXMLPathscl paths = new iTXMLPathscl();
            return paths.GetTOITREKRPTQueueName();
        }

        private int RetrieveData(DataRow row, string columnName)
        {
            int data = 0;
            data = Convert.ToInt32(row[columnName]);
            return data;
        }

        public void UpdateReplyMsg(string xmlMsg)
        {
            string sessionId = "";
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xmlMsg);
            try
            {
                XmlNode xmln = xmlDoc.SelectSingleNode("//BPSR/SID");
                sessionId = xmln.InnerXml; 
                sessionId = xmlDoc.GetElementsByTagName("SID").Item(0).InnerXml;
                if (sessionId != "")
                {
                    string rptFileName = GetRptFileName(sessionId);
                    UtilFileIO.DeleteAFile(rptFileName);
                    File.AppendAllText(rptFileName, xmlMsg);
                }
                else
                {
                    LogMsg("No session Id in xml file.");
                }
            }
            catch (Exception ex)
            {
                LogMsg("UpdateReplyMsg:" + ex.Message);
            }
		}


		#region Logging
		private static void LogTraceMsg(string msg)
		{
			try
			{
				MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("CtrlBagPreTimeSummRpt", msg);
			}
			catch (Exception)
			{
				UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("CtrlBagPreTimeSummRpt", msg);
			}
		}

		private static void LogMsg(string msg)
		{
			try
			{
				MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("CtrlBagPreTimeSummRpt", msg);
			}
			catch (Exception)
			{
				UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("CtrlBagPreTimeSummRpt", msg);
			}
		}
		#endregion

		private DataTable RetrieveReplyMsg(string xmlReplyFileName)
        {

      //      3. Send the result to ITREK by MQ Queue ITREK.SOCC.ITREK.RPT
      //<BPSR>
      //    <SID></SID>
      //    <T></T>
      //    <FR></FR>
      //    <TO></TO>
      //    <TOT></TOT>
      //    <CLL></CLL>
      //    <CLH></CLH>
      //    <CBF></CBF>
      //    <CBLL></CBLL>
      //    <CBLH></CBLH>
      //    <CBL></CBL>
      //</BPSR>
            DataSet ds = new DataSet();
            ds.ReadXml(xmlReplyFileName);

            if (ds == null) throw new ApplicationException("Invalid Information.");
            if (ds.Tables.Count < 1) throw new ApplicationException("Invalid Information.");
            if (ds.Tables[0].Rows.Count < 1) throw new ApplicationException("Invalid Information.");
            DataRow row = ds.Tables[0].Rows[0];

            int totFlightNo = RetrieveData(row, "TOT");
            if (totFlightNo < 1) throw new ApplicationException("No flight was selected.");
            int lightLoader = RetrieveData(row,"CLL");
            int heavyLoader = RetrieveData(row,"CLH");
            int fbMet = RetrieveData(row,"CBF");
            int lbMetLightLoader = RetrieveData(row,"CBLL");
            int lbMetHeavyLoader = RetrieveData(row,"CBLH");

            DataTable dtRpt = new DataTable();
            dtRpt.Columns.Add("REPORT STATISTIC");
            dtRpt.Columns.Add("NUMBER");
            dtRpt.Columns.Add("PERCENTAGE");
            dtRpt.Rows.Add("Total Flights", totFlightNo, "N/A");
            dtRpt.Rows.Add("Number of Light Loader", lightLoader, "N/A");
            dtRpt.Rows.Add("Number of Heavy Loader", heavyLoader, "N/A");
            AddAnRow(dtRpt, "Absolute First Bags Meeting BPT",
                fbMet, totFlightNo);
            AddAnRow(dtRpt, "Absolute Last Bags Meeting BPT - Light Loader",
                    lbMetLightLoader, lightLoader);
            AddAnRow(dtRpt, "Absolute Last Bags Meeting BPT - Heavy Loader",
                    lbMetHeavyLoader, heavyLoader);
            AddAnRow(dtRpt, "Absolute Last Bags Meeting BPT - Total",
                (lbMetHeavyLoader + lbMetLightLoader), totFlightNo);
            return dtRpt;
        }

        private void AddAnRow(DataTable dt, string lbl, Int64 num, Int64 totNum)
        {
            if (totNum > 0)
            {
                dt.Rows.Add(lbl, num, string.Format("{0:#0.00}%", num * 100 / totNum));
            }
            else
            {
                dt.Rows.Add(lbl, num, "-");
            }
        }

        //public void SendReprotToBackend()
        //{
        //}

        public static void HkReport()
        {
            int keepDays = CtrlConfig.GetKeepDataDays();
            //DBBagPresentTimeSummaryReport.GetInstance().UpdateAllFlightBMToBackend(keepDays);
            UpdateBPTRToBackEnd(keepDays);
        }

        private static DSFlight _dsFlightForBPTR = null;

        public static void UpdateBPTRToBackEnd(int keepDays)
        {//Update the data with 
            if (keepDays > 0) keepDays = 0 - keepDays;
            DateTime dtFr = DateTime.Now.AddDays(keepDays);           
            DateTime dtTo = DateTime.Now;

            DSFlight dsCurFlight = CtrlFlight.RetrieveFlights("", dtFr, dtTo);
            if (dsCurFlight != null)
            {
                DSFlight dsNewFlight = new DSFlight();
                DBBagPresentTimeSummaryReport dbBptr = DBBagPresentTimeSummaryReport.GetInstance();

                foreach (DSFlight.FLIGHTRow curRow in dsCurFlight.FLIGHT.Select())
                {
                    string flightUrno = curRow.URNO;

                    try
                    {
                        bool updFlag = false;

                        if (_dsFlightForBPTR == null)
                        {
                            updFlag = true;
                        }
                        else
                        {
                            DSFlight.FLIGHTRow[] prevDataRow = (DSFlight.FLIGHTRow[]) _dsFlightForBPTR.FLIGHT.Select("URNO='" + flightUrno + "'");
                            if (prevDataRow == null) updFlag = true;//No Data Previously
                            else if (prevDataRow.Length < 1) updFlag = true;//No Data Previously
                            else
                            {
                                if ( (prevDataRow[0].TERMINAL != curRow.TERMINAL)
                                    || (prevDataRow[0].L_H != curRow.L_H)
                                    || (prevDataRow[0].BG_FST_BM_MET != curRow.BG_FST_BM_MET)
                                    || (prevDataRow[0].AP_FST_BM_MET != curRow.AP_FST_BM_MET)
                                    || (prevDataRow[0].BG_LST_BM_MET != curRow.BG_LST_BM_MET)
                                    || (prevDataRow[0].AP_LST_BM_MET != curRow.AP_LST_BM_MET)
                                    )
                                {
                                    updFlag = true;
                                }
                                try
                                {
                                    _dsFlightForBPTR.FLIGHT.RemoveFLIGHTRow(prevDataRow[0]);
                                }
                                catch (Exception)
                                {
                                }
                            }
                        }
                        if (updFlag)
                        {
                            dbBptr.UpdateFlightInfoToBackend(flightUrno,
                                curRow.FLNO, curRow.A_D,
                                curRow.ST, curRow.AT,
                                curRow.TERMINAL, curRow.L_H,
                                curRow.BG_FST_BM_MET, curRow.AP_FST_BM_MET,
                                curRow.BG_LST_BM_MET, curRow.AP_LST_BM_MET);
                        }
                        dsNewFlight.FLIGHT.LoadDataRow(curRow.ItemArray, true);
                    }
                    catch (Exception ex)
                    {
                        LogMsg("UpdateBPTRToBackEnd:Err:Flight Urno:" + flightUrno + "==>" + ex.Message);
                    }
                }

                _dsFlightForBPTR = dsNewFlight;
            }
        }
    }
}
