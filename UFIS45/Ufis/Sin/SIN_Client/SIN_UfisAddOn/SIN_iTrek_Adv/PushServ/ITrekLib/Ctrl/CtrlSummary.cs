using System;
using System.Collections.Generic;
using System.Text;

using System.Data;

using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.Util;
using MM.UtilLib;

namespace UFIS.ITrekLib.Ctrl
{
	public class CtrlSummary
	{
		//private bool SummarizeFlightInfo(IDbConnection conn, List<string> flightIds)
		//{
		//    bool isNewConn = false;
		//    UtilDb udb = UtilDb.GetInstance();
		//    IDbCommand cmd = null;

		//    if (conn == null)
		//    {
		//        isNewConn = true;
		//        udb.GetConnection(true);
		//        foreach( string flightId in flightIds )
		//        {
		//        //CtrlFlight.RetrieveFlightsByFlightId( 
		//        }
		//    }

		//    try
		//    {
		//        cmd = udb.GetCommand(true);
		//        cmd.Transaction.Commit();				

		//    }
		//    catch (Exception)
		//    {

		//        throw;
		//    }
		//    finally
		//    {
		//        if (isNewConn)
		//        {
		//            udb.CloseCommand(cmd, true);
		//        }
		//        else
		//        {
		//            udb.CloseCommand(cmd, true, false);
		//        }
		//    }
		//}

		

		private void ComputeBMTiming(IDbConnection conn, DSFlight.FLIGHTRow fRow, string flightUrNo)
		{
			fRow.AP_FST_BM = Convert.ToInt32(CtrlConfig.GetBmApronFirstBag());
			fRow.BG_FST_BM = Convert.ToInt32(CtrlConfig.GetBmBaggageFirstBag());

			//Light Loader ==> 6 containers and below
			//Heavy Loader ==> 7 containers and above
			int cntContainer = CtrlTrip.GetContainerCountForAFlight(flightUrNo);
			if (cntContainer <= 6)
			{ //Light Loader 
				//fRow.AP_LST_BM = Convert.ToInt32(ITPath.GetBMApronLastBagLightLoad());
				fRow.AP_LST_BM = Convert.ToInt32(CtrlConfig.GetBMApronLastBagLightLoad());
				//fRow.BG_LST_BM = Convert.ToInt32(ITPath.GetBMBaggageLastBagLightLoad());
				fRow.BG_LST_BM = Convert.ToInt32(CtrlConfig.GetBMBaggageLastBagLightLoad());
				fRow.L_H = "L";
			}
			else
			{ //Heavy Loader
				//fRow.AP_LST_BM = Convert.ToInt32(ITPath.GetBMApronLastBagHeavyLoad());
				fRow.AP_LST_BM = Convert.ToInt32(CtrlConfig.GetBMApronLastBagHeavyLoad());
				//fRow.BG_LST_BM = Convert.ToInt32(ITPath.GetBMBaggageLastBagHeavyLoad());
				fRow.BG_LST_BM = Convert.ToInt32(CtrlConfig.GetBMBaggageLastBagHeavyLoad());
				fRow.L_H = "H";
			}

			////fRow.AP_FST_BM_MET = MetTiming(fRow.AP_FST_BM, fRow.TBS_UP, fRow.FST_BAG);
			////fRow.AP_LST_BM_MET = MetTiming(fRow.AP_LST_BM, fRow.TBS_UP, fRow.LST_BAG);
			//fRow.AP_FST_BM_MET = MetTiming(fRow.AP_FST_BM, fRow.AT, fRow.FST_BAG);
			//fRow.AP_LST_BM_MET = MetTiming(fRow.AP_LST_BM, fRow.AT, fRow.LST_BAG);
			//fRow.BG_FST_BM_MET = MetTiming(fRow.BG_FST_BM, fRow.AT, fRow.FST_BAG);
			//fRow.BG_LST_BM_MET = MetTiming(fRow.BG_LST_BM, fRow.AT, fRow.LST_BAG);

			//try
			//{
			//    if (fRow.AT.Trim() != "")
			//    {
			//        if (fRow.FST_BAG.Trim() != "")
			//        {
			//            fRow.BG_FST_BAG_TIMING = Util.UtilTime.TimeDiffInMinute(fRow.FST_BAG, fRow.AT).ToString();
			//        }
			//        else
			//        {
			//            fRow.BG_FST_BAG_TIMING = "";
			//        }
			//        if (fRow.LST_BAG.Trim() != "")
			//        {
			//            fRow.BG_LST_BAG_TIMING = Util.UtilTime.TimeDiffInMinute(fRow.LST_BAG, fRow.AT).ToString();
			//        }
			//        else
			//        {
			//            fRow.BG_LST_BAG_TIMING = "";
			//        }
			//    }
			//}
			//catch (Exception)
			//{
			//}

			//NOTE: If any changes of PASS/FAIL computation formula, need to change the "BMPassFail.aspx" accordingly

			string bgRefTime = fRow.TBS_UP;
			string bgFstBagTime = fRow.FST_BAG;
			string bgLstBagTime = fRow.LST_BAG;

			string apRefTime = fRow.TBS_UP;
			string apFstBagTime = fRow.FST_BAG;
			string apLstBagTime = fRow.LST_BAG;

			string timing = "";

			fRow.AP_FST_BM_MET = MetTiming(fRow.AP_FST_BM, apRefTime, apFstBagTime, out timing);
			fRow.AP_LST_BM_MET = MetTiming(fRow.AP_LST_BM, apRefTime, apLstBagTime, out timing);
			fRow.BG_FST_BM_MET = MetTiming(fRow.BG_FST_BM, bgRefTime, bgFstBagTime, out timing);
			fRow.BG_FST_BAG_TIMING = timing;
			fRow.BG_LST_BM_MET = MetTiming(fRow.BG_LST_BM, bgRefTime, bgLstBagTime, out timing);
			fRow.BG_LST_BAG_TIMING = timing;
		}

		private string MetTiming(string bmTiming, string refUfisTime, string ufisTimeToMeasure, out string timeDiff)
		{
			string timingMet = "";
			string timing = GetTiming(refUfisTime, ufisTimeToMeasure);
			if (timing != "" && (bmTiming.Trim() != ""))
			{
				if (Convert.ToInt32(bmTiming) >= Convert.ToInt32(timing)) timingMet = "P";//Meet the timing
				else timingMet = "F";
			}
			timeDiff = timing;
			return timingMet;
		}

		private string MetTiming(int bmTiming, string refUfisTime, string ufisTimeToMeasure, out string timeDiff)
		{
			string timingMet = "";
			string timing = GetTiming(refUfisTime, ufisTimeToMeasure);
			if (timing != "")
			{
				if (bmTiming >= Convert.ToInt32(timing)) timingMet = "P";//Meet the timing
				else timingMet = "F";
			}
			timeDiff = timing;
			return timingMet;
		}

		private string GetTiming(string frTime, string toTime)
		{
			string timing = "";
			if ((toTime != null) &&
					(toTime != "") &&
					(frTime != null) &&
					(frTime != ""))
			{
				timing = UtilTime.TimeDiffInMinute(toTime, frTime).ToString();
			}
			return timing;
		}

	}
}
