using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data;

using UFIS.ITrekLib.DS;
using System.Xml;
using UFIS.ITrekLib.Ctrl;
//using iTrekSessionMgr;
using iTrekXML;
using UFIS.ITrekLib.Util;


namespace UFIS.ITrekLib.Ctrl
{
   public class CtrlApp
   {
      private const string ACC_GROUP = "ACC";
      private const string BCC_GROUP = "BCC";
      private const string AO_GROUP = "AO";
      private const string BO_GROUP = "BO";
      private const string SOCC_GROUP = "SOCC";
      private const string LCC_GROUP = "LCC";
      private static string _deviceFile = null;
      private static DateTime _lastDeviceIDFileDateTime = new DateTime(1, 1, 1);
      private static CtrlApp _ctrlApp = null;//Object of this class.

      private CtrlApp()
      {
      }

      public DSCurUser GetCurrentLoginedAOs()
      {
         return GetCurrentLoginedUsers("A");
      }

      public DSCurUser GetCurrentLoginedBOs()
      {
         return GetCurrentLoginedUsers("B");
      }

      private string GetDeviceIDFileName()
      {
         if ((_deviceFile == null) || (_deviceFile == ""))
         {
            iTXMLPathscl path = new iTXMLPathscl();
            _deviceFile = path.GetDeviceIDFile();
         }
         return _deviceFile;
      }

      /// <summary>
      /// To get current login user(s)
      /// </summary>
      /// <param name="indAoBo">user type. "A" - AOs, "B" - BOs, "*" - All Logined users</param>
      /// <returns>DSCurUser</returns>
      private DSCurUser GetCurrentLoginedUsers(string indAoBo)
      {//indAoBo ==> "A" - AOs, "B" - BOs, "*" - All Logined users
         //DSCurUser.LOGINUSERRow row;
         DSCurUser ds = new DSCurUser(), tempDs;
         string whereClause = "LGTYPE='M'";
		 //if (indAoBo != "*")
		 //{
		 //   whereClause += " AND ROLE='" + indAoBo + "'";
		 //}
         //Get the current Login user information

         try
         {
            tempDs = CtrlCurUser.RetrieveAllCurUsers();
            foreach (DSCurUser.LOGINUSERRow tempRow in tempDs.LOGINUSER.Select(whereClause))
            {
				try
				{
					if ((indAoBo == "*") ||
								((tempRow.ROLE != null) && (tempRow.ROLE.Length > 0) && (tempRow.ROLE.Substring(0, 1) == indAoBo)))
					{
						tempRow.NAME = tempRow.NAME + " (" + tempRow.UID + ")";
						ds.LOGINUSER.LoadDataRow(tempRow.ItemArray, true);
					}
				}
				catch (Exception ex)
				{
					LogMsg("GetCurrentLoginedUsers:AErr:" + ex.Message);
				}
            }
         }
         catch (Exception ex)
         {
            LogMsg("GetCurrentLoginedUsers:Err:" + ex.Message);
         }

         /////to do to read the information from DEVICEID.XML file.
         //try
         //{

         //    XmlDocument doc = new XmlDocument();

         //    doc.Load(GetDeviceIDFileName());
         //    XmlNodeList DeviceIDNodeLst = doc.SelectNodes("//deviceIDs/devices");
         //    if (DeviceIDNodeLst.Count > 0)
         //    {
         //        foreach (XmlNode DeviceNode in DeviceIDNodeLst)
         //        {
         //            string sessId = DeviceNode.Attributes.Item(0).Value;
         //            string deviceId = DeviceNode.Attributes.Item(1).Value;
         //            string staffId = DeviceNode.Attributes.Item(2).Value;
         //            string loginTime = DeviceNode.Attributes.Item(3).Value;
         //            string org = DeviceNode.Attributes.Item(4).Value.Trim().Substring(0, 1);
         //            string staffName = "";

         //            if ((indAoBo=="*") || (org == indAoBo))
         //            {
         //                try
         //                {
         //                    staffName = DeviceNode.Attributes.Item(5).Value.Trim() + " (" + staffId + ")";
         //                }
         //                catch (Exception ex)
         //                {
         //                    string fName = "";
         //                    string lName = "";
         //                    staffName = CtrlStaff.GetStaffName(staffId, out fName, out lName);
         //                    staffName = fName + " (" + staffId + ")";
         //                    LogMsg("GetCurrentLoginedUsers:Err:GetStaffName:" + ex.Message);
         //                }

         //                if (staffName == "") staffName = staffId;
         //                row = ds.LOGINUSER.NewLOGINUSERRow();
         //                row.DEVICE_ID = deviceId;
         //                row.LGTYPE = "M";
         //                row.NAME = staffName;
         //                row.SESS_ID = sessId;
         //                row.UID = staffId;
         //                row.ROLE = org;
         //                ds.LOGINUSER.AddLOGINUSERRow(row);
         //            }
         //        }
         //    }
         //}
         //catch(Exception ex)
         //{
         //    LogMsg("GetCurrentLoginedUsers:Err:" + ex.Message);
         //    //UFIS.ITrekLib.Util.UtilLog.LogToGenericEventLog("CtrlApp", ex.Message);
         //}
         return ds;
      }

      /// <summary>
      /// Get one and only one instance of this
      /// </summary>
      /// <returns></returns>
      public static CtrlApp GetInstance()
      {
         //CtrlApp ctrlApp = new CtrlApp();
         //return ctrlApp;
         if (_ctrlApp == null) _ctrlApp = new CtrlApp();
         return _ctrlApp;
      }

      public static void AddGroupReceipients(ref DSCurUser ds)
      {
         if (ds == null) ds = new DSCurUser();
         ds.LOGINUSER.AddLOGINUSERRow("AO", "G", "All AOs", "", "", "");
         ds.LOGINUSER.AddLOGINUSERRow("BO", "G", "All BOs", "", "", "");
         ds.LOGINUSER.AddLOGINUSERRow("ACC", "G", "ACC Group", "", "", "");
         ds.LOGINUSER.AddLOGINUSERRow("BCC", "G", "BCC Group", "", "", "");
         ds.LOGINUSER.AddLOGINUSERRow("LCC", "G", "LCC Group", "", "", "");
         ds.LOGINUSER.AddLOGINUSERRow("SOCC", "G", "SOCC Group", "", "", "");
      }

      public static bool IsGroupUser(string userId)
      {
         bool grpUser = false;

         switch (userId)
         {
            case "AO":
            case "BO":
            case "ACC":
            case "BCC":
            case "LCC":
            case "SOCC":
               grpUser = true;
               break;
         }

         return grpUser;
      }

      public const string MSG_RECEIPIENT_PREFIX_FOR_FLIGHT = "F!";
      public const string MSG_RECEIPIENT_PREFIX_FOR_GROUP = "G!";

      private static void AddFlightReceipients(ref DSCurUser ds, string flightId, string flightInfo)
      {
         if (ds == null) ds = new DSCurUser();
         string name = flightInfo;
         if (string.IsNullOrEmpty(name)) name = "Flight";
         ds.LOGINUSER.AddLOGINUSERRow(MSG_RECEIPIENT_PREFIX_FOR_FLIGHT + flightId, "I", name, "", "", "");
      }

      private static void AddAoFlightReceipients(ref DSCurUser ds, string aoId, string aoName)
      {
         if (string.IsNullOrEmpty(aoId) || string.IsNullOrEmpty(aoName)) throw new ApplicationException("Invalid AO Information.");
         if (ds == null) ds = new DSCurUser();
         ds.LOGINUSER.AddLOGINUSERRow(aoId, "M", aoName, "", "", "");
         LogTraceMsg("AddAoFlightReceipients:" + aoId + ",<" + aoName + ">");
      }

      public static void AddAoFlightReceipients(ref DSCurUser ds, string flightId, ref string flightInfo)
      {
         flightInfo = "";
         if (string.IsNullOrEmpty(flightId)) throw new ApplicationException("Invalid Flight Id");
         if (ds == null) ds = new DSCurUser();
         flightInfo = CtrlFlight.GetFlightSummInfo(flightId);
         string aoId = "";
         string aoFName = "";
         string aoLName = "";

         bool hasAo = CtrlFlight.HasAoInfo(flightId, out aoId, out aoFName, out aoLName);
         if (hasAo)
         {
            if (!string.IsNullOrEmpty(aoId))
            {
               AddAoFlightReceipients(ref ds, aoId, string.Format("{1} ({0})", aoId, aoFName, aoLName));
            }
         }
         AddFlightReceipients(ref ds, flightId, flightInfo);
      }

      public static DSCurUser GetUsersToSendMsgInWeb()
      {
         DSCurUser ds = CtrlApp.GetInstance().GetCurrentLoginedUsers("*");
         DSCurUser.LOGINUSERRow row;

         row = ds.LOGINUSER.NewLOGINUSERRow();
         row.DEVICE_ID = "";
         row.LGTYPE = "G";
         row.NAME = "All AOs";
         row.SESS_ID = "";
         row.UID = "AO";
         ds.LOGINUSER.AddLOGINUSERRow(row);

         row = ds.LOGINUSER.NewLOGINUSERRow();
         row.DEVICE_ID = "";
         row.LGTYPE = "G";
         row.NAME = "All BOs";
         row.SESS_ID = "";
         row.UID = "BO";
         ds.LOGINUSER.AddLOGINUSERRow(row);

         row = ds.LOGINUSER.NewLOGINUSERRow();
         row.DEVICE_ID = "";
         row.LGTYPE = "G";
         row.NAME = "ACC Group";
         row.SESS_ID = "";
         row.UID = "ACC";
         ds.LOGINUSER.AddLOGINUSERRow(row);

         row = ds.LOGINUSER.NewLOGINUSERRow();
         row.DEVICE_ID = "";
         row.LGTYPE = "G";
         row.NAME = "BCC Group";
         row.SESS_ID = "";
         row.UID = "BCC";
         ds.LOGINUSER.AddLOGINUSERRow(row);


         row = ds.LOGINUSER.NewLOGINUSERRow();
         row.DEVICE_ID = "";
         row.LGTYPE = "G";
         row.NAME = "LCC Group";
         row.SESS_ID = "";
         row.UID = "LCC";
         ds.LOGINUSER.AddLOGINUSERRow(row);

         row = ds.LOGINUSER.NewLOGINUSERRow();
         row.DEVICE_ID = "";
         row.LGTYPE = "G";
         row.NAME = "SOCC Group";
         row.SESS_ID = "";
         row.UID = "SOCC";
         ds.LOGINUSER.AddLOGINUSERRow(row);

         return ds;
      }

	  #region Logging
	  private static void LogMsg(string msg)
	  {
		  try
		  {
			  MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("CtrlApp", msg);
		  }
		  catch (Exception)
		  {
			  UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("CtrlApp", msg);
		  }
	  }

	  private static void LogTraceMsg(string msg)
	  {
		  LogMsg(msg);
	  }
	  #endregion
   }
}