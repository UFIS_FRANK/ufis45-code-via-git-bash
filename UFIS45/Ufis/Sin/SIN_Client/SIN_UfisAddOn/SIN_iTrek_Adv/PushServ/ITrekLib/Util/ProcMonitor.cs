using System;
using System.Collections.Generic;
using System.Text;

using System.Management;
using System.Diagnostics;

namespace UFIS.ITrekLib.Util
{
   public class ProcMonitor
   {
      protected PerformanceCounter _cpuCounter;
      protected PerformanceCounter _ramCounter;
      private static ProcMonitor _this = null;
      private static object _lockThis = new object();

      private ProcMonitor()
      {
         _cpuCounter = new PerformanceCounter();
         _cpuCounter.CategoryName = "Processor";
         _cpuCounter.CounterName = "% Processor Time";
         _cpuCounter.InstanceName = "_Total";
         _ramCounter = new PerformanceCounter("Memory", "Available MBytes");
      }

      public static ProcMonitor GetInstance()
      {
         if (_this == null)
         {
            lock (_lockThis)
            {
               if (_this == null)
               {
                  _this = new ProcMonitor();
               }
            }
         }
         return _this;
      }


      // Call this method every time you need to know the current cpu usage. 
      /// <summary>
      /// Get the Current Cpu Usage
      /// </summary>
      /// <returns></returns>
      public string GetCurrentCpuUsage()
      {
         return _cpuCounter.NextValue() + "%";
      }

      // Call this method every time you need to get the amount of the available RAM in Mb 
      /// <summary>
      /// Get Available Memory
      /// </summary>
      /// <returns></returns>
      public string GetAvailableRAM()
      {
         return _ramCounter.NextValue() + "Mb";
      }

      public void RecordProcessesInfo()
      {
         try
         {
            Process[] procs;
            List<ProcInfo> pListInfo = new List<ProcInfo>();

            try
            {
               procs = Process.GetProcesses();

               //loop through every process
               string procName, procInfo;
               foreach (Process proc in procs)
               {
                  if (GetProcInfo(proc, out procName, out procInfo))
                  {
                     pListInfo.Add(new ProcInfo(procName, procInfo));
                  }

                  //cleanup
                  proc.Close();
               }
            }
            catch (Exception)
            {
            }

            LogProcInfo(pListInfo);
            //cleanup
            procs = null;
            if (pListInfo != null)
            {
               pListInfo.Clear();
               pListInfo = null;
            }
         }
         catch (Exception)
         {
         }
      }

      private void LogProcInfo(List<ProcInfo> pProcInfoList)
      {
         StringBuilder sb = new StringBuilder(2);

         string newLine = Environment.NewLine;
         sb.Append(newLine + "==============================" + newLine +
            "MemInfo=" + GetMemInfo() + newLine);

         try
         {
            string logMsg = "Avail Mem=" + GetAvailableRAM() + "|CPU Usage=" + GetCurrentCpuUsage();
            sb.Append(logMsg + newLine);
         }
         catch (Exception)
         {
         }

         if (pProcInfoList != null)
         {
            sb.Append(FormatProcInfo("PId", "Time", "Mem", "PeakMem", "Handles", "Threads", "Name") + newLine);
            pProcInfoList.Sort();
            foreach (ProcInfo pinfo in pProcInfoList)
            {
               sb.Append(pinfo._procInfo + newLine);
            }
         }
         LogMsg(sb.ToString());
      }

      private bool GetProcInfo(Process proc, out string procName, out string procInfo)
      {
         bool hasInfo = false;
         procName = "";
         procInfo = "";
         try
         {
            TimeSpan cputime;
            string name, pid, time, mem, peakmem, handles, threads;

            //refreshing ensures we have the most current info about
            //this process
            proc.Refresh();

            //get a bunch of stuff - processor time, process name,
            //pid, memory usage, peak memory usage, handle count,
            //thread count.
            try
            {
               cputime = proc.TotalProcessorTime;
            }
            catch (Exception)
            {
               cputime = new TimeSpan(0, 0, 0);
            }

            //the name of the process. this is usually exename.exe minus
            //the .exe part
            name = proc.ProcessName;

            //the pid is the unique identifier the OS gave to the process
            pid = proc.Id.ToString();

            //format cpu time to look like hhh:mm:ss
            time = String.Format(
                "{0}:{1}:{2}",
                ((cputime.TotalHours - 1 < 0 ? 0 : cputime.TotalHours - 1)).ToString("##0"),
                cputime.Minutes.ToString("00"),
                cputime.Seconds.ToString("00")
            );

            //get the memory usage
            mem = (proc.WorkingSet64 / 1024).ToString() + "k";

            //get peak memory usage
            peakmem = (proc.PeakWorkingSet64 / 1024).ToString() + "k";

            //get the number of handles the process is using
            handles = proc.HandleCount.ToString();

            //get the number of threads the process is using
            threads = proc.Threads.Count.ToString();

            //print the process information to the console
            //logMsg = "Name:" + name + "|PId:" + pid + "|Time:" + time + "|Mem:" + mem + "|PeakMem:" + peakmem + "|Handles:" + handles + "|Threads:" + threads;
            //logMsg = string.Format("PId= {0,-5}| Time={1,8}| Mem={2,8}| PeakMem={3,8}| Handles={4,5}| Threads={5,5}| Name= {6}",
            //   pid, time, mem, peakmem, handles, threads, name);
            procInfo = FormatProcInfo(pid, time, mem, peakmem, handles, threads, name);
            procName = name;
            hasInfo = true;
         }
         catch (Exception)
         {
         }
         return hasInfo;
      }

      private string FormatProcInfo(string pid, string time, string mem, string peakmem, string handles, string threads, string name)
      {
         return string.Format("{0,-5}| {1,8}| {2,8}| {3,8}| {4,8}| {5,8}| {6}",
               pid, time, mem, peakmem, handles, threads, name);
      }

      private string GetMemInfo()
      {
         string info = "";
         try
         {
            ObjectQuery winQuery = new ObjectQuery("SELECT * FROM Win32_LogicalMemoryConfiguration");

            ManagementObjectSearcher searcher = new ManagementObjectSearcher(winQuery);

            foreach (ManagementObject item in searcher.Get())
            {
               info += "|Total Space = " + item["TotalPageFileSpace"];
               info += "|Total Physical Memory = " + item["TotalPhysicalMemory"];
               info += "|Total Virtual Memory = " + item["TotalVirtualMemory"];
               info += "|Available Virtual Memory = " + item["AvailableVirtualMemory"];
            }
         }
         catch (Exception)
         {
         }
         return info;
      }

      private void LogMsg(string msg)
      {
         try
         {
            UtilLog.LogToTraceFileWithFileName("ProcMonitor", msg, "Proc");
         }
         catch (Exception)
         {
         }
      }
   }

   class ProcInfo : IComparable
   {
      internal string _procName = "";
      internal string _procInfo = "";

      internal ProcInfo(string stProcName, string stProcInfo)
      {
         _procName = stProcName;
         _procInfo = stProcInfo;
      }

      public int CompareTo(object obj)
      {
         ProcInfo pInfo = (ProcInfo)obj;
         return this._procName.CompareTo(pInfo._procName);
      }
   }
}