#define TESTING_ON
#define TRACING_ON
#define LOGGING_ON
#define USING_MQ


using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Diagnostics;
using System.Collections;

using System.Data;
using System.IO;

using UFIS.ITrekLib.DS;
using iTrekXML;
using UFIS.ITrekLib.Util;
using iTrekWMQ;
using MM.UtilLib;

namespace UFIS.ITrekLib.DB
{
    class DBApronServiceReport
    {
        private static Object _lockObj = new Object();//to use in synchronisation of single access
        private static Object _lockObjHt = new Object();//to use in synchronisation of single access
        private static DBApronServiceReport _dbApronServiceReport = null;
        private static DateTime lastWriteDT = new System.DateTime(1, 1, 1);
        private static DateTime _lastRefreshDT = new DateTime(1,1,1);

        
        private static DSApronServiceRpt _dsApronServiceReport = null;
        private static string _fnApronServiceReportXml = null;//file name of ASR XML in full path.

        /// <summary>
        /// Singleton Pattern.
        /// Not allowed to create the object from other class.
        /// To get the object, user "GetInstance()" method.
        /// </summary>
        private DBApronServiceReport() { }

        public void Init()
        {//Initialize all the information, to get the latest data when fetching them again.
            _dsApronServiceReport = null;
            _fnApronServiceReportXml = null;
            lastWriteDT = new System.DateTime(1, 1, 1);
            _lastRefreshDT = new DateTime(1, 1, 1);
        }

        /// <summary>
        /// Get the Instance of DBApronServiceReport. There will be only one reference for all the clients
        /// </summary>
        /// <returns></returns>
        public static DBApronServiceReport GetInstance()
        {
            if (_dbApronServiceReport == null)
            {
                _dbApronServiceReport = new DBApronServiceReport();
            }
            return _dbApronServiceReport;
        }

        public DSApronServiceRpt RetrieveApronServiceReportForAFlight(string flightUrNo)
        {
            DSApronServiceRpt ds = new DSApronServiceRpt();

            foreach (DSApronServiceRpt.ASRRow row in dsApronServiceReport.ASR.Select("UAFT='" + flightUrNo + "'"))
            {
                ds.ASR.LoadDataRow(row.ItemArray, true);
            }

            return ds;
        }

        private string fnApronServiceReportXml
        {
            get
            {
                if ((_fnApronServiceReportXml == null) || (_fnApronServiceReportXml == ""))
                {
                    iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
                    _fnApronServiceReportXml = path.GetApronServiceReportFilePath();
                }

                return _fnApronServiceReportXml;
            }
        }

        private DSApronServiceRpt dsApronServiceReport
        {
            get
            {
                int timeElapse = Math.Abs(UtilTime.DiffInSec(DateTime.Now, _lastRefreshDT));
                if ((_dsApronServiceReport == null) || (timeElapse > 3))
                {
                    LoadDSApronServiceRpt();
                    _lastRefreshDT = DateTime.Now;
                }
                DSApronServiceRpt ds = (DSApronServiceRpt)_dsApronServiceReport.Copy();

                return ds;
            }
        }


        public bool IsApronServiceRptExist(string flightId, out bool draft)
        {
            bool exist = false;
            draft = false;
            exist = IsASRExist(flightId, out draft);
            //DSApronServiceRpt dsTemp = dsApronServiceReport;
            //if (dsTemp != null)
            //{
            //    foreach (DSApronServiceRpt.ASRRow row in dsApronServiceReport.ASR.Select("UAFT='" + flightId + "'"))
            //    {
            //        exist = true;
            //        if ((row.DRAFT != null) && (row.DRAFT == "Y")) { draft = true; }
            //        else draft = false;
            //    }
            //}
            return exist;
        }

        public bool IsApronServiceRptExist(string flightId)
        {
            bool exist = false;
            bool draft;
            exist = IsASRExist(flightId, out draft);

            //DSApronServiceRpt dsTemp = dsApronServiceReport;
            //if (dsTemp != null)
            //{
            //    exist = dsTemp.ASR.Select("UAFT='" + flightId + "'").Length>0;
            //}
            return exist;
        }

        public bool IsDraft(string flightId)
        {
            bool draft = false;
            bool exist = IsASRExist(flightId, out draft);
            //DSApronServiceRpt dsTemp = dsApronServiceReport;
            //if (dsTemp != null)
            //{
            //    foreach (DSApronServiceRpt.ASRRow row in dsApronServiceReport.ASR.Select("UAFT='" + flightId + "'"))
            //    {
            //        if ((row.DRAFT != null) && (row.DRAFT == "Y")) { draft = true; } 
            //    }
            //}
            return draft;
        }

        private void LoadDSApronServiceRpt()
        {
            FileInfo fiXml = new FileInfo(fnApronServiceReportXml);
            DateTime curDT = fiXml.LastWriteTime;

            if ((_dsApronServiceReport == null) || (curDT.CompareTo(lastWriteDT) != 0))
            {
                lock (_lockObj)
                {
                    try
                    {
                        curDT = fiXml.LastWriteTime;//check the time again, in case of lock
                        if ((_dsApronServiceReport == null) || (curDT.CompareTo(lastWriteDT) != 0))
                        {
                            try
                            {
                                DSApronServiceRpt tempDs = new DSApronServiceRpt();
                                tempDs.ASR.ReadXml(fnApronServiceReportXml);
                                if (_dsApronServiceReport == null)
                                {
                                    _dsApronServiceReport = new DSApronServiceRpt();
                                }
                                else
                                {
                                    _dsApronServiceReport.ASR.Clear();
                                }
                                _dsApronServiceReport.ASR.Merge(tempDs.ASR);
                                CreateASRExistTable();
                                lastWriteDT = curDT;
                            }
                            catch (Exception ex)
                            {
                                LogMsg("LoadDSApronServiceRpt:Err: " + ex.Message);
                                TestWriteASR();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogMsg("LoadDSApronServiceRpt:Err:" + ex.Message);
                    }
                }
            }
        }

        private static Hashtable _htASRExist = new Hashtable();

        private bool IsASRExist(string flightUrno, out bool isDraft)
        {
            bool isASRExist = false;
            isDraft = false;

            if (dsApronServiceReport != null)
            {
                if (_htASRExist != null)
                {
                    if (_htASRExist.ContainsKey(flightUrno))
                    {
                        isASRExist = true;
                        object obj = _htASRExist[flightUrno];
                        if (obj != null)
                        {
                            isDraft = (bool)obj;
                        }
                    }
                }
            }
            return isASRExist;
        }

        private void CreateASRExistTable()
        {
            try
            {
                Hashtable ht = new Hashtable();
                DSApronServiceRpt dsTemp = new DSApronServiceRpt();
                dsTemp.ASR.Merge(_dsApronServiceReport.ASR);
                foreach (DSApronServiceRpt.ASRRow row in dsTemp.ASR)
                {
                    bool draft = false;
                    if ((row.DRAFT != null) && (row.DRAFT == "Y")) { draft = true; }

                    ht.Add(row.UAFT, draft);
                }
                _htASRExist = ht;
            }
            catch (Exception ex)
            {
                LogMsg("CreateASRExistTable:Err:" + ex.Message);
            }
        }

        //public void UpdateApronServiceReportsInfoFromBackendToApronServiceReportXmlFile(XmlDocument xmlDoc)
        //{
        //    lock (_lockObj)
        //    {
        //        FileInfo fi = new FileInfo( fnApronServiceReportXml );
        //        if (!fi.Exists){
        //            DSApronServiceRpt temp = new DSApronServiceRpt();
        //            temp.WriteXml(fnApronServiceReportXml);
        //        }

        //        DSApronServiceRpt dsOldApronServiceReport = new DSApronServiceRpt();
        //        dsOldApronServiceReport.ReadXml( fnApronServiceReportXml );

        //        DataSet dsNewApronServiceReport = new DataSet();
        //        XmlNodeReader reader = new XmlNodeReader(xmlDoc);
        //        dsNewApronServiceReport.ReadXml(reader);

        //        int cnt = dsNewApronServiceReport.Tables["ASR"].Rows.Count;
        //        for (int i=0; i<cnt; i++)
        //        {
        //            DataRow newRow = dsNewApronServiceReport.Tables["ASR"].Rows[i];
        //            string urno = newRow["URNO"].ToString();
        //            string flightUrno = newRow["UAFT"].ToString();
        //            string uldNo = newRow["ULDN"].ToString();
        //            DSApronServiceRpt.ASRRow[] tempRowArr = (DSApronServiceRpt.ASRRow[])dsOldApronServiceReport.ASR.Select("URNO='" + urno + "'");
        //            if (tempRowArr.Length>0)
        //            {//Existing record - to update
        //                tempRowArr[0].Delete();

        //            }else
        //            {//New Record - Insert
                        
        //            }
        //            DSApronServiceRpt.ASRRow updRow = dsOldApronServiceReport.ASR.NewASRRow();
        //            //updRow.DES = newRow["DES"].ToString();
        //            //updRow.ISCPM = newRow["ISCPM"].ToString();
        //            //updRow.RCV_BY = newRow["RCV_BY"].ToString();
        //            //updRow.RCV_DT = newRow["RCV_DT"].ToString();
        //            //updRow.RCV_RMK = newRow["RCV_RMK"].ToString();
        //            //updRow.RTRPNO = newRow["RTRPNO"].ToString();
        //            //updRow.SENT_BY = newRow["SEND_BY"].ToString();
        //            //updRow.SENT_DT = newRow["SEND_DT"].ToString();
        //            //updRow.SENT_RMK = newRow["SEND_RMK"].ToString();
        //            //updRow.STRPNO = newRow["STRPNO"].ToString();
        //            //updRow.UAFT = newRow["UAFT"].ToString();
        //            //updRow.ULDN = newRow["ULDN"].ToString();
        //            //updRow.URNO = newRow["URNO"].ToString();
        //            dsOldApronServiceReport.ASR.AddASRRow(updRow);
        //            //dsOldApronServiceReport.ASR.AddASRRow( (DSApronServiceRpt.ASRRow) newRow );
        //        }
        //        dsOldApronServiceReport.ASR.AcceptChanges();
        //        dsOldApronServiceReport.WriteXml(fnApronServiceReportXml);
        //    }
        //}

        public void SaveRpt(string flightUrno, DSApronServiceRpt dsToSave, string userId)
        {
            lock (_lockObj)
            {
                try
                {
                    LogTraceMsg("Saving ASR Report for UAFT=" + flightUrno + "," + userId);
                    DSApronServiceRpt dsCur = dsApronServiceReport;
                    foreach (DSApronServiceRpt.ASRRow row in dsToSave.ASR.Select("UAFT='" + flightUrno + "'"))
                    {
                        dsCur.ASR.LoadDataRow(row.ItemArray, LoadOption.Upsert);
                        if ((row.DRAFT != null) && (row.DRAFT != "Y"))
                        {                            
                            UpdateASRToBackEnd(row);
                        }
                        break;
                    }
                    dsCur.ASR.AcceptChanges();
                    dsCur.ASR.WriteXml(fnApronServiceReportXml);
                    LogTraceMsg("Saved ASR Report for UAFT=" + flightUrno);
                }
                catch (Exception ex)
                {
                    LogMsg("SaveRpt:Err:" + ex.Message);
                }
            }
        }

        public void SaveRpt(string flightUrno, string transitFltUrno, DSApronServiceRpt dsToSave, string userId)
        {
            lock (_lockObj)
            {
                try
                {
                    LogTraceMsg("SaveRpt" + dsToSave.ASR.Rows.Count);
                    DSApronServiceRpt dsCur = (DSApronServiceRpt)dsApronServiceReport.Copy();
                    string stSel = "UAFT='" + flightUrno + "'";
                    if ((transitFltUrno != null) && (transitFltUrno != ""))
                    {
                        stSel += " OR UAFT='" + transitFltUrno + "'";
                    }
                    LogTraceMsg("sel:" + stSel);
                    foreach (DSApronServiceRpt.ASRRow row in dsToSave.ASR.Select(stSel))
                    {
                        dsCur.ASR.LoadDataRow(row.ItemArray, LoadOption.Upsert);
                        LogTraceMsg("Upd:" + row.UAFT + "," + userId );
                        if ((row.DRAFT != null) && (row.DRAFT != "Y"))
                        {
                            UpdateASRToBackEnd(row);
                        }
                    }
                    dsCur.ASR.AcceptChanges();
                    dsCur.ASR.WriteXml(fnApronServiceReportXml);
                    LogTraceMsg("after write");
                }
                catch (Exception ex)
                {
                    LogMsg("SaveRpt:Err:" + ex.Message);
                }
            }
        }

        private void UpdateASRToBackEnd(DSApronServiceRpt.ASRRow row)
        {
            string st = "";
            string at = "";
            st = row.ST;
            at = row.AT;
            if (row.TURN != "")
            {
                if (row.AD == "A")
                {
                    st = row.STA;
                    at = row.ATA;
                }
                else if (row.AD == "D")
                {
                    st = row.STD;
                    at = row.ATD;
                }
            }
            LogTraceMsg("Send ASR Report for UAFT=" + row.UAFT);
            string stXml = "<ASRPTS>" +
                 "<ASR>" +
                    "<UAFT>" + row.UAFT + "</UAFT>" +
                    "<FLNO>" + row.FLNO + "</FLNO>" +
                    "<AD>" + row.AD + "</AD>" +
                    "<AOID>" + row.AOID + "</AOID>" +
                    "<AOFNAME>" + row.AOFNAME + "</AOFNAME>" +
                    "<AOLNAME>" + row.AOLNAME + "</AOLNAME>" +
                    "<ACFTREGN>" + row.ACFTREGN + "</ACFTREGN>" +
                    "<ACFTTYPE>" + row.ACFTTYPE + "</ACFTTYPE>" +
                    "<FR>" + row.FR + "</FR>" +
                    "<TO>" + row.TO + "</TO>" +
                    "<ST>" + st + "</ST>" +
                    "<AT>" + at + "</AT>" +
                    "<BAY>" + row.BAY + "</BAY>" +
                    "<PLBA>" + row.PLBA + "</PLBA>" +
                    "<PLBB>" + row.PLBB + "</PLBB>" +
                    "<JCLFWD>" + row.JCLFWD + "</JCLFWD>" +
                    "<JCLAFT>" + row.JCLAFT + "</JCLAFT>" +
                    "<TRNO>" + row.TRNO + "</TRNO>" +
                    "<SLFWD>" + row.SLFWD + "</SLFWD>" +
                    "<SLAFT>" + row.SLAFT + "</SLAFT>" +
                    "<MODE>" + row.MODE + "</MODE>" +
                    "<LOAD>" + row.LOAD + "</LOAD>" +
                    "<FTRIP>" + row.FTRIP + "</FTRIP>" +
                    "<LTRIP>" + row.LTRIP + "</LTRIP>" +
                    "<UNLEND>" + row.UNLEND + "</UNLEND>" +
                    "<LDSTRT>" + row.LDSTRT + "</LDSTRT>" +
                    "<LDEND>" + row.LDEND + "</LDEND>" +
                    "<LSTDOR>" + row.LSTDOR + "</LSTDOR>" +
                    "<PLB>" + row.PLB + "</PLB>" +
                    "<CBRIEF>" + row.CBRIEF + "</CBRIEF>" +
                    "<CSAFE>" + row.CSAFE + "</CSAFE>" +
                    "<CCONE>" + row.CCONE + "</CCONE>" +
                    "<CLOW>" + row.CLOW + "</CLOW>" +
                    "<CEXT>" + row.CEXT + "</CEXT>" +
                    "<CBRTEST>" + row.CBRTEST + "</CBRTEST>" +
                    "<CDMGBDY>" + row.CDMGBDY + "</CDMGBDY>" +
                    "<CDMGDG>" + row.CDMGDG + "</CDMGDG>" +
                    "<CDG>" + row.CDG + "</CDG>" +
                    "<CULD>" + row.CULD + "</CULD>" +
                    "<OICAO>" + row.OICAO + "</OICAO>" +
                    "<OICAO_D>" + row.OICAO_D + "</OICAO_D>" +
                    "<OBRK>" + row.OBRK + "</OBRK>" +
                    "<OBRK_D>" + row.OBRK_D + "</OBRK_D>" +
                    "<OCPM>" + row.OCPM + "</OCPM>" +
                    "<OCPM_D>" + row.OCPM_D + "</OCPM_D>" +
                    "<OBG>" + row.OBG + "</OBG>" +
                    "<OBG_D>" + row.OBG_D + "</OBG_D>" +
                    "<OWETH>" + row.OWETH + "</OWETH>" +
                    "<OWETH_D>" + row.OWETH_D + "</OWETH_D>" +
                    "<OLGHT>" + row.OLGHT + "</OLGHT>" +
                    "<OLGHT_D>" + row.OLGHT_D + "</OLGHT_D>" +
                    "<OBAY>" + row.OBAY + "</OBAY>" +
                    "<OBAY_D>" + row.OBAY_D + "</OBAY_D>" +
                    "<OOVR>" + row.OOVR + "</OOVR>" +
                    "<OOVR_D>" + row.OOVR_D + "</OOVR_D>" +
                    "<OFOD>" + row.OFOD + "</OFOD>" +
                    "<OFOD_D>" + row.OFOD_D + "</OFOD_D>" +
                    "<OILS>" + row.OILS + "</OILS>" +
                    "<OILS_D>" + row.OILS_D + "</OILS_D>" +
                    "<OJAM>" + row.OJAM + "</OJAM>" +
                    "<OJAM_D>" + row.OJAM_D + "</OJAM_D>" +
                    "<OGSE>" + row.OGSE + "</OGSE>" +
                    "<OGSE_D>" + row.OGSE_D + "</OGSE_D>" +
                    "<OWARP>" + row.OWARP + "</OWARP>" +
                    "<OWARP_D>" + row.OWARP_D + "</OWARP_D>" +
                    "<ODNG>" + row.ODNG + "</ODNG>" +
                    "<ODNG_D>" + row.ODNG_D + "</ODNG_D>" +
                    "<OBLK>" + row.OBLK + "</OBLK>" +
                    "<OBLK_D>" + row.OBLK_D + "</OBLK_D>" +
                    "<ULDCMT>" + row.ULDCMT + "</ULDCMT>" +
                    "<GENCMT>" + row.GENCMT + "</GENCMT>" +
                    "<RSNBPT>" + row.RSNBPT + "</RSNBPT>" +
                    "<DRO>" + row.DRO + "</DRO>" +
                    "<DM>" + row.DM + "</DM>" +
                  "</ASR>" +
                 "</ASRPTS>";
            SendXmlMsgToBackend(stXml);
        }

        private string GetFromITrekToSoccReportQueueName()
        {
            iTXMLPathscl paths = new iTXMLPathscl();
            return paths.GetFROMITREKRPTQueueName();
        }

        [Conditional("USING_MQ")]
        private void SendXmlMsgToBackend(string xmlMsg)
        {
            ITrekWMQClass1 mq = new ITrekWMQClass1();
            mq.putTheMessageIntoQueue(xmlMsg, GetFromITrekToSoccReportQueueName());
        }

        //[Conditional("TRACING_ON")]
        private void LogTraceMsg(string msg)
        {
           try
           {
              MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("DBApronServiceReport", msg);
           }
           catch (Exception)
           {
              UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DBApronServiceReport", msg);
           }
        }

        public void HkReport(ArrayList arrList)
        {
            int cnt = arrList.Count;
            for (int i = 0; i < cnt; i++)
            {
                DelAReport(arrList[i].ToString().Trim());
            }
        }

        private void DelAReport(string flightUrno)
        {
            if (flightUrno == "") return;

            lock (_lockObj)
            {
                try
                {
                    DSApronServiceRpt dsCur = dsApronServiceReport;
                    foreach( DSApronServiceRpt.ASRRow row in dsCur.ASR.Select("UAFT='" + flightUrno + "'"))
                    {
                        row.Delete();
                    }
                    dsCur.AcceptChanges();
                    dsCur.ASR.WriteXml(fnApronServiceReportXml);
                }
                catch (Exception ex)
                {
                    LogMsg("DelAReport:Error:" + flightUrno + ":" + ex.Message);
                }
            }
        }

        [Conditional("LOGGING_ON")]
        private void LogMsg(string msg)
        {
            //UtilLog.LogToGenericEventLog("DBApronServiceReport", msg);
            //UtilLog.LogToTraceFile("DBApronServiceReport", msg);
           LogTraceMsg(msg);
        }

        private void TestWriteASR()
        {
            //DSApronServiceRpt ds = new DSApronServiceRpt();
            //ds.ASR.WriteXml(fnApronServiceReportXml);
        }

    }

}
