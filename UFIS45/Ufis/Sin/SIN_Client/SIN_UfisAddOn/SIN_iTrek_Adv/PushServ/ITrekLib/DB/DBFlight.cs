//#define TESTING_ON
#define TRACE_ON

using System;
using System.Collections.Generic;
using System.Text;

using System.IO;
using System.Diagnostics;
using System.Data;

using System.Collections;

using iTrekXML;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.Util;
using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.Ctrl;
using iTrekWMQ;
using MM.UtilLib;

namespace UFIS.ITrekLib.DB
{
   public class DBFlight : IDisposable
   {
      private static Object _lockObj = new Object();//to use in synchronisation of single access
      private static Object _lockObjArrivalTime = new Object();//to use in synchronisation of single access
      private static Object _lockObjDepartureTime = new Object();//to use in synchronisation of single access
      private static DBFlight _dbFlight = null;


      private static readonly DateTime DEFAULT_DT = new System.DateTime(1, 1, 1);
      private static DateTime _lastFLIGHTWriteDT = DEFAULT_DT;
      private static DateTime _lastRefreshDT = DEFAULT_DT;
      private static DateTime _lastArrivalTimeWriteDT = DEFAULT_DT;
      private static DateTime _lastDepartureTimeWriteDT = DEFAULT_DT;
      private static string _fnArrFlightTimingXml = null;
      private static string _fnDepFlightTimingXml = null;

      private static DSFlight _dsFlight = null;
      private static DSArrivalFlightTimings _dsArrivalFlightTimings = null;
      private static DSDepartureFlightTimings _dsDepartureFlightTimings = null;
      private static string _fnFlightXml = null;//file name of FLIGHT XML in full path.

      /// <summary>
      /// Singleton Pattern.
      /// Not allowed to create the object from other class.
      /// To get the object, user "GetInstance()" method.
      /// </summary>
      private DBFlight()
      {
         //DE.FlightSummUpdated += new FlightSummUpdEventHandler(DE_FlightSummUpdated);
         LogTraceMsg("Subscribe Flight Summ Update");
         DE.FlightSummUpdated += DE_FlightSummUpdated;
      }

      public void Dispose()
      {
         LogTraceMsg("UnSubscribe Flight Summ Update");
         DE.FlightSummUpdated -= DE_FlightSummUpdated;
      }

      void DE_FlightSummUpdated(object sender, DSFlight dsFlight, DateTime dtLastUpd)
      {
         LogTraceMsg("DE_FlightSummUpdated");
         _dsFlight = dsFlight;
         _lastFLIGHTWriteDT = dtLastUpd;
         _lastRefreshDT = DateTime.Now;
      }

      public void Init()
      {//Initialize all the information, to get the latest data when fetching them again.
         _dsFlight = null;
         _dsArrivalFlightTimings = null;
         _dsDepartureFlightTimings = null;
         _fnFlightXml = null;
         _fnArrFlightTimingXml = null;
         _fnDepFlightTimingXml = null;
         _lastFLIGHTWriteDT = DEFAULT_DT;
         _lastArrivalTimeWriteDT = DEFAULT_DT;
         _lastDepartureTimeWriteDT = DEFAULT_DT;
         _lastRefreshDT = DEFAULT_DT;
      }

      /// <summary>
      /// Get the Instance of DBFlight. There will be only one reference for all the clients
      /// </summary>
      /// <returns></returns>
      public static DBFlight GetInstance()
      {
         if (_dbFlight == null)
         {
            _dbFlight = new DBFlight();
         }
         return _dbFlight;
      }

      public string GetFlightInfoSumm(string flightId)
      {
         string info = "";
         try
         {
            DSFlight ds = RetrieveFlightInfoForAFlight(flightId);
            DSFlight.FLIGHTRow row = (DSFlight.FLIGHTRow)ds.FLIGHT.Rows[0];
            //info = row.FL_SUMM;
            string flDate = string.Format("{0:dd/MMM/yyyy}", UtilTime.ConvUfisTimeStringToDateTime(row.ST)).Replace("-", "/");
            info = string.Format("{0}-{1}-{2}",
               row.FLNO, flDate, row.A_D);
         }
         catch (Exception)
         {
         }
         return info;
      }

      //public DSFlight RetrieveArrivalFlights(string terminal, DateTime fr, DateTime to)
      //{
      //    return RetrieveArrivalFlights(terminal, fr, to, EnFlightDateFilterType.UseATETST);
      //}

      //public DSFlight RetrieveArrivalFlights(string terminal, DateTime fr, DateTime to, EnFlightDateFilterType filterDateTye)
      //{
      //    DSFlight ds = new DSFlight();
      //    DSFlight tempDs = dsFlight;
      //    //Assign to local variable
      //    // to avoid calling "property get method" in foreach loop
      //    string stFr = UtilTime.ConvDateTimeToUfisFullDTString(fr);
      //    string stTo = UtilTime.ConvDateTimeToUfisFullDTString(to);
      //    string selString = "A_D='A'";
      //    if (!((terminal == null) || (terminal == "")))
      //    {
      //        selString += " AND TERMINAL='" + terminal + "'";
      //    }

      //    string flDtTag = "FLIGHT_DT";
      //    switch (filterDateTye)
      //    {
      //        case EnFlightDateFilterType.UseATETST:
      //            flDtTag = "FLIGHT_DT";
      //            break;
      //        case EnFlightDateFilterType.UseOBLETST:
      //            flDtTag = "VBA_FLDT";
      //            break;
      //        default:
      //            break;
      //    }
      //    selString += " AND " + flDtTag + ">='" + stFr + "' ";
      //    selString += " AND " + flDtTag + "<='" + stTo + "'";

      //    foreach (DSFlight.FLIGHTRow row in tempDs.FLIGHT.Select(selString))
      //    {
      //        ds.FLIGHT.LoadDataRow(row.ItemArray, true);
      //    }

      //    return ds;
      //}

      ///// <summary>
      ///// Retrieve the flights 
      ///// based on the filter criteria in 'filter'
      ///// </summary>
      ///// <param name="filter">filter criteria object</param>
      ///// <param name="adId">Arrival or Departure Indicator. 'A' - Arrival, 'D' - Departure, '' - All</param>
      ///// <returns></returns>
      //public DSFlight RetrieveFlights(EntWebFlightFilter filter, string adId)
      //{
      //    DSFlight ds = new DSFlight();
      //    DSFlight tempDs = dsFlight;
      //    //Assign to local variable
      //    // to avoid calling "property get method" in foreach loop
      //    //LogTraceMsg("RetrieveFlights:" + filter.UseFrToFixedDateTime + ", " + 
      //    //    filter.FrFixedDateTime.ToLongTimeString() + ", " + filter.FromDateTime.ToLongTimeString());
      //    string stFr = UtilTime.ConvDateTimeToUfisFullDTString(filter.FromDateTime);
      //    string stTo = UtilTime.ConvDateTimeToUfisFullDTString(filter.ToDateTime);
      //    //LogMsg("Retrieving Flight for terminal " + terminal + ", " + stFr + ", " + stTo);
      //    string selString = "";
      //    string conj = "";

      //    if (!string.IsNullOrEmpty(adId))
      //    {
      //        selString += conj + " (A_D='" + adId.Trim().ToUpper() + "')";
      //        conj = " AND ";
      //    }

      //    if (!string.IsNullOrEmpty(filter.Regn))
      //    {
      //        selString += conj + UtilDataSet.PrepareWhereClause(
      //            filter.Regn, EntWebFlightFilter.REGN_SEPERATOR,
      //            "REGN", "'");
      //        conj = " AND ";
      //    }
      //    if (!((filter.TerminalIds == null) || (filter.TerminalIds == "")))
      //    {
      //        selString += conj + UtilDataSet.PrepareWhereClause(
      //            filter.TerminalIds, EntWebFlightFilter.TERMINAL_SEPERATOR,
      //            "TERMINAL", "'");
      //        conj = " AND ";
      //    }

      //    selString += conj + " FLIGHT_DT>='" + stFr + "' ";
      //    selString += " AND FLIGHT_DT<='" + stTo + "'";

      //    LogTraceMsg("RetrieveFlights selString==>" + selString);
      //    foreach (DSFlight.FLIGHTRow row in tempDs.FLIGHT.Select(selString))
      //    {
      //        string airlineCode = "";
      //        try
      //        {
      //            airlineCode = row.FLNO.Substring(0, 2);
      //        }
      //        catch (Exception)
      //        {
      //        }
      //        if (filter.IsMeetCriteria(airlineCode, row.REGN))
      //        {
      //            //LogTraceMsg("RetrieveFlights:IN:" + row.URNO + "," + row.FLNO + "," + row.ST + "," + airlineCode + "," + row.REGN + "," + row.ST_TIME);
      //            ds.FLIGHT.LoadDataRow(row.ItemArray, true);
      //        }
      //        else
      //        {
      //            //LogTraceMsg("RetrieveFlights:OUT:" + row.FLNO + "," + row.ST + "," + airlineCode + "," + row.REGN);
      //        }
      //    }
      //    return ds;
      //}


      /// <summary>
      /// Retrieve the flights 
      /// based on the filter criteria in 'filter'
      /// </summary>
      /// <param name="filter">filter criteria object</param>
      /// <param name="userId">User Id to Filter (if blank or null, no need to filter with user Id)</param>
      /// <param name="maxNoOfFlightsToFetch">Maximum No. of Flights To Fetch (if 0 or -1 then Unlimited')</param>
      /// <param name="sortByTime">Sort by Ascending or Descending or None</param>
      /// <returns>DSFlight</returns>
      public DSFlight RetrieveFlights(EntWebFlightFilter filter,
         string userId, int maxNoOfFlightsToFetch, EnSort sortByTime)
      {
         DSFlight ds = new DSFlight();
         DSFlight tempDs = dsFlight;
         //Assign to local variable
         // to avoid calling "property get method" in foreach loop
         //LogTraceMsg("RetrieveFlights:" + filter.UseFrToFixedDateTime + ", " + 
         //    filter.FrFixedDateTime.ToLongTimeString() + ", " + filter.FromDateTime.ToLongTimeString());
         string stFr = UtilTime.ConvDateTimeToUfisFullDTString(filter.FromDateTime);
         string stTo = UtilTime.ConvDateTimeToUfisFullDTString(filter.ToDateTime);
         //LogMsg("Retrieving Flight for terminal " + terminal + ", " + stFr + ", " + stTo);
         string selString = "";
         string conj = "";

         if (!string.IsNullOrEmpty(filter.FlightType))
         {
            selString += conj + " (A_D='" + filter.FlightType + "')";
            conj = " AND ";
         }

         if (!string.IsNullOrEmpty(userId))
         {
            selString += conj + " (AOID='" + userId + "' OR BOID='" + userId + "')";
            conj = " AND ";
         }

         if (!string.IsNullOrEmpty(filter.Regn))
         {
            selString += conj + UtilDataSet.PrepareWhereClause(
                filter.Regn, EntWebFlightFilter.REGN_SEPERATOR,
                "REGN", "'");
            conj = " AND ";
         }
         if (!((filter.TerminalIds == null) || (filter.TerminalIds == "")))
         {
            selString += conj + UtilDataSet.PrepareWhereClause(
                filter.TerminalIds, EntWebFlightFilter.TERMINAL_SEPERATOR,
                "TERMINAL", "'");
            conj = " AND ";
         }

         selString += string.Format("{0} ({1}>='{2}' AND {1}<='{3}')",
            conj, filter.StDateFilterType, stFr, stTo);
         //selString += conj +" FLIGHT_DT>='" + stFr + "' ";
         //selString += " AND FLIGHT_DT<='" + stTo + "'";

         string stSortBy = "";
         if (sortByTime == EnSort.Asc) stSortBy = filter.StDateFilterType + " ASC";
         else if (sortByTime == EnSort.Desc) stSortBy = filter.StDateFilterType + " DESC";

         LogTraceMsg("RetrieveFlights selString==>" + selString);
         int cnt = 0;
         bool chkCnt = false;
         if (maxNoOfFlightsToFetch > 0) chkCnt = true;

         foreach (DSFlight.FLIGHTRow row in tempDs.FLIGHT.Select(selString, stSortBy))
         {
            string airlineCode = "";
            try
            {
               if (!string.IsNullOrEmpty(row.FLNO))
               {
                  if (row.FLNO.Length > 2)
                     airlineCode = row.FLNO.Substring(0, 3).Trim();
                  else
                     airlineCode = row.FLNO.Substring(0, 2).Trim();
               }
            }
            catch (Exception)
            {
            }

            if (filter.IsMeetCriteria(airlineCode, row.REGN))
            {
               //LogTraceMsg("RetrieveFlights:IN:" + row.URNO + "," + row.FLNO + "," + row.ST + "," + airlineCode + "," + row.REGN + "," + row.ST_TIME);
               ds.FLIGHT.LoadDataRow(row.ItemArray, true);
               if (chkCnt)
               {//Check the Count of row
                  cnt++;
                  if (cnt >= maxNoOfFlightsToFetch) break;
               }
            }
            else
            {
               //LogTraceMsg("RetrieveFlights:OUT:" + row.FLNO + "," + row.ST + "," + airlineCode + "," + row.REGN);
            }
         }

         return ds;
      }


      //public DSFlight RetrieveFlights(string terminal, DateTime fr, DateTime to)
      //{
      //   DSFlight ds = new DSFlight();
      //   DSFlight tempDs = dsFlight;
      //   //Assign to local variable
      //   // to avoid calling "property get method" in foreach loop
      //   string stFr = UtilTime.ConvDateTimeToUfisFullDTString(fr);
      //   string stTo = UtilTime.ConvDateTimeToUfisFullDTString(to);
      //   //LogMsg("Retrieving Flight for terminal " + terminal + ", " + stFr + ", " + stTo);
      //   string selString = "";
      //   string conj = "";
      //   if (!((terminal == null) || (terminal == "")))
      //   {
      //      selString = "TERMINAL='" + terminal + "'";
      //      conj = " AND ";
      //   }

      //   selString += conj + " FLIGHT_DT>='" + stFr + "' ";
      //   selString += " AND FLIGHT_DT<='" + stTo + "'";
      //   //LogTraceMsg("RetrieveFlights selString==>" + selString);
      //   foreach (DSFlight.FLIGHTRow row in tempDs.FLIGHT.Select(selString))
      //   {
      //      ds.FLIGHT.LoadDataRow(row.ItemArray, true);
      //   }

      //   return ds;
      //}


      public DSFlight RetrieveFlightInfoForAFlight(string flightUrNo)
      {
         //LogTraceMsg("DBFlight,RetrieveFlightInfoForAFlight," + flightUrNo);
         DSFlight ds = new DSFlight();
         DSFlight tempDs = dsFlight;
         //Assign to local variable
         // to avoid calling "property get method" in foreach loop

         foreach (DSFlight.FLIGHTRow row in tempDs.FLIGHT.Select("URNO='" + flightUrNo + "'"))
         {
            ds.FLIGHT.LoadDataRow(row.ItemArray, true);
         }

         return ds;
      }


      public string RetrieveRelatedTransitFlightUrnoForFlight(string flightUrNo)
      {
         string stTransitFltUrno = "";

         DSFlight tempDs = dsFlight;
         //Assign to local variable
         // to avoid calling "property get method" in foreach loop
         foreach (DSFlight.FLIGHTRow row in tempDs.FLIGHT.Select("URNO='" + flightUrNo + "'"))
         {
            stTransitFltUrno = row.TURN;
         }

         if (stTransitFltUrno == null) stTransitFltUrno = "";
         stTransitFltUrno = stTransitFltUrno.Trim();
         return stTransitFltUrno;
      }

      public DSFlight RetrieveFlightInfoForAFlightWithDoorClosed(string flightUrNo)
      {
         DSFlight ds = new DSFlight();
         DSFlight tempDs = dsFlight;
         //Assign to local variable
         // to avoid calling "property get method" in foreach loop

         foreach (DSFlight.FLIGHTRow row in tempDs.FLIGHT.Select("URNO='" + flightUrNo + "' AND LST_DOOR<>''"))
         {
            ds.FLIGHT.LoadDataRow(row.ItemArray, true);
         }

         return ds;
      }

      public enum EnSort { Desc = -1, None = 0, Asc = 1 }

      public DSFlight RetrieveAssignedFlights(string terminal, string userId, int maxNoOfFlightsToFetch, EnSort sortByTime,
          DateTime fr, DateTime to)
      {
         DSFlight ds = new DSFlight();
         DSFlight tempDs = dsFlight;
         int cnt = 0;
         string stSortBy = "";
         string terminalSel = "";
         if (!((terminal == null) || (terminal == ""))) terminalSel = " AND TERMINAL='" + terminal + "'";
         if (sortByTime == EnSort.Asc) stSortBy = "FLIGHT_DT ASC";
         else if (sortByTime == EnSort.Desc) stSortBy = "FLIGHT_DT DESC";

         string stFr = UtilTime.ConvDateTimeToUfisFullDTString(fr);
         string stTo = UtilTime.ConvDateTimeToUfisFullDTString(to);
         string timeSelString = " AND FLIGHT_DT>='" + stFr + "' ";
         timeSelString += " AND FLIGHT_DT<='" + stTo + "'";
         string selString = "(AOID='" + userId + "' OR BOID='" + userId + "') " + terminalSel + timeSelString;
         //LogTraceMsg("RetrieveAssignedFlights selstring==>" + selString + ", " + stSortBy);
         foreach (DSFlight.FLIGHTRow row in tempDs.FLIGHT.Select(selString, stSortBy))
         {
            ds.FLIGHT.LoadDataRow(row.ItemArray, true);
            cnt++;
            if (cnt >= maxNoOfFlightsToFetch) break;
         }
         return ds;
      }

      public DSFlight RetrieveAssignedFlights(EntWebFlightFilter filter, string adId, string userId, int maxNoOfFlightsToFetch, EnSort sortByTime)
      {
         if (string.IsNullOrEmpty(userId)) throw new ApplicationException("No User Information.");

         DSFlight ds = new DSFlight();
         DSFlight tempDs = dsFlight;
         int cnt = 0;

         //-----------
         string stFr = UtilTime.ConvDateTimeToUfisFullDTString(filter.FromDateTime);
         string stTo = UtilTime.ConvDateTimeToUfisFullDTString(filter.ToDateTime);
         string selString = "";
         string conj = " AND ";

         selString = "(AOID='" + userId + "' OR BOID='" + userId + "') ";

         if (!string.IsNullOrEmpty(adId))
         {
            selString += conj + " (A_D='" + adId.Trim().ToUpper() + "')";
            conj = " AND ";
         }

         if (!string.IsNullOrEmpty(filter.Regn))
         {
            selString += conj + UtilDataSet.PrepareWhereClause(
                filter.Regn, EntWebFlightFilter.REGN_SEPERATOR,
                "REGN", "'");
            conj = " AND ";
         }
         if (!((filter.TerminalIds == null) || (filter.TerminalIds == "")))
         {
            selString += conj + UtilDataSet.PrepareWhereClause(
                filter.TerminalIds, EntWebFlightFilter.TERMINAL_SEPERATOR,
                "TERMINAL", "'");
            conj = " AND ";
         }

         selString += conj + " FLIGHT_DT>='" + stFr + "' ";
         selString += " AND FLIGHT_DT<='" + stTo + "'";
         string stSortBy = "";
         if (sortByTime == EnSort.Asc) stSortBy = "FLIGHT_DT ASC";
         else stSortBy = "FLIGHT_DT DESC";

         LogTraceMsg("RetrieveAssignedFlights:" + selString);

         LogTraceMsg("RetrieveFlights selString==>" + selString);
         foreach (DSFlight.FLIGHTRow row in tempDs.FLIGHT.Select(selString, stSortBy))
         {
            string airlineCode = "";
            try
            {
               airlineCode = row.FLNO.Substring(0, 3).Trim();
            }
            catch (Exception)
            {
            }
            if (filter.IsMeetCriteria(airlineCode, row.REGN))
            {
               //LogTraceMsg("RetrieveFlights:IN:" + row.FLNO + "," + row.ST + "," + airlineCode + "," + row.REGN);
               ds.FLIGHT.LoadDataRow(row.ItemArray, true);
               cnt++;
               if (cnt >= maxNoOfFlightsToFetch) break;
            }
            else
            {
               //LogTraceMsg("RetrieveFlights:OUT:" + row.FLNO + "," + row.ST + "," + airlineCode + "," + row.REGN);
            }
         }
         return ds;
      }


      public DSFlight RetrieveAssignedFlights(string terminal, string userId, int maxNoOfFlightsToFetch, EnSort sortByTime)
      {
         DSFlight ds = new DSFlight();
         DSFlight tempDs = dsFlight;
         int cnt = 0;
         string stSortBy;
         string terminalSel = "";
         if (!((terminal == null) || (terminal == ""))) terminalSel = " AND TERMINAL='" + terminal + "'";
         if (sortByTime == EnSort.Asc) stSortBy = "FLIGHT_DT ASC";
         else stSortBy = "FLIGHT_DT DESC";
         foreach (DSFlight.FLIGHTRow row in tempDs.FLIGHT.Select("(AOID='" + userId + "' OR BOID='" + userId + "') " + terminalSel, stSortBy))
         {
            ds.FLIGHT.LoadDataRow(row.ItemArray, true);
            cnt++;
            if (cnt >= maxNoOfFlightsToFetch) break;
         }
         return ds;
      }


      public DSFlight RetrieveClosedFlightsForAO(string aoId, int maxNoOfFlightsToFetch, EnSort sortByTime)
      {
         DSFlight ds = new DSFlight();
         DSFlight tempDs = dsFlight;
         int cnt = 0;
         string stSortBy;
         if (sortByTime == EnSort.Asc) stSortBy = "FLIGHT_DT ASC";
         else stSortBy = "FLIGHT_DT DESC";
         foreach (DSFlight.FLIGHTRow row in tempDs.FLIGHT.Select("AOID='" + aoId + "' AND CLOSEDFLIGHTS='Y'", stSortBy))
         {
            ds.FLIGHT.LoadDataRow(row.ItemArray, true);
            cnt++;
            if (cnt >= maxNoOfFlightsToFetch) break;
         }
         return ds;
      }

      public DSFlight RetrieveOpenFlightsForAO(string aoId, int maxNoOfFlightsToFetch)
      {
         DSFlight ds = new DSFlight();
         DSFlight tempDs = dsFlight;
         int cnt = 0;
         foreach (DSFlight.FLIGHTRow row in tempDs.FLIGHT.Select("AOID='" + aoId + "' AND CLOSEDFLIGHTS<>'Y'", "FLIGHT_DT ASC"))
         {
            ds.FLIGHT.LoadDataRow(row.ItemArray, true);
            cnt++;
            if (cnt >= maxNoOfFlightsToFetch) break;
         }
         return ds;
      }

      public DSFlight RetrieveOpenFlightsForAO(string aoId)
      {
         DSFlight ds = new DSFlight();
         DSFlight tempDs = dsFlight;

         foreach (DSFlight.FLIGHTRow row in tempDs.FLIGHT.Select("AOID='" + aoId + "' AND CLOSEDFLIGHTS<>'Y'", "FLIGHT_DT ASC"))
         {
            ds.FLIGHT.LoadDataRow(row.ItemArray, true);
         }
         return ds;
      }
      /*Added by Alphy for testing purpose*/
      public DSFlight RetrieveOpenFlightsForAO()
      {
         DSFlight ds = new DSFlight();
         DSFlight tempDs = dsFlight;

         foreach (DSFlight.FLIGHTRow row in tempDs.FLIGHT.Select("CLOSEDFLIGHTS<>'Y'", "FLIGHT_DT ASC"))
         {
            ds.FLIGHT.LoadDataRow(row.ItemArray, true);
         }
         return ds;
      }

      public DSFlight RetrieveClosedFlightsForAO(int maxNoOfFlightsToFetch, EnSort sortByTime)
      {
         DSFlight ds = new DSFlight();
         DSFlight tempDs = dsFlight;
         int cnt = 0;
         string stSortBy;
         if (sortByTime == EnSort.Asc) stSortBy = "FLIGHT_DT ASC";
         else stSortBy = "FLIGHT_DT DESC";
         foreach (DSFlight.FLIGHTRow row in tempDs.FLIGHT.Select("CLOSEDFLIGHTS='Y'", stSortBy))
         {
            ds.FLIGHT.LoadDataRow(row.ItemArray, true);
            cnt++;
            if (cnt >= maxNoOfFlightsToFetch) break;
         }
         return ds;
      }

      private string fnFlightXml
      {
         get
         {
            if ((_fnFlightXml == null) || (_fnFlightXml == ""))
            {
               iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
               _fnFlightXml = path.GetSummFlightsFilePath();
            }

            return _fnFlightXml;
         }
      }

      private string fnArrFlightTimingXml
      {
         get
         {
            if ((_fnArrFlightTimingXml == null) || (_fnArrFlightTimingXml == ""))
            {
               iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
               _fnArrFlightTimingXml = path.GetArrFlightsTimingFilePath();
            }

            return _fnArrFlightTimingXml;
         }
      }

      private string fnDepFlightTimingXml
      {
         get
         {
            if ((_fnDepFlightTimingXml == null) || (_fnDepFlightTimingXml == ""))
            {
               iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
               _fnDepFlightTimingXml = path.GetDepFlightsTimingFilePath();
            }

            return _fnDepFlightTimingXml;
         }
      }

      public DSFlight dsFlight
      {
         get
         {
            //int timeElapse = Math.Abs(GetTimeInMMSS(DateTime.Now) - GetTimeInMMSS(_lastRefreshDT));
            int timeElapse = Math.Abs(UtilTime.DiffInSec(DateTime.Now, _lastRefreshDT));
            if ((_dsFlight == null) || (timeElapse > 3))
            {
               //LogTraceMsg("TimeElapse:" + timeElapse);
               bool loadingSuccess = false;
               LoadDSFlight(out loadingSuccess, false);
               _lastRefreshDT = DateTime.Now;
               LogTraceMsg("Refresh" + _lastRefreshDT.ToLongTimeString());
            }

            //DSFlight ds = new DSFlight();
            //ds = (DSFlight)_dsFlight.Copy();

            //return ds;
            return (DSFlight)_dsFlight.Copy();
         }
      }

      private DSArrivalFlightTimings dsArrivalFlightTiming
      {
         get
         {
            LoadDSArrivalFlightTimings();
            DSArrivalFlightTimings ds = (DSArrivalFlightTimings)_dsArrivalFlightTimings.Copy();

            return ds;
         }
      }

      private DSDepartureFlightTimings dsDepartureFlightTiming
      {
         get
         {
            LoadDSDepartureFlightTimings();
            DSDepartureFlightTimings ds = (DSDepartureFlightTimings)_dsDepartureFlightTimings.Copy();

            return ds;
         }
      }

      private void LoadDSArrivalFlightTimings()
      {
         FileInfo fiXml = new FileInfo(fnArrFlightTimingXml);
         DateTime curDT = fiXml.LastWriteTime;
         if (!fiXml.Exists) throw new ApplicationException("ArrivalFlightTimings Detail information missing. Please inform to administrator.");

         if ((_dsArrivalFlightTimings == null) || (curDT.CompareTo(_lastArrivalTimeWriteDT) != 0))
         {
            lock (_lockObjArrivalTime)
            {
               try
               {
                  curDT = fiXml.LastWriteTime;//check the time again, in case of lock
                  if ((_dsArrivalFlightTimings == null) || (curDT.CompareTo(_lastArrivalTimeWriteDT) != 0))
                  {
                     try
                     {
                        DSArrivalFlightTimings tempDs = new DSArrivalFlightTimings();
                        tempDs.ReadXml(fnArrFlightTimingXml);
                        _dsArrivalFlightTimings = tempDs;
                        _lastArrivalTimeWriteDT = curDT;
                     }
                     catch (Exception ex)
                     {
                        LogMsg(ex.Message);
                     }
                  }
               }
               catch (Exception ex)
               {
                  LogMsg("LoadDSArrivalFlightTimings:Err" + ex.Message);
               }
            }
         }
      }

      private void LoadDSDepartureFlightTimings()
      {
         FileInfo fiXml = new FileInfo(fnDepFlightTimingXml);
         DateTime curDT = fiXml.LastWriteTime;
         if (!fiXml.Exists) throw new ApplicationException("DepartureFlightTimings Detail information missing. Please inform to administrator.");

         if ((_dsDepartureFlightTimings == null) || (curDT.CompareTo(_lastDepartureTimeWriteDT) != 0))
         {
            lock (_lockObjArrivalTime)
            {
               try
               {
                  curDT = fiXml.LastWriteTime;//check the time again, in case of lock
                  if ((_dsDepartureFlightTimings == null) || (curDT.CompareTo(_lastDepartureTimeWriteDT) != 0))
                  {
                     try
                     {
                        DSDepartureFlightTimings tempDs = new DSDepartureFlightTimings();
                        tempDs.ReadXml(fnDepFlightTimingXml);
                        _dsDepartureFlightTimings = tempDs;
                        _lastDepartureTimeWriteDT = curDT;
                     }
                     catch (Exception ex)
                     {
                        LogMsg(ex.Message);
                     }
                  }
               }
               catch (Exception ex)
               {
                  LogMsg("LoadDSDepartureFlightTimings:Err:" + ex.Message);
               }
            }
         }
      }

      public DSFlight LoadDSFlight(out bool loadSuccess, bool returnACopyWhenSuccess)
      {
         loadSuccess = false;
         //LogMsg("Summarise the flight Infor");
         FileInfo fiXml = new FileInfo(fnFlightXml);
         DateTime curDT = fiXml.LastWriteTime;
         if (!fiXml.Exists) throw new ApplicationException("FLIGHT information missing. Please inform to administrator.");
         if ((_dsFlight == null) || (curDT.CompareTo(_lastFLIGHTWriteDT) != 0))
         {
            lock (_lockObj)
            {
               try
               {
                  fiXml = new FileInfo(fnFlightXml);
                  curDT = fiXml.LastWriteTime;//check the time again, in case of lock
                  if ((_dsFlight == null) || (curDT.CompareTo(_lastFLIGHTWriteDT) != 0))
                  {
                     try
                     {
                        DSFlight tempDs = new DSFlight();
                        //tempDs.ReadXml(fnFlightXml);
                        if (UtilDataSet.LoadDataFromXmlFile(tempDs, fnFlightXml))
                        {
                           if ((tempDs != null) && (tempDs.FLIGHT.Rows.Count > 0))
                           {
                              _dsFlight = tempDs;
                              _lastFLIGHTWriteDT = curDT;
                              loadSuccess = true;
                              SetLatestInfoToDataset(tempDs);
                              //LogTraceMsg("Get new flight info.");
                           }
                           else
                           {
                              LogMsg("No Flight Data.");
                           }
                        }
                     }
                     catch (Exception ex)
                     {
                        LogMsg("LoadDSFlight:Err:" + ex.Message);
                        throw new ApplicationException("Error Accessing Flight Information.");
                     }
                  }

               }
               catch (Exception ex)
               {
                  LogMsg("LoadDSFlight:Err:" + ex.Message);
               }
            }
         }
         else
         {
            loadSuccess = true;
         }

         if ((loadSuccess) && (returnACopyWhenSuccess))
         {
            return (DSFlight)_dsFlight.Copy();
         }
         return _dsFlight;
      }

      private void SetLatestInfoToDataset(DSFlight ds)
      {
         _dsFlight = ds;
         FileInfo fiXml = new FileInfo(fnFlightXml);
         _lastFLIGHTWriteDT = fiXml.LastWriteTime;
      }

      private void SaveFlightXml(DSFlight ds)
      {
         UtilDataSet.WriteXml(ds, fnFlightXml);
         SetLatestInfoToDataset(ds);
      }

      //[Conditional("TRACE_ON")]
      private void LogTraceMsg(string msg)
      {
         try
         {
            MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("DBFlight", msg);
         }
         catch (Exception)
         {
            UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DBFlight", msg);
         }
      }

      private void LogMsg(string msg)
      {
         //Util.UtilLog.LogToGenericEventLog("DBFlight", msg);
         //UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DBFlight", msg);
         try
         {
            MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("DBFlight", msg);
         }
         catch (Exception)
         {
            UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DBFlight", msg);
         }
      }


      public void UpdateApronServiceRportExist(string flightUrNo, bool exist)
      {
         bool hasErr = false;
         lock (_lockObj)
         {
            try
            {
               LockFlightSummForWriting();
               //DSFlight ds = new DSFlight();
               DSFlight tempDs = null;
               //UtilDataSet.LoadDataFromXmlFile(tempDs, fnFlightXml);
               bool loadSuccess = false;
               for (int i = 0; i < 100; i++)
               {
                  tempDs = LoadDSFlight(out loadSuccess, true);
                  if (loadSuccess) break;
               }
               if (loadSuccess)
               {
                  string turnFlUrno = "";
                  foreach (DSFlight.FLIGHTRow row in tempDs.FLIGHT.Select("URNO='" + flightUrNo + "'"))
                  {
                     turnFlUrno = row.TURN;
                     if (exist)
                     {
                        row.HAS_AO_RPT = "Y";
                     }
                     else
                     {
                        row.HAS_AO_RPT = "N";
                     }
                     break;
                  }

                  if ((turnFlUrno != null) && (turnFlUrno != ""))
                  {//Update in Related Transit Flight Record as well.
                     foreach (DSFlight.FLIGHTRow row in tempDs.FLIGHT.Select("URNO='" + turnFlUrno + "'"))
                     {
                        if (exist)
                        {
                           row.HAS_AO_RPT = "Y";
                        }
                        else
                        {
                           row.HAS_AO_RPT = "N";
                        }
                        break;
                     }
                  }
                  tempDs.FLIGHT.AcceptChanges();
                  //tempDs.FLIGHT.WriteXml(fnFlightXml);
                  //UtilDataSet.WriteDataToXmlFile(tempDs, fnFlightXml);
                  SaveFlightXml(tempDs);
               }
               else
               {
                  hasErr = true;
               }
            }
            catch (Exception ex)
            {
               hasErr = true;
               LogMsg("UpdateApronServiceRportExist:Err:" + ex.Message);
            }
            finally
            {
               ReleaseFlightSummAfterWrite();
            }
         }
         if (hasErr) throw new ApplicationException("Unable to save.");
      }


      public void UpdateBagPreTimeReportExist(string flightUrNo, bool exist)
      {
         bool hasErr = false;
         lock (_lockObj)
         {
            try
            {
               LockFlightSummForWriting();
               //DSFlight ds = new DSFlight();
               DSFlight tempDs = null;
               //UtilDataSet.LoadDataFromXmlFile(tempDs, fnFlightXml);
               bool loadSuccess = false;
               for (int i = 0; i < 100; i++)
               {
                  tempDs = LoadDSFlight(out loadSuccess, true);
                  if (loadSuccess) break;
               }

               if (loadSuccess)
               {
                  foreach (DSFlight.FLIGHTRow row in tempDs.FLIGHT.Select("URNO='" + flightUrNo + "'"))
                  {
                     if (exist)
                     {
                        row.HAS_BO_RPT = "Y";
                     }
                     else
                     {
                        row.HAS_BO_RPT = "N";
                     }
                     break;
                  }

                  tempDs.FLIGHT.AcceptChanges();
                  ////tempDs.FLIGHT.WriteXml(fnFlightXml);
                  //UtilDataSet.WriteDataToXmlFile(tempDs, fnFlightXml);
                  SaveFlightXml(tempDs);
               }
               else
               {
                  hasErr = true;
               }
            }
            catch (Exception ex)
            {
               hasErr = true;
               LogMsg("UpdateBagPreTimeReportExist:Err:" + ex.Message);
            }
            finally
            {
               ReleaseFlightSummAfterWrite();
            }
         }
         if (hasErr) throw new ApplicationException("Unable to save.");
      }

      public string UpdateArrivalFlightTiming(string flightUrno, string field, string newTime, string staffUrno)
      {
         string result = "";
         string stMsg = "";

         string qName = GetFromItrekUpdateQueueName();
         lock (_lockObj)
         {
            try
            {
               LockArrFlightTimeForWriting();
               DSArrivalFlightTimings ds = null;
               int updCnt = 0;
               try
               {
                  ds = new DSArrivalFlightTimings();

                  ds.ReadXml(fnArrFlightTimingXml);
                  if (ds == null)
                  {
                     throw new ApplicationException("Unable to get the Arrival Flight Information.");
                  }
               }
               catch (ApplicationException ex) { throw ex; }
               catch (Exception ex)
               {
                  throw new ApplicationException("Unable to get the Arrival Flight Information due to " + ex.Message);
               }

               stMsg += "UpdateArrivalFlightTiming:" + flightUrno + "," + staffUrno;
               ITrekWMQClass1 wmq = new ITrekWMQClass1();

               DSArrivalFlightTimings.AFLIGHTRow[] tempRowArr = (DSArrivalFlightTimings.AFLIGHTRow[])ds.AFLIGHT.Select("URNO='" + flightUrno + "'");
               DSArrivalFlightTimings.AFLIGHTRow tempRow;
               if ((tempRowArr == null) || (tempRowArr.Length < 1))
               {
                  tempRow = ds.AFLIGHT.NewAFLIGHTRow();
                  tempRow.URNO = flightUrno;
                  ds.AFLIGHT.AddAFLIGHTRow(tempRow);
               }
               else
               {
                  tempRow = tempRowArr[0];
               }

               string orgTime = "$#%@!";//Impossible Time
               try
               {
                  orgTime = tempRow[field].ToString();
               }
               catch (Exception ex) { LogTraceMsg("UpdateArrivalFlightTiming:Err:" + field + "," + ex.Message); }
               if (string.IsNullOrEmpty(orgTime)) orgTime = BLANK_TIME;
               if (orgTime != newTime)
               {
                  string status = wmq.PutTimingOnFROMITREKUPDATEQueue(flightUrno, field, newTime, "A", qName, staffUrno);
                  if (status.Trim().ToLower() == "success")
                  {
                     if (newTime == "") newTime = BLANK_TIME;
                     tempRow[field] = newTime;
                     stMsg += "," + field + "," + orgTime + "==>" + newTime;
                     updCnt++;
                  }
                  else
                  {
                     result = status;
                  }
               }

               if (updCnt > 0)
               {
                  ds.AFLIGHT.AcceptChanges();
                  ds.WriteXml(fnArrFlightTimingXml);
                  _dsArrivalFlightTimings = ds;
               }
            }
            catch (Exception ex)
            {
               LogMsg("UpdateArrivalTime:Err:" + ex.Message);
            }
            finally
            {
               if (stMsg != "")
               {
                  LogTraceMsg(stMsg);
               }
               ReleaseArrFlightTimeAfterWrite();
            }
         }
         return result;
      }

      public string UpdateArrivalFlightTiming(string flightUrno, Hashtable htFieldValues, string staffUrno)
      {
         string result = "";
         string stMsg = "";
         string qName = GetFromItrekUpdateQueueName();
         lock (_lockObj)
         {
            try
            {
               LockArrFlightTimeForWriting();
               DSArrivalFlightTimings ds = null;
               int updCnt = 0;
               try
               {
                  ds = new DSArrivalFlightTimings();

                  ds.ReadXml(fnArrFlightTimingXml);
                  if (ds == null)
                  {
                     throw new ApplicationException("Unable to get the Arrival Flight Information.");
                  }
               }
               catch (ApplicationException ex) { throw ex; }
               catch (Exception ex)
               {
                  throw new ApplicationException("Unable to get the Arrival Flight Information due to " + ex.Message);
               }
               stMsg += "UpdateArrivalFlightTiming:" + flightUrno + "," + staffUrno;
               ITrekWMQClass1 wmq = new ITrekWMQClass1();

               DSArrivalFlightTimings.AFLIGHTRow[] tempRowArr = (DSArrivalFlightTimings.AFLIGHTRow[])ds.AFLIGHT.Select("URNO='" + flightUrno + "'");
               DSArrivalFlightTimings.AFLIGHTRow tempRow;
               if ((tempRowArr == null) || (tempRowArr.Length < 1))
               {
                  tempRow = ds.AFLIGHT.NewAFLIGHTRow();
                  tempRow.URNO = flightUrno;
                  ds.AFLIGHT.AddAFLIGHTRow(tempRow);
               }
               else
               {
                  tempRow = tempRowArr[0];
               }

               foreach (string field in htFieldValues.Keys)
               {
                  string newTime = htFieldValues[field].ToString().Trim();
                  //if (newTime == "") continue;

                  string orgTime = "$#%@!";//Impossible Time
                  try
                  {
                     orgTime = tempRow[field].ToString();
                  }
                  catch (Exception ex) { LogTraceMsg("UpdateArrivalFlightTiming1:Err:" + field + "," + ex.Message); }
                  if (string.IsNullOrEmpty(orgTime)) orgTime = BLANK_TIME;
                  if (orgTime != newTime)
                  {
                     string status = wmq.PutTimingOnFROMITREKUPDATEQueue(flightUrno, field, newTime, "A", qName, staffUrno);
                     if (status.Trim().ToLower() == "success")
                     {
                        if (newTime == "") newTime = BLANK_TIME;
                        tempRow[field] = newTime;
                        stMsg += "," + field + "," + orgTime + "==>" + newTime;
                        updCnt++;
                     }
                     else
                     {
                        result = status;
                     }
                  }
               }

               if (updCnt > 0)
               {
                  ds.AFLIGHT.AcceptChanges();
                  ds.WriteXml(fnArrFlightTimingXml);
                  _dsArrivalFlightTimings = ds;
               }
            }
            catch (Exception ex)
            {
               LogMsg("UpdateArrivalTime:Err:" + ex.Message);
            }
            finally
            {
               if (stMsg != "")
               {
                  LogTraceMsg(stMsg);
               }
               ReleaseArrFlightTimeAfterWrite();
            }
         }
         return result;
      }

      public string UpdateDepartureFlightTiming(string flightUrno, string field, string newTime, string staffUrno)
      {
         string result = "";
         string stMsg = "";

         lock (_lockObj)
         {
            try
            {
               LockDepFlightTimeForWriting();
               DSDepartureFlightTimings ds = null;
               int updCnt = 0;
               try
               {
                  ds = new DSDepartureFlightTimings();

                  ds.ReadXml(fnDepFlightTimingXml);
                  if (ds == null)
                  {
                     throw new ApplicationException("Unable to get the Departure Flight Information.");
                  }
               }
               catch (ApplicationException ex) { throw ex; }
               catch (Exception ex)
               {
                  throw new ApplicationException("Unable to get the Departure Flight Information due to " + ex.Message);
               }
               stMsg += "UpdateDepartureFlightTiming:" + flightUrno + "," + staffUrno;

               DSDepartureFlightTimings.DFLIGHTRow[] tempRowArr = (DSDepartureFlightTimings.DFLIGHTRow[])ds.DFLIGHT.Select("URNO='" + flightUrno + "'");
               DSDepartureFlightTimings.DFLIGHTRow tempRow;
               if ((tempRowArr == null) || (tempRowArr.Length < 1))
               {
                  tempRow = ds.DFLIGHT.NewDFLIGHTRow();
                  tempRow.URNO = flightUrno;
                  ds.DFLIGHT.AddDFLIGHTRow(tempRow);
               }
               else
               {
                  tempRow = tempRowArr[0];
               }

               string orgTime = "$#%@!";//Impossible Time
               try
               {
                  orgTime = tempRow[field].ToString();
               }
               catch (Exception ex) { LogTraceMsg("UpdateDepartureFlightTiming:Err:" + field + "," + ex.Message); }

               ITrekWMQClass1 wmq = new ITrekWMQClass1();
               if (string.IsNullOrEmpty(orgTime)) orgTime = BLANK_TIME;
               if (orgTime != newTime)
               {
                  string status = wmq.PutTimingOnFROMITREKUPDATEQueue(flightUrno, field, newTime, "D", GetFromItrekUpdateQueueName(), staffUrno);
                  if (status.Trim().ToLower() == "success")
                  {
                     if (newTime == "") newTime = BLANK_TIME;
                     stMsg += "," + field + "," + orgTime + "==>" + newTime;
                     //LogTraceMsg(stMsg);
                     tempRow[field] = newTime;

                     updCnt++;
                  }
                  else
                  {
                     result = status;
                  }
               }

               if (updCnt > 0)
               {
                  ds.DFLIGHT.AcceptChanges();
                  ds.WriteXml(fnDepFlightTimingXml);
                  _dsDepartureFlightTimings = ds;
               }
            }
            catch (Exception ex)
            {
               LogMsg("UpdateDepartureFlightTiming:Err:" + ex.Message);
            }
            finally
            {
               ReleaseDepFlightTimeAfterWrite();
            }
         }
         return result;
      }

      private const string BLANK_TIME = "-";//Should match with DBSummFlight BLANK_TIME

      public string UpdateDepartureFlightTiming(string flightUrno, Hashtable htFieldValues, string staffUrno)
      {
         string result = "";
         string stMsg = "";
         lock (_lockObj)
         {
            try
            {
               LockDepFlightTimeForWriting();
               DSDepartureFlightTimings ds = null;
               int updCnt = 0;
               try
               {
                  ds = new DSDepartureFlightTimings();

                  ds.ReadXml(fnDepFlightTimingXml);
                  if (ds == null)
                  {
                     throw new ApplicationException("Unable to get the Departure Flight Information.");
                  }
               }
               catch (ApplicationException ex) { throw ex; }
               catch (Exception ex)
               {
                  throw new ApplicationException("Unable to get the Departure Flight Information due to " + ex.Message);
               }


               DSDepartureFlightTimings.DFLIGHTRow[] tempRowArr = (DSDepartureFlightTimings.DFLIGHTRow[])ds.DFLIGHT.Select("URNO='" + flightUrno + "'");
               DSDepartureFlightTimings.DFLIGHTRow tempRow;
               if ((tempRowArr == null) || (tempRowArr.Length < 1))
               {
                  tempRow = ds.DFLIGHT.NewDFLIGHTRow();
                  tempRow.URNO = flightUrno;
                  ds.DFLIGHT.AddDFLIGHTRow(tempRow);
               }
               else
               {
                  tempRow = tempRowArr[0];
               }

               ITrekWMQClass1 wmq = new ITrekWMQClass1();

               foreach (string field in htFieldValues.Keys)
               {
                  string newTime = htFieldValues[field].ToString().Trim();
                  //if (newTime == "") continue;

                  string orgTime = "$#%@!";//Impossible Time
                  try
                  {
                     orgTime = tempRow[field].ToString();
                  }
                  catch (Exception ex) { LogTraceMsg("UpdateDepartureFlightTiming1:Err:" + field + "," + ex.Message); }
                  if (string.IsNullOrEmpty(orgTime)) orgTime = BLANK_TIME;
                  if (orgTime != newTime)
                  {
                     string status = wmq.PutTimingOnFROMITREKUPDATEQueue(flightUrno, field, newTime, "D", GetFromItrekUpdateQueueName(), staffUrno);
                     if (status.Trim().ToLower() == "success")
                     {
                        if (newTime == "") newTime = BLANK_TIME;
                        stMsg += "," + field + "," + orgTime + "==>" + newTime;
                        //LogTraceMsg(stMsg);
                        tempRow[field] = newTime;
                        updCnt++;
                     }
                     else
                     {
                        result = status;
                     }
                  }
               }

               if (updCnt > 0)
               {
                  //if (stUpdFieldValues != "")
                  //{
                  //    "<DFLIGHT><URNO>" + flightUrno + "</URNO>" + stFieldValues + "<ORG>" + orgTable[fncName].ToString() + "</ORG><USTF>" + ustf + "</USTF></DFLIGHT>";
                  //}
                  ds.DFLIGHT.AcceptChanges();
                  ds.WriteXml(fnDepFlightTimingXml);
                  _dsDepartureFlightTimings = ds;
               }
            }
            catch (Exception ex)
            {
               LogMsg("UpdateDepartureFlightTiming1:Err:" + ex.Message);
            }
            finally
            {
               ReleaseDepFlightTimeAfterWrite();
            }
         }
         return result;
      }

      public void DeleteAFlight(string flightUrno)
      {
         bool hasErr = false;
         lock (_lockObj)
         {
            try
            {
               LockFlightSummForWriting();
               //DSFlight tempDs = dsFlight;
               DSFlight tempDs = null;
               bool loadSuccess = false;
               for (int i = 0; i < 100; i++)
               {
                  tempDs = LoadDSFlight(out loadSuccess, true);

                  if (loadSuccess)
                  {
                     foreach (DSFlight.FLIGHTRow row in tempDs.FLIGHT.Select("URNO='" + flightUrno + "'"))
                     {
                        row.Delete();
                     }

                     tempDs.FLIGHT.AcceptChanges();
                     //tempDs.FLIGHT.WriteXml(fnFlightXml);
                     SaveFlightXml(tempDs);
                     break;
                  }
               }
            }
            catch (Exception ex)
            {
               hasErr = true;
               LogMsg("DeleteAFlight:Err:" + ex.Message);
            }
            finally
            {
               ReleaseFlightSummAfterWrite();
            }
         }
         if (hasErr) throw new ApplicationException("Unable to delete flight " + flightUrno);
      }

      public void DeleteAOFromFlight(Hashtable htUaft)
      {
         if (htUaft == null) return;
         if (htUaft.Count < 1) return;
         string[] arrSelSt = UtilMisc.GetDataForSelection(htUaft, 200);

         lock (_lockObj)
         {
            try
            {
               LogTraceMsg("DeleteAOFromFlight:Start");
               LockFlightSummForWriting();
               LogTraceMsg("DeleteAOFromFlight:After Lock");
               DSFlight tempDs = null;
               //tempDs.ReadXml(fnFlightXml);
               //UtilDataSet.LoadDataFromXmlFile(tempDs, fnFlightXml);
               bool loadSuccess = false;
               for (int i = 0; i < 100; i++)
               {
                  tempDs = LoadDSFlight(out loadSuccess, true);
                  LogTraceMsg("DeleteAOFromFlight:after Load:" + loadSuccess);
                  if (loadSuccess) break;
               }
               LogTraceMsg("DeleteAOFromFlight:after load");
               if (loadSuccess)
               {
                  int cntArr = arrSelSt.Length;
                  for (int i = 0; i < cntArr; i++)
                  {
                     string stUafts = arrSelSt[i];
                     if (stUafts == null) continue;
                     foreach (DSFlight.FLIGHTRow row in tempDs.FLIGHT.Select("URNO IN (" + stUafts + ")"))
                     {
                        LogTraceMsg("deleting AO From flight" + row.URNO);
                        row.AO = "";
                        row.AOFNAME = "";
                        row.AOID = "";
                        row.AOLNAME = "";
                     }
                  }
                  tempDs.FLIGHT.AcceptChanges();
                  SaveFlightXml(tempDs);

                  //DBSummFlight.GetInstance().Init();
                  //Init();
               }
               else
               {
                  LogMsg("DeleteAOFromFlight:Err:FailToLoad.");
               }
            }
            catch (Exception ex)
            {
               LogMsg("DeleteAOFromFlight:Err:" + ex.Message);
            }
            finally
            {
               ReleaseFlightSummAfterWrite();
            }
         }
      }

      public void DeleteAOFromFlight(string flightUrno)
      {
         if (flightUrno.Trim() == "") return;
         System.Threading.Thread.Sleep(100);
         lock (_lockObj)
         {
            try
            {
               LockFlightSummForWriting();
               DSFlight tempDs = null;
               //tempDs.ReadXml(fnFlightXml);
               //UtilDataSet.LoadDataFromXmlFile(tempDs, fnFlightXml);
               bool loadSuccess = false;
               for (int i = 0; i < 100; i++)
               {
                  tempDs = LoadDSFlight(out loadSuccess, true);
                  if (loadSuccess) break;
               }
               if (loadSuccess)
               {
                  foreach (DSFlight.FLIGHTRow row in tempDs.FLIGHT.Select("URNO='" + flightUrno + "'"))
                  {
                     LogTraceMsg("deleting AO From flight" + flightUrno);
                     row.AO = "";
                     row.AOFNAME = "";
                     row.AOID = "";
                     row.AOLNAME = "";
                     //tempDs.FLIGHT.AcceptChanges();
                     //tempDs.FLIGHT.WriteXml(fnFlightXml);
                     //UtilDataSet.WriteDataToXmlFile(tempDs, fnFlightXml);
                  }
                  tempDs.FLIGHT.AcceptChanges();
                  SaveFlightXml(tempDs);

                  //DBSummFlight.GetInstance().Init();
                  //Init();
               }
               else
               {
                  LogMsg("DeleteAOFromFlight:Err:FailToLoad.");
               }
            }
            catch (Exception ex)
            {
               LogMsg("DeleteAOFromFlight:Err:" + ex.Message);
            }
            finally
            {
               ReleaseFlightSummAfterWrite();
            }
         }
      }

      public void DeleteAOForMultipleFlight(string flightUrnos)
      {
         if (flightUrnos == null) return;
         if (flightUrnos.Trim() == "") return;
         //System.Threading.Thread.Sleep(100);
         lock (_lockObj)
         {
            try
            {
               LockFlightSummForWriting();
               string[] arrFl = flightUrnos.Split(',');
               string selString = "";
               string seperator = "";
               for (int i = 0; i < arrFl.Length; i++)
               {
                  string urno = arrFl[i].Trim();
                  if (urno == "") continue;
                  selString += seperator + " URNO='" + urno + "'";
                  seperator = " OR ";
               }
               DSFlight tempDs = null;
               //tempDs.ReadXml(fnFlightXml);
               //UtilDataSet.LoadDataFromXmlFile(tempDs, fnFlightXml);
               bool loadSuccess = false;
               for (int i = 0; i < 100; i++)
               {
                  tempDs = LoadDSFlight(out loadSuccess, true);
                  if (loadSuccess) break;
               }
               if (loadSuccess)
               {
                  bool hasChanges = false;
                  foreach (DSFlight.FLIGHTRow row in tempDs.FLIGHT.Select(selString))
                  {
                     hasChanges = true;
                     LogTraceMsg("deleting AO From flight" + row.URNO);
                     row.AO = "";
                     row.AOFNAME = "";
                     row.AOID = "";
                     row.AOLNAME = "";
                  }
                  if (hasChanges)
                  {
                     LogTraceMsg("Has Changes");
                     tempDs.FLIGHT.AcceptChanges();
                     //tempDs.FLIGHT.WriteXml(fnFlightXml);
                     //UtilDataSet.WriteDataToXmlFile(tempDs, fnFlightXml);
                     SaveFlightXml(tempDs);
                  }
                  else
                  {
                     LogTraceMsg("No changes");
                  }
               }
               else
               {
                  LogMsg("DeleteAOFromFlight:Err:Unable to load.");
               }

               //DBSummFlight.GetInstance().Init();
               //Init();
            }
            catch (Exception ex)
            {
               LogMsg("DeleteAOFromFlight:Err:" + ex.Message);
            }
            finally
            {
               ReleaseFlightSummAfterWrite();
            }
         }
      }

      private void LockFlightSummForWriting()
      {
         try
         {
            UtilLock.GetInstance().LockFlightSumm();
         }
         catch (Exception ex)
         {
            LogMsg("LockFlightSummForWriting:Err:" + ex.Message);
            if (ex.InnerException != null)
            {
               LogMsg("LockFlightSummForWriting:err:Inner:" + ex.InnerException.Message);
            }
         }
      }

      private void ReleaseFlightSummAfterWrite()
      {
         try
         {
            UtilLock.GetInstance().ReleaseFlightSumm();
         }
         catch (Exception ex)
         {
            LogMsg("ReleaseFlightSummAfterWrite:Err:" + ex.Message);
            if (ex.InnerException != null)
            {
               LogMsg("ReleaseFlightSummAfterWrite:err:Inner:" + ex.InnerException.Message);
            }
         }
      }

      private void LockArrFlightTimeForWriting()
      {
         try
         {
            UtilLock.GetInstance().LockArrTime();
         }
         catch (Exception ex)
         {
            LogMsg("LockArrFlightTimeForWriting:Err:" + ex.Message);
            if (ex.InnerException != null)
            {
               LogMsg("LockArrFlightTimeForWriting:err:Inner:" + ex.InnerException.Message);
            }
         }
      }

      private void ReleaseArrFlightTimeAfterWrite()
      {
         try
         {
            UtilLock.GetInstance().ReleaseArrTime();
         }
         catch (Exception ex)
         {
            LogMsg("ReleaseArrFlightTimeAfterWrite:Err:" + ex.Message);
            if (ex.InnerException != null)
            {
               LogMsg("ReleaseArrFlightTimeAfterWrite:err:Inner:" + ex.InnerException.Message);
            }
         }
      }

      private void LockDepFlightTimeForWriting()
      {
         try
         {
            UtilLock.GetInstance().LockDepTime();
         }
         catch (Exception ex)
         {
            LogMsg("LockDepFlightTimeForWriting:Err:" + ex.Message);
            if (ex.InnerException != null)
            {
               LogMsg("LockDepFlightTimeForWriting:err:Inner:" + ex.InnerException.Message);
            }
         }
      }

      private void ReleaseDepFlightTimeAfterWrite()
      {
         try
         {
            UtilLock.GetInstance().ReleaseDepTime();
         }
         catch (Exception ex)
         {
            LogMsg("ReleaseDepFlightTimeAfterWrite:Err:" + ex.Message);
            if (ex.InnerException != null)
            {
               LogMsg("ReleaseDepFlightTimeAfterWrite:err:Inner:" + ex.InnerException.Message);
            }
         }
      }

      public DSDepartureFlightTimings RetrieveDepartureFlightTimings(string urno)
      {
         DSDepartureFlightTimings ds = new DSDepartureFlightTimings();
         DSDepartureFlightTimings tempDs = (DSDepartureFlightTimings)dsDepartureFlightTiming.Copy();
         //Assign to local variable
         // to avoid calling "property get method" in foreach loop
         int flightCnt = 0;

         foreach (DSDepartureFlightTimings.DFLIGHTRow row in tempDs.DFLIGHT.Select(("URNO='" + urno + "'")))
         {
            flightCnt++;
            ds.DFLIGHT.LoadDataRow(row.ItemArray, true);
         }
         if (flightCnt == 0)
         {
            CreateNewDepTimings(tempDs, urno);

         }
         return ds;
      }
      public DSArrivalFlightTimings RetrieveArrivalFlightTimings(string urno)
      {
         DSArrivalFlightTimings ds = new DSArrivalFlightTimings();
         DSArrivalFlightTimings tempDs = (DSArrivalFlightTimings)dsArrivalFlightTiming.Copy();
         //Assign to local variable
         // to avoid calling "property get method" in foreach loop
         int flightCnt = 0;

         foreach (DSArrivalFlightTimings.AFLIGHTRow row in tempDs.AFLIGHT.Select(("URNO='" + urno + "'")))
         {
            flightCnt++;
            ds.AFLIGHT.LoadDataRow(row.ItemArray, true);
         }
         if (flightCnt == 0)
         {
            CreateNewArrTimings(tempDs, urno);
         }

         return ds;
      }


      public void CreateNewArrTimings(DSArrivalFlightTimings ds, string urno)
      {

         lock (_lockObj)
         {
            try
            {
               LockArrFlightTimeForWriting();

               DSArrivalFlightTimings.AFLIGHTRow updRow = ds.AFLIGHT.NewAFLIGHTRow();
               updRow.URNO = urno;
               ds.AFLIGHT.AddAFLIGHTRow(updRow);
               ds.AFLIGHT.AcceptChanges();
               ds.WriteXml(fnArrFlightTimingXml);
               _dsArrivalFlightTimings = ds;
            }
            catch (Exception ex)
            {
               LogMsg("CreateNewArrTimings:Err:" + ex.Message);
            }
            finally
            {
               ReleaseArrFlightTimeAfterWrite();
            }
         }
      }

      public void CreateNewDepTimings(DSDepartureFlightTimings ds, string urno)
      {
         lock (_lockObj)
         {
            try
            {
               LockDepFlightTimeForWriting();
               DSDepartureFlightTimings.DFLIGHTRow updRow = ds.DFLIGHT.NewDFLIGHTRow();
               updRow.URNO = urno;
               ds.DFLIGHT.AddDFLIGHTRow(updRow);
               ds.DFLIGHT.AcceptChanges();
               ds.WriteXml(fnDepFlightTimingXml);
               _dsDepartureFlightTimings = ds;
            }
            catch (Exception ex)
            {
               LogMsg("CreateNewDepTimings:Err:" + ex.Message);
            }
            finally
            {
               ReleaseDepFlightTimeAfterWrite();
            }
         }
      }

      public void deleteOldFlightsFromArrTimings(ArrayList lstFlights)
      {
         lock (_lockObj)
         {
            try
            {
               LockArrFlightTimeForWriting();
               DSArrivalFlightTimings ds = null;
               int updCnt = 0;
               try
               {
                  ds = new DSArrivalFlightTimings();

                  ds.ReadXml(fnArrFlightTimingXml);
                  if (ds == null)
                  {
                     throw new ApplicationException("Unable to get the Arrival Flight Information.");
                  }
               }
               catch (ApplicationException ex) { throw ex; }
               catch (Exception ex)
               {
                  throw new ApplicationException("Unable to get the Arrival Flight Information due to " + ex.Message);
               }

               foreach (string flightUrno in lstFlights)
               {
                  DSArrivalFlightTimings.AFLIGHTRow[] tempRowArr = (DSArrivalFlightTimings.AFLIGHTRow[])ds.AFLIGHT.Select("URNO='" + flightUrno + "'");
                  if (tempRowArr.Length > 0)
                  {//Existing record - to update
                     tempRowArr[0].Delete();
                     updCnt++;
                  }
               }

               if (updCnt > 0)
               {
                  ds.AFLIGHT.AcceptChanges();
                  ds.WriteXml(fnArrFlightTimingXml);
                  _dsArrivalFlightTimings = ds;
               }
            }
            catch (Exception ex)
            {
               LogMsg("UpdateArrivalTime:Err:" + ex.Message);
            }
            finally
            {
               ReleaseArrFlightTimeAfterWrite();
            }
         }
      }

      public void deleteOldFlightsFromDepTimings(ArrayList lstFlights)
      {
         lock (_lockObj)
         {
            try
            {
               LockDepFlightTimeForWriting();
               DSDepartureFlightTimings ds = null;
               int updCnt = 0;
               try
               {
                  ds = new DSDepartureFlightTimings();

                  ds.ReadXml(fnDepFlightTimingXml);
                  if (ds == null)
                  {
                     throw new ApplicationException("Unable to get the Departure Flight Information.");
                  }
               }
               catch (ApplicationException ex) { throw ex; }
               catch (Exception ex)
               {
                  throw new ApplicationException("Unable to get the Departure Flight Information due to " + ex.Message);
               }

               foreach (string flightUrno in lstFlights)
               {
                  DSDepartureFlightTimings.DFLIGHTRow[] tempRowArr = (DSDepartureFlightTimings.DFLIGHTRow[])ds.DFLIGHT.Select("URNO='" + flightUrno + "'");
                  if (tempRowArr.Length > 0)
                  {//Existing record - to update
                     tempRowArr[0].Delete();
                     updCnt++;
                  }
               }

               if (updCnt > 0)
               {
                  ds.DFLIGHT.AcceptChanges();
                  ds.WriteXml(fnDepFlightTimingXml);
                  _dsDepartureFlightTimings = ds;
               }
            }
            catch (Exception ex)
            {
               LogMsg("deleteOldFlightsFromDepTimings:Err:" + ex.Message);
            }
            finally
            {
               ReleaseDepFlightTimeAfterWrite();
            }
         }
      }

      private iTXMLPathscl path = new iTXMLPathscl();

      private string GetFromItrekUpdateQueueName()
      {

         return path.GetFROMITREKUPDATEQueueName();
      }

      public ArrayList RetrieveOldFlighs(out ArrayList alLongSpan, out string updatestatus)
      {
         lock (_lockObj)
         {
            ArrayList alShortSpan = new ArrayList();
            alLongSpan = new ArrayList();
            updatestatus = "";

            try
            {
               LockFlightSummForWriting();
               //DSFlight ds = new DSFlight();
               // DBSummFlight.GetInstance().SummFlight();

               DSFlight tempDs = new DSFlight();
               // tempDs.ReadXml(fnFlightXml);
               UtilDataSet.LoadDataFromXmlFile(tempDs, fnFlightXml);
               LogTraceMsg("RetrieveOldFlighs:start");

               int shortLifeSpanDataDays = Math.Abs(CtrlConfig.GetKeepShortLifeSpanDataDays());
               int longLifeSpanDataDays = Math.Abs(CtrlConfig.GetKeepDataDays());
               DateTime curDT = DateTime.Now;
               DateTime dtShort = curDT.AddHours(0 - shortLifeSpanDataDays);
               DateTime dtLong = curDT.Date.AddDays(0 - longLifeSpanDataDays);
               try
               {
                  LogTraceMsg(string.Format("RetrieveOldFlighs:Cnt-{0},Short-{1},Long-{2}",
                      tempDs.FLIGHT.Rows.Count,
                      UtilTime.ConvDateTimeToUfisFullDTString(dtShort),
                      UtilTime.ConvDateTimeToUfisFullDTString(dtLong)));
               }
               catch (Exception)
               {
               }

               StringBuilder sbDelFlights = new StringBuilder();
               int cntDel = 0;
               int cntShort = 0;
               int cntLong = 0;

               foreach (DSFlight.FLIGHTRow row in tempDs.FLIGHT.Select("", "FLIGHT_DT DESC"))
               {
                  string urno = "";
                  string stFlightDt = "";

                  string flInfo = string.Format("FL-{0},{1},{2}.",
                      row.FLIGHT_DT, row.URNO, row.FLNO);
                  string stAction = "";

                  stFlightDt = row["FLIGHT_DT"].ToString();
                  DateTime dtFlightDt = new DateTime(1, 1, 1);
                  try
                  {
                     dtFlightDt = UtilTime.ConvUfisTimeStringToDateTime(stFlightDt);
                  }
                  catch (Exception)
                  {
                  }
                  //if ((UtilTime.ConvUfisTimeStringToDateTime(row["FLIGHT_DT"].ToString()).AddHours(shortLifeSpanDataDays)).CompareTo(curDT) >= 0)
                  if (dtFlightDt.CompareTo(dtShort) >= 0)
                  {
                     urno = row["URNO"].ToString();
                     alShortSpan.Add(urno);
                     alLongSpan.Add(urno);
                     cntShort++; cntLong++;
                     stAction = "SL";
                  }
                  else
                  {
                     //if ((UtilTime.ConvUfisTimeStringToDateTime(row["FLIGHT_DT"].ToString()).Date.AddDays(longLifeSpanDataDays)).CompareTo(curDT.Date) >= 0)
                     if (dtFlightDt.CompareTo(dtLong) >= 0)
                     {
                        urno = row["URNO"].ToString();
                        alLongSpan.Add(urno);
                        cntLong++;
                        stAction = "L";
                     }
                     else
                     {
                        row.Delete();
                        cntDel++;
                        stAction = "D";
                     }
                     //tempDs.FLIGHT.RemoveFLIGHTRow(row);
                  }
                  LogTraceMsg("RetrieveOldFlighs:" + stAction + "-" + flInfo);
               }
               LogTraceMsg(string.Format("RetrieveOldFlighs:Cnt:Sh-{0},Lo-{1},Del-{2}.",
                   cntShort, cntLong, cntDel));

               tempDs.AcceptChanges();
               UtilDataSet.WriteXml(tempDs, fnFlightXml);
               // tempDs.WriteXml(fnFlightXml);
               updatestatus = "Success";
               LogTraceMsg("RetrieveOldFlighs:end" + updatestatus);

            }
            catch (Exception ex)
            {
               updatestatus = "Fail";
               LogMsg("RetrieveOldFlighs:Err:" + ex.Message);
            }
            finally
            {
               ReleaseFlightSummAfterWrite();
            }
            return alShortSpan;
         }
      }

   }
}