using System;
using System.Collections.Generic;
using System.Text;

using System.Collections;
using System.Data;

using UFIS.ITrekLib.Util;
using MM.UtilLib;
using UFIS.ITrekLib.Config;
using UFIS.ITrekLib.CustomException;
using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;

namespace UFIS.ITrekLib.CommMsg
{
	/// <summary>
	/// Class to handle the Incoming Message to iTrek.
	/// </summary>
	public class CMsgIn
	{
		#region MSGIN Methods
		//const string DB_TAB_MSG_IN = "ITK_MGITAB";
		const string MSG_IN_STAT_PROCESSED = "P"; //Processed (Processed by service)
		const string MSG_IN_STAT_OUT = "I"; //Not processed yet. (Received from MQ)
		const string MSG_IN_BATCH_STAT = "L"; //Not processed yet. (Received from MQ)

        private static string GetDb_Tb_MsgIn()
        {
            return DB_Info.GetDbFullName(DB_Info.DB_T_MGITAB);
        }
		/// <summary>
		/// Update the Processed Indicator for In Message
		/// To show that this message is already processed
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="procBy"></param>
		/// <param name="procDt"></param>
		/// <returns>True-Successfully Updated the status.</returns>
		public static bool UpdProcIndForInMessage(UtilDb udb, IDbCommand cmd, string inMsgId, string procBy, string procDt)
		{
			bool commit = false;
			bool success = false;

			try
			{				
				string paramPrefix = UtilDb.GetParaNamePrefix();
				string pProcBy = paramPrefix + "PRBY";
				string pProcDt = paramPrefix + "PRDT";
				string pStat = paramPrefix + "STAT";
				string pUrno = paramPrefix + "URNO";

				cmd.Parameters.Clear();
				udb.AddParam(cmd, pProcBy, procBy);

				string stSetProcDt = "PRDT=SYSDATE";//Default to update with System Date in DBServer.
				if (!string.IsNullOrEmpty(procDt))
				{
					try
					{
						DateTime dt = MM.UtilLib.UtilTime.ConvUfisTimeStringToDateTime(procDt);
						stSetProcDt = "PRDT=" + pProcDt;
						udb.AddParam(cmd, pProcDt, dt);
					}
					catch (Exception)
					{
					}
				}

                cmd.CommandText = "UPDATE " + GetDb_Tb_MsgIn() + " SET PRBY=" + pProcBy +
				   "," + stSetProcDt + ",STAT=" + pStat +
				   " WHERE URNO=" + pUrno;
				cmd.CommandType = CommandType.Text;


				udb.AddParam(cmd, pStat, MSG_IN_STAT_PROCESSED);
				udb.AddParam(cmd, pUrno, inMsgId);

				int cnt = cmd.ExecuteNonQuery();
				if (cnt == 1) success = true;
				else throw new ApplicationException("Fail to update the Processed Indicator.");
				commit = success;
			}
			catch (Exception)
			{
				throw;
			}

			return success;
		}

		/// <summary>
		/// Receive the new message by inserting a record into "MGITAB".
		/// Insert a record (of message received from MQ) into "MGITAB". 
		/// Message will be processed accordingly by other windows service process.
		/// </summary>
		/// <param name="recFrom">Received from (destination of message. It might be MQ Channel)</param>
		/// <param name="msg">Message Text received</param>
		/// <param name="recBy">Received by Id</param>
		/// <param name="msgType">Message Type</param>
		/// <param name="replyFrom">Id to reply to (same as urno in MGITAB)</param>
		/// <returns>URNO of MGITAB for this record.</returns>
		public static string NewMessageToProcess(IDbCommand cmd, string recFrom, string msg, string recBy, string msgType, out string replyFrom)
		{
			bool isNewCmd = false;
			UtilDb udb = UtilDb.GetInstance();
			bool success = false;
			bool commit = false;
			string urno = "";
			replyFrom = "";

			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = udb.GetCommand(true);
				}

				#region param
				string paramPrefix = UtilDb.GetParaNamePrefix();
				string precFrom = paramPrefix + "FRSR";
				string pMsg = paramPrefix + "MSGT";
				string pRcvBy = paramPrefix + "RCBY";
				string pRcvDt = paramPrefix + "RCDT";
				string pStat = paramPrefix + "STAT";
				string pRpyf = paramPrefix + "RPYF";
				string pMsgType = paramPrefix + "MTYP";
				string pUrno = paramPrefix + "URNO";
				#endregion				

				DateTime dt1 = DateTime.Now;
				#region Update the incoming message to MGITAB
				for (int i = 0; i < 20; i++)
				{
					try
					{
						urno = UtilDb.GenUrid(cmd, ProcIds.MGI, false, dt1);

						cmd.CommandText = String.Format(@"INSERT INTO {0} 
            (URNO,FRSR,MSGT,RCBY,RCDT,STAT,RPYF,MTYP) 
            VALUES ({1},{2},{3},{4},SYSDATE,{5},{6},{7})",
						   GetDb_Tb_MsgIn(), pUrno, precFrom, pMsg, pRcvBy, pStat, pRpyf, pMsgType);
						cmd.CommandType = CommandType.Text;
						cmd.Parameters.Clear();
						udb.AddParam(cmd, pUrno, urno);
						udb.AddParam(cmd, precFrom, recFrom);
						udb.AddParam(cmd, pMsg, msg);
						udb.AddParam(cmd, pRcvBy, recBy);
						udb.AddParam(cmd, pStat, MSG_IN_STAT_OUT);
						udb.AddParam(cmd, pRpyf, urno);
						udb.AddParam(cmd, pMsgType, msgType);

						int cnt = cmd.ExecuteNonQuery();

						if (cnt == 1)
						{
							commit = true;
							success = true;
							break;
						}
					}
					catch (NoCtrlSettingException)
					{
						throw;
					}
					catch (Exception ex)
					{
						LogTraceMsg("NewMessageToProcess:Err:continue:" + ex.Message);
						continue;
					}
				}
				#endregion
			}
			catch (Exception ex)
			{
				LogMsg("NewMessageToProcess:Err:" + ex.Message);
			}
			finally
			{
				if (isNewCmd)
				{
					udb.CloseCommand(cmd, commit);
				}
			}

			if (!success) throw new ApplicationException("Unable to create message." + msg );
			return urno;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="recFrom"></param>
		/// <param name="msg"></param>
		/// <param name="recBy"></param>
		/// <param name="msgType"></param>
		/// <param name="replyFrom"></param>
		/// <returns></returns>
		public static string NewBatchMessageToProcess(IDbCommand cmd, string recFrom, string msg, string recBy, string msgType, out string replyFrom)
		{
			string paramPrefix = UtilDb.GetParaNamePrefix();
			string precFrom = paramPrefix + "FRSR";
			string pMsg = paramPrefix + "MSGT";
			string pRcvBy = paramPrefix + "RCBY";
			string pRcvDt = paramPrefix + "RCDT";
			string pStat = paramPrefix + "STAT";
			string pRpyf = paramPrefix + "RPYF";
			string pMsgType = paramPrefix + "MTYP";
			string pUrno = paramPrefix + "URNO";
			string pInd = paramPrefix + "IND";

			replyFrom = "";
			UtilDb udb = UtilDb.GetInstance();
			//bool commit = false;

			bool success = false;
			string urno = "";

			DateTime dt1 = DateTime.Now;

			for (int i = 0; i < 100; i++)
			{
				try
				{
					urno = UtilDb.GenUrid(cmd, ProcIds.MGI, false, dt1);
					LogTraceMsg("NewBatchMessageToProcess urno" + urno);
					cmd.CommandText = String.Format(@"INSERT INTO {0} 
            (URNO,FRSR,MSGL,RCBY,RCDT,STAT,RPYF,MTYP,LIND) 
            VALUES ({1},{2},{3},{4},SYSDATE,{5},{6},{7},{8})",
                       GetDb_Tb_MsgIn(), pUrno, precFrom, pMsg, pRcvBy, pStat, pRpyf, pMsgType, pInd);
					cmd.CommandType = CommandType.Text;
					cmd.Parameters.Clear();
					udb.AddParam(cmd, pUrno, urno);
					udb.AddParam(cmd, precFrom, recFrom);
					udb.AddParam(cmd, pMsg, msg);
					udb.AddParam(cmd, pRcvBy, recBy);
					udb.AddParam(cmd, pStat, MSG_IN_STAT_OUT);
					udb.AddParam(cmd, pRpyf, urno);
					udb.AddParam(cmd, pMsgType, msgType);
					udb.AddParam(cmd, pInd, MSG_IN_BATCH_STAT);

					int cnt = cmd.ExecuteNonQuery();

					if (cnt == 1)
					{
						//commit = true;
						success = true;
						break;
					}
				}
				catch (NoCtrlSettingException)
				{
					throw;
				}
				catch (Exception ex)
				{
					LogTraceMsg("NewBatchMessageToProcess" + ex.Message);
					continue;
				}
			}

			if (!success) throw new ApplicationException("Unable to create new message to send.");
			return urno;
		}
		private static void LogTraceMsg(string msg)
		{
			try
			{
                MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("CMSGIn", msg);
				
			}
			catch (Exception)
			{
				UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("CMsgIn", msg);
			}
		}

		private const string _cmdSelMgi = @"SELECT URNO,MSGT,MTYP,LIND,MSGL";
		private const string FIELD_NAME_MGI = "URNO,MSGT,MTYP,STAT,LIND,MSGL";
		public static List<InMsg> SelectMessageToProcess(UtilDb udb, IDbCommand cmd)
		{
			InMsg inmsg = null;
			List<InMsg> arInmsg = new List<InMsg>();

			try
			{
				// bool success = false;
				string paramPrefix = UtilDb.GetParaNamePrefix();

				string pStat = paramPrefix + "STAT";

				cmd.Parameters.Clear();
				udb.AddParam(cmd, pStat, MSG_IN_STAT_OUT);
				//cmd.CommandText = _cmdSelMgi + " WHERE STAT = " + pStat + " ORDER BY MTYP, RCDT ASC";
				cmd.CommandText = string.Format( "{0} FROM {1} WHERE STAT={2} ORDER BY MTYP, RCDT ASC",
					_cmdSelMgi + GetDb_Tb_MsgIn(), pStat );
				cmd.CommandType = CommandType.Text;
				IDataReader myReader = cmd.ExecuteReader();
				try
				{
					while (myReader.Read())
					{
						inmsg = new InMsg();
						inmsg.Urno = myReader.GetString(0);
						inmsg.Message = myReader.GetString(1);
						inmsg.Mtyp = myReader.GetString(2);
						string stLargInd = myReader.GetString(3);
						if (!string.IsNullOrEmpty(stLargInd))
						{
							if (stLargInd.Trim().ToUpper() == "L")
							{
								inmsg.Mtyp = myReader.GetString(4);
							}
						}
						arInmsg.Add(inmsg);
					}
				}
				finally
				{
					myReader.Close();
				}
			}
			catch (Exception)
			{
			}

			return arInmsg;
		}

		public static int ProcessAll(IDbConnection conn, string procById)
		{
			UtilDb udb = UtilDb.GetInstance();
			Hashtable htErrMsgType = new Hashtable();

			IDbCommand cmdSel = null;
            int count = 0;
			try
			{
				cmdSel = udb.GetCommand(conn, null);

				cmdSel.Parameters.Clear();
				//cmdSel.CommandText = string.Format("SELECT {0} FROM {1} WHERE STAT={2} ORDER BY MTYP, RCDT ASC",
				cmdSel.CommandText = string.Format("SELECT {0} FROM {1} WHERE STAT={2} ORDER BY RCDT ASC",
					FIELD_NAME_MGI,
                GetDb_Tb_MsgIn(),
					udb.PrepareParam(cmdSel, "STAT", MSG_IN_STAT_OUT));
				cmdSel.CommandType = CommandType.Text;
                
				using (IDataReader dr = cmdSel.ExecuteReader())
				{
					while (dr.Read())
					{
                        count++;
						string stType = UtilDb.GetData(dr, "MTYP");
						if (stType == null) stType = "";
						else stType = stType.Trim().ToUpper();

						if (IsMsgSeqCritical(stType) && htErrMsgType.ContainsKey(stType))
						{//Message Sequence is Critical 
							// and has error in privious message for this type.
							continue;
						}

						#region Get A Message
						InMsg inmsg = new InMsg();
						inmsg.Urno = UtilDb.GetData(dr, "URNO");
						inmsg.Message = UtilDb.GetData(dr, "MSGT");
						inmsg.Mtyp = stType;

						string stLargInd = UtilDb.GetData(dr, "LIND");
						if (!string.IsNullOrEmpty(stLargInd))
						{
							if (stLargInd.Trim().ToUpper() == "L")
							{
								inmsg.Message = UtilDb.GetData(dr, "MSGL");
							}
						}
						#endregion

						#region Process A Message
						if (!ProcessAMessage(udb, conn, inmsg, procById))
						{
							if (!htErrMsgType.ContainsKey(stType)) htErrMsgType.Add(stType, stType);
						}
						System.Threading.Thread.Sleep(10);
						#endregion
					}
				}
			}
			catch (Exception ex)
			{
				LogTraceMsg("ProcessAll:Err:" + ex.Message);
			}
			finally
			{
				udb.CloseCommand(cmdSel, false, false);
			}

			foreach (String msgType in htErrMsgType.Keys)
			{
				LogTraceMsg("ProcessAll:HasError For Message Type:" + msgType);
			}
            return count;
		}

		private static bool IsMsgSeqCritical(string msgType)
		{
			bool seqCritical = true;
			if (msgType != null)
			{
				if ((msgType == "STFS") || (msgType == "CPMS"))
				{
					seqCritical = false;
				}
				else
				{
					seqCritical = false;
				}
			}
			return seqCritical;
		}

		private static bool ProcessAMessage(UtilDb udb, IDbConnection conn, InMsg msg, string procById)
		{
			bool commitUpd = false;
			IDbCommand cmd = null;
			LogTraceMsg("MessageType:" + msg.Mtyp);
			if (msg.Mtyp == "FLIGHT")
			{
				commitUpd = DoProcessFlightMsg(conn, msg, procById);
			}
			else if (msg.Mtyp == "JOB")
			{
				commitUpd = DoProcessJobMsg(conn, msg, procById);
			}
			else
			{
				try
				{
					cmd = udb.GetCommandWithTrx(conn);
					bool procSuccess = false;

					switch (msg.Mtyp)
					{
						case "CPMS":
							procSuccess = DoProcessCpmMsg(cmd, msg.Message, "CPMS");
							break;
						case "BRSS":
							procSuccess = DoProcessCpmMsg(cmd, msg.Message, "BRSS");
							break;
						case "BRS":
							procSuccess = DoProcessCpmMsg(cmd, msg.Message, "BRS");
							break;
						case "TRIP":
							LogMsg("Not implemented :TRIP");
							break;
						case "STFS":
							procSuccess = DoProcessStaffMsg(cmd, msg, procById);
							break;
						default:
							break;
					}
					if (procSuccess)
					{
						commitUpd = UpdProcIndForInMessage(udb, cmd, msg.Urno, procById, "");
					}
				}
				catch (Exception ex)
				{
					LogTraceMsg("ProcessAMessage:Err:" + msg.Urno + "," + ex.Message);
				}
				finally
				{
					udb.CloseCommand(cmd, commitUpd, false);
				}
			}

			return commitUpd;
		}

		#region old Process
		/// <summary>
		/// Process all the incoming messages.
		/// </summary>
		/// <param name="conn"></param>
		/// <param name="procById"></param>
		//public static void ProcessAll(IDbConnection conn, string procById)
		//{
		//    UtilDb udb = UtilDb.GetInstance();
		//    Hashtable htErrMsgType = new Hashtable();

		//    #region Get Message To Process
		//    IDbCommand cmdSel = udb.GetCommand(false);
		//    List<InMsg> arrMsg = SelectMessageToProcess(udb, cmdSel);
		//    udb.CloseCommand(cmdSel, false, false);
		//    #endregion

		//    foreach (InMsg msg in arrMsg)
		//    {
		//        bool commitUpd = false;
		//        IDbCommand cmd = null;

		//        #region Skip to process the message for critical message when previously there has error for the same type.
		//        if ((msg.Mtyp == "STFS") || (msg.Mtyp == "CPMS"))
		//        {//Sequence of message is not import for this message type
		//        }
		//        else
		//        {
		//            if (htErrMsgType.ContainsKey(msg.Mtyp))
		//            {//Sequence of message is import for this message type. 
		//                //Do not proceed if previous message has error
		//                continue;
		//            }
		//        }
		//        #endregion

		//        bool procSuccess = false;

		//        try
		//        {
		//            cmd = udb.GetCommandWithTrx(conn);
		//            switch (msg.Mtyp)
		//            {
		//                case "FLIGHT":							
		//                    procSuccess = DoProcessFlightMsg(cmd, msg, procById);
		//                    break;
		//                case "JOB":
		//                    procSuccess = DoProcessJobMsg(cmd, msg.Message);
		//                    break;
		//                case "CPMS":
		//                    procSuccess = DoProcessCpmMsg(cmd, msg.Message, "CPMS");
		//                    break;
		//                case "BRSS":
		//                    procSuccess = DoProcessCpmMsg(cmd, msg.Message, "BRSS");
		//                    break;
		//                case "BRS":
		//                    procSuccess = DoProcessCpmMsg(cmd, msg.Message, "BRS");
		//                    break;
		//                case "TRIP":
		//                    LogMsg("Not implemented :TRIP");
		//                    break;
		//                case "STFS":
		//                    procSuccess = DoProcessStaffMsg(cmd, msg, procById);
		//                    break;
		//                default:
		//                    break;
		//            }
		//            if (procSuccess)
		//            {
		//                commitUpd = UpdProcIndForInMessage(udb, cmd, msg.Urno, procById, "");
		//            }

		//        }
		//        catch (Exception ex)
		//        {
		//            commitUpd = false;
		//            LogMsg("Process All:Err:" + ex.Message);
		//        }
		//        finally
		//        {
		//            if (!commitUpd)
		//            {
		//                if (!htErrMsgType.ContainsKey(msg.Mtyp)) htErrMsgType.Add(msg.Mtyp, msg.Mtyp);
		//            }
		//            udb.CloseCommand(cmd, commitUpd, false);
		//        }
		//    }

		//    foreach (String msgType in htErrMsgType.Keys)
		//    {
		//        LogMsg("ProcessAll:HasError For Message Type:" + msgType);
		//    }
		//}
		#endregion


		#endregion

		#region Process By Message Type
        //private static bool DoProcessJobMsg(IDbCommand cmd, string message)
        //{
        //    return CtrlJob.ProcessInMsgs(cmd, message,);
        //}

		private static bool DoProcessCpmMsg(IDbCommand cmd, string message, string messageType)
		{
			return CtrlCPM.ProcessInMsgs(cmd, message, messageType);
		}



        private static bool DoProcessJobMsg(IDbConnection conn, InMsg msg, string procById)
        {
            UtilDb udb = UtilDb.GetInstance();
            bool commit = false;

            IDbCommand cmd = null;
            
			//string msgWoCmd = "";
            string flId = "";
            string turnFlId = "";
            string messageType = "";
            string staffId = "";
            string msgToSend = "";

            try
            {
                cmd = udb.GetCommandWithTrx(conn);

                CtrlJob.ProcessInMsgs(cmd, msg.Message,out staffId,out flId,out turnFlId,out messageType,out msgToSend);
                
                commit = UpdProcIndForInMessage(udb, cmd, msg.Urno, procById, "");
            }
            catch (Exception ex)
            {
                commit = false;
                LogTraceMsg("DoProcessJobMsg:Err:" + msg.Urno + "," + ex.Message);
            }
            finally
            {
                udb.CloseCommand(cmd, commit, false);
                if (commit)
                {
                    if (messageType == "UPDATE")
                    
                    {
                        if (flId != "" && staffId != "")
                        {
                            CtrlWAPPush.PushJobUpdateChangesMsgAlertToAO(staffId, flId);
                            if (turnFlId != "")
                            {
                                CtrlWAPPush.PushJobUpdateChangesMsgAlertToAO(staffId, turnFlId);
                            }
                        }
                    }

                    if (messageType == "INSERT")
                    {
                        CtrlWAPPush.PushJobXMLChangesMsgAlertToAO(msgToSend, flId);
                    }
                }
            }
            return commit;
        }
		private static bool DoProcessFlightMsg(IDbConnection conn, InMsg msg, string procById)
		{
			UtilDb udb = UtilDb.GetInstance();
			bool commit = false;

			IDbCommand cmd = null;
			string flightMsgType = "";
			string msgWoCmd = "";
            string flId = "";
            DSFlight dsFlight = new DSFlight();

			try
			{
				cmd = udb.GetCommandWithTrx(conn);

				CtrlFlightOrg.GetInstance().ProcessOrgFlightInfo(cmd, msg.Message, procById, out flightMsgType, out msgWoCmd,out flId);
                dsFlight=CtrlFlight.RetrieveFlightsByFlightId(flId);
				commit = UpdProcIndForInMessage(udb, cmd, msg.Urno, procById, "");
			}
			catch (Exception ex)
			{
				commit = false;
				LogTraceMsg("DoProcessFlightMsg:Err:" + msg.Urno + "," + ex.Message);
			}
			finally
			{
				udb.CloseCommand(cmd, commit, false);
				if (commit)
				{
                    if (flightMsgType == "UPDATE")
                    {
                        LogTraceMsg("flightMsgType" + msgWoCmd + ":" + flId);
                        CtrlWAPPush.PushFlightXMLChangesMsgAlertToAO(msgWoCmd,flId,dsFlight);
                    }
				}
			}
			return commit;
		}

		private static bool DoProcessStaffMsg(IDbCommand cmd, InMsg msg, string procById)
		{
			bool success = false;
			try
			{
				Ctrl.CtrlStaff.UploadInterfaceData(cmd, msg.Message, procById);
				success = true;
			}
			catch (Exception ex)
			{
				LogMsg("DoProcessStaffMsg:Err:" + msg.Urno + "," + ex.Message);
			}
			return success;
		}
		#endregion

		private static void LogMsg(string msg)
		{
			try
			{
				MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("CMsgIn", msg);
			}
			catch (Exception)
			{
				UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("CMsgIn", msg);
			}
		}
	}

	public class InMsg
	{
		private string _urno;

		public string Urno
		{
			get { return _urno; }
			set { _urno = value; }
		}
		private string _message;

		public string Message
		{
			get { return _message; }
			set { _message = value; }
		}
		private string _mtyp;

		public string Mtyp
		{
			get { return _mtyp; }
			set { _mtyp = value; }
		}

	}
}