using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using UFIS.ITrekLib.DB;
using UFIS.ITrekLib.Util;
using System.Data;
using System.Collections;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.Misc;
using MM.UtilLib;

namespace UFIS.ITrekLib.Ctrl
{
	public class CtrlTrip
	{
		public static void InsertTripsInfo(XmlDocument xmlDoc, string flightId, string sentDateTime, string sentBy, ArrayList urnoList, int unSelectedCnt)
		{

			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			IDbCommand cmd = null;
			string tripType = "";
			string tripId = "";
			int totUldCount = -1;
			try
			{
				cmd = db.GetCommand(true);
				DateTime curDt = ITrekTime.GetCurrentDateTime();
				if (!DBDTrip.GetInstance().InsertTripsInfo(cmd, xmlDoc, out tripType, curDt, out tripId, unSelectedCnt, out totUldCount))
				{
					throw new ApplicationException("Fail to Save the Timing.");
				}

				if (urnoList.Count > 0)
				{
					CtrlCPM.DoProcessUpdateSendIndicator(cmd, urnoList, flightId, sentBy);
				}

				Dictionary<EnTripTypeSendReceive, string> dictTrip = new Dictionary<EnTripTypeSendReceive, string>();

				if (tripType == "F")
				{
					dictTrip.Add(EnTripTypeSendReceive.SFTRPID, sentDateTime);
					CtrlFlight.UpdateFlightInfoForTrip(cmd, flightId, dictTrip, totUldCount, sentBy, curDt);
				}
				else if (tripType == "L")
				{
					dictTrip.Add(EnTripTypeSendReceive.SLTRPID, sentDateTime);
					CtrlFlight.UpdateFlightInfoForTrip(cmd, flightId, dictTrip, totUldCount, sentBy, curDt);
				}
				else if (tripType == "FL")
				{
					dictTrip.Add(EnTripTypeSendReceive.SFTRPID, sentDateTime);
					dictTrip.Add(EnTripTypeSendReceive.SLTRPID, sentDateTime);
					CtrlFlight.UpdateFlightInfoForTrip(cmd, flightId, dictTrip, totUldCount, sentBy, curDt);
				}
				else
				{
					CtrlFlight.UpdateFlightInfoForTrip(cmd, flightId, dictTrip, totUldCount, sentBy, curDt);
				}
				commit = true;
			}
			catch (Exception ex)
			{
				LogTraceMsg("InsertTripsInfo:Err:" + ex.Message);
				throw;
			}
			finally
			{
				//db.CloseCommand(cmd, commit);
				CtrlFlight.CloseCmdAndRefreshFlightAfterUpdFlightInfo(cmd, commit, flightId);
			}
		}

		public static void ReceiveTrip(string flightId, List<string> urnoList, 
			string sentDateTime, string sentBy, EnTripType tripType)
		{
			UpdateTripsReceived(urnoList, sentDateTime, sentBy, tripType, flightId);
		}

		private static string GenXmlForUldsandUpdateIndicator(EntCPMList cpmList, string flightId, string sentBy, out ArrayList urnoList, DSTrip dsTrip)
		{
			StringBuilder xml = new StringBuilder();
			int cnt = cpmList.Count;
			LogTraceMsg("GenXmlForUldsandUpdateIndicator:Cnt:" + cnt);
			urnoList = new ArrayList();
			//DSCpm dsCpm = new DSCpm();
			//dsCpm = CtrlCPM.RetrieveSentCPMUldForAFlightForTrips(flightId);


			for (int i = 0; i < cnt; i++)
			{
				EntCPM cpm = cpmList[i];
				
				//  DSTrip.TRIPRow tripRow = null;
				int tripRowLength = 0;
				if (!cpm.AllowToChangeContainerNo)
				{
					if (dsTrip.TRIP.Rows.Count != 0)
					{
						try
						{
							tripRowLength = ((DSTrip.TRIPRow[])(dsTrip.TRIP.Select("ULDN='" + iTrekUtils.iTUtils.removeBadCharacters(cpm.ContainerNo) + "'"))).Length;
							//LogTraceMsg(" tripRowLength");
						}
						catch (Exception ex)
						{
							LogTraceMsg("tripRow Null" + ex.Message);
						}
					}
				}
				LogTraceMsg("GenXmlForUldsandUpdateIndicator:" + i + "," + cpm.Urno + "," + tripRowLength);
				if (tripRowLength == 0)
				{
					xml.Append("<ULD>");//(one ULD)
					xml.Append("<ULDN>" + iTrekUtils.iTUtils.removeBadCharacters(cpm.ContainerNo) + "</ULDN>");//(ULD Number)
					xml.Append("<RMK>" + RemoveExcessStringInRemarks(iTrekUtils.iTUtils.removeBadCharacters(cpm.RemarkText)) + "</RMK>");//(Remark)
					xml.Append("<DES>" + cpm.Destination + "</DES>");//(Destination)
					if (cpm.AllowToChangeContainerNo)
					{
						xml.Append("<ISCPM>N</ISCPM>");//(Is CPM/Free, Y  CPM, Otherwise  Free)
					}
					else
					{
						xml.Append("<ISCPM>Y</ISCPM>");//(Is CPM/Free, Y  CPM, Otherwise  Free)
						urnoList.Add(cpm.Urno);
					}

					xml.Append("</ULD>");
				}

			}
			//if (urnoList.Count > 0)
			//{

			//    CtrlDCpm.DoProcessUpdateSendIndicator(urnoList, flightId,sentBy);
			//}
			LogTraceMsg("GenXmlForUldsandUpdateIndicator:Xml:" + xml.ToString());
			return xml.ToString();
		}

		private static string RemoveExcessStringInRemarks(string rmkMessage)
		{
			if (rmkMessage.Length > 255)
			{
				rmkMessage = rmkMessage.Substring(0, 255);
			}
			return rmkMessage;

		}
		/// <summary>
		/// Update the Received Trips Details
		/// </summary>
		/// <param name="cpmList">List of TripDetails Urno</param>
		/// <param name="datetime"></param>
		/// <param name="sentBy">Received By</param>
		/// <param name="stTripType">'F' 'L'</param>
		/// <param name="flnu">Flight Urno</param>
		public static void UpdateTripsReceived(List<string> cpmList, 
			string datetime, string sentBy, EnTripType enTripType,
			string flightId)
		{
			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			IDbCommand cmd = null;
			string tripType = "";
			
			try
			{
				cmd = db.GetCommand(true);

				DateTime curDt = ITrekTime.GetCurrentDateTime();
				string stTripType = GetRecvTripTypeString(flightId, cpmList, enTripType); ;
				if (!DBDTrip.GetInstance().UpdateTripsReceived(cmd, cpmList, datetime, sentBy, stTripType, flightId, out tripType, curDt))
				{
					throw new ApplicationException("Fail to Save the Timing.");
				}

				Dictionary<EnTripTypeSendReceive, string> dictTrip = new Dictionary<EnTripTypeSendReceive, string>();
				if (tripType == "F")
				{
					dictTrip.Add(EnTripTypeSendReceive.RFTRPID, datetime);
					CtrlFlight.UpdateFlightInfoForTrip(cmd, flightId, dictTrip, null, sentBy, curDt);
				}
				else if (tripType == "L")
				{
					dictTrip.Add(EnTripTypeSendReceive.RLTRPID, datetime);
					CtrlFlight.UpdateFlightInfoForTrip(cmd, flightId, dictTrip, null, sentBy, curDt);
				}
				else if (tripType == "FL")
				{
					dictTrip.Add(EnTripTypeSendReceive.RFTRPID, datetime);
					dictTrip.Add(EnTripTypeSendReceive.RLTRPID, datetime);
					CtrlFlight.UpdateFlightInfoForTrip(cmd, flightId, dictTrip, null, sentBy, curDt);
				}
				commit = true;
			}
			catch (Exception)
			{
				throw;
			}
			finally
			{
				//db.CloseCommand(cmd, commit);
				CtrlFlight.CloseCmdAndRefreshFlightAfterUpdFlightInfo(cmd, commit, flightId);
			}
		}

		//public static void UpdateTripsReceived(List<string> cpmList, string datetime, string sentBy, string stTripType, string flnu, string adid)
		//{
		//    UtilDb db = UtilDb.GetInstance();
		//    bool commit = false;
		//    IDbCommand cmd = null;
		//    string tripType = "";
		//    try
		//    {
		//        cmd = db.GetCommand(true);
		//        DateTime curDt = ITrekTime.GetCurrentDateTime();
		//        ArrayList al = new ArrayList(cpmList);
		//        if (!DBDTrip.GetInstance().UpdateTripsReceived(cmd, al, datetime, sentBy, stTripType, flnu, out tripType, curDt))
		//        {
		//            throw new ApplicationException("Fail to Save the Timing.");
		//        }
		//        if (adid == "A")
		//        {
		//            Dictionary<EnTripTypeSendReceive, string> dictTrip = new Dictionary<EnTripTypeSendReceive, string>();
		//            if (tripType == "F")
		//            {
		//                dictTrip.Add(EnTripTypeSendReceive.RFTRPID, datetime);
		//                CtrlFlight.UpdateFlightInfoForTrip(cmd, flnu, dictTrip, null, sentBy, curDt);

		//            }
		//            else if (tripType == "L")
		//            {
		//                dictTrip.Add(EnTripTypeSendReceive.RLTRPID, datetime);
		//                CtrlFlight.UpdateFlightInfoForTrip(cmd, flnu, dictTrip, null, sentBy, curDt);
		//            }
		//        }
		//        commit = true;
		//    }
		//    catch (Exception)
		//    {

		//        throw;
		//    }
		//    finally
		//    {
		//        db.CloseCommand(cmd, commit);
		//    }
		//}

		//public static bool NeedToUpdateLastTrip(string flightId)
		//{
		//    bool needToUpdate = false;
		//    lstTripDateTime = "";
		//    try
		//    {
		//        DSTrip dsTrip = RetrieveTrip(flightId);
		//        if (dsTrip != null && dsTrip.TRIP.Rows.Count > 0)
		//        {
		//            int rtpNo = -1;

		//            try
		//            {
		//                rtpNo = Int32.Parse(dsTrip.TRIP[0].RTRPNO);
		//            }
		//            catch (Exception)
		//            {
		//            }
		//            /*To remove the checking of rTrip==1 when automation of trips is implemented*/
		//            if (rtpNo == 1 && RetrieveTripNotReceived(flightId).TRIP.Count == 0)
		//            {
		//                needToUpdate = true;
		//                //try
		//                //{
		//                //    lstTripDateTime = ((DSTrip.TRIPRow)dsTrip.TRIP.Select("RTRPNO='1'")[0]).RCV_DT;
		//                //}
		//                //catch (Exception)
		//                //{
		//                //}
		//                //if (lstTripDateTime != "")
		//                //{
		//                //    //UpdateReceiveLastTripIdForTrpMst(flightId,userId);
		//                //    needToUpdate = true;
		//                //}
		//            }
		//        }

		//    }
		//    catch (Exception)
		//    {
		//    }
		//    return needToUpdate;
		//}

		//public static void UpdateTripTimingForSend(string flnu, string dateTime, string tripType)
		//{

		//    UtilDb db = UtilDb.GetInstance();
		//    bool commit = false;
		//    IDbCommand cmd = null;
		//    try
		//    {
		//        cmd = db.GetCommand(true);
		//        if (!DBDTrip.GetInstance().UpdateTripTimingForSend(cmd, flnu,dateTime,tripType))
		//        {
		//            throw new ApplicationException("Fail to Save the Timing.");
		//        }
		//        commit = true;
		//    }
		//    catch (Exception)
		//    {

		//        throw;
		//    }
		//    finally
		//    {
		//        db.CloseCommand(cmd, commit);
		//    }
		//}


		public static void UpdateTripTiming(string flnu, Dictionary<ENT.EnTripTypeSendReceive, string> htTrip, string userId)
		{

			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			IDbCommand cmd = null;
			try
			{
				cmd = db.GetCommand(true);
				DateTime curDt = ITrekTime.GetCurrentDateTime();
				if (!DBDTrip.GetInstance().UpdateTripTiming(cmd, flnu, htTrip, userId, curDt))
				{
					throw new ApplicationException("Fail to Save the Timing.");
				}
				commit = true;
			}
			catch (Exception ex)
			{
				LogTraceMsg("UpdateTripTiming:Err:" + ex.Message);
				throw;
			}
			finally
			{
				//db.CloseCommand(cmd, commit);
				CtrlFlight.CloseCmdAndRefreshFlightAfterUpdFlightInfo(cmd, commit, flnu);
			}
		}


		//public static void UpdateTripTimingForRcv(string flnu, string dateTime, string tripType)
		//{

		//    UtilDb db = UtilDb.GetInstance();
		//    bool commit = false;
		//    IDbCommand cmd = null;
		//    try
		//    {
		//        cmd = db.GetCommand(true);
		//        if (!DBDTrip.GetInstance().UpdateTripTimingForRcv(cmd, flnu, dateTime, tripType))
		//        {
		//            throw new ApplicationException("Fail to Save the Timing.");
		//        }
		//        commit = true;
		//    }
		//    catch (Exception)
		//    {

		//        throw;
		//    }
		//    finally
		//    {
		//        db.CloseCommand(cmd, commit);
		//    }
		//}



		public static void UpdateSendLastTripIdForTrpMst(string flnu, string userId, string selOption, string optionTime)
		{
			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			IDbCommand cmd = null;
			try
			{
				cmd = db.GetCommand(true);
				DateTime curDt = ITrekTime.GetCurrentDateTime();
				string tripType = "";
				string trDt = "";
				if (!DBDTrip.GetInstance().UpdateSendLastTripIdForTrpMst(cmd, flnu, userId, curDt, out tripType, out trDt))
				{
					throw new ApplicationException("Fail to Save the Timing.");
				}
				Hashtable htTimings = new Hashtable();
				if (selOption == "UNLOAD-END")
				{
					htTimings.Add(selOption, optionTime);
				}
				//LogTraceMsg("tripType" + tripType);
				//LogTraceMsg("selOption" + selOption);
				// LogTraceMsg("trDt" + trDt);
				if (tripType == "L" && trDt != "" && selOption == "UNLOAD-END")
				{
					//LogTraceMsg("INSIDE UpdateSendLastTripIdForTrpMst");
					htTimings.Add("LTRIP", trDt);
				}
				if (htTimings.Count != 0)
				{
					CtrlFlight.UpdateArrivalFlightTiming(cmd, flnu, htTimings, userId);
				}

				commit = true;
			}
			catch (Exception ex)
			{
				LogTraceMsg("UpdateSendLastTripIdForTrpMst:Err:" + ex.Message);
				throw;
			}
			finally
			{
				//db.CloseCommand(cmd, commit);
				CtrlFlight.CloseCmdAndRefreshFlightAfterUpdFlightInfo(cmd, commit, flnu);
			}
		}


		public static void UpdateReceiveLastTripIdForTrpMst(IDbCommand cmd, string flnu, string userId)
		{
			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			bool isNewCmd = false;
			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = db.GetCommand(true);
				}

				DateTime curDt = ITrekTime.GetCurrentDateTime();
				string tripType = "";
				string trDt = "";
				if (!DBDTrip.GetInstance().UpdateReceiveLastTripIdForTrpMst(cmd, flnu, userId, curDt, out tripType, out trDt))
				{
					throw new ApplicationException("Fail to Save the Timing.");
				}
				//Dictionary<EnTripTypeSendReceive, string> dictTrip = new Dictionary<EnTripTypeSendReceive, string>();

				//if (tripType == "L")
				//{
				//    dictTrip.Add(EnTripTypeSendReceive.RLTRPID, trDt);
				//    CtrlFlight.UpdateFlightInfoForTrip(cmd, flnu, dictTrip, null, userId, curDt);

				//}
				commit = true;
			}
			catch (Exception ex)
			{
				LogTraceMsg("UpdateReceiveLastTripIdForTrpMst:Err:" + ex.Message);
				throw;
			}
			finally
			{
				if (isNewCmd)
				{
					//db.CloseCommand(cmd, commit);
					CtrlFlight.CloseCmdAndRefreshFlightAfterUpdFlightInfo(cmd, commit, flnu);
				}
			}
		}


		public static DSTrip RetrieveTrip(string flightUrno)
		{
			DSTrip ds = null;
			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			IDbCommand cmd = null;
			try
			{
				cmd = db.GetCommand(false);
				ds = DBDTrip.RetrieveTripForAFlight(cmd, flightUrno);
			}
			catch (Exception ex)
			{
				LogTraceMsg("RetrieveTrip:Err:" + ex.Message);
				throw;
			}
			finally
			{
				db.CloseCommand(cmd, commit);
			}

			return ds;
		}


		public static DSTrip RetrieveTripForAFlightReceived(string flightUrno)
		{
			DSTrip ds = null;
			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			IDbCommand cmd = null;
			try
			{
				cmd = db.GetCommand(false);
				ds = DBDTrip.GetInstance().RetrieveTripForAFlightReceived(cmd, flightUrno);
				int cnt = ds.TRIP.Count;
				string st = ds.TRIP.Rows[0].ItemArray.GetValue(0).ToString();
			}
			catch (Exception ex)
			{
				LogTraceMsg("RetrieveTripForAFlightReceived:Err:" + ex.Message);
				throw;
			}
			finally
			{
				db.CloseCommand(cmd, commit);
			}

			return ds;
		}


		public static DSTrip RetrieveTripNotReceived(string flightUrno)
		{
			DSTrip ds = null;
			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			IDbCommand cmd = null;
			try
			{
				cmd = db.GetCommand(false);
				ds = DBDTrip.GetInstance().RetrieveTripForAFlightNotReceived(cmd, flightUrno);
			}
			catch (Exception)
			{
				throw;
			}
			finally
			{
				db.CloseCommand(cmd, commit);
			}

			return ds;
		}

		public static DSTrip RetrieveTripWithMaxCnt(string flightUrno)
		{
			DSTrip ds = null;
			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			IDbCommand cmd = null;
			try
			{
				cmd = db.GetCommand(false);
				ds = DBDTrip.GetInstance().RetrieveTripForAFlightWithMaxCnt(cmd, flightUrno);
			}
			catch (Exception)
			{
				throw;
			}
			finally
			{
				db.CloseCommand(cmd, commit);
			}

			return ds;
		}


		public static DSTrip RetrieveTripforTripUrno(string flightUrno, string urno)
		{
			DSTrip ds = null;
			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			IDbCommand cmd = null;
			try
			{
				cmd = db.GetCommand(false);
				ds = DBDTrip.RetrieveTripForAFlightWithTripUrno(cmd, flightUrno, urno);
			}
			catch (Exception)
			{
				throw;
			}
			finally
			{
				db.CloseCommand(cmd, commit);
			}

			return ds;
		}
		public static DataTable RetrieveUnSentTripsInfo(string flightUrno)
		{
			DataTable dt = new DataTable();
			dt.Columns.Add("ULDNO");
			dt.Columns.Add("ULDINFO");

			DSTrip dsTrip = RetrieveTrip(flightUrno);
			foreach (DSTrip.TRIPRow row in dsTrip.TRIP.Select("", "ISCPM_FORSORT,STRPNO,RTRPNO,ULDN"))
			{
				string st = row.ULDN;
				//string sentDt = GetShortTime(row.SENT_DT);
				//string rcvDt = GetShortTime(row.RCV_DT);
				//if (sentDt != "") st += "(Sent:" + sentDt + ")";
				//if (rcvDt != "") st += "(Received:" + rcvDt + ")";
				dt.Rows.Add(row.ULDN, row.CPM_INFO);
			}
			dt.AcceptChanges();
			return dt;
		}

		public static DataTable RetrieveUnSentCPMandTripsInfo(string flightUrno)
		{
			DataTable dt = new DataTable();
			dt.Columns.Add("ULDNO");
			dt.Columns.Add("ULDINFO");

			DSCpm dsCpm = CtrlCPM.RetrieveUnSentCPMUldForAFlight(flightUrno);
			foreach (DSCpm.CPMDETRow row in dsCpm.CPMDET.Select("", "POSF,DSSN,ULDN"))
			{
				dt.Rows.Add(row.ULDN, row.ULDN);
			}

			DSTrip dsTrip = RetrieveTrip(flightUrno);
			foreach (DSTrip.TRIPRow row in dsTrip.TRIP.Select("", "ISCPM_FORSORT,STRPNO,RTRPNO,ULDN"))
			{
				string st = row.ULDN;
				//string sentDt = GetShortTime(row.SENT_DT);
				//string rcvDt = GetShortTime(row.RCV_DT);
				//if (sentDt != "") st += "(Sent:" + sentDt + ")";
				//if (rcvDt != "") st += "(Received:" + rcvDt + ")";

				dt.Rows.Add(row.ULDN, row.CPM_INFO);
			}
			dt.AcceptChanges();
			return dt;
		}
		public const string FIRST_TRIP_IND = "F";
		public const string LAST_TRIP_IND = "L";
		public const string NEXT_TRIP_IND = "N";

		public static void SaveTripAndUpdateIndicatorForBrs(string flightId,
		EntCPMList cpmList, string sentDateTime, string sentBy, int uldBalCnt)
		{
			string stTripType = "";
			EnTripType tripType = EnTripType.NextTrip;
			DSTrip dsTrip = CtrlTrip.RetrieveTrip(flightId);

			if (uldBalCnt == 0)
			{
				tripType = EnTripType.LastTrip;
			}

			switch (tripType)
			{
				case EnTripType.FirstTrip:
					stTripType = FIRST_TRIP_IND;
					break;
				case EnTripType.LastTrip:
					stTripType = LAST_TRIP_IND;
					break;
				case EnTripType.NextTrip:
					stTripType = NEXT_TRIP_IND;
					break;
			}
			//LogTraceMsg("stTripType" + stTripType);

			string stSendRecv = "S";

			ArrayList urnoList = new ArrayList();
			StringBuilder xml = new StringBuilder();
			xml.Append("<TRIP>");
			xml.Append("<UAFT>" + flightId + "</UAFT>");//Flight UrNo
			xml.Append("<TRPTYPE>" + stTripType + "</TRPTYPE>");//(Trip Type, F  First trip, N  Next Trip, L Last Trip)
			xml.Append("<SR>" + stSendRecv + "</SR>");//(Send or Receive, S Send, R  Receive)
			xml.Append("<SRDT>" + sentDateTime + "</SRDT>");//(Send or Receive Date and Time)
			xml.Append("<SRBY>" + sentBy + "</SRBY>");//(Sent or Received by User Id)
			xml.Append("<CONT>");
			xml.Append(GenXmlForUldsandUpdateIndicator(cpmList, flightId, sentBy, out urnoList, dsTrip));
			xml.Append("</CONT>");
			xml.Append("</TRIP>");
			XmlDocument xmlDoc = new XmlDocument();
			xmlDoc.LoadXml(xml.ToString());
			InsertTripsInfo(xmlDoc, flightId, sentDateTime, sentBy, urnoList, uldBalCnt);

		}

		public static void SaveTripAndUpdateIndicator(string flightId, EntCPMList cpmList, string sentDateTime, string sentBy, bool isCPMAvail, int uldBalCnt)
		{
			//EntCPMList ls = null;            
			string stTripType = "";
			//ls = cpmList.GetSelectedList(ls);
			EnTripType tripType = EnTripType.NextTrip;
			DSTrip dsTrip = CtrlTrip.RetrieveTrip(flightId);
			//LogTraceMsg("uldBalCnt" + uldBalCnt);
			if (isCPMAvail)
			{
				if (uldBalCnt == 0)
				{
					tripType = EnTripType.LastTrip;
				}
			}
			else
			{
				uldBalCnt = 1;

				if (dsTrip == null || dsTrip.TRIP.Rows.Count == 0)
				{
					tripType = EnTripType.FirstTrip;
				}
				else
				{
					tripType = EnTripType.LastTrip;

				}
			}

			switch (tripType)
			{
				case EnTripType.FirstTrip:
					stTripType = FIRST_TRIP_IND;
					break;
				case EnTripType.LastTrip:
					stTripType = LAST_TRIP_IND;
					break;
				case EnTripType.NextTrip:
					stTripType = NEXT_TRIP_IND;
					break;

			}
			ArrayList urnoList = new ArrayList();
			string uldInfo = GenXmlForUldsandUpdateIndicator(cpmList, flightId, sentBy, out urnoList, dsTrip);
			// LogTraceMsg("uldInfo" + uldInfo);
			string stSendRecv = "S";
			if (uldInfo.Length > 0)
			{

				StringBuilder xml = new StringBuilder();
				xml.Append("<TRIP>");
				xml.Append("<UAFT>" + flightId + "</UAFT>");//Flight UrNo
				xml.Append("<TRPTYPE>" + stTripType + "</TRPTYPE>");//(Trip Type, F  First trip, N  Next Trip, L Last Trip)
				xml.Append("<SR>" + stSendRecv + "</SR>");//(Send or Receive, S Send, R  Receive)
				xml.Append("<SRDT>" + sentDateTime + "</SRDT>");//(Send or Receive Date and Time)
				xml.Append("<SRBY>" + sentBy + "</SRBY>");//(Sent or Received by User Id)
				xml.Append("<CONT>");
				xml.Append(uldInfo);
				xml.Append("</CONT>");
				xml.Append("</TRIP>");
				LogTraceMsg("xml" + xml.ToString());
				XmlDocument xmlDoc = new XmlDocument();
				xmlDoc.LoadXml(xml.ToString());

				InsertTripsInfo(xmlDoc, flightId, sentDateTime, sentBy, urnoList, uldBalCnt);
			}
		}

		public static String GetArrFlightUldAOComments(IDbCommand cmd, string flightUrno)
		{
			DSTrip ds = null;
			UtilDb udb = UtilDb.GetInstance();
			bool commit = false;
			bool isNewCmd = false;
			string uldComments = "";

			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = udb.GetCommand(false);
				}

				ds = DBDTrip.RetrieveTripForAFlight(cmd, flightUrno);
				ArrayList arr = new ArrayList();


				foreach (DSTrip.TRIPRow row in ds.TRIP.Select("", "STRPNO ASC,ULDN"))
				{
					string uldNo = "";
					string sentRmk = "";
					string sTripNo = "";
					string des = "";

					uldNo = (string)row.ULDN;
					try
					{
						sentRmk = (string)row.SENT_RMK;
						sTripNo = (string)row.STRPNO;
						des = (string)row.DES;
						if (des.Trim() != "") des = des.Trim() + ",";
					}
					catch { }
					//if (sentRmk != "")
					//{
					//    //arr.Add(uldNo + ", " + sentRmk);
					//    uldComments += sTripNo+ ". " + des + ", " + uldNo + ", " + sentRmk + "\n";
					//}
					//Add des (INTER/LOCAL) by PRF 56(A)
					//User want to see the trip information even though no remark. PRF 56(B)
					uldComments += sTripNo + ". " + des + uldNo + ", " + sentRmk + "\n";
				}
				//return arr;
			}
			catch (Exception ex)
			{
				LogTraceMsg("GetArrFlightUldAOComments:Err:" + ex.Message);
				throw;
			}
			finally
			{
				if (isNewCmd)
				{
					udb.CloseCommand(cmd, commit);
				}
			}
			return uldComments;
		}

		/// <summary>
		/// Retrieve Trip for a flight with given criteria
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="flightId">Flight Id</param>
		/// <param name="bagArrRecvCat">Baggage Arrival Receive Category</param>
		/// <param name="bagArrType">Bagage Arrival Destination Type</param>
		/// <returns>Dataset of the result trip</returns>
		public static DSTrip RetrieveTripForAFlight(IDbCommand cmd, string flightId,
				EnBagArrRecvCategory bagArrRecvCat, EnBagArrType bagArrType)
		{//AM 20080829
			DSTrip ds = new DSTrip();
			UtilDb udb = UtilDb.GetInstance();
			bool commit = false;
			bool isNewCmd = false;

			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = udb.GetCommand(false);
				}
				DSTrip tempDs = DBDTrip.RetrieveTripForAFlight(cmd, flightId);
				//bool found = false;
				string stSel = "(UAFT='" + flightId + "')";

				//switch (bagArrRecvCat)
				//{
				//    case EnBagArrRecvCategory.Recv:
				//        stSel += " AND ((RCV_DT IS NOT NULL) OR (RCV_DT<>''))";
				//        break;
				//    case EnBagArrRecvCategory.UnReceive:
				//        stSel += " AND ((RCV_DT IS NULL) OR (RCV_DT=''))";
				//        break;
				//    case EnBagArrRecvCategory.All:
				//        //No filter required.
				//        break;
				//    case EnBagArrRecvCategory.None:
				//        throw new ApplicationException("Nothing to select. Choose either Received or UnReceived, or All.");
				//    //break;
				//    default:
				//        throw new ApplicationException("Nothing to select. Choose either Received or UnReceived, or All.");
				//    //break;
				//}

				switch (bagArrType)
				{
					case EnBagArrType.Inter:
						stSel += " AND (DES='I')";//Inter
						break;
					case EnBagArrType.Local:
						stSel += " AND (DES='L')";//LOCAL
						break;
					case EnBagArrType.All:
						//No filter required.
						break;
					case EnBagArrType.None:
						throw new ApplicationException("Nothing to select. Choose either Inter or Local, or all.");
					//break;
					default:
						throw new ApplicationException("Nothing to select. Choose either Inter or Local, or all.");
					//break;
				}

				foreach (DSTrip.TRIPRow row in tempDs.TRIP.Select(stSel))
				{
					bool addInd = true;
					
					if ((row.RCV_DT == DBNull.Value.ToString()) || (row.RCV_DT==null))
					{
						row.RCV_DT = "";
					}
					string stRcvDt = row.RCV_DT;

					switch (bagArrRecvCat)
					{
						case EnBagArrRecvCategory.Recv:
							if (string.IsNullOrEmpty(stRcvDt)) addInd = false;
							
							break;
						case EnBagArrRecvCategory.UnReceive:
							if (!string.IsNullOrEmpty(stRcvDt)) addInd = false;
							break;
						case EnBagArrRecvCategory.All:
							addInd = true;
							break;
						case EnBagArrRecvCategory.None:
							throw new ApplicationException("Nothing to select. Choose either Received or UnReceived, or All.");
						//break;
						default:
							throw new ApplicationException("Nothing to select. Choose either Received or UnReceived, or All.");
						//break;
					}

					if (addInd)
					{
						ds.TRIP.LoadDataRow(row.ItemArray, true);
					}
					//found = true;
				}
			}
			catch (Exception ex)
			{
				LogTraceMsg("RetrieveTripForAFlight:Err:" + ex.Message);
				throw;
			}
			finally
			{
				if (isNewCmd)
				{
					udb.CloseCommand(cmd, commit);
				}
			}

			return ds;
		}

		/// <summary>
		/// Get Received Trip Type String
		/// </summary>
		/// <param name="flightId">Flight Id</param>
		/// <param name="urnoList">Urno List of Containers in Trip</param>
		/// <param name="tripType">Trip Type</param>
		/// <returns>Trip Type in String</returns>
		private static string GetRecvTripTypeString(string flightId, List<string> urnoList, EnTripType tripType)
		{
			//if (tripType == EnTripType.ToAutoDefine)
			//{
			//    tripType = AutoDefineArrRecvTripType(flightId, urnoList);
			//    LogTraceMsg("AutoDefine Trip:" + flightId + ",[" +
			//       MM.UtilLib.UtilMisc.ConvListToString(urnoList, ",") + "]," + tripType);
			//}

			return ConvTripTypeToString(tripType);
		}

		/// <summary>
		/// Convert the Trip Type to String
		/// </summary>
		/// <param name="tripType">Trip Type</param>
		/// <returns>Trip Type in String</returns>
		public static string ConvTripTypeToString(EnTripType tripType)
		{
			string stTripType = "";
			switch (tripType)
			{
				case EnTripType.FirstTrip:
					stTripType = FIRST_TRIP_IND;
					break;
				case EnTripType.LastTrip:
					stTripType = LAST_TRIP_IND;
					break;
				case EnTripType.NextTrip:
					stTripType = NEXT_TRIP_IND;
					break;
				case EnTripType.FirstNLastTrip:
					stTripType = FIRST_TRIP_IND + LAST_TRIP_IND;
					break;
				case EnTripType.ToAutoDefine:
					//To auto define the trip type.
					stTripType = "*";
					//throw new ApplicationException("Undefined Trip Type.");
					break;
				default:
					throw new ApplicationException("Invalid Trip Type.");
				//break;
			}
			return stTripType;
		}

		///// <summary>
		///// Define the Arrival Flight Container Receive Trip Type automatically
		///// </summary>
		///// <param name="flightId">Flight Id</param>
		///// <param name="tripIdList">Uld Ids (currently going to acknowledge as received)</param>
		///// <returns>Trip Type (First/Last/Next Trip)</returns>
		//public static EnTripType AutoDefineArrRecvTripType(string flightId, List<string> tripIdList)
		//{
		//    //1. if no first received trip for the flight it is first trip
		//    //2. else if no sent container left for the flight, it is last trip
		//    //3. else it is next trip.
		//    EnTripType tripType = EnTripType.None;
		//    if (IsArrRecvFirstTrip(flightId, tripIdList)) tripType = EnTripType.FirstTrip;
		//    else if (IsArrRecvLastTrip(flightId, tripIdList)) tripType = EnTripType.LastTrip;
		//    else tripType = EnTripType.NextTrip;
		//    return tripType;
		//}

		///// <summary>
		///// Is First Trip for Receiving Container
		///// </summary>
		///// <param name="flightId">Flight Id</param>
		///// <param name="urnoList">Container List To send</param>
		///// <returns></returns>
		//private static bool IsArrRecvFirstTrip(string flightId, List<string> tripIdList)
		//{
		//    bool firstTrip = false;
		//    DateTime dtTime;
		//    firstTrip = !CtrlFlight.IsArrFlightTimeExist(null, flightId, CtrlFlight.EnmArrFlightTimeType.ArrBagFirstTripTime, out dtTime);
		//    return firstTrip;
		//}

		///// <summary>
		///// Is it the Last Trip for Receiving Container
		///// </summary>
		///// <param name="flightId">Flight Id</param>
		///// <param name="urnoList">Container List To send</param>
		///// <returns></returns>
		//private static bool IsArrRecvLastTrip(string flightId, List<string> tripIdList)
		//{
		//    bool lastTrip = false;
		//    DateTime dtTime;
		//    bool isAprSentLastTripExist = CtrlFlight.IsArrFlightTimeExist(null, flightId, CtrlFlight.EnmArrFlightTimeType.ArrAprLastTripTime, out dtTime);
		//    if (isAprSentLastTripExist)
		//    {
		//        //Check any container left out to receive?
		//        EntBagArrContainer entBAContainer = CtrlBagArrival.GetTripInfo(flightId, EnBagArrType.All);
		//        Hashtable ht = entBAContainer.UnReceivedTripList.GetUnRecvUlds();
		//        if (ht == null) throw new ApplicationException("No unreceived containers.");

		//        //Filter the duplicate in Receive Urno List - Start
		//        Hashtable htRecv = new Hashtable();
		//        int cntRecv = tripIdList.Count;
		//        for (int i = cntRecv - 1; i >= 0; i--)
		//        {
		//            if (htRecv.ContainsKey(tripIdList[i])) tripIdList.RemoveAt(i);
		//            else htRecv.Add(tripIdList[i], tripIdList[i]);
		//        }
		//        //Filter the duplicate in Receive Urno List - End

		//        cntRecv = tripIdList.Count;

		//        int cntUnRecv = ht.Count;
		//        if (cntRecv == cntUnRecv)
		//        {
		//            lastTrip = true;
		//            for (int i = 0; i < cntRecv; i++)
		//            {
		//                string id = tripIdList[i];
		//                if (!ht.ContainsKey(id))
		//                {
		//                    lastTrip = false;
		//                    break;
		//                }
		//            }
		//        }
		//    }

		//    return lastTrip;
		//}

		//public static string GetTripType(string flightId, List<string> urnoList)
		//{
		//    LogTraceMsg("GetTripType:FlightId:" + flightId + "," + UtilMisc.ConvListToString(urnoList, ","));
		//    string stType = "N";
		//    DSTrip ds = CtrlTrip.RetrieveTripForAFlight(null, flightId, EnBagArrRecvCategory.All, EnBagArrType.All);
		//    if ((ds == null) || (ds.TRIP.Rows.Count < 1))
		//    {
		//        stType = "F";
		//        LogTraceMsg("GetTripType:NoTrip");
		//    }
		//    else
		//    {
		//        DSTrip.TRIPRow[] rows = (DSTrip.TRIPRow[])ds.TRIP.Select("RCV_DT IS NULL OR RCV_DT=''");
		//        if ((rows == null) || (rows.Length < 1))
		//        {
		//            stType = "F";
		//            LogTraceMsg("GetTripType:NoRecv");
		//        }
		//        else
		//        {
		//            LogTraceMsg("GetTripType:HaveRecv");
		//            DSFlight dsFlight = CtrlFlight.RetrieveFlightsByFlightId(flightId);
		//            DSFlight.FLIGHTRow fRow = (DSFlight.FLIGHTRow)dsFlight.FLIGHT.Rows[0];
		//            if (fRow.CLOSEDFLIGHTS == "Y")
		//            {
		//                LogTraceMsg("GetTripType:ClosedFlight");
		//                bool hasAll = true;
		//                foreach (DSTrip.TRIPRow row in ds.TRIP.Select("RCV_DT IS NULL OR RCV_DT=''"))
		//                {
		//                    LogTraceMsg("GetTripType:HasRcv:" + row.URNO);
		//                    if (!urnoList.Contains(row.URNO))
		//                    {
		//                        LogTraceMsg("GetTripType:HasRcv:No Contain:" + row.ULDN);
		//                        hasAll = false;
		//                        break;
		//                    }
		//                }
		//                if (hasAll)
		//                {
		//                    DSTrip.TRIPRow[] rows2 = (DSTrip.TRIPRow[])ds.TRIP.Select("RCV_DT IS NULL OR RCV_DT=''");
		//                    if ((rows2 == null) || (rows2.Length < 1))
		//                    {
		//                        stType = "FL";
		//                        LogTraceMsg("GetTripType:NoRecv");
		//                    }
		//                    else stType = "L";
		//                }
		//            }
		//        }
		//    }
		//    LogTraceMsg("GetTripType:stType:" + stType);
		//    return stType;
		//}


		private static void LogTraceMsg(string msg)
		{
			MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("CtrlTrip", msg);

		}

	}
}