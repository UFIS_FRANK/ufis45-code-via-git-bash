using System;
using System.Collections.Generic;
using System.Text;

using MM.UtilLib;


namespace UFIS.ITrekLib.Util
{
	public class UtilITrekRegistry
	{
		//private const string REG_ITREK = @"UFIS Airport Solutions\Itrek";
		private const string REG_ITREK_PERM = @"UFIS Airport Solutions\Itrek\Perm";
		private const string REG_ITREK_DATA = @"UFIS Airport Solutions\Itrek\Data";

		public const string REG_ITREK_PERM_ENVIRONMENT = "ENV";
		public const string REG_ITREK_PERM_DB_APP_SCHEMA = "APP_SCHEMA";
		public const string REG_ITREK_PERM_DB_DATA_SCHEMA = "DATA_SCHEMA";

		public const string REG_ITREK_DATA_BASE_DIR = "Dir";
		public const string REG_ITREK_DATA_VERSION = "Version";

		private static object _lockObject = new object();

		//Most software packages will have a registry key inside
		//HKEY_LOCAL_MACHINE->SOFTWARE
		//These keys are available no matter who is logged in and is a good place to stick general application values. Let's put a key in this folder called "My Registry Key".      
		#region Helper Methods
		public static void UpdateKey(string key, string value)
		{
			UtilRegistry.SetData(REG_ITREK_DATA, key, value, true);
		}

		public static string GetData(string key)
		{
			return UtilRegistry.GetData(REG_ITREK_DATA, key);
		}

		public static string GetData_Perm(string key)
		{
			return UtilRegistry.GetData(REG_ITREK_PERM, key);
		}

		public static bool HasData(string key, out string value)
		{
			return UtilRegistry.HasData(REG_ITREK_DATA, key, out value);
		}

		public static bool HasRegistry()
		{
			return UtilRegistry.HasRegistry(REG_ITREK_DATA);
		}

		public static void CreateRegistry()
		{
			UtilRegistry.CreateRegistry(REG_ITREK_DATA);
		}

		public static void RemoveRegistry()
		{
			UtilRegistry.RemoveRegistry(REG_ITREK_DATA);
		}
		#endregion

		#region Get ITrek Related Registry info
		#region Get/Set ITrek installation dependent Registry info
		public static string GetItrekBaseDir()
		{
			return GetData(REG_ITREK_DATA_BASE_DIR);
		}

		public static void SetItrekBaseDir(string baseDir)
		{
			UpdateKey(REG_ITREK_DATA_BASE_DIR, baseDir);
		}

		public static string GetItrekVersion()
		{
			return GetData(REG_ITREK_DATA_VERSION);
		}

		public static void SetItrekVersion(String curVersion)
		{
			UpdateKey(REG_ITREK_DATA_VERSION, curVersion);
		}
		#endregion

		#region Get/Set ITrek Permanent Registry Info for individual Server (Will not change on each installation)
		public static string GetItrekCurrentEnvironment()
		{
			return GetData_Perm(REG_ITREK_PERM_ENVIRONMENT);
		}

		public static string GetDbDataSchemaName()
		{
			return GetData_Perm(REG_ITREK_PERM_DB_DATA_SCHEMA);
		}

		public static string GetDbAppSchemaName()
		{
			return GetData_Perm(REG_ITREK_PERM_DB_APP_SCHEMA);
		}

		#endregion
		#endregion

		#region Logging
		private static void LogTraceMsg(string msg)
		{
			try
			{
				UtilTraceLog.GetInstance().LogTraceMsg("DBDStaff", msg);
			}
			catch (Exception)
			{
				UtilLog.LogToTraceFile("DBDStaff", msg);
			}
		}

		private static void LogMsg(string msg)
		{
			try
			{
				UtilTraceLog.GetInstance().LogTraceMsg("DBDStaff", msg);
			}
			catch (Exception)
			{
				UtilLog.LogToTraceFile("DBDStaff", msg);
			}
		}
		#endregion
	}
}