using System;
using System.Collections.Generic;
using System.Text;

namespace UFIS.ITrekLib.ENT.BPTRSumm
{
	public class EntBptrSumm
	{
		private int _flCnt = 0;//Flight Count
		private int _llFlCnt = 0;//Light Loader Flight Count
		private int _hlFlCnt = 0;//Heavy Loader Flight Count
		private int _fbMetCnt = 0;//First Bag Timing Met Flight Count
		private int _lbMetLlCnt = 0;//Last Bag Timing Met Light Loader Flight Count
		private int _lbMetHlCnt = 0;//Last Bag Timing Met Heavy Loader Flight Count

		public int FlightCount
		{
			get { return _flCnt; }
			//set { _flCnt = value; }
		}

		public int LightLoaderFlightCount
		{
			get { return _llFlCnt; }
		}

		public int HeavyLoaderFlightCount
		{
			get { return _hlFlCnt; }
		}

		public int FirstBagBMMetFlightCount
		{
			get { return _fbMetCnt; }
		}

		public int LastBagBMMetLightLoaderFlightCount
		{
			get { return _lbMetLlCnt; }
		}

		public int LastBagBMMetHeavyLoaderFlightCount
		{
			get { return _lbMetHlCnt; }
		}

		/// <summary>
		/// Create an Instance.
		/// </summary>
		/// <param name="flCnt">Total Flight Count</param>
		/// <param name="llFlCnt">Light Loader Flight Count</param>
		/// <param name="hlFlCnt">Heavy Loader Flight Count</param>
		/// <param name="fbBMMetCnt">First Bag Benchmark Met Flight Count</param>
		/// <param name="lbBMMetLlFlCnt">First Bag Benchmark Met Light Loader Flight Count</param>
		/// <param name="lbBMMetHlFlCnt">First Bag Benchmark Met Heavy Loader Flight Count</param>
		public EntBptrSumm(int flCnt,
			int llFlCnt, int hlFlCnt,
			int fbBMMetCnt,
			int lbBMMetLlFlCnt, int lbBMMetHlFlCnt)
		{
			_flCnt = flCnt;
			_llFlCnt = llFlCnt;
			_hlFlCnt = hlFlCnt;
			_fbMetCnt = fbBMMetCnt;
			_lbMetLlCnt = lbBMMetLlFlCnt;
			_lbMetHlCnt = lbBMMetHlFlCnt;
		}

	}

	public class EntBptrSummCond
	{
		private List<string> _TerminalIds;
		private DateTime _frDateTime;
		private DateTime _toDateTime;

		public EntBptrSummCond(List<string> lsTerminalIds,
			DateTime frDateTime, DateTime toDateTime)
		{
			_TerminalIds = new List<string>();
			if (TerminalIds != null)
			{
				_TerminalIds.AddRange(lsTerminalIds);
			}
			_frDateTime = frDateTime;
			_toDateTime = toDateTime;
		}

		public List<string> TerminalIds
		{
			get
			{
				return _TerminalIds;
			}
		}

		public DateTime FromDateTime
		{
			get {return _frDateTime; }
		}

		public DateTime ToDateTime
		{
			get { return _toDateTime; }
		}
	}
}
