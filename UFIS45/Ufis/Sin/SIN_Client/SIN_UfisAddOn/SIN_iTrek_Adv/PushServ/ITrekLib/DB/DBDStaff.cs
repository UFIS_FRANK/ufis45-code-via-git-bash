using System;
using System.Collections.Generic;
using System.Text;

using System.Data;

using UFIS.ITrekLib.Util;
using MM.UtilLib;
using UFIS.ITrekLib.CommMsg;

using iTrekXML;
using UFIS.ITrekLib.Config;

namespace UFIS.ITrekLib.DB
{
	public class DBDStaff
	{
		private const string DB_T_STAFF_C_STAT_ACTIVE = "A";
		private const string DB_T_STAFF_C_STAT_INACTIVE = "I";
		private const string DB_T_STAFF_C_DELE_DELETED = "Y";
		enum EnumStaffStatus { ACTIVE, DELETE };

		private static Object _lockObj = new Object();//to use in synchronisation of single access
		private static DBDStaff _dbStaff = null;

		private static readonly DateTime DEFAULT_DT = new System.DateTime(1, 1, 1);
		private static DateTime _dtLastRefresh = DEFAULT_DT;
		private static DateTime _dtLastUpload = DEFAULT_DT;

		private DBDStaff() { }

		public void Init()
		{//Initialize all the information, to get the latest data when fetching them again.
			_dtLastRefresh = DEFAULT_DT;
			_dtLastUpload = DEFAULT_DT;
		}

		/// <summary>
		/// Get the Instance of DBDStaff. There will be only one reference for all the clients
		/// </summary>
		/// <returns></returns>
		public static DBDStaff GetInstance()
		{
			if (_dbStaff == null)
			{
				lock (_lockObj)
				{
					if (_dbStaff == null)
					{
						_dbStaff = new DBDStaff();
					}
				}
			}
			return _dbStaff;
		}

		private string GetInString(object obj)
		{
			if (obj != null) return obj.ToString();
			else return "";
		}

		/// <summary>
		/// Load the info in the dataset coming from interface (backend) into iTrek Database
		/// </summary>
		/// <param name="cmd">Database Command</param>
		/// <param name="dsIntStaff">dataset with data coming from interface (backend)</param>
		/// <param name="userId">user Id who update this info</param>
		/// <param name="updateDateTime">update date and time</param>
		/// <param name="cntInsert">number of inserted rows</param>
		/// <param name="cntUpdate">number of updated rows</param>
		/// <param name="cntDelete">number of deleted rows</param>
		/// <param name="cntWithRecordInDb">number of row which are already existed in iTrek Database</param>
		/// <returns>Successfully load into the database or not. True-Success</returns>
		public bool LoadInfoToDb(IDbCommand cmd, DataSet dsIntStaff, string userId, DateTime updateDateTime,
		   out int cntInsert, out int cntUpdate, out int cntDelete, out int cntWithRecordInDb)
		{
			bool success = false;
			UtilDb udb = UtilDb.GetInstance();
			List<string> lsPeno = new List<string>();
			List<string> lsUstf = new List<string>();
			int cntNewId = 0;
			DateTime dt1 = DateTime.Now;

			#region Get all Ustf
			foreach (DataRow row in dsIntStaff.Tables["STF"].Rows)
			{
				string stPeno = (string)row["PNO"];
				if (!string.IsNullOrEmpty(stPeno))
				{
					lsPeno.Add(stPeno);
				}

				string stUstf = GetInString(row["URNO"]).Trim();
				if (!string.IsNullOrEmpty(stUstf))
				{
					lsUstf.Add(stUstf);
				}
			}

			#endregion
			DateTime dt2 = DateTime.Now;

			#region Get the Current Data for all the Peno from Database
			DataSet dsCur = RetrieveDataFromDb(cmd, lsPeno, lsUstf);
			cntWithRecordInDb = dsCur.Tables[0].Rows.Count;
			#endregion
			DateTime dt3 = DateTime.Now;

			#region Update/Insert the Information in DB Dataset
			foreach (DataRow row in dsIntStaff.Tables["STF"].Rows)
			{
				bool createNewRow = false;

				string stUstf = GetInString(row["URNO"]).Trim();
				string chgInd = GetInString(row["CHG"]);
				string stPeno = GetInString(row["PNO"]).Trim();

				if (!string.IsNullOrEmpty(stPeno))
				{//Has Peno Information.
					#region Process with information including PENO
					//string fName = GetInString(row["FNM"]);
					//string lName = GetInString(row["LNM"]);

					DataRow[] rowCurs = dsCur.Tables[0].Select("PENO='" + stPeno + "'");
					if (rowCurs == null || rowCurs.Length < 1)
					{//No match. New Data
						createNewRow = true;
					}
					else if (rowCurs.Length == 1)
					{						
						string stIRef = GetInString(rowCurs[0]["IREF"]);
						if (string.IsNullOrEmpty(stIRef))
						{
							rowCurs[0]["IREF"] = stUstf;
						}
						string staffId = (string)rowCurs[0]["URNO"];
						AssignExistingData(row, staffId, rowCurs[0], userId, updateDateTime, out createNewRow);
					}
					else
					{//Multiple Record with same stPeno.
						int cnt = rowCurs.Length;

						for (int i = 0; i < cnt; i++)
						{
							bool needToCreateNewRow = false;
							string staffId = (string)rowCurs[i]["URNO"];
							string stIRef = GetInString(rowCurs[i]["IREF"]);
							if ((!string.IsNullOrEmpty(stIRef)) && (!string.IsNullOrEmpty(stUstf)) && (stIRef != stUstf)) continue;
							
							if (string.IsNullOrEmpty(stIRef))
							{
								rowCurs[i]["IREF"] = stUstf;
							}

							AssignExistingData(row, staffId, rowCurs[i], userId, updateDateTime, out needToCreateNewRow);
							if (needToCreateNewRow) createNewRow = true;
						}
					}

					if (createNewRow)
					{
						string staffId = UtilDb.PFIX_VIRTUAL_NEW_ID + cntNewId;
						cntNewId++;
						if (!UtilDb.IsRecordExist(cmd, DB_Info.DB_T_STAFF, stPeno))
						{
							staffId = stPeno;//assume no duplicate.
						}
						AssignNewData(row, staffId, dsCur, userId, updateDateTime);
					}
					#endregion
				}
				else if (chgInd == "D")
				{//Deleted the record.
					#region Process Delete without PENO info
					DataRow[] rowCurs = dsCur.Tables[0].Select("IREF='" + stUstf + "'");
					if (rowCurs == null || rowCurs.Length < 1)
					{//No match. Nothing to do
					}
					else
					{
						for (int i = 0; i < rowCurs.Length; i++)
						{
							string staffId = (string)rowCurs[i]["URNO"];
							AssignExistingData(row, staffId, rowCurs[i], userId, updateDateTime, out createNewRow);
						}
					}
					#endregion
				}
			}

			#endregion
			DateTime dt4 = DateTime.Now;
			#region get the date changes (New Record and Updated Records)
			DataTable dtChgIns = dsCur.Tables[0].GetChanges(DataRowState.Added);
			DataTable dtChgUpd = dsCur.Tables[0].GetChanges(DataRowState.Modified);

			int cnt1 = dsCur.Tables[0].Rows.Count;
			//IDbDataAdapter da = GetOrgFlightDataAdapter(cmd);
			cntInsert = 0;
			cntUpdate = 0;
			cntDelete = 0;
			#endregion
			DateTime dt5 = DateTime.Now;
			#region Insert the New Records
			if (dtChgIns != null)
			{
				cntInsert = dtChgIns.Rows.Count;
				//udb.Update(da, dtChgIns);
				cntInsert = InsertDataToDb(cmd, dtChgIns, userId, updateDateTime);
			}
			#endregion
			DateTime dt6 = DateTime.Now;


			#region Update the Changed Records
			if (dtChgUpd != null)
			{
				cntUpdate = dtChgUpd.Rows.Count;
				//udb.Update(da, dtChgUpd);
				int cntToUpd = 0;
				cntUpdate = UpdateDataToDb(cmd, dtChgUpd, userId, updateDateTime, out cntToUpd);
			}
			#endregion
			DateTime dt7 = DateTime.Now;
			LogTraceMsg(string.Format(@"UploadOrgFlightDataToDb:CntXml={0},Existing={1},Insert={2}, Update={3}, Delete={4},
    PrepareInfo   :{5}, GetCur:{6}, PrepareData:{7},
    GetChangedInfo:{8}, Insert:{9}, Update     :{10},
    Total Time    :{11}",
			   dsIntStaff.Tables[0].Rows.Count,
			   cntWithRecordInDb,
			   cntInsert, cntUpdate, cntDelete,
			   UtilTime.ElapsedTime(dt1, dt2),//PrepareInfo Time
			   UtilTime.ElapsedTime(dt2, dt3),//GetCurrent Info Time
			   UtilTime.ElapsedTime(dt3, dt4),//Prepare Data Time
			   UtilTime.ElapsedTime(dt4, dt5),//Get Changed Info (New/Changed)
			   UtilTime.ElapsedTime(dt5, dt6),//Insert New Info
			   UtilTime.ElapsedTime(dt6, dt7),//Update Changed Info
			   UtilTime.ElapsedTime(dt1, dt7)//Total Time
			   ));

			return success;
		}
        private static string GetDb_Tb_Staff()
        {
            return DB_Info.GetDbFullName(DB_Info.DB_T_STAFF);
        }
		public const string DB_T_STAFF = "ITK_STAFF";
		private const string FN_SEL_STAFF = "URNO,PENO,FINM,LANM,STAT,DELE,IREF,CDAT,USEC,USEU,LSTU";
		public const string PROC_ID_TO_GEN_ID_FOR_STAFF = "ITK_STAFF";

		private DataSet RetrieveDataFromDb(IDbCommand cmd, List<string> lsPeno, List<string> lsUstf)
		{
			DataSet dsNew = new DataSet();
			dsNew.Tables.Add();
			UtilDb udb = UtilDb.GetInstance();
			string stPenos = UtilMisc.ConvListToString(lsPeno, ",", "'", "'", 0, lsPeno.Count);
			string stUstf = UtilMisc.ConvListToString(lsUstf, ",", "'", "'", 0, lsUstf.Count);

			if ((!string.IsNullOrEmpty(stUstf)) || (!string.IsNullOrEmpty(stPenos)))
			{
				cmd.CommandText = string.Format("SELECT {0} FROM {1} WHERE STAT='A'",
					  FN_SEL_STAFF,
                     GetDb_Tb_Staff());

				if ((!string.IsNullOrEmpty(stUstf)) && (!string.IsNullOrEmpty(stPenos)))
				{
					cmd.CommandText += string.Format(" AND (IREF IN ({0}) OR PENO IN ({1}))", stUstf, stPenos);
				}
				else
				{
					if (!string.IsNullOrEmpty(stUstf))
					{
						cmd.CommandText += string.Format(" AND IREF IN ({0})", stUstf);
					}
					else if (!string.IsNullOrEmpty(stPenos))
					{
						cmd.CommandText += string.Format(" AND PENO IN ({0})", stPenos);
					}	
				}				

				cmd.CommandType = CommandType.Text;
				cmd.Parameters.Clear();

				udb.FillDataTable(cmd, dsNew.Tables[0]);
				dsNew.AcceptChanges();
			}

			return dsNew;
		}

		private int InsertDataToDb(IDbCommand cmd, DataTable dt, string userId, DateTime curDt)
		{
            return UtilDb.InsertDataToDb(cmd, dt, DB_Info.DB_T_STAFF, PROC_ID_TO_GEN_ID_FOR_STAFF, userId, curDt);
		}

		private int UpdateDataToDb(IDbCommand cmd, DataTable dt,
		   string userId, DateTime curDt,
		   out int totRowCntToUpd)
		{
			totRowCntToUpd = 0;
			return UtilDb.UpdateDataToDb(cmd, dt, DB_Info.DB_T_STAFF, userId, curDt, true, out totRowCntToUpd);
		}

		/// <summary>
		/// Retrieve Staff Urno for given Peno
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="peno"></param>
		/// <returns></returns>
		public string RetrieveUrnoForPeno(IDbCommand cmd, string peno)
		{
			UtilDb udb = UtilDb.GetInstance();
			string urno = null;
			string paramNamePrefix = UtilDb.GetParaNamePrefix();
			string pnStaffId = paramNamePrefix + "PENO";
			cmd.CommandText = string.Format("SELECT {0} FROM {1} WHERE STAT='A' AND PENO = {2}",
			   "URNO",
              GetDb_Tb_Staff(),
			   pnStaffId);

			cmd.CommandType = CommandType.Text;
            cmd.Parameters.Clear();
			udb.AddParam(cmd, pnStaffId, peno);
			LogTraceMsg(" cmd.CommandText " + cmd.CommandText);
			LogTraceMsg("peno:" + peno + ".");
			urno = cmd.ExecuteScalar().ToString();
			return urno;

		}

		#region Assign Data

		/// <summary>
		/// Assign "New" Original Staff Data from backend to iTrek Staff Data
		/// </summary>
		/// <param name="rowInterface">Original Staff Data row from Backend</param>
		/// <param name="staffId">staff id for new data</param>
		/// <param name="dsCurDb">iTrek Staff Data Table</param>
		/// <param name="userId">user id who update this info</param>
		/// <param name="updateDateTime">update date and time</param>
		private void AssignNewData(DataRow rowInterface, string staffId, DataSet dsCurDb,
		   string userId, DateTime updateDateTime)
		{
			DataRow row = dsCurDb.Tables[0].NewRow();
			row["URNO"] = staffId;
			UtilDataSet.AssignData((string)rowInterface["PNO"], row, "PENO");
			UtilDataSet.AssignData((string)rowInterface["FNM"], row, "FINM");
			UtilDataSet.AssignData((string)rowInterface["LNM"], row, "LANM");
			UtilDataSet.AssignData((string)rowInterface["URNO"], row, "IREF");
			UtilDataSet.AssignData(DB_T_STAFF_C_STAT_ACTIVE, row, "STAT");

			row["LSTU"] = updateDateTime;
			row["USEU"] = userId;
			row["CDAT"] = updateDateTime;
			row["USEC"] = userId;

			dsCurDb.Tables[0].LoadDataRow(row.ItemArray, LoadOption.Upsert);
		}

		/// <summary>
		/// Assign Existing Original Staff Data from backend to iTrek Staff Data
		/// </summary>
		/// <param name="rowInterface">Original Staff Data row from Backend</param>
		/// <param name="staffId">staff id</param>
		/// <param name="row">iTrek Staff data table row</param>
		/// <param name="userId">user id who update this info</param>
		/// <param name="updateDateTime">Update date and time</param>
		private void AssignExistingData(DataRow rowInterface, string staffId, DataRow row,
		   string userId, DateTime updateDateTime, out bool createNewRow)
		{
			createNewRow = false;
			string chgInd = GetInString(rowInterface["CHG"]);
			switch (chgInd.Trim().ToUpper())
			{
				case "D"://Deleted record in backend
					UtilDataSet.AssignData(DB_T_STAFF_C_STAT_INACTIVE, row, "STAT");
					UtilDataSet.AssignData(DB_T_STAFF_C_DELE_DELETED, row, "DELE");
					break;
				case "I"://Inserted record in backend
					string fName = GetInString(rowInterface["FNM"]);
					string lName = GetInString(rowInterface["LNM"]);

					string fNameCur = GetInString(row["FINM"]);
					string lNameCur = GetInString(row["LANM"]);

					if (fName.Trim().ToUpper().Equals(fNameCur.Trim().ToUpper()) &&
					   lName.Trim().ToUpper().Equals(lNameCur.Trim().ToUpper()))
					{//Same person.
					}
					else
					{//Different Person. 
						UtilDataSet.AssignData(DB_T_STAFF_C_STAT_INACTIVE, row, "STAT");
						createNewRow = true;
					}
					break;
				case "U"://Updated the record in backend
					//UtilDataSet.AssignData(DB_T_STAFF_C_STAT_ACTIVE, row, "STAT");
					string curStat = GetInString(row["STAT"]);
					if (curStat == DB_T_STAFF_C_STAT_ACTIVE)
					{
						UtilDataSet.AssignData((string)rowInterface["FNM"], row, "FINM");
						UtilDataSet.AssignData((string)rowInterface["LNM"], row, "LANM");
					}
					break;
				case "R"://Request info from iTrek.
					UtilDataSet.AssignData((string)rowInterface["FNM"], row, "FINM");
					UtilDataSet.AssignData((string)rowInterface["LNM"], row, "LANM");
					break;
				default:
					throw new ApplicationException("Invalid Change Type.");
				//break;
			}

			if (row.RowState == DataRowState.Modified)
			{
				row["LSTU"] = updateDateTime;
				row["USEU"] = userId;
			}
		}

		#endregion

		#region Request Data from Back_end
		public static bool RequestStaffDataFromBackEnd(IDbCommand cmd, List<string> lsPenos, string userId)
		{
			bool requested = false;
			if (lsPenos.Count > 0)
			{
				string replyTo = "";
				CMsgOut.RequestStaffInfo(cmd, lsPenos, userId, out replyTo);
			}
			return requested;
		}



		private static string GetSentTo()
		{
			iTXMLPathscl path = new iTXMLPathscl();
			return path.GetFROMITREKMSGQueueName();
		}

		#endregion

		#region Logging
		private void LogTraceMsg(string msg)
		{
			try
			{
				UtilTraceLog.GetInstance().LogTraceMsg("DBDStaff", msg);
			}
			catch (Exception)
			{
				UtilLog.LogToTraceFile("DBDStaff", msg);
			}
		}

		private void LogMsg(string msg)
		{
			try
			{
				UtilTraceLog.GetInstance().LogTraceMsg("DBDStaff", msg);
			}
			catch (Exception)
			{
				UtilLog.LogToTraceFile("DBDStaff", msg);
			}
		}
		#endregion

	}
}
