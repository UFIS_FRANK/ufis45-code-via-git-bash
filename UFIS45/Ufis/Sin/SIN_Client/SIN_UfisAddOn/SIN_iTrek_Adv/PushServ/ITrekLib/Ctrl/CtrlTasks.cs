using System;
using System.Collections.Generic;
using System.Text;

using UFIS.ITrekLib.DB;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.Util;
using System.Collections;

namespace UFIS.ITrekLib.Ctrl
{
   
   public class CtrlTasks
   {
      public enum EnTaskDept{ Apron, Baggage };

      public const string TASK_APRON = "APRON";
      public const string TASK_BAGGAGE = "BAGGAGE";      
       
      private CtrlTasks() { }

      private static void CheckData(ref string tasks, ref string airlineCode)
      {
         if (string.IsNullOrEmpty(tasks)) throw new ApplicationException("No Airline Code.");
         if (string.IsNullOrEmpty(airlineCode)) throw new ApplicationException("No Baggage Type.");

         airlineCode = airlineCode.Trim().ToUpper();
         tasks = tasks.Trim().ToUpper();
         if (airlineCode == "") throw new ApplicationException("No Airline Code.");
         if (tasks == "") throw new ApplicationException("No baggage type.");
      }

      public static void Create(string tasks, string airlineCode)
      {
         DBTasks.GetInstance().Create(tasks, airlineCode);
      }

      public static ArrayList GetAirlineCode(string tasks)
      {
         return DBTasks.GetInstance().GetAirlineCode(tasks);
      }

      //public static string GetAirlineCodeInString(string tasks)
      //{
      //   return DBTasks.GetInstance().GetAirlineCodeInString(tasks);
      //}

      public static string GetAirlineCodeInString(EnTaskDept task)
      {
         string stTask = "";
         switch (task)
         {
            case EnTaskDept.Apron:
               stTask = TASK_APRON;
               break;
            case EnTaskDept.Baggage:
               stTask = TASK_BAGGAGE;
               break;
            default:
               throw new ApplicationException("Invalid Department");
         }
         return DBTasks.GetInstance().GetAirlineCodeInString(stTask);
      }


      /* public static bool IsExist(string tasks, string airlineCode)
       {
           return DBTasks.GetInstance().IsExist(tasks, airlineCode);
       }*/

      public static DSTasks RetrieveAll()
      {
         return DBTasks.GetInstance().RetrieveAll();
      }

      public static bool IsHag(string task, string airlineCode)
      {
         string alCode = airlineCode;
         string hag = task;
         CheckData(ref task, ref airlineCode);
         //string alc = "";
         DSTasks ds = RetrieveAll();
         char[] del = { ';' };

         DSTasks.HAGRow row = (DSTasks.HAGRow)ds.HAG.Select(" TASK='" + task + "'")[0];
         if (row != null)
         {
            foreach (string al in row.ALC2.Split(del))
            {
               if (al == airlineCode)
               {
                  return true;
               }
               else
               {
                  return false;
               }
            }
            return false;
         }
         else
         {
            return false;
         }
      }

      public static void CreateNewTasks(string message)
      {
         DBTasks.GetInstance().CreateNewTasks(message);
      }
   }
}