# define TestingOn

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.IO;
using System.Xml;
using System.Data;
using System.Collections;
using iTrekXML;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.Util;
using MM.UtilLib;

namespace UFIS.ITrekLib.DB
{
    public class DBCpm
    {
        private static Object _lockObj = new Object();//to use in synchronisation of single access
        private static Object _lockObjCpm = new Object();//to use in synchronisation of single access
        private static Object _lockObjCpmDet = new Object();//to use in synchronisation of single access
        private static Object _lockObjCpmDetHist = new Object();//to use in synchronisation of single access        
        private static DBCpm _dbCpm = null;

        private static readonly DateTime DEFAULT_DT = new System.DateTime(1, 1, 1);
        private static DateTime _lastCPMWriteDT = DEFAULT_DT;
        private static DateTime _lastCPMDetWriteDT = DEFAULT_DT;
        private static DateTime _lastCPMDetHistWriteDT = DEFAULT_DT;

        private static DSCpm _dsCpm = null;
        private static DSCpm _dsCpmHist = null;
        private static string _fnCpmXml = null;//file name of CPM XML in full path.
        private static string _fnCpmDetXml = null;//file name of CPM Details XML in full path.
        private static string _fnCpmDetHistXml = null;//file name of CPM Details History XML in full path.

        private static DateTime _lastRefreshDT = DEFAULT_DT;
        private static DateTime _lastRefreshDTHist = DEFAULT_DT;

        /// <summary>
        /// Singleton Pattern.
        /// Not allowed to create the object from other class.
        /// To get the object, user "GetInstance()" method.
        /// </summary>
        private DBCpm() { }

        public void Init()
        {//Initialize all the information, to get the latest data when fetching them again.
            _dsCpm = null;
            _dsCpmHist = null;
            _fnCpmXml = null;
            _fnCpmDetXml = null;
            _fnCpmDetHistXml = null;
            _lastCPMWriteDT = DEFAULT_DT;
            _lastCPMDetWriteDT = DEFAULT_DT;
            _lastCPMDetHistWriteDT = DEFAULT_DT;
            _lastRefreshDT = DEFAULT_DT;
            _lastRefreshDTHist = DEFAULT_DT;
        }

        /// <summary>
        /// Get the Instance of DBCpm. There will be only one reference for all the clients
        /// </summary>
        /// <returns></returns>
        public static DBCpm GetInstance()
        {
            if (_dbCpm == null)
            {
                _dbCpm = new DBCpm();
                //global::UFIS.ITrekLib.Properties.Settings.Default.InternalVersion;
            }
            return _dbCpm;
        }

        /// <summary>
        /// Retrieve the CPMDET information for the given flight without any other filter
        /// </summary>
        /// <param name="flightUrNo">Flight Urno</param>
        /// <returns>DSCpm</returns>
        public DSCpm RetrieveAllCpmDetForAFlight(string flightUrNo)
        {
            DSCpm ds = new DSCpm();
            bool foundCpm = false;

            foreach (DSCpm.CPMDETRow row in dsCpm.CPMDET.Select("FLNU='" + flightUrNo + "'"))
            {
                ds.CPMDET.LoadDataRow(row.ItemArray, true);
                foundCpm = true;
            }

            if (!foundCpm)
            {
                //LogTraceMsg("Not in current CPM:" + flightUrNo);
                foreach (DSCpm.CPMDETRow row in dsCpmHist.CPMDET.Select("FLNU='" + flightUrNo + "'"))
                {
                    ds.CPMDET.LoadDataRow(row.ItemArray, true);
                    foundCpm = true;
                }
            }
            else
            {
                //LogTraceMsg("It is in current CPM" + flightUrNo);
            }

            return ds;
        }

        /// <summary>
        /// Retrieve CpmDetail for a given Flight for Web
        /// </summary>
        /// <param name="flightUrNo">Flight Urno</param>
        /// <returns>DSCpm</returns>
        public DSCpm RetrieveCpmDetForAFlight(string flightUrNo)
        {
            DSCpm ds = new DSCpm();
            //DSCpm tempDs = dsCpm; AM :20080703
            DSCpm tempDs = RetrieveAllCpmDetForAFlight(flightUrNo);//AM:20080703

            foreach (DSCpm.CPMDETRow row in tempDs.CPMDET.Select("FLNU='" + flightUrNo + "'"))
            {
                ds.CPMDET.LoadDataRow(row.ItemArray, true);
            }

            return ds;
        }

       public DSCpm RetrieveCpmDetForAFlightWithoutCargo(string flightUrNo)
       {
          DSCpm ds = new DSCpm();
          //DSCpm tempDs = dsCpm; AM :20080703
          DSCpm tempDs = RetrieveAllCpmDetForAFlight(flightUrNo);//AM:20080703

          foreach (DSCpm.CPMDETRow row in tempDs.CPMDET.Select("FLNU='" + flightUrNo + "' AND CG='N'"))
          {
             ds.CPMDET.LoadDataRow(row.ItemArray, true);
          }

          return ds;
       }

        public bool IsCPMExist(string flightUrno)
        {
            bool exist = false;
            DSCpm tempDs = RetrieveCpmDetForAFlight(flightUrno);

            if (tempDs.CPMDET.Rows.Count > 0) exist = true;
            return exist;
        }

        public DSCpm RetrieveUnSentCPMUldForAFlightForTrips(string flightUrNo)
        {
            DSCpm ds = new DSCpm();
            //DSCpm tempDs = dsCpm; AM :20080703
            //DSCpm tempDs = RetrieveAllCpmDetForAFlight(flightUrNo);//AM:20080703
            DSCpm tempDs = RetrieveCpmDetForAFlightWithoutCargo(flightUrNo);//AM:20080703

            foreach (DSCpm.CPMDETRow row in tempDs.CPMDET.Select("FLNU='" + flightUrNo + "' AND (SENT_IND IS NULL OR SENT_IND<>'Y') AND CG='N'"))
            {
                ds.CPMDET.LoadDataRow(row.ItemArray, true);
            }

            return ds;
        }


        public DSCpm RetrieveUnSentCPMUldForAFlight(string flightUrNo)
        {
            DSCpm ds = new DSCpm();
            //DSCpm tempDs = dsCpm; AM :20080703
            //DSCpm tempDs = RetrieveAllCpmDetForAFlight(flightUrNo);//AM:20080703
            DSCpm tempDs = RetrieveCpmDetForAFlightWithoutCargo(flightUrNo);//AM:20080703

            foreach (DSCpm.CPMDETRow row in tempDs.CPMDET.Select("FLNU='" + flightUrNo + "' AND (SENT_IND IS NULL OR SENT_IND<>'Y')"))
            {
                ds.CPMDET.LoadDataRow(row.ItemArray, true);
            }

            return ds;
        }

        private string fnCpmXml
        {
            get
            {
                if ((_fnCpmXml == null) || (_fnCpmXml == ""))
                {
                    iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
                    _fnCpmXml = path.GetCpmFilePath();
                }

                return _fnCpmXml;
            }
        }

        private string fnCpmDetXml
        {
            get
            {
                if ((_fnCpmDetXml == null) || (_fnCpmDetXml == ""))
                {
                    iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
                    _fnCpmDetXml = path.GetCpmDetFilePath();
                }

                return _fnCpmDetXml;
            }
        }

        private string fnCpmDetHistXml
        {
            get
            {
                if ((_fnCpmDetHistXml == null) || (_fnCpmDetHistXml == ""))
                {
                    iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
                    _fnCpmDetHistXml = path.GetCpmDetHistFilePath();
                    LogTraceMsg("CpmDetHist File:" + _fnCpmDetHistXml + ".");
                }

                return _fnCpmDetHistXml;
            }
        }

        private DSCpm dsCpm
        {
            get
            {
                int timeElapse = Math.Abs(UtilTime.DiffInSec(DateTime.Now, _lastRefreshDT));
                if ((_dsCpm == null) || (timeElapse > 3))
                {
                    LoadDSCpm();
                    _lastRefreshDT = DateTime.Now;
                    LogTraceMsg("CPM Refresh:" + _lastRefreshDT.ToLongTimeString());

                }
                DSCpm ds = (DSCpm)_dsCpm.Copy();

                return ds;
            }
        }

        private DSCpm dsCpmHist
        {
            get
            {
                int timeElapse = Math.Abs(UtilTime.DiffInSec(DateTime.Now, _lastRefreshDTHist));
                if ((_dsCpmHist == null) || (timeElapse > 30))
                {
                    try
                    {
                        LoadDSCpmHist();
                        _lastRefreshDTHist = DateTime.Now;
                        LogTraceMsg("CPM Hist Refresh:" + _lastRefreshDTHist.ToLongTimeString());
                    }
                    catch (Exception ex)
                    {
                        LogTraceMsg("dsCpmHist:Err:" + ex.Message);
                    }
                }
                return (DSCpm)(_dsCpmHist.Copy());
            }
        }

        private void LoadDSCpmHist()
        {
            FileInfo fiXml = new FileInfo(fnCpmDetHistXml);
            DateTime curDT = fiXml.LastWriteTime;
            if (!fiXml.Exists) throw new ApplicationException("CPM Detail History information missing.");

            if ((_dsCpmHist == null) || (curDT.CompareTo(_lastCPMDetHistWriteDT) != 0))
            {
                lock (_lockObjCpmDetHist)
                {
                    try
                    {
                        fiXml = new FileInfo(fnCpmDetHistXml);
                        curDT = fiXml.LastWriteTime;//check the time again, in case of lock
                        if ((_dsCpm == null) || (curDT.CompareTo(_lastCPMDetHistWriteDT) != 0))
                        {
                            try
                            {
                                DSCpm tempDs = new DSCpm();
                                tempDs.ReadXml(fnCpmDetHistXml);
                                _dsCpmHist = tempDs;
                                LogTraceMsg("LoadDSCpmHist:CPMDetCnt:" + _dsCpmHist.CPMDET.Rows.Count);
                                _lastCPMDetHistWriteDT = curDT;
                            }
                            catch (Exception ex)
                            {
                                LogMsg(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogMsg("LoadCpmHist:Err:" + ex.Message);
                    }
                }
            }
        }

        private void LoadDSCpm()
        {
            if (IsNeedToUpdateData())
            {
                lock (_lockObj)
                {
                    try
                    {
                        if (IsNeedToUpdateData())
                        {
                            if (_dsCpm == null) _dsCpm = new DSCpm();
                            //LoadDSCpm_CPMTable();
                            LoadDSCpm_CpmDetailTable();
                        }
                    }
                    catch (Exception ex)
                    {
                        LogMsg("LoadDSCpm:Err:" + ex.Message);
                    }
                }
            }
        }

        private bool IsNeedToUpdateData()
        {
            bool needToUpdate = true;
            if ((_dsCpm == null) ||
                //(IsNeedToUpdateCPMTable()) ||
                (IsNeedToUpdateCPMDetTable())) needToUpdate = true;
            else needToUpdate = false;

            return needToUpdate;
        }

        //private bool IsNeedToUpdateCPMTable()
        //{
        //    bool needToUpdate = false;
        //    FileInfo fiXml = new FileInfo(fnCpmXml);
        //    DateTime curDT = fiXml.LastWriteTime;
        //    if (curDT.CompareTo(_lastCPMWriteDT) != 0) needToUpdate = true;
        //    return needToUpdate;
        //}

        private bool IsNeedToUpdateCPMDetTable()
        {
            bool needToUpdate = false;
            FileInfo fiXml = new FileInfo(fnCpmDetXml);
            DateTime curDT = fiXml.LastWriteTime;
            if (curDT.CompareTo(_lastCPMDetWriteDT) != 0) needToUpdate = true;
            return needToUpdate;
        }


        //private void LoadDSCpm_CPMTable()
        //{
        //    FileInfo fiXml = new FileInfo(fnCpmXml);
        //    DateTime curDT = fiXml.LastWriteTime;
        //    if (!fiXml.Exists)
        //    {
        //        //throw new ApplicationException("CPM information missing. Please inform to administrator.");
        //        return;
        //    }

        //    if ((_dsCpm == null) || (curDT.CompareTo(_lastCPMWriteDT) != 0))
        //    {
        //        lock (_lockObjCpm)
        //        {
        //            try
        //            {
        //                curDT = fiXml.LastWriteTime;//check the time again, in case of lock
        //                if ((_dsCpm == null) || (curDT.CompareTo(_lastCPMWriteDT) != 0))
        //                {
        //                    try
        //                    {
        //                        DSCpm tempDs = new DSCpm();
        //                        tempDs.ReadXml(fnCpmXml);
        //                        if (_dsCpm == null)
        //                        {
        //                            _dsCpm = new DSCpm();
        //                        }
        //                        else
        //                        {
        //                            _dsCpm.CPM.Clear();
        //                        }
        //                        _dsCpm.CPM.Merge(tempDs.CPM);
        //                        _lastCPMWriteDT = curDT;
        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        LogMsg("LoadDSCpm_CPMTable : Error : " + ex.Message);
        //                        TestWriteCPM();
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                LogMsg("LoadDSCpm_CPMTable:Err:" + ex.Message);
        //            }
        //        }
        //    }
        //}

        private void LoadDSCpm_CpmDetailTable()
        {
            FileInfo fiXml = new FileInfo(fnCpmDetXml);
            DateTime curDT = fiXml.LastWriteTime;
            if (!fiXml.Exists) throw new ApplicationException("CPM Detail information missing. Please inform to administrator.");

            if ((_dsCpm == null) || (curDT.CompareTo(_lastCPMDetWriteDT) != 0))
            {
                lock (_lockObjCpmDet)
                {
                    try
                    {
                        fiXml = new FileInfo(fnCpmDetXml);
                        curDT = fiXml.LastWriteTime;//check the time again, in case of lock
                        if ((_dsCpm == null) || (curDT.CompareTo(_lastCPMDetWriteDT) != 0))
                        {
                            try
                            {
                                DSCpm tempDs = new DSCpm();
                                tempDs.ReadXml(fnCpmDetXml);
                                _dsCpm = tempDs;
                                _lastCPMDetWriteDT = curDT;
                            }
                            catch (Exception ex)
                            {
                                LogMsg("LoadDSCpm_CpmDetailTable:Err: " + ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogMsg("LoadDSCpm_CpmDetailTable:Err1:" + ex.Message);
                    }
                }
            }
        }

        public void UpdateSentIndicatorbyUrno(ArrayList lstUrno, string flightUrno)
        {
            lock (_lockObj)
            {
                try
                {
                    LockCpmForWriting();
                    DSCpm ds = null;
                    int updCnt = 0;
                    try
                    {
                        ds = new DSCpm();
                        //LogTraceMsg("TripPath:" + fnTripXml);
                        ds.ReadXml(fnCpmDetXml);
                        if (ds == null)
                        {
                            throw new ApplicationException("Unable to get the Cpm Information.");
                        }
                    }
                    catch (ApplicationException ex) { throw ex; }
                    catch (Exception ex)
                    {
                        throw new ApplicationException("Unable to get the Cpm Information due to " + ex.Message);
                    }

                    foreach (string urno in lstUrno)
                    {
                        DSCpm.CPMDETRow[] tempRowArr = (DSCpm.CPMDETRow[])ds.CPMDET.Select("URNO='" + urno + "'");

                        if (tempRowArr.Length > 0)
                        {
                            tempRowArr[0].SENT_IND = "Y";
                            updCnt++;
                            LogMsg("Updating Sent Indicator:" + urno);
                        }
                    }
                    if (updCnt > 0)
                    {
                        ds.CPMDET.AcceptChanges();
                        ds.CPMDET.WriteXml(fnCpmDetXml);
                        _dsCpm = ds;
                    }
                }
                catch (Exception ex)
                {
                    LogMsg("UpdateSentIndicator:Err:" + ex.Message);
                }
                finally
                {
                    ReleaseCpmAfterWrite();
                }
            }
        }

        public void UpdateSentIndicator(XmlDocument xmlDoc)
        {
            string flightUrno = "";
            string uldNo = "";
            ArrayList al = new ArrayList();
            lock (_lockObj)
            {
                try
                {
                    FileInfo fi = new FileInfo(fnCpmDetXml);
                    if (!fi.Exists)
                    {
                        return;
                    }

                    DSCpm dsOldCPM = new DSCpm();
                    dsOldCPM.ReadXml(fnCpmDetXml);

                    DataSet dsNewTrip = new DataSet();
                    XmlNodeReader reader = new XmlNodeReader(xmlDoc);
                    dsNewTrip.ReadXml(reader);
                    int cnt = dsNewTrip.Tables["TRIP"].Rows.Count;
                    for (int i = 0; i < cnt; i++)
                    {
                        DataRow newRow = dsNewTrip.Tables["TRIP"].Rows[i];
                        string urno = newRow["URNO"].ToString();
                        flightUrno = newRow["UAFT"].ToString();
                        uldNo = newRow["ULDN"].ToString();
                        string sentDt = "";
                        try
                        {
                            sentDt = newRow["SEND_DT"].ToString();
                        }
                        catch (Exception ex)
                        {
                            LogMsg("UpdateSentIndicator SEND_DT Error " + ex.Message);
                        }
                        if (sentDt != "")
                        {
                            DSCpm.CPMDETRow[] tempRowArr = (DSCpm.CPMDETRow[])dsOldCPM.CPMDET.Select("FLNU='" + flightUrno + "' AND ULD_TXT='" + uldNo + "'");
                            if (tempRowArr.Length > 0)
                            {
                                tempRowArr[0].SENT_IND = "Y";
                            }
                            else
                            {
                                al.Add(uldNo);
                            }
                        }
                    }
                    dsOldCPM.CPMDET.AcceptChanges();
                    dsOldCPM.CPMDET.WriteXml(fnCpmDetXml);
                }
                catch (Exception ex)
                {
                    LogMsg("UpdateSentIndicator:Err:" + ex.Message);
                }
            }
            if (al.Count > 0)
            {
                iTXMLcl iTXML = new iTXMLcl();
                iTXML.updateUldsFromContainerXml(flightUrno, al);
            }
        }

        private string getValueFromCPM(DataRow row, string field)
        {
            string value = "";
            try
            {
                value = row[field].ToString().Trim();
            }
            catch (Exception ex)
            {
                LogTraceMsg("getValueFromCPM:Err:" + ex.Message);
            }
            return value;
        }


        public void UpdateCPMInfoFromBackendToCPMXmlFile(XmlDocument xmlDoc)
        {
            lock (_lockObj)
            {
                try
                {
                    LockCpmForWriting();
                    DSCpm ds = null;
                    LogTraceMsg("UpdateCPMInfoFromBackendToCPMXmlFile");
                    try
                    {
                        ds = new DSCpm();

                        ds.ReadXml(fnCpmDetXml);
                        if (ds == null)
                        {
                            throw new ApplicationException("Unable to get the Cpm Information.");
                        }
                    }
                    catch (ApplicationException ex) { throw ex; }
                    catch (Exception ex)
                    {
                        throw new ApplicationException("Unable to get the Cpm Information due to " + ex.Message);

                    }

                    DataSet dsNewCpm = new DataSet();
                    XmlNodeReader reader = new XmlNodeReader(xmlDoc);
                    dsNewCpm.ReadXml(reader);

                    int cnt = dsNewCpm.Tables["CPMDET"].Rows.Count;
                    string flightUrno = "";
                    string loaurno = "";
                    string alc2 = "";
                    int updCnt = 0;
                    bool updStatus = false;

                    if (cnt > 0)
                    {
                        DataRow flightRow = dsNewCpm.Tables["CPMDET"].Rows[0];

                        flightUrno = getValueFromCPM(flightRow, "FLNU");
                        foreach (DSCpm.CPMDETRow row in ds.CPMDET.Select("FLNU='" + flightUrno + "'AND (SENT_IND IS NULL OR SENT_IND<>'Y') "))
                        {
                            row.Delete();
                        }
                        ds.AcceptChanges();
                    }
                    for (int i = 0; i < cnt; i++)
                    {
                        DataRow newRow = dsNewCpm.Tables["CPMDET"].Rows[i];
                        if (i == 0)
                        {
                            flightUrno = getValueFromCPM(newRow, "FLNU");
                            alc2 = getValueFromCPM(newRow, "ALC2");
                        }
                        loaurno = getValueFromCPM(newRow, "URNO");

                        if (loaurno != "")
                        {
                            string uldNo = getValueFromCPM(newRow, "ULDN");
                            string apc3 = "";
                            string cont = "";
                            string posf = "";
                            string valu = "";
                            string trimcont = "";
                            string dssn = "";
                            try
                            {
                                apc3 = getValueFromCPM(newRow, "APC3");
                                try
                                {
                                    cont = getValueFromCPM(newRow, "CONT").ToUpper();
                                }
                                catch (Exception)
                                {
                                }
                                posf = getValueFromCPM(newRow, "POSF");
                                valu = getValueFromCPM(newRow, "VALU");
                                dssn = getValueFromCPM(newRow, "DSSN");
                            }
                            catch (Exception ex)
                            {
                                LogMsg("UpdateCPMInfoFromBackendToCPMXmlFile:Error:" + ex.Message);
                            }
                            string uldText = posf + "/" + uldNo + "/" + apc3 + "/" + valu + "/" + cont;

                            DSCpm.CPMDETRow[] tempRowArr = (DSCpm.CPMDETRow[])ds.CPMDET.Select("URNO='" + loaurno + "'");
                            string send_ind = "";

                            if (tempRowArr.Length > 0)
                            {//Existing record - to update
                                /* Added by Alphy to update send Indicator in backend*/
                                try
                                {
                                    send_ind = tempRowArr[0].SENT_IND.Trim();
                                }
                                catch (Exception)
                                {
                                }
                                tempRowArr[0].Delete();
                                LogTraceMsg("Deleted URNO:" + loaurno);
                            }

                            DSCpm.CPMDETRow updRow = ds.CPMDET.NewCPMDETRow();
                            try
                            {
                                updRow.URNO = loaurno;
                                updRow.APC3 = apc3;
                                updRow.CONT = cont;
                                updRow.DSSN = dssn;
                                updRow.FLNU = flightUrno;
                                updRow.POSF = posf;
                                //updRow.ULDN = newRow["ULDN"].ToString();
                                updRow.ULDN = iTrekUtils.iTUtils.removeBadCharacters(uldText);
                                updRow.VALU = valu;
                                updRow.SENT_IND = send_ind;
                                updRow.ALC2 = alc2;

                                if (alc2 != "" && cont != "")
                                {
                                    if (cont.Length > 1)
                                    {
                                        trimcont = cont.Substring(0, 2);
                                    }
                                    else
                                    {
                                        trimcont = cont.Substring(0, 1);
                                    }
                                    //  LogMsg("trimcont" + trimcont);
                                    updRow.PRTY = Ctrl.CtrlCpmPriority.GetPriority(alc2, trimcont).ToString();
                                }
                                updStatus = true;
                            }

                            catch (Exception ex)
                            {
                                updStatus = false;
                                LogTraceMsg("UpdateCPMInfoFromBackendToCPMXmlFile" + ex.Message);
                                throw new ApplicationException(ex.Message);
                            }
                            if (updStatus)
                            {
                                ds.CPMDET.AddCPMDETRow(updRow);
                                updCnt++;
                            }

                        }
                    }

                    if (updCnt > 0)
                    {
                        ds.CPMDET.AcceptChanges();
                        ds.WriteXml(fnCpmDetXml);
                        _dsCpm = ds;
                        LogTraceMsg("cpm FOR URNO:" + flightUrno);
                    }
                }
                catch (Exception ex)
                {
                    LogTraceMsg("UpdateCPMInfoFromBackendToCPMXmlFile:Err:" + ex.Message);
                }
                finally
                {
                    ReleaseCpmAfterWrite();
                }
            }
        }


        public DSCpm RetrieveCpmDetForAFlightfromBRS(string flightUrNo)
        {
            DSCpm ds = new DSCpm();
            DSCpm tempDs = dsCpm;
            foreach (DSCpm.CPMDETRow row in tempDs.CPMDET.Select("FLNU='" + flightUrNo + "'AND DSSN='BRS'"))
            {
                ds.CPMDET.LoadDataRow(row.ItemArray, true);
            }
            return ds;
        }


        private void LockCpmForWriting()
        {
            try
            {
                UtilLock.GetInstance().LockCpm();
            }
            catch (Exception ex)
            {
                LogMsg("LockCpmForWriting:Err:" + ex.Message);
                if (ex.InnerException != null)
                {
                    LogMsg("LockCpmForWriting:err:Inner:" + ex.InnerException.Message);
                }
            }
        }

        private void ReleaseCpmAfterWrite()
        {
            try
            {
                UtilLock.GetInstance().ReleaseCpm();
            }
            catch (Exception ex)
            {
                LogMsg("ReleaseCpmAfterWrite:Err:" + ex.Message);
                if (ex.InnerException != null)
                {
                    LogMsg("ReleaseCpmAfterWrite:err:Inner:" + ex.InnerException.Message);
                }
            }
        }


        private void LogMsg(string msg)
        {
            //Util.UtilLog.LogToGenericEventLog("DBCpm", msg);
           try
           {
              MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("DBCpm", msg);
           }
           catch (Exception)
           {
              UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DBCpm", msg);
           }
        }

        private void LogTraceMsg(string msg)
        {
            //UtilLog.LogToTraceFile("DBCpm", msg);
           try
           {
              MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("DBCpm", msg);
           }
           catch (Exception)
           {
              UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DBCpm", msg);
           }
        }


        public void deleteOldCPMS(ArrayList alFlightUrno)
        {
            lock (_lockObj)
            {
                try
                {
                    LockCpmForWriting();
                    DSCpm ds = null;

                    try
                    {
                        ds = new DSCpm();

                        ds.ReadXml(fnCpmDetXml);
                        if (ds == null)
                        {
                            throw new ApplicationException("Unable to get the Cpm Information.");
                        }
                    }
                    catch (ApplicationException ex) { throw ex; }
                    catch (Exception ex)
                    {
                        throw new ApplicationException("Unable to get the Cpm Information due to " + ex.Message);
                    }


                    foreach (string urno in alFlightUrno)
                    {
                        foreach (DSCpm.CPMDETRow row in ds.CPMDET.Select("FLNU='" + urno + "'"))
                        {
                            ds.CPMDET.RemoveCPMDETRow(row);

                        }
                        ds.AcceptChanges();
                    }
                    ds.WriteXml(fnCpmDetXml);
                    _dsCpm = ds;
                }

                catch (Exception ex)
                {
                    LogMsg("deleteOldCPMS:Err:" + ex.Message);

                }

            }
        }

        //private void TestWriteCPM()
        //{
            //{
            //    DSCpm ds = new DSCpm();
            //    DSCpm.CPMRow row = ds.CPM.NewCPMRow();
            //    row.URNO = "CPMUrno123";
            //    row.UAFT = "UAFT1324";
            //    row.MSG = "Msg123 afd";
            //    ds.CPM.Rows.Add(row);
            //    ds.CPM.AcceptChanges();
            //    ds.CPM.WriteXml(fnCpmXml);
            //}
        //}


    }
}
