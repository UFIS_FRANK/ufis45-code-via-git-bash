using System;
using System.Collections.Generic;
using System.Text;

using System.Data;
using System.IO;

using UFIS.ITrekLib.DS;
using iTrekXML;
using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.Util;

namespace UFIS.ITrekLib.DB
{
    public class DBCpmPriority
    {
        private static Object _lockObj = new Object();
        private static readonly Object _lockObjInstance = new Object();
        //to use in synchronisation of single access
        private static DBCpmPriority _DBCpmPriority = null;
        private static DateTime lastWriteDT = new System.DateTime(1, 1, 1);

        private static DSCpmPriority _dsOCpmPriority = null;
        private static string _fnXml = null;//file name of XML in full path.

        ///// <summary>
        ///// Singleton Pattern.
        ///// Not allowed to create the object from other class.
        ///// To get the object, user "GetInstance()" method.
        ///// </summary>
        private DBCpmPriority() { }

        public void Init()
        {//Initialize all the information, to get the latest data when fetching them again.
            _dsOCpmPriority = null;
            _fnXml = null;
            lastWriteDT = new System.DateTime(1, 1, 1);
        }

        //  /// <summary>
        //  /// Get the Instance of DBCpmPriority. There will be only one reference for all the clients
        //  /// </summary>
        //  /// <returns></returns>
        public static DBCpmPriority GetInstance()
        {
            if (_DBCpmPriority == null)
            {
                lock (_lockObjInstance)
                {
                    try
                    {
                        if (_DBCpmPriority == null)
                            _DBCpmPriority = new DBCpmPriority();
                    }
                    catch (Exception ex)
                    {
                        LogMsg("GetInstance:Err:" + ex.Message);
                    }
                }
            }
            return _DBCpmPriority;
        }

        private string FnXml
        {
            get
            {
                if ((_fnXml == null) || (_fnXml == ""))
                {
                    iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
                    _fnXml = path.GetCpmPriorityFilePath();
                }

                return _fnXml;
            }
        }

        private DSCpmPriority DsOCpmPriority
        {
            get
            {
                //if (_DsOCpmPriority == null) { LoadDSCpmPriority(); }
                LoadDSCpmPriority();
                DSCpmPriority ds = (DSCpmPriority)_dsOCpmPriority.Copy();

                return ds;
            }
        }


        private void LoadDSCpmPriority()
        {
            FileInfo fiXml = new FileInfo(FnXml);
            DateTime curDT = fiXml.LastWriteTime;

            if ((_dsOCpmPriority == null) || (curDT.CompareTo(lastWriteDT) != 0))
            {
                lock (_lockObj)
                {
                    try
                    {
                        curDT = fiXml.LastWriteTime;//check the time again, in case of lock
                        if ((_dsOCpmPriority == null) || (curDT.CompareTo(lastWriteDT) != 0))
                        {
                            try
                            {
                                DSCpmPriority tempDs = new DSCpmPriority();

                                tempDs.TPR.ReadXml(FnXml);
                                _dsOCpmPriority = tempDs;
                                lastWriteDT = curDT;
                            }
                            catch (Exception ex)
                            {
                                LogMsg("LoadDSCpmPriority : Error : " + ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogMsg("LoadDSCpmPriority:Err:" + ex.Message);
                    }
                }
            }
        }

        private static void LogMsg(string msg)
        {
            //UtilLog.LogToGenericEventLog("DBCpmPriority", msg);
           try
           {
              MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("DBCpmPriority", msg);
           }
           catch (Exception)
           {
              UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DBCpmPriority", msg);
           }
        }

        public bool IsExist(string airlineCode, int priority)
        {
            bool exist = false;

            DSCpmPriority.TPRRow[] rows = (DSCpmPriority.TPRRow[])
                DsOCpmPriority.TPR.Select(GetSelectStatement(airlineCode, priority));

            if ((rows != null) && (rows.Length > 0)) exist = true;
            return exist;
        }

        private string GetSelectStatement(string airlineCode, int priority)
        {
            return "ALC='" + airlineCode + "' AND PR=" + priority;
        }

        public void Update(string airlineCode, int priority, string bagTypes)
        {
            DSCpmPriority ds = ReadFromFile();

            DSCpmPriority.TPRRow[] rows = (DSCpmPriority.TPRRow[])ds.TPR.Select(GetSelectStatement(airlineCode, priority));
            if ((rows == null) || (rows.Length < 1))
            {
                Create(airlineCode, priority, bagTypes);
            }
            else if (rows.Length == 1)
            {
                rows[0].BT = bagTypes;
            }
            else if (rows.Length > 1) throw new ApplicationException("Duplicate Entry");
            ds.TPR.AcceptChanges();
            WriteToFile(ds);
        }

        private DSCpmPriority ReadFromFile()
        {
            DSCpmPriority ds = new DSCpmPriority();
            ds.TPR.ReadXml(FnXml);
            return ds;
        }

        private void WriteToFile(DSCpmPriority ds)
        {
            ds.TPR.AcceptChanges();
            ds.TPR.WriteXml(FnXml, XmlWriteMode.IgnoreSchema, false);
            _dsOCpmPriority = ds;
            lastWriteDT = lastWriteDT.AddYears(-1);
        }

        public void Create(string airlineCode, int priority, string bagTypes)
        {
            if (IsExist(airlineCode, priority))
            {
                Update(airlineCode, priority, bagTypes);
            }
            else
            {
                DSCpmPriority ds = ReadFromFile();
                DSCpmPriority.TPRRow row = (DSCpmPriority.TPRRow)ds.TPR.NewRow();
                row.ALC = airlineCode;
                row.PR = priority;
                row.BT = bagTypes;
                ds.TPR.AddTPRRow(row);
                ds.TPR.AcceptChanges();
                WriteToFile(ds);
            }
        }

        public void Delete(string airlineCode, int priority)
        {
            try
            {
                DSCpmPriority ds = ReadFromFile();

                DSCpmPriority.TPRRow[] rows = (DSCpmPriority.TPRRow[])ds.TPR.Select(GetSelectStatement(airlineCode, priority));
                int cnt = rows.Length;

                for (int i = 0; i < cnt; i++)
                {
                    rows[i].Delete();
                }
                ds.TPR.AcceptChanges();

                WriteToFile(ds);

            }
            catch (Exception ex)
            {
                LogMsg("Delete:Err: " + ex.Message);
                throw new ApplicationException("Unable to delete due to : " + ex.Message);
            }
        }


        public DSCpmPriority RetrieveAll()
        {
            DSCpmPriority ds = null;
            ds = (DSCpmPriority)DsOCpmPriority.Copy();

            return ds;
        }

    }
}

