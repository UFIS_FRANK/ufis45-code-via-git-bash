using System;
using System.Collections.Generic;
using System.Text;

namespace UFIS.ITrekLib.Config
{
	public class DB_Info
	{
		private DB_Info() { }

		public const string DB_T_FLIGHT = "ITK_FLIGHT";
		public const string DB_T_JOB = "ITK_JOB";
		public const string DB_T_MSG = "ITK_MSG";
		public const string DB_T_MSGTO = "ITK_MSGTO";
		public const string DB_T_ASR = "ITK_ASR";
		public const string DB_T_ASR_OTH = "ITK_ASR_OTH";
		public const string DB_T_BPTR = "ITK_BPTR";

		public const string DB_T_CPMDET = "ITK_CPMDET";
		public const string DB_T_CTRTAB = "ITK_CTRTAB";
		public const string DB_T_LOG = "ITK_LOG";
		public const string DB_T_MGITAB = "ITK_MGITAB";
		public const string DB_T_MGOTAB = "ITK_MGOTAB";
		public const string DB_T_RUNTAB = "ITK_RUNTAB";
		public const string DB_T_SDF_ASR_OTH = "ITK_SDF_ASR_OTH";
		public const string DB_T_STAFF = "ITK_STAFF";
		public const string DB_T_TRKTAB = "ITK_TRKTAB";
		public const string DB_T_TRP = "ITK_TRP";
		public const string DB_T_TRPDET = "ITK_TRPDET";
		public const string DB_T_TRPMST = "ITK_TRPMST";

		public const string DB_V_ASR = "V_ITK_ASR";
		public const string DB_V_ASR_OTH = "V_ITK_ASR_OTH";
		public const string DB_V_BPTR_DET = "V_ITK_BPTR_DET";
		public const string DB_V_BPTR_HDR = "V_ITK_BPTR_HDR";
		public const string DB_V_BPTR_SUMM = "V_ITK_BPTR_SUMM";
		public const string DB_V_FLIGHT = "V_ITK_FLIGHT";
		public const string DB_V_GEN_URNO = "V_ITK_GEN_URNO";
		public const string DB_V_JOB = "V_ITK_JOB";
		public const string DB_V_MSG = "V_ITK_MSG";
		public const string DB_V_MSGLOG = "V_ITK_MSGLOG_LOG";
		public const string DB_V_MSGTO = "V_ITK_MSGTO";
		public const string DB_V_TRP = "V_ITK_TRP";


		/// <summary>
		/// Get Full Name (Schema.Table/ViewName) of given Table/View Name "dbName".
		/// e.g. Table Name "dbName" is "ITK_FLIGHT". It will return "ITREKSCH.ITK_FLIGHT".
		/// </summary>
		/// <param name="dbName"></param>
		/// <returns></returns>
		public static string GetDbFullName(string dbName)
		{
			return GetDataSchemaName() + dbName;
		}

		private static object _lockObj = new object();

		private static string _iTrekDbDataSchemaName = null;
		public static string GetDataSchemaName()
		{
			if (_iTrekDbDataSchemaName == null)
			{
				lock (_lockObj)
				{
					if (_iTrekDbDataSchemaName == null)
					{
						string schName = ITrekConfig.ITrekDbDataSchemaName;
						if (string.IsNullOrEmpty(schName))
						{
							_iTrekDbDataSchemaName = "";
						}
						else
						{
							_iTrekDbDataSchemaName = schName + ".";
						}
					}
				}
			}
			return _iTrekDbDataSchemaName;
		}
	}
}
