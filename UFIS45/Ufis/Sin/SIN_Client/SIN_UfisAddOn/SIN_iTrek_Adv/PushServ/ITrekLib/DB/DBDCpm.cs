


# define TestingOn

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.IO;
using System.Xml;
using System.Data;
using System.Collections;
//using iTrekXML;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.Util;
using UFIS.ITrekLib.Misc;
using System.Data.Common;
using UFIS.ITrekLib.Config;
using UFIS.ITrekLib.Ctrl;
using MM.UtilLib;


namespace UFIS.ITrekLib.DB
{
    public class DBDCpm
    {
        private static Object _lockObj = new Object();//to use in synchronisation of single access
        private static Object _lockObjCpmHist = new Object();//to use in synchronisation of single access
        private static Object _lockObjCpmDet = new Object();//to use in synchronisation of single access
        private static DBDCpm _dbCpm = null;

        private static readonly DateTime DEFAULT_DT = new System.DateTime(1, 1, 1);
        private static DateTime _lastCPMDetHistWriteDT = DEFAULT_DT;
        private static DateTime _lastCPMDetWriteDT = DEFAULT_DT;

        private static DSCpm _dsCpm = null;
     
     
        
        /// <summary>
        /// Singleton Pattern.
        /// Not allowed to create the object from other class.
        /// To get the object, user "GetInstance()" method.
        /// </summary>
        private DBDCpm() { }

        public void Init()
        {//Initialize all the information, to get the latest data when fetching them again.
            _dsCpm = null;
           
            _lastCPMDetHistWriteDT = DEFAULT_DT;
            _lastCPMDetWriteDT = DEFAULT_DT;
        }

        /// <summary>
        /// Get the Instance of DBCpm. There will be only one reference for all the clients
        /// </summary>
        /// <returns></returns>
        public static DBDCpm GetInstance()
        {
            if (_dbCpm == null)
            {
                _dbCpm = new DBDCpm();
            }
            return _dbCpm;
        }

        private static string GetDb_Tb_CpmDet()
        {
            return DB_Info.GetDbFullName(DB_Info.DB_T_CPMDET);
        }

        

        public DSCpm RetrieveCpmDetForAFlight(IDbCommand cmd, string flightUrNo)
        {
            DSCpm ds = new DSCpm();
            DSCpm tempDs = new DSCpm();
            LoadData(cmd, ref tempDs, flightUrNo);
            foreach (DSCpm.CPMDETRow row in tempDs.CPMDET.Select("FLNU='" + flightUrNo + "'"))
            {
                ds.CPMDET.LoadDataRow(row.ItemArray, true);
            }

            return ds;
        }

        public DSCpm RetrieveCpmDetForAFlightForTrips(IDbCommand cmd,string flightUrNo)
        {
            DSCpm ds = new DSCpm();
            DSCpm tempDs = new DSCpm();
            LoadData(cmd, ref tempDs, flightUrNo);
            foreach (DSCpm.CPMDETRow row in tempDs.CPMDET.Select("FLNU='" + flightUrNo + "'AND CG='N' AND SENT_IND<>'D'"))
            {

               
                    ds.CPMDET.LoadDataRow(row.ItemArray, true);
              
            }
           

            return ds;
        }

        public bool IsCPMExist(IDbCommand cmd,string flightUrno)
        {
            bool exist = false;
            DSCpm tempDs = RetrieveCpmDetForAFlight(cmd,flightUrno);
            //if (tempDs.CPM.Rows.Count > 0) exist = true;
            if (tempDs.CPMDET.Rows.Count > 0) exist = true;
            return exist;
        }
        public bool IsCPMExist(IDbCommand cmd, string flightUrno,out DSCpm tempDs)
        {
            bool exist = false;
            tempDs = RetrieveCpmDetForAFlight(cmd, flightUrno);
            //if (tempDs.CPM.Rows.Count > 0) exist = true;
            if (tempDs.CPMDET.Rows.Count > 0) exist = true;
            return exist;
        }
        public DSCpm RetrieveUnSentCPMUldForAFlightForTrips(IDbCommand cmd,string flightUrNo)
        {
            DSCpm ds = new DSCpm();
            DSCpm tempDs = new DSCpm();
            LoadData(cmd, ref tempDs, flightUrNo);
            foreach (DSCpm.CPMDETRow row in tempDs.CPMDET.Select("FLNU='" + flightUrNo + "' AND (SENT_IND<>'S' AND SENT_IND<>'D') AND CG='N'"))
            {
               

            
                    ds.CPMDET.LoadDataRow(row.ItemArray, true); 
            }
               
          

            return ds;
        }
        public DSCpm RetrieveUnSentCPMUldForAFlightForTripsForBrs(IDbCommand cmd, string flightUrNo)
        {
            DSCpm ds = new DSCpm();
            DSCpm tempDs = new DSCpm();
            LoadData(cmd, ref tempDs, flightUrNo);
            foreach (DSCpm.CPMDETRow row in tempDs.CPMDET.Select("FLNU='" + flightUrNo + "' AND (SENT_IND<>'S' AND SENT_IND<>'D')"))
            {

                //LogTraceMsg("INSIDE RetrieveUnSentCPMUldForAFlightForTripsForBrs");

                ds.CPMDET.LoadDataRow(row.ItemArray, true);
            }



            return ds;
        }


        public DSCpm RetrieveUnSentCPMUldForAFlight(IDbCommand cmd,string flightUrNo)
        {
            DSCpm ds = new DSCpm();
            DSCpm tempDs = new DSCpm();
            LoadData(cmd, ref tempDs, flightUrNo);
            foreach (DSCpm.CPMDETRow row in tempDs.CPMDET.Select("FLNU='" + flightUrNo + "' AND ( SENT_IND<>'S' AND SENT_IND<>'D')"))
            {
                
                    ds.CPMDET.LoadDataRow(row.ItemArray, true);
                

            }
           

            return ds;
        }

      

       
        private DSCpm dsCpm
        {
            get
            {
                //if (_dsCpm == null) { LoadDSCpm(); }
               // LoadDSCpm();
                DSCpm ds = (DSCpm)_dsCpm.Copy();

                return ds;
            }
        }

       
       

       


        //private bool IsNeedToUpdateData()
        //{
        //    bool needToUpdate = true;
        //    if ((_dsCpm == null) ||(IsNeedToUpdateCPMDetTable())) needToUpdate = true;
        //    else needToUpdate = false;

        //    return needToUpdate;
        //}

       
      


      

       
        private string getValueFromCPM(DataRow row, string field)
        {
            string value = "";
            try
            {
                value = row[field].ToString().Trim();
                
            }
            catch (Exception ex)
            {

                LogTraceMsg("getValueFromCPM" + ex.Message);
            }
            return value;
        }


      


       
        private void LogMsg(string msg)
        {
            //Console.WriteLine(msg);
            UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DBDCpm", msg);
        }

       
        private static void LogTraceMsg(string msg)
        {
            try
            {
                MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("DBDCpm", msg);

            }
            catch (Exception)
            {
                UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DBDCpm", msg);
            }
        }

    
        /// <summary>
        /// Selection Command Text to fill the DSCpm
        /// </summary>
       // private const string _cmdSelCpm = @"SELECT URNO, USTF,UAFT, FCCO, ACFR, ACTO,FLID,DETY,PENO FROM  CPMDET ";
        private const string _cmdSelCpm = @"SELECT * FROM  ";
        private const string _cmdSelCpmDet = @"SELECT URNO,ULDN,FLID AS FLNU,DSSN,APC3,CONT,POSF,VALU,STAT SENT_IND,ALC2,PRTY FROM ";
        /// <summary>
        /// Load Cpm Information for 3 days (From back 2days to Today). Return True when successfully loaded.
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="ds"></param>
        /// <returns>True-Load Successful.</returns>
        private bool LoadData(IDbCommand cmd, ref DSCpm ds)
        {
            bool loadSuccess = false;
            UtilDb db = UtilDb.GetInstance();
            bool isCmdNull = false;
            try
            {
                if (cmd == null)
                {
                    isCmdNull = true;
                    cmd = db.GetCommand(false);
                }
                DateTime dt1 = DateTime.Now;

                if (ds == null) ds = new DSCpm();

                string paramNamePrefix = UtilDb.GetParaNamePrefix();
                string pnFrDate = paramNamePrefix + "FR_FL_DATE";
                string pnToDate = paramNamePrefix + "TO_FL_DATE";

                cmd.CommandText = _cmdSelCpm + GetDb_Tb_CpmDet()+ " WHERE BFLT BETWEEN " + pnFrDate + " AND " + pnToDate;
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Clear();

                DateTime now = DateTime.Now.Date;
                db.AddParam(cmd, pnFrDate, now.AddDays(-2));
                db.AddParam(cmd, pnToDate, now.AddDays(+1).AddMilliseconds(1));

                db.FillDataTable(cmd, ds.CPMDET);

                DateTime dt2 = DateTime.Now;
                // LogMsg("Load Job Data Time:" + UtilTime.ElapsedTime(dt1, dt2));

                if ((ds != null) && (ds.CPMDET.Rows.Count > 0))
                {
                    loadSuccess = true;
                }
                else
                {
                    LogMsg("No Cpm Data.");
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (isCmdNull)
                {
                    db.CloseCommand(cmd, false);
                    cmd = null;
                }
            }
            return loadSuccess;
        }

        /// <summary>
        /// Load Job Data for given flightId. It will return True when loading success.
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="ds"></param>
        /// <param name="flightId">Flight Id to fetch the data</param>
        /// <returns></returns>
        public bool LoadData(IDbCommand cmd, ref DSCpm ds, string flnu)
        {
            bool loadSuccess = false;
            UtilDb db = UtilDb.GetInstance();
            bool isCmdNull = false;
            try
            {
                if (cmd == null)
                {
                    isCmdNull = true;
                    cmd = db.GetCommand(false);
                }
                DateTime dt1 = DateTime.Now;

                if (ds == null) ds = new DSCpm();

                string paramNamePrefix = UtilDb.GetParaNamePrefix();
                string pnFlnu = paramNamePrefix + "FLID";

                cmd.CommandText = _cmdSelCpmDet + GetDb_Tb_CpmDet()+ " WHERE FLID="+pnFlnu;
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Clear();

                db.AddParam(cmd, pnFlnu, flnu);

                db.FillDataTable(cmd, ds.CPMDET);

                if ((ds != null) && (ds.CPMDET.Rows.Count > 0))
                {
                    loadSuccess = true;
                }
                else
                {
                    LogTraceMsg("No Cpm Data for " + flnu);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (isCmdNull)
                {
                    db.CloseCommand(cmd, false);
                    cmd = null;
                }
            }

            return loadSuccess;
        }


        /// <summary>
        /// Load Cpm Data for given flightId. It will return True when loading success.
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="ds"></param>
        /// <param name="flightId">Flight Id to fetch the data</param>
        /// <returns></returns>
        public bool LoadDataSet(IDbCommand cmd, ref DataSet ds, string flnu)
        {
            bool loadSuccess = false;
            UtilDb db = UtilDb.GetInstance();
            bool isCmdNull = false;
            try
            {
                if (cmd == null)
                {
                    isCmdNull = true;
                    cmd = db.GetCommand(false);
                }
                DateTime dt1 = DateTime.Now;

                if (ds == null) ds = new DataSet();

                string paramNamePrefix = UtilDb.GetParaNamePrefix();
                string pnFlnu = paramNamePrefix + "FLID";

                cmd.CommandText = _cmdSelCpm +  GetDb_Tb_CpmDet()+ " WHERE FLID= '" + flnu + "'";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Clear();

                // db.AddParam(cmd, pnFlnu, flnu);

                db.FillDataSet(cmd, ds);

                if ((ds != null) && (ds.Tables["ITK_CPMDET"].Rows.Count > 0))
                {                   
                    loadSuccess = true;
                    LogTraceMsg("loadSuccess"+loadSuccess);
                }
                else
                {
                    LogTraceMsg("No Cpm Data for " + flnu);
                }
            }
            catch (Exception ex)
            {
				LogMsg("LoadDataSet:Err:" + flnu + "," + ex.Message);
                throw;
            }
            finally
            {
                if (isCmdNull)
                {
                    db.CloseCommand(cmd, false);
                    cmd = null;
                }
            }

            return loadSuccess;
        }
/// <summary>
/// /
/// </summary>
/// <param name="cmd"></param>
/// <param name="ds"></param>
/// <param name="urno"></param>
/// <returns></returns>
        public bool LoadDataSetForUrno(IDbCommand cmd, ref DataSet ds, string urno)
        {
            bool loadSuccess = false;
            UtilDb db = UtilDb.GetInstance();
            bool isCmdNull = false;
            try
            {
                if (cmd == null)
                {
                    isCmdNull = true;
                    cmd = db.GetCommand(false);
                }
                DateTime dt1 = DateTime.Now;

                if (ds == null) ds = new DSCpm();

                string paramNamePrefix = UtilDb.GetParaNamePrefix();
                string pnFlnu = paramNamePrefix + "FLID";

                cmd.CommandText = _cmdSelCpm + GetDb_Tb_CpmDet()+ " WHERE BREF= '" + urno + "'";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Clear();

                // db.AddParam(cmd, pnFlnu, flnu);

                db.FillDataSet(cmd, ds);

                if ((ds != null) && (ds.Tables["ITK_CPMDET"].Rows.Count > 0))
                {
                    loadSuccess = true;
                }
                else
                {
                    LogMsg("No Cpm Data for " + urno);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (isCmdNull)
                {
                    db.CloseCommand(cmd, false);
                    cmd = null;
                }
            }

            return loadSuccess;
        }

        /// <summary>
        /// Add a row of the DsCpm Dataset to given DsCpm Dataset. 
        /// If is there any row with same id will be deleted from dataset before adding the new row.
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        private bool AddDsCpmRow(DSCpm ds, DSCpm.CPMDETRow row)
        {
            bool ok = false;
            if (ds == null) throw new ApplicationException("UpdateDsCpm:Null Data.");
            string cpmId = row.URNO;
            foreach (DSCpm.CPMDETRow tRow in ds.CPMDET.Select("URNO='" + cpmId + "'"))
            {
                tRow.Delete();
            }
            ds.CPMDET.AddCPMDETRow(row);
            ok = true;
            return ok;
        }
     //   private const string DB_CPM_TABLE = "ITK_CPMDET";
        //public bool HasChanges(DateTime lastChgDt, out DateTime curChgDt)
        //{
        //    bool changed = true;
        //    bool isExist = false;
        //     curChgDt = UtilDb.GetLastUpdateDateTimeOfTable(null, DB_CPM_TABLE, out isExist);

        //    if (!isExist)
        //    {
        //        throw new ApplicationException("Not able to get the Cpm Last Update Time information.");
        //    }

        //    if (curChgDt.CompareTo(lastChgDt) > 0) changed = true;
        //    else changed = false;

        //    return changed;
        //}

        private static bool _isFetchingCpmInfo = false;

        /// <summary>
        /// Load the job information from database to common static variable dsJob.
        /// </summary>
        /// <param name="loadSuccess">True - Loading Success</param>
        /// <param name="returnACopyWhenSuccess">True-To return the copy of dsJobv when success</param>
        /// <returns>Latest job Information</returns>
        //public DSCpm LoadDSCpm(out bool loadSuccess, bool returnACopyWhenSuccess)
        //{
        //    loadSuccess = false;

        //    //LogMsg("Summarise the flight Infor");
        //    DateTime curChgDt = DEFAULT_DT;
        //    if (!_isFetchingCpmInfo)
        //    {
        //        if ((_dsCpm == null) || HasChanges(_lastCPMDetWriteDT, out curChgDt))
        //        {
        //            lock (_lockObj)
        //            {
        //                _isFetchingCpmInfo = true;
        //                try
        //                {
        //                    if ((_dsCpm == null) || HasChanges(_lastCPMDetWriteDT, out curChgDt))
        //                    {
        //                        try
        //                        {
        //                            DSCpm tempDs = new DSCpm();
        //                            if (LoadData(null, ref tempDs))
        //                            {
        //                                if ((tempDs != null) && (tempDs.CPMDET.Rows.Count > 0))
        //                                {
        //                                    loadSuccess = true;
        //                                    SetLatestInfoToDataset(tempDs, curChgDt);
        //                                    //LogTraceMsg("Get new flight info.");
        //                                }
        //                                else
        //                                {
        //                                    LogMsg("No Cpm Data");
        //                                }
        //                            }
        //                        }
        //                        catch (Exception ex)
        //                        {
        //                            LogMsg("LoadDSCpm:Err:" + ex.Message);
        //                            throw new ApplicationException("Error Accessing Cpm Information.");
        //                        }
        //                    }

        //                }
        //                catch (Exception ex)
        //                {
        //                    LogMsg("LoadDSCpm:Err:" + ex.Message);
        //                }
        //                finally
        //                {
        //                    _isFetchingCpmInfo = false;
        //                }
        //            }
        //        }
        //        else
        //        {
        //            loadSuccess = true;
        //        }
        //    }

        //    if ((loadSuccess) && (returnACopyWhenSuccess))
        //    {
        //        return (DSCpm)_dsCpm.Copy();
        //    }
        //    return _dsCpm;
        //}

        private void SetLatestInfoToDataset(DSCpm ds, DateTime curChgDt)
        {
            _dsCpm = ds;
            _lastCPMDetWriteDT = curChgDt;
        }
        
       
       
     
     

/// <summary>
/// Insert a CPM from backend.Update the status as 'I' Insert,'U' Update
/// 'D' delete in CPMDET.If the new Cpm doesn't contain all the records,the excess 
/// records will be changed to status 'D'.
/// </summary>
/// <param name="cmd"></param>
/// <param name="xmlDoc"></param>
/// <returns>status of insert</returns>
        public bool InsertACpm(IDbCommand cmd, XmlDocument xmlDoc,DateTime curDt)
        {
            bool isOk = false;
            //System.Threading.Thread.Sleep(100);
            
                try
                {
                    UtilDb db = UtilDb.GetInstance();
                    DataSet tempDs = new DataSet();
                    tempDs.Tables.Add("ITK_CPMDET");
                    
                    DataSet dsNewCpm = new DataSet();
                    XmlNodeReader reader = new XmlNodeReader(xmlDoc);
                    dsNewCpm.ReadXml(reader);
                    DataRow dataRow = dsNewCpm.Tables["CPMDET"].Rows[0];
                    string flnu = ((string)dataRow["FLNU"]).Trim();
                    string flid = "";
                    string adid = "";
                    bool hasData = false;
                    try
                    {
                        flid = CtrlFlight.GetFlightIdForUaft(cmd, flnu, UtilTime.ConvDateTimeToUfisFullDTString(curDt), out hasData, out adid);
                        if (flid == "")
                        {
                            throw new ApplicationException("No record for Flight");
                        }
                    }
                    catch (Exception ex)
                    {
						LogMsg("InsertACpm:Err:" + ex.Message);
                        throw;
                    }
                    /* Get the cpm info for giving flight id */
                    LoadDataSet(cmd, ref tempDs, flid);

                    tempDs.AcceptChanges();
                    
                   
                    int cnt = dsNewCpm.Tables["CPMDET"].Rows.Count;
                    int tempDsCnt = tempDs.Tables.Count;
                    string flightUrno = "";
                    string bref = "";
                    string loaurno = "";
                    
                    string alc2 = "";
                    int updCnt = 0;
                    bool updStatus = false;
                    ArrayList lstUrno = new ArrayList();
                    string insUpdStatus = "";
                    DataRow updRow = null;

                    if (tempDsCnt > 0)
                    {
                        foreach (DataRow row in tempDs.Tables["ITK_CPMDET"].Select("FLNU='" + flnu + "'"))
                        {
                            lstUrno.Add(row["BREF"].ToString().Trim());
                        }
                    }

                    DateTime dt1 = DateTime.Now;
                    for (int i = 0; i < cnt; i++)
                    {
                        DataRow newRow = dsNewCpm.Tables["CPMDET"].Rows[i];
                        if (i == 0)
                        {
                            flightUrno = getValueFromCPM(newRow, "FLNU");
                            /*
                             * To be added to get the iTrekFlightId for the backend
                            GetITrekFlightIdForUaft();
                             * */
                            alc2 = getValueFromCPM(newRow, "ALC2");
                        }
                        bref = getValueFromCPM(newRow, "URNO");

                        if (bref != "")
                        {
                            if (lstUrno.Count > 0)
                            {
                                if (lstUrno.Contains(bref))
                                {
                                    lstUrno.Remove(bref);
                                    insUpdStatus = "U";
                                }
                                else
                                {
                                    insUpdStatus = "I";

                                }
                            }
                            else
                            {
                                insUpdStatus = "I";
                            }
                            string uldNo = getValueFromCPM(newRow, "ULDN");
                            string apc3 = "";
                            string cont = "";
                            string posf = "";
                            string valu = "";
                            string trimcont = "";
                            string dssn = "";
                          
                            try
                            {
                                apc3 = getValueFromCPM(newRow, "APC3");
                                try
                                {
                                    cont = getValueFromCPM(newRow, "CONT").ToUpper();
                                }
                                catch (Exception)
                                {


                                }
                                posf = getValueFromCPM(newRow, "POSF");
                                valu = getValueFromCPM(newRow, "VALU");
                                dssn = getValueFromCPM(newRow, "DSSN");

                            }
                            catch (Exception ex)
                            {
                                LogMsg("UpdateCPMInfoFromBackendToCPMXmlFile:Error:" + ex.Message);
                            }
                            string uldText = posf + "/" + uldNo + "/" + apc3 + "/" + valu + "/" + cont;




                            if (insUpdStatus == "I")
                            {
                                updRow = tempDs.Tables["ITK_CPMDET"].NewRow();
                                loaurno = UtilDb.GenUrid(cmd, ProcIds.CPM, false, dt1);
                            }
                            else if (insUpdStatus == "U")
                            {

                                updRow = tempDs.Tables["ITK_CPMDET"].Select("BREF='" + bref + "'")[0];
                                loaurno = updRow["URNO"].ToString();

                            }
                            try
                            {
                                updRow["URNO"] = loaurno;
                                updRow["BREF"] = bref;
                                updRow["APC3"] = apc3;
                                updRow["CONT"] = cont;
                                updRow["DSSN"] = dssn;
                                updRow["FLNU"] = flightUrno;
                                updRow["POSF"] = posf;
                                //updRow.ULDN = newRow["ULDN"].ToString();
                                updRow["ULDN"] = iTrekUtils.iTUtils.removeBadCharacters(uldText);
                                updRow["VALU"] = valu;
                                updRow["STAT"] = insUpdStatus;
                                updRow["ALC2"] = alc2;
                                updRow["FLID"] = flid;                                
                                updRow["LSTU"] = curDt;

                                if (alc2 != "" && cont != "")
                                {
                                    if (cont.Length > 1)
                                    {
                                        trimcont = cont.Substring(0, 2);
                                    }
                                    else
                                    {
                                        trimcont = cont.Substring(0, 1);
                                    }

                                    updRow["PRTY"] = CtrlCpmPriority.GetPriority(alc2, trimcont).ToString();
                                   // updRow["PRTY"] = "1";
                                }
                                updStatus = true;

                            }

                            catch (Exception ex)
                            {
                                updStatus = false;
                                LogTraceMsg("UpdateCPMInfoFromBackendToCPMXmlFile" + ex.Message);
                                throw new ApplicationException(ex.Message);
                            }
                            if (updStatus)
                            {
                                if (insUpdStatus == "I")
                                {
                                    updRow["CDAT"] = curDt;
                                    tempDs.Tables["ITK_CPMDET"].Rows.Add(updRow);
                                }
                                updCnt++;
                            }

                        }
                    }
                    foreach (string balUrno in lstUrno)
                    {

                        updRow = tempDs.Tables["ITK_CPMDET"].Select("BREF='" + balUrno + "'")[0];
                        updRow["STAT"] = "D";
                        updCnt++;

                    }
                    if (updCnt > 0)
                    {
                        cmd.CommandText = _cmdSelCpm + " WHERE FLNU= '" + flnu + "'";
                        //isOk = updateCPMDetTable(db, cmd, tempDs);
                        
                        int insCnt=-1;
                        int updatedCnt=-1;
                        int delCnt=-1;
                        isOk=UtilDb.InsertUpdateDataToDb(cmd, tempDs.Tables["ITK_CPMDET"], DB_Info.DB_T_CPMDET, "", "auto", curDt, out insCnt, out updatedCnt, out delCnt);

                    }

                }
                catch (Exception ex)
                {
                    LogTraceMsg("InsertACpm:err:" + ex.Message);
                    throw;
                    
                }
                finally
                {

                }
                return isOk;
            
        }
        /// <summary>
        /// Insert BRS message.Status 'I' for insert and 'U' for Update
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="xmlDoc"></param>
        /// <returns></returns>

        public bool InsertABrs(IDbCommand cmd, XmlDocument xmlDoc,DateTime curDt)
        {

            bool isOk = false;
            //System.Threading.Thread.Sleep(100);
            lock (_lockObjCpmDet)
            {
                try
                {

                    UtilDb db = UtilDb.GetInstance();
                    //DSJob tempDs = (DSJob)dsJob.Copy();
                    DataSet tempDs = new DataSet();
                    tempDs.Tables.Add("ITK_CPMDET");
                   
                    DataSet dsNewCpm = new DataSet();
                    XmlNodeReader reader = new XmlNodeReader(xmlDoc);
                    dsNewCpm.ReadXml(reader);
                    DataRow dataRow = dsNewCpm.Tables["CPMDET"].Rows[0];
                    string flnu = ((string)dataRow["FLNU"]).Trim();
                    string flid = "";
                    string adid = "";
                    bool hasData = false;
                    try
                    {
                        flid = CtrlFlight.GetFlightIdForUaft(cmd, flnu, UtilTime.ConvDateTimeToUfisFullDTString(curDt), out hasData, out adid);
                        if (flid == "")
                        {

                            throw new ApplicationException("No record for Flight");
                        }
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                    LoadDataSet(cmd, ref tempDs, flid);
                    tempDs.AcceptChanges();
                    int cnt = dsNewCpm.Tables["CPMDET"].Rows.Count;
                    int tempDsCnt = tempDs.Tables.Count;
                    string flightUrno = "";
                    string bref = "";
                    string loaurno = "";
                   
                    string alc2 = "";
                    int updCnt = 0;
                    bool updStatus = false;
                    ArrayList lstUrno = new ArrayList();
                    string insUpdStatus = "";
                    DataRow updRow = null;

                    if (tempDsCnt > 0)
                    {
                        foreach (DataRow row in tempDs.Tables["ITK_CPMDET"].Select("FLID='" + flid + "'"))
                        {
                            lstUrno.Add(row["BREF"].ToString().Trim());

                        }
                    }

                    DateTime dt1 = DateTime.Now;
                    for (int i = 0; i < cnt; i++)
                    {
                        DataRow newRow = dsNewCpm.Tables["CPMDET"].Rows[i];
                        if (i == 0)
                        {
                            flightUrno = getValueFromCPM(newRow, "FLNU");
                            /*
                             * To be added to get the iTrekFlightId for the backend
                            GetITrekFlightIdForUaft();
                             * */
                            alc2 = getValueFromCPM(newRow, "ALC2");
                        }
                        bref = getValueFromCPM(newRow, "URNO");

                        if (bref != "")
                        {
                            if (lstUrno.Count > 0)
                            {
                                if (lstUrno.Contains(bref))
                                {

                                    insUpdStatus = "U";
                                }
                                else
                                {
                                    insUpdStatus = "I";

                                }
                            }
                            else
                            {
                                insUpdStatus = "I";
                            }
                            string uldNo = getValueFromCPM(newRow, "ULDN");
                            string apc3 = "";
                            string cont = "";
                            string posf = "";
                            string valu = "";
                            string trimcont = "";
                            string dssn = "";
                          
                            try
                            {
                                apc3 = getValueFromCPM(newRow, "APC3");
                                try
                                {
                                    cont = getValueFromCPM(newRow, "CONT").ToUpper();
                                }
                                catch (Exception)
                                {


                                }
                                posf = getValueFromCPM(newRow, "POSF");
                                valu = getValueFromCPM(newRow, "VALU");
                                dssn = getValueFromCPM(newRow, "DSSN");

                            }
                            catch (Exception ex)
                            {
                                LogMsg("UpdateCPMInfoFromBackendToCPMXmlFile:Error:" + ex.Message);
                            }
                            string uldText = posf + "/" + uldNo + "/" + apc3 + "/" + valu + "/" + cont;




                            if (insUpdStatus == "I")
                            {
                                updRow = tempDs.Tables["ITK_CPMDET"].NewRow();
                                loaurno = UtilDb.GenUrid(cmd, ProcIds.CPM, false, dt1);
                            }
                            else if (insUpdStatus == "U")
                            {

                                updRow = tempDs.Tables["ITK_CPMDET"].Select("BREF='" + bref + "'")[0];
                                loaurno = updRow["URNO"].ToString();

                            }
                            try
                            {
                                updRow["URNO"] = loaurno;
                                updRow["BREF"] = bref;
                                updRow["APC3"] = apc3;
                                updRow["CONT"] = cont;
                                updRow["DSSN"] = dssn;
                                updRow["FLNU"] = flightUrno;
                                updRow["POSF"] = posf;
                                //updRow.ULDN = newRow["ULDN"].ToString();
                                updRow["ULDN"] = iTrekUtils.iTUtils.removeBadCharacters(uldText);
                                updRow["VALU"] = valu;
                                updRow["STAT"] = insUpdStatus;
                                updRow["ALC2"] = alc2;
                                updRow["FLID"] = flid;
                               
                                updRow["LSTU"] = curDt;

                                if (alc2 != "" && cont != "")
                                {
                                    if (cont.Length > 1)
                                    {
                                        trimcont = cont.Substring(0, 2);
                                    }
                                    else
                                    {
                                        trimcont = cont.Substring(0, 1);
                                    }

                                }
                                updStatus = true;

                            }

                            catch (Exception ex)
                            {
                                updStatus = false;
                                LogTraceMsg("UpdateCPMInfoFromBackendToCPMXmlFile" + ex.Message);
                                throw new ApplicationException(ex.Message);
                            }
                            if (updStatus)
                            {
                                if (insUpdStatus == "I")
                                {
                                    updRow["CDAT"] = curDt;
                                    tempDs.Tables["ITK_CPMDET"].Rows.Add(updRow);
                                }
                                updCnt++;
                            }

                        }
                    }

                    if (updCnt > 0)
                    {

                        cmd.CommandText = _cmdSelCpm + " WHERE FLID= '" + flid + "'";
                       // isOk = updateCPMDetTable(db, cmd, tempDs);
                        int insCnt = -1;
                        int updatedCnt = -1;
                        int delCnt = -1;
                        isOk = UtilDb.InsertUpdateDataToDb(cmd, tempDs.Tables["ITK_CPMDET"], DB_Info.DB_T_CPMDET, "", "auto", curDt, out insCnt, out updatedCnt, out delCnt);

                    }

                }
                catch (Exception ex)
                {
                    LogTraceMsg("InsertABrs:err:" + ex.Message);
                    throw;
                }
                finally
                {

                }
                return isOk;
            }
        }

        private bool updateCPMDetTable(UtilDb db, IDbCommand cmd, DataSet tempDs)
        {
            bool isOk = false;

            try
            {
                DbCommandBuilder builder = db.GetDbCommandBuilder(db.GetDbDataAdapter(cmd));
                builder.DataAdapter.Update(tempDs, GetDb_Tb_CpmDet());
                tempDs.AcceptChanges();
                isOk = true;
            }
            catch (Exception ex)
            {
                LogTraceMsg("updateCPMDetTable"+ex.Message);
                throw new ApplicationException("Unable to update DB" + ex.Message);
            }

            return isOk;
        }
        /// <summary>
        /// Update BRS records in CPMDET for delete action.The Status will be updated as 'D'
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="brsFieldXml"></param>
        /// <returns>Status of Update</returns>
        public bool UpdateBRSFieldsToDB(IDbCommand cmd, XmlDocument brsFieldXml,DateTime curDt)
        {
            string action = "";
            string urno = "";
            bool isOk = false;

            lock (_lockObj)
            {
                try
                {

                    UtilDb db = UtilDb.GetInstance();
                    DataSet ds = null;
                    int updCnt = 0;
                    try
                    {
                        ds = new DataSet();
                        ds.Tables.Add("ITK_CPMDET");
                        XmlElement xmlElement = brsFieldXml.DocumentElement;
                        action = xmlElement.FirstChild.InnerXml.Trim();
                        urno = xmlElement.LastChild.InnerXml.Trim();
                        if (!LoadDataSetForUrno(cmd, ref ds, urno))
                        {
                            throw new ApplicationException("Unable to get the Cpm Information.");
                        }
                    }
                    catch (ApplicationException ex) { throw ex; }
                    catch (Exception ex)
                    {
                        throw new ApplicationException("Unable to get the Cpm Information due to " + ex.Message);
                    }
                    try
                    {

                        if (action == "DELETE")
                        {

                            foreach (DataRow row in ds.Tables["ITK_CPMDET"].Select("BREF='" + urno + "'"))
                            {

                                row["STAT"] = "D";
                                row["LSTU"] = curDt;
                                updCnt++;

                            }
                            if (updCnt > 0)
                            {
                                cmd.CommandText = _cmdSelCpm + " WHERE URNO= '" + urno + "'";
                               // isOk = updateCPMDetTable(db, cmd, ds);
                                int insCnt = -1;
                                int updatedCnt = -1;
                                int delCnt = -1;
                                isOk = UtilDb.InsertUpdateDataToDb(cmd, ds.Tables["ITK_CPMDET"], DB_Info.DB_T_CPMDET, "", "auto", curDt, out insCnt, out updatedCnt, out delCnt);
                            }
                        }

                    }
                    catch (Exception ex1)
                    {

                        LogTraceMsg("UpdateBRSFieldsToContainerXmlFile:Err:" + ex1.Message);

                    }

                }
                catch (Exception ex)
                {
                    LogMsg("UpdateBRSFieldsToContainerXmlFile:Err:" + ex.Message);
                }
                finally
                {

                }
            }
            return isOk;

        }
        /// <summary>
        /// Update the cpm indicator for CPMDET.
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="lstUld">list of urnos which are send</param>
        /// <param name="flnu"></param>
        /// <returns></returns>

        public bool UpdateSendIndicator(IDbCommand cmd, ArrayList lstUld,string flid,string sentBy,DateTime curDt)
        {
           
            bool isOk = false;

            lock (_lockObj)
            {
                try
                {

                    UtilDb db = UtilDb.GetInstance();
                    DataSet ds = null;
                    int updCnt = 0;
                    try
                    {
                        ds = new DataSet();
                        ds.Tables.Add("ITK_CPMDET");
                        if (!LoadDataSet(cmd, ref ds, flid))
                        {
                            throw new ApplicationException("Unable to get the Cpm Information.");
                        }
                    }
                    catch (ApplicationException ex) { throw ex; }
                    catch (Exception ex)
                    {
                        throw new ApplicationException("Unable to get the Cpm Information due to " + ex.Message);
                    }
                    try
                    {
                       // LogTraceMsg("lstUld" + lstUld.Count);

                        foreach(string urno in lstUld)
                        {
                            //LogTraceMsg("Urno:" + urno);
                            DataRow dRow = ds.Tables["ITK_CPMDET"].Select("URNO='" + urno + "'")[0];

                            if (dRow != null)
                            {
                                LogTraceMsg("Urno updated:" + urno);
                                dRow["STAT"] = "S";
                                dRow["USEU"] = sentBy;
                                dRow["LSTU"] = curDt;
                                updCnt++;
                            }

                            
                            
                        }
                       // LogTraceMsg("updCnt" + updCnt);
                        if (updCnt > 0)
                            {
                                cmd.CommandText = _cmdSelCpm + " WHERE FLID= '" + flid + "'";
                                //LogTraceMsg(" cmd.CommandText" + cmd.CommandText);
                               // isOk = updateCPMDetTable(db, cmd, ds);
                                int insCnt = -1;
                                int updatedCnt = -1;
                                int delCnt = -1;
                                isOk = UtilDb.InsertUpdateDataToDb(cmd, ds.Tables["ITK_CPMDET"], DB_Info.DB_T_CPMDET, "", sentBy, curDt, out insCnt, out updatedCnt, out delCnt);
                            }
                       

                    }
                    catch (Exception ex1)
                    {

                        LogTraceMsg("UpdateSendIndicator:Err:" + ex1.Message);

                    }

                }
                catch (Exception ex)
                {
                    LogMsg("UpdateSendIndicator Main:Err:" + ex.Message);
                }
                finally
                {

                }
            }
            return isOk;

        }

        /// <summary>
        /// Insert new records.To be used when BO enters ULD from Handheld Prepare Container screen.
        /// check for records with same uld number.If exists, do not insert.
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="uaft">Urno of Flight</param>
        /// <param name="lstUldNo">list of uld Numbers</param>
        /// <returns>status</returns>
        public bool InsertNewContainerIntoDB(IDbCommand cmd,string flid, ArrayList lstUldNo,string userId,DateTime curDt)
        {
            bool isOk = false;
            lock (_lockObj)
            {
              
                int updCnt = 0;
                UtilDb db = UtilDb.GetInstance();
                DataRow updRow = null;
                string loaurno = "";
                string flnu = "";

                try
                {
                    flnu = CtrlFlight.GetUaftForFlightId(cmd, flid);

                }
                catch (Exception)
                {


                }

                try
                {

                    DataSet tempDs = new DataSet();
                    tempDs.Tables.Add("ITK_CPMDET");
                    LoadDataSet(cmd, ref tempDs, flid);
                    
                  
                    foreach (string uld in lstUldNo)
                    {
                        DataRow[] row = null;
                        if (tempDs != null)
                        {
                            row = tempDs.Tables["ITK_CPMDET"].Select("ULDN='" + uld + "'");
                        }

                        if ( row == null||row.Length == 0)
                        {
                          // LogTraceMsg("row.Length");
                            updRow = tempDs.Tables["ITK_CPMDET"].NewRow();
                            DateTime dt1 = DateTime.Now;
                            loaurno = UtilDb.GenUrid(cmd, ProcIds.CPM, false, dt1);                          
                            updRow["URNO"] = loaurno;
                            updRow["FLID"] = flid;
                            updRow["ULDN"] = iTrekUtils.iTUtils.removeBadCharacters(uld);
                            updRow["STAT"] = "I";
                            updRow["DSSN"] = "BRS";
                            updRow["FLNU"] = flnu;
                            updRow["CDAT"] = curDt;
                            updRow["LSTU"] = curDt;
                            updRow["USEC"] = userId;
                            updRow["USEU"] = userId;
                            

                            tempDs.Tables["ITK_CPMDET"].Rows.Add(updRow);
                            updCnt++;
                           // LogTraceMsg("updCnt" + updCnt);

                        }
                    }
                    if (updCnt > 0)
                    {
                        cmd.CommandText = _cmdSelCpm + " WHERE FLID= '" + flid + "'";
                        //LogTraceMsg("cmd.CommandText" + cmd.CommandText);
                       // isOk = updateCPMDetTable(db, cmd, tempDs);
                        int insCnt = -1;
                        int updatedCnt = -1;
                        int delCnt = -1;
                        isOk = UtilDb.InsertUpdateDataToDb(cmd, tempDs.Tables["ITK_CPMDET"], DB_Info.DB_T_CPMDET, "", userId, curDt, out insCnt, out updatedCnt, out delCnt);
                    }
                }
                catch (Exception ex)
                {
                    LogTraceMsg("createNewContainerIntoContainerXml:Err:" + ex.Message);
                    throw;
                }
                


            }
            return isOk;

        }

        //public DSCpm RetrieveSentCPMUldForAFlightForTrips(IDbCommand cmd, string flightUrNo)
        //{
        //    DSCpm ds = new DSCpm();
        //    DSCpm tempDs = new DSCpm();
        //    LoadData(cmd, ref tempDs, flightUrNo);
        //    foreach (DSCpm.CPMDETRow row in tempDs.CPMDET.Select("FLNU='" + flightUrNo + "' AND SENT_IND='S'"))
        //    {



        //        ds.CPMDET.LoadDataRow(row.ItemArray, true);
        //    }



        //    return ds;
        //}

    }
}
