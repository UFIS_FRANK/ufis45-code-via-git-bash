using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data;

using iTrekXML;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.Util;
using System.Collections;
using MM.UtilLib;

namespace UFIS.ITrekLib.DB
{

    public class DBJob
    {
        private static DSJob _dsJob = null;
        private static DSJob _dsJobHist = null;
        private static DSJob _dsPfc = null;
        private static DBJob _dbJob = null;

        private readonly static DateTime _defaultDT = new DateTime(1, 1, 1);
        private static DateTime _lastJobWriteTime = _defaultDT;
        private static DateTime _lastPfcWriteTime = _defaultDT;
        private static DateTime _lastJobSummWriteTime = _defaultDT;
        private static DateTime _lastJobSummHistWriteTime = _defaultDT;
        private static string _fnJobXmlPath = null;
        private static string _fnPfcXmlPath = null;
        private static string _fnJobSummXmlPath = null;
        private static string _fnJobSummHistXmlPath = null;

        private static object _lockObj = new object();//to use in synchronisation for single access
        private static object _lockJobObj = new object();
        private static object _lockJobHistObj = new object();
        private static object _lockPfcObj = new object();

        private static DateTime _lastJobRefreshDT = _defaultDT;
        private static DateTime _lastJobHistRefreshDT = _defaultDT;
        private static DateTime _lastPfcRefreshDT = _defaultDT;


        private DBJob() { }

        public void Init()
        {
            //_dsJob = null;
            _fnJobXmlPath = null;
            _fnPfcXmlPath = null;
            _fnJobSummXmlPath = null;
            _fnJobSummHistXmlPath = null;
            _lastJobWriteTime = _defaultDT;
            _lastPfcWriteTime = _defaultDT;
            _lastJobSummWriteTime = _defaultDT;
            _lastJobSummHistWriteTime = _defaultDT;
            _lastJobRefreshDT = _defaultDT;
            _lastJobHistRefreshDT = _defaultDT;
            _lastPfcRefreshDT = _defaultDT;
        }

        private string fnJobSummXmlPath
        {
            get
            {
                if ((_fnJobSummXmlPath == null) || (_fnJobSummXmlPath == ""))
                {
                    iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
                    _fnJobSummXmlPath = path.GetJobsSummFilePath();
                }

                return _fnJobSummXmlPath;
            }
        }

        private string fnJobSummHistXmlPath
        {
            get
            {
                if ((_fnJobSummHistXmlPath == null) || (_fnJobSummHistXmlPath == ""))
                {
                    iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
                    _fnJobSummHistXmlPath = path.GetJobsSummHistFilePath();
                }

                return _fnJobSummHistXmlPath;
            }
        }

        private string fnJobXmlPath
        {
            get
            {
                if ((_fnJobXmlPath == null) || (_fnJobXmlPath == ""))
                {
                    iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
                    //_fnJobXmlPath = path.GetJobsFilePath();
                    _fnJobXmlPath = path.GetUpdateableJobsFilePath();
                }

                return _fnJobXmlPath;
            }
        }

        private string fnPfcXmlPath
        {
            get
            {
                if ((_fnPfcXmlPath == null) || (_fnPfcXmlPath == ""))
                {
                    iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
                    _fnPfcXmlPath = path.GetPFCsFilePath();
                }

                return _fnPfcXmlPath;
            }
        }


        private DSJob dsJob
        {
            get
            {
                int timeElapse = Math.Abs(UtilTime.DiffInSec(DateTime.Now, _lastJobRefreshDT));
                if (timeElapse > 3)
                {
                    LogTraceMsg("Job-TimeElapse:" + timeElapse);
                    LoadDsJob_JobTable();
                    _lastJobRefreshDT = DateTime.Now;
                }

                return _dsJob;
            }
        }

        private DSJob dsPfc
        {
            get
            {
                int timeElapse = Math.Abs(UtilTime.DiffInSec(DateTime.Now, _lastPfcRefreshDT));
                if (timeElapse > 6000)
                {
                    LogTraceMsg("PFC-TimeElapse:" + timeElapse);
                    LoadDsJob_PfcTable();
                    _lastPfcRefreshDT = DateTime.Now;
                }

                return _dsPfc;
            }
        }

        private DSJob dsJobHist
        {
            get
            {
                int timeElapse = Math.Abs(UtilTime.DiffInSec(DateTime.Now, _lastJobHistRefreshDT));
                if (timeElapse > 600)
                {
                    LogTraceMsg("Hist-TimeElapse:" + timeElapse);
                    LoadDsJob_JobHistTable();
                    _lastJobHistRefreshDT = DateTime.Now;
                }
                return _dsJobHist;
            }
        }

        //private void LoadDSJob()
        //{
        //    if (IsNeedToUpdateData())
        //    {
        //        lock (_lockObj)
        //        {
        //            try
        //            {
        //                //if (IsNeedToUpdateData())
        //                {
        //                    if (_dsJob == null) _dsJob = new DSJob();
        //                    try
        //                    {
        //                        LoadDsJob_JobTable();
        //                        LoadDsJob_PfcTable();
        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        LogMsg(ex.Message);
        //                        //throw new ApplicationException( "Unable to get staff job assignment information" );
        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                LogMsg("LoadDSJob:Err:" + ex.Message);
        //            }
        //        }
        //    }
        //}

        private void LogTraceMsg(string msg)
        {
            UtilLog.LogToTraceFile("DBJob", msg);
        }

        private void LogMsg(string msg)
        {
            //UtilLog.LogToGenericEventLog("DBJob", msg
            //UtilLog.LogToTraceFile("DBJob", msg);
           try
           {
              MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("DBJob", msg);
           }
           catch (Exception)
           {
              UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DBJob", msg);
           }
        }

        //private bool IsNeedToUpdateData()
        //{
        //    bool needToUpdate = true;
        //    if ((_dsJob == null) ||
        //        (IsNeedToUpdateJobTable()) ||
        //        (IsNeedToUpdatePfcTable())) needToUpdate = true;
        //    else needToUpdate = false;

        //    return needToUpdate;
        //}

        //private bool IsNeedToUpdateJobTable()
        //{
        //    bool needToUpdate = false;
        //    FileInfo fiJobXml = new FileInfo(fnJobXmlPath);
        //    DateTime curDT = fiJobXml.LastWriteTime;
        //    if (curDT.CompareTo(_lastJobWriteTime) != 0) needToUpdate = true;
        //    return needToUpdate;
        //}

        //private bool IsNeedToUpdatePfcTable()
        //{
        //    bool needToUpdate = false;
        //    FileInfo fiPfcXml = new FileInfo(fnPfcXmlPath);
        //    DateTime curDT = fiPfcXml.LastWriteTime;
        //    if (curDT.CompareTo(_lastPfcWriteTime) != 0) needToUpdate = true;
        //    return needToUpdate;
        //}

        public void LoadJobDataToJobSummXmlFile()
        {
            FileInfo fiXml = new FileInfo(fnJobXmlPath);
            DateTime curDT = fiXml.LastWriteTime;
            if (curDT.CompareTo(_lastJobWriteTime) != 0)
            {
                //LogTraceMsg("Before Lock");
                lock (_lockJobObj)
                {
                    try
                    {
                        fiXml = new FileInfo(fnJobXmlPath);
                        curDT = fiXml.LastWriteTime;//check the time again, in case of lock
                        if (curDT.CompareTo(_lastJobWriteTime) != 0)
                        {
                            LockJobSummForWriting();

                            DataSet ds = new DataSet();
                            ds.ReadXml(fnJobXmlPath);
                            if (ds.Tables.Contains("JOB"))
                            {
                                LogTraceMsg("Before creating summary");
                                DSJob dsJobSumm = new DSJob();
                                FileInfo fiSummXml = new FileInfo(fnJobSummXmlPath);
                                if (fiSummXml.Exists)
                                {
                                    try
                                    {
                                        dsJobSumm.JOB.ReadXml(fnJobSummXmlPath);
                                    }
                                    catch (Exception readEx)
                                    {

                                        LogTraceMsg("Exception while reading summary Job" + readEx.Message);
                                        dsJobSumm = new DSJob();
                                        dsJobSumm.JOB.WriteXml(fnJobSummXmlPath);
                                    }
                                }
                                int cnt = ds.Tables["JOB"].Rows.Count;
                                for (int i = 0; i < cnt; i++)
                                {
                                    DataRow row = ds.Tables["JOB"].Rows[i];
                                    string jobUrno = (string)row["URNO"];
                                    if (jobUrno != "")
                                    {
                                        DeleteAJob(dsJobSumm, jobUrno);
                                        DSJob.JOBRow jRow = dsJobSumm.JOB.NewJOBRow();
                                        jRow.EMPTYPE = (string)row["EMPTYPE"];
                                        jRow.FINM = (string)row["FINM"];
                                        jRow.LANM = (string)row["LANM"];
                                        jRow.PENO = (string)row["PENO"];
                                        jRow.UAFT = (string)row["UAFT"];
                                        jRow.URNO = jobUrno;
                                        try
                                        {
                                            jRow.FCCO = (string)row["FCCO"];
                                            jRow.UTPL = (string)row["UTPL"];
                                        }
                                        catch
                                        {
                                        }
                                        dsJobSumm.JOB.AddJOBRow(jRow);
                                    }
                                }
                                dsJobSumm.JOB.AcceptChanges();
                                dsJobSumm.JOB.WriteXml(fnJobSummXmlPath);
                                LogTraceMsg("After creating summary");
                            }
                            _lastJobWriteTime = curDT;
                        }
                    }
                    catch (Exception ex)
                    {
                        LogMsg("LoadJobDataToJobSummXmlFile:err:" + ex.Message);
                    }
                    finally
                    {
                        ReleaseJobSummAfterWrite();
                    }
                }

            }
        }

        private void DeleteAJob(DSJob ds, string jobUrno)
        {
            try
            {
                ds.JOB.Select("URNO='" + jobUrno + "'")[0].Delete();
            }
            catch
            {
            }
        }

        public DSJob LoadDsJob_JobTable()
        {
            //LoadJobDataToJobSummXmlFile();
            FileInfo fiXml = new FileInfo(fnJobSummXmlPath);
            DateTime curDT = fiXml.LastWriteTime;
            if (!fiXml.Exists) throw new ApplicationException("Job information missing. Please inform to administrator.");

            if ((_dsJob == null) || (curDT.CompareTo(_lastJobSummWriteTime) != 0))
            {
                lock (_lockJobObj)
                {
                    try
                    {
                        fiXml = new FileInfo(fnJobSummXmlPath);
                        curDT = fiXml.LastWriteTime;//check the time again, in case of lock
                        if ((_dsJob == null) || (curDT.CompareTo(_lastJobSummWriteTime) != 0))
                        {
                            try
                            {
                                DSJob tempDs = new DSJob();

                                tempDs.JOB.ReadXml(fnJobSummXmlPath);
                                
                                if (_dsJob == null)
                                {
                                    _dsJob = tempDs;
                                }
                                else
                                {
                                    if (tempDs.JOB.Rows.Count > 0)
                                    {
                                        tempDs.PFC.Merge(_dsJob.PFC);
                                        _dsJob = tempDs;
                                    }
                                }
                                _lastJobSummWriteTime = curDT;
                            }
                            catch (Exception ex)
                            {
                                LogMsg(ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogMsg("LoadDsJob_JobTable:err:" + ex.Message);
                        if (ex.InnerException != null)
                        {
                            LogMsg("LoadDsJob_JobTable:err:Inner:" + ex.InnerException.Message);
                        }
                    }

                }
            }
            if (_dsJob == null) _dsJob = new DSJob();
            return _dsJob;
        }


        private void LoadDsJob_PfcTable()
        {
            FileInfo fiXml = new FileInfo(fnPfcXmlPath);
            DateTime curDT = fiXml.LastWriteTime;
            if (!fiXml.Exists)
            {
                //throw new ApplicationException("PFC information missing. Please inform to administrator.");
                return;
            }

            if ((_dsPfc == null) || (curDT.CompareTo(_lastPfcWriteTime) != 0))
            {
                lock (_lockPfcObj)
                {
                    try
                    {
                        curDT = fiXml.LastWriteTime;//check the time again, in case of lock
                        if ((_dsPfc == null) || (curDT.CompareTo(_lastPfcWriteTime) != 0))
                        {
                            try
                            {
                                DSJob tempDs = new DSJob();
                                tempDs.PFC.ReadXml(fnPfcXmlPath);
                                if (_dsPfc == null)
                                {
                                    _dsPfc = tempDs;
                                }
                                else
                                {
                                    if (tempDs.PFC.Rows.Count > 0)
                                    {
                                        _dsPfc = tempDs;
                                    }
                                }
                                _lastPfcWriteTime = curDT;
                            }
                            catch (Exception ex)
                            {
                                LogMsg("LoadDsJob_PfcTable:Err:" + ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogMsg("LoadDsJob_PfcTable:Err:" + ex.Message);
                    }
                }
            }
        }

        public static DBJob GetInstance()
        {
            if (_dbJob == null)
            {
                _dbJob = new DBJob();
            }
            return _dbJob;
        }

        public DSJob RetrieveStaffInfoForAStaff(string staffId)
        {
            DSJob ds = new DSJob();
           
            //to avoid calling the "property get method" when doing foreach loop.
            bool found = false;
            try
            {
                if (!found)
                {
                    DSJob tempDs = (DSJob)dsJob.Copy();//assign to local variable, 
                    foreach (DSJob.JOBRow row in tempDs.JOB.Select("PENO='" + staffId + "'"))
                    {
                        found = true;
                        ds.JOB.LoadDataRow(row.ItemArray, true);
                    }
                }
            }
            catch (Exception ex)
            {
                LogMsg("RetrieveStaffInfoForAStaff:Err:Staff," + staffId + "," + ex.Message);
            }
            try
            {
                if (!found)
                {
                    DSJob tempDs = (DSJob)dsJobHist.Copy();//assign to local variable, 
                    foreach (DSJob.JOBRow row in tempDs.JOB.Select("PENO='" + staffId + "'"))
                    {
                        found = true;
                        ds.JOB.LoadDataRow(row.ItemArray, true);
                    }
                }
            }
            catch (Exception ex)
            {
                LogMsg("RetrieveStaffInfoForAStaff:Err:Staff," + staffId + "," + ex.Message);
            }

            ds.PFC.Merge(dsPfc.PFC);
            return ds;
        }

        public DSJob RetrieveStaffsForAFlight(string flightUrNo)
        {
            DSJob ds = new DSJob();
            bool found = false;

            try
            {
                if (!found)
                {
                    DSJob tempDs = (DSJob)dsJob.Copy();//assign to local variable, 
                    //to avoid calling the "property get method" when doing foreach loop.

                    foreach (DSJob.JOBRow row in tempDs.JOB.Select("UAFT='" + flightUrNo + "'"))
                    {
                        ds.JOB.LoadDataRow(row.ItemArray, true);
                        found = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LogMsg("RetrieveStaffsForAFlight:Err:Flno," + flightUrNo + "," + ex.Message);
            }

            try
            {
                if (!found)
                {
                    DSJob tempDs = (DSJob)dsJobHist.Copy();//assign to local variable, 
                    //to avoid calling the "property get method" when doing foreach loop.

                    foreach (DSJob.JOBRow row in tempDs.JOB.Select("UAFT='" + flightUrNo + "'"))
                    {
                        ds.JOB.LoadDataRow(row.ItemArray, true);
                        found = true;
                    }
                }
            }
            catch (Exception ex)
            {
                LogMsg("RetrieveStaffsForAFlight:Err:HFlno," + flightUrNo + "," + ex.Message);
            }

            ds.PFC.Merge(dsPfc.PFC);
            return ds;
        }

        public string GetAOStaffId(string flightUrNo, out string aoFirstName, out string aoLastName, out string aoPENO)
        {
            string aoId = "";
            try
            {
                DSJob ds = RetrieveStaffsForAFlight(flightUrNo);
                DSJob.JOBRow row = (DSJob.JOBRow)ds.JOB.Select("AO='Y'")[0];
                aoId = row.URNO;
                aoFirstName = row.FINM;
                aoLastName = row.LANM;
                aoPENO = row.PENO;
            }
            catch (Exception ex)
            {
                LogMsg("Unable to get AO Staff Id for flight " + flightUrNo + " due to " + ex.Message);
                throw new ApplicationException("No AO for the flight");
            }
            return aoId;
        }

        public int GetStaffCount(string flightUrNo)
        {
            int cnt = -1;
            try
            {
                DSJob ds = RetrieveStaffsForAFlight(flightUrNo);
                cnt = ds.JOB.Rows.Count;

            }
            catch (Exception ex)
            {
                LogMsg("Unable to get Staff count for flight " + flightUrNo + " due to " + ex.Message);
            }
            return cnt;
        }

        public void deleteOldJobs(ArrayList alFlightUrno)
        {
            lock (_lockJobObj)
            {
                try
                {
                    LockJobSummForWriting();
                    //DSJob tempDs = (DSJob)dsJob.Copy();
                    DSJob tempDs = new DSJob();
                    tempDs.ReadXml(fnJobSummXmlPath);
                    bool hasChanges = false;
                    foreach (string urno in alFlightUrno)
                    {
                        foreach (DSJob.JOBRow row in tempDs.JOB.Select("UAFT='" + urno + "'"))
                        {
                            tempDs.JOB.RemoveJOBRow(row);
                            hasChanges = true;
                        }
                    }
                    if (hasChanges)
                    {
                        tempDs.AcceptChanges();
                        tempDs.WriteXml(fnJobSummXmlPath);
                    }
                }
                catch (Exception ex)
                {
                    LogMsg("deleteOldJobs:error:" + ex.Message);
                }
                finally
                {
                    ReleaseJobSummAfterWrite();
                }

                try
                {
                    DSJob tempDs = new DSJob();
                    tempDs.ReadXml(fnJobSummHistXmlPath);
                    bool hasChanges = false;
                    foreach (string urno in alFlightUrno)
                    {
                        foreach (DSJob.JOBRow row in tempDs.JOB.Select("UAFT='" + urno + "'"))
                        {
                            tempDs.JOB.RemoveJOBRow(row);
                            hasChanges = true;
                        }
                    }
                    if (hasChanges)
                    {
                        tempDs.AcceptChanges();
                        tempDs.WriteXml(fnJobSummHistXmlPath);
                    }
                }
                catch (Exception ex)
                {
                    LogMsg("deleteOldJobs:error:" + ex.Message);
                }

            }
        }

        //private void TestWriteJob()
        //{
        //    //DSJob ds = new DSJob();
        //    //DSJob.JOBRow row = ds.JOB.NewJOBRow();
        //    //row.FCCO="ABC";
        //    //row.FINM = "Julia";
        //    //row.LANM = "Cbe";
        //    //row.URNO = "UR123";
        //    //row.PENO = "PE1324";
        //    //row.UAFT = "UAFT1234";
        //    //row.UTPL = "UTPL1234";
        //    //ds.JOB.Rows.Add(row);
        //    //ds.JOB.AcceptChanges();
        //    //ds.JOB.WriteXml(fnJobXmlPath);
        //}

        //private void TestWritePfc()
        //{
        //    //DSJob ds = new DSJob();
        //    //DSJob.PFCRow row = ds.PFC.NewPFCRow();
        //    //row.URNO = "PFCURNO1234";
        //    //row.FCTC = "ABC";
        //    //row.FCTN = "ABC Function Name";

        //    //ds.PFC.Rows.Add(row);
        //    //ds.PFC.AcceptChanges();
        //    //ds.PFC.WriteXml(fnPfcXmlPath);
        //}

        //public void DeleteAJob(string jobUrno)
        //{
        //    if (jobUrno == null) return;
        //    if (jobUrno.Trim() == "") return;
        //    //System.Threading.Thread.Sleep(100);
        //    lock (_lockJobObj)
        //    {
        //        try
        //        {
        //            LockJobSummForWriting();
        //            //DSJob tempDs = (DSJob)dsJob.Copy();
        //            DSJob tempDs = new DSJob();
        //            tempDs.JOB.ReadXml(fnJobSummXmlPath);
        //            foreach (DSJob.JOBRow row in tempDs.JOB.Select("URNO='" + jobUrno + "'"))
        //            {
        //                string flightUrno = row.UAFT;
        //                if (row.EMPTYPE == "AO")
        //                {
        //                    DBFlight.GetInstance().DeleteAOFromFlight(flightUrno);
        //                }
        //                LogTraceMsg("row deleting " + jobUrno + "," + flightUrno);
        //                row.Delete();
        //                tempDs.JOB.AcceptChanges();
        //                tempDs.JOB.WriteXml(fnJobSummXmlPath);
        //            }

        //            //DBFlight.GetInstance().Init();
        //        }
        //        catch (Exception ex)
        //        {
        //            LogMsg("DeleteAJob:err:" + ex.Message);
        //        }
        //        finally
        //        {
        //            ReleaseJobSummAfterWrite();
        //        }
        //    }
        //}

        public void DeleteAJobForTurnAroundFlight(string jobUrno)
        {
            if (jobUrno == null) return;
            if (jobUrno.Trim() == "") return;
            //System.Threading.Thread.Sleep(100);
            lock (_lockJobObj)
            {
                try
                {
                    LockJobSummForWriting();
                    //DSJob tempDs = (DSJob)dsJob.Copy();
                    DSJob tempDs = new DSJob();
                    tempDs.JOB.ReadXml(fnJobSummXmlPath);
                    jobUrno = jobUrno.Trim();
                    string flightToDelete = "";
                    string seperator = "";
                    bool hasChanges = false;
                    foreach (DSJob.JOBRow row in tempDs.JOB.Select("URNO='" + jobUrno + "' OR URNO='A" + jobUrno + "'"))
                    {
                        string flightUrno = row.UAFT;
                        if (row.EMPTYPE == "AO")
                        {
                            LogTraceMsg("AO for flight:" + flightUrno);
                            flightToDelete += seperator + flightUrno;
                            seperator = ",";
                            //DBFlight.GetInstance().DeleteAOFromFlight(flightUrno);
                        }
                        else
                        {
                            LogTraceMsg("Not AO for flight:" + flightUrno);
                        }
                        hasChanges = true;
                        LogTraceMsg("deleting Job " + jobUrno + "," + flightUrno);
                        row.Delete();
                    }
                    if (hasChanges)
                    {
                        tempDs.JOB.AcceptChanges();
                        tempDs.JOB.WriteXml(fnJobSummXmlPath);
                    }
                    if (flightToDelete != "")
                    {
                        DBFlight.GetInstance().DeleteAOForMultipleFlight(flightToDelete);
                    }

                    //DBFlight.GetInstance().Init();
                }
                catch (Exception ex)
                {
                    LogTraceMsg("DeleteAJob:err:" + ex.Message);
                }
                finally
                {
                    ReleaseJobSummAfterWrite();
                }
            }
        }

        public string GetFlightUrnoForAJob(string jobUrno)
        {
            string flightUrno = "";
            bool found = false;
            if (!found)
            {
                DSJob tempDs = new DSJob();
                tempDs.JOB.ReadXml(fnJobSummXmlPath);
                foreach (DSJob.JOBRow row in tempDs.JOB.Select("URNO='" + jobUrno + "'"))
                {
                    found = true;
                    flightUrno = row.UAFT;
                    break;
                }
            }

            if (!found)
            {
                DSJob tempDs = (DSJob)dsJobHist.Copy();
                foreach (DSJob.JOBRow row in tempDs.JOB.Select("URNO='" + jobUrno + "'"))
                {
                    found = true;
                    flightUrno = row.UAFT;
                    break;
                }
            }
            return flightUrno;
        }


        private void LoadDsJob_JobHistTable()
        {
            //LoadJobDataToJobSummXmlFile();
            FileInfo fiXml = new FileInfo(fnJobSummHistXmlPath);
            DateTime curDT = fiXml.LastWriteTime;
            if (!fiXml.Exists) throw new ApplicationException("Job History information missing. Please inform to administrator.");

            if ((_dsJobHist == null) || (curDT.CompareTo(_lastJobSummHistWriteTime) != 0))
            {
                lock (_lockJobHistObj)
                {
                    try
                    {
                        curDT = fiXml.LastWriteTime;//check the time again, in case of lock
                        if ((_dsJobHist == null) || (curDT.CompareTo(_lastJobSummHistWriteTime) != 0))
                        {
                            try
                            {
                                DSJob tempDs = new DSJob();

                                tempDs.JOB.ReadXml(fnJobSummHistXmlPath);
                                if (_dsJobHist == null)
                                {
                                    _dsJobHist = tempDs;                                    
                                }
                                else
                                {
                                    if (tempDs.JOB.Rows.Count > 0)
                                    {
                                        tempDs.PFC.Merge(_dsJobHist.PFC);
                                        _dsJobHist = tempDs;
                                    }
                                }
                                LogTraceMsg("LoadDsJob_JobHistTable:Loaded.");
                                _lastJobSummHistWriteTime = curDT;
                            }
                            catch (Exception ex)
                            {
                                LogMsg("LoadDsJob_JobHistTable:Err:'" + ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogMsg("LoadDsJob_JobHistTable:err:" + ex.Message);
                    }
                }
            }
        }

        public void MoveJobsToHist(ArrayList alFlightUrno)
        {
            if (alFlightUrno == null) return;
            if (alFlightUrno.Count < 1) return;
            lock (_lockJobObj)
            {
                try
                {
                    LockJobSummForWriting();
                    //DSJob tempDs = (DSJob)dsJob.Copy();
                    DSJob tempDs = new DSJob();
                    tempDs.ReadXml(fnJobSummXmlPath);
                    DSJob tempDsHist = new DSJob();
                    tempDsHist.ReadXml(fnJobSummHistXmlPath);
                    bool hasChanges = false;
                    foreach (string urno in alFlightUrno)
                    {
                        foreach (DSJob.JOBRow row in tempDs.JOB.Select("UAFT='" + urno + "'"))
                        {
                            tempDsHist.JOB.LoadDataRow(row.ItemArray, true);
                            tempDs.JOB.RemoveJOBRow(row);
                            hasChanges = true;
                        }
                    }
                    if (hasChanges)
                    {
                        tempDs.AcceptChanges();
                        tempDsHist.AcceptChanges();
                        tempDs.WriteXml(fnJobSummXmlPath);
                        tempDsHist.WriteXml(fnJobSummHistXmlPath);
                    }
                }
                catch (Exception ex)
                {
                    LogMsg("deleteOldJobs:error:" + ex.Message);
                }
                finally
                {
                    ReleaseJobSummAfterWrite();
                }

            }
        }

        private void LockJobSummForWriting()
        {
            try
            {
                UtilLock.GetInstance().LockJobSumm();
            }
            catch (Exception ex)
            {
                LogMsg("LockJobSummForWriting:Err:" + ex.Message);
                if (ex.InnerException != null)
                {
                    LogMsg("LockJobSummForWriting:err:Inner:" + ex.InnerException.Message);
                }
            }
        }

        private void ReleaseJobSummAfterWrite()
        {
            try
            {
                UtilLock.GetInstance().ReleaseJobSumm();
            }
            catch (Exception ex)
            {
                LogMsg("ReleaseJobSummAfterWrite:Err:" + ex.Message);
                if (ex.InnerException != null)
                {
                    LogMsg("ReleaseJobSummAfterWrite:err:Inner:" + ex.InnerException.Message);
                }
            }
        }

        public DSJob GetAllJobs()
        {
            DSJob ds = null;
            ds = new DSJob();
            ds.Merge(dsJob);
            return ds;
        }

    }
}

