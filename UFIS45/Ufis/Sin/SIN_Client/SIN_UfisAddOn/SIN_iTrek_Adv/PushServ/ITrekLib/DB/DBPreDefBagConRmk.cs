#define TRACING_ON

using System;
using System.Collections.Generic;
using System.Text;

using System.Data;
using System.IO;
using System.Diagnostics;

using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.ENT;
using iTrekXML;
using UFIS.ITrekLib.Util;


namespace UFIS.ITrekLib.DB
{
    public class DBPreDefBagConRmk
    {
        private static Object _lockObj = new Object();
        private static readonly Object _lockObjInstance = new Object();
        //to use in synchronisation of single access
        private static DBPreDefBagConRmk _dbPreDefBagConRmk = null;
        private static DateTime _lastWriteDT = new System.DateTime(1, 1, 1);

        private const string ACTIVE_MSG_STAT = "A";
        private const string INACTIVE_MSG_STAT = "I";

        private static DSPreDefBagConRmk _dsPreDefBagConRmk = null;
        private static string _fnPreDefBagConRmkXml = null;//file name of PREDEFBAGCONRMK XML in full path.

        /// <summary>
        /// Singleton Pattern.
        /// Not allowed to create the object from other class.
        /// To get the object, user "GetInstance()" method.
        /// </summary>
        private DBPreDefBagConRmk() { }

        public void Init()
        {//Initialize all the information, to get the latest data when fetching them again.
            _dsPreDefBagConRmk = null;
            _fnPreDefBagConRmkXml = null;
            _lastWriteDT = new System.DateTime(1, 1, 1);
        }

        /// <summary>
        /// Get the Instance of DBPreDefBagConRmk. There will be only one reference for all the clients
        /// </summary>
        /// <returns></returns>
        public static DBPreDefBagConRmk GetInstance()
        {
            if (_dbPreDefBagConRmk == null)
            {
                lock (_lockObjInstance)
                {
                    try
                    {
                        if (_dbPreDefBagConRmk == null)
                            _dbPreDefBagConRmk = new DBPreDefBagConRmk();
                    }
                    catch (Exception ex)
                    {
                        LogMsg("GetInstance:Err:" + ex.Message);
                    }
                }
            }
            return _dbPreDefBagConRmk;
        }

        private string fnPreDefBagConRmkXml
        {
            get
            {
                if ((_fnPreDefBagConRmkXml == null) || (_fnPreDefBagConRmkXml == ""))
                {
                    iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
                    _fnPreDefBagConRmkXml = path.GetPredefinedBagConRmkFilePath();
                }

                return _fnPreDefBagConRmkXml;
            }
        }

        private DSPreDefBagConRmk dsPreDefBagConRmk
        {
            get
            {
                //if (_dsPreDefBagConRmk == null) { LoadDSPreDefBagConRmk(); }
                LoadDSPreDefBagConRmk();
                DSPreDefBagConRmk ds = (DSPreDefBagConRmk)_dsPreDefBagConRmk.Copy();

                return ds;
            }
        }

        private void LoadDSPreDefBagConRmk()
        {
            FileInfo fiXml = new FileInfo(fnPreDefBagConRmkXml);
            DateTime curDT = fiXml.LastWriteTime;

            if ((_dsPreDefBagConRmk == null) || (curDT.CompareTo(_lastWriteDT) != 0))
            {
                lock (_lockObj)
                {
                    try
                    {
                        curDT = fiXml.LastWriteTime;//check the time again, in case of lock
                        if ((_dsPreDefBagConRmk == null) || (curDT.CompareTo(_lastWriteDT) != 0))
                        {
                            try
                            {
                                DSPreDefBagConRmk tempDs = new DSPreDefBagConRmk();

                                tempDs.PREDEFBAGCONRMK.ReadXml(fnPreDefBagConRmkXml);
                                if (_dsPreDefBagConRmk == null)
                                {
                                    _dsPreDefBagConRmk = new DSPreDefBagConRmk();
                                }
                                else
                                {
                                    _dsPreDefBagConRmk.PREDEFBAGCONRMK.Clear();
                                }
                                _dsPreDefBagConRmk.PREDEFBAGCONRMK.Merge(tempDs.PREDEFBAGCONRMK);
                                _lastWriteDT = curDT;
                            }
                            catch (Exception ex)
                            {
                                LogMsg("LoadDSPreDefBagConRmk:Err:" + ex.Message);
                                //TestWritePREDEFBAGCONRMK();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogMsg("LoadDSPreDefBagConRmk:Err:" + ex.Message);
                    }
                }
            }
        }

        //[Conditional("TRACING_ON")]
        private void LogTraceMsg(string msg)
        {
            //UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DBPreDefBagConRmk", msg);
           try
           {
              MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("DBPreDefBagConRmk", msg);
           }
           catch (Exception)
           {
              UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DBPreDefBagConRmk", msg);
           }
        }

        private static void LogMsg(string msg)
        {
            //UtilLog.LogToGenericEventLog("DBPreDefBagConRmk ", msg);
           try
           {
              MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("DBPreDefBagConRmk", msg);
           }
           catch (Exception)
           {
              UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DBPreDefBagConRmk", msg);
           }
        }

        private void TestWritePREDEFBAGCONRMK()
        {
            //{
            //    DSPreDefBagConRmk ds = new DSPreDefBagConRmk();
            //    DSPreDefBagConRmk.PREDEFBAGCONRMKRow row = ds.PREDEFBAGCONRMK.NewPREDEFBAGCONRMKRow();
            //    row.URNO = "Urno123";
            //    row.MSGTEXT = "MsgText 12";
            //    row.STAT = "A";
            //    ds.PREDEFBAGCONRMK.Rows.Add(row);
            //    ds.PREDEFBAGCONRMK.AcceptChanges();
            //    ds.PREDEFBAGCONRMK.WriteXml(fnPreDefBagConRmkXml);
            //}
        }
        /////////
        public bool IsExistingMsg(string msg)
        {
            return (dsPreDefBagConRmk.PREDEFBAGCONRMK.Select("MSGTEXT='" + msg + "'").Length) > 0;
        }

        private bool IsActiveMsg(string msgId)
        {
            return GetMsgByMsgId(msgId).IsActive();          
        }

        private bool IsValidStatus(string stat)
        {
            bool validStat = false;
            if ((stat == ACTIVE_MSG_STAT) || (stat == INACTIVE_MSG_STAT)) validStat = true;
            return validStat;
        }

        private EntPreDefBagConRmk GetMsgByMsgId(string msgId)
        {
            EntPreDefBagConRmk entMsg = new EntPreDefBagConRmk();
            DSPreDefBagConRmk ds = dsPreDefBagConRmk;
            DSPreDefBagConRmk.PREDEFBAGCONRMKRow[] rows = ((DSPreDefBagConRmk.PREDEFBAGCONRMKRow[])dsPreDefBagConRmk.PREDEFBAGCONRMK.Select("URNO='" + msgId + "'"));

            if (rows.Length > 0)
            {
                DSPreDefBagConRmk.PREDEFBAGCONRMKRow row = rows[0];
                entMsg.rmkId = row.URNO;
                entMsg.rmk = row.MSGTEXT;
                entMsg.stat = row.STAT;
            }
            return entMsg;
        }

        private EntPreDefBagConRmk GetMsgByMsgText(string msg)
        {
            EntPreDefBagConRmk entMsg = new EntPreDefBagConRmk();
            DSPreDefBagConRmk ds = dsPreDefBagConRmk;

            DSPreDefBagConRmk.PREDEFBAGCONRMKRow[] rows = ((DSPreDefBagConRmk.PREDEFBAGCONRMKRow[])dsPreDefBagConRmk.PREDEFBAGCONRMK.Select("MSGTEXT='" + msg + "'"));
            if (rows.Length > 0)
            {
                DSPreDefBagConRmk.PREDEFBAGCONRMKRow row = rows[0];
                entMsg.rmkId = row.URNO;
                entMsg.rmk = row.MSGTEXT;
                entMsg.stat = row.STAT;
            }
            return entMsg;
        }

        public void UpdatePreDefBagConRmk(string msgId, string msg)
        {
            if (IsExistingMsg(msg))
            {
                string otherMsgId = GetUrNoForAMsg(msg);
                if ((otherMsgId!="") && (otherMsgId != msgId))
                {
                    EntPreDefBagConRmk entMsg = GetMsgByMsgId(otherMsgId);
                    if (entMsg.IsActive()) throw new ApplicationException("Duplicate Message.");
                    else
                    {
                        DeletePreDefBagConRmkById(msgId);
                        UpdatePreDefBagConRmk(otherMsgId, msg, ACTIVE_MSG_STAT);
                    }
                }
                else
                {
                    UpdatePreDefBagConRmk(msgId, msg, "");
                }
            }
            else
            {
                UpdatePreDefBagConRmk(msgId, msg, "");
            }
        }

        private void UpdatePreDefBagConRmk(string urNo, string msg, string stat)
        {
            DSPreDefBagConRmk ds = dsPreDefBagConRmk;

            DSPreDefBagConRmk.PREDEFBAGCONRMKRow row = ds.PREDEFBAGCONRMK.FindByURNO(urNo);
            row.MSGTEXT  = msg;
            if (stat != "")
            {
                if (IsValidStatus(stat))
                {
                    row.STAT = stat;
                }
                else throw new ApplicationException("Invalid Status [" + stat + "]");
            }

            ds.PREDEFBAGCONRMK.AcceptChanges();
            ds.PREDEFBAGCONRMK.WriteXml(fnPreDefBagConRmkXml, XmlWriteMode.IgnoreSchema, false);
            _lastWriteDT = _lastWriteDT.AddYears(-1);
        }

        private string GetUrNoForAMsg(string msg)
        {
            string urNo = "";
            try
            {
                urNo = ((DSPreDefBagConRmk.PREDEFBAGCONRMKRow[])dsPreDefBagConRmk.PREDEFBAGCONRMK.Select("MSGTEXT='" + msg + "'"))[0].URNO;
            }
            catch (Exception)
            {
            } 
            return urNo;
        }

        public void CreatePreDefBagConRmk(string msg)
        {
            if (IsExistingMsg(msg))
            {
                string urNo = GetUrNoForAMsg(msg);
                if (IsActiveMsg(urNo)) throw new ApplicationException("Duplicate Message.");
                else
                {
                    UpdatePreDefBagConRmk(urNo, msg, ACTIVE_MSG_STAT);
                }
            }
            else
            {
                string urNo = Convert.ToString(dsPreDefBagConRmk.PREDEFBAGCONRMK.Rows.Count + 1);
                DSPreDefBagConRmk ds = (DSPreDefBagConRmk)dsPreDefBagConRmk.Copy();
                DSPreDefBagConRmk.PREDEFBAGCONRMKRow row = (DSPreDefBagConRmk.PREDEFBAGCONRMKRow)ds.PREDEFBAGCONRMK.NewRow();
                row.MSGTEXT = msg;
                row.URNO = urNo;
                row.STAT = ACTIVE_MSG_STAT;
                //row.MSGPART = msg.Substring(0, 15);
                ds.PREDEFBAGCONRMK.AddPREDEFBAGCONRMKRow(row);
                ds.PREDEFBAGCONRMK.AcceptChanges();
                ds.PREDEFBAGCONRMK.WriteXml(fnPreDefBagConRmkXml, XmlWriteMode.IgnoreSchema, false);
                _lastWriteDT = _lastWriteDT.AddYears(-1);
            }
        }

        public void DeletePreDefBagConRmk(string msg)
        {
            try
            {
                DSPreDefBagConRmk ds = (DSPreDefBagConRmk)dsPreDefBagConRmk.Copy();

                DSPreDefBagConRmk.PREDEFBAGCONRMKRow row = ds.PREDEFBAGCONRMK.FindByURNO(GetUrNoForAMsg(msg));
                row.STAT = INACTIVE_MSG_STAT;
                ds.PREDEFBAGCONRMK.AcceptChanges();
                ds.PREDEFBAGCONRMK.WriteXml(fnPreDefBagConRmkXml, XmlWriteMode.IgnoreSchema, false);
                _lastWriteDT = _lastWriteDT.AddYears(-1);
            }
            catch (Exception ex)
            {
                LogMsg("DeletePreDefBagConRmk(string msg) : " + ex.Message);
                throw new ApplicationException("Unable to delete predefined message due to : " + ex.Message);
            }
        }

        public void DeletePreDefBagConRmkById(string urNo)
        {
            try
            {
                DSPreDefBagConRmk ds = (DSPreDefBagConRmk)dsPreDefBagConRmk.Copy();
                DSPreDefBagConRmk.PREDEFBAGCONRMKRow row = ds.PREDEFBAGCONRMK.FindByURNO(urNo);
                row.STAT = INACTIVE_MSG_STAT;
                ds.PREDEFBAGCONRMK.AcceptChanges();
                ds.PREDEFBAGCONRMK.WriteXml(fnPreDefBagConRmkXml, XmlWriteMode.IgnoreSchema, false);
                _lastWriteDT = _lastWriteDT.AddYears(-1);
            }
            catch (Exception ex)
            {
                LogMsg("DeletePreDefBagConRmkById : " + ex.Message);
                throw new ApplicationException("Unable to delete predefined message");
            }
        }

        public DSPreDefBagConRmk RetrieveActiveRmk()
        {
            DSPreDefBagConRmk ds = new DSPreDefBagConRmk();
            //ds = (DSPreDefBagConRmk)dsPreDefBagConRmk.Copy();
            //ds.PREDEFBAGCONRMK.Clear();

            DSPreDefBagConRmk tempDs = dsPreDefBagConRmk;
            foreach (DSPreDefBagConRmk.PREDEFBAGCONRMKRow row in tempDs.PREDEFBAGCONRMK.Select("STAT='" + ACTIVE_MSG_STAT + "'"))
            {
                ds.PREDEFBAGCONRMK.LoadDataRow(row.ItemArray, true);
            }

            return ds;
        }

        public DSPreDefBagConRmk RetrieveAllPreDefBagConRmk()
        {
            DSPreDefBagConRmk ds = (DSPreDefBagConRmk)dsPreDefBagConRmk.Copy();

            return ds;
        }
        ////////

    }
}
