using System;
using System.Collections.Generic;
using System.Text;

namespace UFIS.ITrekLib.ENT
{
    public class EntPreDefBagConRmk
    {
        private const string INACTIVE_STATUS = "I";
        private const string ACTIVE_STATUS = "A";

        private string _rmkId = null;
        private string _rmk = null;
        private string _stat = null;

        public EntPreDefBagConRmk()
        {
            _rmk = "";
            _stat = INACTIVE_STATUS;
        }

        public string rmkId
        {
            get
            {
                if (_rmkId == null) return ""; else return _rmkId;
            }
            set
            {
                if (_rmkId == null)
                {
                    if ((value == null) || (value == "")) throw new ApplicationException("Invalid Remark Id");
                    _rmkId = value;
                }
                else throw new ApplicationException("Remark Id can not be changed.");
            }
        }

        public string rmk
        {
            get
            {
                if (_rmk == null) return ""; else return _rmk;
            }
            set
            {
                if ((value == null) || (value == ""))
                {
                    _rmk = "";
                    _stat = INACTIVE_STATUS;
                }
            }
        }

        public string stat
        {
            get
            {
                return _stat;
            }
            set
            {
                if ((value == ACTIVE_STATUS) || (value == INACTIVE_STATUS))
                {
                    _stat = value;
                }
                else
                {
                    throw new ApplicationException("Invalid Status");
                }
            }
        }

        public void SetActive()
        {
            stat = ACTIVE_STATUS;
        }

        public void SetInActive()
        {
            stat = INACTIVE_STATUS;
        }

        public bool IsActive()
        {
            if (stat == ACTIVE_STATUS) return true; else return false;
        }
    }
}
