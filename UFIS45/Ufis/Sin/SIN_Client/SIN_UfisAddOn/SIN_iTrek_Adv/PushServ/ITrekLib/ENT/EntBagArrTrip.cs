using System;
using System.Collections.Generic;
using System.Text;

using System.Collections;

using UFIS.ITrekLib.Util;
using MM.UtilLib;

namespace UFIS.ITrekLib.ENT
{
   public class EntBagArrTerminal
   {
      string _terminalId = "";
      ArrayList _arrBeltIds = null; //Array of actual Belt id to select the flights.
      string _stBeltInfo = "";
      EnBagArrType _baType = EnBagArrType.None;
      string _stSelectedBeltId = ""; //Belt Id for drop down list to choose the belt.

      public EntBagArrTerminal()
      {
         _arrBeltIds = new ArrayList();
      }

      public bool IsSameData(EntBagArrTerminal objToCompare)
      {
         bool isEqual = false;
         if ((_terminalId == objToCompare._terminalId) && (_stBeltInfo == objToCompare._stBeltInfo) && (_baType == objToCompare._baType))
         {
            if ((_arrBeltIds != null) && (objToCompare._arrBeltIds != null))
            {
               int cntOrg = _arrBeltIds.Count;
               int cntNew = objToCompare._arrBeltIds.Count;
               if (cntNew == cntOrg)
               {
                  bool hasDiff = false;
                  for (int i = 0; i < cntOrg; i++)
                  {
                     if ((string)_arrBeltIds[i] != (string)objToCompare._arrBeltIds[i])
                     {
                        hasDiff = true;
                        break;
                     }
                  }
                  isEqual = !hasDiff;
               }
            }
         }
         return isEqual;
      }

      public EnBagArrType BagArrType
      {
         get { return _baType; }
         set { _baType = value; }
      }

      public string TerminalId
      {
         get { return _terminalId; }
         set
         {
            if (value == null) _terminalId = "";
            else _terminalId = value;
         }
      }

      public string BeltInfo
      {
         get { return _stBeltInfo; }
         set
         {
            if (value == null) _stBeltInfo = "";
            else _stBeltInfo = value;
         }
      }

      public ArrayList ArrBeltIds
      {
         get { return _arrBeltIds; }
         set
         {
            _arrBeltIds.Clear();
            if (value != null)
               _arrBeltIds.AddRange(value);
         }
      }

      public string DropDownListBeltId
      {
         get
         {
            return _stSelectedBeltId;
         }
         set
         {
            if (value == null) _stSelectedBeltId = "";
            else _stSelectedBeltId = value;
         }
      }
   }

   public class EntBagArrUnReceivedTrip : IComparable
   {
      int _tripNo;
      EntBagArrUldList _arrUld;
      DateTime _sentDateTime;

      public int TripNo
      {
         get { return _tripNo; }
      }

      public string TripNoToDisplayInWeb
      {
         get
         {
            //return "Trip No. [" + _tripNo + "]";
            string st = "-";
            if (_tripNo == 1) st = "1st Trip";
            else if (_tripNo == 2) st = "2nd Trip";
            else if (_tripNo == 3) st = "3rd Trip";
            else if (_tripNo > 3) st = _tripNo.ToString() + "th Trip";
            return st;
         }
      }

      public DateTime SentDateTime
      {
         get { return _sentDateTime; }
         set { _sentDateTime = value; }
      }

      public EntBagArrUnReceivedTrip(int tripNo, DateTime sentDateTime)
      {
         if (tripNo < 1) throw new ApplicationException("Invalid Trip No.");
         _tripNo = tripNo;
         _sentDateTime = sentDateTime;
         _arrUld = new EntBagArrUldList();
      }

      public bool IsMoreThanXMinutes(int minutesPassed)
      {
         if (_sentDateTime.AddMinutes(minutesPassed).CompareTo(DateTime.Now) < 0)
            return true;
         else return false;
      }


      public void AddUld(string uldId, string uldNo, string sendRemk, string recvRemk)
      {
         _arrUld.AddUld(uldId, uldNo, sendRemk, recvRemk);
      }

      public EntBagArrUld this[int idx]
      {
         get
         {
            return _arrUld[idx];
         }
      }

      public void Sort()
      {
         _arrUld.Sort();
      }

      public void Clear()
      {
         _arrUld.Clear();
      }

      public int Count
      {
         get { return _arrUld.Count; }
      }

      public int CompareTo(Object obj)
      {
         return _tripNo - ((EntBagArrUnReceivedTrip)obj)._tripNo;
      }

   }

   public class EntBagArrUnReceivedTripList
   {
      ArrayList _arrUnRecvTrip;
      public EntBagArrUnReceivedTripList()
      {
         _arrUnRecvTrip = new ArrayList();
      }

      public void AddUnRecvTrip(int sentTripNo, string uldId, string uldNo,
  string sendRmk, string sentDt, string recvRmk, string recvDt)
      {
         int cnt = _arrUnRecvTrip.Count;
         EntBagArrUnReceivedTrip ent = null;
         bool foundFlag = false;
         for (int i = 0; i < cnt; i++)
         {
            ent = (EntBagArrUnReceivedTrip)_arrUnRecvTrip[i];
            if (ent.TripNo == sentTripNo)
            {
               foundFlag = true;
               break;
            }
         }
         if (!foundFlag)
         {
            ent = new EntBagArrUnReceivedTrip(sentTripNo, UtilTime.ConvUfisTimeStringToDateTime(sentDt));
            _arrUnRecvTrip.Add(ent);
            _arrUnRecvTrip.Sort();
         }

         ent.AddUld(uldId, uldNo, sendRmk, recvRmk);
      }

      public EntBagArrUnReceivedTrip this[int idx]
      {
         get
         {
            return (EntBagArrUnReceivedTrip)_arrUnRecvTrip[idx];
         }
      }

      public void Sort()
      {
         _arrUnRecvTrip.Sort();
      }

      public void Clear()
      {
         _arrUnRecvTrip.Clear();
      }

      public int Count
      {
         get { return _arrUnRecvTrip.Count; }
      }

      public Hashtable GetUnRecvUlds()
      {
         int cnt = Count;
         Hashtable ht = new Hashtable();
         for (int i = 0; i < cnt; i++)
         {
            EntBagArrUnReceivedTrip ent = this[i];
            for (int j = 0; j < ent.Count; j++)
            {
               EntBagArrUld uld = ent[j];
               ht.Add(uld.Id, uld.UldNo);
            }
         }
         return ht;
      }
   }

   public class EntBagArrContainer
   {
      EntBagArrUnReceivedTripList _unRecvList;
      EntBagArrUldList _recvList;

      EntBagArrUldList _unSentUlds;

      public EntBagArrContainer()
      {
         _unRecvList = new EntBagArrUnReceivedTripList();
         _recvList = new EntBagArrUldList();
         _unSentUlds = new EntBagArrUldList();
      }

      public void AddTrip(string sentTripNo, string uldId, string uldNo,
          string sendRmk, string sentDt, string recvRmk, string recvDt)
      {
		  
         if (sentTripNo == null) return;
         sentTripNo = sentTripNo.Trim();
         if (sentTripNo == "") return;

         int iSentTripNo = int.Parse(sentTripNo);
         if (string.IsNullOrEmpty(recvDt))
         {//Not recveied this uld
            _unRecvList.AddUnRecvTrip(iSentTripNo, uldId, uldNo, sendRmk, sentDt, recvRmk, recvDt);
         }
         else
         {//Received this uld
            _recvList.AddUld(uldId, uldNo, sendRmk, recvRmk);
         }
      }

      public void AddUnSentUld(string cpmId, string uldNo)
      {
         _unSentUlds.AddUld(cpmId, uldNo);
      }

      public EntBagArrUnReceivedTripList UnReceivedTripList
      {
         get
         {
            return _unRecvList;
         }
      }

      public EntBagArrUldList ReceivedUldList
      {
         get
         {
            return _recvList;
         }
      }

      public EntBagArrUldList UnSentUldList
      {
         get
         {
            return _unSentUlds;
         }
      }

   }

   public class EntBagArrUldList
   {
      ArrayList _arrUld;

      public EntBagArrUldList()
      {
         _arrUld = new ArrayList();
      }

      public void AddUld(string id, string uldNo)
      {
         _arrUld.Add(new EntBagArrUld(id, uldNo));
         _arrUld.Sort();
      }

      public void AddUld(string uldId, string uldNo, string sendRemk, string recvRemk)
      {
         _arrUld.Add(new EntBagArrUld(uldId, uldNo, sendRemk, recvRemk));
         _arrUld.Sort();
      }

      public void Sort()
      {
         _arrUld.Sort();
      }

      public void Clear()
      {
         _arrUld.Clear();
      }

      public EntBagArrUld this[int idx]
      {
         get
         {
            if (idx < _arrUld.Count)
               return (EntBagArrUld)_arrUld[idx];
            else throw new ApplicationException("Array Index out of bound.");
         }
      }

      public int Count
      {
         get { return _arrUld.Count; }
      }

      public string UldListString
      {
         get
         {
            string st = "";
            int cnt = this.Count;
            bool firstUld = true;
            for (int i = 0; i < cnt; i++)
            {
               if (string.IsNullOrEmpty(this[i].UldNo)) continue;
               if (firstUld)
               {
                  firstUld = false;
               }
               else
               {
                  st += ", ";
               }
               st += this[i].UldNo;
            }
            return st;
         }
      }
   }

   public class EntBagArrUld : IComparable
   {
      string _id;
      string _uldNo;
      string _sendRmk = "";
      string _rcvRmk = "";

      public EntBagArrUld(string id, string uldNo)
      {
         _id = id;
         _uldNo = uldNo;
      }

      public EntBagArrUld(string id, string uldNo, string sendRemark)
         : this(id, uldNo)
      {
         _sendRmk = sendRemark;
      }

      public EntBagArrUld(string id, string uldNo, string sendRemark, string recvRemark)
         : this(id, uldNo, sendRemark)
      {
         _rcvRmk = recvRemark;
      }

      public string Id
      {
         get { return _id; }
      }

      public string UldNo
      {
         get { return _uldNo; }
      }

      public int CompareTo(object obj)
      {
         return _uldNo.CompareTo(((EntBagArrUld)obj)._uldNo);
      }

      public string SendRemark
      {
         get
         {
            return _sendRmk;
         }
         set
         {
            if (value == null) _sendRmk = "";
            else _sendRmk = value;
         }
      }

      public string RecvRemark
      {
         get
         {
            return _rcvRmk;
         }
         set
         {
            if (value == null) _rcvRmk = "";
            else _rcvRmk = value;
         }
      }
   }

   public class EntBagArrFlightSelectedUld
   {
      private List<string> _arrSelectedUldIds;
      private string _flightId;
      private EnBagArrType _baType = EnBagArrType.None;
      private EnBagArrFlightActionType _baAction = EnBagArrFlightActionType.None;
      private ArrayList _arrSelectedTrip;

      public EntBagArrFlightSelectedUld()
      {
         _arrSelectedUldIds = new List<string>();
         _arrSelectedTrip = new ArrayList();
         _flightId = "";
      }

      public string FlightId
      {
         get { return _flightId; }
         set { _flightId = value; }
      }

      public List<string> SelectedUldIds
      {
         get { return _arrSelectedUldIds; }
         set
         {
            _arrSelectedUldIds = new List<string>();
            _arrSelectedUldIds.AddRange(value);
         }
      }

      public ArrayList SelectedTrip
      {
         get { return _arrSelectedTrip; }
         set
         {
            _arrSelectedTrip = new ArrayList();
            _arrSelectedTrip.AddRange(value);
         }
      }

      public EnBagArrType BagArrType
      {
         get { return _baType; }
         set { _baType = value; }
      }

      public EnBagArrFlightActionType BagArrFlightAction
      {
         get { return _baAction; }
         set { _baAction = value; }
      }
   }

   /// <summary>
   /// List of "EntBagArrFlightSelectedUld" object
   /// </summary>
   public class EntBagArrFlightSelectedUldList
   {
      ArrayList _arr = null;

      public EntBagArrFlightSelectedUldList()
      {
         _arr = new ArrayList();
      }

      public void Add(EntBagArrFlightSelectedUld objBagArrFlightSelectedUld)
      {
         _arr.Add(objBagArrFlightSelectedUld);
      }

      public void Add(EntBagArrFlightSelectedUldList objBagArrFlightSelectedUldList)
      {
         int cnt = objBagArrFlightSelectedUldList.Count;
         for (int i = 0; i < cnt; i++)
         {
            _arr.Add(objBagArrFlightSelectedUldList[i]);
         }
      }

      public void Sort()
      {
         _arr.Sort();
      }

      public void Clear()
      {
         _arr.Clear();
      }

      public EntBagArrFlightSelectedUld this[int idx]
      {
         get
         {
            if (idx < _arr.Count)
               return (EntBagArrFlightSelectedUld)_arr[idx];
            else throw new ApplicationException("Array Index out of bound.");
         }
      }

      public int Count
      {
         get { return _arr.Count; }
      }

      public List<string> GetSelectedUlds(string flightId, EnBagArrType bagArrType, out ArrayList arrSelectedTrips)
      {
         int cnt = Count;
         List<string> arrUldIds = new List<string>();
         arrSelectedTrips = new ArrayList();
         EntBagArrFlightSelectedUld obj = null;
         for (int i = 0; i < cnt; i++)
         {
            obj = (EntBagArrFlightSelectedUld)_arr[i];
            if (obj != null)
            {
               if ((flightId == obj.FlightId) && (bagArrType == obj.BagArrType))
               {
                  arrUldIds.AddRange(obj.SelectedUldIds);
                  arrSelectedTrips.AddRange(obj.SelectedTrip);
                  break;
               }
            }
         }
         return arrUldIds;
      }

   }
}