using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using iTrekXML;
using UFIS.ITrekLib.DS;
using System.Threading;
using UFIS.ITrekLib.Util;
using MM.UtilLib;

namespace UFIS.ITrekLib.DB
{
    public class DBFuncMap
    {
        private static Object _lockObj = new Object();//to use in synchronisation of single access
        private static DBFuncMap _dbFuncMap = null;
        private static DateTime lastWriteDT = new System.DateTime(1, 1, 1);
        private static DateTime _lastRefreshDT = new DateTime();

        private static DSFuncMap _dsFuncMap = null;
        private static string _fnFuncMapXml = null;//file name of FuncMap XML in full path.

        /// <summary>
        /// Singleton Pattern.
        /// Not allowed to create the object from other class.
        /// To get the object, user "GetInstance()" method.
        /// </summary>
        private DBFuncMap() { }

        public void Init()
        {//Initialize all the information, to get the latest data when fetching them again.
            _dsFuncMap = null;
            _fnFuncMapXml = null;
            lastWriteDT = new System.DateTime(1, 1, 1);
            _lastRefreshDT = new DateTime();
        }

        /// <summary>
        /// Get the Instance of DBFuncMap. There will be only one reference for all the clients
        /// </summary>
        /// <returns></returns>
        public static DBFuncMap GetInstance()
        {
            if (_dbFuncMap == null)
            {
                _dbFuncMap = new DBFuncMap();
            }
            return _dbFuncMap;
        }

        public String RetrieveFuncName(string pageName, string controlName)
        {
            DSFuncMap ds = dsFuncMap;
            int cnt = 0;
            string funcName = "";
            try
            {
                foreach (DSFuncMap.FUNCMAPRow row in ds.FUNCMAP.Select("PGNAME='" + pageName + "' AND CTRLNAME='" + controlName + "'"))
                {
                    funcName = row.FUNCNAME;
                    cnt++;
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Error in Retrieving function name due to ==> " + ex.Message);
                //throw;
            }
            if (cnt==0) throw new ApplicationException("No function information for the page (" + pageName + ", " + controlName + ")");
            else if (cnt > 1) throw new ApplicationException("More than one function for the page (" + pageName + "," + controlName + ")");
            else
            {
                if (funcName == "")
                {
                    throw new ApplicationException("No function name for the page (" + pageName + ", " + controlName + ")");
                }
            }

            return funcName;
        }

        private string fnFuncMapXml
        {
            get
            {
                if ((_fnFuncMapXml == null) || (_fnFuncMapXml == ""))
                {
                    iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
                    _fnFuncMapXml = path.GetFuncMapXmlFilePath();
                }

                return _fnFuncMapXml;
            }
        }

        private DSFuncMap dsFuncMap
        {
            get
            {
                if (Math.Abs(UtilTime.DiffInSec(DateTime.Now, _lastRefreshDT)) > 60)
                {
                    LoadDSFuncMap();
                    _lastRefreshDT = DateTime.Now;
                }
                DSFuncMap ds = (DSFuncMap)_dsFuncMap.Copy();

                return ds;
            }
        }

        private void LoadDSFuncMap()
        {
            FileInfo fiXml = new FileInfo(fnFuncMapXml);
            DateTime curDT = fiXml.LastWriteTime;

            if ((_dsFuncMap == null) || (curDT.CompareTo(lastWriteDT) != 0))
            {
                lock (_lockObj)
                {
                    try
                    {
                        curDT = fiXml.LastWriteTime;//check the time again, in case of lock
                        if ((_dsFuncMap == null) || (curDT.CompareTo(lastWriteDT) != 0))
                        {
                            try
                            {
                                DSFuncMap tempDs = new DSFuncMap();
                                //Read data to temporary dataset. 
                                //After successfully read from XML, populate data to real dataset.
                                tempDs.FUNCMAP.ReadXml(fnFuncMapXml);
                                if (_dsFuncMap == null)
                                {
                                    _dsFuncMap = new DSFuncMap();
                                }
                                else
                                {
                                    _dsFuncMap.FUNCMAP.Clear();
                                }
                                _dsFuncMap.FUNCMAP.Merge(tempDs.FUNCMAP);
                                lastWriteDT = curDT;//Data is successfully add in. Update the date time.
                            }
                            catch (Exception ex)
                            {
                                LogMsg("LoadDSFuncMap:Err:" + ex.Message);
                                //TestWriteFuncMap();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogMsg("LoadDSFuncMap:Err:" + ex.Message);
                    }
                }
            }
        }

        private void LogMsg(string msg)
        {
            //UtilLog.LogToGenericEventLog("DBFuncMap", msg);
           try
           {
              MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("DBFuncMap", msg);
           }
           catch (Exception)
           {
              UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DBFuncMap", msg);
           }
        }

        
        private void TestWriteFuncMap()
        {
            //{
            //    DSFuncMap ds = new DSFuncMap();
            //    DSFuncMap.FUNCMAPRow row = ds.FUNCMAP.NewFUNCMAPRow();
            //    row.PGNAME = "FlightInfo";
            //    row.CTRLNAME = "ViewAssignedFlights";
            //    row.FUNCNAME = "FLINFO_AP_AR_ASSIGN";
            //    ds.FUNCMAP.Rows.Add(row);
            //    ds.FUNCMAP.AcceptChanges();
            //    ds.FUNCMAP.WriteXml(fnFuncMapXml);
            //}
        }
    }
}
