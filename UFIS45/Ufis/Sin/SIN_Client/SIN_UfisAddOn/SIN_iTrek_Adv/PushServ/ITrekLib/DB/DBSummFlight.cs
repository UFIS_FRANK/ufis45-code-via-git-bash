using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data;

using System.Threading;

using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.Util;
using iTrekXML;
using MM.UtilLib;

namespace UFIS.ITrekLib.DB
{
    public class DBSummFlight
    {
        private static DSFlight _dsFlight = null;
        private static readonly DateTime DEFAULT_DT = new System.DateTime(1, 1, 1);
        private static DateTime _lastFlightSummXmlDT = DEFAULT_DT;
        private static DateTime _lastOrgFlightXMLDT = DEFAULT_DT;
        private static DateTime _lastArrFlightXMLDT = DEFAULT_DT;
        private static DateTime _lastDepFlightXMLDT = DEFAULT_DT;
        private static DateTime _lastJobXMLDT = DEFAULT_DT;

        private static string _fnFlightXml = null;
        private static string _fnOrgFlightXml = null;
        private static string _fnArrFlightTimingXml = null;
        private static string _fnDepFlightTimingXml = null;
        private static string _fnJobXml = null;

        private const string ADID_DEPARTURE_IND = "D";
        private const string ADID_ARRIVAL_IND = "A";


        private static Object _lockObj = new Object();//to use in synchronisation of single access
        private static Object _lockSummFlightChg = new Object();//To use to synchronise before changing the Summ Flight Dataset
        private static DBSummFlight _dbFlight = null;

        private iTXMLPathscl _iTPath = null;
        private iTXMLPathscl ITPath
        {
            get
            {
                if (_iTPath == null) { _iTPath = new iTXMLPathscl(); }
                return _iTPath;
            }
        }

        private DBSummFlight() { }

        public void Init()
        {//Initialize all the information, to get the latest data when fetching them again.
            _dsFlight = null;

            _fnFlightXml = null;
            _fnOrgFlightXml = null;
            _fnArrFlightTimingXml = null;
            _fnDepFlightTimingXml = null;

            _lastFlightSummXmlDT = DEFAULT_DT;
            _lastOrgFlightXMLDT = DEFAULT_DT;
            _lastArrFlightXMLDT = DEFAULT_DT;
            _lastDepFlightXMLDT = DEFAULT_DT;
            _lastJobXMLDT = DEFAULT_DT;

        }

        /// <summary>
        /// Get the Instance of DBSummFlight. There will be only one reference for all the clients
        /// </summary>
        /// <returns></returns>
        public static DBSummFlight GetInstance()
        {
            if (_dbFlight == null)
            {
                _dbFlight = new DBSummFlight();
            }
            return _dbFlight;
        }

        /// <summary>
        /// Summary Flight Xml
        /// </summary>
        private string fnFlightXml
        {
            get
            {
                if ((_fnFlightXml == null) || (_fnFlightXml == ""))
                {
                    iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
                    _fnFlightXml = path.GetSummFlightsFilePath();
                }

                return _fnFlightXml;
            }
        }

        /// <summary>
        /// Updateable Flights Xml
        /// </summary>
        private string fnOrgFlightXml
        {
            get
            {
                if ((_fnOrgFlightXml == null) || (_fnOrgFlightXml == ""))
                {
                    iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
                    // _fnOrgFlightXml = path.GetFlightsFilePath();
                    _fnOrgFlightXml = path.GetUpdateableFlightsFilePath();

                }

                return _fnOrgFlightXml;
            }
        }

        /// <summary>
        /// Arrival Flight Timing Xml
        /// </summary>
        private string fnArrFlightTimingXml
        {
            get
            {
                if ((_fnArrFlightTimingXml == null) || (_fnArrFlightTimingXml == ""))
                {
                    iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
                    _fnArrFlightTimingXml = path.GetArrFlightsTimingFilePath();
                }

                return _fnArrFlightTimingXml;
            }
        }

        /// <summary>
        /// Departure Flight Timing Xml
        /// </summary>
        private string fnDepFlightTimingXml
        {
            get
            {
                if ((_fnDepFlightTimingXml == null) || (_fnDepFlightTimingXml == ""))
                {
                    iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
                    _fnDepFlightTimingXml = path.GetDepFlightsTimingFilePath();
                }

                return _fnDepFlightTimingXml;
            }
        }

        /// <summary>
        /// Updateable Job Xml
        /// </summary>
        private string fnJobXml
        {
            get
            {
                if ((_fnJobXml == null) || (_fnJobXml == ""))
                {
                    iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
                    //_fnJobXml = path.GetJobsFilePath();
                    _fnJobXml = path.GetUpdateableJobsFilePath();
                }

                return _fnJobXml;
            }
        }

        private bool _hasChanges = false;

        public void SummFlight()
        {
            if (HasChanges())
            {
                LogTraceMsg("SummFlight, Before Lock");
                lock (_lockObj)
                {
                    try
                    {
                        _hasChanges = false;
                        LoadFlightXmlInfo();

                        #region Summarise the data
                        //LoadOrgFlightXMLInfo();
                        //LoadArrFlightTimingXMLInfo();
                        //LoadDepFlightTimingXMLInfo();
                        //LoadJobXMLInfo();

                        Thread tOrgF = new Thread(this.LoadOrgFlightXMLInfo);
                        tOrgF.IsBackground = true;
                        tOrgF.Start();
                        Thread tArrF = new Thread(this.LoadArrFlightTimingXMLInfo);
                        tArrF.IsBackground = true;
                        tArrF.Start();
                        Thread tDepF = new Thread(this.LoadDepFlightTimingXMLInfo);
                        tDepF.IsBackground = true;
                        tDepF.Start();
                        Thread tJob = new Thread(this.LoadJobXMLInfo);
                        tJob.IsBackground = true;
                        tJob.Start();

                        tOrgF.Join();
                        tArrF.Join();
                        tDepF.Join();
                        tJob.Join();
                        #endregion
                        LogTraceMsg("Summarized");
                        UpdateFlightXmlInfo();
                        LogTraceMsg("SummFlight access after");
                    }
                    catch (Exception ex)
                    {
                        LogTraceMsg("SummFlight:Err:" + ex.Message);
                    }
                }
                //LogTraceMsg("SummFlight, After Lock");
            }
        }

        private void LoadFlightXmlInfo()
        {
            if (_dsFlight == null) _dsFlight = new DSFlight();
            //_dsFlight.FLIGHT.Clear();
            if (UtilFileIO.IsFileExist(fnFlightXml))
            {
                try
                {
                    DateTime curDT = GetLastWriteTime(fnFlightXml);
                    if (curDT.CompareTo(_lastFlightSummXmlDT) != 0)
                    {
                        DSFlight dsTemp = new DSFlight();
                        dsTemp.ReadXml(fnFlightXml);
                        _dsFlight = dsTemp;
                        _lastFlightSummXmlDT = curDT;
                    }
                }
                catch (Exception ex)
                {
                    LogMsg("LoadFlightXmlInfo : error : " + ex.Message);
                    throw new ApplicationException("Fail to get Flight Information." + ex.Message);
                }
            }
        }



        private void LoadOrgFlightXMLInfo()
        {
            try
            {
                string flightUrNo = "";
                string arrOrDep = "";

                DateTime curDT = GetLastWriteTime(fnOrgFlightXml);

                if (curDT.CompareTo(_lastOrgFlightXMLDT) != 0)
                {
                    DataSet dsOrgFlightXml = new DataSet();
                    dsOrgFlightXml.ReadXml(fnOrgFlightXml);
                    //Flight XML
                    if (dsOrgFlightXml.Tables.Contains("FLIGHT"))
                    {
                        int cnt = dsOrgFlightXml.Tables["FLIGHT"].Rows.Count;

                        lock (_lockSummFlightChg)
                        {
                            LogTraceMsg("Org Flight Info Processing.");
                            #region Process Flight Information
                            for (int i = 0; i < cnt; i++)
                            {
                                flightUrNo = "";
                                try
                                {
                                    DataRow row = dsOrgFlightXml.Tables["FLIGHT"].Rows[i];
                                    flightUrNo = (string)row["URNO"];
                                    DSFlight.FLIGHTRow[] fRows = (DSFlight.FLIGHTRow[])_dsFlight.FLIGHT.Select("URNO='" + flightUrNo + "'");
                                    DSFlight.FLIGHTRow fRow;
                                    if (fRows.Length < 1)
                                    {//Add new row
                                        fRow = _dsFlight.FLIGHT.NewFLIGHTRow();
                                        fRow.URNO = flightUrNo;
                                    }
                                    else if (fRows.Length == 1)
                                    {
                                        fRow = fRows[0];
                                    }
                                    else
                                    {
                                        throw new ApplicationException("More than one records with flight id (" + flightUrNo + ")");
                                    }

                                    fRow.FLNO = (string)row["FLNO"];
                                    fRow.A_D = (string)row["ADID"];
                                    arrOrDep = fRow.A_D;
                                    fRow.TO = (string)row["DES3"];
                                    fRow.FR = (string)row["ORG3"];
                                    fRow.BELT = (string)row["BLT1"];
                                    //fRow.AIRCRAFT_TYPE = (string)row["TTYP"];
                                    fRow.AIRCRAFT_TYPE = (string)row["ACT3"];
                                    /* For Transit Flight*/
                                    try
                                    {
                                        fRow.TURN = (string)row["TURN"];
                                    }
                                    catch (Exception)
                                    {
                                    }

                                    if (arrOrDep == ADID_ARRIVAL_IND)
                                    {
                                        fRow.ST = (string)row["STOA"];
                                        fRow.ET = (string)row["ETAI"];
                                        fRow.GATE = (string)row["GTA1"];
                                        fRow.PARK_STAND = (string)row["PSTA"];
                                        fRow.TERMINAL = (string)row["TGA1"];
                                    }
                                    else if (arrOrDep == ADID_DEPARTURE_IND)
                                    {
                                        fRow.ST = (string)row["STOD"];
                                        fRow.ET = (string)row["ETDI"];
                                        fRow.GATE = (string)row["GTD1"];
                                        fRow.PARK_STAND = (string)row["PSTD"];
                                        fRow.TERMINAL = (string)row["TGD1"];
                                    }
                                    else
                                    {
                                        throw new ApplicationException("Invalid Arrival or Departure Indicator");
                                    }
                                    try
                                    {
                                        fRow.REGN = (string)row["REGN"];
                                    }
                                    catch (Exception)
                                    {
                                    }

                                    if (fRows.Length < 1)
                                    {//Add new Row
                                        _dsFlight.FLIGHT.AddFLIGHTRow(fRow);
                                        _dsFlight.AcceptChanges();
                                    }
                                }
                                catch (Exception)
                                {
                                    // LogMsg("Flight " + flightUrNo + " rec error : " + ex.Message);
                                }
                            }
                            #endregion
                            LogTraceMsg("Org Flight Info Process Completed.");
                        }
                        _hasChanges = true;
                    }
                    _lastOrgFlightXMLDT = curDT;
                }
            }
            catch (Exception ex)
            {
                LogMsg("Flight error : " + ex.Message);
            }
        }

        private DateTime GetLastWriteTime(string fileName)
        {
            FileInfo fiXml = new FileInfo(fileName);
            DateTime curDT = fiXml.LastWriteTime;
            return curDT;
        }

        private const string BLANK_TIME = "-";//Should match with DBFlight BLANK_TIME

        ///// <summary>
        ///// To get the time. If original time is "BLANK_TIME", it will return ""
        ///// </summary>
        ///// <param name="orgTime"></param>
        ///// <returns></returns>
        //private string GetTime(string orgTime)
        //{
        //    string time = "";
        //    if (!(string.IsNullOrEmpty(orgTime)))
        //    {
        //        if (orgTime.Trim() != BLANK_TIME) time = orgTime;
        //    }
        //    return time;
        //}

        
        /// <summary>
        /// Assign the new Time 'newTime' to fRow[colName]. If newTime is blank, no need to assign. If newTime is 'BLANK_TIME', assign "". Otherwise assign with newTime.
        /// </summary>
        /// <param name="fRow">Flight Row</param>
        /// <param name="colName">column name of fRow to be assigned</param>
        /// <param name="newTime">new time to assign. if blank or null, no need to assign. if 'BLANK-TIME', assign ""</param>
        private void AssignTime(DSFlight.FLIGHTRow fRow, string colName, string newTime)
        {
            try
            {
                if (!(string.IsNullOrEmpty(newTime)))
                {
                    if (newTime.Trim() == BLANK_TIME) fRow[colName] = "";
                    else fRow[colName] = newTime;
                }
            }
            catch (Exception)
            {
            }
        }

        private void LoadArrFlightTimingXMLInfo()
        {
            string flightUrNo = "";
            string arrOrDep = "";
            try
            {
                DateTime curDT = GetLastWriteTime(fnArrFlightTimingXml);

                if (curDT.CompareTo(_lastArrFlightXMLDT) != 0)
                {
                    DSArrivalFlightTimings dsAFlightXml = new DSArrivalFlightTimings();
                    dsAFlightXml.ReadXml(fnArrFlightTimingXml);

                    //Arrival Flight XML
                    int cnt = dsAFlightXml.AFLIGHT.Rows.Count;
                    lock (_lockSummFlightChg)
                    {
                        LogTraceMsg("Arr Flight Info Processing.");
                        #region Process Arrival Flight Information
                        for (int i = 0; i < cnt; i++)
                        {
                            flightUrNo = "";
                            try
                            {
                                DSArrivalFlightTimings.AFLIGHTRow row = (DSArrivalFlightTimings.AFLIGHTRow)dsAFlightXml.AFLIGHT.Rows[i];
                                flightUrNo = row.URNO.ToString();
                                DSFlight.FLIGHTRow[] fRows = (DSFlight.FLIGHTRow[])_dsFlight.FLIGHT.Select("URNO='" + flightUrNo + "'");
                                DSFlight.FLIGHTRow fRow;
                                if (fRows.Length < 1) { throw new ApplicationException("No flight information for flight Urno " + flightUrNo); }
                                else if (fRows.Length > 1)
                                {
                                    throw new ApplicationException("More than one records with flight id (" + flightUrNo + ")");
                                }
                                fRow = fRows[0];

                                arrOrDep = fRow.A_D;
                                if (arrOrDep != ADID_ARRIVAL_IND) throw new ApplicationException("Not Arrival Flight " + flightUrNo);
                                //fRow.AT = row.ATA.ToString();
                                //fRow.MAB = row.MAB.ToString();
                                //fRow.PLB = row.PLB.ToString();
                                //fRow.TBS_UP = row._TBS_UP.ToString();
                                //fRow.AP_FST_TRP = row.FTRIP.ToString();
                                //fRow.BG_FST_TRP = row._FTRIP_B.ToString();
                                //fRow.AP_LST_TRP = row.LTRIP.ToString();
                                //fRow.BG_LST_TRP = row._LTRIP_B.ToString();
                                //fRow.FST_BAG = row.FBAG.ToString();
                                //fRow.LST_BAG = row.LBAG.ToString();
                                //fRow.UNL_STRT = row._UNLOAD_START.ToString();
                                //fRow.UNL_END = row._UNLOAD_END.ToString();

                                AssignTime(fRow, "AT", row.ATA.ToString());
                                AssignTime(fRow, "MAB", row.MAB.ToString());
                                AssignTime(fRow, "PLB", row.PLB.ToString());
                                AssignTime(fRow, "TBS_UP", row._TBS_UP.ToString());
                                AssignTime(fRow, "AP_FST_TRP", row.FTRIP.ToString());
                                AssignTime(fRow, "BG_FST_TRP", row._FTRIP_B.ToString());
                                AssignTime(fRow, "AP_LST_TRP", row.LTRIP.ToString());
                                AssignTime(fRow, "BG_LST_TRP", row._LTRIP_B.ToString());
                                AssignTime(fRow, "FST_BAG", row.FBAG.ToString());
                                AssignTime(fRow, "LST_BAG", row.LBAG.ToString());
                                AssignTime(fRow, "UNL_STRT", row._UNLOAD_START.ToString());
                                AssignTime(fRow, "UNL_END", row._UNLOAD_END.ToString());

                                ComputeBMTiming(fRow, flightUrNo);
                            }
                            catch (Exception)
                            {
                                // LogMsg("Arrival Flight " + flightUrNo + " rec error : " + ex.Message);
                            }
                            _hasChanges = true;
                        }
                        #endregion
                        LogTraceMsg("Arr Flight Info Process Completed.");
                    }
                    _lastArrFlightXMLDT = curDT;
                }
            }
            catch (Exception)
            {
                //LogMsg("Arrival Flight Error " + ex.Message);
            }
        }

        private void LogMsg(string msg)
        {
            //UtilLog.LogToGenericEventLog("DbSummFlight", msg);
           try
           {
              MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("DbSummFlight", msg);
           }
           catch (Exception)
           {
              UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DbSummFlight", msg);
           }
        }

        private void LogTraceMsg(string msg)
        {
            //UtilLog.LogToTraceFile("DbSummFlight", msg);
           try
           {
              MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("DbSummFlight", msg);
           }
           catch (Exception)
           {
              UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DbSummFlight", msg);
           }
        }

        private void ComputeBMTiming(DSFlight.FLIGHTRow fRow, string flightUrNo)
        {
            fRow.AP_FST_BM = Convert.ToInt32(CtrlConfig.GetBmApronFirstBag());
            fRow.BG_FST_BM = Convert.ToInt32(CtrlConfig.GetBmBaggageFirstBag());

            //Light Loader ==> 6 containers and below
            //Heavy Loader ==> 7 containers and above
            int cntContainer = CtrlTrip.GetContainerCountForAFlight(flightUrNo);
            if (cntContainer <= 6)
            { //Light Loader 
                //fRow.AP_LST_BM = Convert.ToInt32(ITPath.GetBMApronLastBagLightLoad());
                fRow.AP_LST_BM = Convert.ToInt32(CtrlConfig.GetBMApronLastBagLightLoad());
                //fRow.BG_LST_BM = Convert.ToInt32(ITPath.GetBMBaggageLastBagLightLoad());
                fRow.BG_LST_BM = Convert.ToInt32(CtrlConfig.GetBMBaggageLastBagLightLoad());
                fRow.L_H = "L";
            }
            else
            { //Heavy Loader
                //fRow.AP_LST_BM = Convert.ToInt32(ITPath.GetBMApronLastBagHeavyLoad());
                fRow.AP_LST_BM = Convert.ToInt32(CtrlConfig.GetBMApronLastBagHeavyLoad());
                //fRow.BG_LST_BM = Convert.ToInt32(ITPath.GetBMBaggageLastBagHeavyLoad());
                fRow.BG_LST_BM = Convert.ToInt32(CtrlConfig.GetBMBaggageLastBagHeavyLoad());
                fRow.L_H = "H";
            }

            ////fRow.AP_FST_BM_MET = MetTiming(fRow.AP_FST_BM, fRow.TBS_UP, fRow.FST_BAG);
            ////fRow.AP_LST_BM_MET = MetTiming(fRow.AP_LST_BM, fRow.TBS_UP, fRow.LST_BAG);
            //fRow.AP_FST_BM_MET = MetTiming(fRow.AP_FST_BM, fRow.AT, fRow.FST_BAG);
            //fRow.AP_LST_BM_MET = MetTiming(fRow.AP_LST_BM, fRow.AT, fRow.LST_BAG);
            //fRow.BG_FST_BM_MET = MetTiming(fRow.BG_FST_BM, fRow.AT, fRow.FST_BAG);
            //fRow.BG_LST_BM_MET = MetTiming(fRow.BG_LST_BM, fRow.AT, fRow.LST_BAG);

            //try
            //{
            //    if (fRow.AT.Trim() != "")
            //    {
            //        if (fRow.FST_BAG.Trim() != "")
            //        {
            //            fRow.BG_FST_BAG_TIMING = Util.UtilTime.TimeDiffInMinute(fRow.FST_BAG, fRow.AT).ToString();
            //        }
            //        else
            //        {
            //            fRow.BG_FST_BAG_TIMING = "";
            //        }
            //        if (fRow.LST_BAG.Trim() != "")
            //        {
            //            fRow.BG_LST_BAG_TIMING = Util.UtilTime.TimeDiffInMinute(fRow.LST_BAG, fRow.AT).ToString();
            //        }
            //        else
            //        {
            //            fRow.BG_LST_BAG_TIMING = "";
            //        }
            //    }
            //}
            //catch (Exception)
            //{
            //}

            //NOTE: If any changes of PASS/FAIL computation formula, need to change the "BMPassFail.aspx" accordingly

            string bgRefTime = fRow.TBS_UP;
            string bgFstBagTime = fRow.FST_BAG;
            string bgLstBagTime = fRow.LST_BAG;

            string apRefTime = fRow.TBS_UP;
            string apFstBagTime = fRow.FST_BAG;
            string apLstBagTime = fRow.LST_BAG;

            string timing = "";

            fRow.AP_FST_BM_MET = MetTiming(fRow.AP_FST_BM, apRefTime, apFstBagTime, out timing);
            fRow.AP_LST_BM_MET = MetTiming(fRow.AP_LST_BM, apRefTime, apLstBagTime, out timing);
            fRow.BG_FST_BM_MET = MetTiming(fRow.BG_FST_BM, bgRefTime, bgFstBagTime, out timing);
            fRow.BG_FST_BAG_TIMING = timing;
            fRow.BG_LST_BM_MET = MetTiming(fRow.BG_LST_BM, bgRefTime, bgLstBagTime, out timing);
            fRow.BG_LST_BAG_TIMING = timing;
        }

        private string MetTiming(string bmTiming, string refUfisTime, string ufisTimeToMeasure, out string timeDiff)
        {
            string timingMet = "";
            string timing = GetTiming(refUfisTime, ufisTimeToMeasure);
            if (timing != "" && (bmTiming.Trim() != ""))
            {
                if (Convert.ToInt32(bmTiming) >= Convert.ToInt32(timing)) timingMet = "P";//Meet the timing
                else timingMet = "F";
            }
            timeDiff = timing;
            return timingMet;
        }

        private string MetTiming(int bmTiming, string refUfisTime, string ufisTimeToMeasure, out string timeDiff)
        {
            string timingMet = "";
            string timing = GetTiming(refUfisTime, ufisTimeToMeasure);
            if (timing != "")
            {
                if (bmTiming >= Convert.ToInt32(timing)) timingMet = "P";//Meet the timing
                else timingMet = "F";
            }
            timeDiff = timing;
            return timingMet;
        }

        private string GetTiming(string frTime, string toTime)
        {
            string timing = "";
            if ((toTime != null) &&
                    (toTime != "") &&
                    (frTime != null) &&
                    (frTime != ""))
            {
                timing = UtilTime.TimeDiffInMinute(toTime, frTime).ToString();
            }
            return timing;
        }

        private void LoadDepFlightTimingXMLInfo()
        {
            string flightUrNo = "";
            string arrOrDep = "";

            try
            {
                DateTime curDT = GetLastWriteTime(fnDepFlightTimingXml);

                if (curDT.CompareTo(_lastDepFlightXMLDT) != 0)
                {
                    DSDepartureFlightTimings dsDFlightXml = new DSDepartureFlightTimings();
                    dsDFlightXml.ReadXml(fnDepFlightTimingXml);

                    //Departure Flight XML
                    int cnt = dsDFlightXml.Tables["DFLIGHT"].Rows.Count;
                    lock (_lockSummFlightChg)
                    {
                        LogTraceMsg("Dep Flight Info Processing.");
                        #region Process Departure Flight Information
                        for (int i = 0; i < cnt; i++)
                        {
                            flightUrNo = "";
                            try
                            {
                                DSDepartureFlightTimings.DFLIGHTRow row = (DSDepartureFlightTimings.DFLIGHTRow)dsDFlightXml.DFLIGHT.Rows[i];
                                flightUrNo = (string)row["URNO"];
                                DSFlight.FLIGHTRow[] fRows = (DSFlight.FLIGHTRow[])_dsFlight.FLIGHT.Select("URNO='" + flightUrNo + "'");
                                DSFlight.FLIGHTRow fRow;
                                if (fRows.Length < 1) { throw new ApplicationException("No flight information for flight Urno " + flightUrNo); }
                                else if (fRows.Length > 1)
                                {
                                    throw new ApplicationException("More than one records with flight id (" + flightUrNo + ")");
                                }
                                fRow = fRows[0];
                                arrOrDep = fRow.A_D;
                                if (arrOrDep != ADID_DEPARTURE_IND) throw new ApplicationException("Not Departure Flight " + flightUrNo);
                                //fRow.AT = row.ATD.ToString();
                                //fRow.MAB = row.MAB.ToString();
                                //fRow.PLB = row.PLB.ToString();
                                //fRow.LD_ST = row._LOAD_START.ToString();
                                //fRow.LD_END = row._LOAD_END.ToString();
                                //fRow.LST_DOOR = row._LAST_DOOR.ToString();
                                //fRow.BG_FST_TRP = row.FTRIP.ToString();
                                //fRow.BG_LST_TRP = row.LTRIP.ToString();

                                AssignTime(fRow, "AT", row.ATD.ToString());
                                AssignTime(fRow, "MAB", row.MAB.ToString());
                                AssignTime(fRow, "PLB", row.PLB.ToString());
                                AssignTime(fRow, "LD_ST", row._LOAD_START.ToString());
                                AssignTime(fRow, "LD_END", row._LOAD_END.ToString());
                                AssignTime(fRow, "LST_DOOR", row._LAST_DOOR.ToString());
                                AssignTime(fRow, "BG_FST_TRP", row.FTRIP.ToString());
                                AssignTime(fRow, "BG_LST_TRP", row.LTRIP.ToString());

                                //try
                                //{
                                //fRow.BG_FST_TRP = (string)row["FTRIP-B"];
                                //fRow.BG_LST_TRP = (string)row["LTRIP-B"];
                                //fRow.FST_CRGO = (string)row["FCARGO"];
                                //}
                                //catch (Exception ex)
                                //{
                                //    LogMsg("Departure Flight " + flightUrNo + " rec FCARGO " + ex.Message);
                                //}
                            }
                            catch (Exception)
                            {
                                //LogMsg("Departure Flight " + flightUrNo + " rec error : " + ex.Message);
                            }
                            _hasChanges = true;
                        }
                        #endregion
                        LogTraceMsg("Arr Flight Info Processing Completed.");
                    }
                    _lastDepFlightXMLDT = curDT;
                }
            }
            catch (Exception ex)
            {
                LogMsg("Departure Flight error : " + ex.Message);
            }
        }

        private void LoadJobXMLInfo()
        {//To get AO, BO
            string flightUrNo = "";
            try
            {
                DateTime curDT = GetLastWriteTime(fnJobXml);

                if (curDT.CompareTo(_lastJobXMLDT) != 0)
                {
                    //Job XML
                    DataSet dsJobXml = new DataSet();

                    dsJobXml.ReadXml(fnJobXml);
                    if (dsJobXml.Tables.Contains("JOB"))
                    {
                        int cnt = dsJobXml.Tables["JOB"].Rows.Count;
                        lock (_lockSummFlightChg)
                        {
                            LogTraceMsg("Job Info Processing.");
                            #region Process Job Information
                            for (int i = 0; i < cnt; i++)
                            {
                                flightUrNo = "";
                                string jobUrno = "";
                                try
                                {
                                    DataRow row = dsJobXml.Tables["JOB"].Rows[i];
                                    string empType = "";
                                    jobUrno = (string)row["URNO"];
                                    try
                                    {
                                        empType = ((string)row["EMPTYPE"]).Trim().ToUpper();
                                    }
                                    catch (Exception ex) { LogMsg("Error in EMPTYPE : " + ex.Message); }
                                    if (empType == "AO")
                                    {
                                        flightUrNo = (string)row["UAFT"];
                                        DSFlight.FLIGHTRow[] fRows = (DSFlight.FLIGHTRow[])_dsFlight.FLIGHT.Select("URNO='" + flightUrNo + "'");
                                        DSFlight.FLIGHTRow fRow;
                                        if (fRows.Length < 1) { throw new ApplicationException("No flight information for flight Urno " + flightUrNo); }
                                        else if (fRows.Length > 1)
                                        {
                                            throw new ApplicationException("More than one records with flight id (" + flightUrNo + ")");
                                        }
                                        fRow = fRows[0];

                                        fRow.AOID = (string)row["PENO"];
                                        string uFName = (string)row["FINM"];
                                        string uLName = (string)row["LANM"];
                                        string aoId = fRow.AOID;
                                        fRow.AO = "(" + aoId + ")" + uFName + " " + uLName.Trim();
                                        fRow.AOFNAME = uFName;
                                        fRow.AOLNAME = uLName;
                                    }
                                }
                                catch (Exception)
                                {
                                    //  LogMsg("Job Msg " + jobUrno + " rec Error : " + ex.Message);
                                }
                            }
                            #endregion
                            LogTraceMsg("Job Info Processing Completed.");
                            _hasChanges = true;
                        }
                    }
                    _lastJobXMLDT = curDT;
                }
            }
            catch (Exception)
            {
                //LogMsg("Job Msg Error : " + ex.Message);
            }
        }

        private void UpdateFlightXmlInfo()
        {
            LogTraceMsg("Has changes to update : " + _hasChanges);
            if (_hasChanges)
            {
                try
                {
                    LockFlightSummForWriting();
                    LogTraceMsg("SummFlight access before");
                    //_dsFlight.FLIGHT.Select("", "ST ASC");
                    //_dsFlight.WriteXml(fnFlightXml);
                    UtilDataSet.WriteXml(_dsFlight, fnFlightXml);
                    DateTime dt = GetLastWriteTime(fnFlightXml);
                    _lastFlightSummXmlDT = dt;
                    DE.FireFlightSummUpdEvent(_dsFlight, _lastFlightSummXmlDT);//Notify the changes of Summ Flight.
                    _hasChanges = false;
                    LogTraceMsg("Updated");
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    ReleaseFlightSummAfterWrite();
                }
            }
        }

        private bool HasChanges()
        {
            bool hasChanges = false;
            DateTime curDT;

            if (!hasChanges)
            {
                curDT = GetLastWriteTime(fnArrFlightTimingXml);
                //LogTraceMsg("CurAFlightDt : " + UtilTime.ConvDateTimeToUfisFullDTString(curDT) + ", " + UtilTime.ConvDateTimeToUfisFullDTString(_lastArrFlightXMLDT));
                if (curDT.CompareTo(_lastArrFlightXMLDT) != 0) hasChanges = true;
            }
            if (!hasChanges)
            {
                curDT = GetLastWriteTime(fnDepFlightTimingXml);
                //LogTraceMsg("CurDFlightDt : " + UtilTime.ConvDateTimeToUfisFullDTString(curDT) + ", " + UtilTime.ConvDateTimeToUfisFullDTString(_lastDepFlightXMLDT));
                if (curDT.CompareTo(_lastDepFlightXMLDT) != 0) hasChanges = true;
            }
            if (!hasChanges)
            {
                curDT = GetLastWriteTime(fnJobXml);
                //LogTraceMsg("CurJobDt : " + UtilTime.ConvDateTimeToUfisFullDTString(curDT) + ", " + UtilTime.ConvDateTimeToUfisFullDTString(_lastJobXMLDT));
                if (curDT.CompareTo(_lastJobXMLDT) != 0) hasChanges = true;
            }
            if (!hasChanges)
            {
                curDT = GetLastWriteTime(fnOrgFlightXml);
                //LogTraceMsg("CurFltDt : " + UtilTime.ConvDateTimeToUfisFullDTString(curDT) + ", " + UtilTime.ConvDateTimeToUfisFullDTString(_lastOrgFlightXMLDT));
                if (curDT.CompareTo(_lastOrgFlightXMLDT) != 0) hasChanges = true;
            }

            //LogTraceMsg("Has Changes in the method : " + hasChanges);
            return hasChanges;
        }

        private void LockFlightSummForWriting()
        {
            try
            {
                UtilLock.GetInstance().LockFlightSumm();
            }
            catch (Exception ex)
            {
                LogMsg("LockFlightSummForWriting:Err:" + ex.Message);
                if (ex.InnerException != null)
                {
                    LogMsg("LockFlightSummForWriting:err:Inner:" + ex.InnerException.Message);
                }
            }
        }

        private void ReleaseFlightSummAfterWrite()
        {
            try
            {
                UtilLock.GetInstance().ReleaseFlightSumm();
            }
            catch (Exception ex)
            {
                LogMsg("ReleaseFlightSummAfterWrite:Err:" + ex.Message);
                if (ex.InnerException != null)
                {
                    LogMsg("ReleaseFlightSummAfterWrite:err:Inner:" + ex.InnerException.Message);
                }
            }
        }

    }
}