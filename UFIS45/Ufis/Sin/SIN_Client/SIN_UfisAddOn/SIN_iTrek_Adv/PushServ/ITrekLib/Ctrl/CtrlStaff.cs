using System;
using System.Collections.Generic;
using System.Text;

using System.Data;
using System.Collections;

using UFIS.ITrekLib.Util;
using UFIS.ITrekLib.DB;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.Misc;
using MM.UtilLib;
namespace UFIS.ITrekLib.Ctrl
{
	public class CtrlStaff
	{

		 ////Staff data from interface
		 ////'<STFS>
		 ////  <STF>
		 ////    <URNO></URNO> //USTF
		 ////    <PNO></PNO>  //PENO
		 ////    <FNM></FNM>  //First Name
		 ////    <LNM></LNM>  //Last Name
		 ////    <STA></STA>  //Current Status. 'A'- Active, 'I'-InActive
		 ////    <CHG></CHG>  //Changes. 'I'-Insert. 'U'-Update. 'D'-Delete. 'R'-Request
		 ////  </STF>
		 ////  <STF>
		 ////    <URNO></URNO> //USTF
		 ////    <PNO></PNO>  //PENO
		 ////    <FNM></FNM>  //First Name
		 ////    <LNM></LNM>  //Last Name
		 ////    <STA></STA>  //Current Status. 'A'- Active, 'I'-InActive
		 ////    <CHG></CHG>  //Changes. 'I'-Insert. 'U'-Update. 'D'-Delete. 'R'-Request
		 ////  </STF> 
		 ////  </STFS>

		/// <summary>
		/// Upload the Interface Staff Data in xml format into iTrek Database.
		/// </summary>
		/// <param name="cmd">Command. null - if no other transactions</param>
		/// <param name="stData">staff data in xml format</param>
		/// <param name="userId">user Id who update the info</param>
		public static void UploadInterfaceData(IDbCommand cmd, string stData, string userId)
		{
			//         stData = @"
			//         <STFS>
			//  <STF>
			//    <URNO>14554343</URNO>
			//    <PNO>00202172</PNO>
			//    <FNM>Fn 202172</FNM>
			//    <LNM>Ln 00202172</LNM>
			//    <STA>A</STA>
			//    <CHG>I</CHG>
			//  </STF>
			//  <STF>
			//    <URNO>2345453</URNO>
			//    <PNO>00202171</PNO>
			//    <FNM>First Name 202171</FNM>
			//    <LNM>Last Name 00202171</LNM>
			//    <STA>A</STA>  //Current Status. 'A'- Active, 'I'-InActive
			//    <CHG>U</CHG>
			//  </STF> 
			//</STFS>";
			DataSet ds = UtilDataSet.LoadDataFromXmlString(stData);

			DateTime dt1 = DateTime.Now;
			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			bool isNewCmd = false;//Is command 'cmd' created from this method?
			int cntUpd, cntIns, cntDel, cntExistingRecords;

			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = db.GetCommand(true);
				}
				//dbFlight.UpdateOrgFlightInfo(cmd, dsOrgFlightXml, userId, ITrekTime.GetCurrentDateTime());
				DBDStaff.GetInstance().LoadInfoToDb(cmd, ds,
				   userId, ITrekTime.GetCurrentDateTime(),
				   out cntIns, out cntUpd, out cntDel, out cntExistingRecords);
				commit = true;
			}
			catch (Exception ex)
			{
				LogMsg("UploadInterfaceData:Err:" + ex.Message);
				throw;
			}
			finally
			{
				if (isNewCmd)
				{
					db.CloseCommand(cmd, commit);
				}
			}
			DateTime dt2 = DateTime.Now;
			string st = string.Format("UploadInterfaceData:CntXml={0},Existing={1},Insert={2}, Update={3}, Delete={4},Elapsed Time:{5}",
			   ds.Tables[0].Rows.Count,
			   cntExistingRecords,
			   cntIns, cntUpd, cntDel,
			   UtilTime.ElapsedTime(dt1, dt2));
			LogTraceMsg(st);

		}

		#region Logging
		private static void LogTraceMsg(string msg)
		{
			try
			{
                MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("CtrlStaff", msg);
				
			}
			catch (Exception)
			{
				UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("CtrlStaff", msg);
			}
		}

		private static void LogMsg(string msg)
		{
			//UFIS.ITrekLib.Util.UtilLog.LogToGenericEventLog("CtrlFlight", msg);
			try
			{

				MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("CtrlFlight", msg);
				
			}
			catch (Exception)
			{
				UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("CtrlFlight", msg);
			}
		}
		#endregion
		public static string RetrieveUrnoForPeno(string peno)
		{
			UtilDb db = UtilDb.GetInstance();

			IDbCommand cmd = null;
			string urno = "";

			try
			{
				cmd = db.GetCommand(false);

				urno = DBDStaff.GetInstance().RetrieveUrnoForPeno(cmd, peno);
                
			}
			catch (Exception ex)
			{
				LogTraceMsg("RetrieveUrnoForPeno" + ex.Message);
			}
			return urno;
		}

		public static string RetrieveUrnoForPeno(IDbCommand cmd, string peno)
		{
			UtilDb udb = UtilDb.GetInstance();

			bool isNewCmd = false;
			bool isNewTrx = false;
			bool commit = false;
			string urno = "";

			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = udb.GetCommand(true);
				}
				else if (cmd.Transaction == null)
				{
					isNewTrx = true;
					cmd.Transaction = cmd.Connection.BeginTransaction();
				}

				urno = DBDStaff.GetInstance().RetrieveUrnoForPeno(cmd, peno);
				if (string.IsNullOrEmpty(urno))
				{
					List<String> lsPeno = new List<string>();
					lsPeno.Add(peno);
					string replyTo = "";
					string reqId = UFIS.ITrekLib.CommMsg.CMsgOut.RequestStaffInfo(cmd, lsPeno, "auto", out replyTo);
					commit = true;
				}
			}
			catch (Exception ex)
			{
				LogTraceMsg("RetrieveUrnoForPeno" + ex.Message);
			}
			finally
			{
				if (isNewCmd)
				{
					udb.CloseCommand(cmd, commit);
				}
				else if (isNewTrx)
				{
					udb.CommitTransaction(cmd, commit);
				}
			}
			return urno;
		}

		public static String RetrieveStaffsForAFlight(string flightId)
		{
			string st = "";

			try
			{
				foreach (DSJob.JOBRow row in RetrieveFlightStaff(flightId).JOB.Rows)
				{
					if ((row.FINM == null) || (row.FINM == ""))
					{
						st += row.PENO + ", " + row.FCTN + "\n";
					}
					else
					{
						st += row.FINM + ", " + row.FCTN + "\n";
					}
				}
			}
			catch (Exception ex)
			{
				LogMsg("RetrieveStaffsForAFlight:Err:Flno:" + flightId + "," + ex.Message);
				throw new ApplicationException("Unable to get the staff information.");
			}
			return st;
		}

		public static DSJob RetrieveFlightStaff(string urNo)
		{
			UtilDb udb = UtilDb.GetInstance();
			DBDJob db = DBDJob.GetInstance();
			DSJob ds = new DSJob();
			IDbCommand cmd = null;
			bool isNewCmd = false;

			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = udb.GetCommand(false);
				}

				ds = db.RetrieveStaffsForAFlight(cmd, urNo);
			}
			catch (Exception ex)
			{
				LogMsg("RetrieveFlightStaff:Err:" + ex.Message);
				throw;
			}
			finally
			{
				if (isNewCmd)
				{
					udb.CloseCommand(cmd, false);
				}
			}

			return ds;
		}
	}
}
