using System;
using System.Collections.Generic;
using System.Text;

using System.Data;
using System.IO;

using UFIS.ITrekLib.DS;
using iTrekXML;
using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.Util;

namespace UFIS.ITrekLib.DB
{
    class DBPreDefMsg
    {
        private static Object _lockObj = new Object();
        private static readonly Object _lockObjInstance = new Object();
        //to use in synchronisation of single access
        private static DBPreDefMsg _dbPreDefMsg = null;
        private static DateTime lastWriteDT = new System.DateTime(1, 1, 1);

        private const string ACTIVE_MSG_STAT = "A";
        private const string INACTIVE_MSG_STAT = "I";

        private static DSPreDefMsg _dsPreDefMsg = null;
        private static string _fnPreDefMsgXml = null;//file name of PREDEFMSG XML in full path.

        /// <summary>
        /// Singleton Pattern.
        /// Not allowed to create the object from other class.
        /// To get the object, user "GetInstance()" method.
        /// </summary>
        private DBPreDefMsg() { }

        public void Init()
        {//Initialize all the information, to get the latest data when fetching them again.
            _dsPreDefMsg = null;
            _fnPreDefMsgXml = null;
            lastWriteDT = new System.DateTime(1, 1, 1);
        }

        /// <summary>
        /// Get the Instance of DBPreDefMsg. There will be only one reference for all the clients
        /// </summary>
        /// <returns></returns>
        public static DBPreDefMsg GetInstance()
        {
            if (_dbPreDefMsg == null)
            {
                lock (_lockObjInstance)
                {
                    try
                    {
                        if (_dbPreDefMsg == null)
                            _dbPreDefMsg = new DBPreDefMsg();
                    }
                    catch (Exception ex)
                    {
                        LogMsg("GetInstance:Err:" + ex.Message);
                    }
                }
            }
            return _dbPreDefMsg;
        }

        //public DSPreDefMsg RetrievePreDefMsgForAFlight(string flightUrNo)
        //{
        //    DSPreDefMsg ds = new DSPreDefMsg();

        //    foreach (DSPreDefMsg.PREDEFMSGRow row in dsPreDefMsg.PREDEFMSG.Select("UAFT='" + flightUrNo + "'"))
        //    {
        //        ds.PREDEFMSG.LoadDataRow(row.ItemArray, true);
        //    }

        //    return ds;
        //}

        private string fnPreDefMsgXml
        {
            get
            {
                if ((_fnPreDefMsgXml == null) || (_fnPreDefMsgXml == ""))
                {
                    iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
                    _fnPreDefMsgXml = path.GetPredefinedMsgFilePath();
                }

                return _fnPreDefMsgXml;
            }
        }

        private DSPreDefMsg dsPreDefMsg
        {
            get
            {
                //if (_dsPreDefMsg == null) { LoadDSPreDefMsg(); }
                LoadDSPreDefMsg();
                DSPreDefMsg ds = (DSPreDefMsg)_dsPreDefMsg.Copy();

                return ds;
            }
        }

        private void LoadDSPreDefMsg()
        {
            FileInfo fiXml = new FileInfo(fnPreDefMsgXml);
            DateTime curDT = fiXml.LastWriteTime;

            if ((_dsPreDefMsg == null) || (curDT.CompareTo(lastWriteDT) != 0))
            {
                lock (_lockObj)
                {
                    try
                    {
                        curDT = fiXml.LastWriteTime;//check the time again, in case of lock
                        if ((_dsPreDefMsg == null) || (curDT.CompareTo(lastWriteDT) != 0))
                        {
                            try
                            {
                                DSPreDefMsg tempDs = new DSPreDefMsg();

                                tempDs.PREDEFMSG.ReadXml(fnPreDefMsgXml);
                                if (_dsPreDefMsg == null)
                                {
                                    _dsPreDefMsg = new DSPreDefMsg();
                                }
                                else
                                {
                                    _dsPreDefMsg.PREDEFMSG.Clear();
                                }
                                _dsPreDefMsg.PREDEFMSG.Merge(tempDs.PREDEFMSG);
                                lastWriteDT = curDT;
                            }
                            catch (Exception ex)
                            {
                                LogMsg("LoadDSPreDefMsg : Error : " + ex.Message);
                                //TestWritePREDEFMSG();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogMsg("LoadDSPreDefMsg:Err:" + ex.Message);
                    }
                }
            }
        }

        private static void LogMsg(string msg)
        {
            //UtilLog.LogToGenericEventLog("DBPreDefMsg", msg);
           try
           {
              MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("DBPreDefMsg", msg);
           }
           catch (Exception)
           {
              UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DBPreDefMsg", msg);
           }
        }

        private void TestWritePREDEFMSG()
        {
            //{
            //    DSPreDefMsg ds = new DSPreDefMsg();
            //    DSPreDefMsg.PREDEFMSGRow row = ds.PREDEFMSG.NewPREDEFMSGRow();
            //    row.URNO = "PREDEFMSGUrno123";
            //    row.UAFT = "UAFT1324";
            //    row.CONTAINERNO = "Container No. 1";
            //    row.DES = "BKK";
            //    row.SENT_DT = DateTime.Now.AddMinutes(-20);
            //    row.RCV_DT = DateTime.Now.AddMinutes(-10);
            //    row.RMKS = "Remark";
            //    ds.PREDEFMSG.Rows.Add(row);
            //    ds.PREDEFMSG.AcceptChanges();
            //    ds.PREDEFMSG.WriteXml(fnPreDefMsgXml);
            //}
        }
        /////////
        public bool IsExistingMsg(string msg)
        {
            return (dsPreDefMsg.PREDEFMSG.Select("MSGTEXT='" + msg + "'").Length) > 0;
        }

        private bool IsActiveMsg(string msgId)
        {
            return GetMsgByMsgId(msgId).IsActive();          
        }

        private bool IsValidStatus(string stat)
        {
            bool validStat = false;
            if ((stat == ACTIVE_MSG_STAT) || (stat == INACTIVE_MSG_STAT)) validStat = true;
            return validStat;
        }

        private EntPreDefMsg GetMsgByMsgId(string msgId)
        {
            EntPreDefMsg entMsg = new EntPreDefMsg();
            DSPreDefMsg ds = dsPreDefMsg;

            DSPreDefMsg.PREDEFMSGRow row = ((DSPreDefMsg.PREDEFMSGRow[])dsPreDefMsg.PREDEFMSG.Select("URNO='" + msgId + "'"))[0];
            entMsg.msgId = row.URNO;
            entMsg.msg = row.MSGTEXT;
            entMsg.stat = row.STAT;
            return entMsg;
        }

        private EntPreDefMsg GetMsgByMsgText(string msg)
        {
            EntPreDefMsg entMsg = new EntPreDefMsg();
            DSPreDefMsg ds = dsPreDefMsg;

            DSPreDefMsg.PREDEFMSGRow row= ((DSPreDefMsg.PREDEFMSGRow[])dsPreDefMsg.PREDEFMSG.Select("MSGTEXT='" + msg + "'"))[0];
            entMsg.msgId = row.URNO;
            entMsg.msg = row.MSGTEXT;
            entMsg.stat = row.STAT;
            return entMsg;
        }

        public void UpdatePredefinedMsg(string msgId, string msg)
        {
            if (IsExistingMsg(msg))
            {
                string otherMsgId = GetUrNoForAMsg(msg);
                if ((otherMsgId!="") && (otherMsgId != msgId))
                {
                    EntPreDefMsg entMsg = GetMsgByMsgId(otherMsgId);
                    if (entMsg.IsActive()) throw new ApplicationException("Duplicate Message.");
                    else
                    {
                        DeletePredefinedMsgById(msgId);
                        UpdatePredefinedMsg(otherMsgId, msg, ACTIVE_MSG_STAT);
                    }
                }
                else
                {
                    UpdatePredefinedMsg(msgId, msg, "");
                }
            }
            else
            {
                UpdatePredefinedMsg(msgId, msg, "");
            }
        }

        private void UpdatePredefinedMsg(string urNo, string msg, string stat)
        {
            //if (!IsValidStatus(stat))
            //{
            //    throw new ApplicationException("Invalid Predefined Message status " + stat);
            //}
            //DSPreDefMsg ds = (DSPreDefMsg)dsPreDefMsg.Copy();
            DSPreDefMsg ds = dsPreDefMsg;

            DSPreDefMsg.PREDEFMSGRow row = ds.PREDEFMSG.FindByURNO(urNo);
            row.MSGTEXT = msg;
            if (stat != "")
            {
                if (IsValidStatus(stat))
                {
                    row.STAT = stat;
                }
                else throw new ApplicationException("Invalid Status [" + stat + "]");
            }

            ds.PREDEFMSG.AcceptChanges();
            ds.PREDEFMSG.WriteXml(fnPreDefMsgXml, XmlWriteMode.IgnoreSchema, false);
            lastWriteDT = lastWriteDT.AddYears(-1);
        }

        private string GetUrNoForAMsg(string msg)
        {
            string urNo = "";
            try
            {
                urNo = ((DSPreDefMsg.PREDEFMSGRow[])dsPreDefMsg.PREDEFMSG.Select("MSGTEXT='" + msg + "'"))[0].URNO;
            }
            catch (Exception)
            {
                
                //throw;
            } return urNo;
        }

        public void CreatePredefinedMsg(string msg)
        {
            if (IsExistingMsg(msg))
            {
                string urNo = GetUrNoForAMsg(msg);
                if (IsActiveMsg(urNo)) throw new ApplicationException("Duplicate Message.");
                else
                {
                    UpdatePredefinedMsg(urNo, msg, ACTIVE_MSG_STAT);
                }
            }
            else
            {
                string urNo = Convert.ToString(dsPreDefMsg.PREDEFMSG.Rows.Count + 1);
                DSPreDefMsg ds = (DSPreDefMsg)dsPreDefMsg.Copy();
                DSPreDefMsg.PREDEFMSGRow row = (DSPreDefMsg.PREDEFMSGRow)ds.PREDEFMSG.NewRow();
                row.MSGTEXT = msg;
                row.URNO = urNo;
                row.STAT = ACTIVE_MSG_STAT;
                //row.MSGPART = msg.Substring(0, 15);
                ds.PREDEFMSG.AddPREDEFMSGRow(row);
                ds.PREDEFMSG.AcceptChanges();
                ds.PREDEFMSG.WriteXml(fnPreDefMsgXml, XmlWriteMode.IgnoreSchema, false);
                lastWriteDT = lastWriteDT.AddYears(-1);
            }
        }

        public void DeletePredefinedMsg(string msg)
        {
            try
            {
                DSPreDefMsg ds = (DSPreDefMsg)dsPreDefMsg.Copy();

                DSPreDefMsg.PREDEFMSGRow row = ds.PREDEFMSG.FindByURNO(GetUrNoForAMsg(msg));
                row.STAT = INACTIVE_MSG_STAT;
                ds.PREDEFMSG.AcceptChanges();
                ds.PREDEFMSG.WriteXml(fnPreDefMsgXml, XmlWriteMode.IgnoreSchema, false);
                lastWriteDT = lastWriteDT.AddYears(-1);
            }
            catch (Exception ex)
            {
                LogMsg("DeletePredefinedMsg(string msg) : " + ex.Message);
                throw new ApplicationException("Unable to delete predefined message due to : " + ex.Message);
            }
        }

        public void DeletePredefinedMsgById(string urNo)
        {
            try
            {
                DSPreDefMsg ds = (DSPreDefMsg)dsPreDefMsg.Copy();
                DSPreDefMsg.PREDEFMSGRow row = ds.PREDEFMSG.FindByURNO(urNo);
                row.STAT = INACTIVE_MSG_STAT;
                ds.PREDEFMSG.AcceptChanges();
                ds.PREDEFMSG.WriteXml(fnPreDefMsgXml, XmlWriteMode.IgnoreSchema, false);
                lastWriteDT = lastWriteDT.AddYears(-1);
            }
            catch (Exception ex)
            {
                LogMsg("DeletePredefinedMsgById : " + ex.Message);
                throw new ApplicationException("Unable to delete predefined message");
            }
        }

        public DSPreDefMsg RetrieveActivePredifinedMsg()
        {
            DSPreDefMsg ds = null;
            ds = (DSPreDefMsg)dsPreDefMsg.Copy();
            ds.PREDEFMSG.Clear();
            foreach (DSPreDefMsg.PREDEFMSGRow row in dsPreDefMsg.PREDEFMSG.Select("STAT='" + ACTIVE_MSG_STAT + "'"))
            {
                ds.PREDEFMSG.LoadDataRow(row.ItemArray, true);
            }

            return ds;
        }

        public DSPreDefMsg RetrieveAllPredefinedMsg()
        {
            DSPreDefMsg ds = (DSPreDefMsg)dsPreDefMsg.Copy();

            return ds;
        }
        ////////

    }
}