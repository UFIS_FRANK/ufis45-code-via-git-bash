using System;
using System.Collections.Generic;
using System.Text;

using System.Data;
using System.IO;

using UFIS.ITrekLib.DS;
using iTrekXML;
using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.Util;
using System.Collections;
using System.Xml;

namespace UFIS.ITrekLib.DB
{
   public class DBTasks
   {
      private static Object _lockObj = new Object();
      private static readonly Object _lockObjInstance = new Object();
      //to use in synchronisation of single access
      private static DBTasks _DBTasks = null;
      private static DateTime lastWriteDT = new System.DateTime(1, 1, 1);

      private static DSTasks _dsTasks = null;
      private static string _fnXml = null;//file name of XML in full path.

      /// <summary>
      /// Singleton Pattern.
      /// Not allowed to create the object from other class.
      /// To get the object, user "GetInstance()" method.
      /// </summary>
      private DBTasks() { }

      public void Init()
      {//Initialize all the information, to get the latest data when fetching them again.
         _dsTasks = null;
         _fnXml = null;
         lastWriteDT = new System.DateTime(1, 1, 1);
      }

      /// <summary>
      /// Get the Instance of DBTasks. There will be only one reference for all the clients
      /// </summary>
      /// <returns></returns>
      public static DBTasks GetInstance()
      {
         if (_DBTasks == null)
         {
            lock (_lockObjInstance)
            {
               try
               {
                  if (_DBTasks == null)
                     _DBTasks = new DBTasks();
               }
               catch (Exception ex)
               {
                  LogMsg("GetInstance:Err:" + ex.Message);
               }
            }
         }
         return _DBTasks;
      }

      private string fnXml
      {
         get
         {
            if ((_fnXml == null) || (_fnXml == ""))
            {
               iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
               _fnXml = path.GetTaskFilePath();
            }

            return _fnXml;
         }
      }

      private DSTasks CurDSTasks
      {
         get
         {
            //if (_DsOCpmPriority == null) { LoadDSCpmPriority(); }
            LoadDSTasks();
            DSTasks ds = (DSTasks)_dsTasks.Copy();

            return ds;
         }
      }


      private void LoadDSTasks()
      {
         FileInfo fiXml = new FileInfo(fnXml);
         DateTime curDT = fiXml.LastWriteTime;

         if ((_dsTasks == null) || (curDT.CompareTo(lastWriteDT) != 0))
         {
            lock (_lockObj)
            {
               try
               {
                  fiXml = new FileInfo(fnXml);
                  curDT = fiXml.LastWriteTime;//check the time again, in case of lock
                  if ((_dsTasks == null) || (curDT.CompareTo(lastWriteDT) != 0))
                  {
                     try
                     {
                        DSTasks tempDs = new DSTasks();

                        tempDs.HAG.ReadXml(fnXml);
                        _dsTasks = tempDs;
                        lastWriteDT = curDT;
                     }
                     catch (Exception ex)
                     {
                        LogMsg("LoadDSTasks : Error : " + ex.Message);
                     }
                  }
               }
               catch (Exception ex)
               {
                  LogMsg("LoadDSTasks:Err:" + ex.Message);
               }
            }
         }
      }

      private static void LogMsg(string msg)
      {
         //UtilLog.LogToGenericEventLog("DBTasks", msg); 
         try
         {
            MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("DBTasks", msg);
         }
         catch (Exception)
         {
            UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DBTasks", msg);
         }
      }

      private static void LogTraceMsg(string msg)
      {
         try
         {
            MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("DBTasks", msg);
         }
         catch (Exception)
         {
            UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DBTasks", msg);
         }
      }


      public bool IsExist(string tasks)
      {
         bool exist = false;

         DSTasks.HAGRow[] rows = (DSTasks.HAGRow[])
             CurDSTasks.HAG.Select(GetSelectStatement(tasks));

         if ((rows != null) && (rows.Length > 0)) exist = true;
         return exist;
      }

      private string GetSelectStatement(string tasks)
      {
         return "TASK='" + tasks + "'";
      }

      private DSTasks ReadFromFile()
      {
         // DSTasks ds = new DSTasks();
         //ds.HAG.ReadXml(fnXml);

         return CurDSTasks;
      }

      private void WriteToFile(DSTasks ds)
      {
         ds.HAG.AcceptChanges();
         ds.HAG.WriteXml(fnXml, XmlWriteMode.IgnoreSchema, false);
         _dsTasks = ds;
         lastWriteDT = lastWriteDT.AddYears(-1);
      }

      public void Create(string tasks, string airlineCode)
      {
         try
         {
            DSTasks ds = ReadFromFile();
            DSTasks.HAGRow row = (DSTasks.HAGRow)ds.HAG.NewRow();
            row.ALC2 = airlineCode;
            row.TASK = tasks;

            ds.HAG.AddHAGRow(row);
            ds.HAG.AcceptChanges();
            WriteToFile(ds);
         }
         catch (Exception ex)
         {
            LogMsg("Exception" + ex.Message);
         }
      }

      public ArrayList GetAirlineCode(string task)
      {
         ArrayList lstAlcList = new ArrayList();
         try
         {
            DSTasks ds = ReadFromFile();
            string alc2String = ((DSTasks.HAGRow)(ds.HAG.Select("TASK='" + task + "'"))[0]).ALC2;
            foreach (string al in alc2String.Split(new char[] { ';' }))
            {
               lstAlcList.Add(al);
            }
         }
         catch (Exception ex)
         {
            LogMsg("Exception" + ex.Message);
         }
         return lstAlcList;
      }

      private static string _ApronAlc="";
      private static string _BaggageAlc="";

      public string GetAirlineCodeInString(string task)
      {
         if (task == null) task = "";
         string strAlc = "";
         try
         {
            DSTasks ds = ReadFromFile();
            strAlc = ((DSTasks.HAGRow)(ds.HAG.Select("TASK='" + task + "'"))[0]).ALC2;
            switch (task.ToUpper())
            {
               case "APRON":
                  if ((!string.IsNullOrEmpty(_ApronAlc)) && (_ApronAlc != strAlc))
                  {
                     LogTraceMsg(string.Format("Task<{0}>,Val<{1}>,bf<{2}>", task, strAlc, _ApronAlc));
                     _ApronAlc = strAlc;
                  }
                  break;
               case "BAGGAGE":
                  if ((!string.IsNullOrEmpty(_BaggageAlc)) && (_BaggageAlc != strAlc))
                  {
                     LogTraceMsg(string.Format("Task<{0}>,Val<{1}>,bf<{2}>", task, strAlc, _BaggageAlc));
                     _BaggageAlc = strAlc;
                  }
                  break;
               default:
                  LogTraceMsg(string.Format("Task<{0}>,Val<{1}>", task, strAlc));
                  break;
            }
         }
         catch (Exception ex)
         {
            LogMsg("GetAirlineCodeInString:Err:" + ex.Message);
         }
         return strAlc;
      }

      public DSTasks RetrieveAll()
      {
         DSTasks ds = null;
         ds = (DSTasks)CurDSTasks.Copy();

         return ds;
      }

      public void CreateNewTasks(string message)
      {
         try
         {
            XmlDocument MainBatchMsgsXml = new XmlDocument();
            MainBatchMsgsXml.LoadXml(message);
            lock (_lockObj)
            {
               try
               {
                  MainBatchMsgsXml.Save(fnXml);
               }
               catch (Exception ex)
               {
                  LogTraceMsg("CreateNewTasks:Err:" + ex.Message);
               }
            }
         }
         catch (Exception ex)
         {
            LogTraceMsg("CreateNewTasks Loading Document:Err:" + ex.Message);
         }
      }

   }
}