using System;
using System.Collections.Generic;
using System.Text;

using System.Collections;

namespace UFIS.ITrekLib.ENT
{
    public class EntAccessRights
    {
        Hashtable htAccessRights = null;

        public EntAccessRights()
        {
            htAccessRights = new Hashtable();
        }

        public bool HasPageCtrl(string pageName, string ctrlName, out bool hasRight)
        {
            bool hasPageCtrl = false;
            hasRight = false;
            EntPageControl key = new EntPageControl(pageName, ctrlName);
            if (htAccessRights.ContainsKey(key))
            {
                hasRight = (bool)htAccessRights[key];
            }
            return hasPageCtrl;
        }

        public void SetPageCtrlAccessRight(string pageName, string ctrlName, bool hasRight)
        {
            EntPageControl key = new EntPageControl(pageName, ctrlName);
            if (htAccessRights.ContainsKey(key))
            {
                htAccessRights[key] = hasRight;
            }
            else
            {
                htAccessRights.Add(key, hasRight);
            }
        }
    }

    public class EntPageControl:IComparable
    {
        private string _pageName = string.Empty;
        private string _ctrlName = string.Empty;

        public EntPageControl(string pageName, string ctrlName)
        {
            _pageName = pageName;
            _ctrlName = ctrlName;            
        }

        public int CompareTo(object obj)
        {
            int result = 0;

            if (obj is EntPageControl)
            {
                EntPageControl entPageCtrl = (EntPageControl)obj;
                result = _pageName.CompareTo(entPageCtrl._pageName);
                if (result == 0)
                {
                    result = _ctrlName.CompareTo(entPageCtrl._ctrlName);
                }
            }
            else throw new ApplicationException("Invalid Object to compare for Page Control.");
            return result;
        }
    }
}
