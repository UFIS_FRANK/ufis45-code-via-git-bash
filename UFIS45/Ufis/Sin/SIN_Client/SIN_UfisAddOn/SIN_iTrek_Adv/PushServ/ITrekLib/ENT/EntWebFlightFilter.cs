using System;
using System.Collections.Generic;
using System.Text;

using System.Collections;

using UFIS.ITrekLib.DS;

namespace UFIS.ITrekLib.ENT
{
   public class EntWebFlightFilter
   {
      #region public constant
      public const char TERMINAL_SEPERATOR = ';';
      public const char AIRLINE_CODE_SEPERATOR = ';';
      public const char REGN_SEPERATOR = ';';
      public const char TIME_SEPERATOR = ':';
      public const string FLIGHTTYPE_ARRIVAL = "A";
      public const string DEF_FLIGHTTYPE = "";
      #endregion


      #region Private Variable
      private EnUserDept _userDept = EnUserDept.Undefined;
      private string _airlineCodesByDept = "";//Need to assign from Control before passing to DB to filter
      private EnFlightDateFilterType _dateFilterType = EnFlightDateFilterType.UseATETST;//Use AT,ET,ST
      private string _flightType = "";//Flight Type. Arrival or Departure.
      #endregion

      #region property
      /// <summary>
      /// Airline codes handled by Dept.
      /// To be assigned by Controller before passing to DB for flight filter
      /// </summary>
      public string AirlineCodesByDept
      {
         get
         {
            if (_airlineCodesByDept == null) _airlineCodesByDept = "";
            return _airlineCodesByDept;
         }
         set
         {
            if (String.IsNullOrEmpty(value)) _airlineCodesByDept = "";
            else _airlineCodesByDept = AIRLINE_CODE_SEPERATOR + value + AIRLINE_CODE_SEPERATOR;
         }
      }

      /// <summary>
      /// Flight Type. Arrival(A) or Departure(D)
      /// </summary>
      public string FlightType
      {
         get
         {
            if (_flightType == null) _flightType = "";
            return _flightType;
         }
         set
         {
            if (value == null) _flightType = "";
            else _flightType = value.Trim().ToUpper();
         }
      }

      /// <summary>
      /// Date Filter Type. Date will be filtered base on the filter type.
      /// </summary>
      public EnFlightDateFilterType DateFilterType
      {
         get
         {
            return _dateFilterType;
         }
         set
         {
            _dateFilterType = value;
         }
      }

      /// <summary>
      /// Get the Dataset field name for Flight Date Time to use in flitering by date time.
      /// </summary>
      public string StDateFilterType
      {
         get
         {
            string st = "FLIGHT_DT";//Dataset field name for Flight Date Time using AT,ET,ST
            switch (DateFilterType)
            {
               case EnFlightDateFilterType.UseATETST:
                  st = "FLIGHT_DT";//Dataset field name for Flight Date Time using AT,ET,ST
                  break;
               case EnFlightDateFilterType.UseOBLETST:
                  st = "VBA_FLDT";//Dataset field name for Flight Date Time using OBL,ET,ST
                  break;
            }
            return st;
         }
      }

      /// <summary>
      /// to filter the airline code according to the User Dept 
      /// </summary>
      public EnUserDept Dept
      {
         get
         {
            return _userDept;
         }
         set
         {
            _userDept = value;
         }
      }

      private string _dayOption = "";//Days Option. Today/Back-1 Day/Back-2 Day

      /// <summary>
      /// Day option to filter. Today(T)/ Back-1 Day (T-1)/ Back-2 Day (T-2)
      /// </summary>
      public string DayOption
      {
         get { return _dayOption; }
         set
         {
            if (value == null) _dayOption = "";
            else _dayOption = value;
         }
      }

      string _terminalIds = ""; //';' seperated terminal Ids (e.g. 1;2;4)
      string _cTerminalIds = ";;";
      //string _whTerminal = "";

      string _dateInd = ""; //date indicator
      string _airlineCodes = ""; //';' seperated airline code
      string _cAirlineCodes = ";;";

      int _frMinute = -120;
      int _toMinute = 240;

      string _frTime = "";// From time (e.g. 11:00)
      string _toTime = "";// To time in 24Hr (e.g. 15:00)

      string _cFrTime = "";// From Time in HHMM format (e.g. 0730)
      string _cToTime = "";// From Time in HHMM format (e.g. 2140)
      //bool _cTimeReverse = false; 
      ////If false, time must be between _cFrTime and _cToTime (inclusive)
      ////         e.g.  From Time 0900 ==> To Time 2100
      ////               flight time must be >= 0900 and <=2100
      ////If true, time must not be between _cToTime and _cFrTime (exclusive)
      ////         e.g.  From Time 2130 ==> To Time 0730
      ////               flight time must NOT be > 0730 and <2130

      string _regn = ""; //Aircraft registration (to use with wildcard)
      string _cRegn = ";;"; //

      readonly char[] delTerminal = { TERMINAL_SEPERATOR };

      /// <summary>
      /// Filter the flight From this minute relative to now
      /// </summary>
      public int FrMinute
      {
         get { return _frMinute; }
         set
         {
            //if (value > 0) value = -value;
            _frMinute = value;
         }
      }

      /// <summary>
      /// Filter the flight To this minute relative to now
      /// </summary>
      public int ToMinute
      {
         get { return _toMinute; }
         set
         {
            //if (value < 0) value = -value;
            _toMinute = value;
         }
      }

      private DateTime _frFixedDateTime;
      private DateTime _toFixedDateTime;
      private bool _useFrToFixedDateTime = false;

      #region Fixed Date Time to filter
      /// <summary>
      /// Indicate whether to use the Fixed Date Time or Relative Date Time from NOW. Default is 'False'
      /// If True, use 'FrFixedDateTime' and 'ToFixedDateTime.
      /// Otherwise, Use 'FromMinute' and 'ToMinute' to comupute the Date Time relative to NOW
      /// </summary>
      public bool UseFrToFixedDateTime
      {
         get { return _useFrToFixedDateTime; }
         set { _useFrToFixedDateTime = value; }
      }

      /// <summary>
      /// Filter from this fixed Date and Time. Applicable when 'UseFrToFixedDateTime' is 'True'
      /// </summary>
      public DateTime FrFixedDateTime
      {
         get { return _frFixedDateTime; }
         set
         {
            _frFixedDateTime = value;
         }
      }

      /// <summary>
      /// Filter to this fixed Date and Time. Applicable when 'UseFrToFixedDateTime' is 'True'
      /// </summary>
      public DateTime ToFixedDateTime
      {
         get { return _toFixedDateTime; }
         set { _toFixedDateTime = value; }
      }
      #endregion

      #region Get 'From' and 'To' Date Time to filter (Use in DB filter)
      public DateTime FromDateTime
      {
         get
         {
            if (UseFrToFixedDateTime) return FrFixedDateTime;
            return DateTime.Now.AddMinutes(_frMinute);
         }
      }

      public DateTime ToDateTime
      {
         get
         {
            if (UseFrToFixedDateTime) return ToFixedDateTime;
            return DateTime.Now.AddMinutes(_toMinute);
         }
      }
      #endregion

      /// <summary>
      /// Is this flight meet with Filter Criteria for Airline code and Registration
      /// </summary>
      /// <param name="airlineCode">Airline Code</param>
      /// <param name="regn">Registration</param>
      /// <returns>Meet Criteria ==> True</returns>
      public bool IsMeetCriteria(string airlineCode, string regn)
      {
         bool meetCriteria = true;         

         if (meetCriteria && Regn != "")
         {
            if (!_cRegn.Contains(REGN_SEPERATOR + regn + REGN_SEPERATOR))
            {
               meetCriteria = false;
            }
         }

         if (meetCriteria && (!string.IsNullOrEmpty(airlineCode)))
         {
            string stCode = AIRLINE_CODE_SEPERATOR + airlineCode + AIRLINE_CODE_SEPERATOR;
			//LogTraceMsg("AirlineCodesFilter:" + AirlineCodesByDept);
            if (meetCriteria && (!string.IsNullOrEmpty(AirlineCodesByDept)))
            {               
               if (!AirlineCodesByDept.Contains(stCode))
               {
                  meetCriteria = false;
               }
            }

            if (meetCriteria && AirlineCodes != "")
            {
               if (!_cAirlineCodes.Contains(stCode))
               {
                  meetCriteria = false;
               }
            }
         }

         return meetCriteria;
      }

      //public string CAirlineCodes
      //{
      //    get{ return _cAirlineCodes ; }
      //}

      //private string CTerminalIds
      //{
      //    get { return _cTerminalIds; }
      //}        

      /// <summary>
      /// Terminal Ids (seperated by ;) to filter
      /// </summary>
      public string TerminalIds
      {
         get
         {
            return _terminalIds;
         }
         set
         {
            if (value == null) value = "";
            else value = value.Trim();

            if (_terminalIds != value)
            {
               _terminalIds = value;
               _cTerminalIds = TERMINAL_SEPERATOR + _terminalIds + TERMINAL_SEPERATOR;
               _filterString = null;
            }
         }
      }

      /// <summary>
      /// Date Indicator. Today, Back 1 day, Back 2 day for display back the info
      /// </summary>
      public string DateInd
      {
         get { return _dateInd; }
         set
         {
            _dateInd = value;
            _filterString = null;
         }
      }

      /// <summary>
      /// Airline Code to filter. Further filter from AirlineCodes for Department
      /// </summary>
      public string AirlineCodes
      {
         get { return _airlineCodes; }
         set
         {
            if (value == null) value = "";
            else value = value.Trim();

            if (_airlineCodes != value)
            {
               _airlineCodes = value;
               _cAirlineCodes = AIRLINE_CODE_SEPERATOR + _airlineCodes + AIRLINE_CODE_SEPERATOR;
               _filterString = null;
            }
         }
      }

      #region Get/Set 'From' and 'To' Time (e.g. 10:30 - 21:50)
      /// <summary>
      /// From Time to filter (e.g. 13:21)
      /// </summary>
      public string FrTime
      {
         get { return _frTime; }
         set
         {
            string stTime = "";
            if (IsValidTime(value, out stTime))
            {
               _frTime = stTime;
               _cFrTime = _frTime.Replace(TIME_SEPERATOR.ToString(), "");
               _filterString = null;
            }
            else
            {
               throw new ApplicationException("Invalid From Time");
            }
         }
      }

      /// <summary>
      /// To Time to filter (e.g. 21.50)
      /// </summary>
      public string ToTime
      {
         get { return _toTime; }
         set
         {
            string stTime = "";
            if (IsValidTime(value, out stTime))
            {
               _toTime = stTime;
               _cToTime = _toTime.Replace(":", "");
               _filterString = null;
            }
            else
            {
               throw new ApplicationException("Invalid To Time");
            }
         }
      }
      #endregion

      #region Get 'From' and 'To' Time Hour and Minute
      /// <summary>
      /// Get From Time Hour
      /// </summary>
      public int FrTimeHH
      {
         get
         {
            int hh = 0;
            if (_frTime != "")
            {
               try
               {
                  char[] del = { TIME_SEPERATOR };
                  string[] timeArr = _frTime.Split(del);
                  hh = Int32.Parse(timeArr[0]);
               }
               catch (Exception)
               {
               }
            }
            return hh;
         }
      }

      /// <summary>
      /// Get From Time Minute
      /// </summary>
      public int FrTimeMM
      {
         get
         {
            int mm = 0;
            if (_frTime != "")
            {
               try
               {
                  char[] del = { TIME_SEPERATOR };
                  string[] timeArr = _frTime.Split(del);
                  mm = Int32.Parse(timeArr[1]);
               }
               catch (Exception)
               {
               }
            }
            return mm;
         }
      }

      /// <summary>
      /// Get To Time Hour
      /// </summary>
      public int ToTimeHH
      {
         get
         {
            int hh = 23;

            if (ToTime != "")
            {
               if ((ToTime != "") && (FrTime.CompareTo(ToTime) > 0))
               {
               }
               else
               {
                  try
                  {
                     char[] del = { TIME_SEPERATOR };
                     string[] timeArr = ToTime.Split(del);
                     hh = Int32.Parse(timeArr[0]);
                  }
                  catch (Exception)
                  {
                  }
               }
            }
            return hh;
         }
      }

      /// <summary>
      /// Get To Time Minute
      /// </summary>
      public int ToTimeMM
      {
         get
         {
            int mm = 59;
            if (ToTime != "")
            {
               if ((ToTime != "") && (FrTime.CompareTo(ToTime) > 0))
               {
               }
               else
               {
                  try
                  {
                     char[] del = { TIME_SEPERATOR };
                     string[] timeArr = ToTime.Split(del);
                     mm = Int32.Parse(timeArr[1]);
                  }
                  catch (Exception)
                  {
                  }
               }
            }
            return mm;
         }
      }
      #endregion


      /// <summary>
      /// Registration Codes to filter (seperated by ;)
      /// </summary>
      public string Regn
      {
         get { return _regn; }
         set
         {
            _regn = value;
            _cRegn = REGN_SEPERATOR + _regn + REGN_SEPERATOR;
            _filterString = null;
         }
      }


      private string _filterString = null;

      /// <summary>
      /// Filter Condition string for display
      /// </summary>
      public string FilterString
      {
         get
         {
            if (_filterString == null)
            {
               _filterString = "Terminal (" + TerminalIds + "), " +
                   DateInd + ", " +
                   "Airlines (" + AirlineCodes + "), " +
                   "From (" + FrTime + ") - To (" + ToTime + "), " +
                   "Regn (" + Regn + ")";
            }
            return _filterString;
         }
      }

      /// <summary>
      /// Is valid Time
      /// </summary>
      /// <param name="time">Time string input (e.g. 9:20)</param>
      /// <param name="stTime">Time string output (e.g. 09:20)</param>
      /// <returns>Valid ==> True</returns>
      public bool IsValidTime(string time, out string stTime)
      {
         bool valid = false;
         stTime = "";

         if (string.IsNullOrEmpty(time)) valid = true;
         else
         {
            try
            {
               char[] del = { ':' };
               string[] timeArr = time.Trim().Split(del);
               int hh = -1;
               int mm = -1;
               switch (timeArr.Length)
               {
                  case 1:
                     mm = 0;
                     if (timeArr[0].Trim() == "") hh = 0;
                     else hh = Int32.Parse(timeArr[0]);
                     break;
                  case 2:
                     if (timeArr[0].Trim() == "") hh = 0;
                     else hh = Int32.Parse(timeArr[0]);

                     if (timeArr[1].Trim() == "") mm = 0;
                     else mm = Int32.Parse(timeArr[1]);
                     break;

               }
               if ((0 <= hh) && (hh <= 23) && (0 <= mm) && (mm <= 59))
               {
                  stTime = hh.ToString().PadLeft(2, '0') + ":" + mm.ToString().PadLeft(2, '0');
                  valid = true;
               }
            }
            catch (Exception)
            {
            }
         }
         return valid;
      }
      #endregion

      private static void LogTraceMsg(string msg)
      {
         UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("CtrlFlight", msg);
      }
   }
}