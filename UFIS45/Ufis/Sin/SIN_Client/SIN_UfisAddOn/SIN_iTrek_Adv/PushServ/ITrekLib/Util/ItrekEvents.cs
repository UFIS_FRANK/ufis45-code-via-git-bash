using System;
using System.Collections.Generic;
using System.Text;

using System.Threading;
using System.Runtime.Remoting.Messaging;

namespace UFIS.ITrekLib.Util
{

    public delegate void FlightSummUpdEventHandler(object sender, DS.DSFlight dsFlight, DateTime dtLastUpd);
    public delegate void JobSummUpdEventHandler(object sender, DS.DSJob dsJob, DateTime dtLastUpd);
    
    class DE
    {
        public static event FlightSummUpdEventHandler FlightSummUpdated;
        public static event JobSummUpdEventHandler JobSummUpdated;

        public static void FireFlightSummUpdEvent(DS.DSFlight ds, DateTime dtLastUpd)
        {
            try
            {
                EventsHelper.FireAsync(FlightSummUpdated, new object[] { null, ds, dtLastUpd });
                //EventsHelper.Fire(FlightSummUpdated, new object[] { null, ds, dtLastUpd });
            }
            catch (Exception ex)
            {
                LogTraceMsg("FireFlightSummUpdEvent:Err:" + ex.Message);
            }
        }

        public static void FireJobSummUpdEvent(DS.DSJob ds, DateTime dtLastUpd)
        {
            try
            {
                EventsHelper.FireAsync(JobSummUpdated, new object[] { null, ds, dtLastUpd });
                //EventsHelper.Fire(FlightSummUpdated, new object[] { null, ds, dtLastUpd });
            }
            catch (Exception ex)
            {
                LogTraceMsg("FireJobSummUpdEvent:Err:" + ex.Message);
            }
        }

        private static void LogTraceMsg(string msg)
        {
            UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DE", msg);
        }
    }

    public class EventsHelper
    {
        //[OneWay]
        public static void Fire(Delegate del, params object[] args)
        {
            if (del == null)
            {
                return;
            }
            Delegate[] delegates = del.GetInvocationList();
            foreach (Delegate sink in delegates)
            {
                try
                {
                    LogTraceMsg("Calling Fire.");
                    InvokeDelegate(sink, args);
                }
                catch (Exception ex)
                {
                    LogTraceMsg("Fire:Err:" + ex.Message);
                }
            }
        }

        //[OneWay]
        private static void InvokeDelegate(Delegate sink, object[] args)
        {
            try
            {
                //LogTraceMsg("InvokeDelegate START");
                sink.DynamicInvoke(args);
                //LogTraceMsg("InvokeDelegate FINISH");
            }
            catch(Exception ex)
            {
                LogTraceMsg("InvokeDelegate ERR" + ex.Message);
            }
        }

        delegate void AsyncInvokeDelegate(Delegate del, params object[] args);
        //[OneWay]
        public static void FireAsync(Delegate del, params object[] args)
        {
            if (del == null)
            {
                return;
            }
            Delegate[] delegates = del.GetInvocationList();

            AsyncInvokeDelegate invoker = new AsyncInvokeDelegate(InvokeDelegate);
            foreach (Delegate sink in delegates)
            {
                try
                {
                    LogTraceMsg("Calling FireAsync.");
                    invoker.BeginInvoke(sink, args, null, null);
                }
                catch (Exception ex)
                {
                    LogTraceMsg("FireAsync:Invoke:Err:" + ex.Message);
                }
            }
        }

        private static void LogTraceMsg(string msg)
        {
            UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("ItrekEvents", msg);
        }

    }

}
