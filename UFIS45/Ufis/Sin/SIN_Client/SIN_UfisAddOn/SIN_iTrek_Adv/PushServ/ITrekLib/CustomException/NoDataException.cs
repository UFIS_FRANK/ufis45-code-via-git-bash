using System;
using System.Collections.Generic;
using System.Text;

namespace UFIS.ITrekLib.CustomException
{
    public class NoDataException: ApplicationException
    {
        private const string DEFAULT_MSG = "No Data.";

        public NoDataException()
            : base(DEFAULT_MSG)
        {
        }

        public NoDataException(string msg)
            : base(msg)
        {
        }
    }
}