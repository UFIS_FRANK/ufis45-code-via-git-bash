using System;
using System.Collections.Generic;
using System.Text;

namespace UFIS.ITrekLib.Config
{
	public class ProcIds
	{
		public const string MGO = "ITK_MGOTAB"; // Communication (MQ) Msg Out - Table ITK_MGOTAB
		public const string MGI = "ITK_MGITAB";// Communication (MQ) Msg In - Table ITK_MGITAB
		public const string FLIGHT = "ITK_FLIGHT";//Flight Id
		public const string MSG = "ITK_MSG";//Message record - Table ITK_MSG;
		public const string MSGTO = "ITK_MSGTO";//Message To record - Table ITK_MSGTO; 

		public const string CPM = "ITK_CPMDET"; //  Cpm Details - Table ITK_CPMDET
		public const string JOB = "ITK_JOB";   // JOB- Table ITK_JOB
		public const string TRP = "ITK_TRP";   // Trip- Table ITK_TRP
		public const string TRPMST = "ITK_TRPMST";   //  Trip Master- Table ITK_TRPMST
		public const string TRPDET = "ITK_TRPDET";   // Trip dETAILS- Table ITK_TRPDET
	}
}
