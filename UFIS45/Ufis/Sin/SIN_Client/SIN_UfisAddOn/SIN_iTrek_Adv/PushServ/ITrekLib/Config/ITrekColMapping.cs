using System;
using System.Collections.Generic;
using System.Text;

using System.IO;
using System.Collections;

namespace UFIS.ITrekLib.Config
{
	/// <summary>
	/// Column Name Mapping
	/// </summary>
	public class ITrekColMapping
	{
		#region Singleton
		private static ITrekColMapping _this = null;
		private ITrekColMapping() { }

		public static ITrekColMapping GetInstance()
		{
			if (_this == null)
			{
				_this = new ITrekColMapping();
			}
			return _this;
		}
		#endregion

		#region ASR Table Mapping
		private static Hashtable _htDsAsrToDbAsrColMap = null;
		private static Hashtable _htDbAsrToDsColMap = null;

		private static bool _preparedAsrMapping = false;
		private static readonly object _lockAsrMappingObj = new object();

		public static bool IsDbAsrColNameForDsColVP(string stDsApronServiceRptColName, out string dbColNamePrefix)
		{
			bool isVp = true;
			dbColNamePrefix = "";
			switch (stDsApronServiceRptColName)
			{
				case "PLBA":
					dbColNamePrefix = "PLBA";
					break;
				case "PLBB":
					dbColNamePrefix = "PLBB";
					break;
				case "JCLFWD":
					dbColNamePrefix = "JFWD";
					break;
				case "JCLAFT":
					dbColNamePrefix = "JAFT";
					break;
				case "TRNO":
					dbColNamePrefix = "TRNO";
					break;
				case "SLFWD":
					dbColNamePrefix = "SFWD";
					break;
				case "SLAFT":
					dbColNamePrefix = "SAFT";
					break;
				default:
					isVp = false;
					break;
			}
			return isVp;
		}

		public static string GetDbAsrColNameForDsCol(string stDsApronServiceRptColName)
		{
			string dbColName = "";
			PrepareAsrMapping();
			if (_htDsAsrToDbAsrColMap.ContainsKey(stDsApronServiceRptColName))
			{
				dbColName = (string)_htDsAsrToDbAsrColMap[stDsApronServiceRptColName];
			}
			//else throw new ApplicationException("Invalid ASR Ds Column Name " + stDsApronServiceRptColName);
			return dbColName;
		}

		public static string GetDsAsrColForDbCol(string stDbColName)
		{
			PrepareAsrMapping();
			if (_htDbAsrToDsColMap.ContainsKey(stDbColName))
			{
				return (string)_htDbAsrToDsColMap[stDbColName];
			}
			else throw new ApplicationException("Invalid ASR Db Column Name " + stDbColName);
		}

		private static void PrepareAsrMapping()
		{
			if (!_preparedAsrMapping)
			{
				lock (_lockAsrMappingObj)
				{
					if (!_preparedAsrMapping)
					{
						_htDsAsrToDbAsrColMap = new Hashtable();
						_htDbAsrToDsColMap = new Hashtable();
						AddAsrMapping("URNO", "URNO");
						AddAsrMapping("MODE", "FLMO");
						AddAsrMapping("LOAD", "LOAD");
						AddAsrMapping("CBRIEF", "CBRF");
						AddAsrMapping("CSAFE", "CSAF");
						AddAsrMapping("CCONE", "CCON");
						AddAsrMapping("CLOW", "CLOW");
						AddAsrMapping("CEXT", "CEXT");
						AddAsrMapping("CBRTEST", "CBRT");
						AddAsrMapping("CDMGBDY", "CDBY");
						AddAsrMapping("CDMGDG", "CDDG");
						AddAsrMapping("CDG", "CDGL");
						AddAsrMapping("CULD", "CULD");
						AddAsrMapping("ULDCMT", "RULD");
						AddAsrMapping("GENCMT", "RGEN");
						AddAsrMapping("RSNBPT", "RRBT");
						AddAsrMapping("DRO", "DROI");
						AddAsrMapping("DM", "DMNI");
						AddAsrMapping("DRAFT", "PSTA");
						_preparedAsrMapping = true;
					}
				}
			}
		}

		private static void AddAsrMapping(string stDsAsrColName, string stDbColName)
		{
			_htDsAsrToDbAsrColMap.Add(stDsAsrColName, stDbColName);
			_htDbAsrToDsColMap.Add(stDbColName, stDsAsrColName);
		}
		#endregion

		#region Flight Table Mapping
		private static Hashtable _htArrFlightToDbFlightColMap = null;
		//DsArrFlightTiming Column Mapping to iTrek DB Flight Table Column
		private static Hashtable _htDepFlightToDbFlightColMap = null;
		//DsDepFlightTiming Column Mapping to iTrek DB Flight Table Column
		private static Hashtable _htDsFlightToDbFlightColMap = null;
		//DsFlight Column Mapping to iTrek DB Flight Table Column
		private static Hashtable _htOrgFlightToDbFlightColMap = null;
		//Original Flight (coming directly from backend) Column Mapping to iTrek DB Flight Table Column

		private static Hashtable _htArrFlightToDsFlightColMap = null;
		//DsArrFlightTiming Column Mapping to DsFlight Column.
		private static Hashtable _htDepFlightToDsFlightColMap = null;
		//DsDepFlightTiming Column Mapping to DsFlight Column.

		private static bool _preparedFlightMapping = false;
		private static readonly object _lockFlightMappingObj = new object();

		private void PrepareFlightMapping()
		{
			if (!_preparedFlightMapping)
			{
				lock (_lockFlightMappingObj)
				{
					if (!_preparedFlightMapping)
					{
						_htArrFlightToDbFlightColMap = new Hashtable();
						_htDepFlightToDbFlightColMap = new Hashtable();
						_htDsFlightToDbFlightColMap = new Hashtable();
						_htOrgFlightToDbFlightColMap = new Hashtable();

						_htArrFlightToDsFlightColMap = new Hashtable();
						_htDepFlightToDsFlightColMap = new Hashtable();

						string st = File.ReadAllText(ITrekConfig.GetPathForFlightDbFileMapping());
						st = st.Replace("\r", "");//Remove '\r'
						string[] stArr = st.Split(new char[] { '\n' });
						int cnt = stArr.Length;
						for (int i = 0; i < cnt; i++)
						{
							PrepareOneFlightMapping(stArr[i]);
						}
						_preparedFlightMapping = true;
					}

				}
			}
		}

		private void PrepareOneFlightMapping(string st)
		{
			if (st.StartsWith("--")) return;//This is comment line. No need to add in the mapping.
			//DBFlight Col Name | DsArrFlightTiming Col Name | DsDepFlightTiming Col Name | OrgFlight Col Name | DsFlightTiming Col Name
			string[] stArr = st.Split(new char[] { '|' });
			int cnt = stArr.Length;

			string colDsArr = "";
			string colDsDep = "";
			string colDsOrg = "";
			string colDsFl = "";
			string colDb = "";
			colDb = stArr[0].Trim().ToUpper();

			if (cnt > 1)
			{
				colDsArr = stArr[1].Trim().ToUpper();
				AddMapping(_htArrFlightToDbFlightColMap, colDsArr, colDb);
			}

			if (cnt > 2)
			{
				colDsDep = stArr[2].Trim().ToUpper();
				AddMapping(_htDepFlightToDbFlightColMap, colDsDep, colDb);
			}

			if (cnt > 3)
			{
				colDsOrg = stArr[3].Trim().ToUpper();
				AddMapping(_htOrgFlightToDbFlightColMap, colDsOrg, colDb);
			}

			if (cnt > 4)
			{
				colDsFl = stArr[4].Trim().ToUpper();
				AddMapping(_htDsFlightToDbFlightColMap, colDsFl, colDb);
				AddMapping(_htDepFlightToDsFlightColMap, colDsDep, colDsFl);
				AddMapping(_htArrFlightToDsFlightColMap, colDsArr, colDsFl);
			}
		}

		public string GetDBT_FlightColForDsArrFlight(string colDsArrFlight, out bool hasMapping)
		{
			if (!_preparedFlightMapping) PrepareFlightMapping();
			return GetMapping(_htArrFlightToDbFlightColMap, colDsArrFlight, out hasMapping);
		}

		public string GetDBT_FlightColForDsDepFlight(string colDsDepFlight, out bool hasMapping)
		{
			if (!_preparedFlightMapping) PrepareFlightMapping();
			return GetMapping(_htDepFlightToDbFlightColMap, colDsDepFlight, out hasMapping);
		}

		public string GetDBT_FlightColForDsFlight(string colDsFlight, out bool hasMapping)
		{
			if (!_preparedFlightMapping) PrepareFlightMapping();
			return GetMapping(_htDsFlightToDbFlightColMap, colDsFlight, out hasMapping);
		}

		public string GetDBT_FlightColForOrgFlight(string colOrgFlight, out bool hasMapping)
		{
			if (!_preparedFlightMapping) PrepareFlightMapping();
			return GetMapping(_htOrgFlightToDbFlightColMap, colOrgFlight, out hasMapping);
		}

		public string GetDsFlightColForDsArrFlight(string colArrFlight, out bool hasMapping)
		{
			if (!_preparedFlightMapping) PrepareFlightMapping();
			return GetMapping(_htArrFlightToDsFlightColMap, colArrFlight, out hasMapping);
		}

		public string GetDsFlightColForDsDepFlight(string colDepFlight, out bool hasMapping)
		{
			if (!_preparedFlightMapping) PrepareFlightMapping();
			return GetMapping(_htDepFlightToDsFlightColMap, colDepFlight, out hasMapping);
		}

		#endregion

		#region Mapping Helper Methods
		private static Hashtable AddMapping(Hashtable ht, string key, string value)
		{
			if (string.IsNullOrEmpty(key) || string.IsNullOrEmpty(value)) return ht;

			if (ht.ContainsKey(key))
			{
				ht[key] = value;
			}
			else
			{
				ht.Add(key, value);
			}
			return ht;
		}

		private static string GetMapping(Hashtable ht, string key, out bool hasMapping)
		{
			string st = key;
			hasMapping = false;
			if (ht.ContainsKey(key))
			{
				hasMapping = true;
				st = Convert.ToString(ht[key]);
			}
			return st;
		}

		#endregion

	}
}