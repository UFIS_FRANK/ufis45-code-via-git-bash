using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;

using UFIS.ITrekLib.DS;
using iTrekXML;
using UFIS.ITrekLib.Util;
using UFIS.ITrekLib.ENT;
using MM.UtilLib;

namespace UFIS.ITrekLib.DB
{
   /// <summary>
   /// Configuration Information from iTrek. (Not coming from UFIS-Backend)
   /// </summary>
    public class DBConfig
    {
        private static Object _lockObj = new Object();
        private static DBConfig _dbConfig = null;
        private static DateTime _lastWriteDT = new System.DateTime(1, 1, 1);

        private static DSConfig _dsConfig = null;
        private static string _fnConfigXml = null;
        private static DateTime _lastRefreshDT = new DateTime();

        private DBConfig() { }

        public void Init()
        {
            _dsConfig = null;
            _fnConfigXml = null;
            _lastWriteDT = new System.DateTime(1, 1, 1);
            _lastRefreshDT = new DateTime();
        }

        public static DBConfig GetInstance()
        {
            if (_dbConfig == null)
            {
                _dbConfig = new DBConfig();
            }
            return _dbConfig;
        }

        public string GetConfigValueFor(string configFor, out string desc)
        {
            string value = "";
            desc = "";
            //DSConfig ds = new DSConfig();
            DSConfig tempDs = dsConfig;
            DSConfig.CONFIGRow[] rowArr = (DSConfig.CONFIGRow[])tempDs.CONFIG.Select("ID='" + configFor + "'");
            if (rowArr.Length > 0)
            {
                value = rowArr[0].VAL;
                desc = rowArr[0].ID_DESC;
            }

            return value;
        }

        public DSConfig RetrieveConfig()
        {
            DSConfig ds = new DSConfig();
            ds.CONFIG.Merge(dsConfig.CONFIG);
            return ds;
        }

       public DSConfig RetrieveConfig(List<string> arrConfigIds)
       {
          DSConfig ds = new DSConfig();
          DSConfig tempDs = dsConfig;
          foreach (DSConfig.CONFIGRow row in tempDs.CONFIG.Rows)
          {
             string id = row.ID;
             if (string.IsNullOrEmpty(id)) continue;
             id = id.Trim();
             if (arrConfigIds.Contains(id))
             {
                ds.CONFIG.LoadDataRow(row.ItemArray, true);
             }
          }
          return ds;
       }

        public DSConfig RetrieveConfigForModule(string module)
        {
            DSConfig ds = new DSConfig();
            DSConfig tempDs = dsConfig;
            foreach( DSConfig.CONFIGRow row in tempDs.CONFIG.Select("MODULE='" + module + "'"))
            {
                ds.CONFIG.LoadDataRow( row.ItemArray, true ); 
            }
            return ds;
        }

        public void CreateNewConfig(string id, string value, string module, string desc, string userId)
        {
            lock (_lockObj)
            {
                try
                {
                    LockForWriting();
                    LogMsg(string.Format("CreateNewConfig:Trying To Create:Id<{0}>,Val<{1}>,mod<{2}>,des<{3}>,By<{4}>.", 
                       id, value, module, desc, userId));
                    DSConfig ds = new DSConfig();
                    ds.ReadXml(fnConfigXml);
                    foreach (DSConfig.CONFIGRow oldRow in ds.CONFIG.Select("ID='" + id + "'"))
                    {
                        throw new ApplicationException("record Id " + id + " already existed.");
                        //oldRow.Delete();
                    }
                    DSConfig.CONFIGRow row = ds.CONFIG.NewCONFIGRow();
                    row.ID = id.Trim().ToUpper();
                    row.VAL = value.Trim();
                    row.MODULE = module.Trim();
                    row.ID_DESC = desc;
                    ds.CONFIG.AddCONFIGRow(row);
                    ds.CONFIG.AcceptChanges();
                    ds.WriteXml(fnConfigXml);
                    LogMsg(string.Format("CreateNewConfig:Created:Id<{0}>,Val<{1}>,mod<{2}>,des<{3}>,By<{4}>.",
                        id, value, module, desc, userId));
                    Init();
                }
                catch (Exception ex)
                {
                    LogMsg("CreateNewConfig:Err:" + ex.Message);
                }
                finally
                {
                    ReleaseAfterWrite();
                }
            }
        }

        public string UpdateConfig(string id, string value, string userId)
        {
           string errMsg = "";
            lock (_lockObj)
            {
                try
                {
                    LockForWriting();
                    DSConfig ds = new DSConfig();
                    ds.ReadXml(fnConfigXml);
                    LogMsg(string.Format("UpdateConfig:Trying To Update:Id<{0}>,Val<{1}>,By<{2}>.", id, value, userId));
                    foreach (DSConfig.CONFIGRow row in ds.CONFIG.Select("ID='" + id + "'"))
                    {
                        row.VAL = value.Trim();
                    }
                    ds.CONFIG.AcceptChanges();
                    ds.WriteXml(fnConfigXml);
                    LogMsg(string.Format("UpdateConfig:Updated:Id<{0}>,Val<{1}>,By<{2}>.", id, value, userId));
                    Init();
                }
                catch (Exception ex)
                {
                    LogMsg("UpdateConfig:Err:" + ex.Message);
                    errMsg = "Fail to update";
                }
                finally
                {
                    ReleaseAfterWrite();
                }
            }
            return errMsg;
        }

        private string fnConfigXml
        {
            get
            {
                if ((_fnConfigXml == null) || (_fnConfigXml == ""))
                {
                    iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
                    _fnConfigXml = path.GetCONFIGFilePath();
                }

                return _fnConfigXml;
            }
        }

        private DSConfig dsConfig
        {
            get
            {
                if (Math.Abs(UtilTime.DiffInSec(DateTime.Now, _lastRefreshDT)) > 60)
                {
                    LoadDSConfig();
                    _lastRefreshDT = DateTime.Now;
                }
                DSConfig ds = (DSConfig)_dsConfig.Copy();

                return ds;
            }
        }

        private void LoadDSConfig()
        {
            FileInfo fiXml = new FileInfo(fnConfigXml);
            DateTime curDT = fiXml.LastWriteTime;

            if ((_dsConfig == null) || (curDT.CompareTo(_lastWriteDT) != 0))
            {
                lock (_lockObj)
                {
                    try
                    {
                        fiXml = new FileInfo(fnConfigXml);
                        curDT = fiXml.LastWriteTime;//check the time again, in case of lock
                        if ((_dsConfig == null) || (curDT.CompareTo(_lastWriteDT) != 0))
                        {
                            try
                            {
                                DSConfig tempDs = new DSConfig();
                                tempDs.CONFIG.ReadXml(fnConfigXml);
                                if (_dsConfig == null) _dsConfig = new DSConfig();
                                else
                                {
                                    _dsConfig.CONFIG.Clear();
                                }
                                _dsConfig.CONFIG.Merge(tempDs.CONFIG);
                                _lastWriteDT = curDT;
                            }
                            catch (Exception ex)
                            {
                                LogMsg("LoadDSConfig:Err:" + ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogMsg("LoadDSConfig:Err:" + ex.Message);
                    }
                }
            }
        }

        //private void LogMsg(string msg)
        //{
        //    UtilLog.LogToTraceFile("DBConfig", msg);
        //}

       private void LogMsg(string msg)
       {
          //UtilLog.LogToGenericEventLog("WEB-FlightInfo", msg);      
          try
          {
             UtilTraceLog.GetInstance().LogTraceMsg("DBConfig", msg);
          }
          catch (Exception)
          {
             UtilLog.LogToTraceFile("DBConfig", msg);
          }
       }

        //private void TestWriteCONFIG()
        //{
        //    {
        //        //DSConfig ds = new DSConfig();
        //        //DSConfig.CONFIGRow row = ds.CONFIG.NewCONFIGRow();
        //        //row.ID = "BM_MAB";
        //        //row.ID_DESC = "MAB Alert Timing";
        //        //row.VAL = "30";
        //        //row.MODULE = "WEB";
        //        //ds.CONFIG.Rows.Add(row);
        //        //ds.CONFIG.AcceptChanges();
        //        //ds.CONFIG.WriteXml(fnConfigXml);
        //    }
        //}

        private void LockForWriting()
        {
            try
            {
                UtilLock.GetInstance().LockConfig();
            }
            catch (Exception ex)
            {
                LogMsg("LockForWriting:Err:" + ex.Message);
                if (ex.InnerException != null)
                {
                    LogMsg("LockForWriting:err:Inner:" + ex.InnerException.Message);
                }
            }
        }

        private void ReleaseAfterWrite()
        {
            try
            {
                UtilLock.GetInstance().ReleaseConfig();
            }
            catch (Exception ex)
            {
                LogMsg("ReleaseAfterWrite:Err:" + ex.Message);
                if (ex.InnerException != null)
                {
                    LogMsg("ReleaseAfterWrite:err:Inner:" + ex.InnerException.Message);
                }
            }
        }
    }
}
