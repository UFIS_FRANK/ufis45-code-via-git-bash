using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.IO;
using iTrekXML;

using MM.UtilLib;

namespace UFIS.ITrekLib.Util
{
   public class UtilLog
   {
      public static void LogToEventLog(string logName, string logSource, string strMessage)
      {
         try
         {

            string strLogSrc = logSource;
            string strLogName = logName;
            EventLog EventLog1 = new System.Diagnostics.EventLog();
            //if (!(EventLog.SourceExists(strLogSrc)))
            //{
            //    EventLog.CreateEventSource(strLogSrc, strLogName);
            //}
            EventLog1.Source = strLogSrc;
            EventLog1.Log = strLogName;
            EventLog1.WriteEntry(strMessage + "(Time:" + DateTime.Now.ToString() + ":" + DateTime.Now.Millisecond.ToString() + ")");
         }
         catch (Exception ex)
         {
            iTXMLPathscl iTPaths = new iTXMLPathscl();

            string parentDir = iTPaths.GetXMLPath();
            if (parentDir[parentDir.Length - 1].ToString() != @"\")
            {
               parentDir += @"\";
            }
            parentDir += @"Log\";
            DateTime dt = System.DateTime.Now;
            string msg = string.Format("{0:ddHHmmss}: {1} : {2}",
                dt,
                logSource,
                strMessage);
            string fName = parentDir + string.Format(@"Log_{0:MMdd}.txt", dt);
            UtilFileIO.WriteToTextFile(fName, msg);

            iTrekUtils.iTUtils.LogToEventLog(logSource + "," + strMessage);
            iTrekUtils.iTUtils.LogToEventLog("UtilLog Exception " + ex.Message);
         }

         //try
         //{
         //    string strLogSrc = "iTrekGeneric";
         //    string strLogName = "iTrekGenericLogs";
         //    System.Diagnostics.EventLog EventLog1 = new System.Diagnostics.EventLog();
         //    if (!(EventLog.SourceExists(strLogSrc)))
         //    {
         //        EventLog.CreateEventSource(strLogSrc, strLogName);

         //    }
         //    EventLog1.Source = strLogSrc;
         //    EventLog1.Log = strLogName;
         //    EventLog1.WriteEntry(strMessage + "(Time:" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + ":" + DateTime.Now.Millisecond.ToString() + ")");
         //}
         //catch
         //{
         //}
      }

      /// <summary>
      /// Get Log Directory Path
      /// </summary>
      /// <returns></returns>
      public static string LogDir()
      {
         iTXMLPathscl iTPaths = new iTXMLPathscl();
         string logDir = iTPaths.GetLogDirPath().Trim();
         if (logDir[logDir.Length - 1].ToString() != @"\")
         {
            logDir += @"\";
         }
         return logDir;
      }

      public static void LogToTraceFile(string logSource, string strMessage)
      {
         if (Ctrl.CtrlConfig.IsTraceModeOn())
         {
            iTXMLPathscl iTPaths = new iTXMLPathscl();
            string logDir = iTPaths.GetLogDirPath().Trim();
            if (logDir[logDir.Length - 1].ToString() != @"\")
            {
               logDir += @"\";
            }

            DateTime dt = System.DateTime.Now;
            string msg = string.Format("{0:ddHHmmss}: {1} : {2}",
                dt,
                logSource,
                strMessage);
            string fName = logDir + string.Format(@"Trc_{0:MMdd}.txt", dt);
            UtilFileIO.WriteToTextFile(fName, msg);
         }
      }

      public static void LogToGenericEventLog(string logSource, string strMessage)
      {
         try
         {
            LogToEventLog("iTrekGenericLogs", logSource, strMessage);
         }
         catch
         {
         }
      }

      //public static void LogToTraceFile(string logDir, string logSource, string strMessage)
      //{

      //    iTXMLPathscl iTPaths = new iTXMLPathscl();
      //    if ((logDir == null) || (logDir.Trim() == ""))
      //    {
      //        logDir = @"C:\";
      //    }
      //    if (logDir[logDir.Length - 1].ToString() != @"\")
      //    {
      //        logDir += @"\";
      //    }

      //    DateTime dt = System.DateTime.Now;
      //    string msg = string.Format("{0:ddHHmmss}: {1} : {2}",
      //        dt,
      //        logSource,
      //        strMessage);
      //    string fName = logDir + string.Format(@"Trc_{0:MMdd}.txt", dt);
      //    Util.UtilFileIO.WriteToTextFile(fName, msg + " " + dt.Millisecond);

      //}

      /// <summary>
      /// Log the information in the file.
      /// </summary>
      /// <param name="logDir"></param>
      /// <param name="logFileName"></param>
      /// <param name="logSource"></param>
      /// <param name="strMessage"></param>
      public static void LogToTraceFileComplete(string logDir, string logFileName, string logSource, string strMessage)
      {
         if (Ctrl.CtrlConfig.IsTraceModeOn())
         {
            if (logDir == null) logDir = @"C:\TEMP\";
            else
            {
               logDir = logDir.Trim();
               if (logDir == "") logDir = @"C:\TEMP\";
            }

            if (logDir[logDir.Length - 1].ToString() != @"\")
            {
               logDir += @"\";
            }

            DateTime dt = System.DateTime.Now;
            string msg = string.Format("{0:ddHHmmss}: {1} : {2}",
                dt,
                logSource,
                strMessage);
            string fName = logDir + string.Format(@"{1}_{0:MMdd}.txt", dt, logFileName);
            UtilFileIO.WriteToTextFile(fName, msg);
         }
      }

      public static void LogToTraceFileWithDir(string logDir, string logSource, string strMessage)
      {
         iTXMLPathscl iTPaths = new iTXMLPathscl();
         if ((logDir == null) || (logDir.Trim() == ""))
         {
            logDir = @"C:\";
         }
         if (logDir[logDir.Length - 1].ToString() != @"\")
         {
            logDir += @"\";
         }

         DateTime dt = System.DateTime.Now;
         string msg = string.Format("{0:ddHHmmss}: {1} : {2}",
             dt,
             logSource,
             strMessage);
         string fName = logDir + string.Format(@"Trc_{0:MMdd}.txt", dt);
         UtilFileIO.WriteToTextFile(fName, msg + " " + dt.Millisecond);
      }

      /// <summary>
      /// Log to Trace File With Given File Name Prefix
      /// </summary>
      /// <param name="logSource">Log Source</param>
      /// <param name="strMessage">Message to Log</param>
      /// <param name="logFileName">Prefix of the File Name to Log (system will auto add the _MMdd.txt as suffix)</param>
      public static void LogToTraceFileWithFileName(string logSource, string strMessage, string logFileName)
      {
         iTXMLPathscl iTPaths = new iTXMLPathscl();
         string logDir = iTPaths.GetLogDirPath();
         LogToTraceFileComplete(logDir, logFileName, logSource, strMessage);
      }
   }
}