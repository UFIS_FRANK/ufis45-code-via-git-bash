using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using UFIS.ITrekLib.DS;
using iTrekXML;
using UFIS.ITrekLib.Util;

namespace UFIS.ITrekLib.DB
{
    public class DBDls
    {
        private static Object _lockObj = new Object();
        private static DBDls _dbDls = null;
        private static DateTime lastWriteDT = new System.DateTime(1, 1, 1);

        private static DSDls _dsDls = null;
        private static string _fnDlsXml = null;


        private DBDls() { }

        public void Init()
        {
            _dsDls = null;
            _fnDlsXml = null;
            lastWriteDT = new System.DateTime(1, 1, 1);
        }

        public static DBDls GetInstance()
        {
            if (_dbDls == null)
            {
                _dbDls = new DBDls();
            }
            return _dbDls;
        }

        public DSDls RetrieveDlsForAFlight(string flightUrNo, string flightNo)
        {
            DSDls ds = new DSDls();

            foreach (DSDls.DLSTABRow row in dsDls.DLSTAB.Select("UAFT='" + flightUrNo + "'"))
            {
                ds.DLSTAB.LoadDataRow(row.ItemArray, true);
            }

            return ds;
        }

        private string fnDlsXml
        {
            get
            {
                if ((_fnDlsXml == null) || (_fnDlsXml == ""))
                {
                    iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
                    _fnDlsXml = path.GetDLSTabFilePath();
                }

                return _fnDlsXml;
            }
        }

        private DSDls dsDls
        {
            get
            {
                //if (_dsDls == null) { LoadDSDls(); }
                LoadDSDls();
                DSDls ds = (DSDls)_dsDls.Copy();

                return ds;
            }
        }

        private void LoadDSDls()
        {
            //LoadDlsDataToDlsSummXmlFile();
            FileInfo fiXml = new FileInfo(fnDlsXml);
            DateTime curDT = fiXml.LastWriteTime;

            if ((_dsDls == null) || (curDT.CompareTo(lastWriteDT) != 0))
            {
                lock (_lockObj)
                {
                    try
                    {
                        curDT = fiXml.LastWriteTime;//check the time again, in case of lock
                        if ((_dsDls == null) || (curDT.CompareTo(lastWriteDT) != 0))
                        {
                            try
                            {
                                DSDls tempDs = new DSDls();
                                tempDs.DLSTAB.ReadXml(fnDlsXml);
                                if (_dsDls == null) _dsDls = new DSDls();
                                else
                                {
                                    _dsDls.DLSTAB.Clear();
                                }
                                _dsDls.DLSTAB.Merge(tempDs.DLSTAB);
                                lastWriteDT = curDT;
                            }
                            catch (Exception ex)
                            {
                                LogMsg("LoadDSDls : Error : " + ex.Message);
                                TestWriteDLS();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogMsg("LoadDSDls:Err:" + ex.Message);
                    }
                }
            }
        }

        public void UpdateDls(string flightUrno, string dlsMsg)
        {
            lock (_lockObj)
            {
                try
                {
                    DSDls tempDs = new DSDls();
                    try
                    {
                        if (File.Exists(fnDlsXml))
                        {
                            tempDs.DLSTAB.ReadXml(fnDlsXml);
                        }
                        else
                        {
                            LogMsg("UpdateDls:" + fnDlsXml + " is missing.");
                        }
                    }
                    catch (Exception ex)
                    {
                        LogMsg("UpdateDls:Err:" + ex.Message);
                    }
                    DSDls.DLSTABRow[] rowArr = null;
                    try
                    {
                        rowArr = (DSDls.DLSTABRow[])tempDs.DLSTAB.Select("UAFT='" + flightUrno);

                    }
                    catch (Exception)
                    {
                    }

                    if ((rowArr != null) && rowArr.Length > 0)
                    {
                        rowArr[0].DLS = dlsMsg;
                    }
                    else
                    {
                        tempDs.DLSTAB.AddDLSTABRow(flightUrno, dlsMsg);
                    }
                    tempDs.DLSTAB.AcceptChanges();
                    tempDs.DLSTAB.WriteXml(fnDlsXml);
                }
                catch (Exception ex)
                {
                    LogMsg("UpdateDls:Err:" + ex.Message);
                }
            }
        }

        private void LogMsg(string msg)
        {
            //UtilLog.LogToGenericEventLog("DBDls", msg);
           try
           {
              MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("DBDls", msg);
           }
           catch (Exception)
           {
              UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DBDls", msg);
           }
        }

        private void TestWriteDLS()
        {
            //{
            //    DSDls ds = new DSDls();
            //    DSDls.DLSTABRow row = ds.DLSTAB.NewDLSTABRow();
            //    row.URNO = "DLSUrno123";
            //    row.UAFT = "UAFT1324";
            //    row.DLS = "Msg123 afd";
            //    ds.DLSTAB.Rows.Add(row);
            //    ds.DLSTAB.AcceptChanges();
            //    ds.DLSTAB.WriteXml(fnDlsXml);
            //}
        }


    }
}
