using System;
using System.Collections.Generic;
using System.Text;

using System.Collections;
using System.Data;

using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.DB;
using MMSoftware.MMWebUtil;
using MM.UtilLib;

namespace UFIS.ITrekLib.Ctrl
{
    public class CtrlTerminal
    {
        private const string ALL_TERMINAL_TEXT = "All Terminals";
        private const string ALL_TERMINAL_ID = "*";

        public static DSTerminal RetrieveTerminal()
        {
            DSTerminal ds = new DSTerminal();
            //DS.DSTerminalTableAdapters.TERMINALTableAdapter da = new UFIS.ITrekLib.DS.DSTerminalTableAdapters.TERMINALTableAdapter();
            //da.Fill(ds.TERMINAL);
            ds = DBTerminal.GetInstance().RetrieveTerminals();
            return ds;
        }

        public static string GetAllTerminalId()
        {
            return ALL_TERMINAL_ID;
        }

        public static string GetDefaultTerminalId()
        {
            return ALL_TERMINAL_ID;
        }

        public static string GetTerminalName(string terminalId)
        {
            string terminalName = "";
            if (terminalId == ALL_TERMINAL_ID)
            {
                terminalName = ALL_TERMINAL_TEXT;
            }
            else
            {
                try
                {
                    DSTerminal ds = RetrieveTerminal();
                    terminalName = ds.TERMINAL.FindByTERM_ID(terminalId).TERM_NAME.ToString();
                }
                catch (Exception)
                {
                }
            }
            return terminalName;
        }

        public static void PopulateWebDDTerminal(System.Web.UI.WebControls.DropDownList ddTerminal, string selectedValue)
        {
            DSTerminal ds = CtrlTerminal.RetrieveTerminal();
            DSTerminal.TERMINALRow row = ds.TERMINAL.NewTERMINALRow();
            row.TERM_ID = ALL_TERMINAL_ID;
            row.TERM_NAME = ALL_TERMINAL_TEXT;
            ds.TERMINAL.AddTERMINALRow(row);

            if (selectedValue == null) { selectedValue = ""; }
            if (selectedValue == "") { selectedValue = CtrlTerminal.GetDefaultTerminalId(); }
            WebUtil.PopulateDropDownList(ddTerminal, ds.TERMINAL.DefaultView, "TERM_NAME", "TERM_ID", selectedValue );
        }

        public static void PopulateWebListboxTerminal(System.Web.UI.WebControls.ListBox lbTerminal, string selectedValue)
        {
            DSTerminal ds = CtrlTerminal.RetrieveTerminal();
            DSTerminal.TERMINALRow row = ds.TERMINAL.NewTERMINALRow();
            row.TERM_ID = ALL_TERMINAL_ID;
            row.TERM_NAME = ALL_TERMINAL_TEXT;
            ds.TERMINAL.AddTERMINALRow(row);

            if (selectedValue == null) { selectedValue = ""; }
            if (selectedValue == "") { selectedValue = CtrlTerminal.GetDefaultTerminalId(); }
            //WebUtil.PopulateDropDownList(lbTerminal, ds.TERMINAL.DefaultView, "TERM_NAME", "TERM_ID", selectedValue);
            lbTerminal.DataSource = ds.TERMINAL.DefaultView;
            lbTerminal.DataMember = "TERMINAL";
            lbTerminal.DataValueField = "TERM_ID";
            lbTerminal.DataTextField = "TERM_NAME";
            
            lbTerminal.DataBind();
        }

        public static void CreateBelts(string stTerminalId, string stSectId, string stBelts)
        {
            CheckDataForBelts(ref stTerminalId, ref stSectId, ref stBelts);

            if (IsExistTermSect(stTerminalId, stSectId))
            {
                throw new ApplicationException("Records already existed.");
            }
            DBBelt.GetInstance().Create(stTerminalId, stSectId, stBelts);
        }

        public static void CreateBelts(string stTerminalId, string stSectId, string stFrBelt, string stToBelt)
        {
            int frBelt = -1;
            int toBelt = -1;
            string errMsg = "";

            try
            {
                CheckDataForBelts(ref stTerminalId, ref stSectId, ref stFrBelt, ref stToBelt, out frBelt, out toBelt);
                if (IsExistTermSect(stTerminalId, stSectId))
                {
                    errMsg += "Records already existed.";
                }
                
                DBBelt.GetInstance().Create(stTerminalId, stSectId, frBelt, toBelt);
            }
            catch (ApplicationException ex)
            {
                errMsg += ex.Message;
            }
            catch (Exception)
            {
                errMsg += "Error in saving the information.";
            }

            if (errMsg != "") throw new ApplicationException(errMsg);
        }

        private static bool IsOverlapBelt(DSBelt.BLTRow row, int frBelt, int toBelt)
        {
            bool overlap = false;

            int eFrBelt = row.FR;
            int eToBelt = row.TO;
            if ((frBelt <= eFrBelt) && (eFrBelt <= toBelt)) overlap = true;
            else if ((frBelt <= eToBelt) && (eToBelt <= toBelt)) overlap = true;
            else if ((eFrBelt <= frBelt) && (frBelt <= eToBelt)) overlap = true;
            else if ((eFrBelt<=toBelt) && (toBelt<=eToBelt)) overlap = true;

            return overlap;
        }

        public static void DeleteBelts(string stTerminalId, string stSectId)
        {
            DBBelt.GetInstance().Delete(stTerminalId, stSectId);
        }

        public static void UpdateBelts(string stTerminalId, string stSectId, string belts)
        {
            CheckDataForBelts(ref stTerminalId, ref stSectId, ref belts);

            if (!IsExistTermSect(stTerminalId, stSectId))
            {
                throw new ApplicationException("Records not existed.");
            }
            DBBelt.GetInstance().Update(stTerminalId, stSectId, belts);
        }

        public static void UpdateBelts(string stTerminalId, string stSectId, string stFrBelt, string stToBelt)
        {
            int frBelt = -1;
            int toBelt = -1;
            string errMsg = "";

            try
            {
                CheckDataForBelts(ref stTerminalId, ref stSectId, ref stFrBelt, ref stToBelt, out frBelt, out toBelt);
                if (!IsExistTermSect(stTerminalId, stSectId))
                {
                    errMsg += "Records not existed.";
                }

                DBBelt.GetInstance().Update(stTerminalId, stSectId, frBelt, toBelt);
            }
            catch (ApplicationException ex)
            {
                errMsg += ex.Message;
            }
            catch (Exception)
            {
                errMsg += "Error in saving the information.";
            }

            if (errMsg != "") throw new ApplicationException(errMsg);
        }

        private static void CheckDataForBelts(ref string stTerminalId, ref string stSectId, ref string stBelts)
        {
            if (string.IsNullOrEmpty(stTerminalId)) throw new ApplicationException("No Terminal");
            if (string.IsNullOrEmpty(stBelts)) throw new ApplicationException("No Baggage Type.");
            if (string.IsNullOrEmpty(stSectId)) throw new ApplicationException("No Section");
            stTerminalId = stTerminalId.Trim().ToUpper();
            stBelts = stBelts.Trim().ToUpper();
            if (stTerminalId == "") throw new ApplicationException("No Terminal.");
            if (stBelts == "") throw new ApplicationException("No Belts.");
        }

        private static void CheckDataForBelts(ref string stTerminalId, ref string stSectId, 
            ref string stFrBelt, ref string stToBelt, out int frBelt, out int toBelt)
        {
            string errMsg = "";
            if (string.IsNullOrEmpty(stTerminalId)) throw new ApplicationException("No Terminal");
            if (string.IsNullOrEmpty(stFrBelt)) throw new ApplicationException("No From Belt.");
            if (string.IsNullOrEmpty(stToBelt)) throw new ApplicationException("No To Belt.");
            if (string.IsNullOrEmpty(stSectId)) throw new ApplicationException("No Section");
            stTerminalId = stTerminalId.Trim().ToUpper();
            stSectId = stSectId.Trim().ToUpper();
            if (stTerminalId == "") throw new ApplicationException("No Terminal.");
            if (stSectId == "") throw new ApplicationException("No Section.");

            if (!(int.TryParse(stFrBelt, out frBelt))) errMsg += "Invalid From Belt.";
            else if (frBelt < 1) errMsg += "Invalid From Belt No.";

            if (!(int.TryParse(stToBelt, out toBelt))) errMsg += "Invalid To Belt.";
            else if (toBelt < 1) errMsg += "Invalid To Belt No.";

            if (frBelt > toBelt) errMsg += "'From Belt Number' should not be larger than 'To Belt Number'";

            //Check Overlap Belt
            DSBelt dsBelt = RetrieveAllBelts();
            foreach (DSBelt.BLTRow row in dsBelt.BLT.Select())
            {
                if ((row.TMNL != stTerminalId) || (row.SECT != stSectId))
                {//No need to check the overlap with existing same Terminal and Section
                    if (IsOverlapBelt(row, frBelt, toBelt))
                    {
                        errMsg += "Belt Number Overlap with Terminal " + row.TMNL_NAME + ", Section " + row.SECT;
                        break;
                    }
                }
            } 

            if (errMsg != "") throw new ApplicationException(errMsg);
        }

        public static bool IsExistTermSect(string stTerminalId, string stSectId)
        {
            return DBBelt.GetInstance().IsExist(stTerminalId, stSectId);
        }

        public static DSBelt RetrieveAllBelts()
        {
            DSBelt ds = new DSBelt();
            ds =  DBBelt.GetInstance().RetrieveAll();
            DBTerminal.GetInstance().RetrieveTerminals(ds.TMN);
            return ds;
        }

        public static DSBelt GetBeltsForTerminal(string terminalId)
        {
            DSBelt ds = DBBelt.GetInstance().RetrieveBelts(terminalId);
            string stBeltInfo = "";
            if ((ds != null) && (ds.BLT != null) && (ds.BLT.Rows.Count > 0))
            {
                foreach (DSBelt.BLTRow row in ds.BLT.Select())
                {
                    try
                    {
                        row.BLTL = GetBeltList(row.BLTS, ref stBeltInfo).Trim();
//                        if (row.BLTL[0] == ',') row.BLTL=row.BLTL.Substring(1).Trim();
                        row.BINF = stBeltInfo.Trim() ;
                        if (row.BINF[0] == ',') row.BINF = row.BINF.Substring(1).Trim();
                    }
                    catch (Exception)
                    {
                    }
                }
            }
            return ds;
        }

        public static string GetBeltList(string stBelt, ref string stBeltInfo)
        {
            stBeltInfo = "";
            ArrayList arr = new ArrayList();
            stBeltInfo = GetBeltsInfo(ref arr, stBelt);

            return UtilMisc.ConvArrListToString(arr, "," ) ;
        }

        public static ArrayList GetBelts(string terminalId, ref string stBeltsInfo)
        {
            ArrayList arr = new ArrayList();
            stBeltsInfo = "";
            DSBelt ds = DBBelt.GetInstance().RetrieveBelts(terminalId);
            if ((ds != null) && (ds.BLT !=null) && (ds.BLT.Rows.Count>0))
            {
                foreach (DSBelt.BLTRow row in ds.BLT.Select())
                {
                    stBeltsInfo += GetBeltsInfo(ref arr, row.BLTS);
                }
            }

            if (stBeltsInfo[0] == ',')
            {
                stBeltsInfo = stBeltsInfo.Substring(1).Trim();
            }
            return arr;
        }

        private static string GetBeltsInfo( ref ArrayList arr, string belts)
        {
            string stBeltsInfo = "";
            if ((belts == null) || (belts.Trim() == "")) { }
            else
            {
                char[] del = { ',' };
                string[] arrComma = belts.Split(del);
                int cnt = arrComma.Length;
                for (int i = 0; i < cnt; i++)
                {
                    GetBeltsArrayByRange(ref arr, arrComma[i], ref stBeltsInfo);
                }
            }
            return stBeltsInfo;
        }

        private static void GetBeltsArrayByRange(ref ArrayList arr, string belts, ref string stBeltsInfo)
        {
            if ((belts == null) || (belts.Trim() == "")) return;
            if (belts.Contains(",")) throw new ApplicationException("Invalid Belt format.");
            char[] del = { '-' };
            string[] arrByRange = belts.Split(del);
            int cnt = arrByRange.Length;
            switch (cnt)
            {
                case 1: 
                    arr.Add(arrByRange[0].Trim());
                    stBeltsInfo += ", Belt " + arrByRange[0].Trim();
                    break;
                case 2:
                    int fr = Convert.ToInt32(arrByRange[0].Trim());
                    int to = Convert.ToInt32(arrByRange[1].Trim());
                    if (fr > to) throw new ApplicationException("Invalid Belt Range [" + fr + "-" + to + "]");
                    stBeltsInfo += ", Belt " + fr.ToString() + " - Belt " + to;
                    for (int i = fr; i <= to; i++)
                    {
                        arr.Add(i.ToString());
                    }
                    break;
                default:
                    throw new ApplicationException("Invalid Belt format");
                    //break;
            }
        }

        public const string BELT_INTER = "Inter";//Baggage Arrival - Interline
        public const string BELT_LOCAL = "Local";//Baggage Arrival - Local

        /// <summary>
        /// Get Belt List for given Terminal
        /// Append with 'INTER' and 'LOCAL'
        /// </summary>
        /// <param name="terminalId">Terminal Id</param>
        /// <returns>DataRow Array with Column 'ID' and 'VAL'</returns>
        public static DataRow[] WebGetBeltForTerminal(string terminalId)
        {
            DataTable dt = new DataTable("Belt");
            string st = "";
            Type typeSt = st.GetType();
            dt.Columns.Add("ID", typeSt);
            dt.Columns.Add("VAL", typeSt);

            DSBelt ds = CtrlTerminal.GetBeltsForTerminal(terminalId);
            foreach (DSBelt.BLTRow row in ds.BLT.Select("TMNL='" + terminalId + "'"))
            {
                dt.Rows.Add(row.BLTL, row.BINF);
            }
            dt.Rows.Add(BELT_INTER, BELT_INTER);
            dt.Rows.Add(BELT_LOCAL, BELT_LOCAL);
            dt.AcceptChanges();

            return dt.Select();
        }

        /// <summary>
        /// Get All Belts' Id in comma seperated string for giving terminal Id
        /// </summary>
        /// <param name="terminalId">Terminal Id</param>
        /// <returns>Comma Seperated Belts' Id String</returns>
        public static string GetAllBeltsForTerminal(string terminalId)
        {
            StringBuilder sb = new StringBuilder();
            DSBelt ds = CtrlTerminal.GetBeltsForTerminal(terminalId);
            string seperator = "";
            foreach (DSBelt.BLTRow row in ds.BLT.Select("TMNL='" + terminalId + "'"))
            {
                sb.Append(seperator + row.BLTL);
                seperator = ",";
            }
            return sb.ToString();
        }
    }
}
