using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Data;

using iTrekXML;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.Util;
using System.Collections;
using UFIS.ITrekLib.CustomException;
using UFIS.ITrekLib.Misc;
using UFIS.ITrekLib.Config;
using MM.UtilLib;
using UFIS.ITrekLib.Ctrl;

namespace UFIS.ITrekLib.DB
{

	public class DBDJob
	{
		private static DSJob _dsJob = null;

		private static DSJob _dsPfc = null;
		private static DBDJob _dbJob = null;

		private readonly static DateTime _defaultDT = new DateTime(1, 1, 1);
		private static DateTime _lastJobWriteTime = _defaultDT;
		private static DateTime _lastPfcWriteTime = _defaultDT;
		private static DateTime _lastJobSummWriteTime = _defaultDT;
		private static DateTime _lastJobSummHistWriteTime = _defaultDT;

		private static string _fnPfcXmlPath = null;


		private static object _lockObj = new object();//to use in synchronisation for single access
		private static object _lockJobObj = new object();
		private static object _lockJobHistObj = new object();
		private static object _lockPfcObj = new object();

		private static DateTime _lastJobRefreshDT = _defaultDT;
		private static DateTime _lastJobHistRefreshDT = _defaultDT;
		private static DateTime _lastPfcRefreshDT = _defaultDT;


		private DBDJob() { }

		public void Init()
		{
			//_dsJob = null;

			_fnPfcXmlPath = null;

			_lastJobWriteTime = _defaultDT;
			_lastPfcWriteTime = _defaultDT;
			_lastJobSummWriteTime = _defaultDT;
			_lastJobSummHistWriteTime = _defaultDT;
			_lastJobRefreshDT = _defaultDT;
			_lastJobHistRefreshDT = _defaultDT;
			_lastPfcRefreshDT = _defaultDT;
		}


		private string fnPfcXmlPath
		{
			get
			{
				if ((_fnPfcXmlPath == null) || (_fnPfcXmlPath == ""))
				{
					iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
					_fnPfcXmlPath = path.GetPFCsFilePath();
				}

				return _fnPfcXmlPath;
			}
		}

		public DSJob dsJob
		{
			get
			{
				int timeElapse = Math.Abs(UtilTime.DiffInSec(DateTime.Now, _lastJobRefreshDT));
				if ((_dsJob == null) || (timeElapse > 3))
				{
					//LogTraceMsg("TimeElapse:" + timeElapse);
					bool loadingSuccess = false;
					LoadDSJob(out loadingSuccess, false);
					_lastJobRefreshDT = DateTime.Now;
					//LogTraceMsg("Refresh" + _lastJobRefreshDT.ToLongTimeString());
				}

				return (DSJob)_dsJob.Copy();
			}
		}
		private DSJob dsPfc
		{
			get
			{
				int timeElapse = Math.Abs(UtilTime.DiffInSec(DateTime.Now, _lastPfcRefreshDT));
				if (timeElapse > 6000)
				{
					LogTraceMsg("PFC-TimeElapse:" + timeElapse);
					LoadDsJob_PfcTable();
					_lastPfcRefreshDT = DateTime.Now;
				}

				return _dsPfc;
			}
		}





		private void LogMsg(string msg)
		{
			//UtilLog.LogToGenericEventLog("DBJob", msg
			UtilLog.LogToTraceFile("DBJob", msg);
		}



		private static void LogTraceMsg(string msg)
		{
			try
			{
				MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("DBDJob", msg);

			}
			catch (Exception)
			{
				UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DBDJob", msg);
			}
		}
		/// <summary>
		/// Insert Job into DB
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="ds"></param>
		/// <param name="curDt"></param>
		/// <param name="userId"></param>
		/// <param name="flid"></param>
		/// <returns></returns>
		public bool InsertAJob(IDbCommand cmd, DataSet ds, DateTime curDt, string userId, out string flid)
		{

			bool isOk = false;
			//System.Threading.Thread.Sleep(100);
			flid = "";
			try
			{

				DataRow dataRow = ds.Tables["JOB"].Rows[0];
				string bref = ((string)dataRow["URNO"]).Trim();
				string jobUrno = "";
				string turnJobUrno = "";
				bool has = false;

				Hashtable htJobDetails = new Hashtable();
				has = HasITrekDbRecordForUrno(cmd, bref, out htJobDetails);
				//LogTraceMsg("has" + has);
				if (!has)
				{
					string paramPrefix = UtilDb.GetParaNamePrefix();
					string uaft = "";
					string peno = "";
					string fcco = "";
					string empType = "";

					UtilDb db = UtilDb.GetInstance();
					DateTime dt1 = DateTime.Now;
					string fieldValue = "";
					string urno = UtilDb.GenUrid(cmd, ProcIds.JOB, false, dt1);
					//LogTraceMsg("urno" + urno);
					string pUrno = paramPrefix + "URNO";
					string pBref = paramPrefix + "BREF";
					string pPeno = paramPrefix + "PENO";
					string pEtype = paramPrefix + "ETYP";
					string pUaft = paramPrefix + "UAFT";
					string pFlid = paramPrefix + "FLID";
					string pFcco = paramPrefix + "FCCO";
					string pCdat = paramPrefix + "CDAT";
					string pUsec = paramPrefix + "USEC";
					bool hasData = false;
					string adid = "";

					foreach (DataColumn dc in ds.Tables["JOB"].Columns)
					{
						fieldValue = dataRow[dc].ToString();
						switch (dc.ColumnName)
						{

							case "UAFT":
								uaft = fieldValue;
								break;
							case "PENO":
                                peno = GetUstfForPeno(cmd,fieldValue) ;/*Get the urno from itk_staff*/
								break;
							case "FCCO":
								fcco = fieldValue;
								break;
							case "EMPTYPE":
								empType = fieldValue;
								break;
							default:
								break;


						}
					}
					/*To be added later to replace FLID*/
					try
					{
						flid = CtrlFlight.GetFlightIdForUaft(cmd, uaft, UtilTime.ConvDateTimeToUfisFullDTString(curDt), out hasData, out adid);
					}
					catch (Exception)
					{

						throw;
					}
					//LogTraceMsg("flid" + flid);
					if (flid == "")
					{
						throw new ApplicationException("Cannot find Flight");
					}

					cmd.CommandText = string.Format("INSERT INTO " + GetDb_Tb_Job() + " (URNO,PENO,UAFT,FCCO,EMPTYPE,BREF,FLID,CDAT,USEC,LSTU,STAT) VALUES ({0},{1},{2},{3},{4},{5},{6},{7},{8},{9},'A')", pUrno, pPeno, pUaft, pFcco, pEtype, pBref, pFlid, pCdat, pUsec, pCdat);

					cmd.CommandType = CommandType.Text;
					cmd.Parameters.Clear();
					db.AddParam(cmd, pUrno, urno);
					db.AddParam(cmd, pBref, bref);
					db.AddParam(cmd, pPeno, peno);
					db.AddParam(cmd, pUaft, uaft);
					db.AddParam(cmd, pFlid, flid);
					db.AddParam(cmd, pFcco, fcco);
					db.AddParam(cmd, pEtype, empType);
					db.AddParam(cmd, pCdat, curDt);
					db.AddParam(cmd, pUsec, userId);
					int updCnt = cmd.ExecuteNonQuery();
					//LogTraceMsg("updCnt" + updCnt);
					if (updCnt < 1) throw new ApplicationException("Update Fail!!!");
                    LogTraceMsg("Inserted Job Urno:" + urno+",Flight ID:"+flid+",peno:"+peno);
					isOk = true;
				}


			}
			catch (Exception ex)
			{
				LogTraceMsg("InsertAJob:err:" + ex.Message);
			}
			finally
			{

			}
			return isOk;

		}
		/// <summary>
		/// Update the status as 'X'
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="bref"></param>
		/// <param name="updDateTime"></param>
		/// <param name="userId"></param>
		/// <returns></returns>
		public bool DeleteAJob(IDbCommand cmd, string bref, DateTime updDateTime, string userId)
		{
			string jobUrno = "";
			bool isOk = false;
			bool has = false;
			string turnJobUrno = "";
			try
			{
				has = HasITrekDbRecordForUrno(cmd, bref, out jobUrno, out turnJobUrno);
				//LogTraceMsg("DeleteAJob has" + has);
				if (has)
				{
					cmd.Parameters.Clear();
					StringBuilder sb = new StringBuilder();
					UtilDb udb = UtilDb.GetInstance();
					sb.Append(udb.PrepareUpdCmdToUpdDb(cmd, "USEU", userId));
					sb.Append("," + udb.PrepareUpdCmdToUpdDb(cmd, "LSTU", updDateTime));
					sb.Append("," + udb.PrepareUpdCmdToUpdDb(cmd, "STAT", "X"));//Deleted Status
					LogTraceMsg("sb" + sb.ToString());
					cmd.CommandText = string.Format("UPDATE {0} SET {1} WHERE URNO='{2}'",
					   GetDb_Tb_Job(), sb.ToString(), jobUrno);
					cmd.CommandType = CommandType.Text;
					//LogTraceMsg("cmd.CommandText" + cmd.CommandText);
					int updCnt = cmd.ExecuteNonQuery();
					//LogTraceMsg("updCnt" + updCnt);
					if (updCnt < 1) throw new ApplicationException("Fail to delete!!!");
                    LogTraceMsg("Updated deleted status for urno :" + jobUrno);
					isOk = true;
					if (turnJobUrno != "")
					{
						cmd.CommandText = string.Format("UPDATE {0} SET {1} WHERE URNO='{2}'",
						 GetDb_Tb_Job(), sb.ToString(), turnJobUrno);
						cmd.CommandType = CommandType.Text;
						//LogTraceMsg("cmd.CommandText" + cmd.CommandText);
						int turnUpdCnt = cmd.ExecuteNonQuery();
                        if (updCnt > 0)
                        {
                            LogTraceMsg("Updated deleted status for turnJobUrno :" + turnJobUrno);
                        }

					}

				}
			}
			catch (Exception ex)
			{
				LogTraceMsg("DeleteAJob:err:" + ex.Message);
			}

			return isOk;

		}


		private static string GetValue(Hashtable ht, string key)
		{
			string st = "";
			object obj = ht[key];
			if (obj != null)
			{
				st = (string)obj;
			}
			return st;
		}
		/// <summary>
		/// Update Job Information
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="ds"></param>
		/// <param name="updDateTime"></param>
		/// <param name="userId"></param>
		/// <param name="jobUrno"></param>
		/// <param name="turnJobUrno"></param>
		/// <returns></returns>
        public bool UpdateAJob(IDbCommand cmd, DataSet ds, DateTime updDateTime, string userId, out string staffId, out string flid, out string turnFlid, out bool hasChangeForFlight, out string oldFlId)
		{

			bool isOk = false;
			//System.Threading.Thread.Sleep(100);
			string jobUrno = "";
			string turnJobUrno = "";
			staffId = "";
			string empType = "";
			flid = "";
			turnFlid = "";
            oldFlId = "";
			bool hasData = false;
			string adid = "";
			Hashtable htJob = new Hashtable();
            hasChangeForFlight = false;
			try
			{

				bool has = false;
				UtilDb udb = UtilDb.GetInstance();
				DataRow dataRow = null;
				bool hasUaft = false;
				try
				{
					dataRow = ds.Tables["JOB"].Rows[0];
					string bref = ((string)dataRow["URNO"]).Trim();
					has = HasITrekDbRecordForUrno(cmd, bref, out htJob);
				}
				catch (Exception)
				{


				}
				LogTraceMsg("UpdateAJob has" + has);
				if (has)
				{

					foreach (String jobDetails in htJob.Keys)
					{
						if (jobDetails == "JOBID")
						{
							jobUrno = GetValue(htJob, jobDetails);
						}
						if (jobDetails == "PENO")
						{
							staffId = GetValue(htJob, jobDetails);
						}
						if (jobDetails == "EMPTYPE")
						{
							empType = GetValue(htJob, jobDetails);

						}
						if (jobDetails == "FLID")
						{
							flid = GetValue(htJob, jobDetails);
						}
						if (jobDetails == "TJOBID")
						{
							turnJobUrno = GetValue(htJob, jobDetails);
						}
						if (jobDetails == "TFLID")
						{
							turnFlid = GetValue(htJob, jobDetails);
						}

					}



					
					
					

					foreach (DataColumn dc in ds.Tables["JOB"].Columns)
					{
						if (dc.ColumnName == "UAFT")
						{
							hasUaft = true;
                            oldFlId = flid;
							flid = dataRow[dc].ToString();
							LogTraceMsg("UAFT" + flid);
                            LogTraceMsg("oldFlId" + oldFlId);
							try
							{
								flid = CtrlFlight.GetFlightIdForUaft(cmd, flid, UtilTime.ConvDateTimeToUfisFullDTString(updDateTime), out hasData, out adid);
							}
							catch (Exception)
							{

								throw new ApplicationException("Cannot find the flight in ITK_FLIGHT" + flid);
							}
                            if (empType == "AO")
                            {

                                hasChangeForFlight = true;
                            }
							turnFlid = "";

						}
                        if (dc.ColumnName == "PENO")
                        {
                           
                                staffId = GetUstfForPeno(cmd,dataRow[dc].ToString());


                            

                            LogTraceMsg("staffId:" + staffId);

                        }
                    }
                    StringBuilder sb = new StringBuilder();
                    cmd.Parameters.Clear();
                    sb.Append( udb.PrepareUpdCmdToUpdDb(cmd, "USEU", userId));
                    sb.Append("," + udb.PrepareUpdCmdToUpdDb(cmd, "LSTU", updDateTime));

                    foreach (DataColumn dc in ds.Tables["JOB"].Columns)
                    {
                        if (dc.ColumnName != "URNO" && dc.ColumnName != "FINM" && dc.ColumnName != "LANM" && dc.ColumnName != "UTPL")
						{

                           
							if (dc.ColumnName == "PENO")
							{


                                sb.Append("," + udb.PrepareUpdCmdToUpdDb(cmd, "PENO", staffId));

							}
							else
							{
								if (dc.ColumnName == "EMPTYPE")
								{
                                    string oldEmpType = empType;
									empType = dataRow[dc].ToString();
                                    if (oldEmpType == "AO" && oldEmpType!=empType)
                                    {
                                        sb.Append("," + udb.PrepareUpdCmdToUpdDb(cmd, "STAT", "U"));
                                    }
								}
								if (dc.ColumnName == "UAFT")
								{
									LogTraceMsg("flid" + flid);
									sb.Append("," + udb.PrepareUpdCmdToUpdDb(cmd, "FLID", flid));
									string uaft = dataRow[dc].ToString();
                                    LogTraceMsg("UAFT" + uaft);
                                    sb.Append("," + udb.PrepareUpdCmdToUpdDb(cmd, "UAFT", uaft));
								}
								else
								{


									sb.Append("," + udb.PrepareUpdCmdToUpdDb(cmd, dc.ColumnName, dataRow[dc].ToString()));
								}
							}


						}

					}
                   
					LogTraceMsg("SBb4 " + sb.ToString());
                   
					cmd.CommandText = string.Format("UPDATE {0} SET {1} WHERE URNO='{2}'",
				   GetDb_Tb_Job(), sb.ToString(), jobUrno);
					cmd.CommandType = CommandType.Text;
					LogTraceMsg("jobUrno" + jobUrno);
					LogTraceMsg("cmd.CommandText" + cmd.CommandText);
					int updCnt = cmd.ExecuteNonQuery();
					LogTraceMsg("updCnt" + updCnt);
					if (updCnt < 1) throw new ApplicationException("Update Fail!!!");
					isOk = true;
					if (!hasUaft && turnJobUrno != "")
					{
						cmd.CommandText = string.Format("UPDATE {0} SET {1} WHERE URNO='{2}'",
						 GetDb_Tb_Job(), sb.ToString(), turnJobUrno);
						cmd.CommandType = CommandType.Text;
						LogTraceMsg("turnJobUrno" + jobUrno);
						LogTraceMsg("cmd.CommandText" + cmd.CommandText);
						int turnUpdCnt = cmd.ExecuteNonQuery();
						LogTraceMsg("turnUpdCnt" + turnUpdCnt);


					}
					LogTraceMsg("empType" + empType);
					if (empType != "AO")
					{

						staffId = "";
						flid = "";
						if (turnJobUrno == "")
						{
							turnFlid = "";
						}

					}




				}


			}
			catch (Exception ex)
			{
				LogTraceMsg("UpdateAJob:err:" + ex.Message);
			}

			return isOk;

		}
		public bool HasITrekDbRecordForUrno(IDbCommand cmd, string bref, out string jobId, out string turnJobId)
		{
			bool has = false;
			bool turnhas = false;
			DateTime flDt = DateTime.Now;
			cmd.Parameters.Clear();
			string whereClause = " WHERE ";
			UtilDb udb = UtilDb.GetInstance();
			jobId = "";
			turnJobId = "";

			whereClause = string.Format(" WHERE BREF={0} AND (CDAT BETWEEN {1} AND {2} AND STAT!='X')",
			   udb.PrepareParam(cmd, "BREF", bref),
			   udb.PrepareParam(cmd, "FR_DT", flDt.AddDays(-14)),
			   udb.PrepareParam(cmd, "TO_DT", flDt));
			LogTraceMsg("whereClause1" + whereClause);
			cmd.CommandText = string.Format("SELECT COUNT(*) FROM {0} {1}", GetDb_Tb_Job(), whereClause);
			cmd.CommandType = CommandType.Text;
			int cnt = Int32.Parse(cmd.ExecuteScalar().ToString());
			if (cnt == 1) has = true;
			else if (cnt > 1)
			{
				LogTraceMsg("HasITrekDbRecordForUrno:Dup:" + bref);
				throw new CEMultipleRecordsException();
			}

			if (has)
			{
				cmd.CommandText = string.Format("SELECT URNO FROM {0} {1}", GetDb_Tb_Job(), whereClause);
				cmd.CommandType = CommandType.Text;
				LogTraceMsg("cmd.CommandText2" + cmd.CommandText);
				try
				{
					jobId = cmd.ExecuteScalar().ToString();
					has = true;
				}
				catch (Exception)
				{
					throw;
				}
				turnhas = false;
				cmd.Parameters.Clear();
				whereClause = "";
                whereClause = string.Format(" WHERE BREF={0} AND (CDAT BETWEEN {1} AND {2} AND STAT!='X')",
			   udb.PrepareParam(cmd, "BREF", "A" + bref),
			   udb.PrepareParam(cmd, "FR_DT", flDt.AddDays(-14)),
			   udb.PrepareParam(cmd, "TO_DT", flDt));
				cmd.CommandText = string.Format("SELECT COUNT(*) FROM {0} {1}", GetDb_Tb_Job(), whereClause);
				cmd.CommandType = CommandType.Text;
				LogTraceMsg("cmd.CommandText3" + cmd.CommandText);
				int cntTurn = Int32.Parse(cmd.ExecuteScalar().ToString());
				if (cntTurn == 1) turnhas = true;
				else if (cntTurn > 1)
				{
					LogTraceMsg("HasITrekDbRecordForUrno:Dup2:" + bref);
					throw new CEMultipleRecordsException();
				}
				if (turnhas)
				{
					cmd.CommandText = string.Format("SELECT URNO FROM {0} {1}", GetDb_Tb_Job(), whereClause);
					cmd.CommandType = CommandType.Text;
					LogTraceMsg("cmd.CommandText4" + cmd.CommandText);
					try
					{
						turnJobId = cmd.ExecuteScalar().ToString();
						has = true;
					}
					catch (Exception)
					{
						throw;
					}


				}

			}

			return has;
		}
		/// <summary>
		/// Get the Job Urno from ITK_JOB for the bref
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="bref">Backend ref urno</param>
		/// <param name="jobId">Job Urno from DB</param>
		/// <returns></returns>
		public bool HasITrekDbRecordForUrno(IDbCommand cmd, string bref, out Hashtable htJobDetails)
		{
			bool has = false;
			bool turnhas = false;
			DateTime flDt = DateTime.Now;
			cmd.Parameters.Clear();
			string whereClause = " WHERE ";
			UtilDb udb = UtilDb.GetInstance();
			string jobId = "";
			string staffId = "";
			string empType = "";
			string flid = "";
			string turnJobId = "";
			whereClause = string.Format(" WHERE BREF={0} AND (CDAT BETWEEN {1} AND {2} AND STAT!='X')",
			   udb.PrepareParam(cmd, "BREF", bref),
			   udb.PrepareParam(cmd, "FR_DT", flDt.AddDays(-14)),
			   udb.PrepareParam(cmd, "TO_DT", flDt));
			LogTraceMsg("whereClause1" + whereClause);
			cmd.CommandText = string.Format("SELECT COUNT(*) FROM {0} {1}", GetDb_Tb_Job(), whereClause);
			cmd.CommandType = CommandType.Text;
			int cnt = Int32.Parse(cmd.ExecuteScalar().ToString());
			if (cnt == 1) has = true;
			else if (cnt > 1)
			{
				LogTraceMsg("HasITrekDbRecordForUrno:Dup3:" + bref);
				throw new CEMultipleRecordsException();
			}
			htJobDetails = new Hashtable();
			if (has)
			{
                
				cmd.CommandText = string.Format("SELECT URNO,PENO,EMPTYPE,FLID FROM {0} {1}", GetDb_Tb_Job(), whereClause);
				cmd.CommandType = CommandType.Text;
				LogTraceMsg("cmd.CommandText2" + cmd.CommandText);

				using (IDataReader dr = cmd.ExecuteReader())
				{
					while (dr.Read())
					{
						try
						{
							jobId = UtilDb.GetData(dr, "URNO");
							htJobDetails.Add("JOBID", jobId);
							LogTraceMsg("jobId" + jobId);
						}
						catch (Exception)
						{


						}
						try
						{
							staffId = UtilDb.GetData(dr, "PENO");
							htJobDetails.Add("PENO", staffId);
							LogTraceMsg("staffId" + staffId);
						}
						catch (Exception)
						{


						}
						try
						{
							empType = UtilDb.GetData(dr, "EMPTYPE");
							htJobDetails.Add("EMPTYPE", empType);
							LogTraceMsg("empType" + empType);
						}
						catch (Exception)
						{


						}
						try
						{
							flid = UtilDb.GetData(dr, "FLID");
							htJobDetails.Add("FLID", flid);
							LogTraceMsg("flid" + flid);
						}
						catch (Exception)
						{


						}
					}
				}

				//try
				//{
				//    jobId = cmd.ExecuteScalar().ToString();
				//    has = true;
				//}
				//catch (Exception)
				//{
				//    throw;
				//}
				turnhas = false;
				cmd.Parameters.Clear();
				whereClause = "";
				whereClause = string.Format(" WHERE BREF={0} AND (CDAT BETWEEN {1} AND {2} AND STAT!='X')",
			   udb.PrepareParam(cmd, "BREF", "A" + bref),
			   udb.PrepareParam(cmd, "FR_DT", flDt.AddDays(-14)),
			   udb.PrepareParam(cmd, "TO_DT", flDt));
				cmd.CommandText = string.Format("SELECT COUNT(*) FROM {0} {1}", GetDb_Tb_Job(), whereClause);
				cmd.CommandType = CommandType.Text;
				LogTraceMsg("cmd.CommandText3" + cmd.CommandText);
				int cntTurn = Int32.Parse(cmd.ExecuteScalar().ToString());
				if (cntTurn == 1) turnhas = true;
				else if (cnt > 1)
				{
					LogTraceMsg("HasITrekDbRecordForUrno:Dup4:" + bref);
					throw new CEMultipleRecordsException();
				}
				if (turnhas)
				{
                    
					cmd.CommandText = string.Format("SELECT URNO,FLID FROM {0} {1}", GetDb_Tb_Job(), whereClause);
					cmd.CommandType = CommandType.Text;
					LogTraceMsg("cmd.CommandText4" + cmd.CommandText);

					using (IDataReader dr = cmd.ExecuteReader())
					{
						while (dr.Read())
						{
							try
							{
								turnJobId = UtilDb.GetData(dr, "URNO");
								htJobDetails.Add("TJOBID", turnJobId);
								LogTraceMsg("turnJobId" + turnJobId);
							}
							catch (Exception)
							{


							}
							try
							{
								flid = UtilDb.GetData(dr, "FLID");
								htJobDetails.Add("TFLID", flid);
								LogTraceMsg("flid" + flid);
							}
							catch (Exception)
							{


							}
						}
					}
					//try
					//{
					//    turnJobId = cmd.ExecuteScalar().ToString();

					//}
					//catch (Exception)
					//{
					//    throw;
					//}


				}

			}

			return has;
		}
		private void LoadDsJob_PfcTable()
		{
			FileInfo fiXml = new FileInfo(fnPfcXmlPath);
			DateTime curDT = fiXml.LastWriteTime;
			if (!fiXml.Exists)
			{
				//throw new ApplicationException("PFC information missing. Please inform to administrator.");
				return;
			}

			if ((_dsPfc == null) || (curDT.CompareTo(_lastPfcWriteTime) != 0))
			{
				lock (_lockPfcObj)
				{
					try
					{
						curDT = fiXml.LastWriteTime;//check the time again, in case of lock
						if ((_dsPfc == null) || (curDT.CompareTo(_lastPfcWriteTime) != 0))
						{
							try
							{
								DSJob tempDs = new DSJob();
								tempDs.PFC.ReadXml(fnPfcXmlPath);
								if (_dsPfc == null)
								{
									_dsPfc = tempDs;
								}
								else
								{
									if (tempDs.PFC.Rows.Count > 0)
									{
										_dsPfc = tempDs;
									}
								}
								_lastPfcWriteTime = curDT;
							}
							catch (Exception ex)
							{
								LogMsg("LoadDsJob_PfcTable:Err:" + ex.Message);
							}
						}
					}
					catch (Exception ex)
					{
						LogMsg("LoadDsJob_PfcTable:Err:" + ex.Message);
					}
				}
			}
		}
		public static DBDJob GetInstance()
		{
			if (_dbJob == null)
			{
				_dbJob = new DBDJob();
			}
			return _dbJob;
		}


		public DSJob RetrieveStaffsForAFlight(IDbCommand cmd, string flightUrNo)
		{
			DSJob ds = new DSJob();

			DSJob tempDs = new DSJob();
			LoadDataForAFlight(cmd, ref tempDs, flightUrNo);
			try
			{



				foreach (DSJob.JOBRow row in tempDs.JOB.Select("UAFT='" + flightUrNo + "'"))
				{
					ds.JOB.LoadDataRow(row.ItemArray, true);

				}

			}
			catch (Exception ex)
			{
				LogMsg("RetrieveStaffsForAFlight:Err:Flno," + flightUrNo + "," + ex.Message);
			}



			ds.PFC.Merge(dsPfc.PFC);
			return ds;
		}
		/// <summary>
		/// Retrieve Job Details for Alert
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="jobUrno"></param>
		/// <param name="turnJobUrno"></param>
		/// <returns></returns>

		public DSJob RetrieveJobForAlert(IDbCommand cmd, string jobUrno, string turnJobUrno)
		{
			DSJob ds = new DSJob();

			DSJob tempDs = new DSJob();
			LoadDataForAJob(cmd, ref tempDs, jobUrno, turnJobUrno);
			try
			{



				foreach (DSJob.JOBRow row in tempDs.JOB.Select())
				{
					ds.JOB.LoadDataRow(row.ItemArray, true);

				}

			}
			catch (Exception ex)
			{
				LogMsg("RetrieveJobForAlert:Err:jobUrno," + jobUrno + "," + ex.Message);
			}




			return ds;
		}

		public Hashtable RetrieveAOIDsForAFlight(IDbCommand cmd, DateTime curDt)
		{
			Hashtable ht = new Hashtable();
			ht = LoadDataForAoId(cmd, curDt);
			return ht;
		}

		public string GetAOStaffId(IDbCommand cmd, string flightUrNo, out string aoFirstName, out string aoLastName, out string aoPENO)
		{
			string aoId = "";
			try
			{
				DSJob ds = RetrieveStaffsForAFlight(cmd, flightUrNo);
				DSJob.JOBRow row = (DSJob.JOBRow)ds.JOB.Select("AO='Y'")[0];
				aoId = row.URNO;
				aoFirstName = row.FINM;
				aoLastName = row.LANM;
				aoPENO = row.PENO;
			}
			catch (Exception ex)
			{
				LogMsg("Unable to get AO Staff Id for flight " + flightUrNo + " due to " + ex.Message);
				throw new ApplicationException("No AO for the flight");
			}
			return aoId;
		}

		public int GetStaffCount(IDbCommand cmd, string flightUrNo)
		{
			int cnt = -1;
			try
			{
				DSJob ds = RetrieveStaffsForAFlight(cmd, flightUrNo);
				cnt = ds.JOB.Rows.Count;

			}
			catch (Exception ex)
			{
				LogMsg("Unable to get Staff count for flight " + flightUrNo + " due to " + ex.Message);
			}
			return cnt;
		}





		//public string GetFlightUrnoForAJob(string jobUrno)
		//{
		//    string flightUrno = "";
		//    bool found = false;
		//    if (!found)
		//    {
		//        DSJob tempDs = new DSJob();
		//        tempDs.JOB.ReadXml(fnJobSummXmlPath);
		//        foreach (DSJob.JOBRow row in tempDs.JOB.Select("URNO='" + jobUrno + "'"))
		//        {
		//            found = true;
		//            flightUrno = row.UAFT;
		//            break;
		//        }
		//    }

		//    if (!found)
		//    {
		//        DSJob tempDs = (DSJob)dsJobHist.Copy();
		//        foreach (DSJob.JOBRow row in tempDs.JOB.Select("URNO='" + jobUrno + "'"))
		//        {
		//            found = true;
		//            flightUrno = row.UAFT;
		//            break;
		//        }
		//    }
		//    return flightUrno;
		//}




		public DSJob GetAllJobs()
		{
			DSJob ds = null;
			ds = new DSJob();
			ds.Merge(dsJob);
			return ds;
		}
		/// <summary>
		/// Selection Command Text to fill the DSFlight
		/// </summary>
		private const string _cmdSelJob = @"SELECT URNO,UAFT, FCCO,PENO ,EMPTYPE FROM  ";
		private const string _cmdSelJobAo = @"SELECT PENO,FLID,STAT FROM  ";
		private const string _cmdSelJobView = @"SELECT URNO,UAFT, FCCO,PENO ,EMPTYPE,FINM,LANM,UTPL FROM ";
		private const string _cmdSelAo = @"SELECT PENO FROM  ";


		/// <summary>
		/// Load Job Information for 3 days (From back 2days to Today). Return True when successfully loaded.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="ds"></param>
		/// <returns>True-Load Successful.</returns>
		private bool LoadData(IDbCommand cmd, ref DSJob ds)
		{
			bool loadSuccess = false;
			UtilDb db = UtilDb.GetInstance();
			bool isCmdNull = false;
			try
			{
				if (cmd == null)
				{
					isCmdNull = true;
					cmd = db.GetCommand(false);
				}
				DateTime dt1 = DateTime.Now;

				if (ds == null) ds = new DSJob();

				string paramNamePrefix = UtilDb.GetParaNamePrefix();
				string pnFrDate = paramNamePrefix + "FR_FL_DATE";
				string pnToDate = paramNamePrefix + "TO_FL_DATE";

				cmd.CommandText = _cmdSelJob + GetDb_Tb_Job() + " WHERE BFLT BETWEEN " + pnFrDate + " AND " + pnToDate;
				cmd.CommandType = CommandType.Text;
				cmd.Parameters.Clear();
				DateTime now = DateTime.Now.Date;
				db.AddParam(cmd, pnFrDate, now.AddDays(-2));
				db.AddParam(cmd, pnToDate, now.AddDays(+1).AddMilliseconds(1));

				db.FillDataTable(cmd, ds.JOB);

				DateTime dt2 = DateTime.Now;
				// LogMsg("Load Job Data Time:" + UtilTime.ElapsedTime(dt1, dt2));

				if ((ds != null) && (ds.JOB.Rows.Count > 0))
				{
					loadSuccess = true;
				}
				else
				{
					LogMsg("No Job Data.");
				}
			}
			catch (Exception)
			{
				throw;
			}
			finally
			{
				if (isCmdNull)
				{
					db.CloseCommand(cmd, false);
					cmd = null;
				}
			}
			return loadSuccess;
		}
		/// <summary>
		/// Load Data For AOID
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="ds"></param>
		/// <returns></returns>
		private Hashtable LoadDataForAoId(IDbCommand cmd, DateTime refTime)
		{
			Hashtable htAoIds = new Hashtable();
			//bool loadSuccess = false;
			UtilDb db = UtilDb.GetInstance();
			bool isCmdNull = false;
			try
			{
				if (cmd == null)
				{
					isCmdNull = true;
					cmd = db.GetCommand(false);
				}
				DateTime dt1 = DateTime.Now;


				string paramNamePrefix = UtilDb.GetParaNamePrefix();
				string pnFrDate = paramNamePrefix + "FR_FL_DATE";

				cmd.CommandText = _cmdSelJobAo + GetDb_Tb_Job() + " WHERE LSTU >" + pnFrDate + " AND ((EMPTYPE='AO') OR (STAT='U')) ";
				cmd.CommandType = CommandType.Text;
				cmd.Parameters.Clear();
				//LogTraceMsg("LoadDataForAoId:cmd.CommandText " + cmd.CommandText);
				DateTime now = DateTime.Now.Date;
				db.AddParam(cmd, pnFrDate, refTime);
				ArrayList lstDelJob = new ArrayList();
				using (IDataReader dr = cmd.ExecuteReader())
				{
					while (dr.Read())
					{
						string flightId = "";
						string curAoId = "";
						string stat = "";
						try
						{
							flightId = UtilDb.GetData(dr, "FLID");
							if (string.IsNullOrEmpty(flightId)) continue;
							curAoId = UtilDb.GetData(dr, "PENO");
							stat = UtilDb.GetData(dr, "STAT");
							if (stat == "X" || stat=="U")
							{
								lstDelJob.Add(flightId);
							}
							else
								if (!htAoIds.ContainsKey(flightId))
								{
									htAoIds.Add(flightId, curAoId);
									//LogTraceMsg("LoadDataForAoId:flightId" + flightId);
									//LogTraceMsg("LoadDataForAoId:curAoId" + curAoId);
								}
						}
						catch (Exception ex)
						{
							LogTraceMsg("LoadDataForAoId:Err:" + ex.Message);
						}
					}
				}

				foreach (string delFlId in lstDelJob)
				{
					if (!htAoIds.ContainsKey(delFlId))
					{
						string aoId = GetAOIdForDeletedJob(cmd, delFlId);
						htAoIds.Add(delFlId, aoId);
						LogTraceMsg("Deleted LoadDataForAoId:delFlId," + delFlId + ",Aoid:" + aoId);
					}
				}
			}
			catch (Exception)
			{
				throw;
			}
			finally
			{
				if (isCmdNull)
				{
					db.CloseCommand(cmd, false);
					cmd = null;
				}
			}
			return htAoIds;
		}
		/// <summary>
		/// Get AOID for the flight
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="flightId"></param>
		/// <returns></returns>


		private string GetAOIdForDeletedJob(IDbCommand cmd, string flightId)
		{


			UtilDb db = UtilDb.GetInstance();
			bool isCmdNull = false;
			string aoId = "";
			try
			{
				if (cmd == null)
				{
					isCmdNull = true;
					cmd = db.GetCommand(false);
				}
				DateTime dt1 = DateTime.Now;


				string paramNamePrefix = UtilDb.GetParaNamePrefix();
				string pnFlId = paramNamePrefix + "FLID";


				cmd.CommandText = _cmdSelAo + GetDb_Tb_Job() + " WHERE FLID =" + pnFlId + " AND EMPTYPE='AO' AND STAT!='X'";
				cmd.CommandType = CommandType.Text;
				cmd.Parameters.Clear();
				LogTraceMsg("GetAOIdForDeletedJob " + cmd.CommandText);

				db.AddParam(cmd, pnFlId, flightId);
				try
				{
                    Object obj = cmd.ExecuteScalar();
                    if(obj!=null)
                    {
					aoId = obj.ToString();
                    }
                   
				}
				catch (Exception ex)
				{
					LogTraceMsg("aoId exception"+ex.Message);
                   

				}


			}
			catch (Exception ex)
			{
				LogTraceMsg("GetAOIdForDeletedJob exception" + ex.Message);
			}
			finally
			{
				if (isCmdNull)
				{
					db.CloseCommand(cmd, false);
					cmd = null;
				}
			}
			return aoId;


		}
		/// <summary>
		/// Load Job Data for given flightId. It will return True when loading success.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="ds"></param>
		/// <param name="flightId">Flight Id to fetch the data</param>
		/// <returns></returns>
		public bool LoadData(IDbCommand cmd, ref DSJob ds, string jobId)
		{
			bool loadSuccess = false;
			UtilDb db = UtilDb.GetInstance();
			bool isCmdNull = false;
			try
			{
				if (cmd == null)
				{
					isCmdNull = true;
					cmd = db.GetCommand(false);
				}
				DateTime dt1 = DateTime.Now;

				if (ds == null) ds = new DSJob();

				string paramNamePrefix = UtilDb.GetParaNamePrefix();
				string pnJobId = paramNamePrefix + "FLID";

				cmd.CommandText = _cmdSelJob + GetDb_Tb_Job() + " WHERE BREF = " + jobId;
				cmd.CommandType = CommandType.Text;
				//cmd.Parameters.Clear();

				//  db.AddParam(cmd, pnJobId, jobId);

				db.FillDataTable(cmd, ds.JOB);

				if ((ds != null) && (ds.JOB.Rows.Count > 0))
				{
					loadSuccess = true;
				}
				else
				{
					// LogMsg("No Job Data for " + jobId);
				}
			}
			catch (Exception)
			{
				throw;
			}
			finally
			{
				if (isCmdNull)
				{
					db.CloseCommand(cmd, false);
					cmd = null;
				}
			}

			return loadSuccess;
		}
		public bool LoadDataForAFlight(IDbCommand cmd, ref DSJob ds, string flightId)
		{
			bool loadSuccess = false;
			UtilDb db = UtilDb.GetInstance();
			bool isCmdNull = false;
			try
			{
				if (cmd == null)
				{
					isCmdNull = true;
					cmd = db.GetCommand(false);
				}
				DateTime dt1 = DateTime.Now;

				if (ds == null) ds = new DSJob();

				string paramNamePrefix = UtilDb.GetParaNamePrefix();
				string pnFlightId = paramNamePrefix + "FLID";
				//LogTraceMsg("flightId" + flightId);
				cmd.CommandText = _cmdSelJobView + GetDb_Vw_Job() + " WHERE UAFT = " + pnFlightId +" and STAT!='X'";
				cmd.CommandType = CommandType.Text;
				cmd.Parameters.Clear();
				//LogTraceMsg("cmd.CommandText" + cmd.CommandText);
				db.AddParam(cmd, pnFlightId, flightId);

				db.FillDataTable(cmd, ds.JOB);

				if ((ds != null) && (ds.JOB.Rows.Count > 0))
				{
					//LogTraceMsg("loadSuccess" + loadSuccess);
					loadSuccess = true;
				}
				else
				{
					//LogMsg("No Job Data for " + flightId);
				}
			}
			catch (Exception)
			{
				throw;
			}
			finally
			{
				if (isCmdNull)
				{
					db.CloseCommand(cmd, false);
					cmd = null;
				}
			}

			return loadSuccess;
		}

		/// <summary>
		/// Select records from View
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="ds"></param>
		/// <param name="jobId"></param>
		/// <param name="turnJobId"></param>
		/// <returns></returns>
		public bool LoadDataForAJob(IDbCommand cmd, ref DSJob ds, string jobId, string turnJobId)
		{
			bool loadSuccess = false;
			UtilDb db = UtilDb.GetInstance();
			bool isCmdNull = false;
			try
			{
				if (cmd == null)
				{
					isCmdNull = true;
					cmd = db.GetCommand(false);
				}
				DateTime dt1 = DateTime.Now;

				if (ds == null) ds = new DSJob();

				string paramNamePrefix = UtilDb.GetParaNamePrefix();
				string pnJobId = paramNamePrefix + "JURNO";
				string pnTurnJobId = paramNamePrefix + "JTURNO";
				//LogTraceMsg("jobId" + jobId);
				//LogTraceMsg("turnJobId" + turnJobId);
				cmd.CommandText = _cmdSelJobView + GetDb_Vw_Job() + " WHERE URNO = " + pnJobId + " OR URNO =" + pnTurnJobId;
				cmd.CommandType = CommandType.Text;
				cmd.Parameters.Clear();
				//LogTraceMsg("cmd.CommandText" + cmd.CommandText);
				db.AddParam(cmd, pnJobId, jobId);
				db.AddParam(cmd, pnTurnJobId, turnJobId);

				db.FillDataTable(cmd, ds.JOB);

				if ((ds != null) && (ds.JOB.Rows.Count > 0))
				{
					//LogTraceMsg("loadSuccess" + loadSuccess);
					loadSuccess = true;
				}
				else
				{
					//LogMsg("No Job Data for " + flightId);
				}
			}
			catch (Exception)
			{
				throw;
			}
			finally
			{
				if (isCmdNull)
				{
					db.CloseCommand(cmd, false);
					cmd = null;
				}
			}

			return loadSuccess;
		}
		/// <summary>
		/// Add a row of the DsJob Dataset to given DsJob Dataset. 
		/// If is there any row with same id will be deleted from dataset before adding the new row.
		/// </summary>
		/// <param name="ds"></param>
		/// <param name="row"></param>
		/// <returns></returns>
		private bool AddDsJobRow(DSJob ds, DSJob.JOBRow row)
		{
			bool ok = false;
			if (ds == null) throw new ApplicationException("UpdateDsJob:Null Data.");
			string jobId = row.URNO;
			foreach (DSJob.JOBRow tRow in ds.JOB.Select("URNO='" + jobId + "'"))
			{
				tRow.Delete();
			}
			ds.JOB.AddJOBRow(row);
			ok = true;
			return ok;
		}
		private const string DB_JOB_TABLE = "ITK_JOB";


		private static string GetDb_Tb_Job()
		{
			return DB_Info.GetDbFullName(DB_Info.DB_T_JOB);
		}
		private static string GetDb_Vw_Job()
		{
			return DB_Info.GetDbFullName(DB_Info.DB_V_JOB);
		}

		public bool HasChanges(DateTime lastChgDt, out DateTime curChgDt)
		{
			bool changed = true;
			bool isExist = false;
			curChgDt = UtilDb.GetLastUpdateDateTimeOfTable(null, DB_Info.DB_T_JOB, out isExist);

			if (!isExist)
			{
				throw new ApplicationException("Not able to get the Job Last Update Time information.");
			}

			if (curChgDt.CompareTo(lastChgDt) > 0) changed = true;
			else changed = false;

			return changed;
		}

		private static bool _isFetchingJobInfo = false;

		/// <summary>
		/// Load the job information from database to common static variable dsJob.
		/// </summary>
		/// <param name="loadSuccess">True - Loading Success</param>
		/// <param name="returnACopyWhenSuccess">True-To return the copy of dsJobv when success</param>
		/// <returns>Latest job Information</returns>
		public DSJob LoadDSJob(out bool loadSuccess, bool returnACopyWhenSuccess)
		{
			loadSuccess = false;

			//LogMsg("Summarise the flight Infor");
			DateTime curChgDt = _defaultDT;
			if (!_isFetchingJobInfo)
			{
				if ((_dsJob == null) || HasChanges(_lastJobWriteTime, out curChgDt))
				{
					lock (_lockObj)
					{
						_isFetchingJobInfo = true;
						try
						{
							if ((_dsJob == null) || HasChanges(_lastJobWriteTime, out curChgDt))
							{
								try
								{
									DSJob tempDs = new DSJob();
									if (LoadData(null, ref tempDs))
									{
										if ((tempDs != null) && (tempDs.JOB.Rows.Count > 0))
										{
											loadSuccess = true;
											SetLatestInfoToDataset(tempDs, curChgDt);
											//LogTraceMsg("Get new flight info.");
										}
										else
										{
											LogMsg("No Job Data");
										}
									}
								}
								catch (Exception ex)
								{
									LogMsg("LoadDSJob:Err:" + ex.Message);
									throw new ApplicationException("Error Accessing Job Information.");
								}
							}

						}
						catch (Exception ex)
						{
							LogMsg("LoadDSJob:Err:" + ex.Message);
						}
						finally
						{
							_isFetchingJobInfo = false;
						}
					}
				}
				else
				{
					loadSuccess = true;
				}
			}

			if ((loadSuccess) && (returnACopyWhenSuccess))
			{
				return (DSJob)_dsJob.Copy();
			}
			return _dsJob;
		}

		private void SetLatestInfoToDataset(DSJob ds, DateTime curChgDt)
		{
			_dsJob = ds;
			_lastJobWriteTime = curChgDt;
		}
		/// <summary>
		/// Update the Original Job Information (coming from backend) to iTrek Database
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="dsOrgJobXml"></param>
		/// <returns>True-Successfully updated</returns>
		public bool UpdateOrgJobInfo(IDbCommand cmd, DataSet dsOrgJobXml)
		{
			bool success = false;
			try
			{
				string jobUrNo = "";


				if (!dsOrgJobXml.Tables.Contains("JOB")) throw new ApplicationException("Invalid Org Job Data to update.");
				int cnt = dsOrgJobXml.Tables["JOB"].Rows.Count;

				lock (_lockObj)
				{
					LogTraceMsg("Org Job Info Processing.");
					#region Process Job Information
					for (int i = 0; i < cnt; i++)
					{
						jobUrNo = "";
						try
						{
							DataRow row = dsOrgJobXml.Tables["JOB"].Rows[i];
							jobUrNo = (string)row["URNO"];
							DSJob.JOBRow[] jRows = (DSJob.JOBRow[])_dsJob.JOB.Select("URNO='" + jobUrNo + "'");
							DSJob.JOBRow jRow;
							if (jRows.Length < 1)
							{//Add new row
								jRow = _dsJob.JOB.NewJOBRow();
								jRow.URNO = jobUrNo;
							}
							else if (jRows.Length == 1)
							{
								jRow = jRows[0];
							}
							else
							{
								throw new ApplicationException("More than one records with job id (" + jobUrNo + ")");
							}
							jRow.EMPTYPE = (string)row["EMPTYPE"];
							jRow.FINM = (string)row["FINM"];
							jRow.LANM = (string)row["LANM"];
							jRow.PENO = (string)row["PENO"];
							jRow.UAFT = (string)row["UAFT"];
							jRow.URNO = jobUrNo;
							try
							{
								jRow.FCCO = (string)row["FCCO"];
								jRow.UTPL = (string)row["UTPL"];
							}
							catch
							{
							}


							if (jRows.Length < 1)
							{//Add new Row
								_dsJob.JOB.AddJOBRow(jRow);
								_dsJob.AcceptChanges();
							}
						}
						catch (Exception)
						{
							// LogMsg("Job " + jobUrNo + " rec error : " + ex.Message);
						}
					}
					#endregion
					LogTraceMsg("Org Job Info Process Completed.");
				}

			}
			catch (Exception ex)
			{
				LogMsg("Job error : " + ex.Message);
			}
			return success;
		}

		/// <summary>
		/// Has record in iTrek Database for given Uaft with given Job Schedule Date.
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="uaft">Job Id from Back End UFIS System</param>
		/// <param name="stJobStDt">Job Schedule  Date and Time</param>
		/// <param name="jobId">job Id in iTrek Database</param>
		/// <returns>True - has record</returns>
		public bool HasITrekDbRecordForUaft(IDbCommand cmd, string uaft, string stJobStDt, out string jobId)
		{
			bool has = false;
			jobId = "";
			DateTime flDt = DateTime.Now;
			if (stJobStDt != null)
			{
				stJobStDt = stJobStDt.Trim();
				if (stJobStDt != "")
				{
					flDt = UtilTime.ConvUfisTimeStringToDateTime(stJobStDt);
				}
			}

			cmd.Parameters.Clear();
			string whereClause = " WHERE ";
			UtilDb udb = UtilDb.GetInstance();

			whereClause = string.Format(" WHERE UAFT={0} AND ST BETWEEN {1} AND {2}",
			   udb.PrepareParam(cmd, "UAFT", uaft),
			   udb.PrepareParam(cmd, "FR_DT", flDt.AddDays(-14)),
			   udb.PrepareParam(cmd, "TO_DT", flDt.AddDays(+14)));

			cmd.CommandText = string.Format("SELECT URNO FROM {0} {1}", GetDb_Tb_Job(), whereClause);
			cmd.CommandType = CommandType.Text;

			try
			{
				jobId = cmd.ExecuteScalar().ToString();
				has = true;
			}
			catch (Exception)
			{
				cmd.CommandText = string.Format("SELECT COUNT(*) FROM {0} {1}", GetDb_Tb_Job(), whereClause);
				cmd.CommandType = CommandType.Text;

				int cnt = Int32.Parse(cmd.ExecuteScalar().ToString());
				if (cnt == 1) has = true;
				else if (cnt > 1)
				{
					LogTraceMsg("HasITrekDbRecordForUaft:Dup:" + uaft + "," + stJobStDt);
					throw new CEMultipleRecordsException();
				}
			}

			return has;
		}

		public string GetITrekJobIdForUaft(IDbCommand cmd, string uaft, string jobDt, bool hasRecord)
		{
			string jobId = "";
			UtilDb udb = UtilDb.GetInstance();

			string pnUaft = UtilDb.GetFormattedParaName("UAFT");
			string whereClause = "WHERE UAFT=" + pnUaft;
			hasRecord = false;
			try
			{
				cmd.CommandText = string.Format("SELECT URNO FROM {0} {1}", GetDb_Tb_Job(), whereClause);
				cmd.CommandType = CommandType.Text;
				cmd.Parameters.Clear();
				udb.AddParam(cmd, pnUaft, uaft);

				jobId = cmd.ExecuteScalar().ToString();
				hasRecord = true;
			}
			catch (Exception)
			{
				//Check whether the record exist
				try
				{
					cmd.CommandText = string.Format("SELECT COUNT(*) FROM {0} {1}", GetDb_Tb_Job(), whereClause);
					cmd.CommandType = CommandType.Text;
					cmd.Parameters.Clear();
					udb.AddParam(cmd, pnUaft, uaft);

					int cnt = Int32.Parse(cmd.ExecuteScalar().ToString());
					if (cnt < 1) hasRecord = false;
					else hasRecord = true;
				}
				catch (Exception)
				{
					throw;
				}
			}

			return jobId;
		}

		public string GetUaftForITrekJobId(IDbCommand cmd, string jobId)
		{
			string uaft = "";
			try
			{
				uaft = UtilDb.GetDataForAColumn(cmd, DB_Info.DB_T_JOB, jobId, "UAFT").ToString();
			}
			catch (Exception ex)
			{
				LogMsg("GetUaftForITrekJobId:Err:" + ex.Message);
				throw new ApplicationException("Unable to get the Job Id.");
			}
			return uaft;
		}

		public void UploadBatchJobDataToDb(IDbCommand cmd, DataSet dsBatchJob,
			Dictionary<string, string> mapUaftToFlid,
   string userId, DateTime updateDateTime,
   out int cntInsert, out int cntUpdate, out int cntDelete, out int cntWithRecordInDb)
		{
			UtilDb udb = UtilDb.GetInstance();
			List<string> lsBref = new List<string>();
			DateTime now = ITrekTime.GetCurrentDateTime();
			DateTime frDate = now.AddDays(-7);
			DateTime toDate = now.AddDays(+7);
			int cntNewId = 0;
			DateTime dt1 = DateTime.Now;

			if (!dsBatchJob.Tables["JOB"].Columns.Contains("FLID"))
			{
				dsBatchJob.Tables["JOB"].Columns.Add("FLID", typeof(string));
			}

			/*
			 *
			URNO -
			UAFT - UAFT
			PENO - PENO
			FCCO - FCCO
			EMPTYPE - EMPTYPE
			BREF - URNO
			FLID - 
			STAT -
			*/
			#region Get all Uaft and From and To DateTime range
			foreach (DataRow row in dsBatchJob.Tables["JOB"].Rows)
			{
				string bref = (string)row["URNO"];
				if (string.IsNullOrEmpty(bref)) continue;
				lsBref.Add(bref);
			}
			#endregion

			DateTime dt2 = DateTime.Now;

			#region Get the Current Data for all the Uaft from Database
			DataSet dsCur = RetrieveDataFromDbForIRef(cmd, lsBref, frDate, toDate);
			//int cnt1 = dsCur.Tables[0].Rows.Count;
			cntWithRecordInDb = dsCur.Tables[0].Rows.Count;
			#endregion
			DateTime dt3 = DateTime.Now;

			#region Update/Insert the Information in DB Dataset
			foreach (DataRow row in dsBatchJob.Tables["JOB"].Rows)
			{
				string uaft = (string)row["UAFT"];
				if (string.IsNullOrEmpty(uaft)) continue;
				string bref = (string)row["URNO"];
				if (string.IsNullOrEmpty(bref)) continue;
				string flid = "";
				if (mapUaftToFlid.ContainsKey(uaft))
				{
					flid = mapUaftToFlid[uaft];
				}

				row["FLID"] = flid;

				DataRow[] rowCurs = dsCur.Tables[0].Select("BREF='" + bref + "'");
				if (rowCurs == null || rowCurs.Length < 1)
				{//No match. New Data
					string jobId = UtilDb.PFIX_VIRTUAL_NEW_ID + cntNewId;
					cntNewId++;
					AssignNewData(cmd,row, jobId, dsCur, userId, updateDateTime);
				}
				else if (rowCurs.Length == 1)
				{
					string jobId = (string)rowCurs[0]["URNO"];
					AssignExistingData(cmd,row, jobId, rowCurs[0], userId, updateDateTime);
				}
				else
				{//Multiple Record with same bref.
					throw new ApplicationException("Duplicate Data for BREF:" + bref);
				}
			}
			#endregion
			DateTime dt4 = DateTime.Now;
			#region get the date changes (New Record and Updated Records)
			DataTable dtChgIns = dsCur.Tables[0].GetChanges(DataRowState.Added);
			DataTable dtChgUpd = dsCur.Tables[0].GetChanges(DataRowState.Modified);

			int cnt1 = dsCur.Tables[0].Rows.Count;
			//IDbDataAdapter da = GetOrgFlightDataAdapter(cmd);
			cntInsert = 0;
			cntUpdate = 0;
			cntDelete = 0;
			#endregion
			DateTime dt5 = DateTime.Now;
			#region Insert the New Records
			if (dtChgIns != null)
			{
				cntInsert = dtChgIns.Rows.Count;
				//udb.Update(da, dtChgIns);
				InsertDataToDb(cmd, dtChgIns, userId, updateDateTime);
			}
			#endregion
			DateTime dt6 = DateTime.Now;


			#region Update the Changed Records
			if (dtChgUpd != null)
			{
				cntUpdate = dtChgUpd.Rows.Count;
				//udb.Update(da, dtChgUpd);
				UpdateDataToDb(cmd, dtChgUpd, userId, updateDateTime);
			}
			#endregion
			DateTime dt7 = DateTime.Now;
			LogTraceMsg(string.Format(@"UploadBatchJobDataToDb:CntXml={0},Existing={1},Insert={2}, Update={3}, Delete={4},
    PrepareInfo   :{5}, GetCur:{6}, PrepareData:{7},
    GetChangedInfo:{8}, Insert:{9}, Update     :{10},
    Total Time    :{11}",
			   dsBatchJob.Tables[0].Rows.Count,
			   cntWithRecordInDb,
			   cntInsert, cntUpdate, cntDelete,
			   UtilTime.ElapsedTime(dt1, dt2),//PrepareInfo Time
			   UtilTime.ElapsedTime(dt2, dt3),//GetCurrent Info Time
			   UtilTime.ElapsedTime(dt3, dt4),//Prepare Data Time
			   UtilTime.ElapsedTime(dt4, dt5),//Get Changed Info (New/Changed)
			   UtilTime.ElapsedTime(dt5, dt6),//Insert New Info
			   UtilTime.ElapsedTime(dt6, dt7),//Update Changed Info
			   UtilTime.ElapsedTime(dt1, dt7)//Total Time
			   ));
		}

		public DataSet RetrieveDataFromDbForIRef(IDbCommand cmd, List<string> lsBref, DateTime frDate, DateTime toDate)
		{
			cmd.Parameters.Clear();
			DataSet dsNew = new DataSet();
			dsNew.Tables.Add();
			UtilDb udb = UtilDb.GetInstance();

			string stBref = UtilMisc.ConvListToString(lsBref, ",", "'", "'", 0, lsBref.Count);
			if ((!string.IsNullOrEmpty(stBref)) && (stBref != "''"))
			{
				string whereClause = string.Format(" WHERE BREF IN ({0}) AND (CDAT BETWEEN {1} AND {2}) AND STAT!='X'",
				   stBref,
				   udb.PrepareParam(cmd, "FR_DT", frDate),
				   udb.PrepareParam(cmd, "TO_DT", toDate));
				cmd.CommandText = "SELECT URNO,UAFT,PENO,FCCO,EMPTYPE,BREF,FLID,STAT,CDAT,USEC,LSTU,USEU FROM " + GetDb_Tb_Job() +
				   whereClause;

				cmd.CommandType = CommandType.Text;

				udb.FillDataTable(cmd, dsNew.Tables[0]);
			}
			dsNew.AcceptChanges();
			return dsNew;
		}


		private void AssignNewData(IDbCommand cmd,DataRow rowIntf, string jobId, DataSet dsCurDb,
   string userId, DateTime updateDateTime)
		{
			//    URNO -
			//UAFT - UAFT
			//PENO - PENO
			//FCCO - FCCO
			//EMPTYPE - EMPTYPE
			//BREF - URNO
			//FLID - 
			//STAT -
			DataRow row = dsCurDb.Tables[0].NewRow();

			string peno = (string)rowIntf["PENO"];
			string ustf = GetUstfForPeno(cmd,peno);
			if (string.IsNullOrEmpty(ustf)) throw new ApplicationException("No Staff Id for " + peno);

			row["URNO"] = jobId;
			row["UAFT"] = rowIntf["UAFT"];
			row["PENO"] = rowIntf["PENO"];
			row["FCCO"] = rowIntf["FCCO"];
			row["EMPTYPE"] = rowIntf["EMPTYPE"];
			row["BREF"] = rowIntf["URNO"];
			row["FLID"] = rowIntf["FLID"];
			row["STAT"] = "A";//Active status

			row["LSTU"] = updateDateTime;
			row["USEU"] = userId;
			row["CDAT"] = updateDateTime;
			row["USEC"] = userId;

			dsCurDb.Tables[0].LoadDataRow(row.ItemArray, LoadOption.Upsert);
		}

		private static string GetUstfForPeno(IDbCommand cmd,string peno)
		{
            string staffId = "";
           
             staffId = CtrlStaff.RetrieveUrnoForPeno( cmd,peno);/*Get the urno from itk_staff*/           
          
            if (staffId == null || staffId == "")
            {
                staffId = peno;

            }
            
			return staffId;
		}

		/// <summary>
		/// Assign Job Data from backend to iTrek Job Data
		/// </summary>
		/// <param name="rowIntf">job Data row from Backend</param>
		/// <param name="jobId">Job id</param>
		/// <param name="rowDb">job data table row in iTrek</param>
		/// <param name="userId">user id who update this info</param>
		/// <param name="updateDateTime">Update date and time</param>
		private void AssignExistingData(IDbCommand cmd,DataRow rowIntf, string jobId, DataRow rowDb,
		   string userId, DateTime updateDateTime)
		{
			//    URNO -
			//UAFT - UAFT
			//PENO - PENO
			//FCCO - FCCO
			//EMPTYPE - EMPTYPE
			//BREF - URNO
			//FLID - 
			//STAT -		
			string peno = (string)rowIntf["PENO"];
			string ustf = GetUstfForPeno(cmd,peno);
			if (string.IsNullOrEmpty(ustf)) throw new ApplicationException("No Staff Id for " + peno);
			UtilDataSet.AssignData((string)rowIntf["UAFT"], rowDb, "UAFT");
			UtilDataSet.AssignData(ustf, rowDb, "PENO");
			UtilDataSet.AssignData((string)rowIntf["FCCO"], rowDb, "FCCO");
			UtilDataSet.AssignData((string)rowIntf["EMPTYPE"], rowDb, "EMPTYPE");
			UtilDataSet.AssignData((string)rowIntf["URNO"], rowDb, "BREF");
			UtilDataSet.AssignData((string)rowIntf["FLID"], rowDb, "FLID");
			if (rowDb.RowState == DataRowState.Modified)
			{
				rowDb["LSTU"] = updateDateTime;
				rowDb["USEU"] = userId;
			}
		}


		private int UpdateDataToDb(IDbCommand cmd, DataTable dt, string userId, DateTime curDt)
		{
			cmd.Parameters.Clear();
			StringBuilder sb = new StringBuilder();
			UtilDb udb = UtilDb.GetInstance();
			List<string> lsColNames = new List<string>();
			string seperator = "";
			string paramPrefix = UtilDb.GetParaNamePrefix();

			foreach (DataColumn col in dt.Columns)
			{
				if ("URNO,CDAT,USEC".Contains(col.ColumnName)) continue;
				lsColNames.Add(col.ColumnName);
				if (col.DataType == typeof(DateTime))
				{
					sb.Append(seperator + udb.PrepareUpdCmdToUpdDbForTime(cmd, col.ColumnName, ""));
				}
				else
				{
					sb.Append(seperator + udb.PrepareUpdCmdToUpdDb(cmd, col.ColumnName, null));
				}
				seperator = ",";
			}

			string stWhere = "URNO=" + udb.PrepareParam(cmd, "URNO", null);
			lsColNames.Add("URNO");

			string stCmd = string.Format("UPDATE {0} SET {1} WHERE {2}",
               GetDb_Tb_Job(), sb.ToString(), stWhere);
			if (stCmd != cmd.CommandText)
			{
				cmd.CommandText = stCmd;
				cmd.CommandType = CommandType.Text;
				cmd.Prepare();
			}

			int cntCol = lsColNames.Count;
			int totUpdCnt = 0;
			foreach (DataRow row in dt.Rows)
			{
				for (int i = 0; i < cntCol; i++)
				{
					((IDataParameter)cmd.Parameters[paramPrefix + lsColNames[i]]).Value = row[lsColNames[i]];
				}
				int updCnt = cmd.ExecuteNonQuery();
				if (updCnt < 1) throw new ApplicationException("Update Fail!!!");
				totUpdCnt++;
			}
			return totUpdCnt;
		}

		/// <summary>
		/// Insert the Original Flight Info data (Datatable are same type as FLIGHT Table) to the itrek database
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="dt"></param>
		/// <param name="userId"></param>
		/// <param name="curDt"></param>
		/// <returns></returns>
		private int InsertDataToDb(IDbCommand cmd, DataTable dt, string userId, DateTime curDt)
		{
			UtilDb udb = UtilDb.GetInstance();
			int totUpdCnt = 0;
			List<string> stIds = UtilDb.GenUrids(cmd, ProcIds.JOB,
			   false, ITrekTime.GetCurrentDateTime(), dt.Rows.Count);//Generate the Urnos.

			cmd.Parameters.Clear();
			StringBuilder sb = new StringBuilder();
			StringBuilder sbDbColNames = new StringBuilder();

			List<string> lsColNamesToAssignValue = new List<string>();//Name of the column to assign the value
			string seperator = "";
			string paramPrefix = UtilDb.GetParaNamePrefix();

			#region Prepare the parameters with names only (without value)
			foreach (DataColumn col in dt.Columns)
			{
				lsColNamesToAssignValue.Add(col.ColumnName);
				if (col.DataType == typeof(DateTime))
				{
					sb.Append(seperator + udb.PrepareParamForTime(cmd, col.ColumnName, ""));
				}
				else
				{
					sb.Append(seperator + udb.PrepareParam(cmd, col.ColumnName, null));
				}
				sbDbColNames.Append(seperator + col.ColumnName);
				seperator = ",";
			}
			#endregion

			string stCmd = string.Format(@"INSERT INTO {0} ({1}) VALUES ({2})",
               GetDb_Tb_Job(), sbDbColNames.ToString(), sb.ToString());

			if (stCmd != cmd.CommandText)
			{
				cmd.CommandText = stCmd;
				cmd.CommandType = CommandType.Text;
				cmd.Prepare();
			}

			int cntCol = lsColNamesToAssignValue.Count;

			int idxRowIdx = -1;
			foreach (DataRow row in dt.Rows)
			{
				idxRowIdx++;
				try
				{
					#region Assign Value for the parameter and Update to the database
					if (row["URNO"].ToString().StartsWith(UtilDb.PFIX_VIRTUAL_NEW_ID))
					{//It is virtual Urno
						row["URNO"] = stIds[idxRowIdx];//Assign the Actual Urno.
					}

					for (int i = 0; i < cntCol; i++)
					{
						string colName = lsColNamesToAssignValue[i];
						try
						{
							((IDataParameter)cmd.Parameters[paramPrefix + colName]).Value = row[colName];
						}
						catch (Exception ex)
						{
							LogMsg("InsertDataToDb:Err:AssignVal:" + colName + ":" + ex.Message + Environment.NewLine + UtilDataSet.GetDataRowString(row));
							//throw new ApplicationException(ex.Message + Environment.NewLine + UtilDataSet.GetDataRowString(row));
						}
					}
					int updCnt = cmd.ExecuteNonQuery();
					if (updCnt < 1) throw new ApplicationException("Update Fail!!!");
					totUpdCnt++;
					#endregion
				}
				catch (Exception ex)
				{
					string st = ex.Message + Environment.NewLine + UtilDataSet.GetDataRowString(row);
					LogMsg("InsertDataToDb:Err:" + ex.Message);
					throw;//Throw error when one record was not able to update.
				}
			}
			return totUpdCnt;
		}


	}
}