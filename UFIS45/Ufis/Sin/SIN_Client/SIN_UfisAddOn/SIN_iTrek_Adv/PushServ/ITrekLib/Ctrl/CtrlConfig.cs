using System;
using System.Collections.Generic;
using System.Text;

using iTrekWMQ;
using iTrekXML;

using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.DB;
using UFIS.ITrekLib.Util;

namespace UFIS.ITrekLib.Ctrl
{
   public class CtrlConfig
   {
      private const string BM_MAB = "BM_MAB";
      private const string BM_PLB = "BM_PLB";
      private const string BM_AP_FST_BAG = "BM_AP_FST_BAG";
      private const string BM_BG_FST_BAG = "BM_BG_FST_BAG";
      private const string BM_AP_LST_BAG_LGT_LOAD = "BM_AP_LST_BAG_LGT_LOAD";
      private const string BM_BG_LST_BAG_LGT_LOAD = "BM_BG_LST_BAG_LGT_LOAD";
      private const string BM_AP_LST_BAG_HVY_LOAD = "BM_AP_LST_BAG_HVY_LOAD";
      private const string BM_BG_LST_BAG_HVY_LOAD = "BM_BG_LST_BAG_HVY_LOAD";
      private const string WEB_FLINFO_TO_MINUTES = "WEB_FLINFO_TO_MINUTES";
      private const string WEB_FLINFO_FROM_MINUTES = "WEB_FLINFO_FROM_MINUTES";
      private const string WAP_JOBALERT_MINUTES = "WAP_JOBALERT_MINUTES";
      private const string KEEP_DAY = "KEEP_DAY";
      private const string KEEP_DAY_SH = "KEEP_DAY_SH";
      private const string WEB_FLINFO_AO_TO_MINUTES = "WEB_FLINFO_AO_TO_MINUTES";
      private const string WEB_FLINFO_AO_FROM_MINUTES = "WEB_FLINFO_AO_FROM_MINUTES";
      private const string TRACE_MODE = "TRACE_MODE";
      private const string WEB_REFRESH_TIMING = "WEB_REFRESH_TIMING";
      private const string WEB_INBOX_TIME = "WEB_INBOX_TIME";
      private const string WEB_OUTBOX_TIME = "WEB_OUTBOX_TIME";
      private const string WEB_BA_HLTRIP_TIME = "WEB_BA_HLTRIP_TIME";
      private const string WEB_BA_HLUNSENT_TIME = "WEB_BA_HLUNSENT_TIME";
      private const string WEB_BA_HLFBAG_TIME = "WEB_BA_HLFBAG_TIME";
      private const string WEB_BA_HLLBAG_TIME = "WEB_BA_HLLBAG_TIME";
      private const string WEB_BA_FR_MINUTES = "WEB_BA_FR_MINUTES";
      private const string WEB_BA_TO_MINUTES = "WEB_BA_TO_MINUTES";


      public static DSConfig RetrieveConfigForModule(string module)
      {
         return DBConfig.GetInstance().RetrieveConfigForModule(module);
      }

      public static DSConfig RetrieveAllConfig()
      {
         return DBConfig.GetInstance().RetrieveConfig();
      }

      public static DSConfig RetrieveBAConfig()
      {
         List<string> arrConfigIds = new List<string>();
         arrConfigIds.Add(WEB_BA_HLTRIP_TIME);
         //Following information will be getting from Back-end.
         //arrConfigIds.Add(WEB_BA_HLUNSENT_TIME);
         //arrConfigIds.Add(WEB_BA_HLFBAG_TIME);
         //arrConfigIds.Add(WEB_BA_HLLBAG_TIME);
         return DBConfig.GetInstance().RetrieveConfig(arrConfigIds);
      }

      public static string GetValueFor(string configFor, out string desc)
      {
         string val = "";
         val = DBConfig.GetInstance().GetConfigValueFor(configFor, out desc);
         //DSConfig ds = GetConfigFor(configFor);

         //if ((ds != null) && (ds.CONFIG.Rows.Count > 0))
         //{
         //    val = ((DSConfig.CONFIGRow)ds.CONFIG.Rows[0]).VAL;
         //}
         return val;
      }

      private static void CreateConfigFor(string configFor, string value, string module, string desc, ENT.EntUser user)
      {
         DBConfig.GetInstance().CreateNewConfig(configFor, value, module, desc, user.UserId);
      }

      public static void CreateConfigForWeb(string configFor, string value, string desc, ENT.EntUser user)
      {
         CreateConfigFor(configFor, value, "WEB", desc, user);
      }

      public static void UpdateConfigValueFroWeb(string configFor, string value, ENT.EntUser user)
      {
         DBConfig.GetInstance().UpdateConfig(configFor, value, user.UserId);
      }

      public static string GetBmMAB()
      {
         string desc = "";
         return DBConfig.GetInstance().GetConfigValueFor(BM_MAB, out desc);
      }


      public static string GetBmPLB()
      {
         string desc = "";
         return DBConfig.GetInstance().GetConfigValueFor(BM_PLB, out desc);
      }

      public static string GetBmApronFirstBag()
      {
         string desc = "";
         return DBConfig.GetInstance().GetConfigValueFor(BM_AP_FST_BAG, out desc);
      }

      public static string GetBmBaggageFirstBag()
      {
         string desc = "";
         return DBConfig.GetInstance().GetConfigValueFor(BM_BG_FST_BAG, out desc);
      }

      public static string GetBMApronLastBagLightLoad()
      {
         string desc = "";
         return DBConfig.GetInstance().GetConfigValueFor(BM_AP_LST_BAG_LGT_LOAD, out desc);
      }

      public static string GetBMBaggageLastBagLightLoad()
      {
         string desc = "";
         return DBConfig.GetInstance().GetConfigValueFor(BM_BG_LST_BAG_LGT_LOAD, out desc);
      }

      public static string GetBMApronLastBagHeavyLoad()
      {
         string desc = "";
         return DBConfig.GetInstance().GetConfigValueFor(BM_AP_LST_BAG_HVY_LOAD, out desc);
      }

      public static string GetBMBaggageLastBagHeavyLoad()
      {
         string desc = "";
         return DBConfig.GetInstance().GetConfigValueFor(BM_BG_LST_BAG_HVY_LOAD, out desc);
      }

      public static string GetWebFLINFO_TO_MINUTES()
      {
         string desc = "";
         return DBConfig.GetInstance().GetConfigValueFor(WEB_FLINFO_TO_MINUTES, out desc);
      }

      public static string GetWebFLINFO_FROM_MINUTES()
      {
         string desc = "";
         return DBConfig.GetInstance().GetConfigValueFor(WEB_FLINFO_FROM_MINUTES, out desc);
      }

      public static string GetWebFLINFO_AO_TO_MINUTES()
      {
         string desc = "";
         return DBConfig.GetInstance().GetConfigValueFor(WEB_FLINFO_AO_TO_MINUTES, out desc);
      }

      public static string GetWebFLINFO_AO_FROM_MINUTES()
      {
         string desc = "";
         return DBConfig.GetInstance().GetConfigValueFor(WEB_FLINFO_AO_FROM_MINUTES, out desc);
      }

      public static string GetWapJobAssignAlertMinutes()
      {
         string desc = "";
         return DBConfig.GetInstance().GetConfigValueFor(WAP_JOBALERT_MINUTES, out desc);
      }

      #region WEB Baggage Arrival Data
      /// <summary>
      /// Hhighlight the Trip with No acknowledged receipt by Baggage after this Minutes
      /// No acknowledged receipt by Baggage
      /// ----------------------------------
      ///     [Containers and trip labels that failed to be acknowledged received X mins 
      ///     after Apron sent trip time, shall be bolded in red]
      /// </summary>
      /// <returns></returns>
      public static int GetWebBAHighlightTripMINUTES()
      {
         string desc = "";
         //Get value from Config.xml
         return Convert.ToInt32(DBConfig.GetInstance().GetConfigValueFor(WEB_BA_HLTRIP_TIME, out desc));
      }

      /// <summary>
      /// Highlight the UnSend Ulds by Apron after this Minutes
      /// Apron sent trip
      /// ---------------
      ///    The expected list of containers (those with CPM) shall have its font bolded in red 
      /// when Apron fails to send its first trip X mins after thumbs up.
      ///
      /// </summary>
      /// <returns></returns>
      public static int GetWebBAHighlightUnsentUldMINUTES()
      {
         string desc = "";
         //return Convert.ToInt32(DBConfig.GetInstance().GetConfigValueFor(WEB_BA_HLUNSENT_TIME, out desc));
         //Get value from UfisConfig.xml (Getting the timing from Ufis)
         return Convert.ToInt32(DBUfisConfig.GetInstance().GetConfigValueFor(WEB_BA_HLUNSENT_TIME, out desc));
      }

      /// <summary>
      /// Highlight the First Bag after this Minutes
      /// First Bag
      /// ---------
      ///    First Bag shall be highlighted (shaded) in red 
      /// when it fails to be triggered 1 min after acknowledged receive of first container.
      /// </summary>
      /// <returns></returns>
      public static int GetWebBAHighlightFirstBagMINUTES()
      {
         string desc = "";
         //Get value from UfisConfig.xml (Getting the timing from Ufis)
         return Convert.ToInt32(DBUfisConfig.GetInstance().GetConfigValueFor(WEB_BA_HLFBAG_TIME, out desc));
      }

      /// <summary>
      /// Highlight the Last Bag after this Minutes
      /// Last Bag
      /// --------
      ///    Last Bag shall be highlighted (shaded) in red 
      /// when it fails to be triggered 3 mins after acknowledged receive of last container.
      /// </summary>
      /// <returns></returns>
      public static int GetWebBAHighlightLastBagMINUTES()
      {
         string desc = "";
         //Get value from UfisConfig.xml (Getting the timing from Ufis)
         return Convert.ToInt32(DBUfisConfig.GetInstance().GetConfigValueFor(WEB_BA_HLLBAG_TIME, out desc));
      }

      /// <summary>
      /// 'From Minutes' to show the flight information for Baggage Arrival
      /// Data will show From Date Time ( Now - From Minutes)
      /// </summary>
      /// <returns></returns>
      public static int GetWebBAFromMinutes()
      {
         string desc = "";
         try
         {
            return Convert.ToInt32(DBConfig.GetInstance().GetConfigValueFor(WEB_BA_FR_MINUTES, out desc));
         }
         catch (Exception)
         {
            throw new ApplicationException("Invalid Baggage Arrival Flight Info 'From Minutes'");
         }
      }

      /// <summary>
      /// 'To Minutes' to show the flight information for Baggage Arrival
      /// Data will show To Date Time (Now + To Minutes) 
      /// </summary>
      /// <returns></returns>
      public static int GetWebBAToMinutes()
      {
         string desc = "";
         try
         {
            return Convert.ToInt32(DBConfig.GetInstance().GetConfigValueFor(WEB_BA_TO_MINUTES, out desc));
         }
         catch (Exception)
         {

            throw new ApplicationException("Invalid Baggage Arrival Flight Info 'To Minutes'");
         }
      }

      #endregion

      /// <summary>
      /// Number of days to keep the data in iTrek Server
      /// </summary>
      /// <returns></returns>
      public static int GetKeepDataDays()
      {
         string desc = "";
         string keepDay = DBConfig.GetInstance().GetConfigValueFor(KEEP_DAY, out desc);
         int days = -3;
         try
         {
            days = Convert.ToInt32(keepDay);
            if (days > 0) days = 0 - days;
         }
         catch (Exception ex)
         {
            LogMsg("GetKeepDataDays:Error:" + ex.Message);
         }
         if (days > 0) days = 0 - days;
         else if (days == 0) days = -3;
         return days;
      }

      public static int GetKeepShortLifeSpanDataDays()
      {
         string desc = "";
         string keepDay = DBConfig.GetInstance().GetConfigValueFor(KEEP_DAY_SH, out desc);
         int days = -1;
         try
         {
            days = Convert.ToInt32(keepDay);
            if (days > 0) days = 0 - days;
         }
         catch (Exception ex)
         {
            LogMsg("GetKeepShortLifeSpanDataDays:Error:" + ex.Message);
         }
         if (days > 0) days = 0 - days;
         else if (days == 0) days = -1;
         return days;
      }

      private static void LogMsg(string msg)
      {
         UtilLog.LogToGenericEventLog("CtrlConfig", msg);
      }

      private static void LogTraceMsg(string msg)
      {
         UtilLog.LogToTraceFile("CtrlConfig", msg);
      }

      public static bool IsTraceModeOn()
      {
         string desc = "";
         bool traceMode = false;
         string stTraceMode = DBConfig.GetInstance().GetConfigValueFor(TRACE_MODE, out desc);
         if (stTraceMode == "1") traceMode = true;
         return traceMode;
      }

      /// <summary>
      /// Get iTrekWeb Refresh Timing in Seconds (+ve)
      /// </summary>
      /// <returns>Time in Seconds</returns>
      public static int GetWebRefreshTiming()
      {
         const int DEF_REFRESH_TIMING = 300;
         int refreshTiming = DEF_REFRESH_TIMING; //in seconds

         string desc = "";
         string stTiming = DBConfig.GetInstance().GetConfigValueFor(WEB_REFRESH_TIMING, out desc);
         if ((stTiming != null) && (stTiming != ""))
         {
            try
            {
               refreshTiming = Convert.ToInt32(stTiming);
               refreshTiming = Math.Abs(refreshTiming);
               if (refreshTiming <= 0)
               {
                  refreshTiming = DEF_REFRESH_TIMING;
               }
            }
            catch (Exception)
            {
               refreshTiming = DEF_REFRESH_TIMING;
            }
         }
         return refreshTiming;
      }

      /// <summary>
      /// Get Time in Hour (-ve) to show the Message in Inbox
      /// </summary>
      /// <returns>Time in Hour (-ve)</returns>
      public static int GetMsgInboxTime()
      {
         const int DEF_HOUR = -12;
         string desc = "";
         string inboxTime = DBConfig.GetInstance().GetConfigValueFor(WEB_INBOX_TIME, out desc);
         int hour = DEF_HOUR;
         try
         {
            hour = Convert.ToInt32(inboxTime);
            if (hour > 0) hour = 0 - hour;
         }
         catch (Exception ex)
         {
            LogMsg("GetMsgInboxTime:Err:" + ex.Message);
         }
         if (hour > 0) hour = 0 - hour;
         else if (hour == 0) hour = DEF_HOUR;
         return hour;
      }

      /// <summary>
      /// Get Time in Hour to show the Message in Outbox
      /// </summary>
      /// <returns>Time in Hour (in -ve)</returns>
      public static int GetMsgOutboxTime()
      {
         const int DEF_HOUR = -12;
         string desc = "";
         string inboxTime = DBConfig.GetInstance().GetConfigValueFor(WEB_OUTBOX_TIME, out desc);
         int hour = DEF_HOUR;
         try
         {
            hour = Convert.ToInt32(inboxTime);
            if (hour > 0) hour = 0 - hour;
         }
         catch (Exception ex)
         {
            LogMsg("GetMsgOutboxTime:Err:" + ex.Message);
         }
         if (hour > 0) hour = 0 - hour;
         else if (hour == 0) hour = DEF_HOUR;
         return hour;
      }

      #region Methods related to UfisConfig
      /// <summary>
      /// Save Ufis Configuration Data
      /// </summary>
      /// <param name="userId">changes made by</param>
      /// <param name="dataId">Configuration Data Id</param>
      /// <param name="dataValue">Value of data</param>
      /// <param name="module">module name</param>
      /// <param name="dataDesc">Description of data</param>
      public static void SaveUfisConfigData(string userId,
          string dataId, string dataValue, string module, string dataDesc)
      {
         DBUfisConfig.GetInstance().Save(userId, dataId, dataValue, module, dataDesc);
      }

      /// <summary>
      /// Save Ufis Configuration Data sent from Backend
      /// </summary>
      /// <param name="configNew">New/Update/Delete Data</param>
      public static void SaveUfisConfigDataFromBackend(DSConfig configNew)
      {
         try
         {
            DBUfisConfig.GetInstance().UpdateData(configNew);
         }
         catch (Exception ex)
         {
            LogMsg("SaveUfisConfigDataFromBackend:Err:" + ex.Message);
            throw new ApplicationException("Unable to save Config Data.");
         }
      }

      private static string _qNameToRequestUfisConfigData = null;
      private static string GetQNameToRequestUfisConfigData()
      {
         if (string.IsNullOrEmpty(_qNameToRequestUfisConfigData))
         {
//#warning TO DO - To get the Queue Name
            iTXMLPathscl path = new iTXMLPathscl();
            _qNameToRequestUfisConfigData = path.GetFROMITREKSELECTQueueName();
         }
         return _qNameToRequestUfisConfigData;
      }

      private static List<string> _ufisConfigDataIdList = null;
      private static List<string> GetUfisConfigDataIdList()
      {
         if (_ufisConfigDataIdList == null)
         {
            _ufisConfigDataIdList = new List<string>();
            _ufisConfigDataIdList.Add(WEB_BA_HLUNSENT_TIME);
            _ufisConfigDataIdList.Add(WEB_BA_HLFBAG_TIME);
            _ufisConfigDataIdList.Add(WEB_BA_HLLBAG_TIME);
         }
         return _ufisConfigDataIdList;
      }

      /// <summary>
      /// Request All the Ufis Configuration Data from Backend.
      /// </summary>
      /// <returns>messageId of the request</returns>
      public static string RequestUfisConfigDataFromBackend()
      {
         string iTrekToSoccQueueName = GetQNameToRequestUfisConfigData();

         ITrekWMQClass1 iTWMQ = new ITrekWMQClass1();
         StringBuilder sb = new StringBuilder();
         sb.Append("<REQCONFIG>");
         List<string> arrIds = GetUfisConfigDataIdList();
         foreach (string id in arrIds)
         {
            sb.Append(string.Format(@"<ID>{0}</ID>", id));
         }
         sb.Append(@"</REQCONFIG>");
         string msg = sb.ToString();
         LogTraceMsg("RequestUfisConfig:" + msg);
         string msgId = iTWMQ.putTheMessageIntoQueue(msg, iTrekToSoccQueueName);
         LogTraceMsg("RequestedUfisConfigData");

         return msgId;
      }

      #endregion

   }
}