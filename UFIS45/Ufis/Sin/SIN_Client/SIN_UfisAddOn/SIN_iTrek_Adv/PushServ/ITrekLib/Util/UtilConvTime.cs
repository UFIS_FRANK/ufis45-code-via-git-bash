using System;
using System.Collections.Generic;
using System.Text;

namespace UFIS.ITrekLib.Util
{
    public class UtilTime
    {
        public static string ConvDateTimeToUfisFullDTString(DateTime dt)
        {
            string st = String.Format("{0:yyyyMMddHHmmss}", dt);
            return st;
        }

        public static string ConvDateTimeToITrekShortTimeString(DateTime dt)
        {
            string st = String.Format("{0:HHmm}", dt);
            return st;
        }

        public static DateTime ConvUfisTimeStringToDateTime(string st)
        {
            DateTime dt;
            if (!IsValidUfisTimeString( st, out dt ))
            {
                throw new ApplicationException("Invalid Date and Time format");
            }
            return dt;
        }

        public static String ConvUfisTimeStringToITrekShortTimeString(string stUfisTime)
        {
            return ConvDateTimeToITrekShortTimeString(ConvUfisTimeStringToDateTime(stUfisTime));
        }

        public static bool IsValidUfisTimeString( string st, out DateTime dt)
        {
            bool valid = false;
            dt = new DateTime(1, 1, 1);
            if (st == "") { valid = true; }
            else if (st.Length == 14)
            {
                try
                {
                    if (Convert.ToInt64(st).ToString() == st)
                    {
                        dt = new DateTime(Convert.ToInt16(st.Substring(0, 4)),
                            Convert.ToInt16(st.Substring(4, 2)),
                            Convert.ToInt16(st.Substring(6, 2)),
                            Convert.ToInt16(st.Substring(8, 2)),
                            Convert.ToInt16(st.Substring(10, 2)),
                            Convert.ToInt16(st.Substring(12, 2)));
                        valid = true;
                    }
                }
                catch (Exception )
                {                   
                    //throw;
                }
            }
            return valid;
        }

        public static DateTime CompDateTimeFromRef(DateTime dt, string hhmm)
        {
            ////To compute date and time for the "hhmm"
            ////  based on the "dt" datetime
            ////  take the nearest time to the base date time.
            DateTime result;
            //if (hhmm.Length != 4) throw new ApplicationException("Invalid time.");
            //int hour = Convert.ToInt32(hhmm.Substring(0, 2));
            //int min = Convert.ToInt32(hhmm.Substring(2, 2));
            //if ((hour < 0) || (hour > 23)) throw new ApplicationException("Invalid Hour.");
            //if ((min < 0) || (min > 59)) throw new ApplicationException("Invalid Minute.");
            //int refHour = dt.Hour;
            //int refMin = dt.Minute;
            //int incDay = 0;
            ////result = new DateTime(dt.Year, dt.Month, dt.Day, hour, min, 0);
            //incDay = ComputeIncDays(refHour, hour);
           
            ////result.AddDays(1);
            //result = new DateTime(dt.Year, dt.Month, dt.Day, hour, min, 0).AddDays(incDay);
            ////result.AddDays( incDay );
            result = CompDateTimeFromRef(dt, hhmm, 12);
            return result;
        }

        public static DateTime CompDateTimeFromRef(DateTime dt, string hhmm, int hourRange)
        {
            //To compute date and time for the "hhmm"
            //  based on the "dt" datetime
            //  take the nearest time to the base date time.
            DateTime result;
            if (hhmm.Length != 4) throw new ApplicationException("Invalid time.");
            int hour = Convert.ToInt32(hhmm.Substring(0, 2));
            int min = Convert.ToInt32(hhmm.Substring(2, 2));
            if ((hour < 0) || (hour > 23)) throw new ApplicationException("Invalid Hour.");
            if ((min < 0) || (min > 59)) throw new ApplicationException("Invalid Minute.");
            int refHour = dt.Hour;
            int refMin = dt.Minute;
            int incDay = 0;
            //result = new DateTime(dt.Year, dt.Month, dt.Day, hour, min, 0);
            incDay = ComputeIncDays(refHour, hour, hourRange);

            //result.AddDays(1);
            result = new DateTime(dt.Year, dt.Month, dt.Day, hour, min, 0).AddDays(incDay);
            //result.AddDays( incDay );
            return result;
        }

        private static int ComputeIncDays(int refHour, int hour, int hourRange)
        {
            int incDay = 0;
            int h1 = hour - refHour; 
            int h2 = h1 + 24;
            int h3 = refHour - hour;
            int h4 = h3 + 24;

            if ((h1 >= 0) && (h1 < hourRange)) incDay = 0;
            else if ((h2 >= 0) && (h2 < hourRange)) incDay = +1;
            else if ((h3 >= 0) && (h3 < hourRange)) incDay = 0;
            else if ((h4 >= 0) && (h4 < hourRange)) incDay = -1;
            else throw new ApplicationException("Invalid Time. Time is not within " + hourRange + " hour.");
            return incDay;
        }
    }
}
