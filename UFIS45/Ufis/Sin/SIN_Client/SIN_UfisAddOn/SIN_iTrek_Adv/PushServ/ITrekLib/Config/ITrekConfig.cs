using System;
using System.Collections.Generic;
using System.Text;

using System.IO;
using MM.UtilLib;

namespace UFIS.ITrekLib.Config
{
	public enum EnumEnv { Dev, UAT, Prod, Unknown };

	public class ITrekConfig
	{
		public static string GetPathForFlightDbFileMapping()
		{
			string fnDbFileMapping = UFIS.ITrekLib.Properties.Settings.Default.File_DB_Flight_Mapping;
			string stBasePath = GetAppBasePath();
			return System.IO.Path.Combine(stBasePath, fnDbFileMapping);
		}

		#region Application Environment Related Info

		public static string GetVersion()
		{
			return Util.UtilITrekRegistry.GetItrekVersion();
		}

		private static EnumEnv _enumEnv = EnumEnv.Unknown;

		/// <summary>
		/// Get What is the Application Environment. (e.g DEV/UAT/PROD)
		/// </summary>
		/// <returns></returns>
		public static EnumEnv GetAppEnv()
		{
			EnumEnv env = _enumEnv;
			//string stEnv = UFIS.ITrekLib.Properties.Settings.Default.AppEnv;
			if (env == EnumEnv.Unknown)
			{
				string stEnv = Util.UtilITrekRegistry.GetItrekCurrentEnvironment();

				if (!string.IsNullOrEmpty(stEnv))
				{
					switch (stEnv.Trim().ToUpper())
					{
						case "DEV":
							env = EnumEnv.Dev;
							break;
						case "UAT":
							env = EnumEnv.UAT;
							break;
						case "PROD":
							env = EnumEnv.Prod;
							break;
					}
				}
				_enumEnv = env;
			}
			return env;
		}

		/// <summary>
		/// Get What is the Application Environment as String for Display
		/// </summary>
		/// <returns></returns>
		public static string GetAppEnvString()
		{
			string stEnv = "Unknown";
			switch (GetAppEnv())
			{
				case UFIS.ITrekLib.Config.EnumEnv.Dev:
					stEnv = "Dev";
					break;
				case UFIS.ITrekLib.Config.EnumEnv.Prod:
					stEnv = "Prod";
					break;
				case UFIS.ITrekLib.Config.EnumEnv.UAT:
					stEnv = "UAT";
					break;

			}
			return stEnv;
		}

		/// <summary>
		/// Get Application Base Path (e.g. C:\Program Files\iTrekOk2Load\)
		/// </summary>
		/// <returns></returns>
		public static string GetAppBasePath()
		{
			//string appBasePath = "";
			//switch (GetAppEnv())
			//{
			//   case EnumEnv.Dev:
			//      appBasePath = UFIS.ITrekLib.Properties.Settings.Default.AppBasePath_DEV;
			//      break;
			//   case EnumEnv.UAT:
			//      appBasePath = UFIS.ITrekLib.Properties.Settings.Default.AppBasePath_UAT;
			//      break;
			//   case EnumEnv.Prod:
			//      appBasePath = UFIS.ITrekLib.Properties.Settings.Default.AppBasePath_PROD;
			//      break;
			//   case EnumEnv.Unknown:
			//      throw new ApplicationException("Unknown Environment!!!");
			//   //break;
			//   default:
			//      throw new ApplicationException("Unknown Environment!!!");
			//   //break;
			//}
			//return appBasePath;
			return ItrekBaseDir;
		}

		#endregion

		/// <summary>
		/// Get What is the type of database used (e.g. ORACLE/MSSQL)
		/// </summary>
		/// <returns></returns>
		public static String GetDbType()
		{
			return UFIS.ITrekLib.Properties.Settings.Default.DB;
		}

		/// <summary>
		/// Get the Database Version
		/// </summary>
		/// <returns></returns>
		public static string GetDbVersion()
		{
			return UFIS.ITrekLib.Properties.Settings.Default.DB_Version;
		}

		/// <summary>
		/// Get the Interface Information file path.		
		/// </summary>
		/// <returns></returns>
		public static string GetIntInfoFilePath()
		{
			return UFIS.ITrekLib.Properties.Settings.Default.IntInfoFilePath;
		}

		/// <summary>
		/// Get the path of the file in which "Decrypted" connection information was kept.
		/// </summary>
		/// <returns></returns>
		public static string GetDbConnFilePath()
		{
			string stBasePath = GetAppBasePath();
			string stDbConnFilePath = UFIS.ITrekLib.Properties.Settings.Default.File_DB_Conn;
			return System.IO.Path.Combine(stBasePath, stDbConnFilePath);
		}

		private static string _dbConnString = null;
		private static DateTime _DtRefreshDbConnString = new DateTime(1, 1, 1);
		private static DateTime _DtFileDbConn = new DateTime(1, 1, 1);

		/// <summary>
		/// Get Database Connection String (include the user id and password as well)
		/// </summary>
		/// <returns></returns>
		public static string GetDbConnString()
		{
			string dbConnString = _dbConnString;
			if (string.IsNullOrEmpty(dbConnString) || (UtilTime.DiffInSec(DateTime.Now, _DtRefreshDbConnString) > 300))
			{
				dbConnString = ReadAndGetDbConnString();
				//LogTraceMsg("dbConnString" + dbConnString);
				_DtRefreshDbConnString = DateTime.Now;
			}

			return dbConnString;
		}

		/// <summary>
		/// Get the Key to Decrypt and Encrypt.
		/// </summary>
		/// <returns></returns>
		private static string GetEncryptionKey()
		{
			return "abcdefghef1234567890ufis";//24 char
		}

		/// <summary>
		/// Encrypt the given decrypted data.
		/// </summary>
		/// <param name="st">Decrypted Data</param>
		/// <returns>Original Data</returns>
		public static string EncryptData(string st)
		{
			return UtilEncrypt.Encrypt(st, true, GetEncryptionKey());
		}

		/// <summary>
		/// Decrypt the given original data.
		/// </summary>
		/// <param name="st">Original Data</param>
		/// <returns>Decrypted Data</returns>
		public static string DecryptData(string st)
		{
			return UtilEncrypt.Decrypt(st, true, GetEncryptionKey());
		}

		/// <summary>
		/// Read the decrypted data from file and get the original db connection string.
		/// </summary>
		/// <returns>Db Connection String</returns>
		public static string ReadAndGetDbConnString()
		{
			try
			{
				string fName = GetDbConnFilePath();
				FileInfo fInfo = new FileInfo(fName);
				DateTime fLastWriteTime = fInfo.LastWriteTime;
				if (fLastWriteTime.CompareTo(_DtFileDbConn) != 0)
				{
					_dbConnString = DecryptData(File.ReadAllText(fName));
					_DtFileDbConn = fLastWriteTime;
					//LogTraceMsg("ReadAndGetDbConnString:ReadConfig.");
				}
			}
			catch (Exception ex)
			{
				LogMsg("ReadAndGetDbConnString:Err:" + ex.Message);
				throw;
			}
			return _dbConnString;
		}

		/// <summary>
		/// Write the decrypted db connection string to file for given original db connection string.
		/// </summary>
		/// <param name="dbConnString"></param>
		public static void WriteDbConnStringToFile(string dbConnString)
		{
			string fName = GetDbConnFilePath();
			File.WriteAllText(fName, EncryptData(dbConnString));
		}

		private static string _iTrekDbDataSchemaName = null;
		public static string ITrekDbDataSchemaName
		{
			get
			{
				if (string.IsNullOrEmpty(_iTrekDbDataSchemaName))
				{
					_iTrekDbDataSchemaName = Util.UtilITrekRegistry.GetDbDataSchemaName();
					if (string.IsNullOrEmpty(_iTrekDbDataSchemaName))
					{
						throw new ApplicationException("ITrek Database Schema Info NOT found.");
					}
				}
				return _iTrekDbDataSchemaName;
			}
		}

		private static string _iTrekBaseDir = null;

		public static string ItrekBaseDir
		{
			get
			{
				if (string.IsNullOrEmpty(_iTrekBaseDir))
				{
					_iTrekBaseDir = Util.UtilITrekRegistry.GetItrekBaseDir();
					if (string.IsNullOrEmpty(_iTrekBaseDir))
					{
						throw new ApplicationException("ITrek Base Directory Info NOT Found.");
					}
				}
				return _iTrekBaseDir;
			}
		}

		private static string _iTrekPathsXmlPath = null;

		public static string GetPathsXmlFilePath()
		{
			if (string.IsNullOrEmpty(_iTrekPathsXmlPath))
			{
				_iTrekPathsXmlPath = UtilMisc.Combine(ItrekBaseDir, @"XML\Paths.xml");
				if (string.IsNullOrEmpty(_iTrekPathsXmlPath)) throw new ApplicationException("Not able to get the Path Information.");
			}
			return _iTrekPathsXmlPath;
		}


		#region Logging
		private static void LogTraceMsg(string msg)
		{
			try
			{
				MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("iTrekConfig", msg);
			}
			catch (Exception)
			{
				UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("iTrekConfig", msg);
			}
		}

		private static void LogMsg(string msg)
		{
			//UtilLog.LogToGenericEventLog("DBDMsg", msg);
			try
			{
				MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("iTrekConfig", msg);
			}
			catch (Exception)
			{
				UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("iTrekConfig", msg);
			}
		}
		#endregion
	}
}