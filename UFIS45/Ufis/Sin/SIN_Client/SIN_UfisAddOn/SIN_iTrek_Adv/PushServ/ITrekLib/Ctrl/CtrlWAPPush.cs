#define LOGGIN_ON
#define TRACING_ON

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Diagnostics;
using System.Threading;

//using iTrekOk2L;
using iTrekSessionMgr;
using iTrekXML;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.DB;
using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.Util;
using MM.UtilLib;
using UFIS.ITrekLib.CommMsg;
using UFIS.ITrekLib.Misc;

namespace UFIS.ITrekLib.Ctrl
{

    public class CtrlWAPPush
    {

        public static void PushFlightXMLChangesMsgAlertToAO(string flightXml,string flightId,DSFlight dsFlight)
        {
            try
            {
                CtrlWAPPush.GetInstance().MyPushFlightXMLChangesMsgAlertToAO(flightXml,flightId,dsFlight);
            }
            catch (Exception ex)
            {
                LogMsg("PushFlightXMLChangesMsgAlertToAO:Error:" + ex.Message);
            }
        }

        public static void PushJobXMLChangesMsgAlertToAO(string jobXml,string flightId)
        {
            try
            {
                CtrlWAPPush.GetInstance().MyPushJobXMLChangesMsgAlertToAO(jobXml,flightId);
            }
            catch (Exception ex)
            {
                LogMsg(jobXml);
                LogMsg("PushJobXMLChangesMsgAlertToAO:Error:" + ex.Message);
            }
        }


        public static void PushJobUpdateChangesMsgAlertToAO(string peno, string flightId)
        {
            try
            {
                CtrlWAPPush.GetInstance().PushJobAssignmentFlightChangesMsgToAO(flightId, peno);
            }
            catch (Exception ex)
            {
               
                LogMsg("PushJobXMLChangesMsgAlertToAO:Error:" + ex.Message);
            }
        }
        public static CtrlWAPPush GetInstance()
        {
            CtrlWAPPush ctrl = new CtrlWAPPush();
            return ctrl;
        }


        private static string GetPapUrl()
        {
            iTXMLPathscl paths = new iTXMLPathscl();
            return paths.GetPapURL();
        }

        public void PushMsgToClient(string clientId, string msg)
        {
            LogTraceMsg("Start of PushMsgToClient");
            try
            {
                //SessionManager sessMgr = new SessionManager();
                //string deviceId = sessMgr.GetDeviceIDByPENO(clientId);
                DSCurUser dsCurUser = CtrlCurUser.RetrieveCurUserForStaffId(clientId);
                string deviceId = dsCurUser.LOGINUSER[0].DEVICE_ID;
                PushMsgToDevice(clientId,deviceId, msg);
            }
            catch (Exception ex)
            {
                LogMsg("PushMsgToClient:Error:" + ex.Message);
            }
            LogTraceMsg("End of PushMsgToClient");
        }

        public void PushMsgToClients(ArrayList clientIdArrays, string msg)
        {
            SessionManager sessMgr = new SessionManager();
            int cnt = clientIdArrays.Count;
            for (int i = 0; i < cnt; i++)
            {
                string clientId = clientIdArrays[i].ToString();
                //string deviceId = sessMgr.GetDeviceIDByPENO(clientId);
                DSCurUser dsCurUser = CtrlCurUser.RetrieveCurUserForStaffId(clientId);
                string deviceId = null;
                try
                {
                    deviceId = dsCurUser.LOGINUSER[0].DEVICE_ID;

                    PushMsgToDevice(clientId,deviceId, msg);
                }
                catch (Exception)
                {
                }
            }
        }

        public void PushMsgToDevices(ArrayList deviceIdArrays, string msg)
        {
            SessionManager sessMgr = new SessionManager();
            int cnt = deviceIdArrays.Count;
            for (int i = 0; i < cnt; i++)
            {
                string deviceId = deviceIdArrays[i].ToString();
              //  PushMsgToDevice(deviceId, msg);
            }
        }

        private string _thDeviceId = null;
        private string _thMsg = null;

        private void PushMsgToDevice(string staffId,string deviceId, string msg)
        {
            //  if ((deviceId != null) && (deviceId != "") && (deviceId != "There are no devices allocated to this staff id (PENO)"))
            string replyTo = "";
            try
            {
                if ((deviceId != null) && (deviceId != ""))
                {
                    CMsgOut.NewMessageToSend(null, deviceId + "|" + staffId, msg, "OPSSPM", "ALERT", out replyTo);


                }
                else
                {
                    LogTraceMsg("PushMsgToDevice:Error:" + deviceId + ", No device Id found");
                }
            }
            catch (Exception ex)
            {
               LogTraceMsg("PushMsgToDevice:Error:"+ex.Message);  
                
            }
        }

        //private void ThPushMsgToDevice()
        //{
        //    string deviceId = _thDeviceId;
        //    string msg = _thMsg;
        //    _thDeviceId = null;
        //    _thMsg = null;
        //    if ((deviceId != null) && (deviceId != "") && (deviceId != "There are no devices allocated to this staff id (PENO)"))
        //    {
        //        System.Threading.Thread.Sleep(GetSleepingTimeForPush());
        //        XmlPushClient pushClient = new XmlPushClient(GetPapUrl());

        //        string pid = pushClient.PushServiceIndication(deviceId, msg, GetUrlForNetAlert());
        //        LogTraceMsg("Pid" + pid + "msg" + msg);
        //    }
        //}


        private int GetSleepingTimeForPush()
        {

            iTXMLPathscl path = new iTXMLPathscl();
            return Convert.ToInt32(path.GetDelayForPush());

        }
        public void PushMsgToFlightApronStaffs(string flightUrNo, string msg,string peno)
        {
            //iTXMLPathscl path = new iTXMLPathscl();
            //SessionManager sessMan = new SessionManager();
            ////Hashtable ht = sessMan.GetDeviceIDSinHTByUAFT(flightUrNo, path.GetJobsFilePath());
            //ArrayList penoAl = sessMan.GetPenoByUAFT(flightUrNo, path.GetJobsFilePath());
            ////key ==> peno, value ==> device Id
            //ArrayList al = new ArrayList();
            //foreach (string peno in penoAl)
            //{
                string deviceid = null;
                try
                {
                    deviceid = CtrlCurUser.RetrieveCurUserForStaffId(peno).LOGINUSER[0].DEVICE_ID;
                    //al.Add(deviceid);
                }
                catch (Exception)
                {
                }
            //}

            //foreach (string deviceId in ht.Values)
            //{
            //    al.Add(deviceId);
            //}
           // PushMsgToDevices(al, msg);
                PushMsgToDevice(peno,deviceid,msg);
        }

        private void PushJobAssignmentChangesMsgToAO(string flightUrNo, string flightNo,
            string estimateTimeOfArrivalOrDeparture,
            string parkingStand,DSFlight ds)
        {   //Do Not use for Flight Assignment changes (in Job Service).
            //Use for Timing changes and/or Parking Stand Changes 
            //To use in Flight Service "UPDATE"
            iTXMLPathscl path = new iTXMLPathscl();
            bool sendMsg = false;
            int jobAlertMinutes = 0;
            //jobAlertMinutes = path.GetWapJobAssignAlertMinutes();
            jobAlertMinutes = Convert.ToInt32(CtrlConfig.GetWapJobAssignAlertMinutes());
            if (jobAlertMinutes > 0) jobAlertMinutes = -1 * Math.Abs(jobAlertMinutes);
            //DSFlight ds = CtrlFlight.RetrieveFlightsByFlightId(flightUrNo);
            string flTime = "";
            string stParkingStand = "";



            if ((ds != null) && (ds.FLIGHT.Rows.Count > 0))
            {
                DSFlight.FLIGHTRow row = ds.FLIGHT[0];
                try
                {
                    flTime = row.ET;
                }
                catch { }
                if (flTime == "")
                {
                    try
                    {
                        flTime = row.ST;
                    }
                    catch { }
                }
                try
                {
                    stParkingStand = row.PARK_STAND;
                }
                catch { }

                try
                {
                    if (stParkingStand == "")
                        stParkingStand = row.GATE;
                }
                catch { }
            }
            if ((estimateTimeOfArrivalOrDeparture != "") && (estimateTimeOfArrivalOrDeparture != flTime))
            {
                sendMsg = true;
                flTime = estimateTimeOfArrivalOrDeparture;
            }
            if ((parkingStand != "") && (parkingStand != stParkingStand))
            {
                sendMsg = true;
                stParkingStand = parkingStand;
            }
            if (sendMsg)
            {
                if (flTime == "") flTime = estimateTimeOfArrivalOrDeparture;
                if (flTime != "")
                {
                    DateTime dtFlTime = UtilTime.ConvUfisTimeStringToDateTime(flTime);
                    if ((dtFlTime.AddMinutes(jobAlertMinutes)).CompareTo(DateTime.Now) <= 0)
                    {//need to alert to user
                        string msg = flightNo
                            + "," + String.Format("{0:HHmm}", dtFlTime)
                            + "," + stParkingStand;
                        if (msg.Length > 18)
                        {

                            msg = msg.Substring(0, 18);

                        }
                        string peno = "";
                        try
                        {
                            peno = ((DSFlight.FLIGHTRow)(ds.FLIGHT.Rows[0])).AOID;
                        }
                        catch (Exception)
                        {
                            
                            
                        }
                        PushMsgToFlightApronStaffs(flightUrNo, msg,peno);
                    }
                }
            }
        }

        private void PushJobAssignmentFlightChangesMsgToAO(string flightUrNo, string peNo)
        {//use for Flight Assignment changes only 
            // to use in Job Service "INSERT"
            int jobAlertMinutes = 0;
            //iTXMLPathscl path = new iTXMLPathscl();
            //jobAlertMinutes = path.GetWapJobAssignAlertMinutes();
            jobAlertMinutes = Convert.ToInt32(CtrlConfig.GetWapJobAssignAlertMinutes());
            if (jobAlertMinutes > 0) jobAlertMinutes = -1 * Math.Abs(jobAlertMinutes);
            bool hasData = false;
            string adid = "";
            DateTime curDt = ITrekTime.GetCurrentDateTime();
            //string flightUrno = CtrlFlight.GetFlightIdForUaft(null, flightUrNo, UtilTime.ConvDateTimeToUfisFullDTString(curDt), out hasData, out adid);
            DSFlight ds = CtrlFlight.RetrieveFlightsByFlightId(flightUrNo);
            string stParkingStand = "";
            string flTime = "";
            string flno = "";
            if ((ds != null) && (ds.FLIGHT.Rows.Count > 0))
            {
                DSFlight.FLIGHTRow row = ds.FLIGHT[0];
                try
                {
                    stParkingStand = row.PARK_STAND;
                }
                catch
                {


                }
                try
                {
                    if (stParkingStand == "")
                        stParkingStand = row.GATE;
                }
                catch { }
                flno = row.FLNO;
                flTime = row.ST;
                try
                {
                    if (row.ET != "") flTime = row.ET;
                }
                catch { }
            }

            if (flTime != "")
            {
                DateTime dtFlTime = UtilTime.ConvUfisTimeStringToDateTime(flTime);
                if ((dtFlTime.AddMinutes(jobAlertMinutes)).CompareTo(DateTime.Now) <= 0)
                {//need to alert to user
                    string msg = flno
                        + "," + String.Format("{0:HHmm}", dtFlTime)
                        + "," + stParkingStand;
                    if (msg.Length > 18)
                    {

                        msg = msg.Substring(0, 18);

                    }
                    //PushMsgToFlightApronStaffs(flightUrNo, msg);
                    PushMsgToClient(peNo, msg);
                }
            }
        }


        private void MyPushFlightXMLChangesMsgAlertToAO(string flightXml,string flightUrno,DSFlight dsFlight)
        {
            XmlDocument newFlightXML = new XmlDocument();
            newFlightXML.LoadXml(flightXml);
            XmlElement newFlightXMLElement = newFlightXML.DocumentElement;
            //string flightUrno = newFlightXMLElement.GetElementsByTagName("URNO").Item(0).InnerXml.Trim();
            string adId = "";
            string orgParkingStand = "";
            string orgST = "";
            string orgET = "";
            string flightNo = "";
            string orgGate = "";
            try
            {
                adId = newFlightXMLElement.GetElementsByTagName("ADID").Item(0).InnerXml;
            }
            catch (Exception)
            {
                try
                {
                    //DSFlight dsFlight = CtrlFlight.RetrieveFlightsByFlightId(flightUrno);
                    if (dsFlight != null)
                    {
                        if (dsFlight.FLIGHT.Rows.Count > 0)
                        {
                            DSFlight.FLIGHTRow row = ((DSFlight.FLIGHTRow)dsFlight.FLIGHT.Rows[0]);
                            adId = row.A_D;
                            try
                            {
                                orgParkingStand = row.PARK_STAND;
                            }
                            catch
                            {


                            }
                            orgST = row.ST;
                            try
                            {
                                orgET = row.ET;
                            }
                            catch
                            {


                            }
                            flightNo = row.FLNO;
                            try
                            {
                                orgGate = row.GATE;
                            }
                            catch
                            {


                            }
                        }
                        else
                        {
                            LogMsg("PushFlightXMLChangesMsgAlertToAO:Error:" + flightUrno + " not found.");
                        }
                    }
                    else
                    {
                        LogMsg("PushFlightXMLChangesMsgAlertToAO: No Flight Info.");
                    }
                }
                catch (Exception ex)
                {
                    LogMsg("PushFlightXMLChangesMsgAlertToAO:ErrorFlight: " + ex.Message);
                    throw;
                }
            }
            string flightSt = "";
            string flightEt = "";
            string flightParkingStand = "";
            string flightTime = "";
            string flightGate = "";

            if (adId == "A")
            {
                //flightSt = newFlightXMLElement.GetElementsByTagName("STOA").Item(0).InnerXml;
                //flightEt = newFlightXMLElement.GetElementsByTagName("ETOA").Item(0).InnerXml;
                //flightParkingStand = newFlightXMLElement.GetElementsByTagName("PSTA").Item(0).InnerXml;
                flightSt = MyGetElementsByTagName(newFlightXMLElement, "STOA");
                flightEt = MyGetElementsByTagName(newFlightXMLElement, "ETAI");
                flightParkingStand = MyGetElementsByTagName(newFlightXMLElement, "PSTA");
                flightGate = MyGetElementsByTagName(newFlightXMLElement, "GTA1");
            }
            else if (adId == "D")
            {
                //flightSt = newFlightXMLElement.GetElementsByTagName("STOD").Item(0).InnerXml;
                //flightEt = newFlightXMLElement.GetElementsByTagName("ETOD").Item(0).InnerXml;
                //flightParkingStand = newFlightXMLElement.GetElementsByTagName("PSTD").Item(0).InnerXml;
                flightSt = MyGetElementsByTagName(newFlightXMLElement, "STOD");
                flightEt = MyGetElementsByTagName(newFlightXMLElement, "ETDI");
                flightParkingStand = MyGetElementsByTagName(newFlightXMLElement, "PSTD");
                flightGate = MyGetElementsByTagName(newFlightXMLElement, "GTD1");
            }
            else
            {
            }

            bool hasChanges = false;
            if (flightSt != null)
            {
                if (flightSt != orgST) { hasChanges = true; }
            }
            else { flightSt = orgST; }

            if (flightEt != null)
            {
                if (flightEt != orgET) { hasChanges = true; }
            }
            else { flightEt = orgET; }

            if (flightParkingStand != null)
            {
                if (flightParkingStand != orgParkingStand) { hasChanges = true; }
            }
            else { flightParkingStand = orgParkingStand; }

            if (flightGate != null)
            {
                if (flightGate != orgGate) { hasChanges = true; }
            }
            else { flightGate = orgGate; }

            if (hasChanges)
            {
                flightTime = flightSt;
                if (flightEt != "") flightTime = flightEt;
                if (flightParkingStand != "")
                {
                    PushJobAssignmentChangesMsgToAO(flightUrno, flightNo, flightTime, flightParkingStand,dsFlight);
                }
                else
                {
                    PushJobAssignmentChangesMsgToAO(flightUrno, flightNo, flightTime, flightGate,dsFlight);
                }
            }
        }

        private string MyGetElementsByTagName(XmlElement flightXmlElement, string tagName)
        {
            string st = "";
            try
            {
                st = flightXmlElement.GetElementsByTagName(tagName).Item(0).InnerXml;
            }
            catch (Exception)
            {
                st = null;
            }
            return st;
        }

        private void MyPushJobXMLChangesMsgAlertToAO(string jobXml, string flightUrno)
        {
            LogTraceMsg("Before Push");
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(jobXml);
            XmlElement xmlEle = xmlDoc.DocumentElement;
           // string flightUrno = MyGetElementsByTagName(xmlEle, "UAFT").Trim();
            string peNo = MyGetElementsByTagName(xmlEle, "PENO");
            if ((flightUrno != "") && (peNo != ""))
            {
                LogTraceMsg("Before Flight Change");
                PushJobAssignmentFlightChangesMsgToAO(flightUrno, peNo);
                LogTraceMsg("After Flight Change");
            }
            else
            {
                LogMsg("PushJobXMLChangesMsgAlertToAO:Error:" + jobXml);
            }
            LogTraceMsg("After Push");
        }

        //[Conditional("TRACING_ON")]
        private static void LogTraceMsg(string msg)
        {
            UtilLog.LogToTraceFile("CtrlWAPPush", msg);
        }

        //[Conditional("LOGGING_ON")]
        private static void LogMsg(string msg)
        {
            UtilLog.LogToGenericEventLog("CtrlWAPPush", msg);
        }
        private string GetUrlForNetAlert()
        {
            iTXMLPathscl paths = new iTXMLPathscl();
            return paths.GetURLForNetAlert() + "login.aspx"; 
        }


        private void ThPushMsgToDevice()
        {
            string deviceId = _thDeviceId;
            string msg = _thMsg;
            try
            {
                _thDeviceId = null;
                _thMsg = null;
                if ((deviceId != null) && (deviceId != "") && (deviceId != "There are no devices allocated to this staff id (PENO)"))
                {
                    LogTraceMsg("Before going to sleep" + msg);
                    System.Threading.Thread.Sleep(GetSleepingTimeForPush());
                    XmlPushClient pushClient = new XmlPushClient(GetPapUrl());

                    string pid = pushClient.PushServiceIndication(deviceId, msg, GetUrlForNetAlert());
                    LogTraceMsg("Pid" + pid + "msg" + msg);
                }
            }
            catch (Exception ex)
            {
                LogMsg("ThPushMsgToDevice:Err:" + deviceId + ":" + msg + ":" +
                ex.Message);
            }
        }

    }
}
