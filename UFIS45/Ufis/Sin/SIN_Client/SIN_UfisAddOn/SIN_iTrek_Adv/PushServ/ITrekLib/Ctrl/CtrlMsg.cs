using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.DB;
//using iTrekXML;
using iTrekSessionMgr;

using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.Util;
using UFIS.ITrekLib.Misc;
using MM.UtilLib;
using UFIS.ITrekLib.CommMsg;

namespace UFIS.ITrekLib.Ctrl
{
	public class CtrlMsg
	{
		#region Retrieve Out Messages
		public static DSMsg RetrieveOutMessages(string staffId)
		{
			DSMsg dsMsg = new DSMsg();

			try
			{
				dsMsg = ((DSMsg)dbMsg.RetrieveOutMsg(staffId));
			}
			catch (Exception ex)
			{
				throw new ApplicationException("Unable to get the  Messages" + ex.Message);
			}
			return dsMsg;
		}

		public static DSMsg RetrieveOutMessages(string staffId, int maxCnt)
		{
			DSMsg dsMsg = new DSMsg();

			try
			{
				dsMsg = ((DSMsg)dbMsg.RetrieveOutMsg(staffId, maxCnt, CtrlConfig.GetMsgOutboxTime()));
			}
			catch (Exception ex)
			{
				LogMsg("RetrieveOutMessages:Error:" + ex.Message);
				throw new ApplicationException("Unable to get the Outbox Messages");
			}
			return dsMsg;
		}

		public static DSMsg RetrieveOutMessageDetailsForUrno(string urno)
		{
			DSMsg dsMsg = new DSMsg();

			try
			{
				dsMsg = ((DSMsg)dbMsg.RetrieveOutMessageDetailsForUrno(urno));
			}
			catch (Exception ex)
			{
				LogMsg("RetrieveOutMessageDetailsForUrno:Error:" + ex.Message);
				throw new ApplicationException("Unable to get the Messages Detail");
			}
			return dsMsg;

		}
		#endregion

		private static DBDMsg dbMsg = DBDMsg.GetInstance();

		#region Retrieve In Messages
		public static DSMsg RetrieveInMessages(string staffId)
		{
			DSMsg dsMsg = new DSMsg();

			try
			{
				dsMsg = ((DSMsg)dbMsg.RetrieveInMsg(staffId));
			}
			catch (Exception ex)
			{
				throw new ApplicationException("Unable to get the Messages " + ex.Message);
			}
			return dsMsg;
		}

		public static DSMsg RetrieveInMessages(string staffId, string jobCat)
		{
			DSMsg dsMsg = new DSMsg();

			try
			{
				dsMsg = ((DSMsg)dbMsg.RetrieveInMsg(staffId, jobCat));
			}
			catch (Exception ex)
			{
				throw new ApplicationException("Unable to get the Messages " + ex.Message);
			}
			return dsMsg;
		}

		public static DSMsg RetrieveInMessages(string staffId, ArrayList groupList)
		{
			DSMsg dsMsg = new DSMsg();

			try
			{
				dsMsg = ((DSMsg)dbMsg.RetrieveInMsg(staffId, groupList));
			}
			catch (Exception ex)
			{
                LogMsg("RetrieveInMessages:Err:" + ex.Message);
				//throw new ApplicationException("Unable to get the  Messages" + ex.Message);
			}
			return dsMsg;
		}


		public static DSMsg RetrieveInMsgForFlight(string urno, string turnUrno)
		{



			DSMsg dsMsg = new DSMsg();

			try
			{
				dsMsg = ((DSMsg)dbMsg.RetrieveInMsgForFlight(urno, turnUrno));
			}
			catch (Exception ex)
			{
                LogMsg("RetrieveInMsgForFlight:Err:" + ex.Message);
				//throw new ApplicationException("Unable to get the  Messages" + ex.Message);
			}
			return dsMsg;
		}


		public static DSMsg RetrieveInMessages(string staffId, ArrayList groupList, int maxCnt)
		{
			DSMsg dsMsg = new DSMsg();

			try
			{
				dsMsg = ((DSMsg)dbMsg.RetrieveInMsg(staffId, groupList, maxCnt, CtrlConfig.GetMsgInboxTime()));
			}
			catch (Exception ex)
			{
				LogMsg("RetrieveInMessages:Err:" + ex.Message);
				//throw new ApplicationException("Unable to get the Inbox Messages.");
			}
			return dsMsg;
		}

		public static DSMsg RetrieveInMessages(string staffId, ArrayList groupList, int maxCnt, EnComposeMsgType msgType)
		{
			DSMsg dsMsg = new DSMsg();

			try
			{
				dsMsg = ((DSMsg)dbMsg.RetrieveInMsg(staffId, groupList, maxCnt, CtrlConfig.GetMsgInboxTime(), msgType));
			}
			catch (Exception ex)
			{
				LogMsg("RetrieveInMessages:Err:" + ex.Message);
				throw new ApplicationException("Unable to get the Inbox Messages.");
			}
			return dsMsg;
		}

		public static DSMsg RetrieveInMessageDetailsForUrno(string urno)
		{
			DSMsg dsMsg = new DSMsg();

			try
			{
				dsMsg = ((DSMsg)dbMsg.RetrieveInMessageDetailsForUrno(urno));
			}
			catch (Exception ex)
			{
				LogMsg("RetrieveInMessageDetailsForUrno:Err:" + ex.Message);
				throw new ApplicationException("Unable to get the Message Detail.");
			}
			return dsMsg;
		}
		#endregion


		/// <summary>
		/// Send the message. If successfully send, it will return True.
		/// </summary>
		/// <param name="recList">Recepient List</param>
		/// <param name="sentByUserId">user id who send the message</param>
		/// <param name="sentByUserName">user name</param>
		/// <param name="msgText">message body</param>
		/// <returns>True==>Success</returns>
		public static bool SendMessage(ArrayList recList, string sentByUserId, string sentByUserName, string msgText)
		{
			IDbCommand cmd = null;
			bool successfullySend = false;

			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			bool isNewCmd = false;//Is command 'cmd' created from this method?
			string msgId = "";
            ArrayList lstReceipient = new ArrayList();
			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = db.GetCommand(true);
				}

				DateTime curDt = ITrekTime.GetCurrentDateTime();
				List<EntMsgTo> lsMsgTo = new List<EntMsgTo>();
				int rCnt = recList.Count;
				for (int i = 0; i < rCnt; i++)
				{
					EnumReceipientType recType;
					string receipientId = "";
					receipientId = ComputeMsgReceipientAndType((string)recList[i], out recType, out receipientId);
					lsMsgTo.Add(new EntMsgTo(receipientId, recType));
				}

                msgId = dbMsg.SendMessage(cmd, new EntMsg(sentByUserId, sentByUserName, msgText, lsMsgTo, curDt), sentByUserId, curDt, out lstReceipient);
				commit = true;
				successfullySend = true;
                try
                {
                    DoProcessForPushNewMessage(cmd, lstReceipient, msgText, sentByUserId);
                }
                catch (Exception)
                {
                    
                   
                }
			}
			catch (Exception ex)
			{
				LogMsg("SendMessage:Err:" + ex.Message);
				throw;
			}
			finally
			{
				if (isNewCmd)
				{
					db.CloseCommand(cmd, commit);
				}
			}
            if (successfullySend)  dbMsg.RefreshData();  

			return successfullySend;
		}

		public static string ComputeMsgReceipientAndType(string formattedReceipientId, out EnumReceipientType receipientType, out string receipientId)
		{
			receipientId = formattedReceipientId;
			receipientType = EnumReceipientType.Unknown;

			if (formattedReceipientId.StartsWith(CtrlApp.MSG_RECEIPIENT_PREFIX_FOR_FLIGHT))
			{
				receipientType = EnumReceipientType.Flight;
				receipientId = formattedReceipientId.Substring(CtrlApp.MSG_RECEIPIENT_PREFIX_FOR_FLIGHT.Length);
			}
			else if (formattedReceipientId.StartsWith(CtrlApp.MSG_RECEIPIENT_PREFIX_FOR_GROUP))
			{
				receipientType = EnumReceipientType.Group;
				receipientId = formattedReceipientId.Substring(CtrlApp.MSG_RECEIPIENT_PREFIX_FOR_GROUP.Length);
			}
			else
			{
				if (CtrlApp.IsGroupUser(formattedReceipientId)) receipientType = EnumReceipientType.Group;
				else receipientType = EnumReceipientType.Person;
			}
			return receipientId;
		}

		public static void UpdateMessageReceivedDetails(string msgToUrno, string staffId, string recvDateTime)
		{
			IDbCommand cmd = null;
			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			bool isNewCmd = false;//Is command 'cmd' created from this method?

			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = db.GetCommand(true);
				}

				DateTime curDt = ITrekTime.GetCurrentDateTime();
				DateTime dtRecv = curDt;

				if (!string.IsNullOrEmpty(recvDateTime)) dtRecv = UtilTime.ConvUfisTimeStringToDateTime(recvDateTime);

				dbMsg.ReceiveMessage(cmd, msgToUrno, staffId, dtRecv, staffId, curDt);
				commit = true;
			}
			catch (Exception ex)
			{
				LogMsg("UpdateMessageReceivedDetails:Err:" + ex.Message);
				throw;
			}
			finally
			{
				if (isNewCmd)
				{
					db.CloseCommand(cmd, commit);
				}
			}

			if (commit) dbMsg.RefreshData();
		}

		public static int GetUnReadInBoxMsgCount(EntUser user)
		{
			int msgCnt = 0;
			string staffId = user.UserId;
			ArrayList groupList = user.GetGroupList();

			try
			{
				msgCnt = dbMsg.GetUnReadInBoxMsgCount(staffId, groupList, CtrlConfig.GetMsgInboxTime());
			}
			catch (Exception ex)
			{
				LogMsg("GetUnReadInBoxMsgCount:Err:" + ex.Message);
				throw new ApplicationException("Unable to get the  Messages" + ex.Message);
			}
			return msgCnt;
		}

		public static void GetMsgInboxInfo(string msgToId, List<string> parrGroupList,
				int maxMsgCnt,
				out int totInMsgCnt, out int totUnReadInMsgCnt)
		{
			DSMsg dsMsg = null;
			ArrayList arr = null;
			totInMsgCnt = 0;
			totUnReadInMsgCnt = 0;

			if (parrGroupList != null)
			{
				arr = new ArrayList(parrGroupList);
			}

			dsMsg = CtrlMsg.RetrieveInMessages(msgToId, arr, maxMsgCnt);
			totInMsgCnt = dsMsg.MSGTO.Count;

			try
			{
				totUnReadInMsgCnt = dsMsg.MSGTO.Select("(RCV_DT IS NULL OR RCV_DT='')").Length;
			}
			catch (Exception)
			{
			}
		}

		public static DSMsgLog RertieveMsgLog(string sessionId,
			  string sentBy, string frUfisDateTime, string toUfisDateTime,
			  int startingIndex, int cnt, string orderBy)
		{
			IDbCommand cmd = null;
			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			bool isNewCmd = false;//Is command 'cmd' created from this method?
			DSMsgLog ds = new DSMsgLog();

			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = db.GetCommand(true);
				}

				ds = dbMsg.RetrieveMsgLog(cmd, sessionId, sentBy, frUfisDateTime, toUfisDateTime, startingIndex, cnt, orderBy);
			}
			catch (Exception ex)
			{
				LogMsg("RertieveMsgLog:Err:" + ex.Message);
				throw;
			}
			finally
			{
				if (isNewCmd)
				{
					db.CloseCommand(cmd, commit);
				}
			}
			return ds;
		}

		private static void LogMsg(string msg)
		{
			try
			{
				MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("CtrlMsg", msg);
			}
			catch (Exception)
			{
				UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("CtrlMsg", msg);
			}
		}
          private static void DoProcessForPushNewMessage(IDbCommand cmd,ArrayList alRec, string msgBody,string sentById)
        {
            string deviceId = "";
            string staffId = "";
            SessionManager sessMan = new SessionManager();
            if ((alRec != null) && (alRec.Count != 0))
            {
                foreach (string rec in alRec)
                {
                    try
                    {
                        if (rec == "AO")
                        {
                            DSCurUser dsCur = CtrlApp.GetInstance().GetCurrentLoginedAOs();
                            foreach (DSCurUser.LOGINUSERRow row in dsCur.LOGINUSER.Select(""))
                            {
                                deviceId = "";
                                staffId = "";
                                try
                                {
                                    staffId = row.UID.Trim();
                                }
                                catch (Exception)
                                {
                                    
                                }
                                try
                                {
                                    deviceId = row.DEVICE_ID.Trim();
                                }
                                catch (Exception)
                                {
                                    
                                    
                                }
                                pushMessage(cmd, staffId, deviceId, msgBody, sentById);
                            }
                        }
                        else
                            if (rec == "BO")
                            {
                                DSCurUser dsCur = CtrlApp.GetInstance().GetCurrentLoginedBOs();
                                foreach (DSCurUser.LOGINUSERRow row in dsCur.LOGINUSER.Select(""))
                                {
                                    deviceId = "";
                                    staffId = "";
                                    try
                                    {
                                        staffId = row.UID.Trim();
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    try
                                    {
                                        deviceId = row.DEVICE_ID.Trim();
                                    }
                                    catch (Exception)
                                    {


                                    }
                                    pushMessage(cmd, staffId, deviceId, msgBody, sentById);
                                }
                            }
                            else
                            {
                                DSCurUser dsCur = CtrlCurUser.RetrieveCurUserForStaffId(rec);
                                //deviceId = sessMan.GetDeviceIDByPENO(rec);
                                try
                                {
                                    deviceId = "";
                                    staffId = "";
                                    try
                                    {
                                        staffId = dsCur.LOGINUSER[0].UID.Trim();
                                    }
                                    catch (Exception)
                                    {

                                    }
                                    try
                                    {
                                        deviceId = dsCur.LOGINUSER[0].DEVICE_ID.Trim();
                                    }
                                    catch (Exception)
                                    {


                                    }

                                    pushMessage(cmd, staffId, deviceId, msgBody, sentById);
                                }
                                catch (Exception)
                                {

                                    LogMsg("Exception while getting DeviceId:");
                                }
                            }
                    }
                    catch (Exception pidEx)
                    {
                        LogMsg("Push New Message exception:" + pidEx.Message);
                    }
                }
            }
        }

        private static void pushMessage(IDbCommand cmd,string staffId,string deviceId, string msgBody,string sentById)
        {
            //XmlPushClient PushClient = new XmlPushClient(GetPapUrl());
           
                if (msgBody.Length > 10)
                {
                    msgBody = msgBody.Substring(0, 10);
                }
           
            
            try
            {
                //string PID = "";
                //string url = GetUrlForNetAlert()+ "login.aspx"; ;
                //LogMsg("url:" + url);
                string replyTo = "";
                if (deviceId != "")
                {
                    CMsgOut.NewMessageToSend(cmd, deviceId + "|" + staffId, "New Msg-" + msgBody, sentById, "ALERT", out replyTo);
                    // PID = PushClient.PushServiceIndication(deviceId, "New Msg-" + msgBody, "http://172.31.246.42/itrek/Messages.aspx?newmessage=alert");
                    //PID = PushClient.PushServiceIndication(deviceId, "New Msg-" + msgBody, url);
                }
                //LogMsg("PushId :" + PID + "-devicedId:" + deviceId);
            }
            catch (Exception ex)
            {
                LogMsg("pushMessage exception:" + ex.Message);
            }
        }
		//private static string  GetPapUrl()
		//{
		//    iTXMLPathscl paths = new iTXMLPathscl();
		//    return paths.GetPapURL();
		//}

		//private static string GetUrlForNetAlert()
		//{
		//    iTXMLPathscl paths = new iTXMLPathscl();
		//    return paths.GetURLForNetAlert();
		//}

        private string GetSessionIdForDeviceId(string deviceId)
        {
            string sessionId = "";
            try
            {
                sessionId = CtrlCurUser.RetrieveCurUserForDeviceId(deviceId).LOGINUSER[0].SESS_ID;
            }
            catch (Exception)
            {


            }

            return sessionId;

        }
	}
}