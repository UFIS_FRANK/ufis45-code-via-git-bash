using System;
using System.Collections.Generic;
using System.Text;

namespace UFIS.ITrekLib.DB
{
    public class DBTest
    {
        private static int _cnt = 0;
        private static DBTest _dbTest = null;
        private static object _lockObj = new object();

        private DBTest() { }

        public static DBTest GetInstance()
        {
            if (_dbTest == null)
            {
                _dbTest = new DBTest();
            }
            return _dbTest;
        }

        public int Cnt
        {
            get
            {
                return _cnt;
            }
            set
            {
                _cnt = value;
            }
        }

        public int IncreaseCnt()
        {
            lock (_lockObj)
            {
                _cnt++;
            }
            return Cnt;
        }

        public int DecreaseCnt()
        {
            lock (_lockObj)
            {
                Cnt--;
            }
            return Cnt;
        }
    }
}
