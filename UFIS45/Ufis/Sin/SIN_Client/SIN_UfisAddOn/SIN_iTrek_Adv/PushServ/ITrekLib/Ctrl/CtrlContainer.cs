using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Data;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.DB;
using iTrekXML;
using iTrekSessionMgr;
using System.Xml;

namespace UFIS.ITrekLib.Ctrl
{
    public class CtrlContainer
    {
        public static DSContainer RetrieveUldNoNotSend(string uaft)
        {

            DBContainer dbContainer = DBContainer.GetInstance();

            DSContainer dsContainer = new DSContainer();

            try
            {
                dsContainer = ((DSContainer)dbContainer.RetrieveUldNoNotSend(uaft));
            }
            catch (Exception )
            {
               //throw new ApplicationException("Unable to get the  Messages" + ex.Message);
            }
            return dsContainer;
        }


       public static DSContainer RetrieveUldNos(string uaft)
       {

           DBContainer dbContainer = DBContainer.GetInstance();

           DSContainer dsContainer = new DSContainer();

           try
           {
               dsContainer = ((DSContainer)dbContainer.RetrieveUldNos(uaft));
           }
           catch (Exception)
           {
               //throw new ApplicationException("Unable to get the  Messages" + ex.Message);
           }
           return dsContainer;
       }
        public static void UpdateUldSendIndicator(string uaft, ArrayList lstUldNo)
        {

            DBContainer dbContainer = DBContainer.GetInstance();


            try
            {
                dbContainer.UpdateUldSendIndicator(uaft, lstUldNo);
            }
            catch (Exception )
            {
               // throw new ApplicationException("Unable to get the  Messages" + ex.Message);
            }
            
        }









        public static void deleteOldContainers(ArrayList lstContainers)
        {
            DBContainer.GetInstance().deleteOldContainers(lstContainers);



        }

        //public static void deleteAContainer(string uaft,string uldNo)
        //{
        //    DBContainer.GetInstance().deleteAContainer(uaft,uldNo);



        //}

        public static void createNewContainerIntoContainerXml(string uaft,ArrayList lstUldNo)
        {
            DBContainer dbContainer = DBContainer.GetInstance();
           
            try
            {
                dbContainer.createNewContainerIntoContainerXml(uaft, lstUldNo);
            }
            catch (Exception)
            {
                
                //throw new ApplicationException("Unable to get the  Messages" + ex.Message);
            }
          
        }

        public static void UpdateBRSInfoFromBackendToContainerXmlFile(XmlDocument xmlDoc)
        {
            DBContainer.GetInstance().UpdateBRSInfoFromBackendToContainerXmlFile(xmlDoc);
            // DBCpm.GetInstance().UpdateSentIndicator(xmlDoc);

        }
        public static void UpdateBRSFieldsToContainerXmlFile(XmlDocument xmlDoc)
        {
            DBContainer.GetInstance().UpdateBRSFieldsToContainerXmlFile(xmlDoc);
            // DBCpm.GetInstance().UpdateSentIndicator(xmlDoc);

        }
       

        
    }
}
