#define TESTING_ON
#define TRACING_ON
#define LOGGING_ON
#define USING_MQ


using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Diagnostics;
using System.Collections;

using System.Data;
using System.IO;

using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.DSDB;
//using iTrekXML;
using UFIS.ITrekLib.Util;
//using iTrekWMQ;
using MM.UtilLib;
using UFIS.ITrekLib.Config;

namespace UFIS.ITrekLib.DB
{
	public class DBDAsr
	{
		public enum EnumAsrStatus { NoAsr, Draft, Submit, Lock, Unknown };

		private static Object _lockObj = new Object();//to use in synchronisation of single access
		private static Object _lockObjHt = new Object();//to use in synchronisation of single access
		private static DBDAsr _dbAsr = null;
		private static DateTime lastWriteDT = new System.DateTime(1, 1, 1);
		private static DateTime _lastRefreshDT = new DateTime(1, 1, 1);

		private static DSApronServiceRpt _dsApronServiceReport = null;
		private static string _fnApronServiceReportXml = null;//file name of ASR XML in full path.

		/// <summary>
		/// Singleton Pattern.
		/// Not allowed to create the object from other class.
		/// To get the object, user "GetInstance()" method.
		/// </summary>
		private DBDAsr() { }

		public void Init()
		{//Initialize all the information, to get the latest data when fetching them again.
			_dsApronServiceReport = null;
			_fnApronServiceReportXml = null;
			lastWriteDT = new System.DateTime(1, 1, 1);
			_lastRefreshDT = new DateTime(1, 1, 1);
		}

		/// <summary>
		/// Get the Instance of DBDAsr. There will be only one reference for all the clients
		/// </summary>
		/// <returns></returns>
		public static DBDAsr GetInstance()
		{
			//if (_dbAsr == null)
			//{
			//   lock (_lockObj)
			//   {
			//      if (_dbAsr == null)
			//      {
			//         DBDAsr temp = new DBDAsr();
			//         System.Threading.Thread.MemoryBarrier();
			//         DBDAsr._dbAsr = temp;
			//      }
			//   }
			//}
			return Nested.instance;
		}

		class Nested
		{
			// Explicit static constructor to tell C# compiler
			// not to mark type as beforefieldinit
			static Nested()
			{
			}

			internal static readonly DBDAsr instance = new DBDAsr();
		}

		//public DSApronServiceRpt RetrieveApronServiceReportForAFlight(string flightUrNo)
		//{
		//    DSApronServiceRpt ds = new DSApronServiceRpt();

		//    foreach (DSApronServiceRpt.ASRRow row in dsApronServiceReport.ASR.Select("URNO='" + flightUrNo + "'"))
		//    {
		//        ds.ASR.LoadDataRow(row.ItemArray, true);
		//    }

		//    return ds;
		//}

		//private string fnApronServiceReportXml
		//{
		//    get
		//    {
		//        if ((_fnApronServiceReportXml == null) || (_fnApronServiceReportXml == ""))
		//        {
		//            iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
		//            _fnApronServiceReportXml = path.GetApronServiceReportFilePath();
		//        }

		//        return _fnApronServiceReportXml;
		//    }
		//}

		//private DSApronServiceRpt dsApronServiceReport
		//{
		//    get
		//    {
		//        int timeElapse = Math.Abs(UtilTime.DiffInSec(DateTime.Now, _lastRefreshDT));
		//        if ((_dsApronServiceReport == null) || (timeElapse > 3))
		//        {
		//            LoadDSApronServiceRpt();
		//            _lastRefreshDT = DateTime.Now;
		//        }
		//        DSApronServiceRpt ds = (DSApronServiceRpt)_dsApronServiceReport.Copy();

		//        return ds;
		//    }
		//}

		//public bool IsApronServiceRptExist(string flightId, out bool draft)
		//{
		//    bool exist = false;
		//    draft = false;
		//    exist = IsASRExist(flightId, out draft);
		//    //DSApronServiceRpt dsTemp = dsApronServiceReport;
		//    //if (dsTemp != null)
		//    //{
		//    //    foreach (DSApronServiceRpt.ASRRow row in dsApronServiceReport.ASR.Select("URNO='" + flightId + "'"))
		//    //    {
		//    //        exist = true;
		//    //        if ((row.DRAFT != null) && (row.DRAFT == "Y")) { draft = true; }
		//    //        else draft = false;
		//    //    }
		//    //}
		//    return exist;
		//}

		//public bool IsApronServiceRptExist(string flightId)
		//{
		//    bool exist = false;
		//    bool draft;
		//    exist = IsASRExist(flightId, out draft);

		//    //DSApronServiceRpt dsTemp = dsApronServiceReport;
		//    //if (dsTemp != null)
		//    //{
		//    //    exist = dsTemp.ASR.Select("URNO='" + flightId + "'").Length>0;
		//    //}
		//    return exist;
		//}

		//public bool IsDraft(string flightId)
		//{
		//    bool draft = false;
		//    bool exist = IsASRExist(flightId, out draft);
		//    //DSApronServiceRpt dsTemp = dsApronServiceReport;
		//    //if (dsTemp != null)
		//    //{
		//    //    foreach (DSApronServiceRpt.ASRRow row in dsApronServiceReport.ASR.Select("URNO='" + flightId + "'"))
		//    //    {
		//    //        if ((row.DRAFT != null) && (row.DRAFT == "Y")) { draft = true; } 
		//    //    }
		//    //}
		//    return draft;
		//}

		private const string V_ITK_ASR_SEL_COLS = "URNO,FLNO,AD,AOID,AOFNAME,AOLNAME,ACFTREGN,ACFTTYPE,ORG3 \"FR\"" +
													",DES3 \"TO\",BAY,PLBA,PLBB,JCLFWD,JCLAFT,SLFWD,SLAFT,TRNO,FLMO \"MODE\"" +
				@",LOAD,FTRIP,LTRIP,UNLEND,LDSTRT,LDEND,
				LSTDOR,PLB,CBRIEF,CSAFE,CCONE,
				CLOW,CEXT,CBRTEST,CDMGBDY,CDMGDG,CDG,
				CULD,OICAO,OICAO_D,OBRK,OBRK_D,
				OCPM,OCPM_D,OBG,OBG_D,OWETH,
				OWETH_D,OLGHT,OLGHT_D,OBAY,OBAY_D,
				OOVR,OOVR_D,OFOD,OFOD_D,OILS,
				OILS_D,OJAM,OJAM_D,OGSE,OGSE_D,
				OWARP,OWARP_D,ODNG,ODNG_D,OBLK,
				OBLK_D,ULDCMT,GENCMT,RSNBPT,DRO,
				DM,DRAFT,STD,ATD,STA,
				ATA, ST,""AT"",TURN";

//        private const string V_ITK_ASR_SEL_COLS2 = "URNO,FLNO,AD,AOID,AOFNAME,AOLNAME,ACFTREGN,ACFTTYPE,ORG3 \"FR\"" +		
//                                                    ",DES3 \"TO\",BAY,PLBA,PLBB,JCLFWD,JCLAFT,SLFWD,SLAFT,TRNO,FLMO \"MODE\"" ;
//                @",LOAD,FTRIP,LTRIP,UNLEND,LDSTRT,LDEND,
//				LSTDOR,PLB,CBRIEF,CSAFE,CCONE,
//				CLOW,CEXT,CBRTEST,CDMGBDY,CDMGDG,CDG,
//				CULD,OICAO,OICAO_D,OBRK,OBRK_D,
//				OCPM,OCPM_D,OBG,OBG_D,OWETH,
//				OWETH_D,OLGHT,OLGHT_D,OBAY,OBAY_D,
//				OOVR,OOVR_D,OFOD,OFOD_D,OILS,
//				OILS_D,OJAM,OJAM_D,OGSE,OGSE_D,
//				OWARP,OWARP_D,ODNG,ODNG_D,OBLK,
//				OBLK_D,DRO,
//				DM,DRAFT,STD,ATD,STA,
//				ATA, ST,""AT"",TURN";
//         * */

		public static bool IsAsrExist(IDbCommand cmd, string flightId, out EnumAsrStatus stat)
		{
			bool exist = false;
			stat = EnumAsrStatus.Unknown;

			bool isNewCmd = false;
			UtilDb udb = UtilDb.GetInstance();

			try
			{
				if (cmd == null)
				{
					isNewCmd = true;
					cmd = udb.GetCommand(false);
				}

				if (UtilDb.IsRecordExist(cmd, DB_Info.DB_T_ASR, flightId))
				{
					exist = true;
					object obj = UtilDb.GetDataForAColumn(cmd, DB_Info.DB_T_ASR, flightId, "PSTA");
					if ((obj == null) || (obj == DBNull.Value))
					{
						stat = EnumAsrStatus.NoAsr;
					}
					else
					{
						string stStat = (string)obj;
						switch (stStat.Trim().ToUpper())
						{
							case "D": stat = EnumAsrStatus.Draft;
								break;
							case "S": stat = EnumAsrStatus.Submit;
								break;
						}
					}
				}
				else
				{
					stat = EnumAsrStatus.NoAsr;
				}
			}
			catch (Exception)
			{

				throw;
			}
			finally
			{
				if (isNewCmd)
				{
					udb.CloseCommand(cmd, false);
				}
			}
			return exist;
		}

		/// <summary>
		/// Retrieve Apron Service Report (ASR). Return true when successfully load (It does not mean ASR is already existed).
		/// </summary>
		/// <param name="cmd"></param>
		/// <param name="flightId"></param>
		/// <param name="ds"></param>
		/// <returns></returns>
		public static bool RetrieveDsApronServiceRpt(IDbCommand cmd, string flightId, ref DSApronServiceRpt ds)
		{
			bool loadSuccess = false;
			UtilDb db = UtilDb.GetInstance();
			bool isCmdNull = false;
			try
			{
				if (cmd == null)
				{
					isCmdNull = true;
					cmd = db.GetCommand(false);
				}
				DateTime dt1 = DateTime.Now;

				if (ds == null) ds = new DSApronServiceRpt();

				cmd.Parameters.Clear();

				DateTime now = Misc.ITrekTime.GetCurrentDateTime().Date;
				cmd.CommandText = string.Format("SELECT {0} FROM {1} WHERE URNO={2}",
					V_ITK_ASR_SEL_COLS,
					GetDb_Vw_Asr(),
					db.PrepareParam(cmd, "URNO", flightId)
					);
				//LogTraceMsg("RetrieveDsApronServiceRpt:flid:" + flightId + ",Cmd:" + cmd.CommandText );
				LogTraceMsg("RetrieveDsApronServiceRpt:flid:" + flightId);
				cmd.CommandType = CommandType.Text;

				db.FillDataTable(cmd, ds.ASR);

				DateTime dt2 = DateTime.Now;
				LogTraceMsg("RetrieveDsApronServiceRpt:Load ASR Data Time:" + UtilTime.ElapsedTime(dt1, dt2));

				if ((ds != null) && (ds.ASR.Rows.Count > 0))
				{
					loadSuccess = true;
				}
				else
				{
					LogTraceMsg("RetrieveDsApronServiceRpt:No Flight Data.");
				}
			}
			catch (Exception ex)
			{
				LogMsg("RetrieveDsApronServiceRpt:Err:" + ex.Message);
				throw;
			}
			finally
			{
				if (isCmdNull)
				{
					db.CloseCommand(cmd, false);
					cmd = null;
				}
			}
			return loadSuccess;
		}


		public static bool RetrieveDsApronServiceRpt2(IDbCommand cmd, string flightId, string stSelCmd, ref DSApronServiceRpt ds)
		{
			bool loadSuccess = false;
			UtilDb db = UtilDb.GetInstance();
			bool isCmdNull = false;
			try
			{
				if (cmd == null)
				{
					isCmdNull = true;
					cmd = db.GetCommand(false);
				}
				DateTime dt1 = DateTime.Now;

				if (ds == null) ds = new DSApronServiceRpt();

				cmd.Parameters.Clear();

				DateTime now = Misc.ITrekTime.GetCurrentDateTime().Date;
				cmd.CommandText = string.Format("SELECT {0} FROM {1} WHERE URNO={2}",
					//V_ITK_ASR_SEL_COLS2,
					stSelCmd,
					GetDb_Vw_Asr(),
					db.PrepareParam(cmd, "URNO", flightId)
					);
				LogTraceMsg("RetrieveDsApronServiceRpt:flid:" + flightId + ",Cmd:" + cmd.CommandText);
				cmd.CommandType = CommandType.Text;

				db.FillDataTable(cmd, ds.ASR);

				DateTime dt2 = DateTime.Now;
				LogMsg("RetrieveDsApronServiceRpt:Load ASR Data Time:" + UtilTime.ElapsedTime(dt1, dt2));

				if ((ds != null) && (ds.ASR.Rows.Count > 0))
				{
					loadSuccess = true;
				}
				else
				{
					LogMsg("RetrieveDsApronServiceRpt:No Flight Data.");
				}
			}
			catch (Exception ex)
			{
				LogMsg("RetrieveDsApronServiceRpt:Err:" + ex.Message);
				throw;
			}
			finally
			{
				if (isCmdNull)
				{
					db.CloseCommand(cmd, false);
					cmd = null;
				}
			}
			return loadSuccess;
		}

		private static Hashtable _htASRExist = new Hashtable();

		public bool IsConfirmASRExist(IDbCommand cmd, string flightId)
		{
			bool exist = false;
			if (_htASRExist.Contains(flightId))
			{
				exist = true;
			}
			else
			{
				EnumAsrStatus stat = EnumAsrStatus.Unknown;
				if (IsAsrExist(cmd, flightId, out stat))
				{
					if ( (stat == EnumAsrStatus.Submit) || 
						(stat == EnumAsrStatus.Lock))
					{
						_htASRExist.Add(flightId, stat);
						exist = true;
					}
				}
			}
			return exist;
		}

		private void CreateASRExistTable()
		{
			try
			{
				Hashtable ht = new Hashtable();
				DSApronServiceRpt dsTemp = new DSApronServiceRpt();
				dsTemp.ASR.Merge(_dsApronServiceReport.ASR);
				foreach (DSApronServiceRpt.ASRRow row in dsTemp.ASR)
				{
					bool draft = false;
					if ((row.DRAFT != null) && (row.DRAFT == "Y")) { draft = true; }

					ht.Add(row.URNO, draft);
				}
				_htASRExist = ht;
			}
			catch (Exception ex)
			{
				LogMsg("CreateASRExistTable:Err:" + ex.Message);
			}
		}

		public static void SaveRpt(IDbCommand cmd, string flightId, DSApronServiceRpt dsToSave, string userId)
		{
			if (cmd == null) throw new ApplicationException("SaveRpt:No DB Command to update.");
			LogTraceMsg("SaveRpt:1");
			DsDbAsr dsDbAsr = ConvDsToDb(cmd, dsToSave, flightId);
			LogTraceMsg("SaveRpt:2");
			List<string> lsFlightIds = new List<string>();
			lsFlightIds.Add(flightId );
			DateTime updDt = Misc.ITrekTime.GetCurrentDateTime();
			LogTraceMsg("SaveRpt:3");
			DsSaveData_Asr(cmd, dsDbAsr, lsFlightIds, userId, updDt);
			LogTraceMsg("SaveRpt:4");
			DsSaveData_AsrOth(cmd, dsDbAsr, lsFlightIds, userId, updDt);
			LogTraceMsg("SaveRpt:5");
		}


		#region Logging
		private static void LogMsg(string msg)
		{
			try
			{
				MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("DBDAsr", msg);
			}
			catch (Exception)
			{
				UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DBDAsr", msg);
			}

		}

		private static void LogTraceMsg(string msg)
		{
			LogMsg(msg);
		}
		#endregion

		private const int MAX_RMK_LEN = 3999;
		public static DsDbAsr ConvDsToDb(IDbCommand cmd, DSApronServiceRpt dsAsr, string flightId)
		{
			if (dsAsr == null) throw new ApplicationException("Nothing to save.");
			DsDbAsr dsDbAsr = DbDSdfAsrOth.GetInstance().GetDsDbAsr(cmd);
			dsDbAsr.ITK_ASR.Clear();
			dsDbAsr.ITK_ASR_OTH.Clear();
			int othCnt = 0;
			int asrCount = 0;
			//LogTraceMsg("ConvDsToDb:1");
			foreach (DSApronServiceRpt.ASRRow row in dsAsr.ASR.Select("URNO='" + flightId + "'"))
			{
				if ((row.ULDCMT != null) && (row.ULDCMT.Length > MAX_RMK_LEN)) row.ULDCMT = row.ULDCMT.Substring(0, MAX_RMK_LEN);
				if ((row.GENCMT != null) && (row.GENCMT.Length > MAX_RMK_LEN) ) row.GENCMT = row.GENCMT.Substring(0, MAX_RMK_LEN);
				if ((row.RSNBPT != null) && (row.RSNBPT.Length > MAX_RMK_LEN)) row.RSNBPT = row.RSNBPT.Substring(0, MAX_RMK_LEN);

				//LogTraceMsg("ConvDsToDb:2");
				asrCount++;
				DsDbAsr.ITK_ASRRow asrDbRow = dsDbAsr.ITK_ASR.NewITK_ASRRow();
				asrDbRow.URNO = flightId;
				dsDbAsr.ITK_ASR.AddITK_ASRRow(asrDbRow);
				//LogTraceMsg("ConvDsToDb:3");
				foreach (DataColumn col in dsAsr.ASR.Columns)
				{
					if (UtilDataSet.IsExpressionColumn( col )) continue;
					if (col.ColumnName == "DRAFT")
					{
						string stDraft = (string) row[col.ColumnName];
						string stPsta = "S";
						if (stDraft == "Y") stPsta = "D";
						asrDbRow["PSTA"] = stPsta ;
						continue;
					}

					//LogTraceMsg("ConvDsToDb:4" + col.ColumnName);
					bool isChkCol = true;
					string sdfOthId = "";
					if (IsOthCol(col.ColumnName, out isChkCol, out sdfOthId))
					{
						//LogTraceMsg("ConvDsToDb:5");
						DsDbAsr.ITK_ASR_OTHRow othRow;
						DsDbAsr.ITK_ASR_OTHRow[] othRows = (DsDbAsr.ITK_ASR_OTHRow[])
							dsDbAsr.ITK_ASR_OTH.Select("FLID='" + flightId + "' AND OTID='" + sdfOthId + "'");
						if ((othRows == null) || (othRows.Length < 1))
						{
							othRow = dsDbAsr.ITK_ASR_OTH.NewITK_ASR_OTHRow();
							othRow.FLID = flightId;
							othRow.OTID = sdfOthId;
							othRow.URNO = UtilDb.PFIX_VIRTUAL_NEW_ID + othCnt++;
							dsDbAsr.ITK_ASR_OTH.AddITK_ASR_OTHRow(othRow);
						}
						else
						{
							othRow = othRows[0];
						}

						if (isChkCol)
						{							
							othRow.CHEK =(string) row[col.ColumnName];
						}
						else
						{
							othRow.REMK = (string) row[col.ColumnName];
						}
					}
					else
					{
						//LogTraceMsg("ConvDsToDb:6");
						AssignDsToDb(row, asrDbRow, col.ColumnName);
						//LogTraceMsg("ConvDsToDb:7");
					}
				}
			}
			if (asrCount < 1) throw new ApplicationException("No ASR info for flight " + flightId);
			return dsDbAsr;
		}

		private static bool IsOthCol(string colNameInDsAsr,out bool isChk, out string sdfOthId)
		{
			isChk = false;
			
			colNameInDsAsr = colNameInDsAsr.Trim().ToUpper();
			if (colNameInDsAsr.EndsWith("_D"))
			{
				colNameInDsAsr = colNameInDsAsr.Substring(0, colNameInDsAsr.Length - 2);
			}
			else isChk = true;

			bool isOth = false;

			isOth = DbDSdfAsrOth.GetIdForDsApronServiceReportCol(colNameInDsAsr, out sdfOthId);

			return isOth;
		}

		private static bool AssignDsToDb(DSApronServiceRpt.ASRRow row, DsDbAsr.ITK_ASRRow asrDbRow, string colNameInDs)
		{
			bool isColToUpd = false;
			object obj = row[colNameInDs];
			if (obj==null) return false;
			if (!string.IsNullOrEmpty(colNameInDs))
			{
				string colNameInDb = "";
				if (ITrekColMapping.IsDbAsrColNameForDsColVP(colNameInDs, out colNameInDb))
				{
					string st = (string)obj;
					char[] del = { '|' };
					string[] vals = st.Split(del);
					asrDbRow[colNameInDb + "_V"] = vals[0];
					if (vals.Length > 1) asrDbRow[colNameInDb + "_P"] = vals[1];
				}
				else
				{
					colNameInDb = ITrekColMapping.GetDbAsrColNameForDsCol(colNameInDs);
					if (!string.IsNullOrEmpty(colNameInDb))
					{
						asrDbRow[colNameInDb] = row[colNameInDs];
						isColToUpd = true;
					}
					else
					{
						//LogTraceMsg("AssignDsToDb:No Mapping:" + colNameInDs);
					}
				}
			}
			return isColToUpd;
		}

		public static bool GetDataForOthCol(DsDbAsr dsAsr, string flightId, string colName, out string chk, out string remk)
		{
			bool hasData = false;
			chk = "N";
			remk = "";

			string stSdfOthId = "";
			if (!DbDSdfAsrOth.GetIdForDsApronServiceReportCol(colName, out stSdfOthId))
			{
				throw new ApplicationException("Column Name not found for " + colName);
			}

			DsDbAsr.ITK_ASR_OTHRow[] rows = (DsDbAsr.ITK_ASR_OTHRow[])
			   dsAsr.ITK_ASR_OTH.Select("FLID='" + flightId + "' AND OTID='" + stSdfOthId + "'");
			if ((rows == null) || (rows.Length < 1))
			{//No data.
			}
			else
			{
				if (rows[0].CHEK == "Y") chk = "Y";
				remk = rows[0].REMK;
				hasData = true;
			}

			return hasData;
		}

		public static void SetDataForOthCol(DsDbAsr dsAsr, string flightId, string colName, string chk, string remk)
		{
			int cnt = 0;
			string stSdfOthId = "";
			if (!DbDSdfAsrOth.GetIdForDsApronServiceReportCol(colName, out stSdfOthId))
			{
				throw new ApplicationException("Column Name not found for " + colName);
			}

			DsDbAsr.ITK_ASR_OTHRow[] rows = (DsDbAsr.ITK_ASR_OTHRow[])
			   dsAsr.ITK_ASR_OTH.Select("FLID='" + flightId + "' AND OTID='" + stSdfOthId + "'");

			if ((rows == null) || (rows.Length < 1))
			{//New data
				DsDbAsr.ITK_ASR_OTHRow row = dsAsr.ITK_ASR_OTH.NewITK_ASR_OTHRow();
				row.URNO = UtilDb.PFIX_VIRTUAL_NEW_ID + cnt;
				cnt++;
				row.CHEK = (chk == "Y") ? "Y" : "N";
				row.REMK = remk;
				row.FLID = flightId;
				row.OTID = stSdfOthId;
				dsAsr.ITK_ASR_OTH.AddITK_ASR_OTHRow(row);
			}
			else
			{//Existing Data
				rows[0].CHEK = (chk == "Y") ? "Y" : "N";
				rows[0].REMK = remk;
			}
		}

		#region Database Operation for ITK_ASR with DSApronServiceRpt
		#region DbAsr_Insert
		//private void DbAsr_Insert(IDbCommand cmd,
		//   string flightId, DSApronServiceRpt.ASRRow row,
		//   string userId, DateTime updateDateTime)
		//{
		//    StringBuilder sb = new StringBuilder();
		//    StringBuilder sbDbColNames = new StringBuilder();
		//    UtilDb udb = UtilDb.GetInstance();

		//    sbDbColNames.Append(@"URNO,FLMO,LOAD,CBRF,CSAF,CCON,CLOW,CEXT,CBRT,CDBY,CDDG,CULD,RULD,RGEN,RRBT,DROI,DMNI,PSTA,SBON,LKON,LKBY,CDAT,USEC,LSTU,USEU");
		//    cmd.Parameters.Clear();

		//    sb.Append(udb.PrepareParam(cmd, "URNO", flightId));
		//    sb.Append("," + udb.PrepareParam(cmd, "FLMO", row.MODE));
		//    sb.Append("," + udb.PrepareParam(cmd, "LOAD", row.LOAD));
		//    sb.Append("," + udb.PrepareParam(cmd, "CBRF", row.CBRIEF));
		//    sb.Append("," + udb.PrepareParam(cmd, "CSAF", row.CSAFE));
		//    sb.Append("," + udb.PrepareParam(cmd, "CCON", row.CCONE));
		//    sb.Append("," + udb.PrepareParam(cmd, "CLOW", row.CLOW));
		//    sb.Append("," + udb.PrepareParam(cmd, "CEXT", row.CEXT));
		//    sb.Append("," + udb.PrepareParam(cmd, "CDBY", row.CDMGBDY));
		//    sb.Append("," + udb.PrepareParam(cmd, "CDDG", row.CDG));
		//    sb.Append("," + udb.PrepareParam(cmd, "CULD", row.CULD));
		//    sb.Append("," + udb.PrepareParam(cmd, "RULD", row.ULDCMT));
		//    sb.Append("," + udb.PrepareParam(cmd, "RGEN", row.GENCMT));
		//    sb.Append("," + udb.PrepareParam(cmd, "RRBT", row.RSNBPT));
		//    sb.Append("," + udb.PrepareParam(cmd, "DROI", row.DRO));
		//    sb.Append("," + udb.PrepareParam(cmd, "DMNI", row.DM));
		//    string stPsta = "D";//Draft
		//    if (!string.IsNullOrEmpty(row.DRAFT))
		//    {
		//        if (row.DRAFT.Trim().ToUpper() == "N") stPsta = "S";
		//    }
		//    sb.Append("," + udb.PrepareParam(cmd, "PSTA", stPsta));
		//    sb.Append("," + udb.PrepareParamForTime(cmd, "LKON", null));
		//    sb.Append("," + udb.PrepareParam(cmd, "LKBY", null));

		//    sb.Append("," + udb.PrepareParam(cmd, "CDAT", updateDateTime));
		//    sb.Append("," + udb.PrepareParam(cmd, "USEC", userId));
		//    sb.Append("," + udb.PrepareParam(cmd, "LSTU", updateDateTime));
		//    sb.Append("," + udb.PrepareParam(cmd, "USEU", userId));

		//    string stCmd = string.Format(@"INSERT INTO {0} ({1}) VALUES ({2})",
		//       GetDb_Tb_Asr(), sbDbColNames.ToString(), sb.ToString());

		//    if (stCmd != cmd.CommandText)
		//    {
		//        cmd.CommandText = stCmd;
		//        cmd.CommandType = CommandType.Text;
		//    }

		//    int updCnt = cmd.ExecuteNonQuery();
		//    if (updCnt < 1) throw new ApplicationException("ASR Creation Fail!!!");
		//}
		#endregion

		#region AssignDataFromDsAsrToDsDb
		//public static void AssignDataFromDsAsrToDsDb(DSApronServiceRpt dsAsr,
		//   string flightId, DSDB.DsDbAsr dsDbAsr,
		//   string userId, DateTime updateDateTime)
		//{
		//    DSApronServiceRpt.ASRRow[] rows = (DSApronServiceRpt.ASRRow[])dsAsr.ASR.Select("URNO='" + flightId + "'");
		//    if ((rows == null) || (rows.Length < 1))
		//    {
		//        throw new ApplicationException("No ASR information to update for the flight.");
		//    }

		//    string stUpdDateTime = UtilTime.ConvDateTimeToUfisFullDTString(updateDateTime);

		//    DSApronServiceRpt.ASRRow row = rows[0];

		//    #region Assign Data to ITK_ASR in Dataset DsDbAsr
		//    DSDB.DsDbAsr.ITK_ASRRow[] dbRows = (DSDB.DsDbAsr.ITK_ASRRow[])dsDbAsr.ITK_ASR.Select("URNO='" + flightId + "'");
		//    DSDB.DsDbAsr.ITK_ASRRow dbRow = null;
		//    bool isNewAsrRow = false;
		//    if ((dbRows == null) || dbRows.Length < 1)
		//    {
		//        isNewAsrRow = true;
		//        dbRow = dsDbAsr.ITK_ASR.NewITK_ASRRow();
		//        dbRow.CDAT = updateDateTime;
		//        dbRow.USEC = userId;
		//        dbRow.URNO = flightId;
		//    }
		//    else { dbRow = dbRows[0]; }

		//    UtilDataSet.AssignData(row.CBRIEF, dbRow, "CBRF");
		//    UtilDataSet.AssignData(row.CBRTEST, dbRow, "CBRT");
		//    UtilDataSet.AssignData(row.CCONE, dbRow, "CCON");
		//    UtilDataSet.AssignData(row.CDMGBDY, dbRow, "CDBY");
		//    UtilDataSet.AssignData(row.CDG, dbRow, "CDDG");
		//    UtilDataSet.AssignData(row.CEXT, dbRow, "CEXT");
		//    UtilDataSet.AssignData(row.CLOW, dbRow, "CLOW");
		//    UtilDataSet.AssignData(row.CSAFE, dbRow, "CSAF");
		//    UtilDataSet.AssignData(row.DM, dbRow, "DMNI");
		//    UtilDataSet.AssignData(row.DRO, dbRow, "DROI");
		//    UtilDataSet.AssignData(row.MODE, dbRow, "FLMO");
		//    UtilDataSet.AssignData(row.LOAD, dbRow, "LOAD");
		//    UtilDataSet.AssignData("", dbRow, "LKBY");
		//    UtilDataSet.AssignData(null, dbRow, "LKON");

		//    string stPsta = "D";//Draft
		//    if (!string.IsNullOrEmpty(row.DRAFT))
		//    {
		//        if (row.DRAFT.Trim().ToUpper() == "N")
		//        {
		//            stPsta = "S";
		//            dbRow.SBON = updateDateTime;
		//        }
		//    }

		//    UtilDataSet.AssignData(stPsta, dbRow, "PSTA");
		//    UtilDataSet.AssignData(row.GENCMT, dbRow, "RGEN");
		//    UtilDataSet.AssignData(row.RSNBPT, dbRow, "RRBT");
		//    UtilDataSet.AssignData(row.ULDCMT, dbRow, "RULD");

		//    UtilDataSet.AssignData(userId, dbRow, "USEU");
		//    UtilDataSet.AssignDateTimeData(stUpdDateTime, dbRow, "LSTU");


		//    if (isNewAsrRow)
		//    {
		//        dsDbAsr.ITK_ASR.AddITK_ASRRow(dbRow);
		//    }

		//    #endregion

		//    #region Assign Data to ITK_ASR_OTH in Dataset DsDbAsr
		//    #endregion
		//}
		#endregion


		//public const string DB_ASR = "ITK_ASR";
		public const string FIELDS_ASR_INSERT = "URNO,FLMO,LOAD,CBRF,CSAF,CCON,CLOW,CEXT,CBRT,CDBY,CDDG,CULD,RULD,RGEN,RRBT,DROI,DMNI,PSTA,SBON,LKON,LKBY,CDAT,USEC,LSTU,USEU";
		//public const string DB_ASR_OTH = "ITK_ASR_OTH";
		public const string DB_ASR_OTH_INSERT = "URNO,FLID,OTID,CHEK,REMK,STAT,CDAT,USEC,LSTU,USEU";		

		public bool IsAsrExist(IDbCommand cmd, string flightId)
		{
			return UtilDb.IsRecordExist(cmd, DB_Info.DB_T_ASR, flightId);
		}


		#endregion

		#region Database Operation for ITK_ASR with DsDbAsr
		private static string GetDb_Tb_Asr()
		{
			return DB_Info.GetDbFullName(DB_Info.DB_T_ASR);
		}

		private static string GetDb_Tb_AsrOth()
		{
			return DB_Info.GetDbFullName(DB_Info.DB_T_ASR_OTH);
		}

		private static string GetDb_Vw_Asr()
		{
			return DB_Info.GetDbFullName(DB_Info.DB_V_ASR);
		}

		#region ASR Const and variables
		private const string PROC_ID_FOR_ASR = "";

		private const string ASR_STAT_NEW = "N";
		private const string ASR_STAT_DRAFT = "D";
		private const string ASR_STAT_SUBMIT = "S";

		private static List<string> _lsAsrColNameToAssignData = null;
		private static object _lockObjForAsr = new object();
		#endregion

		public static int DbLoadData_Asr(IDbCommand cmd,
		   DsDbAsr dsAsr, string flightId, bool prepareDataWhenNoRecord, out bool hasRecord)
		{
			int cnt = -1;
			hasRecord = false;
			if (string.IsNullOrEmpty(flightId))
			{
				throw new ApplicationException("No Flight Id to load the ASR information.");
			}
			List<string> flightIds = new List<string>();
			flightIds.Add(flightId);

			cnt = DbLoadData_Asr(cmd, dsAsr, flightIds);
			if (cnt < 1)
			{
				if (prepareDataWhenNoRecord)
					PrepareAsrForAFlight(dsAsr, flightId);
			}
			else
			{
				hasRecord = true;
			}
			return cnt;
		}


		public static int DbLoadData_Asr(IDbCommand cmd,
		   DsDbAsr dsAsr, List<string> lsFlightIds)
		{
			string stFlightIds = UtilMisc.ConvListToString(lsFlightIds,
	  ",", "'", "'", 0, lsFlightIds.Count);
			cmd.CommandText = string.Format("SELECT {0} FROM {1} WHERE URNO IN ({2})",
			   //"URNO, FLMO, LOAD, CBRF, CSAF, CCON, CLOW, CEXT, CBRT, CDBY, CDDG, CDGL, CULD, RULD, RGEN, RRBT, DROI, DMNI, PSTA, ITK_GET_DATE(SBON) SBON, ITK_GET_DATE(LKON) LKON, LKBY, USEC, CDAT, USEU, LSTU",
			   "*",
			   GetDb_Tb_Asr(),
			   stFlightIds);
			cmd.CommandType = CommandType.Text;
			cmd.Parameters.Clear();

			UtilDb udb = UtilDb.GetInstance();
			dsAsr.ITK_ASR.Clear();
			return udb.FillDataTable(cmd, dsAsr.ITK_ASR);
		}

		public static int DsSaveData_Asr(IDbCommand cmd,
	 DsDbAsr dsAsrToSave, List<string> flightIds,
	 string userId, DateTime updateDateTime)
		{
			DsDbAsr dsAsr = (DsDbAsr)dsAsrToSave.Copy();

			DsDbAsr dsCurAsr = new DsDbAsr();
			UtilDataSet.RemoveExpressionAndRelations(dsCurAsr);
			UtilDataSet.RemoveExpressionAndRelations(dsAsr);

			int newRowCnt = -1;
			int procCnt = 0;

			int cnt = DbLoadData_Asr(cmd, dsCurAsr, flightIds);

			#region Update the information in the current table
			int colCnt = dsAsr.ITK_ASR.Columns.Count;
			foreach (DsDbAsr.ITK_ASRRow row in dsAsrToSave.ITK_ASR.Rows)
			{
				if (string.IsNullOrEmpty(row.URNO))
				{
					//row.URNO = UtilDb.PFIX_VIRTUAL_NEW_ID + newRowCnt;
					//newRowCnt++;
					continue;
				}

				if (!(flightIds.Contains(row.URNO))) continue;
				procCnt++;

				DsDbAsr.ITK_ASRRow[] curRows = (DsDbAsr.ITK_ASRRow[])dsCurAsr.ITK_ASR.Select("URNO='" + row.URNO + "'");
				if ((curRows == null) || (curRows.Length < 1))
				{//New Data
					dsCurAsr.ITK_ASR.LoadDataRow(row.ItemArray, false);
				}
				else
				{//Existing Data
					UtilDataSet.AssignData(row, curRows[0], GetAsrColNameListToAssingData());
				}
			}
			#endregion

			if (procCnt < 1)
			{
				//LogTraceMsg("DsSaveData_Asr:Nothing to Update.");
				throw new ApplicationException("Nothing to update.");
			}

			#region Insert/Update accordingly
			DsDbAsr dsIns = new DsDbAsr();
			DsDbAsr dsUpd = new DsDbAsr();

			DsDbAsr.ITK_ASRDataTable dtIns = (DsDbAsr.ITK_ASRDataTable)dsCurAsr.ITK_ASR.GetChanges(DataRowState.Added);
			DsDbAsr.ITK_ASRDataTable dtUpd = (DsDbAsr.ITK_ASRDataTable)dsCurAsr.ITK_ASR.GetChanges(DataRowState.Modified);

			if ((dtIns != null) && (dtIns.Rows.Count > 0))
			{
				//LogTraceMsg("DsSaveData_Asr:Ins.Count:" + dtIns.Rows.Count);
				DsInsertData_Asr(cmd, dtIns, userId, updateDateTime);
			}

			if ((dtUpd != null) && (dtUpd.Rows.Count > 0))
			{
				//LogTraceMsg("DsSaveData_Asr:Upd.Count:" + dtUpd.Rows.Count);
				int totRowCntToUpdate = 0;
				DsUpdateData_Asr(cmd, dtUpd, userId, updateDateTime, out totRowCntToUpdate);
			}
			#endregion

			return newRowCnt;
		}

		public static List<string> GetAsrColNameListToAssingData()
		{
			if (_lsAsrColNameToAssignData == null)
			{
				lock (_lockObjForAsr)
				{
					if (_lsAsrColNameToAssignData == null)
					{
						_lsAsrColNameToAssignData = new List<string>();
						DsDbAsr ds = new DsDbAsr();
						_lsAsrColNameToAssignData = UtilDataSet.GetNonExpressionColumnList(ds.ITK_ASR);
						_lsAsrColNameToAssignData.Remove("URNO");
						_lsAsrColNameToAssignData.Remove("LSTU");
						_lsAsrColNameToAssignData.Remove("CDAT");
					}
				}
			}
			return _lsAsrColNameToAssignData;
		}

		private static int DsInsertData_Asr(IDbCommand cmd,
		   DsDbAsr.ITK_ASRDataTable dt,
		   string userId, DateTime updateDateTime)
		{
			return UtilDb.InsertDataToDb(cmd, dt, DB_Info.DB_T_ASR, null, userId, updateDateTime);
		}

		private static int DsUpdateData_Asr(IDbCommand cmd,
		   DsDbAsr.ITK_ASRDataTable dt,
		   string userId, DateTime updateDateTime, out int totRowCntToUpdate)
		{
			return UtilDb.UpdateDataToDb(cmd, dt, DB_Info.DB_T_ASR, userId, updateDateTime, false, out totRowCntToUpdate);
		}

		public static void PrepareAsrForAFlight(DsDbAsr dsAsr, string flightId)
		{
			DsDbAsr.ITK_ASRRow[] rows = (DsDbAsr.ITK_ASRRow[])
			   dsAsr.ITK_ASR.Select("URNO='" + flightId + "'");

			if ((rows == null) || (rows.Length < 1))
			{
				DsDbAsr.ITK_ASRRow row = dsAsr.ITK_ASR.NewITK_ASRRow();
				row.URNO = flightId;
				row.SetSBONNull();
				row.SetLKONNull();
				row.SetCDATNull();
				row.SetLSTUNull();

				dsAsr.ITK_ASR.AddITK_ASRRow(row);
				dsAsr.ITK_ASR.AcceptChanges();
			}
		}


		#endregion

		#region Database Operation for ITK_ASR_OTH
		#region ASR_OTH Const and variables.
		private const string PROC_ID_FOR_ASR_OTH = "ITK_ASR_OTH";

		private const string ASR_OTH_STAT_ACTIVE = "A";
		private const string ASR_OTH_STAT_INACTIVE = "I";
		private const string SDF_ASR_OTH_STAT_ACTIVE = "A";
		private const string SDF_ASR_OTH_STAT_INACTIVE = "I";

		private static List<string> _lsAsrOthColNameToAssignData = null;
		private static object _lockObjForAsrOth = new object();
		#endregion

		public static int DbLoadData_AsrOth(IDbCommand cmd,
		   DsDbAsr dsAsr, string flightId, bool refreshOth)
		{
			int cnt = -1;
			if (string.IsNullOrEmpty(flightId))
			{
				throw new ApplicationException("No Flight Id to load the ASR information.");
			}
			List<string> flightIds = new List<string>();
			flightIds.Add(flightId);

			cnt = DbLoadData_AsrOth(cmd, dsAsr, flightIds);
			if ((cnt < 1) || (refreshOth))
			{//No data.
				cnt = 0;
				if (dsAsr.ITK_SDF_ASR_OTH.Rows.Count < 1)
				{
					DbDSdfAsrOth.GetInstance().LoadSdfAsrOth(cmd, dsAsr);
				}
				cnt += PrepareAsrOthUsingSdf(dsAsr, flightId);
			}
			return cnt;
		}

		public static int DbLoadData_AsrOth(IDbCommand cmd,
	 DsDbAsr dsAsr, List<string> flightIds)
		{
			string stFlightIds = UtilMisc.ConvListToString(flightIds,
			   ",", "'", "'", 0, flightIds.Count);
			cmd.Parameters.Clear();
			cmd.CommandText = string.Format("SELECT {0} FROM {1} WHERE FLID IN ({2})",
			   "URNO, FLID, OTID, CHEK, REMK, STAT, USEC, CDAT, USEU, LSTU",
			   GetDb_Tb_AsrOth(),
			   stFlightIds);
			cmd.CommandType = CommandType.Text;

			UtilDb udb = UtilDb.GetInstance();
			dsAsr.ITK_ASR_OTH.Clear();
			
			return udb.FillDataTable(cmd, dsAsr.ITK_ASR_OTH);
		}

		public static int PrepareAsrOthUsingSdf(DsDbAsr dsAsr, string flightId)
		{
			int cnt = 0;

			#region Check existing 'Other' information which are not active and change the status
			foreach (DsDbAsr.ITK_ASR_OTHRow othRow in dsAsr.ITK_ASR_OTH.Select("FLID='" + flightId + "'"))
			{
				DsDbAsr.ITK_SDF_ASR_OTHRow[] sdfRows = (DsDbAsr.ITK_SDF_ASR_OTHRow[])dsAsr.ITK_SDF_ASR_OTH.Select("URNO='" + othRow.OTID + "'");
				if ((sdfRows == null) || (sdfRows.Length < 1))
				{//Not exist in SDF. Need to deactive it.
					othRow.STAT = ASR_OTH_STAT_INACTIVE;
				}
				else
				{
					string sdfStat = sdfRows[0].STAT;
					if (sdfStat == SDF_ASR_OTH_STAT_INACTIVE)
					{
						if (othRow.STAT != ASR_OTH_STAT_INACTIVE) othRow.STAT = ASR_OTH_STAT_INACTIVE;
					}
					else if (sdfStat == SDF_ASR_OTH_STAT_ACTIVE)
					{
						if (othRow.STAT != ASR_OTH_STAT_ACTIVE) othRow.STAT = ASR_OTH_STAT_ACTIVE;
					}
				}

			}
			#endregion

			#region Check for new 'Other' information to add in
			foreach (DsDbAsr.ITK_SDF_ASR_OTHRow sdfRow in dsAsr.ITK_SDF_ASR_OTH.Select("STAT='A'", "LSNO"))
			{
				DsDbAsr.ITK_ASR_OTHRow[] othRows = (DsDbAsr.ITK_ASR_OTHRow[])
				   dsAsr.ITK_ASR_OTH.Select(string.Format("FLID='{0}' AND OTID='{1}'", flightId, sdfRow.URNO));
				if ((othRows == null) || othRows.Length < 1)
				{//Not existed in AsrOth Table
					cnt++;
					DsDbAsr.ITK_ASR_OTHRow othRow = dsAsr.ITK_ASR_OTH.NewITK_ASR_OTHRow();
					othRow.FLID = flightId;
					othRow.OTID = sdfRow.URNO;
					othRow.CHEK = "N";
					othRow.STAT = ASR_OTH_STAT_ACTIVE;
					othRow.CDAT = DateTime.Now;
					othRow.LSTU = othRow.CDAT;
					bool added = false;
					for (int i = 0; i < 1000; i++)
					{
						othRow.URNO = UtilDb.PFIX_VIRTUAL_NEW_ID + cnt + "." + i;
						try
						{
							dsAsr.ITK_ASR_OTH.Rows.Add(othRow);
							added = true;
							break;
						}
						catch (ArgumentException ex)
						{
							LogMsg("PrepareAsrOthUsingSdf:Err:" + flightId + ":" + ex.Message);
							break;
						}
						catch (Exception ex)
						{
							string st = ex.Message;
						}
					}
					if (!added) throw new ApplicationException("Unable to prepare the 'Other' information.");
				}
				else
				{//Existed in AsrOth Table
					othRows[0].STAT = ASR_OTH_STAT_ACTIVE;
				}
			}
			#endregion

			return cnt;
		}

		public static List<string> GetAsrOthColNameListToAssingData()
		{
			if (_lsAsrOthColNameToAssignData == null)
			{
				lock (_lockObjForAsrOth)
				{
					if (_lsAsrOthColNameToAssignData == null)
					{
						_lsAsrOthColNameToAssignData = new List<string>();
						DsDbAsr ds = new DsDbAsr();
						_lsAsrOthColNameToAssignData = UtilDataSet.GetNonExpressionColumnList(ds.ITK_ASR_OTH);
						_lsAsrOthColNameToAssignData.Remove("URNO");
						_lsAsrOthColNameToAssignData.Remove("FLID");
						_lsAsrOthColNameToAssignData.Remove("OTID");
					}
				}
			}
			return _lsAsrOthColNameToAssignData;
		}

		public static int DsSaveData_AsrOth(IDbCommand cmd,
		   DsDbAsr dsAsrToSave, List<string> flightIds,
		   string userId, DateTime updateDateTime)
		{
			LogTraceMsg("DsSaveData_AsrOth:Start.");
			DsDbAsr dsAsr = (DsDbAsr)dsAsrToSave.Copy();

			DsDbAsr dsCurAsr = new DsDbAsr();
			UtilDataSet.RemoveExpressionAndRelations(dsCurAsr);
			UtilDataSet.RemoveExpressionAndRelations(dsAsr);

			int newRowCnt = -1;

			int cnt = DbLoadData_AsrOth(cmd, dsCurAsr, flightIds);

			#region Update the information in the current table
			int colCnt = dsAsr.ITK_ASR_OTH.Columns.Count;
			foreach (DsDbAsr.ITK_ASR_OTHRow row in dsAsr.ITK_ASR_OTH.Rows)
			{
				if (string.IsNullOrEmpty(row.URNO))
				{
					row.URNO = UtilDb.PFIX_VIRTUAL_NEW_ID + newRowCnt;
					newRowCnt++;
				}

				DsDbAsr.ITK_ASR_OTHRow[] curRows = (DsDbAsr.ITK_ASR_OTHRow[])dsCurAsr.ITK_ASR_OTH.Select("FLID='" + row.FLID + "' AND OTID='" + row.OTID + "'");
				if ((curRows == null) || (curRows.Length < 1))
				{//New Data
					dsCurAsr.ITK_ASR_OTH.LoadDataRow(row.ItemArray, false);
				}
				else
				{//Existing Data
					if (curRows[0].OTID != row.OTID)
					{
						throw new ApplicationException("Information of 'Other' field is not allowed.");
					}
					if (curRows[0].FLID != row.FLID)
					{
						throw new ApplicationException("Flight changed.");
					}
					row.URNO = curRows[0].URNO;

					UtilDataSet.AssignData(row, curRows[0], GetAsrOthColNameListToAssingData());
				}
			}
			#endregion

			DsDbAsr dsIns = new DsDbAsr();
			DsDbAsr dsUpd = new DsDbAsr();

			//dsCurAsr.ITK_ASR_OTH.WriteXml(@"C:\TEMP\ASR_OTH.xml");
			DsDbAsr.ITK_ASR_OTHDataTable dtIns = (DsDbAsr.ITK_ASR_OTHDataTable)dsCurAsr.ITK_ASR_OTH.GetChanges(DataRowState.Added);
			DsDbAsr.ITK_ASR_OTHDataTable dtUpd = (DsDbAsr.ITK_ASR_OTHDataTable)dsCurAsr.ITK_ASR_OTH.GetChanges(DataRowState.Modified);

			if ((dtIns != null) && (dtIns.Rows.Count > 0))
			{
				LogTraceMsg("DsSaveData_AsrOth:Ins:" + dtIns.Rows.Count);
				DsInsertData_AsrOth(cmd, dtIns, userId, updateDateTime);
			}

			if ((dtUpd != null) && (dtUpd.Rows.Count > 0))
			{
				//dtUpd.WriteXml(@"C:\TEMP\ASR_OTH_Upd.xml");
				LogTraceMsg("DsSaveData_AsrOth:Upd:" + dtUpd.Rows.Count);
				int totRowCntToUpdate = 0;
				DsUpdateData_AsrOth(cmd, dtUpd, userId, updateDateTime, out totRowCntToUpdate);
			}

			return newRowCnt;
		}

		private static int DsInsertData_AsrOth(IDbCommand cmd,
		   DsDbAsr.ITK_ASR_OTHDataTable dt,
		   string userId, DateTime updateDateTime)
		{
			return UtilDb.InsertDataToDb(cmd, dt, DB_Info.DB_T_ASR_OTH, PROC_ID_FOR_ASR_OTH, userId, updateDateTime);
		}

		private static int DsUpdateData_AsrOth(IDbCommand cmd,
		   DsDbAsr.ITK_ASR_OTHDataTable dt,
		   string userId, DateTime updateDateTime, out int totRowCntToUpdate)
		{
			return UtilDb.UpdateDataToDb(cmd, dt, DB_Info.DB_T_ASR_OTH, userId, updateDateTime, false, out totRowCntToUpdate);
		}


		#endregion
	}

}