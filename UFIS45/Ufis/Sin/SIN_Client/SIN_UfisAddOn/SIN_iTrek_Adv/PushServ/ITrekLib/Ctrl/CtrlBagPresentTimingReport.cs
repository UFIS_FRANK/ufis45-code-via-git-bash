using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

using UFIS.ITrekLib.DB;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.Util;

namespace UFIS.ITrekLib.Ctrl
{
    public class CtrlBagPresentTimingReport
    {
        public static CtrlBagPresentTimingReport GetInstance()
        {
            return new CtrlBagPresentTimingReport();
        }

        public DSBagPresentTimingRpt PopulateBagPresentTimingReport(string flightUrno)
        {
            DSBagPresentTimingRpt dsBPTR = null;
            try
            {
                if (IsBagPresentTimingReportExist(flightUrno))
                {
                    //LogMsg("bptr exist");
                    dsBPTR = RetrieveBagPresentTimingReport(flightUrno);
                }
                else
                {
                    //LogMsg("bptr not exist");
                    dsBPTR = PopulateExistingData(flightUrno);
                }

            }
            catch (Exception ex)
            {
                LogMsg("PopulateBagPresentTimingReport:Error:" + ex.Message);
                throw new ApplicationException("Fail to get the report information.");
            }
            return dsBPTR;
        }

        private DSBagPresentTimingRpt PopulateExistingData(string flightUrno)
        {
            DSBagPresentTimingRpt dsBPRT = new DSBagPresentTimingRpt();
            DSBagPresentTimingRpt.BPTRHDRRow bptrHdrRow = dsBPRT.BPTRHDR.NewBPTRHDRRow();
            bptrHdrRow.UAFT = flightUrno;
            dsBPRT.BPTRHDR.AddBPTRHDRRow(bptrHdrRow);

            PopulateFlightInfo(flightUrno, dsBPRT);
            PopulateTripInfoForArrivalFlight(flightUrno, dsBPRT);
            PopulateAORemarks(flightUrno, dsBPRT);

            dsBPRT.AcceptChanges();
            return dsBPRT;
        }

        private void PopulateAORemarks(string flightUrno, DSBagPresentTimingRpt dsBPRT)
        {
            try
            {
                if (CtrlAsr.IsConfirmedReportExist(flightUrno))
                {
					DSApronServiceRpt dsASR = CtrlAsr.RetrieveApronServiceReport(flightUrno);
                    if ((dsASR != null) && (dsASR.ASR != null))
                    {
                        if (dsASR.ASR.Rows.Count > 0)
                        {
                            bool createNewRow = false;
                            DSBagPresentTimingRpt.BPTRHDRRow row = null;
                            if (dsBPRT.BPTRHDR.Rows.Count < 1)
                            {
                                row = dsBPRT.BPTRHDR.NewBPTRHDRRow();
                            }
                            else
                            {
                                row = (DSBagPresentTimingRpt.BPTRHDRRow)dsBPRT.BPTRHDR.Rows[0];
                            }
                            row.AO_TIME_RMK = ((DSApronServiceRpt.ASRRow)dsASR.ASR.Rows[0]).RSNBPT;
                            if (createNewRow)
                            {
                                dsBPRT.BPTRHDR.AddBPTRHDRRow(row);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogMsg("PopulateAORemarks:Error:" + flightUrno + ":" + ex.Message);
                throw new ApplicationException("Unable to get the AO Timing Remark.") ;
            }
        }

        private void PopulateTripInfoForArrivalFlight(string flightUrno, DSBagPresentTimingRpt dsBPRT)
        {
            try
            {
                DSTrip dsTrip = CtrlTrip.RetrieveTrip(flightUrno);
                int cnt = 0;
                string aoUldRmk = "";
                const string NEWLINE = "\n\r";
                if (dsTrip != null)
                {
                    foreach (DSTrip.TRIPRow tripRow in dsTrip.TRIP.Select("", "RCV_DT, ULDN"))
                    {
                        DSBagPresentTimingRpt.BPTRDETRow bptrDetRow = dsBPRT.BPTRDET.NewBPTRDETRow();
                        bptrDetRow.CLASS = tripRow.CLASS;
                        bptrDetRow.SR_NO = Convert.ToString(++cnt);
                        bptrDetRow.UAFT = flightUrno;
                        bptrDetRow.ULDNO = tripRow.ULDN;

                        bptrDetRow.ULDTIME = tripRow.RCV_DT;
                        if (tripRow.SENT_RMK != "")
                        {
                            aoUldRmk += tripRow.SENT_RMK + NEWLINE;
                        }
                        dsBPRT.BPTRDET.AddBPTRDETRow(bptrDetRow);
                    }
                }

                ((DSBagPresentTimingRpt.BPTRHDRRow)(dsBPRT.BPTRHDR.Rows[0])).AO_TRP_RMK = aoUldRmk;
            }
            catch (Exception ex)
            {
                LogMsg("PopulateTripInfoForArrivalFlight:Error:" + flightUrno + ":" + ex.Message);
                throw new ApplicationException("Unable to get the AO Trip remarks.");
            }
        }

        private void PopulateFlightInfo(string flightUrno, DSBagPresentTimingRpt dsBPRT)
        {
            try
            {
                DSFlight dsFlight = CtrlFlight.RetrieveFlightsByFlightId(flightUrno);
                bool createNewRow = false;
                DSBagPresentTimingRpt.BPTRHDRRow bptrHdrRow = null;
                if (dsBPRT.BPTRHDR.Rows.Count < 1)
                {
                    bptrHdrRow = dsBPRT.BPTRHDR.NewBPTRHDRRow();
                }
                else
                {
                    bptrHdrRow = (DSBagPresentTimingRpt.BPTRHDRRow)dsBPRT.BPTRHDR.Rows[0];
                }

                if (dsFlight.FLIGHT.Rows.Count > 0)
                {
                    DSFlight.FLIGHTRow flightRow = (DSFlight.FLIGHTRow)dsFlight.FLIGHT.Rows[0];
                    bptrHdrRow.ACFT_TYPE = flightRow.AIRCRAFT_TYPE;
                    bptrHdrRow.AO_ID = flightRow.AOID;
                    bptrHdrRow.AO_FNAME = flightRow.AOFNAME;
                    //bptrHdrRow.AO_LNAME = flightRow.AO_LNAME;
                    bptrHdrRow.AT = flightRow.AT;

                    bptrHdrRow.BELT = flightRow.BELT;
                    //bptrHdrRow.BO_ID = flightRow.bo
                    bptrHdrRow.FBAG = flightRow.FST_BAG;
                    bptrHdrRow.FBAG_TIME_MET = flightRow.BG_FST_BM_MET;
                    bptrHdrRow.FLDT = flightRow.ST;
                    bptrHdrRow.FLNU = flightRow.FLNO;
                    bptrHdrRow.FTRIP_B = flightRow.BG_FST_TRP;
                    bptrHdrRow.LBAG = flightRow.LST_BAG;
                    bptrHdrRow.LBAG_TIME_MET = flightRow.BG_LST_BM_MET;
                    bptrHdrRow.LTRIP_B = flightRow.BG_LST_TRP;
                    bptrHdrRow.PARK_STAND = flightRow.PARK_STAND;
                    bptrHdrRow.TBS_UP = flightRow.TBS_UP;
                    bptrHdrRow.UAFT = flightUrno;
                }

                if (createNewRow)
                {
                    dsBPRT.BPTRHDR.AddBPTRHDRRow(bptrHdrRow);
                }

            }
            catch (Exception ex)
            {
                LogMsg("PopulateFlightInfo:Error:" + flightUrno + ":" + ex.Message);
                throw new ApplicationException("Unable to get the flight informatin.");
            }
        }

        private DSBagPresentTimingRpt RetrieveBagPresentTimingReport(string flightUrNo)
        {
            DSBagPresentTimingRpt dsBPRT = DBBagPresentTimingReport.GetInstance().RetrieveBagPresentTimingReportForAFlight(flightUrNo);
            if (dsBPRT.BPTRHDR.Rows.Count > 0)
            {
                DSBagPresentTimingRpt.BPTRHDRRow hdrRow = (DSBagPresentTimingRpt.BPTRHDRRow)dsBPRT.BPTRHDR.Rows[0];
                PopulateAORemarks(hdrRow.UAFT, dsBPRT);
            }

            return dsBPRT;
        }

        public bool IsBagPresentTimingReportExist(string flightUrNo)
        {
            bool exist = false;
            try
            {
                DSBagPresentTimingRpt dsBPRT = RetrieveBagPresentTimingReport(flightUrNo);
                if ((dsBPRT != null) && (dsBPRT.BPTRHDR != null))
                {
                    if (dsBPRT.BPTRHDR.Rows.Count > 0) exist = true;
                }
            }
            catch (Exception ex)
            {
                LogMsg("IsBagPresentTimingReportExist:Error:" + ex.Message);
            }
            return exist;
        }

        public void SaveReport(string flightUrno, string boRemark, EntUser user)
        {
            DSBagPresentTimingRpt dsBPRT = PopulateBagPresentTimingReport(flightUrno);
            DSBagPresentTimingRpt.BPTRHDRRow hdrRow = ((DSBagPresentTimingRpt.BPTRHDRRow)(dsBPRT.BPTRHDR.Rows[0]));
            if (hdrRow.BO_ID.Trim() == "")
            {
                hdrRow.BO_ID = user.UserId;
                hdrRow.BO_FNAME = user.FirstName;
                hdrRow.BO_LNAME = user.LastName;
            }
            hdrRow.BO_RMK = boRemark;
            DBBagPresentTimingReport dbBPTR = DBBagPresentTimingReport.GetInstance();
            dbBPTR.SaveRpt(flightUrno, dsBPRT);
            if (IsBagPresentTimingReportExist(flightUrno))
            {
#warning //TODO - To Update the Baggage Presentation Timing Report Exist
				//CtrlFlight.GetInstance().UpdateBagPreTimeReportExist(flightUrno, true);
            }
        }

        public static void HkReport(ArrayList arrList)
        {
            DBBagPresentTimingReport.GetInstance().HkReport(arrList);
        }

        private static void LogMsg(string msg)
        {
            UtilLog.LogToGenericEventLog("CtrlBagPresentTimingReport", msg);
        }
    }
}
