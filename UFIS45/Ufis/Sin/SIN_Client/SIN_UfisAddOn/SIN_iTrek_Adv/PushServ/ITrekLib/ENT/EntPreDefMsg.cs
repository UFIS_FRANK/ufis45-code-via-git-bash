using System;
using System.Collections.Generic;
using System.Text;

namespace UFIS.ITrekLib.ENT
{
    public class EntPreDefMsg
    {
        private const string INACTIVE_STATUS = "I";
        private const string ACTIVE_STATUS = "A";

        private string _msgId = null;
        private string _msg = null;
        private string _stat = null;

        public EntPreDefMsg()
        {
            _msg = "";
            _stat = INACTIVE_STATUS;
        }

        public string msgId
        {
            get
            {
                if (_msgId == null) return ""; else return _msgId;
            }
            set
            {
                if (_msgId == null)
                {
                    if ((value == null) || (value == "")) throw new ApplicationException("Invalid Message Id");
                    _msgId = value;
                }
                else throw new ApplicationException("Message Id can not be changed.");
            }
        }

        public string msg
        {
            get
            {
                if (_msg == null) return ""; else return _msg;
            }
            set
            {
                if ((value == null) || (value == ""))
                {
                    _msg = "";
                    _stat = INACTIVE_STATUS;
                }
            }
        }

        public string stat
        {
            get
            {
                return _stat;
            }
            set
            {
                if ((value == ACTIVE_STATUS) || (value == INACTIVE_STATUS))
                {
                    _stat = value;
                }
                else
                {
                    throw new ApplicationException("Invalid Status");
                }
            }
        }

        public void SetActive()
        {
            stat = ACTIVE_STATUS;
        }

        public void SetInActive()
        {
            stat = INACTIVE_STATUS;
        }

        public bool IsActive()
        {
            if (stat == ACTIVE_STATUS) return true; else return false;
        }
    }
}
