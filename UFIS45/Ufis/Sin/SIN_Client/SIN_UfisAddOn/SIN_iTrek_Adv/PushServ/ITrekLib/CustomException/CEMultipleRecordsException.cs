using System;
using System.Collections.Generic;
using System.Text;

namespace UFIS.ITrekLib.CustomException
{
   class CEMultipleRecordsException: ApplicationException
    {
      private const string DEFAULT_MSG = "Ambiguous. More than one records!";

        public CEMultipleRecordsException()
            : base(DEFAULT_MSG)
        {
        }

      public CEMultipleRecordsException(string msg)
            : base(msg)
        {
        }
    }
}
