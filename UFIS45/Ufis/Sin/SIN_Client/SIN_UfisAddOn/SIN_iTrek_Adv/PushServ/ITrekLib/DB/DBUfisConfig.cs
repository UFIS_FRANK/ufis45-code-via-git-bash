using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;

using System.Data;

using UFIS.ITrekLib.DS;
using iTrekXML;
using UFIS.ITrekLib.Util;
using UFIS.ITrekLib.ENT;
using MM.UtilLib;

namespace UFIS.ITrekLib.DB
{
   /// <summary>
   /// Configuration Information coming from UFIS (BackEnd)
   /// </summary>
    public class DBUfisConfig
    {
        private static Object _lockObj = new Object();
        private static DBUfisConfig _dbConfig = null;
        private static readonly DateTime DEFAULT_DT = new DateTime(1, 1, 1);
        private static DateTime _lastWriteDT = DEFAULT_DT;

        private static DSConfig _dsConfig = null;
        private static string _fnConfigXml = null;
        private static DateTime _lastRefreshDT = new DateTime();

        private DBUfisConfig() { }

        public void Init()
        {
            _dsConfig = null;
            _fnConfigXml = null;
            _lastWriteDT = DEFAULT_DT;
            _lastRefreshDT = DEFAULT_DT;
        }

        public static DBUfisConfig GetInstance()
        {
            if (_dbConfig == null)
            {
                _dbConfig = new DBUfisConfig();
            }
            return _dbConfig;
        }

        public string GetConfigValueFor(string configFor, out string desc)
        {
            string value = "";
            desc = "";
            DSConfig tempDs = dsConfig;
            DSConfig.CONFIGRow[] rowArr = (DSConfig.CONFIGRow[])tempDs.CONFIG.Select("ID='" + configFor + "'");
            if (rowArr.Length > 0)
            {
                value = rowArr[0].VAL;
                desc = rowArr[0].ID_DESC;
            }

            return value;
        }

        public DSConfig RetrieveConfig()
        {
            DSConfig ds = new DSConfig();
            ds = (DSConfig) dsConfig.Copy();
            return ds;
        }

        public bool IsRecordExist(string id)
        {
            bool exist = false;
            DSConfig tempDs = dsConfig;
            DataRow[] rows = tempDs.CONFIG.Select("ID='" + id + "'");
            if ((rows != null) && (rows.Length > 0)) exist = true;
            return exist;
        }

        public void TestUpdate()
        {
            if (!File.Exists(fnConfigXml))
            {
                try
                {
                    LockForWriting();
                    if (!File.Exists(fnConfigXml))
                    {//check again to make sure no data existed.
                        LogMsg("Create Empty UfisConfig");
                        DSConfig ds = new DSConfig();
                        WriteData(ds);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    ReleaseAfterWrite();
                }
            }
        }

        public void Save(string userId, string id, string value, string module, string desc)
        {
            if (id == null) throw new ApplicationException("Blank Id");
            if (value == null) value = "";
            if (module == null) module = "";
            if (desc == null) desc = "";

            lock (_lockObj)
            {
                try
                {
                    TestUpdate();

                    LockForWriting();
                    DSConfig ds = null;
                    if (ReadData(out ds))
                    {
                        bool hasId = false;
                        foreach (DSConfig.CONFIGRow oldRow in ds.CONFIG.Select("ID='" + id + "'"))
                        {
                            oldRow.VAL = value.Trim();
                            oldRow.MODULE = module.Trim();
                            oldRow.ID_DESC = desc;
                            hasId = true;
                            LogMsg(string.Format("Updating id<{0}>, value<{1}>, by <{2}>", 
                                id, value, userId));
                            break;
                        }
                        if (!hasId)
                        {
                            DSConfig.CONFIGRow row = ds.CONFIG.NewCONFIGRow();
                            row.ID = id.Trim().ToUpper();
                            row.VAL = value.Trim();
                            row.MODULE = module.Trim();
                            row.ID_DESC = desc;
                            ds.CONFIG.AddCONFIGRow(row);
                            LogMsg(string.Format("Creating id<{0}>, value<{1}>, by <{2}>", id, value, userId));
                        }
                        ds.CONFIG.AcceptChanges();
                        WriteData(ds);                        
                    }
                    else
                    {
                        throw new ApplicationException("Unable to Get the Configuration Information.");
                    }
                }
                catch (Exception ex)
                {
                    LogMsg("CreateNewConfig:Err:" + ex.Message);
                    throw;
                }
                finally
                {
                    ReleaseAfterWrite();
                }
            }
        }

        public void CreateNewConfig(string userId, string id, string value, string module, string desc)
        {
            if (id == null) throw new ApplicationException("Blank Id");
            if (value == null) value = "";
            if (module == null) module = "";
            if (desc == null) desc = "";
            lock (_lockObj)
            {
                try
                {
                    TestUpdate();

                    LockForWriting();
                    DSConfig ds = null;
                    if (ReadData(out ds))
                    {
                        foreach (DSConfig.CONFIGRow oldRow in ds.CONFIG.Select("ID='" + id + "'"))
                        {
                            throw new ApplicationException("record Id " + id + " already existed.");
                        }
                        DSConfig.CONFIGRow row = ds.CONFIG.NewCONFIGRow();
                        row.ID = id.Trim().ToUpper();
                        row.VAL = value.Trim();
                        row.MODULE = module.Trim();
                        row.ID_DESC = desc;
                        ds.CONFIG.AddCONFIGRow(row);
                        ds.CONFIG.AcceptChanges();
                        WriteData(ds);
                        LogMsg(string.Format("Create id<{0}>, value<{1}>, by <{2}>", id, value, userId));
                    }
                    else
                    {
                        throw new ApplicationException("Unable to Get the Configuration Information.");
                    }
                }
                catch (Exception ex)
                {
                    LogMsg("CreateNewConfig:Err:" + ex.Message);
                    throw;
                }
                finally
                {
                    ReleaseAfterWrite();
                }
            }
        }

        public void UpdateConfig(string userId, string id, string value)
        {
            if (id == null) throw new ApplicationException("Blank Id");
            if (value == null) value = "";

            lock (_lockObj)
            {
                bool updated = false;
                try
                {
                    LockForWriting();
                    DSConfig ds = null;
                    if (ReadData(out ds))
                    {
                        bool hasId = false;
                        foreach (DSConfig.CONFIGRow row in ds.CONFIG.Select("ID='" + id + "'"))
                        {
                            row.VAL = value.Trim();
                            hasId = true;
                        }

                        if (hasId)
                        {
                            ds.CONFIG.AcceptChanges();
                            WriteData(ds);
                            LogMsg(string.Format("Updated id<{0}>, value>{1}, by<{2}>", id, value, userId));
                            updated = true;
                        }
                        else
                        {
                            throw new ApplicationException("Record Not existed for update.");
                        }

                        if (!updated) throw new ApplicationException("Fail to update the information.");
                    }
                    else
                    {
                        throw new ApplicationException("Unable to Get the Configuration Information.");
                    }
                }
                catch (Exception ex)
                {
                    LogMsg("UpdateConfig:Err:" + ex.Message);
                    throw;
                }
                finally
                {
                    ReleaseAfterWrite();
                }
            }
        }

        private string fnConfigXml
        {
            get
            {
                if ((_fnConfigXml == null) || (_fnConfigXml == ""))
                {
                    iTrekXML.iTXMLPathscl path = new iTXMLPathscl();
                    _fnConfigXml = path.GetUfisCONFIGFilePath();
                }

                return _fnConfigXml;
            }
        }

        private DSConfig dsConfig
        {
            get
            {
                if (Math.Abs(UtilTime.DiffInSec(DateTime.Now, _lastRefreshDT)) > 500)
                {
                    LoadDSConfig();
                    _lastRefreshDT = DateTime.Now;
                }
                DSConfig ds = (DSConfig)_dsConfig.Copy();

                return ds;
            }
        }

        private void LoadDSConfig()
        {
            FileInfo fiXml = new FileInfo(fnConfigXml);
            DateTime curDT = fiXml.LastWriteTime;

            if ((_dsConfig == null) || (curDT.CompareTo(_lastWriteDT) != 0))
            {
                lock (_lockObj)
                {
                    try
                    {
                        fiXml = new FileInfo(fnConfigXml);
                        curDT = fiXml.LastWriteTime;//check the time again, in case of lock
                        if ((_dsConfig == null) || (curDT.CompareTo(_lastWriteDT) != 0))
                        {
                            try
                            {
                                DSConfig tempDs = null;
                                if(ReadData(out tempDs))
                                {
                                    AssignLatestData(tempDs);
                                }
                            }
                            catch (Exception ex)
                            {
                                LogMsg("LoadDSConfig:Err:" + ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogMsg("LoadDSConfig:Err:" + ex.Message);
                    }
                }
            }
        }

        private bool ReadData(out DSConfig ds)
        {
            ds = new DSConfig();
            return UtilDataSet.LoadDataFromXmlFile(ds.CONFIG, fnConfigXml);
        }

        private bool WriteData(DSConfig ds)
        {
            bool success = false;

            try
            {
                LockForWriting();
                if (ds == null) throw new ApplicationException("Invalid Information to Update The data.");
                success = UtilDataSet.WriteXml(ds, fnConfigXml);
                if (success)
                {
                    AssignLatestData(ds);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                ReleaseAfterWrite();
            }
            return success;
        }

        private void AssignLatestData(DSConfig dsLatestData)
        {
            _dsConfig = dsLatestData;
            FileInfo fi = new FileInfo(fnConfigXml);
            _lastRefreshDT = DateTime.Now;
            _lastWriteDT = fi.LastWriteTime;
            
        }

        public void UpdateData(DSConfig dsNew)
        {
            if (dsNew == null) return;
            if (dsNew.CONFIG.Rows.Count < 1) return;

            DSConfig dsOrg=null;
            bool hasData = false;
            for (int retry = 0; retry < 10; retry++)
            {
                if (ReadData(out dsOrg)) { hasData = true; break; }
                System.Threading.Thread.Sleep(20);
            }

            if (!hasData)
            {
                LogMsg("UpdateData:No Data.");
                dsOrg = dsNew;
            }
            else
            {                
                foreach(DSConfig.CONFIGRow newRow in dsNew.CONFIG.Select("","ID"))
                {
                    string id = "";
                    string val = "";
                    string act = "";

                    try
                    {
                        id = newRow.ID;
                        val = newRow.VAL;
                        act = newRow.ACT;
                        if (string.IsNullOrEmpty(id)) continue;
                        if ((act != "U") && (act != "I") & (act != "Q"))
                        {
                            if (act == "D")
                            {
                                val = "-";
                            }
                            else
                            {
                                LogMsg(string.Format("UpdateData:Err:ID<{0}>,VAL<{1}>,ACT<{2}>", id, val, act));
                                continue;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogMsg("UpdateData:Err:" + ex.Message);
                    }

                    bool isNewRecord = false;
                    DSConfig.CONFIGRow[] orgRows = (DSConfig.CONFIGRow[])dsOrg.CONFIG.Select("ID='" + id + "'");
                    DSConfig.CONFIGRow orgRow = null;

                    if ((orgRows != null) && (orgRows.Length > 0))
                    {
                        orgRow = orgRows[0];
                    }
                    else
                    { 
                        isNewRecord = true;
                        orgRow = dsOrg.CONFIG.NewCONFIGRow();
                        orgRow.ID = id;                     
                    }

                    orgRow.VAL = val;
                    orgRow.ACT = act;

                    if (isNewRecord)
                    {
                        dsOrg.CONFIG.AddCONFIGRow(orgRow);
                    }
                }
            }
            dsOrg.CONFIG.AcceptChanges();

            bool successWrite = false;
            for (int i = 0; i < 20; i++)
            {
                if (WriteData(dsOrg))
                {
                    successWrite = true;
                    break;
                }
                System.Threading.Thread.Sleep(10);
            }

            if (!successWrite)
            {
                LogMsg("UpdateData:Unable to Write.");
                throw new ApplicationException("Unable to update Config information.");
            }
        }

        private void LogMsg(string msg)
        {
            //UtilLog.LogToTraceFile("DBUfisConfig", msg);
           try
           {
              MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("DBUfisConfig", msg);
           }
           catch (Exception)
           {
              UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("DBUfisConfig", msg);
           }
        }

        private void LockForWriting()
        {
            try
            {
                UtilLock.GetInstance().LockUfisConfig();
            }
            catch (Exception ex)
            {
                LogMsg("LockForWriting:Err:" + ex.Message);
                if (ex.InnerException != null)
                {
                    LogMsg("LockForWriting:err:Inner:" + ex.InnerException.Message);
                }
            }
        }

        private void ReleaseAfterWrite()
        {
            try
            {
                UtilLock.GetInstance().ReleaseUfisConfig();
            }
            catch (Exception ex)
            {
                LogMsg("ReleaseAfterWrite:Err:" + ex.Message);
                if (ex.InnerException != null)
                {
                    LogMsg("ReleaseAfterWrite:err:Inner:" + ex.InnerException.Message);
                }
            }
        }
    }
}
