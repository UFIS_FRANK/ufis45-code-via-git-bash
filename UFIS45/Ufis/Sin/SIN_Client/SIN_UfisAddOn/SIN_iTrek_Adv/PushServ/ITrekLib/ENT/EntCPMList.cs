using System;
using System.Collections.Generic;
using System.Text;

using System.Collections;

namespace UFIS.ITrekLib.ENT
{
    public class EntCPMList
    {
        private ArrayList _al = null;

        public EntCPMList()
        {
            _al = new ArrayList();
        }

        private ArrayList CpmList
        {
            get
            {
                if (_al == null) _al = new ArrayList();
                return _al;
            }
        }

        public int Count
        {
            get { return CpmList.Count; }
        }

        public EntCPM this[int idx]
        {
            get
            {
                return (EntCPM)CpmList[idx];
            }
            set
            {
                CpmList[idx] = value;
            }
        }

        public EntCPM GetData(int idx)
        {
            return (EntCPM)CpmList[idx];
        }

        public void SetData(int idx, EntCPM cpm)
        {
            if (idx >= 0)
            {
                for (int i = CpmList.Count; i <= idx; i++)
                {
                    CpmList.Add(new EntCPM());
                }
                ((EntCPM)CpmList[idx]).Copy(cpm);
            }
            else
            {
                AddData(cpm);
            }
        }

        public void AddData(EntCPM cpm)
        {
            CpmList.Add(cpm);
        }

        public void Clear()
        {
            CpmList.Clear();
        }

        public int GetIndexOfContainer(string containerNo)
        {
            int cnt = CpmList.Count;
            int idx = -1;
            for (int i = 0; i < cnt; i++)
            {
                if (((EntCPM)CpmList[i]).ContainerNo == containerNo)
                {
                    idx = i;
                    break;
                }
            }
            return idx;
        }

        public void AddData(EntCPMList listToAdd)
        {
            int cnt = listToAdd.Count;
            for (int i = 0; i < cnt; i++)
            {
                AddData(listToAdd.GetData(i));
            }
        }

        public EntCPMList GetSelectedList(EntCPMList list, bool isCPM)
        {
            if (list == null)
            {
                list = new EntCPMList();
            }
            int cnt = CpmList.Count;
            for (int i = 0; i < cnt; i++)
            {
                if (((EntCPM)CpmList[i]).Selected)
                {
                    EntCPM cpm = new EntCPM();
                    cpm.Copy((EntCPM)CpmList[i]);
                    cpm.AllowToChangeContainerNo = !isCPM;
                    list.AddData( cpm );
                }
            }
            return list;
        }


        public EntCPMList GetUnSelectedList(EntCPMList list, bool isCPM)
        {
            if (list == null)
            {
                list = new EntCPMList();
            }
            int cnt = CpmList.Count;
            for (int i = 0; i < cnt; i++)
            {
                if (!(((EntCPM)CpmList[i]).Selected))
               
                {
                    EntCPM cpm = new EntCPM();
                    cpm.Copy((EntCPM)CpmList[i]);
                    cpm.AllowToChangeContainerNo = !isCPM;
                    list.AddData(cpm);
                }
            }
            return list;
        }

        public EntCPMList GetSelectedList(EntCPMList list)
        {
            if (list == null)
            {
                list = new EntCPMList();
            }
            int cnt = CpmList.Count;
            for (int i = 0; i < cnt; i++)
            {
                if (((EntCPM)CpmList[i]).Selected)
                {
                    EntCPM cpm = new EntCPM();
                    cpm.Copy((EntCPM)CpmList[i]);
                    list.AddData(cpm);
                }
            }
            return list;
        }
    }
}
