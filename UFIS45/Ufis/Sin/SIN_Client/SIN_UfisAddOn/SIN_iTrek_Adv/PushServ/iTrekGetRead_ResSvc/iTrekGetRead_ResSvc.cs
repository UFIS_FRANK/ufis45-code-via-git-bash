#define LoggingOn
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using IBM.WMQ;
using System.Timers;
using System.Configuration;
using iTrekWMQ;
using System.IO;
using iTrekXML;
using System.Xml;
using iTrekSessionMgr;
using UFIS.ITrekLib.Util;
using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.CommMsg;

//NB. Tests at bottom of file
namespace iTrekGetRead_ResSvc
{
	/// <summary>
	/// When a request for data has been made by iTrek and put on the 
	/// FROM.ITREK.SELECT queue this service picks up the reply from
	/// TO.ITREK.READ_RES queue and puts it in ALLOK2L/SHOWDLS.xml in the 
	/// relevant users session directory.
	/// 
	/// Also used for updating the timing in ArrivalFlightTiming.xml
	/// and DepartureFlightTiming.xml
	/// According to timing set in SERVICESTIMER
	/// </summary>

	public partial class iTrekGetRead_ResSvc : ServiceBase
	{
		iTXMLcl iTXML;
		iTXMLPathscl iTPaths;
		MQQueue queue;

		ITrekWMQClass1 wmq1;
		//SessionManager sessMan;
		//iTrekWMQClass TOITREKREAD_RESQ;

		public iTrekGetRead_ResSvc()
		{
			InitializeComponent();
			LogMsg("Initialisation iTrekGetRead_ResSvc");

			iTXML = new iTXMLcl();
			iTPaths = new iTXMLPathscl();
		}

		private void LogMsg(string msg)
		{
			iTrekGetRead_ResSvcLog.WriteEntry(msg);
		}

		private void LogTraceMsg(string msg)
		{
			if (CtrlConfig.IsTraceModeOn())
			{
				UtilLog.LogToTraceFile("iTrekGetRead_ResSvc", msg);
			}
		}

		protected override void OnStart(string[] args)
		{
			LogMsg("In OnStart");

			MessageQueueTimer();
			wmq1 = new ITrekWMQClass1();
			//queue = wmq1.getQueueConnection(iTPaths.GetTOITREKREAD_RESQueueName(), MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
		}

		protected override void OnStop()
		{
			LogMsg("In onStop.");
		}

		public void MessageQueueTimer()
		{
			System.Timers.Timer aTimer = new System.Timers.Timer();
			aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
			// Set the Interval to 5 seconds.
			aTimer.Interval = Convert.ToInt32(iTPaths.GetSERVICESTIMER());

			/* To be changed after testing*/
			//aTimer.Interval = Convert.ToInt32(500);
			aTimer.Enabled = true;
		}

		private static bool _IsProcessing = false;
		private static object _lockObj = new object();

		// Specify what you want to happen when the Elapsed event is raised.
		private void OnTimedEvent(object source, ElapsedEventArgs e)
		{
			if (!_IsProcessing)
			{
				lock (_lockObj)
				{
					if (!_IsProcessing)
					{
						try
						{
							_IsProcessing = true;
							DoProcess();
						}
						catch (Exception ex)
						{
							LogMsg(ex.Message);
						}
						finally
						{
							_IsProcessing = false;
						}
					}
				}
			}
		}

		private string GetMessageFromQueue()
		{
			string message = "";
			iTXMLPathscl iTPaths = new iTXMLPathscl();
			//TODO: Check how this is instantiated - hard coded path?
			iTXML = new iTXMLcl();
			//ITrekWMQClass1 TOITREKREAD_RESQ = new ITrekWMQClass1();
			if (wmq1 != null)
			{
				queue = wmq1.getQueueConnection(iTPaths.GetTOITREKREAD_RESQueueName(), MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
			}
			else
			{
				wmq1 = new ITrekWMQClass1();
				queue = wmq1.getQueueConnection(iTPaths.GetTOITREKREAD_RESQueueName(), MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);

			}
			message = wmq1.GetMessageFromTheQueue(queue);
			wmq1.closeQueueConnection(queue, wmq1.getQManager);
			// message = ITrekWMQClass1.GetMessageFromTheQueue(iTPaths.GetTOITREKREAD_RESQueueName());
			return message;
		}

		private void DoProcess()
		{
			try
			{
				for (int i = 0; i < 100; i++)
				{
					string message = "";
					message = GetMessageFromQueue();
					if (message == "MQRC_NO_MSG_AVAILABLE")
					{
						System.Threading.Thread.Sleep(100);
						continue;
						//break;//go out from the FOR Loop
					}
					else
					{
						try
						{
							DoProcessAMsg(message);
						}
						catch (Exception ex)
						{
							LogMsg("ProcessAMsg:" + ex.Message);
						}
					}
				}
			}
			catch (Exception ex)
			{
				LogMsg("DoProcess:Err:" + ex.Message);
			}
		}

		private void DoProcessAMsg(string message)
		{
			//add push info to querystring (no blank spaces)
			// string info = "OK2LOAD-FlightSQ020/AKE21304SQ/SIN/620/B";
			string messageType = "";
			if (message != "")
			{
				if (message != "MQRC_NO_MSG_AVAILABLE")
				{
					messageType = iTXML.GetMsgTypeFromMsg(message);
					LogMsg("messageType:" + messageType);
					if (messageType == "ALLOK2L")
					{
						//TODO: add the path
						SessionManager sessMan = new SessionManager();
						sessMan.CreateXMLFileInSessionDirectory(iTXML.GetSessionIdFromMsg(message), iTXML.TransformStringToXmlDoc(message), "ALLOK2L.xml");
					}
					else if (messageType == "SHOWDLS")
					{
						//  message = iTXML.RemoveBadCharsFromSHOWDLS(message);

						//LogTraceMsg("In SHOWDLS msg: " + message);
						//TODO: add the path

						SessionManager sessMan = new SessionManager();
						sessMan.CreateXMLFileInSessionDirectory(iTXML.GetSessionIdFromMsg(message), iTXML.TransformStringToXmlDoc(message), "SHOWDLS.xml");
					}
					else if (messageType == "DFLIGHT")
					{
						try
						{
							XmlDocument depFltTimingsXml = new XmlDocument();
							depFltTimingsXml.LoadXml(message);
							//iTXML.updateDepartureTimings(depFltTimingsXml.SelectSingleNode("//DFLIGHT/URNO").InnerXml, depFltTimingsXml.SelectSingleNode("//DFLIGHT").ChildNodes[1].Name, depFltTimingsXml.SelectSingleNode("//DFLIGHT").ChildNodes[1].InnerText);
						}
						catch (Exception ex)
						{
							LogMsg("Instantiate MainBatchMsgsToXml:" + ex.Message);
						}
					}
					else if (messageType == "AFLIGHT")
					{
						try
						{
							XmlDocument arrFltTimingsXml = new XmlDocument();
							arrFltTimingsXml.LoadXml(message);
							//iTXML.updateArrivalTimings(arrFltTimingsXml.SelectSingleNode("//AFLIGHT/URNO").InnerXml, arrFltTimingsXml.SelectSingleNode("//AFLIGHT").ChildNodes[1].Name, arrFltTimingsXml.SelectSingleNode("//AFLIGHT").ChildNodes[1].InnerText);
						}
						catch (Exception ex)
						{
							LogMsg("Instantiate MainBatchMsgsToXml:" + ex.Message);
						}
					}
					else if (messageType == "DSTasks")
					{
						try
						{
							CtrlTasks.CreateNewTasks(message);
						}
						catch (Exception ex)
						{
							LogMsg("Instantiate MainBatchMsgsToXml:" + ex.Message);
						}
					}
					else if (messageType == "STFS")
					{
						string replyFrom = "";
						try
						{
							//CMsgIn.NewMessageToProcess(iTPaths.GetTOITREKREAD_RESQueueName(), message, iTPaths.GetTOITREKREAD_RESQueueName(), "STFS", out replyFrom);
							CMsgIn.NewMessageToProcess(null,
								iTPaths.GetTOITREKREAD_RESQueueName(),
								message, "ReadResSvc",
								 "STFS", out replyFrom);
						}
						catch (Exception ex)
						{
							LogMsg("STFS:Err:" + ex.Message);
						}
					}

					LogTraceMsg("In final msg: " + message);
				}
			}
		}

		private int getCounterForLoop()
		{
			iTPaths = new iTXMLPathscl();
			return Convert.ToInt32(iTPaths.GetCountForJobMgrLoop());
		}
	}
}