namespace iTrekGetRead_ResSvc
{
    partial class iTrekGetRead_ResSvc
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.iTrekGetRead_ResSvcLog = new System.Diagnostics.EventLog();
            ((System.ComponentModel.ISupportInitialize)(this.iTrekGetRead_ResSvcLog)).BeginInit();
            // 
            // iTrekGetRead_ResSvcLog
            // 
            this.iTrekGetRead_ResSvcLog.Log = "iTrekGetRead_ResLog";
            this.iTrekGetRead_ResSvcLog.Source = "iTrekGetRead_ResSvcLogSource";
            // 
            // iTrekGetRead_ResSvc
            // 
            this.ServiceName = "iTrekGetRead_ResSvc";
            ((System.ComponentModel.ISupportInitialize)(this.iTrekGetRead_ResSvcLog)).EndInit();

        }

        #endregion

        private System.Diagnostics.EventLog iTrekGetRead_ResSvcLog;

    }
}
