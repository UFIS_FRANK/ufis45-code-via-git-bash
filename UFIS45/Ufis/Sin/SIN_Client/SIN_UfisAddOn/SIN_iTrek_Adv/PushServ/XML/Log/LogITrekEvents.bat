@ECHO OFF
SET LOGWD=F:\Program Files\Ufis\iTrekOk2Load\XML\Log

PUSHD %LOGWD%

rem IF /I "%LOGWD%" NEQ "%CD%" GOTO END
IF "%1"=="V"   GOTO SILENTMODE
IF "%1"==""    GOTO INTERMODE
IF "%1"=="/?"   GOTO SHOWUSAGE
GOTO :END

:SILENTMODE
   rem echo %date%
   rem for /f "tokens=1,2,3 delims=/- " %%a in ("%date%") do set dd=%%a&set       mm=%%b&set yy=%%c
   rem set logDir=EL_%mm%%dd%
   IF "%2%"=="" (
     set logDir=EL_MANUALV
   ) else (
     set logDir=%2%
   )
   GOTO DOLOG

:INTERMODE
   set logDir=EL_MANUAL
   echo Creating Logs under %logDir% folder
   GOTO DOLOG

:SHOWUSAGE
   ECHO LogITrekEvents [param]
   ECHO    param is "/?" , Show this help
   ECHO    param is "" (i.e No parameter), Create the Log in "EL_MANUAL" Folder
   ECHO    param is "V", Create the Log in specific date Folder (e.g EL_0425)
   GOTO END

:DOLOG
IF EXIST .\%logDir%\nul RD /S/Q %logDir%
REM GOTO END
MD %logDir%

call getEventLog %logDir% iTFlightMgrLog %1
call getEventLog %logDir% iTJobMgrLog %1
call getEventLog %logDir% iTreckOk2LLog %1
call getEventLog %logDir% iTrekAuthSvcLog %1
call getEventLog %logDir% iTrekCPMLog %1
call getEventLog %logDir% iTrekDelSessDirsSvcLog %1
call getEventLog %logDir% iTrekDeviceLogs %1
call getEventLog %logDir% iTrekErrorLogs %1
call getEventLog %logDir% iTrekETDSvcLog %1
call getEventLog %logDir% iTrekGenericLogs %1
call getEventLog %logDir% iTrekGetRead_ResLog %1
call getEventLog %logDir% iTrekGlobalLogs %1
call getEventLog %logDir% iTrekMsgLog %1
call getEventLog %logDir% iTrekRptLog %1
call getEventLog %logDir% iTrekTracesLogs %1
call getEventLog %logDir% iTrekLoadAllLog %1
call getEventLog %logDir% iTrekOKToLoadLog %1
call getEventLog %logDir% iTrekUpdateSessionsSvcLog %1
call getEventLog %logDir% Application %1
GOTO END


:END
@ECHO ON