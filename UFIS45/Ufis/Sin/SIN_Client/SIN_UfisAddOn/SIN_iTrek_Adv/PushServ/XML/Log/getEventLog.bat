@ECHO OFF
IF "%2"=="" GOTO ERR2
IF "%1"=="" GOTO ERR1

cscript //h:cscript //s //nologo //B
IF EXIST  %1/%2.CSV DEL  %1/%2.CSV
eventquery /l %2 /V /FO CSV >%1/%2.csv
IF NOT "%3"=="V" ECHO Log to %2.csv
GOTO END

:ERR1
   ECHO No Directory
   ECHO Use "getEventLog directoryName logName"
   GOTO END

:ERR2
   ECHO No logName
   ECHO Use "getEventLog directoryName logName"
   GOTO END

:END
