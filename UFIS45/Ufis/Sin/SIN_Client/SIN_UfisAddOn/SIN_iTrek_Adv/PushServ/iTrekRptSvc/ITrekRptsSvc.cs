#define LOGGING_ON
#define TRACING_ON

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;

using IBM.WMQ;
using System.Timers;
using System.Configuration;

using iTrekUtils;
using iTrekXML;
using iTrekWMQ;
using System.Xml;
using iTrekSessionMgr;

using UFIS.ITrekLib.Util;
using UFIS.ITrekLib.Ctrl;
using System.Threading;

namespace iTrekRptSvc
{
    partial class ITrekRptsSvc : ServiceBase
    {

        private const string HEADER_BPSR = "<BPSR>";//Baggage Presentation Timing Summary Report

        iTXMLcl iTXML;
        iTXMLPathscl iTPaths;
        MQQueue queue = null;

        ITrekWMQClass1 wmq1;
        private static string _queueName = null;

        private string QueueName
        {
            get
            {
                if ((_queueName == null) || (_queueName == ""))
                {
                    try
                    {
                        _queueName = iTPaths.GetTOITREKRPTQueueName();
                        //LogTraceMsg("Queue Name : " + _queueName);
                    }
                    catch (Exception ex)
                    {
                        LogMsg("Queue Name Error : " + ex.Message);
                    }
                }
                return _queueName;
            }
        }

        public ITrekRptsSvc()
        {
            InitializeComponent();
            iTXML = new iTXMLcl();
            iTPaths = new iTXMLPathscl();
            LogMsg("Loaded.");
        }

        protected override void OnStart(string[] args)
        {
            wmq1 = new ITrekWMQClass1();
            LogMsg("OnStart.");
            MessageQueueTimer();
        }

        protected override void OnStop()
        {
            LogMsg("OnStop.");
        }

        public void MessageQueueTimer()
        {
            LogMsg("In MessageQueueTimer");

            System.Timers.Timer aTimer = new System.Timers.Timer();
            aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            aTimer.Interval = Convert.ToInt32(iTPaths.GetSERVICESTIMER());
            aTimer.Enabled = true;
        }

        static int cnt = -1;

        private string GetMsgFromMQ()
        {
            string message = "";
            if (wmq1 != null)
            {
                queue = wmq1.getQueueConnection(QueueName,
                    MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
            }
            else
            {
                wmq1 = new ITrekWMQClass1();
                queue = wmq1.getQueueConnection(QueueName,
                    MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
            }

            message = wmq1.GetMessageFromTheQueue(queue).Trim();
            wmq1.closeQueueConnection(queue, wmq1.getQManager);
            return message;
        }

        // Specify what you want to happen when the Elapsed event is raised.
        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            string message = "";
            if (cnt > 500)
            {
                _queueName = "";
                cnt = 0;
                //LogMsg("In OnTimedEvent");
                DoUpdateAllFlightInfoToBackend();
            }
            cnt++;

            try
            {
                message = GetMsgFromMQ();

                //need to convert string to byte array
                //and back to remove unwanted chars
                message = iTUtils.FromUnicodeByteArrayToString(iTUtils.ConvertStringToByteArray(message));
            }
            catch (Exception ex)
            {
                LogMsg("GetMsgFromMQ Error: " + ex.Message);
            }

            if (message != "")
            {
                if (message == "MQRC_NO_MSG_AVAILABLE")
                {
                    //LogTraceMsg("MQRC_NO_MSG_AVAILABLE");
                }
                else
                {
                    try
                    {
                        DoProcess(message);
                    }
                    catch (Exception ex)
                    {
                        LogMsg("Message Process Error : " + ex.Message);
                    }
                }
            }
        }


        private void DoProcess(string message)
        {
            LogMsg("Msg[" + message.Substring(0, 20) + "...]");
            if (message.Substring(0, HEADER_BPSR.Length) == HEADER_BPSR)
            {
                ProcessBagPreTimeSummMsg(message);
            }
            else
            {
                LogMsg("DoProcess:Unknown Message");
            }
            
        }

        private void DoUpdateAllFlightInfoToBackend()
        {
            try
            {
                LogMsg("DoUpdateAllFlightInfoToBackend");
                Thread trd = new Thread(new ThreadStart(this.UpdateAllFlightInfoToBackend));
                trd.IsBackground = true;
                trd.Start();
            }
            catch (Exception ex)
            {
                LogMsg("DoUpdateAllFlightInfoToBackend:Error:" + ex.Message);
            }
        }

        private void UpdateAllFlightInfoToBackend()
        {
            try
            {
                CtrlBagPresentTimeSummaryReport ctrl = new CtrlBagPresentTimeSummaryReport();
                ctrl.UpdateFlightBatchInfoToBackend();
            }
            catch (Exception ex)
            {
                LogMsg("UpdateAllFlightInfoToBackend:Error:" + ex.Message);
            }                       
        }

        private void ProcessBagPreTimeSummMsg(string msg)
        {
            try
            {
                CtrlBagPresentTimeSummaryReport rpt = new CtrlBagPresentTimeSummaryReport();
                rpt.UpdateReplyMsg(msg);
                LogTraceMsg("ProcessBagPreTimeSummMsg");
            }
            catch (Exception ex)
            {
                LogMsg("ProcessBagPreTimeSummMsg:Error:" + ex.Message);
            }
        }

        //[Conditional("TRACING_ON")]
        private void LogTraceMsg(string msg)
        {
            UtilLog.LogToTraceFile("DBSummFlight", msg);
        }

        [Conditional("LOGGING_ON")]
        private void LogMsg(string msg)
        {
           // iTrekRptLog.WriteEntry(UtilTime.ConvDateTimeToUfisFullDTString(System.DateTime.Now) + ":" + msg);
        }
    }
}
