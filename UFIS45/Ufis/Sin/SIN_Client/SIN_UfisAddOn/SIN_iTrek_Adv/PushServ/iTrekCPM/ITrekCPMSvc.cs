using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using IBM.WMQ;
using System.Timers;
using System.Configuration;

using iTrekUtils;
using iTrekXML;
using iTrekWMQ;
using System.Xml;
using iTrekSessionMgr;
using UFIS.ITrekLib.Util;
using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.CommMsg;
namespace iTrekCPM
{
	public partial class ITrekCPMSvc : ServiceBase
	{
		iTXMLPathscl iTPaths;
		MQQueue queue = null;
		ITrekWMQClass1 wmq1;
		private static string _queueName = null;
		private static Object _lockObj = new Object();//to use in synchronisation of single access
		private static bool _IsProcessing = false;
		private const string HEADER_CPM = "<CPMS>";
		private const string HEADER_BRS = "<BRSS>";
		private const string HEADER_BRS_UPDATE = "<BRS>";
		public ITrekCPMSvc()
		{
			InitializeComponent();

			iTPaths = new iTXMLPathscl();
		}
		private string QueueName
		{
			get
			{
				if ((_queueName == null) || (_queueName == ""))
				{
					try
					{
						_queueName = iTPaths.GetTOITREKCPMQueueName();
					}
					catch (Exception ex)
					{
						LogMsg("QueueName:Err:" + ex.Message);
					}
				}
				return _queueName;
			}
		}


		protected override void OnStart(string[] args)
		{
			wmq1 = new ITrekWMQClass1();
			LogMsg("OnStart.");
			MessageQueueTimer();
		}

		protected override void OnStop()
		{
			LogMsg("iTrekCPMSvc OnStop.");
		}

		public void MessageQueueTimer()
		{
			System.Timers.Timer aTimer = new System.Timers.Timer();
			aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
			aTimer.Interval = Convert.ToInt32(iTPaths.GetSERVICESTIMER());
			aTimer.Enabled = true;
		}
		// Specify what you want to happen when the Elapsed event is raised.
		static int cnt = 0;

		private void OnTimedEvent(object source, ElapsedEventArgs e)
		{
			if (cnt > 100)
			{
				_queueName = "";
				cnt = 0;
				LogMsg("OnTimedEvent");
			}
			cnt++;

			if (!_IsProcessing)
			{
				lock (_lockObj)
				{
					if (!_IsProcessing)
					{
						try
						{
							_IsProcessing = true;
							DoProcess();
						}
						catch (Exception ex)
						{
							LogMsg(ex.Message);
						}
						finally
						{
							_IsProcessing = false;
						}
					}
				}
			}
		}

		private void DoProcess()
		{
			try
			{
				for (int i = 0; i < 100; i++)
				{
					string message = "";

					try
					{
						message = GetMsgFromMQ().Trim();
					}
					catch (Exception ex)
					{
						LogMsg("GetMsgFromMQ:Err:" + ex.Message);
						System.Threading.Thread.Sleep(100);
						continue;
					}

					if (message == "MQRC_NO_MSG_AVAILABLE")
					{
						System.Threading.Thread.Sleep(100);
						continue;
						//break;//go out from the FOR Loop
					}
					else
					{
						try
						{
							DoProcessAMsg(message);
						}
						catch (Exception ex)
						{
							LogMsg("ProcessAMsg:Err:" + ex.Message);
						}
					}
				}
			}
			catch (Exception ex)
			{
				LogMsg("DoProcess:Err:" + ex.Message);
			}
		}

		private string GetMsgFromMQ()
		{
			string message = "";
			if (wmq1 != null)
			{
				queue = wmq1.getQueueConnection(QueueName,
					MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
			}
			else
			{
				wmq1 = new ITrekWMQClass1();
				queue = wmq1.getQueueConnection(QueueName,
					MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
			}

			message = wmq1.GetMessageFromTheQueue(queue);
			wmq1.closeQueueConnection(queue, wmq1.getQManager);

			//message = iTUtils.RemoveTroublesomeCharacters(message);
			message = iTUtils.FromUnicodeByteArrayToString(iTUtils.ConvertStringToByteArray(message));

			return message;
		}

		private void ProcessMsgs(string message, string messageType)
		{
			UtilDb db = UtilDb.GetInstance();
			bool commit = false;
			IDbCommand cmd = null;
			string replyFrom = "";
			try
			{
				cmd = db.GetCommand(true);
				if (message.Length < 4000)
				{
					CMsgIn.NewMessageToProcess(cmd, QueueName, message, "CpmSvc", messageType, out replyFrom);
				}
				else
				{
					CMsgIn.NewBatchMessageToProcess(cmd, QueueName, message, "CpmSvc", messageType, out replyFrom);
				}
				commit = true;
			}
			catch (Exception ex)
			{
				LogMsg("ProcessMsgs:Err:" + ex.Message);
				throw;
			}
			finally
			{
				db.CloseCommand(cmd, commit);
			}
		}


		private void DoProcessAMsg(string message)
		{
			try
			{
				if (message != "")
				{
					if (message == "MQRC_NO_MSG_AVAILABLE")
					{
						// LogMsg("MQRC_NO_MSG_AVAILABLE");
					}
					else
					{
						// LogMsg("Msg [" + message + "...]");
						if (message.Substring(0, HEADER_CPM.Length) == HEADER_CPM)
						{
							ProcessMsgs(message, "CPMS");
						}
						else if (message.Substring(0, HEADER_BRS.Length) == HEADER_BRS)
						{
							ProcessMsgs(message, "BRSS");
						}
						else if (message.Substring(0, HEADER_BRS_UPDATE.Length) == HEADER_BRS_UPDATE)
						{
							ProcessMsgs(message, "BRS");
						}
					}
				}
			}
			catch (Exception ex)
			{
				LogMsg("DoProcessAMsg:Err:" + ex.Message);
			}
		}

		private void LogMsg(string msg)
		{
			iTrekCPMLog.WriteEntry(msg);
		}
	}
}