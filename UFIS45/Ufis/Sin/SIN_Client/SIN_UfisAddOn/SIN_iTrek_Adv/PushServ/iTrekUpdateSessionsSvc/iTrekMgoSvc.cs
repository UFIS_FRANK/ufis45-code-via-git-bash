#define LoggingOn
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using IBM.WMQ;
using System.Timers;
using System.Configuration;
using iTrekWMQ;
using System.IO;
using iTrekXML;
//using NUnit.Framework;
using System.Xml;
using iTrekSessionMgr;
using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.CommMsg;
using UFIS.ITrekLib.Util;
using MM.UtilLib;



//NB. Tests at bottom of file
namespace iTrekMgoSvc
{
	/// <summary>
	/// The flight and job xml files are constantly being updated.
	/// These updates must be reflected in the session directories
	/// that the users are actually using to obtain their data.
	/// This service constantly reviews the current session directories
	/// and reflects changes from the latest xml files in them.
	/// </summary>

	public partial class iTrekMgoSvc : ServiceBase
	{
		IDbConnection conn = null;

		iTXMLPathscl iTPaths = null;
		UtilDb db = UtilDb.GetInstance();

		private static UtilTraceLog _traceLog = UtilTraceLog.GetInstance();
		public iTrekMgoSvc()
		{
			InitializeComponent();
			iTPaths = new iTXMLPathscl();
		}

		protected override void OnStart(string[] args)
		{
			string baseDir = AppDomain.CurrentDomain.BaseDirectory;
			DirectoryInfo directoryInfo =
						 Directory.GetParent(baseDir);
			baseDir = directoryInfo.FullName;
			directoryInfo =
						 Directory.GetParent(baseDir);
			baseDir = directoryInfo.FullName;

			string logDir = Path.Combine(Path.Combine(baseDir, "XML"), "LOG");
			if (logDir[logDir.Length - 1] != '\\') logDir += "\\";
			LogTraceMsg(logDir);
			_traceLog.SetTrace(true);
			_traceLog.SetLogDir(logDir);
			_traceLog.SetLogFileName("CMsgOut");

			MessageQueueTimer();
		}

		private void LogTraceMsg(string msg)
		{
			try
			{
				UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("CMsgOut", msg);
			}
			catch { }
		}

		private void LogMsg(string msg)
		{
			try
			{
				iTrekMgoSvcLog.WriteEntry(msg);
				LogTraceMsg(msg);
			}
			catch (Exception)
			{
			}
		}

		protected override void OnStop()
		{
			CloseConnection();
#if LoggingOn
			LogMsg("In onStop.");
#endif
		}

		public void MessageQueueTimer()
		{
			System.Timers.Timer aTimer = new System.Timers.Timer();
			aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
			aTimer.Interval = Convert.ToInt32(iTPaths.GetSERVICESTIMER());
			aTimer.Enabled = true;
		}

		private static bool _IsProcessing = false;
		private static object _lockObj = new object();
		private int _cntProc = 0;
		// Specify what you want to happen when the Elapsed event is raised.
		private void OnTimedEvent(object source, ElapsedEventArgs e)
		{
			try
			{
				if (!_IsProcessing)
				{
					lock (_lockObj)
					{
						if (!_IsProcessing)
						{
							try
							{
								_IsProcessing = true;
								_cntProc++;
								ProcessMessages();
							}
							catch (Exception ex)
							{
								CloseConnection();
								LogMsg("ProcessMessages:PErr:" + ex.Message);
							}
							finally
							{
								if (_cntProc > 50)
								{
									LogMsg("Active");
									_cntProc = 0;
									CloseConnection();
								}
								_IsProcessing = false;
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				LogMsg("OnTimedEvent:Err:" + ex.Message);
			}
		}

		private IDbConnection GetConnection()
		{
			if (conn == null || conn.State == ConnectionState.Closed || conn.State == ConnectionState.Broken)
			{
				CloseConnection();
				try
				{
					if (db == null) db = UtilDb.GetInstance();
					conn = db.GetConnection(true);
				}
				catch (Exception ex)
				{
					conn = null;
					db = null;
					throw;
				}
				if (conn != null)
					LogMsg("Get Connection");
			}
			return conn;

		}

		private void CloseConnection()
		{
			if (conn != null)
			{
				UtilDb.CloseConnection(conn);
				LogTraceMsg("CloseConnection");
			}
		}

		private void ProcessMessages()
		{
			bool isOk = false;
			try
			{
				for (int i = 0; i < 20; i++)
				{
					List<string> msgList = new List<string>();
					isOk = CMsgOut.ProcessAll(GetConnection(), "MgoSvc", out msgList);
					System.Threading.Thread.Sleep(100);
				}
			}
			catch (Exception ex)
			{
				LogMsg("ProcessMessages:Err:" + ex.Message);				
			}
		}
	}
}