#define LoggingOn
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.ServiceProcess;
using System.Text;
using IBM.WMQ;
using System.Timers;
using System.Configuration;
using iTrekWMQ;
using System.IO;
using iTrekXML;
using System.Xml;
using iTrekSessionMgr;


namespace iTrekAuthSvc
{
    /// <summary>
    /// When a request for data has been made by iTrek and put on the 
    /// FROMITREKAUTH queue this service picks up the reply from
    /// TOITREKAUTH queue and puts it in AUTH.xml in the 
    /// relevant users session directory
    /// According to timing set in SERVICESTIMER
    /// </summary>

    public partial class iTrekAuthSvc : ServiceBase
    {
        iTXMLcl iTXML;
        iTXMLPathscl iTPaths;
        MQQueue queue;

        ITrekWMQClass1 wmq1;
       
        public iTrekAuthSvc()
        {
            InitializeComponent();
            LogMsg("Initialisation iTrekAuthSvc");
            iTXML = new iTXMLcl();
            iTPaths = new iTXMLPathscl();

        }
       
        protected override void OnStart(string[] args)
        {
            LogMsg("In OnStart");
           

            MessageQueueTimer();

            wmq1 = new ITrekWMQClass1();
            //queue = wmq1.getQueueConnection(iTPaths.GetTOITREKAUTHQueueName(), MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
        }

        protected override void OnStop()
        {
            LogMsg("In onStop.");

            //queue.Close();
            //wmq1.getQManager.Close();
            //wmq1.getQManager.Disconnect();
        }

        public void MessageQueueTimer()
        {
            LogMsg("In MessageQueueTimer: GetSERVICESTIMER " + iTPaths.GetSERVICESTIMER());
            System.Timers.Timer aTimer = new System.Timers.Timer();
            aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            // Set the Interval by SERVICESTIMER
            aTimer.Interval = Convert.ToInt32(iTPaths.GetSERVICESTIMER());
            aTimer.Enabled = true;
        }

        private static bool _IsProcessing = false;
        private static object _lockObj = new object();

        // Specify what you want to happen when the Elapsed event is raised.
        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            if (!_IsProcessing)
            {
                lock (_lockObj)
                {
                    try
                    {
                        _IsProcessing = true;
                        DoProcess();
                    }
                    catch (Exception ex)
                    {
                        LogMsg(ex.Message);
                    }
                    finally
                    {
                        _IsProcessing = false;
                    }
                }
            }
        }

        private void DoProcess()
        {

            //int maxCnt = getCounterForLoop();
            try
            {
                for (int i = 0; i < 10; i++)
                {
                    string message = "";
                    message = GetMessageFromQueue();
                    if (message == "MQRC_NO_MSG_AVAILABLE")
                    {
                        break;//go out from the FOR Loop
                    }
                    else
                    {
                        try
                        {
                            DoProcessAMsg(message);
                        }
                        catch (Exception ex)
                        {
                            LogMsg("ProcessAMsg:" + ex.Message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                LogMsg("In Do Process" + ex.Message);
            }
        }

        private void DoProcessAMsg(string message)
        {
            if (message != "")
            {
                if (message != "MQRC_NO_MSG_AVAILABLE")
                {
                    string messageType = "";
                    messageType = iTXML.GetMsgTypeFromMsg(message);

                    //add push info to querystring (no blank spaces)
                    //string info = "OK2LOAD-FlightSQ020/AKE21304SQ/SIN/620/B";

                    SessionManager sessMan = new SessionManager();
                    try
                    {
                        if (messageType == "AUTH")
                        {
                            sessMan.CreateXMLFileInSessionDirectory(iTXML.GetSessionIdFromMsg(message), iTXML.TransformStringToXmlDoc(message), "AUTH.xml");
                            LogMsg(message);
                        }
                        else if (messageType == "USERS")
                        {
                            sessMan.CreateXMLFileInSessionDirectory(iTXML.GetSessionIdFromMsgSibling(message), iTXML.TransformStringToXmlDoc(message), "AUTH2.xml");
                            LogMsg(message);
                        }
                    }
                    catch (Exception authEx)
                    {
                        LogMsg(authEx.Message);
                    }
                }
            }
        }

        private void LogMsg(string msg)
        {
            try
            {
                if (msg != null)
                {
                    int len = msg.Length;
                    if (len > 100) len = 100;
                    iTrekAuthSvcLog.WriteEntry(msg.Substring(0, len));
                }
            }
            catch (Exception ex)
            {

                iTrekAuthSvcLog.WriteEntry("Exc in Log" + ex.Message);
            }
           
        }

        private void LogTraceMsg(string msg)
        {
            LogMsg(msg);
        }

        private string GetMessageFromQueue()
        {
            string message = "";
            iTXMLPathscl iTPaths = new iTXMLPathscl();

            iTXML = new iTXMLcl();
            if (wmq1 != null)
            {
                queue = wmq1.getQueueConnection(iTPaths.GetTOITREKAUTHQueueName(), MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
            }
            else
            {
                wmq1 = new ITrekWMQClass1();
                queue = wmq1.getQueueConnection(iTPaths.GetTOITREKAUTHQueueName(), MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);

            }
            message = wmq1.GetMessageFromTheQueue(queue);
            wmq1.closeQueueConnection(queue, wmq1.getQManager);
            //message = ITrekWMQClass1.GetMessageFromTheQueue(iTPaths.GetTOITREKAUTHQueueName());
            return message;
        }

        private int getCounterForLoop()
        {

            iTPaths = new iTXMLPathscl();
            return Convert.ToInt32(iTPaths.GetCountForJobMgrLoop());

        }
    }
}
