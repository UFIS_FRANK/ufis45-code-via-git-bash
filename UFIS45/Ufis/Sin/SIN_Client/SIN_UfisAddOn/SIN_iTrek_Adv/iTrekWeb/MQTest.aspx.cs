using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using iTrekWMQ;
using IBM.WMQ;

public partial class MQTest : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSend_Click(object sender, EventArgs e)
    {
        ITrekWMQClass1 mq = new ITrekWMQClass1();
        mq.putTheMessageIntoQueue(txtMsgToSend.Text.Trim(), txtMQNameSend.Text.Trim());

    }
    protected void btnRcv_Click(object sender, EventArgs e)
    {
        txtRcvMsg.Text = GetMsgFromMQ(txtMQNameRcv.Text.Trim());
    }

    private string GetMsgFromMQ(String QueueName)
    {
        string message = "";
        MQQueue queue = null;

        ITrekWMQClass1 wmq1=null;

        if (wmq1 != null)
        {
            queue = wmq1.getQueueConnection(QueueName,
                MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
        }
        else
        {
            wmq1 = new ITrekWMQClass1();
            queue = wmq1.getQueueConnection(QueueName,
                MQC.MQOO_INPUT_AS_Q_DEF + MQC.MQOO_FAIL_IF_QUIESCING);
        }

        message = wmq1.GetMessageFromTheQueue(queue).Trim();
        wmq1.closeQueueConnection(queue, wmq1.getQManager);
        return message;
    }
}
