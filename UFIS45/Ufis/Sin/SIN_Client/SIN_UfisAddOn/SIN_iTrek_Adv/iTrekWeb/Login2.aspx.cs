//#define TESTING_ON
//#define PRODUCTION_RELEASE

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

using UFIS.Web.ITrek.Sec;
using UFIS.Web.ITrek.WebMgr;

using MMSoftware.MMWebUtil;
using UFIS.ITrekLib.Util;
using UFIS.ITrekLib.ENT;

public partial class Login2 : System.Web.UI.Page
{
   protected void Page_Load(object sender, EventArgs e)
   {
      MgrNavigation.InitServerInfo(Request);
	  //string ver = System.Configuration.ConfigurationManager.AppSettings["Version"];
	  string ver = UFIS.ITrekLib.Config.ITrekConfig.GetVersion();

	  lblVer.Text = "iTrek " + ver + ". " + UFIS.ITrekLib.Config.ITrekConfig.GetAppEnvString() +
         //Request.ApplicationPath + "." + // '/iTrekWeb
         //Request.AppRelativeCurrentExecutionFilePath + "." +// '~/Login2.aspx
//         ". CurrentExecutionFilePath:" + Request.CurrentExecutionFilePath + Environment.NewLine +
//         ". FilePath:" + Request.FilePath + Environment.NewLine +
//         ". IsSecureConnection:" + Request.IsSecureConnection + Environment.NewLine +
//         ". Path:" + Request.Path + Environment.NewLine +
//         ". PathInfo:" + Request.PathInfo + Environment.NewLine +
//         ". PhysicalApplicationPath:" + Request.PhysicalApplicationPath + Environment.NewLine +
//         ". PhysicalPath:" + Request.PhysicalPath + Environment.NewLine +
         //". RawUrl:" + Request.RawUrl + 
         //". URL.ToString:" + Request.Url.ToString() +
         //". :" + @"http:" + Request.ServerVariables["SERVER_NAME"] + Request.RawUrl.ToString()+
         ""; 
      UFIS.Web.ITrek.WebMgr.MgrApp webAppMgr = new MgrApp();
      //webAppMgr.LoadConfig2(Application);
      UFIS.Web.ITrek.WebMgr.MgrApp.LoadConfig(Application);

      txtUserId.Focus();
   }

   private string MyHttpServerRootPath
   {
      get
      {
         return @"http://" + Request.ServerVariables["SERVER_NAME"] + Request.ApplicationPath;
      }
   }

   private void KeepBrowserInfo()
   {
      try
      {
         int height = 1024;
         int width = 768;
         try
         {
            height = int.Parse(hfScrHeight.Value);
            width = int.Parse(hfScrWidth.Value);
            LogTraceMsg(string.Format("KeepBrowserInfo:w<{0}>,h<{1}>", width, height));
         }
         catch (Exception ex)
         {
            LogTraceMsg("KeepBrowserInfo:Err:" + ex.Message);
         }
         MgrSess.SetBrowserHeight(Session, height);
         MgrSess.SetBrowserWidth(Session, width);
      }
      catch (Exception)
      {
      }
   }

   protected void btnLogin_Click(object sender, EventArgs e)
   {
      string errMsg = "";
      try
      {
         string userId = "00" + txtUserId.Text;
		  //#if PRODUCTION_RELEASE
		  string pwd = txtPwd.Text;
		 if (UFIS.ITrekLib.Config.ITrekConfig.GetAppEnv() != UFIS.ITrekLib.Config.EnumEnv.Prod)
		 {
			 //#else
			 pwd = "00" + txtPwd.Text;
			 //#endif
		 }

         if (CtrlSec.GetInstance().IsAuthenticateUser(userId, pwd, Session, "W"))
         {
            LogTraceMsg(  "Login:Sess:" + Session.SessionID + ",user:" + userId);
            KeepBrowserInfo();
            //FormsAuthentication.RedirectFromLoginPage(txtUserId.Text, false);
            //WebUtil.AlertMsg(Page, "Auth");
            FormsAuthentication.SetAuthCookie(txtUserId.Text, false);
            EntUser user = CtrlSec.GetInstance().AuthenticateUser(Session, Response);
            if (user.UserDept == EnUserDept.Baggage)
            {//Redirect the Baggage User to Baggage Main Screen
               //Response.Redirect("~/Web/iTrekB/BATerminalFlightUlds.aspx");
               Response.Redirect(MyHttpServerRootPath + "/Web/iTrekB/BATerminalFlightUlds.aspx");
            }
            else
            {//Redirect the other user to Web Flight Information Screen               
               //Response.Redirect("~/Web/iTrek/FlightInfo.aspx");
               Response.Redirect(MyHttpServerRootPath + "/Web/iTrek/FlightInfo.aspx");
            }
         }
         else
         {
            errMsg = ((EntUser)(MgrSess.GetUserInfo(Session))).AuthError;
            if ((errMsg == null) || (errMsg == ""))
            {
               WebUtil.AlertMsg(Page, "Invalid user id or password!\nPlease try again.");
            }
            else
            {
               WebUtil.AlertMsg(Page, "Login failed due to " + errMsg);
            }
         }
      }
      catch (ApplicationException ex)
      {
         WebUtil.AlertMsg(Page, "Login failed due to " + ex.Message);
      }
      catch (Exception ex)
      {
         WebUtil.AlertMsg(Page, "Login failed due to " + ex.Message);
      }
   }

   protected void btnCancel_Click(object sender, EventArgs e)
   {
      txtUserId.Text = "";
      txtPwd.Text = "";
   }

   private void LogTraceMsg(string msg)
   {
      try
      {
         MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("Web_Login", msg);
      }
      catch (Exception)
      {
         UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("Web_Login", msg);
      }
   }
}