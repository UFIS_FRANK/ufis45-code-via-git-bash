<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login2.aspx.cs" Inherits="Login2" Theme="SATSTheme" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Login</title>
    <script type="text/javascript" language="javascript">
        function CaptureScreenSize()
        {
            try
            {
               document.getElementById("hfScrWidth").value = screen.availWidth;
               document.getElementById("hfScrHeight").value = screen.availHeight;               
            }catch(err){}
        }
    </script>
</head>
<body onload="javascript:CaptureScreenSize();">
    <form id="form1" runat="server">
    <div class="Logo" id="logo">
            <img src="Images/SATSlogo.gif" alt="SATS" width="100px" />
    </div>
    <div id="cont" style="text-align:center">
    <table style="width: 100%">
            <tr>
                <td style="height: 50px; font-weight: bold; vertical-align: middle; text-align: center;" align="center" valign="middle">
                    &nbsp;<asp:Label ID="lblVer" runat="server" CssClass="Heading" Text="iTrek"></asp:Label></td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                </td>
                            <td style="width: 80px; text-align:left;">
                                User ID</td>
                            <td>
                                <asp:TextBox ID="txtUserId" runat="server" Width="150px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                                </td>
                            <td style="width: 80px; text-align:left;">
                                Password</td>
                            <td>
                                <asp:TextBox ID="txtPwd" runat="server" TextMode="Password" Width="150px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td >
                            </td>
                            <td >
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                    <asp:Label ID="lblMsg" runat="server" Visible="False"></asp:Label></td>
            </tr>
            <tr>
                <td >
                    <table >
                        <tr>
                            <td style="width: 80px">
                            </td>
                            <td style="width: 100px">
                                <asp:Button ID="btnLogin" runat="server" Text="Login" OnClick="btnLogin_Click" EnableViewState="False" /></td>
                            <td style="width: 85px">
                                <asp:Button ID="btnCancel" runat="server" Text="Clear" CausesValidation="False" OnClientClick='document.getElementById("txtUserId").value = ""; document.getElementById("txtPwd").value = "";' UseSubmitBehavior="False" Width="50px" OnClick="btnCancel_Click" EnableViewState="False" /></td>
                        </tr>
                    </table>
                    <asp:HiddenField ID="hfScrWidth" runat="server" />
                    <asp:HiddenField ID="hfScrHeight" runat="server" />
                </td>
            </tr>
        <tr>
            <td align="center">
                <br />
                <br />
                <br />
                <br />
                &nbsp;<a href='http://www.ufis-as.com'>Powered by UFIS AS (c) 2006</a>
                </td>
        </tr>
        </table>
    </div>
    </form>
</body>
</html>
