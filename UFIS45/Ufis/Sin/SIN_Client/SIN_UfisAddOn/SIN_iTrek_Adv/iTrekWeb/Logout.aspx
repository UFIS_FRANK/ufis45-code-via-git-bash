<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Logout.aspx.cs" Inherits="Logout" Theme="SATSTheme"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Logout</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
            <img src="Images/SATSlogo.gif" alt="SATS" width="100px;" />
    </div>
    <div style="text-align:center; vertical-align:middle;" >
        <br />
        <br />
        <table>
        <tr><td>
        <asp:Label ID="lblMsg" runat="server"></asp:Label><br />
        </td>
        </tr>
        <tr><td></td></tr>
        <tr><td></td></tr>
        <tr><td style="height: 26px;">
        <asp:Button ID="btnLogin" runat="server" OnClick="btnLogin_Click" Text="Login Again" EnableViewState="False" />
        </td></tr>
        </table>
        </div>
    </form>
</body>
</html>
