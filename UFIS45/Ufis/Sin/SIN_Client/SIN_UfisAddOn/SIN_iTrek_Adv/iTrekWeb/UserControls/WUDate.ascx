<!--
/* *******************************************************
 * This code created and or modified by Aung Moe
 * Please refer all questions or comments to moepsmm@yahoo.com
 * 
 * You can use this code however you like, but please, make sure you give me 
 * credit on any publishings you do from this code. 
 * 
 * No warrenty is implied for any code here.
 *  ******************************************************* 
 */
 //-->
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WUDate.ascx.cs" Inherits="UserControls_WUDate" %>
<%@ Register Src="Calendar.ascx" TagName="Calendar" TagPrefix="uc1" %>
<table>
<tr>
<td>
<asp:Label ID="lblDate" runat="server"></asp:Label>
<asp:TextBox ID="txtDate" runat="server" MaxLength="11" Width="85px" OnTextChanged="txtDate_TextChanged"></asp:TextBox>
<asp:ImageButton CausesValidation="false" PostBackUrl="" ID="btnShowCalendar" runat="server" ImageUrl="~/images/calendar.jpg" />&nbsp;
<asp:TextBox ID="txtHH" runat="server" MaxLength="2" Width="25px"></asp:TextBox>&nbsp;
<asp:Label ID="lblTimeSeperator" runat="server" Text=":" />&nbsp;
<asp:TextBox ID="txtMM" runat="server" MaxLength="2" Width="25px"></asp:TextBox>&nbsp;
<asp:RangeValidator ID="rvHour" runat="server" MinimumValue="0" MaximumValue="23" ControlToValidate="txtHH" ErrorMessage="* Invalid Hour" ValidationGroup="Calendar" ></asp:RangeValidator>
<asp:RangeValidator ID="rvMinute" runat="server" MinimumValue="0" MaximumValue="59" ControlToValidate="txtMM" ErrorMessage="* Invalid Minute"
    ValidationGroup="Calendar"></asp:RangeValidator>
</td>
</tr>
</table>
<uc1:Calendar ID="Calendar1" runat="server" />

