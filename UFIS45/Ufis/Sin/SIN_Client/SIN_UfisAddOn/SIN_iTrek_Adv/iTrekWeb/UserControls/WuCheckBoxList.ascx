<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WuCheckBoxList.ascx.cs" Inherits="UserControls_WuCheckBoxList" %>
<asp:CheckBox ID="cbHead" runat="server" CssClass="SubHeading" Font-Bold="True" Font-Italic="True" /> &nbsp;&nbsp;&nbsp; 
<asp:CustomValidator ID="cv1" runat="server" ErrorMessage="CustomValidator" ></asp:CustomValidator>
<asp:CheckBoxList ID="cbDet" runat="server" RepeatColumns="10" RepeatDirection="Horizontal"
    Width="900px">
</asp:CheckBoxList>
<asp:HiddenField ID="hfId" runat="server" />

