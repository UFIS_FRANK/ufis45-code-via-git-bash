<!--
/* *******************************************************
 * This code created and or modified by Chris Lasater
 * http://www.geocities.com/lasaterconsult
 * Please refer all questions or comments to chris.lasater@gmail.com
 * 
 * You can use this code however you like, but please, make sure you give me 
 * credit on any publishings you do from this code. 
 * 
 * No warrenty is implied for any code here.
 * Modified by Aung Moe to use within User Control
 *  ******************************************************* 
 */
 //-->
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Calendar.ascx.cs" Inherits="UserControls_Calendar" %>
<script type="text/javascript" language="javascript">

function <%= this.ClientName %>_SetDisplay(doDisplay)
{
	
	if(doDisplay == true)
	{
		document.getElementById('<%= this.ClientName %>').style.display='block';      
        document.getElementById('<%= this.ClientName %>_overShelf').style.display = 'block';
        document.getElementById('<%= MonthChanging.ClientID %>').value="True";
	}
	else
	{
		document.getElementById('<%= this.ClientName %>').style.display='none';   
		document.getElementById('<%= this.ClientName %>_overShelf').style.display = 'none';
        document.getElementById('<%= MonthChanging.ClientID %>').value="False";
	}
	
    document.getElementById('<%= this.ClientName %>_overShelf').style.width = document.getElementById('<%= this.ClientName %>').offsetWidth;
    document.getElementById('<%= this.ClientName %>_overShelf').style.height = document.getElementById('<%= this.ClientName %>').offsetHeight;
    document.getElementById('<%= this.ClientName %>_overShelf').style.top = document.getElementById('<%= this.ClientName %>').offsetTop;
    document.getElementById('<%= this.ClientName %>_overShelf').style.left = document.getElementById('<%= this.ClientName %>').offsetLeft;
    document.getElementById('<%= this.ClientName %>_overShelf').style.x = document.getElementById('<%= this.ClientName %>').offsetTop;
    document.getElementById('<%= this.ClientName %>_overShelf').style.left = document.getElementById('<%= this.ClientName %>').offsetLeft;
    document.getElementById('<%= this.ClientName %>').style.zIndex = 99999;
    document.getElementById('<%= this.ClientName %>_overShelf').style.zIndex = 99999;
    
}

function <%= this.ClientName %>_PopulateText()
{
}

</script>

<iframe id="<%= this.ClientName %>_overShelf" scrolling="no" frameborder="0" 
style="position:absolute; top:0px; left:0px; display:none; "></iframe>
<div id="<%= this.ClientName %>" style=" position:absolute; display:none" >
<table background="/<%= ConfigurationManager.AppSettings["ApplicationName"] %>/images/Block.gif" 
    style="background-repeat:no-repeat"  cellpadding="1"  cellspacing="0">
 <tr>
   <td height=14 align="right" valign="top" 
   style="font-size:10px; font-weight:bold; font-family:Arial, Helvetica, sans-serif; color:#FFFFFF; background-color:#CCCCCC; ">SELECT A DATE&nbsp;&nbsp;&nbsp;
        <a href="#" onClick="<%= this.ClientName %>_SetDisplay(false);">
        <img border="0" src="/<%= ConfigurationManager.AppSettings["ApplicationName"] %>/images/x.gif" align="top" ></a>
   </td>
 </tr>
 <tr >
   <td  >
        <asp:Calendar ID="DateCalendar" runat="server"                                 
            OnSelectionChanged="DateCalendar_SelectionChanged" OnVisibleMonthChanged="DateCalendar_VisibleMonthChanged">                                  
        </asp:Calendar>
   </td>
 </tr>
 
</table>
    
</div>
<asp:HiddenField ID="DateValue" runat="server" />
<asp:HiddenField ID="MonthChanging" runat="server" Value="False"  />
<asp:HiddenField ID="RegisteredControlName" runat="server"/>
<asp:HiddenField ID="RegControlCliendID" runat="server"/>
<script type="text/javascript" language="javascript">

<% if(!IsMonthChanging) { %>
   <%= this.ClientName %>_SetDisplay(false);   
<% } else { %>
    <%= this.ClientName %>_SetDisplay(false); 
     <%= this.ClientName %>_SetDisplay(true);    
<% } %>
    
//alert("DIV:" + document.getElementById('<%= this.ClientName %>').style.zIndex);
//alert("IFRAME:" + document.getElementById('<%= this.ClientName %>_overShelf').style.zIndex);

</script>