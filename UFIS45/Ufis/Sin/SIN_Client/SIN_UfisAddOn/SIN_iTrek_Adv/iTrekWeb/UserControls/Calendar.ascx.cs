/* *******************************************************
 * This code created and or modified by Chris Lasater
 * http://www.geocities.com/lasaterconsult
 * Please refer all questions or comments to chris.lasater@gmail.com
 * 
 * You can use this code however you like, but please, make sure you give me 
 * credit on any publishings you do from this code. 
 * 
 * No warrenty is implied for any code here.
 * 
 *  ******************************************************* 
 */

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Globalization;

public partial class UserControls_Calendar : System.Web.UI.UserControl
{
    

    protected void Page_Load(object sender, EventArgs e)
    {
        
        //if (!IsMonthChanging)
        //{            
        //    this.SelectedDate = DateCalendar.SelectedDate.ToShortDateString();
        //    if (FindRegisteredControl() is TextBox)
        //    {
        //        ((TextBox)FindRegisteredControl()).Text = this.SelectedDate;
        //    }
        //    else if (FindRegisteredControl() is HiddenField)
        //    {
        //        ((HiddenField)FindRegisteredControl()).Value = this.SelectedDate;
        //    }
        //}
    }
    
    public void RegisterControl(ref TextBox controlToRegister)
    {
        controlToRegister.TextChanged += new EventHandler(TextBox_TextChanged);
        controlToRegister.DataBinding += new EventHandler(TextBox_DataBinding);
        controlToRegister.AutoPostBack = true;
        RegisteredControlName.Value = controlToRegister.ID;
        RegControlCliendID.Value = controlToRegister.ClientID;
    }
    public void RegisterControl(ref HiddenField controlToRegister)
    {
        controlToRegister.DataBinding += new EventHandler(HiddenField_DataBinding);        
        RegisteredControlName.Value = controlToRegister.ID;
        RegControlCliendID.Value = controlToRegister.ClientID;
    }     

    public string ClientName
    {
        get { return this.ClientID + "_Selectable"; }
    }
    public string SelectedDate
    {
        get
        {
            DateTime dt = new DateTime();
            string defDate = GetDateString(dt);
            if (DateValue.Value != defDate)
            {
                return DateValue.Value;
            }
            else
                return "";
           
        }
        set
        {
            try
            {                
                DateCalendar.SelectedDate = Convert.ToDateTime(value);
                DateCalendar.VisibleDate = Convert.ToDateTime(value);
                DateValue.Value = GetDateString(DateCalendar.SelectedDate) ;
            }
            catch (Exception)
            {
                DateCalendar.SelectedDate = Convert.ToDateTime(DateValue.Value);
                DateCalendar.VisibleDate = Convert.ToDateTime(DateValue.Value);
            }
        }
    }

    private string GetDateString(DateTime dt)
    {
        return String.Format("{0:dd MMM yyyy}", dt);
    }

    
    protected bool IsMonthChanging
    {
        get { return Convert.ToBoolean(MonthChanging.Value); }
    }

    #region Events
    protected void DateCalendar_SelectionChanged(object sender, EventArgs e)
    {
        this.SelectedDate = DateCalendar.SelectedDate.ToLongDateString();
        if (FindRegisteredControl() is TextBox)
        {
            ((TextBox)FindRegisteredControl()).Text = this.SelectedDate;
        }
        else if (FindRegisteredControl() is HiddenField)
        {
            ((HiddenField)FindRegisteredControl()).Value = this.SelectedDate;
        }
        if (Page.IsPostBack)
        {            
            MonthChanging.Value = "False";
        }
    }

    protected void DateCalendar_VisibleMonthChanged(object sender, MonthChangedEventArgs e)
    {
        MonthChanging.Value = "True";        
    }

    protected void TextBox_TextChanged(object sender, EventArgs e)
    {
        this.SelectedDate = ((TextBox)FindRegisteredControl()).Text;        
    }
    protected void TextBox_DataBinding(object sender, EventArgs e)
    {
        this.SelectedDate = ((TextBox)FindRegisteredControl()).Text;
    }   
   
    protected void HiddenField_DataBinding(object sender, EventArgs e)
    {
        this.SelectedDate = ((HiddenField)FindRegisteredControl()).Value;
    }

    private Control FindRegisteredControl()
    {
        Control control = null;
        if (Page.Master != null)
        {
            foreach (Control innercontrol in Page.Form.Controls)
            {
                control = FindRegControl(innercontrol);
                if (control != null) break;
            }
        }
        else
        {
            control = Page.FindControl(RegisteredControlName.Value);
            control = FindRegControl(control);
            if (control == null)
            {
                foreach (Control innercontrol in Page.Form.Controls)
                {
                    control = FindRegControl(innercontrol);
                    if (control != null) break;
                }
            }
        }
        return control;
    }

    //private Control FindRegControl(ControlCollection ctrlCol)
    //{
    //    Control result = null;
    //    result = ctrlCol.F
    //    foreach (Control ctrl in ctrlCol)
    //    {
    //        result = ctrl.
    //    }
    //}

    private Control FindRegControl(Control ctrl)
    {
        Control control = null;
        if ((ctrl is ContentPlaceHolder) || (ctrl is UserControl))
        {
            control = ctrl.FindControl(RegisteredControlName.Value);
            if (control == null)
            {
                foreach (Control innerControl in ctrl.Controls)
                {
                    control = FindRegControl(innerControl);
                    if (control != null) break;
                }
            }
        }
        else
        {
            control = ctrl;
        }
        if (!((control != null) && (control.ClientID == RegControlCliendID.Value)))
        {
            control = null;
        }
        return control;
    }

    #endregion

}
