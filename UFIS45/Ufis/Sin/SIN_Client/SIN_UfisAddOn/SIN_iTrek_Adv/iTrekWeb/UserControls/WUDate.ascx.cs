using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.CustomException;

public partial class UserControls_WUDate : System.Web.UI.UserControl
{

    protected void Page_Init(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;
        btnShowCalendar.OnClientClick = "javascript:" + Calendar1.ClientName + "_SetDisplay(true); return false;";
        this.Calendar1.RegisterControl(ref txtDate);
        this.Page.LoadComplete += new EventHandler(Page_LoadComplete);
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Page_LoadComplete(object sender, EventArgs e)
    {
        if (Page.IsPostBack) return;
        //if (txtDate.Text.Trim() != "")
        //{
        //    this.txtDate.Text = String.Format("{0:dd MMM yyyy}", DateTime.Now);
        //    this.txtDate.DataBind();
        //}

    }
    protected void txtDate_TextChanged(object sender, EventArgs e)
    {
        Calendar1.SelectedDate = txtDate.Text;
    }

    public DateTime GetDateTime()
    {
        DateTime dt = new DateTime();
        try
        {
            if (txtDate.Text.Trim() == "")
            {
                throw new NoDataException();
            }
            string stHH = txtHH.Text.PadLeft(2, '0');
            string stMM = txtMM.Text.PadLeft(2, '0');
            int hh = Convert.ToInt32(stHH);
            int mm = Convert.ToInt32(stMM);
            dt = Convert.ToDateTime(txtDate.Text);
            dt = dt.AddHours(hh);
            dt = dt.AddMinutes(mm);
        }
        catch (NoDataException)
        {
            throw new NoDataException();
        }
        catch (Exception)
        {
            throw new ApplicationException("Invalid Date Time");
        }
        return dt;
    }

    public void SetDateTime(DateTime dt)
    {
        if (dt == new DateTime())
        {
            txtDate.Text = "";
            txtHH.Text = "";
            txtMM.Text = "";
        }
        else
        {
            txtDate.Text = String.Format("{0:dd MMM yyyy}", dt);
            txtHH.Text = String.Format("{0:HH}", dt);
            txtMM.Text = String.Format("{0:mm}", dt);
        }
    }

    public DateTime DateTimeValue
    {
        get
        {
            return GetDateTime();
        }
        set
        {
            SetDateTime(value);
        }
    }

    public string DateTimeLabel
    {
        set
        {
            if (value == null) value = "";
            lblDate.Text = value;
        }
    }

    public bool ReadOnly
    {
        set
        {
            btnShowCalendar.Visible = !value;
            txtDate.ReadOnly = value;
            txtHH.ReadOnly = value;
            txtMM.ReadOnly = value;
        }
    }

    public bool ShowTime
    {
        set
        {
            if (value == false)
            {
                txtHH.Text = "00";
                txtMM.Text = "00";
                
            }
            txtHH.Visible = value;
            txtMM.Visible = value;
            lblTimeSeperator.Visible = value;
        }
    }
}
