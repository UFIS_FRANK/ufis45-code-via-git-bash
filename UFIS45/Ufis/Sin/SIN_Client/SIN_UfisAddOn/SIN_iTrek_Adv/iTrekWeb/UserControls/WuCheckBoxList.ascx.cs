using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;

public partial class UserControls_WuCheckBoxList : System.Web.UI.UserControl
{
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
    }

    protected override void OnPreRender(EventArgs e)
    {
        base.OnPreRender(e);
        RegJs();
    }
    //    Page_Load(object sender, EventArgs e)
    //{

    //    if (!IsPostBack)
    //    {
    //        //LoadData();
    //    }
    //    RegJs();
    //}

    public string ValidationGroup
    {
        get
        {
            return cv1.ValidationGroup;
        }
        set
        {
            if (value == null) cv1.ValidationGroup = "";
            else cv1.ValidationGroup = value;
        }
    }

    private const string JSID_VALIDATE = "WuCheckBoxListValidation";

    public void PopulateData(List<IdText> arrData, string headName, string stCssClass, bool checkHead, string id)
    {
        cbHead.Text = headName;
        cbHead.Checked = checkHead;
        hfId.Value = id;
        int cnt = arrData.Count;
        IdText idText;

        for (int i = 0; i < cnt; i++)
        {
            idText = arrData[i];
            cbDet.Items.Add(new ListItem(idText.Text, idText.Id, true));
            cbDet.Items[i].Selected = idText.Selected;
            cbDet.CssClass = stCssClass;
        }
        cbHead.CssClass = stCssClass;
    }

    private const string JS_CHKBOX_HEAD_DET = "CheckBoxHeadNDet";

    private void RegJs()
    {
        string jsFName = JSID_VALIDATE + "$F$" + this.UniqueID;
        cv1.ErrorMessage = "* Not selected ALL!!!";
        cv1.ClientValidationFunction = jsFName;
        //RegJsCheckBoxListSelect();
        //RegJsCheckBoxHeadNDetSelect();
        RegJsEqualWidth();
        RegJsHasCheck();
        string varToValidate = "Validate" + this.ClientID;
        cbHead.Attributes.Add("onclick",
            "CheckBoxListSelect('" + cbDet.ClientID + "',this.checked);");
        if (!Page.ClientScript.IsClientScriptBlockRegistered(jsFName))
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
                jsFName,
@"
  function " + jsFName + @"(sender, args)
  {
       args.IsValid = " + JS_CHKBOX_HEAD_DET + "('" + cbHead.ClientID + "','" + cbDet.ClientID + @"');
  }
", true);
        }
    }

    public string CheckListClientId
    {
        get { return cbDet.ClientID; }
    }

    private void RegJsHasCheck()
    {
        string jsFName = "HasCheck$F$" + this.UniqueID;
        if (!Page.ClientScript.IsClientScriptBlockRegistered(jsFName))
        {
            Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
                jsFName, 
                "function " + jsFName + @"(){
                   return HasAnyCheckInCheckBoxList('" + CheckListClientId + @"');
                 }", true);
        }
    }

    private void RegJsEqualWidth()
    {
        const string JS_EQU_WIDTH = "EquWidth";
        string JS_SET_WIDTH = "SetWidthChkBoxList" + cbDet.ClientID;
        if (!Page.ClientScript.IsStartupScriptRegistered(JS_SET_WIDTH))
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(),
                JS_SET_WIDTH,
                JS_EQU_WIDTH + @"('" + cbDet.ClientID + "','" + 95 / cbDet.RepeatColumns + "%');", true);
        }
    }


//    private void RegJsCheckBoxHeadNDetSelect()
//    {
//        const string JS_CHKBOX_HEAD_DET = "CheckBoxHeadNDet";
//        if (!Page.ClientScript.IsClientScriptBlockRegistered(JS_CHKBOX_HEAD_DET))
//        {
//            Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
//                JS_CHKBOX_HEAD_DET,
//@"
//function " + JS_CHKBOX_HEAD_DET + @"(cbHead, cbDet)
//{
////  alert( 'checking(' + cbHead + ',' + cbDet + ')' );
//  var isOk = true;
//  var isHeadChk = document.getElementById(cbHead).checked;
//  if (isHeadChk)
//  {
//    var chkBoxList = document.getElementById(cbDet);
//    var chkBoxCount= chkBoxList.getElementsByTagName('input');
//    
//    if (chkBoxCount!=null)
//    {
//      for(var i=0;i<chkBoxCount.length;i++)
//      {
//        if (!chkBoxCount[i].checked)
//        {
//          isOk = false;
//          break;
//        }
//      }
//    }
//  }
//  return isOk; 
//}
//", true );
//        }

//    }

//    private void RegJsCheckBoxListSelect()
//    {
//        if (!Page.ClientScript.IsClientScriptBlockRegistered("CheckBoxListSelect"))
//        {
//            Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
//                "CheckBoxListSelect",
//@"
//function CheckBoxListSelect(cbControl, state)
//{
//  var chkBoxList = document.getElementById(cbControl);
//  var chkBoxCount= chkBoxList.getElementsByTagName('input');
//  if (chkBoxCount!=null)
//  {
//     for(var i=0;i<chkBoxCount.length;i++)
//     {
//        chkBoxCount[i].checked = state;
//     }
//  }
//  return false; 
//}
//", true);
//        }
//    }

    public List<string> GetSelectedData(out bool headSelected,out string id)
    {
        List<string> result = new List<string>();
        id = hfId.Value;
        headSelected = cbHead.Checked;
        foreach (ListItem item in cbDet.Items)
        {
            if (item.Selected)
            {
                result.Add(item.Value);
            }
        }
        return result;
    }


    public class IdText
    {
        internal string _id = "";
        internal string _txt = "";
        internal bool _selected = false;

        public IdText(string id, string txt, bool selected)
        {
            _id = id;
            _txt = txt;
            _selected = selected;
        }

        public string Id
        {
            get { return _id; }
        }

        public string Text
        {
            get { return _txt; }
        }

        public bool Selected
        {
            get { return _selected; }
            set { _selected = value; }
        }
    }
    private bool _enableSelect = true;
    private bool _hasRetrievedEnableSelect = false;
    private const string VS_WUCBL_ENABLESELECT = "VS_WUCBL_ENABLESELECT";
    private bool EnableSelectInternal
    {
        get
        {
            if (_hasRetrievedEnableSelect)
            {
                try
                {
                    _enableSelect = (bool) ViewState[VS_WUCBL_ENABLESELECT];
                    _hasRetrievedEnableSelect = true;
                }
                catch (Exception)
                {
                }
            }
            return _enableSelect;
        }
        set
        {
            _enableSelect = value;
            ViewState[VS_WUCBL_ENABLESELECT] = _enableSelect;
            _hasRetrievedEnableSelect = true;
        }
    }

    public bool EnableSelect
    {
        get { return EnableSelectInternal; }
        set
        {
            EnableSelectInternal = value;
            cbHead.Enabled = value;
            cbDet.Enabled = value;
        }
    }

    public int ColumnsCount
    {
        get { return cbDet.RepeatColumns; }
        set
        {
            if (value < 1) value = 1;
            cbDet.RepeatColumns = value;
        }
    }
}