using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


namespace UFIS.Web.ITrek.WebMgr
{

    /// <summary>
    /// Summary description for MgrView
    /// </summary>
    public class MgrView
    {


        private const string VS_EDIT_MODE = "VS_EDIT_MODE";
        private const string VS_ED_URNO = "VS_ED_URNO";
        private const string VS_ED_FLNO = "VS_ED_FLNO";


        private MgrView()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public static bool GetModeEdit(StateBag viewState)
        {
            bool editMode = false;
            try
            {
                editMode = (bool)viewState[VS_EDIT_MODE];
            }
            catch (Exception)
            {
            }
            return editMode;
        }

        public static void SetModeEdit(StateBag viewState, bool editMode)
        {
            viewState[VS_EDIT_MODE] = editMode;
        }

        public static string GetUrNoEdit(StateBag viewState)
        {
            string st = "";
            try
            {
                st = (string)viewState[VS_ED_URNO];
            }
            catch (Exception)
            {
            }
            return st;
        }

        public static void SetUrNoEdit(StateBag viewState, string urNo)
        {
            viewState[VS_ED_URNO] = urNo;
        }

        public static string GetFlightNoEdit(StateBag viewState)
        {
            string st = "";
            try
            {
                st = (string)viewState[VS_ED_FLNO];
            }
            catch (Exception)
            {
            }
            return st;
        }

        public static void SetFlightNoEdit(StateBag viewState, string flightNo)
        {
            viewState[VS_ED_FLNO] = flightNo;
        }


    }
}