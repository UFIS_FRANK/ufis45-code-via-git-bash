using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Web.SessionState;
using System.Collections.Generic;

using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.Util;
using UFIS.Web.ITrek.Sec;
using MM.UtilLib;

namespace UFIS.Web.ITrek.Base
{
    /// <summary>
    /// Summary description for BaseWeb
    /// </summary>
    public class BaseWeb:IBaseWeb 
    {
        private HttpSessionState _Session = null;
        private HttpResponse _Response = null;
        public BaseWeb(HttpSessionState session, HttpResponse response )
        {
            _Session = session;
            _Response = response;
        }

        #region User Authentication Handling
        private EntUser _user = null;
        public EntUser LoginedUser
        {
            get
            {
                if (_user == null)
                {
                    try
                    {
                        CtrlSec ctrlsec = CtrlSec.GetInstance();
                        _user = ctrlsec.AuthenticateUser(_Session, _Response);
                    }
                    catch (Exception)
                    {
                    }
                }
                return _user;
            }
        }

        public string LoginedId
        {
            get
            {
                string id = "";
                id = LoginedUser.UserId;
                return id;
            }
        }

        private List<string> _grList = null;
        public List<string> LoginedUserGroupList
        {
            get
            {
                if (_grList == null)
                {
                    try
                    {
                        _grList = UtilMisc.ConvArrayListToStringList(LoginedUser.GetGroupList());
                    }
                    catch (Exception)
                    {
                    }
                }
                return _grList;
            }
        }

        public virtual void DoUnAuthorise()
        {
            _Response.Clear();
            _Response.Redirect("UnAuthorised.aspx", true);
        }

        #endregion
    }
}
