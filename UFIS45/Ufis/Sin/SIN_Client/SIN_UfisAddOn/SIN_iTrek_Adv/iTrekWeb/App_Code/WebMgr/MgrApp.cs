using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using iTrekXML;
using UFIS.ITrekLib.Ctrl;

namespace UFIS.Web.ITrek.WebMgr
{
    /// <summary>
    /// Web Application level Manager
    /// </summary>
    public class MgrApp
    {
        public MgrApp()
        {
        }

        private const string APP_CONFIG = "APP_CONFIG";

        /*** 
         * <Summary>
         * Load the configuration from xml file.
         * When any changes in xml file, call this method again 
         * to reflect the new setting.
         * </Summary>
         */
        public static void LoadConfig(HttpApplicationState app)
        {
            Config cfg = new Config();
            //if (app[APP_CONFIG] != null) app[APP_CONFIG] = null;
            app[APP_CONFIG] = cfg;
        }

        private static Config GetConfig(HttpApplicationState app)
        {
            if (app[APP_CONFIG] == null) LoadConfig(app);
            return (Config)app[APP_CONFIG];
        }

        public void LoadConfig2(HttpApplicationState app)
        {
            LoadConfig(app);
        }

        public static int GetWebFlightInfoFromMinutes(HttpApplicationState app)
        {
            return GetConfig(app).WEB_FLINFO_FR_MINUTES;
        }

        public static int GetWebFlightInfoToMinutes(HttpApplicationState app)
        {
            return GetConfig(app).WEB_FLINFO_TO_MINUTES;
        }

        public static int GetWebBAHighlightTripMINUTES(HttpApplicationState app)
        {
            return GetConfig(app).WEB_BA_HIGHLIGHT_TRIP_MINUTES;
        }

        #region BenchMark Timming
        public static int BM_MAB(HttpApplicationState app)
        {
            return GetConfig(app).BM_MAB;
        }

        public static int BM_PLB(HttpApplicationState app)
        {
            return GetConfig(app).BM_PLB;
        }

        public static int BM_ApronFirstBag(HttpApplicationState app)
        {
            return GetConfig(app).BM_ApronFirstBag;
        }

        public static int BM_ApronLastBagLightLoader(HttpApplicationState app)
        {
            return GetConfig(app).BM_ApronLastBagLightLoader;
        }

        public static int BM_ApronLastBagHeavyLoader(HttpApplicationState app)
        {
            return GetConfig(app).BM_ApronLastBagHeavyLoader;
        }

        public static int BM_BaggageFirstBag(HttpApplicationState app)
        {
            return GetConfig(app).BM_BaggageFirstBag;
        }

        public static int BM_BaggageLastBagLightLoader(HttpApplicationState app)
        {
            return GetConfig(app).BM_BaggageLastBagLightLoader;
        }

        public static int BM_BaggageLastBagHeavyLoader(HttpApplicationState app)
        {
            return GetConfig(app).BM_BaggageLastBagHeavyLoader;
        }
        #endregion
    }

    class Config
    {
        private int _BM_MAB = 0;
        private int _BM_PLB = 0;
        private int _BM_AP_FST_BAG = 0;
        private int _BM_AP_LST_BAG_LGT_LOAD = 0;
        private int _BM_AP_LST_BAG_HVY_LOAD = 0;
        private int _BM_BG_FST_BAG = 0;
        private int _BM_BG_LST_BAG_LGT_LOAD = 0;
        private int _BM_BG_LST_BAG_HVY_LOAD = 0;
        private int _FLINFO_FROM_MINUTES = 0;
        private int _FLINFO_TO_MINUTES = 0;
        private int _BA_HL_TRIP_MINUTES = 0;

        private iTXMLPathscl _paths=null;

        private iTXMLPathscl Paths
        {
            get
            {
                if (_paths == null)
                {
                    _paths = new iTXMLPathscl();
                }
                return _paths;
            }
        }

        ///Get "From Minutes" to select the flight for "Flight Info" Web page
        ///e.g -60 minutes (-1 hours)
        public int WEB_FLINFO_FR_MINUTES
        {
            get
            {
                if (_FLINFO_FROM_MINUTES == 0)
                {
                    //_FLINFO_FROM_MINUTES = Convert.ToInt32(Paths.GetWebFLINFO_FROM_MINUTES());
                    _FLINFO_FROM_MINUTES = Convert.ToInt32(CtrlConfig.GetWebFLINFO_FROM_MINUTES());
                }
                return _FLINFO_FROM_MINUTES;
            }
        }

        ///Get "To Minutes" to select the flight for "Flight Info" Web page
        ///e.g +720 minutes (12 hours)
        public int WEB_FLINFO_TO_MINUTES
        {
            get
            {
                if (_FLINFO_TO_MINUTES == 0)
                {
                    //_FLINFO_TO_MINUTES = Convert.ToInt32(Paths.GetWebFLINFO_TO_MINUTES());
                    _FLINFO_TO_MINUTES = Convert.ToInt32(CtrlConfig.GetWebFLINFO_TO_MINUTES());
                }
                return _FLINFO_TO_MINUTES;
            }
        }

        /// <summary>
        /// Get the minutues to highlight the unreceived trip for Web Baggage Arrival
        /// </summary>
        public int WEB_BA_HIGHLIGHT_TRIP_MINUTES
        {
            get
            {
                if (_BA_HL_TRIP_MINUTES == 0)
                {
                    _BA_HL_TRIP_MINUTES = CtrlConfig.GetWebBAHighlightTripMINUTES();
                }
                return _BA_HL_TRIP_MINUTES;
            }
        }

        #region BenchMark Timing
        public int BM_MAB
        {
            get
            {
                if (_BM_MAB == 0)
                {
                    //_BM_MAB = Convert.ToInt32( Paths.GetBM_MAB() );
                    _BM_MAB = Convert.ToInt32(CtrlConfig.GetBmMAB());
                }
                return _BM_MAB;
            }
        }

        public int BM_PLB
        {
            get
            {
                if (_BM_PLB == 0)
                {
                    //_BM_MAB = Convert.ToInt32( Paths.GetBM_MAB() );
                    _BM_PLB = Convert.ToInt32(CtrlConfig.GetBmPLB());
                }
                return _BM_PLB;
            }
        }

        public int BM_ApronFirstBag
        {
            get
            {
                if (_BM_AP_FST_BAG == 0)
                {
                    //_BM_AP_FST_BAG = Convert.ToInt32(Paths.GetBMApronFirstBag());
                    _BM_AP_FST_BAG = Convert.ToInt32(CtrlConfig.GetBmApronFirstBag());
                }
                return _BM_AP_FST_BAG;
            }
        }

        public int BM_ApronLastBagLightLoader
        {
            get
            {
                if (_BM_AP_LST_BAG_LGT_LOAD == 0)
                {
                    //_BM_AP_LST_BAG_LGT_LOAD = Convert.ToInt32(Paths.GetBMApronLastBagLightLoad());
                    _BM_AP_LST_BAG_LGT_LOAD = Convert.ToInt32(CtrlConfig.GetBMApronLastBagLightLoad());
                }
                return _BM_AP_LST_BAG_LGT_LOAD;
            }
        }

        public int BM_ApronLastBagHeavyLoader
        {
            get
            {
                if (_BM_AP_LST_BAG_HVY_LOAD == 0)
                {
                    //_BM_AP_LST_BAG_HVY_LOAD = Convert.ToInt32(Paths.GetBMApronLastBagHeavyLoad());
                    _BM_AP_LST_BAG_HVY_LOAD = Convert.ToInt32(CtrlConfig.GetBMApronLastBagHeavyLoad());
                }
                return _BM_AP_LST_BAG_HVY_LOAD;
            }
        }

        public int BM_BaggageFirstBag
        {
            get
            {
                if (_BM_BG_FST_BAG == 0)
                {
                    //_BM_BG_FST_BAG = Convert.ToInt32(Paths.GetBMBaggageFirstBag());
                    _BM_BG_FST_BAG = Convert.ToInt32(CtrlConfig.GetBmBaggageFirstBag());
                }
                return _BM_BG_FST_BAG;
            }
        }

        public int BM_BaggageLastBagLightLoader
        {
            get
            {
                if (_BM_BG_LST_BAG_LGT_LOAD == 0)
                {
                    //_BM_BG_LST_BAG_LGT_LOAD = Convert.ToInt32(Paths.GetBMBaggageLastBagLightLoad());
                    _BM_BG_LST_BAG_LGT_LOAD = Convert.ToInt32(CtrlConfig.GetBMBaggageLastBagLightLoad());
                }
                return _BM_BG_LST_BAG_LGT_LOAD;
            }
        }

        public int BM_BaggageLastBagHeavyLoader
        {
            get
            {
                if (_BM_BG_LST_BAG_HVY_LOAD == 0)
                {
                    //_BM_BG_LST_BAG_HVY_LOAD = Convert.ToInt32(Paths.GetBMBaggageLastBagHeavyLoad());
                    _BM_BG_LST_BAG_HVY_LOAD = Convert.ToInt32(CtrlConfig.GetBMBaggageLastBagHeavyLoad());
                }
                return _BM_BG_LST_BAG_HVY_LOAD;
            }
        }
        #endregion

    }


}
