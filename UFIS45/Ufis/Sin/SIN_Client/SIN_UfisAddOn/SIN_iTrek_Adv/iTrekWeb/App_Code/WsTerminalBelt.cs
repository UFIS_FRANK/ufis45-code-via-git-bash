using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;

using System.Data;
using AjaxControlToolkit;
using System.Collections.Generic;
using System.Collections.Specialized;

using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.DS;

/// <summary>
/// Summary description for WsTerminalBelt
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class WsTerminalBelt : System.Web.Services.WebService
{

    public WsTerminalBelt()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld()
    {
        return "Hello World";
    }

    [WebMethod]
    public AjaxControlToolkit.CascadingDropDownNameValue[] WsGetTerminals(string knownCategoryValues, string category)
    {
        List<CascadingDropDownNameValue> values =
          new List<CascadingDropDownNameValue>();
        DSTerminal ds = GetAllTerminals();

        foreach (DataRow dr in ds.TERMINAL.Rows)
        {
            values.Add(new CascadingDropDownNameValue(
              (string)dr["TERM_NAME"], dr["TERM_ID"].ToString()));
        }
        return values.ToArray();

    }

    private DSTerminal GetAllTerminals()
    {
        return CtrlTerminal.RetrieveTerminal();
    }


    [WebMethod]
    public AjaxControlToolkit.CascadingDropDownNameValue[] WsGetBeltsForTerminal(string knownCategoryValues, string category)
    {
        List<CascadingDropDownNameValue> values =
          new List<CascadingDropDownNameValue>();
        try
        {
            StringDictionary kv =
     CascadingDropDown.ParseKnownCategoryValuesString(
     knownCategoryValues);
            string terminalId = "1";
            if (!kv.ContainsKey("Terminal") ||
                kv["Terminal"] == null)
            {
                return null;
            }
            terminalId = kv["Terminal"];

            DataRow[] rows = CtrlTerminal.WebGetBeltForTerminal(terminalId);
            if (rows != null)
            {
                foreach (DataRow dr in rows)
                {
                    values.Add(new CascadingDropDownNameValue(
                      dr["VAL"].ToString(), dr["ID"].ToString()));
                }
            }
        }
        catch (Exception)
        {
        }
        return values.ToArray();

    }  
}

