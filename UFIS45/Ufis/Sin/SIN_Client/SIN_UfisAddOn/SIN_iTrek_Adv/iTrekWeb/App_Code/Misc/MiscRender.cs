using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Text;

namespace UFIS.Web.ITrek.Misc
{
    /// <summary>
    /// Summary description for Render
    /// </summary>
    public class MiscRender
    {
        private MiscRender()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public static void HighLightTransit(GridViewRow row, int cellPosition)
        {
            try
            {
                string stTURN = DataBinder.Eval(row.DataItem, "TURN").ToString().Trim();
                if ((stTURN != null) && (stTURN != ""))
                {
                    //row.CssClass = "HLTsit";
                    //Style s = new Style();
                    //s.BackColor = System.Drawing.Color.Blue;
                    //s.Font.Bold = true;
                    //row.ApplyStyle(s);
                    if (cellPosition < 0) cellPosition = 0;
                    row.Cells[cellPosition].CssClass = "HLTsit";
                    //row.Cells[cellPosition].
                }
                else
                {
                    row.Cells[cellPosition].FindControl("ltlTfln").Visible = false;
                }
            }
            catch (Exception ex)
            {
                LogMsg("HighLightTransit:Err:" + ex.Message);
            }
        }

        private static void LogMsg(string msg)
        {
            UFIS.ITrekLib.Util.UtilLog.LogToGenericEventLog("MiscRender", msg);
        }

       /// <summary>
       /// Freeze the Grid Header (not to move when the data was scrolled)
       /// </summary>
       /// <param name="page">this.Page</param>
       /// <param name="type">this.GetType</param>
       /// <param name="gvClientId">Client Id of the grid view to Freeze the header</param>
       /// <param name="gvCssClass">CssClass Name to apply this grid</param>
       /// <param name="hfClientIdToKeepScrollPos">hidden field id to keep the scroll position</param>
       /// <param name="curScrollPos">current scroll position</param>
        public static void FreezeGridHeader(Page page, Type type,
            string gvClientId, string gvCssClass,
            string hfClientIdToKeepScrollPos, string curScrollPos)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<script type='text/javascript'>" + Environment.NewLine + "<!--");
            sb.AppendLine("FreezeGridViewHeader('" + 
                gvClientId + "','" + gvCssClass + "','" 
                + curScrollPos + "','" + hfClientIdToKeepScrollPos + "');");
            sb.AppendLine(@"    // --></script>");            

            page.ClientScript.RegisterStartupScript(type, 
                "FreezeHeader",
                 sb.ToString());//For Next Implementation to freeze the header
        }
    }
}