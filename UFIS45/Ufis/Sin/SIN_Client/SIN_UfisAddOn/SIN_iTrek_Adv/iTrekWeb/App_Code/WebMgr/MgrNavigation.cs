using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for MgrNavigation
/// </summary>
public class MgrNavigation
{
   private static MgrNavigation _this = null;
   private static object _lockThis = new object();
   private static string _stServerName = "";
   private static string _stApplicationName = "";

   private MgrNavigation()
   {
      //
      // TODO: Add constructor logic here
      //
   }

   public static MgrNavigation GetInstance()
   {
      if (_this == null)
      {
         lock (_lockThis)
         {
            if (_this == null) _this = new MgrNavigation();
         }
      }
      return _this;
   }

   public static string UrlNavBA(EnumBAPage enPage, EnumBAAction enAction)
   {
      string url = "";
      switch (enPage)
      {
         case EnumBAPage.BAFlightMsg:
            break;
         case EnumBAPage.BARcvTime:
            break;
         case EnumBAPage.BARecvdCntr:
            break;
         case EnumBAPage.BAShowCntr:
            break;
         case EnumBAPage.BATerminalFlightUlds:
            url = UrlNavBATerminalFlightUldsPage(enAction);
            break;
         case EnumBAPage.BATimeEntry:
            break;
         case EnumBAPage.None:
            break;
         default:
            break;
      }
      return url;
   }

   public static string UrlNavBATerminalFlightUldsPage(EnumBAAction enAction)
   {
      string url = "";
      switch (enAction)
      {
         case EnumBAAction.BAFlightRecvBtn:
            url = UrlNavBAFlightRecvBtn();
            break;
         case EnumBAAction.BAFlightFirstBagBtn:
            url = UrlNavBAFlightFirstBagBtn();
            break;
         case EnumBAAction.BAFlightLastBagBtn:
            url = UrlNavBAFlightLastBagBtn();
            break;
         case EnumBAAction.BAFlightShowCntrBtn:
            url = UrlNavBAFlightShowCntrBtn();
            break;
         case EnumBAAction.BAFlightMsgBtn:
            url = UrlNavBAFlightMsgBtn();
            break;
         case EnumBAAction.BAFlightRecvCntrLink:
            url = UrlNavBAFlightRecvCntrLink();
            break;
         default:
            break;
      }
      return url;
   }

   public static string UrlNavBARcvTimePage(EnumBAAction enAction)
   {
      string url = "";
      switch (enAction)
      {
         case EnumBAAction.BARecvSendTime:
            url = URL_BA_FLIGHT_RECV_SENT_TIME;
            break;
         case EnumBAAction.BAFlightRecvBtn:
            url = UrlNavBAFlightRecvBtn();
            break;
         case EnumBAAction.BAFlightFirstBagBtn:
            url = UrlNavBAFlightFirstBagBtn();
            break;
         case EnumBAAction.BAFlightLastBagBtn:
            url = UrlNavBAFlightLastBagBtn();
            break;
         case EnumBAAction.BAFlightShowCntrBtn:
            url = UrlNavBAFlightShowCntrBtn();
            break;
         case EnumBAAction.BAFlightMsgBtn:
            url = UrlNavBAFlightMsgBtn();
            break;
         case EnumBAAction.BAFlightRecvCntrLink:
            url = UrlNavBAFlightRecvCntrLink();
            break;
         default:
            break;
      }
      return url;
   }

   public static string UrlNavBATimeEntryPage(EnumBAAction enAction)
   {
      string url = "";
      switch (enAction)
      {
         case EnumBAAction.BARecvSendTime:
            url = URL_BA_FLIGHT_RECV_SENT_TIME;
            break;
         case EnumBAAction.BAFirstBagSendTime:
            url = URL_BA_FLIGHT_FBAG_SENT_TIME;
            break;
         case EnumBAAction.BALastBagSendTime:
            url = URL_BA_FLIGHT_LBAG_SENT_TIME;
            break;
         case EnumBAAction.BAFlightRecvBtn:
            url = UrlNavBAFlightRecvBtn();
            break;
         case EnumBAAction.BAFlightFirstBagBtn:
            url = UrlNavBAFlightFirstBagBtn();
            break;
         case EnumBAAction.BAFlightLastBagBtn:
            url = UrlNavBAFlightLastBagBtn();
            break;
         case EnumBAAction.BAFlightShowCntrBtn:
            url = UrlNavBAFlightShowCntrBtn();
            break;
         case EnumBAAction.BAFlightMsgBtn:
            url = UrlNavBAFlightMsgBtn();
            break;
         case EnumBAAction.BAFlightRecvCntrLink:
            url = UrlNavBAFlightRecvCntrLink();
            break;
         default:
            break;
      }
      return url;
   }

   #region Url Navigation Pages - For Baggage Arrival

   private const string URL_BA_FLIGHT_FBAG_SENT_TIME = "BATerminalFlightUlds.aspx";
   private const string URL_BA_FLIGHT_LBAG_SENT_TIME = "BATerminalFlightUlds.aspx";
   private const string URL_BA_FLIGHT_RECV_SENT_TIME = "BATerminalFlightUlds.aspx";

   public static string UrlNavBAFlightRecvBtn()
   {
      return "BARcvTime.aspx";
   }

   public static string UrlNavBAFlightFirstBagBtn()
   {
      return "BATimeEntry.aspx";
   }

   public static string UrlNavBAFlightLastBagBtn()
   {
      return "BATimeEntry.aspx";
   }

   public static string UrlNavBAFlightShowCntrBtn()
   {
      return "BAShowCntr.aspx";
   }

   public static string UrlNavBAFlightMsgBtn()
   {
      return "BAFlightMsg.aspx";
   }

   public static string UrlNavBAFlightRecvCntrLink()
   {
      return "BARecvdCntr.aspx";
   }
   #endregion

   #region URL Navigation Pages - For SSL
   public static string UrlLoginPage()
   {
      return "/Login2.aspx";
   }

   public static string FullUrlLoginPage()
   {
      return @"http://" +
         _stServerName +
         _stApplicationName +
         UrlLoginPage();
   }

   public static void InitServerInfo(HttpRequest request)
   {
      if (request != null)
      {
         if (string.IsNullOrEmpty(_stServerName))
         {
            lock (_lockThis)
            {
               if (string.IsNullOrEmpty(_stServerName))
               {
                  _stApplicationName = request.ApplicationPath;
                  _stServerName = request.ServerVariables["SERVER_NAME"];
               }
            }
         }
      }
   }

   public static void ShowLoginPage(HttpRequest Request, HttpResponse Response)
   {
      InitServerInfo(Request);
      Response.Redirect(FullUrlLoginPage());
   }

   public static void ShowLoginPage(HttpResponse Response)
   {
      Response.Redirect(FullUrlLoginPage());
   }

   #endregion
}

public enum EnumBAPage { BAFlightMsg, BARcvTime, BARecvdCntr, BAShowCntr, BATerminalFlightUlds, BATimeEntry, None };
public enum EnumBAAction { BAFlightRecvBtn, BAFlightFirstBagBtn, BAFlightLastBagBtn, BAFlightShowCntrBtn, BAFlightMsgBtn, BAFlightRecvCntrLink, BAFirstBagSendTime, BALastBagSendTime, BARecvSendTime };
