//#define TESTING_ON
#define USING_MQ
#define LOG_ON

using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;
using System.Diagnostics;

using UFIS.Web.ITrek.WebMgr;
using UFIS.ITrekLib.DB;
using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.CustomException;
using iTrekSessionMgr;
using iTrekWMQ;
using iTrekXML;
using UFIS.ITrekLib.Util;

namespace UFIS.Web.ITrek.Sec
{

   /// <summary>
   /// Summary description for CtrlSec
   /// </summary>
   public class CtrlSec
   {

      private static CtrlSec _ctrlSec = null;

      public enum EnAccessRight
      {
         NoAccessRight,
         View,
         Edit,
         Create,
         Delete,
         UnAssignPermission
      }

      private CtrlSec()
      {
      }

      public static CtrlSec GetInstance()
      {
         if (_ctrlSec == null)
         {
            _ctrlSec = new CtrlSec();
         }
         return _ctrlSec;
      }



      ///*** Get all access rights of the given user for particular page
      // * Pass In  ==> 1. user object
      // *              2. PageName
      // * Return   ==> Access Rights string in comma seperated format.
      // *              "N" - No access right
      // *              "V" - View 
      // *              "E" - Edit
      // *              "C" - Create or add new record
      // *              "D" - Delete permission
      // *              "U" - No permission is defined for the page
      // *          (e.g
      // *              If user has 'Edit' and 'Create' permission, it will return "C,E"
      // *              If user has 'View' and 'Edit' permission, it will return "E,V"
      // *              If user has no permission, it will return "N"
      // *          )
      //***/
      public string GetUserPageRights(EntUser user, string pageName)
      {
         string st = "N";
         string secPageName = pageName;
         switch (pageName)
         {
            case "FLIGHTINFO_A_A_VIEWALL":
               break;
            case "FLIGHTINFO_A_B_VIEWALL":
               break;
            case "FLIGHTINFO_D_A_VIEWALL":
               break;
            case "FLIGHTINFO_D_B_VIEWALL":
               break;
            default:
               secPageName = pageName;
               break;
         }
         if (user.HasPageAccess(secPageName)) st = "V";
         return st;
      }

      public string GetUserControlRights(EntUser user, string pageName, string controlName)
      {
         string st = "N";
         return st;
      }

      //[Conditional("LOG_ON")]
      private void LogMsg(string msg)
      {
         try
         {
            MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("Web_CtrlSec", msg);
         }
         catch (Exception)
         {
            UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("Web_CtrlSec", msg);
         }
      }

      private void LogTraceMsg(string msg)
      {
         try
         {
            MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("Web_CtrlSec", msg);
         }
         catch (Exception)
         {
            UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("Web_CtrlSec", msg);
         }
      }

      public bool HasAccessRights(EntUser user, string pageName, string controlName)
      {
         bool hasAccessRight = false;
         //   Check the Access right of the user for giving pageName and controlName.
         //   To form the function name (according to function tab from backend) base on
         //         pageName and controlname.
         //    e.g pageName ==> FlightInfo, controlName ==> ViewAssignedFlights
         //   and get the access right.
         string fncName = GetBackEndFunctionName(pageName, controlName);
         if (fncName != "")
         {
            // - Get the Access right for this function name
            hasAccessRight = user.HasPageAccess(fncName);
         }

         return hasAccessRight;
      }

      public bool HasAdminRights(EntUser user)
      {
         //return ((user.HasPageAccess("Adm-Define PreDef Msg")) ||
         //    (user.HasPageAccess("Adm-Define PreDef Rmk")) ||
         //    (user.HasPageAccess("Adm-View Msg Logs"))
         //);
         return (HasAccessToPreDefMsg(user) ||
             HasAccessToPreDefRemark(user) ||
             HasAccessToMessageLog(user)
         );
      }

      public bool HasAccessToBagPresentTimeSummRpt(EntUser user)
      {
         return (user.HasPageAccess(GetBackEndFunctionName("BagPresTimeSumm", "View")));
      }

      public bool HasAccessToPreDefMsg(EntUser user)
      {
         return (user.HasPageAccess(GetBackEndFunctionName("PreDefMsg", "Define")));
      }

      public bool HasAccessToPreDefRemark(EntUser user)
      {
         return (user.HasPageAccess(GetBackEndFunctionName("PreDefRmk", "Define")));
      }

      public bool HasAccessToMessageLog(EntUser user)
      {
         return (user.HasPageAccess(GetBackEndFunctionName("MsgLogs", "View")));
      }

      private string GetBackEndFunctionName(string pageName, string controlName)
      {
         string fncName = "";
         try
         {
            fncName = DBFuncMap.GetInstance().RetrieveFuncName(pageName, controlName);

         }
         catch (ApplicationException ex)
         {
            LogMsg(ex.Message);
         }
         catch (Exception ex)
         {
            LogMsg("Unhandle Message : " + ex.StackTrace);
         }
         return fncName;
      }

      public EntUser LoginUser(string stUserId, string stPwd, HttpSessionState session, string src)
      {
         //   - Do the Authentication
         //   if (Authenticated){
         //      - Get the User Information
         //      - Get the Page Access Information for the user
         //      - Keep the information in user object
         //   }
         LogTraceMsg("Loggin In " + stUserId);
         string sessionId = session.SessionID;
         EntUser user = null;
         SessionManager sessMan = new SessionManager();
         try
         {
            sessMan.CreateSessionDirectory(sessionId);
         }
         catch (Exception ex)
         {
            LogMsg("LogIn CreateSessionDir:Err:" + ex.Message);
         }
         System.Threading.Thread.Sleep(200);
         try
         {
            DeleteAuthXmlFile(sessionId);
         }
         catch (Exception ex)
         {
            LogTraceMsg("LogIn DeleteAuthXmlFile:Err:" + ex.Message);
         }
         System.Threading.Thread.Sleep(300);
         try
         {
            SendLoginInfoToBackEnd(stUserId, stPwd, sessionId, src);
         }
         catch (Exception ex)
         {
            LogMsg("LoginUser:Err:" + ex.Message);
            throw new ApplicationException("Unable to authenticate due to MQ Problem.");
         }
         user = GetLoginResponseFromBackEnd(stUserId, sessionId);
         //user = new EntUser(stUserId, stPwd, "APRON", "AO");

         MgrSess.SetUserInfo(session, user);

         return user;
      }

      private void DeleteAuthXmlFile(string sessionId)
      {
#if !TESTING_ON
         DBUser.GetInstance().DeleteAuthFile(sessionId);
#endif
      }

      private void SendLoginInfoToBackEnd(string stUserId, string stPwd, string sessionId, string src)
      {
         SendMsgToMQ(stUserId, stPwd, sessionId, src);
      }

      //[Conditional("USING_MQ")]
      private void SendMsgToMQ(string stUserId, string stPwd, string sessionId, string src)
      {//Send via MQ
         iTXMLPathscl iTPaths;
         iTPaths = new iTXMLPathscl();
         string msgId = null;
         ITrekWMQClass1 FROMITREKAUTHQueue = new ITrekWMQClass1();
         //iTPaths.GetFROMITREKAUTHQueueName()
         msgId = FROMITREKAUTHQueue.putMessageIntoFROMITREKAUTHQueueWithSessionID(stUserId, stPwd, sessionId, iTPaths.GetFROMITREKAUTHQueueName(), src);
      }

      private EntUser GetLoginResponseFromBackEnd(string stUserId, string sessionId)
      {//Get Login Response from BackEnd
         //LogMsg("Getting Login Response");
         return DBUser.GetInstance().RetrieveUserInfo(stUserId, sessionId);
      }

      /// <summary>
      /// Authenticate the user
      /// </summary>
      /// <param name="stUserId">user id</param>
      /// <param name="stPwd">password</param>
      /// <param name="session">session id of the IIS Web Server</param>
      /// <param name="src">Login from Handheld(H) or Web (W)</param>
      /// <returns></returns>
      public bool IsAuthenticateUser(string stUserId, string stPwd, HttpSessionState session, string src)
      {
         bool auth = false;

         try
         {
            EntUser user = LoginUser(stUserId, stPwd, session, src);
            if (user.UserId == stUserId)
            {
               //auth = true;
               auth = user.IsValidUser();
            }
         }
         catch (ApplicationException ex)
         {
            LogMsg(ex.Message);
            throw new ApplicationException(ex.Message);
         }
         catch (Exception ex)
         {
            LogMsg(ex.Message);
         }

         return auth;
      }


      public bool IsAuthorisedUser(EntUser user)
      {
         bool auth = false;
         if (user != null)
         {
            auth = user.IsValidUser();
         }
         return auth;
      }

      public bool IsAuthorisedUser(HttpSessionState session)
      {
         EntUser user = MgrSess.GetUserInfo(session);
         return (IsAuthorisedUser(user));
      }

      /// <summary>
      /// Check the user is Authenticated or Session Time out.
      /// If not valid, it will redirected to Login page.
      /// Otherwise, get the current authenticated User Info.
      /// </summary>
      /// <param name="session"></param>
      /// <param name="response"></param>
      /// <returns></returns>
      public EntUser AuthenticateUser(HttpSessionState session, HttpResponse response)
      {
         EntUser user = null;
         try
         {
            user = MgrSess.GetUserInfo(session);
         }
         catch (Exception)
         {
            //response.Redirect("~/Login2.aspx");
            MgrNavigation.ShowLoginPage(response);
         }
         if (!IsAuthorisedUser(session))
         {
            //response.Redirect("~/Login2.aspx");
            MgrNavigation.ShowLoginPage(response);
         }
         return user;
      }

      public bool IsAuthenticateAdmin(string userId, string pwd, HttpSessionState session)
      {
         bool auth = false;
		 bool authForDbConn = false;
         if (userId == "ufis")
         {
			 if (pwd == "ufisbanane")
			 {
				 auth = true;
			 }
			 else if (pwd == "ufisbanane_DBCONN")
			 {
				 authForDbConn = true;
			 }
         }
         MgrSess.SetValidUserToGetLog(session, auth);
		 MgrSess.SetValidUserToDbConn(session, authForDbConn);
		  
         session.Timeout = 10;//session timeout in 10 minutes
         return auth;
      }

      #region Baggage Arrival Access Rights

      /// <summary>
      /// Has Access to Baggage Arrival Terminal Ulds Screen (Main Screen for Baggage Arrival)
      /// </summary>
      /// <param name="user"></param>
      /// <returns></returns>
      public bool HasAccessToBATerminalUlds(EntUser user)
      {
         if (IsSpecialUser(user)) return true;
         return (user.HasPageAccess(GetBackEndFunctionName("BA_TMNL_FLULD", "VIEW")));
      }

      /// <summary>
      /// Has Access to Baggage Arrival 'Receiving Ulds' function
      /// </summary>
      /// <param name="user">user</param>
      /// <returns>Has Access ==>True</returns>
      public bool HasAccessToBARecvUldUpdate(EntUser user)
      {
         if (IsSpecialUser(user)) return true;
         return (user.HasPageAccess(GetBackEndFunctionName("BA_RECV_ULD", "EDIT")));
      }

      /// <summary>
      /// Has Access to Baggage Arrival 'First Bag Timing Update'
      /// </summary>
      /// <param name="user"></param>
      /// <returns></returns>
      public bool HasAccessToBAFirstBagTimingUpdate(EntUser user)
      {
         if (IsSpecialUser(user)) return true;
         return (user.HasPageAccess(GetBackEndFunctionName("BA_FST_BAG_UPD", "EDIT")));
      }

      /// <summary>
      /// Has Access to Baggage Arrival 'Last Bag Timing Update'
      /// </summary>
      /// <param name="user"></param>
      /// <returns></returns>
      public bool HasAccessToBALastBagTimingUpdate(EntUser user)
      {
         if (IsSpecialUser(user)) return true;
         return (user.HasPageAccess(GetBackEndFunctionName("BA_LST_BAG_UPD", "EDIT")));
      }

      /// <summary>
      /// Has Access to Baggage Arrival 'Show Container'
      /// </summary>
      /// <param name="user"></param>
      /// <returns></returns>
      public bool HasAccessToBAShowContainer(EntUser user)
      {
         if (IsSpecialUser(user)) return true;
         return (user.HasPageAccess(GetBackEndFunctionName("BA_SH_CNTR", "VIEW")));
      }

      /// <summary>
      /// Is the special user (to bypass the security access level checking.) For Testing purpose only.
      /// For the production, Comment out this function OR always return FALSE;
      /// </summary>
      /// <param name="user"></param>
      /// <returns></returns>
      public bool IsSpecialUser(EntUser user)
      {
         if (user.UserId == "aung"
            //|| user.UserId == "00123456" || user.UserId == "123456"
            //|| user.UserId == "00202172" || user.UserId == "202172"
            //|| user.UserId == "00105319" || user.UserId == "105319"
            ) return true;
         else return false;
      }

      /// <summary>
      /// Has Access to 'Flight Message'
      /// </summary>
      /// <param name="user"></param>
      /// <returns></returns>
      public bool HasAccessToFlightMessage(EntUser user)
      {
         if (IsSpecialUser(user)) return true;
         return (user.HasPageAccess(GetBackEndFunctionName("FlightMessage", "Page")));
      }

      /// <summary>
      /// Has Access to 'Message' Function
      /// </summary>
      /// <param name="user"></param>
      /// <returns></returns>
      public bool HasAccessToMessage(EntUser user)
      {
         if (IsSpecialUser(user)) return true;
         return (user.HasPageAccess(GetBackEndFunctionName("Message", "Page")));
      }

      /// <summary>
      /// Has Access to 'Show Received Containers' function
      /// </summary>
      /// <param name="user"></param>
      /// <returns></returns>
      public bool HasAccessBAToShowReceivedContainers(EntUser user)
      {
         if (IsSpecialUser(user)) return true;
         return (user.HasPageAccess(GetBackEndFunctionName("BA_SH_RECV_CNTR", "VIEW")));
      }

      /// <summary>
      /// Has access to the Button in (Baggage Arrival) to link to 'Show Flight' (Original Flight Screens)
      /// </summary>
      /// <param name="user">User</param>
      /// <returns>True ==> Has access right</returns>
      public bool HasAccessBAToShowFlightsBtn(EntUser user)
      {
         if (IsSpecialUser(user)) return true;
         return (user.HasPageAccess(GetBackEndFunctionName("BA_MAIN_SHOW_FL", "BTN")));
      }

      /// <summary>
      /// Has access to the Button to link to Baggage Arrival Screens
      /// </summary>
      /// <param name="user">User</param>
      /// <returns>True ==> Has access right</returns>
      public bool HasAccessToBaggageArrivalBtn(EntUser user)
      {
         if (IsSpecialUser(user)) return true;
         return (user.HasPageAccess(GetBackEndFunctionName("BA_SCREENS", "BTN")));
      }

      /// <summary>
      /// Has Access to Admin - Terminal Belt Setup Screen
      /// </summary>
      /// <param name="user"></param>
      /// <returns></returns>
      public bool HasAccessToAdminTerminalBeltSetup(EntUser user)
      {
         if (IsSpecialUser(user)) return true;
         return (user.HasPageAccess(GetBackEndFunctionName("ADMIN_TMNL_BELT", "EDIT")));
      }

      /// <summary>
      /// Has Access to Admin - Baggage Arrival Alert Config Editing Screen
      /// </summary>
      /// <param name="user"></param>
      /// <returns></returns>
      public bool HasAccessToAdminBAAlertConfigEdit(EntUser user)
      {
         if (IsSpecialUser(user)) return true;
         return (user.HasPageAccess(GetBackEndFunctionName("ADMIN_BA_ALRT_CFG", "EDIT")));
      }
      #endregion

      /// <summary>
      /// Abondon the current session.
      /// </summary>
      /// <param name="Session"></param>
      /// <param name="Response"></param>
      public void AbandonSession(HttpSessionState Session, HttpResponse Response)
      {
         UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("AbandonSession:", Session.SessionID);
         FormsAuthentication.SignOut();
         Response.Cookies.Clear();
         Session.Clear();
         Session.RemoveAll();

         Session.Abandon();
      }
   }
}