using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Web.SessionState;

using UFIS.Web.ITrek.Sec;
using UFIS.ITrekLib.ENT;


namespace UFIS.Web.ITrek.WebMgr
{
	/// <summary>
	/// Summary description for SessMg
	/// </summary>
	public class MgrSess
	{
		private MgrSess()
		{
		}

		const string SESS_CONFIG = "SESS_CONFIG";

		#region Current Message Id
		private const string SESS_CUR_MSG_ID = "SESS_CUR_MSG_ID";
		public static string GetCurrentMessageId(HttpSessionState session)
		{
			string curMsgId = "";
			curMsgId = session[SESS_CUR_MSG_ID].ToString();
			return curMsgId;
		}
		public static void SetCurrentMessageId(HttpSessionState session, string msgId)
		{
			session[SESS_CUR_MSG_ID] = msgId;
		}
		#endregion

		#region User Information
		//Get and set the User Information from Session variable.
		const string SESS_USER = "SESS_USER";
		public static EntUser GetUserInfo(HttpSessionState session)
		{
			if (session[SESS_USER] == null)
			{
				EntUser user = new EntUser("", "", "AP", "AO");
				session[SESS_USER] = user;
				return (EntUser)session[SESS_USER];
				//throw new ApplicationException("No User Information.");
			}
			else
			{
				try
				{
					return (EntUser)session[SESS_USER];
				}
				catch (Exception)
				{
					throw new ApplicationException("Invalid User Information");
				}
			}
		}

		public static void SetUserInfo(HttpSessionState session, EntUser user)
		{
			session[SESS_USER] = user;
		}
		# endregion

		#region Flight AB (Apron or Baggage)
		const string SESS_FLIGHT_AB = "SESS_FLIGHT_AB";
		public static void SetFlightAprBag(HttpSessionState session, string ApronBeggage)
		{
			session[SESS_FLIGHT_AB] = ApronBeggage;
		}
		public static string GetFlightAprBag(HttpSessionState session)
		{
			string ab = ""; //
			try
			{
				ab = session[SESS_FLIGHT_AB].ToString();
			}
			catch (Exception)
			{
			}
			return ab;
		}
		#endregion

		#region Flight AD (Arrival or Departure)
		const string SESS_FLIGHT_AD = "SESS_FLIGHT_AD";
		public static void SetFlightArrDep(HttpSessionState session, string ArrDep)
		{
			session[SESS_FLIGHT_AD] = ArrDep;
		}
		public static string GetFlightArrDep(HttpSessionState session)
		{
			string ad = ""; //
			try
			{
				ad = session[SESS_FLIGHT_AD].ToString();
			}
			catch (Exception)
			{
			}
			return ad;
		}
		#endregion

		#region Valid User to Get Log
		private const string SESS_VALID_USER_TO_GET_LOG = "SESS_VALID_USER_TO_GET_LOG";
		public static bool IsValidUserToGetLog(HttpSessionState session)
		{
			bool validUser = false;
			try
			{
				string stValid = session[SESS_VALID_USER_TO_GET_LOG].ToString();
				if (stValid == "T") validUser = true;
			}
			catch (Exception)
			{
				//session[SESS_VALID_USER_TO_GET_LOG] = "";
			}
			return validUser;
		}

		public static void SetValidUserToGetLog(HttpSessionState session, bool validUser)
		{
			string stValid = "F";
			if (validUser) { stValid = "T"; }
			session[SESS_VALID_USER_TO_GET_LOG] = stValid;
		}
		#endregion

		#region Valid User for Db Connection
		private const string SESS_VALID_USER_TO_DB_CONN = "SESS_VALID_USER_TO_DB_CONN";
		public static bool IsValidUserToDbConn(HttpSessionState session)
		{
			bool validUser = false;
			try
			{
				string stValid = session[SESS_VALID_USER_TO_DB_CONN].ToString();
				if (stValid == "T") validUser = true;
			}
			catch (Exception)
			{
				//session[SESS_VALID_USER_TO_DB_CONN] = "";
			}
			return validUser;
		}

		public static void SetValidUserToDbConn(HttpSessionState session, bool validUser)
		{
			string stValid = "F";
			if (validUser) { stValid = "T"; }
			session[SESS_VALID_USER_TO_DB_CONN] = stValid;
		}
		#endregion

		#region Baggage Arrival
		private const string SESS_BA_FLCURSEL = "SESS_BA_FLCURSEL";
		public static EntBagArrFlightSelectedUld GetBagArrCurrentSel(HttpSessionState session)
		{
			EntBagArrFlightSelectedUld obj = null;
			obj = (EntBagArrFlightSelectedUld)session[SESS_BA_FLCURSEL];
			return obj;
		}
		public static void SetBagArrCurrentSel(HttpSessionState session, EntBagArrFlightSelectedUld objBagArrFlSelUlds)
		{
			session[SESS_BA_FLCURSEL] = objBagArrFlSelUlds;
		}

		private const string SESS_BA_TERMINAL_INFO = "SESS_BA_TERMINAL_INFO";

		public static EntBagArrTerminal GetBagArrTerminalInfo(HttpSessionState session)
		{
			EntBagArrTerminal obj = null;
			try
			{
				obj = (EntBagArrTerminal)session[SESS_BA_TERMINAL_INFO];
			}
			catch (Exception)
			{
			}
			return obj;
		}

		public static void SetBagArrTerminalInfo(HttpSessionState session, EntBagArrTerminal objBagArrTerminal)
		{
			session[SESS_BA_TERMINAL_INFO] = objBagArrTerminal;
		}

		private const string SESS_BA_SELECTEDULDSFORTERMINAL = "SESS_BA_SELECTEDULDSFORTERMINAL";
		public static EntBagArrFlightSelectedUldList GetBagArrFlightSelectedUldList(HttpSessionState session)
		{
			EntBagArrFlightSelectedUldList obj = null;
			try
			{
				obj = (EntBagArrFlightSelectedUldList)session[SESS_BA_SELECTEDULDSFORTERMINAL];
			}
			catch (Exception)
			{
			}
			return obj;
		}

		public static void SetBagArrFlightSelectedUldList(HttpSessionState session,
			EntBagArrFlightSelectedUldList objBagArrFlightSelectedUldList)
		{
			try
			{
				session[SESS_BA_SELECTEDULDSFORTERMINAL] = objBagArrFlightSelectedUldList;
			}
			catch (Exception)
			{
			}
		}
		#endregion

		#region FlightInfo Filter
		const string SESS_FLIGHT_INFO_FILTER = "SESS_FLIGHT_INFO_FILTER";
		public static void SetFlightInfoFilter(HttpSessionState session, EntWebFlightFilter filter)
		{
			session[SESS_FLIGHT_INFO_FILTER] = filter;
		}
		public static EntWebFlightFilter GetFlightInfoFilter(HttpSessionState session)
		{
			EntWebFlightFilter filter = null;
			try
			{
				filter = (EntWebFlightFilter)session[SESS_FLIGHT_INFO_FILTER];
				if (filter == null) filter = new EntWebFlightFilter();
			}
			catch (Exception)
			{
				filter = new EntWebFlightFilter();
			}
			return filter;
		}
		#endregion

		#region User's Browser Screen Info
		const string SESS_BROWSER_WIDTH = "SESS_BROWSER_WIDTH";
		const string SESS_BROWSER_HEIGHT = "SESS_BROWSER_HEIGHT";
		const int DEFAULT_BROWSER_WIDTH = 1024;
		const int DEFAULT_BROWSER_HEIGHT = 768;

		public static void SetBrowserWidth(HttpSessionState session, int width)
		{
			session[SESS_BROWSER_WIDTH] = width;
		}
		public static int GetBrowserWidth(HttpSessionState session)
		{
			int width = DEFAULT_BROWSER_WIDTH;
			try
			{
				width = (int)session[SESS_BROWSER_WIDTH];
			}
			catch (Exception)
			{
			}
			if (width < DEFAULT_BROWSER_WIDTH) width = DEFAULT_BROWSER_WIDTH;
			return width;
		}

		public static void SetBrowserHeight(HttpSessionState session, int height)
		{
			session[SESS_BROWSER_HEIGHT] = height;
		}
		public static int GetBrowserheight(HttpSessionState session)
		{
			int height = DEFAULT_BROWSER_HEIGHT;
			try
			{
				height = (int)session[SESS_BROWSER_HEIGHT];
			}
			catch (Exception)
			{
			}
			if (height < DEFAULT_BROWSER_HEIGHT) height = DEFAULT_BROWSER_HEIGHT;
			return height;
		}

		#endregion
	}
}