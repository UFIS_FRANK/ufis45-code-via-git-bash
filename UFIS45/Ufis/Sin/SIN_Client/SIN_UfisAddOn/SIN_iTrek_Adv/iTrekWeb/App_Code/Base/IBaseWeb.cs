using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;

using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.ENT;

using UFIS.Web.ITrek.WebMgr;

namespace UFIS.Web.ITrek.Base
{
    /// <summary>
    /// Summary description for IBaseWeb
    /// </summary>
    public interface IBaseWeb
    {
        EntUser LoginedUser{ get; }
        string LoginedId { get; }
        List<string> LoginedUserGroupList{ get; }
        void DoUnAuthorise();
    }
}
