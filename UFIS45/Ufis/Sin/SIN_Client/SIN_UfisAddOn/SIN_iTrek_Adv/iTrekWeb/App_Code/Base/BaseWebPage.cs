using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;

using UFIS.ITrekLib.ENT;

namespace UFIS.Web.ITrek.Base
{
    /// <summary>
    /// Summary description for BaseWebPage
    /// </summary>
    public class BaseWebPage : Page, IBaseWeb
    {
        private BaseWeb _baseWeb = null;

        public BaseWebPage()
            : base()
        {
            
        }

        private BaseWeb MyBaseWeb
        {
            get
            {
                if (_baseWeb == null) _baseWeb = new BaseWeb(Session, Response);
                return _baseWeb;
            }
        }

        #region User Authentication Handling
        public EntUser LoginedUser
        {
            get
            {
                return MyBaseWeb.LoginedUser;
            }
        }

        public string LoginedId
        {
            get
            {
                return MyBaseWeb.LoginedId;
            }
        }

        public List<string> LoginedUserGroupList
        {
            get
            {
                return MyBaseWeb.LoginedUserGroupList;
            }
        }

        public virtual void DoUnAuthorise()
        {
            MyBaseWeb.DoUnAuthorise();
        }

        #endregion
    }

}