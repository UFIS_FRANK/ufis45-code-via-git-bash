using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.Web.ITrek.WebMgr;
using UFIS.ITrekLib.Util;
using iTrekSessionMgr;
using System.Timers;
using UFIS.ITrekLib.Ctrl;

using MM.UtilLib;

using System.IO;

/// <summary>
/// Summary description for Global.
/// </summary>
public class Global : System.Web.HttpApplication
{
    /// <summary>
    /// Required designer variable.
    /// </summary>
    /// 
    private System.Timers.Timer timer = null;

    private static MgrApp webAppMgr = new MgrApp();

    public Global()
    {
        //InitializeComponent();
    }

    private void InitializeTimer()
    {
        LogTraceMsg("Init Timer");
        if (timer == null)
        {
            timer = new System.Timers.Timer();
            timer.AutoReset = true;
            timer.Interval = 60000;
            //timer.Interval = CtrlConfig.GetWebRefreshTiming() * 1000;
            timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
            LogTraceMsg("Timer Initalized.");
        }
    }


    private void timer_Elapsed(object source, System.Timers.ElapsedEventArgs e)
    {
        //LogTraceMsg("Timerstart");
        //UFIS.ITrekLib.Ctrl.CtrlStaff.GetAllStaffNames();
    }

    private void LogTraceMsg(string msg)
    {
        UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("Web-App", msg);
    }

   private static UtilTraceLog _traceLog = UtilTraceLog.GetInstance();

    protected void Application_Start(Object sender, EventArgs e)
    {
        // Code that runs on application startup
       string baseDir = AppDomain.CurrentDomain.BaseDirectory;
       DirectoryInfo directoryInfo =
                    Directory.GetParent(baseDir);
       baseDir = directoryInfo.FullName;
       directoryInfo =
                    Directory.GetParent(baseDir);
       baseDir = directoryInfo.FullName;

       string logDir = Path.Combine( Path.Combine( baseDir, "XML" ), "LOG" );
       if (logDir[logDir.Length - 1] != '\\') logDir += "\\";
       LogTraceMsg( logDir );
       _traceLog.SetTrace(true );
       _traceLog.SetLogDir( logDir );
       _traceLog.SetLogFileName("Web");
       _traceLog.LogTraceMsg( "Global", "Start");

        LogTraceMsg("Appstart");
        LogMsg("Web Applicatioin Start");
        webAppMgr.LoadConfig2(Application);
        //UFIS.ITrekLib.Ctrl.CtrlStaff.GetAllStaffNames();
        //InitializeTimer();
        //Application["Expiration"] = 60;
        //// Create an instance of the Authentication
        //auth.Authentication AuthenticationInstance = new auth.Authentication();
        ////To request a token from the Authentication Web Service use:  AuthenticationInstance.getToken(string_Username,string_Password,int_Expiration)
        //string token = AuthenticationInstance.getToken("[username]", "[password]", System.Convert.ToInt32(Application["Expiration"]));
        //Application["Token"] = token;
        //// Store the time to request a new token before the current one expires

    }

    protected void Application_End(Object sender, EventArgs e)
    {
        //  Code that runs on application shutdown
        LogMsg("Web Applicatioin End");
    }

    protected void Application_BeginRequest(Object sender, EventArgs e)
    {

    }

    protected void Application_EndRequest(Object sender, EventArgs e)
    {

    }

    protected void Application_AuthenticateRequest(Object sender, EventArgs e)
    {

    }

    protected void Application_Error(Object sender, EventArgs e)
    {
        // Code that runs when an unhandled error occurs
        LogMsg("Web Applicatioin Error :" + e.ToString());
    }

    protected void Session_Start(Object sender, EventArgs e)
    {
        LogMsg("Web Session Start for session id " + Session.SessionID);
    }

    protected void Session_End(Object sender, EventArgs e)
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.
        string sessId = Session.SessionID;

        try
        {
            SessionManager sessMgr = new SessionManager();
            sessMgr.DeleteSessionDirectory(sessId);
        }
        catch (Exception ex)
        {
            LogMsg("WEBDelSess:Err:" + ex.Message);
        }
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.
        LogMsg("Web Session End for session id " + sessId);
    }


    void LogMsg(string msg)
    {
        //iTrekUtils.iTUtils.LogToEventLog(msg);
        UFIS.ITrekLib.Util.UtilLog.LogToGenericEventLog("Web-App", msg);
    }

    #region Web Form Designer generated code
    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>

    #endregion
}


