using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;

using UFIS.ITrekLib.ENT;

namespace UFIS.Web.ITrek.Base
{
    /// <summary>
    /// Summary description for BaseWebUserControl
    /// </summary>
    public class BaseWebUserControl : UserControl, IBaseWeb
    {
        private BaseWeb _baseWeb = null;

        public BaseWebUserControl(): base()
        {
            _baseWeb = new BaseWeb(Session, Response);
        }

        #region User Authentication Handling
        public EntUser LoginedUser
        {
            get
            {
                return _baseWeb.LoginedUser;
            }
        }

        public string LoginedId
        {
            get
            {
                return _baseWeb.LoginedId;
            }
        }

        public List<string> LoginedUserGroupList
        {
            get
            {
                return _baseWeb.LoginedUserGroupList;
            }
        }

        public virtual void DoUnAuthorise()
        {
            _baseWeb.DoUnAuthorise();
        }

        #endregion
    }

}