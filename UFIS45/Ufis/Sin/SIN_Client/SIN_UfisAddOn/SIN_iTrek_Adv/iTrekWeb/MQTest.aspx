<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MQTest.aspx.cs" Inherits="MQTest" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        &nbsp;
        <table>
        <tr><td><asp:Label ID="lblMQName" runat="server" Text="MQ Name to send" Width="136px"></asp:Label>
        <asp:TextBox ID="txtMQNameSend" runat="server" Width="336px">ITREK.ITREK.SOCC.RPT</asp:TextBox></td></tr>
        <tr><td>        <asp:Label ID="Label1" runat="server" Text="Message To Send"></asp:Label>
</td></tr>
        <tr><td>        <asp:TextBox ID="txtMsgToSend" runat="server" Height="159px" TextMode="MultiLine"
            Width="600px"></asp:TextBox>&nbsp;
</td></tr>
        <tr><td>
                <asp:Button ID="btnSend" runat="server" OnClick="btnSend_Click" Text="Sent to back end" />&nbsp;
</td></tr>
        <tr><td>
                <asp:Label ID="lblRcvQName" runat="server" Text="MQ Name to receive" Width="150px"></asp:Label>
        <asp:TextBox ID="txtMQNameRcv" runat="server" Width="288px">ITREK.SOCC.ITREK.RPT</asp:TextBox>
</td></tr>
        <tr><td>
                <asp:Button ID="btnRcv" runat="server" OnClick="btnRcv_Click" Text="Receive from back end" />
</td></tr>
        <tr><td>
                <asp:TextBox ID="txtRcvMsg" runat="server" Height="221px" TextMode="MultiLine" Width="602px"></asp:TextBox>
</td></tr>
        </table>
        </div>
    </form>
</body>
</html>
