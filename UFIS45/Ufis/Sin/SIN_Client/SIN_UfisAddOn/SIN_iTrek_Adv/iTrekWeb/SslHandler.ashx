<%@ WebHandler Language="C#" Class="SslHandler" %>

using System;
using System.Web;

public class SslHandler : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        //context.Response.ContentType = "text/plain";
        //context.Response.Write("Hello World");
        String url = context.Request.Url.ToString().ToLower();
        if (context.Request.IsSecureConnection)
        {
            if (!url.Contains("/Login2.aspx".ToLower()))
            {
                //Route(url.Replace("https:", "http:"), context);
                context.Response.Redirect(url.Replace("https:", "http:"));
            }
        }
        else
        {
            if (url.Contains("/Login2.aspx".ToLower()))
            {
                //Route(url.Replace("http:", "https:"), context);
                context.Response.Redirect(url.Replace("http:", "https:"));
            }
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    /// <summary>
    /// Route to the requested page
    /// </summary>
    /// <param name="route"></param>
    /// <param name="context"></param>
    //private void Route(String route, HttpContext context)
    //{
    //    //String target = "~/services/" + route + ".aspx";
    //    string target = route;

    //    IHttpHandler h = PageParser.GetCompiledPageInstance(target, null, context);
    //    h.ProcessRequest(context);
    //}

}

