<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WUFlightDepApron.ascx.cs" Inherits="Web_iTrek_WUFlightDepApron" %>
<div style="width:97%;" class="Heading">Departure Flight - Apron
&nbsp&nbsp&nbsp<asp:Label ID="lblHdr" runat="server" Text=" " CssClass="SubHeading"></asp:Label>
</div>
<div>
<asp:GridView ID="gvFlight" runat="server" AutoGenerateColumns="False" BackColor="White"
    BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black"
    GridLines="Vertical" OnRowDataBound="gvFlight_RowDataBound">
    <FooterStyle BackColor="#CCCCCC" />
    <Columns>
        <asp:BoundField DataField="URNO" HeaderText="URNO" Visible="False" ReadOnly="True" />
        <asp:TemplateField HeaderText="Departure">
            <ItemStyle HorizontalAlign="Left" Width="90px" />
            <ItemTemplate>
                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# Eval("URNO", "ChgDepApronFlightTiming.aspx?URNO={0}") + Eval("FLNO","&FLNO={0}&AD=D&AB=A") %>'
                    Text='<%# Eval("FLNO") %>'></asp:HyperLink>
                <asp:Literal ID="ltlTfln" runat="server" Text='<%# Eval("TFLN", "<BR>({0})") %>'></asp:Literal>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="REGN" HeaderText="Regn" ReadOnly="True" />
        <asp:BoundField DataField="ST_TIME" DataFormatString="{0:hhmm}" HeaderText="STD" HtmlEncode="False" ReadOnly="True" />
        <asp:BoundField DataField="ET_TIME" DataFormatString="{0:hhmm}" HeaderText="ETD" HtmlEncode="False" ReadOnly="True" />
        <asp:BoundField DataField="BAY" HeaderText="Bay" ReadOnly="True" />
        <asp:TemplateField HeaderText="MAB">
            <EditItemTemplate>
                &nbsp;<table>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblEdApron" runat="server" Text='<%# Eval("MAB_TIME", "{0:hhmm}") + "&nbsp" %>'></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:TextBox ID="txtNewMAB" runat="server" Text='<%# Bind("MAB_TIME", "{0:hhmm}") %>'
                                Width="42px"></asp:TextBox></td>
                    </tr>
                </table>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblDispApron" runat="server" Text='<%# Bind("MAB_TIME", "{0:hhmm}") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Load Start">
            <EditItemTemplate>
                &nbsp;<table>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblEdLoadStart" runat="server" Text='<%# Eval("LD_ST_TIME", "{0:hhmm}") + "&nbsp" %>'></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:TextBox ID="txtNewLoadStart" runat="server" Text='<%# Bind("LD_ST_TIME", "{0:hhmm}") %>'
                                Width="48px"></asp:TextBox></td>
                    </tr>
                </table>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblDispLoadStart" runat="server" Text='<%# Bind("LD_ST_TIME", "{0:hhmm}") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Load End">
            <EditItemTemplate>
                &nbsp;<table>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblEdLoadEnd" runat="server" Text='<%# Eval("LD_END_TIME", "{0:hhmm}") + "&nbsp" %>'></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:TextBox ID="txtNewLoadEnd" runat="server" Text='<%# Bind("LD_END_TIME", "{0:hhmm}") %>'
                                Width="50px"></asp:TextBox></td>
                    </tr>
                </table>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblDispLoadEnd" runat="server" Text='<%# Bind("LD_END_TIME", "{0:hhmm}") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Last Door">
            <EditItemTemplate>
                &nbsp;<table>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblEdLastDoor" runat="server" Text='<%# Eval("LST_DOOR_TIME", "{0:hhmm}") + "&nbsp" %>'></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:TextBox ID="txtNewLastDoor" runat="server" Text='<%# Bind("LST_DOOR_TIME", "{0:hhmm}") %>'
                                Width="50px"></asp:TextBox></td>
                    </tr>
                </table>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblDispLastDoor" runat="server" Text='<%# Bind("LST_DOOR_TIME", "{0:hhmm}") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="PLB">
            <EditItemTemplate>
                &nbsp;<table>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblEdPLB" runat="server" Text='<%# Eval("PLB_TIME", "{0:hhmm}") + "&nbsp" %>'></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:TextBox ID="txtNewPLB" runat="server" Text='<%# Bind("PLB_TIME", "{0:hhmm}") %>'
                                Width="50px"></asp:TextBox></td>
                    </tr>
                </table>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblDispPLB" runat="server" Text='<%# Bind("PLB_TIME", "{0:hhmm}") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="AT_TIME" DataFormatString="{0:hhmm}" HeaderText="ATD" HtmlEncode="False" ReadOnly="True" />
        <asp:HyperLinkField DataNavigateUrlFields="URNO,FLNO" DataNavigateUrlFormatString="Staff.aspx?URNO={0}&amp;FLNO={1}&amp;AD=D&amp;AB=A"
            HeaderText="Staff" Text="Show" />
        <asp:HyperLinkField DataNavigateUrlFields="URNO,FLNO" DataNavigateUrlFormatString="DLS.aspx?URNO={0}&amp;FLNO={1}&amp;AD=D&amp;AB=A"
            HeaderText="DLS" Text="Show" />
        <asp:HyperLinkField DataNavigateUrlFields="URNO,FLNO" DataNavigateUrlFormatString="Trips.aspx?URNO={0}&amp;FLNO={1}&amp;AD=D&amp;AB=A"
            HeaderText="Trips" Text="Show" />
<asp:HyperLinkField HeaderText="Apron Service Report" DataNavigateUrlFormatString="ApronServiceReport.aspx?URNO={0}&amp;FLNO={1}&amp;AD=D&amp;AB=A" DataNavigateUrlFields="URNO,FLNO">
<ItemStyle Width="80px" HorizontalAlign="Center"></ItemStyle>
</asp:HyperLinkField>            
        <asp:BoundField HeaderText="ALO" ReadOnly="True" DataField="WD_AO" />
        <asp:HyperLinkField DataNavigateUrlFields="URNO,FLNO" DataNavigateUrlFormatString="FlightMessage.aspx?URNO={0}&amp;FLNO={1}&amp;AD=D&amp;AB=A"
            HeaderText="MSG" Text="New" />
    </Columns>
    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
    <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
    <AlternatingRowStyle BackColor="#CCCCCC" />
</asp:GridView>
    <asp:HiddenField ID="hfScrolPos" runat="server" Value="0" />
</div>