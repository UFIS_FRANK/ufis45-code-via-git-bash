<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WUFlightDepBaggage.ascx.cs" Inherits="Web_iTrek_WUFlightDepBaggage" %>
<div style="width:97%;" class ="Heading">Departure Flight - Baggage
<asp:Label ID="lblHdr" runat="server" Text=" " CssClass="SubHeading"></asp:Label>
</div>
<div>
<asp:GridView ID="gvFlight" runat="server" BackColor="White" BorderColor="#999999"
    BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black" GridLines="Vertical" 
     OnRowDataBound="gvFlight_RowDataBound" AutoGenerateColumns="False" Width="587px">
    <FooterStyle BackColor="#CCCCCC" />
    <Columns>
        <asp:BoundField DataField="URNO" HeaderText="URNO" Visible="False" ReadOnly="True" />
        <asp:TemplateField HeaderText="Departure">
            <ItemStyle HorizontalAlign="Left" Width="90px" />
            <ItemTemplate>
                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# Eval("URNO", "ChgDepBaggageFlightTiing.aspx?URNO={0}") + Eval("FLNO","&FLNO={0}&AD=D&AB=B") %>'
                    Text='<%# Eval("FLNO") %>'></asp:HyperLink>
                <asp:Literal ID="ltlTfln" runat="server" Text='<%# Eval("TFLN", "<BR>({0})") %>'></asp:Literal>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="REGN" HeaderText="Regn" ReadOnly="True" />
        <asp:BoundField DataField="ST_TIME" DataFormatString="{0:hhmm}" HeaderText="STD" HtmlEncode="False" ReadOnly="True" />
        <asp:BoundField DataField="ET_TIME" DataFormatString="{0:hhmm}" HeaderText="ETD" HtmlEncode="False" ReadOnly="True" />
        <asp:BoundField DataField="BAY" HeaderText="Bay" ReadOnly="True" />
        <asp:TemplateField HeaderText="First Trip">
            <EditItemTemplate>
                &nbsp;<table style="width: 52px">
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblEdFirstTrip" runat="server" Text='<%# Eval("BG_FST_TRP_TIME", "{0:hhmm}") + "&nbsp" %>'></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:TextBox ID="txtNewFirstTrip" runat="server" Text='<%# Bind("BG_FST_TRP_TIME", "{0:hhmm}") %>'
                                Width="50px"></asp:TextBox></td>
                    </tr>
                </table>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblDispFirstTrip" runat="server" Text='<%# Bind("BG_FST_TRP_TIME", "{0:hhmm}") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Last Trip">
            <EditItemTemplate>
                &nbsp;<table style="width: 55px">
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblEdLastTrip" runat="server" Text='<%# Eval("BG_LST_TRP_TIME", "{0:hhmm}") + "&nbsp" %>'></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:TextBox ID="txtNewLastTrip" runat="server" Text='<%# Bind("BG_LST_TRP_TIME", "{0:hhmm}") %>'
                                Width="50px"></asp:TextBox></td>
                    </tr>
                </table>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblDispLastTrip" runat="server" Text='<%# Bind("BG_LST_TRP_TIME", "{0:hhmm}") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:HyperLinkField DataNavigateUrlFields="URNO,FLNO" DataNavigateUrlFormatString="FlightMessage.aspx?URNO={0}&amp;FLNO={1}&amp;AD=D&amp;AB=B"
            HeaderText="MSG" Text="New" />
    </Columns>
    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
    <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
    <AlternatingRowStyle BackColor="#CCCCCC" />
</asp:GridView>
    <asp:HiddenField ID="hfScrollPos" runat="server" Value="0" />
</div>