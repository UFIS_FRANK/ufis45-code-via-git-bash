using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.Ctrl;
using UFIS.Web.ITrek.WebMgr;
using UFIS.Web.ITrek.Misc;
using MMSoftware.MMWebUtil;
using UFIS.ITrekLib.Util;
using UFIS.ITrekLib.ENT;
using MM.UtilLib;

public partial class Web_iTrek_WUFlightDepBaggage : System.Web.UI.UserControl
{
    const string DEF_HDR = "Departure Flight - Baggage";
    private bool _isGridHeaderFreeze = false;

    const int CELL_NO_FLNO = 1;

    public bool IsGridHeaderFreeze
    {
        get { return _isGridHeaderFreeze; }
        set { _isGridHeaderFreeze = value; }
    }

    private void FreezeGridHeader()
    {
        if (IsGridHeaderFreeze)
        {
            MiscRender.FreezeGridHeader(this.Page, this.GetType(),
                gvFlight.ClientID, "WrapperDivDepBag",
                hfScrollPos.ClientID, hfScrollPos.Value);
        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        RetrieveUserInformation();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //RetrieveUserInformation();
    }

    private EntUser user = null;
    private string _userId = "";

    private void RetrieveUserInformation()
    {
        try
        {
            if (user == null)
            {
                user = MgrSess.GetUserInfo(Session);
            }
            _userId = user.UserId;
        }
        catch (Exception)
        {
        }
    }

    public void Show(DSFlight ds, string terminalId)
    {
        try
        {
            DataView dv = ds.FLIGHT.DefaultView;
            string stFilter = "A_D='D'";
            if (terminalId == null) terminalId = CtrlTerminal.GetDefaultTerminalId();
            else if (terminalId == "") terminalId = CtrlTerminal.GetDefaultTerminalId();

            if (CtrlTerminal.GetDefaultTerminalId() != terminalId)
            {
                stFilter += " AND TERMINAL='" + terminalId + "'";
            }

            dv.RowFilter = stFilter;
            Show(dv);
            lblHdr.Text = DEF_HDR + "(" + CtrlTerminal.GetTerminalName(terminalId) + ")";

        }
        catch (Exception ex)
        {
            LogMsg("Show:Err:" + ex.Message);
        }
    }

    public void Show(DSFlight ds, EntWebFlightFilter entFilter)
    {
        try
        {
            DataView dv = ds.FLIGHT.DefaultView;
            string stFilter = "A_D='D'";

            dv.RowFilter = stFilter;
            Show(dv);
            lblHdr.Text = " [ " + entFilter.FilterString + " ]";

        }
        catch (Exception ex)
        {
            LogMsg("Show:Err:" + ex.Message);
        }
    }

    public void Show(DataView dv)
    {
        try
        {
            dv.Sort = "FLIGHT_DT ASC";
            gvFlight.DataSource = dv;
            gvFlight.DataBind();
            //lblHdr.Text = dv.Count + ", " + dv.Table.Rows.Count;
            FreezeGridHeader();
        }
        catch (Exception ex)
        {
            LogMsg("ShowDV:Err:" + ex.Message);
        }
    }

    public void PopulateGridForAFlight(string urNo, string flightNo, bool editMode)
    {
        try
        {
            if (editMode)
            {
                gvFlight.EditIndex = 0;
                MgrView.SetFlightNoEdit(ViewState, flightNo);
                MgrView.SetModeEdit(ViewState, editMode);
                MgrView.SetUrNoEdit(ViewState, urNo);
            }
            PopulateGridForAFlight(urNo, flightNo);
        }
        catch (Exception ex)
        {
            LogMsg("PopulateGridForAFlight:Err:" + ex.Message);
        }
    }

    public void PopulateGridForAFlight(string urNo, string flightNo)
    {
        try
        {
            DSFlight ds = null;

            //        ds = CtrlFlight.RetrieveFlightsByUrNo_FlightNo(urNo, flightNo);
            ds = CtrlFlight.RetrieveFlightsByFlightId(urNo);
            DataView dv = ds.FLIGHT.DefaultView;
            dv.RowFilter = "FLNO='" + flightNo + "' AND URNO='" + urNo + "'";

            Show(dv);
        }
        catch (Exception ex)
        {
            LogMsg("PopulateGridForAFlightUF:Err:" + ex.Message);
        }
    }

    public void SaveEditInfo()
    {
        try
        {
            if (MgrView.GetModeEdit(ViewState))
            {//Edit Mode
                string stURNO = MgrView.GetUrNoEdit(ViewState);
                String stFlightNo = MgrView.GetFlightNoEdit(ViewState);
                string flightDateTime = CtrlFlight.GetFlightDateTime(stURNO);

                string stFirstTrip = ConvTime(0, "txtNewFirstTrip", flightDateTime);
                string stLastTrip = ConvTime(0, "txtNewLastTrip", flightDateTime);
                CtrlFlight.SaveDepBagTimingInfo(stURNO, stFlightNo, stFirstTrip, stLastTrip, _userId);
                //WebUtil.AlertMsg(Page, stURNO + "\n" + stFlightNo + "\n" + stFirstTrip + "\n" + stLastTrip );
            }
            else
            {
                WebUtil.AlertMsg(Page, "Unable to save. It is not in Edit Mode.");
            }
        }
        catch (ApplicationException ex)
        {
            throw new ApplicationException(ex.Message);
        }
        catch (Exception ex)
        {
            LogMsg("SaveEditInfo:Err:" + ex.Message);
            throw new ApplicationException("Fail to save.");
        }
    }

    private string ConvTime(int rowNum, string txtBoxName, string refUfisDt)
    {
        string stDt = "";
        try
        {
            string stTime = GetTextBoxData(rowNum, txtBoxName);
            if (stTime != "")
            {
                if (UtilTime.IsValidHHmm(stTime))
                {
                    stDt = UtilTime.ConvHHmmToUfisTimeAfterRefTime(refUfisDt, stTime);
                }
                else
                {
                    throw new ApplicationException("Invalid Time Format.");
                }
            }
        }
        catch (ApplicationException ex)
        {
            throw new ApplicationException(ex.Message);
        }
        catch (Exception)
        {
        }
        return stDt;
    }

    private string GetTextBoxData(int rowNum, string txtBoxName)
    {
        string st = "";
        try
        {
            st = ((TextBox)gvFlight.Rows[rowNum].FindControl(txtBoxName)).Text;
        }
        catch (Exception)
        {
            //throw;
        }
        return st;
    }
    

    protected void gvFlight_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                try
                {
                    MiscRender.HighLightTransit(e.Row, CELL_NO_FLNO);
                }
                catch (Exception)
                {
                }
            }
        }
        catch (Exception ex)
        {
            LogMsg("RowBound:Err:" + ex.Message);
        }
    }


    private void LogMsg(string msg)
    {
        UtilLog.LogToGenericEventLog("WUDepBag", msg);
    }

}
