using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;
using UFIS.Web.ITrek.WebMgr;
using UFIS.Web.ITrek.Sec;
using MMSoftware.MMWebUtil;
using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.Util;

public partial class Web_iTrek_FlightInfo : System.Web.UI.Page
{
   private EntUser user = null;
   private string user_dept = "";
   private string user_role = "";
   private const string PG_NAME = "Flight";
   //private const string PG_NAME_ARR_APR = "FlightArrApr";
   //private const string PG_NAME_ARR_BAG = "FlightArrBag";
   //private const string PG_NAME_DEP_APR = "FlightDepApr";
   //private const string PG_NAME_DEP_BAG = "FlightDepBag";
   private const string CTRL_ASSGN_FL = "ViewAssignFlight";
   private const string CTRL_VIEW_ALL = "ViewAllFlight";


   protected void Page_Load(object sender, EventArgs e)
   {
      try
      {
         this.MaintainScrollPositionOnPostBack = true;
         CtrlSec ctrlsec = CtrlSec.GetInstance();
         user = ctrlsec.AuthenticateUser(Session, Response);
         //((MyMasterPage)Master).AutoRefreshMode = false;

		 //WUFlightArrApron1.Visible = false;
		 WUArAp1.Visible = false;
		 //WUFlightArrBaggage1.Visible = false;
		 WUFlAB1.Visible = false;
         WUDeAr1.Visible = false;
         WUDeBg1.Visible = false;
         RetrieveUserInformation();
         if (!IsPostBack)
         {
            //CtrlTerminal.PopulateWebDDTerminal(ddTerminal, "");
            CtrlTerminal.PopulateWebListboxTerminal(lbTerm, "");
            CtrlFlight ctrlFlight = CtrlFlight.GetInstance();
            WebUtil.PopulateDropDownList(ddTime, ctrlFlight.GetTimeIndTable().DefaultView,
                "VAL", "ID",
                ctrlFlight.GetDefaultTimeInd());
         }

         ShowFlightInfo();
         WebUtil.SetFocus(Page, this.lbTerm);
      }
      catch (ApplicationException ex)
      {
         LogMsg("PageLoad:AppErr:" + ex.Message);
         WebUtil.AlertMsg(Page, ex.Message);

      }
      catch (Exception ex)
      {
         LogMsg("PageLoad:Err:" + ex.Message);
         WebUtil.AlertMsg(Page, "Error accesing flight information.");
      }
   }


   private string GetArrivalOrDeparture()
   {
      string st = "A";//Default to Arrival;
      if (Request.Params["AD"] != null)
      {
         st = Request.Params["AD"].Trim().ToUpper();
      }
      else
      {
         st = MgrSess.GetFlightArrDep(Session);
      }

      if ((st != "A") && (st != "D"))
      {
         st = "A";
      }
      return st;
   }

   private string GetApronOrBaggage()
   {
      string st = "A"; //Default to Apron
      if (Request.Params["AB"] != null) { st = Request.Params["AB"].Trim().ToUpper(); }
      else
      {
         st = MgrSess.GetFlightAprBag(Session);
         if (st == "")
         {
            if (user.Dept == "B") st = "B";
         }
      }
      if ((st != "A") && (st != "B"))
      {
         st = "A";//Default to Apron
      }
      return st;
   }

   private void LogMsg(string msg)
   {
      try
      {
         MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("FlightInfo", msg);
      }
      catch (Exception)
      {
         UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("FlightInfo", msg);
      }
   }

   string stTime = "T";

   private string GetSelectedTerminalIds()
   {
      string terminalIds = "";
      string del = "";
      int cnt = lbTerm.Items.Count;
      for (int i = 0; i < cnt; i++)
      {
         if (lbTerm.Items[i].Selected)
         {
            string tId = lbTerm.Items[i].Value.ToString();
            if (tId == CtrlTerminal.GetAllTerminalId())
            {//User choose "All Terminal"
               terminalIds = "";
               break;
            }
            else
            {
               terminalIds += del + lbTerm.Items[i].Value.ToString();
               del = CtrlFlight.TERMINAL_SEPERATOR;
            }
         }
      }

      return terminalIds;
   }

   private void ShowFlightInfo()
   {
      //LogMsg("Accessing Flight Info");
      String ab = "A";//Apron or Baggage (A/B)
      string ad = "A";//Arrival or Departure (A/D)

      DSFlight ds = null;
      //string terminal = "";
      //terminal = ddTerminal.SelectedValue.ToString();
      //if (terminal == "") terminal = CtrlTerminal.GetDefaultTerminalId(); //default to show flight info from all terminals
      stTime = ddTime.SelectedValue.ToString();//Days Option
      int frMinute = 0;
      int toMinute = 0;


      ad = GetArrivalOrDeparture();
      ab = GetApronOrBaggage();

      //string pageName = PG_NAME + "_" + ab + "_" + ad;
      string pageName = PG_NAME;
      if (ad == "A") { pageName += "Arr"; } else pageName += "Dep";
      if (ab == "A") pageName += "Apr"; else pageName += "Bag";

      CtrlSec ctrlSec = CtrlSec.GetInstance();
      //string stTerminal = ddTerminal.SelectedValue;
      //if (stTerminal == CtrlTerminal.GetAllTerminalId()) stTerminal = "";
      EntWebFlightFilter entFilter = new EntWebFlightFilter();
      //string stTerminal = GetSelectedTerminalIds();
      //entFilter.TerminalIds = stTerminal;
      //entFilter.AirlineCodes = txtALC.Text.Trim().ToUpper();
      //entFilter.DateInd = ddTime.SelectedItem.Text;
      //entFilter.FrTime = txtFrTime.Text.Trim();
      //entFilter.ToTime = txtToTime.Text.Trim();
      //entFilter.Regn = txtRegn.Text.Trim();
      //entFilter.DayOption = stTime;
      entFilter = FilterCondition;
      entFilter.FlightType = ad;
      switch (ab)
      {
         case "A":
            entFilter.Dept = EnUserDept.Apron;
            break;
         case "B":
            entFilter.Dept = EnUserDept.Baggage;
            break;
      }

      if (ctrlSec.HasAccessRights(user, pageName, CTRL_VIEW_ALL))
      {//Can see all the flight information.
         frMinute = CtrlFlight.GetWebFLINFO_FROM_MINUTESByTimeInd(stTime);
         toMinute = CtrlFlight.GetWebFLINFO_TO_MINUTESByTimeInd(stTime);
         entFilter.FrMinute = frMinute;
         entFilter.ToMinute = toMinute;
         //ds = CtrlFlight.RetrieveFlightsForWeb(stTerminal, frMinute, toMinute);
         ds = CtrlFlight.RetrieveFlightsForWeb(entFilter);
      }
      else if (ctrlSec.HasAccessRights(user, pageName, CTRL_ASSGN_FL))
      {//Can see the flights assigned to the person
         //ds = CtrlFlight.RetrieveAssignedFlights(stTerminal, user.UserId);
         frMinute = CtrlFlight.GetWebFLINFO_AO_FROM_MINUTESByTimeInd(stTime);
         toMinute = CtrlFlight.GetWebFLINFO_AO_TO_MINUTESByTimeInd(stTime);
         entFilter.FrMinute = frMinute;
         entFilter.ToMinute = toMinute;
         ds = CtrlFlight.RetrieveAssignedFlightsForWeb(entFilter, ad, user.UserId);
      }
      else
      {//Not Authorised to see the flight information.
         DoUnAuthorise();
      }

      DataView dv = ds.FLIGHT.DefaultView;
      if (ds.FLIGHT.Rows.Count > 0)
      {
         string stFilter = "A_D='" + ad + "'";
         //dv.RowFilter = stFilter;//AM 20080506 - Web Control Page will filter accordingly.
      }

      MgrSess.SetFlightAprBag(Session, ab);
      MgrSess.SetFlightArrDep(Session, ad);
      switch (ad + ab)
      {
         case "AA": //Arrival Apron
            //ShowArrivalApron(ds, terminal);
            ShowArrivalApron(ds, entFilter);
            break;
         case "AB": //Arrival Baggage
            //ShowArrivalBaggage(ds, terminal);
            ShowArrivalBaggage(ds, entFilter);
            break;
         case "DA": //Departure Apron
            //ShowDepartureApron(ds, terminal);
            ShowDepartureApron(ds, entFilter);
            break;
         case "DB": //Departure Baggage
            //ShowDepartureBaggage(ds, terminal);
            ShowDepartureBaggage(ds, entFilter);
            break;
         default://default to arrival apron
            //ShowArrivalApron(ds, terminal);
            ShowArrivalApron(ds, entFilter);
            break;
      }
   }

   private void DoUnAuthorise()
   {
      Response.Redirect("~/UnAuthorised.aspx", true);
   }

   private void ShowArrivalApron(DSFlight ds, string terminal)
   {
      //      Web_iTrek_WUFlightArrApron uc =
      //(Web_iTrek_WUFlightArrApron)Page.LoadControl("WUFlightArrApron.ascx");
      //      ph1.Controls.Add(uc);
      //uc.Show(ds, terminal);
	  //WUFlightArrApron1.Show(ds, terminal);
	  //WUFlightArrApron1.Visible = true;
	   WUArAp1.Show(ds, terminal);
	   WUArAp1.Visible = true;
   }

   private void ShowArrivalApron(DSFlight ds, EntWebFlightFilter entFilter)
   {
      //      Web_iTrek_WUFlightArrApron uc =
      //(Web_iTrek_WUFlightArrApron)Page.LoadControl("WUFlightArrApron.ascx");
      //      ph1.Controls.Add(uc);
      //uc.Show(ds, terminal);
	  //WUFlightArrApron1.Show(ds, entFilter);
	  //WUFlightArrApron1.Visible = true;
	   WUArAp1.Show(ds, entFilter);
	   WUArAp1.Visible = true;
   }

   private void ShowArrivalApron(DataView dv)
   {
      //      Web_iTrek_WUFlightArrApron uc =
      //(Web_iTrek_WUFlightArrApron)Page.LoadControl("WUFlightArrApron.ascx");
      //      ph1.Controls.Add(uc);
      //      uc.Show(dv);
	  //WUFlightArrApron1.Show(dv);
	  //WUFlightArrApron1.Visible = true;
	   WUArAp1.Show(dv);
	   WUArAp1.Visible = true;
   }

   private void ShowArrivalBaggage(DSFlight ds, string terminal)
   {
      //        Web_iTrek_WUFlightArrBaggage uc =
      //(Web_iTrek_WUFlightArrBaggage)Page.LoadControl("WUFlightArrBaggage.ascx");
      //        ph1.Controls.Add(uc);
      //        uc.Show(ds, terminal);
	  //WUFlightArrBaggage1.Show(ds, terminal);
	   //WUFlightArrBaggage1.Visible = true;
	  WUFlAB1.Show(ds, terminal);
	  WUFlAB1.Visible = true;
   }

   private void ShowArrivalBaggage(DSFlight ds, EntWebFlightFilter entFilter)
   {
	  //WUFlightArrBaggage1.Show(ds, entFilter);
	  //WUFlightArrBaggage1.Visible = true;
	   WUFlAB1.Show(ds, entFilter);
	   WUFlAB1.Visible = true;
   }

   private void ShowArrivalBaggage(DataView dv)
   {
      //        Web_iTrek_WUFlightArrBaggage uc =
      //(Web_iTrek_WUFlightArrBaggage)Page.LoadControl("WUFlightArrBaggage.ascx");
      //        ph1.Controls.Add(uc);
      //        uc.Show(dv);
	  //WUFlightArrBaggage1.Show(dv);
	  //WUFlightArrBaggage1.Visible = true;
	   WUFlAB1.Show(dv);
	   WUFlAB1.Visible = true;
   }

   private void ShowDepartureApron(DSFlight ds, string terminal)
   {
      //        Web_iTrek_WUFlightDepApron uc =
      //(Web_iTrek_WUFlightDepApron)Page.LoadControl("WUFlightDepApron.ascx");
      //        ph1.Controls.Add(uc);
      //        uc.Show(ds, terminal);
      WUDeAr1.Show(ds, terminal);
      WUDeAr1.Visible = true;
   }

   private void ShowDepartureApron(DSFlight ds, EntWebFlightFilter entFilter)
   {
      WUDeAr1.Show(ds, entFilter);
      WUDeAr1.Visible = true;
   }

   private void ShowDepartureApron(DataView dv)
   {
      //        Web_iTrek_WUFlightDepApron uc =
      //(Web_iTrek_WUFlightDepApron)Page.LoadControl("WUFlightDepApron.ascx");
      //        ph1.Controls.Add(uc);
      //        uc.Show(dv);
      WUDeAr1.Show(dv);
      WUDeAr1.Visible = true;
   }

   private void ShowDepartureBaggage(DSFlight ds, string terminal)
   {
      //        Web_iTrek_WUFlightDepBaggage uc =
      //(Web_iTrek_WUFlightDepBaggage)Page.LoadControl("WUFlightDepBaggage.ascx");
      //        ph1.Controls.Add(uc);
      //        uc.Show(ds,terminal);
      WUDeBg1.Show(ds, terminal);
      WUDeBg1.Visible = true;
   }

   private void ShowDepartureBaggage(DSFlight ds, EntWebFlightFilter entFilter)
   {
      WUDeBg1.Show(ds, entFilter);
      WUDeBg1.Visible = true;
   }

   private void ShowDepartureBaggage(DataView dv)
   {
      //        Web_iTrek_WUFlightDepBaggage uc =
      //(Web_iTrek_WUFlightDepBaggage)Page.LoadControl("WUFlightDepBaggage.ascx");
      //        ph1.Controls.Add(uc);
      //        uc.Show(dv);
      WUDeBg1.Show(dv);
      WUDeBg1.Visible = true;
   }

   private void RetrieveUserInformation()
   {
      try
      {
         //user = MgrSess.GetUserInfo(Session);
         user_dept = user.Dept;
         user_role = user.Role;
      }
      catch (Exception ex)
      {
         String st = ex.Message;
      }
   }

   protected void btnFilter_Click(object sender, EventArgs e)
   {
      try
      {
         //LogMsg("Filter_Clicked.");
         //PolpulateGrid("", "");
         //wuFlightArrApron1.PolpulateGrid("", "");
         //string terminal = ddTerminal.SelectedValue;
         //stTime = ddTime.SelectedValue;
         string stTerminal = GetSelectedTerminalIds();
         EntWebFlightFilter entFilter = new EntWebFlightFilter();
         entFilter.TerminalIds = stTerminal;
         entFilter.FrTime = txtFrTime.Text.Trim();
         entFilter.ToTime = txtToTime.Text.Trim();
         entFilter.Regn = txtRegn.Text.Trim();
         entFilter.AirlineCodes = txtALC.Text.Trim().ToUpper();
         entFilter.DayOption = ddTime.SelectedValue.ToString();//Days Option
         entFilter.DateInd = ddTime.SelectedItem.Text;
         FilterCondition = entFilter;
         ShowFlightInfo();
      }
      catch (Exception ex)
      {
         LogMsg("FilterClick:Err:" + ex.Message);
      }
   }

   private EntWebFlightFilter FilterCondition
   {
      get
      {
         EntWebFlightFilter obj = MgrSess.GetFlightInfoFilter(Session);
         //LogMsg("FilterCondition:Get:" + obj.FilterString);
         return obj;
         //return MgrSess.GetFlightInfoFilter(Session);
      }
      set
      {
         MgrSess.SetFlightInfoFilter(Session, value);
         //LogMsg("FilterCondition:Set:" + value.FilterString);
      }
   }
}