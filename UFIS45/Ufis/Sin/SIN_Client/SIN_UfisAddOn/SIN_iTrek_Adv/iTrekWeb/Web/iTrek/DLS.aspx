<%@ Page Language="C#" MasterPageFile="~/Web/iTrek/MasterPage.master" AutoEventWireup="true" CodeFile="DLS.aspx.cs" Inherits="Web_iTrek_DLS" Title="DLS" Theme="SATSTheme" %>

<%@ Register Src="WUFlightDepApron.ascx" TagName="WUFlightDepApron" TagPrefix="uc1" %>
<%@ Register Src="WUFlightDepBaggage.ascx" TagName="WUFlightDepBaggage" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" Runat="Server">
<div>
    &nbsp;<uc1:WUFlightDepApron ID="wuFlightDepApron1" runat="server" EnableViewState="false"
        Visible="false" />
</div>
<div>
    <table style="width: 990px">
        <tr>
            <td style="width: 100px">
                <uc2:WUFlightDepBaggage ID="wuFlightDepBaggage1" runat="server" EnableViewState="false"
                    Visible="false" />
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
DLS<br />
                <asp:TextBox ID="txtDLS" runat="server" Height="500px" TextMode="MultiLine"
        Width="800px" ReadOnly="True"></asp:TextBox></td>
        </tr>
    </table>
    &nbsp;</div>
</asp:Content>

