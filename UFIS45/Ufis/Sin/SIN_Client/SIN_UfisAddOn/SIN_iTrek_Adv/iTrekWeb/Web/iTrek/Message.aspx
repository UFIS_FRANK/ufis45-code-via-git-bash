<%@ Page Language="C#" MasterPageFile="MasterPage.master" AutoEventWireup="true" CodeFile="Message.aspx.cs" Inherits="Web_iTrek_Message" Title="Messages" Theme="SATSTheme" %>
<%@ Register Src="WUMsgDet.ascx" TagName="WUMsgDet" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" Runat="Server">
    <table style="width: 983px; height: 500px;">
        <tr>
            <td style="width: 300px;" align="left" valign="top">
                <table style="width: 275px">
                    <tr>
                        <td>
                Compose Message</td>
                    </tr>
                    <tr>
                        <td style="height: 137px;">
                <asp:TextBox ID="txtComposeMessage" runat="server" Height="114px" TextMode="MultiLine"
                    Width="265px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td>
                Predefined Messages</td>
                    </tr>
                    <tr>
                        <td>
                <asp:DropDownList ID="ddPredefMsgs" runat="server" Width="270px" DataTextField="MSGTEXT" DataValueField="URNO" 
AutoPostBack="True" OnSelectedIndexChanged="ddPredefMsgs_SelectedIndexChanged">
                    <asp:ListItem></asp:ListItem>
                </asp:DropDownList></td>
                    </tr>
                    <tr>
<td style="height: 180px; border-right: gray thin solid; border-top: gray thin solid; border-left: gray thin solid; border-bottom: gray thin 
solid;">
Send To
                <asp:UpdatePanel id="UpdatePanel1" runat="server">
                    <contenttemplate>
<DIV style="OVERFLOW: auto; WIDTH: 265px; HEIGHT: 220px"><asp:CheckBoxList id="cblSendTo" runat="server" Width="260px" Height="20px"></asp:CheckBoxList></DIV><asp:Timer id="Timer1" runat="server" OnTick="Timer1_Tick" Interval="50000"></asp:Timer> 
</contenttemplate>
                </asp:UpdatePanel></td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <table>
                                <tr>
                                    <td style="width: 49px">
                                    </td>
                                    <td style="width: 149px">
                            <asp:Button ID="btnSendMessage" runat="server" Text="Send Message" OnClick="btnSendMessage_Click" EnableViewState="False" /></td>
                                    <td style="width: 24px">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 600px;" valign="top" align="left">
                <table style="height: 450px">
                    <tr valign="top" align="center">
                        <td style="width: 300px; height: 16px">
                    INBOX &nbsp;&nbsp;
                        </td>
                        <td style="width: 300px; height: 16px">
                    OUTBOX &nbsp;&nbsp;
                       </td>
                    </tr>
                    <tr valign="top" align="center">
                        <td colspan="2" style="height: 370px"><DIV style="WIDTH: 710px; HEIGHT: 370px">
                            <asp:UpdatePanel id="UpdatePanel2" runat="server"><contenttemplate>
<TABLE style="WIDTH: 690px"><TBODY><TR  valign="top"><TD><asp:Panel id="Panel5" runat="server" Width="345px" EnableViewState="False" 
Height="360px"
ScrollBars="Auto"><asp:GridView id="gvInBox" runat="server" BackColor="White" Width="325px" CssClass="GridViewMsg" EnableViewState="False" 
OnSelectedIndexChanged="gvInBox_SelectedIndexChanged" OnRowDataBound="gvInBox_RowDataBound" AutoGenerateColumns="False" BorderColor="#999999" 
BorderStyle="Solid" BorderWidth="1px" CellPadding="3" DataKeyNames="URNO" ForeColor="Black" GridLines="Vertical" PageSize="50" OnRowCommand="gvInBox_RowCommand">
<FooterStyle BackColor="#CCCCCC"></FooterStyle>
<Columns>
<asp:BoundField ReadOnly="True" DataField="SENT_TIME" HeaderText="Time">
<ItemStyle Width="15px" HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="SENT_BY" HeaderText="From">
    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
</asp:BoundField>
<asp:ButtonField HeaderText="Text" DataTextField="MSG_SH_TEXT" CommandName="InDet">
<ItemStyle Width="90px" HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
</asp:ButtonField>
</Columns>
<SelectedRowStyle BackColor="#000099" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
<PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center"></PagerStyle>
<HeaderStyle BackColor="Black" ForeColor="White" Font-Bold="True"></HeaderStyle>
<AlternatingRowStyle BackColor="#CCCCCC"></AlternatingRowStyle>
</asp:GridView></asp:Panel> </TD><TD><asp:Panel id="Panel4" runat="server" Width="345px" 
EnableViewState="False" Height="360px" ScrollBars="Auto"><asp:GridView id="gvOutBox" runat="server" BackColor="White" Width="325px" 
EnableViewState="False" OnSelectedIndexChanged="gvOutBox_SelectedIndexChanged" AutoGenerateColumns="False" BorderColor="#999999" 
BorderStyle="Solid" BorderWidth="1px" CellPadding="3" DataKeyNames="MSG_URNO" ForeColor="Black" GridLines="Vertical" PageSize="50" 
OnRowCommand="gvOutBox_RowCommand">
<FooterStyle BackColor="#CCCCCC"></FooterStyle>
<Columns>
<asp:BoundField DataField="SENT_TIME" HeaderText="Time">
<ItemStyle Width="15px" HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="D_MSG_TO" HeaderText="Receiver">
<ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
</asp:BoundField>
<asp:ButtonField HeaderText="Text" DataTextField="MSG_SH_TEXT" CommandName="OutDet">
<ItemStyle Width="90px" HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
</asp:ButtonField>
</Columns>

<SelectedRowStyle BackColor="#000099" ForeColor="White" Font-Bold="True"></SelectedRowStyle>

<PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center"></PagerStyle>

<HeaderStyle BackColor="Black" ForeColor="White" Font-Bold="True"></HeaderStyle>

<AlternatingRowStyle BackColor="#CCCCCC"></AlternatingRowStyle>
</asp:GridView></asp:Panel> </TD></TR></TBODY></TABLE>
</contenttemplate>
                </asp:UpdatePanel></DIV></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="left" valign="top">
                <uc1:WUMsgDet ID="wuMsgDet1" Visible="false" runat="server" OnLoad="wuMsgDet1_Load" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

