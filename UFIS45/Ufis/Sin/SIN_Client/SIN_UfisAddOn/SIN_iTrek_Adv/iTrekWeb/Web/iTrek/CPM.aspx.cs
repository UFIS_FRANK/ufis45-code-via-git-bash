using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.ENT;
using UFIS.Web.ITrek.Sec;

public partial class Web_iTrek_CPM : System.Web.UI.Page
{
    private const string PG_NAME = "CPM";
    private const string CTRL_VIEW = "View";

    protected void Page_Load(object sender, EventArgs e)
    {
        CtrlSec ctrlsec = CtrlSec.GetInstance();
        EntUser user = ctrlsec.AuthenticateUser(Session, Response);
        if (ctrlsec.HasAccessRights(user, PG_NAME, CTRL_VIEW))
        {
            ShowFlightInfo();
        }
        else
        {
            DoUnAuthorise();
        }
    }

    private void DoUnAuthorise()
    {
        Response.Redirect("~/UnAuthorised.aspx", true);
    }

    private void ShowFlightInfo()
    {
        string flightNo = "", urNo = "", arrDep = "", aprBag = "";
        try
        {
            flightNo = Request.Params["FLNO"].ToString().Trim();
            urNo = Request.Params["URNO"].ToString().Trim();
            arrDep = Request.Params["AD"].ToString().Trim();
            aprBag = Request.Params["AB"].ToString().Trim();
        }
        catch (Exception)
        {
        }

        string cpmMsg = "";
        try
        {
            switch (aprBag)
            {
                case "A": //Arrival Apron
                    wuFlightArrApron1.Visible = true;
                    wuFlightArrApron1.PopulateGridForAFlight(urNo, flightNo);
                    break;
                case "B": //Arrival Baggage
                    wuFlightArrBaggage1.Visible = true;
                    wuFlightArrBaggage1.PopulateGridForAFlight(urNo, flightNo);
                    break;
                default:
                    break;
            }
        }
        catch (Exception )
        {
        }

        try
        {
            cpmMsg = CtrlCPM.RetrieveCPMForAFlight(urNo, flightNo);
        }
        catch (Exception ex)
        {
            cpmMsg = ex.Message;
        }
        txtCPM.Text = cpmMsg;
    }

}
