using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.ENT;
using UFIS.Web.ITrek.Sec;
using UFIS.ITrekLib.Util;
using MMSoftware.MMWebUtil;

public partial class Web_iTrek_BaggagePresentationTimingSummary : System.Web.UI.Page
{
    private const string PG_NAME = "BagPresTimeSumm";
    private const string CTRL_VIEW = "View";
	EntUser _user = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        CtrlSec ctrlsec = CtrlSec.GetInstance();
        _user = ctrlsec.AuthenticateUser(Session, Response);
        if (ctrlsec.HasAccessRights(_user, PG_NAME, CTRL_VIEW))
        {
            if (!IsPostBack)
            {
                CtrlTerminal.PopulateWebDDTerminal(ddTerminal, "");
            }
        }
        else
        {
            DoUnAuthorise();
        }
    }

    private void DoUnAuthorise()
    {
        Response.Redirect("~/UnAuthorised.aspx", true);
    }
    protected void btnGenReport_Click(object sender, EventArgs e)
    {
        try
        {
            GenerateReport();
        }
        catch (ApplicationException ex)
        {
            WebUtil.AlertMsg(Page, ex.Message);
        }
        catch (Exception ex)
        {
			LogMsg("GenerateReport:Err:" + ex.Message);
            WebUtil.AlertMsg(Page, "Fail to Generate report.");
        }
    }

    private void GenerateReport()
    {
		//LogTraceMsg("GenerateReport:Start.");
        string terminalId = ddTerminal.SelectedValue;
        string terminalName = ddTerminal.SelectedItem.Text;
        if (terminalId == CtrlTerminal.GetAllTerminalId()) terminalId = "";
        WUBagTimingSummRpt1.ShowReport(terminalId,
            terminalName,
            txtFromDate.Text,
            txtFromTime.Text,
            txtToDate.Text,
            txtToTime.Text, _user.UserId);
		//LogTraceMsg("GenerateReport:Generated.");
        btnPrint.Visible = true;
        WebUtil.SetJavaScriptAtTop( Page,
            "function PrintRpt(){ \n" +
            //"   alert('abc');\n" +
            "   open_window( 'PrtBagTimingSummRpt.aspx" +
            "?termId=" + terminalId + 
            "&termName=" + terminalName +
            "&frDate=" + txtFromDate.Text +
            "&frTime=" + txtFromTime.Text +
            "&toDate=" + txtToDate.Text +
            "&toTime=" + txtToTime.Text +
            "');\n" +
            "}");
    }

    protected void btnPrint_Click(object sender, EventArgs e)
    {
    }
    protected void btnGenNPrintReport_Click(object sender, EventArgs e)
    {
        try
        {
            GenerateReport();
            WebUtil.SetJavaScriptAtBottom(Page, "PrintRpt();");
        }
        catch (ApplicationException ex)
        {
            WebUtil.AlertMsg(Page, ex.Message);
        }
        catch (Exception ex)
        {
			LogMsg("GenerateReport:Err:" + ex.Message);
            WebUtil.AlertMsg(Page, "Fail to Generate report.");
        }
    }

	#region Logging
	private static void LogTraceMsg(string msg)
	{
		LogMsg(msg);
	}

	private static void LogMsg(string msg)
	{
		try
		{
			MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("BptSummRpt.aspx", msg);
		}
		catch (Exception)
		{
			UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("BptSummRpt.aspx", msg);
		}
	}
	#endregion
}
