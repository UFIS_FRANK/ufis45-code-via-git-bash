<%@ Page Language="C#" MasterPageFile="~/Web/iTrekB/Master.master" AutoEventWireup="true" CodeFile="BAShowCntr.aspx.cs" Inherits="Web_iTrekB_BAShowCntr" Title="Show Container" Theme="SATSTheme" %>

<%@ Register Src="WuBAFlight.ascx" TagName="WuBAFlight" TagPrefix="uc1" %>
<%@ Register Src="WuBATrip.ascx" TagName="WuBATrip" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cph1" Runat="Server">
    <uc1:WuBAFlight ID="WuBAFlight1" runat="server" />
    <hr />
<asp:Label ID="lblHead" CssClass="Heading" runat="server" Text="Show Containers for a flight"></asp:Label>
    <uc2:WuBATrip ID="WuBATrip1" runat="server" />
<asp:Label ID="lblMsg" runat="server" Text=""></asp:Label>    
</asp:Content>

