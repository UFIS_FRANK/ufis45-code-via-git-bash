using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;
using System.Text;

using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.Util;
using MM.UtilLib;

public partial class Web_iTrekB_WuBagArrContiner : System.Web.UI.UserControl
{
	EntBagArrContainer _entUld = null;

	protected override void OnLoad(EventArgs e)
	{
		base.OnLoad(e);
		if (Page.IsPostBack)
		{
			AddDynamicControlForUnRecvTrip();
			AddDynamicControlForRecvUld();
			AddDynamicControlForUnSentUld();
		}
	}

	protected override void OnPreRender(EventArgs e)
	{
		base.OnPreRender(e);
		AddJsHasCheck();
	}

	private const string VS_WUBACONT_VALIDATE_GROUP = "VS_WUBACONT_VALIDATE_GROUP";
	private string vsWuBaContValidateGroup = "";
	private bool hasWuBaContValidateGroup = false;
	public string ValidationGroup
	{
		get
		{
			if (!hasWuBaContValidateGroup)
			{
				vsWuBaContValidateGroup = "";
				try
				{
					vsWuBaContValidateGroup = (string)ViewState[VS_WUBACONT_VALIDATE_GROUP];
					hasWuBaContValidateGroup = true;
				}
				catch (Exception) { }
			}
			return vsWuBaContValidateGroup;
		}
		set
		{
			ViewState[VS_WUBACONT_VALIDATE_GROUP] = value;
			vsWuBaContValidateGroup = value;
		}
	}

	private void AddDynamicControl()
	{

		//ASP.usercontrols_wucheckboxlist_ascx wuCbl3;// = new ASP.usercontrols_wucheckboxlist_ascx();
		////pnlCbl.Controls.Add(wuCbl3);
		//wuCbl3 = (ASP.usercontrols_wucheckboxlist_ascx)LoadControl("UserControls/WuCheckBoxList.ascx");
		//wuCbl3.ID = "wuCbl3";
		////ph1.Controls.Add(wuCbl3);
	}

	/// <summary>
	/// Populate Data for the given flight with given 'Baggage Arrival Type'
	/// And select the trip and uld as given selected Uld and Trip Info
	/// </summary>
	/// <param name="flightUrno">Flight Id</param>
	/// <param name="bagArrType">Baggage Arrival Type</param>
	/// <param name="arrSelectedUldIds">Selected Ulds</param>
	/// <param name="arrSelectedTrip">Selected Trips</param>
	/// <param name="highlightExpectedContainer">True==>Highlight Expected Container.</param>
	public void PopulateData(string flightUrno, EnBagArrType bagArrType,
		List<string> arrSelectedUldIds, ArrayList arrSelectedTrip,
		bool highlightExpectedContainer, out bool hasInfoToShow)
	{
		hasInfoToShow = true;
		//LogTraceMsg("PopulateData:");
		_entUld = CtrlBagArrival.GetTripInfo(flightUrno, bagArrType);
		#region Show Trip sent by apron and Received/UnReceived by Baggage

		UnRecvTripCntInternal = _entUld.UnReceivedTripList.Count;
		//LogTraceMsg("PopulateData:UnRecvCnt:" + _entUld.UnReceivedTripList.Count);
		AddDynamicControlForUnRecvTrip();
		PopulateDataForUnRecvTrip(arrSelectedUldIds, arrSelectedTrip);

		RecvUldCnt = _entUld.ReceivedUldList.Count;
		//LogTraceMsg("PopulateData:RecvCnt:" + _entUld.ReceivedUldList.Count);
		AddDynamicControlForRecvUld();
		PopulateDataForRecvUld();
		#endregion

		#region UnSent Uld
		UnSentUldCnt = _entUld.UnSentUldList.Count;
		//LogTraceMsg("PopulateData:UnSentCnt:" + _entUld.UnSentUldList.Count);
		AddDynamicControlForUnSentUld();
		PopulateDataForUnSentUld(highlightExpectedContainer);
		#endregion
		//Hide, if nothing to show.
		if ((UnRecvTripCntInternal < 1) && (RecvUldCnt < 1) && (UnSentUldCnt < 1))
		{
			//LogTraceMsg("PopulateData:Hide.");
			this.Visible = false;
			hasInfoToShow = false;
		}
	}

	/// <summary>
	/// Get Selected Uld Ids (in trip) and Trip Nos for the flight
	/// </summary>
	/// <param name="arrSelectedTrip">Selected Trip Nos</param>
	/// <returns>Selected Uld Ids</returns>
	public List<string> GetSelectedData(out ArrayList arrSelectedTrip)
	{
		//ArrayList arr = new ArrayList();
		List<string> arr = new List<string>();
		arrSelectedTrip = new ArrayList();
		int cnt = this.phTrip.Controls.Count;
		bool isHeadSelected = false;
		string tripId = "";
		for (int i = 0; i < cnt; i++)
		{

			//ASP.web_itrekb_wubagarrunrecvuld_ascx wu = (ASP.web_itrekb_wubagarrunrecvuld_ascx)phTrip.Controls[i];
			Web_iTrekB_WuBagArrUnRecvUld wu = (Web_iTrekB_WuBagArrUnRecvUld)phTrip.Controls[i];
			List<string> arrTemp = wu.GetSelectedData(out isHeadSelected, out tripId);
			if ((isHeadSelected) && (!string.IsNullOrEmpty(tripId)))
				arrSelectedTrip.Add(tripId);
			if (arrTemp == null) continue;
			if (arrTemp.Count < 1) continue;
			arr.AddRange(arrTemp);
		}
		return arr;
	}


	#region UnReceivedTrip Info
	const string VS_UNRECV_CNT = "VS_UNRECV_CNT";
	private int _unRecvTripCnt = -1;

	/// <summary>
	/// Save the Received Trip Count in view state
	/// </summary>
	private int UnRecvTripCntInternal
	{
		get
		{
			if (_unRecvTripCnt < 0)
			{
				try
				{
					_unRecvTripCnt = (int)ViewState[VS_UNRECV_CNT];
				}
				catch (Exception)
				{
				}
			}
			return _unRecvTripCnt;
		}
		set
		{
			ViewState[VS_UNRECV_CNT] = value;
			_unRecvTripCnt = value;
		}
	}

	/// <summary>
	/// Number of ULDs sent by Apron but have not received by baggage
	/// </summary>
	public int UnRecvTripCnt
	{
		get { return UnRecvTripCntInternal; }
	}

	///// <summary>
	///// Number of ULDs sent by Apron but have not received by baggage
	///// </summary>
	//public int UnRecvTripCnt
	//{
	//    get { return UnRecvTripCntInternal; }
	//}

	/// <summary>
	/// Add/create the controls for UnReceived Trips Information
	/// Note: Update "UnRecvTripCnt" before calling this.
	/// Note2: Call this in Postback to get back the controls
	/// </summary>

	public string JS_HAS_CHK
	{
		get { return "HasCheck$F$" + this.UniqueID; }
	}

	private void AddDynamicControlForUnRecvTrip()
	{
		int cnt = UnRecvTripCnt;
		phTrip.Controls.Clear();
		//LogTraceMsg("AddCtrl:Cnt:" + cnt);
		for (int i = 0; i < cnt; i++)
		{
			//ASP.web_itrekb_wubagarrunrecvuld_ascx wu = new ASP.web_itrekb_wubagarrunrecvuld_ascx();
			//Web_iTrekB_WuBagArrUnRecvUld wu = new Web_iTrekB_WuBagArrUnRecvUld();
			Web_iTrekB_WuBagArrUnRecvUld wu = (Web_iTrekB_WuBagArrUnRecvUld)LoadControl("WuBagArrUnRecvUld.ascx");
			wu.EnableViewState = true;
			wu.ID = "WuC_WuUnRcv" + i;
			wu.Visible = true;
			phTrip.Controls.Add(wu);
		}
	}

	private void AddJsHasCheck()
	{
		StringBuilder sb = new StringBuilder();
		int cnt = this.phTrip.Controls.Count;
		string jsFName = JS_HAS_CHK;

		sb.Append("function " + jsFName + "()");
		sb.Append(@"{
  var has = false;
        ");
		for (int i = 0; i < cnt; i++)
		{
			//ASP.web_itrekb_wubagarrunrecvuld_ascx wu = (ASP.web_itrekb_wubagarrunrecvuld_ascx)phTrip.Controls[i];
			Web_iTrekB_WuBagArrUnRecvUld wu = (Web_iTrekB_WuBagArrUnRecvUld)phTrip.Controls[i];
			sb.Append(@" 
if (HasAnyCheckInCheckBoxList('" + wu.CheckListClientId + "')) return true;");
		}
		sb.Append(@"
  return has;
}");
		if (!Page.ClientScript.IsClientScriptBlockRegistered(jsFName))
		{
			Page.ClientScript.RegisterClientScriptBlock(this.GetType(),
				jsFName, sb.ToString(), true);
		}
	}


	/// <summary>
	/// Populate Data for UnReceived Trips Information.
	/// Note : Add the controls by calling "AddDynamicControlForUnRecvTripCnt", before calling this.
	/// </summary>
	private void PopulateDataForUnRecvTrip(List<string> arrSelectedUldIds, ArrayList arrSelectedTrip)
	{
		int idx = 0;
		int cnt = 0;
		Hashtable htSelectedTrip = UtilMisc.ConvArrayListToHashTable(arrSelectedTrip);
		//LogTraceMsg("PDataForUnRecvTrip:Cnt:" + phTrip.Controls.Count);
		foreach (Control ctl in phTrip.Controls)
		{
			//LogTraceMsg("PDataForUnRecvTrip:Cnt:" + (++cnt));
			//if (ctl is ASP.web_itrekb_wubagarrunrecvuld_ascx)
			if (ctl is Web_iTrekB_WuBagArrUnRecvUld)
			{
				try
				{
					//LogTraceMsg("PDataForUnRecvTrip:Cnt:PopulateData:" + cnt );
					//ASP.web_itrekb_wubagarrunrecvuld_ascx wu = (ASP.web_itrekb_wubagarrunrecvuld_ascx)ctl;

					Web_iTrekB_WuBagArrUnRecvUld wu = (Web_iTrekB_WuBagArrUnRecvUld)ctl;
					wu.ID = "WuC_WuUnRcv" + idx;
					wu.ValidationGroup = this.ValidationGroup;
					wu.PopulateData(_entUld.UnReceivedTripList[idx], arrSelectedUldIds, htSelectedTrip);
				}
				catch (Exception ex)
				{
					LogMsg("PDataForUnRecvTrip:Err:" + ex.Message);
				}
				idx++;
			}
		}
	}
	#endregion

	#region ReceivedUld Info
	const string VS_RECV_CNT = "VS_RECV_CNT";
	private int _recvUldCnt = -1;

	/// <summary>
	/// Save the Received Trip Count in view state
	/// </summary>
	private int RecvUldCnt
	{
		get
		{
			if (_recvUldCnt < 0)
			{
				try
				{
					_recvUldCnt = (int)ViewState[VS_RECV_CNT];
				}
				catch (Exception)
				{
				}
			}
			return _recvUldCnt;
		}
		set
		{
			ViewState[VS_RECV_CNT] = value;
			_recvUldCnt = value;
		}
	}

	/// <summary>
	/// Number of Received ULDs by Baggage
	/// </summary>
	public int RecvUldCntE
	{
		get { return RecvUldCnt; }
	}

	/// <summary>
	/// Add/create the controls for Received ULDs Information
	/// Note: Update "RecvUldCnt" before calling this.
	/// Note2: Call this in Postback to get back the controls
	/// </summary>
	private void AddDynamicControlForRecvUld()
	{
		int cnt = RecvUldCnt;
		phRecv.Controls.Clear();
		if (cnt > 0)
		{
			Label lblTmp = new Label();
			phRecv.Controls.Add(lblTmp);

			Literal ltlBr = new Literal();
			phRecv.Controls.Add(ltlBr);

			lbShowRecvCntr.Visible = true;
		}
		else
		{
			lbShowRecvCntr.Visible = false;
		}
	}

	/// <summary>
	/// Populate Data for Received ULDs Information.
	/// Note : Add the controls by calling "AddDynamicControlForRecvUld", before calling this.
	/// </summary>
	private void PopulateDataForRecvUld()
	{
		string st = _entUld.ReceivedUldList.UldListString;
		if (st != "")
		{
			foreach (Control ctl in phRecv.Controls)
			{
				if (ctl is Label)
				{
					try
					{
						Label lblRecvUld = (Label)ctl;
						lblRecvUld.Text = " " + st;
					}
					catch (Exception)
					{
					}
				}
				else if (ctl is Literal)
				{
					Literal ltlBr = (Literal)ctl;
					ltlBr.Text = "<br />";
				}
			}
		}
	}
	#endregion

	#region Unsent Uld Info
	const string VS_WU_BAC_UNSENT_CNT = "VS_WU_BAC_UNSENT_CNT";
	private int _unSentCnt = -1;

	/// <summary>
	/// Save the UnSent ULD Count in view state
	/// </summary>
	private int UnSentUldCnt
	{
		get
		{
			if (_unSentCnt < 0)
			{
				try
				{
					_unSentCnt = (int)ViewState[VS_WU_BAC_UNSENT_CNT];
				}
				catch (Exception)
				{
				}
			}
			return _unSentCnt;
		}
		set
		{
			ViewState[VS_WU_BAC_UNSENT_CNT] = value;
			_unSentCnt = value;
		}
	}

	/// <summary>
	/// Add/create the controls for UnSent ULDs Information
	/// Note: Update "UnSentUldCnt" before calling this.
	/// Note2: Call this in Postback to get back the controls
	/// </summary>
	private void AddDynamicControlForUnSentUld()
	{
		int cnt = UnSentUldCnt;
		phExp.Controls.Clear();
		if (cnt > 0)
		{
			Label lblTmp = new Label();
			phExp.Controls.Add(lblTmp);
			Literal ltlBr = new Literal();
			phExp.Controls.Add(ltlBr);
		}
	}

	//private bool NeedToHighlightUnsentUld()
	//{
	//    bool toHighlight = false;
	//    //toHighlight = true;//TO DO - To get whether to highlight the Unsent Ulds
	//    CtrlBagArrival.HighlightExpectedContainers( fli
	//    return toHighlight;
	//}

	/// <summary>
	/// Populate Data for UnSent ULDs Information.
	/// Note : Add the controls by calling "AddDynamicControlForUnSentUld", before calling this.
	/// </summary>
	private void PopulateDataForUnSentUld(bool highlightExpectedContainer)
	{
		string st = _entUld.UnSentUldList.UldListString;
		if (st != "")
		{
			foreach (Control ctl in phExp.Controls)
			{
				if (ctl is Label)
				{
					try
					{
						Label lbl = (Label)ctl;

						lbl.Text = st;
						if (highlightExpectedContainer)
						{
							lbl.Attributes.Add("class", "Highlight");
						}
					}
					catch (Exception)
					{
					}
				}
				else if (ctl is Literal)
				{
					Literal ltlBr = (Literal)ctl;
					ltlBr.Text = "<br />";
				}
			}
		}
	}
	#endregion

	private bool _enableUldSelect = true;
	private bool _hasRetrievedEnableUldSelect = false;
	private const string VS_WUBACONTAINER_ENABLEULDSELECT = "VS_WUBACONTAINER_ENABLEULDSELECT";
	private bool EnableUldSelectInternal
	{
		get
		{
			if (_hasRetrievedEnableUldSelect)
			{
				try
				{
					_enableUldSelect = (bool)ViewState[VS_WUBACONTAINER_ENABLEULDSELECT];
					_hasRetrievedEnableUldSelect = true;
				}
				catch (Exception)
				{
				}
			}
			return _enableUldSelect;
		}
		set
		{
			_enableUldSelect = value;
			ViewState[VS_WUBACONTAINER_ENABLEULDSELECT] = _enableUldSelect;
			_hasRetrievedEnableUldSelect = true;
		}
	}

	public bool EnableUldSelect
	{
		get
		{
			return EnableUldSelectInternal;
		}

		set
		{
			EnableUldSelectInternal = value;
			bool enable = value;
			foreach (Control c in phTrip.Controls)
			{
				//if (c is ASP.web_itrekb_wubagarrunrecvuld_ascx)
				if (c is Web_iTrekB_WuBagArrUnRecvUld)
				{
					//ASP.web_itrekb_wubagarrunrecvuld_ascx wu = (ASP.web_itrekb_wubagarrunrecvuld_ascx)c;
					Web_iTrekB_WuBagArrUnRecvUld wu = (Web_iTrekB_WuBagArrUnRecvUld)c;
					wu.EnableUldSelect = enable;
				}
			}
		}
	}


	public delegate void ShowReceivedCntrClickHandler(object sender);
	public event ShowReceivedCntrClickHandler OnShowReceivedCntrClick;
	protected void lbShowRecvCntr_Click(object sender, EventArgs e)
	{
		ShowReceivedCntrClickHandler hdl = OnShowReceivedCntrClick;
		if (hdl != null)
		{
			hdl(this);
		}
	}

	private static void LogMsg(string msg)
	{
		try
		{
			MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("WuBagArrCntr", msg);
		}
		catch (Exception)
		{
			UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("WuBagArrCntr", msg);
		}
	}

	private static void LogTraceMsg(string msg)
	{
		LogMsg(msg);
	}
}