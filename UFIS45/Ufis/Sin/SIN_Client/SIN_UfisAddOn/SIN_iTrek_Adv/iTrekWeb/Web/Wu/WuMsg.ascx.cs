using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;

using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.Util;
using UFIS.ITrekLib.ENT;
using UFIS.Web.ITrek.Sec;
using MM.UtilLib;
using UFIS.Web.ITrek.WebMgr;

using UFIS.Web.ITrek.Base;

public partial class Web_Wu_WuMsg : UserControl
{
   #region User Authentication Handling
   private EntUser _user = null;

   public EntUser LoginedUser
   {
      get
      {
         if (_user == null)
         {
            try
            {
               CtrlSec ctrlsec = CtrlSec.GetInstance();
               _user = ctrlsec.AuthenticateUser(Session, Response);
            }
            catch (Exception)
            {
            }
         }
         return _user;
      }
   }

   public string LoginedId
   {
      get
      {
         //string id = "";
         //id = LoginedUser.UserId;
         //return id;
         return LoginedUser.UserId;
      }
   }

   private List<string> _grList = null;
   public List<string> LoginedUserGroupList
   {
      get
      {
         if (_grList == null)
         {
            try
            {
               _grList = UtilMisc.ConvArrayListToStringList(LoginedUser.GetGroupList());
            }
            catch (Exception)
            {
            }
         }
         return _grList;
      }
   }

   public virtual void DoUnAuthorise()
   {
      Response.Clear();
      Response.Redirect("UnAuthorised.aspx", true);
   }

   #endregion

   const string VS_MSG_ADDH = "VS_MSG_ADDH";
   private int _addHeight = 0;
   public int AdditionalHeight
   {
      get
      {
         if (_addHeight <= 0)
         {
            try
            {
               _addHeight = (int)ViewState[VS_MSG_ADDH];
            }
            catch (Exception)
            {
               ViewState[VS_MSG_ADDH] = 0;
            }
         }
         return _addHeight;
      }
      set
      {
         if (value < 0) value = 0;
         _addHeight = value;
         ViewState[VS_MSG_ADDH] = _addHeight;
         LogMsg("AdditionalHeight:" + _addHeight);
      }
   }

   void WuMsgCompose1_OnRefreshTime(object sender)
   {
      //throw new Exception("The method or operation is not implemented.");
      WuMsgInBox1.Refresh();
      WuMsgOutBox1.Refresh();
   }

   void WUMsgDet1_CloseClick(object sender, UFIS.ITrekLib.EventArg.EaTextEventArgs e)
   {
      WUMsgDet1.Visible = false;
   }

   void WUMsgDet1_ReadClick(object sender, UFIS.ITrekLib.EventArg.EaTextEventArgs e)
   {
      string msgId = e.Text;
      string staffId = "";
      string recvUfisDt = "";
      staffId = LoginedUser.UserId;
      recvUfisDt = UtilTime.ConvDateTimeToUfisFullDTString(System.DateTime.Now);
      CtrlMsg.UpdateMessageReceivedDetails(msgId, staffId, recvUfisDt);
      WUMsgDet1.Visible = false;
   }

   void WuMsgInBox1_OnClickShowMessage(object sender, string msgId)
   {
      WUMsgDet1.ShowMsg(msgId, "I");
   }

   void WuMsgOutBox1_OnClickShowMessage(object sender, string msgId)
   {
      WUMsgDet1.ShowMsg(msgId, "O");
   }

   protected override void OnInit(EventArgs e)
   {
      base.OnInit(e);
      WuMsgOutBox1.OnClickShowMessage += new Web_Wu_WuMsgOutBox.ShowMessageHandler(WuMsgOutBox1_OnClickShowMessage);
      WuMsgInBox1.OnClickShowMessage += new Web_Wu_WuMsgInBox.ShowMessageHandler(WuMsgInBox1_OnClickShowMessage);
      WUMsgDet1.ReadClick += new Web_iTrek_WUMsgDet.ReadClickHandler(WUMsgDet1_ReadClick);
      WUMsgDet1.CloseClick += new Web_iTrek_WUMsgDet.CloseClickHandler(WUMsgDet1_CloseClick);
      WuMsgCompose1.OnRefreshTime += new Web_Wu_WuMsgCompose.RefreshTimerHandler(WuMsgCompose1_OnRefreshTime);
   }

   protected override void OnLoad(EventArgs e)
   {
      base.OnLoad(e);
      if ((!Page.IsPostBack) && (!Page.IsCallback))
      {
         //PopulateData();
         WUMsgDet1.Visible = false;
      }
   }

   protected override void OnPreRender(EventArgs e)
   {
      base.OnPreRender(e);
      WuMsgCompose1.AdditionalHeight = this.AdditionalHeight;
      int diff = 0;
      int bh = MgrSess.GetBrowserheight(Session);
      if (bh > 768) diff = bh - 768;
      if (diff < 0) diff = 0;
      LogMsg(string.Format("SetSendToHeight:bh<{0}>,diff<{1}>,add<{2}>", bh, diff, this.AdditionalHeight));
      Unit uHeight = new Unit(string.Format("{0}px", (300 + AdditionalHeight + diff)));
      WuMsgInBox1.Height = uHeight;
      WuMsgOutBox1.Height = uHeight;
   }

   ///// <summary>
   ///// Populate the message Inbox, Outbox and Receipients
   ///// </summary>
   ///// <param name="userId">User Id Mandatory</param>
   ///// <param name="arrGroupList">Group Id Array for which this user belongs to. Applicable for Normal Message Type. Not applicable for Flight Message Type</param>
   ///// <param name="flightId">Flight Id. Applicable for Flight Message Type only.</param>
   ///// <param name="msgType">Message Type to Populate the message.</param>
   //public void PopulateData(string userId, List<string> arrGroupList, string flightId, EnComposeMsgType msgType)
   //{
   //    if (string.IsNullOrEmpty(flightId))
   //    {
   //        WuMsgCompose1.ComposeMessageNormal();
   //        WuMsgInBox1.PopulateData(userId, arrGroupList, MAX_MSG_CNT, msgType);
   //    }
   //    else
   //    {
   //        WuMsgCompose1.ComposeMessageForFlight(flightId);
   //        WuMsgInBox1.PopulateData(flightId, null, MAX_MSG_CNT, msgType);
   //    }
   //    WuMsgOutBox1.PopulateData(userId, MAX_MSG_CNT);        
   //}

   private const int MAX_MSG_CNT = 50;

   public void PopulateData(EnComposeMsgType msgType, string flightId )
   {
      //LogMsg("PopulateData:" + flightId + "," + msgType);
      string userId = LoginedId;
      List<string> userGroupList = LoginedUserGroupList;

      switch (msgType)
      {
         case EnComposeMsgType.Personal:
            WuMsgCompose1.ComposeMessageNormal();
            WuMsgInBox1.PopulateData(userId, userGroupList, MAX_MSG_CNT, msgType);
            break;
         case EnComposeMsgType.Flight:
            if (string.IsNullOrEmpty(flightId)) throw new ApplicationException("No Flight Id.");
            WuMsgCompose1.ComposeMessageForFlight(flightId);
            WuMsgInBox1.PopulateData(flightId, null, MAX_MSG_CNT, msgType);
            break;
         case EnComposeMsgType.Group:
            WuMsgCompose1.ComposeMessageNormal();
            WuMsgInBox1.PopulateData(userId, userGroupList, MAX_MSG_CNT, msgType);
            break;
         case EnComposeMsgType.Undefined:
            throw new ApplicationException("Invalid Message Type.");
         default:
            throw new ApplicationException("Unknown Message Type.");
      }

      WuMsgOutBox1.PopulateData(userId, MAX_MSG_CNT);
   }

   private void LogMsg(string msg)
   {
      try
      {
         MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("WuMsg", msg);
      }
      catch (Exception)
      {
         UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("WuMsg", msg);
      }
   }
}