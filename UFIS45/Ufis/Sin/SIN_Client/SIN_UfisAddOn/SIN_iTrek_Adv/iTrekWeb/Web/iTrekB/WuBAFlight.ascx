<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WuBAFlight.ascx.cs" Inherits="Web_iTrekB_WuBAFlight" %>
<%@ Register Src="WuBagArrContiner.ascx" TagName="WuBagArrContiner" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<div style="position: relative; left: 1px;width:99%;" class="HeadingBar">
<table style="width: 100%;">
    <tr>
        <td style="width: 300px">
        <asp:Panel ID="pnlHead" runat="server">
        <asp:ImageButton ID="ibExpCol" runat="server" ImageUrl="~/Images/collapse.jpg" AlternateText="Show Detail" CausesValidation="False" />
            <asp:Label ID="lblFInfo" runat="server" Text=""></asp:Label>
        </asp:Panel>
        </td>
        <td style="width: 50px">
            <asp:LinkButton ID="lbRecv" runat="server" ValidationGroup="VGTest1" OnClick="lbRecv_Click" EnableViewState="False">RECV</asp:LinkButton></td>
        <td style="width: 80px">
            &nbsp;<asp:LinkButton ID="lbFBag" runat="server" OnClick="lbFBag_Click">FIRST BAG</asp:LinkButton></td>
        <td style="width: 80px">
            &nbsp;<asp:LinkButton ID="lbLBag" runat="server" OnClick="lbLBag_Click">LAST BAG</asp:LinkButton></td>
        <td style="width: 90px">
            &nbsp;<asp:LinkButton ID="lbShowCtr" runat="server" OnClick="lbShowCtr_Click">SHOW CNTR</asp:LinkButton></td>
        <td style="width: 80px">
            &nbsp;<asp:LinkButton ID="lbFlMsg" runat="server" OnClick="lbFlMsg_Click">MSG (0)</asp:LinkButton></td>
        <td>
        </td>                        
    </tr>
</table>
</div>
<asp:Panel ID="pnlDet" runat="server" style="position: relative; left: 8px;" CssClass="cpBody">
<uc1:WuBagArrContiner ID="wuBagArrContiner1" runat="server" ValidationGroup="VGTest1" EnableViewState="true" Visible="true" />
</asp:Panel>
<cc1:CollapsiblePanelExtender ID="cpeFlight" runat="server" 
    TargetControlID="pnlDet"
    ExpandControlID="pnlHead"
    CollapseControlID="pnlHead" 
    Collapsed="False"
    TextLabelID=""
    ImageControlID="ibExpCol"    
    ExpandedText="(Hide Details...)"
    CollapsedText="(Show Details...)"
    ExpandedImage="~/images/collapse.jpg"
    CollapsedImage="~/images/expand.jpg"
    CollapsedSize="0"
    SuppressPostBack="true"
/>
<asp:Label ID="lblErrMsg" runat="server" ForeColor="Red"></asp:Label>
<asp:HiddenField ID="hfCpeStat" runat="server" OnValueChanged="hfCpeStat_ValueChanged" Value="0"/>

