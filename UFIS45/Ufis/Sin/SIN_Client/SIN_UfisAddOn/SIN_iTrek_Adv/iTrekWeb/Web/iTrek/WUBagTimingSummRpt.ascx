<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WUBagTimingSummRpt.ascx.cs" Inherits="Web_iTrek_WUBagTimingSummRpt" %>
    <asp:Panel ID="pnl1" runat="server" Height="357px" Width="731px" ScrollBars="Auto" Visible="False">
    <div>
    <table>
        <tr><td colspan="6" class="Heading">
        Baggage Presentation Timing Summary Report
        </td>
        </tr>
        <tr>
            <td style="width: 100px">
                Terminal :</td>
            <td style="width: 100px">
                <asp:Label ID="lblTerminal" runat="server" Width="182px"></asp:Label></td>
            <td style="width: 15px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
            </td>
            <td style="width: 40px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
                Date From :</td>
            <td style="width: 100px">
                <asp:Label ID="lblFrDate" runat="server" Width="181px"></asp:Label></td>
            <td style="width: 15px">
            </td>
            <td style="width: 100px">
                Time From :</td>
            <td style="width: 100px">
                <asp:Label ID="lblFrTime" runat="server" Width="164px"></asp:Label></td>
            <td style="width: 40px">
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
                Date To :</td>
            <td style="width: 100px">
                <asp:Label ID="lblToDate" runat="server" Width="180px"></asp:Label></td>
            <td style="width: 15px">
            </td>
            <td style="width: 100px">
                Time To :</td>
            <td style="width: 100px">
                <asp:Label ID="lblToTime" runat="server" Width="160px"></asp:Label></td>
            <td style="width: 40px">
            </td>
        </tr>
    </table>
</div>
<div>
<asp:GridView ID="gvReport" runat="server" BackColor="White" BorderColor="#999999"
    BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black" GridLines="Vertical" Width="627px" OnPageIndexChanging="gvReport_PageIndexChanging">
    <FooterStyle BackColor="#CCCCCC" />
    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
    <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
    <AlternatingRowStyle BackColor="#CCCCCC" />
</asp:GridView>
</div>

    </asp:Panel>
