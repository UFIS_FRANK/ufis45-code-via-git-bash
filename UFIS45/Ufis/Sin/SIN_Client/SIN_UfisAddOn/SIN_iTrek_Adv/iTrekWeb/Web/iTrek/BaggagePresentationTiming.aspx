<%@ Page Language="C#" MasterPageFile="~/Web/iTrek/MasterPage.master" AutoEventWireup="true" CodeFile="BaggagePresentationTiming.aspx.cs" Inherits="Web_iTrek_BaggagePresentationTiming" Title="Baggage Presentation Timing" Theme="SATSTheme" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" Runat="Server">
<div style="width:100%;" class="Heading">
    Baggage Presentation Timing Report<br />
</div>
<div style="width:100%;">
    <table style="width: 980px">
        <tr>
            <td>
            </td>
        </tr>
        <tr>
            <td style="width: 980px; height: 195px;">
                <table style="width: 980px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;" cellspacing="1" border="1" cellpadding="1">
                    <tr>
                        <td class="SubHeading" style="width: 12%">
                            DATE</td>
                        <td class="SubHeading" style="width: 12%">
                            FLIGHT NUMBER</td>
                        <td class="SubHeading" style="width: 12%">
                            BO</td>
                        <td class="SubHeading" style="width: 12%">
                            AIRCRAFT TYPE</td>
                        <td class="SubHeading" style="width: 12%">
                            BELT</td>
                        <td class="SubHeading" style="width: 12%">
                            PARKING STAND</td>
                        <td class="SubHeading" style="width: 12%">
                            ATA</td>
                        <td class="SubHeading" style="width: 12%">
                            AO</td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblDate" runat="server"></asp:Label>&nbsp;</td>
                        <td>
                            <asp:Label ID="lblFlNo" runat="server"></asp:Label>&nbsp;</td>
                        <td>
                            <asp:Label ID="lblBO" runat="server"></asp:Label>&nbsp;</td>
                        <td>
                            <asp:Label ID="lblAcftType" runat="server"></asp:Label>&nbsp;</td>
                        <td>
                            <asp:Label ID="lblBelt" runat="server"></asp:Label>&nbsp;</td>
                        <td>
                            <asp:Label ID="lblParking" runat="server"></asp:Label>&nbsp;</td>
                        <td>
                            <asp:Label ID="lblATA" runat="server"></asp:Label>&nbsp;</td>
                        <td>
                            <asp:Label ID="lblAO" runat="server"></asp:Label>&nbsp;</td>
                    </tr>
                </table>
                <table style="width: 980px; border-top-style: none; border-right-style: none; border-left-style: none; border-bottom-style: none;" cellspacing="1" border="1" cellpadding="1">
                    <tr>
                        <td class="SubHeading" style="width: 200px">
                            CONTAINER</td>
                        <td class="SubHeading" >
                            1</td>
                        <td class="SubHeading" >
                            2</td>
                        <td class="SubHeading">
                            3</td>
                        <td class="SubHeading">
                            4</td>
                        <td class="SubHeading">
                            5</td>
                        <td class="SubHeading">
                            6</td>
                        <td class="SubHeading">
                            7</td>
                        <td class="SubHeading">
                            8</td>
                        <td class="SubHeading">
                            9</td>
                        <td class="SubHeading">
                            10</td>
                    </tr>
                    <tr>
                        <td class="SubHeading" style="width: 200px">
                            Time Arrived:</td>
                        <td>
                            <asp:Label ID="lblCT1" runat="server"></asp:Label>&nbsp;</td>
                        <td>
                            &nbsp;<asp:Label ID="lblCT2" runat="server"></asp:Label>&nbsp;</td>
                        <td>
                            <asp:Label ID="lblCT3" runat="server"></asp:Label>&nbsp;</td>
                        <td>
                            <asp:Label ID="lblCT4" runat="server"></asp:Label>&nbsp;</td>
                        <td>
                            &nbsp;<asp:Label ID="lblCT5" runat="server"></asp:Label></td>
                        <td>
                            &nbsp;<asp:Label ID="lblCT6" runat="server"></asp:Label></td>
                        <td>
                            &nbsp;<asp:Label ID="lblCT7" runat="server"></asp:Label></td>
                        <td>
                            &nbsp;<asp:Label ID="lblCT8" runat="server"></asp:Label></td>
                        <td>
                            &nbsp;<asp:Label ID="lblCT9" runat="server"></asp:Label></td>
                        <td>
                            &nbsp;<asp:Label ID="lblCT10" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td class="SubHeading" style="width: 200px">
                            Number:</td>
                        <td>&nbsp;<asp:Label ID="lblCN1" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;<asp:Label ID="lblCN2" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;<asp:Label ID="lblCN3" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;<asp:Label ID="lblCN4" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;<asp:Label ID="lblCN5" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;<asp:Label ID="lblCN6" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;<asp:Label ID="lblCN7" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;<asp:Label ID="lblCN8" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;<asp:Label ID="lblCN9" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;<asp:Label ID="lblCN10" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="SubHeading" style="width: 200px">
                            Class:</td>
                        <td>&nbsp;<asp:Label ID="lblCC1" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;<asp:Label ID="lblCC2" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;<asp:Label ID="lblCC3" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;<asp:Label ID="lblCC4" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;<asp:Label ID="lblCC5" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;<asp:Label ID="lblCC6" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;<asp:Label ID="lblCC7" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;<asp:Label ID="lblCC8" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;<asp:Label ID="lblCC9" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;<asp:Label ID="lblCC10" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="SubHeading" style="width: 200px">
                            CONTAINER</td>
                        <td class="SubHeading">
                            11</td>
                        <td class="SubHeading">
                            12</td>
                        <td class="SubHeading">
                            13</td>
                        <td class="SubHeading">
                            14</td>
                        <td class="SubHeading">
                            15</td>
                        <td class="SubHeading">
                            16</td>
                        <td class="SubHeading">
                            17</td>
                        <td class="SubHeading">
                            18</td>
                        <td class="SubHeading">
                            19</td>
                        <td class="SubHeading">
                            20</td>
                    </tr>
                    <tr>
                        <td class="SubHeading" style="width: 200px; height: 19px;">
                            Time Arrived:</td>
                        <td style="height: 19px">&nbsp;<asp:Label ID="lblCT11" runat="server"></asp:Label>
                        </td>
                        <td style="height: 19px">&nbsp;<asp:Label ID="lblCT12" runat="server"></asp:Label>
                        </td>
                        <td style="height: 19px">&nbsp;<asp:Label ID="lblCT13" runat="server"></asp:Label>
                        </td>
                        <td style="height: 19px">&nbsp;<asp:Label ID="lblCT14" runat="server"></asp:Label>
                        </td>
                        <td style="height: 19px">&nbsp;<asp:Label ID="lblCT15" runat="server"></asp:Label>
                        </td>
                        <td style="height: 19px">&nbsp;<asp:Label ID="lblCT16" runat="server"></asp:Label>
                        </td>
                        <td style="height: 19px">&nbsp;<asp:Label ID="lblCT17" runat="server"></asp:Label>
                        </td>
                        <td style="height: 19px">&nbsp;<asp:Label ID="lblCT18" runat="server"></asp:Label>
                        </td>
                        <td style="height: 19px">&nbsp;<asp:Label ID="lblCT19" runat="server"></asp:Label>
                        </td>
                        <td style="height: 19px">&nbsp;<asp:Label ID="lblCT20" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="SubHeading" style="width: 200px; height: 33px;">
                            Number:</td>
                        <td style="height: 33px">&nbsp;<asp:Label ID="lblCN11" runat="server"></asp:Label>
                        </td>
                        <td style="height: 33px">&nbsp;<asp:Label ID="lblCN12" runat="server"></asp:Label>
                        </td>
                        <td style="height: 33px">&nbsp;<asp:Label ID="lblCN13" runat="server"></asp:Label>
                        </td>
                        <td style="height: 33px">&nbsp;<asp:Label ID="lblCN14" runat="server"></asp:Label>
                        </td>
                        <td style="height: 33px">&nbsp;<asp:Label ID="lblCN15" runat="server"></asp:Label>
                        </td>
                        <td style="height: 33px">&nbsp;<asp:Label ID="lblCN16" runat="server"></asp:Label>
                        </td>
                        <td style="height: 33px">&nbsp;<asp:Label ID="lblCN17" runat="server"></asp:Label>
                        </td>
                        <td style="height: 33px">&nbsp;<asp:Label ID="lblCN18" runat="server"></asp:Label>
                        </td>
                        <td style="height: 33px">&nbsp;<asp:Label ID="lblCN19" runat="server"></asp:Label>
                        </td>
                        <td style="height: 33px">&nbsp;<asp:Label ID="lblCN20" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="SubHeading" style="width: 200px">
                            Class:</td>
                        <td>&nbsp;<asp:Label ID="lblCC11" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;<asp:Label ID="lblCC12" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;<asp:Label ID="lblCC13" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;<asp:Label ID="lblCC14" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;<asp:Label ID="lblCC15" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;<asp:Label ID="lblCC16" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;<asp:Label ID="lblCC17" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;<asp:Label ID="lblCC18" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;<asp:Label ID="lblCC19" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;<asp:Label ID="lblCC20" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table style="width: 980px; border-top-style: solid; border-right-style: solid; border-left-style: solid; border-bottom-style: solid;" cellspacing="1" border="1" cellpadding="1">
                    <tr>
                        <td class="SubHeading" style="width: 200px">
                            TIMINGS</td>
                        <td  class="SubHeading">
                            FIRST TRIP</td>
                        <td  class="SubHeading">
                            LAST TRIP</td>
                        <td  class="SubHeading">
                            FIRST BAG</td>
                        <td  class="SubHeading">
                            LAST BAG</td>
                        <td  class="SubHeading">
                            FIRST BAG TIMING MET</td>
                        <td  class="SubHeading" rowspan="1">
                            LAST BAG TIMING MET</td>
                    </tr>
                    <tr>
                        <td class="SubHeading" style="width: 200px">
                            Time:</td>
                        <td>&nbsp;<asp:Label ID="lblFstTrp" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;<asp:Label ID="lblLstTrp" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;<asp:Label ID="lblFstBag" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;<asp:Label ID="lblLstBag" runat="server"></asp:Label>
                        </td>
                        <td  rowspan="2">&nbsp;<asp:Label ID="lblFBBM" runat="server"></asp:Label>
                        </td>
                        <td rowspan="2">&nbsp;<asp:Label ID="lblLBBM" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="SubHeading" style="width: 200px">
                            Time from TBS UP:</td>
                        <td>&nbsp;<asp:Label ID="lblTbsUp1" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;<asp:Label ID="lblTbsUp2" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;<asp:Label ID="lblTbsUp3" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;<asp:Label ID="lblTbsUp4" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
                                <table style="width: 980px; border-top-style: solid; border-right-style: solid; border-left-style: solid; border-bottom-style: solid;" cellspacing="1" border="1" cellpadding="1">
                    <tr>
                        <td style="width: 200px" class="SubHeading">
                            AO ULD/TRIP REMARKS</td>
                        <td style="width: 200px" class="SubHeading">
                            AO TIMING REMARKS</td>
                        <td style="width: 200px" class="SubHeading">
                            BO REMARKS</td>
                    </tr>
                    <tr>
                        <td style="height: 50px">
                            <asp:TextBox ID="txtAOUldRmk" runat="server" Height="56px" ReadOnly="True" TextMode="MultiLine"
                                Width="318px"></asp:TextBox></td>
                        <td style="height: 50px">
                            <asp:TextBox ID="txtAOTimeRmk" runat="server" Height="56px" ReadOnly="True" TextMode="MultiLine"
                                Width="318px"></asp:TextBox></td>
                        <td style="height: 50px">
                            <asp:TextBox ID="txtBORmk" runat="server" Height="56px" TextMode="MultiLine" Width="318px"></asp:TextBox></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table style="width: 600px">
        <tr>
            <td style="width: 100px">
            </td>
            <td style="width: 100px">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" Visible="False" OnClick="btnSubmit_Click" EnableViewState="False" /></td>
            <td style="width: 100px">
                <asp:Button ID="btnCancel" runat="server" CausesValidation="False" Text="Cancel" EnableViewState="False" OnClick="btnCancel_Click" /></td>
            <td style="width: 100px">
                <asp:Button ID="btnPrint" OnClientClick="PrintRpt();" runat="server" CausesValidation="False" Text="Print" Visible="False"
                    Width="71px" EnableViewState="False" /></td>
            <td style="width: 100px">
            </td>
        </tr>
    </table>
</div>
<div style="width:100%;">
</div>
</asp:Content>

