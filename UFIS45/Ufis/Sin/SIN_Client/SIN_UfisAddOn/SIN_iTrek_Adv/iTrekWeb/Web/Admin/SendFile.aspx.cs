using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;

using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.DB;
using UFIS.ITrekLib.ENT;
using iTrekSessionMgr;
using UFIS.ITrekLib.Util;
using iTrekXML;
using UFIS.Web.ITrek.WebMgr;

using MMSoftware.MMWebUtil;

public partial class Web_Admin_SendFile : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //WebUtil.AlertMsg(Page, "VALIDa:" + UFIS.Web.ITrek.WebMgr.MgrSess.IsValidUserToGetLog(Session).ToString());
        lblMsg.Text = "";
        if (!MgrSess.IsValidUserToGetLog(Session))
        {
            UnAuthorise();
        }
        else
        {
            string fileName = "";
            string subFolderName = "";
            try
            {
                fileName = Request.Params["fn"];
                subFolderName = Request.Params["subDir"];
                SendFileToBrowser(subFolderName, fileName);
            }
            catch (Exception)
            {
            }
        }
    }

    private void SendFileToBrowser(string subFolderName, string fileName)
    {
        if (fileName == "")
        {
            lblMsg.Text = "No File to Get";
        }
        else
        {
            iTXMLPathscl paths = new iTXMLPathscl();
            string _logDir = paths.GetLogDirPath();
            string fullFileName = (_logDir + subFolderName + fileName);
            //Write the file directly to the HTTP content output stream.
            if (File.Exists(fullFileName))
            {
                Response.Clear();
                Response.Charset = "UTF-8";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
                //Set the appropriate ContentType.
                Response.ContentType = "application/octet-stream";
                //        NOTE: If you want to use the preceding code to support other binary file types, 
                //you must modify the value in the ContentType string so that it specifies the appropriate file format. 
                //The syntax of this string is usually formatted as "type/subtype," 
                //where "type" is the general content category and 
                //"subtype" is the specific content type. 
                //For a full list of supported content types, 
                //refer to your Web browser documentation or 
                //the current HTTP specification. 
                //The following list outlines some common ContentType values: 
                //� "text/HTML" 
                //� "image/GIF" 
                //� "image/JPEG" 
                //� "text/plain" 
                //� "Application/msword" (for Microsoft Word files) 
                //� "Application/x-msexcel" (for Microsoft Excel files) 
                //� "Application/pdf";
                //� "application/octet-stream"

                //Get the physical path to the file.
                //string FilePath = MapPath(_logDir + "Log_0503.txt");
                Response.WriteFile(fullFileName);
                Response.Flush();

                Response.End();
            }
            else
            {
                lblMsg.Text = "File Not Found.";
            }
        }
    }

    private void UnAuthorise()
    {
        lblMsg.Text = "You are not authorised.";
    }
}
