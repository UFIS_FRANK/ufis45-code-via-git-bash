<%@ Page Language="C#" MasterPageFile="~/Web/iTrekB/Master.master" AutoEventWireup="true" CodeFile="BATimeEntry.aspx.cs" Inherits="Web_iTrekB_BATimeEntry" Title="Timing Entry" Theme="SATSTheme" %>

<%@ Register Src="WuBAFlight.ascx" TagName="WuBAFlight" TagPrefix="uc2" %>

<%@ Register Src="../Wu/WuTime.ascx" TagName="WuTime" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cph1" Runat="Server">
    <div style="width: 99%; height:auto">
        <uc2:WuBAFlight ID="wuBAFlight1" runat="server" />
    </div>
    <hr />
    <div class="Heading">
    <asp:Label ID="lblHead" runat="server" CssClass="Heading"></asp:Label>
    </div>
    <div>
    <uc1:WuTime ID="wuTime1" runat="server" />
    <asp:UpdatePanel id="upnl1" runat="server">
        <contenttemplate>
<asp:Label id="lblMsg" runat="server" Width="131px" ForeColor="Red" Font-Bold="True" Visible="False"></asp:Label> 
</contenttemplate>
    </asp:UpdatePanel>
    </div>
    <br />
    
</asp:Content>

