using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;

using UFIS.Web.ITrek.Sec;
using UFIS.Web.ITrek.Base;

//public partial class Web_iTrek_Admin_EditConfig : System.Web.UI.Page
public partial class Web_iTrek_Admin_EditConfig : BaseWebPage
{
   protected override void OnLoad(EventArgs e)
   {
      base.OnLoad(e);
      ((MyMasterPage)Master).AutoRefreshMode = false;

      if (CtrlSec.GetInstance().HasAccessToAdminTerminalBeltSetup(LoginedUser))
      {
         if (!IsPostBack)
         {
            ShowInfo();
         }
      }
      else
      {
         DoUnAuthorise();
      }
   }

   public override void DoUnAuthorise()
   {
      //base.DoUnAuthorise();
      WuUnAuthorise1.Visible = true;
      gvConfig.Visible = false;
   }

   private void ShowInfo()
   {
      WuUnAuthorise1.Visible = false;
      //DSConfig ds = CtrlConfig.RetrieveAllConfig();
      DSConfig ds = CtrlConfig.RetrieveBAConfig();
      DataView dv = ds.CONFIG.DefaultView;
      dv.Sort = "ID_DESC";
      gvConfig.DataSource = dv;
      gvConfig.DataBind();
      gvConfig.Visible = true;
   }

   protected void gvConfig_RowCommand(object sender, GridViewCommandEventArgs e)
   {
      //string cmdName = e.CommandName.ToString();
      //string arg = e.CommandArgument.ToString();
      //string src = e.CommandSource.ToString();
   }
   protected void gvConfig_RowEditing(object sender, GridViewEditEventArgs e)
   {
      //int idx = e.NewEditIndex;
      gvConfig.EditIndex = e.NewEditIndex;
      ShowInfo();
   }
   protected void gvConfig_RowUpdating(object sender, GridViewUpdateEventArgs e)
   {
      int idx = e.RowIndex;
      string id = gvConfig.DataKeys[idx].Value.ToString();
      string value = ((TextBox)(gvConfig.Rows[idx].FindControl("txtValue"))).Text;

      CtrlConfig.UpdateConfigValueFroWeb(id, value, LoginedUser);
      gvConfig.EditIndex = -1;
      ShowInfo();

   }
   protected void gvConfig_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
   {
      gvConfig.EditIndex = -1;
      ShowInfo();
   }
}