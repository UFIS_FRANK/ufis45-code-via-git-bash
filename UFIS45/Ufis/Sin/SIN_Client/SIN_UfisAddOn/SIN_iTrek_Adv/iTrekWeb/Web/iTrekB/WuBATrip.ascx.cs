using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.ENT;
//using UFIS.ITrekLib.Util;

public partial class Web_iTrekB_WuBATrip : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!IsPostBack)
        //{
        //    gvTrips.Visible = false;
        //    gvContainer.Visible = false;
        //}
    }

    /// <summary>
    /// Populate Data To show the Trip and Container Information for given flight Id and Baggage Type
    /// </summary>
    /// <param name="flightId">Flight Id</param>
    /// <param name="tripHeading">Trip Heading</param>
    /// <param name="baType">EnBagArrType Baggage Arrival Type (1.Loca, 2.InterLine, 3.All)</param>
    public void PopulateData(string flightId, string tripHeading, EnBagArrType baType)
    {
        if (!string.IsNullOrEmpty(tripHeading))
        {
            lblTripMsg.Text = tripHeading;
        }
        else
        {
            lblTripMsg.Text = "Trip and Container Information";
        }

        DSTrip dsTrip = null;
        DSCpm dsCpm = null;

        string result = CtrlBagArrival.GetTripNContainerInfo(flightId, baType,
            ref dsTrip, ref dsCpm);

        if ((dsTrip == null) || (dsTrip.TRIP == null) || (dsTrip.TRIP.Rows == null) || (dsTrip.TRIP.Rows.Count < 1))
        {
            gvTrips.Visible = false;
        }
        else
        {
            DataView dv = dsTrip.TRIP.DefaultView;
            dv.Sort = "ULDN ASC, RCV_DT ASC, SENT_DT ASC, STRPNO ASC";
            gvTrips.DataSource = dv;
            gvTrips.DataBind();
            gvTrips.Visible = true;
        }

        if ((dsCpm == null) || (dsCpm.CPMDET == null) || (dsCpm.CPMDET.Rows == null) || (dsCpm.CPMDET.Rows.Count < 1))
        {
            gvContainer.Visible = false;
        }
        else
        {
            gvContainer.Visible = true;
            DataView dv = dsCpm.CPMDET.DefaultView;
            dv.Sort = "ULDN ASC";
            gvContainer.DataSource = dv;
            gvContainer.DataBind();
        }
    }

    /// <summary>
    /// Populate Data to Show the Trip and container information for given flight Id for all baggage type
    /// </summary>
    /// <param name="flightId">Flight Id</param>
    /// <param name="tripHeading">Trip Heading</param>
    public void PopulateData(string flightId, string tripHeading)
    {
        PopulateData(flightId, tripHeading, EnBagArrType.All);
    }    
}
