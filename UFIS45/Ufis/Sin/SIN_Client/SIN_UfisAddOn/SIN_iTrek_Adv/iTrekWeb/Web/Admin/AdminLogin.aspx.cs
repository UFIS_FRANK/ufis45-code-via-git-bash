using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

using UFIS.Web.ITrek.Sec;
using UFIS.Web.ITrek.WebMgr;

using MMSoftware.MMWebUtil;
using UFIS.ITrekLib.Util;
using UFIS.ITrekLib.ENT;

public partial class AdminLogin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //UFIS.Web.ITrek.WebMgr.MgrApp webAppMgr = new MgrApp();
        //webAppMgr.LoadConfig2(Application);
        //UFIS.Web.ITrek.WebMgr.MgrApp.LoadConfig(Application);

        txtUserId.Focus();
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        string errMsg = "";
        try
        {
            string userId = txtUserId.Text;

            string pwd = txtPwd.Text;

            if (CtrlSec.GetInstance().IsAuthenticateAdmin(userId, pwd, Session))
            {
                //FormsAuthentication.RedirectFromLoginPage(txtUserId.Text, false);
                //WebUtil.AlertMsg(Page, "Auth");
                FormsAuthentication.SetAuthCookie(txtUserId.Text, false);
                Response.Redirect("EventLog.aspx");
            }
            else
            {
                //errMsg = ((EntUser)(MgrSess.GetUserInfo(Session))).AuthError;
                if ((errMsg == null) || (errMsg == ""))
                {
                    WebUtil.AlertMsg(Page, "Invalid user id or password!\nPlease try again.");
                }
                else
                {
                    WebUtil.AlertMsg(Page, "Login failed due to " + errMsg);
                }
            }
        }
        catch (ApplicationException ex)
        {
            WebUtil.AlertMsg(Page, "Login failed due to " + ex.Message);
        }
        catch (Exception ex)
        {
            WebUtil.AlertMsg(Page, "Login failed due to " + ex.Message);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        txtUserId.Text = "";
        txtPwd.Text = "";
    }
}
