using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.Ctrl;
using UFIS.Web.ITrek.WebMgr;
using UFIS.Web.ITrek.Sec;
using UFIS.ITrekLib.DS;

//public partial class Web_iTrekB_BARcvTime : System.Web.UI.Page
public partial class Web_iTrekB_BARcvTime : UFIS.Web.ITrek.Base.BaseWebPage
{
   private const string PG_NAME = "ChgTimeArrApr";
   private const string CTRL_VIEW = "Edit";


   protected override void OnInit(EventArgs e)
   {
      base.OnInit(e);
      WuTime1.TimingSendClick += new Web_Wu_WuTime.TimingSendHandler(WuTime1_TimingSendClick);
      ((Web_iTrekB_Master)Master).OnRefresh += new Web_iTrekB_Master.RefreshHandler(Web_iTrekB_BARcvTime_OnRefresh);
   }

   protected override void OnLoad(EventArgs e)
   {
      base.OnLoad(e);
      if (CtrlSec.GetInstance().HasAccessToBARecvUldUpdate(LoginedUser))
      {
         if ((!IsPostBack) && (!Page.IsCallback))
         {
            PopulateData();
         }
      }
      else
      {
         DoUnAuthorise();
      }
   }

   void Web_iTrekB_BARcvTime_OnRefresh(object sender)
   {
      EntBagArrFlightSelectedUld selectedUld = WuBAFlight1.GetSelectedUldsForFlight();
      MgrSess.SetBagArrCurrentSel(Session, selectedUld);
      PopulateData();
   }

   void WuTime1_TimingSendClick(object sender, UFIS.ITrekLib.EventArg.EaTextEventArgs e)
   {
      string st = e.Text;
      SendTripData(e.Text);
      //throw new Exception("The method or operation is not implemented.");
   }

   private void PopulateData()
   {
      EntBagArrFlightSelectedUld obj = MgrSess.GetBagArrCurrentSel(this.Session);
      if ((obj.FlightId == null) || (obj.FlightId.Trim() == ""))
      {
         this.lblHead.Text = "No Flight Information!!!";
         WuBAFlight1.Visible = false;
         WuTime1.Visible = false;
      }
      else
      {
         WuBAFlight1.PopulateData(obj.FlightId, obj.BagArrType, null, obj.SelectedUldIds, obj.SelectedTrip);
         //WuBAFlight1.Collapsed = false;
         WuBAFlight1.ShowDetail = true;
         WuBAFlight1.EnableUldSelect = false;
         WuBAFlight1.Visible = true;
         WuTime1.Visible = true;
      }
   }

   /// <summary>
   /// Send Trip Data for Baggage Arrival Receive Date Time
   /// </summary>
   /// <param name="tripTime">Trip Time in Ufis format (YYYYMMDDhhmmss)</param>
   private void SendTripData(string tripTime)
   {  //Send Trip Information to back end
      //To Do - to send the trip data.
      try
      {
         string flightUrno = WuBAFlight1.FlightUrno;
         DSFlight ds = null;
         DateTime dtRecv = CtrlFlight.BagArrConvTime(flightUrno, tripTime, ref ds);
         EntBagArrFlightSelectedUld obj = WuBAFlight1.GetSelectedUldsForFlight();

         EnTripType tripType = EnTripType.ToAutoDefine; //To Auto Define in CtrlTrip
         LogMsg(string.Format("Receive Trip:FL{0},Time{1},By{2},<{3}>",
            flightUrno, tripTime, LoginedId, MM.UtilLib.UtilMisc.ConvListToString(obj.SelectedUldIds,",")));
		 //CtrlTrip.ReceiveTrip(flightUrno,
		 //    obj.SelectedUldIds, dtRecv, LoginedId, tripType);
		 CtrlTrip.UpdateTripsReceived(obj.SelectedUldIds,
			 MM.UtilLib.UtilTime.ConvDateTimeToUfisFullDTString(dtRecv), 
			 LoginedId, tripType, flightUrno);
         Response.Redirect("~/Web/iTrekB/BATerminalFlightUlds.aspx");
      }
      catch (ApplicationException ex)
      {
         LogMsg("SendTripData:Err:" + ex.Message);
         AlertMsg(ex.Message);
      }
      catch (Exception ex)
      {
         LogMsg("SendTripData:Err:" + ex.Message);
         AlertMsg("Error in Sending Timing.");
      }
   }

   private void AlertMsg(string msg)
   {
      string JS_ALTMSG = "JS_ALTMSG";
      if (!ClientScript.IsStartupScriptRegistered(JS_ALTMSG))
      {
         ClientScript.RegisterStartupScript(this.GetType(), JS_ALTMSG, "alert('" + msg + "');", true);
      }
   }

   private void LogMsg(string msg)
   {
      try
      {
         MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("BaRcvTime", msg);
      }
      catch (Exception)
      {
         UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("BaRcvTime", msg);
      }
   }
}