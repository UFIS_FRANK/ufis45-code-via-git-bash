<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WuBeltSelect.ascx.cs" Inherits="Web_Wu_WuBeltSelect" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
Select Terminal<asp:DropDownList ID="ddTerminal" runat="server" Width="206px" OnDataBound="ddTerminal_DataBound">
</asp:DropDownList>
&nbsp; Select Belt
<asp:DropDownList ID="ddBelt" runat="server" Width="205px" OnDataBound="ddBelt_DataBound">
</asp:DropDownList>
<asp:Button ID="btnSelect" runat="server" Text="Select" OnClick="btnSelect_Click" EnableViewState="False" />&nbsp;
<cc1:CascadingDropDown ID="cddTerminal" runat="server" TargetControlID="ddTerminal" Category="Terminal" LoadingText="[Loading Data...]" PromptText="Please select terminal" ServiceMethod="WsGetTerminals" ServicePath="~/WS/WsTerminalBelt.asmx">
</cc1:CascadingDropDown>
<cc1:CascadingDropDown ID="cddBelt" runat="server" Category="Belt" LoadingText="[Loading Data...]" ParentControlID="ddTerminal" PromptText="Please Select Belt" ServiceMethod="WsGetBeltsForTerminal" TargetControlID="ddBelt" ServicePath="~/WS/WsTerminalBelt.asmx">
</cc1:CascadingDropDown>
