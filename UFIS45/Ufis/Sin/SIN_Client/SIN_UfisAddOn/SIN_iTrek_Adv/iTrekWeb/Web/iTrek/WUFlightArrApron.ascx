<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WUFlightArrApron.ascx.cs" Inherits="Web_iTrek_WUFlightArrApron" %>
<div style="width:97%;" class="Heading">Arrival Flight - Apron &nbsp&nbsp&nbsp<asp:Label ID="lblHdr" runat="server" Text=" " CssClass="SubHeading" EnableViewState="False"></asp:Label>
</div>
<div>
<asp:HiddenField ID="hfScrollTop" runat="server" Value="0" />
<asp:GridView id="gvFl" runat="server" Width="970px" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black" GridLines="Vertical" OnRowDataBound="gvFlight_RowDataBound" AutoGenerateColumns="False" OnRowCommand="gvFlight_RowCommand" DataKeyNames="URNO" OnPageIndexChanging="gvFlight_PageIndexChanging" PageSize="50">
<FooterStyle BackColor="#CCCCCC"></FooterStyle>
<Columns>
<asp:BoundField DataField="URNO" HeaderText="URNO" ReadOnly="True" Visible="False" />
<asp:BoundField DataField="FLNO" HeaderText="FlightNo" ReadOnly="True" Visible="False" >
<ItemStyle Width="50px" />
</asp:BoundField>
<asp:TemplateField HeaderText="Arrival">
<ItemStyle HorizontalAlign="Left" Width="90px" />
<ItemTemplate>
<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# Eval("URNO", "ChgArrApronFlightTiming.aspx?URNO={0}") + Eval("FLNO","&FLNO={0}") %>'
 Text='<%# Eval("FLNO") %>'></asp:HyperLink>&nbsp;
<asp:Literal ID="ltlTfln" runat="server" Text='<%# Eval("TFLN", "<BR>({0})") %>'></asp:Literal>
</ItemTemplate>
</asp:TemplateField>
<asp:BoundField DataField="REGN" HeaderText="Regn" ReadOnly="True" />
<asp:BoundField HtmlEncode="False" DataField="ST_TIME" HeaderText="STA" ReadOnly="True"></asp:BoundField>
<asp:BoundField HtmlEncode="False" DataField="ET_TIME" HeaderText="ETA" ReadOnly="True"></asp:BoundField>
<asp:BoundField DataField="BAY" HeaderText="BAY" ReadOnly="True"></asp:BoundField>
<asp:BoundField HtmlEncode="False" DataField="AT_TIME" HeaderText="ATA" ReadOnly="True"></asp:BoundField>
<asp:TemplateField HeaderText="MAB">
<EditItemTemplate>
&nbsp;<table>
<tr>
<td style="width: 66px">
<asp:Label ID="lblEdMAB" runat="server" Text='<%# Eval("MAB_TIME", "{0:HHmm}") + "&nbsp" %>'></asp:Label></td>
</tr>
<tr>
<td style="width: 66px">
<asp:TextBox ID="txtNewMAB" runat="server" Text='<%# Bind("MAB_TIME", "{0:HHmm}") %>' Width="68px"></asp:TextBox></td>
</tr>
</table>
</EditItemTemplate>
<ItemTemplate>
<asp:Label ID="lblDispMAB" runat="server" Text='<%# Bind("MAB_TIME", "{0:HHmm}") %>'></asp:Label>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="TBS UP">
<EditItemTemplate>
&nbsp;<table>
<tr>
<td style="width: 100px">
<asp:Label ID="lblEdTbsUp" runat="server" Text='<%# Eval("TBS_UP_TIME", "{0:HHmm}") + "&nbsp" %>'></asp:Label></td>
</tr>
<tr>
<td style="width: 100px">
<asp:TextBox ID="txtNewTbsUp" runat="server" Text='<%# Bind("TBS_UP_TIME", "{0:HHmm}") %>' Width="49px"></asp:TextBox></td>
</tr>
</table>
</EditItemTemplate>
<ItemTemplate>
<asp:Label ID="lblDispTbsUp" runat="server" Text='<%# Bind("TBS_UP_TIME", "{0:HHmm}") %>'></asp:Label>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="PLB">
<EditItemTemplate>
&nbsp;<table>
<tr>
<td style="width: 100px">
<asp:Label ID="lblEdPLB" runat="server" Text='<%# Eval("PLB_TIME", "{0:HHmm}") + "&nbsp" %>'></asp:Label></td>
</tr>
<tr>
<td style="width: 100px">
<asp:TextBox ID="txtNewPLB" runat="server" Text='<%# Bind("PLB_TIME", "{0:HHmm}") %>' Width="45px"></asp:TextBox></td>
</tr>
</table>
</EditItemTemplate>
<ItemTemplate>
<asp:Label ID="lblDispPLB" runat="server" Text='<%# Bind("PLB_TIME", "{0:HHmm}") %>'></asp:Label>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="UNL START">
<EditItemTemplate>
&nbsp;<table>
<tr>
<td style="width: 100px">
<asp:Label ID="lblEdUnlStart" runat="server" Text='<%# Eval("UNL_STRT_TIME", "{0:HHmm}") + "&nbsp" %>'></asp:Label></td>
</tr>
<tr>
<td style="width: 100px">
<asp:TextBox ID="txtNewUnlStart" runat="server" Text='<%# Bind("UNL_STRT_TIME", "{0:HHmm}") %>' Width="51px"></asp:TextBox></td>
</tr>
</table>
</EditItemTemplate>
<ItemTemplate>
<asp:Label ID="lblDispUnlStart" runat="server" Text='<%# Eval("UNL_STRT_TIME", "{0:HHmm}") %>'></asp:Label>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="FIRST TRIP">
<EditItemTemplate>
&nbsp;<table>
<tr>
<td style="width: 100px">
<asp:Label ID="lblEdFirstTrip" runat="server" Text='<%# Eval("AP_FST_TRP_TIME", "{0:HHmm}") + "&nbsp" %>'></asp:Label></td>
</tr>
<tr>
<td style="width: 100px">
<asp:TextBox ID="txtNewFirstTrip" runat="server" Text='<%# Bind("AP_FST_TRP_TIME", "{0:HHmm}") %>' Width="58px"></asp:TextBox></td>
</tr>
</table>
</EditItemTemplate>
<ItemTemplate>
<asp:Label ID="lblDispFirstTrip" runat="server" Text='<%# Eval("AP_FST_TRP_TIME", "{0:HHmm}") + "&nbsp" %>'></asp:Label>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="LAST TRIP">
<EditItemTemplate>
&nbsp;<table>
<tr>
<td style="width: 100px">
<asp:Label ID="lblEdLastTrip" runat="server" Text='<%# Eval("AP_LST_TRP_TIME", "{0:HHmm}") + "&nbsp" %>'></asp:Label></td>
</tr>
<tr>
<td style="width: 100px">
<asp:TextBox ID="txtNewLastTrip" runat="server" Text='<%# Bind("AP_LST_TRP_TIME", "{0:HHmm}") %>' Width="57px"></asp:TextBox></td>
</tr>
</table>
</EditItemTemplate>
<ItemTemplate>
<asp:Label ID="lblDispLastTrip" runat="server" Text='<%# Bind("AP_LST_TRP_TIME", "{0:HHmm}") %>'></asp:Label>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="UNLOAD END">
<EditItemTemplate>
&nbsp;<table>
<tr>
<td style="width: 100px">
<asp:Label ID="lblEdUnlEnd" runat="server" Text='<%# Eval("UNL_END_TIME", "{0:HHmm}") + "&nbsp" %>'></asp:Label></td>
</tr>
<tr>
<td style="width: 100px">
<asp:TextBox ID="txtNewUnlEnd" runat="server" Text='<%# Bind("UNL_END_TIME", "{0:HHmm}") %>' Width="59px"></asp:TextBox></td>
</tr>
</table>
</EditItemTemplate>
<ItemTemplate>
<asp:Label ID="lblDispUnlEnd" runat="server" Text='<%# Bind("UNL_END_TIME", "{0:HHmm}") %>'></asp:Label>
</ItemTemplate>
</asp:TemplateField>
<asp:TemplateField HeaderText="Pass Fail">
<ItemTemplate>
<asp:LinkButton ID="lbBMPassFail" runat="server" CausesValidation="false" CommandName="" EnableViewState="True" OnClientClick='<%# "open_window(\"BMPassFail.aspx?URNO=" + Eval("URNO") + "&FLNO=" + Eval("FLNO") + "&AD=A&AB=A\");" %>'></asp:LinkButton>
</ItemTemplate>
</asp:TemplateField>
<asp:HyperLinkField HeaderText="Staff" Text="Show" DataNavigateUrlFormatString="Staff.aspx?FLNO={0}&amp;URNO={1}&amp;AD=A&amp;AB=A" DataNavigateUrlFields="FLNO,URNO"></asp:HyperLinkField>
<asp:HyperLinkField HeaderText="CPM" Text="Show" DataNavigateUrlFormatString="CPM.aspx?URNO={0}&amp;FLNO={1}&amp;AD=A&amp;AB=A" DataNavigateUrlFields="URNO,FLNO"></asp:HyperLinkField>
<asp:HyperLinkField DataNavigateUrlFields="URNO,FLNO" DataNavigateUrlFormatString="Trips.aspx?URNO={0}&amp;FLNO={1}&amp;AD=A&amp;AB=A" HeaderText="Trips" Text="Show" />
<asp:HyperLinkField HeaderText="Apron Service Report" DataNavigateUrlFormatString="ApronServiceReport.aspx?URNO={0}&amp;FLNO={1}&amp;AD=A&amp;AB=A" DataNavigateUrlFields="URNO,FLNO">
<ItemStyle Width="80px" HorizontalAlign="Center"></ItemStyle>
</asp:HyperLinkField>
<asp:BoundField DataField="WD_AO" HeaderText="ALO" ReadOnly="True">
<ItemStyle Width="100px" />
</asp:BoundField>
<asp:HyperLinkField HeaderText="MSG" Text="New" DataNavigateUrlFormatString="FlightMessage.aspx?URNO={0}&amp;AD=A&amp;AB=A&amp;FLNO={1}" DataNavigateUrlFields="URNO,FLNO"></asp:HyperLinkField>
</Columns>
<SelectedRowStyle BackColor="#000099" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
<PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center"></PagerStyle>
<HeaderStyle BackColor="Black" ForeColor="White" Font-Bold="True"></HeaderStyle>
<AlternatingRowStyle BackColor="#CCCCCC"></AlternatingRowStyle>
</asp:GridView>
</div>