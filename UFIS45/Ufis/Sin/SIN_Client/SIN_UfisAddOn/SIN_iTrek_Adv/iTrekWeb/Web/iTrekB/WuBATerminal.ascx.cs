using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.Util;
using UFIS.ITrekLib.EventArg;
using UFIS.Web.ITrek.WebMgr;

public partial class Web_iTrekB_WuBATerminal : System.Web.UI.UserControl
{
   protected void Page_Load(object sender, EventArgs e)
   {
      CreateDynamicControls();
   }

   void Page_LoadComplete(Object o, EventArgs e)
   {
      string st = e.ToString();
   }

   protected override void CreateChildControls()
   {
   }

   private void CreateDynamicControls()
   {
      if ((Page.IsCallback) || (Page.IsPostBack))
      {
         //LogMsg("CreateDynamicControls");
         AddDynamicControls(false);
      }
   }

   protected void Page_PreRender(object sender, EventArgs e)
   {
      //KeepSelectedUldsForTerminal();
   }

   private int _beltCnt = -1;
   private const string VS_BELTCNT_WUTERMINAL = "VS_BELTCNT_WUTERMINAL";
   private int BeltCntInternal
   {
      get
      {
         if (_beltCnt < 0)
         {
            try
            {
               _beltCnt = Convert.ToInt32(ViewState[VS_BELTCNT_WUTERMINAL]);
            }
            catch (Exception)
            {
            }
         }
         return _beltCnt;
      }
      set
      {
         _beltCnt = value;
         ViewState[VS_BELTCNT_WUTERMINAL] = _beltCnt;
      }
   }

   public void PopulateData(EnBagArrType baType, string terminalId,
       string stBeltInfo, bool forcedRefresh,
       ArrayList arrBeltIds)
   {
      EntBagArrFlightSelectedUldList selectedUldList = null;
      try
      {
         selectedUldList = GetSelectedUldsForTerminal();
         MgrSess.SetBagArrFlightSelectedUldList(Session, selectedUldList);
      }
      catch (Exception)
      {
      }
      PopulateData(baType, terminalId, stBeltInfo, forcedRefresh, arrBeltIds, selectedUldList);
   }

   private void LogMsg(string msg)
   {
      //UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("WuBATerminal", msg);
      try
      {
         MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("WuBATerminal", msg);
      }
      catch (Exception)
      {
         UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("WuBATerminal", msg);
      }
   }

   public void PopulateData(EnBagArrType baType, string terminalId,
       string stBeltInfo, bool forcedRefresh,
       ArrayList arrBeltIds,
        EntBagArrFlightSelectedUldList selectedUlds)
   {
      Info = new CurInfo(baType, terminalId, stBeltInfo, arrBeltIds);//Save the current criteria
      if ((!forcedRefresh) && (_isDataRefreshed)) return;

      DSFlight dsFlight = null;
      try
      {
         TerminalIdInternal = terminalId;
         BagArrTypeInternal = baType;
         //LogMsg(string.Format("Terminal-{0},Type-{1}", TerminalIdInternal, BagArrTypeInternal));
         this.lblHead.Text = string.Format("Baggage Arrival {0} {1}", CtrlTerminal.GetTerminalName(terminalId), stBeltInfo);
         //arr.Sort();
         BeltCntInternal = arrBeltIds.Count;
         //LogMsg("BeltCnt:" + BeltCntInternal);
         dsFlight = CtrlBagArrival.GetArrivalFlights(terminalId);
         AddDynamicControls(true);
         PopulateBeltData(baType, terminalId, arrBeltIds, dsFlight, selectedUlds);
         _isDataRefreshed = true;
      }
      catch (Exception ex)
      {
         LogMsg("PopulateData:Err1:" + ex.Message);
         string st = ex.Message;
         DisplayError("Error in Retrieving Flight Information.");

         return;
      }
   }

   private bool _hasAddedControls = false;

   private void AddDynamicControls(bool forceAdding)
   {
      if ((_hasAddedControls) && (!forceAdding)) return;
      else
      {
         int cnt = BeltCntInternal;
         int curIdx = 0;
         int curCnt = phBelts.Controls.Count;
         for (int i = curCnt - 1; i >= 0; i--)
         {
            if (phBelts.Controls[i] is Web_iTrekB_WuBABelt)
            {
               curIdx++;
               if (curIdx > cnt)
               {//Remove the extra
                  phBelts.Controls.RemoveAt(i);
               }
            }
         }
         for (int i = curIdx; i < cnt; i++)
         {
            //ASP.web_itrekb_wubabelt_ascx wu = new ASP.web_itrekb_wubabelt_ascx();
            //Web_iTrekB_WuBABelt wu = new Web_iTrekB_WuBABelt();
            //phBelts.Controls.Add(wu);
            //Web_iTrekB_WuBABelt wu = ;
            Control uc = LoadControl("WuBABelt.ascx");
            Web_iTrekB_WuBABelt wu = (Web_iTrekB_WuBABelt)uc;
            //ASP.web_itrekb_wubabelt_ascx wu = (ASP.web_itrekb_wubabelt_ascx) uc;
            phBelts.Controls.Add(uc);
            wu.BagArrFlightActionClick += new Web_iTrekB_WuBABelt.BagArrFlightActionHandler(wu_BagArrFlightActionClick);
         }
         _hasAddedControls = true;
      }
   }

   public void KeepSelectedUldsForTerminal()
   {
      MgrSess.SetBagArrFlightSelectedUldList(Session,
          GetSelectedUldsForTerminal());
   }


   public EntBagArrFlightSelectedUldList GetSelectedUldsForTerminal()
   {
      EntBagArrFlightSelectedUldList obj = new EntBagArrFlightSelectedUldList();
      foreach (Control c in phBelts.Controls)
      {
         //if (c is ASP.web_itrekb_wubabelt_ascx)
         if (c is Web_iTrekB_WuBABelt)
         {
            //ASP.web_itrekb_wubabelt_ascx wu = (ASP.web_itrekb_wubabelt_ascx)c;
            Web_iTrekB_WuBABelt wu = (Web_iTrekB_WuBABelt)c;
            obj.Add(wu.GetSelectedUldsForBelt());
         }
      }
      return obj;
   }

   void wu_BagArrFlightActionClick(object sender, UFIS.ITrekLib.EventArg.EaBagArrFlightEventArgs bagArrFlightEventArgs)
   {
      string stFlId = bagArrFlightEventArgs.FlightId;
      if (this.BagArrFlightActionClick != null)
      {
         this.BagArrFlightActionClick(sender, bagArrFlightEventArgs);
      }
   }

   //// add a delegate
   public delegate void BagArrFlightActionHandler(object sender,
       EaBagArrFlightEventArgs bagArrFlightEventArgs);


   //// add an event of the delegate type
   public event BagArrFlightActionHandler BagArrFlightActionClick;

   private void PopulateBeltData(EnBagArrType baType, string terminalId,
       ArrayList arrBelts, DSFlight dsFlight, EntBagArrFlightSelectedUldList selectedUlds)
   {
      //int cnt = BeltCntInternal;
      //if (arrBelts.Count < cnt) cnt = arrBelts.Count;
      int idx = 0;
      foreach (Control c in phBelts.Controls)
      {
         //if (c is ASP.web_itrekb_wubabelt_ascx)
         if (c is Web_iTrekB_WuBABelt)
         {
            //ASP.web_itrekb_wubabelt_ascx wu = (ASP.web_itrekb_wubabelt_ascx)c;
            Web_iTrekB_WuBABelt wu = (Web_iTrekB_WuBABelt)c;
            wu.PopulateData(baType, terminalId, arrBelts[idx].ToString(), dsFlight, selectedUlds);
            idx++;
            if (idx >= arrBelts.Count) break;
         }
      }
   }

   private const string JS_ERRMSG_WUBATERM = "JS_ERRMSG_WUBATERM";
   public void DisplayError(string errMsg)
   {
      if (Page.ClientScript.IsStartupScriptRegistered(JS_ERRMSG_WUBATERM))
      {
         Page.ClientScript.RegisterStartupScript(this.GetType(), JS_ERRMSG_WUBATERM,
             "alert('" + errMsg + "')", true);
      }
   }

   private string _terminalId = "";
   private const string VS_TERMID_WUTERMINAL = "VS_TERMID_WUTERMINAL";
   private string TerminalIdInternal
   {
      get
      {
         if (_terminalId == "")
         {
            try
            {
               _terminalId = (string)ViewState[VS_TERMID_WUTERMINAL];
            }
            catch (Exception)
            {
            }
         }
         return _terminalId;
      }
      set
      {
         _terminalId = value;
         ViewState[VS_TERMID_WUTERMINAL] = _terminalId;
      }
   }

   private EnBagArrType _baType = EnBagArrType.None;
   private const string VS_BATYPE_WUTERMINAL = "VS_BATYPE_WUTERMINAL";
   private EnBagArrType BagArrTypeInternal
   {
      get
      {
         if (_baType == EnBagArrType.None)
         {
            try
            {
               _baType = (EnBagArrType)ViewState[VS_BATYPE_WUTERMINAL];
            }
            catch (Exception)
            {
            }
         }
         return _baType;
      }
      set
      {
         _baType = value;
         ViewState[VS_BATYPE_WUTERMINAL] = _baType;
      }
   }

   private bool _isDataRefreshed = false;

   public void Refresh()
   {
      CurInfo info = Info;
      if (info != null)
      {
         PopulateData(info._baType, info._terminalId, info._stBeltInfo, false, info._arrBeltIds);
      }
   }

   [Serializable]
   private class CurInfo
   {
      public string _terminalId;
      public EnBagArrType _baType;
      public string _stBeltInfo;
      public ArrayList _arrBeltIds;

      public CurInfo(EnBagArrType baType, string terminalId,
      string stBeltInfo,
      ArrayList arrBeltIds)
      {
         _terminalId = terminalId;
         _baType = baType;
         _stBeltInfo = stBeltInfo;
         _arrBeltIds = arrBeltIds;
      }
   }

   private CurInfo _info = null;
   private const string VSBATERMINAL = "VSBATERMINAL";

   private CurInfo Info
   {
      get
      {
         if (_info == null)
         {
            try
            {
               _info = (CurInfo)ViewState[VSBATERMINAL];
            }
            catch (Exception)
            {
            }
         }
         return _info;
      }
      set
      {
         _info = value;
         ViewState[VSBATERMINAL] = _info;
      }
   }
}