using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.ENT;
using UFIS.Web.ITrek.WebMgr;
using UFIS.Web.ITrek.Sec;
using UFIS.ITrekLib.Ctrl;

//public partial class Web_iTrekB_BATerminalFlightUlds : System.Web.UI.Page
public partial class Web_iTrekB_BATerminalFlightUlds : UFIS.Web.ITrek.Base.BaseWebPage
{
   protected override void OnInit(EventArgs e)
   {
      base.OnInit(e);
      WuBeltSelect1.BeltSelected += new Web_Wu_WuBeltSelect.BeltSelectHandler(WuBeltSelect1_BeltSelected);
      WuBATerminal1.BagArrFlightActionClick += new Web_iTrekB_WuBATerminal.BagArrFlightActionHandler(WuBATerminal1_BagArrFlightActionClick);
      ((Web_iTrekB_Master)Master).OnRefresh += new Web_iTrekB_Master.RefreshHandler(Web_iTrekB_BATerminalFlightUlds_OnRefresh);
      Timer1.Interval = CtrlConfig.GetWebRefreshTiming() * 1000;
   }

   protected override void OnPreLoad(EventArgs e)
   {
      base.OnPreLoad(e);
      ((Web_iTrekB_Master)Master).AutoRefreshMode = false;
   }

   protected override void OnLoad(EventArgs e)
   {
      base.OnLoad(e);
      if (CtrlSec.GetInstance().HasAccessToBATerminalUlds(LoginedUser))
      {
         if ((!IsPostBack) && (!Page.IsCallback))
         {
            ShowData();
         }
      }
      else
      {
         DoUnAuthorise();
      }
   }

   //protected override void OnLoadComplete(EventArgs e)
   //{
   //    //KeepSelectedData();
   //    //ShowData();
   //}

   protected override void OnPreRender(EventArgs e)
   {
      base.OnPreRender(e);
      KeepSelectedData();
      ShowData();
   }

   private EntBagArrTerminal _objBagArrTerminal = null;
   private EntBagArrTerminal ObjBagArrTerminalInternal
   {
      get
      {
         if (_objBagArrTerminal == null)
         {
            try
            {
               _objBagArrTerminal = MgrSess.GetBagArrTerminalInfo(Session);
            }
            catch (Exception)
            {
            }
         }
         if (_objBagArrTerminal == null) return new EntBagArrTerminal();
         return _objBagArrTerminal;
      }
      set
      {
         _objBagArrTerminal = value;
         MgrSess.SetBagArrTerminalInfo(Session, _objBagArrTerminal);
      }
   }

   void Web_iTrekB_BATerminalFlightUlds_OnRefresh(object sender)
   {
      //KeepSelectedData();
      //ShowData();
   }

   void WuBATerminal1_BagArrFlightActionClick(object sender, UFIS.ITrekLib.EventArg.EaBagArrFlightEventArgs bagArrFlightEventArgs)
   {
      KeepSelectedData();
   }

   void WuBeltSelect1_BeltSelected(object sender, string terminalId, string selectedBeltId,
        ArrayList arrBeltIds, string stBeltInfo, EnBagArrType bagArrType)
   {
      //EntBagArrTerminal objOrg = ObjBagArrTerminalInternal;
      EntBagArrTerminal obj = new EntBagArrTerminal();
      obj.TerminalId = terminalId;
      obj.ArrBeltIds = arrBeltIds;
      obj.BeltInfo = stBeltInfo;
      obj.BagArrType = bagArrType;
      obj.DropDownListBeltId = selectedBeltId;

      ObjBagArrTerminalInternal = obj;
      ShowData();
   }


   private void KeepSelectedData()
   {
      try
      {
         WuBATerminal1.KeepSelectedUldsForTerminal();
         //EntBagArrFlightSelectedUldList selectedUldList = WuBATerminal1.GetSelectedUldsForTerminal();
         //MgrSess.SetBagArrFlightSelectedUldList(Session, WuBATerminal1.GetSelectedUldsForTerminal());
      }
      catch (Exception ex)
      {
         string st = ex.Message;
      }
   }

   private bool _hasShownData = false;

   private void ShowData()
   {
      if (_hasShownData) return;
      _hasShownData = true;
      EntBagArrTerminal obj = ObjBagArrTerminalInternal;
      if ((obj != null) && (!string.IsNullOrEmpty(obj.TerminalId)) && (!string.IsNullOrEmpty(obj.BeltInfo)))
      {
         EntBagArrFlightSelectedUldList selectedUldList = MgrSess.GetBagArrFlightSelectedUldList(Session);
         if ((!Page.IsPostBack) && (!Page.IsCallback))
         {
            WuBeltSelect1.SetSelectedValue(obj.TerminalId, obj.DropDownListBeltId);
         }
         WuBATerminal1.PopulateData(obj.BagArrType, obj.TerminalId, obj.BeltInfo, true, obj.ArrBeltIds, selectedUldList);
      }
   }

   private void LogMsg(string msg)
   {
      try
      {
         MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("BATerminalFlightUlds", msg);
      }
      catch (Exception)
      {
         UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("BATerminalFlightUlds", msg);
      }
   }
}
