using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.Web.ITrek.WebMgr;
using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.Util;
using MM.UtilLib;

public partial class Web_iTrek_MessageDetail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ShowMsg();
        }
    }

    public void ShowMsg()
    {
        string msgId = MgrSess.GetCurrentMessageId(Session);
        if (msgId == "")
        {
            lblMsgHdr.Text = "No message was selected.";
        }
        else
        {
            string inOrOutMsgInd = Request.Params["INOUT"];
            switch (inOrOutMsgInd)
            {
                case "I"://Inbox message
                    ShowInMsg(msgId);
                    break;
                case "O": //Outbox message
                    ShowOutMsg(msgId);
                    break;
                default:
                    lblMsgHdr.Text = "Unidentified message.";
                    break;
            }
        }
    }

    private void ShowInMsg(string msgId)
    {
        DSMsg ds = null;
        //ds = CtrlMsg.RetrieveInMessageDetailsForUrno(msgId);
        txtMsgBody.Text = ds.MSGTO[0].MSG_TEXT;
        string sentDt = ds.MSGTO[0].SENT_DT;
        DateTime dt = UtilTime.ConvUfisTimeStringToDateTime(sentDt);
        lblMsgHdr.Text = string.Format("Sent : " + ds.MSG[0].SENT_BY_NAME + " on {0:dd/MMM/yyyy  HH:mm}", dt);
        lblMsgTo.Text = "To : " + ds.MSGTO[0].MSG_TO_ID;
        string rcvBy = ds.MSGTO[0].RCV_BY;
        string stRcvDt = ds.MSGTO[0].RCV_DT;
        if (stRcvDt == "")
        {
            btnConfirmRead.Visible = true;
            btnNotConfirmRead.Visible = true;
            lblRcvBy.Visible = false;
        }
        else
        {
            btnConfirmRead.Visible = false;
            btnNotConfirmRead.Visible = false;
            DateTime rcvDt = UtilTime.ConvUfisTimeStringToDateTime(stRcvDt);
            lblRcvBy.Text = string.Format("Received by " + rcvBy + " on {0:dd/MMM/yyyy  HH:mm}", rcvDt );
            lblRcvBy.Visible = true;
        }

    }

    private void ShowOutMsg(string msgId)
    {
        DSMsg ds = null;
        ds = CtrlMsg.RetrieveOutMessageDetailsForUrno(msgId);
        txtMsgBody.Text = ds.MSG[0].MSG_BODY;
        string sentDt = ds.MSG[0].SENT_DT;
        DateTime dt = UtilTime.ConvUfisTimeStringToDateTime(sentDt);
        lblMsgHdr.Text = string.Format("Sent : " + ds.MSG[0].SENT_BY_NAME + " on {0:dd/MMM/yyyy  HH:mm}", dt);
        lblMsgTo.Text = "To : " + ds.GetSentTo(msgId);
        btnConfirmRead.Visible = false;
        btnNotConfirmRead.Visible = false;
    }
}
