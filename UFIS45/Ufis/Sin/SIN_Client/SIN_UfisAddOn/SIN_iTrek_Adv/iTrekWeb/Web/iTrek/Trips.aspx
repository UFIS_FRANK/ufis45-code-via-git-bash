<%@ Page Language="C#" MasterPageFile="~/Web/iTrek/MasterPage.master" AutoEventWireup="true" CodeFile="Trips.aspx.cs" Inherits="Web_iTrek_Trips" Title="Trips" Theme="SATSTheme" %>

<%@ Register Src="WUFlightArrApron.ascx" TagName="WUFlightArrApron" TagPrefix="uc1" %>
<%@ Register Src="WUFlightDepApron.ascx" TagName="WUFlightDepApron" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" Runat="Server">
<table style="width: 100%">
<tr><td>
        <uc1:WUFlightArrApron ID="wuFlightArrApron1" runat="server" EnableViewState="true" Visible="false" />
        <uc2:WUFlightDepApron ID="wuFlightDepApron1" runat="server" EnableViewState="true" Visible="false" />
</td></tr>
<tr><td style="height: 500px" valign="top">
    <table>
    <tr><td style="width: 773px">
        <asp:Label ID="lblTripMsg" runat="server" Width="649px"></asp:Label>
    </td></tr>
    <tr><td style="width: 773px">
        <asp:GridView ID="gvTrips" runat="server" Width="771px" AutoGenerateColumns="False" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black" GridLines="Vertical">
        <FooterStyle BackColor="#CCCCCC" />
        <Columns>
            <asp:BoundField DataField="STRPNO" HeaderText="Trip No." />
            <asp:BoundField DataField="ULDN" HeaderText="Container No." />
            <asp:BoundField DataField="VDES" HeaderText="DES" />
            <asp:BoundField DataField="SENT_DT_TIME" DataFormatString="{0:hhmm}" HeaderText="Sent" HtmlEncode="False" />
            <asp:BoundField DataField="SENT_RMK" HeaderText="Sent Remark" />
            <asp:BoundField DataField="RCV_DT_TIME" DataFormatString="{0:hhmm}" HeaderText="Receive" HtmlEncode="False" />
        </Columns>
        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="#CCCCCC" />
        </asp:GridView>
    </td></tr>
    </table>
</td></tr>
</table>
</asp:Content>

