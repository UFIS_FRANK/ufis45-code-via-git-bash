<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WUAFlight.ascx.cs" Inherits="Web_iTrek_WUAFlight" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
    <asp:ScriptManager ID="ScriptManager1"  runat="server" EnablePartialRendering="True">
</asp:ScriptManager>
<table width="970" border="0">
    <tr style="color: white; background-color: black">
        <td style="width: 100px; height: 47px">
            <strong>
            Flight</strong></td>
        <td style="width: 100px; height: 47px">
            <strong>
            STA</strong></td>
        <td style="width: 100px; height: 47px">
            <strong>
            ETA</strong></td>
        <td style="width: 100px; height: 47px">
            <strong>
            ATA</strong></td>
        <td style="width: 100px; height: 47px">
            <strong>
            Bay</strong></td>
        <td style="width: 100px; height: 47px">
            <strong>
            MAB</strong></td>
        <td style="width: 100px; height: 47px">
            <strong>
            TBS UP</strong></td>
        <td style="width: 100px; height: 47px">
            <strong >
            PLB</strong></td>
        <td style="width: 100px; height: 47px">
            <strong>
            UNL STRT</strong></td>
        <td style="width: 100px; height: 47px">
            <strong>
            FIRST TRP</strong></td>
        <td style="width: 100px; height: 47px">
            <strong>
            LAST TRP</strong></td>
        <td style="width: 100px; height: 47px">
            <strong>
            UNLOAD END</strong></td>
        <td style="width: 100px; height: 47px">
            <strong>
            CPM</strong></td>
        <td style="width: 100px; height: 47px">
            <strong>
            Apron Service Report</strong></td>
        <td style="width: 100px; height: 47px">
            <strong>
            ALO</strong></td>
        <td style="width: 100px; height: 47px">
            <strong>
            MSG</strong></td>
        <td style="width: 100px; height: 47px">
        </td>
        <td style="width: 100px; height: 47px">
        </td>
    </tr>
    <tr style="color: black; background-color: gray">
        <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>
        <td style="width: 100px">
            <asp:Label ID="lblFlight" runat="server"></asp:Label></td>
        <td style="width: 100px">
            <asp:Label ID="lblSTA" runat="server"></asp:Label></td>
        <td style="width: 100px">
            <asp:Label ID="lblETA" runat="server"></asp:Label></td>
        <td style="width: 100px">
            <asp:Label ID="lblATA" runat="server"></asp:Label></td>
        <td style="width: 100px">
            <asp:Label ID="lblBay" runat="server"></asp:Label></td>
        <td style="width: 100px">
            <asp:Label ID="lblMAB" runat="server"></asp:Label></td>
        <td style="width: 100px">
            <asp:Label ID="lblTbsUp" runat="server" Width="53px"></asp:Label></td>
        <td style="width: 100px">
            <asp:Label ID="lblPlb" runat="server"></asp:Label></td>
        <td style="width: 100px">
            <asp:Label ID="lblUnlStart" runat="server"></asp:Label></td>
        <td style="width: 100px">
            <asp:Label ID="lblFirstTrip" runat="server"></asp:Label></td>
        <td style="width: 100px">
            <asp:Label ID="lblLastTrip" runat="server"></asp:Label></td>
        <td style="width: 100px">
            <asp:Label ID="lblUnlEnd" runat="server"></asp:Label></td>
        <td style="width: 100px">
        </td>
        <td style="width: 100px">
        </td>
        <td style="width: 100px">
            <asp:Label ID="lblALO" runat="server"></asp:Label></td>
        <td style="width: 100px">
        </td>
        <td style="width: 100px">
        </td>
        <td style="width: 100px">
        </td>
        </ContentTemplate>
        </asp:UpdatePanel>
    </tr>
    <tr>
        <td style="width: 100px; height: 20px;">
        </td>
        <td style="width: 100px; height: 20px;">
        </td>
        <td style="width: 100px; height: 20px;">
        </td>
        <td style="width: 100px; height: 20px;">
        </td>
        <td style="width: 100px; height: 20px;">
        </td>
        <td style="width: 100px; height: 20px;">
            <asp:TextBox ID="txtMAB" runat="server" Visible="False" Width="52px" OnTextChanged="txtMAB_TextChanged"></asp:TextBox></td>
        <td style="width: 100px; height: 20px;">
            <asp:TextBox ID="txtTbsUp" runat="server" Visible="False" Width="52px"></asp:TextBox></td>
        <td style="width: 100px; height: 20px;">
            <asp:TextBox ID="txtPlb" runat="server" Visible="False" Width="52px"></asp:TextBox></td>
        <td style="width: 100px; height: 20px;">
            <asp:TextBox ID="txtUnlStart" runat="server" Visible="False" Width="52px"></asp:TextBox></td>
        <td style="width: 100px; height: 20px;">
            <asp:TextBox ID="txtFirstTrip" runat="server" Visible="False" Width="52px"></asp:TextBox></td>
        <td style="width: 100px; height: 20px;">
            <asp:TextBox ID="txtLastTrip" runat="server" Visible="False" Width="52px"></asp:TextBox></td>
        <td style="width: 100px; height: 20px;">
            <asp:TextBox ID="txtUnlEnd" runat="server" Visible="False" Width="52px"></asp:TextBox></td>
        <td style="width: 100px; height: 20px;">
        </td>
        <td style="width: 100px; height: 20px;">
        </td>
        <td style="width: 100px; height: 20px;">
        </td>
        <td style="width: 100px; height: 20px;">
        </td>
        <td style="width: 100px; height: 20px;">
        </td>
        <td style="width: 100px; height: 20px;">
        </td>
    </tr>
</table>
<asp:HiddenField ID="hfUrNo" runat="server" Visible="False" />
<asp:Timer ID="Timer1" runat="server" OnTick="Timer1_Tick">
</asp:Timer>

