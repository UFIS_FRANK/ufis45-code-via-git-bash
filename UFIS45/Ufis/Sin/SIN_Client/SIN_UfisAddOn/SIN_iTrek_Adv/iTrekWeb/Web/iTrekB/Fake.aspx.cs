using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.Web.ITrek.WebMgr;
using UFIS.ITrekLib.ENT;

public partial class Web_iTrekB_Fake : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        EntUser user = new EntUser( txtUserId.Text.Trim(), "", "A1", "AO,ACC,ADMIN" );
        user.AuthenticateUser = true;
        user.FirstName = "Aung";
        user.LastName = "Moe";
        user.Role = "ADMIN";
        MgrSess.SetUserInfo(Session, user);
        ArrayList sh = new ArrayList();
        ArrayList ln = new ArrayList();
        //string stat = "";
        //sh = UFIS.ITrekLib.DB.DBFlight.GetInstance().RetrieveOldFlighs(out ln, out stat);
        FormsAuthentication.SetAuthCookie(txtUserId.Text.Trim(), false);
        Response.Redirect(@"~/Web/iTrekB/BATerminalFlightUlds.aspx");
    }
}
