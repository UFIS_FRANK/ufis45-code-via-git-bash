using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using MMSoftware.MMWebUtil;
using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.Util;
using UFIS.Web.ITrek.WebMgr;

public partial class Web_Admin_EventLog : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!MgrSess.IsValidUserToGetLog(Session))
        {
            UnAuthorise();
        }
        else
        {
            if (!IsPostBack)
            {
                WUDate1.DateTimeValue = DateTime.Now;
                WUDate1.DateTimeLabel = "Enter Log Date : ";
                //WebUtil.AlertMsg(Page, UFIS.Web.ITrek.WebMgr.MgrSess.IsValidUserToGetLog(Session).ToString());
            }
            SetButtonVisible(true);
            lblMsg.Text = "";
        }
    }

    private void SetButtonVisible(bool visible)
    {
        btnAdhocEventLog.Visible = visible;
        btnConvEventLog.Visible = visible;
        btnGetEventLog.Visible = visible;
        btnGetLog.Visible = visible;
        btnGetTraceLog.Visible = visible;
    }

    private void UnAuthorise()
    {
        lblMsg.Text = "You are not authorised.";
        SetButtonVisible(false);
    }

    private void SendFileToClient(string folderName, string fileName)
    {
        MMSoftware.MMWebUtil.WebUtil.SetJavaScriptAtBottom(Page, "open_small_window('SendFile.aspx?fn=" + fileName + "&subDir=" + folderName + "');");
        //MMSoftware.MMWebUtil.WebUtil.SentFileToClient(Response, folderName + fileName, fileName);
    }

    private void SendAllEventLogs(string folderName)
    {
        string[] eventLogFileNames ={
            "iTFlightMgrLog.csv", "iTJobMgrLog.csv",
            "iTreckOk2LLog.csv", "iTrekAuthSvcLog.csv", 
            "iTrekCPMLog.csv", "iTrekDelSessDirsSvcLog.csv",
            "iTrekDeviceLogs.csv", "iTrekErrorLogs.csv",
            "iTrekETDSvcLog.csv", "iTrekGenericLogs.csv",
            "iTrekGetRead_ResLog.csv", "iTrekGlobalLogs.csv",
            "iTrekMsgLog.csv", "iTrekRptLog.csv",
            "iTrekTracesLogs.csv","iTrekLoadAllLog.csv",
            "iTrekOKToLoadLog.csv","iTrekUpdateSessionsSvcLog.csv",
            "Application.csv"};
        if (!folderName.EndsWith(@"\")) folderName += @"\\";
        //foreach( string logFileName in eventLogFileNames)
        for (int i = 0; i < eventLogFileNames.Length; i++)
        {
            string logFileName = eventLogFileNames[i];
            try
            {
                SendFileToClient(folderName, logFileName);
            }
            catch (Exception ex)
            {
               LogMsg("SendAllEventLogs:" + folderName + logFileName + ":Err:" + ex.Message);
            }
        }
    }

    protected void btnGetEventLog_Click(object sender, EventArgs e)
    {
        DateTime dt = WUDate1.GetDateTime();
        string eventFolderName = CtrlHK.GetInstance().GetEventLogFolderName(dt);
        SendAllEventLogs(eventFolderName);
    }

    private void LogMsg(string msg)
    {
        //UtilLog.LogToGenericEventLog("Error", "Web-EventLog:" + msg);
       try
       {
          MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("Web_EventLog", msg);
       }
       catch (Exception)
       {
          UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("Web_EventLog", msg);
       }
    }

    protected void btnGetTraceLog_Click(object sender, EventArgs e)
    {
        DateTime dt = WUDate1.GetDateTime();
        SendFileToClient("", String.Format("Trc_{0:MMdd}.txt", dt));
        SendFileToClient("", String.Format("Web_{0:MMdd}.txt", dt));
    }
    protected void btnGetLog_Click(object sender, EventArgs e)
    {
        DateTime dt = WUDate1.GetDateTime();
        SendFileToClient("", String.Format("Log_{0:MMdd}.txt", dt));
    }
    protected void btnAdhocEventLog_Click(object sender, EventArgs e)
    {
        SendAllEventLogs("EL_MANUAL");
    }
    protected void btnConvEventLog_Click(object sender, EventArgs e)
    {
        string folderName = CtrlHK.GetInstance().GetITrekAdhocEventLog();
        WebUtil.AlertMsg(Page, "Wait for 3 minutes to complete the conversion of EventLog to CSV File before Download the files.");
    }
}
