<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WuMsg.ascx.cs" Inherits="Web_Wu_WuMsg" %>
<%@ Register Src="WuMsgCompose.ascx" TagName="WuMsgCompose" TagPrefix="uc1" %>
<%@ Register Src="WuMsgInBox.ascx" TagName="WuMsgInBox" TagPrefix="uc2" %>
<%@ Register Src="WuMsgOutBox.ascx" TagName="WuMsgOutBox" TagPrefix="uc3" %>
<%@ Register Src="../iTrek/WUMsgDet.ascx" TagName="WUMsgDet" TagPrefix="uc4" %>
<table style="width: 100%; height:100%;">
    <tr>
        <td rowspan="2" style="width: 300px;" valign="top">
            <uc1:WuMsgCompose id="WuMsgCompose1" runat="server" Visible="true">
            </uc1:WuMsgCompose></td>
        <td style="width: 350px;" valign="top">
            <uc2:WuMsgInBox ID="WuMsgInBox1" runat="server" />
        </td>
        <td  style="width: 350px;" valign="top">
            <uc3:WuMsgOutBox ID="WuMsgOutBox1" runat="server" />
        </td>
    </tr>
    <tr>
        <td style="height: 84px;" colspan="2" align="left" valign="top">
<asp:UpdatePanel ID="updPnl1" runat="server">
<ContentTemplate>
            <uc4:WUMsgDet ID="WUMsgDet1" runat="server" />
</ContentTemplate>
</asp:UpdatePanel>
        </td>
    </tr>
</table>
