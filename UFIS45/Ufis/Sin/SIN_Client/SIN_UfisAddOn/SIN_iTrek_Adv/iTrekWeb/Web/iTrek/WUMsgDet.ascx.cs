using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.EventArg;
using UFIS.Web.ITrek.WebMgr;
using UFIS.Web.ITrek.Sec;
using UFIS.ITrekLib.Util;
using UFIS.ITrekLib.Ctrl;
using MM.UtilLib;

public partial class Web_iTrek_WUMsgDet : System.Web.UI.UserControl
{

    protected void Page_Load(object sender, EventArgs e)
    {       
    }

    public void ShowMsg(string msgId, string inOrOutMsgInd)
    {
        try
        {
            this.Visible = true;
            //string msgId = MgrSess.GetCurrentMessageId(Session);
            if (msgId == "")
            {
                lblErrMsg.Text = "No message was selected.";
            }
            else
            {
                MgrSess.SetCurrentMessageId(Session, msgId);
                //string inOrOutMsgInd = Request.Params["INOUT"];
                switch (inOrOutMsgInd)
                {
                    case "I"://Inbox message
                        ShowInMsg(msgId);
                        break;
                    case "O": //Outbox message
                        ShowOutMsg(msgId);
                        break;
                    default:
                        lblErrMsg.Text = "Unidentified message.";
                        break;
                }
            }
        }
        catch (Exception ex)
        {
            LogMsg("ShowMsg:Err:" + ex.Message);
        }
    }

    private void ShowInMsg(string msgId)
    {
        lblSentTo.Text = "";
        lblErrMsg.Text = "";
        try
        {
            DSMsg ds = null;
            ds = CtrlMsg.RetrieveInMessageDetailsForUrno(msgId);
            DSMsg.MSGTORow toRow = ds.MSGTO[0];
            txtMsg.Text = ds.MSGTO[0].MSG_TEXT;
            string sentDt = ds.MSGTO[0].SENT_DT;
            DateTime dt = UtilTime.ConvUfisTimeStringToDateTime(sentDt);
            string sentBy = ds.MSG[0].SENT_BY_NAME;
            if ((sentBy == null) || (sentBy == ""))
                sentBy = ds.MSG[0].SENT_BY_ID;
            lblSentBy.Text = string.Format("Sent By [" + sentBy + "] on {0:dd/MMM/yyyy  HH:mm}", dt);
            if ((toRow.TP == "F") && (!string.IsNullOrEmpty(toRow.TONM)))
            {
                lblSentTo.Text = "Sent To : " + toRow.TONM;
            }
            else
                lblSentTo.Text = "Sent To : " + toRow.MSG_TO_ID;

            //lblSentTo.Text = "Sent To : " + ds.MSGTO[0].MSG_TO_ID;

            string rcvBy = ds.MSGTO[0].RCV_BY;
            string stRcvDt = ds.MSGTO[0].RCV_DT;
            if (stRcvDt == "")
            {
                btnRead.Visible = true;
            }
            else
            {
                btnRead.Visible = false;
                DateTime rcvDt = UtilTime.ConvUfisTimeStringToDateTime(stRcvDt);
                lblSentTo.Text += string.Format(", Received by [" + rcvBy + "] on {0:dd/MMM/yyyy  HH:mm}", rcvDt);
            }
        }
        catch (Exception)
        {
            lblErrMsg.Text = "Error in Retrieving Message.";
        }
        if (lblErrMsg.Text == "") lblErrMsg.Visible = false; 
        else lblErrMsg.Visible = true;
    }

    private void ShowOutMsg(string msgId)
    {
        btnRead.Visible = false;
        DSMsg ds = null;
        try
        {
            ds = CtrlMsg.RetrieveOutMessageDetailsForUrno(msgId);
            if (ds.MSG.Rows.Count > 0)
            {
                txtMsg.Text = ds.MSG[0].MSG_BODY;
                string sentDt = ds.MSG[0].SENT_DT;
                DateTime dt = UtilTime.ConvUfisTimeStringToDateTime(sentDt);
                lblSentBy.Text = string.Format("Sent : " + ds.MSG[0].SENT_BY_NAME + " on {0:dd/MMM/yyyy  HH:mm}", dt);
                lblSentTo.Text = "To : " + ds.GetSentTo(msgId);
            }
        }
        catch (Exception ex)
        {
            lblErrMsg.Text = "Unable to get the detail of the message.";
            LogMsg(ex.Message);
        }
    }

    private void LogMsg(string msg)
    {
        UFIS.ITrekLib.Util.UtilLog.LogToGenericEventLog("WEB-MsgDet", msg);
    }

    protected void btnRead_Click(object sender, EventArgs e)
    {
        try
        {
            DoUpdateRead();
        }
        catch (Exception)
        {
            throw new ApplicationException("Fail to update the information.");
        }

        try
        {
            string msgId = MgrSess.GetCurrentMessageId(Session);
            EaTextEventArgs arg = new EaTextEventArgs(msgId);
            ReadClick(this, arg);
        }
        catch (Exception ex)
        {
            LogMsg("ReadClick:Err:" + ex.Message);
        }
    }

    private void DoUpdateRead()
    {
        //Update to backend to show that this message was read
    }

    /////////////////
    //// add a delegate

    public delegate void ReadClickHandler(object sender,
        EaTextEventArgs e);
    //// add an event of the delegate type

    public event ReadClickHandler ReadClick;

    /////////////////
    //// add a delegate

    public delegate void CloseClickHandler(object sender,
        EaTextEventArgs e);
    //// add an event of the delegate type

    public event CloseClickHandler CloseClick;
    protected void btnClose_Click(object sender, EventArgs e)
    {
        try
        {
            string msgId = MgrSess.GetCurrentMessageId(Session);
            EaTextEventArgs arg = new EaTextEventArgs(msgId);
            CloseClick(this, arg);
        }
        catch (Exception ex)
        {
            LogMsg("CloseClick:Err:" + ex.Message);
        }
    }

}
