<%@ Page Language="C#" MasterPageFile="~/Web/iTrek/MasterPage.master" AutoEventWireup="true" CodeFile="ChgArrBaggageFlightTiming.aspx.cs" Inherits="Web_iTrek_ChgArrBaggageFlightTiming" Title="Change Arrival Flight Baggage Timing" Theme="SATSTheme" %>

<%@ Register Src="WUFlightArrBaggage.ascx" TagName="WUFlightArrBaggage" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" Runat="Server">
    <table style="width: 727px">
        <tr>
            <td style="width: 100px">
                <uc1:WUFlightArrBaggage ID="wuFlightArrBaggage1" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
                <table style="width: 519px">
                    <tr>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 100px">
                            <asp:Button ID="btnApply" runat="server" EnableViewState="False" Text="Apply" OnClick="btnApply_Click" /></td>
                        <td style="width: 100px">
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" EnableViewState="False"
                                Text="Cancel" PostBackUrl="~/Web/iTrek/FlightInfo.aspx?AD=A&AB=B"  /></td>
                        <td style="width: 100px">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

