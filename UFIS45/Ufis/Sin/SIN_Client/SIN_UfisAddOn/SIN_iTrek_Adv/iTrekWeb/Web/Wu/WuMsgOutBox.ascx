<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WuMsgOutBox.ascx.cs" Inherits="Web_Wu_WuMsgOutBox" %>
OutBox
<asp:UpdatePanel ID="updPnl1" runat="server">
<ContentTemplate>
<asp:Panel id="pnlMsg" runat="server" Width="100%" 
EnableViewState="True" Height="100%" ScrollBars="Auto">
<asp:GridView id="gvOutBox" runat="server" BackColor="White" Width="95%" 
EnableViewState="False" AutoGenerateColumns="False" BorderColor="#999999" 
BorderStyle="Solid" BorderWidth="1px" CellPadding="3" DataKeyNames="MSG_URNO" 
ForeColor="Black" GridLines="Vertical" PageSize="50" 
OnRowCommand="gvOutBox_RowCommand">
<FooterStyle BackColor="#CCCCCC"></FooterStyle>
<Columns>
<asp:BoundField DataField="SENT_TIME" HeaderText="Time">
<ItemStyle Width="15px" HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
</asp:BoundField>
<asp:BoundField DataField="D_MSG_TO" HeaderText="Receiver">
<ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
</asp:BoundField>
<asp:ButtonField HeaderText="Text" DataTextField="MSG_SH_TEXT" CommandName="OutDet">
<ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="100px"></ItemStyle>
</asp:ButtonField>
</Columns>

<SelectedRowStyle BackColor="#000099" ForeColor="White" Font-Bold="True"></SelectedRowStyle>

<PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center"></PagerStyle>

<HeaderStyle BackColor="Black" ForeColor="White" Font-Bold="True"></HeaderStyle>

<AlternatingRowStyle BackColor="#CCCCCC"></AlternatingRowStyle>
</asp:GridView></asp:Panel>
</ContentTemplate>
</asp:UpdatePanel>