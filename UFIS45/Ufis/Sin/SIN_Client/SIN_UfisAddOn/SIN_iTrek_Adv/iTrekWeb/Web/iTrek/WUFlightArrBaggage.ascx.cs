//#define TESTING_ON

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.Ctrl;
using UFIS.Web.ITrek.WebMgr;
using UFIS.Web.ITrek.Sec;
using UFIS.Web.ITrek.Misc;
using MMSoftware.MMWebUtil;
using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.Util;
using MM.UtilLib;

public partial class Web_iTrek_WUFlightArrBaggage : System.Web.UI.UserControl
{
    const string DEF_HDR = "Arrival Flight - Baggage";
    private const string BO_RPT_PG_NAME = "BaggagePresentTiming_Arr";
    private const string BO_RPT_CTRL_CREATE = "Create";
    private const string BO_RPT_CTRL_EDIT = "Edit";

    const int FB_CELL_NO = 12;
    const int LB_CELL_NO = 13;
    //const int PF_CELL_NO = 14;
    //const int CELL_NO_BO_RPT = 16;
    const int PF_FB_CELL_NO = 14;
    const int PF_LB_CELL_NO = 15;
    const int CELL_NO_BO_RPT = 17;


    private bool _isGridHeaderFreeze = false;

    public bool IsGridHeaderFreeze
    {
        get { return _isGridHeaderFreeze; }
        set { _isGridHeaderFreeze = value; }
    }


    private void FreezeGridHeader()
    {
        if (IsGridHeaderFreeze)
        {
            MiscRender.FreezeGridHeader(this.Page, this.GetType(),
                gvFlight.ClientID, "WrapperDivArrBag",
                hfScrollTop.ClientID, hfScrollTop.Value);
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        RetrieveUserInformation();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //RetrieveUserInformation();
    }

    private EntUser user = null;
    private string _userId = "";

    private void RetrieveUserInformation()
    {
        try
        {
            if (user == null)
            {
                user = MgrSess.GetUserInfo(Session);
            }
            _userId = user.UserId;
        }
        catch (Exception)
        {
        }
    }

    //public void ShowGrid(bool show)
    //{
    //    try
    //    {
    //        gvFlight.Visible = show;
    //        if (show == true)
    //        {
    //            DSFlight ds;
    //            ds = CtrlFlight.RetrieveFlights("", DateTime.Now, DateTime.Now);
    //            gvFlight.DataSource = ds;
    //            gvFlight.DataBind();
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        LogMsg("ShowGrid:Err:" + ex.Message);
    //    }
    //}

    public void Show(DataView dv)
    {
        try
        {
            dv.Sort = "FLIGHT_DT ASC";
            gvFlight.DataSource = dv;
            gvFlight.DataBind();
            FreezeGridHeader();
        }
        catch (Exception ex)
        {
            LogMsg("Show:Err:" + ex.Message);
            WebUtil.AlertMsg(Page, ex.Message);
        }
    }

    public void Show(DSFlight ds, string terminalId)
    {
        try
        {
            DataView dv = ds.FLIGHT.DefaultView;
            string stFilter = "A_D='A'";
            if (terminalId == null) terminalId = CtrlTerminal.GetDefaultTerminalId();
            else if (terminalId == "") terminalId = CtrlTerminal.GetDefaultTerminalId();

            if (CtrlTerminal.GetDefaultTerminalId() != terminalId)
            {
                stFilter += " AND TERMINAL='" + terminalId + "'";
            }
            lblHdr.Text = DEF_HDR + " (" + CtrlTerminal.GetTerminalName(terminalId) + ")";

            dv.RowFilter = stFilter;
            Show(dv);
        }
        catch (Exception ex)
        {
            LogMsg("ShowDsT:Err:" + ex.Message);
        }
    }

    public void Show(DSFlight ds, EntWebFlightFilter entFilter)
    {
        try
        {
            DataView dv = ds.FLIGHT.DefaultView;
            string stFilter = "A_D='A'";
            //lblHdr.Text = DEF_HDR + " [" + entFilter.FilterString + "]";
            lblHdr.Text = " [ " + entFilter.FilterString + " ]";

            dv.RowFilter = stFilter;
            Show(dv);
        }
        catch (Exception ex)
        {
            LogMsg("ShowDsT:Err:" + ex.Message);
        }
    }



    public void PopulateGridForAFlight(string urNo, string flightNo, bool editMode)
    {
        try
        {
            if (editMode)
            {
                gvFlight.EditIndex = 0;
                MgrView.SetFlightNoEdit(ViewState, flightNo);
                MgrView.SetModeEdit(ViewState, editMode);
                MgrView.SetUrNoEdit(ViewState, urNo);
            }
            PopulateGridForAFlight(urNo, flightNo);
        }
        catch (Exception ex)
        {
            LogMsg("PopulateGridForAFlight:Err:" + ex.Message);
        }
    }

    public void PopulateGridForAFlight(string urNo, string flightNo)
    {
        try
        {
            DSFlight ds = null;

            ds = CtrlFlight.RetrieveFlightsByFlightId(urNo);

            DataView dv = ds.FLIGHT.DefaultView;
            //dv.RowFilter = "FLNO='" + flightNo + "' AND URNO='" + urNo + "'";
            dv.RowFilter = "URNO='" + urNo + "' AND FLNO='" + flightNo + "'" ;

            Show(dv);
        }
        catch (Exception ex)
        {
            LogMsg("PopulateGridForAFlight:Err:" + ex.Message);
        }
    }

    protected void gvFlight_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                MiscRender.HighLightTransit(e.Row, 1); 
                ShowPassFail(e.Row);
                //ShowTiming(e.Row);
                LinkServiceReport(e.Row);
            }
        }
        catch (Exception ex)
        {
            LogMsg("RowDataBound:Err:" + ex.Message);
        }
    }


    private void ShowTiming(GridViewRow row)
    {
        try
        {
            string atTime = DataBinder.Eval(row.DataItem, "AT").ToString().Trim();
            if (atTime != "")
            {
                DateTime dtATA = UtilTime.ConvUfisTimeStringToDateTime(atTime);
                try
                {
                    string fbTime = DataBinder.Eval(row.DataItem, "FST_BAG").ToString().Trim();
                    if (fbTime != "")
                    {
                        DateTime dtFirstBag = UtilTime.ConvUfisTimeStringToDateTime(fbTime);
                        string fbTiming = UtilTime.TimeDiff(dtFirstBag, dtATA, "M").ToString();
                        row.Cells[FB_CELL_NO].Text = fbTiming;
                    }
                }
                catch (Exception)
                {
                }
                try
                {
                    string lbTime = DataBinder.Eval(row.DataItem, "LST_BAG").ToString().Trim();
                    if (lbTime != "")
                    {
                        DateTime dtLastBag = UtilTime.ConvUfisTimeStringToDateTime(lbTime);
                        string lbTiming = UtilTime.TimeDiff(dtLastBag, dtATA, "M").ToString();
                        row.Cells[LB_CELL_NO].Text = lbTiming;
                    }
                }
                catch (Exception)
                {
                }
            }
        }
        catch (Exception ex)
        {
            LogMsg("ShowTiming:Err:" + ex.Message);
        }
    }

    //private void ShowPassFailWithOneColumn(GridViewRow row)
    //{
    //    try
    //    {
    //        //***Show Pass or Fail.
    //        string flNo = DataBinder.Eval(row.DataItem, "FLNO").ToString().Trim();
    //        string urNo = DataBinder.Eval(row.DataItem, "URNO").ToString().Trim();
    //        //string stFPF = Convert.ToString(DataBinder.Eval(row.DataItem, "BG_FST_BM_MET")).Trim().ToUpper();
    //        //string stLPF = Convert.ToString(DataBinder.Eval(row.DataItem, "BG_LST_BM_MET")).Trim().ToUpper();
    //        string stPF = "";

    //        //if ((stFPF != "") && (stLPF != ""))
    //        //{
    //        //    if ((stFPF == "P") && (stLPF == "P"))
    //        //    {
    //        //        stPF = "P";
    //        //    }
    //        //    else { stPF = "F"; }
    //        //}
    //        stPF = Convert.ToString(DataBinder.Eval(row.DataItem, "BG_PF")).Trim().ToUpper();

    //        //((HyperLink)(row.Cells[PF_CELL_NO]).Controls[0]).NavigateUrl = "~/Web/iTrek/BMPassFail.aspx?FLNO="+ flNo + "&URNO=" + urNo + "&AD=A&AB=B";
    //        //((HyperLink)(row.Cells[PF_CELL_NO]).Controls[0]).Text = stPF;
    //        ((LinkButton)(row.Cells[PF_CELL_NO].FindControl("lbBMPassFail"))).Text = stPF;
    //    }
    //    catch (Exception ex)
    //    {
    //        LogMsg("PF:Err:" + ex.Message);
    //    }

    //}

    private void ShowPassFail(GridViewRow row)
    {
        try
        {
            //***Show Pass or Fail.
            //string flNo = DataBinder.Eval(row.DataItem, "FLNO").ToString().Trim();
            //string urNo = DataBinder.Eval(row.DataItem, "URNO").ToString().Trim();
            string stFPF = Convert.ToString(DataBinder.Eval(row.DataItem, "BG_FST_BM_MET")).Trim().ToUpper();
            string stLPF = Convert.ToString(DataBinder.Eval(row.DataItem, "BG_LST_BM_MET")).Trim().ToUpper();

            ((LinkButton)(row.Cells[PF_FB_CELL_NO].FindControl("lbBMPassFailFBag"))).Text = stFPF;
            ((LinkButton)(row.Cells[PF_LB_CELL_NO].FindControl("lbBMPassFailLBag"))).Text = stLPF;
        }
        catch (Exception ex)
        {
            LogMsg("PF:Err:" + ex.Message);
        }
    }


    private void LinkServiceReport(GridViewRow row)
    {
        try
        {
            //***Show Link for Service Report.
            CtrlSec ctrlSec = CtrlSec.GetInstance();
            string st = "";
            //string msg = "";
            RetrieveUserInformation();
            //EntUser user = MgrSess.GetUserInfo(Session);
            //_userId = user.UserId;
            if (HasBOReport(row))
            {
                if (ctrlSec.HasAccessRights(user, BO_RPT_PG_NAME, BO_RPT_CTRL_EDIT))
                {
                    st = "EDIT";
                }
                else
                {
                    st = "VIEW";
                }
            }
            else
            {
                if (ctrlSec.HasAccessRights(user, BO_RPT_PG_NAME, BO_RPT_CTRL_CREATE))
                {
                    st = "CREATE";
                }
            }
            //#if TESTING_ON
            //        user.Role = EntUser.ROLE_BO;
            //#endif 
            //        try
            //        {
            //            if (HasBOReport(row))
            //            {
            //                if (user.Role == EntUser.ROLE_BAG_DM )
            //                {
            //                    st = "EDIT";
            //                }
            //                else
            //                {
            //                    st = "VIEW";
            //                }
            //            }
            //            else
            //            {
            //                if (user.Role == EntUser.ROLE_BO)
            //                {
            //                    st = "CREATE";
            //                }
            //            }
            //        }
            //        catch (ApplicationException ex)
            //        {
            //            msg = ex.Message;
            //        }
            //        catch (Exception)
            //        {
            //        }

            //((HyperLink)(row.Cells[CELL_NO_AO_RPT]).Controls[0]).NavigateUrl = "~/Web/iTrek/ApronServiceReport.aspx?flNo=123";
            ((HyperLink)(row.Cells[CELL_NO_BO_RPT]).Controls[0]).Text = st;
        }
        catch (Exception ex)
        {
            LogMsg("LindSvcRpt:Err:" + ex.Message);
        }
    }

    private bool HasBOReport(GridViewRow row)
    {
        bool exist = false;
        try
        {
            string st = Convert.ToString(DataBinder.Eval(row.DataItem, "HAS_BO_RPT")).Trim().ToUpper();
            if (st == "Y") exist = true;
            else
            {
#warning //TOD0 - To update the Indicator to show that Apron Service Report is existed.
//                try
//                {
//                    string flightUrno = Convert.ToString(DataBinder.Eval(row.DataItem, "URNO")).Trim().ToUpper();
//                    if (CtrlBagPresentTimingReport.GetInstance().IsBagPresentTimingReportExist(flightUrno))
//                    {
//                        exist = true;
//#warning //TOD0 - To update the Indicator to show that Apron Service Report is existed.
//                        //CtrlFlight.GetInstance().UpdateBagPreTimeReportExist(flightUrno, exist);
//                    }
//                }
//                catch (Exception)
//                {
//                }
            }
        }
        catch (Exception)
        {
        }
        return exist;
    }

    public void SaveEditInfo()
    {
        try
        {
            if (MgrView.GetModeEdit(ViewState))
            {//Edit Mode
                string stURNO = MgrView.GetUrNoEdit(ViewState);
                String stFlightNo = MgrView.GetFlightNoEdit(ViewState);
                string flightDateTime = CtrlFlight.GetFlightDateTime(stURNO);

                string stFirstTrip = ConvTime(0, "txtNewFirstTrip", flightDateTime);
                string stLastTrip = ConvTime(0, "txtNewLastTrip", flightDateTime);
                string stFirstBag = ConvTime(0, "txtNewFirstBag", flightDateTime);
                string stLastBag = ConvTime(0, "txtNewLastBag", flightDateTime);
                CtrlFlight.SaveArrBagTimingInfo(stURNO, stFlightNo, stFirstTrip, stLastTrip, stFirstBag, stLastBag,_userId);
                //WebUtil.AlertMsg(Page, stURNO + "\n" + stFlightNo + "\n" + stFirstTrip + "\n" + stLastTrip + "\n" + stFirstBag);
            }
            else
            {
                WebUtil.AlertMsg(Page, "Unable to save. It is not in Edit Mode.");
            }
        }
        catch (ApplicationException ex)
        {
            throw new ApplicationException(ex.Message);
        }
        catch (Exception ex)
        {
            LogMsg("SaveEditInfo:Err:" + ex.Message);
            throw new ApplicationException("Fail to Save.");
        }
    }

    private string ConvTime(int rowNum, string txtBoxName, string refUfisDt)
    {
        string stDt = "";
        try
        {
            string stTime = GetTextBoxData(rowNum, txtBoxName);
            if (stTime != "")
            {
                if (UtilTime.IsValidHHmm(stTime))
                {
                    stDt = UtilTime.ConvHHmmToUfisTimeAfterRefTime(refUfisDt, stTime);
                }
                else
                {
                    throw new ApplicationException("Invalid Time Format.");
                }
            }
        }
        catch (ApplicationException ex)
        {
            throw new ApplicationException(ex.Message);
        }
        catch (Exception)
        {
        }
        return stDt;
    }

    private string GetTextBoxData(int rowNum, string txtBoxName)
    {
        string st = "";
        try
        {
            st = ((TextBox)gvFlight.Rows[rowNum].FindControl(txtBoxName)).Text;
        }
        catch (Exception)
        {
        }
        return st;
    }

    private void LogMsg(string msg)
    {
        UFIS.ITrekLib.Util.UtilLog.LogToGenericEventLog("WEBWUArrBg", msg);
    }
}
