using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.ENT;
using UFIS.Web.ITrek.Sec;
using iTrekSessionMgr;
using UFIS.Web.ITrek.WebMgr;
using MMSoftware.MMWebUtil;
using UFIS.ITrekLib.Util;
using MM.UtilLib;

public partial class Web_iTrek_Message : System.Web.UI.Page
{
    private string _userId = null;
    private ArrayList _groupList = null;
    private const string PG_NAME = "Message";
    private const string CTRL_VIEW = "Page";
    private const int MAX_MSG_LENGTH = 256;
    private const int MAX_INBOX_SIZE = 50;
    private const int MAX_OUTBOX_SIZE = 50;

    protected void Page_Load(object sender, EventArgs e)
    {
        ((MyMasterPage)Master).AutoRefreshMode = false;
        ((MyMasterPage)Master).GetScriptManager().RegisterPostBackControl(gvInBox);
        ((MyMasterPage)Master).GetScriptManager().RegisterPostBackControl(gvOutBox);
        Timer1.Interval = CtrlConfig.GetWebRefreshTiming() * 1000;
        //((MyMasterPage)Master).GetScriptManager().RegisterPostBackControl(ddPredefMsgs);

        CtrlSec ctrlsec = CtrlSec.GetInstance();
        EntUser user = ctrlsec.AuthenticateUser(Session, Response);
        wuMsgDet1.CloseClick += new Web_iTrek_WUMsgDet.CloseClickHandler(wuMsgDet1_CloseClick);
        wuMsgDet1.ReadClick += new Web_iTrek_WUMsgDet.ReadClickHandler(wuMsgDet1_ReadClick);
        if (ctrlsec.HasAccessRights(user, PG_NAME, CTRL_VIEW))
        {
            _userId = user.UserId;
            _groupList = user.GetGroupList();
            //Get User Id and Group Codes.
            if (!IsPostBack)
            {
                ShowInfo();
            }
            else
            {
                NormalRefreshInfo();
            }
            //ShowInfo();
        }
        else
        {
            DoUnAuthorise();
        }
    }

    void wuMsgDet1_ReadClick(object sender, UFIS.ITrekLib.EventArg.EaTextEventArgs e)
    {
        string msgId = e.Text;
        string staffId = "";
        string recvUfisDt = "";
        staffId = _userId;
        recvUfisDt = UtilTime.ConvDateTimeToUfisFullDTString( System.DateTime.Now);
        CtrlMsg.UpdateMessageReceivedDetails(msgId, staffId, recvUfisDt);
        wuMsgDet1.Visible = false;
        NormalRefreshInfo();
    }

    private void NormalRefreshInfo()
    {
        try
        {
            RefreshInfo();
        }
        catch (ApplicationException ex)
        {
            WebUtil.AlertMsg(Page, ex.Message);
        }
        catch (Exception)
        {
        }
    }

    void wuMsgDet1_CloseClick(object sender, UFIS.ITrekLib.EventArg.EaTextEventArgs e)
    {
        wuMsgDet1.Visible = false;
        NormalRefreshInfo();
    }

    private void DoUnAuthorise()
    {
        Response.Redirect("~/UnAuthorised.aspx", true);
    }

    private void ShowInfo()
    {
        try
        {
            ShowInBox();
            ShowOutBox();
            ShowPredefMsgs();
            ShowSendTo();
        }
        catch (Exception)
        {
        }
    }

    private void ShowInBox()
    {
        try
        {
            DSMsg dsInMsg;
            //dsInMsg = CtrlMsg.RetrieveInMessages(_userId);
            dsInMsg = CtrlMsg.RetrieveInMessages(_userId, _groupList, MAX_INBOX_SIZE);
            DataView dv = dsInMsg.MSGTO.DefaultView;
            dv.Sort = "SENT_DT DESC, URNO";
            gvInBox.DataSource = dv;
            gvInBox.DataBind();
        }
        catch (Exception ex)
        {
            LogMsg("ShowInBox:Err:" + ex.Message);
            //WebUtil.AlertMsg(Page, ex.Message);
        }
    }

    private void ShowOutBox()
    {
        try
        {
            DSMsg dsOutMsg = CtrlMsg.RetrieveOutMessages(_userId, MAX_OUTBOX_SIZE);
            DataView dv = dsOutMsg.MSGTO.DefaultView;
            dv.Sort = "SENT_DT DESC, URNO";
            gvOutBox.DataSource = dv;
            gvOutBox.DataBind();
        }
        catch (Exception ex)
        {
            LogMsg("ShowOutBox:Err:" + ex.Message);
            //WebUtil.AlertMsg(Page, ex.Message);
        }
    }

    private void ShowPredefMsgs()
    {
        try
        {
            DSPreDefMsg dsPreDefMsg;
            dsPreDefMsg = CtrlPreDefMsg.RetrieveActivePredifinedMsg();
            DSPreDefMsg.PREDEFMSGRow row = dsPreDefMsg.PREDEFMSG.NewPREDEFMSGRow();
            row.URNO = "NONE";
            
            dsPreDefMsg.PREDEFMSG.AddPREDEFMSGRow(row);
            DataView dv = dsPreDefMsg.PREDEFMSG.DefaultView;
            dv.Sort = "MSGTEXT";
            ddPredefMsgs.DataSource = dv;
            ddPredefMsgs.DataBind();
        }
        catch (Exception ex)
        {
            LogMsg("ShowPredefMsgs:Err:" + ex.Message);
        }
    }

    private void ShowSendTo()
    {
        DSCurUser ds = CtrlApp.GetUsersToSendMsgInWeb();
        DataView dv = ds.LOGINUSER.DefaultView;
        dv.Sort = "LGTYPE DESC, NAME";
        cblSendTo.DataSource = dv;
        cblSendTo.DataTextField = "NAME";
        cblSendTo.DataValueField = "UID";
        cblSendTo.DataBind();
    }

    protected void gvInBox_SelectedIndexChanged(object sender, EventArgs e)
    {
        string st = e.ToString();
    }
    protected void gvOutBox_SelectedIndexChanged(object sender, EventArgs e)
    {
        string st = e.ToString();
    }
    protected void gvInBox_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string st = e.CommandName;
        if (st == "InDet")
        {
            int idx = Convert.ToInt32(e.CommandArgument.ToString());
            string msgId = ((GridView)sender).DataKeys[idx].Value.ToString();
            string st1 = sender.ToString();
            wuMsgDet1.ShowMsg(msgId, "I");//Show incoming message
            wuMsgDet1.Visible = true;
        }
    }
    protected void btnSendMessage_Click(object sender, EventArgs e)
    {
        ArrayList recList = new ArrayList();
        int cnt = cblSendTo.Items.Count;
        for (int i = 0; i < cnt; i++)
        {
            if (cblSendTo.Items[i].Selected)
            {
                recList.Add(cblSendTo.Items[i].Value);
            }
        }
        if (recList.Count < 1)
            WebUtil.AlertMsg(Page,"Please choose receipient(s).");
        else
            SendMessage(recList);
        NormalRefreshInfo();
    }

    private void SendMessage(ArrayList recList)
    {
        string userId = "";
        string userName = "";
        string msgText = "";
        EntUser user = MgrSess.GetUserInfo(Session);
        userId = user.UserId;
        //userName = user.FirstName + " " + user.LastName;
        userName = user.FirstName + "";
        userName = userName.Trim();
        if (userName == "")
        {
            userName = user.LastName;
        }

        if (txtComposeMessage.Text != "")
        {
            msgText += txtComposeMessage.Text;
        }
        //if (ddPredefMsgs.SelectedItem.Text != "")
        //{
        //    string dot = "";
        //    if ((msgText.Length > 0) && (msgText[msgText.Length - 1] != '.')) { dot = ". "; }
        //    msgText += dot + ddPredefMsgs.SelectedItem.Text;
        //}
        msgText = msgText.Trim();
        if (msgText.Length < 1)
        {
            WebUtil.AlertMsg(Page, "No message text to send.");
        }
        else if (msgText.Length > 256)
        {
            msgText = msgText.Substring(0, MAX_MSG_LENGTH);
            WebUtil.AlertMsg(Page, "Message Length exceeds the maximum length " + MAX_MSG_LENGTH +
                ". ");
        }
        else
        {
            CtrlMsg.SendMessage(recList, userId, userName, msgText);
			//System.Threading.Thread.Sleep(1500);
        }
    }
    protected void btnSendToAll_Click(object sender, EventArgs e)
    {
        ArrayList recList = new ArrayList();
        int cnt = cblSendTo.Items.Count;
        for (int i = 0; i < cnt; i++)
        {
            recList.Add(cblSendTo.Items[i].Value);
        }
        SendMessage(recList);
    }

    protected void gvOutBox_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string st = e.CommandName;
        if (st == "OutDet")
        {
            int idx = Convert.ToInt32(e.CommandArgument.ToString());
            string msgId = ((GridView)sender).DataKeys[idx].Value.ToString();
            string st1 = sender.ToString();
            wuMsgDet1.ShowMsg(msgId, "O");//Show outgoing message
            wuMsgDet1.Visible = true;
        }
    }



    protected void Timer1_Tick(object sender, EventArgs e)
    {
        try
        {
            RefreshInfo();
        }
        catch (ApplicationException ex)
        {
            LogMsg("Timer1_Tick:Err:" + ex.Message);
            throw new ApplicationException("Auto Refresh Error due to : " + ex.Message + ".");
        }
        catch (Exception ex)
        {
            LogMsg("Timer1_Tick:Err:" + ex.Message);
            throw new ApplicationException("Auto Refresh Error due to : " + ex.Message);
        }
    }

    private void LogMsg(string msg)
    {
        UtilLog.LogToGenericEventLog("WEB-Message", msg);
    }

    private void ShowSendToWithExistingSelectItem()
    {//Show new 'Send to' list
        ArrayList recList = new ArrayList();//keep the current selected items
        int cnt = cblSendTo.Items.Count;
        for (int i = 0; i < cnt; i++)
        {
            if (cblSendTo.Items[i].Selected)
            {
                recList.Add(cblSendTo.Items[i].Value);
            }
        }
        
        //Get new 'send to' list
        DSCurUser ds = CtrlApp.GetUsersToSendMsgInWeb();

        DataView dv = ds.LOGINUSER.DefaultView;
        dv.Sort = "LGTYPE DESC, NAME";
        cblSendTo.DataSource = dv;
        cblSendTo.DataTextField = "NAME";
        cblSendTo.DataValueField = "UID";
        cblSendTo.DataBind();

        cnt = cblSendTo.Items.Count;
        for (int i = 0; i < cnt; i++)
        {//select the previous selected items in new list
            if (recList.Contains(cblSendTo.Items[i].Value))
            {
                cblSendTo.Items[i].Selected = true;
            }
        }
    }

    private void RefreshInfo()
    {
        ShowSendToWithExistingSelectItem();
        ShowInBox();
        ShowOutBox();
        ((MyMasterPage)Master).RefreshInfo();
    }
    protected void wuMsgDet1_Load(object sender, EventArgs e)
    {

    }
    protected void ddPredefMsgs_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList dd = (DropDownList)sender;
        txtComposeMessage.Text = dd.SelectedItem.Text;
        //string st = sender.ToString();
        //st += "==>" + e.ToString();
        //txtComposeMessage.Text = "test" + st;
    }

    protected void gvInBox_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            bool bold = false;
            string rcv = DataBinder.Eval(e.Row.DataItem, "RCV_DT").ToString().Trim();
            if (rcv == "")
            {//Not read message
                bold = true;
            }

            for (int i = 0; i < 3; i++)
            {
                e.Row.Cells[i].Font.Bold = bold;
            }
        }
    }
}
