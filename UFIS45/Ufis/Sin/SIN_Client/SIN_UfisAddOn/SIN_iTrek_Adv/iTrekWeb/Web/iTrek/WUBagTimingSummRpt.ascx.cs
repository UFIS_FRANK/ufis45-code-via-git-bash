using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;

using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.ENT.BPTRSumm;
using UFIS.ITrekLib.Util;
using MM.UtilLib;

public partial class Web_iTrek_WUBagTimingSummRpt : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    public void ShowReport(string terminalId, string terminalName,
        string frDate, string frTime,
        string toDate, string toTime, string userId)
    {
		try
		{
			DateTime dtFrom;
			DateTime dtTo;
			if (terminalId == CtrlTerminal.GetAllTerminalId()) terminalId = "";

			try
			{
				dtFrom = UtilTime.ConvStringToDate(frDate.Trim(), frTime.Trim());
			}
			catch (Exception)
			{
				throw new ApplicationException("Invalid From Date Time.");
			}
			try
			{
				dtTo = UtilTime.ConvStringToDate(toDate.Trim(), toTime.Trim());
			}
			catch (Exception)
			{
				throw new ApplicationException("Invalid To Date Time.");
			}

			string frUfisDateTime = UtilTime.ConvDateTimeToUfisFullDTString(dtFrom);
			string toUfisDateTime = UtilTime.ConvDateTimeToUfisFullDTString(dtTo);
			//ShowReportUsingForntEndData(terminalId, terminalName, dtFrom, dtTo);

			ShowReportUsingBackendData(terminalId, terminalName, frUfisDateTime, toUfisDateTime, userId);

		}
		catch (ApplicationException ex)
		{
			LogMsg("ShowReport:Err:" + ex.Message);
			throw;
		}
		catch (Exception ex)
		{
			LogMsg("ShowReport:Err:" + ex.Message);

		}
    }

    private void ShowReportUsingForntEndData(string terminalId, string terminalName, DateTime dtFrom, DateTime dtTo)
    {

        try
        {
            DSFlight dsFlight = CtrlFlight.RetrieveArrivalFlights(terminalId, dtFrom, dtTo);
            int totFlightNo = dsFlight.FLIGHT.Rows.Count;
            if (totFlightNo < 1) throw new ApplicationException("No flight was selected.");
            int lightLoader = 0;
            int heavyLoader = 0;
            int fbMet = 0;
            int lbMetLightLoader = 0;
            int lbMetHeavyLoader = 0;
            for (int i = 0; i < totFlightNo; i++)
            {
                DSFlight.FLIGHTRow row = (DSFlight.FLIGHTRow)dsFlight.FLIGHT.Rows[i];
                string lh = "";
                try
                {
                    lh = row.L_H;
                }
                catch (Exception)
                {
                }

                switch (lh)
                {
                    case "L":
                    case "l":
                        lightLoader++;
                        if (row.BG_LST_BM_MET.Trim().ToUpper() == "P") lbMetLightLoader++;
                        break;
                    case "H":
                    case "h":
                        heavyLoader++;
                        if (row.BG_LST_BM_MET.Trim().ToUpper() == "P") lbMetHeavyLoader++;
                        break;
                    default:
                        totFlightNo--;
                        break;
                }

                try
                {
                    if (row.BG_FST_BM_MET.Trim().ToUpper() == "P") fbMet++;
                }
                catch (Exception)
                {
                }

            }
            if (totFlightNo < 1) throw new ApplicationException("No flight was selected.");

            DataTable dt = new DataTable();
            //DataColumn col = new DataColumn();
            //col.ColumnName = "Title";
            dt.Columns.Add("REPORT STATISTIC");
            dt.Columns.Add("NUMBER");
            dt.Columns.Add("PERCENTAGE");
            dt.Rows.Add("Total Flights", totFlightNo, "N/A");
            dt.Rows.Add("Number of Light Loader", lightLoader, "N/A");
            dt.Rows.Add("Number of Heavy Loader", heavyLoader, "N/A");
            AddAnRow(dt, "Absolute First Bags Meeting BPT",
                fbMet, totFlightNo);
            AddAnRow(dt, "Absolute Last Bags Meeting BPT - Light Loader",
                    lbMetLightLoader, lightLoader);
            AddAnRow(dt, "Absolute Last Bags Meeting BPT - Light Loader",
                    lbMetHeavyLoader, heavyLoader);
            AddAnRow(dt, "Absolute Last Bags Meeting BPT - Total",
                (lbMetHeavyLoader + lbMetLightLoader), totFlightNo);

            gvReport.DataSource = dt;
            gvReport.DataBind();

            //showing label
            //lblFrDate.Text = frDate;
            //lblFrTime.Text = frTime;
            //lblTerminal.Text = terminalName;
            //lblToDate.Text = toDate;
            //lblToTime.Text = toTime;
            lblFrDate.Text = string.Format("{0:dd/MMM/yyyy}", dtFrom);
            lblFrTime.Text = string.Format("{0:HH:mm}", dtFrom);
            lblTerminal.Text = terminalName;
            lblToDate.Text = string.Format("{0:dd/MMM/yyyy}", dtTo);
            lblToTime.Text = string.Format("{0:HH:mm}", dtTo);
            pnl1.Visible = true;

        }
        catch (Exception ex)
        {
            LogMsg("ShowReportUsingForntEndData:Err:" + ex.Message);
        }
    }


    public void ShowReportUsingBackendData(string terminalId, string terminalName,
    string frUfisDateTime,
    string toUfisDateTime,
		string userId)
    {
        try
        {
            if (terminalId == CtrlTerminal.GetAllTerminalId()) terminalId = "";

			//gvReport.DataSource = CtrlBagPresentTimeSummaryReport.RetrieveReport(
			//    Session.SessionID, terminalId, frUfisDateTime, toUfisDateTime);
			//gvReport.DataBind();

		    List<string> lsTerminalIds = new List<string>();
			lsTerminalIds.Add( terminalId );
			EntBptrSummCond cond = null;
			DataTable dt = CtrlBptr.RetrieveBptrSumm(
				lsTerminalIds, frUfisDateTime, toUfisDateTime, 
				userId,
				out cond);

			#region Showing Search Criteria
			DateTime dtFrom = UtilTime.ConvUfisTimeStringToDateTime(frUfisDateTime);
			DateTime dtTo = UtilTime.ConvUfisTimeStringToDateTime(toUfisDateTime);

			dtFrom = cond.FromDateTime;
			dtTo = cond.ToDateTime;

			lblFrDate.Text = string.Format("{0:dd/MMM/yyyy}", dtFrom);
            lblFrTime.Text = string.Format("{0:HH:mm}", dtFrom);
            lblTerminal.Text = terminalName;
            lblToDate.Text = string.Format("{0:dd/MMM/yyyy}", dtTo);
            lblToTime.Text = string.Format("{0:HH:mm}", dtTo);
            pnl1.Visible = true;
			#endregion

			gvReport.DataSource = dt;
			gvReport.DataBind();

		}
        catch (Exception ex)
        {
            LogMsg("ShowReportUsingBackendData:Err:" + ex.Message);
        }
    }

    private void AddAnRow(DataTable dt, string lbl, Int64 num, Int64 totNum)
    {
        if (totNum > 0)
        {
            dt.Rows.Add(lbl, num, string.Format("{0:#0.00}%", num * 100 / totNum));
        }
        else
        {
            dt.Rows.Add(lbl, num, "-");
        }
    }

    protected void gvReport_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvReport.PageIndex = e.NewPageIndex;
    }

	#region Logging
	private static void LogTraceMsg(string msg)
	{
		LogMsg(msg);
	}

	private static void LogMsg(string msg)
	{
		try
		{
			MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("WUBagTSR", msg);
		}
		catch (Exception)
		{
			UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("WUBagTSR", msg);
		}
	}
	#endregion
}
