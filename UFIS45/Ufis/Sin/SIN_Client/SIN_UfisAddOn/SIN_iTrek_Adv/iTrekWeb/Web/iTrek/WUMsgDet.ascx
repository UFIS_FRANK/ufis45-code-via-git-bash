<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WUMsgDet.ascx.cs" Inherits="Web_iTrek_WUMsgDet" %>
<table style="width: 99%">
    <tr>
        <td>
            <asp:Label ID="lblErrMsg" runat="server" Width="99%" Visible="False"></asp:Label>
            <asp:Label ID="lblSentBy" runat="server" Width="99%"></asp:Label>
            <br />
            <asp:Label ID="lblSentTo" runat="server" Width="99%"></asp:Label>&nbsp;
        </td>
    </tr>
    <tr>
        <td >
            <asp:TextBox ID="txtMsg" runat="server" TextMode="MultiLine" Width="99%" ReadOnly="True"></asp:TextBox></td>
    </tr>
    <tr>
        <td style="height: 20px;">
            <table style="width: 400px">
                <tr>
                    <td style="width: 100px">
                    </td>
                    <td style="width: 100px">
                        <asp:Button ID="btnRead" runat="server" OnClick="btnRead_Click" Text="Confirm Read"
                            Visible="False" EnableViewState="False" /></td>
                    <td style="width: 100px" align="center">
                        <asp:Button ID="btnClose" runat="server" OnClick="btnClose_Click" Text="Close" Width="77px" CausesValidation="False" EnableViewState="False" /></td>
                    <td >
                        </td>
                    <td>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>