<%@ Page Language="C#" MasterPageFile="MasterPage.master" AutoEventWireup="true" CodeFile="ChgArrApronFlightTiming.aspx.cs" Inherits="Web_iTrek_ChgArrApronFlightTiming" Title="Change Flight Timing" Theme="SATSTheme" %>

<%@ Register Src="WUFlightArrApron.ascx" TagName="WUFlightArrApron" TagPrefix="uc2" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="atlas" %>

<%@ Register Src="WUAFlight.ascx" TagName="WUAFlight" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" Runat="Server">
    <table style="width: 970px">
        <tr>
            <td style="height: 346px">
                &nbsp;
                <uc2:WUFlightArrApron id="wuFlightArrApron1" runat="server" IsGridHeaderFreeze="false">
                </uc2:WUFlightArrApron></td>
        </tr>
        <tr>
            <td>
                <table style="width: 301px">
                    <tr>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 100px">
                        
                            <asp:Button ID="btnApply" runat="server" Text="Apply" OnClick="btnApply_Click" EnableViewState="False" /></td>
                        <td style="width: 100px">
                            <input id="hbtnCancel" type="button" value="Cancel" onclick="javascript:GotoThePage('FlightInfo.aspx?AD=A&AB=A');"/></td>
                    </tr>
                </table>


            </td>
        </tr>
    </table>
</asp:Content>

