<%@ Page Language="C#" MasterPageFile="~/Web/iTrek/MasterPage.master" AutoEventWireup="true" CodeFile="ChgDepApronFlightTiming.aspx.cs" Inherits="Web_iTrek_ChgDepApronFlightTiming" Title="Change Departure Flight Apron Timing" Theme="SATSTheme" %>

<%@ Register Src="WUFlightDepApron.ascx" TagName="WUFlightDepApron" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" Runat="Server">
    <table style="width: 719px">
        <tr>
            <td style="width: 100px">
                <uc1:WUFlightDepApron ID="wuFlightDepApron1" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="width: 100px; height: 51px;">
                <table style="width: 641px">
                    <tr>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 100px">
                            <asp:Button ID="btnApply" runat="server" OnClick="btnApply_Click" Text="Apply" EnableViewState="False" /></td>
                        <td style="width: 100px">
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CausesValidation="False" PostBackUrl="~/Web/iTrek/FlightInfo.aspx?AD=D&AB=A" EnableViewState="False"  /></td>
                        <td style="width: 100px">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

