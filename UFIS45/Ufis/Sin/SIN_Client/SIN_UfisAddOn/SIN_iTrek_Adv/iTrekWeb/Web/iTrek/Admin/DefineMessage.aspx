<%@ Page Language="C#" MasterPageFile="~/Web/iTrek/MasterPage.master" AutoEventWireup="true" CodeFile="DefineMessage.aspx.cs" Inherits="Web_iTrek_Admin_DefineMessage" Title="Define Message" Theme="SATSTheme" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" Runat="Server">
    <table style="width: 872px; height: 168px">
        <tr>
            <td class="SubHeading">
                            Existing Predefined Messages</td>
        </tr>
        <tr>
            <td>
                <table>
                    <tr>
                        <td>
                            <asp:GridView ID="gvExistingMsg" runat="server" AutoGenerateColumns="False" BackColor="White"
                                BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black"
                                GridLines="Vertical" Width="860px" DataKeyNames="URNO" OnRowCancelingEdit="gvExistingMsg_RowCancelingEdit" OnRowDeleting="gvExistingMsg_RowDeleting" OnRowEditing="gvExistingMsg_RowEditing" OnRowUpdating="gvExistingMsg_RowUpdating" CssClass="GvDefault">
                                <FooterStyle BackColor="#CCCCCC" />
                                <Columns>
                                    <asp:BoundField DataField="URNO" HeaderText="URNO" Visible="False" />
                                    <asp:TemplateField HeaderText="Message">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtMsg" runat="server" Text='<%# Bind("MSGTEXT") %>' Height="38px" TextMode="MultiLine" Width="451px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblMsg" runat="server" Text='<%# Bind("MSGTEXT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField ButtonType="Button" ShowEditButton="True" >
                                        <ItemStyle Width="70px" />
                                    </asp:CommandField>
                                    <asp:CommandField ButtonType="Button" ShowDeleteButton="True" >
                                        <ItemStyle Width="70px" />
                                    </asp:CommandField>
                                </Columns>
                                <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" CssClass="GvHeader" />
                                <AlternatingRowStyle BackColor="#CCCCCC" CssClass="GvAlternate" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table style="width: 718px">
                    <tr>
                        <td style="width: 153px">
                            New Message</td>
                        <td style="width: 100px">
                            <asp:TextBox ID="txtMsg" runat="server" Height="21px" Width="497px" MaxLength="50"></asp:TextBox></td>
                        <td style="width: 100px">
                            &nbsp; &nbsp; &nbsp; &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 153px">
                        </td>
                        <td style="width: 100px">
                            <asp:Button ID="btnCreateMsg" runat="server" Text="Create Message" OnClick="btnCreateMsg_Click" EnableViewState="False" /></td>
                        <td style="width: 100px">
                        </td>
                    </tr>
                </table>
                <br />
                <br />
            </td>
        </tr>
    </table>

</asp:Content>

