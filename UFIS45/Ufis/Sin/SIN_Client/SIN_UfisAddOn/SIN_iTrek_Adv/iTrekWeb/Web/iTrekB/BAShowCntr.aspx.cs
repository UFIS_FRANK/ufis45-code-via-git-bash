using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.ENT;
//using UFIS.ITrekLib.Util;
using UFIS.Web.ITrek.WebMgr;
using UFIS.Web.ITrek.Sec;

//public partial class Web_iTrekB_BAShowCntr : System.Web.UI.Page
public partial class Web_iTrekB_BAShowCntr : UFIS.Web.ITrek.Base.BaseWebPage
{
   protected override void OnInit(EventArgs e)
   {
      base.OnInit(e);
      WuBAFlight1.BagArrFlightActionClick += new Web_iTrekB_WuBAFlight.BagArrFlightActionHandler(WuBAFlight1_BagArrFlightActionClick);
      //((Web_iTrekB_Master)Master).OnRefresh += new Web_iTrekB_Master.RefreshHandler(Web_iTrekB_BATerminalFlightUlds_OnRefresh);
   }

   void WuBAFlight1_BagArrFlightActionClick(object sender, UFIS.ITrekLib.EventArg.EaBagArrFlightEventArgs bagArrFlightEventArgs)
   {      
   }

   void Web_iTrekB_BATerminalFlightUlds_OnRefresh(object sender)
   {
      //PopulateData();
   }

   protected override void OnPreLoad(EventArgs e)
   {
      base.OnPreLoad(e);
   }

   protected override void OnLoad(EventArgs e)
   {
      base.OnLoad(e);
      if (CtrlSec.GetInstance().HasAccessToBAShowContainer(LoginedUser))
      {
         if ((!IsPostBack) && (!Page.IsCallback))
         {
            PopulateData();
         }
      }
      else
      {
         DoUnAuthorise();
      }
   }

   private void PopulateData()
   {
      try
      {
         //EntBagArrTerminal ent = MgrSess.GetBagArrTerminalInfo(Session);
         EntBagArrFlightSelectedUld obj = MgrSess.GetBagArrCurrentSel(this.Session);
         if ((obj.FlightId == null) || (obj.FlightId.Trim() == ""))
         {
            //Response.Redirect(Request.UrlReferrer.AbsoluteUri, true);
            this.lblMsg.Text = "No Flight Information!!!";
            WuBAFlight1.Visible = false;
            WuBATrip1.Visible = false;
         }
         else
         {
            WuBAFlight1.PopulateData(obj.FlightId, obj.BagArrType, null, obj.SelectedUldIds, obj.SelectedTrip);
            //WuBAFlight1.PopulateData(obj.FlightId, EnBagArrType.All, null, obj.SelectedUldIds, obj.SelectedTrip);
            WuBAFlight1.ShowDetail = false;
            WuBAFlight1.Collapsed = true;
            WuBAFlight1.EnableUldSelect = false;
            WuBAFlight1.Visible = true;
            //WuBATrip1.PopulateData(obj.FlightId, "", obj.BagArrType);
            WuBATrip1.PopulateData(obj.FlightId, "", EnBagArrType.All);
            WuBATrip1.Visible = true;
         }
      }
      catch (Exception ex)
      {
         LogMsg("PopulateData:Err:" + ex.Message);
      }
   }

   private void LogMsg(string msg)
   {
      try
      {
         MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("BAShowCntr", msg);
      }
      catch (Exception)
      {
         UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("BAShowCntr", msg);
      }
   }
}