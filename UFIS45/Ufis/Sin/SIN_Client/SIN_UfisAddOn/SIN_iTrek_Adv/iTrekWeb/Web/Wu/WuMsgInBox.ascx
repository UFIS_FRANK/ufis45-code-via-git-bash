<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WuMsgInBox.ascx.cs" Inherits="Web_Wu_WuMsgInBox" %>
InBox
<asp:UpdatePanel ID="updPnl1" runat="server">
<ContentTemplate>
<asp:Panel ID="pnlMsg" runat="server" EnableViewState="False" Height="100%" ScrollBars="Auto"
    Width="100%">
    <asp:GridView ID="gvInBox" runat="server" AutoGenerateColumns="False" BackColor="White"
        BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" CssClass="GridViewMsg"
        DataKeyNames="URNO" EnableViewState="False" ForeColor="Black" GridLines="Vertical"
        OnRowCommand="gvInBox_RowCommand" OnRowDataBound="gvInBox_RowDataBound"
        PageSize="50" Width="95%" >
        <FooterStyle BackColor="#CCCCCC" />
        <Columns>
            <asp:BoundField DataField="SENT_TIME" HeaderText="Time" ReadOnly="True">
                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="15px" />
            </asp:BoundField>
            <asp:BoundField DataField="SENT_BY" HeaderText="From">
                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"/>
            </asp:BoundField>
            <asp:ButtonField CommandName="InDet" DataTextField="MSG_SH_TEXT" HeaderText="Text">
                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="100px"/>
            </asp:ButtonField>
        </Columns>
        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="#CCCCCC" />
    </asp:GridView>
</asp:Panel>
</ContentTemplate>
</asp:UpdatePanel>
