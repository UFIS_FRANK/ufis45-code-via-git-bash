<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WuBelt.ascx.cs" Inherits="Web_iTrek_Admin_WuBelt" %>
<%@ Register Src="../../Wu/WuTerminal.ascx" TagName="WuTerminal" TagPrefix="uc1" %>

<div class="Heading" >
    Terminal Belts</div>
<div>
<asp:HiddenField ID="hfScrollTop" runat="server" Value="0" />    
    <asp:GridView ID="gvBelt" runat="server" AllowSorting="True" AutoGenerateColumns="False" Width="586px" OnRowCancelingEdit="gvBelt_RowCancelingEdit" OnRowEditing="gvBelt_RowEditing" OnRowUpdating="gvBelt_RowUpdating" DataKeyNames="TMNL,SECT" OnRowDeleting="gvBelt_RowDeleting" CssClass="GvDefault" Height="180px">
        <Columns>
            <asp:BoundField DataField="TMNL" HeaderText="Terminal" ReadOnly="True" Visible="False" >
                <ItemStyle Width="40px" />
            </asp:BoundField>
            <asp:BoundField DataField="TMNL_NAME" HeaderText="Terminal" ReadOnly="True" />
            <asp:BoundField DataField="SECT" HeaderText="Section" ReadOnly="True" >
                <ItemStyle Width="40px" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="From Belt">
                <EditItemTemplate>
                    <asp:TextBox ID="txtGvBeltsFr" runat="server" Text='<%# Bind("FR") %>'></asp:TextBox>
                    <asp:RegularExpressionValidator ID="reGvBeltFr" runat="server" ErrorMessage="Invalid Format" ControlToValidate="txtGvBeltsFr" ValidationExpression="\d{1,2}"></asp:RegularExpressionValidator>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblGvBeltsFr" runat="server" Text='<%# Bind("FR") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="To Belt">
                <EditItemTemplate>
                    <asp:TextBox ID="txtGvBeltsTo" runat="server" Text='<%# Bind("TO") %>'></asp:TextBox>
                    <asp:RegularExpressionValidator ID="reGvBeltTo" runat="server" ErrorMessage="Invalid Format" ControlToValidate="txtGvBeltsTo" ValidationExpression="\d{1,2}"></asp:RegularExpressionValidator>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblGvBeltsTo" runat="server" Text='<%# Bind("TO") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:CommandField ButtonType="Button" ShowEditButton="True" >
                <ItemStyle Width="40px" />
            </asp:CommandField>
            <asp:CommandField ButtonType="Button" ShowDeleteButton="True" >
                <ItemStyle Width="40px" />
            </asp:CommandField>
        </Columns>
        <HeaderStyle CssClass="GvHeader" />
        <AlternatingRowStyle CssClass="GvAlternate" />
    </asp:GridView>
    </div>
    <table style="width: 708px">
        <tr>
            <td class="Heading" style="width: 1139px">
            </td>
            <td style="width: 16px">
            </td>
            <td style="width: 415px">
            </td>
            <td style="width: 327px">
            </td>
        </tr>
        <tr>
            <td align="center" class="Heading" colspan="4">
                New Terminal Belt Setting</td>
        </tr>
        <tr>
            <td style="width: 1139px">
                Terminal</td>
            <td style="width: 16px">
            </td>
            <td style="width: 415px">
                <uc1:WuTerminal ID="WuTerminal1" runat="server" />
            </td>
            <td style="width: 327px">
            </td>
        </tr>
        <tr>
            <td style="width: 1139px">
                Section (e.g. 1)</td>
            <td style="width: 16px">
            </td>
            <td style="width: 415px">
                <asp:TextBox ID="txtSection" runat="server"></asp:TextBox></td>
            <td style="width: 327px">
            </td>
        </tr>
        <tr>
            <td style="width: 1139px">
                From Belt (e.g. 16)</td>
            <td style="width: 16px">
            </td>
            <td style="width: 415px">
                <asp:TextBox ID="txtBeltsFr" runat="server" Width="259px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="reBeltFr" runat="server" ErrorMessage="Invalid Format" ControlToValidate="txtBeltsFr" ValidationExpression="\d{1,2}"></asp:RegularExpressionValidator></td>
            <td style="width: 327px">
            </td>
        </tr>
        <tr>
            <td style="width: 1139px">
                To Belt (e.g. 19)</td>
            <td style="width: 16px">
            </td>
            <td style="width: 415px">
                <asp:TextBox ID="txtBeltsTo" runat="server" Width="259px"></asp:TextBox>
                <asp:RegularExpressionValidator ID="reBeltTo" runat="server" ErrorMessage="Invalid Format" ControlToValidate="txtBeltsTo" ValidationExpression="\d{1,2}"></asp:RegularExpressionValidator></td>
            <td style="width: 327px">
            </td>
        </tr>        
        <tr>
            <td style="width: 1139px">
            </td>
            <td style="width: 16px">
            </td>
            <td style="width: 415px">
            </td>
            <td style="width: 327px">
            </td>
        </tr>
    </table>
    <table style="width: 681px">
        <tr>
            <td style="width: 240px">
            </td>
            <td style="width: 100px">
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td style="width: 240px">
            </td>
            <td style="width: 100px">
                <asp:Button ID="btnSave" runat="server" Height="24px" OnClick="btnSave_Click" Text="Save"
                    Width="60px" EnableViewState="False" /></td>
            <td>
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CausesValidation="False" EnableViewState="False" OnClick="btnCancel_Click" /></td>
            <td>
            </td>
        </tr>
        <tr>
            <td style="width: 240px">
            </td>
            <td style="width: 100px">
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
    </table>

