<%@ Page Language="C#" MasterPageFile="~/Web/iTrek/MasterPage.master" AutoEventWireup="true" CodeFile="MessageLogs.aspx.cs" Inherits="Web_iTrek_Admin_MessageLogs" Title="Messages Log" Theme="SATSTheme" %>

<%@ Register Src="../../../UserControls/WUDate.ascx" TagName="WUDate" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" Runat="Server">
    <table>
        <tr>
            <td style="width: 980px;" class="Heading">
            Message Log
            </td>
        </tr>
        <tr>
            <td style="width: 900px;">
                <table>
                    <tr>
                        <td style="width: 200px;">
                            Sent By</td>
                        <td>
                            <asp:TextBox ID="txtFltSentBy" runat="server"></asp:TextBox></td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr style="height:21px">
                        <td style="width: 200px;" valign="middle">
                            Sent On Date Time From
                        </td>
                        <td style="width: 500px;" colspan="2" valign="middle">
                            <uc1:WUDate ID="wuFromDateTime" runat="server" />
                        </td>
                        <td style="width: 100px;">
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr style="height:21px">
                        <td style="width: 200px;"  valign="middle">
                            Sent On Date Time To</td>
                        <td style="width: 500px;" colspan="2"  valign="middle">
                             <uc1:WUDate ID="wuToDateTime" runat="server" />
                        </td>
                        <td style="width: 100px">
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td >
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="Search" /></td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            
            </td>
        </tr>
    </table>
    <br />
    <asp:GridView ID="gvMsgLog" runat="server" AutoGenerateColumns="False" 
    Width="990px" AllowSorting="True" 
    BackColor="White" BorderColor="#999999" BorderStyle="Solid"
    BorderWidth="1px" CellPadding="3" EnableSortingAndPagingCallbacks="False" ForeColor="Black" GridLines="Vertical" 
    PageSize="20"  OnSorted="gvMsgLog_Sorted" OnSorting="gvMsgLog_Sorting" OnRowDataBound="gvMsgLog_RowDataBound">
        <Columns>
            <asp:BoundField DataField="SBY" HeaderText="Sent By" />
            <asp:BoundField DataField="SON" HeaderText="Sent On" ItemStyle-Width="80px" />
            <asp:BoundField DataField="STO" HeaderText="Sent To" />
            <asp:BoundField DataField="RBY" HeaderText="Received By" />
            <asp:BoundField DataField="RON" HeaderText="Received On" /> 
            <asp:BoundField DataField="MSG" HeaderText="Message" /> 
        </Columns>
        <FooterStyle BackColor="#CCCCCC" />
        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="#CCCCCC" />
    </asp:GridView>
                <!--
            <asp:TemplateField HeaderText="MSG">
                <ItemTemplate>
                <a href='<%# "javascript:JsMsgLogDet(" + Eval("URNO") + ")" %>' ><%# Eval("MSG") %></a>
                </ItemTemplate>
            </asp:TemplateField>
            -->
    <table width="700">
    <tr>    <td style="width: 295px"></td>
    <td style="width: 132px">
    <asp:Button ID="btnPrevious" runat="server" Text="Previous <<" OnClick="btnPrevious_Click" Visible="False" EnableViewState="False" />
    </td>
    <td>
    <asp:Button ID="btnNext" runat="server" Text=">> Next" OnClick="btnNext_Click" Visible="False" Width="94px" EnableViewState="False" />
    </td>    <td></td>
    </tr>
    </table>

</asp:Content>

