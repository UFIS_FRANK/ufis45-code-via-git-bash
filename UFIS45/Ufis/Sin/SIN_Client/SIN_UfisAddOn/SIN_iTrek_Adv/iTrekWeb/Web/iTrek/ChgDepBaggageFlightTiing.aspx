<%@ Page Language="C#" MasterPageFile="~/Web/iTrek/MasterPage.master" AutoEventWireup="true" CodeFile="ChgDepBaggageFlightTiing.aspx.cs" Inherits="Web_iTrek_ChgDepBaggageFlightTiing" Title="Change Departure Flight Baggage Timing" Theme="SATSTheme" %>

<%@ Register Src="WUFlightDepBaggage.ascx" TagName="WUFlightDepBaggage" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" Runat="Server">
    <table style="width: 100%">
        <tr>
            <td style="width: 100px">
                <uc1:WUFlightDepBaggage ID="wuFlightDepBaggage1" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
                <table style="width: 636px">
                    <tr>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 100px">
                            &nbsp;<asp:Button ID="btnApply" runat="server" OnClick="btnApply_Click" Text="Apply" EnableViewState="False" /></td>
                        <td style="width: 100px">
                            <asp:Button ID="btnCancel" runat="server" CausesValidation="False" EnableViewState="False"
                                Text="Canel" PostBackUrl="~/Web/iTrek/FlightInfo.aspx?AD=D&AB=B" /></td>
                        <td style="width: 100px">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

