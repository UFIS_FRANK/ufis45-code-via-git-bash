<%@ Page Language="C#" MasterPageFile="~/Web/iTrek/MasterPage.master" AutoEventWireup="true" CodeFile="CPM.aspx.cs" Inherits="Web_iTrek_CPM" Title="CPM" Theme="SATSTheme" %>

<%@ Register Src="WUFlightArrApron.ascx" TagName="WUFlightArrApron" TagPrefix="uc2" %>
<%@ Register Src="WUFlightArrBaggage.ascx" TagName="WUFlightArrBaggage" TagPrefix="uc3" %>

<%@ Register Src="WUAFlight.ascx" TagName="WUAFlight" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" Runat="Server">
    <table style="width: 100%">
        <tr>
            <td>
                <uc2:WUFlightArrApron ID="wuFlightArrApron1" runat="server" EnableViewState="false"
                    Visible="false" />
            </td>
        </tr>
        <tr>
            <td >
                <uc3:WUFlightArrBaggage ID="wuFlightArrBaggage1" runat="server" EnableViewState="false"
                    Visible="false" />
            </td>
        </tr>
        <tr>
            <td>
                CPM<br />
                <asp:TextBox ID="txtCPM" runat="server" Height="500px" ReadOnly="True" TextMode="MultiLine"
                    Width="800px"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                </td>
        </tr>
    </table>
</asp:Content>

