<%@ Page Language="C#" MasterPageFile="MasterPage.master" AutoEventWireup="true" CodeFile="FlightInfo.aspx.cs" Inherits="Web_iTrek_FlightInfo" Title="Flight Information" Theme="SATSTheme" %>
<%@ OutputCache Duration="240" VaryByParam="*" %>
<%@ Register Src="WUFlightArrApron.ascx" TagName="WUFlightArrApron" TagPrefix="uc1" %>
<%@ Register Src="WUFlightArrBaggage.ascx" TagName="WUFlightArrBaggage" TagPrefix="uc2" %>
<%@ Register Src="WUFlightDepBaggage.ascx" TagName="WUFlightDepBaggage" TagPrefix="uc3" %>
<%@ Register Src="WUFlightDepApron.ascx" TagName="WUFlightDepApron" TagPrefix="uc4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cp1" Runat="Server">
<div style="width:980px;">
                <asp:Label ID="lblHdr" runat="server" CssClass="cssHeading"></asp:Label>
</div>
<div>
Flight Filter
    <br />
    <table>
        <tr>
            <td rowspan="2" style="width: 108px">
                <asp:ListBox ID="lbTerm" runat="server" SelectionMode="Multiple" Rows="3"></asp:ListBox></td>
            <td style="width: 135px">
                Days &nbsp;&nbsp;</td>
            <td style="width: 135px">
                <asp:DropDownList ID="ddTime" runat="server" Width="169px"></asp:DropDownList></td>
            <td style="width: 34px">
            </td>
            <td style="width: 133px">
                From Time
                <br />
                (e.g. 10:30)</td>
            <td style="width: 100px">
                <asp:TextBox ID="txtFrTime" runat="server" Width="117px"></asp:TextBox></td>
            <td style="width: 20px">
                &nbsp;</td>
            <td style="width: 101px">
                To Time<br />
                (e.g. 14:30)</td>
            <td style="width: 190px">
                <asp:TextBox ID="txtToTime" runat="server"></asp:TextBox></td>
            <td style="width: 18px">
            </td>
        </tr>
        <tr>
            <td style="width: 135px">
                Airline Code<br />
                (e.g. SQ;MI)</td>
            <td style="width: 135px">
                <asp:TextBox ID="txtALC" runat="server" Width="164px"></asp:TextBox></td>
            <td style="width: 34px">
            </td>
            <td style="width: 133px">
                &nbsp;Aircraft Regn.</td>
            <td style="width: 100px">
                <asp:TextBox ID="txtRegn" runat="server" Width="118px"></asp:TextBox></td>
            <td style="width: 20px">
                </td>
            <td style="width: 101px">
                </td>
            <td style="width: 190px">
                <asp:Button ID="btnFilter" runat="server" Text="Apply Filter" OnClick="btnFilter_Click" EnableViewState="False" /></td>
            <td style="width: 18px">
            </td>
        </tr>
    </table>
</div> 
<div>
    <asp:PlaceHolder ID="ph1" runat="server"></asp:PlaceHolder>
    <uc4:WUFlightDepApron ID="WUDeAr1" runat="server" Visible="false" IsGridHeaderFreeze="true" EnableViewState="false" />
    <uc3:WUFlightDepBaggage ID="WUDeBg1" runat="server" Visible="false" IsGridHeaderFreeze="true"  EnableViewState="false" />
    <uc2:WUFlightArrBaggage ID="WUFlAB1" runat="server" Visible="false" IsGridHeaderFreeze="true"  EnableViewState="false" />
    <uc1:WUFlightArrApron ID="WUArAp1" runat="server" Visible="false" IsGridHeaderFreeze="true"  EnableViewState="false" />
</div>
</asp:Content>

