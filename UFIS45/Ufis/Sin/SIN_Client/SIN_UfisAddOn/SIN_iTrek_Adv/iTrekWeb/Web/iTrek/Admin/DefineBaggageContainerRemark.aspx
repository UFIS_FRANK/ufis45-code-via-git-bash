<%@ Page Language="C#" MasterPageFile="~/Web/iTrek/MasterPage.master" AutoEventWireup="true" CodeFile="DefineBaggageContainerRemark.aspx.cs" Inherits="Web_iTrek_Admin_DefineBaggageContainerRemark" Title="Baggage Container Remark" Theme="SATSTheme" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" Runat="Server">
    <table style="width: 872px; height: 168px">
        <tr>
            <td class="SubHeading">
                            Existing Predefined Baggage Container Remarks</td>
        </tr>
        <tr>
            <td>
                            <asp:GridView ID="gvExistingRmk" runat="server" AutoGenerateColumns="False" BackColor="White"
                                BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black"
                                GridLines="Vertical" Width="860px" DataKeyNames="URNO" OnRowCancelingEdit="gvExistingRmk_RowCancelingEdit" OnRowDeleting="gvExistingRmk_RowDeleting" OnRowEditing="gvExistingRmk_RowEditing" OnRowUpdating="gvExistingRmk_RowUpdating">
                                <FooterStyle BackColor="#CCCCCC" />
                                <Columns>
                                    <asp:BoundField DataField="URNO" HeaderText="URNO" Visible="False" />
                                    <asp:TemplateField HeaderText="Message">
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtMsg" runat="server" Text='<%# Bind("MSGTEXT") %>' Height="38px" TextMode="MultiLine" Width="451px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblMsg" runat="server" Text='<%# Bind("MSGTEXT") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:CommandField ButtonType="Button" ShowEditButton="True" >
                                        <ItemStyle Width="70px" />
                                    </asp:CommandField>
                                    <asp:CommandField ButtonType="Button" ShowDeleteButton="True" >
                                        <ItemStyle Width="70px" />
                                    </asp:CommandField>
                                </Columns>
                                <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                                <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="#CCCCCC" />
                            </asp:GridView>

            </td>
        </tr>
        <tr>
            <td>
                <table style="width: 718px">
                    <tr>
                        <td style="width: 153px">
                            New Remark</td>
                        <td style="width: 100px">
                            <asp:TextBox ID="txtRmk" runat="server" Height="40px" Width="497px"></asp:TextBox></td>
                        <td style="width: 100px">
                            &nbsp; &nbsp; &nbsp; &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 153px">
                        </td>
                        <td style="width: 100px">
                            <asp:Button ID="btnCreate" runat="server" Text="Create" Width="166px" OnClick="btnCreate_Click" EnableViewState="False" /></td>
                        <td style="width: 100px">
                        </td>
                    </tr>
                </table>
                <br />
                <br />
            </td>
        </tr>
    </table>
</asp:Content>

