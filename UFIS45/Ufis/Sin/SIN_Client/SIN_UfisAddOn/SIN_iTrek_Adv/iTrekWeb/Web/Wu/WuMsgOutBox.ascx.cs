using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;

using UFIS.Web.ITrek.WebMgr;

using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.Util;

public partial class Web_Wu_WuMsgOutBox : System.Web.UI.UserControl
{
   public delegate void ShowMessageHandler(object sender, string msgId);
   public event ShowMessageHandler OnClickShowMessage;

   protected override void OnLoad(EventArgs e)
   {
      base.OnLoad(e);
   }

   protected override void OnPreRender(EventArgs e)
   {
      try
      {
         base.OnPreRender(e);
         Refresh();
      }
      catch (Exception ex)
      {
         LogMsg("OnPreRender:Err:" + ex.Message);
      }
   }

   public Unit Width
   {
      get { return pnlMsg.Width; }
      set
      {
         pnlMsg.Width = value;
      }
   }

   public Unit Height
   {
      get { return pnlMsg.Height; }
      set
      {
         pnlMsg.Height = value;
         LogMsg("Height:" + pnlMsg.Height.ToString());
      }
   }

   public void DisplayData(string userId, int maxMsgCnt)
   {
      try
      {
         DSMsg dsOutMsg = CtrlMsg.RetrieveOutMessages(userId, maxMsgCnt);
         DataView dv = dsOutMsg.MSGTO.DefaultView;
         dv.Sort = "SENT_DT DESC, URNO";
         gvOutBox.DataSource = dv;
         gvOutBox.DataBind();
      }
      catch (Exception ex)
      {
         LogMsg("PopulateData:Err:" + ex.Message);
         //WebUtil.AlertMsg(Page, ex.Message);
      }
   }

   public void PopulateData(string userId, int maxMsgCnt)
   {
      CurInfo info = new CurInfo(userId, maxMsgCnt);
      Info = info;
      //DisplayData(info._msgUserId, info._maxMsgCnt);
   }

   protected void gvOutBox_RowCommand(object sender, GridViewCommandEventArgs e)
   {
      string st = e.CommandName;
      if (st == "OutDet")
      {
         int idx = Convert.ToInt32(e.CommandArgument.ToString());
         string msgId = ((GridView)sender).DataKeys[idx].Value.ToString();
         //string st1 = sender.ToString();
         //wuMsgDet1.ShowMsg(msgId, "O");//Show outgoing message
         //wuMsgDet1.Visible = true;
         if (OnClickShowMessage != null) OnClickShowMessage(this, msgId);
      }
   }

   public void Refresh()
   {
      CurInfo info = Info;
      if (info != null)
         DisplayData(info._msgUserId, info._maxMsgCnt);
   }

   [Serializable]
   private class CurInfo
   {
      public string _msgUserId;
      public int _maxMsgCnt;

      public CurInfo(string userId, int maxMsgCnt)
      {
         _msgUserId = userId;
         _maxMsgCnt = maxMsgCnt;
      }
   }

   private CurInfo _info = null;
   private const string VSMSGOUTBOX = "VSMSGOUTBOX";

   private CurInfo Info
   {
      get
      {
         if (_info == null)
         {
            try
            {
               _info = (CurInfo)ViewState[VSMSGOUTBOX];
            }
            catch (Exception)
            {
            }
         }
         return _info;
      }
      set
      {
         _info = value;
         ViewState[VSMSGOUTBOX] = _info;
      }
   }


   private void LogMsg(string msg)
   {
      //UtilLog.LogToTraceFile("WuMsgOutBox", msg);
      try
      {
         MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("WuMsgOutBox", msg);
      }
      catch (Exception)
      {
         UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("WuMsgOutBox", msg);
      }
   }
}