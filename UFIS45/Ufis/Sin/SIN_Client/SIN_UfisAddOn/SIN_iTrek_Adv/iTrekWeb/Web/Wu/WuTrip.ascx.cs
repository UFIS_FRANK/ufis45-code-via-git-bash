using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.Ctrl;

public partial class Web_Wu_WuTrip : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
    }

    private void ShowTrips(string flightId, string tripHeading)
    {
        lblTripMsg.Text = tripHeading;
        DSTrip dsTrip = null;
        DSCpm dsCpm = null;
        string result = CtrlBagArrival.GetTripNContainerInfo(flightId, UFIS.ITrekLib.ENT.EnBagArrType.All,
            ref dsTrip, ref dsCpm);
        if (dsTrip.TRIP.Rows.Count < 1) throw new ApplicationException("No Trip Information.");
        DataView dv = dsTrip.TRIP.DefaultView;
        //dv.Sort="SENT_DT";
        dv.Sort = "STRPNO ASC,ULDN";
        gvTrips.DataSource = dv;
        gvTrips.DataBind();
    }
}
