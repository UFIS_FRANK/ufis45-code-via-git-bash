<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AdminLogin.aspx.cs" Inherits="AdminLogin" Theme="SATSTheme" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Admin Login</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <strong><span style="font-size: 24pt; color: #3366ff" class="Logo">
    SATS </span></strong>
    </div>
    <div>
    <table style="width: 692px">
            <tr>
                <td style="width: 700px; height: 50px; font-weight: bold; vertical-align: middle; text-align: center;" align="center" valign="middle">
                    iTrek
                    Admin</td>
            </tr>
            <tr>
                <td>
                    <table style="width: 541px">
                        <tr>
                            <td style="width: 286px">
                                </td>
                            <td style="width: 130px">
                                User ID</td>
                            <td style="width: 79px">
                                <asp:TextBox ID="txtUserId" runat="server" Width="145px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 286px">
                                </td>
                            <td style="width: 130px">
                                Password</td>
                            <td style="width: 79px">
                                <asp:TextBox ID="txtPwd" runat="server" TextMode="Password" Width="144px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 286px">
                            </td>
                            <td style="width: 130px">
                            </td>
                            <td style="width: 79px">
                            </td>
                        </tr>
                    </table>
                    <asp:Label ID="lblMsg" runat="server" Visible="False" Width="637px"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 100px">
                    <table style="width: 577px">
                        <tr>
                            <td style="width: 327px">
                            </td>
                            <td style="width: 100px">
                                <asp:Button ID="btnLogin" runat="server" Text="Login" OnClick="btnLogin_Click" /></td>
                            <td style="width: 85px">
                                <asp:Button ID="btnCancel" runat="server" Text="Clear" CausesValidation="False" OnClientClick='document.getElementById("txtUserId").value = ""; document.getElementById("txtPwd").value = "";' UseSubmitBehavior="False" Width="50px" OnClick="btnCancel_Click" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
        <tr>
            <td align="center">
                <br />
                <br />
                <br />
                <br />
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <a href='http://www.ufis-as.com'>
                Powered by UFIS AS (c) 2006</a>
                </td>
        </tr>
        </table>
    </div>
    </form>
</body>
</html>
