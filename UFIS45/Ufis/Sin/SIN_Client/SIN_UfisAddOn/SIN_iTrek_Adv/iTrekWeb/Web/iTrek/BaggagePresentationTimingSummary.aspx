<%@ Page Language="C#" MasterPageFile="~/Web/iTrek/MasterPage.master" AutoEventWireup="true" CodeFile="BaggagePresentationTimingSummary.aspx.cs" Inherits="Web_iTrek_BaggagePresentationTimingSummary" Title="Baggage Presentation Timing Summary" Theme="SATSTheme" %>

<%@ Register Src="WUBagTimingSummRpt.ascx" TagName="WUBagTimingSummRpt" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" Runat="Server">
    <table style="width: 100%">
        <tr>
            <td class="Heading" style="width: 735px">
                Baggage Presentation Timing Summary</td>
        </tr>
        <tr>
            <td style="width: 900px">
                <table style="width: 890px">
                    <tr>
                        <td>
                            Terminal :</td>
                        <td>
                            <asp:DropDownList ID="ddTerminal" runat="server" Width="163px">
                            </asp:DropDownList></td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            Date From :</td>
                        <td style="width: 300px">
                            <asp:TextBox ID="txtFromDate" runat="server"></asp:TextBox>
                            DD MMM YYYY</td>
                        <td style="width: 22px">
                            </td>
                        <td style="width: 100px">
                            Time From :</td>
                        <td style="width: 200px">
                            <asp:TextBox ID="txtFromTime" runat="server" Width="48px"></asp:TextBox>
                            HHMM</td>
                        <td >
                            </td>
                    </tr>
                    <tr>
                        <td >
                            Date To :</td>
                        <td>
                            <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
                            DD MMM YYYY</td>
                        <td>
                        </td>
                        <td>
                            Time To :</td>
                        <td>
                            <asp:TextBox ID="txtToTime" runat="server" Width="51px"></asp:TextBox>
                            HHMM</td>
                        <td>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 735px">
                <table style="width: 588px">
                    <tr>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 100px; text-align: center">
                            <asp:Button ID="btnGenReport" runat="server" Text="Generate Report" OnClick="btnGenReport_Click" EnableViewState="False" /></td>
                        <td style="width: 34px; text-align: center">
                        </td>
                        <td style="width: 239px">
                            <asp:Button ID="btnGenNPrintReport" runat="server" Text="Generate and Print Report" OnClick="btnGenNPrintReport_Click" EnableViewState="False" /></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 100px; text-align: center">
                            <asp:Button ID="btnPrint" runat="server" Text="Print" Visible="False" OnClientClick="PrintRpt();" EnableViewState="False" />
                            </td>
                        <td style="width: 34px">
                        </td>
                        <td style="width: 239px">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 735px">
                &nbsp;<uc1:WUBagTimingSummRpt ID="WUBagTimingSummRpt1" runat="server" EnableViewState="true" />
            </td>
        </tr>
    </table>
</asp:Content>

