using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.Ctrl;
using MMSoftware.MMWebUtil;
using UFIS.ITrekLib.ENT;
using UFIS.Web.ITrek.Sec;
using UFIS.ITrekLib.Util;


public partial class Web_iTrek_Admin_DefineMessage : System.Web.UI.Page
{
    private const string PG_NAME = "PreDefMsg";
    private const string CTRL_VIEW = "Define";

    protected void Page_Load(object sender, EventArgs e)
    {
        ((MyMasterPage)Master).AutoRefreshMode = false;
        CtrlSec ctrlsec = CtrlSec.GetInstance();
        EntUser user = ctrlsec.AuthenticateUser(Session, Response);
        if (ctrlsec.HasAccessRights(user, PG_NAME, CTRL_VIEW))
        {
            if (!IsPostBack)
            {
                ShowPredefinedMsg();
            }
            SetFocus(txtMsg);
        }
        else
        {
            DoUnAuthorise();
        }
    }

    private void DoUnAuthorise()
    {
        Response.Redirect("~/UnAuthorised.aspx", true);
    }

    void ShowPredefinedMsg()
    {
        gvExistingMsg.DataSource = CtrlPreDefMsg.RetrieveActivePredifinedMsg();
        gvExistingMsg.DataBind();
    }

    private const int MAX_MSG_LEN = 140;

    protected void btnCreateMsg_Click(object sender, EventArgs e)
    {
        string msg = "";
        try
        {
            if (txtMsg.Text != "")
            {
                string st = txtMsg.Text.Trim();
                if (st.Length > MAX_MSG_LEN)
                {
                    msg = "Exceeds maximum lenght limit of " + MAX_MSG_LEN;
                }
                else
                {
                    CtrlPreDefMsg.CreatePredefinedMsg(txtMsg.Text);
                    txtMsg.Text = "";
                }
            }
        }
        catch (Exception ex)
        {
            msg = ex.Message;
        }
        if (msg != "")
        {
            WebUtil.AlertMsg(Page, msg);
        }
        ShowPredefinedMsg();
    }
    protected void gvExistingMsg_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvExistingMsg.EditIndex = -1;
        ShowPredefinedMsg();
    }

    protected void gvExistingMsg_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //int idx = e.RowIndex;
        try
        {
            string msgId = ((GridView)sender).DataKeys[e.RowIndex].Value.ToString();
            CtrlPreDefMsg.DeletePredefinedMsg(msgId);
        }
        catch (Exception ex)
        {
            LogMsg("Delete:Error:" + ex.Message);
            WebUtil.AlertMsg(Page, "Fail to Delete.");
        }
        ShowPredefinedMsg();
        return;
    }
    private void LogMsg(string msg)
    {
        UtilLog.LogToTraceFile("DefineMessage", msg);
    }
    protected void gvExistingMsg_RowEditing(object sender, GridViewEditEventArgs e)
    {
       int idx = e.NewEditIndex;
       gvExistingMsg.EditIndex = idx;
       ShowPredefinedMsg();
    }
    protected void gvExistingMsg_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            int idx = e.RowIndex;
            //string msgId = ((GridView)sender).DataKeys[e.RowIndex].Value.ToString();
            string msgId1 = gvExistingMsg.DataKeys[e.RowIndex].Value.ToString();
            string msg = ((TextBox)(gvExistingMsg.Rows[idx].FindControl("txtMsg"))).Text;

            CtrlPreDefMsg.UpdatePredefinedMsg(msgId1, msg);
        }
        catch (Exception ex)
        {
            LogMsg("Update:Error:" + ex.Message);
            WebUtil.AlertMsg(Page, "Fail to update.");
        }

        gvExistingMsg.EditIndex = -1;
        ShowPredefinedMsg();
    }
}
