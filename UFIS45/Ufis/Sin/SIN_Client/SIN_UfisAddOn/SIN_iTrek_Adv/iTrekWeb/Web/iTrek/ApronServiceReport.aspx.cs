using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.ENT;
using UFIS.Web.ITrek.Sec;
using UFIS.Web.ITrek.WebMgr;
using UFIS.ITrekLib.Util;
using MMSoftware.MMWebUtil;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.Ctrl;
using iTrekUtils;
using MM.UtilLib;

public partial class Web_iTrek_ApronServiceReport : System.Web.UI.Page
{

    private const char SEPERATOR = '|';
    private enum EnEditMode { CreateMode, EditMode, ViewMode };
    private EnEditMode _curEditMode = EnEditMode.ViewMode;

    string flightUrNo = "";
    string flightNo = "";

    private const string PG_NAME = "ApronServiceReport";
    private const string CTRL_VIEW = "View";
    private const string CTRL_EDIT_ONLY = "Edit";
    private const string CTRL_CREATE = "Create";
    private DSApronServiceRpt _dsASR = null;

    private EntUser _user = null;
    private EntUser CurUser
    {
        get
        {
            if (_user == null)
            {
                CtrlSec ctrlsec = CtrlSec.GetInstance();
                _user = ctrlsec.AuthenticateUser(Session, Response);
            }
            return _user;
        }
    }
    private DSApronServiceRpt DsASR
    {
        get
        {
            if (_dsASR == null)
            {
                try
                {
                    _dsASR = (DSApronServiceRpt)ViewState["VS_DSASR"];
                }
                catch (Exception)
                {
                }
                if ((_dsASR == null) || (_dsASR.ASR.Rows.Count < 1))
                {
                    _dsASR = CtrlAsr.PopulateApronServiceReport(null, flightUrNo);
                }
            }
            return _dsASR;
        }
        //set
        //{
        //    _dsASR = value;
        //}
	}

	#region Javascript to check the Mandatory info
	private const string JS_CHK_MANDATORY = @"function trim(stringToTrim) {	return stringToTrim.replace(/^\s+|\s+$/g,'');}
function ltrim(stringToTrim) {	return stringToTrim.replace(/^\s+/,'');}
function rtrim(stringToTrim) {	return stringToTrim.replace(/\s+$/,'');}
function ValidateMandatory()
{
   var valid = true;
   var arrFields = fieldToCheck.split(',');
   var arrDesc = fieldDesc.split(',');
   var cnt = arrFields.length;
   var i=0;
   var msg = '';
   var tMsg = '';

   for(i=0; i<cnt; i++)
   {
      tMsg += document.getElementById(arrFields[i]).id + ':' + document.getElementById(arrFields[i]).value;
      if (trim(document.getElementById(arrFields[i]).value)=='')
      {
          msg += arrDesc[i] + '\n';
          comma = ', ';
          valid = false;
          tMsg += ':Invalid\n';
      }
      else{
		tMsg += ':Valid\n';
      }
   }
   if (valid==false)
   {
       return confirm('There are missing information in following fields\n\n' + msg + '\n\nDo you want to continue?');
   }

   return valid;
}
";
	#endregion

	private void RegisterJsValueToCheck(string fieldIdsToCheck, string fieldDescsToCheck)
    {
        //LogMsg("RegisterJsValueToCheck:start");
        string st = string.Format(@"<script type='text/javascript'>
var fieldToCheck='{0}';
//Fields to check
var fieldDesc = '{1}';
//Field Name (for user) to display corresponding to fieldToCheck
{2}
</script>", fieldIdsToCheck, fieldDescsToCheck, JS_CHK_MANDATORY);
        ClientScript.RegisterClientScriptBlock(this.GetType(), "CheckingMandatory", st);
        //LogMsg("RegisterJsValueToCheck:" + st);
    }

	protected override void OnPreRender(EventArgs e)
	{
		base.OnPreRender(e);
		ShowInfo();
	}

	protected override void OnLoad(EventArgs e)
	//protected void Page_Load(object sender, EventArgs e)
    {
		LogTraceMsg("PageLoad");
        ((MyMasterPage)Master).AutoRefreshMode = false;

        try
        {
            flightUrNo = Request.Params["URNO"];
            flightNo = Request.Params["FLNO"];
            //LogMsg("ASR:" + flightUrNo + "," + flightNo );
        }
        catch (Exception)
        {
        }

        if (flightUrNo.Trim() == "")
        {
            WebUtil.AlertMsg(Page, "No flight number to get the report.");
        }
        else
        {
			//if (!IsPostBack)
			//{
			//    ShowInfo();
			//}
        }
    }

    private bool IsReportExist(string flightUrno)
    {
		return CtrlAsr.IsConfirmedReportExist(null, flightUrNo);
    }

    private void ShowInfo()
    {
        try
        {
			LogTraceMsg("ShowInfo:Start:");
            CtrlSec ctrlsec = CtrlSec.GetInstance();
			LogTraceMsg("ShowInfo:1");
            if (IsReportExist(flightUrNo))
            {
				LogTraceMsg("ShowInfo:1-1");
                if (ctrlsec.HasAccessRights(CurUser, PG_NAME, CTRL_EDIT_ONLY))
                {
					LogTraceMsg("ShowInfo:2");
                    DoEdit();
					LogTraceMsg("ShowInfo:3");
                }
                else if (ctrlsec.HasAccessRights(CurUser, PG_NAME, CTRL_VIEW))
                {
					LogTraceMsg("ShowInfo:4");
                    DoView();
					LogTraceMsg("ShowInfo:5");
                }
                else
                {
					LogTraceMsg("ShowInfo:6");
                    //WebUtil.AlertMsg(Page, "Not authorised to create the report for the flight " + flightNo);
                    DoUnAuthorised();
					LogTraceMsg("ShowInfo:7");
                }
            }
            else
            {
				LogTraceMsg("ShowInfo:1-2");
                if (ctrlsec.HasAccessRights(CurUser, PG_NAME, CTRL_CREATE))
                {
					LogTraceMsg("ShowInfo:8");
					if (IsOwnFlight(CurUser, flightUrNo))
					{
						LogTraceMsg("ShowInfo:9");
						DoCreate();
						LogTraceMsg("ShowInfo:10");
					}
					else
					{
						WebUtil.AlertMsg(Page, "Not authorised to create the report for the flight " + flightNo);
					}      
                }
                else DoReportNotReady();
            }
        }
        catch (ApplicationException ex)
        {
			LogMsg("ShowInfo:Err:App." + ex.Message);
            WebUtil.AlertMsg(Page,"Error. " + ex.Message);
        }
        catch (Exception ex)
        {
			LogMsg("ShowInfo:Err:" + ex.Message);
            WebUtil.AlertMsg(Page,"Error." + ex.Message);
        }
    }

    private void DoUnAuthorised()
    {
        Response.Redirect("~/UnAuthorised.aspx", true);
    }

    private void DoReportNotReady()
    {
        WebUtil.AlertMsg(Page, "Report not ready yet");
    }

    private bool IsOwnFlight(EntUser user, string flightUrno)
    {
        if (user.UserId == CtrlFlight.GetAOIdForFlight(flightUrno)) return true;
        else return false;
    }

    private void DoCreate()
    {
        LogTraceMsg("ASR:DoCreate");
        _curEditMode = EnEditMode.CreateMode;
        ShowAllInfo();
        SetReadOnly(false);
        btnSave.Visible = true;
        btnCancel.Visible = true;
        btnPrint.Visible = false;
        //btnSaveAsDraft.Visible = false;
    }

    private void DoEdit()
    {
		LogTraceMsg("ASR:DoEdit");
        _curEditMode = EnEditMode.EditMode;
        ShowAllInfo();
        SetReadOnly(false);
        btnSave.Visible = true;
        btnCancel.Visible = true;
        btnPrint.Visible = true;
        //btnSaveAsDraft.Visible = false;
    }

    private void DoView()
    {
		LogTraceMsg("ASR:DoView");
        _curEditMode = EnEditMode.ViewMode;
        ShowAllInfo();
        SetReadOnly(true);
        btnSave.Visible = false;
        btnCancel.Visible = false;
        btnPrint.Visible = true;
        //btnSaveAsDraft.Visible = false;
    }

    private void ShowAllInfo()
    {
		LogTraceMsg("ShowAllInfo");
        if ((DsASR == null) || (DsASR.ASR.Rows.Count <1))
        {
            WebUtil.AlertMsg(Page, "Error in retrieving the information.");
        }
        else
        {
            if (DsASR.ASR.Rows.Count > 0)
            {
                DSApronServiceRpt.ASRRow row = (DSApronServiceRpt.ASRRow)DsASR.ASR.Rows[0];
                ShowFlightInfo(row);
                ShowRSMInfo(row);
                ShowCheckList(row);
                ShowOthers(row);
            }
        }
    }

    private void SetDropDownData(DropDownList dd, string value)
    {
        if ((value != null) && (value.Trim() != ""))
        {
            int cnt = dd.Items.Count;
            value = value.Trim().ToUpper();
            for (int i = 0; i < cnt; i++)
            {
                if (dd.Items[i].Value.ToUpper() == value)
                {
                    dd.SelectedIndex = i;
                    break;
                }
            }
        }
    }
    private void SetCheckBoxData(CheckBox ckb, string value)
    {
        if (value == null) value = "";
        if (value.Trim().ToUpper() == "Y")
        {
            ckb.Checked = true;
        }
        else ckb.Checked = false;
    }
    private string GetCheckBoxData(CheckBox ckb)
    {
        if (ckb.Checked) return "Y";
        else return "N";
    }

    private const string FMT_DATE = "dd MMM yyyy";
    private readonly System.Drawing.Color DATA_CHG_ALERT_COLOR = System.Drawing.Color.Red;

    private bool CheckDataChange(DSApronServiceRpt.ASRRow curRow, DSApronServiceRpt.ASRRow newRow,
        string fieldName, TextBox txtBox)
    {
        bool changed = false;
        if ((curRow != null) && (newRow != null))
        {
            try
            {
                if (curRow[fieldName] != newRow[fieldName])
                {
                    changed = true;
                    txtBox.BackColor = DATA_CHG_ALERT_COLOR;
                }
            }
            catch (Exception)
            {
            }
        }
        return changed;
    }

    private void CheckMandatoryBeforeSubmit(bool checkArr, bool checkDep)
    {
        //\\ For Mandatory fields for Arrival: 
//(Time First Bag Despatched, Time Last Bag Despatched, Time Unloading Completed) and ATA are Mandatory for Arrival and Transit Flight 

//\\ For Mandatory fields for Departure: 
//(Time Loading Commenced, Time Loading Completed, Time Last GSE Removed/Cargo Door Closed, Time Last PLB Removed ) and ATD are Mandatory for Departure and Transit Flight. 

        string ids = "";
        string descs = "";
        if (checkArr)
        {
            ids += this.txtATA.ClientID + "," + this.txtFirstBag.ClientID + "," +
                this.txtLastBag.ClientID + "," + this.txtUnlEnd.ClientID;
            descs += "ATA,Time First Bag Despatched,Time Last Bag Despatched,Time Unloading Completed";
        }

        if (checkDep)
        {
            if (ids != "")
            {
                ids += ",";
                descs += ",";
            }
            ids += this.txtATD.ClientID + "," + 
                this.txtLoadStart.ClientID + "," + this.txtLoadEnd.ClientID + "," +
                this.txtDoorClose + "," + this.txtPLB.ClientID;
            descs += "ATD,Time Loading Commenced,Time Loading Completed,Time Last GSE Removed/Cargo Door Closed,Time Last PLB Removed";
        }

        this.RegisterJsValueToCheck(ids, descs);
    }

    private void ShowFlightInfo(DSApronServiceRpt.ASRRow row)
    {
        //txtAOName.Text = row.AOFNAME;// +" " + row.AOLNAME + "(" + row.AOID + ")";
        txtAOName.Text = row.AOFNAME + " (" + row.AOID + ")";
        txtFlightDate.Text = String.Format("{0:" + FMT_DATE + "}", UtilTime.ConvUfisTimeStringToDateTime(row.ST));
        txtRegn.Text = row.ACFTREGN;
        txtType.Text = row.ACFTTYPE;
        txtFrom.Text = row.FR;
        txtTo.Text = row.TO;
        txtFlightNo.Text = row.FLNO;

        if (row.TURN == "")
        {
            if (row.AD == "A")
            {//Arrival Flight
                txtSTA.Text = ConvUfisTimeToTimeString(row.ST);
                txtATA.Text = ConvUfisTimeToTimeString(row.AT);
                txtSTD.Text = "";
                txtATD.Text = "";
                txtFirstBag.Text = ConvUfisTimeToTimeString(row.FTRIP);
                txtLastBag.Text = ConvUfisTimeToTimeString(row.LTRIP);
                txtUnlEnd.Text = ConvUfisTimeToTimeString(row.UNLEND);
                CheckMandatoryBeforeSubmit(true, false);
            }
            else
            {//Departure Flight
                txtSTA.Text = "";
                txtATA.Text = "";
                txtSTD.Text = ConvUfisTimeToTimeString(row.ST);
                txtATD.Text = ConvUfisTimeToTimeString(row.AT);
                txtLoadStart.Text = ConvUfisTimeToTimeString(row.LDSTRT);
                txtLoadEnd.Text = ConvUfisTimeToTimeString(row.LDEND);
                txtDoorClose.Text = ConvUfisTimeToTimeString(row.LSTDOR);
                txtPLB.Text = ConvUfisTimeToTimeString(row.PLB);
                CheckMandatoryBeforeSubmit(false, true);
            }
        }
        else
        {//Turnaround Flight
            txtSTA.Text = ConvUfisTimeToTimeString(row.STA);
            txtATA.Text = ConvUfisTimeToTimeString(row.ATA);
            txtSTD.Text = ConvUfisTimeToTimeString(row.STD);
            txtATD.Text = ConvUfisTimeToTimeString(row.ATD);

            txtFirstBag.Text = ConvUfisTimeToTimeString(row.FTRIP);
            txtLastBag.Text = ConvUfisTimeToTimeString(row.LTRIP);
            txtUnlEnd.Text = ConvUfisTimeToTimeString(row.UNLEND);

            txtLoadStart.Text = ConvUfisTimeToTimeString(row.LDSTRT);
            txtLoadEnd.Text = ConvUfisTimeToTimeString(row.LDEND);
            txtDoorClose.Text = ConvUfisTimeToTimeString(row.LSTDOR);
            txtPLB.Text = ConvUfisTimeToTimeString(row.PLB);
            CheckMandatoryBeforeSubmit(true, true);
        }

        txtBay.Text = row.BAY;
        txtUldComments.Text = row.ULDCMT;
        txtGenComments.Text = row.GENCMT;
        txtBPTReason.Text = row.RSNBPT;
        txtDutyManager.Text = row.DM;
        txtDutyRoomOfficer.Text = row.DRO;
    }

    //private void SetMode()
    //{        
    //    if (_curEditMode==EnEditMode.CreateMode){
    //        SetReadOnly(false);
    //    }else if (_curEditMode==EnEditMode.EditMode){
    //        SetReadOnly(false);
    //    }else if (_curEditMode==EnEditMode.ViewMode){
    //        SetReadOnly(true);
    //    }else{
    //        SetReadOnly(true);
    //    }
    //}

    private void SetReadOnly(bool readOnlyMode)
    {
        SetRSMReadOnly(readOnlyMode);
        SetCheckListReadOnly(readOnlyMode);
        SetOtherReadOnly(readOnlyMode);

        txtGenComments.ReadOnly = readOnlyMode;
        txtBPTReason.ReadOnly = readOnlyMode;
        txtDutyManager.ReadOnly = readOnlyMode;
        txtDutyRoomOfficer.ReadOnly = readOnlyMode;
    }

	#region Logging
	private static void LogMsg(string msg)
	{
		try
		{
			MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("WEB_ASR", msg);
		}
		catch (Exception)
		{
			UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("WEB_ASR", msg);
		}
	}

	private static void LogTraceMsg(string msg)
	{
		LogMsg(msg);
	}
	#endregion

    private void SaveInfo(bool draft)
    {
		try
		{
			//DSApronServiceRpt dsASR = new DSApronServiceRpt();
			LogMsg("Save:" + draft);
			string errMsg = "";
			if (txtGenComments.Text.Trim().Length > 4000) errMsg += "\n General Comments exceed the limit 4000 character.";
			if (txtBPTReason.Text.Trim().Length > 2000) errMsg += "\n Reason for BPT Failuer exceed the limit 2000 character.";
			if (errMsg != "")
			{
				WebUtil.AlertMsg(Page, errMsg);
			}
			else
			{
				if (DsASR.ASR.Rows.Count > 0)
				{
					DSApronServiceRpt.ASRRow row = (DSApronServiceRpt.ASRRow)DsASR.ASR.Rows[0];
					AssignDataFromCheckList(row);
					AssignDataFromOther(row);
					AssignDataFromRmk(row);
					AssignDataFromRSM(row);
					if (draft == true)
					{
						row.DRAFT = "Y";
					}
					else
					{
						row.DRAFT = "N";
					}
					CtrlAsr.SaveApronServiceReport(null, flightUrNo, DsASR, CurUser);
				}
			}
		}
		catch (Exception ex)
		{
			LogMsg("SaveInfo:Err:" + ex.Message);
			WebUtil.AlertMsg(Page,"Fail to save.");
		}
    }

    #region RSM Information
    private void SetRSMReadOnly(bool readOnlyMode)
    {
        //RSM
        txtPLBDoorA.ReadOnly = readOnlyMode;
        txtPLBDoorAName.ReadOnly = readOnlyMode;
        txtPLBDoorB.ReadOnly = readOnlyMode;
        txtPLBDoorBName.ReadOnly = readOnlyMode;
        txtJCLFwd.ReadOnly = readOnlyMode;
        txtJCLFwdName.ReadOnly = readOnlyMode;
        txtJCLAft.ReadOnly = readOnlyMode;
        txtJCLAftName.ReadOnly = readOnlyMode;
        txtTRNo.ReadOnly = readOnlyMode;
        txtTRNoName.ReadOnly = readOnlyMode;
        txtSLFwd.ReadOnly = readOnlyMode;
        txtSLFwdName.ReadOnly = readOnlyMode;
        txtSLAft.ReadOnly = readOnlyMode;
        txtSLAftName.ReadOnly = readOnlyMode;
        ddOpMode.Enabled = !readOnlyMode;
        ddLoadChange.Enabled = !readOnlyMode;
    }


    private string GetRSMInfo2Fields(string rsmInfo, out string name)
    {
        name = "";
        char[] sep = {SEPERATOR};
        string[] str = rsmInfo.Split(sep);
        string vehNo = rsmInfo;

        if (str.Length > 1)
        {
            vehNo = str[0];
            name = str[1];
        }
        return vehNo;
    }

    private void ShowRSMInfo(DSApronServiceRpt.ASRRow row)
    {

        string name = "";

        txtPLBDoorA.Text = GetRSMInfo2Fields(row.PLBA, out name);
        txtPLBDoorAName.Text = name;

        txtPLBDoorB.Text = GetRSMInfo2Fields(row.PLBB, out name);
        txtPLBDoorBName.Text = name;

        txtJCLFwd.Text = GetRSMInfo2Fields(row.JCLFWD, out name);
        txtJCLFwdName.Text = name;

        txtJCLAft.Text = GetRSMInfo2Fields(row.JCLAFT, out name);
        txtJCLAftName.Text = name;

        txtTRNo.Text = GetRSMInfo2Fields(row.TRNO, out name);
        txtTRNoName.Text = name;

        txtSLFwd.Text = GetRSMInfo2Fields(row.SLFWD, out name);
        txtSLFwdName.Text = name;

        txtSLAft.Text = GetRSMInfo2Fields(row.SLAFT, out name);
        txtSLAftName.Text = name;
     
        //txtPLBDoorA.Text = row.PLBA;
        //txtPLBDoorB.Text = row.PLBB;
        //txtJCLFwd.Text = row.JCLFWD;
        //txtJCLAft.Text = row.JCLAFT;
        //txtTRNo.Text = row.TRNO;
        //txtSLFwd.Text = row.SLFWD;
        //txtSLAft.Text = row.SLAFT;
        SetDropDownData(ddOpMode, row.MODE);
        SetDropDownData(ddLoadChange, row.LOAD);
    }

    private void AssignDataFromRSM(DSApronServiceRpt.ASRRow row)
    {
        row.PLBA = iTUtils.removeBadCharacters(txtPLBDoorA.Text.Trim() + SEPERATOR + txtPLBDoorAName.Text.Trim());
        row.PLBB = iTUtils.removeBadCharacters(txtPLBDoorB.Text.Trim()+ SEPERATOR + txtPLBDoorBName.Text.Trim());
        row.JCLFWD = iTUtils.removeBadCharacters(txtJCLFwd.Text.Trim()+ SEPERATOR + txtJCLFwdName.Text.Trim());
        row.JCLAFT = iTUtils.removeBadCharacters(txtJCLAft.Text.Trim()+ SEPERATOR + txtJCLAftName.Text.Trim());
        row.TRNO = iTUtils.removeBadCharacters(txtTRNo.Text.Trim()+ SEPERATOR + txtTRNoName.Text.Trim());
        row.SLFWD = iTUtils.removeBadCharacters(txtSLFwd.Text.Trim()+ SEPERATOR + txtSLFwdName.Text.Trim());
        row.SLAFT = iTUtils.removeBadCharacters(txtSLAft.Text.Trim() + SEPERATOR + txtSLAftName.Text.Trim());


        //row.PLBA = txtPLBDoorA.Text.Trim();
        //row.PLBB = txtPLBDoorB.Text.Trim();
        //row.JCLFWD = txtJCLFwd.Text.Trim();
        //row.JCLAFT = txtJCLAft.Text.Trim();
        //row.TRNO = txtTRNo.Text.Trim();
        //row.SLFWD = txtSLFwd.Text.Trim();
        //row.SLAFT = txtSLAft.Text.Trim();
        row.MODE = ddOpMode.SelectedValue;
        row.LOAD = ddLoadChange.SelectedValue;
    }

    #endregion

    #region Check List Information
    private void SetCheckListReadOnly(bool readOnlyMode)
    {
        ////Checklist
        ckbCHKBrief.Enabled = !readOnlyMode;
        ckbCHKSafetyGear.Enabled = !readOnlyMode;
        ckbCHKCone.Enabled = !readOnlyMode;
        ckbCHKRailsLower.Enabled = !readOnlyMode;
        ckbCHKSkyloader.Enabled = !readOnlyMode;

        ////I've checked
        ckbCHKAerobridge.Enabled = !readOnlyMode;
        ckbCHKBody.Enabled = !readOnlyMode;
        ckbCHKDG.Enabled = !readOnlyMode;
        ckbCHKDGLoaded.Enabled = !readOnlyMode;
        ckbCHKULD.Enabled = !readOnlyMode;
    }

    private void ShowCheckList(DSApronServiceRpt.ASRRow row)
    {
        SetCheckBoxData(ckbCHKBrief, row.CBRIEF);
        SetCheckBoxData(ckbCHKSafetyGear, row.CSAFE);
        SetCheckBoxData(ckbCHKCone, row.CCONE);
        SetCheckBoxData(ckbCHKRailsLower, row.CLOW);
        SetCheckBoxData(ckbCHKSkyloader, row.CEXT);

        SetCheckBoxData(ckbCHKAerobridge, row.CBRTEST);
        SetCheckBoxData(ckbCHKBody, row.CDMGBDY);
        SetCheckBoxData(ckbCHKDG, row.CDMGDG);
        SetCheckBoxData(ckbCHKDGLoaded, row.CDG);
        SetCheckBoxData(ckbCHKULD, row.CULD);
    }

    private void AssignDataFromCheckList(DSApronServiceRpt.ASRRow row)
    {
        row.CBRIEF = GetCheckBoxData(ckbCHKBrief);
        row.CSAFE = GetCheckBoxData(ckbCHKSafetyGear);
        row.CCONE = GetCheckBoxData(ckbCHKCone);
        row.CLOW = GetCheckBoxData(ckbCHKRailsLower);
        row.CEXT = GetCheckBoxData(ckbCHKSkyloader);
        
        row.CBRTEST = GetCheckBoxData(ckbCHKAerobridge);
        row.CDMGBDY = GetCheckBoxData(ckbCHKBody);
        row.CDMGDG = GetCheckBoxData(ckbCHKDG);
        row.CDG = GetCheckBoxData(ckbCHKDGLoaded);
        row.CULD = GetCheckBoxData(ckbCHKULD);
    }
    #endregion

    #region Other Information
    private void SetOtherReadOnly(bool readOnlyMode)
    {
        ////Other
        ckbICAO.Enabled = !readOnlyMode;
        ckbBreak.Enabled = !readOnlyMode;
        ckbOthCPM.Enabled = !readOnlyMode;
        ckbOthWrongBag.Enabled = !readOnlyMode;
        ckbOthWeather.Enabled = !readOnlyMode;
        ckbOthLighting.Enabled = !readOnlyMode;
        ckbOthPoorBay.Enabled = !readOnlyMode;
        ckbOthOverStowing.Enabled = !readOnlyMode;
        ckbOthFOD.Enabled = !readOnlyMode;
        ckbOthInFlight.Enabled = !readOnlyMode;
        ckbOthDoorJam.Enabled = !readOnlyMode;
        ckbOthGSE.Enabled = !readOnlyMode;
        ckbOthDamageContainer.Enabled = !readOnlyMode;
        ckbOthDangerCargo.Enabled = !readOnlyMode;
        ckbOthBlock.Enabled = !readOnlyMode;

        ////Other-Description
        txtOthICAO.ReadOnly = readOnlyMode;
        txtOthBreak.ReadOnly = readOnlyMode;
        txtOthCPM.ReadOnly = readOnlyMode;
        txtOthWrongBag.ReadOnly = readOnlyMode;
        txtOthWeather.ReadOnly = readOnlyMode;
        txtOthLighting.ReadOnly = readOnlyMode;
        txtOthPoorBay.ReadOnly = readOnlyMode;
        txtOthOverStowing.ReadOnly = readOnlyMode;
        txtOthFOD.ReadOnly = readOnlyMode;
        txtOthInFlight.ReadOnly = readOnlyMode;
        txtOthDoorJam.ReadOnly = readOnlyMode;
        txtOthGSE.ReadOnly = readOnlyMode;
        txtOthDamageContainer.ReadOnly = readOnlyMode;
        txtOthDangerCargo.ReadOnly = readOnlyMode;
        txtOthBlock.ReadOnly = readOnlyMode;
    }

    private void ShowOthers(DSApronServiceRpt.ASRRow row)
    {
        SetCheckBoxData(ckbICAO, row.OICAO);
        txtOthICAO.Text = row.OICAO_D;
        SetCheckBoxData(ckbBreak, row.OBRK);
        txtOthBreak.Text = row.OBRK_D;
        SetCheckBoxData(ckbOthCPM, row.OCPM);
        txtOthCPM.Text = row.OCPM_D;
        SetCheckBoxData(ckbOthWrongBag, row.OBG);
        txtOthWrongBag.Text = row.OBG_D;
        SetCheckBoxData(ckbOthWeather, row.OWETH);
        txtOthWeather.Text = row.OWETH_D;
        SetCheckBoxData(ckbOthLighting, row.OLGHT);
        txtOthLighting.Text = row.OLGHT_D;
        SetCheckBoxData(ckbOthPoorBay, row.OBAY);
        txtOthPoorBay.Text = row.OBAY_D;
        SetCheckBoxData(ckbOthOverStowing, row.OOVR);
        txtOthOverStowing.Text = row.OOVR_D;
        SetCheckBoxData(ckbOthFOD, row.OFOD);
        txtOthFOD.Text = row.OFOD_D;
        SetCheckBoxData(ckbOthInFlight, row.OILS);
        txtOthInFlight.Text = row.OILS_D;
        SetCheckBoxData(ckbOthDoorJam, row.OJAM);
        txtOthDoorJam.Text = row.OJAM_D;
        SetCheckBoxData(ckbOthGSE, row.OGSE);
        txtOthGSE.Text = row.OGSE_D;
        SetCheckBoxData(ckbOthDamageContainer, row.OWARP);
        txtOthDamageContainer.Text = row.OWARP_D;
        SetCheckBoxData(ckbOthDangerCargo, row.ODNG);
        txtOthDangerCargo.Text = row.ODNG_D;
        SetCheckBoxData(ckbOthBlock, row.OBLK);
        txtOthBlock.Text = row.OBLK_D;
    }

    private void AssignDataFromOther(DSApronServiceRpt.ASRRow row)
    {
        row.OICAO = GetCheckBoxData(ckbICAO);
        row.OICAO_D = iTUtils.removeBadCharacters(txtOthICAO.Text.Trim());
        row.OBRK = GetCheckBoxData(ckbBreak);
        row.OBRK_D = iTUtils.removeBadCharacters(txtOthBreak.Text.Trim());
        row.OCPM = GetCheckBoxData(ckbOthCPM);
        row.OCPM_D = iTUtils.removeBadCharacters(txtOthCPM.Text.Trim());
        row.OBG = GetCheckBoxData(ckbOthWrongBag);
        row.OBG_D = iTUtils.removeBadCharacters(txtOthWrongBag.Text.Trim());
        row.OWETH = GetCheckBoxData(ckbOthWeather);
        row.OWETH_D = iTUtils.removeBadCharacters(txtOthWeather.Text.Trim());
        row.OLGHT = GetCheckBoxData(ckbOthLighting);
        row.OLGHT_D = iTUtils.removeBadCharacters(txtOthLighting.Text.Trim());
        row.OBAY = GetCheckBoxData(ckbOthPoorBay);
        row.OBAY_D = iTUtils.removeBadCharacters(txtOthPoorBay.Text.Trim());
        row.OOVR = GetCheckBoxData(ckbOthOverStowing);
        row.OOVR_D = iTUtils.removeBadCharacters(txtOthOverStowing.Text.Trim());
        row.OFOD = GetCheckBoxData(ckbOthFOD);
        row.OFOD_D = iTUtils.removeBadCharacters(txtOthFOD.Text.Trim());
        row.OILS = GetCheckBoxData(ckbOthInFlight);
        row.OILS_D = iTUtils.removeBadCharacters(txtOthInFlight.Text.Trim());
        row.OJAM = GetCheckBoxData(ckbOthDoorJam);
        row.OJAM_D = iTUtils.removeBadCharacters(txtOthDoorJam.Text.Trim());
        row.OGSE = GetCheckBoxData(ckbOthGSE);
        row.OGSE_D = iTUtils.removeBadCharacters(txtOthGSE.Text.Trim());
        row.OWARP = GetCheckBoxData(ckbOthDamageContainer);
        row.OWARP_D = iTUtils.removeBadCharacters(txtOthDamageContainer.Text.Trim());
        row.ODNG = GetCheckBoxData(ckbOthDangerCargo);
        row.ODNG_D = iTUtils.removeBadCharacters(txtOthDangerCargo.Text.Trim());
        row.OBLK = GetCheckBoxData(ckbOthBlock);
        row.OBLK_D = iTUtils.removeBadCharacters(txtOthBlock.Text.Trim());
    }
    #endregion

    private void AssignDataFromRmk(DSApronServiceRpt.ASRRow row)
    {
        row.GENCMT = iTUtils.removeBadCharacters(txtGenComments.Text.Trim());
        row.RSNBPT = iTUtils.removeBadCharacters(txtBPTReason.Text.Trim());
        row.DM = iTUtils.removeBadCharacters(txtDutyManager.Text.Trim());
        row.DRO = iTUtils.removeBadCharacters(txtDutyRoomOfficer.Text.Trim());
    }

    private string ConvUfisTimeToTimeString(string ufisTimeString)
    {
        string time = "";
        try
        {
            if ((ufisTimeString != null) && (ufisTimeString != ""))
            {
                time = UtilTime.ConvUfisDateTimeStringToDisplayTime(ufisTimeString);
            }
        }
        catch (Exception)
        {
        }
        return time;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        SaveInfo(false);
        ShowInfo();
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        //ShowInfo();
        string ad = "";
        string ab = "";
        try
        {
            ad = Request.Params["AD"];
            ab = Request.Params["AB"];
        }
        catch (Exception)
        {
        }
        string param = "";
        if (ad != "")
        {
            if ((ad != "A") && (ad != "B")) ad = "A";
            param = "?AD=" + ad + "&AB=A";
        }
        Response.Redirect("FlightInfo.aspx" + param, true );
    }
    //protected void btnSaveAsDraft_Click(object sender, EventArgs e)
    //{
    //    SaveInfo(true);
    //}
}
