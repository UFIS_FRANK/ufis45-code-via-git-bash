using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;

using UFIS.Web.ITrek.WebMgr;

using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.Util;

public partial class Web_Wu_WuMsgInBox : System.Web.UI.UserControl
{

   public delegate void ShowMessageHandler(object sender, string msgId);
   public event ShowMessageHandler OnClickShowMessage;

   protected override void OnLoad(EventArgs e)
   {
      base.OnLoad(e);
   }

   protected override void OnPreRender(EventArgs e)
   {
      base.OnPreRender(e);
      Refresh();
   }

   public Unit Width
   {
      get { return pnlMsg.Width; }
      set
      {
         pnlMsg.Width = value;
      }
   }

   public Unit Height
   {
      get { return pnlMsg.Height; }
      set { pnlMsg.Height = value; }
   }

   private void DisplayData(string userId, List<string> arrGroupList, int maxMsgCnt, EnComposeMsgType msgType)
   {
      ArrayList arr = null;
      if (arrGroupList != null)
         arr = new ArrayList(arrGroupList);
      try
      {
         DSMsg dsInMsg;
         dsInMsg = CtrlMsg.RetrieveInMessages(userId, arr, maxMsgCnt, msgType);
         DataView dv = dsInMsg.MSGTO.DefaultView;
         dv.Sort = "SENT_DT DESC, URNO";
         gvInBox.DataSource = dv;
         gvInBox.DataBind();
      }
      catch (Exception ex)
      {
         LogMsg("ShowInBox:Err:" + ex.Message);
      }
   }

   public void PopulateData(string userId, List<string> arrGroupList, int maxMsgCnt, EnComposeMsgType msgType)
   {

      CurInfo info = new CurInfo(userId, arrGroupList, maxMsgCnt, msgType);
      Info = info;
      //DisplayData(userId, arrGroupList, maxMsgCnt);
   }

   public void Refresh()
   {
      CurInfo info = Info;
      if (info != null)
         DisplayData(info._msgInUserId, info._arrGroupList, info._maxMsgCnt, info._msgType);
   }

   [Serializable]
   private class CurInfo
   {
      public string _msgInUserId;
      public List<string> _arrGroupList;
      public int _maxMsgCnt;
      public EnComposeMsgType _msgType;

      public CurInfo(string userId, List<string> arrGroupList, int maxMsgCnt, EnComposeMsgType msgType)
      {
         _msgInUserId = userId;
         _arrGroupList = arrGroupList;
         _maxMsgCnt = maxMsgCnt;
         _msgType = msgType;
      }
   }

   private CurInfo _info = null;
   private const string VSMSGINBOX = "VSMSGINBOX";

   private CurInfo Info
   {
      get
      {
         if (_info == null)
         {
            try
            {
               _info = (CurInfo)ViewState[VSMSGINBOX];
            }
            catch (Exception)
            {
            }
         }
         return _info;
      }
      set
      {
         _info = value;
         ViewState[VSMSGINBOX] = _info;
      }
   }

   private void LogMsg(string msg)
   {
      UtilLog.LogToTraceFile("WuMsgInBox", msg);
   }

   protected void gvInBox_RowDataBound(object sender, GridViewRowEventArgs e)
   {
      if (e.Row.RowType == DataControlRowType.DataRow)
      {
         bool bold = false;
         string rcv = DataBinder.Eval(e.Row.DataItem, "RCV_DT").ToString().Trim();
         if (rcv == "")
         {//Not read message
            bold = true;
         }

         for (int i = 0; i < 3; i++)
         {
            e.Row.Cells[i].Font.Bold = bold;
         }
      }
   }
   protected void gvInBox_RowCommand(object sender, GridViewCommandEventArgs e)
   {
      string st = e.CommandName;
      if (st == "InDet")
      {
         int idx = Convert.ToInt32(e.CommandArgument.ToString());
         string msgId = ((GridView)sender).DataKeys[idx].Value.ToString();
         //string st1 = sender.ToString();
         //wuMsgDet1.ShowMsg(msgId, "I");//Show incoming message
         //wuMsgDet1.Visible = true;
         if (OnClickShowMessage != null)
         {
            OnClickShowMessage(this, msgId);
         }
      }
   }
}