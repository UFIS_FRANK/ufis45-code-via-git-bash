<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WuBABelt.ascx.cs" Inherits="Web_iTrekB_WuBABelt" %>
<%@ Register Src="WuBAFlight.ascx" TagName="WuBAFlight" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<div style="position: relative; left: 0px;" class="HeadingBar">
<table style="width: 100%;">
    <tr>
        <td style="width: 600px">
        <asp:Panel ID="pnlHead" runat="server" OnLoad="pnlHead_Load" >
        <asp:ImageButton ID="ibExpCol" runat="server" ImageUrl="~/Images/collapse.jpg" AlternateText="Show Detail" CausesValidation="False" />
            <asp:Label ID="lblBInfo" runat="server" Text=""></asp:Label>
        </asp:Panel>
        </td>
        <td>
        </td>                        
    </tr>
</table>
</div>
<asp:Panel ID="pnlDet" runat="server" style="position: relative; left: 8px;" CssClass="cpBody">
<asp:PlaceHolder ID="phFlights" runat="server" EnableViewState="true"></asp:PlaceHolder>       
    <asp:Label ID="lblNoFlight" runat="server" Text="No Flight" Visible="False" EnableViewState="False"></asp:Label></asp:Panel>
<cc1:CollapsiblePanelExtender ID="cpeFlight" runat="server" 
    TargetControlID="pnlDet"
    ExpandControlID="pnlHead"
    CollapseControlID="pnlHead" 
    Collapsed="false"
    TextLabelID=""
    ImageControlID="ibExpCol"        
    ExpandedText="(Hide Details...)"
    CollapsedText="(Show Details...)"
    ExpandedImage="~/images/collapse.jpg"
    CollapsedImage="~/images/expand.jpg"
    CollapsedSize="0"
    SuppressPostBack="true"    
/>
<asp:HiddenField ID="hfCpeStat" runat="server" OnValueChanged="hfCpeStat_ValueChanged" Value="0"/>

