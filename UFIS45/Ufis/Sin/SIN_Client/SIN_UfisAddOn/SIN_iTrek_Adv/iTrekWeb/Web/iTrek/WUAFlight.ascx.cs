using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;

public partial class Web_iTrek_WUAFlight : System.Web.UI.UserControl
{
    private string _URNO = "";
    private string _FLNO = "";

   protected override void OnInit(EventArgs e)
   {
      base.OnInit(e);
      Timer1.Interval = CtrlConfig.GetWebRefreshTiming() * 1000;
   }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
//            string st = "Not post back";
          
        }
    }

    public void SetFlight(string urNo, string flightNo)
    {
        _URNO = urNo;
        _FLNO = flightNo;
        ViewState["URNO"] = urNo;
        ViewState["FLNO"] = flightNo;
    }

    public void ShowFlightInfo()
    {
        DSFlight ds = null;

        try
        {
//            ds = CtrlFlight.RetrieveFlightsByUrNo_FlightNo(_URNO, _FLNO);
            ds = CtrlFlight.RetrieveFlightsByFlightId(_URNO);

            DataView dv = ds.FLIGHT.DefaultView;
            dv.RowFilter = "FLNO='" + _FLNO + "'";
            if (dv.Count > 0)
            {
                ShowDetail(dv[0]);
            }
        }
        catch (Exception)
        {
            
            throw;
        }
    }

    private void ShowDetail(DataRowView row)
    {
        this.lblALO.Text = Convert.ToString(row["AO"]);
        this.lblATA.Text = String.Format("{0:hhmm}",row["ATA"]);
        this.lblBay.Text = Convert.ToString(row["BAY"]);
        this.lblETA.Text = String.Format("{0:hhmm}", row["ETA"]);
        this.lblFirstTrip.Text = String.Format("{0:hhmm}", row["AP_FST_TRP"]);
        this.lblFlight.Text = Convert.ToString(row["FLNO"]);
        this.lblLastTrip.Text = String.Format("{0:hhmm}", row["AP_LST_TRP"]);
        this.lblMAB.Text = String.Format("{0:hhmm}", row["MAB"]);
        this.lblPlb.Text = String.Format("{0:hhmm}", row["PLB"]);
        this.lblSTA.Text = String.Format("{0:hhmm}", row["STA"]);
        this.lblTbsUp.Text = String.Format("{0:hhmm}", row["TBS_UP"]);
        this.lblUnlEnd.Text = String.Format("{0:hhmm}", row["UNL_END"]);
        this.lblUnlStart.Text = String.Format("{0:hhmm}", row["UNL_STRT"]);
        if (!IsPostBack)
        {
            CopyForEntry();
        }
    }

    private void CopyForEntry()
    {
        this.txtFirstTrip.Text = this.lblFirstTrip.Text;
        this.txtLastTrip.Text = this.lblLastTrip.Text;
        this.txtMAB.Text = this.lblMAB.Text;
        this.txtPlb.Text = this.lblPlb.Text;
        this.txtTbsUp.Text = this.lblTbsUp.Text;
        this.txtUnlEnd.Text = this.lblUnlEnd.Text;
        this.txtUnlStart.Text = this.lblUnlStart.Text;
    }

    public void ShowFlightInfo(string urNo, string flightNo)
    {
        SetFlight(urNo, flightNo);
        ShowFlightInfo();
    }

    //public void ShowFlightInfo( string flNo, DateTime dtDate )
    //{
    //    DSFlight ds = null;

    //    ds = CtrlFlight.RetrieveFlightsByFlightNo(flNo, dtDate);

    //    DataView dv = ds.FLIGHT.DefaultView;
    //    dv.RowFilter = "FLNO='" + flNo + "'";
    //    this.lblALO.Text = Convert.ToString( dv.Table.Rows[0]["AO"]);
    //    ShowDetail(dv.Table.Rows[0]);
    //}

    public void ShowTextBoxes(bool show)
    {
        this.txtFirstTrip.Visible = show;
        this.txtLastTrip.Visible = show;
        this.txtMAB.Visible = show;
        this.txtPlb.Visible = show;
        this.txtTbsUp.Visible = show;
        this.txtUnlEnd.Visible = show;
        this.txtUnlStart.Visible = show;
    }
    public string GetFirstTripTime() { return txtFirstTrip.Text; }
    public void SetFirstTripTime(string value) { txtFirstTrip.Text=value; }
    public string GetLastTripTime() { return txtLastTrip.Text; }
    public void SetLastTripTime(string value) { txtLastTrip.Text = value; }
    public string GetMABTime() { return txtMAB.Text; }
    public void SetMABTime(string value) { txtMAB.Text = value; }
    public string GetPlbTime() { return txtPlb.Text; }
    public void SetPlbTime(string value) { txtPlb.Text = value; }
    public string GetTbsUpTime() { return txtTbsUp.Text; }
    public void SetTbsUpTime(string value) { txtTbsUp.Text = value; }
    public string GetUnlEndTime() { return txtUnlEnd.Text; }
    public void SetUnlEndTime(string value) { txtUnlEnd.Text = value; }
    public string GetUnlStartTime() { return txtUnlStart.Text; }
    public void SetUnlStartTime(string value) { txtUnlStart.Text = value; }

    public void SaveData()
    {

    }
    protected void txtMAB_TextChanged(object sender, EventArgs e)
    {
        ShowFlightInfo();
        this.txtTbsUp.Text = "ABC";
    }

    protected void Timer1_Tick(object sender, EventArgs e)
    {
        string flightNo = Convert.ToString(ViewState["FLNO"]);
        string urNo = Convert.ToString(ViewState["URNO"]);
        ShowFlightInfo(urNo,flightNo);
        this.txtTbsUp.Text = "ABC";
    }
}
