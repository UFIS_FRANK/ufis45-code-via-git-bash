using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using MMSoftware.MMWebUtil;
using UFIS.Web.ITrek.Sec;

public partial class Web_iTrek_PrtBagTimingSummRpt : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack){
            ShowRpt();
        }
    }

    void ShowRpt()
    {
        try
        {
            string terminalId = Request.Params["termId"];
            string terminalName = Request.Params["termName"];
            string frDate = Request.Params["frDate"];
            string frTime = Request.Params["frTime"];
            string toDate = Request.Params["toDate"];
            string toTime = Request.Params["toTime"];

            WUBagTimingSummRpt1.ShowReport(terminalId, terminalName,
                frDate, frTime,
				toDate, toTime, CtrlSec.GetInstance().AuthenticateUser(Session, Response).UserId);

            WebUtil.SetJavaScriptAtBottom(Page, "window.print();");
        }
        catch (ApplicationException ex)
        {
            WebUtil.AlertMsg(Page, ex.Message);
        }
        catch (Exception)
        {
            WebUtil.AlertMsg(Page, "Unable to get the report.");
        }
    }
}
