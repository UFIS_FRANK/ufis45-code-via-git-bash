using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;

using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.Util;
using UFIS.Web.ITrek.WebMgr;
using UFIS.Web.ITrek.Sec;
using MM.UtilLib;

//public partial class Web_iTrekB_BATimeEntry : System.Web.UI.Page
public partial class Web_iTrekB_BATimeEntry : UFIS.Web.ITrek.Base.BaseWebPage
{
	protected override void OnInit(EventArgs e)
	{
		base.OnInit(e);
		wuTime1.TimingSendClick += new Web_Wu_WuTime.TimingSendHandler(wuTime1_TimingSendClick);
	}

	protected override void OnLoad(EventArgs e)
	{
		base.OnLoad(e);
		EntBagArrFlightSelectedUld obj = MgrSess.GetBagArrCurrentSel(this.Session);
		bool hasAccess = false;
		switch (obj.BagArrFlightAction)
		{
			//case EnBagArrFlightActionType.Receive:
			//    break;
			case EnBagArrFlightActionType.FirstBag:
				hasAccess = CtrlSec.GetInstance().HasAccessToBAFirstBagTimingUpdate(LoginedUser);
				break;
			case EnBagArrFlightActionType.LastBag:
				hasAccess = CtrlSec.GetInstance().HasAccessToBALastBagTimingUpdate(LoginedUser);
				break;
			//case EnBagArrFlightActionType.ShowCntr:
			//    break;
			//case EnBagArrFlightActionType.FlMsg:
			//    break;
			//case EnBagArrFlightActionType.ShowRecvdCntr:
			//    break;
			//case EnBagArrFlightActionType.None:
			//    break;
			default:
				break;
		}
		if (hasAccess)
		{
			if ((!IsPostBack) && (!Page.IsCallback))
			{
				PopulateData();
			}
		}
		else
		{
			DoUnAuthorise();
		}
	}

	protected override void OnPreRender(EventArgs e)
	{
		base.OnPreRender(e);
		//string msg = lblMsg.Text;
	}

	void wuTime1_TimingSendClick(object sender, UFIS.ITrekLib.EventArg.EaTextEventArgs e)
	{
		lblMsg.Text = "";
		lblMsg.Visible = false;
		string stTime = e.Text;
		string errMsg = "";
		string stDateTime = "";
		string url = "";
		if (!string.IsNullOrEmpty(stTime))
		{
			EntBagArrFlightSelectedUld obj = MgrSess.GetBagArrCurrentSel(this.Session);
			if ((obj.FlightId != null) && (obj.FlightId.Trim() != ""))
			{
				try
				{
					DateTime dt = CtrlFlight.ComputeFlightRelDateTime(obj.FlightId, stTime);
					if (dt > DateTime.Now) errMsg = "Future Time is not allowed.";

					stDateTime = UtilTime.ConvDateTimeToUfisFullDTString(dt);
				}
				catch (ApplicationException ex)
				{
					errMsg = ex.Message;
				}
				catch (Exception)
				{
					errMsg = "Invalid Time";
				}
				if (errMsg == "")
				{
					try
					{
						switch (obj.BagArrFlightAction)
						{
							case EnBagArrFlightActionType.FirstBag:
								SendFirstBagTiming(obj.FlightId, stDateTime);
								url = MgrNavigation.UrlNavBATimeEntryPage(EnumBAAction.BAFirstBagSendTime);
								break;
							case EnBagArrFlightActionType.LastBag:
								SendLastBagTiming(obj.FlightId, stDateTime);
								url = MgrNavigation.UrlNavBATimeEntryPage(EnumBAAction.BALastBagSendTime);
								break;
							default:
								break;
						}
					}
					catch (ApplicationException ex)
					{
						errMsg = ex.Message;
					}
					catch (Exception)
					{
						errMsg = "Unable to save";
					}
				}

				if (errMsg != "")
					ShowErrMsg(errMsg);

			}
		}
		if (!string.IsNullOrEmpty(url)) Response.Redirect(url, true);
	}


	private void ShowErrMsg(string msg)
	{
		lblMsg.Text = msg;
		lblMsg.Visible = true;
		if (!ClientScript.IsStartupScriptRegistered("ERRMSG"))
		{
			ClientScript.RegisterStartupScript(this.GetType(), "ERRMSG", "alert('" + msg + "');", true);
		}
	}

	private void PopulateData()
	{
		EntBagArrFlightSelectedUld obj = MgrSess.GetBagArrCurrentSel(this.Session);
		if (obj == null)
		{
		}
		else
		{
			if ((obj.FlightId == null) || (obj.FlightId.Trim() == ""))
			{
				//Response.Redirect(Request.UrlReferrer.AbsoluteUri, true);
				this.lblHead.Text = "No Flight Information!!!";

				wuBAFlight1.Visible = false;
				wuTime1.Visible = false;
			}
			else
			{
				wuBAFlight1.PopulateData(obj.FlightId, obj.BagArrType, null, obj.SelectedUldIds, obj.SelectedTrip);
				wuBAFlight1.ShowDetail = false;
				wuBAFlight1.Collapsed = true;
				wuBAFlight1.EnableUldSelect = false;
				wuBAFlight1.Visible = true;
				wuTime1.Visible = true;
			}
			switch (obj.BagArrFlightAction)
			{
				//case EnBagArrFlightActionType.Receive:
				//    break;
				case EnBagArrFlightActionType.FirstBag:
					lblHead.Text = "First Bag Timing";
					break;
				case EnBagArrFlightActionType.LastBag:
					lblHead.Text = "Last Bag Timing";
					break;
				//case EnBagArrFlightActionType.ShowCntr:
				//    break;
				//case EnBagArrFlightActionType.FlMsg:
				//    break;
				//case EnBagArrFlightActionType.None:
				//    lblHead.Text = "";
				//    break;
				default:
					lblHead.Text = "Invalid Timing";
					break;
			}
		}

	}

	private void SendFirstBagTiming(string flightId, string stDateTime)
	{
		CtrlFlight.SaveArrBagTimingByTag(null, flightId, LoginedId, stDateTime, CtrlFlight.BA_FIRST_BAG_TIME_TAG);
	}

	private void SendLastBagTiming(string flightId, string stDateTime)
	{
		try
		{
			//CtrlTrip.UpdateLastTrip(flightId, LoginedId);
			//CtrlFlight.SaveArrBagTimingByTag(flightId, LoginedId, stDateTime, CtrlFlight.BA_LAST_BAG_TIMG_TAG);
			CtrlBagArrival.SendLastBagTiming(null, flightId, stDateTime, LoginedId);
		}
		catch (Exception ex)
		{
			LogMsg("SendLastBagTiming:Err:" + ex.Message);
		}
	}

	private void LogMsg(string msg)
	{
		try
		{
			MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("BATimeEntry", msg);
		}
		catch (Exception)
		{
			UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("BATimeEntry", msg);
		}
	}

	private void ShowErr(string msg)
	{
		lblMsg.Text = msg;
		lblMsg.Visible = true;
	}
}