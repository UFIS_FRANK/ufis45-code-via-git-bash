<%@ Page Language="C#" MasterPageFile="~/Web/iTrekB/Master.master" AutoEventWireup="true" CodeFile="BARecvdCntr.aspx.cs" Inherits="Web_iTrekB_BARecvdCntr" Title="Received Containers" Theme="SATSTheme" %>

<%@ Register Src="WuBAFlight.ascx" TagName="WuBAFlight" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cph1" Runat="Server">
    <uc1:WuBAFlight ID="WuBAFlight1" runat="server" />
    <hr />
    <table>
    <tr><td style="width: 773px">
        <asp:Label ID="lblTripMsg" runat="server" Width="649px"></asp:Label>
    </td></tr>
    <tr><td style="width: 773px">
        <asp:GridView ID="gvTrips" runat="server" Width="771px" AutoGenerateColumns="False" 
        BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" 
        CellPadding="3" ForeColor="Black" GridLines="Vertical" Visible="False">
        <FooterStyle BackColor="#CCCCCC" />
        <Columns>
            <asp:BoundField DataField="ULDN" HeaderText="Container No." />
            <asp:BoundField DataField="VDES" HeaderText="Destination" />
            <asp:BoundField DataField="RCV_DT_TIME" DataFormatString="{0:hhmm}" HeaderText="Receive" HtmlEncode="False" />
            <asp:BoundField DataField="SENT_DT_TIME" DataFormatString="{0:hhmm}" HeaderText="Sent" HtmlEncode="False" />
            <asp:BoundField DataField="STRPNO" HeaderText="Sent Trip No." />
        </Columns>
        <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
        <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="#CCCCCC" />
        </asp:GridView>
    </td></tr>
</table>
</asp:Content>

