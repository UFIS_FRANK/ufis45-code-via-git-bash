using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.Ctrl;
using MMSoftware.MMWebUtil;
using UFIS.ITrekLib.ENT;
using UFIS.Web.ITrek.Sec;
using UFIS.ITrekLib.Util;
using UFIS.Web.ITrek.Misc;

public partial class Web_iTrek_Admin_BaggagePriority : System.Web.UI.Page
{
    private const string PG_NAME = "DefCpmPrio";
    private const string CTRL_VIEW = "Define";


    private void FreezeGridHeader()
    {
            MiscRender.FreezeGridHeader(this.Page, this.GetType(),
                gvBagPriority.ClientID, "WrapperDivCpmPriority",
                hfScrollTop.ClientID, hfScrollTop.Value);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ((MyMasterPage)Master).AutoRefreshMode = false;
        CtrlSec ctrlsec = CtrlSec.GetInstance();
        EntUser user = ctrlsec.AuthenticateUser(Session, Response);
        if (ctrlsec.HasAccessRights(user, PG_NAME, CTRL_VIEW))
        {
            if (!IsPostBack)
            {
                ShowInfo();
            }
            SetFocus(txtAcc);
            FreezeGridHeader();
        }
        else
        {
            DoUnAuthorise();
        }
    }

    private void DoUnAuthorise()
    {
        Response.Redirect("~/UnAuthorised.aspx", true);
    }

    private void ShowInfo()
    {
        try
        {
            DSCpmPriority ds = CtrlCpmPriority.RetrieveAll();
            DataView dv = ds.TPR.DefaultView;
            dv.Sort = "ALC ASC,PR ASC";
            gvBagPriority.DataSource = dv;
            gvBagPriority.DataBind();
        }
        catch (Exception ex)
        {
            LogMsg("ShowInfo:Err:" + ex.Message);
            WebUtil.AlertMsg(Page, "Error in Displaying Information.");
        }
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            int prio = -1;
            try
            {
                prio = Int32.Parse(txtPriority.Text.Trim());
            }
            catch (Exception)
            {
                throw new ApplicationException("Invalid Prioroity");
            }
            CtrlCpmPriority.Create(txtAcc.Text, prio, txtBgType.Text.Trim());
        }
        catch (ApplicationException ex)
        {
            LogMsg("Save:Err:" + ex.Message);
            WebUtil.AlertMsg(this.Page, ex.Message);
        }
        catch (Exception ex)
        {
            LogMsg("Save:Err:" + ex.Message);
            WebUtil.AlertMsg(Page, "Error in saving.");
        }
        ShowInfo();
    }

    protected void gvBagPriority_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int idx = e.RowIndex;
        string msg = "";
        string errMsg = "";
        try
        {
            //msg += "Idx:" + idx + ", " + gvBagPriority.Rows.Count;
            if ((idx >= 0) && (idx < gvBagPriority.Rows.Count))
            {
                DataKey dataKey = gvBagPriority.DataKeys[idx];
                string alc = dataKey.Values[0].ToString();
                string prio = dataKey.Values[1].ToString();
                string bagTypes = ((TextBox)(gvBagPriority.Rows[idx].FindControl("txtGvBagTypes"))).Text.Trim().ToUpper();
                msg += ",alc==>" + alc + ",prio==>" + prio + ",bagTypes==>" + bagTypes;
                CtrlCpmPriority.Update(alc, int.Parse(prio), bagTypes);
            }
        }
        catch (ApplicationException ex)
        {
            errMsg = "Update Error due to : " + ex.Message;
            msg += errMsg;
        }
        catch (Exception ex)
        {
            errMsg = "Error in Updating";
            msg += errMsg + ":" + ex.Message;
        }
        if (msg != "")
        {
            //WebUtil.AlertMsg(Page, msg);
            LogMsg("Update:" + msg);
        }
        if (errMsg!="")
        {
            WebUtil.AlertMsg(Page, errMsg);
        }
        gvBagPriority.EditIndex = -1;
        ShowInfo();
    }

    protected void gvBagPriority_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvBagPriority.EditIndex = -1;
        ShowInfo();
    }
    protected void gvBagPriority_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvBagPriority.EditIndex = e.NewEditIndex;
        ShowInfo();
    }
    protected void gvBagPriority_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int idx = e.RowIndex;
        
        string msg = "";
        string errMsg = "";
        try
        {
            msg = "Idx:" + idx + ", " + gvBagPriority.Rows.Count + "," + e.Keys.Count;
            if ((idx >= 0) && (idx < gvBagPriority.Rows.Count))
            {
                DataKey dataKey = gvBagPriority.DataKeys[idx];
                string alc = dataKey.Values[0].ToString();
                string prio = dataKey.Values[1].ToString();
                //string alc = e.Keys[0].ToString();
                //string prio = e.Keys[1].ToString();
                //string bagTypes = ((TextBox)(gvBagPriority.Rows[idx].FindControl("txtGvBagTypes"))).Text.Trim().ToUpper();
                msg += ",alc==>" + alc + ",prio==>" + prio;
                CtrlCpmPriority.Delete(alc, int.Parse(prio));
            }
        }
        catch (ApplicationException ex)
        {
            errMsg = "Delete Error due to :" + ex.Message;
            msg += errMsg;
        }
        catch (Exception ex)
        {
            errMsg = "Error in Deleting";
            msg += errMsg + ":" + ex.Message;
        }
        if (msg != "")
        {
            LogMsg("Delete:" + msg);
        }
        if (errMsg != "")
        {
            WebUtil.AlertMsg(Page, errMsg);
        }
        gvBagPriority.EditIndex = -1;
        ShowInfo();
    }

    private void LogMsg(string msg)
    {
        UtilLog.LogToTraceFile("BaggagePriority", msg);
    }
}
