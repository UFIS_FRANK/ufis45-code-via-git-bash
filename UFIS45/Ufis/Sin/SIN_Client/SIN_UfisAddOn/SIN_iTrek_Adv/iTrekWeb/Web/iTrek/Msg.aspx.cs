using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;
using UFIS.Web.ITrek.WebMgr;
using UFIS.Web.ITrek.Sec;

using UFIS.Web.ITrek.Base;

public partial class Web_iTrek_Msg : BaseWebPage
{
   protected override void OnPreLoad(EventArgs e)
   {
      base.OnPreLoad(e);
      ((MyMasterPage)Master).AutoRefreshMode = false;
   }

   protected override void OnLoad(EventArgs e)
   {
      base.OnLoad(e);

      if (CtrlSec.GetInstance().HasAccessToMessage(LoginedUser))
      {
         if ((!Page.IsPostBack) && (!Page.IsCallback))
         {
            PopulateData();
         }
      }
      else
      {
         DoUnAuthorise();
      }
   }

   private void PopulateData()
   {
      try
      {
         WuMsg1.PopulateData(UFIS.ITrekLib.ENT.EnComposeMsgType.Personal, null);
         WuMsg1.AdditionalHeight = 130;//height in px;
         //LogMsg("PopulateData:SetAddHeight:" + WuMsg1.AdditionalHeight);
      }
      catch (Exception ex)
      {
         LogMsg("PopulateData:Err:" + ex.Message);
      }
   }

   private void LogMsg(string msg)
   {
      try
      {
         MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("BAMsg", msg);
      }
      catch (Exception)
      {
         UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("BAMsg", msg);
      }
   }
}
