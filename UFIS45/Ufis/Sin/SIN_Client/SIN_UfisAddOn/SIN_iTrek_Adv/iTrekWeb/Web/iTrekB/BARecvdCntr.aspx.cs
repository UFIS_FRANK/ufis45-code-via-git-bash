using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.Ctrl;
using UFIS.Web.ITrek.WebMgr;
using UFIS.Web.ITrek.Sec;

//public partial class Web_iTrekB_BARecvdCntr : System.Web.UI.Page
public partial class Web_iTrekB_BARecvdCntr : UFIS.Web.ITrek.Base.BaseWebPage
{
    protected override void OnLoad(EventArgs e)
    {
        base.OnLoad(e);
        if (CtrlSec.GetInstance().HasAccessBAToShowReceivedContainers(LoginedUser))
        {
            if ((!IsPostBack) && (!Page.IsCallback))
            {
                ShowInfo();
            }
        }
        else
        {
            DoUnAuthorise();
        }
    }

    private void ShowInfo()
    {
        //string flightId = Request.Params["ID"].Trim();
        //EntBagArrTerminal ent = MgrSess.GetBagArrTerminalInfo(Session);
        EntBagArrFlightSelectedUld obj = MgrSess.GetBagArrCurrentSel(this.Session);
        if ((obj.FlightId == null) || (obj.FlightId.Trim() == ""))
        {
            //this.lblMsg.Text = "No Flight Information!!!";
        }
        else
        {
            //PopulateData(flightId, "Received Containers", ent.BagArrType);
            WuBAFlight1.PopulateData(obj.FlightId, obj.BagArrType, null, obj.SelectedUldIds, obj.SelectedTrip);
            WuBAFlight1.Collapsed = true;
            WuBAFlight1.EnableUldSelect = false;
            WuBAFlight1.Visible = true;
            PopulateData(obj.FlightId, "Received Containers", obj.BagArrType);
        }
    }

    /// <summary>
    /// Populate Data To show the Trip and Container Information for given flight Id and Baggage Type
    /// </summary>
    /// <param name="flightId">Flight Id</param>
    /// <param name="tripHeading">Trip Heading</param>
    /// <param name="baType">EnBagArrType Baggage Arrival Type (1.Loca, 2.InterLine, 3.All)</param>
    public void PopulateData(string flightId, string tripHeading, EnBagArrType baType)
    {
        if (!string.IsNullOrEmpty(tripHeading))
        {
            lblTripMsg.Text = tripHeading;
        }
        else
        {
            lblTripMsg.Text = "Received Container Information";
        }

        DSTrip dsTrip = null;
        DSCpm dsCpm = null;

		//string result = CtrlBagArrival.GetTripNContainerInfo(flightId, baType,
		//    ref dsTrip, ref dsCpm);
		CtrlBagArrival.GetTripInfo(flightId, baType, ref dsTrip);

        if ((dsTrip == null) || (dsTrip.TRIP == null) || (dsTrip.TRIP.Rows == null) || (dsTrip.TRIP.Rows.Count < 1))
        {
            gvTrips.Visible = false;
        }
        else
        {
            DataView dv = dsTrip.TRIP.DefaultView;
            dv.Sort = "ULDN ASC, RCV_DT ASC, SENT_DT ASC, STRPNO ASC";
            dv.RowFilter = "(RCV_DT IS NOT NULL) AND (RCV_DT<>'')";
            gvTrips.DataSource = dv;
            gvTrips.DataBind();
            gvTrips.Visible = true;
        }

    }

    /// <summary>
    /// Populate Data to Show the Trip and container information for given flight Id for all baggage type
    /// </summary>
    /// <param name="flightId">Flight Id</param>
    /// <param name="tripHeading">Trip Heading</param>
    public void PopulateData(string flightId, string tripHeading)
    {
        PopulateData(flightId, tripHeading, EnBagArrType.All);
    }    

}
