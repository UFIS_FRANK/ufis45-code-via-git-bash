using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.Ctrl;
using MMSoftware.MMWebUtil;
using UFIS.ITrekLib.ENT;
using UFIS.Web.ITrek.Sec;
using UFIS.ITrekLib.Util;
using UFIS.Web.ITrek.Misc;

public partial class Web_iTrek_Admin_WuBelt : System.Web.UI.UserControl
{
    private void FreezeGridHeader()
    {
        MiscRender.FreezeGridHeader(this.Page, this.GetType(),
            gvBelt.ClientID, "WrapperDivBeltAdmin",
            hfScrollTop.ClientID, hfScrollTop.Value);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if ((!Page.IsPostBack) && (!Page.IsCallback))
        {
            ShowInfo();
        }
    }

    private void ShowInfo()
    {
        try
        {
            DSBelt ds = CtrlTerminal.RetrieveAllBelts();
            DataView dv = ds.BLT.DefaultView;
            dv.Sort = "TMNL ASC,SECT ASC,BLTS";
            gvBelt.DataSource = dv;
            gvBelt.DataBind();
            FreezeGridHeader();
        }
        catch (Exception ex)
        {
            LogMsg("ShowInfo:Err:" + ex.Message);
            WebUtil.AlertMsg(Page, "Error in Displaying Information.");
        }
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            //CtrlTerminal.CreateBelts(WuTerminal1.GetSelectedId(), txtSection.Text.Trim(), txtBelts.Text.Trim());
            CtrlTerminal.CreateBelts(WuTerminal1.GetSelectedId(), txtSection.Text.Trim(), 
                txtBeltsFr.Text.Trim(), txtBeltsTo.Text.Trim());
            this.txtSection.Text = "";
            this.txtBeltsFr.Text = "";
            this.txtBeltsTo.Text = "";
        }
        catch (ApplicationException ex)
        {
            LogMsg("Save:Err:" + ex.Message);
            WebUtil.AlertMsg(this.Page, ex.Message);
        }
        catch (Exception ex)
        {
            LogMsg("Save:Err:" + ex.Message);
            WebUtil.AlertMsg(Page, "Error in saving.");
        }
        ShowInfo();
    }

    protected void gvBelt_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int idx = e.RowIndex;
        string msg = "";
        string errMsg = "";
        try
        {
            if ((idx >= 0) && (idx < gvBelt.Rows.Count))
            {
                DataKey dataKey = gvBelt.DataKeys[idx];
                string termId = dataKey.Values[0].ToString();
                string sectId = dataKey.Values[1].ToString();
                //string belts = ((TextBox)(gvBelt.Rows[idx].FindControl("txtGvBelts"))).Text.Trim().ToUpper();
                //msg += ",termId==>" + termId + ",sectId==>" + sectId + ",belts==>" + belts;
                string frBelt = ((TextBox)(gvBelt.Rows[idx].FindControl("txtGvBeltsFr"))).Text.Trim().ToUpper();
                string toBelt = ((TextBox)(gvBelt.Rows[idx].FindControl("txtGvBeltsTo"))).Text.Trim().ToUpper();
                msg += ",termId==>" + termId + ",sectId==>" + sectId + ",frBelt==>" + frBelt + ",ToBelt==>" + toBelt;
                CtrlTerminal.UpdateBelts(termId, sectId, frBelt, toBelt);
            }
        }
        catch (ApplicationException ex)
        {
            errMsg = "Update Error due to : " + ex.Message;
            msg += errMsg;
        }
        catch (Exception ex)
        {
            errMsg = "Error in Updating";
            msg += errMsg + ":" + ex.Message;
        }
        if (msg != "")
        {
            LogMsg("Update:" + msg);
        }
        if (errMsg != "")
        {
            WebUtil.AlertMsg(Page, errMsg);
        }
        gvBelt.EditIndex = -1;
        ShowInfo();
    }

    protected void gvBelt_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvBelt.EditIndex = -1;
        ShowInfo();
    }
    protected void gvBelt_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvBelt.EditIndex = e.NewEditIndex;
        ShowInfo();
    }
    protected void gvBelt_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int idx = e.RowIndex;

        string msg = "";
        string errMsg = "";
        try
        {
            msg = "Idx:" + idx + ", " + gvBelt.Rows.Count + "," + e.Keys.Count;
            if ((idx >= 0) && (idx < gvBelt.Rows.Count))
            {
                DataKey dataKey = gvBelt.DataKeys[idx];
                string termId = dataKey.Values[0].ToString();
                string sectId = dataKey.Values[1].ToString();
                msg += ",Terminal==>" + termId + ",section==>" + sectId;
                CtrlTerminal.DeleteBelts(termId, sectId);
            }
        }
        catch (ApplicationException ex)
        {
            errMsg = "Delete Error due to :" + ex.Message;
            msg += errMsg;
        }
        catch (Exception ex)
        {
            errMsg = "Error in Deleting";
            msg += errMsg + ":" + ex.Message;
        }
        if (msg != "")
        {
            LogMsg("Delete:" + msg);
        }
        if (errMsg != "")
        {
            WebUtil.AlertMsg(Page, errMsg);
        }
        gvBelt.EditIndex = -1;
        ShowInfo();
    }

    private void LogMsg(string msg)
    {
        UtilLog.LogToTraceFile("Belt", msg);
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        this.txtBeltsFr.Text = "";
        this.txtBeltsTo.Text = "";
        this.txtSection.Text = "";
    }
}
