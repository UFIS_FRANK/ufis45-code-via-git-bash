using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.Ctrl;
using MMSoftware.MMWebUtil;
using UFIS.ITrekLib.ENT;
using UFIS.Web.ITrek.Sec;
using UFIS.ITrekLib.Util;

using UFIS.ITrekLib.CustomException;
using MM.UtilLib;

public partial class Web_iTrek_Admin_MessageLogs : System.Web.UI.Page
{
	private const string PG_NAME = "MsgLogs";
	private const string CTRL_VIEW = "View";

	private const string VS_CUR_SORTBY = "VS_CUR_SORTBY";

	private const string VS_CUR_STARTING_INDEX = "VS_CUR_STARTING_INDEX";
	private const string VS_TOT_CNT = "VS_TOT_CNT";

	private const string JS_SHOW_MSG_DET = "<script type='text/javascript'>var cnt=0; function JsMsgLogDet( urno ){ cnt++; if (cnt>5000) cnt=0; var dt=new Date(); window.showModalDialog('MessageLogDetail.aspx?URNO=' + urno + '&CntNo=' + cnt + '&Dt=' + dt.toGMTString(), '', 'dialogWidth:900px;dialogHeight:400px;status:no;');}</script> ";

	private string CurSortBy
	{
		get
		{
			string st = "SORTON";
			st = ViewState[VS_CUR_SORTBY].ToString();
			if ((st == null) || (st == "")) st = "SORTON";
			return st;
		}
		set
		{
			ViewState[VS_CUR_SORTBY] = value;
		}
	}

	private int CurStartingIndex
	{
		get
		{
			int stIndex = 0;
			try
			{
				stIndex = Convert.ToInt32(ViewState[VS_CUR_STARTING_INDEX]);
			}
			catch (Exception)
			{
			}
			return stIndex;
		}
		set
		{
			if (value < 0) value = 0;
			ViewState[VS_CUR_STARTING_INDEX] = value;
		}
	}

	private void GotoNextPage()
	{
		CurStartingIndex += PageSize;
	}
	private void GotoPreviousPage()
	{
		CurStartingIndex -= PageSize;
	}

	private void GotoPageNo(int pageNo)
	{
		CurStartingIndex = (pageNo - 1) * PageSize;
	}

	private const int MIN_PG_SIZE = 10;
	private const int MAX_PG_SIZE = 20;

	private int PageSize
	{
		get
		{
			int pSize = MIN_PG_SIZE;
			pSize = gvMsgLog.PageSize;
			if (pSize < MIN_PG_SIZE)
			{//Minimum Page Size
				pSize = MIN_PG_SIZE;
			}
			if (pSize > MAX_PG_SIZE)
			{//Maximum Page Size
				pSize = MAX_PG_SIZE;
			}
			gvMsgLog.PageSize = pSize;
			return pSize;
		}
	}

	private int GetPageCount()
	{
		int pCnt = TotRecCnt / PageSize;
		if ((TotRecCnt % PageSize) != 0) pCnt++;
		return pCnt;

	}

	private bool HasPreviousPage()
	{
		bool hasPrevious = false;
		if (TotRecCnt > 0)
		{
			if (CurStartingIndex > 0) hasPrevious = true;
			else hasPrevious = false;
		}
		return hasPrevious;
	}

	private bool HasNextPage()
	{
		bool hasNext = false;
		if (TotRecCnt > 0)
		{
			if ((CurStartingIndex + PageSize) >= TotRecCnt) hasNext = false;
			else hasNext = true;
		}
		return hasNext;
	}

	private void ShowPreviousButton()
	{
		btnPrevious.Visible = HasPreviousPage();
	}

	private void ShowNextButton()
	{
		btnNext.Visible = HasNextPage();
	}

	private int TotRecCnt
	{
		get
		{
			int cnt = 0;
			try
			{
				cnt = Convert.ToInt32(ViewState[VS_TOT_CNT]);
			}
			catch (Exception)
			{
			}
			return cnt;
		}
		set
		{
			if (value < 0) value = 0;
			ViewState[VS_TOT_CNT] = value;
		}
	}

	protected void Page_Load(object sender, EventArgs e)
	{
		((MyMasterPage)Master).AutoRefreshMode = false;

		CtrlSec ctrlsec = CtrlSec.GetInstance();
		EntUser user = ctrlsec.AuthenticateUser(Session, Response);
		ClientScript.RegisterClientScriptBlock(this.GetType(), "JsMsgLogDet123", JS_SHOW_MSG_DET);
		if (ctrlsec.HasAccessRights(user, PG_NAME, CTRL_VIEW))
		{
			if (!IsPostBack)
			{
				//Show("");
				//wuFromDateTime.SetDateTime(DateTime.Now.AddMonths(-1));
				//wuToDateTime.SetDateTime(DateTime.Now);
			}
		}
		else
		{
			DoUnAuthorise();
		}
	}

	private void DoUnAuthorise()
	{
		Response.Redirect("~/UnAuthorised.aspx", true);
	}

	private void CheckDateTime(out string frUfisDateTime, out bool hasFrDateTime, out string toUfisDateTime, out bool hasToDateTime)
	{
		string errMsg = "";
		const string NEWLINE = "\n";
		frUfisDateTime = "";
		toUfisDateTime = "";

		hasFrDateTime = false;
		hasToDateTime = false;

		try
		{
			DateTime frDateTime = System.DateTime.Now;
			DateTime toDateTime = System.DateTime.Now;
			try
			{
				frDateTime = wuFromDateTime.GetDateTime();
				hasFrDateTime = true;
			}
			catch (NoDataException)
			{
				frDateTime = DateTime.Now.AddYears(-1);
			}
			catch (Exception)
			{
				errMsg += "Invalid From Date Time" + NEWLINE;
			}
			try
			{
				toDateTime = wuToDateTime.GetDateTime();
				hasToDateTime = true;
			}
			catch (NoDataException)
			{
				toDateTime = DateTime.Now;
			}
			catch (Exception)
			{
				errMsg += "Invalid To Date Time" + NEWLINE;
			}
			if (errMsg == "")
			{
				if (frDateTime.CompareTo(toDateTime) >= 0) errMsg = "'From Date and Time' must be before 'To Date and Time'";
				frUfisDateTime = UtilTime.ConvDateTimeToUfisFullDTString(frDateTime);
				toUfisDateTime = UtilTime.ConvDateTimeToUfisFullDTString(toDateTime);
			}
		}
		catch (Exception)
		{
		}
		if (errMsg != "") throw new ApplicationException(errMsg);
	}

	private void ShowResult(bool show)
	{
		gvMsgLog.Visible = show;
		btnNext.Visible = show;
		btnPrevious.Visible = show;
	}

	private void Show(string sortBy)
	{
		string msg = "";
		string stSort = sortBy;
		string stFrUfisDateTime = "";
		string stToUfisDateTime = "";
		string stSentBy = txtFltSentBy.Text.Trim();
		int startingIndex = CurStartingIndex;

		TotRecCnt = 0;
		btnNext.Visible = false;
		btnPrevious.Visible = false;

		try
		{
			bool hasFrDateTime = false;
			bool hasToDateTime = false;
			CheckDateTime(out stFrUfisDateTime, out hasFrDateTime, out stToUfisDateTime, out hasToDateTime);
			if ((!hasFrDateTime) && (!hasToDateTime) && (txtFltSentBy.Text.Trim() == ""))
			{
				WebUtil.AlertMsg(Page, "Please fill up the Search Criteria.");
				ShowResult(false);
			}
			else
			{
				//DataView dv = CtrlMsgLog.RetrieveMsgLog().MSGLOG.DefaultView;
				//DSMsgLog ds = CtrlMsgLog.RertieveMsgLog(Session.SessionID, stSentBy,
				//    stFrUfisDateTime, stToUfisDateTime,
				//    startingIndex, PageSize, stSort);
				DSMsgLog ds = CtrlMsg.RertieveMsgLog(Session.SessionID, stSentBy,
					 stFrUfisDateTime, stToUfisDateTime,
					 startingIndex, PageSize, stSort);
				if (ds != null)
				{
					ShowResult(true);
					DataView dv = ds.LOG.DefaultView;

					string stFilter = "";
					stFilter = "SON>='" + stFrUfisDateTime + "'";
					stFilter += " AND SON<='" + stToUfisDateTime + "'";
					if (txtFltSentBy.Text.Trim() != "")
					{
						stFilter += "AND SENTBY='%" + txtFltSentBy.Text.Trim() + "%'";
					}

					if ((stSort == "") || (stSort == "SON"))
					{
						stSort = "SON DESC";
					}
					else
					{
						stSort += " AND SON DESC";
					}
					dv.Sort = stSort;
					//dv.RowFilter = stFilter;
					CurSortBy = stSort;
					gvMsgLog.DataSource = dv;
					gvMsgLog.DataBind();
					gvMsgLog.Visible = true;
					if (gvMsgLog.Rows.Count < 1)
					{
						msg = "No message.";
					}
					try
					{
						TotRecCnt = Convert.ToInt32(((DSMsgLog.HDRRow)ds.HDR.Rows[0]).TOT_CNT);
					}
					catch (Exception)
					{
					}
					ShowNextButton();
					ShowPreviousButton();
				}
				else
				{
					ShowResult(false);
				}
			}
		}
		catch (ApplicationException ex)
		{
			msg = ex.Message;
		}
		catch (Exception ex)
		{
			LogMsg("Retrieving Message error: " + ex.Message);
			msg = "Error in retrieving message logs.";
		}
		if (msg != "")
		{
			WebUtil.AlertMsg(Page, msg);
		}
	}

	protected void gvMsgLog_Sorting(object sender, GridViewSortEventArgs e)
	{
		//e.SortDirection = SortDirection.Ascending;
		Show(e.SortExpression);
	}

	protected void gvMsgLog_Sorted(object sender, EventArgs e)
	{

	}
	protected void btnSearch_Click(object sender, EventArgs e)
	{
		CurStartingIndex = 0;
		Show("");
	}

	protected void btnNext_Click(object sender, EventArgs e)
	{
		GotoNextPage();
		Show("");
	}
	protected void btnPrevious_Click(object sender, EventArgs e)
	{
		GotoPreviousPage();
		Show("");
	}
	protected void gvMsgLog_RowDataBound(object sender, GridViewRowEventArgs e)
	{
		if (e.Row.RowType == DataControlRowType.DataRow)
		{
			try
			{
				ShowDateTime(e.Row);
				//ShowMsg(e.Row);
			}
			catch (Exception)
			{
			}
		}
	}

	private void ShowDateTime(GridViewRow row)
	{
		const int CELL_NO_FOR_SENT_ON = 1;
		const int CELL_NO_FOR_RECV_ON = 4;

		string stSentOn = DataBinder.Eval(row.DataItem, "SON").ToString().Trim() + "";
		string stRecvOn = DataBinder.Eval(row.DataItem, "RON").ToString().Trim() + "";
		stSentOn = UtilTime.ConvUfisDateTimeStringToDisplayDateTime(stSentOn);
		stRecvOn = UtilTime.ConvUfisDateTimeStringToDisplayDateTime(stRecvOn);
		row.Cells[CELL_NO_FOR_SENT_ON].Text = stSentOn;
		row.Cells[CELL_NO_FOR_RECV_ON].Text = stRecvOn;
	}

	const int CN_MSG = 8;

	private void ShowMsg(GridViewRow row)
	{
		try
		{
			string stMsg = Convert.ToString(DataBinder.Eval(row.DataItem, "MSG")).Trim(); ;
			string stUrno = Convert.ToString(DataBinder.Eval(row.DataItem, "URNO")).Trim().ToUpper();
			((HyperLink)(row.Cells[CN_MSG]).Controls[0]).Text = stMsg;
			((HyperLink)(row.Cells[CN_MSG]).Controls[0]).NavigateUrl = "javascript:iTrekOpenWindow('MessageLogDetail.aspx?URNO=" + stUrno + "');";
			//((LinkButton)(row.Cells[21]).Controls[0]).Text = stPF;
			//            <asp:HyperLinkField HeaderText="Pass Fail" DataNavigateUrlFormatString="BMPassFail.aspx?URNO={0}&amp;FLNO={1}&amp;AD=A&amp;AB=A" DataNavigateUrlFields="URNO,FLNO">
			//<ItemStyle HorizontalAlign="Center"></ItemStyle>
			//</asp:HyperLinkField>

			//((LinkButton)(row.Cells[PF_CELL_NO].FindControl("lbBMPassFail"))).Text = stPF;

		}
		catch (Exception ex)
		{
			LogMsg("PF:Err:" + ex.Message);
		}
	}


	private void LogMsg(string msg)
	{
		try
		{
			MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("MessageLogs", msg);
		}
		catch (Exception)
		{
			UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("MessageLogs", msg);
		}
	}

}
