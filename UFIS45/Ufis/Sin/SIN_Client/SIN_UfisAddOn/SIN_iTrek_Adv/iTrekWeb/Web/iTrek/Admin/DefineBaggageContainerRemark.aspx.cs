using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.Ctrl;
using MMSoftware.MMWebUtil;
using UFIS.ITrekLib.ENT;
using UFIS.Web.ITrek.Sec;
using UFIS.ITrekLib.Util;

public partial class Web_iTrek_Admin_DefineBaggageContainerRemark : System.Web.UI.Page
{
    private const string PG_NAME = "PreDefRmk";
    private const string CTRL_VIEW = "Define";

    protected void Page_Load(object sender, EventArgs e)
    {
        ((MyMasterPage)Master).AutoRefreshMode = false;

        CtrlSec ctrlsec = CtrlSec.GetInstance();
        EntUser user = ctrlsec.AuthenticateUser(Session, Response);
        if (ctrlsec.HasAccessRights(user, PG_NAME, CTRL_VIEW))
        {
            if (!IsPostBack)
            {
                ShowPreDefinedRmk();
            }
        }
        else
        {
            DoUnAuthorise();
        }
    }

    private void DoUnAuthorise()
    {
        Response.Redirect("~/UnAuthorised.aspx", true);
    }

    void ShowPreDefinedRmk()
    {       
        gvExistingRmk.DataSource = CtrlPreDefBagConRmk.RetrieveActiveRmk();
        gvExistingRmk.DataBind();
    }

    protected void btnCreate_Click(object sender, EventArgs e)
    {
        string msg = "";
        try
        {
            if (txtRmk.Text != "")
            {
                CtrlPreDefBagConRmk.CreatePreDefBagConRmk(txtRmk.Text);
                txtRmk.Text = "";
            }
        }
        catch (Exception ex)
        {
            msg = ex.Message;
        }
        if (msg != "")
        {
            LogMsg("Create:Error:" + msg);
            WebUtil.AlertMsg(Page, msg);
        }
        ShowPreDefinedRmk();
    }

    protected void gvExistingRmk_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvExistingRmk.EditIndex = -1;
        ShowPreDefinedRmk();
    }

    protected void gvExistingRmk_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //int idx = e.RowIndex;
        try
        {
            string msgId = ((GridView)sender).DataKeys[e.RowIndex].Value.ToString();
            CtrlPreDefBagConRmk.DeletePreDefBagConRmk(msgId);
        }
        catch (Exception ex)
        {
            WebUtil.AlertMsg(Page, "Fail to delete.");
            LogMsg("Delete:Error:" + ex.Message);
        }
        ShowPreDefinedRmk();
    }
    protected void gvExistingRmk_RowEditing(object sender, GridViewEditEventArgs e)
    {
        int idx = e.NewEditIndex;
        gvExistingRmk.EditIndex = idx;
        ShowPreDefinedRmk();
    }
    protected void gvExistingRmk_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int idx = e.RowIndex;
        //string msgId = ((GridView)sender).DataKeys[e.RowIndex].Value.ToString();
        try
        {
            if ((idx >= 0) && (idx < gvExistingRmk.Rows.Count))
            {
                string msgId1 = gvExistingRmk.DataKeys[e.RowIndex].Value.ToString();
                string msg = ((TextBox)(gvExistingRmk.Rows[idx].FindControl("txtMsg"))).Text;
                CtrlPreDefBagConRmk.UpdatePreDefBagConRmk(msgId1, msg);
            }

            gvExistingRmk.EditIndex = -1;
        }
        catch (Exception ex)
        {
            WebUtil.AlertMsg(Page, "Fail to update.");
            LogMsg("RowUpdating:Error:" + ex.Message);
        }
        ShowPreDefinedRmk();
    }

    private void LogMsg(string msg)
    {
        UtilLog.LogToGenericEventLog("DefineBaggageContainerRemark", msg);
    }

}
