<%@ Page Language="C#" MasterPageFile="~/Web/iTrek/MasterPage.master" AutoEventWireup="true" CodeFile="Staff.aspx.cs" Inherits="Web_iTrek_Staff" Title="Flight Staff" Theme="SATSTheme" %>

<%@ Register Src="WUFlightArrApron.ascx" TagName="WUFlightArrApron" TagPrefix="uc2" %>
<%@ Register Src="WUFlightDepApron.ascx" TagName="WUFlightDepApron" TagPrefix="uc3" %>

<%@ Register Src="WUAFlight.ascx" TagName="WUAFlight" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" Runat="Server">
    <table style="width: 970px">
        <tr>
            <td>
                <uc2:WUFlightArrApron ID="wuFlightArrApron1" runat="server" Visible="false" />
            </td>
        </tr>
        <tr>
            <td>
                <uc3:WUFlightDepApron ID="wuFlightDepApron1" runat="server" Visible="false" />
            </td>
        </tr>
        <tr>
            <td style="height: 138px" >
                <strong>Staff<br />
                </strong>
                <asp:TextBox ID="TextBox1" runat="server" Height="325px" Width="813px" TextMode="MultiLine" ReadOnly="True"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>

