<%@ Page Language="C#" AutoEventWireup="true" EnableTheming="true" CodeFile="EventLog.aspx.cs" Inherits="Web_Admin_EventLog" StylesheetTheme="SATSTheme" %>

<%@ Register Src="../../UserControls/WUDate.ascx" TagName="WUDate" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Log Retrieval</title>
    <script type="text/javascript" src="../includes/Master.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table style="width: 900px; height: 220px">
            <tr>
                <td style="height: 15px">
                    &nbsp;<uc1:WUDate ID="WUDate1" runat="server" ShowTime="false" />
                </td>
            </tr>
            <tr>
                <td style="height: 69px">
                    <table style="width: 705px">
                        <tr>
                            <td align="center" style="width: 100px; height: 21px">
                                <asp:Button ID="btnGetEventLog" runat="server" OnClick="btnGetEventLog_Click" Text="Get Event Log File" /></td>
                            <td align="center" style="width: 100px; height: 21px">
                                <asp:Button ID="btnGetTraceLog" runat="server" OnClick="btnGetTraceLog_Click" Text="Get Trace Log File" /></td>
                            <td align="center" style="width: 100px; height: 21px">
                                <asp:Button ID="btnGetLog" runat="server" OnClick="btnGetLog_Click" Text="Get Text Log File" /></td>
                        </tr>
                    </table>
                    <asp:Button ID="btnConvEventLog" runat="server" OnClick="btnConvEventLog_Click" Text="Convert Event Log to Text File" />
                    <asp:Button ID="btnAdhocEventLog" runat="server" OnClick="btnAdhocEventLog_Click"
                        Text="Get UpToDate Event Log Text File" /></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblMsg" runat="server" Text="" Width="556px"></asp:Label></td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
