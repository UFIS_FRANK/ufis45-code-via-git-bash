using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;

using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.ENT;
using UFIS.Web.ITrek.WebMgr;
using UFIS.ITrekLib.EventArg;

public partial class Web_iTrekB_WuBAFlight : System.Web.UI.UserControl
{
   protected override void OnInit(EventArgs e)
   {
      base.OnInit(e);
      wuBagArrContiner1.OnShowReceivedCntrClick += new Web_iTrekB_WuBagArrContiner.ShowReceivedCntrClickHandler(wuBagArrContiner1_OnShowReceivedCntrClick);

   }

   protected override void OnLoad(EventArgs e)
   {
      base.OnLoad(e);
      AddJs();
   }


   protected override void OnPreRender(EventArgs e)
   {
      base.OnPreRender(e);
      PrepareFlightMenu();
      SetCollapsePanel();
      PrepareMsg(FlightUrno);
      HighlightInfo();
   }

   /// <summary>
   /// Prepare the Menu button/Link for this flight
   /// </summary>
   private void PrepareFlightMenu()
   {
      try
      {
         UFIS.Web.ITrek.Sec.CtrlSec ctrlSec = UFIS.Web.ITrek.Sec.CtrlSec.GetInstance();
         EntUser user = ctrlSec.AuthenticateUser(Session, Response);
         MenuFirstBagAccessible = ctrlSec.HasAccessToBAFirstBagTimingUpdate(user);
         MenuLastBagAccessible = ctrlSec.HasAccessToBALastBagTimingUpdate(user);
         MenuRecvAccessible = ctrlSec.HasAccessToBARecvUldUpdate(user);
         MenuShowCntrAccessible = ctrlSec.HasAccessToBAShowContainer(user);
         MenuFlightMsgAccessible = ctrlSec.HasAccessToFlightMessage(user);
      }
      catch (Exception ex)
      {
         LogMsg("PrepareFlightMenu:Err:" + ex.Message);
      }
   }

   #region Menu Accessible Properties

   public bool MenuRecvAccessible
   {
      get { return lbRecv.Visible; }
      set { lbRecv.Visible = value; }
   }

   public bool MenuFirstBagAccessible
   {
      get { return lbFBag.Enabled; }
      set
      {
         if (lbFBag.Enabled)
            lbFBag.Enabled = value;
      }
   }

   public bool MenuLastBagAccessible
   {
      get { return lbLBag.Enabled; }
      set
      {
         if (lbLBag.Enabled)
            lbLBag.Enabled = value;
      }
   }

   public bool MenuShowCntrAccessible
   {
      get { return lbShowCtr.Visible; }
      set { lbShowCtr.Visible = value; }
   }

   public bool MenuFlightMsgAccessible
   {
      get { return lbFlMsg.Enabled; }
      set
      {
         if (lbFlMsg.Enabled)
            lbFlMsg.Enabled = value;
      }
   }

   #endregion

   private const string CSS_HIGHLIGHT = "Highlight";
   private const string CSS_HIGHLIGHT_DISABLED = "HighlightDisabled";
   private const string CSS_NORMAL = "";

   private void HighlightInfo()
   {
      DSFlight dsFlight = null;
      DSTrip dsTrip = null;

      //To highlight First Bag
      if (lbFBag.Visible)
      {
         if (CtrlBagArrival.HighlightFirstBag(FlightUrno, ref dsFlight, ref dsTrip))
         {
            if (lbFBag.Enabled)
            {
               lbFBag.CssClass = CSS_HIGHLIGHT;
            }
            else
            {
               lbFBag.CssClass = CSS_HIGHLIGHT_DISABLED;
            }
         }
         else lbFBag.CssClass = CSS_NORMAL;
      }

      //To highlight Last Bag
      if (lbLBag.Visible)
      {
         if (CtrlBagArrival.HighlightLastBag(FlightUrno, ref dsFlight, ref dsTrip))
         {
            if (lbLBag.Enabled)
            {
               lbLBag.CssClass = CSS_HIGHLIGHT;
            }
            else
            {
               lbLBag.CssClass = CSS_HIGHLIGHT_DISABLED;
            }
         }
         else lbLBag.CssClass = CSS_NORMAL;
      }
   }

   private string _vg = null;
   private const string VS_WUBAGLIGHT_VG = "VS_WUBAGLIGHT_VG";

   public string ValidationGroup
   {
      get
      {
         if (string.IsNullOrEmpty(_vg))
         {
            try
            {
               _vg = ViewState[VS_WUBAGLIGHT_VG].ToString();
            }
            catch (Exception)
            {
               _vg = "VGWuBaFlight" + this.ClientID;
            }
         }
         return _vg;
      }
      set
      {
         if (value == null) value = "";
         ViewState[VS_WUBAGLIGHT_VG] = value;
         _vg = value;
      }
   }

   private string _flightUrno = null;
   private const string VS_FURNO_WUBAFLIGHT = "VS_FURNO_WUBAFLIGHT";
   private EnBagArrType _baType = EnBagArrType.None;
   private const string VS_BAT_WUBAFLIGHT = "VS_BAT_WUBAFLIGHT";

   private EnBagArrType BagArrTypeInternal
   {
      get
      {
         if (_baType == EnBagArrType.None)
         {
            try
            {
               _baType = (EnBagArrType)ViewState[VS_BAT_WUBAFLIGHT];
            }
            catch (Exception)
            {
               _baType = EnBagArrType.None;
            }
         }
         return _baType;
      }
      set
      {
         _baType = value;
         ViewState[VS_BAT_WUBAFLIGHT] = _baType;
      }
   }

   public EnBagArrType BagArrType
   {
      get { return BagArrTypeInternal; }
   }

   private string FlightUrnoInternal
   {
      get
      {
         if (string.IsNullOrEmpty(_flightUrno))
         {
            try
            {
               _flightUrno = (string)ViewState[VS_FURNO_WUBAFLIGHT];
            }
            catch { }
         }
         return _flightUrno;
      }
      set
      {
         if (value == null) value = "";
         _flightUrno = value;
         ViewState[VS_FURNO_WUBAFLIGHT] = _flightUrno;
      }
   }

   public string FlightUrno
   {
      get
      {
         return FlightUrnoInternal;
      }
   }

   //public void PopulateData(string flightUrno, EnBagArrType baType,
   //    DSFlight dsFlight)
   //{
   //    PopulateData(flightUrno, baType, dsFlight, null, null);
   //}

   public void PopulateData(string flightUrno, EnBagArrType baType,
       DSFlight dsFlight, List<string> arrSelectedUldIds, ArrayList arrSelectedTrip)
   {
      FlightUrnoInternal = flightUrno;
      BagArrTypeInternal = baType;
      DSFlight ds = dsFlight;
      DSFlight.FLIGHTRow row = null;
      bool hasData = false;

      if (dsFlight != null)
      {
         DSFlight.FLIGHTRow[] rows = (DSFlight.FLIGHTRow[])dsFlight.FLIGHT.Select("URNO='" + flightUrno + "'");
         if ((rows != null) && (rows.Length > 0))
         {
            hasData = true;
            row = rows[0];
         }
      }
      if (!hasData)
      {
         ds = CtrlFlight.RetrieveFlightsByFlightId(flightUrno);
         DSFlight.FLIGHTRow[] rows = (DSFlight.FLIGHTRow[])ds.FLIGHT.Select("URNO='" + flightUrno + "'");
         if ((rows != null) && (rows.Length > 0))
         {
            hasData = true;
            row = rows[0];
         }
      }

	  if (row == null)
	  {
		  row = ds.FLIGHT.NewFLIGHTRow();
		  LogMsg("PopulateData:NoFlightData." + flightUrno);
	  }

      string firstBagTime = "";
      string lastBagTime = "";
      DSArrivalFlightTimings dsArrFlTimings = null;
      CtrlBagArrival.HasFirstBag(flightUrno, ref ds, ref dsArrFlTimings, out firstBagTime);
      CtrlBagArrival.HasLastBag(flightUrno, ref ds, ref dsArrFlTimings, out lastBagTime);
      DSTrip dsTrip = null;
      bool highlightExpectedContainers = CtrlBagArrival.HighlightExpectedContainers(flightUrno, ref dsFlight, ref dsTrip);

      PrepareContainer(flightUrno, baType, arrSelectedUldIds, arrSelectedTrip, highlightExpectedContainers);
      PopulateFlightInfo(flightUrno, row);
      PrepareFirstBag(flightUrno, wuBagArrContiner1.RecvUldCntE, firstBagTime);
      PrepareLastBag(flightUrno, firstBagTime, lastBagTime);
      //PrepareMsg(flightUrno);
      this.Visible = true;
   }

   //public ArrayList GetSelectedData(out ArrayList arrSelectedTrip)
   //{
   //    return wuBagArrContiner1.GetSelectedData(out arrSelectedTrip);
   //}

   /// <summary>
   /// Get Selected Uld Ids (in Trip) and Trip Nos for the flight
   /// </summary>
   /// <returns></returns>
   public EntBagArrFlightSelectedUld GetSelectedUldsForFlight()
   {
      EntBagArrFlightSelectedUld obj = new EntBagArrFlightSelectedUld();
      obj.BagArrType = BagArrType;
      obj.FlightId = FlightUrno;
      ArrayList arrSelectedTrip = null;
      obj.SelectedUldIds = wuBagArrContiner1.GetSelectedData(out arrSelectedTrip);
      obj.SelectedTrip = arrSelectedTrip;
      return obj;
   }

   private void PrepareContainer(string flightUrno, EnBagArrType baType,
       List<string> arrSelectedUldIds, ArrayList arrSelectedTrip,
       bool highlightExpectedContainer)
   {
      if (ShowDetail)
      {
		  LogMsg("PrepareContainer:ShowDet:" + flightUrno);
         bool hasDetInfo = true;
         string stValidationGroup = this.ValidationGroup;
         wuBagArrContiner1.ValidationGroup = stValidationGroup;
         lbRecv.ValidationGroup = stValidationGroup;
         lbRecv.CommandArgument = this.ClientID;
         wuBagArrContiner1.PopulateData(flightUrno, baType, arrSelectedUldIds, arrSelectedTrip, highlightExpectedContainer, out hasDetInfo);

		 if (!hasDetInfo)
		 {
			 LogMsg("No Container Detail:" + flightUrno);
			 ShowDetail = false;
		 }
		 else
		 {
			 ShowDetail = true;
			 LogMsg("Has Container Detail:" + flightUrno);
			 int unRecvTripCnt = wuBagArrContiner1.UnRecvTripCnt;
			 lbRecv.Font.Underline = true;
			 if (unRecvTripCnt < 1)
			 {//No UnReceived Trip Count. Disable the "Recv" button
				 lbRecv.Enabled = false;
			 }
			 else
			 {
				 lbRecv.Enabled = true;
				 lbRecv.Attributes.Add("onclick", "javascript: if (" + wuBagArrContiner1.JS_HAS_CHK + "()){ return true;} else { alert('Please select the container'); return false;}");
			 }
		 }
      }
   }

   private void PrepareFirstBag(string flightUrno, int recvUldCntByBo, string firstBagTime)
   {
      bool enableFirstBag = false;
      if (!HasTime(firstBagTime))
      {//No First bag time
         if (recvUldCntByBo > 0) enableFirstBag = true;
      }

      this.lbFBag.Enabled = enableFirstBag;
      //if (!enableFirstBag)
         this.lbFBag.Font.Underline = true;
   }

   private bool HasTime(string time)
   {
      bool has = false;
      if ((time == null) || (time.Trim() == ""))
      {//No time
         has = false;
      }
      else
      {
         has = true;
      }
      return has;
   }

   private void PrepareLastBag(string flightUrno, string firstBagTime, string lastBagTime)
   {
      bool enable = false;
      if (HasTime(firstBagTime) && (!HasTime(lastBagTime)))
      {//Enable Last Bag
         enable = true;
      }
      this.lbLBag.Enabled = enable;
      //if (!enable)
         this.lbLBag.Font.Underline = true;
   }

   private void PrepareMsg(string flightUrno)
   {
      int totMsgCnt = 0;
      int totUnReadMsgCnt = 0;

      CtrlMsg.GetMsgInboxInfo(flightUrno, null, 50, out totMsgCnt, out totUnReadMsgCnt);
      lbFlMsg.Text = string.Format("MSG ({0}/{1})", totUnReadMsgCnt, totMsgCnt);
      if (totUnReadMsgCnt > 0)
      {
         lbFlMsg.CssClass = CSS_HIGHLIGHT;
      }
      else lbFlMsg.CssClass = "";
   }

   private void PopulateFlightInfo(String flightUrno, DSFlight.FLIGHTRow row)
   {
      this.lblFInfo.Text = string.Format("{0} {1} ({2})",
          row.FLNO, GetArrFlightTime(row), GetCpmInfo(flightUrno));
   }

   private string GetArrFlightTime(DSFlight.FLIGHTRow row)
   {
      string flightTime = "";
      //if (!string.IsNullOrEmpty(row.AT_TIME))
      //{
      //   flightTime = "ATA " + row.AT_TIME;
      //}
      if (!string.IsNullOrEmpty(row.OBL_TIME))
      {//OBL ==> ATA for SQ and MI
                 //OBL for other flghts
         flightTime = "ATA " + row.OBL_TIME;
      }
      else if (!string.IsNullOrEmpty(row.ET_TIME))
      {
         flightTime = "ETA " + row.ET_TIME;
      }
      else
      {
         flightTime = "STA " + row.ST_TIME;
      }
      return flightTime;
   }

   private string GetCpmInfo(string flightUrno)
   {
      int cntSin, cntInt;
      string st = CtrlCPM.GetUldInfoForAFlight(flightUrno, out cntSin, out cntInt, false);
      return "CPM SIN: " + cntSin + ", INTER: " + cntInt;
   }

   protected void lbRecv_Click(object sender, EventArgs e)
   {
      OnClick(EnBagArrFlightActionType.Receive);
      Response.Redirect("~/Web/iTrekB/BARcvTime.aspx?FLID=" + FlightUrno);
   }

   void wuBagArrContiner1_OnShowReceivedCntrClick(object sender)
   {
      OnClick(EnBagArrFlightActionType.ShowRecvdCntr);
      Response.Redirect("~/Web/iTrekB/BARecvdCntr.aspx?ID=" + FlightUrno);
   }

   //public void RefreshInfo()
   //{
   //    PopulateData(FlightUrnoInternal, BagArrTypeInternal, null);
   //}

   //protected void btSubmit_Click(object sender, EventArgs e)
   //{
   //    string st = e.ToString();
   //    string st1 = sender.ToString();
   //    RefreshInfo();
   //}

   //// add a delegate
   public delegate void BagArrFlightActionHandler(object sender,
       EaBagArrFlightEventArgs bagArrFlightEventArgs);


   //// add an event of the delegate type
   public event BagArrFlightActionHandler BagArrFlightActionClick;
   protected void lbFBag_Click(object sender, EventArgs e)
   {
      OnClick(EnBagArrFlightActionType.FirstBag);
      Response.Redirect("~/Web/iTrekB/BATimeEntry.aspx?A=F&FLID=" + FlightUrno);
   }
   protected void lbLBag_Click(object sender, EventArgs e)
   {
      OnClick(EnBagArrFlightActionType.LastBag);
      Response.Redirect("~/Web/iTrekB/BATimeEntry.aspx?A=L&FLID=" + FlightUrno);
   }

   protected void lbShowCtr_Click(object sender, EventArgs e)
   {
      OnClick(EnBagArrFlightActionType.ShowCntr);
      Response.Redirect("~/Web/iTrekB/BAShowCntr.aspx?FLID=" + FlightUrno);
   }

   private bool _enableUldSelect = true;
   private bool _hasRetrievedEnableUldSelect = false;
   private const string VS_WUBAFL_ENABLEULDSELECT = "VS_WUBAFL_ENABLEULDSELECT";
   private bool EnableUldSelectInternal
   {
      get
      {
         if (_hasRetrievedEnableUldSelect)
         {
            try
            {
               _enableUldSelect = (bool)ViewState[VS_WUBAFL_ENABLEULDSELECT];
               _hasRetrievedEnableUldSelect = true;
            }
            catch (Exception)
            {
            }
         }
         return _enableUldSelect;
      }
      set
      {
         _enableUldSelect = value;
         ViewState[VS_WUBAFL_ENABLEULDSELECT] = _enableUldSelect;
         _hasRetrievedEnableUldSelect = true;
      }
   }

   public bool EnableUldSelect
   {
      get
      {
         return EnableUldSelectInternal;
      }

      set
      {
         EnableUldSelectInternal = value;
         wuBagArrContiner1.EnableUldSelect = EnableUldSelectInternal;
      }
   }

   public bool Collapsed
   {
      get
      {
         return cpeFlight.Collapsed;
      }
      set
      {
         cpeFlight.Collapsed = value;
      }
   }

   #region Show Detail Information for a flight
   private const string VS_WUBAFL_SHOWDET = "VS_WUBAFL_SHOWDET";
   private bool _showDet = true;
   private bool _hasShowDet = false;

   private bool ShowDetailInternal
   {
      get
      {
         if (_hasShowDet)
         {
            try
            {
               _showDet = (bool)ViewState[VS_WUBAFL_SHOWDET];
               _hasShowDet = true;
            }
            catch (Exception)
            {
				_showDet = true;
				_hasShowDet = false;
            }
         }
         return _showDet;
      }
      set
      {
         _showDet = value;
         ViewState[VS_WUBAFL_SHOWDET] = _showDet;
         _hasShowDet = true;
      }
   }
   public bool ShowDetail
   {
      get
      {
         return ShowDetailInternal;
      }

      set
      {
         ShowDetailInternal = value;
         wuBagArrContiner1.Visible = ShowDetailInternal;
      }
   }
   #endregion

   private void OnClick(EnBagArrFlightActionType actionType)
   {
      try
      {
         lblErrMsg.Text = "";
         lblErrMsg.Visible = false;
         ArrayList arrSelectedTrip = null;
         List<string> arrSelectedUld = wuBagArrContiner1.GetSelectedData(out arrSelectedTrip);
         EntBagArrFlightSelectedUld objBaFlightSelUlds = new EntBagArrFlightSelectedUld();
         objBaFlightSelUlds.FlightId = FlightUrno;
         objBaFlightSelUlds.SelectedUldIds = arrSelectedUld;
         objBaFlightSelUlds.SelectedTrip = arrSelectedTrip;
         objBaFlightSelUlds.BagArrType = BagArrTypeInternal;
         objBaFlightSelUlds.BagArrFlightAction = actionType;
         MgrSess.SetBagArrCurrentSel(this.Session, objBaFlightSelUlds);

         if (BagArrFlightActionClick != null)
         {
            EaBagArrFlightEventArgs obj = new EaBagArrFlightEventArgs(
                FlightUrno, actionType,
                arrSelectedUld, "");
            BagArrFlightActionClick(this, obj);
         }
      }
      catch (Exception ex)
      {
         LogMsg("OnClick:Err:" + ex.Message);
         //throw;
         lblErrMsg.Text = "Error: " + ex.Message;
         lblErrMsg.Visible = true;
      }
   }

   private void LogMsg(string msg)
   {
      try
      {
         MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("WuBAFlight", msg);
      }
      catch (Exception)
      {
         UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("WuBAFlight", msg);
      }
   }

	private void LogTraceMsg(string msg)
	{
		LogMsg(msg);
	}

   protected void lbFlMsg_Click(object sender, EventArgs e)
   {
      OnClick(EnBagArrFlightActionType.FlMsg);
      Response.Redirect("~/Web/iTrekB/BAFlightMsg.aspx?FLID=" + FlightUrno);
   }

   private void SetCollapsePanel()
   {
      if (cpeFlight.Collapsed) hfCpeStat.Value = "1";
      else hfCpeStat.Value = "0";
   }

   private string JS_CHGCPESTAT = "JS_CHGCPESTAT";

   private void AddJs()
   {
      if (!Page.ClientScript.IsClientScriptBlockRegistered(JS_CHGCPESTAT))
      {
         string stJs = "";
//         stJs = @"function changeCpeState(cpeId,hfId){
//  objHiddenField = document.getElementById(hfId); 
//  if (objHiddenField.value != '1')
//  objHiddenField.value = '1';
//  else objHiddenField.value = '0';
//}";
         Page.ClientScript.RegisterClientScriptBlock(this.GetType(), JS_CHGCPESTAT, stJs, true);
         this.pnlHead.Attributes.Add("onclick", string.Format("javascript:changeCpeState('{0}', '{1}');", cpeFlight.ClientID, hfCpeStat.ClientID));
      }
   }

   //private string JS_SMOOTH_COLLAPSABLE = "JS_SMOOTH_COLLAPSABLE";
   private void AddJsSmoothCollapsablePanel()
   {
      //try
      //{
      //   string jsId = JS_SMOOTH_COLLAPSABLE + cpeFlight.ClientID;
      //   if (!Page.ClientScript.IsStartupScriptRegistered(jsId))
      //   {
      //      string stJs = "";
      //      stJs = string.Format("collapsiblePanelSmoothAnimation('{0}','{1}','{2}');",
      //         cpeFlight.ClientID, 300, 0.010);
      //      stJs = "function pageLoad(sender, args) {" + stJs + "}";

      //      Page.ClientScript.RegisterStartupScript(this.GetType(), jsId, stJs, true);
      //   }
      //}
      //catch (Exception)
      //{
      //}
   }

   void DoCollapseExpand()
   {
      if (hfCpeStat.Value == "1")
      {
         CollapseCpe(true);
      }
      else
      {
         CollapseCpe(false);
      }
   }

   void CollapseCpe(bool value)
   {
      cpeFlight.Collapsed = value;
      cpeFlight.ClientState = value.ToString().ToLower();
   }

   protected void hfCpeStat_ValueChanged(object sender, EventArgs e)
   {
      //string st = this.hfCpeStat.Value;
      DoCollapseExpand();
   }
}