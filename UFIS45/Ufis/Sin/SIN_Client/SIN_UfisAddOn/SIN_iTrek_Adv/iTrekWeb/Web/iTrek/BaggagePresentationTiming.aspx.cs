using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.Web.ITrek.Sec;
using UFIS.Web.ITrek.WebMgr;
using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;
using MMSoftware.MMWebUtil;
using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.Util;
using iTrekUtils;
using MM.UtilLib;

public partial class Web_iTrek_BaggagePresentationTiming : System.Web.UI.Page
{
    private EntUser user = null;
    string flightUrNo = "";

    private const string PG_NAME = "BaggagePresentTiming_Arr";
    private const string CTRL_VIEW = "View";
    private const string CTRL_EDIT_ONLY = "Edit";
    private const string CTRL_CREATE = "Create";

    private DSBagPresentTimingRpt _dsBPTR = null;

	protected override void OnPreRender(EventArgs e)
	{
		base.OnPreRender(e);
		ShowBaggagePresentationTimingReport();
	}

	protected override void OnLoad(EventArgs e)
	//protected void Page_Load(object sender, EventArgs e)
    {
        ((MyMasterPage)Master).AutoRefreshMode = false;

        flightUrNo = Request.Params["URNO"].Trim();
		//if (!IsPostBack)
		//{
		//    ShowBaggagePresentationTimingReport();
		//}
    }

    private void ShowBaggagePresentationTimingReport()
    {
		try
		{
			CtrlSec ctrlsec = CtrlSec.GetInstance();
			user = ctrlsec.AuthenticateUser(Session, Response);

			if (IsBOReportExist(flightUrNo))
			{
				if (ctrlsec.HasAccessRights(user, PG_NAME, CTRL_EDIT_ONLY))
				{
					DoEdit();
				}
				else if (ctrlsec.HasAccessRights(user, PG_NAME, CTRL_VIEW))
				{
					DoView();
				}
				else
				{
					DoUnAuthorised();
				}
				//if (ctrlsec.HasAccessRights(user, PG_NAME, CTRL_EDIT_ONLY))
				//{
				//    DoEdit();
				//}
				//else
				//{
				//    DoView();
				//}
			}
			else
			{
				if (ctrlsec.HasAccessRights(user, PG_NAME, CTRL_CREATE))
				{
					//No need to do the checking 
					//whether the flight is assigned to the current user
					//refer to email on 15 Feb 2007 by Aung Moe
					//if (IsOwnFlight(user, flightUrNo)) DoCreate();
					//else
					//{
					//    WebUtil.AlertMsg(Page, "Not authorised to create the report for the flight " + flightNo);
					//}      
					DoCreate();
				}
				else DoReportNotReady();
			}
		}
		catch (Exception ex)
		{
			WebUtil.AlertMsg(Page, "Error getting the BPTR Data.");
		}
    }

    private bool IsOwnFlight(EntUser user, string flightUrNo)
    {
        bool ownFlight = false;
        return ownFlight;
    }

    private void DoUnAuthorised()
    {
        Response.Redirect("~/UnAuthorised.aspx", true);
    }

    private void DoReportNotReady()
    {
        WebUtil.AlertMsg(Page, "Report not ready yet");
    }

    private DSBagPresentTimingRpt DsBPTR
    {
        get
        {
            if (_dsBPTR == null)
            {
                try
                {
					//_dsBPTR = CtrlBagPresentTimingReport.GetInstance().PopulateBagPresentTimingReport(flightUrNo);
					_dsBPTR = CtrlBptr.GetInstance().PopulateBagPresentTimingReport(flightUrNo);

                }
                catch (Exception)
                {
                    WebUtil.AlertMsg(Page, "Unable to get the report information.");
                    _dsBPTR = new DSBagPresentTimingRpt();
                }
            }
            return _dsBPTR;
        }
    }

    private void DoView()
    {
        //_dsBPTR = CtrlBagPresentTimingReport.GetInstance().PopulateBagPresentTimingReport(flightUrNo);
        ShowInfo();
        //SetViewMode(true);
        SetViewMode(CTRL_VIEW);
    }

    private void DoCreate()
    {
        //_dsBPTR = CtrlBagPresentTimingReport.GetInstance().PopulateBagPresentTimingReport(flightUrNo);
        ShowInfo();
        //SetViewMode(false);
        SetViewMode(CTRL_CREATE);
    }

    private void DoEdit()
    {
        //_dsBPTR = CtrlBagPresentTimingReport.GetInstance().PopulateBagPresentTimingReport(flightUrNo);

        ShowInfo();
        //SetViewMode(false);
        //btnPrint.Visible = true;
        SetViewMode(CTRL_EDIT_ONLY);
    }

    private bool IsBOReportExist(string flightUrNo)
    {
        return CtrlBptr.GetInstance().IsBagPresentTimingReportExist(null, flightUrNo);
    }


    private void ShowInfo()
    {
        ShowFlightInfo();
        ShowContainerInfo();
        ShowTimingInfo();
        AddJSPrint();
    }

    private void AddJSPrint()
    {
        //WebUtil.SetJavaScriptAtTop(Page,
        //    "function PrintRpt(){ \n" +
        //    "   open_window( 'PrtBagTimingRpt.aspx" +
        //    "?flightUrno=" + flightUrNo +
        //    "');\n" +
        //    "}");
        WebUtil.SetJavaScriptAtTop(Page,
    "function PrintRpt(){ \n" +
    "window.print();" +
    "}");
    }

    private void SetViewMode(string CrEdView)
    {
        if (CrEdView == CTRL_CREATE)
        {
            txtBORmk.ReadOnly = false;
            btnPrint.Visible = false;
			btnCancel.Visible = true;
            btnSubmit.Visible = true;
        }
        else if (CrEdView == CTRL_EDIT_ONLY)
        {
            txtBORmk.ReadOnly = false;
            btnPrint.Visible = true;
			btnCancel.Visible = true;
            btnSubmit.Visible = true;
        }
        else
        {
            txtBORmk.ReadOnly = true;
            btnPrint.Visible = true;
			btnCancel.Visible = false;
            btnSubmit.Visible = false;
        }
    }

    private void SetViewMode(bool viewOnly)
    {//Show/Hide the information according to Edit mode
        txtBORmk.ReadOnly = viewOnly;
        btnPrint.Visible = !viewOnly;
		btnCancel.Visible = !viewOnly;
        btnSubmit.Visible = viewOnly;
    }

    private void ShowFlightInfo()
    {
        DSBagPresentTimingRpt.BPTRHDRRow row = DsBPTR.BPTRHDR[0];
        lblAcftType.Text = row.ACFT_TYPE;
        lblAO.Text = row.AO;
    }

    private void ShowContainerInfo()
    {
        if (DsBPTR.BPTRHDR.Rows.Count > 0)
        {
            DSBagPresentTimingRpt.BPTRHDRRow row = (DSBagPresentTimingRpt.BPTRHDRRow)DsBPTR.BPTRHDR.Rows[0];
            string flDt = row.FLDT;
            if ((flDt != null) && (flDt != ""))
            {
                flDt = String.Format("{0:dd/MMM/yyyy}",UtilTime.ConvUfisTimeStringToDateTime(flDt));
            }
            lblDate.Text = flDt; 
            lblFlNo.Text = row.FLNU;
            lblBO.Text = row.BO;
            lblAcftType.Text = row.ACFT_TYPE;
            lblBelt.Text = row.BELT;
            lblParking.Text = row.PARK_STAND;
            lblATA.Text = ConvToTimeString(row.AT);
            lblAO.Text = row.AO;
            lblFstTrp.Text = ConvToTimeString(row.FTRIP_B);
            lblLstTrp.Text = ConvToTimeString(row.LTRIP_B);
            lblFstBag.Text = ConvToTimeString(row.FBAG);
            lblLstBag.Text = ConvToTimeString(row.LBAG);
            if (row.FBAG_TIME_MET == "P"){ lblFBBM.Text = "YES";  }
            else { lblFBBM.Text = "NO"; }
            if (row.LBAG_TIME_MET == "P") { lblLBBM.Text = "YES"; } 
            else lblLBBM.Text = "NO";

            try
            {                
                lblTbsUp1.Text = UtilTime.TimeDiffString(row.FTRIP_B, row.TBS_UP, "M");
                lblTbsUp2.Text = UtilTime.TimeDiffString(row.LTRIP_B, row.TBS_UP, "M");
                lblTbsUp3.Text = UtilTime.TimeDiffString(row.FBAG, row.TBS_UP, "M");
                lblTbsUp4.Text = UtilTime.TimeDiffString(row.LBAG, row.TBS_UP, "M");
            }
            catch (Exception)
            {
            }
            txtAOUldRmk.Text = row.AO_TRP_RMK.Replace("\n\r","\n").Replace("\n\n","\n");
            txtBORmk.Text = row.BO_RMK.Replace("\n\r", "\n").Replace("\n\n", "\n");
            txtAOTimeRmk.Text = row.AO_TIME_RMK.Replace("\n\r","\n").Replace("\n\n","\n");
        }
    }

    private string ConvToTimeString(string ufisDt)
    {
        return UtilTime.ConvUfisDateTimeStringToDisplayTime(ufisDt);
        //return String.Format("{0:hh:mm}", UtilTime.ConvUfisTimeStringToDateTime(ufisDt));
    }

    private void SetLableText(string labelId, string labelValue)
    {
        Label lbl = null;
        Control ctl = this.FindControl(labelId);
        if (ctl != null)
        {
            lbl = (Label)ctl;
            lbl.Text = labelValue;
        }
    }

    private void ShowTimingInfo()
    {
        int cnt = 0;
        foreach (DSBagPresentTimingRpt.BPTRDETRow row in DsBPTR.BPTRDET.Select("", "SR_NO"))
        {
            cnt++;
            if (cnt > 20) break;
            if (cnt < 1) continue;
            //SetLableText("lblCT" + cnt, ConvToTimeString(row.ULDTIME));
            //SetLableText("lblCN" + cnt, row.ULDNO);
            //SetLableText("lblCC" + cnt, row.CLASS);

            switch (cnt)
            {
                case 1:
                    lblCT1.Text = ConvToTimeString(row.ULDTIME);
                    lblCN1.Text = row.ULDNO;
                    lblCC1.Text = row.CLASS;
                    break;
                case 2:
                    lblCT2.Text = ConvToTimeString(row.ULDTIME);
                    lblCN2.Text = row.ULDNO;
                    lblCC2.Text = row.CLASS; break;
                case 3:
                    lblCT3.Text = ConvToTimeString(row.ULDTIME);
                    lblCN3.Text = row.ULDNO;
                    lblCC3.Text = row.CLASS;
                    break;
                case 4:
                    lblCT4.Text = ConvToTimeString(row.ULDTIME);
                    lblCN4.Text = row.ULDNO;
                    lblCC4.Text = row.CLASS;
                    break;
                case 5:
                    lblCT5.Text = ConvToTimeString(row.ULDTIME);
                    lblCN5.Text = row.ULDNO;
                    lblCC5.Text = row.CLASS;
                    break;
                case 6:
                    lblCT6.Text = ConvToTimeString(row.ULDTIME);
                    lblCN6.Text = row.ULDNO;
                    lblCC6.Text = row.CLASS;
                    break;
                case 7:
                    lblCT7.Text = ConvToTimeString(row.ULDTIME);
                    lblCN7.Text = row.ULDNO;
                    lblCC7.Text = row.CLASS;
                    break;
                case 8:
                    lblCT8.Text = ConvToTimeString(row.ULDTIME);
                    lblCN8.Text = row.ULDNO;
                    lblCC8.Text = row.CLASS;
                    break;
                case 9:
                    lblCT9.Text = ConvToTimeString(row.ULDTIME);
                    lblCN9.Text = row.ULDNO;
                    lblCC9.Text = row.CLASS;
                    break;
                case 10:
                    lblCT10.Text = ConvToTimeString(row.ULDTIME);
                    lblCN10.Text = row.ULDNO;
                    lblCC10.Text = row.CLASS;
                    break;
                case 11:
                    lblCT11.Text = ConvToTimeString(row.ULDTIME);
                    lblCN11.Text = row.ULDNO;
                    lblCC11.Text = row.CLASS;
                    break;
                case 12:
                    lblCT12.Text = ConvToTimeString(row.ULDTIME);
                    lblCN12.Text = row.ULDNO;
                    lblCC12.Text = row.CLASS;
                    break;
                case 13:
                    lblCT13.Text = ConvToTimeString(row.ULDTIME);
                    lblCN13.Text = row.ULDNO;
                    lblCC13.Text = row.CLASS;
                    break;
                case 14:
                    lblCT14.Text = ConvToTimeString(row.ULDTIME);
                    lblCN14.Text = row.ULDNO;
                    lblCC14.Text = row.CLASS;
                    break;
                case 15:
                    lblCT15.Text = ConvToTimeString(row.ULDTIME);
                    lblCN15.Text = row.ULDNO;
                    lblCC15.Text = row.CLASS;
                    break;
                case 16:
                    lblCT16.Text = ConvToTimeString(row.ULDTIME);
                    lblCN16.Text = row.ULDNO;
                    lblCC16.Text = row.CLASS;
                    break;
                case 17:
                    lblCT17.Text = ConvToTimeString(row.ULDTIME);
                    lblCN17.Text = row.ULDNO;
                    lblCC17.Text = row.CLASS;
                    break;
                case 18:
                    lblCT18.Text = ConvToTimeString(row.ULDTIME);
                    lblCN18.Text = row.ULDNO;
                    lblCC18.Text = row.CLASS;
                    break;
                case 19:
                    lblCT19.Text = ConvToTimeString(row.ULDTIME);
                    lblCN19.Text = row.ULDNO;
                    lblCC19.Text = row.CLASS;
                    break;
                case 20:
                    lblCT20.Text = ConvToTimeString(row.ULDTIME);
                    lblCN20.Text = row.ULDNO;
                    lblCC20.Text = row.CLASS;
                    break;
            }
        }
        //aOUldRmks = new ArrayList();
        //try
        //{
        //    dsTrip = CtrlTrip.RetrieveTrip(flightUrNo);
        //    int cnt = dsTrip.TRIP.Rows.Count;
        //    if (cnt > 0)
        //    {
        //        lblCT1.Text = dsTrip.TRIP[0].RCV_DT_TIME;
        //        lblCN1.Text = dsTrip.TRIP[0].ULDN;
        //        lblCC1.Text = dsTrip.TRIP[0].CLASS;//To use CONT from CPMDetail
        //        if ((dsTrip.TRIP[0].SENT_RMK != null) && (dsTrip.TRIP[0].SENT_RMK != ""))
        //        {
        //            aOUldRmks.Add(dsTrip.TRIP[0].SENT_RMK);
        //        }
        //    }

        //    if (cnt > 1)
        //    {
        //        lblCT2.Text = dsTrip.TRIP[1].RCV_DT_TIME;
        //        lblCN2.Text = dsTrip.TRIP[1].ULDN;
        //        lblCC2.Text = dsTrip.TRIP[1].CLASS;
        //        if ((dsTrip.TRIP[1].SENT_RMK != null) && (dsTrip.TRIP[1].SENT_RMK != ""))
        //        {
        //            aOUldRmks.Add(dsTrip.TRIP[1].SENT_RMK);
        //        }
        //    }

        //    if (cnt > 2)
        //    {
        //        lblCT3.Text = dsTrip.TRIP[2].RCV_DT_TIME;
        //        lblCN3.Text = dsTrip.TRIP[2].ULDN;
        //        lblCC3.Text = dsTrip.TRIP[2].CLASS;
        //        if ((dsTrip.TRIP[2].SENT_RMK != null) && (dsTrip.TRIP[2].SENT_RMK != ""))
        //        {
        //            aOUldRmks.Add(dsTrip.TRIP[2].SENT_RMK);
        //        }
        //    }

        //}
        //catch (Exception)
        //{          
        //}
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
		try
		{
			CtrlSec ctrlsec = CtrlSec.GetInstance();
			user = ctrlsec.AuthenticateUser(Session, Response);
			CtrlBptr.GetInstance().SaveReport(flightUrNo, iTUtils.removeBadCharacters(txtBORmk.Text), user);
			ShowBaggagePresentationTimingReport();
		}
		catch (Exception ex)
		{
			WebUtil.AlertMsg(Page, "Fail to save data due to " + ex.Message);
		}
    }

	protected void btnCancel_Click(object sender, EventArgs e)
	{
		Response.Redirect("FlightInfo.aspx?AD=A&AB=B", true);
	}
}
