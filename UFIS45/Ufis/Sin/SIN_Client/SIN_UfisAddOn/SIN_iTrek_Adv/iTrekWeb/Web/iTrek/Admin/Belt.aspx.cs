using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.Web.ITrek.Sec;
using UFIS.Web.ITrek.Base;

//BaseWebPage
//public partial class Web_iTrek_Admin_Belt : System.Web.UI.Page
public partial class Web_iTrek_Admin_Belt : BaseWebPage
{
   protected override void OnLoad(EventArgs e)
   {
      base.OnLoad(e);
      ((MyMasterPage)Master).AutoRefreshMode = false;
      if (CtrlSec.GetInstance().HasAccessToAdminTerminalBeltSetup(LoginedUser))
      {
         DoAuthorise(true);
      }
      else
      {
         DoAuthorise(false);
      }
   }

   public override void DoUnAuthorise()
   {
      //base.DoUnAuthorise();
      DoAuthorise(false);
   }

   private void DoAuthorise(bool authorise)
   {
      //base.DoUnAuthorise();
      WuUnAuthorise1.Visible = !authorise;
      WuBelt1.Visible = authorise;
   }
}
