using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.Ctrl;
using UFIS.Web.ITrek.Sec;
using UFIS.ITrekLib.ENT;

public partial class Web_iTrek_Trips : System.Web.UI.Page
{
    private const string PG_NAME = "TripsApr";
    private const string CTRL_VIEW = "View";

    protected void Page_Load(object sender, EventArgs e)
    {
        CtrlSec ctrlsec = CtrlSec.GetInstance();
        EntUser user = ctrlsec.AuthenticateUser(Session, Response);
        if (ctrlsec.HasAccessRights(user, PG_NAME, CTRL_VIEW))
        {

            string stUrNo = "";
            string stFlightNo = "";
            string stArrDep = "";
            string stAprBag = "";

            try
            {
                stUrNo = Request.Params["URNO"].Trim();
                stFlightNo = Request.Params["FLNO"].Trim();
                stArrDep = Request.Params["AD"].Trim();
                stAprBag = Request.Params["AB"].Trim();

                switch (stArrDep)
                {
                    case "A": //Arrival
                        ShowArrivalApron(stUrNo, stFlightNo);
                        lblTripMsg.Text = "Confirmed (sent) Batches of Containers (Apron to Baggage)";
                        ShowTrips(stUrNo, stFlightNo);
                        break;
                    case "D": //Departure
                        ShowDepartureApron(stUrNo, stFlightNo);
                        lblTripMsg.Text = "Confirmed (received) Batches of Containers (Baggage to Apron)";
                        ShowTrips(stUrNo, stFlightNo);
                        break;
                    default:
                        lblTripMsg.Text = "Invalid Information Request.";
                        throw new ApplicationException("Invalid Information Request.");
                        //break;
                }
            }
            catch (ApplicationException ex)
            {
                MMSoftware.MMWebUtil.WebUtil.AlertMsg(Page, ex.Message);
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                //throw ;
            }
        }
        else
        {
            DoUnAuthorise();
        }
    }

    private void DoUnAuthorise()
    {
        Response.Redirect("~/UnAuthorised.aspx", true);
    }

    private void ShowArrivalApron(string urNo, string flightNo)
    {
        wuFlightArrApron1.PopulateGridForAFlight(urNo, flightNo);
        //pnlFlightArrApron.Visible = true;
        wuFlightArrApron1.Visible = true;
        
        //Web_iTrek_WUFlightArrApron wuArrApron = new Web_iTrek_WUFlightArrApron();
        //wuArrApron.PolpulateGridForAFlight(urNo, flightNo);
    }

    private void ShowDepartureApron(string urNo, string flightNo)
    {
        wuFlightDepApron1.PopulateGridForAFlight(urNo, flightNo);
        //pnlFlightDepApron.Visible = true;
        wuFlightDepApron1.Visible = true;
    }

    private void ShowTrips( string urNo, string flightNo)
    {
        DSTrip ds = null;
        ds = CtrlTrip.RetrieveTrip(urNo);
        if (ds.TRIP.Rows.Count < 1) throw new ApplicationException("No Trip Information.");
        DataView dv = ds.TRIP.DefaultView;
        //dv.Sort="SENT_DT";
        dv.Sort = "STRPNO ASC,ULDN";
        gvTrips.DataSource = dv;
        gvTrips.DataBind();
    }
}
