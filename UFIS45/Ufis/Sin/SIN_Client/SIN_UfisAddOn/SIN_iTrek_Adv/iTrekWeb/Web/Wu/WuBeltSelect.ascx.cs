using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using AjaxControlToolkit;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web.Services;
using System.Web.Services.Protocols;

using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.DS;
using MM.UtilLib;

public partial class Web_Wu_WuBeltSelect : System.Web.UI.UserControl
{
   private string _terminal = "";
   private string _belt = "";

   private void KeepSelectedData()
   {
      try
      {
         //LogMsg(string.Format("KeepSelectedData:t<{0}>,b<{1}>", ddTerminal.SelectedItem.Value, ddBelt.SelectedItem.Value));
         SetSelectedValue( ddTerminal.SelectedItem.Value, ddBelt.SelectedItem.Value );
      }
      catch (Exception ex)
      {
         LogMsg("KeepSelectedData:Err:" + ex.Message);
      }
   }

   protected override void OnLoad(EventArgs e)
   {
      base.OnLoad(e);
      if (IsPostBack)
      {
         //LogMsg("OnLoad");
         KeepSelectedData();
      }
   }

   public void SetSelectedValue(string termilanId, string beltId)
   {
      try
      {
         _terminal = termilanId;
         _belt = beltId;
         ddTerminal.SelectedItem.Value = termilanId;
         ddBelt.SelectedItem.Value = beltId;
         //LogMsg( string.Format("SetSelectedValue:t<{0}>,b<{1}>", termilanId, beltId ));

      }
      catch (Exception ex)
      {
         LogMsg("SetSelectedValue:Err:" + ex.Message);
      }
   }

   protected override void OnPreRender(EventArgs e)
   {
      base.OnPreRender(e);
      //LogMsg("OnPreRender");
      SetDropdownListData();
   }

   private void SetDropdownListData()
   {
      SetDropdownListTerminalData();
      SetDropdownListBeltData();
   }

   private void SetDropdownListBeltData()
   {
      if (!string.IsNullOrEmpty(_belt))
      {
         try
         {
            ddBelt.SelectedValue = _belt;
            cddBelt.SelectedValue = _belt;
            //LogMsg("SetDropdownListBeltData:Set Data, " + _belt);
         }
         catch (Exception ex)
         {
            LogMsg("SetDropdownListBeltData:Err:" + ex.Message);
         }
      }
   }

   private void SetDropdownListTerminalData()
   {
      if (!string.IsNullOrEmpty(_terminal))
      {
         try
         {
            ddTerminal.SelectedItem.Value = _terminal;
            cddTerminal.SelectedValue = _terminal;
            //LogMsg("SetDropdownListTerminalData:Set Data, " + _terminal);
         }
         catch (Exception ex)
         {
            LogMsg("SetDropdownListTerminalData:Err:" + ex.Message);
         }
      }
   }


   private void PopulateTerminal()
   {
      //DSTerminal ds = GetAllTerminals();
      //DataView dv = ds.TERMINAL.DefaultView;
      //dv.Sort = "TERM_NAME";
      //this.ddTerminal.DataSource = dv;
      //this.ddTerminal.DataBind();
      //this.ddTerminal.DataTextField = "TERM_NAME";
      //this.ddTerminal.DataValueField = "TERM_ID";
   }

   private void LogMsg(string msg)
   {
      try
      {
         MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("WuBeltSelect", msg);
      }
      catch (Exception)
      {
         UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("WuBeltSelect", msg);
      }
   }

   protected void btnSelect_Click(object sender, EventArgs e)
   {
      string terminalId = this.ddTerminal.SelectedItem.Value;
      string beltIds = this.ddBelt.SelectedItem.Value;
      string beltName = this.ddBelt.SelectedItem.Text;
      EnBagArrType baType = EnBagArrType.None;
      string selectedBeltId = this.ddBelt.SelectedItem.Value;

      if (BeltSelected != null)
      {
         switch (beltIds)
         {
            case CtrlTerminal.BELT_INTER:
               baType = EnBagArrType.Inter;
               beltIds = CtrlTerminal.GetAllBeltsForTerminal(terminalId);
               break;
            case CtrlTerminal.BELT_LOCAL:
               baType = EnBagArrType.Local;
               beltIds = CtrlTerminal.GetAllBeltsForTerminal(terminalId);
               break;
            default:
               baType = EnBagArrType.Local;
               break;
         }
         ArrayList arrBeltIds = UtilMisc.ConvDelimitedStringToArrayList(beltIds, ',');
         BeltSelected(this, terminalId, selectedBeltId, arrBeltIds, beltName, baType);
         //SetDropdownListData();
      }
   }

   //// add a delegate
   public delegate void BeltSelectHandler(object sender,
       string terminalId, string selectedBeltId, ArrayList arrBeltIds, string stBeltInfo, EnBagArrType bagArrType);


   //// add an event of the delegate type
   public event BeltSelectHandler BeltSelected;

   protected void ddBelt_DataBound(object sender, EventArgs e)
   {
      LogMsg("ddBelt_DataBound");
      SetDropdownListBeltData();
   }
   protected void ddTerminal_DataBound(object sender, EventArgs e)
   {
      LogMsg("ddTerminal_DataBound");
      SetDropdownListTerminalData();
   }
}