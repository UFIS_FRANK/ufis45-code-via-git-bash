<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WuMsgCompose.ascx.cs" Inherits="Web_Wu_WuMsgCompose" %>
<table style="width: 100%">
<tr valign="top">
  <td>Compose Message</td>
</tr>
<tr>
  <td>
      <asp:TextBox ID="txtComposeMessage" runat="server" TextMode="MultiLine"
       Width="100%" Height="100px"></asp:TextBox>
</td>
</tr>
<tr>
  <td>Predefined Messages</td>
</tr>
<tr>
  <td>
  <asp:UpdatePanel ID="updPnl2" runat="server" ChildrenAsTriggers="False" UpdateMode="Conditional">
<ContentTemplate>
    <asp:DropDownList ID="ddPredefMsgs" runat="server" Width="100%" DataTextField="MSGTEXT" DataValueField="URNO" 
AutoPostBack="True" OnSelectedIndexChanged="ddPredefMsgs_SelectedIndexChanged">
      <asp:ListItem></asp:ListItem>
    </asp:DropDownList></ContentTemplate>
      <Triggers>
          <asp:AsyncPostBackTrigger ControlID="tmr1" EventName="Tick" />
      </Triggers>
</asp:UpdatePanel></td>
</tr>
<tr>
  <td style="border-right: gray thin solid; border-top: gray thin solid; border-left: gray thin solid; border-bottom: gray thin 
solid;">
Send To
  <asp:UpdatePanel id="updPnlSendTo" runat="server" UpdateMode="Conditional">
  <Triggers>
   <asp:AsyncPostBackTrigger ControlID="tmr1" EventName="Tick" />
</Triggers>
  <contenttemplate>
   <asp:Panel ID="pnlSendTo" runat="server" ScrollBars="Auto" Height="200px" Width="100%" Wrap="False" >  
    <asp:CheckBoxList id="cblSendTo" runat="server" Width="95%" ></asp:CheckBoxList>
    </asp:Panel>
    <asp:Timer id="tmr1" runat="server" OnTick="Timer1_Tick"></asp:Timer> 
  </contenttemplate>
  </asp:UpdatePanel>
  </td>
</tr>
<tr>
  <td valign="top">
    <table>
      <tr>
        <td style="width: 25px"></td>
        <td>
          <asp:Button ID="btnSendMessage" runat="server" Text="Send Message" OnClick="btnSendMessage_Click" EnableViewState="False" /></td>
        <td style="width: 20px"></td>
      </tr>
    </table>
  </td>
</tr>
</table>
