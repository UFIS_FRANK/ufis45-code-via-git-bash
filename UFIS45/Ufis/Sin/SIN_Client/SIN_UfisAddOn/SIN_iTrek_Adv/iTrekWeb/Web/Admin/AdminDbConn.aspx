<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AdminDbConn.aspx.cs" Inherits="Web_Admin_AdminDbConn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <caption>
                iTrek Database Connection</caption>
            <tr>
                <td style="width: 100px">
                </td>
                <td style="width: 100px">
                </td>
                <td style="width: 101px">
                </td>
                <td style="width: 100px">
                </td>
                <td style="width: 100px">
                </td>
            </tr>
            <tr>
                <td style="width: 100px">
                </td>
                <td style="width: 100px">
                    Conn</td>
                <td style="width: 101px">
                    <asp:TextBox ID="txtConn" runat="server" Height="180px" TextMode="MultiLine" Width="663px"></asp:TextBox></td>
                <td style="width: 100px">
                </td>
                <td style="width: 100px">
                </td>
            </tr>
            <tr>
                <td style="width: 100px">
                </td>
                <td style="width: 100px">
                    Cmd</td>
                <td style="width: 101px">
                    &nbsp;<asp:TextBox ID="txtCmd" runat="server" EnableViewState="False" Height="149px"
                        TextMode="MultiLine" Width="654px"></asp:TextBox></td>
                <td style="width: 100px">
                </td>
                <td style="width: 100px">
                </td>
            </tr>
            <tr>
                <td style="width: 100px">
                </td>
                <td style="width: 100px">
                </td>
                <td style="width: 101px">
                    &nbsp;<asp:Label ID="lblMsg" runat="server" EnableViewState="False" Font-Bold="True"
                        ForeColor="Red"></asp:Label>
                    <asp:Label ID="lblInfo" runat="server" EnableViewState="False"></asp:Label>
                    <table>
                        <tr>
                            <td style="width: 100px">
                                <asp:Button ID="btnLoad" runat="server" CausesValidation="False" EnableViewState="False"
                                    OnClick="btnLoad_Click" Text="Load Connection" /></td>
                            <td style="width: 100px">
                            </td>
                            <td style="width: 100px">
                                <asp:Button ID="btnTest" runat="server" OnClick="btnTest_Click" Text="Test Connection" /></td>
                            <td style="width: 100px">
                            </td>
                            <td style="width: 100px">
                                <asp:Button ID="btnSave" runat="server" CausesValidation="False" EnableViewState="False"
                                    OnClick="btnSave_Click" Text="Save Connection" /></td>
                        </tr>
                    </table>
                </td>
                <td style="width: 100px">
                </td>
                <td style="width: 100px">
                </td>
            </tr>
            <tr>
                <td style="width: 100px">
                </td>
                <td style="width: 100px">
                </td>
                <td style="width: 101px">
                </td>
                <td style="width: 100px">
                </td>
                <td style="width: 100px">
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
