using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.Ctrl;

public partial class Web_Wu_WuTerminal : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((!Page.IsPostBack) && (!Page.IsCallback))
        {
            PopulateData();
        }
    }

    private void PopulateData()
    {
        DSTerminal ds = CtrlTerminal.RetrieveTerminal();
        DataView dv = ds.TERMINAL.DefaultView;
        dv.Sort = "TERM_NAME";
        ddTerminal.DataSource = dv;
        ddTerminal.DataTextField = "TERM_NAME";
        ddTerminal.DataValueField = "TERM_ID";
        ddTerminal.DataBind();
    }

    public string GetSelectedId()
    {
        return ddTerminal.SelectedValue;
    }


}
