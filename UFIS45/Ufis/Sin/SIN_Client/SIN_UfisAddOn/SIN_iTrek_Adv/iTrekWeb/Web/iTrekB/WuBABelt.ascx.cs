using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;

using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.EventArg;
using UFIS.Web.ITrek.WebMgr;

public partial class Web_iTrekB_WuBABelt : System.Web.UI.UserControl
{
   protected void Page_Load(object sender, EventArgs e)
   {
      if ((Page.IsPostBack) || (Page.IsCallback))
      {
         //LogMsg("PostBack/CallBack");
         AddDynamicControls(false);
      }
      //else
      //{
      //    SetCollapsePanel();
      //}
   }

   private void LogMsg(string msg)
   {
      //UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("WuBABelt", msg);
      try
      {
         MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("WuBABelt", msg);
      }
      catch (Exception)
      {
         UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("WuBABelt", msg);
      }
   }

   protected override void OnPreRender(EventArgs e)
   {
      base.OnPreRender(e);
      SetCollapsePanel();
      AddJsSmoothCollapsablePanel();
   }

   private void SetCollapsePanel()
   {
      if (cpeFlight != null)
      {
         if (cpeFlight.Collapsed) hfCpeStat.Value = "1";
         else hfCpeStat.Value = "0";
      }
   }

   //private void CreateDynamicControls()
   //{
   //    if ((Page.IsCallback) || (Page.IsPostBack))
   //    {
   //        AddDynamicControls(false);
   //    }
   //}

   private int _flightCnt = -1;
   private const string VS_FLCNT_WUBAFLIGHT = "VS_FLCNT_WUBAFLIGHT";

   private int FlightCntInternal
   {
      get
      {
         if (_flightCnt < 0)
         {
            try
            {
               _flightCnt = (int)ViewState[VS_FLCNT_WUBAFLIGHT];
            }
            catch (Exception)
            {
            }
         }
         return _flightCnt;
      }
      set
      {
         _flightCnt = value;
         //LogMsg("FlightCnt:" + _flightCnt);
         ViewState[VS_FLCNT_WUBAFLIGHT] = _flightCnt;
      }
   }

   BeltFlight _objBeltFlight = null;


   public void PopulateData(EnBagArrType baType, string terminalId, string beltId, DSFlight dsFlight)
   {
      EntBagArrFlightSelectedUldList selectedUldList = MgrSess.GetBagArrFlightSelectedUldList(Session);
      PopulateData(baType, terminalId, beltId, dsFlight, selectedUldList);
   }

   public void PopulateData(EnBagArrType baType, string terminalId, string beltId,
       DSFlight dsFlight, EntBagArrFlightSelectedUldList selectedUldList)
   {
      beltId = beltId.Trim();
      if (this.lblBInfo != null)
         this.lblBInfo.Text = "Belt " + beltId;
      else LogMsg("lblBInfo is null");
      GetFlightData(terminalId, beltId, dsFlight);
      BeltFlight objBeltFlight = new BeltFlight(beltId);
      _objBeltFlight = objBeltFlight;

      foreach (DSFlight.FLIGHTRow row in dsFlight.FLIGHT.Select("TERMINAL='" + terminalId + "' AND BELT='" + beltId + "'", "FLIGHT_DT DESC"))
      {
         objBeltFlight.AddFlight(row.BELT, row.URNO);
      }
      LogMsg(lblBInfo.Text + ", FlCnt=" + objBeltFlight.Count);
      FlightCntInternal = objBeltFlight.Count;

      AddDynamicControls(true);

      PopulateDataForBelt(baType, terminalId, beltId, dsFlight, selectedUldList);
   }

   private bool _hasAddedControls = false;

   private void AddDynamicControls(bool forceAdding)
   {
      try
      {
         AddJs();
         if ((_hasAddedControls) && (!forceAdding)) return;
         else
         {
            int cnt = FlightCntInternal;
            int curIdx = 0;
            int curCnt = phFlights.Controls.Count;
            for (int i = curCnt - 1; i >= 0; i--)
            {
               if (phFlights.Controls[i] is Web_iTrekB_WuBAFlight)
               {
                  curIdx++;
                  if (curIdx > cnt)
                  {//Remove the extra
                     phFlights.Controls.RemoveAt(i);
                  }
               }
            }
            //LogMsg("AddDynamicControls:Remove");

            if (cnt < 1)
            {
               lblNoFlight.Visible = true;
            }
            else
            {
               lblNoFlight.Visible = false;

               //for (int i = 0; i < cnt; i++)
               for (int i = curIdx; i < cnt; i++)
               {
                  //ASP.web_itrekb_wubaflight_ascx wu = new ASP.web_itrekb_wubaflight_ascx();
                  //Web_iTrekB_WuBAFlight wu = new Web_iTrekB_WuBAFlight();
                  Web_iTrekB_WuBAFlight wu = (Web_iTrekB_WuBAFlight)LoadControl("WuBAFlight.ascx");
                  phFlights.Controls.Add(wu);
                  wu.BagArrFlightActionClick += new Web_iTrekB_WuBAFlight.BagArrFlightActionHandler(wu_BagArrFlightActionClick);
               }
            }
            //LogMsg("AddDynamicControls:ADD");
            _hasAddedControls = true;
         }
      }
      catch (Exception)
      {
      }
   }

   public EntBagArrFlightSelectedUldList GetSelectedUldsForBelt()
   {
      EntBagArrFlightSelectedUldList obj = new EntBagArrFlightSelectedUldList();
      foreach (Control c in phFlights.Controls)
      {
         //if (c is ASP.web_itrekb_wubaflight_ascx)
         if (c is Web_iTrekB_WuBAFlight)
         {
            //ASP.web_itrekb_wubaflight_ascx wu = (ASP.web_itrekb_wubaflight_ascx)c;
            Web_iTrekB_WuBAFlight wu = (Web_iTrekB_WuBAFlight)c;
            obj.Add(wu.GetSelectedUldsForFlight());
         }
      }
      return obj;
   }

   void wu_BagArrFlightActionClick(object sender, UFIS.ITrekLib.EventArg.EaBagArrFlightEventArgs bagArrFlightEventArgs)
   {
      string stFlId = bagArrFlightEventArgs.FlightId;
      if (this.BagArrFlightActionClick != null)
      {
         this.BagArrFlightActionClick(sender, bagArrFlightEventArgs);
      }
   }

   //// add a delegate
   public delegate void BagArrFlightActionHandler(object sender,
       EaBagArrFlightEventArgs bagArrFlightEventArgs);


   //// add an event of the delegate type
   public event BagArrFlightActionHandler BagArrFlightActionClick;

   public void GetFlightData(string terminalId, string beltId, DSFlight dsFlight)
   {
      if (dsFlight == null)
      {
         dsFlight = CtrlBagArrival.GetArrivalFlights(terminalId);
      }
   }

   public void PopulateDataForBelt(EnBagArrType baType, string terminalId, string beltId,
       DSFlight dsFlight, EntBagArrFlightSelectedUldList selectedUldList)
   {
      if (FlightCntInternal < 1)
      {
         for (int i = phFlights.Controls.Count - 1; i >= 0; i--)
         {
            if (phFlights.Controls[i] is Web_iTrekB_WuBAFlight)
            {
               phFlights.Controls.RemoveAt(i);
            }
         }
         return;
      }

      int idx = 0;
      BeltFlight beltFlight = _objBeltFlight;

      foreach (Control c in phFlights.Controls)
      {
         //if (c is ASP.web_itrekb_wubaflight_ascx)
         if (c is Web_iTrekB_WuBAFlight)
         {
            try
            {
               //ASP.web_itrekb_wubaflight_ascx wu = (ASP.web_itrekb_wubaflight_ascx)c;
               Web_iTrekB_WuBAFlight wu = (Web_iTrekB_WuBAFlight)c;
               List<string> arrSelectedUldIds = null;
               ArrayList arrSelectedTrip = null;
               if (selectedUldList != null)
               {
                  arrSelectedUldIds = selectedUldList.GetSelectedUlds(beltFlight[idx], baType, out arrSelectedTrip);
               }

               wu.PopulateData(beltFlight[idx], baType, dsFlight, arrSelectedUldIds, arrSelectedTrip);

            }
            catch (Exception ex)
            {
               LogMsg("PopulateDataForBelt:Err:" + ex.Message);
            }
            idx++;
         }
      }
   }

   private string JS_CHGCPESTAT = "JS_CHGCPESTAT";

   private void AddJs()
   {
      try
      {
         if (!Page.ClientScript.IsClientScriptBlockRegistered(JS_CHGCPESTAT))
         {
            string stJs = "";
            //            stJs = @"function changeCpeState(cpeId,hfId){
            //            //alert( 'CpeId:' + cpeId + ',' + hfId ); 
            //            //try{
            //              //objExtender = $find(cpeId); 
            //              //objExtender = $find('cpeBehaviorID'); 
            //              //if (objExtender==null) alert('null');
            ////objExtender = $find(cpeId);if (objExtender==null) alert('null');
            //      // objExtender = document.getElementById(cpeId); if (objExtender==null) alert(cpeId + ':null');
            //            //}catch( ex) { alert('Err:' + ex.description ); }
            //            objHiddenField = document.getElementById(hfId); 
            //  if (objHiddenField.value != '1')
            // objHiddenField.value = '1';
            //  else objHiddenField.value = '0';
            ////objHf = document.getElementById(cpeId+'_ClientState');
            ////alert('hfid:' + objHf.id);
            ////try{
            ////alert(objExtender.get_Collapsed());
            ////             if(objExtender.get_Collapsed()){  
            ////                 alert('Collapsed');  
            ////                 objHiddenField.value = '0'; 
            ////                 objHf.value = 'true'; 
            ////             }  
            ////            else{
            ////alert('Expand');
            ////objHiddenField.value = '1'; 
            ////objHf.value = 'false';
            ////}
            ////}catch (ex) { alert('Err1:' + ex.description);}
            ////alert(objHiddenField.value);
            //         }";

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), JS_CHGCPESTAT, stJs, true);
            this.pnlHead.Attributes.Add("onclick", string.Format("javascript:changeCpeState('{0}', '{1}');", cpeFlight.ClientID, hfCpeStat.ClientID));
         }

      }
      catch (Exception)
      {
      }
   }

   void DoCollapseExpand()
   {
      if (hfCpeStat.Value == "1")
      {
         CollapseCpe(true);
      }
      else
      {
         CollapseCpe(false);
      }
   }

   void CollapseCpe(bool value)
   {
      cpeFlight.Collapsed = value;
      cpeFlight.ClientState = value.ToString().ToLower();
   }

   private class BeltFlight
   {
      string _beltId = null;
      ArrayList _arrFlights = null;
      public BeltFlight(string beltId)
      {
         if (beltId == null) beltId = "";
         _beltId = beltId;
         _arrFlights = new ArrayList();
      }
      internal void AddFlight(string beltId, string flightId)
      {
         if (beltId == null) beltId = "";
         if (beltId == _beltId)
         {
            _arrFlights.Add(flightId);
         }
      }

      internal string this[int idx]
      {
         get
         {
            return (string)_arrFlights[idx];
         }
      }

      internal int Count
      {
         get { return _arrFlights.Count; }
      }

      internal string BeltId
      {
         get { return _beltId; }
      }
   }

   protected void hfCpeStat_ValueChanged(object sender, EventArgs e)
   {
      //string st = this.hfCpeStat.Value;
      DoCollapseExpand();
   }
   protected void pnlHead_Load(object sender, EventArgs e)
   {
      //string st = this.hfCpeStat.Value;
      //DoCollapseExpand();
   }

   //private string JS_SMOOTH_COLLAPSABLE_B = "JS_SMOOTH_COLLAPSABLE_B";
   private void AddJsSmoothCollapsablePanel()
   {
      //try
      //{
      //   string jsId = JS_SMOOTH_COLLAPSABLE_B + cpeFlight.ClientID;
      //   if (!Page.ClientScript.IsStartupScriptRegistered(jsId))
      //   {
      //      string stJs = "";
      //      stJs = string.Format("collapsiblePanelSmoothAnimation('{0}','{1}','{2}');",
      //         cpeFlight.ClientID, 300, 0.010);
      //      stJs = "function pageLoad(sender, args) {" + stJs + "}";

      //      Page.ClientScript.RegisterStartupScript(this.GetType(), jsId, stJs, true);
      //   }
      //}
      //catch (Exception)
      //{
      //}
   }
}