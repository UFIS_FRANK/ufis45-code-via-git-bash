<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WUAdminMenu.ascx.cs" Inherits="Web_iTrek_WUAdminMenu" %>
<asp:Menu ID="MnuAdmin" runat="server" Height="25px" Width="93px" DynamicVerticalOffset="5">
    <DynamicHoverStyle CssClass="cssMenuHover" />
    <DynamicMenuStyle CssClass="cssMenuDynamic" />
    <DynamicMenuItemStyle CssClass="cssMenuItemDynamic" />
    <Items>
        <asp:MenuItem Text="Admin" Value="Admin">
            <asp:MenuItem NavigateUrl="~/Web/iTrek/Admin/DefineMessage.aspx" Text="Predefine Message"
                Value="Predefine Message"></asp:MenuItem>
            <asp:MenuItem NavigateUrl="~/Web/iTrek/Admin/DefineBaggageContainerRemark.aspx" Text="Predefine Baggage Remark"
                Value="Predefine Baggage Remark"></asp:MenuItem>
            <asp:MenuItem NavigateUrl="~/Web/iTrek/Admin/MessageLogs.aspx" Text="Messages Log"
                Value="Messages Log"></asp:MenuItem>
            <asp:MenuItem NavigateUrl="~/Web/iTrek/Admin/BaggagePriority.aspx" Text="CPM Priority"
                Value="CPM Priority"></asp:MenuItem>
            <asp:MenuItem NavigateUrl="~/Web/iTrek/Admin/Belt.aspx" Text="Terminal Belt" Value="Terminal Belt">
            </asp:MenuItem>
            <asp:MenuItem NavigateUrl="~/Web/iTrek/Admin/EditConfig.aspx?TP=BA_ALRT" Text="BA Alert Configuration"
                ToolTip="Baggage Arrival - Alert Timing Configuration" Value="BA Alert Configuration">
            </asp:MenuItem>
        </asp:MenuItem>
    </Items>
    <LevelSelectedStyles>
        <asp:MenuItemStyle CssClass="cssMenuSelected" Font-Underline="False" />
    </LevelSelectedStyles>
    <LevelMenuItemStyles>
        <asp:MenuItemStyle CssClass="cssMenuItem" Font-Underline="False" />
    </LevelMenuItemStyles>
    <LevelSubMenuStyles>
        <asp:SubMenuStyle CssClass="cssMenuSub" Font-Underline="False" />
    </LevelSubMenuStyles>
</asp:Menu>
