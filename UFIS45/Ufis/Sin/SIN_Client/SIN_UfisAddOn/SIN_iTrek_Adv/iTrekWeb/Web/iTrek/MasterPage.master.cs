using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.Web.ITrek.Sec;
using UFIS.Web.ITrek.WebMgr;
using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.ENT;

using MMSoftware.MMWebUtil;

public partial class MyMasterPage : System.Web.UI.MasterPage
{
   public delegate void RefreshHandler(object sender);
   public event RefreshHandler OnRefresh;

    private bool _autoRefreshMode = true;

    public bool AdminMenuVisible
    {
        get{ return WUAdminMenu1.Visible;  }
        set{ WUAdminMenu1.Visible = value; }
    }

    private bool BaggagePresentationTimingSummaryReportVisible
    {
        get { return lbBagTimeSummReport.Visible; }
        set { lbBagTimeSummReport.Visible = value; }
    }

    public void RefreshInfo()
    {
		//DateTime dtNow = DateTime.Now;
        //lblRefreshTime.Text = "Last Refresh " + DateTime.Now.ToLongTimeString() + " / " + string.Format("{0:dd MMM yyyy}", dtNow);
        ShowRefreshTime();
        ShowNewMsgCount();
    }

    public bool AutoRefreshMode
    {
        get { return _autoRefreshMode; }
        set { _autoRefreshMode = value; }
    }

	protected override void OnPreRender(EventArgs e)
	{
		base.OnPreRender(e);
		RefreshInfo();
	}

    private void SetRefresh()
    {
        if (_autoRefreshMode)
        {
            //WebUtil.SetJavaScriptAtBottom(Page, "selfRefresh(" + CtrlConfig.GetWebRefreshTiming() + ");");
            WebUtil.SetJavaScriptAtBottom(Page, "selfRefreshByClick(" + CtrlConfig.GetWebRefreshTiming() + ",'" + lbRefreshNow.ClientID + "');"); 
        }
    }

    public ScriptManager GetScriptManager()
    {
        return ScriptManager1;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ////Response.Expires = 20;
        ////Response.ExpiresAbsolute = System.DateTime.Now.AddMinutes(-1);
        Response.AddHeader("pragma", "no-cache");
        Response.AddHeader("cache-control", "private");
        Response.CacheControl = "no-cache";

        //if (Request.IsAuthenticated)
        {
            if (!CtrlSec.GetInstance().IsAuthorisedUser(Session))
            {
                //String st = "";
                //st += ResolveUrl("~/Login2.aspx");
                //st += "\n" + Request.Url.GetLeftPart(UriPartial.Path);
                //WebUtil.AlertMsg(Page, st);
                //WebUtil.AlertMsgAndGotoPage("Please login.", ResolveUrl("~/Login2.aspx"), Page);
                WebUtil.AlertMsgAndGotoPage("Please Login.", MgrNavigation.FullUrlLoginPage(), Page);
            }
            else
            {
                SetRefresh();
                ShowRefreshTime();
                EntUser user = MgrSess.GetUserInfo(Session);
                lblDept.Text = user.Dept;
                string userName = "";
                if (user.LastName == "") userName = user.FirstName;
                else userName = user.LastName;
                userName += " (" + user.UserId + ")";
                lblUser.Text = userName;
                ShowMenu(user);
                ShowNewMsgCount();
                ShowBaggageArrivalScreens(user);
            }
        }
    }

   private void ShowBaggageArrivalScreens(EntUser user)
   {
      bool hasAccess = CtrlSec.GetInstance().HasAccessToBaggageArrivalBtn(user);
      lbShowBagArr.Visible = hasAccess;
   }

    private void ShowRefreshTime()
    {
        DateTime dtNow = DateTime.Now;
        lblRefreshTime.Text = "Last Refresh " + dtNow.ToLongTimeString() + "  " + string.Format("{0:dd MMM yyyy}", dtNow);
    }

    public void ShowMenu(EntUser user)
    {
        AdminMenuVisible = CtrlSec.GetInstance().HasAdminRights(user);
        //lbBagTimeSummReport.Visible = CtrlSec.GetInstance().HasAccessToBagPresentTimeSummRpt(user);
        //lbApron.Visible = CtrlSec.GetInstance().HasAccessToMenuApron(user);
        //lbBaggage.Visible = CtrlSec.GetInstance().HasAccessToMenuBaggage(user);
    }

    public void ShowNewMsgCount()
    {
        try
        {
            EntUser user = MgrSess.GetUserInfo(Session);
            int newMsgCnt = CtrlMsg.GetUnReadInBoxMsgCount(user);
            string stNewMsg;
            if (newMsgCnt < 1) stNewMsg = "0 NEW";
            else stNewMsg = newMsgCnt + " NEW";
            lblNewMsg.Text = stNewMsg;
            lblNewMsg.CssClass = "NewMsg";
        }
        catch (Exception)
        {
            lblNewMsg.Text = "-";
        }
    }

    protected void btnLogout_Click(object sender, EventArgs e)
    {
        FormsAuthentication.SignOut();
        Response.Cookies.Clear();
        Session.RemoveAll();
        Session.Abandon();
        
        Response.Redirect("~/logout.aspx");
    }

    protected void lbBagTimeSummReport_Click(object sender, EventArgs e)
    {

    }

    protected void lbApron_Click(object sender, EventArgs e)
    {
        MgrSess.SetFlightAprBag(Session, "A");
    }
    protected void lbBaggage_Click(object sender, EventArgs e)
    {
        MgrSess.SetFlightAprBag(Session, "B");
    }
    protected void lbDeparture_Click(object sender, EventArgs e)
    {
        MgrSess.SetFlightArrDep(Session, "D");
    }
    protected void lbArrival_Click(object sender, EventArgs e)
    {
        MgrSess.SetFlightArrDep(Session, "A");
    }
    protected void lbRefreshNow_Click(object sender, EventArgs e)
    {
        RefreshHandler hdl = OnRefresh;
        if (hdl != null)
        {
           hdl(this);
        }
    }
}
