<%@ Page Language="C#" MasterPageFile="~/Web/iTrek/MasterPage.master" AutoEventWireup="true" CodeFile="BaggagePriority.aspx.cs" Inherits="Web_iTrek_Admin_BaggagePriority" Title="CPM Priority" StylesheetTheme="SATSTheme" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" Runat="Server">
<div class="Heading" >CPM Priority</div>
<div>
<asp:HiddenField ID="hfScrollTop" runat="server" Value="0" />
    <asp:GridView ID="gvBagPriority" runat="server" AllowSorting="True" AutoGenerateColumns="False" Width="435px" OnRowCancelingEdit="gvBagPriority_RowCancelingEdit" OnRowEditing="gvBagPriority_RowEditing" OnRowUpdating="gvBagPriority_RowUpdating" DataKeyNames="ALC,PR" OnRowDeleting="gvBagPriority_RowDeleting" CssClass="GvDefault" Height="180px">
        <Columns>
            <asp:BoundField DataField="ALC" HeaderText="Airline Code" ReadOnly="True" >
                <ItemStyle Width="40px" />
            </asp:BoundField>
            <asp:BoundField DataField="PR" HeaderText="Priority" ReadOnly="True" >
                <ItemStyle Width="40px" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="Baggage Type">
                <EditItemTemplate>
                    <asp:TextBox ID="txtGvBagTypes" runat="server" Text='<%# Bind("BT") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("BT") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:CommandField ButtonType="Button" ShowEditButton="True" >
                <ItemStyle Width="40px" />
            </asp:CommandField>
            <asp:CommandField ButtonType="Button" ShowDeleteButton="True" >
                <ItemStyle Width="40px" />
            </asp:CommandField>
        </Columns>
        <HeaderStyle CssClass="GvHeader" />
        <AlternatingRowStyle CssClass="GvAlternate" />
    </asp:GridView>
    </div>
    <table style="width: 708px">
        <tr>
            <td class="Heading" style="width: 1139px">
            </td>
            <td style="width: 16px">
            </td>
            <td style="width: 415px">
            </td>
            <td style="width: 327px">
            </td>
        </tr>
        <tr>
            <td align="center" class="Heading" colspan="4">
                New CPM Priority Setting</td>
        </tr>
        <tr>
            <td style="width: 1139px">
                Airline Code (e.g. SQ)</td>
            <td style="width: 16px">
            </td>
            <td style="width: 415px">
                <asp:TextBox ID="txtAcc" runat="server"></asp:TextBox></td>
            <td style="width: 327px">
            </td>
        </tr>
        <tr>
            <td style="width: 1139px">
                Priority (e.g. 1)</td>
            <td style="width: 16px">
            </td>
            <td style="width: 415px">
                <asp:TextBox ID="txtPriority" runat="server"></asp:TextBox></td>
            <td style="width: 327px">
            </td>
        </tr>
        <tr>
            <td style="width: 1139px">
                Baggage Types (Seperated by ';'. e.g. BT;BU;BS)</td>
            <td style="width: 16px">
            </td>
            <td style="width: 415px">
                <asp:TextBox ID="txtBgType" runat="server" Width="259px"></asp:TextBox></td>
            <td style="width: 327px">
            </td>
        </tr>
        <tr>
            <td style="width: 1139px">
            </td>
            <td style="width: 16px">
            </td>
            <td style="width: 415px">
            </td>
            <td style="width: 327px">
            </td>
        </tr>
    </table>
    <table style="width: 681px">
        <tr>
            <td style="width: 240px">
            </td>
            <td style="width: 100px">
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td style="width: 240px">
            </td>
            <td style="width: 100px">
                <asp:Button ID="btnSave" runat="server" Height="24px" OnClick="btnSave_Click" Text="Save"
                    Width="60px" /></td>
            <td>
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" /></td>
            <td>
            </td>
        </tr>
        <tr>
            <td style="width: 240px">
            </td>
            <td style="width: 100px">
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
    </table>
</asp:Content>

