#define LOGINNG_ON

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

using System.Text;

using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;
using UFIS.Web.ITrek.WebMgr;
using UFIS.Web.ITrek.Sec;
using UFIS.Web.ITrek.Misc;
using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.Util;
using MM.UtilLib;

public partial class Web_iTrek_WUFlightArrApron : System.Web.UI.UserControl
{
    const int _FROM_PERIOD_IN_HR = -1;
    const int _TO_PERIOD_IN_HR = +12;
    private EntUser user = null;
    //private string user_dept = "";
    //private string user_role = "";

    private const string AO_RPT_PG_NAME = "ApronServiceReport";
    private const string AO_RPT_CTRL_CREATE = "Create";
    private const string AO_RPT_CTRL_EDIT = "Edit";

    private bool _isGridHeaderFreeze = false;

    public bool IsGridHeaderFreeze
    {
        get { return _isGridHeaderFreeze; }
        set { _isGridHeaderFreeze = value; }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        RetrieveUserInformation();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //RetrieveUserInformation();
    }

    private void FreezeGridHeader()
    {
        if (IsGridHeaderFreeze)
        {
            MiscRender.FreezeGridHeader(this.Page, this.GetType(),
                gvFl.ClientID, "WrapperDivArrApr",
                hfScrollTop.ClientID, hfScrollTop.Value);
        }
    }

    private void RetrieveUserInformation()
    {
        try
        {
            if (user == null)
            {
                user = MgrSess.GetUserInfo(Session);
            }
            _userId = user.UserId;
        }
        catch (Exception)
        {
            //String st = ex.Message;
        }
    }

    public void PopulateGridForAFlight(string urNo, string flightNo, bool editMode)
    {
        try
        {
            if (editMode)
            {
                gvFl.EditIndex = 0;
                MgrView.SetUrNoEdit(ViewState, urNo);
                MgrView.SetModeEdit(ViewState, editMode);
                MgrView.SetFlightNoEdit(ViewState, flightNo);
            }
            PopulateGridForAFlight(urNo, flightNo);
            if (editMode)
            {
                if (gvFl.Rows.Count > 0)
                {
                    gvFl.Rows[0].FindControl("txtNewMAB").Focus();
                }
            }
        }
        catch (Exception ex)
        {
            LogMsg("PopulateGridForAFlight:Err:" + ex.Message);
        }
    }

    public void PopulateGridForAFlight(string urNo, string flightNo)
    {
        try
        {
            DSFlight ds = null;

            ds = CtrlFlight.RetrieveFlightsByFlightId(urNo);
            DataView dv = ds.FLIGHT.DefaultView;
            dv.RowFilter = "FLNO='" + flightNo + "' AND URNO='" + urNo + "'";

            Show(dv);

            //lblHdr.Text = dv.Count + ", " + dv.Table.Rows.Count;
        }
        catch (Exception ex)
        {
            LogMsg("Populate Error:" + ex.Message);
        }
    }

    public void Show(DataView dv)
    {
        try
        {
            dv.Sort = "FLIGHT_DT ASC";
            gvFl.DataSource = dv;
            gvFl.DataBind();
            //lblHdr.Text = dv.Count + ", " + dv.Table.Rows.Count;
            FreezeGridHeader();
        }
        catch (Exception ex)
        {
            LogMsg("Show:Err:" + ex.Message);
        }
    }

    public void Show(DSFlight ds, string terminalId)
    {
        try
        {
            DataView dv = ds.FLIGHT.DefaultView;
            string stFilter = "A_D='A'";
            if (terminalId == null) terminalId = CtrlTerminal.GetDefaultTerminalId();
            else if (terminalId == "") terminalId = CtrlTerminal.GetDefaultTerminalId();

            if (CtrlTerminal.GetDefaultTerminalId() != terminalId)
            {
                stFilter += " AND TERMINAL='" + terminalId + "'";
            }

            dv.RowFilter = stFilter;
            Show(dv);
            lblHdr.Text = "Arrival Flight - Apron " + "(" + CtrlTerminal.GetTerminalName(terminalId) + ")";
        }
        catch (Exception ex)
        {
            LogMsg("ShowDST:Err:" + ex.Message);
        }
    }

    public void Show(DSFlight ds, EntWebFlightFilter entFilter)
    {
        try
        {
            DataView dv = ds.FLIGHT.DefaultView;
            string stFilter = "A_D='A'";
            //if (terminalId == null) terminalId = CtrlTerminal.GetDefaultTerminalId();
            //else if (terminalId == "") terminalId = CtrlTerminal.GetDefaultTerminalId();

            //if (CtrlTerminal.GetDefaultTerminalId() != terminalId)
            //{
            //    stFilter += " AND TERMINAL='" + terminalId + "'";
            //}

            dv.RowFilter = stFilter;
            Show(dv);
            //lblHdr.Text = "Arrival Flight - Apron " + "(" + CtrlTerminal.GetTerminalName(terminalId) + ")";
            lblHdr.Text = " [ " + entFilter.FilterString + " ]";
        }
        catch (Exception ex)
        {
            LogMsg("ShowDST:Err:" + ex.Message);
        }
    }


    protected void gvFlight_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                try
                {
                    MiscRender.HighLightTransit(e.Row, FLNO_CELL_NO);   
                    HighLightMAB(e.Row);
                    HighLightPLB(e.Row);
                    ShowPassFail(e.Row);
                    LinkServiceReport(e.Row);
                }
                catch (Exception)
                {
                }
            }
        }
        catch (Exception ex)
        {
            LogMsg("RowBound:Err:" + ex.Message);
        }
    }

    private void HighLightMAB(GridViewRow row)
    {
        try
        {            
            string stMAB = DataBinder.Eval(row.DataItem, "MAB").ToString().Trim();
            if (stMAB == "")
            {
                string stETA = DataBinder.Eval(row.DataItem, "ET").ToString().Trim();
                if (stETA == "")
                {
                    stETA = DataBinder.Eval(row.DataItem, "ST").ToString().Trim();
                }
                if (stETA != "")
                {
                    //DateTime eta = Convert.ToDateTime(stETA);
                    DateTime eta = UtilTime.ConvUfisTimeStringToDateTime(stETA);
                    int bmMAB = MgrApp.BM_MAB(Application);
                    //MMSoftware.MMWebUtil.WebUtil.AlertMsg(Page, bmMAB.ToString());
                    if (eta.AddMinutes(-1 * bmMAB).CompareTo(DateTime.Now) < 0)
                    {
                        //row.Cells[MAB_CELL_NO].BackColor = System.Drawing.Color.Red;
                        row.Cells[MAB_CELL_NO].CssClass = "HighLightMAB";
                    }
                }
            }
        }
        catch (Exception ex)
        {
            LogMsg("HighLightMAB:Error:" + ex.Message);
        }
    }

    private void HighLightPLB(GridViewRow row)
    {
        try
        {
            string stPLB = "";
            try
            {
                stPLB = DataBinder.Eval(row.DataItem, "PLB").ToString().Trim();
            }
            catch (Exception ex)
            {
                LogMsg("PLB Error." + ex.Message);
            } 
            if (stPLB != "")
            {
                string stRef = "";
                try
                {
                    stRef = DataBinder.Eval(row.DataItem, "TBS_UP").ToString().Trim();
                }
                catch (Exception ex)
                {
                    LogMsg("TBS_UP Error."+ ex.Message);
                } 
                if (stRef != "")
                {
                    //LogTraceMsg("HighlightPLB:" + stPLB + ", " + stRef);
                    DateTime dtRef = UtilTime.ConvUfisTimeStringToDateTime(stRef);
                    //LogTraceMsg("HighlightPLB:dtRef:" + stRef + ", " + dtRef.ToString());
                    DateTime dtBM = UtilTime.ConvUfisTimeStringToDateTime(stPLB);
                    //LogTraceMsg("HighlightPLB:dtBM:" + stPLB + ", " + dtBM.ToString());
                    int bm = MgrApp.BM_PLB(Application);
                    //LogTraceMsg("HighlightPLB:" + dtRef.ToString() + ", " + dtBM.ToString() + ", " + bm);
                    if (dtRef.AddMinutes(bm).CompareTo(dtBM) < 0)
                    {
                        //row.Cells[MAB_CELL_NO].BackColor = System.Drawing.Color.Red;
                        row.Cells[PLB_CELL_NO].CssClass = "HighLightMAB";
                    }
                }
            }
        }
        catch (Exception ex)
        {
            LogMsg("HighLightPLB:Error:" + ex.Message);
        }
    }


    private void ShowPassFail(GridViewRow row)
    {
        try
        {
            //***Show Pass or Fail.
            //string stFPF = Convert.ToString(DataBinder.Eval(row.DataItem, "AP_FST_BM_MET")).Trim().ToUpper();
            //string stLPF = Convert.ToString(DataBinder.Eval(row.DataItem, "AP_LST_BM_MET")).Trim().ToUpper();
            string stPF = "";

            //if ((stFPF != "") && (stLPF != ""))
            //{
            //    if ((stFPF == "P") && (stLPF == "P"))
            //    {
            //        stPF = "P";
            //    }
            //    else { stPF = "F"; }
            //}
            stPF = Convert.ToString(DataBinder.Eval(row.DataItem, "AP_PF")).Trim().ToUpper();
            //((HyperLink)(row.Cells[PF_CELL_NO]).Controls[0]).Text = stPF;
            //((LinkButton)(row.Cells[21]).Controls[0]).Text = stPF;
            //            <asp:HyperLinkField HeaderText="Pass Fail" DataNavigateUrlFormatString="BMPassFail.aspx?URNO={0}&amp;FLNO={1}&amp;AD=A&amp;AB=A" DataNavigateUrlFields="URNO,FLNO">
            //<ItemStyle HorizontalAlign="Center"></ItemStyle>
            //</asp:HyperLinkField>

            ((LinkButton)(row.Cells[PF_CELL_NO].FindControl("lbBMPassFail"))).Text = stPF;

        }
        catch (Exception ex)
        {
            LogMsg("PF:Err:" + ex.Message);
        }
    }

    private string _userId = "";

    private void LinkServiceReport(GridViewRow row)
    {
        try
        {
            //***Show Link for Service Report.
			//LogTraceMsg("LinkServiceReport");
            string flightUrno = Convert.ToString(DataBinder.Eval(row.DataItem, "URNO"));
			
            string aoId = "";
            try
            {
                aoId = Convert.ToString(DataBinder.Eval(row.DataItem, "AOID"));
            }
            catch (Exception)
            {
            }
			//LogTraceMsg("LinkServiceReport:Urno:" + flightUrno + ",AOID:" + aoId);

            CtrlSec ctrlSec = CtrlSec.GetInstance();

            string st = "";
            RetrieveUserInformation();
            //EntUser user = MgrSess.GetUserInfo(Session);
            //_userId = user.UserId;
            if (HasAOReport(row))
            {
				//LogTraceMsg("LinkServiceReport:5");
                //LogMsg("Has Aoreport, aoid : " + aoId + ", " + flightUrno);
                if (ctrlSec.HasAccessRights(user, AO_RPT_PG_NAME, AO_RPT_CTRL_EDIT))
                {
					//LogTraceMsg("LinkServiceReport:5-1");
                    st = "EDIT";
                }
                else
                {
					//LogTraceMsg("LinkServiceReport:5-2");
                    st = "VIEW";
                }
                //LogMsg("aoid:" + aoId + ", " + flightUrno + ",Access:" + st);
            }
            else
            {
                //LogMsg("Has no ao report ");
                if (ctrlSec.HasAccessRights(user, AO_RPT_PG_NAME, AO_RPT_CTRL_CREATE))
                {
					//LogTraceMsg("LinkServiceReport:6");
                    //LogMsg("aoid:" + aoId + ", " + flightUrno + ",Has AccessRight:" + st + ", userid :" + user.UserId);

					if (user.UserId == aoId)
					{//OWN Flight
						st = "CREATE";
						//LogTraceMsg("LinkServiceReport:6-1");
					}
					else
					{
						//LogTraceMsg("LinkServiceReport:6-2");
						st = "";
					}
                }
                else
                {
                    //LogMsg("No acces right, userid : " + user.UserId + ", " + AO_RPT_PG_NAME + ", " + AO_RPT_CTRL_CREATE);
                }
                //LogMsg("aoid:" + aoId + ", " + flightUrno + ",Access:" + st);
            }


            //((HyperLink)(row.Cells[CELL_NO_AO_RPT]).Controls[0]).NavigateUrl = "~/Web/iTrek/ApronServiceReport.aspx?flNo=123";
            ((HyperLink)(row.Cells[CELL_NO_AO_RPT]).Controls[0]).Text = st;
			//LogTraceMsg("LinkServiceReport:10");

        }
        catch (Exception ex)
        {
            LogMsg("LinkServiceReport:Err:" + ex.Message);
        }
    }

    private bool IsOwnFlight(EntUser user, string flightUrno)
    {
        if (user.UserId == CtrlFlight.GetAOIdForFlight(flightUrno)) return true;
        else return false;
    }

    private bool HasAOReport(GridViewRow row)
    {
        bool hasRpt = false;
        try
        {
            string st = Convert.ToString(DataBinder.Eval(row.DataItem, "HAS_AO_RPT")).Trim().ToUpper();
            if (st == "Y")
            {
                hasRpt = true;
            }
//            else
//            {
//                string flightUrno = Convert.ToString(DataBinder.Eval(row.DataItem, "URNO"));
//                hasRpt = CtrlAsr.IsConfirmedReportExist(flightUrno);
//#warning //TOD0 - To update the Indicator to show that Apron Service Report is existed.
//                //if (hasRpt)
//                //{
//                //    CtrlFlight.GetInstance().UpdateApronServiceReportExist(flightUrno, hasRpt);
//                //}
//            }
        }
        catch (Exception ex)
        {
            LogMsg("HasAORpt:Err:" + ex.Message);
        } 
        return hasRpt;
    }


    protected void gvFlight_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //Console.Write("row");
    }

    public void SaveEditInfo()
    {
        try
        {
            if (MgrView.GetModeEdit(ViewState))
            {//Edit Mode
                //string stURNO = gvFl.Rows[0].Cells[0].Text;
                //stURNO = gvFl.DataKeys[0].Value.ToString();

                //string stFlightNo = gvFl.Rows[0].Cells[1].Text;

                string stURNO = MgrView.GetUrNoEdit(ViewState);
                String stFlightNo = MgrView.GetFlightNoEdit(ViewState);
                string flightDateTime = CtrlFlight.GetFlightDateTime(stURNO);

                string stMAB = ConvTime(0, "txtNewMAB", flightDateTime);
                string stTBSUP = ConvTime(0, "txtNewTbsUp", flightDateTime);
                string stPLB = ConvTime(0, "txtNewPLB", flightDateTime);
                string stUNLSTART = ConvTime(0, "txtNewUnlStart", flightDateTime);
                string stFirstTrip = ConvTime(0, "txtNewFirstTrip", flightDateTime);
                string stLastTrip = ConvTime(0, "txtNewLastTrip", flightDateTime);
                string stUnlEnd = ConvTime(0, "txtNewUnlEnd", flightDateTime);
                stURNO = MgrView.GetUrNoEdit(ViewState);
                stFlightNo = MgrView.GetFlightNoEdit(ViewState);

                //MMSoftware.MMWebUtil.WebUtil.AlertMsg(Page, stURNO + "\n" + stFlightNo + "\n" + stMAB + "\n" + stTBSUP + "\n" + stPLB);
                CtrlFlight.SaveArrAprTimingInfo(stURNO, stFlightNo,
                    stMAB, stTBSUP,
                    stPLB, stUNLSTART,
                    stFirstTrip, stLastTrip, stUnlEnd, _userId);
            }
            else
            {//Not in Edit Mode
                throw new ApplicationException("Unable to save. It is not in Edit Mode");
            }

        }
        catch (ApplicationException ex)
        {
            throw new ApplicationException(ex.Message);
        }
        catch (Exception ex)
        {
            LogMsg("SaveEditInfo:Err:" + ex.Message);
            throw new ApplicationException("Unable to save.");
        }
    }

    private string ConvTime(int rowNum, string txtBoxName, string refUfisDt)
    {
        string stDt = "";
        try
        {
            string stTime = GetTextBoxData(rowNum, txtBoxName);
            if (stTime != "")
            {
                if (UtilTime.IsValidHHmm(stTime))
                {
                    //stDt = UtilTime.ConvHHmmToUfisTimeAfterRefTime(refUfisDt, stTime);
                    DateTime refDt = UtilTime.ConvUfisTimeStringToDateTime(refUfisDt);
                    stDt = UtilTime.ConvDateTimeToUfisFullDTString(UtilTime.CompDateTimeFromRef(refDt, stTime, 6));
                }
                else
                {
                    throw new ApplicationException("Invalid Time Format.");
                }
            }
        }
        catch (ApplicationException ex)
        {
            throw new ApplicationException(ex.Message);
        }
        catch (Exception)
        {
        }
        return stDt;
    }

    private string GetTextBoxData(int rowNum, string txtBoxName)
    {
        string st = "";
        try
        {
            st = ((TextBox)gvFl.Rows[rowNum].FindControl(txtBoxName)).Text;
        }
        catch (Exception)
        {
        }
        return st;
    }

	#region Logging
	private static void LogMsg(string msg)
	{
		try
		{
			MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("WEB_ApronArrival", msg);
		}
		catch (Exception)
		{
			UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("WEB_ApronArrival", msg);
		}

	}

	private static void LogTraceMsg(string msg)
	{
		LogMsg(msg);
	}
	#endregion

    protected void gvFlight_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvFl.PageIndex = e.NewPageIndex;
    }

    const int FLNO_CELL_NO = 2;
    const int MAB_CELL_NO = 8;
    const int PLB_CELL_NO = 10;
    const int PF_CELL_NO = 15;
    const int CELL_NO_AO_RPT = 19;
}
