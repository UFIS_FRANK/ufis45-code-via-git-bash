<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WuTime.ascx.cs" Inherits="Web_Wu_WuTime" %>
<asp:UpdatePanel ID="updPnlSelTime" runat="server" UpdateMode="Conditional">
<ContentTemplate>
<asp:RadioButtonList ID="rblTiming" runat="server" Width="174px" OnSelectedIndexChanged="rblTiming_SelectedIndexChanged" AutoPostBack="True">
</asp:RadioButtonList>
</ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="tmrRefreshRblTime" EventName="Tick" />
    </Triggers>
</asp:UpdatePanel>
<asp:UpdatePanel ID="updPnlTime" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
<asp:Label ID="lblTiming" runat="server" Text="Time "></asp:Label> &nbsp; &nbsp;&nbsp;
<asp:TextBox ID="txtTiming" runat="server" Width="77px" MaxLength="4"></asp:TextBox>
<span style="position:relative;width:auto">
<span style="position:absolute;left:10px;width:100px;"><asp:RequiredFieldValidator ID="rfvTime" runat="server" ControlToValidate="txtTiming"
            ErrorMessage="* No Timing" SetFocusOnError="True" ValidationGroup="vgTimeWuTime" Display="Dynamic"></asp:RequiredFieldValidator>
</span>
<span style="position:absolute;left:10px;width:100px;">&nbsp;<asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtTiming"
            Display="Dynamic" ErrorMessage="Invalid Time" MaximumValue="2359" MinimumValue="0000"
            Type="Integer" ValidationGroup="vgTimeWuTime"></asp:RangeValidator></span>
    <asp:Label ID="lblErrMsg" runat="server" Width="128px"></asp:Label></span>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="rblTiming" EventName="SelectedIndexChanged" />
    </Triggers>
</asp:UpdatePanel>
&nbsp; &nbsp;&nbsp; &nbsp;
<asp:Timer ID="tmrRefreshRblTime" runat="server" OnTick="tmrRefreshRblTime_Tick">
</asp:Timer>
<table style="width: 180px">
    <tr>
        <td style="width: 30px">
        </td>
        <td style="width: 85px">
        <asp:Button ID="btnSubmit" runat="server" Text="Send" OnClick="btnSubmit_Click" ValidationGroup="vgTimeWuTime" />
        </td>
        <td>
        </td>
        <td>
            &nbsp;</td>
    </tr>
</table>
