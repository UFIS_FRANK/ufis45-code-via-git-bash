using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.ENT;
using UFIS.Web.ITrek.Sec;

public partial class Web_iTrek_ChgArrFlightTiming : System.Web.UI.Page
{


    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            ShowFlightInfo();
        }

    }

    private void DoUnAuthorise()
    {
        Response.Redirect("~/UnAuthorised.aspx", true);
    }

    private void ShowFlightInfo()
    {
        string flightNo = "", urNo = "";
        flightNo = Request.Params["FLNO"].ToString().Trim();
        urNo = Request.Params["URNO"].ToString().Trim();

        WUAFlight1.ShowFlightInfo(urNo,flightNo );
        WUAFlight1.ShowTextBoxes(true);
    }

    protected void btnApply_Click(object sender, EventArgs e)
    {
        WUAFlight1.SaveData();
    }
}
