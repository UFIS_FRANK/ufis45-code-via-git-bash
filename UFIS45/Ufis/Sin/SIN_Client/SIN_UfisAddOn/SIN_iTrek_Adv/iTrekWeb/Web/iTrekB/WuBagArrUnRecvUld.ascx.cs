using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;

using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.Ctrl;

public partial class Web_iTrekB_WuBagArrUnRecvUld : System.Web.UI.UserControl
{
	private bool _showRemark = true;

	protected void Page_Load(object sender, EventArgs e)
	{
		if (Page.IsPostBack)
		{
			try
			{
				_showRemark = (bool)ViewState[VS_WU_BAUNRCVULD_RRMK_SHOWRMK];
			}
			catch (Exception)
			{
			}
			AddDynamicControlForRemk();
		}
	}

	public string ValidationGroup
	{
		get
		{
			return WuCheckBoxList1.ValidationGroup;
		}
		set
		{
			if (WuCheckBoxList1 == null) return;
			if (value == null) WuCheckBoxList1.ValidationGroup = "";
			else WuCheckBoxList1.ValidationGroup = value;
		}
	}

	public string CheckListClientId
	{
		get { return WuCheckBoxList1.CheckListClientId; }
	}

	private const string VS_WU_BAUNRCVULD_RRMK_SHOWRMK = "VS_WU_BAUNRCVULD_RRMK_SHOWRMK";

	public bool ShowRemark
	{
		get { return _showRemark; }
		set
		{
			_showRemark = value;
			ViewState[VS_WU_BAUNRCVULD_RRMK_SHOWRMK] = _showRemark;
		}
	}

	private void AddDynamicControlForRemk()
	{
		try
		{
			if (!ShowRemark) return;
			int cnt = RemkCount;
			if (cnt > 0)
			{
				for (int i = 0; i < cnt; i++)
				{
					if (i == 0)
					{
						Label lb = new Label();
						phRemk.Controls.Add(lb);
						lb.Text = "REMARK: ";
						//Literal lt = new Literal();
						//phRemk.Controls.Add(lt);
						//lt.Text = "REMARK:";
					}
					else
					{
						Label lb = new Label();
						phRemk.Controls.Add(lb);
						lb.Text = ", ";
					}
					HyperLink hl = new HyperLink();
					phRemk.Controls.Add(hl);
				}
				Literal br = new Literal();
				br.Text = "<br />";
				phRemk.Controls.Add(br);
			}
		}
		catch (Exception ex)
		{
			LogMsg("AddDynamicControlForRemk:Err:" + ex.Message);
			throw;
		}
	}

	private bool IsSelected(EntBagArrUld uld, List<string> arrSelected)
	{
		bool selected = false;
		if (arrSelected != null)
		{
			int cnt = arrSelected.Count;

			for (int i = 0; i < cnt; i++)
			{
				string selId = (string)arrSelected[i];
				if (selId == uld.Id)
				{
					selected = true;
					break;
				}
			}
		}
		return selected;
	}

	public void PopulateData(EntBagArrUnReceivedTrip ent, List<string> arrSelectedUldIds, Hashtable htSelectedTrip)
	{
		try
		{
			_remkCnt = 0;
			string errMsg = "";

			//List<ASP.usercontrols_wucheckboxlist_ascx.IdText> arr = new List<UserControls_WuCheckBoxList.IdText>();
			List<UserControls_WuCheckBoxList.IdText> arr = new List<UserControls_WuCheckBoxList.IdText>();
			int cnt = ent.Count;
			//LogTraceMsg("PopulateData:Cnt:" + cnt);

			for (int i = 0; i < cnt; i++)
			{
				EntBagArrUld uld = ent[i];
				//LogTraceMsg("PopulateData:i:" + i);
				if (uld == null) continue;
				//LogTraceMsg("PopulateData:Added:i:" + i );
				arr.Add(new UserControls_WuCheckBoxList.IdText(ent[i].Id, ent[i].UldNo, IsSelected(ent[i], arrSelectedUldIds)));
				if (ShowRemark && (uld.SendRemark != ""))
				{
					AddRemark(uld.Id, uld.UldNo, uld.SendRemark, uld.RecvRemark);
				}
			}

			string cssClass = "";

			if (CtrlBagArrival.HighlightUnRecvTrip(ent, out errMsg))
			{
				cssClass = CtrlBagArrival.CSS_HIGHLIGHT;
			}

			if ((errMsg != "") && (!Page.ClientScript.IsClientScriptBlockRegistered("HlErr")))
			{
				Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "HlErr",
					@"alert('" + errMsg + "');", true);
			}

			string tripId = ent.TripNo.ToString();
			//LogTraceMsg("PopulateData:ArrCnt:" + arr.Count );
			WuCheckBoxList1.ColumnsCount = 5;//Maximum number of columns in a row
			WuCheckBoxList1.PopulateData(arr, ent.TripNoToDisplayInWeb, cssClass,
				CheckedTripHead(tripId, htSelectedTrip), tripId);

			if (_remkCnt > 0)
			{
				RemkCount = _remkCnt;
				Literal br = new Literal();
				br.Text = "<br />";
				phRemk.Controls.Add(br);
			}
		}
		catch (Exception ex)
		{
			LogMsg("PopulateData:Err:" + ex.Message);
			throw;
		}
	}

	private bool CheckedTripHead(string trip, Hashtable htSelectedTrip)
	{
		return htSelectedTrip.ContainsKey(trip);
	}

	private int _remkCnt = -1;
	private const string VS_WU_BAUNRCVULD_RRMK_CNT = "VS_WU_BAUNRCVULD_RRMK_CNT";

	private int RemkCount
	{
		get
		{
			if (_remkCnt < 0)
			{
				try
				{
					_remkCnt = (int)ViewState[VS_WU_BAUNRCVULD_RRMK_CNT];
				}
				catch (Exception)
				{
					ViewState[VS_WU_BAUNRCVULD_RRMK_CNT] = -1;
					//LogMsg("RemkCount:Err:" + ex.Message);
				}
			}
			return _remkCnt;
		}
		set
		{
			ViewState[VS_WU_BAUNRCVULD_RRMK_CNT] = value;
			_remkCnt = value;
		}
	}

	private void AddRemark(string id, string uldNo, string sendRemk, string recvRemk)
	{
		try
		{
			if (string.IsNullOrEmpty(sendRemk)) return;
			if (!ShowRemark) return;
			if (_remkCnt == 0)
			{
				Label lb = new Label();
				phRemk.Controls.Add(lb);
				lb.Text = "REMARK: ";
			}
			else
			{
				Label lb = new Label();
				phRemk.Controls.Add(lb);
				lb.Text = ", ";
			}
			_remkCnt++;
			HyperLink hl = new HyperLink();
			phRemk.Controls.Add(hl);
			hl.Text = uldNo;
			hl.NavigateUrl = "self();";
			hl.ToolTip = sendRemk.Replace(@"'", @"\'").Replace(@"\n", @", ");
			hl.Attributes.Add("onclick", "alert('" + sendRemk.Replace(@"'", @"\'") + "'); return false;");
		}
		catch (Exception ex)
		{
			LogMsg("AddRemark:Err:" + ex.Message);
			throw;
		}
	}

	public List<string> GetSelectedData(out bool isTripSelected, out string tripId)
	{
		return WuCheckBoxList1.GetSelectedData(out isTripSelected, out tripId);
	}

	public bool EnableUldSelect
	{
		get
		{
			return WuCheckBoxList1.EnableSelect;
		}
		set
		{
			WuCheckBoxList1.EnableSelect = value;
		}
	}

	private static void LogMsg(string msg)
	{
		try
		{
			MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("WuBagArrUnRecvUld", msg);
		}
		catch (Exception)
		{
			UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("WuBagArrUnRecvUld", msg);
		}
	}

	private static void LogTraceMsg(string msg)
	{
		LogMsg(msg);
	}

}
