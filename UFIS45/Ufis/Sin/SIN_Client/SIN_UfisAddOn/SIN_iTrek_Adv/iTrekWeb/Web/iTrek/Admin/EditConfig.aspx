<%@ Page Language="C#" MasterPageFile="~/Web/iTrek/MasterPage.master" AutoEventWireup="true" CodeFile="EditConfig.aspx.cs" Inherits="Web_iTrek_Admin_EditConfig" Title="Configuration" Theme="SATSTheme" %>

<%@ Register Src="../../Wu/WuUnAuthorise.ascx" TagName="WuUnAuthorise" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" Runat="Server">
    <table style="width: 100%">
        <tr>
            <td style="width: 500px" class="Heading">
                System Configuration</td>
        </tr>
        <tr>
            <td >
                <asp:GridView ID="gvConfig" runat="server" AutoGenerateColumns="False" Width="675px" BackColor="White" BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" DataKeyNames="ID" ForeColor="Black" GridLines="Vertical" OnRowCancelingEdit="gvConfig_RowCancelingEdit" OnRowCommand="gvConfig_RowCommand" OnRowEditing="gvConfig_RowEditing" OnRowUpdating="gvConfig_RowUpdating">
                    <Columns>
                        <asp:BoundField DataField="ID" ReadOnly="True" Visible="False" />
                        <asp:BoundField DataField="ID_DESC" HeaderText="Configuration For" ReadOnly="True"
                            SortExpression="ID_DESC" >
                            <ItemStyle Width="400px" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Setting">
                            <EditItemTemplate>
                                <asp:TextBox ID="txtValue" runat="server" Text='<%# Bind("VAL") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("VAL") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="100px" />
                        </asp:TemplateField>
                        <asp:CommandField ShowEditButton="True" />
                    </Columns>
                    <FooterStyle BackColor="#CCCCCC" />
                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                    <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="#CCCCCC" />
                </asp:GridView>
                </td>
        </tr>
        <tr>
            <td>
                <uc1:WuUnAuthorise ID="WuUnAuthorise1" runat="server" EnableTheming="true" EnableViewState="false"
                    Visible="false" />
            </td>
        </tr>
    </table>
</asp:Content>

