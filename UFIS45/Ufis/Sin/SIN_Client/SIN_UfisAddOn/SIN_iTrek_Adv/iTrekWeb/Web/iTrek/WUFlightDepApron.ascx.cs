#define LOGGING_ON

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Diagnostics;

using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.Ctrl;
using UFIS.Web.ITrek.WebMgr;
using MMSoftware.MMWebUtil;
using UFIS.ITrekLib.Util;
using UFIS.Web.ITrek.Sec;
using UFIS.Web.ITrek.Misc;
using UFIS.ITrekLib.ENT;
using MM.UtilLib;

public partial class Web_iTrek_WUFlightDepApron : System.Web.UI.UserControl
{
    const string DEF_HDR = "Departure Flight - Apron";

    private const string AO_RPT_PG_NAME = "ApronServiceReport";
    private const string AO_RPT_CTRL_CREATE = "Create";
    private const string AO_RPT_CTRL_EDIT = "Edit";

    const int CELL_NO_AO_RPT = 15;

    private bool _isGridHeaderFreeze = false;

    public bool IsGridHeaderFreeze
    {
        get { return _isGridHeaderFreeze; }
        set { _isGridHeaderFreeze = value; }
    }

    //private EntUser user = null;

    protected void Page_Init(object sender, EventArgs e)
    {
        RetrieveUserInformation();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //RetrieveUserInformation();
    }

    private EntUser user = null;
    private string _userId = "";

    private void RetrieveUserInformation()
    {
        try
        {
            if (user == null)
            {
                user = MgrSess.GetUserInfo(Session);
            }
            _userId = user.UserId;
        }
        catch (Exception)
        {
        }
    }

    private void FreezeGridHeader()
    {
        if (IsGridHeaderFreeze)
        {
            MiscRender.FreezeGridHeader(this.Page, this.GetType(),
                gvFlight.ClientID, "WrapperDivDepApr",
                hfScrolPos.ClientID, hfScrolPos.Value);
        }
    }

    public void Show(DSFlight ds, string terminalId)
    {
        try
        {
            DataView dv = ds.FLIGHT.DefaultView;
            string stFilter = "A_D='D'";
            if (terminalId == null) terminalId = CtrlTerminal.GetDefaultTerminalId();
            else if (terminalId == "") terminalId = CtrlTerminal.GetDefaultTerminalId();

            if (CtrlTerminal.GetDefaultTerminalId() != terminalId)
            {
                stFilter += " AND TERMINAL='" + terminalId + "'";
            }

            dv.RowFilter = stFilter;
            Show(dv);
            lblHdr.Text = DEF_HDR + "(" + CtrlTerminal.GetTerminalName(terminalId) + ")";

        }
        catch (Exception ex)
        {
            LogMsg("Show:Err:" + ex.Message);
        }
    }

    public void Show(DSFlight ds, EntWebFlightFilter entFilter)
    {
        try
        {
            DataView dv = ds.FLIGHT.DefaultView;
            string stFilter = "A_D='D'";

            dv.RowFilter = stFilter;
            Show(dv);
            lblHdr.Text = " [ " + entFilter.FilterString + " ]";

        }
        catch (Exception ex)
        {
            LogMsg("Show:Err:" + ex.Message);
        }
    }

    public void PopulateGridForAFlight(string urNo, string flightNo)
    {
        try
        {
            DSFlight ds = null;

            //        ds = CtrlFlight.RetrieveFlightsByUrNo_FlightNo(urNo, flightNo);
            ds = CtrlFlight.RetrieveFlightsByFlightId(urNo);
            DataView dv = ds.FLIGHT.DefaultView;
            dv.RowFilter = "FLNO='" + flightNo + "' AND URNO='" + urNo + "'";

            Show(dv);
        }
        catch (Exception ex)
        {
            LogMsg("PopulateGridForAFlight:Err:" + ex.Message);
        }
    }

    public void Show(DataView dv)
    {
        try
        {
            dv.Sort = "FLIGHT_DT ASC";
            gvFlight.DataSource = dv;
            gvFlight.DataBind();
            //lblHdr.Text = dv.Count + ", " + dv.Table.Rows.Count;
            FreezeGridHeader();
        }
        catch (Exception ex)
        {
            LogMsg("ShowDV:Err:" + ex.Message);
        }
    }

    public void PopulateGridForAFlight(string urNo, string flightNo, bool editMode)
    {
        try
        {
            if (editMode)
            {
                gvFlight.EditIndex = 0;
                MgrView.SetFlightNoEdit(ViewState, flightNo);
                MgrView.SetModeEdit(ViewState, editMode);
                MgrView.SetUrNoEdit(ViewState, urNo);

            }
            PopulateGridForAFlight(urNo, flightNo);

            if (editMode)
            {
                try
                {
                    gvFlight.Rows[0].FindControl("txtNewMAB").Focus();
                }
                catch (Exception)
                {
                    //throw;
                }
            }
        }
        catch (Exception ex)
        {
            LogMsg("PopulateGridForAFlight:Err:" + ex.Message);
        }
    }

    public void SaveEditInfo()
    {
        try
        {
            if (MgrView.GetModeEdit(ViewState))
            {//Edit Mode
                string stURNO = MgrView.GetUrNoEdit(ViewState);
                String stFlightNo = MgrView.GetFlightNoEdit(ViewState);
                string flightDateTime = CtrlFlight.GetFlightDateTime(stURNO);

                string stMAB = ConvTime(0, "txtNewMAB", flightDateTime);
                string stPLB = ConvTime(0, "txtNewPLB", flightDateTime);
                string stLoadStart = ConvTime(0, "txtNewLoadStart", flightDateTime);
                string stLoadEnd = ConvTime(0, "txtNewLoadEnd", flightDateTime);
                string stLastDoor = ConvTime(0, "txtNewLastDoor", flightDateTime);
                //string stFirstCargo = ConvTime(0, "txtNewFirstCargo", flightDateTime);
                CtrlFlight.SaveDepAprTimingInfo(stURNO, stFlightNo,
                    stMAB, stPLB,
                    stLoadStart, stLoadEnd,
                    stLastDoor, _userId);
                //WebUtil.AlertMsg(Page, stURNO + "\n" + stFlightNo + "\n" + stMAB + "\n" + stPLB + "\n" + stLoadStart);
            }
            else
            {//Not in Edit Mode
                WebUtil.AlertMsg(Page, "Unable to save. It is not in Edit Mode.");
            }
        }
        catch (ApplicationException ex)
        {
            throw new ApplicationException(ex.Message);
        }
        catch (Exception ex)
        {
            LogMsg("SaveEditInfo:Err:" + ex.Message);
            throw new ApplicationException("Fail to save.");
        }
    }

    private string ConvTime(int rowNum, string txtBoxName, string refUfisDt)
    {
        string stDt = "";
        try
        {
            string stTime = GetTextBoxData(rowNum, txtBoxName);
            if (stTime != "")
            {
                if (UtilTime.IsValidHHmm(stTime))
                {
                    stDt = UtilTime.ConvHHmmToUfisTimeAfterRefTime(refUfisDt, stTime);
                }
                else
                {
                    throw new ApplicationException("Invalid Time Format.");
                }
            }
        }
        catch (ApplicationException ex)
        {
            throw new ApplicationException(ex.Message);
        }
        catch (Exception)
        {
        }
        return stDt;
    }

    private string GetTextBoxData(int rowNum, string txtBoxName)
    {
        string st = "";
        try
        {
            st = ((TextBox)gvFlight.Rows[rowNum].FindControl(txtBoxName)).Text;
        }
        catch (Exception)
        {

            //throw;
        }
        return st;
    }

    private void LinkServiceReport(GridViewRow row)
    {
        try
        {
            //***Show Link for Service Report.
            string flightUrno = Convert.ToString(DataBinder.Eval(row.DataItem, "URNO"));
            string aoId = "";
            try
            {
                aoId = Convert.ToString(DataBinder.Eval(row.DataItem, "AOID"));
            }
            catch (Exception)
            {
            }

            CtrlSec ctrlSec = CtrlSec.GetInstance();

            string st = "";
            RetrieveUserInformation();
            //EntUser user = MgrSess.GetUserInfo(Session);
            //userId = user.UserId;
            if (HasAOReport(row))
            {
                //LogMsg("Has Aoreport, aoid : " + aoId + ", " + flightUrno);
                if (ctrlSec.HasAccessRights(user, AO_RPT_PG_NAME, AO_RPT_CTRL_EDIT))
                {
                    st = "EDIT";
                }
                else
                {
                    st = "VIEW";
                }
                //LogMsg("aoid:" + aoId + ", " + flightUrno + ",Access:" + st);
            }
            else
            {
                //LogMsg("Has no ao report ");
                if (ctrlSec.HasAccessRights(user, AO_RPT_PG_NAME, AO_RPT_CTRL_CREATE))
                {
                    //LogMsg("aoid:" + aoId + ", " + flightUrno + ",Has AccessRight:" + st + ", userid :" + user.UserId);

                    if (user.UserId == aoId)
                    {//OWN Flight
                        st = "CREATE";
                    }
                    else st = "";
                }
                else
                {
                    //LogMsg("No acces right, userid : " + user.UserId + ", " + AO_RPT_PG_NAME + ", " + AO_RPT_CTRL_CREATE);
                }
                //LogMsg("aoid:" + aoId + ", " + flightUrno + ",Access:" + st);
            }

            ((HyperLink)(row.Cells[CELL_NO_AO_RPT]).Controls[0]).Text = st;


        }
        catch (Exception ex)
        {
            LogMsg("LinkServiceReport:Err:" + ex.Message);
        }
    }

    private bool HasAOReport(GridViewRow row)
    {
        bool hasRpt = false;
        try
        {
            string st = Convert.ToString(DataBinder.Eval(row.DataItem, "HAS_AO_RPT")).Trim().ToUpper();
            if (st == "Y")
            {
                hasRpt = true;
            }
			//else
			//{
				//string flightUrno = Convert.ToString(DataBinder.Eval(row.DataItem, "URNO"));
				//hasRpt = CtrlAsr.IsConfirmedReportExist(flightUrno);
#warning //TOD0 - To update the Indicator to show that Apron Service Report is existed.
				//if (hasRpt)
				//{
				//    CtrlFlight.GetInstance().UpdateApronServiceReportExist(flightUrno, hasRpt);
				//}
			//}

        }
        catch (Exception ex)
        {
            LogMsg("HasAOReport:Err:" + ex.Message);
        } 
        return hasRpt;
    }

    protected void gvFlight_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            try
            {
                MiscRender.HighLightTransit(e.Row, 1);
                LinkServiceReport(e.Row);
            }
            catch (Exception)
            {
            }
        }
    }

    //[Conditional("LOGINNG_ON")]
    private void LogMsg(string msg)
    {
        UtilLog.LogToGenericEventLog("WUDepApr", msg);
    }
}
