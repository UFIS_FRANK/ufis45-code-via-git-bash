using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.Util;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.ENT;
using UFIS.Web.ITrek.Sec;
using MMSoftware.MMWebUtil;
using MM.UtilLib;

public partial class Web_iTrek_BMPassFail : System.Web.UI.Page
{
    DSFlight ds=null;
    private const string PG_NAME = "BM_Arr";
    private const string CTRL_VIEW = "View";
    EntUser user = null;

    protected void Page_Load(object sender, EventArgs e)
    {
            ShowInfo();
    }

    private string GetParam(string paramKey)
    {
        string st = "";
        try
        {
            st = Request.Params[paramKey].ToString().Trim();
        }
        catch (Exception)
        {
        }
        return st;
    }

    private void ShowInfo()
    {
        pnlArrApron.Visible = false;
        pnlArrBaggage.Visible = false;
        string pageName = PG_NAME;

        string flightNo = "";
        string urNo = "";
        string AppBag = "";
        string ArrDep = "";
        string stFL = "";

        try
        {
            CtrlSec ctrlsec = CtrlSec.GetInstance();
            user = ctrlsec.AuthenticateUser(Session, Response);

            flightNo = Request.Params["FLNO"].ToString().Trim();
            urNo = Request.Params["URNO"].ToString().Trim();
            AppBag = Request.Params["AB"].ToString().Trim();
            ArrDep = GetParam("AD");
            stFL = GetParam("FL");

            if (AppBag == "A")
            {
                pageName += "Apr";
            }
            else if (AppBag == "B")
            {
                pageName += "Bag";
            }

            if (ctrlsec.HasAccessRights(user, pageName, CTRL_VIEW))
            {
                ds = CtrlFlight.RetrieveFlightsByFlightId(urNo);

                switch (AppBag)
                {
                    case "A": //Apron
                        ShowArrivalApron(urNo, flightNo);
                        break;
                    case "B": //Baggage
                        if (String.IsNullOrEmpty(stFL))
                            ShowArrivalBaggage(urNo, flightNo);
                        else
                            ShowArrivalBaggageSingle(urNo, flightNo, stFL);
                        break;
                    default://Show both
                        ShowArrivalApron(urNo, flightNo);
                        ShowArrivalBaggage(urNo, flightNo);
                        break;
                }
            }
            else
            {
                lblMsg.Text = "You are not authorised.";
            }
        }
        catch (Exception ex)
        {
            string st = ex.Message;
        }
    }

    private void ShowArrivalApron(string urNo, string flightNo)
    {
        if (CtrlSec.GetInstance().HasAccessRights(user,PG_NAME + "Apr", CTRL_VIEW))
        {
            pnlArrApron.Visible = true;
            //wuFlightArrApron1.PopulateGridForAFlight(urNo, flightNo);
            lblArrAprHdr.Visible = true;
            lblArrAprHdr.Text = "Arrival Flight - Apron - Benchmark for Flight No. ( " + flightNo + " )";
            if ((ds != null) && (ds.FLIGHT.Rows.Count == 1))
            {
                DSFlight.FLIGHTRow row = ds.FLIGHT[0];
                DateTime dtRefTime = UtilTime.ConvUfisTimeStringToDateTime(row.TBS_UP);
                //DateTime dtFirstBag = UtilTime.ConvUfisTimeStringToDateTime(row.AP_FST_TRP);
                //DateTime dtLastBag = UtilTime.ConvUfisTimeStringToDateTime(row.AP_LST_TRP);
                DateTime dtFirstBag = UtilTime.ConvUfisTimeStringToDateTime(row.FST_BAG);
                DateTime dtLastBag = UtilTime.ConvUfisTimeStringToDateTime(row.LST_BAG);
                string firstBagCssClass = "";
                string lastBagCssClass = "";
                string firstBagPF = "";
                string lastBagPF = "";
                switch (row.AP_FST_BM_MET.ToUpper().Trim())
                {
                    case "P":
                        firstBagCssClass = "BMPass";
                        firstBagPF = "Pass";
                        break;
                    case "F":
                        firstBagCssClass = "BMFail";
                        firstBagPF = "Fail";
                        break;
                }

                switch (row.AP_LST_BM_MET.ToUpper().Trim())
                {
                    case "P":
                        lastBagCssClass = "BMPass";
                        lastBagPF = "Pass";
                        break;
                    case "F":
                        lastBagCssClass = "BMFail";
                        lastBagPF = "Fail";
                        break;
                }

                lblArrApFirstBag.Text = dtFirstBag.ToLongTimeString();
                lblArrAprRefTime.Text = dtRefTime.ToLongTimeString();
                lblArrAprRefTime2.Text = lblArrAprRefTime.Text;
                lblArrApLastBag.Text = dtLastBag.ToLongTimeString();
                lblArrApFirstBagTime.Text = TimeDiff(dtFirstBag, dtRefTime, "M").ToString();
                lblArrApLastBagTime.Text = TimeDiff(dtLastBag, dtRefTime, "M").ToString();
                lblArrApFirstBagBenchMark.Text = row.AP_FST_BM.ToString();
                lblArrApLastBagBenchMark.Text = row.AP_LST_BM.ToString();
                lblArrApFirstBagPassFail.Text = firstBagPF;
                lblArrApFirstBagPassFail.CssClass = firstBagCssClass;
                lblArrApLastBagPassFail.Text = lastBagPF;
                lblArrApLastBagPassFail.CssClass = lastBagCssClass;
                //lblArrApFirstBagTime.Text = dtFirstBag - dtRefTime;
                //lblArrApLastBagTime.Text = dtLastBag - dtRefTime;
                //lblArrBgFirstBagBM.Text = CtrlFlight.
            }
        }
    }


    /// <summary>
    /// Get the time difference (dt1-dt2) in the unit of "dtUnitInd"
    /// </summary>
    /// <param name="dt1"></param>
    /// <param name="dt2"></param>
    /// <param name="dtUnitInd">'S'-Sec, 'M'-Min, 'H'-Hour</param>
    /// <returns></returns>
    private int TimeDiff(DateTime dt1, DateTime dt2, string dtUnitInd)
    {
        int diff = 0;

        switch (dtUnitInd)
        {
            case "S": //Time Difference in Second
                diff += dt1.Second - dt2.Second + (dt1.Minute - dt2.Minute) * 60 +
                    (dt1.Hour - dt2.Hour) * 3600 + (dt1.DayOfYear - dt2.DayOfYear) * 3600 * 24;
                break;
            case "M": //Time Difference in Minute
                diff += (dt1.Minute - dt2.Minute) +
                    (dt1.Hour - dt2.Hour) * 60 + (dt1.DayOfYear - dt2.DayOfYear) * 60 * 24;
                break;
            case "H":
                diff += (dt1.Hour - dt2.Hour) + (dt1.DayOfYear - dt2.DayOfYear) * 24;
                break;
            default:
                throw new ApplicationException("Invalid Unit to Compute the Time Difference.");
                //break;
        }
        return diff;
    }


    private void ShowArrivalBaggageSingle(string urNo, string flightNo, string stFL)
    {
        if (CtrlSec.GetInstance().HasAccessRights(user, PG_NAME + "Bag", CTRL_VIEW))
        {
            pnlBASingle.Visible = true;
            lblHdr.Visible = true;
            lblHdr.Text = "Arrival Flight - Baggage - Benchmark for Flight No. ( " + flightNo + " )";

            if ((ds != null) && (ds.FLIGHT.Rows.Count == 1))
            {
                DSFlight.FLIGHTRow row = ds.FLIGHT[0];
                DateTime dtRefTime = UtilTime.ConvUfisTimeStringToDateTime(row.TBS_UP);
                DateTime dtActTime=new DateTime(1,1,1);
                string cssClass = "";
                string stPF = "";
                string stSubHdr = "";
                string stBM = "";
                string stPFToShow = "";

                if (stFL == "F")
                {
                    dtActTime = UtilTime.ConvUfisTimeStringToDateTime(row.FST_BAG);
                    stPF = row.BG_FST_BM_MET.ToUpper().Trim();
                    stSubHdr = "First Bag";
                    stBM = row.BG_FST_BM.ToString();
                }
                else if (stFL == "L")
                {
                    dtActTime = UtilTime.ConvUfisTimeStringToDateTime(row.LST_BAG);
                    stPF = row.BG_LST_BM_MET.ToUpper().Trim();
                    stSubHdr = "Last Bag";
                    stBM = row.BG_LST_BM.ToString();
                }

                switch (stPF)
                {
                    case "P":
                        cssClass = "BMPass";
                        stPFToShow = "Pass";
                        break;
                    case "F":
                        cssClass = "BMFail";
                        stPFToShow = "Fail";
                        break;
                }

                lblSubHdr.Text = stSubHdr + " Timing";
                lblRefTimeData.Text = dtRefTime.ToLongTimeString();
                lblActTimeLbl.Text = stSubHdr;
                lblActTimeData.Text = dtActTime.ToLongTimeString();
                lblTimingLbl.Text = stSubHdr + " Time";
                lblTimingData.Text = TimeDiff(dtActTime, dtRefTime, "M").ToString();
                lblBMTimingData.Text = stBM;
                lblPF.Text = stPFToShow;
                lblPF.CssClass = cssClass;
            }
        }
    }

    private void ShowArrivalBaggage(string urNo, string flightNo)
    {
        if (CtrlSec.GetInstance().HasAccessRights(user,PG_NAME + "Bag", CTRL_VIEW))
        {
            pnlArrBaggage.Visible = true;
            //wuFlightArrBaggage1.PopulateGridForAFlight(urNo, flightNo);
            lblArrBagHdr.Visible = true;
            lblArrBagHdr.Text = "Arrival Flight - Baggage - Benchmark for Flight No. ( " + flightNo + " )";

            if ((ds != null) && (ds.FLIGHT.Rows.Count == 1))
            {
                DSFlight.FLIGHTRow row = ds.FLIGHT[0];
                DateTime dtRefTime = UtilTime.ConvUfisTimeStringToDateTime(row.AT);
                DateTime dtFirstBag = UtilTime.ConvUfisTimeStringToDateTime(row.FST_BAG);
                DateTime dtLastBag = UtilTime.ConvUfisTimeStringToDateTime(row.LST_BAG);
                string firstBagCssClass = "";
                string lastBagCssClass = "";
                string firstBagPF = "";
                string lastBagPF = "";
                switch (row.BG_FST_BM_MET.ToUpper().Trim())
                {
                    case "P":
                        firstBagCssClass = "BMPass";
                        firstBagPF = "Pass";
                        break;
                    case "F":
                        firstBagCssClass = "BMFail";
                        firstBagPF = "Fail";
                        break;
                }

                switch (row.BG_LST_BM_MET.ToUpper().Trim())
                {
                    case "P":
                        lastBagCssClass = "BMPass";
                        lastBagPF = "Pass";
                        break;
                    case "F":
                        lastBagCssClass = "BMFail";
                        lastBagPF = "Fail";
                        break;
                }

                lblArrBgFirstBag.Text = dtFirstBag.ToLongTimeString();
                lblArrBagRefTime.Text = dtRefTime.ToLongTimeString();
                lblArrBagRefTime2.Text = lblArrBagRefTime.Text;
                lblArrBgLastBag.Text = dtLastBag.ToLongTimeString();
                lblArrBgFirstBagTime.Text = TimeDiff(dtFirstBag, dtRefTime, "M").ToString();
                lblArrBgLastBagTime.Text = TimeDiff(dtLastBag, dtRefTime, "M").ToString();
                lblArrBgFirstBagBM.Text = row.BG_FST_BM.ToString();
                lblArrBgLastBagBM.Text = row.BG_LST_BM.ToString();
                lblArrBgFirstBagPassFail.Text = firstBagPF;
                lblArrBgFirstBagPassFail.CssClass = firstBagCssClass;
                lblArrBgLastBagPassFail.Text = lastBagPF;
                lblArrBgLastBagPassFail.CssClass = lastBagCssClass;
            }

        }
    }

}
