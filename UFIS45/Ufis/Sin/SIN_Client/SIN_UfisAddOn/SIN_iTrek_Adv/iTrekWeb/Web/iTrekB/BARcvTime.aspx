<%@ Page Language="C#" MasterPageFile="~/Web/iTrekB/Master.master" AutoEventWireup="true" CodeFile="BARcvTime.aspx.cs" Inherits="Web_iTrekB_BARcvTime" Title="Baggage Arrival - Receive Timing" Theme="SATSTheme" %>

<%@ Register Src="WuBAFlight.ascx" TagName="WuBAFlight" TagPrefix="uc1" %>
<%@ Register Src="../Wu/WuTime.ascx" TagName="WuTime" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cph1" Runat="Server">
    <uc1:WuBAFlight ID="WuBAFlight1" runat="server" Collapsed="false" EnableUldSelect="false" />
    <hr />
    <div class="Heading">
    <asp:Label ID="lblHead" runat="server" CssClass="Heading" Text="Receive Timing"></asp:Label>
    </div>
    <uc2:WuTime ID="WuTime1" runat="server" />
    
</asp:Content>

