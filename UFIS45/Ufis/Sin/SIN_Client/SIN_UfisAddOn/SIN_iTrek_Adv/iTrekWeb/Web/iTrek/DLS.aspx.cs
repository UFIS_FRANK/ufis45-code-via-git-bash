using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.ENT;
using UFIS.Web.ITrek.Sec;
using iTrekSessionMgr;

public partial class Web_iTrek_DLS : System.Web.UI.Page
{
    private const string PG_NAME = "DLS";
    private const string CTRL_VIEW = "View";

    protected void Page_Load(object sender, EventArgs e)
    {
        CtrlSec ctrlsec = CtrlSec.GetInstance();
        EntUser user = ctrlsec.AuthenticateUser(Session, Response);
        if (ctrlsec.HasAccessRights(user, PG_NAME, CTRL_VIEW))
        {
            ShowFlightInfo();
        }
        else
        {
            DoUnAuthorise();
        }
    }

    private void DoUnAuthorise()
    {
        Response.Redirect("~/UnAuthorised.aspx", true);
    }

    private void ShowFlightInfo()
    {
        string flightNo = "", urNo = "", arrDep = "", aprBag = "";
        try
        {
            flightNo = Request.Params["FLNO"].ToString().Trim();
            urNo = Request.Params["URNO"].ToString().Trim();
            arrDep = Request.Params["AD"].ToString().Trim();
            aprBag = Request.Params["AB"].ToString().Trim();
        }
        catch (Exception)
        {
        }
        string dlsMsg = "";
        try
        {
            switch (aprBag)
            {
                case "A": //Departure Apron
                    wuFlightDepApron1.Visible = true;
                    wuFlightDepApron1.PopulateGridForAFlight(urNo, flightNo);
                    break;
                case "B": //Departure Baggage
                    wuFlightDepBaggage1.Visible = true;
                    wuFlightDepBaggage1.PopulateGridForAFlight(urNo, flightNo);
                    break;
                default:
                    break;
            }
        }
        catch (Exception)
        {
        }

        try
        {
            dlsMsg = CtrlDLS.RetrieveDLSForAFlight(urNo, flightNo, Session.SessionID);
            SessionManager sessMgr = new SessionManager();
            //dlsMsg = sessMgr.GetShowDLS(flightNo, urNo, Session.SessionID);
            //dlsMsg = CtrlDLS.GetDLSFromSessionDir("");
        }
        catch (ApplicationException ex)
        {
            dlsMsg = ex.Message;
        }
        catch (Exception ex)
        {
            dlsMsg = ex.Message;
            dlsMsg = "DLS Message error.";
            //throw;
        }
        txtDLS.Text = dlsMsg;
    }

}
