<%@ Control Language="C#" AutoEventWireup="true" CodeFile="WUFlightArrBaggage.ascx.cs" Inherits="Web_iTrek_WUFlightArrBaggage" %>
<div style="width:97%;" class="Heading">Arrival Flight - Baggage
&nbsp&nbsp&nbsp<asp:Label ID="lblHdr" runat="server" Text=" " CssClass="SubHeading" EnableViewState="False"></asp:Label>
</div>
<div>
<asp:HiddenField ID="hfScrollTop" runat="server" Value="0" />
<asp:GridView ID="gvFlight" runat="server" AutoGenerateColumns="False" BackColor="White"
    BorderColor="#999999" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" ForeColor="Black"
    GridLines="Vertical" OnRowDataBound="gvFlight_RowDataBound">
    <FooterStyle BackColor="#CCCCCC" />
    <Columns>
        <asp:BoundField DataField="URNO" HeaderText="URNO" Visible="False" ReadOnly="True" />
        <asp:TemplateField HeaderText="Arrival">
            <ItemStyle HorizontalAlign="Left" Width="90px" />
            <ItemTemplate>
                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# Eval("URNO", "ChgArrBaggageFlightTiming.aspx?URNO={0}") + Eval("FLNO","&FLNO={0}&AD=A&AB=B") %>'
                    Text='<%# Eval("FLNO") %>'></asp:HyperLink>
                <asp:Literal ID="ltlTfln" runat="server" Text='<%# Eval("TFLN", "<BR>({0})") %>'></asp:Literal>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="REGN" HeaderText="Regn" ReadOnly="True" />
        <asp:BoundField DataField="ST_TIME" HeaderText="STA" HtmlEncode="False" ReadOnly="True" />
        <asp:BoundField DataField="ET_TIME" HeaderText="ETA" HtmlEncode="False" ReadOnly="True" />
        <asp:BoundField DataField="AT_TIME" HeaderText="ATA" HtmlEncode="False" ReadOnly="True" />
        <asp:BoundField DataField="BELT" HeaderText="Belt" ReadOnly="True" />
        <asp:BoundField DataField="TBS_UP_TIME" HeaderText="TBS UP" ReadOnly="True" />
        <asp:TemplateField HeaderText="First Trip">
            <EditItemTemplate>
                &nbsp;<table>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblEdFirstTrip" runat="server" Text='<%# Eval("BG_FST_TRP_TIME", "{0:hhmm}") + "&nbsp" %>'></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:TextBox ID="txtNewFirstTrip" runat="server" Text='<%# Bind("BG_FST_TRP_TIME", "{0:hhmm}") %>'
                                Width="53px"></asp:TextBox></td>
                    </tr>
                </table>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblDispFirstTrip" runat="server" Text='<%# Bind("BG_FST_TRP_TIME", "{0:hhmm}") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="First Bag">
            <EditItemTemplate>
                &nbsp;<table>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblEdFirstBag" runat="server" Text='<%# Eval("FST_BAG_TIME", "{0:hhmm}") + "&nbsp" %>'></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:TextBox ID="txtNewFirstBag" runat="server" Text='<%# Bind("FST_BAG_TIME", "{0:hhmm}") %>'
                                Width="48px"></asp:TextBox></td>
                    </tr>
                </table>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblDispFirstBag" runat="server" Text='<%# Bind("FST_BAG_TIME", "{0:hhmm}") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Last Trip">
            <EditItemTemplate>
                &nbsp;<table>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblDispLastTrip" runat="server" Text='<%# Eval("BG_LST_TRP_TIME", "{0:hhmm}") + "&nbsp" %>'></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:TextBox ID="txtNewLastTrip" runat="server" Text='<%# Bind("BG_LST_TRP_TIME", "{0:hhmm}") %>'
                                Width="43px"></asp:TextBox></td>
                    </tr>
                </table>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblDispLastTrip" runat="server" Text='<%# Bind("BG_LST_TRP_TIME", "{0:hhmm}") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Last Bag">
            <EditItemTemplate>
                &nbsp;<table>
                    <tr>
                        <td style="width: 100px">
                            <asp:Label ID="lblEdLastBag" runat="server" Text='<%# Eval("LST_BAG_TIME", "{0:hhmm}") + "&nbsp" %>'></asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <asp:TextBox ID="txtNewLastBag" runat="server" Text='<%# Bind("LST_BAG_TIME", "{0:hhmm}") %>'
                                Width="50px"></asp:TextBox></td>
                    </tr>
                </table>
            </EditItemTemplate>
            <ItemTemplate>
                <asp:Label ID="lblDispLastBag" runat="server" Text='<%# Bind("LST_BAG_TIME", "{0:hhmm}") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField HeaderText="First Bag Timing" ReadOnly="True" DataField="BG_FST_BAG_TIMING" />
        <asp:BoundField HeaderText="Last Bag Timing" ReadOnly="True" DataField="BG_LST_BAG_TIMING" />
<asp:TemplateField HeaderText="FBag Pass/Fail">
        <ItemTemplate>
            <asp:LinkButton ID="lbBMPassFailFBag" runat="server" CausesValidation="false" CommandName=""
                EnableViewState="True" OnClientClick='<%# "open_window(\"BMPassFail.aspx?URNO=" + Eval("URNO") + "&FLNO=" + Eval("FLNO") + "&FL=F&AD=A&AB=B\");" %>'></asp:LinkButton>
        </ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField HeaderText="LBag Pass/Fail">
        <ItemTemplate>
            <asp:LinkButton ID="lbBMPassFailLBag" runat="server" CausesValidation="false" CommandName=""
                EnableViewState="True" OnClientClick='<%# "open_window(\"BMPassFail.aspx?URNO=" + Eval("URNO") + "&FLNO=" + Eval("FLNO") + "&FL=L&AD=A&AB=B\");" %>'></asp:LinkButton>
        </ItemTemplate>
    </asp:TemplateField>
        <asp:HyperLinkField DataNavigateUrlFields="URNO,FLNO" DataNavigateUrlFormatString="CPM.aspx?URNO={0}&amp;FLNO={1}&amp;AD=A&amp;AB=B"
            HeaderText="CPM" Text="Show" />
        <asp:HyperLinkField DataNavigateUrlFields="URNO,FLNO" DataNavigateUrlFormatString="BaggagePresentationTiming.aspx?URNO={0}&amp;FLNO={1}&amp;AD=A&amp;AB=B"
            HeaderText="Baggage Timing Report" />
        <asp:HyperLinkField DataNavigateUrlFields="URNO,FLNO" DataNavigateUrlFormatString="FlightMessage.aspx?URNO={0}&amp;FLNO={1}&amp;AD=A&amp;AB=B"
            HeaderText="MSG" Text="New" />
    </Columns>
    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
    <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
    <HeaderStyle BackColor="Black" Font-Bold="True" ForeColor="White" />
    <AlternatingRowStyle BackColor="#CCCCCC" />
</asp:GridView>
</div>
