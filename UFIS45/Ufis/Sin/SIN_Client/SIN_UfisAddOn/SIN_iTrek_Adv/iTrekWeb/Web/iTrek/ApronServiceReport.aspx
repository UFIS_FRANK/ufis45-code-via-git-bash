<%@ Page Language="C#" MasterPageFile="~/Web/iTrek/MasterPage.master" AutoEventWireup="true" CodeFile="ApronServiceReport.aspx.cs" Inherits="Web_iTrek_ApronServiceReport" Title="Apron Service Report" Theme="SATSTheme" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" Runat="Server">
    <table style="width: 1000px">
        <tr>
            <td class="Heading" style="width: 900px">
                APRON SERVICE REPORT</td>
        </tr>
    </table>
    <div>
    <br />
                <table style="width: 750px">
                    <tr>
                        <td style="width: 150px; height: 26px;">
                            ALO Name</td>
                        <td style="width: 100px; height: 26px;">
                            <asp:TextBox ID="txtAOName" runat="server" ReadOnly="True" Width="221px"></asp:TextBox></td>
                        <td style="width: 100px; height: 26px;">
                        </td>
                        <td style="width: 100px; height: 26px;">
                            Flight No</td>
                        <td style="width: 100px; height: 26px;">
                            <asp:TextBox ID="txtFlightNo" runat="server" ReadOnly="True"></asp:TextBox></td>
                        <td style="height: 26px">
                        </td>
                        <td style="height: 26px" >
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            Date</td>
                        <td style="width: 100px">
                            <asp:TextBox ID="txtFlightDate" runat="server" ReadOnly="True" Width="220px"></asp:TextBox></td>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 100px">
                            STA</td>
                        <td style="width: 100px">
                            <asp:TextBox ID="txtSTA" runat="server" ReadOnly="True"></asp:TextBox></td>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 100px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            Regn</td>
                        <td style="width: 100px">
                            <asp:TextBox ID="txtRegn" runat="server" ReadOnly="True" Width="220px"></asp:TextBox></td>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 100px">
                            ATA</td>
                        <td style="width: 100px">
                            <asp:TextBox ID="txtATA" runat="server" ReadOnly="True"></asp:TextBox></td>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 100px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            Type</td>
                        <td style="width: 100px">
                            <asp:TextBox ID="txtType" runat="server" ReadOnly="True" Width="219px"></asp:TextBox></td>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 100px">
                            STD</td>
                        <td style="width: 100px">
                            <asp:TextBox ID="txtSTD" runat="server" ReadOnly="True"></asp:TextBox></td>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 100px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px; height: 26px;">
                            From</td>
                        <td style="width: 100px; height: 26px;">
                            <asp:TextBox ID="txtFrom" runat="server" ReadOnly="True" Width="219px"></asp:TextBox></td>
                        <td style="width: 100px; height: 26px;">
                        </td>
                        <td style="width: 100px; height: 26px;">
                            ATD</td>
                        <td style="width: 100px; height: 26px;">
                            <asp:TextBox ID="txtATD" runat="server" ReadOnly="True"></asp:TextBox></td>
                        <td style="width: 100px; height: 26px;">
                        </td>
                        <td style="width: 100px; height: 26px;">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px; height: 26px;">
                            To</td>
                        <td style="width: 100px; height: 26px;">
                            <asp:TextBox ID="txtTo" runat="server" ReadOnly="True" Width="217px"></asp:TextBox></td>
                        <td style="width: 100px; height: 26px;">
                        </td>
                        <td style="width: 100px; height: 26px;">
                            BAY</td>
                        <td style="width: 100px; height: 26px;">
                            <asp:TextBox ID="txtBay" runat="server" ReadOnly="True"></asp:TextBox></td>
                        <td style="width: 100px; height: 26px;">
                        </td>
                        <td style="width: 100px; height: 26px;">
                        </td>
                    </tr>
                </table>
<br /><br />
        <table  style="width: 670px">
            <tr>
                <td style="width: 90px">
                </td>
                <td style="width: 235px">
                </td>
                <td style="width: 3px">
                </td>
                <td style="width: 45px">
                </td>
                <td style="width: 235px">
                </td>
                <td>
                </td>
            </tr>
                <tr>
                    <td colspan="6" class="Heading" style="height: 22px">
                        RSM</td>
                </tr>
            <tr>
                <td style="width: 90px">
                </td>
                <td>
                    &nbsp;<table>
                        <tr>
                            <td style="width: 78px">
                                Vehicle No</td>
                            <td style="width: 18px">
                            </td>
                            <td style="width: 100px">
                                &nbsp; &nbsp; Staff Name</td>
                        </tr>
                    </table>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
                <tr>
                    <td style="width: 90px">
                        PLB Door A</td>
                    <td>
                        <asp:TextBox ID="txtPLBDoorA"   MaxLength="10"  runat="server" Width="72px"></asp:TextBox>
                        <asp:TextBox ID="txtPLBDoorAName"   MaxLength="20"  runat="server" Width="145px"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                    <td>
                        SL No. (FWD)</td>
                    <td>
                        <asp:TextBox ID="txtSLFwd"  MaxLength="10"  runat="server" Width="72px"></asp:TextBox>
                        <asp:TextBox ID="txtSLFwdName"  MaxLength="20"  runat="server" Width="145px"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="width: 90px">
                        PLB Door B</td>
                    <td>
                        <asp:TextBox ID="txtPLBDoorB" runat="server" MaxLength="10" Width="72px"></asp:TextBox>
                        <asp:TextBox ID="txtPLBDoorBName" runat="server" MaxLength="20" Width="145px"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                    <td>
                        SL No. (AFT)</td>
                    <td>
                        <asp:TextBox ID="txtSLAft"  MaxLength="10" runat="server" Width="72px"></asp:TextBox>
                        <asp:TextBox ID="txtSLAftName"  MaxLength="20" runat="server" Width="145px"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="width: 90px">
                        JCL No. (FWD)</td>
                    <td>
                        <asp:TextBox ID="txtJCLFwd" MaxLength="10" runat="server" Width="72px"></asp:TextBox>
                        <asp:TextBox ID="txtJCLFwdName" MaxLength="20" runat="server" Width="145px"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                    <td rowspan="2">
                        Mode of Ops</td>
                    <td rowspan="2">
                        <asp:DropDownList ID="ddOpMode" runat="server" Width="206px">
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="O">Originator</asp:ListItem>
                            <asp:ListItem Value="TA">Turnaround</asp:ListItem>
                            <asp:ListItem Value="TS">Transit</asp:ListItem>
                            <asp:ListItem Value="N">Nightstop</asp:ListItem>
                        </asp:DropDownList></td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="width: 90px">
                        JCL No. (AFT)</td>
                    <td>
                        <asp:TextBox ID="txtJCLAft" MaxLength="10" runat="server" Width="72px"></asp:TextBox>
                        <asp:TextBox ID="txtJCLAftName" MaxLength="20" runat="server" Width="145px"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="width: 90px">
                        TR No.</td>
                    <td >
                        <asp:TextBox ID="txtTRNo" MaxLength="10" runat="server" Width="72px"></asp:TextBox>
                        <asp:TextBox ID="txtTRNoName" MaxLength="20" runat="server" Width="145px"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                    <td>
                        Load Change</td>
                    <td>
                        <asp:DropDownList ID="ddLoadChange" runat="server" Width="206px">
                            <asp:ListItem Selected="True"></asp:ListItem>
                            <asp:ListItem Value="P">Partial</asp:ListItem>
                            <asp:ListItem Value="C">Complete</asp:ListItem>
                        </asp:DropDownList></td>
                    <td>
                    </td>
                </tr>
            </table>
            </div>
        <div>
            <br /><br />
            <table style="width: 620px">
                <tr>
                    <td colspan="3" class="Heading" style="height: 32px">
                        ARRIVAL</td>
                    <td colspan="3" class="Heading" style="height: 32px">
                        DEPARTURE</td>
                </tr>
                <tr>
                    <td style="width: 160px">
                        Time First Bag Despatched</td>
                    <td style="width: 70px">
                        <asp:TextBox ID="txtFirstBag" runat="server" ReadOnly="True" Width="70px"></asp:TextBox></td>
                    <td style="width: 10px">
                    </td>
                    <td style="width: 160px">
                        Time Loading Commenced</td>
                    <td style="width: 70px">
                        <asp:TextBox ID="txtLoadStart" runat="server" ReadOnly="True" Width="70px"></asp:TextBox></td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        Time Last Bag Despatched</td>
                    <td>
                        <asp:TextBox ID="txtLastBag" runat="server" ReadOnly="True" Width="70px"></asp:TextBox></td>
                    <td>
                    </td>
                    <td>
                        Time Loading Completed</td>
                    <td>
                        <asp:TextBox ID="txtLoadEnd" runat="server" ReadOnly="True" Width="70px"></asp:TextBox></td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                        Time Unloading Completed</td>
                    <td>
                        <asp:TextBox ID="txtUnlEnd" runat="server" ReadOnly="True" Width="69px"></asp:TextBox></td>
                    <td>
                    </td>
                    <td>
                        Time Last GSE Removed/Cargo Door Closed</td>
                    <td>
                        <asp:TextBox ID="txtDoorClose" runat="server" ReadOnly="True" Width="69px"></asp:TextBox></td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                    </td>
                    <td>
                        Time Last PLB Removed</td>
                    <td>
                        <asp:TextBox ID="txtPLB" runat="server" ReadOnly="True" Width="70px"></asp:TextBox></td>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
        <br /><br />
    <table style="width: 650px;">
        <tr>
            <td style="width: 240px" class="Heading">
                Checklist</td>
            <td style="width: 20px" class="Heading">
                (Tick)</td>
            <td style="width: 10px" class="Heading">
            </td>
            <td style="width: 300px" class="Heading">
                I've checked with my team members to confirm that :</td>
            <td style="width: 20px" class="Heading">
                (Tick)</td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                All My Team Members are Briefed</td>
            <td>
                <asp:CheckBox ID="ckbCHKBrief" runat="server" /></td>
            <td >
            </td>
            <td>
                Aerobridge Tested before operating</td>
            <td>
                <asp:CheckBox ID="ckbCHKAerobridge" runat="server" /></td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                All my team members are wearing safety gear</td>
            <td>
                <asp:CheckBox ID="ckbCHKSafetyGear" runat="server" /></td>
            <td>
            </td>
            <td>
                No damage to the aircraft body or door</td>
            <td>
                <asp:CheckBox ID="ckbCHKBody" runat="server" /></td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Placement of cones before start of activities</td>
            <td>
                <asp:CheckBox ID="ckbCHKCone" runat="server" /></td>
            <td>
            </td>
            <td>
                No leakage / damage of DG</td>
            <td>
                <asp:CheckBox ID="ckbCHKDG" runat="server" /></td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Rails are lowered before loader docks / undocks from acft</td>
            <td>
                <asp:CheckBox ID="ckbCHKRailsLower" runat="server" /></td>
            <td>
            </td>
            <td>
                DG loaded in the position as planned</td>
            <td>
                <asp:CheckBox ID="ckbCHKDGLoaded" runat="server" /></td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Skyloader canopy / rails extended wheelchoke placed</td>
            <td>
                <asp:CheckBox ID="ckbCHKSkyloader" runat="server" /></td>
            <td>
            </td>
            <td>
                ULD restrain looks / stops in the aft / fwd holds raised and secured</td>
            <td>
                <asp:CheckBox ID="ckbCHKULD" runat="server" /></td>
            <td>
            </td>
        </tr>
    </table>
    <br /><br />
    <table style="width: 700px">
        <tr>
            <td style="width: 200px" class="Heading">
                OTHERS</td>
            <td style="width: 16px" class="Heading">
                (TICK)</td>
            <td style="width: 420px" class="Heading">
                Description</td>
            <td>
            </td>
        </tr>
        <tr>
            <td style="height: 18px" >
                ICAO Annex 17</td>
            <td style="height: 18px">
                <asp:CheckBox ID="ckbICAO" runat="server" /></td>
            <td colspan="2" style="height: 18px">
                <asp:TextBox ID="txtOthICAO" runat="server" Width="350px" MaxLength="40"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                Brake checks before docking (failure report)</td>
            <td>
                <asp:CheckBox ID="ckbBreak" runat="server" /></td>
            <td colspan="2">
                <asp:TextBox ID="txtOthBreak" runat="server" Width="350px" MaxLength="40"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                Incorrect CPM or no CPM</td>
            <td >
                <asp:CheckBox ID="ckbOthCPM" runat="server" /></td>
            <td colspan="2">
                <asp:TextBox ID="txtOthCPM" runat="server" Width="350px" MaxLength="40"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                Baggage / Cargo sent wrongly to flight</td>
            <td >
                <asp:CheckBox ID="ckbOthWrongBag" runat="server" /></td>
            <td colspan="2">
                <asp:TextBox ID="txtOthWrongBag" runat="server" Width="350px" MaxLength="40"></asp:TextBox></td>
        </tr>
        <tr>
            <td >
                Inclement weather</td>
            <td >
                <asp:CheckBox ID="ckbOthWeather" runat="server" /></td>
            <td colspan="2">
                <asp:TextBox ID="txtOthWeather" runat="server" Width="350px" MaxLength="40"></asp:TextBox></td>
        </tr>
        <tr>
            <td >
                Lightning warning</td>
            <td>
                <asp:CheckBox ID="ckbOthLighting" runat="server" /></td>
            <td colspan="2">
                <asp:TextBox ID="txtOthLighting" runat="server" Width="350px" MaxLength="40"></asp:TextBox></td>
        </tr>
        <tr>
            <td >
                Poor housekeeping of bay</td>
            <td >
                <asp:CheckBox ID="ckbOthPoorBay" runat="server" /></td>
            <td colspan="2">
                <asp:TextBox ID="txtOthPoorBay" runat="server" Width="350px" MaxLength="40"></asp:TextBox></td>
        </tr>
        <tr>
            <td >
                Overstowing in bulkhold</td>
            <td >
                <asp:CheckBox ID="ckbOthOverStowing" runat="server" /></td>
            <td colspan="2">
                <asp:TextBox ID="txtOthOverStowing" runat="server" Width="350px" MaxLength="40"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                FOD found before flight arrival</td>
            <td >
                <asp:CheckBox ID="ckbOthFOD" runat="server" /></td>
            <td colspan="2">
                <asp:TextBox ID="txtOthFOD" runat="server" Width="350px" MaxLength="40"></asp:TextBox></td>
        </tr>
        <tr>
            <td >
                Inflight loading system unserviceable</td>
            <td >
                <asp:CheckBox ID="ckbOthInFlight" runat="server" /></td>
            <td colspan="2">
                <asp:TextBox ID="txtOthInFlight" runat="server" Width="350px" MaxLength="40"></asp:TextBox></td>
        </tr>
        <tr>
            <td >
                Jammed aircraft cargo door</td>
            <td >
                <asp:CheckBox ID="ckbOthDoorJam" runat="server" /></td>
            <td colspan="2">
                <asp:TextBox ID="txtOthDoorJam" runat="server" Width="350px" MaxLength="40"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                GSE unserviceable (JCP / TPT / SL)</td>
            <td >
                <asp:CheckBox ID="ckbOthGSE" runat="server" /></td>
            <td colspan="2">
                <asp:TextBox ID="txtOthGSE" runat="server" Width="350px" MaxLength="40"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                Warped / damaged container / pallet</td>
            <td >
                <asp:CheckBox ID="ckbOthDamageContainer" runat="server" /></td>
            <td colspan="2">
                <asp:TextBox ID="txtOthDamageContainer" runat="server"  Width="350px" MaxLength="40"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                Dangerous cargo offloaded (ULD / Trolley No)</td>
            <td >
                <asp:CheckBox ID="ckbOthDangerCargo" runat="server" /></td>
            <td colspan="2">
                <asp:TextBox ID="txtOthDangerCargo" runat="server" Width="350px" MaxLength="40"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                Blocking of aircraft fueling vehicle (incident report)</td>
            <td>
                <asp:CheckBox ID="ckbOthBlock" runat="server" /></td>
            <td>
                <asp:TextBox ID="txtOthBlock" runat="server" Width="350px" MaxLength="40"></asp:TextBox></td>
        </tr>
    </table>
    <br />
    <div>

    <table style="width: 670px">
        <tr>
            <td class="Heading">
            <br />
                ULD Comments</td>
        </tr>
        <tr>
            <td style="width: 650px; height: 232px;">
                <asp:TextBox ID="txtUldComments" runat="server" Height="383px" TextMode="MultiLine"
                    Width="650px" ReadOnly="True" Columns="100" Rows="20"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="Heading" style="width: 931px">
            <br />
                General Comments</td>
        </tr>
        <tr>
            <td style="width: 650px; height: 155px;">
                <asp:TextBox ID="txtGenComments" runat="server" Height="348px" TextMode="MultiLine"
                    Width="650px" Columns="100" MaxLength="100" Rows="8"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="Heading"><br />
                Reason for BPT failure</td>
        </tr>
        <tr>
            <td style="width: 650px; height: 70px;">
                <asp:TextBox ID="txtBPTReason" runat="server" Height="200px" TextMode="MultiLine"
                    Width="650px" Rows="10"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
            <br />
                <table style="width: 100%">
                    <tr>
                        <td style="width: 150px;" class="Heading">
                            Duty Room Officer</td>
                        <td style="width:85px;">
                            <asp:TextBox ID="txtDutyRoomOfficer" runat="server" Width="150px" MaxLength="20"></asp:TextBox></td>
                        <td style="width: 10px;">
                        </td>
                        <td style="width: 140px;" class="Heading">
                            Duty Manager</td>
                        <td style="width: 100px;">
                            <asp:TextBox ID="txtDutyManager" runat="server" Width="150px" MaxLength="20"></asp:TextBox></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 700px">
                <table style="width: 500px">
                    <tr>
                        <td >
                        </td>
                        <td >
                            &nbsp;</td><td>
                            <asp:Button ID="btnSave" runat="server" Text="Submit" EnableViewState="False" OnClick="btnSave_Click" Visible="False" Width="105px" OnClientClick="return ValidateMandatory();" /></td>
                        <td>
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CausesValidation="False" EnableViewState="False" OnClick="btnCancel_Click" Visible="False" Width="105px" /></td>
                        <td>
                            <asp:Button ID="btnPrint" runat="server" Text="Print" Width="105px" CausesValidation="False" OnClientClick="PrintMe();" UseSubmitBehavior="False" EnableViewState="False" Visible="False" /></td>
                        <td >
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </div>
</asp:Content>

