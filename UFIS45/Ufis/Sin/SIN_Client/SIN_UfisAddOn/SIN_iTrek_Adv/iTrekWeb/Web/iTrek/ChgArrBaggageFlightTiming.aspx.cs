using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.ENT;
using UFIS.Web.ITrek.Sec;
using MMSoftware.MMWebUtil;

public partial class Web_iTrek_ChgArrBaggageFlightTiming : System.Web.UI.Page
{
    private const string PG_NAME = "ChgTimeArrBag";
    private const string CTRL_VIEW = "Edit";



    protected void Page_PreRender(Object sender, EventArgs e)
    {
        CtrlSec ctrlsec = CtrlSec.GetInstance();
        EntUser user = ctrlsec.AuthenticateUser(Session, Response);
        if (ctrlsec.HasAccessRights(user, PG_NAME, CTRL_VIEW))
        {
            ShowFlightInfo();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CtrlSec ctrlsec = CtrlSec.GetInstance();
        EntUser user = ctrlsec.AuthenticateUser(Session, Response);
        if (ctrlsec.HasAccessRights(user, PG_NAME, CTRL_VIEW))
        {
            //if (!IsPostBack)
            //{
            //    ShowFlightInfo();
            //}
        }
        else
        {
            DoUnAuthorise();
        }
    }

    private void DoUnAuthorise()
    {
        Response.Redirect("~/UnAuthorised.aspx", true);
    }

    private void ShowFlightInfo()
    {
        try
        {
            string flightNo = "", urNo = "";
            flightNo = Request.Params["FLNO"].ToString().Trim();
            urNo = Request.Params["URNO"].ToString().Trim();
            wuFlightArrBaggage1.PopulateGridForAFlight(urNo, flightNo, true);
        }
        catch (Exception ex)
        {
            LogMsg("ShowFlightInfo:Err:" + ex.Message);
            WebUtil.AlertMsg(Page, "Error Loading Flight Information.");
        }
    }

    protected void btnApply_Click(object sender, EventArgs e)
    {
        try
        {
            wuFlightArrBaggage1.SaveEditInfo();
            //System.Threading.Thread.Sleep(2500);
            WebUtil.AlertMsg(Page, "Data was saved.");
        }
        catch (ApplicationException ex)
        {
            WebUtil.AlertMsg(Page, ex.Message);
        }
        catch (Exception ex)
        {
			LogMsg("Save:Err:" + ex.Message);
            WebUtil.AlertMsg(Page, "Unable to Save Data.");
        }
        finally
        {
            ShowFlightInfo();
        }
    }

	#region Logging
	private void LogMsg(string msg)
	{
		try
		{
			MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("WEBChgArrBgT", msg);
		}
		catch (Exception)
		{
			UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("WEBChgArrBgT", msg);
		}
	}

	private void LogTraceMsg(string msg)
	{
		LogMsg(msg);
	}
	#endregion

}
