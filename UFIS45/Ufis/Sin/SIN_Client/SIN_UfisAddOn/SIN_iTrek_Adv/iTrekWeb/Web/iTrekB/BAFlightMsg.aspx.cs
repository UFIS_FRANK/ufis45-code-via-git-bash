using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Collections.Generic;

using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;
using UFIS.Web.ITrek.WebMgr;
using UFIS.Web.ITrek.Sec;

using UFIS.Web.ITrek.Base;

//public partial class Web_iTrekB_BAFlightMsg : System.Web.UI.Page
public partial class Web_iTrekB_BAFlightMsg : BaseWebPage
{

   protected override void OnPreLoad(EventArgs e)
   {
      base.OnPreLoad(e);
      ((Web_iTrekB_Master)Master).AutoRefreshMode = false;
   }

   protected override void OnLoad(EventArgs e)
   {
      base.OnLoad(e);
      if (CtrlSec.GetInstance().HasAccessToFlightMessage(LoginedUser))
      {
         if ((!Page.IsPostBack) && (!Page.IsCallback))
         {
            PopulateData();
         }
      }
      else
      {
         DoUnAuthorise();
      }
   }

   private void PopulateData()
   {
      try
      {
         EntBagArrFlightSelectedUld obj = MgrSess.GetBagArrCurrentSel(this.Session);
         if (obj == null)
         {
         }
         else
         {
            //LogMsg("PopulateData.");
            if ((obj.FlightId == null) || (obj.FlightId.Trim() == ""))
            {
               //this.lblHead.Text = "No Flight Information!!!";
               //wuBAFlight1.Visible = false;
            }
            else
            {
               //wuBAFlight1.PopulateData(obj.FlightId, obj.BagArrType, null, obj.SelectedUld);
               //wuBAFlight1.Collapsed = true;
               //wuBAFlight1.EnableUldSelect = false;
               //wuBAFlight1.Visible = true;
            }

            //List<string> arrGroupList = new List<string>();
            //arrGroupList.Add("AO");
            //arrGroupList.Add("ACC");

            //WuMsg1.PopulateData(LoginedId, LoginedUserGroupList, obj.FlightId, EnComposeMsgType.Flight);
            WuMsg1.PopulateData(EnComposeMsgType.Flight, obj.FlightId);
         }
      }
      catch (Exception ex)
      {
         LogMsg("PopulateData:Err:" + ex.Message);
         //throw;
      }
   }

   private void LogMsg(string msg)
   {
      try
      {
         MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("BAFlightMsg", msg);
      }
      catch (Exception)
      {
         UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("BAFlightMsg", msg);
      }
   }
}