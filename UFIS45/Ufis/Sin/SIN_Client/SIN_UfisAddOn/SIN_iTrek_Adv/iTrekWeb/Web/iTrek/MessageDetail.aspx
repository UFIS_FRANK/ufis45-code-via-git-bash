<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MessageDetail.aspx.cs" Inherits="Web_iTrek_MessageDetail" Theme="SATSTheme" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <link href="../../Web/includes/StyleSheet.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Label ID="lblMsgHdr" runat="server" Height="30px" Width="631px"></asp:Label>
        <br />
        <asp:Label ID="lblMsgTo" runat="server" Height="27px" Width="629px"></asp:Label>
        <br />
        <asp:Label ID="lblRcvBy" runat="server" Height="25px" Width="628px"></asp:Label>
    </div>
    <div>
        <asp:TextBox ID="txtMsgBody" runat="server" Height="190px" TextMode="MultiLine" Width="628px"></asp:TextBox>
    </div>
    <div>
        <br />
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        <asp:Button ID="btnConfirmRead" runat="server" Text="Confirm Read" EnableViewState="False" /> &nbsp; &nbsp;
        <asp:Button ID="btnNotConfirmRead" runat="server" Text="Not Confirm Read" EnableViewState="False" CausesValidation="False" />
        
    </div>
    </form>
</body>
</html>
