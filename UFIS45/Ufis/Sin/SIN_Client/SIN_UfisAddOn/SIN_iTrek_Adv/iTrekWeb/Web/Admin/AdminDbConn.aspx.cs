using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.Config;
using UFIS.ITrekLib.Util;
using UFIS.Web.ITrek.WebMgr;


public partial class Web_Admin_AdminDbConn : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
	{
		if (!MgrSess.IsValidUserToDbConn(Session))
		{
			UnAuthorise();
		}
		else
		{
		}
		lblMsg.Text = "";
		lblInfo.Text = "";
	}
	protected void btnLoad_Click(object sender, EventArgs e)
	{
		try
		{
			txtConn.Text = ITrekConfig.ReadAndGetDbConnString();
		}
		catch (Exception ex)
		{
			lblMsg.Text = ex.Message;
		}
	}
	protected void btnTest_Click(object sender, EventArgs e)
	{
		try
		{
			string stCmd = txtCmd.Text.Trim();
			string stConn = txtConn.Text.Trim();
			IDbCommand cmd = null;

			if ((string.IsNullOrEmpty(stConn)) || (string.IsNullOrEmpty(stCmd)))
			{
				throw new ApplicationException("No Conn/Cmd");
			}
			UtilDb udb = UtilDb.GetInstance();
			object obj = null;

			try
			{
				cmd = udb.GetCommand(stConn, false);
				cmd.CommandText = stCmd;
				cmd.CommandType = CommandType.Text;

				obj = cmd.ExecuteScalar();
				if ((obj != null) && (obj != DBNull.Value))
				{
					string st = obj.ToString();
					lblInfo.Text = stCmd + Environment.NewLine + "Success:Return:" + st;
				}
				else
				{
					lblInfo.Text = stCmd + Environment.NewLine + "Success:No Info Return.";
				}
			}
			catch (Exception ex)
			{
				lblMsg.Text = "Error:" + ex.Message;
			}
			finally
			{
				udb.CloseCommand(cmd, false);
			}			
		}
		catch (Exception ex)
		{
			lblMsg.Text = ex.Message;
		}
	}

	protected void btnSave_Click(object sender, EventArgs e)
	{
		try
		{
			string stConn = txtConn.Text.Trim();
			if (string.IsNullOrEmpty(stConn))
			{
				throw new ApplicationException("No Conn/Cmd");
			}
			ITrekConfig.WriteDbConnStringToFile(stConn);
			lblInfo.Text = "Successfully updated the connection string.";
		}
		catch (Exception ex)
		{
			lblMsg.Text = ex.Message;
		}
	}

	private void UnAuthorise()
	{
		lblMsg.Text = "You are not authorised.";
		SetButtonVisible(false);
	}

	private void SetButtonVisible(bool visible)
	{
		btnLoad.Visible = visible;
		btnSave.Visible = visible;
		btnTest.Visible = visible;
		txtConn.Visible = visible;

		if (visible)
		{
			txtConn.Text = "";
		}
	}
}
