﻿var version = parseInt(navigator.appVersion);

function myRefresh( targetUrl, refreshTimeInMiliSecond )
{
    try{
        if (targetUrl=="") targetUrl = window.location.href;
        var st = "window.location.replace('" + targetUrl + "')";
        
        alert(st);
          // replace is supported
          if (version>=4 || window.location.replace)
            setTimeout(st,refreshTimeInMiliSecond);
          else
            setTimeout("window.location.href ='"+ targetUrl + "'", refreshTimeInMiliSecond);
    }catch (exception ){}
}
 
function selfRefresh( refreshTimeInSecond )
{
    try{
        var targetUrl = window.location.href;
        var st="";
        //targetUrl = "javascript:__doPostBack('ctl00$lbRefreshNow','')";

        // replace is supported
        if (version>=4 || window.location.replace){
            st = 'window.location.replace("' + targetUrl + '")';
        }
        else
            st = "window.location.href ='"+ targetUrl + "'";
        setTimeout(st,refreshTimeInSecond * 1000);
    }catch (exception ){}
}

//var refreshNowId = "";

function selfRefreshPostback( refreshTimeInSecond, postBackClientId )
{
   try
   {
      //alert( "selfRefreshPostback: " + postBackFunction );
      //setTimeout(postBackFunction,refreshTimeInSecond * 1000);
      var st = "javascript:__doPostBack('" + postBackClientId + "','')";
      //alert( "selfRefreshPostback: " + st );
      //refreshNowId = postBackClientId;
      setTimeout(st,refreshTimeInSecond * 1000);
   }
   catch(ex){}
}

//function clickRefreshNow()
//{
//   if (refreshNowId!="")
//   {
//      alert("ClickRefreshNow: " + refreshNowId );
//      clickElement(refreshNowId);
//   }
//}


function selfRefreshNow()
{
    GotoThePage( window.location.href );
}


function GotoThePage( targetUrl )
{
    try{
        var st="";

        // replace is supported
        if (version>=4 || window.location.replace)
            window.location.replace(  targetUrl);
        else
            window.location.href =targetUrl ;
        
    }catch (exception ){}
}


function ShowPopup(urlName) {
  //alert('showpopup' + urlName);
  window.showModalDialog(urlName, "Dialog", 
     "dialogWidth:200px;dialogHeight:300px;");
}


function open_window(myurl) {
   var winwidth = window.screen.availWidth - 30;
   var winheight = window.screen.availHeight - 50;
   var winleft = 0+10;
   var wintop = 0+10;

   var newWindow;
   var props = 'scrollBars=yes,resizable=yes,toolbar=no,menubar=no,location=no,directories=no,width=' + winwidth + ',height=' + winheight + ',top=' + wintop + ',left=' + winleft;
   newWindow = window.open(myurl, '', props);
}

function open_small_window(myurl) {
   var winwidth = 150;
   var winheight = 10;
   var winleft = window.screen.availWidth / 2-80;
   var wintop = window.screen.availHeight/2-10;

   var newWindow;
   var props = 'scrollBars=yes,resizable=no,toolbar=no,menubar=no,location=no,directories=no,width=' + winwidth + ',height=' + winheight + ',top=' + wintop + ',left=' + winleft;
   newWindow = window.open(myurl, '', props);
}

function PrintMe()
{
    window.print();
}

function EquWidth(cbDet,width)
{
  var isOk = true;

  try{
    var chkBoxList = document.getElementById(cbDet);
    var vTd= chkBoxList.getElementsByTagName('td');
    
    if ((vTd!=undefined) && (vTd!=null))
    {
      for(var i=0;i<vTd.length;i++)
      {
        var td = vTd.item(i);
        td.setAttribute('width',width);
      }
    }
  }catch(err){ 
  }
  
  return isOk; 
}

function CheckBoxHeadNDet(cbHead, cbDet)
{
  var isOk = true;
  var isHeadChk = document.getElementById(cbHead).checked;
  if (isHeadChk)
  {
    var chkBoxList = document.getElementById(cbDet);
    var chkBoxCount= chkBoxList.getElementsByTagName('input');
    
    if (chkBoxCount!=null)
    {
      for(var i=0;i<chkBoxCount.length;i++)
      {
        if (!chkBoxCount[i].checked)
        {
          isOk = false;
          break;
        }
      }
    }
  }
  return isOk; 
}

function CheckBoxListSelect(cbControl, state)
{
  var chkBoxList = document.getElementById(cbControl);
  var chkBoxCount= chkBoxList.getElementsByTagName('input');
  if (chkBoxCount!=null)
  {
     for(var i=0;i<chkBoxCount.length;i++)
     {
        chkBoxCount[i].checked = state;
     }
  }
  return false; 
}

function HasAnyCheckInCheckBoxList(cbControl)
{
  var hasCheck = false;
  
  var chkBoxList = document.getElementById(cbControl);
  var chkBoxCount= chkBoxList.getElementsByTagName('input');
  if (chkBoxCount!=null)
  {
     //alert('HasAnyCheckInCheckBoxList:' + chkBoxCount);
     for(var i=0;i<chkBoxCount.length;i++)
     {
        if (chkBoxCount[i].checked)
        {
          hasCheck = true;
          break;
        }
     }
  }
  return hasCheck;
}

function SetTextBoxByDropDown( ddObj, txtBoxId, stPrefix, stSuffix )
{
  try{
    var selTxt = ddObj[ddObj.selectedIndex].text;
    document.getElementById(txtBoxId).value = stPrefix + selTxt + stSuffix;
  }catch(err){ 
  }
}

function KeepScrollPos( divObj, varName )
{
   try{
      document.getElementById(varName).value = divObj.scrollTop;
   }catch (exception ){}
} 

function FreezeGridViewHeader(gridID, wrapperDivCssClass, scrollPos, scrollVar) 
{
    /// <summary>
    ///   Used to create a fixed GridView header and allow scrolling
    /// </summary>
    /// <param name="gridID" type="String">
    ///   Client-side ID of the GridView control
    /// </param>
    /// <param name="wrapperDivCssClass" type="String">
    ///   CSS class to be applied to the GridView's wrapper div element.  
    ///   Class MUST specify the CSS height and width properties.  
    ///   Example: width:800px;height:400px;border:1px solid black;
    /// </param>
   try{
      var grid = document.getElementById(gridID);
      if (grid != 'undefined')
      {
        grid.style.visibility = 'hidden';
        var div = null;
        if (grid.parentNode != 'undefined') 
        {
             //Find wrapper div output by GridView
           div = grid.parentNode;
           if (div.tagName == "DIV")
           {
              div.className = wrapperDivCssClass;  
              div.style.overflow = "auto"; 
              try{    
                 div.onscroll = function(){return KeepScrollPos(this, scrollVar);}
              }catch (exception ){}    
            }
         }         
         //Find DOM TBODY element and remove first TR tag from 
         //it and add to a THEAD element instead so CSS styles
         //can be applied properly in both IE and FireFox
         var tags = grid.getElementsByTagName('TBODY');
         if (tags != 'undefined')
         {
            var tbody = tags[0];
            var trs = tbody.getElementsByTagName('TR');
            var headerHeight = 8;
            if (trs != 'undefined') 
            {
               headerHeight += trs[0].offsetHeight;
               var headTR = tbody.removeChild(trs[0]);
               var head = document.createElement('THEAD');
               head.appendChild(headTR);
               grid.insertBefore(head, grid.firstChild);
             }
      //Needed for Firefox
             tbody.style.height = (div.offsetHeight -  headerHeight) + 'px';
             tbody.style.overflowX = "hidden";
             tbody.overflow = 'auto';
             tbody.overflowX = 'hidden';
         }
         grid.style.visibility = 'visible';
         div.scrollTop  = scrollPos;
       }
   }catch (exception ){}
}    
 
function ChangeHeightPixel(divId1, orgResolutionHeight, orgHeightPixel, extraHeight )
{
   try{
       var diff = 0;
//       alert('ChangeHeightPixel:0');
       var elemt = document.getElementById( divId1 );
       if (elemt != 'undefined')
       {
          if (screen.height>orgResolutionHeight)
          {
             diff = screen.height - orgResolutionHeight;
          }
          //alert('ChangeHeightPixel:' + diff );
          elemt.style.height = (orgHeightPixel + extraHeight + diff ) + "px";
       }
       else
       {
            alert( divId1 + ':Not found.');
       }
   }catch(exception){ }
}


function collapsiblePanelSmoothAnimation(panelId, fps, dura)
{
   try
   {
       var collPanel = $find(panelId);
       if (collPanel!=null)
       {
         //collPanel._animation._fps = 45;
         //collPanel._animation._duration = 0.90;
         collPanel._animation._fps = fps;
         collPanel._animation._duration = dura;
       }
   }catch(exception){ }
}


//function changeCpeState(cpeId,hfId){
            //alert( 'CpeId:' + cpeId + ',' + hfId ); 
            //try{
              //objExtender = $find(cpeId); 
              //objExtender = $find('cpeBehaviorID'); 
              //if (objExtender==null) alert('null');
//objExtender = $find(cpeId);if (objExtender==null) alert('null');
      // objExtender = document.getElementById(cpeId); if (objExtender==null) alert(cpeId + ':null');
            //}catch( ex) { alert('Err:' + ex.description ); }
//            objHiddenField = document.getElementById(hfId); 
//  if (objHiddenField.value != '1')
 //objHiddenField.value = '1';
//  else objHiddenField.value = '0';
//objHf = document.getElementById(cpeId+'_ClientState');
//alert('hfid:' + objHf.id);
//try{
//alert(objExtender.get_Collapsed());
//             if(objExtender.get_Collapsed()){  
//                 alert('Collapsed');  
//                 objHiddenField.value = '0'; 
//                 objHf.value = 'true'; 
//             }  
//            else{
//alert('Expand');
//objHiddenField.value = '1'; 
//objHf.value = 'false';
//}
//}catch (ex) { alert('Err1:' + ex.description);}
//alert(objHiddenField.value);
//         }

function changeCpeState(cpeId,hfId){
  objHiddenField = document.getElementById(hfId); 
  if (objHiddenField.value != '1')
  objHiddenField.value = '1';
  else objHiddenField.value = '0';
}

function simulateClick(idToClick) {
  var cb = document.getElementById(idToClick); 
  if (document.createEvent)
  {
//      alert('simulateClick');
      var evt = document.createEvent("MouseEvents");
//      alert('simulateClick-m');
      evt.initMouseEvent("click", true, true, window,
        0, 0, 0, 0, 0, false, false, false, false, 0, null);
//        alert('simulateClick-mi');
      cb.dispatchEvent(evt);
  }
  else
  {
     cb.click();
  }
}

function clickElement(elementid){
    try{
        var e = document.getElementById(elementid);
        if (typeof e == 'object') {
            if(typeof e.click != 'undefined') {
                e.click();
    //            alert('click');
                return false;
            }
            else if(document.createEvent) {
                var evObj = document.createEvent('MouseEvents');
                evObj.initEvent('click',true,true);
                e.dispatchEvent(evObj);
    //            alert('createEvent');
                return false;
            }
            else if(document.createEventObject) {
                e.fireEvent('onclick');
    //            alert('createEventObject');
                return false;
            }
            else {
                e.click();
    //            alert('click');
                return false;
            }
        }
    }catch (exception ){ selfRefresh( 5 );}
}

function selfRefreshByClick( refreshTimeInSecond, idToClick )
{
    try{
        var st="";
        //st = "simulateClick('" + idToClick + "');";
        st = "clickElement('" + idToClick + "');";
        setTimeout(st,refreshTimeInSecond * 1000);
    }catch (exception ){}
}