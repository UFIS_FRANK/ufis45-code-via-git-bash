using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.DS.DSFlightStaffTableAdapters;
using UFIS.Web.ITrek.Sec;
using UFIS.ITrekLib.ENT;

public partial class Web_iTrek_Staff : System.Web.UI.Page
{
    private const string PG_NAME = "Staff";
    private const string CTRL_VIEW = "View";

    protected void Page_Load(object sender, EventArgs e)
    {
        CtrlSec ctrlsec = CtrlSec.GetInstance();
        EntUser user = ctrlsec.AuthenticateUser(Session, Response);
        if (ctrlsec.HasAccessRights(user, PG_NAME, CTRL_VIEW))
        {
            string flNo = "", urNo = "", arrDep = "";
            try
            {
                flNo = Request.Params["FLNO"].ToString().Trim();
                urNo = Request.Params["URNO"].ToString().Trim();
                arrDep = Request.Params["AD"].ToString().Trim();
            }
            catch (Exception)
            {
            }
            ShowFlightInfo(urNo, flNo, arrDep);
        }
        else
        {
            DoUnAuthorise();
        }
    }

    private void DoUnAuthorise()
    {
        Response.Redirect("~/UnAuthorised.aspx", true);
    }

    private void ShowFlightInfo(string urNo, string flightNo, string arrDep)
    {
        try
        {
            //wuAFlight1.ShowFlightInfo(urNo, flightNo);
            //wuAFlight1.ShowTextBoxes(false);
            //DSFlightStaff ds = new DSFlightStaff();
            //FLIGHT_STAFFTableAdapter taFlightStaff = new FLIGHT_STAFFTableAdapter();
            //STAFFTableAdapter taStaff = new STAFFTableAdapter();
            //taStaff.Fill(ds.STAFF);
            //taFlightStaff.Fill(ds.FLIGHT_STAFF);
            //GridView1.DataSource = ds;
            //GridView1.DataBind();
            switch (arrDep)
            {
                case "A": //Arrival Flight
                    wuFlightArrApron1.Visible = true;
                    wuFlightArrApron1.PopulateGridForAFlight(urNo, flightNo);
                    break;
                case "D": //Departure Flight
                    wuFlightDepApron1.Visible = true;
                    wuFlightDepApron1.PopulateGridForAFlight(urNo, flightNo);
                    break;
                default:
                    break;
            }
            TextBox1.Text = CtrlStaff.RetrieveStaffsForAFlight(urNo);
            //gvStaff.DataSource = CtrlStaff.RetrieveFlightStaff(urNo, flightNo);
            //gvStaff.DataBind();
        }
        catch (Exception ex)
        {
            MMSoftware.MMWebUtil.WebUtil.AlertMsg( Page, "Error : " + ex.Message);
        }
    }
}
