using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.EventArg;

public partial class Web_Wu_WuTime : System.Web.UI.UserControl
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.Page.IsPostBack)
            SetDateTimeList();
    }

    private int _noOfTiming = 10;
    private string HHMMFormat = "HHmm";

    public int NumberOfTiming
    {
        get { return _noOfTiming; }
        set
        {
            if (value < 1) _noOfTiming = 1;
            else if (value > 100) _noOfTiming = 100;
            else _noOfTiming = value;
        }
    }

    private string CurSelectedTime()
    {
        string st = "";
        try
        {
            st = this.rblTiming.SelectedValue;
        }
        catch (Exception)
        {
        }
        return st;
    }

    public void SetDateTimeList()
    {
        int noOfTiming = NumberOfTiming;
        string curSelTime = CurSelectedTime();
        this.rblTiming.Items.Clear();

        DateTime currentTime = DateTime.Now;
        string time;
        rblTiming.Items.Add("NOW " + currentTime.ToString(HHMMFormat));
        for (int i = 1; i < noOfTiming; i++)
        {
            time = currentTime.AddMinutes(-i).ToString(HHMMFormat) + "   ";

            rblTiming.Items.Add(time);
        }
        if (this.rblTiming.Items.Contains(new ListItem(curSelTime)))
        {
            this.rblTiming.SelectedValue = curSelTime;
        }
    }    

    public string TextBoxLabel
    {
        get { return this.lblTiming.Text; }
        set { this.lblTiming.Text = value; }
    }

    private void SendTiming(string timing)
    {
        txtTiming.Text = timing;
        if (!IsValidTime(timing))
        {
            this.Page.ClientScript.RegisterStartupScript(this.GetType(),"showErr", @"alert('Invalid Timing');", true);
        }
        else
        {
            try
            {
                if (TimingSendClick != null)
                {
                    EaTextEventArgs arg = new EaTextEventArgs(timing);
                    TimingSendClick(this, arg);
                }
            }
            catch (Exception)
            {
            }
        }
    }

    private bool IsValidTime(string timing)
    {
        bool isValid = true;
        string st = timing.PadLeft(4,'0');
        int hh = -1;
        int mm = -1;
        if (!Int32.TryParse(st.Substring(0, 2), out hh)) isValid = false;
        else
        {
            if (!Int32.TryParse(st.Substring(2, 2), out mm)) isValid = false;
        }

        if (isValid)
        {
            if ((hh < 0) || (hh > 23)) isValid = false;
            else if ((mm < 0) || (mm > 59)) isValid = false;
        }

        if (isValid)
        {//Check the input time is between -4hr to now
           DateTime curDt = DateTime.Now;
           DateTime prevDt = curDt.Subtract(new TimeSpan(1,0,0,0));//-1 day
           int curHH = curDt.Hour;
           int curMM = curDt.Minute;
           DateTime dtIn1 = new DateTime(curDt.Year, curDt.Month, curDt.Day, hh, mm, 00);
           DateTime dtIn2 = new DateTime(prevDt.Year, prevDt.Month, prevDt.Day, hh, mm, 00);
           DateTime frDt = curDt.Subtract(new TimeSpan(4, 0, 0)); //-4hr
           if (MM.UtilLib.UtilTime.IsWithinTime(dtIn1, frDt, curDt))
           {
              isValid = true;
           }
           else if (MM.UtilLib.UtilTime.IsWithinTime(dtIn2, frDt, curDt))
           {
              isValid = true;
           }
           else isValid = false;
        }
        return isValid;
    }


    /////////////////
    //// add a delegate

    public delegate void TimingSendHandler(object sender,
        EaTextEventArgs e);


    //// add an event of the delegate type

    public event TimingSendHandler TimingSendClick;


    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        SendTiming(txtTiming.Text.Trim());
    }
    protected void rblTiming_SelectedIndexChanged(object sender, EventArgs e)
    {
        string selectedTime = e.ToString();
        selectedTime = this.rblTiming.SelectedItem.Text;
        string st = this.rblTiming.SelectedValue;
        //string selectedTime = e..Text;
        selectedTime = selectedTime.Replace("NOW ", "").Trim();
        txtTiming.Text = selectedTime;
        //SendTiming(selectedTime);
    }

    protected void tmrRefreshRblTime_Tick(object sender, EventArgs e)
    {
        //string curSelText = this.rblTiming.SelectedValue;
        this.SetDateTimeList();
        //if (this.rblTiming.Items.Contains(new ListItem(curSelText)))
        //{
        //    this.rblTiming.SelectedValue = curSelText;
        //}
    }

}
