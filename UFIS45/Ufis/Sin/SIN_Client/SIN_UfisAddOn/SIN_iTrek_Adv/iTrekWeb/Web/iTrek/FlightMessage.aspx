<%@ Page Language="C#" MasterPageFile="~/Web/iTrek/MasterPage.master" AutoEventWireup="true" CodeFile="FlightMessage.aspx.cs" Inherits="Web_iTrek_FlightMessage" Title="Flight Message" Theme="SATSTheme" %>
<%@ Register Src="../Wu/WuMsg.ascx" TagName="WuMsg" TagPrefix="uc1" %>
<%@ Register Src="WUFlightArrBaggage.ascx" TagName="WUFlightArrBaggage" TagPrefix="uc3" %>
<%@ Register Src="WUFlightDepApron.ascx" TagName="WUFlightDepApron" TagPrefix="uc4" %>
<%@ Register Src="WUFlightDepBaggage.ascx" TagName="WUFlightDepBaggage" TagPrefix="uc5" %>
<%@ Register Src="WUFlightArrApron.ascx" TagName="WUFlightArrApron" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" Runat="Server">
    <table style="width: 100%">
        <tr>
            <td>
                <uc2:WUFlightArrApron ID="wuFlightArrApron1" runat="server" EnableViewState="true" Visible="false" />
                <uc3:WUFlightArrBaggage ID="wuFlightArrBaggage1" runat="server" EnableViewState="true"
                    Visible="false" />
                <uc4:WUFlightDepApron ID="wuFlightDepApron1" runat="server" EnableViewState="true"
                    Visible="false" />
    <uc5:WUFlightDepBaggage ID="wuFlightDepBaggage1" runat="server" EnableViewState="true"  Visible="false" />
            </td>
        </tr>
       <asp:Panel ID="pnlMsgOld" runat="server" Visible="False">    
        <tr>
            <td style="height: 237px">
                <table style="width: 1000px">
                    <tr>
                        <td style="width: 100px; text-align: center">
                            Compose Message</td>
                        <td style="width: 400px" colspan="2">
                            &nbsp;to :
                            <asp:Label ID="lblSendTo" runat="server" Height="20px" Width="350px"></asp:Label>
                            </td>
                        <td >
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px; height: 26px">
                            <asp:TextBox ID="txtMsg" runat="server" Height="61px" Width="347px" TextMode="MultiLine"></asp:TextBox></td>
                        <td style="width: 96px; height: 26px">
                            <asp:Button ID="btnSendMessage" runat="server" Text="Send Message" OnClick="btnSendMessage_Click" EnableViewState="False" /></td>
                        <td style="width: 100px; height: 26px">
                        </td>
                        <td style="width: 100px; height: 26px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 96px">
                        </td>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 100px">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 24px">
                Predefined Messages
                <asp:DropDownList ID="ddPreDefinedMsg" runat="server" Width="245px" DataTextField="MSGTEXT" DataValueField="URNO" AutoPostBack="True" OnSelectedIndexChanged="ddPreDefinedMsg_SelectedIndexChanged">
                </asp:DropDownList></td>
        </tr>
        </asp:Panel>
        <tr>
            <td style="height: 4px">
            </td>
        </tr>
        <tr>
            <td><uc1:WuMsg ID="WuMsg1" runat="server" />               
            </td>
        </tr>
    </table>
</asp:Content>

