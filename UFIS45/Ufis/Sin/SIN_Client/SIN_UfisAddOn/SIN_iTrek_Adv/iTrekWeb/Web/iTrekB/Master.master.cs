using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.Web.ITrek.Sec;
using UFIS.Web.ITrek.WebMgr;
using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.ENT;

using MMSoftware.MMWebUtil;

public partial class Web_iTrekB_Master : System.Web.UI.MasterPage
{
   public delegate void RefreshHandler(object sender);
   public event RefreshHandler OnRefresh;

   private bool _autoRefreshMode = true;

   //public bool AdminMenuVisible
   //{
   //    get { return WUAdminMenu1.Visible; }
   //    set { WUAdminMenu1.Visible = value; }
   //}

   public void RefreshInfo()
   {
      ShowRefreshTime();
      ShowNewMsgCount();
   }

	protected override void OnPreRender(EventArgs e)
	{
		base.OnPreRender(e);
		RefreshInfo();
	}

   public bool AutoRefreshMode
   {
      get
      {
         //LogMsg("AutoRefreshMode:" + _autoRefreshMode);
         return _autoRefreshMode;
      }
      set
      {
         _autoRefreshMode = value;
         //LogMsg("AutoRefreshMode:Set:" + _autoRefreshMode);
      }
   }

   private void SetRefresh()
   {
      if (AutoRefreshMode)
      {
         //WebUtil.SetJavaScriptAtBottom(Page, "selRefreshPostback(" + CtrlConfig.GetWebRefreshTiming() +"," + this.lbRefreshNow.ClientID + ");");
         //WebUtil.SetJavaScriptAtBottom(Page, "selfRefreshPostback(" + CtrlConfig.GetWebRefreshTiming() + ",'" + this.lbRefreshNow.ClientID + "');");
         WebUtil.SetJavaScriptAtBottom(Page, "selfRefresh(" + CtrlConfig.GetWebRefreshTiming() + ");");
      }
   }

   public ScriptManager GetScriptManager()
   {
      return ScriptManager1;
   }

   protected void Page_Load(object sender, EventArgs e)
   {
      ////Response.Expires = 20;
      ////Response.ExpiresAbsolute = System.DateTime.Now.AddMinutes(-1);
      Response.AddHeader("pragma", "no-cache");
      Response.AddHeader("cache-control", "private");
      Response.CacheControl = "no-cache";

      if (Request.IsAuthenticated)
      {
         if (!CtrlSec.GetInstance().IsAuthorisedUser(Session))
         {
            //String st = "";
            //st += ResolveUrl("~/Login2.aspx");
            //st += "\n" + Request.Url.GetLeftPart(UriPartial.Path);
            //WebUtil.AlertMsgAndGotoPage("Please login.", ResolveUrl("~/Login2.aspx"), Page);
            WebUtil.AlertMsgAndGotoPage("Please Login.", MgrNavigation.FullUrlLoginPage(), Page);
         }
         else
         {
            EntUser user = MgrSess.GetUserInfo(Session);
            lblDept.Text = user.Dept;
            string userName = "";
            if (user.LastName == "") userName = user.FirstName;
            else userName = user.LastName;
            userName += " (" + user.UserId + ")";
            lblUser.Text = userName;
            //    ShowMenu(user);
            ShowNewMsgCount();
            SetRefresh();
            ShowRefreshTime();
            ShowFlightLink(user);
         }
      }
      else
      {
         //WebUtil.AlertMsgAndGotoPage("Please login.", ResolveUrl("~/Login2.aspx"), Page);
         WebUtil.AlertMsgAndGotoPage("Please Login.", MgrNavigation.FullUrlLoginPage(), Page);
      }
   }

   private void ShowFlightLink(EntUser user)
   {
      bool hasRight = CtrlSec.GetInstance().HasAccessBAToShowFlightsBtn(user);
      hlShowFlight.Visible = hasRight;
   }

   private void ShowRefreshTime()
   {
      DateTime dtNow = DateTime.Now;
      lblRefreshTime.Text = "Last Refresh " + dtNow.ToLongTimeString() + "  " + string.Format("{0:dd MMM yyyy}", dtNow);
   }

   public void ShowMenu(EntUser user)
   {
      //AdminMenuVisible = CtrlSec.GetInstance().HasAdminRights(user);
   }

   public void ShowNewMsgCount()
   {
      try
      {
         EntUser user = MgrSess.GetUserInfo(Session);
         int newMsgCnt = CtrlMsg.GetUnReadInBoxMsgCount(user);
         string stNewMsg;
         if (newMsgCnt < 1) stNewMsg = "0 NEW";
         else stNewMsg = newMsgCnt + " NEW";
         lblNewMsg.Text = stNewMsg;
         lblNewMsg.CssClass = "NewMsg";
      }
      catch (Exception)
      {
         lblNewMsg.Text = "-";
      }
   }

   protected void btnLogout_Click(object sender, EventArgs e)
   {
      FormsAuthentication.SignOut();
      Response.Cookies.Clear();
      Session.RemoveAll();
      Session.Abandon();

      Response.Redirect("~/logout.aspx");
   }

   protected void lbArrFlight_Click(object sender, EventArgs e)
   {
      Response.Redirect("~/Web/iTrekB/BATerminalFlightUlds.aspx");
   }
   protected void lbDepFlight_Click(object sender, EventArgs e)
   {
      try
      {
         string st2 = Request.Url.AbsoluteUri;
         int idx = st2.IndexOf("/iTrekWeb/");
         string url = st2.Substring(0, idx + 1) + "iTrek";
         AbandonSession();
         Response.Redirect(url);
      }
      catch (Exception ex)
      {
         LogMsg("lbDepFlight_Click:Err:" + ex.Message);
      }
   }

   private void AbandonSession()
   {
      LogMsg("AbandonSession:ToAccess iTrek HandHeld:" + Session.SessionID);
      CtrlSec.GetInstance().AbandonSession(Session, Response);
      MMSoftware.MMWebUtil.WebUtil.SetJavaScriptAtTop(Page, @"history.go(1);
// Browser Back Button
window.history.forward(1); // this works for IE standalone");
   }


   protected void lbRefreshNow_Click(object sender, EventArgs e)
   {
      string st = e.ToString();
      RefreshHandler hdl = OnRefresh;
      if (hdl != null)
      {
         hdl(this);
      }
   }

   //public bool ShowFlightMenuVisible
   //{
   //   get
   //   {
   //      return hlShowFlight.Visible;
   //   }
   //   set
   //   {
   //      if (hlShowFlight.Visible != value)
   //      {
   //         hlShowFlight.Visible = value;
   //      }
   //   }
   //}

   private void LogMsg(string msg)
   {
      try
      {
         MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("ITrekB_Master", msg);
      }
      catch (Exception)
      {
         UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("ITrekB_Master", msg);
      }
   }
}
