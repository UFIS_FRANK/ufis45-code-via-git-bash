using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.ENT;
using UFIS.Web.ITrek.Sec;
using iTrekSessionMgr;
using UFIS.Web.ITrek.WebMgr;
using MMSoftware.MMWebUtil;
using UFIS.ITrekLib.Util;

public partial class Web_Wu_WuMsgCompose : System.Web.UI.UserControl
{
   public delegate void RefreshTimerHandler(object sender);
   public event RefreshTimerHandler OnRefreshTime;

   private const int MAX_MSG_LENGTH = 256;

   private string VS_WUMSGCOMPOSE_TP = "VS_WUMSGCOMPOSE_TP";
   private EnComposeMsgType _composeMsgType = EnComposeMsgType.Undefined;
   private string VS_WUMSGCOMPOSE_FLID = "VS_WUMSGCOMPOSE_FLID";
   private string _flightId = null;

   private string _flightInfo = "";

   private EnComposeMsgType MsgType
   {
      get
      {
         if (_composeMsgType == EnComposeMsgType.Undefined)
         {
            try
            {
               _composeMsgType = (EnComposeMsgType)ViewState[VS_WUMSGCOMPOSE_TP];
            }
            catch (Exception)
            {
            }
         }
         return _composeMsgType;
      }
      set
      {
         _composeMsgType = value;
         ViewState[VS_WUMSGCOMPOSE_TP] = _composeMsgType;
      }
   }

   private string FlightId
   {
      get
      {
         if (_flightId == null)
         {
            try
            {
               _flightId = (string)ViewState[VS_WUMSGCOMPOSE_FLID];
            }
            catch (Exception)
            {
            }
         }
         return _flightId;
      }
      set
      {
         _flightId = value;
         ViewState[VS_WUMSGCOMPOSE_FLID] = _flightId;
      }
   }

   ScriptManager SM;

   const string VS_MSG_COMPOSE_ADDH = "VS_MSG_COMPOSE_ADDH";
   private int _addHeight = 0;
   public int AdditionalHeight
   {
      get
      {
         if (_addHeight <= 0)
         {
            try
            {
               _addHeight = (int)ViewState[VS_MSG_COMPOSE_ADDH];
            }
            catch (Exception)
            {
               ViewState[VS_MSG_COMPOSE_ADDH] = 0;
            }
         }
         return _addHeight;
      }
      set
      {
         _addHeight = value;
         ViewState[VS_MSG_COMPOSE_ADDH] = _addHeight;
		 //LogMsg("AdditionalHeight:" + _addHeight);
      }
   }

   protected override void OnInit(EventArgs e)
   {
      base.OnInit(e);
      SM = ScriptManager.GetCurrent(Page);
      SM.RegisterAsyncPostBackControl(ddPredefMsgs);
      SM.RegisterAsyncPostBackControl(cblSendTo);
      SM.RegisterAsyncPostBackControl(txtComposeMessage);
      tmr1.Interval = CtrlConfig.GetWebRefreshTiming() * 1000;
   }

   private void SetSendToWidth()
   {
      int diff = 0;
      int bh = MgrSess.GetBrowserheight(Session);
      if (bh > 768) diff = bh - 768;
	  //LogMsg(string.Format("SetSendToWidth:bw<{0}>,diff<{1}>,add<{2}>", bh, diff, AdditionalHeight));
      pnlSendTo.Height = new Unit(string.Format("{0}px", (200 + AdditionalHeight + diff)));
   }

   protected override void OnLoad(EventArgs e)
   {
      base.OnLoad(e);
      //SM.RegisterAsyncPostBackControl(this.tmr1);
      string curClientId = this.ClientID;
      //if (!Page.ClientScript.IsClientScriptBlockRegistered("DivSendTo"))
      //{
      //   Page.ClientScript.RegisterStartupScript( 
      //      this.GetType(), 
      //      "DivSendTo", 
      //      string.Format("ChangeHeightPixel('{0}','divSendTo','1024','200', '{1}');", updPnlSendTo.ClientID, AdditionalHeight  ), 
      //      true 
      //      );
      //}
   }

   protected override void OnPreRender(EventArgs e)
   {
      base.OnPreRender(e);
      SetPredefMsgChangeClientEvent();
      SetSendToWidth();
   }

   private void SetPredefMsgChangeClientEvent()
   {
      try
      {
         ddPredefMsgs.Attributes.Remove("OnChange");
      }
      catch (Exception)
      {
      }

      string stPrefix = "";
      _flightInfo = CtrlFlight.GetFlightSummInfo(FlightId);
      if (!string.IsNullOrEmpty(_flightInfo))
      {
         if (_flightInfo.Trim() != "") stPrefix = _flightInfo + " : ";
      }

      ddPredefMsgs.Attributes.Add("OnChange", string.Format("SetTextBoxByDropDown(this,'{0}','{1}',''); return false;",
         txtComposeMessage.ClientID, stPrefix));
   }

   public void ComposeMessageForFlight(string flightId)
   {
      //LogMsg("ComposeMessageForFlight:" + flightId);
      MsgType = EnComposeMsgType.Flight;
      FlightId = flightId;
      ShowSendToWithExistingSelectItem();
      ShowPredefMsgs();
   }

   public void ComposeMessageNormal()
   {
      //LogMsg("ComposeMessageNormal");
      MsgType = EnComposeMsgType.Personal;
      FlightId = "";
      ShowSendToWithExistingSelectItem();
      ShowPredefMsgs();
   }


   protected void btnSendMessage_Click(object sender, EventArgs e)
   {
      ArrayList recList = new ArrayList();
      int cnt = cblSendTo.Items.Count;
      for (int i = 0; i < cnt; i++)
      {
         if (cblSendTo.Items[i].Selected)
         {
            recList.Add(cblSendTo.Items[i].Value);
         }
      }
      if (recList.Count < 1)
         WebUtil.AlertMsg(Page, "Please choose receipient(s).");
      else
         SendMessage(recList);
   }

   private void SendMessage(ArrayList recList)
   {
      string userId = "";
      string userName = "";
      string msgText = "";
      EntUser user = MgrSess.GetUserInfo(Session);
      userId = user.UserId;
      //userName = user.FirstName + " " + user.LastName;
      userName = user.FirstName + "";
      userName = userName.Trim();
      if (userName == "")
      {
         userName = user.LastName;
      }

      if (txtComposeMessage.Text != "")
      {
         msgText += txtComposeMessage.Text;
      }

      msgText = msgText.Trim();
      if (msgText.Length < 1)
      {
         WebUtil.AlertMsg(Page, "No message text to send.");
      }
      else if (msgText.Length > 256)
      {
         msgText = msgText.Substring(0, MAX_MSG_LENGTH);
         WebUtil.AlertMsg(Page, "Message Length exceeds the maximum length " + MAX_MSG_LENGTH +
             ". ");
      }
      else
      {
         if (CtrlMsg.SendMessage(recList, userId, userName, msgText))
         {
            ClearSelection();
            System.Threading.Thread.Sleep(1500);
         }
      }
   }

   protected void btnSendToAll_Click(object sender, EventArgs e)
   {
      ArrayList recList = new ArrayList();
      int cnt = cblSendTo.Items.Count;
      for (int i = 0; i < cnt; i++)
      {
         recList.Add(cblSendTo.Items[i].Value);
      }
      SendMessage(recList);
   }

   private void ShowSendToWithExistingSelectItem()
   {//Show new 'Send to' list
      ArrayList recList = new ArrayList();//keep the current selected items
      int cnt = cblSendTo.Items.Count;
      for (int i = 0; i < cnt; i++)
      {
         if (cblSendTo.Items[i].Selected)
         {
            recList.Add(cblSendTo.Items[i].Value);
            //LogMsg("Selected:" + cblSendTo.Items[i].Value);
         }
      }
      DSCurUser ds = null;
      switch (MsgType)
      {
         case EnComposeMsgType.Personal:
            //Get new 'send to' list
            ds = CtrlApp.GetUsersToSendMsgInWeb();
            break;
         case EnComposeMsgType.Flight:
            CtrlApp.AddAoFlightReceipients(ref ds, FlightId, ref _flightInfo);
            CtrlApp.AddGroupReceipients(ref ds);
            break;
         case EnComposeMsgType.Undefined:
            break;
         default:
            break;
      }


      DataView dv = ds.LOGINUSER.DefaultView;
      dv.Sort = "LGTYPE DESC, NAME";
      cblSendTo.DataSource = dv;
      cblSendTo.DataTextField = "NAME";
      cblSendTo.DataValueField = "UID";
      cblSendTo.DataBind();

      cnt = cblSendTo.Items.Count;
      for (int i = 0; i < cnt; i++)
      {//select the previous selected items in new list
         if (recList.Contains(cblSendTo.Items[i].Value))
         {
            cblSendTo.Items[i].Selected = true;
         }
      }
   }

   public void RefreshInfo()
   {
      //LogMsg("RefreshInfo." + txtComposeMessage.Text);
      ShowSendToWithExistingSelectItem();
      ShowPredefMsgs();
      //ShowInBox();
      //ShowOutBox();
      //((MyMasterPage)Master).RefreshInfo();
   }

   protected void Timer1_Tick(object sender, EventArgs e)
   {
      try
      {
         //LogMsg("Timer1Tick");
         RefreshInfo();
         if (OnRefreshTime != null)
         {
            OnRefreshTime(this);
         }
         //LogMsg("Timer1Tick-Finish.");
      }
      catch (ApplicationException ex)
      {
         LogMsg("Timer1_Tick:Err:" + ex.Message);
         throw new ApplicationException("Auto Refresh Error due to : " + ex.Message + ".");
      }
      catch (Exception ex)
      {
         LogMsg("Timer1_Tick:Err:" + ex.Message);
         throw new ApplicationException("Auto Refresh Error due to : " + ex.Message);
      }
   }

   protected void ddPredefMsgs_SelectedIndexChanged(object sender, EventArgs e)
   {
      return;
      //Text box will be updated from client side code.
      //Will not use server side code.

      //try
      //{
      //   //DropDownList dd = (DropDownList)sender;
      //   if (ddPredefMsgs.SelectedIndex >= 0)
      //   {
      //      //LogMsg("PredefMsg_Idx_changed.");
      //      if (MsgType == EnComposeMsgType.Flight)
      //      {
      //         txtComposeMessage.Text = CtrlFlight.GetFlightSummInfo(FlightId) + " : " + ddPredefMsgs.SelectedItem.Text;
      //      }
      //      else
      //      {
      //         txtComposeMessage.Text = ddPredefMsgs.SelectedItem.Text;
      //      }
      //   }
      //}
      //catch (Exception ex)
      //{
      //   LogMsg("PredefMsg_Sel_Changed:Err:" + ex.Message);
      //}
   }

   private void ShowPredefMsgs()
   {
      try
      {
         //LogMsg("ShowPredefMsgs.[" + txtComposeMessage.Text + "]");
         DSPreDefMsg dsPreDefMsg;
         dsPreDefMsg = CtrlPreDefMsg.RetrieveActivePredifinedMsg();
         DSPreDefMsg.PREDEFMSGRow row = dsPreDefMsg.PREDEFMSG.NewPREDEFMSGRow();
         row.URNO = "NONE";

         dsPreDefMsg.PREDEFMSG.AddPREDEFMSGRow(row);
         DataView dv = dsPreDefMsg.PREDEFMSG.DefaultView;
         dv.Sort = "MSGTEXT";
         ddPredefMsgs.DataSource = dv;
         ddPredefMsgs.DataBind();
      }
      catch (Exception ex)
      {
         LogMsg("ShowPredefMsgs:Err:" + ex.Message);
      }
   }

   private void LogMsg(string msg)
   {
      //UtilLog.LogToTraceFile("WuMsgOutBox", msg);
      try
      {
         MM.UtilLib.UtilTraceLog.GetInstance().LogTraceMsg("WuMsgCompose", msg);
      }
      catch (Exception)
      {
         UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("WuMsgCompose", msg);
      }
   }

   private void ClearSelection()
   {
      int cnt = cblSendTo.Items.Count;
      for (int i = 0; i < cnt; i++)
      {//unselect list
         cblSendTo.Items[i].Selected = false;
      }
   }
}