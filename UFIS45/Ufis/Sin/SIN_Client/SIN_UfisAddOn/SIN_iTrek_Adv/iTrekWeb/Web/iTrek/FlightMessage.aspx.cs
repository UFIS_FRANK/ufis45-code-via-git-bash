using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.ENT;
using UFIS.Web.ITrek.Sec;
using UFIS.Web.ITrek.WebMgr;
using MMSoftware.MMWebUtil;
using UFIS.ITrekLib.Util;
using UFIS.Web.ITrek.Base;

public partial class Web_iTrek_FlightMessage : BaseWebPage 
{
    private const string PG_NAME = "FlightMessage";
    private const string CTRL_VIEW = "Page";
    private const string VS_AO_ID = "VS_AO_ID";
    private const string VS_FL_ID = "VS_FL_ID";
    private const string VS_FL_NUM = "VS_FL_NUM";
    private const int MAX_MSG_LEN = 256;


    private string FlightId
    {
        get
        {
            string flId = "";
            try
            {
                flId = (string) ViewState[VS_FL_ID];
            }
            catch (Exception)
            {
            }
            return flId;
        }
        set
        {
            ViewState[VS_FL_ID] = value;
        }
    }

    private string FlightNumber
    {
        get
        {
            string flNum = "";
            try
            {
                flNum = (string)ViewState[VS_FL_NUM];
            }
            catch (Exception)
            {
            }
            return flNum;
        }
        set
        {
            ViewState[VS_FL_NUM] = value;
        }
    }
    private string AOId
    {
        get
        {
            string aoId = "";
            try
            {
                aoId = (string) ViewState[VS_AO_ID];
            }
            catch (Exception)
            {
            }
            return aoId;
        }
        set
        {
            ViewState[VS_AO_ID] = value;
        }
    }


   protected override void OnPreLoad(EventArgs e)
   {
      base.OnPreLoad(e);
      ((MyMasterPage)Master).AutoRefreshMode = false;
   }

   protected override void OnLoad(EventArgs e)
   {
      base.OnLoad(e);
      if (CtrlSec.GetInstance().HasAccessToFlightMessage(LoginedUser))
      {
         if ((!Page.IsPostBack) && (!Page.IsCallback))
         {
            ShowInfo();
         }
      }
      else
      {
         DoUnAuthorise();
      }
   }

   private void ShowFlightMessageOld()
   {
      ShowPredefMsgs();
      try
      {
         DSFlight dsFlight = CtrlFlight.RetrieveFlightsByFlightId(FlightId);
         if ((dsFlight != null) && (dsFlight.FLIGHT.Rows.Count > 0))
         {
            DSFlight.FLIGHTRow row = (DSFlight.FLIGHTRow)dsFlight.FLIGHT.Rows[0];
            //lblSendTo.Text = (String).FLIGHT.Rows[0]["AO"];
            string aoFirstName = "";
            string aoLastName = "";
            string aoId = "";
            //string aoId = CtrlStaff.GetAOId(flightUrNo, out aoLastName, out aoFirstName);

            aoId = row.AOID;
            aoFirstName = row.AOFNAME;
            aoLastName = row.AOLNAME;

            lblSendTo.Text = aoId + " : " + aoFirstName + ", " + aoLastName;
            AOId = aoId;
            if ((aoId == null) || (aoId == "")) WebUtil.AlertMsg(Page, "No AO for this flight.");
         }
      }
      catch (Exception)
      {
      }
   }

   private void ShowFlightMessageNew()
   {
      ShowFlightMessage(FlightId);
   }

    private void ShowInfo()
    {

        string flightUrNo = "";
        string flightNo = "";

        try
        {
            flightUrNo = Request.Params["URNO"].Trim();
            flightNo = Request.Params["FLNO"].Trim();
            FlightNumber = flightNo;
            FlightId = flightUrNo;
            if (txtMsg.Text.Trim() == "") txtMsg.Text = FlightNumber + ": Key In your message here";
            string arrDep = Request.Params["AD"].Trim().ToUpper();
            string aprBag = Request.Params["AB"].Trim().ToUpper();

            ShowGrid(flightUrNo, flightNo, arrDep, aprBag);
            ShowFlightMessageNew();
        }
        catch (Exception)
        {
        }
    }

    private void ShowFlightMessage(string flightId)
    {
        WuMsg1.PopulateData(EnComposeMsgType.Flight, flightId);
    }

    private void ShowPredefMsgs()
    {
        try
        {
            DSPreDefMsg dsPreDefMsg;
            dsPreDefMsg = CtrlPreDefMsg.RetrieveActivePredifinedMsg();
            DSPreDefMsg.PREDEFMSGRow row = dsPreDefMsg.PREDEFMSG.NewPREDEFMSGRow();
            row.URNO = "NONE";

            dsPreDefMsg.PREDEFMSG.AddPREDEFMSGRow(row);
            DataView dv = dsPreDefMsg.PREDEFMSG.DefaultView;
            dv.Sort = "MSGTEXT";
            ddPreDefinedMsg.DataSource = dv;
            ddPreDefinedMsg.DataBind();
        }
        catch (Exception)
        {
        }
    }

    //private void DoUnAuthorise()
    //{
    //    Response.Redirect("~/UnAuthorised.aspx", true);
    //}

    private void ShowGrid(string flightUrNo, string flightNo, string arrDep, string aprBag)
    {
        if (aprBag == "A")
        {//Apron
            if (arrDep == "A")
            {//Arrival
                wuFlightArrApron1.PopulateGridForAFlight(flightUrNo, flightNo, false);
                wuFlightArrApron1.Visible = true;
            }
            else if (arrDep == "D")
            {
                wuFlightDepApron1.PopulateGridForAFlight(flightUrNo, flightNo, false);
                wuFlightDepApron1.Visible = true;
            }
        }
        else if (aprBag == "B")
        {//Baggage
            if (arrDep == "A")
            {//Arrival
                wuFlightArrBaggage1.PopulateGridForAFlight(flightUrNo, flightNo, false);
                wuFlightArrBaggage1.Visible = true;
            }
            else if (arrDep == "D")
            {
                wuFlightDepBaggage1.PopulateGridForAFlight(flightUrNo, flightNo, false);
                wuFlightDepBaggage1.Visible = true;
            }
        }
    }
    protected void btnSendMessage_Click(object sender, EventArgs e)
    {
        if ((AOId == null) || (AOId == ""))
        {
            WebUtil.AlertMsg(Page, "Unable to send. No Apron Officer for this flight.");
        }
        else
        {
            try
            {
                string userId = "";
                string userName = "";
                string msgText = "";
                EntUser user = MgrSess.GetUserInfo(Session);
                userId = user.UserId;
                //userName = user.FirstName + " " + user.LastName;
                userName = user.FirstName + "";
                userName = userName.Trim();
                if (userName == "")
                {
                    userName = user.LastName + "";
                }

                msgText = txtMsg.Text.Trim();
                //if (ddPreDefinedMsg.SelectedItem.Text != "")
                //{
                //    string dot = "";
                //    if ((msgText.Length > 0) && (msgText[msgText.Length - 1] != '.')) { dot = ". "; }
                //    msgText += dot + ddPreDefinedMsg.SelectedItem.Text;
                //}
                //msgText = msgText.Trim();
                if (msgText.Length < 1)
                {
                    WebUtil.AlertMsg(Page, "No message text to send.");
                }
                else if (msgText.Length > MAX_MSG_LEN)
                {
                    WebUtil.AlertMsg(Page, "Message Length exceeds maximum lenght " + MAX_MSG_LEN);
                }
                else
                {
                    ArrayList ar = new ArrayList();
                    ar.Add(AOId);
                    bool success = CtrlMsg.SendMessage(ar, userId, userName, msgText);
                }
            }
            catch (Exception ex)
            {
                LogMsg("SendMessage:Error:" + ex.Message);
            }
        }
    }
    protected void ddPreDefinedMsg_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtMsg.Text = FlightNumber + ", " + ddPreDefinedMsg.SelectedItem.Text;
    }

    private void LogMsg(string msg)
    {
        UtilLog.LogToGenericEventLog("FlightMessage.aspx", msg);
    }
}
