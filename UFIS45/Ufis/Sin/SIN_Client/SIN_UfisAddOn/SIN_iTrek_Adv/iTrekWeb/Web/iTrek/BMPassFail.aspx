<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BMPassFail.aspx.cs" Inherits="Web_iTrek_BMPassFail" Theme="SATSTheme" %>

<%@ Register Src="WUFlightArrApron.ascx" TagName="WUFlightArrApron" TagPrefix="uc1" %>
<%@ Register Src="WUFlightArrBaggage.ascx" TagName="WUFlightArrBaggage" TagPrefix="uc2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Benchmark</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
            <asp:Label ID="lblMsg" runat="server" Width="679px" Visible="False"></asp:Label>
        <asp:Panel ID="pnlArrApron" runat="server" Height="82px" Width="942px">
            <br />
            <table style="width: 888px">
                <tr>
                    <td style="width: 100px">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblArrAprHdr" runat="server" CssClass="Heading" Visible="False" Width="100%"></asp:Label></td>
                </tr>
                <tr>
                    <td style="width: 100px">
                        <table style="width: 863px">
                            <tr>
                                <td colspan="2" style=" height: 20px">
                                    <strong><span style="text-decoration: underline">First Bag Timing</span></strong></td>
                                <td style="width: 100px; height: 20px">
                                </td>
                                <td colspan="2" style=" height: 20px">
                                    <strong><span style="text-decoration: underline">Last Bag Timing</span></strong></td>
                                <td style="width: 100px; height: 20px">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 189px; height: 21px;">
                                    Tbs Up</td>
                                <td style="width: 150px; height: 21px;">
                                    <asp:Label ID="lblArrAprRefTime" runat="server"></asp:Label></td>
                                <td style="width: 20px; height: 21px;">
                                </td>
                                <td style="width: 194px; height: 21px;">
                                    Tbs Up</td>
                                <td style="width: 150px; height: 21px;">
                                    <asp:Label ID="lblArrAprRefTime2" runat="server"></asp:Label></td>
                                <td style="height: 21px" >
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 189px">
                                    First Bag</td>
                                <td style="width: 150px">
                                    <asp:Label ID="lblArrApFirstBag" runat="server"></asp:Label></td>
                                <td style="width: 20px">
                                </td>
                                <td style="width: 194px">
                                    Last Bag</td>
                                <td style="width: 150px">
                                    <asp:Label ID="lblArrApLastBag" runat="server"></asp:Label></td>
                                <td >
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 189px">
                                    First Bag Time</td>
                                <td style="width: 150px">
                                    <asp:Label ID="lblArrApFirstBagTime" runat="server"></asp:Label></td>
                                <td style="width: 20px">
                                </td>
                                <td style="width: 194px">
                                    Last Bag Time</td>
                                <td style="width: 150px">
                                    <asp:Label ID="lblArrApLastBagTime" runat="server"></asp:Label></td>
                                <td >
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 189px">
                                    Benchmark Time</td>
                                <td style="width: 100px">
                                    <asp:Label ID="lblArrApFirstBagBenchMark" runat="server"></asp:Label></td>
                                <td style="width: 100px">
                                </td>
                                <td style="width: 194px">
                                    Benchmark Time</td>
                                <td style="width: 100px">
                                    <asp:Label ID="lblArrApLastBagBenchMark" runat="server"></asp:Label></td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 189px">
                                    Pass / Fail</td>
                                <td style="width: 100px">
                                    <asp:Label ID="lblArrApFirstBagPassFail" runat="server"></asp:Label></td>
                                <td style="width: 100px">
                                </td>
                                <td style="width: 194px">
                                    Pass / Fail</td>
                                <td style="width: 100px">
                                    <asp:Label ID="lblArrApLastBagPassFail" runat="server"></asp:Label></td>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br />
            <br />
        </asp:Panel>
    
    </div>
        <asp:Panel ID="pnlArrBaggage" runat="server" Height="74px" Width="940px">
            <br />
            <table style="width: 896px">
                <tr>
                    <td >
                        &nbsp;</td>
                </tr>
                <tr>
                    <td >
                        <asp:Label ID="lblArrBagHdr" runat="server" CssClass="Heading" Visible="False" Width="100%"></asp:Label></td>
                </tr>
                <tr>
                    <td >
                        <table style="width: 863px">
                            <tr>
                                <td style="width: 202px; height: 20px">
                                    <strong><span style="text-decoration: underline">First Bag Timing</span></strong></td>
                                <td style="width: 100px; height: 20px">
                                </td>
                                <td style="width: 36px; height: 20px">
                                </td>
                                <td style="width: 182px; height: 20px">
                                    <strong><span style="text-decoration: underline">Last Bag Timing</span></strong></td>
                                <td style="width: 181px; height: 20px">
                                </td>
                                <td style="width: 100px; height: 20px">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 202px; height: 6px;">
                                    Tbs Up</td>
                                <td style="width: 100px; height: 6px;">
                                    <asp:Label ID="lblArrBagRefTime" runat="server"></asp:Label></td>
                                <td style="width: 36px; height: 6px;">
                                </td>
                                <td style="width: 182px; height: 6px;">
                                    Tbs Up</td>
                                <td style="width: 181px; height: 6px;">
                                    <asp:Label ID="lblArrBagRefTime2" runat="server"></asp:Label></td>
                                <td style="width: 100px; height: 6px;">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 202px">
                                    First Bag</td>
                                <td style="width: 100px">
                                    <asp:Label ID="lblArrBgFirstBag" runat="server"></asp:Label></td>
                                <td style="width: 36px">
                                </td>
                                <td style="width: 182px">
                                    Last Bag</td>
                                <td style="width: 181px">
                                    <asp:Label ID="lblArrBgLastBag" runat="server"></asp:Label></td>
                                <td style="width: 100px">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 202px">
                                    First Bag Time</td>
                                <td style="width: 100px">
                                    <asp:Label ID="lblArrBgFirstBagTime" runat="server"></asp:Label></td>
                                <td style="width: 36px">
                                </td>
                                <td style="width: 182px">
                                    Last Bag Time</td>
                                <td style="width: 181px">
                                    <asp:Label ID="lblArrBgLastBagTime" runat="server"></asp:Label></td>
                                <td style="width: 100px">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 202px">
                                    Benchmark Time</td>
                                <td style="width: 100px">
                                    <asp:Label ID="lblArrBgFirstBagBM" runat="server"></asp:Label></td>
                                <td style="width: 36px">
                                </td>
                                <td style="width: 182px">
                                    Benchmark Time</td>
                                <td style="width: 181px">
                                    <asp:Label ID="lblArrBgLastBagBM" runat="server"></asp:Label></td>
                                <td style="width: 100px">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 202px">
                                    Pass / Fail</td>
                                <td style="width: 100px">
                                    <asp:Label ID="lblArrBgFirstBagPassFail" runat="server"></asp:Label></td>
                                <td style="width: 36px">
                                </td>
                                <td style="width: 182px">
                                    Pass / Fail</td>
                                <td style="width: 181px">
                                    <asp:Label ID="lblArrBgLastBagPassFail" runat="server"></asp:Label></td>
                                <td style="width: 100px">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br />
            <asp:Button ID="btnClose" runat="server" Text="Close" EnableViewState="False" /><br />
        </asp:Panel>
        <asp:Panel ID="pnlBASingle" runat="server" Visible="False">
            <br />
            <table style="width: 896px">
                <tr>
                    <td >
                        &nbsp;</td>
                </tr>
                <tr>
                    <td >
                        <asp:Label ID="lblHdr" runat="server" CssClass="Heading" Visible="False" Width="100%"></asp:Label></td>
                </tr>
                <tr>
                    <td >
                        <table style="width: 863px">
                            <tr>
                                <td style="width: 202px; height: 20px">
                                    <strong><span style="text-decoration: underline"><asp:Label ID="lblSubHdr"
                                        runat="server" Font-Bold="True" Font-Underline="True"></asp:Label></span></strong></td>
                                <td style="width: 100px; height: 20px">
                                </td>
                                <td style="width: 36px; height: 20px">
                                </td>
                                <td style="width: 100px; height: 20px">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 202px; height: 6px;">
                                    Tbs Up</td>
                                <td style="width: 100px; height: 6px;">
                                    <asp:Label ID="lblRefTimeData" runat="server"></asp:Label></td>
                                <td style="width: 36px; height: 6px;">
                                </td>
                                <td style="width: 100px; height: 6px;">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 202px">
                                    <asp:Label ID="lblActTimeLbl" runat="server" Text="First Bag"></asp:Label></td>
                                <td style="width: 100px">
                                    <asp:Label ID="lblActTimeData" runat="server"></asp:Label></td>
                                <td style="width: 36px">
                                </td>
                                <td style="width: 100px">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 202px">
                                    <asp:Label ID="lblTimingLbl" runat="server" Text="First Bag Time"></asp:Label></td>
                                <td style="width: 100px">
                                    <asp:Label ID="lblTimingData" runat="server"></asp:Label></td>
                                <td style="width: 36px">
                                </td>
                                <td style="width: 100px">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 202px">
                                    Benchmark Time</td>
                                <td style="width: 100px">
                                    <asp:Label ID="lblBMTimingData" runat="server"></asp:Label></td>
                                <td style="width: 36px">
                                </td>
                                <td style="width: 100px">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 202px">
                                    Pass / Fail</td>
                                <td style="width: 100px">
                                    <asp:Label ID="lblPF" runat="server"></asp:Label></td>
                                <td style="width: 36px">
                                </td>
                                <td style="width: 100px">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br />
            <br />
        </asp:Panel>
    </form>
</body>
</html>
