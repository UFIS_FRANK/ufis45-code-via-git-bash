<%@ Page Language="C#" MasterPageFile="~/Web/iTrek/MasterPage.master" AutoEventWireup="true" CodeFile="HstRpt.aspx.cs" Inherits="Web_iTrek_HstRpt" Title="Retrieve Historical Report" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cp1" Runat="Server">
    <table style="width: 980px">
        <tr>
            <td class="cssHeading" style="width: 100px">
                Retrieve Historical Report</td>
        </tr>
        <tr>
            <td style="width: 100px">
                <table style="width: 556px">
                    <tr>
                        <td style="width: 100px; height: 21px">
                            Flight #</td>
                        <td style="width: 100px; height: 21px">
                            <asp:TextBox ID="txtFlightNo" runat="server"></asp:TextBox></td>
                        <td style="width: 100px; height: 21px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px; height: 26px">
                            Flight Date</td>
                        <td style="width: 100px; height: 26px">
                            <asp:TextBox ID="txtFlightDate" runat="server"></asp:TextBox></td>
                        <td style="width: 100px; height: 26px">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            Report Type</td>
                        <td style="width: 100px">
                            <asp:DropDownList ID="ddRptType" runat="server" Width="246px">
                                <asp:ListItem Selected="True"></asp:ListItem>
                                <asp:ListItem Value="ASR">Apron Service Report</asp:ListItem>
                                <asp:ListItem Value="BPTR">Baggage Presentation Timing Report</asp:ListItem>
                            </asp:DropDownList></td>
                        <td style="width: 100px">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="width: 100px">
                <table style="width: 506px">
                    <tr>
                        <td style="width: 116px">
                        </td>
                        <td style="width: 100px">
                            <asp:Button ID="btnGenerate" runat="server" Text="Generate Report" EnableViewState="False" /></td>
                        <td style="width: 100px">
                        </td>
                        <td style="width: 100px">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

