using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Logout : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("Web-Logout:", Session.SessionID);
                FormsAuthentication.SignOut();
                lblMsg.Text = String.Format("You successfully logged out at {0:dd MMM yyyy   HH:mm}", System.DateTime.Now);
                Response.Cookies.Clear();
                Session.Clear();
                Session.RemoveAll();

                Session.Abandon();
                MMSoftware.MMWebUtil.WebUtil.SetJavaScriptAtTop(Page, @"history.go(1);
// Browser Back Button
window.history.forward(1); // this works for IE standalone");
            }
            catch (Exception)
            {
            }
        }
        else
        {
            //Response.Redirect("Login.aspx");
           MgrNavigation.ShowLoginPage(Request, Response);
        }
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        //Response.Redirect("Login.aspx");
       MgrNavigation.ShowLoginPage(Request, Response);
    }
}
