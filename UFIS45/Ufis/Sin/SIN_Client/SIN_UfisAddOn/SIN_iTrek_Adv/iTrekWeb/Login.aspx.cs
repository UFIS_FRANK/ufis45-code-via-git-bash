using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Login : System.Web.UI.Page
{
   protected override void OnInit(EventArgs e)
   {
      base.OnInit(e);
      //Response.Redirect(MyHttpServerRootPath + "/Login2.aspx?OnInit");
      MgrNavigation.ShowLoginPage(Request, Response);
   }

   protected void Page_Load(object sender, EventArgs e)
   {

      //Response.Redirect(MyHttpServerRootPath + "/Login2.aspx?OnLoad");
      MgrNavigation.ShowLoginPage(Request, Response);
   }

   private string MyHttpServerRootPath
   {
      get
      {
         return @"https://" + Request.ServerVariables["SERVER_NAME"] + Request.ApplicationPath;
      }
   }
}
