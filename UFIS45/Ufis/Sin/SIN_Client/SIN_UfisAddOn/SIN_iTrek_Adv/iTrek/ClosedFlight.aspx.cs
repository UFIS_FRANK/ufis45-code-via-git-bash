using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using iTrekSessionMgr;
using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.EventArg;
using UFIS.Wap.ITrek.WebMgr;

using MM.UtilLib;
using iTrekXML;

public partial class WAP_ClosedFlight : System.Web.UI.MobileControls.MobilePage
{
    private string VS_CUR_FL_ID = "VS_CUR_FL_ID";
    
    private string GetCurFlightId()
    {
        string flId = "";
        try
        {
            flId = (string)ViewState[VS_CUR_FL_ID];
        }
        catch (Exception)
        {
        }
        return flId;
    }

    private void SetCurFlightId(string flightId)
    {
        ViewState[VS_CUR_FL_ID] = flightId;
    }

   

    protected void Page_Load(object sender, EventArgs e)
    {
        string uId = "";
        MgrSec.IsAuthenticatedUser(this, Session, out uId);

        string urno = Request.QueryString["urno"];
        if (!string.IsNullOrEmpty(urno))
        {
            if (IsPostBack)
            {
                ShowClosedFlights();                
            }
            else
            {
                SetCurFlightId(urno);
                ShowFlightDetail();
            }
            return;
        }

        if (!IsPostBack)
        {
            ShowClosedFlights();
        }
    }

    private void ShowClosedFlights()
    {
        DSFlight ds = null;
		
        try
        {
            ds=CtrlFlight.RetrieveClosedFlightsForAO(MgrSess.GetUser(Session),3);
        }
        catch (Exception)
        {
        }
        if ((ds == null) || (ds.FLIGHT.Rows.Count < 1))
        {
            lblClosedFlightErrMsg.Text = "No closed flights.";
        }
        else
        {
			DSFlight dsClosedFlights = new DSFlight();
			foreach (DSFlight.FLIGHTRow row in ds.FLIGHT.Rows)
			{
                //Checking has done in dbdflight.
                dsClosedFlights.FLIGHT.LoadDataRow(row.ItemArray, true);
                //if (UtilDataSet.IsNullOrEmptyValue(row, "TURN"))
                //{
                //    dsClosedFlights.FLIGHT.LoadDataRow(row.ItemArray, true);
                //}
                //else if (row.A_D=="D")
                //{
                //    dsClosedFlights.FLIGHT.LoadDataRow(row.ItemArray, true);
                //}              
			}

			if (dsClosedFlights.FLIGHT.Rows.Count < 1)
			{
				lblClosedFlightErrMsg.Text = "No closed flights.";
			}
			else
			{
				lstClosedFlights.DataSource = dsClosedFlights.FLIGHT;
				lstClosedFlights.DataBind();
			}
        }
        ActiveForm = frmClosedFlight;
    }

    protected void lstClosedFlights_ItemCommand(object sender, ListCommandEventArgs e)
    {
        string urno = e.ListItem.Value;
        SetCurFlightId(urno);
        //Set the flight info in Session
        MgrSess.SetFlight(Session, CtrlFlight.GetFlightFromFlightDataset(urno));
        ShowFlightDetail();
        
    }

    private void ShowFlightDetail()
    {
        string urno = GetCurFlightId();
        DSFlight ds = CtrlFlight.RetrieveFlightsByFlightId(urno);
        DSFlight transitds = new DSFlight();
        DSFlight.FLIGHTRow transitRow = null;
        DSFlight.FLIGHTRow row = (DSFlight.FLIGHTRow)ds.FLIGHT.Rows[0];
        if (row.TURN != "")
        {
			
            transitds = CtrlFlight.RetrieveFlightsByFlightId(row.TURN);
            transitRow = (DSFlight.FLIGHTRow)transitds.FLIGHT.Rows[0];
			if (row.A_D == "A")
			{//Arrival Flight.
				//To display info, row must contain Departure Flight Info
				//                 transitRow must contain Arrival Flight Info
				DSFlight.FLIGHTRow tempRow = row;
				row = transitRow;
				transitRow = tempRow;
			}
			SetCurFlightId(row.URNO);
            Session["TransitUrno"] = row.TURN;
        }
        else
        {
            Session["TransitUrno"] = "";
        }
        
        MWU_CompanyHeading2.ShowFlightInfoHeading(row.FL_SUMM);
        lblHdgStaffCnt.Text = "Staff: " + CtrlJob.GetStaffCount(urno);

        lstClosedFlightTimes.Items.Clear();
        lstClosedFlightCmd.Items.Clear();
        lstClosedFlightTimes.Items.Add("MAB     " + GetTimeFromString(row.MAB));
        if (transitds.FLIGHT.Rows.Count != 0)
		{	//To display info, row must contain Departure Flight Info
			//                 transitRow must contain Arrival Flight Info
            lstClosedFlightTimes.Items.Add("ATA     " + GetTimeFromString(transitRow.AT));
            lstClosedFlightTimes.Items.Add("TBS-UP  " + GetTimeFromString(transitRow.TBS_UP));
            lstClosedFlightTimes.Items.Add("PLB(A)    " + GetTimeFromString(transitRow.PLB));
            lstClosedFlightTimes.Items.Add("UNLOAD-START " + GetTimeFromString(transitRow.UNL_STRT));
            lstClosedFlightTimes.Items.Add("FTRIP   " + GetTimeFromString(transitRow.AP_FST_TRP));
            lstClosedFlightTimes.Items.Add("LTRIP " + GetTimeFromString(transitRow.AP_LST_TRP));
            lstClosedFlightTimes.Items.Add("UNLOAD-END " + GetTimeFromString(transitRow.UNL_END));
            lstClosedFlightTimes.Items.Add("LOAD-START " + GetTimeFromString(row.LD_ST));
            lstClosedFlightTimes.Items.Add("LOAD-END " + GetTimeFromString(row.LD_END));
            lstClosedFlightTimes.Items.Add("LAST-DOOR " + GetTimeFromString(row.LST_DOOR));
            lstClosedFlightTimes.Items.Add("PLB(D)     " + GetTimeFromString(row.PLB));
            lstClosedFlightTimes.Items.Add("ATD     " + GetTimeFromString(row.AT));
            lstClosedFlightCmd.Items.Add("SHOW CPM");
            lstClosedFlightCmd.Items.Add("SHOW TRIPS");
            lstClosedFlightCmd.Items.Add("ALL OK2LOAD");
            lstClosedFlightCmd.Items.Add("SHOW DLS");
        }
        else

        if (row.A_D == "A")
        {//Arrival Flight
            lstClosedFlightTimes.Items.Add("ATA     " + GetTimeFromString(row.AT));
            lstClosedFlightTimes.Items.Add("TBS-UP  " + GetTimeFromString(row.TBS_UP));
            lstClosedFlightTimes.Items.Add("PLB     " + GetTimeFromString(row.PLB));
            lstClosedFlightTimes.Items.Add("UNLOAD-START " + GetTimeFromString(row.UNL_STRT));
            lstClosedFlightTimes.Items.Add("FTRIP   " + GetTimeFromString(row.AP_FST_TRP));
            lstClosedFlightTimes.Items.Add("LTRIP " + GetTimeFromString(row.AP_LST_TRP));
            lstClosedFlightTimes.Items.Add("UNLOAD-END " + GetTimeFromString(row.UNL_END));
            lstClosedFlightCmd.Items.Add("SHOW CPM");
        }
        else
        {//Departure Flight
            lstClosedFlightTimes.Items.Add("LOAD-START " + GetTimeFromString(row.LD_ST));
            lstClosedFlightTimes.Items.Add("LOAD-END " + GetTimeFromString(row.LD_END));
            lstClosedFlightTimes.Items.Add("LAST-DOOR " + GetTimeFromString(row.LST_DOOR));
            lstClosedFlightTimes.Items.Add("PLB     " + GetTimeFromString(row.PLB));
            lstClosedFlightTimes.Items.Add("ATD     " + GetTimeFromString(row.AT));
            lstClosedFlightCmd.Items.Add("ALL OK2LOAD");
            lstClosedFlightCmd.Items.Add("SHOW DLS");
            lstClosedFlightCmd.Items.Add("SHOW TRIPS");
        }


        lstClosedFlightCmd.Items.Add("SHOW STAFF");
        lstClosedFlightCmd.Items.Add("SHOW EQUIPMENT");
        lstClosedFlightCmd.Items.Add("ASR");   
      
        ActiveForm = frmClosedFlightDetail;
    }

    private string GetTimeFromString(string ufisDt)
    {
        string time = "";
        try
        {
            time = ufisDt.Substring(8, 4);
        }
        catch (Exception)
        {
        }
        return time;
    }

    protected void cmdClosedFlightBack_Click(object sender, EventArgs e)
    {
        RedirectToMobilePage("~/Menu.aspx", true);
    }
    protected void cmdBackClosedFlightDetail_Click(object sender, EventArgs e)
    {
        ActiveForm = frmClosedFlight;
    }
    protected void lstClosedFlightCmd_ItemCommand(object sender, ListCommandEventArgs e)
    {
       
        string cmd = e.ListItem.Text;
        switch (cmd)
        {
            case "SHOW CPM":
                ShowCPM("cpm");
                break;
            case "SHOW STAFF":
                ShowStaff();
                break;
            case "SHOW EQUIPMENT":
                ShowEquipment();
                break;
            case "ASR":
                ShowASR();
                break;
            case "ALL OK2LOAD":
                ShowAllOkToLoad();
                break;
            case "SHOW DLS":
                ShowDLS();
                break;
            case "SHOW TRIPS":
                ShowCPM("trips");
                break;
        }
    }

    private void ShowCPM(string type)
    {
        String flightId = GetCurFlightId();
        if (flightId != "")
        {
            if (Session["TransitUrno"].ToString() == "")
            {
                if (type == "cpm")
                {
                    MWUShowCpm1.ShowCpm(flightId, Session["TransitUrno"].ToString(), "A", "");
                    MWU_CompanyHeading3.ShowFlightInfoHeading(CtrlFlight.GetFlightSummInfo(flightId));
                    ActiveForm = frmShowCPM;
                }
                else
                    if (type == "trips")
                    {
                        MWUShowCpm1.ShowCpm(flightId, Session["TransitUrno"].ToString(), "D", "");
                        MWU_CompanyHeading3.ShowFlightInfoHeading(CtrlFlight.GetFlightSummInfo(flightId));
                        ActiveForm = frmShowCPM;
                    }
                //RedirectToMobilePage("Staff.aspx?ref=ClosedFlight.aspx&FLID=" + flightId);
            }
            else
            {
                if (type == "cpm")
                {
                    MWUShowCpm1.ShowCpm(flightId, Session["TransitUrno"].ToString(), "T", type);
                }
                else

                    if (type == "trips")

                    {
                        MWUShowCpm1.ShowCpm(flightId, Session["TransitUrno"].ToString(), "T", type);
                    }
                MWU_CompanyHeading3.ShowFlightInfoHeading(CtrlFlight.GetFlightSummInfo(flightId));
                ActiveForm = frmShowCPM;
            }
        }
    }

    private void ShowStaff()
    {
        //RedirectToMobilePage("Staff.aspx?ref=ClosedFlight.aspx");
        String flightId = GetCurFlightId();
        if (flightId != "")
        {
            MWUShowStaff1.ShowStaff(flightId);
            MWU_CompanyHeading4.ShowFlightInfoHeading(CtrlFlight.GetFlightSummInfo(flightId));
            ActiveForm = frmShowStaff;
        }
    }

    private void ShowEquipment()
    {
        RedirectToMobilePage("gse/Equipments.aspx?reqPg=" + Request.Url.Segments[Request.Url.Segments.Length-1]);
    }
    private void ShowASR()
    {
        RedirectToMobilePage("gse/RsmInfos.aspx?reqPg=" + Request.Url.Segments[Request.Url.Segments.Length - 1]);
    }
    private void ShowAllOkToLoad()
    {
        string urno = GetCurFlightId();
        DSFlight ds = CtrlFlight.RetrieveFlightsByFlightId(urno);
        DSFlight.FLIGHTRow row = (DSFlight.FLIGHTRow)ds.FLIGHT.Rows[0];
        SessionManager sessMan = new SessionManager();
        ActiveForm = frmOK2Load;
        MWU_CompanyHeading5.ShowFlightInfoHeading(row.FL_SUMM);      

        ArrayList lstOK2Loads = new ArrayList();
        lstOK2Loads.Clear();
        lstAllOK2Load.Items.Clear();
        // lstOK2Loads = sessMan.GetAllOk2LoadAL(Session["FlightNO"].ToString(), Session["FlightURNO"].ToString(), Session["sessionid"].ToString());
        lstOK2Loads = sessMan.GetAllOk2LoadAL(row.FLNO, urno, Session["sessionid"].ToString());
        lstAllOK2Load.Visible = true;
        lstAllOK2Load.DataSource = lstOK2Loads;
        lstAllOK2Load.DataBind();
        ActiveForm = frmOK2Load;
        lblOk2Load.Text = "ULDs RECEIVED";
    
    }



    private void ShowDLS()
    {
        string urno = GetCurFlightId();
        DSFlight ds = CtrlFlight.RetrieveFlightsByFlightId(urno);
        DSFlight.FLIGHTRow row = (DSFlight.FLIGHTRow)ds.FLIGHT.Rows[0];
        SessionManager sessMan = new SessionManager();
        string DLS = sessMan.GetShowDLS(row.FLNO, urno, Session["sessionid"].ToString());
      
        lblDLS.Text = "SHOW DLS";
       MWU_CompanyHeading6.ShowFlightInfoHeading(row.FL_SUMM);  
        if (DLS != "")
        {
           
            txtVShowDLS.Text = DLS;
           
            ActiveForm = frmDLS;

            


        }
        else
        {
            ActiveForm = frmDLS;

            txtVShowDLS.Text = "No DLS data available for this flight";
        }
    
    
    }
    protected void cmdBackShowCpmClosedFlight_Click(object sender, EventArgs e)
    {
        ShowFlightDetail();
    }
    protected void cmdBackShowStaffClosedFlight_Click(object sender, EventArgs e)
    {
        ShowFlightDetail();
    }
    protected void cmdBackShowCPM_Click(object sender, EventArgs e)
    {
        ShowFlightDetail();
    }
    protected void cmdBackFromOK2L_Click(object sender, EventArgs e)
    {
        ShowFlightDetail();
    }
    protected void cmdBackFromDLS_Click(object sender, EventArgs e)
    {
        ShowFlightDetail();
    }
    protected override void OnViewStateExpire(EventArgs e)
    {


        iTrekUtils.iTUtils.LogToEventLog("ViewState Expired in Closed Flight", "iTrekError");
        RedirectToMobilePage("Login.aspx");

    }
}
