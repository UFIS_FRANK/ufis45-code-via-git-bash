using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.EventArg;

public partial class WAP_MWUCpmManual : System.Web.UI.MobileControls.MobileUserControl
{
    private const string BASEID = "MWUCpmManual__";
    private const string PFIX_SEL_LIST = "SelList__";
    private const string PFIX_TXT_BOX = "TxtBox__";
    private const string PFIX_LIST = "List__";
    private const string PFIX_PANEL = "Panel__";

    private int _numOfEntry = 1;
    private bool _hasChkBox = true;
    private bool _hasLink = false;
    private string _linkText = "REMARK";

    public string LinkText
    {
        get
        {
            if (_linkText == "") _linkText = "REMARK";
            return _linkText;
        }
        set
        {
            if ((value != null) && (value != "")) _linkText = value;
        }
    }

    public bool HasLink
    {
        get { return _hasLink; }
        set
        {
            _hasLink = value;
        }
    }

    public int NumOfEntry
    {
        get{ return _numOfEntry; }
        set
        {
            if (value < 1) value = 1; else if (value > 100) value = 100;
            _numOfEntry = value;
        }
    }

    public bool HasChkBox
    {
        get { return _hasChkBox; }
        set
        {
            _hasChkBox = value;
        }
    }
    private const string VS_CNT = "MWUCpmManual_VS_CNT";



    protected void Page_Load(object sender, EventArgs e)
    {
        //if(!IsPostBack)
            SetData();
    }

    public void SetData()
    {
        SetData(NumOfEntry);
    }

    public EntCPMList GetData(EntCPMList cpmList)
    {
        try
        {
            string contNo = "";
            int cnt = this.Panel1.Controls.Count;
            if (cpmList == null) cpmList = new EntCPMList();
            for (int i = 0; i < cnt; i++)
            {
                contNo = ((System.Web.UI.MobileControls.TextBox)this.Panel1.FindControl(PFIX_TXT_BOX + i)).Text.Trim();
                try
                {
                  long testInt= Convert.ToInt64(contNo);
                   contNo = "BT" + contNo;
                }
                catch 
                {
                    
                    
                }
                if (cpmList.Count <= i)
                {
                    cpmList.SetData(i, new EntCPM());
                }
                cpmList.GetData(i).ContainerNo = contNo;

                if (HasChkBox)
                {
                    System.Web.UI.MobileControls.SelectionList selList = (System.Web.UI.MobileControls.SelectionList)this.Panel1.FindControl(PFIX_SEL_LIST + i);
                    cpmList.GetData(i).Selected = selList.Items[0].Selected;
                }
                else
                {
                    if (contNo == "")
                    {
                        cpmList.GetData(i).ClearData();
                    }
                    else cpmList.GetData(i).Selected = true;
                }
            }
        }
        catch (Exception ex)
        {
           string st = ", err : " + ex.Message;
        }
        return cpmList;
    }

    public void SetData( int noOfEntry )
    {
        if (noOfEntry<1) noOfEntry = 1;
        if (noOfEntry>100) noOfEntry = 100;
        List lst = new List();

        for (int i=0; i<noOfEntry; i++)
        {
            //System.Web.UI.MobileControls.Panel pnl = new System.Web.UI.MobileControls.Panel();
            //pnl.ID = PFIX_PANEL + i;

            if (HasChkBox)
            {
                SelectionList selList = new SelectionList();
                selList.BreakAfter = false;
                selList.SelectType = ListSelectType.CheckBox;
                selList.ID = PFIX_SEL_LIST + i;
                selList.Items.Add("");
                selList.SelectedIndexChanged += new EventHandler(selList_SelectedIndexChanged);
                //selList.Items[0].Selected = true;
                //pnl.Controls.Add(selList);
                this.Panel1.Controls.Add(selList);
            }
            
            System.Web.UI.MobileControls.TextBox txtBox = new System.Web.UI.MobileControls.TextBox();
            txtBox.ID = PFIX_TXT_BOX + i;
            txtBox.TextChanged += new EventHandler(txtBox_TextChanged);
            txtBox.EnableViewState = true;
            txtBox.EnsureTemplatedUI();
            //pnl.Controls.Add(txtBox);
            this.Panel1.Controls.Add(txtBox);

            //txtBox.Text = "TEST " + i;

            //Link lnk = new Link();
            //lnk.ID = "MWUCmpManual__lnk__" + i;
            //lnk.Text = "REMARK";
            //lnk.NavigateUrl = "#frmReport";
            //pnl.Controls.Add(lnk);

            if (HasLink)
            {
                System.Web.UI.MobileControls.List lst1 = new List();
                lst1.ID = PFIX_LIST + i;
                //lst1.Items.Add("REMARK");
                MobileListItem mli = new MobileListItem(LinkText, i.ToString());
                //MobileListItem mli = new MobileListItem("REMARK", i.ToString());
                lst1.Items.Add(mli);
                lst1.ItemCommand += new ListCommandEventHandler(lst1_ItemCommand);

                //pnl.Controls.Add(lst1);
                this.Panel1.Controls.Add(lst1);
            }

            //this.Panel1.Controls.Add(pnl);          
        }
        
        //lblTest.Text = GetInfo();
    }

    void selList_SelectedIndexChanged(object sender, EventArgs e)
    {
        System.Web.UI.MobileControls.SelectionList selList = (System.Web.UI.MobileControls.SelectionList)sender;
        bool chk = selList.Items[0].Selected;
        string st = selList.ID;
        string st2 = selList.Items[0].Value;
        string st3 = selList.Items[0].Text;
    }

     //add a delegate
    public delegate void LinkClickHandler(object sender,
        EaLinkClickEventArgs e);

    // add an event of the delegate type       
    public event LinkClickHandler LinkClick;


    void lst1_ItemCommand(object sender, ListCommandEventArgs e)
    {
        try
        {
            System.Web.UI.MobileControls.List lst = (System.Web.UI.MobileControls.List)sender;
            string id = lst.ID;
            String st2 = lst.Items[0].Value;
            string st3 = lst.Items[0].Text;
            EaLinkClickEventArgs arg = new EaLinkClickEventArgs(Convert.ToInt32(st2));
            LinkClick(this, arg);
        }
        catch (Exception)
        {
        }
    }

    // add a delegate
    public delegate void TextChangedHandler(object sender,
        TextBoxEventArgs e);

    // add an event of the delegate type       
    public event TextChangedHandler TextChanged;

    void txtBox_TextChanged(object sender, EventArgs e)
    {
        //throw new Exception("The method or operation is not implemented.");
        try
        {
            string st = ((System.Web.UI.MobileControls.TextBox)sender).Text;
            //lblTest.Text = st;
            TextBoxEventArgs ev = new TextBoxEventArgs(st);
            TextChanged(this, ev);
        }
        catch (Exception)
        {
        }
        
    }

    public string GetInfo()
    {
        int ctrlCnt = this.Controls.Count;
        string st = "";
        //st += TextBox1.Text;
        try
        {
            int cnt = this.Panel1.Controls.Count;
            cnt = _numOfEntry;
            for (int i = 0; i < cnt; i++)
            {
                if (HasChkBox)
                {
                    System.Web.UI.MobileControls.SelectionList selList = (System.Web.UI.MobileControls.SelectionList)this.Panel1.FindControl(PFIX_SEL_LIST + i);
                    st += ", chk-" + i + " : " + selList.Items[0].Selected.ToString();
                }
                st += ", tb-" + i + " : " + ((System.Web.UI.MobileControls.TextBox)this.Panel1.FindControl(PFIX_TXT_BOX + i)).Text;
            }
        }
        catch (Exception ex)
        {
            st += ", err : " + ex.Message;
        }

        //for (int i = 0; i < ctrlCnt; i++)
        //{
        //    Control ctrl = this.Controls[i];
        //    st += i + " : " + ctrl.GetType() + " - " + ctrl.ID + ", ";
        //    if (ctrl.HasControls())
        //    {
        //        try
        //        {
        //            st += "textbox1 = " + ((System.Web.UI.MobileControls.TextBox)ctrl.FindControl("MWUCmpManual__txtBox__1")).Text;

        //        }
        //        catch (Exception ex)
        //        {
        //            st += ", Err : " + ex.Message;
        //            //throw;
        //        }
        //    }
        //    //if (this.Controls[i].GetType()==System.Web.UI.MobileControls.TextBox
        //}
        
        return st;
    }


    protected void Panel1_Load(object sender, EventArgs e)
    {
        Console.WriteLine("Panel1 Load");
    }
    protected void Panel1_PreRender(object sender, EventArgs e)
    {
        Console.WriteLine("Panel1 Prerender");
        
        
        string st = Response.ToString();
        
    }
}

public class TextBoxEventArgs : EventArgs
{
    string _text="";
    public TextBoxEventArgs(string textBoxText)
    {
        if (textBoxText == null) textBoxText = "";
        this._text = textBoxText;
    }

    public string Text
    {
        get { return _text; }
    }
}




