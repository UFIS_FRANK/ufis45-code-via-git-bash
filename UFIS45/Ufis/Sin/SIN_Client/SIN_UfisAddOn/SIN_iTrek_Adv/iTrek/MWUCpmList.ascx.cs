using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.EventArg;

public partial class WAP_MWUCpmList : System.Web.UI.MobileControls.MobileUserControl
{

    #region private attributes
    private const string VS_ITEM_LIST = "MWUCpmList-VS_ITEM_LIST";
    private const string VS_CB_OR_RB = "MWUCpmList-VS_CB_OR_RB";//Check Box or Radio Button List
    private const string CB = "C"; //Check Box
    private const string RB = "R"; //Radio Button
    private const string VSITEMPERPG = "MWUCpmListVsItemPerPage";
    //Max No. of items per page
    //if <1, show all the items in a page
    private const string VSPGNO = "MWUCpmListVsPageNo"; //Current Page No.

    private const string BASEID = "MWUCpmManual__";
    private const string PFIX_SEL_LIST = BASEID + "SelList__";
    private const string PFIX_TXT_BOX = BASEID + "TxtBox__";
    private const string PFIX_LIST = BASEID + "List__";
    private const string PFIX_PANEL = BASEID + "Panel__";

    private string _linkText = "REMARK12";
    private int _noOfEntry = 0;
    private ArrayList al = null;

    private DataTable _dataSource = null;
    private string _dataTextField = "";
    private string _dataValueField = "";
    private string _sortBy = null;

    private string _chkBoxOrRadioButton = CB;
    private bool _RemarkCmdVisible = false;

    private int _curPageNo = -1;
    private int _itemPerPage = -2;
    #endregion

    #region properties
    private int CurPageNo
    {
        get
        {
            if (_curPageNo < 1)
            {
                try
                {
                    _curPageNo = Convert.ToInt32(ViewState[VSPGNO].ToString());
                }
                catch (Exception)
                {
                }
            }
            if (_curPageNo < 1)
            {
                _curPageNo = 1;
                ViewState[VSPGNO] = _curPageNo;
            }
            return _curPageNo;
        }
        set
        {
            if (value > 1)
            {
                if (ItemPerPage < 1)
                {
                    value = 1;
                }
                else if (value > NoOfPage)
                {
                    value = NoOfPage;
                }
            }

            if (value < 1) { value = 1; }
            _curPageNo = value;
            ViewState[VSPGNO] = _curPageNo;
        }
    }
    private void KeepCurrentInfo()
    {
        al = (ArrayList)ViewState[VS_ITEM_LIST];
        if (al != null)
        {
            int cnt = al.Count;
            EntItem entItem;
            string containerNo = "";
            if (ChkBoxOrRadioButton == CB)
            {
                for (int i = 0; i < cnt; i++)
                {
                    entItem = (EntItem)al[i];
                    containerNo = entItem.ItemValue;
                    try
                    {
                        System.Web.UI.MobileControls.SelectionList selList = (System.Web.UI.MobileControls.SelectionList)this.Panel1.FindControl(PFIX_SEL_LIST + i);
                        if (selList != null)
                        {
                            ((EntItem)al[i]).Selected = selList.Items[0].Selected;
                            ViewState[VS_ITEM_LIST] = al;
                        }
                    }
                    catch (Exception)
                    {
                    }
                }
            }
        }
    }

    public int ItemPerPage
    {
        get
        {
            if (_itemPerPage < -1)
            {
                try
                {
                    _itemPerPage = Convert.ToInt32(ViewState[VSITEMPERPG].ToString());
                }
                catch (Exception)
                {
                }
            }

            if (_itemPerPage < -1)
            {
                _itemPerPage = -1;
                ViewState[VSITEMPERPG] = _itemPerPage;
            }
            return _itemPerPage;
        }
        set
        {
            _itemPerPage = value;
            ViewState[VSITEMPERPG] = _itemPerPage;
        }
    }

    public int NoOfPage
    {
        get
        {
            int pageCnt = 1;
            int itemPerPage = ItemPerPage;
            if (itemPerPage > 0)// paging on
            {
                if (al != null)
                {
                    pageCnt = al.Count / itemPerPage;
                    if ((al.Count % itemPerPage) > 0)
                    {
                        pageCnt++;
                    }
                }
            }
            return pageCnt;
        }
    }

    public bool IsFirstPage
    {
        get
        {
            bool firstPage = false;
            if (ItemPerPage < 1)// show all in one page
            {
                firstPage = true;
            }
            else if (CurPageNo == 1) firstPage = true;
            return firstPage;
        }
    }

    public bool IsLastPage
    {
        get
        {
            bool lastPage = false;
            if (ItemPerPage < 1)// show all in one page
                lastPage = true;
            else if (CurPageNo >= NoOfPage) lastPage = true;
            return lastPage;
        }
    }

    public void GotoNextPage()
    {
        KeepCurrentInfo();
        CurPageNo = CurPageNo + 1;
    }

    public void GotoPrevPage()
    {
        KeepCurrentInfo();
        CurPageNo = CurPageNo - 1;
    }

    public int NumOfEntry
    {
        get { return _noOfEntry; }
        set
        {
            if (value < 1) value = 1; else if (value > 100) value = 100;
            _noOfEntry = value;
        }
    }
    public DataTable DataSource
    {
        get
        {
            return _dataSource;
        }
        set
        {
            _dataSource = value;
        }
    }
    public string DataTextField
    {
        get { return _dataTextField; }
        set { _dataTextField = value; }
    }
    public string DataValueField
    {
        get { return _dataValueField; }
        set { _dataValueField = value; }
    }
    public string SortBy
    {
        get
        {
            if (_sortBy == null) _sortBy = "";
            return _sortBy;
        }
        set
        {
            _sortBy = value;
        }
    }

    public string LinkText
    {
        get
        {
            if (_linkText == "") _linkText = "REMARK";
            return _linkText;
        }
        set
        {
            if ((value != null) && (value != "")) _linkText = value;
        }
    }
    public string ChkBoxOrRadioButton
    {
        get { return _chkBoxOrRadioButton; }
        set
        {
            if ((value == CB) || (value == RB))
            {
                _chkBoxOrRadioButton = value;
            }
            else
            {
                throw new ApplicationException("Invalid Parameter for CheckBoxOrRadioButton");
            }
        }
    }
    [DefaultValue(false)]
    public bool RemarkCmdVisible
    {
        get { return _RemarkCmdVisible; }
        set { _RemarkCmdVisible = value; }
    }
    #endregion

    #region Page Events
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack)
        {
            RestoreInfo();
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        this.ShowInfo();
    }
    #endregion

    #region Link Click Event Handler
    //add a delegate
    public delegate void LinkClickHandler(object sender,
        EaLinkClickEventArgs e);

    // add an event of the delegate type       
    public event LinkClickHandler LinkClick;


    void lst1_ItemCommand(object sender, ListCommandEventArgs e)
    {
        try
        {
            System.Web.UI.MobileControls.List lst = (System.Web.UI.MobileControls.List)sender;
            string id = lst.ID;
            String st2 = lst.Items[0].Value;
            string st3 = lst.Items[0].Text;
            EaLinkClickEventArgs arg = new EaLinkClickEventArgs(Convert.ToInt32(st2));
            LinkClick(this, arg);
        }
        catch (Exception)
        {
        }
    }
    #endregion

    private void RestoreInfo()
    {
        try
        {
            if (DataSource != null) DataBind();
            else
            {
                ArrayList alItem = (ArrayList)ViewState[VS_ITEM_LIST];
                if (alItem != null)
                {
                    SetData(alItem);
                    ShowData(alItem);
                }
            }
        }
        catch (Exception ex)
        {
            string st = ex.Message;
        }
    }

    private void ShowInfo()
    {
        try
        {
            ArrayList alItem = (ArrayList)ViewState[VS_ITEM_LIST];
            ShowData(alItem);
        }
        catch (Exception)
        {
        }
    }

    private void ShowItem(string text, string value, int idx)
    {
        SelectionList selList = new SelectionList();
        selList.BreakAfter = false;

        if (ChkBoxOrRadioButton == CB)
        {
            selList.SelectType = ListSelectType.CheckBox;
            selList.ID = PFIX_SEL_LIST + idx;
        }
        else if (ChkBoxOrRadioButton == RB)
        {
            selList.SelectType = ListSelectType.Radio;
            selList.ID = PFIX_SEL_LIST + "0";
        }

        MobileListItem mli = new MobileListItem(text, value);
        selList.Items.Add(mli);
        //pnl.Controls.Add(selList);
        this.Panel1.Controls.Add(selList);

        if (RemarkCmdVisible)
        {

            System.Web.UI.MobileControls.List lst1 = new List();
            lst1.ID = PFIX_LIST + idx;
            MobileListItem mli1 = new MobileListItem(LinkText, idx.ToString());
            lst1.Items.Add(mli1);
            lst1.ItemCommand += new ListCommandEventHandler(lst1_ItemCommand);

            this.Panel1.Controls.Add(lst1);
        }
    }

    private void ShowItem(int idx, EntItem entItem)
    {
        SelectionList selList = new SelectionList();
        selList.BreakAfter = false;

        if (ChkBoxOrRadioButton == CB)
        {
            selList.SelectType = ListSelectType.CheckBox;
            selList.ID = PFIX_SEL_LIST + idx;
        }
        else if (ChkBoxOrRadioButton == RB)
        {
            selList.SelectType = ListSelectType.Radio;
            selList.ID = PFIX_SEL_LIST + "0";
        }

        MobileListItem mli = new MobileListItem(entItem.ItemText, entItem.ItemValue);
        mli.Selected = entItem.Selected;
        selList.Items.Add(mli);
        //pnl.Controls.Add(selList);
        this.Panel1.Controls.Add(selList);

        if (RemarkCmdVisible)
        {
            System.Web.UI.MobileControls.List lst1 = new List();
            lst1.ID = PFIX_LIST + idx;
            MobileListItem mli1 = new MobileListItem(LinkText, idx.ToString());
            lst1.Items.Add(mli1);
            lst1.ItemCommand += new ListCommandEventHandler(lst1_ItemCommand);

            this.Panel1.Controls.Add(lst1);
        }
    }

    private void ShowData(ArrayList alItem)
    {
        this.Panel1.Controls.Clear();

        int cnt = alItem.Count;
        int frCnt = 0;
        int toCnt = cnt;
        if (ItemPerPage > 0)//Paging On
        {
            if (CurPageNo > NoOfPage) CurPageNo = NoOfPage;
            frCnt = ItemPerPage * (CurPageNo - 1);
            toCnt = frCnt + ItemPerPage;
            if (toCnt > cnt) toCnt = cnt;
        }
        else
        {
            CurPageNo = 1;
        }
        for (int i = frCnt; i < toCnt; i++)
        {
            EntItem item = (EntItem)alItem[i];
            //ShowItem(item.ItemText, item.ItemValue, i);
            ShowItem(i, item);
        }
    }

    private void SetData(ArrayList alItem)
    {
        al = alItem;
    }


    public void Clear()
    {
        DataSource = null;
        Panel1.Controls.Clear();
    }

    public EntCPMList GetData(EntCPMList cpmList)
    {
        if (cpmList == null)
        {
            cpmList = new EntCPMList();
        }
        if (al != null)
        {
            int cnt = al.Count;
            EntItem entItem;
            string containerNo = "";
            string urno = "";
            string destination = "L";
            if (ChkBoxOrRadioButton == CB)
            {
                for (int i = 0; i < cnt; i++)
                {
                    entItem = (EntItem)al[i];
                    //*Alphy*// containerNo = entItem.ItemValue;
                    containerNo = entItem.ItemText;
                    
                    urno = entItem.ItemValue;
                    try
                    {
                        destination = containerNo.Split(new char[] { '/' })[4].ToString();
                        if (destination.Trim().StartsWith("BT") || destination.Trim().StartsWith("T"))
                        {
                            destination = "I";
                        }
                        else
                        {
                            destination = "L";
                        }
                            
                    }
                    catch (Exception)
                    {
                        destination = "L";
                        
                    }
                    EntCPM cpm;
                    int idx = cpmList.GetIndexOfContainer(containerNo);
                    if (idx < 0)
                    {
                        cpm = new EntCPM();
                    }
                    else
                    {
                        cpm = cpmList.GetData(idx);
                    }
                    cpm.ContainerNo = containerNo;
                    cpm.Urno = urno;
                    cpm.Selected = entItem.Selected;
                    
                        cpm.Destination = destination;
                    
                   

                    try
                    {
                        System.Web.UI.MobileControls.SelectionList selList = (System.Web.UI.MobileControls.SelectionList)this.Panel1.FindControl(PFIX_SEL_LIST + i);
                        if (selList != null)
                        {
                            cpm.Selected = selList.Items[0].Selected;
                            ((EntItem)al[i]).Selected = cpm.Selected;
                            ViewState[VS_ITEM_LIST] = al;
                        }
                    }
                    catch (Exception)
                    {
                    }
                    cpmList.SetData(idx, cpm);
                }
            }
            else if (ChkBoxOrRadioButton == RB)
            {
                EntCPM cpm = new EntCPM();
                //System.Web.UI.MobileControls.SelectionList selList = (System.Web.UI.MobileControls.SelectionList)this.Panel1.FindControl(PFIX_SEL_LIST + "0");
                //cpm.Selected = selList.SelectedIndex;
                cpmList.AddData(cpm);
            }
        }
        return cpmList;
    }

    public new void DataBind()
    {
        if (_dataSource == null) throw new ApplicationException("No Data Source.");
        if ((_dataTextField == null) || (_dataTextField == "")) throw new ApplicationException("No Data Text Field.");
        if ((_dataValueField == null) || (_dataValueField == "")) throw new ApplicationException("No Data Value Field.");

        int noOfEntry;
        noOfEntry = DataSource.Rows.Count;
        al = null;
        foreach (DataRow dr in _dataSource.Select("", SortBy))
        {
            AddItem(dr[DataTextField].ToString(), dr[DataValueField].ToString());
        }
    }

    public void AddItem(string text, string value)
    {
        EntItem item = new EntItem(text, value);
        if (al == null) al = new ArrayList();
        al.Add(item);
        ViewState[VS_ITEM_LIST] = al;
    }

}
[Serializable]
class EntItem
{
    private string _text;
    private string _value;
    private bool _selected = false;

    internal string ItemText
    {
        get { return _text; }
        set { _text = value; }
    }

    internal string ItemValue
    {
        get { return _value; }
        set { _value = value; }
    }

    internal bool Selected
    {
        get { return _selected; }
        set { _selected = value; }
    }

    internal EntItem(string text, string value)
    {
        _text = text;
        _value = value;
    }
}
