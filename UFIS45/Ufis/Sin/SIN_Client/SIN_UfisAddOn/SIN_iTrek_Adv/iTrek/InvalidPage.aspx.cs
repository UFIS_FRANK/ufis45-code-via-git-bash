using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using iTrekXML;
using UFIS.ITrekLib.Ctrl;

public partial class InvalidPage : System.Web.UI.MobileControls.MobilePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        lblErrorMsg.Text = "Invalid entry.Press Go to go to Menu";
    }
    protected void cmdError_Click(object sender, EventArgs e)
    {
        iTXMLcl iTXML = new iTXMLcl();
        if (Context.Session.Count == 0)
        {
            RedirectToMobilePage("Login.aspx");
        }
        else
        {
            string jobCategory = Session["ORGCODE"].ToString();

            if (jobCategory.Substring(0, 1) == "A")
            {
                RedirectToMobilePage("Menu.aspx?MENU=OP");
            }
            else

                if (jobCategory.Substring(0, 1) == "B")
                {
                    RedirectToMobilePage("Baggage.aspx");
                }


        }
    }

    protected override void OnViewStateExpire(EventArgs e)
    {


        iTrekUtils.iTUtils.LogToEventLog("ViewState Expired in error page", "iTrekError");
        RedirectToMobilePage("Login.aspx");

    }
}
