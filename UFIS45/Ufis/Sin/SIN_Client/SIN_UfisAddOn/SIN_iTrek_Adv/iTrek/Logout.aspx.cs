using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using iTrekXML;
using iTrekSessionMgr;

public partial class Logout : System.Web.UI.MobileControls.MobilePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            lblLogout.Text = "Logout";
            DateTime curTime = DateTime.Now;
            string curStr = curTime.ToShortTimeString();
            txVLogout.Text = "You logged out at " + curStr + "<br/>";
            txVLogout.Text += "Press Go to Login again.";
        }
            
            //if (Context.Session.Count >1)
            //{
            //try
            //{
               
            //    iTXMLcl iTXML = new iTXMLcl();
            //    string PENO = Session["staffid"].ToString();
            //    iTXML.DeleteDeviceIDEntryByStaffId(PENO);
            //    iTrekUtils.iTUtils.LogToEventLog("Logged Out Staffid=" + PENO, "iTrekDevice");
            //    //Session.Clear();
            //    MobileFormsAuthentication.SignOut();
            //    Session.Abandon();
            //    //Session.Abandon();
            //}
            //catch (Exception sessExcep)
            //{
            //    //ActiveForm = frmError;
            //    //lblErrorMsg.Text = "Timeout error occured. Please try again";
            //    iTrekUtils.iTUtils.LogToEventLog("Logout_Click" + sessExcep.Message, "iTrekError");
            //    RedirectToMobilePage("iTrekError.aspx");

            //}
           // }
            
        
        
    }
    protected void cmdLogin_Click(object sender, EventArgs e)
    {
        MobileFormsAuthentication.SignOut();
     RedirectToMobilePage("Login.aspx");
    }
    protected override void OnViewStateExpire(EventArgs e)
    {


        iTrekUtils.iTUtils.LogToEventLog("ViewState Expired in Logout Page", "iTrekError");
        RedirectToMobilePage("Login.aspx");

    }
}
