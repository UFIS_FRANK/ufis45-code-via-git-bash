<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ClosedFlight.aspx.cs" Inherits="WAP_ClosedFlight" EnableSessionState="True" %>

<%@ Register Src="MWU_CompanyHeading.ascx" TagName="MWU_CompanyHeading" TagPrefix="uc1" %>
<%@ Register Src="MWUShowCpm.ascx" TagName="MWUShowCpm" TagPrefix="uc2" %>
<%@ Register Src="MWUShowStaff.ascx" TagName="MWUShowStaff" TagPrefix="uc3" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmClosedFlight" runat="server"><uc1:MWU_CompanyHeading ID="MWU_CompanyHeading1"
        runat="server" /> <mobile:Label ID="lblClosedFlight" Runat="server" Alignment="Center">Closed Flights</mobile:Label><br /><mobile:List ID="lstClosedFlights" Runat="server" DataTextField="FL_SUMM"
            DataValueField="URNO" Decoration="Numbered" OnItemCommand="lstClosedFlights_ItemCommand"></mobile:List>&nbsp; <mobile:Label
                ID="lblClosedFlightErrMsg" Runat="server">
            </mobile:Label> <mobile:Command ID="cmdClosedFlightBack" Runat="server" OnClick="cmdClosedFlightBack_Click">Back</mobile:Command></mobile:Form>
    <mobile:Form ID="frmClosedFlightDetail" Runat="server"><uc1:MWU_CompanyHeading ID="MWU_CompanyHeading2"
        runat="server" /> <mobile:Label ID="lblHead3ClosedFlightDetail" Runat="server" Alignment="Center">Closed Flight</mobile:Label> <mobile:Label
            ID="lblHdgStaffCnt" Runat="server" Alignment="Center">
        </mobile:Label> <mobile:List
            ID="lstClosedFlightTimes" Runat="server" Decoration="Numbered" EnableViewState="False">
        </mobile:List> <mobile:List ID="lstClosedFlightCmd" Runat="server" OnItemCommand="lstClosedFlightCmd_ItemCommand"></mobile:List>&nbsp; <mobile:Command ID="cmdBackClosedFlightDetail" Runat="server"
            OnClick="cmdBackClosedFlightDetail_Click">Back</mobile:Command></mobile:Form>
    <mobile:Form ID="frmShowStaff" Runat="server"><uc1:MWU_CompanyHeading ID="MWU_CompanyHeading4" runat="server" />&nbsp; <mobile:Label ID="lblHead3ShowStaffClosedFlight" Runat="server" Alignment="Center">Staff</mobile:Label> <uc3:MWUShowStaff ID="MWUShowStaff1" runat="server" /> <mobile:Command ID="cmdBackShowStaffClosedFlight" Runat="server" OnClick="cmdBackShowStaffClosedFlight_Click">Back</mobile:Command></mobile:Form>
    <mobile:Form ID="frmShowCPM" Runat="server"><uc1:MWU_CompanyHeading ID="MWU_CompanyHeading3" runat="server" />&nbsp; <mobile:Label ID="lblHead3ShowCPM" Runat="server" Alignment="Center">CPM</mobile:Label> <mobile:Command
        ID="cmdBackShowCPM" Runat="server" BreakAfter="False" EnableViewState="False"
        OnClick="cmdBackShowCPM_Click" SoftkeyLabel="Back">
    </mobile:Command> <uc2:MWUShowCpm ID="MWUShowCpm1" runat="server" /> <mobile:Command ID="cmdBackShowCpmClosedFlight" Runat="server" OnClick="cmdBackShowCpmClosedFlight_Click">Back</mobile:Command></mobile:Form>
     <mobile:Form ID="frmOK2Load" Runat="server"><uc1:MWU_CompanyHeading ID="MWU_CompanyHeading5"
         runat="server" /> <mobile:Label ID="lblOk2Load" Runat="server">
        </mobile:Label> <br /><mobile:List ID="lstAllOK2Load" Runat="server">
        </mobile:List> <mobile:Command ID="cmdBackFromOK2L" Runat="server" OnClick="cmdBackFromOK2L_Click"
            SoftkeyLabel="Back">
        </mobile:Command></mobile:Form>
         <mobile:Form ID="frmDLS" Runat="server"> <uc1:MWU_CompanyHeading ID="MWU_CompanyHeading6"
             runat="server" /><mobile:Label ID="lblDLS" Runat="server">
        </mobile:Label> <mobile:TextView ID="txtVShowDLS" Runat="server">
        </mobile:TextView> <mobile:Command ID="cmdBackFromDLS" Runat="server" OnClick="cmdBackFromDLS_Click"
            SoftkeyLabel="Back">
        </mobile:Command></mobile:Form>
</body>
</html>
