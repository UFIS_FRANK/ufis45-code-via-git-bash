using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;


using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.EventArg;


public partial class WAP_MWUBagRemk : System.Web.UI.MobileControls.MobileUserControl
{

    private const string SELF_ENTRY_TEXT = "Self Entry";
    private const string SELF_ENTRY_VALUE = "-1";

    private EntRemark _curRmk = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //SetPreDefRmk();
            //ShowCurRmk();
        }
    }

    public void SetCurRmk(EntRemark rmk)
    {
        _curRmk = rmk;
        SetPreDefRmk();
        ShowCurRmk();
    }

    public void ShowCurRmk()
    {
        //if (_curRmk != null)
        //{
        //    string st = _curRmk.Trim().ToUpper();
        //    int cnt = slstPreDefRmk.Items.Count;
        //    slstPreDefRmk.SelectedIndex = -1;
        //    for (int i = 0; i < cnt; i++)
        //    {
        //        if (slstPreDefRmk.Items[i].Text.ToUpper() == st)
        //        {
        //            slstPreDefRmk.SelectedIndex = i;
        //            break;
        //        }
        //    }
        //    if (slstPreDefRmk.SelectedIndex < 0) txtUserRmk.Text = _curRmk;
        //}

        if (_curRmk != null)
        {
            int rmkCnt = _curRmk.GetPreDefRmkCount();

            for (int rmkIdx = 0; rmkIdx < rmkCnt; rmkIdx++)
            {
                string rmkId = _curRmk.GetPreDefRmkValueAt(rmkIdx);
                SetSelection(_curRmk.GetPreDefRmkValueAt(rmkIdx), _curRmk.GetPreDefRmkTextAt(rmkIdx)); 
            }
            if (_curRmk.HasSelfEntry())
            {
                SetSelection(SELF_ENTRY_VALUE, SELF_ENTRY_TEXT);
                txtUserRmk.Text = _curRmk.GetSelfEntryText();
            }
        }       
    }

    private void SetSelection(string id, string text)
    {       
        int cnt = slstPreDefRmk.Items.Count;
        text = text.Trim().ToUpper();
        for (int i = 0; i < cnt; i++)
        {
            MobileListItem mli = slstPreDefRmk.Items[i];
            if ((mli.Value == id) && (mli.Text.Trim().ToUpper()==text))
            {
                slstPreDefRmk.Items[i].Selected = true;
            }
        }
    }

    public void SetPreDefRmk()
    {
        slstPreDefRmk.Items.Clear();
        slstPreDefRmk.DataSource = CtrlPreDefBagConRmk.RetrieveActiveRmk().PREDEFBAGCONRMK;
        slstPreDefRmk.DataBind();
        MobileListItem mli = new MobileListItem(SELF_ENTRY_TEXT, SELF_ENTRY_VALUE);
        slstPreDefRmk.Items.Add(mli);
        txtUserRmk.Text = "";
    }

    // add a delegate
    public delegate void RemarkSaveClickEventHandler(object sender,
        EaRemarkSaveEventArgs e);

    // add an event of the delegate type       
    public event RemarkSaveClickEventHandler RemarkSaveClick;


    protected void cmdBagRemkSave_Click(object sender, EventArgs e)
    {
        string rmk;
        EntRemark entRmk = new EntRemark();
        try
        {
            rmk = txtUserRmk.Text;
            int cnt = slstPreDefRmk.Items.Count;
            for (int i = 0; i < cnt; i++)
            {
                MobileListItem mli = slstPreDefRmk.Items[i];
                if (mli.Selected)
                {
                    if ((mli.Text == SELF_ENTRY_TEXT) && (mli.Value == SELF_ENTRY_VALUE))
                    {
                        if (txtUserRmk.Text.Trim() == "")
                        {
                            entRmk.AddSelfEntryText("N/A");

                        }
                        else
                        {
                            entRmk.AddSelfEntryText(txtUserRmk.Text);
                        }
                    }
                    else
                    {
                        entRmk.AddPreDefRmk( new PreDefRmk(mli.Value, mli.Text ));
                    }
                }
            }
           
                
           
            EaRemarkSaveEventArgs arg = new EaRemarkSaveEventArgs(entRmk);
            RemarkSaveClick(this, arg);
        }
        catch (Exception)
        {

        }
    }

    protected void slstPreDefRmk_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Console.WriteLine("");
    }


    // add a delegate
    public delegate void RemarkBackClickEventHandler(object sender,
        EventArgs e);

    // add an event of the delegate type       
    public event RemarkBackClickEventHandler RemarkBack_Click;

    protected void cmdBagRmkBack_Click(object sender, EventArgs e)
    {
        try
        {
            RemarkBack_Click(this, e);
        }
        catch (Exception)
        {
        }
    }
}
