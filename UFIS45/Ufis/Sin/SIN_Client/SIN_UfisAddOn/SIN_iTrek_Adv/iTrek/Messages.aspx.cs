using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using iTrekSessionMgr;
using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;
using UFIS.Wap.ITrek.WebMgr;
using UFIS.ITrekLib.ENT;

public partial class Messages : System.Web.UI.MobileControls.MobilePage
{

	protected void Page_Load(object sender, EventArgs e)
	{
		string uId = "";
		MgrSec.IsAuthenticatedUser(this, Session, out uId);
		frmInbox.ControlToPaginate = lstInMsgs;
		try
		{
			if (!IsPostBack)
			{
				if (Request.QueryString.Count > 0)
				{
					if (Request.QueryString["newmessage"].ToString() == "yes")
					{
						getNewMessageInfo();

						//txtFreeMsg.Text =selFlno;
						ActiveForm = frmNewMessage;
					}
					else if (Request.QueryString["newmessage"].ToString() == "alert")
					{
						getInBoxMessages();
					}
					else if (Request.QueryString["newmessage"].ToString() == "flight")
					{
						getFlightMessages();
					}
				}
			}
		}
		catch (Exception ex)
		{
			// iTrekUtils.iTUtils.LogToEventLog("message page load" + ex.Message, "iTrekError");
		}
	}



	protected void lstMsgMenu_ItemCommand(object sender, ListCommandEventArgs e)
	{
		SessionManager sessMan = new SessionManager();
		ArrayList lstInOutMessage = new ArrayList();

		//int cnt=dsPDM.PREDEFMSG.Rows.Count;
		//DSPreDefMsg.PREDEFMSGRow pdmRow= DSPreDefMsg.PREDEFMSGRow("000000","Create Free Text");
		//dsPDM.PREDEFMSG.AddPREDEFMSGRow( pdmRow);
		//dsPDM.PREDEFMSG.MSGPARTColumn.Expression=dsPDM.PREDEFMSG.MSGTEXTColumn.

		if (e.ListItem.Text == "NEW")
		{
			try
			{
				DSPreDefMsg dsPDM = CtrlPreDefMsg.RetrieveActivePredifinedMsg();
				lstNewMsg.DataSource = dsPDM;
				lstNewMsg.DataMember = "PREDEFMSG";
				lstNewMsg.DataTextField = "MSGTEXT";
				lstNewMsg.DataValueField = "URNO";
				lstNewMsg.DataBind();
				//Hashtable htPredefinedMsgs = new Hashtable();
				//htPredefinedMsgs = sessMan.getPredefinedMessages();
				//Session["PreDefinedMsg"] = htPredefinedMsgs;
				//IDictionaryEnumerator preDefenum = htPredefinedMsgs.GetEnumerator();

				//MobileListItem mb;
				//lstNewMsg.Items.Add("Create Free");
				//while (preDefenum.MoveNext())
				//{
				//    mb = new MobileListItem();
				//    mb.Text = preDefenum.Value.ToString().Substring(0, 5);
				//    mb.Value = preDefenum.Key.ToString();
				//    lstNewMsg.Items.Add(mb);   

				//}
			}
			catch (Exception ex)
			{
			}
			ActiveForm = frmNewMessage;
		}
		else
			if (e.ListItem.Text == "INBOX")
			{
				getInBoxMessages();
			}
			else
				if (e.ListItem.Text == "OUTBOX")
				{
					lblInfo.Visible = false;
					lstInMsgs.Items.Clear();
					DSMsg dsMsg = CtrlMsg.RetrieveOutMessages(MgrSess.GetUser(Session));
					MobileListItem mb;
					int outMsgCnt = dsMsg.MSG.Count;
					string recList = "";
                    
					if (outMsgCnt != 0)
					{
						foreach (DSMsg.MSGRow msgRow in dsMsg.MSG.Select("", "SENT_DT DESC"))
						{
                            string seperator = string.Empty;
							recList = msgRow["MSG_DISP"].ToString();
							foreach (System.Data.DataRow childRow in msgRow.GetChildRows("MSG_MSG_TO"))
							{
                                //if (childRow["TP"].ToString() == "" || childRow["TP"].ToString() == null)
                                //{
                                //    // recList += childRow["MSG_TO_ID"].ToString() + ",";
                                //    recList += childRow["TONM"].ToString() + "(" + childRow["MSG_TO_ID"].ToString() + "),";
                                //}
                                //else
                                //{
                                //    recList += childRow["TONM"].ToString();
                                //}

                                if ((!string.IsNullOrEmpty(childRow["TP"].ToString())) && (childRow["TP"].ToString() == "F"))
                                {//Flight Recepient
                                    if (string.IsNullOrEmpty(childRow["TONM"].ToString()))
                                        recList += seperator + childRow["MSG_TO_ID"].ToString();
                                    else
                                        recList += seperator + childRow["TONM"].ToString();
                                }
                                else
                                {
                                    recList += seperator + childRow["MSG_TO_ID"].ToString();
                                }
                                seperator = ",";
							}
                            //recList = recList.Remove(recList.Length - 1, 1);
                            recList = recList.TrimEnd(',');
							mb = new MobileListItem(recList, msgRow["URNO"].ToString());
							lstInMsgs.Items.Add(mb);
							recList = "";
						}
					}
					else
					{
						lblInfo.Text = "No Messages";
						lblInfo.Visible = true;
					}
					//DataView dv = dsMsg.MSG.DefaultView;
					//lblInfo.Visible = false;
					//if (dv.Count == 0)
					//{

					//    lblInfo.Text = "No Messages";
					//    lblInfo.Visible = true;
					//}
					//else
					//{
					//    dv.Sort = "SENT_DT DESC";
					//    //int tempCnt= dsMsg.MSG_TO.MSG_TOColumn.
					//    //lstInMsgs.DataSource = dsMsg;
					//    lstInMsgs.DataSource = dv;
					//    lstInMsgs.DataMember = "MSG";
					//    lstInMsgs.DataTextField = "MSG_DISP";
					//    lstInMsgs.DataValueField = "URNO";
					//    lstInMsgs.DataBind();
					//}
					//lstInOutMessage = sessMan.getOutMessagesforStaffId(Session["staffid"].ToString());
					//MobileListItem mb;
					//foreach (Message msg in lstInOutMessage)
					//{
					//    mb = new MobileListItem(msg.SendDateTime + msg.MsgBody.Substring(0, 5) + "   " + msg.FromName + "(" + msg.FromId + " )", msg.MsgUrno);
					//    lstInMsgs.Items.Add(mb);
					//}
					lblHdng3.Text = "OUTBOX";
					ActiveForm = frmInbox;
				}
	}

	protected void lstNewMsg_ItemCommand(object sender, ListCommandEventArgs e)
	{
		DSPreDefMsg dsPDM = CtrlPreDefMsg.RetrieveActivePredifinedMsg();
		//lstNewMsg.DataSource.ToString();
		if (e.ListItem.Index == 0)
		{
			try
			{
				if (Request.QueryString["newmessage"].ToString() == "yes")
				{
					txtFreeMsg.Text = MgrSess.GetFlight(Session).FLNO + ": ";
					lstFlight.Items.Add(new MobileListItem("Send to Flight", MgrSess.GetFlight(Session).URNO));
					lstFlight.Visible = true;
				}
				else
				{
					txtFreeMsg.Text = "";
				}
			}
			catch (Exception ex)
			{
				txtFreeMsg.Text = "";
			}
		}
		else
		{
			DataRow[] dROW = dsPDM.PREDEFMSG.Select("URNO='" + e.ListItem.Value + "'");
			string test = dROW[0][1].ToString();
			int arCnt = dROW.Length;
			/* Get the Predefined Message*/
			//((Fli/t)((Hashtable)(Session["FlightsList"]))[selIndex]);
			//string msgValue = e.ListItem.Value;
			// string  predefinedMessage = (string)(((Hashtable)Session["PreDefinedMsg"])[msgValue]);
			try
			{
				if (Request.QueryString["newmessage"].ToString() == "yes")
				{
					txtFreeMsg.Text = MgrSess.GetFlight(Session).FLNO + ": " + test;
					lstFlight.Items.Add(new MobileListItem("Send to Flight", MgrSess.GetFlight(Session).URNO));
					lstFlight.Visible = true;
				}
				else
				{
					txtFreeMsg.Text = test;
				}
			}
			catch (Exception ex1)
			{
				txtFreeMsg.Text = test;
			}
		}

		ActiveForm = frmFreeMsg;
	}
	/*
	 * Get the Message from TextBox and the list of receipients from checkbox
	 * 
	 * 
	 * */
	protected void cmdSendMsg_Click(object sender, EventArgs e)
	{
		SessionManager sessMan = new SessionManager();

		string message = txtFreeMsg.Text;
		ArrayList lstRecAddresses = new ArrayList();
		lblErrMsg.Text = "";
		if (chkAddresses.Selection == null && lstFlight.Selection == null)
		{
			lblErrMsg.Text = "Please Select a receipient";
			lblErrMsg.Visible = true;
			ActiveForm = frmFreeMsg;
		}
		else if (message.Trim() == "")
		{
			lblErrMsg.Text = "Please enter a message.";
			lblErrMsg.Visible = true;
			ActiveForm = frmFreeMsg;
		}
		else
		{
			for (int i = 0; i < 5; i++)
			{
				if (chkAddresses.Items[i].Selected)
				{
					if (chkAddresses.Items[i].Value != "All")
					{
						lstRecAddresses.Add(chkAddresses.Items[i].Value);
					}
					else
					{
						lstRecAddresses.Clear();
						lstRecAddresses.Add("ACC");
						lstRecAddresses.Add("BCC");
						lstRecAddresses.Add("SOCC");
						lstRecAddresses.Add("LCC");
						if (Request.QueryString["newmessage"] != null)
						{
							if (Request.QueryString["newmessage"].ToString() == "yes" && MgrSess.GetFlight(Session) != null)
							{
								lstRecAddresses.Add("F!" + MgrSess.GetFlight(Session).URNO);
							}
						}
					}
				}
			}
			if (lstFlight.Selection != null)
			{
				if (lstFlight.Items[0].Selected)
				{
					lstRecAddresses.Add("F!" + MgrSess.GetFlight(Session).URNO);
				}
			}
			try
			{
				CtrlMsg.SendMessage(lstRecAddresses, MgrSess.GetUser(Session), Session["staffName"].ToString(), message);
			}
			catch (Exception)
			{
				lblErrMsg.Text = "Unsuccessful in sending message.";
			}

			try
			{
				if (Request.QueryString["newmessage"].ToString() == "yes")
				{
					if (Session["ORGCODE"].ToString().Substring(0, 1) == "A")
					{
						RedirectToMobilePage("Menu.aspx?MENU=SELFLT");
					}
					else
					{
						RedirectToMobilePage("Baggage.aspx?MENU=SELFLT");
					}
				}
				else
				{
					ActiveForm = frmMessaages;
				}
			}
			catch (Exception ex)
			{
				ActiveForm = frmMessaages;
			}
		}
	}


	protected void lstInMsgs_ItemCommand(object sender, ListCommandEventArgs e)
	{
		//string testSt=((List)sender).DataSource.ToString();
		//string msgUrno = e.ListItem.Value;
		SessionManager sessMan = new SessionManager();
		Message msg = null;
		string displayStrForHdr = "";
		string msgBody = "";
		if (lblHdng3.Text == "INBOX")
		{
			displayStrForHdr = "From:";
			// DSMsg dsMsg = CtrlMsg.RetrieveInMessages(Session["staffid"].ToString());
			DSMsg dsMsg = CtrlMsg.RetrieveInMessageDetailsForUrno(e.ListItem.Value);

			DSMsg.MSGRow[] dROW = (DSMsg.MSGRow[])dsMsg.MSG.Select();

			DSMsg.MSGRow msgRow = dROW[0];

			DSMsg.MSGTORow[] dChildRow = (DSMsg.MSGTORow[])dsMsg.MSGTO.Select();
			DSMsg.MSGTORow msgToRow = dChildRow[0];

			msgBody = msgRow.MSG_BODY.ToString();
			//msg=sessMan.getMessageDetailsforUrno(msgUrno, true);

			//displayStrForHdr=msg.FromName+" ( "+msg.FromId+" )";
			displayStrForHdr += msgRow.SENT_BY_NAME + " (" + msgRow.SENT_BY_ID + ")";
			Session["MsgToUrno"] = msgToRow.URNO;
			//Session["MessageReceived"] = dChildRow[0][3].ToString();
			Session["MessageReceived"] = msgToRow.RCV_BY;
		}
		else
		{
			if (lblHdng3.Text == "OUTBOX")
			{
				displayStrForHdr = "To:";

				//DSMsg dsMsg = CtrlMsg.RetrieveOutMessages(Session["staffid"].ToString());
				DSMsg dsMsg = CtrlMsg.RetrieveOutMessageDetailsForUrno(e.ListItem.Value);
				//DataRow[] dROW = dsMsg.MSG.Select("URNO='" + e.ListItem.Value + "'");
				DSMsg.MSGRow[] dROW = (DSMsg.MSGRow[])dsMsg.MSG.Select();
				DSMsg.MSGTORow[] dROWChild = (DSMsg.MSGTORow[])dsMsg.MSGTO.Select();
				DSMsg.MSGRow msgRow = dROW[0];

				//DataRow[] dROWChild = dsMsg.MSGTO.Select("MSG_URNO='" + e.ListItem.Value + "'");
				//DataRow[] dROWChild = dsMsg.MSGTO.
				msgBody = msgRow.MSG_BODY;
                string seperator = string.Empty;
				foreach (DSMsg.MSGTORow msgToRow in dROWChild)
				{
					// displayStrForHdr += msgToRow[2].ToString() + ",";
                    //=======================
                    //if (msgToRow.TP == "" || msgToRow.TP == null)
                    //{
                    //    displayStrForHdr += msgToRow.TONM + "(" + msgToRow.MSG_TO_ID + "),";
                    //}
                    //else
                    //{
                    //    displayStrForHdr += msgToRow.TONM;
                    //}

                    //=======================
                    if ((!string.IsNullOrEmpty(msgToRow.TP)) && (msgToRow.TP == "F"))
                    {//Flight Recepient
                        if (string.IsNullOrEmpty(msgToRow.TONM))
                            displayStrForHdr += seperator + msgToRow.MSG_TO_ID;
                        else
                            displayStrForHdr += seperator + msgToRow.TONM;
                    }
                    else
                    {
                        displayStrForHdr += seperator + msgToRow.MSG_TO_ID;
                    }
                    seperator = ",";
                    //=======================

				}
                displayStrForHdr = displayStrForHdr.TrimEnd(',');
				//displayStrForHdr = displayStrForHdr.Remove(displayStrForHdr.Length - 1, 1);
				//    msg = sessMan.getMessageDetailsforUrno(msgUrno, false);
				//    foreach(string rcv in msg.LstReceipients)
				//{
				//    displayStrForHdr += rcv + ",";
				//}
				//displayStrForHdr=displayStrForHdr.Remove(displayStrForHdr.Length-1 , 1);
			}
		}
		lblMsgHdr.Text = displayStrForHdr;

		//txtRecMsg.Text = msg.SendDateTime + " " + msg.MsgBody;
		txtRecMsg.Text = msgBody;
		ActiveForm = frmRecMsg;
	}

	protected void lstNewMsg_DataBinding(object sender, EventArgs e)
	{
	}

	protected void cmdCloseMsg_Click(object sender, EventArgs e)
	{
		SessionManager sessMan = new SessionManager();
		string curDtTime = "";
		if (lblHdng3.Text == "INBOX")
		{
			if (Session["MessageReceived"].ToString().Trim().Length == 0)
			{
				//Change to update in the iTrek DB instead of sending to back-end
				//20 Jul 2009 - by AM
				//sessMan.updateMessageReceivedDetails(Session["MsgToUrno"].ToString(), MgrSess.GetUser(Session));				
				CtrlMsg.UpdateMessageReceivedDetails(Session["MsgToUrno"].ToString(), MgrSess.GetUser(Session), null); 
			}
		}
		ActiveForm = frmInbox;
	}


	private void getNewMessageInfo()
	{
		DSPreDefMsg dsPDM = CtrlPreDefMsg.RetrieveActivePredifinedMsg();
		lstNewMsg.DataSource = dsPDM;
		lstNewMsg.DataMember = "PREDEFMSG";
		lstNewMsg.DataTextField = "MSGTEXT";
		lstNewMsg.DataValueField = "URNO";
		lstNewMsg.DataBind();
	}

	private void getInBoxMessages()
	{
		string jobCat = "";
		if (Session["ORGCODE"].ToString().Substring(0, 1) == "A")
		{
			jobCat = "AO";
		}
		else
		{
			jobCat = "BO";
		}
            
        //Commented By Nyan on 3 August 2009 
        //DSMsg dsMsg = CtrlMsg.RetrieveInMessages(MgrSess.GetUser(Session), jobCat);
        ArrayList list = new ArrayList();
        list.Add(jobCat);
        //50 = number of Maximum message;
        DSMsg dsMsg = CtrlMsg.RetrieveInMessages(MgrSess.GetUser(Session), list, 50, EnComposeMsgType.Personal);


		DataView dv = dsMsg.MSGTO.DefaultView;

		lblInfo.Visible = false;
		lstInMsgs.Items.Clear();
		if (dv.Count == 0)
		{
			lblInfo.Text = "No Messages";
			lblInfo.Visible = true;
		}
		else
		{
			dv.Sort = "SENT_DT DESC";
			//int tempCnt = dsMsg.MSGTO.Count;
			//lstInMsgs.DataSource = dsMsg;
			lstInMsgs.DataSource = dv;
			lstInMsgs.DataMember = "MSGTO";
			lstInMsgs.DataTextField = "MSG_DISP";
			lstInMsgs.DataValueField = "URNO";
			lstInMsgs.DataBind();
		}
		//lstInOutMessage = sessMan.getInMessagesforStaffId(Session["staffid"].ToString());
		////lstInMessage.Add("Proceed");
		//MobileListItem mb;
		//foreach (Message msg in lstInOutMessage)
		//{
		//    mb = new MobileListItem(msg.SendDateTime+msg.MsgBody.Substring(0, 5) +"   "+msg.FromName+"("+msg.FromId +" )", msg.MsgUrno);
		//    lstInMsgs.Items.Add(mb);
		//}
		lblHdng3.Text = "INBOX";
		ActiveForm = frmInbox;
	}

	protected void cmdBack_Click(object sender, EventArgs e)
	{
		if (Session["ORGCODE"].ToString().Substring(0, 1) == "A")
		{
			RedirectToMobilePage("Menu.aspx");
		}
		else
		{
			RedirectToMobilePage("Baggage.aspx");
		}
	}

	protected void cmdBack1_Click(object sender, EventArgs e)
	{
		try
		{
			if (Request.QueryString.Count > 0)
			{
				if (Request.QueryString["newmessage"].ToString() == "yes")
				{
					if (Session["ORGCODE"].ToString().Substring(0, 1) == "A")
					{
						RedirectToMobilePage("Menu.aspx?MENU=SELFLT");
					}
					else
					{
						RedirectToMobilePage("Baggage.aspx?MENU=SELFLT");
					}
				}
				else
					if (Request.QueryString["newmessage"].ToString() == "flight")
					{
						if (Session["ORGCODE"].ToString().Substring(0, 1) == "A")
						{
							RedirectToMobilePage("Menu.aspx?MENU=SELFLT");
						}
						else
						{
							RedirectToMobilePage("Baggage.aspx?MENU=SELFLT");
						}
					}
					else
					{
						ActiveForm = frmMessaages;
					}
			}
			else
			{
				ActiveForm = frmMessaages;
			}
		}
		catch (Exception ex)
		{
			ActiveForm = frmMessaages;
		}
	}

	protected void cmdBack2_Click(object sender, EventArgs e)
	{
		ActiveForm = frmNewMessage;
	}

	protected void cmdBack3_Click(object sender, EventArgs e)
	{
		string selection = "";

		try
		{
			selection = Request.QueryString["newmessage"].ToString();
		}
		catch (Exception ex)
		{
			selection = "";
		}
		if (selection == "flight")
		{
			if (Session["ORGCODE"].ToString().Substring(0, 1) == "A")
			{
				RedirectToMobilePage("Menu.aspx?MENU=SELFLT");
			}
			else
			{
				RedirectToMobilePage("Baggage.aspx?MENU=SELFLT");
			}
		}
		else
		{
			ActiveForm = frmMessaages;
		}
	}

	protected override void OnViewStateExpire(EventArgs e)
	{
		iTrekUtils.iTUtils.LogToEventLog("ViewState Expired in Messages", "iTrekError");
		RedirectToMobilePage("Login.aspx");
	}


	private void getFlightMessages()
	{
		lblInfo.Visible = false;
		lstInMsgs.Items.Clear();
		DSMsg dsMsg = CtrlMsg.RetrieveInMsgForFlight(MgrSess.GetFlight(Session).URNO, MgrSess.GetFlight(Session).TURN);
		MobileListItem mb;
		int outMsgCnt = dsMsg.MSG.Count;
		string recList = "";

		if (outMsgCnt != 0)
		{
			foreach (DSMsg.MSGRow msgRow in dsMsg.MSG.Select("", "SENT_DT DESC"))
			{
				string msgToUrno = "";
				recList = msgRow["MSG_DISP"].ToString();
				foreach (System.Data.DataRow childRow in msgRow.GetChildRows("MSG_MSG_TO"))
				{
					if (childRow["TP"].ToString() == "" || childRow["TP"].ToString() == null)
					{
						// recList += childRow["MSG_TO_ID"].ToString() + ",";
						recList += childRow["TONM"].ToString() + "(" + childRow["MSG_TO_ID"].ToString() + "),";
					}
					else
					{


						recList += childRow["TONM"].ToString();
					}
					msgToUrno = childRow["URNO"].ToString();
				}
				recList = recList.Remove(recList.Length - 1, 1);
				//    mb = new MobileListItem(recList, msgRow["URNO"].ToString());
				mb = new MobileListItem(recList, msgToUrno);
				lstInMsgs.Items.Add(mb);
				recList = "";
			}
		}
		else
		{
			lblInfo.Text = "No Messages";
			lblInfo.Visible = true;

		}
		//DataView dv = dsMsg.MSG.DefaultView;
		//lblInfo.Visible = false;
		//if (dv.Count == 0)
		//{

		//    lblInfo.Text = "No Messages";
		//    lblInfo.Visible = true;
		//}
		//else
		//{
		//    dv.Sort = "SENT_DT DESC";
		//    //int tempCnt= dsMsg.MSG_TO.MSG_TOColumn.
		//    //lstInMsgs.DataSource = dsMsg;
		//    lstInMsgs.DataSource = dv;
		//    lstInMsgs.DataMember = "MSG";
		//    lstInMsgs.DataTextField = "MSG_DISP";
		//    lstInMsgs.DataValueField = "URNO";
		//    lstInMsgs.DataBind();
		//}
		//lstInOutMessage = sessMan.getOutMessagesforStaffId(Session["staffid"].ToString());
		//MobileListItem mb;
		//foreach (Message msg in lstInOutMessage)
		//{
		//    mb = new MobileListItem(msg.SendDateTime + msg.MsgBody.Substring(0, 5) + "   " + msg.FromName + "(" + msg.FromId + " )", msg.MsgUrno);
		//    lstInMsgs.Items.Add(mb);
		//}
		lblHdng3.Text = "INBOX";
		ActiveForm = frmInbox;
	}
}