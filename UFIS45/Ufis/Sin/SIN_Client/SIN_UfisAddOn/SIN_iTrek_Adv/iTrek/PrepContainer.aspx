<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PrepContainer.aspx.cs" Inherits="WAP_PrepContainer" %>

<%@ Register Src="MWUCpmManual.ascx" TagName="MWUCpmManual" TagPrefix="uc1" %>
<%@ Register Src="MWUBagRemk.ascx" TagName="MWUBagRemk" TagPrefix="uc2" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmPrepContainer" runat="server">
        <uc1:MWUCpmManual ID="mwuCpmManual1" runat="server" HasChkBox="false" HasLink="true"
            LinkText="Remarks" NumOfEntry="10" />
        <mobile:Command ID="cmdPrepContainerSave" Runat="server">Save</mobile:Command>
        <mobile:Command ID="cmdPrepContainerBack" Runat="server">Back</mobile:Command>

    </mobile:Form>
    
</body>
</html>
