using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using UFIS.ITrekLib.Ctrl;
using MM.UtilLib;
using System.IO;



	/// <summary>
	/// Summary description for Global.
	/// </summary>
	public class Global : System.Web.HttpApplication
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		

		public Global()
		{
			
		}


        private static UtilTraceLog _traceLog = UtilTraceLog.GetInstance();

        protected void Application_Start(Object sender, EventArgs e)
        {
            // Code that runs on application startup
            string baseDir = AppDomain.CurrentDomain.BaseDirectory;
            DirectoryInfo directoryInfo =
                         Directory.GetParent(baseDir);
            baseDir = directoryInfo.FullName;
            directoryInfo =
                         Directory.GetParent(baseDir);
            baseDir = directoryInfo.FullName;

            string logDir = Path.Combine(Path.Combine(baseDir, "XML"), "LOG");
            if (logDir[logDir.Length - 1] != '\\') logDir += "\\";
            LogTraceMsg(logDir);
            _traceLog.SetTrace(true);
            _traceLog.SetLogDir(logDir);
            _traceLog.SetLogFileName("Handheld");
            _traceLog.LogTraceMsg("Global", "Start");

            LogTraceMsg("Appstart");
            LogMsg("Web Applicatioin Start");            
            iTrekUtils.iTUtils.LogToEventLog("Application_Start ", "ItrekGlobal");
            //// Store the time to request a new token before the current one expires

        }

        private void LogTraceMsg(string msg)
        {
            UFIS.ITrekLib.Util.UtilLog.LogToTraceFile("HH-App", msg);
        }
        void LogMsg(string msg)
        {
            //iTrekUtils.iTUtils.LogToEventLog(msg);
            UFIS.ITrekLib.Util.UtilLog.LogToGenericEventLog("HH-App", msg);
        }
 
		protected void Session_Start(Object sender, EventArgs e)
		{
            string SID = "";
            try
            {
                
                SID = Request.Headers["x-up-subno"].ToString();
                SID = SID.Replace("_WAPGW.grid.net.sg", "");
            }
            catch (Exception)
            {

                //throw;
            }
            // Code that runs when a new session is started
            iTrekUtils.iTUtils.LogToEventLog("Session_Start" + "SessionID = " + Session.SessionID + " DeviceID=" + SID, "ItrekGlobal");
            //iTrekUtils.iTUtils.LogToEventLog("Session Time Out" + Session.Timeout.ToString(), "ItrekGlobal");
		}


		protected void Application_BeginRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_EndRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_AuthenticateRequest(Object sender, EventArgs e)
		{

		}

		protected void Application_Error(Object sender, EventArgs e)
		{
            iTrekUtils.iTUtils.LogToEventLog("Application_Error ", "ItrekGlobal");

		}

		protected void Session_End(Object sender, EventArgs e)
		{
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.
            try
            {
              iTrekXML.iTXMLcl iTXML = new iTrekXML.iTXMLcl();
               iTrekSessionMgr.SessionManager sessMan=new  iTrekSessionMgr.SessionManager();
               iTrekUtils.iTUtils.LogToEventLog("Session_End" + "SessionID = " + Session.SessionID, "ItrekGlobal");
               sessMan.DeleteSessionDirectory(Session.SessionID);
               iTrekUtils.iTUtils.LogToEventLog("Delete Session Directory" + "SessionID = " + Session.SessionID, "ItrekGlobal");
               CtrlCurUser.DeleteDeviceIdEntryBySessID(Session.SessionID);
                //iTXML.DeleteDeviceIDEntryBySessID(Session.SessionID);
              
              
            }
            catch (Exception ex)
            {

                iTrekUtils.iTUtils.LogToEventLog("Session_End_Exception"+ex.Message, "ItrekGlobal");
            
            }

		}



		protected void Application_End(Object sender, EventArgs e)
		{
            iTrekUtils.iTUtils.LogToEventLog("Application_End ", "ItrekGlobal");

		}
			
		#region Web Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		
		#endregion
	}


