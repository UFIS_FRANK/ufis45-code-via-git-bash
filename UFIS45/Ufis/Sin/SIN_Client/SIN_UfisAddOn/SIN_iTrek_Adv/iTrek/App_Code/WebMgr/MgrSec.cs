using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.SessionState;

namespace UFIS.Wap.ITrek.WebMgr
{

    /// <summary>
    /// Summary description for MgrSec
    /// </summary>
    public class MgrSec
    {
        private MgrSec()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public static bool IsAuthenticatedUser(System.Web.UI.MobileControls.MobilePage page, HttpSessionState session, out string userId)
        {
            bool auth = false;
            userId = "";
            try
            {
                userId = MgrSess.GetUser(session);
            }
            catch (Exception)
            {
                page.RedirectToMobilePage("~/Login.aspx", true);
            }
            if ((userId != null) && (userId != ""))
            {
                auth = true;
            }
            else
            {
                page.RedirectToMobilePage("~/Login.aspx", true);
            }
            return auth;
        }

        public static void SetAuthenticateUser(HttpSessionState session, string userId)
        {
            MgrSess.SetUser(session,userId);
        }
    }

}