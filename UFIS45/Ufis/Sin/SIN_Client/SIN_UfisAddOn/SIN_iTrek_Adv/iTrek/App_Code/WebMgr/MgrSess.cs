using System;
using System.Collections;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Web.SessionState;
using UFIS.ITrekLib.ENT;
using iTrekSessionMgr;
using iTrekXML;

namespace UFIS.Wap.ITrek.WebMgr
{

    /// <summary>
    /// Summary description for MgrSess
    /// </summary>
    public class MgrSess
    {
        private MgrSess()
        {
        }

        #region Current Flight No
        private const string SESS_CUR_FLIGHT_URNO = "SESS_CUR_FLIGHT_URNO";
        public static void SetCurrentFlightUrno(HttpSessionState session, string flightUrNo)
        {
            session[SESS_CUR_FLIGHT_URNO] = flightUrNo;
        }
        public static string GetCurrentFlightUrno(HttpSessionState session)
        {
            string flightUrno = "";
            try
            {
                flightUrno = session[SESS_CUR_FLIGHT_URNO].ToString();
            }
            catch (Exception)
            {
            }
            return flightUrno;
        }
        #endregion

        #region Apron Arrival CPM Auto Information
        const string SESS_AP_ARR_CPM_AUTO = "SESS_AP_ARR_CPM_AUTO";
        public static void SetApronArrivalCPMAutoList(HttpSessionState session, EntCPMList cpmList)
        {
            session[SESS_AP_ARR_CPM_AUTO] = cpmList;
        }
        public static EntCPMList GetApronArrivalCPMAutoList(HttpSessionState session)
        {
            EntCPMList cpmList = null;
            try
            {
                cpmList = (EntCPMList)session[SESS_AP_ARR_CPM_AUTO];
            }
            catch (Exception)
            {
            }
            return cpmList;
        }

        #endregion

        #region Apron Arrival CPM Manual Information
        const string SESS_AP_ARR_CPM_MAN = "SESS_AP_ARR_CPM_MAN";
        public static void SetApronArrivalCPMManualList(HttpSessionState session, EntCPMList cpmList)
        {
            session[SESS_AP_ARR_CPM_MAN] = cpmList;
        }
        public static EntCPMList GetApronArrivalCPMManualList(HttpSessionState session)
        {
            EntCPMList cpmList = null;
            try
            {
                cpmList = (EntCPMList)session[SESS_AP_ARR_CPM_MAN];
            }
            catch (Exception)
            {
            }
            return cpmList;
        }

        #endregion

        #region Flight Info to Show on the top
        const string SESS_FLIGHT_INFO = "SESS_FLIGHT_INFO";
        public static void SetFlightInfo(HttpSessionState session, string info)
        {
            session[SESS_FLIGHT_INFO] = info;
        }
        public static string GetFlightInfo(HttpSessionState session)
        {
            string info = ""; //
            try
            {
                info = session[SESS_FLIGHT_INFO].ToString();
            }
            catch (Exception)
            {
            }
            return info;
        }
        #endregion
        #region Flight Object to Show on the top
        const string SESS_SELECTED_FLIGHT = "SESS_SELECTED_FLIGHT";
        public static void SetFlight(HttpSessionState session, Flight selFlt)
        {
            session[SESS_SELECTED_FLIGHT] = selFlt;
        }
        public static Flight GetFlight(HttpSessionState session)
        {
            Flight selFlt = null; 
            try
            {
                selFlt =(Flight) session[SESS_SELECTED_FLIGHT];
            }
            catch (Exception)
            {
            }
            return selFlt;
        }
        #endregion
        #region UserId
        const string SESS_USER_ID = "SESS_USER_ID";
		/// <summary>
		/// Set the Current Login UserId
		/// </summary>
		/// <param name="session"></param>
		/// <param name="userId"></param>
        public static void SetUser(HttpSessionState session,string userId)
        {
            session[SESS_USER_ID] = userId;
        }

		/// <summary>
		/// Get the current Login UserId
		/// </summary>
		/// <param name="session"></param>
		/// <returns></returns>
        public static string GetUser(HttpSessionState session)
        {
            string userId = "";
            try
            {
                userId = session[SESS_USER_ID].ToString();
            }
            catch (Exception)
            {
            }
            return userId;
        }
        #endregion

        #region RSM
        const string RSM_STAFF = "RSM_STAFF";
        public static void SetRsmStaff(HttpSessionState session, Hashtable rsm)
        {
            session[RSM_STAFF] = rsm;
        }

        public static Hashtable GetRsmStaff(HttpSessionState session)
        {
            Hashtable rsm = null;
            try
            {
                rsm = (Hashtable)session[RSM_STAFF];
            }
            catch(Exception)
            {
            }
            return rsm;
        }
        #endregion

        #region Back URL
        const string BACK_URL = "BACK_URL";
        public static void SetBackUrl(HttpSessionState session, string url)
        {
            session[BACK_URL] = url;
        }

        public static string GetBackUrl(HttpSessionState session)
        {
            string url = string.Empty;
            try
            {
                url = (string)session[BACK_URL];
            }
            catch (Exception)
            {
            }
            return url;
        }
        #endregion

        //#region DepTime
        //const string SESS_DEP_TIME = "SESS_DEP_TIME";
        //public static void SetDepTime(HttpSessionState session, DateTime depTime)
        //{
        //    session[SESS_DEP_TIME] = depTime;
        //}
        //public static DateTime GetDepTime(HttpSessionState session)
        //{
        //    DateTime depTime = DateTime.Now;
        //    try
        //    {
        //        depTime = (DateTime)session[SESS_DEP_TIME];
        //    }
        //    catch (Exception)
        //    {
        //    }
        //    return depTime;
        //}
       // #endregion
       // #region ArrTime
       // const string SESS_ARR_TIME = "SESS_ARR_TIME";
       // public static void SetArrTime(HttpSessionState session, DateTime arrTime)
       // {
       //     session[SESS_ARR_TIME] = arrTime;
       // }
       // public static DateTime GetArrTime(HttpSessionState session)
       // {
       //     DateTime arrTime = DateTime.Now;
       //     try
       //     {
       //         arrTime = (DateTime)session[SESS_ARR_TIME];
       //     }
       //     catch (Exception)
       //     {
       //     }
       //     return arrTime;
       // }
       //#endregion
    }
}
