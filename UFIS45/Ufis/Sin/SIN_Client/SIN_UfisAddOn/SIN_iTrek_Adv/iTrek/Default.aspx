
<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Default" EnableViewState="false" EnableViewStateMac="false"%>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="Form1" runat="server">
        <mobile:Label ID="Label1" Runat="server">Page Cannot Be Found</mobile:Label>
        <br />
        <mobile:Command ID="Command1" Runat="server" OnClick="Command1_Click">Login</mobile:Command>

    </mobile:Form>
</body>
</html>
