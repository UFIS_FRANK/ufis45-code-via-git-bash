#define UsingQueues
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.ENT;
using UFIS.Wap.ITrek.WebMgr;
using iTrekSessionMgr;
using iTrekXML;
using UFIS.ITrekLib.EventArg;
using iTrekWMQ;
using UFIS.ITrekLib.Util;
using MM.UtilLib;
using System.Collections.Generic;

public partial class BaggageHandlingArr : System.Web.UI.MobileControls.MobilePage
{
    private const string PFIX_RMK_LIST = "Rmk (";
    private const string SFIX_RMK_LIST = ")";
    private const string RMK_IND = "**";
    EnTripType trType = EnTripType.None;
    protected void Page_Load(object sender, EventArgs e)
    {
        string uId = "";
        MgrSec.IsAuthenticatedUser(this, Session, out uId);
        MWUTiming1.TimingSendClick += new WAP_MWUTiming.TimingSendHandler(MWUTiming1_TimingSendClick);
        MWU_CompanyHeading1.ShowFlightInfoHeading();
        MWU_CompanyHeading2.ShowFlightInfoHeading();
        MWU_CompanyHeading3.ShowFlightInfoHeading();
        // chkContainerList.DataSource = CtrlTrip.RetrieveTrip(MgrSess.GetFlight(Session).URNO);
        if (!IsPostBack)
        {
          //  this.MWUTiming1.setTextBoxLabel(Request.QueryString["Selected"].ToString());
            getUpdatedUldNumbers();
           
            //lblDisplayFlt.Text = selectedFlight.DISPLAYOPENFLIGHTS;
            //chkContainerList.DataSource = CtrlTrip.RetrieveTrip(selectedFlight.URNO);
            //chkContainerList.DataMember = "TRIP";
            //chkContainerList.DataTextField = "ULDN";
            //chkContainerList.DataBind();
        }
    }
    protected void Command1_Click(object sender, EventArgs e)
    {
        getRemarks();
    }
    protected void cmdBack_Click(object sender, EventArgs e)
    {
        ActiveForm = frmContainerList;
    }
    protected void lstRemarks_ItemCommand(object sender, ListCommandEventArgs e)
    {

        string selValue = e.ListItem.Value;
       
        DSTrip dsTrip = CtrlTrip.RetrieveTripforTripUrno(selectedFlight.URNO, selValue);
        txtVRemarks.Text = dsTrip.TRIP[0]["SENT_RMK"].ToString();
        ActiveForm = frmViewRemarks;

    }
    private EnTripType TripType
    {
        get
        {
            return trType;
        }
        set
        {
            trType = value;
        }
    }

    protected void cmdSendTrip_Click(object sender, EventArgs e)
    {
        if (txtVFlightDetailsTiming.Text == selectedFlight.DISPLAYBOFLIGHTMENU)
        {
            int chkCount = chkContainerList.Items.Count;
            List<string> lstChkdContainer = new List<string>();            
            bool isTrip = false;
            string tripNo = "";
            string conturno = "";
            string value = "";


            if (chkContainerList.Selection == null)
            {
                lblErrMsg.Text = "Please Select a Container";
                lblErrMsg.Visible = true;
                //ActiveForm = frmFreeMsg;
            }
            else
            {
                for (int i = 0; i < chkCount; i++)
                {
                    try
                    {
                        // iTrekUtils.iTUtils.LogToEventLog("chkContainerList.Items[i].Value" + chkContainerList.Items[i].Value, "iTrekError");
                        conturno = chkContainerList.Items[i].Value.Substring(chkContainerList.Items[i].Value.IndexOf(":", 0) + 1).Replace(":", "").Trim();
                        value = chkContainerList.Items[i].Value.Substring(0, (chkContainerList.Items[i].Value.IndexOf(":", 0)));
                    }
                    catch (Exception ex)
                    {
                        iTrekUtils.iTUtils.LogToEventLog("urno" + ex.Message, "iTrekError");
                        value = chkContainerList.Items[i].Value;

                    }
                    // iTrekUtils.iTUtils.LogToEventLog("urno" + conturno, "iTrekError");
                    //  iTrekUtils.iTUtils.LogToEventLog("urno" + value, "iTrekError");
                    //   iTrekUtils.iTUtils.LogToEventLog("tripNo"+tripNo, "iTrekError");
                    if (isTrip)
                    {
                        if (value == tripNo)
                        {
                            //   iTrekUtils.iTUtils.LogToEventLog("add" + conturno, "iTrekError");
                            lstChkdContainer.Add(conturno);
                        }
                        else
                        {

                            isTrip = false;

                        }
                    }
                    if (!isTrip)
                    {
                        if (chkContainerList.Items[i].Selected)
                        {
                            if (chkContainerList.Items[i].Text == chkContainerList.Items[i].Value)
                            {
                                isTrip = true;
                                tripNo = chkContainerList.Items[i].Text;

                            }
                            else
                            {

                                //  iTrekUtils.iTUtils.LogToEventLog("conturno" + conturno, "iTrekError");
                                lstChkdContainer.Add(conturno);
                                //  iTrekUtils.iTUtils.LogToEventLog("add in !isTrip" + conturno, "iTrekError");
                                //iTrekUtils.iTUtils.LogToEventLog("urno" + ((DataRow)(chkContainerList.Items[i].DataItem))["URNO"].ToString(), "iTrekError");
                            }
                        }
                    }

                }
                Session["CHECKED_CONTAINER"] = lstChkdContainer;
                //iTrekUtils.iTUtils.LogToEventLog("urno" + lstChkdContainer., "iTrekError");
                txtVFlightDetailsTiming.Text = selectedFlight.DISPLAYBOFLIGHTMENU;
                this.MWUTiming1.setTextBoxLabel(Request.QueryString["Selected"].ToString());
                lblSelectedOption.Text = MWUTiming1.getTextBoxLabel();

                ActiveForm = frmBOTiming;
            }
        }
        else
        {
            RedirectToMobilePage("Baggage.aspx?MENU=OP");
        }

    }

    void MWUTiming1_TimingSendClick(object sender, EaTextEventArgs e)
    {//Send Trip Information to back end
        string time = e.Text;
        string selectedTime = "";
        string convTime = "Invalid";
        if (checkValidityOfTime(time))
        {
            selectedTime = time.Substring(0, 2) + ":" + time.Substring(2, 2);

            convTime = getPrevDayStatus(selectedTime);


        }
        if (this.MWUTiming1.getTextBoxLabel() == "FTRIP")
        {
            TripType = EnTripType.FirstTrip;
        }
        if (this.MWUTiming1.getTextBoxLabel() == "LTRIP")
        {
            TripType = EnTripType.LastTrip;
        }
        if (this.MWUTiming1.getTextBoxLabel() == "NEXT-TRIP")
        {
            TripType = EnTripType.NextTrip;
            
        }
        try{
                        SendTripData(convTime);
                 

            }
            catch (Exception ex)
            {
                lblTimingAlertMsg.Visible = true;
                lblTimingAlertMsg.Text = "Timing input was not successful.Please try to send the timing again";
            }
        }

    
    private bool checkValidityOfTime(string time)
    {
        DateTime dtselectedTime = DateTime.Now;
        bool dtStatus = true;

        string selectedTime = "";



        try
        {
            selectedTime = time.Substring(0, 2) + ":" + time.Substring(2, 2);
        }
        catch (ArgumentOutOfRangeException exc)
        {

            lblTimingAlertMsg.Visible = true;
            lblTimingAlertMsg.Text = "Invalid Time.Please enter time between 0000 - 2359";
            dtStatus = false;
        }
        try
        {
            dtselectedTime = Convert.ToDateTime(selectedTime);
        }
        catch (FormatException ex)
        {
            lblTimingAlertMsg.Visible = true;
            lblTimingAlertMsg.Text = "Invalid Time.Please enter time between 0000 - 2359";
            dtStatus = false;
        }


        return dtStatus;
    }

    private string getPrevDayStatus(string selectedTime)
    {
        bool prevDayStatus = false;
        string time = "";
        DateTime dtselectedTime = DateTime.Now;
        //dtselectedTime = Convert.ToDateTime(selectedTime);
        dtselectedTime = UtilTime.CompDateTimeFromRef(selectedFlight.FLIGHTDATE, selectedTime);
        selectedTime = UtilTime.ConvDateTimeToUfisFullDTString(dtselectedTime);
       // selectedTime = dtselectedTime.ToString("HH:mm");
        DateTime curTime = DateTime.Now;
        if (dtselectedTime > curTime)
        {
            //if (checkTimeforMidNight(dtselectedTime, curTime))
            //{
            //    prevDayStatus = true;
            //    time = getTimeInOracleFormat(prevDayStatus, selectedTime);

            //}
            //else
            //{
                lblTimingAlertMsg.Visible = true;
                lblTimingAlertMsg.Text = "Time entered must be earlier than current time - please re-enter";
                time = "Invalid";
            //}
        }
        else
        {
            time = getTimeInOracleFormat(prevDayStatus, selectedTime);

        }
        return time;
    }

    private bool checkTimeforMidNight(DateTime selTime, DateTime curTime)
    {
        if (curTime.Hour == 0 && selTime.Hour == 23)
        {
            return true;
        }
        return false;
    }
    private string getTimeInOracleFormat(bool prevDayStatus, string selectedTime)
    {
        //iTXMLPathscl iTPaths = new iTXMLPathscl();
        //iTXMLcl iTXML = new iTXMLcl();
        //string TimeInOracleFormat = iTrekUtils.iTUtils.convertStandardDateTimeToOracleFormat(iTXML.ProcessSubmittedTime(selectedTime, prevDayStatus));
        string TimeInOracleFormat = selectedTime;
        return TimeInOracleFormat;
    }
    private string UserId
    {

        get
        {
            string userId = MgrSess.GetUser(Session);
            //userId = MgrSess.GetUserId();
            return userId;
        }
    }

    private void SendTripData(string tripTime)
    {   //Send Trip Information to back end

        CtrlTrip.ReceiveTrip(selectedFlight.URNO, ((List<string>)Session["CHECKED_CONTAINER"]), tripTime, UserId, TripType);
       // CtrlTrip.SaveTrip(selectedFlight.URNO, ((ArrayList)Session["CHECKED_CONTAINER"]), tripTime, UserId, EnTripType.NextTrip, EnTripSendRecv.Receive);
        txtVFlightDetailsTiming.Text = "";

        RedirectToMobilePage("Baggage.aspx?MENU=OP");
    }
    private Flight selectedFlight
    {

        get
        {
            Flight selFlt = MgrSess.GetFlight(Session);
            return selFlt;
        }

    }
    protected void cmdBack3_Click(object sender, EventArgs e)
    {
        getRemarks();
    }
    private void getUpdatedUldNumbers()
    {
        txtVFlightDetailsTiming.Text = selectedFlight.DISPLAYBOFLIGHTMENU;
        lblSelectedOption1.Text = Request.QueryString["Selected"].ToString();
        //Get the list of trips from trips xml
        //Flight selFlt = ((Flight)Session["userFlight"]);
        MobileListItem mb;
        string value;
        ArrayList alULD = new ArrayList();
       
        // DSTrip dsTrip = CtrlTrip.RetrieveTrip(MgrSess.GetFlight(Session).URNO);
        DSTrip dsTrip = CtrlTrip.RetrieveTripNotReceived(selectedFlight.URNO);
        DSTrip dsFullTrip = CtrlTrip.RetrieveTrip(selectedFlight.URNO);
        int balTripCnt = dsTrip.TRIP.Rows.Count;
        int tripcnt = dsFullTrip.TRIP.Rows.Count;
        int maxCnt = 20;
        
        chkContainerList.Items.Clear();
        if (balTripCnt != 0)      
      
        {
            for (int i = 0; i <= tripcnt; i++)
            {

                int j = i + 1;
                value = "TRIP" + j;
                if (dsTrip.TRIP.Select("STRPNO='" + j + "'").Length != 0)
                {

                    mb = new MobileListItem(value, value);
                    chkContainerList.Items.Add(mb);
                   // maxCnt--;
                }
                alULD.Clear();
                //Get ULD in this trip
                foreach (DataRow tripRow in dsTrip.TRIP.Select("STRPNO='" + j + "'"))
                {

                  
                    alULD.Add(tripRow);

                }
                
                foreach (DataRow uldRow in alULD)
                {
                    mb = new MobileListItem(uldRow["ULDN"].ToString(), value+":"+uldRow["URNO"].ToString());
                   // iTrekUtils.iTUtils.LogToEventLog("urno" + ((DataRow)(mb.DataItem))["URNO"].ToString(), "iTrekError");
                    chkContainerList.Items.Add(mb);
                    maxCnt--;

                }

                if (maxCnt <= 0) break;
              

            }
        }
        else

        {

            Command1.Visible = false;
            cmdSendTrip.Visible = false;
            lblErrMsg.Visible = true;
            lblErrMsg.Text = "Sorry,No ULDs entered by the AO";
        
        }

        ActiveForm = frmContainerList;
    }
    protected void cmdBack1_Click(object sender, EventArgs e)
    {
        RedirectToMobilePage("Baggage.aspx?MENU=SELFLT");
    }
    //private void updateFlightTiming(string selectedTime)
    //{
    //    string sendStatus = "";
    //    if (txtVFlightDetailsTiming.Text == selectedFlight.DISPLAYBOFLIGHTMENU)
    //    {
    //    iTXMLPathscl iTPaths = new iTXMLPathscl();
    //    iTXMLcl iTXML = new iTXMLcl();
    //     string selOption = "";        
        
    //   string StaffID = Session["StaffID"].ToString();
       
        
       

    //                if (this.MWUTiming1.getTextBoxLabel() == "FTRIP")
    //                {
    //                    selOption = "FTRIP-B";

    //                }
                    
    //                    else

    //                        if (this.MWUTiming1.getTextBoxLabel() == "LTRIP")
    //                        {
    //                            selOption = "LTRIP-B";

    //                        }

                       
       


    //            if (selectedFlight.ADID == "A")
    //            {
    //                 CtrlFlight.UpdateArrivalFlightTiming(null,MgrSess.GetFlight(Session).URNO, selOption, selectedTime,StaffID);

                  
    //            }
    //            else
    //            {

    //                CtrlFlight.UpdateDepartureFlightTiming(null,MgrSess.GetFlight(Session).URNO, selOption, selectedTime,  StaffID); ;
                   
    //            }

    //        }
    //        else
    //        {
                
    //            RedirectToMobilePage("InvalidPage.aspx");

    //        }
           
        

    //}
    protected void Command2_Click(object sender, EventArgs e)
    {
        getUpdatedUldNumbers();
    }

    private void getRemarks()
    {
        lstRemarks.Items.Clear();

        
        string rmkInd = "";
        DSTrip dsTrip = CtrlTrip.RetrieveTripNotReceived(selectedFlight.URNO);
        int cnt = dsTrip.TRIP.Count;
        MobileListItem mb;

        foreach (DSTrip.TRIPRow trRow in dsTrip.TRIP.Select())
        {
            if (trRow["SENT_RMK"].ToString() != null)
            {
                mb = new MobileListItem(rmkInd + PFIX_RMK_LIST + trRow["ULDN"].ToString() + SFIX_RMK_LIST, trRow["URNO"].ToString());
                lstRemarks.Items.Add(mb);
            }


        }
        //lstRmkList.Items.Add(rmkInd + PFIX_RMK_LIST + cpmSelectedList.GetData(i).ContainerNo + SFIX_RMK_LIST);
        //lstRemarks.DataSource = 
        //lstRemarks.DataMember = "TRIP";
        //lstRemarks.DataTextField = "SENT_RMK";
        //lstRemarks.DataBind();
        ActiveForm = frmRemarkList;
    }

    protected override void OnViewStateExpire(EventArgs e)
    {


        iTrekUtils.iTUtils.LogToEventLog("ViewState Expired in Baggage Handling Arrival", "iTrekError");
        RedirectToMobilePage("Login.aspx");

    }
}
