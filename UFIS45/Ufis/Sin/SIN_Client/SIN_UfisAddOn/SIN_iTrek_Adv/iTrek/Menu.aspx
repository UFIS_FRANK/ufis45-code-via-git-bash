<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Menu.aspx.cs" Inherits="Menu" EnableSessionState="True"%>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>



<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
   <mobile:Form id="frmMenu" runat="server"><mobile:Label ID="lblTitle" Runat="server"
        Alignment="Center" EnableViewState="False">SATS iTrek</mobile:Label><mobile:Label ID="lblMenu" Runat="server"
            Alignment="Center" EnableViewState="False">MAIN MENU</mobile:Label>
        <mobile:Label ID="lblUserIdName" Runat="server" Alignment="Center" Visible="False">
        </mobile:Label>     
      <mobile:Label ID="lblAlertMsg" Runat="server" Alignment="Center" Visible="False" EnableViewState="False">
        </mobile:Label>
        <mobile:List ID="lstMainMenu" Runat="server" OnItemCommand="lstMainMenu_ItemCommand" >
            <Item Text="Open Flights" Value="Open Flights" />
            <Item Text="Closed Flights" Value="Closed Flights" />
            <Item Text="Messages" Value="Messages" />
            <Item Text="Logout" Value="Logout" />
        </mobile:List>        
    </mobile:Form>
    <mobile:Form ID="frmOpenFlights" Runat="server">
        <mobile:Label ID="lblTitle1" Runat="server" Alignment="Center" EnableViewState="False">SATS iTrek</mobile:Label>
        <mobile:Label ID="lblOpenFlights" Runat="server" Alignment="Center"></mobile:Label>
        <mobile:List ID="lstOpenFlights" Runat="server" OnItemCommand="lstOpenFlights_ItemCommand" >
        </mobile:List>
         <br />
        <mobile:Command ID="cmdBack" Runat="server" OnClick="cmdBack_Click" EnableViewState="False">Back</mobile:Command>
       </mobile:Form>
    <mobile:Form ID="frmOpenFlightsMenu" Runat="server"><mobile:Label ID="lblTitle3"
        Runat="server" Alignment="Center" EnableViewState="False">SATS iTrek</mobile:Label>
        <mobile:TextView ID="txtVFlightDetailsOpenFlightMenu" Runat="server">
        </mobile:TextView>    
        <mobile:Label ID="lblStaffNo" Runat="server">
        </mobile:Label>
         <mobile:TextView ID="txtTiming" Runat="server"></mobile:TextView>     
        <mobile:List ID="lstSelFlights" Runat="server" OnItemCommand="lstSelFlights_ItemCommand">
         </mobile:List>
        <mobile:Command ID="cmdBack1" Runat="server" OnClick="cmdBack1_Click">Back</mobile:Command>
        
    </mobile:Form><mobile:Form ID="frmTiming" Runat="server"><mobile:Label ID="lblTitle4" Runat="server" Alignment="Center">SATS iTrek</mobile:Label>
        <mobile:TextView ID="txtVFlightDetailsTiming" Runat="server"></mobile:TextView>
      
        <mobile:Label ID="lblSelectedOption" Runat="server">
        </mobile:Label>
    
        <mobile:Label ID="lblTimingAlertMsg" Runat="server" EnableViewState="False" Visible="False" BreakAfter="True" >
        </mobile:Label>        
        <mobile:List ID="lstTime" Runat="server" OnItemCommand="lstTime_ItemCommand" >
         </mobile:List>        
        <mobile:Label ID="lblTime" Runat="server"  ></mobile:Label> 
        <br />                   
        <mobile:TextBox ID="txtBxGetTime" Runat="server" EnableViewState="False" MaxLength="4" 
            Numeric="True" Size="4">
        </mobile:TextBox>
       
        <br />
   <mobile:Command ID="cmdTime" Runat="server" OnClick="cmdTime_Click" >Go </mobile:Command>
       
        <mobile:Command ID="cmdBackfrmTime" Runat="server" OnClick="cmdBackfrmTime_Click">Back</mobile:Command>
      
    </mobile:Form>
   <mobile:Form ID="frmOK2Load" Runat="server" EnableViewState="False"><mobile:Label ID="lblTitle6" Runat="server"
        Alignment="Center" EnableViewState="False">SATS iTrek</mobile:Label>
        <br />
        <mobile:TextView ID="txtVFlightDetailsOK2L" Runat="server">
        </mobile:TextView>
        <mobile:Label ID="lblOk2Load" Runat="server">
        </mobile:Label>
        <br />
        <mobile:List ID="lstAllOK2Load" Runat="server" EnableViewState="False">
        </mobile:List>
        <mobile:Command ID="cmdBackFromOK2L" Runat="server" OnClick="cmdBackFromOK2L_Click"
            SoftkeyLabel="Back">
        </mobile:Command>
    </mobile:Form>
    <mobile:Form ID="frmDLS" Runat="server" EnableViewState="False">
                <mobile:Label ID="lblTitle5" Runat="server" Alignment="Center" EnableViewState="False">SATS iTrek</mobile:Label>
        <mobile:TextView ID="txtVFlightDetailsDLS" Runat="server">
        </mobile:TextView>
        <mobile:Label ID="lblDLS" Runat="server">
        </mobile:Label>
        <br />
        <mobile:TextView ID="txtVShowDLS" Runat="server" EnableViewState="False">
        </mobile:TextView>
        <br />
        <mobile:Command ID="cmdBackFromDLS" Runat="server" OnClick="cmdBackFromDLS_Click"
            SoftkeyLabel="Back">
        </mobile:Command>
    </mobile:Form>
    <mobile:Form ID="frmAlertMsg" Runat="server">
        <mobile:Label ID="lblHdr" Runat="server" Alignment="Center">SATS iTrek</mobile:Label>
        <br />
        <mobile:TextView ID="txtVAlertHdr" Runat="server">
        </mobile:TextView>
        <br />
        <mobile:Label ID="lblOption"
        Runat="server"></mobile:Label>
        <mobile:Label ID="lblAlertMessage"
        Runat="server"></mobile:Label>
        <mobile:Command ID="cmdYes" Runat="server" OnClick="cmdYes_Click">Yes</mobile:Command>
        <mobile:Command ID="cmdNo" Runat="server" OnClick="cmdNo_Click">No</mobile:Command>
        </mobile:Form>
</body>
</html>
