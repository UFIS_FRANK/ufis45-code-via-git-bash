<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Staff.aspx.cs" Inherits="WAP_Staff" EnableSessionState="True"%>

<%@ Register Src="MWUShowStaff.ascx" TagName="MWUShowStaff" TagPrefix="uc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmStaff" runat="server" OnActivate="frmStaff_Activate"><mobile:Label
        ID="lblHead1" Runat="server" Alignment="Center" Font-Bold="True">SATS iTrek</mobile:Label>&nbsp;<mobile:TextView
            ID="txtVFltHdng" Runat="server">
        </mobile:TextView><br /><mobile:Label ID="lblStaffFunc" Runat="server" Alignment="Center"
            Font-Bold="True">Staff/Functions</mobile:Label> <uc1:MWUShowStaff ID="MWUShowStaff1"
                runat="server" /> <mobile:Command ID="cmdBackFromStaff" Runat="server" OnClick="cmdBackFromStaff_Click" SoftkeyLabel="Back">Back</mobile:Command></mobile:Form>&nbsp;<br>
</br>
</body>
</html>
