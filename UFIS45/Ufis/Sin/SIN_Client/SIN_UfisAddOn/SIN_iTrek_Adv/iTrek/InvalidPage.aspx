<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InvalidPage.aspx.cs" Inherits="InvalidPage" EnableSessionState="True"%>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmError" runat="server">
<mobile:Label ID="lblErrTitle" Runat="server" Alignment="Center" EnableViewState="False">SATS iTrek</mobile:Label>
        <br /><mobile:Label ID="lblErrorMsg" Runat="server" EnableViewState="False"></mobile:Label>
        <mobile:Command ID="cmdError" Runat="server" OnClick="cmdError_Click">Go</mobile:Command>
    </mobile:Form>
</body>
</html>
