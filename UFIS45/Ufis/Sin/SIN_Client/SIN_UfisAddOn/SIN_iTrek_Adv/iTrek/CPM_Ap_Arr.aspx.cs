using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;
using UFIS.Wap.ITrek.WebMgr;
using UFIS.ITrekLib.EventArg;

public partial class WAP_CPM_Ap_Arr : System.Web.UI.MobileControls.MobilePage
{
    private const string CPM_MANUAL_LIST = "CPM_MANUAL_LIST";
    private const string CPM_AUTO_LIST = "CPM_AUTO_LIST";
    private const string CPM_RMK_IDX = "CPM_RMK_IDX";
    private const string CPM_RMK_IDX_AUTO_OR_MANUAL = "CPM_RMK_IDX_AUTO_OR_MANUAL";
    private const string AUTO_LIST = "A";
    private const string MANUAL_LIST = "M";


    protected void Page_Load(object sender, EventArgs e)
    {
        //DSFuncMap ds = new DSFuncMap();
        //UFIS.ITrekLib.DS.DSFuncMapTableAdapters.FUNCMAP1TableAdapter ta = new UFIS.ITrekLib.DS.DSFuncMapTableAdapters.FUNCMAP1TableAdapter();
        //ta.Fill(ds.FUNCMAP1);
        //ds.WriteXml(@"D:\am\FUNCMAP.XML");

        mwuCpmList1.ChkBoxOrRadioButton = "C";
        if (!IsPostBack)
        {
            GetDataFromSession();
            ActiveForm = frmCPMApArr;
            mwuCpmList1.AddItem("abc", "1");
            mwuCpmList1.AddItem("cde", "2");
        }
        mwuCpmList1.LinkClick += new WAP_MWUCpmList.LinkClickHandler(mwuCpmList1_LinkClick);
        mwuCpmManual1.LinkClick += new WAP_MWUCpmManual.LinkClickHandler(mwuCpmManual1_LinkClick);
        mwuBagRemk1.RemarkSaveClick += new WAP_MWUBagRemk.RemarkSaveClickEventHandler(mwuBagRemk1_RemarkSaveClick); ;
        mwuBagRemk1.RemarkBack_Click += new WAP_MWUBagRemk.RemarkBackClickEventHandler(mwuBagRemk1_RemarkBack_Click);
    }

    protected void frmCPMApArr_Activate(object sender, EventArgs e)
    {
        //mwuCpmList1.ShowInfo();
    }

    void mwuCpmList1_LinkClick(object sender, EaLinkClickEventArgs e)
    {
        EntCPMList cpmList;
        try
        {
            cpmList = (EntCPMList)ViewState[CPM_AUTO_LIST];
            cpmList = mwuCpmList1.GetData(cpmList);
            ViewState[CPM_AUTO_LIST] = cpmList;
            ViewState[CPM_RMK_IDX] = e.idx;
            ViewState[CPM_RMK_IDX_AUTO_OR_MANUAL] = AUTO_LIST;
            ShowRemarkForm();
        }
        catch (Exception)
        {
        }
    }

    void mwuCpmManual1_LinkClick(object sender, EaLinkClickEventArgs e)
    {
        EntCPMList cpmManualList ;
        try
        {
            cpmManualList = (EntCPMList)ViewState[CPM_MANUAL_LIST];
            cpmManualList = mwuCpmManual1.GetData(cpmManualList);
            ViewState[CPM_MANUAL_LIST] = cpmManualList;
            ViewState[CPM_RMK_IDX] = e.idx;
            ViewState[CPM_RMK_IDX_AUTO_OR_MANUAL] = MANUAL_LIST;
            ShowRemarkForm();
        }
        catch (Exception)
        {
        }
    }

    #region Remark Form
    //********************************
    private void ShowRemarkForm()
    {
        string autoOrManual = (string)ViewState[CPM_RMK_IDX_AUTO_OR_MANUAL];
        EntCPMList cpmList;
        int idx = (int)ViewState[CPM_RMK_IDX];
        EntRemark rmk = null;
        if (autoOrManual == AUTO_LIST)
        {
            cpmList = (EntCPMList)ViewState[CPM_AUTO_LIST];
            rmk = cpmList.GetData(idx).Remark;
        }
        else
        {
            cpmList = (EntCPMList)ViewState[CPM_MANUAL_LIST];
            rmk = cpmList.GetData(idx).Remark;
        }

        mwuBagRemk1.SetCurRmk(rmk);
        ActiveForm = frmCPMApArrRemk;
    }

    void mwuBagRemk1_RemarkSaveClick(object sender, UFIS.ITrekLib.EventArg.EaRemarkSaveEventArgs e)
    {
        SaveRemark(e.Remk);
        ActiveForm = frmCPMApArr;
    }

    private void SaveRemark(EntRemark rmk)
    {
        string autoOrManual = (string)ViewState[CPM_RMK_IDX_AUTO_OR_MANUAL];
        EntCPMList cpmList;
        int idx = (int)ViewState[CPM_RMK_IDX];
        if (autoOrManual == AUTO_LIST)
        {
            cpmList = (EntCPMList)ViewState[CPM_AUTO_LIST];
            cpmList.GetData(idx).Remark = rmk;
            ViewState[CPM_AUTO_LIST] = cpmList;
        }
        else
        {
            cpmList = (EntCPMList)ViewState[CPM_MANUAL_LIST];
            cpmList.GetData(idx).Remark = rmk;
            ViewState[CPM_MANUAL_LIST] = cpmList;
        }
    }

    void mwuBagRemk1_RemarkBack_Click(object sender, EventArgs e)
    {
        //throw new Exception("The method or operation is not implemented.");
        ActiveForm = frmCPMApArr;
    }

    //********************************
    #endregion

    #region Send Data
    protected void cmdCPMApArrSend2_Click(object sender, EventArgs e)
    {
        SendData();
    }

    private void SendData()
    {
        KeepData();
        string timingUrl = "Timing.aspx";
        Response.Redirect(timingUrl);
    }

    protected void cmdCPMApArrSend1_Click(object sender, EventArgs e)
    {
        SendData();
    }
    #endregion

    private void KeepData()
    {
        MgrSess.SetApronArrivalCPMAutoList(Session, (EntCPMList)ViewState[CPM_AUTO_LIST]);
        MgrSess.SetApronArrivalCPMManualList(Session, (EntCPMList)ViewState[CPM_MANUAL_LIST]);
    }

    private void GetDataFromSession()
    {
        ViewState[CPM_MANUAL_LIST] = MgrSess.GetApronArrivalCPMManualList(Session);
        ViewState[CPM_AUTO_LIST] = MgrSess.GetApronArrivalCPMAutoList(Session);
    }

    private void ClearSessionInfo()
    {
        MgrSess.SetApronArrivalCPMManualList(Session, null);
        MgrSess.SetApronArrivalCPMAutoList(Session, null);
    }

    protected void frmCPMApArrRemk_Activate(object sender, EventArgs e)
    {
        //EntCPMList cpmList;
        //cpmList = (EntCPMList)ViewState[CPM_MANUAL_LIST];
        //cpmList = (EntCPMList)ViewState[CPM_AUTO_LIST];      
    }
    protected void frmCPMApArrRemk_Init(object sender, EventArgs e)
    {

    }
}
