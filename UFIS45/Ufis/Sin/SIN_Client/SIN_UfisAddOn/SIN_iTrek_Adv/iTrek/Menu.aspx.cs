#define UsingQueues
using System;
using System.Configuration;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using System.IO;
using System.Net;
using System.Xml;
using iTrekSessionMgr;
using iTrekXML;
using iTrekWMQ;
using iTrekUtils;
using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.Util;
using UFIS.ITrekLib.DS;
using UFIS.Wap.ITrek.WebMgr;
using UFIS.ITrekLib.ENT;
using MM.UtilLib;

public partial class Menu : System.Web.UI.MobileControls.MobilePage
{
	//TODO: Change to use without these vars?
	string FlightNo = "";

	//TODO: Change to use without session vars?


	protected void Page_Load(object sender, EventArgs e)
	{
		string uId = "";
		MgrSec.IsAuthenticatedUser(this, Session, out uId);
		SessionManager sessMan = new SessionManager();

		//Avoid null obj ref error    

		if (Context.Session.Count == 0)
		{
			//RedirectToMobilePage("Login.aspx");
		}
		else
		{
			try
			{
				if (!IsPostBack)
				{
					string menuOption = Request.QueryString["MENU"].Trim().ToUpper();
					switch (menuOption)
					{
						case "OP":
							getOpenFlights();
							break;
						case "SELFLT":
							getSelectedFlightMenu();
							break;
						//case "CL":
					}
				}
			}
			catch (Exception ex)
			{//Form.PagerStyle.NextPageText 

				//swallow error - ex.Message;
			}
			string sessid = Session["sessionid"].ToString();

			lblUserIdName.Visible = true;
			lblUserIdName.Text = MgrSess.GetUser(Session) + "-" + Session["staffName"];
		}
	}


	protected void lstMainMenu_ItemCommand(object sender, ListCommandEventArgs e)
	{
		string jobCategory = "AO";/* Get the Job Category*/

		if (jobCategory == "AO")
		{
			if (e.ListItem.Text == "Open Flights")
			{
				getOpenFlights();
			}
			else if (e.ListItem.Text == "Closed Flights")
			{
				if (getClosedFlightsCount() != 0)
				{
					RedirectToMobilePage("ClosedFlight.aspx");
				}
				else
				{
					ActiveForm = frmMenu;
					lblAlertMsg.Visible = true;
					lblAlertMsg.Text = "There are currently no closed flights assigned to your Staff ID";
				}
			}
			else if (e.ListItem.Text == "Logout")
			{
				try
				{
					iTrekUtils.iTUtils.LogToEventLog("Logged Out Staffid=" + MgrSess.GetUser(Session), "iTrekDevice");
				}
				catch (Exception ex)
				{
					iTrekUtils.iTUtils.LogToEventLog("Error while loggingOut" + ex.Message, "iTrekError");
				}
				Session.Abandon();
				RedirectToMobilePage("Logout.aspx");
			}
			else if (e.ListItem.Text == "Messages")
			{
				RedirectToMobilePage("Messages.aspx");
			}
		}
	}

	private ArrayList GetOpenFlightListFromFlightDataset(DSFlight ds)
	{
		ArrayList lstFlight = new ArrayList();
		foreach (DSFlight.FLIGHTRow row in ds.FLIGHT.Select())
		{
			if (row.TURN == "")
			{
				Flight flight = new Flight(row.URNO, row.FLNO, row.ST, row.ST, row.REGN, row.TO, row.FR, row.ET, row.ET, row.GATE, row.GATE, row.A_D, row.PARK_STAND, row.PARK_STAND, row.BELT, row.OBL, row.OBL, row.TERMINAL, row.TERMINAL, row.TTYP, row.AIRCRAFT_TYPE, row.TURN, row.UAFT);
				lstFlight.Add(flight);
			}
			else if (row.A_D == "A")
			{
				bool isClosed = false;
				string flightId = "";
				string adid = "";
				string turnFlightId = "";
				if (!CtrlFlight.IsClosedFlight(row, out flightId, out adid, out turnFlightId))
				{
					Flight aFlight = new Flight(row.URNO, row.FLNO, row.ST, row.ST, row.REGN, row.TO, row.FR, row.ET, row.ET, row.GATE, row.GATE, row.A_D, row.PARK_STAND, row.PARK_STAND, row.BELT, row.OBL, row.OBL, row.TERMINAL, row.TERMINAL, row.TTYP, row.AIRCRAFT_TYPE, row.TURN, row.UAFT);
					lstFlight.Add(aFlight);
				}
			}
		}
		return lstFlight;
	}

	private Flight GetFlightFromFlightDataset(DSFlight ds)
	{
		ArrayList lstFlight = new ArrayList();
		Flight flight = null;
		if (ds.FLIGHT.Rows.Count > 0)
		{
			DSFlight.FLIGHTRow row = (DSFlight.FLIGHTRow)ds.FLIGHT.Rows[0];
			DSFlight.FLIGHTRow rowTurn = row;
			if (!string.IsNullOrEmpty(row.TURN))
			{
				DSFlight dsTurn = CtrlFlight.RetrieveFlightsByFlightId(row.TURN);
				if ((dsTurn != null) && (dsTurn.FLIGHT.Rows.Count > 0))
				{
					rowTurn = (DSFlight.FLIGHTRow)dsTurn.FLIGHT.Rows[0];

					if (row.A_D != "A")
					{
						DSFlight.FLIGHTRow rowTemp = row;
						row = rowTurn;
						rowTurn = rowTemp;
					}
				}
			}

			flight = new Flight(row.URNO, row.FLNO,
	rowTurn.ST, row.ST, row.REGN, row.TO, row.FR,
	rowTurn.ET, row.ET, row.GATE, rowTurn.GATE, row.A_D, row.PARK_STAND, rowTurn.PARK_STAND, row.BELT,
	rowTurn.OBL, row.OBL, row.TERMINAL, rowTurn.TERMINAL, row.TTYP, row.AIRCRAFT_TYPE, row.TURN, row.UAFT);
		}

		return flight;
	}

	protected void getOpenFlights()
	{
		txtVAlertHdr.Text = "";

		try
		{
			DSFlight dsFlt = new DSFlight();

			try
			{
				//dsFlt = CtrlFlight.RetrieveFlights("", DateTime.Now.AddHours(-4), DateTime.Now.AddHours(8), MgrSess.GetUser(Session));
				dsFlt = CtrlFlight.RetrieveOpenFlightsForAO(
					MgrSess.GetUser(Session), 
					6,
					DateTime.Now.AddHours(-4), 
					DateTime.Now.AddHours(8)
					);

				iTrekUtils.iTUtils.LogToEventLog("dsFlt.FLIGHT.Rows.Count " + dsFlt.FLIGHT.Rows.Count, "iTrekError");
			}
			catch (Exception dsException)
			{
				iTrekUtils.iTUtils.LogToEventLog("Getting DataSet " + dsException.Message, "iTrekError");
			}

			txtVFlightDetailsTiming.Text = "";
			txtVFlightDetailsOpenFlightMenu.Text = "";
			MgrSess.SetFlight(Session, null);
			iTXMLPathscl itpaths = new iTXMLPathscl();
			iTXMLcl iTXML = new iTXMLcl(itpaths.applicationBasePath);
			SessionManager sessMan = new SessionManager();
			ArrayList lstFlights = new ArrayList();
			ArrayList lstOpenFlt = new ArrayList();
			lstFlights.Clear();
			lstOpenFlt.Clear();
			int fltCnt = 0;
			int transitFltCnt = 0;
			ArrayList openFlights = new ArrayList();
			openFlights.Clear();
			lstOpenFlt.Clear();
			lstFlights.Clear();
			lstOpenFlt = GetOpenFlightListFromFlightDataset(dsFlt);

			if (lstOpenFlt != null && lstOpenFlt.Count != 0)
			{
				lstOpenFlt.Sort();
				//only fetch last 3 flights
				if (lstOpenFlt.Count > 3)

					try
					{
						lstOpenFlt.RemoveRange(3, lstOpenFlt.Count - 3);
					}
					catch (Exception ex)
					{
						iTrekUtils.iTUtils.LogToEventLog("Error Message" + ex.Message, "iTrekError");
					}
				lstFlights = iTXML.ReorderFlightsEarliestOnTop(lstOpenFlt);

			}

			//lstFlights = iTXML.ReorderFlightsEarliestOnTop(lstFlights);

			Session["FlightsList"] = null;

			int i = 0;

			lstOpenFlights.Items.Clear();
			if (lstFlights != null && lstFlights.Count != 0)
			{
				Hashtable ht = new Hashtable(lstFlights.Count);
				ht.Clear();
				// lstOpenFlights.ItemCount = lstFlights.Count;
				foreach (Flight flts in lstFlights)
				{

					ht.Add(i, flts);
					string lstItem = flts.DISPLAYOPENFLIGHTS;

					MobileListItem mb = new MobileListItem();
					mb.Text = lstItem;
					lstOpenFlights.Items.Add(mb);

					i++;
				}

				ActiveForm = frmOpenFlights;
				lstOpenFlights.Visible = true;
				lblOpenFlights.Text = "Open Flights";
				Session["FlightsList"] = ht;
			}

			else
			{
				ActiveForm = frmMenu;
				lblAlertMsg.Visible = true;
				lblAlertMsg.Text = "There are currently no  flights assigned to your Staff ID";
			}
		}
		catch (Exception arrExcep)
		{
			iTrekUtils.iTUtils.LogToEventLog("getOpenFlights " + arrExcep.Message, "iTrekError");
			RedirectToMobilePage("iTrekError.aspx");
		}
	}

	public void getSelectedFlightMenu()
	{
		try
		{
			txtVAlertHdr.Text = "";
			txtVFlightDetailsTiming.Text = "";
			txtVFlightDetailsOpenFlightMenu.Text = "";

			Flight flt = null;
			Flight flight = null;
			try
			{
				flt = MgrSess.GetFlight(Session);
			}
			catch (Exception ex)
			{
			}

			iTXMLPathscl itpaths = new iTXMLPathscl();
			iTXMLcl iTXML = new iTXMLcl(itpaths.applicationBasePath);

			if (flt != null)
			{
				try
				{
					//flight = iTXML.UserSessionReadFlightFromCurrentFlightXMLFile(Session["sessionid"].ToString(), "//FLIGHTS/FLIGHT[URNO='" + flt.URNO + "']", "flights.xml");
					flight = GetFlightFromFlightDataset(CtrlFlight.RetrieveFlightsByFlightId(flt.URNO));
				}
				catch (Exception)
				{
				}

				if (flight != null)
				{
					MgrSess.SetFlight(Session, flight);
					flt = flight;
				}
				iTrekSessionMgr.SessionManager sessMan = new iTrekSessionMgr.SessionManager();

				/* Added by Alphy on 22 Jan07 to get STOD.So kept flight object in session*/
				//Session["userFlight"] = flt;
				string fltADID = flt.ADID;
#if UsingQueues
				//CtrlCPM.RequestCPM(flt.URNO);
				if (fltADID == "T" || fltADID == "A")
				{
					CtrlCPM.CheckExistCPM(flt.URNO);
					if (fltADID == "T")
					{
						CtrlCPM.CheckExistCPM(flt.TURN);
					}
				}
				else if (fltADID == "D")
				{
					CtrlCPM.CheckExistCPM(flt.URNO);
				}
#endif
				ArrayList arrList = new ArrayList();
				ArrayList depList = new ArrayList();
				ArrayList transList = new ArrayList();
				txtTiming.Text = "";
				string[] strDep = new string[] { "MAB", "LOAD-START", "LOAD-END", "TRIPS", "LAST-DOOR", "PLB", "ATD" };
				string[] strArr = new string[] { "MAB", "ATA", "TBS-UP", "PLB", "UNLOAD-START", "FTRIP", "NEXT-TRIP", "LTRIP", "UNLOAD-END" };
				string[] strTransit = new string[] { "MAB        ", "ATA        ", "TBS-UP   ", "PLB(A)       ", "UNLOAD-START", "FTRIP", "NEXT-TRIP", "LTRIP", "UNLOAD-END", "LOAD-START", "LOAD-END", "TRIPS", "LAST DOOR", "PLB(D)", "ATD        " };
				lstSelFlights.Items.Clear();
				int rowCntToSelect = 0;
				bool depStatus = true;
				bool arrStatus = true;

				if (fltADID == "T")
				{
					DSArrivalFlightTimings dsArrFlightTiming = CtrlFlight.RetrieveArrivalFlightTimings(flt.URNO);
					DSDepartureFlightTimings dsDepflightTiming = CtrlFlight.RetrieveDepartureFlightTimings(flt.TURN);
					DSArrivalFlightTimings.AFLIGHTRow row = null;
					DSDepartureFlightTimings.DFLIGHTRow rowDep = null;
					if (dsArrFlightTiming.AFLIGHT.Rows.Count > 0)
					{
						row = ((DSArrivalFlightTimings.AFLIGHTRow)dsArrFlightTiming.AFLIGHT.Rows[0]);
					}

					if (dsDepflightTiming.DFLIGHT.Rows.Count > 0)
					{
						rowDep = ((DSDepartureFlightTimings.DFLIGHTRow)dsDepflightTiming.DFLIGHT.Rows[0]);
					}

					if (row == null || row.MAB == "" || row.MAB == "-")
					{
						transList.Add("MAB");
						depStatus = false;
						arrStatus = false;
					}
					else
					{
						txtTiming.Text += "MAB  " + UtilTime.ConvUfisTimeStringToDateTime(row.MAB.ToString()).ToString("HH:mm") + "</br>";
					}

					if (row == null || row.ATA == "" || row.ATA == "-")
					{
						transList.Add("ATA");
						arrStatus = false;
					}
					else
					{
						txtTiming.Text += "ATA  " + UtilTime.ConvUfisTimeStringToDateTime(row.ATA.ToString()).ToString("HH:mm") + "</br>";
					}

					if (row == null || row._TBS_UP == "" || row._TBS_UP == "-")
					{
						transList.Add("TBS-UP");
						arrStatus = false;
					}
					else
					{
						txtTiming.Text += "TBS-UP  " + UtilTime.ConvUfisTimeStringToDateTime(row._TBS_UP.ToString()).ToString("HH:mm") + "</br>";
					}

					if (row == null || row.PLB == "" || row.PLB == "-")
					{
						transList.Add("PLB(A)");
					}
					else
					{
						txtTiming.Text += "PLB(A) " + UtilTime.ConvUfisTimeStringToDateTime(row.PLB.ToString()).ToString("HH:mm") + "</br>";
					}

					if (row == null || row._UNLOAD_START == "" || row._UNLOAD_START == "-")
					{
						transList.Add("UNLOAD-START");
						arrStatus = false;
					}
					else
					{
						txtTiming.Text += "UNLOAD-START " + UtilTime.ConvUfisTimeStringToDateTime(row._UNLOAD_START.ToString()).ToString("HH:mm") + "</br>";
					}

					if (row == null || row.FTRIP == "" || row.FTRIP == "-")
					{
					}
					else
					{
						txtTiming.Text += "FTRIP" + UtilTime.ConvUfisTimeStringToDateTime(row.FTRIP.ToString()).ToString("HH:mm") + "</br>";
					}

					transList.Add("TRIPS(A)");

					if (row == null || row.LTRIP == "" || row.LTRIP == "-")
					{
					}
					else
					{

						txtTiming.Text += "LTRIP " + UtilTime.ConvUfisTimeStringToDateTime(row.LTRIP.ToString()).ToString("HH:mm") + "</br>";
					}

					if (row == null || row._UNLOAD_END == "" || row._UNLOAD_END == "-")
					{
						if (arrStatus == false)
						{
						}
						else
						{
							transList.Add("UNLOAD-END");
						}
					}
					else
					{
						txtTiming.Text += "UNLOAD-END " + UtilTime.ConvUfisTimeStringToDateTime(row._UNLOAD_END.ToString()).ToString("HH:mm") + "</br>";
					}

					if (rowDep == null || rowDep._LOAD_START == "" || rowDep._LOAD_START == "-")
					{
						transList.Add("LOAD-START");
						depStatus = false;
					}
					else
					{
						txtTiming.Text += "LOAD-START " + UtilTime.ConvUfisTimeStringToDateTime(rowDep._LOAD_START.ToString()).ToString("HH:mm") + "</br>";
					}

					transList.Add("TRIPS(D)");

					if (rowDep == null || rowDep._LOAD_END == "" || rowDep._LOAD_END == "-")
					{
						transList.Add("LOAD-END");
						depStatus = false;
					}
					else
					{
						txtTiming.Text += "LOAD-END " + UtilTime.ConvUfisTimeStringToDateTime(rowDep._LOAD_END.ToString()).ToString("HH:mm") + "</br>";
					}

					if (rowDep == null || rowDep._LAST_DOOR == "" || rowDep._LAST_DOOR == "-")
					{
						transList.Add("LAST-DOOR");
						depStatus = false;
					}
					else
					{
						txtTiming.Text += "LAST-DOOR " + UtilTime.ConvUfisTimeStringToDateTime(rowDep._LAST_DOOR.ToString()).ToString("HH:mm") + "</br>";

					}

					if (rowDep == null || rowDep.PLB == "" || rowDep.PLB == "-")
					{
						transList.Add("PLB(D)");
					}
					else
					{
						txtTiming.Text += "PLB(D) " + UtilTime.ConvUfisTimeStringToDateTime(rowDep.PLB.ToString()).ToString("HH:mm") + "</br>";
					}

					if (rowDep == null || rowDep.ATD == "" || rowDep.ATD == "-")
					{
						if (depStatus == false)
						{
						}
						else
						{
							transList.Add("ATD");
						}
					}
					else
					{
					}

					transList.Add("SHOW CPM");
					transList.Add("SHOW TRIPS");
					transList.Add("ALL OK2LOAD");
					transList.Add("SHOW DLS");
					transList.Add("SHOW STAFF");
					transList.Add("SHOW EQUIPMENT");
                    transList.Add("ASR");
					transList.Add("SEND MESSAGE");
					transList.Add("VIEW MESSAGE");
					lstSelFlights.DataSource = transList;
					lstSelFlights.DataBind();
				}
				else if (fltADID == "D")
				{
					DSDepartureFlightTimings dsDepflightTiming = CtrlFlight.RetrieveDepartureFlightTimings(flt.URNO);
					DSDepartureFlightTimings.DFLIGHTRow rowDep = null;
					if (dsDepflightTiming.DFLIGHT.Rows.Count > 0)
					{
						rowDep = ((DSDepartureFlightTimings.DFLIGHTRow)dsDepflightTiming.DFLIGHT.Rows[0]);
					}

					if (rowDep == null || rowDep.MAB == "" || rowDep.MAB == "-")
					{
						depList.Add("MAB");
						depStatus = false;
					}
					else
					{
						txtTiming.Text += "MAB " + UtilTime.ConvUfisTimeStringToDateTime(rowDep.MAB.ToString()).ToString("HH:mm") + "</br>";
					}

					if (rowDep == null || rowDep._LOAD_START == "" || rowDep._LOAD_START == "-")
					{
						depList.Add("LOAD-START");
						depStatus = false;
					}
					else
					{
						txtTiming.Text += "LOAD-START " + UtilTime.ConvUfisTimeStringToDateTime(rowDep._LOAD_START.ToString()).ToString("HH:mm") + "</br>";
					}

					depList.Add("TRIPS");

					if (rowDep == null || rowDep._LOAD_END == "" || rowDep._LOAD_END == "-")
					{
						depList.Add("LOAD-END ");
						depStatus = false;
					}
					else
					{
						txtTiming.Text += "LOAD-END " + UtilTime.ConvUfisTimeStringToDateTime(rowDep._LOAD_END.ToString()).ToString("HH:mm") + "</br>";

					}

					if (rowDep == null || rowDep._LAST_DOOR == "" || rowDep._LAST_DOOR == "-")
					{
						depList.Add("LAST-DOOR");
						depStatus = false;
					}
					else
					{
						txtTiming.Text += "LAST-DOOR " + UtilTime.ConvUfisTimeStringToDateTime(rowDep._LAST_DOOR.ToString()).ToString("HH:mm") + "</br>";

					}

					if (rowDep == null || rowDep.PLB == "" || rowDep.PLB == "-")
					{
						depList.Add("PLB");
					}
					else
					{
						txtTiming.Text += "PLB " + UtilTime.ConvUfisTimeStringToDateTime(rowDep.PLB.ToString()).ToString("HH:mm") + "</br>";
					}

					if (rowDep == null || rowDep.ATD == "" || rowDep.ATD == "-")
					{
						if (depStatus == false)
						{
						}
						else
						{
							depList.Add("ATD");
						}
					}

					depList.Add("ALL OK2LOAD");
					depList.Add("SHOW DLS");
					depList.Add("SHOW TRIPS");
					depList.Add("SHOW STAFF");
					depList.Add("SHOW EQUIPMENT");
                    depList.Add("ASR");
					depList.Add("SEND MESSAGE");
					depList.Add("VIEW MESSAGE");
					lstSelFlights.DataSource = depList;
					lstSelFlights.DataBind();
				}
				else if (fltADID == "A")
				{
					DSArrivalFlightTimings dsArrFlightTiming = CtrlFlight.RetrieveArrivalFlightTimings(flt.URNO);
					DSArrivalFlightTimings.AFLIGHTRow row = null;
					if (dsArrFlightTiming.AFLIGHT.Rows.Count > 0)
					{
						row = ((DSArrivalFlightTimings.AFLIGHTRow)dsArrFlightTiming.AFLIGHT.Rows[0]);
					}

					if (row == null || row.MAB == "" || row.MAB == "-")
					{
						arrList.Add("MAB");
						arrStatus = false;
					}
					else
					{
						txtTiming.Text += "MAB " + UtilTime.ConvUfisTimeStringToDateTime(row.MAB.ToString()).ToString("HH:mm") + "</br>";
					}

					if (row == null || row.ATA == "" || row.ATA == "-")
					{
						arrList.Add("ATA");
						arrStatus = false;
					}
					else
					{
						txtTiming.Text += "ATA " + UtilTime.ConvUfisTimeStringToDateTime(row.ATA.ToString()).ToString("HH:mm") + "</br>";
					}

					if (row == null || row._TBS_UP == "" || row._TBS_UP == "-")
					{
						arrList.Add("TBS-UP");
						arrStatus = false;
					}
					else
					{
						txtTiming.Text += "TBS-UP " + UtilTime.ConvUfisTimeStringToDateTime(row._TBS_UP.ToString()).ToString("HH:mm") + "</br>";
					}

					if (row == null || row.PLB == "" || row.PLB == "-")
					{
						arrList.Add("PLB");
					}
					else
					{
						txtTiming.Text += "PLB " + UtilTime.ConvUfisTimeStringToDateTime(row.PLB.ToString()).ToString("HH:mm") + "</br>";
					}

					if (row == null || row._UNLOAD_START == "" || row._UNLOAD_START == "-")
					{
						arrList.Add("UNLOAD-START");
						arrStatus = false;
					}
					else
					{
						txtTiming.Text += "UNLOAD-START " + UtilTime.ConvUfisTimeStringToDateTime(row._UNLOAD_START.ToString()).ToString("HH:mm") + "</br>";
					}

					if (row == null || row.FTRIP == "" || row.FTRIP == "-")
					{

					}
					else
					{
						txtTiming.Text += "FTRIP " + UtilTime.ConvUfisTimeStringToDateTime(row.FTRIP.ToString()).ToString("HH:mm") + "</br>";
					}

					arrList.Add("TRIPS");

					if (row == null || row.LTRIP == "" || row.LTRIP == "-")
					{
					}
					else
					{

						txtTiming.Text += "LTRIP " + UtilTime.ConvUfisTimeStringToDateTime(row.LTRIP.ToString()).ToString("HH:mm") + "</br>";
					}

					if (row == null || row._UNLOAD_END == "" || row._UNLOAD_END == "-")
					{
						if (arrStatus == false)
						{
						}
						else
						{
							arrList.Add("UNLOAD-END");
						}
					}
					else
					{
					}

					arrList.Add("SHOW CPM");
					arrList.Add("SHOW STAFF");
					arrList.Add("SHOW EQUIPMENT");
                    arrList.Add("ASR");
					arrList.Add("SEND MESSAGE");
					arrList.Add("VIEW MESSAGE");
					lstSelFlights.DataSource = arrList;
					lstSelFlights.DataBind();
				}

				try
				{
					lblStaffNo.Text = "Staff: " + CtrlJob.GetStaffCount(flt.URNO);
				}
				catch (Exception ex)
				{
					iTrekUtils.iTUtils.LogToEventLog("Getting Staff Count " + ex.Message, "iTrekError");
				}
				txtVFlightDetailsOpenFlightMenu.Text = flt.DISPLAYFLIGHTSMENU;
				ActiveForm = frmOpenFlightsMenu;
			}
			else
			{
				getOpenFlights();
			}
		}
		catch (Exception arrExcep)
		{
			iTrekUtils.iTUtils.LogToEventLog("lstOpenFlights_ItemCommand " + arrExcep.Message, "iTrekError");
			RedirectToMobilePage("iTrekError.aspx");
		}
	}

	protected void lstOpenFlights_ItemCommand(object sender, ListCommandEventArgs e)
	{
		int selIndex = e.ListItem.Index;
		Flight flt = null;
		try
		{
			flt = ((Flight)((Hashtable)(Session["FlightsList"]))[selIndex]);
		}
		catch (Exception)
		{
			getOpenFlights();
		}

		if (flt != null)
		{
			MgrSess.SetFlight(Session, flt);
			getSelectedFlightMenu();
		}
	}

	protected void cmdLogin_Click(object sender, EventArgs e)
	{
		iTXMLcl iTXML = new iTXMLcl();
		SessionManager sessMan = new SessionManager();
		try
		{
			if (Context.Session.Count == 0)
			{
				RedirectToMobilePage("Login.aspx");
			}
			else
			{
				string PENO = MgrSess.GetUser(Session);
				CtrlCurUser.DeleteDeviceIdEntryByStaffID(PENO);

				Session.Clear();
				Session.Abandon();

				RedirectToMobilePage("Login.aspx");
			}
		}
		catch (Exception logExc)
		{

			iTrekUtils.iTUtils.LogToEventLog("cmdLogin_Click" + logExc.Message, "iTrekError");
			RedirectToMobilePage("iTrekError.aspx");
		}
	}

	protected void cmdBack1_Click(object sender, EventArgs e)
	{
		getOpenFlights();
	}

	protected void lstSelFlights_ItemCommand(object sender, ListCommandEventArgs e)
	{
		string listItem = e.ListItem.Text.Trim();
        string reqPg = Request.Url.Segments[Request.Url.Segments.Length - 1];

		iTXMLPathscl itpaths = new iTXMLPathscl();
		iTXMLcl itxml = new iTXMLcl(itpaths.applicationBasePath);
		SessionManager sessMan = new SessionManager();

		Flight selFlt = null;
		try
		{
			selFlt = MgrSess.GetFlight(Session);
		}
		catch (Exception ex)
		{
		}

		if (selFlt != null)
		{
			if (txtVFlightDetailsOpenFlightMenu.Text == selFlt.DISPLAYFLIGHTSMENU)
			{
				if (selFlt.ADID == "T")
				{
					if (listItem.Length > 5)
					{
						if (listItem.Substring(0, 5) == "LTRIP")
						{
							listItem = "LTRIP";
						}
					}
					if (listItem == "ALL OK2LOAD")
					{
						//TODO;Remove?
						txtVFlightDetailsOK2L.Text = selFlt.DISPLAYFLIGHTSMENU;

						//Have to go through GetAllOk2LoadAL (not GetAllOk2LoadALFromSessionDir) to
						//implement delay timing
						ArrayList lstOK2Loads = new ArrayList();
						lstOK2Loads.Clear();
						lstAllOK2Load.Items.Clear();

						lstOK2Loads = sessMan.GetAllOk2LoadAL(selFlt.FLNO, CtrlFlight.GetUaftForFlightId(null, selFlt.TURN), Session["sessionid"].ToString());
						if (lstOK2Loads.Count != 0)
						{
							lstAllOK2Load.Visible = true;
							lstAllOK2Load.DataSource = lstOK2Loads;
							lstAllOK2Load.DataBind();
						}
						ActiveForm = frmOK2Load;
						lblOk2Load.Text = "ULDs RECEIVED";
					}
					else if (listItem == "SHOW DLS")/*Changed by Alphy for the DLs*/
					{
						string DLS = sessMan.GetShowDLS(selFlt.FLNO, CtrlFlight.GetUaftForFlightId(null, selFlt.TURN), Session["sessionid"].ToString());

						lblDLS.Text = e.ListItem.Text;
						txtVFlightDetailsDLS.Text = "";
						txtVFlightDetailsDLS.Text = selFlt.DISPLAYFLIGHTSMENU;

						if (DLS != "")
						{
							txtVShowDLS.Text = DLS;
							ActiveForm = frmDLS;
						}
						else
						{
							ActiveForm = frmDLS;
							txtVShowDLS.Text = "No DLS data available for this flight";
						}
					}
					else if (listItem == "SEND MESSAGE")
					{
						RedirectToMobilePage("Messages.aspx?newmessage=yes");
					}
					else if (listItem == "VIEW MESSAGE")
					{
						RedirectToMobilePage("Messages.aspx?newmessage=flight");
					}
					else if (listItem == "TRIPS(D)")
					{
						RedirectToMobilePage("DepAprTrip.aspx");
					}
					else if (listItem == "SHOW STAFF")
					{
						RedirectToMobilePage("Staff.aspx");
					}
					else if (listItem == "SHOW EQUIPMENT")
					{
                        RedirectToMobilePage("gse/Equipments.aspx?reqPg=" + reqPg);
					}
                    else if (listItem == "ASR")
                    {
                        RedirectToMobilePage("gse/RSMInfos.aspx?reqPg=" + reqPg);
                    }
                    else if (listItem == "TRIPS(A)")
                    {
                        RedirectToMobilePage("ArrAprTrip.aspx?selectedTrip=" + listItem);
                    }
                    else if (listItem == "SHOW CPM")
                    {
                        RedirectToMobilePage("ShowCPM.aspx?selection=cpm");
                    }
                    else if (listItem == "SHOW TRIPS")
                    {
                        RedirectToMobilePage("ShowCPM.aspx?selection=trips");
                    }
                    else
                    {
                        if (listItem == "LAST-DOOR")
                        {
                            DSTrip dsTrip = CtrlTrip.RetrieveTripNotReceived(selFlt.TURN);
                            if (dsTrip.TRIP.Count != 0)
                            {
                                lblOption.Text = listItem;
                                lblAlertMessage.Text = "ALERT: There are pending containers in TRIPS! To continue, Press Yes. Return to TRIPS list, Press No.";
                                txtVAlertHdr.Text = selFlt.DISPLAYFLIGHTSMENU;
                                ActiveForm = frmAlertMsg;
                            }
                            else
                            {
                                populateLstTime(e.ListItem.Text.Trim());
                            }
                        }
                        else if (listItem == "UNLOAD-END")
                        {
                            EntDsCPM dsCpm = CtrlCPM.RetrieveUnSentCPMUldForAFlightForAO(selFlt.URNO);
                            if (dsCpm.UldCount(selFlt.URNO) != 0)
                            {
                                Session["LTRIP"] = "";
                                lblOption.Text = listItem;
                                lblAlertMessage.Text = "ALERT: There are pending containers in TRIPS! To continue, Press Yes. Return to TRIPS list, Press No.";
                                txtVAlertHdr.Text = selFlt.DISPLAYFLIGHTSMENU;
                                ActiveForm = frmAlertMsg;
                            }
                            else
                            {
                                populateLstTime(e.ListItem.Text.Trim());
                            }
                        }
                        else
                        {
                            populateLstTime(e.ListItem.Text.Trim());
                        }
                    }
				}
				else if (selFlt.ADID == "D")
				{
					if (listItem == "ATD")
					{
						populateLstTime(e.ListItem.Text.Trim());
					}
					else if (listItem == "ALL OK2LOAD")
					{
						//TODO;Remove?
						txtVFlightDetailsOK2L.Text = selFlt.DISPLAYFLIGHTSMENU;

						//Have to go through GetAllOk2LoadAL (not GetAllOk2LoadALFromSessionDir) to
						//implement delay timing
						ArrayList lstOK2Loads = new ArrayList();
						lstOK2Loads.Clear();
						lstAllOK2Load.Items.Clear();
						// lstOK2Loads = sessMan.GetAllOk2LoadAL(Session["FlightNO"].ToString(), Session["FlightURNO"].ToString(), Session["sessionid"].ToString());
						lstOK2Loads = sessMan.GetAllOk2LoadAL(selFlt.FLNO, selFlt.UAFT, Session["sessionid"].ToString());
						if (lstOK2Loads.Count != 0)
						{
							lstAllOK2Load.Visible = true;
							lstAllOK2Load.DataSource = lstOK2Loads;
							lstAllOK2Load.DataBind();
						}
						ActiveForm = frmOK2Load;
						lblOk2Load.Text = "ULDs RECEIVED";
					}
					else if (listItem == "SHOW DLS")/*Changed by Alphy for the DLs*/
					{
						string DLS = sessMan.GetShowDLS(selFlt.FLNO, selFlt.UAFT, Session["sessionid"].ToString());

						lblDLS.Text = e.ListItem.Text;
						txtVFlightDetailsDLS.Text = "";
						txtVFlightDetailsDLS.Text = selFlt.DISPLAYFLIGHTSMENU;
						if (DLS != "")
						{
							txtVShowDLS.Text = DLS;
							ActiveForm = frmDLS;
						}
						else
						{
							ActiveForm = frmDLS;
							txtVShowDLS.Text = "No DLS data available for this flight";
						}
					}
					else if (listItem == "SEND MESSAGE")
					{
						RedirectToMobilePage("Messages.aspx?newmessage=yes");
					}
					else if (listItem == "VIEW MESSAGE")
					{
						RedirectToMobilePage("Messages.aspx?newmessage=flight");
					}
					else if (listItem == "TRIPS")
					{
						RedirectToMobilePage("DepAprTrip.aspx");
					}
					else if (listItem == "SHOW STAFF")
					{
						RedirectToMobilePage("Staff.aspx");
					}
					else if (listItem == "SHOW EQUIPMENT")
					{
                        RedirectToMobilePage("gse/Equipments.aspx?reqPg=" + reqPg);
					}
                    else if (listItem == "ASR")
                    {
                        RedirectToMobilePage("gse/RSMInfos.aspx?reqPg=" + reqPg);
                    }
					else if (listItem == "SHOW TRIPS")
					{
						RedirectToMobilePage("ShowCPM.aspx?selection=trips");
					}
					else
					{
						if (listItem == "LAST-DOOR")
						{
							DSTrip dsTrip = CtrlTrip.RetrieveTripNotReceived(selFlt.URNO);
							if (dsTrip.TRIP.Count != 0)
							{
								lblOption.Text = listItem;
								lblAlertMessage.Text = "ALERT: There are pending containers in TRIPS! To continue, Press Yes. Return to TRIPS list, Press No .";
								txtVAlertHdr.Text = selFlt.DISPLAYFLIGHTSMENU;
								ActiveForm = frmAlertMsg;
							}
							else
							{
								populateLstTime(e.ListItem.Text.Trim());
							}
						}
						else
						{
							populateLstTime(e.ListItem.Text.Trim());
						}
					}
				}
				else if (selFlt.ADID == "A")
				{
					if (listItem.Length > 5)
					{
						if (listItem.Substring(0, 5) == "LTRIP")
						{
							listItem = "LTRIP";
						}
					}

					if (listItem == "TRIPS")
					{
						RedirectToMobilePage("ArrAprTrip.aspx?selectedTrip=" + listItem);
					}
					else if (listItem == "SHOW CPM")
					{
						RedirectToMobilePage("ShowCPM.aspx?selection=cpm");
					}
					else if (listItem == "SHOW STAFF")
					{
						RedirectToMobilePage("Staff.aspx");
					}
					else if (listItem == "SHOW EQUIPMENT")
					{
                        RedirectToMobilePage("gse/Equipments.aspx?reqPg=" + reqPg);
					}
                    else if (listItem == "ASR")
                    {
                        RedirectToMobilePage("gse/RSMInfos.aspx?reqPg=" + reqPg);
                    }
					else if (listItem == "SEND MESSAGE")
					{
						RedirectToMobilePage("Messages.aspx?newmessage=yes");
					}
					else if (listItem == "VIEW MESSAGE")
					{
						RedirectToMobilePage("Messages.aspx?newmessage=flight");
					}
					else
					{
						if (listItem == "UNLOAD-END")
						{
							EntDsCPM dsCpm = CtrlCPM.RetrieveUnSentCPMUldForAFlightForAO(selFlt.URNO);
							if (dsCpm.UldCount(selFlt.URNO) != 0)
							{
								Session["LTRIP"] = "";
								lblOption.Text = listItem;
								lblAlertMessage.Text = "ALERT: There are pending containers in TRIPS! To continue, Press Yes. Return to TRIPS list, Press No .";
								txtVAlertHdr.Text = selFlt.DISPLAYFLIGHTSMENU;
								ActiveForm = frmAlertMsg;
							}
							else
							{
								populateLstTime(e.ListItem.Text.Trim());
							}
						}
						else
						{
							populateLstTime(listItem);
						}
					}
				}
			}
			else
			{
				getOpenFlights();
			}
		}
		else
		{
			getOpenFlights();
		}
	}


	protected void cmdTime_Click(object sender, EventArgs e)
	{
		try
		{

			iTXMLPathscl iTPaths = new iTXMLPathscl();
			iTXMLcl iTXML = new iTXMLcl();
			DateTime dtselectedTime = DateTime.Now;
			bool dtStatus = true;
			bool prevDayStatus = false;
			string selectedTime = "";

			Flight selFlt = null;
			try
			{
				selFlt = MgrSess.GetFlight(Session);
			}
			catch (Exception ex)
			{
			}

			if (selFlt != null)
			{
				if (txtVFlightDetailsTiming.Text == selFlt.DISPLAYFLIGHTSMENU)
				{
					try
					{
						selectedTime = txtBxGetTime.Text.Substring(0, 2) + ":" + txtBxGetTime.Text.Substring(2, 2);
					}
					catch (ArgumentOutOfRangeException exc)
					{

						lblTimingAlertMsg.Visible = true;
						lblTimingAlertMsg.Text = "Invalid Time.Please enter time between 0000 - 2359";
						dtStatus = false;
					}
					try
					{
						dtselectedTime = Convert.ToDateTime(selectedTime);
					}
					catch (FormatException ex)
					{
						lblTimingAlertMsg.Visible = true;
						lblTimingAlertMsg.Text = "Invalid Time.Please enter time between 0000 - 2359";
						dtStatus = false;
					}

					if (dtStatus)
					{

						dtselectedTime = UtilTime.CompDateTimeFromRef(selFlt.FLIGHTDATE, selectedTime);
						selectedTime = UtilTime.ConvDateTimeToUfisFullDTString(dtselectedTime);

						DateTime curTime = DateTime.Now;
						if (dtselectedTime > curTime)
						{
							lblTimingAlertMsg.Visible = true;
							lblTimingAlertMsg.Text = "Time entered must be earlier than current time - please re-enter";
							dtStatus = false;
						}
					}

					if (dtStatus)
					{
						if (selFlt.ADID == "T")
						{
							if (lblSelectedOption.Text == "ATD")
							{
								updateFlightTiming(prevDayStatus, selectedTime);
								updateFlightList();
							}
							else
							{
								updateFlightTiming(prevDayStatus, selectedTime);
								getSelectedFlightMenu();
							}
						}
						else if (selFlt.ADID == "D")
						{
							if (lblSelectedOption.Text == "ATD")
							{
								updateFlightTiming(prevDayStatus, selectedTime);
								updateFlightList();
							}
							else
							{
								updateFlightTiming(prevDayStatus, selectedTime);
								getSelectedFlightMenu();
							}
						}
						else if (selFlt.ADID == "A")
						{
							if (lblSelectedOption.Text == "UNLOAD-END")
							{
								updateFlightTiming(prevDayStatus, selectedTime);
								updateFlightList();
							}
							else
							{
								updateFlightTiming(prevDayStatus, selectedTime);
								getSelectedFlightMenu();
							}
						}
					}
					else
					{
						ActiveForm = frmTiming;
					}
				}
				else
				{
					getOpenFlights();
				}
			}
			else
			{
				getOpenFlights();
			}
		}
		catch (Exception timeExcep)
		{
			iTrekUtils.iTUtils.LogToEventLog("cmdTime_Click" + timeExcep.Message, "iTrekError");
			lblTimingAlertMsg.Visible = true;
			lblTimingAlertMsg.Text = "Timing input was not successful.Please try to send the timing again";
		}
	}


	protected void cmdBackfrmTime_Click(object sender, EventArgs e)
	{
		getSelectedFlightMenu();
	}

	protected void cmdBackFromDLS_Click(object sender, EventArgs e)
	{
		getSelectedFlightMenu();
	}

	protected void cmdBackFromOK2L_Click(object sender, EventArgs e)
	{
		getSelectedFlightMenu();
	}

	protected void lstTime_ItemCommand(object sender, ListCommandEventArgs e)
	{
		try
		{
			iTXMLPathscl iTPaths = new iTXMLPathscl();
			iTXMLcl iTXML = new iTXMLcl();
			DateTime dtselectedTime;
			bool prevDayStatus = false;
			string selectedTime = "";
			//Flight selFlt = ((Flight)Session["userFlight"]);


			Flight selFlt = null;
			try
			{
				selFlt = MgrSess.GetFlight(Session);
			}
			catch (Exception ex)
			{
			}

			if (selFlt != null)
			{
				if (txtVFlightDetailsTiming.Text == selFlt.DISPLAYFLIGHTSMENU)
				{
					selectedTime = e.ListItem.Text;
					selectedTime = selectedTime.Replace("NOW ", "");
					dtselectedTime = UtilTime.CompDateTimeFromRef(selFlt.FLIGHTDATE, selectedTime);
					selectedTime = UtilTime.ConvDateTimeToUfisFullDTString(dtselectedTime);

					if (selFlt.ADID == "T")
					{
						if (lblSelectedOption.Text == "ATD")
						{

							updateFlightTiming(prevDayStatus, selectedTime);
							updateFlightList();
						}
						else
						{
							updateFlightTiming(prevDayStatus, selectedTime);
							getSelectedFlightMenu();
						}
					}
					else if (selFlt.ADID == "D")
					{
						if (lblSelectedOption.Text == "ATD")
						{
							updateFlightTiming(prevDayStatus, selectedTime);
							updateFlightList();
						}
						else
						{
							updateFlightTiming(prevDayStatus, selectedTime);
							getSelectedFlightMenu();
						}
					}
					else if (selFlt.ADID == "A")
					{
						if (lblSelectedOption.Text == "UNLOAD-END")
						{
							updateFlightTiming(prevDayStatus, selectedTime);
							updateFlightList();
						}
						else
						{
							updateFlightTiming(prevDayStatus, selectedTime);
							getSelectedFlightMenu();
						}
					}
				}
				else
				{
					getOpenFlights();
				}
			}
			else
			{
				getOpenFlights();
			}
		}
		catch (Exception timeExcep)
		{
			iTrekUtils.iTUtils.LogToEventLog("cmdTime_Click" + timeExcep.Message, "iTrekError");
			lblTimingAlertMsg.Visible = true;
			lblTimingAlertMsg.Text = "Timing input was not successful.Please try to send the timing again";
		}
	}

	protected void cmdBack_Click(object sender, EventArgs e)
	{
		ActiveForm = frmMenu;
	}



	private bool checkForTimingInDsFlight(string selectedOption, string adId)
	{
		bool hasValue = false;
		Flight selFlt = MgrSess.GetFlight(Session);
		string FlightURNO = selFlt.URNO;
		DSArrivalFlightTimings dsArrFlightTiming = new DSArrivalFlightTimings();
		DSDepartureFlightTimings dsDepflightTiming = new DSDepartureFlightTimings();
		DSArrivalFlightTimings.AFLIGHTRow rowArr = null;
		DSDepartureFlightTimings.DFLIGHTRow rowDep = null;
		if (adId == "A")
		{
			dsArrFlightTiming = CtrlFlight.RetrieveArrivalFlightTimings(FlightURNO);
			if (dsArrFlightTiming.AFLIGHT.Rows.Count > 0)
			{
				rowArr = ((DSArrivalFlightTimings.AFLIGHTRow)dsArrFlightTiming.AFLIGHT.Rows[0]);
			}

			if (selectedOption == "MAB")
			{
				if (rowArr == null || rowArr.MAB == null || rowArr.MAB == "")
				{
					hasValue = false;
				}
				else hasValue = true;
			}
			else if (selectedOption == "ATA")
			{
				if (rowArr == null || rowArr.ATA == null || rowArr.ATA == "")
				{
					hasValue = false;
				}
				else hasValue = true;
			}
			else if (selectedOption == "TBS-UP")
			{
				if (rowArr == null || rowArr._TBS_UP == null || rowArr._TBS_UP == "")
				{
					hasValue = false;
				}
				else hasValue = true;
			}
			else if (selectedOption == "PLB")
			{
				if (rowArr == null || rowArr.PLB == null || rowArr.PLB == "")
				{
					hasValue = false;
				}
				else hasValue = true;
			}
			else if (selectedOption == "UNLOAD-START")
			{
				if (rowArr == null || rowArr._UNLOAD_START == null || rowArr._UNLOAD_START == "")
				{
					hasValue = false;
				}
				else hasValue = true;
			}
			else if (selectedOption == "UNLOAD-END")
			{
				if (rowArr == null || rowArr._UNLOAD_END == null || rowArr._UNLOAD_END == "")
				{
					hasValue = false;
				}
				else hasValue = true;
			}
		}
		else if (adId == "D")
		{
			if (selFlt.ADID == "T")
			{
				FlightURNO = selFlt.TURN;
			}

			dsDepflightTiming = CtrlFlight.RetrieveDepartureFlightTimings(FlightURNO);
			if (dsDepflightTiming.DFLIGHT.Rows.Count > 0)
			{
				rowDep = ((DSDepartureFlightTimings.DFLIGHTRow)dsDepflightTiming.DFLIGHT.Rows[0]);
			}

			if (selectedOption == "MAB")
			{
				if (rowDep == null || rowDep.MAB == null || rowDep.MAB == "")
				{

					hasValue = false;
				}
				else hasValue = true;

			}
			else if (selectedOption == "LOAD-START")
			{
				if (rowDep == null || rowDep._LOAD_START == null || rowDep._LOAD_START == "")
				{

					hasValue = false;
				}
				else hasValue = true;
			}
			else if (selectedOption == "LOAD-END")
			{
				if (rowDep == null || rowDep._LOAD_END == null || rowDep._LOAD_END == "")
				{

					hasValue = false;
				}
				else hasValue = true;
			}
			else if (selectedOption == "LAST-DOOR")
			{
				if (rowDep == null || rowDep._LAST_DOOR == null || rowDep._LAST_DOOR == "")
				{

					hasValue = false;
				}
				else hasValue = true;
			}
			else if (selectedOption == "PLB")
			{
				if (rowDep == null || rowDep.PLB == null || rowDep.PLB == "")
				{

					hasValue = false;
				}
				else hasValue = true;
			}
			else if (selectedOption == "ATD")
			{
				if (rowDep == null || rowDep.ATD == null || rowDep.ATD == "")
				{

					hasValue = false;
				}
				else hasValue = true;
			}
		}

		return hasValue;
	}

	private void updateFlightTiming(bool prevDayStatus, string selectedTime)
	{
		string msgUpdateStatus = "";
		try
		{
			iTXMLcl iTXML = new iTXMLcl();

			Flight selFlt = MgrSess.GetFlight(Session);
			string FlightURNO = selFlt.URNO;
			string TURN = selFlt.TURN;

			string StaffID = MgrSess.GetUser(Session);

			string TimeInOracleFormat = selectedTime;

			if (selFlt.DISPLAYFLIGHTSMENU == txtVFlightDetailsTiming.Text)
			{
				string ltrip = "";
				try
				{
					ltrip = Session["LTRIP"].ToString();
				}
				catch (Exception)
				{
				}

				if (lblSelectedOption.Text == "UNLOAD-END")
				{
					if (!checkForTimingInDsFlight(lblSelectedOption.Text, "A"))
					{
						CtrlTrip.UpdateSendLastTripIdForTrpMst(FlightURNO, StaffID, lblSelectedOption.Text, TimeInOracleFormat);
					}
				}
				else if (selFlt.ADID == "T")
				{
					if (lblSelectedOption.Text == "MAB")
					{
						if (!checkForTimingInDsFlight(lblSelectedOption.Text, "A"))
						{
							CtrlFlight.UpdateArrivalFlightTiming(null, FlightURNO, lblSelectedOption.Text, TimeInOracleFormat, StaffID);
						}
						if (!checkForTimingInDsFlight(lblSelectedOption.Text, "D"))
						{
							CtrlFlight.UpdateDepartureFlightTiming(null, TURN, lblSelectedOption.Text, TimeInOracleFormat, StaffID);
						}

					}
					else if (lblSelectedOption.Text == "LOAD-START" || lblSelectedOption.Text == "LOAD-END" || lblSelectedOption.Text == "LAST-DOOR" || lblSelectedOption.Text == "PLB(D)" || lblSelectedOption.Text == "ATD")
					{
						if (lblSelectedOption.Text == "PLB(D)")
						{
							if (!checkForTimingInDsFlight("PLB", "D"))
							{
								CtrlFlight.UpdateDepartureFlightTiming(null, TURN, "PLB", TimeInOracleFormat, StaffID);
							}
						}
						else
						{
							if (!checkForTimingInDsFlight(lblSelectedOption.Text, "D"))
							{
								CtrlFlight.UpdateDepartureFlightTiming(null, TURN, lblSelectedOption.Text, TimeInOracleFormat, StaffID);
							}
						}
					}
					else if (lblSelectedOption.Text == "PLB(A)")
					{
						if (!checkForTimingInDsFlight("PLB", "A"))
						{
							CtrlFlight.UpdateArrivalFlightTiming(null, FlightURNO, "PLB", TimeInOracleFormat, StaffID);
						}
					}
					else
					{
						if (!checkForTimingInDsFlight(lblSelectedOption.Text, "A"))
						{
							CtrlFlight.UpdateArrivalFlightTiming(null, FlightURNO, lblSelectedOption.Text, TimeInOracleFormat, StaffID);
						}
					}
				}
				else if (selFlt.ADID == "A")
				{
					if (!checkForTimingInDsFlight(lblSelectedOption.Text, "A"))
					{
						CtrlFlight.UpdateArrivalFlightTiming(null, FlightURNO, lblSelectedOption.Text, TimeInOracleFormat, StaffID);
					}
				}
				else if (selFlt.ADID == "D")
				{
					if (!checkForTimingInDsFlight(lblSelectedOption.Text, "D"))
					{
						CtrlFlight.UpdateDepartureFlightTiming(null, FlightURNO, lblSelectedOption.Text, TimeInOracleFormat, StaffID);
					}
				}


				if (selFlt.ADID == "T")
				{
					if (lblSelectedOption.Text.Trim() == "ATD")
					{
						Session["ClosedFlight"] = FlightURNO;
					}
				}
				else if (lblSelectedOption.Text.Trim() == "ATD" || lblSelectedOption.Text.Trim() == "UNLOAD-END")
				{
					Session["ClosedFlight"] = FlightURNO;
				}
			}
			else
			{
				RedirectToMobilePage("InvalidPage.aspx");
			}
		}
		catch (Exception ex)
		{
			iTrekUtils.iTUtils.LogToEventLog("updateFlightTiming" + ex.Message, "iTrekError");
			throw;
		}
	}


	private void updateFlightList()
	{
		getOpenFlights();
	}


	private void populateLstTime(string timeMode)
	{
		txtVAlertHdr.Text = "";

		Flight selFlt = null;
		try
		{
			selFlt = MgrSess.GetFlight(Session);
		}
		catch (Exception ex)
		{
		}

		if (selFlt != null)
		{
			if (txtVFlightDetailsOpenFlightMenu.Text == selFlt.DISPLAYFLIGHTSMENU)
			{
				lstTime.Items.Clear();
				ActiveForm = frmTiming;
				txtVFlightDetailsTiming.Text = MgrSess.GetFlight(Session).DISPLAYFLIGHTSMENU;

				lblSelectedOption.Text = timeMode;
				lblTime.Text = timeMode;
				DateTime currentTime = DateTime.Now;

				lstTime.Items.Add("NOW " + currentTime.ToString("HH:mm"));
				for (double d = -1.0; d >= -9.0; d--)
				{
					lstTime.Items.Add(currentTime.AddMinutes(d).ToString("HH:mm"));
				}
				txtVFlightDetailsOpenFlightMenu.Text = "";
			}
			else
			{
				getOpenFlights();
			}
		}
	}

	private void getUpdatedCPMValues()
	{
	}

	protected override void OnViewStateExpire(EventArgs e)
	{
		iTrekUtils.iTUtils.LogToEventLog("ViewState Expired in menu", "iTrekError");
		RedirectToMobilePage("Login.aspx");
	}


	protected void cmdError_Click(object sender, EventArgs e)
	{
		iTXMLcl iTXML = new iTXMLcl();
		if (Context.Session.Count == 0)
		{
			RedirectToMobilePage("Login.aspx");
		}
		else
		{
			string PENO = MgrSess.GetUser(Session);
			CtrlCurUser.DeleteDeviceIdEntryByStaffID(PENO);

			Session.Clear();
			Session.Abandon();

			RedirectToMobilePage("Login.aspx");
		}
	}


	private int getClosedFlightsCount()
	{
		DSFlight ds = null;
		try
		{
			ds = CtrlFlight.RetrieveClosedFlightsForAO(MgrSess.GetUser(Session), 3);
		}
		catch (Exception)
		{
		}
		if ((ds == null) || (ds.FLIGHT.Rows.Count < 1))
		{
			return 0;
		}
		else
		{
			return ds.FLIGHT.Rows.Count;
		}
	}


	protected void cmdYes_Click(object sender, EventArgs e)
	{
		Flight selFlt = null;
		try
		{
			selFlt = MgrSess.GetFlight(Session);
		}
		catch (Exception)
		{
		}

		if (selFlt != null)
		{
			if (txtVAlertHdr.Text == selFlt.DISPLAYFLIGHTSMENU)
			{
				if (lblOption.Text == "LAST-DOOR")
				{
					populateLstTime("LAST-DOOR");
				}
				else if (lblOption.Text == "UNLOAD-END")
				{
					populateLstTime("UNLOAD-END");
					Session["LTRIP"] = selFlt.URNO;
				}
				else
				{
					getOpenFlights();
				}
			}
			else
			{
				getOpenFlights();
			}
		}
		else
		{
			getOpenFlights();
		}
	}
	protected void cmdNo_Click(object sender, EventArgs e)
	{
		Flight selFlt = MgrSess.GetFlight(Session);
		if (selFlt != null)
		{
			if (txtVAlertHdr.Text == selFlt.DISPLAYFLIGHTSMENU)
			{
				if (lblOption.Text == "LAST-DOOR")
				{
					RedirectToMobilePage("DepAprTrip.aspx");
				}
				else if (lblOption.Text == "UNLOAD-END")
				{
					RedirectToMobilePage("ArrAprTrip.aspx");
				}
				else
				{
					getOpenFlights();
				}
			}
			else
			{
				getOpenFlights();

			}
		}
		else
		{
			getOpenFlights();
		}
	}
}