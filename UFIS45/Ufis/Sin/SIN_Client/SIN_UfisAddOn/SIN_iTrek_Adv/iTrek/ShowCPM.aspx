<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ShowCPM.aspx.cs" Inherits="WAP_ShowCPM" EnableSessionState="True"%>

<%@ Register Src="MWU_CompanyHeading.ascx" TagName="MWU_CompanyHeading" TagPrefix="uc2" %>

<%@ Register Src="MWUShowCpm.ascx" TagName="MWUShowCpm" TagPrefix="uc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmShowCPM" runat="server">
        <uc2:MWU_CompanyHeading ID="MWU_CompanyHeading1" runat="server" />
        <mobile:Label ID="Label1" Runat="server" Alignment="Center">CONTAINERS</mobile:Label>
        <uc1:MWUShowCpm id="MWUShowCpm1" runat="server">
        </uc1:MWUShowCpm>
        <mobile:Command ID="cmdBack" Runat="server" OnClick="cmdBack_Click">Back</mobile:Command>

    </mobile:Form>
</body>
</html>
