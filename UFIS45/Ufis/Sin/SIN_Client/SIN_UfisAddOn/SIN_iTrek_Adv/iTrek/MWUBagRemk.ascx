<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MWUBagRemk.ascx.cs" Inherits="WAP_MWUBagRemk" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<mobile:SelectionList ID="slstPreDefRmk" Runat="server" SelectType="CheckBox" DataTextField="MSGTEXT" DataValueField="URNO" OnSelectedIndexChanged="slstPreDefRmk_SelectedIndexChanged"></mobile:SelectionList> 
<mobile:TextBox ID="txtUserRmk" Runat="server" Wrapping="Wrap" MaxLength="50">
</mobile:TextBox> <mobile:Command ID="cmdBagRemkSave" Runat="server" OnClick="cmdBagRemkSave_Click" SoftkeyLabel="Save">Save</mobile:Command> 
<mobile:Command ID="cmdBagRmkBack" Runat="server" CausesValidation="False" EnableViewState="False" OnClick="cmdBagRmkBack_Click" SoftkeyLabel="Back">Back</mobile:Command>
