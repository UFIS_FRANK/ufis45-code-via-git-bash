using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class WAP_Timing : System.Web.UI.MobileControls.MobilePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        mwuTiming1.TimingSendClick += new WAP_MWUTiming.TimingSendHandler(mwuTiming1_TimingSendClick);
        if (IsPostBack)
        {
        }
        else
        {
            mwuTiming1.SetDateTimeList();
        }
    }

    void mwuTiming1_TimingSendClick(object sender, UFIS.ITrekLib.EventArg.EaTextEventArgs e)
    { //Send Timing
        string st = e.Text;
        Console.WriteLine(st);
        //Response.Redirect("CPM_Ap_Arr.aspx?dt="+ st);
    }

    private void KeepData()
    {
        this.HiddenVariables.Remove("back");
        this.HiddenVariables.Add("back", "frmTiming");
       
    }
    protected void frmTiming_Deactivate(object sender, EventArgs e)
    {
        Console.WriteLine("frmTiming_DeActivate");
    }
    protected void frmTiming_Activate(object sender, EventArgs e)
    {
        Console.WriteLine("frmTiming_Activate");
    }
    protected void cmdSend_Click(object sender, EventArgs e)
    {
    }
}


