<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Logout.aspx.cs" Inherits="Logout" EnableSessionState="False"%>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form ID="frmLogout" Runat="server">
        <mobile:Label ID="lblTitle2" Runat="server" Alignment="Center" EnableViewState="False">SATS iTrek</mobile:Label>
        <mobile:Label ID="lblLogout" Runat="server" Alignment="Center" EnableViewState="False">
        </mobile:Label>
          <mobile:TextView ID="txVLogout" Runat="server" EnableViewState="False">
        </mobile:TextView>
         <mobile:Command ID="cmdLogin" Runat="server" OnClick="cmdLogin_Click" SoftkeyLabel="Go" EnableViewState="False">
        </mobile:Command>
    </mobile:Form>
</body>
</html>
