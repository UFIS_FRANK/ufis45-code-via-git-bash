using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.EventArg;

public partial class WAP_MWUTiming : System.Web.UI.MobileControls.MobileUserControl
{
    private int _noOfTiming = 10;
    private string HHMMFormat = "HHmm";

    public int NumberOfTiming
    {
        get { return _noOfTiming; }
        set
        {
            if (value < 1) _noOfTiming = 1;
            else if (value > 100) _noOfTiming = 100; 
            else _noOfTiming = value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SetDateTimeList();
    }

    public void SetDateTimeList()
    {
        int noOfTiming = NumberOfTiming;
        lstTime.Items.Clear();
        lstTime.Decoration = ListDecoration.Numbered;

        DateTime currentTime = DateTime.Now;
        //DataSet ds = new DataSet();
        string time;
        lstTime.Items.Add("NOW " + currentTime.ToString(HHMMFormat));
        for (int i = 1; i < noOfTiming; i++) 
        {
            time = currentTime.AddMinutes(-i).ToString(HHMMFormat) + "   ";
           
            lstTime.Items.Add(time);
        }
    }
    public void setTextBoxLabel(string txtLabel)
    {
        this.lblTiming.Text = txtLabel;
    
    }
    public string getTextBoxLabel()
    {
       return this.lblTiming.Text;

    }

    protected void lstTime_ItemCommand(object sender, ListCommandEventArgs e)
    {
        string selectedTime = e.ListItem.Text;
        selectedTime = selectedTime.Replace("NOW ", "").Trim();
        txtTiming.Text = selectedTime;
        SendTiming(selectedTime);

    }

    private void SendTiming(string timing)
    {
        //txtTiming.Text = timing;
        txtTiming.Text = string.Empty;
        try
        {
            EaTextEventArgs arg = new EaTextEventArgs(timing);
            TimingSendClick(this, arg);

        }
        catch (Exception)
        {
        }        
    }

    protected void cmdTimingSend_Click(object sender, EventArgs e)
    {
        SendTiming(txtTiming.Text);
    }


    /////////////////
    //// add a delegate

    public delegate void TimingSendHandler(object sender,
        EaTextEventArgs e);


    //// add an event of the delegate type

    public event TimingSendHandler TimingSendClick;

 


}
