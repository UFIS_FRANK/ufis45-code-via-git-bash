<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MWUTiming.ascx.cs" Inherits="WAP_MWUTiming" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>

<mobile:List ID="lstTime" Runat="server" Decoration="Numbered" OnItemCommand="lstTime_ItemCommand"></mobile:List> 
<mobile:Label ID="lblTiming" Runat="server" Alignment="Left"  ></mobile:Label> 
<mobile:Label ID="lblDummy1" Runat="server" Alignment="Left"  ></mobile:Label> 
<mobile:TextBox ID="txtTiming" Runat="server" MaxLength="4" Numeric="True" Size="4" Alignment="Left"  >
</mobile:TextBox>
<mobile:Label ID="lblDummy2" Runat="server" Alignment="Center"  ></mobile:Label> 
 <mobile:Command ID="cmdTimingSend" Runat="server" OnClick="cmdTimingSend_Click" SoftkeyLabel="Go" Alignment="Left">Go</mobile:Command>
