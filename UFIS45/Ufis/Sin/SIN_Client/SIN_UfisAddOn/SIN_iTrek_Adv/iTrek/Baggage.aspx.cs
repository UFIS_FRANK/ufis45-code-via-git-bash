using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using iTrekSessionMgr;
using iTrekXML;
using UFIS.Wap.ITrek.WebMgr;
using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.Util;
using MM.UtilLib;



public partial class Baggage : System.Web.UI.MobileControls.MobilePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string uId = "";
        MgrSec.IsAuthenticatedUser(this, Session, out uId);
        try
        {
            string menuOption = Request.QueryString["MENU"].Trim().ToUpper();
            switch (menuOption)
            {
                case "OP":
                    selectedFlightMenu();
                    break;
                case "SELFLT":
                    selectedFlightMenu();
                    break;

                //case "CL":

            }
        }
        catch (Exception ex)
        {//Form.PagerStyle.NextPageText 

            //swallow error - ex.Message;
        }
        try
        {
            DSTerminal dsTerminal = CtrlTerminal.RetrieveTerminal();
            lstTerminal.DataSource = dsTerminal;
            lstTerminal.DataMember = "TERMINAL";
            lstTerminal.DataTextField = "TERM_NAME";
            lstTerminal.DataValueField = "TERM_ID";
            lstTerminal.DataBind();
            string[] menu = new string[] {  "Messages" ,"Logout"};
            lstMenu.DataSource = menu;
            lstMenu.DataBind();
            lblUserIdName1.Text = MgrSess.GetUser(Session) + "-" + Session["staffName"];
        }
            
        
        catch (Exception lEx)
        {
            iTrekUtils.iTUtils.LogToEventLog("From Baggage" + lEx.Message);
        
        
        }
    }


    private string UserId
    {

        get
        {
            string userId = MgrSess.GetUser(Session);
            //userId = MgrSess.GetUserId();
            return userId;
        }
    }
    private ArrayList GetFlightListFromFlightDataset(DSFlight ds)
    {
        ArrayList lstFlight = new ArrayList();
        foreach (DSFlight.FLIGHTRow row in ds.FLIGHT.Select())
        {
			if (row.IsTTYPNull()) row.TTYP = "";
            Flight flight = new Flight(row.URNO, row.FLNO, row.ST, row.ST, row.REGN, row.TO, row.FR, row.ET, row.ET, row.GATE, row.GATE, row.A_D, row.PARK_STAND, row.PARK_STAND, row.BELT, row.OBL, row.OBL, row.TERMINAL, row.TERMINAL, row.TTYP, row.AIRCRAFT_TYPE, row.TURN,row.UAFT);
            lstFlight.Add(flight);
        }
        return lstFlight;

    }
    protected void lstBOMainMenu_ItemCommand(object sender, ListCommandEventArgs e)
    {

        cmdNavigate.Text = "Previous Flights";
        if (e.ListItem.Text == "Arr Flights")
        {
            getNextTenFlights("A");
        }
        else
            if (e.ListItem.Text == "Dep Flights")
        {
            getNextTenFlights("D");
        }
        else
            if (e.ListItem.Text == "Messages")
            {
                RedirectToMobilePage("Messages.aspx");

            }
           
        
    }
    protected void cmdNavigate_Click(object sender, EventArgs e)
    {
        if (cmdNavigate.Text == "Previous Flights")
        {
            getPreviousTenFlights(Session["FlightADID"].ToString());
            cmdNavigate.Text = "Next Flights";

        }
        else
            if (cmdNavigate.Text == "Next Flights")
            {
                getNextTenFlights(Session["FlightADID"].ToString());
                cmdNavigate.Text = "Previous Flights";
            }


    }



    private void getNextTenFlights(string flightType)
    {

        iTXMLPathscl itpaths = new iTXMLPathscl();
        iTXMLcl iTXML = new iTXMLcl(itpaths.applicationBasePath);
        SessionManager sessMan = new SessionManager();
        ArrayList lstFlights = new ArrayList();
        lstFlights.Clear();
        lstFlightsMenu.Items.Clear();
        Session["FlightsList"] = null;
       
        Session["FlightADID"] = flightType;
        int i = 0;
       
        
        DSFlight dsFlight = new DSFlight();
        if (flightType == "A")
        {
            dsFlight = CtrlFlight.RetrieveArrivalFlights(Session["Terminal"].ToString(), DateTime.Now, DateTime.Now.AddHours(8.0));
        }
        else if (flightType == "D")
        {

            dsFlight = CtrlFlight.RetrieveDepartureFlights(Session["Terminal"].ToString(), DateTime.Now, DateTime.Now.AddHours(8.0));
        }
        lstFlights = GetFlightListFromFlightDataset(dsFlight);


        if (lstFlights != null && lstFlights.Count != 0)
        {
           // lstFlights.Sort();        
           
            lstFlights = iTXML.ReorderFlightsEarliestOnTop(lstFlights);
        }
      
        ArrayList taskList = CtrlTasks.GetAirlineCode("BAGGAGE");
        Hashtable ht = new Hashtable(lstFlights.Count);
        string alc2 = "";
        if (lstFlights != null && lstFlights.Count != 0)
        {
           
            ht.Clear();
            // lstOpenFlights.ItemCount = lstFlights.Count;
            foreach (Flight flts in lstFlights)
            {
                try
                {
                    alc2 = flts.FLNO.Substring(0, 3);
                }
                catch (Exception)
                {
                    
                    
                }
                if (taskList.Contains(alc2.Trim()) && i < 10)
                {
                    ht.Add(i, flts);
                    string lstItem = flts.DISPLAYBOFLIGHT;

                    MobileListItem mb = new MobileListItem();


                    mb.Text = lstItem;



                    lstFlightsMenu.Items.Add(mb);

                    i++;
                }
            }
        }

        ActiveForm = frmBOFlights;
        Session["FlightsList"] = ht;
        if (flightType == "A")
        {
            lblHdng.Text = "Baggage Arrival";
        }
        else if (flightType == "D")
        {
            lblHdng.Text = "Baggage Departure";
        
        }
    
    
    
    
    }

    private void getPreviousTenFlights(string flightType)
    {

        iTXMLPathscl itpaths = new iTXMLPathscl();
        iTXMLcl iTXML = new iTXMLcl(itpaths.applicationBasePath);
        SessionManager sessMan = new SessionManager();
        ArrayList lstFlights = new ArrayList();
        lstFlights.Clear();
        lstFlightsMenu.Items.Clear();
         Session["FlightADID"] = flightType;
         Session["FlightsList"] = null;
        int i = 0;
       // lstFlights = iTXML.ReadPreviousTenFlightElementsFromCurrentFlightXMLFile("//FLIGHTS/FLIGHT", itpaths.GetFlightsFilePath(), flightType,Session["ORGCODE"].ToString());
        //lstFlights = iTXML.ReadPreviousTenFlightElementsFromCurrentFlightXMLFile("//FLIGHTS/FLIGHT", itpaths.GetFlightsFilePath(), flightType, Session["Terminal"].ToString());

        //lstFlights = iTXML.ReorderFlightsEarliestOnTop(lstFlights);
        DSFlight dsFlight = new DSFlight();
        if (flightType == "A")
        {
             dsFlight = CtrlFlight.RetrieveArrivalFlights(Session["Terminal"].ToString(), DateTime.Now.AddHours(-4.0), DateTime.Now);
        }
        else if (flightType == "D")
        {

            dsFlight = CtrlFlight.RetrieveDepartureFlights(Session["Terminal"].ToString(), DateTime.Now.AddHours(-4.0), DateTime.Now);
        }
        lstFlights = GetFlightListFromFlightDataset(dsFlight);
        //lstFlights = iTXML.ReadNextTenFlightElementsFromCurrentFlightXMLFile("//FLIGHTS/FLIGHT", itpaths.GetFlightsFilePath(), flightType, Session["Terminal"].ToString());

        if (lstFlights != null && lstFlights.Count != 0)
        {
            //lstFlights.Sort();

            lstFlights = iTXML.ReorderFlightsEarliestOnTop(lstFlights);
        }
        lstFlights.Reverse();
        ArrayList taskList=CtrlTasks.GetAirlineCode("BAGGAGE");
        Hashtable ht = new Hashtable(lstFlights.Count);
        string alc2 = "";
        bool containsAlc2 = false;
        Hashtable flightHt = new Hashtable();
        if (lstFlights != null && lstFlights.Count != 0)
        {
            
            ht.Clear();
            // lstOpenFlights.ItemCount = lstFlights.Count;
            foreach (Flight flts in lstFlights)
            {
                try
                {
                    alc2 = flts.FLNO.Substring(0, 3);
                }
                catch (Exception)
                {


                }
                if (taskList.Contains(alc2.Trim()) && i < 10)
                {
                    ht.Add(i, flts);
                    i++;
                    containsAlc2 = true;
                }
            }
            int k = 0;
            flightHt.Clear();
            if (containsAlc2)
            {
                for (int j = i - 1; j >= 0; j--)
                {

                    Flight flight = (Flight)ht[j];
                    string lstItem = flight.DISPLAYBOFLIGHT;
                    MobileListItem mb = new MobileListItem();
                    mb.Text = lstItem;
                    lstFlightsMenu.Items.Add(mb);
                    flightHt.Add(k, flight);
                    k++;
                }
            }
        }

        ActiveForm = frmBOFlights;
        Session["FlightsList"] = flightHt;
        if (flightType == "A")
        {
            lblHdng.Text = "Baggage Arrival";
        }
        else if (flightType == "D")
        {
            lblHdng.Text = "Baggage Departure";

        }




    }


    private void selectedFlightMenu()
    {

        Flight flt = MgrSess.GetFlight(Session);
        if (flt != null)
        {
        string fltADID = flt.ADID;
        iTrekSessionMgr.SessionManager sessMan = new iTrekSessionMgr.SessionManager();
        iTrekXML.iTXMLPathscl iTPaths = new iTrekXML.iTXMLPathscl();
        iTXMLcl iTXML = new iTXMLcl(iTPaths.applicationBasePath);
        int rowCntToSelect = 0;
        int arrCnt = 0;
        string valueInArray = "";
        txtTiming.Text = "";
        ArrayList arrList = new ArrayList();
        ArrayList depList = new ArrayList();
        string[] strDep = new string[] { "FTRIP", "NEXT-TRIP", "LTRIP" };
        string[] strArr = new string[] { "FTRIP", "FIRST BAG", "NEXT-TRIP", "LTRIP", "LAST BAG" };
        if (flt.ADID == "D")
        {
           // CtrlCPM.CheckExistCPMForAFlight(flt.URNO);

            CtrlCPM.CheckExistCPM(flt.URNO);

           
            DSFlight dsFlt = CtrlFlight.RetrieveFlightsByFlightIdWithDoorClosed(flt.URNO);
            int cnt = dsFlt.Tables["Flight"].Rows.Count;
            if (cnt > 0)
            {
                if (!(flt.lastDoorStatus))
                {
                    flt.DISPLAYBOFLIGHTMENU += "Last Door Closed";
                }

            }
        }
        else
        {
            int cntSin, cntInt = 0;
            CtrlCPM.CheckExistCPM(flt.URNO);
            if (!(flt.lastDoorStatus))
            {
                flt.DISPLAYBOFLIGHTMENU += CtrlCPM.GetUldInfoForAFlight(flt.URNO, out cntSin, out cntInt);
            }

            

        }
        //Session["userFlight"] = flt;

        MgrSess.SetFlight(Session, flt);
        lblFltHdng.Text = flt.DISPLAYBOFLIGHTMENU;
        if (fltADID == "D")
        {
            depList.Add("PREP.CONTAINER");
            DSDepartureFlightTimings dsDepflightTiming = CtrlFlight.RetrieveDepartureFlightTimings(flt.URNO);
            DSDepartureFlightTimings.DFLIGHTRow rowDep = null;
            if (dsDepflightTiming.DFLIGHT.Rows.Count > 0)
            {
                rowDep = ((DSDepartureFlightTimings.DFLIGHTRow)dsDepflightTiming.DFLIGHT.Rows[0]);
            }

            if (rowDep == null || rowDep.FTRIP == "" || rowDep.FTRIP == "-")
            {
               // depList.Add("FTRIP");
            }
            else
            {
                txtTiming.Text += "FTRIP  " + UtilTime.ConvUfisTimeStringToDateTime(rowDep.FTRIP.ToString()).ToString("HH:mm") + "</br>";
            }
            depList.Add("TRIPS");
            if (rowDep == null || rowDep.LTRIP == "" || rowDep.LTRIP == "-")
            {
               // depList.Add("LTRIP");
            }
            else
            {
               // depList.Add("LTRIP");
                txtTiming.Text += "LTRIP  " + UtilTime.ConvUfisTimeStringToDateTime(rowDep.LTRIP.ToString()).ToString("HH:mm") + "</br>";
            }
          //  DataSet dsDepflightTiming = iTXML.getDepartureFlightTimingsFromXML(flt.URNO);
           
            depList.Add("SEND MESSAGE");
            depList.Add("VIEW MESSAGE");
            depList.Add("SHOW TRIPS");
            lstSelFltMenu.DataSource = depList;
            lstSelFltMenu.DataBind();
            ActiveForm = frmSelectedFltMenu;

        }
        else
        {
            DSArrivalFlightTimings dsArrFlightTiming = CtrlFlight.RetrieveArrivalFlightTimings(flt.URNO);
            DSArrivalFlightTimings.AFLIGHTRow row = null;
            if (dsArrFlightTiming.AFLIGHT.Rows.Count > 0)
            {
                row = ((DSArrivalFlightTimings.AFLIGHTRow)dsArrFlightTiming.AFLIGHT.Rows[0]);
            }

            if (row == null || row._FTRIP_B == "" || row._FTRIP_B == "-")
            {
                arrList.Add("FTRIP");
            }
            else 
            {
                txtTiming.Text += "FTRIP "  + UtilTime.ConvUfisTimeStringToDateTime(row._FTRIP_B.ToString()).ToString("HH:mm") + "</br>";
            }
            if (row == null || row.FBAG == "" || row.FBAG == "-")
            {
                arrList.Add("FIRST BAG");
            }
            else
            {
                txtTiming.Text += "FIRST BAG " + UtilTime.ConvUfisTimeStringToDateTime(row.FBAG.ToString()).ToString("HH:mm") + "</br>";
            }
            arrList.Add("NEXT-TRIP");
            if (row == null || row._LTRIP_B == "" || row._LTRIP_B == "-")
            {
                arrList.Add("LTRIP");
            }
            else
            {
                arrList.Add("LTRIP");
                txtTiming.Text += "LTRIP " + UtilTime.ConvUfisTimeStringToDateTime(row._LTRIP_B.ToString()).ToString("HH:mm") + "</br>";
            }
            if (row == null || row.LBAG == "" || row.LBAG == "-")
            {
                arrList.Add("LAST BAG");
            }
            else
            {
                arrList.Add("LAST BAG");
                txtTiming.Text += "LAST BAG  " + UtilTime.ConvUfisTimeStringToDateTime(row.LBAG.ToString()).ToString("HH:mm") + "</br>";
            }
           
            
            arrList.Add("SHOW CPM");
            arrList.Add("SEND MESSAGE");
            arrList.Add("VIEW MESSAGE");
            lstSelFltMenu.DataSource = arrList;
            lstSelFltMenu.DataBind();
            ActiveForm = frmSelectedFltMenu;
        }
    }
    else
    {
        RedirectToMobilePage("InvalidPage.aspx");

    }
    
    
    }
    protected void lstFlightsMenu_ItemCommand(object sender, ListCommandEventArgs e)
    {
        int selIndex = e.ListItem.Index;
        Flight flt = ((Flight)((Hashtable)(Session["FlightsList"]))[selIndex]);
        //Session["userFlight"] = flt;
        MgrSess.SetFlight(Session, flt);
        selectedFlightMenu();
        
    }
    protected void lstSelFltMenu_ItemCommand(object sender, ListCommandEventArgs e)
    {
        string listItem = e.ListItem.Text.Trim();
        getFlightMenu(listItem);
    }
    private void getFlightMenu(string listItem)
    {
       
        iTXMLPathscl itpaths = new iTXMLPathscl();
        iTXMLcl itxml = new iTXMLcl(itpaths.applicationBasePath);
        SessionManager sessMan = new SessionManager();
        //Flight selFlt = ((Flight)Session["userFlight"]);
        Flight selFlt = MgrSess.GetFlight(Session);
        if (listItem.Length > 8)
        {
            if (listItem.Substring(0, 8) == "LAST BAG")
            {
                listItem = "LAST BAG";
            }
        }


        if (listItem.Length > 5)
        {
            if (listItem.Substring(0, 5) == "LTRIP")
            {
                listItem = "LTRIP";
            }
        }


        if (selFlt.ADID == "D")
        {
            if (listItem == "PREP.CONTAINER")
            {
                txtVSelFlt.Text = selFlt.DISPLAYBOFLIGHTMENU;
                txtBox1.Text = "";
                txtBox10.Text = "";
                txtBox11.Text = "";
                txtBox12.Text = "";
                txtBox13.Text = "";
                txtBox14.Text = "";
                txtBox15.Text = "";
                txtBox16.Text = "";
                txtBox17.Text = "";
                txtBox18.Text = "";
                txtBox19.Text = "";
                txtBox2.Text = "";
                txtBox20.Text = "";
                txtBox3.Text = "";
                txtBox4.Text = "";
                txtBox5.Text = "";
                txtBox6.Text = "";
                txtBox7.Text = "";
                txtBox8.Text = "";
                txtBox9.Text = "";

                ActiveForm = frmPrepContainer;

            }
            else
               // if (listItem == "FTRIP" || listItem == "NEXT-TRIP" || listItem == "LTRIP")
                if (listItem =="TRIPS")
                {
                    RedirectToMobilePage("BaggageHandling.aspx?Selected=" + listItem);
                }
                else if (listItem == "SEND MESSAGE")
                {

                    RedirectToMobilePage("Messages.aspx?newmessage=yes");


                }
                else if (listItem == "VIEW MESSAGE")
                {

                    RedirectToMobilePage("Messages.aspx?newmessage=flight");

                }
                else if (listItem == "SHOW TRIPS")
                {
                    RedirectToMobilePage("ShowCPM.aspx?selection=trips");


                }


        }
        else
        {
            if (listItem == "FIRST BAG" || listItem == "LAST BAG")
            {
                RedirectToMobilePage("BaggageHandling.aspx?Selected=" + listItem);

            }
            else if (listItem == "SEND MESSAGE")
            {

                RedirectToMobilePage("Messages.aspx?newmessage=yes");


            }
            else if (listItem == "SHOW CPM")
            {

               RedirectToMobilePage("ShowCPM.aspx?selection=cpm");


            }
            else if (listItem == "VIEW MESSAGE")
            {

                RedirectToMobilePage("Messages.aspx?newmessage=flight");

            }
            else
            {
                RedirectToMobilePage("BaggageHandlingArr.aspx?Selected=" + listItem);
            }
            

        }
    }
    protected void cmdSavePrepContainer_Click(object sender, EventArgs e)
    {
        ArrayList alContainer = new ArrayList();
        //Flight selFlt = ((Flight)Session["userFlight"]);
       Flight selFlt = MgrSess.GetFlight(Session);
        iTXMLPathscl iTPaths = new iTXMLPathscl();
        iTXMLcl iTXML = new iTXMLcl(iTPaths.applicationBasePath);
        if (txtBox1.Text.Trim() != "")
        {
            alContainer.Add(txtBox1.Text);
        }
        if (txtBox2.Text.Trim() != "")
        {
            alContainer.Add(txtBox2.Text);
        }
        if (txtBox3.Text.Trim() != "")
        {
            alContainer.Add(txtBox3.Text);
        }
        if (txtBox4.Text.Trim() != "")
        {
            alContainer.Add(txtBox4.Text);
        }
        if (txtBox5.Text.Trim() != "")
        {
            alContainer.Add(txtBox5.Text);
        }
        if (txtBox6.Text.Trim() != "")
        {
            alContainer.Add(txtBox6.Text);
        }
        if (txtBox7.Text.Trim() != "")
        {
            alContainer.Add(txtBox7.Text);
        }
        if (txtBox8.Text.Trim() != "")
        {
            alContainer.Add(txtBox8.Text);
        }
        if (txtBox9.Text.Trim() != "")
        {
            alContainer.Add(txtBox9.Text);
        }
        if (txtBox10.Text.Trim() != "")
        {
            alContainer.Add(txtBox10.Text);
        }
        if (txtBox11.Text.Trim() != "")
        {
            alContainer.Add(txtBox11.Text);
        }
        if (txtBox12.Text.Trim() != "")
        {
            alContainer.Add(txtBox12.Text);
        }
        if (txtBox13.Text.Trim() != "")
        {
            alContainer.Add(txtBox13.Text);
        }
        if (txtBox14.Text.Trim() != "")
        {
            alContainer.Add(txtBox14.Text);
        }
        if (txtBox15.Text.Trim() != "")
        {
            alContainer.Add(txtBox15.Text);
        }
        if (txtBox16.Text.Trim() != "")
        {
            alContainer.Add(txtBox16.Text);
        }
        if (txtBox17.Text.Trim() != "")
        {
            alContainer.Add(txtBox17.Text);
        }
        if (txtBox18.Text.Trim() != "")
        {
            alContainer.Add(txtBox18.Text);
        }
        if (txtBox19.Text.Trim() != "")
        {
            alContainer.Add(txtBox19.Text);
        }
        if (txtBox20.Text.Trim() != "")
        {
            alContainer.Add(txtBox20.Text);
        }

        CtrlCPM.InsertNewContainerIntoDB(selFlt.URNO, alContainer, UserId);
       // iTXML.createContainerXmlForFlight(selFlt.URNO,alContainer);
        ActiveForm = frmSelectedFltMenu;
    }



    protected void cmdBackFromFltMenu_Click(object sender, EventArgs e)
    {
        ActiveForm = frmBoMainMenu;
    }
    protected void cmBackFromSelFlightMenu_Click(object sender, EventArgs e)
    {
        if (cmdNavigate.Text == "Previous Flights")
        {
            getNextTenFlights(Session["FlightADID"].ToString());
            
            //cmdNavigate.Text = "Next Flights";

        }
        else
            if (cmdNavigate.Text == "Next Flights")
            {
                getPreviousTenFlights(Session["FlightADID"].ToString());
                
            }
    }
    protected void cmdBackFromPrepCont_Click(object sender, EventArgs e)
    {
        ActiveForm = frmSelectedFltMenu;
    }
    protected override void OnViewStateExpire(EventArgs e)
    {


        iTrekUtils.iTUtils.LogToEventLog("ViewState Expired in Baggage", "iTrekError");
        RedirectToMobilePage("Login.aspx");

    }
    protected void lstTerminal_ItemCommand(object sender, ListCommandEventArgs e)
    {
        
        string[] boMenu = new string[] { "Arr Flights", "Dep Flights", "Messages" };
        lstBOMainMenu.DataSource = boMenu;
        lstBOMainMenu.DataBind();
        lblUserIdName.Text = MgrSess.GetUser(Session) + "-" + Session["staffName"];
        lblHdrTerm.Text = "Baggage " + e.ListItem.Text.ToString();         
        Session["Terminal"] = e.ListItem.Value.ToString();
        ActiveForm = frmBoMainMenu;
    }
    protected void lstMenu_ItemCommand(object sender, ListCommandEventArgs e)
    {
        if (e.ListItem.Text == "Messages")
        {
            RedirectToMobilePage("Messages.aspx");

        }
        else


            if (e.ListItem.Text == "Logout")
            {
                Session.Abandon();
                RedirectToMobilePage("Logout.aspx");

            }
    }
    protected void cmdBack_Click(object sender, EventArgs e)
    {
        ActiveForm = frmTerminal;
    }
}
