<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Messages.aspx.cs" Inherits="Messages" EnableSessionState="True"%>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
   <mobile:Form id="frmMessaages" runat="server">
        <mobile:Label ID="lblTitle" Runat="server" Alignment="Center" Wrapping="Wrap">SATS iTrek</mobile:Label>
        <mobile:Label ID="lblHeading" Runat="server" Alignment="Center" Wrapping="Wrap">MESSAGING</mobile:Label>
        <br />
        <mobile:List ID="lstMsgMenu" Runat="server" OnItemCommand="lstMsgMenu_ItemCommand">
            <Item Text="NEW" Value="New" />
            <Item Text="INBOX" Value="Text" />
            <Item Text="OUTBOX" Value="Text" />
            </mobile:List>
             <mobile:Command ID="cmdBack" Runat="server"  EnableViewState="False" OnClick="cmdBack_Click">Back</mobile:Command>
           </mobile:Form>
    <mobile:Form ID="frmNewMessage" Runat="server">
        <mobile:Label ID="lblTitle1" Runat="server" Alignment="Center">SATS iTrek</mobile:Label>
        <mobile:Label ID="lblHeading1" Runat="server" Alignment="Center">MESSAGING</mobile:Label>
        <mobile:List ID="lstNewMsg" Runat="server" OnItemCommand="lstNewMsg_ItemCommand" Alignment="Left" OnDataBinding="lstNewMsg_DataBinding" Wrapping="NoWrap">
        </mobile:List>
         <mobile:Command ID="cmdBack1" Runat="server"  EnableViewState="False" OnClick="cmdBack1_Click" >Back</mobile:Command>
    </mobile:Form>
    <mobile:Form ID="frmFreeMsg" Runat="server">
        <mobile:Label ID="lblTitle2" Runat="server" Alignment="Center">SATS iTrek</mobile:Label>
        <br />
        <mobile:Label ID="lblHeading2" Runat="server" Alignment="Center">MESSAGING</mobile:Label>
        <br />
        <mobile:Label ID="lblErrMsg" Runat="server" Visible="False" EnableViewState="False">
        </mobile:Label>
        <br />
        
         <mobile:TextBox ID="txtFreeMsg" Runat="server" Alignment="Center" MaxLength="256" Size="20" Wrapping="Wrap" >
        </mobile:TextBox>
        <mobile:SelectionList ID="lstFlight" Runat="server" Rows="1" SelectType="CheckBox"
            Visible="False">
        </mobile:SelectionList>
        <mobile:SelectionList ID="chkAddresses" Runat="server" Rows="5" SelectType="CheckBox" EnableViewState="False">
            <Item Text="Send to ACC" Value="ACC" />
            <Item Text="Send to BCC" Value="BCC" />
            <Item Text="Send to SOCC" Value="SOCC" />
            <Item Text="Send to LCC" Value="LCC" />
            <Item Text="Send to All" Value="All" />
        </mobile:SelectionList>
        
        <br />
        <mobile:Command ID="cmdSendMsg" Runat="server" OnClick="cmdSendMsg_Click">Send</mobile:Command>
         <mobile:Command ID="cmdBack2" Runat="server"  EnableViewState="False" OnClick="cmdBack2_Click"  >Back</mobile:Command>
    </mobile:Form>
    <mobile:Form ID="frmInbox" Runat="server"><mobile:Label ID="lblTitle3" Runat="server" Alignment="Center">SATS iTrek</mobile:Label> <mobile:Label ID="lblHdng3" Runat="server" Alignment="Center"></mobile:Label> <br /><mobile:Label ID="lblInfo" Runat="server" Visible="False">
        </mobile:Label> 
        <mobile:List ID="lstInMsgs" Runat="server" OnItemCommand="lstInMsgs_ItemCommand" Wrapping="NoWrap" ></mobile:List>       


   <mobile:Command ID="cmdBack3" Runat="server"  EnableViewState="False" OnClick="cmdBack3_Click">Back</mobile:Command></mobile:Form>
    <mobile:Form ID="frmRecMsg" Runat="server">
        <mobile:Label ID="lblTitle4" Runat="server" Alignment="Center">SATS iTrek</mobile:Label>
        <mobile:Label ID="lblHdng4" Runat="server" Alignment="Center">MESSAGING</mobile:Label>
        <br />
        <mobile:Label ID="lblMsgHdr" Runat="server">
        </mobile:Label>
        <br />
        <mobile:TextView ID="txtRecMsg" Runat="server">TextView</mobile:TextView>
          <mobile:Command ID="cmdCloseMsg" Runat="server" OnClick="cmdCloseMsg_Click" >Close</mobile:Command>
    </mobile:Form>
</body>
</html>
