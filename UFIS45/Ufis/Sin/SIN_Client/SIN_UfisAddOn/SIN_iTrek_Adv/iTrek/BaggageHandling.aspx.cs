#define UsingQueues
#define TestingOn

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using iTrekSessionMgr;
using iTrekXML;
using UFIS.ITrekLib.EventArg;
using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.CustomException;
using UFIS.ITrekLib.DS;
using UFIS.Wap.ITrek.WebMgr;
using iTrekWMQ;
using UFIS.ITrekLib.Util;
using MM.UtilLib;




public partial class BaggageHandling : System.Web.UI.MobileControls.MobilePage
{
    private const string VS_CPM_AVAIL_IND = "VS_CPM_AVAIL_IND";
    private const string VS_CUR_CONTAINER_NO = "VS_CUR_CONTAINER_NO";
    private const string VS_CPM_AUTO_LIST = "VS_CPM_AUTO_LIST";
    //private const string VS_CPM_MANUAL_LIST = "VS_CPM_MANUAL_LIST";
    //private const string VS_CPM_NO_LIST = "VS_CPM_NO_LIST";
    private const string VS_CUR_ENT_DS_CPM = "VS_CUR_ENT_DS_CPM";


    private const string PFIX_RMK_LIST = "Rmk (";
    private const string SFIX_RMK_LIST = ")";
    private const string RMK_IND = "**";
    EnTripType trType=EnTripType.FirstTrip;

    protected void Page_Load(object sender, EventArgs e)
    {
        string uId = "";
        MgrSec.IsAuthenticatedUser(this, Session, out uId);
        MWUTiming1.TimingSendClick += new WAP_MWUTiming.TimingSendHandler(MWUTiming1_TimingSendClick);
        MWUBagRemk1.RemarkBack_Click += new WAP_MWUBagRemk.RemarkBackClickEventHandler(MWUBagRemk1_RemarkBack_Click);
        MWUBagRemk1.RemarkSaveClick += new WAP_MWUBagRemk.RemarkSaveClickEventHandler(MWUBagRemk1_RemarkSaveClick);
        //Flight selFlt = ((Flight)Session["userFlight"]);
        iTXMLPathscl itpaths = new iTXMLPathscl();

        iTXMLcl iTXML = new iTXMLcl(itpaths.applicationBasePath);
        Label2.Text = "";
        try
        {
//#if TestingOn
//            if (!IsPostBack)
//            {
//                LoadContainerInfo();
//            }
//            return;
//#endif
            
            if (selectedFlight.ADID == "A")
            {
                if (Request.QueryString["Selected"].ToString() == "FIRST BAG" || Request.QueryString["Selected"].ToString() == "LAST BAG")
                {
                    populateLstTime();
                    this.MWUTiming1.setTextBoxLabel(Request.QueryString["Selected"].ToString());
                }
            }

            else
            {
                //if (Request.QueryString["Selected"].ToString() == "FTRIP" || Request.QueryString["Selected"].ToString() == "NEXT-TRIP" || Request.QueryString["Selected"].ToString() == "LTRIP")
              //  {
                    try
                    {
                        if (!IsPostBack)
                        {

                            LoadContainerInfo();
                           // this.MWUTiming1.setTextBoxLabel(Request.QueryString["Selected"].ToString());
                        }

                    }
                    catch (Exception ex)
                    {
                        //ShowFormMsg("Error. " + ex.Message);
                    }

                    // iTXML.getContainerDetailsFromXML(selFlt.URNO);
               // }
            }
        }
        catch (Exception ex)
        {
        }
    }

    void MWUBagRemk1_RemarkSaveClick(object sender, EaRemarkSaveEventArgs e)
    {
        try
        {
            SaveAnRemark(CurContainerNo, e.Remk);
            ShowFormRemarkList();
        }
        catch (Exception ex)
        {
           //ShowFormMsg("Error. " + ex.Message);
        }
    }

    void MWUBagRemk1_RemarkBack_Click(object sender, EventArgs e)
    {
        ActiveForm = frmRemarkList;
    }

    private void SaveAnRemark(string containerNo, EntRemark rmk)
    {
        bool foundContainer = false;
        if (!foundContainer)
        {
            EntCPMList cpmList = CPMAutoList;
            int cnt = cpmList.Count;
            for (int i = 0; i < cnt; i++)
            {
                if (cpmList.GetData(i).ContainerNo == containerNo)
                {
                    cpmList.GetData(i).Remark = rmk;
                    CPMAutoList = cpmList;
                    foundContainer = true;
                    break;
                }
            }
        }
       // iTrekUtils.iTUtils.LogToEventLog("foundContainer" + foundContainer, "iTrekError");

        if (!foundContainer) throw new ApplicationException("Unable to find the container in the list");
    }


    private void LoadContainerInfo()
    {
        string cpmAvail = "";
        string errMsg = "";
        iTXMLPathscl itpaths = new iTXMLPathscl();

        iTXMLcl iTXML = new iTXMLcl(itpaths.applicationBasePath);
        txtVFltDetail.Text = selectedFlight.DISPLAYBOFLIGHTMENU;
        Label2.Text = "";
        
        try
        {



            DSCpm dsContainer = CtrlCPM.RetrieveUldNoNotSend(FlightUrNo);
            DSCpm dsContainerFull = CtrlCPM.RetrieveUldNos(FlightUrNo);
            if (dsContainer.CPMDET.Rows.Count == dsContainerFull.CPMDET.Rows.Count)
            {
                MWUTiming1.setTextBoxLabel("FTRIP");
                TripType = EnTripType.FirstTrip;
            }
            else
            {
                MWUTiming1.setTextBoxLabel("NEXT-TRIP");
                TripType = EnTripType.NextTrip;
            }
           
              
            if (dsContainer.CPMDET.Rows.Count != 0)
            {
                MWUCpmList1.Clear();
                MWUCpmList1.DataSource = dsContainer.CPMDET;
                MWUCpmList1.DataTextField = "ULD_TXT";
                MWUCpmList1.DataValueField = "URNO";
                MWUCpmList1.DataBind();
            }
            else
            {
               
                cmdRemarkForDep.Visible = false;
                Command1.Visible = false;
                Label2.Text = "Sorry,no containers to send";
                Label2.Visible = true;
            
            }
            ActiveForm = frmContainer;
               
        }
        catch (NoRecordException)
        {
            cpmAvail = "N";
        }
        catch (Exception ex)
        {
            errMsg = ex.Message;
        }
        if (cpmAvail == "") errMsg = "Error accessing CPM Information";     
    }

    private void populateLstTime()
    {
        txtVFltDetail.Text = "";
        txtVFlightDetailsTiming.Text = selectedFlight.DISPLAYBOFLIGHTMENU;
        lblSelectedOption.Text = this.MWUTiming1.getTextBoxLabel();          
        DateTime currentTime = DateTime.Now;
        ActiveForm = frmBOTiming;
    }

   

    protected void chkULD_SelectedIndexChanged(object sender, EventArgs e)
    {
        //string sel = chkULD.Selection.Text;
        //string sel1 = chkULD.Selection.Value;
    }

    //protected void cmdReceived_Click(object sender, EventArgs e)
    //{
    //    Hashtable htULDS = new Hashtable();
    //    foreach(MobileListItem chk in  chkULD.Items)
    //    {
    //        if (chk.Selected)
    //        {
    //            htULDS.Add(chk.Text,chk.Value);
    //        }        
    //    }
    //}

    protected void cmdBack_Click(object sender, EventArgs e)
    {
        RedirectToMobilePage("Baggage.aspx");
    }


    private Flight selectedFlight
    {
        get
        {
         //Flight  selFlt = ((Flight)Session["userFlight"]);
            Flight selFlt = MgrSess.GetFlight(Session);
         return selFlt;
        }
    }
    protected void cmdBack1_Click(object sender, EventArgs e)
    {
        //Flight selFlt = ((Flight)Session["userFlight"]);
        if (selectedFlight.ADID == "A")
        {
            RedirectToMobilePage("Baggage.aspx?MENU=OP");

        }
        else
        {
            if (Session["WITHREMARKS"] == "Yes")
            {
                ShowFormRemarkList();
            }
            else
            {
               LoadContainerInfo();
            }
        }
        
    }

    void MWUTiming1_TimingSendClick(object sender, EaTextEventArgs e)
    {
        //Send Trip Information to back end
        string time = e.Text;
        string selectedTime = "";
        string convTime = "Invalid";
        if (checkValidityOfTime(time))
        {
            selectedTime = time.Substring(0, 2) + ":" + time.Substring(2, 2);

           convTime= getPrevDayStatus(selectedTime);
        
        
        }
        
        if (convTime != "Invalid")
        {
            try
            {
                
                    if (selectedFlight.ADID == "A")
                    {
                        if (this.MWUTiming1.getTextBoxLabel() == "FIRST BAG" || this.MWUTiming1.getTextBoxLabel() == "LAST BAG")
                        {
                           
                           updateFlightTiming(convTime) ;                            
                           RedirectToMobilePage("Baggage.aspx?MENU=OP");
                           
                        }
                       
                    }
                    else if (selectedFlight.ADID == "D")
                    {
                       
                       
                            SendTripData(convTime);
                        
                    }
                }


            
            catch (Exception ex)
            {
                ShowFormMsg("Timing input was not successful.Please try to send the timing again");
            }
        }
        
    }

    private void SendTripData(string tripTime)
    {   //Send Trip Information to back end
        EntCPMList tripList = GetSelectedList();
        if (selectedFlight.ADID == "D")
        {

            CtrlTrip.SaveTripAndUpdateIndicatorForBrs(FlightUrNo, tripList, tripTime, UserId, GetUnSelectedList().Count);
        }
       
        if (selectedFlight.ADID == "D")
        {
            if (GetUnSelectedList().Count != 0)
            {
                CPMAutoList.Clear();
                MWUTiming1.setTextBoxLabel("NEXT-TRIP");
                LoadContainerInfo();
            }
            else
            {
                RedirectToMobilePage("Baggage.aspx?MENU=OP");
            }
        }
        else
        {
            RedirectToMobilePage("Baggage.aspx?MENU=OP");
        }
    }

    private string FlightUrNo
    {
        get
        {
            return selectedFlight.URNO;
          
        }
    }

    private EnTripType TripType
    {
        get
        {            
            return trType;
        }
        set
        {
            trType = value;
        }
    }

    private string UserId
    {
        get
        {
            return MgrSess.GetUser(Session);
          
        }
    }



    private EntCPMList GetSelectedList()
    {
        EntCPMList cpmSelectedList = null;
        EntCPMList cpmList = CPMAutoList;
        cpmSelectedList = cpmList.GetSelectedList(cpmSelectedList,true);        
        return cpmSelectedList;
    }

    private EntCPMList GetUnSelectedList()
    {
        EntCPMList cpmSelectedList = null;
        EntCPMList cpmList = CPMAutoList;
        cpmSelectedList = cpmList.GetUnSelectedList(cpmSelectedList, false);
        return cpmSelectedList;
    }

    private EntCPMList CPMAutoList
    {
        get
        {
            EntCPMList cpmList = null;
            try
            {
                cpmList = (EntCPMList)ViewState[VS_CPM_AUTO_LIST];
                cpmList = MWUCpmList1.GetData(cpmList);
            }
            catch (Exception)
            {
            }
            if (cpmList == null) cpmList = new EntCPMList();
            return cpmList;
        }
        set
        {
            ViewState[VS_CPM_AUTO_LIST] = value;
        }
    }

    

    private bool checkValidityOfTime(string time)
    {
            DateTime dtselectedTime = DateTime.Now;
            bool dtStatus = true;
            
            string selectedTime = "";
          


            try
            {
                selectedTime = time.Substring(0, 2) + ":" + time.Substring(2, 2);
            }
            catch (ArgumentOutOfRangeException exc)
            {

                lblTimingAlertMsg.Visible = true;
                lblTimingAlertMsg.Text = "Invalid Time.Please enter time between 0000 - 2359";
                dtStatus = false;
            }
            try
            {
                dtselectedTime = Convert.ToDateTime(selectedTime);
            }
            catch (FormatException ex)
            {
                lblTimingAlertMsg.Visible = true;
                lblTimingAlertMsg.Text = "Invalid Time.Please enter time between 0000 - 2359";
                dtStatus = false;
            }

            
            return dtStatus;
    }

    private string getPrevDayStatus(string selectedTime)
    {
        bool prevDayStatus = false;
        string time = "";
        DateTime dtselectedTime = DateTime.Now;
       
        dtselectedTime = UtilTime.CompDateTimeFromRef(selectedFlight.FLIGHTDATE, selectedTime);
        selectedTime = UtilTime.ConvDateTimeToUfisFullDTString(dtselectedTime);
      
        DateTime curTime = DateTime.Now;
        if (dtselectedTime > curTime)
        {
           
                lblTimingAlertMsg.Visible = true;
                lblTimingAlertMsg.Text = "Time entered must be earlier than current time - please re-enter";
                time = "Invalid";
            
        }
        else
        {
            time = getTimeInOracleFormat(prevDayStatus, selectedTime);

        }
        return time;
    }

    private bool checkTimeforMidNight(DateTime selTime, DateTime curTime)
    {
        if (curTime.Hour == 0 && selTime.Hour == 23)
        {
            return true;
        }
        return false;
    }
    private string getTimeInOracleFormat(bool prevDayStatus, string selectedTime)
    {
        
        string TimeInOracleFormat = selectedTime;
        return TimeInOracleFormat;
    }


    private bool checkForTimingInDsFlight(string selectedOption, string adId)
    {
        bool hasValue = false;
        Flight selFlt = MgrSess.GetFlight(Session);
        string FlightURNO = selFlt.URNO;
        DSArrivalFlightTimings dsArrFlightTiming = new DSArrivalFlightTimings();        
        DSArrivalFlightTimings.AFLIGHTRow rowArr = null;       
        if (adId == "A")
        {
            dsArrFlightTiming = CtrlFlight.RetrieveArrivalFlightTimings(FlightURNO);
            if (dsArrFlightTiming.AFLIGHT.Rows.Count > 0)
            {
                rowArr = ((DSArrivalFlightTimings.AFLIGHTRow)dsArrFlightTiming.AFLIGHT.Rows[0]);
            }
            if (selectedOption == "FBAG")
            {
                if (rowArr==null||rowArr.FBAG == null || rowArr.FBAG == "")
                {

                    hasValue = false;
                }
                else hasValue = true;

            }
            
               



        }

        


        return hasValue;

    }

    private string updateFlightTiming(string selectedTime)
    {
        string sendStatus = "";

        if (txtVFlightDetailsTiming.Text == selectedFlight.DISPLAYBOFLIGHTMENU)
        {      
       
        string FlightURNO = selectedFlight.URNO;       
        string selOption = "";

        if (selectedFlight.ADID == "A")
        {
            if (this.MWUTiming1.getTextBoxLabel() == "FIRST BAG")
            {
                selOption = "FBAG";

            }
            else
                if (this.MWUTiming1.getTextBoxLabel() == "LAST BAG")
                {
                    selOption = "LBAG";

                }

        }
        else
        {

            selOption = this.MWUTiming1.getTextBoxLabel();

        }
              
                if (selectedFlight.ADID == "A")
                {
                    if (selOption == "FBAG")
                    {
                        CtrlFlight.UpdateArrivalFlightTiming(null, FlightURNO, selOption, selectedTime, UserId);
                    }
                    else
                        if (selOption == "LBAG")
                        {
                            CtrlBagArrival.SendLastBagTiming(null, FlightURNO, selectedTime, UserId);
                        }                   
                    
                  
                }
               
          
                txtVFlightDetailsTiming.Text = "";
            }
            else
            {
               
                RedirectToMobilePage("InvalidPage.aspx");

            }
            return sendStatus;
    }
    protected void cmdRemarkForDep_Click(object sender, EventArgs e)
    {
       if (txtVFltDetail.Text == selectedFlight.DISPLAYBOFLIGHTMENU)
        {
        if (GetSelectedList().Count != 0)
        {
            Session["WITHREMARKS"] = "Yes";
            ShowFormRemarkList();
        }
         else
        {
            Label2.Text = "Sorry,no containers selected";
            Label2.Visible = true;
        
        }
    }
    else
    {

        RedirectToMobilePage("Baggage.aspx?MENU=OP");
    }
    }

    private void ShowFormRemarkList()
    {
        lstRmkList.Items.Clear();
        txtvFlInfo.Text = selectedFlight.DISPLAYBOFLIGHTMENU;
        EntCPMList cpmSelectedList = GetSelectedList();

        int cnt = cpmSelectedList.Count;
        for (int i = 0; i < cnt; i++)
        {
            string rmkInd = "";
            if (cpmSelectedList.GetData(i).RemarkText != "") rmkInd = RMK_IND;
            lstRmkList.Items.Add(rmkInd + PFIX_RMK_LIST + cpmSelectedList.GetData(i).ContainerNo + SFIX_RMK_LIST);
        }
        if (lstRmkList.Items.Count <= 0)
        {
            lblNoDataRemkList.Text = "No Container was selected. Do you want to continue?";
            lblNoDataRemkList.Visible = true;
        }
        else
        {
            lblNoDataRemkList.Visible = false;
        }
        ActiveForm = frmRemarkList;
    }
    protected void lstRmkList_ItemCommand(object sender, ListCommandEventArgs e)
    {
        try
        {
            string st = e.ListItem.Text;
            int startPos = PFIX_RMK_LIST.Length;
            if (st.Substring(0, RMK_IND.Length) == RMK_IND) { startPos += RMK_IND.Length; }
            st = st.Substring(startPos, st.Length - startPos - SFIX_RMK_LIST.Length);
            CurContainerNo = st;
            ShowFormAnRemark();
        }
        catch (Exception ex)
        {
           // ShowFormMsg("Error. " + ex.Message);
        }
    }
    private void ShowFormAnRemark()
    {
        string containerNo = CurContainerNo;
        lblContainerNo.Text = containerNo;
        EntRemark rmk = GetCurrentRemark(containerNo);
        MWUBagRemk1.SetCurRmk(rmk);
        txtFlInfo.Text = selectedFlight.DISPLAYBOFLIGHTMENU;
        ActiveForm = frmRemk;
    }

    private void ShowFormMsg(string msg)
    {
        lblTimingAlertMsg.Visible = true;
        lblTimingAlertMsg.Text = msg;
        //ActiveForm = frmMsgArrAprTrip;
    }

    private string CurContainerNo
    {
        get
        {
            string st = "";
            try
            {
                st = (string)ViewState[VS_CUR_CONTAINER_NO];
                if (st == null) st = "";
                st = st.Trim();
            }
            catch (Exception)
            {
            }
            return st;
        }
        set
        {
            if (value == null) value = "";
            ViewState[VS_CUR_CONTAINER_NO] = value;
        }
    }

    private EntRemark GetCurrentRemark(string containerNo)
    {
        EntRemark rmk = null;
      
            if (rmk == null)
            {
                EntCPMList cpmListFree = CPMAutoList;
                int cnt = cpmListFree.Count;
                for (int i = 0; i < cnt; i++)
                {
                    if (cpmListFree.GetData(i).ContainerNo == containerNo)
                    {
                        rmk = cpmListFree.GetData(i).Remark;
                        break;
                    }
                }
            }
        
        else
        {
            //rmk = CPMNoList.GetData(0).Remark;
        }
        return rmk;
    }
    protected void cmdContinueRemarkList_Click(object sender, EventArgs e)
    {
        populateLstTime();
    }
    protected void cmdBackRemarkList_Click(object sender, EventArgs e)
    {
        ActiveForm = frmContainer;
    }
    protected void cmdBack2_Click(object sender, EventArgs e)
    {
        RedirectToMobilePage("Baggage.aspx?MENU=OP");
    }
    
    protected void cmdRemarks_Click(object sender, EventArgs e)
    {

    }
    protected void Command1_Click(object sender, EventArgs e)
    {
        if (txtVFltDetail.Text == selectedFlight.DISPLAYBOFLIGHTMENU)
        {
            if (GetSelectedList().Count != 0)
            {
                Session["WITHREMARKS"] = "No";
                populateLstTime();
            }
            else
            {
                Label2.Text = "Sorry,no containers selected";
                Label2.Visible = true;

            }
        }
        else
        {

            RedirectToMobilePage("Baggage.aspx?MENU=OP");
        }
    }

    protected override void OnViewStateExpire(EventArgs e)
    {


        iTrekUtils.iTUtils.LogToEventLog("ViewState Expired in Baggage Handling", "iTrekError");
        RedirectToMobilePage("Login.aspx");

    }
}
