<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BaggageHandling.aspx.cs" Inherits="BaggageHandling" EnableSessionState="True"%>

<%@ Register Src="MWUCpmList.ascx" TagName="MWUCpmList" TagPrefix="uc2" %>
<%@ Register Src="MWUBagRemk.ascx" TagName="MWUBagRemk" TagPrefix="uc2" %>
<%@ Register Src="MWUTiming.ascx" TagName="MWUTiming" TagPrefix="uc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
  <mobile:Form ID="frmContainer" Runat="server"><mobile:Label ID="lblTitle5" Runat="server" Alignment="Center">SATS iTrek</mobile:Label> 
    <mobile:TextView ID="txtVFltDetail" Runat="server"></mobile:TextView> 
    <mobile:Label ID="Label2" Runat="server" Visible="False"></mobile:Label> 
    <uc2:MWUCpmList ID="MWUCpmList1" runat="server" />  
     <mobile:Command ID="cmdRemarkForDep" Runat="server" OnClick="cmdRemarkForDep_Click"  >Next/Add Remk</mobile:Command>
      <br />
      <mobile:Command ID="Command1" Runat="server" OnClick="Command1_Click">Send</mobile:Command>
      <br />
      <mobile:Command ID="cmdBack2" Runat="server" OnClick="cmdBack2_Click" >Back</mobile:Command>
      </mobile:Form><mobile:Form ID="frmBOTiming" Runat="server"><mobile:Label ID="lblTitle4" Runat="server" Alignment="Center">SATS iTrek</mobile:Label> <mobile:TextView ID="txtVFlightDetailsTiming" Runat="server"></mobile:TextView> <mobile:Label ID="lblSelectedOption" Runat="server">
        </mobile:Label> <mobile:Label ID="lblTimingAlertMsg" Runat="server" EnableViewState="False" Visible="False" BreakAfter="True" >
        </mobile:Label><uc1:MWUTiming ID="MWUTiming1" runat="server" /> <mobile:Command ID="cmdBack1" Runat="server" OnClick="cmdBack1_Click" >Back</mobile:Command></mobile:Form>
    
       <mobile:Form ID="frmRemarkList" Runat="server">
       <mobile:Label ID="Label1" Runat="server" Alignment="Center">SATS iTrek</mobile:Label> 
        <mobile:TextView ID="txtvFlInfo" Runat="server">
           </mobile:TextView>
       <mobile:Label ID="lblH2RemarkList" Runat="server" Alignment="Center" Font-Bold="True">Create Remark</mobile:Label>
       <mobile:Label ID="lblNoDataRemkList" Runat="server" Visible="False">
        </mobile:Label>
        <mobile:List ID="lstRmkList" Runat="server" Decoration="Numbered" OnItemCommand="lstRmkList_ItemCommand">
        </mobile:List>
        <mobile:Command ID="cmdContinueRemarkList" Runat="server" OnClick="cmdContinueRemarkList_Click">Send</mobile:Command>
        <mobile:Command ID="cmdBackRemarkList" Runat="server" OnClick="cmdBackRemarkList_Click">Back</mobile:Command>
    </mobile:Form>
    <mobile:Form ID="frmRemk" Runat="server">
    <mobile:Label ID="Label3" Runat="server" Alignment="Center">SATS iTrek</mobile:Label> 
        <mobile:TextView ID="txtFlInfo" Runat="server">
           </mobile:TextView>
        <mobile:Label ID="lblH2Rmk" Runat="server" Alignment="Center" Font-Bold="True">REMARK</mobile:Label>
        <mobile:Label ID="lblContainerNo" Runat="server">
        </mobile:Label>
        <uc2:MWUBagRemk ID="MWUBagRemk1" runat="server" /></mobile:Form>    
</body>
</html>
