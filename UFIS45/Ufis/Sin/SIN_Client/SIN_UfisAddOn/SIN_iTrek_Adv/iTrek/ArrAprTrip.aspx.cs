#define TestingOn
#define UsingQueues


using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.EventArg;
using UFIS.ITrekLib.Util;
using UFIS.Wap.ITrek.WebMgr;
using UFIS.ITrekLib.CustomException;
using iTrekXML;
using iTrekWMQ;
using MM.UtilLib;
public partial class WAP_ArrAprTrip : System.Web.UI.MobileControls.MobilePage
{
    private const string VS_CPM_AVAIL_IND = "VS_CPM_AVAIL_IND";
    private const string VS_CUR_CONTAINER_NO = "VS_CUR_CONTAINER_NO";
    private const string VS_CPM_AUTO_LIST = "VS_CPM_AUTO_LIST";
    private const string VS_CPM_MANUAL_LIST = "VS_CPM_MANUAL_LIST";
    private const string VS_CPM_NO_LIST = "VS_CPM_NO_LIST";
    private const string VS_CUR_ENT_DS_CPM = "VS_CUR_ENT_DS_CPM";
    

    private const string PFIX_RMK_LIST = "Rmk (";
    private const string SFIX_RMK_LIST = ")";
    private const string RMK_IND = "**";
    EnTripType trType = EnTripType.FirstTrip;

    private EntCPMList CPMAutoList
    {
        get
        {
            EntCPMList cpmList = null;
            try
            {
                cpmList = (EntCPMList)ViewState[VS_CPM_AUTO_LIST];
                cpmList = MWUCpmList1.GetData(cpmList);
               
            }
            catch (Exception)
            {
            }
            if (cpmList == null) cpmList = new EntCPMList();
            return cpmList;
        }
        set
        {
            ViewState[VS_CPM_AUTO_LIST] = value;
        }
    }
    private EntCPMList CPMManualList
    {
        get
        {
            EntCPMList cpmList = null;
            try
            {
                cpmList = (EntCPMList)ViewState[VS_CPM_MANUAL_LIST];
                cpmList = MWUCpmManual1.GetData(cpmList);
            }
            catch (Exception)
            {
            }
            if (cpmList == null) cpmList = new EntCPMList();
            return cpmList;
        }
        set
        {
            ViewState[VS_CPM_MANUAL_LIST] = value;
        }
    }
    private EntCPMList CPMNoList
    {
        get
        {
            EntCPMList cpmList = null;
            try
            {
                try
                {
                    cpmList = (EntCPMList)ViewState[VS_CPM_NO_LIST];
                }
                catch (Exception)
                {
                }
                if (cpmList == null) cpmList = new EntCPMList();
                int i = 0;
                int interContCnt=0;
                 int interBTCnt=0;
                 int locContCnt=0;
                 int locBTCnt=0;

               try 
	{	        
		 interContCnt=Convert.ToInt32(txtIntCont.Text.Trim());
	}
	catch (Exception)
	{
		
		
	}
                try 
	{	        
		interBTCnt=Convert.ToInt32(txtIntBt.Text.Trim());
	}
	catch (Exception)
	{
		
		
	}
                try 
	{	        
		locContCnt= Convert.ToInt32(txtLocCont.Text.Trim());
	}
	catch (Exception)
	{
		
		
	}
    try
    {
        locBTCnt = Convert.ToInt32(txtLocBt.Text.Trim());
    }
    catch (Exception)
    {


    }
    try
    {
        if (Session["FROMREMARKS"] == "Yes")
        {

            cpmList = null;
            cpmList = new EntCPMList();
            ViewState[VS_CPM_NO_LIST] = null;
        }
    }
    catch (Exception)
    {
        
        
    }



                if (slType.SelectedIndex == 0)
                {
                    if (txtIntCont.Text.Trim() != "")
                    {
                        i += interContCnt;
                    
                    
                    }
                    if (txtIntBt.Text.Trim() != "")
                    {
                        i += interBTCnt;


                    }
                }
                if (slType2.SelectedIndex == 0)
                {
                    if (txtLocCont.Text.Trim() != "")
                    {
                        i += locContCnt;


                    }
                    if (txtLocBt.Text.Trim() != "")
                    {
                        i += locBTCnt;


                    }
                }

                if (cpmList.Count < 1)
                {
                    for (int j = 0; j < i; j++)
                    {
                        cpmList.AddData(new EntCPM());
                    }
                    
                }
             
                int k = 0;

                EntCPM cpm = cpmList.GetData(k);
                    if (slType.SelectedIndex < 0)
                    {
                        cpm.ContainerNo = "";
                        cpm.Destination = "";
                       // cpm.Remark = null;
                        cpm.Selected = false;
                    }
                    else
                    {
                        string des = "";
                        string containerNo = "";
                        des = slType.Items[slType.SelectedIndex].Value;
                        if (des != "")
                        {
                            if (txtIntCont.Text.Trim() != "")
                            {
                                int contNo = 0;
                                for (int l = 0; l < interContCnt; l++)
                                {
                                     cpm = cpmList.GetData(k);
                                     contNo++;
                                     containerNo = "Container" + contNo+"("+des+")";
                                    cpm.ContainerNo = containerNo;
                                    cpm.Destination = des;
                                    cpm.Selected = true;
                                    k++;
                                }
                            }
                            
                        }
                        // containerNo = slNoOfContainer.Items[slNoOfContainer.SelectedIndex].Text;
                        
                    }

                    EntCPM cpm1 = cpmList.GetData(k);
                    if (slType.SelectedIndex < 0)
                    {
                        cpm1.ContainerNo = "";
                        cpm1.Destination = "";
                        //cpm1.Remark = null;
                        cpm1.Selected = false;
                    }
                    else
                    {
                        string des = "";
                        string containerNo = "";
                        des = slType.Items[slType.SelectedIndex].Value;
                        if (des != "")
                        {
                            if (txtIntBt.Text.Trim() != "")
                            {
                                int contNo = 0;
                                for (int l = 0; l < interBTCnt; l++)
                                {
                                   cpm1 = cpmList.GetData(k);
                                   contNo++;
                                   containerNo = "BT" + contNo;
                                   cpm1.ContainerNo = containerNo + "(" + des + ")";
                                    cpm1.Destination = des;
                                    cpm1.Selected = true;
                                    k++;
                                }
                                
                            }
                            
                        }

                        
                    }

                    EntCPM cpm2 = cpmList.GetData(k);

                    if (slType2.SelectedIndex < 0)
                    {
                        cpm2.ContainerNo = "";
                        cpm2.Destination = "";
                        //cpm2.Remark = null;
                        cpm2.Selected = false;
                    }
                    else
                    {
                        string des = "";
                        string containerNo = "";
                        des = slType2.Items[slType2.SelectedIndex].Value;
                        if (des != "")
                        {

                            if (txtLocCont.Text.Trim() != "")
                            {
                                int contNo = 0;
                                for (int l = 0; l < locContCnt; l++)
                                {
                                    cpm2 = cpmList.GetData(k);
                                    contNo++;
                                    containerNo = "Container" + contNo;
                                    cpm2.ContainerNo = containerNo + "(" + des + ")"; ;
                                    cpm2.Destination = des;
                                    cpm2.Selected = true;
                                    k++;
                                }

                            }
                           
                        }


                    }

                    EntCPM cpm3 = cpmList.GetData(k);

                    if (slType2.SelectedIndex < 0)
                    {
                        cpm3.ContainerNo = "";
                        cpm3.Destination = "";
                        //cpm3.Remark = null;
                        cpm3.Selected = false;
                    }
                    else
                    {
                        string des = "";
                        string containerNo = "";
                        des = slType2.Items[slType2.SelectedIndex].Value;
                        if (des != "")
                        {
                            if (txtLocBt.Text.Trim() != "")
                            {
                                int contNo = 0;
                                for (int l = 0; l < locBTCnt; l++)
                                {
                                    cpm3 = cpmList.GetData(k);
                                    contNo++;
                                    containerNo = "BT" + contNo;
                                    cpm3.ContainerNo = containerNo + "(" + des + ")"; ;
                                    cpm3.Destination = des;
                                    cpm3.Selected = true;
                                    k++;
                                }
                                containerNo = "BT" + txtLocBt.Text;
                            }
                           
                        }


                    }
                
            }
            catch (Exception)
            {
            }
            if (cpmList == null) cpmList = new EntCPMList();
            Session["FROMREMARKS"] = "No";
            return cpmList;
        }
        set
        {
            ViewState[VS_CPM_NO_LIST] = value;
        }
    }
    private string CurContainerNo
    {
        get
        {
            string st = "";
            try
            {
                st = (string)ViewState[VS_CUR_CONTAINER_NO];
                if (st == null) st = "";
                st = st.Trim();
            }
            catch (Exception)
            {
            }
            return st;
        }
        set
        {
            if (value == null) value = "";
            ViewState[VS_CUR_CONTAINER_NO] = value;
        }
    }
    private string FlightUrNo
    {
        get
        {
            return MgrSess.GetFlight(Session).URNO;
        }
    }
    private EntDsCPM CurDsCPM
    {
        get
        {
            EntDsCPM ent = null;
            try
            {
                ent = (EntDsCPM)ViewState[VS_CUR_ENT_DS_CPM];
            }
            catch (Exception)
            {
            } 
            return ent;
        }
        set
        {
            try
            {
                ViewState[VS_CUR_ENT_DS_CPM] = value;
            }
            catch (Exception)
            {
                
                
            }
        }
    }

    protected void Page_Init(Object sender, EventArgs e)
    {
        MWUBagRemk1.RemarkSaveClick += new WAP_MWUBagRemk.RemarkSaveClickEventHandler(MWUBagRemk1_RemarkSaveClick);
        MWUBagRemk1.RemarkBack_Click += new WAP_MWUBagRemk.RemarkBackClickEventHandler(MWUBagRemk1_RemarkBack_Click);
        MWUTiming1.TimingSendClick += new WAP_MWUTiming.TimingSendHandler(MWUTiming1_TimingSendClick);

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        //Flight fl = new Flight("1234", "MI1234", "20070518000000", "20070518000000", "", "",
        //    "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");

        //MgrSess.SetFlight(Session, fl);
        string uId = "";
        MgrSec.IsAuthenticatedUser(this, Session, out uId);
     
        if (!IsPostBack)
        {
            try
            {
                
                MWU_CompanyHeading1.ShowFlightInfoHeading();
                MWU_CompanyHeading2.ShowFlightInfoHeading();
                MWU_CompanyHeading3.ShowFlightInfoHeading();
                MWU_CompanyHeading4.ShowFlightInfoHeading();
                MWU_CompanyHeading5.ShowFlightInfoHeading();
                MWU_CompanyHeading6.ShowFlightInfoHeading();
                lblHidden.Text = MWU_CompanyHeading4.GetFlightInfoHeading();
                LoadCPMInfo();
                ShowCPMForm();
            }
            catch (Exception ex)
            {
                //ShowFormMsg("Error. " + ex.Message);
            }
        }
    }
    
    private void LoadCPMInfo()
    {
        string cpmAvail = "";
        string errMsg = "";
        string flightUrNo = FlightUrNo;
        if (flightUrNo == "")
        {
            ShowFormMsg("No Flight Information.");
        }

        try
        {

            EntDsCPM entDsCpm = CtrlCPM.RetrieveUnSentCPMUldForAFlightForAO(flightUrNo);
            DSCpm dsCpmAll = CtrlCPM.RetrieveUldNos(flightUrNo);
            CurDsCPM = entDsCpm;
            int cnt = entDsCpm.UldCount(flightUrNo);
         
            if (cnt < 1) cpmAvail = "N";
                 
            else
            {
                if (dsCpmAll.CPMDET.Rows.Count == cnt)
                {
                    MWUTiming1.setTextBoxLabel("FTRIP");
                    TripType = EnTripType.FirstTrip;
                }
                else
                {
                    MWUTiming1.setTextBoxLabel("NEXT-TRIP");
                    TripType = EnTripType.NextTrip;
                
                }
                
                cpmAvail = "Y";
                DSCpm dsCpm = entDsCpm.DsCPM;                
                MWUCpmList1.DataSource = dsCpm.CPMDET;
                MWUCpmList1.DataTextField = "ULD_TXT";
                /*Alphy*/
                MWUCpmList1.DataValueField = "URNO";
                MWUCpmList1.SortBy = "PRTY ASC,POSF DESC";
                MWUCpmList1.DataBind();
            }
        }
        catch (NoRecordException)
        {
            cpmAvail = "N";
        }
        catch (Exception ex)
        {
            errMsg = ex.Message;
        }
        if (cpmAvail == "") errMsg = "Error accessing CPM Information";

        ViewState[VS_CPM_AVAIL_IND] = cpmAvail;
    }

    void MWUTiming1_TimingSendClick(object sender, EaTextEventArgs e)
    {//Send Trip Information to back end


        if (selectedFlight != null)
        {
            if (lblHidden.Text == selectedFlight.DISPLAYFLIGHTSMENU)
            {
                string time = e.Text;
                string selectedTime = "";
                string convTime = "Invalid";
                if (checkValidityOfTime(time))
                {
                    selectedTime = time.Substring(0, 2) + ":" + time.Substring(2, 2);

                    convTime = getPrevDayStatus(selectedTime);
                }
                if (convTime != "Invalid")
                {
                        try
                       {
                      

                                SendTripData(convTime);
                               
                            
                        }
                    
                    catch (Exception ex)
                    {
                        ShowFormMsg("Timing input was not successful.Please try to send the timing again" + ex.Message);
                    }
                }
            }
            else
            {
                lblHidden.Text = "";
                RedirectToMobilePage("InvalidPage.aspx");
                //RedirectToMobilePage("Menu.aspx?MENU=OP");
            }
        }
        else
        {
            lblHidden.Text = "";
            RedirectToMobilePage("InvalidPage.aspx");
            //RedirectToMobilePage("Menu.aspx?MENU=OP");
        }
    }

    private void GoBackToMenu()
    {
        lblErrMsg.Text = "";
        if (IsCPMAvail())
        {
        if (GetUnSelectedList().Count != 0)
        {
            CPMAutoList .Clear();
            CPMManualList.Clear();
            CPMNoList.Clear();
            MWUTiming1.setTextBoxLabel("NEXT-TRIP");
            TripType = EnTripType.NextTrip;
            LoadCPMInfo();
            ShowCPMForm();
        }
        else
        {
            lblHidden.Text = "";
            RedirectToMobilePage("Menu.aspx?MENU=SELFLT");
        }
        }
        else
        {
            lblHidden.Text = "";
         RedirectToMobilePage("Menu.aspx?MENU=SELFLT");
        }
    }

    private string UserId
    {
        get
        {
            string userId = "";
            userId = MgrSess.GetUser(Session);
            return userId;
        }
    }

    private EnTripType TripType
    {
        get
        {
            return trType;
        }
        set
        {
            trType = value;
        }
    }

    private Flight selectedFlight
    {
        get
        {
            Flight selFlt = MgrSess.GetFlight(Session);
            
            return selFlt;
        }
    }
    private void SendTripData(string tripTime)
    {   //Send Trip Information to back end
        EntCPMList tripList = GetSelectedList();
        //CtrlTrip.SaveTrip(FlightUrNo, tripList, tripTime, UserId, TripType, EnTripSendRecv.Send);
        CtrlTrip.SaveTripAndUpdateIndicator(FlightUrNo, tripList, tripTime, UserId, IsCPMAvail(), GetUnSelectedList().Count);
        GoBackToMenu();
    }  

    void MWUBagRemk1_RemarkBack_Click(object sender, EventArgs e)
    {
        ActiveForm = frmRemarkList;
    }

    void MWUBagRemk1_RemarkSaveClick(object sender, EaRemarkSaveEventArgs e)
    {
        try
        {
            SaveAnRemark(CurContainerNo, e.Remk);
            ShowFormRemarkList();
        }
        catch (Exception ex)
        {
            //ShowFormMsg("Error. " + ex.Message);
        }
    }

    private bool IsCPMAvail()
    {
        string stCpmAvail = "";
        bool cpmAvail = false;

        try
        {
            stCpmAvail = ViewState[VS_CPM_AVAIL_IND].ToString();
        }
        catch (Exception)
        {//Not assign to this view state yet.            
        }
        if (stCpmAvail == "")
            stCpmAvail = CheckCPMAvailable();

        switch (stCpmAvail)
        {
            case "Y":
                cpmAvail = true;
                break;
            case "N":
                cpmAvail = false;
                break;
            default:
                throw new ApplicationException("Unable to get the CPM Information.");
        }
        return cpmAvail;
    }

    private string CheckCPMAvailable()
    {
        string cpmAvail = "";
        string flightUrNo = FlightUrNo;
        if (flightUrNo == "")
        {
            ShowFormMsg("No Flight Information.");
        }
        if (CurDsCPM.UldCount(flightUrNo) < 1) cpmAvail = "N";
        else cpmAvail = "Y";
        ViewState[VS_CPM_AVAIL_IND] = cpmAvail;
        return cpmAvail;
    }

    protected void cmdContinueCPMAvailArrAprTrip_Click(object sender, EventArgs e)
    {
        Session["WITHREMARKS"] = "Yes";
        EntCPMList cpmSelectedList = GetSelectedList();
        int cnt = cpmSelectedList.Count;
        if (cnt <= 0)
        {
            lblErrMsg.Text = "Please select a container";
            lblErrMsg.Visible = true;
        }
        else
        {
            if (this.MWUTiming1.getTextBoxLabel() == "LTRIP")
            {
                if (GetUnSelectedList().Count != 0)
                {
                    ShowFormMsg("LTRIP");
                }
                else
                {
                    ShowFormRemarkList();
                }
            }
            else
            {
                if (GetUnSelectedList().Count == 0)
                {
                    MWUTiming1.setTextBoxLabel("LTRIP");
                    

                }
                ShowFormRemarkList();
            }
        }
    }

    private EntCPMList GetSelectedList()
    {
        EntCPMList cpmSelectedList = null;
        EntCPMList cpmList = CPMAutoList;
        cpmSelectedList = cpmList.GetSelectedList(cpmSelectedList, true);
        EntCPMList cpmListFree = CPMManualList;
        
        cpmSelectedList = cpmListFree.GetSelectedList(cpmSelectedList, false);
        
        cpmSelectedList = CPMNoList.GetSelectedList(cpmSelectedList,false);
        
        return cpmSelectedList;
    }

    private EntCPMList GetUnSelectedList()
    {
        EntCPMList cpmSelectedList = null;
        EntCPMList cpmList = CPMAutoList;
        cpmSelectedList = cpmList.GetUnSelectedList(cpmSelectedList, true);        
        return cpmSelectedList;
    }

    protected void lstRmkList_ItemCommand(object sender, ListCommandEventArgs e)
    {
        try
        {
            string st = e.ListItem.Text;
            int startPos = PFIX_RMK_LIST.Length;
            if (st.Substring(0, RMK_IND.Length) == RMK_IND) { startPos += RMK_IND.Length; }
            st = st.Substring(startPos, st.Length - startPos - SFIX_RMK_LIST.Length);
            CurContainerNo = st;
            ShowFormAnRemark();
        }
        catch (Exception ex)
        {
           // ShowFormMsg("Error. " + ex.Message);
        }
    }

    private void SaveAnRemark(string containerNo, EntRemark rmk)
    {
        bool foundContainer = false ;
        if (IsCPMAvail())
        {
            if (!foundContainer)
            {
                EntCPMList cpmList = CPMAutoList;
                int cnt = cpmList.Count;
                for (int i = 0; i < cnt; i++)
                {
                    if (cpmList.GetData(i).ContainerNo == containerNo)
                    {
                        cpmList.GetData(i).Remark = rmk;
                        CPMAutoList = cpmList;
                        foundContainer = true;
                        break;
                    }
                }
            }

            if (!foundContainer)
            {
                EntCPMList cpmListFree = CPMManualList;
                cpmListFree = MWUCpmManual1.GetData(cpmListFree);
                int cnt = cpmListFree.Count;
                for (int i = 0; i < cnt; i++)
                {
                    if (cpmListFree.GetData(i).ContainerNo == containerNo)
                    {
                        cpmListFree.GetData(i).Remark = rmk;
                        CPMManualList = cpmListFree;
                        foundContainer = true;
                        break;
                    }
                }
            }
        }
        else
        {
            if (!foundContainer)
            {
                EntCPMList cpmNoList = CPMNoList;
                int cnt = CPMNoList.Count;
                for (int i = 0; i < cnt; i++)
                {
                    if (cpmNoList.GetData(i).ContainerNo == containerNo)
                    {

                        cpmNoList.GetData(i).Remark = rmk;
                        CPMNoList = cpmNoList;
                        foundContainer = true;
                        break;
                    }
                }
            }
        }
        if (!foundContainer) throw new ApplicationException("Unable to find the container in the list");
    }

    private EntRemark GetCurrentRemark(string containerNo)
    {
        EntRemark rmk = null;
        if (IsCPMAvail())
        {
            if (rmk == null)
            {
                EntCPMList cpmList = CPMAutoList;
                int cnt = cpmList.Count;
                for (int i = 0; i < cnt; i++)
                {
                    if (cpmList.GetData(i).ContainerNo == containerNo)
                    {
                        rmk = cpmList.GetData(i).Remark;
                        break;
                    }
                }
            }

            if (rmk == null)
            {
                EntCPMList cpmListFree = CPMManualList;
                int cnt = cpmListFree.Count;
                for (int i = 0; i < cnt; i++)
                {
                    if (cpmListFree.GetData(i).ContainerNo == containerNo)
                    {
                        rmk = cpmListFree.GetData(i).Remark;
                        break;
                    }
                }
            }
        }
        else
        {
            if (rmk == null)
            {
                EntCPMList cpmNoList = CPMNoList;
                int cnt = cpmNoList.Count;
                for (int i = 0; i < cnt; i++)
                {
                    if (cpmNoList.GetData(i).ContainerNo == containerNo)
                    {
                        rmk = CPMNoList.GetData(i).Remark;
                        break;
                    }
                }
            }
            
        }
        return rmk;
    }
    protected void cmdContinueRemarkList_Click(object sender, EventArgs e)
    {
        ShowFormTiming();
    }

    protected void cmdBackRemarkList_Click(object sender, EventArgs e)
    {
        ShowCPMForm();
    }

    protected void frmTimingArrAprTrip_Activate(object sender, EventArgs e)
    {
        MWUTiming1.SetDateTimeList();
    }

    private void ShowCPMForm()
    {
        lblErrMsg.Text = "";
        if (IsCPMAvail())
        {
            ShowFormTripCPMAvail();
        }
        else
        {
            ShowFromTripNoCPM();
        }
    }

    private void ShowFormTiming()
    {
        EntCPMList cpmList = CPMAutoList;
        EntCPMList cpmListFree = CPMManualList;
        lblH2TimingArrAprTrip.Text = MWUTiming1.getTextBoxLabel();
        ActiveForm = frmTimingArrAprTrip;
    }

    private void ShowFormTimingWithoutRemark()
    {
        
        ActiveForm = frmTimingArrAprTrip;
    }
    private void ShowFormAnRemark()
    {
        string containerNo = CurContainerNo;
        lblContainerNo.Text = containerNo;
        EntRemark rmk = GetCurrentRemark(containerNo);
        MWUBagRemk1.SetCurRmk(rmk);
        ActiveForm = frmRemk;
    }
    private void ShowFormRemarkList()
    {
        lstRmkList.Items.Clear();
        EntCPMList cpmSelectedList = GetSelectedList();

        int cnt = cpmSelectedList.Count;
        for (int i = 0; i < cnt; i++)
        {
            string rmkInd = "";
            if (cpmSelectedList.GetData(i).RemarkText != "") rmkInd = RMK_IND;
            lstRmkList.Items.Add(rmkInd + PFIX_RMK_LIST + cpmSelectedList.GetData(i).ContainerNo + SFIX_RMK_LIST );
        }
        if (lstRmkList.Items.Count <= 0)
        {
            lblNoDataRemkList.Text = "No Container was selected. Do you want to continue?";
            lblNoDataRemkList.Visible = true;
        }
        else
        {
            lblNoDataRemkList.Visible = false;
        }
        ActiveForm = frmRemarkList;
    }
    private void ShowFromTripNoCPM()
    {
        Session["WITHREMARKS"] = "No";
        ActiveForm = frmTripNoCPM;
    }
    private void ShowFormTripCPMAvail()
    {
        int cntSin, cntInt ;
        lblH2ULDInfoTrip.Text = CtrlCPM.GetUldInfoForAFlight(selectedFlight.URNO, out cntSin, out cntInt);
        MWUCpmManual1.Visible = false;
        cmdNextULDPage.Visible = true;
        cmdPrevULDPage.Visible = true;
        if (MWUCpmList1.IsLastPage)
        {
            MWUCpmManual1.Visible = true;
            cmdNextULDPage.Visible = false;
        }
        if (MWUCpmList1.IsFirstPage)
        {
            cmdPrevULDPage.Visible = false;
        }
        ActiveForm = frmTripCPMAvail;
    }
    private void ShowFormMsg(string msg)
    {
        if (msg == "LTRIP")
        {
            cmdBackMsgArrAprTrip.Visible = false;
            cmdContToBag.Visible = true;
            cmdContinue.Visible = true;
            lblMsgMsgArrAprTrip.Text = "There are more ULDS to select.Click select to select balance ULDS or click continue to enter time.";
        }
        else
        {
            lblMsgMsgArrAprTrip.Text = msg;
        }
        ActiveForm = frmMsgArrAprTrip;
    }
    protected void cmdContinuerNoCPMArrAprTrip_Click(object sender, EventArgs e)
    {
        EntCPMList cpmSelectedList = GetSelectedList();
        int cnt = cpmSelectedList.Count;
        if (cnt <= 0)
        {
            lblErrMsg1.Text = "Please select a container";
            lblErrMsg1.Visible = true;
        }
        else
        {
            Session["WITHREMARKS"] = "Yes";
            Session["FROMREMARKS"] = "Yes";
            MWUTiming1.setTextBoxLabel("TRIPS");
            lblH2TimingArrAprTrip.Text = MWUTiming1.getTextBoxLabel();
            ShowFormRemarkList();
        }
    }
    protected void cmdBackTimingArrAprTrip_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["WITHREMARKS"] == "Yes")
            {
                ShowFormRemarkList();
            }
            else
            {
                ShowCPMForm();
            }
        }
        catch (Exception ex)
        {            
        }
    }
    protected void cmdBackMsgArrAprTrip_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["WITHREMARKS"] == "Yes")
            {
                ShowFormRemarkList();
            }
            else
            {
                ShowCPMForm();
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void cmdBackCPMAvailArrAprTrip_Click(object sender, EventArgs e)
    {
        RedirectToMobilePage("Menu.aspx?MENU=SELFLT");
    }
    protected void cmdBackNoCPMArrAprTrip_Click(object sender, EventArgs e)
    {
        RedirectToMobilePage("Menu.aspx?MENU=SELFLT");
    }

    private bool checkValidityOfTime(string time)
    {
        DateTime dtselectedTime = DateTime.Now;
        bool dtStatus = true;

        string selectedTime = "";
        try
        {
            selectedTime = time.Substring(0, 2) + ":" + time.Substring(2, 2);
        }
        catch (ArgumentOutOfRangeException exc)
        {
            lblTimingAlertMsg.Visible = true;
            lblTimingAlertMsg.Text = "Invalid Time.Please enter time between 0000 - 2359";
            dtStatus = false;
        }
        try
        {
            dtselectedTime = Convert.ToDateTime(selectedTime);
        }
        catch (FormatException ex)
        {
            lblTimingAlertMsg.Visible = true;
            lblTimingAlertMsg.Text = "Invalid Time.Please enter time between 0000 - 2359";
            dtStatus = false;
        }

        return dtStatus;
    }

    private string getPrevDayStatus(string selectedTime)
    {
        bool prevDayStatus = false;
        string time = "";
        DateTime dtselectedTime = DateTime.Now;
        //dtselectedTime = Convert.ToDateTime(selectedTime);
        dtselectedTime = UtilTime.CompDateTimeFromRef(selectedFlight.FLIGHTDATE, selectedTime);
        selectedTime = UtilTime.ConvDateTimeToUfisFullDTString(dtselectedTime);
      //  selectedTime = dtselectedTime.ToString("HH:mm");
        DateTime curTime = DateTime.Now;
        if (dtselectedTime > curTime)
        {
            //if (checkTimeforMidNight(dtselectedTime, curTime))
            //{
            //    prevDayStatus = true;
            //    time = getTimeInOracleFormat(prevDayStatus, selectedTime);
            //}
            //else
            //{
                lblTimingAlertMsg.Visible = true;
                lblTimingAlertMsg.Text = "Time entered must be earlier than current time - please re-enter";
                time = "Invalid";
            //}
        }
        else
        {
            time = getTimeInOracleFormat(prevDayStatus, selectedTime);

        }
        return time;
    }

    private bool checkTimeforMidNight(DateTime selTime, DateTime curTime)
    {
        if (curTime.Hour == 0 && selTime.Hour == 23)
        {
            return true;
        }
        return false;
    }
    private string getTimeInOracleFormat(bool prevDayStatus, string selectedTime)
    {
        
        string TimeInOracleFormat = selectedTime;
        return TimeInOracleFormat;
    }

    //private void updateFlightTiming(string selectedTime)
    //{
        
    //    string FlightURNO = FlightUrNo;
       
    //    string StaffID = Session["StaffID"].ToString();
    //    string selOption = "";
       

    //    selOption = this.MWUTiming1.getTextBoxLabel();
    //    string sendStatus = "";
    //    if (lblHidden.Text == selectedFlight.DISPLAYFLIGHTSMENU)
    //    {
    //        if (selectedFlight.ADID == "A" || selectedFlight.ADID == "T")
    //        {



    //           CtrlFlight.UpdateArrivalFlightTiming(null,FlightURNO, selOption, selectedTime, StaffID);
                
    //        }
    //    }
        
    //        else
    //    {
           
    //        RedirectToMobilePage("InvalidPage.aspx");
    //    }
        

    //}
    
    protected void cmdContinue_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["WITHREMARKS"] == "Yes")
            {
                ShowFormRemarkList();
            }
            else
            {
                ShowFormTiming(); 
            }
        }
        catch (Exception ex)
        {
            ShowCPMForm();
        }
        
    }
    protected void cmdContToBag_Click(object sender, EventArgs e)
    {
        ShowFormTripCPMAvail();
    }
    protected void Command1_Click(object sender, EventArgs e)
    {
        Session["WITHREMARKS"] = "No";
        EntCPMList cpmList = CPMAutoList;
        EntCPMList cpmListFree = CPMManualList;
        //lblH2TimingArrAprTrip.Text = MWUTiming1.getTextBoxLabel();
        EntCPMList cpmSelectedList = GetSelectedList();
        int cnt = cpmSelectedList.Count;
        if (cnt <= 0)
        {
            lblErrMsg.Text = "Please select a container";
            lblErrMsg.Visible = true;
        }
        else if (this.MWUTiming1.getTextBoxLabel() == "LTRIP")
        {
            if (GetUnSelectedList().Count != 0)
            {
                ShowFormMsg("LTRIP");
            }
            else
            {
                ActiveForm = frmTimingArrAprTrip;
            }
        }
        else
        {
            if (GetUnSelectedList().Count == 0)
            {
                MWUTiming1.setTextBoxLabel("LTRIP");
                

            }
            ActiveForm = frmTimingArrAprTrip;
        }
        lblH2TimingArrAprTrip.Text = MWUTiming1.getTextBoxLabel();
    }

    protected void Command2_Click(object sender, EventArgs e)
    {
        EntCPMList cpmSelectedList = GetSelectedList();
        int cnt = cpmSelectedList.Count;
        if (cnt <= 0)
        {
            lblErrMsg1.Text = "Please select a container";
            lblErrMsg1.Visible = true;
        }
        else
        {
            Session["WITHREMARKS"] = "No";

            MWUTiming1.setTextBoxLabel("TRIPS");
            lblH2TimingArrAprTrip.Text = MWUTiming1.getTextBoxLabel();
            ActiveForm = frmTimingArrAprTrip;
        }
    }

    protected void cmdPrevULDPage_Click(object sender, EventArgs e)
    {
        MWUCpmList1.GotoPrevPage();
        ShowCPMForm();
    }
    protected void cmdNextULDPage_Click(object sender, EventArgs e)
    {
        MWUCpmList1.GotoNextPage();
        ShowCPMForm();
    }

    protected override void OnViewStateExpire(EventArgs e)
    {

 iTrekUtils.iTUtils.LogToEventLog("ViewState Expired in Apron Arrival", "iTrekError");
       
        RedirectToMobilePage("Login.aspx");

    }
   
}
