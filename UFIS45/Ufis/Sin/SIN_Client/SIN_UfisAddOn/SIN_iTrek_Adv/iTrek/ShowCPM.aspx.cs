using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using UFIS.Wap.ITrek.WebMgr;

public partial class WAP_ShowCPM : System.Web.UI.MobileControls.MobilePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string uId = "";
        MgrSec.IsAuthenticatedUser(this, Session, out uId);
        string selection = "";

        try
        {
            selection = Request.QueryString["selection"].ToString();
            
        }
        catch (Exception ex)
        {
            
            
        }
        if (!IsPostBack)
        {
            
            string flightUrno = MgrSess.GetFlight(Session).URNO;
            //flightUrno = "1234";
            MWU_CompanyHeading1.ShowFlightInfoHeading();
            string turnUrno = MgrSess.GetFlight(Session).TURN;
            MWUShowCpm1.ShowCpm(flightUrno,turnUrno,MgrSess.GetFlight(Session).ADID,selection);
        }
    }
    protected void cmdBack_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["ORGCODE"].ToString().Substring(0, 1) == "A")
            {
                RedirectToMobilePage("Menu.aspx?MENU=SELFLT");
            }
            else
            {

                RedirectToMobilePage("Baggage.aspx?MENU=SELFLT");

            }
        }
        catch (Exception ex)
        {
            iTrekUtils.iTUtils.LogToEventLog("Error in Back of CPM" + ex.Message, "iTrekError");
        }
        
    }

    protected override void OnViewStateExpire(EventArgs e)
    {


        iTrekUtils.iTUtils.LogToEventLog("ViewState Expired in Show CPM", "iTrekError");
        RedirectToMobilePage("Login.aspx");

    }
}
