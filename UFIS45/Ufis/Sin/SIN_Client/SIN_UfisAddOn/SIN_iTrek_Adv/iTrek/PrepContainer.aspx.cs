using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.EventArg;
using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.Ctrl;

public partial class WAP_PrepContainer : System.Web.UI.MobileControls.MobilePage
{
    private const string CPM_MANUAL_LIST = "CPM_MANUAL_LIST";
    private const string CPM_AUTO_LIST = "CPM_AUTO_LIST";
    private const string CPM_RMK_IDX = "CPM_RMK_IDX";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ActiveForm = frmPrepContainer;
        }
       //mwuBagRemk1.RemarkSaveClick += new WAP_MWUBagRemk.RemarkSaveClickEventHandler(mwuBagRemk1_RemarkSaveClick);
       // mwuBagRemk1.RemarkBack_Click += new WAP_MWUBagRemk.RemarkBackClickEventHandler(mwuBagRemk1_RemarkBack_Click);
        mwuCpmManual1.LinkClick += new WAP_MWUCpmManual.LinkClickHandler(mwuCpmManual1_LinkClick);
    }

    void mwuCpmManual1_LinkClick(object sender, UFIS.ITrekLib.EventArg.EaLinkClickEventArgs e)
    {
        EntCPMList cpmManualList;
        try
        {
            cpmManualList = (EntCPMList)ViewState[CPM_MANUAL_LIST];
            cpmManualList = mwuCpmManual1.GetData(cpmManualList);
            ViewState[CPM_MANUAL_LIST] = cpmManualList;
            ViewState[CPM_RMK_IDX] = e.idx;
            ShowRemarkForm();
        }
        catch (Exception)
        {
        }
    }

    #region Remark Form
    //********************************
    private void ShowRemarkForm()
    {
        EntCPMList cpmList;
        int idx = (int)ViewState[CPM_RMK_IDX];
        EntRemark rmk = null;
        cpmList = (EntCPMList)ViewState[CPM_MANUAL_LIST];
        rmk = cpmList.GetData(idx).Remark;

        //mwuBagRemk1.SetCurRmk(rmk);
        //ActiveForm = frmRemark;
    }

    void mwuBagRemk1_RemarkSaveClick(object sender, UFIS.ITrekLib.EventArg.EaRemarkSaveEventArgs e)
    {
        SaveRemark(e.Remk);
        ActiveForm = frmPrepContainer;
    }

    private void SaveRemark(EntRemark rmk)
    {
        EntCPMList cpmList;
        int idx = (int)ViewState[CPM_RMK_IDX];
        cpmList = (EntCPMList)ViewState[CPM_MANUAL_LIST];
        cpmList.GetData(idx).Remark = rmk;
        ViewState[CPM_MANUAL_LIST] = cpmList;
    }

    void mwuBagRemk1_RemarkBack_Click(object sender, EventArgs e)
    {
        ActiveForm = frmPrepContainer;
    }

    //********************************
    #endregion

}
