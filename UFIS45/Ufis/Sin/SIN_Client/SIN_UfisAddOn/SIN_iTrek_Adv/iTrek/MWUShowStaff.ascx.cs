using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;

public partial class WAP_MWUShowStaff : System.Web.UI.MobileControls.MobileUserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void ShowStaff(string flightId)
    {
        //lstStaff.DataTextField = "DNAME";
        //lstStaff.DataValueField = "URNO";
        //lstStaff.DataSource = CtrlStaff.RetrieveFlightStaff(flightId).JOB;
        //lstStaff.DataBind();
        //lstStaff.Visible = true;
        tvStaff.Text = CtrlJob.RetrieveStaffsForAFlight(flightId).Replace("\n", "<br>");
    }

}
