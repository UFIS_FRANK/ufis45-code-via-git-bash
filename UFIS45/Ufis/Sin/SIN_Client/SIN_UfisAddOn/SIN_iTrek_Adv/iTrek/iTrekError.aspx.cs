using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using iTrekXML;
using UFIS.ITrekLib.Ctrl;
using UFIS.Wap.ITrek.WebMgr;

public partial class iTrekError : System.Web.UI.MobileControls.MobilePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        lblErrorMsg.Text = "Page Cannot be found.Please Login again";
    }
    protected void cmdError_Click(object sender, EventArgs e)
    {
        iTXMLcl iTXML = new iTXMLcl();
        if (Context.Session.Count == 0)
        {
            RedirectToMobilePage("Login.aspx");
        }
        else
        {

            string PENO = MgrSess.GetUser(Session);
            CtrlCurUser.DeleteDeviceIdEntryByStaffID(PENO);
           // iTXML.DeleteDeviceIDEntryByStaffId(PENO);
            Session.Clear();
            Session.Abandon();

            RedirectToMobilePage("Login.aspx");



        }
    }

    protected override void OnViewStateExpire(EventArgs e)
    {


        iTrekUtils.iTUtils.LogToEventLog("ViewState Expired in error page", "iTrekError");
        RedirectToMobilePage("Login.aspx");

    }
}
