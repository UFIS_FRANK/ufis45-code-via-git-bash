using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class Default : System.Web.UI.MobileControls.MobilePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Command1_Click(object sender, EventArgs e)
    {
        //Session.Abandon();
        //RedirectToMobilePage("Login.aspx");
        //iTrekUtils.iTUtils.LogToEventLog("Context.Session.Count" + Context.Session.Count, "iTrekError");
        if (Context.Session.Count == 0)
        {
            RedirectToMobilePage("Login.aspx");
        }
        else
        {
            string jobCategory = Session["ORGCODE"].ToString();

            if (jobCategory.Substring(0, 1) == "A")
            {
                RedirectToMobilePage("Menu.aspx?MENU=OP");
            }
            else

                if (jobCategory.Substring(0, 1) == "B")
                {
                    RedirectToMobilePage("Baggage.aspx");
                }


        }
    }
    protected override void OnViewStateExpire(EventArgs e)
    {

        if (Context.Session.Count == 0)
        {
            RedirectToMobilePage("Login.aspx");
        }
        else
        {
            string jobCategory = Session["ORGCODE"].ToString();

            if (jobCategory.Substring(0, 1) == "A")
            {
                RedirectToMobilePage("Menu.aspx?MENU=OP");
            }
            else

                if (jobCategory.Substring(0, 1) == "B")
                {
                    RedirectToMobilePage("Baggage.aspx");
                }


        }
        //Session.Abandon();
        //iTrekUtils.iTUtils.LogToEventLog("ViewState Expired in DEFAULT", "iTrekError");
        //RedirectToMobilePage("iTrekError.aspx");

    }

}
