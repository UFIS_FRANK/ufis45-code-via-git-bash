using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.ENT;
using UFIS.ITrekLib.EventArg;

public partial class WAP_MWUShowCpm : System.Web.UI.MobileControls.MobileUserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void ShowCpm(string flightId,string turnUrno,string adid,string selection)
    {
        string errMsg = "";
        try
        {
         if(adid=="A")
         {
            DataTable dt = CtrlTrip.RetrieveUnSentCPMandTripsInfo(flightId);
            lstCpm.DataTextField = "ULDINFO";
            lstCpm.DataValueField = "ULDNO";
            lstCpm.DataSource = dt;
            lstCpm.DataBind();
            if (dt.Rows.Count < 1) errMsg = "No CPM information.";
         }
         else if (adid == "D")
         {
             DataTable dt = CtrlTrip.RetrieveUnSentTripsInfo(flightId);
             lstCpm.DataTextField = "ULDINFO";
             lstCpm.DataValueField = "ULDNO";
             lstCpm.DataSource = dt;
             lstCpm.DataBind();
             if (dt.Rows.Count < 1) errMsg = "No TRIP information.";
         }
         else if (adid == "T")
         {
             if (selection == "cpm")
             {
                 DataTable dt = CtrlTrip.RetrieveUnSentCPMandTripsInfo(flightId);
                 lstCpm.DataTextField = "ULDINFO";
                 lstCpm.DataValueField = "ULDNO";
                 lstCpm.DataSource = dt;
                 lstCpm.DataBind();
                 if (dt.Rows.Count < 1) errMsg = "No CPM information.";
             }
             else
                 if (selection == "trips")
                 {

                     DataTable dt = CtrlTrip.RetrieveUnSentTripsInfo(turnUrno);
                     lstCpm.DataTextField = "ULDINFO";
                     lstCpm.DataValueField = "ULDNO";
                     lstCpm.DataSource = dt;
                     lstCpm.DataBind();
                     if (dt.Rows.Count < 1) errMsg = "No TRIP information.";
                 
                 }
         
         }

            
        }
        catch (ApplicationException ex)
        {
            errMsg = ex.Message;
        }
        catch (Exception ex)
        {
            errMsg = "Error in retrieving CPM Information.";
        }
        if (errMsg != "")
        {
            lblShowCpmErr.Text = errMsg;
            lblShowCpmErr.Visible = true;
        }
        else
        {
            lblShowCpmErr.Visible = false;
        }

    }
}
