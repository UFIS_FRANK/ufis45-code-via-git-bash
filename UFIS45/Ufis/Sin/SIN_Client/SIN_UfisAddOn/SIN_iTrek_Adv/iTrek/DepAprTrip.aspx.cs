using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.ENT;
using UFIS.Wap.ITrek.WebMgr;
using iTrekSessionMgr;
using iTrekXML;
using UFIS.ITrekLib.EventArg;
using UFIS.ITrekLib.Util;
using MM.UtilLib;
using System.Collections.Generic;

public partial class DepAprTrip : System.Web.UI.MobileControls.MobilePage
{
    private const string PFIX_RMK_LIST = "Rmk (";
    private const string SFIX_RMK_LIST = ")";
    private const string RMK_IND = "**";
    protected void Page_Load(object sender, EventArgs e)
    {
        string uId = "";
        MgrSec.IsAuthenticatedUser(this, Session, out uId);
        MWUTiming1.TimingSendClick += new WAP_MWUTiming.TimingSendHandler(MWUTiming1_TimingSendClick);
        MWU_CompanyHeading1.ShowFlightInfoHeading();
        MWU_CompanyHeading2.ShowFlightInfoHeading();
        MWU_CompanyHeading3.ShowFlightInfoHeading();
        MWU_CompanyHeading4.ShowFlightInfoHeading();
        lblHidden.Text = MWU_CompanyHeading4.GetFlightInfoHeading();
       // chkContainerList.DataSource = CtrlTrip.RetrieveTrip(MgrSess.GetFlight(Session).URNO);
        if (!IsPostBack)
        {
            getContainers();
            
        }
    }



    private void getContainers()
    {
        Label2.Text = "TRIPS";
        DSTrip dsTrip = new DSTrip();

        //DSTrip dsTrip = CtrlTrip.RetrieveTripWithMaxCnt(selectedFlight.URNO);
        if (selectedFlight.ADID == "T")
        {
            dsTrip = CtrlTrip.RetrieveTripWithMaxCnt(selectedFlight.TURN);
        }
        else
        {
           dsTrip = CtrlTrip.RetrieveTripWithMaxCnt(selectedFlight.URNO);
        }
        
        DataView dv = dsTrip.TRIP.DefaultView;
        
        dv.RowFilter = "IsNull(RCV_DT,'')=''";
        dv.Sort = "SENT_DT DESC";
        DSCpm dsContainer = new DSCpm();
        if (selectedFlight.ADID == "T")
        {
            dsContainer = CtrlCPM.RetrieveUldNoNotSend(selectedFlight.TURN);
        }
        else
        {
            dsContainer = CtrlCPM.RetrieveUldNoNotSend(selectedFlight.URNO);
        }
        //if (dsTrip.TRIP.Rows.Count != 0)
        if (dv.Count != 0)
        {
            chkContainerList.DataSource = dv;
            chkContainerList.DataMember = "TRIP";
            chkContainerList.DataTextField = "CPM_INFO";
            chkContainerList.DataValueField = "URNO";
            chkContainerList.DataBind();
            
        }
        else
        {
            Command1.Visible = false;
            cmdSendTrip.Visible = false;
            if (dsContainer.CPMDET.Rows.Count == 0)
            {
                lblErrMsg.Text = "Sorry,No ULDs registered by BO";
                lblErrMsg.Visible = true;

            }
            else
            {
                lblErrMsg.Text = "Baggage Preview";
                lblErrMsg.Visible = true;
            }

        }
       // iTrekUtils.iTUtils.LogToEventLog("selectedFlight" + selectedFlight.URNO, "iTrekError");
     //  DSContainer dsContainer= CtrlContainer.RetrieveUldNoNotSend(selectedFlight.URNO);
      // iTrekUtils.iTUtils.LogToEventLog("dsContainer" + dsContainer.Container.Rows.Count, "iTrekError");
        DSCpm.CPMDETRow[] containerRow = ((DSCpm.CPMDETRow[])dsContainer.CPMDET.Select());
       txtvUld.Text = "";
       foreach (DSCpm.CPMDETRow row in containerRow)
       {
           txtvUld.Text += row.ULDN + "</br>";
       
       }
    
    
    
    }
    protected void Command1_Click(object sender, EventArgs e)
    {
        lstRemarks.Items.Clear();

       
        string rmkInd = "";
        string selectedurn = "";
        if (selectedFlight.ADID == "T")
        {
            selectedurn = selectedFlight.TURN;
            
        }
        else
        {
            selectedurn = selectedFlight.URNO;
        }
        DSTrip dsTrip = CtrlTrip.RetrieveTripNotReceived(selectedurn);
       
        int cnt = dsTrip.TRIP.Count;
        MobileListItem mb ;

        foreach (DSTrip.TRIPRow trRow in dsTrip.TRIP.Select())
        {
            if (trRow["SENT_RMK"].ToString() !=null)
            {
                mb = new MobileListItem(rmkInd + PFIX_RMK_LIST + trRow["ULDN"].ToString() + SFIX_RMK_LIST, trRow["URNO"].ToString());
                lstRemarks.Items.Add(mb);
            }
        
        
        } 
       //lstRmkList.Items.Add(rmkInd + PFIX_RMK_LIST + cpmSelectedList.GetData(i).ContainerNo + SFIX_RMK_LIST);
        //lstRemarks.DataSource = 
        //lstRemarks.DataMember = "TRIP";
        //lstRemarks.DataTextField = "SENT_RMK";
        //lstRemarks.DataBind();
        ActiveForm = frmRemarkList;
    }
    protected void cmdBack_Click(object sender, EventArgs e)
    {
        ActiveForm = frmContainerList;
    }
    protected void lstRemarks_ItemCommand(object sender, ListCommandEventArgs e)
    {
       
       string selValue= e.ListItem.Value;
       
     //  DSTrip dsTrip = CtrlTrip.RetrieveTripforTripUrno(selectedFlight.URNO, selValue);
       if (selectedFlight.ADID == "T")
       {
           DSTrip dsTrip = CtrlTrip.RetrieveTripforTripUrno(selectedFlight.TURN, selValue);
           txtVRemarks.Text = dsTrip.TRIP[0]["SENT_RMK"].ToString();
       }
       else
       {
           DSTrip dsTrip = CtrlTrip.RetrieveTripforTripUrno(selectedFlight.URNO, selValue);
           txtVRemarks.Text = dsTrip.TRIP[0]["SENT_RMK"].ToString();
       
       }
       ActiveForm = frmViewRemarks;

    }
    protected void cmdSendTrip_Click(object sender, EventArgs e)
    {
        int chkCount=chkContainerList.Items.Count;
        List<string> lstChkdContainer = new List<string>();
        if (chkContainerList.Selection == null)
        {
            lblErrMsg.Text = "Please Select a Container";
           lblErrMsg.Visible = true;
            
        }
        else
        {
            for (int i = 0; i < chkCount; i++)
            {
                if (chkContainerList.Items[i].Selected)
                {
                    //iTrekUtils.iTUtils.LogToEventLog("urno" + chkContainerList.Items[i].Value, "iTrekError");
                    lstChkdContainer.Add(chkContainerList.Items[i].Value);
                }

            }
            Session["CHECKED_CONTAINER"] = lstChkdContainer;
           // txtVFlightDetailsTiming.Text = selectedFlight.DISPLAYFLIGHTSMENU;
            lblSelectedOption.Text = "TRIPS";
            this.MWUTiming1.setTextBoxLabel("TRIPS");
            ActiveForm = frmAOTiming;
        }
      
        
    }

    void MWUTiming1_TimingSendClick(object sender, EaTextEventArgs e)
    {//Send Trip Information to back end
        if (selectedFlight != null)
        {
            if (lblHidden.Text == selectedFlight.DISPLAYFLIGHTSMENU)
            {
                string time = e.Text;
                string selectedTime = "";
                string convTime = "Invalid";
                if (checkValidityOfTime(time))
                {
                    selectedTime = time.Substring(0, 2) + ":" + time.Substring(2, 2);

                    convTime = getPrevDayStatus(selectedTime);


                }

                if (convTime != "Invalid")
                {
                    try
                    {

                        SendTripData(convTime);

                    }
                    catch (Exception ex)
                    {
                        ShowFormMsg("Timing input was not successful.Please try to send the timing again" + ex.Message);
                    }
                }
            }
            else
            {
                lblHidden.Text = "";
                RedirectToMobilePage("InvalidPage.aspx");
                //RedirectToMobilePage("Menu.aspx?MENU=OP");
            }

        }
        else
        {
            lblHidden.Text = "";
            RedirectToMobilePage("InvalidPage.aspx");
           // RedirectToMobilePage("Menu.aspx?MENU=OP");
        }

    }

    private void ShowFormMsg(string msg)
    {
        lblTimingAlertMsg.Visible = true;
        lblTimingAlertMsg.Text = msg;
        //ActiveForm = frmMsgArrAprTrip;
    }
    private bool checkValidityOfTime(string time)
    {
        DateTime dtselectedTime = DateTime.Now;
        bool dtStatus = true;

        string selectedTime = "";



        try
        {
            selectedTime = time.Substring(0, 2) + ":" + time.Substring(2, 2);
        }
        catch (ArgumentOutOfRangeException exc)
        {

            lblTimingAlertMsg.Visible = true;
            lblTimingAlertMsg.Text = "Invalid Time.Please enter time between 0000 - 2359";
            dtStatus = false;
        }
        try
        {
            dtselectedTime = Convert.ToDateTime(selectedTime);
        }
        catch (FormatException ex)
        {
            lblTimingAlertMsg.Visible = true;
            lblTimingAlertMsg.Text = "Invalid Time.Please enter time between 0000 - 2359";
            dtStatus = false;
        }


        return dtStatus;
    }

    private string getPrevDayStatus(string selectedTime)
    {
        bool prevDayStatus = false;
        string time = "";
        DateTime dtselectedTime = DateTime.Now;
        //dtselectedTime = Convert.ToDateTime(selectedTime);
        dtselectedTime = UtilTime.CompDateTimeFromRef(selectedFlight.FLIGHTDATE, selectedTime);
        selectedTime = UtilTime.ConvDateTimeToUfisFullDTString(dtselectedTime);
        //selectedTime = dtselectedTime.ToString("HH:mm");
        DateTime curTime = DateTime.Now;
        if (dtselectedTime > curTime)
        {
            //if (checkTimeforMidNight(dtselectedTime, curTime))
            //{
            //    prevDayStatus = true;
            //    time = getTimeInOracleFormat(prevDayStatus, selectedTime);

            //}
            //else
            //{
                lblTimingAlertMsg.Visible = true;
                lblTimingAlertMsg.Text = "Time entered must be earlier than current time - please re-enter";
                time = "Invalid";
            //}
        }
        else
        {
            time = getTimeInOracleFormat(prevDayStatus, selectedTime);

        }
        return time;
    }

    private bool checkTimeforMidNight(DateTime selTime, DateTime curTime)
    {
        if (curTime.Hour == 0 && selTime.Hour == 23)
        {
            return true;
        }
        return false;
    }
    private string getTimeInOracleFormat(bool prevDayStatus, string selectedTime)
    {
        //iTXMLPathscl iTPaths = new iTXMLPathscl();
        //iTXMLcl iTXML = new iTXMLcl();
        //string TimeInOracleFormat = iTrekUtils.iTUtils.convertStandardDateTimeToOracleFormat(iTXML.ProcessSubmittedTime(selectedTime, prevDayStatus));
        string TimeInOracleFormat = selectedTime;
        return TimeInOracleFormat;
    }
private string UserId
    {
    
        get
        {
            string userId = MgrSess.GetUser(Session);
            //userId = MgrSess.GetUserId();
            return userId;
        }
    }
    
    private void SendTripData(string tripTime)
    {   //Send Trip Information to back end

        //if (selectedFlight.ADID == "T")
        //{
        //    CtrlTrip.SaveTrip(selectedFlight.TURN, ((ArrayList)Session["CHECKED_CONTAINER"]), tripTime, UserId, EnTripType.NextTrip, EnTripSendRecv.Receive);
        //}
        //else
        //{
        //    CtrlTrip.SaveTrip(selectedFlight.URNO, ((ArrayList)Session["CHECKED_CONTAINER"]), tripTime, UserId, EnTripType.NextTrip, EnTripSendRecv.Receive);
        //}
        if (lblHidden.Text == selectedFlight.DISPLAYFLIGHTSMENU)
        {
            if (selectedFlight.ADID == "T")
            {
                CtrlTrip.ReceiveTrip(selectedFlight.TURN, ((List<string>)Session["CHECKED_CONTAINER"]), tripTime, UserId, EnTripType.NextTrip);
            }
            else
            {
                CtrlTrip.ReceiveTrip(selectedFlight.URNO, ((List<string>)Session["CHECKED_CONTAINER"]), tripTime, UserId, EnTripType.NextTrip);
            }
            RedirectToMobilePage("Menu.aspx?MENU=SELFLT");
        }
        else
        {
            lblHidden.Text = "";
            RedirectToMobilePage("InvalidPage.aspx");
          //  RedirectToMobilePage("Menu.aspx?MENU=OP");
        }

        
    }
    private Flight selectedFlight
    {

        get
        {
            //Flight selFlt = ((Flight)Session["userFlight"]);
            Flight selFlt = MgrSess.GetFlight(Session);
            return selFlt;
        }

    }
    protected void cmdBack3_Click(object sender, EventArgs e)
    {
        ActiveForm = frmRemarkList;
    }
    protected void cmdBack1_Click(object sender, EventArgs e)
    {
        RedirectToMobilePage("Menu.aspx?MENU=SELFLT");
    }
    protected void Command2_Click(object sender, EventArgs e)
    {
        //getContainers();
        ActiveForm = frmContainerList;
    }
    protected override void OnViewStateExpire(EventArgs e)
    {


        iTrekUtils.iTUtils.LogToEventLog("ViewState Expired in Apron Departure", "iTrekError");
        RedirectToMobilePage("Login.aspx");

    }
    
}
