<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Baggage.aspx.cs" Inherits="Baggage" EnableSessionState="True"%>

<%@ Register Src="MWUShowStaff.ascx" TagName="MWUShowStaff" TagPrefix="uc1" %>


<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
   

    <mobile:Form ID="frmTerminal" Runat="server">
        <mobile:Label ID="Label1" Runat="server" Alignment="Center">SATS iTrek</mobile:Label>
        <mobile:Label ID="Label2" Runat="server" Alignment="Center">MAIN MENU</mobile:Label>
        <mobile:Label ID="lblUserIdName1" Runat="server" Alignment="Center">Label</mobile:Label>
      
        <mobile:List ID="lstTerminal" Runat="server" OnItemCommand="lstTerminal_ItemCommand">
        </mobile:List>
        <mobile:List ID="lstMenu" Runat="server" OnItemCommand="lstMenu_ItemCommand">
        </mobile:List>
            </mobile:Form>

    <mobile:Form id="frmBoMainMenu" runat="server">
<mobile:Label ID="lblTitle" Runat="server"
        Alignment="Center" EnableViewState="False">SATS iTrek</mobile:Label>
       
        <mobile:Label ID="lblHdrTerm" Runat="server" Alignment="Center">
        </mobile:Label>
      
        <mobile:Label ID="lblUserIdName" Runat="server" Alignment="Center">
        </mobile:Label>     
        <mobile:List ID="lstBOMainMenu" Runat="server" OnItemCommand="lstBOMainMenu_ItemCommand"  >
        </mobile:List> 
        <mobile:Command ID="cmdBack" Runat="server" OnClick="cmdBack_Click">Back</mobile:Command>
    </mobile:Form>
    <mobile:Form ID="frmBOFlights" Runat="server">
    <mobile:Label ID="lblTitle1" Runat="server"
        Alignment="Center" EnableViewState="False">SATS iTrek</mobile:Label>
        <mobile:Label ID="lblHdng" Runat="server"
            Alignment="Center" ></mobile:Label>
             <mobile:Label ID="lblFlights" Runat="server"
            Alignment="Center" >Flights</mobile:Label>
        <mobile:List ID="lstFlightsMenu" Runat="server" OnItemCommand="lstFlightsMenu_ItemCommand">
        </mobile:List>
        <mobile:Command ID="cmdNavigate" Runat="server" OnClick="cmdNavigate_Click">Previous Flights</mobile:Command>
        <mobile:Command ID="cmdBackFromFltMenu" Runat="server" OnClick="cmdBackFromFltMenu_Click">Back</mobile:Command>
    </mobile:Form>
    <mobile:Form ID="frmSelectedFltMenu" Runat="server">
    <mobile:Label ID="lblTitle2" Runat="server"
        Alignment="Center" EnableViewState="False">SATS iTrek</mobile:Label>
        <br />
        <mobile:TextView ID="lblFltHdng" Runat="server">
        </mobile:TextView>
        <br />
        <mobile:TextView ID="txtTiming" Runat="server"></mobile:TextView>
        <mobile:List ID="lstSelFltMenu" Runat="server" OnItemCommand="lstSelFltMenu_ItemCommand">
        </mobile:List>
        <mobile:Command ID="cmBackFromSelFlightMenu" Runat="server" OnClick="cmBackFromSelFlightMenu_Click">Back</mobile:Command>
    </mobile:Form>
    <mobile:Form ID="frmPrepContainer" Runat="server">
    <mobile:Label ID="lblTitle5" Runat="server" Alignment="Center">SATS iTrek</mobile:Label>
        <mobile:TextView ID="txtVSelFlt" Runat="server"></mobile:TextView>
      
        <mobile:Label ID="lblPrepContainer" Runat="server" Alignment="Center">Prep. Container
        </mobile:Label>
        <mobile:TextBox ID="txtBox1" Runat="server" MaxLength="15">
        </mobile:TextBox>
        <mobile:TextBox ID="txtBox2" Runat="server" MaxLength="15">
        </mobile:TextBox>
        <mobile:TextBox ID="txtBox3" Runat="server" MaxLength="15">
        </mobile:TextBox>
        <mobile:TextBox ID="txtBox4" Runat="server" MaxLength="15">
        </mobile:TextBox>
        <mobile:TextBox ID="txtBox5" Runat="server" MaxLength="15">
        </mobile:TextBox>
        <mobile:TextBox ID="txtBox6" Runat="server" MaxLength="15">
        </mobile:TextBox>
        <mobile:TextBox ID="txtBox7" Runat="server" MaxLength="15">
        </mobile:TextBox>
        <mobile:TextBox ID="txtBox8" Runat="server" MaxLength="15">
        </mobile:TextBox>
        <mobile:TextBox ID="txtBox9" Runat="server" MaxLength="15">
        </mobile:TextBox>
        <mobile:TextBox ID="txtBox10" Runat="server" MaxLength="15">
        </mobile:TextBox>
          <mobile:TextBox ID="txtBox11" Runat="server" MaxLength="15">
        </mobile:TextBox>
        <mobile:TextBox ID="txtBox12" Runat="server" MaxLength="15">
        </mobile:TextBox>
        <mobile:TextBox ID="txtBox13" Runat="server" MaxLength="15">
        </mobile:TextBox>
        <mobile:TextBox ID="txtBox14" Runat="server" MaxLength="15">
        </mobile:TextBox>
        <mobile:TextBox ID="txtBox15" Runat="server" MaxLength="15">
        </mobile:TextBox>
        <mobile:TextBox ID="txtBox16" Runat="server" MaxLength="15">
        </mobile:TextBox>
        <mobile:TextBox ID="txtBox17" Runat="server" MaxLength="15">
        </mobile:TextBox>
        <mobile:TextBox ID="txtBox18" Runat="server" MaxLength="15">
        </mobile:TextBox>
        <mobile:TextBox ID="txtBox19" Runat="server" MaxLength="15">
        </mobile:TextBox>
        <mobile:TextBox ID="txtBox20" Runat="server" MaxLength="15">
        </mobile:TextBox>
        <mobile:Command ID="cmdSavePrepContainer" Runat="server" OnClick="cmdSavePrepContainer_Click">Save</mobile:Command>
        <mobile:Command ID="cmdBackFromPrepCont" Runat="server" OnClick="cmdBackFromPrepCont_Click">Back</mobile:Command>
    </mobile:Form>
</body>
</html>
