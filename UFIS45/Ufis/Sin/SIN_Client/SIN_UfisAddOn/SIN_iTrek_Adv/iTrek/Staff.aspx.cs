using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.ENT;
using UFIS.Wap.ITrek.WebMgr;

public partial class WAP_Staff : System.Web.UI.MobileControls.MobilePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string uId = "";
        MgrSec.IsAuthenticatedUser(this, Session, out uId);        
        
    }

    protected void frmStaff_Activate(object sender, EventArgs e)
    {
        txtVFltHdng.Text = MgrSess.GetFlight(Session).DISPLAYFLIGHTSMENU;
        //lstStaff.DataTextField = "FINM";
        //lstStaff.DataValueField = "URNO";
        //lstStaff.DataSource = CtrlStaff.RetrieveFlightStaff(MgrSess.GetFlight(Session).URNO);
        //lstStaff.DataBind();
        //lstStaff.Visible = true;
       // tvStaff.Text = CtrlStaff.RetrieveStaffsForAFlight(MgrSess.GetFlight(Session).URNO).Replace("\n", "<br>");
        MWUShowStaff1.ShowStaff(MgrSess.GetFlight(Session).URNO);
    }

  

    protected void cmdBackFromStaff_Click(object sender, EventArgs e)
    {
        RedirectToMobilePage("Menu.aspx?MENU=SELFLT");
    }

    protected override void OnViewStateExpire(EventArgs e)
    {


        iTrekUtils.iTUtils.LogToEventLog("ViewState Expired in Show Staff", "iTrekError");
        RedirectToMobilePage("Login.aspx");

    }
}
