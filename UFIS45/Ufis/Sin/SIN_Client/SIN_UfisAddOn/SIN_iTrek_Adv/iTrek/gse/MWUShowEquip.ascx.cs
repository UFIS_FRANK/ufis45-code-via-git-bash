using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.ENT;
using UFIS.Wap.ITrek.WebMgr;

public partial class gse_MWUShowEquip : System.Web.UI.MobileControls.MobileUserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    public void ShowEquipment(string flightID,EnGseEquipmentStatus equipStatus)
    {
        List<string> equipList = new List<string>();
        equipList = CtrlGse.RetrieveEquipmentsForAFlight(flightID, equipStatus);
        StringBuilder sb = new StringBuilder();
        foreach (string equip in equipList)
        {
            sb.Append(equip);
            sb.Append("<br>");
        }
        if (string.IsNullOrEmpty(sb.ToString()))
        {
            tvEquip.Text = "No Equipment information.";
        }
        else
        {
            tvEquip.Text = sb.ToString();
        }
    }
}
