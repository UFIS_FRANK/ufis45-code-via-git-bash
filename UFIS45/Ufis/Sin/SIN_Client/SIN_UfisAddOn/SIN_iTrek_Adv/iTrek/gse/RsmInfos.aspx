<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RsmInfos.aspx.cs" Inherits="gse_RsmInfos" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="Form1" runat="server">
    <mobile:Label ID="lbHeader" Runat="server"></mobile:Label><br />
    <mobile:Panel ID="pnlLink" Runat="server">
    <mobile:Link ID="lnkPlba" NavigateUrl="Staff.aspx?rsm=PLB A" Runat="server" ></mobile:Link>
    <mobile:Link ID="lnkPlbb" NavigateUrl="Staff.aspx?rsm=PLB B" Runat="server" ></mobile:Link>
    <mobile:Link ID="lnkPlbc" NavigateUrl="Staff.aspx?rsm=PLB C" Runat="server" ></mobile:Link> 
    <mobile:Link ID="lnkFwd" NavigateUrl="Staff.aspx?rsm=GSE(FWD)" Runat="server" ></mobile:Link> 
    <mobile:Link ID="lnkAft" NavigateUrl="Staff.aspx?rsm=GSE(AFT)" Runat="server" ></mobile:Link> 
    <mobile:Command ID="cmdSave" Runat="server" OnClick="cmdSave_Click" SoftkeyLabel="Back">Submit</mobile:Command><br />
    <mobile:Command ID="cmdSaveDraft" Runat="server" OnClick="cmdSaveDraft_Click" SoftkeyLabel="" >Save as Draft</mobile:Command> 
    </mobile:Panel>
    <mobile:Panel ID="pnlText" Runat="server">
    <mobile:Label ID="lbPlba" Runat="server"></mobile:Label>
    <mobile:Label ID="lbPlbb" Runat="server"></mobile:Label>
    <mobile:Label ID="lbPlbc" Runat="server"></mobile:Label>
    <mobile:Label ID="lbFwd" Runat="server"></mobile:Label>
    <mobile:Label ID="lbAft" Runat="server"></mobile:Label>
    </mobile:Panel>
    <mobile:Command ID="cmdCancel" Runat="server" OnClick="cmdCancel_Click" SoftkeyLabel="" >Cancel</mobile:Command>&nbsp;
</mobile:Form>
</body>
</html>
