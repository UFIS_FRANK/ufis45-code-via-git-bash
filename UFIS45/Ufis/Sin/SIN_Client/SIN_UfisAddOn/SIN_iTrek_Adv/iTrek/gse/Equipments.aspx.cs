using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;


using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.ENT;
using UFIS.Wap.ITrek.WebMgr;

public partial class gse_Equipments : System.Web.UI.MobileControls.MobilePage
{
    protected string _btn1 = string.Empty;
    protected string _btn2 = string.Empty;
    private string _flightNo = string.Empty;
    private string _turnAroundNo = string.Empty;
    private string _reqPg = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {

        string uId = string.Empty;
        MgrSec.IsAuthenticatedUser(this, Session, out uId);
        _flightNo = MgrSess.GetFlight(Session).URNO;
        if (string.IsNullOrEmpty(_flightNo))
        {
            RedirectToMobilePage("../Menu.aspx");
        }

        if (!IsPostBack)
        {
            _btn1 = "PREV";
            _btn2 = "NEXT";
            MWUShowEquip.ShowEquipment(_flightNo, EnGseEquipmentStatus.Current);
            cmdBtn1.Text = _btn1;
            cmdBtn1.SoftkeyLabel = _btn1;
            cmdBtn2.Text = _btn2;
            cmdBtn2.SoftkeyLabel = _btn2;
        }

        //Set the request url
        _reqPg = Request.QueryString["reqPg"];
        if (string.IsNullOrEmpty(_reqPg))
        {
            RedirectToMobilePage("../Menu.aspx?MENU=SELFLT");
        }
        else
        {
            if (_reqPg.Contains("Menu.aspx"))
            {
                _reqPg = "../Menu.aspx?MENU=SELFLT";
            }
            else if (_reqPg.Contains("ClosedFlight.aspx"))
            {
                _reqPg = "../" + _reqPg + "?urno=" + _flightNo;
            }
            MgrSess.SetBackUrl(Session, _reqPg);           
        }
    }

    protected void frmEquip_Activate(object sender, EventArgs e)
    {
        txtVFltHdng.Text = CtrlFlight.GetFlightFromFlightDataset(_flightNo).DISPLAYFLIGHTSMENU; //MgrSess.GetFlight(Session).DISPLAYFLIGHTSMENU;
        txtVEquip.Text = "CURRENT";
    }

    protected void cmdBackFromEquip_Click(object sender, EventArgs e)
    {
        //RedirectToMobilePage("../Menu.aspx?MENU=SELFLT");
        RedirectToMobilePage(MgrSess.GetBackUrl(Session));
        
    }

    protected override void OnViewStateExpire(EventArgs e)
    {
        iTrekUtils.iTUtils.LogToEventLog("ViewState Expired in Show Equip", "iTrekError");
        RedirectToMobilePage("Login.aspx");
    }

    protected void cmdBtn1_Click(object sender, EventArgs e)
    {
        if (cmdBtn1.Text == "PREV")
        {
            _btn1 = "CURR";
            _btn2 = "NEXT";
            txtVEquip.Text = "PREVIOUS";
            MWUShowEquip.ShowEquipment(_flightNo,EnGseEquipmentStatus.Previous);
        }
        else if (cmdBtn1.Text == "CURR")
        {
            _btn1 = "PREV";
            _btn2 = "NEXT";
            txtVEquip.Text = "CURRENT";
            MWUShowEquip.ShowEquipment(_flightNo,EnGseEquipmentStatus.Current);
        }
        cmdBtn1.Text = _btn1;
        cmdBtn1.SoftkeyLabel = _btn1;
        cmdBtn2.Text = _btn2;
        cmdBtn2.SoftkeyLabel = _btn2;
    }
    protected void cmdBtn2_Click(object sender, EventArgs e)
    {
        if (cmdBtn2.Text == "NEXT")
        {
            _btn1 = "CURR";
            _btn2 = "PREV";
            txtVEquip.Text = "NEXT";
            MWUShowEquip.ShowEquipment(_flightNo,EnGseEquipmentStatus.Next);
        }
        else if (cmdBtn2.Text == "PREV")
        {
            _btn1 = "CURR";
            _btn2 = "NEXT";
            txtVEquip.Text = "PREVIOUS";
            MWUShowEquip.ShowEquipment(_flightNo,EnGseEquipmentStatus.Previous);
        }
        cmdBtn1.Text = _btn1;
        cmdBtn1.SoftkeyLabel = _btn1;
        cmdBtn2.Text = _btn2;
        cmdBtn2.SoftkeyLabel = _btn2;
    }
}
