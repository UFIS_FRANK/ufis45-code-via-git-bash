<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Staff.aspx.cs" Inherits="gse_Staff" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="Form1" runat="server">
    <mobile:Label ID="lbHeader" Runat="server" Font-Bold="true" ></mobile:Label><br />
    <mobile:SelectionList ID="lsStaff" Runat="server" SelectType="DropDown">        
    </mobile:SelectionList>
    <mobile:TextBox ID="txtStaff" Runat="server"> </mobile:TextBox>
    <mobile:Command ID="cmdOk" Runat="server" OnItemCommand="cmdOk_Click">OK</mobile:Command>
    <mobile:Command ID="cmdCancel" Runat="server" OnItemCommand="cmdCancel_Click">Cancel</mobile:Command>
    
    </mobile:Form>
</body>
</html>
