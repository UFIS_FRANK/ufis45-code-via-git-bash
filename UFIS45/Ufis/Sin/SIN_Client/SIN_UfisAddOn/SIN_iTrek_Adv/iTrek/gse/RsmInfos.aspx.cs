using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DSDB;
using UFIS.ITrekLib.ENT;
using UFIS.Wap.ITrek.WebMgr;
using MM.UtilLib;

public partial class gse_RsmInfos : System.Web.UI.MobileControls.MobilePage
{
    private string _flightNo = string.Empty;
    private string _reqPg = string.Empty;
    protected static string _strPlba = "PLB A";
    protected static string _strPlbb = "PLB B";
    protected static string _strPlbc = "PLB C";
    protected static string _strFwd = "GSE(FWD)";
    protected static string _strAft = "GSE(AFT)";
    private string sperator = "/";
    bool isSubmit = false;
    Hashtable htRsmStaff = null;
    DsDbAsr1 dsAsr = new DsDbAsr1();
    string _uId = string.Empty;
    EntUser curUser = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        MgrSec.IsAuthenticatedUser(this, Session, out _uId);
        curUser = new EntUser();
        curUser.UserId = _uId;

        lbHeader.Text = MgrSess.GetFlight(Session).DISPLAYRSMMENU;
        _flightNo = MgrSess.GetFlight(Session).URNO;
        if (string.IsNullOrEmpty(_flightNo))
        {
            RedirectToMobilePage("../Menu.aspx");
        }
        dsAsr = CtrlAsr.RetrieveAsr(_flightNo, string.Empty);
        isSubmit = UtilDataSet.GetDataString(dsAsr.V_ITK_ASR1.Rows[0], "PSTA") == "S" ? true : false;
        cmdSave.Visible = !isSubmit; //Hide buttons if submitted
        cmdSaveDraft.Visible = !isSubmit;
        
        
        htRsmStaff = MgrSess.GetRsmStaff(Session);
        //Assignment not found in Session so check in the db from ASR 
        if (htRsmStaff == null)
        {
            GetAssignedGse(ref htRsmStaff);
        }
        if (htRsmStaff != null)
        {
            if (isSubmit)
            {
                PopulateViewOnly();                               
            }
            else
            {
                PopulateSessionForm();
            }
        }
        else
        {
            PopulateCleanForm();
        }
        //Set the request url
        _reqPg = Request.QueryString["reqPg"];
        if (string.IsNullOrEmpty(_reqPg))
        {
            if (string.IsNullOrEmpty(MgrSess.GetBackUrl(Session)))
            {
                RedirectToMobilePage("../Menu.aspx?MENU=SELFLT");
            }
            else
            {
                if (MgrSess.GetBackUrl(Session).Contains("Menu.aspx"))
                {
                    cmdSave.Visible = false;
                }
            }
        }
        else
        {
            if (_reqPg.Contains("Menu.aspx"))
            {
                _reqPg = "../Menu.aspx?MENU=SELFLT";
                cmdSave.Visible = false;
            }
            else if (_reqPg.Contains("ClosedFlight.aspx"))
            {
                _reqPg = "../" + _reqPg + "?urno=" + _flightNo;
            }
            MgrSess.SetBackUrl(Session, _reqPg);
        }
    }

    private void PopulateViewOnly()
    {
        cmdSave.Visible = false;
        cmdSaveDraft.Visible = false;
        pnlLink.Visible = false;
        pnlText.Visible = true;
        lbPlba.Text = _strPlba + GetHashtableValue(htRsmStaff, _strPlba);
        lbPlbb.Text = _strPlbb + GetHashtableValue(htRsmStaff, _strPlbb);
        lbPlbc.Text = _strPlbc + GetHashtableValue(htRsmStaff, _strPlbc);
        lbFwd.Text = _strFwd + GetHashtableValue(htRsmStaff, _strFwd);
        lbAft.Text = _strAft + GetHashtableValue(htRsmStaff, _strAft);      

    }

    private void PopulateSessionForm()
    {
        pnlLink.Visible = true;
        pnlText.Visible = false;
        lnkPlba.Text = _strPlba + GetHashtableValue(htRsmStaff, _strPlba);
        lnkPlbb.Text = _strPlbb + GetHashtableValue(htRsmStaff, _strPlbb);
        lnkPlbc.Text = _strPlbc + GetHashtableValue(htRsmStaff, _strPlbc);
        lnkFwd.Text = _strFwd + GetHashtableValue(htRsmStaff, _strFwd);
        lnkAft.Text = _strAft + GetHashtableValue(htRsmStaff, _strAft);
    }
    
    private void GetAssignedGse(ref Hashtable htRsmStaff)
    {
        htRsmStaff = new Hashtable();
        Hashtable htColKey = new Hashtable();
        htColKey.Add("PLBA_P", _strPlba);
        htColKey.Add("PLBB_P", _strPlbb);
        htColKey.Add("PLBC_P", _strPlbc);
        htColKey.Add("GSE1_P", _strFwd);
        htColKey.Add("GSE2_P", _strAft);

        foreach(DsDbAsr1.V_ITK_ASR1Row row in dsAsr.V_ITK_ASR1)
        {
            string strValue = string.Empty;
            string dbValue = string.Empty;

            foreach (string col in htColKey.Keys)
            {
                dbValue = (string) (row[col]==DBNull.Value?string.Empty:row[col]);
                strValue = (string)htColKey[col];
                if (!string.IsNullOrEmpty(dbValue))
                {
                    if (htRsmStaff.Contains(strValue))
                    {
                        htRsmStaff[strValue] = dbValue;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(dbValue))
                            htRsmStaff.Add(strValue, dbValue);
                    }
                }
            }
        }
        MgrSess.SetRsmStaff(Session, htRsmStaff);
    }

    private string GetHashtableValue(Hashtable ht,string key)
    {
        string value = string.Empty;
        if (ht != null)
        {
            if (ht.ContainsKey(key))
            {
                value = (string)ht[key];
            }
        }
        if (value != string.Empty)
        {
            value = sperator + value ;
        }
        return value;
    }

    private void PopulateCleanForm()
    {
        lnkPlba.Text = _strPlba;
        lnkPlbb.Text = _strPlbb;
        lnkPlbc.Text = _strPlbc;
        lnkFwd.Text = _strFwd;
        lnkAft.Text = _strAft;

    }

    protected void cmdSave_Click(object sender, EventArgs e)
    {
        DsDbAsr1 dsAsr = new DsDbAsr1();
        PrepareDsToSave(dsAsr, true);


        CtrlAsr.SaveASR(null, _flightNo, dsAsr, curUser, EnAsrSaveType.Rsm);
        PopulateViewOnly();
    }
    
    protected void cmdSaveDraft_Click(object sender, EventArgs e)
    {
        DsDbAsr1 dsAsr = new DsDbAsr1();
        PrepareDsToSave(dsAsr,false);
        CtrlAsr.SaveASR(null, _flightNo, dsAsr, curUser, EnAsrSaveType.Rsm);
    }
    
    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        MgrSess.SetRsmStaff(Session,null);
        RedirectToMobilePage(MgrSess.GetBackUrl(Session));

    }

    private void PrepareDsToSave(DsDbAsr1 dsAsr,bool isSubmit)
    {
        DsDbAsr1.V_ITK_ASR1Row rowAsr = dsAsr.V_ITK_ASR1.NewV_ITK_ASR1Row();
        rowAsr.URNO = _flightNo;
        rowAsr.PLBA_P = htRsmStaff[_strPlba] == null ? string.Empty : htRsmStaff[_strPlba].ToString();
        rowAsr.PLBB_P = htRsmStaff[_strPlbb] == null ? string.Empty : htRsmStaff[_strPlbb].ToString();
        rowAsr.PLBC_P = htRsmStaff[_strPlbc] == null ? string.Empty : htRsmStaff[_strPlbc].ToString();
        rowAsr.GSE1_P = htRsmStaff[_strFwd] == null ? string.Empty : htRsmStaff[_strFwd].ToString();
        rowAsr.GSE2_P = htRsmStaff[_strAft] == null ? string.Empty : htRsmStaff[_strAft].ToString();
        rowAsr.PSTA = isSubmit ? "N": "Y";
        dsAsr.V_ITK_ASR1.AddV_ITK_ASR1Row(rowAsr);
    }
}
