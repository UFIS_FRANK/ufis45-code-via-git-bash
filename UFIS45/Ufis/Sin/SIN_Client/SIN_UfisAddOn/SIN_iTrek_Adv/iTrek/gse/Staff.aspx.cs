using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using UFIS.ITrekLib.DS;
using UFIS.ITrekLib.DSDB;
using UFIS.ITrekLib.Ctrl;
using UFIS.Wap.ITrek.WebMgr;
using UFIS.ITrekLib.ENT;

public partial class gse_Staff : System.Web.UI.MobileControls.MobilePage
{
    protected static string _strPlba = "PLB A";
    protected static string _strPlbb = "PLB B";
    protected static string _strPlbc = "PLB C";
    protected static string _strFwd = "GSE(FWD)";
    protected static string _strAft = "GSE(AFT)";
    private string _flightID = string.Empty;
    private Hashtable _htRsmStaff;
    private string _rsm = string.Empty;
    string _uId = string.Empty;
    EntUser curUser = null;

    protected void Page_Load(object sender, EventArgs e)
    {

        MgrSec.IsAuthenticatedUser(this, Session, out _uId);
        curUser = new EntUser();
        curUser.UserId = _uId;

        _flightID = MgrSess.GetFlight(Session).URNO;
        if(string.IsNullOrEmpty(_flightID))
        {
            RedirectToMobilePage("../Menu.aspx");
        }
        _htRsmStaff = MgrSess.GetRsmStaff(Session);
        _rsm = Request.QueryString["rsm"];
        if (string.IsNullOrEmpty(_rsm))
        {
            //Cannot directly come to Staff page to assign RSM
            RedirectToMobilePage("RsmInfos.aspx");
        }
        else
        {
            lbHeader.Text = _rsm;
            cmdOk.CommandArgument = _rsm;
        }
        
        if (!IsPostBack)
        {
            PopulateForm();
        }
    }

    private void PopulateForm()
    {
//#warning For testing only
        //_flightID = "556501073";
        DSJob dsJob = CtrlStaff.RetrieveFlightStaffWithoutBA(_flightID);

        PopulateList(lsStaff, dsJob);
        MobileListItem itemOth = new MobileListItem("Other", "0");
        lsStaff.Items.Add(itemOth);

        if (_htRsmStaff != null)
        {
            if (_htRsmStaff.ContainsKey(_rsm))
            {
                string valueToSelect =(string)_htRsmStaff[_rsm];
                foreach (MobileListItem item in lsStaff.Items)
                {
                    if (item.Text == valueToSelect)
                    {
                        item.Selected = true;
                    }
                }
            }
        }
        
        
    }
    private void PopulateList(SelectionList ls, DataSet ds)
    {
        ls.DataSource = ((DSJob)ds).JOB;
        ls.DataTextField = ((DSJob)ds).JOB.NAMEIDColumn.ColumnName;
        ls.DataValueField = ((DSJob)ds).JOB.URNOColumn.ColumnName;
        ls.DataBind();
    }

    protected void cmdOk_Click(object sender, EventArgs e)
    {
        _htRsmStaff = MgrSess.GetRsmStaff(Session);
        if (_htRsmStaff == null)
        {
            _htRsmStaff = new Hashtable();
        }
        string rsm = cmdOk.CommandArgument;
        string selectedText = string.Empty;

        if (lsStaff.Items.Count> 0)
        {
            selectedText = lsStaff.Selection.Text;            
        }
        if (selectedText.ToLower() == "other")
        {
            selectedText = txtStaff.Text;
        }
        if (_htRsmStaff.Contains(rsm))
        {
            _htRsmStaff[rsm] = selectedText;
        }
        else
        {
            _htRsmStaff.Add(rsm, selectedText);
        }

        MgrSess.SetRsmStaff(Session, _htRsmStaff);
        DsDbAsr1 dsAsr = new DsDbAsr1();
        PrepareDsToSave(dsAsr);
        CtrlAsr.SaveASR(null, _flightID, dsAsr, curUser, EnAsrSaveType.Rsm);

        RedirectToMobilePage("RsmInfos.aspx");
    }

    private void PrepareDsToSave(DsDbAsr1 dsAsr)
    {
        DsDbAsr1.V_ITK_ASR1Row rowAsr = dsAsr.V_ITK_ASR1.NewV_ITK_ASR1Row();
        rowAsr.URNO = _flightID;
        rowAsr.PLBA_P = _htRsmStaff[_strPlba] == null ? string.Empty : _htRsmStaff[_strPlba].ToString();
        rowAsr.PLBB_P = _htRsmStaff[_strPlbb] == null ? string.Empty : _htRsmStaff[_strPlbb].ToString();
        rowAsr.PLBC_P = _htRsmStaff[_strPlbc] == null ? string.Empty : _htRsmStaff[_strPlbc].ToString();
        rowAsr.GSE1_P = _htRsmStaff[_strFwd] == null ? string.Empty : _htRsmStaff[_strFwd].ToString();
        rowAsr.GSE2_P = _htRsmStaff[_strAft] == null ? string.Empty : _htRsmStaff[_strAft].ToString();
        rowAsr.PSTA = "Y";
        dsAsr.V_ITK_ASR1.AddV_ITK_ASR1Row(rowAsr);
    }

    protected void cmdCancel_Click(object sender, EventArgs e)
    {
        RedirectToMobilePage("RsmInfos.aspx");
    }
}
