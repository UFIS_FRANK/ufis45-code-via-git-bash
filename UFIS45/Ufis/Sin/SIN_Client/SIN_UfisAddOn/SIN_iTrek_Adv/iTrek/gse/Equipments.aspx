<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Equipments.aspx.cs" Inherits="gse_Equipments" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register Src="~/gse/MWUShowEquip.ascx" TagName="MWUShowEquip" TagPrefix="uc1" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmEquip" runat="server" OnActivate="frmEquip_Activate">
    <%--<mobile:Link ID="lkVeh" Runat="server" NavigateUrl="GseInfos.aspx" SoftkeyLabel="VehicleLink">GSE Infos</mobile:Link>--%>
    <mobile:Label ID="lblHead1" Runat="server" Alignment="Center" Font-Bold="True">SATS iTrek</mobile:Label><br />
    <mobile:TextView ID="txtVFltHdng" Runat="server"></mobile:TextView><br />
    <mobile:Label ID="lblEquipFunc" Runat="server" Alignment="Center" Font-Bold="True">Equipments</mobile:Label> &nbsp; 
    <mobile:TextView ID="txtVEquip" Runat="server"></mobile:TextView><br />
    <uc1:MWUShowEquip ID="MWUShowEquip" runat="server" /> 
    <mobile:Command ID="cmdBackFromEquip" Runat="server" OnClick="cmdBackFromEquip_Click" SoftkeyLabel="Back">FLIGHT MENU</mobile:Command><br/>
    <mobile:Command ID="cmdBtn1" Runat="server" OnClick="cmdBtn1_Click" SoftkeyLabel="" ></mobile:Command> 
    <mobile:Command ID="cmdBtn2" Runat="server" OnClick="cmdBtn2_Click" SoftkeyLabel="" ></mobile:Command>&nbsp; 
    
         
    
 
</mobile:Form>
    
  
</body>
</html>
