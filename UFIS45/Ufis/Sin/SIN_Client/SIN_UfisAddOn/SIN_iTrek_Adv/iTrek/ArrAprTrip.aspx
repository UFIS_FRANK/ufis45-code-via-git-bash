<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ArrAprTrip.aspx.cs" Inherits="WAP_ArrAprTrip" EnableSessionState="True"%>

<%@ Register Src="MWUCpmManual.ascx" TagName="MWUCpmManual" TagPrefix="uc5" %>

<%@ Register Src="MWUCpmList.ascx" TagName="MWUCpmList" TagPrefix="uc4" %>

<%@ Register Src="MWU_CompanyHeading.ascx" TagName="MWU_CompanyHeading" TagPrefix="uc1" %>
<%@ Register Src="MWUBagRemk.ascx" TagName="MWUBagRemk" TagPrefix="uc2" %>
<%@ Register Src="MWUTiming.ascx" TagName="MWUTiming" TagPrefix="uc3" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmTripCPMAvail" runat="server"><uc1:MWU_CompanyHeading ID="MWU_CompanyHeading1" runat="server" /> <mobile:Label ID="lblH2ULDInfoTrip" Runat="server">
        </mobile:Label> <mobile:Label ID="lblH3CPMAvail" Runat="server">
        </mobile:Label> <br /><mobile:Label ID="lblErrMsg" Runat="server" Visible="False">
        </mobile:Label> <br /><uc4:MWUCpmList ID="MWUCpmList1" runat="server" ChkBoxOrRadioButton="C"
            RemarkCmdVisible="false" EnableViewState="true" ItemPerPage="10" /> <uc5:MWUCpmManual ID="MWUCpmManual1" runat="server" HasChkBox="false"
                HasLink="false" NumOfEntry="4" />&nbsp;<mobile:Command ID="cmdPrevULDPage" Runat="server" OnClick="cmdPrevULDPage_Click">Prev ULDs</mobile:Command> <mobile:Command
                    ID="cmdNextULDPage" Runat="server" OnClick="cmdNextULDPage_Click">Next ULDs</mobile:Command> <mobile:Command ID="cmdContinueCPMAvailArrAprTrip" Runat="server" OnClick="cmdContinueCPMAvailArrAprTrip_Click">Next/Add Remk</mobile:Command><br /><mobile:Command ID="Command1" Runat="server" OnClick="Command1_Click">Send</mobile:Command> <br /><mobile:Command ID="cmdBackCPMAvailArrAprTrip" Runat="server" OnClick="cmdBackCPMAvailArrAprTrip_Click">Back</mobile:Command></mobile:Form>
    
    <mobile:Form ID="frmTripNoCPM" Runat="server"><uc1:MWU_CompanyHeading ID="MWU_CompanyHeading2" runat="server" /> <mobile:Label ID="lblNoCPMTripNoCPM" Runat="server" Alignment="Center">NO CPM AVAILABLE</mobile:Label> <br /><mobile:Label ID="lblErrMsg1" Runat="server" Visible="False">
        </mobile:Label> <br /><mobile:SelectionList ID="slType" Runat="server" SelectType="CheckBox">
            <Item Text="INTER" Value="I" />
        </mobile:SelectionList>  <mobile:TextBox ID="txtIntCont" Runat="server" Numeric="True" MaxLength="1"  Size="1">
        </mobile:TextBox> <mobile:Label ID="Label2" Runat="server" >CONTAINER</mobile:Label> <mobile:Label ID="Label6" Runat="server" Alignment="Right"></mobile:Label> <mobile:TextBox ID="txtIntBt" Runat="server"  Numeric="True" MaxLength="1" Size="1" Alignment="Left">
        </mobile:TextBox><mobile:Label ID="Label3" Runat="server" Alignment="Left" >BT</mobile:Label> <mobile:SelectionList ID="slType2" Runat="server" Rows="1" SelectType="CheckBox">
            <Item Text="LOCAL" Value="L" />
</mobile:SelectionList><mobile:TextBox ID="txtLocCont" Runat="server" Numeric="True" MaxLength="1" Size="1"></mobile:TextBox><mobile:Label ID="Label5" Runat="server">CONTAINER</mobile:Label><mobile:Label ID="Label7" Runat="server" Alignment="Right"></mobile:Label>
        <mobile:TextBox ID="txtLocBt" Runat="server" Numeric="True" MaxLength="1" Size="1">
        </mobile:TextBox> <mobile:Label ID="Label4" Runat="server">BT</mobile:Label><mobile:Command ID="cmdContinuerNoCPMArrAprTrip" Runat="server" OnClick="cmdContinuerNoCPMArrAprTrip_Click">Add Remarks</mobile:Command> <mobile:Command ID="Command2" Runat="server" OnClick="Command2_Click">Send</mobile:Command> <br /><mobile:Command ID="cmdBackNoCPMArrAprTrip" Runat="server" OnClick="cmdBackNoCPMArrAprTrip_Click">Back</mobile:Command></mobile:Form>&nbsp;
           
    <mobile:Form ID="frmRemarkList" Runat="server">
        <uc1:MWU_CompanyHeading ID="MWU_CompanyHeading6" runat="server" />
        <mobile:Label ID="lblH2RemarkList" Runat="server" Alignment="Center" Font-Bold="True">Create Remark</mobile:Label>
        <mobile:Label ID="lblNoDataRemkList" Runat="server" Visible="False">
        </mobile:Label>
        <mobile:List ID="lstRmkList" Runat="server" Decoration="Numbered" OnItemCommand="lstRmkList_ItemCommand">
        </mobile:List>
        <mobile:Command ID="cmdContinueRemarkList" Runat="server" OnClick="cmdContinueRemarkList_Click">Send</mobile:Command>
        <mobile:Command ID="cmdBackRemarkList" Runat="server" OnClick="cmdBackRemarkList_Click">Back</mobile:Command>
    </mobile:Form>&nbsp;
    <mobile:Form ID="frmRemk" Runat="server">
        <uc1:MWU_CompanyHeading ID="MWU_CompanyHeading3" runat="server" />
        <mobile:Label ID="lblH2Rmk" Runat="server" Alignment="Center" Font-Bold="True">REMARK</mobile:Label>
        <mobile:Label ID="lblContainerNo" Runat="server">
        </mobile:Label>
        <uc2:MWUBagRemk ID="MWUBagRemk1" runat="server" /></mobile:Form>
    <mobile:Form ID="frmTimingArrAprTrip" Runat="server" OnActivate="frmTimingArrAprTrip_Activate">
        <uc1:MWU_CompanyHeading ID="MWU_CompanyHeading4" runat="server" />
        <mobile:Label ID="lblHidden" Runat="server" Visible="False">
        </mobile:Label>
        <br />
        <mobile:Label ID="lblH2TimingArrAprTrip" Runat="server" Alignment="Center" Font-Bold="True"></mobile:Label>
        <mobile:Label ID="lblTimingAlertMsg" Runat="server" Visible="False">
        </mobile:Label>
        <br />
        <uc3:MWUTiming ID="MWUTiming1" runat="server"  />
        <mobile:Command ID="cmdBackTimingArrAprTrip" Runat="server" OnClick="cmdBackTimingArrAprTrip_Click">Back</mobile:Command>
    </mobile:Form>
    <mobile:Form ID="frmMsgArrAprTrip" Runat="server">
        <uc1:MWU_CompanyHeading ID="MWU_CompanyHeading5" runat="server" />
        <mobile:Label ID="Label1" Runat="server" Alignment="Center">WARNING</mobile:Label>
        <br />
        <mobile:Label ID="lblMsgMsgArrAprTrip" Runat="server"></mobile:Label>
        <mobile:Command ID="cmdContToBag" Runat="server" Visible="False" OnClick="cmdContToBag_Click">Select ULD</mobile:Command>
        <br />
        <mobile:Command ID="cmdContinue" Runat="server" Visible="False" OnClick="cmdContinue_Click">Continue</mobile:Command>
        <br />
        <mobile:Command ID="cmdBackMsgArrAprTrip" Runat="server" OnClick="cmdBackMsgArrAprTrip_Click">Back</mobile:Command>
    </mobile:Form>
</body>
</html>
