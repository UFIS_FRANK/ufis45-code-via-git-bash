<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CPM_Ap_Arr.aspx.cs" Inherits="WAP_CPM_Ap_Arr" %>

<%@ Register Src="MWUCpmManual.ascx" TagName="MWUCpmManual" TagPrefix="uc2" %>

<%@ Register Src="MWUBagRemk.ascx" TagName="MWUBagRemk" TagPrefix="uc3" %>

<%@ Register Src="MWUCpmList.ascx" TagName="MWUCpmList" TagPrefix="uc1" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmCPMApArr" runat="server" OnActivate="frmCPMApArr_Activate"><mobile:Label ID="lblITrekHead" Runat="server" Alignment="Center" Font-Bold="True">SATS iTrek</mobile:Label> <mobile:Label ID="lblFlightInfoHeading" Runat="server" Alignment="Center" Font-Bold="True">
        </mobile:Label> <mobile:Label ID="lblCPMHeader" Runat="server" Alignment="Center">
        </mobile:Label> <mobile:Label ID="lblCPM" Runat="server" Alignment="Center">CPM</mobile:Label> 
        <mobile:Command ID="cmdCPMApArrSend1" Runat="server" OnClick="cmdCPMApArrSend1_Click">Send</mobile:Command>
            <mobile:Panel
                ID="Panel1" Runat="server" Alignment="Left" BreakAfter="True" Wrapping="NoWrap">
                <uc1:MWUCpmList ID="mwuCpmList1" runat="server" />
                <uc2:MWUCpmManual ID="mwuCpmManual1" runat="server" HasChkBox="true" HasLink="true"
                    NumOfEntry="4" /></mobile:Panel> 
            <br />
            <mobile:Command ID="cmdCPMApArrSend2" Runat="server" SoftkeyLabel="Send" OnClick="cmdCPMApArrSend2_Click">Send</mobile:Command> 
            <mobile:Command ID="cmdCPMApArrBack" Runat="server" SoftkeyLabel="Back">Back</mobile:Command>
    </mobile:Form>
    <mobile:Form ID="frmCPMApArrRemk" Runat="server" OnActivate="frmCPMApArrRemk_Activate" OnInit="frmCPMApArrRemk_Init">
        <uc3:MWUBagRemk ID="mwuBagRemk1" runat="server" /></mobile:Form>
</body>
</html>
