
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Page  Inherits="Login" Language="C#" CodeFile="Login.aspx.cs" EnableSessionState="True" %>




<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
<mobile:Form id="formA" runat="server" Paginate="True"  >
<mobile:Label runat="server" id="heading" Alignment="Center" EnableViewState="False">SATS iTrek</mobile:Label> 
<mobile:Label runat="server" id="lblstaffid" EnableViewState="False">Login Staff ID</mobile:Label> 
<mobile:TextBox id="txtbxstaffid" runat="Server"/> 
<mobile:Label runat="server" id="lblpword" EnableViewState="False">Enter password</mobile:Label> 
<mobile:TextBox id="txtbxpword" runat="Server" Password="True" />
<mobile:Command runat="Server" OnClick="Login_Click" SoftkeyLabel="Go" ID="cmdLogin">Go</mobile:Command>
<mobile:Label id="message" runat="server" EnableViewState="False"/>
</mobile:Form>

</body>
</html>