//#define UseTestData
//#define Production
using System;
using System.Xml;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using System.Web.Mobile;
using iTrekSessionMgr;
using iTrekXML;
using System.IO;
using UFIS.Wap.ITrek.WebMgr;
using UFIS.ITrekLib.Ctrl;
using UFIS.ITrekLib.DS;

public partial class Login : System.Web.UI.MobileControls.MobilePage
{

	iTXMLPathscl iTPaths = new iTXMLPathscl();
	iTXMLcl iTXML = new iTXMLcl();
	SessionManager sessMan = new SessionManager();
    CtrlSessionMgr ctrlSessMan = new CtrlSessionMgr();
	string SID = "";
	string sessionid = "";


	protected void Page_Load(object sender, EventArgs e)
	{
		try
		{
			if (!IsPostBack)
			{
				if (Session.Count > 0)
				{
					MobileFormsAuthentication.RedirectFromLoginPage("login", false);
				}
			}
			else
			{
			}
			string ver = UFIS.ITrekLib.Config.ITrekConfig.GetVersion();
			
			//lblVer.Text = "Version " + ver + ". " + UFIS.ITrekLib.Config.ITrekConfig.GetAppEnvString();
			heading.Text = "SATS iTrek Ver " + ver + ". " + UFIS.ITrekLib.Config.ITrekConfig.GetAppEnvString();
		}
		catch (Exception LoginEx)
		{
			iTrekUtils.iTUtils.LogToEventLog("On PageLoad of Login" + LoginEx.Message, "iTrekError");
		}
	}


	protected void Login_Click(Object sender, EventArgs e)
	{
		try
		{
#if UseTestData

        sessionid = iTPaths.GetTestSessionId();
        Session["sessionid"] = sessionid;
        SID = iTPaths.GetTestSID();

#else
			try
			{
				sessMan.DeleteSessionDirectory(Session.SessionID);
			}
			catch (Exception ex)
			{
				// iTrekUtils.iTUtils.LogToEventLog("On Deleting" + loginEx.Message, "iTrekError");
			}

			sessionid = Page.Session.SessionID;

			Session["sessionid"] = sessionid;
			//Device sends a Request.Headers["x-up-subno"];
			//when appl live
			SID = Request.Headers["x-up-subno"].ToString();
			SID = SID.Replace("_WAPGW.grid.net.sg", "");
#endif
		}
		catch (Exception loginEx)
		{
			iTrekUtils.iTUtils.LogToEventLog("On Getting Session Id and SID" + loginEx.Message, "iTrekError");
		}

	
//#if Production
//        if (txtbxstaffid.Text.Length == 0 || txtbxpword.Text.Length == 0)
//#else
		if (txtbxstaffid.Text.Length == 0 || txtbxpword.Text.Length == 0)
//#endif
		{
			message.Text = "Staff ID does not exist.  Please try again or contact supervisor or administrator";
			message.Visible = true;
		}
		//else if (iTXML.PENOExist("00" + txtbxstaffid.Text))
		//{
		//    message.Text = "Staff ID currently in use. Please try again or contact supervisor or administrator";
		//    message.Visible = true;
		//}
		else
		{
			txtbxstaffid.Text = "00" + txtbxstaffid.Text.Trim();
			//Session["staffid"] = txtbxstaffid.Text;
			MgrSess.SetUser(Session, txtbxstaffid.Text);
			txtbxpword.Text = txtbxpword.Text.Trim();
			//if (UFIS.ITrekLib.Config.ITrekConfig.GetAppEnv() == UFIS.ITrekLib.Config.EnumEnv.Prod)
			//{
			//    //#if Production
			//    txtbxpword.Text = txtbxpword.Text.Trim();
			//}
			//else
			//{
			//    //#else
			//    txtbxpword.Text = "00" + txtbxpword.Text.Trim();
			//    //#endif
			//}
			//TODO: Configure DateTime.Now?
			
            bool isValidSession = false;

            try
            {
                sessMan = new SessionManager(SID, txtbxstaffid.Text, DateTime.Now, txtbxpword.Text);
                sessMan.CreateSessionDirectory(sessionid);
                isValidSession = sessMan.IsValidSessionWithPassword(sessionid);
            }
            catch (Exception sessEx)
            {
                iTrekUtils.iTUtils.LogToEventLog("On Validating Session" + sessEx.Message, "iTrekError");
            }

			if (isValidSession)
			{
				//when login use Session Manager constructor that checks time and 
				//also checks device and staffid s
				MgrSec.SetAuthenticateUser(Session, txtbxstaffid.Text);
				//string jobCategory = sessMan.GetOrganisationCodeFromSessionDir(Session["sessionid"].ToString());
                string jobCategory = ctrlSessMan.GetOrganisationCodeFromSessionDir(Session["sessionid"].ToString());
				Session["ORGCODE"] = jobCategory;

				//  iTXML.WriteStaffSessionandDeviceIDXmlToDeviceIDFile(sessionid, SID, txtbxstaffid.Text,jobCategory);
				string changeStatus = "";

				Session["loginTime"] = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");

				Session["staffName"] = sessMan.GetStaffNameFromAuthFileInSessionDir(Session["sessionid"].ToString());
				CtrlCurUser.CreateCurUser(sessionid, "M", SID, txtbxstaffid.Text, Session["loginTime"].ToString(), jobCategory, Session["staffName"].ToString());
				iTrekUtils.iTUtils.LogToEventLog("DeviceiD=" + SID + ";Staffid=" + txtbxstaffid.Text + ";SessionId=" + sessionid + ";Org=" + jobCategory, "iTrekDevice");

                if (string.IsNullOrEmpty(jobCategory))
                {
                    txtbxstaffid.Text = string.Empty;
                    txtbxpword.Text = string.Empty;
                    message.Text = "Undefined Organisation Code!";
                }
                else
                {
                    if (jobCategory.Substring(0, 1) == "A")
                    {
                        //XmlDocument xmldoc = sessMan.GetFlightsByStaffID(txtbxstaffid.Text, iTPaths.GetJobsFilePath(), iTPaths.GetFlightsFilePath(), out changeStatus);
                        //sessMan.CreateXMLFileInSessionDirectory(sessionid, xmldoc, "flights.xml");
                        Session["ClosedFlight"] = "0";
                        RedirectToMobilePage("Menu.aspx");
                    }
                    else if (jobCategory.Substring(0, 1) == "B")
                    {
                        RedirectToMobilePage("Baggage.aspx");
                    }
                }
				//MobileFormsAuthentication.RedirectFromLoginPage(txtbxstaffid.Text, false);
			}
			else
			{
				message.Visible = true;
				txtbxstaffid.Text = "";

				if (sessMan.geterrMsgFromMQ != "success")
				{
					//lblstaffid.Visible = true;
					//txtbxstaffid.Visible = true;
					//cmdLogin.Visible = true;
					//message.Visible = true;
					//txtbxstaffid.Text = "";
					message.Text = "Sorry,we are unable to process your request.Please try again after some time";
				}
				else
				{
					//lblstaffid.Visible = true;
					//txtbxstaffid.Visible = true;
					//cmdLogin.Visible = true;
					//message.Visible = true;
					//txtbxstaffid.Text = "";
					message.Text = "Staff ID does not exist.  Please try again or contact supervisor or administrator.";
				}
			}
		}
	}

	protected override void OnViewStateExpire(EventArgs e)
	{
		//ActiveForm = frmLoginError;
		//lblLoginErrorMsg.Text = "Timeout error occured. Please try again.";

		RedirectToMobilePage("Login.aspx");
	}

	protected void cmdLoginError_Click(object sender, EventArgs e)
	{
		ActiveForm = formA;
	}
}