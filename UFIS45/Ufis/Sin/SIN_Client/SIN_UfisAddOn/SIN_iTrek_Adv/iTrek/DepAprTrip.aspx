<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DepAprTrip.aspx.cs" Inherits="DepAprTrip" EnableSessionState="True"%>

<%@ Register Src="MWU_CompanyHeading.ascx" TagName="MWU_CompanyHeading" TagPrefix="uc1" %>
<%@ Register Src="MWUTiming.ascx" TagName="MWUTiming" TagPrefix="uc2" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="frmContainerList" runat="server"><uc1:MWU_CompanyHeading ID="MWU_CompanyHeading1"
        runat="server" /><mobile:Label
            ID="Label2" Runat="server">
        </mobile:Label> <br /><mobile:Label ID="lblErrMsg" Runat="server" Visible="False">
        </mobile:Label><br /><mobile:SelectionList ID="chkContainerList" Runat="server" SelectType="CheckBox">
        </mobile:SelectionList> <mobile:TextView ID="txtvUld" Runat="server">
        </mobile:TextView><br /><mobile:Command ID="Command1" Runat="server" OnClick="Command1_Click">View Remarks</mobile:Command> <mobile:Command
            ID="cmdSendTrip" Runat="server" OnClick="cmdSendTrip_Click">Received</mobile:Command> <mobile:Command ID="cmdBack1"
                Runat="server" OnClick="cmdBack1_Click">Back</mobile:Command></mobile:Form>
    <mobile:Form ID="frmRemarkList" Runat="server"><uc1:MWU_CompanyHeading ID="MWU_CompanyHeading2"
        runat="server"  />&nbsp; <mobile:Label
            ID="Label3" Runat="server" Alignment="Center">View Remarks
        </mobile:Label> <mobile:List ID="lstRemarks" Runat="server" OnItemCommand="lstRemarks_ItemCommand">
        </mobile:List> <mobile:Command ID="cmdBack" Runat="server" OnClick="cmdBack_Click">Back</mobile:Command></mobile:Form>
    <mobile:Form ID="frmViewRemarks" Runat="server"><uc1:MWU_CompanyHeading ID="MWU_CompanyHeading3" runat="server" />&nbsp; <mobile:TextView ID="txtVRemarks" Runat="server">
        </mobile:TextView> <mobile:Command ID="cmdBack3" Runat="server" OnClick="cmdBack3_Click">Back</mobile:Command></mobile:Form>
    <mobile:Form ID="frmAOTiming" Runat="server"><uc1:MWU_CompanyHeading ID="MWU_CompanyHeading4"
        runat="server" />
        <mobile:Label ID="lblHidden" Runat="server" Visible="False">
        </mobile:Label>
        <br />
        <mobile:Label ID="lblSelectedOption" Runat="server">
        </mobile:Label> <mobile:Label ID="lblTimingAlertMsg" Runat="server" EnableViewState="False" Visible="False" BreakAfter="True" >
        </mobile:Label><uc2:MWUTiming ID="MWUTiming1" runat="server" /> <mobile:Command ID="Command2" Runat="server" OnClick="Command2_Click" >Back</mobile:Command></mobile:Form>
</body>
</html>
