using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using UFIS.Wap.ITrek.WebMgr;

public partial class WAP_MWU_CompanyHeading : System.Web.UI.MobileControls.MobileUserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    public void ShowFlightInfoHeading()
    {
       // ShowFlightInfoHeading(MgrSess.GetFlightInfo(Session));
        try
        {
            if (Session["ORGCODE"].ToString().Substring(0, 1) == "A")
            {
                ShowFlightInfoHeading(MgrSess.GetFlight(Session).DISPLAYFLIGHTSMENU);
            }
            else
            {

                ShowFlightInfoHeading(MgrSess.GetFlight(Session).DISPLAYBOFLIGHTMENU);
            }
        }
        catch (Exception ex)
        {
           
        
        
        }
        
    }

    public string GetFlightInfoHeading()
    {

        return MgrSess.GetFlight(Session).DISPLAYFLIGHTSMENU;
    
    
    }

    public void ShowFlightInfoHeading(string hdr)
    {
        lblHead2FlightInfo.Text = hdr;
        lblHead2FlightInfo.Visible = true;
    }
}
