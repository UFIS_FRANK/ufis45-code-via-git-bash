<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Test.aspx.cs" Inherits="Test"  EnableSessionState="True"%>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>

<html xmlns="http://www.w3.org/1999/xhtml" >
<body>
    <mobile:Form id="Form1" runat="server">
        <mobile:List ID="List1" Runat="server" OnItemCommand="List1_ItemCommand">
            <Item Text="Test1" Value="Test1" />
            <Item Text="Test2" Value="Test2" />
            <Item Text="Test3" Value="Test3" />
        </mobile:List>

    </mobile:Form>
    <mobile:Form ID="Form2" Runat="server">
        <mobile:Label ID="Label1" Runat="server">Test1</mobile:Label>
    </mobile:Form>
    <mobile:Form ID="Form3" Runat="server">
        <mobile:Label ID="Label2" Runat="server">Test2</mobile:Label>
    </mobile:Form>
    <mobile:Form ID="Form4" Runat="server">
        <mobile:Label ID="Label3" Runat="server">Test3</mobile:Label>
    </mobile:Form>
</body>
</html>
