using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Drawing;
using System.Security.Permissions;
using System.Collections.Specialized;
using System.Text.RegularExpressions;

namespace UFIS.UtilWeb
{
    /// <summary>
    /// Declares the class for the child controls to include in the control collection.
    /// </summary>
    [AspNetHostingPermission(SecurityAction.Demand,
        Level = AspNetHostingPermissionLevel.Minimal)]
    public class MyTableCell : TableCell, INamingContainer { };
    [AspNetHostingPermission(SecurityAction.Demand,
        Level = AspNetHostingPermissionLevel.Minimal)]
    public class Cell
    {
        string _id;
        string _value;
        Color _backColor;

        public string CellID
        {
            get
            { return _id; }
            set
            { _id = value; }
        }

        public string Text
        {
            get
            { return _value; }
            set
            { _value = value; }
        }

        public Color BackColor
        {
            get
            { return _backColor; }
            set
            { _backColor = value; }
        }
    };
    /// <summary>
    /// Class name: TextBoxControlBuilder
    /// Used in building of UfisTextBox 
    /// </summary>
    [AspNetHostingPermission(SecurityAction.Demand,
   Level = AspNetHostingPermissionLevel.Minimal)]
    public class TextBoxControlBuilder : ControlBuilder
    {
        public override Type GetChildControlType(string tagName, IDictionary attribs)
        {
            // Allows TableRow without "runat=server" attribute to be added to the collection.
            if (String.Compare(tagName, "cell", true) == 0)
                return typeof(Cell);
            return null;
        }

        public override void AppendLiteralString(string s)
        {
            // Ignores literals between rows.
        }

    }
    /// <summary>
    /// Class name: UfisTextBox
    /// Processes the element declarations within a control declaration. 
    /// This builds the actual control.
    /// 
    /// Usage (with text):<prog:MyTextBox Columns="1" Rows="1" Width="600px" runat="server" Text="some test" ID="myctrl2" ></prog:MyTextBox>
    /// myctrl2.Text = "";
    /// 
    /// Usage (with DataSource):<prog:MyTextBox Columns="1" Rows="1" Width="600px" runat="server"  ID="myctrl2" ></prog:MyTextBox>
    /// myctrl2.DataSource = dataView1;
    /// myctrl2.DataValueField = "ProductName";
    /// myctrl2.DataBind();
    /// </summary>
    [AspNetHostingPermission(SecurityAction.Demand,
        Level = AspNetHostingPermissionLevel.Minimal)]
    [ControlBuilderAttribute(typeof(TextBoxControlBuilder))]
    public class UfisTextBox : Control, INamingContainer, ITextControl, IStyleSheet, IPostBackDataHandler
    {
        /// <summary>
        /// The container in which one row one column table or multiple row multiple columns
        /// that depends on Text or DataSource.
        /// If Text is using , the table will have 1 row and 1 column.
        /// If Datasource is using, the table will have multiple rows and columns according to the datasource.
        /// </summary>
        Table _table;

        /// <summary>
        /// the width of the control
        /// </summary>
        private Unit _width;

        /// <summary>
        /// the no. of rows to be included in the control
        /// </summary>
        private int _rows = 1;

        /// <summary>
        /// the no. of columns to be used in creation of the table
        /// </summary>
        private int _columns = 1;

        /// <summary>
        /// the collection of cells that are added to the table
        /// </summary>
        Hashtable _cellObjects = new Hashtable();

        /// <summary>
        /// If the Text property is using the _dataSource will be null
        /// </summary>
        private string _text = "";

        /// <summary>
        /// the style to be rendered to the table
        /// </summary>
        private string _style = "";

        /// <summary>
        /// to hold DataValueField,i.e column name if the datasource property is used
        /// </summary>
        private string _datavalue = "";

        /// <summary>
        /// storing the data if the DataSource property is used
        /// When Text property is used , the _dataSource will be null.
        /// </summary>
        IList _dataSource;

        /// <summary>
        /// Storing the ID
        /// </summary>
        string masterID = "textbox";

        public IList DataSource
        {
            get
            {
                if (_dataSource == null)
                    _dataSource = (IList)ViewState[masterID + "_dtsc"];
                return _dataSource;
            }
            set
            {
                if ((value is IList) || (value == null))
                {
                    _text = "";
                    _dataSource = value;
                    ViewState[masterID + "_dtsc"] = value;
                    _rows = _dataSource.Count;
                    Columns = 1;

                }
                else
                {
                    throw new ArgumentException();
                }
            }
        }

        public string Text
        {
            get
            {
                if (_text == null)
                    _text = (string)ViewState[masterID + "_text"];
                return _text;
            }
            set
            {
                if (value != null)
                {
                    _text = value;
                    ViewState[masterID + "_text"] = value;
                    _rows = 1;
                    _columns = 1;
                    _dataSource = null;
                }
                else
                {
                    _text = "";
                }
            }
        }
        public int Rows
        {
            get
            { return _rows; }
            set
            { _rows = value; }
        }
        public string DataValueField
        {
            get
            { return _datavalue; }
            set
            { _datavalue = value; }
        }

        public String Style
        {
            set
            { _style = value; }
        }

        public Unit Width
        {
            get
            { return _width; }
            set
            { _width = value; }
        }

        public int Columns
        {
            get
            { return _columns; }
            set
            { _columns = value; }
        }

        public void CreateStyleRule(Style style, IUrlResolutionService urlResolver, string selector)
        {

        }
        public void RegisterStyle(Style style, IUrlResolutionService urlResolver)
        {

        }
        protected void createNewRow(int rowNumber)
        {

            // Creates a row and adds it to the table.
            TableRow row = new TableRow();
            _table.Rows.Add(row);

            // Creates a cell that contains text.
            for (int y = 0; y < Columns; y++)
                appendCell(row, rowNumber, y);

        }
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            this.masterID = this.ID;
        }
        /// <summary>
        /// Called whenever a cell is added 
        /// </summary>
        /// <param name="row">the row object to be inserted</param>
        /// <param name="rowNumber">the row no to be inserted</param>
        /// <param name="cellNumber">the cell no to be inserted</param>
        protected void appendCell(TableRow row, int rowNumber, int cellNumber)
        {
            TableCell cell = new TableCell();

            string tmpText = "";
             if (_dataSource == null)
             {
                 tmpText = this._text;
             }
             else
             {
                 if (_dataSource[rowNumber] is System.Data.DataRowView)
                {
                    if (_datavalue != null && _datavalue.Length > 0)
                    {
                        string tmp = ((System.Data.DataRowView)_dataSource[rowNumber])[_datavalue].ToString();
                        tmpText = tmp;
                    }
                }
                else
                 {
                     string tmp = (string)_dataSource[rowNumber];
                     tmpText = tmp;
                 }
             }

             cell.Text = HttpUtility.HtmlDecode(StripHtml(tmpText));
            

            row.Cells.Add(cell);
        }
        /// <summary>
        /// Called at runtime when a child object is added to the collection.  
        /// </summary>
        /// <param name="obj"></param>
        protected override void AddParsedSubObject(object obj)
        {
            Cell cell = obj as Cell;
            if (cell != null)
            {
                _cellObjects.Add(cell.CellID, cell);
            }

        }
        /// <summary>
        /// Function name: OnPreRender.
        /// Carries out changes affecting the control state and renders the resulting UI.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreRender(EventArgs e)
        {
            
            // Increases the number of rows if needed
            while (_table.Rows.Count < Rows)
            {
                createNewRow(_table.Rows.Count);
            }

            // Checks that each row has the correct number of columns.
            for (int i = 0; i < _table.Rows.Count; i++)
            {
                while (_table.Rows[i].Cells.Count < Columns)
                {
                    appendCell(_table.Rows[i], i, _table.Rows[i].Cells.Count);
                }

                while (_table.Rows[i].Cells.Count > Columns)
                {
                    _table.Rows[i].Cells.RemoveAt(_table.Rows[i].Cells.Count - 1);
                }
            }
        }
        /// <summary>
        /// Function name: CreateChildControls.
        /// Adds the Table and the text control to the control collection.
        /// </summary>
        protected override void CreateChildControls()
        {
            _table = new Table();
            _table.Width = _width;

            _table.CssClass = _style;
            Controls.Add(_table);
        }

        #region IPostBackDataHandler Members
        public event EventHandler TextChanged;

        bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection)
        {
            return this.LoadPostData(postDataKey, postCollection);
        }
        public virtual bool LoadPostData(string postDataKey,
         NameValueCollection postCollection)
        {

            String presentValue = Text;
            String postedValue = postCollection[postDataKey];

            if (presentValue == null || !presentValue.Equals(postedValue))
            {
                Text = postedValue;
                return true;
            }

            return false;
        }
        public virtual void RaisePostDataChangedEvent()
        {
            OnTextChanged(EventArgs.Empty);
        }


        protected virtual void OnTextChanged(EventArgs e)
        {
            if (TextChanged != null)
                TextChanged(this, e);
        }

        #endregion

        /// <summary>
        /// To be enable to know carriage return 
        /// and replace with <br/> 
        /// </summary>
        //private static Regex _CarriageReturnEx = new Regex("[\r\n]|[\n]", RegexOptions.Compiled);
        private static Regex _CarriageReturnEx = new Regex("(\r\n\\s\\s)|(\n\\s\\s)", RegexOptions.Compiled);
        private static Regex _exSpace = new Regex("(\\s)", RegexOptions.Compiled);
        private static Regex _CarriageReturnEx2 = new Regex("(\r\n)|(\n)", RegexOptions.Compiled);
        public static string StripHtml(string val)
        {
            string s = _CarriageReturnEx.Replace(val, "<br/>&nbsp;&nbsp;");
            //return s;
            return _CarriageReturnEx2.Replace(s, "<br/>");
        }

        private string BreakLongWord(string text)
        {
            char[] spilter = { ' ' };
            string[] words = text.Split(spilter);
            System.Text.StringBuilder strDesc = new System.Text.StringBuilder("");
            int iLength = Convert.ToInt16(this._width.Value);
            foreach (string word in words)
            {
                string newword = "";
                if (word.Length > this.Width.Value )
                {


                    int parts = word.Length / iLength;
                    int remainder = word.Length % iLength;
                    int counter = parts;
                    if (remainder > 0)
                        counter++;

                    for (int i = 1; i <= counter; i++)
                    {
                        if (i == counter)
                            newword = newword + word.Substring((i - 1) * iLength);
                        else
                            newword = newword + word.Substring((i - 1) * iLength, iLength) + " ";
                    }

                }
                else
                    newword = word;
                strDesc.Append(newword + "<br/>");
            }

            return strDesc.ToString().Substring(0, strDesc.ToString().Length - 2);
        } 

    }
}