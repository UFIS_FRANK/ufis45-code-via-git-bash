VERSION 5.00
Begin VB.Form MyPictures 
   Caption         =   "Form1"
   ClientHeight    =   6900
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   12300
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   6900
   ScaleWidth      =   12300
   StartUpPosition =   3  'Windows Default
   Begin VB.Image imgCtl 
      Height          =   480
      Index           =   2
      Left            =   2850
      Picture         =   "MyPictures.frx":0000
      Top             =   300
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image imgCtl 
      Height          =   480
      Index           =   1
      Left            =   2460
      Picture         =   "MyPictures.frx":08CA
      Top             =   300
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image imgCtl 
      Height          =   480
      Index           =   0
      Left            =   2070
      Picture         =   "MyPictures.frx":1194
      Top             =   300
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image SmallPlain 
      Height          =   195
      Index           =   0
      Left            =   60
      Picture         =   "MyPictures.frx":1A5E
      ToolTipText     =   "Tooltip 0"
      Top             =   330
      Width           =   195
   End
   Begin VB.Image SmallPlain 
      Height          =   195
      Index           =   1
      Left            =   300
      Picture         =   "MyPictures.frx":1CA8
      ToolTipText     =   "Tooltip 0"
      Top             =   330
      Width           =   195
   End
   Begin VB.Image SmallPlain 
      Height          =   195
      Index           =   2
      Left            =   540
      Picture         =   "MyPictures.frx":1EF2
      ToolTipText     =   "Tooltip 0"
      Top             =   330
      Width           =   195
   End
   Begin VB.Image SmallPlain 
      Height          =   195
      Index           =   3
      Left            =   780
      Picture         =   "MyPictures.frx":213C
      ToolTipText     =   "Tooltip 0"
      Top             =   330
      Width           =   195
   End
   Begin VB.Image SmallPlain 
      Height          =   195
      Index           =   4
      Left            =   1020
      Picture         =   "MyPictures.frx":2386
      ToolTipText     =   "Tooltip 0"
      Top             =   330
      Width           =   195
   End
   Begin VB.Image SmallPlain 
      Height          =   195
      Index           =   5
      Left            =   1260
      Picture         =   "MyPictures.frx":25D0
      ToolTipText     =   "Tooltip 0"
      Top             =   330
      Width           =   195
   End
   Begin VB.Image SmallPlain 
      Height          =   195
      Index           =   6
      Left            =   1500
      Picture         =   "MyPictures.frx":281A
      ToolTipText     =   "Tooltip 0"
      Top             =   330
      Width           =   195
   End
   Begin VB.Image SmallPlain 
      Height          =   195
      Index           =   7
      Left            =   1740
      Picture         =   "MyPictures.frx":2A64
      ToolTipText     =   "Tooltip 0"
      Top             =   330
      Width           =   195
   End
   Begin VB.Image SmallDark 
      Height          =   195
      Index           =   0
      Left            =   60
      Picture         =   "MyPictures.frx":2CAE
      ToolTipText     =   "Tooltip 0"
      Top             =   570
      Width           =   195
   End
   Begin VB.Image SmallDark 
      Height          =   195
      Index           =   1
      Left            =   300
      Picture         =   "MyPictures.frx":2EF8
      ToolTipText     =   "Tooltip 0"
      Top             =   570
      Width           =   195
   End
   Begin VB.Image SmallDark 
      Height          =   195
      Index           =   2
      Left            =   540
      Picture         =   "MyPictures.frx":3142
      ToolTipText     =   "Tooltip 0"
      Top             =   570
      Width           =   195
   End
   Begin VB.Image SmallDark 
      Height          =   195
      Index           =   3
      Left            =   780
      Picture         =   "MyPictures.frx":338C
      ToolTipText     =   "Tooltip 0"
      Top             =   570
      Width           =   195
   End
   Begin VB.Image SmallDark 
      Height          =   195
      Index           =   4
      Left            =   1020
      Picture         =   "MyPictures.frx":35D6
      ToolTipText     =   "Tooltip 0"
      Top             =   570
      Width           =   195
   End
   Begin VB.Image SmallDark 
      Height          =   195
      Index           =   5
      Left            =   1260
      Picture         =   "MyPictures.frx":3820
      ToolTipText     =   "Tooltip 0"
      Top             =   570
      Width           =   195
   End
   Begin VB.Image SmallDark 
      Height          =   195
      Index           =   6
      Left            =   1500
      Picture         =   "MyPictures.frx":3A6A
      ToolTipText     =   "Tooltip 0"
      Top             =   570
      Width           =   195
   End
   Begin VB.Image SmallDark 
      Height          =   195
      Index           =   7
      Left            =   1740
      Picture         =   "MyPictures.frx":3CB4
      ToolTipText     =   "Tooltip 0"
      Top             =   570
      Width           =   195
   End
   Begin VB.Image picBcSync 
      Height          =   480
      Index           =   2
      Left            =   5580
      Picture         =   "MyPictures.frx":3EFE
      Top             =   30
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picBcSync 
      Height          =   480
      Index           =   1
      Left            =   5070
      Picture         =   "MyPictures.frx":47C8
      Top             =   30
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image picBcSync 
      Height          =   480
      Index           =   0
      Left            =   4560
      Picture         =   "MyPictures.frx":5092
      Top             =   30
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image AmpelSmall 
      Height          =   240
      Index           =   11
      Left            =   3000
      Picture         =   "MyPictures.frx":595C
      Top             =   30
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image AmpelSmall 
      Height          =   240
      Index           =   10
      Left            =   2730
      Picture         =   "MyPictures.frx":5AA6
      Top             =   30
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image AmpelSmall 
      Height          =   240
      Index           =   9
      Left            =   2460
      Picture         =   "MyPictures.frx":5BF0
      Top             =   30
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image AmpelSmall 
      Height          =   240
      Index           =   8
      Left            =   2190
      Picture         =   "MyPictures.frx":5D3A
      Top             =   30
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image AmpelSmall 
      Height          =   240
      Index           =   7
      Left            =   1920
      Picture         =   "MyPictures.frx":5E84
      Top             =   30
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image AmpelSmall 
      Height          =   240
      Index           =   6
      Left            =   1650
      Picture         =   "MyPictures.frx":5FCE
      Top             =   30
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image AmpelSmall 
      Height          =   240
      Index           =   5
      Left            =   1380
      Picture         =   "MyPictures.frx":6118
      Top             =   30
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image AmpelSmall 
      Height          =   240
      Index           =   4
      Left            =   1110
      Picture         =   "MyPictures.frx":6262
      Top             =   30
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image AmpelSmall 
      Height          =   240
      Index           =   3
      Left            =   840
      Picture         =   "MyPictures.frx":63AC
      Top             =   30
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image AmpelSmall 
      Height          =   240
      Index           =   2
      Left            =   570
      Picture         =   "MyPictures.frx":64F6
      Top             =   30
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image AmpelSmall 
      Height          =   240
      Index           =   1
      Left            =   300
      Picture         =   "MyPictures.frx":6640
      Top             =   30
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image AmpelSmall 
      Height          =   240
      Index           =   0
      Left            =   30
      Picture         =   "MyPictures.frx":678A
      Top             =   30
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image LoginMask 
      Height          =   3450
      Index           =   1
      Left            =   6150
      Picture         =   "MyPictures.frx":68D4
      Top             =   2100
      Width           =   6105
   End
   Begin VB.Image LoginMask 
      Height          =   3450
      Index           =   0
      Left            =   30
      Picture         =   "MyPictures.frx":4B4C6
      Top             =   2100
      Width           =   6105
   End
End
Attribute VB_Name = "MyPictures"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

