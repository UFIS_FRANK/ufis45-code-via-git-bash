VERSION 5.00
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.OCX"
Object = "{7DA325C2-D4A4-11D3-9B36-020128B0A0FF}#1.0#0"; "UfisBroadcasts.ocx"
Object = "{A2F31E92-C74F-11D3-A251-00500437F607}#1.0#0"; "UfisCom.ocx"
Begin VB.Form RemoteStart 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Staff Monitor Remote Control"
   ClientHeight    =   5580
   ClientLeft      =   45
   ClientTop       =   210
   ClientWidth     =   8100
   Icon            =   "RemoteStart.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5580
   ScaleWidth      =   8100
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer tmrStart 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   60
      Top             =   1410
   End
   Begin VB.Timer tmrFocusX 
      Enabled         =   0   'False
      Interval        =   4000
      Left            =   60
      Top             =   960
   End
   Begin VB.PictureBox MainPanel 
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   0  'None
      Height          =   1365
      Index           =   0
      Left            =   1110
      ScaleHeight     =   1365
      ScaleWidth      =   5055
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   4050
      Visible         =   0   'False
      Width           =   5055
      Begin VB.PictureBox CheckNetLedPanel 
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         Height          =   375
         Index           =   0
         Left            =   6390
         ScaleHeight     =   375
         ScaleWidth      =   555
         TabIndex        =   47
         TabStop         =   0   'False
         Top             =   2850
         Width           =   555
         Begin VB.Image CheckNetLed 
            Height          =   195
            Index           =   1
            Left            =   210
            Picture         =   "RemoteStart.frx":0442
            Top             =   0
            Width           =   195
         End
         Begin VB.Image CheckNetLed 
            Height          =   195
            Index           =   0
            Left            =   0
            Picture         =   "RemoteStart.frx":068C
            Top             =   0
            Width           =   195
         End
      End
      Begin VB.Timer CheckNetTimer 
         Enabled         =   0   'False
         Index           =   0
         Interval        =   1000
         Left            =   6360
         Top             =   2280
      End
      Begin MSWinsockLib.Winsock CheckNetPort 
         Index           =   0
         Left            =   6360
         Top             =   1710
         _ExtentX        =   741
         _ExtentY        =   741
         _Version        =   393216
      End
      Begin UFISBROADCASTSLib.UfisBroadcasts UfisBroadcasts 
         Left            =   6300
         Top             =   900
         _Version        =   65536
         _ExtentX        =   1402
         _ExtentY        =   1085
         _StockProps     =   0
      End
      Begin UFISCOMLib.UfisCom UfisCom 
         Left            =   6300
         Top             =   150
         _Version        =   65536
         _ExtentX        =   1455
         _ExtentY        =   1138
         _StockProps     =   0
      End
      Begin VB.Label TopLabel 
         Alignment       =   2  'Center
         Caption         =   "Connecting"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   0
         TabIndex        =   46
         Top             =   0
         Width           =   2895
      End
      Begin VB.Label MainShadow 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "System Environment Check"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   4
         Left            =   7230
         TabIndex        =   25
         Top             =   1680
         Visible         =   0   'False
         Width           =   2925
      End
      Begin VB.Label MainShadow 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "System Environment Check"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   3
         Left            =   7230
         TabIndex        =   24
         Top             =   1440
         Width           =   2925
      End
      Begin VB.Label MainShadow 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "System Environment Check"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   2
         Left            =   7230
         TabIndex        =   23
         Top             =   1230
         Width           =   2925
      End
      Begin VB.Label AnyShadow 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Route to Host  ............."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Index           =   7
         Left            =   7230
         TabIndex        =   22
         Top             =   3600
         Width           =   2160
      End
      Begin VB.Label AnyShadow 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Route to Host  ............."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Index           =   6
         Left            =   7230
         TabIndex        =   21
         Top             =   3360
         Width           =   2160
      End
      Begin VB.Label AnyShadow 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Route to Host  ............."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Index           =   5
         Left            =   7230
         TabIndex        =   20
         Top             =   3150
         Width           =   2160
      End
      Begin VB.Label AnyShadow 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Route to Host  ............."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Index           =   4
         Left            =   7230
         TabIndex        =   19
         Top             =   2940
         Width           =   2160
      End
      Begin VB.Label AnyShadow 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Route to Host  ............."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Index           =   3
         Left            =   7230
         TabIndex        =   18
         Top             =   2730
         Width           =   2160
      End
      Begin VB.Label AnyShadow 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Route to Host  ............."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Index           =   2
         Left            =   7230
         TabIndex        =   17
         Top             =   2520
         Width           =   2160
      End
      Begin VB.Label AnyShadow 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Route to Host  ............."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Index           =   1
         Left            =   7230
         TabIndex        =   16
         Top             =   2310
         Width           =   2160
      End
      Begin VB.Label AnyShadow 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Route to Host  ............."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   240
         Index           =   0
         Left            =   7245
         TabIndex        =   15
         Top             =   2085
         Width           =   2160
      End
      Begin VB.Label AnyLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Route to Host 1  ............."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   1
         Left            =   420
         TabIndex        =   14
         Top             =   3600
         Visible         =   0   'False
         Width           =   2325
      End
      Begin VB.Image AnyIcon 
         Height          =   480
         Index           =   1
         Left            =   2730
         Picture         =   "RemoteStart.frx":08D6
         Top             =   3510
         Visible         =   0   'False
         Width           =   480
      End
      Begin VB.Image AnyIcon 
         Height          =   480
         Index           =   7
         Left            =   2730
         Picture         =   "RemoteStart.frx":11A0
         Top             =   2850
         Width           =   480
      End
      Begin VB.Image AnyIcon 
         Height          =   480
         Index           =   6
         Left            =   2730
         Picture         =   "RemoteStart.frx":1A6A
         Top             =   2610
         Width           =   480
      End
      Begin VB.Image AnyIcon 
         Height          =   480
         Index           =   5
         Left            =   2730
         Picture         =   "RemoteStart.frx":2334
         Top             =   2370
         Width           =   480
      End
      Begin VB.Image AnyIcon 
         Height          =   480
         Index           =   4
         Left            =   2730
         Picture         =   "RemoteStart.frx":2BFE
         Top             =   2130
         Width           =   480
      End
      Begin VB.Image AnyIcon 
         Height          =   480
         Index           =   3
         Left            =   2730
         Picture         =   "RemoteStart.frx":34C8
         Top             =   1530
         Width           =   480
      End
      Begin VB.Image AnyIcon 
         Height          =   480
         Index           =   2
         Left            =   2730
         Picture         =   "RemoteStart.frx":3D92
         Top             =   1290
         Width           =   480
      End
      Begin VB.Label AnyLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Confirm UDP ..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   7
         Left            =   540
         TabIndex        =   13
         Top             =   3000
         Width           =   1455
      End
      Begin VB.Label AnyLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Login Check   ..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   6
         Left            =   540
         TabIndex        =   12
         Top             =   2760
         Width           =   1545
      End
      Begin VB.Label AnyLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Initializing  ..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   5
         Left            =   540
         TabIndex        =   11
         Top             =   2520
         Width           =   1305
      End
      Begin VB.Label AnyLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Connecting ..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   4
         Left            =   540
         TabIndex        =   10
         Top             =   2280
         Width           =   1320
      End
      Begin VB.Label AnyLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Time Service  ..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   3
         Left            =   540
         TabIndex        =   9
         Top             =   1680
         Width           =   1560
      End
      Begin VB.Label AnyLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Attach Spooler  ..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   2
         Left            =   540
         TabIndex        =   8
         Top             =   1440
         Width           =   1710
      End
      Begin VB.Label AnyLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Route to Host  ............."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   0
         Left            =   540
         TabIndex        =   7
         Top             =   870
         Width           =   2160
      End
      Begin VB.Label MainLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "3.) UFIS CEDA Services"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   3
         Left            =   240
         TabIndex        =   6
         Top             =   2040
         Width           =   2505
      End
      Begin VB.Label MainLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "2.) UFIS UDP Messages"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   2
         Left            =   240
         TabIndex        =   5
         Top             =   1200
         Width           =   2505
      End
      Begin VB.Image AnyIcon 
         Height          =   480
         Index           =   0
         Left            =   2730
         Picture         =   "RemoteStart.frx":465C
         Top             =   720
         Width           =   480
      End
      Begin VB.Label MainLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "1.) Network Check"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   270
         Index           =   1
         Left            =   240
         TabIndex        =   3
         Top             =   600
         Width           =   1935
      End
      Begin VB.Label MainShadow 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "System Environment Check"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Index           =   0
         Left            =   7230
         TabIndex        =   2
         Top             =   180
         Width           =   3180
      End
      Begin VB.Label MainLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "System Environment Check"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   285
         Index           =   0
         Left            =   240
         TabIndex        =   1
         Top             =   180
         Width           =   3180
      End
      Begin VB.Label MainShadow 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "1.) Network Check"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   270
         Index           =   1
         Left            =   7245
         TabIndex        =   4
         Top             =   615
         Width           =   1935
      End
      Begin VB.Image BackGrndPic 
         Height          =   570
         Index           =   0
         Left            =   0
         Top             =   0
         Width           =   615
      End
   End
   Begin VB.Timer tmrFlow 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   60
      Top             =   510
   End
   Begin MSWinsockLib.Winsock CdrPort 
      Left            =   60
      Top             =   3150
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin VB.PictureBox MainPanel 
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   0  'None
      Height          =   3660
      Index           =   1
      Left            =   1110
      ScaleHeight     =   3660
      ScaleWidth      =   6105
      TabIndex        =   26
      TabStop         =   0   'False
      Top             =   150
      Visible         =   0   'False
      Width           =   6105
      Begin VB.CheckBox chkTask 
         Caption         =   "Start Remote"
         Height          =   285
         Index           =   2
         Left            =   1590
         Style           =   1  'Graphical
         TabIndex        =   43
         TabStop         =   0   'False
         Top             =   2970
         Width           =   1425
      End
      Begin VB.CheckBox chkTask 
         Caption         =   "Disconnect"
         Height          =   285
         Index           =   1
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   42
         TabStop         =   0   'False
         Top             =   3300
         Width           =   1425
      End
      Begin VB.CheckBox chkTask 
         Caption         =   "Connect Remote"
         Height          =   285
         Index           =   0
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   41
         TabStop         =   0   'False
         Top             =   2970
         Width           =   1425
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Exit"
         Height          =   285
         Left            =   2340
         TabIndex        =   34
         TabStop         =   0   'False
         Top             =   3300
         Width           =   675
      End
      Begin VB.CommandButton btnStartApp 
         Caption         =   "Start Remote"
         Height          =   285
         Left            =   1590
         TabIndex        =   33
         TabStop         =   0   'False
         Top             =   2970
         Width           =   1425
      End
      Begin VB.CommandButton btnConnect 
         Caption         =   "Connect Remote"
         Height          =   285
         Left            =   120
         TabIndex        =   32
         TabStop         =   0   'False
         Top             =   2970
         Width           =   1425
      End
      Begin VB.CommandButton btnDisconnect 
         Caption         =   "Disconnect"
         Height          =   285
         Left            =   120
         TabIndex        =   31
         TabStop         =   0   'False
         Top             =   3300
         Width           =   1425
      End
      Begin VB.CommandButton btnHide 
         Caption         =   "Hide"
         Height          =   285
         Left            =   1590
         TabIndex        =   30
         TabStop         =   0   'False
         Top             =   3300
         Width           =   705
      End
      Begin VB.Label Label11 
         BackColor       =   &H00FFC0C0&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   3090
         TabIndex        =   45
         Top             =   2610
         Width           =   2895
      End
      Begin VB.Label Label10 
         Alignment       =   2  'Center
         BackColor       =   &H00FFC0C0&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   120
         TabIndex        =   44
         Top             =   2610
         Width           =   2895
      End
      Begin VB.Image Image6 
         Height          =   195
         Left            =   3660
         Picture         =   "RemoteStart.frx":4F26
         Top             =   360
         Visible         =   0   'False
         Width           =   195
      End
      Begin VB.Image Image5 
         Height          =   195
         Left            =   2220
         Picture         =   "RemoteStart.frx":5170
         Top             =   360
         Visible         =   0   'False
         Width           =   195
      End
      Begin VB.Image Image4 
         Height          =   195
         Left            =   3870
         Picture         =   "RemoteStart.frx":53BA
         Top             =   360
         Visible         =   0   'False
         Width           =   195
      End
      Begin VB.Image Image3 
         Height          =   195
         Left            =   5730
         Picture         =   "RemoteStart.frx":5604
         Top             =   360
         Visible         =   0   'False
         Width           =   195
      End
      Begin VB.Image Image2 
         Height          =   195
         Left            =   2010
         Picture         =   "RemoteStart.frx":584E
         Top             =   360
         Visible         =   0   'False
         Width           =   195
      End
      Begin VB.Image Image1 
         Height          =   195
         Left            =   180
         Picture         =   "RemoteStart.frx":5A98
         Top             =   360
         Visible         =   0   'False
         Width           =   195
      End
      Begin VB.Label Label9 
         Alignment       =   2  'Center
         BackColor       =   &H000080FF&
         Caption         =   "Label7"
         Height          =   210
         Left            =   4080
         TabIndex        =   40
         Top             =   360
         Visible         =   0   'False
         Width           =   1665
      End
      Begin VB.Label Label8 
         Alignment       =   2  'Center
         BackColor       =   &H000080FF&
         Caption         =   "Label7"
         Height          =   210
         Left            =   390
         TabIndex        =   39
         Top             =   360
         Visible         =   0   'False
         Width           =   1605
      End
      Begin VB.Label Label7 
         Alignment       =   2  'Center
         BackColor       =   &H000080FF&
         Caption         =   "Label7"
         Height          =   210
         Left            =   2190
         TabIndex        =   38
         Top             =   360
         Visible         =   0   'False
         Width           =   1665
      End
      Begin VB.Label Label6 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Update Service"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   3795
         TabIndex        =   37
         Top             =   690
         Width           =   1245
      End
      Begin VB.Label Label5 
         BackColor       =   &H00FFC0C0&
         BorderStyle     =   1  'Fixed Single
         Height          =   300
         Left            =   120
         TabIndex        =   36
         Top             =   300
         Width           =   5865
      End
      Begin VB.Label Label4 
         BackColor       =   &H00FFC0C0&
         BorderStyle     =   1  'Fixed Single
         Height          =   1635
         Left            =   3090
         TabIndex        =   35
         Top             =   930
         Width           =   2895
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Remote Port"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   2580
         TabIndex        =   29
         Top             =   60
         Width           =   1065
      End
      Begin VB.Label Label2 
         BackColor       =   &H00FFC0C0&
         BorderStyle     =   1  'Fixed Single
         Height          =   1635
         Left            =   120
         TabIndex        =   28
         Top             =   930
         Width           =   2895
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Monitoring"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   210
         Left            =   1095
         TabIndex        =   27
         Top             =   690
         Width           =   915
      End
   End
   Begin MSWinsockLib.Winsock CdiPort 
      Index           =   0
      Left            =   60
      Top             =   2700
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin MSWinsockLib.Winsock RemotePort 
      Index           =   0
      Left            =   60
      Top             =   2250
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin VB.Timer tmrCheck 
      Enabled         =   0   'False
      Interval        =   10000
      Left            =   60
      Top             =   60
   End
   Begin VB.Menu RCPopup 
      Caption         =   "RCPopup"
      Visible         =   0   'False
      Begin VB.Menu msg1 
         Caption         =   "Message"
      End
      Begin VB.Menu Rest1 
         Caption         =   "Show Me"
      End
      Begin VB.Menu Exit1 
         Caption         =   "Exit"
      End
   End
End
Attribute VB_Name = "RemoteStart"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Declare Function GetWindowThreadProcessId Lib "user32" (ByVal hWnd As Long, ByRef lpdwProcessId As Long) As Integer
Private Declare Function AllowSetForegroundWindow Lib "user32.dll" (ByVal dwProcessId As Long) As Long
Private Declare Function GetForegroundWindow Lib "user32" () As Long

Dim MyCtrlFunc As String
Dim ApplIsMaster As Boolean
Dim MyOwnIniFile As String
Dim MyApplProcessID As Long
Dim RemoteIsOpen As Boolean
Dim RemoteIsBusy As Boolean
Dim RemoteIdx As Integer
Dim CdiPortIsValid As Boolean
Dim MyCtrlPath As String
Dim MyCtrlAppl As String
Dim MyCtrlName As String
Dim MyCtrlIniFile As String
Dim MyHostName As String
Dim MyHostType As String
Dim ButtonPushedInternal As Boolean
Dim RestartCount As Integer
Dim CtrlStartPID 'As Double 'As Long
Dim CtrlStartTime As Long
Dim CtrlReStartTime As Long
Dim CtrlCheckTime As Long
Dim CtrlInitTime As Long
Dim CtrlRecvPID As Long

Private Sub btnConnect_Click()
    Dim TcpAdr As String
    On Error GoTo ErrHdl
    ButtonPushedInternal = True
    chkTask(0).Value = 1
    TcpAdr = CdiPort(0).LocalIP
    CdiPort(0).RemoteHost = TcpAdr
    CdiPort(0).RemotePort = "4397"
    CdiPort(0).Connect TcpAdr, "4397"
    CdiPortIsValid = True
    ButtonPushedInternal = False
    Exit Sub
ErrHdl:
    Label10.Caption = "Undefined Error on CdiPort"
    chkTask(0).Value = 0
    ButtonPushedInternal = False
End Sub

Private Sub btnDisconnect_Click()
    ButtonPushedInternal = True
    chkTask(1).Value = 1
    CdiPort_Close 0
    ButtonPushedInternal = False
End Sub

Private Sub CdiPort_Close(Index As Integer)
    If CdiPort(Index).State <> sckClosed Then CdiPort(Index).Close
    If CdiPort(0).State <> sckClosed Then CdiPort(0).Close
    ButtonPushedInternal = True
    chkTask(0).Value = 0
    chkTask(1).Value = 0
    ButtonPushedInternal = False
End Sub

Private Sub CdiPort_Connect(Index As Integer)
    CdiPortIsValid = True
End Sub

Private Sub CdiPort_DataArrival(Index As Integer, ByVal bytesTotal As Long)
    Static msgRest As String
    Dim RcvData As String
    Dim rcvText As String
    Dim rcvPID As String
    Dim RcvMsg As String
    Dim tmpLen As String
    Dim MsgLen As Integer
    Dim txtLen As Integer
    Dim msgPos As Integer
    Dim gotMsg As Boolean
    
    CdiPort(Index).GetData RcvData, vbString
    rcvText = msgRest & RcvData
    msgRest = ""
    txtLen = Len(rcvText)
    gotMsg = True
    msgPos = 1
    While (msgPos < txtLen) And (gotMsg = True)
        gotMsg = False
        If (msgPos + 8) < txtLen Then
            tmpLen = Mid(rcvText, msgPos, 8)
            MsgLen = Val(tmpLen)
        End If
        If (msgPos + 7 + MsgLen) <= txtLen Then
            msgPos = msgPos + 8
            RcvMsg = Mid(rcvText, msgPos, MsgLen)
            msgPos = msgPos + MsgLen
            'RunRemoteMsg RcvMsg
            GetKeyItem rcvPID, RcvMsg, "(PID", ")"
            CtrlRecvPID = Val(rcvPID)
            'If CtrlRecvPID = CtrlStartPID Then
                'tmrFocus.Enabled = True
            'Else
            'End If
            Label2.Caption = RcvMsg
            gotMsg = True
            Label10.Caption = ""
        End If
    Wend
    If gotMsg = False Then
        msgRest = Mid(rcvText, msgPos)
    End If
    CdiPort_Close 0
    CdiPortIsValid = True
End Sub

Private Sub CdiPort_Error(Index As Integer, ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)
    Label2.Caption = "Connection Check: Error " & CStr(Number) & vbNewLine & "No response from " & MyCtrlName
    Label10.Caption = Description
    CdiPort_Close 0
    CdiPortIsValid = False
    tmrCheck.Enabled = False
    tmrFocusX.Interval = 4000
    tmrFocusX.Enabled = False
    tmrStart.Interval = CtrlReStartTime
    tmrStart.Enabled = True
End Sub

Private Sub CheckNetTimer_Timer(Index As Integer)
    Dim tmpTag As String
    If CheckNetLedPanel(0).Visible = True Then
        tmpTag = CheckNetLedPanel(0).Tag
        If tmpTag = "CHK" Then
            CheckNetLedPanel(0).Tag = ""
            CheckNetLed(0).Picture = MyPictures.AmpelSmall(4).Picture
            CheckNetLed(1).Picture = MyPictures.AmpelSmall(4).Picture
        Else
            CheckNetLedPanel(0).Tag = "CHK"
            CheckNetLed(0).Picture = MyPictures.AmpelSmall(0).Picture
            CheckNetLed(1).Picture = MyPictures.AmpelSmall(0).Picture
        End If
    End If
End Sub

Private Sub chkTask_Click(Index As Integer)
    If chkTask(Index).Value = 1 Then
        chkTask(Index).BackColor = LightGreen
        If Not ButtonPushedInternal Then
            Select Case Index
                Case 0
                    btnConnect_Click
                Case 1
                    btnDisconnect_Click
                Case 2
                    btnStartApp_Click
                Case Else
            End Select
        End If
    Else
        chkTask(Index).BackColor = vbButtonFace
    End If
End Sub

Private Sub Command1_Click()
    ShutDownRequested = True
    RemotePort_Close 0
    End
End Sub

Private Sub btnStartApp_Click()
    Dim CtrlFile As String
    Dim CtrlPara As String
    Dim CfgData As String
    Dim p1 As Long
    ButtonPushedInternal = True
    chkTask(2).Value = 1
    CtrlPara = " SESS=" & CStr(RestartCount + 1) & " "
    CtrlPara = CtrlPara & "PPID=" & CStr(MyApplProcessID) & " "
    CtrlPara = CtrlPara & "HWND=" & CStr(Me.hWnd) & " "
    CtrlFile = MyCtrlPath & MyCtrlAppl & CtrlPara
    If MyCtrlAppl <> "" Then
        On Error GoTo ErrorHandle
        CtrlStartPID = Shell(CtrlFile, vbNormalFocus)
        If CtrlStartPID > 0 Then
            Label2.Caption = "Started: " & MyCtrlName & " (" & CStr(CtrlStartPID) & ")"
            RestartCount = RestartCount + 1
            If RestartCount = 1 Then
                Label10.Caption = "Started PID " & CStr(CtrlStartPID)
            Else
                Label10.Caption = "Restart " & CStr(RestartCount) & " (" & CStr(CtrlStartPID) & ")"
            End If
            CfgData = GetIniEntry(MyCtrlIniFile, "GLOBAL", "", "APPL_MODE", "APPL")
            If CfgData = "KIOSK" Then
                Command1.Enabled = False
            Else
                Command1.Enabled = True
            End If
            tmrFocusX.Enabled = True
            tmrCheck.Interval = CtrlInitTime
            tmrCheck.Enabled = True
            'btnHide_Click
        Else
            Label2.Caption = "Error Starting: " & vbNewLine & CtrlFile
        End If
    Else
        Label2.Caption = "No Application Configured:" & vbNewLine & vbNewLine & CtrlFile
    End If
    CdiPortIsValid = True
    chkTask(2).Value = 0
    ButtonPushedInternal = False
    Exit Sub
ErrorHandle:
    Label2.Caption = "Error Starting: " & vbNewLine & CtrlFile
    Label10.Caption = "Error: " & Err.Number & " " & Err.Description
    chkTask(2).Value = 0
    ButtonPushedInternal = False
End Sub

Private Sub btnHide_Click()
    Hook Me.hWnd   ' Set up our handler
    AddIconToTray Me.hWnd, Me.Icon, Me.Icon.Handle, App.EXEName + " / Monitoring: " + MyCtrlName
    Me.Hide
End Sub

Private Sub Form_Activate()
    Static IsInit As Boolean
    Dim p1 As Long
    Dim p2 As Long
    If Not IsInit Then
        p1 = GetWindowThreadProcessId(Me.hWnd, p2)
        MyApplProcessID = p2
    End If
End Sub

Private Sub Form_Load()
    Dim tmpCapt As String
    Dim tmpPath As String
    Dim tmpFile As String
    Dim tmpName As String
    Dim CfgData As String
    Dim TimeCtrl As Long
    Dim CaptHeight As Long
    Dim BordWidth As Long
    Dim i As Integer
    If App.PrevInstance Then
        End
    End If
    App.TaskVisible = False
    Set MyMainForm = Me
    SuspendErrMsg = True
    tmpPath = App.Path & "\Config"
    tmpName = Dir(tmpPath, vbDirectory)
    If tmpName = "" Then tmpPath = App.Path
    MyOwnIniFile = tmpPath & "\" & App.EXEName & ".ini"
    MyCtrlFunc = GetIniEntry(MyOwnIniFile, "GLOBAL", "", "CTRL_FUNC", "WATCH")
    If MyCtrlFunc = "MASTER" Then
        ApplIsMaster = True
    Else
        ApplIsMaster = False
    End If
    
    MyCtrlPath = GetIniEntry(MyOwnIniFile, "GLOBAL", "", "CTRL_PATH", App.Path)
    MyCtrlAppl = GetIniEntry(MyOwnIniFile, "GLOBAL", "", "CTRL_APPL", "StaffMonitor.exe")
    MyCtrlName = GetItem(MyCtrlAppl, 1, ".")
    tmpPath = MyCtrlPath & "\Config"
    tmpName = Dir(tmpPath, vbDirectory)
    If tmpName = "" Then tmpPath = MyCtrlPath
    
    
    MyCtrlIniFile = tmpPath & "\" & MyCtrlName & ".ini"
    MyHostName = GetIniEntry(MyCtrlIniFile, "GLOBAL", "", "HOSTNAME", "")
    MyHostType = GetIniEntry(MyCtrlIniFile, "GLOBAL", "", "HOSTTYPE", "Production Server")
    
    CfgData = GetIniEntry(MyCtrlIniFile, "GLOBAL", "", "APPL_MODE", "APPL")
    If CfgData = "KIOSK" Then
        Command1.Enabled = False
    Else
        Command1.Enabled = True
    End If
    
    'tmpCapt = "Connecting " & MyCtrlName & " to " & MyHostType
    tmpCapt = "Connecting " & "VDU Unit " & CdiPort(0).LocalHostName & " to " & MyHostType
    
    TopLabel(0).Caption = tmpCapt
    
    Label3.Caption = "Monitoring: " + MyCtrlName
    If Right(MyCtrlPath, 1) <> "\" Then MyCtrlPath = MyCtrlPath + "\"
    tmrCheck.Enabled = False
    TimeCtrl = Val(GetIniEntry(MyOwnIniFile, "GLOBAL", "", "CTRL_CHECK", "10")) * 1000
    If TimeCtrl < 4000 Then TimeCtrl = 4000
    CtrlCheckTime = TimeCtrl
    TimeCtrl = Val(GetIniEntry(MyOwnIniFile, "GLOBAL", "", "CTRL_INIT", "2")) * 1000
    If TimeCtrl < 2000 Then TimeCtrl = 2000
    CtrlInitTime = TimeCtrl
    tmrCheck.Interval = TimeCtrl
    tmrCheck.Enabled = False
    
    'TimeCtrl = Val(GetIniEntry(MyOwnIniFile, "GLOBAL", "", "CTRL_FOCUS", "1")) * 1000
    'If TimeCtrl < 1000 Then TimeCtrl = 1000
    'TimeCtrl = 4000
    'TimeCtrl = 250
    'tmrFocus.Interval = TimeCtrl
    'tmrFocus.Enabled = False
    
    TimeCtrl = Val(GetIniEntry(MyOwnIniFile, "GLOBAL", "", "CTRL_START", "200")) * 1
    If TimeCtrl < 100 Then TimeCtrl = 100
    CtrlStartTime = TimeCtrl
    TimeCtrl = Val(GetIniEntry(MyOwnIniFile, "GLOBAL", "", "CTRL_RE_START", "200")) * 1
    If TimeCtrl < 500 Then TimeCtrl = 500
    CtrlReStartTime = TimeCtrl
    
    CheckNetLedPanel(0).BackColor = MainPanel(0).BackColor
    CheckNetLed(0).Picture = MyPictures.AmpelSmall(3).Picture
    CheckNetLed(1).Picture = MyPictures.AmpelSmall(3).Picture
    CheckNetLedPanel(0).Height = CheckNetLed(0).Height
    CheckNetLedPanel(0).Width = CheckNetLed(1).Left + CheckNetLed(1).Width
    ArrangeSystemCheckLayout
    CaptHeight = Me.Height - Me.ScaleHeight
    BordWidth = Me.Width - Me.ScaleWidth
    
    Me.Height = MainPanel(0).Height + CaptHeight '+ 1500
    'BottomPanel(0).Top = TopPanel(0).Height + MainPanel(0).Height
    Me.Width = MainPanel(0).Width + BordWidth
    Me.Left = Screen.Width - Me.Width
    For i = 1 To MainPanel.UBound
        MainPanel(i).Top = MainPanel(0).Top
        MainPanel(i).Left = MainPanel(0).Left
        MainPanel(i).Width = MainPanel(0).Width
        MainPanel(i).Height = MainPanel(0).Height
    Next
    MainPanel(0).Visible = False
    MainPanel(1).Visible = True
    OpenRemotePort
    CdiPortIsValid = False
    tmrCheck.Enabled = False
    If Not ApplIsMaster Then
        'tmrFlow.Tag = "CHK_NET"
        'tmrFlow.Tag = "RUN_APP"
        tmrFlow.Tag = "RUN_MON"
        tmrFlow.Interval = 100
        tmrFlow.Enabled = True
    End If
End Sub
Private Sub ArrangeSystemCheckLayout()
    Dim NewTop As Long
    Dim NewLeft As Long
    Dim MainLeft As Long
    Dim AnyLeft As Long
    Dim IconLeft As Long
    Dim i As Integer
    Dim j As Integer
    MainLabel(0).ZOrder
    MainPanel(0).Top = 0
    MainPanel(0).Left = 0
    'BackGrndPic(0).Picture = MyPictures.LoginMask(1)
    BackGrndPic(0).Left = 0
    BackGrndPic(0).Width = MyPictures.LoginMask(1).Width
    BackGrndPic(0).Height = MyPictures.LoginMask(1).Height
    MainPanel(0).Width = BackGrndPic(0).Width '+ 60
    NewTop = 0
    TopLabel(0).Top = NewTop
    TopLabel(0).Left = 0
    TopLabel(0).Width = MainPanel(0).ScaleWidth
    
    NewTop = NewTop + TopLabel(0).Height
    BackGrndPic(0).Top = NewTop
    MainPanel(0).Height = NewTop + BackGrndPic(0).Height '+ 60
    For i = 0 To AnyLabel.UBound
        While (AnyLabel(i).Width < AnyLabel(0).Width)
            AnyLabel(i).Caption = AnyLabel(i).Caption & "."
        Wend
        AnyIcon(i).Picture = MyPictures.imgCtl(1).Picture
        AnyShadow(i).Visible = False
    Next
    NewTop = NewTop + 120
    MainLeft = 120
    For i = 0 To MainLabel.UBound
        MainLabel(i).Left = MainLeft
        MainLabel(i).Top = NewTop
        MainShadow(i).Caption = MainLabel(i).Caption
        MainLabel(i).ForeColor = vbWhite
        MainShadow(i).ForeColor = vbBlack
        MainShadow(i).Top = MainLabel(i).Top + 15
        MainShadow(i).Left = MainLabel(i).Left + 15
        MainLabel(i).ZOrder
        NewTop = NewTop + MainLabel(i).Height
        Select Case i
            Case 0
                NewTop = NewTop + 180
                MainLeft = MainLeft + 150
                AnyLeft = MainLeft + 330
                IconLeft = AnyLeft + AnyLabel(0).Width
            Case 1
                For j = 0 To 0
                    AnyLabel(j).Left = AnyLeft
                    AnyIcon(j).Left = IconLeft
                    AnyLabel(j).Top = NewTop
                    AnyIcon(j).Top = NewTop + ((AnyLabel(j).Height - AnyIcon(j).Height) / 2) - 30
                    AnyShadow(i).Caption = AnyLabel(i).Caption
                    AnyShadow(i).Top = AnyLabel(i).Top - 15
                    AnyShadow(i).Left = AnyLabel(i).Left - 15
                    AnyShadow(i).Visible = False
                    AnyLabel(i).ZOrder
                    NewTop = NewTop + AnyLabel(i).Height
                Next
                NewTop = NewTop + 90
            Case 2
                For j = 2 To 3
                    AnyLabel(j).Left = AnyLeft
                    AnyIcon(j).Left = IconLeft
                    AnyLabel(j).Top = NewTop
                    AnyIcon(j).Top = NewTop + ((AnyLabel(j).Height - AnyIcon(j).Height) / 2) - 30
                    AnyShadow(i).Caption = AnyLabel(i).Caption
                    AnyShadow(i).Top = AnyLabel(i).Top - 15
                    AnyShadow(i).Left = AnyLabel(i).Left - 15
                    AnyShadow(i).Visible = False
                    AnyLabel(i).ZOrder
                    NewTop = NewTop + AnyLabel(i).Height
                Next
                NewTop = NewTop + 90
            Case 3
                For j = 4 To 7
                    AnyLabel(j).Left = AnyLeft
                    AnyIcon(j).Left = IconLeft
                    AnyLabel(j).Top = NewTop
                    AnyIcon(j).Top = NewTop + ((AnyLabel(j).Height - AnyIcon(j).Height) / 2) - 30
                    AnyShadow(i).Caption = AnyLabel(i).Caption
                    AnyShadow(i).Top = AnyLabel(i).Top - 15
                    AnyShadow(i).Left = AnyLabel(i).Left - 15
                    AnyShadow(i).Visible = False
                    AnyLabel(i).ZOrder
                    NewTop = NewTop + AnyLabel(i).Height
                Next
                NewTop = NewTop + 90
            Case Else
        End Select
    Next
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Cancel = True
    'Unload MyPictures
End Sub

Public Sub RemotePort_Close(Index As Integer)
    If RemotePort(Index).State <> sckClosed Then RemotePort(Index).Close
    If RemotePort(0).State <> sckClosed Then RemotePort(0).Close
    If Index > 0 Then Unload RemotePort(Index)
    RemoteIsOpen = False
    RemoteIsBusy = False
    RemoteIdx = -1
    Label1.Caption = "Remote Connection Closed"
    If Not ShutDownRequested Then OpenRemotePort
End Sub

Private Sub RemotePort_Connect(Index As Integer)
    MsgBox "Remote Connected"
    RemoteIdx = 0
    RemoteIsBusy = True
End Sub

Private Sub RemotePort_ConnectionRequest(Index As Integer, ByVal requestID As Long)
    Dim OutMsg As String
    If RemotePort(0).State <> sckClosed Then RemotePort(0).Close
    RemoteIdx = 1
    Load RemotePort(RemoteIdx)
    Label1.Caption = "Connected: " & requestID
    RemotePort(RemoteIdx).Accept requestID
    OutMsg = "CONNEX,HOST,-1||" & RemotePort(0).LocalIP & " / " & RemotePort(0).LocalHostName
    SendRemoteMsg OutMsg
End Sub

Private Sub RemotePort_DataArrival(Index As Integer, ByVal bytesTotal As Long)
    Static msgRest As String
    Dim RcvData As String
    Dim rcvText As String
    Dim RcvMsg As String
    Dim tmpLen As String
    Dim MsgLen As Integer
    Dim txtLen As Integer
    Dim msgPos As Integer
    Dim gotMsg As Boolean
    RemotePort(Index).GetData RcvData, vbString
    rcvText = msgRest & RcvData
    msgRest = ""
    txtLen = Len(rcvText)
    gotMsg = True
    msgPos = 1
    While (msgPos < txtLen) And (gotMsg = True)
        gotMsg = False
        If (msgPos + 8) < txtLen Then
            tmpLen = Mid(rcvText, msgPos, 8)
            MsgLen = Val(tmpLen)
        End If
        If (msgPos + 7 + MsgLen) <= txtLen Then
            msgPos = msgPos + 8
            RcvMsg = Mid(rcvText, msgPos, MsgLen)
            msgPos = msgPos + MsgLen
            RunRemoteMsg RcvMsg
            gotMsg = True
        End If
    Wend
    If gotMsg = False Then
        msgRest = Mid(rcvText, msgPos)
    End If
End Sub

Private Sub RemotePort_Error(Index As Integer, ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)
    'MsgBox "Error: " & Number & vbLf & Description
    RemotePort_Close Index
End Sub

Private Sub RemotePort_SendComplete(Index As Integer)
'
End Sub

Private Sub RemotePort_SendProgress(Index As Integer, ByVal bytesSent As Long, ByVal bytesRemaining As Long)
'
End Sub

Public Sub OpenRemotePort()
    On Error GoTo ErrorHandle
    If (Not RemoteIsOpen) And (Not RemoteIsBusy) Then
        RemotePort(0).LocalPort = "4396"
        RemotePort(0).Listen
        RemoteIdx = 0
        Label1.Caption = "Listening on Remote Port"
        Label2.Caption = ""
        RemoteIsOpen = True
    End If
    Exit Sub
ErrorHandle:
    'End
End Sub

Private Sub RunRemoteMsg(RcvTcpMsg As String)
    Dim RcvTsk As String
    Dim RcvMsg As String
    Dim RcvCmd As String
    Dim RunCmd As String
    Dim RunIdx As String
    Dim OutMsg As String
    Dim tmpdat As String
    Dim CtrlStartPID As Double
    'MsgBox RcvTcpMsg
    RcvTsk = GetItem(RcvTcpMsg, 1, "||")
    RcvMsg = GetItem(RcvTcpMsg, 2, "||")
    RcvCmd = GetItem(RcvTsk, 1, ",")
    RunCmd = GetItem(RcvTsk, 2, ",")
    RunIdx = GetItem(RcvTsk, 3, ",")
    Label2.Caption = RcvMsg
    Select Case RcvCmd
        Case "START"
            CtrlStartPID = Shell(RcvMsg, vbNormalFocus)
            Label2.Caption = Label2.Caption & vbNewLine & Str(CtrlStartPID)
            SendRemoteMsg "STARTED,APPL," & Str(CtrlStartPID) & "||" & RcvMsg
        Case "CCO"
            SendRemoteMsg "CCA"
        Case Else
            'MsgBox RcvTcpMsg
    End Select
End Sub

Public Sub SendRemoteMsg(OutText As String)
    Dim tmpLen As String
    Dim totLen As Integer
    totLen = Len(OutText)
    tmpLen = Trim(Str(totLen))
    tmpLen = Right(("00000000" & tmpLen), 8)
    'MsgBox "Now Sending Answer"
    RemotePort(RemoteIdx).SendData tmpLen & OutText
    'MsgBox "Answer Sent!"
End Sub


Private Sub tmrCheck_Timer()
    tmrCheck.Enabled = False
    If ShutDownRequested = True Then
        End
    End If
    If CdiPortIsValid = True Then
        btnConnect_Click
    Else
        'btnStartApp_Click
    End If
    tmrCheck.Interval = CtrlCheckTime
    tmrCheck.Enabled = True
End Sub
' Handler for mouse events occuring in system tray.
Public Sub SysTrayMouseEventHandler()
    SetForegroundWindow Me.hWnd
    PopupMenu RCPopup, vbPopupMenuRightButton
End Sub

Private Sub msg1_Click()
    MsgBox "This is a test message", vbOKOnly, "Hello"
End Sub

Private Sub Rest1_Click()
    Unhook    ' Return event control to windows
    Me.Show
    RemoveIconFromTray
End Sub

Private Sub Exit1_Click()
    Unhook    ' Return event control to windows
    RemoveIconFromTray
    ShutDownRequested = True
    tmrCheck.Enabled = False
    tmrCheck.Interval = 500
    tmrCheck.Enabled = True
End Sub

Private Sub tmrFlow_Timer()
    Dim tmpTag As String
    Dim i As Integer
    tmrFlow.Enabled = False
    tmpTag = tmrFlow.Tag
    Select Case tmpTag
        Case "CHK_NET"
            For i = 0 To AnyIcon.UBound
                AnyIcon(i).Picture = MyPictures.imgCtl(0).Picture
            Next
            tmrFlow.Tag = "RUN_APP"
            tmrFlow.Enabled = True
        Case "RUN_APP"
            MainPanel(1).Visible = True
            MainPanel(0).Visible = False
            tmrFlow.Tag = "RUN_MON"
            tmrFlow.Enabled = True
        Case "RUN_MON"
            tmrStart.Interval = CtrlStartTime
            tmrStart.Enabled = True
        Case Else
    End Select
End Sub

Private Sub tmrFocusX_Timer()
    Dim fgwHwnd As Long
    Dim p1 As Long
    tmrFocusX.Enabled = False
    fgwHwnd = GetForegroundWindow()
    If fgwHwnd = Me.hWnd Then
        p1 = AllowSetForegroundWindow(CtrlStartPID)
    End If
    tmrFocusX.Interval = 1000
    tmrFocusX.Enabled = True
End Sub

Private Sub tmrStart_Timer()
    tmrStart.Enabled = False
    If Not ApplIsMaster Then btnStartApp_Click
End Sub
