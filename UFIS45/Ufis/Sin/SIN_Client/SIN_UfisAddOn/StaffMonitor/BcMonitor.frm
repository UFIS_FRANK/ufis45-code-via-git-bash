VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "tab.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form BcMonitor 
   BorderStyle     =   5  'Sizable ToolWindow
   Caption         =   "Broadcast Monitor"
   ClientHeight    =   8550
   ClientLeft      =   60
   ClientTop       =   330
   ClientWidth     =   3420
   Icon            =   "BcMonitor.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8550
   ScaleWidth      =   3420
   ShowInTaskbar   =   0   'False
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   1
      Top             =   8265
      Width           =   3420
      _ExtentX        =   6033
      _ExtentY        =   503
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin TABLib.TAB BcList 
      Height          =   1935
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   1425
      _Version        =   65536
      _ExtentX        =   2514
      _ExtentY        =   3413
      _StockProps     =   64
   End
End
Attribute VB_Name = "BcMonitor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Function BcInput(ByVal BcNum As String, ByVal DestName As String, ByVal RecvName As String, ByVal Cmd As String, ByVal ObjName As String, ByVal Selection As String, ByVal Fields As String, ByVal Data As String, ByVal twe As String, ByVal tws As String, ByVal OrigName As String) As Long
    Dim BcText As String
    Dim TblBcNum As String
    Dim tmpTime As String
    Dim MsgTxt As String
    Dim ConText As String
    Dim CurBcNum As Long
    Dim BcDiff As Long
    Dim CurLine As Long
    Dim TxtColor As Long
    Dim BckColor As Long
    tmpTime = Format(Now, "YYYY.MM.DD/hh:mm:ss")
    CurBcNum = Val(BcNum)
    If BcMonActive = True Then
        'BcDiff = CurBcNum - LastBcNumber
        'If (BcDiff <> 1) And (BcDiff <> -29999) Then
        '    BcText = tmpTime + Chr(16) + Chr(16) + BcNum + Chr(16) + "CHECK DIFF!" + Chr(16) + CStr(BcDiff - 1) + Chr(16) + " " + Chr(16) + " " + Chr(16) + " " + Chr(16) + " " + Chr(16) + " " + Chr(16) + " " + Chr(16) + " " + Chr(16) + " "
        '    BcList.InsertTextLine BcText, False
        '    CurLine = BcList.GetLineCount - 1
        '    BcList.SetLineColor CurLine, vbWhite, vbRed
        '    ConText = " >> CHECKDIFF: " + Left(CStr(BcDiff - 1) + "      ", 6)
        'End If
    End If
    'MsgTxt = "BCMONITOR (" + Left(OrigName + "           ", 10) + ") BCNUM=" + Left(BcNum + "      ", 6) + "(CMD=" + Left(Cmd + "     ", 5) + " OBJ=" + ObjName + ")" + ConText
    'MainForm.MsgLogging MsgTxt
    ConText = ""
    BcText = tmpTime + Chr(16) + OrigName + Chr(16) + BcNum + Chr(16) + DestName + Chr(16) + RecvName + Chr(16) + twe + Chr(16) + tws + Chr(16) + Cmd + Chr(16) + ObjName + Chr(16) + Selection + Chr(16) + Data + Chr(16) + Fields + Chr(16)
    BcList.InsertTextLine BcText, False
    CurLine = BcList.GetLineCount - 1
    If OrigName = MyHostName Then
        TxtColor = vbBlack
        BckColor = LightestYellow
    Else
        TxtColor = vbBlack
        BckColor = LightGrey
    End If
    BcList.SetLineColor CurLine, TxtColor, BckColor
    If CurLine > 1000 Then
        BcList.DeleteLine 0
    End If
    BcList.AutoSizeColumns
    BcList.OnVScrollTo CurLine
    BcList.Refresh
    BcMonActive = True
    LastBcNumber = CurBcNum
    BcInput = CurLine
End Function

Private Sub Form_Load()
    Dim NewSize As Long
    Dim CapHeight As Long
    Dim FraWidth As Long
    CapHeight = MainForm.Height - (MainForm.ScaleHeight * Screen.TwipsPerPixelY)
    FraWidth = (MainForm.Width - (MainForm.ScaleWidth * Screen.TwipsPerPixelX)) / 2
    Me.Top = MainForm.Top + ((MainForm.PageTitle.Top + MainForm.PageTitle.Height) * Screen.TwipsPerPixelY) + CapHeight - FraWidth
    Me.Left = MainForm.Left + ((MainForm.ScaleWidth * Screen.TwipsPerPixelX) - Me.Width) + FraWidth
    Me.Height = (MainForm.Height) - Me.Top + MainForm.Top - FraWidth
    BcList.ResetContent
    BcList.HeaderString = "Time,OrigName,BcNum,DestName,RecvName,TWE,TWS,Cmd ,ObjName,Selection,Data,Fields"
    BcList.SetFieldSeparator (Chr(16))
    BcList.FontName = "Courier New"
    BcList.ShowHorzScroller True
    BcList.ShowVertScroller True
    BcList.AutoSizeByHeader = True
    'MainForm.MsgLogging "BCMONITOR ACTIVATED"
    Me.Show
End Sub

Private Sub Form_Resize()
    Dim NewSize As Long
    NewSize = Me.ScaleWidth
    If NewSize > 600 Then BcList.Width = NewSize
    NewSize = Me.ScaleHeight - StatusBar1.Height
    If NewSize > 600 Then BcList.Height = NewSize
End Sub

Private Sub Form_Unload(Cancel As Integer)
    'MainForm.MsgLogging "BCMONITOR RESET AND CLOSED"
End Sub
