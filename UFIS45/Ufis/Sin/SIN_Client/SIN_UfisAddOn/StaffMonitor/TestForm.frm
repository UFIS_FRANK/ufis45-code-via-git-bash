VERSION 5.00
Begin VB.Form TestForm 
   Caption         =   "Form1"
   ClientHeight    =   5685
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   7005
   LinkTopic       =   "Form1"
   ScaleHeight     =   5685
   ScaleWidth      =   7005
   StartUpPosition =   3  'Windows Default
   Begin VB.PictureBox Picture1 
      AutoSize        =   -1  'True
      Height          =   540
      Left            =   1800
      Picture         =   "TestForm.frx":0000
      ScaleHeight     =   480
      ScaleWidth      =   360
      TabIndex        =   2
      Top             =   3840
      Width           =   420
   End
   Begin VB.TextBox Text2 
      Height          =   2835
      Left            =   3690
      TabIndex        =   1
      Text            =   "Text2"
      Top             =   690
      Width           =   2475
   End
   Begin VB.TextBox Text1 
      Height          =   2895
      Left            =   840
      TabIndex        =   0
      Text            =   "Text1"
      Top             =   660
      Width           =   2505
   End
End
Attribute VB_Name = "TestForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Option Explicit

'On form1 place 2 textboxes (with a height for a couple of lines) and 1 picturebox.
'Select a bitmap for the picturebox and set the autosize on true.
Private Declare Function CreateCaret Lib "user32" (ByVal hwnd As Long, ByVal hBitmap As Long, ByVal nWidth As Long, ByVal nHeight As Long) As Long
Private Declare Function ShowCaret Lib "user32" (ByVal hwnd As Long) As Long
Private Declare Function GetFocus Lib "user32" () As Long
Private Sub Form_Load()
    'KPD-Team 1999
    'URL: http://www.allapi.net/
    'E-Mail: KPDTeam@Allapi.net

    'Execute the app. (F5) and you'll see the difference of the cursorshapes.
End Sub
Sub Text1_GotFocus()
    'retrieve the window which has the focus
    h& = GetFocus&()
    'retrieve the handle of our picture
    b& = Picture1.Picture
    'Create a new cursor
    '(handle, bitmap 0=none, width, height)
    Call CreateCaret(h&, b&, 10, 10)
    'Show our new cursor
    X& = ShowCaret&(h&)
End Sub
Private Sub Text2_GotFocus()
    'retrieve the window which has the focus
    h& = GetFocus&()
    'Create a new cursor
    Call CreateCaret(h&, 0, 30, 30)
    'Show the new cursor
    X& = ShowCaret&(h&)
End Sub
