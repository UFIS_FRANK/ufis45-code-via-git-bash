Attribute VB_Name = "MyOwnLib"
Option Explicit

Public Declare Function GetKeyState Lib "user32" (ByVal nVirtKey As Long) As Integer

Public ShowMousePointer As Boolean

Public RestoreFilter As Boolean
Public AutoAodbFilterLoad As Boolean
Public ApplIsFdsMain As Boolean
Public MyOwnButtonFace As Long
Public DataFilterVpfr As String
Public DataFilterVpto As String
Public ApplMainCode As String
Public ApplFuncCode As String
Public FilterFileName As String
Public MyDateInputFormat As String
Public MyTimeInputFormat As String
Public AodbReaderSection As String
Public CurCedaIdx As Integer

Public MyTabTrigger01(2) As String
Public MyTabTrigger02(2) As String
Public MyTabTrigger03(2) As String

Public SrvLocTime As String
Public SrvUtcTime As String
Public WksLocTime As String
Public WksUtcTime As String
Public SrvLocDate As Date
Public SrvUtcDate As Date
Public WksLocDate As Date
Public WksUtcDate As Date
Public WksLocDiff As Long
Public WksUtcDiff As Long
Public WksNowDate As Date
Public WksPrvDate As Date
Public WksNowDiff As Long

Public ApplIsInKioskMode As Boolean
Public ApplDefaultKioskMode As Boolean
Public MyConfigPath As String
Public PageGroupIniFile As String
Public StaffPageIniFile As String
Public PageLayoutIniFile As String
Public StaffPageSection As String

Public MsgLoggingIsActive As Boolean
Public WinShutDown As Boolean
Public MyHostName As String
Public MyHostType As String
Public MyApplVersion As String
Public MyUserName As String
Public MyShortName As String
Public MyTblExt As String
Public MyHopo As String

Public MyVduName As String
Public MyWksName As String
Public MyWksType As String
Public MyUnitLocation As String
Public MyBdpsSecUrno As String
Public MyBdpsSecStat As String
Public MyBdpsSecRema As String

Public SyncCursorBusy As Boolean

Public MyCcaVpfr As String
Public MyCcaVpto As String
Public TimesInLocal As Boolean
Public ShowBasics As Boolean
Public ReloadOnDemand As Boolean
Public DarkLineColor As Long
Public LightLineColor As Long
Public LineTextColor As Long
Public LastBcNumber As Long
Public BcMonActive As Boolean

Public Const MAX_MSG_CNT = 40
Public Const MAX_MSG_TYP = 20

Public Type MsgTimer
    TotalSec As Integer
    CountSec As Integer
End Type
Public Type MsgLayout
    LblBackStyle As Integer
    LblBorderStyle As Integer
    LblForeColor As Long
    LblBackColor As Long
    LblTextTrim As Boolean
    LblTextUpper As Boolean
    SepBorderStyle As Integer
    SepBackColor As Long
    MsgAnimation As MsgTimer
End Type
Public Type MyInfoRec
    MsgSysIdx As Integer
    MsgSysRow As Integer
    MsgSysMsgUrno As String
    MsgSysRefUrno As String
    MsgSysCount As Long
    MsgSysInUse As Boolean
    MsgTypeIdx As Integer
    MsgDspType As String
    MsgDspPrio As String
    MsgDspText As String
    MsgDspAddi As String
    
    MsgTimeStampBgn As String
    MsgTimeStampEnd As String
    MsgAniStatus As Integer
    MsgNrmTimer As MsgTimer
    MsgAniTimer As MsgTimer
    
    MsgLblNorm As MsgLayout
    MsgLblAni1 As MsgLayout
    MsgLblAni2 As MsgLayout
    
    MsgBlinkStatus As Integer
    MsgLayoutType As Integer
    
    MsgDspLayout As Integer
    MsgDspTxtColor As Long
    MsgDspBckColor As Long
    MsgExpireTime As Long
    
    MsgTrgBlkDefTime As Integer
    MsgTrgBlkDefPuls As Integer
    MsgTrgBlkAltTime As Integer
    MsgTrgBlkAltPuls As Integer
    MsgTrgBlkDefTxtColor As Long
    MsgTrgBlkDefBckColor As Long
    MsgTrgBlkAltTxtColor As Long
    MsgTrgBlkAltBckColor As Long
End Type

Public CurCfgInfoRec As MyInfoRec
Public MsgTypeArray(MAX_MSG_TYP) As MyInfoRec

Public Sub InitMsgInfoTypes(TypeCfgTab As TABLib.TAB)
    Dim LineNo As Long
    Dim i As Integer
    LineNo = 0
    For i = 0 To MAX_MSG_TYP
        FillMyInfoRec TypeCfgTab, LineNo, MsgTypeArray(i)
        LineNo = LineNo + 1
    Next
End Sub

Public Sub FillMyInfoRec(TypeCfgTab As TABLib.TAB, LineNo As Long, CurInfoRec As MyInfoRec)
    Dim AllArea As String
    Dim tmpArea As String
    Dim tmpList As String
    Dim tmpProp As String
    Dim tmpData As String
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim iVal As Integer
    Dim Itm As Integer
    Dim DatIdx As Integer
    Dim ObjIdx As Integer
    Dim AreaRec As MsgLayout
    CurInfoRec.MsgSysCount = 0
    CurInfoRec.MsgSysInUse = False
    'CfgTab(i).LogicalFieldList = "SQID,CSRC,TYPE,PRIO,TEXT,NORM,ANI1,ANI2,ADDI"
    CurInfoRec.MsgSysIdx = Val(TypeCfgTab.GetFieldValue(LineNo, "SQID"))
    CurInfoRec.MsgDspType = TypeCfgTab.GetFieldValue(LineNo, "TYPE")
    CurInfoRec.MsgDspPrio = TypeCfgTab.GetFieldValue(LineNo, "PRIO")
    CurInfoRec.MsgDspText = TypeCfgTab.GetFieldValue(LineNo, "TEXT")
    CurInfoRec.MsgDspAddi = ""
    AllArea = "NORM,ANI1,ANI2"
    Itm = 0
    tmpArea = "START"
    While tmpArea <> ""
        Itm = Itm + 1
        tmpArea = GetItem(AllArea, Itm, ",")
        If tmpArea <> "" Then
            Select Case Itm
                Case 1
                    AreaRec = CurInfoRec.MsgLblNorm
                Case 2
                    AreaRec = CurInfoRec.MsgLblAni1
                Case 3
                    AreaRec = CurInfoRec.MsgLblAni2
                Case Else
            End Select
            tmpList = TypeCfgTab.GetFieldValue(LineNo, tmpArea)
            tmpList = Mid(tmpList, 2)
            DatIdx = 0
            tmpProp = "START"
            While tmpProp <> ""
                DatIdx = DatIdx + 1
                tmpProp = GetItem(tmpList, DatIdx, "#")
                DatIdx = DatIdx + 1
                tmpData = GetItem(tmpList, DatIdx, "#")
                Select Case tmpProp
                    Case "TRNS"
                        iVal = Val(tmpData)
                        If iVal = 0 Then AreaRec.LblBackStyle = 1 Else AreaRec.LblBackStyle = 0
                    Case "BORD"
                        iVal = Val(tmpData)
                        AreaRec.LblBorderStyle = iVal
                    Case "TRIM"
                        iVal = Val(tmpData)
                        If iVal = 1 Then AreaRec.LblTextTrim = True Else AreaRec.LblTextTrim = False
                    Case "UPCS"
                        iVal = Val(tmpData)
                        If iVal = 1 Then AreaRec.LblTextUpper = True Else AreaRec.LblTextUpper = False
                    Case "SEPB"
                        iVal = Val(tmpData)
                        AreaRec.SepBorderStyle = iVal
                    Case "BKCO"
                        AreaRec.LblBackColor = Val(tmpData)
                    Case "SEPC"
                        AreaRec.SepBackColor = Val(tmpData)
                    Case "TXCO"
                        AreaRec.LblForeColor = Val(tmpData)
                    Case "TIME"
                        AreaRec.MsgAnimation.TotalSec = Val(tmpData)
                        AreaRec.MsgAnimation.CountSec = 0
                    Case Else
                End Select
            Wend
            Select Case Itm
                Case 1
                    CurInfoRec.MsgLblNorm = AreaRec
                Case 2
                    CurInfoRec.MsgLblAni1 = AreaRec
                Case 3
                    CurInfoRec.MsgLblAni2 = AreaRec
                Case Else
            End Select
        End If
    Wend
    CurInfoRec.MsgAniTimer.TotalSec = CurInfoRec.MsgLblAni1.MsgAnimation.TotalSec
    CurInfoRec.MsgNrmTimer.TotalSec = CurInfoRec.MsgLblNorm.MsgAnimation.TotalSec * 60
End Sub

Public Sub ReadCedaData(CurTab As TABLib.TAB, TableName As String, Fields As String, SqlKey As String)
    Dim tmpData As String
    CurTab.ResetContent
    CurTab.LogicalFieldList = Fields
    CurTab.HeaderString = Fields
    tmpData = CreateLengthList(Fields)
    CurTab.HeaderLengthString = tmpData
    CurTab.ColumnWidthString = tmpData
    CurTab.AutoSizeByHeader = True
    CurTab.ShowHorzScroller False
    CurTab.CedaCurrentApplication = MyShortName '& "," & "4.5.1.4"
    CurTab.CedaHopo = MyHopo
    CurTab.CedaIdentifier = "IDX"
    CurTab.CedaPort = "3357"
    CurTab.CedaReceiveTimeout = "250"
    CurTab.CedaRecordSeparator = vbLf
    CurTab.CedaSendTimeout = "250"
    CurTab.CedaServerName = MyHostName
    CurTab.CedaTabext = MyTblExt
    CurTab.CedaUser = MyUserName
    CurTab.CedaWorkstation = MyWksName
    If CedaIsConnected Then
        If (TableName <> "") And (Fields <> "") Then
            CurTab.CedaAction "RTA", TableName, Fields, "", SqlKey
        End If
    End If
    'CurTab.AutoSizeColumns
    'CurTab.RedrawTab
End Sub

Public Function CreateLengthList(FieldList As String) As String
    Dim ItmCnt As Integer
    Dim ItmDat As String
    Dim Result As String
    ItmDat = "START"
    ItmCnt = 0
    Result = "10"
    While ItmDat <> ""
        ItmCnt = ItmCnt + 1
        ItmDat = GetItem(FieldList, ItmCnt, ",")
        If ItmDat <> "" Then
            Result = Result + ",10"
        End If
    Wend
    CreateLengthList = Result
End Function

Public Sub SetFieldValue(FieldName As String, NewValue As String, DataList As String, FieldList As String)
    Dim ItmNbr As Integer
    If InStr(FieldList, FieldName) > 0 Then
        ItmNbr = GetItemNo(FieldList, FieldName)
        SetItem DataList, ItmNbr, ",", NewValue
    End If
End Sub

Public Sub SetAllFormsOnTop(SetTop As Boolean)

End Sub

Public Sub GetColorValues(LineColors As String)
    Dim strColor As String
    strColor = GetItem(LineColors, 1, ",")
    DarkLineColor = TranslateColor(strColor)
    strColor = GetItem(LineColors, 2, ",")
    LightLineColor = TranslateColor(strColor)
End Sub
Public Function TranslateColor(ColorText As String) As Long
    Dim tmpColor As Long
    Select Case ColorText
        Case "GRAY1"
            tmpColor = &HFFFFFF
        Case "GRAY2"
            tmpColor = &HE0E0E0
        Case "GRAY3"
            tmpColor = &HC0C0C0
        Case "GRAY4"
            tmpColor = &H808080
        Case "GRAY5"
            tmpColor = &H404040
        Case "GRAY6"
            tmpColor = &H0&
        Case "RED1"
            tmpColor = &HC0C0FF
        Case "RED2"
            tmpColor = &H8080FF
        Case "RED3"
            tmpColor = &HFF&
        Case "RED4"
            tmpColor = &HC0&
        Case "RED5"
            tmpColor = &H80&
        Case "RED6"
            tmpColor = &H40&
        Case "AMBER1"
            tmpColor = &HC0E0FF
        Case "AMBER2"
            tmpColor = &H80C0FF
        Case "AMBER3"
            tmpColor = &H80FF&
        Case "AMBER4"
            tmpColor = &H40C0&
        Case "AMBER5"
            tmpColor = &H4080&
        Case "AMBER6"
            tmpColor = &H404080
        Case "YELLOW1"
            tmpColor = &HC0FFFF
        Case "YELLOW2"
            tmpColor = &H80FFFF
        Case "YELLOW3"
            tmpColor = &HFFFF&
        Case "YELLOW4"
            tmpColor = &HC0C0&
        Case "YELLOW5"
            tmpColor = &H8080&
        Case "YELLOW6"
            tmpColor = &H4040&
        Case "GREEN1"
            tmpColor = &HC0FFC0
        Case "GREEN2"
            tmpColor = &H80FF80
        Case "GREEN3"
            tmpColor = &HFF00&
        Case "GREEN4"
            tmpColor = &HC000&
        Case "GREEN5"
            tmpColor = &H8000&
        Case "GREEN6"
            tmpColor = &H4000&
        Case "CYAN1"
            tmpColor = &HFFFFC0
        Case "CYAN2"
            tmpColor = &HFFFF80
        Case "CYAN3"
            tmpColor = &HFFFF00
        Case "CYAN4"
            tmpColor = &HC0C000
        Case "CYAN5"
            tmpColor = &H808000
        Case "CYAN6"
            tmpColor = &H404000
        Case "BLUE1"
            tmpColor = &HFFC0C0
        Case "BLUE2"
            tmpColor = &HFF8080
        Case "BLUE3"
            tmpColor = &HFF0000
        Case "BLUE4"
            tmpColor = &HC00000
        Case "BLUE5"
            tmpColor = &H800000
        Case "BLUE6"
            tmpColor = &H400000
        Case "MAGENTA1"
            tmpColor = &HFFC0FF
        Case "MAGENTA2"
            tmpColor = &HFF80FF
        Case "MAGENTA3"
            tmpColor = &HFF00FF
        Case "MAGENTA4"
            tmpColor = &HC000C0
        Case "MAGENTA5"
            tmpColor = &H800080
        Case "MAGENTA6"
            tmpColor = &H400040
        Case "DARKBLUE"
            tmpColor = &HC00000
        Case "NORMALBLUE", "BLUE"
            tmpColor = vbBlue
        Case "LIGHTBLUE"
            tmpColor = &HFF0000
        Case "LIGHTGREY", "GREY"
            tmpColor = LightGray
        Case "NORMALGREY", "GRAY"
            tmpColor = NormalGray
        Case "DARKGREY"
            tmpColor = DarkGray
        Case "YELLOW"
            tmpColor = vbYellow
        Case "DARKAMBER"
            tmpColor = &H40C0&
        Case "LIGHTAMBER"
            tmpColor = &H80FF&
        Case "DARKRED"
            tmpColor = &HC0&
        Case "RED"
            tmpColor = &HFF&
        Case "DARKGREEN"
            tmpColor = &H80FF80
        Case "LIGHTGREEN"
            tmpColor = &HC0FFC0
        Case "DARKYELLOW"
            tmpColor = &H80FFFF
        Case "LIGHTYELLOW"
            tmpColor = &HC0FFFF
        Case "DARK"
            tmpColor = &HC00000
        Case "LIGHT"
            tmpColor = &HFF0000
        Case "WHITE"
            tmpColor = vbWhite
        Case Else
            tmpColor = vbBlack
    End Select
    TranslateColor = tmpColor
End Function

Public Sub SearchInTabList(CurTab As TABLib.TAB, ColNo As Long, CurText As String, NewText As Boolean, KeyCols As String, KeyValues As String)
    Dim HitLst As String
    Dim HitRow As Long
    Dim tmpText As String
    Dim LoopCount As Integer
    Dim KeyColNbr As String
    Dim KeyValue As String
    Dim tmpData As String
    Dim CheckIt As Boolean
    Dim KeyFound As Boolean
    Dim LineNo As Long
    Dim KeyCol As Long
    Dim i As Integer
    tmpText = Trim(CurText)
    If tmpText <> "" Then
        CheckIt = True
        If NewText = True Then
            LineNo = CurTab.GetCurrentSelected
            If LineNo >= 0 Then
                tmpData = CurTab.GetColumnValue(LineNo, ColNo)
                If InStr(tmpData, tmpText) > 0 Then CheckIt = False
            End If
        End If
        If CheckIt Then
            LoopCount = 0
            If KeyCols = "" Then
                Do
                    HitLst = CurTab.GetNextLineByColumnValue(ColNo, tmpText, 1)
                    If HitLst <> "" Then
                        HitRow = Val(HitLst)
                        CurTab.OnVScrollTo HitRow
                        CurTab.SetCurrentSelection HitRow
                        LoopCount = LoopCount + 1
                    Else
                        HitRow = -1
                        CurTab.SetCurrentSelection HitRow
                        LoopCount = LoopCount + 1
                    End If
                Loop While LoopCount < 2 And HitLst = ""
            Else
                SyncCursorBusy = True
                Do
                    HitLst = CurTab.GetNextLineByColumnValue(ColNo, tmpText, 1)
                    If HitLst <> "" Then
                        HitRow = Val(HitLst)
                        CurTab.OnVScrollTo HitRow
                        KeyFound = True
                        KeyCol = -1
                        i = 0
                        Do
                            i = i + 1
                            KeyColNbr = GetItem(KeyCols, i, ",")
                            If KeyColNbr <> "" Then
                                KeyValue = GetItem(KeyValues, i, ",")
                                KeyCol = Val(KeyColNbr)
                                tmpData = CurTab.GetColumnValue(HitRow, KeyCol)
                                If InStr(tmpData, KeyValue) <= 0 Then KeyFound = False
                            End If
                        Loop While (KeyFound = True) And (KeyColNbr <> "")
                        CurTab.SetCurrentSelection HitRow
                        If KeyFound = True Then LoopCount = 2
                        HitLst = ""
                    Else
                        HitRow = -1
                        CurTab.SetCurrentSelection HitRow
                        LoopCount = LoopCount + 1
                    End If
                Loop While LoopCount < 2 And HitLst = ""
                SyncCursorBusy = False
                CurTab.SetCurrentSelection HitRow
            End If
            CurTab.Refresh
        End If
    End If
End Sub

Public Function keyCheck() As Integer
    Dim tmpMsg As String
    If GetKeyState(vbKeyTab) < 0 Then
        keyCheck = vbKeyTab
    ElseIf GetKeyState(vbKeyLeft) < 0 Then
        keyCheck = vbKeyLeft
    ElseIf GetKeyState(vbKeyRight) < 0 Then
        keyCheck = vbKeyRight
    ElseIf GetKeyState(vbKeyUp) < 0 Then
        keyCheck = vbKeyUp
    ElseIf GetKeyState(vbKeyDown) < 0 Then
        keyCheck = vbKeyDown
    End If
End Function


Public Function CreateEmptyLine(FieldList As String) As String
    Dim Result As String
    Dim ItemNo As Long
    Dim FldNam As String
    Result = ""
    ItemNo = 1
    FldNam = GetRealItem(FieldList, ItemNo, ",")
    While FldNam <> ""
        Result = Result & ","
        ItemNo = ItemNo + 1
        FldNam = GetRealItem(FieldList, ItemNo, ",")
    Wend
    CreateEmptyLine = Result
End Function

Public Function DrawBackGround(MyPanel As PictureBox, MyColor As Integer, DrawHoriz As Boolean, DrawDown As Boolean) As Long
    Const intBLUESTART% = 255
    Const intBLUEEND% = 0
    Const intBANDHEIGHT% = 15
    Const intRed% = 1
    Const intGreen% = 2
    Const intBlue% = 4
    Const intBackRed% = 8
    Const intBackGreen% = 16
    Const intBackBlue% = 32
    Dim sngBlueCur As Single
    Dim sngBlueStep As Single
    Dim intFormHeight As Single
    Dim intFormWidth As Single
    Dim intX As Single
    Dim intY As Single
    Dim iColor As Integer
    Dim iRed As Single, iBlue As Single, iGreen As Single
    Dim ReturnColor As Long
    ReturnColor = vbWhite
    If MyColor >= 0 Then
        intFormHeight = MyPanel.ScaleHeight
        intFormWidth = MyPanel.ScaleWidth
    
        iColor = MyColor
        sngBlueCur = intBLUESTART
    
        If DrawDown Then
            If DrawHoriz Then
                sngBlueStep = intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormHeight
                For intY = 0 To intFormHeight Step intBANDHEIGHT
                    If iColor And intBlue Then iBlue = sngBlueCur
                    If iColor And intRed Then iRed = sngBlueCur
                    If iColor And intGreen Then iGreen = sngBlueCur
                    If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
                    If iColor And intBackRed Then iRed = 255 - sngBlueCur
                    If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
                    MyPanel.Line (-1, intY - 1)-(intFormWidth, intY + intBANDHEIGHT), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep
                    If intY = 0 Then ReturnColor = RGB(iRed, iGreen, iBlue)
                Next intY
            Else
                sngBlueStep = intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormWidth
                For intX = 0 To intFormWidth Step intBANDHEIGHT
                    If iColor And intBlue Then iBlue = sngBlueCur
                    If iColor And intRed Then iRed = sngBlueCur
                    If iColor And intGreen Then iGreen = sngBlueCur
                    If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
                    If iColor And intBackRed Then iRed = 255 - sngBlueCur
                    If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
                    MyPanel.Line (intX - 1, -1)-(intX + intBANDHEIGHT, intFormHeight), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep
                Next intX
            End If
        Else
            If DrawHoriz Then
                sngBlueStep = intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormHeight
                For intY = intFormHeight To 0 Step -intBANDHEIGHT
                    If iColor And intBlue Then iBlue = sngBlueCur
                    If iColor And intRed Then iRed = sngBlueCur
                    If iColor And intGreen Then iGreen = sngBlueCur
                    If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
                    If iColor And intBackRed Then iRed = 255 - sngBlueCur
                    If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
                    MyPanel.Line (-1, intY - 1)-(intFormWidth, intY + intBANDHEIGHT), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep
                Next intY
            Else
                sngBlueStep = intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormWidth
                For intX = intFormWidth To 0 Step -intBANDHEIGHT
                    If iColor And intBlue Then iBlue = sngBlueCur
                    If iColor And intRed Then iRed = sngBlueCur
                    If iColor And intGreen Then iGreen = sngBlueCur
                    If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
                    If iColor And intBackRed Then iRed = 255 - sngBlueCur
                    If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
                    MyPanel.Line (intX - 1, -1)-(intX + intBANDHEIGHT, intFormHeight), RGB(iRed, iGreen, iBlue), BF
                    sngBlueCur = sngBlueCur + sngBlueStep
                Next intX
            End If
        End If
    
    End If
    DrawBackGround = ReturnColor
End Function


Public Function CheckTimeValues(CheckTime As String, AsTimes As Boolean, IsReady As Boolean) As Boolean
    Dim TimeStrg As String
    Dim tmpStrg1 As String
    Dim tmpStrg2 As String
    Dim IsValid As Boolean
    Dim tmpHours As String
    Dim tmpMinute As String
    Dim valH As Integer
    Dim valM As Integer
    Dim chkH As String
    Dim chkM As String
    IsValid = False
    TimeStrg = CheckTime
    If TimeStrg = "" Then IsValid = True
    If Not IsValid Then
        If InStr(TimeStrg, ":") > 0 Then
            If Len(TimeStrg) > 3 Then
                tmpHours = GetRealItem(TimeStrg, 0, ":")
                If Len(tmpHours) < 2 Then tmpHours = Right("00" & tmpHours, 2)
                tmpMinute = GetRealItem(TimeStrg, 1, ":")
                If Len(tmpMinute) < 2 Then tmpMinute = Right("00" & tmpMinute, 2)
                IsValid = True
            End If
        Else
            If Len(TimeStrg) > 2 Then
                tmpMinute = Right(TimeStrg, 2)
                tmpHours = Left(TimeStrg, Len(TimeStrg) - 2)
                IsValid = True
            End If
        End If
        If IsValid Then
            If Len(tmpHours) <> 2 Then IsValid = False
            If Len(tmpMinute) <> 2 Then IsValid = False
            valH = Val(tmpHours)
            chkH = Right("00" & CStr(valH), 2)
            valM = Val(tmpMinute)
            chkM = Right("00" & CStr(valM), 2)
            If chkH <> tmpHours Then IsValid = False
            If chkM <> tmpMinute Then IsValid = False
            If valM > 59 Then IsValid = False
            If (AsTimes) And valH > 23 Then IsValid = False
            If IsValid Then TimeStrg = chkH & ":" & chkM
        End If
    End If
    If IsValid Then
        tmpStrg1 = Replace(CheckTime, ":", "", 1, -1, vbBinaryCompare)
        tmpStrg2 = Replace(TimeStrg, ":", "", 1, -1, vbBinaryCompare)
        If tmpStrg1 = tmpStrg2 Then CheckTime = TimeStrg
    End If
    CheckTimeValues = IsValid
End Function



