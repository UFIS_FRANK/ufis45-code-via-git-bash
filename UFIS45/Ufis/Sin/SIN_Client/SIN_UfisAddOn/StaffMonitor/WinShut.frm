VERSION 5.00
Begin VB.Form WinShut 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Terminate Windows"
   ClientHeight    =   3810
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   2910
   Icon            =   "WinShut.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3810
   ScaleWidth      =   2910
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CheckBox chkExit 
      Caption         =   "Exit"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   1
      Left            =   1500
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   2850
      Width           =   1035
   End
   Begin VB.CheckBox ChkCfg 
      Caption         =   "Close this Window"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Index           =   0
      Left            =   390
      Style           =   1  'Graphical
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   3330
      Width           =   2145
   End
   Begin VB.CheckBox chkExit 
      Caption         =   "OK"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   0
      Left            =   390
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   2850
      Width           =   1035
   End
   Begin VB.Frame Frame2 
      Caption         =   "Add Parameter"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   915
      Left            =   360
      TabIndex        =   5
      Top             =   1770
      Width           =   2175
      Begin VB.CheckBox Check1 
         Caption         =   "Force Function"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   120
         TabIndex        =   7
         Top             =   270
         Value           =   1  'Checked
         Width           =   1695
      End
      Begin VB.CheckBox Check2 
         Caption         =   "Force if Hung"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   120
         TabIndex        =   6
         Top             =   540
         Width           =   1695
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Desired Feature"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1425
      Left            =   360
      TabIndex        =   0
      Top             =   210
      Width           =   2205
      Begin VB.OptionButton Option1 
         Caption         =   "Logoff"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   180
         TabIndex        =   4
         Top             =   270
         Width           =   1395
      End
      Begin VB.OptionButton Option2 
         Caption         =   "Shutdown"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   180
         TabIndex        =   3
         Top             =   540
         Width           =   1395
      End
      Begin VB.OptionButton Option3 
         Caption         =   "Reboot"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   180
         TabIndex        =   2
         Top             =   810
         Width           =   1395
      End
      Begin VB.OptionButton Option4 
         Caption         =   "Power Off"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Left            =   180
         TabIndex        =   1
         Top             =   1080
         Value           =   -1  'True
         Width           =   1395
      End
   End
End
Attribute VB_Name = "WinShut"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit



''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Copyright �1996-2009 VBnet, Randy Birch, All Rights Reserved.
' Some pages may also contain other copyrights by the author.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' Distribution: You can freely use this code in your own
'               applications, but you may not reproduce
'               or publish this code on any web site,
'               online service, or distribute as source
'               on any media without express permission.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Private Const TOKEN_ADJUST_PRIVILEGES As Long = &H20
Private Const TOKEN_QUERY As Long = &H8
Private Const SE_PRIVILEGE_ENABLED As Long = &H2

Private Const EWX_LOGOFF As Long = &H0
Private Const EWX_SHUTDOWN As Long = &H1
Private Const EWX_REBOOT As Long = &H2
Private Const EWX_FORCE As Long = &H4
Private Const EWX_POWEROFF As Long = &H8
Private Const EWX_FORCEIFHUNG As Long = &H10 '2000/XP only

Private Const VER_PLATFORM_WIN32_NT As Long = 2

Private Type OSVERSIONINFO
  OSVSize         As Long
  dwVerMajor      As Long
  dwVerMinor      As Long
  dwBuildNumber   As Long
  PlatformID      As Long
  szCSDVersion    As String * 128
End Type

Private Type LUID
   dwLowPart As Long
   dwHighPart As Long
End Type

Private Type LUID_AND_ATTRIBUTES
   udtLUID As LUID
   dwAttributes As Long
End Type

Private Type TOKEN_PRIVILEGES
   PrivilegeCount As Long
   laa As LUID_AND_ATTRIBUTES
End Type
      
Private Declare Function ExitWindowsEx Lib "user32" _
   (ByVal dwOptions As Long, _
   ByVal dwReserved As Long) As Long

Private Declare Function GetCurrentProcess Lib "kernel32" () As Long

Private Declare Function OpenProcessToken Lib "advapi32" _
  (ByVal ProcessHandle As Long, _
   ByVal DesiredAccess As Long, _
   TokenHandle As Long) As Long

Private Declare Function LookupPrivilegeValue Lib "advapi32" _
   Alias "LookupPrivilegeValueA" _
  (ByVal lpSystemName As String, _
   ByVal lpName As String, _
   lpLuid As LUID) As Long

Private Declare Function AdjustTokenPrivileges Lib "advapi32" _
  (ByVal TokenHandle As Long, _
   ByVal DisableAllPrivileges As Long, _
   NewState As TOKEN_PRIVILEGES, _
   ByVal BufferLength As Long, _
   PreviousState As Any, _
   ReturnLength As Long) As Long

Private Declare Function GetVersionEx Lib "kernel32" _
   Alias "GetVersionExA" _
  (lpVersionInformation As OSVERSIONINFO) As Long
  
Public Sub SetExitMode(UseMode As String)
    Dim tmpTag As String
    tmpTag = "EXIT"
    Select Case UseMode
        Case "LOGOFF"
            Option1.Value = True
            tmpTag = "SHUT"
        Case "SHUTDOWN"
            Option2.Value = True
            tmpTag = "SHUT"
        Case "REBOOT"
            Option3.Value = True
            tmpTag = "SHUT"
        Case "POWEROFF"
            Option4.Value = True
            tmpTag = "SHUT"
        Case "EXIT"
        Case "TERMINATE"
            tmpTag = Me.Tag
            Select Case tmpTag
                Case "EXIT"
                    End
                Case "SHUT"
                    ShutMeDown
                Case Else
                    End
            End Select
        Case Else
    End Select
    Me.Tag = tmpTag
End Sub

Private Sub ShutMeDown()

   Dim uFlags As Long
   Dim success As Long
   
    WinShutDown = True
        
   If Option1.Value = True Then uFlags = EWX_LOGOFF
   If Option2.Value = True Then uFlags = EWX_SHUTDOWN
   If Option3.Value = True Then uFlags = EWX_REBOOT
   If Option4.Value = True Then uFlags = EWX_POWEROFF
   
   If Check1.Value = vbChecked Then uFlags = uFlags Or EWX_FORCE
   If Check2.Value = vbChecked Then uFlags = uFlags Or EWX_FORCEIFHUNG
     
  'if running under NT or better,
  'the shutdown privileges need to
  'be adjusted to allow the ExitWindowsEx
  'call. If the adjust call fails on a NT+
  'system, success holds False, preventing shutdown.
   If IsWinNTPlus() Then
   
      success = EnableShutdownPrivledges()
      If success Then Call ExitWindowsEx(uFlags, 0&)
         
   Else
   
     '9x system, so just do it
      Call ExitWindowsEx(uFlags, 0&)
      
   End If

End Sub
  
  
  
Private Sub ChkCfg_Click(Index As Integer)
    Unload Me
End Sub


Private Sub chkExit_Click(Index As Integer)
    Dim tmpTag As String
    If chkExit(Index).Value = 1 Then
        chkExit(Index).Value = 0
        Select Case Index
            Case 0
                ShutMeDown
            Case 1
                tmpTag = Me.Tag
                If tmpTag = "SHUT" Then
                    ShutMeDown
                Else
                End If
            Case Else
        End Select
    End If
End Sub


Private Function IsWinNTPlus() As Boolean

  'returns True if running Windows NT,
  'Windows 2000, Windows XP, or .net server
   #If Win32 Then
  
      Dim OSV As OSVERSIONINFO
   
      OSV.OSVSize = Len(OSV)
   
      If GetVersionEx(OSV) = 1 Then

         IsWinNTPlus = (OSV.PlatformID = VER_PLATFORM_WIN32_NT) And _
                       (OSV.dwVerMajor >= 4)
      End If

   #End If

End Function


Private Function EnableShutdownPrivledges() As Boolean

   Dim hProcessHandle As Long
   Dim hTokenHandle As Long
   Dim lpv_la As LUID
   Dim token As TOKEN_PRIVILEGES
   
   hProcessHandle = GetCurrentProcess()
   
   If hProcessHandle <> 0 Then
   
     'open the access token associated
     'with the current process. hTokenHandle
     'returns a handle identifying the
     'newly-opened access token
      If OpenProcessToken(hProcessHandle, _
                        (TOKEN_ADJUST_PRIVILEGES Or TOKEN_QUERY), _
                         hTokenHandle) <> 0 Then
   
        'obtain the locally unique identifier
        '(LUID) used on the specified system
        'to locally represent the specified
        'privilege name. Passing vbNullString
        'causes the api to attempt to find
        'the privilege name on the local system.
         If LookupPrivilegeValue(vbNullString, _
                                 "SeShutdownPrivilege", _
                                 lpv_la) <> 0 Then
         
           'TOKEN_PRIVILEGES contains info about
           'a set of privileges for an access token.
           'Prepare the TOKEN_PRIVILEGES structure
           'by enabling one privilege.
            With token
               .PrivilegeCount = 1
               .laa.udtLUID = lpv_la
               .laa.dwAttributes = SE_PRIVILEGE_ENABLED
            End With
   
           'Enable the shutdown privilege in
           'the access token of this process.
           'hTokenHandle: access token containing the
           '  privileges to be modified
           'DisableAllPrivileges: if True the function
           '  disables all privileges and ignores the
           '  NewState parameter. If FALSE, the
           '  function modifies privileges based on
           '  the information pointed to by NewState.
           'token: TOKEN_PRIVILEGES structure specifying
           '  an array of privileges and their attributes.
           '
           'Since were just adjusting to shut down,
           'BufferLength, PreviousState and ReturnLength
           'can be passed as null.
            If AdjustTokenPrivileges(hTokenHandle, _
                                     False, _
                                     token, _
                                     ByVal 0&, _
                                     ByVal 0&, _
                                     ByVal 0&) <> 0 Then
                                     
              'success, so return True
               EnableShutdownPrivledges = True
   
            End If  'AdjustTokenPrivileges
         End If  'LookupPrivilegeValue
      End If  'OpenProcessToken
   End If  'hProcessHandle

End Function


