VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "tab.ocx"
Begin VB.Form MyKeyCodes 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Staff Monitor Configuration"
   ClientHeight    =   7890
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   14460
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "MyKeyCodes.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7890
   ScaleWidth      =   14460
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Tag             =   "7305,7860"
   Begin VB.CheckBox ChkCfg 
      Caption         =   "Key Codes"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   0
      Left            =   0
      Style           =   1  'Graphical
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   0
      Width           =   1095
   End
   Begin VB.PictureBox CfgPanel 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3675
      Index           =   0
      Left            =   30
      ScaleHeight     =   3615
      ScaleWidth      =   4935
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   3330
      Visible         =   0   'False
      Width           =   4995
      Begin TABLib.TAB CfgTab 
         Height          =   2055
         Index           =   0
         Left            =   0
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   510
         Width           =   4905
         _Version        =   65536
         _ExtentX        =   8652
         _ExtentY        =   3625
         _StockProps     =   64
      End
      Begin VB.Image CfgIcon 
         Height          =   480
         Index           =   0
         Left            =   810
         Picture         =   "MyKeyCodes.frx":0442
         Top             =   2250
         Visible         =   0   'False
         Width           =   480
      End
      Begin VB.Label CfgCaption 
         Caption         =   "Keyboard Configuration"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   0
         Left            =   240
         TabIndex        =   3
         Top             =   2940
         Visible         =   0   'False
         Width           =   405
      End
   End
   Begin VB.Image MyIcon 
      Height          =   480
      Index           =   1
      Left            =   630
      Picture         =   "MyKeyCodes.frx":0884
      Top             =   750
      Width           =   480
   End
   Begin VB.Image MyIcon 
      Height          =   480
      Index           =   0
      Left            =   30
      Picture         =   "MyKeyCodes.frx":0CC6
      Top             =   720
      Visible         =   0   'False
      Width           =   480
   End
End
Attribute VB_Name = "MyKeyCodes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim CfgIdx As Integer
Dim AnyIdx As Integer
Dim MyCaption As String

Public Function GetKeyCodeMean(KeyCode As String) As String
    Dim HitLine As String
    Dim LineCnt As Long
    Dim LineNo As Long
    Dim KeyMean As String
    CfgTab(0).SetInternalLineBuffer True
    HitLine = CfgTab(0).GetLinesByColumnValue(1, KeyCode, 0)
    LineCnt = Val(HitLine)
    If LineCnt = 1 Then
        LineNo = CfgTab(0).GetNextResultLine
        If LineNo >= 0 Then
            KeyMean = CfgTab(0).GetColumnValue(LineNo, 0)
        End If
    ElseIf LineCnt = 0 Then
        KeyMean = "NOK"
    Else
        KeyMean = "CNT"
    End If
    CfgTab(0).SetInternalLineBuffer False
    GetKeyCodeMean = KeyMean
End Function

Private Sub InitCfgTab(Index As Integer)
    Dim tmpRec As String
    Dim i As Integer
    Dim j As Integer
    i = Index
    Select Case Index
        Case 0
            CfgTab(i).ResetContent
            CfgTab(i).HeaderString = "FUNC,KEYS,W,REMARK" & Space(100)
            CfgTab(i).HeaderLengthString = "50,50,50,50"
            CfgTab(i).ColumnAlignmentString = "L,L,C,L"
            CfgTab(i).HeaderAlignmentString = CfgTab(i).ColumnAlignmentString
            CfgTab(i).FontName = "Arial"
            CfgTab(i).SetTabFontBold True
            CfgTab(i).LineHeight = 20
            CfgTab(i).FontSize = CfgTab(i).LineHeight - 2
            CfgTab(i).HeaderFontSize = CfgTab(i).FontSize
            CfgTab(i).DisplayBackColor = vbBlue
            CfgTab(i).DisplayTextColor = vbWhite
            CfgTab(i).SelectBackColor = vbBlack
            CfgTab(i).SelectTextColor = vbYellow
            CfgTab(i).EmptyAreaBackColor = CfgTab(i).DisplayBackColor
            CfgTab(i).ShowVertScroller False
            CfgTab(i).AutoSizeByHeader = True
            CfgTab(i).LifeStyle = True
            tmpRec = ""
            tmpRec = tmpRec & "PGDN,+,N,Page Down (Next Screen)" & vbNewLine
            tmpRec = tmpRec & "PGUP,-,N,Page Up (Previous Screen)" & vbNewLine
            tmpRec = tmpRec & "MSGW,//,Y,Open/Close Info Objects" & vbNewLine
            tmpRec = tmpRec & "MSG0,//0,N,Open/Close Update Monitor" & vbNewLine
            tmpRec = tmpRec & "MSG1,//1,N,Open/Close Message Details 1" & vbNewLine
            tmpRec = tmpRec & "MSG2,//2,N,Open/Close Message Details 2" & vbNewLine
            tmpRec = tmpRec & "MSG3,//3,N,Open/Close Message Details 3" & vbNewLine
            tmpRec = tmpRec & "MSG4,//4,N,Open/Close Message Details 4" & vbNewLine
            tmpRec = tmpRec & "MSG5,//5,N,Open/Close Message Details 5" & vbNewLine
            tmpRec = tmpRec & "EXIT,.0.,N,Exit Application" & vbNewLine
            tmpRec = tmpRec & "CLS,.00,N,Close Focus Window" & vbNewLine
            tmpRec = tmpRec & "KBD,.01,N,Show Keyboard Configuration" & vbNewLine
            tmpRec = tmpRec & "RPT,..,N,Repeat Last Function" & vbNewLine
            CfgTab(i).InsertBuffer tmpRec, vbNewLine
            CfgTab(i).AutoSizeColumns
        Case Else
    End Select
End Sub

Private Sub ChkCfg_Click(Index As Integer)
    On Error Resume Next
    If ChkCfg(Index).Value = 1 Then
        If CfgIdx >= 0 Then ChkCfg(CfgIdx).Value = 0
        CfgIdx = Index
        Me.Caption = CfgCaption(CfgIdx)
        Me.Icon = CfgIcon(CfgIdx)
        ChkCfg(CfgIdx).BackColor = LightYellow
        CfgPanel(CfgIdx).Visible = True
        Select Case Index
            Case Else
        End Select
        Form_Resize
        CfgTab(CfgIdx).SetFocus
    Else
        If CfgIdx >= 0 Then
            CfgPanel(CfgIdx).Visible = False
            ChkCfg(CfgIdx).BackColor = vbButtonFace
        End If
        Me.Caption = MyCaption
        Me.Icon = MyIcon(0)
        CfgIdx = -1
    End If
End Sub

Private Sub Form_Activate()
    If CfgIdx < 0 Then
        MyCaption = Me.Caption
        ChkCfg(0).Value = 1
    End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    MainForm.KeyCodeInput KeyCode, Shift, 0, ""
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    MainForm.KeyCodeInput KeyAscii, 0, 3, ""
End Sub

Private Sub Form_Load()
    Dim tmpTag As String
    tmpTag = Me.Tag
    If tmpTag = "" Then tmpTag = "7305,7860"
    Me.Width = Val(GetItem(tmpTag, 1, ","))
    Me.Height = Val(GetItem(tmpTag, 2, ","))
    CfgIdx = -1
    AnyIdx = -1
    Me.Icon = MyIcon(0)
    InitCfgTab 0
    InitCfgTab 1
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode >= 0 Then
        Cancel = False
        Me.Hide
    End If
End Sub

Private Sub Form_Resize()
    Dim NewTop As Long
    Dim NewSize As Long
    If CfgIdx >= 0 Then
        NewSize = Me.ScaleWidth
        NewTop = ChkCfg(0).Top + ChkCfg(0).Height + 30
        CfgPanel(CfgIdx).Left = 0
        CfgPanel(CfgIdx).Top = NewTop
        CfgPanel(CfgIdx).Width = NewSize
        NewSize = Me.ScaleHeight - NewTop
        CfgPanel(CfgIdx).Height = NewSize
        CfgTab(CfgIdx).Top = 0
        CfgTab(CfgIdx).Left = 0
        Select Case CfgIdx
            Case 0
                NewSize = CfgPanel(CfgIdx).ScaleWidth
                CfgTab(CfgIdx).Width = NewSize
                NewSize = CfgPanel(CfgIdx).ScaleHeight
                CfgTab(CfgIdx).Height = NewSize
            Case Else
                NewSize = CfgPanel(CfgIdx).ScaleWidth
                CfgTab(CfgIdx).Width = NewSize
                NewSize = CfgPanel(CfgIdx).ScaleHeight
                CfgTab(CfgIdx).Height = NewSize
        End Select
    End If
End Sub

Private Sub CfgTab_HitKeyOnLine(Index As Integer, ByVal Key As Integer, ByVal LineNo As Long)
    MainForm.KeyCodeInput Key, 0, 1, ""
End Sub

