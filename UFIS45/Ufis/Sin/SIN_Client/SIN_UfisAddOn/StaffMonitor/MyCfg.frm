VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "tab.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Begin VB.Form MyCfg 
   Caption         =   "Staff Monitor Configuration"
   ClientHeight    =   7995
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   12075
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "MyCfg.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   7995
   ScaleWidth      =   12075
   StartUpPosition =   1  'CenterOwner
   Tag             =   "7305,7920"
   Begin VB.PictureBox AnyPanel 
      BackColor       =   &H000040C0&
      Height          =   7125
      Index           =   0
      Left            =   10680
      ScaleHeight     =   7065
      ScaleWidth      =   255
      TabIndex        =   120
      TabStop         =   0   'False
      Top             =   330
      Visible         =   0   'False
      Width           =   315
      Begin VB.Frame IniFrame 
         Caption         =   "Actual Page"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2445
         Index           =   0
         Left            =   3330
         TabIndex        =   252
         Top             =   3030
         Visible         =   0   'False
         Width           =   3165
         Begin VB.PictureBox IniTabPanel 
            Height          =   1425
            Index           =   0
            Left            =   1110
            ScaleHeight     =   1365
            ScaleWidth      =   4755
            TabIndex        =   265
            Top             =   270
            Width           =   4815
            Begin TABLib.TAB IniActTab 
               Height          =   765
               Index           =   0
               Left            =   0
               TabIndex        =   266
               TabStop         =   0   'False
               Top             =   0
               Width           =   3015
               _Version        =   65536
               _ExtentX        =   5318
               _ExtentY        =   1349
               _StockProps     =   64
            End
         End
         Begin VB.PictureBox MsgPanel 
            Height          =   1425
            Index           =   9
            Left            =   120
            ScaleHeight     =   1365
            ScaleWidth      =   885
            TabIndex        =   260
            Top             =   270
            Width           =   945
            Begin VB.CheckBox ChkType 
               Caption         =   "Get MSG"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   39
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   264
               TabStop         =   0   'False
               Top             =   1035
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               Caption         =   "Get WKS"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   38
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   263
               TabStop         =   0   'False
               Top             =   690
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               Caption         =   "Open"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   37
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   262
               TabStop         =   0   'False
               Top             =   0
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               Caption         =   "Create"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   36
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   261
               TabStop         =   0   'False
               Top             =   345
               Width           =   885
            End
         End
         Begin VB.PictureBox IniBtnPanel 
            Height          =   1425
            Index           =   0
            Left            =   5970
            ScaleHeight     =   1365
            ScaleWidth      =   885
            TabIndex        =   255
            Top             =   270
            Width           =   945
            Begin VB.CheckBox ChkType 
               Caption         =   "Undo"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   35
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   259
               TabStop         =   0   'False
               Top             =   345
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               Caption         =   "Save"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   34
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   258
               TabStop         =   0   'False
               Top             =   0
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               Caption         =   "Deploy"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   33
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   257
               TabStop         =   0   'False
               Top             =   690
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               Caption         =   "Submit"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   32
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   256
               TabStop         =   0   'False
               Top             =   1035
               Width           =   885
            End
         End
         Begin VB.PictureBox IniCapPanel 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   0
            Left            =   120
            ScaleHeight     =   285
            ScaleWidth      =   6735
            TabIndex        =   253
            TabStop         =   0   'False
            Top             =   1770
            Width           =   6795
            Begin VB.CheckBox CurPageNoRgt 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   360
               Index           =   0
               Left            =   810
               Style           =   1  'Graphical
               TabIndex        =   307
               TabStop         =   0   'False
               Top             =   0
               Visible         =   0   'False
               Width           =   990
            End
            Begin VB.TextBox IniInfoTitle 
               Alignment       =   2  'Center
               BackColor       =   &H8000000F&
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   0
               Left            =   480
               TabIndex        =   254
               Text            =   "Text1"
               Top             =   0
               Width           =   6735
            End
         End
      End
      Begin VB.TextBox AnyText 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   465
         Index           =   0
         Left            =   3990
         MultiLine       =   -1  'True
         ScrollBars      =   1  'Horizontal
         TabIndex        =   249
         Top             =   6510
         Visible         =   0   'False
         Width           =   945
      End
      Begin VB.CheckBox chkAdv 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   3
         Left            =   6075
         Style           =   1  'Graphical
         TabIndex        =   219
         TabStop         =   0   'False
         Top             =   855
         Width           =   1035
      End
      Begin VB.CheckBox chkAdv 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   2
         Left            =   3360
         Style           =   1  'Graphical
         TabIndex        =   218
         TabStop         =   0   'False
         Top             =   855
         Width           =   1035
      End
      Begin VB.CheckBox chkAdv 
         Caption         =   "Save"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   1
         Left            =   4620
         Style           =   1  'Graphical
         TabIndex        =   217
         TabStop         =   0   'False
         Top             =   6660
         Width           =   1035
      End
      Begin VB.CheckBox chkAdv 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   0
         Left            =   4410
         Style           =   1  'Graphical
         TabIndex        =   216
         TabStop         =   0   'False
         Top             =   855
         Width           =   1650
      End
      Begin VB.Frame AnyFrame 
         Height          =   5505
         Index           =   2
         Left            =   3360
         TabIndex        =   215
         Top             =   1110
         Width           =   3765
      End
      Begin VB.Frame AnyFrame 
         Height          =   825
         Index           =   1
         Left            =   3360
         TabIndex        =   214
         Top             =   0
         Width           =   3765
      End
      Begin VB.Frame AnyFrame 
         Height          =   7005
         Index           =   0
         Left            =   60
         TabIndex        =   121
         Top             =   0
         Width           =   3255
         Begin VB.CheckBox chkMainLayout 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Top Button Panel"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   1
            Left            =   300
            TabIndex        =   203
            TabStop         =   0   'False
            Top             =   1140
            Width           =   2400
         End
         Begin VB.CheckBox chkMainLayout 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Unit Panel"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   0
            Left            =   300
            TabIndex        =   202
            TabStop         =   0   'False
            Top             =   510
            Width           =   2400
         End
         Begin VB.PictureBox picDummy 
            BorderStyle     =   0  'None
            Height          =   195
            Index           =   0
            Left            =   510
            ScaleHeight     =   195
            ScaleWidth      =   2640
            TabIndex        =   196
            TabStop         =   0   'False
            Top             =   840
            Width           =   2640
            Begin VB.PictureBox L1Color 
               BackColor       =   &H00FFFFFF&
               Height          =   195
               Index           =   1
               Left            =   810
               ScaleHeight     =   135
               ScaleWidth      =   135
               TabIndex        =   201
               TabStop         =   0   'False
               Top             =   0
               Width           =   195
            End
            Begin VB.PictureBox L2Color 
               BackColor       =   &H00000000&
               Height          =   195
               Index           =   1
               Left            =   1740
               ScaleHeight     =   135
               ScaleWidth      =   135
               TabIndex        =   200
               TabStop         =   0   'False
               Top             =   0
               Width           =   195
            End
            Begin VB.CheckBox chkWSpace 
               Caption         =   "Line"
               Height          =   210
               Index           =   1
               Left            =   1020
               TabIndex        =   199
               TabStop         =   0   'False
               Top             =   0
               Width           =   690
            End
            Begin VB.CheckBox chkBSpace 
               Caption         =   "Line"
               Height          =   210
               Index           =   1
               Left            =   1950
               TabIndex        =   198
               TabStop         =   0   'False
               Top             =   0
               Width           =   660
            End
            Begin VB.CheckBox chkSwap 
               Caption         =   "Swap"
               Height          =   210
               Index           =   1
               Left            =   0
               TabIndex        =   197
               TabStop         =   0   'False
               Top             =   0
               Width           =   780
            End
         End
         Begin VB.CheckBox chkMainLayout 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Bottom Button Panel"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   8
            Left            =   300
            TabIndex        =   195
            TabStop         =   0   'False
            Top             =   6390
            Width           =   2400
         End
         Begin VB.CheckBox chkMainLayout 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Multi Function Panel"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   7
            Left            =   300
            TabIndex        =   194
            TabStop         =   0   'False
            Top             =   5760
            Width           =   2400
         End
         Begin VB.CheckBox chkMainLayout 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Data Grid"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   5
            Left            =   300
            TabIndex        =   193
            TabStop         =   0   'False
            Top             =   4920
            Width           =   2400
         End
         Begin VB.CheckBox chkMainLayout 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Page Title"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   4
            Left            =   300
            TabIndex        =   192
            TabStop         =   0   'False
            Top             =   4290
            Width           =   2400
         End
         Begin VB.CheckBox chkMainLayout 
            BackColor       =   &H00E0E0E0&
            Caption         =   "News Panel 16"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   6
            Left            =   300
            TabIndex        =   191
            TabStop         =   0   'False
            Top             =   3660
            Width           =   2400
         End
         Begin VB.CheckBox chkMainLayout 
            BackColor       =   &H00E0E0E0&
            Caption         =   "News Panel 32"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   9
            Left            =   300
            TabIndex        =   190
            TabStop         =   0   'False
            Top             =   3030
            Width           =   2400
         End
         Begin VB.CheckBox chkMainLayout 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Message Panel"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   3
            Left            =   300
            TabIndex        =   189
            TabStop         =   0   'False
            Top             =   2400
            Width           =   2400
         End
         Begin VB.CheckBox chkMainLayout 
            BackColor       =   &H00E0E0E0&
            Caption         =   "Info Title"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   240
            Index           =   2
            Left            =   300
            TabIndex        =   188
            TabStop         =   0   'False
            Top             =   1770
            Width           =   2400
         End
         Begin VB.PictureBox picDummy 
            BorderStyle     =   0  'None
            Height          =   195
            Index           =   1
            Left            =   510
            ScaleHeight     =   195
            ScaleWidth      =   2640
            TabIndex        =   182
            TabStop         =   0   'False
            Top             =   210
            Width           =   2640
            Begin VB.CheckBox chkSwap 
               Caption         =   "Swap"
               Height          =   210
               Index           =   0
               Left            =   0
               TabIndex        =   187
               TabStop         =   0   'False
               Top             =   0
               Width           =   780
            End
            Begin VB.CheckBox chkBSpace 
               Caption         =   "Line"
               Height          =   210
               Index           =   0
               Left            =   1950
               TabIndex        =   186
               TabStop         =   0   'False
               Top             =   0
               Width           =   660
            End
            Begin VB.CheckBox chkWSpace 
               Caption         =   "Line"
               Height          =   210
               Index           =   0
               Left            =   1020
               TabIndex        =   185
               TabStop         =   0   'False
               Top             =   0
               Width           =   690
            End
            Begin VB.PictureBox L2Color 
               BackColor       =   &H00000000&
               Height          =   195
               Index           =   0
               Left            =   1740
               ScaleHeight     =   135
               ScaleWidth      =   135
               TabIndex        =   184
               TabStop         =   0   'False
               Top             =   0
               Width           =   195
            End
            Begin VB.PictureBox L1Color 
               BackColor       =   &H00FFFFFF&
               Height          =   195
               Index           =   0
               Left            =   810
               ScaleHeight     =   135
               ScaleWidth      =   135
               TabIndex        =   183
               TabStop         =   0   'False
               Top             =   0
               Width           =   195
            End
         End
         Begin VB.PictureBox picDummy 
            BorderStyle     =   0  'None
            Height          =   195
            Index           =   2
            Left            =   510
            ScaleHeight     =   195
            ScaleWidth      =   2640
            TabIndex        =   176
            TabStop         =   0   'False
            Top             =   1470
            Width           =   2640
            Begin VB.CheckBox chkSwap 
               Caption         =   "Swap"
               Height          =   210
               Index           =   2
               Left            =   0
               TabIndex        =   181
               TabStop         =   0   'False
               Top             =   0
               Width           =   780
            End
            Begin VB.CheckBox chkBSpace 
               Caption         =   "Line"
               Height          =   210
               Index           =   2
               Left            =   1950
               TabIndex        =   180
               TabStop         =   0   'False
               Top             =   0
               Width           =   660
            End
            Begin VB.CheckBox chkWSpace 
               Caption         =   "Line"
               Height          =   210
               Index           =   2
               Left            =   1020
               TabIndex        =   179
               TabStop         =   0   'False
               Top             =   0
               Width           =   690
            End
            Begin VB.PictureBox L2Color 
               BackColor       =   &H00000000&
               Height          =   195
               Index           =   2
               Left            =   1740
               ScaleHeight     =   135
               ScaleWidth      =   135
               TabIndex        =   178
               TabStop         =   0   'False
               Top             =   0
               Width           =   195
            End
            Begin VB.PictureBox L1Color 
               BackColor       =   &H00FFFFFF&
               Height          =   195
               Index           =   2
               Left            =   810
               ScaleHeight     =   135
               ScaleWidth      =   135
               TabIndex        =   177
               TabStop         =   0   'False
               Top             =   0
               Width           =   195
            End
         End
         Begin VB.PictureBox picDummy 
            BorderStyle     =   0  'None
            Height          =   195
            Index           =   3
            Left            =   510
            ScaleHeight     =   195
            ScaleWidth      =   2640
            TabIndex        =   170
            TabStop         =   0   'False
            Top             =   2100
            Width           =   2640
            Begin VB.PictureBox L1Color 
               BackColor       =   &H00FFFFFF&
               Height          =   195
               Index           =   3
               Left            =   810
               ScaleHeight     =   135
               ScaleWidth      =   135
               TabIndex        =   175
               TabStop         =   0   'False
               Top             =   0
               Width           =   195
            End
            Begin VB.PictureBox L2Color 
               BackColor       =   &H00000000&
               Height          =   195
               Index           =   3
               Left            =   1740
               ScaleHeight     =   135
               ScaleWidth      =   135
               TabIndex        =   174
               TabStop         =   0   'False
               Top             =   0
               Width           =   195
            End
            Begin VB.CheckBox chkWSpace 
               Caption         =   "Line"
               Height          =   210
               Index           =   3
               Left            =   1020
               TabIndex        =   173
               TabStop         =   0   'False
               Top             =   0
               Width           =   690
            End
            Begin VB.CheckBox chkBSpace 
               Caption         =   "Line"
               Height          =   210
               Index           =   3
               Left            =   1950
               TabIndex        =   172
               TabStop         =   0   'False
               Top             =   0
               Width           =   660
            End
            Begin VB.CheckBox chkSwap 
               Caption         =   "Swap"
               Height          =   210
               Index           =   3
               Left            =   0
               TabIndex        =   171
               TabStop         =   0   'False
               Top             =   0
               Width           =   780
            End
         End
         Begin VB.PictureBox picDummy 
            BorderStyle     =   0  'None
            Height          =   195
            Index           =   4
            Left            =   510
            ScaleHeight     =   195
            ScaleWidth      =   2640
            TabIndex        =   164
            TabStop         =   0   'False
            Top             =   2730
            Width           =   2640
            Begin VB.PictureBox L1Color 
               BackColor       =   &H00FFFFFF&
               Height          =   195
               Index           =   4
               Left            =   810
               ScaleHeight     =   135
               ScaleWidth      =   135
               TabIndex        =   169
               TabStop         =   0   'False
               Top             =   0
               Width           =   195
            End
            Begin VB.PictureBox L2Color 
               BackColor       =   &H00000000&
               Height          =   195
               Index           =   4
               Left            =   1740
               ScaleHeight     =   135
               ScaleWidth      =   135
               TabIndex        =   168
               TabStop         =   0   'False
               Top             =   0
               Width           =   195
            End
            Begin VB.CheckBox chkWSpace 
               Caption         =   "Line"
               Height          =   210
               Index           =   4
               Left            =   1020
               TabIndex        =   167
               TabStop         =   0   'False
               Top             =   0
               Width           =   690
            End
            Begin VB.CheckBox chkBSpace 
               Caption         =   "Line"
               Height          =   210
               Index           =   4
               Left            =   1950
               TabIndex        =   166
               TabStop         =   0   'False
               Top             =   0
               Width           =   660
            End
            Begin VB.CheckBox chkSwap 
               Caption         =   "Swap"
               Height          =   210
               Index           =   4
               Left            =   0
               TabIndex        =   165
               TabStop         =   0   'False
               Top             =   0
               Width           =   780
            End
         End
         Begin VB.PictureBox picDummy 
            BorderStyle     =   0  'None
            Height          =   195
            Index           =   5
            Left            =   510
            ScaleHeight     =   195
            ScaleWidth      =   2640
            TabIndex        =   158
            TabStop         =   0   'False
            Top             =   3360
            Width           =   2640
            Begin VB.CheckBox chkSwap 
               Caption         =   "Swap"
               Height          =   210
               Index           =   5
               Left            =   0
               TabIndex        =   163
               TabStop         =   0   'False
               Top             =   0
               Width           =   780
            End
            Begin VB.CheckBox chkBSpace 
               Caption         =   "Line"
               Height          =   210
               Index           =   5
               Left            =   1950
               TabIndex        =   162
               TabStop         =   0   'False
               Top             =   0
               Width           =   660
            End
            Begin VB.CheckBox chkWSpace 
               Caption         =   "Line"
               Height          =   210
               Index           =   5
               Left            =   1020
               TabIndex        =   161
               TabStop         =   0   'False
               Top             =   0
               Width           =   690
            End
            Begin VB.PictureBox L2Color 
               BackColor       =   &H00000000&
               Height          =   195
               Index           =   5
               Left            =   1740
               ScaleHeight     =   135
               ScaleWidth      =   135
               TabIndex        =   160
               TabStop         =   0   'False
               Top             =   0
               Width           =   195
            End
            Begin VB.PictureBox L1Color 
               BackColor       =   &H00FFFFFF&
               Height          =   195
               Index           =   5
               Left            =   810
               ScaleHeight     =   135
               ScaleWidth      =   135
               TabIndex        =   159
               TabStop         =   0   'False
               Top             =   0
               Width           =   195
            End
         End
         Begin VB.PictureBox picDummy 
            BorderStyle     =   0  'None
            Height          =   195
            Index           =   6
            Left            =   510
            ScaleHeight     =   195
            ScaleWidth      =   2640
            TabIndex        =   152
            TabStop         =   0   'False
            Top             =   3990
            Width           =   2640
            Begin VB.CheckBox chkSwap 
               Caption         =   "Swap"
               Height          =   210
               Index           =   6
               Left            =   0
               TabIndex        =   157
               TabStop         =   0   'False
               Top             =   0
               Width           =   780
            End
            Begin VB.CheckBox chkBSpace 
               Caption         =   "Line"
               Height          =   210
               Index           =   6
               Left            =   1950
               TabIndex        =   156
               TabStop         =   0   'False
               Top             =   0
               Width           =   660
            End
            Begin VB.CheckBox chkWSpace 
               Caption         =   "Line"
               Height          =   210
               Index           =   6
               Left            =   1020
               TabIndex        =   155
               TabStop         =   0   'False
               Top             =   0
               Width           =   690
            End
            Begin VB.PictureBox L2Color 
               BackColor       =   &H00000000&
               Height          =   195
               Index           =   6
               Left            =   1740
               ScaleHeight     =   135
               ScaleWidth      =   135
               TabIndex        =   154
               TabStop         =   0   'False
               Top             =   0
               Width           =   195
            End
            Begin VB.PictureBox L1Color 
               BackColor       =   &H00FFFFFF&
               Height          =   195
               Index           =   6
               Left            =   810
               ScaleHeight     =   135
               ScaleWidth      =   135
               TabIndex        =   153
               TabStop         =   0   'False
               Top             =   0
               Width           =   195
            End
         End
         Begin VB.PictureBox picDummy 
            BorderStyle     =   0  'None
            Height          =   195
            Index           =   7
            Left            =   510
            ScaleHeight     =   195
            ScaleWidth      =   2640
            TabIndex        =   146
            TabStop         =   0   'False
            Top             =   4620
            Width           =   2640
            Begin VB.PictureBox L1Color 
               BackColor       =   &H00FFFFFF&
               Height          =   195
               Index           =   7
               Left            =   810
               ScaleHeight     =   135
               ScaleWidth      =   135
               TabIndex        =   151
               TabStop         =   0   'False
               Top             =   0
               Width           =   195
            End
            Begin VB.PictureBox L2Color 
               BackColor       =   &H00000000&
               Height          =   195
               Index           =   7
               Left            =   1740
               ScaleHeight     =   135
               ScaleWidth      =   135
               TabIndex        =   150
               TabStop         =   0   'False
               Top             =   0
               Width           =   195
            End
            Begin VB.CheckBox chkWSpace 
               Caption         =   "Line"
               Height          =   210
               Index           =   7
               Left            =   1020
               TabIndex        =   149
               TabStop         =   0   'False
               Top             =   0
               Width           =   690
            End
            Begin VB.CheckBox chkBSpace 
               Caption         =   "Line"
               Height          =   210
               Index           =   7
               Left            =   1950
               TabIndex        =   148
               TabStop         =   0   'False
               Top             =   0
               Width           =   660
            End
            Begin VB.CheckBox chkSwap 
               Caption         =   "Swap"
               Height          =   210
               Index           =   7
               Left            =   0
               TabIndex        =   147
               TabStop         =   0   'False
               Top             =   0
               Width           =   780
            End
         End
         Begin VB.PictureBox picDummy 
            BorderStyle     =   0  'None
            Height          =   195
            Index           =   8
            Left            =   510
            ScaleHeight     =   195
            ScaleWidth      =   2640
            TabIndex        =   140
            TabStop         =   0   'False
            Top             =   5250
            Width           =   2640
            Begin VB.CheckBox chkSwap 
               Caption         =   "Swap"
               Height          =   210
               Index           =   8
               Left            =   0
               TabIndex        =   145
               TabStop         =   0   'False
               Top             =   0
               Width           =   780
            End
            Begin VB.CheckBox chkBSpace 
               Caption         =   "Line"
               Height          =   210
               Index           =   8
               Left            =   1950
               TabIndex        =   144
               TabStop         =   0   'False
               Top             =   0
               Width           =   660
            End
            Begin VB.CheckBox chkWSpace 
               Caption         =   "Line"
               Height          =   210
               Index           =   8
               Left            =   1020
               TabIndex        =   143
               TabStop         =   0   'False
               Top             =   0
               Width           =   690
            End
            Begin VB.PictureBox L2Color 
               BackColor       =   &H00000000&
               Height          =   195
               Index           =   8
               Left            =   1740
               ScaleHeight     =   135
               ScaleWidth      =   135
               TabIndex        =   142
               TabStop         =   0   'False
               Top             =   0
               Width           =   195
            End
            Begin VB.PictureBox L1Color 
               BackColor       =   &H00FFFFFF&
               Height          =   195
               Index           =   8
               Left            =   810
               ScaleHeight     =   135
               ScaleWidth      =   135
               TabIndex        =   141
               TabStop         =   0   'False
               Top             =   0
               Width           =   195
            End
         End
         Begin VB.PictureBox picDummy 
            BorderStyle     =   0  'None
            Height          =   195
            Index           =   9
            Left            =   510
            ScaleHeight     =   195
            ScaleWidth      =   2640
            TabIndex        =   134
            TabStop         =   0   'False
            Top             =   5460
            Width           =   2640
            Begin VB.CheckBox chkSwap 
               Caption         =   "Swap"
               Height          =   210
               Index           =   9
               Left            =   0
               TabIndex        =   139
               TabStop         =   0   'False
               Top             =   0
               Width           =   780
            End
            Begin VB.CheckBox chkBSpace 
               Caption         =   "Line"
               Height          =   210
               Index           =   9
               Left            =   1950
               TabIndex        =   138
               TabStop         =   0   'False
               Top             =   0
               Width           =   660
            End
            Begin VB.CheckBox chkWSpace 
               Caption         =   "Line"
               Height          =   210
               Index           =   9
               Left            =   1020
               TabIndex        =   137
               TabStop         =   0   'False
               Top             =   0
               Width           =   690
            End
            Begin VB.PictureBox L2Color 
               BackColor       =   &H00000000&
               Height          =   195
               Index           =   9
               Left            =   1740
               ScaleHeight     =   135
               ScaleWidth      =   135
               TabIndex        =   136
               TabStop         =   0   'False
               Top             =   0
               Width           =   195
            End
            Begin VB.PictureBox L1Color 
               BackColor       =   &H00FFFFFF&
               Height          =   195
               Index           =   9
               Left            =   810
               ScaleHeight     =   135
               ScaleWidth      =   135
               TabIndex        =   135
               TabStop         =   0   'False
               Top             =   0
               Width           =   195
            End
         End
         Begin VB.PictureBox picDummy 
            BorderStyle     =   0  'None
            Height          =   195
            Index           =   10
            Left            =   510
            ScaleHeight     =   195
            ScaleWidth      =   2640
            TabIndex        =   128
            TabStop         =   0   'False
            Top             =   6090
            Width           =   2640
            Begin VB.CheckBox chkSwap 
               Caption         =   "Swap"
               Height          =   210
               Index           =   10
               Left            =   0
               TabIndex        =   133
               TabStop         =   0   'False
               Top             =   0
               Width           =   780
            End
            Begin VB.CheckBox chkBSpace 
               Caption         =   "Line"
               Height          =   210
               Index           =   10
               Left            =   1950
               TabIndex        =   132
               TabStop         =   0   'False
               Top             =   0
               Width           =   660
            End
            Begin VB.CheckBox chkWSpace 
               Caption         =   "Line"
               Height          =   210
               Index           =   10
               Left            =   1020
               TabIndex        =   131
               TabStop         =   0   'False
               Top             =   0
               Width           =   690
            End
            Begin VB.PictureBox L2Color 
               BackColor       =   &H00000000&
               Height          =   195
               Index           =   10
               Left            =   1740
               ScaleHeight     =   135
               ScaleWidth      =   135
               TabIndex        =   130
               TabStop         =   0   'False
               Top             =   0
               Width           =   195
            End
            Begin VB.PictureBox L1Color 
               BackColor       =   &H00FFFFFF&
               Height          =   195
               Index           =   10
               Left            =   810
               ScaleHeight     =   135
               ScaleWidth      =   135
               TabIndex        =   129
               TabStop         =   0   'False
               Top             =   0
               Width           =   195
            End
         End
         Begin VB.PictureBox picDummy 
            BorderStyle     =   0  'None
            Height          =   195
            Index           =   11
            Left            =   510
            ScaleHeight     =   195
            ScaleWidth      =   2640
            TabIndex        =   122
            TabStop         =   0   'False
            Top             =   6720
            Width           =   2640
            Begin VB.CheckBox chkSwap 
               Caption         =   "Swap"
               Height          =   210
               Index           =   11
               Left            =   0
               TabIndex        =   127
               TabStop         =   0   'False
               Top             =   0
               Width           =   780
            End
            Begin VB.CheckBox chkBSpace 
               Caption         =   "Line"
               Height          =   210
               Index           =   11
               Left            =   1950
               TabIndex        =   126
               TabStop         =   0   'False
               Top             =   0
               Width           =   660
            End
            Begin VB.CheckBox chkWSpace 
               Caption         =   "Line"
               Height          =   210
               Index           =   11
               Left            =   1020
               TabIndex        =   125
               TabStop         =   0   'False
               Top             =   0
               Width           =   690
            End
            Begin VB.PictureBox L2Color 
               BackColor       =   &H00000000&
               Height          =   195
               Index           =   11
               Left            =   1740
               ScaleHeight     =   135
               ScaleWidth      =   135
               TabIndex        =   124
               TabStop         =   0   'False
               Top             =   0
               Width           =   195
            End
            Begin VB.PictureBox L1Color 
               BackColor       =   &H00FFFFFF&
               Height          =   195
               Index           =   11
               Left            =   810
               ScaleHeight     =   135
               ScaleWidth      =   135
               TabIndex        =   123
               TabStop         =   0   'False
               Top             =   0
               Width           =   195
            End
         End
         Begin VB.CheckBox chkDummy 
            BackColor       =   &H00E0E0E0&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Index           =   0
            Left            =   210
            Style           =   1  'Graphical
            TabIndex        =   204
            TabStop         =   0   'False
            Top             =   420
            Width           =   2865
         End
         Begin VB.CheckBox chkDummy 
            BackColor       =   &H00E0E0E0&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Index           =   1
            Left            =   210
            Style           =   1  'Graphical
            TabIndex        =   205
            TabStop         =   0   'False
            Top             =   1050
            Width           =   2865
         End
         Begin VB.CheckBox chkDummy 
            BackColor       =   &H00E0E0E0&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Index           =   2
            Left            =   210
            Style           =   1  'Graphical
            TabIndex        =   206
            TabStop         =   0   'False
            Top             =   1680
            Width           =   2865
         End
         Begin VB.CheckBox chkDummy 
            BackColor       =   &H00E0E0E0&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Index           =   3
            Left            =   210
            Style           =   1  'Graphical
            TabIndex        =   207
            TabStop         =   0   'False
            Top             =   2310
            Width           =   2865
         End
         Begin VB.CheckBox chkDummy 
            BackColor       =   &H00E0E0E0&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Index           =   9
            Left            =   210
            Style           =   1  'Graphical
            TabIndex        =   208
            TabStop         =   0   'False
            Top             =   2940
            Width           =   2865
         End
         Begin VB.CheckBox chkDummy 
            BackColor       =   &H00E0E0E0&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Index           =   6
            Left            =   210
            Style           =   1  'Graphical
            TabIndex        =   209
            TabStop         =   0   'False
            Top             =   3570
            Width           =   2865
         End
         Begin VB.CheckBox chkDummy 
            BackColor       =   &H00E0E0E0&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Index           =   4
            Left            =   210
            Style           =   1  'Graphical
            TabIndex        =   210
            TabStop         =   0   'False
            Top             =   4200
            Width           =   2865
         End
         Begin VB.CheckBox chkDummy 
            BackColor       =   &H00E0E0E0&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Index           =   5
            Left            =   180
            Style           =   1  'Graphical
            TabIndex        =   211
            TabStop         =   0   'False
            Top             =   4830
            Width           =   2865
         End
         Begin VB.CheckBox chkDummy 
            BackColor       =   &H00E0E0E0&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Index           =   7
            Left            =   180
            Style           =   1  'Graphical
            TabIndex        =   212
            TabStop         =   0   'False
            Top             =   5670
            Width           =   2865
         End
         Begin VB.CheckBox chkDummy 
            BackColor       =   &H00E0E0E0&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   405
            Index           =   8
            Left            =   180
            Style           =   1  'Graphical
            TabIndex        =   213
            TabStop         =   0   'False
            Top             =   6300
            Width           =   2865
         End
      End
      Begin VB.Label AnyLabel 
         Caption         =   "Label1"
         Height          =   195
         Index           =   0
         Left            =   0
         TabIndex        =   228
         Top             =   0
         Visible         =   0   'False
         Width           =   975
      End
   End
   Begin VB.PictureBox AnyPanel 
      BackColor       =   &H000040C0&
      Height          =   7125
      Index           =   1
      Left            =   10230
      ScaleHeight     =   7065
      ScaleWidth      =   315
      TabIndex        =   225
      TabStop         =   0   'False
      Top             =   330
      Visible         =   0   'False
      Width           =   375
      Begin VB.Frame IniFrame 
         Caption         =   "Page Group Configuration"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2235
         Index           =   1
         Left            =   0
         TabIndex        =   267
         Top             =   0
         Width           =   7035
         Begin VB.PictureBox IniTabPanel 
            Height          =   1425
            Index           =   1
            Left            =   1110
            ScaleHeight     =   1365
            ScaleWidth      =   4755
            TabIndex        =   280
            Top             =   270
            Width           =   4815
            Begin TABLib.TAB IniActTab 
               Height          =   765
               Index           =   1
               Left            =   0
               TabIndex        =   281
               TabStop         =   0   'False
               Top             =   0
               Visible         =   0   'False
               Width           =   3015
               _Version        =   65536
               _ExtentX        =   5318
               _ExtentY        =   1349
               _StockProps     =   64
            End
         End
         Begin VB.PictureBox MsgPanel 
            Height          =   1425
            Index           =   11
            Left            =   120
            ScaleHeight     =   1365
            ScaleWidth      =   885
            TabIndex        =   275
            Top             =   270
            Width           =   945
            Begin VB.CheckBox ChkType 
               Caption         =   "Get MSG"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   47
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   279
               TabStop         =   0   'False
               Top             =   1035
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               Caption         =   "Get WKS"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   46
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   278
               TabStop         =   0   'False
               Top             =   690
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               Caption         =   "Open"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   45
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   277
               TabStop         =   0   'False
               Top             =   0
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               Caption         =   "Create"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   44
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   276
               TabStop         =   0   'False
               Top             =   345
               Width           =   885
            End
         End
         Begin VB.PictureBox IniBtnPanel 
            Height          =   1425
            Index           =   1
            Left            =   5970
            ScaleHeight     =   1365
            ScaleWidth      =   885
            TabIndex        =   270
            Top             =   270
            Width           =   945
            Begin VB.CheckBox ChkType 
               Caption         =   "Undo"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   43
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   274
               TabStop         =   0   'False
               Top             =   345
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               Caption         =   "Save"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   42
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   273
               TabStop         =   0   'False
               Top             =   0
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               Caption         =   "Deploy"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   41
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   272
               TabStop         =   0   'False
               Top             =   690
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               Caption         =   "Submit"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   40
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   271
               TabStop         =   0   'False
               Top             =   1035
               Width           =   885
            End
         End
         Begin VB.PictureBox IniCapPanel 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   1
            Left            =   120
            ScaleHeight     =   285
            ScaleWidth      =   6735
            TabIndex        =   268
            TabStop         =   0   'False
            Top             =   1770
            Width           =   6795
            Begin VB.CheckBox CurPageNoRgt 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   360
               Index           =   1
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   308
               TabStop         =   0   'False
               Top             =   0
               Visible         =   0   'False
               Width           =   990
            End
            Begin VB.TextBox IniInfoTitle 
               Alignment       =   2  'Center
               BackColor       =   &H8000000F&
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   1
               Left            =   450
               TabIndex        =   269
               Text            =   "Text1"
               Top             =   0
               Visible         =   0   'False
               Width           =   6735
            End
         End
      End
      Begin VB.TextBox AnyText 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1065
         Index           =   1
         Left            =   30
         MultiLine       =   -1  'True
         ScrollBars      =   1  'Horizontal
         TabIndex        =   250
         Top             =   5880
         Width           =   7035
      End
      Begin VB.Label AnyLabel 
         Caption         =   "Label1"
         Height          =   195
         Index           =   1
         Left            =   0
         TabIndex        =   227
         Top             =   0
         Visible         =   0   'False
         Width           =   975
      End
   End
   Begin VB.PictureBox AnyPanel 
      BackColor       =   &H000040C0&
      Height          =   7125
      Index           =   3
      Left            =   9720
      ScaleHeight     =   7065
      ScaleWidth      =   315
      TabIndex        =   246
      TabStop         =   0   'False
      Top             =   330
      Visible         =   0   'False
      Width           =   375
      Begin VB.Frame IniFrame 
         Caption         =   "Module Configuration"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2235
         Index           =   3
         Left            =   0
         TabIndex        =   282
         Top             =   0
         Width           =   7035
         Begin VB.PictureBox IniTabPanel 
            Height          =   1425
            Index           =   3
            Left            =   1110
            ScaleHeight     =   1365
            ScaleWidth      =   4755
            TabIndex        =   295
            Top             =   270
            Width           =   4815
            Begin TABLib.TAB IniActTab 
               Height          =   765
               Index           =   3
               Left            =   0
               TabIndex        =   296
               TabStop         =   0   'False
               Top             =   0
               Visible         =   0   'False
               Width           =   3015
               _Version        =   65536
               _ExtentX        =   5318
               _ExtentY        =   1349
               _StockProps     =   64
            End
         End
         Begin VB.PictureBox MsgPanel 
            Height          =   1425
            Index           =   12
            Left            =   120
            ScaleHeight     =   1365
            ScaleWidth      =   885
            TabIndex        =   290
            Top             =   270
            Width           =   945
            Begin VB.CheckBox ChkType 
               Caption         =   "Get MSG"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   55
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   294
               TabStop         =   0   'False
               Top             =   1035
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               Caption         =   "Get WKS"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   54
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   293
               TabStop         =   0   'False
               Top             =   690
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               Caption         =   "Open"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   53
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   292
               TabStop         =   0   'False
               Top             =   0
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               Caption         =   "Create"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   52
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   291
               TabStop         =   0   'False
               Top             =   345
               Width           =   885
            End
         End
         Begin VB.PictureBox IniBtnPanel 
            Height          =   1425
            Index           =   3
            Left            =   5970
            ScaleHeight     =   1365
            ScaleWidth      =   885
            TabIndex        =   285
            Top             =   270
            Width           =   945
            Begin VB.CheckBox ChkType 
               Caption         =   "Undo"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   51
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   289
               TabStop         =   0   'False
               Top             =   345
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               Caption         =   "Save"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   50
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   288
               TabStop         =   0   'False
               Top             =   0
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               Caption         =   "Deploy"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   49
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   287
               TabStop         =   0   'False
               Top             =   690
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               Caption         =   "Submit"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   48
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   286
               TabStop         =   0   'False
               Top             =   1035
               Width           =   885
            End
         End
         Begin VB.PictureBox IniCapPanel 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   3
            Left            =   120
            ScaleHeight     =   285
            ScaleWidth      =   6735
            TabIndex        =   283
            TabStop         =   0   'False
            Top             =   1770
            Width           =   6795
            Begin VB.CheckBox CurPageNoRgt 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   360
               Index           =   3
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   309
               TabStop         =   0   'False
               Top             =   0
               Visible         =   0   'False
               Width           =   990
            End
            Begin VB.TextBox IniInfoTitle 
               Alignment       =   2  'Center
               BackColor       =   &H8000000F&
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   3
               Left            =   690
               TabIndex        =   284
               Text            =   "Text1"
               Top             =   0
               Visible         =   0   'False
               Width           =   6735
            End
         End
      End
      Begin VB.TextBox AnyText 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1755
         Index           =   3
         Left            =   90
         MultiLine       =   -1  'True
         ScrollBars      =   1  'Horizontal
         TabIndex        =   251
         Top             =   3090
         Width           =   2115
      End
      Begin VB.Label AnyLabel 
         Caption         =   "Label1"
         Height          =   195
         Index           =   3
         Left            =   0
         TabIndex        =   247
         Top             =   0
         Visible         =   0   'False
         Width           =   975
      End
   End
   Begin VB.CheckBox chkAny 
      Caption         =   "Module"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   3
      Left            =   1110
      Style           =   1  'Graphical
      TabIndex        =   245
      TabStop         =   0   'False
      Top             =   0
      Width           =   1095
   End
   Begin VB.PictureBox AnyPanel 
      BackColor       =   &H000040C0&
      Height          =   7125
      Index           =   2
      Left            =   1440
      ScaleHeight     =   7065
      ScaleWidth      =   7215
      TabIndex        =   226
      TabStop         =   0   'False
      Top             =   330
      Visible         =   0   'False
      Width           =   7275
      Begin VB.TextBox AnyText 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4605
         Index           =   2
         Left            =   60
         MultiLine       =   -1  'True
         ScrollBars      =   1  'Horizontal
         TabIndex        =   248
         Text            =   "MyCfg.frx":0442
         Top             =   2370
         Width           =   7035
      End
      Begin VB.Frame IniFrame 
         Caption         =   "Actual Page"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2235
         Index           =   2
         Left            =   60
         TabIndex        =   230
         Top             =   90
         Width           =   7035
         Begin VB.PictureBox IniCapPanel 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   420
            Index           =   2
            Left            =   120
            ScaleHeight     =   360
            ScaleWidth      =   6735
            TabIndex        =   243
            TabStop         =   0   'False
            Top             =   1740
            Width           =   6795
            Begin VB.CheckBox CurPageNoRgt 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   360
               Index           =   2
               Left            =   5745
               Style           =   1  'Graphical
               TabIndex        =   306
               TabStop         =   0   'False
               Top             =   0
               Width           =   990
            End
            Begin VB.CheckBox CurPageNoLft 
               Caption         =   "1001"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   360
               Index           =   2
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   305
               TabStop         =   0   'False
               Top             =   0
               Width           =   990
            End
            Begin VB.TextBox IniInfoTitle 
               Alignment       =   2  'Center
               BackColor       =   &H8000000F&
               BorderStyle     =   0  'None
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   300
               Index           =   2
               Left            =   1020
               TabIndex        =   244
               Text            =   "Text1"
               Top             =   45
               Width           =   3615
            End
         End
         Begin VB.PictureBox IniBtnPanel 
            Height          =   1425
            Index           =   2
            Left            =   5970
            ScaleHeight     =   1365
            ScaleWidth      =   885
            TabIndex        =   238
            Top             =   270
            Width           =   945
            Begin VB.CheckBox ChkType 
               Caption         =   "Submit"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   31
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   242
               TabStop         =   0   'False
               Top             =   1035
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               Caption         =   "Deploy"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   30
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   241
               TabStop         =   0   'False
               Top             =   690
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               Caption         =   "Save"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   29
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   240
               TabStop         =   0   'False
               Top             =   0
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               Caption         =   "Undo"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   28
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   239
               TabStop         =   0   'False
               Top             =   345
               Width           =   885
            End
         End
         Begin VB.PictureBox MsgPanel 
            Height          =   1425
            Index           =   10
            Left            =   120
            ScaleHeight     =   1365
            ScaleWidth      =   885
            TabIndex        =   233
            Top             =   270
            Width           =   945
            Begin VB.CheckBox ChkType 
               Caption         =   "Create"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   27
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   237
               TabStop         =   0   'False
               Top             =   345
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               Caption         =   "Open"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   26
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   236
               TabStop         =   0   'False
               Top             =   0
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               Caption         =   "Get WKS"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   25
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   235
               TabStop         =   0   'False
               Top             =   690
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               Caption         =   "Get MSG"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   24
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   234
               TabStop         =   0   'False
               Top             =   1035
               Width           =   885
            End
         End
         Begin VB.PictureBox IniTabPanel 
            Height          =   1425
            Index           =   2
            Left            =   1110
            ScaleHeight     =   1365
            ScaleWidth      =   4755
            TabIndex        =   231
            Top             =   270
            Width           =   4815
            Begin VB.PictureBox CopyPagePanel 
               Height          =   1095
               Left            =   2460
               ScaleHeight     =   1035
               ScaleWidth      =   1485
               TabIndex        =   301
               Top             =   120
               Visible         =   0   'False
               Width           =   1545
               Begin VB.CheckBox ChkType 
                  Caption         =   "Copy Page"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   330
                  Index           =   57
                  Left            =   60
                  Style           =   1  'Graphical
                  TabIndex        =   303
                  TabStop         =   0   'False
                  Top             =   660
                  Width           =   1365
               End
               Begin VB.TextBox CopyPageNo 
                  Alignment       =   2  'Center
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   360
                  Left            =   60
                  TabIndex        =   302
                  Top             =   270
                  Width           =   1365
               End
               Begin VB.Label Label2 
                  Caption         =   "Copy From Page"
                  Height          =   255
                  Left            =   150
                  TabIndex        =   304
                  Top             =   30
                  Width           =   1395
               End
            End
            Begin VB.PictureBox NewPagePanel 
               Height          =   1095
               Left            =   810
               ScaleHeight     =   1035
               ScaleWidth      =   1485
               TabIndex        =   297
               Top             =   120
               Visible         =   0   'False
               Width           =   1545
               Begin VB.TextBox NewPageNo 
                  Alignment       =   2  'Center
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   360
                  Left            =   60
                  TabIndex        =   299
                  Top             =   270
                  Width           =   1365
               End
               Begin VB.CheckBox ChkType 
                  Caption         =   "Open Page"
                  BeginProperty Font 
                     Name            =   "Arial"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   700
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   330
                  Index           =   56
                  Left            =   60
                  Style           =   1  'Graphical
                  TabIndex        =   298
                  TabStop         =   0   'False
                  Top             =   660
                  Width           =   1365
               End
               Begin VB.Label Label1 
                  Caption         =   "New Page Number"
                  Height          =   255
                  Left            =   90
                  TabIndex        =   300
                  Top             =   30
                  Width           =   1395
               End
            End
            Begin TABLib.TAB IniActTab 
               Height          =   555
               Index           =   2
               Left            =   0
               TabIndex        =   232
               TabStop         =   0   'False
               Top             =   0
               Width           =   1035
               _Version        =   65536
               _ExtentX        =   1826
               _ExtentY        =   979
               _StockProps     =   64
            End
         End
      End
      Begin VB.Label AnyLabel 
         Caption         =   "Label2"
         Height          =   195
         Index           =   2
         Left            =   0
         TabIndex        =   229
         Top             =   0
         Visible         =   0   'False
         Width           =   975
      End
   End
   Begin VB.CheckBox chkAny 
      Caption         =   "Pages"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   2
      Left            =   3330
      Style           =   1  'Graphical
      TabIndex        =   224
      TabStop         =   0   'False
      Top             =   0
      Width           =   1095
   End
   Begin VB.CheckBox chkAny 
      Caption         =   "Groups"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   1
      Left            =   2220
      Style           =   1  'Graphical
      TabIndex        =   223
      TabStop         =   0   'False
      Top             =   0
      Width           =   1095
   End
   Begin VB.PictureBox CfgPanel 
      BackColor       =   &H000080FF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7125
      Index           =   1
      Left            =   11400
      ScaleHeight     =   7065
      ScaleWidth      =   135
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   330
      Visible         =   0   'False
      Width           =   195
      Begin VB.Frame FraType 
         Caption         =   "Standard Message Layout Types"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2235
         Index           =   0
         Left            =   60
         TabIndex        =   8
         Top             =   90
         Width           =   7035
         Begin VB.PictureBox MsgPanel 
            Height          =   1425
            Index           =   2
            Left            =   5970
            ScaleHeight     =   1365
            ScaleWidth      =   885
            TabIndex        =   24
            Top             =   270
            Width           =   945
            Begin VB.CheckBox ChkType 
               Caption         =   "Submit"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   7
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   28
               TabStop         =   0   'False
               Top             =   1035
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               Caption         =   "Deploy"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   6
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   27
               TabStop         =   0   'False
               Top             =   690
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               Caption         =   "DB Save"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   5
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   26
               TabStop         =   0   'False
               Top             =   345
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               Caption         =   "Save As"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   4
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   25
               TabStop         =   0   'False
               Top             =   0
               Width           =   885
            End
         End
         Begin VB.PictureBox TopInfoPanel 
            BackColor       =   &H8000000C&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   0
            Left            =   120
            ScaleHeight     =   285
            ScaleWidth      =   6735
            TabIndex        =   16
            TabStop         =   0   'False
            Top             =   1770
            Width           =   6795
            Begin VB.CheckBox chkTest 
               Caption         =   "Test"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   9
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   0
               Left            =   6180
               Style           =   1  'Graphical
               TabIndex        =   76
               TabStop         =   0   'False
               Top             =   0
               Width           =   525
            End
            Begin VB.PictureBox TopLine 
               BackColor       =   &H0080FF80&
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   0
               Left            =   405
               ScaleHeight     =   225
               ScaleWidth      =   5535
               TabIndex        =   18
               TabStop         =   0   'False
               Top             =   0
               Width           =   5595
               Begin VB.PictureBox LinePanel 
                  BackColor       =   &H00E0E0E0&
                  BorderStyle     =   0  'None
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   225
                  Index           =   0
                  Left            =   0
                  ScaleHeight     =   225
                  ScaleWidth      =   5295
                  TabIndex        =   19
                  TabStop         =   0   'False
                  Top             =   0
                  Width           =   5295
                  Begin VB.Label TopSep 
                     BackColor       =   &H00808080&
                     BeginProperty Font 
                        Name            =   "Arial"
                        Size            =   9.75
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   240
                     Index           =   2
                     Left            =   4410
                     TabIndex        =   75
                     Top             =   0
                     Width           =   60
                  End
                  Begin VB.Label TopLabel 
                     AutoSize        =   -1  'True
                     BackColor       =   &H00C0E0FF&
                     Caption         =   "TEST 1234"
                     BeginProperty Font 
                        Name            =   "Arial"
                        Size            =   9.75
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     ForeColor       =   &H00000000&
                     Height          =   240
                     Index           =   2
                     Left            =   3180
                     TabIndex        =   74
                     Tag             =   "Test 1234"
                     Top             =   0
                     Width           =   975
                  End
                  Begin VB.Label TopSep 
                     BackColor       =   &H00808080&
                     BeginProperty Font 
                        Name            =   "Arial"
                        Size            =   9.75
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   240
                     Index           =   1
                     Left            =   2880
                     TabIndex        =   23
                     Top             =   0
                     Width           =   60
                  End
                  Begin VB.Label TopLabel 
                     AutoSize        =   -1  'True
                     BackColor       =   &H00C0FFC0&
                     Caption         =   "TEST 1234"
                     BeginProperty Font 
                        Name            =   "Arial"
                        Size            =   9.75
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     ForeColor       =   &H00000000&
                     Height          =   240
                     Index           =   1
                     Left            =   1560
                     TabIndex        =   22
                     Tag             =   "Test 1234"
                     Top             =   0
                     Width           =   975
                  End
                  Begin VB.Label TopSep 
                     BackColor       =   &H00808080&
                     BeginProperty Font 
                        Name            =   "Arial"
                        Size            =   9.75
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   240
                     Index           =   0
                     Left            =   1260
                     TabIndex        =   21
                     Top             =   0
                     Width           =   60
                  End
                  Begin VB.Label TopLabel 
                     AutoSize        =   -1  'True
                     BackColor       =   &H00C0FFFF&
                     Caption         =   "TEST 1234"
                     BeginProperty Font 
                        Name            =   "Arial"
                        Size            =   9.75
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     ForeColor       =   &H00000000&
                     Height          =   240
                     Index           =   0
                     Left            =   0
                     TabIndex        =   20
                     Tag             =   "Test 1234"
                     Top             =   0
                     Width           =   975
                  End
               End
            End
            Begin VB.CheckBox chkTopLine 
               Caption         =   "[1]"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   9
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   0
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   17
               TabStop         =   0   'False
               Top             =   0
               Width           =   405
            End
         End
         Begin VB.PictureBox MsgPanel 
            Height          =   1425
            Index           =   1
            Left            =   120
            ScaleHeight     =   1365
            ScaleWidth      =   885
            TabIndex        =   11
            Top             =   270
            Width           =   945
            Begin VB.CheckBox ChkType 
               Caption         =   "Open As"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   0
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   15
               TabStop         =   0   'False
               Top             =   0
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               Caption         =   "DB Load"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   1
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   14
               TabStop         =   0   'False
               Top             =   345
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               Caption         =   "Get WKS"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   2
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   13
               TabStop         =   0   'False
               Top             =   690
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               Caption         =   "Get MSG"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   3
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   12
               TabStop         =   0   'False
               Top             =   1035
               Width           =   885
            End
         End
         Begin VB.PictureBox MsgPanel 
            Height          =   1425
            Index           =   0
            Left            =   1110
            ScaleHeight     =   1365
            ScaleWidth      =   4725
            TabIndex        =   9
            Top             =   270
            Width           =   4785
            Begin TABLib.TAB CfgTab 
               Height          =   765
               Index           =   1
               Left            =   0
               TabIndex        =   10
               TabStop         =   0   'False
               Top             =   0
               Width           =   3015
               _Version        =   65536
               _ExtentX        =   5318
               _ExtentY        =   1349
               _StockProps     =   64
            End
         End
      End
      Begin VB.Frame FraType 
         Caption         =   "Message Type Properties"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4485
         Index           =   1
         Left            =   60
         TabIndex        =   7
         Top             =   2430
         Width           =   7035
         Begin VB.Frame MsgPropCfg 
            Caption         =   "Message Animation"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   795
            Index           =   6
            Left            =   180
            TabIndex        =   70
            Top             =   3510
            Width           =   2115
            Begin VB.CheckBox TypeProp 
               Caption         =   "Timer"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   270
               Index           =   24
               Left            =   0
               TabIndex        =   220
               Tag             =   "NORM.TIME"
               Top             =   0
               Visible         =   0   'False
               Width           =   1935
            End
            Begin MSComctlLib.Slider MsgTimer 
               Height          =   435
               Index           =   0
               Left            =   30
               TabIndex        =   71
               Top             =   270
               Width           =   2055
               _ExtentX        =   3625
               _ExtentY        =   767
               _Version        =   393216
               SelStart        =   5
               Value           =   5
            End
         End
         Begin VB.Frame MsgPropCfg 
            Caption         =   "Animation Timer 2"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   795
            Index           =   8
            Left            =   4740
            TabIndex        =   69
            Top             =   3510
            Width           =   2115
            Begin VB.CheckBox TypeProp 
               Caption         =   "Timer"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   270
               Index           =   26
               Left            =   0
               TabIndex        =   222
               Tag             =   "ANI2.TIME"
               Top             =   0
               Visible         =   0   'False
               Width           =   1935
            End
            Begin MSComctlLib.Slider MsgTimer 
               Height          =   435
               Index           =   2
               Left            =   30
               TabIndex        =   73
               Tag             =   "ANI2.TIME"
               Top             =   270
               Width           =   2055
               _ExtentX        =   3625
               _ExtentY        =   767
               _Version        =   393216
               SelStart        =   2
               Value           =   2
            End
         End
         Begin VB.Frame MsgPropCfg 
            Caption         =   "Animation Timer 1"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   795
            Index           =   7
            Left            =   2460
            TabIndex        =   68
            Top             =   3510
            Width           =   2115
            Begin VB.CheckBox TypeProp 
               Caption         =   "Timer"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   270
               Index           =   25
               Left            =   0
               TabIndex        =   221
               Tag             =   "ANI1.TIME"
               Top             =   0
               Visible         =   0   'False
               Width           =   1935
            End
            Begin MSComctlLib.Slider MsgTimer 
               Height          =   435
               Index           =   1
               Left            =   30
               TabIndex        =   72
               Top             =   270
               Width           =   2055
               _ExtentX        =   3625
               _ExtentY        =   767
               _Version        =   393216
               SelStart        =   2
               Value           =   2
            End
         End
         Begin VB.Frame MsgPropCfg 
            Caption         =   "Message Separator"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   885
            Index           =   5
            Left            =   4740
            TabIndex        =   64
            Top             =   2310
            Width           =   2115
            Begin VB.CheckBox TypeProp 
               Caption         =   "Inset Border"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   270
               Index           =   23
               Left            =   120
               TabIndex        =   67
               Tag             =   "ANI2.SEPB"
               Top             =   270
               Width           =   1935
            End
            Begin VB.PictureBox SepBckColor 
               BackColor       =   &H00808080&
               Height          =   195
               Index           =   2
               Left            =   120
               ScaleHeight     =   135
               ScaleWidth      =   135
               TabIndex        =   65
               TabStop         =   0   'False
               Top             =   570
               Width           =   195
            End
            Begin VB.CheckBox TypeProp 
               Caption         =   "Back Color"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   270
               Index           =   22
               Left            =   120
               TabIndex        =   66
               Tag             =   "ANI2.SEPC"
               Top             =   540
               Width           =   1935
            End
         End
         Begin VB.Frame MsgPropCfg 
            Caption         =   "Message Separator"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   885
            Index           =   4
            Left            =   2460
            TabIndex        =   60
            Top             =   2310
            Width           =   2115
            Begin VB.CheckBox TypeProp 
               Caption         =   "Inset Border"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   270
               Index           =   21
               Left            =   120
               TabIndex        =   63
               Tag             =   "ANI1.SEPB"
               Top             =   270
               Width           =   1935
            End
            Begin VB.PictureBox SepBckColor 
               BackColor       =   &H00808080&
               Height          =   195
               Index           =   1
               Left            =   120
               ScaleHeight     =   135
               ScaleWidth      =   135
               TabIndex        =   61
               TabStop         =   0   'False
               Top             =   570
               Width           =   195
            End
            Begin VB.CheckBox TypeProp 
               Caption         =   "Back Color"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   270
               Index           =   20
               Left            =   120
               TabIndex        =   62
               Tag             =   "ANI1.SEPC"
               Top             =   540
               Width           =   1935
            End
         End
         Begin VB.Frame MsgPropCfg 
            Caption         =   "Message Separator"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   885
            Index           =   3
            Left            =   180
            TabIndex        =   56
            Top             =   2310
            Width           =   2115
            Begin VB.CheckBox TypeProp 
               Caption         =   "Inset Border"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   270
               Index           =   18
               Left            =   120
               TabIndex        =   57
               Tag             =   "NORM.SEPB"
               Top             =   270
               Width           =   1935
            End
            Begin VB.PictureBox SepBckColor 
               BackColor       =   &H00808080&
               Height          =   195
               Index           =   0
               Left            =   120
               ScaleHeight     =   135
               ScaleWidth      =   135
               TabIndex        =   59
               TabStop         =   0   'False
               Top             =   570
               Width           =   195
            End
            Begin VB.CheckBox TypeProp 
               Caption         =   "Back Color"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   270
               Index           =   19
               Left            =   120
               TabIndex        =   58
               Tag             =   "NORM.SEPC"
               Top             =   540
               Width           =   1935
            End
         End
         Begin VB.Frame MsgPropCfg 
            Caption         =   "Normal Text Layout"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1965
            Index           =   0
            Left            =   180
            TabIndex        =   47
            Top             =   300
            Width           =   2115
            Begin VB.CheckBox TypeProp 
               Caption         =   "Transparent"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   270
               Index           =   0
               Left            =   120
               TabIndex        =   55
               Tag             =   "NORM.TRNS"
               Top             =   270
               Width           =   1935
            End
            Begin VB.CheckBox TypeProp 
               Caption         =   "Inset Border"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   270
               Index           =   1
               Left            =   120
               TabIndex        =   54
               Tag             =   "NORM.BORD"
               Top             =   540
               Width           =   1935
            End
            Begin VB.CheckBox TypeProp 
               Caption         =   "Text Trimmed"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   270
               Index           =   2
               Left            =   120
               TabIndex        =   53
               Tag             =   "NORM.TRIM"
               Top             =   810
               Width           =   1935
            End
            Begin VB.CheckBox TypeProp 
               Caption         =   "Text Uppercase"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   270
               Index           =   3
               Left            =   120
               TabIndex        =   52
               Tag             =   "NORM.UPCS"
               Top             =   1080
               Width           =   1935
            End
            Begin VB.PictureBox MsgTxtColor 
               BackColor       =   &H00000000&
               Height          =   195
               Index           =   0
               Left            =   120
               ScaleHeight     =   135
               ScaleWidth      =   135
               TabIndex        =   50
               TabStop         =   0   'False
               Top             =   1380
               Width           =   195
            End
            Begin VB.PictureBox MsgBckColor 
               BackColor       =   &H0080FFFF&
               Height          =   195
               Index           =   0
               Left            =   120
               ScaleHeight     =   135
               ScaleWidth      =   135
               TabIndex        =   48
               TabStop         =   0   'False
               Top             =   1650
               Width           =   195
            End
            Begin VB.CheckBox TypeProp 
               Caption         =   "Text Color"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   270
               Index           =   4
               Left            =   120
               TabIndex        =   51
               Tag             =   "NORM.TXCO"
               Top             =   1350
               Width           =   1935
            End
            Begin VB.CheckBox TypeProp 
               Caption         =   "Back Color"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   270
               Index           =   5
               Left            =   120
               TabIndex        =   49
               Tag             =   "NORM.BKCO"
               Top             =   1620
               Width           =   1935
            End
         End
         Begin VB.Frame MsgPropCfg 
            Caption         =   "Animated Layout 2"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1965
            Index           =   2
            Left            =   4740
            TabIndex        =   38
            Top             =   300
            Width           =   2115
            Begin VB.PictureBox MsgBckColor 
               BackColor       =   &H00FFFFFF&
               Height          =   195
               Index           =   2
               Left            =   120
               ScaleHeight     =   135
               ScaleWidth      =   135
               TabIndex        =   46
               TabStop         =   0   'False
               Top             =   1650
               Width           =   195
            End
            Begin VB.CheckBox TypeProp 
               Caption         =   "Back Color"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   270
               Index           =   17
               Left            =   120
               TabIndex        =   45
               Tag             =   "ANI2.BKCO"
               Top             =   1620
               Width           =   1935
            End
            Begin VB.PictureBox MsgTxtColor 
               BackColor       =   &H000000FF&
               Height          =   195
               Index           =   2
               Left            =   120
               ScaleHeight     =   135
               ScaleWidth      =   135
               TabIndex        =   44
               TabStop         =   0   'False
               Top             =   1380
               Width           =   195
            End
            Begin VB.CheckBox TypeProp 
               Caption         =   "Text Color"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   270
               Index           =   16
               Left            =   120
               TabIndex        =   43
               Tag             =   "ANI2.TXCO"
               Top             =   1350
               Width           =   1935
            End
            Begin VB.CheckBox TypeProp 
               Caption         =   "Text Uppercase"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   270
               Index           =   15
               Left            =   120
               TabIndex        =   42
               Tag             =   "ANI2.UPCS"
               Top             =   1080
               Width           =   1935
            End
            Begin VB.CheckBox TypeProp 
               Caption         =   "Text Trimmed"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   270
               Index           =   14
               Left            =   120
               TabIndex        =   41
               Tag             =   "ANI2.TRIM"
               Top             =   810
               Width           =   1935
            End
            Begin VB.CheckBox TypeProp 
               Caption         =   "Inset Border"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   270
               Index           =   13
               Left            =   120
               TabIndex        =   40
               Tag             =   "ANI2.BORD"
               Top             =   540
               Width           =   1935
            End
            Begin VB.CheckBox TypeProp 
               Caption         =   "Transparent"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   270
               Index           =   12
               Left            =   120
               TabIndex        =   39
               Tag             =   "ANI2.TRNS"
               Top             =   270
               Width           =   1935
            End
         End
         Begin VB.Frame MsgPropCfg 
            Caption         =   "Animated Layout 1"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   1965
            Index           =   1
            Left            =   2460
            TabIndex        =   29
            Top             =   300
            Width           =   2115
            Begin VB.PictureBox MsgBckColor 
               BackColor       =   &H000000FF&
               Height          =   195
               Index           =   1
               Left            =   120
               ScaleHeight     =   135
               ScaleWidth      =   135
               TabIndex        =   37
               TabStop         =   0   'False
               Top             =   1650
               Width           =   195
            End
            Begin VB.CheckBox TypeProp 
               Caption         =   "Back Color"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   270
               Index           =   11
               Left            =   120
               TabIndex        =   36
               Tag             =   "ANI1.BKCO"
               Top             =   1620
               Width           =   1935
            End
            Begin VB.PictureBox MsgTxtColor 
               BackColor       =   &H00FFFFFF&
               Height          =   195
               Index           =   1
               Left            =   120
               ScaleHeight     =   135
               ScaleWidth      =   135
               TabIndex        =   35
               TabStop         =   0   'False
               Top             =   1380
               Width           =   195
            End
            Begin VB.CheckBox TypeProp 
               Caption         =   "Text Color"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   270
               Index           =   10
               Left            =   120
               TabIndex        =   34
               Tag             =   "ANI1.TXCO"
               Top             =   1350
               Width           =   1935
            End
            Begin VB.CheckBox TypeProp 
               Caption         =   "Text Uppercase"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   270
               Index           =   9
               Left            =   120
               TabIndex        =   33
               Tag             =   "ANI1.UPCS"
               Top             =   1080
               Width           =   1935
            End
            Begin VB.CheckBox TypeProp 
               Caption         =   "Text Trimmed"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   270
               Index           =   8
               Left            =   120
               TabIndex        =   32
               Tag             =   "ANI1.TRIM"
               Top             =   810
               Width           =   1935
            End
            Begin VB.CheckBox TypeProp 
               Caption         =   "Inset Border"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   270
               Index           =   7
               Left            =   120
               TabIndex        =   31
               Tag             =   "ANI1.BORD"
               Top             =   540
               Width           =   1935
            End
            Begin VB.CheckBox TypeProp 
               Caption         =   "Transparent"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   270
               Index           =   6
               Left            =   120
               TabIndex        =   30
               Tag             =   "ANI1.TRNS"
               Top             =   270
               Width           =   1935
            End
         End
      End
      Begin VB.Image CfgIcon 
         Height          =   480
         Index           =   1
         Left            =   0
         Picture         =   "MyCfg.frx":0448
         Top             =   0
         Visible         =   0   'False
         Width           =   480
      End
      Begin VB.Label CfgCaption 
         Caption         =   "Message Type Configuration"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   225
         Index           =   1
         Left            =   0
         TabIndex        =   6
         Top             =   -30
         Visible         =   0   'False
         Width           =   2205
      End
   End
   Begin VB.CheckBox chkAny 
      Caption         =   "Main Panel"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   0
      Left            =   0
      Style           =   1  'Graphical
      TabIndex        =   119
      TabStop         =   0   'False
      Top             =   0
      Width           =   1095
   End
   Begin VB.CheckBox ChkCfg 
      Caption         =   "MSG Rules"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   2
      Left            =   8490
      Style           =   1  'Graphical
      TabIndex        =   80
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.PictureBox CfgPanel 
      BackColor       =   &H000080FF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7125
      Index           =   2
      Left            =   11670
      ScaleHeight     =   7065
      ScaleWidth      =   165
      TabIndex        =   78
      TabStop         =   0   'False
      Top             =   330
      Visible         =   0   'False
      Width           =   225
      Begin VB.Frame FraType 
         Caption         =   "Standard Message Types"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2145
         Index           =   4
         Left            =   60
         TabIndex        =   117
         Top             =   4770
         Width           =   7035
      End
      Begin VB.Frame FraType 
         Caption         =   "Standard Message Types"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2235
         Index           =   3
         Left            =   60
         TabIndex        =   105
         Top             =   2430
         Width           =   7035
         Begin VB.PictureBox TopInfoPanel 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   2
            Left            =   120
            ScaleHeight     =   285
            ScaleWidth      =   6735
            TabIndex        =   118
            TabStop         =   0   'False
            Top             =   1770
            Width           =   6795
         End
         Begin VB.PictureBox MsgPanel 
            Height          =   1425
            Index           =   8
            Left            =   1110
            ScaleHeight     =   1365
            ScaleWidth      =   4725
            TabIndex        =   116
            Top             =   270
            Width           =   4785
         End
         Begin VB.PictureBox MsgPanel 
            Height          =   1425
            Index           =   7
            Left            =   120
            ScaleHeight     =   1365
            ScaleWidth      =   885
            TabIndex        =   111
            Top             =   270
            Width           =   945
            Begin VB.CheckBox ChkType 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   23
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   115
               TabStop         =   0   'False
               Top             =   1035
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   22
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   114
               TabStop         =   0   'False
               Top             =   690
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   21
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   113
               TabStop         =   0   'False
               Top             =   345
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   20
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   112
               TabStop         =   0   'False
               Top             =   0
               Width           =   885
            End
         End
         Begin VB.PictureBox MsgPanel 
            Height          =   1425
            Index           =   6
            Left            =   5970
            ScaleHeight     =   1365
            ScaleWidth      =   885
            TabIndex        =   106
            Top             =   270
            Width           =   945
            Begin VB.CheckBox ChkType 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   19
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   110
               TabStop         =   0   'False
               Top             =   0
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               Caption         =   "Pick BC"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   18
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   109
               TabStop         =   0   'False
               Top             =   345
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   17
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   108
               TabStop         =   0   'False
               Top             =   690
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   16
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   107
               TabStop         =   0   'False
               Top             =   1035
               Width           =   885
            End
         End
      End
      Begin VB.Frame FraType 
         Caption         =   "Notification Message Rules"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2235
         Index           =   2
         Left            =   60
         TabIndex        =   81
         Top             =   90
         Width           =   7035
         Begin VB.PictureBox MsgPanel 
            Height          =   1425
            Index           =   5
            Left            =   1110
            ScaleHeight     =   1365
            ScaleWidth      =   4725
            TabIndex        =   103
            Top             =   270
            Width           =   4785
            Begin TABLib.TAB CfgTab 
               Height          =   765
               Index           =   2
               Left            =   0
               TabIndex        =   104
               TabStop         =   0   'False
               Top             =   0
               Width           =   3015
               _Version        =   65536
               _ExtentX        =   5318
               _ExtentY        =   1349
               _StockProps     =   64
            End
         End
         Begin VB.PictureBox MsgPanel 
            Height          =   1425
            Index           =   4
            Left            =   120
            ScaleHeight     =   1365
            ScaleWidth      =   885
            TabIndex        =   98
            Top             =   270
            Width           =   945
            Begin VB.CheckBox ChkType 
               Caption         =   "Get MSG"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   15
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   102
               TabStop         =   0   'False
               Top             =   1035
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               Caption         =   "Get WKS"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   14
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   101
               TabStop         =   0   'False
               Top             =   690
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               Caption         =   "DB Load"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   13
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   100
               TabStop         =   0   'False
               Top             =   0
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               Caption         =   "Open As"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   12
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   99
               TabStop         =   0   'False
               Top             =   345
               Width           =   885
            End
         End
         Begin VB.PictureBox TopInfoPanel 
            BackColor       =   &H8000000C&
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   1
            Left            =   120
            ScaleHeight     =   285
            ScaleWidth      =   6735
            TabIndex        =   87
            TabStop         =   0   'False
            Top             =   1770
            Width           =   6795
            Begin VB.CheckBox chkTopLine 
               Caption         =   "//1"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   9
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   1
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   97
               TabStop         =   0   'False
               Top             =   0
               Width           =   405
            End
            Begin VB.PictureBox TopLine 
               BackColor       =   &H0080FF80&
               BeginProperty Font 
                  Name            =   "MS Sans Serif"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   1
               Left            =   405
               ScaleHeight     =   225
               ScaleWidth      =   5535
               TabIndex        =   89
               TabStop         =   0   'False
               Top             =   0
               Width           =   5595
               Begin VB.PictureBox LinePanel 
                  BackColor       =   &H00E0E0E0&
                  BorderStyle     =   0  'None
                  BeginProperty Font 
                     Name            =   "MS Sans Serif"
                     Size            =   8.25
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   225
                  Index           =   1
                  Left            =   0
                  ScaleHeight     =   225
                  ScaleWidth      =   5295
                  TabIndex        =   90
                  TabStop         =   0   'False
                  Top             =   0
                  Width           =   5295
                  Begin VB.Label TopLabel 
                     AutoSize        =   -1  'True
                     BackColor       =   &H00C0FFFF&
                     Caption         =   "TEST 1234"
                     BeginProperty Font 
                        Name            =   "Arial"
                        Size            =   9.75
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     ForeColor       =   &H00000000&
                     Height          =   240
                     Index           =   5
                     Left            =   0
                     TabIndex        =   96
                     Tag             =   "Test 1234"
                     Top             =   0
                     Width           =   975
                  End
                  Begin VB.Label TopSep 
                     BackColor       =   &H00808080&
                     BeginProperty Font 
                        Name            =   "Arial"
                        Size            =   9.75
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   240
                     Index           =   5
                     Left            =   1260
                     TabIndex        =   95
                     Top             =   0
                     Visible         =   0   'False
                     Width           =   60
                  End
                  Begin VB.Label TopLabel 
                     AutoSize        =   -1  'True
                     BackColor       =   &H00C0FFC0&
                     Caption         =   "TEST 1234"
                     BeginProperty Font 
                        Name            =   "Arial"
                        Size            =   9.75
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     ForeColor       =   &H00000000&
                     Height          =   240
                     Index           =   4
                     Left            =   1560
                     TabIndex        =   94
                     Tag             =   "Test 1234"
                     Top             =   0
                     Visible         =   0   'False
                     Width           =   975
                  End
                  Begin VB.Label TopSep 
                     BackColor       =   &H00808080&
                     BeginProperty Font 
                        Name            =   "Arial"
                        Size            =   9.75
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   240
                     Index           =   4
                     Left            =   2880
                     TabIndex        =   93
                     Top             =   0
                     Visible         =   0   'False
                     Width           =   60
                  End
                  Begin VB.Label TopLabel 
                     AutoSize        =   -1  'True
                     BackColor       =   &H00C0E0FF&
                     Caption         =   "TEST 1234"
                     BeginProperty Font 
                        Name            =   "Arial"
                        Size            =   9.75
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     ForeColor       =   &H00000000&
                     Height          =   240
                     Index           =   3
                     Left            =   3180
                     TabIndex        =   92
                     Tag             =   "Test 1234"
                     Top             =   0
                     Visible         =   0   'False
                     Width           =   975
                  End
                  Begin VB.Label TopSep 
                     BackColor       =   &H00808080&
                     BeginProperty Font 
                        Name            =   "Arial"
                        Size            =   9.75
                        Charset         =   0
                        Weight          =   700
                        Underline       =   0   'False
                        Italic          =   0   'False
                        Strikethrough   =   0   'False
                     EndProperty
                     Height          =   240
                     Index           =   3
                     Left            =   4410
                     TabIndex        =   91
                     Top             =   0
                     Visible         =   0   'False
                     Width           =   60
                  End
               End
            End
            Begin VB.CheckBox chkTest 
               Caption         =   "Test"
               Enabled         =   0   'False
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   9
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Index           =   1
               Left            =   6180
               Style           =   1  'Graphical
               TabIndex        =   88
               TabStop         =   0   'False
               Top             =   0
               Width           =   525
            End
         End
         Begin VB.PictureBox MsgPanel 
            Height          =   1425
            Index           =   3
            Left            =   5970
            ScaleHeight     =   1365
            ScaleWidth      =   885
            TabIndex        =   82
            Top             =   270
            Width           =   945
            Begin VB.CheckBox ChkType 
               Caption         =   "Save As"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   11
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   86
               TabStop         =   0   'False
               Top             =   345
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               Caption         =   "DB Save"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   10
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   85
               TabStop         =   0   'False
               Top             =   0
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               Caption         =   "Deploy"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   9
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   84
               TabStop         =   0   'False
               Top             =   690
               Width           =   885
            End
            Begin VB.CheckBox ChkType 
               Caption         =   "Submit"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   8.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   8
               Left            =   0
               Style           =   1  'Graphical
               TabIndex        =   83
               TabStop         =   0   'False
               Top             =   1035
               Width           =   885
            End
         End
      End
      Begin VB.Label CfgCaption 
         AutoSize        =   -1  'True
         Caption         =   "Message Rule Configuration"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Index           =   2
         Left            =   570
         TabIndex        =   79
         Top             =   30
         Visible         =   0   'False
         Width           =   1995
      End
      Begin VB.Image CfgIcon 
         Height          =   480
         Index           =   2
         Left            =   30
         Picture         =   "MyCfg.frx":0D12
         Top             =   30
         Visible         =   0   'False
         Width           =   480
      End
   End
   Begin VB.CheckBox ChkSysLib 
      Caption         =   "System"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   0
      Left            =   7350
      Style           =   1  'Graphical
      TabIndex        =   77
      TabStop         =   0   'False
      Top             =   0
      Visible         =   0   'False
      Width           =   1095
   End
   Begin MSComDlg.CommonDialog CommonDialog 
      Left            =   30
      Top             =   330
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      CancelError     =   -1  'True
   End
   Begin VB.CheckBox ChkCfg 
      Caption         =   "MSG Types"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   1
      Left            =   4440
      Style           =   1  'Graphical
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   0
      Width           =   1095
   End
   Begin VB.CheckBox ChkCfg 
      Caption         =   "Key Codes"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   0
      Left            =   5550
      Style           =   1  'Graphical
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   0
      Width           =   1095
   End
   Begin VB.PictureBox CfgPanel 
      BackColor       =   &H000080FF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   7125
      Index           =   0
      Left            =   11100
      ScaleHeight     =   7065
      ScaleWidth      =   165
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   330
      Visible         =   0   'False
      Width           =   225
      Begin TABLib.TAB CfgTab 
         Height          =   2055
         Index           =   0
         Left            =   0
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   120
         Width           =   4905
         _Version        =   65536
         _ExtentX        =   8652
         _ExtentY        =   3625
         _StockProps     =   64
      End
      Begin VB.Image CfgIcon 
         Height          =   480
         Index           =   0
         Left            =   60
         Picture         =   "MyCfg.frx":101C
         Top             =   2250
         Visible         =   0   'False
         Width           =   480
      End
      Begin VB.Label CfgCaption 
         Caption         =   "Keyboard Configuration"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   165
         Index           =   0
         Left            =   60
         TabIndex        =   5
         Top             =   2850
         Visible         =   0   'False
         Width           =   405
      End
   End
   Begin VB.Image MyIcon 
      Height          =   480
      Index           =   1
      Left            =   150
      Picture         =   "MyCfg.frx":145E
      Top             =   750
      Width           =   480
   End
   Begin VB.Image MyIcon 
      Height          =   480
      Index           =   0
      Left            =   870
      Picture         =   "MyCfg.frx":18A0
      Top             =   780
      Visible         =   0   'False
      Width           =   480
   End
End
Attribute VB_Name = "MyCfg"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim CfgIdx As Integer
Dim AnyIdx As Integer
Dim MyCaption As String
Dim RefreshMain As Boolean
Dim Initializing As Boolean
Dim InfoTypesFile As String
Dim ThisIsANewPage As Boolean
Dim NewPageFileName As String
Dim NewPageFilePath As String
Dim MyCurDispPage As String

Public Function GetKeyCodeMean(KeyCode As String) As String
    Dim HitLine As String
    Dim LineCnt As Long
    Dim LineNo As Long
    Dim KeyMean As String
    CfgTab(0).SetInternalLineBuffer True
    HitLine = CfgTab(0).GetLinesByColumnValue(1, KeyCode, 0)
    LineCnt = Val(HitLine)
    If LineCnt = 1 Then
        LineNo = CfgTab(0).GetNextResultLine
        If LineNo >= 0 Then
            KeyMean = CfgTab(0).GetColumnValue(LineNo, 0)
        End If
    ElseIf LineCnt = 0 Then
        KeyMean = "NOK"
    Else
        KeyMean = "CNT"
    End If
    CfgTab(0).SetInternalLineBuffer False
    GetKeyCodeMean = KeyMean
End Function

Private Sub InitCfgTab(Index As Integer)
    Dim tmpRec As String
    Dim i As Integer
    Dim j As Integer
    i = Index
    Select Case Index
        Case 0
            CfgTab(i).ResetContent
            CfgTab(i).HeaderString = "FUNC,KEYS,W,REMARK" & Space(100)
            CfgTab(i).HeaderLengthString = "50,50,50,50"
            CfgTab(i).ColumnAlignmentString = "L,L,C,L"
            CfgTab(i).HeaderAlignmentString = CfgTab(i).ColumnAlignmentString
            CfgTab(i).FontName = "Arial"
            CfgTab(i).SetTabFontBold True
            CfgTab(i).LineHeight = 20
            CfgTab(i).FontSize = CfgTab(i).LineHeight - 2
            CfgTab(i).HeaderFontSize = CfgTab(i).FontSize
            CfgTab(i).DisplayBackColor = vbBlue
            CfgTab(i).DisplayTextColor = vbWhite
            CfgTab(i).SelectBackColor = vbBlack
            CfgTab(i).SelectTextColor = vbYellow
            CfgTab(i).EmptyAreaBackColor = CfgTab(i).DisplayBackColor
            CfgTab(i).ShowVertScroller False
            CfgTab(i).AutoSizeByHeader = True
            CfgTab(i).LifeStyle = True
            tmpRec = ""
            tmpRec = tmpRec & "PGDN,+,N,Page Down (Next Screen)" & vbNewLine
            tmpRec = tmpRec & "PGUP,-,N,Page Up (Previous Screen)" & vbNewLine
            tmpRec = tmpRec & "MSGW,//,Y,Open/Close Info Objects" & vbNewLine
            tmpRec = tmpRec & "MSG0,//0,N,Open/Close Update Monitor" & vbNewLine
            tmpRec = tmpRec & "MSG1,//1,N,Open/Close Message Details 1" & vbNewLine
            tmpRec = tmpRec & "MSG2,//2,N,Open/Close Message Details 2" & vbNewLine
            tmpRec = tmpRec & "MSG3,//3,N,Open/Close Message Details 3" & vbNewLine
            tmpRec = tmpRec & "MSG4,//4,N,Open/Close Message Details 4" & vbNewLine
            tmpRec = tmpRec & "MSG5,//5,N,Open/Close Message Details 5" & vbNewLine
            tmpRec = tmpRec & "EXIT,.0.,N,Exit Application" & vbNewLine
            tmpRec = tmpRec & "CLS,.00,N,Close Focus Window" & vbNewLine
            tmpRec = tmpRec & "KBD,.01,N,Show Keyboard Configuration" & vbNewLine
            tmpRec = tmpRec & "RPT,..,N,Repeat Last Function" & vbNewLine
            CfgTab(i).InsertBuffer tmpRec, vbNewLine
            CfgTab(i).AutoSizeColumns
        Case 1
            CfgTab(i).ResetContent
            CfgTab(i).LogicalFieldList = "SQID,CSRC,TYPE,PRIO,TEXT,NORM,ANI1,ANI2,ADDI"
            CfgTab(i).HeaderString = "ID ,CFG,TYPE,PRIO,MSG TYPE PURPOSE            ,NORM,ANI1,ANI2,ADDI"
            CfgTab(i).HeaderLengthString = "50,50,50,50,50,50,50,50,50"
            CfgTab(i).ColumnAlignmentString = "R,C,L,C,L,L,L,L,L"
            CfgTab(i).HeaderAlignmentString = CfgTab(i).ColumnAlignmentString
            CfgTab(i).FontName = "Arial"
            CfgTab(i).SetTabFontBold True
            CfgTab(i).LineHeight = 18
            CfgTab(i).FontSize = CfgTab(i).LineHeight - 2
            CfgTab(i).HeaderFontSize = CfgTab(i).FontSize
            'CfgTab(i).DisplayBackColor = LightGrey
            'CfgTab(i).DisplayTextColor = vbBlack
            'CfgTab(i).SelectBackColor = vbBlue
            'CfgTab(i).SelectTextColor = vbWhite
            CfgTab(i).EmptyAreaBackColor = CfgTab(i).DisplayBackColor
            CfgTab(i).ShowVertScroller True
            CfgTab(i).ShowHorzScroller False
            CfgTab(i).AutoSizeByHeader = True
            CfgTab(i).LifeStyle = True
            CfgTab(i).ResetContent
            InfoTypesFile = App.Path & "\Config\" & "StaffMonitor_MsgCfg1.dat"
            If Dir(InfoTypesFile) <> "" Then
                CfgTab(i).ReadFromFile InfoTypesFile
            Else
                tmpRec = ""
                tmpRec = tmpRec & "0,SYS,DFLT,ANY,Default Message Layout,,,," & vbNewLine
                tmpRec = tmpRec & "1,USR,FLTU,HIGH,Critical Flight Update,,,," & vbNewLine
                tmpRec = tmpRec & "2,USR,FLTU,DFLT,Important Flight Update,,,," & vbNewLine
                tmpRec = tmpRec & "3,USR,FLTU,INFO,Any Other Flight Update,,,," & vbNewLine
                For j = 4 To MAX_MSG_TYP
                    tmpRec = tmpRec & CStr(j) & ",SYS,FREE,,,,,," & vbNewLine
                Next
                CfgTab(i).InsertBuffer tmpRec, vbNewLine
            End If
            CfgTab(i).AutoSizeColumns
        Case 3
'            CfgTab(i).Top = 0
'            CfgTab(i).Left = 0
'            'CfgTab(i).Width = MsgPanel(9).ScaleWidth
'            'CfgTab(i).Height = MsgPanel(9).ScaleHeight
'            CfgTab(i).ResetContent
'            CfgTab(i).HeaderString = "DATALINE" & Space(1000)
'            CfgTab(i).HeaderLengthString = "50"
'            CfgTab(i).ColumnAlignmentString = "L"
'            CfgTab(i).MainHeader = True
'            CfgTab(i).HeaderAlignmentString = CfgTab(i).ColumnAlignmentString
'            CfgTab(i).FontName = "Courier New"
'            CfgTab(i).SetTabFontBold True
'            CfgTab(i).LineHeight = 14
'            CfgTab(i).FontSize = CfgTab(i).LineHeight
'            CfgTab(i).HeaderFontSize = CfgTab(i).FontSize
'            CfgTab(i).SetMainHeaderFont CfgTab(i).FontSize, False, False, True, 0, "Courier New"
'            CfgTab(i).SetMainHeaderValues "1", " ", ""
'            'CfgTab(i).DisplayBackColor = vbBlue
'            'CfgTab(i).DisplayTextColor = vbYellow
'            'CfgTab(i).SelectBackColor = vbBlack
'            'CfgTab(i).SelectTextColor = vbYellow
'            'CfgTab(i).EmptyAreaBackColor = CfgTab(i).DisplayBackColor
'            CfgTab(i).ShowVertScroller False
'            CfgTab(i).ShowHorzScroller True
'            CfgTab(i).AutoSizeByHeader = True
'            CfgTab(i).LifeStyle = True
'            CfgTab(i).AutoSizeColumns
        Case Else
    End Select
End Sub

Private Sub AnyText_Change(Index As Integer)
    If Not StopNestedCalls Then
        Select Case Index
            Case 1
                If AnyText(Index).Text <> AnyText(Index).Tag Then ChkType(42).Enabled = True Else ChkType(42).Enabled = False
            Case 2
                If AnyText(Index).Text <> AnyText(Index).Tag Then ChkType(29).Enabled = True Else ChkType(29).Enabled = False
            Case 3
                If AnyText(Index).Text <> AnyText(Index).Tag Then ChkType(50).Enabled = True Else ChkType(50).Enabled = False
            Case Else
        End Select
    End If
End Sub

Private Sub CfgTab_RowSelectionChanged(Index As Integer, ByVal LineNo As Long, ByVal Selected As Boolean)
    Dim tmpTxt As String
    Dim i As Integer
    If (Selected) And (LineNo >= 0) Then
        Select Case Index
            Case 1
                SetMsgCfgRec LineNo, 1
                tmpTxt = ""
                tmpTxt = tmpTxt & CfgTab(Index).GetFieldValue(LineNo, "TYPE")
                tmpTxt = tmpTxt & "/"
                tmpTxt = tmpTxt & CfgTab(Index).GetFieldValue(LineNo, "PRIO")
                For i = 0 To 2
                    TopLabel(i).Tag = tmpTxt
                    TopLabel(i).Caption = tmpTxt
                Next
                ArrangeMsgRow 0
            Case Else
        End Select
    End If
End Sub

Private Sub chkAny_Click(Index As Integer)
    If chkAny(Index).Value = 1 Then
        chkAny(Index).BackColor = LightGreen
        If CfgIdx >= 0 Then ChkCfg(CfgIdx).Value = 0
        If AnyIdx >= 0 Then chkAny(AnyIdx).Value = 0
        AnyIdx = Index
        AnyPanel(Index).Visible = True
        AnyPanel(Index).ZOrder
        Form_Resize
        Select Case Index
            Case 1
                If Trim(AnyText(AnyIdx).Text) = "" Then
                    ChkType(45).Value = 1
                End If
            Case 2
                If Trim(AnyText(AnyIdx).Text) = "" Then
                    ChkType(26).Value = 1
                End If
            Case 3
                If Trim(AnyText(AnyIdx).Text) = "" Then
                    ChkType(53).Value = 1
                End If
            Case Else
        End Select
    Else
        AnyPanel(Index).Visible = False
        AnyIdx = -1
        chkAny(Index).BackColor = vbButtonFace
    End If
End Sub

Private Sub chkBSpace_Click(Index As Integer)
    If RefreshMain Then
        If chkBSpace(Index).Value = 1 Then
            MainForm.DarkSpacer(Index).Visible = True
            chkBSpace(Index).FontBold = True
        Else
            MainForm.DarkSpacer(Index).Visible = False
        End If
        MainForm.ResizeMyForm
    End If
    If chkBSpace(Index).Value = 1 Then
        chkBSpace(Index).FontBold = True
    Else
        chkBSpace(Index).FontBold = False
    End If
End Sub

Private Sub ChkCfg_Click(Index As Integer)
    On Error Resume Next
    If ChkCfg(Index).Value = 1 Then
        If AnyIdx >= 0 Then chkAny(AnyIdx).Value = 0
        If CfgIdx >= 0 Then ChkCfg(CfgIdx).Value = 0
        CfgIdx = Index
        Me.Caption = CfgCaption(CfgIdx)
        Me.Icon = CfgIcon(CfgIdx)
        ChkCfg(CfgIdx).BackColor = LightYellow
        CfgPanel(CfgIdx).Visible = True
        Select Case Index
            Case 1
                SetMsgCfgRec 0, 0
                CfgTab(1).SetCurrentSelection 0
                chkTopLine(0).Value = 1
                ArrangeMsgRow 0
            Case Else
        End Select
        Form_Resize
        CfgTab(CfgIdx).SetFocus
    Else
        If CfgIdx >= 0 Then
            CfgPanel(CfgIdx).Visible = False
            ChkCfg(CfgIdx).BackColor = vbButtonFace
        End If
        Me.Caption = MyCaption
        Me.Icon = MyIcon(0)
        CfgIdx = -1
    End If
End Sub

Private Sub chkMainLayout_Click(Index As Integer)
    If RefreshMain Then
        If chkMainLayout(Index).Value = 1 Then
            Select Case Index
                Case 0
                    MainForm.TopInfoPanel(8).Visible = True
                Case 1
                    MainForm.ButtonPanel(0).Visible = True
                Case 2
                    MainForm.TopInfoPanel(2).Visible = True
                Case 3
                    MainForm.TopInfoPanel(0).Visible = True
                Case 4
                    MainForm.TopInfoPanel(1).Visible = True
                Case 5
                    MainForm.DataTab(0).Visible = True
                    MainForm.DataTab(1).Visible = True
                Case 6
                    MainForm.TopInfoPanel(5).Visible = True
                Case 7
                    MainForm.BotInfoPanel(0).Visible = True
                Case 8
                    MainForm.ButtonPanel(1).Visible = True
                Case 9
                    MainForm.TopInfoPanel(7).Visible = True
                Case Else
            End Select
        Else
            Select Case Index
                Case 0
                    MainForm.TopInfoPanel(8).Visible = False
                Case 1
                    MainForm.ButtonPanel(0).Visible = False
                Case 2
                    MainForm.TopInfoPanel(2).Visible = False
                Case 3
                    MainForm.TopInfoPanel(0).Visible = False
                Case 4
                    MainForm.TopInfoPanel(1).Visible = False
                Case 5
                    MainForm.DataTab(0).Visible = False
                    MainForm.DataTab(1).Visible = False
                Case 6
                    MainForm.TopInfoPanel(5).Visible = False
                Case 7
                    MainForm.BotInfoPanel(0).Visible = False
                Case 8
                    MainForm.ButtonPanel(1).Visible = False
                Case 9
                    MainForm.TopInfoPanel(7).Visible = False
                Case Else
            End Select
        End If
        MainForm.ResizeMyForm
    End If
    If chkMainLayout(Index).Value = 1 Then
        chkDummy(Index).BackColor = LightGreen
        chkMainLayout(Index).BackColor = LightGreen
    Else
        chkDummy(Index).BackColor = LightGrey
        chkMainLayout(Index).BackColor = LightGrey
    End If
End Sub

Private Sub chkSwap_Click(Index As Integer)
    If chkSwap(Index).Value = 1 Then
        L1Color(Index).Left = chkBSpace(Index).Left - L1Color(Index).Width - 15
        L2Color(Index).Left = chkWSpace(Index).Left - L2Color(Index).Width - 15
        MainForm.LightSpacer(Index).BackColor = L2Color(Index).BackColor
        MainForm.DarkSpacer(Index).BackColor = L1Color(Index).BackColor
    Else
        L1Color(Index).Left = chkWSpace(Index).Left - L1Color(Index).Width - 15
        L2Color(Index).Left = chkBSpace(Index).Left - L2Color(Index).Width - 15
        MainForm.LightSpacer(Index).BackColor = L1Color(Index).BackColor
        MainForm.DarkSpacer(Index).BackColor = L2Color(Index).BackColor
    End If
    If chkSwap(Index).Value = 1 Then
        chkSwap(Index).FontBold = True
    Else
        chkSwap(Index).FontBold = False
    End If
End Sub
Private Sub InitMainLayout()
    Dim i As Integer
    RefreshMain = False
    For i = 0 To chkWSpace.UBound
        L1Color(i).BackColor = MainForm.LightSpacer(i).BackColor
        L2Color(i).BackColor = MainForm.DarkSpacer(i).BackColor
        If MainForm.LightSpacer(i).Visible Then chkWSpace(i).Value = 1
        If MainForm.DarkSpacer(i).Visible Then chkBSpace(i).Value = 1
    Next
    If MainForm.TopInfoPanel(8).Visible Then chkMainLayout(0).Value = 1
    If MainForm.ButtonPanel(0).Visible Then chkMainLayout(1).Value = 1
    If MainForm.TopInfoPanel(2).Visible Then chkMainLayout(2).Value = 1
    If MainForm.TopInfoPanel(0).Visible Then chkMainLayout(3).Value = 1
    If MainForm.TopInfoPanel(7).Visible Then chkMainLayout(9).Value = 1
    If MainForm.TopInfoPanel(5).Visible Then chkMainLayout(6).Value = 1
    If MainForm.TopInfoPanel(1).Visible Then chkMainLayout(4).Value = 1
    If MainForm.DataTab(0).Visible Then chkMainLayout(5).Value = 1
    If MainForm.DataTab(1).Visible Then chkMainLayout(5).Value = 1
    If MainForm.BotInfoPanel(0).Visible Then chkMainLayout(7).Value = 1
    If MainForm.ButtonPanel(1).Visible Then chkMainLayout(8).Value = 1
    RefreshMain = True
End Sub
Private Sub ChkSysLib_Click(Index As Integer)
    On Error Resume Next
    If ChkSysLib(Index).Value = 1 Then
        ChkSysLib(Index).BackColor = LightGreen
        Select Case Index
            Case 0
                SysFunc.Show , Me
            Case Else
        End Select
    Else
        Select Case Index
            Case 0
                SysFunc.Hide
            Case Else
        End Select
        ChkSysLib(Index).BackColor = vbButtonFace
    End If
End Sub

Private Sub chkTest_Click(Index As Integer)
    'TypIdx = 1
    'MsgTypeArray(TypIdx).MsgTrgBlkStatus = 0
    'MsgTypeArray(TypIdx).MsgTrgBlkAltBckColor = LightestYellow
    'MsgTypeArray(TypIdx).MsgTrgBlkAltTxtColor = vbBlack
    'MsgTypeArray(TypIdx).MsgTrgBlkDefBckColor = vbButtonFace
    'MsgTypeArray(TypIdx).MsgTrgBlkDefTxtColor = vbRed
End Sub

Private Sub chkTopLine_Click(Index As Integer)
    Dim tmpTag As String
    Dim RowIdx As Integer
    If chkTopLine(Index).Value = 1 Then
        tmpTag = chkTopLine(Index).Tag
        If tmpTag = "" Then tmpTag = "-1"
        RowIdx = Val(tmpTag) + 1
        If RowIdx > MainForm.chkTopLine.UBound Then RowIdx = 0
        'chkTopLine(Index).Caption = MainForm.chkTopLine(RowIdx).Caption
        chkTopLine(Index).Caption = "[" & CStr(RowIdx + 1) & "]"
        LinePanel(0).BackColor = MainForm.LinePanel(RowIdx).BackColor
        tmpTag = CStr(RowIdx)
        chkTopLine(Index).Tag = tmpTag
        chkTopLine(Index).Value = 0
    End If
End Sub

Private Sub ChkType_Click(Index As Integer)
    If ChkType(Index).Value = 1 Then
        ChkType(Index).BackColor = LightGreen
        ChkType(Index).Refresh
        Select Case Index
            Case 18
                BcMonitor.Show , MainForm
            Case 26
                OpenCurrentMainPage
                ChkType(Index).Value = 0
            Case 27
                CreateNewMainPage
                ChkType(Index).Value = 0
            Case 28
                DeployCurrentMainPage "UNDO"
                ChkType(Index).Value = 0
            Case 29
                DeployCurrentMainPage "SAVE"
                ChkType(Index).Value = 0
            Case 30
                DeployCurrentMainPage "DEPLOY"
                ChkType(Index).Value = 0
            Case 41
                SaveGroupConfig "DEPLOY"
                ChkType(Index).Value = 0
            Case 42
                SaveGroupConfig "SAVE"
                ChkType(Index).Value = 0
            Case 43
                SaveGroupConfig "UNDO"
                ChkType(Index).Value = 0
            Case 45
                OpenGroupConfig
                ChkType(Index).Value = 0
            Case 49
                SaveModuleConfig "DEPLOY"
                ChkType(Index).Value = 0
            Case 50
                SaveModuleConfig "SAVE"
                ChkType(Index).Value = 0
            Case 51
                SaveModuleConfig "UNDO"
                ChkType(Index).Value = 0
            Case 53
                OpenModuleConfig
                ChkType(Index).Value = 0
            Case 56
                CheckNewMainPage
                ChkType(Index).Value = 0
            Case 57
                CopyNewMainPage
                ChkType(Index).Value = 0
        End Select
    Else
        ChkType(Index).BackColor = vbButtonFace
        ChkType(Index).Refresh
    End If
End Sub
Private Sub OpenCurrentMainPage()
    Dim LineText As String
    Dim FileText As String
    Dim PageFile As String
    Dim FileLen As Long
    Dim fn As Integer
    NewPagePanel.Visible = False
    CopyPagePanel.Visible = False
    IniActTab(AnyIdx).Visible = True
    MainForm.GetMainFormDataGrid IniActTab(AnyIdx), False, 14
    IniInfoTitle(AnyIdx).Text = MainForm.InfoTitle(1).Caption
    MyCurDispPage = MainForm.PageCode(1).Caption
    fn = FreeFile()
    Open PageLayoutIniFile For Binary Access Read As #fn
    FileLen = LOF(fn)
    FileText = String(FileLen, " ")
    Get #fn, 1, FileText
    Close fn
    StopNestedCalls = True
    AnyText(AnyIdx).Text = FileText
    AnyText(AnyIdx).Tag = FileText
    StopNestedCalls = False
    PageFile = PageLayoutIniFile & ".bak"
    FileText = Dir(PageFile)
    If FileText <> "" Then
        ChkType(28).Enabled = True
        ChkType(30).Enabled = True
    Else
        ChkType(28).Enabled = False
        ChkType(30).Enabled = False
    End If
    ChkType(29).Enabled = False
    IniActTab(AnyIdx).AutoSizeColumns
    CurPageNoLft(AnyIdx).Caption = MyCurDispPage
    CurPageNoRgt(AnyIdx).Caption = MyCurDispPage
End Sub
Private Sub CreateNewMainPage()
    IniActTab(AnyIdx).Visible = False
    NewPagePanel.Visible = True
    CopyPagePanel.Visible = True
    NewPageNo.SetFocus
End Sub
Private Sub CheckNewMainPage()
    Dim tmpNumber As String
    Dim FileName As String
    Dim PageIniFile As String
    Dim tmpData As String
    'IniActTab(AnyIdx).Visible = False
    tmpNumber = Trim(NewPageNo.Text)
    tmpNumber = Right("0000" & tmpNumber, 4)
    
    FileName = tmpNumber
    FileName = "PAGE_" & FileName
    FileName = FileName & ".INI"
    PageIniFile = MyConfigPath & "\" & FileName
    tmpData = Dir(PageIniFile)
    If UCase(tmpData) = UCase(FileName) Then
        ThisIsANewPage = False
        MainForm.HandleOpenPage tmpNumber
        ChkType(26).Value = 1
    Else
        ThisIsANewPage = True
        NewPageFileName = tmpNumber
        NewPageFilePath = PageIniFile
        If Trim(CopyPageNo.Text) <> "" Then
            CopyNewMainPage
        Else
            tmpData = ""
            tmpData = tmpData & "[PAGE_GLOBAL]" & vbNewLine
            tmpData = tmpData & vbNewLine
            tmpData = tmpData & "[PAGE_MASTER]" & vbNewLine
            tmpData = tmpData & vbNewLine
            AnyText(AnyIdx).Tag = ""
            AnyText(AnyIdx).Text = tmpData
        End If
        ChkType(29).Enabled = True
        MyCurDispPage = CStr(Val(tmpNumber))
        CurPageNoLft(AnyIdx).Caption = MyCurDispPage
        CurPageNoRgt(AnyIdx).Caption = MyCurDispPage
    End If
End Sub
Private Sub CopyNewMainPage()
    Dim tmpNumber As String
    Dim FileName As String
    Dim FileText As String
    Dim PageIniFile As String
    Dim tmpData As String
    Dim FileLen As Long
    Dim PosBgn As Long
    Dim PosEnd As Long
    Dim fn As Integer
    tmpNumber = Trim(CopyPageNo.Text)
    tmpNumber = Right("0000" & tmpNumber, 4)
    FileName = tmpNumber
    FileName = "PAGE_" & FileName
    FileName = FileName & ".INI"
    PageIniFile = MyConfigPath & "\" & FileName
    tmpData = Dir(PageIniFile)
    If UCase(tmpData) = UCase(FileName) Then
        fn = FreeFile()
        Open PageIniFile For Binary Access Read As #fn
        FileLen = LOF(fn)
        FileText = String(FileLen, " ")
        Get #fn, 1, FileText
        Close fn
        StopNestedCalls = True
        PosBgn = InStr(FileText, "PAGE_NUMBER")
        If PosBgn > 0 Then
            PosEnd = InStr(PosBgn, FileText, vbNewLine)
            If PosEnd > 0 Then
                FileText = Left(FileText, PosBgn - 1) & Mid(FileText, PosEnd)
            End If
        End If
        PosBgn = InStr(FileText, "PAGE_GROUP")
        If PosBgn > 0 Then
            PosEnd = InStr(PosBgn, FileText, vbNewLine)
            If PosEnd > 0 Then
                FileText = Left(FileText, PosBgn - 1) & Mid(FileText, PosEnd)
            End If
        End If
        AnyText(AnyIdx).Text = FileText
        AnyText(AnyIdx).Tag = FileText & "@@"
        StopNestedCalls = False
        ChkType(29).Enabled = True
    End If
End Sub
Private Sub DeployCurrentMainPage(ForWhat As String)
    Dim PageNumber As String
    Dim PageFile As String
    Dim FileText As String
    Dim fn As Integer
    Select Case ForWhat
        Case "SAVE"
            If ThisIsANewPage Then
                PageFile = NewPageFilePath
                FileText = Dir(PageFile)
                If FileText = "" Then
                    FileText = AnyText(AnyIdx).Text
                    fn = FreeFile()
                    Open NewPageFilePath For Binary Access Write As #fn
                    Put #fn, 1, FileText
                    Close #fn
                    PageNumber = NewPageFileName
                    MainForm.HandleOpenPage PageNumber
                    If PageNumber < "0010" Then ChkType(26).Value = 1
                End If
                ThisIsANewPage = False
            Else
                PageFile = PageLayoutIniFile & ".bak"
                FileText = Dir(PageFile)
                If FileText = "" Then
                    Name PageLayoutIniFile As PageFile
                    ChkType(28).Enabled = True
                Else
                    Kill PageLayoutIniFile
                    ChkType(28).Enabled = True
                End If
                FileText = AnyText(2).Text
                fn = FreeFile()
                Open PageLayoutIniFile For Binary Access Write As #fn
                Put #fn, 1, FileText
                Close #fn
                PageNumber = MainForm.MyActivePageNumber
                MainForm.HandleOpenPage PageNumber
                If PageNumber < "0010" Then ChkType(26).Value = 1
            End If
        Case "UNDO"
            If Not ThisIsANewPage Then
                PageFile = PageLayoutIniFile & ".bak"
                FileText = Dir(PageFile)
                If FileText <> "" Then
                    Kill PageLayoutIniFile
                    Name PageFile As PageLayoutIniFile
                    PageNumber = MainForm.MyActivePageNumber
                    MainForm.HandleOpenPage PageNumber
                    If PageNumber < "0010" Then ChkType(26).Value = 1
                End If
            End If
        Case "DEPLOY"
            If Not ThisIsANewPage Then
                PageFile = PageLayoutIniFile & ".bak"
                FileText = Dir(PageFile)
                If FileText <> "" Then
                    Kill PageFile
                    PageNumber = MainForm.MyActivePageNumber
                    MainForm.HandleOpenPage PageNumber
                    If PageNumber < "0010" Then ChkType(26).Value = 1
                End If
                ChkType(28).Enabled = False
            End If
        Case Else
    End Select
End Sub
Private Sub OpenGroupConfig()
    Dim LineText As String
    Dim FileText As String
    Dim PageFile As String
    Dim FileLen As Long
    Dim fn As Integer
    'NewPagePanel.Visible = False
    'CopyPagePanel.Visible = False
    'IniActTab(AnyIdx).Visible = True
    'MainForm.GetMainFormDataGrid IniActTab(AnyIdx), False, 14
    'IniInfoTitle(AnyIdx).Text = MainForm.InfoTitle(1).Caption
    fn = FreeFile()
    Open PageGroupIniFile For Binary Access Read As #fn
    FileLen = LOF(fn)
    FileText = String(FileLen, " ")
    Get #fn, 1, FileText
    Close fn
    StopNestedCalls = True
    AnyText(AnyIdx).Text = FileText
    AnyText(AnyIdx).Tag = FileText
    StopNestedCalls = False
    PageFile = PageGroupIniFile & ".bak"
    FileText = Dir(PageFile)
    If FileText <> "" Then
        ChkType(43).Enabled = True
        ChkType(41).Enabled = True
    Else
        ChkType(43).Enabled = False
        ChkType(41).Enabled = False
    End If
    ChkType(42).Enabled = False
    IniActTab(AnyIdx).AutoSizeColumns
End Sub
Private Sub SaveGroupConfig(ForWhat As String)
    Dim PageNumber As String
    Dim PageFile As String
    Dim FileText As String
    Dim fn As Integer
    Select Case ForWhat
        Case "SAVE"
            PageFile = PageGroupIniFile & ".bak"
            FileText = Dir(PageFile)
            If FileText = "" Then
                Name PageGroupIniFile As PageFile
                ChkType(43).Enabled = True
            Else
                Kill PageGroupIniFile
                ChkType(43).Enabled = True
            End If
            FileText = AnyText(AnyIdx).Text
            fn = FreeFile()
            Open PageGroupIniFile For Binary Access Write As #fn
            Put #fn, 1, FileText
            Close #fn
            ChkType(45).Value = 1
            'PageNumber = MainForm.MyActivePageNumber
            'MainForm.HandleOpenPage PageNumber
            'If PageNumber < "0010" Then ChkType(26).Value = 1
        Case "UNDO"
            PageFile = PageGroupIniFile & ".bak"
            FileText = Dir(PageFile)
            If FileText <> "" Then
                Kill PageGroupIniFile
                Name PageFile As PageGroupIniFile
                'PageNumber = MainForm.MyActivePageNumber
                'MainForm.HandleOpenPage PageNumber
                'If PageNumber < "0010" Then ChkType(26).Value = 1
            End If
            ChkType(45).Value = 1
        Case "DEPLOY"
            PageFile = PageGroupIniFile & ".bak"
            FileText = Dir(PageFile)
            If FileText <> "" Then
                Kill PageFile
                'PageNumber = MainForm.MyActivePageNumber
                'MainForm.HandleOpenPage PageNumber
                'If PageNumber < "0010" Then ChkType(26).Value = 1
            End If
            ChkType(43).Enabled = False
            ChkType(41).Enabled = False
        Case Else
    End Select
End Sub
Private Sub OpenModuleConfig()
    Dim LineText As String
    Dim FileText As String
    Dim PageFile As String
    Dim FileLen As Long
    Dim fn As Integer
    'NewPagePanel.Visible = False
    'CopyPagePanel.Visible = False
    'IniActTab(AnyIdx).Visible = True
    'MainForm.GetMainFormDataGrid IniActTab(AnyIdx), False, 14
    'IniInfoTitle(AnyIdx).Text = MainForm.InfoTitle(1).Caption
    fn = FreeFile()
    Open StaffPageIniFile For Binary Access Read As #fn
    FileLen = LOF(fn)
    FileText = String(FileLen, " ")
    Get #fn, 1, FileText
    Close fn
    StopNestedCalls = True
    AnyText(AnyIdx).Text = FileText
    AnyText(AnyIdx).Tag = FileText
    StopNestedCalls = False
    PageFile = StaffPageIniFile & ".bak"
    FileText = Dir(PageFile)
    If FileText <> "" Then
        ChkType(51).Enabled = True
        ChkType(49).Enabled = True
    Else
        ChkType(51).Enabled = False
        ChkType(49).Enabled = False
    End If
    ChkType(50).Enabled = False
    IniActTab(AnyIdx).AutoSizeColumns
End Sub
Private Sub SaveModuleConfig(ForWhat As String)
    Dim PageNumber As String
    Dim PageFile As String
    Dim FileText As String
    Dim fn As Integer
    Select Case ForWhat
        Case "SAVE"
            PageFile = StaffPageIniFile & ".bak"
            FileText = Dir(PageFile)
            If FileText = "" Then
                Name StaffPageIniFile As PageFile
                ChkType(51).Enabled = True
            Else
                Kill StaffPageIniFile
                ChkType(51).Enabled = True
            End If
            FileText = AnyText(AnyIdx).Text
            fn = FreeFile()
            Open StaffPageIniFile For Binary Access Write As #fn
            Put #fn, 1, FileText
            Close #fn
            ChkType(53).Value = 1
            'PageNumber = MainForm.MyActivePageNumber
            'MainForm.HandleOpenPage PageNumber
            'If PageNumber < "0010" Then ChkType(26).Value = 1
            MainForm.InitApplication
        Case "UNDO"
            PageFile = StaffPageIniFile & ".bak"
            FileText = Dir(PageFile)
            If FileText <> "" Then
                Kill StaffPageIniFile
                Name PageFile As StaffPageIniFile
                'PageNumber = MainForm.MyActivePageNumber
                'MainForm.HandleOpenPage PageNumber
                'If PageNumber < "0010" Then ChkType(26).Value = 1
            End If
            ChkType(53).Value = 1
        Case "DEPLOY"
            PageFile = StaffPageIniFile & ".bak"
            FileText = Dir(PageFile)
            If FileText <> "" Then
                Kill PageFile
                'PageNumber = MainForm.MyActivePageNumber
                'MainForm.HandleOpenPage PageNumber
                'If PageNumber < "0010" Then ChkType(26).Value = 1
            End If
            ChkType(51).Enabled = False
            ChkType(49).Enabled = False
        Case Else
    End Select
End Sub
Private Sub chkWSpace_Click(Index As Integer)
    If RefreshMain Then
        If chkWSpace(Index).Value = 1 Then
            MainForm.LightSpacer(Index).Visible = True
        Else
            MainForm.LightSpacer(Index).Visible = False
        End If
        MainForm.ResizeMyForm
    End If
    If chkWSpace(Index).Value = 1 Then
        chkWSpace(Index).FontBold = True
    Else
        chkWSpace(Index).FontBold = False
    End If
End Sub

Private Sub Form_Activate()
    Static IsInit As Boolean
    If Not IsInit Then
        IsInit = True
        If AnyIdx < 0 Then
            MyCaption = Me.Caption
            'chkAny(2).Value = 1
            'ChkType(26).Value = 1
        End If
    End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If AnyIdx < 0 Then
        MainForm.KeyCodeInput KeyCode, Shift, 0, ""
    End If
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    If AnyIdx < 0 Then
        MainForm.KeyCodeInput KeyAscii, 0, 3, ""
    End If
End Sub

Private Sub Form_Load()
    Dim tmpTag As String
    Dim i As Integer
    On Error Resume Next
    CfgIdx = -1
    AnyIdx = -1
    For i = 0 To CfgPanel.UBound
        CfgPanel(i).Visible = False
        CfgPanel(i).BackColor = vbButtonFace
    Next
    For i = 0 To AnyPanel.UBound
        AnyPanel(i).Visible = False
        AnyPanel(i).BackColor = vbButtonFace
        IniFrame(i).Top = 90
        IniFrame(i).Left = 60
        AnyText(i).Left = IniFrame(i).Left
        AnyText(i).Top = IniFrame(i).Top + IniFrame(i).Height + 30
        IniActTab(i).Left = 0
        IniActTab(i).Top = 0
        IniActTab(i).ShowHorzScroller True
        IniActTab(i).ShowVertScroller False
        IniBtnPanel(i).ZOrder
        CurPageNoRgt(i).ZOrder
    Next
    tmpTag = Me.Tag
    If tmpTag = "" Then tmpTag = "7305,7920"
    Me.Width = Val(GetItem(tmpTag, 1, ","))
    Me.Height = Val(GetItem(tmpTag, 2, ","))
    Me.Icon = MyIcon(0)
    InitMainLayout
    InitCfgTab 0
    InitCfgTab 1
    InitCfgTab 3
    InitMsgInfoTypes CfgTab(1)
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode >= 0 Then
        Cancel = True
        Me.Hide
    End If
End Sub

Private Sub Form_Resize()
    Dim NewTop As Long
    Dim NewSize As Long
    On Error Resume Next
    If AnyIdx >= 0 Then
        NewSize = Me.ScaleWidth
        NewTop = ChkCfg(0).Top + ChkCfg(0).Height + 30
        AnyPanel(AnyIdx).Left = 0
        AnyPanel(AnyIdx).Top = NewTop
        If NewSize > 120 Then AnyPanel(AnyIdx).Width = NewSize
        NewSize = AnyPanel(AnyIdx).ScaleWidth - (AnyText(AnyIdx).Left * 2) + 30
        If NewSize > 120 Then
            AnyText(AnyIdx).Width = NewSize
            IniFrame(AnyIdx).Width = NewSize
            IniBtnPanel(AnyIdx).Left = NewSize - IniBtnPanel(AnyIdx).Width - 90
            NewSize = IniBtnPanel(AnyIdx).Left - IniTabPanel(AnyIdx).Left - 60
            If NewSize > 120 Then IniTabPanel(AnyIdx).Width = NewSize
            NewSize = IniFrame(AnyIdx).Width - IniCapPanel(AnyIdx).Left - 90
            IniCapPanel(AnyIdx).Width = NewSize
            CurPageNoRgt(AnyIdx).Left = IniCapPanel(AnyIdx).ScaleWidth - CurPageNoRgt(AnyIdx).Width
            NewSize = CurPageNoRgt(AnyIdx).Left - IniInfoTitle(AnyIdx).Left - 15
            IniInfoTitle(AnyIdx).Width = NewSize
            IniActTab(AnyIdx).Width = IniTabPanel(AnyIdx).ScaleWidth
            IniActTab(AnyIdx).Height = IniTabPanel(AnyIdx).ScaleHeight
        End If
        NewSize = Me.ScaleHeight - NewTop
        If NewSize > 120 Then AnyPanel(AnyIdx).Height = NewSize
        NewSize = AnyPanel(AnyIdx).ScaleHeight - AnyText(AnyIdx).Top - 30
        If NewSize > 120 Then AnyText(AnyIdx).Height = NewSize
    End If
    If CfgIdx >= 0 Then
        NewSize = Me.ScaleWidth
        NewTop = ChkCfg(0).Top + ChkCfg(0).Height + 30
        CfgPanel(CfgIdx).Left = 0
        CfgPanel(CfgIdx).Top = NewTop
        If NewSize > 120 Then CfgPanel(CfgIdx).Width = NewSize
        NewSize = Me.ScaleHeight - NewTop
        If NewSize > 120 Then CfgPanel(CfgIdx).Height = NewSize
        CfgTab(CfgIdx).Top = 0
        CfgTab(CfgIdx).Left = 0
        Select Case CfgIdx
            Case 0
                NewSize = CfgPanel(CfgIdx).ScaleWidth
                If NewSize > 120 Then CfgTab(CfgIdx).Width = NewSize
                NewSize = CfgPanel(CfgIdx).ScaleHeight
                If NewSize > 120 Then CfgTab(CfgIdx).Height = NewSize
            Case 1
                NewSize = MsgPanel(0).ScaleWidth
                If NewSize > 120 Then CfgTab(CfgIdx).Width = NewSize
                NewSize = MsgPanel(0).ScaleHeight
                If NewSize > 120 Then CfgTab(CfgIdx).Height = NewSize
                chkTest(0).Left = TopInfoPanel(0).ScaleWidth - chkTest(0).Width
                TopLine(0).Width = chkTest(0).Left - TopLine(0).Left
                LinePanel(0).Width = TopLine(0).ScaleWidth - LinePanel(0).Left
            Case Else
                NewSize = CfgPanel(CfgIdx).ScaleWidth
                If NewSize > 120 Then CfgTab(CfgIdx).Width = NewSize
                NewSize = CfgPanel(CfgIdx).ScaleHeight
                If NewSize > 120 Then CfgTab(CfgIdx).Height = NewSize
        End Select
    End If
End Sub

Private Sub CfgTab_HitKeyOnLine(Index As Integer, ByVal Key As Integer, ByVal LineNo As Long)
    MainForm.KeyCodeInput Key, 0, 1, ""
End Sub

Private Sub L1Color_Click(Index As Integer)
    Dim ObjColor As Long
    ObjColor = L1Color(Index).BackColor
    ObjColor = GetObjectColor(ObjColor)
    L1Color(Index).BackColor = ObjColor
    MainForm.LightSpacer(Index).BackColor = ObjColor
End Sub

Private Sub L2Color_Click(Index As Integer)
    Dim ObjColor As Long
    ObjColor = L2Color(Index).BackColor
    ObjColor = GetObjectColor(ObjColor)
    L2Color(Index).BackColor = ObjColor
    MainForm.DarkSpacer(Index).BackColor = ObjColor
End Sub

Private Sub MsgTimer_Click(Index As Integer)
    Dim idx As Integer
    If Not Initializing Then GetMsgCfgRec -1, True
End Sub

Private Sub MsgTimer_Scroll(Index As Integer)
    If Not Initializing Then GetMsgCfgRec -1, True
End Sub

Private Sub NewPageNo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then ChkType(56).Value = 1
End Sub

Private Sub TypeProp_Click(Index As Integer)
    Dim tmpTxt As String
    Dim tmpTag As String
    Dim tmpArea As String
    Dim tmpMean As String
    Dim ObjColor As Long
    Dim LineNo As Long
    Dim MsgIdx As Integer
    'If Not Initializing Then
        tmpTag = TypeProp(Index).Tag
        tmpArea = ""
        tmpMean = ""
        If tmpTag <> "" Then
            tmpArea = GetItem(tmpTag, 1, ".")
            tmpMean = GetItem(tmpTag, 2, ".")
        End If
        Select Case tmpArea
            Case "NORM"
                MsgIdx = 0
            Case "ANI1"
                MsgIdx = 1
            Case "ANI2"
                MsgIdx = 2
            Case Else
            MsgIdx = -1
        End Select
        If MsgIdx >= 0 Then
            Select Case tmpMean
                Case "TRNS"
                    If TypeProp(Index).Value = 1 Then
                        TopLabel(MsgIdx).BackStyle = 0
                    Else
                        TopLabel(MsgIdx).BackStyle = 1
                    End If
                Case "BORD"
                    If TypeProp(Index).Value = 1 Then
                        TopLabel(MsgIdx).BorderStyle = 1
                        TopLabel(MsgIdx).Top = -30
                    Else
                        TopLabel(MsgIdx).BorderStyle = 0
                        TopLabel(MsgIdx).Top = 0
                    End If
                Case "TRIM"
                    tmpTxt = TopLabel(MsgIdx).Caption
                    tmpTag = TopLabel(MsgIdx).Tag
                    If TypeProp(Index).Value = 1 Then
                        tmpTxt = Trim(tmpTxt)
                        tmpTag = Trim(tmpTag)
                    Else
                        tmpTxt = Trim(tmpTxt)
                        tmpTxt = " " & tmpTxt & " "
                        tmpTag = Trim(tmpTag)
                        tmpTag = " " & tmpTag & " "
                    End If
                    TopLabel(MsgIdx).Caption = tmpTxt
                    TopLabel(MsgIdx).Tag = tmpTag
                Case "UPCS"
                    tmpTag = TopLabel(MsgIdx).Tag
                    If TypeProp(Index).Value = 1 Then
                        tmpTxt = UCase(tmpTag)
                    Else
                        tmpTxt = tmpTag
                    End If
                    TopLabel(MsgIdx).Caption = tmpTxt
                Case "TXCO"
                    If TypeProp(Index).Value = 1 Then
                        ObjColor = MsgTxtColor(MsgIdx).BackColor
                        If Not Initializing Then ObjColor = GetObjectColor(ObjColor)
                        MsgTxtColor(MsgIdx).BackColor = ObjColor
                        TopLabel(MsgIdx).ForeColor = ObjColor
                        TypeProp(Index).Value = 0
                    End If
                Case "BKCO"
                    If TypeProp(Index).Value = 1 Then
                        ObjColor = MsgBckColor(MsgIdx).BackColor
                        If Not Initializing Then ObjColor = GetObjectColor(ObjColor)
                        MsgBckColor(MsgIdx).BackColor = ObjColor
                        TopLabel(MsgIdx).BackColor = ObjColor
                        TypeProp(Index).Value = 0
                    End If
                Case "SEPB"
                    If TypeProp(Index).Value = 1 Then
                        TopSep(MsgIdx).BorderStyle = 1
                    Else
                        TopSep(MsgIdx).BorderStyle = 0
                    End If
                Case "SEPC"
                    If TypeProp(Index).Value = 1 Then
                        ObjColor = SepBckColor(MsgIdx).BackColor
                        If Not Initializing Then ObjColor = GetObjectColor(ObjColor)
                        SepBckColor(MsgIdx).BackColor = ObjColor
                        TopSep(MsgIdx).BackColor = ObjColor
                        TypeProp(Index).Value = 0
                    End If
                Case Else
            End Select
            ArrangeMsgRow 0
            If Not Initializing Then
                LineNo = CfgTab(1).GetCurrentSelected
                GetMsgCfgRec LineNo, True
            End If
        End If
    'End If
End Sub
Private Function GetObjectColor(CurColor As Long) As Long
    Dim UseColor As Long
    On Error Resume Next
    UseColor = CurColor
    CommonDialog.Flags = cdlCCRGBInit
    CommonDialog.Color = UseColor
    CommonDialog.ShowColor
    UseColor = CommonDialog.Color
    GetObjectColor = UseColor
End Function
Private Sub ArrangeMsgRow(RowIdx As Integer)
    Dim OldTag As String
    Dim NewTag As String
    Dim tmpItm As String
    Dim NewLeft As Long
    Dim MaxLeft As Long
    Dim MsgIdx As Integer
    Dim MaxIdx As Integer
    Dim itm As Integer
    OldTag = LinePanel(RowIdx).Tag
    If OldTag = "" Then OldTag = "0,1,2"
    If OldTag <> "" Then
        MaxIdx = -1
        MaxLeft = LinePanel(RowIdx).ScaleWidth
        NewTag = ""
        NewLeft = 0
        tmpItm = "START"
        itm = 0
        While tmpItm <> ""
            itm = itm + 1
            tmpItm = GetItem(OldTag, itm, ",")
            If tmpItm <> "" Then
                MsgIdx = Val(tmpItm)
                If NewLeft < MaxLeft Then
                    NewTag = NewTag & CStr(MsgIdx) & ","
                    TopLabel(MsgIdx).Left = NewLeft
                    NewLeft = NewLeft + TopLabel(MsgIdx).Width
                    TopSep(MsgIdx).Left = NewLeft
                    NewLeft = NewLeft + TopSep(MsgIdx).Width
                    MaxIdx = MsgIdx
                Else
                    'CleanUpMsgLabels MsgIdx, 0
                End If
            End If
        Wend
        LinePanel(RowIdx).Tag = NewTag
        If MaxIdx >= 0 Then
            TopSep(MaxIdx).Visible = False
        End If
    End If
End Sub

Private Sub GetMsgCfgRec(UseLineNo As Long, SaveIt As Boolean)
    Dim tmpTxt As String
    Dim tmpTag As String
    Dim tmpArea As String
    Dim tmpMean As String
    Dim tmpCode As String
    Dim tmpData As String
    Dim tmpValu As String
    Dim MeanCode(0 To 2) As String
    Dim MeanData(0 To 2) As String
    Dim ObjColor As Long
    Dim LineNo As Long
    Dim MsgIdx As Integer
    Dim ObjIdx As Integer
    Dim i As Integer
    LineNo = UseLineNo
    If LineNo < 0 Then LineNo = CfgTab(1).GetCurrentSelected
    If LineNo >= 0 Then
        tmpData = "NORM,ANI1,ANI2"
        For MsgIdx = 0 To 2
            MeanCode(MsgIdx) = GetItem(tmpData, (MsgIdx + 1), ",")
            MeanData(MsgIdx) = ""
        Next
        For ObjIdx = 0 To TypeProp.UBound
            tmpArea = ""
            tmpMean = ""
            tmpTag = TypeProp(ObjIdx).Tag
            If tmpTag <> "" Then
                tmpArea = GetItem(tmpTag, 1, ".")
                tmpMean = GetItem(tmpTag, 2, ".")
            End If
            MsgIdx = -1
            For i = 0 To 2
                If (tmpArea = MeanCode(i)) Then
                    MsgIdx = i
                    Exit For
                End If
            Next
            If MsgIdx >= 0 Then
                tmpCode = "#" & tmpMean & "#"
                Select Case tmpMean
                    Case "TXCO"
                        ObjColor = MsgTxtColor(MsgIdx).BackColor
                        tmpData = CStr(ObjColor)
                    Case "BKCO"
                        ObjColor = MsgBckColor(MsgIdx).BackColor
                        tmpData = CStr(ObjColor)
                    Case "SEPC"
                        ObjColor = SepBckColor(MsgIdx).BackColor
                        tmpData = CStr(ObjColor)
                    Case "TIME"
                        tmpData = CStr(MsgTimer(MsgIdx).Value)
                    Case Else
                        tmpData = CStr(TypeProp(ObjIdx).Value)
                End Select
                tmpValu = tmpCode & tmpData
                MeanData(MsgIdx) = MeanData(MsgIdx) & tmpValu
            End If
        Next
        If LineNo >= 0 Then
            For MsgIdx = 0 To 2
                tmpArea = MeanCode(MsgIdx)
                tmpData = MeanData(MsgIdx)
                CfgTab(1).SetFieldValues LineNo, tmpArea, tmpData
            Next
            If SaveIt Then
                CfgTab(1).WriteToFile InfoTypesFile, False
            End If
            CfgTab(1).AutoSizeColumns
        End If
    End If
End Sub

Private Sub SetMsgCfgRec(LineNo As Long, ForWhat As Integer)
    Dim AllArea As String
    Dim tmpArea As String
    Dim tmpList As String
    Dim tmpProp As String
    Dim tmpData As String
    Dim MaxLine As Long
    Dim CurLine As Long
    Dim itm As Integer
    Dim DatIdx As Integer
    Dim ObjIdx As Integer
    Initializing = True
    AllArea = "NORM,ANI1,ANI2"
    Select Case ForWhat
        Case 0
            tmpArea = GetItem(AllArea, 1, ",")
            tmpList = CfgTab(1).GetFieldValue(0, tmpArea)
            If tmpList = "" Then
                GetMsgCfgRec 0, False
                tmpList = CfgTab(1).GetFieldValues(0, AllArea)
                MaxLine = CfgTab(1).GetLineCount - 1
                For CurLine = 1 To MaxLine
                    CfgTab(1).SetFieldValues CurLine, AllArea, tmpList
                Next
            End If
        Case 1
            itm = 0
            tmpArea = "START"
            While tmpArea <> ""
                itm = itm + 1
                tmpArea = GetItem(AllArea, itm, ",")
                If tmpArea <> "" Then
                    tmpList = CfgTab(1).GetFieldValue(LineNo, tmpArea)
                    tmpList = Mid(tmpList, 2)
                    DatIdx = 0
                    tmpProp = "START"
                    While tmpProp <> ""
                        DatIdx = DatIdx + 1
                        tmpProp = GetItem(tmpList, DatIdx, "#")
                        DatIdx = DatIdx + 1
                        tmpData = GetItem(tmpList, DatIdx, "#")
                        ObjIdx = GetIdxByTypeProp(tmpArea, tmpProp)
                        Select Case tmpProp
                            Case "BKCO"
                                MsgBckColor(itm - 1).BackColor = Val(tmpData)
                                If ObjIdx >= 0 Then TypeProp(ObjIdx).Value = 1
                            Case "SEPC"
                                SepBckColor(itm - 1).BackColor = Val(tmpData)
                                If ObjIdx >= 0 Then TypeProp(ObjIdx).Value = 1
                            Case "TXCO"
                                MsgTxtColor(itm - 1).BackColor = Val(tmpData)
                                If ObjIdx >= 0 Then TypeProp(ObjIdx).Value = 1
                            Case "TIME"
                                MsgTimer(itm - 1).Value = Val(tmpData)
                            Case Else
                                If ObjIdx >= 0 Then TypeProp(ObjIdx).Value = Val(tmpData)
                        End Select
                    Wend
                End If
            Wend
            FillMyInfoRec CfgTab(1), LineNo, CurCfgInfoRec
        Case Else
    End Select
    Initializing = False
End Sub
Private Function GetIdxByTypeProp(CurArea As String, CurType As String) As Integer
    Dim tmpProp As String
    Dim tmpTag As String
    Dim i As Integer
    Dim idx As Integer
    idx = -1
    tmpProp = CurArea & "." & CurType
    For i = 0 To TypeProp.UBound
        tmpTag = TypeProp(i).Tag
        If tmpTag = tmpProp Then
            idx = i
            Exit For
        End If
    Next
    GetIdxByTypeProp = idx
End Function

