VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{A2F31E92-C74F-11D3-A251-00500437F607}#1.0#0"; "UfisCom.ocx"
Object = "{7DA325C2-D4A4-11D3-9B36-020128B0A0FF}#1.0#0"; "UfisBroadcasts.ocx"
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MainForm 
   BorderStyle     =   5  'Sizable ToolWindow
   Caption         =   "Staff Monitor"
   ClientHeight    =   9600
   ClientLeft      =   165
   ClientTop       =   435
   ClientWidth     =   13410
   FillColor       =   &H00C0C0C0&
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "MainForm.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   640
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   894
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox TopInfoPanel 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1485
      Index           =   14
      Left            =   2340
      ScaleHeight     =   1485
      ScaleWidth      =   4680
      TabIndex        =   160
      TabStop         =   0   'False
      Top             =   4830
      Visible         =   0   'False
      Width           =   4680
      Begin VB.PictureBox TopInfoPanel 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1425
         Index           =   15
         Left            =   60
         ScaleHeight     =   1365
         ScaleWidth      =   4500
         TabIndex        =   161
         TabStop         =   0   'False
         Top             =   0
         Width           =   4560
         Begin TABLib.TAB MsgTplTab 
            Height          =   1365
            Index           =   0
            Left            =   0
            TabIndex        =   162
            TabStop         =   0   'False
            Top             =   0
            Width           =   2505
            _Version        =   65536
            _ExtentX        =   4419
            _ExtentY        =   2408
            _StockProps     =   64
         End
      End
   End
   Begin VB.PictureBox TopInfoPanel 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   13
      Left            =   0
      ScaleHeight     =   315
      ScaleWidth      =   2145
      TabIndex        =   158
      TabStop         =   0   'False
      Top             =   4290
      Visible         =   0   'False
      Width           =   2145
      Begin VB.CheckBox chkTool 
         Caption         =   "Text Templates"
         Height          =   300
         Index           =   20
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   159
         TabStop         =   0   'False
         Top             =   0
         Width           =   1770
      End
   End
   Begin VB.PictureBox TopInfoPanel 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   12
      Left            =   30
      ScaleHeight     =   300
      ScaleWidth      =   11205
      TabIndex        =   141
      TabStop         =   0   'False
      Top             =   1980
      Visible         =   0   'False
      Width           =   11265
      Begin VB.CheckBox chkTool 
         Caption         =   "Delete"
         Enabled         =   0   'False
         Height          =   300
         Index           =   26
         Left            =   9000
         Style           =   1  'Graphical
         TabIndex        =   156
         TabStop         =   0   'False
         Top             =   0
         Width           =   780
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Update"
         Enabled         =   0   'False
         Height          =   300
         Index           =   25
         Left            =   8220
         Style           =   1  'Graphical
         TabIndex        =   155
         TabStop         =   0   'False
         Top             =   0
         Width           =   780
      End
      Begin VB.PictureBox MsgTypBut 
         BorderStyle     =   0  'None
         Height          =   300
         Left            =   6210
         ScaleHeight     =   300
         ScaleWidth      =   1200
         TabIndex        =   150
         TabStop         =   0   'False
         Top             =   0
         Width           =   1200
         Begin VB.OptionButton optType 
            Caption         =   "1"
            Height          =   300
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   154
            TabStop         =   0   'False
            Top             =   0
            Width           =   300
         End
         Begin VB.OptionButton optType 
            Caption         =   "2"
            Height          =   300
            Index           =   1
            Left            =   300
            Style           =   1  'Graphical
            TabIndex        =   153
            TabStop         =   0   'False
            Top             =   0
            Width           =   300
         End
         Begin VB.OptionButton optType 
            Caption         =   "3"
            Height          =   300
            Index           =   2
            Left            =   600
            Style           =   1  'Graphical
            TabIndex        =   152
            TabStop         =   0   'False
            Top             =   0
            Width           =   300
         End
         Begin VB.OptionButton optType 
            Caption         =   "4"
            Height          =   300
            Index           =   3
            Left            =   900
            Style           =   1  'Graphical
            TabIndex        =   151
            TabStop         =   0   'False
            Top             =   0
            Width           =   300
         End
      End
      Begin VB.TextBox txtMsgText 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   3240
         TabIndex        =   148
         TabStop         =   0   'False
         Top             =   -15
         Width           =   2955
      End
      Begin VB.OptionButton optArea 
         Caption         =   "5"
         Height          =   300
         Index           =   4
         Left            =   1215
         Style           =   1  'Graphical
         TabIndex        =   147
         TabStop         =   0   'False
         Top             =   0
         Width           =   300
      End
      Begin VB.OptionButton optArea 
         Caption         =   "4"
         Height          =   300
         Index           =   3
         Left            =   915
         Style           =   1  'Graphical
         TabIndex        =   146
         TabStop         =   0   'False
         Top             =   0
         Width           =   300
      End
      Begin VB.OptionButton optArea 
         Caption         =   "3"
         Height          =   300
         Index           =   2
         Left            =   615
         Style           =   1  'Graphical
         TabIndex        =   145
         TabStop         =   0   'False
         Top             =   0
         Width           =   300
      End
      Begin VB.OptionButton optArea 
         Caption         =   "2"
         Height          =   300
         Index           =   1
         Left            =   315
         Style           =   1  'Graphical
         TabIndex        =   144
         TabStop         =   0   'False
         Top             =   0
         Width           =   300
      End
      Begin VB.OptionButton optArea 
         Caption         =   "1"
         Height          =   300
         Index           =   0
         Left            =   15
         Style           =   1  'Graphical
         TabIndex        =   143
         TabStop         =   0   'False
         Top             =   0
         Width           =   300
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Submit"
         Height          =   300
         Index           =   24
         Left            =   7440
         Style           =   1  'Graphical
         TabIndex        =   142
         TabStop         =   0   'False
         Top             =   0
         Width           =   780
      End
      Begin VB.Label AreaMean 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "RESOURCE CHANGES"
         ForeColor       =   &H00000000&
         Height          =   195
         Left            =   1545
         TabIndex        =   149
         Top             =   60
         Width           =   1695
      End
   End
   Begin VB.PictureBox ButtonPanel 
      BackColor       =   &H8000000C&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   525
      Index           =   0
      Left            =   0
      ScaleHeight     =   35
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   925
      TabIndex        =   11
      TabStop         =   0   'False
      Top             =   300
      Width           =   13875
      Begin VB.PictureBox TogglePanel 
         BackColor       =   &H000080FF&
         BorderStyle     =   0  'None
         Height          =   495
         Index           =   0
         Left            =   2100
         ScaleHeight     =   33
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   61
         TabIndex        =   168
         Top             =   0
         Visible         =   0   'False
         Width           =   915
         Begin VB.CheckBox chkToggle 
            Caption         =   " "
            Height          =   420
            Index           =   0
            Left            =   0
            Style           =   1  'Graphical
            TabIndex        =   170
            TabStop         =   0   'False
            Top             =   0
            Width           =   420
         End
         Begin VB.CheckBox chkToggle 
            Caption         =   " "
            Height          =   420
            Index           =   1
            Left            =   420
            Style           =   1  'Graphical
            TabIndex        =   169
            TabStop         =   0   'False
            Top             =   0
            Width           =   420
         End
      End
      Begin VB.Frame LedFrame 
         BackColor       =   &H000080FF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         Left            =   10260
         TabIndex        =   43
         Top             =   120
         Width           =   1170
         Begin VB.Shape BcRcvInd 
            FillColor       =   &H00C0C0C0&
            FillStyle       =   0  'Solid
            Height          =   105
            Index           =   1
            Left            =   990
            Shape           =   3  'Circle
            Top             =   270
            Width           =   165
         End
         Begin VB.Shape BcUpdInd 
            FillColor       =   &H00C0C0C0&
            FillStyle       =   0  'Solid
            Height          =   105
            Index           =   1
            Left            =   990
            Shape           =   3  'Circle
            Top             =   30
            Width           =   165
         End
         Begin VB.Shape BcRcvInd 
            FillColor       =   &H00C0C0C0&
            FillStyle       =   0  'Solid
            Height          =   105
            Index           =   0
            Left            =   870
            Shape           =   3  'Circle
            Top             =   270
            Width           =   165
         End
         Begin VB.Shape BcUpdInd 
            FillColor       =   &H00C0C0C0&
            FillStyle       =   0  'Solid
            Height          =   105
            Index           =   0
            Left            =   870
            Shape           =   3  'Circle
            Top             =   30
            Width           =   165
         End
         Begin VB.Label lblRefresh 
            Alignment       =   2  'Center
            BackColor       =   &H00C0C0C0&
            BackStyle       =   0  'Transparent
            Caption         =   "00:00:00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   165
            Index           =   1
            Left            =   75
            TabIndex        =   45
            Top             =   210
            Width           =   735
         End
         Begin VB.Label lblRefresh 
            Alignment       =   2  'Center
            BackColor       =   &H00C0C0C0&
            BackStyle       =   0  'Transparent
            Caption         =   "00:00:00"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00000000&
            Height          =   165
            Index           =   0
            Left            =   75
            TabIndex        =   44
            Top             =   0
            Width           =   735
         End
         Begin VB.Shape RefreshInd 
            BorderColor     =   &H00000000&
            FillColor       =   &H00C0C0C0&
            FillStyle       =   0  'Solid
            Height          =   105
            Index           =   0
            Left            =   870
            Shape           =   3  'Circle
            Top             =   150
            Width           =   165
         End
         Begin VB.Shape RefreshInd 
            FillColor       =   &H00C0C0C0&
            FillStyle       =   0  'Solid
            Height          =   105
            Index           =   1
            Left            =   990
            Shape           =   3  'Circle
            Top             =   150
            Width           =   165
         End
         Begin VB.Shape CedaCon 
            FillColor       =   &H0000FF00&
            FillStyle       =   0  'Solid
            Height          =   405
            Index           =   0
            Left            =   870
            Top             =   0
            Visible         =   0   'False
            Width           =   285
         End
         Begin VB.Shape RemoteCon 
            FillColor       =   &H00FFFFFF&
            FillStyle       =   0  'Solid
            Height          =   105
            Index           =   0
            Left            =   870
            Shape           =   3  'Circle
            Top             =   150
            Width           =   165
         End
         Begin VB.Shape RemoteCon 
            FillColor       =   &H00FFFFFF&
            FillStyle       =   0  'Solid
            Height          =   105
            Index           =   1
            Left            =   990
            Shape           =   3  'Circle
            Top             =   150
            Width           =   165
         End
      End
      Begin VB.CheckBox PageTitle 
         Caption         =   "Page Title"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   525
         Left            =   495
         Style           =   1  'Graphical
         TabIndex        =   42
         TabStop         =   0   'False
         Top             =   0
         Width           =   1995
      End
      Begin VB.CheckBox MainTitle 
         Caption         =   "CONNECTING"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   525
         Left            =   2535
         Style           =   1  'Graphical
         TabIndex        =   41
         TabStop         =   0   'False
         Top             =   0
         Width           =   2700
      End
      Begin VB.CheckBox chkIcon 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   540
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   40
         TabStop         =   0   'False
         Top             =   0
         Width           =   465
      End
      Begin VB.CheckBox chkFunc 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   540
         Left            =   11715
         Picture         =   "MainForm.frx":030A
         Style           =   1  'Graphical
         TabIndex        =   39
         TabStop         =   0   'False
         Top             =   0
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.CheckBox chkSpace 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   525
         Left            =   12585
         Style           =   1  'Graphical
         TabIndex        =   38
         TabStop         =   0   'False
         Top             =   0
         Width           =   570
      End
      Begin VB.CheckBox lblNow 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   525
         Left            =   5280
         Style           =   1  'Graphical
         TabIndex        =   37
         TabStop         =   0   'False
         Top             =   0
         Width           =   3270
      End
      Begin VB.CheckBox lblUTC 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   525
         Left            =   8595
         Style           =   1  'Graphical
         TabIndex        =   36
         TabStop         =   0   'False
         Top             =   0
         Width           =   1605
      End
      Begin VB.CheckBox chkAlive 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   18
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   525
         Left            =   10275
         Style           =   1  'Graphical
         TabIndex        =   35
         TabStop         =   0   'False
         Top             =   0
         Width           =   1245
      End
   End
   Begin VB.PictureBox TopInfoPanel 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   11
      Left            =   5070
      ScaleHeight     =   300
      ScaleWidth      =   1065
      TabIndex        =   137
      TabStop         =   0   'False
      Top             =   900
      Width           =   1125
   End
   Begin VB.PictureBox TopInfoPanel 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   10
      Left            =   0
      ScaleHeight     =   300
      ScaleWidth      =   4890
      TabIndex        =   133
      TabStop         =   0   'False
      Top             =   900
      Width           =   4950
      Begin VB.CheckBox chkTool 
         Height          =   300
         Index           =   27
         Left            =   3030
         Picture         =   "MainForm.frx":074C
         Style           =   1  'Graphical
         TabIndex        =   157
         TabStop         =   0   'False
         Top             =   0
         Width           =   330
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Filter Tools"
         Height          =   300
         Index           =   19
         Left            =   315
         Style           =   1  'Graphical
         TabIndex        =   138
         TabStop         =   0   'False
         Top             =   0
         Width           =   1200
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Messages"
         Height          =   300
         Index           =   23
         Left            =   1845
         Style           =   1  'Graphical
         TabIndex        =   140
         TabStop         =   0   'False
         Top             =   0
         Width           =   1200
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Favorites"
         Height          =   300
         Index           =   16
         Left            =   3690
         Style           =   1  'Graphical
         TabIndex        =   134
         TabStop         =   0   'False
         Top             =   0
         Width           =   1200
      End
      Begin VB.CheckBox chkTool 
         Height          =   300
         Index           =   22
         Left            =   1530
         Picture         =   "MainForm.frx":13E1
         Style           =   1  'Graphical
         TabIndex        =   139
         TabStop         =   0   'False
         Top             =   0
         Width           =   330
      End
      Begin VB.CheckBox chkTool 
         Height          =   300
         Index           =   18
         Left            =   3375
         Picture         =   "MainForm.frx":196B
         Style           =   1  'Graphical
         TabIndex        =   136
         TabStop         =   0   'False
         Top             =   0
         Width           =   330
      End
      Begin VB.CheckBox chkTool 
         Height          =   300
         Index           =   17
         Left            =   0
         Picture         =   "MainForm.frx":1CF5
         Style           =   1  'Graphical
         TabIndex        =   135
         TabStop         =   0   'False
         Top             =   0
         Width           =   330
      End
   End
   Begin VB.PictureBox TopInfoPanel 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   9
      Left            =   6330
      ScaleHeight     =   300
      ScaleWidth      =   2055
      TabIndex        =   125
      TabStop         =   0   'False
      Top             =   900
      Visible         =   0   'False
      Width           =   2115
      Begin VB.CheckBox chkMind 
         Caption         =   " "
         Height          =   300
         Index           =   0
         Left            =   1140
         Style           =   1  'Graphical
         TabIndex        =   132
         TabStop         =   0   'False
         Top             =   0
         Visible         =   0   'False
         Width           =   870
      End
      Begin VB.CheckBox chkTool 
         Height          =   300
         Index           =   15
         Left            =   360
         Picture         =   "MainForm.frx":227F
         Style           =   1  'Graphical
         TabIndex        =   128
         TabStop         =   0   'False
         Top             =   0
         Width           =   330
      End
      Begin VB.CheckBox chkTool 
         Enabled         =   0   'False
         Height          =   300
         Index           =   14
         Left            =   720
         Picture         =   "MainForm.frx":2609
         Style           =   1  'Graphical
         TabIndex        =   127
         TabStop         =   0   'False
         Top             =   0
         Width           =   330
      End
      Begin VB.CheckBox chkTool 
         Height          =   300
         Index           =   13
         Left            =   0
         Picture         =   "MainForm.frx":2993
         Style           =   1  'Graphical
         TabIndex        =   126
         TabStop         =   0   'False
         Top             =   0
         Width           =   330
      End
   End
   Begin VB.TextBox txtTabEditLookUp 
      Alignment       =   2  'Center
      Height          =   315
      Index           =   1
      Left            =   2010
      TabIndex        =   117
      Text            =   "TabEditLookUpField"
      Top             =   6750
      Visible         =   0   'False
      Width           =   1875
   End
   Begin VB.TextBox txtTabEditLookUp 
      Alignment       =   2  'Center
      Height          =   315
      Index           =   0
      Left            =   60
      TabIndex        =   116
      Text            =   "TabEditLookUpField"
      Top             =   6750
      Visible         =   0   'False
      Width           =   1875
   End
   Begin VB.PictureBox TopInfoPanel 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   720
      Index           =   6
      Left            =   0
      ScaleHeight     =   660
      ScaleWidth      =   13125
      TabIndex        =   105
      TabStop         =   0   'False
      Top             =   3480
      Visible         =   0   'False
      Width           =   13185
      Begin VB.CheckBox chkTool 
         Height          =   330
         Index           =   8
         Left            =   5280
         Picture         =   "MainForm.frx":2D1D
         Style           =   1  'Graphical
         TabIndex        =   123
         TabStop         =   0   'False
         Top             =   330
         Width           =   330
      End
      Begin VB.CheckBox chkTool 
         Height          =   330
         Index           =   7
         Left            =   5280
         Picture         =   "MainForm.frx":315F
         Style           =   1  'Graphical
         TabIndex        =   122
         TabStop         =   0   'False
         Top             =   0
         Width           =   330
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Reset"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   660
         Index           =   6
         Left            =   0
         Picture         =   "MainForm.frx":35A1
         Style           =   1  'Graphical
         TabIndex        =   121
         TabStop         =   0   'False
         Top             =   0
         Width           =   1305
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Edit"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   660
         Index           =   2
         Left            =   3960
         Picture         =   "MainForm.frx":392B
         Style           =   1  'Graphical
         TabIndex        =   106
         TabStop         =   0   'False
         Top             =   0
         Width           =   1305
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "View"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   660
         Index           =   5
         Left            =   2640
         Picture         =   "MainForm.frx":3D6D
         Style           =   1  'Graphical
         TabIndex        =   119
         TabStop         =   0   'False
         Top             =   0
         Width           =   1305
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Load"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   660
         Index           =   3
         Left            =   10020
         Picture         =   "MainForm.frx":3E67
         Style           =   1  'Graphical
         TabIndex        =   112
         TabStop         =   0   'False
         Top             =   0
         Width           =   1305
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Command2"
         Height          =   585
         Left            =   9870
         TabIndex        =   118
         Top             =   30
         Visible         =   0   'False
         Width           =   2895
      End
      Begin VB.CheckBox chkTool 
         Height          =   360
         Index           =   4
         Left            =   1680
         Picture         =   "MainForm.frx":3FB1
         Style           =   1  'Graphical
         TabIndex        =   115
         TabStop         =   0   'False
         Top             =   -30
         Visible         =   0   'False
         Width           =   645
      End
      Begin VB.PictureBox MyKeyPadPanel 
         BackColor       =   &H000080FF&
         Height          =   405
         Index           =   1
         Left            =   5685
         ScaleHeight     =   345
         ScaleWidth      =   3705
         TabIndex        =   114
         TabStop         =   0   'False
         Top             =   -30
         Width           =   3765
         Begin VB.TextBox FilterText 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   360
            Index           =   2
            Left            =   -30
            TabIndex        =   120
            TabStop         =   0   'False
            Text            =   "Text2"
            Top             =   -30
            Width           =   2745
         End
      End
      Begin VB.TextBox FilterText 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   1
         Left            =   7395
         TabIndex        =   111
         TabStop         =   0   'False
         Text            =   "text1"
         Top             =   330
         Width           =   1965
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Search"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   660
         Index           =   1
         Left            =   1320
         Picture         =   "MainForm.frx":453B
         Style           =   1  'Graphical
         TabIndex        =   110
         TabStop         =   0   'False
         Top             =   0
         Width           =   1305
      End
      Begin VB.TextBox FilterText 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Index           =   0
         Left            =   5700
         TabIndex        =   113
         TabStop         =   0   'False
         Text            =   "text0"
         Top             =   330
         Visible         =   0   'False
         Width           =   1545
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Enable"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   660
         Index           =   0
         Left            =   1320
         Picture         =   "MainForm.frx":4AC5
         Style           =   1  'Graphical
         TabIndex        =   109
         TabStop         =   0   'False
         Top             =   0
         Width           =   1305
      End
   End
   Begin VB.PictureBox AdvPanel 
      Height          =   525
      Index           =   1
      Left            =   5610
      ScaleHeight     =   465
      ScaleWidth      =   555
      TabIndex        =   93
      TabStop         =   0   'False
      Top             =   7170
      Visible         =   0   'False
      Width           =   615
      Begin VB.TextBox AdvText 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   1
         Left            =   0
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   96
         TabStop         =   0   'False
         Top             =   0
         Width           =   555
      End
   End
   Begin VB.Timer tmrTest 
      Enabled         =   0   'False
      Interval        =   500
      Left            =   12270
      Top             =   2370
   End
   Begin VB.PictureBox LightSpacer 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   30
      Index           =   0
      Left            =   10290
      ScaleHeight     =   30
      ScaleWidth      =   1695
      TabIndex        =   51
      TabStop         =   0   'False
      Top             =   1620
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.PictureBox DarkSpacer 
      BackColor       =   &H00000000&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   30
      Index           =   0
      Left            =   8460
      ScaleHeight     =   30
      ScaleWidth      =   1695
      TabIndex        =   29
      TabStop         =   0   'False
      Top             =   1620
      Visible         =   0   'False
      Width           =   1695
   End
   Begin VB.PictureBox AdvPanel 
      Height          =   525
      Index           =   0
      Left            =   4920
      ScaleHeight     =   465
      ScaleWidth      =   555
      TabIndex        =   49
      TabStop         =   0   'False
      Top             =   7170
      Visible         =   0   'False
      Width           =   615
      Begin VB.TextBox AdvText 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Index           =   0
         Left            =   0
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   94
         TabStop         =   0   'False
         Top             =   0
         Width           =   555
      End
   End
   Begin VB.PictureBox TopInfoPanel 
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   540
      Index           =   7
      Left            =   60
      ScaleHeight     =   480
      ScaleWidth      =   6495
      TabIndex        =   48
      TabStop         =   0   'False
      Top             =   2370
      Visible         =   0   'False
      Width           =   6555
      Begin VB.Label AdvMsgText 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         BackStyle       =   0  'Transparent
         Caption         =   "Unit Disabled by Security Module"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   0
         Left            =   3690
         TabIndex        =   104
         Top             =   0
         Visible         =   0   'False
         Width           =   3150
      End
      Begin VB.Label AdvMsgText 
         AutoSize        =   -1  'True
         BackColor       =   &H00FFFFFF&
         BackStyle       =   0  'Transparent
         Caption         =   "Access Denied."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   240
         Index           =   1
         Left            =   3690
         TabIndex        =   103
         Top             =   240
         Visible         =   0   'False
         Width           =   1440
      End
      Begin VB.Image AdvIcon 
         Height          =   480
         Index           =   1
         Left            =   3120
         Picture         =   "MainForm.frx":504F
         Top             =   0
         Visible         =   0   'False
         Width           =   480
      End
      Begin VB.Image AdvIcon 
         Height          =   480
         Index           =   0
         Left            =   2460
         Picture         =   "MainForm.frx":5491
         Top             =   0
         Visible         =   0   'False
         Width           =   480
      End
   End
   Begin VB.PictureBox TopInfoPanel 
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   360
      Index           =   5
      Left            =   60
      ScaleHeight     =   300
      ScaleWidth      =   6495
      TabIndex        =   28
      TabStop         =   0   'False
      Top             =   4560
      Visible         =   0   'False
      Width           =   6555
      Begin VB.CheckBox chkTool 
         Height          =   300
         Index           =   12
         Left            =   360
         Picture         =   "MainForm.frx":5D5B
         Style           =   1  'Graphical
         TabIndex        =   131
         TabStop         =   0   'False
         Top             =   0
         Width           =   330
      End
      Begin VB.CheckBox chkTool 
         Height          =   300
         Index           =   10
         Left            =   0
         Picture         =   "MainForm.frx":60E5
         Style           =   1  'Graphical
         TabIndex        =   130
         TabStop         =   0   'False
         Top             =   0
         Width           =   330
      End
      Begin VB.CheckBox chkTool 
         Height          =   300
         Index           =   11
         Left            =   720
         Picture         =   "MainForm.frx":646F
         Style           =   1  'Graphical
         TabIndex        =   129
         TabStop         =   0   'False
         Top             =   0
         Width           =   330
      End
   End
   Begin VB.Timer tmrSize 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   12720
      Top             =   2370
   End
   Begin VB.Timer tmrBlink 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   11670
      Tag             =   "30"
      Top             =   2340
   End
   Begin VB.PictureBox TopInfoPanel 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1185
      Index           =   4
      Left            =   6180
      ScaleHeight     =   1125
      ScaleWidth      =   5565
      TabIndex        =   20
      TabStop         =   0   'False
      Top             =   6030
      Visible         =   0   'False
      Width           =   5625
      Begin TABLib.TAB OcmTab 
         Height          =   885
         Index           =   0
         Left            =   150
         TabIndex        =   22
         TabStop         =   0   'False
         Top             =   150
         Width           =   2505
         _Version        =   65536
         _ExtentX        =   4419
         _ExtentY        =   1561
         _StockProps     =   64
      End
   End
   Begin VB.Timer tmrExit 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   11190
      Tag             =   "30"
      Top             =   2340
   End
   Begin VB.PictureBox ButtonPanel 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Index           =   1
      Left            =   0
      ScaleHeight     =   27
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   585
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   8220
      Visible         =   0   'False
      Width           =   8775
      Begin VB.PictureBox ButtonBack 
         BackColor       =   &H0000C0C0&
         BorderStyle     =   0  'None
         Height          =   405
         Index           =   1
         Left            =   2520
         ScaleHeight     =   27
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   285
         TabIndex        =   163
         Top             =   0
         Width           =   4275
         Begin VB.CheckBox btnCdiServer 
            Caption         =   "LISTENING"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   13.5
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   390
            Left            =   2070
            Style           =   1  'Graphical
            TabIndex        =   166
            TabStop         =   0   'False
            Top             =   0
            Visible         =   0   'False
            Width           =   2025
         End
         Begin VB.CheckBox chkHide 
            Caption         =   "HIDE"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   13.5
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   390
            Left            =   1170
            Style           =   1  'Graphical
            TabIndex        =   165
            TabStop         =   0   'False
            Top             =   0
            Visible         =   0   'False
            Width           =   885
         End
         Begin VB.CommandButton btnExit 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   13.5
               Charset         =   161
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   390
            Left            =   0
            TabIndex        =   164
            TabStop         =   0   'False
            Top             =   0
            Width           =   1155
         End
      End
      Begin VB.CommandButton btnNext 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   13.5
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   7980
         TabIndex        =   15
         TabStop         =   0   'False
         Top             =   0
         Width           =   735
      End
      Begin VB.CommandButton btnPrev 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   13.5
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   7080
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   0
         Width           =   795
      End
      Begin VB.CheckBox chkLeftAction 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   13.5
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   0
         Width           =   1155
      End
      Begin VB.CheckBox chkRightAction 
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   13.5
            Charset         =   161
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   1200
         Style           =   1  'Graphical
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   0
         Width           =   1185
      End
   End
   Begin VB.PictureBox TopInfoPanel 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   270
      Index           =   2
      Left            =   0
      ScaleHeight     =   270
      ScaleWidth      =   13095
      TabIndex        =   8
      TabStop         =   0   'False
      Top             =   1320
      Width           =   13095
      Begin VB.Label KeyInputLabel 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "INPUT"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Index           =   1
         Left            =   11430
         TabIndex        =   53
         Top             =   30
         Visible         =   0   'False
         Width           =   690
      End
      Begin VB.Label KeyInputLabel 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "INPUT"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Index           =   0
         Left            =   12285
         TabIndex        =   52
         Top             =   30
         Width           =   690
      End
      Begin VB.Label OcmLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "ONLINE UPDATE MONITOR"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Index           =   1
         Left            =   7500
         TabIndex        =   21
         Top             =   30
         Visible         =   0   'False
         Width           =   2985
      End
      Begin VB.Label OcmLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "ONLINE MESSAGE MONITOR"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   270
         Index           =   0
         Left            =   45
         TabIndex        =   9
         Top             =   30
         Width           =   3180
      End
   End
   Begin VB.PictureBox TopInfoPanel 
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   450
      Index           =   1
      Left            =   30
      ScaleHeight     =   450
      ScaleWidth      =   13215
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   2970
      Width           =   13215
      Begin VB.PictureBox PagePanel 
         BorderStyle     =   0  'None
         Height          =   450
         Index           =   0
         Left            =   0
         ScaleHeight     =   450
         ScaleWidth      =   4575
         TabIndex        =   46
         TabStop         =   0   'False
         Top             =   0
         Visible         =   0   'False
         Width           =   4575
         Begin VB.CheckBox chkTool 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   14.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   390
            Index           =   21
            Left            =   15
            Picture         =   "MainForm.frx":67F9
            Style           =   1  'Graphical
            TabIndex        =   167
            TabStop         =   0   'False
            Top             =   45
            Width           =   765
         End
         Begin VB.PictureBox PageNoPanel 
            Height          =   420
            Index           =   0
            Left            =   2610
            ScaleHeight     =   360
            ScaleWidth      =   1740
            TabIndex        =   74
            Top             =   30
            Width           =   1800
            Begin VB.Label PageCode 
               Alignment       =   2  'Center
               BackColor       =   &H0000C000&
               BackStyle       =   0  'Transparent
               Caption         =   "1234"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   14.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   0
               Left            =   495
               TabIndex        =   77
               Top             =   15
               Width           =   750
            End
            Begin VB.Label PageGrpTo 
               Alignment       =   2  'Center
               BackColor       =   &H000000FF&
               BackStyle       =   0  'Transparent
               Caption         =   "1234"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Index           =   0
               Left            =   1305
               TabIndex        =   76
               Top             =   90
               Width           =   450
            End
            Begin VB.Label PageGrpFr 
               Alignment       =   2  'Center
               BackColor       =   &H0000FFFF&
               BackStyle       =   0  'Transparent
               Caption         =   "1234"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Index           =   0
               Left            =   15
               TabIndex        =   75
               Top             =   90
               Width           =   450
            End
            Begin VB.Label AnyLabel 
               Alignment       =   1  'Right Justify
               BackStyle       =   0  'Transparent
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   420
               Index           =   9
               Left            =   -30
               TabIndex        =   78
               Top             =   -30
               Width           =   510
            End
            Begin VB.Label AnyLabel 
               Alignment       =   1  'Right Justify
               BackStyle       =   0  'Transparent
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   420
               Index           =   10
               Left            =   480
               TabIndex        =   79
               Top             =   -30
               Width           =   780
            End
            Begin VB.Label AnyLabel 
               Alignment       =   1  'Right Justify
               BackStyle       =   0  'Transparent
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   420
               Index           =   11
               Left            =   1260
               TabIndex        =   80
               Top             =   -30
               Width           =   510
            End
         End
         Begin VB.PictureBox TabCntPanel 
            Height          =   420
            Index           =   0
            Left            =   0
            ScaleHeight     =   360
            ScaleWidth      =   1740
            TabIndex        =   68
            Top             =   30
            Width           =   1800
            Begin VB.Image MainTool 
               Height          =   360
               Index           =   0
               Left            =   0
               Picture         =   "MainForm.frx":75BB
               Top             =   0
               Width           =   720
            End
            Begin VB.Label AnyLabel 
               Alignment       =   1  'Right Justify
               BackStyle       =   0  'Transparent
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   420
               Index           =   8
               Left            =   1260
               TabIndex        =   73
               Top             =   -30
               Width           =   510
            End
            Begin VB.Label AnyLabel 
               Alignment       =   1  'Right Justify
               BackStyle       =   0  'Transparent
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   420
               Index           =   7
               Left            =   750
               TabIndex        =   72
               Top             =   -30
               Width           =   510
            End
            Begin VB.Label PageCnt 
               Alignment       =   1  'Right Justify
               AutoSize        =   -1  'True
               BackColor       =   &H0000C000&
               BackStyle       =   0  'Transparent
               Caption         =   "1234"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Index           =   0
               Left            =   1305
               TabIndex        =   70
               Top             =   120
               Width           =   420
            End
            Begin VB.Label PagePos 
               Alignment       =   1  'Right Justify
               AutoSize        =   -1  'True
               BackColor       =   &H000000FF&
               BackStyle       =   0  'Transparent
               Caption         =   "1234"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Index           =   0
               Left            =   795
               TabIndex        =   69
               Top             =   120
               Width           =   420
            End
            Begin VB.Image LeftTool 
               Height          =   360
               Index           =   0
               Left            =   0
               Picture         =   "MainForm.frx":837D
               Stretch         =   -1  'True
               Top             =   0
               Width           =   360
            End
            Begin VB.Image RightTool 
               Height          =   360
               Index           =   0
               Left            =   360
               Picture         =   "MainForm.frx":8A7F
               Stretch         =   -1  'True
               Top             =   0
               Width           =   360
            End
            Begin VB.Label AnyLabel 
               Alignment       =   1  'Right Justify
               BackStyle       =   0  'Transparent
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   420
               Index           =   6
               Left            =   -30
               TabIndex        =   71
               Top             =   -30
               Width           =   780
            End
         End
         Begin VB.Label InfoTitle 
            Alignment       =   2  'Center
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFF80&
            BackStyle       =   0  'Transparent
            Caption         =   "PAGE 1"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   14.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   0
            Left            =   1785
            TabIndex        =   47
            Top             =   60
            Width           =   1050
         End
      End
      Begin VB.PictureBox PagePanel 
         BorderStyle     =   0  'None
         Height          =   450
         Index           =   1
         Left            =   5490
         ScaleHeight     =   450
         ScaleWidth      =   7125
         TabIndex        =   25
         TabStop         =   0   'False
         Top             =   0
         Visible         =   0   'False
         Width           =   7125
         Begin VB.CheckBox chkTool 
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   14.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   390
            Index           =   9
            Left            =   15
            Picture         =   "MainForm.frx":9181
            Style           =   1  'Graphical
            TabIndex        =   124
            TabStop         =   0   'False
            Top             =   45
            Width           =   765
         End
         Begin VB.PictureBox TabCntPanel 
            Height          =   420
            Index           =   1
            Left            =   0
            ScaleHeight     =   360
            ScaleWidth      =   1740
            TabIndex        =   59
            Top             =   30
            Width           =   1800
            Begin VB.Image MainTool 
               Height          =   360
               Index           =   1
               Left            =   0
               Picture         =   "MainForm.frx":9F43
               Top             =   0
               Width           =   720
            End
            Begin VB.Image RightTool 
               Height          =   360
               Index           =   1
               Left            =   360
               Picture         =   "MainForm.frx":AD05
               Stretch         =   -1  'True
               Top             =   0
               Width           =   360
            End
            Begin VB.Image LeftTool 
               Height          =   360
               Index           =   1
               Left            =   0
               Picture         =   "MainForm.frx":B00F
               Stretch         =   -1  'True
               Top             =   0
               Width           =   360
            End
            Begin VB.Label PagePos 
               Alignment       =   1  'Right Justify
               AutoSize        =   -1  'True
               BackColor       =   &H000000FF&
               BackStyle       =   0  'Transparent
               Caption         =   "1234"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Index           =   1
               Left            =   795
               TabIndex        =   61
               Top             =   120
               Width           =   420
            End
            Begin VB.Label PageCnt 
               Alignment       =   1  'Right Justify
               AutoSize        =   -1  'True
               BackColor       =   &H0000C000&
               BackStyle       =   0  'Transparent
               Caption         =   "1234"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Index           =   1
               Left            =   1305
               TabIndex        =   60
               Top             =   120
               Width           =   420
            End
            Begin VB.Label AnyLabel 
               Alignment       =   1  'Right Justify
               BackStyle       =   0  'Transparent
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   420
               Index           =   0
               Left            =   -30
               TabIndex        =   62
               Top             =   -30
               Width           =   780
            End
            Begin VB.Label AnyLabel 
               Alignment       =   1  'Right Justify
               BackStyle       =   0  'Transparent
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   420
               Index           =   1
               Left            =   750
               TabIndex        =   63
               Top             =   -30
               Width           =   510
            End
            Begin VB.Label AnyLabel 
               Alignment       =   1  'Right Justify
               BackStyle       =   0  'Transparent
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   420
               Index           =   2
               Left            =   1260
               TabIndex        =   64
               Top             =   -30
               Width           =   510
            End
         End
         Begin VB.PictureBox PageNoPanel 
            Height          =   420
            Index           =   1
            Left            =   5100
            ScaleHeight     =   360
            ScaleWidth      =   1740
            TabIndex        =   55
            Top             =   30
            Width           =   1800
            Begin VB.Label PageGrpFr 
               Alignment       =   2  'Center
               BackColor       =   &H0000FFFF&
               BackStyle       =   0  'Transparent
               Caption         =   "1234"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Index           =   1
               Left            =   15
               TabIndex        =   58
               Top             =   90
               Width           =   450
            End
            Begin VB.Label PageGrpTo 
               Alignment       =   2  'Center
               BackColor       =   &H000000FF&
               BackStyle       =   0  'Transparent
               Caption         =   "1234"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Index           =   1
               Left            =   1305
               TabIndex        =   57
               Top             =   90
               Width           =   450
            End
            Begin VB.Label PageCode 
               Alignment       =   2  'Center
               BackColor       =   &H0000C000&
               BackStyle       =   0  'Transparent
               Caption         =   "1234"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   14.25
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   330
               Index           =   1
               Left            =   495
               TabIndex        =   56
               Top             =   15
               Width           =   750
            End
            Begin VB.Label AnyLabel 
               Alignment       =   1  'Right Justify
               BackStyle       =   0  'Transparent
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   420
               Index           =   3
               Left            =   -30
               TabIndex        =   65
               Top             =   -30
               Width           =   510
            End
            Begin VB.Label AnyLabel 
               Alignment       =   1  'Right Justify
               BackStyle       =   0  'Transparent
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   420
               Index           =   4
               Left            =   480
               TabIndex        =   66
               Top             =   -30
               Width           =   780
            End
            Begin VB.Label AnyLabel 
               Alignment       =   1  'Right Justify
               BackStyle       =   0  'Transparent
               BorderStyle     =   1  'Fixed Single
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   12
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   420
               Index           =   5
               Left            =   1260
               TabIndex        =   67
               Top             =   -30
               Width           =   510
            End
         End
         Begin VB.Label InfoTitle 
            Alignment       =   2  'Center
            AutoSize        =   -1  'True
            BackColor       =   &H00FFFF80&
            BackStyle       =   0  'Transparent
            Caption         =   "PAGE 2"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   14.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   1
            Left            =   2835
            TabIndex        =   26
            Top             =   60
            Width           =   1050
         End
      End
   End
   Begin VB.PictureBox BotInfoPanel 
      BackColor       =   &H00E0E0E0&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   540
      Index           =   0
      Left            =   30
      ScaleHeight     =   480
      ScaleWidth      =   4785
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   7170
      Visible         =   0   'False
      Width           =   4845
   End
   Begin VB.PictureBox TopInfoPanel 
      BackColor       =   &H8000000C&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   0
      Left            =   30
      ScaleHeight     =   255
      ScaleWidth      =   6525
      TabIndex        =   4
      TabStop         =   0   'False
      Top             =   1620
      Visible         =   0   'False
      Width           =   6585
      Begin VB.CheckBox chkTopLine 
         Caption         =   "TIME CHG"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   0
         Style           =   1  'Graphical
         TabIndex        =   19
         TabStop         =   0   'False
         Top             =   0
         Width           =   1005
      End
      Begin VB.PictureBox TopLine 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Index           =   0
         Left            =   1170
         ScaleHeight     =   240
         ScaleWidth      =   2985
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   0
         Width           =   3045
         Begin VB.PictureBox LinePanel 
            BackColor       =   &H00FF0000&
            BorderStyle     =   0  'None
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   270
            Index           =   0
            Left            =   150
            ScaleHeight     =   270
            ScaleWidth      =   2685
            TabIndex        =   17
            TabStop         =   0   'False
            Top             =   0
            Width           =   2685
            Begin VB.Image Image1 
               Height          =   240
               Left            =   0
               Picture         =   "MainForm.frx":B711
               Top             =   0
               Visible         =   0   'False
               Width           =   240
            End
            Begin VB.Label TopSep 
               BackColor       =   &H00808080&
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   240
               Index           =   0
               Left            =   1620
               TabIndex        =   27
               Top             =   0
               Width           =   60
            End
            Begin VB.Label TopLabel 
               AutoSize        =   -1  'True
               BackColor       =   &H00FFFFFF&
               BorderStyle     =   1  'Fixed Single
               Caption         =   "TEST 1"
               BeginProperty Font 
                  Name            =   "Arial"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   700
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               ForeColor       =   &H00000000&
               Height          =   300
               Index           =   0
               Left            =   630
               TabIndex        =   18
               Top             =   0
               Width           =   720
            End
         End
      End
   End
   Begin TABLib.TAB CdrHdl 
      Height          =   615
      Left            =   60
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   6000
      Visible         =   0   'False
      Width           =   2475
      _Version        =   65536
      _ExtentX        =   4366
      _ExtentY        =   1085
      _StockProps     =   64
   End
   Begin VB.Timer BcSpoolTimer 
      Interval        =   5
      Left            =   7230
      Top             =   2340
   End
   Begin MSWinsockLib.Winsock CdiPort 
      Index           =   0
      Left            =   6720
      Top             =   2340
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin MSWinsockLib.Winsock RbcBcPort 
      Index           =   0
      Left            =   7740
      Top             =   2340
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin UFISBROADCASTSLib.UfisBroadcasts RcvBc 
      Left            =   10320
      Top             =   2280
      _Version        =   65536
      _ExtentX        =   1402
      _ExtentY        =   1032
      _StockProps     =   0
   End
   Begin VB.Timer tmrMinutes 
      Left            =   9180
      Top             =   2340
   End
   Begin VB.Timer tmrSeconds 
      Left            =   8700
      Top             =   2340
   End
   Begin UFISCOMLib.UfisCom Ufis 
      Left            =   9660
      Top             =   2280
      _Version        =   65536
      _ExtentX        =   1085
      _ExtentY        =   1032
      _StockProps     =   0
   End
   Begin MSWinsockLib.Winsock RemotePort 
      Index           =   0
      Left            =   8220
      Top             =   2340
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin TABLib.TAB BcSpoolerTab 
      Height          =   615
      Left            =   2670
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   6000
      Visible         =   0   'False
      Width           =   1425
      _Version        =   65536
      _ExtentX        =   2514
      _ExtentY        =   1085
      _StockProps     =   64
   End
   Begin TABLib.TAB DataTab 
      Height          =   525
      Index           =   0
      Left            =   90
      TabIndex        =   0
      Top             =   5400
      Visible         =   0   'False
      Width           =   1605
      _Version        =   65536
      _ExtentX        =   2831
      _ExtentY        =   926
      _StockProps     =   64
   End
   Begin TABLib.TAB DataTab 
      Height          =   525
      Index           =   1
      Left            =   1770
      TabIndex        =   2
      Top             =   5400
      Visible         =   0   'False
      Width           =   1605
      _Version        =   65536
      _ExtentX        =   2831
      _ExtentY        =   926
      _StockProps     =   64
   End
   Begin VB.PictureBox TopInfoPanel 
      BackColor       =   &H00E0E0E0&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   8
      Left            =   -30
      ScaleHeight     =   240
      ScaleWidth      =   13395
      TabIndex        =   30
      TabStop         =   0   'False
      Top             =   0
      Width           =   13395
      Begin VB.PictureBox LedPanel 
         BackColor       =   &H000000FF&
         BorderStyle     =   0  'None
         Height          =   240
         Index           =   3
         Left            =   6870
         ScaleHeight     =   240
         ScaleWidth      =   600
         TabIndex        =   102
         TabStop         =   0   'False
         Top             =   0
         Width           =   600
         Begin VB.Image MyLed 
            Height          =   195
            Index           =   5
            Left            =   240
            Picture         =   "MainForm.frx":BA9B
            Top             =   0
            Width           =   195
         End
         Begin VB.Image MyLed 
            Height          =   195
            Index           =   4
            Left            =   30
            Picture         =   "MainForm.frx":BCE5
            Top             =   0
            Width           =   195
         End
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Error Handle"
         Height          =   255
         Left            =   5850
         TabIndex        =   101
         TabStop         =   0   'False
         Top             =   0
         Visible         =   0   'False
         Width           =   1305
      End
      Begin VB.TextBox Text3 
         Height          =   315
         Left            =   10350
         ScrollBars      =   2  'Vertical
         TabIndex        =   95
         TabStop         =   0   'False
         Top             =   0
         Visible         =   0   'False
         Width           =   2055
      End
      Begin VB.PictureBox LedPanel 
         BackColor       =   &H000000FF&
         BorderStyle     =   0  'None
         Height          =   240
         Index           =   2
         Left            =   12630
         ScaleHeight     =   240
         ScaleWidth      =   240
         TabIndex        =   92
         TabStop         =   0   'False
         Top             =   0
         Width           =   240
         Begin VB.Image TcpLed 
            Height          =   195
            Index           =   5
            Left            =   0
            Picture         =   "MainForm.frx":BF2F
            Top             =   0
            Width           =   195
         End
         Begin VB.Image TcpLed 
            Height          =   195
            Index           =   4
            Left            =   0
            Picture         =   "MainForm.frx":C179
            Top             =   0
            Width           =   195
         End
      End
      Begin MSComctlLib.Slider Slider1 
         Height          =   315
         Left            =   9390
         TabIndex        =   91
         TabStop         =   0   'False
         Top             =   -30
         Visible         =   0   'False
         Width           =   3135
         _ExtentX        =   5530
         _ExtentY        =   556
         _Version        =   393216
         Min             =   10
         Max             =   40
         SelStart        =   29
         Value           =   29
      End
      Begin VB.PictureBox LedPanel 
         BackColor       =   &H000000FF&
         BorderStyle     =   0  'None
         Height          =   240
         Index           =   1
         Left            =   60
         ScaleHeight     =   240
         ScaleWidth      =   240
         TabIndex        =   85
         TabStop         =   0   'False
         Top             =   0
         Width           =   240
         Begin VB.Image TcpLed 
            Height          =   195
            Index           =   3
            Left            =   0
            Picture         =   "MainForm.frx":C3C3
            Top             =   0
            Width           =   195
         End
         Begin VB.Image TcpLed 
            Height          =   195
            Index           =   2
            Left            =   0
            Picture         =   "MainForm.frx":C60D
            Top             =   0
            Width           =   195
         End
      End
      Begin VB.CommandButton Command3 
         Caption         =   "WriteToFile"
         Height          =   255
         Left            =   8070
         TabIndex        =   54
         TabStop         =   0   'False
         Top             =   0
         Visible         =   0   'False
         Width           =   1305
      End
      Begin VB.PictureBox LedPanel 
         BackColor       =   &H000000FF&
         BorderStyle     =   0  'None
         Height          =   240
         Index           =   0
         Left            =   12990
         ScaleHeight     =   240
         ScaleWidth      =   240
         TabIndex        =   50
         TabStop         =   0   'False
         Top             =   0
         Width           =   240
         Begin VB.Image TcpLed 
            Height          =   195
            Index           =   0
            Left            =   0
            Picture         =   "MainForm.frx":C857
            Top             =   0
            Width           =   195
         End
         Begin VB.Image TcpLed 
            Height          =   195
            Index           =   1
            Left            =   0
            Picture         =   "MainForm.frx":CAA1
            Top             =   0
            Width           =   195
         End
      End
      Begin VB.Label UnitLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "1234"
         Height          =   210
         Index           =   9
         Left            =   4890
         TabIndex        =   100
         Top             =   15
         Width           =   360
      End
      Begin VB.Label UnitLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "SID:"
         Height          =   210
         Index           =   8
         Left            =   4530
         TabIndex        =   99
         Top             =   15
         Width           =   300
      End
      Begin VB.Label UnitLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "1234"
         Height          =   210
         Index           =   7
         Left            =   4050
         TabIndex        =   98
         Top             =   15
         Width           =   360
      End
      Begin VB.Label UnitLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "PID:"
         Height          =   210
         Index           =   6
         Left            =   3660
         TabIndex        =   97
         Top             =   15
         Width           =   300
      End
      Begin VB.Label UnitStatus 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "S"
         Height          =   210
         Index           =   4
         Left            =   9480
         TabIndex        =   88
         Top             =   15
         Visible         =   0   'False
         Width           =   105
      End
      Begin VB.Label UnitLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "WKS:"
         Height          =   210
         Index           =   2
         Left            =   1350
         TabIndex        =   87
         Top             =   15
         Width           =   405
      End
      Begin VB.Label UnitLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "????"
         Height          =   210
         Index           =   3
         Left            =   1920
         TabIndex        =   86
         Top             =   15
         Width           =   420
      End
      Begin VB.Label UnitStatus 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "3"
         Height          =   210
         Index           =   3
         Left            =   11640
         TabIndex        =   84
         Top             =   15
         Visible         =   0   'False
         Width           =   90
      End
      Begin VB.Label UnitStatus 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "2"
         Height          =   210
         Index           =   2
         Left            =   11340
         TabIndex        =   83
         Top             =   15
         Visible         =   0   'False
         Width           =   90
      End
      Begin VB.Label UnitStatus 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "1"
         Height          =   210
         Index           =   1
         Left            =   11040
         TabIndex        =   82
         Top             =   15
         Visible         =   0   'False
         Width           =   90
      End
      Begin VB.Label UnitStatus 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         Height          =   210
         Index           =   0
         Left            =   10770
         TabIndex        =   81
         Top             =   15
         Visible         =   0   'False
         Width           =   90
      End
      Begin VB.Image Image8 
         Appearance      =   0  'Flat
         Height          =   240
         Left            =   7800
         Picture         =   "MainForm.frx":CCEB
         Top             =   0
         Visible         =   0   'False
         Width           =   240
      End
      Begin VB.Label UnitLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "????"
         Height          =   210
         Index           =   5
         Left            =   3030
         TabIndex        =   34
         Top             =   15
         Width           =   420
      End
      Begin VB.Label UnitLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "LOC:"
         Height          =   210
         Index           =   4
         Left            =   2520
         TabIndex        =   33
         Top             =   15
         Width           =   390
      End
      Begin VB.Label UnitLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "????"
         Height          =   210
         Index           =   1
         Left            =   750
         TabIndex        =   32
         Top             =   15
         Width           =   420
      End
      Begin VB.Label UnitLabel 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "UNIT:"
         Height          =   210
         Index           =   0
         Left            =   300
         TabIndex        =   31
         Top             =   15
         Width           =   405
      End
   End
   Begin VB.PictureBox InfoTabPanel 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3675
      Index           =   0
      Left            =   7140
      ScaleHeight     =   3615
      ScaleWidth      =   5565
      TabIndex        =   23
      TabStop         =   0   'False
      Top             =   3960
      Visible         =   0   'False
      Width           =   5625
      Begin TABLib.TAB InfoTab 
         Height          =   1065
         Index           =   0
         Left            =   0
         TabIndex        =   24
         TabStop         =   0   'False
         Top             =   0
         Width           =   4905
         _Version        =   65536
         _ExtentX        =   8652
         _ExtentY        =   1879
         _StockProps     =   64
      End
   End
   Begin VB.PictureBox TopInfoPanel 
      BackColor       =   &H00808080&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Index           =   3
      Left            =   90
      ScaleHeight     =   13
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   375
      TabIndex        =   16
      TabStop         =   0   'False
      Top             =   7830
      Visible         =   0   'False
      Width           =   5625
   End
   Begin TABLib.TAB DataTab 
      Height          =   525
      Index           =   2
      Left            =   3450
      TabIndex        =   89
      TabStop         =   0   'False
      Top             =   5400
      Visible         =   0   'False
      Width           =   1605
      _Version        =   65536
      _ExtentX        =   2831
      _ExtentY        =   926
      _StockProps     =   64
   End
   Begin TABLib.TAB DataTab 
      Height          =   525
      Index           =   3
      Left            =   5130
      TabIndex        =   90
      TabStop         =   0   'False
      Top             =   5400
      Visible         =   0   'False
      Width           =   1605
      _Version        =   65536
      _ExtentX        =   2831
      _ExtentY        =   926
      _StockProps     =   64
   End
   Begin TABLib.TAB TabLookUp 
      Height          =   465
      Index           =   0
      Left            =   90
      TabIndex        =   107
      Top             =   4890
      Visible         =   0   'False
      Width           =   1605
      _Version        =   65536
      _ExtentX        =   2831
      _ExtentY        =   820
      _StockProps     =   64
      Columns         =   10
      FontName        =   "Arial"
   End
   Begin TABLib.TAB TabLookUp 
      Height          =   465
      Index           =   1
      Left            =   1770
      TabIndex        =   108
      Top             =   4890
      Visible         =   0   'False
      Width           =   1605
      _Version        =   65536
      _ExtentX        =   2831
      _ExtentY        =   820
      _StockProps     =   64
      Columns         =   10
      FontName        =   "Arial"
   End
   Begin VB.Image MyIcons 
      Height          =   360
      Index           =   8
      Left            =   9660
      Picture         =   "MainForm.frx":D075
      Top             =   1770
      Visible         =   0   'False
      Width           =   720
   End
   Begin VB.Image MyIcons 
      Height          =   360
      Index           =   7
      Left            =   8880
      Picture         =   "MainForm.frx":DE37
      Top             =   1770
      Visible         =   0   'False
      Width           =   720
   End
   Begin VB.Image MyIcons 
      Height          =   240
      Index           =   6
      Left            =   8550
      Picture         =   "MainForm.frx":EBF9
      Top             =   1770
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image MyIcons 
      Height          =   240
      Index           =   5
      Left            =   8220
      Picture         =   "MainForm.frx":ED43
      Top             =   1770
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image MyLed 
      Height          =   195
      Index           =   6
      Left            =   11910
      Picture         =   "MainForm.frx":EE8D
      Top             =   2130
      Visible         =   0   'False
      Width           =   195
   End
   Begin VB.Image MyIcons 
      Height          =   240
      Index           =   4
      Left            =   7830
      Picture         =   "MainForm.frx":F0D7
      Top             =   1770
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image MyIcons 
      Height          =   240
      Index           =   3
      Left            =   7560
      Picture         =   "MainForm.frx":F221
      Top             =   1770
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image MyIcons 
      Height          =   240
      Index           =   2
      Left            =   7290
      Picture         =   "MainForm.frx":F36B
      Top             =   1770
      Visible         =   0   'False
      Width           =   240
   End
   Begin VB.Image MyLed 
      Height          =   195
      Index           =   3
      Left            =   12930
      Picture         =   "MainForm.frx":F4B5
      Top             =   2130
      Visible         =   0   'False
      Width           =   195
   End
   Begin VB.Image MyLed 
      Height          =   195
      Index           =   2
      Left            =   12690
      Picture         =   "MainForm.frx":F6FF
      Top             =   2130
      Visible         =   0   'False
      Width           =   195
   End
   Begin VB.Image MyLed 
      Height          =   195
      Index           =   1
      Left            =   12450
      Picture         =   "MainForm.frx":F949
      Top             =   2130
      Visible         =   0   'False
      Width           =   195
   End
   Begin VB.Image MyLed 
      Height          =   195
      Index           =   0
      Left            =   12210
      Picture         =   "MainForm.frx":FB93
      Top             =   2130
      Visible         =   0   'False
      Width           =   195
   End
   Begin VB.Image MyIcons 
      Height          =   480
      Index           =   0
      Left            =   12240
      Picture         =   "MainForm.frx":FDDD
      Top             =   1590
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image MyIcons 
      Height          =   480
      Index           =   1
      Left            =   12840
      Picture         =   "MainForm.frx":1021F
      Top             =   1590
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Menu RCPopup 
      Caption         =   "Popup"
      Visible         =   0   'False
      Begin VB.Menu msg1 
         Caption         =   "Info"
      End
      Begin VB.Menu Rest1 
         Caption         =   "Show Me"
      End
      Begin VB.Menu Exit1 
         Caption         =   "Exit"
      End
   End
End
Attribute VB_Name = "MainForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim ButtonPushedInternal As Boolean
Dim PageJustLoaded As Boolean
Dim StartDataView As String

Dim PendingOpenFavorite As Integer
Dim TabEditLookUpIndex As Integer
Dim TabEditColNo As Long
Dim TabEditLineNo As Long
Dim TabEditLookUpType As String
Dim TabEditLookUpTabObj As TABLib.TAB
Dim TabEditIsUpperCase As Boolean
Dim CurrentTabFont As String

Dim TabEditLookUpValueCol As Long
Dim TabEditLookUpFixed As Integer

Dim TabLookIdx(2) As Integer
Dim TabLookCol(2) As Long

Dim PageChanged As Boolean
Dim CountDownAnimation As Boolean
Dim NoGroupTitle(2) As Boolean
Dim NoPageTitle(2) As Boolean
Dim NoTableName(2) As Boolean
Dim NoGridHeader(2) As Boolean

Dim MyApplThreadID As Long
Dim MyApplProcessID As Long
Dim MyCtrlSessionID As Long
Dim MyCtrlProcessID As Long
Dim MyCtrlThreadID As Long
Dim MyCtrlHWnd As Long
Dim CedaWasConnected As Boolean
Dim InftabWhere As String
Dim TimerCaption As String
Dim PageFunction(2) As String
Dim PageCursor(2) As String
Dim AskUser As Boolean
Dim MyLoginStatus As Boolean

Public MyActivePageNumber As String
Public MyLastPageNumber As String

Dim MyApplMode As String
Dim MyDefReloadMin As String
Dim MyDefReloadMax As String
Dim MyDefKeepUpdate As String
Dim MyDefLineHeight(2) As Integer
Dim MyDefFontSize(2) As Integer
Dim MyDefLineColors(2) As String
Dim MyDefTextColor(2) As String

Dim MyTabLineColors(2) As String
Dim MyTabTextColor(2) As String

Dim MyLineTextColor(2) As Long
Dim MyAutoSizeCols(2) As Boolean
Dim MyLogoPath As String

Public WorkOffLine As Boolean
Public RemoteControl As Boolean
Public RemoteIsOpen As Boolean
Public RemoteIsBusy As Boolean
Public RemoteIdx As Integer
Public CdiIdx As Integer
Public CdiServer As Boolean
Public CdiPacketComplete As Boolean
Public CdiPortsChanged As Boolean
Public CdiSendOnConnex As Boolean
Public CdiActIdx As Integer
Private CdiMsgRest(1000) As String
Private CdiRecSep As String

Public BcPortConnected As Boolean

Public omDisplayMode As String
Dim RemoteColor(2) As Long
Dim GotUtcDiff As Boolean
Dim GotLtDiff As Boolean
Dim GotNowDiff As Boolean
Dim UTC_Diff As Long
Dim LT_Diff As Long


Dim PageSectionFound(2) As String
Dim SupportCCaTab(2) As Boolean

Dim IniFileFound As String
Dim MainSectionFound As String
Dim RefreshMode As Boolean
Dim UpdateOnReload As Boolean
Dim UpdateCount As Long

Dim RefreshCyclus As Integer
Dim RefreshCountDown(2) As Boolean
Dim CountDownTime As Integer
Dim MinCountDown(2) As Integer
Dim SecCountDown(2) As Integer
Dim currScrollPos(2) As Integer

Dim TabIdx As Integer

Dim ApplWatched As String
Dim MyTcpIpAdr As String
Dim HexTcpAdr As String

Dim PageIsValid(2) As Boolean
Dim MyDataTable(2) As String
Dim MainCaption(2) As String
Dim PageNumber(2) As String
Dim PageCaption(2) As String
Dim InfoCaption(2) As String
Dim UseBcNums(2) As String
Dim LastBcNum(2) As Integer
Dim LostBroadCast(2) As Boolean
Dim ReloadData(2) As Boolean
Dim MyPageFields(2) As String
Dim MyPageFieldCount(2) As Long
Dim MyFieldLen As String
Dim MyCedaFields(2) As String
Dim MyTimeFields(2) As String
Dim MyDateFields(2) As String
Dim MyHourFields(2) As String
Dim MyTimeCols(2) As String
Dim MyCedaTitle(2) As String
Dim MyPageGrpKey(2) As String
Dim MyPageSqlKey(2) As String
Dim MyDefTrigger01(2) As String
Dim MyDefTrigger02(2) As String
Dim MyDefTrigger03(2) As String
Dim MyTabFontName(2) As String
Dim CdiPageFields(2) As String
Dim CdiKeyFields(2) As String
Dim CdiTriggers(2) As String
Dim CdiFieldLength(2) As String
Dim ToggleButtons As String
Dim MyLoadButton As String
Dim CurLoadButton As String
Dim KeepUpdate As Integer
Dim EmptyLine(2) As String
Dim ListLines(2) As Integer
Dim MyUrnoIdx(2) As Integer
Dim MyFlnoIdx(2) As Integer
Dim MyCkifIdx(2) As Integer
Dim MyCkitIdx(2) As Integer
Dim MyStodIdx(2) As Integer
Dim DefSaveTimer As Integer
Dim ActSaveTimer As Integer
Dim SavePath As String
Dim SendUpdates As Boolean
Dim AutoSize As Boolean
Dim InfoPanelVisible As Boolean
Dim OcmPanelVisible As Boolean
Dim ExitChkVisible As Boolean
Dim ActNumInput As String
Dim LastKeyFunc As String
Dim ShowBothGrids As Boolean
Dim ShowBothGridsSve As Boolean
Dim ToggleGridView As Integer
Dim MyMainCaption As String
Dim PageMainTitle As String
Dim CurInfoTabIdx As Integer

Dim MsgSysIdCount As Long
Dim MsgInfoArray(MAX_MSG_CNT) As MyInfoRec
Dim MsgSysBlinkLst(MAX_MSG_CNT) As Integer
Dim MsgSysBlinkCnt As Integer
'GFO Added
Dim Show_MyCFG As String
'Dim MsgTypeArray(MAX_MSG_TYP) As MyInfoRec

Public Sub InitApplication()
    Dim CurSection As String
    Dim CfgData As String
    Dim tmpCfgDat As String
    Dim PageFile As String
    Dim NewLeft As Long
    Dim NewSize As Long
    Dim tmpVal As Long
    If App.PrevInstance = True Then End
    'We are still not visible yet
    WorkOffLine = False
    GetGlobalConfig
    
    'Arrange a default layout
    RemoteColor(0) = &HFF00&
    RemoteColor(1) = &HC000&
    RefreshInd(0).FillColor = RemoteColor(0)
    RefreshInd(1).FillColor = RemoteColor(0)
    
    'FuncButton
    ExitChkVisible = True
    
    'Get some configured layout variations
    CurSection = StaffPageSection & "_MAIN_LAYOUT"
    MyMainCaption = GetIniEntry(StaffPageIniFile, CurSection, "", "APPL_TITLE", "UFIS Staff Monitor")
    tmpCfgDat = GetIniEntry(StaffPageIniFile, CurSection, "", "TITLE_SIZE", "340")
    PageTitle.Width = Val(tmpCfgDat)
    CfgData = GetIniEntry(StaffPageIniFile, CurSection, "", "TOP_INFO_HEIGHT", "21")
    tmpVal = Val(CfgData)
    If tmpVal < TopInfoPanel(2).Height Then tmpVal = TopInfoPanel(2).Height
    TopInfoPanel(2).Height = tmpVal
    OcmLabel(0).Top = TopInfoPanel(2).ScaleHeight - OcmLabel(0).Height
    OcmLabel(1).Top = TopInfoPanel(2).ScaleHeight - OcmLabel(1).Height
    CfgData = GetIniEntry(StaffPageIniFile, CurSection, "", "SHOW_PAGE_TITLE", "YES")
    If CfgData = "YES" Then TopInfoPanel(1).Visible = True Else TopInfoPanel(1).Visible = False
    
    ' GFO Added
    CfgData = GetIniEntry(StaffPageIniFile, CurSection, "", "SHOW_FILTER_BUTTONS", "YES")
    If CfgData = "YES" Then TopInfoPanel(10).Visible = True Else TopInfoPanel(10).Visible = False
    
    Show_MyCFG = GetIniEntry(StaffPageIniFile, CurSection, "", "SHOW_MYCFG", "YES")
     
    
    tmpCfgDat = GetIniEntry(StaffPageIniFile, CurSection, "", "SHOW_UPDATE_MSG", "NO")
    If tmpCfgDat = "YES" Then OcmPanelVisible = True Else OcmPanelVisible = False
    tmpCfgDat = GetIniEntry(StaffPageIniFile, CurSection, "", "SHOW_INFO_PANEL", "YES")
    If tmpCfgDat = "YES" Then InfoPanelVisible = True Else InfoPanelVisible = OcmPanelVisible
    TopInfoPanel(0).Visible = InfoPanelVisible
    TopInfoPanel(4).Visible = OcmPanelVisible
    OcmLabel(1).Visible = OcmPanelVisible
    tmpCfgDat = GetIniEntry(StaffPageIniFile, CurSection, "", "SHOW_BUTTON_ROW", "NO")
    If tmpCfgDat = "YES" Then ButtonPanel(1).Visible = True Else ButtonPanel(1).Visible = False
    
    tmpCfgDat = GetIniEntry(StaffPageIniFile, CurSection, "", "COUNT_DOWN", "NO")
    If tmpCfgDat = "YES" Then CountDownAnimation = True Else CountDownAnimation = False
    
    tmpVal = ButtonPanel(0).Height
    chkIcon.Height = tmpVal
    chkIcon.Width = tmpVal + 4
    PageTitle.Left = chkIcon.Left + chkIcon.Width
    PageTitle.Height = tmpVal
    chkFunc.Height = tmpVal
    MainTitle.Height = tmpVal
    lblNow.Height = tmpVal
    lblUTC.Height = tmpVal
    chkAlive.Height = tmpVal
    chkSpace.Height = tmpVal
    tmpVal = (tmpVal - LedFrame.Height) / 2
    LedFrame.Top = chkAlive.Top + tmpVal
    chkToggle(0).Height = chkIcon.Height
    chkToggle(1).Height = chkIcon.Height
    chkToggle(0).Width = chkIcon.Width
    chkToggle(1).Width = chkIcon.Width
    chkToggle(0).Left = 0
    chkToggle(1).Left = chkToggle(0).Left + chkToggle(0).Width
    TogglePanel(0).Height = chkToggle(0).Height
    TogglePanel(0).Left = chkIcon.Left + chkIcon.Width
    TogglePanel(0).Width = chkToggle(1).Left + chkToggle(1).Width
       
    
    
    tmpCfgDat = GetIniEntry(StaffPageIniFile, CurSection, "", "EXIT_WIDTH", "100")
    btnExit.Tag = tmpCfgDat
    btnExit.Width = Abs(Val(tmpCfgDat))
    chkHide.Tag = GetIniEntry(StaffPageIniFile, CurSection, "", "HIDE_APP", "NO")
    If chkHide.Tag = "YES" Then chkHide.Visible = True Else chkHide.Visible = False
    
    ArrangeBottomButtons "INIT"
    
    GetPosAndSize
    
    NewLeft = PageTitle.Left + PageTitle.Width
    MainTitle.Left = NewLeft
    NewLeft = Me.ScaleWidth - chkFunc.Width
    NewLeft = Me.ScaleWidth - 1
    If ExitChkVisible Then
        NewLeft = Me.ScaleWidth - chkFunc.Width
        chkFunc.Left = NewLeft
    End If
    NewLeft = NewLeft - chkAlive.Width
    chkAlive.Left = NewLeft
    NewLeft = NewLeft - lblUTC.Width
    lblUTC.Left = NewLeft
    NewLeft = NewLeft - lblNow.Width
    lblNow.Left = NewLeft
    NewLeft = NewLeft - MainTitle.Width
    MainTitle.Left = NewLeft
    
    chkFunc.Visible = ExitChkVisible
    LedFrame.Left = chkAlive.Left + 2
    NewLeft = chkFunc.Left + chkFunc.Width
    chkSpace.Left = NewLeft
    NewSize = MainTitle.Left - PageTitle.Left
    If NewSize < 12 Then
        NewSize = 12
    End If
    PageTitle.Width = NewSize
    
    tmpCfgDat = GetIniEntry(StaffPageIniFile, CurSection, "", "TIMES_LOCAL", "YES")
    If tmpCfgDat = "YES" Then TimesInLocal = True Else TimesInLocal = False
    
    tmpCfgDat = GetIniEntry(StaffPageIniFile, CurSection, "", "SHOW_BASICS", "NO")
    If tmpCfgDat = "YES" Then ShowBasics = True Else ShowBasics = False
    
    tmpCfgDat = GetIniEntry(StaffPageIniFile, CurSection, "", "ENABLE_RELOAD", "NO")
    If tmpCfgDat = "YES" Then ReloadOnDemand = True Else ReloadOnDemand = False
    
    ToggleButtons = GetIniEntry(StaffPageIniFile, CurSection, "", "TOGGLE_BUTTONS", ToggleButtons)
    chkLeftAction.Caption = GetIniEntry(StaffPageIniFile, CurSection, "", "LEFT_BUTTON", "LEFT_BUTTON")
    chkRightAction.Caption = GetIniEntry(StaffPageIniFile, CurSection, "", "RIGHT_BUTTON", "RIGHT_BUTTON")
    btnExit.Caption = GetIniEntry(StaffPageIniFile, CurSection, "", "EXIT_BUTTON", "EXIT")
    btnPrev.Caption = GetIniEntry(StaffPageIniFile, CurSection, "", "PREV_BUTTON", "PAGE UP")
    btnNext.Caption = GetIniEntry(StaffPageIniFile, CurSection, "", "NEXT_BUTTON", "PAGE DOWN")
    
    InitInfoPanel StaffPageSection
    InitMsgTplTab StaffPageSection
    
    GetPageDefaults StaffPageSection, 0
    GetPageDefaults StaffPageSection, 1
    
    tmpCfgDat = GetIniEntry(StaffPageIniFile, CurSection, "", "AUTO_SIZE", "YES")
    If tmpCfgDat = "YES" Then AutoSize = True Else AutoSize = False
    UpdateCount = 0
    
    CurSection = StaffPageSection & "_PAGE_LIST"
    PageFile = GetIniEntry(StaffPageIniFile, CurSection, "", "START_PAGE", "")
    
    InitPagePanel PageFile
    
    If Not RemoteControl Then
        OpenRemotePort
        'If ApplWatched = "YES" Then
        '    On Error GoTo ErrorHandle
        '    Shell "d:\ufis\system\RemoteCtrl.exe"
        '    On Error GoTo 0
        'End If
        
        'AppendLines 0
        'AppendLines 1
        'ApplicationIsStarted = True
        GotUtcDiff = False
        BcSpoolerTab.ResetContent
        BcSpoolerTab.SetFieldSeparator (Chr(16))
        'MsgBox "BC Activated"
        ReloadData(0) = False
        ReloadData(1) = False
        'PageCode(0).Caption = PageNumber(0)
        'PageCode(1).Caption = PageNumber(1)
'        If MyLoadButton = "LEFT" Then
'            PageTitle.Caption = PageCaption(0)
'            InfoTitle(0).Caption = InfoCaption(0)
'            PageCode(0).Caption = PageNumber(0)
'            PagePanel(0).Visible = True
'            DataTab(0).Visible = True
'            DataTab(1).Visible = False
'            PagePanel(1).Visible = False
'        Else
'            PageTitle.Caption = PageCaption(1)
'            InfoTitle(1).Caption = InfoCaption(1)
'            PageCode(1).Caption = PageNumber(1)
'            PagePanel(1).Visible = True
'            DataTab(1).Visible = True
'            DataTab(0).Visible = False
'            PagePanel(0).Visible = False
'        End If
        'If ShowBothGrids Then
        '    PageTitle.Caption = PageMainTitle
        '    InfoTitle(0).Caption = InfoCaption(0)
        '    'PageCode(0).Caption = PageNumber(0)
        '    PagePanel(0).Visible = True
        '    If MyDataTable(0) <> "" Then DataTab(0).Visible = True
        '    InfoTitle(1).Caption = InfoCaption(1)
        '    'PageCode(1).Caption = PageNumber(1)
        '    PagePanel(1).Visible = True
        '    If MyDataTable(1) <> "" Then DataTab(1).Visible = True
        'End If
        
        'LastBcNum(0) = 0
        'LastBcNum(1) = 0
        'LostBroadCast(0) = False
        'LostBroadCast(1) = False
        'CurLoadButton = "BOTH"
        'MinCountDown(0) = -1
        'SecCountDown(0) = -1
        'MinCountDown(1) = -1
        'SecCountDown(1) = -1
        'RefreshCountDown(0) = True
        'RefreshCountDown(1) = True
        'tmrSeconds.Interval = 500
        'tmrMinutes.Interval = 60000
        
        
    Else
        tmrSeconds.Interval = 0
        tmrMinutes.Interval = 0
        Me.Caption = "REMOTE STAFF PAGE"
        PageTitle.Caption = "STAFF PAGES"
        InfoTitle(0).Caption = "STAFF PAGES"
        MainTitle.Caption = "REMOTE CONTROL"
        lblRefresh(0).Caption = "REMOTE"
        lblRefresh(1).Caption = "00:00:00"
        lblNow.Caption = "00.00.00 / 00:00:00"
        lblUTC.Caption = "00:00:00"
        RefreshCountDown(0) = True
        RefreshCountDown(1) = True
        'tmrSeconds.Interval = 500
        'tmrMinutes.Interval = 60000
    End If
    'App.TaskVisible = False
    If chkHide.Tag = "AUTO" Then
        Hook Me.hwnd   ' Set up our handler
        AddIconToTray Me.hwnd, Me.Icon, Me.Icon.Handle, App.EXEName + " / " + MainTitle.Caption
        Me.Hide
    End If
    Exit Sub
ErrorHandle:
    Resume Next
End Sub
Private Sub GetPosAndSize()
    Dim CurSection As String
    Dim tmpCfgDat As String
    CurSection = StaffPageSection & "_MAIN_LAYOUT"
    tmpCfgDat = GetIniEntry(StaffPageIniFile, CurSection, "", "APPL_POS", "0,0")
    If MyApplMode = "KIOSK" Then tmpCfgDat = "0,0"
    Me.Left = Val(GetItem(tmpCfgDat, 1, ",")) * Screen.TwipsPerPixelX
    Me.Top = Val(GetItem(tmpCfgDat, 2, ",")) * Screen.TwipsPerPixelY
    tmpCfgDat = GetIniEntry(StaffPageIniFile, CurSection, "", "APPL_SIZE", "FULL")
    If MyApplMode = "KIOSK" Then tmpCfgDat = "FULL"
    If tmpCfgDat = "FULL" Then
        tmpCfgDat = Str(Screen.Width / Screen.TwipsPerPixelX) & "," & Screen.Height / Screen.TwipsPerPixelY
    End If
    Me.Width = Val(GetItem(tmpCfgDat, 1, ",")) * Screen.TwipsPerPixelX
    Me.Height = Val(GetItem(tmpCfgDat, 2, ",")) * Screen.TwipsPerPixelY
End Sub
Private Sub InitPagePanel(UsePageFile As String)
    Dim FileName As String
    Dim CurSection As String
    Dim GrpSection As String
    Dim CfgData As String
    Dim CurPageNumber As String
    Dim tmpCfgDat As String
    Dim i As Integer
    PageSectionFound(0) = "NO"
    PageSectionFound(1) = "NO"
    If UsePageFile <> "" Then
        'MainTitle.Caption = "PAGE " & UsePageFile
        FileName = UCase(UsePageFile)
        MyLastPageNumber = MyActivePageNumber
        MyActivePageNumber = UsePageFile
        CurPageNumber = CStr(Val(UsePageFile))
        If Left(FileName, 5) <> "PAGE_" Then FileName = "PAGE_" & FileName
        If Right(FileName, 4) <> ".INI" Then FileName = FileName & ".INI"
        PageLayoutIniFile = MyConfigPath & "\" & FileName
        CfgData = Dir(PageLayoutIniFile)
        If UCase(CfgData) = UCase(FileName) Then
            ShowBothGrids = False
            PageIsValid(0) = False
            PageIsValid(1) = False
            If Val(CurPageNumber) < 10 Then
                chkTool(9).Visible = False
                MainTool(0).Visible = False
                LeftTool(0).Visible = True
                RightTool(0).Visible = True
                MainTool(1).Visible = False
                LeftTool(1).Visible = True
                RightTool(1).Visible = True
            Else
                chkTool(9).Visible = True
                MainTool(0).Visible = True
                LeftTool(0).Visible = False
                RightTool(0).Visible = False
                MainTool(1).Visible = True
                LeftTool(1).Visible = False
                RightTool(1).Visible = False
            End If
            PagePos(0).Caption = ""
            PagePos(1).Caption = ""
            PageCnt(0).Caption = ""
            PageCnt(1).Caption = ""
            TabEditLookUpIndex = -1
            
                       
            CurSection = "PAGE_GLOBAL"
            CfgData = GetIniEntry(PageLayoutIniFile, CurSection, "", "PAGE_GROUP", "0")
            CfgData = Right("0000" & CfgData, 4)
            CfgData = "GROUP_" & CfgData
            GrpSection = CfgData
            
            CfgData = GetIniEntry(PageGroupIniFile, GrpSection, "", "GROUP_TITLE", "")
            If CfgData = "" Then NoGroupTitle(0) = True Else NoGroupTitle(0) = False
            If CfgData = "" Then CfgData = "GROUP TITLE UNDEFINED"
            PageMainTitle = GetIniEntry(PageLayoutIniFile, CurSection, "", "GROUP_TITLE", CfgData)
            NoGroupTitle(1) = NoGroupTitle(0)
            
            CfgData = GetIniEntry(PageGroupIniFile, GrpSection, "", "PAGE_GRP_BGN", "-")
            PageGrpFr(0).Caption = GetIniEntry(PageLayoutIniFile, CurSection, "", "PAGE_GRP_BGN", CfgData)
            CfgData = GetIniEntry(PageGroupIniFile, GrpSection, "", "PAGE_GRP_END", "-")
            PageGrpTo(0).Caption = GetIniEntry(PageLayoutIniFile, CurSection, "", "PAGE_GRP_END", CfgData)
            PageGrpFr(1).Caption = PageGrpFr(0).Caption
            PageGrpTo(1).Caption = PageGrpTo(0).Caption
            
            CfgData = GetIniEntry(PageGroupIniFile, GrpSection, "", "GROUP_FILTER", "")
            MyPageGrpKey(0) = CfgData
            MyPageGrpKey(1) = CfgData
            
            CfgData = GetIniEntry(PageLayoutIniFile, CurSection, "", "PAGE_NUMBER", CurPageNumber)
            PageCode(0).Caption = CfgData
            PageCode(1).Caption = PageCode(0).Caption
            
            tmpCfgDat = GetIniEntry(PageLayoutIniFile, CurSection, "", "PAGE_PANEL", "MASTER")
            If tmpCfgDat = "MASTER" Then MyLoadButton = "RIGHT" Else MyLoadButton = "LEFT"
            tmpCfgDat = GetIniEntry(PageLayoutIniFile, CurSection, "", "RELOAD_MIN", MyDefReloadMin)
            CountDownTime = Val(tmpCfgDat)
            tmpCfgDat = GetIniEntry(PageLayoutIniFile, CurSection, "", "RELOAD_MAX", MyDefReloadMax)
            RefreshCyclus = Val(tmpCfgDat)
            tmpCfgDat = GetIniEntry(PageLayoutIniFile, CurSection, "", "KEEP_UPDATE", MyDefKeepUpdate)
            KeepUpdate = Val(tmpCfgDat)
            
            currScrollPos(0) = 0
            currScrollPos(1) = 0
    
            CfgData = GetIniEntry(PageLayoutIniFile, CurSection, "", "SHOW_BOTH_GRIDS", "NO")
            If CfgData = "YES" Then
                ShowBothGrids = True
                If ToggleButtons = "YES" Then
                    ButtonPanel(1).Visible = True
                    
                End If
            Else
                'If ToggleButtons = "NO" Then
                    ButtonPanel(1).Visible = False
                'End If
                ShowBothGrids = False
            End If
            ShowBothGridsSve = ShowBothGrids
            If ToggleButtons = "YES" Then
                'TogglePanel(0).Visible = True
                'PageTitle.Left = TogglePanel(0).Left + TogglePanel(0).Width
                'PageTitle.Width = MainTitle.Left - PageTitle.Left
                TogglePanel(0).Visible = False
                PageTitle.Left = TogglePanel(0).Left
                PageTitle.Width = MainTitle.Left - PageTitle.Left
            Else
                TogglePanel(0).Visible = False
                PageTitle.Left = TogglePanel(0).Left
                PageTitle.Width = MainTitle.Left - PageTitle.Left
            End If
            
            GetPageConfig PageLayoutIniFile, "SISTER", PageCaption(0), 0
            GetPageConfig PageLayoutIniFile, "MASTER", PageCaption(1), 1
            
            PageJustLoaded = True
            If ShowBothGridsSve Then
                StartDataView = GetIniEntry(PageLayoutIniFile, CurSection, "", "START_DATA_VIEW", "BOTH")
            Else
                StartDataView = GetIniEntry(PageLayoutIniFile, CurSection, "", "START_DATA_VIEW", "MASTER")
            End If
            
            For i = 0 To 1
                CloneGridLayout TabLookUp(i), DataTab(i)
                TabLookUp(i).ShowVertScroller False
                TabLookUp(i).ShowHorzScroller False
                TabLookUp(i).Height = TabLookUp(i).LineHeight
                TabLookUp(i).MainHeaderOnly = True
                TabLookUp(i).MainHeader = False
                TabLookUp(i).CursorLifeStyle = True
                TabLookUp(i).ResetContent
                TabLookUp(i).InsertTextLine EmptyLine(i), False
                TabLookUp(i).SetCurrentSelection 0
            Next
            
            
            If (Not ShowBothGrids) And (MyLoadButton = "LEFT") Then
                PageTitle.Caption = PageCaption(0)
                InfoTitle(0).Caption = InfoCaption(0)
                If (MyDataTable(0) <> "") Or (Not NoPageTitle(0)) Then
                    PagePanel(0).Visible = True
                    DataTab(0).Visible = True
                    PageIsValid(0) = True
                End If
                DataTab(1).Visible = False
                PagePanel(1).Visible = False
            End If
            If (Not ShowBothGrids) And (MyLoadButton = "RIGHT") Then
                PageTitle.Caption = PageCaption(1)
                InfoTitle(1).Caption = InfoCaption(1)
                If (MyDataTable(1) <> "") Or (Not NoPageTitle(1)) Then
                    PagePanel(1).Visible = True
                    DataTab(1).Visible = True
                    PageIsValid(1) = True
                End If
                DataTab(0).Visible = False
                PagePanel(0).Visible = False
            End If
            If ShowBothGrids Then
                PageTitle.Caption = PageCaption(0)
                InfoTitle(0).Caption = InfoCaption(0)
                InfoTitle(1).Caption = InfoCaption(1)
                If MyDataTable(0) <> "" Then
                    PagePanel(0).Visible = True
                    DataTab(0).Visible = True
                    PageIsValid(0) = True
                Else
                    PagePanel(0).Visible = False
                    DataTab(0).Visible = False
                End If
                If MyDataTable(1) <> "" Then
                    PagePanel(1).Visible = True
                    DataTab(1).Visible = True
                    PageIsValid(1) = True
                Else
                    PagePanel(1).Visible = False
                    DataTab(1).Visible = False
                End If
            End If
            
            If ((MyDataTable(0) = "") And (NoPageTitle(0))) Or (MyDataTable(0) = "DUMMY") Then
                PageIsValid(0) = False
                DataTab(0).Visible = False
            End If
            If ((MyDataTable(1) = "") And (NoPageTitle(1))) Or (MyDataTable(1) = "DUMMY") Then
                PageIsValid(1) = False
                DataTab(1).Visible = False
            End If
            
            Select Case PageFunction(0)
                Case "PAGE", "INDEX"
                    TabCntPanel(0).Visible = True
                    PageNoPanel(0).Visible = True
                    If PageIsValid(0) Then DataTab(0).Visible = True
                Case "GROUP_INDEX", "PAGE_INDEX"
                    TabCntPanel(0).Visible = True
                    PageNoPanel(0).Visible = True
                    DataTab(0).Visible = True
                    InitGroupPages 0, PageFunction(0)
                Case "TEXT"
                    TabCntPanel(0).Visible = False
                    PageNoPanel(0).Visible = False
                    DataTab(0).Visible = False
                Case Else
            End Select
            Select Case PageFunction(1)
                Case "PAGE", "INDEX"
                    TabCntPanel(1).Visible = True
                    PageNoPanel(1).Visible = True
                    If PageIsValid(1) Then DataTab(1).Visible = True
                Case "GROUP_INDEX", "PAGE_INDEX"
                    TabCntPanel(1).Visible = True
                    PageNoPanel(1).Visible = True
                    DataTab(1).Visible = True
                    InitGroupPages 1, PageFunction(1)
                    If (FormIsVisible("MyCfg")) Then
                        If (MyCfg.chkAny(2).Value = 1) Then
                            MyCfg.ChkType(26).Value = 1
                        End If
                    End If
                Case "TEXT"
                    TabCntPanel(1).Visible = False
                    PageNoPanel(1).Visible = False
                    DataTab(1).Visible = False
                Case Else
            End Select
            
            Form_Resize
            LastBcNum(0) = 0
            LastBcNum(1) = 0
            LostBroadCast(0) = False
            LostBroadCast(1) = False
            CurLoadButton = "BOTH"
            MinCountDown(0) = -1
            SecCountDown(0) = 0
            MinCountDown(1) = -1
            SecCountDown(1) = 0
            RefreshCountDown(0) = True
            RefreshCountDown(1) = True
            If TimesInLocal = True Then lblNow.Value = 1
                
            If ApplicationIsStarted Then
                PageChanged = True
                tmrSeconds.Interval = 250
                tmrMinutes.Interval = 60000
                Me.SetFocus
            End If
        End If
    End If
End Sub

Private Sub InitInfoPanel(CfgSection As String)
    Dim CurSection As String
    Dim CfgData As String
    Dim CfgKey As String
    Dim NewTop As Long
    Dim TabTop As Long
    Dim TabLeft As Long
    Dim TabSize As Long
    Dim LineNo As Long
    Dim i As Integer
    Dim idx As Integer
    Dim max As Integer
    tmrBlink.Enabled = False
    If InfoPanelVisible Then
        For i = 0 To TopLine.UBound
            TopLine(i).Visible = False
        Next
        For i = 0 To MAX_MSG_CNT
            If i > TopLabel.UBound Then Load TopLabel(i)
            If i > TopSep.UBound Then Load TopSep(i)
            TopLabel(i).Caption = ""
            TopLabel(i).Tag = ""
            TopLabel(i).Visible = False
            TopSep(i).Visible = False
            MsgInfoArray(i).MsgSysCount = 0
            MsgInfoArray(i).MsgSysInUse = False
        Next
        CurSection = CfgSection & "_INFO_PANEL"
        MsgSysBlinkCnt = 0
        CfgData = GetIniEntry(StaffPageIniFile, CurSection, "", "INFO_LINES", "5")
        If Val(CfgData) > 0 Then
            max = Val(CfgData) - 1
            NewTop = 0
            TabTop = TopInfoPanel(0).Top
            TabLeft = 60
            CfgData = GetIniEntry(StaffPageIniFile, CurSection, "", "AREA_TITLE_WIDTH", "50")
            TabSize = Val(CfgData) * 15
            chkTopLine(0).Width = TabSize
            For idx = 0 To max
                If idx > TopLine.UBound Then
                    Load TopLine(idx)
                    Load LinePanel(idx)
                    Load chkTopLine(idx)
                    Set LinePanel(idx).Container = TopLine(idx)
                    Set chkTopLine(idx).Container = TopInfoPanel(0)
                    Load InfoTabPanel(idx)
                    Load InfoTab(idx)
                    Set InfoTab(idx).Container = InfoTabPanel(idx)
                End If
                TopLine(idx).Top = NewTop
                chkTopLine(idx).Top = NewTop
                chkTopLine(idx).Left = 0
                chkTopLine(idx).Height = TopLine(idx).Height
                TopLine(idx).Left = chkTopLine(idx).Width
                LinePanel(idx).Left = 0
                LinePanel(idx).Height = TopLine(idx).ScaleHeight
                CfgKey = "AREA" & CStr(idx + 1) & "_TITLE"
                CfgData = GetIniEntry(StaffPageIniFile, CurSection, "", CfgKey, "")
                CfgData = Replace(CfgData, "[", "", 1, -1, vbBinaryCompare)
                CfgData = Replace(CfgData, "]", "", 1, -1, vbBinaryCompare)
                chkTopLine(idx).Caption = CfgData
                CfgKey = "AREA" & CStr(idx + 1) & "_MSG_KEY"
                CfgData = "AREA" & CStr(idx + 1)
                CfgData = GetIniEntry(StaffPageIniFile, CurSection, "", CfgKey, CfgData)
                chkTopLine(idx).Tag = CfgData
                chkTopLine(idx).Visible = True
                LinePanel(idx).Visible = True
                LinePanel(idx).Tag = ""
                TopLine(idx).Visible = True
                InfoTab(idx).Visible = True
                InfoTabPanel(idx).Visible = False
                NewTop = NewTop + TopLine(0).Height
                TabTop = TabTop + (TopLine(0).Height / 15)
                TabLeft = TabLeft + 60
            Next
            NewTop = NewTop / Screen.TwipsPerPixelY
            'Add the frame pixels
            NewTop = NewTop + 4
            TopInfoPanel(0).Height = NewTop
            TopInfoPanel(4).Height = TopInfoPanel(0).Height
        End If
        InitInfoTab CurSection
        InitOcmTab
    End If
End Sub
Private Sub GetGlobalConfig()
    Dim ret As Integer
    Dim blRet As Boolean
    Dim tmpData As String
    Dim tmpCfgDat As String
    Dim tmpFile As String
    Dim tmpPath As String
    Dim tmpName As String
    Dim tmpVal As Integer
    Dim ItmNbr As Integer
    Dim CurSection As String
    'SuspendErrMsg = True
    
    'StaffPageIniFile = Trim(GetItem(Command, 1, "/"))
    StaffPageIniFile = ""
    If StaffPageIniFile = "" Then StaffPageIniFile = App.EXEName + ".ini"
    tmpPath = App.Path & "\Config"
    tmpName = Dir(tmpPath, vbDirectory)
    If tmpName = "" Then tmpPath = App.Path
    MyConfigPath = tmpPath
    StaffPageIniFile = MyConfigPath & "\" & StaffPageIniFile
    PageGroupIniFile = MyConfigPath & "\" & "PageGroups.ini"
    
    tmpCfgDat = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "SHOW_MY_UNIT", "YES")
    If tmpCfgDat = "YES" Then
        UnitLabel(0).Visible = True
        UnitLabel(1).Visible = True
    Else
        UnitLabel(0).Visible = False
        UnitLabel(1).Visible = False
    End If
    tmpCfgDat = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "SHOW_MY_LOC", "YES")
    If tmpCfgDat = "YES" Then
        UnitLabel(4).Visible = True
        UnitLabel(5).Visible = True
    Else
        UnitLabel(4).Visible = False
        UnitLabel(5).Visible = False
    End If
    tmpCfgDat = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "SHOW_MY_PID", "NO")
    If tmpCfgDat = "YES" Then
        UnitLabel(6).Visible = True
        UnitLabel(7).Visible = True
    Else
        UnitLabel(6).Visible = False
        UnitLabel(7).Visible = False
    End If
    tmpCfgDat = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "SHOW_MY_SID", "NO")
    If tmpCfgDat = "YES" Then
        UnitLabel(8).Visible = True
        UnitLabel(9).Visible = True
    Else
        UnitLabel(8).Visible = False
        UnitLabel(9).Visible = False
    End If
    tmpCfgDat = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "SHOW_FOCUS_LED", "NO")
    If tmpCfgDat = "YES" Then
        LedPanel(3).Visible = True
    Else
        LedPanel(3).Visible = False
    End If
    
    ApplWatched = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "APPL_WATCHED", "NO")
    MyHostName = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "HOSTNAME", "LOCAL")
    If MyHostName = "LOCAL" Then WorkOffLine = True
    MyHostType = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "HOSTTYPE", "PRODUCTION")
    MyTblExt = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "TABLEEXTENSION", "TAB")
    MyHopo = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "HOMEAIRPORT", "???")
    HomeAirport = MyHopo
    MyUserName = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "UFIS_USER", "STFMON")
    MyShortName = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "SHORT_NAME", "STAFF")
    ToggleButtons = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "TOGGLE_BUTTONS", "NO")
    SavePath = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "SAVE_PATH", "")
    If Len(SavePath) > 0 And Right(SavePath, 1) <> "\" Then SavePath = SavePath + "\"
    tmpData = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "SAVE_TIMER", "-1")
    DefSaveTimer = Val(tmpData)
    ActSaveTimer = DefSaveTimer
    
    SendUpdates = False
    CdiIdx = -1
    If Not RemoteControl Then
        tmpCfgDat = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "KIOSK_MODE", "YES")
        If (MyWksType <> "WKS") And (tmpCfgDat = "YES") Then
            ApplDefaultKioskMode = True
        Else
            ApplDefaultKioskMode = False
        End If
        tmpCfgDat = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "APPL_MODE", "APPL")
        MyApplMode = tmpCfgDat
        If MyWksType = "WKS" Then MyApplMode = "APPL"
        If MyApplMode = "KIOSK" Then
            ApplIsInKioskMode = True
            tmpCfgDat = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "SHOW_MOUSE", "YES")
            If tmpCfgDat = "YES" Then ShowMousePointer = True Else ShowMousePointer = False
            'Me.Moveable = False
            SysFunc.HideMyMouse
            SysFunc.HideMyTaskBar
            SysFunc.HideMyDesktop
            LedPanel(3).Visible = False
        Else
            ApplIsInKioskMode = False
            tmpCfgDat = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "SHOW_MOUSE", "YES")
            If tmpCfgDat = "YES" Then ShowMousePointer = True Else ShowMousePointer = False
            'Me.Moveable = True
            SysFunc.ShowMyMouse
            SysFunc.ShowMyTaskBar
            SysFunc.ShowMyDesktop
            LedPanel(3).Visible = True
        End If
        
    
        
        tmpData = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "AUTO_LOGOFF", "-1")
        If Val(tmpData) > 0 Then
            tmrExit.Tag = CStr(Val(tmpData) * 60)
            tmrExit.Enabled = True
            TcpLed(4).Visible = True
            TcpLed(5).Visible = False
            tmpData = "LOGOFF"
        Else
            tmrExit.Enabled = False
            TcpLed(4).Visible = False
            TcpLed(5).Visible = True
            tmpData = "EXIT"
        End If
        tmpData = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "EXIT_MODE", tmpData)
        WinShut.SetExitMode tmpData
        tmpData = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "ASK_USER_ON_EXIT", "NO")
        If tmpData = "YES" Then AskUser = True Else AskUser = False
        tmpData = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "SEND_UPDATES", "NO")
        If tmpData = "YES" Then
            OpenCdiPort
            SendUpdates = True
            tmpData = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "CDI_REC_SEP", "LF")
            Select Case tmpData
                Case "LF"
                    CdiRecSep = vbLf
                Case "CR"
                    CdiRecSep = vbCr
                Case "CRLF"
                    CdiRecSep = vbNewLine
                Case "NONE"
                    CdiRecSep = ""
                Case Else
                    CdiRecSep = ""
            End Select
        End If
        StaffPageSection = Trim(GetItem(Command, 2, "/"))
        If StaffPageSection = "" Then
            StaffPageSection = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "ACTIVE_SECTION", "STAFF_MONITOR")
        End If
    Else
        tmpData = GetIniEntry(StaffPageIniFile, "GLOBAL", "", "ACCESS_PORT", "0000")
        If tmpData <> "4397" Then
            MsgBox "Sorry, access denied. No Permission for Remote Control !"
            End
        End If
    End If
    
    MyApplVersion = "StaffPage," & App.Major & "." & App.Minor & "." & App.Revision & ",11"
    MyTcpIpAdr = RbcBcPort(0).LocalIP
    HexTcpAdr = ""
    For ItmNbr = 1 To 4
        tmpVal = Val(GetItem(MyTcpIpAdr, ItmNbr, "."))
        tmpData = Hex(tmpVal)
        HexTcpAdr = HexTcpAdr + Right("00" + tmpData, 2)
    Next
    
    UpdateOnReload = True
    CdiSendOnConnex = True
    
End Sub

Private Sub GetPageDefaults(CurIniSection As String, CurTabIdx As Integer)
    Dim CurSection As String
    Dim tmpCfgDat As String
    Dim tmpVal As Long
    Dim NewTop As Long
    Dim NewLeft As Long
    Dim NewSize As Long
    Dim ChkSize0 As Long
    Dim ChkSize1 As Long
    Dim ChkSize2 As Long
    Dim i As Integer
    CurSection = CurIniSection & "_PAGE_DEFAULTS"
    
    MyTabFontName(CurTabIdx) = GetIniEntry(StaffPageIniFile, CurSection, "", "FONT_NAME", MyTabFontName(CurTabIdx))
    DataTab(CurTabIdx).FontName = MyTabFontName(CurTabIdx)
    
    tmpCfgDat = GetIniEntry(StaffPageIniFile, CurSection, "", "LINE_HEIGHT", "24")
    MyDefLineHeight(CurTabIdx) = Val(tmpCfgDat)
    DataTab(CurTabIdx).LineHeight = Val(tmpCfgDat)
    tmpCfgDat = GetIniEntry(StaffPageIniFile, CurSection, "", "TEXT_HEIGHT", "24")
    MyDefFontSize(CurTabIdx) = Val(tmpCfgDat)
    DataTab(CurTabIdx).HeaderFontSize = Val(tmpCfgDat)
    DataTab(CurTabIdx).FontSize = Val(tmpCfgDat)
    
    'CURRENTLY ONE SET OF BACKCOLOR FOR BOTH GRIDS
    MyDefLineColors(CurTabIdx) = GetIniEntry(StaffPageIniFile, CurSection, "", "LINE_COLORS", "DARK,LIGHT")
    GetColorValues MyDefLineColors(CurTabIdx)
    MyDefTextColor(CurTabIdx) = GetIniEntry(StaffPageIniFile, CurSection, "", "TEXT_COLOR", "YELLOW")
    MyLineTextColor(CurTabIdx) = TranslateColor(MyDefTextColor(CurTabIdx))
    'CURRENTLY ONE TEXT COLOR FOR BOTH GRIDS
    LineTextColor = MyLineTextColor(CurTabIdx)
    
    MyDefTrigger01(CurTabIdx) = GetIniEntry(StaffPageIniFile, CurSection, "", "TRIGGER_01", "-60")
    MyDefTrigger02(CurTabIdx) = GetIniEntry(StaffPageIniFile, CurSection, "", "TRIGGER_02", "240")
    MyDefTrigger03(CurTabIdx) = GetIniEntry(StaffPageIniFile, CurSection, "", "TRIGGER_03", "-10")
    
    tmpCfgDat = GetIniEntry(StaffPageIniFile, CurSection, "", "RELOAD_MIN", "2")
    MyDefReloadMin = tmpCfgDat
    CountDownTime = Val(tmpCfgDat)
    tmpCfgDat = GetIniEntry(StaffPageIniFile, CurSection, "", "RELOAD_MAX", "5")
    MyDefReloadMax = tmpCfgDat
    RefreshCyclus = Val(tmpCfgDat)
    tmpCfgDat = GetIniEntry(StaffPageIniFile, CurSection, "", "KEEP_UPDATE", "20")
    MyDefKeepUpdate = tmpCfgDat
    KeepUpdate = Val(tmpCfgDat)
    
    
    tmpCfgDat = GetIniEntry(StaffPageIniFile, CurSection, "", "SCROLLBAR_V", "NO")
    If tmpCfgDat = "YES" Then DataTab(CurTabIdx).ShowVertScroller True Else DataTab(CurTabIdx).ShowVertScroller False
    tmpCfgDat = GetIniEntry(StaffPageIniFile, CurSection, "", "SCROLLBAR_H", "NO")
    If tmpCfgDat = "YES" Then DataTab(CurTabIdx).ShowHorzScroller True Else DataTab(CurTabIdx).ShowHorzScroller False
    
    DataTab(CurTabIdx).SetTabFontBold True
    
End Sub

Private Sub GetPageConfig(PageLayoutIniFile As String, CurAction As String, CurPageTitle As String, CurTabIdx As Integer)
    Dim CurSection As String
    Dim tmpData As String
    Dim tmpVal As String
    Dim tmpMainHead As String
    Dim tmpMainGroup As String
    Dim ItmNbr As Integer
    Dim CurPos As Integer
    Dim CurCol As Long
    Dim ItmCnt As Integer
    Dim ColCnt As Long
    Dim MaxItm As Long
    Dim MaxCol As Long
    Dim ColDif As Long
    Dim CurItm As Long
    Dim FldNam As String
    Dim i As Integer
    PageSectionFound(CurTabIdx) = "YES"
    DataTab(CurTabIdx).ResetContent
    MaxCol = DataTab(CurTabIdx).GetColumnCount - 1
    For CurCol = 0 To MaxCol
        DataTab(CurTabIdx).DateTimeResetColumn CurCol
        DataTab(CurTabIdx).DateTimeSetInputFormatString CurCol, ""
        DataTab(CurTabIdx).DateTimeSetOutputFormatString CurCol, ""
        DataTab(CurTabIdx).DateTimeSetUTCOffsetMinutes CurCol, 0
    Next
    
    CurSection = "PAGE" + "_" + CurAction
    'CURRENTLY ONE SET OF BACKCOLOR FOR BOTH GRIDS
    tmpData = GetIniEntry(PageLayoutIniFile, CurSection, "", "LINE_COLORS", MyDefLineColors(CurTabIdx))
    MyTabLineColors(CurTabIdx) = tmpData
    GetColorValues tmpData
    tmpData = GetIniEntry(PageLayoutIniFile, CurSection, "", "TEXT_COLOR", MyDefTextColor(CurTabIdx))
    MyTabTextColor(CurTabIdx) = tmpData
    MyLineTextColor(CurTabIdx) = TranslateColor(tmpData)
    'CURRENTLY ONE TEXT COLOR FOR BOTH GRIDS
    LineTextColor = MyLineTextColor(CurTabIdx)
    
    'TO BE MADE CONFIGURABLE
    DataTab(CurTabIdx).SelectColumnBackColor = TranslateColor("AMBER3")
    DataTab(CurTabIdx).SelectColumnTextColor = vbBlack
    DataTab(CurTabIdx).ColSelectionRemoveAll
            
    AppendLines CurTabIdx
    
    tmpData = GetIniEntry(PageLayoutIniFile, CurSection, "", "PAGE_FUNCTION", "PAGE")
    tmpData = GetIniEntry(PageLayoutIniFile, CurSection, "", "PAGE_LAYOUT", tmpData)
    PageFunction(CurTabIdx) = tmpData
    
    tmpData = PageMainTitle
    PageCaption(CurTabIdx) = GetIniEntry(PageLayoutIniFile, CurSection, "", "PAGE_TITLE", tmpData)
    
    If NoGroupTitle(CurTabIdx) Then tmpData = "" Else tmpData = PageCaption(CurTabIdx)
    tmpData = GetIniEntry(PageLayoutIniFile, CurSection, "", "INFO_TITLE", tmpData)
    If tmpData = "" Then NoPageTitle(CurTabIdx) = True Else NoPageTitle(CurTabIdx) = False
    If tmpData = "" Then tmpData = "PAGE TITLE UNDEFINED"
    tmpData = Replace(tmpData, "#", " ", 1, -1, vbBinaryCompare)
    InfoCaption(CurTabIdx) = tmpData
    
    PageCursor(CurTabIdx) = GetIniEntry(PageLayoutIniFile, CurSection, "", "SCROLL_TYPE", "PAGE")
    
    MyPageFields(CurTabIdx) = GetIniEntry(PageLayoutIniFile, CurSection, "", "FIELD_LIST", "")
    MyCedaFields(CurTabIdx) = GetIniEntry(PageLayoutIniFile, CurSection, "", "CEDA_FIELDS", "")
    MyTimeFields(CurTabIdx) = GetIniEntry(PageLayoutIniFile, CurSection, "", "TIME_FIELDS", "")
    MyDateFields(CurTabIdx) = GetIniEntry(PageLayoutIniFile, CurSection, "", "DATE_FIELDS", "")
    MyHourFields(CurTabIdx) = GetIniEntry(PageLayoutIniFile, CurSection, "", "HOUR_FIELDS", "")
    DataTab(CurTabIdx).LogicalFieldList = MyPageFields(CurTabIdx)
    MyFieldLen = GetIniEntry(PageLayoutIniFile, CurSection, "", "WIDTH_PIXEL", tmpVal)
    DataTab(CurTabIdx).HeaderLengthString = MyFieldLen + ",53,110,53,53"
    CurPos = 0
    ItmNbr = 0
    CurCol = -1
    MyTimeCols(CurTabIdx) = ""
    EmptyLine(CurTabIdx) = ""
    Do
        ItmNbr = ItmNbr + 1
        CurCol = CurCol + 1
        FldNam = GetItem(MyPageFields(CurTabIdx), ItmNbr, ",")
        If FldNam <> "" Then
            tmpVal = GetItem(MyFieldLen, ItmNbr, ",")
            If tmpVal <> "" Then
                CurPos = CurPos + Val(tmpVal)
            End If
            If InStr(MyTimeFields(CurTabIdx), FldNam) > 0 Then
                DataTab(CurTabIdx).DateTimeSetColumn CurCol
                DataTab(CurTabIdx).DateTimeSetInputFormatString CurCol, "YYYYMMDDhhmmss"
                DataTab(CurTabIdx).DateTimeSetOutputFormatString CurCol, "hh':'mm"
                MyTimeCols(CurTabIdx) = MyTimeCols(CurTabIdx) & CStr(CurCol) & ","
            End If
            If InStr(MyDateFields(CurTabIdx), FldNam) > 0 Then
                DataTab(CurTabIdx).DateTimeSetColumn CurCol
                DataTab(CurTabIdx).DateTimeSetInputFormatString CurCol, "YYYYMMDD"
                DataTab(CurTabIdx).DateTimeSetOutputFormatString CurCol, "DDMMMYY"
            End If
            If InStr(MyCedaFields(CurTabIdx), FldNam) > 0 Then
                DataTab(CurTabIdx).DateTimeSetColumn CurCol
                DataTab(CurTabIdx).DateTimeSetInputFormatString CurCol, "YYYYMMDDhhmmss"
                DataTab(CurTabIdx).DateTimeSetOutputFormatString CurCol, "DDMMMYY'/'hh':'mm"
            End If
            If InStr(MyHourFields(CurTabIdx), FldNam) > 0 Then
                DataTab(CurTabIdx).DateTimeSetColumn CurCol
                DataTab(CurTabIdx).DateTimeSetInputFormatString CurCol, "hhmm"
                DataTab(CurTabIdx).DateTimeSetOutputFormatString CurCol, "hh':'mm"
            End If
            EmptyLine(CurTabIdx) = EmptyLine(CurTabIdx) & ","
        End If
    Loop While FldNam <> ""
    
    
    MyPageFieldCount(CurTabIdx) = CurCol
    'DataTab(CurTabIdx).Width = CurPos + 1000
    DataTab(CurTabIdx).Left = 2
    
    tmpData = GetIniEntry(PageLayoutIniFile, CurSection, "", "LIST_HEADER", "")
    If tmpData = "" Then NoGridHeader(CurTabIdx) = True Else NoGridHeader(CurTabIdx) = False
    If tmpData <> "" Then tmpData = tmpData & ",STA,SRT,MRK,VER" Else tmpData = "GRID HEADER UNDEFDINED@,,,,,"
    tmpData = Replace(tmpData, "@", Space(100), 1, -1, vbBinaryCompare)
    DataTab(CurTabIdx).HeaderString = tmpData
    
    DataTab(CurTabIdx).LeftTextOffset = Val(GetIniEntry(PageLayoutIniFile, CurSection, "", "LEFT_MARGIN", "0"))
    
    MyCedaTitle(CurTabIdx) = GetIniEntry(PageLayoutIniFile, CurSection, "", "CEDA_TITLE", "LOAD")
    
    MyDataTable(CurTabIdx) = GetIniEntry(PageLayoutIniFile, CurSection, "", "TABLE_NAME", "")
    If MyDataTable(CurTabIdx) = "" Then NoTableName(CurTabIdx) = True Else NoTableName(CurTabIdx) = False
    MyPageSqlKey(CurTabIdx) = GetIniEntry(PageLayoutIniFile, CurSection, "", "USE_SQL_KEY", "")
    
    MyTabTrigger01(CurTabIdx) = GetIniEntry(PageLayoutIniFile, CurSection, "", "TRIGGER_01", MyDefTrigger01(CurTabIdx))
    MyTabTrigger02(CurTabIdx) = GetIniEntry(PageLayoutIniFile, CurSection, "", "TRIGGER_02", MyDefTrigger02(CurTabIdx))
    MyTabTrigger03(CurTabIdx) = GetIniEntry(PageLayoutIniFile, CurSection, "", "TRIGGER_03", MyDefTrigger03(CurTabIdx))
    
    tmpData = GetIniEntry(PageLayoutIniFile, CurSection, "", "AUTO_SIZE_COLS", "YES")
    If tmpData = "YES" Then MyAutoSizeCols(CurTabIdx) = True Else MyAutoSizeCols(CurTabIdx) = False
    
    UseBcNums(CurTabIdx) = GetIniEntry(PageLayoutIniFile, CurSection, "", "BC_NUMBERS", "ALL")
    MyUrnoIdx(CurTabIdx) = GetItemNo(MyPageFields(CurTabIdx), "URNO") - 1
    MyFlnoIdx(CurTabIdx) = GetItemNo(MyPageFields(CurTabIdx), "FLNO") - 1
    MyCkifIdx(CurTabIdx) = GetItemNo(MyPageFields(CurTabIdx), "CKIF") - 1
    MyCkitIdx(CurTabIdx) = GetItemNo(MyPageFields(CurTabIdx), "CKIT") - 1
    MyStodIdx(CurTabIdx) = GetItemNo(MyPageFields(CurTabIdx), "STOD") - 1
    CdiPageFields(CurTabIdx) = GetIniEntry(PageLayoutIniFile, CurSection, "", "CDI_FLD_LST", "")
    CdiFieldLength(CurTabIdx) = GetIniEntry(PageLayoutIniFile, CurSection, "", "CDI_FLD_LEN", "")
    CdiKeyFields(CurTabIdx) = GetIniEntry(PageLayoutIniFile, CurSection, "", "CDI_KEY_FLD", "")
    CdiTriggers(CurTabIdx) = GetIniEntry(PageLayoutIniFile, CurSection, "", "CDI_TRIGGER", "")
    DataTab(CurTabIdx).LifeStyle = True
    'DataTab(CurTabIdx).CursorLifeStyle = True
    SupportCCaTab(CurTabIdx) = False
    tmpData = GetIniEntry(PageLayoutIniFile, CurSection, "", "SUPPORT_CCA", "NO")
    If tmpData = "YES" Then SupportCCaTab(CurTabIdx) = True
    'DataTab(CurTabIdx).GridLineColor = DarkLineColor
    'DataTab(CurTabIdx).GridLineColor = vbMagenta
    
    DataTab(CurTabIdx).MainHeader = True
    tmpMainHead = GetIniEntry(PageLayoutIniFile, CurSection, "", "MAIN_HEADER", "")
    tmpMainGroup = GetIniEntry(PageLayoutIniFile, CurSection, "", "MAIN_GROUPS", "")
    ItmCnt = CInt(ItemCount(tmpMainHead, ","))
    ColCnt = 0
    For i = 1 To ItmCnt
        ColCnt = ColCnt + Val(GetItem(tmpMainGroup, i, ","))
    Next
    MaxCol = DataTab(CurTabIdx).GetColumnCount
    ColDif = CInt(MaxCol - ColCnt)
    If ColDif > 0 Then
        If tmpMainHead <> "" Then tmpMainHead = tmpMainHead & ","
        If tmpMainGroup <> "" Then tmpMainGroup = tmpMainGroup & ","
        tmpMainGroup = tmpMainGroup & CStr(ColDif)
    End If
    DataTab(CurTabIdx).SetMainHeaderValues tmpMainGroup, tmpMainHead, ""
    DataTab(CurTabIdx).SetMainHeaderFont 23, False, False, True, 0, MyTabFontName(CurTabIdx)
    
    DataTab(CurTabIdx).CreateDecorationObject "CIC1", "T,L", "2,2", Str(vbWhite) & "," & Str(vbWhite)
    DataTab(CurTabIdx).CreateDecorationObject "CIC2", "T,R", "2,3", Str(vbWhite) & "," & Str(vbBlack)
    DataTab(CurTabIdx).CreateDecorationObject "CIC3", "L", "2", Str(vbWhite)
    DataTab(CurTabIdx).CreateDecorationObject "CIC4", "R", "3", Str(vbBlack)
    DataTab(CurTabIdx).CreateDecorationObject "CIC5", "B,L", "3,2", Str(vbBlack) & "," & Str(vbWhite)
    DataTab(CurTabIdx).CreateDecorationObject "CIC6", "B,R", "3,3", Str(vbBlack) & "," & Str(vbBlack)
    DataTab(CurTabIdx).CreateDecorationObject "UPDS", "B,R,T,L", "2,2,2,2", Str(vbRed) & "," & Str(vbRed) & "," & Str(vbRed) & "," & Str(vbRed)
    DataTab(CurTabIdx).CursorDecoration DataTab(CurTabIdx).LogicalFieldList, "B,L,T,R", "3,3,2,2", Str(vbBlack) & "," & Str(vbWhite) & "," & Str(vbWhite) & "," & Str(vbBlack)
    
    DataTab(CurTabIdx).CreateCellObj "UpdCell", vbRed, vbWhite, DataTab(CurTabIdx).FontSize, False, False, True, 0, MyTabFontName(CurTabIdx)
    DataTab(CurTabIdx).CreateCellObj "UpdCell2", vbRed, vbBlack, DataTab(CurTabIdx).FontSize, False, False, True, 0, MyTabFontName(CurTabIdx)
    
    DataTab(CurTabIdx).DefaultCursor = False
    
    tmpData = "L,L,"
    For CurCol = 2 To DataTab(CurTabIdx).GetColumnCount - 1
        tmpData = tmpData & "C,"
    Next
    tmpData = GetIniEntry(PageLayoutIniFile, CurSection, "", "ALIGN_DATA", tmpData)
    tmpData = Replace(tmpData, " ", "", 1, -1, vbBinaryCompare)
    DataTab(CurTabIdx).ColumnAlignmentString = tmpData
    tmpData = GetIniEntry(PageLayoutIniFile, CurSection, "", "ALIGN_HEAD", tmpData)
    tmpData = Replace(tmpData, " ", "", 1, -1, vbBinaryCompare)
    DataTab(CurTabIdx).HeaderAlignmentString = tmpData
    DataTab(CurTabIdx).AutoSizeByHeader = True
    If MyAutoSizeCols(CurTabIdx) = True Then
        DataTab(CurTabIdx).AutoSizeColumns
    End If
    TabLookUp(CurTabIdx).HeaderLengthString = DataTab(CurTabIdx).HeaderLengthString
    TabLookUp(CurTabIdx).ColumnAlignmentString = DataTab(CurTabIdx).ColumnAlignmentString
    
End Sub

Private Sub QueryFlights(CurTabIdx As Integer)
    Dim ret As Integer
    Dim MaxItm As Integer
    Dim CurItm As Integer
    Dim Where As String
    Dim i As Integer
    Dim Records As Long
    Dim LineCount As Integer
    Dim strRawBuffer As String
    Dim FldNam As String
    Dim FldDat As String
    Dim CurFlno As String
    Dim CurUrno As String
    Dim CurCkif As String
    Dim CurCkit As String
    Dim CurStod As String
    Dim tmpUrno As String
    Dim CurLinNbr As String
    Dim tmpLinNbr As String
    Dim LinCnt As Integer
    Dim newRecDat As String
    Dim sveRecDat As String
    Dim CcaRecDat As String
    Dim strTotalBuffer As String
    Dim CurAction As String
    Dim tmpSave As String
    Dim DarkLine As Boolean
    Dim CcatabWhere As String
    Dim CCaCnt As Long
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim UpdLine As Long
    Dim Via3List As String
    Dim tmpVia3 As String
    Dim tmpVial As String
    Dim JfnoList As String
    Dim tmpJfno As String
    Dim cPos As Integer
    Dim tmpData As String
    If ApplIsInKioskMode Then SysFunc.ShowMyMouse
    Screen.MousePointer = 11
    If WorkOffLine Then CedaIsConnected = False
    tmrMinutes.Interval = 0
    tmrSeconds.Interval = 0
    Where = MakeWhere(CurTabIdx, CcatabWhere)
    CloseConnection False
    OpenConnection False
    If CcatabWhere <> "" Then
        If (MyUrnoIdx(CurTabIdx) < 0) Then SupportCCaTab(CurTabIdx) = False
        If (MyCkifIdx(CurTabIdx) < 0) Then SupportCCaTab(CurTabIdx) = False
        If (MyCkitIdx(CurTabIdx) < 0) Then SupportCCaTab(CurTabIdx) = False
        If (MyFlnoIdx(CurTabIdx) < 0) Then SupportCCaTab(CurTabIdx) = False
        If (MyStodIdx(CurTabIdx) < 0) Then SupportCCaTab(CurTabIdx) = False
        If SupportCCaTab(CurTabIdx) Then BasicData.LoadCcaTab CcatabWhere, False
    End If
    PrepareMessage MyCedaTitle(CurTabIdx)
    CedaWasConnected = False
    If CedaIsConnected = True Then
        CedaWasConnected = True
        If UpdateOnReload = True Then
            MarkTabLines CurTabIdx, -1, MyPageFieldCount(CurTabIdx) + 2, "OLD"
        End If
        strTotalBuffer = ""
        If PageSectionFound(CurTabIdx) = "YES" Then
            CedaCon(0).FillColor = vbGreen
            'MsgLogging UCase(App.EXEName) & " LOADING " & omDisplayMode
            tmpSave = MainTitle.Caption
            MainTitle.Caption = "LOADING"
            ReadCedaData CdrHdl, MyDataTable(CurTabIdx), MyPageFields(CurTabIdx), Where
            LineCount = CdrHdl.GetLineCount - 1
            MaxItm = ItemCount(MyPageFields(CurTabIdx), ",")
            LinCnt = 0
            For i = 0 To LineCount
                newRecDat = CdrHdl.GetLineValues(i)
                FldDat = GetFieldValue("MTOW", newRecDat, MyPageFields(CurTabIdx))
                If FldDat <> "" Then
                    FldDat = Trim(GetItem(FldDat, 1, "."))
                    FldDat = Right("00000" + FldDat, 5)
                    SetFieldValue "MTOW", FldDat, newRecDat, MyPageFields(CurTabIdx)
                End If
                FldDat = GetFieldValue("DOOP", newRecDat, MyPageFields(CurTabIdx))
                If FldDat <> "" Then
                    FldDat = Replace(FldDat, "0", "-", 1, -1, vbBinaryCompare)
                    SetFieldValue "DOOP", FldDat, newRecDat, MyPageFields(CurTabIdx)
                End If
                FldDat = GetFieldValue("FREQ", newRecDat, MyPageFields(CurTabIdx))
                If FldDat <> "" Then
                    FldDat = Replace(FldDat, "1", "", 1, -1, vbBinaryCompare)
                    SetFieldValue "FREQ", FldDat, newRecDat, MyPageFields(CurTabIdx)
                End If
                FldDat = GetFieldValue("NSTP", newRecDat, MyPageFields(CurTabIdx))
                If FldDat <> "" Then
                    FldDat = Replace(FldDat, "0", "", 1, -1, vbBinaryCompare)
                    SetFieldValue "NSTP", FldDat, newRecDat, MyPageFields(CurTabIdx)
                End If
                If UpdateOnReload = True Then
                    CurLinNbr = Right("000000" + CStr(LinCnt), 6)
                    tmpLinNbr = CurLinNbr & "_0000000000"
                    JfnoList = ""
                    If InStr(MyPageFields(CurTabIdx), "JFNO") > 0 Then
                        'FldDat = GetFieldValue("JFNO", newRecDat, MyPageFields(CurTabIdx))
                        'If FldDat = "" Then
                        '    FldDat = "BA 7381  JL 5084  9W 4081  AF 8087"
                        '    SetFieldValue "JFNO", FldDat, newRecDat, MyPageFields(CurTabIdx)
                        'End If
                        FldDat = GetFieldValue("JFNO", newRecDat, MyPageFields(CurTabIdx))
                        If FldDat <> "" Then
                            JfnoList = FldDat
                            tmpJfno = ""
                            SetFieldValue "JFNO", tmpJfno, newRecDat, MyPageFields(CurTabIdx)
                        End If
                    End If
                    If InStr(MyPageFields(CurTabIdx), "VIAL") > 0 Then
                        Via3List = ""
                        FldDat = GetFieldValue("VIAL", newRecDat, MyPageFields(CurTabIdx))
                        If FldDat <> "" Then
                            cPos = 2
                            tmpVia3 = "START"
                            While tmpVia3 <> ""
                                tmpVia3 = Mid(FldDat, cPos, 3)
                                If tmpVia3 = "" Then tmpVia3 = Mid(FldDat, cPos + 3, 4)
                                If tmpVia3 <> "" Then
                                    Via3List = Via3List & tmpVia3 & "/"
                                End If
                                cPos = cPos + 120
                            Wend
                            Via3List = Left(Via3List, Len(Via3List) - 1)
                        End If
                        SetFieldValue "VIAL", Via3List, newRecDat, MyPageFields(CurTabIdx)
                        If InStr(MyPageFields(CurTabIdx), "'RD'") > 0 Then
                            tmpVia3 = GetFieldValue("DES3", newRecDat, MyPageFields(CurTabIdx))
                            If Via3List <> "" Then Via3List = Via3List & "/"
                            Via3List = Via3List & tmpVia3
                            SetFieldValue "'RD'", Via3List, newRecDat, MyPageFields(CurTabIdx)
                        End If
                        If InStr(MyPageFields(CurTabIdx), "'RA'") > 0 Then
                            tmpVia3 = GetFieldValue("ORG3", newRecDat, MyPageFields(CurTabIdx))
                            If Via3List <> "" Then Via3List = "/" & Via3List
                            Via3List = tmpVia3 & Via3List
                            SetFieldValue "'RA'", Via3List, newRecDat, MyPageFields(CurTabIdx)
                        End If
                    End If
                    sveRecDat = newRecDat
                    newRecDat = newRecDat + ",NEW," + tmpLinNbr + ",NEW,"
                    UpdLine = RefreshOnBc("UFR", MyPageFields(CurTabIdx), newRecDat, CurTabIdx, CurLinNbr, False)
                    If (UpdLine >= 0) And (JfnoList <> "") Then
                        newRecDat = sveRecDat
                        CurUrno = GetFieldValue("URNO", newRecDat, MyPageFields(CurTabIdx))
                        cPos = 1
                        tmpJfno = "START"
                        While tmpJfno <> ""
                            newRecDat = sveRecDat
                            tmpJfno = Trim(Mid(JfnoList, cPos, 9))
                            If tmpJfno <> "" Then
                                tmpData = Replace(tmpJfno, " ", "_", 1, -1, vbBinaryCompare)
                                tmpUrno = CurUrno & "_" & CStr(cPos)
                                tmpJfno = "[" & tmpJfno & "]"
                                SetFieldValue "FLNO", tmpJfno, newRecDat, MyPageFields(CurTabIdx)
                                SetFieldValue "URNO", tmpUrno, newRecDat, MyPageFields(CurTabIdx)
                                tmpLinNbr = CurLinNbr & "_" & Left(tmpData & "_000000000", 10)
                                newRecDat = newRecDat + ",NEW," + tmpLinNbr + ",NEW,"
                                UpdLine = RefreshOnBc("UFR", MyPageFields(CurTabIdx), newRecDat, CurTabIdx, CurLinNbr, False)
                                cPos = cPos + 9
                            End If
                        Wend
                    End If
                    If (UpdLine >= 0) And (SupportCCaTab(CurTabIdx) = True) Then
                        CurUrno = GetFieldValue("URNO", newRecDat, MyPageFields(CurTabIdx))
                        CurFlno = GetFieldValue("FLNO", newRecDat, MyPageFields(CurTabIdx))
                        CurCkif = GetFieldValue("CKIF", newRecDat, MyPageFields(CurTabIdx))
                        CurCkit = GetFieldValue("CKIT", newRecDat, MyPageFields(CurTabIdx))
                        CurStod = GetFieldValue("STOD", newRecDat, MyPageFields(CurTabIdx))
                        CCaCnt = 0
                        If CurCkif <> "" Then CCaCnt = BasicData.LookUpCca(CurUrno, CurFlno, CurStod, CurCkif, CurCkit, False)
                        If CCaCnt > 0 Then
                            DataTab(CurTabIdx).SetDecorationObject UpdLine, MyCkifIdx(CurTabIdx), "CIC1"
                            DataTab(CurTabIdx).SetDecorationObject UpdLine, MyCkitIdx(CurTabIdx), "CIC2"
                            MaxLine = CCaCnt - 1
                            For CurLine = 0 To MaxLine
                                CcaRecDat = EmptyLine(CurTabIdx)
                                tmpUrno = CurUrno & "_" & CStr(CurLine + 1)
                                tmpLinNbr = CurLinNbr & "_" & CStr(CurLine + 1)
                                FldDat = BasicData.CicGrp(1).GetColumnValue(CurLine, 1)
                                SetItem CcaRecDat, (MyCkifIdx(CurTabIdx) + 1), ",", FldDat
                                FldDat = BasicData.CicGrp(1).GetColumnValue(CurLine, 2)
                                SetItem CcaRecDat, (MyCkitIdx(CurTabIdx) + 1), ",", FldDat
                                SetItem CcaRecDat, (MyUrnoIdx(CurTabIdx) + 1), ",", tmpUrno
                                CcaRecDat = CcaRecDat + ",NEW," + tmpLinNbr + ",NEW,"
                                UpdLine = RefreshOnBc("UFR", MyPageFields(CurTabIdx), CcaRecDat, CurTabIdx, CurLinNbr, False)
                                DataTab(CurTabIdx).SetDecorationObject UpdLine, MyCkifIdx(CurTabIdx), "CIC3"
                                DataTab(CurTabIdx).SetDecorationObject UpdLine, MyCkitIdx(CurTabIdx), "CIC4"
                            Next
                            DataTab(CurTabIdx).SetDecorationObject UpdLine, MyCkifIdx(CurTabIdx), "CIC5"
                            DataTab(CurTabIdx).SetDecorationObject UpdLine, MyCkitIdx(CurTabIdx), "CIC6"
                        End If
                    End If
                Else
                    strTotalBuffer = strTotalBuffer + newRecDat + vbLf
                End If
                LinCnt = LinCnt + 1
            Next i
        End If
        If UpdateOnReload = True Then
            DataTab(CurTabIdx).ColSelectionRemoveAll
            DeleteOldTabLines CurTabIdx, MyPageFieldCount(CurTabIdx) + 2, "OLD"
            DataTab(CurTabIdx).Sort MyPageFieldCount(CurTabIdx) + 1, True, True
        Else
            DataTab(CurTabIdx).ResetContent
            Records = DataTab(CurTabIdx).InsertBuffer(strTotalBuffer, vbLf)
        End If
        CheckLogoColumn CurTabIdx
        tmpUrno = ""
        CurUrno = ""
        DarkLine = False
        GetColorValues MyTabLineColors(CurTabIdx)
        LineTextColor = MyLineTextColor(CurTabIdx)
        LineCount = DataTab(CurTabIdx).GetLineCount - 1
        For i = 0 To LineCount
            If MyUrnoIdx(CurTabIdx) >= 0 Then
                tmpUrno = DataTab(CurTabIdx).GetColumnValue(i, MyUrnoIdx(CurTabIdx))
                tmpUrno = GetItem(tmpUrno, 1, "_")
                If tmpUrno <> CurUrno Then DarkLine = Not DarkLine
                If DarkLine Then
                  DataTab(CurTabIdx).SetLineColor i, LineTextColor, DarkLineColor
                  DataTab(CurTabIdx).SetLineTag i, CStr(DarkLineColor)
                Else
                  DataTab(CurTabIdx).SetLineColor i, LineTextColor, LightLineColor
                  DataTab(CurTabIdx).SetLineTag i, CStr(LightLineColor)
                End If
                CurUrno = tmpUrno
            Else
                If (i Mod 2) = 0 Then
                  DataTab(CurTabIdx).SetLineColor i, LineTextColor, DarkLineColor
                  DataTab(CurTabIdx).SetLineTag i, CStr(DarkLineColor)
                Else
                  DataTab(CurTabIdx).SetLineColor i, LineTextColor, LightLineColor
                  DataTab(CurTabIdx).SetLineTag i, CStr(LightLineColor)
                End If
            End If
        Next i
        DataTab(CurTabIdx).OnVScrollTo currScrollPos(CurTabIdx)
        MainTitle.Caption = tmpSave
    End If
    
    AppendLines CurTabIdx
    CloseConnection False
    If MyAutoSizeCols(CurTabIdx) = True Then DataTab(CurTabIdx).AutoSizeColumns
    TabLookUp(CurTabIdx).HeaderLengthString = DataTab(CurTabIdx).HeaderLengthString
    TabLookUp(CurTabIdx).ColumnAlignmentString = DataTab(CurTabIdx).ColumnAlignmentString
    
    If (CedaWasConnected = True) Or (WorkOffLine) Then
        SetServerPanel "READY"
    Else
        SetServerPanel "NOSERVER"
    End If
    UnitStatus(4).Caption = MyHostName
    LostBroadCast(CurTabIdx) = False
    RefreshCountDown(CurTabIdx) = False
    If MinCountDown(CurTabIdx) < 0 Then MinCountDown(CurTabIdx) = RefreshCyclus
    DataTab(CurTabIdx).RedrawTab
    tmrMinutes.Interval = 60000
    tmrSeconds.Interval = 1000
    Screen.MousePointer = 0
    If ApplIsInKioskMode Then SysFunc.HideMyMouse
    If (CurTabIdx = 1) And (PendingOpenFavorite >= 0) Then
        i = PendingOpenFavorite
        HandleOpenFavorite "LOOK", i
    End If
End Sub

Private Sub AdvText_KeyPress(Index As Integer, KeyAscii As Integer)
    KeyCodeInput KeyAscii, 0, 4, "Form.KeyPress"
End Sub

Private Sub btnCdiServer_Click()
    If btnCdiServer.Value = 1 Then
        SetFormOnTop Me, False
        CdiList.Show , Me
        SetFormOnTop Me, True
    Else
        Unload CdiList
    End If
End Sub

Private Sub btnExit_Click()
    Dim RetVal As Integer
    RetVal = 1
    If AskUser Then
        UserAnswer = "TIMER,KEYPAD"
        RetVal = MyMsgBox.CallAskUser(0, 0, 0, "Application Control", "Do you want to exit ?", "ask", "1: Yes,0: No", UserAnswer)
    End If
    If RetVal = 1 Then
        If ApplIsInKioskMode Then SysFunc.ShowMyMouse
        Screen.MousePointer = 11
        ShutDownRequested = True
        RemotePort_Close 0
        If MyHostName <> "LOCAL" Then
            tmrSeconds.Enabled = False
            tmrMinutes.Enabled = False
            BcSpoolTimer.Enabled = False
            BcPortConnected = False
            If Not RemoteControl Then
                'MsgLogging UCase(App.EXEName) & " LOGOFF FROM CEDA"
                MainTitle.Caption = "LOGOFF"
                OpenConnection True
                'If ServerIsAvailable = True Then
                    PrepareMessage "LOGOFF"
                    If CedaIsConnected = True Then
                        RetVal = Ufis.CallServer("SBC", "STAFF/CLOSE", "ALL FIELDS", MyTcpIpAdr, HexTcpAdr, "")
                    End If
                'End If
                CloseConnection True
                'MsgLogging UCase(App.EXEName) & " DISCONNECTED"
                'MsgLogging UCase(App.EXEName) & " EXIT"
            End If
        End If
        If ApplIsInKioskMode Then
            'SysFunc.ShowMyMouse
            'SysFunc.ShowMyTaskBar
            'SysFunc.ShowMyDesktop
            SysFunc.HideMyMouse
            SysFunc.HideMyTaskBar
            SysFunc.HideMyDesktop
        End If
        Screen.MousePointer = 0
        WinShut.SetExitMode "TERMINATE"
    Else
        chkFunc.Value = 0
        Me.SetFocus
    End If
End Sub

Private Sub chkIcon_Click()
    Dim iCmd As Integer
    If chkIcon.Value = 1 Then
        iCmd = 1
        Select Case iCmd
            Case 0
            Case 1
                ' GFO Added
                If Show_MyCFG = "YES" Then
                    MyCfg.Show , Me
                    MyCfg.chkAny(2).Value = 1
                    MyCfg.ChkType(26).Value = 1
                End If
            Case 2
                SetFormOnTop Me, False
                SysFunc.Show , Me
            Case 3
                SavePicture Me.Icon, "StfPage.ico"
            Case Else
        End Select
        chkIcon.Value = 0
    End If
End Sub

Private Sub QueryLoadData(ByVal UseButton As String, LoadData As Boolean)
    If Not RemoteControl Then
        'If GotUtcDiff = False Then
        '    UTC_Diff = GetUTCDifference()
        'End If
        If TimesInLocal = True Then lblNow.Value = 1
        If (PageIsValid(0)) And ((UseButton = "LEFT") Or (UseButton = "BOTH")) Then
            ButtonPushedInternal = True
            ReloadData(0) = LoadData
            chkLeftAction.Value = 0
            chkLeftAction.Value = 1
            ButtonPushedInternal = False
        End If
        If (PageIsValid(1)) And ((UseButton = "RIGHT") Or (UseButton = "BOTH")) Then
            ButtonPushedInternal = True
            ReloadData(1) = LoadData
            chkRightAction.Value = 0
            chkRightAction.Value = 1
            ButtonPushedInternal = False
        End If
    End If
    If Not ApplicationIsStarted Then
        If RemoteControl Then
            RefreshCountDown(0) = False
            RefreshCountDown(1) = False
            SetFormOnTop Me, False
            ShowRemote.Show , Me
            SetFormOnTop Me, True
            tmrSeconds.Interval = 0
            tmrMinutes.Interval = 0
            lblRefresh(1).Caption = "00:00:00"
            lblRefresh(0).Caption = "REMOTE"
            lblNow.Caption = "00.00.00 / 00:00:00"
            lblUTC.Caption = "00:00:00"
        End If
        ApplicationIsStarted = True
    End If
    If UseButton = "LEFT" Or (UseButton = "BOTH") Then
        ReloadData(0) = False
    End If
    If UseButton = "RIGHT" Or (UseButton = "BOTH") Then
        ReloadData(1) = False
    End If
End Sub


Public Function AddInfoMessage(ForWhat As String, UseRow As Integer, TypIdx As Integer, MsgText As String, MsgUrno As String, RefUrno As String, MsgData As String) As Integer
    Dim tmpText As String
    Dim OldTag As String
    Dim NewTag As String
    Dim NewLeft As Long
    Dim MsgIdx As Integer
    Dim RowIdx As Integer
    Dim NewMsg As Boolean
    Dim CheckIt As Boolean
    Dim IsInit As Boolean
    Dim MsgIniRec As MsgLayout
    RowIdx = UseRow
    CheckIt = True
    Select Case ForWhat
        Case "IRT"
            MsgIdx = GetFreeMsgLabel(0)
            NewMsg = True
            IsInit = False
        Case "INIT"
            MsgIdx = GetFreeMsgLabel(0)
            NewMsg = True
            IsInit = True
        Case "URT"
            MsgIdx = GetMsgIdxByUrno(0, MsgUrno)
            NewMsg = False
        Case "DRT"
            MsgIdx = GetMsgIdxByUrno(0, MsgUrno)
            If MsgIdx > 0 Then
                RowIdx = MsgInfoArray(MsgIdx).MsgSysRow
                MsgInfoArray(MsgIdx).MsgSysInUse = False
                ArrangeMsgRow RowIdx
            End If
            CheckIt = False
        Case Else
    End Select
    If (CheckIt) And (MsgIdx > 0) Then
        Set TopLabel(MsgIdx).Container = LinePanel(RowIdx)
        Set TopSep(MsgIdx).Container = LinePanel(RowIdx)
        MsgInfoArray(MsgIdx).MsgTypeIdx = TypIdx
        MsgInfoArray(MsgIdx) = MsgTypeArray(TypIdx)
        MsgInfoArray(MsgIdx).MsgSysRow = RowIdx
        MsgInfoArray(MsgIdx).MsgSysMsgUrno = MsgUrno
        MsgInfoArray(MsgIdx).MsgSysRefUrno = RefUrno
        
        If NewMsg Then MsgSysIdCount = MsgSysIdCount + 1
        MsgInfoArray(MsgIdx).MsgSysInUse = True
        MsgInfoArray(MsgIdx).MsgSysCount = MsgSysIdCount
        If NewMsg Then
            If (Not IsInit) And (MsgInfoArray(MsgIdx).MsgAniTimer.TotalSec > 0) Then
                MsgIniRec = MsgInfoArray(MsgIdx).MsgLblAni1
                MsgInfoArray(MsgIdx).MsgAniStatus = 1
            Else
                MsgIniRec = MsgInfoArray(MsgIdx).MsgLblNorm
                MsgInfoArray(MsgIdx).MsgAniStatus = 0
            End If
        End If
        tmpText = MsgText
        If MsgIniRec.LblTextTrim = True Then
            tmpText = Trim(tmpText)
        Else
            tmpText = " " & Trim(tmpText) & " "
        End If
        If MsgIniRec.LblTextUpper = True Then
            tmpText = UCase(tmpText)
            TopLabel(MsgIdx).Top = -15
        Else
            TopLabel(MsgIdx).Top = 0
        End If
        TopLabel(MsgIdx).Caption = tmpText
        MsgInfoArray(MsgIdx).MsgDspText = tmpText
        MsgInfoArray(MsgIdx).MsgDspAddi = MsgData
        TopLabel(MsgIdx).BorderStyle = MsgIniRec.LblBorderStyle
        TopLabel(MsgIdx).BackStyle = MsgIniRec.LblBackStyle
        TopLabel(MsgIdx).ForeColor = MsgIniRec.LblForeColor
        TopLabel(MsgIdx).BackColor = MsgIniRec.LblBackColor
        If NewMsg Then
            NewLeft = 0
            TopLabel(MsgIdx).Left = NewLeft
            NewLeft = NewLeft + TopLabel(MsgIdx).Width
            TopSep(MsgIdx).Left = NewLeft
            TopLabel(MsgIdx).Visible = True
            TopSep(MsgIdx).Visible = True
            OldTag = LinePanel(RowIdx).Tag
            NewTag = CStr(MsgIdx) & "," & OldTag
            LinePanel(RowIdx).Tag = NewTag
        End If
        ArrangeMsgRow RowIdx
        If MsgInfoArray(MsgIdx).MsgAniStatus > 0 Then
            MsgInfoArray(MsgIdx).MsgNrmTimer.CountSec = MsgInfoArray(MsgIdx).MsgNrmTimer.TotalSec
            MsgInfoArray(MsgIdx).MsgAniTimer.CountSec = MsgInfoArray(MsgIdx).MsgAniTimer.TotalSec
            MsgSysBlinkCnt = MsgSysBlinkCnt + 1
            MsgSysBlinkLst(MsgSysBlinkCnt) = MsgIdx
            tmrBlink.Enabled = True
        End If
    End If
    AddInfoMessage = MsgIdx
End Function

Private Sub chkAlive_Click()
    If chkAlive.Value = 1 Then
        MyAboutBox.SetLifeStyle = True
        MyAboutBox.VersionTab.LifeStyle = True
        MyAboutBox.Show , Me
        MyAboutBox.VersionTab.InsertTextLine "UfisBroadcasts.ocx,1.0.0.2,Nov 25 2001 - 14:56:22,N.A.", False
        MyAboutBox.VersionTab.AutoSizeColumns
        chkAlive.Value = 0
    End If
End Sub

Private Sub chkFunc_Click()
    If chkFunc.Value = 1 Then
        btnExit_Click
    Else
    End If
End Sub

Private Sub chkHide_Click()
    If chkHide.Value = 1 Then
        Hook Me.hwnd   ' Set up our handler
        AddIconToTray Me.hwnd, Me.Icon, Me.Icon.Handle, App.EXEName + " / " + MainTitle.Caption
        Me.Hide
        chkHide.Value = 0
    End If
End Sub

Private Sub chkLeftAction_Click()
    On Error Resume Next
    If chkLeftAction.Value = 1 Then
        If PageIsValid(0) = True Then
            chkRightAction.Value = 0
            TabIdx = 0
            omDisplayMode = "LEFT"
            If (ReloadData(0) = False) Or (RefreshMode = True) Then
                PageTitle.Caption = PageMainTitle
                InfoTitle(0).Caption = InfoCaption(0)
                DataTab(0).Visible = True
                PagePanel(0).Visible = True
                If (Not ShowBothGrids) Or (Not ButtonPushedInternal) Then
                    PageTitle.Caption = PageCaption(0)
                    DataTab(1).Visible = False
                    PagePanel(1).Visible = False
                    If Not ButtonPushedInternal Then
                        ShowBothGrids = False
                        Form_Resize
                    End If
                End If
            End If
            If (ReloadData(0) = True) Or (RefreshMode = True) Then
                CurLoadButton = "LEFT"
                QueryFlights 0
            End If
            ReloadData(0) = False
        Else
            RefreshCountDown(0) = False
        End If
        If DataTab(0).Visible Then DataTab(0).SetFocus
        chkLeftAction.Value = 0
        If PageFunction(0) = "INDEX" Then
            If DataTab(0).GetCurrentSelected < 0 Then DataTab(0).SetCurrentSelection 0
        End If
    End If
End Sub

Private Sub chkMind_Click(Index As Integer)
    If chkMind(Index).Value = 1 Then
        chkMind(Index).BackColor = LightYellow
        chkMind(Index).ForeColor = vbBlack
        PendingOpenFavorite = -1
        HandleOpenFavorite "OPEN", Index
    Else
        chkMind(Index).BackColor = vbButtonFace
        chkMind(Index).ForeColor = vbBlack
        PendingOpenFavorite = -1
    End If
End Sub
Private Sub HandleOpenFavorite(ForWhat As String, Index As Integer)
    Dim tmpData As String
    Dim tmpPage As String
    Dim tmpUrno As String
    Dim tmpFlno As String
    Dim NxtCmd As String
    Dim GotCmd As String
    Dim HitList As String
    Dim LineNo As Long
    Dim ColNo As Long
    Dim i As Integer
    If Index >= 0 Then
        tmpData = chkMind(Index).Tag
        tmpPage = GetItem(tmpData, 1, ",")
        tmpUrno = GetItem(tmpData, 2, ",")
        tmpFlno = GetItem(tmpData, 3, ",")
    End If
    GotCmd = ForWhat
    NxtCmd = ""
    While GotCmd <> ""
        Select Case GotCmd
            Case "OPEN"
                If tmpPage <> MyActivePageNumber Then
                    PendingOpenFavorite = Index
                    HandleOpenPage tmpPage
                    NxtCmd = ""
                Else
                    NxtCmd = "LOOK"
                End If
            Case "LOOK"
                ColNo = CLng(GetRealItemNo(DataTab(TabIdx).LogicalFieldList, "URNO"))
                If ColNo >= 0 Then
                    HitList = DataTab(TabIdx).GetLinesByColumnValue(ColNo, tmpUrno, 0)
                    If HitList <> "" Then
                        LineNo = Val(HitList)
                        DataTab(TabIdx).OnVScrollTo LineNo
                        DataTab(TabIdx).SetCurrentSelection LineNo
                    End If
                End If
                NxtCmd = ""
            Case Else
                If Index < 0 Then
                    For i = 0 To chkMind.UBound
                        If chkMind(i).Tag <> "" Then
                            If InStr(chkMind(i).Tag, ForWhat) > 0 Then
                                chkMind(i).BackColor = vbRed
                                chkMind(i).ForeColor = vbWhite
                            End If
                        End If
                    Next
                End If
                NxtCmd = ""
        End Select
        GotCmd = NxtCmd
    Wend
End Sub
Private Sub chkRightAction_Click()
    Dim tmpVal As Integer
    On Error Resume Next
    If chkRightAction.Value = 1 Then
        If PageIsValid(1) = True Then
            chkLeftAction.Value = 0
            TabIdx = 1
            omDisplayMode = "RIGHT"
            If (ReloadData(1) = False) Or (RefreshMode = True) Then
                PageTitle.Caption = PageMainTitle
                InfoTitle(1).Caption = InfoCaption(1)
                DataTab(1).Visible = True
                PagePanel(1).Visible = True
                If (Not ShowBothGrids) Or (Not ButtonPushedInternal) Then
                    PageTitle.Caption = PageCaption(1)
                    DataTab(0).Visible = False
                    PagePanel(0).Visible = False
                    If Not ButtonPushedInternal Then
                        ShowBothGrids = False
                        Form_Resize
                    End If
                End If
            End If
            If (ReloadData(1) = True) Or (RefreshMode = True) Then
                CurLoadButton = "RIGHT"
                tmpVal = Val(MyActivePageNumber)
                If tmpVal > 10 Then
                    QueryFlights 1
                    If tmpVal = 9998 Then
                        AdvMsgText(1).Caption = MyBdpsSecRema
                        AdvIcon(0).Left = PagePanel(0).Left + InfoTitle(0).Left
                        AdvIcon(1).Left = PagePanel(1).Left + InfoTitle(1).Left - (AdvIcon(1).Width * 2)
                        AdvMsgText(0).Left = AdvIcon(1).Left + AdvIcon(1).Width + 120
                        AdvMsgText(1).Left = AdvMsgText(0).Left
                        AdvIcon(0).Visible = True
                        AdvIcon(1).Visible = True
                        AdvMsgText(0).Visible = True
                        AdvMsgText(1).Visible = True
                    End If
                Else
                    'InitGroupPages 1
                End If
            End If
            ReloadData(1) = False
        Else
            RefreshCountDown(1) = False
        End If
        If DataTab(1).Visible Then DataTab(1).SetFocus
        chkRightAction.Value = 0
        If PageFunction(1) = "INDEX" Then
            If DataTab(1).GetCurrentSelected < 0 Then DataTab(1).SetCurrentSelection 0
        End If
    End If
End Sub

Private Sub btnNext_Click()
    Dim diff As Integer
    Dim tmpUrno As String
    Dim CurFunc As String
    Dim LineNo As Long
    Dim MaxLine As Long
    Dim IdxMin As Integer
    Dim IdxMax As Integer
    Dim UseTabidx As Integer
    IdxMin = 0
    IdxMax = 1
    For UseTabidx = IdxMin To IdxMax
        Select Case PageCursor(UseTabidx)
            Case "INDEX"
                CurFunc = "INDEX"
            'Case "PAGE_INDEX"
            '    CurFunc = "INDEX"
            Case Else
                CurFunc = "PAGE"
        End Select
        If CurFunc = "INDEX" Then
            MaxLine = DataTab(UseTabidx).GetLineCount - 1
            LineNo = DataTab(UseTabidx).GetCurrentSelected
            If LineNo < MaxLine Then
                LineNo = LineNo + 1
                DataTab(UseTabidx).SetCurrentSelection LineNo
                If LineNo >= (DataTab(UseTabidx).GetVScrollPos + ListLines(UseTabidx)) Then
                    currScrollPos(UseTabidx) = DataTab(UseTabidx).GetVScrollPos + 1
                    DataTab(UseTabidx).OnVScrollTo currScrollPos(UseTabidx)
                End If
            End If
        ElseIf CurFunc = "PAGE" Then
            If (currScrollPos(UseTabidx) + ListLines(UseTabidx)) < DataTab(UseTabidx).GetLineCount - ListLines(UseTabidx) Then
                currScrollPos(UseTabidx) = currScrollPos(UseTabidx) + ListLines(UseTabidx)
            Else
                diff = ((DataTab(UseTabidx).GetLineCount - ListLines(UseTabidx)) - currScrollPos(UseTabidx))
                currScrollPos(UseTabidx) = currScrollPos(UseTabidx) + diff
            End If
            'tmpUrno = DataTab(UseTabidx).GetColumnValue(currScrollPos(UseTabidx), MyUrnoIdx(UseTabidx))
            'While InStr(tmpUrno, "_") > 0
            '    currScrollPos(UseTabidx) = currScrollPos(UseTabidx) - 1
            '    tmpUrno = DataTab(UseTabidx).GetColumnValue(currScrollPos(UseTabidx), MyUrnoIdx(UseTabidx))
            'Wend
            DataTab(UseTabidx).OnVScrollTo currScrollPos(UseTabidx)
        End If
    Next
End Sub

Private Sub btnPrev_Click()
    Dim tmpUrno As String
    Dim CurFunc As String
    Dim LineNo As Long
    Dim IdxMin As Integer
    Dim IdxMax As Integer
    Dim UseTabidx As Integer
    IdxMin = 0
    IdxMax = 1
    For UseTabidx = IdxMin To IdxMax
        Select Case PageCursor(UseTabidx)
            Case "INDEX"
                CurFunc = "INDEX"
            'Case "PAGE_INDEX"
            '    CurFunc = "INDEX"
            Case Else
                CurFunc = "PAGE"
        End Select
        If CurFunc = "INDEX" Then
            'MaxLine = DataTab(UseTabidx).GetLineCount - 1
            LineNo = DataTab(UseTabidx).GetCurrentSelected
            If LineNo > 0 Then
                LineNo = LineNo - 1
                DataTab(UseTabidx).SetCurrentSelection LineNo
                If LineNo < DataTab(UseTabidx).GetVScrollPos Then
                    currScrollPos(UseTabidx) = DataTab(UseTabidx).GetVScrollPos - 1
                    DataTab(UseTabidx).OnVScrollTo currScrollPos(UseTabidx)
                End If
            End If
        ElseIf CurFunc = "PAGE" Then
            If currScrollPos(UseTabidx) >= 0 Then
                currScrollPos(UseTabidx) = currScrollPos(UseTabidx) - ListLines(UseTabidx)
            End If
            If currScrollPos(UseTabidx) < 0 Then
                currScrollPos(UseTabidx) = 0
            End If
            'If currScrollPos(UseTabidx) > 0 Then
                'LineNo = currScrollPos(UseTabidx) + ListLines(UseTabidx)
                'tmpUrno = DataTab(UseTabidx).GetColumnValue(LineNo, MyUrnoIdx(UseTabidx))
                'While InStr(tmpUrno, "_") > 0
                '    currScrollPos(UseTabidx) = currScrollPos(UseTabidx) + 1
                '    LineNo = currScrollPos(UseTabidx) + ListLines(UseTabidx)
                '    tmpUrno = DataTab(UseTabidx).GetColumnValue(LineNo, MyUrnoIdx(UseTabidx))
                'Wend
            'End If
            DataTab(UseTabidx).OnVScrollTo currScrollPos(UseTabidx)
        End If
    Next
End Sub


Private Sub AppendLines(CurTabIdx As Integer)
    Dim AddLinCnt As Long
    Dim CurLinCnt As Long
    Dim AddLin As Long
    Dim NewLine As String
        GetColorValues MyTabLineColors(CurTabIdx)
        LineTextColor = MyLineTextColor(CurTabIdx)
    CurLinCnt = DataTab(CurTabIdx).GetLineCount
    AddLinCnt = ListLines(CurTabIdx) - CurLinCnt
    If AddLinCnt > 0 Then
        NewLine = EmptyLine(CurTabIdx) + ",,,,"
        CurLinCnt = CurLinCnt - 1
        For AddLin = 1 To AddLinCnt
            DataTab(CurTabIdx).InsertTextLine NewLine, False
            CurLinCnt = CurLinCnt + 1
            If (CurLinCnt Mod 2) = 0 Then
                DataTab(CurTabIdx).SetLineColor CurLinCnt, LineTextColor, DarkLineColor
                DataTab(CurTabIdx).SetLineTag CurLinCnt, CStr(DarkLineColor)
            Else
                DataTab(CurTabIdx).SetLineColor CurLinCnt, LineTextColor, LightLineColor
                DataTab(CurTabIdx).SetLineTag CurLinCnt, CStr(LightLineColor)
            End If
        Next
        DataTab(CurTabIdx).RedrawTab
    End If
End Sub

Private Sub chkTool_Click(Index As Integer)
    Dim tmpStr As String
    Dim idx As Integer
    On Error Resume Next
    If chkTool(Index).Value = 1 Then
        chkTool(Index).BackColor = LightGreen
        Select Case Index
            Case 0
            Case 1
                chkTool(4).Visible = True
                chkTool(4).Value = 1
                chkTool(Index).Value = 0
            Case 2
                Form_Resize
            Case 3
                QueryFlights 0
                QueryFlights 1
                chkTool(Index).Value = 0
            Case 4
                JumpF3Key
                chkTool(Index).Value = 0
            Case 5
                GetFilterDialog "AODB"
                chkTool(Index).Value = 0
            Case 6
                ResetFilters
                chkTool(Index).Value = 0
            Case 7
                GetFilterDialog "TEXT1"
                chkTool(Index).Value = 0
            Case 8
                GetFilterDialog "TEXT2"
                chkTool(Index).Value = 0
            Case 9
                ArrangeFilterPanel 1
            Case 14
                AddFlightToFavorList "REM"
                chkTool(Index).Value = 0
            Case 15
                AddFlightToFavorList "ADD"
                chkTool(Index).Value = 0
            Case 16, 18
                If TopInfoPanel(9).Visible Then
                    TopInfoPanel(9).Visible = False
                    TopInfoPanel(11).Visible = True
                    TopInfoPanel(10).Tag = ""
                Else
                    If TopInfoPanel(10).Tag <> "" Then
                        idx = Val(TopInfoPanel(10).Tag)
                        If idx <> Index Then chkTool(idx).Value = 1
                    End If
                    TopInfoPanel(9).Visible = True
                    TopInfoPanel(11).Visible = False
                    TopInfoPanel(10).Tag = CStr(Index)
                End If
                Form_Resize
                chkTool(Index).Value = 0
            Case 17, 19
                If chkTool(9).Value = 1 Then
                    chkTool(9).Value = 0
                Else
                    chkTool(9).Value = 1
                End If
                chkTool(Index).Value = 0
            Case 20
                If TopInfoPanel(14).Visible Then
                    TopInfoPanel(14).Visible = False
                Else
                    TopInfoPanel(14).Visible = True
                    TopInfoPanel(14).ZOrder
                    TopInfoPanel(13).ZOrder
                    MsgTplTab(0).SetCurrentSelection 0
                    MsgTplTab(0).SetFocus
                End If
                Form_Resize
                chkTool(Index).Value = 0
            Case 21
                ArrangeFilterPanel 0
            Case 22, 23
                If TopInfoPanel(12).Visible Then
                    CloseConnection True
                    TopInfoPanel(12).Visible = False
                    TopInfoPanel(13).Visible = False
                    TopInfoPanel(14).Visible = False
                    If optArea(0).Tag <> "" Then
                        idx = Val(optArea(0).Tag)
                        optArea(idx).BackColor = vbButtonFace
                        chkTopLine(idx).BackColor = vbButtonFace
                    End If
                    TopInfoPanel(10).Tag = ""
                Else
                    If TopInfoPanel(10).Tag <> "" Then
                        idx = Val(TopInfoPanel(10).Tag)
                        If idx <> Index Then chkTool(idx).Value = 1
                    End If
                    TopInfoPanel(12).Visible = True
                    TopInfoPanel(12).ZOrder
                    OpenConnection True
                    TopInfoPanel(13).Visible = True
                    TopInfoPanel(13).ZOrder
                    txtMsgText.SetFocus
                    If optArea(0).Tag <> "" Then
                        idx = Val(optArea(0).Tag)
                        optArea(idx).BackColor = LightGreen
                        chkTopLine(idx).BackColor = LightYellow
                    End If
                    chkTool(20).Value = 1
                    txtMsgText.SetFocus
                    TopInfoPanel(10).Tag = CStr(Index)
                End If
                Form_Resize
                chkTool(Index).Value = 0
            Case 24
                SubmitInfoMessage "INSERT"
                chkTool(Index).Value = 0
            Case 25
                SubmitInfoMessage "UPDATE"
                chkTool(Index).Value = 0
            Case 26
                SubmitInfoMessage "DELETE"
                chkTool(Index).Value = 0
            Case 27
                OpenConnection False
                InitInfoMessages
                CloseConnection False
                If txtMsgText.Visible Then txtMsgText.SetFocus Else Me.SetFocus
                chkTool(Index).Value = 0
            Case Else
                chkTool(Index).Value = 0
        End Select
    Else
        chkTool(Index).BackColor = vbButtonFace
        Select Case Index
            Case 1
                chkTool(4).Visible = False
            Case 2
                Form_Resize
            Case 3
            Case 9
                ArrangeFilterPanel 1
            Case 21
                ArrangeFilterPanel 0
            Case Else
        End Select
    End If
End Sub
Private Sub ArrangeFilterPanel(Index As Integer)
    Dim ShowPanel As Boolean
    ShowPanel = False
    If chkTool(21).Value = 1 Then
        ShowPanel = True
        chkTool(21).Picture = MyIcons(8).Picture
    Else
        chkTool(21).Picture = MyIcons(7).Picture
    End If
    If chkTool(9).Value = 1 Then
        ShowPanel = True
        chkTool(9).Picture = MyIcons(8).Picture
    Else
        chkTool(9).Picture = MyIcons(7).Picture
    End If
    If ShowPanel Then
        TopInfoPanel(6).Visible = True
        ArrangeLookUpGrids Index, "SHOW"
        chkTool(0).Value = 1
    Else
        TopInfoPanel(6).Visible = False
        ArrangeLookUpGrids Index, "HIDE"
        chkTool(0).Value = 0
    End If
    Form_Resize
End Sub
Private Sub ArrangeLookUpGrids(Index As Integer, ForWhat As String)
    Dim tmpStr As String
    Dim idx As Integer
    idx = Index
    Select Case ForWhat
        Case "SHOW"
            For idx = 0 To 1
                TabLookUp(idx).HeaderLengthString = DataTab(idx).HeaderLengthString
                TabLookUp(idx).ColumnAlignmentString = DataTab(idx).ColumnAlignmentString
                TabLookUp(idx).InsertTextLine CreateEmptyLine(TabLookUp(Index).LogicalFieldList), False
                TabLookUp(idx).SetCurrentSelection 0
                TabLookUp(idx).Visible = True
                tmpStr = TabLookUp(idx).GetLineValues(0)
                TabLookUp(idx).ColumnAlignmentString = Replace(tmpStr, ",", "C,", 1, -1, vbBinaryCompare) & "C"
                TabLookUp(idx).GridLineColor = vbBlack
                SetLookUpKeyValues idx
                GetLookUpKeyValues idx
            Next
            'TabEditLookUpIndex = Index
        Case "HIDE"
            For idx = 0 To 1
                TabLookUp(idx).Visible = False
            Next
        Case "SIZE"
            For idx = 0 To 1
                If TopInfoPanel(6).Visible Then TabLookUp(idx).Visible = DataTab(idx).Visible
                TabLookUp(idx).Left = DataTab(idx).Left
                TabLookUp(idx).Width = DataTab(idx).Width
            Next
        Case Else
    End Select
End Sub
Private Sub SubmitInfoMessage(ForWhat As String)
    Dim SqlCmd As String
    Dim SqlTbl As String
    Dim SqlKey As String
    Dim SqlFld As String
    Dim SqlDat As String
    Dim tmpTxt As String
    Dim idx As Integer
    Select Case ForWhat
        Case "INSERT"
            tmpTxt = Trim(txtMsgText.Text)
            If tmpTxt <> "" Then
                If optType(0).Tag <> "" Then
                    idx = Val(optType(0).Tag)
                    tmpTxt = tmpTxt & "|" & CStr(idx)
                End If
                If optArea(0).Tag <> "" Then
                    idx = Val(optArea(0).Tag)
                    SqlCmd = "IBT"
                    SqlTbl = "INFTAB"
                    SqlFld = "APC3,APPL,FUNK,AREA,TEXT,CDAT,LSTU,USEC,USEU,HOPO"
                    SqlDat = ""
                    SqlDat = SqlDat & "MSG" & ","
                    SqlDat = SqlDat & "STAFFMONITOR" & ","
                    SqlDat = SqlDat & "USERINPUT" & ","
                    SqlDat = SqlDat & "AREA" & CStr(idx + 1) & ","
                    SqlDat = SqlDat & tmpTxt & ","
                    SqlDat = SqlDat & WksLocTime & ","
                    SqlDat = SqlDat & WksLocTime & ","
                    SqlDat = SqlDat & "VDU-INS" & ","
                    SqlDat = SqlDat & "VDU-INS" & ","
                    SqlDat = SqlDat & HomeAirport
                    SqlKey = "          "
                    If Not WorkOffLine Then
                        'OpenConnection True
                        Ufis.CallServer SqlCmd, SqlTbl, SqlFld, SqlDat, SqlKey, "120"
                        'CloseConnection True
                    End If
                End If
            End If
        Case "UPDATE"
            tmpTxt = Trim(txtMsgText.Text)
            If tmpTxt <> "" Then
                If optType(0).Tag <> "" Then
                    idx = Val(optType(0).Tag)
                    tmpTxt = tmpTxt & "|" & CStr(idx)
                End If
                If optArea(0).Tag <> "" Then
                    idx = Val(optArea(0).Tag)
                    SqlCmd = "URT"
                    SqlTbl = "INFTAB"
                    SqlFld = "APC3,APPL,FUNK,AREA,TEXT,CDAT,LSTU,USEC,USEU,URNO,HOPO"
                    SqlDat = ""
                    SqlDat = SqlDat & "MSG" & ","
                    SqlDat = SqlDat & "STAFFMONITOR" & ","
                    SqlDat = SqlDat & "USERINPUT" & ","
                    SqlDat = SqlDat & "AREA" & CStr(idx + 1) & ","
                    SqlDat = SqlDat & tmpTxt & ","
                    SqlDat = SqlDat & WksLocTime & ","
                    SqlDat = SqlDat & WksLocTime & ","
                    SqlDat = SqlDat & "VDU-UPD" & ","
                    SqlDat = SqlDat & "VDU-UPD" & ","
                    SqlDat = SqlDat & chkTool(25).Tag & ","
                    SqlDat = SqlDat & HomeAirport
                    SqlKey = "WHERE URNO='" & chkTool(25).Tag & "'"
                    If Not WorkOffLine Then
                        'OpenConnection True
                        Ufis.CallServer SqlCmd, SqlTbl, SqlFld, SqlDat, SqlKey, "120"
                        'CloseConnection True
                    End If
                    chkTool(25).Tag = ""
                    chkTool(26).Tag = ""
                    chkTool(25).Enabled = False
                    chkTool(26).Enabled = False
                    chkTool(24).Enabled = True
                End If
            End If
        Case "DELETE"
            tmpTxt = Trim(txtMsgText.Text)
            If tmpTxt <> "" Then
                If optType(0).Tag <> "" Then
                    idx = Val(optType(0).Tag)
                    tmpTxt = tmpTxt & "|" & CStr(idx)
                End If
                If optArea(0).Tag <> "" Then
                    idx = Val(optArea(0).Tag)
                    SqlCmd = "DRT"
                    SqlTbl = "INFTAB"
                    SqlFld = "APC3,APPL,FUNK,AREA,TEXT,CDAT,LSTU,USEC,USEU,URNO,HOPO"
                    SqlDat = ""
                    SqlDat = SqlDat & "MSG" & ","
                    SqlDat = SqlDat & "STAFFMONITOR" & ","
                    SqlDat = SqlDat & "USERINPUT" & ","
                    SqlDat = SqlDat & "AREA" & CStr(idx + 1) & ","
                    SqlDat = SqlDat & tmpTxt & ","
                    SqlDat = SqlDat & WksLocTime & ","
                    SqlDat = SqlDat & WksLocTime & ","
                    SqlDat = SqlDat & "VDU-DEL" & ","
                    SqlDat = SqlDat & "VDU-DEL" & ","
                    SqlDat = SqlDat & chkTool(26).Tag & ","
                    SqlDat = SqlDat & HomeAirport
                    SqlKey = "WHERE URNO='" & chkTool(26).Tag & "'"
                    If Not WorkOffLine Then
                        'OpenConnection True
                        Ufis.CallServer SqlCmd, SqlTbl, SqlFld, SqlDat, SqlKey, "120"
                        'CloseConnection True
                    End If
                    chkTool(25).Tag = ""
                    chkTool(26).Tag = ""
                    chkTool(25).Enabled = False
                    chkTool(26).Enabled = False
                    chkTool(24).Enabled = True
                End If
            End If
        Case Else
    End Select
End Sub
Private Sub AddFlightToFavorList(ForWhat As String)
    Dim tmpFlno As String
    Dim tmpData As String
    Dim tmpUrno As String
    Dim tmpPage As String
    Dim NewLeft As Long
    Dim PosOffs As Long
    Dim n As Integer
    Dim i As Integer
    Dim idx As Integer
    Dim maxM As Long
    Dim m As Long
    Dim LineNo As Long
    Dim CheckRem As Boolean
    Dim FoundSome As Boolean
    Select Case ForWhat
        Case "ADD"
            idx = TabIdx
            LineNo = DataTab(idx).GetCurrentSelected
            If LineNo >= 0 Then
                m = 0
                n = -1
                For i = 0 To chkMind.UBound
                    If chkMind(i).Tag = "" Then
                        If n < 0 Then n = i
                    Else
                        m = m + 1
                    End If
                Next
                m = m + 1
                NewLeft = chkTool(13).Left + chkTool(13).Width
                PosOffs = (chkTool(14).Left - NewLeft)
                maxM = PosOffs / (chkMind(0).Width + 15)
                If maxM >= m Then
                    PosOffs = PosOffs - ((chkMind(0).Width + 15) * m)
                    PosOffs = PosOffs / 2
                    If PosOffs < 15 Then PosOffs = 15
                    If n < 0 Then n = chkMind.UBound + 1
                    If n > chkMind.UBound Then
                        Load chkMind(n)
                    End If
                    tmpFlno = DataTab(idx).GetFieldValue(LineNo, "FLNO")
                    tmpUrno = DataTab(idx).GetFieldValue(LineNo, "URNO")
                    tmpPage = MyActivePageNumber
                    tmpData = ""
                    tmpData = tmpData & tmpPage & ","
                    tmpData = tmpData & tmpUrno & ","
                    tmpData = tmpData & tmpFlno & ","
                    chkMind(n).Tag = tmpData
                    chkMind(n).Caption = tmpFlno
                    NewLeft = NewLeft + PosOffs
                    For i = 0 To chkMind.UBound
                        If chkMind(i).Tag <> "" Then
                            chkMind(i).Left = NewLeft
                            chkMind(i).Visible = True
                            NewLeft = NewLeft + chkMind(i).Width + 15
                        End If
                    Next
                    If maxM = m Then
                        chkTool(15).Enabled = False
                    Else
                        chkTool(15).Enabled = True
                    End If
                Else
                    chkTool(15).Enabled = False
                End If
                If m > 0 Then
                    chkTool(14).Enabled = True
                End If
            End If
        Case "REM"
            FoundSome = False
            CheckRem = True
            While CheckRem = True
                CheckRem = False
                m = 0
                maxM = 0
                n = -1
                For i = 0 To chkMind.UBound
                    If chkMind(i).Value = 1 Then
                        If n < 0 Then n = i
                        CheckRem = True
                        FoundSome = True
                    ElseIf chkMind(i).Tag <> "" Then
                        m = m + 1
                        maxM = i
                    End If
                Next
                If n < 0 Then
                    If FoundSome = False Then
                        n = maxM
                        m = m - 1
                        FoundSome = True
                    End If
                End If
                If n >= 0 Then
                    NewLeft = chkTool(13).Left + chkTool(13).Width
                    PosOffs = (chkTool(14).Left - NewLeft)
                    PosOffs = PosOffs - ((chkMind(0).Width + 15) * m)
                    PosOffs = PosOffs / 2
                    If PosOffs < 15 Then PosOffs = 15
                    chkMind(n).Tag = ""
                    chkMind(n).Caption = ""
                    chkMind(n).Visible = False
                    chkMind(n).Value = 0
                    NewLeft = NewLeft + PosOffs
                    For i = 0 To chkMind.UBound
                        If chkMind(i).Tag <> "" Then
                            chkMind(i).Left = NewLeft
                            chkMind(i).Visible = True
                            NewLeft = NewLeft + chkMind(i).Width + 15
                        End If
                    Next
                    chkTool(15).Enabled = True
                    If m = 0 Then
                        chkTool(14).Enabled = False
                    End If
                End If
            Wend
        Case Else
    End Select
End Sub

Private Sub ResetFilters()
    Dim i As Integer
    chkTool(7).Value = 0
    chkTool(8).Value = 0
    chkTool(2).Value = 0
    chkTool(1).Value = 0
    chkTool(0).Value = 0
    Unload FilterDialog
    For i = 0 To FilterText.UBound
        FilterText(i).Text = ""
    Next
    chkTool(0).Value = 1
    chkTool(3).Value = 1
End Sub
Private Sub GetFilterDialog(ForWhat As String)
    Dim FilterResult As String
    Dim PatchCodes As String
    Dim PatchValues As String
    Dim tmpTop As Long
    Select Case ForWhat
        Case "AODB"
            tmpTop = 330
            FilterResult = FilterDialog.InitMyLayout(Me, 0, tmpTop, True, "AODB_FILTER,LOAD")
            PatchCodes = GetRealItem(FilterResult, 0, Chr(16))
            PatchValues = GetRealItem(FilterResult, 1, Chr(16))
            FilterResult = TranslateFilter(PatchCodes, PatchValues)
            FilterText(2).Text = FilterResult
        Case "TEXT1"
            FilterResult = FilterText(2).Text
            FilterResult = MyTextEditor.EditThisText(FilterResult)
            FilterText(2).Text = FilterResult
        Case "TEXT2"
            FilterResult = FilterText(1).Text
            FilterResult = MyTextEditor.EditThisText(FilterResult)
            FilterText(1).Text = FilterResult
        Case Else
    End Select
End Sub
Private Function TranslateFilter(PatchCodes As String, PatchValues As String) As String
    Dim Result As String
    Dim PatchName As String
    Dim CheckPatch As String
    Dim PatchData As String
    Dim AddSqlKey As String
    Dim tmpZone As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    Dim tmpTifr As String
    Dim tmpTito As String
    Dim ItemNo As Long
    Dim AppendUnknown As Boolean
    Result = ""
    tmpVpfr = ""
    tmpVpto = ""
    tmpTifr = ""
    tmpTito = ""
    AddSqlKey = ""
    AppendUnknown = True
    PatchName = GetRealItem(PatchCodes, ItemNo, Chr(15))
    While PatchName <> ""
        CheckPatch = "[" & PatchName & "]"
        PatchData = GetRealItem(PatchValues, ItemNo, Chr(15))
        Select Case PatchName
            Case "ZONE"
                tmpZone = PatchData
            Case "VPFR"
                tmpVpfr = PatchData
                'SqlVpfr = PatchData
            Case "VPTO"
                tmpVpto = PatchData
                'SqlVpto = PatchData
            Case "TIFR"
                tmpTifr = PatchData
                'SqlTifr = PatchData
            Case "TITO"
                tmpTito = PatchData
                'SqlTito = PatchData
            Case Else
                If AppendUnknown Then
                    'If InStr(CurSqlKey, CheckPatch) = 0 Then
                        Select Case PatchName
                            Case "APC3"
                                If AddSqlKey <> "" Then AddSqlKey = AddSqlKey & " AND "
                                AddSqlKey = AddSqlKey & "(ORG3 IN (" & PatchData & ") OR DES3 IN (" & PatchData & "))"
                            'Do not auto-append these
                            Case "HOPO"
                            Case "TIFR"
                            Case "TITO"
                            Case Else
                                If AddSqlKey <> "" Then AddSqlKey = AddSqlKey & " AND "
                                AddSqlKey = AddSqlKey & PatchName & " IN (" & PatchData & ")"
                        End Select
                    'Else
                    '    Select Case PatchName
                    '        Case "TIFR"
                    '            If Len(PatchData) = 4 Then PatchData = PatchData & "00"
                    '        Case "TITO"
                    '            If Len(PatchData) = 4 Then PatchData = PatchData & "59"
                    '        Case Else
                    '    End Select
                    'End If
                End If
                'CurSqlKey = Replace(CurSqlKey, CheckPatch, PatchData, 1, -1, vbBinaryCompare)
        End Select
        ItemNo = ItemNo + 1
        PatchName = GetRealItem(PatchCodes, ItemNo, Chr(15))
    Wend
    Result = AddSqlKey
    TranslateFilter = Result
End Function
Private Sub chkTopLine_Click(Index As Integer)
    If chkTopLine(Index).Value = 1 Then
        If CurInfoTabIdx >= 0 Then chkTopLine(CurInfoTabIdx).Value = 0
        chkTopLine(Index).BackColor = LightGreen
        InfoTabPanel(Index).Visible = True
        InfoTabPanel(Index).ZOrder
        CurInfoTabIdx = Index
        InfoTab(Index).SetFocus
    Else
        InfoTabPanel(Index).Visible = False
        CurInfoTabIdx = -1
        chkTopLine(Index).BackColor = vbButtonFace
    End If
End Sub

Private Sub Command1_Click()
    'Static IsSolved As Boolean
    Dim wHdl
    'HandleSysError "NET", "No Network", IsSolved
    'IsSolved = Not IsSolved
    'whdl=getfor
    TestForm.Show
End Sub

Private Sub Command2_Click()
    Dim LineNo As Long
    Dim tmpData As String
    For LineNo = 101 To 120
        tmpData = "SQ 30" & CStr(LineNo)
        DataTab(TabIdx).SetFieldValues LineNo - 101, "FLNO", tmpData
        tmpData = "HKG/NRT"
        DataTab(TabIdx).SetFieldValues LineNo - 101, "'RD'", tmpData
    Next
    DataTab(TabIdx).AutoSizeColumns
    TabLookUp(TabIdx).HeaderLengthString = DataTab(TabIdx).HeaderLengthString
    TabLookUp(TabIdx).ColumnAlignmentString = DataTab(TabIdx).ColumnAlignmentString
    DataTab(TabIdx).Refresh
    Command2.Visible = False
End Sub

Private Sub Command3_Click()
    Dim tmpPath As String
    Dim tmpFile As String
    Dim tmpFileName As String
    Dim i As Integer
    tmpPath = App.Path
    tmpFile = "DataTab_0.dat"
    tmpFileName = tmpPath & "\" & tmpFile
    DataTab(0).WriteToFile tmpFileName, False
    tmpFile = "DataTab_1.dat"
    tmpFileName = tmpPath & "\" & tmpFile
    DataTab(1).WriteToFile tmpFileName, False
    For i = 0 To BasicData.BasTab.UBound
        tmpFile = "BasTab_" & CStr(i) & ".dat"
        tmpFileName = tmpPath & "\" & tmpFile
        BasicData.BasTab(i).WriteToFile tmpFileName, False
    Next
    For i = 0 To BasicData.CcaTab.UBound
        tmpFile = "ccaTab_" & CStr(i) & ".dat"
        tmpFileName = tmpPath & "\" & tmpFile
        BasicData.CcaTab(i).WriteToFile tmpFileName, False
    Next
    For i = 0 To BasicData.CcaTab.UBound
        tmpFile = "ccaTab_" & CStr(i) & ".dat"
        tmpFileName = tmpPath & "\" & tmpFile
        BasicData.CcaTab(i).WriteToFile tmpFileName, False
    Next
End Sub

Private Sub CheckLogoColumn(CurTabIdx As Integer)
    Dim MyLogoName As String
    Dim LogoObject As String
    Dim NoLogoObject As String
    Dim tmpFlno As String
    Dim tmpLogo As String
    Dim tmpList As String
    Dim tmpPath As String
    Dim tmpName As String
    Dim tmpFile As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim FlnoCol As Long
    Dim LogoCol As Long
    MyLogoPath = App.Path & "\Logos"
    GetFileAndPath MyLogoPath, tmpFile, tmpPath
    tmpName = Dir(MyLogoPath, vbDirectory)
    If tmpName = "" Then
        MyLogoPath = "D:\Ufis\System\GocToolPics\Logos"
        GetFileAndPath MyLogoPath, tmpFile, tmpPath
        tmpName = Dir(MyLogoPath, vbDirectory)
    End If
    If tmpName = tmpFile Then
        LogoCol = GetRealItemNo(DataTab(CurTabIdx).LogicalFieldList, "'LG'")
        If LogoCol >= 0 Then
            FlnoCol = GetRealItemNo(DataTab(CurTabIdx).LogicalFieldList, "FLNO")
            If FlnoCol >= 0 Then
                tmpList = DataTab(CurTabIdx).Tag
                If tmpList = "" Then
                    tmpFlno = "??"
                    tmpLogo = "," & tmpFlno & ","
                    MyLogoName = "NO_LOGO.bmp"
                    NoLogoObject = MyLogoPath & "\" & MyLogoName
                    DataTab(CurTabIdx).CreateCellBitmpapObj tmpFlno, NoLogoObject
                    tmpList = tmpList & tmpLogo
                End If
                MaxLine = DataTab(CurTabIdx).GetLineCount - 1
                For CurLine = 0 To MaxLine
                    DataTab(CurTabIdx).SetColumnValue CurLine, LogoCol, ""
                    tmpFlno = DataTab(CurTabIdx).GetColumnValue(CurLine, FlnoCol)
                    tmpFlno = Trim(tmpFlno)
                    If Left(tmpFlno, 1) = "[" Then
                        tmpFlno = Mid(tmpFlno, 2)
                        tmpFlno = Left(tmpFlno, Len(tmpFlno) - 1)
                    End If
                    tmpFlno = UCase(Trim(Left(tmpFlno, 3)))
                    If tmpFlno = "" Then tmpFlno = "--"
                    tmpLogo = "," & tmpFlno & ","
                    If InStr(tmpList, tmpLogo) = 0 Then
                        DataTab(CurTabIdx).CreateCellBitmpapObj tmpFlno, NoLogoObject
                        MyLogoName = tmpFlno & "_S.bmp"
                        LogoObject = MyLogoPath & "\" & MyLogoName
                        DataTab(CurTabIdx).CreateCellBitmpapObj tmpFlno, LogoObject
                        tmpList = tmpList & tmpLogo
                    End If
                    DataTab(CurTabIdx).SetCellBitmapProperty CurLine, LogoCol, tmpFlno
                Next
                DataTab(CurTabIdx).Tag = tmpList
                DataTab(CurTabIdx).Refresh
            End If
        End If
    End If
End Sub

Private Sub DataTab_GotFocus(Index As Integer)
    TabEditLookUpIndex = Index
End Sub

Private Sub DataTab_OnVScroll(Index As Integer, ByVal LineNo As Long)
    Dim UseLine As Long
    UseLine = DataTab(Index).GetVScrollPos
    PagePos(Index).Caption = CStr(UseLine + 1)
    PageCnt(Index).Caption = CStr(DataTab(Index).GetLineCount)
End Sub

Private Sub DataTab_RowSelectionChanged(Index As Integer, ByVal LineNo As Long, ByVal Selected As Boolean)
    Dim tmpText As String
    Dim UseLine As Long
    Dim TxtColor As Long
    Dim BckColor As Long
    Dim idx As Integer
    If Selected Then
        If LineNo >= 0 Then
            UseLine = LineNo
        Else
            UseLine = DataTab(Index).GetVScrollPos
        End If
        PagePos(Index).Caption = CStr(UseLine + 1)
        PageCnt(Index).Caption = CStr(DataTab(Index).GetLineCount)
        If Index = 0 Then idx = 1 Else idx = 0
        If (PageFunction(Index) = "INDEX") And (PageFunction(idx) = "TEXT") Then
            tmpText = DataTab(Index).GetFieldValue(LineNo, "TEXT")
            tmpText = CleanString(tmpText, FOR_CLIENT, True)
            AdvText(idx).Text = tmpText
        End If
    End If
End Sub

Private Sub DataTab_SendLButtonClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    'DataTab(Index).SetCurrentSelection -1
End Sub

Private Sub DataTab_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim tmpData As String
    If LineNo >= 0 Then
        Select Case PageFunction(Index)
            Case "PAGE_INDEX"
                tmpData = DataTab(Index).GetFieldValue(LineNo, "PGNO")
                OpenGridPage tmpData
            Case "GROUP_INDEX"
                OpenGridPage "2"
            Case Else
        End Select
    ElseIf LineNo = -1 Then
        SortPageData Index, ColNo
    End If
End Sub
Private Sub OpenGridPage(PageNo As String)
    Dim tmpData As String
    Dim tmpCheck As String
    Dim tmpValue As Long
    tmpData = PageNo
    tmpValue = Val(tmpData)
    If tmpValue > 0 Then
        tmpCheck = CStr(tmpValue)
        If tmpCheck = tmpData Then
            HandleOpenPage tmpCheck
        End If
    End If
End Sub
Private Sub SortPageData(Index As Integer, ColNo As Long)
    DataTab(Index).ColSelectionRemoveAll
    If DataTab(Index).CurrentSortColumn <> ColNo Then
        DataTab(Index).Sort CStr(ColNo), True, True
    Else
        If DataTab(Index).SortOrderASC = True Then
            DataTab(Index).Sort CStr(ColNo), False, True
        Else
            DataTab(Index).Sort CStr(ColNo), True, True
        End If
    End If
    DataTab(Index).ColSelectionAdd ColNo
    DataTab(Index).Refresh
End Sub
Private Sub DataTab_SendRButtonClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim tmpFld As String
    Dim tmpdat As String
    Dim tmpGrp As String
    Dim UseIdx As Integer
    Dim GetFldLst As String
    Dim tmpResult As String
    Dim CurUrno As String
    Dim CurFlno As String
    Dim CurStod As String
    Dim CurCkif As String
    Dim CurCkit As String
    Dim tmpCount As Long
    Dim CurLine As Long
    Dim CurBackColor As Long
    If LineNo >= 0 Then
        CurBackColor = Val(DataTab(Index).GetLineTag(LineNo))
        DataTab(Index).SetLineColor LineNo, vbBlack, CurBackColor
        UseIdx = -1
        tmpFld = GetRealItem(MyPageFields(Index), ColNo, ",")
        tmpdat = Trim(DataTab(Index).GetColumnValue(LineNo, ColNo))
        Select Case tmpFld
            Case "FLNO"
                tmpdat = Trim(Left(tmpdat, 3))
                GetFldLst = "ALC2,ALC3,ALFN"
                UseIdx = 0
            Case "ORG3", "DES3", "VIA3"
                GetFldLst = "APC3,APC4,APFN"
                UseIdx = 1
            Case "ACT3", "ACT5", "ACTI"
                GetFldLst = "ACT3,ACT5,ACFN"
                UseIdx = 2
            Case "CKIF", "CKIT"
                tmpdat = DataTab(Index).GetLineValues(LineNo)
                CurUrno = GetFieldValue("URNO", tmpdat, MyPageFields(Index))
                If InStr(CurUrno, "_") > 0 Then CurUrno = ""
                If CurUrno <> "" Then
                    CurFlno = GetFieldValue("FLNO", tmpdat, MyPageFields(Index))
                    CurCkif = GetFieldValue("CKIF", tmpdat, MyPageFields(Index))
                    CurCkit = GetFieldValue("CKIT", tmpdat, MyPageFields(Index))
                    CurStod = GetFieldValue("STOD", tmpdat, MyPageFields(Index))
                    UseIdx = 99
                End If
            Case Else
        End Select
        If (UseIdx >= 0) And (UseIdx < 99) Then
            If tmpdat <> "" Then
                tmpResult = BasicData.LookUpData(UseIdx, tmpFld, tmpdat, GetFldLst)
                If tmpResult <> "" Then
                    tmpResult = Replace(tmpResult, ",", vbNewLine, 1, -1, vbBinaryCompare)
                    tmpResult = tmpResult & "              "
                    MyMsgBox.AskUser 0, 0, 0, "Basic Data Info", tmpResult, "infomsg", "", UserAnswer
                End If
            End If
        End If
        If UseIdx = 99 Then
            tmpCount = BasicData.LookUpCca(CurUrno, CurFlno, CurStod, CurCkif, CurCkit, True)
            tmpResult = ""
            If tmpCount > 0 Then
                For CurLine = 0 To tmpCount - 1
                    tmpGrp = BasicData.CicGrp(1).GetLineValues(CurLine)
                    tmpdat = GetItem(tmpGrp, 1, ",")
                    tmpGrp = Replace(tmpGrp, (tmpdat & ","), (tmpdat & ":  "), 1, -1, vbBinaryCompare)
                    tmpGrp = Replace(tmpGrp, ",(", "  (", 1, -1, vbBinaryCompare)
                    tmpResult = tmpResult & tmpGrp & vbNewLine
                Next
                tmpResult = Left(tmpResult, (Len(tmpResult) - 2))
                tmpResult = Replace(tmpResult, ",", " - ", 1, -1, vbBinaryCompare)
                tmpResult = tmpResult & "        "
            End If
            If tmpResult = "" Then tmpResult = "No counters planned or opened."
            MyMsgBox.AskUser 0, 0, 0, "Full Checkin Counter Info", tmpResult, "infomsg", "", UserAnswer
        End If
        DataTab(Index).SetLineColor LineNo, LineTextColor, CurBackColor
        'DataTab(Index).CursorLifeStyle = False
    End If
    DataTab(Index).SetCurrentSelection -1
End Sub

Private Sub DataTab_TimerExpired(Index As Integer, ByVal LineNo As Long, ByVal LineStatus As Long)
    Dim CurColor As Long
    DataTab(Index).ResetCellProperties LineNo
    CurColor = Val(DataTab(Index).GetLineTag(LineNo))
    DataTab(Index).SetLineColor LineNo, LineTextColor, CurColor
    DataTab(Index).RedrawTab
    If DataTab(Index).GetLineStatusValue(LineNo) > 0 Then UpdateCount = UpdateCount - 1
    DataTab(Index).SetLineStatusValue LineNo, 0
    ToggleUpdLed
    If UpdateCount < 0 Then UpdateCount = 0
End Sub

Private Sub Form_Activate()
    Static IsInit As Boolean
    Dim CtrlMsg As String
    Dim CfgData As String
    Dim p1 As Long
    Dim p2 As Long
    Dim i As Integer
    Dim LineNo As Long
    If Not IsInit Then
        IsInit = True
        Ufis.StayConnected = True
        If ApplIsInKioskMode Then
            SetFormOnTop Me, True
        End If
        p1 = GetWindowThreadProcessId(Me.hwnd, p2)
        MyApplProcessID = p2
        UnitLabel(7).Caption = CStr(MyApplProcessID)
        GetKeyItem CtrlMsg, Command, "SESS=", " "
        If CtrlMsg = "" Then CtrlMsg = "0"
        MyCtrlSessionID = Val(CtrlMsg)
        
        GetKeyItem CtrlMsg, Command, "PPID=", " "
        If CtrlMsg = "" Then CtrlMsg = "-1"
        MyCtrlProcessID = Val(CtrlMsg)
        GetKeyItem CtrlMsg, Command, "HWND=", " "
        If CtrlMsg = "" Then CtrlMsg = "-1"
        MyCtrlHWnd = Val(CtrlMsg)
        UnitLabel(9).Caption = Command
        
        Load MyCfg
        LineNo = 0
        For i = 0 To optType.UBound
            CfgData = MyCfg.CfgTab(1).GetFieldValue(LineNo, "TEXT")
            optType(i).ToolTipText = CfgData
            LineNo = LineNo + 1
        Next
        optType(0).Value = True
        
        ArrangeUnitLabels
        Form_Resize
        Me.Refresh
        If GotUtcDiff = False Then
            GetUTCDifference
            If TimesInLocal = True Then lblNow.Value = 1
        End If
        CloseConnection False
        OpenConnection False
        
        'TEST
        'MyLoginStatus = False
        
        SetServerPanel "READY"
        
        If MyLoginStatus <> True Then
            TopInfoPanel(7).Visible = True
            Form_Resize
            Me.Refresh
            HandleOpenPage "9998"
        End If
        'Load BasicData
        InitInfoMessages
        optArea(4).Value = True
        CloseConnection False
        tmrSeconds.Interval = 250
        tmrMinutes.Interval = 60000
        ApplicationIsStarted = True
    End If
End Sub
Private Sub InitInfoMessages()
    Dim tmpData As String
    Dim tmpArea As String
    Dim tmpText As String
    Dim tmpUrno As String
    Dim tmpLstu As String
    Dim TimeVal As Date
    Dim tmpValue As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim i As Integer
    For i = 0 To MAX_MSG_CNT
        MsgInfoArray(i).MsgSysInUse = False
    Next
    For i = 0 To TopLabel.UBound
        TopLabel(i).Visible = False
        TopSep(i).Visible = False
    Next
    For i = 0 To LinePanel.UBound
        LinePanel(i).Tag = ""
    Next
    For i = 0 To InfoTab.UBound
        InfoTab(i).ResetContent
        'InfoTab(i).Refresh
    Next
    
    MakeWhere TabIdx, tmpData
    ReadCedaData CdrHdl, "INFTAB", "CDAT,LSTU,APPL,FUNK,AREA,TEXT,URNO", InftabWhere
    CdrHdl.Sort "0", True, True
    CdrHdl.Sort "1", True, True
    MaxLine = CdrHdl.GetLineCount - 1
    For CurLine = 0 To MaxLine
        tmpArea = CdrHdl.GetFieldValue(CurLine, "AREA")
        tmpText = CdrHdl.GetFieldValue(CurLine, "TEXT")
        tmpLstu = CdrHdl.GetFieldValue(CurLine, "LSTU")
        tmpUrno = CdrHdl.GetFieldValue(CurLine, "URNO")
        CheckInfoMessage "INIT", tmpArea, tmpText, tmpLstu, tmpUrno, ""
    Next
End Sub
           'CheckInfoMessage "TRIGGERS", "TK 123: ETAI=20091020124000", "12345678"
Private Sub CheckInfoMessage(CurCmd As String, msgArea As String, RcvMsgText As String, CurLstu As String, CurUrno As String, RefUrno As String)
    Dim CurSection As String
    Dim MsgTplFull As String
    Dim MsgTplType As String
    Dim MsgTplArea As String
    Dim MsgTplText As String
    Dim MsgTplRcvd As String
    Dim tmpTist As String
    Dim newRec As String
    Dim MsgText As String
    Dim CurText As String
    Dim CurArea As String
    Dim tmpData As String
    Dim tmpCheck As String
    Dim tmpFlno As String
    Dim tmpField As String
    Dim tmpValue As String
    Dim tabValue As String
    Dim DspTime As String
    Dim TimeVal As Date
    Dim RowIdx As Integer
    Dim MsgIdx As Integer
    Dim TypIdx As Integer
    Dim LineNo As Long
    Dim TplUsed As Boolean
    CurArea = UCase(msgArea)
    MsgText = RcvMsgText
    TplUsed = False
    'If CurArea = "TRIGGERS" Then
    If CurArea <> "" Then
        MsgTplFull = ""
        MsgText = CleanString(MsgText, FOR_CLIENT, True)
        CurText = GetItem(MsgText, 1, "|")
        MsgTplRcvd = GetItem(MsgText, 2, "|")
        tmpFlno = GetItem(CurText, 1, ":")
        tmpData = GetItem(CurText, 2, ":")
        tmpField = Trim(GetItem(tmpData, 1, "="))
        tmpValue = Trim(GetItem(tmpData, 2, "="))
        If tmpField <> "" Then
            CurSection = StaffPageSection & "_INFO_TEMPLATES"
            MsgTplFull = GetIniEntry(StaffPageIniFile, CurSection, "", tmpField, "")
        End If
        If MsgTplFull = "" Then MsgTplFull = "NONE"
        MsgTplType = GetItem(MsgTplFull, 1, ",")
        MsgTplArea = GetItem(MsgTplFull, 2, ",")
        MsgTplText = GetItem(MsgTplFull, 4, ",")
        TypIdx = Val(GetItem(MsgTplFull, 3, ","))
        tmpData = MsgTplText
        If InStr(tmpData, "[FLNO]") > 0 Then
            tmpData = Replace(tmpData, "[FLNO]", tmpFlno, 1, -1, vbBinaryCompare)
            TplUsed = True
        End If
        Select Case MsgTplType
            Case "TIME"
                If tmpValue <> "" Then
                    TimeVal = CedaFullDateToVb(tmpValue)
                    TimeVal = DateAdd("n", UTC_Diff, TimeVal)
                    If InStr(tmpData, "[TIME]") > 0 Then
                        DspTime = Format(TimeVal, "hhmm")
                        tmpData = Replace(tmpData, "[TIME]", DspTime, 1, -1, vbBinaryCompare)
                        tabValue = Format(TimeVal, "hh:mm")
                        'tabValue = DspTime
                        TplUsed = True
                    End If
                    If InStr(tmpData, "[TDAY]") > 0 Then
                        DspTime = Format(TimeVal, "dd")
                        tmpData = Replace(tmpData, "[TDAY]", DspTime, 1, -1, vbBinaryCompare)
                        tabValue = DspTime
                        TplUsed = True
                    End If
                    If InStr(tmpData, "[DATE]") > 0 Then
                        DspTime = Format(TimeVal, "ddmmmyy")
                        DspTime = UCase(DspTime)
                        tmpData = Replace(tmpData, "[DATE]", DspTime, 1, -1, vbBinaryCompare)
                        tabValue = DspTime
                        TplUsed = True
                    End If
                End If
                TplUsed = True
            Case "GATE", "UPDM"
                tmpData = Replace(tmpData, "[VALUE]", tmpValue, 1, -1, vbBinaryCompare)
                tabValue = tmpValue
                TplUsed = True
            Case Else
                tmpCheck = tmpData
                tmpData = Replace(tmpData, "[VALUE]", tmpValue, 1, -1, vbBinaryCompare)
                If tmpCheck <> tmpData Then
                    TplUsed = True
                Else
                    TplUsed = False
                End If
                tabValue = tmpValue
        End Select
    End If
    If (Not TplUsed) And (Left(CurArea, 4) = "AREA") Then
        CurText = GetItem(MsgText, 1, "|")
        MsgTplRcvd = GetItem(MsgText, 2, "|")
        TypIdx = Val(GetItem(MsgTplRcvd, 1, ","))
        If (TypIdx <= 0) And (Left(CurArea, 4) = "AREA") Then TypIdx = 0
        tmpData = CurText
        MsgTplArea = CurArea
    ElseIf Not TplUsed Then
        TypIdx = -1
    End If
    If TypIdx >= 0 Then
        RowIdx = GetTopLineIdx(MsgTplArea)
        If (RowIdx < 0) And (Left(MsgTplArea, 4) = "AREA") Then MsgTplArea = Mid(MsgTplArea, 5)
        If RowIdx < 0 Then RowIdx = Val(MsgTplArea) - 1
        If RowIdx < 0 Then RowIdx = chkTopLine.UBound
        If RowIdx > chkTopLine.UBound Then RowIdx = chkTopLine.UBound
        Select Case CurCmd
            Case "IRT", "INIT"
                MsgIdx = AddInfoMessage(CurCmd, RowIdx, TypIdx, tmpData, CurUrno, "", RcvMsgText)
                If MsgIdx >= 0 Then
                    newRec = ""
                    newRec = newRec & CurLstu & ","
                    newRec = newRec & " " & tmpFlno & ","
                    newRec = newRec & tmpField & ","
                    newRec = newRec & tabValue & ","
                    newRec = newRec & " " & tmpData & ","
                    newRec = newRec & CurUrno & ","
                    newRec = newRec & CStr(MsgIdx) & ","
                    InfoTab(RowIdx).InsertTextLineAt 0, newRec, False
                    InfoTab(RowIdx).AutoSizeColumns
                    LineNo = InfoTab(RowIdx).GetLineCount - 1
                    If LineNo > 500 Then InfoTab(RowIdx).DeleteLine LineNo
                End If
            Case "URT"
                MsgIdx = AddInfoMessage(CurCmd, RowIdx, TypIdx, tmpData, CurUrno, "", RcvMsgText)
                If MsgIdx >= 0 Then
                End If
            Case "DRT"
                MsgIdx = AddInfoMessage(CurCmd, RowIdx, TypIdx, tmpData, CurUrno, "", RcvMsgText)
                If MsgIdx >= 0 Then
                End If
            Case Else
        End Select
    End If
End Sub
Private Function GetTopLineIdx(AreaCode As String) As Integer
    Dim tmpTag As String
    Dim idx As Integer
    Dim i As Integer
    idx = -1
    For i = 0 To chkTopLine.UBound
        tmpTag = chkTopLine(i).Tag
        If InStr(tmpTag, AreaCode) > 0 Then
            idx = i
            Exit For
        End If
    Next
    GetTopLineIdx = idx
End Function

Private Sub ArrangeUnitLabels()
    Dim NewLeft As Long
    Dim i As Integer
    MyVduName = GetMyUnitProperty("UNIT")
    MyWksName = GetMyUnitProperty("WKS")
    MyWksType = GetMyUnitProperty("TYPE")
    UnitLabel(1).Caption = MyVduName
    UnitLabel(3).Caption = MyWksName
    MyUnitLocation = GetMyUnitLocation
    UnitLabel(5).Caption = MyUnitLocation
    If MyWksType = "WKS" Then
        MyLed(4).Picture = MyLed(6).Picture
        MyLed(5).Picture = MyLed(6).Picture
        MyLed(4).Visible = True
        MyLed(5).Visible = True
    End If
    'Else
        If MyVduName = MyWksName Then
            TcpLed(2).Picture = MyLed(2).Picture
            TcpLed(3).Picture = MyLed(1).Picture
            UnitLabel(2).Visible = False
            UnitLabel(3).Visible = False
        Else
            TcpLed(2).Picture = MyLed(3).Picture
            TcpLed(3).Picture = MyLed(0).Picture
            UnitLabel(2).Visible = True
            UnitLabel(3).Visible = True
        End If
    'End If
    NewLeft = LedPanel(1).Left + LedPanel(1).Width + 30
    For i = 0 To UnitLabel.UBound
        UnitLabel(i).Left = NewLeft
        If UnitLabel(i).Visible Then
            NewLeft = NewLeft + UnitLabel(i).Width + 30 + ((i Mod 2) * 90)
        End If
    Next
End Sub
Private Function GetMyUnitProperty(ForWhat As String) As String
    Dim tmpPath As String
    Dim tmpResult As String
    Dim tmpData As String
    Dim WksName As String
    Dim UnitName As String
    Dim fn As Integer
    On Error Resume Next
    Select Case ForWhat
        Case "UNIT"
            tmpPath = App.Path & "\unitlocation.txt"
            fn = FreeFile
            Open tmpPath For Input As #fn
            Line Input #fn, UnitName
            Close fn
            UnitName = UCase(UnitName)
            tmpResult = UnitName
        Case "WKS", "WKS-NAME"
            WksName = CdiPort(0).LocalHostName
            WksName = UCase(WksName)
            tmpResult = WksName
        Case "LOCATION"
            tmpPath = App.Path & "\unitlocation.txt"
            fn = FreeFile
            Open tmpPath For Input As #fn
            Line Input #fn, UnitName
            Line Input #fn, UnitName
            Close fn
            UnitName = UCase(UnitName)
            tmpResult = UnitName
        Case "TYPE", "WKS-TYPE"
            tmpPath = App.Path & "\unitlocation.txt"
            fn = FreeFile
            Open tmpPath For Input As #fn
            Line Input #fn, tmpData
            Line Input #fn, tmpData
            Line Input #fn, UnitName
            Close fn
            If UnitName = "" Then UnitName = "VDU"
            UnitName = UCase(UnitName)
            tmpResult = UnitName
        Case Else
            WksName = CdiPort(0).LocalHostName
            WksName = UCase(WksName)
            tmpResult = WksName
    End Select
    GetMyUnitProperty = tmpResult
End Function
Private Function GetMyUnitLocation() As String
    Dim tmpPath As String
    Dim tmpData As String
    Dim tmpResult As String
    Dim fn As Integer
    tmpResult = "UNKNOWN"
    On Error Resume Next
    tmpPath = App.Path & "\unitlocation.txt"
    fn = FreeFile
    Open tmpPath For Input As #fn
    Line Input #fn, tmpData
    Line Input #fn, tmpResult
    Close fn
    GetMyUnitLocation = UCase(tmpResult)
End Function

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    CheckKeyboard Shift
    If (TabEditLookUpIndex >= 0) Or (KeyCode = vbKeyF3) Then
        Select Case KeyCode
            Case vbKeyF3
                JumpF3Key
            Case vbKeyCapital
                'Here we get 'Key Changing" ...
                'See KeyUp Event
                'n = GetKeyState(vbKeyCapital)
            Case Else
        End Select
    ElseIf (Not txtTabEditLookUp(0).Visible) And (Not txtTabEditLookUp(1).Visible) Then
        KeyCodeInput KeyCode, Shift, 0, "Form.KeyDown"
    End If
End Sub
Private Sub JumpF3Key()
    Dim tmpText As String
    Dim KeyCols As String
    Dim KeyVals As String
    Dim iCnt As Integer
    Dim n As Integer
    If TabEditLookUpType = "LOOK" Then
        If TabEditLookUpIndex >= 0 Then
            If txtTabEditLookUp(TabEditLookUpIndex).Visible Then
                'GetLookUpColsAndKeys TabEditLookUpIndex, KeyCols, KeyVals
                'imgAnyPic2(TabEditLookUpIndex).Picture = ImagePool.imgLookUpA.Picture
                SyncCursorBusy = True
                SearchInTabList DataTab(TabEditLookUpIndex), TabEditColNo, txtTabEditLookUp(TabEditLookUpIndex).Text, False, "", ""
                SyncCursorBusy = False
            'ElseIf TabEditLookUpFixed >= 0 Then
            '    iCnt = GetLookUpColsAndKeys(TabEditLookUpFixed, KeyCols, KeyVals)
            '    If iCnt > 0 Then
            '        'If iCnt = 1 Then imgAnyPic2(TabEditLookUpIndex).Picture = ImagePool.imgLookUpA.Picture Else imgAnyPic2(TabEditLookUpIndex).Picture = ImagePool.imgLookUpB.Picture
            '        tmpText = TabLookUp(TabEditLookUpFixed).GetColumnValue(0, TabEditLookUpValueCol)
            '        SearchInTabList DataTab(TabEditLookUpFixed), TabEditLookUpValueCol, tmpText, False, KeyCols, KeyVals
            '    End If
            ElseIf TabEditLookUpValueCol >= 0 Then
                iCnt = GetLookUpColsAndKeys(TabEditLookUpIndex, KeyCols, KeyVals)
                If iCnt > 0 Then
                    'If iCnt = 1 Then imgAnyPic2(TabEditLookUpIndex).Picture = ImagePool.imgLookUpA.Picture Else imgAnyPic2(TabEditLookUpIndex).Picture = ImagePool.imgLookUpB.Picture
                    tmpText = TabEditLookUpTabObj.GetColumnValue(TabEditLineNo, TabEditLookUpValueCol)
                    SearchInTabList DataTab(TabEditLookUpIndex), TabEditLookUpValueCol, tmpText, False, KeyCols, KeyVals
                End If
            End If
        End If
    End If
End Sub
Private Sub CreateSqlFromLookUp()
    Dim KeyCols As String
    Dim KeyVals As String
    Dim FldName As String
    Dim tmpName As String
    Dim FldData As String
    Dim ColText As String
    Dim FldColNo As Long
    Dim SqlKey As String
    Dim KeyItm As Integer
    Dim iCnt As Integer
    Dim idx As Integer
    SqlKey = ""
    If TabEditLookUpValueCol >= 0 Then
        idx = TabEditLookUpIndex
        If idx < 0 Then idx = 1
        iCnt = GetLookUpColsAndKeys(idx, KeyCols, KeyVals)
        KeyItm = 0
        ColText = "START"
        While ColText <> ""
            KeyItm = KeyItm + 1
            ColText = GetItem(KeyCols, KeyItm, ",")
            If ColText <> "" Then
                FldColNo = Val(ColText)
                FldName = GetRealItem(DataTab(idx).LogicalFieldList, FldColNo, ",")
                FldData = GetItem(KeyVals, KeyItm, ",")
                tmpName = FldName
                Select Case tmpName
                    Case "'RA'"
                        If SqlKey <> "" Then SqlKey = SqlKey & " AND "
                        SqlKey = SqlKey & "((" & "ORG3" & " LIKE '%" & FldData & "%')"
                        SqlKey = SqlKey & " OR (" & "VIAL" & " LIKE '% " & FldData & "%')"
                        SqlKey = SqlKey & " OR (" & "VIA3" & " LIKE '%" & FldData & "%'))"
                    Case "'RD'"
                        If SqlKey <> "" Then SqlKey = SqlKey & " AND "
                        SqlKey = SqlKey & "((" & "DES3" & " LIKE '%" & FldData & "%')"
                        SqlKey = SqlKey & " OR (" & "VIAL" & " LIKE '% " & FldData & "%')"
                        SqlKey = SqlKey & " OR (" & "VIA3" & " LIKE '%" & FldData & "%'))"
                    Case Else
                        If SqlKey <> "" Then SqlKey = SqlKey & " AND "
                        SqlKey = SqlKey & "(" & FldName & " LIKE '%" & FldData & "%')"
                End Select
            End If
        Wend
        FilterText(idx).Text = SqlKey
    End If
End Sub
Private Sub Form_KeyPress(KeyAscii As Integer)
    If TabEditLookUpIndex >= 0 Then
    ElseIf (Not txtTabEditLookUp(0).Visible) And (Not txtTabEditLookUp(1).Visible) Then
        KeyCodeInput KeyAscii, 0, 3, "Form.KeyPress"
    End If
End Sub
Private Sub DataTab_HitKeyOnLine(Index As Integer, ByVal Key As Integer, ByVal LineNo As Long)
    Dim tmpData As String
    TabEditLookUpIndex = Index
    If LineNo >= 0 Then
        If ActNumInput <> "" Then
            KeyCodeInput Key, 0, 1, "DataTab.HitKeyOnLine." & CStr(Index)
        ElseIf Key = 13 Then
            Select Case PageFunction(Index)
                Case "PAGE_INDEX"
                    tmpData = DataTab(Index).GetFieldValue(LineNo, "PGNO")
                    OpenGridPage tmpData
                Case "GROUP_INDEX"
                    OpenGridPage "2"
                Case Else
                    KeyCodeInput Key, 0, 1, "DataTab.HitKeyOnLine." & CStr(Index)
            End Select
        Else
            KeyCodeInput Key, 0, 1, "DataTab.HitKeyOnLine." & CStr(Index)
        End If
    Else
        KeyCodeInput Key, 0, 1, "DataTab.HitKeyOnLine." & CStr(Index)
    End If
End Sub

Private Sub InfoTab_HitKeyOnLine(Index As Integer, ByVal Key As Integer, ByVal LineNo As Long)
    Dim tmpData As String
    If LineNo >= 0 Then
        If ActNumInput <> "" Then
            KeyCodeInput Key, 0, 1, "InfoTab.HitKeyOnLine." & CStr(Index)
        ElseIf Key = 13 Then
            InfoTab_SendLButtonDblClick Index, LineNo, 0
        Else
            KeyCodeInput Key, 0, 1, "InfoTab.HitKeyOnLine." & CStr(Index)
        End If
    Else
        KeyCodeInput Key, 0, 1, "InfoTab.HitKeyOnLine." & CStr(Index)
    End If
End Sub

Private Sub InfoTab_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim tmpData As String
    Dim MsgUrno As String
    Dim MsgIdx As Integer
    tmpData = InfoTab(Index).GetFieldValue(LineNo, "MIDX")
    MsgIdx = Val(tmpData)
    MsgUrno = InfoTab(Index).GetFieldValue(LineNo, "URNO")
    If MsgUrno = MsgInfoArray(MsgIdx).MsgSysMsgUrno Then
        TopLabel_Click MsgIdx
    End If
End Sub

Private Sub MsgTplTab_HitKeyOnLine(Index As Integer, ByVal Key As Integer, ByVal LineNo As Long)
    Select Case Key
        Case 13
            SetMsgTemplate Index, LineNo
        Case Else
    End Select
End Sub

Private Sub MsgTplTab_RowSelectionChanged(Index As Integer, ByVal LineNo As Long, ByVal Selected As Boolean)
    Dim MsgText As String
    Dim msgArea As String
    Dim MsgType As String
    Dim idx As Integer
    On Error Resume Next
    If Selected Then
        If LineNo >= 0 Then
            MsgText = MsgTplTab(Index).GetFieldValue(LineNo, "TEXT")
            msgArea = MsgTplTab(Index).GetFieldValue(LineNo, "AREA")
            MsgType = MsgTplTab(Index).GetFieldValue(LineNo, "TYPE")
            idx = Val(msgArea) - 1
            optArea(idx).Value = True
            idx = Val(MsgType) - 1
            optType(idx).Value = True
        End If
    End If
    MsgTplTab(Index).SetFocus
End Sub
Private Sub SetMsgTemplate(Index As Integer, LineNo As Long)
    Dim MsgText As String
    Dim msgArea As String
    Dim MsgType As String
    Dim idx As Integer
    On Error Resume Next
    'If Selected Then
        If LineNo >= 0 Then
            MsgText = MsgTplTab(Index).GetFieldValue(LineNo, "TEXT")
            txtMsgText.Text = MsgText
        End If
    'End If
    txtMsgText.SetFocus
    txtMsgText.SelStart = Len(MsgText)
End Sub

Private Sub MsgTplTab_SendLButtonDblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    SetMsgTemplate Index, LineNo
End Sub

Private Sub OcmLabel_DblClick(Index As Integer)
    chkTool(27).Value = 1
End Sub

Private Sub OcmTab_HitKeyOnLine(Index As Integer, ByVal Key As Integer, ByVal LineNo As Long)
    KeyCodeInput Key, 0, 1, "OcmTab.HitKeyOnLine." & CStr(Index)
End Sub
Public Sub KeyCodeInput(KeyAscCode As Integer, Shift As Integer, MyCaller As Integer, ConText As String)
    Static LastCaller As Integer
    Dim GotKey As String
    Dim MsgDbg As String
    Dim tmpDbg As String
    Dim CheckIt As Boolean
    'If Text3.Visible Then
    '    tmpDbg = "[" & ConText & "]" & vbTab
    '    If MyCaller <> 1 Then tmpDbg = tmpDbg & vbTab
    '    MsgDbg = "From[" & CStr(MyCaller) & "]" & tmpDbg & "KeyAsc[" & CStr(KeyAscCode) & "] Shift[" & CStr(Shift) & "]"
    '    Text3.Text = MsgDbg & vbNewLine & Text3.Text
    '    tmrTest.Enabled = True
    'End If
    CheckAutoLogoff
    CheckIt = True
    If TopInfoPanel(12).Visible Then CheckIt = False
    If CheckIt Then
        GotKey = ActNumInput
        Select Case MyCaller
            Case 0
                'KeyCodes from Form.KeyDown
                Select Case KeyAscCode
                    Case 13
                        HandleEnterKey
                    Case 27
                        HandleEscCode
                    Case Else
                End Select
            Case 1
                'KeyCodes from TabOcx.HitKeyOnLine
                Select Case KeyAscCode
                    Case 13
                        HandleEnterKey
                    Case 27
                        HandleEscCode
                    Case Else
                End Select
            Case 3
                'ASCII Codes from Form.KeyPress
                Select Case KeyAscCode
                    Case 27
                        HandleEscCode
                    Case 48 To 57
                        ActNumInput = ActNumInput & Chr(KeyAscCode)
                    Case 42     'Star
                        Select Case GotKey
                            Case "*"
                                ActNumInput = "**"
                            Case "**"
                                ActNumInput = "***"
                            Case Else
                                ActNumInput = "*"
                        End Select
                    Case 46   'Period
                        Select Case GotKey
                            Case "."
                                ActNumInput = ".."
                            Case ".."
                                ActNumInput = "..."
                            Case Else
                                ActNumInput = "."
                        End Select
                    Case 47  'Slash
                        Select Case GotKey
                            Case "/"
                                ActNumInput = "//"
                            Case "//"
                                ActNumInput = "///"
                            Case Else
                                ActNumInput = "/"
                        End Select
                    Case 43    'Plus
                        ActNumInput = "+"
                    Case 45    'Minus
                        ActNumInput = "-"
                    Case Else
                        ActNumInput = ""
                End Select
            Case Else
        End Select
        LastCaller = MyCaller
        chkFunc.Caption = ActNumInput
        KeyInputLabel(0).Caption = ActNumInput
        If ActNumInput = "" Then KeyInputLabel(0).Caption = "INPUT"
        If ActNumInput <> GotKey Then
            ActNumInput = LookupKeyInput(ActNumInput)
            chkFunc.Caption = ActNumInput
            KeyInputLabel(0).Caption = ActNumInput
        End If
        If ActNumInput <> "" Then
            chkFunc.BackColor = vbWhite
        Else
            chkFunc.BackColor = vbButtonFace
            KeyInputLabel(0).Caption = "INPUT"
        End If
    End If
End Sub
Private Sub HandleEscCode()
    Dim CheckIt As Boolean
    CheckIt = True
    If TopInfoPanel(12).Visible Then CheckIt = False
    If CheckIt Then
        If ActNumInput <> "" Then
            ActNumInput = ""
        ElseIf FormIsVisible("MyKeyCodes") Then
            MyKeyCodes.Hide
        ElseIf (TopInfoPanel(4).Visible) And (TopInfoPanel(4).Height > TopInfoPanel(0).Height) Then
            TopInfoPanel(4).Height = TopInfoPanel(0).Height
        ElseIf CurInfoTabIdx >= 0 Then
            chkTopLine(CurInfoTabIdx).Value = 0
        ElseIf Val(MyActivePageNumber) > 9900 Then
            HandleOpenPage MyLastPageNumber
        ElseIf Val(MyActivePageNumber) > 10 Then
            HandleOpenPage "2"
        ElseIf Val(MyActivePageNumber) = 2 Then
            HandleOpenPage "1"
        Else
            'HandleOpenPage "0"
        End If
    End If
End Sub
Private Sub HandleEnterKey()
    Dim tmpCode As String
    Dim tmpValue As Long
    Dim tmpLen As Long
    If ActNumInput <> "" Then
        tmpLen = Len(ActNumInput)
        tmpValue = Val(ActNumInput)
        tmpCode = Right("0000000000" & CStr(tmpValue), tmpLen)
        If tmpCode = ActNumInput Then
            'It's a page number
            If MyLoginStatus = True Then
                HandleOpenPage tmpCode
            End If
            ActNumInput = ""
        End If
    End If
End Sub
Public Sub HandleOpenPage(UserPageCode As String)
    Dim SysPageCode As String
    Dim tmpData As String
    If UserPageCode <> "" Then
        SysPageCode = Right("0000000000" & CStr(UserPageCode), 4)
        If SysPageCode <> "0000" Then
            InitPagePanel SysPageCode
        Else
            btnExit_Click
        End If
    End If
End Sub
Private Function LookupKeyInput(KeyCode As String) As String
    Dim KeyMean As String
    Dim KeyOut As String
    Dim CallDone As Boolean
    KeyOut = KeyCode
    KeyMean = MyKeyCodes.GetKeyCodeMean(KeyCode)
    If InStr("NOK,CNT", KeyMean) = 0 Then
        If KeyMean = "RPT" Then KeyMean = LastKeyFunc
        CallDone = CallKeyFunction(KeyMean)
        If CallDone = True Then
            LastKeyFunc = KeyMean
            KeyOut = ""
        End If
    End If
    LookupKeyInput = KeyOut
End Function
Private Function CallKeyFunction(KeyFunc As String) As Boolean
    Dim KeyMean As String
    Dim xx As Integer
    Dim idx As Integer
    Dim MaxLine As Long
    Dim LineNo As Long
    Dim TopLine As Long
    Dim BotLine As Long
    Dim VisLines As Long
    Dim TopScroll As Long
    Dim RetVal As Boolean
    xx = 1
    RetVal = True
    KeyMean = KeyFunc
    Select Case KeyMean
        Case "PGDN"
            If CurInfoTabIdx < 0 Then
                btnNext_Click
            Else
                If xx = 0 Then
                    LineNo = InfoTab(CurInfoTabIdx).GetVScrollPos
                    VisLines = ((InfoTab(CurInfoTabIdx).Height / 15) / InfoTab(CurInfoTabIdx).LineHeight) - 2
                    LineNo = LineNo + VisLines
                    InfoTab(CurInfoTabIdx).OnVScrollTo LineNo
                Else
                    MaxLine = InfoTab(CurInfoTabIdx).GetLineCount - 1
                    LineNo = InfoTab(CurInfoTabIdx).GetCurrentSelected
                    If LineNo < MaxLine Then
                        LineNo = LineNo + 1
                        InfoTab(CurInfoTabIdx).SetCurrentSelection LineNo
                        TopLine = InfoTab(CurInfoTabIdx).GetVScrollPos
                        VisLines = ((InfoTab(CurInfoTabIdx).Height / 15) / InfoTab(CurInfoTabIdx).LineHeight) - 2
                        BotLine = TopLine + VisLines - 1
                        If LineNo > BotLine Then
                            TopScroll = TopLine + (LineNo - BotLine)
                            InfoTab(CurInfoTabIdx).OnVScrollTo TopScroll
                        ElseIf LineNo < TopLine Then
                            TopScroll = LineNo
                            InfoTab(CurInfoTabIdx).OnVScrollTo TopScroll
                        End If
                    End If
                End If
            End If
        Case "PGUP"
            If CurInfoTabIdx < 0 Then
                btnPrev_Click
            Else
                If xx = 0 Then
                    LineNo = InfoTab(CurInfoTabIdx).GetVScrollPos
                    VisLines = ((InfoTab(CurInfoTabIdx).Height / 15) / InfoTab(CurInfoTabIdx).LineHeight) - 2
                    LineNo = LineNo - VisLines
                    If LineNo < 0 Then LineNo = 0
                    InfoTab(CurInfoTabIdx).OnVScrollTo LineNo
                Else
                    LineNo = InfoTab(CurInfoTabIdx).GetCurrentSelected
                    If LineNo > 0 Then
                        LineNo = LineNo - 1
                        InfoTab(CurInfoTabIdx).SetCurrentSelection LineNo
                        TopLine = InfoTab(CurInfoTabIdx).GetVScrollPos
                        VisLines = ((InfoTab(CurInfoTabIdx).Height / 15) / InfoTab(CurInfoTabIdx).LineHeight) - 2
                        BotLine = TopLine + VisLines - 1
                        If LineNo > BotLine Then
                            TopScroll = TopLine + (LineNo - BotLine)
                            InfoTab(CurInfoTabIdx).OnVScrollTo TopScroll
                        ElseIf LineNo < TopLine Then
                            TopScroll = LineNo
                            InfoTab(CurInfoTabIdx).OnVScrollTo TopScroll
                        End If
                    End If
                End If
            End If
        Case "EXIT"
            chkFunc.Value = 1
        Case "MSGW"
            'chkFunc.BackColor = LightYellow
            RetVal = False
        Case "MSG0"
            ToggleOcmPanel
        Case "MSG1", "MSG2", "MSG3", "MSG4", "MSG5"
            idx = Val(Right(KeyMean, 1)) - 1
            If chkTopLine(idx).Value = 1 Then
                chkTopLine(idx).Value = 0
            Else
                chkTopLine(idx).Value = 1
            End If
        Case "KBD"
            MyKeyCodes.Show , Me
        Case "CLS"
            MyKeyCodes.Hide
        Case Else
            RetVal = False
    End Select
    CallKeyFunction = RetVal
End Function
Private Sub Form_Load()
    Dim i As Integer
    ApplicationIsStarted = False
    ApplicationIsBusy = True
    MyApplThreadID = App.ThreadID
    Set MainDialog = Me
    MyOwnButtonFace = vbButtonFace
    ApplMainCode = "VDU"
    ApplFuncCode = "FCS"
    CurCedaIdx = 0
    PendingOpenFavorite = -1
    
    'AskUser = True
    chkIcon.Picture = Me.Icon
    Set MyMainForm = Me
    TimerCaption = "LOCAL TIME"
    MyWksName = CdiPort(0).LocalHostName
    CurInfoTabIdx = -1
    DarkSpacer(0).ZOrder
    LightSpacer(0).ZOrder
    For i = 1 To 11
        Load DarkSpacer(i)
        DarkSpacer(i).ZOrder
        Load LightSpacer(i)
        LightSpacer(i).ZOrder
    Next
    DarkSpacer(0).Visible = True
    DarkSpacer(1).Visible = True
    DarkSpacer(4).Visible = True
    DarkSpacer(4).BackColor = vbButtonFace
    For i = 0 To PagePanel.UBound
        PageGrpFr(i).Caption = ""
        PageGrpTo(i).Caption = ""
        PageCode(i).Caption = ""
        PagePos(i).Caption = ""
        PageCnt(i).Caption = ""
    Next
    For i = 0 To UnitStatus.UBound
        UnitStatus(i).Caption = ""
    Next
    'DarkLineColor = &HC00000
    'LightLineColor = &HFF0000
    'LineTextColor = TranslateColor("YELLOW")
    
    DarkLineColor = &H40C0&
    LightLineColor = &H80FF&
    LineTextColor = TranslateColor("YELLOW")
    
    Load MyKeyCodes
    Load SysFunc
    Load WinShut
    
    For i = 0 To LedPanel.UBound
        LedPanel(i).Width = 210
        LedPanel(i).Top = 15
        LedPanel(i).BackColor = TopInfoPanel(8).BackColor
    Next
    LedPanel(1).Left = 60
    LedPanel(3).Width = MyLed(5).Left + MyLed(5).Width + 30
    LedFrame.BackColor = chkAlive.BackColor
    'MyTabFontName(0) = "Courier New"
    'MyTabFontName(1) = "Courier New"
    MyTabFontName(0) = "Arial"
    MyTabFontName(1) = "Arial"
    
    For i = 0 To FilterText.UBound
        FilterText(i).Text = ""
    Next
    TabLookUp(0).CreateDecorationObject "'S3'", "BTL", "8", CStr(LightRed)
    TabLookUp(1).CreateDecorationObject "'S3'", "BTL", "8", CStr(LightRed)
    
    TopInfoPanel(13).Width = chkTool(20).Width / 15
    TopInfoPanel(13).Height = chkTool(20).Height / 15
    MsgTplTab(0).Top = 0
    MsgTplTab(0).Left = 0
    MsgTplTab(0).Width = TopInfoPanel(15).ScaleWidth
    MsgTplTab(0).Height = TopInfoPanel(15).ScaleHeight
    
    InitApplication
    
    'If AutoSize Then
    '    Form_Resize
        AppendLines 0
        AppendLines 1
    'End If
    Me.Caption = MyMainCaption
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 3 Then
        Cancel = False
        btnExit_Click
    End If
End Sub

Public Sub ResizeMyForm()
    Form_Resize
End Sub
Private Sub Form_Resize()
    Dim NewSize As Long
    Dim NewLeft As Long
    Dim LeftSize As Long
    Dim RightSize As Long
    Dim NewTop As Long
    Dim TabHeight As Long
    Dim TopBottom As Long
    Dim NewHeight As Long
    Dim VisLines As Long
    Dim tmpVal As Long
    Dim i As Integer
    On Error Resume Next
    If (Not WinShutDown) And (Me.WindowState <> vbMinimized) Then
        NewSize = Me.ScaleWidth
        ButtonPanel(0).Width = NewSize
        ButtonPanel(1).Width = NewSize
        NewLeft = chkFunc.Left + chkFunc.Width
        chkSpace.Left = NewLeft
        NewSize = NewSize - NewLeft
        If NewSize > 2 Then chkSpace.Width = NewSize
        
        NewSize = Me.ScaleWidth - (TopInfoPanel(0).Left * 2)
        TopInfoPanel(0).Width = NewSize
        NewSize = TopInfoPanel(0).ScaleWidth - TopLine(0).Left
        For i = 0 To TopLine.UBound
            TopLine(i).Width = NewSize
            LinePanel(i).Left = 0
            LinePanel(i).Width = TopLine(i).ScaleWidth
        Next
        
        NewTop = 0
        
        LightSpacer(0).Top = NewTop
        LightSpacer(0).Left = 0
        LightSpacer(0).Width = Me.ScaleWidth
        If LightSpacer(0).Visible Then NewTop = NewTop + LightSpacer(0).Height
        DarkSpacer(0).Top = NewTop
        DarkSpacer(0).Left = 0
        DarkSpacer(0).Width = Me.ScaleWidth
        If DarkSpacer(0).Visible Then NewTop = NewTop + DarkSpacer(0).Height
        TopInfoPanel(8).Top = NewTop
        TopInfoPanel(8).Left = -2
        TopInfoPanel(8).Width = Me.ScaleWidth + 4
        LedPanel(0).Left = TopInfoPanel(8).ScaleWidth - LedPanel(0).Width - 30
        LedPanel(2).Left = LedPanel(0).Left - LedPanel(2).Width - 15
        LedPanel(3).Left = (lblNow.Left) * 15
        If TopInfoPanel(8).Visible Then NewTop = NewTop + TopInfoPanel(8).Height
        
        LightSpacer(1).Top = NewTop
        LightSpacer(1).Left = 0
        LightSpacer(1).Width = Me.ScaleWidth
        If LightSpacer(1).Visible Then NewTop = NewTop + LightSpacer(1).Height
        DarkSpacer(1).Top = NewTop
        DarkSpacer(1).Left = 0
        DarkSpacer(1).Width = Me.ScaleWidth
        If DarkSpacer(1).Visible Then NewTop = NewTop + DarkSpacer(1).Height
        ButtonPanel(0).Top = NewTop
        If ButtonPanel(0).Visible Then NewTop = NewTop + ButtonPanel(0).Height
        
        TopInfoPanel(10).Top = NewTop
        TopInfoPanel(10).Left = TopInfoPanel(0).Left
        
        TopInfoPanel(11).Top = NewTop
        TopInfoPanel(11).Left = TopInfoPanel(10).Left + TopInfoPanel(10).Width
        TopInfoPanel(11).Width = TopInfoPanel(0).Left + TopInfoPanel(0).Width - TopInfoPanel(11).Left
        
        TopInfoPanel(12).Top = NewTop
        TopInfoPanel(12).Left = TopInfoPanel(11).Left
        TopInfoPanel(12).Width = TopInfoPanel(11).Width
        TopInfoPanel(13).Top = TopInfoPanel(12).Top + TopInfoPanel(12).Height + 1
        TopInfoPanel(13).Left = TopInfoPanel(12).Left + (txtMsgText.Left / 15) - TopInfoPanel(13).Width + 4
        TopInfoPanel(14).Top = TopInfoPanel(13).Top
        TopInfoPanel(14).Left = TopInfoPanel(13).Left + TopInfoPanel(13).Width - 6
        
        chkTool(26).Left = TopInfoPanel(12).ScaleWidth - chkTool(26).Width
        chkTool(25).Left = chkTool(26).Left - chkTool(25).Width - 15
        chkTool(24).Left = chkTool(25).Left - chkTool(24).Width - 15
        MsgTypBut.Left = chkTool(24).Left - MsgTypBut.Width - 30
        NewSize = MsgTypBut.Left - txtMsgText.Left - 30
        If NewSize > 300 Then txtMsgText.Width = NewSize
        
        TopInfoPanel(9).Top = NewTop
        TopInfoPanel(9).Left = TopInfoPanel(11).Left
        TopInfoPanel(9).Width = TopInfoPanel(11).Width
        chkTool(15).Left = TopInfoPanel(9).ScaleWidth - chkTool(15).Width
        chkTool(14).Left = chkTool(15).Left - chkTool(14).Width
        
        If TopInfoPanel(10).Visible Then NewTop = NewTop + TopInfoPanel(10).Height
        
        
        LightSpacer(2).Top = NewTop
        LightSpacer(2).Left = 0
        LightSpacer(2).Width = Me.ScaleWidth
        If LightSpacer(2).Visible Then NewTop = NewTop + LightSpacer(2).Height
        DarkSpacer(2).Top = NewTop
        DarkSpacer(2).Left = 0
        DarkSpacer(2).Width = Me.ScaleWidth
        If DarkSpacer(2).Visible Then NewTop = NewTop + DarkSpacer(2).Height
        TopInfoPanel(2).Top = NewTop
        TopInfoPanel(2).Left = TopInfoPanel(0).Left
        TopInfoPanel(2).Width = TopInfoPanel(0).Width
        KeyInputLabel(0).Left = TopInfoPanel(2).ScaleWidth - KeyInputLabel(0).Width - 60
        If TopInfoPanel(2).Visible Then NewTop = NewTop + TopInfoPanel(2).Height
        
        LightSpacer(3).Top = NewTop
        LightSpacer(3).Left = 0
        LightSpacer(3).Width = Me.ScaleWidth
        If LightSpacer(3).Visible Then NewTop = NewTop + LightSpacer(3).Height
        DarkSpacer(3).Top = NewTop
        DarkSpacer(3).Left = 0
        DarkSpacer(3).Width = Me.ScaleWidth
        If DarkSpacer(3).Visible Then NewTop = NewTop + DarkSpacer(3).Height
        TopInfoPanel(0).Top = NewTop
        TopInfoPanel(4).Top = TopInfoPanel(0).Top
        If TopInfoPanel(0).Visible Then NewTop = NewTop + TopInfoPanel(0).Height
        
        LightSpacer(4).Top = NewTop
        LightSpacer(4).Left = 0
        LightSpacer(4).Width = Me.ScaleWidth
        If LightSpacer(4).Visible Then NewTop = NewTop + LightSpacer(4).Height
        DarkSpacer(4).Top = NewTop
        DarkSpacer(4).Left = 0
        DarkSpacer(4).Width = Me.ScaleWidth
        If DarkSpacer(4).Visible Then NewTop = NewTop + DarkSpacer(4).Height
        TopInfoPanel(7).Top = NewTop
        TopInfoPanel(7).Left = -2
        TopInfoPanel(7).Width = Me.ScaleWidth + 4
        If TopInfoPanel(7).Visible Then NewTop = NewTop + TopInfoPanel(7).Height
        
        LightSpacer(5).Top = NewTop
        LightSpacer(5).Left = 0
        LightSpacer(5).Width = Me.ScaleWidth
        If LightSpacer(5).Visible Then NewTop = NewTop + LightSpacer(5).Height
        DarkSpacer(5).Top = NewTop
        DarkSpacer(5).Left = 0
        DarkSpacer(5).Width = Me.ScaleWidth
        If DarkSpacer(5).Visible Then NewTop = NewTop + DarkSpacer(5).Height
        TopInfoPanel(5).Top = NewTop
        TopInfoPanel(5).Left = -2
        TopInfoPanel(5).Width = Me.ScaleWidth + 4
        If TopInfoPanel(5).Visible Then NewTop = NewTop + TopInfoPanel(5).Height
        
        LightSpacer(6).Top = NewTop
        LightSpacer(6).Left = 0
        LightSpacer(6).Width = Me.ScaleWidth
        If LightSpacer(6).Visible Then NewTop = NewTop + LightSpacer(6).Height
        DarkSpacer(6).Top = NewTop
        DarkSpacer(6).Left = 0
        DarkSpacer(6).Width = Me.ScaleWidth
        If DarkSpacer(6).Visible Then NewTop = NewTop + DarkSpacer(6).Height
        TopInfoPanel(1).Top = NewTop
        TopInfoPanel(1).Left = TopInfoPanel(0).Left
        TopInfoPanel(1).Width = TopInfoPanel(0).Width
        If TopInfoPanel(1).Visible Then NewTop = NewTop + TopInfoPanel(1).Height
        
        
        TopInfoPanel(6).Top = NewTop
        TopInfoPanel(6).Left = TopInfoPanel(0).Left
        TopInfoPanel(6).Width = TopInfoPanel(0).Width
        If chkTool(2).Value = 1 Then
            chkTool(3).Left = TopInfoPanel(6).ScaleWidth - chkTool(3).Width
            chkTool(3).Picture = MyIcons(5).Picture
            FilterText(1).Left = chkTool(7).Left + chkTool(7).Width + 15
            FilterText(1).Width = chkTool(3).Left - FilterText(1).Left - 15
            FilterText(1).Visible = True
            FilterText(1).ZOrder
            MyKeyPadPanel(1).Left = FilterText(1).Left
            MyKeyPadPanel(1).Width = FilterText(1).Width + 30
            FilterText(2).Left = -15
            FilterText(2).Width = MyKeyPadPanel(1).ScaleWidth + 15
            FilterText(2).Visible = True
            MyKeyPadPanel(1).Visible = True
            FilterText(1).SetFocus
            chkTool(7).Visible = True
            chkTool(8).Visible = True
        Else
            chkTool(3).Left = chkTool(2).Left + chkTool(2).Width + 15
            chkTool(3).Picture = MyIcons(6).Picture
            FilterText(1).Visible = False
            MyKeyPadPanel(1).Visible = False
            chkTool(7).Visible = False
            chkTool(8).Visible = False
        End If
        If TopInfoPanel(6).Visible Then NewTop = NewTop + TopInfoPanel(6).Height
        
        LightSpacer(7).Top = NewTop
        LightSpacer(7).Left = 0
        LightSpacer(7).Width = Me.ScaleWidth
        If LightSpacer(7).Visible Then NewTop = NewTop + LightSpacer(7).Height
        DarkSpacer(7).Top = NewTop
        DarkSpacer(7).Left = 0
        DarkSpacer(7).Width = Me.ScaleWidth
        If DarkSpacer(7).Visible Then NewTop = NewTop + DarkSpacer(7).Height
        
        
        TabLookUp(0).Top = NewTop
        TabLookUp(0).Left = DataTab(0).Left
        TabLookUp(0).Width = DataTab(0).Width
        
        TabLookUp(1).Top = NewTop
        TabLookUp(1).Left = DataTab(1).Left
        TabLookUp(1).Width = DataTab(1).Width
        If (TabLookUp(1).Visible) Or (TabLookUp(0).Visible) Then NewTop = NewTop + TabLookUp(1).Height
        
        DataTab(0).Top = NewTop
        DataTab(1).Top = NewTop
        
        NewSize = Me.ScaleWidth - (TopInfoPanel(0).Left * 2)
        BotInfoPanel(0).Width = NewSize
        If TopInfoPanel(4).Visible Then
            TopInfoPanel(4).Left = Me.ScaleWidth - TopInfoPanel(4).Width - TopInfoPanel(0).Left
            OcmLabel(1).Left = TopInfoPanel(4).Left * 15
            OcmLabel(1).Visible = True
            NewSize = TopInfoPanel(4).Left - (TopInfoPanel(0).Left * 2) - 8
        End If
        
        NewSize = Me.ScaleWidth - (DataTab(0).Left * 2)
        LeftSize = NewSize
        RightSize = NewSize
        If ShowBothGrids Then
            NewSize = NewSize - 10
            LeftSize = NewSize / 2
            RightSize = NewSize - LeftSize
            DataTab(1).Left = LeftSize + 10 + DataTab(0).Left
        Else
            DataTab(1).Left = DataTab(0).Left
        End If
        DataTab(0).Width = LeftSize
        DataTab(1).Width = RightSize
        PagePanel(0).Left = (DataTab(0).Left * 15) - (TopInfoPanel(1).Left * 15)
        PagePanel(1).Left = (DataTab(1).Left * 15) - (TopInfoPanel(1).Left * 15)
        PagePanel(0).Width = DataTab(0).Width * 15
        PagePanel(1).Width = (DataTab(1).Width * 15)
        ArrangeLookUpGrids -1, "SIZE"
        
        TopBottom = Me.ScaleHeight
        If DarkSpacer(11).Visible Then
            TopBottom = TopBottom - DarkSpacer(11).Height
            DarkSpacer(11).Top = TopBottom
            DarkSpacer(11).Left = 0
            DarkSpacer(11).Width = Me.ScaleWidth
        End If
        If LightSpacer(11).Visible Then
            TopBottom = TopBottom - LightSpacer(11).Height
            LightSpacer(11).Top = TopBottom
            LightSpacer(11).Left = 0
            LightSpacer(11).Width = Me.ScaleWidth
        End If
        
        If ButtonPanel(1).Visible Then
            'Minimum Button Height for the Used Font
            TopBottom = TopBottom - 21
        End If
        
        If DarkSpacer(10).Visible Then
            TopBottom = TopBottom - DarkSpacer(10).Height
            DarkSpacer(10).Top = TopBottom
            DarkSpacer(10).Left = 0
            DarkSpacer(10).Width = Me.ScaleWidth
        End If
        If LightSpacer(10).Visible Then
            TopBottom = TopBottom - LightSpacer(10).Height
            LightSpacer(10).Top = TopBottom
            LightSpacer(10).Left = 0
            LightSpacer(10).Width = Me.ScaleWidth
        End If
        If BotInfoPanel(0).Visible Then
            TopBottom = TopBottom - BotInfoPanel(0).Height
            BotInfoPanel(0).Top = TopBottom
        End If
        If DarkSpacer(9).Visible Then
            TopBottom = TopBottom - DarkSpacer(9).Height
            DarkSpacer(9).Top = TopBottom
            DarkSpacer(9).Left = 0
            DarkSpacer(9).Width = Me.ScaleWidth
        End If
        
        If LightSpacer(9).Visible Then
            TopBottom = TopBottom - LightSpacer(9).Height
            LightSpacer(9).Top = TopBottom
            LightSpacer(9).Left = 0
            LightSpacer(9).Width = Me.ScaleWidth
        End If
        
        
        
        TabHeight = TopBottom - DataTab(0).Top
        If AutoSize Then
            VisLines = (CLng((TabHeight - DataTab(0).LineHeight) \ CLng(DataTab(0).LineHeight))) + 1
            NewSize = VisLines * DataTab(0).LineHeight
            If (DataTab(0).Top + TabHeight + 1) > Me.ScaleHeight Then
                VisLines = VisLines - 1
            End If
            ListLines(0) = (VisLines - 1) '+ 2
            ListLines(1) = (VisLines - 1) '+ 2
            TabHeight = NewSize
        End If
        DataTab(0).Height = TabHeight
        DataTab(1).Height = TabHeight
                
        For i = 0 To 1
            If PageFunction(i) = "TEXT" Then
                AdvPanel(i).Top = DataTab(i).Top
                AdvPanel(i).Left = DataTab(i).Left
                AdvPanel(i).Width = DataTab(i).Width
                AdvPanel(i).Height = DataTab(i).Height
                AdvText(i).Top = 0
                AdvText(i).Left = 0
                AdvText(i).Width = AdvPanel(i).ScaleWidth
                AdvText(i).Height = AdvPanel(i).ScaleHeight
                AdvPanel(i).Visible = True
                AdvPanel(i).ZOrder
            Else
                AdvPanel(i).Visible = False
            End If
        Next
        
        TopBottom = DataTab(0).Top + TabHeight
        If LightSpacer(8).Visible Then
            LightSpacer(8).Top = TopBottom
            LightSpacer(8).Left = 0
            LightSpacer(8).Width = Me.ScaleWidth
            TopBottom = TopBottom + LightSpacer(8).Height
        End If
        If DarkSpacer(8).Visible Then
            DarkSpacer(8).Top = TopBottom
            DarkSpacer(8).Left = 0
            DarkSpacer(8).Width = Me.ScaleWidth
            TopBottom = TopBottom + DarkSpacer(8).Height
        End If
        
        
        PageNoPanel(0).Left = PagePanel(0).ScaleWidth - PageNoPanel(0).Width
        PageNoPanel(1).Left = PagePanel(1).ScaleWidth - PageNoPanel(1).Width
        tmpVal = (PagePanel(0).ScaleWidth - InfoTitle(0).Width) \ 2
        InfoTitle(0).Left = tmpVal
        tmpVal = (PagePanel(1).ScaleWidth - InfoTitle(1).Width) \ 2
        InfoTitle(1).Left = tmpVal
        TopInfoPanel(3).Left = 2
        TopInfoPanel(3).Width = Me.ScaleWidth - 4
        tmpVal = DataTab(0).Top + TabHeight
        If tmpVal < Me.ScaleHeight Then
            TopInfoPanel(3).Top = tmpVal
            TopInfoPanel(3).Height = Me.ScaleHeight - tmpVal
            tmpVal = TopInfoPanel(3).Height
            If tmpVal > 3 Then
                TopInfoPanel(3).Visible = True
            Else
                TopInfoPanel(3).Visible = False
            End If
        Else
            TopInfoPanel(3).Visible = False
        End If
        ArrangeBottomButtons "SIZE"
        
        ArrangeInfoTabs -1, -1
    ElseIf (Not WinShutDown) And (Me.WindowState = vbMinimized) Then
        Me.WindowState = vbNormal
    End If
End Sub
Private Sub ArrangeBottomButtons(ForWhat As String)
    Dim NewHeight As Long
    Dim LeftWidth As Long
    Dim RightWidth As Long
    Dim NewLeft As Long
    Dim NewWidth As Long
    Select Case ForWhat
        Case "INIT"
            ButtonPanel(1).Height = chkLeftAction.Height
            NewLeft = 0
            btnExit.Left = NewLeft
            btnExit.Width = Abs(Val(btnExit.Tag))
            NewLeft = NewLeft + btnExit.Width
            If chkHide.Tag = "YES" Then
                chkHide.Left = NewLeft
                NewLeft = NewLeft + chkHide.Width
            End If
            If CdiIdx > 0 Then
                btnCdiServer.Left = NewLeft
                NewLeft = NewLeft + btnCdiServer.Width
            End If
            NewWidth = NewLeft
            ButtonBack(1).Width = NewWidth
        Case "SIZE"
            If ButtonPanel(1).Visible Then
                ButtonPanel(1).Top = TopInfoPanel(3).Top
                NewHeight = TopInfoPanel(3).Height
                ButtonPanel(1).Height = NewHeight
                ButtonBack(1).Height = NewHeight
                chkLeftAction.Height = NewHeight
                chkRightAction.Height = NewHeight
                btnExit.Height = NewHeight
                chkHide.Height = NewHeight
                btnCdiServer.Height = NewHeight
                btnPrev.Height = NewHeight
                btnNext.Height = NewHeight
                
                NewWidth = ButtonPanel(1).ScaleWidth - ButtonBack(1).Width
                LeftWidth = NewWidth / 2
                RightWidth = NewWidth - LeftWidth
                ButtonBack(1).Left = LeftWidth
                chkLeftAction.Left = 0
                chkLeftAction.Width = (LeftWidth / 2)
                chkRightAction.Left = chkLeftAction.Left + chkLeftAction.Width
                chkRightAction.Width = LeftWidth - chkLeftAction.Width
                btnPrev.Left = ButtonBack(1).Left + ButtonBack(1).Width
                btnPrev.Width = RightWidth / 2
                btnNext.Left = btnPrev.Left + btnPrev.Width
                btnNext.Width = RightWidth - btnPrev.Width
                
            End If
        Case Else
    End Select
End Sub
Private Sub Form_Unload(Cancel As Integer)
    If Not ShutDownRequested Then
        btnExit_Click
    End If
End Sub

Private Sub LedFrame_Click()
    chkAlive.Value = 1
End Sub

Private Sub InfoTab_TimerExpired(Index As Integer, ByVal LineNo As Long, ByVal LineStatus As Long)
    Dim idx As Integer
    Dim LineStat As Long
    Dim ForeColor As Long
    Dim BackColor As Long
    InfoTab(Index).GetLineColor LineNo, ForeColor, BackColor
    InfoTab(Index).SetLineColor LineNo, BackColor, ForeColor
    LineStat = InfoTab(Index).GetLineStatusValue(LineNo) - 1
    InfoTab(Index).SetLineStatusValue LineNo, LineStat
    If LineStat > 0 Then InfoTab(Index).TimerSetValue LineNo, 1
    InfoTab(Index).RedrawTab
End Sub

Private Sub lblNow_Click()
    Dim TmpCol As String
    Dim ColNo As Long
    Dim tmpItm As Integer
    Dim i As Integer
    If lblNow.Value = 1 Then
        For i = 0 To 1
            tmpItm = 1
            TmpCol = GetItem(MyTimeCols(i), tmpItm, ",")
            While TmpCol <> ""
                ColNo = Val(TmpCol)
                DataTab(i).DateTimeSetUTCOffsetMinutes ColNo, UTC_Diff
                tmpItm = tmpItm + 1
                TmpCol = GetItem(MyTimeCols(i), tmpItm, ",")
            Wend
            DataTab(i).Refresh
            MainCaption(i) = TimerCaption
        Next
        MainTitle.Tag = "LOC"
        MainTitle.Caption = MainCaption(TabIdx)
        lblNow.Value = 0
    End If
End Sub

Private Sub lblRefresh_Click(Index As Integer)
    chkAlive.Value = 1
End Sub

Private Sub lblUTC_Click()
    Dim TmpCol As String
    Dim ColNo As Long
    Dim tmpItm As Integer
    Dim i As Integer
    If lblUTC.Value = 1 Then
        For i = 0 To 1
            tmpItm = 1
            TmpCol = GetItem(MyTimeCols(i), tmpItm, ",")
            While TmpCol <> ""
                ColNo = Val(TmpCol)
                DataTab(i).DateTimeSetUTCOffsetMinutes ColNo, 0
                tmpItm = tmpItm + 1
                TmpCol = GetItem(MyTimeCols(i), tmpItm, ",")
            Wend
            DataTab(i).Refresh
            MainCaption(i) = "TIMES IN UTC"
        Next
        MainTitle.Caption = MainCaption(TabIdx)
        MainTitle.Tag = "UTC"
        lblUTC.Value = 0
    End If
End Sub

Private Sub MainTitle_Click()
    If MainTitle.Value = 1 Then
        If MainTitle.Tag = "LOC" Then
            lblUTC.Value = 1
        Else
            lblNow.Value = 1
        End If
        MainTitle.Value = 0
    End If
End Sub

Private Sub optArea_Click(Index As Integer)
    Dim idx As Integer
    If optArea(Index).Value = True Then
        If optArea(0).Tag <> "" Then
            idx = Val(optArea(0).Tag)
            optArea(idx).BackColor = vbButtonFace
            chkTopLine(idx).BackColor = vbButtonFace
        End If
        optArea(Index).BackColor = LightGreen
        If TopInfoPanel(12).Visible Then chkTopLine(Index).BackColor = LightYellow
        AreaMean.Caption = optArea(Index).ToolTipText
        If txtMsgText.Visible Then txtMsgText.SetFocus
        optArea(0).Tag = CStr(Index)
    End If
End Sub

Private Sub optType_Click(Index As Integer)
    Dim idx As Integer
    If optType(Index).Value = True Then
        If optType(0).Tag <> "" Then
            idx = Val(optType(0).Tag)
            optType(idx).BackColor = vbButtonFace
        End If
        optType(Index).BackColor = LightGreen
        If txtMsgText.Visible Then txtMsgText.SetFocus
        optType(0).Tag = CStr(Index)
    End If
End Sub

Private Sub PageTitle_Click()
    If PageTitle.Value = 1 Then
        If ShowBasics = True Then
            BasicData.Show
            BasicData.Refresh
        End If
        If ReloadOnDemand = True Then
            BasicData.CcaTab(0).myTag = ""
            BasicData.CcaTab(1).myTag = ""
            QueryFlights 0
            QueryFlights 1
        End If
        If ShowBothGridsSve Then
            ToggleGridView = ToggleGridView + 1
            If ToggleGridView > 2 Then ToggleGridView = 0
            If ToggleGridView > 0 Then
                ShowBothGrids = False
            Else
                ShowBothGrids = True
            End If
        End If
        If Not ShowBothGrids Then
            ButtonPushedInternal = True
            If TabIdx = 0 Then
                chkRightAction.Value = 1
            Else
                chkLeftAction.Value = 1
            End If
            ButtonPushedInternal = False
        Else
            PageTitle.Caption = PageMainTitle
            PagePanel(0).Visible = True
            PagePanel(1).Visible = True
            If PageIsValid(0) Then DataTab(0).Visible = True
            If PageIsValid(1) Then DataTab(1).Visible = True
        End If
        Form_Resize
        If DataTab(TabIdx).Visible Then DataTab(TabIdx).SetFocus
        PageTitle.Value = 0
    End If
End Sub

Private Sub RightTool_Click(Index As Integer)
'    If TopInfoPanel(9).Visible Then
'        TopInfoPanel(9).Visible = False
'        'RightTool(Index).Tag = "SHOW"
'    Else
'        TopInfoPanel(9).Visible = True
'        'RightTool(Index).Tag = "HIDE"
'    End If
'    Form_Resize
End Sub

Private Sub Slider1_Click()
    DataTab(TabIdx).LineHeight = Slider1.Value + 6
    DataTab(TabIdx).FontSize = DataTab(TabIdx).LineHeight
    DataTab(TabIdx).HeaderFontSize = DataTab(TabIdx).FontSize
    'Form_Resize
    'AppendLines TabIdx
    DataTab(TabIdx).AutoSizeColumns
    TabLookUp(TabIdx).HeaderLengthString = DataTab(TabIdx).HeaderLengthString
    TabLookUp(TabIdx).ColumnAlignmentString = DataTab(TabIdx).ColumnAlignmentString
    DataTab(TabIdx).Refresh
End Sub

Private Sub Slider1_Scroll()
    DataTab(TabIdx).LineHeight = Slider1.Value + 6
    DataTab(TabIdx).FontSize = DataTab(TabIdx).LineHeight
    DataTab(TabIdx).HeaderFontSize = DataTab(TabIdx).FontSize
    'Form_Resize
    'AppendLines TabIdx
    DataTab(TabIdx).AutoSizeColumns
    TabLookUp(TabIdx).HeaderLengthString = DataTab(TabIdx).HeaderLengthString
    TabLookUp(TabIdx).ColumnAlignmentString = DataTab(TabIdx).ColumnAlignmentString
    DataTab(TabIdx).Refresh
End Sub

Private Sub TabLookUp_GotFocus(Index As Integer)
    TabEditLookUpIndex = Index
    'ToogleLookUp Index
    'txtTabEditLookUp_KeyDown Index, vbKeyReturn, 0
End Sub

Private Sub TabLookUp_SendLButtondblClick(Index As Integer, ByVal LineNo As Long, ByVal ColNo As Long)
    Dim FldName As String
    Dim SysFldList As String
    Dim idx As Integer
    TabEditLookUpIndex = Index
    idx = Index
    FldName = GetRealItem(TabLookUp(idx).LogicalFieldList, ColNo, ",")
    If InStr(FldName, "'") > 0 Then
        'SysFldList = AddAftVersFields(Index, "", 0) 'Status Icons
        'If InStr(SysFldList, FldName) > 0 Then
        '    SyncCursorBusy = True
        '    SearchInTabList DataTab(idx), ColNo, "__", False, "", ""
        '    SyncCursorBusy = False
        'End If
        AdjustTabEditField Index, "LOOK", TabLookUp(Index), LineNo, ColNo
    ElseIf InStr(MyTimeFields(Index), FldName) > 0 Then
    Else
        AdjustTabEditField Index, "LOOK", TabLookUp(Index), LineNo, ColNo
    End If
End Sub

Private Sub Text3_Change()
    If Text3.Text <> "" Then MsgBox Asc(Left(Text3.Text, 1))
End Sub

Private Sub tmrBlink_Timer()
    Dim OldMsgIniRec As MsgLayout
    Dim NewMsgIniRec As MsgLayout
    Dim tmpText As String
    Dim tmpTag As String
    Dim i As Integer
    Dim MsgIdx As Integer
    Dim TypeChange As Boolean
    Dim ReorgRow As Boolean
    For i = 1 To MsgSysBlinkCnt
        TypeChange = False
        MsgIdx = MsgSysBlinkLst(i)
        If MsgInfoArray(MsgIdx).MsgNrmTimer.CountSec > 0 Then
            MsgInfoArray(MsgIdx).MsgNrmTimer.CountSec = MsgInfoArray(MsgIdx).MsgNrmTimer.CountSec - 1
            If MsgInfoArray(MsgIdx).MsgNrmTimer.CountSec <= 0 Then
                If MsgInfoArray(MsgIdx).MsgAniStatus = 1 Then
                    OldMsgIniRec = MsgInfoArray(MsgIdx).MsgLblAni1
                Else
                    OldMsgIniRec = MsgInfoArray(MsgIdx).MsgLblAni2
                End If
                NewMsgIniRec = MsgInfoArray(MsgIdx).MsgLblNorm
                MsgInfoArray(MsgIdx).MsgAniStatus = 0
                MsgInfoArray(MsgIdx).MsgNrmTimer.CountSec = -1
                CleanUpMsgLabels MsgIdx, 1
                TypeChange = True
            End If
        End If
        If MsgInfoArray(MsgIdx).MsgNrmTimer.CountSec >= 0 Then
            MsgInfoArray(MsgIdx).MsgAniTimer.CountSec = MsgInfoArray(MsgIdx).MsgAniTimer.CountSec - 1
            If MsgInfoArray(MsgIdx).MsgAniTimer.CountSec <= 0 Then
                If MsgInfoArray(MsgIdx).MsgAniStatus = 1 Then
                    If MsgInfoArray(MsgIdx).MsgLblAni2.MsgAnimation.TotalSec > 0 Then
                        OldMsgIniRec = MsgInfoArray(MsgIdx).MsgLblAni1
                        NewMsgIniRec = MsgInfoArray(MsgIdx).MsgLblAni2
                        MsgInfoArray(MsgIdx).MsgAniStatus = 2
                        TypeChange = True
                    Else
                        MsgInfoArray(MsgIdx).MsgAniTimer.CountSec = MsgInfoArray(MsgIdx).MsgLblAni1.MsgAnimation.TotalSec
                    End If
                Else
                    OldMsgIniRec = MsgInfoArray(MsgIdx).MsgLblAni2
                    NewMsgIniRec = MsgInfoArray(MsgIdx).MsgLblAni1
                    MsgInfoArray(MsgIdx).MsgAniStatus = 1
                    TypeChange = True
                End If
            End If
        End If
        If TypeChange Then
            ReorgRow = False
            tmpText = MsgInfoArray(MsgIdx).MsgDspText
            If NewMsgIniRec.LblTextTrim = True Then
                tmpText = Trim(tmpText)
            Else
                tmpText = " " & Trim(tmpText) & " "
            End If
            If NewMsgIniRec.LblTextUpper = True Then
                tmpText = UCase(tmpText)
                TopLabel(MsgIdx).Top = -15
            Else
                TopLabel(MsgIdx).Top = 0
            End If
            TopLabel(MsgIdx).Caption = tmpText
            TopLabel(MsgIdx).BorderStyle = NewMsgIniRec.LblBorderStyle
            TopLabel(MsgIdx).BackStyle = NewMsgIniRec.LblBackStyle
            TopLabel(MsgIdx).ForeColor = NewMsgIniRec.LblForeColor
            TopLabel(MsgIdx).BackColor = NewMsgIniRec.LblBackColor
            MsgInfoArray(MsgIdx).MsgAniTimer.CountSec = NewMsgIniRec.MsgAnimation.TotalSec
            If NewMsgIniRec.LblBorderStyle <> OldMsgIniRec.LblBorderStyle Then ReorgRow = True
            If NewMsgIniRec.LblTextTrim <> OldMsgIniRec.LblTextTrim Then ReorgRow = True
            If NewMsgIniRec.LblTextUpper <> OldMsgIniRec.LblTextUpper Then ReorgRow = True
            If ReorgRow Then ArrangeMsgRow MsgInfoArray(MsgIdx).MsgSysRow
        End If
    Next
End Sub

Private Sub tmrExit_Timer()
    Dim tmpTag As String
    Dim tmpSec As Long
    tmpTag = chkFunc.Tag
    If Val(tmpTag) < 1 Then
        tmpTag = tmrExit.Tag
        chkFunc.BackColor = vbButtonFace
        chkFunc.ForeColor = vbBlack
        chkFunc.Caption = ""
        chkFunc.Picture = MyIcons(0).Picture
    End If
    tmpSec = Val(tmpTag) - 1
    Select Case tmpSec
        Case 0 To 20
            chkFunc.BackColor = vbRed
            chkFunc.ForeColor = vbWhite
            chkFunc.Caption = CStr(tmpSec)
            If tmpSec < 1 Then chkFunc.Value = 1
        Case 21 To 30
            chkFunc.BackColor = &H80FF&
            chkFunc.Picture = MyIcons(1).Picture
        Case Else
    End Select
    tmpTag = CStr(tmpSec)
    chkFunc.Tag = tmpTag
End Sub
Private Sub CheckAutoLogoff()
    Dim tmpTag As String
    If tmrExit.Enabled Then
        If chkFunc.BackColor <> vbWhite Then
            chkFunc.Caption = ""
            KeyInputLabel(0).Caption = ""
            chkFunc.BackColor = vbWhite
            chkFunc.ForeColor = vbBlack
            chkFunc.Picture = MyIcons(0).Picture
        End If
        tmpTag = tmrExit.Tag
        chkFunc.Tag = tmpTag
    End If
End Sub
Private Sub tmrSeconds_Timer()
    Dim tmpTime As Date
    Dim tmpSrvLoc As Date
    Dim tmpSrvUtc As Date
    Dim tmpWksLoc As Date
    Dim tmpWksUtc As Date
    Dim tmpDiff As Long
    Dim tmpButton
    Dim CurTabIdx As Integer
    Dim MinSec As Integer
    Dim fgwHwnd As Long
    Dim fgwPID
    Dim p1 As Long
    Dim p2 As Long
    On Error Resume Next
    If ShutDownRequested = True Then
        btnExit_Click
    End If
    'SetFormOnTop Me, True
    fgwHwnd = GetForegroundWindow()
    If FormIsVisible("MyCfg") Then fgwHwnd = Me.hwnd
    If (MyWksType <> "WKS") And (fgwHwnd <> Me.hwnd) Then
        MyLed(4).Picture = MyLed(0).Picture
        If fgwHwnd <> MyCtrlHWnd Then
            If ApplIsInKioskMode Then
                p1 = PostMessage(fgwHwnd, WM_CLOSE, 0&, 0&)
            End If
        End If
        AppActivate MyApplProcessID
        fgwHwnd = GetForegroundWindow()
        If fgwHwnd <> Me.hwnd Then
            MyLed(5).Picture = MyLed(0).Picture
        Else
           MyLed(5).Picture = MyLed(2).Picture
        End If
    ElseIf MyWksType <> "WKS" Then
        MyLed(4).Picture = MyLed(2).Picture
        MyLed(5).Picture = MyLed(2).Picture
    End If
    If ApplIsInKioskMode Then
        SetFormOnTop Me, True
    End If
    DoEvents
    WksNowDate = Now
    If Not GotLtDiff Then
        If SrvUtcTime <> "" Then
            SrvLocDate = CedaFullDateToVb(SrvLocTime)
            SrvUtcDate = CedaFullDateToVb(SrvUtcTime)
            WksLocDate = Now
            WksLocDiff = DateDiff("n", SrvLocDate, WksLocDate)
            If Abs(WksLocDiff) <= 2 Then
                WksLocDiff = 0
                TimerCaption = "LOCAL TIME"
                MainTitle.Caption = TimerCaption
            Else
                TimerCaption = "SYSTEM TIME"
                MainTitle.Caption = TimerCaption
            End If
            WksLocDate = DateAdd("n", -WksLocDiff, WksLocDate)
            GotLtDiff = True
        Else
            WksLocDate = Now
            WksPrvDate = WksLocDate
        End If
    End If
    WksNowDiff = DateDiff("s", WksNowDate, WksPrvDate)
    WksLocDate = DateAdd("s", -WksNowDiff, WksLocDate)
    WksUtcDate = DateAdd("n", -UTC_Diff, WksLocDate)
    WksPrvDate = WksNowDate
    lblNow.Caption = Format(WksLocDate, "dd.mm.yy / hh:mm:ss")
    lblUTC.Caption = Format(WksUtcDate, "hh:mm:ss")
    WksLocTime = Format(WksLocDate, "yyyymmddhhmmss")
    If Not CdiServer Then
        If RemoteCon(0).Visible = True Then
            If Val(RemoteCon(0).Tag) <= 0 Then
                RemoteCon(0).Visible = False
                RemoteCon(1).Visible = False
            Else
                RemoteCon(0).Tag = Str(Val(RemoteCon(0).Tag) - 1)
            End If
        End If
        MinSec = 1000
        For CurTabIdx = 0 To 1
            If (Not PageIsValid(CurTabIdx)) Then RefreshCountDown(CurTabIdx) = False
            If RefreshCountDown(CurTabIdx) = True Then
                SecCountDown(CurTabIdx) = SecCountDown(CurTabIdx) - 1
                If SecCountDown(CurTabIdx) < MinSec Then MinSec = SecCountDown(CurTabIdx)
                If SecCountDown(CurTabIdx) <= 0 Then
                    tmpButton = CurLoadButton
                    MinSec = 1000
                    If CurTabIdx = 0 Then
                        CurLoadButton = "LEFT"
                        QueryLoadData CurLoadButton, True
                    Else
                        CurLoadButton = "RIGHT"
                        QueryLoadData CurLoadButton, True
                        If (PageChanged) And (CurTabIdx = 1) Then
                            If (FormIsVisible("MyCfg")) Then
                                If (MyCfg.chkAny(2).Value = 1) Then
                                    MyCfg.ChkType(26).Value = 1
                                End If
                            End If
                            PageChanged = False
                        End If
                    End If
                    If tmpButton <> CurLoadButton Then
                        CurLoadButton = MyLoadButton
                        omDisplayMode = MyLoadButton
                    End If
                    If DataTab(CurTabIdx).Visible Then DataTab(CurTabIdx).SetFocus
                    Me.SetFocus
                End If
            Else
                If (Not PageIsValid(CurTabIdx)) Then LostBroadCast(CurTabIdx) = False
                If LostBroadCast(CurTabIdx) Then
                    If CountDownAnimation Then
                        If RefreshInd(CurTabIdx).FillColor = vbRed Then
                            RefreshInd(CurTabIdx).FillColor = vbYellow
                        Else
                            RefreshInd(CurTabIdx).FillColor = vbRed
                        End If
                    End If
                End If
            End If
            DataTab(CurTabIdx).TimerCheck
        Next
        If PageJustLoaded Then
            If ShowBothGridsSve Then
                If ToggleButtons = "YES" Then
                    Select Case StartDataView
                        Case "MASTER"
                            chkRightAction.Value = 1
                        Case "SISTER"
                            chkLeftAction.Value = 1
                        Case "DOUBLE"
                            'Do Nothing (Default)
                        Case Else
                    End Select
                End If
            End If
            PageJustLoaded = False
        End If
        If MinSec < 1000 Then
            If CountDownAnimation Then
                lblRefresh(1).Caption = Trim(Str(MinSec)) & " SEC"
                If RefreshInd(0).FillColor = RemoteColor(0) Then
                    RefreshInd(0).FillColor = RemoteColor(1)
                    RefreshInd(1).FillColor = RemoteColor(1)
                Else
                    RefreshInd(0).FillColor = RemoteColor(0)
                    RefreshInd(1).FillColor = RemoteColor(0)
                End If
            End If
        End If
        ShowUnitStatus
    End If
End Sub

Private Sub tmrMinutes_Timer()
    Dim CurTabIdx As Integer
    If Not CdiServer Then
        If DefSaveTimer > 0 Then
            ActSaveTimer = ActSaveTimer - 1
            If ActSaveTimer <= 0 Then
                WriteDataToFile 0
                WriteDataToFile 1
                ActSaveTimer = DefSaveTimer
            End If
        End If
        For CurTabIdx = 0 To 1
            If PageIsValid(CurTabIdx) Then
                MinCountDown(CurTabIdx) = MinCountDown(CurTabIdx) - 1
                If MinCountDown(CurTabIdx) = 1 Then
                    If Not RefreshCountDown(CurTabIdx) Then
                        SecCountDown(CurTabIdx) = 60
                        RefreshCountDown(CurTabIdx) = True
                        lblRefresh(0).Caption = "RELOAD"
                    End If
                End If
                If MinCountDown(CurTabIdx) <= 0 Then MinCountDown(CurTabIdx) = RefreshCyclus
            End If
        Next
        ShowUnitStatus
    End If
End Sub
Private Sub ShowUnitStatus()
    Dim CurTabIdx As Integer
    For CurTabIdx = 0 To 1
        UnitStatus(CurTabIdx).Caption = CStr(MinCountDown(CurTabIdx))
        UnitStatus(CurTabIdx + 2).Caption = CStr(SecCountDown(CurTabIdx))
    Next
End Sub

Private Function MakeWhere(CurTabIdx As Integer, CcatabWhere) As String
    Dim MyDate As Date
    Dim X1 As Date
    Dim X2 As Date
    Dim x3 As Date
    Dim strX1 As String
    Dim strX2 As String
    Dim strX3 As String
    Dim tmpText As String
    Dim strWhere As String
    Dim grpWhere As String
    'MyDate = DateAdd("n", -UTC_Diff, Now)
    MyDate = WksUtcDate
    X1 = DateAdd("n", Val(MyTabTrigger01(CurTabIdx)), MyDate)
    X2 = DateAdd("n", Val(MyTabTrigger02(CurTabIdx)), MyDate)
    x3 = DateAdd("n", Val(MyTabTrigger03(CurTabIdx)), MyDate)
    strX1 = Format(X1, "yyyymmddhhmmss")
    strX2 = Format(X2, "yyyymmddhhmmss")
    strX3 = Format(x3, "yyyymmddhhmmss")
    strWhere = MyPageSqlKey(CurTabIdx)
    If MyPageGrpKey(CurTabIdx) <> "" Then
        grpWhere = "WHERE (" & MyPageGrpKey(CurTabIdx) & ") AND"
        strWhere = Replace(strWhere, "WHERE", grpWhere, 1, 1, vbBinaryCompare)
    End If
    tmpText = Trim(FilterText(1).Text)
    If tmpText <> "" Then
        grpWhere = "WHERE (" & tmpText & ") AND"
        strWhere = Replace(strWhere, "WHERE", grpWhere, 1, 1, vbBinaryCompare)
    End If
    tmpText = Trim(FilterText(2).Text)
    If tmpText <> "" Then
        grpWhere = "WHERE (" & tmpText & ") AND"
        strWhere = Replace(strWhere, "WHERE", grpWhere, 1, 1, vbBinaryCompare)
    End If
    
    strWhere = Replace(strWhere, "[TRIGGER_01]", strX1, 1, -1, vbBinaryCompare)
    strWhere = Replace(strWhere, "[TRIGGER_02]", strX2, 1, -1, vbBinaryCompare)
    strWhere = Replace(strWhere, "[TRIGGER_03]", strX3, 1, -1, vbBinaryCompare)
    strWhere = Replace(strWhere, "[HOPO]", MyHopo, 1, -1, vbBinaryCompare)
    
    CcatabWhere = ""
    If SupportCCaTab(CurTabIdx) = True Then
        X1 = DateAdd("n", -240, X1)
        X2 = DateAdd("n", 240, X2)
        strX1 = Format(X1, "yyyymmddhhmmss")
        strX2 = Format(X2, "yyyymmddhhmmss")
        MyCcaVpfr = strX1
        MyCcaVpto = strX2
        
        CcatabWhere = CcatabWhere & "(CKBS<'[TRIGGER_02]' AND CKES>'[TRIGGER_01]')"
        CcatabWhere = CcatabWhere & " AND CKIC<>' '"
        CcatabWhere = Replace(CcatabWhere, "[TRIGGER_01]", strX1, 1, -1, vbBinaryCompare)
        CcatabWhere = Replace(CcatabWhere, "[TRIGGER_02]", strX2, 1, -1, vbBinaryCompare)
    End If
    
    'INFTAB RECORDS LSTU IN LOCAL TIME
    'MyDate = Now
    MyDate = WksLocDate
    X1 = DateAdd("n", Val(MyTabTrigger01(CurTabIdx)), MyDate)
    X2 = DateAdd("n", Val(MyTabTrigger02(CurTabIdx)), MyDate)
    x3 = DateAdd("n", Val(MyTabTrigger03(CurTabIdx)), MyDate)
    strX1 = Format(X1, "yyyymmddhhmmss")
    strX2 = Format(X2, "yyyymmddhhmmss")
    strX3 = Format(x3, "yyyymmddhhmmss")
    InftabWhere = "WHERE APC3='MSG' AND APPL='STAFFMONITOR' AND "
    InftabWhere = InftabWhere & "(LSTU BETWEEN '[TRIGGER_01]' AND '[TRIGGER_02]')"
    InftabWhere = Replace(InftabWhere, "[TRIGGER_01]", strX1, 1, -1, vbBinaryCompare)
    InftabWhere = Replace(InftabWhere, "[TRIGGER_02]", strX2, 1, -1, vbBinaryCompare)
    
    MakeWhere = strWhere
End Function

Private Sub RcvBc_ReceiveBroadcast(ByVal BcNum As String, ByVal DestName As String, ByVal RecvName As String, ByVal Cmd As String, ByVal ObjName As String, ByVal Selection As String, ByVal Fields As String, ByVal Data As String, ByVal twe As String, ByVal tws As String, ByVal OrigName As String)
    Static LastBcNum As String
    Dim BcText As String
    Dim TblBcNum As String
    Dim tmpArea As String
    Dim tmpText As String
    Dim tmpUsid As String
    Dim tmpStat As String
    Dim tmpUrno As String
    Dim tmpLstu As String
    Dim BcMonLine As Long
    Dim TxtColor As Long
    Dim BckColor As Long
    If BcPortConnected Then
        If FormIsLoaded("BcMonitor") Then
            BcMonLine = BcMonitor.BcInput(BcNum, DestName, RecvName, Cmd, ObjName, Selection, Fields, Data, twe, tws, OrigName)
        End If
        ' Note: This program acts as server for other staff pages or interfaces.
        ' After the internal BC handling the changes will be send out to all
        ' connected clients by the subroutine "SendCdiMsg".
        ' The application must wait until each message is send completely and
        ' the only way to get the "SendComplete" information is to look for
        ' the related event on the windows message queue with "DoEvents".
        ' In order to avoid troubles in the workflow while DoEvents is invoked,
        ' all incoming BC's should not be processed immediately.
        ' BC's are stored in a BcSpooler and the evaluation is triggered by
        ' a timer of 5 milliseconds, which is disabled during "DoEvents".
        If BcRcvInd(0).FillColor = &HC000& Then
            BcRcvInd(0).FillColor = &HFF00&
            BcRcvInd(1).FillColor = &HC000&
        Else
            BcRcvInd(1).FillColor = &HFF00&
            BcRcvInd(0).FillColor = &HC000&
        End If
        If Trim(OrigName) = Trim(MyHostName) Then
            If BcNum <> LastBcNum Then
                If Trim(ObjName) = MyDataTable(0) Or Trim(ObjName) = MyDataTable(1) Or Cmd = "CLO" Then
                    BcText = BcNum + Chr(16) + DestName + Chr(16) + RecvName + Chr(16) + Cmd + Chr(16) + ObjName + Chr(16) + Selection + Chr(16) + Fields + Chr(16) + Data + Chr(16) + twe + Chr(16) + tws
                    BcSpoolerTab.InsertTextLine BcText, False
                    If FormIsLoaded("BcMonitor") Then
                        TxtColor = vbBlack
                        BckColor = LightYellow
                        BcMonitor.BcList.SetLineColor BcMonLine, TxtColor, BckColor
                    End If
                    ToggleUpdLed
                End If
                If ((SupportCCaTab(0) = True) Or (SupportCCaTab(1) = True)) And (Trim(ObjName) = "CCATAB") Then
                    TblBcNum = GetItem(twe, 3, ",")
                    BasicData.RefreshCcaOnBc TblBcNum, Cmd, Selection, Fields, Data
                End If
                If Trim(ObjName) = "INFTAB" Then
                    If BcNum <> LastBcNum Then
                        tmpArea = GetFieldValue("AREA", Data, Fields)
                        tmpText = GetFieldValue("TEXT", Data, Fields)
                        tmpLstu = GetFieldValue("LSTU", Data, Fields)
                        tmpUrno = GetFieldValue("URNO", Data, Fields)
                        CheckInfoMessage Cmd, tmpArea, tmpText, tmpLstu, tmpUrno, ""
                    End If
                End If
                If Trim(ObjName) = "SECTAB" Then
                    If BcNum <> LastBcNum Then
                        tmpUsid = GetFieldValue("USID", Data, Fields)
                        tmpStat = GetFieldValue("STAT", Data, Fields)
                        tmpText = GetFieldValue("REMA", Data, Fields)
                        tmpUrno = GetUrnoFromSqlKey(Selection)
                        If (tmpUrno = MyBdpsSecUrno) Or (tmpUsid = MyWksName) Then
                            If tmpStat = "1" Then
                                If MyLoginStatus <> True Then
                                    MyLoginStatus = True
                                    TopInfoPanel(7).Visible = False
                                    Form_Resize
                                    Me.Refresh
                                    HandleOpenPage "0001"
                                End If
                            Else
                                If MyLoginStatus = True Then
                                    MyLoginStatus = False
                                    MyBdpsSecRema = tmpText
                                    TopInfoPanel(7).Visible = True
                                    Form_Resize
                                    Me.Refresh
                                    HandleOpenPage "9998"
                                End If
                            End If
                            If InStr(tmpText, "KIOSKMODE") > 0 Then
                                If InStr(tmpText, " ON ") > 0 Then
                                    If ApplIsInKioskMode <> True Then
                                        ApplIsInKioskMode = True
                                        WritePrivateProfileString "GLOBAL", "APPL_MODE", "KIOSK", StaffPageIniFile
                                        SysFunc.HideMyMouse
                                        SysFunc.HideMyTaskBar
                                        SysFunc.HideMyDesktop
                                        LedPanel(3).Visible = False
                                        GetPosAndSize
                                    End If
                                ElseIf InStr(tmpText, " OFF ") > 0 Then
                                    If ApplIsInKioskMode = True Then
                                        ApplIsInKioskMode = False
                                        WritePrivateProfileString "GLOBAL", "APPL_MODE", "APPL", StaffPageIniFile
                                        SysFunc.ShowMyMouse
                                        SysFunc.ShowMyTaskBar
                                        SysFunc.ShowMyDesktop
                                        LedPanel(3).Visible = True
                                        GetPosAndSize
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
                If Trim(ObjName) = "TIMERS" Then
                    Select Case Cmd
                        Case "TIME"
                            SrvLocTime = GetItem(tws, 2, ",")
                            SrvUtcTime = GetItem(tws, 1, ",")
                            GotLtDiff = False
                        Case "TICO"
                            'SrvLocTime = GetItem(tws, 2, ",")
                            'SrvUtcTime = GetItem(tws, 1, ",")
                        Case Else
                    End Select
                End If
                tmpUrno = GetFieldValue("URNO", Data, Fields)
                HandleOpenFavorite tmpUrno, -1
            End If
            LastBcNum = BcNum
        End If
    End If
End Sub

Private Sub BcSpoolTimer_Timer()
    If (Not CdiServer) And (BcSpoolerTab.GetLineCount > 0) Then
        GetBcFromSpooler
    End If
End Sub

Private Sub GetBcFromSpooler()
    Dim BcEvent As String
    Dim TblBcNum As String
    Dim NewBcNum As Integer
    Dim BcNum As String
    Dim DestName As String
    Dim RecvName As String
    Dim Cmd As String
    Dim Object As String
    Dim Selection As String
    Dim Fields As String
    Dim Data As String
    Dim twe As String
    Dim tws As String
    Dim BcSep As String
    Dim CurTabIdx As Integer
    If BcSpoolerTab.GetLineCount > 0 Then
        BcEvent = BcSpoolerTab.GetLineValues(0)
        BcSpoolerTab.DeleteLine 0
        BcSep = Chr(16)
        BcNum = GetItem(BcEvent, 1, BcSep)
        DestName = GetItem(BcEvent, 2, BcSep)
        RecvName = GetItem(BcEvent, 3, BcSep)
        Cmd = GetItem(BcEvent, 4, BcSep)
        Object = GetItem(BcEvent, 5, BcSep)
        Selection = GetItem(BcEvent, 6, BcSep)
        Fields = GetItem(BcEvent, 7, BcSep)
        Data = GetItem(BcEvent, 8, BcSep)
        twe = GetItem(BcEvent, 9, BcSep)
        tws = GetItem(BcEvent, 10, BcSep)
        'MsgBox BcEvent
        If Cmd = "CLO" Then
            End
        End If
        For CurTabIdx = 0 To 1
            If Trim(Object) = MyDataTable(CurTabIdx) Then
                TblBcNum = ""
                Select Case UseBcNums(CurTabIdx)
                    Case "ARR"
                        TblBcNum = GetItem(twe, 4, ",")
                    Case "DEP"
                        TblBcNum = GetItem(twe, 5, ",")
                    Case "ALL"
                        TblBcNum = GetItem(twe, 3, ",")
                End Select
                NewBcNum = Val(TblBcNum)
                If NewBcNum > 0 Then
                    If NewBcNum <> LastBcNum(CurTabIdx) Then
                        If LastBcNum(CurTabIdx) = 0 Then LastBcNum(CurTabIdx) = NewBcNum - 1
                        LastBcNum(CurTabIdx) = LastBcNum(CurTabIdx) + 1
                        If LastBcNum(CurTabIdx) > 30000 Then LastBcNum(CurTabIdx) = 1
                        If LastBcNum(CurTabIdx) <> NewBcNum Then
                            LostBroadCast(CurTabIdx) = True
                            If MinCountDown(CurTabIdx) > CountDownTime Then
                                MinCountDown(CurTabIdx) = CountDownTime
                            End If
                        End If
                        RefreshOnBc Cmd, Fields, Data, CurTabIdx, "", True
                    End If
                    LastBcNum(CurTabIdx) = NewBcNum
                Else
                    If (LastBcNum(CurTabIdx) > 0) And (NewBcNum <> 0) Then
                        If Abs(NewBcNum) <> LastBcNum(CurTabIdx) Then
                            LastBcNum(CurTabIdx) = Abs(NewBcNum)
                            LostBroadCast(CurTabIdx) = True
                            If MinCountDown(CurTabIdx) > CountDownTime Then
                                MinCountDown(CurTabIdx) = CountDownTime
                            End If
                        End If
                    End If
                End If
                ToggleUpdLed
            End If
        Next
    End If
End Sub

Private Function RefreshOnBc(CurCmd As String, CurFld As String, CurDat As String, CurTabIdx As Integer, TabLineNo As String, CheckCic As Boolean) As Long
    Dim TimeFields As String
    Dim tmpOrig As String
    Dim tmpDest As String
    Dim tmpUrno As String
    Dim MsgTxt As String
    Dim ChgLst As String
    Dim OldLst As String
    Dim NewLst As String
    Dim tmpList As String
    Dim tmpRecIdx As Long
    Dim LineNo As Long
    Dim UpdLineNo As Long
    Dim tmpRecDat As String
    Dim newRecDat As String
    Dim OutRecDat As String
    Dim CurItm As Integer
    Dim MaxItm As Integer
    Dim FldNam As String
    Dim FldIdx As Integer
    Dim FldDat As String
    Dim ColDat As String
    Dim ColLst As String
    Dim OutMsg As String
    Dim CurFlno As String
    Dim CurUrno As String
    Dim CurCkif As String
    Dim CurCkit As String
    Dim CurStod As String
    Dim ChkUrno As String
    Dim tmpLinNbr As String
    Dim CcaRecDat As String
    Dim CurLinNbr As String
    Dim CCaCnt As Long
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim CurBackColor As Long
    Dim IsValidBc As Boolean
    Dim UpdateIt As Boolean
    
    UpdateIt = False
    UpdLineNo = -1
    tmpList = ""
    If MyUrnoIdx(CurTabIdx) >= 0 Then IsValidBc = True
    If IsValidBc Then
        tmpUrno = GetFieldValue("URNO", CurDat, CurFld)
        If tmpUrno <> "" Then
            tmpList = DataTab(CurTabIdx).GetLinesByColumnValue(MyUrnoIdx(CurTabIdx), tmpUrno, 0)
            If tmpList <> "" Then
                'We have it in the list
                LineNo = Val(tmpList)
                tmpRecDat = DataTab(CurTabIdx).GetLineValues(LineNo)
                newRecDat = ""
                'Now look what's to do
                Select Case Trim(CurCmd)
                    Case "UFR"
                        ColLst = ""
                        ChgLst = ""
                        MaxItm = ItemCount(MyPageFields(CurTabIdx), ",")
                        For CurItm = 1 To MaxItm
                            FldNam = GetItem(MyPageFields(CurTabIdx), CurItm, ",")
                            FldIdx = GetItemNo(CurFld, FldNam)
                            If FldIdx > 0 Then
                                FldDat = GetItem(CurDat, FldIdx, ",")
                                If FldNam = "FLNO" Then
                                    'FldDat = Left(FldDat, 7) + Mid(FldDat, 9, 1)
                                ElseIf FldNam = "MTOW" Then
                                    If TabLineNo = "" Then
                                        FldDat = Trim(GetItem(FldDat, 1, "."))
                                        FldDat = Right("00000" + FldDat, 5)
                                    End If
                                ElseIf InStr(MyTimeFields(CurTabIdx), FldNam) > 0 Then
                                    'FldDat = GetTimeFromCedaDate(FldDat)
                                End If
                                ColDat = GetItem(tmpRecDat, CurItm, ",")
                                If ColDat <> FldDat Then
                                    UpdateIt = True
                                    ChgLst = ChgLst & FldNam & ","
                                    OldLst = OldLst & ColDat & ","
                                    NewLst = NewLst & FldDat & ","
                                    ColLst = ColLst & Trim(Str(CurItm - 1)) & ","
                                End If
                            Else
                                FldDat = GetItem(tmpRecDat, CurItm, ",")
                            End If
                            newRecDat = newRecDat & FldDat & ","
                        Next
                    Case "IFR"
                    Case "ISF"
                    Case "DFR"
                        'DataTab(CurTabIdx).DeleteLine LineNo
                        'RefreshColoredLines "", LineNo, "", vbRed, True, CurTabIdx
                        'UpdateIt = True
                    Case "UPS"
                    Case "UPJ"
                    Case Else
                        'Nothing to do
                End Select
                UpdLineNo = LineNo
                If UpdateIt Then
                    lblRefresh(0) = "UPDATE"
                    lblRefresh(1) = Format(Now, "hh:mm:ss")
                    If newRecDat <> "" Then
                        OutRecDat = newRecDat
                        If UpdateOnReload = True Then
                            If TabLineNo <> "" Then
                                newRecDat = newRecDat + "UPD," + TabLineNo + ",UPD"
                            Else
                                newRecDat = newRecDat + "UPD," + GetItem(tmpRecDat, MaxItm + 2, ",")
                            End If
                        End If
                        DataTab(CurTabIdx).UpdateTextLine LineNo, newRecDat, True
                        RefreshColoredLines tmpUrno, LineNo, ColLst, vbGreen, True, CurTabIdx
                        If (SendUpdates) And (CdiIdx > 0) Then
                            OutMsg = MyPageFields(CurTabIdx) & vbNewLine & OutRecDat & vbNewLine & ChgLst & vbNewLine & OldLst & vbNewLine & NewLst
                            SendCdiMsg OutMsg, -1, CurTabIdx
                        End If
                        If (CheckCic = True) And (SupportCCaTab(CurTabIdx)) Then
                            CurUrno = GetFieldValue("URNO", tmpRecDat, MyPageFields(CurTabIdx))
                            CurFlno = GetFieldValue("FLNO", tmpRecDat, MyPageFields(CurTabIdx))
                            CurCkif = GetFieldValue("CKIF", tmpRecDat, MyPageFields(CurTabIdx))
                            CurCkit = GetFieldValue("CKIT", tmpRecDat, MyPageFields(CurTabIdx))
                            CurStod = GetFieldValue("STOD", tmpRecDat, MyPageFields(CurTabIdx))
                            CCaCnt = 0
                            'If (CurCkif <> "") Then CCaCnt = BasicData.LookUpCca(CurUrno, CurFlno, CurStod, CurCkif, CurCkit, False)
                            If (CurCkif <> "") Then CCaCnt = BasicData.LookUpCca(CurUrno, CurFlno, CurStod, CurCkif, CurCkit, True)
                            If CCaCnt > 0 Then
                                DataTab(CurTabIdx).SetDecorationObject LineNo, MyCkifIdx(CurTabIdx), "CIC1"
                                DataTab(CurTabIdx).SetDecorationObject LineNo, MyCkitIdx(CurTabIdx), "CIC2"
                                CurBackColor = Val(DataTab(CurTabIdx).GetLineTag(LineNo))
                                CurLinNbr = Right("000000" + CStr(LineNo), 6)
                                MaxLine = CCaCnt - 1
                                For CurLine = 0 To MaxLine
                                    CcaRecDat = EmptyLine(CurTabIdx)
                                    tmpUrno = CurUrno & "_" & CStr(CurLine + 1)
                                    tmpLinNbr = CurLinNbr & "_" & CStr(CurLine + 1)
                                    FldDat = BasicData.CicGrp(1).GetColumnValue(CurLine, 1)
                                    SetItem CcaRecDat, (MyCkifIdx(CurTabIdx) + 1), ",", FldDat
                                    FldDat = BasicData.CicGrp(1).GetColumnValue(CurLine, 2)
                                    SetItem CcaRecDat, (MyCkitIdx(CurTabIdx) + 1), ",", FldDat
                                    SetItem CcaRecDat, (MyUrnoIdx(CurTabIdx) + 1), ",", tmpUrno
                                    CcaRecDat = CcaRecDat + ",UPD," + tmpLinNbr + ",,"
                                    LineNo = LineNo + 1
                                    ChkUrno = DataTab(CurTabIdx).GetColumnValue(LineNo, MyUrnoIdx(CurTabIdx))
                                    ChkUrno = GetItem(ChkUrno, 1, "_")
                                    If ChkUrno = CurUrno Then
                                        DataTab(CurTabIdx).UpdateTextLine LineNo, CcaRecDat, True
                                    Else
                                        DataTab(CurTabIdx).InsertTextLineAt LineNo, CcaRecDat, True
                                        DataTab(CurTabIdx).SetLineColor LineNo, LineTextColor, CurBackColor
                                        DataTab(CurTabIdx).SetLineTag LineNo, CStr(CurBackColor)
                                    End If
                                    DataTab(CurTabIdx).SetDecorationObject LineNo, MyCkifIdx(CurTabIdx), "CIC3"
                                    DataTab(CurTabIdx).SetDecorationObject LineNo, MyCkitIdx(CurTabIdx), "CIC4"
                                Next
                                DataTab(CurTabIdx).SetDecorationObject LineNo, MyCkifIdx(CurTabIdx), "CIC5"
                                DataTab(CurTabIdx).SetDecorationObject LineNo, MyCkitIdx(CurTabIdx), "CIC6"
                                LineNo = LineNo + 1
                                ChkUrno = DataTab(CurTabIdx).GetColumnValue(LineNo, MyUrnoIdx(CurTabIdx))
                                ChkUrno = GetItem(ChkUrno, 1, "_")
                                While ChkUrno = CurUrno
                                    DataTab(CurTabIdx).DeleteLine LineNo
                                    LineNo = LineNo + 1
                                    ChkUrno = DataTab(CurTabIdx).GetColumnValue(LineNo, MyUrnoIdx(CurTabIdx))
                                    ChkUrno = GetItem(ChkUrno, 1, "_")
                                Wend
                            End If
                        End If
                    End If
                Else
                    If TabLineNo <> "" Then
                        'Reload is running
                        'DataTab(CurTabIdx).SetColumnValue LineNo, MyPageFieldCount(CurTabIdx), "REFRESH"
                        DataTab(CurTabIdx).SetColumnValue LineNo, MyPageFieldCount(CurTabIdx) + 1, TabLineNo
                        DataTab(CurTabIdx).SetColumnValue LineNo, MyPageFieldCount(CurTabIdx) + 2, "USE"
                    End If
                End If
            Else
                If TabLineNo <> "" Then
                    DataTab(CurTabIdx).InsertTextLine CurDat, False
                    OutMsg = MyPageFields(CurTabIdx) & vbNewLine & CurDat & vbNewLine & "" & vbNewLine & "" & vbNewLine & ""
                    SendCdiMsg OutMsg, -1, CurTabIdx
                    UpdLineNo = DataTab(CurTabIdx).GetLineCount - 1
                End If
            End If
        End If
    End If
    RefreshOnBc = UpdLineNo
End Function

Private Sub RefreshColoredLines(ActUrno As String, LineNo As Long, ColLst As String, TextColor As Long, DoRefresh As Boolean, CurTabIdx As Integer)
    Dim tmpUrno As String
    Dim tmpVal As String
    Dim TmpCol As String
    Dim NewList As String
    Dim tmpList As String
    Dim TimerData As String
    Dim TimerKey As String
    Dim TimerList As String
    Dim TimerLine As Long
    Dim TimerUrno As String
    Dim UrnoLine As String
    Dim TimerCol As String
    Dim TimerType As String
    Dim ilCnt As Integer
    Dim ItmNbr As Integer
    Dim CurLine As Long
    Dim CurCol As Long
    Dim MaxLine As Long
    Dim UseColor As Long
    Dim CurStat As Long
    Dim CurColor As Long
    Dim IsRefreshed As Boolean
    Dim ActTabIdx As Integer
    If LineNo >= 0 Then
        If ActUrno <> "" Then
            CurColor = Val(DataTab(CurTabIdx).GetLineTag(LineNo))
            DataTab(CurTabIdx).SetLineColor LineNo, TextColor, CurColor
            ItmNbr = 0
            Do
                ItmNbr = ItmNbr + 1
                TmpCol = GetItem(ColLst, ItmNbr, ",")
                If TmpCol <> "" Then
                    CurCol = Val(TmpCol)
                    DataTab(CurTabIdx).SetCellProperty LineNo, CurCol, "UpdCell"
                End If
            Loop While TmpCol <> ""
            CurStat = DataTab(CurTabIdx).GetLineStatusValue(LineNo)
            If CurStat < 1 Then UpdateCount = UpdateCount + 1
            DataTab(CurTabIdx).SetLineStatusValue LineNo, 1
            DataTab(CurTabIdx).TimerSetValue LineNo, KeepUpdate
            DataTab(CurTabIdx).RedrawTab
            ToggleUpdLed
        End If
    End If
End Sub
Private Sub ToggleUpdLed()
    If UpdateCount > 0 Then
        If BcUpdInd(0).FillColor = &HFF00& Then
            BcUpdInd(0).FillColor = vbYellow
            BcUpdInd(1).FillColor = &HFF00&
        Else
            BcUpdInd(1).FillColor = vbYellow
            BcUpdInd(0).FillColor = &HFF00&
        End If
    Else
        If BcUpdInd(0).FillColor = &HC000& Then
            BcUpdInd(0).FillColor = &HFF00&
            BcUpdInd(1).FillColor = &HC000&
        Else
            BcUpdInd(1).FillColor = &HFF00&
            BcUpdInd(0).FillColor = &HC000&
        End If
    End If
End Sub
Private Function GetTimeFromCedaDate(CedaDate As String) As String
    Dim Result As String
    Result = ""
    If Trim(CedaDate) <> "" Then
        Result = Mid(CedaDate, 9, 2) + ":" + Mid(CedaDate, 11, 2)
    End If
    GetTimeFromCedaDate = Result
End Function

Public Function GetUTCDifference() As Integer
    Dim ret As Integer
    Dim buf As String
    Dim strArr() As String
    Dim strUtcArr() As String
    Dim i As Integer, j As Integer
    Dim count As Integer
    Dim istrRet As Integer
    Dim CurrStr As String
    Dim tmpLine As String
    Dim tmpTime As Date
    Dim tmpDiff As Integer
    If ApplIsInKioskMode Then SysFunc.ShowMyMouse
    Screen.MousePointer = 11
    If WorkOffLine Then CedaIsConnected = False
    OpenConnection True
    GetUTCDifference = 0
    PrepareMessage "INIT"
    If CedaIsConnected = True Then
        If Ufis.CallServer("GFR", "", "", "", "[CONFIG]", "") = 0 Then
            buf = Ufis.GetDataBuffer(True)
        End If
        GetKeyItem tmpLine, buf, "UTCD,", vbLf
        tmpDiff = Val(GetItem(tmpLine, 1, ","))
        GetKeyItem tmpLine, buf, "SRVT,", vbLf
        SrvLocTime = GetItem(tmpLine, 1, ",")
        SrvUtcTime = GetItem(tmpLine, 3, ",")
        GotLtDiff = False
    Else
        tmpDiff = Val(GetIniEntry(StaffPageIniFile, "GLOBAL", "", "UTC_TIME_DIFF", "0"))
    End If
    GotUtcDiff = True
    UTC_Diff = tmpDiff
    tmrSeconds_Timer
    
    'Now that we know the current time we'll check BDPS SEC
    MainTitle.Caption = "LOGIN"
    MyLoginStatus = True
    If CedaIsConnected Then
        MyLoginStatus = BdpsCheck
    End If
    
    'We'll register for broadcasts even if the login didn't work.
    'It might be that someone grants the access with BdpsSec
    'And then we can start in full active mode
    
    ConnectBcPort
    PrepareMessage "LOGIN"
    If CedaIsConnected = True Then
        ret = Ufis.CallServer("SBC", "STAFF/KEEP", "ALL FIELDS", MyTcpIpAdr, HexTcpAdr, "")
    End If
    Screen.MousePointer = 0
    If ApplIsInKioskMode Then SysFunc.HideMyMouse
    GetUTCDifference = tmpDiff
End Function

Private Sub OpenConnection(ForNetin As Boolean)
    Dim ret As Boolean
    If WorkOffLine Then CedaIsConnected = False
    ret = CedaIsConnected
    CedaCon(0).Visible = True
    CedaCon(0).FillColor = NormalYellow
    If ForNetin Then
        If MyHostName <> "LOCAL" Then
            If CedaIsConnected = False Then
                'MsgLogging UCase(App.EXEName) & " CONNECTING TO " & UCase(MyHostName)
                ret = Ufis.InitCom(MyHostName, "CEDA")
            End If
        End If
    Else
        ret = Not WorkOffLine
    End If
    CedaIsConnected = ret
    If CedaIsConnected Then
        CedaCon(0).FillColor = LightGreen
    Else
        CedaCon(0).FillColor = vbRed
    End If
End Sub
Private Sub PrepareMessage(ConText As String)
    Ufis.HomeAirport = MyHopo
    Ufis.TableExt = MyTblExt
    Ufis.Module = MyApplVersion
    Ufis.twe = ""
    Ufis.UserName = MyUserName
    Ufis.tws = MyShortName + " " + ConText
    Ufis.SetCedaPerameters MyUserName, MyHopo, MyTblExt
End Sub
Private Sub CloseConnection(ForNetin As Boolean)
    If (ForNetin) Or (CedaIsConnected) Then
        If MyHostName <> "LOCAL" Then
            If CedaIsConnected = True Then Ufis.CleanupCom
        End If
    End If
    CedaIsConnected = False
    CedaCon(0).Visible = CedaIsConnected
End Sub
Private Sub ConnectBcPort()
    If MyHostName <> "LOCAL" Then
        RcvBc.InitComm
        BcPortConnected = True
    End If
End Sub

Public Sub RemotePort_Close(Index As Integer)
    If RemotePort(Index).state <> sckClosed Then RemotePort(Index).Close
    If RemotePort(0).state <> sckClosed Then RemotePort(0).Close
    If Index > 0 Then Unload RemotePort(Index)
    RemoteIsOpen = False
    RemoteIsBusy = False
    RemoteIdx = -1
    If RemoteControl Then
        If ShowRemote.optConnect(0).Value = True Then
            ShowRemote.optConnect(1).Value = True
        End If
    Else
        If Not ShutDownRequested Then
            OpenRemotePort
        End If
    End If
End Sub

Private Sub RemotePort_Connect(Index As Integer)
    Dim TestString As String
    'MsgBox "Connected"
    RemoteIdx = 0
    RemoteCon(0).Visible = True
    RemoteCon(1).Visible = True
    RemoteCon(0).Tag = "1"
    RemoteIsBusy = True
    If RemoteControl Then
        ShowRemote.optConnect(0).Caption = "Connected"
        ShowRemote.optConnect(0).BackColor = LightGreen
        ShowRemote.optConnect(1).Enabled = True
        ShowRemote.chkCCO.Enabled = True
        ShowRemote.ToolFrame.Enabled = True
    End If
    TestString = "{=TOT=}      174{=CMD=}RT{=IDN=}ROS_INIT_LOAD{=TBL=}ACTTAB{=EXT=}TAB{=HOPO=}ATH{=FLD=}ACT3,ACT5,ACTI,CDAT{=WHE=}WHERE URNO > 0{=USR=}MWO{=WKS=}wks104{=SEPA=}" & vbLf & "{=APP=}Rostering"
    RemotePort(Index).SendData TestString
End Sub

Private Sub RemotePort_ConnectionRequest(Index As Integer, ByVal requestID As Long)
    Dim OutMsg As String
    RemoteCon(0).Visible = True
    RemoteCon(1).Visible = True
    RemoteCon(0).Tag = "1"
    If RemotePort(0).state <> sckClosed Then RemotePort(0).Close
    RemoteIdx = 1
    Load RemotePort(RemoteIdx)
    RemotePort(RemoteIdx).Accept requestID
    If TcpLed(0).Visible Then
        TcpLed(1).Visible = True
        TcpLed(0).Visible = False
        TcpLed(2).Visible = True
        TcpLed(3).Visible = False
    Else
        TcpLed(0).Visible = True
        TcpLed(1).Visible = False
        TcpLed(3).Visible = True
        TcpLed(2).Visible = False
    End If
    OutMsg = ""
    OutMsg = OutMsg & "CONX: " & RemotePort(0).LocalIP & vbNewLine
    OutMsg = OutMsg & "UNIT: " & UnitLabel(1).Caption & " (PID" & CStr(MyApplProcessID) & ")" & vbNewLine
    OutMsg = OutMsg & "WKSN: " & UnitLabel(3).Caption & vbNewLine
    OutMsg = OutMsg & "LOCN: " & UnitLabel(5).Caption & vbNewLine
    OutMsg = OutMsg & "PAGE: " & PageCode(1).Caption & vbNewLine
    OutMsg = OutMsg & "MODE: " & MainTitle.Caption & vbNewLine
    OutMsg = OutMsg & "TIME: " & lblNow.Caption
    SendRemoteMsg OutMsg
End Sub

Private Sub RemotePort_DataArrival(Index As Integer, ByVal bytesTotal As Long)
    Static RemMsgRest As String
    Dim RcvData As String
    Dim rcvText As String
    Dim RcvMsg As String
    Dim tmpLen As String
    Dim MsgLen As Integer
    Dim txtLen As Integer
    Dim msgPos As Integer
    Dim gotMsg As Boolean
    RemotePort(Index).GetData RcvData, vbString
    rcvText = RemMsgRest & RcvData
    RemMsgRest = ""
    'MsgBox "Received Message:" & vbNewLine & rcvText
    'Text1.Text = RcvData
    txtLen = Len(rcvText)
    gotMsg = True
    msgPos = 1
    While (msgPos < txtLen) And (gotMsg = True)
        gotMsg = False
        If (msgPos + 8) < txtLen Then
            tmpLen = Mid(rcvText, msgPos, 8)
            MsgLen = Val(tmpLen)
        End If
        If (msgPos + 7 + MsgLen) <= txtLen Then
            msgPos = msgPos + 8
            RcvMsg = Mid(rcvText, msgPos, MsgLen)
            msgPos = msgPos + MsgLen
            RunRemoteMsg RcvMsg
            gotMsg = True
        End If
    Wend
    If gotMsg = False Then
        RemMsgRest = Mid(rcvText, msgPos)
    End If
End Sub

Private Sub RemotePort_Error(Index As Integer, ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)
    'MsgBox "Error: " & Number & vbLf & Description
    RemotePort_Close Index
End Sub

Private Sub RemotePort_SendComplete(Index As Integer)
    'Text1.Text = Text1.Text & vbNewLine & "Ready."
End Sub

Private Sub RemotePort_SendProgress(Index As Integer, ByVal bytesSent As Long, ByVal bytesRemaining As Long)
    'Text1.Text = Str(bytesSent) & " /" & Str(bytesRemaining)
End Sub

Public Sub OpenRemotePort()
    On Error Resume Next
    If (Not RemoteIsOpen) And (Not RemoteIsBusy) Then
        RemotePort(0).LocalPort = "4397"
        RemotePort(0).Listen
        RemoteIdx = 0
        'MsgBox "Listening"
        RemoteIsOpen = True
    End If
End Sub

Private Sub RunRemoteMsg(RcvTcpMsg As String)
    Dim RcvTsk As String
    Dim RcvMsg As String
    Dim RcvCmd As String
    Dim RunCmd As String
    Dim RunIdx As String
    Dim OutMsg As String
    Dim tmpdat As String
    RcvTsk = GetItem(RcvTcpMsg, 1, "||")
    RcvMsg = GetItem(RcvTcpMsg, 2, "||")
    RcvCmd = GetItem(RcvTsk, 1, ",")
    RunCmd = GetItem(RcvTsk, 2, ",")
    RunIdx = GetItem(RcvTsk, 3, ",")
    Select Case RcvCmd
        Case "CONNEX"
            Select Case RunCmd
                Case "HOST"
                    ShowRemote.Location.Text = RcvMsg
                Case Else
            End Select
        
        Case "SEND"
            Select Case RunCmd
                Case "CLOCK"
                    OutMsg = "ANSW,CLOCK," & RunIdx & "||" & Now
                    SendRemoteMsg OutMsg
                Case "TIMES"
                    tmpdat = lblNow.Caption & " / UTC=" & lblUTC.Caption & ","
                    tmpdat = tmpdat & lblRefresh(0).Caption & "," & lblRefresh(1).Caption
                    OutMsg = "ANSW,TIMES," & RunIdx & "||" & tmpdat
                    SendRemoteMsg OutMsg
                Case "BUTTONS"
                    tmpdat = ""
                    tmpdat = tmpdat & MainTitle.Caption & vbLf
                    tmpdat = tmpdat & PageTitle.Caption & vbLf
                    tmpdat = tmpdat & lblNow.Caption & vbLf
                    tmpdat = tmpdat & lblUTC.Caption & vbLf
                    tmpdat = tmpdat & lblRefresh(0).Caption & vbLf
                    tmpdat = tmpdat & lblRefresh(1).Caption & vbLf
                    tmpdat = tmpdat & chkLeftAction.Caption & vbLf
                    tmpdat = tmpdat & chkRightAction.Caption & vbLf
                    tmpdat = tmpdat & btnExit.Caption & vbLf
                    tmpdat = tmpdat & btnPrev.Caption & vbLf
                    tmpdat = tmpdat & btnNext.Caption & vbLf
                    tmpdat = tmpdat & MyFieldLen & vbLf
                    tmpdat = tmpdat & DataTab(0).GetHeaderText & vbLf
                    OutMsg = "ANSW,BUTTONS," & RunIdx & "||" & tmpdat
                    SendRemoteMsg OutMsg
                Case "DATA"
                    tmpdat = GetTabDataBuffer()
                    OutMsg = "ANSW,DATA," & RunIdx & "||" & tmpdat
                    SendRemoteMsg OutMsg
                
                Case Else
            End Select
        Case "ANSW"
            Select Case RunCmd
                Case "CLOCK"
                    ShowRemote.RemoteData.Caption = RcvMsg
                Case "TIMES"
                    'ShowRemote.RemoteData.Caption = RcvMsg
                    lblNow.Caption = GetItem(RcvMsg, 1, "/ UTC=")
                    tmpdat = GetItem(RcvMsg, 2, "/ UTC=")
                    lblUTC.Caption = GetItem(tmpdat, 1, ",")
                    lblRefresh(0).Caption = GetItem(tmpdat, 2, ",")
                    lblRefresh(1).Caption = GetItem(tmpdat, 3, ",")
                Case "BUTTONS"
                    MainTitle.Caption = GetItem(RcvMsg, 1, vbLf)
                    PageTitle.Caption = GetItem(RcvMsg, 2, vbLf)
                    lblNow.Caption = GetItem(RcvMsg, 3, vbLf)
                    lblUTC.Caption = GetItem(RcvMsg, 4, vbLf)
                    lblRefresh(0).Caption = GetItem(RcvMsg, 5, vbLf)
                    lblRefresh(1).Caption = GetItem(RcvMsg, 6, vbLf)
                    chkLeftAction.Caption = GetItem(RcvMsg, 7, vbLf)
                    chkRightAction.Caption = GetItem(RcvMsg, 8, vbLf)
                    btnExit.Caption = GetItem(RcvMsg, 9, vbLf)
                    btnPrev.Caption = GetItem(RcvMsg, 10, vbLf)
                    btnNext.Caption = GetItem(RcvMsg, 11, vbLf)
                    MyFieldLen = GetItem(RcvMsg, 12, vbLf)
                    DataTab(0).HeaderLengthString = MyFieldLen
                    DataTab(0).HeaderString = GetItem(RcvMsg, 13, vbLf)
                Case "DATA"
                    DataTab(0).ResetContent
                    DataTab(0).InsertBuffer RcvMsg, vbLf
                Case Else
            End Select
        Case "CCO"
            If Not RemoteControl Then
                If RefreshInd(0).FillColor <> vbRed Then
                    RefreshInd(0).FillColor = vbRed
                    RefreshInd(1).FillColor = RemoteColor(0)
                Else
                    RefreshInd(0).FillColor = RemoteColor(0)
                    RefreshInd(1).FillColor = vbRed
                End If
                SendRemoteMsg "CCA"
                tmpdat = lblNow.Caption & " / UTC=" & lblUTC.Caption & ","
                tmpdat = tmpdat & lblRefresh(0).Caption & "," & lblRefresh(1).Caption
                OutMsg = "ANSW,TIMES,-1||" & tmpdat
                SendRemoteMsg OutMsg
            End If
        Case "CCA"
            If RemoteControl Then
                ShowRemote.chkRemote.BackColor = LightGreen
            End If
        Case "PUSH"
            ButtonPushedInternal = True
            Select Case RunCmd
                Case "MAIN_TITLE"
                    MainTitle.Value = 1
                Case "LEFT"
                    chkLeftAction.Value = 1
                Case "RIGHT"
                    chkRightAction.Value = 1
                Case "EXIT"
                    btnExit_Click
                Case "PREV"
                    btnPrev_Click
                Case "NEXT"
                    btnNext_Click
                Case Else
            End Select
            ButtonPushedInternal = False
            OutMsg = "PUSHED," & RunCmd & "," & RunIdx & "||"
            SendRemoteMsg OutMsg
        Case "PUSHED"
            ShowRemote.optTool(0).Value = True
        Case "STARTED"
            ShowRemote.StartApp.Value = 0
            ShowRemote.chkRemoteStart.Value = 0
            ShowRemote.optConnect(0).Value = True
        Case Else
            'MsgBox RcvMsg
    End Select
End Sub

Public Sub SendRemoteMsg(OutText As String)
    Dim tmpLen As String
    Dim totLen As Integer
    totLen = Len(OutText)
    tmpLen = Trim(Str(totLen))
    tmpLen = Right(("00000000") & tmpLen, 8)
    'MsgBox tmpLen & OutText
    RemotePort(RemoteIdx).SendData tmpLen & OutText
End Sub

Private Function GetTabDataBuffer() As String
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim Result As String
    Result = ""
    MaxLine = DataTab(0).GetLineCount - 1
    For CurLine = 0 To MaxLine
        Result = Result & DataTab(0).GetLineValues(CurLine) & vbLf
    Next
    GetTabDataBuffer = Result
End Function

Private Sub WriteDataToFile(CurTabIdx As Integer)
    Dim WrkFileName As String
    Dim CurFileName As String
    Dim CurLin As Long
    Dim MaxLin As Long
    Dim tmpRec As String
    WrkFileName = SavePath + GetTimeStamp(0) + ".txt"
    CurFileName = SavePath + MyCedaTitle(CurTabIdx) + GetTimeStamp(0) + ".txt"
    Open WrkFileName For Output As #1
        tmpRec = "EXPORTED: " + MyCedaTitle(CurTabIdx) + " / " + GetTimeStamp(0)
        Print #1, tmpRec
        Print #1, MyPageFields(CurTabIdx)
        MaxLin = DataTab(CurTabIdx).GetLineCount - 1
        For CurLin = 0 To MaxLin
            tmpRec = DataTab(CurTabIdx).GetLineValues(CurLin)
            Print #1, tmpRec
        Next
    Close #1
    Name WrkFileName As CurFileName
End Sub

Private Sub CdiPort_ConnectionRequest(Index As Integer, ByVal requestID As Long)
    Dim OutMsg As String
    If (Index = 0) And (Not CdiServer) Then
        'CdiIdx = CdiIdx + 1
        CdiIdx = GetFreeCdiIdx()
        Load CdiPort(CdiIdx)
        CdiPort(CdiIdx).LocalPort = 0
        CdiPort(CdiIdx).Accept requestID
        CdiPortsChanged = True
        SetServerCaption
        'MsgLogging UCase(App.EXEName) & " CONNECTED: " & CdiPort(Index).RemoteHostIP
        If CdiPageFields(0) = "" Then
            OutMsg = "CONNEX,HOST,-1||" & CdiPort(0).LocalIP & " / " & CdiPort(0).LocalHostName & " / IDX" & Str(CdiIdx)
            SendCdiMsg OutMsg, CdiIdx, 0
        End If
        If CdiSendOnConnex = True Then
            CdiSendAllRecs 0, CdiIdx
            CdiSendAllRecs 1, CdiIdx
        End If
    End If
End Sub

Private Sub CdiPort_Close(Index As Integer)
    'MsgLogging UCase(App.EXEName) & " CLOSED BY: " & CdiPort(Index).RemoteHostIP
    If CdiPort(Index).state <> sckClosed Then CdiPort(Index).Close
    If Index > 0 Then Unload CdiPort(Index)
    If CdiServer And Index = CdiActIdx Then CdiPacketComplete = True 'Stop sending
    SetServerCaption
    CdiPortsChanged = True
End Sub

Private Sub SetServerCaption()
    Dim iCnt As Integer
    iCnt = CdiPort.count - 1
    If iCnt > 1 Then
        btnCdiServer.Caption = Trim(Str(iCnt)) & " CLIENTS"
    ElseIf iCnt = 1 Then
        btnCdiServer.Caption = "1 CLIENT"
    Else
        btnCdiServer.Caption = "LISTENING"
    End If
    'MsgLogging UCase(App.EXEName) & " " & btnCdiServer.Caption
    If btnCdiServer.Value = 1 Then
        CdiList.InitMyList
    End If
End Sub

Private Sub CdiPort_DataArrival(Index As Integer, ByVal bytesTotal As Long)
    Dim RcvData As String
    Dim rcvText As String
    Dim RcvMsg As String
    Dim tmpLen As String
    Dim MsgLen As Integer
    Dim txtLen As Integer
    Dim msgPos As Integer
    Dim gotMsg As Boolean
    CdiPort(Index).GetData RcvData, vbString
    rcvText = CdiMsgRest(Index) & RcvData
    'MsgBox Index & vbNewLine & rcvText
    If Not CdiServer Then
        CdiMsgRest(Index) = ""
        'Text1.Text = RcvData
        txtLen = Len(rcvText)
        gotMsg = True
        msgPos = 1
        While (msgPos < txtLen) And (gotMsg = True)
            gotMsg = False
            If (msgPos + 8) < txtLen Then
                tmpLen = Mid(rcvText, msgPos, 8)
                MsgLen = Val(tmpLen)
            End If
            If (msgPos + 7 + MsgLen) <= txtLen Then
                msgPos = msgPos + 8
                RcvMsg = Mid(rcvText, msgPos, MsgLen)
                msgPos = msgPos + MsgLen
                'Hier muessen the events gespoolt werden !!!
                RunCdiMsg RcvMsg, Index
                gotMsg = True
            End If
        Wend
        If (gotMsg = False) And (msgPos > 0) Then
            CdiMsgRest(Index) = Mid(rcvText, msgPos)
        End If
    Else
        CdiMsgRest(Index) = rcvText
        'Must set a flag that a message was buffered !!
        MsgBox "Message buffered."
    End If
End Sub

Private Sub RunCdiMsg(RcvMsg As String, Index As Integer)
    Dim CdiCmd As String
    Dim CdiMsg As String
    Dim FirstChar As String
    Dim LastChar As String
    Dim tmpData As String
    FirstChar = Left(RcvMsg, 1)
    LastChar = Right(RcvMsg, 1)
    CdiMsg = Mid(RcvMsg, 2, Len(RcvMsg) - 2)
    CdiCmd = GetItem(CdiMsg, 1, ",")
    Select Case CdiCmd
        Case "MYN"  'My Name
            tmpData = GetItem(CdiMsg, 2, ",")
            'CdiPort(Index).Tag = tmpData
            CdiPort(Index).Tag = CdiMsg
            'MsgBox CdiMsg
        Case Else
            'MsgBox "Client sends garbage!"
    End Select
End Sub

Private Sub CdiPort_Error(Index As Integer, ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)
    'MsgBox "Error: " & Number & vbLf & Description
    CdiPort_Close Index
    'CdiControl.BorderColor = vbRed
End Sub

Private Sub CdiPort_SendComplete(Index As Integer)
    CdiPacketComplete = True
End Sub

Private Sub CdiPort_SendProgress(Index As Integer, ByVal bytesSent As Long, ByVal bytesRemaining As Long)
    'Text1.Text = Str(bytesSent) & " /" & Str(bytesRemaining)
End Sub

Private Sub OpenCdiPort()
    On Error Resume Next
    If Not RemoteControl Then
        CdiPort(0).LocalPort = "4395"
        CdiPort(0).Listen
        CdiIdx = 0
        'CdiControl.Visible = True
        'CdiControl.BorderColor = LightYellow
        btnCdiServer.Visible = True
    End If
End Sub

Public Sub SendCdiMsg(OutText As String, TcpIdx As Integer, CurTabIdx As Integer)
    Dim InternalError As Boolean
    Dim txtLen As String
    Dim totLen As Integer
    Dim MsgTxt(2) As String
    Dim cdiObj As Winsock
    Dim TotalWait As Single
    Dim CurIdx As Integer
    Dim CurMsg As Integer
    Dim ActFldLst As String
    Dim ActDatLst As String
    Dim ChgFldLst As String
    Dim OldDatLst As String
    Dim NewDatLst As String
    Dim FldNam As String
    Dim FldDat As String
    Dim OldDat As String
    Dim FldLen As Integer
    Dim CurItm As Integer
    Dim ItmTxt As String
    Dim OldTxt As String
    Dim ItmLen As Integer
    If CdiPort.count > 1 Then
        MsgTxt(0) = ""
        MsgTxt(1) = ""
        If CdiPageFields(CurTabIdx) <> "" Then
            ActFldLst = GetItem(OutText, 1, vbNewLine)
            ActDatLst = GetItem(OutText, 2, vbNewLine)
            CurItm = 0
            ItmTxt = "START"
            Do
                CurItm = CurItm + 1
                FldNam = GetItem(CdiTriggers(CurTabIdx), CurItm, ",")
                If FldNam <> "" Then
                    ItmTxt = GetFieldValue(FldNam, ActDatLst, ActFldLst)
                End If
            Loop While (FldNam <> "") And (ItmTxt = "")
            If ItmTxt <> "" Then
                ChgFldLst = GetItem(OutText, 3, vbNewLine)
                OldDatLst = GetItem(OutText, 4, vbNewLine)
                NewDatLst = GetItem(OutText, 5, vbNewLine)
                CurItm = 0
                Do
                    CurItm = CurItm + 1
                    FldNam = GetItem(CdiPageFields(CurTabIdx), CurItm, ",")
                    If FldNam <> "" Then
                        FldLen = Val(GetItem(CdiFieldLength(CurTabIdx), CurItm, ","))
                        FldDat = GetFieldValue(FldNam, ActDatLst, ActFldLst)
                        OldDat = GetFieldValue(FldNam, OldDatLst, ChgFldLst)
                        If InStr(CdiKeyFields(CurTabIdx), FldNam) > 0 Then
                            If (OldDat <> "") And (FldDat <> OldDat) Then
                                Mid(MsgTxt(0), 1, 1) = "D"
                                Mid(MsgTxt(1), 1, 1) = "I"
                            End If
                        End If
                        If (InStr(CdiTriggers(CurTabIdx), FldNam) > 0) And (InStr(ChgFldLst, FldNam) > 0) Then
                            If OldDat = "" Then
                                Mid(MsgTxt(0), 1, 1) = "D"
                                Mid(MsgTxt(1), 1, 1) = "I"
                                'OldDat = Space(FldLen)
                            End If
                        End If
                        Select Case FldNam
                            Case "*MT*"
                                If ChgFldLst <> "" Then
                                    OldDat = "U"
                                    FldDat = "U"
                                Else
                                    OldDat = "D"
                                    FldDat = "I"
                                End If
                            Case "ADID"
                                FldDat = GetFieldValue(FldNam, ActDatLst, ActFldLst)
                                'Attention: Validation for NoiseMonitor Interface only !!
                                If FldDat = "B" Then
                                    If InStr(ActFldLst, "ORG4") > 0 Then
                                        FldDat = "A"
                                    ElseIf InStr(ActFldLst, "DES4") > 0 Then
                                        FldDat = "D"
                                    End If
                                End If
                            Case "----"
                                FldDat = ""
                            Case "ORIG"
                                FldDat = GetFieldValue("VIA4", ActDatLst, ActFldLst)
                                If FldDat = "" Then
                                    FldDat = GetFieldValue("ORG4", ActDatLst, ActFldLst)
                                End If
                            Case "DEST"
                                FldDat = GetFieldValue("VIA4", ActDatLst, ActFldLst)
                                If FldDat = "" Then
                                    FldDat = GetFieldValue("DES4", ActDatLst, ActFldLst)
                                End If
                            'Case Else
                            '    FldDat = GetFieldValue(FldNam, ActDatLst, ActFldLst)
                        End Select
                        If OldDat = "" Then OldDat = FldDat
                        OldTxt = Left(OldDat + Space(20), FldLen)
                        ItmTxt = Left(FldDat + Space(20), FldLen)
                        MsgTxt(0) = MsgTxt(0) + OldTxt
                        MsgTxt(1) = MsgTxt(1) + ItmTxt
                    End If
                Loop While FldNam <> ""
                MsgTxt(0) = MsgTxt(0) + CdiRecSep
                MsgTxt(1) = MsgTxt(1) + CdiRecSep
                If Left(MsgTxt(0), 1) = Left(MsgTxt(1), 1) Then
                    MsgTxt(1) = ""
                End If
            End If
        Else
            MsgTxt(0) = Chr(16) + OutText + Chr(17)
            totLen = Len(MsgTxt(0))
            txtLen = Trim(Str(totLen))
            txtLen = Right(("00000000" + txtLen), 8)
            MsgTxt(0) = txtLen + MsgTxt(0)
        End If
        On Error GoTo ErrorHandle
        CurMsg = 0
        Do
            If MsgTxt(CurMsg) <> "" Then
                CdiActIdx = 0
                If TcpIdx > 0 Then
                    CdiPort(TcpIdx).SendData MsgTxt(CurMsg)
                    TotalWait = WaitForComplete(TcpIdx)
                Else
                    CdiServer = True
                    Do
                        CdiPortsChanged = False
                        InternalError = False
                        For Each cdiObj In CdiPort
                            If Not InternalError Then
                                CurIdx = cdiObj.Index
                                If Not InternalError Then
                                    If CurIdx > CdiActIdx Then
                                        cdiObj.SendData MsgTxt(CurMsg)
                                        If Not InternalError Then
                                            TotalWait = WaitForComplete(cdiObj.Index)
                                        End If
                                    End If
                                End If
                            End If
                            If CdiPortsChanged Then Exit For
                            InternalError = False
                        Next
                    Loop While CdiPortsChanged
                    CdiServer = False
                End If
                CdiActIdx = 0
                'MsgLogging MsgTxt(CurMsg)
            End If
            CurMsg = CurMsg + 1
        Loop While CurMsg <= 1
    End If
    Exit Sub
ErrorHandle:
    ' The error reason only can be the access to the CdiPort element.
    ' We differ between two cases: single or multiple attach
    'MsgBox "Error: " & Err.Description
    If TcpIdx > 0 Then
        ' Single transmission. The error will be solved by "PortError"
        ' and the port should be closed.
        CdiActIdx = 0
        Exit Sub
    Else
        ' Don't know the error reason. Set a flag and proceed.
        InternalError = True
        Resume Next
    End If
End Sub

Private Function WaitForComplete(Index As Integer) As Single
    Dim StartTime As Single
    CdiPacketComplete = False
    CdiActIdx = Index
    StartTime = Timer
    While Not CdiPacketComplete
        'Waiting for CdiPort_SendComplete-Event
        DoEvents
    Wend
    WaitForComplete = Timer - StartTime
End Function

Private Sub MarkTabLines(CurTabIdx As Integer, UseLine As Long, UseCol As Long, UseText As String)
    Dim MaxLin As Long
    Dim BgnLin As Long
    Dim CurLin As Long
    If UseLine >= 0 Then
        BgnLin = UseLine
        MaxLin = UseLine
    Else
        BgnLin = 0
        MaxLin = DataTab(CurTabIdx).GetLineCount - 1
    End If
    For CurLin = BgnLin To MaxLin
        DataTab(CurTabIdx).SetColumnValue CurLin, UseCol, UseText
    Next
End Sub
Private Sub DeleteOldTabLines(CurTabIdx As Integer, UseCol As Long, UseText As String)
    Dim LineList As String
    Dim ItmCnt As Integer
    Dim CurItm As Integer
    Dim CurLine As Long
    LineList = DataTab(CurTabIdx).GetLinesByColumnValue(UseCol, UseText, 0)
    If LineList <> "" Then
        ItmCnt = ItemCount(LineList, ",")
        For CurItm = ItmCnt To 1 Step -1
            CurLine = Val(GetItem(LineList, CurItm, ","))
            If DataTab(CurTabIdx).GetLineStatusValue(CurLine) > 0 Then UpdateCount = UpdateCount - 1
            DataTab(CurTabIdx).DeleteLine CurLine
        Next
        DataTab(CurTabIdx).RedrawTab
        If UpdateCount < 0 Then UpdateCount = 0
    End If
End Sub

Private Sub CdiSendAllRecs(CurTabIdx As Integer, CurCdiIdx As Integer)
    Dim MaxLin As Long
    Dim BgnLin As Long
    Dim CurLin As Long
    Dim tmpRec As String
    Dim OutMsg As String
    'If ApplIsInKioskMode Then SysFunc.ShowMyMouse
    Screen.MousePointer = 11
    BgnLin = 0
    MaxLin = DataTab(CurTabIdx).GetLineCount - 1
    For CurLin = BgnLin To MaxLin
        tmpRec = DataTab(CurTabIdx).GetLineValues(CurLin)
        If GetItem(tmpRec, 1, ",") <> "" Then
            OutMsg = MyPageFields(CurTabIdx) & vbNewLine & tmpRec & vbNewLine & "" & vbNewLine & "" & vbNewLine & ""
            SendCdiMsg OutMsg, CurCdiIdx, CurTabIdx
        End If
    Next
    Screen.MousePointer = 0
    If ApplIsInKioskMode Then SysFunc.HideMyMouse
End Sub

Public Sub MsgLogging(MsgTxt As String)
    Static MyFileCnt As Integer
    Dim MyLogFile As String
    Dim NewName As String
    Dim ExtName As String
    Dim FileNbr As Integer
    Dim tmpTime As String
    Dim OutTxt As String
    Dim MyLogSize As Long
    If MsgLoggingIsActive Then
        FileNbr = FreeFile
        MyLogFile = App.Path + "\" + App.EXEName + ".log"
        tmpTime = Format(Now, "YYYY.MM.DD/hh:mm:ss")
        OutTxt = "[" + tmpTime + "] " + MsgTxt
        Open MyLogFile For Append As #FileNbr
        Print #FileNbr, OutTxt
        Close FileNbr
        MyLogSize = FileLen(MyLogFile)
        If MyLogSize > 1000000 Then
            MyFileCnt = MyFileCnt + 1
            ExtName = Right("0000" & Trim(Str(MyFileCnt)), 4)
            NewName = App.EXEName & "_" & ExtName & ".log"
            On Error GoTo ErrorHandle
            Name MyLogFile As NewName
        End If
    End If
    Exit Sub
ErrorHandle:
    MyFileCnt = MyFileCnt + 1
    ExtName = Right("0000" & Trim(Str(MyFileCnt)), 4)
    NewName = App.EXEName & "_" & ExtName & ".log"
    Resume
End Sub

' Handler for mouse events occuring in system tray.
Public Sub SysTrayMouseEventHandler()
    SetForegroundWindow Me.hwnd
    PopupMenu RCPopup, vbPopupMenuRightButton
End Sub

Private Sub msg1_Click()
    CdiList.Show
End Sub

Private Sub Rest1_Click()
    Unhook    ' Return event control to windows
    Me.Show
    RemoveIconFromTray
End Sub

Private Sub Exit1_Click()
    Unhook    ' Return event control to windows
    RemoveIconFromTray
    ShutDownRequested = True
End Sub

Private Function GetFreeCdiIdx()
    Dim iCnt As Integer
    Dim ObjIdx As Integer
    Dim cdiObj As Winsock
    iCnt = -1
    For Each cdiObj In CdiPort
        iCnt = iCnt + 1
        ObjIdx = cdiObj.Index
        If ObjIdx > iCnt Then Exit For
    Next
    If iCnt = ObjIdx Then iCnt = ObjIdx + 1
    GetFreeCdiIdx = iCnt
End Function

Private Sub InitOcmTab()
    Dim newRec As String
    Dim i As Integer
    Dim j As Integer
    i = 0
    OcmTab(i).ResetContent
    OcmTab(i).HeaderString = " TIME , FLIGHT,NEW,UPDATED,REMARK" & Space(100)
    OcmTab(i).HeaderLengthString = "50,50,50,50,50"
    OcmTab(i).ColumnAlignmentString = "C,L,C,C,L"
    OcmTab(i).HeaderAlignmentString = OcmTab(i).ColumnAlignmentString
    OcmTab(i).FontName = "Arial"
    OcmTab(i).SetTabFontBold True
    OcmTab(i).LineHeight = TopLine(0).Height / 15
    OcmTab(i).FontSize = OcmTab(i).LineHeight - 2
    OcmTab(i).HeaderFontSize = OcmTab(i).FontSize
    OcmTab(i).DisplayBackColor = vbBlue
    OcmTab(i).DisplayTextColor = vbWhite
    OcmTab(i).SelectBackColor = vbBlack
    OcmTab(i).SelectTextColor = vbYellow
    OcmTab(i).EmptyAreaBackColor = OcmTab(i).DisplayBackColor
    OcmTab(i).ShowVertScroller False
    OcmTab(i).AutoSizeByHeader = True
    OcmTab(i).LifeStyle = True
    OcmTab(i).Top = 0
    OcmTab(i).Left = 0
    OcmTab(i).Width = TopInfoPanel(4).ScaleWidth
    OcmTab(i).Height = TopInfoPanel(4).ScaleHeight
    OcmTab(i).AutoSizeColumns
    'NewRec = "00:10, SHQ6616/14,ATA,12:00,Expected"
    'For j = 1 To 10
    '    OcmTab(i).InsertTextLineAt 0, NewRec, False
    '    OcmTab(i).AutoSizeColumns
    '    OcmTab(i).Refresh
    'Next
End Sub
Private Sub InitMsgTplTab(StaffSection As String)
    Dim CurSection As String
    Dim newRec As String
    Dim tplDat As String
    Dim tplKey As String
    Dim FldData As String
    Dim RecData As String
    Dim RecFields As String
    Dim LineNo As Long
    Dim i As Integer
    Dim j As Integer
    i = 0
    MsgTplTab(i).ResetContent
    MsgTplTab(i).HeaderString = " TEXT" & Space(100) & ", AREA,STYLE"
    MsgTplTab(i).LogicalFieldList = "TEXT,AREA,TYPE"
    MsgTplTab(i).HeaderLengthString = "50,50,50"
    MsgTplTab(i).ColumnAlignmentString = "L,L,L"
    MsgTplTab(i).HeaderAlignmentString = MsgTplTab(i).ColumnAlignmentString
    MsgTplTab(i).FontName = "Arial"
    MsgTplTab(i).SetTabFontBold True
    'MsgTplTab(i).LineHeight = TopLine(0).Height / 15
    'MsgTplTab(i).FontSize = MsgTplTab(i).LineHeight - 2
    MsgTplTab(i).HeaderFontSize = MsgTplTab(i).FontSize
    'MsgTplTab(i).DisplayBackColor = vbBlue
    'MsgTplTab(i).DisplayTextColor = vbWhite
    'MsgTplTab(i).SelectBackColor = vbBlack
    'MsgTplTab(i).SelectTextColor = vbYellow
    'MsgTplTab(i).EmptyAreaBackColor = MsgTplTab(i).DisplayBackColor
    MsgTplTab(i).ShowVertScroller True
    MsgTplTab(i).AutoSizeByHeader = True
    MsgTplTab(i).LifeStyle = True
    MsgTplTab(i).CursorLifeStyle = True
    MsgTplTab(i).Top = 0
    MsgTplTab(i).Left = 0
    'MsgTplTab(i).Width = TopInfoPanel(4).ScaleWidth
    'MsgTplTab(i).Height = TopInfoPanel(4).ScaleHeight
    'NewRec = "00:10, SHQ6616/14,ATA,12:00,Expected"
    'For j = 1 To 10
    '    MsgTplTab(i).InsertTextLineAt 0, NewRec, False
    '    MsgTplTab(i).AutoSizeColumns
    '    MsgTplTab(i).Refresh
    'Next
    CurSection = StaffSection & "_TEXT_TEMPLATES"
    newRec = CreateEmptyLine(MsgTplTab(i).LogicalFieldList)
    RecFields = "TEXT,AREA,TYPE"
    j = 0
    tplDat = "START"
    While tplDat <> ""
        j = j + 1
        tplKey = "TEXT_" & CStr(j)
        tplDat = GetIniEntry(StaffPageIniFile, CurSection, "", tplKey, "")
        If tplDat <> "" Then
            RecData = ""
            FldData = GetItem(tplDat, 1, ",")
            If FldData = "" Then FldData = "Undefined Text"
            RecData = RecData & FldData & ","
            FldData = GetItem(tplDat, 2, ",")
            If FldData = "" Then FldData = "5"
            If Val(FldData) < 1 Then FldData = "1"
            If Val(FldData) > 5 Then FldData = "5"
            RecData = RecData & FldData & ","
            FldData = GetItem(tplDat, 3, ",")
            If FldData = "" Then FldData = "1"
            If Val(FldData) < 1 Then FldData = "1"
            If Val(FldData) > 5 Then FldData = "5"
            RecData = RecData & FldData & ","
            MsgTplTab(i).InsertTextLine newRec, False
            LineNo = MsgTplTab(i).GetLineCount - 1
            MsgTplTab(i).SetFieldValues LineNo, RecFields, RecData
        End If
    Wend
    MsgTplTab(i).AutoSizeColumns
End Sub

Private Sub ToggleOcmPanel()
    Dim NewSize As Long
    If TopInfoPanel(4).Height > TopInfoPanel(0).Height Then
        NewSize = TopInfoPanel(0).Height
    Else
        NewSize = Me.ScaleHeight - TopInfoPanel(4).Top
    End If
    TopInfoPanel(4).Height = NewSize
    OcmTab(0).Height = TopInfoPanel(4).ScaleHeight
    TopInfoPanel(4).ZOrder
End Sub

Private Sub InitInfoTab(CurSection As String)
    Dim newRec As String
    Dim CfgData As String
    Dim CfgKey As String
    Dim PrvTitle As String
    Dim RptCnt As Integer
    Dim CurCol As Long
    Dim i As Integer
    Dim j As Integer
    Dim MsgRec As MyInfoRec
    PrvTitle = ""
    For i = 0 To InfoTab.UBound
        InfoTab(i).ResetContent
        InfoTab(i).LogicalFieldList = "TIME,FLNO,TYPE,DATA,TEXT,URNO,MIDX"
        InfoTab(i).HeaderString = " TIME , FLIGHT , TYPE , NEW , MESSAGE TEXT                                        ,URNO,MSGIDX"
        InfoTab(i).HeaderLengthString = "50,50,50,50,50,50,50"
        InfoTab(i).ColumnAlignmentString = "C,L,C,C,L,R,R"
        InfoTab(i).HeaderAlignmentString = InfoTab(i).ColumnAlignmentString
        InfoTab(i).FontName = "Arial"
        InfoTab(i).SetTabFontBold True
        InfoTab(i).LineHeight = TopLine(0).Height / 15
        InfoTab(i).FontSize = InfoTab(i).LineHeight - 2
        InfoTab(i).HeaderFontSize = InfoTab(i).FontSize
        InfoTab(i).MainHeader = True
        InfoTab(i).SetMainHeaderFont InfoTab(i).FontSize, False, False, True, 0, InfoTab(i).FontName
        CfgData = chkTopLine(i).Caption
        If CfgData = "" Then
            CfgData = PrvTitle
            If CfgData = "" Then CfgData = "Message Area " & CStr(i + 1)
        End If
        PrvTitle = CfgData
        If chkTopLine(i).Caption = "" Then
            RptCnt = RptCnt + 1
            CfgData = Trim(CfgData) & " " & CStr(RptCnt)
        Else
            RptCnt = 1
        End If
        CfgKey = "AREA" & CStr(i + 1) & "_TAB_TITLE"
        CfgData = GetIniEntry(StaffPageIniFile, CurSection, "", CfgKey, CfgData)
        CfgData = Replace(CfgData, "[", "", 1, -1, vbBinaryCompare)
        CfgData = Replace(CfgData, "]", "", 1, -1, vbBinaryCompare)
        CfgData = Trim(CfgData)
        optArea(i).ToolTipText = CfgData
        CfgData = "MSG," & CfgData & "        "
        InfoTab(i).SetMainHeaderValues "1,4,2", CfgData, ","
        'InfoTab(i).DisplayBackColor = vbBlue
        'InfoTab(i).DisplayTextColor = vbWhite
        InfoTab(i).SelectBackColor = vbBlack
        InfoTab(i).SelectTextColor = vbYellow
        InfoTab(i).EmptyAreaBackColor = LightGray
        InfoTab(i).ShowVertScroller False
        InfoTab(i).AutoSizeByHeader = True
        InfoTab(i).LifeStyle = True
        InfoTab(i).ColSelectionAdd 4
        InfoTab(i).SelectColumnBackColor = LightYellow
        InfoTab(i).SelectColumnTextColor = vbBlack
        CurCol = 0
        InfoTab(i).DateTimeSetColumn CurCol
        InfoTab(i).DateTimeSetInputFormatString CurCol, "YYYYMMDDhhmmss"
        InfoTab(i).DateTimeSetOutputFormatString CurCol, "hh':'mm"
        InfoTab(i).AutoSizeColumns
        'InfoTab(i).DefaultCursor = False
    Next
End Sub

Private Sub ArrangeInfoTabs(Index As Integer, ForWhat As Integer)
    Dim NewTop As Long
    Dim idx As Integer
    Dim i1 As Integer
    Dim i2 As Integer
    idx = Index
    If idx >= 0 Then
        i1 = idx
        i2 = idx
    Else
        i1 = 0
        i2 = InfoTabPanel.UBound
    End If
    For idx = i1 To i2
        NewTop = TopInfoPanel(0).Top + (TopLine(idx).Top / 15) - ((TopLine(idx).Height / 15) * 0) + 1
        InfoTabPanel(idx).Top = NewTop
        InfoTabPanel(idx).Left = TopInfoPanel(0).Left + (TopLine(idx).Left / 15) + 2
        InfoTab(idx).Top = 0
        InfoTab(idx).Left = 0
        InfoTab(idx).Width = InfoTabPanel(idx).ScaleWidth
        InfoTab(idx).Height = InfoTabPanel(idx).ScaleHeight
    Next
End Sub

Private Function GetFreeMsgLabel(ForWhat As Integer) As Integer
    Dim GotIdx As Integer
    Dim i As Integer
    GotIdx = -1
    i = 0
    While (GotIdx < 0) And (i < MAX_MSG_CNT)
        i = i + 1
        If MsgInfoArray(i).MsgSysInUse = False Then
            GotIdx = i
        End If
    Wend
    GetFreeMsgLabel = GotIdx
End Function
Private Function GetMsgIdxByUrno(ForWhat As Integer, MsgUrno As String) As Integer
    Dim GotIdx As Integer
    Dim i As Integer
    GotIdx = -1
    i = 0
    While (GotIdx < 0) And (i < MAX_MSG_CNT)
        i = i + 1
        If MsgInfoArray(i).MsgSysMsgUrno = MsgUrno Then
            GotIdx = i
        End If
    Wend
    GetMsgIdxByUrno = GotIdx
End Function


Private Sub CleanUpMsgLabels(MsgIdx As Integer, ForWhat As Integer)
    Dim i As Integer
    Dim j As Integer
    If ForWhat < 1 Then
        MsgInfoArray(MsgIdx).MsgSysInUse = False
        TopLabel(MsgIdx).Visible = False
        TopSep(MsgIdx).Visible = False
    End If
    If ForWhat <= 1 Then
        If MsgSysBlinkCnt > 0 Then
            j = 0
            For i = 1 To MsgSysBlinkCnt
                j = j + 1
                If j < i Then
                    MsgSysBlinkLst(j) = MsgSysBlinkLst(i)
                ElseIf MsgSysBlinkLst(i) = MsgIdx Then
                    j = i - 1
                End If
            Next
            If j < i Then MsgSysBlinkCnt = MsgSysBlinkCnt - 1
            If MsgSysBlinkCnt = 0 Then tmrBlink.Enabled = False
        End If
    End If
End Sub
Private Sub ArrangeMsgRow(RowIdx As Integer)
    Dim OldTag As String
    Dim NewTag As String
    Dim tmpItm As String
    Dim NewLeft As Long
    Dim MaxLeft As Long
    Dim MsgIdx As Integer
    Dim MaxIdx As Integer
    Dim itm As Integer
    OldTag = LinePanel(RowIdx).Tag
    If OldTag <> "" Then
        MaxIdx = -1
        MaxLeft = LinePanel(RowIdx).ScaleWidth
        NewTag = ""
        NewLeft = 0
        tmpItm = "START"
        itm = 0
        While tmpItm <> ""
            itm = itm + 1
            tmpItm = GetItem(OldTag, itm, ",")
            If tmpItm <> "" Then
                MsgIdx = Val(tmpItm)
                If (MsgInfoArray(MsgIdx).MsgSysInUse) And (NewLeft < MaxLeft) Then
                    NewTag = NewTag & CStr(MsgIdx) & ","
                    TopLabel(MsgIdx).Left = NewLeft
                    NewLeft = NewLeft + TopLabel(MsgIdx).Width
                    TopSep(MsgIdx).Left = NewLeft
                    NewLeft = NewLeft + TopSep(MsgIdx).Width
                    MaxIdx = MsgIdx
                Else
                    CleanUpMsgLabels MsgIdx, 0
                End If
            End If
        Wend
        LinePanel(RowIdx).Tag = NewTag
        If MaxIdx >= 0 Then
            TopSep(MaxIdx).Visible = False
        End If
    End If
End Sub

Private Sub tmrSize_Timer()
    tmrSize.Enabled = False
    'MyCfg.Show , Me
    'Me.Refresh
    'MyCfg.Refresh
    'MyCfg.ChkCfg(1).SetFocus
End Sub

Private Sub tmrTest_Timer()
    tmrTest.Enabled = False
    'Text3.Text = "-------------------------------" & vbNewLine & Text3.Text
End Sub

Private Sub InitGroupPages(CurTabIdx As Integer, ForWhat As String)
    Dim newRec As String
    Dim tmpGrno As String
    Dim tmpUrno As String
    Dim CurUrno As String
    Dim LineText As String
    Dim tmpPageList As String
    Dim tmpData As String
    Dim UpdFldLst As String
    Dim UpdDatLst As String
    Dim TitleColor As Long
    Dim LineCount As Long
    Dim LineNo As Long
    Dim DarkLine As Boolean
    Dim i As Integer
    Dim fn As Integer
    tmrMinutes.Interval = 0
    tmrSeconds.Interval = 0
    newRec = EmptyLine(CurTabIdx)
    DataTab(CurTabIdx).ResetContent
    UpdFldLst = ""
    UpdDatLst = ""
    tmpPageList = ""
    LineNo = -1
    fn = FreeFile()
    Open PageGroupIniFile For Input As #fn
    While Not EOF(fn)
        Line Input #fn, LineText
        If Left(LineText, 6) = "[GROUP" Then
            If UpdFldLst <> "" Then
                DataTab(CurTabIdx).SetFieldValues LineNo, UpdFldLst, UpdDatLst
                If ForWhat = "PAGE_INDEX" Then
                    InitPageIndexList DataTab(CurTabIdx), tmpPageList, tmpGrno
                End If
                UpdFldLst = ""
                UpdDatLst = ""
                tmpPageList = ""
            End If
            tmpGrno = GetItem(LineText, 2, "_")
            tmpGrno = Left(tmpGrno, Len(tmpGrno) - 1)
            DataTab(CurTabIdx).InsertTextLine newRec, False
            LineNo = DataTab(CurTabIdx).GetLineCount - 1
            tmpData = tmpGrno & "_0000"
            UpdFldLst = UpdFldLst & "URNO" & ","
            UpdDatLst = UpdDatLst & tmpData & ","
        ElseIf Left(LineText, 10) = "GROUP_NAME" Then
            tmpData = Trim(GetItem(LineText, 2, "="))
            UpdFldLst = UpdFldLst & "UNIT" & ","
            UpdDatLst = UpdDatLst & tmpData & ","
        ElseIf Left(LineText, 10) = "GROUP_AREA" Then
            tmpData = Trim(GetItem(LineText, 2, "="))
            UpdFldLst = UpdFldLst & "AREA" & ","
            UpdDatLst = UpdDatLst & tmpData & ","
        ElseIf Left(LineText, 11) = "GROUP_TITLE" Then
            tmpData = Trim(GetItem(LineText, 2, "="))
            UpdFldLst = UpdFldLst & "GRNM" & ","
            UpdDatLst = UpdDatLst & tmpData & ","
        ElseIf Left(LineText, 12) = "PAGE_GRP_BGN" Then
            tmpData = Trim(GetItem(LineText, 2, "="))
            UpdFldLst = UpdFldLst & "PGFR" & ","
            UpdDatLst = UpdDatLst & tmpData & ","
        ElseIf Left(LineText, 12) = "PAGE_GRP_END" Then
            tmpData = Trim(GetItem(LineText, 2, "="))
            UpdFldLst = UpdFldLst & "PGTO" & ","
            UpdDatLst = UpdDatLst & tmpData & ","
        ElseIf Left(LineText, 12) = "PAGE_GRP_RNG" Then
            tmpData = Trim(GetItem(LineText, 2, "="))
            If tmpPageList = "" Then tmpPageList = tmpData
            If ForWhat = "GROUP_INDEX" Then
                tmpData = Replace(tmpData, ",", "/", 1, -1, vbBinaryCompare)
                UpdFldLst = UpdFldLst & "PGNO" & ","
                UpdDatLst = UpdDatLst & tmpData & ","
            Else
                UpdFldLst = UpdFldLst & "PGNO" & ","
                UpdDatLst = UpdDatLst & "PAGES" & ","
            End If
        ElseIf Left(LineText, 11) = "ACTIVE_PAGES" Then
            tmpPageList = Trim(GetItem(LineText, 2, "="))
        ElseIf Left(LineText, 11) = "DESCRIPTION" Then
            tmpData = Trim(GetItem(LineText, 2, "="))
            UpdFldLst = UpdFldLst & "PGNM" & ","
            UpdDatLst = UpdDatLst & tmpData & ","
        End If
    Wend
    Close fn
    
    If LineNo >= 0 Then
        If UpdFldLst <> "" Then
            DataTab(CurTabIdx).SetFieldValues LineNo, UpdFldLst, UpdDatLst
            UpdFldLst = ""
            UpdDatLst = ""
        End If
    End If
    
    If ForWhat = "GROUP_INDEX" Then
        tmpUrno = ""
        CurUrno = ""
        GetColorValues MyTabLineColors(CurTabIdx)
        LineTextColor = MyLineTextColor(CurTabIdx)
        DarkLine = False
        LineCount = DataTab(CurTabIdx).GetLineCount - 1
        For i = 0 To LineCount
            If MyUrnoIdx(CurTabIdx) >= 0 Then
                tmpUrno = DataTab(CurTabIdx).GetColumnValue(i, MyUrnoIdx(CurTabIdx))
                tmpUrno = GetItem(tmpUrno, 1, "_")
                If tmpUrno <> CurUrno Then DarkLine = Not DarkLine
                If DarkLine Then
                  DataTab(CurTabIdx).SetLineColor i, LineTextColor, DarkLineColor
                  DataTab(CurTabIdx).SetLineTag i, CStr(DarkLineColor)
                Else
                  DataTab(CurTabIdx).SetLineColor i, LineTextColor, LightLineColor
                  DataTab(CurTabIdx).SetLineTag i, CStr(LightLineColor)
                End If
                CurUrno = tmpUrno
            Else
                If (i Mod 2) = 0 Then
                  DataTab(CurTabIdx).SetLineColor i, LineTextColor, DarkLineColor
                  DataTab(CurTabIdx).SetLineTag i, CStr(DarkLineColor)
                Else
                  DataTab(CurTabIdx).SetLineColor i, LineTextColor, LightLineColor
                  DataTab(CurTabIdx).SetLineTag i, CStr(LightLineColor)
                End If
            End If
        Next i
    Else
        tmpUrno = ""
        CurUrno = ""
        DarkLine = False
        tmpData = GetIniEntry(PageLayoutIniFile, "PAGE_MASTER", "", "TITLE_COLOR", "BLACK")
        TitleColor = TranslateColor(tmpData)
        GetColorValues MyTabLineColors(CurTabIdx)
        LineTextColor = MyLineTextColor(CurTabIdx)
        
        LineCount = DataTab(CurTabIdx).GetLineCount - 1
        For i = 0 To LineCount
            If MyUrnoIdx(CurTabIdx) >= 0 Then
                tmpUrno = DataTab(CurTabIdx).GetColumnValue(i, MyUrnoIdx(CurTabIdx))
                tmpData = GetItem(tmpUrno, 2, "_")
                If (tmpData = "0000") And (TitleColor <> vbBlack) Then
                    DataTab(CurTabIdx).SetLineColor i, vbBlack, TitleColor
                Else
                    tmpUrno = GetItem(tmpUrno, 1, "_")
                    If tmpUrno <> CurUrno Then DarkLine = Not DarkLine
                    If TitleColor <> vbBlack Then DarkLine = True
                    If DarkLine Then
                      DataTab(CurTabIdx).SetLineColor i, LineTextColor, DarkLineColor
                      DataTab(CurTabIdx).SetLineTag i, CStr(DarkLineColor)
                    Else
                      DataTab(CurTabIdx).SetLineColor i, LineTextColor, LightLineColor
                      DataTab(CurTabIdx).SetLineTag i, CStr(LightLineColor)
                    End If
                End If
                CurUrno = tmpUrno
            Else
                If (i Mod 2) = 0 Then
                  DataTab(CurTabIdx).SetLineColor i, LineTextColor, DarkLineColor
                  DataTab(CurTabIdx).SetLineTag i, CStr(DarkLineColor)
                Else
                  DataTab(CurTabIdx).SetLineColor i, LineTextColor, LightLineColor
                  DataTab(CurTabIdx).SetLineTag i, CStr(LightLineColor)
                End If
            End If
        Next i
    End If
    
    AppendLines CurTabIdx
    
    If MyAutoSizeCols(CurTabIdx) = True Then DataTab(CurTabIdx).AutoSizeColumns
    If (CedaWasConnected = True) Or (WorkOffLine) Then
        SetServerPanel "READY"
    Else
        SetServerPanel "NOSERVER"
    End If
    TabLookUp(CurTabIdx).HeaderLengthString = DataTab(CurTabIdx).HeaderLengthString
    TabLookUp(CurTabIdx).ColumnAlignmentString = DataTab(CurTabIdx).ColumnAlignmentString
    UnitStatus(4).Caption = MyHostName
    LostBroadCast(CurTabIdx) = False
    RefreshCountDown(CurTabIdx) = False
    MinCountDown(CurTabIdx) = RefreshCyclus
    DataTab(CurTabIdx).RedrawTab
    tmrMinutes.Interval = 60000
    tmrSeconds.Interval = 1000
    CedaCon(0).FillColor = vbGreen
    CedaCon(0).Visible = False
    Screen.MousePointer = 0
    If ApplIsInKioskMode Then SysFunc.HideMyMouse
End Sub
Private Sub SetServerPanel(ForWhat As String)
    Select Case ForWhat
        Case "READY"
            RefreshInd(0).FillColor = RemoteColor(0)
            RefreshInd(1).FillColor = RemoteColor(0)
            lblRefresh(0) = "SYSTEM"
            lblRefresh(1) = "STATUS"
            BcRcvInd(0).FillColor = &HC000&
            BcRcvInd(1).FillColor = &HC000&
            BcUpdInd(0).FillColor = &HC000&
            BcUpdInd(1).FillColor = &HC000&
        Case "NOSERVER"
            RefreshInd(0).FillColor = vbRed
            RefreshInd(1).FillColor = vbRed
            BcRcvInd(0).FillColor = vbRed
            BcRcvInd(1).FillColor = vbRed
            BcUpdInd(0).FillColor = vbRed
            BcUpdInd(1).FillColor = vbRed
            lblRefresh(0) = "SERVER"
            lblRefresh(1) = MyHostName
        Case Else
            RefreshInd(0).FillColor = RemoteColor(0)
            RefreshInd(1).FillColor = RemoteColor(0)
            lblRefresh(0) = "SYSTEM"
            lblRefresh(1) = "STATUS"
            BcRcvInd(0).FillColor = &HC000&
            BcRcvInd(1).FillColor = &HC000&
            BcUpdInd(0).FillColor = &HC000&
            BcUpdInd(1).FillColor = &HC000&
    End Select
End Sub
Private Sub InitPageIndexList(CurTab As TABLib.TAB, GrpPageList As String, GrpUrno As String)
    Dim CurPageList As String
    Dim PageRange As String
    Dim PageNum As String
    Dim tmpData As String
    Dim CurGrpUrno As String
    Dim PagGrpUrno As String
    Dim UpdFldLst As String
    Dim UpdDatLst As String
    Dim itm As Integer
    Dim iPgFr As Integer
    Dim iPgTo As Integer
    Dim iPage As Integer
    Dim LineNo As Long
    Dim CurPageIniFile As String
    Dim CurFileName As String
    Dim newRec As String
    newRec = EmptyLine(CurTab.Index)
    CurGrpUrno = GetItem(GrpUrno, 1, "_")
    CurPageList = GrpPageList
    PageRange = "START"
    itm = 0
    While PageRange <> ""
        itm = itm + 1
        PageRange = GetItem(CurPageList, itm, ",")
        If PageRange <> "" Then
            If InStr(PageRange, "-") > 0 Then
                iPgFr = Val(GetItem(PageRange, 1, "-"))
                iPgTo = Val(GetItem(PageRange, 2, "-"))
            Else
                iPgFr = Val(GetItem(PageRange, 1, "-"))
                iPgTo = Val(GetItem(PageRange, 1, "-"))
            End If
            For iPage = iPgFr To iPgTo
                PageNum = Right("0000" & CStr(iPage), 4)
                CurFileName = "PAGE_" & PageNum & ".ini"
                CurPageIniFile = MyConfigPath & "\" & CurFileName
                tmpData = Dir(CurPageIniFile)
                If UCase(tmpData) = UCase(CurFileName) Then
                    tmpData = GetIniEntry(CurPageIniFile, "PAGE_MASTER", "", "INFO_TITLE", "NO NAME")
                    tmpData = GetIniEntry(CurPageIniFile, "PAGE_GLOBAL", "", "GROUP_TITLE", tmpData)
                    tmpData = GetItem(tmpData, 1, "#")
                    CurTab.InsertTextLine newRec, False
                    LineNo = CurTab.GetLineCount - 1
                    UpdFldLst = "PGNO,PGNM,URNO"
                    PagGrpUrno = CurGrpUrno & "_" & PageNum
                    UpdDatLst = CStr(iPage) & "," & tmpData & "," & PagGrpUrno
                    CurTab.SetFieldValues LineNo, UpdFldLst, UpdDatLst
                End If
            Next
        End If
    Wend
End Sub
Public Sub HandleSysError(ForWhat As String, ConText As String, IsSolved As Boolean)
    'If Not IsSolved Then
    '    HdlMsgIcon(0).Picture = MyIcons(2).Picture
    '    TopInfoPanel(5).Visible = True
    '    Form_Resize
    'Else
    '    TopInfoPanel(5).Visible = False
    '    Form_Resize
    'End If
End Sub

Private Function BdpsCheck() As Boolean
    Dim CurLoginStatus As Boolean
    Dim BdpsFields As String
    Dim BdpsData As String
    Dim BdpsTable As String
    Dim AutKey As String
    Dim AutCmd As String
    Dim tmpData As String
    Dim CurTime As Date
    Dim RetVal As Integer
    CurLoginStatus = True
    PrepareMessage "SEC"
    BdpsTable = "SECTAB"
    BdpsFields = "URNO,STAT,REMA,USID,NAME,TYPE,LANG"
    AutKey = "WHERE USID='" & MyWksName & "'"
    AutCmd = "RTA"
    Ufis.ClearDataBuffer
    BdpsData = " "
    RetVal = Ufis.CallServer(AutCmd, BdpsTable, BdpsFields, BdpsData, AutKey, "")
    BdpsData = Ufis.GetDataBuffer(True)
    Ufis.ClearDataBuffer
    If BdpsData <> "" Then
        MyBdpsSecUrno = GetItem(BdpsData, 1, ",")
        MyBdpsSecStat = GetItem(BdpsData, 2, ",")
        MyBdpsSecRema = GetItem(BdpsData, 3, ",")
        If MyBdpsSecStat = "1" Then
            CurLoginStatus = True
        Else
            CurLoginStatus = False
        End If
        If (CurLoginStatus = True) And (MyCtrlSessionID = 1) Then
            If MyWksType <> "WKS" Then
                If (ApplDefaultKioskMode) And (Not ApplIsInKioskMode) Then
                    ApplIsInKioskMode = True
                    WritePrivateProfileString "GLOBAL", "APPL_MODE", "KIOSK", StaffPageIniFile
                    SysFunc.HideMyMouse
                    SysFunc.HideMyTaskBar
                    SysFunc.HideMyDesktop
                    LedPanel(3).Visible = False
                    GetPosAndSize
                End If
                BdpsData = ""
                If ApplIsInKioskMode Then
                    BdpsData = BdpsData & "KIOSKMODE ON / "
                Else
                    BdpsData = BdpsData & "KIOSKMODE OFF / "
                End If
                If MyVduName = MyWksName Then
                    CurTime = Now
                    tmpData = Format(CurTime, "ddmmmyy/hhmm")
                    tmpData = UCase(tmpData)
                    BdpsData = BdpsData & "RESET " & tmpData
                Else
                    BdpsData = BdpsData & "LOCATION " & MyUnitLocation
                End If
                If BdpsData <> MyBdpsSecRema Then
                    PrepareMessage "SEC"
                    BdpsTable = "SECTAB"
                    BdpsFields = "REMA"
                    AutKey = "WHERE URNO=" & MyBdpsSecUrno
                    AutCmd = "URT"
                    Ufis.ClearDataBuffer
                    RetVal = Ufis.CallServer(AutCmd, BdpsTable, BdpsFields, BdpsData, AutKey, "")
                    Ufis.ClearDataBuffer
                    MyBdpsSecRema = BdpsData
                End If
            End If
        End If
    ElseIf MyWksType <> "WKS" Then
        BdpsFields = "USID,NAME,TYPE,PASS,STAT,LANG,REMA,MODU,VAFR,VATO,FREQ"
        BdpsData = ""
        BdpsData = BdpsData & MyWksName & ","
        If MyVduName = MyWksName Then
            BdpsData = BdpsData & "" & MyUnitLocation & ","
        Else
            BdpsData = BdpsData & "UNIT " & MyVduName & ","
        End If
        BdpsData = BdpsData & "W" & ","
        BdpsData = BdpsData & "VduStation" & ","
        BdpsData = BdpsData & "1" & ","
        BdpsData = BdpsData & "0" & ","
        If ApplIsInKioskMode Then
            BdpsData = BdpsData & "KIOSKMODE ON / "
        Else
            BdpsData = BdpsData & "KIOSKMODE OFF / "
        End If
        If MyVduName = MyWksName Then
            CurTime = Now
            tmpData = Format(CurTime, "ddmmmyy/hhmm")
            tmpData = UCase(tmpData)
            BdpsData = BdpsData & "INIT " & tmpData & ","
        Else
            BdpsData = BdpsData & "LOCATION " & MyUnitLocation & ","
        End If
        BdpsData = BdpsData & "0" & ","
        CurTime = Now
        CurTime = DateAdd("d", -1, CurTime)
        tmpData = Format(CurTime, "yyyymmdd000000")
        BdpsData = BdpsData & tmpData & ","
        BdpsData = BdpsData & "20370314000000" & ","
        BdpsData = BdpsData & "1234567"
        
        AutCmd = "SEC"
        AutKey = "WHERE NEWSEC"
    
        RetVal = Ufis.CallServer(AutCmd, BdpsTable, BdpsFields, BdpsData, AutKey, "")
        BdpsData = Ufis.GetDataBuffer(True)
        Ufis.ClearDataBuffer
    End If

    BdpsCheck = CurLoginStatus
End Function

Public Sub GetMainFormDataGrid(UseTab As TABLib.TAB, WithData As Boolean, SetFontSize As Integer)
    Dim tmpRec As String
    Dim tmpLgList As String
    Dim tmpLgName As String
    Dim TxtColor As Long
    Dim BckColor As Long
    Dim LineNo As Long
    Dim ColNo As Long
    UseTab.ResetContent
    CloneGridLayout UseTab, DataTab(1)
    If SetFontSize > 4 Then
        UseTab.LineHeight = SetFontSize + 1
        UseTab.FontSize = SetFontSize
        UseTab.HeaderFontSize = SetFontSize
        UseTab.SetMainHeaderFont SetFontSize, False, False, True, 0, "Courier New"
    End If
    ColNo = GetRealItemNo(UseTab.LogicalFieldList, "'LG'")
    For LineNo = 0 To 3
        tmpRec = DataTab(1).GetLineValues(LineNo)
        UseTab.InsertTextLine tmpRec, False
        DataTab(1).GetLineColor LineNo, TxtColor, BckColor
        UseTab.SetLineColor LineNo, TxtColor, BckColor
        UseTab.ResetBitmapProperties LineNo
        If ColNo >= 0 Then
            tmpRec = DataTab(1).GetBitmapFileName(LineNo, ColNo)
            If tmpRec <> "" Then
                tmpLgName = "LG_" & CStr(LineNo)
                UseTab.CreateCellBitmpapObj tmpLgName, tmpRec
                UseTab.SetCellBitmapProperty LineNo, ColNo, tmpLgName
            End If
        End If
    Next
    UseTab.AutoSizeColumns
    
End Sub

Private Sub TopLabel_Click(Index As Integer)
    Dim MsgData As String
    Dim tmpData As String
    Dim AftFlno As String
    Dim AftFina As String
    Dim AftData As String
    Dim InfUrno As String
    Dim HitList As String
    Dim ColList As String
    Dim DatList As String
    Dim MsgIdx As Integer
    Dim TopLine As Long
    Dim BotLine As Long
    Dim TopScroll As Long
    Dim LineNo As Long
    Dim ColNo As Long
    Dim RowIdx As Integer
    Dim idx As Integer
    MsgIdx = Index
    MsgData = MsgInfoArray(MsgIdx).MsgDspAddi
    InfUrno = MsgInfoArray(MsgIdx).MsgSysMsgUrno
    RowIdx = MsgInfoArray(MsgIdx).MsgSysRow
    If TopInfoPanel(12).Visible Then
        txtMsgText.Text = GetItem(MsgData, 1, "|")
        MsgData = GetItem(MsgData, 2, "|")
        If MsgData <> "" Then
            idx = Val(MsgData)
        Else
            idx = MsgInfoArray(MsgIdx).MsgTypeIdx
        End If
        optType(idx).Value = True
        optArea(RowIdx).Value = True
        chkTool(25).Tag = InfUrno
        chkTool(26).Tag = InfUrno
        chkTool(24).Enabled = False
        chkTool(25).Enabled = True
        chkTool(26).Enabled = True
    Else
        AftFlno = GetItem(MsgData, 1, ":")
        tmpData = GetItem(MsgData, 2, ":")
        AftFina = Trim(GetItem(tmpData, 1, "="))
        AftData = Trim(GetItem(tmpData, 2, "="))
        ColList = ""
        ColNo = GetRealItemNo(DataTab(TabIdx).LogicalFieldList, "FLNO")
        If ColNo >= 0 Then
            ColList = ColList & CStr(ColNo) & ","
            DatList = DatList & AftFlno & ","
        End If
        ColNo = GetRealItemNo(DataTab(TabIdx).LogicalFieldList, AftFina)
        If ColNo >= 0 Then
            ColList = ColList & CStr(ColNo) & ","
            DatList = DatList & AftData & ","
        End If
        If ColList <> "" Then
            ColList = Left(ColList, Len(ColList) - 1)
            DatList = Left(DatList, Len(DatList) - 1)
        End If
        DataTab(TabIdx).SetCurrentSelection -1
        If (ColList <> "") And (InStr(ColList, ",") > 0) Then
            HitList = DataTab(TabIdx).GetLinesByMultipleColumnValue(ColList, DatList, 0)
            If HitList <> "" Then
                LineNo = Val(HitList)
                If LineNo >= 0 Then
                    DataTab(TabIdx).SetCurrentSelection LineNo
                    TopLine = DataTab(TabIdx).GetVScrollPos
                    BotLine = TopLine + ListLines(TabIdx) - 1
                    If LineNo > BotLine Then
                        TopScroll = TopLine + (LineNo - BotLine)
                        DataTab(TabIdx).OnVScrollTo TopScroll
                    ElseIf LineNo < TopLine Then
                        TopScroll = LineNo
                        DataTab(TabIdx).OnVScrollTo TopScroll
                    End If
                End If
            End If
        End If
    End If
End Sub

Private Sub AdjustTabEditField(Index As Integer, TabType As String, CurTab As TABLib.TAB, LineNo As Long, ColNo As Long)
    TabEditLookUpIndex = Index
    TabEditColNo = ColNo
    TabEditLineNo = LineNo
    TabEditLookUpType = TabType
    Set TabEditLookUpTabObj = CurTab
    SetTxtTabEditLookUpPosition Index
End Sub

Private Sub SetTxtTabEditLookUpPosition(Index As Integer)
    Dim tmpName As String
    Dim FieldTop As Long
    Dim FieldLeft As Long
    Dim LengthList As String
    Dim LengthItem As String
    Dim CurLength As Long
    Dim BgnCol As Long
    Dim CurCol As Long
    If TabEditLookUpType = "LOOK" Then
        TabLookUp(Index).ResetLineDecorations 0
        TabLookUp(Index).SetDecorationObject 0, TabEditColNo, "'S3'"
    End If
    FieldTop = TabLookUp(Index).Top
    txtTabEditLookUp(Index).Top = FieldTop - 2
    LengthList = TabLookUp(Index).HeaderLengthString
    FieldLeft = TabLookUp(Index).Left
    BgnCol = TabLookUp(Index).GetHScrollPos
    For CurCol = BgnCol To TabEditColNo - 1
        LengthItem = GetRealItem(LengthList, CurCol, ",")
        CurLength = Val(LengthItem) '* 15
        FieldLeft = FieldLeft + CurLength
    Next
    txtTabEditLookUp(Index).Left = FieldLeft + 1
    LengthItem = GetRealItem(LengthList, TabEditColNo, ",")
    CurLength = Val(LengthItem) '* 15
    If CurLength < 20 Then CurLength = 20
    txtTabEditLookUp(Index).Width = CurLength
    txtTabEditLookUp(Index).Height = TabLookUp(Index).LineHeight '* 15
    txtTabEditLookUp(Index).Text = TabLookUp(Index).GetColumnValue(TabEditLineNo, TabEditColNo)
    txtTabEditLookUp(Index).Tag = txtTabEditLookUp(Index).Text
    TabEditIsUpperCase = True
    tmpName = GetRealItem(TabLookUp(Index).LogicalFieldList, TabEditColNo, ",")
    If InStr("USEC,USEU,MEAN,VALU", tmpName) > 0 Then TabEditIsUpperCase = False
    txtTabEditLookUp(Index).FontBold = True
    CurrentTabFont = DataTab(Index).FontName
    txtTabEditLookUp(Index).Font = CurrentTabFont
    txtTabEditLookUp(Index).FontSize = DataTab(Index).FontSize - 9
    txtTabEditLookUp(Index).ZOrder
    txtTabEditLookUp(Index).Visible = True
    txtTabEditLookUp(Index).SetFocus
    SetTextSelected
End Sub

Private Sub txtMsgText_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyEscape
            If chkTool(24).Enabled = False Then
                chkTool(25).Tag = ""
                chkTool(26).Tag = ""
                chkTool(25).Enabled = False
                chkTool(26).Enabled = False
                chkTool(24).Enabled = True
            Else
                txtMsgText.Text = ""
            End If
        Case vbKeyReturn
            If chkTool(24).Enabled Then
                chkTool(24).Value = 1
            Else
                chkTool(25).Value = 1
            End If
        Case Else
    End Select
End Sub

Private Sub txtMsgText_KeyPress(KeyAscii As Integer)
    Select Case Chr(KeyAscii)
        Case ",", vbLf, vbCr
            ActNumInput = ""
            KeyAscii = 0
        Case Else
            'KeyAscii = Asc(UCase(Chr(KeyAscii)))
    End Select
End Sub

Private Sub txtTabEditLookUp_Change(Index As Integer)
    SyncCursorBusy = True
    SearchInTabList DataTab(Index), TabEditColNo, txtTabEditLookUp(Index).Text, True, "", ""
    TabLookUp(Index).SetColumnValue 0, TabEditColNo, txtTabEditLookUp(Index).Text
    SetLookUpKeyValues Index
    CreateSqlFromLookUp
    SyncCursorBusy = False
End Sub
Private Sub ToogleLookUp(Index)
    Dim RetVal As Boolean
    'TabEditLookUpIndex = TabLookIdx(Index)
    'TabEditLookUpValueCol = TabLookCol(Index)
    SyncCursorBusy = True
    'SearchInTabList DataTab(Index), TabEditColNo, txtTabEditLookUp(Index).Text, True, "", ""
    'TabLookUp(Index).SetColumnValue 0, TabEditColNo, txtTabEditLookUp(Index).Text
    'SetLookUpKeyValues Index
    'CreateSqlFromLookUp
    SyncCursorBusy = False
    'RetVal = SetLookUpKeyValues(Index)
    'DataTab(Index).SetFocus
    'DataTab(Index).SetCurrentSelection DataTab(Index).GetCurrentSelected
    'If RetVal = False Then TabEditLookUpIndex = -1
End Sub
Private Sub txtTabEditLookUp_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    Dim RetVal As Boolean
    Select Case KeyCode
        'Case vbKeyEscape
            'retVal = SetLookUpKeyValues(Index)
            'txtTabEditLookUp(Index).Text = txtTabEditLookUp(Index).Tag
            'DataTab(idx).SetFocus
        Case vbKeyReturn, vbKeyEscape
            ActNumInput = ""
            RetVal = SetLookUpKeyValues(Index)
            DataTab(Index).SetFocus
            DataTab(Index).SetCurrentSelection DataTab(Index).GetCurrentSelected
            If RetVal = False Then TabEditLookUpIndex = -1
        Case Else
    End Select
End Sub

Private Sub txtTabEditLookUp_KeyPress(Index As Integer, KeyAscii As Integer)
    Select Case Chr(KeyAscii)
        Case ",", vbLf, vbCr
            ActNumInput = ""
            KeyAscii = 0
        Case Else
            If TabEditIsUpperCase Then KeyAscii = Asc(UCase(Chr(KeyAscii)))
    End Select
End Sub

Private Sub txtTabEditLookUp_LostFocus(Index As Integer)
    Dim CurKey As Integer
    Dim tmpText As String
    Dim CurCol As Long
    Dim RetVal As Boolean
    If chkTool(1).Value = 1 Then
        'txtTabEditLookUp_KeyDown Index, vbCr,  0
    End If
    CurKey = keyCheck
    txtTabEditLookUp(Index).Visible = False
    If Trim(txtTabEditLookUp(Index).Text) <> "" Then TabEditLookUpValueCol = TabEditColNo
    TabLookUp(Index).SetColumnValue TabEditLineNo, TabEditColNo, txtTabEditLookUp(Index).Text
    Select Case CurKey
        Case vbKeyTab
            SetLookUpKeyValues Index
            If KeybdIsShift = False Then
                If TabEditColNo < (TabLookUp(Index).GetColumnCount - 1) Then TabEditColNo = TabEditColNo + 1
            Else
                If TabEditColNo > 0 Then TabEditColNo = TabEditColNo - 1
            End If
            SetTxtTabEditLookUpPosition Index
        Case Else
            If TabEditLookUpType = "LOOK" Then
                'RetVal = SetLookUpKeyValues(Index)
                TabLookUp(Index).ResetLineDecorations 0
                tmpText = ""
                If TabEditLookUpValueCol >= 0 Then tmpText = TabLookUp(Index).GetColumnValue(0, TabEditLookUpValueCol)
                If Trim(tmpText) = "" Then
                    TabEditLookUpValueCol = -1
                    For CurCol = 0 To TabLookUp(Index).GetColumnCount - 1
                        If Trim(TabLookUp(Index).GetColumnValue(0, CurCol)) <> "" Then
                            TabEditLookUpValueCol = CurCol
                            Exit For
                        End If
                    Next
                End If
                If TabEditLookUpValueCol >= 0 Then
                    TabLookUp(Index).SetDecorationObject 0, TabEditLookUpValueCol, "'S3'"
                    TabLookUp(Index).Refresh
                Else
                    If Index = TabEditLookUpFixed Then TabEditLookUpFixed = -1
                End If
                TabEditColNo = TabEditLookUpValueCol
                SetLookUpKeyValues Index
            End If
    End Select
    TabLookCol(Index) = TabEditLookUpValueCol
End Sub

Private Function SetLookUpKeyValues(Index) As Boolean
    Dim tmpCols As String
    Dim tmpKeys As String
    Dim tmpData As String
    Dim tmpLineTag As String
    Dim tmpTabTag As String
    Dim CurCol As Long
    Dim MaxCol As Long
    SetLookUpKeyValues = False
    tmpCols = ""
    tmpKeys = ""
    tmpLineTag = ""
    tmpTabTag = ""
    MaxCol = TabLookUp(Index).GetColumnCount - 1
    For CurCol = 0 To MaxCol
        tmpData = Trim(TabLookUp(Index).GetColumnValue(0, CurCol))
        If tmpData <> "" Then
            tmpCols = tmpCols & CStr(CurCol) & ","
            tmpKeys = tmpKeys & tmpData & ","
        End If
    Next
    If tmpCols <> "" Then
        tmpCols = Left(tmpCols, Len(tmpCols) - 1)
        tmpKeys = Left(tmpKeys, Len(tmpKeys) - 1)
        tmpLineTag = tmpLineTag & "{=COLS=}" & tmpCols
        tmpLineTag = tmpLineTag & "{=KEYS=}" & tmpKeys
        tmpTabTag = tmpTabTag & "{=IDX=}" & CStr(Index)
        tmpTabTag = tmpTabTag & "{=COL=}" & CStr(TabEditLookUpValueCol)
        tmpTabTag = tmpTabTag & "{=TYP=}" & TabEditLookUpType
        SetLookUpKeyValues = True
    Else
        If Index = TabEditLookUpIndex Then TabEditLookUpIndex = -1
    End If
    TabLookUp(Index).SetLineTag 0, tmpLineTag
    TabLookUp(Index).Tag = tmpTabTag
End Function

Private Function GetLookUpKeyValues(Index) As Boolean
    Dim tmpData As String
    Dim tmpTabTag As String
    GetLookUpKeyValues = False
    tmpTabTag = TabLookUp(Index).Tag
    If tmpTabTag <> "" Then
        'GetKeyItem tmpData, tmpTabTag, "{=IDX=}", "{="
        TabEditLookUpIndex = Index
        GetKeyItem tmpData, tmpTabTag, "{=COL=}", "{="
        TabEditLookUpValueCol = Val(tmpData)
        GetKeyItem TabEditLookUpType, tmpTabTag, "{=TYP=}", "{="
        Set TabEditLookUpTabObj = TabLookUp(Index)
        GetLookUpKeyValues = True
    End If
    TabLookCol(Index) = TabEditLookUpValueCol
End Function


Private Function GetLookUpColsAndKeys(Index As Integer, KeyCols As String, KeyVals As String) As Integer
    Dim tmpTag As String
    KeyCols = ""
    KeyVals = ""
    If Index >= 0 Then
        tmpTag = TabLookUp(Index).GetLineTag(0)
        GetKeyItem KeyCols, tmpTag, "{=COLS=}", "{="
        GetKeyItem KeyVals, tmpTag, "{=KEYS=}", "{="
    End If
    GetLookUpColsAndKeys = ItemCount(KeyCols, ",")
End Function


