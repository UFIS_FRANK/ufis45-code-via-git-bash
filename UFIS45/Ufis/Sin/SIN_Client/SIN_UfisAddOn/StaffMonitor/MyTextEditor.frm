VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form MyTextEditor 
   Caption         =   "Filter Text Editor"
   ClientHeight    =   3225
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   7500
   Icon            =   "MyTextEditor.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   3225
   ScaleWidth      =   7500
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   1
      Top             =   2910
      Width           =   7500
      _ExtentX        =   13229
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   10134
         EndProperty
      EndProperty
   End
   Begin VB.TextBox Text1 
      Height          =   2655
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Top             =   120
      Width           =   4125
   End
End
Attribute VB_Name = "MyTextEditor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim TextResult As String
Public Function EditThisText(UseText As String) As String
    Text1.Text = UseText
    Me.Show vbModal
    EditThisText = TextResult
End Function

Private Sub Form_Load()
    Text1.Top = 30
    Text1.Left = 30
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    TextResult = Text1.Text
End Sub

Private Sub Form_Resize()
    Dim NewSize As Long
    NewSize = Me.ScaleWidth - 60
    If NewSize > 150 Then Text1.Width = NewSize
    NewSize = Me.ScaleHeight - StatusBar1.Height - 30
    If NewSize > 150 Then Text1.Height = NewSize
End Sub
