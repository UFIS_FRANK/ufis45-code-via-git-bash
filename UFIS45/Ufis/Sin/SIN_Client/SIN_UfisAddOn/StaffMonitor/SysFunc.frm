VERSION 5.00
Begin VB.Form SysFunc 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Staff Monitor System Functions"
   ClientHeight    =   2640
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6480
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "SysFunc.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2640
   ScaleWidth      =   6480
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CheckBox ChkCfg 
      Caption         =   "Close this Window"
      Height          =   345
      Index           =   0
      Left            =   2370
      Style           =   1  'Graphical
      TabIndex        =   16
      TabStop         =   0   'False
      Top             =   2190
      Width           =   1845
   End
   Begin VB.CommandButton Command16 
      Caption         =   "Button"
      Enabled         =   0   'False
      Height          =   705
      Left            =   5880
      TabIndex        =   15
      Top             =   1530
      Visible         =   0   'False
      Width           =   705
   End
   Begin VB.CommandButton Command15 
      Caption         =   "Funny"
      Enabled         =   0   'False
      Height          =   465
      Left            =   4860
      TabIndex        =   14
      Top             =   1590
      Visible         =   0   'False
      Width           =   1545
   End
   Begin VB.CommandButton Command14 
      Caption         =   "Set Keyboard On"
      Enabled         =   0   'False
      Height          =   465
      Left            =   3270
      TabIndex        =   13
      Top             =   1050
      Width           =   1545
   End
   Begin VB.CommandButton Command12 
      Caption         =   "Set Keyboard Off"
      Enabled         =   0   'False
      Height          =   465
      Left            =   3270
      TabIndex        =   12
      Top             =   570
      Width           =   1545
   End
   Begin VB.CommandButton Command11 
      Caption         =   "Desktop Info"
      Enabled         =   0   'False
      Height          =   465
      Left            =   1680
      TabIndex        =   11
      Top             =   90
      Width           =   1545
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Task Bar Info"
      Height          =   465
      Left            =   90
      TabIndex        =   10
      Top             =   90
      Width           =   1545
   End
   Begin VB.CommandButton Command4 
      Caption         =   "Hide Task Bar"
      Height          =   465
      Left            =   90
      TabIndex        =   9
      Top             =   570
      Width           =   1545
   End
   Begin VB.CommandButton Command5 
      Caption         =   "Show Task Bar"
      Height          =   465
      Left            =   90
      TabIndex        =   8
      Top             =   1050
      Width           =   1545
   End
   Begin VB.CommandButton Command6 
      Caption         =   "Hide Desktop"
      Height          =   465
      Left            =   1680
      TabIndex        =   7
      Top             =   570
      Width           =   1545
   End
   Begin VB.CommandButton Command7 
      Caption         =   "Show Desktop"
      Height          =   465
      Left            =   1680
      TabIndex        =   6
      Top             =   1050
      Width           =   1545
   End
   Begin VB.CommandButton Command8 
      Caption         =   "Keyboard Info"
      Height          =   465
      Left            =   3270
      TabIndex        =   5
      Top             =   90
      Width           =   1545
   End
   Begin VB.CommandButton Command9 
      Caption         =   "Hide Mouse"
      Enabled         =   0   'False
      Height          =   465
      Left            =   4860
      TabIndex        =   4
      Top             =   570
      Width           =   1545
   End
   Begin VB.CommandButton Command10 
      Caption         =   "Show Mouse"
      Enabled         =   0   'False
      Height          =   465
      Left            =   4860
      TabIndex        =   3
      Top             =   1050
      Width           =   1545
   End
   Begin VB.CommandButton Command13 
      Caption         =   "Mouse Info"
      Height          =   465
      Left            =   4860
      TabIndex        =   2
      Top             =   90
      Width           =   1545
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Lock WKS"
      Height          =   465
      Left            =   1680
      TabIndex        =   1
      Top             =   1560
      Width           =   1545
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Show Shutdown"
      Height          =   465
      Left            =   3270
      TabIndex        =   0
      Top             =   1560
      Width           =   1545
   End
End
Attribute VB_Name = "SysFunc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Declare Function LockWorkStation Lib "user32.dll" () As Long

Private Sub ChkCfg_Click(Index As Integer)
    Unload Me
End Sub

Private Sub Command1_Click()
    SetFormOnTop Me, False
     WinShut.Show , Me
     
End Sub
 
Private Sub Command14_Click()
    'chkIcon.Picture = Me.Icon
    'chkFunc.Picture = Me.Icon
    'SysFunc.Show , Me
End Sub

Private Sub Command2_Click()
    'KPD-Team 2000
    'URL: http://www.allapi.net/
    'E-Mail: KPDTeam@allapi.net
    LockWorkStation
End Sub

Private Sub Command10_Click()
    If Not ShowMousePointer Then
        ShowMyMouse
    End If
End Sub
Public Sub ShowMyMouse()
    ShowCursor& 1
End Sub

Sub DisableCtrlAltDelete(bDisabled As Boolean)
Dim X As Long
X = SystemParametersInfo(97, bDisabled, CStr(1), 0)
End Sub

Private Sub Command12_Click()
DisableCtrlAltDelete (False)
End Sub

Private Sub Command11_Click()
DisableCtrlAltDelete (True)
End Sub

Private Sub Command13_Click()
MsgBox "The check of mouse existence returned: " & CheckMouse
End Sub


Function CircleIt(Contrl As Control)
Dim hwndDest
Dim hr&, dl&
Dim usew&, useh&
hwndDest = Contrl.hwnd
usew& = Contrl.Width / Screen.TwipsPerPixelX
useh& = Contrl.Height / Screen.TwipsPerPixelY
hr& = CreateEllipticRgn(0, 0, usew&, useh&)
dl& = SetWindowRgn(hwndDest, hr, True)
End Function

Private Sub Command15_Click()
'CircleIt Command16
'Command16.Visible = True
'Command16.ZOrder
End Sub

Private Sub Command3_Click()
Dim rc As Rect
Dim ABD As APPBARDATA
Dim state As Long
Dim position As Integer
Dim hWndAppBar As Long
Dim Msg As String
SetFormOnTop Me, False
'initialize the APPBARDATA structure
ABD.cbSize = Len(ABD)
'get the appbar state
state = SHAppBarMessage(ABM_GETSTATE, ABD)
'prepare the appropriate message based on the returned state
Select Case state
Case False
Msg = Msg & " - Auto Hide= False, Always on Top = False." & vbCrLf
Msg = Msg & " - User allows apps cover the taskbar." & vbCrLf
Msg = Msg & " - The taskbar must be manually invoked with maximized apps."
Case ABS_ALWAYSONTOP
Msg = Msg & " - Always on Top = True." & vbCrLf
Msg = Msg & " - User wants the taskbar on-screen at all times." & vbCrLf
Msg = Msg & " - The available screen is reduced by the taskbar size."
Case Else
Msg = Msg & " - Auto Hide = True." & vbCrLf
Msg = Msg & " - The taskbar appears on a mousemove." & vbCrLf
Msg = Msg & " - There are taskbar(s) positioned on the "
'see which edge has a taskbar
For position = ABE_LEFT To ABE_BOTTOM
ABD.uEdge = position
hWndAppBar = SHAppBarMessage(ABM_GETAUTOHIDEBAR, ABD)
If hWndAppBar > 0 Then
Select Case position
Case ABE_LEFT: Msg = Msg & "LEFT "
Case ABE_TOP: Msg = Msg & "TOP "
Case ABE_RIGHT: Msg = Msg & "RIGHT "
Case ABE_BOTTOM: Msg = Msg & "BOTTOM "
End Select
End If
Next
End Select
'display the results
MsgBox Msg
End Sub


Private Sub Command4_Click()
    HideMyTaskBar
End Sub
Public Sub HideMyTaskBar()
    Dim hwnd1 As Long
    hwnd1 = FindWindow("Shell_traywnd", "")
    Call SetWindowPos(hwnd1, 0, 0, 0, 0, 0, SWP_HIDEWINDOW)
End Sub
Private Sub Command5_Click()
    ShowMyTaskBar
End Sub
Public Sub ShowMyTaskBar()
    Dim hwnd1 As Long
    hwnd1 = FindWindow("Shell_traywnd", "")
    Call SetWindowPos(hwnd1, 0, 0, 0, 0, 0, SWP_SHOWWINDOW)
End Sub
Private Sub Command6_Click()
    HideMyDesktop
End Sub
Public Sub HideMyDesktop()
    Dim hHandle As Long
    hHandle = FindWindow("progman", vbNullString)
    Call ShowWindow(hHandle, SW_HIDE)
End Sub
Private Sub Command7_Click()
    ShowMyDesktop
End Sub
Public Sub ShowMyDesktop()
    Dim hHandle As Long
    hHandle = FindWindow("progman", vbNullString)
    Call ShowWindow(hHandle, SW_NORMAL)
End Sub
Private Sub Command8_Click()
Dim t As String
Dim k As Long
k = GetKeyboardType(0)
If k = 1 Then t = "PC or compatible 83-key keyboard"
If k = 2 Then t = "Olivetti 102-key keyboard"
If k = 3 Then t = "AT or compatible 84-key keyboard"
If k = 4 Then t = "Enhanced(IBM) 101-102-key keyboard"
If k = 5 Then t = "Nokia 1050 keyboard"
If k = 6 Then t = "Nokia 9140 keyboard"
If k = 7 Then t = "Japanese keyboard"
MsgBox "Type of keyboard : " & t
End Sub

Private Sub Command9_Click()
    HideMyMouse
End Sub
Public Sub HideMyMouse()
    If Not ShowMousePointer Then
        ShowCursor& 0
    End If
End Sub

