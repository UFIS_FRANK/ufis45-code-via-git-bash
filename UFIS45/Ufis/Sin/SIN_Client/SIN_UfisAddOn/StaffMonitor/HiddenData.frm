VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "tab.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form HiddenData 
   Caption         =   "Hidden Data and Configuration"
   ClientHeight    =   7515
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   12285
   Icon            =   "HiddenData.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   7515
   ScaleWidth      =   12285
   StartUpPosition =   3  'Windows Default
   Begin TABLib.TAB UrnoList 
      Height          =   4965
      Index           =   0
      Left            =   8430
      TabIndex        =   7
      Top             =   420
      Width           =   1185
      _Version        =   65536
      _ExtentX        =   2090
      _ExtentY        =   8758
      _StockProps     =   64
   End
   Begin TABLib.TAB TableDataTab 
      Height          =   1575
      Index           =   0
      Left            =   60
      TabIndex        =   5
      Top             =   3690
      Width           =   3015
      _Version        =   65536
      _ExtentX        =   5318
      _ExtentY        =   2778
      _StockProps     =   64
   End
   Begin TABLib.TAB FormCfgTab 
      Height          =   1575
      Left            =   60
      TabIndex        =   4
      Top             =   420
      Width           =   3465
      _Version        =   65536
      _ExtentX        =   6112
      _ExtentY        =   2778
      _StockProps     =   64
   End
   Begin TABLib.TAB BasicDataTab 
      Height          =   1545
      Index           =   0
      Left            =   60
      TabIndex        =   3
      Top             =   2070
      Width           =   3015
      _Version        =   65536
      _ExtentX        =   5318
      _ExtentY        =   2725
      _StockProps     =   64
   End
   Begin TABLib.TAB TabCfgTab 
      Height          =   1575
      Left            =   3600
      TabIndex        =   2
      Top             =   420
      Width           =   4305
      _Version        =   65536
      _ExtentX        =   7594
      _ExtentY        =   2778
      _StockProps     =   64
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   315
      Left            =   0
      TabIndex        =   1
      Top             =   7200
      Width           =   12285
      _ExtentX        =   21669
      _ExtentY        =   556
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   21140
         EndProperty
      EndProperty
   End
   Begin VB.CheckBox chkClose 
      Caption         =   "Close"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   30
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   30
      Width           =   855
   End
   Begin TABLib.TAB FilterTab 
      Height          =   1575
      Index           =   0
      Left            =   60
      TabIndex        =   6
      Top             =   5340
      Width           =   3015
      _Version        =   65536
      _ExtentX        =   5318
      _ExtentY        =   2778
      _StockProps     =   64
   End
End
Attribute VB_Name = "HiddenData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim TabMaximized As Boolean
Dim CurMaxTab As TABLib.TAB
Dim PreFetchFilters As Integer

Public Function CheckTabConfig(TabCfgLine As String) As String
    Dim Result As String
    Dim KeyValue As String
    Dim LineNo As Long
    Result = ""
    KeyValue = GetRealItem(TabCfgLine, 0, Chr(15))
    If KeyValue <> "" Then
        TabCfgTab.IndexCreate "KEYCODE", 0
        TabCfgTab.SetInternalLineBuffer True
        LineNo = Val(TabCfgTab.GetLinesByIndexValue("KEYCODE", KeyValue, 0))
        If LineNo > 0 Then
            If LineNo = 1 Then
                LineNo = TabCfgTab.GetNextResultLine
                TabCfgTab.UpdateTextLine LineNo, TabCfgLine, False
            Else
                MsgBox "TAB_KEY [" & KeyValue & "] not unique !!"
            End If
        Else
            TabCfgTab.InsertTextLine TabCfgLine, False
        End If
        'TabCfgTab.AutoSizeColumns
        'TabCfgTab.Refresh
        TabCfgTab.SetInternalLineBuffer False
    End If
    CheckTabConfig = Result
End Function

Public Function UpdateTabConfig(TabCode As String, FieldList As String, ValueList As String) As String
    Dim Result As String
    Dim LineNo As Long
    Dim ColNo As Long
    Dim tmpLine As String
    Dim tmpFld As String
    Dim tmpVal As String
    Dim tmpList As String
    Dim i As Integer
    Result = ""
    If TabCode <> "" Then
        TabCfgTab.IndexCreate "KEYCODE", 0
        TabCfgTab.SetInternalLineBuffer True
        LineNo = Val(TabCfgTab.GetLinesByIndexValue("KEYCODE", TabCode, 0))
        If LineNo > 0 Then
            If LineNo = 1 Then
                LineNo = TabCfgTab.GetNextResultLine
                TabCfgTab.SetFieldValues LineNo, FieldList, ValueList
            Else
                MsgBox "TAB_KEY [" & TabCode & "] Found=" & CStr(LineNo) & " !!"
            End If
            TabCfgTab.SetInternalLineBuffer False
        Else
            TabCfgTab.SetInternalLineBuffer False
            tmpLine = CreateEmptyLine(TabCfgTab.LogicalFieldList)
            tmpLine = Replace(tmpLine, ",", Chr(15), 1, -1, vbBinaryCompare)
            TabCfgTab.InsertTextLine tmpLine, False
            LineNo = TabCfgTab.GetLineCount - 1
            'Due to Macke in tab.ocx
            tmpFld = "START"
            i = 0
            While tmpFld <> ""
                i = i + 1
                tmpFld = GetItem(FieldList, i, Chr(15))
                tmpVal = GetItem(ValueList, i, Chr(15))
                If tmpFld <> "" Then
                    ColNo = CLng(GetRealItemNo(TabCfgTab.LogicalFieldList, tmpFld))
                    If ColNo >= 0 Then TabCfgTab.SetColumnValue LineNo, ColNo, tmpVal
                End If
            Wend
            'tmpList = Replace(FieldList, Chr(15), ",", 1, -1, vbBinaryCompare)
            'TabCfgTab.SetFieldValues LineNo, tmpList, ValueList
        End If
        'TabCfgTab.AutoSizeColumns
        'TabCfgTab.Refresh
    End If
    UpdateTabConfig = Result
End Function

Public Sub RemoveImpConfig()
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim tmpType As String
    MaxLine = TabCfgTab.GetLineCount - 1
    While CurLine <= MaxLine
        tmpType = TabCfgTab.GetFieldValue(CurLine, "TYPE")
        If InStr("IMP,SEL,DAT", tmpType) > 0 Then
            TabCfgTab.DeleteLine CurLine
            MaxLine = MaxLine - 1
            CurLine = CurLine - 1
        End If
        CurLine = CurLine + 1
    Wend
End Sub

Public Function CreateFilterTab(Index As Integer) As String
    Dim TabCfgLine As String
    If Index > FilterTab.UBound Then
        Load FilterTab(Index)
        FilterTab(Index).Visible = True
        FilterTab(Index).Left = (FilterTab(Index).Width + 60) * Index + 120
    End If
    FilterTab(Index).ResetContent
    FilterTab(Index).LogicalFieldList = ""
    FilterTab(Index).HeaderString = "Data" & Space(200)
    FilterTab(Index).HeaderLengthString = "10"
    FilterTab(Index).ColumnWidthString = "10"
    FilterTab(Index).SetMainHeaderValues "1", "Filter TAB Data", ""
    FilterTab(Index).MainHeader = True
    FilterTab(Index).EnableHeaderSizing True
    FilterTab(Index).AutoSizeByHeader = True
    FilterTab(Index).AutoSizeColumns
    FilterTab(Index).myName = "HIDDEN_FILTER_" & CStr(Index)
    TabCfgLine = FilterTab(Index).myName & Chr(15)
    TabCfgLine = TabCfgLine & "0" & Chr(15)
    TabCfgLine = TabCfgLine & "V" & Chr(15)
    TabCfgLine = TabCfgLine & "DAT" & Chr(15)
    TabCfgLine = TabCfgLine & "AODB" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & FilterTab(Index).LogicalFieldList & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    CheckTabConfig TabCfgLine
    CreateFilterTab = FilterTab(Index).myName
End Function

Public Function CreateBasicDataTab(Index As Integer, TableName As String, TableFields As String, SqlKey) As String
    Dim TabCfgLine As String
    If Index > BasicDataTab.UBound Then
        Load BasicDataTab(Index)
        BasicDataTab(Index).Visible = True
        BasicDataTab(Index).Left = (BasicDataTab(Index).Width + 60) * Index + 120
    End If
    BasicDataTab(Index).ResetContent
    BasicDataTab(Index).LogicalFieldList = TableFields
    BasicDataTab(Index).HeaderString = TableFields
    BasicDataTab(Index).HeaderLengthString = "10,10,10"
    BasicDataTab(Index).ColumnWidthString = "10,10,10"
    BasicDataTab(Index).SetMainHeaderValues "3", TableName & " Basic Data", ""
    BasicDataTab(Index).MainHeader = True
    BasicDataTab(Index).EnableHeaderSizing True
    BasicDataTab(Index).AutoSizeByHeader = True
    BasicDataTab(Index).AutoSizeColumns
    BasicDataTab(Index).myName = "BASIC_" & TableName
    TabCfgLine = BasicDataTab(Index).myName & Chr(15)
    TabCfgLine = TabCfgLine & "0" & Chr(15)
    TabCfgLine = TabCfgLine & "V" & Chr(15)
    TabCfgLine = TabCfgLine & "BAS" & Chr(15)
    TabCfgLine = TabCfgLine & "AODB" & Chr(15)
    TabCfgLine = TabCfgLine & TableName & Chr(15)
    TabCfgLine = TabCfgLine & TableFields & Chr(15)
    TabCfgLine = TabCfgLine & SqlKey & Chr(15)
    TabCfgLine = TabCfgLine & BasicDataTab(Index).LogicalFieldList & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    CheckTabConfig TabCfgLine
    TabCfgLine = "" & vbNewLine
    TabCfgLine = TabCfgLine & "[=IDX=]" & CStr(Index) & "[/=IDX=]" & vbNewLine
    TabCfgLine = TabCfgLine & "[=TAB=]" & TableName & "[/=TAB=]" & vbNewLine
    TabCfgLine = TabCfgLine & "[=FLD=]" & TableFields & "[/=FLD=]" & vbNewLine
    TabCfgLine = TabCfgLine & "[=SQL=]" & SqlKey & "[/=SQL=]"
    BasicDataTab(Index).myTag = TabCfgLine
    CreateBasicDataTab = BasicDataTab(Index).myName
End Function
Public Function GetConfigValues(FilterTabCode As String, CfgFields As String) As String
    Dim Result As String
    Dim TabNo As Long
    Result = ""
    TabNo = GetCfgTabLineNo(FilterTabCode)
    If TabNo >= 0 Then
        Result = TabCfgTab.GetFieldValues(TabNo, CfgFields)
    End If
    GetConfigValues = Result
End Function
Public Function GetFilterData(FilterTabCode As String, BufferFields As String, PatchCodes As String, PatchValues As String) As String
    Dim Result As String
    Dim DataTab As String
    Dim TableName As String
    Dim FldLst As String
    Dim LookField As String
    Dim GetField As String
    Dim LookValue As String
    Dim GetValue As String
    Dim TabNo As Long
    Dim LineNo As Long
    Dim CurLine As Long
    Dim i As Integer
    Result = ""
    TabNo = GetCfgTabLineNo(FilterTabCode)
    If TabNo >= 0 Then
        DataTab = TabCfgTab.GetFieldValue(TabNo, "DSRC")
        For i = 0 To FilterTab.UBound
            If FilterTab(i).myName = DataTab Then
                ReadAodbData FilterTab(i), True, "0", PatchCodes, PatchValues, True, False, -1
                LineNo = FilterTab(i).GetLineCount - 1
                If LineNo >= 0 Then
                    FldLst = FilterTab(i).LogicalFieldList
                    LookField = GetRealItem(FldLst, 0, ",")
                    GetField = GetRealItem(FldLst, 1, ",")
                    Select Case LookField
                        Case "ALC3"
                            TableName = "ALTTAB"
                        Case "APC3"
                            TableName = "APTTAB"
                        Case "ACT3"
                            TableName = "ACTTAB"
                        Case Else
                            TableName = ""
                    End Select
                    If TableName <> "" Then
                        For CurLine = 0 To LineNo
                            LookValue = FilterTab(i).GetColumnValue(CurLine, 0)
                            GetValue = BasicDataLookUp(TableName, LookField, LookValue, GetField, True)
                            FilterTab(i).SetColumnValue CurLine, 1, GetValue
                        Next
                    Else
                        If LookField = "FLNO" Then
                            For CurLine = 0 To LineNo
                                LookValue = FilterTab(i).GetColumnValue(CurLine, 0)
                                LookValue = Trim(Left(LookValue, 3))
                                If Len(LookValue) = 2 Then
                                    GetValue = BasicDataLookUp("ALTTAB", "ALC2", LookValue, "ALC3", True)
                                Else
                                    GetValue = BasicDataLookUp("ALTTAB", "ALC3", LookValue, "ALC2", True)
                                End If
                                FilterTab(i).SetColumnValue CurLine, 1, GetValue
                            Next
                        End If
                    End If
                    Result = FilterTab(i).GetBufferByFieldList(0, LineNo, BufferFields, vbLf)
                End If
                Exit For
            End If
        Next
    End If
    GetFilterData = Result
End Function

Public Sub ReadAodbData(CurTab As TABLib.TAB, SetHeader As Boolean, UniqueColumns As String, PatchCodes As String, PatchValues As String, AppendUnknown As Boolean, CheckPreFetch As Boolean, ReadSteps As Long)
    Dim TblNam As String
    Dim CurFldLst As String
    Dim FldLst As String
    Dim LogFld As String
    Dim CurSqlKey As String
    Dim AddSqlKey As String
    Dim UseSqlKey As String
    Dim SqlKey As String
    Dim PatchName As String
    Dim CheckPatch As String
    Dim PatchData As String
    Dim tmpZone As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    Dim tmpTifr As String
    Dim tmpTito As String
    Dim SqlVpfr As String
    Dim SqlVpto As String
    Dim SqlTifr As String
    Dim SqlTito As String
    Dim BgnTime As String
    Dim EndTime As String
    Dim tmpInf1 As String
    Dim tmpInf2 As String
    Dim KeyName As String
    Dim KeyIndx As String
    Dim KeyList As String
    Dim LineNo As Long
    Dim ItemNo As Long
    Dim SqlItemNo As Long
    Dim ReadDays As Long
    Dim CurKeyLine As Long
    Dim SigIdx As Integer
    Dim KeyIdx As Integer
    Dim KeyPos As Integer
    Dim KeyType As Integer
    Dim KeySize As Integer
    Dim idx As Integer
    Dim ShowProgInfo As Boolean
    Dim SqlKeyPeriod As Boolean
    Dim FirstDay
    Dim LastDay
    Dim ReadFrom
    Dim ReadTo

    ShowProgInfo = FormIsVisible("WorkProgress")
    CurTab.ResetContent
    LineNo = GetCfgTabLineNo(CurTab.myName)
    If LineNo >= 0 Then
        TblNam = TabCfgTab.GetFieldValue(LineNo, "DTAB")
        FldLst = TabCfgTab.GetFieldValue(LineNo, "DFLD")
        LogFld = TabCfgTab.GetFieldValue(LineNo, "LFLD")
        If LogFld = "" Then LogFld = FldLst
        If FldLst <> "" Then
            SigIdx = 1
            Screen.MousePointer = 11
            CurTab.ResetContent
            If SetHeader Then
                CurTab.HeaderString = LogFld
                CurTab.LogicalFieldList = LogFld
            End If
            'CurTab.Refresh
            idx = CurCedaIdx + 1
            CurTab.CedaCurrentApplication = "STFMONVDU" & "," & "4.5.1.4"
            CurTab.CedaHopo = HomeAirport
            CurTab.CedaIdentifier = "IDX"
            CurTab.CedaPort = "3357"
            CurTab.CedaReceiveTimeout = "250"
            CurTab.CedaRecordSeparator = vbLf
            CurTab.CedaSendTimeout = "250"
            CurTab.CedaServerName = MyHostName
            CurTab.CedaTabext = "TAB"
            CurTab.CedaUser = "STFMONVDU"
            CurTab.CedaWorkstation = MyWksName
            CurTab.CedaPacketSize = 500
            CurTab.SetUniqueFields UniqueColumns
            
            ServerIsAvailable = True
            If ServerIsAvailable Then
                'ToggleServerButtons idx, 1
                DoEvents
                SqlKey = TabCfgTab.GetFieldValue(LineNo, "DSQL")
                AddSqlKey = ""
                SqlItemNo = 0
                CurFldLst = GetRealItem(FldLst, SqlItemNo, Chr(14))
                While CurFldLst <> ""
                    If InStr(CurFldLst, "'") > 0 Then
                        CurFldLst = GetCleanFieldList(CurFldLst)
                    End If
                    CurSqlKey = GetRealItem(SqlKey, SqlItemNo, Chr(14))
                    tmpVpfr = ""
                    tmpVpto = ""
                    tmpTifr = ""
                    tmpTito = ""
                    SqlVpfr = ""
                    SqlVpto = ""
                    SqlTifr = ""
                    SqlTito = ""
                    ItemNo = 0
                    PatchName = GetRealItem(PatchCodes, ItemNo, Chr(15))
                    While PatchName <> ""
                        CheckPatch = "[" & PatchName & "]"
                        PatchData = GetRealItem(PatchValues, ItemNo, Chr(15))
                        Select Case PatchName
                            Case "ZONE"
                                tmpZone = PatchData
                            Case "VPFR"
                                tmpVpfr = PatchData
                                SqlVpfr = PatchData
                            Case "VPTO"
                                tmpVpto = PatchData
                                SqlVpto = PatchData
                            Case "TIFR"
                                tmpTifr = PatchData
                                SqlTifr = PatchData
                            Case "TITO"
                                tmpTito = PatchData
                                SqlTito = PatchData
                            Case Else
                                If AppendUnknown Then
                                    If InStr(CurSqlKey, CheckPatch) = 0 Then
                                        Select Case PatchName
                                            Case "APC3"
                                                AddSqlKey = AddSqlKey & " AND (ORG3 IN (" & PatchData & ") OR DES3 IN (" & PatchData & "))"
                                            'Do not auto-append these
                                            Case "HOPO"
                                            Case "TIFR"
                                            Case "TITO"
                                            Case Else
                                                AddSqlKey = AddSqlKey & " AND " & PatchName & " IN (" & PatchData & ")"
                                        End Select
                                    Else
                                        Select Case PatchName
                                            Case "TIFR"
                                                If Len(PatchData) = 4 Then PatchData = PatchData & "00"
                                            Case "TITO"
                                                If Len(PatchData) = 4 Then PatchData = PatchData & "59"
                                            Case Else
                                        End Select
                                    End If
                                End If
                                CurSqlKey = Replace(CurSqlKey, CheckPatch, PatchData, 1, -1, vbBinaryCompare)
                        End Select
                        ItemNo = ItemNo + 1
                        PatchName = GetRealItem(PatchCodes, ItemNo, Chr(15))
                    Wend
                    CheckPatch = "[NO_FILTER]"
                    If InStr(CurSqlKey, CheckPatch) > 0 Then
                        CurSqlKey = Replace(CurSqlKey, CheckPatch, "", 1, -1, vbBinaryCompare)
                        AddSqlKey = ""
                    End If
                    CheckPatch = "[FILTER]"
                    If InStr(CurSqlKey, CheckPatch) > 0 Then
                        CurSqlKey = Replace(CurSqlKey, CheckPatch, AddSqlKey, 1, -1, vbBinaryCompare)
                        AddSqlKey = ""
                    End If
                    If CurSqlKey <> "" Then CurSqlKey = "WHERE " & CurSqlKey
                    If AddSqlKey <> "" Then
                        CurSqlKey = CurSqlKey & AddSqlKey
                        AddSqlKey = ""
                    End If
                    SqlKeyPeriod = False
                    If InStr(CurSqlKey, "[VPFR]") > 0 Then SqlKeyPeriod = True
                    If InStr(CurSqlKey, "[VPTO]") > 0 Then SqlKeyPeriod = True
                    If InStr(CurSqlKey, "[TIFR]") > 0 Then SqlKeyPeriod = True
                    If InStr(CurSqlKey, "[TITO]") > 0 Then SqlKeyPeriod = True
                    If (tmpVpfr = "") Or (SqlKeyPeriod = False) Then
                        KeyIdx = -1
                        CurKeyLine = -1
                        UseSqlKey = CurSqlKey
                        KeyType = 0
                        KeyPos = InStr(UseSqlKey, "[LIST_")
                        If KeyPos > 0 Then
                            KeyName = Mid(UseSqlKey, KeyPos, 9)
                            KeyIndx = Mid(KeyName, 7, 2)
                            KeyIdx = Val(KeyIndx)
                            KeyIdx = (PreFetchFilters * (CurCedaIdx + 1)) - PreFetchFilters + KeyIdx
                            KeyList = "START"
                            While KeyList <> ""
                                KeyList = GetSqlKeyList(KeyIdx, CurKeyLine, KeyType)
                                If KeyList <> "" Then
                                    UseSqlKey = Replace(CurSqlKey, KeyName, KeyList, 1, -1, vbBinaryCompare)
                                    'MainDialog.AdjustServerSignals True, SigIdx
                                    CurTab.CedaAction "RTA", TblNam, CurFldLst, "", UseSqlKey
                                    'MainDialog.AdjustServerSignals False, SigIdx
                                End If
                            Wend
                        Else
                            'MainDialog.AdjustServerSignals True, SigIdx
                            CurTab.CedaAction "RTA", TblNam, CurFldLst, "", UseSqlKey
                            'MainDialog.AdjustServerSignals False, SigIdx
                        End If
                    Else
                        BgnTime = SqlVpfr & SqlTifr
                        EndTime = SqlVpto & SqlTito
                        FirstDay = CedaFullDateToVb(BgnTime)
                        LastDay = CedaFullDateToVb(EndTime)
                        'ReadDays = 7
                        ReadDays = ReadSteps
                        If ReadDays < 0 Then ReadDays = 999
                        If ReadDays < 5 Then ReadDays = 5
                        ReadFrom = FirstDay
                        Do
                            ReadTo = DateAdd("d", (ReadDays - 1), ReadFrom)
                            ReadTo = DateAdd("n", -1, ReadTo)
                            If ReadTo > LastDay Then ReadTo = LastDay
                            tmpVpfr = Format(ReadFrom, "yyyymmdd")
                            tmpVpto = Format(ReadTo, "yyyymmdd")
                            UseSqlKey = Replace(CurSqlKey, "[VPFR]", tmpVpfr, 1, -1, vbBinaryCompare)
                            UseSqlKey = Replace(UseSqlKey, "[VPTO]", tmpVpto, 1, -1, vbBinaryCompare)
                            tmpTifr = Format(ReadFrom, "hhmm") & "00"
                            tmpTito = Format(ReadTo, "hhmm") & "59"
                            UseSqlKey = Replace(UseSqlKey, "[TIFR]", tmpTifr, 1, -1, vbBinaryCompare)
                            UseSqlKey = Replace(UseSqlKey, "[TITO]", tmpTito, 1, -1, vbBinaryCompare)
                            'MainDialog.AdjustServerSignals True, SigIdx
                            If ShowProgInfo Then
                                tmpInf1 = Format(ReadFrom, "yyyy.mm.dd / hh:mm")
                                tmpInf2 = Format(ReadTo, "yyyy.mm.dd / hh:mm")
                                'WorkProgress.SetContextInfo tmpInf1, tmpInf2
                            End If
                            DoEvents
                            CurTab.CedaAction "RTA", TblNam, CurFldLst, "", UseSqlKey
                            'MainDialog.AdjustServerSignals False, SigIdx
                            DoEvents
                            SigIdx = SigIdx + 1
                            If SigIdx > 2 Then SigIdx = 1
                            ReadFrom = DateAdd("n", 1, ReadTo)
                            'If StopAllLoops Then ReadFrom = DateAdd("d", 1, LastDay)
                        Loop While (ReadFrom <= LastDay) ' And (Not CedaError)
                    End If
                    SqlItemNo = SqlItemNo + 1
                    CurFldLst = GetRealItem(FldLst, SqlItemNo, Chr(14))
                    CurSqlKey = GetRealItem(SqlKey, SqlItemNo, Chr(14))
                    'If StopAllLoops Then CurFldLst = ""
                    DoEvents
                Wend
                'ToggleServerButtons idx, 0
                DoEvents
            Else
                'If HostName = "LOCAL" Then CurTab.ReadFromFile "c:\ufis\basicdata" & Trim(Str(Index)) & ".txt"
            End If
            'CurTab.AutoSizeColumns
            'CurTab.Refresh
            Screen.MousePointer = 0
        End If
    End If
    Screen.MousePointer = 0
End Sub
Private Function GetCleanFieldList(UseFldList As String) As String
    Dim OutFld As String
    Dim GetFld As String
    Dim PutFld As String
    Dim fld As Integer
    If InStr(UseFldList, "'") > 0 Then
        OutFld = ""
        fld = 0
        GetFld = "START"
        While GetFld <> ""
            fld = fld + 1
            GetFld = GetItem(UseFldList, fld, ",")
            If GetFld <> "" Then
                If Left(GetFld, 1) = "'" Then
                    Select Case GetFld
                        Case "'CB'"
                            PutFld = "'N'"
                        Case "'BLT1'", "'BLT2'", "'GAT1'", "'GAT2'"
                            'Do nothing
                            PutFld = GetFld
                        Case Else
                            PutFld = "' '"
                    End Select
                Else
                    PutFld = GetFld
                End If
                OutFld = OutFld & PutFld & ","
            End If
        Wend
        OutFld = Left(OutFld, Len(OutFld) - 1)
    Else
        OutFld = UseFldList
    End If
    GetCleanFieldList = OutFld
End Function
Private Function GetSqlKeyList(KeyIdx As Integer, KeyLineNo As Long, KeyType As Integer) As String
    Dim KeyList As String
    Dim KeyData As String
    Dim BgnKeyLine As Long
    Dim CurKeyLine As Long
    Dim EndKeyLine As Long
    Dim MaxKeyLine As Long
    Dim KeyCount As Long
    On Error Resume Next
    BgnKeyLine = KeyLineNo + 1
    MaxKeyLine = UrnoList(KeyIdx).GetLineCount - 1
    EndKeyLine = KeyLineNo + 500
    If EndKeyLine > MaxKeyLine Then EndKeyLine = MaxKeyLine
    For CurKeyLine = BgnKeyLine To EndKeyLine
        KeyData = UrnoList(KeyIdx).GetColumnValue(CurKeyLine, 0)
        If KeyType = 1 Then
            'ASCII: Append prefix/suffix
            KeyData = "'" & KeyData & "'"
        End If
        KeyList = KeyList & KeyData & ","
    Next
    If KeyList <> "" Then KeyList = Left(KeyList, Len(KeyList) - 1)
    KeyLineNo = EndKeyLine
    GetSqlKeyList = KeyList
End Function
Public Sub PreFetchAodbData(PatchCodes As String, PatchValues As String, AppendUnknown As Boolean, ReadSteps As Long)
    Dim CurTab As TABLib.TAB
    Dim UniqueColumns As String
    Dim TblNam As String
    Dim CurFldLst As String
    Dim FldLst As String
    Dim CurSqlKey As String
    Dim AddSqlKey As String
    Dim UseSqlKey As String
    Dim SqlKey As String
    Dim PatchName As String
    Dim CheckPatch As String
    Dim PatchData As String
    Dim tmpZone As String
    Dim tmpVpfr As String
    Dim tmpVpto As String
    Dim tmpTifr As String
    Dim tmpTito As String
    Dim SqlVpfr As String
    Dim SqlVpto As String
    Dim SqlTifr As String
    Dim SqlTito As String
    Dim BgnTime As String
    Dim EndTime As String
    Dim tmpInf1 As String
    Dim tmpInf2 As String
    Dim tmpData As String
    Dim tmpGrpName As String
    Dim tmpGrpText As String
    Dim tmpGrpTask As String
    Dim tmpGrpCode As String
    Dim tmpIniName As String
    Dim tmpGrpIndx As String
    Dim ReadFilter As String
    Dim KeyName As String
    Dim KeyIndx As String
    Dim KeyList As String
    Dim LineNo As Long
    Dim ItemNo As Long
    Dim SqlItemNo As Long
    Dim ReadDays As Long
    Dim SigIdx As Integer
    Dim PreCnt As Integer
    Dim PreGrp As Integer
    Dim GrpCnt As Integer
    Dim GrpIdx As Integer
    Dim CurIdx As Integer
    Dim CurLoop As Integer
    
    Dim CurKeyLine As Long
    Dim KeyIdx As Integer
    Dim KeyPos As Integer
    Dim KeyType As Integer
    Dim KeySize As Integer
    
    Dim idx As Integer
    Dim ShowProgInfo As Boolean
    Dim SqlKeyPeriod As Boolean
    Dim DefLoad As Boolean
    Dim FirstDay
    Dim LastDay
    Dim ReadFrom
    Dim ReadTo
    On Error Resume Next
    If AodbReaderSection <> "" Then
        ShowProgInfo = FormIsVisible("WorkProgress")
        For CurLoop = 1 To 2
            tmpData = GetIniEntry(myIniFullName, AodbReaderSection, "", "KEY_DATA_LISTS", "0")
            PreCnt = Val(tmpData)
            PreFetchFilters = PreCnt
            If PreCnt > 0 Then
                CurIdx = (PreFetchFilters * (CurCedaIdx + 1)) - PreFetchFilters
                For PreGrp = 1 To PreCnt
                    CurIdx = CurIdx + 1
                    'Init the UrnoList (TAB.OCX)
                    'If it already exists we'll resume next on error
                    Load UrnoList(CurIdx)
                    Set CurTab = UrnoList(CurIdx)
                    If CurLoop = 1 Then
                        UniqueColumns = "0"
                        CurTab.ResetContent
                        CurTab.HeaderString = "URNO"
                        CurTab.LogicalFieldList = "URNO"
                        CurTab.HeaderLengthString = "100"
                        CurTab.ColumnWidthString = "10"
                        CurTab.ColumnAlignmentString = "R"
                        CurTab.SetUniqueFields UniqueColumns
                        
                        CurTab.CedaCurrentApplication = "STFMONVDU" & "," & "4.5.1.4"
                        CurTab.CedaHopo = HomeAirport
                        CurTab.CedaIdentifier = "IDX"
                        CurTab.CedaPort = "3357"
                        CurTab.CedaReceiveTimeout = "250"
                        CurTab.CedaRecordSeparator = vbLf
                        CurTab.CedaSendTimeout = "250"
                        CurTab.CedaServerName = MyHostName
                        CurTab.CedaTabext = "TAB"
                        CurTab.CedaUser = "STFMONVDU"
                        CurTab.CedaWorkstation = MyWksName
                        CurTab.CedaPacketSize = 500
                    End If
                    CurTab.ShowHorzScroller False
                    
                    tmpGrpIndx = Right("00" & CStr(PreGrp), 2)
                    tmpGrpName = "LIST_" & tmpGrpIndx
                    tmpGrpText = "List " & tmpGrpIndx
                    
                    tmpIniName = tmpGrpName & "_LOADER"
                    tmpData = GetIniEntry(myIniFullName, AodbReaderSection, "", tmpIniName, "DEF")
                    If tmpData = "DEF" Then
                        GrpCnt = 1
                        DefLoad = True
                    Else
                        GrpCnt = Val(tmpData)
                        DefLoad = False
                    End If
                    If GrpCnt > 0 Then
                        For GrpIdx = 1 To GrpCnt
                            If CurLoop = 1 Then
                                tmpGrpTask = "SQL Keys"
                                If DefLoad = True Then tmpIniName = tmpGrpName & "_READER" Else tmpIniName = tmpGrpName & "_READ_" & CStr(GrpIdx)
                                ReadFilter = GetIniEntry(myIniFullName, AodbReaderSection, "", tmpIniName, "")
                                If (ReadFilter = "") And (GrpIdx = 1) Then
                                    If DefLoad = False Then tmpIniName = tmpGrpName & "_READER" Else tmpIniName = tmpGrpName & "_READ_" & CStr(GrpIdx)
                                    ReadFilter = GetIniEntry(myIniFullName, AodbReaderSection, "", tmpIniName, "")
                                End If
                            Else
                                tmpGrpTask = "Extend"
                                If DefLoad = True Then tmpIniName = tmpGrpName & "_EXTEND" Else tmpIniName = tmpGrpName & "_EXTD_" & CStr(GrpIdx)
                                ReadFilter = GetIniEntry(myIniFullName, AodbReaderSection, "", tmpIniName, "")
                                If (ReadFilter = "") And (GrpIdx = 1) Then
                                    If DefLoad = False Then tmpIniName = tmpGrpName & "_EXTEND" Else tmpIniName = tmpGrpName & "_EXTD_" & CStr(GrpIdx)
                                    ReadFilter = GetIniEntry(myIniFullName, AodbReaderSection, "", tmpIniName, "")
                                End If
                            End If
                            If ReadFilter <> "" Then
                                tmpGrpCode = tmpGrpName & "_TEXT_" & CStr(GrpIdx)
                                tmpGrpText = GetIniEntry(myIniFullName, AodbReaderSection, "", tmpGrpCode, tmpGrpText)
                            
                                TblNam = GetItem(ReadFilter, 1, "|")
                                FldLst = GetItem(ReadFilter, 2, "|")
                                SqlKey = GetItem(ReadFilter, 3, "|")
                                
                                If InStr(TblNam, ":") > 0 Then
                                    tmpGrpText = GetItem(TblNam, 1, ":")
                                    TblNam = Trim(GetItem(TblNam, 2, ":"))
                                End If
                                
                                'WorkProgress.CreateNewEntry tmpGrpTask, tmpGrpText, "Waiting For Server", False
                                'WorkProgress.UpdateProgress 0, 250, 0, 0
                            
                                SigIdx = 1
                                idx = CurCedaIdx + 1
                                
                                If ServerIsAvailable Then
                                    'ToggleServerButtons idx, 1
                                    DoEvents
                                    AddSqlKey = ""
                                    SqlItemNo = 0
                                    CurFldLst = FldLst
                                    CurSqlKey = SqlKey
                                    tmpVpfr = ""
                                    tmpVpto = ""
                                    tmpTifr = ""
                                    tmpTito = ""
                                    SqlVpfr = ""
                                    SqlVpto = ""
                                    SqlTifr = ""
                                    SqlTito = ""
                                    ItemNo = 0
                                    PatchName = GetRealItem(PatchCodes, ItemNo, Chr(15))
                                    While PatchName <> ""
                                        CheckPatch = "[" & PatchName & "]"
                                        PatchData = GetRealItem(PatchValues, ItemNo, Chr(15))
                                        Select Case PatchName
                                            Case "ZONE"
                                                tmpZone = PatchData
                                            Case "VPFR"
                                                tmpVpfr = PatchData
                                                SqlVpfr = PatchData
                                            Case "VPTO"
                                                tmpVpto = PatchData
                                                SqlVpto = PatchData
                                            Case "TIFR"
                                                tmpTifr = PatchData
                                                SqlTifr = PatchData
                                            Case "TITO"
                                                tmpTito = PatchData
                                                SqlTito = PatchData
                                            Case Else
                                                If AppendUnknown Then
                                                    If InStr(CurSqlKey, CheckPatch) = 0 Then
                                                        Select Case PatchName
                                                            Case "APC3"
                                                                AddSqlKey = AddSqlKey & " AND (ORG3 IN (" & PatchData & ") OR DES3 IN (" & PatchData & "))"
                                                            'Do not auto-append these
                                                            Case "HOPO"
                                                            Case "TIFR"
                                                            Case "TITO"
                                                            Case Else
                                                                AddSqlKey = AddSqlKey & " AND " & PatchName & " IN (" & PatchData & ")"
                                                        End Select
                                                    Else
                                                        Select Case PatchName
                                                            Case "TIFR"
                                                                If Len(PatchData) = 4 Then PatchData = PatchData & "00"
                                                            Case "TITO"
                                                                If Len(PatchData) = 4 Then PatchData = PatchData & "59"
                                                            Case Else
                                                        End Select
                                                    End If
                                                End If
                                                CurSqlKey = Replace(CurSqlKey, CheckPatch, PatchData, 1, -1, vbBinaryCompare)
                                        End Select
                                        ItemNo = ItemNo + 1
                                        PatchName = GetRealItem(PatchCodes, ItemNo, Chr(15))
                                    Wend
                                    CheckPatch = "[NO_FILTER]"
                                    If InStr(CurSqlKey, CheckPatch) > 0 Then
                                        CurSqlKey = Replace(CurSqlKey, CheckPatch, "", 1, -1, vbBinaryCompare)
                                        AddSqlKey = ""
                                    End If
                                    CheckPatch = "[FILTER]"
                                    If InStr(CurSqlKey, CheckPatch) > 0 Then
                                        CurSqlKey = Replace(CurSqlKey, CheckPatch, AddSqlKey, 1, -1, vbBinaryCompare)
                                        AddSqlKey = ""
                                    End If
                                    If CurSqlKey <> "" Then CurSqlKey = "WHERE " & CurSqlKey
                                    If AddSqlKey <> "" Then
                                        CurSqlKey = CurSqlKey & AddSqlKey
                                        AddSqlKey = ""
                                    End If
                                    SqlKeyPeriod = False
                                    If InStr(CurSqlKey, "[VPFR]") > 0 Then SqlKeyPeriod = True
                                    If InStr(CurSqlKey, "[VPTO]") > 0 Then SqlKeyPeriod = True
                                    If InStr(CurSqlKey, "[TIFR]") > 0 Then SqlKeyPeriod = True
                                    If InStr(CurSqlKey, "[TITO]") > 0 Then SqlKeyPeriod = True
                                    If (tmpVpfr = "") Or (SqlKeyPeriod = False) Then
                                        KeyIdx = -1
                                        CurKeyLine = -1
                                        UseSqlKey = CurSqlKey
                                        KeyType = 0
                                        KeySize = 9
                                        KeyPos = InStr(UseSqlKey, "[LIST_")
                                        If KeyPos = 0 Then
                                            KeyType = 1
                                            KeySize = 11
                                            KeyPos = InStr(UseSqlKey, "['LIST_")
                                        End If
                                        If KeyPos > 0 Then
                                            KeyName = Mid(UseSqlKey, KeyPos, KeySize)
                                            KeyIndx = Mid(KeyName, KeyType + 7, 2)
                                            KeyIdx = Val(KeyIndx)
                                            KeyIdx = (PreFetchFilters * (CurCedaIdx + 1)) - PreFetchFilters + KeyIdx
                                            KeyList = "START"
                                            While KeyList <> ""
                                                KeyList = GetSqlKeyList(KeyIdx, CurKeyLine, KeyType)
                                                If KeyList <> "" Then
                                                    UseSqlKey = Replace(CurSqlKey, KeyName, KeyList, 1, -1, vbBinaryCompare)
                                                    'MainDialog.AdjustServerSignals True, SigIdx
                                                    CurTab.CedaAction "RTA", TblNam, CurFldLst, "", UseSqlKey
                                                    'MainDialog.AdjustServerSignals False, SigIdx
                                                End If
                                            Wend
                                        Else
                                            'MainDialog.AdjustServerSignals True, SigIdx
                                            CurTab.CedaAction "RTA", TblNam, CurFldLst, "", CurSqlKey
                                            'MainDialog.AdjustServerSignals False, SigIdx
                                        End If
                                    Else
                                        BgnTime = SqlVpfr & SqlTifr
                                        EndTime = SqlVpto & SqlTito
                                        FirstDay = CedaFullDateToVb(BgnTime)
                                        LastDay = CedaFullDateToVb(EndTime)
                                        'ReadDays = 7
                                        ReadDays = ReadSteps
                                        If ReadDays < 0 Then ReadDays = 999
                                        If ReadDays < 5 Then ReadDays = 5
                                        ReadFrom = FirstDay
                                        Do
                                            ReadTo = DateAdd("d", (ReadDays - 1), ReadFrom)
                                            ReadTo = DateAdd("n", -1, ReadTo)
                                            If ReadTo > LastDay Then ReadTo = LastDay
                                            tmpVpfr = Format(ReadFrom, "yyyymmdd")
                                            tmpVpto = Format(ReadTo, "yyyymmdd")
                                            UseSqlKey = Replace(CurSqlKey, "[VPFR]", tmpVpfr, 1, -1, vbBinaryCompare)
                                            UseSqlKey = Replace(UseSqlKey, "[VPTO]", tmpVpto, 1, -1, vbBinaryCompare)
                                            tmpTifr = Format(ReadFrom, "hhmm") & "00"
                                            tmpTito = Format(ReadTo, "hhmm") & "59"
                                            UseSqlKey = Replace(UseSqlKey, "[TIFR]", tmpTifr, 1, -1, vbBinaryCompare)
                                            UseSqlKey = Replace(UseSqlKey, "[TITO]", tmpTito, 1, -1, vbBinaryCompare)
                                            'MainDialog.AdjustServerSignals True, SigIdx
                                            If ShowProgInfo Then
                                                tmpInf1 = Format(ReadFrom, "yyyy.mm.dd / hh:mm")
                                                tmpInf2 = Format(ReadTo, "yyyy.mm.dd / hh:mm")
                                                'WorkProgress.SetContextInfo tmpInf1, tmpInf2
                                            End If
                                            DoEvents
                                            CurTab.CedaAction "RTA", TblNam, CurFldLst, "", UseSqlKey
                                            'MainDialog.AdjustServerSignals False, SigIdx
                                            DoEvents
                                            SigIdx = SigIdx + 1
                                            If SigIdx > 2 Then SigIdx = 1
                                            ReadFrom = DateAdd("n", 1, ReadTo)
                                            'If StopAllLoops Then ReadFrom = DateAdd("d", 1, LastDay)
                                            DoEvents
                                        Loop While (ReadFrom <= LastDay) ' And (Not CedaError)
                                    End If
                                    'WorkProgress.SetTaskText "Keys Fetched"
                                    'WorkProgress.UpdateProgress 250, 250, 0, CurTab.GetLineCount
                                    'If StopAllLoops Then CurFldLst = ""
                                    'ToggleServerButtons idx, 0
                                    DoEvents
                                End If
                            End If
                        Next
                    End If
                    CurTab.Sort "0", True, True
                Next
            End If
        Next
    End If
    Screen.MousePointer = 0
End Sub

Private Function GetCfgTabLineNo(TabCode As String) As Long
    Dim CurLine As Long
    Dim MaxLine As Long
    Dim LineNo As Long
    Dim tmpCode As String
    LineNo = -1
    'MaxLine = TabCfgTab.GetLineCount - 1
    'CurLine = 0
    'While (CurLine <= MaxLine) And (LineNo < 0)
    '    tmpCode = TabCfgTab.GetColumnValue(CurLine, 0)
    '    If tmpCode = TabCode Then LineNo = CurLine
    '    CurLine = CurLine + 1
    'Wend
    
    If TabCode <> "" Then
        'Just to be safe
        TabCfgTab.IndexCreate "KEYCODE", 0
        TabCfgTab.SetInternalLineBuffer True
        LineNo = Val(TabCfgTab.GetLinesByIndexValue("KEYCODE", TabCode, 0))
        If LineNo = 1 Then
            LineNo = TabCfgTab.GetNextResultLine
        Else
            LineNo = -1
        End If
        TabCfgTab.SetInternalLineBuffer False
    End If
    
    GetCfgTabLineNo = LineNo
End Function
Private Sub InitMyForm()
    Dim TabCfgLine As String
    TabCfgTab.ResetContent
    TabCfgTab.LogicalFieldList = "CODE,FIDX,STAT,TYPE,DSRC,DTAB,DFLD,DSQL,LFLD,CAPT,CODE,CALL,MASK,EDIT,MAPP,NAME,SORT,SYNC,REMA"
    'TabCfgTab.HeaderString = "Unique Key,FX,S,Typ,Data Source,Table,Table Fields,SQL Selection,Logical Fields,Caption,Action,Called,Edit Mask,Edit Fields,Mapped Fields,Name,Synch.Cursor,Remark"
    TabCfgTab.HeaderString = "CODE,FIDX,STAT,TYPE,DSRC,DTAB,DFLD,DSQL,LFLD,CAPT,CODE,CALL,MASK,EDIT,MAPP,NAME,SORT,SYNC,REMA"
    TabCfgTab.HeaderLengthString = "10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10"
    TabCfgTab.ColumnWidthString = "20,2,1,3,20,6,250,250,250,20,10,10,40,20,20,10,10,10,10"
    TabCfgTab.SetMainHeaderValues "18", "Data TAB Configuration", ""
    TabCfgTab.MainHeader = True
    TabCfgTab.SetFieldSeparator Chr(15)
    TabCfgTab.ShowHorzScroller True
    TabCfgTab.EnableHeaderSizing True
    TabCfgTab.AutoSizeByHeader = True
    TabCfgTab.AutoSizeColumns
    TabCfgTab.myName = "HIDDEN_TABCFG"
    TabCfgLine = TabCfgTab.myName & Chr(15)
    TabCfgLine = TabCfgLine & "0" & Chr(15)
    TabCfgLine = TabCfgLine & "V" & Chr(15)
    TabCfgLine = TabCfgLine & "CFG" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & TabCfgTab.LogicalFieldList & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    TabCfgLine = TabCfgLine & "" & Chr(15)
    CheckTabConfig TabCfgLine
    
    CreateFilterTab 0
    StatusBar1.ZOrder
End Sub

Private Sub chkClose_Click()
    If chkClose.Value = 1 Then
        Me.Hide
        chkClose.Value = 0
    End If
End Sub

Private Sub Form_Load()
    InitMyForm
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If UnloadMode = 0 Then
        Me.Hide
        Cancel = True
    End If
End Sub

Private Sub Form_Resize()
    If TabMaximized Then
        CurMaxTab.Width = Me.ScaleWidth
        If Me.ScaleHeight > 1500 Then
            CurMaxTab.Height = Me.ScaleHeight - StatusBar1.Height - CurMaxTab.Top
        End If
    End If
End Sub

Private Sub TabCfgTab_SendLButtonDblClick(ByVal LineNo As Long, ByVal ColNo As Long)
    Dim tmpTag As String
    If LineNo >= 0 Then
    ElseIf LineNo = -1 Then
        TabCfgTab.Sort CStr(ColNo), True, True
        TabCfgTab.Refresh
    Else
        If TabMaximized Then
            tmpTag = TabCfgTab.Tag
            TabCfgTab.Left = Val(GetRealItem(tmpTag, 0, ","))
            TabCfgTab.Top = Val(GetRealItem(tmpTag, 1, ","))
            TabCfgTab.Width = Val(GetRealItem(tmpTag, 2, ","))
            TabCfgTab.Height = Val(GetRealItem(tmpTag, 3, ","))
            TabMaximized = False
        Else
            tmpTag = ""
            tmpTag = tmpTag & CStr(TabCfgTab.Left) & ","
            tmpTag = tmpTag & CStr(TabCfgTab.Top) & ","
            tmpTag = tmpTag & CStr(TabCfgTab.Width) & ","
            tmpTag = tmpTag & CStr(TabCfgTab.Height)
            TabCfgTab.Tag = tmpTag
            TabCfgTab.Top = chkClose.Top + chkClose.Height + 60
            TabCfgTab.Left = 0
            TabCfgTab.ZOrder
            TabCfgTab.AutoSizeColumns
            TabCfgTab.Refresh
            Set CurMaxTab = TabCfgTab
            TabMaximized = True
            Form_Resize
        End If
    End If
End Sub

Public Sub InitBasicData(Index As Integer, TableName As String, TableFields As String, SqlKey As String, FName As String)
    Dim HiddenTab As String
    Dim tmpFldLst As String
    Dim tmpIdxName As String
    Dim i As Long
    HiddenTab = CreateBasicDataTab(Index, TableName, TableFields, SqlKey)
    If FName = "" Then
        CedaIsConnected = True
        If CedaIsConnected Then
            ReadAodbData BasicDataTab(Index), True, "", "", "", False, False, -1
        End If
    Else
        BasicDataTab(Index).ReadFromFile FName
    End If
    tmpFldLst = BasicDataTab(Index).LogicalFieldList
    For i = 0 To 1
        tmpIdxName = GetRealItem(tmpFldLst, i, ",")
        BasicDataTab(Index).IndexCreate tmpIdxName, i
    Next
End Sub
Public Function BasicDataLookUp(TableName As String, LookField As String, LookValue As String, GetField As String, FirstHit As Boolean) As String
    Dim Result As String
    Dim TabName As String
    Dim LineNo As Long
    Dim i As Integer
    Result = ""
    If LookValue <> "" Then
        TabName = "BASIC_" & TableName
        For i = 0 To BasicDataTab.UBound
            If BasicDataTab(i).myName = TabName Then
                BasicDataTab(i).SetInternalLineBuffer True
                LineNo = Val(BasicDataTab(i).GetLinesByIndexValue(LookField, LookValue, 0))
                While LineNo >= 0
                    LineNo = BasicDataTab(i).GetNextResultLine
                    If LineNo >= 0 Then Result = Result & BasicDataTab(i).GetFieldValues(LineNo, GetField) & vbLf
                Wend
                BasicDataTab(i).SetInternalLineBuffer False
                Exit For
            End If
        Next
        If Len(Result) > 0 Then Result = Left(Result, Len(Result) - 1)
        If FirstHit Then Result = GetRealItem(Result, 0, vbLf)
    End If
    BasicDataLookUp = Result
End Function

Public Sub CreateConfigEntry(Fields As String, Values As String)
    Dim tmpLine As String
    Dim LineNo As Long
    Dim ColNo As Long
    Dim i As Integer
    Dim itmField As String
    Dim itmValue As String
    tmpLine = CreateEmptyLine(TabCfgTab.LogicalFieldList)
    tmpLine = Replace(tmpLine, ",", Chr(15), 1, -1, vbBinaryCompare)
    TabCfgTab.InsertTextLine tmpLine, False
    LineNo = TabCfgTab.GetLineCount - 1
    i = 0
    itmField = "START"
    While itmField <> ""
        i = i + 1
        itmField = GetItem(Fields, i, Chr(15))
        If itmField <> "" Then
            itmValue = GetItem(Values, i, Chr(15))
            ColNo = CLng(GetItemNo(TabCfgTab.LogicalFieldList, itmField) - 1)
            TabCfgTab.SetColumnValue LineNo, ColNo, itmValue
        End If
    Wend
    TabCfgTab.RedrawTab
End Sub

Private Sub UrnoList_PackageReceived(Index As Integer, ByVal lpPackage As Long)
    Static TipToggle As Boolean
    Dim tmpTask As String
    Dim tmpCdrErr As String
    Dim CurCount As Long
    Dim CurTotal As Long
    Dim CurValue As Long
    'lpPackage:
    '500 = PacketSize
    'Less than 500 = LastPack
    '0 = No matching data
    '0 could be an ORA-ERR as well
    tmpTask = "Got Data Package"
    TipToggle = Not TipToggle
    'MainDialog.AdjustServerSignals TipToggle, 1
    'CurCount = WorkProgress.GetProgressValue("VAL1")
    'CurTotal = WorkProgress.GetProgressValue("VAL2")
    CurCount = CurCount + 1
    CurTotal = CurTotal + lpPackage
    CurValue = CurCount
    If CurValue > 250 Then CurValue = 10
    'WorkProgress.SetTaskText tmpTask
    'WorkProgress.UpdateProgress CurValue, 250, CurCount, CurTotal
    'SetProgressValue Index, tmpTask, CurValue, 250, CurCount, CurTotal, False
    If lpPackage <= 0 Then
        'Have a breakpoint
        'tmpCdrErr = FileData(Index).GetLastCedaError
        'If tmpCdrErr <> "" Then
        'CurValue = CurValue
        'End If
    End If
End Sub

