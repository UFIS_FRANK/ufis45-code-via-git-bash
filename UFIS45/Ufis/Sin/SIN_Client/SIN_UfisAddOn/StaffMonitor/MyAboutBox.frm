VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "tab.ocx"
Object = "{6FBA474E-43AC-11CE-9A0E-00AA0062BB4C}#1.0#0"; "SYSINFO.OCX"
Begin VB.Form MyAboutBox 
   AutoRedraw      =   -1  'True
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "About MyApp"
   ClientHeight    =   4905
   ClientLeft      =   2340
   ClientTop       =   1935
   ClientWidth     =   8145
   ClipControls    =   0   'False
   Icon            =   "MyAboutBox.frx":0000
   LinkTopic       =   "Form2"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4905
   ScaleWidth      =   8145
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CheckBox chkOsFormat 
      Height          =   210
      Left            =   180
      TabIndex        =   11
      Top             =   3480
      Value           =   1  'Checked
      Width           =   210
   End
   Begin TABLib.TAB VersionTab 
      Height          =   2055
      Left            =   180
      TabIndex        =   10
      Top             =   930
      Width           =   5265
      _Version        =   65536
      _ExtentX        =   9287
      _ExtentY        =   3625
      _StockProps     =   64
   End
   Begin SysInfoLib.SysInfo SysInfo 
      Left            =   4230
      Top             =   3690
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
   End
   Begin VB.CommandButton cmdHelp 
      Caption         =   "&Help"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   6780
      TabIndex        =   5
      Top             =   3480
      Width           =   1245
   End
   Begin VB.PictureBox picIcon 
      AutoSize        =   -1  'True
      ClipControls    =   0   'False
      Height          =   540
      Left            =   180
      Picture         =   "MyAboutBox.frx":0442
      ScaleHeight     =   337.12
      ScaleMode       =   0  'User
      ScaleWidth      =   337.12
      TabIndex        =   1
      Top             =   180
      Width           =   540
   End
   Begin VB.CommandButton cmdOK 
      Cancel          =   -1  'True
      Caption         =   "OK"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Left            =   6765
      TabIndex        =   0
      Top             =   4215
      Width           =   1260
   End
   Begin VB.CommandButton cmdSysInfo 
      Caption         =   "&System Info..."
      Height          =   345
      Left            =   6780
      TabIndex        =   2
      Top             =   3855
      Width           =   1245
   End
   Begin VB.Image GoldenBorder 
      BorderStyle     =   1  'Fixed Single
      Height          =   240
      Left            =   -15
      Picture         =   "MyAboutBox.frx":074C
      Top             =   4665
      Width           =   9435
   End
   Begin VB.Label lblPcInfo 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "PC Info"
      ForeColor       =   &H8000000E&
      Height          =   195
      Index           =   3
      Left            =   210
      TabIndex        =   9
      Top             =   4350
      Width           =   525
   End
   Begin VB.Label lblPcInfo 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "PC Info"
      ForeColor       =   &H8000000E&
      Height          =   195
      Index           =   2
      Left            =   210
      TabIndex        =   8
      Top             =   4110
      Width           =   525
   End
   Begin VB.Label lblPcInfo 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "PC Info"
      ForeColor       =   &H8000000E&
      Height          =   195
      Index           =   1
      Left            =   210
      TabIndex        =   7
      Top             =   3870
      Width           =   525
   End
   Begin VB.Label lblPcInfo 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "PC Info"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H8000000E&
      Height          =   195
      Index           =   0
      Left            =   510
      TabIndex        =   6
      Top             =   3480
      Width           =   645
   End
   Begin VB.Label lblApplVers 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Application Version"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   195
      Left            =   870
      TabIndex        =   4
      Top             =   150
      Width           =   1650
   End
   Begin VB.Label lblDescription 
      AutoSize        =   -1  'True
      BackColor       =   &H00008080&
      BackStyle       =   0  'Transparent
      Caption         =   "App Description"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   161
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   195
      Left            =   4740
      TabIndex        =   3
      Top             =   150
      Width           =   1365
   End
End
Attribute VB_Name = "MyAboutBox"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Public SetLifeStyle As Boolean
Public SetGoldenBorder As Boolean
Private MyParent As Variant
Private MyCallButton As Control
Private myControls() As Control
Private MyAtlServerVersions As String

' Reg Key Security Options...
Const READ_CONTROL = &H20000
Const KEY_QUERY_VALUE = &H1
Const KEY_SET_VALUE = &H2
Const KEY_CREATE_SUB_KEY = &H4
Const KEY_ENUMERATE_SUB_KEYS = &H8
Const KEY_NOTIFY = &H10
Const KEY_CREATE_LINK = &H20
Const KEY_ALL_ACCESS = KEY_QUERY_VALUE + KEY_SET_VALUE + _
                       KEY_CREATE_SUB_KEY + KEY_ENUMERATE_SUB_KEYS + _
                       KEY_NOTIFY + KEY_CREATE_LINK + READ_CONTROL
                     
' Reg Key ROOT Types...
Const HKEY_LOCAL_MACHINE = &H80000002
Const ERROR_SUCCESS = 0
Const REG_SZ = 1                         ' Unicode nul terminated string
Const REG_DWORD = 4                      ' 32-bit number

Const gREGKEYSYSINFOLOC = "SOFTWARE\Microsoft\Shared Tools Location"
Const gREGVALSYSINFOLOC = "MSINFO"
Const gREGKEYSYSINFO = "SOFTWARE\Microsoft\Shared Tools\MSINFO"
Const gREGVALSYSINFO = "PATH"

Private Declare Function RegOpenKeyEx Lib "advapi32" Alias "RegOpenKeyExA" (ByVal hKey As Long, ByVal lpSubKey As String, ByVal ulOptions As Long, ByVal samDesired As Long, ByRef phkResult As Long) As Long
Private Declare Function RegQueryValueEx Lib "advapi32" Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, ByRef lpType As Long, ByVal lpData As String, ByRef lpcbData As Long) As Long
Private Declare Function RegCloseKey Lib "advapi32" (ByVal hKey As Long) As Long


Public Sub SetComponents(appControls() As Control, appAtlServerVersions As String)
    Dim i As Integer
    Dim lb As Integer
    MyAtlServerVersions = appAtlServerVersions
    On Error GoTo Proceed
    lb = UBound(appControls)
    If lb > 0 Then
        ReDim myControls(UBound(appControls))
        For i = 0 To UBound(appControls)
            Set myControls(i) = appControls(i)
        Next
    End If
Proceed:
End Sub

Public Sub RegisterMyParent(Caller As Form, CallButton As Control, TaskValue As String)
    Set MyParent = Caller
    Set MyCallButton = CallButton
End Sub

Private Sub Form_Load()
    Dim tmpTxt As String
    Dim tmpTxt2 As String
    Dim tmpBuild As String
    Dim tmpRema As String
    Dim tmpBuff As String
    Dim tmpdat As String
    Dim i As Integer
    Dim strCtlName As String
    Dim arr() As String

    Me.Caption = "About " & App.Title
    If SetGoldenBorder Then
        GoldenBorder.Visible = True
    Else
        GoldenBorder.Visible = False
        Me.Height = Me.Height - GoldenBorder.Height
    End If
    VersionTab.ResetContent
    VersionTab.FontName = "Arial"
    VersionTab.FontSize = 16
    VersionTab.LineHeight = 19
    VersionTab.Left = picIcon.Left
    VersionTab.Width = cmdHelp.Left + cmdHelp.Width - VersionTab.Left
    VersionTab.Height = cmdHelp.Top - VersionTab.Top - 255
    
    VersionTab.HeaderString = "Component,Version,Built,Remark" & Space(66)
    VersionTab.HeaderLengthString = "100,100,100,100"
    VersionTab.HeaderAlignmentString = "C,C,C,L"
    VersionTab.ColumnAlignmentString = "L,C,C,L"
    VersionTab.EmptyAreaRightColor = vbWhite
    VersionTab.SetMainHeaderValues "4", "Integrated Ufis Components", ""
    VersionTab.SetMainHeaderFont 16, False, False, True, 0, "Arial"
    VersionTab.MainHeader = True
    VersionTab.AutoSizeByHeader = True
    VersionTab.AutoSizeColumns
    VersionTab.Refresh
    
    lblApplVers.Caption = GetApplVersionString
    
    tmpBuff = ""
    tmpBuild = "16JUN03/15:35"
    tmpRema = "Read from Property"
    On Error GoTo Proceed
    For i = 0 To UBound(myControls) - 1
        strCtlName = TypeName(myControls(i))
        If strCtlName = "AATLoginControl" Then
            tmpTxt = "AATLoginControl.ocx"
            arr() = Split(myControls(i).InfoAppVersion, "/")
            tmpdat = Trim(arr(1))
            tmpTxt2 = tmpdat
        Else
            If strCtlName = "Amp_test" Then
                tmpTxt = "TrafficLight.ocx"
                tmpTxt2 = "4.5.0.1"
            Else
                tmpTxt = strCtlName & ".ocx"
                tmpTxt2 = myControls(i).Version
            End If
        End If
        tmpTxt = Replace(tmpTxt, ",", ".", 1, -1, vbBinaryCompare)
        tmpTxt2 = Replace(tmpTxt2, " ", "", 1, -1, vbBinaryCompare)
        tmpTxt2 = Replace(tmpTxt2, ",", ".", 1, -1, vbBinaryCompare)
        tmpBuff = tmpBuff & tmpTxt & "," & tmpTxt2 & "," & tmpBuild & "," & tmpRema & vbLf
    Next i
Proceed:

    If tmpBuff = "" Then tmpBuff = AutoDetectComponents
    
    VersionTab.InsertBuffer tmpBuff, vbLf
    VersionTab.InsertBuffer MyAtlServerVersions, vbLf
    VersionTab.AutoSizeColumns
    VersionTab.Refresh
    
    tmpTxt = ""
    tmpTxt = tmpTxt & "Module: " & App.ProductName & vbNewLine
    tmpTxt = tmpTxt & App.FileDescription & vbNewLine
    tmpTxt = tmpTxt & App.CompanyName
    lblDescription.Caption = tmpTxt
        
    Select Case SysInfo.OSPlatform
        Case 0
            tmpTxt = "Win32s"
        Case 1
            tmpTxt = "Windows 95/98"
        Case 2
            tmpTxt = "Windows NT"
        Case Else
            tmpTxt = "Win ? (" & CStr(i) & ")"
            'tmpTxt = ""
    End Select
    If tmpTxt <> "" Then tmpTxt = "OS: " & tmpTxt & " " & CStr(SysInfo.OSVersion) & " (Build " & CStr(SysInfo.OSBuild) & ")"
    lblPcInfo(0).Caption = tmpTxt
    
    CheckPowerOnOff
    CheckBatteryLifeTime
    CheckBatteryStatus
    If SetLifeStyle Then
        DrawLifeStyle 7
        For i = 0 To lblPcInfo.UBound
            lblPcInfo(i).ForeColor = vbWhite
        Next
    Else
        For i = 0 To lblPcInfo.UBound
            lblPcInfo(i).ForeColor = vbBlack
        Next
    End If
End Sub

Private Function GetApplVersionString() As String
    Dim tmpTxt As String
    Dim tmpMajor As String
    Dim tmpMinor As String
    Dim tmpRevis As String
    tmpMajor = App.Major
    tmpMinor = App.Minor
    tmpRevis = App.Revision
    tmpTxt = App.Title & vbNewLine
    Select Case SysInfo.OSPlatform
        Case 0  'Win32s
        Case 1  'Win 95/98
        Case 2  'Win NT
            If SysInfo.OSVersion > 4 Then
                'Win 2000
                If chkOsFormat.Value = 1 Then tmpRevis = "0." & tmpRevis
            Else
                'Win NT 4
                If chkOsFormat.Value = 1 Then
                    tmpMinor = Right("0000" & tmpMinor, 2)
                    tmpRevis = Right("0000" & tmpRevis, 4)
                End If
            End If
        Case Else
    End Select
    tmpTxt = tmpTxt & "Version " & tmpMajor & "." & tmpMinor & "." & tmpRevis
    tmpTxt = tmpTxt & vbNewLine & App.Comments
    GetApplVersionString = tmpTxt
End Function
Private Sub CheckPowerOnOff()
    'lblPcInfo(1) = "AC Power: Unknown"
    lblPcInfo(1).Caption = ""
    If SysInfo.ACStatus <> 255 Then
        Select Case SysInfo.ACStatus
           Case 0
               lblPcInfo(1) = "AC Power: Off"
           Case 1
              lblPcInfo(1) = "AC Power: On"
           Case 255
              lblPcInfo(1) = "AC Power: Unknown"
        End Select
    End If
End Sub
Private Sub CheckBatteryLifeTime()
    'lblPcInfo(2).Caption = "Battery Time: " & "1:25" & " left from " & "4:00" & " total. (" & CStr(SysInfo.BatteryLifePercent) & "%)"
    lblPcInfo(2).Caption = ""
    If SysInfo.BatteryLifeTime <> &HFFFFFFFF Then
        Dim TimeLeft As String
        Dim TimeTotal As String
        Dim temp
        temp = TimeSerial(0, 0, SysInfo.BatteryLifeTime)
        TimeLeft = Format(temp, "h:mm")
        temp = TimeSerial(0, 0, SysInfo.BatteryFullTime)
        TimeTotal = Format(temp, "h:mm")
        lblPcInfo(2).Caption = "Battery Time: " & TimeLeft & " left from " & TimeTotal & " total. (" & CStr(SysInfo.BatteryLifePercent) & "%)"
    End If
End Sub
Private Sub CheckBatteryStatus()
    'lblPcInfo(3).Caption = "No Battery Status"
    lblPcInfo(3).Caption = ""
    If SysInfo.BatteryStatus < 10 Then
        Select Case SysInfo.BatteryStatus
           Case 1
              lblPcInfo(3).Caption = "Battery OK"
           Case 2
              lblPcInfo(3).Caption = "Battery Low"
           Case 4
              lblPcInfo(3).Caption = "Battery Critical"
           Case 8
              lblPcInfo(3).Caption = "Battery Charging"
           Case 128, 255
              lblPcInfo(3).Caption = "No Battery Status"
        End Select
    End If
End Sub
Public Sub StartSysInfo()
    On Error GoTo SysInfoErr
  
    Dim rc As Long
    Dim SysInfoPath As String
    
    ' Try To Get System Info Program Path\Name From Registry...
    If GetKeyValue(HKEY_LOCAL_MACHINE, gREGKEYSYSINFO, gREGVALSYSINFO, SysInfoPath) Then
    ' Try To Get System Info Program Path Only From Registry...
    ElseIf GetKeyValue(HKEY_LOCAL_MACHINE, gREGKEYSYSINFOLOC, gREGVALSYSINFOLOC, SysInfoPath) Then
        ' Validate Existance Of Known 32 Bit File Version
        If (Dir(SysInfoPath & "\MSINFO32.EXE") <> "") Then
            SysInfoPath = SysInfoPath & "\MSINFO32.EXE"
            
        ' Error - File Can Not Be Found...
        Else
            GoTo SysInfoErr
        End If
    ' Error - Registry Entry Can Not Be Found...
    Else
        GoTo SysInfoErr
    End If
    
    Call Shell(SysInfoPath, vbNormalFocus)
    
    Exit Sub
SysInfoErr:
    MsgBox "System Information (MSINFO32.EXE) Is Unavailable At This Time", vbOKOnly
End Sub

Public Function GetKeyValue(KeyRoot As Long, KeyName As String, SubKeyRef As String, ByRef KeyVal As String) As Boolean
    Dim i As Long                                           ' Loop Counter
    Dim rc As Long                                          ' Return Code
    Dim hKey As Long                                        ' Handle To An Open Registry Key
    Dim hDepth As Long                                      '
    Dim KeyValType As Long                                  ' Data Type Of A Registry Key
    Dim tmpVal As String                                    ' Tempory Storage For A Registry Key Value
    Dim KeyValSize As Long                                  ' Size Of Registry Key Variable
    '------------------------------------------------------------
    ' Open RegKey Under KeyRoot {HKEY_LOCAL_MACHINE...}
    '------------------------------------------------------------
    rc = RegOpenKeyEx(KeyRoot, KeyName, 0, KEY_ALL_ACCESS, hKey) ' Open Registry Key
    
    If (rc <> ERROR_SUCCESS) Then GoTo GetKeyError          ' Handle Error...
    
    tmpVal = String$(1024, 0)                             ' Allocate Variable Space
    KeyValSize = 1024                                       ' Mark Variable Size
    
    '------------------------------------------------------------
    ' Retrieve Registry Key Value...
    '------------------------------------------------------------
    rc = RegQueryValueEx(hKey, SubKeyRef, 0, _
                         KeyValType, tmpVal, KeyValSize)    ' Get/Create Key Value
                        
    If (rc <> ERROR_SUCCESS) Then GoTo GetKeyError          ' Handle Errors
    
    If (Asc(Mid(tmpVal, KeyValSize, 1)) = 0) Then           ' Win95 Adds Null Terminated String...
        tmpVal = Left(tmpVal, KeyValSize - 1)               ' Null Found, Extract From String
    Else                                                    ' WinNT Does NOT Null Terminate String...
        tmpVal = Left(tmpVal, KeyValSize)                   ' Null Not Found, Extract String Only
    End If
    '------------------------------------------------------------
    ' Determine Key Value Type For Conversion...
    '------------------------------------------------------------
    Select Case KeyValType                                  ' Search Data Types...
    Case REG_SZ                                             ' String Registry Key Data Type
        KeyVal = tmpVal                                     ' Copy String Value
    Case REG_DWORD                                          ' Double Word Registry Key Data Type
        For i = Len(tmpVal) To 1 Step -1                    ' Convert Each Bit
            KeyVal = KeyVal + Hex(Asc(Mid(tmpVal, i, 1)))   ' Build Value Char. By Char.
        Next
        KeyVal = Format$("&h" + KeyVal)                     ' Convert Double Word To String
    End Select
    
    GetKeyValue = True                                      ' Return Success
    rc = RegCloseKey(hKey)                                  ' Close Registry Key
    Exit Function                                           ' Exit
    
GetKeyError:      ' Cleanup After An Error Has Occured...
    KeyVal = ""                                             ' Set Return Val To Empty String
    GetKeyValue = False                                     ' Return Failure
    rc = RegCloseKey(hKey)                                  ' Close Registry Key
End Function

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    'Me.Hide
    'RefreshMain
End Sub

Private Sub picIcon_Click()
    If GoldenBorder.Visible Then
        Me.Height = Me.Height - GoldenBorder.Height
        GoldenBorder.Visible = False
    Else
        Me.Height = Me.Height + GoldenBorder.Height
        GoldenBorder.Visible = True
    End If
End Sub

Private Sub SysInfo_PowerStatusChanged()
    CheckPowerOnOff
    CheckBatteryStatus
    CheckBatteryLifeTime
End Sub

Private Sub DrawLifeStyle(MyColor As Integer)
    Const intBLUESTART% = 255
    Const intBLUEEND% = 0
    Const intBANDHEIGHT% = 15
    Const intSHADOWSTART% = 64
    Const intSHADOWCOLOR% = 0
    Const intTEXTSTART% = 0
    Const intTEXTCOLOR% = 15
    Const intRed% = 1
    Const intGreen% = 2
    Const intBlue% = 4
    Const intBackRed% = 8
    Const intBackGreen% = 16
    Const intBackBlue% = 32
    Dim sngBlueCur As Single
    Dim sngBlueStep As Single
    Dim intFormHeight As Single
    Dim intFormWidth As Single
    Dim intX As Single
    Dim intY As Single
    Dim iColor As Integer
    Dim iRed As Single, iBlue As Single, iGreen As Single
    Dim ReturnColor As Long
    If MyColor >= 0 Then
        intFormHeight = Me.ScaleHeight
        intFormWidth = Me.ScaleWidth
        iColor = MyColor
        sngBlueCur = intBLUESTART
        sngBlueStep = (intBANDHEIGHT * (intBLUEEND - intBLUESTART) / intFormHeight)
        For intY = 0 To intFormHeight Step intBANDHEIGHT
            If iColor And intBlue Then iBlue = sngBlueCur
            If iColor And intRed Then iRed = sngBlueCur
            If iColor And intGreen Then iGreen = sngBlueCur
            If iColor And intBackBlue Then iBlue = 255 - sngBlueCur
            If iColor And intBackRed Then iRed = 255 - sngBlueCur
            If iColor And intBackGreen Then iGreen = 255 - sngBlueCur
            Me.Line (-1, intY - 1)-(intFormWidth, intY + intBANDHEIGHT), RGB(iRed, iGreen, iBlue), BF
            sngBlueCur = sngBlueCur + sngBlueStep
        Next intY
    End If
End Sub

Public Function AutoDetectComponents() As String
    Dim i As Integer
    Dim Result As String
    Dim tmpList As String
    Dim tmpLine As String
    Dim tmpData As String
    Dim strCtlName As String
    Dim curCtrl As Control
    On Error Resume Next
    Result = ""
    tmpList = ","
    For i = 0 To Forms.Count - 1
        For Each curCtrl In Forms(i)
            strCtlName = TypeName(curCtrl)
            If InStr(tmpList, strCtlName) = 0 Then
                tmpList = tmpList & strCtlName & ","
                tmpLine = ""
                tmpData = ""
                tmpData = curCtrl.Version
                If tmpData <> "" Then
                    tmpData = Replace(tmpData, ",", ".", 1, -1, vbBinaryCompare)
                    tmpData = Replace(tmpData, " ", "", 1, -1, vbBinaryCompare)
                    tmpLine = strCtlName & ".ocx," & tmpData
                    tmpData = "N.A."
                    tmpData = curCtrl.BuildDate
                    tmpLine = tmpLine & "," & tmpData
                    tmpData = "N.A."
                    tmpData = curCtrl.FileDescription
                    tmpLine = tmpLine & "," & tmpData
                    Result = Result & tmpLine & vbLf
                End If
            End If
        Next
    Next i
    AutoDetectComponents = Result
End Function


Private Sub chkOsFormat_Click()
    lblApplVers.Caption = GetApplVersionString
End Sub

Private Sub cmdHelp_Click()
    DisplayHtmHelpFile "", Me.Name, Me.ActiveControl.Name
End Sub

Private Sub cmdSysInfo_Click()
  Call StartSysInfo
End Sub

Private Sub cmdOK_Click()
    Unload Me
End Sub

