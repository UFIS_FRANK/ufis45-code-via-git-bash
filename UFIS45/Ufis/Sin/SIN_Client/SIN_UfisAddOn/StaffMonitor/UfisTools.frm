VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "tab.ocx"
Object = "{6FBA474E-43AC-11CE-9A0E-00AA0062BB4C}#1.0#0"; "SYSINFO.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Begin VB.Form UfisTools 
   Caption         =   "Ufis Tool Objects"
   ClientHeight    =   3420
   ClientLeft      =   -15
   ClientTop       =   -15
   ClientWidth     =   5775
   Icon            =   "UfisTools.frx":0000
   LinkTopic       =   "Form4"
   LockControls    =   -1  'True
   ScaleHeight     =   3420
   ScaleWidth      =   5775
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin TABLib.TAB BlackBox 
      Height          =   1845
      Index           =   0
      Left            =   240
      TabIndex        =   1
      Top             =   870
      Width           =   2985
      _Version        =   65536
      _ExtentX        =   5265
      _ExtentY        =   3254
      _StockProps     =   64
   End
   Begin SysInfoLib.SysInfo SysInfo 
      Left            =   2610
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
   End
   Begin VB.FileListBox FileList 
      Height          =   480
      Left            =   210
      TabIndex        =   0
      Top             =   90
      Width           =   1755
   End
   Begin MSComDlg.CommonDialog CommonDialog 
      Left            =   2040
      Top             =   120
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
End
Attribute VB_Name = "UfisTools"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub SysInfo_DisplayChanged()
    HandleDisplayChanged
End Sub

Private Sub SysInfo_SysColorsChanged()
    HandleSysColorsChanged
End Sub

Private Sub SysInfo_TimeChanged()
    HandleTimeChanged
End Sub

Public Function GetApplVersion(ShowExtended As Boolean) As String
    Dim tmpTxt As String
    Dim tmpMajor As String
    Dim tmpMinor As String
    Dim tmpRevis As String
    tmpMajor = App.Major
    tmpMinor = App.Minor
    tmpRevis = App.Revision
    tmpTxt = ""
    Select Case SysInfo.OSPlatform
        Case 0  'Win32s
        Case 1  'Win 95/98
        Case 2  'Win NT
            If SysInfo.OSVersion > 4 Then
                'Win 2000
                If ShowExtended Then tmpRevis = "0." & tmpRevis
            Else
                'Win NT 4
                If ShowExtended Then
                    tmpMinor = Right("0000" & tmpMinor, 2)
                    tmpRevis = Right("0000" & tmpRevis, 4)
                End If
            End If
        Case Else
    End Select
    tmpTxt = tmpTxt & tmpMajor & "." & tmpMinor & "." & tmpRevis
    GetApplVersion = tmpTxt
End Function
