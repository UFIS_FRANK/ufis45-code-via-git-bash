using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.MobileControls;
using Ufis.MobileClient.BL.Common;

namespace MobileCustomControls
{
	public class ServiceMask : MobileControl

	{

		private BusinessEntities.ServiceJCDS _myService;

		public BusinessEntities.ServiceJCDS MyService 
		{
			get 
			{
				return _myService;
			}
			set 
			{
				_myService = value;
			}
		}
		protected string RowsOfDataSet()
		{
			string rows = "0";

			if(MyService != null)
				rows = MyService.ServiceJC.Count.ToString();
					
			return rows;
		}

		protected string TimeOfDate(string dateString){
		
			char[] splitter  = {' '};
			string[] timeString = new string[2];

			timeString = dateString.Split(splitter);
			return timeString[1];
 		}

		protected string ServiceName(int i)
		{
		
			if(MyService.ServiceJC[i].Status.StartsWith("P"))
				return "PLANNED";
			else if(MyService.ServiceJC[i].Status.StartsWith("I"))
				return "INFORMED";
			else if(MyService.ServiceJC[i].Status.StartsWith("C"))
				return "CONFIRMED";
		    else if(MyService.ServiceJC[i].Status.StartsWith("F"))
				return "FINISHED";
			else return "ERROR";
		}

		protected override void OnLoad(EventArgs e)
		{
			Label serviceLabel = new Label();
			serviceLabel.Text = "Services";
			serviceLabel.StyleReference = "TitleLeft";
			Controls.Add(serviceLabel);

			if (!Page.IsPostBack)
			{
				base.OnLoad(e);

				for (int i=0; i < int.Parse(RowsOfDataSet()); i++)
				{
					
					Label servLabel = new Label();
					Label empLabel = new Label();
					Label timeLabel = new Label();
					
					switch (ServiceName(i))
					{
						case "PLANNED":
							servLabel.StyleReference = "ServicePlanned";
							empLabel.StyleReference = "ServicePlanned";
							timeLabel.StyleReference = "ServicePlanned";
							break;
						case "INFORMED":
							servLabel.StyleReference = "ServiceInformed";
							empLabel.StyleReference = "ServiceInformed";
							timeLabel.StyleReference = "ServiceInformed";
							break;
						case "CONFIRMED":
							servLabel.StyleReference = "ServiceConfirmed";
							empLabel.StyleReference = "ServiceConfirmed";
							timeLabel.StyleReference = "ServiceConfirmed";
							break;
						case "FINISHED":
							servLabel.StyleReference = "ServiceFinished";
							empLabel.StyleReference = "ServiceFinished";
							timeLabel.StyleReference = "ServiceFinished";
							break;
						default:
							servLabel.StyleReference = "TextLeft";
							empLabel.StyleReference = "TextLeft";
							timeLabel.StyleReference = "TextLeft";
							break;
					}

					servLabel.Text = 1+i + ". " + MyService.ServiceJC[i].Service;
					Controls.Add(servLabel);
					empLabel.Text = MyService.ServiceJC[i].Employee;
					Controls.Add(empLabel);
					timeLabel.Text = TimeOfDate(MyService.ServiceJC[i].Duty_time_start) + " - " + TimeOfDate(MyService.ServiceJC[i].Duty_time_end);
					Controls.Add(timeLabel);
					
				} 

				if (int.Parse(RowsOfDataSet()) == 0) 
				{
					Label noneLabel = new Label();
					noneLabel.Text = "--- NONE ---";
					noneLabel.StyleReference = "ErrorLeft";
					Controls.Add(noneLabel);
				}
			}		
		}
	}
}