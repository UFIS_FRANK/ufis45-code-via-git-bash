<%@ Register TagPrefix="custom" Namespace="MobileCustomControls" Assembly="InfoPC" %>
<%@ Page language="c#" Codebehind="Telex.aspx.cs" Inherits="InfoPC.Telex" AutoEventWireup="false" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<HEAD>
	<!--#include file="Stylesheet.aspx" -->
	<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
	<meta name="CODE_LANGUAGE" content="C#">
	<meta name="vs_targetSchema" content="http://schemas.microsoft.com/Mobile/Page">
</HEAD>
<body Xmlns:mobile="http://schemas.microsoft.com/Mobile/WebForm">
	<mobile:Form id="Form1" runat="server" title="InfoPC mobile Client - Telex">
		<P>
			<custom:RotationMask id="rotmask" runat="server" info="true"></custom:RotationMask>
		</P>
		<P>
			<mobile:Image id="Image1" runat="server" ImageUrl="InformationImage.gif"></mobile:Image>
			<custom:TelexMask id="telmask" runat="server"></custom:TelexMask>
		</P>
		<P>
			<mobile:Command id="backBtn" runat="server">Back</mobile:Command>
			<mobile:Command id="exitBtn" runat="server">Exit</mobile:Command>
		</P>
	</mobile:Form>
</body>
