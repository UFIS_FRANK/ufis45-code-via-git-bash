<mobile:StyleSheet runat="server" ID="Style">
   <Style 
	  Name			= "TitleCenter"
	  Font-Size		= "Large" 
	  Font-Name		= "Arial" 
	  Font-Bold		= "true"
      Font-Italic	= "True" 
      ForeColor		= "black" 
      BackColor		= "white" 
      Wrapping		= "Wrap" 
      Alignment		= "Center">  
   </Style>
   <Style 
	  Name			= "TitleLeft"
	  Font-Size		= "Large" 
	  Font-Name		= "Arial" 
	  Font-Bold		= "true"
      Font-Italic	= "false" 
      ForeColor		= "black" 
      BackColor		= "white" 
      Wrapping		= "NoWrap" 
      Alignment		= "Left">  
   </Style>
   <Style 
	  Name			= "TextLeft"
	  Font-Size		= "Normal" 
	  Font-Name		= "Arial" 
	  Font-Bold		= "false"
      Font-Italic	= "false" 
      ForeColor		= "black" 
      BackColor		= "white" 
      Wrapping		= "Wrap" 
      Alignment		= "Left">  
   </Style>   
   <Style 
	  Name			= "ErrorLeft"
	  Font-Size		= "Normal" 
	  Font-Name		= "Arial" 
	  Font-Bold		= "false"
      Font-Italic	= "true" 
      ForeColor		= "red" 
      BackColor		= "white" 
      Wrapping		= "Wrap" 
      Alignment		= "Left">  
   </Style>   
   <Style 
	  Name			= "LinkLeft"
	  Font-Size		= "Normal" 
	  Font-Name		= "Arial" 
	  Font-Bold		= "true"
      Font-Italic	= "true" 
      ForeColor		= "purple" 
      BackColor		= "white" 
      Wrapping		= "NoWrap" 
      Alignment		= "Left">  
   </Style>
   <Style 
	  Name			= "DebugLeft"
	  Font-Size		= "Normal" 
	  Font-Name		= "Arial" 
	  Font-Bold		= "true"
      Font-Italic	= "true" 
      ForeColor		= "purple" 
      BackColor		= "white" 
      Wrapping		= "NoWrap" 
      Alignment		= "Left">  
   </Style>
   <Style 
	  Name			= "ServicePlanned"
	  Font-Size		= "Normal" 
	  Font-Name		= "Arial" 
	  Font-Bold		= "false"
      Font-Italic	= "false" 
      ForeColor		= "blue" 
      BackColor		= "white" 
      Wrapping		= "NoWrap" 
      Alignment		= "Left">  
   </Style>
   <Style 
	  Name			= "ServiceInformed"
	  Font-Size		= "Normal" 
	  Font-Name		= "Arial" 
	  Font-Bold		= "false"
      Font-Italic	= "false" 
      ForeColor		= "orange" 
      BackColor		= "white" 
      Wrapping		= "NoWrap" 
      Alignment		= "Left">  
   </Style>
   <Style 
	  Name			= "ServiceConfirmed"
	  Font-Size		= "Normal" 
	  Font-Name		= "Arial" 
	  Font-Bold		= "false"
      Font-Italic	= "false" 
      ForeColor		= "green" 
      BackColor		= "white" 
      Wrapping		= "NoWrap" 
      Alignment		= "Left">  
   </Style>
   <Style 
	  Name			= "ServiceFinished"
	  Font-Size		= "Normal" 
	  Font-Name		= "Arial" 
	  Font-Bold		= "false"
      Font-Italic	= "false" 
      ForeColor		= "gray" 
      BackColor		= "white" 
      Wrapping		= "NoWrap" 
      Alignment		= "Left">  
   </Style>
   <Style 
	  Name			= "TextHeader"
	  Font-Size		= "Normal" 
	  Font-Name		= "Arial" 
	  Font-Bold		= "true"
      Font-Italic	= "false" 
      ForeColor		= "black" 
      BackColor		= "white" 
      Wrapping		= "NoWrap" 
      Alignment		= "Left">  
   </Style>
</mobile:StyleSheet>