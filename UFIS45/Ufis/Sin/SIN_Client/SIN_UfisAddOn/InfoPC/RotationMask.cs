using System;
using System.IO;
using System.Data;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.HtmlControls;
using Ufis.MobileClient.BL.Common;


namespace MobileCustomControls 
{
	/// <summary>
	/// Summary description for ArrivalInformation.
	/// </summary>
	public class RotationMask : MobileControl, INamingContainer
	{

		private BusinessEntities.FlightJCDS _myFlight;

		public BusinessEntities.FlightJCDS MyFlight 
		{
			get 
			{
				return _myFlight;
			}
			set 
			{
				_myFlight = value;
			}
		}

		private string _mask;

		public string Mask
		{
			get
			{
				return _mask;
			}
			set 
			{
				_mask = value;
			}
		}

		private bool _info;

		public bool Info
		{
			get
			{
				return _info;
			}
			set 
			{
				_info = value;
			}
		}

		protected override void OnDataBinding(EventArgs e)
		{
			ClearChildViewState();
			Controls.Clear();
			CreateChildControls();
		}

		protected override void CreateChildControls()
		{

			if(Mask == "Debug") 
			{

				Label rotationLabel = new Label();
				rotationLabel.Text = "URNO of Arrival Record: " + MyFlight.FlightJC[0].Rotation;
				Controls.Add(rotationLabel);

				Label rotationIDLabel = new Label();
				rotationIDLabel.Text = "Rotation ID: " + MyFlight.FlightJC[0].Rotation_id;
				Controls.Add(rotationIDLabel);

				Label urnoLabel = new Label();
				urnoLabel.Text = "Unique record number: " + MyFlight.FlightJC[0].Urno;
				Controls.Add(urnoLabel);

			}

			if(Info == true)
			{

				Label regnLabel = new Label();
				regnLabel.Text = "Regn.: " + MyFlight.FlightJC[0].Registration;
				regnLabel.StyleReference = "TextHeader";
				Controls.Add(regnLabel);
			
				Label acLabel = new Label();
				acLabel.Text = "A/C: " + MyFlight.FlightJC[0].Aircraft_type;
				acLabel.StyleReference = "TextHeader";
				Controls.Add(acLabel);

				Label dateLabel = new Label();
				dateLabel.Text = "Date: " + MyFlight.FlightJC[0].Last_update;
				dateLabel.StyleReference = "TextHeader";
				Controls.Add(dateLabel);
			}

			if(Mask == "Remark") 
			{

				Label remarkLabel = new Label();
				remarkLabel.Text = "Flight Remarks";
				remarkLabel.StyleReference = "TitleLeft";
				Controls.Add(remarkLabel);

				Label remLabel = new Label();

				if (MyFlight.FlightJC[0].Remark1.Length < 1) {
					remLabel.Text = "--- NONE ---";
					remLabel.StyleReference = "ErrorLeft";
				} 
				else {
					remLabel.Text = MyFlight.FlightJC[0].Remark1 + MyFlight.FlightJC[0].Remark2;
					remLabel.StyleReference = "TextLeft";
				}

				Controls.Add(remLabel);
			}

			if(Mask == "Arrival") 
			{

				Label arrivalLabel = new Label();
				arrivalLabel.Text = "Arrival Information";
				arrivalLabel.StyleReference = "TitleLeft";
				Controls.Add(arrivalLabel);

				Label fltNrALabel = new Label();
				fltNrALabel.Text = "Flt.Nr.: " + MyFlight.FlightJC[0].FlightNumberA;
				fltNrALabel.StyleReference = "TextLeft";
				Controls.Add(fltNrALabel);

				Label origLabel = new Label();
				origLabel.Text = "Orig.: " + MyFlight.FlightJC[0].Origin;
				origLabel.StyleReference = "TextLeft";
				Controls.Add(origLabel);

				Label viaALabel = new Label();
				viaALabel.Text = "Via: " + MyFlight.FlightJC[0].ViaA;
				viaALabel.StyleReference = "TextLeft";
				Controls.Add(viaALabel);

				Label natALabel = new Label();
				natALabel.Text = "Nat: " + MyFlight.FlightJC[0].NatA;
				natALabel.StyleReference = "TextLeft";
				Controls.Add(natALabel);

				Label staLabel = new Label();
				staLabel.Text = "STA: " + MyFlight.FlightJC[0].STA;
				staLabel.StyleReference = "TextLeft";
				Controls.Add(staLabel);

				Label etaLabel = new Label();
				etaLabel.Text = "ETA: " + MyFlight.FlightJC[0].ETA;
				etaLabel.StyleReference = "TextLeft";
				Controls.Add(etaLabel);

				Label posALabel = new Label();
				posALabel.Text = "Pos.: " + MyFlight.FlightJC[0].PositionA;
				posALabel.StyleReference = "TextLeft";
				Controls.Add(posALabel);

				Label gateALabel = new Label();
				gateALabel.Text = "Gate: " + MyFlight.FlightJC[0].GateA;
				gateALabel.StyleReference = "TextLeft";
				Controls.Add(gateALabel);

				Label bltALabel = new Label();
				bltALabel.Text = "Blt: " + MyFlight.FlightJC[0].Baggage_beltA;
				bltALabel.StyleReference = "TextLeft";
				Controls.Add(bltALabel);

				Label paxALabel = new Label();
				paxALabel.Text = "PAX: " + MyFlight.FlightJC[0].PaxA;
				paxALabel.StyleReference = "TextLeft";
				Controls.Add(paxALabel);

				Label cargoALabel = new Label();
				cargoALabel.Text = "CGO: " + MyFlight.FlightJC[0].CargoA + " t";
				cargoALabel.StyleReference = "TextLeft";
				Controls.Add(cargoALabel);

				Label bagsALabel = new Label();
				bagsALabel.Text = "Bag: " + MyFlight.FlightJC[0].BagsA + " St.";
				bagsALabel.StyleReference = "TextLeft";
				Controls.Add(bagsALabel);

				Label mailALabel = new Label();
				mailALabel.Text = "Mail: " + MyFlight.FlightJC[0].MailA + " St.";
				mailALabel.StyleReference = "TextLeft";
				Controls.Add(mailALabel);


			} 
			else if(Mask == "Departure") 
			{

				Label departureLabel = new Label();
				departureLabel.Text = "Departure Information";
				departureLabel.StyleReference = "TitleLeft";
				Controls.Add(departureLabel);

				Label fltNrDLabel = new Label();
				fltNrDLabel.Text = "Flt.Nr.: " + MyFlight.FlightJC[0].FlightNumberD;
				fltNrDLabel.StyleReference = "TextLeft";
				Controls.Add(fltNrDLabel);

				Label destLabel = new Label();
				destLabel.Text = "Dest.: " + MyFlight.FlightJC[0].Destination;
				destLabel.StyleReference = "TextLeft";
				Controls.Add(destLabel);

				Label viaDLabel = new Label();
				viaDLabel.Text = "Via: " + MyFlight.FlightJC[0].ViaD;
				viaDLabel.StyleReference = "TextLeft";
				Controls.Add(viaDLabel);

				Label natDLabel = new Label();
				natDLabel.Text = "Nat: " + MyFlight.FlightJC[0].NatD;
				natDLabel.StyleReference = "TextLeft";
				Controls.Add(natDLabel);

				Label stdLabel = new Label();
				stdLabel.Text = "STD: " + MyFlight.FlightJC[0].STD;
				stdLabel.StyleReference = "TextLeft";
				Controls.Add(stdLabel);

				Label etdLabel = new Label();
				etdLabel.Text = "ETD: " + MyFlight.FlightJC[0].ETD;
				etdLabel.StyleReference = "TextLeft";
				Controls.Add(etdLabel);

				Label posDLabel = new Label();
				posDLabel.Text = "Pos.: " + MyFlight.FlightJC[0].PositionD;
				posDLabel.StyleReference = "TextLeft";
				Controls.Add(posDLabel);

				Label gateDLabel = new Label();
				gateDLabel.Text = "Gate: " + MyFlight.FlightJC[0].GateD;
				gateDLabel.StyleReference = "TextLeft";
				Controls.Add(gateDLabel);

				Label bltDLabel = new Label();
				bltDLabel.Text = "Blt: " + MyFlight.FlightJC[0].Baggage_beltD;
				bltDLabel.StyleReference = "TextLeft";
				Controls.Add(bltDLabel);

				Label paxDLabel = new Label();
				paxDLabel.Text = "PAX: " + MyFlight.FlightJC[0].PaxD;
				paxDLabel.StyleReference = "TextLeft";
				Controls.Add(paxDLabel);

				Label cargoDLabel = new Label();
				cargoDLabel.Text = "CGO: " + MyFlight.FlightJC[0].CargoD + " t";
				cargoDLabel.StyleReference = "TextLeft";
				Controls.Add(cargoDLabel);

				Label bagsDLabel = new Label();
				bagsDLabel.Text = "Bag: " + MyFlight.FlightJC[0].BagsD + " St.";
				bagsDLabel.StyleReference = "TextLeft";
				Controls.Add(bagsDLabel);

				Label mailDLabel = new Label();
				mailDLabel.Text = "Mail: " + MyFlight.FlightJC[0].MailD + " St.";
				mailDLabel.StyleReference = "TextLeft";
				Controls.Add(mailDLabel);

			}


			ChildControlsCreated = true;
		}
	}
}
