<%@ Register TagPrefix="custom" Namespace="MobileCustomControls" Assembly="InfoPC" %>
<%@ Page language="c#" Codebehind="Remarks.aspx.cs" Inherits="InfoPC.Remarks" AutoEventWireup="false" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<HEAD>
	<!--#include file="Stylesheet.aspx" -->
	<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
	<meta name="CODE_LANGUAGE" content="C#">
	<meta name="vs_targetSchema" content="http://schemas.microsoft.com/Mobile/Page">
</HEAD>
<body Xmlns:mobile="http://schemas.microsoft.com/Mobile/WebForm">
	<mobile:Form id="Form1" runat="server" title="InfoPC mobile Client - Remarks">
		<custom:RotationMask id="rotinfo" runat="server" info="true" ></custom:RotationMask>
		<mobile:Image id="Image1" runat="server" ImageUrl="FlightImage.gif"></mobile:Image>
		<custom:RotationMask id="rotmask" runat="server" mask="Remark"></custom:RotationMask>
		<mobile:Command id="backBtn" runat="server">Back</mobile:Command>
		<mobile:Command id="exitBtn" runat="server">Exit</mobile:Command>
	</mobile:Form>
</body>
