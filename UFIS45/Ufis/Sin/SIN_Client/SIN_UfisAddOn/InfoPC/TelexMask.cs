using System;
using System.IO;
using System.Data;
using System.Web.UI;
using System.Web.UI.MobileControls;
using Ufis.MobileClient.BL.Common;


namespace MobileCustomControls 
{
	/// <summary>
	/// Summary description for ArrivalInformation.
	/// </summary>
	public class TelexMask : System.Web.UI.MobileControls.List
	{

		private BusinessEntities.TelexJCDS _myTelex;
	
		public BusinessEntities.TelexJCDS MyTelex 
		{
			get {return _myTelex;}
			set {_myTelex = value;}
		}

		protected string RowsOfDataSet()
		{
			string rows = "0";

			if(MyTelex != null)
				rows = MyTelex.TelexJC.Count.ToString();
					
			return rows;
		}

		protected override void OnLoad(EventArgs e)
		{
			if (!Page.IsPostBack)
			{

				base.OnLoad(e);

				for (int i=0; i <= int.Parse(RowsOfDataSet()); i++)
				{

					try
					{
						string telextype = "";
						if (MyTelex.TelexJC[i].Telex_type != null) 
						{
							telextype = MyTelex.TelexJC[i].Telex_type;
						} 
						else telextype = "NOTYPE";

						MobileListItem item = new MobileListItem();

						item.Text = telextype + " " + MyTelex.TelexJC[i].Send_receive_time;

						item.Value = i.ToString();
						item.StyleReference = "LinkLeft";
						Items.Add(item);

					} 
					catch (Exception exc) {}
				}

				if(int.Parse(RowsOfDataSet()) == 0)
				{
					Label noneLabel = new Label();
					noneLabel.Text = "---NONE---";
					noneLabel.StyleReference = "ErrorLeft";
					Controls.Add(noneLabel);
				}

			}

			this.ItemCommand += new ListCommandEventHandler(HandleCommand);
		}

		protected void HandleCommand(Object sender, ListCommandEventArgs e)
		{

			Link telexLink = new Link();
			telexLink.Text = "Show Loading Information";
			telexLink.NavigateUrl = "Telex.aspx";
			telexLink.StyleReference = "LinkLeft";
			Controls.Add(telexLink);

			
			int selection = int.Parse(e.ListItem.Value);

			Label titleLabel = new Label();
			titleLabel.Text = MyTelex.TelexJC[selection].Telex_type + " " + MyTelex.TelexJC[selection].Date_of_creation;
			titleLabel.StyleReference = "TitleLeft";
			Controls.Add(titleLabel);

			Label fnLabel = new Label();
			fnLabel.Text = "Flight: " + MyTelex.TelexJC[selection].Flno;
			fnLabel.StyleReference = "TextLeft";
			Controls.Add(fnLabel);

			Label ttypeLabel = new Label();
			ttypeLabel.Text = "Telex type: " + MyTelex.TelexJC[selection].Telex_type;
			ttypeLabel.StyleReference = "TextLeft";
			Controls.Add(ttypeLabel);

			Label tlxLabel = new Label();
			tlxLabel.Text = "Telex message: " + MyTelex.TelexJC[selection].Telex_part1 + MyTelex.TelexJC[selection].Telex_part2;
			tlxLabel.StyleReference = "TextLeft";
			Controls.Add(tlxLabel);

			/*
			Label airlineLabel = new Label();
			airlineLabel.Text = "Airline: " + MyTelex.TelexJC[selection].Airline;
			Controls.Add(airlineLabel);

			Label remarksLabel = new Label();
			remarksLabel.Text = "Remarks: " + MyTelex.TelexJC[selection].Remarks;
			Controls.Add(remarksLabel);

			Label docLabel = new Label();
			docLabel.Text = "Date of creation: " + MyTelex.TelexJC[selection].Date_of_creation;
			Controls.Add(docLabel);

			Label fkLabel = new Label();
			fkLabel.Text = "Flight key: " + MyTelex.TelexJC[selection].Flight_key;
			Controls.Add(fkLabel);

			Label fdLabel = new Label();
			fdLabel.Text = "Flight date: " + MyTelex.TelexJC[selection].Flight_date;
			Controls.Add(fdLabel);			

			Label fsLabel = new Label();
			fsLabel.Text = "Flight suffix: " + MyTelex.TelexJC[selection].Flight_suffix;
			Controls.Add(fsLabel);

			Label fuLabel = new Label();
			fuLabel.Text = "Flight urno: " + MyTelex.TelexJC[selection].Flight_urno;
			Controls.Add(fuLabel);

						Label hopoLabel = new Label();
						hopoLabel.Text = "Home airport: " + MyTelex.TelexJC[selection].Home_airport;
						Controls.Add(hopoLabel);

						Label updateLabel = new Label();
						updateLabel.Text = "Last update: " + MyTelex.TelexJC[selection].Last_update;
						Controls.Add(updateLabel);

						Label copLabel = new Label();
						copLabel.Text = "Counter of parts: " + MyTelex.TelexJC[selection].Counter_of_parts;
						Controls.Add(copLabel);

						Label mnLabel = new Label();
						mnLabel.Text = "Message number: " + MyTelex.TelexJC[selection].Message_number;
						Controls.Add(mnLabel);

						Label mtLabel = new Label();
						mtLabel.Text = "Message text: " + MyTelex.TelexJC[selection].Message_text;
						Controls.Add(mtLabel);

						Label logLabel = new Label();
						logLabel.Text = "Logging code: " + MyTelex.TelexJC[selection].Logging_code;
						Controls.Add(logLabel);

						Label srLabel = new Label();
						srLabel.Text = "Sender receiver code: " + MyTelex.TelexJC[selection].Sender_receiver_code;
						Controls.Add(srLabel);

						Label statLabel = new Label();
						statLabel.Text = "Status: " + MyTelex.TelexJC[selection].Status;
						Controls.Add(statLabel);

						Label srtLabel = new Label();
						srtLabel.Text = "Send receive time: " + MyTelex.TelexJC[selection].Send_receive_time;
						Controls.Add(srtLabel);

						Label ikLabel = new Label();
						ikLabel.Text = "Internal key: " + MyTelex.TelexJC[selection].Internal_key;
						Controls.Add(ikLabel);
	
			Label tnumLabel = new Label();
			tnumLabel.Text = "Telex number: " + MyTelex.TelexJC[selection].Telex_number;
			Controls.Add(tnumLabel);

			Label urnoLabel = new Label();
			urnoLabel.Text = "Urno: " + MyTelex.TelexJC[selection].Urno;
			Controls.Add(urnoLabel);

			Label creatorLabel = new Label();
			creatorLabel.Text = "Creator: " + MyTelex.TelexJC[selection].Creator;
			Controls.Add(creatorLabel);

			Label updaterLabel = new Label();
			updaterLabel.Text = "Updater: " + MyTelex.TelexJC[selection].Updater;
			Controls.Add(updaterLabel);
*/
		}
	}
}
