<%@ Page language="c#" Codebehind="JobCard.aspx.cs" Inherits="InfoPC.JobCard" AutoEventWireup="false" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="custom" Namespace="MobileCustomControls" Assembly="InfoPC" %>
<HTML>
	<!--#include file="Stylesheet.aspx" -->
	<body>
		<mobile:form runat="server" ID="Form1" title="InfoPC mobile Client - JobCard">
			<P>
				<custom:RotationMask id="rotmask" runat="server" info="true"></custom:RotationMask></P>
			<P>
				<mobile:Image id="Image1" runat="server" ImageUrl="ArrivalImage.gif"></mobile:Image>
				<mobile:Command id="arrivalBtn" runat="server" Format="Link" StyleReference="LinkLeft">Show Arrival Information</mobile:Command>
				<mobile:Image id="Image2" runat="server" ImageUrl="DepartureImage.gif"></mobile:Image>
				<mobile:Command id="departureBtn" runat="server" Format="Link" StyleReference="LinkLeft">Show Departure Information</mobile:Command></P>
			<P>
				<mobile:Image id="Image4" runat="server" ImageUrl="ServiceImage.gif"></mobile:Image>
				<custom:ServiceMask id="sermask" runat="server"></custom:ServiceMask></P>
			<P>
				<mobile:Image id="Image3" runat="server" ImageUrl="FlightImage.gif"></mobile:Image>
				<mobile:Command id="remarksBtn" runat="server" Format="Link" StyleReference="LinkLeft">Show Flight Remarks</mobile:Command>
				<mobile:Image id="Image5" runat="server" ImageUrl="InformationImage.gif"></mobile:Image>
				<mobile:Command id="telexBtn" runat="server" Format="Link" StyleReference="LinkLeft">Show Loading Information</mobile:Command></P>
			<P>
				<mobile:Command id="backBtn" runat="server">Back</mobile:Command>
				<mobile:Command id="exitBtn" runat="server">Exit</mobile:Command></P>
		</mobile:form>
	</body>
</HTML>
