<%@ Page language="c#" Codebehind="Bye.aspx.cs" Inherits="InfoPC.Bye" AutoEventWireup="false" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<HEAD>
	<!--#include file="Stylesheet.aspx" -->
	<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
	<meta name="CODE_LANGUAGE" content="C#">
	<meta name="vs_targetSchema" content="http://schemas.microsoft.com/Mobile/Page">
</HEAD>
<body Xmlns:mobile="http://schemas.microsoft.com/Mobile/WebForm">
	<mobile:Form id="Form1" runat="server" title="InfoPC mobile Client - Bye">
		<mobile:Label id="Label1" runat="server">Your session has been terminated!</mobile:Label>
		<mobile:Image id="pbABBLogo" runat="server" Alignment="Center" ImageUrl="ABBLogo.JPG"></mobile:Image>
		<mobile:Image id="pbUFISLogo" runat="server" Alignment="Center" ImageUrl="Ufislogo.JPG"></mobile:Image>
	</mobile:Form>
</body>
