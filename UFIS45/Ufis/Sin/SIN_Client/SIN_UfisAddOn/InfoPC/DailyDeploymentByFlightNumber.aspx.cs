using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Microsoft.ApplicationBlocks.UIProcess;
using Ufis.MobileClient.BL.Common;

namespace InfoPC
{
	/// <summary>
	/// Summary description for DailyDeploymentByFlightNumber.
	/// </summary>
	public class DailyDeploymentByFlightNumber : MobileWebFormView
	{
		protected System.Web.UI.MobileControls.Label Label1;
		protected System.Web.UI.MobileControls.Label Label2;
		protected System.Web.UI.MobileControls.Label Label3;
		protected System.Web.UI.MobileControls.Label Label4;
		protected System.Web.UI.MobileControls.Calendar ctlDate;
		protected System.Web.UI.MobileControls.TextBox txtRegn;
		protected System.Web.UI.MobileControls.TextBox txtAct;
		protected System.Web.UI.MobileControls.Command btnOK;
		protected System.Web.UI.MobileControls.Command bCheck;
		protected System.Web.UI.MobileControls.SelectionList slFlno;
		protected System.Web.UI.MobileControls.Label lblErrorText;
		protected System.Web.UI.MobileControls.Form Form1;

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (this.Application["Controller"] == null)
			{
				this.Application.Add("Controller",ThisController);
			}

			if(!this.IsPostBack)
			{
				DateTime from,to,current;
				ThisController.Duration(out from,out to,out current);

				this.ctlDate.SelectedDate = current;

				ArrayList listOfFlights = ThisController.GetListOfFlights(this.ctlDate.SelectedDate.Date);
				this.slFlno.Items.Clear();
				foreach(string flno in listOfFlights)
				{
					this.slFlno.Items.Add(flno);
				}
				this.ctlDate.DataBind();
			}
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.bCheck.Click += new System.EventHandler(this.bCheck_Click);
			this.ctlDate.SelectionChanged += new System.EventHandler(this.ctlDate_SelectionChanged);
			this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
			this.Form1.Activate += new System.EventHandler(this.Form1_Activate);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private Controller ThisController
		{
			get
			{
				return (Controller)this.Controller;
			}
		}

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			string regn,act;
			if(ThisController.SelectFlight(this.slFlno.Selection.Value, this.ctlDate.SelectedDate.Date,out regn, out act))
			{
				this.txtRegn.Text = regn;
				this.txtAct.Text = act;
				this.lblErrorText.Visible = false;
				ThisController.SelectService("Next");
			}
			else
			{
				this.lblErrorText.Visible = true;
				this.lblErrorText.Text = "specified valid flight number";
			}	
		}



		private void bCheck_Click(object sender, System.EventArgs e)
		{
			string regn,act;
			if(ThisController.SelectFlight(this.slFlno.Selection.Value, this.ctlDate.SelectedDate.Date,out regn, out act))
			{
				this.txtRegn.Text = regn;
				this.txtAct.Text = act;
				this.lblErrorText.Visible = false;
			}
			else
			{
				this.txtRegn.Text =string.Empty;
				this.txtAct.Text = string.Empty;
				this.lblErrorText.Visible = true;
				this.lblErrorText.Text = "Flight number is not valid";
			}
		}

		private void ctlDate_SelectionChanged(object sender, System.EventArgs e)
		{
			DateTime from,to,current;
			if(ThisController.Duration(out from, out to, out current))
			{
				if(this.ctlDate.SelectedDate.Date < from || this.ctlDate.SelectedDate.Date > to)
				{
					this.lblErrorText.Visible= true;
					this.lblErrorText.Text = string.Format("Date must be within {0} and {1}",from.ToShortDateString(), to.ToShortDateString());
					this.ctlDate.SelectedDate=current;
					ArrayList listOfFlights = ThisController.GetListOfFlights(this.ctlDate.SelectedDate.Date);
					this.slFlno.Items.Clear();
					foreach(string flno in listOfFlights)
					{
						this.slFlno.Items.Add(flno);
					}
					this.DataBind();
				}
				else
				{
					ArrayList listOfFlights = ThisController.GetListOfFlights(this.ctlDate.SelectedDate.Date);
					this.slFlno.Items.Clear();
					foreach(string flno in listOfFlights)
					{
						this.slFlno.Items.Add(flno);
					}
					
					this.slFlno.SelectedIndex = -1;
					this.lblErrorText.Visible = false;
					this.lblErrorText.Text = string.Empty;
					this.DataBind();
				}
			}
		}

		private void Form1_Activate(object sender, System.EventArgs e)
		{
		
		}
	}
}
