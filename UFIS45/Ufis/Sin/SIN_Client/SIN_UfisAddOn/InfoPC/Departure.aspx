<%@ Register TagPrefix="custom" Namespace="MobileCustomControls" Assembly="InfoPC" %>
<%@ Page language="c#" Codebehind="Departure.aspx.cs" Inherits="InfoPC.Departure" AutoEventWireup="false" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<HEAD>
	<!--#include file="Stylesheet.aspx" -->
	<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
	<meta name="CODE_LANGUAGE" content="C#">
	<meta name="vs_targetSchema" content="http://schemas.microsoft.com/Mobile/Page">
</HEAD>
<body Xmlns:mobile="http://schemas.microsoft.com/Mobile/WebForm">
	<mobile:Form id="Form1" runat="server" title="InfoPC mobile Client - Departure">
		<CUSTOM:ROTATIONMASK id="rotinfo" runat="server" info="true"></CUSTOM:ROTATIONMASK>
		<mobile:Image id="Image1" runat="server" ImageUrl="ArrivalImage.gif"></mobile:Image>
		<mobile:Command id="nextBtn" runat="server" StyleReference="LinkLeft" Format="Link">Show Arrival Information</mobile:Command>
		<mobile:Image id="Image2" runat="server" ImageUrl="DepartureImage.gif"></mobile:Image>
		<CUSTOM:ROTATIONMASK id="rotmask" runat="server" mask="Departure"></CUSTOM:ROTATIONMASK>
		<mobile:Command id="backBtn" runat="server">Back</mobile:Command>
		<mobile:Command id="exitBtn" runat="server">Exit</mobile:Command>
	</mobile:Form>
</body>
