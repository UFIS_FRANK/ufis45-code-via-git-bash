using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Microsoft.ApplicationBlocks.UIProcess;
using Ufis.MobileClient.BL.Common;

namespace InfoPC
{
	/// <summary>
	/// Summary description for Arrival.
	/// </summary>
	public class Arrival : MobileWebFormView
	{
		protected System.Web.UI.MobileControls.Form Form1;
		protected System.Web.UI.MobileControls.Command backBtn;
		protected System.Web.UI.MobileControls.Command exitBtn;
		protected MobileCustomControls.RotationMask rotinfo, rotmask;
		protected System.Web.UI.MobileControls.Image Image1;
		protected System.Web.UI.MobileControls.Image Image2;
		protected System.Web.UI.MobileControls.Command nextBtn;

		private void Page_Load(object sender, System.EventArgs e)
		{
			if (!this.IsPostBack)
			{
				int refresh = (int)ThisController.PageRefresh.TotalSeconds;
				Response.AppendHeader("Refresh",refresh.ToString());
			}

			this.PrepareData();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.nextBtn.Click += new System.EventHandler(this.nextBtn_Click);
			this.backBtn.Click += new System.EventHandler(this.backBtn_Click);
			this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
			this.Form1.Activate += new System.EventHandler(this.Form1_Activate);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		public Controller ThisController
		{
			get
			{
				return (Controller)this.Controller;
			}
		}


		private void PrepareData()
		{
			BusinessEntities.FlightJCDS flight;
			DateTime date;

			if (ThisController.FlightInformationJC(out flight,out date))
			{
				rotmask.MyFlight = flight;
				rotinfo.MyFlight = flight;
			}
		}
	


		private void exitBtn_Click(object sender, System.EventArgs e)
		{
			ThisController.SelectService("Exit");
		}
		private void backBtn_Click(object sender, System.EventArgs e)
		{
			ThisController.SelectService("Previous");
		}
		private void nextBtn_Click(object sender, System.EventArgs e)
		{
			ThisController.SelectService("Next");
		}

		private void Form1_Activate(object sender, System.EventArgs e)
		{
		
		}

	}
}
