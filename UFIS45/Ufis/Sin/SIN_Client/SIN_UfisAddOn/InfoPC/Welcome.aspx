<%@ Page language="c#" Codebehind="Welcome.aspx.cs" Inherits="InfoPC.Welcome" AutoEventWireup="false" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>

<!--#include file="Stylesheet.aspx" -->

<body>
	<mobile:Form id="Form1" runat="server" title="InfoPC mobile Client - Welcome">
		<mobile:Label id="Label1" runat="server" StyleReference="TitleCenter">Welcome to the InfoPC mobile Client</mobile:Label>
		<mobile:Image id="pbABBLogo" runat="server" ImageUrl="ABBLogo.JPG" Alignment="Center"></mobile:Image>
		<mobile:Image id="pbUFISLogo" runat="server" ImageUrl="Ufislogo.JPG" Alignment="Center"></mobile:Image>
	</mobile:Form>
</body>
