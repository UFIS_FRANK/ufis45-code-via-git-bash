<%@ Page language="c#" Codebehind="DailyDeploymentByFlightNumber.aspx.cs" Inherits="InfoPC.DailyDeploymentByFlightNumber" AutoEventWireup="false" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<!--#include file="Stylesheet.aspx" -->
<body>
	<mobile:Form id="Form1" runat="server" title="InfoPC mobile Client - Daily deployment by flight number">
		<mobile:Label id="Label1" runat="server" StyleReference="TitleLeft">Flight number :</mobile:Label>
		<mobile:SelectionList id="slFlno" runat="server" DESIGNTIMEDRAGDROP="11"></mobile:SelectionList>
		<mobile:Label id="lblErrorText" runat="server" StyleReference="ErrorLeft"></mobile:Label>
		<mobile:Command id="bCheck" runat="server">Check</mobile:Command>
		<mobile:Label id="Label2" runat="server" StyleReference="TitleLeft">Date:</mobile:Label>
		<mobile:Calendar id="ctlDate" runat="server" StyleReference="TextLeft"></mobile:Calendar>
		<mobile:Label id="Label3" runat="server" StyleReference="TitleLeft">Aircraft registration :</mobile:Label>
		<mobile:TextBox id="txtRegn" runat="server"></mobile:TextBox>
		<mobile:Label id="Label4" runat="server" StyleReference="TitleLeft">Aircraft type :</mobile:Label>
		<mobile:TextBox id="txtAct" runat="server"></mobile:TextBox>
		<mobile:Command id="btnOK" runat="server">Submit</mobile:Command>
	</mobile:Form>
</body>
