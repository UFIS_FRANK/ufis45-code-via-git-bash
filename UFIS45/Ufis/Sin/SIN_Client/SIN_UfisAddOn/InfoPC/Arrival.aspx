<%@ Register TagPrefix="custom" Namespace="MobileCustomControls" Assembly="InfoPC" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Page language="c#" Codebehind="Arrival.aspx.cs" Inherits="InfoPC.Arrival" AutoEventWireup="false" %>
<HEAD>
	<!--#include file="Stylesheet.aspx" -->
	<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
	<meta name="CODE_LANGUAGE" content="C#">
	<meta name="vs_targetSchema" content="http://schemas.microsoft.com/Mobile/Page">
</HEAD>
<body Xmlns:mobile="http://schemas.microsoft.com/Mobile/WebForm">
	<mobile:Form id="Form1" runat="server" title="InfoPC mobile Client - Arrival">
		<CUSTOM:ROTATIONMASK id="rotinfo" runat="server" info="true"></CUSTOM:ROTATIONMASK>
		<mobile:Image id="Image1" runat="server" ImageUrl="DepartureImage.gif"></mobile:Image>
		<mobile:Command id="nextBtn" runat="server" StyleReference="LinkLeft" Format="Link">Show Departure Information</mobile:Command>
		<mobile:Image id="Image2" runat="server" ImageUrl="ArrivalImage.gif"></mobile:Image>
		<CUSTOM:ROTATIONMASK id="rotmask" runat="server" mask="Arrival"></CUSTOM:ROTATIONMASK>
		<mobile:Command id="backBtn" runat="server">Back</mobile:Command>
		<mobile:Command id="exitBtn" runat="server">Exit</mobile:Command>
	</mobile:Form>
</body>
