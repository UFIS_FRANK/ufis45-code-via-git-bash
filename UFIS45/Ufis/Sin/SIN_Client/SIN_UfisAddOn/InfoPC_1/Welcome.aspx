<%@ Page language="c#" Classname="InfoPC.Welcome" CompileWith="Welcome.aspx.cs" Inherits="System.Web.UI.MobileControls.MobilePage" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>

<!--#include file="Stylesheet.aspx" -->

<body>
	<mobile:Form id="Form1" runat="server" title="InfoPC mobile Client - Welcome">
		<mobile:Label id="Label1" runat="server" StyleReference="TitleCenter">Welcome to the InfoPC mobile Client</mobile:Label>
		<mobile:Image id="pbABBLogo" runat="server" ImageUrl="images/ABBLogo.JPG" Alignment="Center" NavigateUrl="Welcome.aspx"></mobile:Image>
		<mobile:Image id="pbUFISLogo" runat="server" ImageUrl="images/Ufislogo.JPG" Alignment="Center" NavigateUrl="Welcome.aspx"></mobile:Image>
		<mobile:Label ID="Label2" Runat="server" StyleReference="TextLeft">Please stand-by. The service is starting ...</mobile:Label>
	</mobile:Form>
</body>

