using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Microsoft.ApplicationBlocks.UIProcess;
using Ufis.MobileClient.BL.Common;



namespace InfoPC
{
	/// <summary>
	/// Summary description for DailyDeploymentByStaffNumber.
	/// </summary>
    public partial class DailyDeploymentByStaffNumber
    {


		private void Page_Load(object sender, System.EventArgs e)
		{
            if (this.Application["Controller"] == null)
            {
                this.Application.Add("Controller", ThisController);
            }

            if (!this.IsPostBack)
            {
                DateTime from, to, current;
                ThisController.Duration(out from, out to, out current);
                this.ctlDate.SelectedDate = current;
                this.ctlDate.DataBind();
            }
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.CmdSubmit.Click += new System.EventHandler(this.CmdSubmit_Click);
			this.CmdExit.Click += new System.EventHandler(this.CmdExit_Click);
			this.Load += new System.EventHandler(this.Page_Load);

		}
		#endregion

		private Controller ThisController
		{
			get
			{
				return (Controller)this.Controller;
			}
		}

		private void CmdSubmit_Click(object sender, System.EventArgs e)
		{

            if (ThisController.SelectStaffJC(this.txtPeno.Text, this.ctlDate.SelectedDate.Date))
            {
				this.lblErrorText.Visible = false;
				ThisController.SelectService("Next");		
			}
			else
			{
				this.lblErrorText.Visible = true;
				this.lblErrorText.Text = "specified a valid employee";
			}		
		}
		

		private void CmdExit_Click(object sender, System.EventArgs e)
		{
			ThisController.SelectService("Exit");		
		}

        private void Form1_Activate(object sender, System.EventArgs e)
        {

        }

        private void ctlDate_SelectionChanged(object sender, System.EventArgs e)
        {
            DateTime from, to, current;
            if (ThisController.Duration(out from, out to, out current))
            {
                if (this.ctlDate.SelectedDate.Date < from || this.ctlDate.SelectedDate.Date > to)
                {
                    this.lblErrorText.Visible = true;
                    this.lblErrorText.Text = string.Format("Date must be within {0} and {1}", from.ToShortDateString(), to.ToShortDateString());
                    this.ctlDate.SelectedDate = current;
                    this.DataBind();
                }
                else
                {
                    this.lblErrorText.Visible = false;
                    this.lblErrorText.Text = string.Empty;
                    this.DataBind();
                }
            }
        }
    }
}
