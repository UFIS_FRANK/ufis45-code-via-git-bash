<%@ Page language="c#" Classname="InfoPC.Bye" CompileWith="Bye.aspx.cs" Inherits="Microsoft.ApplicationBlocks.UIProcess.MobileWebFormView" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>

<HEAD>
	<!--#include file="Stylesheet.aspx" -->
	<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1"/>
	<meta name="CODE_LANGUAGE" content="C#"/>
	 
</HEAD>
<body>
	<mobile:Form id="Form1" runat="server" title="InfoPC mobile Client - Bye">
		<mobile:Label id="Label1" runat="server">Your session has been terminated!</mobile:Label>
		<mobile:Image id="pbABBLogo" runat="server" Alignment="Center" ImageUrl="images/ABBLogo.JPG"></mobile:Image>
		<mobile:Image id="pbUFISLogo" runat="server" Alignment="Center" ImageUrl="images/Ufislogo.JPG"></mobile:Image>
	</mobile:Form>
</body>

