<%@ Register TagPrefix="custom" Namespace="MobileCustomControls" Assembly="MobileCustomControls" %>
<%@ Page language="c#" Classname="InfoPC.Remarks" CompileWith="Remarks.aspx.cs" Inherits="Microsoft.ApplicationBlocks.UIProcess.MobileWebFormView" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>

<HEAD>
	<!--#include file="Stylesheet.aspx" -->
	<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1"/>
	<meta name="CODE_LANGUAGE" content="C#"/>
	 
</HEAD>
<body>
	<mobile:Form id="Form1" runat="server" title="InfoPC mobile Client - Remarks" onactivate="Form1_Activate">
		<custom:rotationmask id="rotinfo" runat="server" info="true" ></custom:RotationMask>
		<mobile:Image id="Image1" runat="server" ImageUrl="images/FlightImage.gif"></mobile:Image>
		<custom:rotationmask id="rotmask" runat="server" mask="Remark"></custom:RotationMask>
		<mobile:Command id="backBtn" runat="server" onclick="backBtn_Click">Back</mobile:Command>
		<mobile:Command id="exitBtn" runat="server" onclick="exitBtn_Click">Exit</mobile:Command>
	</mobile:Form>
</body>

