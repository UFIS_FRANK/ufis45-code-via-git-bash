<%@ Page language="c#" Classname="InfoPC.JobCard" CompileWith="JobCard.aspx.cs" Inherits="Microsoft.ApplicationBlocks.UIProcess.MobileWebFormView" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="custom" Namespace="MobileCustomControls" Assembly="MobileCustomControls" %>

	<!--#include file="Stylesheet.aspx" -->
	<body>
		<mobile:form runat="server" ID="Form1" title="InfoPC mobile Client - JobCard" onactivate="Form1_Activate"> 
		    <mobile:Command id="staffBtn" runat="server" onclick="staffBtn_Click" Format="Link" stylereference="LinkLeft">Show Staff Information</mobile:Command> 
		    <mobile:Label ID="serviceLabel" Runat="server" StyleReference="TitleLeft">Services for Shift</mobile:Label>
		    <custom:ServiceMask id="servicemask" runat="server"></custom:ServiceMask> 
	        <mobile:Command id="backBtn" runat="server" onclick="backBtn_Click">Back</mobile:Command> 
		    <mobile:Command id="exitBtn" runat="server" onclick="exitBtn_Click">Exit</mobile:Command>
		</mobile:form>
	</body>

