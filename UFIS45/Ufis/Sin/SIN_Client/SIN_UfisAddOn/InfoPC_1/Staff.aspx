<%@ Page language="c#" Classname="InfoPC.Staff" CompileWith="Staff.aspx.cs" Inherits="Microsoft.ApplicationBlocks.UIProcess.MobileWebFormView" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="custom" Namespace="MobileCustomControls" Assembly="MobileCustomControls" %>

	<!--#include file="Stylesheet.aspx" -->
	<body>
		<mobile:form runat="server" ID="Form1" title="InfoPC mobile Client - JobCard" onactivate="Form1_Activate">
			<custom:StaffMask id="stamask" runat="server"></custom:StaffMask>
			<mobile:Image id="Image4" runat="server" ImageUrl="images/ServiceImage.gif"></mobile:Image>
			<mobile:Command id="backBtn" runat="server" onclick="backBtn_Click">Back</mobile:Command>
			<mobile:Command id="exitBtn" runat="server" onclick="exitBtn_Click">Exit</mobile:Command>
		</mobile:form>
	</body>