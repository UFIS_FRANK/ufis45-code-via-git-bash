<mobile:StyleSheet runat="server" ID="Style">
   <Style 
	  Name			= "TitleCenter"
	  Font-Size		= "Large" 
	  Font-Name		= "Arial" 
	  Font-Bold		= "true"
      Font-Italic	= "True" 
      ForeColor		= "black" 
      BackColor		= "white" 
      Wrapping		= "Wrap" 
      Alignment		= "Center">  
   </Style>
   <Style 
	  Name			= "TitleLeft"
	  Font-Size		= "Large" 
	  Font-Name		= "Arial" 
	  Font-Bold		= "true"
      Font-Italic	= "false" 
      ForeColor		= "black" 
      BackColor		= "white" 
      Wrapping		= "NoWrap" 
      Alignment		= "Left">  
   </Style>
   <Style 
	  Name			= "TextLeft"
	  Font-Size		= "Normal" 
	  Font-Name		= "Arial" 
	  Font-Bold		= "false"
      Font-Italic	= "false" 
      ForeColor		= "black" 
      BackColor		= "white" 
      Wrapping		= "Wrap" 
      Alignment		= "Left">  
   </Style>   
   <Style 
	  Name			= "ErrorLeft"
	  Font-Size		= "Normal" 
	  Font-Name		= "Arial" 
	  Font-Bold		= "false"
      Font-Italic	= "true" 
      ForeColor		= "red" 
      BackColor		= "white" 
      Wrapping		= "Wrap" 
      Alignment		= "Left">  
   </Style>   
   <Style 
	  Name			= "LinkLeft"
	  Font-Size		= "Normal" 
	  Font-Name		= "Arial" 
	  Font-Bold		= "true"
      Font-Italic	= "true" 
      ForeColor		= "purple" 
      BackColor		= "white" 
      Wrapping		= "NoWrap" 
      Alignment		= "Left">  
   </Style>
   <Style 
	  Name			= "DebugLeft"
	  Font-Size		= "Normal" 
	  Font-Name		= "Arial" 
	  Font-Bold		= "true"
      Font-Italic	= "true" 
      ForeColor		= "purple" 
      BackColor		= "white" 
      Wrapping		= "NoWrap" 
      Alignment		= "Left">  
   </Style>
   <Style 
	  Name			= "ServicePlanned"
	  Font-Size		= "Normal" 
	  Font-Name		= "Arial" 
	  Font-Bold		= "true"
      Font-Italic	= "false" 
      ForeColor		= "#8AD3F7"
      BackColor		= "white" 
      Wrapping		= "NoWrap" 
      Alignment		= "Left">  
   </Style>
   <Style 
	  Name			= "ServiceInformed"
	  Font-Size		= "Normal" 
	  Font-Name		= "Arial" 
	  Font-Bold		= "true"
      Font-Italic	= "false" 
      ForeColor		= "#F3C28D"
      BackColor		= "white" 
      Wrapping		= "NoWrap" 
      Alignment		= "Left">  
   </Style>
   <Style 
	  Name			= "ServiceConfirmed"
	  Font-Size		= "Normal" 
	  Font-Name		= "Arial" 
	  Font-Bold		= "true"
      Font-Italic	= "false" 
      ForeColor		= "#9BE899" 
      BackColor		= "white" 
      Wrapping		= "NoWrap" 
      Alignment		= "Left">  
   </Style>
   <Style 
	  Name			= "ServiceFinished"
	  Font-Size		= "Normal" 
	  Font-Name		= "Arial" 
	  Font-Bold		= "true"
      Font-Italic	= "false" 
      ForeColor		= "#C0C0C0" 
      BackColor		= "white" 
      Wrapping		= "NoWrap" 
      Alignment		= "Left">  
   </Style>
   <Style 
	  Name			= "TextHeader"
	  Font-Size		= "Normal" 
	  Font-Name		= "Arial" 
	  Font-Bold		= "true"
      Font-Italic	= "false" 
      ForeColor		= "black" 
      BackColor		= "white" 
      Wrapping		= "NoWrap" 
      Alignment		= "Left">  
   </Style>
</mobile:StyleSheet>