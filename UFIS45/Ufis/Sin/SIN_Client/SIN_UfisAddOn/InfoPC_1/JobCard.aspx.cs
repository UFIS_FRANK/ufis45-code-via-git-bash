using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using Microsoft.ApplicationBlocks.UIProcess;
using Ufis.MobileClient.BL.Common;

namespace InfoPC
{
	/// <summary>
	/// Summary description for JobCard.
	/// </summary>
	public partial class JobCard 
	{
        protected System.Web.UI.MobileControls.ObjectList staffinfo;
        protected System.Web.UI.MobileControls.ObjectList flightinfo;
		protected System.Web.UI.MobileControls.ObjectList serviceinfo;
		protected System.Web.UI.MobileControls.ObjectList telexinfo;
        protected System.Web.UI.MobileControls.ObjectList jobinfo;


        private void Page_Load(object sender, System.EventArgs e)
		{
            if (!this.IsPostBack)
			{
				int refresh = (int)ThisController.PageRefresh.TotalSeconds;
				Response.AppendHeader("Refresh",refresh.ToString()); 
            }

			this.PrepareData();
		}

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.servicemask.ItemCommand += new ListCommandEventHandler(HandleCommand);
        }
		#endregion

		public Controller ThisController
		{
			get
			{
				return (Controller)this.Controller;
			}
		}

        private void PrepareData()
        {
            BusinessEntities.JobJCDS job;

            if (ThisController.JobInformationFromShiftJC(out job))
            {
                servicemask.MyJob = job;
            }
        }


        protected void HandleCommand(Object sender, ListCommandEventArgs e)
        {
            
            if (ThisController.SelectFlightJC(e.ListItem.Value.ToString()))
            {
                ThisController.SelectService("Job");
            }
            
        }

        private void staffBtn_Click(object sender, System.EventArgs e)
        {
            ThisController.SelectService("Staff");
        }
        private void exitBtn_Click(object sender, System.EventArgs e)
		{
			ThisController.SelectService("Exit");
		}
        private void backBtn_Click(object sender, System.EventArgs e)
		{
			ThisController.SelectService("Previous");
		}
        private void Form1_Activate(object sender, System.EventArgs e)
		{
		
		}
    }
}
