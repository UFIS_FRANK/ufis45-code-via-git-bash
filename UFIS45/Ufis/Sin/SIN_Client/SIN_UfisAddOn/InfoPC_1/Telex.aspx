<%@ Register TagPrefix="custom" Namespace="MobileCustomControls" Assembly="MobileCustomControls" %>
<%@ Page language="c#" Classname="InfoPC.Telex" CompileWith="Telex.aspx.cs" Inherits="Microsoft.ApplicationBlocks.UIProcess.MobileWebFormView" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>

<HEAD>
	<!--#include file="Stylesheet.aspx" -->
	<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1"/>
	<meta name="CODE_LANGUAGE" content="C#"/>
	 
</HEAD>
<body>
	<mobile:Form id="Form1" runat="server" title="InfoPC mobile Client - Telex" onactivate="Form1_Activate">
		<P>
			<custom:RotationMask id="rotmask" runat="server" info="true"></custom:RotationMask>
		</P>
		<P>
			<mobile:Image id="Image1" runat="server" ImageUrl="images/InformationImage.gif"></mobile:Image>
			<mobile:Label ID="title" Runat="server" StyleReference="TitleLeft">Telex Viewer</mobile:Label>
			<custom:TelexMask id="telmask" runat="server"></custom:TelexMask>
		</P>
		<P>
			<mobile:Command id="backBtn" runat="server" onclick="backBtn_Click">Back</mobile:Command>
			<mobile:Command id="exitBtn" runat="server" onclick="exitBtn_Click">Exit</mobile:Command>
		</P>
	</mobile:Form>
</body>

