<%@ Register TagPrefix="custom" Namespace="MobileCustomControls" Assembly="MobileCustomControls" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Page language="c#" Classname="InfoPC.Arrival" CompileWith="Arrival.aspx.cs" Inherits="Microsoft.ApplicationBlocks.UIProcess.MobileWebFormView" %>

<HEAD>
	<!--#include file="Stylesheet.aspx" -->
	<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1"/>
	<meta name="CODE_LANGUAGE" content="C#"/>
	 
</HEAD>
<body>
	<mobile:Form id="Form1" runat="server" title="InfoPC mobile Client - Arrival" onactivate="Form1_Activate">
		<CUSTOM:ROTATIONMASK id="rotinfo" runat="server" info="true"></CUSTOM:ROTATIONMASK>
		<mobile:Image id="Image1" runat="server" ImageUrl="images/DepartureImage.gif"></mobile:Image>
		<mobile:Command id="nextBtn" runat="server" StyleReference="LinkLeft" Format="Link" onclick="nextBtn_Click">Show Departure Information</mobile:Command>
		<mobile:Image id="Image2" runat="server" ImageUrl="images/ArrivalImage.gif"></mobile:Image>
		<CUSTOM:ROTATIONMASK id="rotmask" runat="server" mask="Arrival"></CUSTOM:ROTATIONMASK>
		<mobile:Command id="backBtn" runat="server" onclick="backBtn_Click">Back</mobile:Command>
		<mobile:Command id="exitBtn" runat="server" onclick="exitBtn_Click">Exit</mobile:Command>
	</mobile:Form>
</body>

