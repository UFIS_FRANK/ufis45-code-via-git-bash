<%@ Page language="c#" Classname="InfoPC.DailyDeploymentByStaffNumber" CompileWith="DailyDeploymentByStaffNumber.aspx.cs" Inherits="Microsoft.ApplicationBlocks.UIProcess.MobileWebFormView" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="custom" Namespace="MobileCustomControls" Assembly="MobileCustomControls" %>
<!--#include file="Stylesheet.aspx" -->

<body Xmlns:mobile="http://schemas.microsoft.com/Mobile/WebForm">
	<mobile:Form id="Form1" runat="server" title="InfoPC mobile Client - Daily deployment by staff number">
	    <mobile:Label id="Label3" runat="server" StyleReference="TitleCenter">Welcome to the InfoPC mobile Client</mobile:Label>
		<mobile:Label id="Label1" runat="server" StyleReference="TextLeft">Employee number :</mobile:Label>
		<mobile:TextBox id="txtPeno" runat="server"></mobile:TextBox>
		<mobile:Label id="lblErrorText" runat="server"></mobile:Label>
		<mobile:RequiredFieldValidator id="rfvPeno1" runat="server" ControlToValidate="txtPeno" Display="Static" ForeColor="Red"
		ErrorMessage="Employee number must be specified"></mobile:RequiredFieldValidator>
	    <mobile:Calendar id="ctlDate" runat="server" StyleReference="TextLeft"></mobile:Calendar>
	    <mobile:Command id="CmdSubmit" runat="server">Submit</mobile:Command>
		<mobile:Command id="CmdExit" runat="server">Exit</mobile:Command>
	</mobile:Form>
</body>
