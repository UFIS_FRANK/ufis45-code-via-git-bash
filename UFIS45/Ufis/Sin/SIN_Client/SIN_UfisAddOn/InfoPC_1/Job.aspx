<%@ Page language="c#" Classname="InfoPC.Job" CompileWith="Job.aspx.cs" Inherits="Microsoft.ApplicationBlocks.UIProcess.MobileWebFormView" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="custom" Namespace="MobileCustomControls" Assembly="MobileCustomControls" %>

	<!--#include file="Stylesheet.aspx" -->
	<body>
		<mobile:form runat="server" ID="Form1" title="InfoPC mobile Client - Job" onactivate="Form1_Activate"> 
		    <mobile:Command id="flightBtn" runat="server" onclick="flightBtn_Click" Format="Link" stylereference="LinkLeft">Show Flight Information</mobile:Command> 
		    <mobile:Command id="remarkBtn" runat="server" onclick="remarkBtn_Click" Format="Link" stylereference="LinkLeft">Show Flight Remarks</mobile:Command> 
		    <mobile:Command id="telexBtn" runat="server" onclick="telexBtn_Click" Format="Link" stylereference="LinkLeft">Show Telex Viewer</mobile:Command> 
		    <custom:JobMask id="jobmask" runat="server"></custom:JobMask> 
		    <custom:RotationMask id="flightmask" Mask="Job" runat="server"></custom:RotationMask> 
		    <mobile:Command id="backBtn" runat="server" onclick="backBtn_Click">Back</mobile:Command> 
		    <mobile:Command id="exitBtn" runat="server" onclick="exitBtn_Click">Exit</mobile:Command></mobile:form>
	</body>
