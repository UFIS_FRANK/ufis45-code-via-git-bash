using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Microsoft.ApplicationBlocks.UIProcess;
using Ufis.MobileClient.BL.Common;



namespace InfoPC
{
	/// <summary>
	/// Summary description for DailyDeploymentByStaffNumber.
	/// </summary>
    public partial class Shift
    {


		private void Page_Load(object sender, System.EventArgs e)
		{
            ProvideData();
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.shiftmask.ItemCommand += new ListCommandEventHandler(HandleCommand);

		}
		#endregion

		private Controller ThisController
		{
			get
			{
				return (Controller)this.Controller;
			}
		}

        private void ProvideData()
        {

                BusinessEntities.ShiftJCDS shift;

                if (ThisController.ShiftInformationJC(out shift)) {
                    this.shiftmask.MyShift = shift;
                }
        }

        protected void HandleCommand(Object sender, ListCommandEventArgs e)
        {

            if (ThisController.SelectShiftJC(e.ListItem.Value.ToString()))
            {
                ThisController.SelectService("Next");
            }

        }

        private void CmdBack_Click(object sender, System.EventArgs e)
        {
            ThisController.SelectService("Previous");
        }

        private void CmdExit_Click(object sender, System.EventArgs e)
		{
			ThisController.SelectService("Exit");		
		}

        private void Form1_Activate(object sender, System.EventArgs e)
        {

        }
    }
}
