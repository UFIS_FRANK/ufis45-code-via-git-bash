<%@ Page language="c#" Classname="InfoPC.Shift" CompileWith="Shift.aspx.cs" Inherits="Microsoft.ApplicationBlocks.UIProcess.MobileWebFormView" %>
<%@ Register TagPrefix="mobile" Namespace="System.Web.UI.MobileControls" Assembly="System.Web.Mobile" %>
<%@ Register TagPrefix="custom" Namespace="MobileCustomControls" Assembly="MobileCustomControls" %>
<!--#include file="Stylesheet.aspx" -->

<body Xmlns:mobile="http://schemas.microsoft.com/Mobile/WebForm">
	<mobile:Form id="Form1" runat="server" title="InfoPC mobile Client - Shifts">
	    <mobile:Label id="Label3" runat="server" StyleReference="TitleLeft">Shifts</mobile:Label>	
	    <custom:ShiftMask id="shiftmask" runat="server"></custom:ShiftMask>
	    <mobile:Command id="CmdBack" runat="server" onclick="CmdBack_Click">Back</mobile:Command>
		<mobile:Command id="CmdExit" runat="server" onclick="CmdExit_Click">Exit</mobile:Command>
	</mobile:Form>
</body>
