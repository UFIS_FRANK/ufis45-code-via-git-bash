using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.Mobile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using Microsoft.ApplicationBlocks.UIProcess;
using Ufis.MobileClient.BL.Common;

namespace InfoPC
{
    /// <summary>
    /// Summary description for JobCard.
    /// </summary>
    public partial class Job
    {

        private void Page_Load(object sender, System.EventArgs e)
        {
            if (!this.IsPostBack)
            {
                int refresh = (int)ThisController.PageRefresh.TotalSeconds;
                Response.AppendHeader("Refresh", refresh.ToString());
            }

            this.PrepareData();
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion

        public Controller ThisController
        {
            get
            {
                return (Controller)this.Controller;
            }
        }

        private void PrepareData()
        {
            BusinessEntities.JobJCDS job;
            BusinessEntities.FlightJCDS flight;
            DateTime date;

            this.jobmask.JobUrno = ThisController.GetSelectedJobJC();

            if (ThisController.JobInformationJC(out job))
            {
                this.jobmask.MyJob = job;
                
                if (ThisController.FlightInformationJC(out flight, out date))
                {
                    this.flightmask.Type = ThisController.GetFlightType();
                    this.flightmask.MyFlight = flight;
                }

            }
        }

        private void flightBtn_Click(object sender, System.EventArgs e)
        {
            if (ThisController.GetFlightType() == "A")
            {
                ThisController.SelectService("Arrival");
            }
            else if (ThisController.GetFlightType() == "D")
            {
                ThisController.SelectService("Departure");
            }
        }
        private void remarkBtn_Click(object sender, System.EventArgs e)
        {
            ThisController.SelectService("Remarks");
        }
        private void telexBtn_Click(object sender, System.EventArgs e)
        {
            ThisController.SelectService("Telex");
        }
        private void exitBtn_Click(object sender, System.EventArgs e)
        {
            ThisController.SelectService("Exit");
        }
        private void backBtn_Click(object sender, System.EventArgs e)
        {
            ThisController.SelectService("Previous");
        }
        private void Form1_Activate(object sender, System.EventArgs e)
        {

        }
    }
}
