VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "Comdlg32.ocx"
Begin VB.Form MainDialog 
   BackColor       =   &H00E1D8D3&
   Caption         =   "Flight List Report"
   ClientHeight    =   8910
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   12540
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   9
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "MainDialog.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8910
   ScaleWidth      =   12540
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin MSComDlg.CommonDialog cdgSave 
      Left            =   11880
      Top             =   3480
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.PictureBox picInput 
      Align           =   3  'Align Left
      BackColor       =   &H00F5EBE6&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   8910
      Left            =   405
      ScaleHeight     =   8910
      ScaleWidth      =   4305
      TabIndex        =   33
      Top             =   0
      Width           =   4305
      Begin VB.OptionButton optReportType 
         BackColor       =   &H00F5EBE6&
         Caption         =   "Daily Flights"
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   0
         Top             =   600
         Value           =   -1  'True
         Width           =   1335
      End
      Begin VB.OptionButton optReportType 
         BackColor       =   &H00F5EBE6&
         Caption         =   "Weekly Flights"
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   13
         Top             =   3720
         Width           =   1575
      End
      Begin VB.Frame fraWeekly 
         BackColor       =   &H00F5EBE6&
         Height          =   2055
         Left            =   120
         TabIndex        =   14
         Top             =   3720
         Width           =   4095
         Begin VB.TextBox txtOrigin 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            DataField       =   "NULL"
            Enabled         =   0   'False
            Height          =   360
            Left            =   1920
            MaxLength       =   3
            TabIndex        =   21
            Top             =   840
            Width           =   570
         End
         Begin VB.CommandButton cmdLookups 
            BackColor       =   &H00E1D8D3&
            Caption         =   "..."
            Enabled         =   0   'False
            Height          =   375
            Index           =   1
            Left            =   2520
            Style           =   1  'Graphical
            TabIndex        =   22
            TabStop         =   0   'False
            Top             =   840
            Width           =   375
         End
         Begin VB.OptionButton optWeeklyTypes 
            BackColor       =   &H00F5EBE6&
            Caption         =   "Arriving from"
            Enabled         =   0   'False
            Height          =   255
            Index           =   1
            Left            =   240
            TabIndex        =   20
            Top             =   840
            Width           =   1575
         End
         Begin VB.OptionButton optWeeklyTypes 
            BackColor       =   &H00F5EBE6&
            Caption         =   "Departing from"
            Enabled         =   0   'False
            Height          =   255
            Index           =   0
            Left            =   240
            TabIndex        =   15
            Top             =   360
            Value           =   -1  'True
            Width           =   1575
         End
         Begin VB.CommandButton cmdLookups 
            BackColor       =   &H00E1D8D3&
            Caption         =   "..."
            Enabled         =   0   'False
            Height          =   375
            Index           =   2
            Left            =   2520
            Style           =   1  'Graphical
            TabIndex        =   27
            TabStop         =   0   'False
            Top             =   1440
            Width           =   375
         End
         Begin VB.CommandButton cmdLookups 
            BackColor       =   &H00E1D8D3&
            Caption         =   "..."
            Enabled         =   0   'False
            Height          =   375
            Index           =   0
            Left            =   3600
            Style           =   1  'Graphical
            TabIndex        =   19
            TabStop         =   0   'False
            Top             =   360
            Width           =   375
         End
         Begin VB.TextBox txtAirline 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            DataField       =   "NULL"
            Enabled         =   0   'False
            Height          =   360
            Left            =   1920
            MaxLength       =   2
            TabIndex        =   26
            Top             =   1440
            Width           =   570
         End
         Begin VB.TextBox txtDestination 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            DataField       =   "NULL"
            Enabled         =   0   'False
            Height          =   360
            Left            =   3000
            MaxLength       =   3
            TabIndex        =   18
            Top             =   360
            Width           =   570
         End
         Begin VB.Label lblLabels 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "to"
            Height          =   255
            Index           =   3
            Left            =   3000
            TabIndex        =   23
            Top             =   840
            Width           =   375
         End
         Begin VB.Label lblDes 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H8000000C&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "SIN"
            ForeColor       =   &H80000008&
            Height          =   360
            Left            =   3405
            TabIndex        =   24
            Top             =   840
            Width           =   570
         End
         Begin VB.Label lblOrg 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            BackColor       =   &H8000000C&
            BorderStyle     =   1  'Fixed Single
            Caption         =   "SIN"
            ForeColor       =   &H80000008&
            Height          =   360
            Left            =   1920
            TabIndex        =   16
            Top             =   360
            Width           =   570
         End
         Begin VB.Label lblLabels 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Airline (2LC)"
            Height          =   255
            Index           =   5
            Left            =   0
            TabIndex        =   25
            Top             =   1440
            Width           =   1575
         End
         Begin VB.Label lblLabels 
            Alignment       =   2  'Center
            BackStyle       =   0  'Transparent
            Caption         =   "to"
            Height          =   255
            Index           =   4
            Left            =   2520
            TabIndex        =   17
            Top             =   360
            Width           =   375
         End
      End
      Begin VB.Frame fraDaily 
         BackColor       =   &H00F5EBE6&
         Height          =   2895
         Left            =   120
         TabIndex        =   1
         Top             =   600
         Width           =   4095
         Begin VB.CheckBox chkADID 
            Appearance      =   0  'Flat
            BackColor       =   &H00F5EBE6&
            Caption         =   "Departure"
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   1
            Left            =   1800
            TabIndex        =   12
            Top             =   2400
            Width           =   1215
         End
         Begin VB.CheckBox chkADID 
            Appearance      =   0  'Flat
            BackColor       =   &H00F5EBE6&
            Caption         =   "Arrival"
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   0
            Left            =   1800
            TabIndex        =   11
            Top             =   2160
            Width           =   855
         End
         Begin VB.CheckBox chkAirlines 
            Appearance      =   0  'Flat
            BackColor       =   &H00F5EBE6&
            Caption         =   "Other Airlines"
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   1
            Left            =   1800
            TabIndex        =   10
            Top             =   1680
            Width           =   1455
         End
         Begin VB.CheckBox chkAirlines 
            Appearance      =   0  'Flat
            BackColor       =   &H00F5EBE6&
            Caption         =   "SQ/MI"
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   0
            Left            =   1800
            TabIndex        =   9
            Top             =   1440
            Width           =   855
         End
         Begin VB.TextBox txtDate 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            DataField       =   "NULL"
            Height          =   360
            Index           =   1
            Left            =   1800
            MaxLength       =   10
            TabIndex        =   6
            Top             =   840
            Width           =   1285
         End
         Begin VB.TextBox txtDate 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            DataField       =   "NULL"
            Height          =   360
            Index           =   0
            Left            =   1800
            MaxLength       =   10
            TabIndex        =   3
            Top             =   360
            Width           =   1285
         End
         Begin VB.TextBox txtTime 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            DataField       =   "NULL"
            Height          =   360
            Index           =   1
            Left            =   3120
            MaxLength       =   5
            TabIndex        =   7
            Top             =   840
            Width           =   735
         End
         Begin VB.TextBox txtTime 
            Alignment       =   2  'Center
            Appearance      =   0  'Flat
            DataField       =   "NULL"
            Height          =   360
            Index           =   0
            Left            =   3120
            MaxLength       =   5
            TabIndex        =   4
            Top             =   360
            Width           =   735
         End
         Begin VB.Label lblLabels 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Report period from"
            Height          =   255
            Index           =   0
            Left            =   120
            TabIndex        =   2
            Top             =   360
            Width           =   1575
         End
         Begin VB.Label lblLabels 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "to"
            Height          =   255
            Index           =   1
            Left            =   1320
            TabIndex        =   5
            Top             =   840
            Width           =   375
         End
         Begin VB.Label lblLabels 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Airlines"
            Height          =   255
            Index           =   2
            Left            =   120
            TabIndex        =   8
            Top             =   1440
            Width           =   1575
         End
      End
      Begin VB.PictureBox picCaption 
         BackColor       =   &H00EAD7C6&
         BorderStyle     =   0  'None
         Height          =   350
         Left            =   40
         ScaleHeight     =   345
         ScaleWidth      =   4230
         TabIndex        =   34
         Top             =   40
         Width           =   4225
         Begin VB.Image imgCaption 
            Height          =   240
            Left            =   25
            Picture         =   "MainDialog.frx":038A
            Top             =   35
            Width           =   240
         End
         Begin VB.Image imgMinimized 
            Height          =   195
            Left            =   4000
            Picture         =   "MainDialog.frx":0714
            ToolTipText     =   "Minimize"
            Top             =   60
            Width           =   195
         End
         Begin VB.Label lblCaption 
            AutoSize        =   -1  'True
            BackStyle       =   0  'Transparent
            Caption         =   "Report Parameters"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   225
            Left            =   360
            TabIndex        =   35
            Top             =   25
            Width           =   1635
         End
         Begin VB.Image imgMaximized 
            Height          =   195
            Left            =   4000
            Picture         =   "MainDialog.frx":0AEB
            ToolTipText     =   "Maximized"
            Top             =   60
            Visible         =   0   'False
            Width           =   195
         End
      End
      Begin VB.PictureBox picReportButtons 
         BackColor       =   &H00E1D8D3&
         BorderStyle     =   0  'None
         Height          =   425
         Left            =   40
         ScaleHeight     =   420
         ScaleWidth      =   4230
         TabIndex        =   36
         Top             =   6600
         Width           =   4225
         Begin VB.CommandButton cmdClose 
            BackColor       =   &H00F5EBE6&
            Cancel          =   -1  'True
            Caption         =   "Close"
            Height          =   355
            Left            =   2520
            Style           =   1  'Graphical
            TabIndex        =   30
            Top             =   25
            Width           =   1215
         End
         Begin VB.CommandButton cmdOK 
            BackColor       =   &H00F5EBE6&
            Caption         =   "OK"
            Default         =   -1  'True
            Height          =   355
            Left            =   600
            Style           =   1  'Graphical
            TabIndex        =   29
            Top             =   25
            Width           =   1215
         End
      End
      Begin VB.CheckBox chkNewTab 
         Appearance      =   0  'Flat
         BackColor       =   &H00F5EBE6&
         Caption         =   "Open report in a new tab"
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   120
         TabIndex        =   28
         Top             =   6000
         Width           =   2415
      End
      Begin VB.Line linRight 
         BorderColor     =   &H00808080&
         BorderWidth     =   2
         X1              =   4290
         X2              =   4290
         Y1              =   0
         Y2              =   6600
      End
      Begin VB.Line linTop 
         BorderColor     =   &H00808080&
         BorderWidth     =   3
         X1              =   0
         X2              =   4320
         Y1              =   0
         Y2              =   0
      End
      Begin VB.Line linLeft 
         BorderColor     =   &H00FFFFFF&
         BorderWidth     =   3
         X1              =   0
         X2              =   0
         Y1              =   0
         Y2              =   6600
      End
      Begin VB.Line linBottom 
         BorderColor     =   &H00FFFFFF&
         BorderWidth     =   3
         X1              =   4320
         X2              =   0
         Y1              =   8160
         Y2              =   8160
      End
   End
   Begin VB.PictureBox picMain 
      BackColor       =   &H00F0CAA6&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   9270
      Left            =   4680
      ScaleHeight     =   9270
      ScaleWidth      =   7095
      TabIndex        =   37
      Top             =   0
      Width           =   7095
      Begin VB.PictureBox picReports 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         Height          =   6255
         Index           =   0
         Left            =   0
         ScaleHeight     =   6255
         ScaleWidth      =   7095
         TabIndex        =   38
         Top             =   840
         Visible         =   0   'False
         Width           =   7095
      End
      Begin VB.PictureBox picTabContainer 
         BackColor       =   &H00E1D8D3&
         BorderStyle     =   0  'None
         Height          =   400
         Left            =   0
         ScaleHeight     =   405
         ScaleWidth      =   7095
         TabIndex        =   39
         Top             =   0
         Visible         =   0   'False
         Width           =   7095
         Begin VB.PictureBox picArrows 
            BackColor       =   &H00E1D8D3&
            BorderStyle     =   0  'None
            Height          =   375
            Left            =   6480
            ScaleHeight     =   375
            ScaleWidth      =   615
            TabIndex        =   40
            Top             =   0
            Width           =   615
            Begin VB.Image imgArrows 
               Height          =   150
               Index           =   2
               Left            =   390
               Picture         =   "MainDialog.frx":0EB8
               Top             =   100
               Width           =   150
            End
            Begin VB.Image imgArrows 
               Height          =   150
               Index           =   1
               Left            =   220
               Picture         =   "MainDialog.frx":13A2
               Top             =   100
               Visible         =   0   'False
               Width           =   150
            End
            Begin VB.Image imgArrows 
               Height          =   150
               Index           =   0
               Left            =   40
               Picture         =   "MainDialog.frx":188C
               Top             =   100
               Visible         =   0   'False
               Width           =   150
            End
         End
         Begin VB.PictureBox picTabs 
            BackColor       =   &H00E1D8D3&
            BorderStyle     =   0  'None
            Height          =   375
            Left            =   0
            ScaleHeight     =   375
            ScaleWidth      =   2415
            TabIndex        =   41
            Top             =   0
            Width           =   2415
            Begin VB.PictureBox picTabItems 
               BackColor       =   &H00F5EBE6&
               Height          =   375
               Index           =   0
               Left            =   0
               ScaleHeight     =   315
               ScaleWidth      =   1755
               TabIndex        =   42
               Top             =   0
               Width           =   1815
               Begin VB.Image imgTabCloseMarks 
                  Height          =   120
                  Index           =   0
                  Left            =   1560
                  Picture         =   "MainDialog.frx":1D76
                  ToolTipText     =   "Close"
                  Top             =   30
                  Width           =   120
               End
               Begin VB.Label lblTabItems 
                  AutoSize        =   -1  'True
                  BackStyle       =   0  'Transparent
                  Caption         =   "FV Report (1)"
                  Height          =   225
                  Index           =   0
                  Left            =   360
                  TabIndex        =   43
                  Top             =   30
                  Width           =   1080
               End
               Begin VB.Image imgTabIcons 
                  Height          =   240
                  Index           =   0
                  Left            =   25
                  Picture         =   "MainDialog.frx":2220
                  Top             =   35
                  Width           =   240
               End
            End
         End
      End
      Begin VB.PictureBox picReportToolbar 
         BorderStyle     =   0  'None
         Height          =   400
         Left            =   0
         ScaleHeight     =   405
         ScaleWidth      =   7095
         TabIndex        =   44
         Top             =   360
         Visible         =   0   'False
         Width           =   7095
         Begin VB.PictureBox picToolbars 
            BackColor       =   &H80000016&
            BorderStyle     =   0  'None
            Height          =   375
            Index           =   1
            Left            =   1720
            ScaleHeight     =   375
            ScaleMode       =   0  'User
            ScaleWidth      =   1583.678
            TabIndex        =   47
            Top             =   0
            Width           =   1575
            Begin VB.Label lblToolbars 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Export to PDF"
               Height          =   225
               Index           =   1
               Left            =   360
               TabIndex        =   48
               Top             =   30
               Width           =   1110
            End
            Begin VB.Image imgToolbars 
               Height          =   240
               Index           =   1
               Left            =   25
               Picture         =   "MainDialog.frx":25AA
               Top             =   35
               Width           =   240
            End
         End
         Begin VB.PictureBox picToolbars 
            BackColor       =   &H80000016&
            BorderStyle     =   0  'None
            Height          =   375
            Index           =   0
            Left            =   0
            ScaleHeight     =   375
            ScaleWidth      =   1695
            TabIndex        =   45
            Top             =   0
            Width           =   1695
            Begin VB.Image imgToolbars 
               Height          =   240
               Index           =   0
               Left            =   25
               Picture         =   "MainDialog.frx":2934
               Top             =   35
               Width           =   240
            End
            Begin VB.Label lblToolbars 
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Export to Excel"
               Height          =   225
               Index           =   0
               Left            =   360
               TabIndex        =   46
               Top             =   30
               Width           =   1185
            End
         End
      End
   End
   Begin VB.PictureBox picButtons 
      Align           =   3  'Align Left
      BackColor       =   &H00E1D8D3&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   8910
      Left            =   0
      ScaleHeight     =   8910
      ScaleWidth      =   405
      TabIndex        =   31
      Top             =   0
      Visible         =   0   'False
      Width           =   400
      Begin VB.PictureBox picParams 
         BackColor       =   &H00F5EBE6&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2025
         Left            =   25
         ScaleHeight     =   2025
         ScaleWidth      =   345
         TabIndex        =   32
         Top             =   25
         Width           =   350
         Begin VB.Image imgParams 
            Height          =   240
            Left            =   35
            Picture         =   "MainDialog.frx":2CBE
            Top             =   25
            Width           =   240
         End
      End
   End
   Begin VB.Menu mnuPopup 
      Caption         =   "PopupMenu"
      Visible         =   0   'False
      Begin VB.Menu mnuItems 
         Caption         =   "Item 0"
         Index           =   0
      End
   End
End
Attribute VB_Name = "MainDialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const WINDOW_TITLE = "Flight List Report"

Private Const MIN_WINDOW_WIDTH = 7500
Private Const MIN_WINDOW_HEIGHT = 4000

Private Const SPACE_BETWEEN_TABS = 20

Private Declare Function SelectObject Lib "gdi32" (ByVal hDC As Long, ByVal hObject As Long) As Long
Private Declare Function DeleteObject Lib "gdi32" (ByVal hObject As Long) As Long

Private PDFExport As ActiveReportsPDFExport.ARExportPDF
Attribute PDFExport.VB_VarHelpID = -1
Private ExcelExport As ActiveReportsExcelExport.ARExportExcel
Attribute ExcelExport.VB_VarHelpID = -1

Private m_Font As Long
Private m_IsActivated As Boolean
Private m_SelectedTab As Integer
Private m_FLTReports As New Collection

Private blnOpenFileAfterExporting  As Boolean

Private Sub LoadBasicData()
    Dim blnShowSplashScreen As Boolean
    
    blnShowSplashScreen = (GetConfigEntry(colConfigs, "MAIN", "SHOW_SPLASH_SCREEN", "NO") = "YES")
    pstrBasicData = GetConfigEntry(colConfigs, "MAIN", "BASIC_DATA", "NONE")
    
    If pstrBasicData <> "NONE" Then
        BasicData.ShowSplashScreen = blnShowSplashScreen
        BasicData.DataTables = pstrBasicData
        Call ChangeFormBorderStyle(BasicData, vbBSNone)
        BasicData.Show vbModal
        Unload BasicData
    End If
End Sub

Private Function AddNewTab(ByVal TabCaption As String, Optional ByVal ToolTipText As String = "", _
Optional ByVal Selected As Boolean = True) As Integer
    Dim NewIndex As Integer
    Dim pic As PictureBox
    
    If Not picTabContainer.Visible Then
        NewIndex = picTabItems.UBound
        
        Call ChangeTabCaption(NewIndex, TabCaption)
        imgTabCloseMarks(NewIndex).Left = lblTabItems(NewIndex).Left + lblTabItems(NewIndex).Width + 120
        picTabItems(NewIndex).Width = imgTabCloseMarks(NewIndex).Left + 255
        picTabItems(NewIndex).Visible = True
        
        picTabContainer.Visible = True
        picReportToolbar.Visible = True
    Else
        NewIndex = picTabItems.UBound + 1
        
        Load picTabItems(NewIndex)
        Set picTabItems(NewIndex).Container = picTabs
        
        Load imgTabIcons(NewIndex)
        Set imgTabIcons(NewIndex).Container = picTabItems(NewIndex)
        imgTabIcons(NewIndex).Left = imgTabIcons(NewIndex - 1).Left
        imgTabIcons(NewIndex).Visible = True
        
        Load lblTabItems(NewIndex)
        Set lblTabItems(NewIndex).Container = picTabItems(NewIndex)
        Call ChangeTabCaption(NewIndex, TabCaption)
        lblTabItems(NewIndex).Left = lblTabItems(NewIndex - 1).Left
        lblTabItems(NewIndex).Visible = True
        
        Load imgTabCloseMarks(NewIndex)
        Set imgTabCloseMarks(NewIndex).Container = picTabItems(NewIndex)
        imgTabCloseMarks(NewIndex).Left = lblTabItems(NewIndex).Left + lblTabItems(NewIndex).Width + 120
        imgTabCloseMarks(NewIndex).Visible = True
        
        picTabItems(NewIndex).Left = picTabItems(NewIndex - 1).Left + picTabItems(NewIndex - 1).Width + SPACE_BETWEEN_TABS
        picTabItems(NewIndex).Width = imgTabCloseMarks(NewIndex).Left + 255
        picTabItems(NewIndex).Visible = True
        
        Load picReports(NewIndex)
        
        Load mnuItems(NewIndex)
    End If
    
    picReports(NewIndex).ZOrder
    
    Call SetTabToolTipText(NewIndex, ToolTipText)
    
    picTabs.Width = picTabItems(NewIndex).Left + picTabItems(NewIndex).Width
    
    mnuItems(NewIndex).Caption = lblTabItems(NewIndex).Tag
    mnuItems(NewIndex).Visible = True
        
    If Selected Then
        Call SelectTabItem(NewIndex)
        m_SelectedTab = NewIndex
    End If
    
    Call ArrangeTabs
    
    AddNewTab = NewIndex
End Function

Private Sub ArrangeTabs(Optional ByVal MakeSelectedTabVisible As Boolean = True)
    Dim sngWidth As Single
    Dim sngDiff As Single
    
    sngWidth = picTabContainer.Width - picArrows.Width

    If picTabs.Width <= sngWidth Then
        picTabs.Left = 0
    Else
        If MakeSelectedTabVisible Then
            Call EnsureSelectedTabVisible(sngWidth)
        End If
        
        sngDiff = sngWidth - (picTabs.Left + picTabs.Width)
        If sngDiff > 0 Then
            picTabs.Left = picTabs.Left + sngDiff
        End If
    End If
    
    Call SetArrowVisibility(sngWidth)
End Sub

Private Function BuildToolTipText(colParams As Collection) As String
    Dim dtmBeginDate As Date
    Dim dtmEndDate As Date
    Dim strText As String
    
    dtmBeginDate = colParams.Item("BEGINDATE")
    dtmEndDate = colParams.Item("ENDDATE")
    
    strText = Format(dtmBeginDate, "dd-mm-yyyy hh:mm") & " / "
    If Format(dtmBeginDate, "ddmmyyyy") = Format(dtmEndDate, "ddmmyyyy") Then
        strText = strText & Format(dtmEndDate, "hh:mm")
    Else
        strText = strText & Format(dtmEndDate, "dd-mm-yyyy hh:mm")
    End If
    
    If optReportType(0).Value Then 'Daily
        strText = "Daily, " & strText
        Select Case colParams.Item("ALC2")
            Case 0
                strText = strText & ", SQ/MI"
            Case 1
                strText = strText & ", Other Airlines"
            Case Else
                strText = strText & ", All Airlines"
        End Select
        Select Case colParams.Item("ADID")
            Case 0
                strText = strText & ", ARR"
            Case 1
                strText = strText & ", DEP"
            Case Else
                strText = strText & ", ARR/DEP"
        End Select
    Else
        strText = "Weekly, " & strText & ", Des: " & colParams.Item("APC3")
        If Trim(colParams.Item("ALC2")) = "" Then
                strText = strText & ", All Airlines"
        Else
            strText = strText & ", Airline: " & colParams.Item("ALC2")
        End If
    End If
    
    BuildToolTipText = strText
End Function

Private Function BuildWhereClause(colParams As Collection) As String
    Dim strWhere As String
    Dim strBeginDate As String
    Dim strEndDate As String

    strBeginDate = Format(LocalDateToUTC(colParams.Item("BEGINDATE"), UtcTimeDiff), "YYYYMMDDhhmmss")
    strEndDate = Format(LocalDateToUTC(colParams.Item("ENDDATE"), UtcTimeDiff), "YYYYMMDDhhmmss")
        
    If optReportType(0).Value Then 'Daily
        Select Case colParams.Item("ADID")
            Case 0 'Arrival
                strWhere = "((TIFA BETWEEN '" & strBeginDate & "' AND '" & strEndDate & "') OR " & _
                    "(STOA BETWEEN '" & strBeginDate & "' AND '" & strEndDate & "')) AND ADID = 'A'"
            Case 1 'Departure
                strWhere = "((TIFD BETWEEN '" & strBeginDate & "' AND '" & strEndDate & "') OR " & _
                    "(STOD BETWEEN '" & strBeginDate & "' AND '" & strEndDate & "')) AND ADID <> 'A'"
            Case 2 'All
                strWhere = "((((TIFA BETWEEN '" & strBeginDate & "' AND '" & strEndDate & "') OR " & _
                    "(STOA BETWEEN '" & strBeginDate & "' AND '" & strEndDate & "')) AND ADID = 'A') OR " & _
                    "(((TIFD BETWEEN '" & strBeginDate & "' AND '" & strEndDate & "') OR " & _
                    "(STOD BETWEEN '" & strBeginDate & "' AND '" & strEndDate & "')) AND ADID <> 'A'))"
        End Select
        Select Case colParams.Item("ALC2")
            Case 0  'SQ/MI
                strWhere = strWhere & " AND ALC2 IN ('SQ', 'MI')"
            Case 1  'Other Airlines
                strWhere = strWhere & " AND ALC2 NOT IN ('SQ', 'MI')"
            Case 2 'All
                'Do nothing
        End Select
    Else 'Weekly
        If optWeeklyTypes(0).Value Then 'Departure
            strWhere = "((TIFD BETWEEN '" & strBeginDate & "' AND '" & strEndDate & "') OR " & _
                "(STOD BETWEEN '" & strBeginDate & "' AND '" & strEndDate & "')) AND " & _
                "ADID <> 'A' AND (DES3 = '" & colParams.Item("APC3") & "' OR " & _
                "VIAL LIKE '%" & colParams.Item("APC3") & colParams.Item("APC4") & "%')"
        Else 'Arrival
            strWhere = "((TIFA BETWEEN '" & strBeginDate & "' AND '" & strEndDate & "') OR " & _
                "(STOA BETWEEN '" & strBeginDate & "' AND '" & strEndDate & "')) AND " & _
                "ADID = 'A' AND (ORG3 = '" & colParams.Item("APC3") & "' OR " & _
                "VIAL LIKE '%" & colParams.Item("APC3") & colParams.Item("APC4") & "%')"
        End If
        If Trim(colParams.Item("ALC2")) <> "" Then
            strWhere = strWhere & " AND ALC2 = '" & colParams.Item("ALC2") & "'"
        End If
    End If
    
    BuildWhereClause = strWhere
End Function

Private Sub ChangeTabCaption(ByVal Index As Integer, ByVal TabCaption As String, _
Optional ByVal IsNewTab As Boolean = True)
    Dim sngLabelWidth As Single
    Dim sngCaptionWidth As Single
    
    lblTabItems(Index).Tag = TabCaption
    If Not IsNewTab Then
        sngLabelWidth = picTabItems(Index).Width - 255 - lblTabItems(Index).Left - 120
        sngCaptionWidth = Me.TextWidth(TabCaption)
        
        If sngLabelWidth < sngCaptionWidth Then
            TabCaption = TrimTextAddDots(TabCaption, sngLabelWidth)
        End If
    End If
    
    lblTabItems(Index).Caption = TabCaption
End Sub

Private Sub ChangeWindowTitle()
    Me.Caption = WINDOW_TITLE
    If m_SelectedTab > -1 Then
        Me.Caption = Me.Caption & " - " & lblTabItems(m_SelectedTab).Tag
        
        If picTabItems(m_SelectedTab).ToolTipText <> "" Then
            Me.Caption = Me.Caption & " [" & picTabItems(m_SelectedTab).ToolTipText & "]"
        End If
    End If
End Sub

Private Sub DrawBorder(picObject As PictureBox, ByVal LevelledUp As Boolean)
    If LevelledUp Then
        linTop.BorderColor = vbWhite
        linLeft.BorderColor = vbWhite
        linBottom.BorderColor = &H808080
        linRight.BorderColor = &H808080
    Else
        linTop.BorderColor = Me.BackColor
        linLeft.BorderColor = Me.BackColor
        linBottom.BorderColor = Me.BackColor
        linRight.BorderColor = Me.BackColor
    End If
End Sub

Private Sub EnsureSelectedTabVisible(ByVal SpaceWidth As Single)
    If picTabItems(m_SelectedTab).Left + picTabs.Left < 0 Then 'selected tab is hidden at the left side
        picTabs.Left = -picTabItems(m_SelectedTab).Left 'show selected tab as the most left item
    Else
        If picTabItems(m_SelectedTab).Left + picTabItems(m_SelectedTab).Width > _
        SpaceWidth - picTabs.Left Then  'selected tab is hidden at the right side
            picTabs.Left = -(picTabItems(m_SelectedTab).Left + picTabItems(m_SelectedTab).Width - _
                SpaceWidth)  'show selected tab as the most right item
        End If
    End If
End Sub

Private Sub ExportReport(ByVal Index As Integer)
    Dim strFileName As String
    Dim rptFLT As FlightListReport
    Dim strDefPath As String
    Dim strDefFile As String
    Dim Media As String
    
    If m_SelectedTab > -1 Then
        Media = Choose(Index + 1, "excel", "pdf")
    
        strDefPath = GetConfigEntry(colConfigs, "MAIN", "EXPORT_" & UCase(Media) & "_PATH", App.Path)
        If Dir(strDefPath, vbDirectory) = "" Then
            If Not CreateFolder(strDefPath) Then
                MsgBox "Folder " & strDefPath & " could not be created!", vbCritical, Me.Caption
                Exit Sub
            End If
        End If
        
        strDefFile = lblTabItems(m_SelectedTab).Tag
        strFileName = GetFileName(Me.cdgSave, strDefPath, strDefFile, Media)
        If strFileName <> "" Then
            If Dir(strFileName) <> "" Then
                On Error Resume Next
                Err.Number = 0
                Kill strFileName
                If Err.Number <> 0 Then
                    MsgBox "Could not overwrite file " & strFileName & vbNewLine & Err.Description, vbCritical, Me.Caption
                    Err.Number = 0
                    Exit Sub
                End If
                On Error GoTo 0
            End If
            
            Set rptFLT = m_FLTReports.Item("Rpt" & m_SelectedTab)
            
            Select Case Index
                Case 0 'Excel
                    Set ExcelExport = New ActiveReportsExcelExport.ARExportExcel
                    ExcelExport.FileName = strFileName
                    ExcelExport.Export rptFLT.Pages

                    Set ExcelExport = Nothing
                Case 1 'PDF
                    Set PDFExport = New ActiveReportsPDFExport.ARExportPDF
                    PDFExport.FileName = strFileName
                    PDFExport.Export rptFLT.Pages

                    Set PDFExport = Nothing
            End Select
            
            If blnOpenFileAfterExporting Then
                OpenFileUsingDefaultApp Me, strFileName
            End If
        End If
    End If
End Sub

Private Function GetMostLeftTabItem() As Integer
    Dim pic As PictureBox
    Dim Index As Integer
    
    Index = -1
    For Each pic In picTabItems
        If picTabs.Left + pic.Left + pic.Width + SPACE_BETWEEN_TABS >= 0 Then
            Index = pic.Index
            Exit For
        End If
    Next pic
    
    GetMostLeftTabItem = Index
End Function

Private Function GetMostRightTabItem(ByVal SpaceWidth As Single) As Integer
    Dim pic As PictureBox
    Dim Index As Integer
    
    Index = -1
    For Each pic In picTabItems
        If picTabs.Left + pic.Left + pic.Width > SpaceWidth Then
            Index = pic.Index
            Exit For
        End If
    Next pic
    
    GetMostRightTabItem = Index
End Function

Private Function GetCheckValue(oCheckBoxes As Variant) As Integer
    Dim chk As CheckBox
    Dim intResult As Integer
    
    intResult = 0
    For Each chk In oCheckBoxes
        intResult = intResult + IIf(chk.Value, 1, 0) * (chk.Index + 1)
    Next chk
    GetCheckValue = intResult - 1
End Function

Private Function GetReportParams() As Collection
    Dim colReportParams As Collection
        
    Dim i As Integer
    Dim dtmDate(1) As Date
    Dim intAirlines As Integer
    Dim intADID As Integer
    Dim blnValidUserDates As Boolean
    Dim strMessage As String
    Dim txt As TextBox
    Dim strFlightType As String
    
    If optReportType(0).Value Then 'Daily
        blnValidUserDates = True
        'Check the timeframe
        For i = 0 To 1
            Select Case txtDate(i).DataField
                Case "ERROR"
                    MsgBox "Invalid date!", vbInformation, "Flight List"
                    txtDate(i).SetFocus
                    blnValidUserDates = False
                    Exit Function
                Case "NULL"
                    dtmDate(i) = CDate(0)
                    blnValidUserDates = False
                Case Else
                    dtmDate(i) = DateSerial(CInt(Right(txtDate(i).DataField, 4)), _
                                            CInt(Mid(txtDate(i).DataField, 4, 2)), _
                                            CInt(Left(txtDate(i).DataField, 2)))
            End Select
            
            Select Case txtTime(i).DataField
                Case "ERROR"
                    MsgBox "Invalid time!", vbInformation, "Flight List"
                    txtTime(i).SetFocus
                    Exit Function
                Case "NULL"
                    If i = 1 Then
                        dtmDate(i) = dtmDate(i) + _
                                     TimeSerial(23, 59, 0)
                    End If
                Case Else
                    dtmDate(i) = dtmDate(i) + _
                                 TimeSerial(CInt(Left(txtTime(i).DataField, 2)), _
                                            CInt(Right(txtTime(i).DataField, 2)), 0)
            End Select
        Next i
        
        If blnValidUserDates Then
            If dtmDate(0) > dtmDate(1) Then
                strMessage = "The begin for the timeframe is after the end of the timeframe!" & vbNewLine & _
                    "Begin : " & Format(dtmDate(0), "dd-mm-yyyy hh:mm") & " > " & _
                    "End : " & Format(dtmDate(1), "dd-mm-yyyy hh:mm")
                    
                MsgBox strMessage, vbInformation, "Flight List"
                txtDate(0).SetFocus
                Exit Function
            End If
            
            If dtmDate(0) < DateAdd("d", -3, Date) Or dtmDate(1) >= DateAdd("d", 3, Date) Then
                strMessage = "You can only select the flights within following timeframe!" & vbNewLine & _
                    "Begin : " & Format(DateAdd("d", -3, Date), "dd-mm-yyyy hh:mm") & vbNewLine & _
                    "End : " & Format(DateAdd("n", -1, DateAdd("d", 3, Date)), "dd-mm-yyyy hh:mm")
                    
                MsgBox strMessage, vbInformation, "Flight List"
                txtDate(0).SetFocus
                Exit Function
            End If
        End If
        
        intAirlines = GetCheckValue(chkAirlines)
        If intAirlines = -1 Then
            MsgBox "Please select the airlines!", vbInformation, "Flight List"
            chkAirlines(0).SetFocus
            Exit Function
        End If
        
        intADID = GetCheckValue(chkADID)
        If intADID = -1 Then
            MsgBox "Please select departure or/and arrival flights!", vbInformation, "Flight List"
            chkADID(0).SetFocus
            Exit Function
        End If
        
        Set colReportParams = New Collection
        colReportParams.Add dtmDate(0), "BEGINDATE"
        colReportParams.Add dtmDate(1), "ENDDATE"
        colReportParams.Add intAirlines, "ALC2"
        colReportParams.Add intADID, "ADID"
    Else 'Weekly
        If optWeeklyTypes(0).Value Then 'Departure
            Set txt = txtDestination
            strFlightType = "destination"
        Else
            Set txt = txtOrigin
            strFlightType = "origin"
        End If
        
        If Trim(txt.Text) = "" Then
            MsgBox "Please specify flight " & strFlightType & "!", vbInformation, "Flight List"
            txt.SetFocus
            Exit Function
        End If
            
        rsAirports.Find "APC3 = '" & txt.Text & "'", , , 1
        If rsAirports.EOF Then
            MsgBox "Flight " & strFlightType & " not found in basic data!", vbInformation, "Flight List"
            txt.SetFocus
            Exit Function
        End If
                
        If Trim(txtAirline.Text) <> "" Then
            rsAirlines.Find "ALC2 = '" & txtAirline.Text & "'", , , 1
            If rsAirlines.EOF Then
                MsgBox "Airline not found in basic data!", vbInformation, "Flight List"
                txtAirline.SetFocus
                Exit Function
            End If
        End If
        
        dtmDate(0) = Now
        dtmDate(1) = DateAdd("n", -1, DateAdd("d", 7, dtmDate(0)))
        
        Set colReportParams = New Collection
        colReportParams.Add dtmDate(0), "BEGINDATE"
        colReportParams.Add dtmDate(1), "ENDDATE"
        colReportParams.Add txt.Text, "APC3"
        colReportParams.Add rsAirports.Fields("APC4").Value, "APC4"
        colReportParams.Add txtAirline.Text, "ALC2"
    End If
    
    Set GetReportParams = colReportParams
End Function

Private Function PrepareReportDataSource(colParams As Collection) As Boolean
    Dim blnOK As Boolean
    Dim colLocalParams As CollectionExtended
    Dim strWhere As String
    
    blnOK = False
    If InStr(pstrDataSource & ",", "AFTTAB,") > 0 Then
        strWhere = BuildWhereClause(colParams)
                
        Set colLocalParams = New CollectionExtended
        colLocalParams.Add strWhere, "WhereClause"
        
        pstrAftUrnoList = AddDataSource(colDataSources, "AFTTAB", colLocalParams, "URNO")
        
        Set rsAft = colDataSources.Item("AFTTAB")
        If Not (rsAft Is Nothing) Then
            If Not (rsAft.BOF And rsAft.EOF) Then
                Call ReadDataSource(pstrAftUrnoList)
            End If
        End If
        
        If colDataSources.Exists("DCFTAB") Then
            Set rsDelay = colDataSources.Item("DCFTAB")
        End If
        
        Call CreateReportDataSource
        
        blnOK = True
    End If
    PrepareReportDataSource = blnOK
End Function

Private Sub CreateReportDataSource()
    Dim fld As ADODB.Field
    
    Set rsReport = New ADODB.Recordset
    For Each fld In rsAft.Fields
        Select Case fld.Name
            Case "STOA"
                rsReport.Fields.Append "STDT", fld.Type, fld.DefinedSize, adFldIsNullable
            Case "STOD"
                'do nothing
            Case Else
                rsReport.Fields.Append fld.Name, fld.Type, fld.DefinedSize, adFldIsNullable
        End Select
    Next fld
    rsReport.Open
    rsReport.Sort = "ALC2,ADID,STDT"
    
    Do While Not rsAft.EOF
        rsReport.AddNew
        For Each fld In rsAft.Fields
            Select Case fld.Name
                Case "ADID"
                    If rsAft.Fields("ADID").Value = "A" Then
                        rsReport.Fields(fld.Name).Value = "A"
                    Else
                        rsReport.Fields(fld.Name).Value = "D"
                    End If
                Case "STOA"
                    If rsAft.Fields("ADID").Value = "A" Then
                        rsReport.Fields("STDT").Value = fld.Value
                    End If
                Case "STOD"
                    If rsAft.Fields("ADID").Value <> "A" Then
                        rsReport.Fields("STDT").Value = fld.Value
                    End If
                Case "ALC2"
                    Select Case fld.Value
                        Case "SQ", "MI"
                            rsReport.Fields(fld.Name).Value = 0
                        Case Else
                            rsReport.Fields(fld.Name).Value = 1
                    End Select
                Case Else
                    rsReport.Fields(fld.Name).Value = fld.Value
            End Select
        Next fld
        rsReport.Update
        
        rsAft.MoveNext
    Loop
    
    If Not (rsReport.BOF And rsReport.EOF) Then
        rsReport.MoveFirst
    End If
End Sub

Private Sub ReadDataSource(ByVal FlightUrno As String)
    Dim intUrnoCountInWhereClause As Integer
    Dim vntFlightUrno As Variant
    Dim intUrnoCount As Integer
    Dim intArrayCount As Integer
    Dim i As Integer, j As Integer
    Dim strUrno As String
    Dim intStart As Integer, intEnd As Integer
    Dim strWhere As String
    Dim strDataSource As String
    Dim vntDataSources As Variant
    Dim vntDataSourceItem As Variant
    
    intUrnoCountInWhereClause = Val(GetConfigEntry(colConfigs, "MAIN", "URNO_COUNT_IN_WHERE_CLAUSE", "300"))
    
    vntFlightUrno = Split(FlightUrno, ",")
    intUrnoCount = UBound(vntFlightUrno) + 1
    If intUrnoCount > intUrnoCountInWhereClause Then
        FlightUrno = ""
        intArrayCount = Int(intUrnoCount / intUrnoCountInWhereClause) + 1
        For i = 1 To intArrayCount
            intStart = intUrnoCountInWhereClause * (i - 1)
            intEnd = intStart + (intUrnoCountInWhereClause - 1)
            If intEnd > intUrnoCount - 1 Then
                intEnd = intUrnoCount - 1
            End If
        
            strUrno = ""
            For j = intStart To intEnd
                strUrno = strUrno & "," & vntFlightUrno(j)
            Next j
            strUrno = Mid(strUrno, 2)
            
            FlightUrno = FlightUrno & "|" & strUrno
        Next i
        If FlightUrno <> "" Then FlightUrno = Mid(FlightUrno, 2)
    End If
    
    If colParameters.Exists("AftUrnoList") Then
        colParameters.Remove "AftUrnoList"
    End If
    colParameters.Add FlightUrno, "AftUrnoList"
    
    vntDataSources = Split(pstrDataSource, ",")
    For Each vntDataSourceItem In vntDataSources
        If vntDataSourceItem <> "AFTTAB" Then
            Call AddDataSource(colDataSources, vntDataSourceItem, colParameters, , True, True, "|")
        End If
    Next vntDataSourceItem
End Sub

Private Function RemoveTabItem(ByVal Index As Integer) As Integer
    Dim pic As PictureBox
    Dim IsSelected As Boolean
    Dim sngLeft As Single
    Dim PreviousIndex As Integer
    Dim NextIndex As Integer
    
    PreviousIndex = -1
    NextIndex = -1

    IsSelected = (picTabItems(Index).BorderStyle = 1)
    sngLeft = picTabItems(Index).Left
    
    Unload m_FLTReports.Item("Rpt" & CStr(Index))
    m_FLTReports.Remove "Rpt" & CStr(Index)
    
    If Index = 0 Then
        picTabItems(Index).Visible = False
        picReports(Index).Visible = False
    Else
        Unload imgTabIcons(Index)
        Unload lblTabItems(Index)
        Unload imgTabCloseMarks(Index)
        Unload picTabItems(Index)
        Unload picReports(Index)
        Unload mnuItems(Index)
    End If
    
    For Each pic In picTabItems
        If pic.Index > Index Then
            If NextIndex = -1 Then NextIndex = pic.Index
            
            pic.Left = sngLeft
            
            sngLeft = sngLeft + pic.Width + 10
        ElseIf pic.Index < Index Then
            If pic.Visible Then PreviousIndex = pic.Index
        End If
    Next pic
    
    picTabs.Width = picTabItems(picTabItems.UBound).Left + picTabItems(picTabItems.UBound).Width
    
    If (picTabItems.UBound = 0) And (Not picTabItems(0).Visible) Then
        picTabContainer.Visible = False
        m_SelectedTab = -1
        Call ArrangeTabs(False)
    Else
        If IsSelected Then
            m_SelectedTab = -1
            
            If NextIndex <> -1 Then
                m_SelectedTab = NextIndex
            ElseIf PreviousIndex <> -1 Then
                m_SelectedTab = PreviousIndex
            End If
            
            If m_SelectedTab > -1 Then
                Call SelectTabItem(m_SelectedTab)
            Else
                Call ArrangeTabs(False)
            End If
        Else
            Call ArrangeTabs(False)
        End If
    End If
    
    If m_SelectedTab = -1 Then
        picReportToolbar.Visible = False
    End If
    
    RemoveTabItem = m_SelectedTab
End Function

Private Sub ScrollTab(ByVal ScrollDirection As Integer)
    Dim sngWidth As Single
    Dim Index As Integer
    Dim sngLeft As Single
    
    sngWidth = picTabContainer.Width - picArrows.Width

    If ScrollDirection = 0 Then 'Left
        sngLeft = picTabs.Left + sngWidth
        If sngLeft > 0 Then
            sngLeft = 0
        Else
            Index = GetMostLeftTabItem()
            If Index > -1 Then
                sngLeft = -(picTabItems(Index).Left + picTabItems(Index).Width - _
                    sngWidth)
            End If
        End If
    Else 'Right
        sngLeft = picTabs.Left - sngWidth
        If sngLeft < sngWidth - picTabs.Width Then
            sngLeft = sngWidth - picTabs.Width
        Else
            Index = GetMostRightTabItem(sngWidth)
            If Index > -1 Then
                sngLeft = -picTabItems(Index).Left
            End If
        End If
    End If
    picTabs.Left = sngLeft
    
    Call SetArrowVisibility(sngWidth)
End Sub

Private Sub SelectTabItem(ByVal Index As Integer)
    Dim pic As PictureBox
    
    For Each pic In picTabItems
        If pic.Index = Index Then
            pic.BorderStyle = 1
            picReports(Index).Visible = True
        Else
            pic.BorderStyle = 0
            picReports(pic.Index).Visible = False
        End If
    Next pic
    
    If m_SelectedTab > -1 Then
        mnuItems(m_SelectedTab).Checked = False
    End If
    
    m_SelectedTab = Index
    
    mnuItems(m_SelectedTab).Checked = True
    
    Call ChangeWindowTitle
    
    Call ArrangeTabs
End Sub

Private Sub SetArrowVisibility(ByVal SpaceWidth As Single)
    imgArrows(0).Visible = (picTabs.Left < 0)
    imgArrows(1).Visible = (picTabs.Left + picTabs.Width > SpaceWidth)
End Sub

Private Sub SetTabToolTipText(ByVal Index As Integer, ByVal ToolTipText As String)
    imgTabIcons(Index).ToolTipText = ToolTipText
    lblTabItems(Index).ToolTipText = ToolTipText
    picTabItems(Index).ToolTipText = ToolTipText
End Sub

Private Function TrimTextAddDots(ByVal Caption As String, ByVal Width As Single) As String
    Dim strText As String
    Dim i As Integer
    
    For i = 1 To Len(Caption)
        strText = Left(Caption, i) & "..."
        
        If Me.TextWidth(strText) > Width Then
            strText = Left(Caption, i - 1) & "..."
        
            Exit For
        End If
    Next i
    
    TrimTextAddDots = strText
End Function

Private Sub cmdClose_Click()
    Unload Me
End Sub

Private Sub cmdLookups_Click(Index As Integer)
    Dim strSelectedData As String
    Dim txt As TextBox

    Select Case Index
        Case 0, 1 'Airport
            If Index = 0 Then 'Departure
                Set txt = txtDestination
            Else 'Arrival
                Set txt = txtOrigin
            End If
        
            Set LookupForm.DataSource = rsAirports
            LookupForm.Columns = "APC3,APC4,APFN"
            LookupForm.ColumnHeaders = "APC3,APC4,Description"
            LookupForm.ColumnWidths = "800,1000,6000"
            LookupForm.KeyColumns = "APC3"
            LookupForm.Caption = "Airports"
            LookupForm.AllowMultiSelection = False
            LookupForm.IsOnTop = False
            LookupForm.Show vbModal, Me
        
            strSelectedData = LookupForm.SelectedData
            If strSelectedData <> "" Then
                txt.Text = RTrim(strSelectedData)
            End If
            txt.SetFocus
        Case 2 'Airline
            Set LookupForm.DataSource = rsAirlines
            LookupForm.Columns = "ALC2,ALC3,ALFN"
            LookupForm.ColumnHeaders = "ALC2,ALC3,Description"
            LookupForm.ColumnWidths = "800,800,6000"
            LookupForm.KeyColumns = "ALC2"
            LookupForm.Caption = "Airlines"
            LookupForm.AllowMultiSelection = False
            LookupForm.IsOnTop = False
            LookupForm.Show vbModal, Me
        
            strSelectedData = LookupForm.SelectedData
            If strSelectedData <> "" Then
                txtAirline.Text = RTrim(strSelectedData)
            End If
            txtAirline.SetFocus
    End Select
End Sub

Private Sub cmdOK_Click()
    Dim NewIndex As Integer
    Dim ToolTipText As String
    Dim TabCaption As String
    Dim dtmPrintDate As Date
    Dim colReportParams As Collection
    Dim rptFLT As FlightListReport
    
    Set colReportParams = GetReportParams()
    
    If Not (colReportParams Is Nothing) Then
        Call PrepareReportDataSource(colReportParams)
        
        If (rsReport.BOF And rsReport.EOF) Then
            MsgBox "No flights found within the criteria specified!", vbInformation, "Flight List"
        Else
            If optReportType(0).Value Then
                dtmPrintDate = Now
                TabCaption = "Daily_" & Format(dtmPrintDate, "YYYYMMDDhhmmss")
            Else
                dtmPrintDate = colReportParams.Item("BEGINDATE")
                TabCaption = "Weekly_" & Format(dtmPrintDate, "YYYYMMDDhhmmss")
            End If
        
            ToolTipText = BuildToolTipText(colReportParams)
            If (Not picTabContainer.Visible) Or (chkNewTab.Value = vbChecked) Then
                NewIndex = AddNewTab(TabCaption, ToolTipText, True)
            Else
                NewIndex = m_SelectedTab
                
                Call ChangeTabCaption(NewIndex, TabCaption, False)
                Call SetTabToolTipText(NewIndex, ToolTipText)
                Call ChangeWindowTitle
                
                mnuItems(NewIndex).Caption = TabCaption
    
                Unload m_FLTReports.Item("Rpt" & CStr(NewIndex))
                m_FLTReports.Remove "Rpt" & CStr(NewIndex)
            End If
            
            Set rptFLT = New FlightListReport
            rptFLT.DESC.Caption = ToolTipText
            rptFLT.fldRptTime.Text = dtmPrintDate
            FormWithinForm picReports(NewIndex), rptFLT
            rptFLT.Move 0, 0, picReports(NewIndex).Width, picReports(NewIndex).Height
            rptFLT.Show
            
            m_FLTReports.Add rptFLT, "Rpt" & CStr(NewIndex)
        End If
    End If
End Sub

Private Sub Form_Activate()
    If Not m_IsActivated Then
        m_IsActivated = True
        
        txtDate(0).SetFocus
    End If
End Sub

Private Sub Form_Load()
    m_SelectedTab = -1
    
    m_Font = SelectObject(picParams.hDC, CreateCustomFont("Arial", 9, 270))
    
    DrawBorder picInput, False
    
    Set colConfigs = ReadConfigurations(myIniFullName)
    pblnNewSubdelayCodes = (GetConfigEntry(colConfigs, "MAIN", "NEW_MULTI_SUBDELAY_CODES", "NO") = "YES")
    pstrDataSource = GetConfigEntry(colConfigs, "MAIN", "DATA_SOURCE", "AFTTAB")
    blnOpenFileAfterExporting = (GetConfigEntry(colConfigs, "DEICING", "OPEN_FILE_AFTER_EXPORTING", "YES") = "YES")
    
    Call LoadBasicData
    If colDataSources.Exists("DENTAB") Then
        Set rsDelayDesc = colDataSources.Item("DENTAB")
    End If
    If colDataSources.Exists("APTTAB") Then
        Set rsAirports = colDataSources.Item("APTTAB")
    End If
    If colDataSources.Exists("ALTTAB") Then
        Set rsAirlines = colDataSources.Item("ALTTAB")
    End If
        
    txtDate(0).Text = Format(Date, "DD-MM-YYYY")
    txtDate(1).Text = txtDate(0).Text
    txtTime(0).Text = "00:00"
    txtTime(1).Text = "23:59"
    
    m_IsActivated = False
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Dim pic As PictureBox
    
    If MsgBox("Exit program?", vbQuestion + vbYesNo + vbDefaultButton2, "Flight List") = vbNo Then
        Cancel = True
        Exit Sub
    End If
    
    For Each pic In picTabItems
        If pic.Visible Then
            Unload m_FLTReports.Item("Rpt" & CStr(pic.Index))
        End If
    Next pic
End Sub

Private Sub Form_Resize()
    Dim sngLeft As Single
    Dim sngTop As Single
    Dim sngWidth As Single
    Dim sngHeight As Single
    
    On Error Resume Next

    If Me.WindowState <> vbMinimized Then
        If Me.Width < MIN_WINDOW_WIDTH Then Me.Width = MIN_WINDOW_WIDTH
        If Me.Height < MIN_WINDOW_HEIGHT Then Me.Height = MIN_WINDOW_HEIGHT
    
        sngLeft = 0
        If picButtons.Visible Then
            sngLeft = sngLeft + picButtons.Width
        End If
        If picInput.Visible And picInput.Align = 3 Then
            sngLeft = sngLeft + picInput.Width
        End If
        
        sngTop = 25
        
        sngWidth = Me.ScaleWidth - sngLeft - 25
        If sngWidth < 0 Then sngWidth = 0
        
        sngHeight = Me.ScaleHeight - 50
        If sngHeight < 0 Then sngHeight = 0
    
        picMain.Move sngLeft, 25, sngWidth, sngHeight
        
        If picInput.Align = 0 Then
            sngHeight = picMain.Height - 400
            If sngHeight < 0 Then sngHeight = 400
            
            picInput.Height = sngHeight
        End If
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set PDFExport = Nothing
    Set ExcelExport = Nothing
    
    DeleteObject SelectObject(picParams.hDC, m_Font)
    
    Set rsAft = Nothing
    Set rsDelay = Nothing
    Set oUfisServer = Nothing
End Sub

Private Sub imgArrows_Click(Index As Integer)
    Select Case Index
        Case 0
            Call ScrollTab(0)
        Case 1
            Call ScrollTab(1)
        Case 2
            PopupMenu mnuPopup, , , picTabContainer.Height
    End Select
End Sub

Private Sub imgMaximized_Click()
    imgMinimized.Visible = True
    imgMaximized.Visible = False
    
    picInput.Align = 3
    picInput.Visible = True
    picButtons.Visible = False
    
    DrawBorder picInput, False
    
    picParams.BorderStyle = 0
    
    Call Form_Resize
End Sub

Private Sub imgMinimized_Click()
    imgMaximized.Visible = True
    imgMinimized.Visible = False
    
    picInput.Visible = False
    picButtons.Visible = True
    
    Call Form_Resize
End Sub

Private Sub imgParams_Click()
    Call picParams_Click
End Sub

Private Sub imgTabCloseMarks_Click(Index As Integer)
    Call RemoveTabItem(Index)
End Sub

Private Sub imgTabIcons_Click(Index As Integer)
    Call picTabItems_Click(Index)
End Sub

Private Sub imgToolbars_Click(Index As Integer)
    Call picToolbars_Click(Index)
End Sub

Private Sub lblTabItems_Click(Index As Integer)
    Call picTabItems_Click(Index)
End Sub

Private Sub lblToolbars_Click(Index As Integer)
    Call picToolbars_Click(Index)
End Sub

Private Sub mnuItems_Click(Index As Integer)
    Call picTabItems_Click(Index)
End Sub

Private Sub optReportType_Click(Index As Integer)
    Dim blnDailyFlights As Boolean
    
    blnDailyFlights = optReportType(0).Value
    
    txtDate(0).Enabled = blnDailyFlights
    txtDate(1).Enabled = blnDailyFlights
    txtTime(0).Enabled = blnDailyFlights
    txtTime(1).Enabled = blnDailyFlights
    chkAirlines(0).Enabled = blnDailyFlights
    chkAirlines(1).Enabled = blnDailyFlights
    chkADID(0).Enabled = blnDailyFlights
    chkADID(1).Enabled = blnDailyFlights
    
    optWeeklyTypes(0).Enabled = Not blnDailyFlights
    optWeeklyTypes(1).Enabled = Not blnDailyFlights
    txtDestination.Enabled = (Not blnDailyFlights) And (optWeeklyTypes(0).Value)
    txtOrigin.Enabled = (Not blnDailyFlights) And (optWeeklyTypes(1).Value)
    cmdLookups(0).Enabled = txtDestination.Enabled
    cmdLookups(1).Enabled = txtOrigin.Enabled
    txtAirline.Enabled = Not blnDailyFlights
    cmdLookups(2).Enabled = Not blnDailyFlights
    
    If blnDailyFlights Then
        txtDate(0).SetFocus
    Else
        If optWeeklyTypes(0).Value Then
            txtDestination.SetFocus
        Else
            txtOrigin.SetFocus
        End If
    End If
End Sub

Private Sub optWeeklyTypes_Click(Index As Integer)
    Dim blnDeparture As Boolean

    blnDeparture = optWeeklyTypes(0).Value
    
    txtDestination.Enabled = blnDeparture
    cmdLookups(0).Enabled = blnDeparture
    txtOrigin.Enabled = Not blnDeparture
    cmdLookups(1).Enabled = Not blnDeparture
End Sub

Private Sub picInput_Resize()
    linLeft.Y2 = picInput.Height
    linRight.Y2 = picInput.Height
    linBottom.Y1 = picInput.Height - 20
    linBottom.Y2 = linBottom.Y1
End Sub

Private Sub picMain_Resize()
    Dim pic As PictureBox
    Dim sngHeight As Single
    
    picTabContainer.Move 0, 0, picMain.Width, picTabContainer.Height
    picReportToolbar.Move 0, picTabContainer.Height, picMain.Width, picReportToolbar.Height
    
    sngHeight = picMain.Height - (picTabContainer.Height + picReportToolbar.Height)
    If sngHeight < 0 Then sngHeight = 0
    For Each pic In Me.picReports
        pic.Move 0, picTabContainer.Height + picReportToolbar.Height, picMain.Width, sngHeight
    Next pic
End Sub

Private Sub picParams_Click()
    If picParams.BorderStyle = 0 Then
        picParams.BorderStyle = 1
        
        picInput.Align = 0
        picInput.Move picButtons.Width + 50, 75, picInput.Width, picMain.Height - 400
        
        picInput.Visible = True
        
        DrawBorder picInput, True
    Else
        picParams.BorderStyle = 0
        
        picInput.Visible = False
        
        DrawBorder picInput, False
    End If
End Sub

Private Sub picParams_Paint()
    picParams.CurrentX = 250
    picParams.CurrentY = 350
    picParams.Print "Report Parameters"
End Sub

Private Sub picReports_Resize(Index As Integer)
    Dim rptFLT As FlightListReport

    If picReports(Index).Visible Then
        Set rptFLT = m_FLTReports.Item("Rpt" & CStr(Index))
        rptFLT.Move 0, 0, picReports(Index).Width, picReports(Index).Height
    End If
End Sub

Private Sub picTabContainer_Resize()
    Dim sngLeft As Single

    sngLeft = picTabContainer.Width - picArrows.Width
    If sngLeft < 0 Then sngLeft = 0
    picArrows.Left = sngLeft
    
    Call ArrangeTabs(False)
End Sub

Private Sub picTabItems_Click(Index As Integer)
    If picTabItems(Index).BorderStyle = 0 Then
        Call SelectTabItem(Index)
    End If
End Sub

Private Sub picToolbars_Click(Index As Integer)
    Call ExportReport(Index)
End Sub

Private Sub txtAirline_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtDate_Change(Index As Integer)
    Dim strDate As String
    
    strDate = CheckValidDateExt(txtDate(Index).Text)
    If strDate = "ERROR" Then
        txtDate(Index).BackColor = &HC0C0FF
    Else
        txtDate(Index).BackColor = vbWindowBackground
    End If
    
    txtDate(Index).DataField = strDate
End Sub

Private Sub txtDate_KeyPress(Index As Integer, KeyAscii As Integer)
    Const VALID_CHARS = "0123456789-./ "
    
    Select Case KeyAscii
        Case vbKeyBack
        Case Else
            If InStr(VALID_CHARS, Chr(KeyAscii)) <= 0 Then
                KeyAscii = 0
            End If
    End Select
End Sub

Private Sub txtDate_Validate(Index As Integer, Cancel As Boolean)
    If txtDate(Index).DataField <> "ERROR" And txtDate(Index).DataField <> "NULL" Then
        txtDate(Index).Text = txtDate(Index).DataField
    End If
End Sub

Private Sub txtDestination_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtOrigin_KeyPress(KeyAscii As Integer)
    KeyAscii = Asc(UCase(Chr(KeyAscii)))
End Sub

Private Sub txtTime_Change(Index As Integer)
    Dim strTime As String
    
    strTime = CheckValidTimeExt(txtTime(Index).Text)
    If strTime = "ERROR" Then
        txtTime(Index).BackColor = &HC0C0FF
    Else
        txtTime(Index).BackColor = vbWindowBackground
    End If
    
    txtTime(Index).DataField = strTime
End Sub

Private Sub txtTime_KeyPress(Index As Integer, KeyAscii As Integer)
    Const VALID_CHARS = "0123456789:. "
    
    Select Case KeyAscii
        Case vbKeyBack
        Case Else
            If InStr(VALID_CHARS, Chr(KeyAscii)) <= 0 Then
                KeyAscii = 0
            End If
    End Select
End Sub

Private Sub txtTime_Validate(Index As Integer, Cancel As Boolean)
    If txtTime(Index).DataField <> "ERROR" And txtTime(Index).DataField <> "NULL" Then
        txtTime(Index).Text = txtTime(Index).DataField
    End If
End Sub
