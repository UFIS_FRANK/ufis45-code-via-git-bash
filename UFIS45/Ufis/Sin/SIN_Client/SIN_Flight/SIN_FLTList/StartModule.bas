Attribute VB_Name = "StartModule"
Option Explicit

Public LoginUserName As String

Sub Main()
    If Not DetermineIniFile Then
        
        MsgBox "Sorry. Couldn't find the INI file!" & vbNewLine & myIniFullName
        
    Else
    
        Set oUfisServer = New UfisServer
        oUfisServer.StayConnected = True
        oUfisServer.TwsCode = ".."
        oUfisServer.ConnectToCeda
            
        LoginUserName = DoLogin()
        If LoginUserName <> "" Then
            oUfisServer.UserName = LoginUserName
            
            Call GetUTCLocalTimeDiff(myIniFullName)
            
            Set MyMainForm = MainDialog
            MyMainForm.Show
        End If
    End If
End Sub

Private Function DetermineIniFile() As Boolean
    Dim tmpStr As String
    Dim IniFound As Boolean
    
    If DEFAULT_CEDA_INI = "" Then GetUfisDir
    myIniPath = UFIS_SYSTEM
    
    myIniFile = App.EXEName & ".ini"
    myIniFullName = myIniPath & "\" & myIniFile
        
    tmpStr = Dir(myIniFullName)
    IniFound = (UCase(tmpStr) = UCase(myIniFile))
    
    DetermineIniFile = IniFound
End Function

Private Function DoLogin() As String
    Dim oLogin As UFISLogin
    
    Dim AppFullVersion As String
    Dim AppMajorVersion As String
    Dim strRegister As String
    Dim strRetLogin As String
    
    AppMajorVersion = App.Major & "." & App.Minor
    AppFullVersion = AppMajorVersion & ".0." & App.Revision
    
    ' build the register string for BDPS-SEC
    strRegister = App.EXEName & "," & _
        "InitModu,InitModu,Initialize (InitModu),B,-"
    
    Set oLogin = New UFISLogin
    With oLogin
        .ApplicationName = App.EXEName
        .VersionString = AppFullVersion
        .InfoCaption = "Info about this application"
        .InfoButtonVisible = True
        .InfoUfisVersion = "UFIS Version " & AppMajorVersion
        .InfoAppVersion = App.Title & " " & AppFullVersion
        .InfoCopyright = "� 2001-2005 UFIS Airport Solutions GmbH"
        .InfoAAT = "UFIS Airport Solutions GmbH"
        .UserNameLCase = True 'automatic upper case letters
        .LoginAttempts = 3
        .RegisterApplicationString = strRegister
        strRetLogin = .ShowLoginDialog
    End With
    Set oLogin = Nothing
    
    DoLogin = strRetLogin
End Function

Public Sub HandleBroadCast(ReqId As String, DestName As String, RecvName As String, CedaCmd As String, ObjName As String, Seq As String, Tws As String, Twe As String, CedaSqlKey As String, Fields As String, Data As String, BcNum As String)
    If CedaCmd = "CLO" Then
        MsgBox "The server had switched." & vbCrLf & "You can restart the application in one minute."
        End
    End If
End Sub
