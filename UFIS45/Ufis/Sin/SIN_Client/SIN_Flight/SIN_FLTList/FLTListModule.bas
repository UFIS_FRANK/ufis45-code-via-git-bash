Attribute VB_Name = "FLTListModule"
Option Explicit

Private Const LOGPIXELSY = 90

Private Declare Function SetParent Lib "user32" _
    (ByVal hWndChild As Long, ByVal hWndNewParent As Long) As Long
    
Private Declare Function GetDC Lib "user32" (ByVal hwnd As Long) As Long
Private Declare Function ReleaseDC Lib "user32" (ByVal hwnd As Long, ByVal hDC As Long) As Long
Private Declare Function GetDeviceCaps Lib "gdi32" (ByVal hDC As Long, ByVal nIndex As Long) As Long
Private Declare Function MulDiv Lib "kernel32" (ByVal nNumber As Long, ByVal nNumerator As Long, ByVal nDenominator As Long) As Long
Private Declare Function CreateFont Lib "gdi32" Alias "CreateFontA" (ByVal nHeight As Long, ByVal nWidth As Long, ByVal nEscapement As Long, ByVal nOrientation As Long, ByVal fnWeight As Long, ByVal fdwItalic As Boolean, ByVal fdwUnderline As Boolean, ByVal fdwStrikeOut As Boolean, ByVal fdwCharSet As Long, ByVal fdwOutputPrecision As Long, ByVal fdwClipPrecision As Long, ByVal fdwQuality As Long, ByVal fdwPitchAndFamily As Long, ByVal lpszFace As String) As Long
    
Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" _
    (ByVal hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, _
    ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long

Private Const SW_SHOWNORMAL = 1

'Public Variables for Configurations
Public colConfigs As CollectionExtended
Public pblnNewSubdelayCodes As Boolean

'Public Variables for Data Sources
Public pstrBasicData As String
Public pstrDataSource As String
Public colDataSources As New CollectionExtended

'Public Variables for Parameters
Public colParameters As New CollectionExtended

'Public Variable for UFISServer
Public oUfisServer As UfisServer

'Public Variable for Data
Public rsReport As ADODB.Recordset
Public rsAft As ADODB.Recordset
Public rsDelay As ADODB.Recordset
Public rsDelayDesc As ADODB.Recordset
Public rsAirlines As ADODB.Recordset
Public rsAirports As ADODB.Recordset

Public pstrAftUrnoList As String

Public Function CreateCustomFont(ByVal Font As String, ByVal Size As Long, ByVal Degrees As Integer) As Long
    Dim nDC As Long, nHeight As Long

    nDC = GetDC(0&)
    nHeight = -MulDiv(Size, GetDeviceCaps(nDC, LOGPIXELSY), 72)
    ReleaseDC 0&, nDC

    CreateCustomFont = CreateFont(nHeight, 0&, 10& * Degrees, 0&, 0&, False, False, False, 0&, 0&, 0&, 0&, 0&, Font)
End Function

Public Sub ChangeFormBorderStyle(FormToChange As Form, ByVal BorderStyle As FormBorderStyleConstants)
    FormToChange.BorderStyle = BorderStyle
    FormToChange.Caption = FormToChange.Caption
End Sub

Public Function CheckValidDateExt(ByVal sDate As String, Optional ByVal AllowNull As Boolean = True) As String
    Const VALID_DATE_SEP = "-./ "
    
    Dim nLoc As Integer
    Dim i As Integer
    Dim sSep As String
    Dim vDate As Variant
    Dim sDay As String
    Dim sMonth As String
    Dim sYear As String
    Dim dtmDate As Date
    Dim nDateLength As Integer
        
    CheckValidDateExt = "ERROR"
    
    If Trim(sDate) = "" Then
        If AllowNull Then CheckValidDateExt = "NULL"
        
        Exit Function
    End If
    
    On Error GoTo ErrHandle
        
    'check for separator
    nLoc = 0
    For i = 1 To Len(VALID_DATE_SEP)
        sSep = Mid(VALID_DATE_SEP, i, 1)
        nLoc = InStr(sDate, sSep)
        If nLoc > 0 Then Exit For
    Next i
    
    If nLoc > 0 Then 'Has separator
        vDate = Split(sDate, sSep)
        If UBound(vDate) <> 2 Then
            Exit Function
        End If
        
        sDay = vDate(0)
        sMonth = vDate(1)
        sYear = vDate(2)
    Else 'No separator
        nDateLength = Len(sDate)
        If nDateLength <> 6 And nDateLength <> 8 Then
            Exit Function
        End If
        
        sDay = Left(sDate, 2)
        sMonth = Mid(sDate, 3, 2)
        sYear = Right(sDate, nDateLength - 4)
    End If
    If Len(sYear) = 2 Then
        sYear = Left(CStr(Year(Date)), 2) & sYear
    End If
        
    sDate = Format(sYear, "0000") & Format(sMonth, "00") & Format(sDay, "00")
    
    If Not CheckValidDate(sDate) Then
        Exit Function
    End If
    
    'Get the date
    dtmDate = DateSerial(CInt(sYear), CInt(sMonth), CInt(sDay))
        
    CheckValidDateExt = Format(dtmDate, "dd-mm-yyyy")
    
    Exit Function
    
ErrHandle:
End Function

Public Function CheckValidTimeExt(ByVal sTime As String, Optional ByVal AllowNull As Boolean = True) As String
    Dim nLoc As Integer
    Dim sTimePart As String
    Dim sAddPart As String
    Dim sSign As String
    Dim sHour As String
    Dim sMinute As String
    Dim sSep As String
    Dim blnAdd As Boolean
    Dim i As Integer
    Dim nAdjust As Integer
        
    CheckValidTimeExt = "ERROR"
    
    If Trim(sTime) = "" Then
        If AllowNull Then CheckValidTimeExt = "NULL"
        
        Exit Function
    End If
    
    'look for plus or minus sign
    blnAdd = False
    sSign = "+"
    nLoc = InStr(sTime, sSign)
    If nLoc > 0 Then
        blnAdd = True
    Else
        sSign = "-"
        nLoc = InStr(sTime, sSign)
        If nLoc > 0 Then
            blnAdd = True
        End If
    End If
    
    If blnAdd Then
        sTimePart = Trim(Left(sTime, nLoc - 1))
        sAddPart = Trim(Mid(sTime, nLoc + 1))
    Else
        sTimePart = Trim(sTime)
    End If
    
    'check for the additional part
    If blnAdd Then
        If sAddPart = "" Then
            Exit Function
        Else
            If CStr(Abs(CInt(Val(sAddPart)))) <> sAddPart Then
                Exit Function
            End If
        End If
    End If
    
    'check for time part
    'this is valid 20:10 20.10 20 10 2010
    nAdjust = 0
    sSep = ":"
    nLoc = InStr(sTimePart, sSep)
    If nLoc = 0 Then
        sSep = "."
        nLoc = InStr(sTimePart, sSep)
        If nLoc = 0 Then
            sSep = " "
            nLoc = InStr(sTimePart, sSep)
            If nLoc = 0 Then
                nLoc = 3
                nAdjust = 1
            End If
        End If
    End If
    
    sHour = Trim(Left(sTimePart, nLoc - 1))
    sMinute = Trim(Mid(sTimePart, nLoc + 1 - nAdjust))
    
    'cek for hour
    If sHour = "" Then
        Exit Function
    Else
        If CStr(Abs(CInt(Val(sHour)))) <> sHour Then
            If Format(CStr(Abs(CInt(Val(sHour)))), "00") <> sHour Then
                Exit Function
            End If
        Else
            If CInt(sHour) > 23 Then
                Exit Function
            End If
        End If
    End If
    
    'cek for minute
    If sMinute = "" Then
        Exit Function
    Else
        If CStr(Abs(CInt(Val(sMinute)))) <> sMinute Then
            If Format(CStr(Abs(CInt(Val(sMinute)))), "00") <> sMinute Then
                Exit Function
            End If
        Else
            If CInt(sMinute) > 59 Then
                Exit Function
            End If
        End If
    End If
    
    CheckValidTimeExt = Format(sHour, "00") & ":" & Format(sMinute, "00")
    If blnAdd Then
        CheckValidTimeExt = CheckValidTimeExt & ";" & sSign & sAddPart
    End If
End Function

Public Function FormWithinForm(Parent As Object, Child As Object)
    On Error Resume Next
    
    SetParent Child.hwnd, Parent.hwnd
    FormWithinForm = (Err.Number = 0 And Err.LastDllError = 0)
End Function

Public Function GetUTCLocalTimeDiff(Optional ByVal IniFile As String = "") As Integer
    Dim tmpTag As String
    
    oUfisServer.SetUtcTimeDiff
    If IniFile <> "" Then
        tmpTag = GetIniEntry(IniFile, "MAIN", "", "UTC_TIME_DIFF", CStr(UtcTimeDiff))
        UtcTimeDiff = Val(tmpTag)
    End If
    GetUTCLocalTimeDiff = UtcTimeDiff
End Function

Public Function GetViaListString(ByVal ViaList As String, Optional ByVal ViaNumber As Integer = 8) As String
    Const SEP_CHAR = "/"
    Dim i As Integer
    Dim strRet As String
    Dim strVia As String
    
    strRet = ""
    For i = 1 To ViaNumber
        strVia = Mid(ViaList, 120 * i - 119, 120)
        strVia = Mid(strVia, 2, 3)
        If strVia = "" Then Exit For
        
        strRet = strRet & SEP_CHAR & strVia
    Next i
    If strRet <> "" Then strRet = Mid(strRet, 2)
    GetViaListString = strRet
End Function

Public Function LocalDateToUTC(ByVal LocalDate As Date, ByVal TimeDiff As Integer) As Date
    LocalDateToUTC = DateAdd("H", -(TimeDiff / 60), LocalDate)
End Function

Public Function Repeat(ByVal Number As Integer, ByVal Text As String)
    Dim Result As String
    Dim i As Integer

    Result = ""
    For i = 1 To Number
        Result = Result & Text
    Next

    Repeat = Result
End Function

Public Function ReadConfigurations(ByVal ConfigFileName As String) As CollectionExtended
    Dim intFile As Integer
    Dim strLine As String
    Dim strSection As String
    Dim strKey As String
    Dim strDefinition As String
    Dim intPos As Integer
    Dim ilCount As Integer
    
    On Error GoTo HandleError
    
    Set ReadConfigurations = New CollectionExtended
    
    intFile = FreeFile
    Open ConfigFileName For Input As #intFile
    Do While (Not EOF(intFile))
        Line Input #intFile, strLine
        strLine = Trim(strLine)
        If Left(strLine, 1) = "*" Or Left(strLine, 1) = "#" Then 'Comment
            'Ignore
        Else
            If Left(strLine, 1) = "[" And Right(strLine, 1) = "]" Then 'New Section
                strSection = Mid(strLine, 2, Len(strLine) - 2)
            Else 'Entry
                intPos = InStr(strLine, "=")
                If intPos > 0 Then
                    strKey = strSection & Chr(0) & RTrim(Left(strLine, intPos - 1))
                    strDefinition = EvaluateParameters(colParameters, LTrim(Mid(strLine, intPos + 1)))
                    
                    If Not ReadConfigurations.Exists(strKey) Then
                        ReadConfigurations.Add strDefinition, strKey
                    End If
                End If
            End If
        End If
    Loop
    Close #intFile
    
    Exit Function
    
HandleError:
End Function

Public Function FullDateTimeToShortTime(ByVal BaseDate As Date, ByVal FullDate As Date) As String
    Dim strAdd As String
    Dim nDayAdd As Integer
    Dim dtmTime As Date
    
    If FullDate <> CDate(0) Then
        nDayAdd = DateDiff("d", BaseDate, FullDate)
        If nDayAdd = 0 Then
            strAdd = ""
        Else
            strAdd = CStr(nDayAdd)
            If nDayAdd > 0 Then
                strAdd = "+" & strAdd
            End If
        End If
        dtmTime = TimeSerial(Hour(FullDate), Minute(FullDate), 0)
        FullDateTimeToShortTime = Format(dtmTime, "hh:mm") & strAdd
    End If
End Function

Public Function GetConfigEntry(ConfigCollection As CollectionExtended, ByVal sAppType As String, _
ByVal Key As String, ByVal DefaultVal As String) As String
    Dim strValidAppType As String
    Dim strReturn As String
    Dim strKey As String
    
    strReturn = DefaultVal
    strKey = sAppType & Chr(0) & Key
    If ConfigCollection.Exists(strKey) Then
        strReturn = ConfigCollection.Item(strKey)
    Else
        strKey = "MAIN" & Chr(0) & "VALID_COMMAND"
        If ConfigCollection.Exists(strKey) Then
            strValidAppType = ConfigCollection.Item(strKey)
        End If
        
        If InStr(strValidAppType & ",", sAppType & ",") > 0 Then
            strKey = "MAIN" & Chr(0) & Key
            If ConfigCollection.Exists(strKey) Then
                strReturn = ConfigCollection.Item(strKey)
            End If
        End If
    End If
    
    GetConfigEntry = strReturn
End Function

Public Function AddDataSource(colDataSources As CollectionExtended, ByVal DataSourceName As String, _
colParameters As CollectionExtended, Optional ByVal KeyColumn, Optional ByVal UseTabObject As Boolean = False, _
Optional ByVal SplitWhereClause As Boolean = False, Optional ByVal Separator As String = "|", _
Optional ByVal OverwriteDataSource As Boolean = True) As String
    Dim oUFISRec As UFISRecordset
    Dim rsResult As ADODB.Recordset
    Dim strDataTableName As String
    Dim strDataFields As String
    Dim strDataTypes As String
    Dim strDataSizes As String
    Dim strDataOrder As String
    Dim vntDataWhere As Variant
    Dim strKeyList As String
    
    strKeyList = ""
    
    strDataTableName = GetConfigEntry(colConfigs, "DATA_SOURCES", DataSourceName & "_TABLE", DataSourceName)
    strDataFields = GetConfigEntry(colConfigs, "DATA_SOURCES", DataSourceName & "_FIELDS", "")
    strDataTypes = GetConfigEntry(colConfigs, "DATA_SOURCES", DataSourceName & "_TYPES", "")
    strDataSizes = GetConfigEntry(colConfigs, "DATA_SOURCES", DataSourceName & "_SIZES", "")
    strDataOrder = EvaluateParameters(colParameters, _
        GetConfigEntry(colConfigs, "DATA_SOURCES", DataSourceName & "_ORDER", ""))
    vntDataWhere = EvaluateParameters(colParameters, _
        GetConfigEntry(colConfigs, "DATA_SOURCES", DataSourceName & "_WHERE", ""), SplitWhereClause, Separator)
    
    Set oUFISRec = New UFISRecordset
    With oUFISRec
        Set .UfisServer = oUfisServer
        .ConvertSpecialChars = True
        .CheckOldVersionWhenConverting = True
        If strDataTableName = "AFTTAB" Then
            .CommandType = cmdGetFlightRecords
        Else
            .CommandType = cmdReadTable
        End If
        .TableName = strDataTableName
        .ColumnList = strDataFields
        If strDataOrder <> "" Then
            .OrderByClause = strDataOrder
        End If
        If IsArray(vntDataWhere) Then
            .WhereClause = vntDataWhere
        Else
            If vntDataWhere <> "" Then
                .WhereClause = vntDataWhere
            End If
        End If
        If strDataTypes <> "" Then
            .ColumnTypeList = strDataTypes
        End If
        If strDataSizes <> "" Then
            .ColumnSizeList = strDataSizes
        End If
        
        If Not IsMissing(KeyColumn) Then
            .KeyColumn = KeyColumn
            .PopulateKeyStringList = True
        End If
        
        Set rsResult = .CreateRecordset(UseTabObject)
        If strDataOrder <> "" Then
            rsResult.Sort = strDataOrder
        End If
        If Not IsMissing(KeyColumn) Then
            strKeyList = .KeyStringList
        End If
        
        If colDataSources.Exists(DataSourceName) Then
            If OverwriteDataSource Then
                colDataSources.Remove DataSourceName
                
                colDataSources.Add rsResult, DataSourceName
            Else
                Call CombineDataSource(colDataSources.Item(DataSourceName), rsResult)
            End If
        Else
            colDataSources.Add rsResult, DataSourceName
        End If
    End With
    
    AddDataSource = strKeyList
End Function

Public Function DateTimeToDate(ByVal DateTimeValue As Date) As Date
    DateTimeToDate = DateSerial(Year(DateTimeValue), Month(DateTimeValue), Day(DateTimeValue))
End Function

Public Function EvaluateParameters(colParameters As CollectionExtended, ByVal Text As String, _
Optional ByVal SplitWhereClause As Boolean = False, Optional ByVal Separator As String = "|") As Variant
    Const PARAM_BEGIN = "{?"
    Const PARAM_END = "}"
    
    Dim vntReturn As Variant
    Dim intStart As Integer
    Dim intPosBegin As Integer
    Dim intPosEnd As Integer
    Dim strParamKey As String
    Dim strParamValue As String
    Dim vntParamValues As Variant
    Dim vntParamValueItem As Variant
    Dim strArrayParamKey As String
    Dim i As Integer
    
    intStart = 1
    Do
        intPosBegin = InStr(intStart, Text, PARAM_BEGIN)
        If intPosBegin > 0 Then
            intPosEnd = InStr(intPosBegin + 2, Text, PARAM_END)
            If intPosEnd > 0 Then
                strParamKey = Mid(Text, intPosBegin + 2, intPosEnd - intPosBegin - 2)
                If colParameters.Exists(strParamKey) Then
                    strParamValue = colParameters.Item(strParamKey)
                    If SplitWhereClause Then
                        If InStr(strParamValue, Separator) > 0 Then
                            strArrayParamKey = strParamKey
                            vntParamValues = Split(strParamValue, Separator)
                        Else
                            Text = Left(Text, intPosBegin - 1) & strParamValue & Mid(Text, intPosEnd + 1)
                        End If
                    Else
                        Text = Left(Text, intPosBegin - 1) & strParamValue & Mid(Text, intPosEnd + 1)
                    End If
                End If
                intStart = intPosEnd + 1
            Else
                intPosBegin = 0
            End If
        End If
    Loop Until intPosBegin = 0
    
    'Evaluate array parameters
    If IsEmpty(vntParamValues) Then
        vntReturn = Text
    Else
        vntReturn = ""
        For Each vntParamValueItem In vntParamValues
            vntReturn = vntReturn & Chr(0) & Replace(Text, PARAM_BEGIN & strArrayParamKey & PARAM_END, vntParamValueItem)
        Next vntParamValueItem
        If vntReturn <> "" Then
            vntReturn = Mid(vntReturn, 2)
            vntReturn = Split(vntReturn, Chr(0))
        End If
    End If
    
    EvaluateParameters = vntReturn
End Function

Private Sub CombineDataSource(MainDataSource As ADODB.Recordset, SubDataSource As ADODB.Recordset)
    Dim fld As ADODB.Field
    
    Do While Not SubDataSource.EOF
        MainDataSource.AddNew
        For Each fld In MainDataSource.Fields
            fld.Value = SubDataSource.Fields(fld.Name).Value
        Next fld
        MainDataSource.Update
        
        SubDataSource.MoveNext
    Loop
End Sub

Public Function UTCDateToLocal(ByVal UTCDate As Date, ByVal TimeDiff) As Date
    UTCDateToLocal = DateAdd("H", (TimeDiff / 60), UTCDate)
End Function

Public Function GetFileName(cdgSave As CommonDialog, ByVal DefaultDir As String, ByVal FileName As String, ByVal ExportType As String) As String
    Dim strFileName As String
    Dim strTitle As String
    Dim strDefaultExt As String
    Dim strFileFilter As String
    
    On Error GoTo ErrGetFileName
    
    strTitle = "Export Report to "
    If ExportType = "excel" Then 'Excel
        strFileFilter = "Excel File"
        strDefaultExt = "xls"
    Else 'PDF
        strFileFilter = "PDF File"
        strDefaultExt = "pdf"
    End If
    strTitle = strTitle & strFileFilter
    strFileFilter = strFileFilter & "s (*." & strDefaultExt & ")|*." & strDefaultExt
    
    With cdgSave
        .CancelError = True
        .DialogTitle = strTitle
        .FileName = FileName
        .DefaultExt = strDefaultExt
        .InitDir = DefaultDir
        .Filter = "All Files (*.*)|*.*|" & strFileFilter
        .FilterIndex = 2
        .Flags = cdlOFNPathMustExist Or cdlOFNOverwritePrompt
        .ShowSave
    
        strFileName = .FileName
    End With
    
ErrGetFileName:
    GetFileName = strFileName
End Function

Public Function CreateFolder(ByVal FolderPath As String) As Boolean
    Dim aPath As Variant
    Dim i As Integer
    Dim sPath As String

    On Error GoTo ErrCreate
    
    CreateFolder = False
    
    aPath = Split(FolderPath, "\")
    sPath = aPath(0)
    For i = 1 To UBound(aPath)
        sPath = sPath & "\" & aPath(i)
        
        If Dir(sPath, vbDirectory) = "" Then
            MkDir sPath
        End If
    Next i
    
    CreateFolder = True
    
ErrCreate:
End Function

Public Function OpenFileUsingDefaultApp(oParent As Object, ByVal FullFileName As String) As Long
    Dim strDir As String
    
    strDir = Left(FullFileName, 3)
    OpenFileUsingDefaultApp = _
        ShellExecute(oParent.hwnd, vbNullString, FullFileName, vbNullString, strDir, SW_SHOWNORMAL)
End Function

