Attribute VB_Name = "mdlMain"
'#######################################################################
'# Main form
'# Terminal Appl Update
'#
'# adapted by KKH 20070727
'#
'# 20070727
'#######################################################################

'#######################################################################
'# Global Variables
'#######################################################################

Option Explicit

Global gsAppName As String
Global gsUserName As String
Global gbDebug As Boolean


'### GLOBAL UTC OFFSET IN HOURS
Global gUTCOffset As Single

'### GLOBAL FIELD LIST FOR R01TAB > DRRTAB
Global gR01TAB(0 To 100) As String     'array to allow search position
Global gR01FieldList As String         'comma separated list
Global gR01FieldDescList As String     'comma separated list for description of the fields to show header string
Global gR01items As Long               'Number of items

'### GLOBAL FIELD LIST FOR R02TAB > JOBTAB
Global gR02TAB(0 To 100) As String     'array to allow search position
Global gR02FieldList As String         'comma separated list
Global gR02FieldDescList As String     'comma separated list for description of the fields to show header string
Global gR02items As Long               'Number of items
Global initFlag As Boolean
Global initShiftFlag As Boolean
Global globalR02Query As String
Global sectabFlag As Boolean


'#######################################################################
'# MAIN FUNCTION called first
'#######################################################################
Sub Main()
    Dim strRetLogin As String
    Dim strCommand As String

    'gsAppName = "HASConfigTool"
    'gsAppName = "BDPSUIF"
    gsAppName = "FlightTerminalAppl"
    'gsAppName = "RMSLOGVIEWER"
    strCommand = Command()
    If strCommand = "DEBUG" Then
        gbDebug = True
    Else
        gbDebug = False
    End If

    Load frmMain
    frmMain.Visible = False

    'do the login
    InitLoginControl
    If gbDebug = False Then
        strRetLogin = frmMain.AATLoginControl1.ShowLoginDialog
    Else
        strRetLogin = "OK"
    End If

    'look after the login result
    If strRetLogin = "CANCEL" Or strRetLogin = "ERROR" Then
        frmMain.UfisCom1.CleanupCom
        End
    Else
        frmMain.UfisCom1.CleanupCom
        gsUserName = frmMain.AATLoginControl1.GetUserName()
        frmMain.SetPrivStat
        frmData.InitialLoadData

        If gbDebug = True Then
            frmData.Visible = True
        End If

        'frmMain.RefreshView
        frmMain.StatusBar1.Panels(1).Text = "Ready."
        frmMain.Show 'vbModal
    End If
End Sub

'#######################################################################
'# Initialize login control
'#######################################################################

Private Sub InitLoginControl()
    Dim strRegister As String
    
    ' giving the login control an UfisCom to do the login
    Set frmMain.AATLoginControl1.UfisComCtrl = frmMain.UfisCom1

    ' setting some information to be displayed on the login control
    frmMain.AATLoginControl1.ApplicationName = gsAppName
    frmMain.AATLoginControl1.VersionString = "4.5.0.2"
    'frmMain.AATLoginControl1.InfoCaption = "Info about RMSLOG VIEWER"
    frmMain.AATLoginControl1.InfoCaption = "Info about Flight Terminal Appl"
    frmMain.AATLoginControl1.InfoButtonVisible = True
    frmMain.AATLoginControl1.InfoUfisVersion = "UFIS Version 4.5"
    'frmMain.AATLoginControl1.InfoAppVersion = CStr("RMSLogViewer 4.5.0.2")
    frmMain.AATLoginControl1.InfoAppVersion = CStr("FlightTerminalAppl 4.5.0.1")
    frmMain.AATLoginControl1.InfoCopyright = "� 2001-2005 UFIS Airport Solutions GmbH"
    frmMain.AATLoginControl1.InfoAAT = "UFIS Airport Solutions GmbH"
    frmMain.AATLoginControl1.UserNameLCase = False 'not automatic upper case letters
    'frmMain.AATLoginControl1.ApplicationName = "HASConfigTool"
    frmMain.AATLoginControl1.ApplicationName = "FlightTerminalAppl"
    frmMain.AATLoginControl1.LoginAttempts = 3
    
    strRegister = "FlightTerminalAppl,InitModu,InitModu,Initialize (InitModu),B,-"
    strRegister = strRegister & ",TAB_Configuration,m_TAB_Configuration,Action,A,1"
    frmMain.AATLoginControl1.RegisterApplicationString = strRegister
End Sub



'#######################################################################
'###
'#######################################################################
Function SortedR02Fields(inVal As String, inField As String)
    Dim l As Long
    Dim tmpPOS As Long
    Dim tmpItems(0 To 100) As String
    Dim strKey As String
    Dim RetVal As String
    Dim tmpVal As String
     
    For l = 3 To gR02items Step 1
        strKey = gR02TAB(l)
        tmpVal = GetFieldValue(strKey, inVal, inField)
        
        If InStr(1, "TIME,CDAT,LSTU", strKey) > 0 Then
            If tmpVal <> "" Then
                tmpVal = CedaFullDateToVb(tmpVal)
            End If
        End If
        
        If InStr(1, "ACFR,ACTO,PLFR,PLTO", strKey) > 0 Then
            If tmpVal <> "" Then
                tmpVal = DateAdd("h", gUTCOffset, CedaFullDateToVb(tmpVal))
            End If
        End If
        
        'RetVal = RetVal + GetFieldValue(strKey, inVal, inField)
        RetVal = RetVal + tmpVal
        If l < gR02items Then RetVal = RetVal + ","
    Next l
    
    SortedR02Fields = RetVal
    
End Function

'#######################################################################
'###
'#######################################################################
Function SortedR01Fields(inVal As String, inField As String)
    Dim l As Long
    Dim tmpPOS As Long
    Dim tmpItems(0 To 100) As String
    Dim strKey As String
    Dim RetVal As String
    Dim tmpVal As String
     
    For l = 3 To gR01items Step 1
        strKey = gR01TAB(l)
        tmpVal = GetFieldValue(strKey, inVal, inField)
        
        If InStr(1, "TIME,AVFR,AVTO,CDAT,LSTU,SBFR,SBTO", strKey) > 0 Then
            If tmpVal <> "" Then
                'kkh
                'tmpVal = CedaFullDateToVb(tmpVal)
                tmpVal = DateAdd("h", gUTCOffset, CedaFullDateToVb(tmpVal))
            End If
        End If
        
        'RetVal = RetVal + GetFieldValue(strKey, inVal, inField)
        RetVal = RetVal + tmpVal
        If l < gR01items Then RetVal = RetVal + ","
    Next l
    
    SortedR01Fields = RetVal
    
End Function
