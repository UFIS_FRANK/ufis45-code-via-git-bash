VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{A2F31E92-C74F-11D3-A251-00500437F607}#1.0#0"; "UfisCom.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{7BDE1363-DF9A-4D96-A2A0-4E85E0191F0F}#1.0#0"; "AatLogin.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmMain 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Flight Terminal Appl 1.01"
   ClientHeight    =   9885
   ClientLeft      =   150
   ClientTop       =   435
   ClientWidth     =   13320
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   9885
   ScaleWidth      =   13320
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdReset 
      Caption         =   "&Reset"
      Height          =   375
      Left            =   3720
      TabIndex        =   57
      Top             =   7800
      Width           =   975
   End
   Begin VB.TextBox txtLogPath 
      Height          =   375
      Left            =   7680
      TabIndex        =   25
      Text            =   "D:\tmpLog"
      Top             =   9120
      Width           =   3615
   End
   Begin VB.ComboBox cmbTerminal 
      Height          =   315
      Left            =   3720
      TabIndex        =   8
      Top             =   6600
      Width           =   2175
   End
   Begin TABLib.TAB tab_AFTTAB 
      Height          =   6855
      Left            =   6840
      TabIndex        =   3
      Tag             =   "{=TABLE=}AFTTAB{=FIELDS=}URNO,FLNO,FLDA,DES3,ORG3,SEAS,ALC2,TGA1,TGD1,RKEY"
      Top             =   1560
      Width           =   5985
      _Version        =   65536
      _ExtentX        =   10557
      _ExtentY        =   12091
      _StockProps     =   64
   End
   Begin VB.TextBox txtServer 
      Enabled         =   0   'False
      Height          =   330
      Left            =   1080
      TabIndex        =   5
      Text            =   "green"
      Top             =   9120
      Width           =   945
   End
   Begin VB.TextBox txtHopo 
      Enabled         =   0   'False
      Height          =   330
      Left            =   3240
      TabIndex        =   4
      Text            =   "SIN"
      Top             =   9120
      Width           =   945
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "&Exit"
      Height          =   375
      Index           =   2
      Left            =   11880
      TabIndex        =   1
      Top             =   9120
      Width           =   1095
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   300
      Left            =   0
      TabIndex        =   0
      Top             =   9585
      Width           =   13320
      _ExtentX        =   23495
      _ExtentY        =   529
      SimpleText      =   "Ready."
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   18309
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            TextSave        =   "24-Oct-07"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   2
            TextSave        =   "5:48 PM"
         EndProperty
      EndProperty
   End
   Begin UFISCOMLib.UfisCom UfisCom1 
      Left            =   0
      Top             =   0
      _Version        =   65536
      _ExtentX        =   1111
      _ExtentY        =   1164
      _StockProps     =   0
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   8655
      Left            =   120
      TabIndex        =   9
      Top             =   240
      Width           =   13095
      _ExtentX        =   23098
      _ExtentY        =   15266
      _Version        =   393216
      TabHeight       =   520
      TabCaption(0)   =   "SQ"
      TabPicture(0)   =   "frmMain.frx":030A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Other Airlines"
      TabPicture(1)   =   "frmMain.frx":0326
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Others"
      Tab(1).Control(1)=   "cmdUpdate(0)"
      Tab(1).Control(2)=   "cmbSeason"
      Tab(1).Control(3)=   "cmdBack"
      Tab(1).Control(4)=   "cmdFront"
      Tab(1).Control(5)=   "ListAirlineTo"
      Tab(1).Control(6)=   "ListAirlineFrom"
      Tab(1).ControlCount=   7
      TabCaption(2)   =   "Ad Hoc"
      TabPicture(2)   =   "frmMain.frx":0342
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "AdHoc"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).ControlCount=   1
      Begin VB.Frame Frame1 
         Caption         =   "SQ Flight"
         Height          =   7335
         Left            =   240
         TabIndex        =   36
         Top             =   840
         Width           =   6375
         Begin VB.PictureBox Picture1 
            BorderStyle     =   0  'None
            Height          =   1455
            Left            =   480
            Picture         =   "frmMain.frx":035E
            ScaleHeight     =   1455
            ScaleWidth      =   1935
            TabIndex        =   58
            Top             =   5760
            Width           =   1935
         End
         Begin VB.CommandButton cmdUpdateSQ 
            Caption         =   "&Update"
            Height          =   375
            Index           =   1
            Left            =   4440
            TabIndex        =   50
            Top             =   6720
            Width           =   1095
         End
         Begin VB.CommandButton cmdNatureBackSQ 
            Caption         =   "<<"
            Height          =   375
            Left            =   2520
            TabIndex        =   48
            Top             =   4080
            Width           =   1095
         End
         Begin VB.CommandButton cmdNatureFrontSQ 
            Caption         =   ">>"
            Height          =   375
            Left            =   2520
            TabIndex        =   47
            Top             =   3480
            Width           =   1095
         End
         Begin VB.ListBox ListNatureToSQ 
            Height          =   1425
            Left            =   3840
            MultiSelect     =   2  'Extended
            Sorted          =   -1  'True
            TabIndex        =   46
            Top             =   3240
            Width           =   1695
         End
         Begin VB.ListBox ListNatureFromSQ 
            Height          =   1425
            Left            =   480
            MultiSelect     =   2  'Extended
            Sorted          =   -1  'True
            TabIndex        =   45
            Top             =   3240
            Width           =   1695
         End
         Begin VB.ComboBox cmbSeasonSQ 
            Height          =   315
            Left            =   3360
            TabIndex        =   43
            Top             =   5040
            Width           =   2175
         End
         Begin VB.CommandButton cmdBackSQ 
            Caption         =   "<<"
            Height          =   375
            Left            =   2520
            TabIndex        =   42
            Top             =   2040
            Width           =   1095
         End
         Begin VB.CommandButton cmdFrontSQ 
            Caption         =   ">>"
            Height          =   375
            Left            =   2520
            TabIndex        =   41
            Top             =   1320
            Width           =   1095
         End
         Begin VB.ListBox ListAirportTo 
            Height          =   1620
            Left            =   3840
            MultiSelect     =   2  'Extended
            Sorted          =   -1  'True
            TabIndex        =   38
            Top             =   1080
            Width           =   1695
         End
         Begin VB.ListBox ListAirportFrom 
            Height          =   1620
            Left            =   480
            MultiSelect     =   2  'Extended
            Sorted          =   -1  'True
            TabIndex        =   37
            Top             =   1080
            Width           =   1695
         End
         Begin VB.Label Label12 
            Caption         =   "Season : "
            Height          =   375
            Left            =   480
            TabIndex        =   49
            Top             =   5040
            Width           =   2295
         End
         Begin VB.Label Label11 
            Caption         =   "Flight Nature :"
            Height          =   255
            Left            =   480
            TabIndex        =   44
            Top             =   2880
            Width           =   1935
         End
         Begin VB.Label Label9 
            Caption         =   "Terminal to update : "
            Height          =   375
            Left            =   480
            TabIndex        =   40
            Top             =   5520
            Width           =   2895
         End
         Begin VB.Label Label10 
            Caption         =   "Destination(Airport Code):"
            Height          =   375
            Left            =   480
            TabIndex        =   39
            Top             =   720
            Width           =   1935
         End
      End
      Begin VB.ListBox ListAirlineFrom 
         Height          =   1620
         ItemData        =   "frmMain.frx":08A4
         Left            =   -74280
         List            =   "frmMain.frx":08A6
         MultiSelect     =   2  'Extended
         Sorted          =   -1  'True
         TabIndex        =   18
         Top             =   1920
         Width           =   1695
      End
      Begin VB.ListBox ListAirlineTo 
         Height          =   1620
         Left            =   -70920
         MultiSelect     =   2  'Extended
         Sorted          =   -1  'True
         TabIndex        =   17
         Top             =   1920
         Width           =   1695
      End
      Begin VB.CommandButton cmdFront 
         Caption         =   ">>"
         Height          =   375
         Left            =   -72240
         TabIndex        =   16
         Top             =   2160
         Width           =   1095
      End
      Begin VB.CommandButton cmdBack 
         Caption         =   "<<"
         Height          =   375
         Left            =   -72240
         TabIndex        =   15
         Top             =   2880
         Width           =   1095
      End
      Begin VB.ComboBox cmbSeason 
         Height          =   315
         Left            =   -71400
         TabIndex        =   14
         Top             =   5880
         Width           =   2175
      End
      Begin VB.CommandButton cmdUpdate 
         Caption         =   "&Update"
         Height          =   375
         Index           =   0
         Left            =   -70320
         TabIndex        =   13
         Top             =   7560
         Width           =   1095
      End
      Begin VB.Frame AdHoc 
         Caption         =   "Ad Hoc"
         Height          =   7335
         Left            =   -74760
         TabIndex        =   10
         Top             =   840
         Width           =   6375
         Begin VB.TextBox txtSuffix 
            Height          =   315
            Left            =   5040
            TabIndex        =   55
            ToolTipText     =   "Suffix"
            Top             =   1440
            Width           =   375
         End
         Begin MSComCtl2.DTPicker DateFrom 
            Height          =   375
            Left            =   480
            TabIndex        =   52
            Top             =   2640
            Width           =   2295
            _ExtentX        =   4048
            _ExtentY        =   661
            _Version        =   393216
            Format          =   57081857
            CurrentDate     =   39335
         End
         Begin VB.CommandButton cmdUpdateAdhoc 
            Caption         =   "&Update"
            Height          =   375
            Index           =   2
            Left            =   4440
            TabIndex        =   51
            Top             =   6720
            Width           =   1095
         End
         Begin VB.TextBox txtFLNO 
            Height          =   315
            Left            =   3960
            TabIndex        =   35
            ToolTipText     =   "Fight number"
            Top             =   1440
            Width           =   975
         End
         Begin VB.ComboBox cmbAirline 
            Height          =   315
            ItemData        =   "frmMain.frx":08A8
            Left            =   1440
            List            =   "frmMain.frx":08AA
            TabIndex        =   34
            ToolTipText     =   "Airline"
            Top             =   1440
            Width           =   1335
         End
         Begin VB.OptionButton optArr 
            Caption         =   "Arrival"
            Height          =   495
            Left            =   1440
            TabIndex        =   12
            Top             =   600
            Value           =   -1  'True
            Width           =   1455
         End
         Begin VB.OptionButton optDep 
            Caption         =   "Departure"
            Height          =   495
            Left            =   3120
            TabIndex        =   11
            Top             =   600
            Width           =   1335
         End
         Begin MSComCtl2.DTPicker DateTo 
            Height          =   375
            Left            =   3200
            TabIndex        =   53
            Top             =   2640
            Width           =   2295
            _ExtentX        =   4048
            _ExtentY        =   661
            _Version        =   393216
            Format          =   57081857
            CurrentDate     =   39335
         End
         Begin VB.Label Label14 
            Caption         =   "To : "
            Height          =   375
            Left            =   3240
            TabIndex        =   56
            Top             =   2280
            Width           =   495
         End
         Begin VB.Label Label13 
            Caption         =   "Flight No:"
            Height          =   375
            Left            =   3120
            TabIndex        =   54
            Top             =   1440
            Width           =   735
         End
         Begin VB.Label Label8 
            Caption         =   "From : "
            Height          =   375
            Left            =   480
            TabIndex        =   33
            Top             =   2280
            Width           =   495
         End
         Begin VB.Label Label7 
            Caption         =   "Flight :"
            Height          =   375
            Left            =   480
            TabIndex        =   32
            Top             =   1440
            Width           =   735
         End
         Begin VB.Label Label6 
            Caption         =   "Arr/Dep :"
            Height          =   375
            Left            =   480
            TabIndex        =   31
            Top             =   720
            Width           =   1215
         End
         Begin VB.Label Label4 
            Caption         =   "Terminal to update : "
            Height          =   375
            Left            =   480
            TabIndex        =   23
            Top             =   5520
            Width           =   2895
         End
      End
      Begin VB.Frame Others 
         Caption         =   "Other Airline"
         Height          =   7335
         Left            =   -74760
         TabIndex        =   19
         Top             =   840
         Width           =   6375
         Begin VB.CommandButton cmdNatureBack 
            Caption         =   "<<"
            Height          =   375
            Left            =   2520
            TabIndex        =   30
            Top             =   4080
            Width           =   1095
         End
         Begin VB.CommandButton cmdNatureFront 
            Caption         =   ">>"
            Height          =   375
            Left            =   2520
            TabIndex        =   29
            Top             =   3480
            Width           =   1095
         End
         Begin VB.ListBox ListNatureTo 
            Height          =   1425
            Left            =   3840
            MultiSelect     =   2  'Extended
            Sorted          =   -1  'True
            TabIndex        =   28
            Top             =   3240
            Width           =   1695
         End
         Begin VB.ListBox ListNatureFrom 
            Height          =   1425
            Left            =   480
            MultiSelect     =   2  'Extended
            Sorted          =   -1  'True
            TabIndex        =   27
            Top             =   3240
            Width           =   1695
         End
         Begin VB.Label lbl6 
            Caption         =   "Flight Nature :"
            Height          =   495
            Left            =   480
            TabIndex        =   26
            Top             =   2880
            Width           =   1215
         End
         Begin VB.Label Label1 
            Caption         =   "Airline(s) : "
            Height          =   375
            Left            =   480
            TabIndex        =   22
            Top             =   720
            Width           =   2295
         End
         Begin VB.Label Label2 
            Caption         =   "Season : "
            Height          =   375
            Left            =   480
            TabIndex        =   21
            Top             =   5040
            Width           =   2295
         End
         Begin VB.Label Label3 
            Caption         =   "Terminal to update : "
            Height          =   375
            Left            =   480
            TabIndex        =   20
            Top             =   5520
            Width           =   3375
         End
      End
   End
   Begin AATLOGINLib.AatLogin AATLoginControl1 
      Height          =   615
      Left            =   0
      TabIndex        =   2
      Top             =   8400
      Visible         =   0   'False
      Width           =   855
      _Version        =   65536
      _ExtentX        =   1508
      _ExtentY        =   1085
      _StockProps     =   0
   End
   Begin VB.Label Label5 
      Caption         =   "Change the path for log file :"
      Height          =   375
      Left            =   5160
      TabIndex        =   24
      Top             =   9120
      Width           =   2175
   End
   Begin VB.Label lblHOPO 
      Caption         =   "HOPO"
      Height          =   375
      Left            =   2400
      TabIndex        =   7
      Top             =   9120
      Width           =   735
   End
   Begin VB.Label lblServer 
      Caption         =   "Server"
      Height          =   255
      Left            =   240
      TabIndex        =   6
      Top             =   9120
      Width           =   615
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'#######################################################################
'# Main form
'# Terminal Appl Update
'#
'# adapted by KKH 20070727
'#
'# 20070727
'#######################################################################
Option Explicit

Dim strHOPO As String
Dim strServer As String
Dim globURNOlist As String

Dim globTestOnly As Boolean
Private strTableExt As String
Dim updateAction As Boolean
Dim strTerminal As String
Dim strTerminalOri As String
Dim i As Integer
Dim tmpFROM As String
Dim FileNumber
Dim strFileName As String
Dim tmpTO As String
Dim recordcnt As Long
Dim CloseConnex As Boolean

Private Sub cmdFrontSQ_Click()
    If ListAirportFrom.SelCount < 1 Then
        MsgBox "Please select Destination Airport"
        Exit Sub
    End If
    For i = ListAirportFrom.ListCount - 1 To 0 Step -1
        If ListAirportFrom.Selected(i) = True Then
            ListAirportTo.AddItem ListAirportFrom.List(i)
            ListAirportFrom.RemoveItem i
        End If
    Next i
'    ListAirportTo.AddItem ListAirportFrom.Text
'    ListAirportFrom.RemoveItem ListAirportFrom.ListIndex
End Sub
Private Sub cmdBackSQ_Click()
    If ListAirportTo.SelCount < 1 Then
        MsgBox "Please select Destination Airport"
        Exit Sub
    End If
    For i = ListAirportTo.ListCount - 1 To 0 Step -1
        If ListAirportTo.Selected(i) = True Then
            ListAirportFrom.AddItem ListAirportTo.List(i)
            ListAirportTo.RemoveItem i
        End If
    Next i
'    ListAirportFrom.AddItem ListAirportTo.Text
'    ListAirportTo.RemoveItem ListAirportTo.ListIndex
End Sub

Private Sub cmdFront_Click()
    If ListAirlineFrom.SelCount < 1 Then
        MsgBox "Please select Airline"
        Exit Sub
    End If
    For i = ListAirlineFrom.ListCount - 1 To 0 Step -1
        If ListAirlineFrom.Selected(i) = True Then
            ListAirlineTo.AddItem ListAirlineFrom.List(i)
            ListAirlineFrom.RemoveItem i
        End If
    Next i
End Sub

Private Sub cmdBack_Click()
    If ListAirlineTo.SelCount < 1 Then
        MsgBox "Please select Airline"
        Exit Sub
    End If
    For i = ListAirlineTo.ListCount - 1 To 0 Step -1
        If ListAirlineTo.Selected(i) = True Then
            ListAirlineFrom.AddItem ListAirlineTo.List(i)
            ListAirlineTo.RemoveItem i
        End If
    Next i
End Sub

Private Sub cmdNatureBack_Click()
    If ListNatureTo.SelCount < 1 Then
        MsgBox "Please select Nature"
        Exit Sub
    End If
    For i = ListNatureTo.ListCount - 1 To 0 Step -1
        If ListNatureTo.Selected(i) = True Then
            ListNatureFrom.AddItem ListNatureTo.List(i)
            ListNatureTo.RemoveItem i
        End If
    Next i
End Sub

Private Sub cmdNatureBackSQ_Click()
    If ListNatureToSQ.SelCount < 1 Then
        MsgBox "Please select Nature"
        Exit Sub
    End If
    For i = ListNatureToSQ.ListCount - 1 To 0 Step -1
        If ListNatureToSQ.Selected(i) = True Then
            ListNatureFromSQ.AddItem ListNatureToSQ.List(i)
            ListNatureToSQ.RemoveItem i
        End If
    Next i
End Sub

Private Sub cmdNatureFront_Click()
    If ListNatureFrom.SelCount < 1 Then
        MsgBox "Please select Nature"
        Exit Sub
    End If
    For i = ListNatureFrom.ListCount - 1 To 0 Step -1
        If ListNatureFrom.Selected(i) = True Then
            ListNatureTo.AddItem ListNatureFrom.List(i)
            ListNatureFrom.RemoveItem i
        End If
    Next i
End Sub

Private Sub cmdNatureFrontSQ_Click()
    If ListNatureFromSQ.SelCount < 1 Then
        MsgBox "Please select Nature"
        Exit Sub
    End If
    For i = ListNatureFromSQ.ListCount - 1 To 0 Step -1
        If ListNatureFromSQ.Selected(i) = True Then
            ListNatureToSQ.AddItem ListNatureFromSQ.List(i)
            ListNatureFromSQ.RemoveItem i
        End If
    Next i
'    ListNatureToSQ.AddItem ListNatureFromSQ.Text
'    ListNatureFromSQ.RemoveItem ListNatureFromSQ.ListIndex
End Sub

'#######################################################################
'### Leave application
'#######################################################################

Private Sub cmdExit_Click(Index As Integer)
End
    Dim ilRet
    ilRet = MsgBox("Do you want to exit ?", vbYesNo, "EXIT")
    If ilRet = vbYes Then
        Unload frmData
        End
    End If

End Sub

'#######################################################################
'### INIT DB TABLES
'#######################################################################
Sub InitTabs()

    Dim l As Integer
    Dim SqlKey As String
    Dim SqlKey1 As String
    Dim CedaDataAfttab As String
    Dim AllAftFldLst As String
    Dim tmpRectab As String
    Dim tmpstr As String
    
    Dim strCmbAirline1 As String
    Dim strCmbAirline2 As String
    Dim strListNature As String
    Dim strListAirport As String
    
    frmData.InitTabGeneral frmData.tab_GATTAB
    frmData.InitTabForCedaConnection frmData.tab_GATTAB
    frmData.LoadData frmData.tab_GATTAB, " "
    
    frmData.InitTabGeneral frmData.tab_SEATAB
    frmData.InitTabForCedaConnection frmData.tab_SEATAB
    frmData.LoadData frmData.tab_SEATAB, " "
    
    tmpRectab = frmData.tab_GATTAB.SelectDistinct("TERM", "", "", ",", True)
    'Debug.Print tmpRectab
    If Len(tmpRectab) > 0 Then
        For i = 1 To Len(tmpRectab)
            tmpstr = GetItem(tmpRectab, i, ",")
            'Debug.Print tmpstr
            If tmpstr <> "" Then
                cmbTerminal.AddItem tmpstr
            End If
        Next i
    End If
    
    For l = 0 To frmData.tab_SEATAB.GetLineCount - 1 Step 1
        SqlKey1 = frmData.tab_SEATAB.GetColumnValues(l, 0)
        cmbSeason.AddItem SqlKey1
        cmbSeasonSQ.AddItem SqlKey1
    Next l
    
    'Initialise option for AdHoc update
    frmData.InitTabGeneral frmData.tab_ALTTAB
    frmData.InitTabForCedaConnection frmData.tab_ALTTAB
    frmData.LoadData frmData.tab_ALTTAB, "order by ALC2"
    
    For l = 0 To frmData.tab_ALTTAB.GetLineCount - 1 Step 1
        If frmData.tab_ALTTAB.GetColumnValues(l, 0) = "" Then
            strCmbAirline1 = "        " + frmData.tab_ALTTAB.GetColumnValues(l, 1)
        Else
            strCmbAirline1 = frmData.tab_ALTTAB.GetColumnValues(l, 0) + "   " + frmData.tab_ALTTAB.GetColumnValues(l, 1)
            strCmbAirline2 = frmData.tab_ALTTAB.GetColumnValues(l, 0)
        End If
        cmbAirline.AddItem strCmbAirline1
    Next l
    
    DateFrom.Value = Now
    DateTo.Value = Now
    
    'Initialise option for Other Airline(s) Update
    For l = 0 To frmData.tab_ALTTAB.GetLineCount - 1 Step 1
        If frmData.tab_ALTTAB.GetColumnValues(l, 0) = "" Then
            strCmbAirline1 = "        " + frmData.tab_ALTTAB.GetColumnValues(l, 1)
        Else
            strCmbAirline1 = frmData.tab_ALTTAB.GetColumnValues(l, 0) + "   " + frmData.tab_ALTTAB.GetColumnValues(l, 1)
            strCmbAirline2 = frmData.tab_ALTTAB.GetColumnValues(l, 0)
        End If
        ListAirlineFrom.AddItem strCmbAirline1
    Next l
    
    frmData.InitTabGeneral frmData.tab_NATTAB
    frmData.InitTabForCedaConnection frmData.tab_NATTAB
    frmData.LoadData frmData.tab_NATTAB, "order by TTYP"
        
    For l = 0 To frmData.tab_NATTAB.GetLineCount - 1 Step 1
        strListNature = frmData.tab_NATTAB.GetColumnValues(l, 0)
        ListNatureFrom.AddItem strListNature
        ListNatureFromSQ.AddItem strListNature
    Next l
    
    'Initialise option for SQ update
    frmData.InitTabGeneral frmData.tab_APTTAB
    frmData.InitTabForCedaConnection frmData.tab_APTTAB
    frmData.LoadData frmData.tab_APTTAB, "order by APC3"
        
    For l = 0 To frmData.tab_APTTAB.GetLineCount - 1 Step 1
        If frmData.tab_APTTAB.GetColumnValues(l, 0) = "" Then
            strListAirport = "          " + frmData.tab_APTTAB.GetColumnValues(l, 1)
        Else
            strListAirport = frmData.tab_APTTAB.GetColumnValues(l, 0) + "   " + frmData.tab_APTTAB.GetColumnValues(l, 1)
        End If
        ListAirportFrom.AddItem strListAirport
    Next l

    'cmbTerminal
    'cmbSeason.Sorted
    
End Sub

Private Function CheckLogPath()
    Dim tmpPath As String
    CheckLogPath = False
    If txtLogPath.Text = "" Then
        MsgBox "Please enter path for log file"
        txtLogPath.SetFocus
        CheckLogPath = True
        Exit Function
    End If
    
    tmpPath = txtLogPath.Text
    If Dir$(tmpPath, vbDirectory) = "" Then
        'Directory doesnt exist, create automatically
        MkDir tmpPath
    End If
End Function

Private Sub cmdReset_Click()
    ClearForm True
End Sub

Private Sub cmdUpdate_Click(Index As Integer)
        
    Dim l As Integer
    Dim strSeason As String
    Dim strTerminal As String
    Dim strAirline As String
    Dim strNature As String
    Dim airlineCnt As Integer
    Dim natureCnt As Integer
    Dim strWhere As String
    Dim strURNO As String
    Dim strDES3 As String
    Dim strORG3 As String
    Dim strFLD As String
    
    Dim incCnt As Long
       
    Dim ilRet
    ilRet = MsgBox("Are you sure you want to update ?", vbYesNo, "UPDATE")
    If ilRet = vbNo Then
        Exit Sub
    End If
    
    'Get the airline name from the listbox
    airlineCnt = ListAirlineTo.ListCount

    For l = 0 To airlineCnt - 1
        If l = 0 Then
            strAirline = "'" + Left(ListAirlineTo.List(l), 2) + "'"
        Else
            strAirline = strAirline + "," + "'" + Left(ListAirlineTo.List(l), 2) + "'"
        End If
    Next l
    
    'Get the nature from the listbox
    natureCnt = ListNatureTo.ListCount

    For l = 0 To natureCnt - 1
        If l = 0 Then
            strNature = "'" + ListNatureTo.List(l) + "'"
        Else
            strNature = strNature + "," + "'" + ListNatureTo.List(l) + "'"
        End If
    Next l
    
    strSeason = cmbSeason.Text
    strTerminal = cmbTerminal.Text
    
    If ListAirlineTo.ListCount < 1 Then
        MsgBox "Please select Airline"
        ListAirlineFrom.SetFocus
        Exit Sub
    End If
    
    If ListNatureTo.ListCount < 1 Then
        MsgBox "Please select Flight Nature"
        ListNatureFrom.SetFocus
        Exit Sub
    End If
    
    If strSeason = "" Then
        MsgBox "Please select a season"
        cmbSeason.SetFocus
        Exit Sub
    End If
    
    If strTerminal = "" Then
        MsgBox "Please select terminal"
        cmbTerminal.SetFocus
        Exit Sub
    End If

    If CheckLogPath = True Then Exit Sub
    
    ConnectUfisCom
    
    tmpFROM = Format(Date, "YYYYMMDD") & Format(Time, "hhmmss")
        
    strFileName = txtLogPath.Text & "\AdDoc" & tmpFROM & ".log"
    FileNumber = FreeFile   ' Get unused file number.

    Open strFileName For Output As #FileNumber ' Create file name.

    Print #FileNumber, "---------------------------------------------------------------"
    Print #FileNumber, "Update started at " & tmpFROM
    Print #FileNumber, "---------------------------------------------------------------"

    Close #FileNumber
    tab_AFTTAB.ResetContent
    tab_AFTTAB.Refresh
    
    strWhere = "where SEAS = '" & strSeason & "' and ALC2 in (" & strAirline & ") and TTYP in (" & strNature & ")"
    'LoadData tab_AFTTAB, "where SEAS = '& strSeason & ' and ALC2 in (" & strAirline & ")"
    StatusBar1.Panels(1).Text = "Searching ...  "
    LoadData tab_AFTTAB, strWhere
    tab_AFTTAB.AutoSizeColumns
    tab_AFTTAB.Refresh
    
    Debug.Print strWhere
    incCnt = tab_AFTTAB.GetLineCount
    If Not CedaIsConnected Then UfisServer.ConnectToCeda
    For i = 0 To tab_AFTTAB.GetLineCount - 1 Step 1
        
        updateAction = False
        ' get field values
        strURNO = tab_AFTTAB.GetFieldValues(i, "URNO")
        strDES3 = tab_AFTTAB.GetFieldValues(i, "DES3")
        strORG3 = tab_AFTTAB.GetFieldValues(i, "ORG3")
        
        MousePointer = vbHourglass
        ' kkh - check whether to update TGA1/TGA1
        If strDES3 = "SIN" Then
            updateAction = True
            strWhere = "where urno = " & strURNO & ""
            strFLD = "TGA1"
            strTerminalOri = tab_AFTTAB.GetFieldValues(i, "TGA1")
        Else
            If strORG3 = "SIN" Then
                updateAction = True
                strWhere = "where urno = " & strURNO & ""
                strFLD = "TGD1"
                strTerminalOri = tab_AFTTAB.GetFieldValues(i, "TGD1")
            End If
        End If
        
        If updateAction = True Then
            FileNumber = FreeFile   ' Get unused file number.
            Open strFileName For Append As #FileNumber    ' Create file name.
            Print #FileNumber, "URNO : " & strURNO & " | FLNO : " & tab_AFTTAB.GetFieldValues(i, "FLNO") & " | Update " & strFLD & " " & strTerminalOri & " TO "; strTerminal
            Close #FileNumber
            
            UfisServer.CallCeda CedaDataAnswer, "URT", "AFTTAB", strFLD, strTerminal, strWhere, "", 0, False, False
            recordcnt = recordcnt + 1
            StatusBar1.Panels(1).Text = "Updating record : " & recordcnt
        End If
    Next i
    If incCnt = 0 Then
        MsgBox "No record to update"
        Kill (strFileName)
        StatusBar1.Panels(1).Text = "Ready."
    Else
        FileNumber = FreeFile   ' Get unused file number.
        Open strFileName For Append As #FileNumber    ' Create file name.
        tmpTO = Format(Date, "YYYYMMDD") & Format(Time, "hhmmss")
        Print #FileNumber, "Total records Update : " & recordcnt
        Print #FileNumber, "---------------------------------------------------------------"
        Print #FileNumber, "Update finished at " & tmpTO
        Print #FileNumber, "---------------------------------------------------------------"
        Close #FileNumber   ' Close file.
        ClearForm False
        StatusBar1.Panels(1).Text = "Update finished, total no of records updated " & recordcnt
    End If
    MousePointer = vbDefault
    If CloseConnex Then UfisServer.DisconnectFromCeda
    DisconnetUfisCom
End Sub


Private Sub cmdUpdateAdhoc_Click(Index As Integer)
    Dim l As Integer
    Dim strAirline As String
    Dim airlineCnt As Integer
    Dim strWhere As String
    Dim strURNO As String
    Dim strSIN As String
    Dim strADID As String
    Dim strFLD As String
    Dim strFLNO As String
    Dim strDateFrom As String
    Dim strDateTo As String
    Dim strSuffix As String
            
    Dim incCnt As Long
    Dim ilRet
    
    recordcnt = 0
    
    ilRet = MsgBox("Are you sure you want to update ?", vbYesNo, "UPDATE")
    If ilRet = vbNo Then
        Exit Sub
    End If
    
    If optArr.Value = True Then
        strADID = "DES3"
    Else
        strADID = "ORG3"
    End If
    
    strAirline = cmbAirline.Text
    
    If strAirline = "" Then
        MsgBox "Please select Airline"
        cmbAirline.SetFocus
        Exit Sub
    End If
    If Len(Trim(strAirline)) > 3 Then
        strAirline = Left(strAirline, 2)
    Else
        strAirline = strAirline
    End If
    
    strFLNO = txtFLNO.Text
    
'    If strFLNO = "" Then
'        MsgBox "Please enter Flight no"
'        txtFLNO.SetFocus
'        Exit Sub
'    End If
    strSuffix = txtSuffix.Text
    
    strDateFrom = Format(DateFrom.Value, "YYYYMMDD")
    strDateTo = Format(DateTo.Value, "YYYYMMDD")
    strTerminal = cmbTerminal.Text
    
    If strTerminal = "" Then
        MsgBox "Please select Terminal to Update"
        cmbTerminal.SetFocus
        Exit Sub
    End If
        
    If CheckLogPath = True Then Exit Sub
    
    ConnectUfisCom
    
    tmpFROM = Format(Date, "YYYYMMDD") & Format(Time, "hhmmss")
        
    strFileName = txtLogPath.Text & "\AdDoc" & tmpFROM & ".log"
    FileNumber = FreeFile   ' Get unused file number.

    Open strFileName For Output As #FileNumber ' Create file name.

    Print #FileNumber, "---------------------------------------------------------------"
    Print #FileNumber, "Update started at " & tmpFROM
    Print #FileNumber, "---------------------------------------------------------------"

    Close #FileNumber
    
    tab_AFTTAB.ResetContent
    tab_AFTTAB.Refresh
    
    strSIN = "= 'SIN'"
    strADID = strADID + strSIN
    If Len(strAirline) = 2 Then
        If strSuffix <> "" Then
            'strWhere = "where ALC2 = '" & strAirline & "' and " & strADID & " and Trim(FLNO) like '" & strAirline & "" + "%" + "" & strFLNO & "" + "%" + "" & strSuffix & "' and FLDA between '" & strDateFrom & "' and '" & strDateTo & "'"
            strWhere = "where ALC2 = '" & strAirline & "' and " & strADID & " and FLTN = '" & Trim(strFLNO) & "' and FLNS = '" & strSuffix & "' and FLDA between '" & strDateFrom & "' and '" & strDateTo & "'"
        Else
            'strWhere = "where ALC2 = '" & strAirline & "' and " & strADID & " and Trim(FLNO) like '" & strAirline & "" + "%" + "" & strFLNO & "' and FLDA between '" & strDateFrom & "' and '" & strDateTo & "'"
            strWhere = "where ALC2 = '" & strAirline & "' and " & strADID & " and FLTN = '" & Trim(strFLNO) & "' and FLDA between '" & strDateFrom & "' and '" & strDateTo & "'"
        End If
    Else
        If strSuffix <> "" Then
            strWhere = "where ALC3 = '" & Trim(strAirline) & "' and " & strADID & " and FLTN = '" & strFLNO & "' and FLNS = '" & strSuffix & "' and FLDA between '" & strDateFrom & "' and '" & strDateTo & "'"
        Else
            strWhere = "where ALC3 = '" & Trim(strAirline) & "' and " & strADID & " and FLTN = '" & strFLNO & "' and FLDA between '" & strDateFrom & "' and '" & strDateTo & "'"
        End If
    End If
       
    If strFLNO = "" Then
        strWhere = "where ALC2 = '" & strAirline & "' and " & strADID & " and FLDA between '" & strDateFrom & "' and '" & strDateTo & "'"
        ilRet = MsgBox("Are you sure you want to update all " & strAirline & " flights?", vbYesNo, "UPDATE RECORD")
    Else
        ilRet = MsgBox("Are you sure you want to update flight " & strAirline & " " & strFLNO & " " & strSuffix & " ?", vbYesNo, "UPDATE RECORD")
    End If
    
    If ilRet = vbNo Then
        Exit Sub
    End If
        
    StatusBar1.Panels(1).Text = "Searching ...  "
    LoadData tab_AFTTAB, strWhere
    tab_AFTTAB.AutoSizeColumns
    tab_AFTTAB.Refresh
    
    incCnt = tab_AFTTAB.GetLineCount
    If Not CedaIsConnected Then UfisServer.ConnectToCeda
    For i = 0 To tab_AFTTAB.GetLineCount - 1 Step 1
        updateAction = False
        strURNO = tab_AFTTAB.GetFieldValues(i, "URNO")
        
        MousePointer = vbHourglass
        ' kkh - check whether to update TGA1/TGA1
        If optArr.Value = True Then
            updateAction = True
            strWhere = "where urno = " & strURNO & ""
            strFLD = "TGA1"
            strTerminalOri = tab_AFTTAB.GetFieldValues(i, "TGA1")
        Else
            updateAction = True
            strWhere = "where urno = " & strURNO & ""
            strFLD = "TGD1"
            strTerminalOri = tab_AFTTAB.GetFieldValues(i, "TGD1")
            
        End If
        If updateAction = True Then
            FileNumber = FreeFile   ' Get unused file number.
            Open strFileName For Append As #FileNumber    ' Create file name.
            Print #FileNumber, "URNO : " & strURNO & " | FLNO : " & tab_AFTTAB.GetFieldValues(i, "FLNO") & " | Update " & strFLD & " " & strTerminalOri & " TO "; strTerminal
            Close #FileNumber
            
            strFLNO = tab_AFTTAB.GetFieldValues(i, "FLNO")
            UfisServer.CallCeda CedaDataAnswer, "URT", "AFTTAB", strFLD, strTerminal, strWhere, "", 0, False, False
            recordcnt = recordcnt + 1
            StatusBar1.Panels(1).Text = "Updating record : " & recordcnt
        End If
    Next i
    
    If incCnt = 0 Then
        MsgBox "No record to update"
        Kill (strFileName)
        StatusBar1.Panels(1).Text = "Ready."
    Else
        FileNumber = FreeFile   ' Get unused file number.
        Open strFileName For Append As #FileNumber    ' Create file name.
        tmpTO = Format(Date, "YYYYMMDD") & Format(Time, "hhmmss")
        Print #FileNumber, "Total records Update : " & recordcnt
        Print #FileNumber, "---------------------------------------------------------------"
        Print #FileNumber, "Update finished at " & tmpTO
        Print #FileNumber, "---------------------------------------------------------------"
        Close #FileNumber   ' Close file.
        ClearForm False
        StatusBar1.Panels(1).Text = "Update finished, total no of records updated " & recordcnt
    End If
    MousePointer = vbDefault
    If CloseConnex Then UfisServer.DisconnectFromCeda
    DisconnetUfisCom
    
End Sub

Private Sub cmdUpdateSQ_Click(Index As Integer)
    Dim l As Integer
    Dim strSeasonSQ As String
    Dim strTerminal As String
    Dim strAirport As String
    Dim strNatureSQ As String
    Dim airportCnt As Integer
    Dim natureSQCnt As Integer
    Dim strWhere As String
    Dim strURNO As String
    Dim strRKEY As String
    Dim strORG3 As String
    Dim strFLD As String
    
    Dim incCnt As Long
    Dim updateStat As Integer
       
    Dim ilRet
    ilRet = MsgBox("Are you sure you want to update ?", vbYesNo, "UPDATE")
    If ilRet = vbNo Then
        Exit Sub
    End If
    
    'Get the airline name from the listbox
    airportCnt = ListAirportTo.ListCount

    For l = 0 To airportCnt - 1
        If l = 0 Then
            strAirport = "'" + Left(ListAirportTo.List(l), 3) + "'"
        Else
            strAirport = strAirport + "," + "'" + Left(ListAirportTo.List(l), 3) + "'"
        End If
    Next l
    
    'Get the nature from the listbox
    natureSQCnt = ListNatureToSQ.ListCount

    For l = 0 To natureSQCnt - 1
        If l = 0 Then
            strNatureSQ = "'" + ListNatureToSQ.List(l) + "'"
        Else
            strNatureSQ = strNatureSQ + "," + "'" + ListNatureToSQ.List(l) + "'"
        End If
    Next l
    
    strSeasonSQ = cmbSeasonSQ.Text
    strTerminal = cmbTerminal.Text
    
    If ListAirportTo.ListCount < 1 Then
        MsgBox "Please select Destination Airport"
        ListAirportFrom.SetFocus
        Exit Sub
    End If
    
    If ListNatureToSQ.ListCount < 1 Then
        MsgBox "Please select Flight Nature"
        ListNatureFromSQ.SetFocus
        Exit Sub
    End If
    
    If strSeasonSQ = "" Then
        MsgBox "Please select a season"
        cmbSeasonSQ.SetFocus
        Exit Sub
    End If
    
    If strTerminal = "" Then
        MsgBox "Please select terminal"
        cmbTerminal.SetFocus
        Exit Sub
    End If

    If CheckLogPath = True Then Exit Sub
    
    ConnectUfisCom
    
    tmpFROM = Format(Date, "YYYYMMDD") & Format(Time, "hhmmss")
        
    strFileName = txtLogPath.Text & "\AdDoc" & tmpFROM & ".log"
    FileNumber = FreeFile   ' Get unused file number.

    Open strFileName For Output As #FileNumber ' Create file name.

    Print #FileNumber, "---------------------------------------------------------------"
    Print #FileNumber, "Update started at " & tmpFROM
    Print #FileNumber, "---------------------------------------------------------------"

    Close #FileNumber
    tab_AFTTAB.ResetContent
    tab_AFTTAB.Refresh
    
    strWhere = "where SEAS = '" & strSeasonSQ & "' and ALC2 = 'SQ' and ORG3 = 'SIN' and DES3 in (" & strAirport & ") and TTYP in (" & strNatureSQ & ")"
    'LoadData tab_AFTTAB, "where SEAS = '& strSeason & ' and ALC2 in (" & strAirline & ")"
    StatusBar1.Panels(1).Text = "Searching ...  "
    Debug.Print strWhere
    LoadData tab_AFTTAB, strWhere
    tab_AFTTAB.AutoSizeColumns
    tab_AFTTAB.Refresh
    
    
    incCnt = tab_AFTTAB.GetLineCount
    If Not CedaIsConnected Then UfisServer.ConnectToCeda
    For i = 0 To tab_AFTTAB.GetLineCount - 1 Step 1
        
        updateAction = False
        ' get field values
        strURNO = tab_AFTTAB.GetFieldValues(i, "URNO")
        strRKEY = tab_AFTTAB.GetFieldValues(i, "RKEY")
        strORG3 = tab_AFTTAB.GetFieldValues(i, "ORG3")
                
        MousePointer = vbHourglass
        ' kkh - check whether to update TGA1/TGA1
        If strORG3 = "SIN" Then
            updateAction = True
            strWhere = "where urno = " & strURNO & ""
            strFLD = "TGD1"
            strTerminalOri = tab_AFTTAB.GetFieldValues(i, "TGD1")
            strRKEY = tab_AFTTAB.GetFieldValues(i, "RKEY")
        End If
        
        If updateAction = True Then
            FileNumber = FreeFile   ' Get unused file number.
            Open strFileName For Append As #FileNumber    ' Create file name.
            
            updateStat = UfisServer.CallCeda(CedaDataAnswer, "URT", "AFTTAB", strFLD, strTerminal, strWhere, "", 0, False, False)
            If updateStat = 0 Then
                recordcnt = recordcnt + 1
                StatusBar1.Panels(1).Text = "Updating record : " & recordcnt
                Print #FileNumber, "URNO : " & strURNO & " | FLNO : " & tab_AFTTAB.GetFieldValues(i, "FLNO") & " | Update " & strFLD & " " & strTerminalOri & " TO "; strTerminal
            End If
            
            ' Update arrival flight with the RKEY
            strWhere = "where RKEY = " & strRKEY & " and DES3 = 'SIN' and ALC2 = 'SQ'"
            strFLD = "TGA1"
            updateStat = UfisServer.CallCeda(CedaDataAnswer, "URT", "AFTTAB", strFLD, strTerminal, strWhere, "", 0, False, False)
            If updateStat = 0 Then
                recordcnt = recordcnt + 1
                StatusBar1.Panels(1).Text = "Updating record : " & recordcnt
                Print #FileNumber, "URNO : " & strRKEY & " | FLNO : ------ | Update " & strFLD & " " & strTerminalOri & " TO "; strTerminal
            End If
            Close #FileNumber
        End If
    Next i
    If incCnt = 0 Then
        MsgBox "No record to update"
        Kill (strFileName)
        StatusBar1.Panels(1).Text = "Ready."
    Else
        FileNumber = FreeFile   ' Get unused file number.
        Open strFileName For Append As #FileNumber    ' Create file name.
        tmpTO = Format(Date, "YYYYMMDD") & Format(Time, "hhmmss")
        Print #FileNumber, "Total records Update : " & recordcnt
        Print #FileNumber, "---------------------------------------------------------------"
        Print #FileNumber, "Update finished at " & tmpTO
        Print #FileNumber, "---------------------------------------------------------------"
        Close #FileNumber   ' Close file.
        ClearForm False
        StatusBar1.Panels(1).Text = "Update finished, total no of records updated " & recordcnt
    End If
    MousePointer = vbDefault
    If CloseConnex Then UfisServer.DisconnectFromCeda
    DisconnetUfisCom
        
End Sub

'#######################################################################
'### FORM LOAD
'#######################################################################
Private Sub Form_Load()

    strHOPO = GetIniEntry("", "RMS", "GLOBAL", "HOMEAIRPORT", "XXX")
    strServer = GetIniEntry("", "RMS", "GLOBAL", "HOSTNAME", "XXX")
    strTableExt = GetIniEntry("", "RMS", "GLOBAL", "TABLEEXTENSION", "TAB")
    'strTableExt = "TAB"
    txtHopo.Text = strHOPO
    txtServer.Text = strServer
    
    InitTabGeneral tab_AFTTAB
    InitTabForCedaConnection tab_AFTTAB
    InitTabs
    
End Sub


'#######################################################################
'###
'#######################################################################

Public Sub SetPrivStat()
    Dim strPrivRet As String
    strPrivRet = AATLoginControl1.GetPrivileges("m_TAB_Configuration")

    If strPrivRet = "0" Or strPrivRet = "-" Then
        SSTab1.TabVisible(0) = False
    End If
End Sub

'#######################################################################
'###
'#######################################################################
Private Sub Form_Unload(Cancel As Integer)
    cmdExit_Click (0)
End Sub
Public Sub InitTabForCedaConnection(ByRef rTab As TABLib.Tab)
    'do the initializing for the CEDA-connection
    rTab.CedaServerName = strServer
    rTab.CedaPort = "3357"
    rTab.CedaHopo = strHOPO
    'rTab.CedaCurrentApplication = gsAppName
    rTab.CedaTabext = strTableExt
    'rTab.CedaUser = gsUserName
    'rTab.CedaWorkstation = frmMain.AATLoginControl1.GetWorkStationName
    rTab.CedaSendTimeout = "120"
    rTab.CedaReceiveTimeout = "240"
    rTab.CedaRecordSeparator = Chr(10)
    'rTab.CedaIdentifier = gsAppName
End Sub


Public Sub InitTabGeneral(ByRef rTab As TABLib.Tab)
    rTab.ResetContent
    rTab.FontName = "Courier New"
    rTab.HeaderFontSize = "18"
    rTab.FontSize = "16"
    rTab.EnableHeaderSizing True
    rTab.ShowHorzScroller True
    rTab.AutoSizeByHeader = True
    rTab.SetFieldSeparator ","

    Dim ilCnt As Integer
    Dim strFields As String
    Dim strHeaderLengthString As String
    GetKeyItem strFields, rTab.Tag, "{=FIELDS=}", "{="
    ilCnt = ItemCount(strFields, ",")
    For i = 1 To ilCnt Step 1
        strHeaderLengthString = strHeaderLengthString & "10,"
    Next i
    strHeaderLengthString = Left(strHeaderLengthString, Len(strHeaderLengthString) - 1)
    rTab.HeaderLengthString = strHeaderLengthString

    rTab.HeaderString = strFields
    rTab.LogicalFieldList = strFields

    rTab.EnableHeaderSizing True

    ' set style of highlighted line
    Dim strColors As String
    strColors = CStr(vbBlue) & "," & CStr(vbBlue)
    rTab.CursorDecoration rTab.LogicalFieldList, "B,T", "3,3", strColors
    rTab.DefaultCursor = False
End Sub


Private Sub ConnectUfisCom()
    UfisCom1.Module = "Terminal Appl"
    UfisCom1.HomeAirport = txtHopo.Text
    UfisCom1.ServerName = txtServer.Text
    UfisCom1.TableExt = "TAB"
    UfisCom1.UserName = GetWindowsUserName
    UfisCom1.WorkStation = GetWorkStationName
    UfisCom1.SetCedaPerameters GetWindowsUserName, txtHopo.Text, "TAB"
    UfisCom1.InitCom txtServer.Text, "CEDA"
End Sub

Private Sub DisconnetUfisCom()
    UfisCom1.CleanupCom
End Sub

Public Sub LoadData(ByRef rTab As TABLib.Tab, ByRef pWhere As String)
    Dim strCommand As String
    Dim strTable As String
    Dim strFieldList As String
    Dim strData As String
    Dim strWhere As String

    MousePointer = vbHourglass
    strCommand = "RT"
    rTab.ResetContent

    GetKeyItem strTable, rTab.Tag, "{=TABLE=}", "{="
    GetKeyItem strFieldList, rTab.Tag, "{=FIELDS=}", "{="
    strData = ""
    strWhere = pWhere
    If Len(strWhere) = 0 Then
        GetKeyItem strWhere, rTab.Tag, "{=WHERE=}", "{="
    End If
    If rTab.CedaAction(strCommand, strTable, strFieldList, strData, strWhere) = False Then
        MsgBox "Error loading " & strTable & "!" & vbCrLf & _
            "Field list: " & strFieldList & vbCrLf & _
            "Where statement: " & strWhere, vbCritical, "Error reading data (RT)"
            rTab.GetLastCedaError
    End If
    Debug.Print strFieldList
    MousePointer = vbDefault
End Sub

Public Sub ClearForm(reset As Boolean)
    
    cmbAirline.Clear
    txtFLNO.Text = ""
    txtSuffix.Text = ""
    DateFrom.Value = Now
    DateTo.Value = Now
    cmbTerminal.Clear
    cmbSeason.Clear
    cmbSeasonSQ.Clear
    
    ListAirlineTo.Clear
    ListNatureTo.Clear
    ListNatureToSQ.Clear
    ListAirportTo.Clear
    
    ListAirlineFrom.Clear
    ListNatureFrom.Clear
    ListNatureFromSQ.Clear
    ListAirportFrom.Clear

    If reset = True Then
        tab_AFTTAB.ResetContent
        tab_AFTTAB.Refresh
        StatusBar1.Panels(1).Text = "Ready."
    End If
    
    InitTabs
End Sub

