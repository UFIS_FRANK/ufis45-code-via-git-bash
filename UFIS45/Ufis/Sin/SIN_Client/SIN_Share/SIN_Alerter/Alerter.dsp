# Microsoft Developer Studio Project File - Name="Alerter" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=Alerter - Win32 THODebug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "Alerter.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Alerter.mak" CFG="Alerter - Win32 THODebug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Alerter - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "Alerter - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE "Alerter - Win32 THODebug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Alerter - Win32 Release"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "c:\Ufis_Bin\Release"
# PROP Intermediate_Dir "c:\Ufis_Intermediate\Alerter\Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /I ".\\" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "NDEBUG"
# ADD RSC /l 0x407 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 C:\Ufis_Bin\ClassLib\Release\CCSClass.lib C:\Ufis_Bin\Release\ufis32.lib WSock32.lib /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "Alerter - Win32 Debug"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "c:\Ufis_Bin\Debug"
# PROP Intermediate_Dir "c:\Ufis_Intermediate\Alerter\Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I ".\\" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "_DEBUG"
# ADD RSC /l 0x407 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 c:\Ufis_bin\Classlib\Debug\CCSClass.lib c:\Ufis_bin\Debug\ufis32.lib WSock32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ELSEIF  "$(CFG)" == "Alerter - Win32 THODebug"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Alerter___Win32_THODebug"
# PROP BASE Intermediate_Dir "Alerter___Win32_THODebug"
# PROP BASE Ignore_Export_Lib 0
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Alerter___Win32_THODebug"
# PROP Intermediate_Dir "Alerter___Win32_THODebug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I ".\\" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I ".\\" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x407 /d "_DEBUG"
# ADD RSC /l 0x407 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 c:\Ufis_bin\Classlib\Debug\CCSClass.lib c:\Ufis_bin\Debug\ufis32.lib WSock32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 c:\Ufis_bin\Classlib\Debug\CCSClass.lib c:\Ufis_bin\Debug\ufis32.lib WSock32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "Alerter - Win32 Release"
# Name "Alerter - Win32 Debug"
# Name "Alerter - Win32 THODebug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Wrapper\aatlogin.cpp
# End Source File
# Begin Source File

SOURCE=.\Alerter.cpp
# End Source File
# Begin Source File

SOURCE=.\Alerter.odl
# End Source File
# Begin Source File

SOURCE=.\Alerter.rc
# End Source File
# Begin Source File

SOURCE=.\AlerterDlg.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Wrapper\bcproxy.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgProxy.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgResizeHelper.cpp
# End Source File
# Begin Source File

SOURCE=.\STabCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Wrapper\tab.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Wrapper\trafficlight.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Wrapper\ufiscom.cpp
# End Source File
# Begin Source File

SOURCE=.\VersionInfo.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Wrapper\aatlogin.h
# End Source File
# Begin Source File

SOURCE=.\Alerter.h
# End Source File
# Begin Source File

SOURCE=.\AlerterDlg.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Wrapper\bcproxy.h
# End Source File
# Begin Source File

SOURCE=.\DlgProxy.h
# End Source File
# Begin Source File

SOURCE=.\DlgResizeHelper.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\STabCtrl.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Wrapper\tab.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Wrapper\trafficlight.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\_Standard\_Standard_Client\_Standard_Share\_Standard_Wrapper\ufiscom.h
# End Source File
# Begin Source File

SOURCE=.\VersionInfo.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\abb.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Alerter.ico
# End Source File
# Begin Source File

SOURCE=.\res\Alerter.rc2
# End Source File
# End Group
# Begin Source File

SOURCE=.\Alerter.reg
# End Source File
# End Target
# End Project
# Section Alerter : {D4058E75-5880-11D7-8009-00010215BFDE}
# 	2:21:DefaultSinkHeaderFile:_aatlogincontrol.h
# 	2:16:DefaultSinkClass:C_AATLoginControl
# End Section
# Section Alerter : {64E8E383-05E2-11D2-9B1D-9B0630BC8F12}
# 	2:5:Class:CTAB
# 	2:10:HeaderFile:tab.h
# 	2:8:ImplFile:tab.cpp
# End Section
# Section Alerter : {D58F51D1-5888-11D7-8009-00010215BFDE}
# 	2:21:DefaultSinkHeaderFile:_aatlogincontrol.h
# 	2:16:DefaultSinkClass:C_AATLoginControl
# End Section
# Section Alerter : {A2F31E95-C74F-11D3-A251-00500437F607}
# 	2:21:DefaultSinkHeaderFile:ufiscom.h
# 	2:16:DefaultSinkClass:CUfisCom
# End Section
# Section Alerter : {59BEC158-923C-11D5-9969-0000865098D4}
# 	2:21:DefaultSinkHeaderFile:amp_test.h
# 	2:16:DefaultSinkClass:CAmp_test
# End Section
# Section Alerter : {D4058E74-5880-11D7-8009-00010215BFDE}
# 	2:5:Class:C_AATLoginControl
# 	2:10:HeaderFile:_aatlogincontrol.h
# 	2:8:ImplFile:_aatlogincontrol.cpp
# End Section
# Section Alerter : {D58F51D0-5888-11D7-8009-00010215BFDE}
# 	2:5:Class:C_AATLoginControl
# 	2:10:HeaderFile:_aatlogincontrol.h
# 	2:8:ImplFile:_aatlogincontrol.cpp
# End Section
# Section Alerter : {A2F31E93-C74F-11D3-A251-00500437F607}
# 	2:5:Class:CUfisCom
# 	2:10:HeaderFile:ufiscom.h
# 	2:8:ImplFile:ufiscom.cpp
# End Section
# Section Alerter : {59BEC156-923C-11D5-9969-0000865098D4}
# 	2:5:Class:CAmp_test
# 	2:10:HeaderFile:amp_test.h
# 	2:8:ImplFile:amp_test.cpp
# End Section
# Section Alerter : {D58F51E3-5888-11D7-8009-00010215BFDE}
# 	2:21:DefaultSinkHeaderFile:_aatlogincontrol.h
# 	2:16:DefaultSinkClass:C_AATLoginControl
# End Section
# Section Alerter : {64E8E385-05E2-11D2-9B1D-9B0630BC8F12}
# 	2:21:DefaultSinkHeaderFile:tab.h
# 	2:16:DefaultSinkClass:CTAB
# End Section
# Section Alerter : {D58F51E2-5888-11D7-8009-00010215BFDE}
# 	2:5:Class:C_AATLoginControl
# 	2:10:HeaderFile:_aatlogincontrol.h
# 	2:8:ImplFile:_aatlogincontrol.cpp
# End Section
