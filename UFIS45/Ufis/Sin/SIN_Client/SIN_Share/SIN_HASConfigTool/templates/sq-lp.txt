LOADING INSTRUCTION/REPORT                  CHECKED         EDNO
 ALL WEIGHTS IN KILOS                                          1
 FROM/TO FLIGHT       A-C/REG  VERSION       CREW   DATE     TIME
 SIN DEL SQ0408       9VSQA    12/42/234     2/14   15AUG02  1136
 PLANNED LOAD
 DEL P  4 J 19 Y115 C  4068  M   283* B  4232
 JOINING SPECS
 LOADING INSTRUCTION                                       ACTUAL
 --------------------------------------------              WEIGHT
 CPT 1 FLF MAX 12696                       ::              IN KGS
                                           ----------------------
 :11L      AVE      SQ        :11R      AVE      SQ       :
 :ONLOAD DEL E/82             :ONLOAD DEL BY/418R M/283R
 :REPORT                      :REPORT
 :...............................................................
 :12L      AVE      SQ        :12R      AVE      SQ       D
 :ONLOAD DEL BJ/462R          :ONLOAD DEL BP/224R
 :REPORT                      :REPORT
 :...............................................................
