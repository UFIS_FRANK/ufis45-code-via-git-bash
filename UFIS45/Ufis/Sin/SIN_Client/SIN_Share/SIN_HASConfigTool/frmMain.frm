VERSION 5.00
Object = "{64E8E382-05E2-11D2-9B1D-9B0630BC8F12}#1.0#0"; "TAB.ocx"
Object = "{A2F31E92-C74F-11D3-A251-00500437F607}#1.0#0"; "UfisCom.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{7BDE1363-DF9A-4D96-A2A0-4E85E0191F0F}#1.0#0"; "AatLogin.ocx"
Begin VB.Form frmMain 
   Caption         =   "HASConfigTool"
   ClientHeight    =   12900
   ClientLeft      =   165
   ClientTop       =   450
   ClientWidth     =   10290
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   12900
   ScaleWidth      =   10290
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
   Begin TabDlg.SSTab SSTab1 
      Height          =   12465
      Left            =   75
      TabIndex        =   31
      Top             =   90
      Width           =   10065
      _ExtentX        =   17754
      _ExtentY        =   21987
      _Version        =   393216
      Tabs            =   2
      TabHeight       =   520
      TabCaption(0)   =   "Configuration"
      TabPicture(0)   =   "frmMain.frx":030A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "cmdSave"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "cmdExit(0)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Frame2"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Frame1"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "AATLoginControl1"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).ControlCount=   5
      TabCaption(1)   =   "Manual Input"
      TabPicture(1)   =   "frmMain.frx":0326
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame3"
      Tab(1).Control(1)=   "Frame4"
      Tab(1).Control(2)=   "cmdExit(1)"
      Tab(1).Control(3)=   "cmdSend2Server"
      Tab(1).Control(4)=   "CMDSendRequest"
      Tab(1).ControlCount=   5
      Begin AATLOGINLib.AatLogin AATLoginControl1 
         Height          =   1095
         Left            =   600
         TabIndex        =   66
         Top             =   480
         Visible         =   0   'False
         Width           =   1575
         _Version        =   65536
         _ExtentX        =   2778
         _ExtentY        =   1931
         _StockProps     =   0
      End
      Begin VB.CommandButton CMDSendRequest 
         Caption         =   "Send Request"
         Height          =   405
         Left            =   -70597
         Style           =   1  'Graphical
         TabIndex        =   65
         Top             =   10395
         Width           =   1440
      End
      Begin VB.CommandButton cmdSend2Server 
         Caption         =   "Send2Server"
         Height          =   405
         Left            =   -72232
         Style           =   1  'Graphical
         TabIndex        =   52
         Top             =   10395
         Width           =   1440
      End
      Begin VB.CommandButton cmdExit 
         Caption         =   "Exit"
         Height          =   405
         Index           =   1
         Left            =   -68977
         TabIndex        =   53
         Top             =   10395
         Width           =   1440
      End
      Begin VB.Frame Frame4 
         Caption         =   "Template"
         Height          =   4185
         Left            =   -74760
         TabIndex        =   51
         Top             =   5805
         Width           =   9630
         Begin TABLib.TAB TAB_COMMANDS 
            Height          =   3285
            Left            =   5910
            TabIndex        =   56
            Tag             =   "{=FIELDS=}Commands"
            Top             =   660
            Width           =   3480
            _Version        =   65536
            _ExtentX        =   6138
            _ExtentY        =   5794
            _StockProps     =   64
         End
         Begin TABLib.TAB TAB_AFT 
            Height          =   3285
            Left            =   330
            TabIndex        =   54
            Tag             =   "{=FIELDS=}Flight,STD,DEST,S"
            Top             =   675
            Width           =   5010
            _Version        =   65536
            _ExtentX        =   8837
            _ExtentY        =   5794
            _StockProps     =   64
         End
         Begin VB.Label Label15 
            Caption         =   "Command:"
            Height          =   225
            Left            =   5895
            TabIndex        =   57
            Top             =   300
            Width           =   2490
         End
         Begin VB.Label Label14 
            Caption         =   "Flight:"
            Height          =   225
            Left            =   345
            TabIndex        =   55
            Top             =   315
            Width           =   2490
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Screen content"
         Height          =   5055
         Left            =   -74760
         TabIndex        =   49
         Top             =   525
         Width           =   9630
         Begin VB.TextBox txtScreen 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   4440
            Left            =   255
            MultiLine       =   -1  'True
            ScrollBars      =   3  'Both
            TabIndex        =   50
            Top             =   360
            Width           =   9150
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Authorisation settings"
         Height          =   5370
         Left            =   240
         TabIndex        =   43
         Top             =   480
         Width           =   9630
         Begin VB.TextBox mailList 
            Height          =   285
            Left            =   2280
            TabIndex        =   70
            Top             =   4800
            Width           =   6495
         End
         Begin VB.TextBox expDate 
            Height          =   285
            Left            =   2280
            TabIndex        =   69
            Top             =   4320
            Width           =   1455
         End
         Begin VB.TextBox txtPASW 
            Height          =   285
            Left            =   255
            TabIndex        =   2
            Top             =   3720
            Width           =   1755
         End
         Begin VB.ComboBox cmbWildcard 
            Height          =   315
            Left            =   7935
            TabIndex        =   5
            Text            =   "Combo1"
            Top             =   3495
            Width           =   1455
         End
         Begin VB.CommandButton cmdHATTAB 
            Caption         =   "Insert Wildcard:"
            Height          =   405
            Index           =   3
            Left            =   7965
            TabIndex        =   6
            Top             =   2955
            Width           =   1440
         End
         Begin VB.TextBox txtLGOU 
            Height          =   1035
            Left            =   5145
            MultiLine       =   -1  'True
            ScrollBars      =   3  'Both
            TabIndex        =   4
            Text            =   "frmMain.frx":0342
            Top             =   2970
            Width           =   2520
         End
         Begin VB.TextBox txtNAME 
            Height          =   285
            Left            =   255
            TabIndex        =   1
            Top             =   2985
            Width           =   1755
         End
         Begin VB.CommandButton cmdHATTAB 
            Caption         =   "Delete"
            Height          =   405
            Index           =   2
            Left            =   7950
            TabIndex        =   9
            Top             =   1290
            Width           =   1440
         End
         Begin VB.CommandButton cmdHATTAB 
            Caption         =   "Update"
            Height          =   405
            Index           =   1
            Left            =   7950
            TabIndex        =   8
            Top             =   840
            Width           =   1440
         End
         Begin VB.CommandButton cmdHATTAB 
            Caption         =   "Insert"
            Height          =   405
            Index           =   0
            Left            =   7980
            TabIndex        =   7
            Top             =   345
            Width           =   1440
         End
         Begin VB.TextBox txtLGIN 
            Height          =   1035
            Left            =   2310
            MultiLine       =   -1  'True
            ScrollBars      =   3  'Both
            TabIndex        =   3
            Text            =   "frmMain.frx":034C
            Top             =   2970
            Width           =   2520
         End
         Begin TABLib.TAB TAB_AUTHORISATION 
            Height          =   2265
            Left            =   240
            TabIndex        =   44
            TabStop         =   0   'False
            Tag             =   "{=FIELDS=}Name,Password,Login,Logout,Expiry Date,Mailing List"
            Top             =   375
            Width           =   7425
            _Version        =   65536
            _ExtentX        =   13097
            _ExtentY        =   3995
            _StockProps     =   64
         End
         Begin VB.Label Label24 
            Caption         =   "Mail List (Separator ; )"
            Height          =   255
            Left            =   240
            TabIndex        =   68
            Top             =   4800
            Width           =   1935
         End
         Begin VB.Label Label20 
            Caption         =   "Expiry Date (yyyymmdd) :"
            Height          =   375
            Left            =   240
            TabIndex        =   67
            Top             =   4320
            Width           =   1815
         End
         Begin VB.Label Label4 
            Caption         =   "Password:"
            Height          =   255
            Left            =   255
            TabIndex        =   48
            Top             =   3420
            Width           =   1830
         End
         Begin VB.Label Label3 
            Caption         =   "Logout:"
            Height          =   255
            Left            =   5145
            TabIndex        =   47
            Top             =   2730
            Width           =   1830
         End
         Begin VB.Label Label2 
            Caption         =   "Login:"
            Height          =   255
            Left            =   2310
            TabIndex        =   46
            Top             =   2730
            Width           =   1830
         End
         Begin VB.Label Label1 
            Caption         =   "Name:"
            Height          =   255
            Left            =   255
            TabIndex        =   45
            Top             =   2730
            Width           =   1830
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Configuration settings"
         Height          =   5580
         Left            =   225
         TabIndex        =   32
         Top             =   6000
         Width           =   9600
         Begin VB.TextBox txtLPGC 
            Height          =   315
            Left            =   2760
            TabIndex        =   24
            ToolTipText     =   "Column Number for ""Keywords Last Page"""
            Top             =   5115
            Width           =   600
         End
         Begin VB.TextBox txtLPGL 
            Height          =   315
            Left            =   1995
            TabIndex        =   23
            ToolTipText     =   "Line Number for ""Keywords Last Page"""
            Top             =   5115
            Width           =   600
         End
         Begin VB.TextBox txtLPGK 
            Height          =   315
            Left            =   3525
            TabIndex        =   25
            ToolTipText     =   "Keywords to identify a ""Last Page"""
            Top             =   5115
            Width           =   5865
         End
         Begin VB.TextBox txtNPGC 
            Height          =   315
            Left            =   2760
            TabIndex        =   21
            ToolTipText     =   "Column Number for ""Keywords Next Page"""
            Top             =   4500
            Width           =   600
         End
         Begin VB.TextBox txtNPGL 
            Height          =   315
            Left            =   1995
            TabIndex        =   20
            ToolTipText     =   "Line Number for ""Keywords Next Page"""
            Top             =   4500
            Width           =   600
         End
         Begin VB.TextBox txtNPGK 
            Height          =   315
            Left            =   3525
            TabIndex        =   22
            ToolTipText     =   "Keywords to identify a ""Next Page"""
            Top             =   4500
            Width           =   5865
         End
         Begin VB.TextBox txtCMDN 
            Height          =   315
            Left            =   240
            TabIndex        =   19
            ToolTipText     =   "Command for ""Next Page"""
            Top             =   4500
            Width           =   1665
         End
         Begin VB.ComboBox cmbACTN 
            Height          =   315
            Left            =   7515
            TabIndex        =   16
            Text            =   "Combo1"
            ToolTipText     =   "Select the ACTN of the command: 'START' or 'REMV'."
            Top             =   3105
            Width           =   1305
         End
         Begin VB.TextBox txtEVNT 
            Height          =   315
            Left            =   6690
            TabIndex        =   15
            Text            =   "LPF"
            ToolTipText     =   "Enter the event for this command, e.g.'FC','LPF'."
            Top             =   3120
            Width           =   690
         End
         Begin VB.TextBox txtSERL 
            Height          =   315
            Left            =   6060
            TabIndex        =   14
            Text            =   "1"
            ToolTipText     =   "Enter the sequence number of this comand to order them."
            Top             =   3120
            Width           =   480
         End
         Begin VB.ComboBox cmbSTAT 
            Height          =   315
            Left            =   4605
            TabIndex        =   13
            Text            =   "Combo1"
            ToolTipText     =   "Select the status of the command: 'ACTIVE' or 'CANCELLED'."
            Top             =   3120
            Width           =   1305
         End
         Begin VB.TextBox txtTIHR 
            Height          =   315
            Left            =   4620
            TabIndex        =   18
            Text            =   "72,48,24,12,6,3,2"
            ToolTipText     =   "Enter the hours the timer event should occour, e.g. '72,48,24,12,6,3,2'."
            Top             =   3735
            Width           =   4200
         End
         Begin VB.TextBox txtTIMI 
            Height          =   315
            Left            =   240
            TabIndex        =   17
            Text            =   "70,60,50,40,30,25,20,15,10,5,ATD"
            ToolTipText     =   "Enter the minutes the timer event should occour, e.g. '70,60,50,40,30,25,20,15,10,5,ATD'."
            Top             =   3735
            Width           =   4200
         End
         Begin VB.TextBox txtCOMD 
            Height          =   315
            Left            =   2400
            TabIndex        =   12
            Text            =   "KWDSQ--/date/SIN"
            ToolTipText     =   "Enter a command, e.g. '@P/IN'."
            Top             =   3120
            Width           =   2025
         End
         Begin VB.ComboBox cmbGRPT 
            Height          =   315
            Left            =   885
            TabIndex        =   11
            Text            =   "Combo1"
            ToolTipText     =   "Select a group the airline belongs to."
            Top             =   3120
            Width           =   1305
         End
         Begin VB.TextBox txtALC2 
            Height          =   315
            Left            =   240
            TabIndex        =   10
            Text            =   "SQ"
            ToolTipText     =   "Enter 2-letter airline-code, e.g. 'SQ'"
            Top             =   3120
            Width           =   420
         End
         Begin VB.CommandButton cmdHCFTAB 
            Caption         =   "Insert"
            Height          =   405
            Index           =   0
            Left            =   7950
            TabIndex        =   26
            Top             =   315
            Width           =   1440
         End
         Begin VB.CommandButton cmdHCFTAB 
            Caption         =   "Update"
            Height          =   405
            Index           =   1
            Left            =   7935
            TabIndex        =   27
            Top             =   780
            Width           =   1440
         End
         Begin VB.CommandButton cmdHCFTAB 
            Caption         =   "Delete"
            Height          =   405
            Index           =   2
            Left            =   7935
            TabIndex        =   28
            Top             =   1260
            Width           =   1440
         End
         Begin TABLib.TAB TAB_CONFIGURATION 
            Height          =   2400
            Left            =   255
            TabIndex        =   33
            TabStop         =   0   'False
            Tag             =   $"frmMain.frx":037A
            Top             =   360
            Width           =   7425
            _Version        =   65536
            _ExtentX        =   13097
            _ExtentY        =   4233
            _StockProps     =   64
         End
         Begin VB.Label Label23 
            Caption         =   "ColNo:"
            Height          =   240
            Left            =   2760
            TabIndex        =   64
            Top             =   4875
            Width           =   615
         End
         Begin VB.Label Label22 
            Caption         =   "LineNo:"
            Height          =   240
            Left            =   1995
            TabIndex        =   63
            Top             =   4875
            Width           =   825
         End
         Begin VB.Label Label21 
            Caption         =   "Keywords to identify last page:"
            Height          =   240
            Left            =   3525
            TabIndex        =   62
            Top             =   4875
            Width           =   2910
         End
         Begin VB.Label Label19 
            Caption         =   "ColNo:"
            Height          =   240
            Left            =   2760
            TabIndex        =   61
            Top             =   4260
            Width           =   615
         End
         Begin VB.Label Label18 
            Caption         =   "LineNo:"
            Height          =   240
            Left            =   1995
            TabIndex        =   60
            Top             =   4260
            Width           =   825
         End
         Begin VB.Label Label17 
            Caption         =   "Keywords to identify next page:"
            Height          =   240
            Left            =   3525
            TabIndex        =   59
            Top             =   4260
            Width           =   2910
         End
         Begin VB.Label Label16 
            Caption         =   "Cmd next page:"
            Height          =   240
            Left            =   240
            TabIndex        =   58
            Top             =   4260
            Width           =   1560
         End
         Begin VB.Line Line1 
            X1              =   105
            X2              =   9495
            Y1              =   4155
            Y2              =   4155
         End
         Begin VB.Label Label13 
            Caption         =   "ACTN:"
            Height          =   330
            Left            =   7515
            TabIndex        =   42
            Top             =   2820
            Width           =   1275
         End
         Begin VB.Label Label12 
            Caption         =   "Event:"
            Height          =   180
            Left            =   6720
            TabIndex        =   41
            Top             =   2835
            Width           =   555
         End
         Begin VB.Label Label11 
            Caption         =   "Seq.:"
            Height          =   240
            Left            =   6060
            TabIndex        =   40
            Top             =   2835
            Width           =   465
         End
         Begin VB.Label Label10 
            Caption         =   "Status:"
            Height          =   330
            Left            =   4605
            TabIndex        =   39
            Top             =   2835
            Width           =   1275
         End
         Begin VB.Label Label9 
            Caption         =   "Timer hours:"
            Height          =   225
            Left            =   4605
            TabIndex        =   38
            Top             =   3495
            Width           =   1470
         End
         Begin VB.Label Label8 
            Caption         =   "Timer minutes:"
            Height          =   225
            Left            =   240
            TabIndex        =   37
            Top             =   3495
            Width           =   1470
         End
         Begin VB.Label Label7 
            Caption         =   "Command:"
            Height          =   225
            Left            =   2385
            TabIndex        =   36
            Top             =   2835
            Width           =   990
         End
         Begin VB.Label Label6 
            Caption         =   "Group type:"
            Height          =   240
            Left            =   885
            TabIndex        =   35
            Top             =   2835
            Width           =   1350
         End
         Begin VB.Label Label5 
            Caption         =   "ALC2:"
            Height          =   240
            Left            =   240
            TabIndex        =   34
            Top             =   2835
            Width           =   825
         End
      End
      Begin VB.CommandButton cmdExit 
         Caption         =   "Exit"
         Height          =   405
         Index           =   0
         Left            =   5205
         TabIndex        =   30
         Top             =   11760
         Width           =   1440
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "Save"
         Height          =   405
         Left            =   3585
         Style           =   1  'Graphical
         TabIndex        =   29
         Top             =   11760
         Width           =   1440
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   300
      Left            =   0
      TabIndex        =   0
      Top             =   12600
      Width           =   10290
      _ExtentX        =   18150
      _ExtentY        =   529
      SimpleText      =   "Ready."
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   3
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   12515
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   6
            TextSave        =   "7/4/2008"
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Style           =   5
            Alignment       =   2
            TextSave        =   "10:17 AM"
         EndProperty
      EndProperty
   End
   Begin UFISCOMLib.UfisCom UfisCom1 
      Left            =   0
      Top             =   0
      _Version        =   65536
      _ExtentX        =   1111
      _ExtentY        =   1164
      _StockProps     =   0
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'One function modified to accept 2 more input fields - Thanooj 20060605
'Functions is 1.GetHATTAB_String

Option Explicit

Private ilLastTxtFocus As Integer
Private ilSelStart As Integer
Private strHATTAB_Sepa As String
Private strHCFTAB_Sepa As String

Private Sub cmdExit_Click(Index As Integer)
    If cmdSave.BackColor = vbRed Then
        Dim ilRet As Integer
        ilRet = MsgBox("Do you want to save your changes? If not, they will be lost!", vbYesNo, "Save...?")
        If ilRet = vbYes Then
            cmdSave_Click
        End If
    End If

    Unload frmData
    End
End Sub

Private Sub CMDSendRequest_Click()
    Dim ilCurrSelCmd As Integer
    Dim ilCurrSelFlight As Integer
    
    ilCurrSelCmd = TAB_COMMANDS.GetCurrentSelected
    ilCurrSelFlight = TAB_AFT.GetCurrentSelected

    If ilCurrSelCmd < 0 Or ilCurrSelFlight < 0 Then
        MsgBox "Please select a command and a flight!", vbCritical, "No selection"
    Else
        Dim strCommand As String
        Dim strURNO As String
        strCommand = TAB_COMMANDS.GetColumnValue(CLng(ilCurrSelCmd), 0)
        GetKeyItem strURNO, TAB_AFT.GetLineTag(CLng(ilCurrSelFlight)), "{=URNO=}", "{="
        frmData.SendToServer "", strCommand, strURNO
    End If
End Sub

Private Sub cmdSend2Server_Click()
    Dim ilCurrSelCmd As Integer
    Dim ilCurrSelFlight As Integer
    
    ilCurrSelCmd = TAB_COMMANDS.GetCurrentSelected
    ilCurrSelFlight = TAB_AFT.GetCurrentSelected

    If ilCurrSelCmd < 0 Or ilCurrSelFlight < 0 Then
        MsgBox "Please select a command and a flight!", vbCritical, "No selection"
    Else
        If Len(txtScreen.Text) = 0 Then
            MsgBox "The template and/or the screen is empty! Sending aborted.", vbCritical, "Empty screen"
        Else
            Dim strCommand As String
            Dim strURNO As String
            strCommand = TAB_COMMANDS.GetColumnValue(CLng(ilCurrSelCmd), 0)
            GetKeyItem strURNO, TAB_AFT.GetLineTag(CLng(ilCurrSelFlight)), "{=URNO=}", "{="
            frmData.SendToServer txtScreen.Text, strCommand, strURNO
        End If
    End If
End Sub

Private Sub Form_Load()
    ilLastTxtFocus = 0
    strHATTAB_Sepa = ","
    strHCFTAB_Sepa = Chr(15)

    InitHATTAB
    InitHCFTAB
    InitAFTTAB
    IniTabCommands

    InitCombos
End Sub

Public Sub RefreshView()
    RefreshConfigurationTab
    RefreshManualInputTab
End Sub

Public Sub SetPrivStat()
    Dim strPrivRet As String
    strPrivRet = AATLoginControl1.GetPrivileges("m_TAB_Configuration")

    If strPrivRet = "0" Or strPrivRet = "-" Then
        SSTab1.TabVisible(0) = False
    End If
End Sub

Private Sub RefreshConfigurationTab()
    Dim ilLastSel As Integer

    ilLastSel = TAB_AUTHORISATION.GetCurrentSelected

    GenerateConfigurationTabViewData

    TAB_AUTHORISATION_SendLButtonClick CLng(ilLastSel), 0

    TAB_AUTHORISATION.AutoSizeColumns
    TAB_CONFIGURATION.AutoSizeColumns

    TAB_AUTHORISATION.RedrawTab
    TAB_CONFIGURATION.RedrawTab
    Me.Refresh
End Sub

Private Sub GenerateConfigurationTabViewData()
    Dim ilLastSel As Integer
    Dim ilVScrollPos As Integer
    Dim strUHAT As String
    Dim i As Integer
    Dim strTmp As String

    ilLastSel = TAB_AUTHORISATION.GetCurrentSelected
    ilVScrollPos = TAB_AUTHORISATION.GetVScrollPos
    GetKeyItem strUHAT, TAB_AUTHORISATION.GetLineTag(CLng(ilLastSel)), "{=URNO=}", "{="

    frmData.GenerateView_HATTAB TAB_AUTHORISATION
    frmData.GenerateView_HCFTAB TAB_CONFIGURATION, strUHAT

    TAB_AUTHORISATION.Sort 0, True, True
    TAB_CONFIGURATION.Sort 0, True, True
    
    If Len(strUHAT) > 0 Then
        For i = 0 To TAB_AUTHORISATION.GetLineCount - 1 Step 1
            GetKeyItem strTmp, TAB_AUTHORISATION.GetLineTag(CLng(i)), "{=URNO=}", "{="
            If strTmp = strUHAT Then
                TAB_AUTHORISATION.SetCurrentSelection CLng(i)
                Exit For
            End If
        Next i
    End If

    If ilVScrollPos <= ilLastSel Then
        TAB_AUTHORISATION.OnVScrollTo ilVScrollPos
    End If
    TAB_CONFIGURATION_SendLButtonClick -1, 0
End Sub

Private Sub cmdHATTAB_Click(Index As Integer)
    Select Case (Index)
    Case 0:
        InsertHATTAB
    Case 1:
        UpdateHATTAB
    Case 2:
        DeleteHATTAB
    Case 3:
        InsertWildcard
    End Select
    TAB_AUTHORISATION.Sort 0, True, True
End Sub

Private Sub cmdSave_Click()
    Me.MousePointer = vbHourglass

    frmData.InternalSaveHATTAB TAB_AUTHORISATION
    frmData.InternalSaveHCFTAB TAB_CONFIGURATION
    frmData.UpdateDB
    cmdSave.BackColor = vbButtonFace
    TAB_AUTHORISATION.RedrawTab
    TAB_CONFIGURATION.RedrawTab

    Me.MousePointer = vbDefault
End Sub

Private Sub Form_Unload(Cancel As Integer)
    cmdExit_Click (0)
End Sub

Private Sub TAB_AFT_BoolPropertyChanged(ByVal LineNo As Long, ByVal ColNo As Long, ByVal NewValue As Boolean)
    Dim strLineValues As String
    Dim strURNO As String
    Dim ilCntActive As Integer
    Dim ilCntCancelled As Integer
    Dim strSTAT As String

    strLineValues = TAB_AFT.GetLineValues(LineNo)
    GetKeyItem strURNO, TAB_AFT.GetLineTag(LineNo), "{=URNO=}", "{="
    strSTAT = frmData.GetSevtabStat(strURNO, ilCntActive, ilCntCancelled)

    If ilCntActive = 0 And ilCntCancelled = 0 Then
        strLineValues = Left(strLineValues, Len(strLineValues) - 2)
        TAB_AFT.DeleteLine LineNo
        TAB_AFT.InsertTextLine strLineValues, False
        TAB_AFT.Sort 1, True, True
        TAB_AFT.SetCurrentSelection LineNo
    Else
        If NewValue = False Then
            strSTAT = "CANCELLED"
        Else
            strSTAT = "ACTIVE"
        End If
        frmData.UpdateSevtab strURNO, strSTAT
    End If
End Sub

Private Sub TAB_AFT_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo < 0 Then
        TAB_AFT.SetCurrentSelection -1
        frmData.GenerateView_COMMANDS TAB_COMMANDS, "NOTHING"
    Else
        Dim strALC2 As String
        strALC2 = Trim(Left(TAB_AFT.GetColumnValue(LineNo, 0), 3))
        frmData.GenerateView_COMMANDS TAB_COMMANDS, strALC2
    End If
    TAB_COMMANDS.RedrawTab
End Sub

Private Sub TAB_AUTHORISATION_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    If cmdSave.BackColor = vbRed Then
        Dim ilRet As Integer
        ilRet = MsgBox("Do you want to save your changes? If not, they will be lost!", vbYesNo, "Save...?")
        If ilRet = vbYes Then
            cmdSave_Click
        Else
            cmdSave.BackColor = vbButtonFace
        End If
    End If

    If LineNo < 0 Then
        TAB_AUTHORISATION.SetCurrentSelection -1
        txtNAME.Text = ""
        txtPASW.Text = ""
        txtLGIN.Text = ""
        txtLGOU.Text = ""
        expDate.Text = ""
        mailList.Text = ""
    Else
        'Fields: Name,Password,Login,Logout
        Dim strTmp As String
        txtNAME.Text = TAB_AUTHORISATION.GetColumnValue(LineNo, 0)
        txtPASW.Text = TAB_AUTHORISATION.GetColumnValue(LineNo, 1)
        strTmp = TAB_AUTHORISATION.GetColumnValue(LineNo, 2)
        txtLGIN.Text = Replace(strTmp, Chr(180), vbCrLf)
        strTmp = TAB_AUTHORISATION.GetColumnValue(LineNo, 3)
        txtLGOU.Text = Replace(strTmp, Chr(180), vbCrLf)
        expDate.Text = TAB_AUTHORISATION.GetColumnValue(LineNo, 4)
        mailList.Text = TAB_AUTHORISATION.GetColumnValue(LineNo, 5)
    End If
    GenerateConfigurationTabViewData
    TAB_CONFIGURATION.AutoSizeColumns
    TAB_CONFIGURATION.RedrawTab
End Sub

Private Sub TAB_COMMANDS_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo < 0 Then
        TAB_COMMANDS.SetCurrentSelection -1
    Else
        Dim strPath As String
        GetKeyItem strPath, TAB_COMMANDS.GetLineTag(LineNo), "{=FULL_TEMPLATE_PATH=}", "{="
        frmData.LoadTemplateFile txtScreen, strPath
    End If
End Sub

Private Sub TAB_CONFIGURATION_SendLButtonClick(ByVal LineNo As Long, ByVal ColNo As Long)
    If LineNo < 0 Then
        TAB_CONFIGURATION.SetCurrentSelection -1
        txtSERL.Text = ""
        txtALC2.Text = ""
        cmbGRPT.Text = ""
        txtCOMD.Text = ""
        txtEVNT.Text = ""
        txtTIMI.Text = ""
        txtTIHR.Text = ""
        cmbSTAT.Text = ""
        cmbACTN.Text = ""
        txtCMDN.Text = ""
        txtNPGK.Text = ""
        txtNPGL.Text = ""
        txtNPGC.Text = ""
        txtLPGK.Text = ""
        txtLPGL.Text = ""
        txtLPGC.Text = ""
    Else
        Dim strTmp As String
        txtSERL.Text = TAB_CONFIGURATION.GetColumnValue(LineNo, 0)
        txtALC2.Text = TAB_CONFIGURATION.GetColumnValue(LineNo, 1)
        cmbGRPT.Text = TAB_CONFIGURATION.GetColumnValue(LineNo, 2)
        txtCOMD.Text = TAB_CONFIGURATION.GetColumnValue(LineNo, 3)
        txtEVNT.Text = TAB_CONFIGURATION.GetColumnValue(LineNo, 4)
        txtTIMI.Text = TAB_CONFIGURATION.GetColumnValue(LineNo, 5)
        txtTIHR.Text = TAB_CONFIGURATION.GetColumnValue(LineNo, 6)
        cmbSTAT.Text = TAB_CONFIGURATION.GetColumnValue(LineNo, 7)
        cmbACTN.Text = TAB_CONFIGURATION.GetColumnValue(LineNo, 8)
        txtCMDN.Text = TAB_CONFIGURATION.GetColumnValue(LineNo, 9)
        txtNPGL.Text = TAB_CONFIGURATION.GetColumnValue(LineNo, 10)
        txtNPGC.Text = TAB_CONFIGURATION.GetColumnValue(LineNo, 11)
        txtNPGK.Text = TAB_CONFIGURATION.GetColumnValue(LineNo, 12)
        txtLPGL.Text = TAB_CONFIGURATION.GetColumnValue(LineNo, 13)
        txtLPGC.Text = TAB_CONFIGURATION.GetColumnValue(LineNo, 14)
        txtLPGK.Text = TAB_CONFIGURATION.GetColumnValue(LineNo, 15)
        '"1|SEPA|BA|SEPA|GRP2|SEPA|@I|SEPA|FC|SEPA|90,75,60,50,40,30|SEPA|72,48,24,12,6,3,2|SEPA|ACTIVE|SEPA|START|SEPA|@D|SEPA|99|SEPA|99|SEPA|)|SEPA||SEPA||SEPA|"
    End If
End Sub

Private Sub txtLGIN_LostFocus()
    ilSelStart = txtLGIN.SelStart
    ilLastTxtFocus = 1
End Sub

Private Sub txtLGOU_LostFocus()
    ilSelStart = txtLGOU.SelStart
    ilLastTxtFocus = 2
End Sub

Private Sub txtCOMD_LostFocus()
    ilSelStart = txtCOMD.SelStart
    ilLastTxtFocus = 3
End Sub

Private Sub InitCombos()
    AddConfigEntriesToCombo cmbACTN, "HCFTAB.ACTN", "START,REMV"

    AddConfigEntriesToCombo cmbGRPT, "HCFTAB.GRPT", "GRP1,GRP2,GRP3"

    AddConfigEntriesToCombo cmbSTAT, "HCFTAB.STAT", "ACTIVE,CANCELLED"

    AddConfigEntriesToCombo cmbWildcard, "WILDCARDS", ",{=USER=},{=PASS=},{=DATE=},{=FLNO=}"
End Sub

Private Sub AddConfigEntriesToCombo(ByRef rCombo As ComboBox, ByRef rKey As String, ByRef rDefault As String)
    Dim ilCnt As Integer
    Dim i As Integer
    Dim strLineNo As Integer
    Dim strItems As String
    Dim strTmp As String

    strTmp = frmData.tab_CONFIG.GetLinesByColumnValue(0, rKey, 0)
    If IsNumeric(strTmp) = True Then
        strItems = frmData.tab_CONFIG.GetColumnValue(CLng(strTmp), 1)
    Else
        strItems = rDefault
    End If

    ilCnt = ItemCount(strItems, ",")
    For i = 1 To ilCnt Step 1
        rCombo.AddItem GetItem(strItems, i, ",")
    Next i
    rCombo.Text = ""
End Sub

Private Sub InsertWildcard()
    If ilLastTxtFocus = 0 Then
        MsgBox "Please select the 'Login' or the 'Logout' field first to add the wildcard!", vbInformation
    Else
        Dim strText As String
        Dim strTmp As String
        Select Case ilLastTxtFocus
            Case 1:
                strText = txtLGIN.Text
                strTmp = Left(strText, ilSelStart) & cmbWildcard.Text & Mid(strText, ilSelStart + 1)
                txtLGIN.Text = strTmp

            Case 2:
                strText = txtLGOU.Text
                strTmp = Left(strText, ilSelStart) & cmbWildcard.Text & Mid(strText, ilSelStart + 1)
                txtLGOU.Text = strTmp

            Case 3:
                strText = txtCOMD.Text
                strTmp = Left(strText, ilSelStart) & cmbWildcard.Text & Mid(strText, ilSelStart + 1)
                txtCOMD.Text = strTmp
        End Select
    End If
End Sub

Private Sub InitHATTAB()
    TAB_AUTHORISATION.ResetContent
    TAB_AUTHORISATION.FontName = "Courier New"
    TAB_AUTHORISATION.HeaderFontSize = "18"
    TAB_AUTHORISATION.FontSize = "16"
    TAB_AUTHORISATION.EnableHeaderSizing True
    TAB_AUTHORISATION.ShowHorzScroller True
    TAB_AUTHORISATION.AutoSizeByHeader = True
    TAB_AUTHORISATION.SetFieldSeparator strHATTAB_Sepa
    TAB_AUTHORISATION.SetTabFontBold True

    Dim strFields As String
    GetKeyItem strFields, TAB_AUTHORISATION.Tag, "{=FIELDS=}", "{="
    TAB_AUTHORISATION.HeaderLengthString = "10,10,10,10,10,10"
    strFields = Replace(strFields, ",", strHATTAB_Sepa)
    TAB_AUTHORISATION.HeaderString = strFields
    TAB_AUTHORISATION.AutoSizeColumns
End Sub

Private Sub InsertHATTAB()
    'Fields: Name,Password,Login,Logout
    Dim strInsert As String
    Dim strURNO As String

    strInsert = GetHATTAB_String()
    strURNO = frmData.GetUrno()

    TAB_AUTHORISATION.InsertTextLine strInsert, False
    TAB_AUTHORISATION.SetLineColor TAB_AUTHORISATION.GetLineCount - 1, vbBlack, vbGreen
    TAB_AUTHORISATION.AutoSizeColumns
    TAB_AUTHORISATION.RedrawTab

    TAB_AUTHORISATION.SetLineTag TAB_AUTHORISATION.GetLineCount - 1, "{=URNO=}" & strURNO
    cmdSave.BackColor = vbRed
End Sub

Private Sub UpdateHATTAB()
    Dim llRow As Long
    llRow = TAB_AUTHORISATION.GetCurrentSelected
    If llRow < 0 Or llRow > TAB_AUTHORISATION.GetLineCount Then
        MsgBox "Please select an authorization record first!", vbInformation
    Else
        Dim strUpdate As String
        strUpdate = GetHATTAB_String()
        TAB_AUTHORISATION.UpdateTextLine llRow, strUpdate, False
        TAB_AUTHORISATION.SetLineColor llRow, vbBlack, vbGreen
        TAB_AUTHORISATION.AutoSizeColumns
        TAB_AUTHORISATION.RedrawTab
        cmdSave.BackColor = vbRed
    End If
End Sub

Private Sub DeleteHATTAB()
    Dim llRow As Long
    llRow = TAB_AUTHORISATION.GetCurrentSelected
    If llRow < 0 Or llRow > TAB_AUTHORISATION.GetLineCount Then
        MsgBox "Please select a record first!", vbInformation
    Else
        Dim ilRet As Integer
        ilRet = MsgBox("Are you sure to delete this entry? The corresponding configurations will be deleted too!", vbYesNo)
        If ilRet = vbYes Then
            Dim strURNO As String
            GetKeyItem strURNO, TAB_AUTHORISATION.GetLineTag(llRow), "{=URNO=}", "{="
            frmData.DeleteInternal frmData.tab_HATTAB, strURNO
            TAB_AUTHORISATION.DeleteLine llRow
            TAB_AUTHORISATION.AutoSizeColumns
            TAB_AUTHORISATION.RedrawTab
            cmdSave.BackColor = vbRed
        End If
    End If
End Sub

'This function changed to accept 2 more input fields.. Thanooj - Date: 20060605

Private Function GetHATTAB_String() As String
    Dim strRet As String
    Dim strTmp As String

    strRet = txtNAME.Text & strHATTAB_Sepa & txtPASW.Text & strHATTAB_Sepa

    strTmp = txtLGIN.Text
    strTmp = Replace(strTmp, vbCrLf, Chr(180))
    If Right(strTmp, 2) = Chr(180) Then
        strTmp = Left(strTmp, Len(strTmp) - 2)
    End If
    strRet = strRet & strTmp & strHATTAB_Sepa

    strTmp = txtLGOU.Text
    strTmp = Replace(strTmp, vbCrLf, Chr(180))
    If Right(strTmp, 2) = Chr(180) Then
        strTmp = Left(strTmp, Len(strTmp) - 2)
    End If
    strRet = strRet & strTmp
    
    'Below line added  .. Thanooj
    strRet = strRet & strHATTAB_Sepa & expDate.Text & strHATTAB_Sepa & mailList.Text
    

    GetHATTAB_String = strRet
End Function


'
' -------------------------------------------------------------------------
' start handling concerning HCFTAB
' -------------------------------------------------------------------------
'

Private Sub cmdHCFTAB_Click(Index As Integer)
    Select Case (Index)
    Case 0:
        InsertHCFTAB
    Case 1:
        UpdateHCFTAB
    Case 2:
        DeleteHCFTAB
    End Select
    TAB_CONFIGURATION.Sort "0", True, True
End Sub

Private Sub InitHCFTAB()
    'Seq.,ALC2,Group,Cmd,Event,Tmer min.,Timer hrs.,Status,ACTN
    TAB_CONFIGURATION.ResetContent
    TAB_CONFIGURATION.FontName = "Courier New"
    TAB_CONFIGURATION.HeaderFontSize = "18"
    TAB_CONFIGURATION.FontSize = "16"
    TAB_CONFIGURATION.EnableHeaderSizing True
    TAB_CONFIGURATION.ShowHorzScroller True
    TAB_CONFIGURATION.AutoSizeByHeader = True
    TAB_CONFIGURATION.SetFieldSeparator strHCFTAB_Sepa
    TAB_CONFIGURATION.SetTabFontBold True

    Dim strFields As String
    GetKeyItem strFields, TAB_CONFIGURATION.Tag, "{=FIELDS=}", "{="
    TAB_CONFIGURATION.HeaderLengthString = "10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10"
    strFields = Replace(strFields, ",", strHCFTAB_Sepa)
    TAB_CONFIGURATION.HeaderString = strFields
    TAB_CONFIGURATION.AutoSizeColumns
End Sub

Private Sub InsertHCFTAB()
    Dim llRow As Long
    llRow = TAB_AUTHORISATION.GetCurrentSelected
    If llRow < 0 Or llRow > TAB_AUTHORISATION.GetLineCount Then
        MsgBox "Please select an authorization to assign this command first!", vbInformation
    Else
        Dim strInsert As String
        Dim strURNO As String
        Dim strUHAT As String
        Dim strLineTag As String

        strInsert = GetHCFTAB_String()
        strURNO = frmData.GetUrno()
        GetKeyItem strUHAT, TAB_AUTHORISATION.GetLineTag(llRow), "{=URNO=}", "{="

        TAB_CONFIGURATION.InsertTextLine strInsert, False
        TAB_CONFIGURATION.SetLineColor TAB_CONFIGURATION.GetLineCount - 1, vbBlack, vbGreen
        TAB_CONFIGURATION.AutoSizeColumns
        TAB_CONFIGURATION.RedrawTab

        strLineTag = "{=URNO=}" & strURNO & "{=UHAT=}" & strUHAT
        TAB_CONFIGURATION.SetLineTag TAB_CONFIGURATION.GetLineCount - 1, strLineTag
        cmdSave.BackColor = vbRed
    End If
End Sub

Private Sub UpdateHCFTAB()
    Dim llRow As Long
    llRow = TAB_CONFIGURATION.GetCurrentSelected
    If llRow < 0 Or llRow > TAB_CONFIGURATION.GetLineCount Then
        MsgBox "Please select a configuration record first!", vbInformation
    Else
        Dim strUpdate As String
        strUpdate = GetHCFTAB_String()
        TAB_CONFIGURATION.UpdateTextLine llRow, strUpdate, False
        TAB_CONFIGURATION.SetLineColor llRow, vbBlack, vbGreen
        TAB_CONFIGURATION.AutoSizeColumns
        TAB_CONFIGURATION.RedrawTab
        cmdSave.BackColor = vbRed
    End If
End Sub

Private Sub DeleteHCFTAB()
    Dim llRow As Long
    llRow = TAB_CONFIGURATION.GetCurrentSelected
    If llRow < 0 Or llRow > TAB_CONFIGURATION.GetLineCount Then
        MsgBox "Please select a configuration record first!", vbInformation
    Else
        Dim ilRet As Integer
        ilRet = MsgBox("Are you sure to delete this configuration entry?", vbYesNo)
        If ilRet = vbYes Then
            Dim strURNO As String
            GetKeyItem strURNO, TAB_CONFIGURATION.GetLineTag(llRow), "{=URNO=}", "{="
            frmData.DeleteInternal frmData.tab_HCFTAB, strURNO

            TAB_CONFIGURATION.DeleteLine llRow
            TAB_CONFIGURATION.AutoSizeColumns
            TAB_CONFIGURATION.RedrawTab
            cmdSave.BackColor = vbRed
        End If
    End If
End Sub

Private Function GetHCFTAB_String() As String
    Dim strRet As String
    'Fields: Seq.,ALC2,Group,Cmd,Event,Timer min.,Timer hrs.,Status,ACTN
    strRet = txtSERL.Text & strHCFTAB_Sepa & txtALC2.Text & strHCFTAB_Sepa & _
             cmbGRPT.Text & strHCFTAB_Sepa & txtCOMD.Text & strHCFTAB_Sepa & _
             txtEVNT.Text & strHCFTAB_Sepa & txtTIMI.Text & strHCFTAB_Sepa & _
             txtTIHR.Text & strHCFTAB_Sepa & cmbSTAT.Text & strHCFTAB_Sepa & _
             cmbACTN.Text & strHCFTAB_Sepa & txtCMDN.Text & strHCFTAB_Sepa & _
             txtNPGL.Text & strHCFTAB_Sepa & txtNPGC.Text & strHCFTAB_Sepa & _
             txtNPGK.Text & strHCFTAB_Sepa & txtLPGL.Text & strHCFTAB_Sepa & _
             txtLPGC.Text & strHCFTAB_Sepa & txtLPGK.Text
    GetHCFTAB_String = strRet
End Function

Public Sub RefreshManualInputTab()
    frmData.GenerateView_AFTTAB TAB_AFT
    'frmData.GenerateView_COMMANDS
End Sub

Private Sub InitAFTTAB()
    TAB_AFT.ResetContent
    TAB_AFT.FontName = "Courier New"
    TAB_AFT.HeaderFontSize = "18"
    TAB_AFT.FontSize = "16"
    TAB_AFT.EnableHeaderSizing True
    TAB_AFT.SetTabFontBold True
    Dim strFields As String
    GetKeyItem strFields, TAB_AFT.Tag, "{=FIELDS=}", "{="
    TAB_AFT.HeaderLengthString = "110,130,60,20"
    TAB_AFT.HeaderString = strFields
    TAB_AFT.ColumnAlignmentString = "L,C,L,C"
    TAB_AFT.DateTimeSetColumn 1
    TAB_AFT.DateTimeSetInputFormatString 1, "YYYYMMDDhhmmss"
    TAB_AFT.SetColumnBoolProperty 3, "1", "0"

    Dim strLineNo  As String
    Dim strFormat As String
    strLineNo = frmData.tab_CONFIG.GetLinesByColumnValue(0, "STD_OUTPUT_FORMAT", 0)
    If IsNumeric(strLineNo) = True Then
        strFormat = frmData.tab_CONFIG.GetColumnValue(CLng(strLineNo), 1)
    Else
        strFormat = "DD'/'MM'/'YYYY' 'hh':'mm"
    End If

    TAB_AFT.DateTimeSetOutputFormatString 1, strFormat
End Sub

Private Sub IniTabCommands()
    TAB_COMMANDS.ResetContent
    TAB_COMMANDS.FontName = "Courier New"
    TAB_COMMANDS.HeaderFontSize = "18"
    TAB_COMMANDS.FontSize = "16"
    TAB_COMMANDS.EnableHeaderSizing True
    TAB_COMMANDS.SetTabFontBold True
    TAB_COMMANDS.SetFieldSeparator Chr(15)
    Dim strFields As String
    GetKeyItem strFields, TAB_COMMANDS.Tag, "{=FIELDS=}", "{="
    TAB_COMMANDS.HeaderLengthString = "215"
    TAB_COMMANDS.HeaderString = strFields
End Sub
