#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/PVG/PVG_Server/Base/Server/Interface/tighdl.c 1.2 2009/07/08 18:00:02SGT fei Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :  Mei                                                      */
/* Date           :  13 June 2007                                             */
/* Description    :  To transfer Shang Hai Terminal 1 flight information with */
/*                   Terminal 2 information                                   */
/*                                                                            */
/* Update history :                                                           */
/* 2009-04-27   MEI  Remove STOD check when changes for Check-in Counter      */
/* 2009-07-07   MEI  To include info on CKES and CKBS when performing counter */
/*           deletion such that ACTION can decide whether to forward to cdi2  */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */

static char sccs_version[] ="@(#) UFIS44 (c) ABB AAT/I skeleton.c 44.1.0 / 11.12.2002 HEB";

/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */

#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "fditools.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "rmschkrul.h"
#include "syslib.h"
#include "db_if.h"
#include <sys/time.h>
#include <cedatime.h>

#ifdef _HPUX_SOURCE
    extern long timezone;
    extern char *tzname[2];
    extern int _daylight;
    extern long _timezone;
    extern char *_tzname[2];
#endif

#ifndef BOOL
#define BOOL int
#endif

#define DTTMLEN  14      /* YYYYMMDDHHMMSS+TZ  */
#define ACTNLEN   1      /* Action - I, U or D */
#define ADIDLEN   1      /* A or D             */
#define DATELEN   8      /* YYYYMMDD           */      
#define FLNOLEN   9       
#define SEQELEN   6      /* Sequence number 0-999999 */       

/* INFOBJ_FLIGHT */
#define URNOLEN  10
#define REGNLEN  12      /* Registration */
#define CSGNLEN   8      /* Call-sign    */
#define CTY3LEN   3      /* Origin or Destination 3 LC - IATA Code */
#define CTY4LEN   4      /* Origin or Destination 4 LC - optional  */
#define FTYPLEN   1      /* Flight Status */
#define TTYPLEN   5      /* Flight Nature */
#define ACT3LEN   3      /* Aircraft Type 3 LC - IATA */
#define ACT5LEN   5      /* Aircraft Type 5 LC - ICAO optional */
#define VIANLEN   2      /* Number of VIAs */
#define VIALLEN  1024    /* VIA list       */
#define JFNOLEN  90      /* Joint Flight Number */
#define JCNTLEN   2      /* Number of Joint Flight */
#define RUNWLEN   4      /* Arrival or Departure Runway */
#define REMPLEN   4      /* FIDs Remark */
#define DCOELEN   2      /* Delay Code */
#define DTIMLEN   4      /* Delay Time */
#define GATELEN   5      /* Gate */
#define TGATLEN   1      /* Terminal Gate */
#define TMBTLEN   1      /* Terminal Belt */
#define BELTLEN   5      /* Belt */
#define CODPLEN   1      /* Y - Yes or N - No for Code Sharing Partnerflight */
#define PSTALEN   5      /* Parking Stand */
#define FLTILEN   1      /* Flight ID */
#define DIVRLEN   14     /* Divert Info */
#define BAZSLEN   3      /* Carousel */

/* INFOBJ_COUNTER */
#define COUNLEN  5       /* Counter         */ 
#define CTYPLEN  1       /* CIC Type        */
#define CKITLEN  1       /* CIC Terminal    */
#define DISPLEN  60      /* Display Info    */
#define REMALEN  60      /* Free Text Field */

#define MQRDLEN        15000   /* For Msg from the MQ */
#define DBQRLEN         5000   /* For DB SELECT query */
#define MAX_VIAS           8    
#define MAX_JOINT_FLIGHT  10    
#define MAX_FLIGHT_FIELDS 52
/*#define SECONDS_PER_DAY   86400*/

#define MQ_STATUS_OK  1    /* For MQ Status Update */
#define MQ_STATUS_ERR 2    /* For MQ Status Update */

#define TYPE_BULK    1
#define TYPE_INSERT  2
#define TYPE_UPDATE  3
#define TYPE_DELETE  4

typedef struct
{
    char TIME[DTTMLEN+5];
    char ACTN[ACTNLEN+1];
    char ADID[ADIDLEN+1];
    char STDT[DTTMLEN+1];
    char FLNO[FLNOLEN+1];
    char SEQE[SEQELEN+1];
} INFOBJ_GENERIC;

typedef struct
{
    char FLNO[FLNOLEN+1];
    char URNO[URNOLEN+1];
    char FTYP[FTYPLEN+1];
    char AIRB[DTTMLEN+1];
    char LAND[DTTMLEN+1];
    char VIAN[VIANLEN+1];
    char VIAL[VIALLEN+1];
    char ROUT[2048];        /* Virtual field */
    char JFNO[JFNOLEN+1];
    char JCNT[JCNTLEN+1];
    char COSH[100];         /* Virtual field */
    char CODP[CODPLEN+1];
    char SKED[DTTMLEN+1];
} INFOBJ_FLIGHT;

typedef struct
{
    char URNO[URNOLEN+1];
    char CKIC[COUNLEN+1];
    char CTYP[CTYPLEN+1];
    char CKIT[CKITLEN+1];
    char CKBS[DTTMLEN+1];
    char CKES[DTTMLEN+1];
    char DISP[DISPLEN+1];
    char REMA[REMALEN+1];
    char ACT3[8];
    char CDAT[16];
    char CGRU[260];
    char COIX[12];
    char COPN[4];
    char FLNO[12];
    char FLNU[12];
    char GHPU[12];
    char GHSU[12];
    char GPMU[12];
    char LSTU[16];
    char STAT[12];
    char STOD[16];
} INFOBJ_COUNTER;

typedef struct 
{
    int ilFound;
    char pclValue[800];
    char pclFldName[8];
} SPECIAL_FLFLDS;

/* For global airline codes */
typedef struct 
{
    char ALC2[3];
    char ALC3[4];
    char spare;
} TIG_AIRLINE_CODES;

int debug_level = TRACE;
/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;             /* The queue item pointer  */
static EVENT *prgEvent     = NULL;             /* The event pointer       */
static int   igItemLen     = 0;                /* length of incoming item */
static EVENT *prgOutEvent  = NULL;

static char cgProcessName[20] = "\0";
static char cgConfigFile[512] = "\0";
static char cgConfigOfficerFile[512] = "\0";
static char cgHopo[8] = "\0";                 /* default home airport    */
static char cgTabEnd[8] ="\0";                /* default table extension */
static char pcgCSFtyp[10] = "\0";
static char pcgCSTtyp[10] = "\0";


static long lgEvtCnt = 0;
static int igFlightModid;
static int igIpcconsModid;
static int igSqlModid;
static int igSqlxModid;
static int igSendPriority;
static int igHourDifference = 8;
static int igSequenceNo = 0;

#define ARR_FLIGHT      0
#define DEP_FLIGHT      1
#define NUM_BULK_TIME   3
#define MAX_SEQUENCE_NO 999999
#define ONE_VIAL_SIZE   76
#define ONE_JFNO_SIZE   9

#define START_DATA_SIZE  10000
#define BLK_DATA_SIZE    5000
#define CHAR_BLANK " "

static TIG_AIRLINE_CODES *prgALC;
static int igXmlCapSize = 0;
static int igNum_Airline_Codes = 0;
static char pcgXmlStr[START_DATA_SIZE];
static char pcgXmlHeader[300] = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<MSG xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"MSG.xsd\">\n";
static char pcgXmlFileDirectory[200];

/* 60 fields */
/* Fields : ETAI, ETDI, AIRB and LAND are special, since they are readonly fields. Other fields are upd actually */

#define NUM_FLINSERT_FIELDS 62
static char pcgInFlightFields[NUM_FLINSERT_FIELDS][5] 
                                                    = { "REGN","CSGN","DES4","ORG4","ACT3","ACT5","RWYA","RWYD","REMP","DCD1","DCD2","DTD1",
                                                        "DTD2","GTA1","GTA2","GTD1","GTD2","TGA1","TGA2","TGD1","TGD2","BLL1","BLL2","BLT1",
                                                        "BLT2","PSTA","PSTD","DIVR","BAZ1","BAZ2","ORG3","DES3","TTYP","FTYP","VIAN","VIAL",
                                                        "JFNO","JCNT","CODP","BOAO","BOAC","GA1B","GA2B","GD1B","GD2B","GA1E","GA2E","GD1E",
                                                        "GD2E","B1BS","B1ES","B2BS","B2ES","BAO1","BAC1","BAO2","BAC2","ETOA","ETOD","AIRB",
                                                        "LAND","SKED" };

static char pcgDbFlightFields[NUM_FLINSERT_FIELDS][5] 
                                                    = { "REGN","CSGN","DES4","ORG4","ACT3","ACT5","RWYA","RWYD","REMP","DCD1","DCD2","DTD1",
                                                        "DTD2","GTA1","GTA2","GTD1","GTD2","TGA1","TGA2","TGD1","TGD2","TMB1","TMB2","BLT1",
                                                        "BLT2","PSTA","PSTD","DIVR","BAZ1","BAZ2","ORG3","DES3","TTYP","FTYP","VIAN","VIAL",
                                                        "JFNO","JCNT"," "   ,"BOAO","BOAC","GA1B","GA2B","GD1B","GD2B","GA1E","GA2E","GD1E",
                                                        "GD2E","B1BS","B1ES","B2BS","B2ES","BAO1","BAC1","BAO2","BAC2","ETOA","ETOD","AIRD",
                                                        "LNDD"," " };

/* Design for field insertion criteria */
/* 1st char = S/N:special or normal(field which needs special attention)                    */
/* 2nd char = T/D: time field or normal data field                                          */
/* 3rd char = A/D/X: to be processed only when matched with ADID, X for not applicable      */
#define I_SPEC  5           
#define I_TIME  6           
#define I_ADID  7
#define C_SPEC  'S'         /* dun append field directly                */
#define C_NORM  'N'         /* append field directly                    */
#define C_CHEK  'C'         /* append field directly and chk it as well */
#define C_TIME  'T'         /* a time field with 17 chars               */
#define C_DATA  'D'         /* data field                               */
#define C_ARID  'A'         /* arrival flight                           */
#define C_DEID  'D'         /* departure flight                         */
#define C_NOTA  'X'         /* no seggregation on ADID, fields will not be inserted if ADID dun match */
static char pcgFlFldsMarker[NUM_FLINSERT_FIELDS][12] 
                                                    = { "REGN(NDX)","CSGN(NDX)","DES4(NDX)","ORG4(NDX)","ACT3(NDX)","ACT5(NDX)","RWYA(NDX)","RWYD(NDX)",
                                                        "REMP(NDX)","DCD1(NDX)","DCD2(NDX)","DTD1(NDX)","DTD2(NDX)","GTA1(ND )","GTA2(ND )","GTD1(NDX)",
                                                        "GTD2(NDX)","TGA1(ND )","TGA2(ND )","TGD1(NDX)","TGD2(NDX)","TMB1(NDX)","TMB2(NDX)","BLT1(NDX)",
                                                        "BLT2(NDX)","PSTA(NDX)","PSTD(NDX)","DIVR(NDX)","BAZ1(NDX)","BAZ2(NDX)","ORG3(SDX)","DES3(SDX)",
                                                        "TTYP(CDX)","FTYP(CDX)","VIAN(SDX)","VIAL(SDX)","JFNO(SDX)","JCNT(SDX)","CODP(SDX)","BOAO(NTX)",
                                                        "BOAC(NTX)","GA1B(NT )","GA2B(NT )","GD1B(NTX)","GD2B(NTX)","GA1E(NT )","GA2E(NT )","GD1E(NTX)",
                                                        "GD2E(NTX)","B1BS(NTX)","B1ES(NTX)","B2BS(NTX)","B2ES(NTX)","BAO1(NTX)","BAC1(NTX)","BAO2(NTX)",
                                                        "BAC2(NTX)","ETOA(NTX)","ETOD(NTX)","AIRD(NTX)","LNDD(NTX)","SKED(STX)" };

static char pcgOutFlightFields[26][5] = { "ONBL", "OFBL", "GTA1", "GTA2",
                                          "GTD1", "GTD2", "TGA1", "TGA2",
                                          "TGD1", "TGD2", "GA1X", "GA2X",
                                          "GD1X", "GD2X", "GA1Y", "GA2Y",
                                          "GD1Y", "GD2Y", "TMB1", "TMB2",
                                          "BLT1", "BLT2", "B1BA", "B1EA",
                                          "B2BA", "B2EA" };

/* 7 fields */
static char pcgCounterFields[DBQRLEN]= "CKIC,CKIT,CKBS,CKES,DISP,REMA";
/*static char pcgCounterFields[DBQRLEN]= "CKIC,CTYP,CKIT,CKBS,CKES,DISP,REMA";*/
                                       
/******************************************************************************/
/* Function prototypes                                                        */
/******************************************************************************/
static int  Init_Process();
static int ReadConfigEntry(char *pcpFname, char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer);
static int ReadConfigLine(char *pcpFname, char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer);


static int  Reset(void);                       /* Reset program          */
static void Terminate(int ipSleep);            /* Terminate program      */
static void HandleSignal(int);                 /* Handles signals        */
static void HandleErr(int);                    /* Handles general errors */
static void HandleQueErr(int);                 /* Handles queuing errors */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/
static int  HandleData(EVENT *prpEvent);       /* Handles event data     */
static int GetElementValue(char *pclDest, char *pclOrigData, char *pclBegin, char *pclEnd);

static int  TriggerAction(char *pcpTableName);
static int  WaitAndCheckQueue(int ipTimeout, int ipModId, ITEM **prpItem);
static int  CheckQueue(int ipModId, ITEM **prpItem);
static int  GetRowLength(char *pcpHopo, char *pcpTana, char *pcpFieldList,long *pipLen);
static int  GetFieldLength(char *pcpHopo, char *pcpTana, char *pcpFina,long *pipLen);
static int getOrPutDBData(char *pcpSelection, char *pcpData, unsigned int ipMode);
static void HandleMQData(char *pcpFields, char *pcpData, char *pcpCmd);
static int HandleMQFlightData( char *pcpData, INFOBJ_GENERIC rpGeneric );
static int HandleMQCounterData( char *pcpData, INFOBJ_GENERIC rpGeneric );
static void HandleUfisFlightData(char *pcpFields, char *pcpData, char *pclCmd, char *pcpSelection);
static void HandleUfisCounterData(char *pcpFields, char *pcpData, char *pclCmd, char *pcpSelection);
static int getOrPutDBData(char *pcpSelection, char *pcpData, unsigned int ipMode);
static int PackHeaderXml( char *pcpXmlStr, INFOBJ_GENERIC rpGeneric, int ipPackType, int *piSequenceNo );
static int GetTimeZone();
static int GetFlightUrno( INFOBJ_GENERIC rpGeneric, char *pcpUrno );
static int GetCounterRec( INFOBJ_COUNTER *prpCounter );
static int GetCodeshareInfo( char *pcpVERS, char *pcpFTYP, char *pcpUrno );
static int xml_untag( char *pcpResult, char *pcpXml, char *pcpKey, int ipUntagCnt );
static void ReadXMLFile(char *pcpFields, char *pcpData, char *pcpCmd);
static int ReadXMLHeader( char *pcpHeaderStr, INFOBJ_GENERIC *rpGeneric );
static void InformTIG( char *pclXmlStr, int plSequenceNo );
static int ConvertSpecialChar( char *pcpInStr, char *pcpOutStr );
static int RemoveNewLine( char *pcpStr );
static int ConvertTag( char *pcpStr );
static void GetALCData();
static int GetALC( char *pcpALC2, char *pcpALC3 );
static int BuildFlightNbr (char *pclFlightNbr, char *pclfnr3, char *pclfnr2, char *pclalc3, char *pclalc2, char *pclflnr, char *pclflsf );
static int FormatJFNO( char cpAction, char *pcpJFNO, char *pcpJCNT, char *pcpCOSH );
static int FormatVIAL( char *pcpVIAL, char *pcpVIAN, char *pcpROUT );
/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
    int ilRc = RC_SUCCESS;          /* Return code          */
    int ilCnt = 0;


    INITIALIZE;         /* General initialization   */

    strcpy(cgProcessName,argv[0]);

    dbg(TRACE,"MAIN: version <%s>",sccs_version);

    /* Attach to the MIKE queues */
    do
    {
        ilRc = init_que();
        if(ilRc != RC_SUCCESS)
        {
            dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        }/* end of if */
    }while((ilCnt < 10) && (ilRc != RC_SUCCESS));

    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
        sleep(60);
        exit(1);
    }else{
        dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
    }/* end of if */

    do
    {
        ilRc = init_db();
        if (ilRc != RC_SUCCESS)
        {
            check_ret(ilRc);
            dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
            sleep(6);
            ilCnt++;
        } /* end of if */
    } while((ilCnt < 10) && (ilRc != RC_SUCCESS));

    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
        sleep(60);
        exit(2);
    }else{
        dbg(TRACE,"MAIN: init_db() OK!");
    } /* end of if */

    /* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */
    sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
    dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
    ilRc = TransferFile(cgConfigFile);
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */

    sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
    dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
    ilRc = TransferFile(cgConfigFile);
    if(ilRc != RC_SUCCESS)
    { 
        dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
    } /* end of if */

    ilRc = SendRemoteShutdown(mod_id);
    if(ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
    } /* end of if */

    if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
    {
        dbg(DEBUG,"MAIN: waiting for status switch ...");
        HandleQueues();
    }/* end of if */

    if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
    {
        dbg(TRACE,"MAIN: initializing ...");
            ilRc = Init_Process();
            if(ilRc != RC_SUCCESS)
            {
                dbg(TRACE,"Init_Process: init failed!");
            } /* end of if */

    } else {
        Terminate(30);
    }/* end of if */




    dbg(TRACE,"=====================");
    dbg(TRACE,"MAIN: initializing OK");
    dbg(TRACE,"=====================");
    
    for(;;)
    {
        ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* Acknowledge the item */
        ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
        if( ilRc != RC_SUCCESS ) 
        {
            /* handle que_ack error */
            HandleQueErr(ilRc);
            continue;
        } /* fi */

        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */

        prgEvent = (EVENT *) prgItem->text;
                
        if( ilRc == RC_SUCCESS )
        {
            
            lgEvtCnt++;

            switch( prgEvent->command )
            {
            case    HSB_STANDBY :
                ctrl_sta = prgEvent->command;
                HandleQueues();
                break;  
            case    HSB_COMING_UP   :
                ctrl_sta = prgEvent->command;
                HandleQueues();
                break;  
            case    HSB_ACTIVE  :
                ctrl_sta = prgEvent->command;
                break;  
            case    HSB_ACT_TO_SBY  :
                ctrl_sta = prgEvent->command;
                /* CloseConnection(); */
                HandleQueues();
                break;  
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate(1);
                break;  
            case    HSB_STANDALONE  :
                ctrl_sta = prgEvent->command;
                ResetDBCounter();
                break;  
            case    REMOTE_DB :
                /* ctrl_sta is checked inside */
                HandleRemoteDB(prgEvent);
                break;
            case    SHUTDOWN    :
                /* process shutdown - maybe from uutil */
                Terminate(1);
                break;
                    
            case    RESET       :
                ilRc = Reset();
                break;
                    
            case    EVENT_DATA  :
                if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
                {
                    ilRc = HandleData(prgEvent);
                    if(ilRc != RC_SUCCESS)
                    {
                        HandleErr(ilRc);
                    }/* end of if */
                }else{
                    dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
                    DebugPrintItem(TRACE,prgItem);
                    DebugPrintEvent(TRACE,prgEvent);
                }/* end of if */
                break;
                

            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;
            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;
            default         :
                dbg(TRACE,"MAIN: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */

        } 
        else 
        {
            /* Handle queuing errors */
            HandleQueErr(ilRc);
        } /* end else */
        
    } /* end for */
    
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/



static int ReadConfigEntry(char *pcpFname, char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer)
{
    int ilRc = RC_SUCCESS;

    char clSection[124] = "\0";
    char clKeyword[124] = "\0";

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRc = iGetConfigEntry(pcpFname,clSection,clKeyword,
               CFG_STRING,pcpCfgBuffer);
    if(ilRc != RC_SUCCESS){
        dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
    } 
    else{
        dbg(DEBUG,"Config Entry <%s>,<%s>:<%s> found in %s",
        clSection, clKeyword ,pcpCfgBuffer, cgConfigFile);
    }/* end of if */
    return ilRc;
}

static int ReadConfigLine(char *pcpFname, char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer)
{
    FILE *rlFptr;
    BOOL ilFound = FALSE;
    BOOL ilStartFound = FALSE;
    char pclLine[2048];
    char pclStartSec[32];
    char pclEndSec[32];
    char *pclToken;

    rlFptr = fopen( pcpFname, "r" );
    if( rlFptr == NULL )
    {
        dbg(TRACE,"Error in reading Config Entry errno = %d<%s>", errno, strerror(errno) );
        return RC_FAIL;
    }
    sprintf( pclStartSec, "[%s]", pcpSection );
    sprintf( pclEndSec, "[%s_END]", pcpSection );

    while( !feof(rlFptr) )
    {
        if( fgets( pclLine, sizeof(pclLine), rlFptr ) == NULL )
            break;
        if( !strncmp(pclLine, pclEndSec, strlen(pclEndSec)) )
           break;
        if( !strncmp(pclLine, pclStartSec, strlen(pclStartSec)) )
        {
           ilStartFound = TRUE;
           continue;
        }
        if( ilStartFound == FALSE )
           continue;;
        pclToken = (char *)strtok( pclLine, " " );
        if( pclToken == NULL )
            continue;
        if( strncmp(pclToken, pcpKeyword, strlen(pcpKeyword)) )
            continue;
      
        pclToken = (char *)strtok( NULL, "'" );
        pclToken = (char *)strtok( NULL, "'" );
        memcpy( pcpCfgBuffer, pclToken, strlen(pclToken) );
        pcpCfgBuffer[strlen(pclToken)] = '\0';
        ilFound = TRUE;
        break;
    }
    fclose(rlFptr);
    if( ilFound == FALSE )
        return RC_FAIL;
    return RC_SUCCESS;
}

static int Init_Process()
{
    int  ilRc = RC_SUCCESS;         /* Return code */
    char  clTmpStr[100] = "\0";
    char pclSqlBuf[DBQRLEN] = "\0";
    char pclSqlData[DBQRLEN] = "\0";
    char *pclFunc = "Init_Process";


    /* read HomeAirPort from SGS.TAB */
    ilRc = tool_search_exco_data("SYS", "HOMEAP", cgHopo);
    if (ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"<%s> EXTAB,SYS,HOMEAP not found in SGS.TAB", pclFunc);
        Terminate(30);
    } 
    else 
        dbg(TRACE,"<%s> home airport <%s>", pclFunc,cgHopo);

    
    ilRc = tool_search_exco_data("ALL","TABEND", cgTabEnd);
    if (ilRc != RC_SUCCESS)
    {
        dbg(TRACE,"<%s> EXTAB,ALL,TABEND not found in SGS.TAB", pclFunc);
        Terminate(30);
    } 
    else 
        dbg(TRACE,"<%s> table extension <%s>", pclFunc,cgTabEnd);

    igFlightModid = tool_get_q_id( "flight" );
    dbg(TRACE,"<%s> Flight Handler Id <%d>", pclFunc,igFlightModid);

    igIpcconsModid = tool_get_q_id( "ipccons" );
    dbg(TRACE,"<%s> IPC Connector Id <%d>", pclFunc,igIpcconsModid);

    ilRc = ReadConfigEntry( cgConfigFile, "MAIN","SQLHDL",clTmpStr);
    dbg(TRACE,"<%s> igSqlModid <%s>", pclFunc, clTmpStr );
    igSqlModid = atoi(clTmpStr);

    ilRc = ReadConfigEntry( cgConfigFile, "MAIN","WORKING_SQLHDL",clTmpStr);
    if( ilRc != RC_SUCCESS )
        igSqlxModid = igSqlModid;
    else
    {
        dbg(TRACE,"<%s> Working SQLHDL <%s>", pclFunc, clTmpStr );
        igSqlxModid = tool_get_q_id( clTmpStr );
        dbg(TRACE,"<%s> Working Sql <%d>", pclFunc,igSqlxModid);
    }



    ilRc = ReadConfigEntry( cgConfigFile, "MAIN","SEND_PRIO",clTmpStr);
    dbg(TRACE,"<%s> cgSendPrio <%s>", pclFunc, clTmpStr );
    igSendPriority = atoi(clTmpStr);


    /* set debug level */
    ilRc = ReadConfigEntry( cgConfigFile, "MAIN","DEBUG_LEVEL",clTmpStr);
    debug_level = TRACE;
    if( !strncmp( clTmpStr, "DEBUG", 5 ) )
         debug_level = DEBUG;
    dbg(TRACE,"<%s> debug_level <%s><%d>",pclFunc, clTmpStr, debug_level);


    /* To determine how much max XML data to send thru MQ */
    ilRc = ReadConfigEntry( cgConfigFile, "MAIN","XML_CAP_SIZE",clTmpStr);
    igXmlCapSize = atoi( clTmpStr );
    if( igXmlCapSize <= 0 )
            igXmlCapSize = 1 * 1024 * 1024;  /* Default to 1MB */
    dbg(TRACE,"<%s> XML_CAP_SIZE <%s><%d>",pclFunc, clTmpStr, igXmlCapSize);

    /* Get the difference in hours and pass to TIG */
    igHourDifference = GetTimeZone();
    dbg( TRACE, "<%s> Timezone <%d>", pclFunc, igHourDifference );


    /* Where all XML files are stored */
    ilRc = ReadConfigEntry( cgConfigFile, "MAIN","XML_FILE_DIRECTORY",pcgXmlFileDirectory);
    dbg(TRACE,"<%s> XML_FILE_DIRECTORY <%s>",pclFunc, pcgXmlFileDirectory);

    ilRc = ReadConfigEntry( cgConfigFile, "CODESHARE","FIELD_FTYP",pcgCSFtyp);
    if( ilRc != RC_SUCCESS )
        strcpy( pcgCSFtyp, " " );
    dbg(TRACE,"<%s> CODESHARE- FIELD_FTYP <%s>",pclFunc, pcgCSFtyp);

    ilRc = ReadConfigEntry( cgConfigFile, "CODESHARE","FIELD_TTYP",pcgCSTtyp);
    dbg(TRACE,"<%s> CODESHARE- FIELD_TTYP <%s>",pclFunc, pcgCSTtyp);

    GetALCData();

    sprintf( pclSqlBuf, "%s", "SELECT SEQN FROM TGSTAB ORDER BY MKEY DESC" );
    dbg( TRACE, "<%s> SqlBuf <%s>", pclFunc, pclSqlBuf );

    igSequenceNo = 0;
    ilRc = getOrPutDBData(pclSqlBuf,pclSqlData,START);
    if( ilRc != DB_SUCCESS )
        dbg( TRACE, "<%s> ERROR!!!!!  Unable to get SEQN from TGSTAB!!!!!", pclFunc );
    igSequenceNo = atoi(pclSqlData);
    dbg( TRACE, "<%s> igSequenceNo <%d>", pclFunc, igSequenceNo );


    if (ilRc != RC_SUCCESS)
        dbg(TRACE,"<%s> failed", pclFunc);

    return(ilRc);

} /* end of initialize */




/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
    int ilRc = RC_SUCCESS;              /* Return code */
    
    dbg(TRACE,"Reset: now resetting");
    
    return ilRc;
    
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
    /* unset SIGCHLD ! DB-Child will terminate ! */

    dbg(TRACE,"Terminate: now DB logoff ...");

    signal(SIGCHLD,SIG_IGN);

    logoff();

    dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);


    dbg(TRACE,"Terminate: now leaving ...");

    fclose(outp);
    free( (TIG_AIRLINE_CODES *)prgALC );

    sleep(ipSleep < 1 ? 1 : ipSleep);
    
    exit(0);
    
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/

static void HandleSignal(int pipSig)
{
    dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);

    switch(pipSig)
    {
    default :
        Terminate(1);
        break;
    } /* end of switch */

    exit(1);
    
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
    dbg(TRACE,"HandleErr: <%d> : unknown ERROR",pipErr);

    return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
    switch(pipErr) {
    case    QUE_E_FUNC  :   /* Unknown function */
        dbg(TRACE,"<%d> : unknown function",pipErr);
        break;
    case    QUE_E_MEMORY    :   /* Malloc reports no memory */
        dbg(TRACE,"<%d> : malloc failed",pipErr);
        break;
    case    QUE_E_SEND  :   /* Error using msgsnd */
        dbg(TRACE,"<%d> : msgsnd failed",pipErr);
        break;
    case    QUE_E_GET   :   /* Error using msgrcv */
        dbg(TRACE,"<%d> : msgrcv failed",pipErr);
        break;
    case    QUE_E_EXISTS    :
        dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
        break;
    case    QUE_E_NOFIND    :
        dbg(TRACE,"<%d> : route not found ",pipErr);
        break;
    case    QUE_E_ACKUNEX   :
        dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
        break;
    case    QUE_E_STATUS    :
        dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
        break;
    case    QUE_E_INACTIVE  :
        dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
        break;
    case    QUE_E_MISACK    :
        dbg(TRACE,"<%d> : missing ack ",pipErr);
        break;
    case    QUE_E_NOQUEUES  :
        dbg(TRACE,"<%d> : queue does not exist",pipErr);
        break;
    case    QUE_E_RESP  :   /* No response on CREATE */
        dbg(TRACE,"<%d> : no response on create",pipErr);
        break;
    case    QUE_E_FULL  :
        dbg(TRACE,"<%d> : too many route destinations",pipErr);
        break;
    case    QUE_E_NOMSG :   /* No message on queue */
        dbg(TRACE,"<%d> : no messages on queue",pipErr);
        break;
    case    QUE_E_INVORG    :   /* Mod id by que call is 0 */
        dbg(TRACE,"<%d> : invalid originator=0",pipErr);
        break;
    case    QUE_E_NOINIT    :   /* Queues is not initialized*/
        dbg(TRACE,"<%d> : queues are not initialized",pipErr);
        break;
    case    QUE_E_ITOBIG    :
        dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
        break;
    case    QUE_E_BUFSIZ    :
        dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
        break;
    default         :   /* Unknown queue error */
        dbg(TRACE,"<%d> : unknown error",pipErr);
        break;
    } /* end switch */
         
    return;

} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
    int ilRc = RC_SUCCESS;          /* Return code */
    int ilBreakOut = FALSE;
    
    do
    {
        ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
        /* depending on the size of the received item  */
        /* a realloc could be made by the que function */
        /* so do never forget to set event pointer !!! */
        prgEvent = (EVENT *) prgItem->text; 

        if( ilRc == RC_SUCCESS )
        {
            /* Acknowledge the item */
            ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
            if( ilRc != RC_SUCCESS ) 
            {
                /* handle que_ack error */
                HandleQueErr(ilRc);
            } /* fi */
        
            switch( prgEvent->command )
            {
            case    HSB_STANDBY :
                ctrl_sta = prgEvent->command;
                break;  
    
            case    HSB_COMING_UP   :
                ctrl_sta = prgEvent->command;
                break;  
    
            case    HSB_ACTIVE  :
                ctrl_sta = prgEvent->command;
                ilBreakOut = TRUE;
                break;  

            case    HSB_ACT_TO_SBY  :
                ctrl_sta = prgEvent->command;
                break;  
    
            case    HSB_DOWN    :
                /* whole system shutdown - do not further use que(), send_message() or timsch() ! */
                ctrl_sta = prgEvent->command;
                Terminate(1);
                break;  
    
            case    HSB_STANDALONE  :
                ctrl_sta = prgEvent->command;
                ResetDBCounter();
                ilBreakOut = TRUE;
                break;  

            case    REMOTE_DB :
                /* ctrl_sta is checked inside */
                HandleRemoteDB(prgEvent);
                break;

            case    SHUTDOWN    :
                Terminate(1);
                break;
                        
            case    RESET       :
                ilRc = Reset();
                break;
                        
            case    EVENT_DATA  :
                dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
                    
            case    TRACE_ON :
                dbg_handle_debug(prgEvent->command);
                break;

            case    TRACE_OFF :
                dbg_handle_debug(prgEvent->command);
                break;

            default         :
                dbg(TRACE,"HandleQueues: unknown event");
                DebugPrintItem(TRACE,prgItem);
                DebugPrintEvent(TRACE,prgEvent);
                break;
            } /* end switch */
        } else {
            /* Handle queuing errors */
            HandleQueErr(ilRc);
        } /* end else */
    } while (ilBreakOut == FALSE);

            ilRc = Init_Process();
            if(ilRc != RC_SUCCESS)
            {
                dbg(TRACE,"InitItkhdl: init failed!");
            } 
     

} /* end of HandleQueues */
    



/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(EVENT *prpEvent)
{
    int    ilRc = RC_SUCCESS;         /* Return code */
    int    ilCmd = 0;
    BC_HEAD *prlBchead       = NULL;
    CMDBLK  *prlCmdblk       = NULL;
    char    *pclSelection    = NULL;
    char    *pclFields       = NULL;
    char    *pclData         = NULL;
    char    *pclRow          = NULL;
    char        pclTable[34];
    char pclFNR2[16] = "\0";
    char pclFNR3[16] = "\0";
    char pclALC2[16] = "\0";
    char pclALC3[16] = "\0";
    char pclFLTN[16] = "\0";
    char pclFLTS[16] = "\0";
    char pclTwstart[40] = "\0";

    prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
    prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
    pclSelection = prlCmdblk->data;
    pclFields    = pclSelection + strlen(pclSelection) + 1;
    pclData      = pclFields + strlen(pclFields) + 1;


    strcpy(pclTable,prlCmdblk->obj_name);

    dbg(TRACE,"========== START <%10.10d> ==========",lgEvtCnt);

    /****************************************/
    DebugPrintBchead(TRACE,prlBchead);
    DebugPrintCmdblk(TRACE,prlCmdblk);
    dbg(TRACE,"Command: <%s>",prlCmdblk->command);
    dbg(TRACE,"tw_start: <%s>",prlCmdblk->tw_start);
    dbg(TRACE,"originator follows event = %p ",prpEvent);

    dbg(TRACE,"originator<%d>",prpEvent->originator);
    dbg(TRACE,"selection follows Selection = %p ",pclSelection);
    dbg(TRACE,"selection <%s>",pclSelection);
    dbg(TRACE,"fields    <%s>",pclFields);
    dbg(TRACE,"data      <%s>",pclData);
    /****************************************/

    strcpy( pclTwstart, prlCmdblk->tw_start );

    /* Handle batch update of flight information */
    /* From NTISCH process */
    if (!strcmp(prlCmdblk->command,"BLK"))
        BuildFlightNbr (pclData, pclFNR3, pclFNR2, pclALC3, pclALC2, pclFLTN, pclFLTS);

    if (!strcmp(prlCmdblk->command,"XMLI"))
        HandleMQData(pclFields, pclData, prlCmdblk->command);

    /* Handle online-flight updates */
        /* From ACTION process */
    if (!strcmp(pclTable,"AFTTAB"))
        if( strncmp( pclTwstart, "tighdl", 6 ) )
            HandleUfisFlightData(pclFields, pclData, prlCmdblk->command, pclSelection);
    
    if (!strcmp(pclTable,"CCATAB"))
    {
        if( strncmp( pclTwstart, "tighdl", 6 ) )
            HandleUfisCounterData(pclFields, pclData, prlCmdblk->command, NULL);
        else if( !strncmp(prlCmdblk->command, "URT",3) )
            HandleUfisCounterData(pclFields, pclData, prlCmdblk->command, pclSelection);
    }

    if (!strcmp(prlCmdblk->command,"TST"))
        ReadXMLFile(pclFields, pclData, prlCmdblk->command);

    if (!strcmp(prlCmdblk->command,"UPD"))
        GetALCData();
    if (!strcmp(prlCmdblk->command,"LIV"))
    {
        SendCedaEvent(igIpcconsModid, 0, "tighdl", "EXCO", "", "", "XMLO", "", "",
                          "", "HEARTBEAT", "", igSendPriority, NETOUT_NO_ACK);
    }

    dbg(TRACE,"==========  END  <%10.10d> ==========",lgEvtCnt);

    return(RC_SUCCESS);
    
} /* end of HandleData */



static void HandleMQData(char *pcpFields, char *pcpData, char *pcpCmd)
{
    INFOBJ_GENERIC rlGeneric;
    int ilRc = RC_SUCCESS;
    int ilFound;
    char pclSqlBuf[DBQRLEN] = "\0";     
    char pclTmpStr[DBQRLEN] = "\0";     
    char *pclFunc = "HandleMQData";


    dbg(TRACE,"<%s> ---------- START ---------",pclFunc);

    ilFound = GetElementValue(pclTmpStr, pcpData, "<INFOBJ_GENERIC>", "</INFOBJ_GENERIC>");
    if( ilFound == FALSE )
    {
        dbg( TRACE, "<%s> ERROR: No XML header. DATA = (%s)", pclFunc, pcpData );
        return;
    }
    
    memset( &rlGeneric, 0, sizeof(INFOBJ_GENERIC) );
    ilFound = ReadXMLHeader( pclTmpStr, &rlGeneric );
    if( ilFound == FALSE )
        return;

    ilFound = GetElementValue(pclTmpStr, pcpData, "<INFOBJ_FLIGHT>", "</INFOBJ_FLIGHT>");
    if( ilFound == FALSE )
    {
        ilFound = GetElementValue(pclTmpStr, pcpData, "<INFOBJ_COUNTER>", "</INFOBJ_COUNTER>");
        if( ilFound == FALSE )
        {
            dbg( TRACE, "<%s> ERROR: Invalid XML data from MQ. DATA = (%s)", pclFunc, pcpData );
            return;
        }
        RemoveNewLine(pcpData);
        HandleMQCounterData(pcpData, rlGeneric);
        return;
    }
    RemoveNewLine(pcpData);
    HandleMQFlightData(pcpData, rlGeneric);

    dbg(TRACE,"<%s> ---------- END ---------",pclFunc);
                                    
} /* end HandleMQData */


static int ReadXMLHeader( char *pcpHeaderStr, INFOBJ_GENERIC *rpGeneric )
{
    int ilFound;
    char pclTmpStr[DBQRLEN] = "\0";     
    char pclHeaderStr[1000] = "\0";
    char *pclFunc = "ReadXMLHeader";
 
    dbg(TRACE,"<%s> ---------- START ---------",pclFunc);
    dbg(TRACE,"<%s> Header String (%s)",pclFunc, pcpHeaderStr );

    strcpy( pclHeaderStr, pcpHeaderStr );
    ConvertTag(pclHeaderStr);
    
    GetElementValue(rpGeneric->TIME, pclHeaderStr, "<TIMESTAMP>", "</TIMESTAMP>" );
    GetElementValue(rpGeneric->ACTN, pclHeaderStr, "<ACTIONTYPE>", "</ACTIONTYPE>" );
    GetElementValue(rpGeneric->ADID, pclHeaderStr, "<ADID>", "</ADID>" );
    GetElementValue(rpGeneric->STDT, pclHeaderStr, "<STDT>", "</STDT>" );
    GetElementValue(rpGeneric->FLNO, pclHeaderStr, "<FLNO>", "</FLNO>" );
    GetElementValue(rpGeneric->SEQE, pclHeaderStr, "<SEQUENCE>", "</SEQUENCE>" );
  
    dbg( DEBUG, "<%s> TIMESTAMP : <%s>", pclFunc, rpGeneric->TIME );
    dbg( DEBUG, "<%s> ACTIONTYPE: <%s>", pclFunc, rpGeneric->ACTN );
    dbg( DEBUG, "<%s> ADID      : <%s>", pclFunc, rpGeneric->ADID );
    dbg( DEBUG, "<%s> STDT      : <%s>", pclFunc, rpGeneric->STDT );
    dbg( DEBUG, "<%s> FLNO      : <%s>", pclFunc, rpGeneric->FLNO );
    dbg( DEBUG, "<%s> SEQE      : <%s>", pclFunc, rpGeneric->SEQE );

    if( strlen(rpGeneric->TIME) <= 0 || strlen(rpGeneric->ACTN) <= 0 || 
        strlen(rpGeneric->ADID) <= 0 || strlen(rpGeneric->STDT) <= 0 || 
        strlen(rpGeneric->FLNO) <= 0 || strlen(rpGeneric->SEQE) <= 0) 
    {
        dbg( TRACE, "<%s> ERROR: Invalid XML data. DATA = (%s)", pclFunc, pclHeaderStr );
        return FALSE;
    }
    if( rpGeneric->ACTN[0] != 'I' && rpGeneric->ACTN[0] != 'U' && rpGeneric->ACTN[0] != 'D' )
    {
        dbg( TRACE, "<%s> ERROR: Invalid Action Type. DATA = (%s)", pclFunc, pclHeaderStr );
        return FALSE;
    }

    dbg(TRACE,"<%s> ---------- END ---------",pclFunc);
    return TRUE;
}

static int HandleMQFlightData( char *pcpData, INFOBJ_GENERIC rpGeneric )
{
    struct tm *rlTmptr;
    INFOBJ_FLIGHT rlFlight;
    SPECIAL_FLFLDS rlSff[12];  /* Special flight fields */
    BOOL blCodeshare;
    int ili, ilj;
    int ilCODP, ilFTYP;
    int ilVIAL, ilVIAN;
    int ilJFNO, ilJCNT;
    int ilDES3, ilORG3;
    int ilSKED;
    int ilTTYP;
    int ilAction;
    int ilFound;
    int ilRc;
    char pclTmpStr[DBQRLEN] = "\0";     
    char pclBegin[20] = "\0";
    char pclEnd[20] = "\0";
    char pclData[DBQRLEN] = "\0";     
    char pclSqlWhere[DBQRLEN] = "\0";     
    char pclAction[10] = "\0";
    char pclFlightFields[DBQRLEN] = "\0";
    char pclFlightValues[DBQRLEN] = "\0";
    char pclFNR2[16] = "\0";
    char pclFNR3[16] = "\0";
    char pclALC2[16] = "\0";
    char pclALC3[16] = "\0";
    char pclFLTN[16] = "\0";
    char pclFLTS[16] = "\0";
    char pclFLTM[16] = "\0";   /* New field: Flight Time */
    char pclFTYP[FTYPLEN+1];
    char pclVERS[21];
    char *pclFunc = "HandleMQFlightData";
 
    dbg(TRACE,"<%s> ---------- START ---------",pclFunc);
    
    memset( &rlFlight, 0, sizeof(INFOBJ_FLIGHT) );
    memset( rlSff, 0, sizeof(rlSff) );

    if( rpGeneric.ACTN[0] == 'I' )      { strcpy( pclAction, "ISF" ); ilAction = TYPE_INSERT; }
    else if( rpGeneric.ACTN[0] == 'U' ) { strcpy( pclAction, "UFR" ); ilAction = TYPE_UPDATE; }
    else if( rpGeneric.ACTN[0] == 'D' ) { strcpy( pclAction, "DFR" ); ilAction = TYPE_DELETE; }


    ilRc = BuildFlightNbr (rpGeneric.FLNO, pclFNR3, pclFNR2, pclALC3, pclALC2, pclFLTN, pclFLTS);
    if( ilRc == RC_FAIL )
    {
        dbg( TRACE, "<%s> Error in building flight number", pclFunc );
        return;
    }

    dbg( DEBUG, "<%s> FNR2 <%s>", pclFunc, pclFNR2 );
    dbg( DEBUG, "<%s> FNR3 <%s>", pclFunc, pclFNR3 );
    dbg( DEBUG, "<%s> ALC2 <%s>", pclFunc, pclALC2 );
    dbg( DEBUG, "<%s> ALC3 <%s>", pclFunc, pclALC3 );
    dbg( DEBUG, "<%s> FLTN <%s>", pclFunc, pclFLTN );
    dbg( DEBUG, "<%s> FLTS <%s>", pclFunc, pclFLTS );

    if( ilRc == 2 )
        strcpy( rpGeneric.FLNO, pclFNR2 );
    else if( ilRc == 3 )
        strcpy( rpGeneric.FLNO, pclFNR3 );
    else
    {
        dbg( TRACE, "<%s> Invalid RC from BuildFlightNbr <%d>", pclFunc, ilRc );
        return;
    }

    if( GetFlightUrno( rpGeneric, rlFlight.URNO ) == FALSE )
    {
        dbg( TRACE, "<%s> ERROR: No record found", pclFunc );
        /* If rec is not found, update request will be processed as INSERT */
        if( ilAction == TYPE_UPDATE )
        {
            ilAction = TYPE_INSERT;
            strcpy( pclAction, "ISF" );
        }
        if( ilAction != TYPE_INSERT )
            return;
    }
    else
    {
        if( ilAction == TYPE_INSERT )
        {
            /*dbg( TRACE, "<%s> Record exist, abort insertion! URNO <%s>", pclFunc, rlFlight.URNO );
            return;*/
            dbg( TRACE, "<%s> Record exist, change to upd-URNO <%s>", pclFunc, rlFlight.URNO );
            ilAction = TYPE_UPDATE;
            strcpy( pclAction, "UFR" );
        }
        sprintf( pclSqlWhere, " WHERE URNO = %s", rlFlight.URNO );
        dbg( TRACE, "<%s> pclSqlWhere <%s>", pclFunc, pclSqlWhere );
    }

    if( ilAction == TYPE_DELETE )
    {
        ilRc = SendCedaEvent(igFlightModid, 0, "tighdl", "EXCO", "tighdl", "", pclAction, "AFTTAB", pclSqlWhere,
                             "", "", "", igSendPriority, NETOUT_NO_ACK);
        dbg( TRACE, "<%s> DELETE Flight! ilRc = %d", pclFunc, ilRc );
        return ilRc;
    }

    if( rpGeneric.ADID[0] != 'A' && rpGeneric.ADID[0] != 'D' )
    {
        dbg( TRACE, "<%s> Dunno what to do with this ADID <%s>", pclFunc,rpGeneric.ADID );
        return;
    }

    ilFound = xml_untag( pclData, pcpData, "INFOBJ_FLIGHT", 1 );
    if( ilFound == FALSE )
        return;
    strcpy( rlFlight.FLNO, rpGeneric.FLNO );
    dbg( TRACE, "<%s> Flight XML = (%s)", pclFunc, pclData );
    ConvertTag(pclData);

    ilj = 0;
    for( ili = 0; ili < NUM_FLINSERT_FIELDS; ili++ )
    {
        ilFound = FALSE;
        sprintf( pclBegin, "<%s>", pcgInFlightFields[ili] );
        sprintf( pclEnd,   "</%s>",pcgInFlightFields[ili] );
        ilFound = GetElementValue(pclTmpStr, pclData, pclBegin, pclEnd);

        if( pcgFlFldsMarker[ili][I_SPEC] == C_SPEC || pcgFlFldsMarker[ili][I_SPEC] == C_CHEK )
        {
            rlSff[ilj].ilFound = ilFound;
            strcpy( rlSff[ilj].pclFldName, pcgInFlightFields[ili] );
            if( ilFound == TRUE )
            {
                if( pcgFlFldsMarker[ili][I_TIME] == C_TIME )
                    strncpy( rlSff[ilj].pclValue, pclTmpStr, DTTMLEN );
                else
                    strcpy( rlSff[ilj].pclValue, pclTmpStr );
                dbg( TRACE, "<%s> %s = <%s>", pclFunc, pcgInFlightFields[ili], pclTmpStr );
            }
            ilj++;
            if( pcgFlFldsMarker[ili][I_SPEC] == C_SPEC )
                continue;
        }

        if( ilFound == TRUE )
        {
            if( pcgFlFldsMarker[ili][I_ADID] != C_NOTA)
            {
                dbg( DEBUG, "<%s> <%s> Marker = <%s>", pclFunc, pcgInFlightFields[ili], pcgFlFldsMarker[ili] ); 
                if( pcgFlFldsMarker[ili][I_ADID] != rpGeneric.ADID[0] )
                    continue;
            }
            strcat( pclFlightFields, pcgDbFlightFields[ili] ); 
            strcat( pclFlightFields, "," ); 
            if( pcgFlFldsMarker[ili][I_TIME] == C_TIME )
                strncat( pclFlightValues, pclTmpStr, DTTMLEN ); 
            else
                strcat( pclFlightValues, pclTmpStr ); 
            strcat( pclFlightValues, "," ); 
            dbg( TRACE, "<%s> %s = <%s>", pclFunc, pcgInFlightFields[ili], pclTmpStr );
        }
    }
    dbg( TRACE, "<%s> Flight fields (%s)", pclFunc, pclFlightFields );
    dbg( TRACE, "<%s> Flight values (%s)", pclFunc, pclFlightValues );
    dbg( TRACE, "<%s> No. of special fields <%d>", pclFunc, ilj );

    for( ili = 0; ili < ilj; ili++ )
    {
        if( !strncmp(rlSff[ili].pclFldName, "DES3", 4 ) )
            ilDES3 = ili;
        else if( !strncmp(rlSff[ili].pclFldName, "ORG3", 4 ) )
            ilORG3 = ili;
        else if( !strncmp(rlSff[ili].pclFldName, "FTYP", 4 ) )
            ilFTYP = ili;
        else if( !strncmp(rlSff[ili].pclFldName, "TTYP", 4 ) )
            ilTTYP = ili;
        else if( !strncmp(rlSff[ili].pclFldName, "VIAL", 4 ) )
            ilVIAL = ili;
        else if( !strncmp(rlSff[ili].pclFldName, "VIAN", 4 ) )
            ilVIAN = ili;
        else if( !strncmp(rlSff[ili].pclFldName, "JFNO", 4 ) )
            ilJFNO = ili;
        else if( !strncmp(rlSff[ili].pclFldName, "JCNT", 4 ) )
            ilJCNT = ili;
        else if( !strncmp(rlSff[ili].pclFldName, "CODP", 4 ) )
            ilCODP = ili;
        else if( !strncmp(rlSff[ili].pclFldName, "SKED", 4 ) )
            ilSKED = ili;
    }

    dbg( DEBUG, "<%s> ilORG3 = %d ilFound = %d", pclFunc, ilORG3, ilFound );
    dbg( DEBUG, "<%s> ilDES3 = %d ilFound = %d", pclFunc, ilDES3, ilFound );
    dbg( DEBUG, "<%s> ilFTYP = %d ilFound = %d", pclFunc, ilFTYP, ilFound );
    dbg( DEBUG, "<%s> ilTTYP = %d ilFound = %d", pclFunc, ilTTYP, ilFound );
    dbg( DEBUG, "<%s> ilVIAL = %d ilFound = %d", pclFunc, ilVIAL, ilFound );
    dbg( DEBUG, "<%s> ilVIAN = %d ilFound = %d", pclFunc, ilVIAN, ilFound );
    dbg( DEBUG, "<%s> ilJFNO = %d ilFound = %d", pclFunc, ilJFNO, ilFound );
    dbg( DEBUG, "<%s> ilJCNT = %d ilFound = %d", pclFunc, ilJCNT, ilFound );
    dbg( DEBUG, "<%s> ilCODP = %d ilFound = %d", pclFunc, ilCODP, ilFound );
    dbg( DEBUG, "<%s> ilSKED = %d ilFound = %d", pclFunc, ilSKED, ilFound );

    /* stage 1 verification */
    if( ilAction == TYPE_INSERT )
    {
        /* These 4 fields are mandatory */
        if( rpGeneric.ADID[0] == 'A' && strlen(rlSff[ilORG3].pclValue) <= 0 )
        {   
            dbg( TRACE, "<%s> Invalid ORG3. DATA = (%s)", pclFunc, pclData );
            return;
        }
        if( rpGeneric.ADID[0] == 'D' && strlen(rlSff[ilDES3].pclValue) <= 0 )
        {   
            dbg( TRACE, "<%s> Invalid DES3. DATA = (%s)", pclFunc, pclData );
            return;
        }
        if( strlen(rlSff[ilFTYP].pclValue) <= 0 || strlen(rlSff[ilTTYP].pclValue) <= 0 )
        {
            dbg( TRACE, "<%s> Invalid FTYP/TTYP for insert. DATA = (%s)", pclFunc, pclData );
            return;
        }
        if( rpGeneric.ADID[0] == 'A' )
            strcpy( rlSff[ilDES3].pclValue, cgHopo );
        else
            strcpy( rlSff[ilORG3].pclValue, cgHopo );

        if( strlen(rlSff[ilSKED].pclValue) <= 0 )
        {
            dbg( TRACE, "<%s> No Scheduled flight date/time provided!!", pclFunc );
            return;
        }
    }
    if( strlen(rlSff[ilSKED].pclValue) > 0 )
    {
        if( rpGeneric.ADID[0] == 'A' )
            strcat( pclFlightFields, "STOA," );
        else
            strcat( pclFlightFields, "STOD," );
        sprintf( pclTmpStr, "%s,", rlSff[ilSKED].pclValue );
        strcat( pclFlightValues, pclTmpStr );
    }
    if( strlen(rlSff[ilORG3].pclValue) > 0 )
    {
        strcat( pclFlightFields, "ORG3," );
        sprintf( pclTmpStr, "%s,", rlSff[ilORG3].pclValue );
        strcat( pclFlightValues, pclTmpStr );
    }
    if( strlen(rlSff[ilDES3].pclValue) > 0 )
    {
        strcat( pclFlightFields, "DES3," );
        sprintf( pclTmpStr, "%s,", rlSff[ilDES3].pclValue );
        strcat( pclFlightValues, pclTmpStr );
    }

    if( rlSff[ilCODP].ilFound == TRUE ) 
    {
        strcpy( pclTmpStr, "," );
        if( rlSff[ilCODP].pclValue[0] == 'Y' )
            strcpy( pclTmpStr, "CODESHARE," );
        strcat(pclFlightFields, "VERS,"); 
        strcat( pclFlightValues, pclTmpStr );

    }

    /*if( rlSff[ilCODP].ilFound == TRUE || rlSff[ilFTYP].ilFound == TRUE ) 
    {
        if( ilAction == TYPE_UPDATE )
        {
            blCodeshare = FALSE;
            GetCodeshareInfo( pclVERS, pclFTYP, rlFlight.URNO );
            if( strlen(pclVERS) > 0 ) 
                blCodeshare = TRUE;

            if( (rlSff[ilCODP].ilFound == TRUE && rlSff[ilCODP].pclValue[0] == 'N') || 
                (rlSff[ilCODP].ilFound == FALSE && blCodeshare == FALSE) )
            {
                if( rlSff[ilFTYP].ilFound == TRUE )
                    sprintf(pclTmpStr, "%s, ,", rlSff[ilFTYP].pclValue);  
                else
                {   
                    if( blCodeshare == TRUE )
                        sprintf(pclTmpStr, "%c, ,", pclVERS[10]);
                    else
                        sprintf(pclTmpStr, "%s, ,", pclFTYP);
                }
            }
            else if( (rlSff[ilCODP].ilFound == TRUE && rlSff[ilCODP].pclValue[0] == 'Y') || (blCodeshare == TRUE) )
            {
                if( rlSff[ilFTYP].ilFound == TRUE )
                    sprintf(pclTmpStr, "%s,CODESHARE(%c),", pcgCSFtyp, rlSff[ilFTYP].pclValue[0]);  
                else
                {
                    if( blCodeshare == TRUE )
                        sprintf(pclTmpStr, "%s,CODESHARE(%c),", pcgCSFtyp, pclVERS[10]);  
                    else
                        sprintf(pclTmpStr, "%s,CODESHARE(%c),", pcgCSFtyp, pclFTYP[0]);  
                }
            }
        }
        else
        {
            if( rlSff[ilCODP].pclValue[0] == 'Y' )
                sprintf( pclTmpStr, "%s,CODESHARE(%c),", pcgCSFtyp, rlSff[ilFTYP].pclValue[0] );
            else
                sprintf( pclTmpStr, "%s, ,", rlSff[ilFTYP].pclValue );
        }
        strcat(pclFlightFields, "FTYP,VERS,"); 
        strcat( pclFlightValues, pclTmpStr );
    }*/


    /* VIAL and VIAN come in a pair */
    if( rlSff[ilVIAL].ilFound == TRUE && rlSff[ilVIAN].ilFound == TRUE )
    {
        if( FormatVIAL( rlSff[ilVIAL].pclValue, rlSff[ilVIAN].pclValue, rlFlight.ROUT ) == FALSE )
        {
            memset( rlSff[ilVIAL].pclValue, 0, sizeof(rlSff[ilVIAL].pclValue) );
            memset( rlSff[ilVIAN].pclValue, 0, sizeof(rlSff[ilVIAN].pclValue) );
        }
        else
        {
            strcat(pclFlightFields, "VIAL,VIAN,"); 
            strcat(pclFlightValues, rlSff[ilVIAL].pclValue );
            strcat(pclFlightValues, "," );
            strcat(pclFlightValues, rlSff[ilVIAN].pclValue );
            strcat(pclFlightValues, "," );
        }
    }

    if( rlSff[ilJFNO].ilFound == TRUE && rlSff[ilJCNT].ilFound== TRUE )
    {
        if( FormatJFNO( rpGeneric.ACTN[0], rlSff[ilJFNO].pclValue, rlSff[ilJCNT].pclValue, rlFlight.COSH ) == FALSE )
        {
            memset( rlSff[ilJFNO].pclValue, 0, sizeof(rlSff[ilJFNO].pclValue) );
            memset( rlSff[ilJCNT].pclValue, 0, sizeof(rlSff[ilJCNT].pclValue) );
        }
        else
        {
            strcat(pclFlightFields, "JFNO,JCNT,"); 
            strcat(pclFlightValues, rlSff[ilJFNO].pclValue );
            strcat(pclFlightValues, "," );
            strcat(pclFlightValues, rlSff[ilJCNT].pclValue );
            strcat(pclFlightValues, "," );
        }
    }


        
    dbg( DEBUG, "<%s> Virtual Fields => ROUT: <%s>", pclFunc, rlFlight.ROUT);
    dbg( DEBUG, "<%s> Virtual Fields => COSH: <%s>", pclFunc, rlFlight.COSH);

    if( strlen(pclFlightFields) <= 0 )
    {
        dbg( TRACE, "<%s> Nothing to update", pclFunc );
        return;
    }

    if( ilAction == TYPE_INSERT )
    {
        sprintf( pclSqlWhere, "WHERE %s,%s,1234567,1234567,%s", rpGeneric.STDT, rpGeneric.STDT, rpGeneric.STDT );
        dbg( TRACE, "<%s> pclSqlWhere for INSERT <%s>", pclFunc, pclSqlWhere );
        sprintf( pclTmpStr, "%s", "ADID,FLDA,FLNO,ALC2,ALC3,FLTN,FLNS" );
        strcat( pclFlightFields, pclTmpStr );
        sprintf( pclTmpStr, "%s,%s,%s,%s,%s,%s,%s", rpGeneric.ADID,rpGeneric.STDT, rpGeneric.FLNO, 
                                                          pclALC2, pclALC3, pclFLTN, pclFLTS );
        strcat( pclFlightValues, pclTmpStr );
    }
    else
    {
        pclFlightFields[strlen(pclFlightFields)-1] = '\0';
        pclFlightValues[strlen(pclFlightValues)-1] = '\0';
    }

    dbg( TRACE, "<%s> FlightFields <%s>", pclFunc, pclFlightFields );
    dbg( TRACE, "<%s> FlightValues <%s>", pclFunc, pclFlightValues );

    ilRc = SendCedaEvent(igFlightModid, 0, "tighdl", "EXCO", "tighdl", "", pclAction, "AFTTAB", pclSqlWhere,
                         pclFlightFields, pclFlightValues, "", igSendPriority, NETOUT_NO_ACK);
        
    dbg( TRACE, "<%s> Action <%s> ilRc <%d>", pclFunc, pclAction, ilRc );
    dbg(TRACE,"<%s> ---------- END ---------",pclFunc);
} /* HandleMQFlightData */



static int HandleMQCounterData( char *pcpData, INFOBJ_GENERIC rpGeneric )
{
    INFOBJ_COUNTER rlCounter;
    int ili;
    int ilNumber;
    int ilAction;
    int ilFound;
    int ilRc;
    char pclTmpStr[DBQRLEN] = "\0";     
    char pclData[DBQRLEN] = "\0";       
    char pclSqlWhere[DBQRLEN] = "\0";     
    char pclSqlBuf[DBQRLEN] = "\0";     
    char pclAction[10] = "\0";
    char pclSTOD[DTTMLEN+1] = "\0";
    char pclCounterFields[DBQRLEN] = "\0";
    char pclCounterValues[DBQRLEN] = "\0";
    char pclUpdateFields[DBQRLEN]  = "\0";
    char pclUpdateValues[DBQRLEN]  = "\0";
    char pclUAFT[12] = "\0";
    char pclFNR2[16] = "\0";
    char pclFNR3[16] = "\0";
    char pclALC2[16] = "\0";
    char pclALC3[16] = "\0";
    char pclFLTN[16] = "\0";
    char pclFLTS[16] = "\0";
    char pclFLTM[16] = "\0";   /* New field: Flight Time */
    char *pclFunc = "HandleMQCounterData";
 
    dbg(TRACE,"<%s> ---------- START ---------",pclFunc);
    
    memset( &rlCounter, 0, sizeof(INFOBJ_COUNTER) );

    if( rpGeneric.ACTN[0] == 'I' )      { strcpy( pclAction, "IRT" ); ilAction = TYPE_INSERT; }
    else if( rpGeneric.ACTN[0] == 'U' ) { strcpy( pclAction, "URT" ); ilAction = TYPE_UPDATE; }
    else if( rpGeneric.ACTN[0] == 'D' ) { strcpy( pclAction, "DRT" ); ilAction = TYPE_DELETE; }

    if( rpGeneric.ADID[0] != 'D' )
    {
        /* Only departure requires a check-in counter */
        dbg( TRACE, "<%s> Dunno what to do with this ADID <%s>", pclFunc, rpGeneric.ADID );
        return;
    }


    ilRc = BuildFlightNbr (rpGeneric.FLNO, pclFNR3, pclFNR2, pclALC3, pclALC2, pclFLTN, pclFLTS);
    if( ilRc == RC_FAIL )
    {
        dbg( TRACE, "<%s> Error in building flight number", pclFunc );
        return;
    }

    dbg( DEBUG, "<%s> FNR2 <%s>", pclFunc, pclFNR2 );
    dbg( DEBUG, "<%s> FNR3 <%s>", pclFunc, pclFNR3 );
    dbg( DEBUG, "<%s> ALC2 <%s>", pclFunc, pclALC2 );
    dbg( DEBUG, "<%s> ALC3 <%s>", pclFunc, pclALC3 );
    dbg( DEBUG, "<%s> FLTN <%s>", pclFunc, pclFLTN );
    dbg( DEBUG, "<%s> FLTS <%s>", pclFunc, pclFLTS );

    if( ilRc == 2 )
        strcpy( rpGeneric.FLNO, pclFNR2 );
    else if( ilRc == 3 )
        strcpy( rpGeneric.FLNO, pclFNR3 );
    else
    {
        dbg( TRACE, "<%s> Invalid RC from BuildFlightNbr <%d>", pclFunc, ilRc );
        return;
    }


    if( GetFlightUrno( rpGeneric, pclUAFT ) == FALSE )
    {
        dbg( TRACE, "<%s> No flight record found", pclFunc );
        return;
    }
    dbg( TRACE, "<%s>: Flight URNO <%s>", pclFunc, pclUAFT );


    sprintf( pclSqlBuf, "SELECT TRIM(STOD) FROM AFTTAB WHERE URNO = %s", pclUAFT );
    dbg( TRACE, "%s: STOD SqlBuf = <%s>", pclFunc, pclSqlBuf );

    ilRc = getOrPutDBData(pclSqlBuf,pclSTOD,START);
    if( ilRc != DB_SUCCESS )
        return FALSE;
    dbg( TRACE, "%s: Flight STOD <%s>", pclFunc, pclSTOD );

    ili = 1;
    ilFound = TRUE;
    while( ilFound == TRUE )
    {
        memset( &rlCounter, 0, sizeof(INFOBJ_COUNTER) );
        ilFound = xml_untag( pclData, pcpData, "INFOBJ_COUNTER", ili );
        if( ilFound == FALSE )
            break;
        dbg( TRACE, "<%s> Counter XML %d = (%s)", pclFunc, ili, pclData );
        ConvertTag(pclData);
        ili++;

        memset( pclUpdateFields, 0, sizeof(pclUpdateFields) );
        memset( pclUpdateValues, 0, sizeof(pclUpdateValues) );
        
        if(GetElementValue(rlCounter.CKIC, pclData, "<CKIC>", "</CKIC>") == TRUE && ilAction == TYPE_UPDATE) 
        {   strcat(pclUpdateFields, "CKIC,"); sprintf(pclTmpStr, "%s,", rlCounter.CKIC); strcat( pclUpdateValues, pclTmpStr ); }
        if(GetElementValue(rlCounter.CKIT, pclData, "<CKIT>", "</CKIT>") == TRUE && ilAction == TYPE_UPDATE) 
        {   strcat(pclUpdateFields, "CKIT,"); sprintf(pclTmpStr, "%s,", rlCounter.CKIT); strcat( pclUpdateValues, pclTmpStr ); }


        /* These 2 fields are mandatory */
        if( strlen(rlCounter.CKIC) <= 0 || strlen(rlCounter.CKIT) <= 0 )
        {
            dbg( TRACE, "<%s> Invalid CKIC/CKIT DATA = (%s)", pclFunc, pclData );
            continue;
        }
 
        strcpy( rlCounter.FLNO, rpGeneric.FLNO );
        strcpy( rlCounter.FLNU, pclUAFT );
        strcpy( rlCounter.STOD, pclSTOD );
        if( GetCounterRec( &rlCounter ) == FALSE )
        {
            dbg( TRACE, "<%s> No record found", pclFunc );
            /* If rec is not found, update request will be processed as INSERT */
            if( ilAction == TYPE_UPDATE )
            {
                ilAction = TYPE_INSERT;
                strcpy( pclAction, "IRT" );
            }
            if( ilAction != TYPE_INSERT )
                continue;
        }
        else
        {
            if( ilAction == TYPE_INSERT )
            {
                dbg( TRACE, "<%s> Record exist, abort insertion! URNO <%s>", pclFunc, rlCounter.URNO );
                continue;
            }
            sprintf( pclSqlWhere, "WHERE URNO = %s", rlCounter.URNO );
            dbg( TRACE, "<%s> pclSqlWhere <%s>", pclFunc, pclSqlWhere );
        }

        /* CKES and CKBS are verified in ACTION to decide whether to forward info to cdi2 i.e. fids_srv */
        /* Thus values are filled even when it is a delete */
        if( ilAction == TYPE_DELETE )
        {
            GetServerTimeStamp("LOC",1,0,rlCounter.LSTU);
            sprintf( pclCounterFields, "%s", "ACT3,CDAT,CGRU,CKBA,CKBS,CKEA,CKES,CKIC,CKIF,CKIT,"
                                             "COIX,COPN,CTYP,DISP,FLNO,FLNU,GHPU,GHSU,GPMU,LSTU,"
                                             "PRFL,REMA,STAT,STOD,URNO,USEC,USEU" );
            sprintf( pclCounterValues, "%s,%s,%s, ,%s, ,%s, , , ,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s, , ,%s,%s,%s,%s,%s",                      
                                      rlCounter.ACT3,rlCounter.CDAT,rlCounter.CGRU,
                                      rlCounter.CKBS,rlCounter.CKES,
                                      rlCounter.COIX,rlCounter.COPN,rlCounter.CTYP,
                                      rlCounter.DISP,rlCounter.FLNO,rlCounter.FLNU,
                                      rlCounter.GHPU,rlCounter.GHSU,rlCounter.GPMU,
                                      rlCounter.LSTU,rlCounter.STAT,rlCounter.STOD,
                                      rlCounter.URNO,mod_name,mod_name );
            ilRc = SendCedaEvent(igSqlxModid, 0, "tighdl", "EXCO", "tighdl", "", "URT", "CCATAB", pclSqlWhere,
                                 pclCounterFields, pclCounterValues, "", igSendPriority, NETOUT_NO_ACK);
            dbg( TRACE, "<%s> UPDATE Counter! ilRc = %d", pclFunc, ilRc );
            
            /* This wont work if there are >1 sqlhdl working. DELETE will always take the higher priority */
            /* Thus, change the design to DELETE only when UPDATE has been finished                       */
            /*ilRc = SendCedaEvent(igSqlxModid, 0, "tighdl", "EXCO", "tighdl", "", pclAction, "CCATAB", pclSqlWhere,
                                 "", "", "", igSendPriority, NETOUT_NO_ACK);
            dbg( TRACE, "<%s> DELETE Counter! ilRc = %d", pclFunc, ilRc );*/
            return ilRc;
        }
        
        
        if(GetElementValue(pclTmpStr, pclData, "<CKBS>", "</CKBS>") == TRUE )
        {
            strncpy( rlCounter.CKBS, pclTmpStr, DTTMLEN );
            if( ilAction == TYPE_UPDATE) 
            {   strcat(pclUpdateFields, "CKBS,"); sprintf(pclTmpStr, "%s,", rlCounter.CKBS); strcat( pclUpdateValues, pclTmpStr ); }
        }

        if(GetElementValue(pclTmpStr, pclData, "<CKES>", "</CKES>") == TRUE )
        {
            strncpy( rlCounter.CKES, pclTmpStr, DTTMLEN );
            if( ilAction == TYPE_UPDATE) 
            {   strcat(pclUpdateFields, "CKES,"); sprintf(pclTmpStr, "%s,", rlCounter.CKES); strcat( pclUpdateValues, pclTmpStr ); }
        }

        if(GetElementValue(rlCounter.DISP, pclData, "<DISP>", "</DISP>") == TRUE && ilAction == TYPE_UPDATE) 
        {   strcat(pclUpdateFields, "DISP,"); sprintf(pclTmpStr, "%s,", rlCounter.DISP); strcat( pclUpdateValues, pclTmpStr ); }

        if(GetElementValue(rlCounter.REMA, pclData, "<REMA>", "</REMA>") == TRUE && ilAction == TYPE_UPDATE) 
        {   strcat(pclUpdateFields, "REMA,"); sprintf(pclTmpStr, "%s,", rlCounter.REMA); strcat( pclUpdateValues, pclTmpStr ); }

 
        /* These 2 fields are mandatory */
        if( ilAction == TYPE_INSERT &&
            (strlen(rlCounter.CKBS) <= 0 || strlen(rlCounter.CKES) <= 0) )
        {
            dbg( TRACE, "<%s> Invalid CKBS/CKES for insert. DATA = (%s)", pclFunc, pclData );
            continue;
        }
        dbg( DEBUG, "<%s> CKIC: <%s>", pclFunc, rlCounter.CKIC);
        dbg( DEBUG, "<%s> CKIT: <%s>", pclFunc, rlCounter.CKIT);
        dbg( DEBUG, "<%s> CKBS: <%s>", pclFunc, rlCounter.CKBS);
        dbg( DEBUG, "<%s> CKES: <%s>", pclFunc, rlCounter.CKES);
        dbg( DEBUG, "<%s> DISP: <%s>", pclFunc, rlCounter.DISP);
        dbg( DEBUG, "<%s> REMA: <%s>", pclFunc, rlCounter.REMA);

        if( ilAction == TYPE_INSERT )
        {

            sprintf( pclCounterFields, "%s", "FLNO,FLNU,STOD," );
            sprintf( pclCounterValues, "%s,%s,%s,", rpGeneric.FLNO, rlCounter.FLNU, pclSTOD);

            /* 7 fields */
            strcat( pclCounterFields, pcgCounterFields );
            sprintf( pclTmpStr, "%s,%s,%s,%s,%s,%s",     
                                rlCounter.CKIC, rlCounter.CKIT,
                                rlCounter.CKBS, rlCounter.CKES, rlCounter.DISP,
                                rlCounter.REMA );
            strcat( pclCounterValues, pclTmpStr );

        }
        else if( ilAction == TYPE_UPDATE )
        {
            if( strlen(pclUpdateFields) > 0 )
            {
                sprintf( pclCounterFields, "%s", pclUpdateFields );    /* Excluding the comma in the front of str */
                sprintf( pclCounterValues, "%s", pclUpdateValues );    /* Excluding the comma in the front of str */
                pclCounterFields[strlen(pclUpdateFields)-1] = '\0'; /* Including the comma at the end of str */
                pclCounterValues[strlen(pclUpdateValues)-1] = '\0'; /* Including the comma at the end of str */
            }
            else
            {
                dbg( TRACE, "<%s> Nothing to update", pclFunc );
                continue;
            }
        }
        dbg( TRACE, "<%s> CounterFields <%s>", pclFunc, pclCounterFields );
        dbg( TRACE, "<%s> CounterValues <%s>", pclFunc, pclCounterValues );
        if( ilAction == TYPE_INSERT )
            ilRc = SendCedaEvent(igSqlModid, 0, "tighdl", "EXCO", "tighdl", "", pclAction, "CCATAB", "",
                                 pclCounterFields, pclCounterValues, "", igSendPriority, NETOUT_NO_ACK);
        else
            ilRc = SendCedaEvent(igSqlModid, 0, "tighdl", "EXCO", "tighdl", "", pclAction, "CCATAB", pclSqlWhere,
                                 pclCounterFields, pclCounterValues, "", igSendPriority, NETOUT_NO_ACK);
        dbg( TRACE, "<%s> Action <%s> ilRc <%d>", pclFunc, pclAction, ilRc );
    } /* while xml_untag loop */
    dbg(TRACE,"<%s> ---------- END ---------",pclFunc);
} /* HandleMQCounterData */


static void HandleUfisFlightData(char *pcpFields, char *pcpData, char *pcpCmd, char *pcpSelection)
{
    INFOBJ_GENERIC rlGeneric;
    BOOL blInformTIG = FALSE;
    BOOL blTimeField = FALSE;
    BOOL blNoFLDA = FALSE;
    int ilRc = RC_SUCCESS;
    int ilItemCount = 0;
    int ilItemPosition = 0;
    int ili, ilj;
    int ilAction;
    int ilSequenceNo;
    char pclUrno[URNOLEN+1];
    char pclSqlBuf[DBQRLEN] = "\0";     
    char pclTmpStr[DBQRLEN] = "\0";     
    char pclTmpStr2[DBQRLEN] = "\0";     
    char pclData[2][DBQRLEN];
    char pclInfo[2][500];
    char pclTimeFields[14][5] = { "ONBL","OFBL","GA1X","GA2X",
                                  "GD1X","GD2X","GA1Y","GA2Y",
                                  "GD1Y","GD2Y","B1BA","B1EA",
                                  "B2BA","B2EA" };

    char *pclFunc = "HandleUfisFlightData";
    char *pclToken;

    dbg(TRACE,"<%s> ---------- START ---------",pclFunc);

    memset( pclInfo, 0, sizeof(pclInfo) );
    memset( pclData, 0, sizeof(pclData) );
    memset( &rlGeneric, 0, sizeof(INFOBJ_GENERIC) );

    strcpy( pclData[0], pcpData );
    pclToken = (char *)strstr( pclData[0], "\n" );
    if( pclToken != NULL )
    {
        *pclToken = '\0';
        pclToken++;
        strcpy( pclData[1], pclToken );
    }
    dbg(DEBUG,"<%s> Str1 <%s>",pclFunc, pclData[0] );
    dbg(DEBUG,"<%s> Str2 <%s>",pclFunc, pclData[1] );

    if( !strncmp( pcpCmd, "DFR", 3 ) || !strncmp(pcpCmd, "DRT", 3) )
        ilAction = TYPE_DELETE;
    else if( !strncmp( pcpCmd, "IFR", 3 ) || !strncmp(pcpCmd, "IRT",3) )
        ilAction = TYPE_INSERT;
    else if( !strncmp( pcpCmd, "UFR", 3 ) || !strncmp(pcpCmd, "URT",3) )
        ilAction = TYPE_UPDATE;
    else
    {
        dbg( TRACE, "<%s> Invalid ACTIONTYPE Cmd <%s>", pclFunc, pcpCmd );
        return;
    }


    /* Prepare header information. Assuming data remained unchanged */
    memset( &rlGeneric, 0, sizeof(INFOBJ_GENERIC) );

    /* ADID */
    ilItemPosition = get_item_no(pcpFields,"ADID",5);
    if( ilItemPosition < 0 )
    {
        dbg(TRACE,"<%s> ADID not found <%s>",pclFunc, pcpFields );
        return;
    }
    get_real_item(pclInfo[0], pclData[0], ilItemPosition+1);
    strcpy( rlGeneric.ADID, pclInfo[0] );
    dbg( TRACE, "<%s> ADID = <%s>", pclFunc, rlGeneric.ADID );

    /* STDT */
    blNoFLDA = FALSE;
    ilItemPosition = get_item_no(pcpFields,"FLDA",5);
    if( ilItemPosition < 0 )
    {
        dbg(TRACE,"<%s> FLDA not found <%s>",pclFunc, pcpFields );
        blNoFLDA = TRUE;
    }
    else
    {
        get_real_item(pclInfo[0], pclData[0], ilItemPosition+1);
        if( strlen(pclInfo[0]) <= 0 )
        {
            dbg(TRACE,"<%s> No FLDA values found <%s>",pclFunc, pclInfo[0] );
            blNoFLDA = TRUE;
        }
    }
    /* Get STOA/STOD instead */
    if( blNoFLDA == TRUE )
    {
        if( rlGeneric.ADID[0] == 'A' )
            ilItemPosition = get_item_no(pcpFields,"STOA",5);
        else
            ilItemPosition = get_item_no(pcpFields,"STOD",5);
        if( ilItemPosition < 0 )
        {
            dbg(TRACE,"<%s> STOA/STOD not found <%s>",pclFunc, pcpFields );
            return;
        }
        get_real_item(pclInfo[0], pclData[0], ilItemPosition+1);
    }
    sprintf( rlGeneric.STDT, "%8.8s", pclInfo[0] );
    dbg( TRACE, "<%s> FLDA aka STDT = <%s>", pclFunc, rlGeneric.STDT );

    /* FLNO */
    ilItemPosition = get_item_no(pcpFields,"FLNO",5);
    if( ilItemPosition < 0 )
    {
        dbg(TRACE,"<%s> FLNO not found <%s>",pclFunc, pcpFields );
        return;
    }
    get_real_item(pclInfo[0], pclData[0], ilItemPosition+1);
    strcpy( rlGeneric.FLNO, pclInfo[0] );
    dbg( TRACE, "<%s> FLNO = <%s>", pclFunc, rlGeneric.FLNO );


    sprintf( pcgXmlStr, "%s\n<MSGSTREAM_OUT>\n", pcgXmlHeader );
    if( PackHeaderXml( pclTmpStr, rlGeneric, ilAction, &ilSequenceNo ) == FALSE )
        return;
    strcat( pcgXmlStr, pclTmpStr );

    if( ilAction == TYPE_DELETE )
    {
        sprintf( pclTmpStr, "%s", "</MSGSTREAM_OUT>\n"
                                  "</MSG>\n" );
        strcat( pcgXmlStr, pclTmpStr );
        dbg( TRACE, "<%s> DELETE! Xml (%s)", pclFunc, pcgXmlStr );
        InformTIG( pcgXmlStr, ilSequenceNo );
        return;
    }        

    /* Insert and Update */
    sprintf( pclTmpStr, "%s", "<MSGOBJECTS>\n"
                              "<INFOBJ_FLIGHT>\n" );
    strcat( pcgXmlStr, pclTmpStr );
    for( ili = 0; ili < 26; ili++ )
    {
            ilItemPosition = get_item_no(pcpFields,pcgOutFlightFields[ili],5);
            if( ilItemPosition < 0 )
            {
                /*sprintf( pclTmpStr, "<%s></%s>", pcgOutFlightFields[ili], pcgOutFlightFields[ili] );
                strcat( pcgXmlStr, pclTmpStr );*/
                continue;
            }
            get_real_item(pclInfo[1], pclData[0], ilItemPosition+1);
            get_real_item(pclInfo[0], pclData[1], ilItemPosition+1);
            dbg(DEBUG,"<%s> Item <%d> Field <%s> OValue <%s> NValues <%s>",pclFunc, ili, 
                                                             pcgOutFlightFields[ili], pclInfo[0], pclInfo[1]);
            if( ilAction == TYPE_UPDATE && !strcmp( pclInfo[0], pclInfo[1] ) )
                continue;
            blInformTIG = TRUE;

            memset( pclTmpStr, 0, sizeof(pclTmpStr) );
            if( ConvertSpecialChar( pclInfo[1], pclTmpStr ) == TRUE )
            {
                sprintf( pclInfo[1], "%s", pclTmpStr );
                dbg( TRACE, "<%s> after conversion, val <%s>", pclFunc, pclInfo[1] );
            }

            blTimeField = FALSE;
            for( ilj = 0; ilj < 14; ilj++ )
            {
                if( strncmp(pcgOutFlightFields[ili], pclTimeFields[ilj], 4 ) )
                    continue;
                if( strlen(pclInfo[1]) > 0 )
                    sprintf( pclTmpStr, "<%s>%14.14s+%2.2d</%s>\n", pcgOutFlightFields[ili], pclInfo[1], igHourDifference,
                                                               pcgOutFlightFields[ili] );             
                else
                    sprintf( pclTmpStr, "<%s>%17.17s</%s>\n", pcgOutFlightFields[ili], CHAR_BLANK, pcgOutFlightFields[ili] );             
                strcat( pcgXmlStr, pclTmpStr );
                blTimeField = TRUE;
                break;
            }
            if( blTimeField == TRUE )
                continue;

            /* No choice has to hard code field names, as these names are differnet from DB */
            if( !strncmp(pcgOutFlightFields[ili], "TMB1", 4 ) )
                sprintf( pclTmpStr, "<BLL1>%s</BLL1>\n", pclInfo[1] );
            if( !strncmp(pcgOutFlightFields[ili], "TMB2", 4 ) )
                sprintf( pclTmpStr, "<BLL2>%s</BLL2>\n", pclInfo[1] );
            else
                sprintf( pclTmpStr, "<%s>%s</%s>\n", pcgOutFlightFields[ili], pclInfo[1], pcgOutFlightFields[ili] );
            strcat( pcgXmlStr, pclTmpStr );
    }
    sprintf( pclTmpStr, "%s", "</INFOBJ_FLIGHT>\n"
                              "</MSGOBJECTS>\n"
                              "</MSGSTREAM_OUT>\n"
                              "</MSG>\n" );
    strcat( pcgXmlStr, pclTmpStr );
    dbg( TRACE, "<%s> Xml (%s)", pclFunc, pcgXmlStr );
    if( blInformTIG == TRUE )
        InformTIG( pcgXmlStr, ilSequenceNo );
    else
        dbg( TRACE, "<%s> Nothing to inform", pclFunc );

    dbg(TRACE,"<%s> ---------- END ---------",pclFunc);

} /* HandleUfisFlightData */


/* Mandatory fields are CKIC, CKBA and CKEA */
/* This dun work for DELETE as Flight info has already been deleted from AFTTAB */
static void HandleUfisCounterData( char *pcpFields, char *pcpData, char *pcpCmd, char *pcpSelection )
{
    INFOBJ_GENERIC rlGeneric;
    BOOL blDeleteCounter = FALSE;
    BOOL blInsertCounter = FALSE;
    BOOL blInformTIG = FALSE;
    int ilRc = RC_SUCCESS;
    int ilItemCount = 0;
    int ilItemPosition = 0;
    int ili, ilj;
    int ilAction;
    int ilSequenceNo;
    char pclOutCounterFields[2][5] = { "CKIC", "CKIT" };
    char pclTimeFields[2][5] = { "CKBA", "CKEA" };
    char pclTmpStr[DBQRLEN] = "\0";     
    char pclSqlBuf[DBQRLEN] = "\0";     
    char pclSqlWhere[100];
    char pclUrno[URNOLEN+1] = "\0";
    char pclData[2][DBQRLEN];
    char pclInfo[2][100];

    char *pclFunc = "HandleUfisCounterData";
    char *pclToken;

    dbg(TRACE,"<%s> ---------- START ---------",pclFunc);

    memset( pclInfo, 0, sizeof(pclInfo) );
    memset( pclData, 0, sizeof(pclData) );
    memset( &rlGeneric, 0, sizeof(INFOBJ_GENERIC) );

    strcpy( pclData[0], pcpData );
    pclToken = (char *)strstr( pclData[0], "\n" );
    if( pclToken != NULL )
    {
        *pclToken = '\0';
        pclToken++;
        strcpy( pclData[1], pclToken );
    }
    dbg(DEBUG,"<%s> Str1 <%s>",pclFunc, pclData[0] );
    dbg(DEBUG,"<%s> Str2 <%s>",pclFunc, pclData[1] );

    if( !strncmp(pcpCmd, "URT",3) )
    {
        if( pcpSelection != NULL )
        {
            dbg( TRACE, "<%s> check CKIC, is it an URT of erasing CKIC", pclFunc );
            ilItemPosition = get_item_no(pcpFields,"CKIC",5);
            if( ilItemPosition >= 0 )
            {
                get_real_item(pclInfo[0], pclData[0], ilItemPosition+1);
                if( strlen(pclInfo[0]) <= 0 )
                {
                    ilRc = SendCedaEvent(igSqlxModid, 0, "tighdl", "EXCO", "tighdl", "", "DRT", "CCATAB", pcpSelection,
                                         "", "", "", igSendPriority, NETOUT_NO_ACK);
                    dbg( TRACE, "<%s> DELETE Counter! ilRc = %d", pclFunc, ilRc );
                    return;
                }
            }
        }
    }


    /* Prepare header information */
    memset( &rlGeneric, 0, sizeof(INFOBJ_GENERIC) );
    
    /* FLNU which is the URNO of AFTTAB */
    ilItemPosition = get_item_no(pcpFields,"FLNU",5);
    if( ilItemPosition < 0 )
    {
         dbg(TRACE,"<%s> FLNU not found <%s>",pclFunc, pcpFields );
         return;
    }
    get_real_item(pclInfo[0], pclData[0], ilItemPosition+1);
    strcpy( pclUrno, pclInfo[0] );
    dbg( TRACE, "<%s> FLNU - aka UAFT = <%s>", pclFunc, pclUrno );

    sprintf( pclSqlBuf, "SELECT TRIM(ADID), TRIM(FLNO), TRIM(FLDA) FROM AFTTAB WHERE URNO = %s", pclUrno );
    dbg( TRACE, "<%s> SqlBuf <%s>", pclFunc, pclSqlBuf );
    ilRc = getOrPutDBData(pclSqlBuf, pclTmpStr, START);
    if( ilRc != DB_SUCCESS )
    {
        dbg( TRACE, "<%s> FLNU <%s> not found in AFTTAB", pclFunc, pclUrno );
        return;
    }
    get_fld(pclTmpStr,FIELD_1,STR,ADIDLEN,rlGeneric.ADID);
    get_fld(pclTmpStr,FIELD_2,STR,FLNOLEN,rlGeneric.FLNO);
    get_fld(pclTmpStr,FIELD_3,STR,DATELEN,rlGeneric.STDT);  

    dbg( DEBUG, "<%s> ADID = <%s>", pclFunc, rlGeneric.ADID );
    dbg( DEBUG, "<%s> FLNO = <%s>", pclFunc, rlGeneric.FLNO );
    dbg( DEBUG, "<%s> STDT = <%s>", pclFunc, rlGeneric.STDT );


    /* Not possible, all clearing off is send as URT */
    /*if( !strncmp( pcpCmd, "DFR", 3 ) || !strncmp(pcpCmd, "DRT",3) )
        ilAction = TYPE_DELETE; */
    if( !strncmp( pcpCmd, "IFR", 3 ) || !strncmp(pcpCmd, "IRT",3) )
        ilAction = TYPE_INSERT;
    else if( !strncmp( pcpCmd, "UFR", 3 ) || !strncmp(pcpCmd, "URT",3) )
        ilAction = TYPE_UPDATE;
    else
    {
        dbg( TRACE, "<%s> Invalid ACTIONTYPE Cmd <%s>", pclFunc, pcpCmd );
        return;
    }

    if( ilAction == TYPE_UPDATE )
    {
        /* Replace counter */
        blDeleteCounter = FALSE;
        for( ili = 0; ili < 2; ili++ )
        {
            ilItemPosition = get_item_no(pcpFields,pclOutCounterFields[ili],5);
            if( ilItemPosition < 0 )
                return;
            get_real_item(pclInfo[0], pclData[0], ilItemPosition+1);
            get_real_item(pclInfo[1], pclData[1], ilItemPosition+1);
            if( strcmp(pclInfo[0],pclInfo[1]) ) /* Different Counter */
            {
                blDeleteCounter = TRUE;
                break;
            }
        }
    }

    blInformTIG = FALSE;
    if( ilAction == TYPE_INSERT  || (ilAction == TYPE_UPDATE && blDeleteCounter == FALSE))
    {
        if( ilAction == TYPE_INSERT )
            blInformTIG = TRUE;

        sprintf( pcgXmlStr, "%s\n<MSGSTREAM_OUT>\n", pcgXmlHeader );
        if( PackHeaderXml( pclTmpStr, rlGeneric, ilAction, &ilSequenceNo ) == FALSE )
            return;
        strcat( pcgXmlStr, pclTmpStr );
        
        sprintf( pclTmpStr, "%s", "<MSGOBJECTS>\n"
                                  "<INFOBJ_COUNTER>\n" );
        strcat( pcgXmlStr, pclTmpStr );

        for( ili = 0; ili < 2; ili++ )
        {
            ilItemPosition = get_item_no(pcpFields,pclOutCounterFields[ili],5);
            if( ilItemPosition < 0 )
                return;
            get_real_item(pclInfo[0], pclData[0], ilItemPosition+1);
            sprintf( pclTmpStr, "<%s>%s</%s>\n", pclOutCounterFields[ili], pclInfo[0], pclOutCounterFields[ili] );
            strcat( pcgXmlStr, pclTmpStr );
        }
        for( ili = 0; ili < 2; ili++ )
        {
            ilItemPosition = get_item_no(pcpFields,pclTimeFields[ili],5);
            if( ilItemPosition < 0 )
                return;
            get_real_item(pclInfo[0], pclData[0], ilItemPosition+1);
            if( ilAction == TYPE_UPDATE )
            {
                get_real_item(pclInfo[1], pclData[1], ilItemPosition+1);
                if( !strcmp(pclInfo[0], pclInfo[1]) )
                    continue;
                blInformTIG = TRUE;
            }
            if( strlen(pclInfo[0]) > 0 )
                sprintf( pclTmpStr, "<%s>%14.14s+%2.2d</%s>\n", pclTimeFields[ili], pclInfo[0], igHourDifference, pclTimeFields[ili] );             
            else
                sprintf( pclTmpStr, "<%s>%17.17s</%s>\n", pclTimeFields[ili], CHAR_BLANK, pclTimeFields[ili] );
            strcat( pcgXmlStr, pclTmpStr );
        }
        sprintf( pclTmpStr, "%s", "</INFOBJ_COUNTER>\n"
                                  "</MSGOBJECTS>\n"
                                  "</MSGSTREAM_OUT>\n"
                                  "</MSG>\n" );
        strcat( pcgXmlStr, pclTmpStr );
        if( blInformTIG == TRUE )
            InformTIG( pcgXmlStr, ilSequenceNo );
        dbg(TRACE,"<%s> ---------- END ---------",pclFunc);
        return;
    }


    blInsertCounter = FALSE;
    if( blDeleteCounter == TRUE )
    {
        /* Check whether counter is really being deleted or there is jus a reshuffling of counter info in ascending order */
        sprintf( pclSqlBuf, "SELECT URNO FROM CCATAB WHERE FLNU = %s AND CKIC = '%s'", pclUrno, pclInfo[1] );
        dbg( TRACE, "<%s> SqlBuf <%s>", pclFunc, pclSqlBuf );
        ilRc = getOrPutDBData(pclSqlBuf, pclTmpStr, START);
        if( ilRc == DB_SUCCESS )
        {
            dbg( TRACE, "<%s> COUNTER <%s> is in CCATAB. Dun delete. Duplicate entry of counters occur.", pclFunc, pclInfo[1] );
            blDeleteCounter = FALSE;
        }

        if( blDeleteCounter == TRUE )
        {
            dbg( TRACE, "<%s> DELETE old counter <%s> first", pclFunc, pclInfo[1] );
            sprintf( pcgXmlStr, "%s\n<MSGSTREAM_OUT>\n", pcgXmlHeader );
            if( PackHeaderXml( pclTmpStr, rlGeneric, TYPE_DELETE, &ilSequenceNo ) == FALSE )
                return;
            strcat( pcgXmlStr, pclTmpStr );

            sprintf( pclTmpStr, "%s", "<MSGOBJECTS>\n"
                                      "<INFOBJ_COUNTER>\n" );
            strcat( pcgXmlStr, pclTmpStr );
        }

        for( ili = 0; ili < 2; ili++ )
        {
            ilItemPosition = get_item_no(pcpFields,pclOutCounterFields[ili],5);
            get_real_item(pclInfo[1], pclData[1], ilItemPosition+1);
            get_real_item(pclInfo[0], pclData[0], ilItemPosition+1);
            if( strlen(pclInfo[0]) > 0 )
                blInsertCounter = TRUE;
            sprintf( pclTmpStr, "<%s>%s</%s>\n", pclOutCounterFields[ili], pclInfo[1], pclOutCounterFields[ili] );
            strcat( pcgXmlStr, pclTmpStr );
        }

        if( blDeleteCounter == TRUE )
        {
            sprintf( pclTmpStr, "%s", "</INFOBJ_COUNTER>\n"
                                      "</MSGOBJECTS>\n"
                                      "</MSGSTREAM_OUT>\n"
                                      "</MSG>\n" );
            strcat( pcgXmlStr, pclTmpStr );
            InformTIG( pcgXmlStr, ilSequenceNo );
        }

        if( blInsertCounter == FALSE )
            return;

        dbg( TRACE, "<%s> Insert new counter <%s>", pclInfo[1] );
        sprintf( pcgXmlStr, "%s\n<MSGSTREAM_OUT>\n", pcgXmlHeader );
        if( PackHeaderXml( pclTmpStr, rlGeneric, TYPE_INSERT, &ilSequenceNo ) == FALSE )
            return;
        strcat( pcgXmlStr, pclTmpStr );

        sprintf( pclTmpStr, "%s", "<MSGOBJECTS>\n"
                                  "<INFOBJ_COUNTER>\n" );
        strcat( pcgXmlStr, pclTmpStr );

        for( ili = 0; ili < 2; ili++ )
        {
            ilItemPosition = get_item_no(pcpFields,pclOutCounterFields[ili],5);
            get_real_item(pclInfo[0], pclData[0], ilItemPosition+1);
            sprintf( pclTmpStr, "<%s>%s</%s>\n", pclOutCounterFields[ili], pclInfo[0], pclOutCounterFields[ili] );
            strcat( pcgXmlStr, pclTmpStr );
        }
        sprintf( pclTmpStr, "%s", "</INFOBJ_COUNTER>\n"
                                  "</MSGOBJECTS>\n"
                                  "</MSGSTREAM_OUT>\n"
                                  "</MSG>\n" );
        strcat( pcgXmlStr, pclTmpStr );
        InformTIG( pcgXmlStr, ilSequenceNo );
        dbg(TRACE,"<%s> ---------- END ---------",pclFunc);
        return;
    }

                                    
} /* HandleUfisCounterData */


static int GetElementValue(char *pcpDest, char *pcpOrigData, char *pcpBegin, char *pcpEnd)
{
    long llSize = 0;
    char pclTmpStr[DBQRLEN] = "\0";
    char pclEnd[100] = "\0";
    char *pclTemp = NULL;
    char *pclFunc = "GetElementValue";
    
    pclTemp = CedaGetKeyItem(pcpDest, &llSize, pcpOrigData, pcpBegin, pcpEnd, TRUE);
    if(pclTemp != NULL)
        return TRUE;
    else
    {
        strncpy( pclTmpStr, &pcpEnd[2], strlen(pcpEnd)-3 );    /* </INFOBJ_FLIGHT> */
        sprintf( pclEnd, "<%s/>", pclTmpStr );                 /* <INFOBJ_FLIGHT/> */  
        /*dbg( DEBUG, "<%s> pclEnd <%s>", pclFunc, pclEnd );*/
        strcpy( pclTmpStr, pcpOrigData );
        pclTemp = (char *)strstr(pclTmpStr, pclEnd);
        if(pclTemp != NULL)
        {
            strcpy( pclTemp, " " );
            return TRUE;
        }
        else
            return FALSE;
    }
}

static int TriggerAction(char *pcpTableName)
{
  int           ilRc          = RC_SUCCESS ;    /* Return code */
  EVENT        *prlEvent      = NULL ;
  BC_HEAD      *prlBchead     = NULL ;
  CMDBLK       *prlCmdblk     = NULL ;
  /*ACTIONConfig *prlAction     = NULL ;*/
    ACTIONConfig rlAction;
    
  char         *pclSelection  = NULL ;
  char         *pclFields     = NULL ;
  char         *pclData       = NULL ;
  long          llActSize     = 0 ;
  int           ilAnswerQueue = 0 ;
  char          clQueueName[16] ;
    
  if(ilRc == RC_SUCCESS)
  {

  }/* end of if */
    

  if (ilRc == RC_SUCCESS)
  {
   sprintf (&clQueueName[0],"%s2", mod_name) ;

   ilRc = GetDynamicQueue(&ilAnswerQueue,&clQueueName[0]);
   if (ilRc != RC_SUCCESS)
   {
    dbg(TRACE,"TriggerAction: GetDynamicQueue failed <%d>",ilRc);
   }
   else
   {
    dbg(DEBUG,"TriggerAction: got <%d> for <%s>",ilAnswerQueue, &clQueueName[0]);
   }/* end of if */
  }/* end of if */
    
  if (ilRc == RC_SUCCESS)
  {
   llActSize = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + sizeof(ACTIONConfig) + 666;
   prgOutEvent = realloc(prgOutEvent,llActSize);
   if(prgOutEvent == NULL)
   {
    dbg(TRACE,"TriggerAction: realloc out event <%d> bytes failed",llActSize);
    ilRc = RC_FAIL ;
   } /* end of if */
  } /* end of if */

  if(ilRc == RC_SUCCESS)
  {
   prlBchead     = (BC_HEAD *) ((char *) prgOutEvent + sizeof(EVENT)) ;
   prlCmdblk     = (CMDBLK *)  ((char *) prlBchead->data) ;
   pclSelection  = prlCmdblk->data ;
   pclFields     = pclSelection + strlen (pclSelection) + 1 ;
   pclData       = pclFields + strlen (pclFields) + 1 ;
   strcpy (pclData, "DYN") ;
    
        memset(&rlAction,0,sizeof(ACTIONConfig));
        
    prlBchead->rc = RC_SUCCESS;
    
   prgOutEvent->type        = USR_EVENT ;
   prgOutEvent->command     = EVENT_DATA ;
   prgOutEvent->originator  = ilAnswerQueue ;
   prgOutEvent->retry_count = 0 ;
   prgOutEvent->data_offset = sizeof(EVENT) ;
   prgOutEvent->data_length = llActSize-sizeof(EVENT) ;

   rlAction.iEmptyFieldHandling = iDELETE_ALL_BLANKS ;
   rlAction.iIgnoreEmptyFields = 0 ;
   strcpy(rlAction.pcSndCmd, "") ;
   sprintf(rlAction.pcSectionName,"%s_%s", mod_name,pcpTableName) ;
   strcpy(rlAction.pcTableName, pcpTableName) ;
   strcpy(rlAction.pcFields, "-") ;
     /*********************** MCU TEST ***************/
   strcpy(rlAction.pcFields, "") ;
     /*********************** MCU TEST ***************/
     /************/
     if (!strcmp(pcpTableName,"SPRTAB"))
        {
    strcpy(rlAction.pcFields, "PKNO,USTF,ACTI,FCOL") ;
     }
     /************/
   strcpy(rlAction.pcSectionCommands, "IRT,URT,DRT") ;
   rlAction.iModID = mod_id ;

   /****************************************
     DebugPrintEvent(DEBUG,prgOutEvent) ;
     DebugPrintBchead(DEBUG,prlBchead) ;
     DebugPrintCmdblk(DEBUG,prlCmdblk) ;
     dbg (DEBUG,"TriggerAction: selection <%s>",pclSelection) ;
     dbg (DEBUG,"TriggerAction: fields    <%s>",pclFields) ;
     dbg (DEBUG,"TriggerAction: data      <%s>",pclData) ;
     DebugPrintACTIONConfig(DEBUG,&rlAction) ;
   ****************************************/

   if(ilRc == RC_SUCCESS)
   {
    rlAction.iADFlag = iDELETE_SECTION ;
             memcpy(pclData + strlen(pclData) + 1,&rlAction,sizeof(ACTIONConfig));

    ilRc = que(QUE_PUT, 7400, ilAnswerQueue, PRIORITY_3, llActSize, (char *) prgOutEvent) ; 
    if(ilRc != RC_SUCCESS)
    {
     dbg(TRACE,"TriggerAction: QUE_PUT failed <%d>",ilRc) ;
    }
    else
    {
     ilRc = WaitAndCheckQueue(10, ilAnswerQueue,&prgItem) ;
     if(ilRc != RC_SUCCESS)
     {
      dbg(TRACE,"TriggerAction: WaitAndCheckQueue failed <%d>",ilRc) ;
     }
     else
     {
      /* return value for delete does not matter !!! */
        
      /****************************************
      prlEvent     = (EVENT *)   ((char *)prgItem->text) ;
      prlBchead    = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT)) ;
      prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data) ;
      pclSelection = (char *)    prlCmdblk->data ;
      pclFields    = (char *)    pclSelection + strlen(pclSelection) + 1;
      pclData      = (char *)    pclFields + strlen(pclFields) + 1;
      DebugPrintItem(DEBUG, prgItem) ;
      DebugPrintEvent(DEBUG, prlEvent) ;
      DebugPrintBchead(DEBUG, prlBchead) ;
      DebugPrintCmdblk(DEBUG, prlCmdblk) ;
      dbg(DEBUG,"TriggerAction: selection <%s>",pclSelection) ;
      dbg(DEBUG,"TriggerAction: fields    <%s>",pclFields) ;
      dbg(DEBUG,"TriggerAction: data      <%s>",pclData) ;
      ****************************************/
    } /* end of if */
   }/* end of if */
  }/* end of if */

  if(ilRc == RC_SUCCESS)
  {
    rlAction.iADFlag = iADD_SECTION ;
      memcpy(pclData + strlen(pclData) + 1,&rlAction,sizeof(ACTIONConfig));
         
   ilRc = que(QUE_PUT, 7400, ilAnswerQueue, PRIORITY_3, llActSize,
                                              (char *) prgOutEvent ) ;
   if(ilRc != RC_SUCCESS)
   {
    dbg(TRACE,"TriggerAction: QUE_PUT failed <%d>",ilRc) ;
   }
   else
   {
    ilRc = WaitAndCheckQueue(10,ilAnswerQueue,&prgItem) ;
    if(ilRc != RC_SUCCESS)
    {
     dbg(TRACE,"TriggerAction: WaitAndCheckQueue failed <%d>",ilRc) ;
    }
    else
    {
     prlEvent     = (EVENT *)   ((char *)prgItem->text) ;
     prlBchead    = (BC_HEAD *) ((char *)prlEvent + sizeof(EVENT)) ;
     prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data) ;
     pclSelection = (char *)    prlCmdblk->data ;
     pclFields    = (char *)    pclSelection + strlen(pclSelection) + 1 ;
     pclData      = (char *)    pclFields + strlen(pclFields) + 1 ;

     if(strcmp(pclData,"SUCCESS") != 0)
     {
      dbg(TRACE,"TriggerAction: add dynamic action config failed") ;
      DebugPrintItem(DEBUG,prgItem) ;
      DebugPrintEvent(DEBUG,prlEvent) ;
      DebugPrintBchead(DEBUG,prlBchead) ;
      DebugPrintCmdblk(DEBUG,prlCmdblk) ;
      dbg (DEBUG,"TriggerAction: selection <%s>",pclSelection) ;
      dbg (DEBUG,"TriggerAction: fields    <%s>",pclFields) ;
      dbg(DEBUG,"TriggerAction: data      <%s>",pclData) ;
      ilRc = RC_FAIL ;
     }
     else
     {
      dbg(DEBUG,"TriggerAction: add dynamic action config OK") ;
     }/* end of if */
    }/* end of if */
   }/* end of if */
  }/* end of if */

  ilRc = que(QUE_DELETE,ilAnswerQueue,ilAnswerQueue,0,0,0) ;
  if(ilRc != RC_SUCCESS)
  {
   dbg(TRACE,"que QUE_DELETE <%d> failed <%d>",ilAnswerQueue, ilRc) ;
  }
  else
  {
   dbg(DEBUG,"TriggerAction: queue <%d> <%s> deleted", ilAnswerQueue, &clQueueName[0]) ;
  }/* end of if */
 }/* end of if */

 return (ilRc) ;
    
}
static int WaitAndCheckQueue(int ipTimeout, int ipModId, ITEM **prpItem)
{
    int ilRc          = RC_SUCCESS;                      /* Return code */
    int ilWaitCounter = 0;

    do
    {
        sleep(1);
        ilWaitCounter += 1;

        ilRc = CheckQueue(ipModId,prpItem);
        if(ilRc != RC_SUCCESS)
        {
            if(ilRc != QUE_E_NOMSG)
            {
                dbg(TRACE,"WaitAndCheckQueue: CheckQueue <%d> failed <%d>",mod_id,ilRc);
            }/* end of if */
        }/* end of if */
    }while((ilRc == QUE_E_NOMSG) && (ilWaitCounter < ipTimeout));

    if(ilWaitCounter >= ipTimeout)
    {
        dbg(TRACE,"WaitAndCheckQueue: timeout reached <%d>",ipTimeout);
        ilRc = RC_FAIL;
    }/* end of if */

    return ilRc;
}


static int CheckQueue(int ipModId, ITEM **prpItem)
{
    int     ilRc       = RC_SUCCESS;                      /* Return code */
    int     ilItemSize = 0;
    EVENT *prlEvent    = NULL;
    static  ITEM  *prlItem      = NULL;
    
    ilItemSize = I_SIZE;
    
    ilRc = que(QUE_GETBIGNW,ipModId,ipModId,PRIORITY_3,ilItemSize,(char *) &prlItem);
    if(ilRc == RC_SUCCESS)
    {
        dbg(TRACE,"DebugPrintItem with prlItem follows");
        DebugPrintItem(TRACE,prlItem);
        *prpItem = prlItem;
        dbg(TRACE,"DebugPrintItem with prpItem follows");
        DebugPrintItem(TRACE,*prpItem);
        prlEvent = (EVENT*) ((prlItem)->text);

        switch( prlEvent->command )
        {
            case    HSB_STANDBY :
                dbg(TRACE,"CheckQueue: HSB_STANDBY");
                HandleQueues();
                ctrl_sta = prlEvent->command;
                break;
    
            case    HSB_COMING_UP   :
                dbg(TRACE,"CheckQueue: HSB_COMING_UP");
                ctrl_sta = prlEvent->command;
                break;
    
            case    HSB_ACTIVE  :
                dbg(TRACE,"CheckQueue: HSB_ACTIVE");
                ctrl_sta = prlEvent->command;
                break;
    
            case    HSB_ACT_TO_SBY  :
                dbg(TRACE,"CheckQueue: HSB_ACT_TO_SBY");
                HandleQueues();
                ctrl_sta = prlEvent->command;
                break;
    
            case    HSB_DOWN    :
                dbg(TRACE,"CheckQueue: HSB_DOWN");
                ctrl_sta = prlEvent->command;
                /* Acknowledge the item 
                ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
                if( ilRc != RC_SUCCESS )
                {
                    HandleQueErr(ilRc);
                }  */
                Terminate(30);
                break;
        
        case    HSB_STANDALONE  :
                dbg(TRACE,"CheckQueue: HSB_STANDALONE");
                ctrl_sta = prlEvent->command;
                break;
    
            case    SHUTDOWN    :
                dbg(TRACE,"CheckQueue: SHUTDOWN");
                /* Acknowledge the item */
                ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
                if( ilRc != RC_SUCCESS )
                {
                    /* handle que_ack error */
                    HandleQueErr(ilRc);
                } /* fi */
                Terminate(30);
                break;
    
            case    RESET       :
                ilRc = Reset();
                break;
    
            case    EVENT_DATA  :
                /* DebugPrintItem(DEBUG,prgItem); */
                /* DebugPrintEvent(DEBUG,prlEvent); */
                break;
    
            case    TRACE_ON :
                dbg_handle_debug(prlEvent->command);
                break;
    
            case    TRACE_OFF :
                dbg_handle_debug(prlEvent->command);
                break;


            default         :
                dbg(TRACE,"CheckQueue: unknown event");
                DebugPrintItem(TRACE,prlItem);
                DebugPrintEvent(TRACE,prlEvent);
                break;
    
        } /* end switch */
    
        /* Acknowledge the item */
        ilRc = que(QUE_ACK,0,ipModId,0,0,NULL);
        if( ilRc != RC_SUCCESS )
        {
            /* handle que_ack error */
            HandleQueErr(ilRc);
        } /* fi */
    }else{
        if(ilRc != QUE_E_NOMSG)
        {
            dbg(TRACE,"CheckQueue: que(QUE_GETBIG,%d,%d,...) failed <%d>",ipModId,ipModId,ilRc);
            HandleQueErr(ilRc);
        }/* end of if */
    }/* end of if */
       
    return ilRc;
}/* end of CheckQueue */

/******************************************************************************/
/******************************************************************************/
static int GetRowLength(char *pcpHopo, char *pcpTana, char *pcpFieldList,
                        long *plpLen)
{
    int  ilRc = RC_SUCCESS;         /* Return code */
    int  ilNoOfItems = 0;
    int  ilLoop = 0;
    long llFldLen = 0;
    long llRowLen = 0;
    char clFina[8];
    
    ilNoOfItems = get_no_of_items(pcpFieldList);

    ilLoop = 1;
    do
    {
        ilRc = get_real_item(&clFina[0],pcpFieldList,ilLoop);
        if(ilRc > 0)
        {
            ilRc = GetFieldLength(pcpHopo,pcpTana,&clFina[0],&llFldLen);
            if(ilRc == RC_SUCCESS)
            {
                llRowLen++;
                llRowLen += llFldLen;
            }/* end of if */
        }/* end of if */
        ilLoop++;
    }while((ilRc == RC_SUCCESS) && (ilLoop <= ilNoOfItems));

    if(ilRc == RC_SUCCESS)
    {
        *plpLen = llRowLen;
    }/* end of if */

    return(ilRc);
    
} /* end of GetRowLength */


/******************************************************************************/
/******************************************************************************/
static int GetFieldLength(char *pcpHopo, char *pcpTana, char *pcpFina,
                            long *plpLen)
{
    int  ilRc = RC_SUCCESS;         /* Return code */
    int  ilCnt = 0;
    char clTaFi[32];
    char clFele[16];
    char clFldLst[16];

    ilCnt = 1;
    sprintf(&clTaFi[0],"%s,%s",pcpTana,pcpFina);
    sprintf(&clFldLst[0],"TANA,FINA"); /* wird von syslib zerst�rt */

    /*dbg(TRACE,"GetFieldLength Tana <%s> Fina <%s>",pcpTana,pcpFina);*/

    ilRc = syslibSearchSystabData(&cgTabEnd[0],&clFldLst[0],&clTaFi[0],"FELE",&clFele[0],&ilCnt,"");
    switch (ilRc)
    {
    case RC_SUCCESS :
        *plpLen = atoi(&clFele[0]);
        /* dbg(DEBUG,"GetFieldLength: <%s,%s,%s> <%d>",pcpHopo,pcpTana,pcpFina,*plpLen);*/
        break;

    case RC_NOT_FOUND :
        dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_NOT_FOUND <%s>",ilRc,&clTaFi[0]);
        break;

    case RC_FAIL :
        dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> RC_FAIL",ilRc);
        break;

    default :
        dbg(TRACE,"GetFieldLength: syslibSSD returned <%d> unexpected",ilRc);
        break;
    }/* end of switch */

    return(ilRc);
    
} /* end of GetFieldLength */

static int getOrPutDBData(char *pcpSelection, char *pcpData, unsigned int ipMode)
{
  int ilGetRc = DB_SUCCESS;
  short slFkt = 0;
  short slCursor = 0;
  char errBuff[128];

  slFkt = ipMode;
  slCursor = 0;
  ilGetRc = sql_if (slFkt, &slCursor, pcpSelection, pcpData);
  close_my_cursor (&slCursor);
  if (ilGetRc == DB_SUCCESS) {
    return RC_SUCCESS;
  }
  else {
    if (ilGetRc != NOTFOUND) {
      get_ora_err(ilGetRc, &errBuff[0]);
      dbg (TRACE, "<getOrPutDBData> Error getting Oracle data error <%s>", &errBuff[0]);
      return RC_FAIL;
    }
    else  return ilGetRc;
  }
}

/* All fields are mandatory */
static int PackHeaderXml( char *pcpXmlStr, INFOBJ_GENERIC rpGeneric, int ipPackType, int *piSequenceNo )
{
    int ilRc;
    char pclFNR2[16] = "\0";
    char pclFNR3[16] = "\0";
    char pclALC2[16] = "\0";
    char pclALC3[16] = "\0";
    char pclFLTN[16] = "\0";
    char pclFLTS[16] = "\0";
    char pclTmpStr[50] = "\0"; 
    char pclActionStr[5][2] = { "","I","I","U","D" };
    char *pclFunc = "PackHeaderXml";

    if( ipPackType < 0 || ipPackType >= 5 )
        return;

    if( igSequenceNo >= MAX_SEQUENCE_NO )
        *piSequenceNo = 1;
    else
        *piSequenceNo = igSequenceNo + 1;

    GetServerTimeStamp("UTC",1,0,pclTmpStr);

    ilRc = BuildFlightNbr (rpGeneric.FLNO, pclFNR3, pclFNR2, pclALC3, pclALC2, pclFLTN, pclFLTS );
    if( ilRc == RC_FAIL )
    {
        dbg( TRACE, "<%s> Error in building flight number", pclFunc );
        return FALSE;
    }
    dbg( DEBUG, "<%s> FNR2 <%s>", pclFunc, pclFNR2 );
    dbg( DEBUG, "<%s> FNR3 <%s>", pclFunc, pclFNR3 );
    dbg( DEBUG, "<%s> ALC2 <%s>", pclFunc, pclALC2 );
    dbg( DEBUG, "<%s> ALC3 <%s>", pclFunc, pclALC3 );
    dbg( DEBUG, "<%s> FLTN <%s>", pclFunc, pclFLTN );
    dbg( DEBUG, "<%s> FLTS <%s>", pclFunc, pclFLTS );

    if( ilRc == 2 )
        strcpy( rpGeneric.FLNO, pclFNR2 );
    else if( ilRc == 3 )
        strcpy( rpGeneric.FLNO, pclFNR3 );
    else
    {
        dbg( TRACE, "<%s> Invalid RC from BuildFlightNbr <%d>", pclFunc, ilRc );
        return FALSE;
    }

    if( strlen(rpGeneric.STDT) <= 0 || strlen(rpGeneric.FLNO) <= 0 )
    {
        dbg( TRACE, "<%s> Invalid STDT/FLNO", pclFunc );
        return FALSE;
    }

    sprintf( pcpXmlStr, "<INFOBJ_GENERIC>\n"
                        "<TIMESTAMP>%14.14s+%2.2d</TIMESTAMP>\n"
                        "<ACTIONTYPE>%c</ACTIONTYPE>\n"
                        "<ADID>%c</ADID>\n"
                        "<STDT>%8.8s</STDT>\n"
                        "<FLNO>%-9.9s</FLNO>\n"
                        "<SEQUENCE>%d</SEQUENCE>\n"
                        "</INFOBJ_GENERIC>\n", pclTmpStr, igHourDifference, 
                                            pclActionStr[ipPackType][0],
                                            rpGeneric.ADID[0], rpGeneric.STDT, 
                                            rpGeneric.FLNO, *piSequenceNo );
     dbg( TRACE, "<%s> Xml (%s)", pclFunc, pcpXmlStr );
     return TRUE;
}
   
static int GetTimeZone()
{
    struct tm *rlTmptr;
    time_t ilNowTime; 
    time_t ilUtcTime; 
    time_t ilLocTime;

    ilNowTime = time(0);
    ilUtcTime = ilNowTime;
    ilLocTime = ilNowTime;

    rlTmptr = (struct tm *)gmtime((time_t *)&ilUtcTime);
    ilUtcTime = mktime( (struct tm *)rlTmptr );

    rlTmptr = (struct tm *)localtime((time_t *)&ilLocTime);
    ilLocTime = mktime( (struct tm *)rlTmptr );

    return( (ilLocTime-ilUtcTime)/3600 );
}

/* rpGeneric.STDT is most matching to field FLDA in AFTTAB */
static int GetFlightUrno( INFOBJ_GENERIC rpGeneric, char *pcpUrno )
{
    int ilRc = RC_SUCCESS;
    int ilsi = FLNOLEN - 1;
    char pclSqlBuf[DBQRLEN] = "\0";
    char pclData[DBQRLEN] = "\0";
    char *pclFunc = "GetFlightUrno";

    /* Special handling procedures for RETURN FLIGHT(R)/TAXIWAY(A) */
    /* Since 2 records exist in the DB with ADID = D and ADID = B  */
    if( (rpGeneric.FLNO[ilsi] == 'R' || rpGeneric.FLNO[ilsi] == 'A') && rpGeneric.ADID[0] == 'D' )
        sprintf( pclSqlBuf, "SELECT TRIM(URNO) FROM AFTTAB WHERE FLNO = '%s' AND ADID = '%c' "
                            "AND FLDA = '%s'", rpGeneric.FLNO, rpGeneric.ADID[0], rpGeneric.STDT );
    else
        sprintf( pclSqlBuf, "SELECT TRIM(URNO) FROM AFTTAB WHERE FLNO = '%s' AND (ADID = '%c' OR ADID = 'B') "
                            "AND FLDA = '%s'", rpGeneric.FLNO, rpGeneric.ADID[0], rpGeneric.STDT );

    dbg( TRACE, "%s: URNO SqlBuf = <%s>", pclFunc, pclSqlBuf );

    ilRc = getOrPutDBData(pclSqlBuf,pclData,START);
    if( ilRc != DB_SUCCESS )
        return FALSE;
    strcpy( pcpUrno, pclData );
    if( strlen(pcpUrno) > URNOLEN )
        pcpUrno[URNOLEN] = '\0';

    return TRUE;
}

static int GetCounterRec( INFOBJ_COUNTER *prpCounter )
{
    int ilRc = RC_SUCCESS;
    char pclSqlBuf[DBQRLEN] = "\0";
    char pclData[DBQRLEN] = "\0";
    char pclUtcTime[DTTMLEN+1] = "\0";
    char *pclFunc = "GetCounterRec";

    /** Mei, commented on 27-Apr-09 **/
    /*****
     sprintf( pclSqlBuf, "SELECT TRIM(URNO),TRIM(ACT3),TRIM(CDAT),TRIM(CGRU),TRIM(COIX),TRIM(COPN),TRIM(CTYP),"
                        "TRIM(DISP),TRIM(GHPU),TRIM(GHSU),TRIM(GPMU),TRIM(STAT),TRIM(STOD),TRIM(CKBS),TRIM(CKES) "
                        "FROM CCATAB WHERE FLNO = '%s' AND FLNU = '%s' AND CKIC = '%s' "
                        "AND CKIT = '%s' AND STOD = '%s' ", prpCounter->FLNO, prpCounter->FLNU, prpCounter->CKIC, 
                                                            prpCounter->CKIT, prpCounter->STOD );
    ******/

    sprintf( pclSqlBuf, "SELECT TRIM(URNO),TRIM(ACT3),TRIM(CDAT),TRIM(CGRU),TRIM(COIX),TRIM(COPN),TRIM(CTYP),"
                        "TRIM(DISP),TRIM(GHPU),TRIM(GHSU),TRIM(GPMU),TRIM(STAT),TRIM(STOD),TRIM(CKBS),TRIM(CKES) "
                        "FROM CCATAB WHERE FLNO = '%s' AND FLNU = '%s' AND CKIC = '%s' "
                        "AND CKIT = '%s'", prpCounter->FLNO, prpCounter->FLNU, prpCounter->CKIC, prpCounter->CKIT );
    dbg( TRACE, "%s: URNO SqlBuf = <%s>", pclFunc, pclSqlBuf );

    ilRc = getOrPutDBData(pclSqlBuf,pclData,START);
    if( ilRc != DB_SUCCESS )
        return FALSE;
    get_fld(pclData,FIELD_1,STR,12,prpCounter->URNO);
    get_fld(pclData,FIELD_2,STR, 8,prpCounter->ACT3);
    get_fld(pclData,FIELD_3,STR,16,prpCounter->CDAT);
    get_fld(pclData,FIELD_4,STR,260,prpCounter->CGRU);
    get_fld(pclData,FIELD_5,STR,12,prpCounter->COIX);
    get_fld(pclData,FIELD_6,STR, 4,prpCounter->COPN);
    get_fld(pclData,FIELD_7,STR,CTYPLEN,prpCounter->CTYP);
    get_fld(pclData,FIELD_8,STR,DISPLEN,prpCounter->DISP);
    get_fld(pclData,FIELD_9,STR,12,prpCounter->GHPU);
    get_fld(pclData,FIELD_10,STR,12,prpCounter->GHSU);
    get_fld(pclData,FIELD_11,STR,12,prpCounter->GPMU);
    get_fld(pclData,FIELD_12,STR,12,prpCounter->STAT);
    get_fld(pclData,FIELD_13,STR,16,prpCounter->CKBS);
    get_fld(pclData,FIELD_14,STR,16,prpCounter->CKES);

    dbg( DEBUG, "%s: URNO <%s>", pclFunc, prpCounter->URNO );
    dbg( DEBUG, "%s: ACT3 <%s>", pclFunc, prpCounter->ACT3 );
    dbg( DEBUG, "%s: CDAT <%s>", pclFunc, prpCounter->CDAT );
    dbg( DEBUG, "%s: CGRU <%s>", pclFunc, prpCounter->CGRU );
    dbg( DEBUG, "%s: COIX <%s>", pclFunc, prpCounter->COIX );
    dbg( DEBUG, "%s: COPN <%s>", pclFunc, prpCounter->COPN );
    dbg( DEBUG, "%s: CTYP <%s>", pclFunc, prpCounter->CTYP );
    dbg( DEBUG, "%s: DISP <%s>", pclFunc, prpCounter->DISP );
    dbg( DEBUG, "%s: GHPU <%s>", pclFunc, prpCounter->GHPU );
    dbg( DEBUG, "%s: GHSU <%s>", pclFunc, prpCounter->GHSU );
    dbg( DEBUG, "%s: GPMU <%s>", pclFunc, prpCounter->GPMU );
    dbg( DEBUG, "%s: STAT <%s>", pclFunc, prpCounter->STAT );
    dbg( DEBUG, "%s: CKBS <%s>", pclFunc, prpCounter->CKBS );
    dbg( DEBUG, "%s: CKES <%s>", pclFunc, prpCounter->CKES );
    return TRUE;
}

static int GetCodeshareInfo( char *pcpVERS, char *pcpFTYP, char *pcpUrno )
{
    int ilRc = RC_SUCCESS;
    char pclSqlBuf[DBQRLEN] = "\0";
    char pclData[DBQRLEN] = "\0";
    char *pclFunc = "GetCodeshareInfo";


    sprintf( pclSqlBuf, "SELECT TRIM(VERS), TRIM(FTYP) FROM AFTTAB WHERE URNO = %s ", pcpUrno   );

    dbg( TRACE, "%s: SqlBuf = <%s>", pclFunc, pclSqlBuf );

    ilRc = getOrPutDBData(pclSqlBuf,pclData,START);
    if( ilRc != DB_SUCCESS )
        return FALSE;
    get_fld(pclData,FIELD_1,STR,20,pcpVERS);
    get_fld(pclData,FIELD_2,STR, 1,pcpFTYP);

    dbg( TRACE, "%s: VERS <%s> FTYP <%s>", pclFunc, pcpVERS, pcpFTYP );
    return TRUE;
}

/***********************************************************/
/* This function extract the string embedded by the XML tag*/
/* It will extract the X tag required                      */
/* Eg. xml_untag( result, str, "AAA", 1) returns "Tag1"    */
/* Eg. xml_untag( result, str, "AAA", 2) returns "Tag2"    */
/* str = <AAA>Tag1</AAA><AAA>Tag2</AAA>                    */
/***********************************************************/
static int xml_untag( char *pcpResult, char *pcpXml, char *pcpKey, int ipUntagCnt )
{
    int ilLen = 0;
    int ilUntagCnt = 0;
    char *pclFunc = "xml_untag";
    char *pclStr = NULL;
    char *pclStr1 = NULL;
    char *pclKey = NULL;
    char *pclXml = NULL;

    if( ipUntagCnt <= 0 )
    {
        dbg( TRACE, "<%s> Invalid Tag Cnt = <%d>", pclFunc, ipUntagCnt );
        return FALSE;
    }
    dbg( DEBUG, "<%s> Tag Cnt = <%d>", pclFunc, ipUntagCnt );

    ilLen = strlen( pcpXml );
    dbg( DEBUG, "<%s> XML Len = <%d>", pclFunc, ilLen );

    if( strlen( pcpKey ) >= ilLen )
    {
        dbg( TRACE, "<%s> Invalid key = (%s)", pclFunc, pcpKey );
        return FALSE;
    }
    dbg( DEBUG, "<%s> Key Len = <%d>", pclFunc, strlen(pcpKey) );

    pclKey = (char *)malloc( ilLen + 10 ); /* Reserve some buffer */
    if( pclKey == NULL )
    {
        dbg( TRACE, "%s: <%d> ERROR!!! in getting memory!!!", pclFunc, __LINE__ );
        exit( 1 );
    }
    pclXml = (char *)malloc( ilLen );
    if( pclXml == NULL )
    {
        dbg( TRACE, "%s: <%d> ERROR!!! in getting memory!!!", pclFunc, __LINE__ );
        exit( 1 );
    }
    memcpy( pclXml, pcpXml, ilLen ); /* To preserve the org str */

    while( ilUntagCnt < ipUntagCnt )
    {
        ilUntagCnt++;

        sprintf( pclKey, "<%s>", pcpKey );
        pclStr = (char *)strstr( pclXml, pclKey );
        if( pclStr == NULL )
        {
            free(pclKey);
            free(pclXml);
            return FALSE;
        }
        pclStr += strlen( pclKey );
        sprintf( pclKey, "</%s>", pcpKey );
        pclStr1 = (char *)strstr( pclStr, pclKey );
        if( pclStr1 == NULL )
        {
            free(pclKey);
            free(pclXml);
            return FALSE;
        }
        if( ilUntagCnt >= ipUntagCnt )
            break;
        strcpy( pclXml, pclStr1 );
    }

    ilLen = 0;
    ilLen = strlen( pclStr ) - strlen( pclStr1 );
    if( ilLen <= 0 )
    {
        free(pclKey);
        free(pclXml);
        return FALSE;
    }
    memcpy( pcpResult, pclStr, ilLen );
    dbg( DEBUG, "<%s> Result Str <%s>", pclFunc, pcpResult );
    pcpResult[ilLen] = '\0';
    free(pclKey);
    free(pclXml);
    return TRUE;
}

static void ReadXMLFile(char *pcpFields, char *pcpData, char *pcpCmd)
{
    FILE *prlFptr;
    char pclFileName[1000] = "\0";
    char pclLine[1000] = "\0";
    char pclMessage[10000] = "\0";
    char *pclFunc = "ReadXMLFile";

    strcpy( pclFileName, pcpData );
    dbg( TRACE, "<%s> Reading file <%s>", pclFunc, pclFileName );

    prlFptr = fopen( pclFileName, "r" );
    if( prlFptr == NULL )
    {
        dbg( TRACE, "<%s> Unable to open file error %d<%s>", pclFunc, errno, strerror(errno) );
        sprintf( pclFileName, "%s/TIG_TO_UFIS.xml", pcgXmlFileDirectory );
        if( (prlFptr = fopen( pclFileName, "r" )) == NULL )
        {
            dbg( TRACE, "<%s> Unable to open default file <%s> error %d<%s>", pclFunc, pclFileName, errno, strerror(errno) );
            return;
        }
    }

    while( !feof(prlFptr) )
    {
        if( fgets( pclLine, sizeof(pclLine), prlFptr ) == NULL )
            break;
        strcat( pclMessage, pclLine );
    }
    pclMessage[strlen(pclMessage)-1] = '\0';
    dbg( DEBUG, "<%s> Line read (%s)", pclFunc, pclMessage );
    HandleMQData( pcpFields, pclMessage, "XMLI" );
    fclose(prlFptr);
}

static void InformTIG( char *pcpXmlStr, int plSequenceNo )
{
    FILE *rlFptr;
    struct tm *rlTmptr;
    time_t ilNow;
    static int ilTimeCounter = 0;
    int ilLen = 0;
    int ilRc;
    char pclFname[200] = "\0";
    char pclSqlBuf[DBQRLEN] = "\0";
    char pclSqlData[DBQRLEN] = "\0";
    char pclTmpStr[100] = "\0";
    char pclMkeyStr[100] = "\0";
    char pclSeqnStr[100] = "\0";
    static char pclPrevTimeStr[50] = "\0";
    char *pclFunc = "InformTIG";

/*  sprintf( pclFname, "%s/UFIS_TO_TIG.%6.6d", pcgXmlFileDirectory, igSequenceNo );   
    if( (rlFptr = fopen(pclFname, "w")) == NULL )
    {
        dbg( TRACE, "<%s> Unable to open file <%s> Error %d<%s>", pclFunc, pclFname, errno, strerror(errno) );
        return;
    }
    ilLen = strlen(pclXmlStr);
    if( fwrite( pclXmlStr, ilLen, 1, rlFptr ) != 1 )
    {
        fclose( rlFptr );
        dbg( TRACE, "<%s> Unable to write file <%s> Error %d<%s>", pclFunc, pclFname, errno, strerror(errno) );
        return;
    }
    fclose( rlFptr );
    dbg( TRACE, "<%s> Xml <%s> stored in file <%s> successfully ", pclFunc, pclXmlStr, pclFname );
*/
/*  sprintf( pclWmqCon, "/ceda/bin/wmqcon file %s", pclFname );
    dbg( TRACE, "<%s> calling <%s>", pclFunc, pclWmqCon );
    system( pclWmqCon );
*/
    ilNow = time(0);
    rlTmptr = (struct tm *)localtime( (time_t *)&ilNow );
    sprintf( pclTmpStr, "%4.4d%2.2d%2.2d%2.2d%2.2d%2.2d", rlTmptr->tm_year+1900, rlTmptr->tm_mon+1,
                                   rlTmptr->tm_mday, rlTmptr->tm_hour, rlTmptr->tm_min, rlTmptr->tm_sec );
    if( !strncmp( pclTmpStr, pclPrevTimeStr, 14 ) )
        ilTimeCounter++;
    else
        ilTimeCounter = 0;
    if( ilTimeCounter > 999 )
        ilTimeCounter = 0;
    strncpy( pclPrevTimeStr, pclTmpStr, 14 );
    dbg( TRACE, "<%s> ilTimeCounter = %d", pclFunc, ilTimeCounter );

    sprintf( pclMkeyStr, "%s%3.3d", pclTmpStr, ilTimeCounter );
    sprintf( pclSeqnStr, "%d", plSequenceNo );

    sprintf( pclSqlBuf, "INSERT INTO TGSTAB VALUES ( '%s', '%d', '%s', 'SEND_PEND', ' ' )", pclMkeyStr, plSequenceNo, pcpXmlStr );
    dbg( TRACE, "<%s> SqlBuf <%s>", pclFunc, pclSqlBuf );

    ilRc = getOrPutDBData(pclSqlBuf,pclSqlData,START);
    if( ilRc != DB_SUCCESS )
    {
        dbg( TRACE, "<%s> ERROR!!!!!  Unable to store data into TRSTAB!!!!!", pclFunc );
        return;
    }
    
    ilRc = SendCedaEvent(igIpcconsModid, 0, "tighdl", "EXCO", pclMkeyStr, pclSeqnStr, "XMLO", "", "",
                         "", pcpXmlStr, "", igSendPriority, NETOUT_NO_ACK);
    dbg( TRACE, "<%s> SEND IPCCONS! ilRc = %d", pclFunc, ilRc );

    /* In case some hiccup and record not send. Thus, only upd global sequence num now */
    igSequenceNo = plSequenceNo;
    return;
}

static int ConvertSpecialChar( char *pcpInStr, char *pcpOutStr )
{
    BOOL blConverted = FALSE;
    int ilLen;
    int ili, ilj;
    unsigned char clChar;
    char *pclFunc = "ConvertSpecialChar";

    dbg( TRACE, "<%s> ------------- START ---------------", pclFunc );
    dbg( TRACE, "<%s> pcpInStr (%s)", pclFunc, pcpInStr );
    ilLen = strlen(pcpInStr);
    pcpOutStr[0] = '\0';
    ilj = 0;
    for( ili = 0; ili < ilLen; ili++ )
    {
        clChar = pcpInStr[ili];
        switch( clChar )
        {
            case( 0x26 ):
                strcat(pcpOutStr,"&amp");
                ilj += 4;
                blConverted = TRUE;
                break;
            case( 0x60 ):
                strcat(pcpOutStr,"&apos");
                ilj += 5;
                blConverted = TRUE;
                break;
            case( 0x22 ):
                strcat(pcpOutStr,"&quot");
                ilj += 4;
                blConverted = TRUE;
                break;
            case( 0x3C ):
                strcat(pcpOutStr,"&lt");
                ilj += 3;
                blConverted = TRUE;
                break;
            case( 0x3E ):
                strcat(pcpOutStr,"&gt");
                ilj += 3;
                blConverted = TRUE;
                break;
            default:
                pcpOutStr[ilj++] = pcpInStr[ili];
                continue;
        }
    }
    pcpOutStr[ilj] = '\0';
    dbg( TRACE, "<%s> pcpOutStr (%s)", pclFunc, pcpOutStr );
    dbg( TRACE, "<%s> ------------- END ---------------", pclFunc );
    return blConverted;
}

/* this function assumes that anything anchor with < and > has to be converted to lower case */
static int ConvertTag( char *pcpStr )
{
    BOOL blConverted = FALSE;
    BOOL blStartConvert = FALSE;
    int ili, ilj = 0;
    int ilLen;
    char *pclFunc = "ConvertTag";

    ilLen = strlen(pcpStr);
    for( ili = 0; ili < ilLen; ili++ )
    {
        if( pcpStr[ili] == '<' )
        {
            blStartConvert = TRUE; 
            continue;
        }
        if( pcpStr[ili] == '>' )
        {
            blStartConvert = FALSE; 
            continue;
        }
        if( blStartConvert == TRUE )
            pcpStr[ili] = toupper(pcpStr[ili]);
    }
    dbg( TRACE, "<%s> Result Str (%s)", pclFunc, pcpStr );
}

static int RemoveNewLine( char *pcpStr )
{
    int ili;
    int ilLen;

    ilLen = strlen(pcpStr);
    for( ili = 0; ili < ilLen; ili++ )
    {
        if( pcpStr[ili] == '\n' )
            pcpStr[ili] = ' ';
    }
}

/* This function will break pclFlightNbr into 2 and 3 airline codes flight number */
/* Assuming pclFlightNbr comes in 2 or 3 airline code */
static int BuildFlightNbr (char *pcpFlightNbr, char *pcpfnr3, char *pcpfnr2, char *pcpalc3, char *pcpalc2, char *pcpflnr, char *pcpflsf )
{
    int ilProcessType;
    int ili, ilj, ilk;
    int ilLen;
    int ilRc;
    char pclData[DBQRLEN];
    char *pclFunc = "BuildFlightNbr";

    dbg( DEBUG, "<%s> Flight Number received <%s>", pclFunc, pcpFlightNbr );

    ilLen = strlen(pcpFlightNbr);
    ili = ilProcessType = 2;
    /* flight is in 3 airine code */
    if( pcpFlightNbr[2] >= 'A' && pcpFlightNbr[2] <= 'Z' )
        ili = ilProcessType = 3;

    memset( pcpalc2, 0, strlen(pcpalc2) );
    memset( pcpalc3, 0, strlen(pcpalc3) );
    if( ilProcessType == 3 )
        sprintf( pcpalc3, "%3.3s", pcpFlightNbr );
    else    
        sprintf( pcpalc2, "%2.2s ", pcpFlightNbr );

    ilk = ilj = 0;
    /* Strip off leading zeroes and blanks */
    for( ili; ili < ilLen; ili++ )
    {
        if( pcpFlightNbr[ili] == ' ' || pcpFlightNbr[ili] == '0'  )
            continue;
        break;
    }
    for( ili; ili < ilLen; ili++ )
    {
        if( pcpFlightNbr[ili] >= '0' && pcpFlightNbr[ili] <= '9' )
        {
            pcpflnr[ilj++] = pcpFlightNbr[ili];
            continue;
        }
        /* Remove trailing blank */
        if( pcpFlightNbr[ili] == ' '  )
            continue;
        if( pcpFlightNbr[ili] >= 'A' && pcpFlightNbr[ili] <= 'Z' )
            pcpflsf[ilk++] = pcpFlightNbr[ili];
        break;
    }
    pcpflnr[ilj] = '\0';
    if( ilk == 0 ) /* No suffix */
    {
        pcpflsf[0] = ' ';
        ilk++;
    }
    pcpflsf[ilk]   = '\0';
    if( strlen(pcpflnr) <= 0 )
        return RC_FAIL;
    while( strlen(pcpflnr) > 0 && strlen(pcpflnr) <= 2 )
    {
        sprintf( pclData, "0%s", pcpflnr );
        strcpy( pcpflnr, pclData );
    }

    dbg( TRACE, "<%s> Process Type <%d>", pclFunc, ilProcessType );
    dbg( TRACE, "<%s> 1. ALC2 <%s>", pclFunc, pcpalc2 );
    dbg( TRACE, "<%s> 1. ALC3 <%s>", pclFunc, pcpalc3 );
    dbg( TRACE, "<%s>    FLNR <%s>", pclFunc, pcpflnr );
    dbg( TRACE, "<%s>    FLSF <%s>", pclFunc, pcpflsf );

    if( GetALC(pcpalc2, pcpalc3) == FALSE )
        dbg( TRACE, "<%s> No ALC found", pclFunc );

    sprintf( pcpfnr2, "%2.2s %-5.5s%c", pcpalc2, pcpflnr, pcpflsf[0] );
    sprintf( pcpfnr3, "%3.3s%-5.5s%c", pcpalc3, pcpflnr, pcpflsf[0] );

    dbg( TRACE, "<%s> 2. FNR2 <%s>", pclFunc, pcpfnr2 );
    dbg( TRACE, "<%s> 2. FNR3 <%s>", pclFunc, pcpfnr3 );
    dbg( TRACE, "<%s> 2. ALC2 <%s>", pclFunc, pcpalc2 );
    dbg( TRACE, "<%s> 2. ALC3 <%s>", pclFunc, pcpalc3 );
    return ilProcessType;
}

static void GetALCData()
{
    int ilRc;
    int ili = 0;
    short slSqlFunc = START;
    short slSqlCursor = 0;
    char pclSqlBuf[DBQRLEN];
    char pclSqlData[DBQRLEN];
    char *pclFunc = "GetALCData";
    
    sprintf( pclSqlBuf, "%s", "SELECT COUNT(*) FROM ALTTAB" );
    dbg( TRACE, "<%s> SqlBuf <%s>", pclFunc, pclSqlBuf );

    ilRc = getOrPutDBData(pclSqlBuf,pclSqlData,START);
    if( ilRc != DB_SUCCESS )
    {
        dbg( TRACE, "<%s> ERROR!!!!!  Unable to get data from ALTTAB!!!!!", pclFunc );
        dbg( TRACE, "<%s> ERROR!!!!!  ALC2 and ALC3 in AFTTAB will be blank when insert flight", pclFunc );
        return;
    }
    igNum_Airline_Codes = atoi(pclSqlData);
    dbg( TRACE, "<%s> No. of Airline Codes = %d", pclFunc, igNum_Airline_Codes );

    if( igNum_Airline_Codes <= 0 )
        return;

    if( prgALC != NULL )
    {
        dbg( TRACE, "<%s> Free prgALC", pclFunc );
        free( (TIG_AIRLINE_CODES *)prgALC );
    }

    prgALC = (TIG_AIRLINE_CODES *)malloc( (igNum_Airline_Codes+5) * sizeof(TIG_AIRLINE_CODES) );

    sprintf( pclSqlBuf, "%s", "SELECT TRIM(ALC2), TRIM(ALC3) FROM ALTTAB ORDER BY ALC2" );
    dbg( TRACE, "<%s> SqlBuf <%s>", pclFunc, pclSqlBuf );

    slSqlCursor = 0;
    slSqlFunc   = START;
    while((ilRc = sql_if(slSqlFunc,&slSqlCursor,pclSqlBuf,pclSqlData)) == DB_SUCCESS)
    {
        slSqlFunc = NEXT;
        get_fld(pclSqlData,FIELD_1,STR,2,prgALC[ili].ALC2);
        get_fld(pclSqlData,FIELD_2,STR,3,prgALC[ili].ALC3);
        dbg( DEBUG, "<%s> Rec %d ALC2 <%s> ALC3 <%s>", pclFunc, ili, prgALC[ili].ALC2, prgALC[ili].ALC3 );
        ili++;
    }
    dbg( TRACE, "<%s> Loaded <%d> airline codes", pclFunc, ili );
    close_my_cursor(&slSqlCursor);
}

static int GetALC( char *pcpALC2, char *pcpALC3 )
{
    int ilWhichALC = -1;
    int ili;
    char *pclFunc = "GetALC";

    if( strlen(pcpALC2) > 0 )
        ilWhichALC = 2;
    else if( strlen(pcpALC3) > 0 )
        ilWhichALC = 3;
    else
        return FALSE;

    for( ili = 0; ili < igNum_Airline_Codes; ili++ )
    {
        if( ilWhichALC == 2 )       
        {
            if( strncmp( prgALC[ili].ALC2, pcpALC2, 2 ) )
                continue;
            if( prgALC[ili].ALC2[0] > pcpALC2[0] )
                break;
            strcpy( pcpALC3, prgALC[ili].ALC3 );
            dbg( TRACE, "<%s> Found ALC3 <%s>", pclFunc, pcpALC3 );
            return TRUE;
        }
        else
        {
            if( strncmp( prgALC[ili].ALC3, pcpALC3, 3 ) )
                continue;
            strcpy( pcpALC2, prgALC[ili].ALC2 );
            dbg( TRACE, "<%s> Found ALC2 <%s>", pclFunc, pcpALC2 );
            return TRUE;
        }
    }
    return FALSE;
}


static int FormatJFNO( char cpAction, char *pcpJFNO, char *pcpJCNT, char *pcpCOSH )
{
    int ili;
    int ilRc;
    int ilLen;
    int ilCount;
    char pclTmpStr[30];
    char pclFNR2[16] = "\0";
    char pclFNR3[16] = "\0";
    char pclALC2[16] = "\0";
    char pclALC3[16] = "\0";
    char pclFLTN[16] = "\0";
    char pclFLTS[16] = "\0";
    char *pclFunc = "FormatJFNO";

    ilLen = strlen(pcpJFNO);
    ilCount = atoi( pcpJCNT);
    dbg( TRACE, "<%s> Len of JFNO <%d> JCNT <%d>", pclFunc, ilLen, ilCount );

    if( ilLen == 0 && ilCount == 0 )
        return TRUE;

    if( (ilLen % ONE_JFNO_SIZE) != 0 )
    {
        dbg( TRACE, "<%s> Invalid length <%d> of JFNO", pclFunc, ilLen );
        return FALSE;
    }

    if( (ilLen/ONE_JFNO_SIZE) != ilCount )
    {
        dbg( TRACE, "<%s> Invalid JCNT <%d> does not match with length of JFNO", pclFunc, ilCount );
        ilCount = ilLen / ONE_JFNO_SIZE;
        dbg( TRACE, "<%s> Change count to <%d>", pclFunc, ilCount );
    }

    if( ilCount < 0 || ilCount > 9 )
    {
        dbg( TRACE, "<%s> Invalid JCNT = <%d> valid is 0-9", pclFunc, ilCount );
        return FALSE;
    }


    /* Not sure what to do if update is to send deleted JFNO */
    if( cpAction == 'U' )
        cpAction = 'I';

    /* Format for COSH is I;FLNO;|D;;| */
    /* but not useful in this sense */
    for( ili = 0; ili < ilLen; ili+=ONE_JFNO_SIZE )
    {
        sprintf( pclTmpStr, "%9.9s", &pcpJFNO[ili] );
        ilRc = BuildFlightNbr (pclTmpStr, pclFNR3, pclFNR2, pclALC3, pclALC2, pclFLTN, pclFLTS);
        if( ilRc == 2 )
            strcpy( pclTmpStr, pclFNR2 );
        else if( ilRc == 3 )
            strcpy( pclTmpStr, pclFNR3 );

        strcpy( pcpJFNO, pcpCOSH ); /* In case string is altered */
        if( strstr(pcpJFNO, pclTmpStr) ) /* Flight re-occurence */
        {
            ilCount--;
            continue;
        }
        strcat( pcpCOSH, pclTmpStr );
/*      sprintf( pclTmpStr, "%c;", cpAction );
        strcat( pcpCOSH, pclTmpStr );

        sprintf( pclTmpStr, "%9.9s", &pcpJFNO[ili] );
        ilRc = BuildFlightNbr (pclTmpStr, pclFNR3, pclFNR2, pclALC3, pclALC2, pclFLTN, pclFLTS);
        if( ilRc == 2 )
            strcpy( pclTmpStr, pclFNR2 );
        else if( ilRc == 3 )
            strcpy( pclTmpStr, pclFNR3 );

        strcat( pcpCOSH, pclTmpStr );
        if( (ili+ONE_JFNO_SIZE) < ilLen )
            strcat( pcpCOSH, ";|" );
*/
    }
    strcpy( pcpJFNO, pcpCOSH );
    sprintf( pcpJCNT, "%d", ilCount );
    dbg( TRACE, "<%s> pcpJFNO <%s> pcpJCNT <%s> pcpCOSH <%s>", pclFunc, pcpJFNO, pcpJCNT, pcpCOSH );
    return TRUE;
}

static int FormatVIAL( char *pcpVIAL, char *pcpVIAN, char *pcpROUT )
{
    int ili;
    int ilLen;
    int ilCount;
    int ilDivValue;
    char pclTmpStr[1000];
    char pclVIAL[1500] = "\0";
    char *pclFunc = "FormatVIAL";
    char pclFields[50] = "[FIDS;APC3;APC4;STOA;ETOA;LAND;ONBL]";

    ilLen = strlen(pcpVIAL);
    ilCount = atoi(pcpVIAN);
    dbg( TRACE, "<%s> Len of VIAL <%d> VIAN <%d>", pclFunc, ilLen, ilCount );

    if( ilLen == 0 && ilCount == 0 )
        return TRUE;

    if( ilCount < 0 || ilCount > 8 )
    {
        dbg( TRACE, "<%s> Invalid VIAN = <%d> valid is 0-8", pclFunc, ilCount );
        return FALSE;
    }
    if( (ilLen/ONE_VIAL_SIZE) != ilCount )
    {
        dbg( TRACE, "<%s> Invalid VIAN does not correspond to len of VIAL", pclFunc );
        return FALSE;
    }

    if( (ilLen % ONE_VIAL_SIZE ) != 0 )
        return FALSE;

    strcpy( pcpROUT, pclFields );
    for( ili = 0; ili < ilLen; ili+=ONE_VIAL_SIZE )
    {
        sprintf( pclTmpStr, "%c;%3.3s;%4.4s;%14.14s;%14.14s;%14.14s;%14.14s",
                            pcpVIAL[ili],    &pcpVIAL[ili+1], &pcpVIAL[ili+4],
                            &pcpVIAL[ili+8], &pcpVIAL[ili+25], &pcpVIAL[ili+42],
                            &pcpVIAL[ili+59] );
        strcat( pcpROUT, pclTmpStr );
        sprintf( pclTmpStr, "%c%3.3s%4.4s%14.14s%14.14s%14.14s%14.14s%56.56s",
                            pcpVIAL[ili],    &pcpVIAL[ili+1], &pcpVIAL[ili+4],
                            &pcpVIAL[ili+8], &pcpVIAL[ili+25], &pcpVIAL[ili+42],
                            &pcpVIAL[ili+59], CHAR_BLANK );
        strcat( pclVIAL, pclTmpStr );
        if( (ili+ONE_VIAL_SIZE) < ilLen ) /* some more record */
            strcat( pcpROUT, "|" );
    }
    strcpy( pcpVIAL, pclVIAL );
    dbg( TRACE, "<%s> pcpVIAL <%s> pcpROUT <%s>", pclFunc, pcpVIAL, pcpROUT );
    return TRUE;
}
