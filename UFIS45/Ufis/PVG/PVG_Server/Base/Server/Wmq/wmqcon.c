#ifndef _DEF_mks_version
#define _DEF_mks_version
#include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/PVG/PVG_Server/Base/Server/Wmq/wmqcon.c 1.2 2008/10/02 20:41:00SGT fei Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :                                                           */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history : HEB: added Alerter-Funktionality                          */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */

static char sccs_version[] ="@(#) UFIS45 (c) ABB AAT/I wmqcon.c 45.3 / 04.06.2002 HEB";


/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is NOT a MIKE main program */


/* #define U_MAIN */
/* #define UGCCS_PRG */
/* #define STH_USE */
#include <stdio.h>
#include <sys/stat.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
/* #include "quedef.h" */
/* #include "uevent.h" */
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "fditools.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "rmschkrul.h"
/* #include "syslib.h" */
#include "db_if.h"
#include <time.h>
#include <cedatime.h>
#include "tcpudp.h"

/* includes for WebsphereMQ  */
#include <cmqc.h>

#ifdef _HPUX_SOURCE
extern int daylight;
extern long timezone;
extern char *tzname[2];
#else
extern int _daylight;
extern long _timezone;
extern char *_tzname[2];
#endif

#define TCP_SHUTDOWN    0x14046701
#define TCP_DATA        0x14046702
#define TCP_KEEPALIVE   0x14046703
#define TCP_ACKNOWLEDGE 0x14046704

typedef struct {
        long command;
        long length;
}TCP_INFOHEADER;
#define TCP_INFOHEADER_SIZE sizeof(TCP_INFOHEADER)

static TCP_INFOHEADER InfoHeader;

static int igAcceptSocket = 0;


int debug_level = 0;

/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/

#define MAX_SEND_QUEUES = 20;

static char cgSendCmd[16] = "MSGO";
static char  cgConfigFile[512] = "\0";
static char  cgDefSendFilePath[512] = "\0";
static char  cgDefSaveFilePath[512] = "\0";
static char  cgActSendFile[512] = "\0";
static char  cgActSaveFile[512] = "\0";
static char  cgHopo[8] = "???\0";                         /* default home airport    */
static char  cgTabEnd[8] ="???\0";                       /* default table extension */

static long lgEvtCnt = 0;
static int igStartUpType = 0;
static int igSeqLen = 6;

static MQBYTE *RecBuffer = NULL;  /* The MQ Receive Buffer */
static MQLONG RecBuffLen = -1;    /* and its length        */
static int igMsgFldSize = 100;

static char pcgGlobalDataBuff[1024*8];
static char *pcgTcpMsgBuf = NULL;
static int igTcpBufLen = 0;
static int igTcpMsgLen = 0;
static int igExitOnError = FALSE;

static int igCurMsgSeqNo = -1;
static int igNxtMsgSeqNo = 0;
static int igMaxMsgSeqNo = 999999;
static int igCurTistCnt = 0;
static char cgCurTgrSeqn[16] = "";
static char cgPrvTimeStamp[16] = "0";
static char cgCurTimeStamp[16] = "";
static char cgCurMsgMkey[32] = "";
static char cgMaxMsgMkey[32] = "";
 
static char cgDbLogging[20] = "\0";
static char cgRecovery[10] = "\0";
static char cgUseAlerter[100] = "\0";
static char cgSelection[200] = "\0";
static char cgNameAlert[1024] = "\0";
static char cgContextAlert[2048] = "\0";
static char cgMQServer[100] = "\0";
static char cgQMgrReceive[50] = "\0";
static char cgQMgrSend[50] = "\0";
static char cgMqReceive[50] = "\0";
static char cgMqSend[1024] = "\0";
static char cgMqSendPrio[110] = "\0";
static int igMqSendPrio = 0;
static char cgCedaSend[10] = "\0";
static char cgCedaSendPrio[10] = "\0";
static char cgSimulation[10] = "\0";
static char cgInterval[10] = "\0";
static char cgReconnectMq[10] = "\0";
static char cgCleanMqr[10] = "\0";
static char cgCleanMqs[10] = "\0";

static int igPutFailed = FALSE;
static int igPutFailedOld = FALSE;
static char cgFirstFailedUrno[20] = "\0";
static char cgFirstFailedData[4002] = "\0";
static char cgUseQName[10] = "\0";
static char igUseQName = FALSE;

static char cgWaitInterval[410] = "\0";
static int bgConnected = FALSE;
static long lgCount = -1;
static int igQueueCount = 0;
static int igWorkSocket = -1;
static int igGotAlarm = FALSE;
static igExpectedSignal = 0;
static int igReadTimeout = 10;

typedef struct
{
	MQHOBJ	HobjSend;
	char	cgQName[50];
	int		igQPrio;
	int		igQDynPrio;
	int		bgQConnected;
	int		igPutFailed;
	int		igPutFailedOld;
	char	cgFirstFailedData[20];
	char	cgFirstFailedUrno[20];
	char	cgUseAlerter[10];
	char	cgNameAlert[50];
	char	cgContextAlert[100];
	char	cgWaitInterval[20];
} _MyQueues;

_MyQueues rgMyQueues[20];

static int igUsedAsTool = FALSE;
static int igRoleIsSender = FALSE;
static int igRoleIsReader = FALSE;
static int igSocket = -1;
static char cgServiceName[32] = "UFIS_CONX";
static char cgHostName[32] = "green";

/*Handles for Sending*/
MQHCONN  HconSend;                   /* connection handle             */
MQHOBJ   HobjSend;                   /* object handle                 */

/*Handles for Receiving */
MQHCONN  HconRecBrowse;              /* connection handle             */
MQHCONN  HconRecGet;                 /* connection handle             */
MQHOBJ   HobjRecBrowse;              /* object handle                 */
MQHOBJ   HobjRecGet;

/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/

extern int init_db ();




static int Init_Process(int argc, char *argv[]);
static int ReadConfigEntry(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer, char *pcpDefault);


static int Reset(void);                       /* Reset program          */
static void Terminate(int ipSleep);            /* Terminate program      */
static void HandleSignal(int);                 /* Handles signals        */
static void HandleQueErr(int);                 /* Handles queuing errors */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/


static int GetCommand(char *pcpCommand, int *pipCmd);

static int ReceiveFromMQ(int ipForWhat);
static int ConnectMQ(char *pcpQManager, MQHCONN *Hcon);
static int TimeToStr(char *pcpTime,time_t lpTime);
static int CheckIfMessageInWork();
static int OpenSendMQ(int ipQueueNumber);
static int CreateLogFile();
static void myinitialize(int argc,char *argv[]);
static int SendMsgToMq(char *pcpData, int ipQueueNumber, char *pcpTgsMsgKey);
static int MarkToSend(char *pcpUrno, int ipQueueNumber);
static int SendFailedMessages(int ipQueueNumber);
static void CleanDb();
static int DoRecovery(char *pcpBeginTime, int ipQueueNumber);
static int GetQueueNumber(char *clQueueName);
static int GetFileContent(void);
static int ConnectIpcAsReader(void);
static int ConnectIpcAsSender(void);
static int GetTcpMsg(char **pcpCmd, char **pcpSel, char **pcpFld, char **pcpDat);
static int PutTcpMsg(char *pcpCommand, char *pcpSelection, char *pcpFields, char *pcpData);

static int MyTcpSend(int ipSocket, char *pcpBuf, int ipBufLen);
static int MyTcpSendBuf(int ipSocket, char *pcpBuf, int ipBufLen);
static int MyTcpCloseSocket(int ipSocket);
static int MyTcpRecv(int ipSocket, char **pppBuf, int *pipBufLen);
static int MyTcpRecvBuf(int ipSocket, char *pcpBuf, int ipBufLen);

static int GetCurrentMsgSequence(char *pcpTable);
static int CheckMessageRecovery(char *pcpTable, char *pcpMkey);
static void GetDataFieldSize(char *pcpTable);

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
    int	ilRC = RC_SUCCESS;			/* Return code			*/
    int	ilCnt = 0;
    int ilWaitSec = 0;
    int ilWaitTtl = 0;
    int ilTryAgain = 5;
    int ilOldDebugLevel = 0;
    int ili = 0;
    int i = 0;
    int ilQueueNumber = 0;

/*    INITIALIZE;	*/
    debug_level = TRACE;
    myinitialize(argc,argv);

    (void)SetSignals(HandleSignal);

  errno = 0;
  i = sigignore(SIGCHLD);
  dbg(TRACE,"MAIN: sigingore(SIGCHLD) returns <%d>; ERRNO=<%s>",i,strerror(errno));


    debug_level = DEBUG;

    dbg(TRACE,"MAIN: version <%s>",sccs_version);
    sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
    dbg(TRACE,"ConfigFile <%s>",cgConfigFile);

    ilRC = Init_Process(argc,argv);


  /* Init the DataBase (Login) */
  if (ilRC == RC_SUCCESS)
  {
    igExpectedSignal = SIGCLD;
    ilCnt = ilTryAgain;
    ilWaitSec = 5;
    ilWaitTtl = 0;
    do
    {
      ilRC = init_db ();
      if (ilRC != RC_SUCCESS)
      {
        sleep (ilWaitSec);
        ilCnt--;
        ilWaitTtl += ilWaitSec;
      }
    }
    while ((ilCnt > 0) && (ilRC != RC_SUCCESS));

    if ((ilRC != RC_SUCCESS) || (ilWaitTtl > 0))
    {
      dbg (TRACE, "WAITED %d SECONDS FOR LOGIN DATABASE", ilWaitTtl);
      if (ilRC != RC_SUCCESS)
      {
        dbg (TRACE, "LOGIN DATABASE FAILED");
      }
    } 
    igExpectedSignal = -1;
  }  

    debug_level = TRACE;
    if (ilRC == RC_SUCCESS)
    {
      dbg (TRACE, "LOGIN DATABASE OK");

      if (igRoleIsReader == TRUE)
      {
        dbg(TRACE,"WMQ CONNECTOR: MY ROLE IS RECEIVER");
        GetDataFieldSize("TGRTAB");
        dbg(TRACE,"=============================================");
        switch (igStartUpType)
        {
	  case 0: /* NONE */
		ReceiveFromMQ(0);
		break;
	  case 1: /* TEST */
		ReceiveFromMQ(1);
		break;
	  case 2: /* TEXT */
		ReceiveFromMQ(2);
		break;
	  case 3: /* FILE */
		ReceiveFromMQ(3);
		break;
	  case 5: /* PORT */
		ilRC = ConnectIpcAsReader();
		break;
	  default:
            dbg(TRACE,"NO START-UP FUNCTION DEFINED, SO TERMINATING ...");
            break;
        } /* end of switch */
      }

      if (igRoleIsSender == TRUE)
      {
        dbg(TRACE,"WMQ CONNECTOR: MY ROLE IS SENDER");
        GetDataFieldSize("TGSTAB");
        dbg(TRACE,"=============================================");
	if((bgConnected == FALSE) && (igStartUpType < 5))
	{
		ilRC = ConnectMQ(cgQMgrSend, &HconSend);
		ilRC = OpenSendMQ(ilQueueNumber);
	}
        switch (igStartUpType)
        {
	  case 1: /* TEST */
                strcpy(pcgGlobalDataBuff,"THIS IS JUST A SHORT TEST MESSAGE.");
		ilRC = SendMsgToMq(pcgGlobalDataBuff, ilQueueNumber,"");
		break;
	  case 2: /* TEXT */
		ilRC = SendMsgToMq(pcgGlobalDataBuff, ilQueueNumber,"");
		break;
	  case 3: /* FILE */
                (void) GetFileContent();
		ilRC = SendMsgToMq(pcgGlobalDataBuff, ilQueueNumber,"");
		break;
	  case 5: /* PORT */
		ilRC = ConnectIpcAsSender();
		break;
	  case 99:
		break;
	  default:
            dbg(TRACE,"NO START-UP FUNCTION DEFINED, SO TERMINATING ...");
            break;
        } /* end of switch */
      }
    }
    else
    {
    }
    dbg(TRACE,"MAIN: -------------------------------> EXIT");
    Terminate(1);

    /* Never comes here */
    exit(0);
      
} /* end of MAIN */

/* ===================================================================== */
static int ConnectIpcAsReader(void)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = RC_SUCCESS;
  int ilLen = 0;
  int ilAttempts = 0;
  int ilMaxAttempts = 5;
  char pclTmp[128];
  if (igSocket >= 0)
  {
    TcpClose(igSocket);
    igSocket = -1;
  }
  ilRc = TcpConnect(&igSocket,cgServiceName,cgHostName);
  if (ilRc == RC_SUCCESS)
  {
    dbg(TRACE,"CONNECTION ESTABLISHED ON SOCKET %d",igSocket);
    sprintf(pclTmp,"READER,PID=%d,SOCK=%d,WAIT",getpid(),igSocket);
    ilLen = strlen(pclTmp) + 1;
    ilRc = PutTcpMsg("INIT", "", "", pclTmp);
    dbg(TRACE,"MSG <%s> LEN=%d SENT=%d",pclTmp,ilLen,ilRc);
    if (ilRc == RC_SUCCESS)
    {
      ilRc = RC_SUCCESS;
    }
    else
    {
      dbg(TRACE,"TRANSMISSION FAILED");
      ilRc = RC_FAIL;
    }
  }
  else
  {
    dbg(TRACE,"CONNECTION FAILED");
    ilRc = RC_FAIL;
  }

  if (ilRc == RC_SUCCESS)
  {
	dbg(TRACE,"%05d: connect for receiving from MQ",__LINE__);
        ilAttempts = 0;
	while((bgConnected == FALSE) && (ilAttempts < ilMaxAttempts))
	{
		dbg(TRACE,"---> CONNECT QUEUE BROWSER");
		ilRc = ConnectMQ(cgQMgrReceive, &HconRecBrowse);
		dbg(TRACE,"---> CONNECT QUEUE READER");
		ilRc = ConnectMQ(cgQMgrReceive, &HconRecGet);
	        if (bgConnected == FALSE)
                {
                  ilAttempts++;
                  sprintf(pclTmp,"READER,WMQWAIT,TRY%d",ilAttempts);
                  ilLen = strlen(pclTmp) + 1;
                  ilRc = PutTcpMsg("INIT", "", "", pclTmp);
                  dbg(TRACE,"MSG <%s> LEN=%d SENT=%d",pclTmp,ilLen,ilRc);
                  if (ilRc == RC_SUCCESS)
                  {
                    ilRc = RC_SUCCESS;
		    sleep(atoi(cgReconnectMq));
                  }
                  else
                  {
                    dbg(TRACE,"TRANSMISSION FAILED");
                    ilAttempts = ilMaxAttempts + 1;
                    ilRc = RC_FAIL;
                  }
                }
	}
        if (bgConnected == TRUE)
        {
          ilRc = RC_SUCCESS;
        }
  }

  if ((ilRc == RC_SUCCESS) && (bgConnected == TRUE) && (igSocket >= 0))
  {
    ilRc = RC_SUCCESS;
    ilRc = GetCurrentMsgSequence("TGRTAB");
  }
  else
  {
    ilRc = RC_FAIL;
  }

  if (igSocket >= 0)
  {
    if (ilRc == RC_SUCCESS)
    {
      sprintf(pclTmp,"READER,READY");
    }
    else
    {
      sprintf(pclTmp,"READER,TERMINATE");
    }
    ilLen = strlen(pclTmp) + 1;
    dbg(TRACE,"SEND FINAL INIT MSG <%s> TO PARENT",pclTmp);
    ilGetRc = PutTcpMsg("INIT", "", "", pclTmp);
    dbg(DEBUG,"MSG <%s> LEN=%d SENT=%d",pclTmp,ilLen,ilGetRc);
    if (ilGetRc != RC_SUCCESS)
    {
      dbg(TRACE,"TRANSMISSION FAILED");
      ilRc = RC_FAIL;
    }
  }

  if (ilRc == RC_SUCCESS)
  {
    ilRc = ReceiveFromMQ(4);
  }

  if (ilRc != RC_SUCCESS)
  {
    if (igSocket >= 0)
    {
      TcpClose(igSocket);
      igSocket = -1;
    }
  }
  return ilRc;
}

/* ******************************************************************* */
static int PutTcpMsg(char *pcpCommand, char *pcpSelection, char *pcpFields, char *pcpData)
{
  int ilRc = RC_SUCCESS;
  int ilLen = 0;
  char pclTmp[32] = "";

  sprintf(pcgTcpMsgBuf,"{#LEN#}0000000000{/#LEN#}\n"
                         "{#CMD#}%s{/#CMD#}\n{#SEL#}%s{/#SEL#}\n"
                         "{#FLD#}%s{/#FLD#}\n{#DAT#}%s{/#DAT#}",
                         pcpCommand,pcpSelection,pcpFields,pcpData);

  igTcpMsgLen = strlen(pcgTcpMsgBuf) + 1;
  sprintf(pclTmp,"{#LEN#}%0.10d{/#LEN#}",igTcpMsgLen);
  ilLen = strlen(pclTmp);
  strncpy(pcgTcpMsgBuf,pclTmp,ilLen);

  ilRc = MyTcpSend(igSocket,pcgTcpMsgBuf,igTcpMsgLen);
  if (ilRc == igTcpMsgLen)
  {
    dbg(TRACE,"MSG <%s> SENT TO IPCCON LEN=%d",pcpCommand,ilRc);
    ilRc = RC_SUCCESS;
  }
  else
  {
    dbg(TRACE,"MSG TO IPCCON FAILED (RC=%d)",ilRc);
    ilRc = RC_FAIL;
  }

  return ilRc;
}

/* ===================================================================== */
static int ConnectIpcAsSender(void)
{
  int ilRc = RC_SUCCESS;
  int ilReady = RC_FAIL;
  int ilLoop = TRUE;
  int ilQueueNumber = 0;
  int ilLen = 0;
  int ili = 0;
  char *pclCmd = NULL;
  char *pclSel = NULL;
  char *pclFld = NULL;
  char *pclDat = NULL;
  char pclTmp[128];
  char pclOutCmd[16] = "";
  char pclOutSel[16] = "";
  char pclOutFld[16] = "";
  char pclOutDat[16] = "";
  
  if (igSocket >= 0)
  {
    TcpClose(igSocket);
    igSocket = -1;
  }
  ilRc = TcpConnect(&igSocket,cgServiceName,cgHostName);
  if (ilRc == RC_SUCCESS)
  {
    dbg(TRACE,"CONNECTION ESTABLISHED ON SOCKET %d",igSocket);
    sprintf(pclTmp,"SENDER,PID=%d,SOCK=%d,WAIT",getpid(),igSocket);
    ilLen = strlen(pclTmp) + 1;
    ilRc = PutTcpMsg("INIT", "", "", pclTmp);
    dbg(TRACE,"MSG <%s> LEN=%d SENT=%d",pclTmp,ilLen,ilRc);
    if (ilRc == RC_SUCCESS)
    {
      ilRc = RC_SUCCESS;
    }
    else
    {
      dbg(TRACE,"TRANSMISSION FAILED");
      ilRc = RC_FAIL;
    }
  }
  else
  {
    dbg(TRACE,"CONNECTION FAILED");
    ilRc = RC_FAIL;
  }

  if (ilRc == RC_SUCCESS)
  {
    if (igRoleIsSender == TRUE)
    {
	dbg(TRACE,"%05d: connect for sending to MQ",__LINE__);
	if((strlen(cgQMgrSend) > 0) && (bgConnected == FALSE))
	{
		ilRc = ConnectMQ(cgQMgrSend, &HconSend);
    		if (ilRc == RC_SUCCESS)
    		{
		  dbg(TRACE,"%05d: open %d queues for sending to MQ",__LINE__, igQueueCount);
		  for(ili = 0 ; (ili < igQueueCount) && (ilRc == RC_SUCCESS) ; ili++)
		  {
		        dbg(TRACE,"open queue %d for sending to MQ",ili);
			ilRc = OpenSendMQ(ili);	
		  }
		}
		if (ilRc != RC_SUCCESS)
		{
		  dbg(TRACE,"UNABLE TO CONNECT TO QUEUE");
		}
	}
	else
	{
		dbg(TRACE,"%05d: Send-Qmanager not configured or already connected",__LINE__);
    		ilRc = RC_FAIL;
	}
    }
  }

  if (ilRc != RC_SUCCESS)
  {
    ilLoop = FALSE;
  }

  if (igSocket >= 0)
  {
    if (ilRc == RC_SUCCESS)
    {
      dbg(TRACE,"CONNECTION ESTABLISHED ON QUEUES");
      sprintf(pclTmp,"SENDER,WMQCONN_OK,WAIT");
      ilRc = PutTcpMsg("INIT", "", "", pclTmp);
      dbg(TRACE,"MSG <%s> LEN=%d SENT=%d",pclTmp,ilLen,ilRc);
      ilReady = CheckMessageRecovery("TGSTAB","");
    }

    if ((ilRc == RC_SUCCESS) && (ilReady == RC_SUCCESS))
    {
      dbg(TRACE,"BRIDGE CONNECTION READY TO GO ...");
      sprintf(pclTmp,"SENDER,RECOVERY_OK,READY");
      ilReady = RC_SUCCESS;
    }
    else
    {
      dbg(TRACE,"CONNECTION ON QUEUES OR MESSAGE RECOVERY FAILED");
      sprintf(pclTmp,"SENDER,TERMINATING");
      ilReady = RC_FAIL;
    }
    ilLen = strlen(pclTmp) + 1;
    ilRc = PutTcpMsg("INIT", "", "", pclTmp);
    dbg(TRACE,"MSG <%s> LEN=%d SENT=%d",pclTmp,ilLen,ilRc);
    if (ilRc == RC_SUCCESS)
    {
      ilRc = ilReady;
    }
    else
    {
      dbg(TRACE,"TRANSMISSION FAILED");
      ilRc = RC_FAIL;
      ilReady = RC_FAIL;
    }
  }

  ilRc = ilReady;
  while ((ilRc == RC_SUCCESS) && (ilLoop == TRUE))
  {
    ilRc = GetTcpMsg(&pclCmd, &pclSel, &pclFld, &pclDat);
    if (ilRc == RC_SUCCESS)
    {
      dbg(DEBUG,"CHECK COMMAND <%s> FROM IPC",pclCmd);
      strcpy(pclOutCmd,"TCP_ACK");
      strcpy(pclOutSel,"");
      strcpy(pclOutFld,"");
      strcpy(pclOutDat,"");
      if ((strcmp(pclCmd,cgSendCmd) == 0) || (strcmp(pclCmd,"MSGO") == 0))
      {
        strcpy(pcgGlobalDataBuff,pclDat);
        igCurMsgSeqNo = atoi(pclFld);
        if (igCurMsgSeqNo != igNxtMsgSeqNo)
        {
          dbg(TRACE,"=== MESSAGE SYNCHRONIZATION ERROR ===");
          dbg(TRACE,"UNEXPECTED SEQUENCE %d RECEIVED (EXPECTED = %d)",igCurMsgSeqNo,igNxtMsgSeqNo);
          ilRc = CheckMessageRecovery("TGSTAB",pclSel);
        }
        if (igCurMsgSeqNo == igNxtMsgSeqNo)
        {
          ilRc = SendMsgToMq(pcgGlobalDataBuff, ilQueueNumber, pclSel);
          igNxtMsgSeqNo++;
          if (igNxtMsgSeqNo > igMaxMsgSeqNo)
          {
            igNxtMsgSeqNo = 1;
          }
        }
      }
      else if (strcmp(pclCmd,"FILE") == 0)
      {
        strcpy(cgActSendFile,pclDat);
        ilRc = GetFileContent();
        if (ilRc == RC_SUCCESS)
        {
	  ilRc = SendMsgToMq(pcgGlobalDataBuff, ilQueueNumber,"");
        }
      }
      else if (strcmp(pclCmd,"FNAM") == 0)
      {
        sprintf(cgActSendFile,"%s%s",cgDefSendFilePath,pclDat);
        ilRc = GetFileContent();
        if (ilRc == RC_SUCCESS)
        {
	  ilRc = SendMsgToMq(pcgGlobalDataBuff, ilQueueNumber,"");
        }
        ilLoop = TRUE;
      }
      else if (strcmp(pclCmd,"FSEQ") == 0)
      {
        sprintf(cgActSendFile,"%s%s",cgDefSendFilePath,pclDat);
        ilLen = strlen(pclDat);
        if (ilLen <= igSeqLen)
        {
          strcpy(pclTmp,"000000000000000000000000000000000000000000");
          strcpy(&pclTmp[igSeqLen-ilLen],pclDat);
          sprintf(cgActSendFile,"%s%s",cgDefSendFilePath,pclTmp);
        }
        ilRc = GetFileContent();
        if (ilRc == RC_SUCCESS)
        {
	  ilRc = SendMsgToMq(pcgGlobalDataBuff, ilQueueNumber,"");
        }
      }
      else if (strcmp(pclCmd,"CHKC") == 0)
      {
        ilLoop = TRUE;
      }
      else if (strcmp(pclCmd,"EXIT") == 0)
      {
        ilLoop = FALSE;
      }
      else
      {
        dbg(TRACE,"GOT UNKNOWN CMD <%s>",pclCmd);
      }
      ilRc = PutTcpMsg(pclOutCmd, pclOutSel, pclOutFld, pclOutDat);
    }
    else
    {
      if (ilRc == RC_TIMEOUT)
      {
        ilRc = RC_SUCCESS;
      }
    }
  }

  if (igSocket >= 0)
  {
    TcpClose(igSocket);
    igSocket = -1;
  }

  return ilRc;
}


/* ===================================================================== */
static int GetTcpMsg(char **pcpCmd, char **pcpSel, char **pcpFld, char **pcpDat)
{
  int ilRc = RC_SUCCESS;
  long llCmdSize = 0;
  long llSelSize = 0;
  long llFldSize = 0;
  long llDatSize = 0;
  int ilCnt = 0;
  char pclDummy[4] = "";
  dbg(TRACE,"--- WAITING ON TCP/IP BRIDGE <%s> (SOCK=%d) ---",cgServiceName,igSocket);
  if (pcgTcpMsgBuf != NULL)
  {
    pcgTcpMsgBuf[0] = 0x00;
  }
  ilRc = MyTcpRecv(igSocket,&pcgTcpMsgBuf,&igTcpBufLen);
  dbg(DEBUG,"GOT MSG LEN(RC)=%d FROM TCP",ilRc);

  ilCnt = 0;
  if (ilRc > 0)
  {
    ilRc = RC_SUCCESS;
    dbg(DEBUG,"GOT MSG <%s>",pcgTcpMsgBuf);
    *pcpCmd = CedaGetKeyItem(pclDummy, &llCmdSize, pcgTcpMsgBuf, "{#CMD#}", "{/#CMD#}", FALSE);
    *pcpSel = CedaGetKeyItem(pclDummy, &llSelSize, pcgTcpMsgBuf, "{#SEL#}", "{/#SEL#}", FALSE);
    *pcpFld = CedaGetKeyItem(pclDummy, &llFldSize, pcgTcpMsgBuf, "{#FLD#}", "{/#FLD#}", FALSE);
    *pcpDat = CedaGetKeyItem(pclDummy, &llDatSize, pcgTcpMsgBuf, "{#DAT#}", "{/#DAT#}", FALSE);
    if (*pcpCmd != NULL)
    {
      (*pcpCmd)[llCmdSize] = 0x00;
      dbg(TRACE,"GOT CMD <%s>",*pcpCmd);
      ilCnt++;
    }
    if (*pcpSel != NULL)
    {
      (*pcpSel)[llSelSize] = 0x00;
      if (llSelSize > 0)
      {
        dbg(TRACE,"GOT SEL <%s>",*pcpSel);
      }
      ilCnt++;
    }
    if (*pcpFld != NULL)
    {
      (*pcpFld)[llFldSize] = 0x00;
      if (llFldSize > 0)
      {
        dbg(TRACE,"GOT FLD <%s>",*pcpFld);
      }
      ilCnt++;
    }
    if (*pcpDat != NULL)
    {
      (*pcpDat)[llDatSize] = 0x00;
      if (llDatSize > 0)
      {
        dbg(TRACE,"GOT DAT\n%s\n(LEN=%d)",*pcpDat,llDatSize);
      }
      ilCnt++;
    }

    if (ilCnt != 4)
    {
      dbg(TRACE,"GOT INCOMPLETE MESSAGE");
      ilRc = RC_FAIL;
    }

  }
  else
  {
    if (ilRc != RC_TIMEOUT)
      ilRc = RC_FAIL;

  }

  return ilRc;
}

/* ===================================================================== */
static int ReadConfigEntry(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer, char *pcpDefault)
{
    int ilRC = RC_SUCCESS;

    char clSection[124] = "\0";
    char clKeyword[124] = "\0";

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRC = iGetConfigEntry(cgConfigFile,clSection,clKeyword, CFG_STRING,pcpCfgBuffer);
    if(ilRC != RC_SUCCESS)
    {
	strcpy(pcpCfgBuffer,pcpDefault);
	dbg(TRACE,"DEFAULT VALUE [%s] <%s> = <%s>", clSection, clKeyword ,pcpCfgBuffer);
    } 
    else
    {
	dbg(TRACE,"CONFIGURATION [%s] <%s> = <%s>", clSection, clKeyword ,pcpCfgBuffer);
    }/* end of if */
    return ilRC;
}


/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/

static int Init_Process(int argc, char *argv[])
{
    int  ilRC = RC_SUCCESS;			/* Return code */
    char clSection[64] = "\0";
    char clKeyword[64] = "\0";
    long pclAddFieldLens[12];
    char pclAddFields[256] = "\0";
    char pclSelection[1024] = "\0";
    char pclTmpData[512];
    short slCursor = 0;
    short slSqlFunc = 0;
    char  clBreak[24] = "\0";
    char clQueueName[30] = "\0";
    int ili = 0;
    char clTemp[100] = "\0";
    int blMoreQueues = TRUE;
    int ilPid = 0;
    int ilLen = 0;

    debug_level = DEBUG;

    ilRC = RC_SUCCESS;
    igUsedAsTool = FALSE;
    igRoleIsSender = FALSE;
    igRoleIsReader = FALSE;
    if (ilRC == RC_SUCCESS)
    {
        dbg(TRACE,"===> CONFIG OF WMQ CONNECTOR");
	ilRC = ReadConfigEntry("MAIN","ROLE",clTemp,"UNDEF");
        if (strcmp(clTemp,"SEND") == 0)
        {
            igRoleIsSender = TRUE;
       	    ReadConfigEntry("MAIN","SEND_CMD",cgSendCmd,"MSGO");
       	    ReadConfigEntry("MAIN","SEND_FILE_PATH",cgDefSendFilePath,"/ceda/exco/");
        }
        else if (strcmp(clTemp,"READ") == 0)
        {
            igRoleIsReader = TRUE;
       	    ReadConfigEntry("MAIN","SAVE_FILE_PATH",cgDefSaveFilePath,"/ceda/exco/");
        }
        else
        {
          dbg(TRACE,"ROLE UNDEFINED");
          dbg(TRACE,"--------------");
        }
    }

    if (ilRC == RC_SUCCESS)
    {
        dbg(TRACE, "===> CONFIG OF TCP/IP NETWORK");
        ilRC = ReadConfigEntry("NETWORK","HOSTNAME",cgHostName,"UNDEFINED");
        ilRC = ReadConfigEntry("NETWORK","SERVICE",cgServiceName,"UNDEFINED");

        if (igRoleIsSender == TRUE)
        {
          dbg(TRACE,"===> CONFIG OF WMQ SENDER");
	  ilRC = ReadConfigEntry("WMQUEUE","MQSERVER",cgMQServer,"");
	  ilRC = ReadConfigEntry("WMQUEUE","MQ_MGR_SEND",cgQMgrSend,"");
	  ilRC = ReadConfigEntry("WMQUEUE","MQ_SEND",cgMqSend,"");
	  ilRC = ReadConfigEntry("WMQUEUE","MQ_SEND_PRIO",cgMqSendPrio,"0");
	  ilRC = ReadConfigEntry("RECONNECT","RECONNECT_MQ",cgReconnectMq,"10");
	}

        if (igRoleIsReader == TRUE)
        {
          dbg(TRACE,"===> CONFIG OF WMQ READER");
	  ilRC = ReadConfigEntry("WMQUEUE","MQSERVER",cgMQServer,"");
	  ilRC = ReadConfigEntry("WMQUEUE","MQ_MGR_RECEIVE",cgQMgrReceive,"");
	  ilRC = ReadConfigEntry("WMQUEUE","MQ_RECEIVE",cgMqReceive,"");
	  ilRC = ReadConfigEntry("WMQUEUE","WAIT_INTERVAL",cgWaitInterval,"60000");
	  ilRC = ReadConfigEntry("RECONNECT","RECONNECT_MQ",cgReconnectMq,"10");
	}

        dbg(TRACE,"===> CONFIG FOR LOGGING");
	ilRC = ReadConfigEntry("MAIN","DB_LOGGING",cgDbLogging,"OFF");
       	ilRC = ReadConfigEntry("MAIN","RECOVERY",cgRecovery,"OFF");
        if (igRoleIsReader == TRUE)
        {
	  ilRC = ReadConfigEntry("CLEANUP","CLEAN_MQR",cgCleanMqr,"30");
	}
        if (igRoleIsSender == TRUE)
        {
	  ilRC = ReadConfigEntry("CLEANUP","CLEAN_MQS",cgCleanMqs,"30");
	}
	if ((strcmp(cgDbLogging,"OFF") == 0) && (strcmp(cgRecovery,"OFF") != 0))
	{
		strcpy(cgRecovery,"OFF");
		dbg(TRACE,"%05d: RECOVERY switched to <OFF> because DB-logging is <OFF>",__LINE__);
	}

        dbg(TRACE,"===> CONFIG FOR ALERTER");
	ilRC = ReadConfigEntry("MAIN","USE_ALERTER",cgUseAlerter,"NO");
	ilRC = ReadConfigEntry("MAIN","NAME_SEND_TO_ALERTER",cgNameAlert,"");
	ilRC = ReadConfigEntry("MAIN","USE_QUEUENAME_FOR_INAM",cgUseQName,"NO");
	ilRC = ReadConfigEntry("MAIN","CONTEXT_FOR_ALERT",cgContextAlert,"");
	if((strcmp(cgUseQName,"NO") == 0) && (strstr(cgMqSend,",") == NULL))
	{
		igUseQName = FALSE;
	}
	else
	{
		igUseQName = TRUE;
	}

	ili = 0;
	while((igRoleIsSender == TRUE) && (blMoreQueues == TRUE))
	{
		ilRC = get_item(ili+1,cgMqSend,rgMyQueues[ili].cgQName,0,",","\0","\0");
		if(ilRC == FALSE) blMoreQueues = FALSE;

		ilRC = get_item(ili+1,cgMqSendPrio,clTemp,0,",","\0","\0");
		if(ilRC == FALSE) blMoreQueues = FALSE;
		rgMyQueues[ili].igQPrio = atoi(clTemp);
		rgMyQueues[ili].igQDynPrio = -1;

		ilRC = get_item(ili+1,cgUseAlerter,rgMyQueues[ili].cgUseAlerter,0,",","\0","\0");
		if(ilRC == FALSE) blMoreQueues = FALSE;

		ilRC = get_item(ili+1,cgNameAlert,rgMyQueues[ili].cgNameAlert,0,",","\0","\0");
		if(ilRC == FALSE) blMoreQueues = FALSE;
			
		ilRC = get_item(ili+1,cgContextAlert,rgMyQueues[ili].cgContextAlert,0,",","\0","\0");
		if(ilRC == FALSE) blMoreQueues = FALSE;
			
		ilRC = get_item(ili+1,cgWaitInterval,rgMyQueues[ili].cgWaitInterval,0,",","\0","\0");
		if(ilRC == FALSE) blMoreQueues = FALSE;
			
		rgMyQueues[ili].bgQConnected = FALSE;
		rgMyQueues[ili].igPutFailed = FALSE;
		rgMyQueues[ili].igPutFailedOld = FALSE;
		rgMyQueues[ili].cgFirstFailedUrno[0] = '\0';
		rgMyQueues[ili].cgFirstFailedData[0] = '\0';
		ili++;
	}
	/* igQueueCount = ili -1; */
	igQueueCount = ili;

        if (igRoleIsSender == TRUE)
        {
	    for(ili = 0 ; ili < igQueueCount ; ili++)
	    {
		dbg(DEBUG,"%05d: ......... QUEUE <%d> .......... found",__LINE__,ili);
		dbg(DEBUG,"%05d: QName: <%s>",__LINE__,rgMyQueues[ili].cgQName);
		dbg(DEBUG,"%05d: QPrio: <%d>",__LINE__,rgMyQueues[ili].igQPrio);
		dbg(DEBUG,"%05d: QUseAlerter: <%s>",__LINE__,rgMyQueues[ili].cgUseAlerter);		
		dbg(DEBUG,"%05d: QNameAlert: <%s>",__LINE__,rgMyQueues[ili].cgNameAlert);
		dbg(DEBUG,"%05d: QContextAlert: <%s>",__LINE__,rgMyQueues[ili].cgContextAlert);
		dbg(DEBUG,"%05d: QWaitInterval: <%s>",__LINE__,rgMyQueues[ili].cgWaitInterval);
		dbg(DEBUG,"%05d: QConnected: <%d>",__LINE__,rgMyQueues[ili].bgQConnected);
		dbg(DEBUG,"%05d: QFirstFailedUrno: <%s>",__LINE__,rgMyQueues[ili].cgFirstFailedUrno);
		dbg(DEBUG,"%05d: QFirstFailedData: <%s>",__LINE__,rgMyQueues[ili].cgFirstFailedData);
	    }
	}
      ilRC = RC_SUCCESS;
    }

    igTcpBufLen = 1024 * 1024 * 4;
    pcgTcpMsgBuf = (char *)malloc(igTcpBufLen);

  igStartUpType = 0;
  if (argc > 1)
  {
    dbg(TRACE,"=============================================");
    dbg(TRACE,"COMMAND LINE PARAMETERS (START-UP ARGUMENTS):");
    dbg(TRACE,"---------------------------------------------");
    for (ili=0; ili<argc; ili++)
    {
      dbg(TRACE,"PARAM %d= <%s>",ili,argv[ili]);
    }
    dbg(TRACE,"=============================================");
    strcpy(pclTmpData,argv[1]);
    str_chg_upc(pclTmpData);
    dbg(TRACE,"COMMAND <%s>",pclTmpData);

    if (igRoleIsSender == TRUE)
    {
      if (strcmp(pclTmpData,"TEST") == 0)
      {
        igStartUpType = 1;
      } 
      else if (strcmp(pclTmpData,"TEXT") == 0)
      {
        igStartUpType = 2;
        strcpy(pcgGlobalDataBuff,"TEXT: THIS IS A DEFAULT MESSAGE.");
        if (argc > 2)
        {
          strcpy(pcgGlobalDataBuff,argv[2]);
        }
      } 
      else if (strcmp(pclTmpData,"FILE") == 0)
      {
        igStartUpType = 3;
        strcpy(cgActSendFile,"NO_FILENAME_DEFINED");
        if (argc > 2)
        {
          strcpy(cgActSendFile,argv[2]);
        }
      } 
      else if (strcmp(pclTmpData,"FNAM") == 0)
      {
        igStartUpType = 3;
        sprintf(cgActSendFile,"%sNO_FILENAME_DEFINED",cgDefSendFilePath);
        if (argc > 2)
        {
          sprintf(cgActSendFile,"%s%s",cgDefSendFilePath,argv[2]);
        }
      } 
      else if (strcmp(pclTmpData,"FSEQ") == 0)
      {
        igStartUpType = 3;
        sprintf(cgActSendFile,"%sNO_FILENAME_DEFINED",cgDefSendFilePath);
        if (argc > 2)
        {
          sprintf(cgActSendFile,"%s%s",cgDefSendFilePath,argv[2]);
          ilLen = strlen(argv[2]);
          if (ilLen <= igSeqLen)
          {
            strcpy(clTemp,"000000000000000000000000000000000000000000");
            strcpy(&clTemp[igSeqLen-ilLen],argv[2]);
            sprintf(cgActSendFile,"%s%s",cgDefSendFilePath,clTemp);
          }
        }
      } 
      else if (strcmp(pclTmpData,"PORT") == 0)
      {
        igStartUpType = 5;
      } 
      else
      {
        dbg(TRACE,"COMMAND <%s> UNKNOWN * USING DEFAULT <TEST>",pclTmpData);
        dbg(TRACE,"=============================================");
      }
    }

    if (igRoleIsReader == TRUE)
    {
      if (strcmp(pclTmpData,"TEST") == 0)
      {
        igStartUpType = 1;
      } 
      else if (strcmp(pclTmpData,"MQGET") == 0)
      {
        igStartUpType = 2;
      } 
      else if (strcmp(pclTmpData,"MQRUN") == 0)
      {
        igStartUpType = 3;
      } 
      else if (strcmp(pclTmpData,"PORT") == 0)
      {
        igStartUpType = 5;
      } 
      else
      {
        dbg(TRACE,"COMMAND <%s> UNKNOWN * USING DEFAULT <TEST>",pclTmpData);
        dbg(TRACE,"=============================================");
       }
    }
  }
  else
  {
    if (igRoleIsReader == TRUE)
    {
      printf("=============================\n");
      printf("WMQ CONNECTOR USAGE AS READER\n");
      printf("-----------------------------\n");
      printf("%s test\n",mod_name);
      printf("%s mqget\n",mod_name);
      printf("%s mqrun\n",mod_name);
      printf("=============================\n");
    }
    if (igRoleIsSender == TRUE)
    {
      printf("=============================\n");
      printf("WMQ CONNECTOR USAGE AS SENDER\n");
      printf("-----------------------------\n");
      printf("%s test\n",mod_name);
      printf("%s text \"your text\"\n",mod_name);
      printf("%s file \"path\\file\"\n",mod_name);
      printf("%s fnam filename\n",mod_name);
      printf("%s fseq msg_no\n",mod_name);
      printf("=============================\n");
    }
  }

    if ((igRoleIsSender == TRUE) && (igStartUpType < 5))
    {
	dbg(TRACE,"%05d: connect for sending to MQ",__LINE__);
	if((strlen(cgQMgrSend) > 0) && (bgConnected == FALSE))
	{
		ilRC = ConnectMQ(cgQMgrSend, &HconSend);
    		if (ilRC == RC_SUCCESS)
    		{
		  dbg(TRACE,"%05d: open %d queues for sending to MQ",__LINE__, igQueueCount);
		  for(ili = 0 ; ili < igQueueCount ; ili++)
		  {
		        dbg(TRACE,"open queue %d for sending to MQ",ili);
			ilRC = OpenSendMQ(ili);	
		  }
		}
	}
	else
	{
		dbg(TRACE,"%05d: Send-Qmanager not configured or already connected",__LINE__);
	}
    }

    if ((igRoleIsReader == TRUE) && (igStartUpType < 5))
    {
	dbg(TRACE,"%05d: connect for receiving from MQ",__LINE__);
	while(bgConnected == FALSE)
	{
		dbg(TRACE,"---> CONNECT QUEUE BROWSER");
		ilRC = ConnectMQ(cgQMgrReceive, &HconRecBrowse);
		dbg(TRACE,"---> CONNECT QUEUE READER");
		ilRC = ConnectMQ(cgQMgrReceive, &HconRecGet);
	        if (bgConnected == FALSE)
                {
		  sleep(atoi(cgReconnectMq));
                }
	}
    }

    dbg(TRACE,"MY PID: <%d>",getpid());

    if (ilRC != RC_SUCCESS)
    {
      dbg(TRACE,"=====================");
      dbg(TRACE,"Init_Process failed");
      dbg(TRACE,"=====================");
    }
    else
    {
      dbg(TRACE,"Init_Process is OK");
    }

    debug_level = TRACE;
	
    return(ilRC);
	
} /* end of Init_Process */




/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
    int	ilRC = RC_SUCCESS;				/* Return code */
	
    dbg(TRACE,"Reset: now resetting");
	
    return ilRC;
	
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
    int ilRC = RC_SUCCESS;
	int ili = 0;

	MQLONG   C_options;              /* MQCLOSE options                 */
    MQLONG   CompCode;               /* completion code                 */
    MQLONG   Reason;                 /* reason code                     */

    C_options = MQCO_NONE;           /* no close options             */

    if ((igRoleIsReader == TRUE) && (igStartUpType >= 0))
    {
	MQCLOSE (HconRecBrowse, &HobjRecBrowse, C_options, &CompCode, &Reason);
	dbg(TRACE,"Terminate: Closed Browse Queue ended with Reasoncode: <%d>, CompletionCode: <%d>",Reason,CompCode);
	MQCLOSE (HconRecGet, &HobjRecGet, C_options, &CompCode, &Reason);
	dbg(TRACE,"Terminate: Closed Reader Queue ended with Reasoncode: <%d>, CompletionCode: <%d>",Reason,CompCode);
	MQDISC(&HconRecBrowse,&CompCode,&Reason);
	dbg(TRACE,"Terminate: Disconnect Browse connection ended with Reasoncode: <%d>, CompletionCode: <%d>",Reason,CompCode);
	/* MQDISC Disconnects from QueueManager */
        /* So the second call is not necessary. */
	/* MQDISC(&HconRecGet,&CompCode,&Reason); */
	dbg(TRACE,"Terminate: Disconnect Reader connection ended with Reasoncode: <%d>, CompletionCode: <%d>",Reason,CompCode);
    }

    if(igRoleIsSender == TRUE)   
    {
	for(ili = 0 ; ili < igQueueCount ; ili++)
	{
		MQCLOSE (HconSend, &rgMyQueues[ili].HobjSend, C_options, &CompCode, &Reason);	
	}

	MQDISC(&HconSend,&CompCode,&Reason);
	dbg(TRACE,"Terminate: Closed Send connection ended with Reasoncode: <%d>, CompletionCode: <%d>",Reason,CompCode);
    }

    /* unset SIGCHLD ! DB-Child will terminate ! */

    dbg(TRACE,"Terminate: now DB logoff ...");
    logoff();

    if (igSocket >= 0)
    {
      dbg(TRACE,"TCP/IP BRIDGE CLOSED ...");
      TcpClose(igSocket);
      igSocket = -1;
    }

    dbg(TRACE,"Terminate: now leaving ...");
    fclose(outp);
    exit(0);
	
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
//Below function got changed .. its throwing SIGCHLD signal .. remove that part .. need to check.
static void HandleSignal(int ipSig)
{
    int    ilRC = RC_SUCCESS;            /* Return code */
    switch(ipSig)
    {
	case SIGTERM:
		dbg(TRACE,"SIGNAL <%d> (SIGTERM) RECEIVED. TERMINATING NOW ...",ipSig);
                Terminate(0);
		break;
        case SIGCLD:
            if (igExpectedSignal == SIGCLD)
            {
              dbg(DEBUG,"GOT EXPECTED SIGNAL <SIGCLD> (%d)",ipSig);
            }
            else
            {
              dbg(TRACE,"GOT UNEXPECTED SIGNAL <SIGCLD> (%d)",ipSig);
            }
            break;
        case SIGPIPE:
            dbg(TRACE,"GOT SIGNAL <SIGPIPE> (%d) (BROKEN LINK)",ipSig);
            break;
	case SIGALRM:
                if (igExpectedSignal == SIGALRM)
                {
                  igGotAlarm = TRUE;
		  dbg(DEBUG,"EXPECTED SIGNAL RECEIVED <%d>(SIGALRM)",ipSig);
                }
                else
                {
		  dbg(TRACE,"sql_handle_sig: Received Unexpected Signal<%d>(SIGALRM)",ipSig);
                }
		break;
	default:
		dbg(TRACE,"sql_handle_sig: Received Unexpected Signal<%d>",ipSig);
		return;
		break;
    } /* end of switch */
    /* exit(0); */
    
} /* end of HandleSignal */

/******************************************************************************/
/******************************************************************************/
static int ConnectMQ(char *pcpQManager, MQHCONN *Hcon)
{
    static int ilRC = RC_SUCCESS;
 
    MQLONG   CompCode;               /* completion code               */
    MQLONG   CReason;                /* reason code for MQCONN        */

    static char clMyMQServer[100] = "\0";
    static char clNowTime[20] = "\0";
    static char clAlert[256] = "\0";

    /* SET ENVIRONMENT TO CONNECT TO THE RIGHT QMANAGER */
    sprintf(clMyMQServer,"MQSERVER=%s",cgMQServer);
    ilRC=putenv(clMyMQServer);
   
    /* CONNECT TO GIVEN QMANAGER */
    CompCode = MQCC_FAILED;
    
	MQCONN(pcpQManager,                  /* queue manager                  */
	       Hcon,                         /* connection handle              */
	       &CompCode,                    /* completion code                */
	       &CReason);                    /* reason code                    */

	/* report reason and stop if it failed     */
	if (CompCode == MQCC_FAILED)
	{
	    dbg(TRACE,"%05d: MQCONN ended with reason code <%d>, now waiting <%s> seconds and try to reconnect",__LINE__, CReason, cgReconnectMq);
		if(strcmp(cgUseAlerter,"YES") == 0)
		{
			sprintf(clAlert,"<%s>: MQCONN ended with reason code <%d> while connecting to QManager <%s>",cgContextAlert,CReason,pcpQManager);
			ilRC = AddAlert2(cgNameAlert," "," ","E",clAlert," ",TRUE,FALSE,"ALERT",mod_name," "," ");
		}
		else
		{
			dbg(TRACE,"%05d: !!!!!!!!!! ERROR WHILE connecting to Qmanager !!!!!!!!!!!!!!!",__LINE__);
		}
		bgConnected = FALSE;	   
	}
	else
	{
		bgConnected = TRUE;
	    dbg(TRACE,"%05d: MQCONN connected with QManager <%s>",__LINE__, pcpQManager);
	    dbg(TRACE,"%05d: on Server: <%s>",__LINE__,cgMQServer); 
	}
    return ilRC;
}

/* ********************************************************************************** */
static int ReceiveFromMQ(int ipForWhat)
{
    int ilRC = RC_SUCCESS;
    long llMsgCnt = 0;
    MQLONG   OpenBrowseOptions;              /* MQOPEN options                */
    MQHOBJ   QueueBrowseHobj;                   /* object handle                 */
    MQOD     QueueBrowseOd = {MQOD_DEFAULT};    /* Object Descriptor             */
    MQLONG   QueueBrowseOpenCode;               /* MQOPEN completion code        */
    MQLONG   QueueBrowseCompCode;               /* MQOPEN completion code        */
    MQLONG   QueueBrowseReason;                 /* reason code                   */
    MQGMO    QueueBrowseGmo = {MQGMO_DEFAULT};   /* get message options           */
    MQMD     ReceiveBrowseMd = {MQMD_DEFAULT};    /* Message Descriptor            */

    MQLONG   OpenOptions;              /* MQOPEN options                */
    MQHOBJ   QueueHobj;                   /* object handle                 */
    MQOD     QueueOd = {MQOD_DEFAULT};    /* Object Descriptor             */
    MQLONG   QueueOpenCode;               /* MQOPEN completion code        */
    MQLONG   QueueCompCode;               /* MQOPEN completion code        */
    MQLONG   QueueReason;                 /* reason code                   */
    MQGMO    QueueGmo = {MQGMO_DEFAULT};   /* get message options           */
    MQMD     ReceiveMd = {MQMD_DEFAULT};    /* Message Descriptor            */

    MQLONG   MessLen;                /* message length received       */

    int ilMsgFldPos = 0;
    int ilMsgFldLen = 0;
    int ilMsgFldCurPack = 0;
    int ilMsgFldTtlPack = 0;
    int ilMsgFldTistBgn = 0;
    int ilMsgCount = 0;
    int ilNoMsgCount = 0;
    char clMsgId[30] = "\0";
    char clMyTime[20] = "\0";
    short slFkt = 0;
    short slCursor = 0;
    char clData[100] = "\0";
    char clUrno[12] = "\0";
    char clNowTime[20] = "\0";
    char clSqlBuf[4096*2] ="\0";
    char clMsgFldData[4096] ="\0";
    char clMsgOutSel[64];
  char clMsgOutCmd[8] = "\0";

    static char clAlert[256] = "\0";

  char *pclCmd = NULL;
  char *pclSel = NULL;
  char *pclFld = NULL;
  char *pclDat = NULL;


  /* Initialize the MQ receive buffer */
  if (RecBuffLen <= 0)
  {
    RecBuffLen = igMsgFldSize;
    RecBuffer = (MQBYTE *)malloc(RecBuffLen);
    dbg(TRACE,"INITIAL MQGET BUFFERSIZE SET TO %d BYTES",RecBuffLen);
  }

/* CONNECT TO QMANAGER */
    if(strlen(cgQMgrReceive) == 0)
    {
	dbg(TRACE,"%05d: Receive-QManager not configured",__LINE__);
    }
    else
    {
	while(bgConnected == FALSE)
	{
		ilRC = ConnectMQ(cgQMgrReceive, &HconRecBrowse);
		ilRC = ConnectMQ(cgQMgrReceive, &HconRecGet);
	        if (bgConnected == FALSE)
                {
		  sleep(atoi(cgReconnectMq));
                }
	}
    
    
/* OPEN QUEUE FOR BROWSING */
	OpenBrowseOptions = MQOO_INPUT_AS_Q_DEF    /* open queue for input      */
	    + MQOO_BROWSE            /* open for browsing */
	    + MQOO_FAIL_IF_QUIESCING;              /* but not if MQM stopping   */

	strcpy(QueueBrowseOd.ObjectName,cgMqReceive); 	     
    
	QueueBrowseReason = 1111; /* != MQRC_NONE */
	QueueBrowseOpenCode = MQCC_FAILED;
    
	while ((QueueBrowseReason != MQRC_NONE) || (QueueBrowseOpenCode == MQCC_FAILED))
	{
	    MQOPEN(HconRecBrowse,                      /* connection handle            */
		   &QueueBrowseOd,                /* object descriptor for queue  */
		   OpenBrowseOptions,                 /* open options                 */
		   &HobjRecBrowse,                     /* object handle                */
		   &QueueBrowseOpenCode,                 /* completion code              */
		   &QueueBrowseReason);                  /* reason code                  */

	    if (QueueBrowseReason != MQRC_NONE)
	    {
		dbg(TRACE,"%05d: MQOPEN ended with reason code <%d>",__LINE__, QueueBrowseReason);
		if(strcmp(cgUseAlerter,"YES") == 0)
		{
			sprintf(clAlert,"<%s>: MQOPEN ended with reason code <%d> while connecting to Queue <%s>",cgContextAlert,QueueBrowseReason,cgMqReceive);
			ilRC = AddAlert2(cgNameAlert," "," ","E",clAlert," ",TRUE,FALSE,"ALERT",mod_name," "," ");
		}
		else
		{
			dbg(TRACE,"%05d: !!!!!!!!!! ERROR WHILE opening queue !!!!!!!!!!!!!!!",__LINE__);
		}


		ilRC = RC_FAIL;
	    }

	    if (QueueBrowseOpenCode == MQCC_FAILED)
	    {
		dbg(TRACE,"%05d: unable to open receive-queue",__LINE__);
		ilRC = RC_FAIL;
	    }

	    if (ilRC == RC_FAIL)
	    {
		dbg(TRACE,"%05d: waiting <%s> seconds before try to reconnect",__LINE__,cgReconnectMq);
		sleep(atoi(cgReconnectMq));
	    }
	    else
	    {
		dbg(TRACE,"%05d: Queue <%s> opened for browsing",__LINE__,cgMqReceive);
	    }
	} /* end of while (QueueBrowseReason.......*/

/* OPEN QUEUE FOR Receiving with GET (and delete item on queue) */
	OpenOptions = MQOO_INPUT_AS_Q_DEF    /* open queue for input      */
	    +  MQOO_FAIL_IF_QUIESCING;              /* but not if MQM stopping   */

	strcpy(QueueOd.ObjectName,cgMqReceive); 	     
    
	QueueReason = 1111; /* != MQRC_NONE */
	QueueOpenCode = MQCC_FAILED;
    
	while ((QueueReason != MQRC_NONE) || (QueueOpenCode == MQCC_FAILED))
	{
	    MQOPEN(HconRecGet,                      /* connection handle            */
		   &QueueOd,                /* object descriptor for queue  */
		   OpenOptions,                 /* open options                 */
		   &HobjRecGet,                     /* object handle                */
		   &QueueOpenCode,                 /* completion code              */
		   &QueueReason);                  /* reason code                  */

	    if (QueueReason != MQRC_NONE)
	    {
		dbg(TRACE,"%05d: MQOPEN ended with reason code <%d>",__LINE__, QueueReason);

		if(strcmp(cgUseAlerter,"YES") == 0 )
		{
			sprintf(clAlert,"<%s>: MQOPEN ended with reason code <%d> while connecting to Queue <%s>",cgContextAlert,QueueReason,cgMqReceive);
			ilRC = AddAlert2(cgNameAlert," "," ","E",clAlert," ",TRUE,FALSE,"ALERT",mod_name," "," ");
		}
		else
		{
			dbg(TRACE,"%05d: !!!!!!!!!! ERROR WHILE opening queue !!!!!!!!!!!!!!!",__LINE__);
		}


		ilRC = RC_FAIL;
	    }

	    if (QueueOpenCode == MQCC_FAILED)
	    {
		dbg(TRACE,"%05d: unable to open receive-queue",__LINE__);
		ilRC = RC_FAIL;
	    }

	    if (ilRC == RC_FAIL)
	    {
		dbg(TRACE,"%05d: waiting <%s> seconds before try to reconnect",__LINE__,cgReconnectMq);
		sleep(atoi(cgReconnectMq));
	    }
	    else
	    {
		dbg(TRACE,"%05d: Queue <%s> opened for receive and delete item",__LINE__,cgMqReceive);
	    }
	} /* end of while (QueueReason.......*/


/* NOW LOOP FOR RECEIVING AND SENDING THE DATA FROM THE MQ-QUEUE */


	for(;;)
	{
/* NOW GET FIRST ITEM ON QUEUE WITH BROWSE, SO THE ITEM WILL NOT BE DELETED FROM THE QUEUE */

	    dbg(TRACE,"%05d: =================Wait for next message on Queue <%s>==============",__LINE__,cgMqReceive);
	    QueueBrowseGmo.Options = MQGMO_WAIT
		+ MQGMO_FAIL_IF_QUIESCING
		+ MQGMO_BROWSE_FIRST
		+ MQGMO_ACCEPT_TRUNCATED_MSG;
	    /*QueueBrowseGmo.WaitInterval = MQWI_UNLIMITED;*/       /* no limit for waiting     */

	    QueueBrowseGmo.WaitInterval = atol(cgWaitInterval);
	    dbg(TRACE,"%05d: MQGET-WaitInterval = <%d>",__LINE__, atol(cgWaitInterval));
	    if (atol(cgWaitInterval) == 0)
	    {
		dbg(TRACE,"%05d: MQGET-WaitInterval = 0 ----> set WaitInterval to 60000");
		QueueBrowseGmo.WaitInterval = 60000;
	    }
		if(atol(cgWaitInterval) == -1)
		{
			QueueBrowseGmo.WaitInterval = MQWI_UNLIMITED;
			dbg(TRACE,"%05d: MQGET-WaitInterval = MQWI_UNLIMITED",__LINE__);
		}
                if (ipForWhat == 0)
                {
		  QueueBrowseGmo.WaitInterval = 1000;
                  dbg(TRACE,"MQ TEST: WAIT INTERVAL SET TO %d MILLI SEC",QueueBrowseGmo.WaitInterval);
                }
                if (ipForWhat == 1)
                {
		  QueueBrowseGmo.WaitInterval = 10000;
                  dbg(TRACE,"MQ TEST: WAIT INTERVAL SET TO %d MILLI SEC",QueueBrowseGmo.WaitInterval);
                }

	    memcpy(ReceiveBrowseMd.MsgId, MQMI_NONE, sizeof(ReceiveBrowseMd.MsgId));
	    memcpy(ReceiveBrowseMd.CorrelId, MQCI_NONE, sizeof(ReceiveBrowseMd.CorrelId));
    
	do
	{
            ilNoMsgCount = 0;
	    do 
	    {
		MessLen = 0;
		QueueBrowseCompCode = 0;
		QueueBrowseReason = 0;
		MQGET(HconRecBrowse,                /* connection handle                 */
		      HobjRecBrowse,                /* object handle                     */
		      &ReceiveBrowseMd,                 /* message descriptor                */
		      &QueueBrowseGmo,                /* get message options               */
		      RecBuffLen,              /* buffer length                     */
		      RecBuffer,              /* message buffer                    */
		      &MessLen,            /* message length                    */
		      &QueueBrowseCompCode,           /* completion code                   */
		      &QueueBrowseReason);            /* reason code                       */

		dbg(TRACE,"MQGET BROWSE STATUS: BUFFLEN=%d MESSLEN=%d COMPCODE=%d REASON=%d",
                                           RecBuffLen,MessLen,QueueBrowseCompCode,QueueBrowseReason);
		if(QueueBrowseReason == 2009)
		{
		    dbg(TRACE,"%05d: MQGET ended with Reasoncode: <%d>",__LINE__,QueueBrowseReason);
		    dbg(TRACE,"MQ CONNECTION BROKEN, SO TERMINATE ...");
		    Terminate(1);
		}

		if(QueueBrowseReason == 2033)
		{
                        /* ilNoMsgCount++; */
			dbg(TRACE,"WAITED %d MILLISECONDS WITH NO MESSAGES ON THE QUEUE",QueueBrowseGmo.WaitInterval);
			dbg(TRACE,"POLL TCP/IP BRIDGE AND QUEUE CHECK FROM IPCCON");
                        if (ilNoMsgCount < 10)
                        {
			  ilRC = PutTcpMsg("CHKC", "QUE_CHK", "", "");
                        }
                        else
                        {
			  ilRC = PutTcpMsg("EXIT", "RESTART", "", "");
                        }
			if (ilRC != RC_SUCCESS)
			{
				dbg(TRACE,"PutTcpMsg failed###################");
				Terminate(1);
			}
    			ilRC = GetTcpMsg(&pclCmd, &pclSel, &pclFld, &pclDat);
			if (ilRC != RC_SUCCESS)
			{
				dbg(TRACE,"GetTcpMsg failed###################");
				Terminate(1);
			}
			else
			{
				if (strcmp(pclCmd,"EXIT") == 0)
				{
					Terminate(1);
				}
			}
			dbg(TRACE,"------ WAITING ON QUEUE <%s> ------",cgMqReceive);
		}

	    } while ((QueueBrowseReason == 2033) && (ipForWhat >= 2)); /*2033 = no message and timeout*/

	    if (QueueBrowseReason == MQRC_NONE)
	    {
	        strcpy(clMyTime,ReceiveBrowseMd.PutDate);
	        clMyTime[16] = '\0';
		RecBuffer[MessLen] = '\0';
		{
			/* Replace '\0' in the message by ' ' because the message will be handled
		   	as string in further processing */
			 int ilLc;

			 for(ilLc = 0; ilLc < MessLen; ilLc++)
			 {
			 	if (RecBuffer[ilLc] == '\0')
				{
					RecBuffer[ilLc] = ' ';
				}
			}
		}
		dbg(TRACE,"MQGET (BROWSE) received Message:\n%s\n(MQTime: <%s> MQLen: %d)",RecBuffer,clMyTime,MessLen);
	    }
	    else
	    {
		if(QueueBrowseReason != 2033)
		{
			RecBuffer[RecBuffLen] = '\0';
			dbg(TRACE,"MQGET ended with Reasoncode: <%d>",QueueBrowseReason);
			dbg(TRACE,"MQGET received Message: <%s> (Length=%d",RecBuffer,MessLen);

			if(QueueBrowseReason != 2079)
			{
			    dbg(TRACE,"%05d: !!!!!!!!!! ERROR WHILE RECEIVING MESSAGE !!!!!!!!!!!!!!!",__LINE__);
			    if(QueueBrowseReason == 2016)
			    {
				dbg(TRACE,"%05d: GetInhibited, waiting 120 seconds for retry",__LINE__);
				sleep(120);
			    }
			    else
			    {
				Terminate(1);
			    }
			}
			else
			{
				dbg(TRACE,"MQ MESSAGE TRUNCATED (MSG LEN=%d) (BUFFER LEN=%d)",MessLen,RecBuffLen);
                                RecBuffLen = MessLen + 16;
				RecBuffer = (MQBYTE*)realloc((MQBYTE*)RecBuffer, RecBuffLen);
				dbg(TRACE,"RECEIVE BUFFER LENGTH EXTENDED TO %d BYTES. TRYING AGAIN ...",RecBuffLen);
			}
		}
	    }
	}
	while ((QueueBrowseReason != MQRC_NONE) && (ipForWhat >= 2));

	if(QueueBrowseReason != 2033)
	{

            /* SEND DATA TO CEDA */
	    if ((QueueBrowseReason != 2016) && (ipForWhat >= 4))
	    {
			ConvertClientStringToDb(RecBuffer);
                        igCurMsgSeqNo++;
			if (igCurMsgSeqNo > igMaxMsgSeqNo)
			{
				dbg(TRACE,"SEQ_NO (%d) EXCEEDS MAX (%d) SWAPPED TO 1",igCurMsgSeqNo,igMaxMsgSeqNo);
				igCurMsgSeqNo = 1;
			}
                        sprintf(cgCurTgrSeqn,"%d",igCurMsgSeqNo);
			GetServerTimeStamp("LOC",1,0,cgCurTimeStamp);
                        if (strcmp(cgCurTimeStamp,cgPrvTimeStamp) != 0)
                        {
                          igCurTistCnt = 0;
                        }
                        else
                        {
                          igCurTistCnt++;
                        }
                        strcpy(cgPrvTimeStamp,cgCurTimeStamp);
			ilMsgFldTistBgn = igCurTistCnt;

			if (MessLen <= igMsgFldSize)
                        {
			  ilMsgFldTtlPack = 1;
                          sprintf(cgCurMsgMkey,"%s%0.3d",cgCurTimeStamp,igCurTistCnt);
			  strcpy(clSqlBuf,"INSERT INTO TGRTAB (MKEY,SEQN,STAT,DATA,OUTT)"
                                          " VALUES "
                                          "(:VMKEY,:VSEQN,:VSTAT,:VDATA,:VOUTT)");
                          sprintf(pcgGlobalDataBuff,"%s,%d,SEND_PEND,%s, ",cgCurMsgMkey,igCurMsgSeqNo,RecBuffer);
			  slFkt = IGNORE;
			  slCursor = 0;
                          delton(pcgGlobalDataBuff);
			  ilRC = sql_if (slFkt, &slCursor, clSqlBuf, pcgGlobalDataBuff);
			  commit_work();
			  close_my_cursor (&slCursor);
			}
			else
			{
			  ilMsgFldCurPack = 1;
			  ilMsgFldLen = igMsgFldSize - 1;
			  ilMsgFldTtlPack = ((MessLen - 1) / ilMsgFldLen) + 1;
                          dbg(TRACE,"MSG PACKAGES IN TGRTAB = %d",ilMsgFldTtlPack);
			  strcpy(clSqlBuf,"INSERT INTO TGRTAB (MKEY,SEQN,STAT,DATA,OUTT)"
                                          " VALUES "
                                          "(:VMKEY,:VSEQN,:VSTAT,:VDATA,:VOUTT)");
			  ilMsgFldPos = 0;
			  while (ilMsgFldCurPack <= ilMsgFldTtlPack)
			  {
                            sprintf(cgCurMsgMkey,"%s%0.3d",cgCurTimeStamp,igCurTistCnt);
			    strncpy(clMsgFldData,(char *)&(RecBuffer[ilMsgFldPos]),ilMsgFldLen);
			    clMsgFldData[ilMsgFldLen] = '$';
			    clMsgFldData[ilMsgFldLen+1] = '\0';
                            sprintf(pcgGlobalDataBuff,"%s,%d,SP_%d/%d,%s, ",
                                    cgCurMsgMkey,igCurMsgSeqNo,ilMsgFldCurPack,ilMsgFldTtlPack,clMsgFldData);
			    slFkt = IGNORE;
			    slCursor = 0;
                            delton(pcgGlobalDataBuff);
			    ilRC = sql_if (slFkt, &slCursor, clSqlBuf, pcgGlobalDataBuff);
			    commit_work();
			    close_my_cursor (&slCursor);
			    ilMsgFldCurPack++;
                            igCurTistCnt++;
			    ilMsgFldPos += ilMsgFldLen;
			    if (ilMsgFldCurPack >= ilMsgFldTtlPack)
			    {
			      ilMsgFldLen = MessLen - ilMsgFldPos;
			    }
			  }
                          igCurTistCnt--;
			}
			
			if (ilRC == DB_SUCCESS)
			{
			    dbg(TRACE,"MSG (%d) INSERTED INTO TGRTAB WITH %d PACKETS OF MAX %d BYTES",
					igCurMsgSeqNo,ilMsgFldTtlPack,igMsgFldSize);
                            strcpy(clMsgOutCmd,"MSGI");
			}
			if (ilRC != DB_SUCCESS)
			{
			    dbg(TRACE,"==============================");
			    dbg(TRACE,"ERROR PROCESSING SEQ_NO %d",igCurMsgSeqNo);
			    dbg(TRACE,"INSERT MSG INTO TGRTAB FAILED.");
			    dbg(TRACE,"==============================");
                            if (igExitOnError == TRUE)
                            {
			      dbg(TRACE,"=>TERMINATING DUE TO DB ERROR=");
			      dbg(TRACE,"==============================");
			      Terminate(1);
                            }
                            else
                            {
			      dbg(TRACE,">IGNORING DB ERROR AND PROCEED");
			      dbg(TRACE,"==============================");
                              strcpy(clMsgOutCmd,"MSGF");
			      ilRC = DB_SUCCESS;
                            }
			}

			if (ilRC == DB_SUCCESS)
			{
			    ConvertDbStringToClient(RecBuffer);
			    sprintf(clMsgOutSel,"%s,%d,%d",cgCurTimeStamp,ilMsgFldTistBgn,igCurTistCnt);
			    ilRC = PutTcpMsg(clMsgOutCmd, clMsgOutSel, cgCurTgrSeqn, (char *)RecBuffer);
			    if (ilRC != RC_SUCCESS)
			    {
			    	    dbg(TRACE,"PutTcpMsg failed###################");
				    Terminate(1);
			    }
			    dbg(TRACE,"WAIT FOR MSG_ACK FROM IPCCON");
    			    ilRC = GetTcpMsg(&pclCmd, &pclSel, &pclFld, &pclDat);
			    if (ilRC != RC_SUCCESS)
			    {
				    dbg(TRACE,"GetTcpMsg failed###################");
				    Terminate(1);
			    }
			    else
			    {
				    if (strcmp(pclCmd,"EXIT") == 0)
				    {
					Terminate(1);
				    }
			    }
			}
	    }
    }

/* NOW GET FIRST ITEM ON QUEUE AND DELETE IT FROM THE QUEUE */
    
	    QueueGmo.Options = MQGMO_NO_WAIT
		+ MQGMO_FAIL_IF_QUIESCING
		+ MQGMO_ACCEPT_TRUNCATED_MSG;
    
	    memcpy(ReceiveMd.MsgId, MQMI_NONE, sizeof(ReceiveMd.MsgId));
	    memcpy(ReceiveMd.CorrelId, MQCI_NONE, sizeof(ReceiveMd.CorrelId));
		do
		{
	    MQGET(HconRecGet,                /* connection handle                 */
		  HobjRecGet,                /* object handle                     */
		  &ReceiveMd,                 /* message descriptor                */
		  &QueueGmo,                /* get message options               */
		  RecBuffLen,              /* buffer length                     */
		  RecBuffer,              /* message buffer                    */
		  &MessLen,            /* message length                    */
		  &QueueCompCode,           /* completion code                   */
		  &QueueReason);            /* reason code                       */

		if(QueueReason == 2009)
		{
			Terminate(1);
		}

	    if (QueueReason == MQRC_NONE)
	    {
		RecBuffer[MessLen] = '\0';
		dbg(DEBUG,"%05d: MQGET received and deleted Message: <%s>",__LINE__,RecBuffer);
		dbg(TRACE,"%05d: MQGET received and deleted Message from Queue",__LINE__);
		llMsgCnt++;
		if ((ipForWhat == 2) && (llMsgCnt >= 10))
                {
			dbg(TRACE,"TEST END: 10 MESSAGES RECEIVED. NOW TERMINATING ...");
			Terminate(1);
                }
	    }
	    else
	    {

		if(QueueReason != 2033)	
		{
		dbg(TRACE,"%05d: MQGET ended with Reasoncode: <%d>",__LINE__,QueueReason);
		dbg(TRACE,"%05d: MQGET received and deleted Message from MQ: <%s>",__LINE__,RecBuffer);
		if(strcmp(cgUseAlerter,"YES") == 0 )
		{
			sprintf(clAlert,"<%s>: MQGET ended with Reasoncode: <%d> ",cgContextAlert,QueueReason);
			ilRC = AddAlert2(cgNameAlert," "," ","E",clAlert," ",TRUE,FALSE,"ALERT",mod_name," "," ");
		}
		else
		{
			dbg(TRACE,"%05d: !!!!!!!!!! ERROR WHILE RECEIVING MESSAGE !!!!!!!!!!!!!!!",__LINE__);
		}

		if(QueueReason != 2079)
		{
		    if(QueueReason == 2016)
		    {
			dbg(TRACE,"%05d: GetInhibited, waiting 120 seconds for retry",__LINE__);
			sleep(120);
		    }
		    else
		    {
				if(QueueReason != 2033)	
				{
					Terminate(1);
				}
		    }
		}
		}
	    }
		}
		while ((QueueBrowseReason != MQRC_NONE) && (ipForWhat >= 2));

	if (ipForWhat < 2)
        {
	  break;
        }
	} /* end of for(;;) */

    } /* end of if(strlen(Receive-Qmanager.....)) */

 return ilRC;

}


static int TimeToStr(char *pcpTime,time_t lpTime)
{
    struct tm *_tm;
    char   _tmpc[6];

    /*_tm = (struct tm *)localtime(&lpTime); */
    _tm = (struct tm *)gmtime(&lpTime);
    /*      strftime(pcpTime,15,"%" "Y%" "m%" "d%" "H%" "M%" "S",_tm); */


    sprintf(pcpTime,"%4d%02d%02d%02d%02d%02d",
            _tm->tm_year+1900,_tm->tm_mon+1,_tm->tm_mday,_tm->tm_hour,
            _tm->tm_min,_tm->tm_sec);

    return RC_SUCCESS;

}     /* end of TimeToStr */


static int CheckIfMessageInWork()
{
    int ilRC = RC_SUCCESS;
    int ilRC2 = RC_FAIL;
    short slFkt = 0;
    short slCursor = 0;
    char clData[4500] = "\0";
    char clUrno[12] = "\0";
    char clRecUrno[12] = "\0";
    char clReas[8] = "\0";
    char clTput[18] = "\0";
    int ilCount = 1;
    int ilAckRC = RC_FAIL;
    char clNowTime[20] = "\0";
    char clSqlBuf[4500] ="\0";
    int ilLen = 0;


    dbg(TRACE,"-----CheckIfMessageInWork start -----");
    while (ilRC == RC_SUCCESS)
    {
	slFkt = START;
	slCursor = 0;

	sprintf(clSqlBuf,"SELECT URNO,DATA,REAS,TPUT FROM MQRTAB WHERE STAT ='RECEIVED' and INAM='%s'",mod_name);

	ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clData);
	
	commit_work();
	close_my_cursor (&slCursor);
	
	if(ilRC == RC_SUCCESS)
	{
	    BuildItemBuffer (clData, "", 4, ",");
	    strcpy(clSqlBuf,clData); /* clSqlBuf wird hier nur temporaer benutzt, hat nichts mit sql zu tun */
	    
	    ilLen = get_real_item (clUrno, clSqlBuf, 1);
	    ilLen = get_real_item (clData, clSqlBuf, -2);
	    ilLen = get_real_item (clReas, clSqlBuf, 3);
	    ilLen = get_real_item (clTput, clSqlBuf, 4);
	    ConvertDbStringToClient(clData);
	    dbg(TRACE,"%05d: Search for IN WORK return ilRC: <%d>, clUrno <%s>, clData: <%s>, clReas: <%s>, clTput: <%s>",__LINE__,ilRC,clUrno,clData,clReas,clTput);
		/*
	    ilRC2 = SendToCeda(clData,clTput,atol(clReas),clUrno);
		*/

	}
    }


    dbg(TRACE,"-----CheckIfMessageInWork end -----");
	
    return ilRC;
}

static int OpenSendMQ(int ipQueueNumber)
{
    int ilRC = RC_SUCCESS;
    MQOD od = {MQOD_DEFAULT};
    MQLONG   O_options;              
    MQLONG   QueueOpenCode;               /* completion code                 */
    MQLONG   QueueReason;                 /* reason code                     */
    static char clAlert[256] = "\0";

		if(bgConnected == FALSE)
		{
			ilRC = ConnectMQ(cgQMgrSend, &HconSend);
		}
    strcpy(od.ObjectName, rgMyQueues[ipQueueNumber].cgQName);
    
    O_options = MQOO_OUTPUT         /* open queue for output          */
	+ MQOO_FAIL_IF_QUIESCING; /* but not if MQM stopping    */
    QueueReason = 1111; /* != MQRC_NONE */
    QueueOpenCode = MQCC_FAILED;
    

	MQOPEN (HconSend, &od, O_options, &rgMyQueues[ipQueueNumber].HobjSend, &QueueOpenCode, &QueueReason);

	if (QueueReason != MQRC_NONE)
	{
	    dbg(TRACE,"%05d: MQOPEN ended with reason code <%d>",__LINE__, QueueReason);
			if (QueueReason ==  MQRC_CONNECTION_BROKEN)
			{
				  dbg(TRACE,"Connection broken, try to reconnect at next message");
			  	bgConnected = FALSE;
			}
		if(strcmp(cgUseAlerter,"YES") == 0 )
		{
			sprintf(clAlert,"<%s>: MQOPEN ended with reason code <%d> while connecting to Queue <%s>",cgContextAlert,QueueReason,rgMyQueues[ipQueueNumber].cgQName);
			ilRC = AddAlert2(cgNameAlert," "," ","E",clAlert," ",TRUE,FALSE,"ALERT",mod_name," "," ");
		}
		else
		{
			dbg(TRACE,"%05d: !!!!!!!!!! ERROR WHILE Open Queue <%s> !!!!!!!!!!!!!!!",__LINE__,rgMyQueues[ipQueueNumber].cgQName);
		}


	    ilRC = RC_FAIL;
	}

	if (QueueOpenCode == MQCC_FAILED)
	{
	    dbg(TRACE,"%05d: unable to open send-queue <%s>",__LINE__,rgMyQueues[ipQueueNumber].cgQName);
	    ilRC = RC_FAIL;
	}

	if (ilRC == RC_FAIL)
	{
	    /*dbg(TRACE,"%05d: waiting <%s> seconds before try to reconnect",__LINE__,cgReconnectMq);*/
	    /*sleep(atoi(cgReconnectMq));*/
	}
	else
	{
	    dbg(TRACE,"%05d: Queue <%s> opened for sending to MQ",__LINE__,rgMyQueues[ipQueueNumber].cgQName);
	}

    return ilRC;
}

static void myinitialize(int argc, char *argv[])
{
    int ili = 0;
    char pclTmpData[512];
    mod_name = argv[0];
    /* 20020822 JIM: if argv[0] contains path (i.e. started by ddd), skip path */
    if (strrchr(mod_name,'/')!= NULL)
    {
	mod_name= strrchr(mod_name,'/');
	mod_name++;
    }

    sprintf(__CedaLogFile,"%s/%s%.5ld.log",getenv("DBG_PATH"),mod_name,getpid());
    outp = fopen(__CedaLogFile,"w");

    mod_id = 9999;
    ctrl_sta = 1;

    glbrc = init();
    if(glbrc)
    {
	fprintf(outp,"\n<%s><%ld>STHINIT failed: ret = %d\n",mod_name,getpid(),glbrc);
	exit(1);
    }

    return;
}

static int GetFileContent(void)
{
  int ilRC = RC_SUCCESS;
  long llSize = 0;
  FILE 	*fp=NULL;
  struct stat rlStat;
  sprintf(pcgGlobalDataBuff,"Default Message: File \"%s\" not found.",cgActSendFile);
  fp = fopen(cgActSendFile, "r");
  if (fp != NULL)
  {
    ilRC = fstat(fileno(fp), &rlStat);
    llSize = rlStat.st_size;
    if (llSize < (1024*1024))
    {
      fread((void*)pcgGlobalDataBuff, (size_t)1, (size_t)(llSize), fp);
    }
    fclose(fp);
    fp = NULL;
  }
  else
  {
    dbg(TRACE,"%s",pcgGlobalDataBuff);
    ilRC = RC_FAIL;
  }
  return ilRC;
}

/* ************************************************* */
static int SendMsgToMq(char *pcpData, int ipQueueNumber, char *pcpTgsMsgKey)
{
    int ilRC = RC_SUCCESS;
    char clMyTime[30] = "\0";
    char clNowTime[30] = "\0";
    short slFkt = 0;
    short slCursor = 0;
    char clUrno[20] = "\0";
    char clSqlBuf[4500] = "\0";
    char clData[4500] = "\0";
    char clAlert[256] = "\0";


    MQMD     md = {MQMD_DEFAULT};    /* Message Descriptor              */
    MQLONG   CompCode;               /* completion code                 */
    MQLONG   Reason;                 /* reason code                     */
    MQPMO    pmo = {MQPMO_DEFAULT};   /* put message options             */

    dbg(TRACE,"-----SendMsgToMq start -----");

/* INSERT MESSAGE INTO MQSTAB */
	if(strcmp(cgDbLogging,"ON") == 0)
	{
		ilRC = GetNextValues(clUrno,1);


		if(rgMyQueues[ipQueueNumber].igPutFailed == TRUE)
		{
			if((strcmp(cgDbLogging,"ON") == 0) && (strcmp(cgRecovery,"ON") == 0))
			{
				ilRC = SendFailedMessages(ipQueueNumber);
			}
		}


		TimeToStr(clNowTime,time(NULL));
		ConvertClientStringToDb(pcpData);

		if(strcmp(cgRecovery,"ON") == 0)
		{
			if(igUseQName == FALSE)
			{
				sprintf(clSqlBuf,"INSERT INTO MQSTAB (URNO,INAM,DATA,STAT,LSTU) VALUES (%d,'%s','%s','%s','%s')",
						atol(clUrno),mod_name,pcpData,"TO SEND",clNowTime);
			}
			else
			{
				sprintf(clSqlBuf,"INSERT INTO MQSTAB (URNO,INAM,DATA,STAT,LSTU) VALUES (%d,'%s','%s','%s','%s')",
						atol(clUrno),rgMyQueues[ipQueueNumber].cgQName,pcpData,"TO SEND",clNowTime);
			}
		}
		else
		{
			if(igUseQName == FALSE)
			{
				sprintf(clSqlBuf,"INSERT INTO MQSTAB (URNO,INAM,DATA,STAT,LSTU) VALUES (%d,'%s','%s','%s','%s')",
						atol(clUrno),mod_name,pcpData,"FAILED",clNowTime);
			}
			else
			{
				sprintf(clSqlBuf,"INSERT INTO MQSTAB (URNO,INAM,DATA,STAT,LSTU) VALUES (%d,'%s','%s','%s','%s')",
						atol(clUrno),rgMyQueues[ipQueueNumber].cgQName,pcpData,"FAILED",clNowTime);
			}

		}

		ConvertDbStringToClient(pcpData);

		dbg(DEBUG,"%05d: clSqlBuf: <%s>",__LINE__,clSqlBuf);
		slFkt = START;
		slCursor = 0;
		ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clData);
		if (ilRC != RC_SUCCESS)
		{
			dbg(TRACE,"sql_if failed###################");
		}
		commit_work();
		close_my_cursor (&slCursor);

	}

    if (rgMyQueues[ipQueueNumber].igPutFailed == FALSE)
    {
		/* PUT MESSAGE TO MQ QUEUE */

		memcpy(md.Format,           
			MQFMT_STRING, MQ_FORMAT_LENGTH);
		memcpy(md.MsgId, MQMI_NONE, sizeof(md.MsgId));
		if(rgMyQueues[ipQueueNumber].igQDynPrio == -1)
		{
			md.Priority = rgMyQueues[ipQueueNumber].igQPrio;
		}
		else
		{
			md.Priority = rgMyQueues[ipQueueNumber].igQDynPrio;
		}

		pmo.Options = MQPMO_NEW_MSG_ID
			+ MQPMO_FAIL_IF_QUIESCING;

		dbg(DEBUG,"Send to Queue: <%s>\n%s\n(length = %d)",rgMyQueues[ipQueueNumber].cgQName,pcpData,strlen(pcpData));
		MQPUT(HconSend,                /* connection handle               */
			rgMyQueues[ipQueueNumber].HobjSend,          /* object handle                   */
			&md,                 /* message descriptor              */
			&pmo,                /* default options                 */
			strlen(pcpData),              /* buffer length                   */
			pcpData,              /* message buffer                  */
			&CompCode,           /* completion code                 */
			&Reason);            /* reason code                     */

		strcpy(clMyTime,md.PutDate);
		clMyTime[16] = '\0';
		/* report reason, if any */
		dbg(TRACE,"MQPUT ended with reason code <%d>, MQTime: <%s>", Reason, clMyTime);

		if(Reason == 2009)
		{
			Terminate(1);
		}
		if (Reason == MQRC_NONE)
		{
		    if (pcpTgsMsgKey[0] != '\0')
                    {
			GetServerTimeStamp("LOC",1,0,clMyTime);
			sprintf(clSqlBuf,"UPDATE TGSTAB SET STAT='SEND_OK',OUTT='%s' WHERE MKEY='%s'",clMyTime,pcpTgsMsgKey);
			dbg(TRACE,"<%s>",clSqlBuf);
			slFkt = START;
			slCursor = 0;
			ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clData);
			if (ilRC != RC_SUCCESS)
			{
			  dbg(TRACE,"sql_if failed###################");
			}
			commit_work();
			close_my_cursor (&slCursor);
                    }
		}
		else
		{
		if(strcmp(rgMyQueues[ipQueueNumber].cgUseAlerter,"YES") == 0 )
		{
			sprintf(clAlert,"<%s>: MQPUT ended with reason code <%d> ",rgMyQueues[ipQueueNumber].cgContextAlert,Reason);
			ilRC = AddAlert2(rgMyQueues[ipQueueNumber].cgNameAlert," "," ","E",clAlert,pcpData,TRUE,FALSE,"ALERT",rgMyQueues[ipQueueNumber].cgQName," "," ");
		}
		else
		{
			dbg(TRACE,"%05d: !!!!!!!!!! ERROR WHILE SENDING MESSAGE !!!!!!!!!!!!!!!",__LINE__);
		}

			
			if(strcmp(cgDbLogging,"ON") == 0)
			{

				if (strcmp(cgRecovery,"ON") == 0)
				{
					ilRC = MarkToSend(clUrno,ipQueueNumber);
					rgMyQueues[ipQueueNumber].igPutFailed = TRUE;
					rgMyQueues[ipQueueNumber].bgQConnected = FALSE;
					if(rgMyQueues[ipQueueNumber].igPutFailedOld == FALSE)
					{
						strcpy(rgMyQueues[ipQueueNumber].cgFirstFailedUrno,clUrno);
						strcpy(rgMyQueues[ipQueueNumber].cgFirstFailedData,pcpData);
					}
				}
			}
		}
	}/*geh�rt zu (rgMyQueues[ipQueueNumber].igPutFailed == FALSE)*/
	else
	{
		if (strcmp(cgRecovery,"ON") == 0)
		{
			MarkToSend(clUrno,ipQueueNumber);
		}
	}


    dbg(TRACE,"-----SendMsgToMq end -----");

    return ilRC;
}




static int SendFailedMessages(int ipQueueNumber)
{
    int ilRC = RC_SUCCESS;
    int ilRC2 = RC_SUCCESS;
    short slFkt = 0;
    short slCursor = 0;
    short slFkt2 = 0;
    short slCursor2 = 0;
    char clSqlBuf[4500] = "\0";
    char clData[4500] = "\0";
    int ilLen = 0;
    char clUrno[20] ="\0";
    char clMyTime[30] ="\0";
    char clNowTime[20] ="\0";
    char clAlert[256] = "\0";


    MQMD     md = {MQMD_DEFAULT};    /* Message Descriptor              */
    MQLONG   CompCode;               /* completion code                 */
    MQLONG   Reason;                 /* reason code                     */
    MQPMO    pmo = {MQPMO_DEFAULT};  /* put message options             */

    dbg(TRACE,"-----SendFailedMessages start -----");

    slFkt = START;
    slCursor = 0;

	ilRC = OpenSendMQ(ipQueueNumber);

    while (ilRC == RC_SUCCESS)
    {
		if(strlen(rgMyQueues[ipQueueNumber].cgFirstFailedUrno) == 0)
		{
			if(igUseQName == FALSE)
			{
				sprintf(clSqlBuf,"SELECT URNO, DATA FROM MQSTAB WHERE STAT = 'TO SEND' AND INAM ='%s' ORDER BY COUN",mod_name);
			}
			else
			{
				sprintf(clSqlBuf,"SELECT URNO, DATA FROM MQSTAB WHERE STAT = 'TO SEND' AND INAM ='%s' ORDER BY COUN",rgMyQueues[ipQueueNumber].cgQName);
			}
			ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clData);
			if (ilRC != RC_SUCCESS)
			{
				dbg(TRACE,"%05d: sql_if returnd ilRC: <%d> probably no Data TO SEND found",__LINE__,ilRC);
				lgCount = 0;
			}
			slFkt = NEXT;
		
			BuildItemBuffer (clData, "", 2, ",");
			strcpy(clSqlBuf,clData); /* clSqlBuf wird hier nur temporaer benutzt, hat nichts mit sql zu tun */
		    
			ilLen = get_real_item (clUrno, clSqlBuf, 1);
			ilLen = get_real_item (clData, clSqlBuf, -2);
		}
		else
		{
			strcpy(clUrno,rgMyQueues[ipQueueNumber].cgFirstFailedUrno);
			strcpy(clData,rgMyQueues[ipQueueNumber].cgFirstFailedData);
		}
		if (ilRC == RC_SUCCESS)
		{
	    
		/* PUT MESSAGE TO MQ QUEUE */
		memcpy(md.Format, MQFMT_STRING, MQ_FORMAT_LENGTH);
	    memcpy(md.MsgId, MQMI_NONE, sizeof(md.MsgId));
		
		if(rgMyQueues[ipQueueNumber].igQDynPrio == -1)
		{
			md.Priority = rgMyQueues[ipQueueNumber].igQPrio;
		}
		else
		{
			md.Priority = rgMyQueues[ipQueueNumber].igQDynPrio;
		}

	    pmo.Options = MQPMO_NEW_MSG_ID
		+ MQPMO_FAIL_IF_QUIESCING;
	    ConvertDbStringToClient(clData);
	    dbg(TRACE,"%05d: Send to Queue: <%s> clData <%s>, length <%d>",__LINE__,rgMyQueues[ipQueueNumber].cgQName,clData,strlen(clData));
	    MQPUT(HconSend,                /* connection handle               */
		  rgMyQueues[ipQueueNumber].HobjSend,          /* object handle                   */
		  &md,                 /* message descriptor              */
		  &pmo,                /* default options                 */
		  strlen(clData),              /* buffer length                   */
		  clData,              /* message buffer                  */
		  &CompCode,           /* completion code                 */
		  &Reason);            /* reason code                     */

	    strcpy(clMyTime,md.PutDate);
	    clMyTime[16] = '\0';
	    /* report reason, if any */
	    dbg(TRACE,"%05d: MQPUT ended with reason code <%d>, Time: <%s>",__LINE__, Reason, clMyTime);

		if(Reason == 2009)
		{
			Terminate(1);
		}

	    if (Reason == MQRC_NONE)
	    {    
			TimeToStr(clNowTime,time(NULL));
			sprintf(clSqlBuf,"UPDATE MQSTAB SET COUN = 0, STAT='OK', LSTU='%s',TPUT = '%s' WHERE URNO=%s",clNowTime,clMyTime,clUrno);
			slFkt2 = START;
			slCursor2 = 0;
			ilRC2 = sql_if (slFkt2, &slCursor2, clSqlBuf, clData);
			if (ilRC2 != RC_SUCCESS)
			{
				dbg(TRACE,"%05d: clSqlBuf: <%s>",__LINE__,clSqlBuf);
				dbg(TRACE,"%05d: sql_if returnd ilRC: <%d>",__LINE__,ilRC2);
			}
			commit_work();
			close_my_cursor (&slCursor2);
			rgMyQueues[ipQueueNumber].igPutFailed = FALSE;
			rgMyQueues[ipQueueNumber].cgFirstFailedUrno[0] = '\0';
			rgMyQueues[ipQueueNumber].cgFirstFailedData[0] = '\0';
	    }
	    else
	    {
			if(strcmp(rgMyQueues[ipQueueNumber].cgUseAlerter,"YES") == 0 )
			{
				sprintf(clAlert,"<%s>: MQPUT ended with reason code <%d> ",rgMyQueues[ipQueueNumber].cgContextAlert,Reason);
				ilRC = AddAlert2(rgMyQueues[ipQueueNumber].cgNameAlert," "," ","E",clAlert,clData,TRUE,FALSE,"ALERT",rgMyQueues[ipQueueNumber].cgQName," "," ");
			}
			else
			{
				dbg(TRACE,"%05d: !!!!!!!!!! ERROR WHILE SENDING MESSAGE !!!!!!!!!!!!!!!",__LINE__);
			}

			ilRC = RC_FAIL;
			rgMyQueues[ipQueueNumber].igPutFailed = TRUE;

			if(rgMyQueues[ipQueueNumber].igPutFailed == FALSE)
			{
				strcpy(rgMyQueues[ipQueueNumber].cgFirstFailedUrno,clUrno);
				strcpy(rgMyQueues[ipQueueNumber].cgFirstFailedData,clData);
			}
	    }
	}
    }/*end of while(ilRC == RC_SUCCESS)*/
    close_my_cursor (&slCursor);

    dbg(TRACE,"-----SendFailedMessages end -----");

    return rgMyQueues[ipQueueNumber].igPutFailed;
} /*end of SendFailedMessages*/


static int MarkToSend(char *pcpUrno, int ipQueueNumber)
{
    int ilRC = RC_SUCCESS;
    char clNowTime[30] = "\0";
    short slFkt = 0;
    short slCursor = 0;
    char clSqlBuf[4500] = "\0";
    char clData[50] = "\0";

    dbg(TRACE,"-----MarkToSend start -----");
	if(lgCount == -1)
	{
		if(igUseQName == FALSE)
		{
			sprintf(clSqlBuf,"SELECT MAX(COUN) FROM MQSTAB WHERE INAM ='%s'",mod_name);
		}
		else
		{
			sprintf(clSqlBuf,"SELECT MAX(COUN) FROM MQSTAB WHERE INAM ='%s'",rgMyQueues[ipQueueNumber].cgQName);
		}
		slFkt = START;
		slCursor = 0;
		ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clData);
		if (ilRC != RC_SUCCESS)
		{
			dbg(TRACE,"sql_if failed###################");
		}
		commit_work();
		close_my_cursor (&slCursor);
    
		lgCount = atol(clData);
	}
	lgCount++;
    TimeToStr(clNowTime,time(NULL));

    sprintf(clSqlBuf,"UPDATE MQSTAB SET COUN = %d, STAT = 'TO SEND', LSTU = '%s' WHERE URNO = %s",lgCount,clNowTime,pcpUrno);

    dbg(TRACE,"%05d: Mark Message clSqlBuf: <%s>",__LINE__,clSqlBuf);

    slFkt = START;
    slCursor = 0;
    ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clData);
    if (ilRC != RC_SUCCESS)
    {
	dbg(TRACE,"sql_if failed###################");
    }
    commit_work();
    close_my_cursor (&slCursor);
    dbg(TRACE,"-----MarkToSend end -----");

    return ilRC;
}


static void CleanDb()
{
    int ilRC = RC_SUCCESS;
    short slFkt = 0;
    short slCursor = 0;
    char clSqlBuf[200] = "\0";
    char clData[50] = "\0";
    char clDelTime[30] = "\0";
    long llCleanMqr = 0;
    long llCleanMqs = 0;
	int ili = 0;

    dbg(TRACE,"-----CleanDb start-----");

/* NOW CLEAN UP MQRTAB*/

    TimeToStr(clDelTime,time(NULL));
    llCleanMqr = atol(cgCleanMqr);
    if (llCleanMqr > 30)
    {
	llCleanMqr = 30;
    }
    llCleanMqr = llCleanMqr*24*60*60*(-1);
    AddSecondsToCEDATime(clDelTime,llCleanMqr,1);
    dbg(TRACE,"Now Delete Records older than <%s> from MQRTAB for INAM = <%s>",clDelTime,mod_name);

    slFkt = START;
    slCursor = 0;
    
    sprintf(clSqlBuf,"DELETE FROM MQRTAB WHERE TPUT < '%s' AND INAM = '%s'",clDelTime,mod_name);
    ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clData);
    if (ilRC != RC_SUCCESS)
    {
	dbg(TRACE,"%05d: sql_if failed or nothing to delete in CleanDb",__LINE__);
    }
    commit_work();
    close_my_cursor (&slCursor);


/* NOW CLEAN UP MQSTAB */  

    TimeToStr(clDelTime,time(NULL));
    llCleanMqs = atol(cgCleanMqs);
    if (llCleanMqs > 30)
    {
	llCleanMqs = 30;
    }
    llCleanMqs = llCleanMqs*24*60*60*(-1);
    AddSecondsToCEDATime(clDelTime,llCleanMqs,1);
    


	if(igUseQName == TRUE)
	{
		for(ili = 0 ; ili < 20 ; ili++)
		{
			if(strlen(rgMyQueues[ili].cgQName) > 0)
			{
				dbg(TRACE,"Now Delete Records older than <%s> from MQSTAB for INAM = <%s>",clDelTime,rgMyQueues[ili].cgQName);
				slFkt = START;
				slCursor = 0;

				sprintf(clSqlBuf,"DELETE FROM MQSTAB WHERE TPUT < '%s' AND INAM = '%s'",clDelTime,rgMyQueues[ili].cgQName);
				ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clData);
				if (ilRC != RC_SUCCESS)
				{
					dbg(TRACE,"%05d: sql_if failed or nothing to delete in CleanDb",__LINE__);
				}
				commit_work();
				close_my_cursor (&slCursor);
			}
		}
	}
	else
	{
		dbg(TRACE,"Now Delete Records older than <%s> from MQSTAB for INAM = <%s>",clDelTime,mod_name);
		slFkt = START;
		slCursor = 0;

		sprintf(clSqlBuf,"DELETE FROM MQSTAB WHERE TPUT < '%s' AND INAM = '%s'",clDelTime,mod_name);
		ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clData);
		if (ilRC != RC_SUCCESS)
		{
			dbg(TRACE,"%05d: sql_if failed or nothing to delete in CleanDb",__LINE__);
		}
		commit_work();
		close_my_cursor (&slCursor);
	}


    dbg(TRACE,"-----CleanDb end-----");


}


static int DoRecovery(char *pcpBeginTime, int ipQueueNumber)
{
    int ilRC = RC_SUCCESS;
    int ilRC2 = RC_SUCCESS;
    short slFkt = 0;
    short slCursor = 0;
    short slFkt2 = 0;
    short slCursor2 = 0;
    char clSqlBuf[4500] = "\0";
    char clData[4500] = "\0";
    int ilLen = 0;
    char clUrno[20] ="\0";
    char clMyTime[30] ="\0";
    char clNowTime[20] ="\0";

    MQMD     md = {MQMD_DEFAULT};    /* Message Descriptor              */
    MQLONG   CompCode;               /* completion code                 */
    MQLONG   Reason;                 /* reason code                     */
    MQPMO    pmo = {MQPMO_DEFAULT};  /* put message options             */

    dbg(TRACE,"-----DoRecovery start -----");

    slFkt = START;
    slCursor = 0;

    while (ilRC == RC_SUCCESS)
    {
	if(strlen(rgMyQueues[ipQueueNumber].cgFirstFailedUrno) == 0)
	{
	    sprintf(clSqlBuf,"SELECT URNO, DATA FROM MQSTAB WHERE INAM ='%s' AND TPUT > '%s' ORDER BY TPUT",mod_name,pcpBeginTime);
	    dbg(TRACE,"%05d: clSqlBuf: <%s>",__LINE__,clSqlBuf);
	    ilRC = sql_if (slFkt, &slCursor, clSqlBuf, clData);
	    if (ilRC != RC_SUCCESS)
	    {
		dbg(TRACE,"%05d: sql_if returnd ilRC: <%d> probably no Data TO SEND found",__LINE__,ilRC);
	    }
	    slFkt = NEXT;
	
	    BuildItemBuffer (clData, "", 2, ",");
	    strcpy(clSqlBuf,clData); /* clSqlBuf wird hier nur temporaer benutzt, hat nichts mit sql zu tun */
	    
	    ilLen = get_real_item (clUrno, clSqlBuf, 1);
	    ilLen = get_real_item (clData, clSqlBuf, -2);
	}
	else
	{
	    strcpy(clUrno,cgFirstFailedUrno);
	    strcpy(clData,cgFirstFailedData);
	}
	if (ilRC == RC_SUCCESS)
	{
	    
/* PUT MESSAGE TO MQ QUEUE */
	    memcpy(md.Format,           
		   MQFMT_STRING, MQ_FORMAT_LENGTH);
	    memcpy(md.MsgId, MQMI_NONE, sizeof(md.MsgId));
		md.Priority = igMqSendPrio;

	    pmo.Options = MQPMO_NEW_MSG_ID
		+ MQPMO_FAIL_IF_QUIESCING;
	    ConvertDbStringToClient(clData);
	    dbg(TRACE,"%05d: Send to Queue: <%s> clData <%s>, length <%d>",__LINE__,cgMqSend,clData,strlen(clData));
	    if(((strcmp(mod_name,"tams")) == 0) && (clData[0] == 'R') && (clData[1] == 'Q'))
	    {
		dbg(TRACE,"%05d: dont send AdhocRequest in adhocrequest-mode",__LINE__);
	    }
	    else
	    {
		MQPUT(HconSend,                /* connection handle               */
		      HobjSend,          /* object handle                   */
		      &md,                 /* message descriptor              */
		      &pmo,                /* default options                 */
		      strlen(clData),              /* buffer length                   */
		      clData,              /* message buffer                  */
		      &CompCode,           /* completion code                 */
		      &Reason);            /* reason code                     */

		strcpy(clMyTime,md.PutDate);
		clMyTime[16] = '\0';
		/* report reason, if any */
		dbg(TRACE,"%05d: MQPUT ended with reason code <%d>, Time: <%s>",__LINE__, Reason, clMyTime);

		if(Reason == 2009)
		{
			Terminate(1);
		}
	    }

	}
    }/*end of while(ilRC == RC_SUCCESS)*/
    close_my_cursor (&slCursor);

    dbg(TRACE,"-----DoRecovery end -----");

    return 0;
} /*end of DoRecovery*/

static int GetQueueNumber(char *clQueueName)
{
	int ili = 0;
	int ilResult = -1;

	for(ili = 0 ; ili < 20 ; ili++)
	{
		if(strcmp(rgMyQueues[ili].cgQName,clQueueName) == 0)
		{
			ilResult = ili;
		}
	}
	return ilResult;
}

/* *************************************************************** */
/*                                                                 */
/*                                                                 */
/*                                                                 */
/* *************************************************************** */
static int MyTcpSend(int ipSocket, char *pcpBuf, int ipBufLen)
{
        int ilRC = RC_SUCCESS;
        int ilSendLen = 0;


        /***** Parameter check *****/

        if(ipSocket < 1)
        {
                ilRC = RC_FAIL;
                dbg(TRACE,"MyTcpSend: invalid 1st Parameter");
        }/* end of if */

        if(pcpBuf == NULL)
        {
                ilRC = RC_FAIL;
                dbg(TRACE,"MyTcpSend: invalid 2nd Parameter");
        }/* end of if */

        if(ipBufLen < 1)
        {
                ilRC = RC_FAIL;
                dbg(TRACE,"MyTcpSend: invalid 3rd Parameter");
        }/* end of if */


        /***** functionality *****/

        if(ilRC == RC_SUCCESS)
        {
                InfoHeader.command = htonl(TCP_DATA);
                InfoHeader.length  = htonl(ipBufLen);

                ilSendLen = MyTcpSendBuf(ipSocket,(char *) &InfoHeader,sizeof(TCP_INFOHEADER));
                if(ilSendLen != sizeof(TCP_INFOHEADER))
                {
                        ilRC = RC_FAIL;
                }/* end of if */
        }/* end of if */

        if(ilRC == RC_SUCCESS)
        {
                ilRC = MyTcpSendBuf(ipSocket,pcpBuf,ipBufLen);
        }/* end of if */

        return(ilRC);

}/* end of MyTcpSend */

/* *************************************************************** */
/*                                                                 */
/*                                                                 */
/*                                                                 */
/* *************************************************************** */
static int MyTcpSendBuf(int ipSocket, char *pcpBuf, int ipBufLen)
{
        int ilRC = RC_SUCCESS;
        int ilSendLen = 0;
        int ilRemainder = 0;
        int ilFlag = 0;
        char *pclBufOfs = NULL;


        /***** Parameter check *****/

        /* FLW: internal static functions always get correct parameters !!! */



        /***** functionality *****/

        pclBufOfs   = pcpBuf;
        ilRemainder = ipBufLen;

        while(ilRemainder != 0)
        {
                errno = 0;
                ilSendLen = write(ipSocket,pclBufOfs,ilRemainder);
                /* ilSendLen = send(ipSocket,pclBufOfs,ilRemainder,ilFlag); */
                if(ilSendLen > 0)
                {
                        if(ilSendLen == ilRemainder)
                        {
                                ilRC         = ipBufLen;
                                ilRemainder  = 0;
                        }else{
                                ilRemainder -= ilSendLen;
                                pclBufOfs   += ilSendLen;
                        }/* end of if */
                }else{
                        switch(errno)
                        {
                        case EINTR :
                                dbg(DEBUG,"MyTcpSendBuf: errno EINTR <%d> ilRecvLen <%d>",errno,ilSendLen);
                                break;

                        case 0 :
                                dbg(DEBUG,"MyTcpSendBuf: errno <%d> ilRecvLen <%d>",errno,ilSendLen);
                                MyTcpCloseSocket(ipSocket);
                                ilRC = RC_FAIL;
                                ilRemainder = 0;
                                break;

                        default :
                                dbg(TRACE,"MyTcpSendBuf: <%d> bytes <%s>",ilSendLen,strerror(errno));
                                MyTcpCloseSocket(ipSocket);
                                ilRC = RC_FAIL;
                                ilRemainder = 0;
                                break;
                        }/* end of switch */
                }/* end of if */
        }/* end of while */

        return(ilRC);

}/* end of MyTcpSendBuf */

/* *************************************************************** */
/*                                                                 */
/*                                                                 */
/*                                                                 */
/* *************************************************************** */
static int MyTcpCloseSocket(int ipSocket)
{
        int ilRC = RC_SUCCESS;


        /***** Parameter check *****/

        /* FLW: internal static functions always get correct parameters !!! */



        /***** functionality *****/

        if(ilRC == RC_SUCCESS)
        {
                errno = 0;
                close(ipSocket);
                switch(errno)
                {
                case 0 :
                        break;
                default :
                        /*dbg(TRACE,"MyTcpCloseSocket: close <%s>",strerror(errno));*/
                        break;
                }/* end of switch */

                errno = 0;
                shutdown(ipSocket,2);
                switch(errno)
                {
                case 0 :
                        break;
                default :
                        /*dbg(TRACE,"MyTcpCloseSocket: shutdown <%s>",strerror(errno));*/
                        break;
                }/* end of switch */

        }/* end of if */

        return(ilRC);

}/* end of MyTcpCloseSocket */

/* *************************************************************** */
/*                                                                 */
/*                                                                 */
/*                                                                 */
/* *************************************************************** */
static int MyTcpRecv(int ipSocket, char **pppBuf, int *pipBufLen)
{
        int ilRC = RC_SUCCESS;
        int ilLoop = FALSE;
        int ilRecvLen = 0;


        /***** Parameter check *****/

        if(ipSocket < 0)
        {
                ilRC = RC_FAIL;
                dbg(TRACE,"MyTcpRecv: invalid 1st Parameter");
        }/* end of if */

        if(pppBuf == NULL)
        {
                ilRC = RC_FAIL;
                dbg(TRACE,"MyTcpRecv: invalid 2nd Parameter");
        }/* end of if */

        if(pipBufLen == NULL)
        {
                ilRC = RC_FAIL;
                dbg(TRACE,"MyTcpRecv: invalid 3rd Parameter");
        }/* end of if */


        /***** functionality *****/

        if(ilRC == RC_SUCCESS)
        {
                do
                {
                        ilLoop = FALSE;
                        ilRecvLen = MyTcpRecvBuf(ipSocket,(char *) &InfoHeader, TCP_INFOHEADER_SIZE);
                        if(ilRecvLen == RC_TIMEOUT)
                        {
                          ilRC = RC_TIMEOUT;
                        }
                        else
                        {
                          if(ilRecvLen != TCP_INFOHEADER_SIZE)
                          {
                                if(ilRecvLen != RC_RESET)
                                {
                                        dbg(TRACE,"MyTcpRecv: recv InfoHeader failed <%d> <%d>",ilRecvLen,TCP_INFOHEADER_SIZE);
                                        ilRC = RC_FAIL;
                                }
                                else
                                {
                                        ilRC = ilRecvLen;
                                        ilLoop = FALSE;
                                }/* end of if */
                          }
                          else
                          {
                                if(ntohl(InfoHeader.command) == TCP_KEEPALIVE)
                                {
                                        dbg(DEBUG,"MyTcpRecv: TCP_KEEPALIVE received");
                                        ilLoop = TRUE;
                                }/* end of if */
                          }/* end of if */
                        }
                }while((ilLoop == TRUE) && (ilRC == RC_SUCCESS));
        }/* end of if */

        if(ilRC == RC_SUCCESS)
        {
                switch(ntohl(InfoHeader.command))
                {
                case TCP_DATA :
                        ilRecvLen = ntohl(InfoHeader.length);
                        if(ilRecvLen > *pipBufLen)
                        {
                                errno = 0;
                                *pppBuf = realloc(*pppBuf,ilRecvLen);
                                if(*pppBuf == NULL)
                                {
                                        ilRC = RC_FAIL;
                                        dbg(TRACE,"MyTcpRecv: realloc(%d) failed <%s>",ilRecvLen,strerror(errno));
                                }else{
                                        *pipBufLen = ilRecvLen;
                                        ilRC = RC_SUCCESS;
                                        dbg(DEBUG,"MyTcpRecv: new buffer size <%d>",ilRecvLen);
                                }/* end of if */
                        }/* end of if */
                        if(ilRC == RC_SUCCESS)
                        {
                                memset(*pppBuf,0x00,ilRecvLen);
                                ilRC = MyTcpRecvBuf(ipSocket,*pppBuf,ilRecvLen);
                        }/* end of if */
                        break;
                case TCP_SHUTDOWN :
                        MyTcpCloseSocket(ipSocket);
                        ilRC = RC_SHUTDOWN;
                        dbg(DEBUG,"MyTcpRecv: TCP_SHUTDOWN received");
                        break;
                default :
                        ilRC = RC_FAIL;
                        dbg(TRACE,"MyTcpRecv: unknown command");
                        break;
                }/* end of switch */
        }/* end of if */

        return(ilRC);

}/* end of MyTcpRecv*/

/* *************************************************************** */
/*                                                                 */
/*                                                                 */
/*                                                                 */
/* *************************************************************** */
static int MyTcpRecvBuf(int ipSocket, char *pcpBuf, int ipBufLen)
{
        int ilRC = RC_SUCCESS;
        int ilRecvLen = 0;
        int ilRemainder = 0;
        int ilFlag = 0;
        char *pclBufOfs = NULL;


        /***** Parameter check *****/

        /* FLW: internal static functions always get correct parameters !!! */



        /***** functionality *****/

        if(ilRC == RC_SUCCESS)
        {
                pclBufOfs   = pcpBuf;
                ilRemainder = ipBufLen;

                while(ilRemainder != 0)
                {
                        errno = 0;
                        igGotAlarm = FALSE;
                        igExpectedSignal = SIGALRM;
                        alarm(igReadTimeout);
                        ilRecvLen = read(ipSocket,pclBufOfs,ilRemainder);
                        alarm(0);
                        igExpectedSignal = -1;
                        if(ilRecvLen > 0)
                        {
                                if(ilRecvLen == ilRemainder)
                                {
                                        ilRC         = ipBufLen;
                                        ilRemainder  = 0;
                                }else{
                                        ilRemainder -= ilRecvLen;
                                        pclBufOfs   += ilRecvLen;
                                }/* end of if */
                        }
                        else
                        {
                                if (igGotAlarm == FALSE)
                                {
                                  switch(errno)
                                  {
                                    case EINTR :
                                        dbg(DEBUG,"MyTcpRecvBuf: errno EINTR <%d> ilRecvLen <%d>",errno,ilRecvLen);
                                        if (ilRemainder == TCP_INFOHEADER_SIZE)
                                        {
                                                ilRC = RC_RESET;
                                                ilRemainder = 0;
                                        }/* end of if */
                                        break;

                                    case 0 :
                                        dbg(DEBUG,"MyTcpRecvBuf: errno <%d> ilRecvLen <%d>",errno,ilRecvLen);
                                        MyTcpCloseSocket(ipSocket);
                                        ilRC = RC_FAIL;
                                        ilRemainder = 0;
                                        break;

                                    default :
                                        dbg(TRACE,"MyTcpRecvBuf: <%d> bytes <%s>",ilRecvLen,strerror(errno));
                                        MyTcpCloseSocket(ipSocket);
                                        ilRC = RC_FAIL;
                                        ilRemainder = 0;
                                        break;
                                  }/* end of switch */
                                }
                                else
                                {
                                  dbg(TRACE,"--- WAITED %d SECONDS WITH NO MESSAGES ON THE BRIDGE",igReadTimeout);
                                  ilRC = RC_TIMEOUT;
                                  ilRemainder = 0;
                                }
                        }/* end of if */
                }/* end of while */
        }/* end of if */

        return(ilRC);

}/* end of MyTcpRecvBuf */

/* ************************************************************************* */
static int CheckMessageRecovery(char *pcpTable, char *pcpMkey)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  int ilQueueNumber = 0;
  int ilMaxSeqn = 0;
  short slCursor = 0;
  short slFkt = 0;
  char pclSqlBuf[128] = "";
  char pclSqlDat[4096*2] = "";
  char pclMsgMkey[32] = "";
  char pclMsgSeqn[8] = "";
  char pclMsgStat[16] = "";
  char pclMsgData[4096] = "";
  if (pcpMkey[0] != '\0')
  {
    dbg(TRACE,"===> CHECK MESSAGE STATUS OF <%s>",pcpMkey);

    sprintf(pclSqlBuf,"SELECT OUTT FROM %s WHERE MKEY='%s'",pcpTable,pcpMkey);
    dbg(TRACE,"%s",pclSqlBuf);
    slCursor = 0;
    slFkt = START;
    pclSqlDat[0] = 0x00;
    ilGetRc = sql_if (slFkt, &slCursor, pclSqlBuf, pclSqlDat);
    if (ilGetRc == DB_SUCCESS)
    {
      ilRc = RC_SUCCESS;
    }
    else
    {
      strcpy(pclSqlDat," ");
      ilRc = RC_SUCCESS;
      if (ilGetRc != NOTFOUND)
      {
        dbg (TRACE, "=====================================================");
        dbg (TRACE, "ERROR: COULD NOT READ %s",pcpTable);
        dbg (TRACE, "=====================================================");
        ilRc = RC_FAIL;
      }
    }
    close_my_cursor (&slCursor);
    if (pclSqlDat[0] != ' ')
    {
      dbg(TRACE,"MESSAGE <%s> ALREADY SENT (%s)",pcpMkey,pclSqlDat);
      return ilRc;
      dbg(TRACE,"MESSAGE <%s> NOT YET SENT. SO RECOVER ALL ...",pcpMkey);
    }
  }

  dbg(TRACE,"===> CHECK MESSAGE RECOVERY");
  igCurMsgSeqNo = -1;
  GetCurrentMsgSequence(pcpTable);
  dbg(TRACE,"CURR MAX SEQUENCE = %d",igCurMsgSeqNo);
  ilMaxSeqn = igCurMsgSeqNo;

  sprintf(pclSqlBuf,"SELECT MKEY,STAT,SEQN,DATA FROM %s WHERE OUTT=' ' ORDER BY MKEY",pcpTable);
  dbg(TRACE,"%s",pclSqlBuf);
  slCursor = 0;
  slFkt = START;
  pclSqlDat[0] = 0x00;
  ilGetRc = DB_SUCCESS;
  while (ilGetRc == DB_SUCCESS)
  {
    ilGetRc = sql_if (slFkt, &slCursor, pclSqlBuf, pclSqlDat);
    if (ilGetRc == DB_SUCCESS)
    {
      ilRc = RC_SUCCESS;
      BuildItemBuffer(pclSqlDat,"",4,",");
      (void) get_real_item(cgCurMsgMkey, pclSqlDat, 1);
      (void) get_real_item(pclMsgStat, pclSqlDat, 2);
      (void) get_real_item(pclMsgSeqn, pclSqlDat, 3);
      (void) get_real_item(pclMsgData, pclSqlDat, 4);
      dbg(TRACE,"---------------- RECOVERY -----------------");
      dbg(TRACE,"MKEY <%s> STAT <%s> SEQN <%s>",cgCurMsgMkey,pclMsgStat,pclMsgSeqn);
      dbg(DEBUG,"DATA:\n%s",pclMsgData);
      igCurMsgSeqNo = atoi(pclMsgSeqn);
      ilRc = SendMsgToMq(pclMsgData, ilQueueNumber, cgCurMsgMkey);
      dbg(TRACE,"-------------------------------------------");
    }
    else
    {
      strcpy(pclSqlDat,"0");
      dbg(TRACE,"NO MORE PENDING MESSAGES FOUND");
      ilRc = RC_SUCCESS;
      if (ilGetRc != NOTFOUND)
      {
        dbg (TRACE, "=====================================================");
        dbg (TRACE, "ERROR: COULD NOT READ %s",pcpTable);
        dbg (TRACE, "=====================================================");
        ilRc = RC_FAIL;
      }
    }
    slFkt = NEXT;
  }
  close_my_cursor (&slCursor);

  dbg(TRACE,"CURR MSG SEQUENCE = %d",igCurMsgSeqNo);
  igNxtMsgSeqNo = igCurMsgSeqNo + 1;
  if (igNxtMsgSeqNo > igMaxMsgSeqNo)
  {
    igNxtMsgSeqNo = 1;
  }
  dbg(TRACE,"NEXT MSG SEQUENCE = %d",igNxtMsgSeqNo);

  dbg(TRACE,"===> END OF MESSAGE RECOVERY");
  return ilRc;
}

/* ************************************************************************* */
static int GetCurrentMsgSequence(char *pcpTable)
{
  int ilRc = RC_SUCCESS;
  int ilGetRc = DB_SUCCESS;
  short slCursor = 0;
  short slFkt = 0;
  char pclSqlBuf[128] = "";
  char pclSqlDat[4096*2] = "";
  char pclMsgMkey[32] = "";
  char pclMsgSeqn[8] = "";
  char pclMsgStat[16] = "";
  char pclMsgData[4096] = "";
  dbg(TRACE,"---> FETCH LAST MESSAGE SEQUENCE");

  sprintf(pclSqlBuf,"SELECT MAX(MKEY) FROM %s",pcpTable);
  dbg(TRACE,"%s",pclSqlBuf);
  slCursor = 0;
  slFkt = START;
  cgMaxMsgMkey[0] = 0x00;

  ilGetRc = sql_if (slFkt, &slCursor, pclSqlBuf, cgMaxMsgMkey);
  if (ilGetRc == DB_SUCCESS)
  {
    ilRc = RC_SUCCESS;
  }
  else
  {
    strcpy(cgMaxMsgMkey,"0");
    ilRc = RC_SUCCESS;
    if (ilGetRc != NOTFOUND)
    {
      dbg (TRACE, "=====================================================");
      dbg (TRACE, "ERROR: COULD NOT READ %s",pcpTable);
      dbg (TRACE, "=====================================================");
      ilRc = RC_FAIL;
    }
  }
  close_my_cursor (&slCursor);

  sprintf(pclSqlBuf,"SELECT SEQN FROM %s WHERE MKEY='%s'",pcpTable,cgMaxMsgMkey);
  dbg(TRACE,"%s",pclSqlBuf);
  slCursor = 0;
  slFkt = START;
  pclSqlDat[0] = 0x00;

  ilGetRc = sql_if (slFkt, &slCursor, pclSqlBuf, pclSqlDat);
  if (ilGetRc == DB_SUCCESS)
  {
    ilRc = RC_SUCCESS;
  }
  else
  {
    strcpy(pclSqlDat,"-1");
    ilRc = RC_SUCCESS;
    if (ilGetRc != NOTFOUND)
    {
      dbg (TRACE, "=====================================================");
      dbg (TRACE, "ERROR: COULD NOT READ %s",pcpTable);
      dbg (TRACE, "=====================================================");
      ilRc = RC_FAIL;
    }
  }
  close_my_cursor (&slCursor);

  igCurMsgSeqNo = atoi(pclSqlDat);
  
  dbg(TRACE,"MAX MKEY <%s>",cgMaxMsgMkey);
  dbg(TRACE,"CUR SEQN (%d)",igCurMsgSeqNo);
  dbg(TRACE,"---> END FETCH LAST MESSAGE SEQUENCE");
  return ilRc;
}

/* ********************************************************** */
static void GetDataFieldSize(char *pcpTable)
{
  int ilRc = RC_SUCCESS;
  short slFkt = 0;
  short slCursor = 0;
  char clSqlBuf[256] = "";
  char clData[128] = "";
  char clTemp[128] = "";

  slFkt = START;
  slCursor = 0;
  clData[0] = '\0';

  sprintf(clSqlBuf,"SELECT DATA_LENGTH FROM USER_TAB_COLUMNS WHERE TABLE_NAME='%s' AND COLUMN_NAME='DATA'",pcpTable);
  ilRc = sql_if (slFkt, &slCursor, clSqlBuf, clData);
  close_my_cursor (&slCursor);
  commit_work();
  if (ilRc != DB_SUCCESS)
  {
    sprintf(clData,"%d",igMsgFldSize);
    dbg(TRACE,"DEFAULT MsgFldSize SET TO <%s>",clData);
  }
  ilRc = ReadConfigEntry("BRIDGE_SPOOLER","DATA_LENGTH",clTemp,clData);
  igMsgFldSize = atoi(clTemp);
  dbg(TRACE,"HANDLED MsgFldSize SET TO <%d>",igMsgFldSize);
  return;
}

