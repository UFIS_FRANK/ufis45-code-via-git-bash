#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/PVG/PVG_Server/Base/Server/Kernel/upahdl.c 1.3 2005/05/11 16:07:38SGT hag Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :                                                           */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history :                                                           */
/* 20030630 JIM: may core!: dbg(TRACE,"Command: <%s>,prlCmdblk->command")     */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* source-code-control-system version string                                  */

/* static char sccs_version[] ="@(#) UFIS44 (c) ABB AAT/I upahdl.c 43.1.0 / 29.03.2005 HAG";*/

/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */


#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "sthdef.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "fditools.h"
#include "libccstr.h"
#include "l_ccstime.h"
#include "syslib.h"
#include "db_if.h"
#include <time.h>
#include <cedatime.h>
#include "helpful.h"

#ifdef _HPUX_SOURCE
	extern int daylight;
	extern long timezone;
	extern char *tzname[2];
#else
	extern int _daylight;
	extern long _timezone;
	extern char *_tzname[2];
#endif

extern int get_no_of_items(char *s);

#define MAXDATA 2048
#define SENDER	1
#define PATCHER 2

#define URNOS_TO_FETCH 100

#define INSERT_CMD "IRT,IFR"
#define REGUALAR_CMD "URT,DRT,UFR,DFR"
#define SPECIAL_CMD "ISF,SPR,JOF"
#define AFTTAB_CMD "ISF,SPR,JOF,IFR,UFR,DFR"
#define DELETE_CMD "DRT,DFR"
 

int IS_EMPTY (char *pcpBuf) 
{
	return	(!pcpBuf || *pcpBuf==' ' || *pcpBuf=='\0') ? TRUE : FALSE;
}

int debug_level = 0;

char cgAttSep[2]="\1";

/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static EVENT	*prgOutEvent  = NULL;
static long		lgOutEventSize  = 0L ;

static char cgProcessName[20] = "\0";
static char  cgConfigFile[512] = "\0";
static char  cgHopo[8] = "\0";                         /* default home airport    */
static char  cgTabEnd[8] ="\0";                       /* default table extension */

static long lgEvtCnt = 0;
static	int	igStartUpMode = DEBUG;
static	int	igRuntimeMode = TRACE;
static int igDest = 0;
static int igMode = 0;

static long   lgActUrno =0 ;
static int   igReservedUrnoCnt = 0;
static int igFlightID = 7800;
static int igSqlhdlID = 7150;


/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
static int  Init_Upahdl();
static int ReadConfigEntry(char *pcpSection,char *pcpKeyword,
			   char *pcpCfgBuffer);


static int	Reset(void);                       /* Reset program          */
static void	Terminate(int ipSleep);            /* Terminate program      */
static void	HandleSignal(int);                 /* Handles signals        */
static void	HandleErr(int);                    /* Handles general errors */
static void   HandleQueErr(int);                 /* Handles queuing errors */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/


static int	HandleData(EVENT *prpEvent);       /* Handles event data     */
static int  GetCommand(char *pcpCommand, int *pipCmd);
static int GetDebugLevel(char *pcpMode, int *pipMode);
static int GetKeyFields ( char *pcpCmd, char *pcpTable, char *pcpKeyFields );
static int GetMissingKeyFields ( char *pcpGiven, char *pcpRequested, char *pcpMissing );
static int GetUrnoFromSelection(char *pcpSelection,char *pcpUrno);
static int ReadMissingData ( char *pcpTable, char *pcpUrno, char *pcpFields, char *pcpDataList );
static int BuildOutEventSender ( char *pcpAddFields, char *pcpAddData );
static int FindNewUrno ( char *pcpTable, char *pcpKeyFlds, char *pcpFields, 
						 char *pcpData, char *pcpUrno );
static int GetFieldData ( char *pcpFields, char *pcpData, char *pcpFld, char *pcpFldDat );
static int AddRelatedRecords ( char *pcpTab, char *pcpFlds1, char *pcpDat1, char *pcpFlds2, char *pcpDat2 );
static int DoAddPatch ( char *pcpTab, char *pcpFlds, char *pcpDat, char *pcpPatchedDat );
static int ReplaceUrno ( char *pcpSel, char *pcpFlds, char *pcpDat, char *pcpUrno );
static int GetNextUrno(char *pcpUrno);
static int BuildOutEventPatcher ( char *pcpSel, char *pcpData );
static int BuildSPRSelection ( char *pcpKeyFlds, char *pcpFlds, char *pcpDat, char *pcpNewSel );
static int AddJOFRecords ( char *pcpSel, char *pcpKeyFlds, char *pcpData );
static int HandleJOF ( char *pcpKeyFlds, char *pcpSel, char *pcpData, char *pcpOutData );
static int BuildOutEvent ( char *pcpSel, char *pcpFld, char *pcpDat );
static int AddRelatedISFRecord ( char *pcpSel, char *pcpKeyFlds, char *pcpData );
static int HandleSpecialISF ( char *pcpKeyFlds, char *pcpSel, char *pcpData, char *pcpOutData );
static int GetOriDest4Cmd ( char *pcpCmd );
static int IsToTransfer ( char *pcpCmd, char *pcpTable );

/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
	int	ilRc = RC_SUCCESS;			/* Return code			*/
	int	ilCnt = 0;
	int ilOldDebugLevel = 0;


	INITIALIZE;			/* General initialization	*/



	debug_level = TRACE;

	strcpy(cgProcessName,argv[0]);

	dbg(TRACE,"MAIN: version <%s>",mks_version);

	/* Attach to the MIKE queues */
	do
	{
		ilRc = init_que();
		if(ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"MAIN: init_que() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		}/* end of if */
	}while((ilCnt < 10) && (ilRc != RC_SUCCESS));

	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_que() failed! waiting 60 sec ...");
		sleep(60);
		exit(1);
	}else{
		dbg(TRACE,"MAIN: init_que() OK! mod_id <%d>",mod_id);
	}/* end of if */

	do
	{
		ilRc = init_db();
		if (ilRc != RC_SUCCESS)
		{
			check_ret(ilRc);
			dbg(TRACE,"MAIN: init_db() failed! waiting 6 sec ...");
			sleep(6);
			ilCnt++;
		} /* end of if */
	} while((ilCnt < 10) && (ilRc != RC_SUCCESS));

	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
		sleep(60);
		exit(2);
	}else{
		dbg(TRACE,"MAIN: init_db() OK!");
	} /* end of if */

	/* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */

	sprintf(cgConfigFile,"%s/%s",getenv("BIN_PATH"),mod_name);
	dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
	ilRc = TransferFile(cgConfigFile);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */

	sprintf(cgConfigFile,"%s/%s.cfg",getenv("CFG_PATH"),mod_name);
	dbg(TRACE,"ConfigFile <%s>",cgConfigFile);
	ilRc = TransferFile(cgConfigFile);
	if(ilRc != RC_SUCCESS)
	{ 
		dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */

	ilRc = SendRemoteShutdown(mod_id);
	if(ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: SendRemoteShutdown(%d) failed!",mod_id);
	} /* end of if */

	if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE) && (ctrl_sta != HSB_ACT_TO_SBY))
	{
		dbg(DEBUG,"MAIN: waiting for status switch ...");
		HandleQueues();
	}/* end of if */

	if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
	{
		dbg(TRACE,"MAIN: initializing ...");
			ilRc = Init_Upahdl();
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"Init_Upahdl: init failed!");
			} /* end of if */

	} else {
		Terminate(30);
	}/* end of if */



	dbg(TRACE,"=====================");
	dbg(TRACE,"MAIN: initializing OK");
	dbg(TRACE,"=====================");

	debug_level = igRuntimeMode;


	for(;;)
	{
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* Acknowledge the item */
		ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
		if( ilRc != RC_SUCCESS ) 
		{
			/* handle que_ack error */
			HandleQueErr(ilRc);
		} /* fi */

		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */

		prgEvent = (EVENT *) prgItem->text;
				
		if( ilRc == RC_SUCCESS )
		{
			
			lgEvtCnt++;

			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				break;	
			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				/* CloseConnection(); */
				HandleQueues();
				break;	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate(1);
				break;	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				break;	
			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;
			case	SHUTDOWN	:
				/* process shutdown - maybe from uutil */
				Terminate(1);
				break;
					
			case	RESET		:
				ilRc = Reset();
				break;
					
			case	EVENT_DATA	:
				if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
				{
					ilRc = HandleData(prgEvent);
					if(ilRc != RC_SUCCESS)
					{
						HandleErr(ilRc);
					}/* end of if */
				}else{
					dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
				}/* end of if */
				break;
				

			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;
			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;
			default			:
				dbg(TRACE,"MAIN: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */

		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		} /* end else */
		
	} /* end for */
	
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/



static int ReadConfigEntry(char *pcpSection,char *pcpKeyword, char *pcpCfgBuffer)
{
    int ilRc = RC_SUCCESS;

    char clSection[124] = "\0";
    char clKeyword[124] = "\0";

    strcpy(clSection,pcpSection);
    strcpy(clKeyword,pcpKeyword);

    ilRc = iGetConfigEntry(cgConfigFile,clSection,clKeyword,
			   CFG_STRING,pcpCfgBuffer);
    if(ilRc != RC_SUCCESS)
    {
	dbg(TRACE,"Not found in %s: <%s> <%s>",cgConfigFile,clSection,clKeyword);
    } 
    else
    {
	dbg(DEBUG,"Config Entry <%s>,<%s>:<%s> found in %s",
	    clSection, clKeyword ,pcpCfgBuffer, cgConfigFile);
    }/* end of if */
    return ilRc;
}


static int Init_Upahdl()
{
	int  ilRc = RC_SUCCESS;			/* Return code */
	char  clValue[24] = "\0";

	if(ilRc == RC_SUCCESS)
	{
		GetDebugLevel("STARTUP_MODE", &igStartUpMode);
		GetDebugLevel("RUNTIME_MODE", &igRuntimeMode);
	}
	debug_level = igStartUpMode;

	if(ilRc == RC_SUCCESS)
	{
		/* read HomeAirPort from SGS.TAB */
		ilRc = tool_search_exco_data("SYS", "HOMEAP", cgHopo);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"<Init_Upahdl> EXTAB,SYS,HOMEAP not found in SGS.TAB");
			Terminate(30);
		} else {
			dbg(TRACE,"<Init_Upahdl> home airport <%s>",cgHopo);
		}
	}

	if(ilRc == RC_SUCCESS)
	{
		ilRc = tool_search_exco_data("ALL","TABEND", cgTabEnd);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"<Init_Upahdl> EXTAB,ALL,TABEND not found in SGS.TAB");
			Terminate(30);
		} else {
			dbg(TRACE,"<Init_Upahdl> table extension <%s>",cgTabEnd);
		}
	}

	if(ilRc == RC_SUCCESS)
	{
		if ( ReadConfigEntry ( "MAIN", "MODE", clValue ) == RC_SUCCESS )
		{
			if ( strcasecmp ( clValue, "PATCH" ) == 0 )
				igMode = PATCHER;
			else if ( strcasecmp ( clValue, "SEND" ) == 0 )
				igMode = SENDER;
		}
		if ( ReadConfigEntry ( "MAIN", "DESTINATION", clValue ) == RC_SUCCESS )
			igDest = atoi ( clValue );
		if ( !igMode || (igDest<0) )
			ilRc = RC_FAIL;
		dbg ( DEBUG, "Init_Upahdl: igMode <%d> igDest <%d>", igMode, igDest );
	}
	return(ilRc);
	
} /* end of initialize */




/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
	int	ilRc = RC_SUCCESS;				/* Return code */
	
	dbg(TRACE,"Reset: now resetting");
	
	return ilRc;
	
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleep)
{
	/* unset SIGCHLD ! DB-Child will terminate ! */

	dbg(TRACE,"Terminate: now DB logoff ...");

	signal(SIGCHLD,SIG_IGN);

	logoff();

	dbg(TRACE,"Terminate: now sleep(%d) ...",ipSleep);


	dbg(TRACE,"Terminate: now leaving ...");

	fclose(outp);

	sleep(ipSleep < 1 ? 1 : ipSleep);
	
	exit(0);
	
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/

static void HandleSignal(int pipSig)
{
	dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);

	switch(pipSig)
	{
	default	:
		Terminate(1);
		break;
	} /* end of switch */

	exit(1);
	
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
	dbg(TRACE,"HandleErr: <%d> : unknown ERROR",pipErr);

	return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
	switch(pipErr) {
	case	QUE_E_FUNC	:	/* Unknown function */
		dbg(TRACE,"<%d> : unknown function",pipErr);
		break;
	case	QUE_E_MEMORY	:	/* Malloc reports no memory */
		dbg(TRACE,"<%d> : malloc failed",pipErr);
		break;
	case	QUE_E_SEND	:	/* Error using msgsnd */
		dbg(TRACE,"<%d> : msgsnd failed",pipErr);
		break;
	case	QUE_E_GET	:	/* Error using msgrcv */
		dbg(TRACE,"<%d> : msgrcv failed",pipErr);
		break;
	case	QUE_E_EXISTS	:
		dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
		break;
	case	QUE_E_NOFIND	:
		dbg(TRACE,"<%d> : route not found ",pipErr);
		break;
	case	QUE_E_ACKUNEX	:
		dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
		break;
	case	QUE_E_STATUS	:
		dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
		break;
	case	QUE_E_INACTIVE	:
		dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
		break;
	case	QUE_E_MISACK	:
		dbg(TRACE,"<%d> : missing ack ",pipErr);
		break;
	case	QUE_E_NOQUEUES	:
		dbg(TRACE,"<%d> : queue does not exist",pipErr);
		break;
	case	QUE_E_RESP	:	/* No response on CREATE */
		dbg(TRACE,"<%d> : no response on create",pipErr);
		break;
	case	QUE_E_FULL	:
		dbg(TRACE,"<%d> : too many route destinations",pipErr);
		break;
	case	QUE_E_NOMSG	:	/* No message on queue */
		dbg(TRACE,"<%d> : no messages on queue",pipErr);
		break;
	case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
		dbg(TRACE,"<%d> : invalid originator=0",pipErr);
		break;
	case	QUE_E_NOINIT	:	/* Queues is not initialized*/
		dbg(TRACE,"<%d> : queues are not initialized",pipErr);
		break;
	case	QUE_E_ITOBIG	:
		dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
		break;
	case	QUE_E_BUFSIZ	:
		dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
		break;
	default			:	/* Unknown queue error */
		dbg(TRACE,"<%d> : unknown error",pipErr);
		break;
	} /* end switch */
         
	return;

} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
	int	ilRc = RC_SUCCESS;			/* Return code */
	int	ilBreakOut = FALSE;
	
	do
	{
		ilRc = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;	

		if( ilRc == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRc = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRc != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRc);
			} /* fi */
		
			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				ilBreakOut = TRUE;
				break;	

			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), send_message() or timsch() ! */
				ctrl_sta = prgEvent->command;
				Terminate(1);
				break;	
	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				ilBreakOut = TRUE;
				break;	

			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;

			case	SHUTDOWN	:
				Terminate(1);
				break;
						
			case	RESET		:
				ilRc = Reset();
				break;
						
			case	EVENT_DATA	:
				dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
					
			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;

			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;

			default			:
				dbg(TRACE,"HandleQueues: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRc);
		} /* end else */
	} while (ilBreakOut == FALSE);

			ilRc = Init_Upahdl();
			if(ilRc != RC_SUCCESS)
			{
				dbg(TRACE,"InitDemhdl: init failed!");
			} 
     

} /* end of HandleQueues */
	



/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData(EVENT *prpEvent)
{
	int	   ilRc           = RC_SUCCESS;			/* Return code */
	int      ilCmd          = 0;

	BC_HEAD *prlBchead       = NULL;
	CMDBLK  *prlCmdblk       = NULL;
	char    *pclSelection    = NULL;
	char    *pclFields       = NULL;
	char    *pclData         = NULL;
	char    *pclRow          = NULL;
	char 		clUrnoList[MAXDATA+1];
	char 		clTable[34];
	int			ilUpdPoolJob = TRUE;
	char		clKeyFields[104];
	char		clMissingFlds[104];
	char 		clDataList[MAXDATA+1];
	char 		clNewSel[MAXDATA+1];
	char 		clOriFlds[MAXDATA+1];
	char 		clOriData[MAXDATA+1];
	int			ilSelItems, ilFldItems, ilDatItems, i;
	int			ilPassThrough;
	char    *pclSavSelection    = NULL;
	char    *pclSavFields       = NULL;
	char    *pclSavData         = NULL;
	int		ilFwdDest=0;
	long	llActSize;
	short	blISFWithRel = 0;

	prlBchead    = (BC_HEAD *) ((char *)prpEvent + sizeof(EVENT));
	prlCmdblk    = (CMDBLK *)  ((char *)prlBchead->data);
	pclSelection = prlCmdblk->data;
	pclFields    = pclSelection + strlen(pclSelection) + 1;
	pclData      = pclFields + strlen(pclFields) + 1;

	pclSavSelection    = pclSelection;
	pclSavFields       = pclFields   ;
	pclSavData         = pclData     ;

	strcpy(clTable,prlCmdblk->obj_name);
	clTable[6] = '\0';
	if ( strstr(AFTTAB_CMD, prlCmdblk->command) && !clTable[0] )
		sprintf ( clTable, "AFT%s", cgTabEnd );
		
	dbg(TRACE,"========== START <%10.10d> ==========",lgEvtCnt);

	/****************************************/
	dbg ( TRACE, "item->priority <%d>", prgItem->priority );
	dbg(TRACE,"event->originator <%d>",prpEvent->originator);
	dbg(TRACE,"event->data_length<%ld>",prpEvent->data_length);
	DebugPrintBchead(TRACE,prlBchead);
	DebugPrintCmdblk(TRACE,prlCmdblk);
	dbg(TRACE,"selection <%s>",pclSelection);
	dbg(TRACE,"fields    <%s>",pclFields);
	dbg(TRACE,"data      <%s>",pclData);
	/****************************************/
	if ( igMode == SENDER )
		ilRc = IsToTransfer ( prlCmdblk->command, clTable );
	if ( ilRc == RC_SUCCESS )
		ilRc = GetKeyFields ( prlCmdblk->command, clTable, clKeyFields );
	if ( ilRc == RC_SUCCESS )
	{
		ilSelItems = GetNoOfElements ( pclSelection, '\n' );
		if ( ( strcmp ( prlCmdblk->command, "ISF" ) == 0 ) && (ilSelItems>1) )
		{	/* naechste Ausnahme, bei Towing hat selection ploetzlich ein newline */
			ilSelItems = 1;
			blISFWithRel = 1;
		}
		ilFldItems = GetNoOfElements ( pclFields, '\n' );
		ilDatItems = GetNoOfElements ( pclData, '\n' );

		ilPassThrough = TRUE;

		dbg ( TRACE, "HandleData: Items in SEL %d, FIELDS %d, DATA %d",
			          ilSelItems, ilFldItems, ilDatItems );

		if ( (ilSelItems<=1) || ( (ilSelItems<=ilFldItems) && (ilSelItems<=ilDatItems) ) )
		{
			if ( ilSelItems  < 1 )
				ilSelItems  = 1;
			for ( i=1; i<=ilSelItems; i++ )
			{
				clMissingFlds[0] = '\0';
				clDataList[0] = '\0';
				clUrnoList[0] = '\0';

				if ( strcmp ( prlCmdblk->command, "ISF" ) == 0 )
				{	/* naechste Ausnahme, bei Towing hat selection ploetzlich ein newline */
					strcpy ( clNewSel, pclSelection );
				}
				else
					GetNextDataItem(clNewSel,&pclSelection,"\n","","\0\0");
				
				GetNextDataItem(clOriFlds,&pclFields,"\n","","\0\0");
				GetNextDataItem(clOriData,&pclData,"\n","","\0\0");
		
				if ( igMode == SENDER )
				{
					if ( strstr ( REGUALAR_CMD, prlCmdblk->command ) )
					{
						if ( IS_EMPTY(clOriFlds) && !IS_EMPTY(clOriData) )
						{
							dbg ( TRACE, "HandleData: Unexpected characters in data -> CLEAR DATA string!!" );
							clOriData[0] = '\0';
						}
						ilRc = GetUrnoFromSelection ( clNewSel, clUrnoList );
						if ( (ilRc==RC_SUCCESS) && strstr(DELETE_CMD, prlCmdblk->command) &&
							 !IS_EMPTY(clOriFlds) && IS_EMPTY(clOriData) )
						{
							dbg ( TRACE, "HandleData: Fields are filled, but data is empty -> going to read !!" );
							ilRc = ReadMissingData ( clTable, clUrnoList, clOriFlds, clOriData );
						}
						if ( ilRc == RC_SUCCESS )
							ilRc = GetMissingKeyFields ( clOriFlds, clKeyFields, clMissingFlds );
						if ( (ilRc== RC_SUCCESS) && clMissingFlds[0] )
							ilRc = ReadMissingData ( clTable, clUrnoList, clMissingFlds, clDataList );
						ilPassThrough = FALSE;
					}
					if ( (ilRc==RC_SUCCESS) && 
						 ( strstr(REGUALAR_CMD,prlCmdblk->command) || strstr(INSERT_CMD,prlCmdblk->command) )
					   )
					{
						ilRc = AddRelatedRecords ( clTable,  clOriFlds, clOriData, clMissingFlds, clDataList );
						ilPassThrough = FALSE;
					}
					if ( (ilRc==RC_SUCCESS) && !strcmp( prlCmdblk->command, "SPR" ) )
					{
						if ( IS_EMPTY(clOriFlds) )
							clOriData[0] = '\0';
						ilRc = GetUrnoFromSelection ( clNewSel, clUrnoList );
						if ( ilRc == RC_SUCCESS )
							ilRc = ReadMissingData ( clTable, clUrnoList, clKeyFields, clDataList );
						strcpy ( clMissingFlds, clKeyFields );
						ilPassThrough = FALSE;
					}
					if ( (ilRc==RC_SUCCESS) && !strcmp( prlCmdblk->command, "JOF" ) )
					{
						ilRc = AddJOFRecords ( clNewSel, clKeyFields, clDataList );
						ilPassThrough = FALSE;
					}
					if ( (ilRc==RC_SUCCESS) && blISFWithRel )
					{
						ilPassThrough = FALSE;
						ilRc = AddRelatedISFRecord ( clNewSel, clKeyFields, clDataList );
					}
					if ( ilRc == RC_SUCCESS )
					{
						if ( clMissingFlds[0] )
						{	
							if ( clOriFlds[0] )
								strcat ( clOriFlds, "," );
							strcat ( clOriFlds, clMissingFlds );
						}
						if ( clDataList[0] )
						{
							if ( clOriData[0] && (clDataList[0] != cgAttSep[0]) )
								strcat ( clOriData, "," );
							strcat ( clOriData, clDataList );
						}
						if ( ilPassThrough )
							ilRc = BuildOutEvent ( pclSavSelection, pclSavFields, pclSavData );
						else
							ilRc = BuildOutEvent ( clNewSel, clOriFlds, clOriData );

					}
				}
				else if ( igMode == PATCHER )
				{
					ilPassThrough = FALSE;
					if ( strstr(REGUALAR_CMD,prlCmdblk->command) || strstr(INSERT_CMD,prlCmdblk->command) )
						ilRc = DoAddPatch ( clTable, clOriFlds, clOriData, clDataList );
					else if ( !strcmp( prlCmdblk->command, "SPR" ) )
						ilRc = BuildSPRSelection ( clKeyFields, clOriFlds, clOriData, clNewSel );
					else if ( !strcmp( prlCmdblk->command, "JOF" ) )
					{
						ilRc = HandleJOF ( clKeyFields, clNewSel, clOriData, clDataList );
					}
					else if ( blISFWithRel )
					{
						ilRc = HandleSpecialISF ( clKeyFields, clNewSel, clOriData, clDataList );
					}
					else
					{
						strcpy ( clDataList, clOriData );
						ilPassThrough = TRUE;
					}
					if ( ilRc == RC_SUCCESS ) 
					{
						if ( strstr(REGUALAR_CMD,prlCmdblk->command) )
							ilRc = FindNewUrno	( clTable, clKeyFields, clOriFlds, clDataList, clUrnoList );
						else if ( strstr(INSERT_CMD,prlCmdblk->command) )
							ilRc = GetNextUrno(clUrnoList);
					}
					if ( ( ilRc == RC_SUCCESS ) && *clUrnoList )
						ilRc = ReplaceUrno ( clNewSel, clOriFlds, clDataList, clUrnoList );
					if ( ilRc == RC_SUCCESS )
					{
						if ( ilPassThrough )
							ilRc = BuildOutEvent ( pclSavSelection, pclSavFields, pclSavData );
						else
							ilRc = BuildOutEvent ( clNewSel, clOriFlds, clDataList );
					}
				}
				if ( ilRc == RC_SUCCESS )
				{
					ilRc = que (QUE_PUT, igDest, mod_id, prgItem->priority, lgOutEventSize, 
								(char *)prgOutEvent) ;
					dbg ( TRACE, "HandleData: forward modified event to <%d> RC <%d>", igDest, ilRc );
					if (ilRc != RC_SUCCESS)
					{
						HandleQueErr (ilRc) ;
						DebugPrintEvent (TRACE, prgOutEvent) ;
						/*DebugPrintBchead (TRACE, prlBchead) ;
						DebugPrintCmdblk (TRACE, prlCmdblk) ;*/
					} /* end of if */
				}
			}
		}	
		else
			dbg ( TRACE, "HandleData: Different length of Selection, field and data list -> EVENT WILL NOT BE PROCESSED !!!" );
			  
	}
	if ( igMode == SENDER )
	{
		ilFwdDest = GetOriDest4Cmd ( prlCmdblk->command );
		if ( ilFwdDest > 0 )
		{
			llActSize = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + prgEvent->data_length + 10 ;  
			ilRc = que (QUE_PUT, ilFwdDest, mod_id, prgItem->priority, llActSize, (char *)prgEvent) ;
			dbg ( TRACE, "HandleData: forward original event to <%d> RC <%d>", ilFwdDest, ilRc );
			if (ilRc != RC_SUCCESS)
			{
				HandleQueErr (ilRc) ;
				DebugPrintEvent (TRACE, prgEvent) ;
			} /* end of if */
		}
	}
	dbg(TRACE,"==========  END  <%10.10d> ==========",lgEvtCnt);
	return(RC_SUCCESS);
	
} /* end of HandleData */

static int GetDebugLevel(char *pcpMode, int *pipMode)
{
	int		ilRC = RC_SUCCESS;
	char	clCfgValue[64];

	ilRC = iGetConfigRow(cgConfigFile, "SYSTEM", pcpMode, CFG_STRING,
							clCfgValue);

	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"SYSTEM %s not found in <%s>", pcpMode, cgConfigFile);
	}
	else
	{
		dbg(TRACE, "SYSTEM %s <%s>", pcpMode, clCfgValue);
		if (!strcmp(clCfgValue, "DEBUG"))
			*pipMode = DEBUG;
		else if (!strcmp(clCfgValue, "TRACE"))
			*pipMode = TRACE;
		else
			*pipMode = 0;
	}

	return ilRC;
}

static int GetKeyFields ( char *pcpCmd, char *pcpTable, char *pcpKeyFields )
{
	char clEntry[24];
	int ilRc;

	if ( !pcpCmd || !pcpTable || !pcpKeyFields )
	{
		dbg ( TRACE, "GetKeyFields: At least one parameter is null");
		return RC_FAIL;
	}
	if ( strlen ( pcpTable ) < 3 )
	{
		dbg ( TRACE, "GetKeyFields: Table not filled");
		return RC_FAIL;
	}
	
	strncpy ( clEntry, pcpTable, 3 );
	sprintf ( &(clEntry[3]), "_%s", pcpCmd );

	ilRc = iGetConfigRow(cgConfigFile, "KEYFIELDS", clEntry, CFG_STRING, pcpKeyFields);
	if ( ilRc != RC_SUCCESS )
	{
		clEntry[3] = '\0';
		ilRc = iGetConfigRow(cgConfigFile, "KEYFIELDS", clEntry, CFG_STRING, pcpKeyFields);
	}
	if ( ilRc != RC_SUCCESS )
	{
		pcpKeyFields[0] = '\0';
		dbg ( DEBUG, "GetKeyFields: no configuration for table <%s> command <%s>", pcpTable, pcpCmd );
	}
	else
		dbg ( DEBUG, "GetKeyFields: found fields <%s> for entry <%s>", pcpKeyFields, clEntry );
	return ilRc;
}

static int GetMissingKeyFields ( char *pcpGiven, char *pcpRequested, char *pcpMissing )
{
	int		ilNoReq=0, i, ilItemNo, ilRc=RC_SUCCESS;
	char	clFina[12];

	if ( !pcpGiven || !pcpRequested || !pcpMissing )
	{
		dbg ( TRACE, "GetMissingKeyFields: At least one parameter is null");
		return RC_FAIL;
	}

	pcpMissing[0] = '\0';
	ilNoReq = get_no_of_items(pcpRequested);

	for ( i=1; (ilRc==RC_SUCCESS) && (i<=ilNoReq); i++ )
	{
		if(get_real_item(clFina,pcpRequested,i) > 0)
		{
			ilItemNo = get_item_no(pcpGiven, clFina, 5);
			if (ilItemNo < 0)
			{
				if ( pcpMissing[0] ) 
					strcat ( pcpMissing, "," );
				strcat ( pcpMissing, clFina ); 
			}
		}
		else
		{
			dbg ( TRACE, "GetMissingKeyFields; Cannot extract field %d/%d from <%s>", 
				  i, ilNoReq, pcpRequested );
			ilRc = RC_FAIL;
		}
	}
	dbg ( TRACE, "GetMissingKeyFields: KeyFields <%s> Missing <%s> RC <%d>", pcpRequested, pcpMissing, ilRc );
	return ilRc;
}


/****************************************************************************
* Function: GetUrnoFromSelection
* Parameter:           
*			 
* Return:
* Description: gets the Urno from the SQL Statement "WHERE URNO = 'xxxxxxx'"
*			   
*****************************************************************************/
static int GetUrnoFromSelection(char *pcpSelection,char *pcpUrno)
{
	char *pclFirst=0;
	char *pclLast=0;
	char pclTmp[24];
	int ilLen, ilRc = RC_FAIL;
	
	pcpUrno[0] = '\0';
	pclFirst = strchr(pcpSelection,'\'');
	if (pclFirst != NULL)
	{
		pclFirst++;
		pclLast  = strrchr(pcpSelection,'\'');
		if (pclLast == NULL)
		{
			pclLast = pclFirst + strlen(pclFirst);
		}
	}
	else
	{
		pclFirst = pcpSelection;
		while (!isdigit(*pclFirst) && *pclFirst != '\0')
		{
			pclFirst++;
		}
		pclLast = pclFirst;
		while ( isdigit(*pclLast) )
			pclLast++;
		/*strcpy(pcpUrno,pclFirst);*/
	}
	if ( pclLast && pclFirst )
	{
		ilLen = pclLast-pclFirst < 22 ? pclLast - pclFirst : 22;
		strncpy(pclTmp,pclFirst,ilLen);
		pclTmp[ilLen] = '\0';
		strcpy(pcpUrno,pclTmp);
		if ( strlen ( pcpUrno ) > 0 )
			ilRc = RC_SUCCESS;
	}
	dbg ( DEBUG, "GetUrnoFromSelection: SEL <%s> URNO <%s> RC <%d>", pcpSelection, pcpUrno, ilRc );
	return ilRc;
}


static int ReadMissingData ( char *pcpTable, char *pcpUrno, char *pcpFields, char *pcpDataList )
{
	char pclDataArea[2256];
	char pclSqlBuf[256], *ps;
	short slCursor = 0;
	int ilRc, i, ilNoFld ;
	
	pcpDataList[0] = '\0';

	sprintf(pclSqlBuf, "SELECT %s FROM %s WHERE URNO = '%s'",pcpFields, pcpTable, pcpUrno);

	ilRc = sql_if(START,&slCursor,pclSqlBuf,pclDataArea);
	if (ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"ReadMissingData: Reading with selection <%s> failed, RC <%d>", pclSqlBuf, ilRc );
		check_ret(ilRc);
	} /* end while */
	else
	{
		ilNoFld = get_no_of_items(pcpFields);
		ps = pclDataArea;
		for ( i=1; (i<=ilNoFld) &&(ilRc==RC_SUCCESS); i++ )
	 	{
			if ( strlen(pcpDataList) + strlen(ps) + 1 < MAXDATA )
			{
				strcat ( pcpDataList, ps );
				if ( i < ilNoFld )
					strcat ( pcpDataList, "," );
				ps += strlen (ps) + 1;
			}
			else 
				ilRc = RC_OVERFLOW;
		}	
	}

	close_my_cursor(&slCursor);
	slCursor = 0;

	dbg ( TRACE, "ReadMissingData: Table <%s> Urno <%s> Fields <%s> -->", pcpTable, pcpUrno, pcpFields );
	dbg ( TRACE, "ReadMissingData: RC <%d> Data <%s>", ilRc, pcpDataList );
	return ilRc;
}

static int BuildOutEventSender ( char *pcpAddFields, char *pcpAddData )
{
	int		ilRc = RC_SUCCESS;
	BC_HEAD *prlBchead    = NULL;
	CMDBLK  *prlCmdblk    = NULL;
	char    *pclSelection = NULL;
	char    *pclFields    = NULL;
	char    *pclData      = NULL;
	char    *pclDataSrc    = NULL;
	long    llActSize   = 0;

	llActSize = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + prgEvent->data_length + 10 ;  
	llActSize += strlen(pcpAddFields);
	llActSize += strlen(pcpAddData);

	if ( llActSize > lgOutEventSize)
	{
		prgOutEvent = realloc (prgOutEvent, llActSize) ;
		if (prgOutEvent == NULL)
		{
			dbg (TRACE, "BuildOutEventSender: realloc out event <%ld> bytes failed",llActSize);
			ilRc = RC_FAIL ;
			lgOutEventSize = 0L ;

		} /* end of if */
		else
		{
			dbg (TRACE, "BuildOutEventSender: realloc out event <%ld> bytes ok", llActSize );
			lgOutEventSize = llActSize ;
		}
	} /* end of if */

	if ( ilRc == RC_SUCCESS )
	{
		prlBchead    = (BC_HEAD *) ((char *) prgEvent + sizeof(EVENT)) ;
		prlCmdblk    = (CMDBLK *)  ((char *) prlBchead->data) ;
		pclSelection = prlCmdblk->data ;
		pclFields    = pclSelection + strlen(pclSelection) + 1;
		/*  Only pointer to original Data has to be stored */
		pclDataSrc = pclFields + strlen(pclFields) + 1;

		memcpy (prgOutEvent, prgEvent, prgEvent->data_length + sizeof(EVENT)) ;
		
		prgOutEvent->originator = mod_id;	
		prlBchead    = (BC_HEAD *) ((char *) prgOutEvent + sizeof(EVENT)) ;
		prlCmdblk    = (CMDBLK *)  ((char *) prlBchead->data) ;
		pclSelection = prlCmdblk->data ;
		pclFields    = pclSelection + strlen(pclSelection) + 1;
		if ( pcpAddFields && *pcpAddFields )
		{
			if ( *pclFields )
				strcat ( pclFields, "," );
			strcat ( pclFields, pcpAddFields );
		}
		pclData = pclFields + strlen(pclFields) + 1;
		strcpy ( pclData, pclDataSrc );
		if ( pcpAddData && *pcpAddData )
		{
			if ( *pclData && (*pcpAddData!=cgAttSep[0]) )
				strcat ( pclData, "," );
			strcat ( pclData, pcpAddData );
		}

		prgOutEvent->data_length = llActSize - sizeof(EVENT) ;
		dbg (DEBUG, "BuildOutEventSender: event->data_length  <%ld> llActSize <%ld>", 
					 prgOutEvent->data_length, llActSize) ;              
		dbg ( TRACE, "BuildOutEventSender: Fields <%s>", pclFields );
		dbg ( TRACE, "BuildOutEventSender: Data   <%s>", pclData );
	}
	return ilRc;
}


static int FindNewUrno ( char *pcpTable, char *pcpKeyFlds, char *pcpFields, 
						 char *pcpData, char *pcpUrno )
{
	char	pclDataArea[1200];
	char	pclSqlBuf[1200], clTmp[500];
	short	slCursor = 0;
	int		ilNoReq=0, i, ilRc=RC_SUCCESS;
	char	clFina[12], clAdid=' ';
	
	pcpUrno[0] = '\0';

	sprintf(pclSqlBuf, "SELECT URNO FROM %s WHERE", pcpTable );
	ilNoReq = get_no_of_items(pcpKeyFlds);

	for ( i=1; (ilRc==RC_SUCCESS) && (i<=ilNoReq); i++ )
	{
		if(get_real_item(clFina,pcpKeyFlds,i) > 0)
		{
			ilRc = GetFieldData ( pcpFields, pcpData, clFina, pclDataArea );
			if ( ilRc == RC_SUCCESS )
			{
				if ( strstr ( pcpTable, "AFT" ) )
				{
					if ( strcmp ( clFina, "ADID" ) == 0 )
						clAdid = *pclDataArea;
					if ( ( clAdid=='A' ) && ( strcmp ( clFina, "STOD" ) == 0 ) )
						continue;	/* STOD may be different for arrival on both systems */
					if ( ( clAdid=='B' ) && ( strcmp ( clFina, "STOA" ) == 0 ) )
						continue;	/* STOA may be different for return flights on both systems */
					if ( ( clAdid=='D' ) && ( strcmp ( clFina, "STOA" ) == 0 ) )
						continue;	/* STOA may be different for departures on both systems */
				}
				if ( i>1 )
					strcat ( pclSqlBuf, " AND" );
				sprintf ( clTmp, " %s='%s'", clFina, pclDataArea );
				strcat ( pclSqlBuf, clTmp );
			}
			else
			{
				dbg ( TRACE, "FindNewUrno; No data for key field <%s> in data list <%s>", 
					  clFina, pcpData );
				ilRc = RC_NOT_FOUND;
			}
		}
		else
		{
			dbg ( TRACE, "FindNewUrno; Cannot extract field %d/%d from <%s>", 
				  i, ilNoReq, pcpKeyFlds );
			ilRc = RC_FAIL;
		}

	}	
	if ( ilRc == RC_SUCCESS )
	{
		ilRc = sql_if(START,&slCursor,pclSqlBuf,pclDataArea);
		if (ilRc != RC_SUCCESS)
		{
			dbg(TRACE,"FindNewUrno: Reading with selection <%s> failed, RC <%d>", pclSqlBuf, ilRc );
			check_ret(ilRc);
		} 
	}
	if ( ilRc == RC_SUCCESS )
	{
		strcpy ( pcpUrno, pclDataArea );
		dbg(TRACE,"FindNewUrno: Found URNO <%s>", pcpUrno );
	}
	else
		dbg(TRACE,"FindNewUrno: unable to find new URNO for <%s> <%s>", pcpFields, pcpData );

	close_my_cursor(&slCursor);
	slCursor = 0;

	return ilRc;
}


static int GetFieldData ( char *pcpFields, char *pcpData, char *pcpFld, char *pcpFldDat )
{
	int ilItemNo, ilRc = RC_SUCCESS;
			
	ilItemNo = get_item_no(pcpFields, pcpFld, 5);
	
	if (ilItemNo < 0)
	{
		dbg ( TRACE, "GetFieldData; Field <%s> missing in field list <%s>", 
			  pcpFld, pcpFields );
		ilRc = RC_NOT_FOUND;
	}
	else
	{
		ilItemNo++;
		if ( ilItemNo <= get_no_of_items ( pcpData ) )
		{
			get_real_item(pcpFldDat,pcpData,ilItemNo);
			if ( *pcpFldDat == '\0' )
				strcpy ( pcpFldDat, " " );
			dbg ( DEBUG, "GetFieldData; Found field <%s> Data <%s>", pcpFld, pcpFldDat );
		}
		else
		{
			dbg ( TRACE, "GetFieldData; No data for field <%s> in data list <%s>", pcpFld, pcpData );
			ilRc = RC_NOT_FOUND;
		}
	}
	return ilRc;
}

static int AddRelatedRecords ( char *pcpTab, char *pcpFlds1, char *pcpDat1, char *pcpFlds2, char *pcpDat2 )
{
	char clEntry[10], clPatchFields[101], clKeyFields[101], clBuffer[121];
	char clTable[11], *pclTmp, clSinglePatch[41], clDataList[1024];
	char clFina[11], clUrno[21], *pclDel;
	int ilRc, ilRc1, ilExamined, ilTabCnt;
	int blFound;

	if ( !pcpTab || !pcpFlds1 || !pcpDat1 || !pcpFlds2 || !pcpDat2 )
	{
		dbg ( TRACE, "AddRelatedRecords: At least one parameter is null");
		return RC_FAIL;
	}

	strncpy ( clEntry, pcpTab, 3 );
	clEntry[3] = '\0';
	clPatchFields[0] = '\0';

	ilRc = iGetConfigRow(cgConfigFile, "ADDPATCH", clEntry, CFG_STRING, clPatchFields);
	if ( ( ilRc != RC_SUCCESS ) || ( strlen(clPatchFields) < 4 ) ) 
	{
		clKeyFields[0] = '\0';
		dbg ( DEBUG, "AddRelatedRecords: no addition fields configured for table <%s>", pcpTab );
		ilRc = RC_SUCCESS;
	}
	else
	{
		/*  Example for config entry: CCA = FLNU:AFT,ALC;GHPU:GHP;GHSU:GHS;GPMU:GPM */
		/*  means: fields to patch in CCA: FLNU is URNO from AFTTAB or ALCTAB,		*/
		/*	GHPU is URNO from GHPTAB etc.											*/
		dbg ( DEBUG, "AddRelatedRecords: patch-config <%s> for table <%s>", clPatchFields, pcpTab );
		pclTmp = clPatchFields;
		  
		while(GetNextDataItem(clSinglePatch,&pclTmp,";","","  ") > 0)
		{
			dbg ( DEBUG, "AddRelatedRecords: field to patch <%s>", clSinglePatch );
			strncpy ( clFina, clSinglePatch, 4 );
			clFina[4] = '\0';
			clUrno[0] = '\0';
			clDataList[0] = '\0';

			ilRc1 = GetFieldData ( pcpFlds1, pcpDat1, clFina, clUrno );
			if ( ilRc1 != RC_SUCCESS )
				ilRc1 = GetFieldData ( pcpFlds2, pcpDat2, clFina, clUrno );
			if ( ilRc1 != RC_SUCCESS )
				dbg ( TRACE, "AddRelatedRecords: field <%s> not in event's field list", clFina );
			else if ( atol(clUrno) <= 0 )
			{
				dbg ( TRACE, "AddRelatedRecords: number <%s> in field <%s> is 0", clUrno, clFina );
				ilRc1 = RC_FAIL;
			}
			if ( ilRc1 ==  RC_SUCCESS )
			{
				clTable[0] = '\0';
				ilTabCnt = 0;
				if ( !strncmp ( pcpTab, "BLK", 3 ) )
				{
					ilRc = GetFieldData ( pcpFlds1, pcpDat1, "TABN", clTable );
					if ( ilRc != RC_SUCCESS )
					{
						ilRc = GetFieldData ( pcpFlds2, pcpDat2, "TABN", clTable );
					}
					if ( ilRc == RC_SUCCESS )
						ilTabCnt = 1;
				}
				else
				{
					if ( pclDel = strchr ( clSinglePatch, ':' ) )
					{
						ilTabCnt = get_no_of_items(pclDel+1);
						if( get_real_item(clTable, pclDel+1,1) <= 0 )
							ilTabCnt = 0;
					}
				}
				ilExamined = 0;
				blFound = FALSE;
				while ( !blFound && (ilExamined < ilTabCnt) )
				{
					ilExamined++;
					sprintf ( &(clTable[3]), cgTabEnd );
					ilRc1 = GetKeyFields ( "URT", clTable, clKeyFields );	
					if ( ilRc1 != RC_SUCCESS )
						dbg ( TRACE, "AddRelatedRecords: didn't find key fields for table <%s>", clTable );
					else
					{
						dbg ( DEBUG, "AddRelatedRecords: found key fields <%s> for table <%s>", clKeyFields, clTable );
					}
					if ( ilRc1 == RC_SUCCESS )
					{
						ilRc1 = ReadMissingData ( clTable, clUrno, clKeyFields, clDataList );
						if ( ilRc1 == RC_SUCCESS )
						{
							blFound = TRUE;
							dbg ( DEBUG, "AddRelatedRecords: Loaded data <%s> for <%s>=<%s>", clDataList, 
								  clFina, clUrno );
						}
						else
							dbg ( TRACE, "AddRelatedRecords: No data found for <%s>=<%s>", clFina, clUrno );

					}
					if ( !blFound && (ilExamined < ilTabCnt) )
					{
						if( get_real_item(clTable, pclDel+1,ilExamined+1) <= 0 )
						{
							dbg ( TRACE, "AddRelatedRecords: error reading %d. table from <%s>",
							ilExamined+1, pclDel+1 );	  
							ilExamined = ilTabCnt;
						}
					}
				}
				if ( !blFound )
					ilRc = RC_NOT_FOUND;
				else
				{
					sprintf ( clBuffer, "%s%s:%s;%s;", cgAttSep, clFina, clTable, clKeyFields );
					strcat ( pcpDat2, clBuffer );
					strcat ( pcpDat2, clDataList );
				}
			}
		}
	}
	return ilRc;
}

static int DoAddPatch ( char *pcpTab, char *pcpFlds, char *pcpDat, char *pcpPatchedDat )
{
	int ilRc, ilRc1, ilItemNo  ;
	char clEntry[10], clFina[11], clPatchFields[101], clOldUrno[21], clTable[11];
	char *pclTmp, *pclPtr1;
	char clFieldlist[101], clDatalist[1024], clUrno[21], clSinglePatch[41];

	strncpy ( clEntry, pcpTab, 3 );
	clEntry[3] = '\0';
	clPatchFields[0] = '\0';

	strcpy ( pcpPatchedDat, pcpDat );

	ilRc = iGetConfigRow(cgConfigFile, "ADDPATCH", clEntry, CFG_STRING, clPatchFields);
	if ( ( ilRc != RC_SUCCESS ) || ( strlen(clPatchFields) < 4 ) ) 
	{
		dbg ( DEBUG, "DoAddPatch: no addition fields configured for table <%s>", pcpTab );
		ilRc = RC_SUCCESS;
	}
	else
	{
		dbg ( DEBUG, "DoAddPatch: patch-config <%s> for table <%s>", clPatchFields, pcpTab );
		pclTmp = clPatchFields;
		  
		while(GetNextDataItem(clSinglePatch,&pclTmp,";","","  ") > 0)
		{
			strncpy ( clFina, clSinglePatch, 4 );
			clFina[4] = '\0';

			clUrno[0] = '\0';
			clDatalist[0] = '\0';

			ilItemNo = get_item_no(pcpFlds, clFina, 5);
			if ( ilItemNo < 0 )
				dbg ( DEBUG, "DoAddPatch; Field <%s> missing in field list <%s>", 
					  clFina, pcpFlds );
			else
			{
				dbg ( DEBUG, "DoAddPatch: field to patch <%s>", clFina );
				/*  looking for appended record for patching of this field */
				sprintf ( clEntry, "%s%s:", cgAttSep, clFina );
				pclPtr1 = strstr ( pcpDat, clEntry );
				if ( !pclPtr1 )
					dbg ( TRACE, "DoAddPatch: no add data found with <%s>", clEntry );
				else
				{
					ilRc1 = RC_SUCCESS;
					pclPtr1 += strlen ( clEntry );
					if ( GetNextDataItem(clTable,&pclPtr1,";","","  ") < 0)
					{
						dbg ( TRACE, "DoAddPatch: unable to extract table from <%s>", pclPtr1 );
						ilRc1 = RC_FAIL;
					}
					else if ( GetNextDataItem(clFieldlist,&pclPtr1,";","","  ") < 0)
					{
						dbg ( TRACE, "DoAddPatch: unable to extract field list from <%s>", pclPtr1 );
						ilRc1 = RC_FAIL;
					}
					else if ( GetNextDataItem(clDatalist,&pclPtr1,cgAttSep,"","  ") < 0)
					{
						dbg ( TRACE, "DoAddPatch: unable to extract data list from <%s>", pclPtr1 );
						ilRc1 = RC_FAIL;
					}
					if ( ilRc1 == RC_SUCCESS )
					{
						ilRc1 = FindNewUrno ( clTable, clFieldlist, clFieldlist, clDatalist, clUrno );
						if ( ilRc1 != RC_SUCCESS )
							dbg ( TRACE, "DoAddPatch: unable to read new URNO using <%s> <%s>", 
								  clFieldlist, clDatalist );
					}	
				}
				pclPtr1 = strstr ( pcpPatchedDat, clEntry ); 
				/* cut appended Record(s) from new data string */
				if ( pclPtr1 )
					*pclPtr1 = '\0';
				if ( clUrno[0] )
				{	/* we found the new URNO */
					if ( get_real_item(clOldUrno,pcpDat,ilItemNo+1) >= 0)
					{
						dbg ( DEBUG, "DoAddPatch: <%s> -> <%s>", clOldUrno, clUrno );
						ilRc1 = SearchStringAndReplace(pcpPatchedDat, clOldUrno, clUrno );
						if ( ilRc1 != RC_SUCCESS )
							dbg ( TRACE, "DoAddPatch: SearchStringAndReplace failed RC <%d>", ilRc1 );
					}
					else
						ilRc = RC_FAIL;
				}
				if ( ilRc1 != RC_SUCCESS )
					ilRc = RC_FAIL;
			}
		}
		dbg ( TRACE, "DoAddPatch: finished RC <%d> old data <%s> new data <%s>", ilRc, pcpDat, pcpPatchedDat );
	}
		
	return ilRc;

}
				

/********************************************************************/
static int GetNextUrno(char *pcpUrno)
{
	int ilRC = RC_SUCCESS ;
	char  pclDataArea[IDATA_AREA_SIZE] ;
	
	if (igReservedUrnoCnt <= 0)
	{                            /* hole Urno buffer -> get new Urnos */
		memset( pclDataArea, 0, IDATA_AREA_SIZE) ;
		if ((ilRC = GetNextValues(pclDataArea, URNOS_TO_FETCH)) != RC_SUCCESS)
		{
			dbg ( TRACE,"GetNextValues failed RC <%d>", ilRC );
		}
		else
		{
			strcpy ( pcpUrno, pclDataArea );
			igReservedUrnoCnt = URNOS_TO_FETCH - 1 ;
			lgActUrno = atol(pcpUrno);
			dbg ( DEBUG, "GetNextUrno: fetched %d Urnos. Next is <%s>", 
				  URNOS_TO_FETCH, pcpUrno ) ;
		}
	}
	else
	{
		igReservedUrnoCnt-- ;
		lgActUrno++ ;
		sprintf ( pcpUrno, "%ld", lgActUrno );      /* get next reserved Urno */
		dbg ( DEBUG, "GetNextUrno: Next Urno <%s>, %d Urnos remaining", 
			  pcpUrno, igReservedUrnoCnt ) ;
	}
	
	return (ilRC) ;
} /* getNextUrno () */


static int ReplaceUrno ( char *pcpSel, char *pcpFlds, char *pcpDat, char *pcpUrno )
{
	char clOldUrno[21]="";
	int ilRc = RC_SUCCESS, ilRc1=RC_SUCCESS;

	GetUrnoFromSelection ( pcpSel, clOldUrno );
	if ( *clOldUrno )
		ilRc = SearchStringAndReplace( pcpSel, clOldUrno, pcpUrno );
	if ( ilRc != RC_SUCCESS )
		dbg ( TRACE, "ReplaceUrno: substitution of <%s> by <%s> in selection <%s> failed!",
			clOldUrno, pcpUrno, pcpSel );
	if ( GetFieldData ( pcpFlds, pcpDat, "URNO", clOldUrno ) == RC_SUCCESS )
		ilRc1 = SearchStringAndReplace( pcpDat, clOldUrno, pcpUrno );
	if ( ilRc1 != RC_SUCCESS )
		dbg ( TRACE, "ReplaceUrno: substitution of <%s> by <%s> in data <%s> failed!",
			clOldUrno, pcpUrno, pcpDat );
	ilRc |= ilRc1;
	dbg ( DEBUG, "ReplaceUrno: finished RC <%d>", ilRc );
	return ilRc;
}



static int BuildOutEventPatcher ( char *pcpSel, char *pcpData )
{
	int		ilRc = RC_SUCCESS, ilOld, ilNew;
	BC_HEAD *prlBchead    = NULL;
	CMDBLK  *prlCmdblk    = NULL;
	char    *pclSelection = NULL;
	char    *pclFields    = NULL;
	char    *pclData      = NULL;
	char    *pclDataSrc   = NULL;
	char    *pclFieldsSrc =NULL;
	long    llActSize   = 0;

	prlBchead    = (BC_HEAD *) ((char *) prgEvent + sizeof(EVENT)) ;
	prlCmdblk    = (CMDBLK *)  ((char *) prlBchead->data) ;
	pclSelection = prlCmdblk->data ;
	pclFieldsSrc    = pclSelection + strlen(pclSelection) + 1;
	pclDataSrc = pclFieldsSrc + strlen(pclFieldsSrc) + 1;

	ilOld = strlen(pclSelection) + strlen(pclDataSrc);
	ilNew = strlen(pcpSel) + strlen(pcpData);
	
	llActSize = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + prgEvent->data_length + 10 ; 
	if ( ilOld < ilNew )
	{
		llActSize -= ilOld;
		llActSize += ilNew;
	}
	dbg ( DEBUG, "BuildOutEventPatcher: ilOld <%d> ilNew <%d> llActSize <%ld>", ilOld, ilNew, llActSize );

	if ( llActSize > lgOutEventSize)
	{
		prgOutEvent = realloc (prgOutEvent, llActSize) ;
		if (prgOutEvent == NULL)
		{
			dbg (TRACE, "BuildOutEventPatcher: realloc out event <%ld> bytes failed",llActSize);
			ilRc = RC_FAIL ;
			lgOutEventSize = 0L ;

		} /* end of if */
		else
		{
			dbg (TRACE, "BuildOutEventPatcher: realloc out event <%ld> bytes ok", llActSize );
			lgOutEventSize = llActSize ;
		}
	} /* end of if */

	if ( ilRc == RC_SUCCESS )
	{
		memcpy (prgOutEvent, prgEvent, prgEvent->data_length + sizeof(EVENT)) ;
		
		prgOutEvent->originator = mod_id;	
		prlBchead    = (BC_HEAD *) ((char *) prgOutEvent + sizeof(EVENT)) ;
		prlCmdblk    = (CMDBLK *)  ((char *) prlBchead->data) ;
		pclSelection = prlCmdblk->data ;
		strcpy ( pclSelection, pcpSel );
		pclFields    = pclSelection + strlen(pclSelection) + 1;
		strcpy ( pclFields, pclFieldsSrc );
		pclData = pclFields + strlen(pclFields) + 1;
		strcpy ( pclData, pcpData );

		prlBchead->rc = NETOUT_NO_ACK; /* no acknowledge wanted! */

		prgOutEvent->data_length = llActSize - sizeof(EVENT) ;
		dbg (DEBUG, "BuildOutEventPatcher: event->data_length  <%ld> llActSize <%ld>", 
					 prgOutEvent->data_length, llActSize) ;              
		dbg ( TRACE, "BuildOutEventPatcher: Selection <%s>", pclSelection );
		dbg ( TRACE, "BuildOutEventPatcher: Fields <%s>", pclFields );
		dbg ( TRACE, "BuildOutEventPatcher: Data   <%s>", pclData );
	}
	return ilRc;
}


static int BuildSPRSelection ( char *pcpKeyFlds, char *pcpFlds, char *pcpDat, char *pcpNewSel )
{
	int ilRc ;
	char clUrno[24], clDataArea[101];

	ilRc = FindNewUrno ( "AFTTAB", pcpKeyFlds, pcpFlds, pcpDat, clUrno );
	if ( ilRc == RC_SUCCESS )
		ilRc = ReadMissingData ( "AFTTAB", clUrno, "RKEY", clDataArea );
	if ( ilRc == RC_SUCCESS )
		sprintf ( pcpNewSel, "%s,%s", clUrno, clDataArea );
	dbg ( DEBUG, "BuildSPRSelection: SEL <%s> RC <%d>", pcpNewSel, ilRc );
	return ilRc;
}

static int AddJOFRecords ( char *pcpSel, char *pcpKeyFlds, char *pcpData )
{
	int ilNumbers, i, ilRc = RC_SUCCESS;
	char clValue[24];
	char clDataList[1024];
	int  ilUrnosFound = 0;
	
	if(get_real_item(clValue,pcpSel,1) > 0)
	{
		if ( atol(clValue) > 0 )		/* real URNO to patch */
		{
			ilRc = ReadMissingData ( "AFTTAB", clValue, pcpKeyFlds, clDataList );
			if ( ilRc == RC_SUCCESS )
			{
				strcat ( pcpData, cgAttSep );
				strcat ( pcpData, "SEL1:" );
				strcat ( pcpData, pcpKeyFlds );
				strcat ( pcpData, ";" );
				strcat ( pcpData, clDataList );
				ilUrnosFound++;
			}
		}
	}
	if(get_real_item(clValue,pcpSel,3) > 0)
	{
		if ( atol(clValue) > 0 )		/* real URNO to patch */
		{
			ilRc = ReadMissingData ( "AFTTAB", clValue, pcpKeyFlds, clDataList );
			if ( ilRc == RC_SUCCESS )
			{
				strcat ( pcpData, cgAttSep );
				strcat ( pcpData, "SEL3:" );
				strcat ( pcpData, pcpKeyFlds );
				strcat ( pcpData, ";" );
				strcat ( pcpData, clDataList );
				ilUrnosFound++;
			}
		}
	}
	ilRc = (ilUrnosFound==2) ? RC_SUCCESS : RC_FAIL;
	
	dbg ( TRACE, "AddJOFRecords: KeyFields <%s> Selection  <%s> RC <%d>", 
		  pcpKeyFlds, pcpSel, ilRc );
	return ilRc;
}


static int HandleJOF ( char *pcpKeyFlds, char *pcpSel, char *pcpData, char *pcpOutData )
{
	int ilRc1, ilRc2;
	char *pclPtr1, *pclCutAt=0;
	char clFieldList[1024], clDataList[1024];
	char clUrno1[24]="", clUrno2[24]="", clRkey1[24]="", clRkey2[24]="";
	char clSel1Str[10], clSel3Str[10];

	sprintf ( clSel1Str,  "%sSEL1:", cgAttSep );
	sprintf ( clSel3Str,  "%sSEL3:", cgAttSep );
	
	strcpy ( pcpOutData, pcpData );

	ilRc1 = ilRc2 = RC_SUCCESS;

	pclPtr1 = strstr ( pcpOutData, clSel1Str );
	if ( pclPtr1 )
	{
		pclCutAt = pclPtr1;
		pclPtr1 += strlen ( clSel1Str );
		if ( GetNextDataItem(clFieldList,&pclPtr1,";","","  ") < 0)
		{
			dbg ( TRACE, "HandleJOF: unable to extract 1st field list from <%s>", pclPtr1 );
			ilRc1 = RC_FAIL;
		}
		if ( GetNextDataItem(clDataList,&pclPtr1,cgAttSep,"","  ") < 0)
		{
			dbg ( TRACE, "HandleJOF: unable to extract 1st data list from <%s>", pclPtr1 );
			ilRc1 = RC_FAIL;
		}
		if ( ilRc1 == RC_SUCCESS )
			ilRc1 = FindNewUrno ( "AFTTAB", pcpKeyFlds, clFieldList, clDataList, clUrno1 );
		if ( ilRc1 == RC_SUCCESS )
			ilRc1 = ReadMissingData ( "AFTTAB", clUrno1, "RKEY", clRkey1 );
		if ( ilRc1 != RC_SUCCESS )
			dbg ( TRACE, "HandleJOF: Unable to find 1st flight Data <%s>", pcpData );
		else
		{
			dbg ( DEBUG, "HandleJOF: Found 1st flight URNO <%s> RKEY <%s>", clUrno1, clRkey1 );
			if ( strcmp ( clUrno1, clRkey1 ) == 0 )
				strcpy ( clRkey1, "0" );
		}
	}
	pclPtr1 = strstr ( pcpOutData, clSel3Str );
	if ( pclPtr1 )
	{
		pclPtr1 += strlen ( clSel3Str );
		if ( GetNextDataItem(clFieldList,&pclPtr1,";","","  ") < 0)
		{
			dbg ( TRACE, "HandleJOF: unable to extract 2nd field list from <%s>", pclPtr1 );
			ilRc2 = RC_FAIL;
		}
		if ( GetNextDataItem(clDataList,&pclPtr1,cgAttSep,"","  ") < 0)
		{
			dbg ( TRACE, "HandleJOF: unable to extract 2nd data list from <%s>", pclPtr1 );
			ilRc2 = RC_FAIL;
		}
		if ( ilRc2 == RC_SUCCESS )
			ilRc2 = FindNewUrno ( "AFTTAB", pcpKeyFlds, clFieldList, clDataList, clUrno2 );
		if ( ilRc2 == RC_SUCCESS )
			ilRc2 = ReadMissingData ( "AFTTAB", clUrno2, "RKEY", clRkey2 );
		if ( ilRc2 != RC_SUCCESS )
			dbg ( TRACE, "HandleJOF: Unable to find 2nd flight Data <%s>", pcpData );
		else
		{
			dbg ( DEBUG, "HandleJOF: Found 2nd flight URNO <%s> RKEY <%s>", clUrno2, clRkey2 );
			if ( strcmp ( clUrno2, clRkey2 ) == 0 )
				strcpy ( clRkey2, "0" );
		}
	}
	if ( pclCutAt )
		*pclCutAt = '\0';
	if ( *clUrno1 && *clUrno2 )
	{
		sprintf ( pcpSel, "%s,%s,%s,%s", clUrno1, clRkey1, clUrno2, clRkey2 ); 
		dbg ( TRACE, "HandleJOF: Patched selection successful to <%s>", pcpSel );
		return RC_SUCCESS;
	}
	else
	{
		dbg ( TRACE, "HandleJOF: Patch of selection <%s> failed, Urno1 <%s> Urno2 <%s>", 
			  pcpSel, clUrno1, clUrno2 );
		return RC_FAIL;
	}
}


static int BuildOutEvent ( char *pcpSel, char *pcpFld, char *pcpDat )
{
	int		ilRc = RC_SUCCESS;
	BC_HEAD *prlBchead    = NULL;
	CMDBLK  *prlCmdblk    = NULL;
	char    *pclSelection = NULL;
	char    *pclFields    = NULL;
	char    *pclData      = NULL;
	long    llActSize   = 0;

	llActSize = sizeof(EVENT) + sizeof(BC_HEAD) + sizeof(CMDBLK) + 10 ;  
	llActSize += strlen ( pcpSel );
	llActSize += strlen ( pcpFld );
	llActSize += strlen ( pcpDat );

	if ( llActSize > lgOutEventSize)
	{
		prgOutEvent = realloc (prgOutEvent, llActSize) ;
		if (prgOutEvent == NULL)
		{
			dbg (TRACE, "BuildOutEvent: realloc out event <%ld> bytes failed",llActSize);
			ilRc = RC_FAIL ;
			lgOutEventSize = 0L ;

		} /* end of if */
		else
		{
			dbg (TRACE, "BuildOutEvent: realloc out event <%ld> bytes ok", llActSize );
			lgOutEventSize = llActSize ;
		}
	} /* end of if */

	if ( ilRc == RC_SUCCESS )
	{
		prlBchead    = (BC_HEAD *) ((char *) prgEvent + sizeof(EVENT)) ;
		prlCmdblk    = (CMDBLK *)  ((char *) prlBchead->data) ;

		memcpy (prgOutEvent, prgEvent, llActSize-1 );
		
		prgOutEvent->originator = mod_id;	
		prlBchead    = (BC_HEAD *) ((char *) prgOutEvent + sizeof(EVENT)) ;
		prlCmdblk    = (CMDBLK *)  ((char *) prlBchead->data) ;
		pclSelection = prlCmdblk->data ;
		strcpy ( pclSelection, pcpSel );
		pclFields    = pclSelection + strlen(pclSelection) + 1;
		strcpy ( pclFields, pcpFld );
		pclData = pclFields + strlen(pclFields) + 1;
		strcpy ( pclData, pcpDat );

		prgOutEvent->data_length = llActSize - sizeof(EVENT) ;
		dbg (DEBUG, "BuildOutEvent: event->data_length  <%ld> llActSize <%ld>", 
					 prgOutEvent->data_length, llActSize) ;              
		dbg ( TRACE, "BuildOutEvent: Sel    <%s>", pclSelection );
		dbg ( TRACE, "BuildOutEvent: Fields <%s>", pclFields );
		dbg ( TRACE, "BuildOutEvent: Data   <%s>", pclData );
	}
	return ilRc;
}

static int AddRelatedISFRecord ( char *pcpSel, char *pcpKeyFlds, char *pcpData )
{
	int ilNumbers, i, ilRc = RC_SUCCESS;
	char clUrno[24]="", *pclDel;
	char clDataList[1024];
	int  ilUrnosFound = 0;
	
	pclDel = strchr ( pcpSel, '\n' );
	if ( pclDel )
		strcpy ( clUrno, pclDel+1 );
	if ( strlen ( clUrno ) < 1 )
	{
		dbg ( TRACE, "AddRelatedISFRecord: Couldn't find URNO in Selection <%s>", pcpSel );
		ilRc = RC_FAIL;
	}
	if ( ilRc == RC_SUCCESS )
	{
		ilRc = ReadMissingData ( "AFTTAB", clUrno, pcpKeyFlds, clDataList );
		if ( ilRc == RC_SUCCESS )
		{
			strcat ( pcpData, cgAttSep );
			strcat ( pcpData, "SEL2:" );
			strcat ( pcpData, pcpKeyFlds );
			strcat ( pcpData, ";" );
			strcat ( pcpData, clDataList );
		}
	}
	dbg ( TRACE, "AddRelatedISFRecord: KeyFields <%s> Selection  <%s> RC <%d>", 
		  pcpKeyFlds, pcpSel, ilRc );
	return ilRc;
}


static int HandleSpecialISF ( char *pcpKeyFlds, char *pcpSel, char *pcpData, char *pcpOutData )
{
	int ilRc1;
	char *pclPtr, *pclCutAt=0, *pclSelDel=0;
	char clFieldList[1024], clDataList[1024];
	char clUrno1[24]="";
	char clSel1Str[10];

	sprintf ( clSel1Str,  "%sSEL2:", cgAttSep );
	
	strcpy ( pcpOutData, pcpData );

	ilRc1 = RC_SUCCESS;

	pclPtr = strstr ( pcpOutData, clSel1Str );
	if ( pclPtr )
	{
		pclCutAt = pclPtr;
		pclPtr += strlen ( clSel1Str );
		if ( GetNextDataItem(clFieldList,&pclPtr,";","","  ") < 0)
		{
			dbg ( TRACE, "HandleSpecialISF: unable to extract rel. field list from <%s>", pclPtr );
			ilRc1 = RC_FAIL;
		}
		if ( GetNextDataItem(clDataList,&pclPtr,cgAttSep,"","  ") < 0)
		{
			dbg ( TRACE, "HandleSpecialISF: unable to extract rel. data list from <%s>", pclPtr );
			ilRc1 = RC_FAIL;
		}
		if ( ilRc1 == RC_SUCCESS )
			ilRc1 = FindNewUrno ( "AFTTAB", pcpKeyFlds, clFieldList, clDataList, clUrno1 );
		if ( ilRc1 != RC_SUCCESS )
			dbg ( TRACE, "HandleSpecialISF: Unable to find related flight Data <%s>", pcpData );
		else
		{
			dbg ( DEBUG, "HandleSpecialISF: Found related flight URNO <%s> ", clUrno1 );
		}
	}
	if ( pclCutAt )
		*pclCutAt = '\0';
	if ( *clUrno1 )
	{
		pclSelDel = strchr(pcpSel, '\n' ) ;
	}
	if ( pclSelDel ) 
	{
		strcpy ( pclSelDel+1, clUrno1 );	
		dbg ( TRACE, "HandleSpecialISF: Patched selection successful to <%s>", pcpSel );
		return RC_SUCCESS;
	}
	else
	{
		dbg ( TRACE, "HandleSpecialISF: Patch of selection <%s> failed, Urno1 <%s>",  pcpSel, clUrno1 );
		return RC_FAIL;
	}
}

static int GetOriDest4Cmd ( char *pcpCmd )
{
	char clEntry[21];
	int ilDest = 0;

	int ilRc = ReadConfigEntry("CMDDEST",pcpCmd, clEntry);
	if ( ilRc == RC_SUCCESS )
		ilDest = atoi ( clEntry );
	if ( ilDest <= 0 )
	{
		if ( strstr ( AFTTAB_CMD, pcpCmd ) )
			ilDest = igFlightID;
		else
			ilDest = igSqlhdlID;
	}
	return ilDest;
}


static int IsToTransfer ( char *pcpCmd, char *pcpTable )
{
	char clEntry[24], clValue[24]="";
	int ilRc;

	if ( !pcpCmd || !pcpTable )
	{
		dbg ( TRACE, "IsToTransfer: At least one parameter is null");
		return RC_INIT_FAIL;
	}
	if ( strlen ( pcpTable ) < 3 )
	{
		dbg ( TRACE, "IsToTransfer: Table not filled");
		return RC_INIT_FAIL;
	}
	
	strncpy ( clEntry, pcpTable, 3 );
	sprintf ( &(clEntry[3]), "_%s", pcpCmd );

	ilRc = iGetConfigRow(cgConfigFile, "TRANSFER", clEntry, CFG_STRING, clValue );
	if ( ilRc != RC_SUCCESS )
	{
		dbg ( DEBUG, "IsToTransfer: no configuration for table <%s> command <%s>", pcpTable, pcpCmd );
	}
	if ( strcmp ( clValue, "NO" ) == 0 )
	{
		dbg ( TRACE, "IsToTransfer: Found Entry <%s> Value <%s> -> NO TRANSFER", clEntry, clValue );
		return RC_IGNORE;
	}
	else
		return RC_SUCCESS;
}
