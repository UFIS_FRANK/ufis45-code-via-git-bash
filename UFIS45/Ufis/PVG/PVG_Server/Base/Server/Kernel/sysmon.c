#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] = "@(#) "UFIS_VERSION" $Id: Ufis/PVG/PVG_Server/Base/Server/Kernel/sysmon.c 1.2 2005/09/20 16:26:11SGT mcu Exp  $";
#endif /* _DEF_mks_version */
#define         UGCCS_PRG
#define		 U_MAIN

/*$:
---------------------------------------------------------------

	S Y S T E M  S T A R T U P  P R O G R A M M

---------------------------------------------------------------

Version         : 0.0   June 1989       Juli 1993   Oct 1993 
Programmer      : KJK
Name            : SYSTRT

Letzte Aenderung:1. Parametercheck 'main' auf 2 oder 3 Para-
		     meter. Bei 3 zusaetzlich auf 'd'.(Jel/10/93)
		 2. Ausgabe des Prozessnamen bei Spawn Error. (Jel/10/93) 
		 3. Statusuebergabe (Master/Slave: ctrl_sta (1/0))
		    (Jel/10/93)
   20010831: dbg benoetigt __CedaLogFile fuer das Auto-Switchen des Logs   
   20011203: - exit forked process in case of error, parent has to clean up 
             - kill_kids as void, to avoid compiler warning
             - send_message declared
             - ignore SIGTERM in case of shutdown
             - after shutdown timeout kill remaining childs
             - read shutdown timeout from environment SDTIMOUT to sync
               with sysqcp
             - sccs_... string with compile date and time stamp
   20021213 JIM: on Linux (RedHat, Kernel 2.4.9) a SIG_TERM calls sig_kill
                 but resumes to wait(). wait() only returns for SIG_CHLD
   20040129 JIM: allow storing core files of childs in directories:
                 try DBG_PATH/procinfo/proc_name, DBG_PATH/procinfo/ and DBG_PATH
   20040618 JIM: allow group to read core files
   20040701 JIM: debug errno and errmsg (if available) when restart child failed
             
	File formats:
	-------------


	'systab.db1'    internal format

			______________________________  
			| size of system table file  |  type long
			------------------------------
			| current max table no.      |  type int
			------------------------------
			| 1st system table header    |  type TABHDR
			.                            .
			.                            .
			|                            |
			------------------------------
			.                            .
			.                            .
			.                            .
			------------------------------
			| nth system table header    | type TABHDR
			. where n is current max.    .
			. table no.                  .
			|                            |
			------------------------------
			| system table no. 1         |
			.                            .
			|                            |
			------------------------------
			.                            .
			.                            .
			.                            .
			------------------------------
			| system table no. n         |
			. where n is current max.    .
			. table no.                  .
			|                            |
			------------------------------
								     $:*/

/*2+*/

/* Standard - Libraries */
#include <time.h>
#include <stdio.h>
#include <ctype.h>
#include <signal.h>
#include <setjmp.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <errno.h>
#include <fcntl.h>

/* Eigendefinierte System - Libraries */
#include "glbdef.h" /* Globale Definitonen */
#include "sptdef.h" /* Def. f. SPTAB (System Parameter Table) */
#include "uevent.h" /* Def. d. Internal Events                */
#include "quedef.h" /* Def. f. QCP (Queue Control Program)+Queuing Primitives*/
#include "systrt.h" /* Def. f. Systrt.c                       */
#include "pntdef.h" /* Def. d. Struct's f. PNTAB (Prozess Name Table) */
#include "msgno.h"  /* Def. d. Message Handler Message Numbers        */
#include "ugccsma.h"
#include "tools.h"
#include "hsbsub.h"
/*#include "send_tools.h"*/
#include "nmghdl.h"
#include "db_if.h"
#include "syslib.h"

extern  int send_message(int,int,int,int,char*); /*CEDA message handler if*/

/* Konstanten Definition */
#define REPEAT 5                /* Schleifensteuerung f. sleep() */

/* Max. number of Homeairports = Number of systabs */
#define MAX_NO_HOMEAP 1

#define MAXNCKIDS 50

/* Variablen Deklaration */
MYKIDS mykids;                  /* child process structure (see <systrt.h>) */
struct {
	char task_name[24];
	int  MaxStarts;
	int  Starts;
	int  Wait;
	int Fatal;
	long child_pid;
	time_t LastStart;
} NcKids[MAXNCKIDS+1];                  /* child process structure for non ceda programs */

SHRD_MEM shrd_mem;              /* shared memory structure (see <systrt.h>) */
SYSWORK syswork;                /* internal structure      (see <systrt.h>) */
  
PNTREC pntab, *pntrec = &pntab; /* process name record (PNTAB) (see <pntdef.h>)                                    */
static int proc_id;             /* my process id */
static int maxtab;              /* max. system table no. */
static int path_prefix = 0;     /* length of path definition */
static int alarm_ind = 0;       /* set to 1 when alarm */
static int kill_ind = 0;        /* set to 1 when shutdown signal */
static int queues_init = 0;     /* Queues initialized indicator */
static int len = 0;             /* Space for locating memory    */
static int rv = 0;              /* Systrt_to_stedex return value */
static char pcgHomeAirport[MAX_NO_HOMEAP][12];

char    *mod_name = "sysmon";   /*  */
int     mod_id = SYSTRT;        /*  */

/* Definitions for send_stat() */
#define DEATH_REPEAT 10                 /* Max.Account of dead Childs   */
static char sysid1[SYSID_SIZE+1];       /* 1. sysid for send_stat       */
static char devnam1[DEVICEID_SIZE+1];   /* 1. device name for send_stat */
static char sysid2[SYSID_SIZE+1];       /* 2. sysid for send_stat       */
static char devnam2[DEVICEID_SIZE+1];   /* 2. device name for send_stat */

static char runpath[128];
static char corebase[128];
static char corepath[128];

static int txt_no  = 0;                 /* Number of text areas         */
static int txt_pos = 0;                 /* Number of text positions     */
static	int	igRestartPause	= 5;
static	int	igCfgTmp	= 0;
static	char	cfgfile[256];

char    *bbuffer = "  ";                /*  */
char    tbuffer[TSTR_LEN+1];            /*  */

struct tm  *lt;                         /* stringtime structure         */
static time_t t;

int	mod_out = STEIF;                /* queue to stedex              */
int	debug_level = DEBUG;
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE	*outp;*/

static char logfile[256];

/**************************  Function prototype header **********************/
extern  int heart_beat(void ); /* i am still alive */
static  void kill_kids();
static  void sig_kill(int );   /* kill child processes */
static  void sig_chld(int );   /* signal: child processes terminated */
static  int sysfind();
static  int sysread();
static  void init_systrt(void );
static  int check_queue(void );
static int StartNcProcess(int ipPrognum,int ipCount);
static int CheckForNcKids(long child_id);
/****************************************************************************/


extern GBLADR gbladr;           /* global addresses         (see <glbdef.h>) */
STHBCB sthbcb;                  /* system table handler bcb (see <sthdef.h>) */
SRCDES srcdes;                  /* search descriptor        (see <sthdef.h>) */




/*      SHARED MEMORY ACCESS ROUTINES (stbsubr) */


int shm_ass();          /* 'shmget' */
char *shm_acc();        /* 'shmat'  */
int clean();            /* 'shmdt' + 'shmctl' */


/**************************************************************************
**                                                                       **
**      m  a  i  n                                                       **
**                                                                       **
***************************************************************************/
void SetDbgLimits (long lpMaxDbgLines, int ipMaxDbgFileNo);
/* 20040109 JIM: replaced "main(int argc,char **argv)" by define "MAIN" */
MAIN
{
	register SYSWORK *psyswork = &syswork;
	long ret;                      /* Returnwert von sth() */
	int rc;
	register long i = 0l;
	register long x = 0l;
	long y = 0l;
	long child_id = 0l;
	long temp_status = 0l;
	int maxrec;
	char sgsin[30];
	char control[6];
	char in_file[128]; /* pointer to the input file name */
	char cpstr[128];     
	int proc_cntr = 0;                           /* Process counter */
	int ctrl_sta = 1;
	char clSection[64];
	char clKeyword[64];
	char clStatus[64];
	int apcnt=1;
	char buf[12];
	int j=0;
  char *pclErrMsgPtr;

	setpgrp();
  
	memset(logfile,0x00,256);
	sprintf(logfile,"%s/sysmon.log",getenv("DBG_PATH"));
	strcpy(__CedaLogFile,logfile);

	outp = fopen(logfile,"w");
  
	memset(in_file,0x00,128);
	sprintf(in_file,"systab.db1");
  
	signal(SIGINT,SIG_IGN);
  
	/**********************************************************************
	*  PARAMETER-CHECK                                                    * 
	*  Nachfolgend werden die Kommandozeilenparameter ueberprueft.        *
	*  In einem ersten Test wird die Zahl der Parameter (2 || 3) geprueft.*
	*  Im zweiten Test wird bei 3 Parametern der Inhalt des 3.ten Para-   * 
	*  meters geprueft (== 'd' (Debug-Modus)).                            *
	*  Verlaeuft einer der beiden Tests nicht erfolgreich, wird das Pro-  *
	*  gramm mit einer Fehlermeldung auf stderr unterbrochen.             *
	***********************************************************************/

	dbg(DEBUG,"main: checking parameter ...");

	/*
	*
	*  PARAMETER-CHECK_1: Parameteranzahl 2 oder 3 oder 4 ?
	*/

	if ((argc < 4) || (argc > 5))  /* 4:Standardbetrieb; 5:Debug-Modus */ 
	{
		/* Programmaufruf fehlerhaft ==> EXIT(1)        */ 
		if ( argc < 4) /* weniger als 4 Parameter */ 
		{
			dbg(TRACE," SYSTRT - Too few arguments");
			dbg(TRACE," The arguments are:");
			dbg(TRACE," 1. Directory name of CEDA bin files");
			dbg(TRACE," 2. Directory name of CEDA systab.db1");
			dbg(TRACE," 3. Directory name of CEDA runtime");
		} else /* mehr als 5 Parameter */ {
			dbg(TRACE,"SYSTRT - Too much arguments");
			dbg(TRACE," The arguments are:");
			dbg(TRACE," 1. Directory name of UGCCS bin files");
			dbg(TRACE," 2. Directory name of UGCCS systab.db1");
			dbg(TRACE," 3. Directory name of CEDA runtime");
		} /* end if PARAMETER-CHECK_1 */ 
    
		dbg(TRACE,"exit ...");
		exit(1);
    
	} /* end if :  PARAMETER-CHECK_1 */
  
	/*
	*
	* PARAMETER - CHECK_2: 3.ter Parameter != 'd'
	* 
	*/

	if ((argc == 5) && (*argv[4] != 'd')) 
	{
		dbg(TRACE,"SYSTRT - Too much arguments (P3)");
		dbg(TRACE," The arguments are:");
		dbg(TRACE," 1. Directory name of UGCCS bin files");
		dbg(TRACE," 2. Directory name of UGCCS systab.db1");
		dbg(TRACE," 3. Directory name of CEDA runtime");
    
		dbg(TRACE,"exit ...");
		exit(1);
    
	} /* end if PARAMETER-CHECK_2 */ 
  
	/* Programmnamen ueberspringen  */

	argc--; argv++;
  
	/* Pointer initialisieren       */

	psyswork->pshrd_mem = &shrd_mem; 
	psyswork->pmykids = &mykids; 
	psyswork->pgbladr = &gbladr;
	psyswork->pbcb = &sthbcb;
  
	/* get my process id */

	psyswork->pshrd_mem->id = getpid();
  
	/* 1.ten Pfadnamen eintragen (BIN-Dateien) */ 

	strcpy(psyswork->pmykids->path_name,*argv++);
	strcat(psyswork->pmykids->path_name,"/");
  /* 20040129 JIM: allow storing core files in directories */
	strcpy(corebase,getenv("DBG_PATH"));

	path_prefix = strlen(psyswork->pmykids->path_name);
	psyswork->anyptr = (char *) &psyswork->pmykids->path_name[path_prefix];
  
  
	/* Dateinamen (systab.db1) mit 2.tem Pfad verketten und
	*  in 'sgsin' schreiben
	*/

	if ((--argc > 0) && (*argv != NULL)) 
	{
		strcpy(sgsin,*argv++);
		strcat(sgsin,"/");
		strcat(sgsin,in_file);
	} else {
		strcpy(sgsin,in_file);
	} /* end if */
  
	if ((--argc > 0) && (*argv != NULL)) 
	{
		strcat(runpath,*argv++);
		strcat(runpath,"/");
	} else {
		strcpy(runpath,"/tmp");
	}
  
	/* Wurde Debug-Parameter 'd' uebergeben ? */ 

	if ((--argc > 0) && (*argv != NULL)) 
	{
		if (**argv == 'd') 
		{
			debug_level = DEBUG;
		} /* end if  Debug-Modus aktivieren */ 
	} /* end if  Debug-Parameter */
  
	memset(cfgfile,0x00,256);
	sprintf(cfgfile,"%s/hsb.dat",getenv("CFG_PATH"));
  
	dbg(DEBUG,"SYSTRT - Assigning shared memory and reading %s", sgsin);
  
	/* Check Input file (0 = o.k.) */

	if ((ret = sysread(psyswork,sgsin)) !=0) /* process input file */
	{
		dbg(TRACE,"SYSTRT - Input file error");
		dbg(TRACE,"exit ...");
		exit(1);
	} /* end if Check Input file */
  
	dbg(TRACE,"Shared memory assigned and file %s read",sgsin);
  
	if (!shmctl(psyswork->pshrd_mem->id,IPC_STAT, (struct shmid_ds *) &psyswork->pshrd_mem->shdms))
	{
		dbg(TRACE,"cuid = %d", psyswork->pshrd_mem->shdms.shm_perm.cuid);
		dbg(TRACE,"cgid = %d", psyswork->pshrd_mem->shdms.shm_perm.cgid);
		dbg(TRACE,"uid  = %d", psyswork->pshrd_mem->shdms.shm_perm.uid);
		dbg(TRACE,"gid  = %d", psyswork->pshrd_mem->shdms.shm_perm.gid);
		dbg(TRACE,"mode = %o", psyswork->pshrd_mem->shdms.shm_perm.mode);
		dbg(TRACE,"segsz = %x", psyswork->pshrd_mem->shdms.shm_segsz);
		dbg(TRACE,"cpid = %d", psyswork->pshrd_mem->shdms.shm_cpid);
		dbg(TRACE,"lpid = %d", psyswork->pshrd_mem->shdms.shm_lpid);
		dbg(TRACE,"nattch = %d", psyswork->pshrd_mem->shdms.shm_nattch);
		dbg(TRACE,"atime = %d", psyswork->pshrd_mem->shdms.shm_atime);
		dbg(TRACE,"dtime = %d", psyswork->pshrd_mem->shdms.shm_dtime);
		dbg(TRACE,"ctime = %d", psyswork->pshrd_mem->shdms.shm_ctime);
	} /* end if (!shmctl) */ 
  
	psyswork->pbcb->function = (STTABLE|STLOCATE);
	psyswork->pbcb->inputbuffer = NULL;
	psyswork->pbcb->outputbuffer = (char *) &psyswork->psptrec;
	psyswork->pbcb->tabno = SPTAB;
	psyswork->pbcb->recno = 0;
  
	x = REPEAT;
	while(((ret = sth(psyswork->pbcb,psyswork->pgbladr)) !=0) && (--x))
	{
		sleep(1);
	}
  
	if (ret) /* error from STH */
	{
		dbg(TRACE,"SYSTRT - Error [locate SPTAB] is %d", ret);
    
		clean((char *) psyswork->pshrd_mem->shm_base, psyswork->pshrd_mem->id, (struct shmid_ds *) &psyswork->pshrd_mem->shdms);
		dbg(TRACE,"exit ...");
		exit(1);
	} /* end if (ret) */

	/******************* Now SYSLIB Calls **************************/
	rc = tool_search_exco_data ("ALL", "TABEND", pcgHomeAirport[0]);
	if (rc != RC_SUCCESS)
	{
	  dbg(TRACE,"Entry in sgs.tab EXTAB TABEND not found!");
	  pcgHomeAirport[0][0] = '\0';
	}
	else
	{
	  dbg(TRACE,"Table Extension found: %s",pcgHomeAirport[0]);
	  /* Now the DB-Init */
	  while (init_db()) 
	  {
	    dbg(DEBUG,"Waiting for Database connection.");
	    sleep(5);
	  } /* end while */
	  dbg(TRACE,"DB Init OK");
  
	  (void) syslibDestroySMS (pcgHomeAirport[0]); 
	  (void) syslibCreateSMS (pcgHomeAirport[0]);

	} /* fi */

	/* Anzahl der Eintraege in PNTAB feststellen */

	maxrec = psyswork->psptrec->mrecno[PNTAB];
  
	dbg(DEBUG,"maxrec[PNTAB] is %d",maxrec);
  
	for (i = 1; i <= maxrec; i++) 
	{
		psyswork->pbcb->function = (STLOCK|STRECORD|STREAD);
		psyswork->pbcb->inputbuffer = NULL;
		psyswork->pbcb->outputbuffer = (char *) pntrec;
		psyswork->pbcb->tabno = PNTAB;
		psyswork->pbcb->recno = i;
    
		x = REPEAT;
		while(((ret = sth(psyswork->pbcb,psyswork->pgbladr)) !=0) && (--x))
		{
			sleep(1);
		}
    
		if (ret) /* error from STH */ 
		{
			dbg(TRACE,"SYSTRT - Error [read PNTAB] is %d", ret);
			kill_ind = 1; /* set terminate children */
			break;
		} /* end if */
    
		if (pntrec->taskname[0] == 0) /* dummy record, break */ 
		{
			psyswork->pbcb->function = (STRECORD|STUNLOCK);
			psyswork->pbcb->inputbuffer =
			psyswork->pbcb->outputbuffer = NULL;
			psyswork->pbcb->tabno = PNTAB;
			psyswork->pbcb->recno = i;

			sth(psyswork->pbcb,psyswork->pgbladr);
      
			if (i == 1) /* no process to start */ 
			{
				dbg(TRACE, "SYSTRT - Process name table empty !!");
						
				clean((char *) psyswork->pshrd_mem->shm_base, psyswork->pshrd_mem->id, (struct shmid_ds *) &psyswork->pshrd_mem->shdms);
	
				dbg(TRACE,"exit ...");
				
				/******************* Now SYSLIB Calls **************************/
				for (j=0; j<apcnt; j++)
				{
					rc = syslibDestroySMS (pcgHomeAirport[j]); /* Quick hack */
				} /* for */
				exit(1);
			} /* endif  no process to start */

			break;

		} /* endif dummy record, break */
    
		/* Kopiert taskname[0] in anyptr  (for blkmove() see STBsubr.c) */ 
		blkmove((char *) &pntrec->taskname[0],(char *) psyswork->anyptr, sizeof(pntrec->taskname));

		/* Taskname-String abschliessen */

		*(psyswork->anyptr + sizeof(pntrec->taskname)) = 0;
    
		/* Prozessname aus anyptr in psyswork->task_name[i-1][0] eintragen */ 

		strcpy((char *) &psyswork->pmykids->task_name[i - 1][0], psyswork->anyptr);
    
		/* Prozessnummer aus PNTAB in psyswork-prozessnummer eintragen */

		itoa(pntrec->process_no, &psyswork->pmykids->proc_no[i-1][0], 10);
		proc_cntr = i;
    
		dbg(DEBUG,"child_name is %s", &psyswork->pmykids->task_name[i - 1][0]);
    
		sprintf(clSection,"main");
		sprintf(clKeyword,"PAUSE");

		rc=iGetConfigEntry(cfgfile,clSection,clKeyword,CFG_INT,(char *)&igCfgTmp);
		if(rc == RC_SUCCESS)
		{
			igRestartPause = igCfgTmp;
		}/* end of if */
    
		sprintf(clSection,"main");
		sprintf(clKeyword,"STATUS");

		rc=iGetConfigEntry(cfgfile,clSection,clKeyword,CFG_STRING,(char *)clStatus);
		if(rc != RC_SUCCESS) 
		{
			/******************* Now SYSLIB Calls **************************/
			for (j=0; j<apcnt; j++)
			{
				rc = syslibDestroySMS (pcgHomeAirport[j]); /* Quick hack */
			} /* for */

			dbg(TRACE,"Control-Status-Variable nicht gefunden");
			dbg(TRACE,"exit ...");
			exit(1);
		} else {
			status2sts(clStatus,&ctrl_sta);
			dbg(TRACE,"starting in status <%d><%s>",ctrl_sta,clStatus);
			sprintf(control,"%d",ctrl_sta);
		}		
    
    /* Initialisierung der Kommandoparameter (cmd[]) fuer execv */
    
		psyswork->pmykids->cmd[0] = psyswork->anyptr; /* Prozessname */
		psyswork->pmykids->cmd[1] = (char *) &psyswork->pmykids-> proc_no[i-1][0]; /* Prozessnummer */
		psyswork->pmykids->cmd[2] = (char *)&control[0]; 
		psyswork->pmykids->cmd[3] = NULL; 
    
		dbg(DEBUG, "Pathname is: %s",psyswork->pmykids->path_name);
		dbg(DEBUG, "Executing command: %s %s %s %s", runpath, psyswork->pmykids->cmd[0], psyswork->pmykids->cmd[1], psyswork->pmykids->cmd[2]);
   		 
		dbg(DEBUG,"SYSTRT - starting child: %s ", psyswork->pmykids->cmd[0]);
    
		if ((psyswork->pmykids->child_id[i - 1] = fork()) == 0) 
		{
			signal(SIGTERM,SIG_IGN);
			sprintf(cpstr,"cp %s/",getenv("BIN_PATH"));
			strcat(cpstr,psyswork->pmykids->cmd[0]);
			strcat(cpstr," ");
			strcat(cpstr,runpath);
			system(cpstr);
			strcpy(cpstr,runpath);
			strcat(cpstr,psyswork->pmykids->cmd[0]);
			dbg(DEBUG,"CSTART: %s %s",cpstr,psyswork->pmykids->cmd[0]);

      /* 20040129 JIM: allow storing core files in directories */
  		(void) chdir(corebase);
      strcpy(corepath,corebase);
      strcat(corepath,"/procinfo/");
      /* if DBG_PATH/procinfo/ exists, change to it */
    	(void) chdir(corepath);
      strcat(corepath,psyswork->pmykids->cmd[0]);
      /* if DBG_PATH/procinfo/proc_name exists, change to it */
    	(void) chdir(corepath);
      if (getcwd(corepath,sizeof(corepath)) != NULL)
      {
   		  dbg(DEBUG,"working dir: <%s> ",corepath);
      }

			if ((execv(cpstr,psyswork->pmykids->cmd)) == NOLUCK) 
			{
				/******************* Now SYSLIB Calls **************************/
				for (j=0; j<apcnt; j++)
				{
					rc = syslibDestroySMS (pcgHomeAirport[j]); /* Quick hack */
				} /* for */

				/* Error starting child */
				dbg(TRACE,"\n\nERROR stating child: %s\n\n",psyswork->pmykids->cmd[0]);
				send_message(IPRIO_ADMIN,SYSTEXECC,0,0,psyswork->pmykids->cmd[0]);
				dbg(TRACE,"exit ...");
				exit(10);
			} /* end if */
      
		} /* end if */
    
		sleep(igRestartPause);
		dbg(DEBUG,"\n");
    
		psyswork->pmykids->child_deaths[i -1] = 0; /* reset */
    
		pntrec->pid = psyswork->pmykids->child_id[i - 1];
    
		psyswork->pbcb->function = (STUNLOCK|STRECORD|STWRITE);
		psyswork->pbcb->inputbuffer = (char *) pntrec;
		psyswork->pbcb->outputbuffer = NULL;
		psyswork->pbcb->tabno = PNTAB;
		psyswork->pbcb->recno = i;
   		 
		x = REPEAT;
		while(((ret = sth(psyswork->pbcb,psyswork->pgbladr)) !=0) && (--x)) 
		{
			sleep(1);
		} /* end while */
    
		if (ret) /* error from STH */ 
		{
			dbg(TRACE,"SYSTRT - Error [write PNTAB] was %d", ret);
			kill_ind = 1;
			break;
		} /* end if */
	} /* end for */
 
 	for (i = 0; i < MAXNCKIDS; i++)
	{
		char clProgname[24];
		char clProgkey[24];
		char cfgfile[512];

		dbg(TRACE,"Starting non ceda programs %d",i);
		sprintf(cfgfile,"%s/sysmon.cfg",getenv("CFG_PATH"));
		sprintf(clProgkey,"PROG%02d",i+1);
		rc=iGetConfigEntry(cfgfile,"PROGRAMS",clProgkey,CFG_STRING,clProgname);
		if(rc == RC_SUCCESS)
		{
			char clTmpString[124];

			strcpy(NcKids[i].task_name,clProgname);
			rc=iGetConfigEntry(cfgfile,clProgname,"MAXSTARTS",CFG_STRING,clTmpString);
			if(rc == RC_SUCCESS)
			{
				NcKids[i].MaxStarts = atoi(clTmpString);
			}
			else
			{
				NcKids[i].MaxStarts = 5;
			}
			NcKids[i].Starts = 0;
			dbg(TRACE,"MaxStarts for <%s> set to %d",NcKids[i].task_name,NcKids[i].MaxStarts);

			rc=iGetConfigEntry(cfgfile,clProgname,"WAIT",CFG_STRING,clTmpString);
			if(rc == RC_SUCCESS)
			{
				NcKids[i].Wait = atoi(clTmpString);
			}
			else
			{
				NcKids[i].Wait = 10; 
			}
			dbg(TRACE,"Wait for <%s> set to %d",NcKids[i].task_name,NcKids[i].Wait);
			rc=iGetConfigEntry(cfgfile,clProgname,"FATAL",CFG_STRING,clTmpString);
			if(rc == RC_SUCCESS)
			{
				NcKids[i].Fatal = strcmp(clTmpString,"YES") ? FALSE : TRUE;
			}
			else
			{
				NcKids[i].Fatal = FALSE;
			}
			dbg(TRACE,"Fatal for <%s> set to %s",NcKids[i].task_name,NcKids[i].Fatal ? "TRUE" : "FALSE");
		}
		else
		{
			break;
		}
			StartNcProcess(i,NcKids[i].MaxStarts);
	}


	init_systrt();
  
	if (proc_cntr != 0) 
	{
		for ( i=1; i<=proc_cntr; i++) 
		{
			/* send message to SOFTWARE-CONTROL screen */
			/* rv=systrt_to_stedex(psyswork->pmykids->task_name[i-1], ST_UP,"Child started !");*/
			if (rv == FATAL) 
			{
				break;
			} /* end of if */
		} /* end of for */
    
		/* at least the SYSTRT message */
		/* rv = systrt_to_stedex("systrt",ST_UP,"Child started !");*/
	} /* end of if */
  
	signal(SIGTERM,sig_kill); /* enable terminate signal */
  
	for(;;)
	{
		if (!kill_ind) 
		{
			child_id = wait((int *)&psyswork->pmykids->status);
			temp_status = (psyswork->pmykids->status >> 8) & 0xff;
      
			signal(SIGALRM,SIG_IGN); /* no alarms */
      
			if (temp_status == 10) 
			{
				/* NEU JEL 131093 */ 

				y = sysfind(psyswork,child_id);

				send_message(IPRIO_ADMIN,SYSTSPAWNE,0,0,psyswork->pmykids->task_name[y]); 
				if (y == NOLUCK) 
				{
					dbg(TRACE,"Dead child: %d not found! Now checking Non-Ceda childs",
								child_id);
					if ((y=CheckForNcKids(child_id)) == RC_FAIL)
					{
						dbg(TRACE,"Dead child: %d not found in Non-Ceda childs",
								child_id);
					}
					else 
					{
						NcKids[y].child_pid = 0; /* clear */
						dbg(TRACE,"%05d:NcKids[%d].child_pid set to %d",__LINE__,
								y,NcKids[y].child_pid);
					} /* end else */
				} 
				else 
				{
					psyswork->pmykids->child_id[y] = 0; /* clear */
				} /* end else */

				kill_ind = 1; /* set shutdown indicator */

			} /* end if */
		} /* end if */
    
		if (kill_ind) /* terminate */ 
		{
			dbg(TRACE,"kill_ind set !");
#ifndef _LINUX  /* on LINUX avoid zombies */
			signal(SIGCHLD,SIG_IGN);
#else 
			signal(SIGCHLD,sig_chld);
#endif
			kill_kids(psyswork); /* terminate child processes */
			break;
		} /* end if terminate */
    
		dbg(DEBUG,"child id from wait was %d",child_id);
		dbg(DEBUG,"errno from wait was %d",errno);
    
		if (alarm_ind) /* alarm rang */
		{
			dbg(TRACE,"Alarm rang !!!");
      
			alarm_ind = 0; /* reset indicator */
			alarm(25); /* requeue alarm */
      
			if ((child_id == NOLUCK) && (errno == EINTR))
			{
				dbg(TRACE,"wait was interrupted !");
				dbg(TRACE,"child id is %d errno is %d", child_id,errno);
	
				errno = 0;
				continue;
			} /* end if (child-id == NOLUCK) */
		} /* end if alarm rang */
    
		if (psyswork->pmykids->status)
		{
			dbg(TRACE,"errno from child: %d", errno);
      
			if ((errno) && (errno != ENOTTY)) 
			{
				dbg(TRACE,"SYSTRT - Error from child");
			} /* end if */
      
			if (errno == ECHILD)
			{
				dbg(TRACE,"SYSTRT - All children terminated !");
				break;
			} /* end if */
      
			dbg(TRACE, "Child id: %d terminated", child_id);
		} /* end if status */
    
		dbg(DEBUG,"status after wait is %d %d %o", psyswork->pmykids->status,psyswork->pmykids->status, psyswork->pmykids->status);
    
		if ((y = sysfind(psyswork,child_id)) == NOLUCK) /* not found */ 
		{
			dbg(TRACE,"Dead child: %d not found !", child_id);
			if ((y=CheckForNcKids(child_id)) == RC_FAIL)
			{
					dbg(TRACE,"Dead child: %d not found in Non-Ceda childs",
							child_id);
					continue;
			}
			else 
			{
				NcKids[y].child_pid = 0; /* clear */
				dbg(TRACE,"%05d:NcKids[%d].child_pid set to %d",__LINE__,
					y,NcKids[y].child_pid);
				StartNcProcess(y,1);
				continue;
			} /* end else */
		} /* endif (sysfind() == NOLUCK) */
    
		psyswork->pmykids->child_deaths[y] += 1;
    
		/* child termiated */
		send_message(IPRIO_ADMIN,SYSTCHILDT,0,0,psyswork->pmykids->task_name[y]);
    
		/* send message to SOFTWARE-CONTROL screen */
		if( psyswork->pmykids->child_deaths[y] >= DEATH_REPEAT ) 
		{
			/* rv = systrt_to_stedex(psyswork->pmykids->task_name[y], ST_DOWN,"Child terminated !");*/
      
			if( rv == FATAL) 
			{
				break;
			} /* endif (rv == FATAL) */
		} /* endif (DEATH_REPEAT) */
    
		dbg(TRACE,"Deaths of child: %s id: %d is %d", &psyswork->pmykids->task_name[y][0], child_id, psyswork->pmykids->child_deaths[y]);
    
		rc = check_queue();
		if ( rc != REMOVE ) 
		{
      
			/* restart child */
			send_message(IPRIO_ADMIN,SYSTCHILDR,0,0,psyswork->pmykids->task_name[y]);
      
      
			strcpy(psyswork->anyptr, (char *) &psyswork->pmykids->task_name[y][0]);
      
			psyswork->pmykids->cmd[0] = (char *) &psyswork->pmykids->task_name[y][0];
     		 
			psyswork->pmykids->cmd[1] = (char *) &psyswork->pmykids->proc_no[y][0];
      
			ctrl_sta = get_system_state();  
			if(ctrl_sta == FATAL)
			{
				/******************* Now SYSLIB Calls **************************/
				for (j=0; j<apcnt; j++)
				{
					rc = syslibDestroySMS (pcgHomeAirport[j]); /* Quick hack */
				} /* for */

				dbg(TRACE,"Control-Status-Variable nicht gefunden");
				dbg(TRACE,"exit ...");
				exit(1);
			} else {
				sprintf(control,"%d",ctrl_sta);
			}		
      
			/* psyswork->pmykids->cmd[2] = (char *) itoa(ctrl_sta); */
      
			psyswork->pmykids->cmd[2] = control;
			psyswork->pmykids->cmd[3] = NULL; 
      
			dbg(DEBUG, "Pathname is: %s",psyswork->pmykids->path_name);
			dbg(DEBUG, "Restarting child: %s", psyswork->pmykids->cmd[0]);
      
			psyswork->pmykids->child_id[y] = 0; /* clear */
			if ((psyswork->pmykids->child_id[y] = fork()) == 0)
			{
				signal(SIGTERM,SIG_IGN);
				sprintf(cpstr,"cp %s/",getenv("BIN_PATH"));
				strcat(cpstr,psyswork->pmykids->cmd[0]);
				strcat(cpstr," ");
				strcat(cpstr,runpath);
				system(cpstr);
				strcpy(cpstr,runpath);
				strcat(cpstr,psyswork->pmykids->cmd[0]);

				dbg(DEBUG,"RESTART1: %s",cpstr);

        /* 20040129 JIM: allow storing core files in directories */
  			(void) chdir(corebase);
        strcpy(corepath,corebase);
        strcat(corepath,"/procinfo/");
        /* if DBG_PATH/procinfo/ exists, change to it */
    		(void) chdir(corepath);
        strcat(corepath,psyswork->pmykids->cmd[0]);
        /* if DBG_PATH/procinfo/proc_name exists, change to it */
    		(void) chdir(corepath);
        if (getcwd(corepath,sizeof(corepath)) != NULL)
        {
   			  dbg(DEBUG,"working dir: <%s> ",corepath);
        }

        /* 20040618 JIM: allow group to read core file in this dir: */
        (void) chmod("core",(S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP));
        errno= 0; /* 20040701 JIM: debug errno and errmsg (if available) */
				if ((execv(cpstr,psyswork->pmykids->cmd)) == NOLUCK)
				{
					/* error starting child */
          pclErrMsgPtr = strerror(errno);
          if (pclErrMsgPtr == NULL)
          {
             /* 20040701 JIM: debug errno */
					   dbg(TRACE,"ERROR <%d> restarting child",errno);
          }
          else
          {
             /* 20040701 JIM: debug errno and errmsg (if available) */
					   dbg(TRACE,"ERROR <%d> restarting child: <%s>",errno,pclErrMsgPtr);
          }
					send_message(IPRIO_ADMIN,SYSTEXECC,0,0,psyswork->pmykids->cmd[0]);
					dbg(TRACE,"exit 10 for forked process - This will shut down UFIS!");
				  exit(10); /* this is the forked child, parent has to clean up */
				}
			}
      
      /* 20040618 JIM: allow group to read core file in sysmon dir: */
      (void) chmod("core",(S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP));

			psyswork->pbcb->function = (STLOCK|STRECORD|STREAD);
			psyswork->pbcb->inputbuffer = NULL;
			psyswork->pbcb->outputbuffer = (char *) pntrec;
			psyswork->pbcb->tabno = PNTAB;
			psyswork->pbcb->recno = y + 1;
      
			x = REPEAT;
			while(((ret = sth(psyswork->pbcb,psyswork->pgbladr)) !=0) && (--x))
			{
				sleep(1);
			}
      
			if (ret) /* error from STH */
			{
				dbg(TRACE,"SYSTRT - Error [read PNTAB] is %d", ret);
				kill_ind = 1;
				continue;
			}
      
			pntrec->pid = psyswork->pmykids->child_id[y];
			psyswork->pbcb->function = (STUNLOCK|STRECORD|STWRITE);
			psyswork->pbcb->inputbuffer = (char *) pntrec;
			psyswork->pbcb->outputbuffer = NULL;
			psyswork->pbcb->tabno = PNTAB;
			psyswork->pbcb->recno = y + 1;
     	 
			x = REPEAT;
			while(((ret = sth(psyswork->pbcb,psyswork->pgbladr)) !=0) && (--x))
			{
				sleep(1);
			}
      
			if (ret) /* error from STH */
			{
				dbg(TRACE,"SYSTRT - Error [write PNTAB] was %d", ret);
				kill_ind = 1;
				continue;
			}
		} else {
			send_message(IPRIO_ADMIN,SYSTCHILDKD,0,0,psyswork->pmykids->task_name[y]);
		}
    
	} /* end of forever */


	/******************* Now SYSLIB Calls **************************/
	for (j=0; j<apcnt; j++)
	{
		rc = syslibDestroySMS (pcgHomeAirport[j]); /* Quick hack */
	} /* for */


	sleep(10);

	dbg(TRACE,"clean ...");

	clean((char *) psyswork->pshrd_mem->shm_base,psyswork->pshrd_mem->id, (struct shmid_ds *) &psyswork->pshrd_mem->shdms);

	dbg(TRACE,"exit 0 ...");

	exit(0);
  
  return 1; /* not reached, but to avoid compiler warning */
  
} /* end of main               */

static int CheckForNcKids(long child_id)
{
	int ilRc = RC_FAIL;
	char cpstr[1024];
	int ilLc;

	for (ilLc = 0;ilLc < MAXNCKIDS; ilLc++)
	{
		if (NcKids[ilLc].child_pid == child_id)
		{
			dbg(TRACE,"NcKid %d found at %d",child_id,ilLc);
			return(ilLc);
		}
	}
	return ilRc;
}

static int StartNcProcess(int ipPrognum,int ipMaxStarts)
{
	int ilRc = RC_FAIL;
	int ilLc;
	char cpstr[1024];
	pid_t ilChildPid;
	time_t tlNow;

	tlNow = time(NULL);

	if (NcKids[ipPrognum].Starts <   NcKids[ipPrognum].MaxStarts )
	{
		ilChildPid = fork();

	if (ilChildPid == 0) 
	{
			signal(SIGTERM,SIG_IGN);
			sprintf(cpstr,"cp %s/",getenv("BIN_PATH"));
			strcat(cpstr,NcKids[ipPrognum].task_name);
			strcat(cpstr," ");
			strcat(cpstr,runpath);
			system(cpstr);
			strcpy(cpstr,runpath);
			strcat(cpstr,NcKids[ipPrognum].task_name);
			dbg(DEBUG,"CSTART2: %s %s",cpstr,NcKids[ipPrognum].task_name);
			/* 20040129 JIM: allow storing core files in directories */
			(void) chdir(corebase);
			strcpy(corepath,corebase);
			strcat(corepath,"/procinfo/");
			/* if DBG_PATH/procinfo/ exists, change to it */
			(void) chdir(corepath);
			strcat(corepath,NcKids[ipPrognum].task_name);
			/* if DBG_PATH/procinfo/proc_name exists, change to it */
			(void) chdir(corepath);
			if (getcwd(corepath,sizeof(corepath)) != NULL)
			{
				dbg(DEBUG,"working dir: <%s> ",corepath);
			}

			if ((execlp(cpstr,NcKids[ipPrognum].task_name,NULL)) == NOLUCK)
			{
				dbg(TRACE,"\n\nERROR starting child: %s ERROR: %d %s\n\n",
						NcKids[ipPrognum].task_name,errno,strerror(errno));
				ilRc = RC_SUCCESS;
				dbg(TRACE,"\n\nERROR starting non ceda child: %s\n\n",
							NcKids[ipPrognum].task_name);
				if (NcKids[ipPrognum].Fatal == TRUE)
				{
					/* Error starting child */
					send_message(IPRIO_ADMIN,SYSTEXECC,0,0,NcKids[ipPrognum].task_name);
					dbg(TRACE,"exit ...");
					exit(10);
				}
				else
				{
					exit(0);
				}
			} 
			NcKids[ipPrognum].Starts = 0;
		}
		else
		{
			NcKids[ipPrognum].child_pid = ilChildPid;
			dbg(TRACE,"%05d:NcKids[%d].child_pid set to %d",__LINE__,
					ipPrognum,NcKids[ipPrognum].child_pid);
			ilRc = RC_SUCCESS;
			NcKids[ipPrognum].Starts++;
			if (tlNow > (NcKids[ipPrognum].LastStart + (NcKids[ipPrognum].Wait)))
			{
				NcKids[ipPrognum].LastStart = tlNow;
				NcKids[ipPrognum].Starts = 0;
			}
		}
	}
	else
	{
		ilRc = RC_SUCCESS;
	}
	return ilRc;
}
/***************************************************************************
 *                                                                          *
 *       h  e  a  r  t  _  b  e  a  t                                       *
 *                                                                          *
 *       I  a m  s t i l l  a l i v e                                       *
 *                                                                          *
 ****************************************************************************/

heart_beat()
{
	alarm_ind = 1; /* set alarm rang */
  
	if (queues_init == 0) 
	{
		init_que();
		queues_init =1;
	} /* end if */
  
	dbg(DEBUG,"Simulating heart beat !");
  
	que(QUE_WAKE_UP,0,2,1,0,0);
}

/***************************************************************************
 *                                                                          *
 *       k i l l  - k i d s                                                 *
 *                                                                          *
 *       terminates all child processes when SIGHUP received                *
 *                                                                          *
 ****************************************************************************/
static void kill_kids(register SYSWORK *psyswork)
{
	register long x;
  time_t ltPause= 90;
  time_t ltPauseEnd= 0;
  int ilAlarmCnt= 0;
  char *lpcShutDownTimeOut = NULL;
  

	alarm(0);
  
  lpcShutDownTimeOut= getenv("SDTIMOUT");
  if (lpcShutDownTimeOut == NULL)
  {
     dbg(TRACE,"kill_kids : shutdown timeout SDTIMOUT not set in environment"); 
     dbg(TRACE,"kill_kids : using %d seconds as shutdown timeout ",ltPause); 
  }
  else
  {
     ltPause=atoi(lpcShutDownTimeOut);
     if (ltPause < 1)
     {
        dbg(TRACE,"kill_kids : SDTIMOUT in environment must be greater 1"); 
        ltPause = 90;
        dbg(TRACE,"kill_kids : using %d seconds as shutdown timeout ",ltPause); 
     }
  }
#ifndef _LINUX  /* on LINUX already done in SIG_KILL */
	dbg(TRACE,"kill_kids : SIGTERM to %s",&psyswork->pmykids->task_name[0][0]);
	kill((pid_t)psyswork->pmykids->child_id[0],SIGTERM); /* kill QCP */
#endif

	dbg(TRACE,"kill_kids : sleep %d",ltPause);
  ltPauseEnd= time(NULL) + ltPause + 1; /* add 1 second for sysqcp */
  while (time(NULL) < ltPauseEnd )
  {
	   sleep(ltPauseEnd - time(NULL));
     ilAlarmCnt++;
  }
  if (ilAlarmCnt>1)
  {
	   dbg(TRACE,"kill_kids : sleep interrupted %d times",ilAlarmCnt-1);
  }
	signal(SIGALRM,SIG_DFL);
  
	for (x = NO_OF_KIDS-1; x > 0; x--) /* child_id array */
	{
		if (psyswork->pmykids->child_id[x])
		{
   	  dbg(TRACE,"kill_kids : SIGKILL to %s...",
         &psyswork->pmykids->task_name[x][0]);
	    kill((pid_t)psyswork->pmykids->child_id[x],SIGKILL); /* kill process */
		}
	}

	for (x = MAXNCKIDS-1; x > 0; x--) /* child_id array */
	{
		if (NcKids[x].child_pid)
		{
   	  dbg(TRACE,"kill non CEDA kids : SIGKILL to %s...",
         &NcKids[x].task_name[0]);
	    kill((pid_t)NcKids[x].child_pid,SIGKILL); /* kill process */
		}
	}
	dbg(TRACE,"SYSTRT : Now terminating");
}

/***************************************************************************
 *                                                                          *
 *       s i g  _  k i l l                                                  *
 *                                                                          *
 *       sets the indicator for terminating the child processes             *
 *                                                                          *
 ****************************************************************************/

static void sig_kill(int sig)
{
	register SYSWORK *psyswork = &syswork;

	dbg(TRACE,"SYSTRT - Stop signal received !");
	dbg(TRACE," SYSTRT - Terminating all child processes !");
#ifndef _LINUX  /* on LINUX avoid zombies */
	signal(SIGCHLD,SIG_IGN);
#endif      
	signal(SIGCHLD,sig_chld);
	kill_ind = 1; /* set kill indicator */
#ifdef _LINUX  
  /* after processing sig_kill the linux sysmon still is in wait() and
     does not terminate the sysqcp. The wait() blocks until the first 
     terminating child, and when sysqcp does not terminate, this wait()
     is forever
  */   
	dbg(TRACE,"sig_kill : SIGTERM to %s",&psyswork->pmykids->task_name[0][0]);
	kill((pid_t)psyswork->pmykids->child_id[0],SIGTERM); /* kill QCP */
#endif      
	return;
}

/***************************************************************************
 *                                                                          *
 *       s i g  _  c h i l d                                                *
 *                                                                          *
 *       while terminating remove terminating child processes from list     *
 *                                                                          *
 ****************************************************************************/

static  void sig_chld(int sig)   /* signal: child processes terminated */
{
	register SYSWORK *psyswork = &syswork;
	long y = 0l;
	long child_id = 0l;

	child_id = wait((int *)&psyswork->pmykids->status);
  if (child_id > 0)
  {	/* no Timeout from alarm */
    if ((y = sysfind(psyswork,child_id)) != NOLUCK) /* child found */ 
    {
      psyswork->pmykids->child_id[y]= 0; /* no need to kill anymore */
    }
  }
	return;
}


/***************************************************************************
 *                                                                          *
 *       s  y  s  f  i  n  d                                                *
 *                                                                          *
 *       returns the index of the child that died or -1 if child not found  *
 *                                                                          *
 ****************************************************************************/
static sysfind(register SYSWORK *psyswork,register long child_id)
{
	register long x;
  
	for (x = 0; x < NO_OF_KIDS; x++) /* child_id array */
	{
		if (psyswork->pmykids->child_id[x])
		{
			if (child_id == psyswork->pmykids->child_id[x])
			{
				return(x); /* found */
			}
		}
	}
  
	return(NOLUCK); /* not found */
  
}

/****************************************************************************/
/*                                                                          */
/*      s y s r e a d                                                       */
/*                                                                          */
/****************************************************************************/
static sysread(register SYSWORK *psyswork,char *sgsin)
/* sgsin : pointer to input file name */
{
	int fd;
	register long sz;
	register char *rdaddr;
  
	dbg(DEBUG,"Reading %s",sgsin);
  
  
	if ((fd = open(sgsin,0)) <= 0) 
	{
		return(errno);
	}
  
	if (read(fd,(long *) &psyswork->pshrd_mem->size,sizeof(long)) <= 0)
	{
		return(errno);
	}
  
	if (read(fd,(int *) &maxtab,sizeof(int)) <= 0)
	{
		return(errno);
	}
  
	if ((psyswork->pshrd_mem->id = shm_ass(psyswork->pshrd_mem->size, (long)(IPC_CREAT|0666))) == NOLUCK)
	{
		return(errno);
	}
  
	if ((psyswork->pshrd_mem->shm_base = rdaddr = shm_acc(psyswork->pshrd_mem->id,NULL, psyswork->pshrd_mem->size,psyswork->pgbladr)) == FAILURE)
	{
		return(errno);
	}
  
	sz = psyswork->pshrd_mem->size; /* systab size */
	while (sz > 0)
	{
		if ((read(fd,(char *) rdaddr,(sz > 0x8000) ? 0x8000 : sz)) <= 0)
		{
			return(errno);
		}
		rdaddr += 0x8000;
		sz -= 0x8000;
	}
  
	psyswork->pshrd_mem->shm_end = psyswork->pshrd_mem->shm_base + psyswork->pshrd_mem->size;
  
	close(fd);
  
	return(0);
}

/* ------------------------------------------------------------------------
   F u n c t i o n   init_systrt()
   
   Following the initializations
   -------------------------------------------------------------------------*/
static void init_systrt()
{
	ITEM	item;
	int rc;
  
	/* for send_status  NOT USED IN CEDA */
	/* len = sizeof(EVENT) +  sizeof(STEDIN) + STRLEN;*/
	/* status_evn = (char *) calloc(1,len);   */
	/* initialize queue */

	dbg(DEBUG,"SYSTRT - Waiting for queues ");
  
	do 
	{
		rc = init_que();
		if( rc) 
		{
			sleep(1);
		} /* fi */
	} while( rc != 0 );    
  
	queues_init = 1;

	/* now create my queue */
	do 
	{
		rc = que(QUE_CREATE,SYSTRT,SYSTRT,PRIORITY_3,7,"sysmon");
		if( rc) 
		{
			dbg(TRACE,"SYSTRT: Queue creation failed ");
			sleep(1);
		} /* fi */
    
		alarm(10);
		if ( que(QUE_RESP,SYSTRT,SYSTRT,PRIORITY_3,sizeof(ITEM),(char *)&item) != RC_SUCCESS ) 
		{
			dbg(TRACE,"SYSTRT: Queue does not response ");
		} /* end if */
    
		alarm(0);
    
	} while( rc != 0 );    
  
} /* end init systrt */

/* ------------------------------------------------------------------------
   Following the check_queue function
   
   here I check my queue for a possible remove of a child.
   
   If you want to remove a child, you have to send a REMOVE to me,
   then send a SHUTDOWN to the child you want to kill, and hope
   that in this time no other program leaves us. !!!!
   -------------------------------------------------------------------------*/
static int check_queue()
{
	int rc;         /* return value */ 
	ITEM       *item;
	EVENT      *event;
	len  = sizeof(ITEM) + sizeof(EVENT);
	item = (ITEM *)malloc(len);
  
	if ( item == NULL ) 
	{
		dbg(TRACE,"SYSTRT: I guess we have some memory problems");
		return KEEP;   /* this is not a solution */
	} /* fi */
  
	rc = que(QUE_GETNW,0,SYSTRT,0,len,(char *)item);
  
	if ( rc == OK) 
	{
		que(QUE_ACK,0,SYSTRT,0,0,NULL);
		event = (EVENT *)item->text;
    
		/* maybe we want to expand this function in future */
    
		switch ( event->command ) 
		{
		case    REMOVE          :
			rc = REMOVE;
			break;
		default                 :
			rc = KEEP;
			break;
		} /* end switch */
	} else {
		rc = KEEP;
	} /* esle */
  
	free(item);
  
	return rc;
  
} /* end check_queue */

/* -------------------------------------------------------------------------*/
/*      Following the systrt_to_stedex function                             */
/* -------------------------------------------------------------------------*/
/*
 *static int systrt_to_stedex(char *child_name, long status, char *str)
 *{
 *
 *	    if ( status_evn != NULL )  
 *	    {
 *
 *		txt_pos = 0;    
 *		txt_no  = 0;    
 *		memset (sysid1,0x00,SYSID_SIZE+1);
 *		memset (devnam1,0x00,DEVICEID_SIZE+1);
 *		memset (sysid2,0x00,SYSID_SIZE+1);
 *		memset (devnam2,0x00,DEVICEID_SIZE+1);
 *
 *
 *		strncpy(devnam1,"P_CTRL",DEVICEID_SIZE);
 *		send_status(2501,status_evn,0L,0L,PRIORITY_3,ST_CHANGE,sysid1,
 *			devnam1, NULL,NULL,ST_UP,1,0,&bbuffer);
 *
 *	
 *		strncpy(sysid1,"PROCESS",SYSID_SIZE);
 *		sprintf(devnam1,"%-10s",child_name);
 *		strncpy(devnam2,"P_CTRL",DEVICEID_SIZE);
 *		send_status(2501,status_evn,0L,0L,PRIORITY_3,ST_CHANGE,sysid1,
 *			devnam1,sysid2,devnam2,status,1,txt_pos,&bbuffer);
 *
 *		txt_no = 1;
 *		txt_pos = 1;    
 *		t = time(NULL);
 *		lt = gmtime(&t);
 *		sprintf(tbuffer,"%02d:%02d:%02d\0",lt->tm_hour,lt->tm_min,
 *							       lt->tm_sec);
 *		send_status(2501,status_evn,0L,0L,PRIORITY_3,ST_TEXT,
 *			sysid1,devnam1, NULL,NULL,0L,txt_no,txt_pos,tbuffer);
 *
 *		txt_no = 2;
 *		txt_pos = 1;    
 *		send_status(2501,status_evn,0L,0L,PRIORITY_3,ST_TEXT,sysid1,
 *			devnam1, NULL,NULL,0L,txt_no,txt_pos,str);
 *
 *	    }
 *	    else
 *	    {
 *		    dbg(TRACE,"SYSTRT: I guess we have a memory problem");
 *		    return(FATAL);
 *	    }
 *
 *	    return(NONFATAL);
 *
 *}
 */ /* end systrt_to_stedex */  /* ----- END OF COMMENT --------- */

