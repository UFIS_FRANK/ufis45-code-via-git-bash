#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/PVG/PVG_Server/Base/Server/Kernel/impmgr.c 1.1 2005/08/05 23:16:18SGT jim Exp  $";
#endif /* _DEF_mks_version */
/******************************************************************************/
/*                                                                            */
/* ABB ACE/FC Program Skeleton                                                */
/*                                                                            */
/* Author         :                                                           */
/* Date           :                                                           */
/* Description    :                                                           */
/*                                                                            */
/* Update history : 12.12.2000 GHe                                            */
/* 20040319 JIM: increment all STOx day in every loop, so avoiding bug when   */
/*               first valid day by FREQ is not VPFR                          */
/*               ilDayOffset removed, all STOx counting itself                */
/*                                                                            */
/* 20050805 JIM: special version PVG vor schedule.sh and special FLIIMP, taken*/
/*               from UFIS43                                                  */
/******************************************************************************/
/*                                                                            */
/* be carefule with strftime or similar functions !!!                         */
/*                                                                            */
/******************************************************************************/
/* This program is a MIKE main program */
#define U_MAIN
#define UGCCS_PRG
#define STH_USE
#include <stdio.h>
#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include <ctype.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

#include "ugccsma.h"
#include "msgno.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "timdef.h"
#include "sthdef.h" 
#include "router.h"
#include "debugrec.h"
#include "hsbsub.h"
#include "db_if.h"
#include "impmgr.h"
#include "calutl.h"
#include "send.h"


#define DATABLK_SIZE     (256*1024)
#define C_MAX_SP_FIELDS        (10) 
#define C_MAXFIELDS           (100) 
#define C_MAXFIELDNAMELEN       (4)
#define C_MAXFIELDVALLEN     (2048)

/* selection keyword inps */
#define C_IMPORT           (1) 
#define C_PREVIEW          (2) 

/* field struct definition */
#define C_AKTIV            (1)
#define C_DEAKTIV          (0)
#define C_NOTSET           (-1)
#define C_SET              (1)
#define C_DISABLE          (0)
#define C_ENABLE           (1)
#define C_FOUND            (0) 
#define C_NOTFOUND         (1)
#define C_SEARCH           (1)
#define C_INSERT           (2)
#define C_UPDATE           (3)
#define C_SELECT           (4)
#define C_FLDCMP           (5)

#define C_FIRST            (1)
#define C_NEXT             (2) 
#define C_LAST             (3) 
#define C_PREV             (4)

/* FUNCTION ERROR CODES */
#define C_NO_ERROR           (0) 
#define C_UNKNOWN_COMMAND    (1)
#define C_NO_ACTIVE_FIELDS   (2) 
#define C_SET_ON_FIRST_FIELD (3)
#define C_SET_ON_LAST_FIELD  (4)


/* DEFINITION FOR LOADING URNOS */
#define C_RESERVED_X_URNOS   (100)

/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
FILE *outp       = NULL ;
int  debug_level = 0; /* instead of DEBUG */


extern BuildItemBuffer(char *, char *, int, char *) ; 

/******************************************************************************/
/* STRUCTS                                                                    */
/******************************************************************************/
typedef struct
{
 int  Status      [C_MAXFIELDS];
 char Fieldname   [C_MAXFIELDS][C_MAXFIELDNAMELEN+1];
 char FieldVal    [C_MAXFIELDS][C_MAXFIELDVALLEN];
 int  StrItem     [C_MAXFIELDS];
 long StrBeginPos [C_MAXFIELDS];
 long ValLength   [C_MAXFIELDS];
 long Item        [C_MAXFIELDS];
 int  ReadFromEv  [C_MAXFIELDS];
 int  LastAttach  [C_MAXFIELDS]; 
 int  DoFldCmp    [C_MAXFIELDS]; 
 int  DoUpdate    [C_MAXFIELDS]; 
 int  DoSelect    [C_MAXFIELDS];
 int  DoInsert    [C_MAXFIELDS];
 int  DoSearch    [C_MAXFIELDS];

} ST_EVT_DATA_HDL;



struct stPtrF
{
 struct stPtrF  *prev  ;
 struct stPtrF  *next  ;
 char FKEY[20] ;
} ;
typedef struct stPtrF ST_LST_FKEY ; 


typedef struct
{
 int  Status      [C_MAX_SP_FIELDS];
 char Fieldname   [C_MAX_SP_FIELDS][C_MAXFIELDNAMELEN+1];
 int  Hndl1       [C_MAX_SP_FIELDS];
 int  Hndl2       [C_MAX_SP_FIELDS];
 int  Hndl3       [C_MAX_SP_FIELDS];
 int  Hndl4       [C_MAX_SP_FIELDS];
 int  Hndl5       [C_MAX_SP_FIELDS];
} ST_SPECIAL_FLD_HDL;


/*--------------------------------------------------------*/
/* used counter struct                                    */
/*--------------------------------------------------------*/
typedef struct
{
 long iHitCounter ;       /* counts all hits for all event              */
 long iArrivalCounter ;   /* counts Arrival or Arrival/Depature hits    */
 long iDepartureCounter ; /* counts Departure or Arrival/Departure hits */
 long iInsertArrCounter ; /* counts all inserts records for Arrival     */
													/* or Arrival/Departure */   
 long iUpdateArrCounter ; /* counts all update records for Arrival      */
													/* or Arrival/Departure */   
 long iDoNotArrCounter ;  /* counts all hits for arrival, where no chg. */
													/* exists               */   
 long iDoNotDepCounter ;  /* counts all hits for departure, where no    */
													/* changes exists       */   
 long iInsertDepCounter ; /* counts all inserts records for Departure   */
													/* or Arrival/Departure */   
 long iUpdateDepCounter ; /* counts all update records for Departure    */
													/* or Arrival/Departure */   
} ST_CNT_HDL;




/* Global variables                                                           */
/******************************************************************************/
static ITEM  *prgItem      = NULL;                /* The queue item pointer  */
static EVENT *prgEvent     = NULL;                /* The event pointer       */
static int   igItemLen     = 0;                   /* length of incoming item */
static int   igInitOK      = FALSE;
/* static int igToFlight = 7805; */
static int igToFlight = 7790;
static char  cgConfigFile[512];
static ST_EVT_DATA_HDL  sDepFldArr;
static ST_EVT_DATA_HDL  sArrFldArr;
static ST_SPECIAL_FLD_HDL sgSpFields ;
static char  pcgTblNam[12] ; 
static char  pcgTblExt[12] ; 
static char  pcgASqlBuf[12*1024] ; 
static char  pcgDSqlBuf[12*1024] ; 
static char  pcgDataArea[DATABLK_SIZE] ; 
static char  pcgTmpDataArea[DATABLK_SIZE] ; 
static int   igRealImportFlag ;     /* write and send 2 flight if TRUE */      

/* static char  pcgTwEnd[64] ; */
/* static char  pcgTwStart[64] ; */

static int   igActUrno ; 
static int   igReservedUrnoCnt ; 
static int   igUseImpTab = -1 ; /* On init: do not use IMPTAB */

static char  pcgRecvName[64] ;  /* BC-Head used as WKS-Name */
static char  pcgDestName[64] ;  /* BC-Head used as USR-Name */
static char  pcgTwStart[64] ;   /* BC-Head used as Stamp */
static char  pcgTwEnd[64] ;     /* BC-Head used as Stamp */

static ST_LST_FKEY  *gsActFKey = NULL ;
static ST_LST_FKEY  *gsFirstFKey = NULL ;
static ST_LST_FKEY  *gsHelpFKey = NULL ;
static ST_LST_FKEY  *gsNewElement = NULL ;

static int importFlag ;
static int previewFlag ; 

/******************************************************************************/
/* Flags for testing                                                          */
/******************************************************************************/
/* if one of the parameters is set to = 1 --> test is active ELSE no test */

static int gl_updateTest ; 
static int gl_noChangeTest ; 
static int igTestFktFlag ;      /* if set to TRUE >> additional fct. output */ 

/******************************************************************************/
/* External functions	                                                        */
/******************************************************************************/

extern int get_item (int, char *, char *, int, char *, char *, char *) ; 
extern int get_real_item (char *, char *, int) ; 
extern int get_item_no (char *, char *, int) ; 
extern int FullDayDate (char * clDateStr, int ilDay ) ; 


/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
static int Init_impmgr() ;
static int	Reset(void) ;                      /* Reset program          */
static void	Terminate(void);                   /* Terminate program      */
static void	HandleSignal(int);                 /* Handles signals        */
static void	HandleErr(int);                    /* Handles general errors */
static void	HandleQueErr(int);                 /* Handles queuing errors */
static int	HandleData(void);                  /* Handles event data     */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/
static int  fillFldStruct(char *, ST_EVT_DATA_HDL *) ;  
static int  ManageImportData (ST_EVT_DATA_HDL *pclArrFldArr,  
				 						          ST_EVT_DATA_HDL *pclDepFldArr, int ipSelFlg) ; 
static int FormatDayFreq (char *, char *) ;
static int createFKEY (char *FKey, char *clArrFLTN, char *clACL3, char *clFLNS,
                       char *clActDate, char clADID ) ; 
static int setSelectInfFlag (int *iplSelFlg, char *pclSelSubStr) ; 
static int fillDataInFldStruct (char *pclDataStr, ST_EVT_DATA_HDL *sFldArr); 
static int ResetFldStruct (ST_EVT_DATA_HDL *sFldArr) ; 
static int getIntDaysFromStr (int *ilDayCnt, int *ilSelectDay, char *cpDayStr);
static int getFldDataFromStruct (char *pclRetStr, char *pclFld,    
                                 ST_EVT_DATA_HDL sFldArr) ;  
static int setChkFldinStruct4Fld (char *pclFld, int iDoWhat, 
																	ST_EVT_DATA_HDL *sFldArr) ;  
static int ifRecordChanged (int *iChgFlg, char *pcgSearchFldLst,
														char *pcgDataArea, ST_EVT_DATA_HDL sFldArr) ; 
static int SQLInsertRecord (char *cpUsedFlds, char *cpUsedData, 
														char *cpTblNam, char *cpTblExt ) ; 
static int AddFld2Struct (ST_EVT_DATA_HDL *sFldArr, char *cpFldName,
                          char *cpValStr ) ;   
static int ReplaceFldDataInStruct (char *cpDatStr, char *pclFld,  
                                   ST_EVT_DATA_HDL *sFldArr) ;  
static int setStatusInStruct (char *pclFld, int iDoWhat, int iKindOfFlg,
                              ST_EVT_DATA_HDL *sFldArr) ;  
static int SQLSelectRecord (int *foundRecord, char *cpSelFlds, char *cpSelData, 
														char *cpTblNam, char *cpTblExt, char *cpCondFld,
														char *cpCondData) ;  
static int createFldAndDataLst (char *pcFldLst, char *pcDataLst, int iKindOf,
                                ST_EVT_DATA_HDL sFldArr) ;  
static int prepSqlValueStr (int iItems, char *cpValStr) ;  
static int SQLgetALC3Code (char *pcALC3, char *pcAIRL) ; 
static int getItemCnt (char *cpFldStr, int *ipCnt) ; 
static int resetCounters (ST_CNT_HDL * stCntInf) ;  
static int getDataFromDbStrs (int *iItemNo, char *cFld,   
                              char *pcgSearchFldLst, /* feld list select */
                              char *pcgDataArea,     /* data list select */ 
                              char *pcgFoundData) ;  
static int  GetLocalTimeStamp(char *cpDateTimeStr) ; 
static int  GetNextUrno(int *pipUrno) ; 
static int  setNextFldPosInStruct (int *ipActionResult, int iAction,  
					  						           ST_EVT_DATA_HDL *sFldArr) ;  
static int  getFldStruct (char *pclFldsStr, char *pclDataStr, int iAction,  
												  ST_EVT_DATA_HDL *sFldArr) ;  
static int  SQLInsertHistoryRecs (ST_EVT_DATA_HDL *sEvtChgData, 
													        ST_EVT_DATA_HDL *sSqlChgData, 
													        ST_EVT_DATA_HDL *sFldData, int iplFlag) ;
static int  SqlIMCTabRecs (char *cpAFKEY, char *cpDFKEY, ST_CNT_HDL stCnt,
													 int iplFlag) ;
static int  readDataItemFromStr( char *cpFldData, char *cpFldName, 
																 char *cpFldLst, char *cpDataArea) ;  
static int  DoesFldExistInStruct (char *pclFld, ST_EVT_DATA_HDL sFldArr) ;  
static int  InsOrReplFld2Struct (ST_EVT_DATA_HDL *sFldArr, char *cpFldName,
                                 char *cpValStr ) ;  
static int  GetNextUrnoItem (int *pipUrno) ; 
static int  IsThisASpecialFld (char *cpField) ; 
static int  createVIALCompleteSubStr ( char *cpNewVIALSubStr, char *cpSubStr,
																			char *cpCompleteStr, int iStrlen ) ;  
static int  spVIALCondition (char *cpDataArea, ST_EVT_DATA_HDL *sFldArr,
														 char *, char *) ;   
static int  getChgFlds (int *iChgFlg, 
									  		char *pcgSearchFldLst,      /* feld list select */
                        char *pcgDataArea,          /* data list select */ 
											  ST_EVT_DATA_HDL *sFldArr, 
                        ST_EVT_DATA_HDL *sChgFldsFromEvent, 
                        ST_EVT_DATA_HDL *sChgFldsFromSelect) ;  
static int GetRVSFKEYS (ST_EVT_DATA_HDL *pclFldArr) ; 
static int OutputFKeyList (void) ; 
static int DeleteFKeyList (void) ; 
static int WriteNewFKEYListElement (char *pcFKEY) ; 
static int FKEYInList (char *cpFKey, int *iFKeyNotFound) ; 
void ClearFkeyInsert (char *pclFkey);
int MarkFkeyInsert (char *pclFkey);
void CheckImptabUsage ();



/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
	int	ilRC = RC_SUCCESS;			/* Return code			*/
	int	ilCnt = 0;
  char pclFldLst[4098];
	char pclDatLst[4098];

	/*--------------------------------------------------------------------*/
	/* the normal sequenz of this process is the simulation mode. This    */
	/* means, that the flag igRealImportFlag is set to FALSE. This        */
	/* incident can be solved by the event 998. The 998 calling causes a  */
	/* toggle to the igRealImportFlag.                                    */
	/*--------------------------------------------------------------------*/

	debug_level = DEBUG ;     /* also debug level is on */

	INITIALIZE;			/* General initialization	*/
	dbg (TRACE, "MAIN: version <%s>", mks_version);
  igRealImportFlag = TRUE ;

	/* setting Testfalgs */
  /* igTestFktFlag = TRUE ; 20030611 JIM */
  igTestFktFlag = FALSE ;
	gl_updateTest = 0 ;      /* Arrival, Departure selects a different record */ 
  gl_noChangeTest = 0 ; 
  igActUrno = 0 ;          /* URNO VALUE - not set */ 
  igReservedUrnoCnt = 0 ;  /* Urno Buffer - no reserved Urnos */ 

	strcpy (pcgTblNam, "AFT") ; 
	strcpy (pcgTblExt, "TAB") ;


	/* - SET INFO FOR SPECIAL HANDLING FIELDS -------- */
  sgSpFields.Status[0] = C_AKTIV ;            /* set ACTIVE */ 
	strcpy (sgSpFields.Fieldname[0], "VIAL") ;     /* set fieldname */ 
  sgSpFields.Hndl1[0] = 0 ;                    /* not used in moment */ 
  sgSpFields.Hndl2[0] = 0 ;                    /* not used in moment */ 
  sgSpFields.Hndl3[0] = 0 ;                    /* not used in moment */ 
  sgSpFields.Hndl4[0] = 0 ;                    /* not used in moment */ 
  sgSpFields.Hndl5[0] = 0 ;                    /* not used in moment */ 
  sgSpFields.Status[1] = C_DEAKTIV ;          /* set next field INACTIVE */ 

	/* Attach to the MIKE queues */
	do
	{
		ilRC = init_que();
		if(ilRC != RC_SUCCESS)
		{
			dbg(TRACE, "MAIN: init_que() failed! waiting 6 sec ...") ;
			sleep(6);
			ilCnt++;
		}/* end of if */
	}while((ilCnt < 10) && (ilRC != RC_SUCCESS));
	if(ilRC != RC_SUCCESS)
	{
	 dbg(TRACE, "MAIN: init_que() failed! waiting 60 sec ...") ;
	 sleep(60) ;
	 exit(1) ;
	}
	else
	{
	 dbg (TRACE, "MAIN: init_que() OK! mod_id <%d>", mod_id) ;
	}/* end of if */
	do
	{
		ilRC = init_db();
		if (ilRC != RC_SUCCESS)
		{
		 check_ret(ilRC);
		 dbg (TRACE, "MAIN: init_db() failed! waiting 6 sec ...") ;
		 sleep(6) ;
		 ilCnt++ ;
		} /* end of if */
	} while((ilCnt < 10) && (ilRC != RC_SUCCESS));
	if(ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_db() failed! waiting 60 sec ...");
		sleep(60);
		exit(2);
	}
	else
	{
	 dbg(TRACE,"MAIN: init_db() OK!");
	} /* end of if */

  /* logon to DB is ok, but do NOT use DB while ctrl_sta == HSB_COMING_UP !!! */
	sprintf(cgConfigFile, "%s/impmgr", getenv("BIN_PATH"));
	ilRC = TransferFile(cgConfigFile);
	if(ilRC != RC_SUCCESS)
	{
	 dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile);
	} /* end of if */

	/* uncomment if necessary */
	/* sprintf(cgConfigFile,"%s/impmgr.cfg",getenv("CFG_PATH")); */
	/* ilRC = TransferFile(cgConfigFile); */
	/* if(ilRC != RC_SUCCESS) */
	/* { */
	/* 	dbg(TRACE,"MAIN: TransferFile(%s) failed!",cgConfigFile); */
	/* } */ /* end of if */

	ilRC = SendRemoteShutdown(mod_id) ;
	if(ilRC != RC_SUCCESS)
	{
	 dbg(TRACE, "MAIN: SendRemoteShutdown(%d) failed!", mod_id) ;
	} /* end of if */
	if((ctrl_sta != HSB_STANDALONE) && (ctrl_sta != HSB_ACTIVE)
		 && (ctrl_sta != HSB_ACT_TO_SBY))
	{
		dbg(DEBUG,"MAIN: waiting for status switch ...");
		HandleQueues();
	}/* end of if */
	if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE)
		 || (ctrl_sta == HSB_ACT_TO_SBY))
	{
		dbg(TRACE,"MAIN: initializing ...");
		if(igInitOK == FALSE)
		{
			ilRC = Init_impmgr();
			if(ilRC != RC_SUCCESS)
			{
				dbg(TRACE,"Init_impmgr: init failed!");
			} /* end of if */
		}/* end of if */
	} else {
		Terminate();
	}/* end of if */
	dbg(TRACE, "MAIN: initializing OK") ;
	for(;;)
	{
		dbg(TRACE,"================= START/END =========================");
		ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;
				
		if( ilRC == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRC != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRC);
			} /* fi */
			
			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				HandleQueues();
				break;	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				break;	
			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				/* CloseConnection(); */
				HandleQueues();
				break;	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(), */
				/* send_message() or timsch() !                      */
				ctrl_sta = prgEvent->command;
				Terminate();
				break;	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command;
				ResetDBCounter();
				break;	
			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;
			case	SHUTDOWN	:
				/* process shutdown - maybe from uutil */
				Terminate();
				break;
					
			case	RESET		:
				ilRC = Reset();
				break;
					
			case	EVENT_DATA	:
				if((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) 
						|| (ctrl_sta == HSB_ACT_TO_SBY))
				{
					ilRC = HandleData();
					if(ilRC != RC_SUCCESS)
					{
						HandleErr(ilRC);
					}/* end of if */
				}else{
					dbg(TRACE,"MAIN: wrong hsb status <%d>",ctrl_sta);
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
				}/* end of if */
				break;
					
			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;
			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;


			case 	100:                               /* data testmode */

				/* be carefully with the testsize max. 2048 bytes */
  
				igTestFktFlag = TRUE ; 
	      gl_updateTest = 1 ;      /* Arrival, Departure selects a dif. record */ 

				dbg (DEBUG, "-----------------------------------------------------") ; 
				dbg (DEBUG, "|   USING TEST DATAS                                |") ;
				dbg (DEBUG, "-----------------------------------------------------") ; 

				strcpy(pclFldLst,
/* Arrival */  "STOA,STOD,VPFR,VPTO,FLTN,FLNS,FRQD,ALC3,FRQW,AIRL,VIAL"
				       "\n"
/* Departu */  "STOA,STOD,VPFR,VPTO,FLTN,FLNS,FRQD,ALC3,FRQW,AIRL,VIAL"
							 "\n"
							) ;

				/* correct data with timerange for 2 days   */
				strcpy(pclDatLst,
				"20000926083833,"           /* STOA Arrival */
				"20000926083833,"           /* STOD Arrival */
				"20000826083800,"           /* VPFR Arrival */
				"20000928083833,"           /* VPTO Arrival */
				"123,"                      /* FLTN Arrival */
				"#,"                        /* FLNS Arrival */
				"1.3.5..,"                  /* FRQD Arrival */
				"ac3,"                      /* ALC3 Arrival */
				"6,"                        /* FRQW Arrival */
				"OA,"                       /* AIRL End of Arrival */
				" 1IS 6"                    /* FRQW Arrival */
				"\n"                        /* --- DIV Arrival <-> Depature record */
				"20000928083833,"           /* STOA Departure */
				"20000926083833,"           /* STOD Departure */
				"20000826083800,"           /* VPFR Departure */
				"20000926083800,"           /* VPTO Departure */
				","                         /* FLTN Departure */
				"#,"                        /* FLNS Departure */
				"1.2.3.4.5.6,"              /* FRED Departure */
				"ac3,"                      /* ALC3 Departure */
				"1,"                        /* FRQW Departure */
				"OA,"                       /* AIRL Departure */
				" 1IS 7"                    /* FRQW Arrival */
				"\n"                        /* end marker for Arr. and Dep. record */ 
				) ; 

				dbg (DEBUG, "SendCedaEvent --> FLIGHT IMP (TEST)") ; 
        SendCedaEvent(mod_id,0,"ufis","","0","ATH,TAB,BDPS-UIF",
                      "IMP","ALTTAB","PREVIEW,IMPORT",pclFldLst,pclDatLst,
											"", 4,RC_SUCCESS);

				break;


			case 	101:                        /* data testmode RVS */

				/* be carefully with the testsize max. 2048 bytes */
  
				igTestFktFlag = TRUE ; 
	      gl_updateTest = 1 ;      /* Arrival, Departure selects a dif. record */ 

				dbg (DEBUG, "-----------------------------------------------------") ; 
				dbg (DEBUG, "|   USING TEST DATAS                                |") ;
				dbg (DEBUG, "-----------------------------------------------------") ; 

				strcpy(pclFldLst,  "ALC3,FLTN,FLNS,ADID,VPFR,VPTO,FRQD,FRQW") ;

				/* correct data with timerange for more days   */
				strcpy(pclDatLst,
				"ac3,"                      /* ALC3 */
				"123,"                      /* FLTN */
				"#,"                        /* FLNS */
				"A,"                        /* ADID = Arrival */
				"20000826083800,"           /* VPFR */
				"20000928083833,"           /* VPTO */
				"1.3.56.,"                  /* FRQD */
				"1"                         /* FRQW */
				"\n"                        /* end marker for record */ 

				"ac3,"                      /* ALC3 */
				"345,"                      /* FLTN */
				"#,"                        /* FLNS */
				"D,"                        /* ADID = Arrival */
				"20000826083800,"           /* VPFR */
				"20000904083815,"           /* VPTO */
				"1.3.5..,"                  /* FRQD */
				"1"                         /* FRQW */
				"\n"                        /* end marker for record */ 
				) ; 

				dbg (DEBUG, "SendCedaEvent --> FLIGHT RVS (TEST)") ; 
        SendCedaEvent(mod_id,0,"ufis","","0","ATH,TAB,BDPS-UIF",
                      "RVS","ALTTAB","PREVIEW",pclFldLst,pclDatLst,
											"", 4,RC_SUCCESS);

				break;




      case 997: 
                if (igTestFktFlag == FALSE)
								{ 
                 igTestFktFlag = TRUE ; 
                 debug_level = DEBUG ;     /* also debug level is on */
                }
								else 
								{ 
                 igTestFktFlag = FALSE ; 
                } 
              break ;


			case 998:

	/*--------------------------------------------------------------------*/
	/* the normal sequenz of this process is the simulation mode. This    */
	/* means, that the flag igRealImportFlag is set to FALSE. This        */
	/* incident can be solved by the event 998. The 998 calling causes a  */
	/* toggle to the igRealImportFlag.                                    */
	/*--------------------------------------------------------------------*/

                dbg (DEBUG, "---------------------------------") ; 
                if (igRealImportFlag == FALSE)
								{ 
								 dbg (DEBUG, "MAIN: real import is switched on!") ; 
                 debug_level = DEBUG ;     /* also debug level is on */
                 igRealImportFlag = TRUE ; 
                }
								else 
								{ 
								 dbg (DEBUG, "MAIN: real import is switched off!") ; 
                 igRealImportFlag = FALSE ; 
                } 
                dbg (DEBUG, "---------------------------------") ; 
              break ;

			default			:
				dbg(TRACE,"MAIN: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		} else 
			{
	  		/* Handle queuing errors */
  			HandleQueErr(ilRC);
  		} /* end else */
		
	} /* end for */
	
	/* exit(0);      not reached      */
	
} /* end of MAIN */

/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int Init_impmgr()
{
	int	ilRC = RC_SUCCESS;			/* Return code */
	/* now reading from configfile or from database */

  CheckImptabUsage();

	igInitOK = TRUE;
	return(ilRC);
	
} /* end of initialize */

/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
	int	ilRC = RC_SUCCESS;				/* Return code */
	
	dbg(TRACE,"Reset: now resetting");
	
	return ilRC;
	
} /* end of Reset */

/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate()
{
	/* unset SIGCHLD ! DB-Child will terminate ! */
	logoff();
	dbg(TRACE,"Terminate: now leaving ...");
	
	exit(0);
	
} /* end of Terminate */

/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int pipSig)
{
	int	ilRC = RC_SUCCESS;			/* Return code */
	dbg(TRACE,"HandleSignal: signal <%d> received",pipSig);
	switch(pipSig)
	{
	default	:
		Terminate();
		break;
	} /* end of switch */
	exit(0);
	
} /* end of HandleSignal */

/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
	int	ilRC = RC_SUCCESS;
	return;
} /* end of HandleErr */

/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
	int	ilRC = RC_SUCCESS;
	
	switch(pipErr) {
	case	QUE_E_FUNC	:	/* Unknown function */
		dbg(TRACE,"<%d> : unknown function",pipErr);
		break;
	case	QUE_E_MEMORY	:	/* Malloc reports no memory */
		dbg(TRACE,"<%d> : malloc failed",pipErr);
		break;
	case	QUE_E_SEND	:	/* Error using msgsnd */
		dbg(TRACE,"<%d> : msgsnd failed",pipErr);
		break;
	case	QUE_E_GET	:	/* Error using msgrcv */
		dbg(TRACE,"<%d> : msgrcv failed",pipErr);
		break;
	case	QUE_E_EXISTS	:
		dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
		break;
	case	QUE_E_NOFIND	:
		dbg(TRACE,"<%d> : route not found ",pipErr);
		break;
	case	QUE_E_ACKUNEX	:
		dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
		break;
	case	QUE_E_STATUS	:
		dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
		break;
	case	QUE_E_INACTIVE	:
		dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
		break;
	case	QUE_E_MISACK	:
		dbg(TRACE,"<%d> : missing ack ",pipErr);
		break;
	case	QUE_E_NOQUEUES	:
		dbg(TRACE,"<%d> : queue does not exist",pipErr);
		break;
	case	QUE_E_RESP	:	/* No response on CREATE */
		dbg(TRACE,"<%d> : no response on create",pipErr);
		break;
	case	QUE_E_FULL	:
		dbg(TRACE,"<%d> : too many route destinations",pipErr);
		break;
	case	QUE_E_NOMSG	:	/* No message on queue */
		dbg(TRACE,"<%d> : no messages on queue",pipErr);
		break;
	case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
		dbg(TRACE,"<%d> : invalid originator=0",pipErr);
		break;
	case	QUE_E_NOINIT	:	/* Queues is not initialized*/
		dbg(TRACE,"<%d> : queues are not initialized",pipErr);
		break;
	case	QUE_E_ITOBIG	:
		dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
		break;
	case	QUE_E_BUFSIZ	:
		dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
		break;
	default			:	/* Unknown queue error */
		dbg(TRACE,"<%d> : unknown error",pipErr);
		break;
	} /* end switch */
         
	return;
} /* end of HandleQueErr */

/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
	int	ilRC = RC_SUCCESS;			/* Return code */
	int	ilBreakOut = FALSE;
	
	do
	{
		ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,igItemLen,(char *)&prgItem);
		/* depending on the size of the received item  */
		/* a realloc could be made by the que function */
		/* so do never forget to set event pointer !!! */
		prgEvent = (EVENT *) prgItem->text;	
		if( ilRC == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRC != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRC);
			} /* fi */
		
			switch( prgEvent->command )
			{
			case	HSB_STANDBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_COMING_UP	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_ACTIVE	:
				ctrl_sta = prgEvent->command;
				ilBreakOut = TRUE;
				break;	
			case	HSB_ACT_TO_SBY	:
				ctrl_sta = prgEvent->command;
				break;	
	
			case	HSB_DOWN	:
				/* whole system shutdown - do not further use que(),  */
				/* send_message() or timsch() !                       */
				ctrl_sta = prgEvent->command ;
				Terminate();
				break;	
	
			case	HSB_STANDALONE	:
				ctrl_sta = prgEvent->command ;
				ResetDBCounter();
				ilBreakOut = TRUE;
				break;	
			case	REMOTE_DB :
				/* ctrl_sta is checked inside */
				HandleRemoteDB(prgEvent);
				break;
			case	SHUTDOWN	:
				Terminate();
				break;
						
			case	RESET		:
				ilRC = Reset();
				break;
						
			case	EVENT_DATA	:
				dbg(TRACE,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
					
			case	TRACE_ON :
				dbg_handle_debug(prgEvent->command);
				break;
			case	TRACE_OFF :
				dbg_handle_debug(prgEvent->command);
				break;
			default			:
				dbg(TRACE,"HandleQueues: unknown event");
				DebugPrintItem(TRACE,prgItem);
				DebugPrintEvent(TRACE,prgEvent);
				break;
			} /* end switch */
		} else {
			/* Handle queuing errors */
			HandleQueErr(ilRC);
		} /* end else */
	} while (ilBreakOut == FALSE);
	if(igInitOK == FALSE)
	{
			ilRC = Init_impmgr();
			if(ilRC != RC_SUCCESS)
			{
				dbg(TRACE,"Init_impmgr: init failed!");
			} /* end of if */
	}/* end of if */
	/* OpenConnection(); */
} /* end of HandleQueues */
	

/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData()
{
	int     ilRC = RC_SUCCESS;			/* Return code */
  int     ilZ ;
	int     ilI ;
  int     iplSelFlg ;
	char    pclActCmd[16];
  char    pclChkCmd[64];
  char    pclUpdData[16];
  char    pclUpdSelKey[32];
  char    pclTmpBuf[32];
  char    cpAFldsStr[1024];
	char    cpDFldsStr[1024];
	char    cpADataStr[8182];
	char    cpDDataStr[8182];
  char    cpDataStr[8182] ;
  char    pclSelStr[1024] ;
  char    pclSelSubStr[10][128] ;
	char    pclHelpStr[8182] ; 
	char    *pclStrPos ; 
	BC_HEAD *prlBchd;
  CMDBLK  *prlCmdblk;
  char    *pclSelKey;
  char    *pclFields;
  char    *pclData;
  int     ilReady ; 
  char    *cpPtrStr ;
/* char    pcgDestName[200] ; */ 
  char    pclChkWks[128] ; 
	char    pclChkUsr[128] ; 

	iplSelFlg = 0 ;         /* reset selection flag */ 
  if (igTestFktFlag)
  {
   dbg (DEBUG, "HandleData >>") ; 
  }
  prlBchd   = (BC_HEAD *)((char *)prgEvent + sizeof(EVENT));
  prlCmdblk = (CMDBLK *)((char *)prlBchd->data);
  pclSelKey = (char *)prlCmdblk->data;
  pclFields = (char *)pclSelKey + strlen(pclSelKey) + 1; 
  pclData   = (char *)pclFields + strlen(pclFields) + 1;


	/* Save Global Elements of BC_HEAD */
	/* WorkStation */
	memset (pcgRecvName, '\0', (sizeof(prlBchd->recv_name) + 1)) ; 
	strncpy (pcgRecvName, prlBchd->recv_name, sizeof(prlBchd->recv_name)) ;

	/* User */
	memset (pcgDestName, '\0', sizeof(prlBchd->recv_name)) ; 
	strcpy (pcgDestName, prlBchd->dest_name) ;   /* UserLoginName */
  sprintf (pclChkWks, ",%s,", pcgRecvName) ; 
	sprintf (pclChkUsr, ",%s,", pcgDestName) ; 

	/* SaveGlobal Info of CMDBLK */
	strcpy (pcgTwStart, prlCmdblk->tw_start) ; 
	strcpy (pcgTwEnd, prlCmdblk->tw_end) ; 
  strcpy (pclActCmd,prlCmdblk->command);

  dbg(TRACE,"CMD <%s>  WKS <%s> USR <%s>",
						pclActCmd, pcgRecvName, pcgDestName) ; 
  dbg(TRACE,"SPECIAL INFO TWS <%s>  TWE <%s> ", 
						pcgTwStart, pcgTwEnd ); 

  dbg(TRACE,"... FIELDS <%s>", pclFields); 
  dbg(TRACE,"... DATA   <%s>", pclData); 
  dbg(TRACE,"... SELECT <%s>", pclSelKey);
  importFlag = 0 ; 
	previewFlag = 0 ; 


  if (prlBchd->rc == RC_FAIL)
  {
   return RC_SUCCESS ;
  } /* end if */

  strcpy(pclActCmd,prlCmdblk->command);
  sprintf(pclChkCmd,",%s,",pclActCmd);

  DebugPrintBchead(TRACE,prlBchd); 
  DebugPrintCmdblk(TRACE,prlCmdblk);
  dbg(TRACE, "CMD <%s>", pclActCmd);


  /* handle commands */

  if(strcmp(pclActCmd,"IMP") == 0)        /* **** IMPORT CMD **** */
  {
	 dbg (DEBUG, "------------------------------") ; 
	 dbg (DEBUG, "HandleData: get <IMP> commando") ; 
	 dbg (DEBUG, "------------------------------") ; 

   if (igTestFktFlag )
	 {
    dbg (DEBUG, "HandleData: ------ get selection fields ---------");  
    dbg (DEBUG, "HandleData: incoming Field string=<%s>", pclSelKey);  
   }
	 if (strstr (pclSelKey, "PREVIEW") != NULL) previewFlag = 1 ; 
	 if (strstr (pclSelKey, "IMPORT") != NULL) importFlag = 1 ;

   if (importFlag == 1) 
	 {
	  dbg (DEBUG, "SendCedaEvent --> FLIGHT IMPORT START IMX") ; 
	  SendCedaEvent(igToFlight,0,"import","EXCO",pcgTwStart,pcgTwEnd,
                  "IMX","AFTTAB","IMP,IMPORT,START","","","", 5,RC_SUCCESS) ;
   }
   if (previewFlag == 1) 
	 {
	  dbg (DEBUG, "SendCedaEvent --> FLIGHT PREVIEW START IMX") ; 
	  SendCedaEvent(igToFlight,0,"import","EXCO",pcgTwStart,pcgTwEnd,
                  "IMX","AFTTAB","IMP,PREVIEW,START","","","", 5,RC_SUCCESS) ;
   }


	 ilRC = ResetFldStruct (&sDepFldArr) ; 
   ilRC = ResetFldStruct (&sArrFldArr) ; 
 
	 strcpy (cpAFldsStr, pclFields) ; 
   if (strlen (cpAFldsStr) == 0)
   {
    ilRC = RC_FAIL ;
		dbg (TRACE, "HandleData: No fields for interpreted") ; 
   }

   if (ilRC == RC_SUCCESS) 
   {
   	if (igTestFktFlag )
  	{
     dbg (DEBUG, "HandleData: ------- get selection fields -----------") ;  
     dbg (DEBUG, "HandleData: incoming Field string = <%s>", cpAFldsStr) ;  
    }

		pclStrPos = strstr (cpAFldsStr, "\n") ;
		if (pclStrPos != NULL)
		{
     *pclStrPos = 0x00 ; 
    }
		dbg (DEBUG, "HandleData: cpAFldsStr = <%s>", cpAFldsStr) ; 
    ilZ = strlen (cpAFldsStr) ;
    strcpy (cpDFldsStr, &pclFields[ilZ+1]) ;
    pclStrPos = strstr (cpDFldsStr, "\n") ; 
    if ( pclStrPos != NULL) 
    {
     *pclStrPos = 0x00 ; 
		 dbg (DEBUG, "HandleData: incoming Field string P2 = <%s>", cpDFldsStr); 
		}

		if (ilRC == RC_SUCCESS)
		{
     /* fill in field information in data struct            */
	   if (igTestFktFlag ) dbg (DEBUG, "HandleData: CALL ARR functions") ; 
     fillFldStruct (cpAFldsStr, &sArrFldArr) ;        
	   if (igTestFktFlag ) dbg (DEBUG, "HandleData: CALL DEP functions") ;   
     fillFldStruct (cpDFldsStr, &sDepFldArr) ;       
    }

    /* additional fields !!! IMPORTANT ALWAYS BEHIND fillFldStruct !!! */ 
    ilRC = InsOrReplFld2Struct (&sArrFldArr, "ALC3", "" );  
    ilRC = InsOrReplFld2Struct (&sDepFldArr, "ALC3", "" );  
    ilRC = InsOrReplFld2Struct (&sArrFldArr, "FKEY", "" );  
    ilRC = InsOrReplFld2Struct (&sDepFldArr, "FKEY", "" );  
    ilRC = InsOrReplFld2Struct (&sArrFldArr, "URNO", "" );  
    ilRC = InsOrReplFld2Struct (&sDepFldArr, "URNO", "" );  
    ilRC = InsOrReplFld2Struct (&sArrFldArr, "RTAB", "AFT" );  
    ilRC = InsOrReplFld2Struct (&sDepFldArr, "RTAB", "AFT" );  

		/* ------- interpret EVENT DATA ------------------------*/
		if (ilRC == RC_SUCCESS)
		{ 
     strcpy (cpADataStr, pclData) ;  
     ilReady = RC_SUCCESS ;
		 do 
		 {
      strcpy (cpDataStr, cpADataStr) ;
			if (strlen (cpDataStr) > 0 ) 
			{
       ilZ = 0 ; 
       /* get next data pair */ 
  	   if (igTestFktFlag )
       {
        dbg (DEBUG, "HandleData: - DATA PAIR SPLIT -----------------------");  
        dbg (DEBUG, "HandleData: incoming Data string = <%s>", cpADataStr);  
       }
       pclStrPos = strstr (cpADataStr, "\n") ;
       if (pclStrPos != NULL)
       {
        *pclStrPos = 0x00 ; 
       } 
       else
       {
        dbg (DEBUG, "HandleData: no lines interpreted!" ) ; 
        ilReady = RC_FAIL ; 
       }
  	   if (igTestFktFlag )
       {
        dbg (DEBUG, "HandleData: (1) cpADataStr = <%s>", cpADataStr) ; 
       }
       ilZ += strlen (cpADataStr) + 1 ;
       strcpy (cpDDataStr, &cpDataStr[ilZ]) ;
       pclStrPos = strstr (cpDDataStr, "\n") ; 
       if ( pclStrPos != NULL) 
       {
		    *pclStrPos = 0x00 ;
        if (igTestFktFlag )
        {
         dbg (DEBUG, "HandleData: (2) cpDDataStr = <%s>", cpDDataStr) ; 
        }
			  ilZ += strlen (cpDDataStr) + 1 ; 
        ilRC = fillDataInFldStruct (cpADataStr, &sArrFldArr) ;   
        ilRC = fillDataInFldStruct (cpDDataStr, &sDepFldArr) ;   
        /*------------------------------------------------------------*/ 
	      /* Enable all fields, which must be used for insert,update,. */
	      /*------------------------------------------------------------*/
      	ilRC = setStatusInStruct ("", C_ENABLE, 0, &sArrFldArr) ;  
      	ilRC = setStatusInStruct ("", C_ENABLE, 0, &sDepFldArr) ;  

        ilRC = ManageImportData (&sArrFldArr, &sDepFldArr, iplSelFlg) ;
        strcpy (cpADataStr, &cpDataStr[ilZ]) ; 
			 }
			 else 
			 {
        if (igTestFktFlag) dbg(DEBUG,"HandleData: all data lines interpreted"); 
			  ilReady = RC_FAIL ; 
       }
      }
      else
      {
       if (igTestFktFlag) dbg(DEBUG, "HandleData: all data lines interpreted"); 
       ilReady = RC_FAIL ; 
      }
     } while (ilReady != RC_FAIL ) ; 
    } /* if SUCCESS */

    if (importFlag == 1) 
  	{
	   dbg (DEBUG, "SendCedaEvent --> FLIGHT IMPORT END IMX") ; 
	   SendCedaEvent(igToFlight,0,"import","EXCO",pcgTwStart,pcgTwEnd,
                   "IMX","AFTTAB","IMP,IMPORT,END","","","", 5,RC_SUCCESS) ;
    }
    if (previewFlag == 1) 
	  {
	   dbg (DEBUG, "SendCedaEvent --> FLIGHT PREVIEW END IMX") ; 
	   SendCedaEvent(igToFlight,0,"import","EXCO",pcgTwStart,pcgTwEnd,
                   "IMX","AFTTAB","IMP,PREVIEW,END","","","", 5,RC_SUCCESS) ;
    }

    /* pclDepFldsStr = all departure fields                       */
		/* pclArrFldStr  = all arrival fields                         */
    /* sDepFldArr    = structure for all departure fields         */
    /* sArrFldArr    = structure for all Arrival fields           */
	  gl_updateTest    = 0 ;   /* switch off the testmode, if exist */ 

   }   /* if SUCCESS */
  } 
  else if(strcmp(pclActCmd, "RVS") == 0)        /* **** RVS IMPORT CMD **** */
  {
  	dbg (DEBUG, "-----------------------------") ; 
	  dbg (DEBUG, "HandleData: get <RVS> command") ; 
	  dbg (DEBUG, "-----------------------------") ; 
 
    ilRC = ResetFldStruct (&sArrFldArr) ;    /* only use this struct */ 
	  strcpy (cpAFldsStr, pclFields) ; 
    if (strlen (cpAFldsStr) == 0)
    {
     ilRC = RC_FAIL ;
		 dbg (TRACE, "HandleData: No fields for interpreted") ; 
    }

    if (ilRC == RC_SUCCESS)        /* FIELD HANDLING */
    {
     if (igTestFktFlag )
  	 {
      dbg (DEBUG, "HandleData: ------- get selection fields -----------") ;  
      dbg (DEBUG, "HandleData: incoming Field string = <%s>", cpAFldsStr) ;  
     }
		 pclStrPos = strstr (cpAFldsStr, "\n") ;
		 if (pclStrPos != NULL)
		 {
      *pclStrPos = 0x00 ; 
     }
		 dbg (DEBUG, "HandleData: cpAFldsStr = <%s>", cpAFldsStr) ; 
		 if (ilRC == RC_SUCCESS)
		 {
      /* fill in: field information in data struct            */
	    if (igTestFktFlag ) dbg (DEBUG, "HandleData: FILL FIELDSSTRUCT") ; 
      fillFldStruct (cpAFldsStr, &sArrFldArr) ;        
     }
     /* additional fields !!! IMPORTANT ALWAYS BEHIND fillFldStruct !!! */ 
     ilRC = InsOrReplFld2Struct (&sArrFldArr, "ALC3", "" );  
     ilRC = InsOrReplFld2Struct (&sArrFldArr, "ALC2", "" );  
     ilRC = InsOrReplFld2Struct (&sArrFldArr, "AIRL", "" );  
     ilRC = InsOrReplFld2Struct (&sArrFldArr, "RTAB", "AFT" );  
	  } /* ilRC = RC_SUCCESS --- for field handling  */ 

		/* ------- interpret EVENT DATA ------------------------*/
		if (ilRC == RC_SUCCESS)
		{ 
     strcpy (cpADataStr, pclData) ;  
     ilReady = RC_SUCCESS ;
		 do 
		 {
      strcpy (cpDataStr, cpADataStr) ;
			if (strlen (cpDataStr) > 0 ) 
			{
       ilZ = 0 ; 
       /* get next data pair */ 
  	   if (igTestFktFlag )
       {
        dbg (DEBUG, "HandleData: - DATA PART INTERPRETING --------------");  
        dbg (DEBUG, "HandleData: incoming Data string = <%s>", cpADataStr);  
       }
       pclStrPos = strstr (cpADataStr, "\n") ;
       if (pclStrPos != NULL)
       {
        *pclStrPos = 0x00 ; 
       } 
       else
       {
        dbg (DEBUG, "HandleData: no lines interpreted!" ) ; 
        ilReady = RC_FAIL ; 
       }
  	   if (igTestFktFlag )
       {
        dbg (DEBUG, "HandleData: (1) cpADataStr = <%s>", cpADataStr) ; 
       }
       ilZ += strlen (cpADataStr) + 1 ;

       ilRC = fillDataInFldStruct (cpADataStr, &sArrFldArr) ;   
       strcpy (cpADataStr, &cpDataStr[ilZ]) ;
       /*------------------------------------------------------------*/ 
	     /* Enable all fields, which must be used for insert,update,. */
	     /*------------------------------------------------------------*/
       ilRC = setStatusInStruct ("", C_ENABLE, 0, &sArrFldArr) ;  

       DeleteFKeyList () ; 
       ilRC = GetRVSFKEYS (&sArrFldArr) ;

			}  /* if strlen > 0 -> more data aviable */
			else 
			{
       if (igTestFktFlag)
       {
        dbg (DEBUG, "HandleData: all data lines interpreted" ) ; 
       }
			 ilReady = RC_FAIL ; 
      }
     } while (ilReady != RC_FAIL ) ; 
    } /* if SUCCESS  if data handling */

		gl_updateTest    = 0 ;   /* switch off the testmode, if exist */ 
  } /* if command == RVS */
  else if(strcmp(pclActCmd, "IMPCN") == 0) /* Clear table IMPTAB */
  {
    ClearFkeyInsert ("=ALL=");
  }
  else if(strcmp(pclActCmd, "UFR") == 0) /* answer from fligth? */
  {
    dbg(TRACE, "answer from flight? ignored ...");
  }
  else if(strcmp(pclActCmd, "ISF") == 0) /* answer from fligth? */
  {
    dbg(TRACE, "answer from flight? ignored ...");
  }
  else       /* reached unknown command */
  {
    dbg(TRACE, "UNKNOWN COMMAND RECEIVED");
    dbg(TRACE, "=== DATA ELEMENTS FROM ORIGINATOR =======");
    dbg(TRACE, "... FIELDS <%s>", pclFields); 
    dbg(TRACE, "... DATA   <%s>", pclData); 
    dbg(TRACE, "... SELECT <%s>", pclSelKey);
    dbg(TRACE, "=========================================");
  }

	if (ilRC != RC_SUCCESS)
  {
   /* Handle Special Errors */
   dbg (TRACE, "FINAL RESULT: NOTHING TO DO") ;
   ilRC = RC_SUCCESS ;
  } /* end if */

  if (igTestFktFlag) dbg (DEBUG, "HandleData <<") ; 
  return ilRC;
} /* end of HandleData */






/* ***************************************************
* Function: 	  WriteNewFKeyListElement 
* Parameter:  	pcFKEY 
* Return:     	RC_SUCCESS, RC_FAIL
* Description:	insert a new list element  
* ***************************************************/
static int WriteNewFKEYListElement (char *pcFKEY)
{
 int   ilRC = RC_SUCCESS ;

 gsNewElement = (ST_LST_FKEY *) malloc (sizeof(struct stPtrF)) ;
 if (gsNewElement == NULL) 
 {
	dbg (TRACE, "MALLOC ERR for new list element!") ;
	ilRC = RC_FAIL ; 
 }
 else
 {
  if(gsActFKey == NULL)    /* first element in the list */
	{
	 gsActFKey   = gsNewElement ;
	 gsFirstFKey = gsNewElement ;
	 gsActFKey->prev = NULL ; 
	 gsActFKey->next = NULL ; 
	 strcpy (gsActFKey->FKEY, pcFKEY) ; 
  }
	else    /* new element in the existing list */
	{
	 gsHelpFKey = gsActFKey ;   /* save info to existing element */
	 gsActFKey = gsNewElement ; 
	 gsHelpFKey->next = gsActFKey ; 
	 gsActFKey->prev = gsHelpFKey ; 
	 strcpy (gsActFKey->FKEY, pcFKEY) ; 
	} /* existing element */
 }
 return (ilRC) ; 
} /* end of WriteNewFKEYListElement */



/* ***************************************************
* Function: 	  DeleteFKeyList 
* Parameter:  	 
* Return:     	RC_SUCCESS, RC_FAIL
* Description:	delete all list elements  
* ***************************************************/
static int DeleteFKeyList (void)
{
 int   ilRC = RC_SUCCESS ;

 gsHelpFKey = gsFirstFKey ; 
 if (gsHelpFKey == NULL) 
 {
  gsActFKey = NULL ;  
  gsFirstFKey = NULL ;  
 }
 else
 {
	while (gsHelpFKey->next != NULL)   /* search the end of the list */ 
  {
	 /* dbg (DEBUG, "Delete Goto next list element") ; */ 
   gsHelpFKey = gsHelpFKey->next ;
	}
  while (gsHelpFKey->prev != NULL)
	{
	 gsActFKey = gsHelpFKey ; 
	 gsHelpFKey = gsHelpFKey->prev ;
	 gsActFKey->next = NULL ; 
	 gsActFKey->prev = NULL ; 
	 free (gsActFKey) ;
	 /* dbg (DEBUG, "Delete one list element") ; */ 
  }
	gsHelpFKey->next = NULL ; 
	free (gsHelpFKey) ; /* destroy last element */
  gsActFKey = NULL ;  
  gsFirstFKey = NULL ;  
	/* dbg (DEBUG, "Delete last list element") ; */ 
 }	
 return (ilRC) ; 	
} /* deleteFKeyList () */



/* ***************************************************
* Function: 	  OutputFKeyList 
* Parameter:  	
* Return:     	RC_SUCCESS, RC_FAIL
* Description:	outputs all list FKEYS  
* ***************************************************/
static int OutputFKeyList (void)
{
 int   ilRC = RC_SUCCESS ;

 gsHelpFKey = gsFirstFKey ; 
 if (gsHelpFKey == NULL) 
 {
	/* dbg (DEBUG, "OutputFKeyList: No elements for OUTPUT") ; */ 
 }
 else
 {
	while (gsHelpFKey->next != NULL)   /* search the end of the list */ 
  {
	 /* dbg (DEBUG, "OutputFKeyList: FKEY = <%s>", gsHelpFKey->FKEY) ; */  
   gsHelpFKey = gsHelpFKey->next ; 
	}
	/* dbg (DEBUG, "OutputFKeyList: LAST FKEY = <%s>", gsHelpFKey->FKEY) ; */  
 }	
 return (ilRC) ; 	
} /* outputFKeyList () */



/* ***************************************************
* Function: 	  FKEYInList 
* Parameter:    *cpFKey, *iFKeyNotFound	
* Return:     	RC_SUCCESS, RC_FAIL
* Description:	compares all list FKEYS with cpFKey  
* ***************************************************/
static int FKEYInList (char *cpFKey, int *ipFKeyNotFound)
{
 int   ilRC = RC_SUCCESS ;
 int   iNotFound ; 

 *ipFKeyNotFound = TRUE ; 
 gsHelpFKey = gsFirstFKey ; 
 if (gsHelpFKey == NULL) 
 {
	dbg (DEBUG, "FKeyInList: No elements for SEARCH") ; 
 }
 else
 {
	iNotFound = TRUE ; 
	while ((gsHelpFKey->next != NULL) && (iNotFound)) /* search to end of list */ 
  {
	 if (strcmp (gsHelpFKey->FKEY, cpFKey) == 0) 
	 {
	  dbg (DEBUG, "FKEYInList: Found CMP FKEY = <%s> <%s>",
								gsHelpFKey->FKEY, cpFKey) ;
    iNotFound = FALSE ; 
    *ipFKeyNotFound = FALSE ; 
   }
   gsHelpFKey = gsHelpFKey->next ; 
	}
	dbg (DEBUG, "OutputFKeyList: LAST FKEY = <%s>", gsHelpFKey->FKEY) ;   
 }	
 return (ilRC) ; 	
} /* FKeyInList () */



/* ***************************************************
* Function: 	  GetRVSFKEYS 
* Parameter:  	Fieldstr,Datastr (for Arrival and Departure)
* Return:     	RC_SUCCESS, RC_FAIL
* Description:	checks flight records of validy. 
* ***************************************************/
static int GetRVSFKEYS (ST_EVT_DATA_HDL *pclFldArr)
{
  int   ilRC = RC_SUCCESS ; 
	char  pclHelpStr[1024] ;
	int   ilVpfr_Days ; 
	int   ilVpfrDay ; 
	int   ilVpto_Days ; 
	int   ilVptoDay ; 
	int   iFoundFKeys ; 
	int   ilLoopVpto_Days ; 
  int   iFKeyNotFound ; 
	int   ilLoopVpfr_Days ; 
	int   ilFreqCount ; 
  int   ilFreqVal ;
  int   ilWeekDay ; 		 
	int   ilDateOffset ;
  int   ilDayCount ; 
  int   ilValidWeek ;
	int   ilValidDays ; 
	int   ilToDate ; 
	int   ilFrDate ;
  int   ilChkFirstDate ; 
  int   ilLoopDay ; 
  int   ilChckDay ;
	char  clDateStr [64] ;
	char  pclLoopFreChrs[256] ; 
	char  clVPFR [256] ; 
  char  clVPTO [256] ; 
  char  clFRED [256] ; 
  char  clFRQW [256] ; 
  char  clFLTN [256] ; 
  char  clALC3 [256] ; 
  char  clFLNS [256] ; 
	char  clALC2 [256] ; 
	char  clADID [256] ; 
	char  clFKEY [256] ;
	char  clspFKey [256] ;
	char  clTBLName [12] ; 
	char  clExtTBLName [12] ; 
	char  pcgSearchFldLst[1024] ;
  int   ilGetRc ; 
  short sql_cursor ; 
  short sql_fkt ; 
  char  pcSqlStatement[1024] ; 
  char  clFKeyPool[2048] ; 
  char  cpData[1024] ;
	char  cpCondData[1024] ; 


	if (igTestFktFlag)
  {
   dbg (DEBUG, "GetRVSFKEYS >>") ; 
  }
	/*------------------------------------------------------------*/
	/* Read all important fields from the struct                  */
	/*------------------------------------------------------------*/
	/* Interpret the fields VPFR,VPTO,FRED,FRQW FOR ARRAVIAL AND DEPATURE */
  /* fields, we are important used */
	iFoundFKeys = FALSE ; 

	ilRC = 0 ; 
	ilRC = getFldDataFromStruct (clVPFR, "VPFR", *pclFldArr) ;  
	ilRC = getFldDataFromStruct (clVPTO, "VPTO", *pclFldArr) ;  
	ilRC = getFldDataFromStruct (clFRED, "FRQD", *pclFldArr) ;  
	ilRC = getFldDataFromStruct (clFRQW, "FRQW", *pclFldArr) ;  
	ilRC = getFldDataFromStruct (clFLNS, "FLNS", *pclFldArr) ;  
	ilRC = getFldDataFromStruct (clADID, "ADID", *pclFldArr) ; /* A/D */  
	ilRC = getFldDataFromStruct (clFLTN, "FLTN", *pclFldArr) ;  
	ilRC = getFldDataFromStruct (clALC3, "ALC3", *pclFldArr) ;  
	ilRC = getFldDataFromStruct (clALC2, "ALC2", *pclFldArr) ;  

  /* set table and extension name directly */
	strcpy (clTBLName, "AFT") ;  
	strcpy (clExtTBLName, "TAB") ;  

	/*---------------------------------------------------------------------*/
	/* create ALC3 code                                                    */
	/* its not secure, that the impmgr process gets the ALC3 code directly */
	/* So, impmgr needs a special field named AIRL. This field includes    */
	/* information about ALC2 and ALC3. The function SQLgetALC3Code must   */
	/* generate this ALC3 Code                                             */
	/* Important is, that the ALC3 Code is used for flight process inf.    */
	/* and the AIRL field is only for information                          */
	/*---------------------------------------------------------------------*/
 	if (strlen(clALC3) == 0)
	{
	 ilRC |= SQLgetALC3Code (clALC3, clALC2) ;  
   ilRC |= ReplaceFldDataInStruct(clALC3, "ALC3", pclFldArr) ;   
  }

	ilRC = getFldDataFromStruct (clFLNS, "FLNS", *pclFldArr) ;  
	if (ilRC == RC_FAIL)
	{
   ilRC = AddFld2Struct (pclFldArr, "FLNS", "#" );  
   strcpy (clFLNS, "#") ;
   ilRC |= ReplaceFldDataInStruct(clFLNS, "FLNS", pclFldArr) ;   
  }

  if (ilRC > 0) /* found not all important fields */
	{
	 ilRC = RC_SUCCESS ; 
	}
	else 
	{
   if (igTestFktFlag)
   {
    dbg (DEBUG, "GetRVSFKEYS: Found all fields -> Create all computed FKEYS!"); 
   }
	 ilRC = RC_SUCCESS ; 
	} 

  /* Interpret date and time */
  if (ilRC == RC_SUCCESS) 
	{
   ilRC |= getIntDaysFromStr (&ilVpfr_Days, &ilVpfrDay, clVPFR) ;
   ilRC |= getIntDaysFromStr (&ilVpto_Days, &ilVptoDay, clVPTO) ;
   ilRC = RC_SUCCESS ; 
		
   /* convert day frequenz and week sequenz */  
	 ilLoopVpfr_Days = ilVpfr_Days;
   ilLoopVpto_Days = ilVpto_Days;
   ilFreqCount     = FormatDayFreq (clFRED, pclLoopFreChrs) ;
   ilFreqVal       = atoi (clFRQW) ;
   ilWeekDay       = ilVpfrDay ;  
	 ilDateOffset    = 0;
	 dbg(DEBUG, "GetRVSFKEYS: FRED <%s> WEEKLY FREQ <%d>", clFRED, ilFreqVal) ;
   dbg(DEBUG, "GetRVSFKEYS: DAYS <%d>", ilFreqCount) ;

   if (ilFreqVal < 1)
   {
    ilFreqVal = 1;
   } /* end if */

	 if (ilFreqVal > 1)
   {
    dbg(TRACE, "GetRVSFKEYS: WEEKLY FREQUENCY IS %d", ilFreqVal) ;
   } /* end if */
  } /* if Success */

  ilDayCount = -1 ;
	ilValidDays = 0 ;
	ilValidWeek = TRUE ;
	ilToDate = ilLoopVpto_Days ; 
	ilFrDate = ilLoopVpfr_Days ;
  ilChkFirstDate = FALSE ; 

  if (ilRC == RC_SUCCESS) 
	{
	 for (ilLoopDay = ilLoopVpfr_Days; ilLoopDay <= ilLoopVpto_Days; ilLoopDay++)
   {
		dbg (DEBUG, "IMG -> TEST for the day <%d>", ilLoopDay) ; 
    ilDayCount++;
    if (ilValidWeek == TRUE)
    {
     ilChckDay = ilWeekDay - 1;
		 dbg (DEBUG, "GetRVSFKEYS: dayFrequenz|checkDay:<%s> | <%d>",
									pclLoopFreChrs, ilChckDay) ;  
     if (pclLoopFreChrs[ilChckDay] != ' ') 
     {
  		dbg(DEBUG, "GetRVSFKEYS: ---------------- HIT ------------------") ;

			ilChkFirstDate = TRUE ; 
      ilRC = FullDayDate ( clDateStr, ilLoopDay ) ;

			clFKEY[0] = 0x00 ; 
			ilRC=createFKEY(clFKEY,clFLTN,clALC3,clFLNS,clDateStr,'A'); 
      dbg (DEBUG, "FKEY = <%s>", clFKEY) ; 
      ilRC = WriteNewFKEYListElement (clFKEY) ; /* add to the temporary list */
      iFoundFKeys = TRUE ; 
		 } /* if loop Freq chars */		

	   ilValidDays++;
     if (ilValidDays == 7)  
     {
      ilValidDays = (ilFreqVal - 1) * 7;
      if (ilValidDays > 0)
      {
       ilValidWeek = FALSE ;
      } 
     } 
    }
    else
    {              /* Skip Over Not Valid Days */
     ilValidDays-- ;
     if (ilValidDays < 1)
     {
      ilValidWeek = TRUE;
     } 
    } 

    if (ilChkFirstDate) 
	  {
	   ilToDate++ ; 
	   ilFrDate++ ;
    }

    ilWeekDay++;
    if (ilWeekDay > 7)
    {
     ilWeekDay = 1 ;
    } /* end if */
   } /* end for */
  } /* if RC_SUCCESS */

 OutputFKeyList () ; /* help function, to see the act. temprary list */

 if (iFoundFKeys)  
 {
	/* create the <like FKEY> clspFKEY */
  strcpy (clspFKey, clFKEY) ; 
	clspFKey[9] = 0x00 ;    /* cut FKey string at date start */
	sprintf (&clspFKey[9], "%%%%%%%%%%%%%%%%") ;    /* set wildcards */ 

  /* create the SQL select statment */
  /* put in information for SQL statement */
  if (clADID[0] == 'A') /* read all arrival FKEYS */
  {
	 strcat (clspFKey, "A") ; 
   sprintf (pcSqlStatement, 
	 "SELECT FKEY FROM %s%s WHERE (STOA BETWEEN '%s' AND '%s') AND FKEY LIKE '%s'"
	 , clTBLName, clExtTBLName, clVPFR, clVPTO, clspFKey ) ;
  } 
  else   /* select statement for departure */ 
  {
	 strcat (clspFKey, "D") ; 
   sprintf (pcSqlStatement, 
	 "SELECT FKEY FROM %s%s WHERE (STOD BETWEEN '%s' AND '%s') AND FKEY LIKE '%s'"
	 , clTBLName, clExtTBLName, clVPFR, clVPTO, clspFKey ) ;
  }
  if (igTestFktFlag)
  {
   dbg (DEBUG, "SQLSelectRecord: SQL SELECT STATEMENT = <%s>", pcSqlStatement); 
  }

  sprintf (cpData, cpCondData) ;        /* prepare data pool */ 
  sql_cursor = 0 ; 
  sql_fkt = START ; 
  clFKeyPool[0] = 0x00 ;  

  /* ilGetRc = DB_SUCCESS ; TEST !!!! */ 
	do 
  {
   /* ilGetRc = sql_if (sql_fkt, &sql_cursor, pcSqlStatement, cpData) ; */
	 if (ilGetRc == DB_SUCCESS) 
   {
	  /* strcpy (cpData, "00123ac3#20000924A") ; TEST */ /* nicht vorhanden */
	  /* strcpy (cpData, "00123ac3#20000923A") ; TEST */ /* vorhanden */
	  cpData[18] = 0x00 ;
	  dbg (DEBUG, "FOUND SQL FKEY Is <%s>", cpData) ; 
    /* search, if this record is a normal FKEY */
	  /* if not so, added to the field FKEY List */ 
    ilRC = FKEYInList (cpData, &iFKeyNotFound) ; 
    if (iFKeyNotFound) 
	  {
		 strcat (clFKeyPool, cpData) ;  
     strcat (clFKeyPool, "/n") ;  /* set end marker */ 
    }
   }
   sql_fkt = NEXT ; 
	 /* ilGetRc = RC_FAIL ; TEST !!!!! */
  } while (ilGetRc == DB_SUCCESS) ; 
  close_my_cursor (&sql_cursor) ; 

  /* if a FKEY list exist, send this to the flight */ 
  if (ilRC == RC_SUCCESS) 
  {
	 if (clFKeyPool[0] != 0x00) 
	 {
		dbg (DEBUG, "SendCedaEvent --> FLIGHT RVS (5)") ; 
    SendCedaEvent(igToFlight, 0, "import","EXCO",pcgTwStart,pcgTwEnd,
                 "RVS", "AFTTAB", "clArrFKEY", "FKEY", clFKeyPool,
                 "", 5, RC_SUCCESS) ;
    dbg (DEBUG, "STF: -> RVS IMPORT --- (not really) send to flight INSERT");
    dbg (DEBUG, "STF: -> Command = RVS") ;  
    dbg (DEBUG, "STF: -> UsedFields = FKEY") ;  
    dbg (DEBUG, "STF: -> UsedData   = <%s>", clFKeyPool) ;  
    dbg (DEBUG, "STF: -> KEY        = FKEY") ;  
   }
  } /* if RC_SUCCESS */

 } /* if found fKeys */
 if (igTestFktFlag) dbg (DEBUG, "GetRVSFKEYS <<") ; 
 return (ilRC) ; 

} /* end of GetRVSFKEYS () */







/* ***************************************************
* Function: 	  ManageImportData
* Parameter:  	Fieldstr,Datastr (for Arrival and Departure)
* Return:     	RC_SUCCESS, RC_FAIL
* Description:	checks flight records of validy. 
* ***************************************************/
static int ManageImportData (ST_EVT_DATA_HDL *pclArrFldArr,  
														 ST_EVT_DATA_HDL *pclDepFldArr,  
														 int  iplFlag)
{
  int ilRC = RC_SUCCESS ; 
  int ilGetRc = 0 ; 
  int ilDGetRc = 0 ; 
	int ilItemNo = 0 ; 
	int ilLen = 0 ;
	int ilLoopVpfr_Days; 
	int ilLoopVpto_Days; 
	int ilDepVpfr_Days; 
	int ilAriVpto_Days; 
	int ilAriVpfr_Days; 
	int ilDepVpto_Days;
	int ilMinVal ; 
	int ilAriVpfrDay ; 
	int ilAriVptoDay ; 
  int ilAriStoa_Days ;
 	int ilAriStod_Days ; /* 20040319 JIM */
  int ilDepStoa_Days ; /* 20040319 JIM */
	int ilDepStod_Days ;
  int ilAriStoaDay ;
	int ilAriStodDay ;
  int ilDepStoaDay ;
	int ilDepStodDay ;
	int ilDepVpfrDay ; 
	int ilDepVptoDay ;
	int ilFreqaCount ; 
	int ilFreqdCount ; 
  int ilFreqVal ;
  int ilLoopDay ; 
  int ilValidWeek ;
	int ilValidDays ; 
  int ilWeekDay ; 		 
  int ilChckDay ;
	int iChgFlg ;
	int iDChgFlg ;
	int ilRotation ;
	char clAriALC3 [256] ; 
	char clAriFLNS [256] ; 
	char clAriFLTN [256] ; 
	char clAriSTOA [256] ; 
  char clAriSTOD [256] ; 
	char clAriVPFR [256] ; 
  char clAriVPTO [256] ; 
  char clAriFRED [256] ; 
  char clAriFRQW [256] ; 
	char clDepALC3 [256] ; 
	char clDepFLNS [256] ; 
	char clDepFLTN [256] ; 
	char clDepSTOA [256] ; 
  char clDepSTOD [256] ; 
  char clDepVPFR [256] ; 
  char clDepVPTO [256] ; 
  char clDepFRED [256] ; 
  char clDepFRQW [256] ;
	char pclAriFreChrs [256] ; 
  char pclDepFreChrs [256] ;
	char pclLoopFreChrs[256] ; 
  char clArrFKEY [256] ;
  char clDepFKEY [256] ;
	char clDateStr [64] ;
	char pclHelpStr[1024] ;
	char pcgSearchAFldLst[1024] ;
	char pcgSearchDFldLst[1024] ;
	char pclFldLst[1024] ;
	char pclDFldLst[4092] ;
	char pclIMC_AFKey[256] ;
	char pclIMC_DFKey[256] ;
	char cpTemp[1024] ;
  int  ilChkFirstDate ; 
  int  ilSqlStrItem ;
	int  pclSQLVal [128] ; 
  char pclCmdToUse[32];
  ST_CNT_HDL stCnt ; 
	ST_EVT_DATA_HDL sChgFldsFromEvent ;
	ST_EVT_DATA_HDL sChgFldsFromSelect ;  


  if (igTestFktFlag)
  {
   dbg (DEBUG, "ManageImportData >>") ; 
  }

	ilRC = resetCounters (&stCnt) ;  

	/*---------------------------------------------------------------------*/
	/* create ALC3 code                                                    */
	/* its not secure, that the impmgr process gets the ALC3 code directly */
	/* So, impmgr needs a special field named AIRL. This field includes    */
	/* information about ALC2 and ALC3. The function SQLgetALC3Code must   */
	/* generate this ALC3 Code                                             */
	/* Important is, that the ALC3 Code must send to the flight process    */
	/* and the AIRL field is only for information                          */
	/*---------------------------------------------------------------------*/
 	ilRC = getFldDataFromStruct (pclHelpStr, "AIRL", *pclArrFldArr) ;  
 	if (pclHelpStr[0] != 0x00)
	{
	 ilRC |= SQLgetALC3Code (clAriALC3, pclHelpStr) ;  
   ilRC |= ReplaceFldDataInStruct(clAriALC3, "ALC3", pclArrFldArr) ;   
  }
  ilRC |= getFldDataFromStruct (pclHelpStr, "AIRL", *pclDepFldArr) ;  
 	if (pclHelpStr[0] != 0x00)
	{
 	 ilRC |= SQLgetALC3Code (clDepALC3, pclHelpStr) ;  
   ilRC |= ReplaceFldDataInStruct(clDepALC3, "ALC3", pclDepFldArr) ;   
	}

	ilRC = getFldDataFromStruct (clAriFLNS, "FLNS", *pclArrFldArr) ;  
	if (ilRC == RC_FAIL)
	{
   ilRC = AddFld2Struct (pclArrFldArr, "FLNS", "#" );  
   strcpy (clAriFLNS, "#") ;
   ilRC |= ReplaceFldDataInStruct(clAriFLNS, "FLNS", pclArrFldArr) ;   
  }
	ilRC = getFldDataFromStruct (clDepFLNS, "FLNS", *pclDepFldArr) ;  
	if (ilRC == RC_FAIL)
	{
   ilRC = AddFld2Struct (pclDepFldArr, "FLNS", "#" );  
   strcpy (clDepFLNS, "#") ;
   ilRC |= ReplaceFldDataInStruct(clDepFLNS, "FLNS", pclDepFldArr) ;   
  }

  
  /*------------------------------------------------------------*/ 
	/* Disable all fields, which must not be sent to the flight   */
	/* We use the field doInsert (== C_INSERT) of the struct      */
	/*------------------------------------------------------------*/
  ilRC = setStatusInStruct ("FKEY", C_DISABLE, C_INSERT, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("AIRL", C_DISABLE, C_INSERT, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("VPFR", C_DISABLE, C_INSERT, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("VPTO", C_DISABLE, C_INSERT, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("FRQD", C_DISABLE, C_INSERT, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("FRQW", C_DISABLE, C_INSERT, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("MLIN", C_DISABLE, C_INSERT, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("ELIN", C_DISABLE, C_INSERT, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("URNO", C_DISABLE, C_INSERT, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("RTAB", C_DISABLE, C_INSERT, pclArrFldArr) ;  

  ilRC = setStatusInStruct ("FKEY", C_DISABLE, C_INSERT, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("AIRL", C_DISABLE, C_INSERT, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("VPFR", C_DISABLE, C_INSERT, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("VPTO", C_DISABLE, C_INSERT, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("FRQD", C_DISABLE, C_INSERT, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("FRQW", C_DISABLE, C_INSERT, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("MLIN", C_DISABLE, C_INSERT, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("ELIN", C_DISABLE, C_INSERT, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("URNO", C_DISABLE, C_INSERT, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("RTAB", C_DISABLE, C_INSERT, pclDepFldArr) ;  

  /*------------------------------------------------------------*/ 
	/* Enable all fields, which must be used for SQL select       */
	/* We use the field doSelect (== C_SELECT) of the struct      */
	/*------------------------------------------------------------*/
  ilRC = setStatusInStruct ("", C_ENABLE, C_SELECT, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("URNO", C_ENABLE,  C_SELECT, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("FKEY", C_DISABLE, C_SELECT, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("AIRL", C_DISABLE, C_SELECT, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("VPFR", C_DISABLE, C_SELECT, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("VPTO", C_DISABLE, C_SELECT, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("FRQW", C_DISABLE, C_SELECT, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("FRQD", C_DISABLE, C_SELECT, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("ALC3", C_DISABLE, C_SELECT, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("MLIN", C_DISABLE, C_SELECT, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("ELIN", C_DISABLE, C_SELECT, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("FLTN", C_DISABLE, C_SELECT, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("FLNS", C_DISABLE, C_SELECT, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("RTAB", C_DISABLE, C_SELECT, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("ALC3", C_ENABLE,  C_SELECT, pclArrFldArr) ;  

  ilRC = setStatusInStruct ("", C_ENABLE, C_SELECT, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("URNO", C_ENABLE,  C_SELECT, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("FKEY", C_DISABLE, C_SELECT, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("AIRL", C_DISABLE, C_SELECT, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("VPFR", C_DISABLE, C_SELECT, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("VPTO", C_DISABLE, C_SELECT, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("FRQW", C_DISABLE, C_SELECT, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("FRQD", C_DISABLE, C_SELECT, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("ALC3", C_DISABLE, C_SELECT, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("MLIN", C_DISABLE, C_SELECT, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("ELIN", C_DISABLE, C_SELECT, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("FLTN", C_DISABLE, C_SELECT, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("FLNS", C_DISABLE, C_SELECT, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("RTAB", C_DISABLE, C_SELECT, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("ALC3", C_ENABLE, C_SELECT, pclDepFldArr) ;  


  /*------------------------------------------------------------*/ 
	/* Enable all fields, which are needed for sql string compare */
	/* We use the field doFldCmd (== C_FLDCMP) of the struct      */
	/*------------------------------------------------------------*/
  ilRC = setStatusInStruct ("FKEY", C_DISABLE, C_FLDCMP, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("AIRL", C_DISABLE, C_FLDCMP, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("MLIN", C_DISABLE, C_FLDCMP, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("ELIN", C_DISABLE, C_FLDCMP, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("VPFR", C_DISABLE, C_FLDCMP, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("VPTO", C_DISABLE, C_FLDCMP, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("FRQW", C_DISABLE, C_FLDCMP, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("FRQD", C_DISABLE, C_FLDCMP, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("URNO", C_DISABLE, C_FLDCMP, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("RTAB", C_DISABLE, C_FLDCMP, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("ALC3", C_ENABLE, C_FLDCMP, pclArrFldArr) ;  

  ilRC = setStatusInStruct ("AIRL", C_DISABLE, C_FLDCMP, pclDepFldArr) ;        
  ilRC = setStatusInStruct ("FKEY", C_DISABLE, C_FLDCMP, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("MLIN", C_DISABLE, C_FLDCMP, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("ELIN", C_DISABLE, C_FLDCMP, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("VPFR", C_DISABLE, C_FLDCMP, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("VPTO", C_DISABLE, C_FLDCMP, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("FRQW", C_DISABLE, C_FLDCMP, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("FRQD", C_DISABLE, C_FLDCMP, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("URNO", C_DISABLE, C_FLDCMP, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("RTAB", C_DISABLE, C_FLDCMP, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("ALC3", C_ENABLE, C_FLDCMP, pclDepFldArr) ;  


  /*------------------------------------------------------------*/ 
	/* Enable all fields, which are needed for update a flight    */
	/* We use the field doUpdate (== C_UPDATE) of the struct      */
	/*------------------------------------------------------------*/
  ilRC = setStatusInStruct ("FKEY", C_DISABLE, C_UPDATE, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("AIRL", C_DISABLE, C_UPDATE, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("VPFR", C_DISABLE, C_UPDATE, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("VPTO", C_DISABLE, C_UPDATE, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("FRQD", C_DISABLE, C_UPDATE, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("FRQW", C_DISABLE, C_UPDATE, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("MLIN", C_DISABLE, C_UPDATE, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("ELIN", C_DISABLE, C_UPDATE, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("URNO", C_DISABLE, C_UPDATE, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("RKEY", C_DISABLE, C_UPDATE, pclArrFldArr) ;  
  ilRC = setStatusInStruct ("ALC3", C_ENABLE, C_UPDATE, pclArrFldArr) ;  

  ilRC = setStatusInStruct ("FKEY", C_DISABLE, C_UPDATE, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("AIRL", C_DISABLE, C_UPDATE, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("VPFR", C_DISABLE, C_UPDATE, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("VPTO", C_DISABLE, C_UPDATE, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("FRQD", C_DISABLE, C_UPDATE, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("FRQW", C_DISABLE, C_UPDATE, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("MLIN", C_DISABLE, C_UPDATE, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("ELIN", C_DISABLE, C_UPDATE, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("URNO", C_DISABLE, C_UPDATE, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("RKEY", C_DISABLE, C_UPDATE, pclDepFldArr) ;  
  ilRC = setStatusInStruct ("ALC3", C_ENABLE, C_UPDATE, pclDepFldArr) ;  


	/*------------------------------------------------------------*/
	/* Read all important fields from the struct                  */
	/*------------------------------------------------------------*/
	/* Interpret the fields VPFR,VPTO,FRED,FRQW FOR ARRAVIAL AND DEPATURE */
  /* fields, we are important used */

	ilRC = 0 ; 
	ilRC = getFldDataFromStruct (clAriVPFR, "VPFR", *pclArrFldArr) ;  
	ilRC = getFldDataFromStruct (clAriVPTO, "VPTO", *pclArrFldArr) ;  
	ilRC = getFldDataFromStruct (clAriFRED, "FRQD", *pclArrFldArr) ;  
	ilRC = getFldDataFromStruct (clAriFRQW, "FRQW", *pclArrFldArr) ;  
	ilRC = getFldDataFromStruct (clAriFLTN, "FLTN", *pclArrFldArr) ;  
	ilRC = getFldDataFromStruct (clAriSTOA, "STOA", *pclArrFldArr) ;  
	ilRC = getFldDataFromStruct (clAriSTOD, "STOD", *pclArrFldArr) ;   /* 20030611 JIM */

	/* ---------- get Departures fields ------------ */ 
	ilRC = getFldDataFromStruct (clDepVPFR, "VPFR", *pclDepFldArr) ;  
	ilRC = getFldDataFromStruct (clDepVPTO, "VPTO", *pclDepFldArr) ;  
	ilRC = getFldDataFromStruct (clDepFRED, "FRQD", *pclDepFldArr) ;  
	ilRC = getFldDataFromStruct (clDepFRQW, "FRQW", *pclDepFldArr) ;  
	ilRC = getFldDataFromStruct (clDepFLTN, "FLTN", *pclDepFldArr) ;  
	ilRC = getFldDataFromStruct (clDepSTOA, "STOA", *pclDepFldArr) ;   /* 20030611 JIM */
	ilRC = getFldDataFromStruct (clDepSTOD, "STOD", *pclDepFldArr) ;  

  if (ilRC > 0) /* found not all important fields */
	{
	 ilRC = RC_SUCCESS ; 
	}
	else 
	{
   if (igTestFktFlag)
   {
    dbg (DEBUG, "ManageImportData: Found all fields -> WORK!") ; 
   }
	 ilRC = RC_SUCCESS ; 
	} 

  /* Interpret date and time */
  if (ilRC == RC_SUCCESS) 
	{
   ilRC |= getIntDaysFromStr (&ilAriVpfr_Days, &ilAriVpfrDay, clAriVPFR) ;
   ilRC |= getIntDaysFromStr (&ilAriVpto_Days, &ilAriVptoDay, clAriVPTO) ;
   ilRC |= getIntDaysFromStr (&ilAriStoa_Days, &ilAriStoaDay, clAriSTOA) ;
   ilRC |= getIntDaysFromStr (&ilAriStod_Days, &ilAriStoaDay, clAriSTOD) ; /* 20040319 JIM */

   ilRC |= getIntDaysFromStr (&ilDepVpfr_Days, &ilDepVpfrDay, clDepVPFR) ;
   ilRC |= getIntDaysFromStr (&ilDepVpto_Days, &ilDepVptoDay, clDepVPTO) ;
   ilRC |= getIntDaysFromStr (&ilDepStoa_Days, &ilDepStodDay, clDepSTOA) ; /* 20040319 JIM */
   ilRC |= getIntDaysFromStr (&ilDepStod_Days, &ilDepStodDay, clDepSTOD) ;
   ilRC = RC_SUCCESS ; 
	
   /* convert day frequenz and week sequenz */  
   if (ilRC == RC_SUCCESS)
   {
  	ilRotation = 0 ;
	  if (clAriFLTN[0] != 0x00) ilRotation += 1  ; 
	  if (clDepFLTN[0] != 0x00) ilRotation += 2  ; 
    if (ilRotation == 0) ilRC = RC_FAIL ;     

    switch (ilRotation)
    {
		 case 1 :                         /* Arrival */
	          ilLoopVpfr_Days = ilAriVpfr_Days;
          	ilLoopVpto_Days = ilAriVpto_Days;
            ilFreqaCount    = FormatDayFreq (clAriFRED, pclLoopFreChrs) ;
            ilFreqVal       = atoi (clAriFRQW) ;
            ilWeekDay       = ilAriVpfrDay ;
						dbg(DEBUG, "ManageImportData: FRED <%s> WEEKLY FREQ <%d>",
											 clAriFRED, ilFreqVal) ;
					break ;

		 case 2 :                         /* Departure */
	          ilLoopVpfr_Days = ilDepVpfr_Days;
          	ilLoopVpto_Days = ilDepVpto_Days;
            ilFreqaCount    = FormatDayFreq (clDepFRED, pclLoopFreChrs) ;
            ilFreqVal       = atoi (clDepFRQW) ;
            ilWeekDay       = ilDepVpfrDay ;  
            dbg(DEBUG, "ManageImportData: FREA <%s> WEEKLY FREQ <%d>",
											 clDepFRED, ilFreqVal) ;
					break ;

		 case 3 :                         /* Arrival and Departure */
	          ilLoopVpfr_Days = ilAriVpfr_Days;
          	ilLoopVpto_Days = ilAriVpto_Days;
            ilFreqaCount    = FormatDayFreq (clAriFRED, pclLoopFreChrs) ;
            ilFreqVal       = atoi (clAriFRQW) ;
            ilWeekDay       = ilAriVpfrDay ;  

            dbg(DEBUG, "ManageImportData: FREA <%s> WEEKLY FREQ <%d>",
											 clAriFRED, ilFreqVal) ;
					break ;
    } ;

    dbg(DEBUG, "ManageImportData: DAYS <%d>", ilFreqaCount) ;

    if (ilFreqVal < 1)
    {
     ilFreqVal = 1;
    } /* end if */

		if (ilFreqVal > 1)
    {
     dbg(TRACE, "ManageImportData: WEEKLY FREQUENCY IS %d", ilFreqVal) ;
    } /* end if */
   } /* if Success */

	 ilValidDays = 0 ;
	 ilValidWeek = TRUE ;
   ilChkFirstDate = FALSE ; 
	 pclIMC_AFKey[0] = 0x00 ;
	 pclIMC_DFKey[0] = 0x00 ;

   if (ilRC == RC_SUCCESS) 
	 {
		for (ilLoopDay = ilLoopVpfr_Days; ilLoopDay <= ilLoopVpto_Days; ilLoopDay++)
    {
		 dbg (DEBUG, "IMG -> TEST for the day <%d>", ilLoopDay) ; 
     if (ilValidWeek == TRUE)
     {
      ilChckDay = ilWeekDay - 1;
			dbg (DEBUG, "ManageImportData: dayFrequenz|checkDay:<%s> | <%d>",
									pclLoopFreChrs, ilChckDay) ;  
      if (pclLoopFreChrs[ilChckDay] != ' ') 
      {
  		 dbg(DEBUG, "ManageImportData: ---------------- HIT ------------------") ;

			 (stCnt.iHitCounter)++ ;  
  		 dbg(DEBUG, "ManageImportData: HIT!! <%ld> FOR CASE %d",
									stCnt.iHitCounter, ilRotation);
			 if ((ilRotation == 1) || (ilRotation == 3))   /* check arrival */
       {
			  dbg  (DEBUG, "ManageImportData: CASE 1 -------------------: Arrival") ; 
				ilChkFirstDate = TRUE ; 
        ilRC = FullDayDate ( clDateStr, ilAriStoa_Days ) ;

				clArrFKEY[0] = 0x00 ; 
				ilRC=createFKEY(clArrFKEY,clAriFLTN,clAriALC3,clAriFLNS,clDateStr,'A'); 
	      strcpy (pclIMC_AFKey, clArrFKEY) ;  
        dbg (DEBUG, "FKEY = <%s>", clArrFKEY) ; 
				ilRC = ReplaceFldDataInStruct (clArrFKEY, "FKEY", pclArrFldArr ) ;  

        ilRC=createFldAndDataLst(pclFldLst,pcgDataArea,C_SELECT,*pclArrFldArr);
        ilRC = SQLSelectRecord (&ilGetRc, pclFldLst, pcgDataArea, 
			  						     		    pcgTblNam, pcgTblExt, "FKEY", clArrFKEY ) ;

				/* --------- for import and preview ------------ */
				dbg (DEBUG, "ManageImportData: Select data = <%s>", pcgDataArea) ;  
        ilRC |= FullDayDate ( clDateStr, ilAriStoa_Days ) ;
				strcpy (&clDateStr[8], &clAriSTOA[8]) ;  
        ilRC |= ReplaceFldDataInStruct (clDateStr, "STOA",  pclArrFldArr) ;  
        /* 20030611 JIM: if STOD is given for arrival, replace this date part also */
        if (clAriSTOD[0]!=0)
        {
          /* arrival time may not be before departure time */
          if (strcmp(&clAriSTOD[8],&clAriSTOA[8]) > 0)
          { /* arrival : move departure to day before */
             ilRC |= FullDayDate ( clDateStr, ilAriStoa_Days - 1) ;
			       dbg (DEBUG, "ManageImportData: arrival : move departure to day before <%s>", clDateStr) ;  
          }
          strcpy (&clDateStr[8], &clAriSTOD[8]) ;  
          ilRC |= ReplaceFldDataInStruct (clDateStr, "STOD",  pclArrFldArr) ;  
	        dbg (DEBUG, "ManageImportData: using STOD <%s>", clDateStr) ;  
        }
				if (ilGetRc == TRUE) 
			  {                                      /* --- found a record --- */
				 ilRC = getChgFlds (&iChgFlg, pclFldLst, pcgDataArea, pclArrFldArr, 
                            &sChgFldsFromEvent, &sChgFldsFromSelect) ;  
 
	  		 if (iChgFlg == TRUE)
			   {                 /* record was changed -> update record */
          /* read field URNO from select string and store in struct->URNO */
          ilRC = readDataItemFromStr(cpTemp, "URNO", pclFldLst, pcgDataArea);
          if (ilRC == RC_FAIL) 
	  			{
	  			 dbg(TRACE, "ManageImportData: cant read URNO FROM SELECT Str !!!") ; 
          }
				  else 
				  {
				   ilRC |= ReplaceFldDataInStruct (cpTemp, "URNO",  pclArrFldArr) ;  
				  } 

          (stCnt.iUpdateArrCounter)++ ;  
					dbg (DEBUG, "the record is changed !!! ") ; 
          /*
          if (previewFlag == 1)  
          {                                    * do something *
           ilRC = SQLInsertHistoryRecs (&sChgFldsFromEvent,  
							  					              &sChgFldsFromSelect, pclArrFldArr, 2 );
          }
          if (importFlag == 1)  
          {                                    * do something /
           ilRC = SQLInsertHistoryRecs (&sChgFldsFromEvent,  
							  					              &sChgFldsFromSelect, pclArrFldArr, 3 );
          }
          */
          ilRC=createFldAndDataLst(pclFldLst,pcgDataArea,C_UPDATE,*pclArrFldArr); 
          if (previewFlag == 1)  
          {                                    /* do something */
           /* write input to special table */ 
					 dbg (DEBUG, "STF: -> PREVIEW -- Send to flight UPDATE ") ;  
           dbg (DEBUG, "STF: -> Command = UFX") ;  
           dbg (DEBUG, "STF: -> UsedFields = <%s>", pclFldLst) ;  
           dbg (DEBUG, "STF: -> UsedData   = <%s>", pcgDataArea) ;
					 dbg (DEBUG, "STF: -> KEY =   <%s>", clArrFKEY) ;  
					}
          if (importFlag == 1)
					{
           ClearFkeyInsert (clArrFKEY);
		       dbg (DEBUG, "SendCedaEvent --> FLIGHT UFX (6) <%s>",clArrFKEY) ; 
		       dbg (DEBUG, "ManageImportData: FLD <%s>",pclFldLst) ; 
		       dbg (DEBUG, "ManageImportData: DAT <%s>",pcgDataArea) ; 
	         SendCedaEvent(igToFlight,0,"import","EXCO",pcgTwStart,pcgTwEnd,
                    "UFX","AFTTAB",clArrFKEY,pclFldLst,pcgDataArea,
                    "", 5,RC_SUCCESS);
				  } /* else import */
         } /* if changed flag == true */
         else     /* record found, but no changes */ 
				 {
          stCnt.iDoNotArrCounter++ ;  
    			dbg (DEBUG, "STF: -> FOUND ARRIVAL ARRIVAL/DEPARTURE, BUT") ; 
          dbg (DEBUG, "     -> NO CHANGES ARE IMPORTANT !!!") ;  
				 }
				} /* getLRC = TRUE, select record does not exist */

				else  /* cant find the select statement FKEY */ 
		  	{     /* ---- Insert arrival record does not exist ---- */ 
         stCnt.iInsertArrCounter++ ;  
         ilRC=createFldAndDataLst(pclFldLst,pcgDataArea,C_INSERT,*pclArrFldArr); 
         if (previewFlag == 1)  
         {                                    /* do something */
          /* write input to special table */ 
				  dbg (DEBUG, "STF: -> PREVIEW INSERT --- no SendCedaEvent()") ;  
          dbg (DEBUG, "STF: -> Command = ISX") ;  
          dbg (DEBUG, "STF: -> UsedFields = <%s>", pclFldLst) ;  
          dbg (DEBUG, "STF: -> UsedData   = <%s>", pcgDataArea) ;  
			  	dbg (DEBUG, "STF: -> KEY =        <%s>", clArrFKEY) ;  
				 }

				 if (importFlag == 1)
				 {
          strcpy(pclCmdToUse,"ISX");
          if (MarkFkeyInsert (clArrFKEY) == TRUE)
          {
             strcpy(pclCmdToUse,"UFX");
					   dbg (DEBUG, "SendCedaEvent --> insert pending, using UFX! ") ; 
          }
					dbg (DEBUG, "SendCedaEvent --> FLIGHT %s (1) <%s>",pclCmdToUse,clArrFKEY) ; 
          dbg (DEBUG, "ManageImportData: FLD <%s>",pclFldLst) ; 
	        dbg (DEBUG, "ManageImportData: DAT <%s>",pcgDataArea) ; 
	        SendCedaEvent(igToFlight,0,"import","EXCO",pcgTwStart,pcgTwEnd,
                   pclCmdToUse,"AFTTAB",clArrFKEY,pclFldLst,pcgDataArea,
                   "", 5,RC_SUCCESS);
         }

        }   /* else insert rec */ 
       } /* if rotation = arrival */ 


			if ((ilRotation == 2)  || (ilRotation == 3))
			{                                          /* check departure  */
       stCnt.iDepartureCounter++ ;  
       dbg (DEBUG,"ManageImportData: found departure : > dep. counter = <%ld>", 
                  stCnt.iDepartureCounter) ;  

			 dbg  (DEBUG, "ManageImportData: CASE 2 -------------------: Depature") ; 
			 ilChkFirstDate = TRUE ; 
       ilRC = FullDayDate ( clDateStr, ilDepStod_Days ) ;

			 clDepFKEY[0] = 0x00 ; 
			 ilRC=createFKEY(clDepFKEY,clDepFLTN,clDepALC3,clDepFLNS,clDateStr,'D'); 
	     strcpy (pclIMC_DFKey, clDepFKEY) ;  
			 ilRC = ReplaceFldDataInStruct (clDepFKEY, "FKEY", pclDepFldArr ) ;  

       ilRC=createFldAndDataLst(pclFldLst,pcgDataArea,C_SELECT,*pclDepFldArr);
       ilRC = SQLSelectRecord (&ilGetRc, pclFldLst, pcgDataArea, 
			 						     		    pcgTblNam, pcgTblExt, "FKEY", clDepFKEY ) ;
			 /* --------- for import and preview ------------ */

			 ilRC |= FullDayDate ( clDateStr, ilDepStod_Days ) ;
       strcpy (&clDateStr[8], &clDepSTOD[8]) ;  
       ilRC |= ReplaceFldDataInStruct (clDateStr, "STOD",  pclDepFldArr) ;  
       /* 20030611 JIM: if STOa is given for departure, replace this date part also */
       if (clDepSTOA[0]!=0)
       {
         /* arrival time may not be before departure time */
         if (strcmp(&clDepSTOD[8],&clDepSTOA[8]) > 0)
         { /* departure : move arrival to day after */
           ilRC |= FullDayDate ( clDateStr, ilDepStod_Days + 1 ) ;
		       dbg (DEBUG, "ManageImportData: arrival : move arrival to day after <%s>", clDateStr) ;  
         }
         strcpy (&clDateStr[8], &clDepSTOA[8]) ;  
         ilRC |= ReplaceFldDataInStruct (clDateStr, "STOA",  pclDepFldArr) ;  
         dbg (DEBUG, "ManageImportData: using STOA <%s>", clDateStr) ;  
       }
		   if (ilGetRc == TRUE) 
			 {                                      /* --- found a record --- */
			  dbg (DEBUG, "ManageImportData: Select data = <%s>", pcgDataArea) ;  
        ilRC = getChgFlds (&iChgFlg, pclFldLst, pcgDataArea, pclDepFldArr, 
                           &sChgFldsFromEvent, &sChgFldsFromSelect) ;  
	  		if (iChgFlg == TRUE)
				{                 /* record was changed -> update record */
         /* read field URNO from select string and store in struct->URNO */
         ilRC = readDataItemFromStr(cpTemp, "URNO", pclFldLst, pcgDataArea);
         if (ilRC == RC_FAIL) 
	  		 {
	  			dbg(TRACE, "ManageImportData: cant read URNO FROM SELECT Str !!!") ; 
         }
				 else 
				 {
				  ilRC |= ReplaceFldDataInStruct (cpTemp, "URNO",  pclDepFldArr) ;  
				 } 

         (stCnt.iUpdateDepCounter)++ ;  
         dbg (DEBUG, "the record is changed !!! ") ; 
         /*
         if (previewFlag == 1)
				 {
          ilRC = SQLInsertHistoryRecs (&sChgFldsFromEvent,  
			  									             &sChgFldsFromSelect, pclDepFldArr, 2 ) ;  
         }
         if (importFlag == 1)
				 {
          ilRC = SQLInsertHistoryRecs (&sChgFldsFromEvent,  
			  									             &sChgFldsFromSelect, pclDepFldArr, 3 ) ;  
         }
         */
         ilRC=createFldAndDataLst(pclFldLst,pcgDataArea,C_UPDATE,*pclDepFldArr);

         if (previewFlag == 1)  
         {                                    /* do something */
          /* write input to special table */ 
					dbg (DEBUG, "STF: -> PREVIEW INSERT --- Send to flight INSERT ") ;  
          dbg (DEBUG, "STF: -> Command = UFX") ;  
          dbg (DEBUG, "STF: -> UsedFields = <%s>", pclFldLst) ;  
          dbg (DEBUG, "STF: -> UsedData   = <%s>", pcgDataArea) ;  
			    dbg (DEBUG, "STF: -> KEY        = <%s>", clDepFKEY) ;  
				 }
				 if (importFlag == 1)
				 {
          ClearFkeyInsert (clDepFKEY);
				  dbg (DEBUG, "SendCedaEvent --> FLIGHT UFX (2) <%s>",clDepFKEY) ; 
          dbg (DEBUG, "ManageImportData: FLD <%s>",pclFldLst) ; 
          dbg (DEBUG, "ManageImportData: DAT <%s>",pcgDataArea) ; 
	        SendCedaEvent(igToFlight,0,"import","EXCO",pcgTwStart,pcgTwEnd,
                   "UFX","AFTTAB",clDepFKEY,pclFldLst,pcgDataArea,
                   "", 5,RC_SUCCESS);
         }
        } /* if changed flag == true */
        else     /* record found, but no changes */ 
				{
         stCnt.iDoNotDepCounter++ ;  
         dbg (DEBUG, "STF:found the record but no change exist !!! ") ; 
         dbg (DEBUG, "STF: -> Compare Fields = <%s>", pcgSearchAFldLst) ;  
         dbg (DEBUG, "STF: -> UsedData   = <%s>", pcgDataArea) ;  
				}
			 } /* getLRC = TRUE, select record does not exist */

			 else   /* cant find this FKEY record in the database */ 
		   {      /* ---- Insert departure record does not exist ---- */ 
        stCnt.iInsertDepCounter++ ;  
        ilRC=createFldAndDataLst(pclFldLst,pcgDataArea,C_INSERT,*pclDepFldArr); 
        if (previewFlag == 1)  
        {                                    /* do something */
         /* write input to special table */ 
				 dbg (DEBUG, "STF: -> PREVIEW INSERT --- no CedaSend()") ;  
         dbg (DEBUG, "STF: -> Command = ISX") ;  
         dbg (DEBUG, "STF: -> UsedFields = <%s>", pclFldLst) ;  
         dbg (DEBUG, "STF: -> UsedData   = <%s>", pcgDataArea) ;  
				}
        else       /* import */
        { 
          strcpy(pclCmdToUse,"ISX");
          if (MarkFkeyInsert (clDepFKEY) == TRUE)
          {
             strcpy(pclCmdToUse,"UFX");
					   dbg (DEBUG, "SendCedaEvent --> insert pending, using UFX! ") ; 
          }
					dbg (DEBUG, "SendCedaEvent --> FLIGHT %s (3) <%s>",pclCmdToUse,clDepFKEY) ; 
          dbg (DEBUG, "ManageImportData: FLD <%s>",pclFldLst) ; 
          dbg (DEBUG, "ManageImportData: DAT <%s>",pcgDataArea) ; 
	       SendCedaEvent(igToFlight,0,"import","EXCO",pcgTwStart,pcgTwEnd,
                   pclCmdToUse,"AFTTAB",clDepFKEY,pclFldLst,pcgDataArea,
                   "", 5,RC_SUCCESS);
        }
       }   /* else insert rec */

			 if (ilRotation == 3) 
			 {
        if (previewFlag == 1)  
        {                                    /* do something */
         dbg (DEBUG, "STF: -> PREVIEW APP/DEP. Inf. - send to flight ") ;  
         dbg (DEBUG, "STF: -> Command = JOX") ;  
         dbg (DEBUG, "STF: -> UsedFields = <%s>", pclFldLst) ;  
         dbg (DEBUG, "STF: -> UsedData   = <%s>", pcgDataArea) ;  
         dbg (DEBUG, "STF: -> Arrival FKEY <%s>", clArrFKEY) ;  
         dbg (DEBUG, "STF: -> Departu.FKEY <%s>", clDepFKEY) ;  
				}
				if (importFlag == 1) 
				{
				 dbg (DEBUG, "SendCedaEvent --> FLIGHT JOX (4)") ; 
				 SendCedaEvent(igToFlight,0,"import","EXCO",pcgTwStart,pcgTwEnd,
                   "JOX","AFTTAB",clArrFKEY,"Rotation link",clDepFKEY,
                   "", 5,RC_SUCCESS);
        } /* else if import */
			 } /* if rotation == 3 */
      } /* if case departure */  
		 } /* if loop chars */		

	  ilValidDays++;
    if (ilValidDays == 7)  
    {
     ilValidDays = (ilFreqVal - 1) * 7;
     if (ilValidDays > 0)
     {
      ilValidWeek = FALSE ;
     } 
    }
   } /* if valid days */
   else
   {
    /* Skip Over Not Valid Days */
    ilValidDays-- ;
    if (ilValidDays < 1)
    {
     ilValidWeek = TRUE;
    } 
   } 
   
   /* 20040319 JIM: increment all STOx day for every loop: */
   ilAriStoa_Days++ ;
	 ilAriStod_Days++ ; 
	 ilDepStod_Days++ ;
	 ilDepStoa_Days++ ; 

   ilWeekDay++;
   if (ilWeekDay > 7)
   {
    ilWeekDay = 1 ;
   } /* end if */
  } /* end for */

 } /* if RC_SUCCESS */

 } /* if RC_SUCCESS */

/* 
 if (previewFlag == 1)
 {
  ilRC = SqlIMCTabRecs (pclIMC_AFKey, pclIMC_DFKey, stCnt, 2) ;
 }
 if (importFlag == 1)
 {
  ilRC = SqlIMCTabRecs (pclIMC_AFKey, pclIMC_DFKey, stCnt, 3) ;
 }
*/
 if (igTestFktFlag)
 {
  dbg (DEBUG, "ManageImportData <<") ; 
 }
 return (ilRC) ; 

} /* end of ManageImportData () */






/*******************************************************************/
static int setSelectInfFlag (int *iplSelFlg, char *pclSelSubStr)
{
 int ilRC = RC_FAIL ; 

 if (igTestFktFlag)
 {
  dbg (DEBUG, "setSelectInfFlag >>") ; 
  dbg (DEBUG, "setSelectInfFlag: compare sub str is = <%s>", pclSelSubStr) ; 
 }

 if (strcmp (pclSelSubStr, "IMPORT") == 0) 
 {
	*iplSelFlg += C_IMPORT ; 
  ilRC = RC_SUCCESS ; 
 }
 if (strcmp (pclSelSubStr, "PREVIEW") == 0) 
 {
	*iplSelFlg += C_PREVIEW ; 
  ilRC = RC_SUCCESS ; 
 }

 if (igTestFktFlag)
 {
  dbg (DEBUG, "setSelectInfFlag: compare flag is = <%d>", *iplSelFlg) ; 
 }

 return (RC_SUCCESS) ; 

} /* function setSelectInfFlag() */


/*******************************************************************/
static int resetCounters (ST_CNT_HDL * stCntInf) 
{
 int ilRC = RC_SUCCESS ; 

 if (igTestFktFlag) dbg (DEBUG, "resetCounters >>") ; 
 stCntInf->iHitCounter = 0L ;  
 stCntInf->iArrivalCounter = 0L ;  
 stCntInf->iDepartureCounter = 0L ;  
 stCntInf->iInsertArrCounter = 0L ;  
 stCntInf->iUpdateArrCounter = 0L ;  
 stCntInf->iDoNotArrCounter  = 0L ;  
 stCntInf->iDoNotDepCounter  = 0L ; 
 stCntInf->iInsertDepCounter = 0L ;  
 stCntInf->iUpdateDepCounter = 0L ;  
 if (igTestFktFlag) dbg (DEBUG, "resetCounters <<") ; 

 return (ilRC) ; 

} /* resetCounters () */




/*******************************************************************/
static int getIntDaysFromStr (int *ilDayCnt, int *ilSelectDay, char *cpDayStr)  
{
 int ilMinVal ; 
 int ilRC = RC_SUCCESS ; 

 /* working with all fields */
 if (cpDayStr[0] == 0x00) 
 {
  *ilDayCnt = 0 ;
	*ilSelectDay = 0 ; 
 }
 else
 {
  ilRC = GetFullDay (cpDayStr, ilDayCnt, &ilMinVal, ilSelectDay) ;
  if (ilRC != RC_SUCCESS)
  {
   dbg (TRACE, "getIntDaysFromStr: ERR Can't interpret date = <%s>",
               cpDayStr ) ;
  }
 }
 return (ilRC) ; 
} /* getIntDaysFromStr () */ 



/******************************************************************************/
static int getChgFlds (int *iChgFlg, 
											 char *pcgSearchFldLst,      /* field list select */
                       char *pcgDataArea,          /* data list select */ 
											 ST_EVT_DATA_HDL *sFldArr, 
                       ST_EVT_DATA_HDL *sChgFldsFromEvent, 
                       ST_EVT_DATA_HDL *sChgFldsFromSelect
                      ) 
{
 /* compare the field/dataList with the struct values. Is in the struct */
 /* sFldArr the information <DO NOT COMPARE> set the data field are     */
 /* ignored. Additional all fields are compared and all changes are     */
 /* stored in the memory struct : sChgFldsFromEvent (Event/Old values   */
 /* from the field. sChgFldsFromSelect are simular the storage for the  */
 /* Select/New values for the changed fields                            */

 int ilRC = RC_SUCCESS ; 
 int iFndItem ; 
 int ilItemPos ;  
 int iDone ; 
 int i ; 
 int fldPos ;
 int fldLen ;
 int ilZ ;
 int ilDoStatus = 0 ; 
 int ilSqlStrItem ; 
 int ilStatus ; 
 int ilStatusS ; 
 int ilLen ; 
 char delim[2] ; 
 char pclActSqlField[32] ; 
 char pclRetStr[2096] ;
 char pclSQLVal[2096] ; 
 int  isVialFldNdx ; 
 int  iSpecialFld ;
 char	cpSVialSqlFld[2096] ;
 char cmpStr[2096] ;
 char cpVialOld [2096] ; 

 *iChgFlg = FALSE ;   /* set to TRUE, if any changes in the selected fields */ 
 fldPos = 0 ; 
 fldLen = 4 ;
 ilItemPos = 1 ; 
 ilStatus = C_NOTFOUND ;
 strcpy (delim, ",") ;                        /* used delimiter */ 
 iDone = FALSE ; 

 /* reset the fields from the storage field information structs */
 ilRC = ResetFldStruct (sChgFldsFromEvent) ; 
 ilRC = ResetFldStruct (sChgFldsFromSelect) ; 
 isVialFldNdx = 0 ;  
 do
 {
  /* read next field from selected SQL field */ 
  /* dbg(DEBUG,"getChgFlds: --- next -----------------------------------------"); */
	iFndItem = getDataFromDbStrs (&ilItemPos, pclActSqlField, pcgSearchFldLst, 
					    							  	pcgDataArea, pclSQLVal) ; 
  if (iFndItem == RC_SUCCESS)     /* found onother field !!! */
	{
   /* search this field in the struct */
   ilZ = 0 ;
   ilStatusS = C_NOTFOUND ; 
	 
   while ((sFldArr->Status[ilZ] == C_AKTIV) && (ilStatusS == C_NOTFOUND)) 
   {
    iSpecialFld = 0 ; 
		if (strcmp (pclActSqlField, sFldArr->Fieldname[ilZ]) == 0) 
    {
		 if (IsThisASpecialFld (pclActSqlField) == RC_SUCCESS)
		 {                                /* handle a special field */
/*
      if (strcmp (pclActSqlField, "VIAL") == 0) 
			{  * handle special condition VIAL *
       isVialFldNdx = 1 ; * ilZ ; *
			 strcpy (cpSVialSqlFld, pclSQLVal);
			 iSpecialFld = 1 ;
       ilDoStatus = 0 ;
			 ilStatusS = C_FOUND ; 
* +++ *  igTestFktFlag = TRUE;
			}
*/
		 }
     if (iSpecialFld == 0)
		 {
      strcpy (pclRetStr, sFldArr->FieldVal[ilZ]); 
      ilStatusS = C_FOUND ; 
      if (igTestFktFlag)
      {
       dbg (DEBUG, "getChgFlds: FOUND STRUCT VAL=<%s> FLD=<%s>",
  									 pclRetStr, sFldArr->Fieldname[ilZ]) ; 
      }
  	  if (sFldArr->DoFldCmp[ilZ] == C_ENABLE)
      {
       if (igTestFktFlag)
       {
		    dbg(DEBUG,"getChgFlds: Search Field <%s> is enabled !",pclActSqlField) ;
       }
  	   ilDoStatus = 1 ; 
      }
      else 
      {
       if (igTestFktFlag)
       {
	  	  dbg(DEBUG,"getChgFlds: Search Field <%s> is DISABLED !",pclActSqlField);
       }
		   ilDoStatus = 0 ; 
      }
     }
    }
    ilZ++ ; 
	 } /* while field in struct not found */ 

   if (ilStatusS == C_NOTFOUND)
   { 
    dbg(TRACE,"getChgFlds: CANT FIND FLD IN STRUCT: <%s>", pclActSqlField) ;
    if (isVialFldNdx != 0)  
    {
     spVIALCondition (cpSVialSqlFld, sFldArr, cmpStr, cpVialOld) ;   
     strcpy (pclRetStr, cmpStr); 
     dbg (DEBUG, "getChgFlds: xxx SQL Data Value is = <%s>", cpSVialSqlFld) ; 
     dbg (DEBUG, "getChgFlds: x compare SQL<%s> with EV<%s>",cpSVialSqlFld,                                                                  cmpStr); 
	   dbg (DEBUG, "getChgFlds: xxx compare Field = VIAL" );
     if (strcmp (/* pclRetStr */ cpSVialSqlFld, cmpStr) != 0) 
     {                            
      if (igTestFktFlag)
      {
	     dbg (DEBUG, "getChgFlds: Fld val was changed <%s>", pclActSqlField) ; 
      }
      AddFld2Struct (sChgFldsFromEvent,  "VIAL", cpVialOld /* pclRetStr */ ) ;  
      AddFld2Struct (sChgFldsFromSelect, "VIAL", cpSVialSqlFld ) ;  
      *iChgFlg = TRUE ;
     }
    }
    return (RC_FAIL) ;              /* Abort while not found */
	 }

   if (iSpecialFld == 0)
   {
	  if ((ilRC == RC_SUCCESS) && (ilDoStatus != 0))
    {       /* search for the value - get above !!! */
     if (strcmp (pclRetStr, pclSQLVal) != 0)  
     {                                           /* fieldval was changed */
      dbg (DEBUG,"getChgFlds: Field <%s>: CP: EV: <%s> => DB: <%s>", pclActSqlField,pclRetStr,pclSQLVal );
			/* write fieldinfo into the structs */
      AddFld2Struct (sChgFldsFromEvent,  pclActSqlField, pclRetStr ) ;  
      AddFld2Struct (sChgFldsFromSelect, pclActSqlField, pclSQLVal ) ;  
      *iChgFlg = TRUE ;
     }
     else
     {
      dbg (DEBUG,"getChgFlds: Field <%s>: OK: EV: <%s> == DB: <%s>", pclActSqlField,pclRetStr,pclSQLVal );
     }
		} /* if RC_SUCCESS */
   } /* if special */

  } /* if found more fields */ 
	else                           /* no more fields */ 
	{ 
	 iDone = TRUE ; 
	}
	ilItemPos++ ;               /* reset for next field */ 
	pclActSqlField[0]  = 0x00 ; 
 } while(iDone == FALSE) ; 

 if (isVialFldNdx != 0)  
 {
  ilRC = spVIALCondition (cpSVialSqlFld, sFldArr, cmpStr, cpVialOld) ;   
  strcpy (pclRetStr, cmpStr); 
  dbg (DEBUG, "getChgFlds: xxx SQL Data Value is = <%s>", cpSVialSqlFld) ; 
	dbg (DEBUG, "getChgFlds: x compare SQL<%s> with EV<%s>",cpSVialSqlFld,                                                                  cmpStr); 
	dbg (DEBUG, "getChgFlds: xxx compare Field = VIAL" );
  if (strcmp (/* pclRetStr */ cpSVialSqlFld, cmpStr) != 0) 
  {                            
   if (igTestFktFlag)
   {
	  dbg (DEBUG, "getChgFlds: Fld val was changed <%s>", pclActSqlField) ; 
   }
   AddFld2Struct (sChgFldsFromEvent,  "VIAL", cpVialOld /* pclRetStr */ ) ;  
   AddFld2Struct (sChgFldsFromSelect, "VIAL", cpSVialSqlFld ) ;  
   *iChgFlg = TRUE ;
  }
 }
/* +++ */  igTestFktFlag = FALSE;
 return (ilRC) ; 

} /* getChgFlds */



/**********************************************************************/
static int IsThisASpecialFld (char *cpField)
{
 int ilRC ; 
 int ilZ ;
 int iNotFound ; 

 if (igTestFktFlag)
 {
  dbg (DEBUG, "IsThisASpecialFld >>") ; 
 }
 ilRC = RC_FAIL ; 
 iNotFound = TRUE ; 
 ilZ = 0 ;
 while ((sgSpFields.Status[ilZ] == C_AKTIV) && (iNotFound == TRUE))
 {
	if (igTestFktFlag)
  {
   dbg (DEBUG, "IsThisASpecialFld: compare fields <%s> <%s>", cpField,
							 sgSpFields.Fieldname[ilZ]) ; 
  }
  if (strcmp (cpField, sgSpFields.Fieldname[ilZ]) == 0) 
	{
	 ilRC = RC_SUCCESS ;       /* found the field */
   iNotFound = FALSE ; 
   if (igTestFktFlag)
   {
    dbg (DEBUG, "IsThisASpecialFld: Found special field=<%s>", cpField) ; 
   }
  }
  ilZ++ ; 
  if (ilZ >= (C_MAX_SP_FIELDS - 1)) iNotFound = FALSE ; /* reached limit */
 }  

 if (igTestFktFlag)
 {
  dbg (DEBUG, "IsThisASpecialFld <<") ; 
 }
 return (ilRC) ;  

} /* IsThisASpecialFld */







/*************************************************************************/
static int spVIALCondition (char *cpDataArea, ST_EVT_DATA_HDL *sFldArr,
														char *compareStr, char *cpSTValOld)  
{
 int  ilRC ; 
 int  ilZ ;
 int  iSubStrTest ; 
 int  iFoundPos ; 
 int  isFldArrPos ; 
 char cplRecVal[2048] ; 
 char cplSearchSeq[12] ; 
 char cplCmpStr[2048] ; 
 int  ilCmpStrPos ;
 char cplSearchHlpStr[2048] ;
 char *pclStrPos ;
 char cplRecordValNew[2048] ; 
 int  iCmpSubStrLen ; 
 char cplNewVIALSubStr[1096] ;  

/* +++ */  igTestFktFlag = TRUE;
 if (igTestFktFlag)
 {
  dbg (DEBUG, "spVIALCondition >>") ; 
  dbg (DEBUG, "spVIALCondition: VIAL SQL data =<%s>", cpDataArea ) ; 
 }
 ilRC = RC_SUCCESS ; 
 ilZ = 0 ; 
 isFldArrPos = 0 ; 
 iCmpSubStrLen = 120 ; 
 iFoundPos = FALSE ;

 /* search the act. position for the field in the structure */
 while ((sFldArr->Status[isFldArrPos] == C_AKTIV) && (iFoundPos == FALSE))
 {
  if (strcmp (sFldArr->Fieldname[isFldArrPos], "VIAL") == 0)
	{
   /* found the position */
	 iFoundPos = TRUE ; 
	}
  else 
	{
   isFldArrPos++ ; 
	 if (isFldArrPos >= C_MAXFIELDS)
	 {
    ilRC = RC_FAIL ; 
		iFoundPos = TRUE ;      /* for abort */
	 }
  } /* not found else */
 } /* while not found fieldpos */

 if (ilRC == RC_SUCCESS) 
 {
  dbg (DEBUG, "spVIALCondition: found the item position for VIAL = <%d>",
							isFldArrPos) ; 
 } 
 else 
 {
  dbg (DEBUG, "spVIALCondition: found NOT the item position for VIAL!!!" ) ; 
 } 

 if (ilRC == RC_SUCCESS)
 {
	/* ------- create a copy of the struct string ---*/
	strcpy (cplRecVal, sFldArr->FieldVal[isFldArrPos]) ; /* inc. partly inf */ 
  strcpy (cpSTValOld, cplRecVal) ; 
	strcpy (cplCmpStr, cpDataArea) ;       /* includes all data information */ 

 
 /* ------- get searchStr ------------------------*/
  if (strlen (cplRecVal) >= 4)
	{ 
	 memcpy (cplSearchSeq, &cplRecVal[1], 3) ; 
	 cplSearchSeq[3] = 0x00 ; 
	} 
  else 
  {
	 /* do nothing -> cant create serach str */
	 dbg (DEBUG, "spVIALCondition: Cant create Search key !!!") ; 
  } 

  if (ilRC == RC_SUCCESS)
	{
   /* search for the first sub string in cmpStr */
   /* <cplCmpStr> has fixed length of 120 bytes */
   /* <cplRecordVal> can includes minimal information */
   /* ilCmpStrPos is the actually position in the cmpStr */
	 /* <cplRecordValNew> includes the new max. information */
   ilCmpStrPos = 0 ;   /* set on start position */
   cplRecordValNew[0] = 0x00 ;
	 ilZ = 0 ; 
   iSubStrTest = FALSE ; 
	 do 
	 {
    strcpy (cplSearchHlpStr, cplRecVal) ;
		if (strlen (cplSearchHlpStr) > 3 )   /* Airline typ must exist */ 
		{
		 dbg(DEBUG, "spVIALCondition: searching substring = <%s> in <%s>", cplSearchSeq,&cplSearchHlpStr[2]) ; 
     pclStrPos = strstr (&cplSearchHlpStr[2], cplSearchSeq) ;
     if (pclStrPos != NULL)
     {                         /* found a substring */
      pclStrPos-- ; 
      *pclStrPos = 0x00 ; 
      if (igTestFktFlag)
      {
			 dbg(DEBUG, "spVIALCondition: Found substring = <%s>", cplSearchHlpStr) ; 
      }
			/* create the append string */
			createVIALCompleteSubStr (cplNewVIALSubStr, cplSearchHlpStr, 
																&cplCmpStr[ilCmpStrPos], iCmpSubStrLen) ; 
      ilCmpStrPos += iCmpSubStrLen ; 
		  strcat (cplRecordValNew, cplNewVIALSubStr) ; 

			/* set new sub string information for the following substring */
      ilZ += strlen (cplSearchHlpStr) ; 
			strcpy (cplSearchHlpStr, &cplSearchSeq[ilZ]) ; 
     } 
     else                /* no more data in sub string */
     {
      if (igTestFktFlag)
      {
       dbg (DEBUG, "spVIALCondition: found no more substrings - use this" ) ; 
			 dbg (DEBUG, "spVIALCondition: Found substring = <%s>", cplSearchHlpStr);
      }
      /* create the append string */
			ilRC = createVIALCompleteSubStr (cplNewVIALSubStr, cplSearchHlpStr, 
						       										&cplCmpStr[ilCmpStrPos], iCmpSubStrLen) ; 
      ilCmpStrPos += iCmpSubStrLen ; 
		  strcat (cplRecordValNew, cplNewVIALSubStr) ;
			iSubStrTest = TRUE ; 
     }
    } /* if strlen > 2 */
    else
		{
     iSubStrTest = TRUE ; 
		} 
   } while ( iSubStrTest == FALSE ) ;

	 /* so write the new VIAL STRING into the act. STRUCT FIELDVALUE */
   /* strcpy (sFldArr->FieldVal[isFldArrPos], cplRecordValNew) ; */ 
   strcpy (compareStr, cplRecordValNew) ; 

  } /* ilRC == RC_SUCCESS */
 } /* if ilRC == RC_SUCCESS */
  
 if (igTestFktFlag)
 {
  dbg (DEBUG, "spVIALCondition <<") ; 
 }
/* +++ */  igTestFktFlag = FALSE;
 return (ilRC) ; 

} /* spVIALCondition */




/**********************************************************************/
static int createVIALCompleteSubStr ( char *cpNewVIALSubStr, char *cpSubStr,
																			char *cpCompleteStr, int iStrlen )  
{
 int    ilRC ;
 int    ilZ ;
 int    ivsql ; 
 size_t ilN ; 
 int    ilSubStrLen ;
 int    ilSQLStrLen ;
 int    ilStructStrLen ; 
 int    ilCmpStrLen ; 


 ivsql = 0 ;          /* SQL STRING wird angepasst */
 ivsql = 1 ;          /* STRUCT STRING WIRD ANGEPASST */
/* +++ */  igTestFktFlag = TRUE;
 if (igTestFktFlag)
 {
  dbg (DEBUG, "createVIALCompleteSubStr >>") ; 
 }
 ilRC = RC_SUCCESS ; 
 ilZ = 0 ;
 ilN = iStrlen ;
 ilSQLStrLen    = strlen (cpCompleteStr) ;
 ilStructStrLen = strlen (cpSubStr) ; 
 ilCmpStrLen    = strlen (cpCompleteStr) ;

 dbg (DEBUG, "VIALCompleteSubStr: len of SQL string = <%d> ", ilSQLStrLen) ; 
 dbg (DEBUG, "VIALCompleteSubStr: len of STR string = <%d> ", ilStructStrLen) ; 
 dbg (DEBUG, "VIALCompleteSubStr: SQL Str is : <%s>", cpCompleteStr) ; 
 dbg (DEBUG, "VIALCompleteSubStr: Struct Str is : <%s>", cpSubStr) ; 
 memset (cpNewVIALSubStr, 0x00, ilN) ; 
 ilZ = 4 ; 
 if (strlen(cpSubStr) == 0) 
 {
  ilZ = 0 ; 
  cpNewVIALSubStr[0] = 0x00 ;   	
 }
 else 
 {
	memcpy (cpNewVIALSubStr, cpSubStr, 4) ; 
 } 
 
 if (ivsql == 1) /* struct string wird angepasst */
 {
	dbg (DEBUG, "VIALCompleteSubStr: SQL -> Struct String") ; 
  if (ilSQLStrLen > ilStructStrLen)
	{
	 iStrlen = ilSQLStrLen ; 
	 do 
	 {
	  if (cpCompleteStr[ilZ] == ' ') 
	  {
     cpNewVIALSubStr[ilZ] = cpSubStr[ilZ] ;
	  }
	  else 
	  {
	   cpNewVIALSubStr[ilZ] = cpCompleteStr[ilZ] ; 
	  }
    ilZ++ ; 
   } while (ilZ < iStrlen) ;
   cpNewVIALSubStr[iStrlen] = 0x00 ; 
  } 
 }

 if (ivsql == 0) /* SQL string wird angepasst */
 {
  if (ilStructStrLen > ilSQLStrLen)
	{
	 dbg (DEBUG, "VIALCompleteSubStr: Struct -> SQL String") ; 
	 iStrlen = ilStructStrLen ; 
	 do 
	 {
	  if (cpSubStr[ilZ] == ' ') 
	  {
     cpNewVIALSubStr[ilZ] = cpCompleteStr[ilZ] ;
	  }
	  else 
	  {
	   cpNewVIALSubStr[ilZ] = cpSubStr[ilZ] ; 
	  }
    ilZ++ ; 
   } while (ilZ < iStrlen) ;
   cpNewVIALSubStr[iStrlen] = 0x00 ; 
  } 
 }

 if (igTestFktFlag)
 {
  dbg (DEBUG, "VIALCompleteSubStr: New Cmp Str is : <%s>", cpNewVIALSubStr) ; 
  dbg (DEBUG, "createVIALCompleteSubStr <<") ; 
 }
/* +++ */  igTestFktFlag = FALSE;
 return (ilRC) ; 

} /*  createVIALCompleteSubStr() */





/**********************************************************************/
static int readDataItemFromStr( char *cpFldData, char *cpFldName, 
																char *cpFldLst, char *cpDataArea) 
{
 int ilRC ;
 int ilItem ; 
 
 ilRC = RC_SUCCESS ;
 ilItem = 0 ; 
 ilRC = getDataFromDbStrs(&ilItem, cpFldName, cpFldLst, cpDataArea, cpFldData) ;
 return (ilRC) ; 

} /* function readDataItemFromStr () */





/******************************************************************************/
static int getDataFromDbStrs (int *iItemNo, char *cFld,   
                              char *pcgSearchFldLst, /* field list select */
                              char *pcgDataArea,     /* data list select */ 
                              char *pcgFoundData
														 ) 
{
 /* if ilItemNo (Input != 0) : search the item (1..n) and return the  */
 /* fieldname and data. If not found return inItemNo = 0              */
 /* if cFld (Input != "") : search the fieldname and return the       */
 /* ilItemNo (1..n) and data. If not found return inItemNo = 0        */
 
 int  ilRC = RC_SUCCESS ; 
 int  ilLen ;
 int  ilFoundItem ; 
 int  ilStatus ; 

 ilStatus = C_NOTFOUND ;
 strcpy (pcgFoundData, "") ;        
 if(*iItemNo == 0)
 {                                   /* search with field */
	ilFoundItem = get_item_no (pcgSearchFldLst, cFld, 5) + 1 ; 
	if (ilFoundItem > 0)
	{ 
	 /* found the field -> read data  */
   ilLen = get_real_item ( pcgFoundData, pcgDataArea, ilFoundItem ) ; 
   if (ilLen > 0) 
	 {
  	ilStatus = C_FOUND ;
		/* dbg (DEBUG, "getDataFromDbStrs: found fielddata <%s> from field <%s>",
								pcgFoundData, cFld) ; */
   } 
	 else 
	 {
  	ilStatus = C_FOUND ;
		/* dbg(DEBUG,"getDataFromDbStrs: fielddata length == 0 for field <%s>", cFld); */
   }
	} 
  else      /* fld name not found */
	{
	 dbg(DEBUG,"getDataFromDbStrs: field <%s> not found", cFld); 
	}
 }
 else   /* search with item */
 {                                                        /* get Field */
  ilFoundItem = *iItemNo ; 
	ilLen = get_real_item ( cFld, pcgSearchFldLst, ilFoundItem ) ; 
  if (ilLen > 0) 
	{
   /* dbg (DEBUG, "getDataFromDbStrs: found field <%s> for item <%d>",
								cFld, ilFoundItem) ; */
   ilLen = get_real_item ( pcgFoundData, pcgDataArea, ilFoundItem ) ; 
   if (ilLen > 0) 
	 {
    ilStatus = C_FOUND ;
	  /* dbg (DEBUG, "getDataFromDbStrs: found fielddata <%s> from field <%s>",
  							pcgFoundData, cFld) ; */
   } 
   else 
	 {
    ilStatus = C_FOUND ;
	  /* dbg(DEBUG,"getDataFromDbStrs: fielddata length == 0 for field <%s>", cFld); */
   }
  } 
	else 
	{
	 /* dbg(DEBUG,"getDataFromDbStrs: field name length == 0 for item <%d>",ilFoundItem); */
  }
 } /* search with item */ 

 if (ilStatus == C_NOTFOUND) 
 {
	ilRC = RC_FAIL ;
 } 
 else 
 {
  ilRC = RC_SUCCESS ; 
  *iItemNo = ilFoundItem ; 
 }
 return (ilRC) ; 
} /* getDataFromDbStrs () */

 


/******************************************************************************/
static int createFKEY (char *clFKEY, char *clFLTN, char *clALC3, char *clFLNS,
											 char *clActDate, char clADID)
{
 int   ilRC = RC_SUCCESS ; 
 char  clhFltn[10] ; 
 char  clhDate[32] ; 

 strcpy (clhDate, clActDate) ;
 clhDate[8] = 0x00 ; 
 strcpy (clhFltn, clFLTN) ; 
 if (strlen(clFLTN) < 5) 
 {
  sprintf (clhFltn, "%#05s", clFLTN ) ;
 }
 if (strlen (clFLNS) == 0) 
 {
	strcpy (clFLNS, "#") ;
 }
 else 
 {
  if (clFLNS[0] == ' ')
  {
   strcpy (clFLNS, "#") ;
	}
 }
 
 sprintf (clFKEY, "%s%s%c%s%c", clhFltn, clALC3, clFLNS[0], clhDate, clADID) ; 
 if (igTestFktFlag)
 {
  dbg (DEBUG, "createFKey: ------------- FKEY = <%s>", clFKEY) ; 
 }
 return (ilRC) ; 

} /* function createFKey() */



/******************************************************************************/
static int FormatDayFreq(char *pcpFreq,char *pcpFreqChr)
{
  int ilIdx;
  int ilChrPos;
  int ilStrLen;
  int ilDayCnt = 0;
  char pclTmpChr[] = "X";
  strcpy(pcpFreqChr,"       ");
  ilStrLen = strlen(pcpFreq);
  for (ilChrPos=0; ilChrPos<ilStrLen; ilChrPos++)
  {
    if (((pcpFreq[ilChrPos] >= '1') && (pcpFreq[ilChrPos] <= '7')) ||
         (pcpFreq[ilChrPos] == 'X'))
    {
      if (pcpFreq[ilChrPos] == 'X')
      {
        pcpFreqChr[ilChrPos] = ilChrPos + '1';
      } /* end if */
      else
      {
       pclTmpChr[0] = pcpFreq[ilChrPos];
       ilIdx = atoi(pclTmpChr) - 1;
       pcpFreqChr[ilIdx] = pcpFreq[ilChrPos];
      } /* end else */
      ilDayCnt++;
    } /* end if */
  } /* end for */
  return ilDayCnt;
} /* end FormatDayFreq */


/******************************************************************************/
static int getItemCnt (char *cpFldStr, int *ipCnt)
{
 int   ilRC = RC_SUCCESS ; 
 int   ilZ ;
 int   ilCnt ; 
 int   ilReady ; 
 char  clSubStr1[1024] ;
 char  clSubStr[1024] ;
 char  *cpChrPos ; 

 ilZ = 0 ;
 ilCnt = 1 ; 
 ilReady = RC_SUCCESS ; 
 strcpy (clSubStr, cpFldStr) ;
 strcpy (clSubStr1, cpFldStr) ;
 if (strlen (clSubStr) < 3) ilCnt = 0 ;
 do 
 {
	strcpy (clSubStr1, clSubStr) ; 
  ilZ = 0 ; 
  cpChrPos = strstr (clSubStr, ",") ; 
  if (cpChrPos != NULL)                 /* more selections */
  {
	 ilCnt++ ; 
   *cpChrPos = 0x00 ;
   ilZ = strlen(clSubStr)+1 ;  
   strcpy (clSubStr, &clSubStr1[ilZ]) ;  
  }
	else
	{
   ilReady = RC_FAIL ;  
	}
 } while (ilReady != RC_FAIL ) ; 
 *ipCnt = ilCnt ;
 return (ilRC) ; 
 
} /* getItemCnt () */





/******************************************************************************/
static int SQLInsertHistoryRecs (ST_EVT_DATA_HDL *sEvtChgData, 
													       ST_EVT_DATA_HDL *sSqlChgData, 
													       ST_EVT_DATA_HDL *sFldData, int iplFlag)
{
 int   ilRC = RC_SUCCESS ; 
 int   ilNextUrno ; 
 int   ilGetRc ; 
 int   ilDBRc ; 
 int   iAction ;  
 short sql_cursor ; 
 short sql_fkt ; 
 char  cpUsedFlds[1024] ; 
 char  pcSqlStatement[1024] ; 
 char  cplValStr[1024] ; 
 int   ilItems ;
 int   dbAccessIsDone ; 
 char  cDateTimeStamp[iTIME_SIZE+1] ; 
 char  cpVALO [1024] ; 
 char  cpVALN [1024] ; 
 char  cpRFLD [1024] ; 
 char  cpRTAB [1024] ; 
 char  cpRURN [1024] ; 
 char  cpFKYN [1024] ;
 char  cpFKYD [1024] ;
 char  cpFKEY [1024] ; 
 char  cpUSEC [1024] ; 
 char  cpTRKY [1024] ; 
 char  cpWKSN [1024] ; 
 char  cpStat [2] ; 

 /* write in the following information for one record */
 /* URNO : new URNO for the record                    */
 /* CDAT : date timestamp                             */
 /* RTAB : table from select -> AFT  (par: reqTbl)    */
 /* RURN : URNO from select table                     */
 /* FKYN : FKEY partly info [0]..[8] and [18]         */
 /* FKYD : AFT.FKEY [10]..17]                         */
 /* RFLD : AFT.STOA from sFldStruct                   */
 /* VALO : chg. data from select stat. sSqlChgData    */
 /* VALN : chg. data from client stat. sChgData       */
 /* STAT : P for Preview, I for Import                */

 dbAccessIsDone = FALSE ; 
 strcpy (cpStat, "P") ;                /* for preview */
 if (iplFlag == 3) strcpy (cpStat, "I") ; 
 strcpy (cpUsedFlds, "URNO,CDAT,USEC,WKSN,TRKY,RTAB,RURN,FKYN,FKYD,RFLD,VALO,VALN,STAT") ; 
 ilRC = getItemCnt (cpUsedFlds, &ilItems) ;     
 ilRC = prepSqlValueStr (ilItems, cplValStr) ;  

 /* put in information for SQL statement */
 sprintf (pcSqlStatement, "INSERT INTO ICHTAB (%s) %s", cpUsedFlds, cplValStr) ; 
 sql_cursor = 0 ; 
 sql_fkt = START ;
 ilGetRc = DB_SUCCESS ; 
 iAction = C_FIRST ; 

 ilGetRc = getFldStruct (cpRFLD, cpVALO, iAction, sSqlChgData) ;  
 while ((ilGetRc != RC_FAIL) && (ilRC == RC_SUCCESS))  /* more changed datas */ 
 {
	/* create next history record */ 
  ilRC = getFldDataFromStruct (cpVALN, cpRFLD, *sEvtChgData) ; 
  iAction = C_NEXT ; 
	if (ilRC == RC_FAIL)
	{
	 /* found not the compare field !!! */
	 dbg (TRACE, "Found not the compare field <%s>", cpRFLD) ;
  } 
	else 
	{        /* get all other dates */
   ilRC = GetNextUrnoItem (&ilNextUrno) ;     /* get next Urno */ 
   if (ilRC != RC_SUCCESS) 
   {
    dbg(TRACE,"SQLInsertHistoryRecs: !!! GET NO URNO for insert history !!!"); 
    return (RC_FAIL) ; 
   }
	 else    /* all other not critical dates */ 
	 {
    strcpy (cpUSEC, pcgDestName) ; 
    strcpy (cpTRKY, pcgTwStart) ;
		if (strlen (cpTRKY) == 0) strcpy (cpTRKY, " ") ; 
    strcpy (cpWKSN, pcgRecvName) ; 
    ilRC = GetLocalTimeStamp(cDateTimeStamp) ; 
    ilRC |= getFldDataFromStruct (cpFKEY, "FKEY", *sFldData) ; 
    ilRC |= getFldDataFromStruct (cpRTAB, "RTAB", *sFldData) ; 
    ilRC |= getFldDataFromStruct (cpRURN, "URNO", *sFldData) ; 
    if (ilRC != RC_SUCCESS)
		{
     dbg (TRACE, "SQLInsertHistoryRecs: One of the the fields doesnt exists %s",                 "DateTimeStamp, FKEY, RTAB, RURN") ; 
		}
    else 
		{
 /* create datastr like:                                       */ 
 /* flds : URNO,CDAT,USEC,WKSN,TRKY,RTAB,RURN,FKYN,FKYD,RFLD,VALO,VALN,STAT   */
 /* len  :  10   14   32   32    8   32   10   10    8   4    64   64   1     */
     strcpy (cpFKYN, cpFKEY) ;      /* 0..8 + 18  */ 
     cpFKYN[9] = cpFKEY[17] ;
		 cpFKYN[10] = 0x00 ;
		 strcpy (cpFKYD, &cpFKEY[9]) ; 
		 cpFKYD[8] = 0x00 ; 
     sprintf (cplValStr, "%#010d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
		                     ilNextUrno, cDateTimeStamp,
                         cpUSEC, cpWKSN, cpTRKY, 
												 cpRTAB, cpRURN, cpFKYN, cpFKYD,
												 cpRFLD, cpVALO, cpVALN, cpStat ) ;  
     dbg (DEBUG, "SELECT SQL VALUE STRING IS: <%s>", cplValStr) ; 

		} 
    if (ilRC != RC_FAIL) 
		{ 
		 /* write this into the history database */
     dbg (DEBUG, "write the chages to the database" ) ; 
     delton (cplValStr) ;  
     ilDBRc = sql_if (sql_fkt, &sql_cursor, pcSqlStatement, cplValStr) ;
     /*  ilDBRc = RC_FAIL ; */    /* REMOVE THIS AFTER TEST !!! */ 
		 dbAccessIsDone = TRUE ; 
     if (ilDBRc != DB_SUCCESS) 
     {
      dbg (TRACE, "-------------------------------------------------") ; 
  	  dbg (TRACE, "SQLInsertRecord : CANT INSERT RECORD stat.: <%s>",
  		  					pcSqlStatement) ; 
  	  dbg (TRACE, "-------------------------------------------------") ; 

  	  ilRC = RC_FAIL ;
			if (gl_updateTest == 1) ilRC = RC_SUCCESS ; 
     }
		 else 
		 {
			commit_work() ; 
     }

    }
	 } /* other criticals */

	} /* found a compare field */
  /* getNextchaged value */
	ilGetRc = getFldStruct (cpRFLD, cpVALO, iAction, sSqlChgData) ;  
 } /* while not all chaged fields interpreted */

 if (dbAccessIsDone == TRUE)
 {
  close_my_cursor (&sql_cursor) ; 
 } 
 
 return (ilRC) ;

} /* SQLInsertHistoryRecs() */




/******************************************************************************/
static int  SqlIMCTabRecs (char *cpAFKEY, char *cpDFKEY, ST_CNT_HDL stCnt,
													 int iplFlag) 
{
 int   ilRC = RC_SUCCESS ; 
 int   ilNextUrno ; 
 int   ilGetRc ; 
 int   ilDBRc ; 
 int   iAction ;  
 short sql_cursor ; 
 short sql_fkt ; 
 char  cpUsedFlds[1024] ; 
 char  pcSqlStatement[1024] ; 
 char  cplValStr[1024] ; 
 int   ilItems ;
 char  cDateTimeStamp[iTIME_SIZE+1] ; 
 char  cpAFKYN [1024] ;
 char  cpDFKYN [1024] ;
 char  cpUSEC [1024] ; 
 char  cpTRKY [1024] ; 
 char  cpWKSN [1024] ; 
 char  cpStat [2] ; 

 /* write in the following information for one record */
 /* URNO : new URNO for the record                    */
 /* CDAT : date timestamp                             */
 /* USEC : table from select -> AFT  (par: reqTbl)    */
 /* WKSN : WORKSTATION                                */
 /* TRKY : Transaction key                            */
 /* FKYN : FKEY partly info [0]..[8] and [18]         */
 /* CALH : counters all hits                          */
 /* CARH : counters all arrival hits                  */
 /* CEDH : counters all departure hits                */
 /* CIAR : counters all Arrival Inserts               */
 /* CUAR : counters all Arrival Updates               */
 /* CNAR : counters all Arrival do nothing            */
 /* CIDE : counters all Departure Inserts             */
 /* CUDE : counters all Departure Updates             */
 /* CNDE : counters all Departure do nothing          */
 /* STAT : P for Preview, I for Import                */

 strcpy (cpStat, "P") ;                /* for preview */
 if (iplFlag == 3) strcpy (cpStat, "I") ; 
 strcpy (cpUsedFlds, 
 "URNO,CDAT,USEC,WKSN,TRKY,FKYA,FKYD,CALH,CARH,CDEH,CIAR,CUAR,CNAR,CIDE,CUDE,CNDE,STAT") ; 
 ilRC = getItemCnt (cpUsedFlds, &ilItems) ;     
 ilRC = prepSqlValueStr (ilItems, cplValStr) ;  

 /* put in information for SQL statement */
 sprintf (pcSqlStatement, "INSERT INTO IMCTAB (%s) %s", cpUsedFlds, cplValStr) ; 
 sql_cursor = 0 ; 
 sql_fkt = START ;
 ilGetRc = DB_SUCCESS ; 
 iAction = C_FIRST ; 

 ilRC = GetNextUrnoItem (&ilNextUrno) ;     /* get next Urno */ 
 if (ilRC != RC_SUCCESS) 
 {
  dbg(TRACE,"SqlIMCTabRecs: !!! GET NO URNO for insert history !!!"); 
  return (RC_FAIL) ; 
 }
 else    /* all other not critical dates */ 
 {
  strcpy (cpUSEC, pcgDestName) ; 
  strcpy (cpTRKY, pcgTwStart) ;
  if (strlen (cpTRKY) == 0) strcpy (cpTRKY, " ") ; 
  strcpy (cpWKSN, pcgRecvName) ; 
  ilRC = GetLocalTimeStamp(cDateTimeStamp) ; 
  if (ilRC != RC_SUCCESS)
	{
   dbg (TRACE, "SQLIMCTabRecs: Err by create Date Time stamp") ; 
	}
  else 
	{
   /* create datastr like:                                       */ 
   /* flds : URNO,CDAT,USEC,WKSN,TRKY,FKYN,CALH,CARH,CDEH,CIAR,  */
   /*        CUAR,CNAR,CIDE,CUDE,CNDE,STAT                       */
   /* len  : 10 14 32 32 8 10 10 8 8 8 8 8 8 8 8 8 1             */

	 if (strlen (cpAFKEY) > 0) 
	 {
    strcpy (cpAFKYN, cpAFKEY) ;      /* 0..8 + 18  */ 
    cpAFKYN[9] = cpAFKEY[17] ;
    cpAFKYN[10] = 0x00 ;
   } 
	 else strcpy (cpAFKYN, " ") ; 
	 if (strlen (cpDFKEY) > 0) 
	 {
	  strcpy (cpDFKYN, cpDFKEY) ;      /* 0..8 + 18  */ 
    cpDFKYN[9] = cpDFKEY[17] ;
    cpDFKYN[10] = 0x00 ;
   }
	 else strcpy (cpDFKYN, " ") ; 
		sprintf (cplValStr,
    "%#010d,%s,%s,%s,%s,%s,%s,%8ld,%8ld,%8ld,%8ld,%8ld,%8ld,%8ld,%8ld,%8ld,%s",
		        ilNextUrno, cDateTimeStamp, cpUSEC, cpWKSN, cpTRKY, 
	 		  		cpAFKYN, cpDFKYN, stCnt.iHitCounter, stCnt.iArrivalCounter,
				  	stCnt.iDepartureCounter, stCnt.iInsertArrCounter,
				    stCnt.iUpdateArrCounter, stCnt.iDoNotArrCounter,
			  	  stCnt.iInsertDepCounter, stCnt.iUpdateDepCounter,
				    stCnt.iDoNotDepCounter, cpStat ) ;  
   dbg (DEBUG, "SqlIMCTabRecs: SELECT SQL VALUE STRING IS: <%s>", cplValStr) ; 
 
	 /* write this into the IHC database table */
   dbg (DEBUG, "SqlIMCTabRecs: write the statistik to the database" ) ; 
   delton (cplValStr) ;  
   ilDBRc = sql_if (sql_fkt, &sql_cursor, pcSqlStatement, cplValStr) ;
   /*  ilDBRc = RC_FAIL ; */    /* REMOVE THIS AFTER TEST !!! */ 
   if (ilDBRc != DB_SUCCESS) 
   {
    dbg (TRACE, "-------------------------------------------------") ; 
    dbg (TRACE, "SqlIMCTabRecs: CANT INSERT RECORD stat.: <%s>",
  	  					pcSqlStatement) ; 
    dbg (TRACE, "-------------------------------------------------") ; 

    ilRC = RC_FAIL ;
		if (gl_updateTest == 1) ilRC = RC_SUCCESS ; 
   }
	 else 
	 {
		commit_work() ; 
   }
   close_my_cursor (&sql_cursor) ; 
  } /* time,date ok */
 } /* urno ok */

 return (ilRC) ;

} /* SqlIMCTabRecs () */






/*******************************************************************/
static int prepSqlValueStr (int iItems, char *cpValStr) 
{
 int   ilRC = RC_SUCCESS ; 
 int   ilGetRc ; 
 int   ilZ ; 
 int   ilFirst ;
 char  cpDivStr[2] ; 
 char  cpVal[20] ; 

 strcpy (cpDivStr, ",") ;
 ilFirst = TRUE ; 
 strcpy (cpValStr, "VALUES (") ; 
 if (iItems == 0 ) return (RC_FAIL) ; 
 for (ilZ = 0; ilZ < iItems; ilZ++)
 {
	if (ilFirst == FALSE) strcat (cpValStr, cpDivStr) ;
  ilFirst = FALSE ;    
  sprintf (cpVal, ":val%d",ilZ); 
	strcat (cpValStr, cpVal) ; 
 }
 strcat (cpValStr, ")") ;
 if (igTestFktFlag)
 {
  dbg (DEBUG, "prepSqlValueStr: CREATE PREPARE SQL VAL STR = <%s>", cpValStr) ; 
 }
 return (ilRC) ; 
} /* prepSqlValueStr() */



/******************************************************************************/
static int SQLSelectRecord (int *foundRecord, char *cpSelFlds, char *cpData, 
														char *cpTblNam, char *cpTblExt, char *cpCondFld,
														char *cpCondData) 
{
 int   ilRC = RC_SUCCESS ; 
 int   ilGetRc ; 
 short sql_cursor ; 
 short sql_fkt ; 
 char  pcSqlStatement[1024] ; 

 /* put in information for SQL statement */
 sprintf (pcSqlStatement, "SELECT %s FROM %s%s WHERE %s = '%s'",  
					cpSelFlds, cpTblNam, cpTblExt, cpCondFld, cpCondData ) ; 
 if (igTestFktFlag)
 {
  dbg (DEBUG, "SQLSelectRecord: SQL SELECT STATEMENT = <%s>", pcSqlStatement) ; 
 }

 sprintf (cpData, cpCondData) ;        /* prepare data pool */ 
 sql_cursor = 0 ; 
 sql_fkt = START ; 

 ilGetRc = sql_if (sql_fkt, &sql_cursor, pcSqlStatement, cpData) ;
 if (ilGetRc == DB_SUCCESS) 
 {
	BuildItemBuffer(cpData,cpSelFlds,0,",");
	ilRC = RC_SUCCESS ;
	*foundRecord = TRUE ;
 }
 else
 {
	*foundRecord = FALSE ;
 } 
 close_my_cursor (&sql_cursor) ; 
 return (ilRC) ;

} /* SQLSelectRecord () */




/******************************************************************************/
static int SQLgetALC3Code (char *pcALC3, char *pcAIRL) 
{
 int   ilRC = RC_SUCCESS ; 
 int   ilGetRc ; 
 short sql_cursor ; 
 short sql_fkt ; 
 char  pcSqlStatement[1024] ; 

 if (strlen(pcAIRL) == 3) 
 {                           /* pcAIRL is ident. with pcALC3 */
  strcpy (pcALC3, pcAIRL) ; 
  if (igTestFktFlag)
  {
   dbg(DEBUG, "SQLgetALC3Code: ALC3 CODE = <%s>", pcALC3); 
  }
 }
 else 
 {                            /* put information for SQL statement */
  sprintf(pcSqlStatement, "SELECT ALC3 FROM ALTTAB WHERE ALC2 = '%s'", pcAIRL); 
  if (igTestFktFlag)
  {
   dbg(DEBUG, "SQLgetALC3Code: SQL SELECT STATEMENT = <%s>", pcSqlStatement); 
  }

  sql_cursor = 0 ; 
  sql_fkt = START ; 
  ilGetRc = sql_if (sql_fkt, &sql_cursor, pcSqlStatement, pcgTmpDataArea) ;
  if (ilGetRc == DB_SUCCESS) 
  {
   memcpy (pcALC3, pcgTmpDataArea, 3) ;
   pcALC3[3] = 0x00 ; 
   if (igTestFktFlag)
   {
    dbg(DEBUG, "SQLgetALC3Code: ALC3 CODE = <%s>", pcALC3) ; 
   }
	}
  else
  {
	 ilRC = RC_FAIL ;  
   dbg(TRACE, "SQLgetALC3Code: ERR !!! FOUND NOT AN ALC3 CODE !!!") ; 
  } 
  close_my_cursor (&sql_cursor) ; 
 }
 return (ilRC) ;

} /* SQLGetALC3Code() */




/********************************************************************/
static int GetNextUrno(int *pipUrno)
{
	int				ilRC;
	int				ilOldDbgLvl;
	char			pclDataArea[IDATA_AREA_SIZE];
	
  if (igTestFktFlag)
  {
   dbg (DEBUG, "GetNextUrno >>") ; 
  }
	/* clear buffer */
	memset((void*)pclDataArea, 0x00, IDATA_AREA_SIZE);

	/* get the next... */
	ilOldDbgLvl = debug_level;
	debug_level = TRACE;
	if ((ilRC = GetNextValues(pclDataArea, C_RESERVED_X_URNOS)) != RC_SUCCESS)
	{
   dbg(TRACE,"GetNextValues returns: %d", ilRC);
	 debug_level = ilOldDbgLvl;
   ilRC = RC_FAIL ;  
	}
	else 
	{
	 debug_level = ilOldDbgLvl;
  }

	/* convert to integer */
	*pipUrno = atoi(pclDataArea);
  if (igTestFktFlag)
  {
   dbg (DEBUG, "GetNextUrno <<") ; 
  }
	return RC_SUCCESS;

} /* getNextUrno () */




/********************************************************************/
static int GetNextUrnoItem(int *pipUrno)
{
	int				ilRC;

  if (igTestFktFlag)
  {
   dbg (DEBUG, "GetNextUrnoItem >>") ; 
  }
	igReservedUrnoCnt-- ; 
  ilRC = RC_SUCCESS ; 
	if (igReservedUrnoCnt <= 0)
	{                            /* hole Urno buffer -> get new Urnos */
   ilRC = GetNextUrno (pipUrno) ; 
   igReservedUrnoCnt = C_RESERVED_X_URNOS - 1 ;
   igActUrno = *pipUrno ; 
	}
  else 
	{
	 igActUrno++ ; 
	 *pipUrno = igActUrno ;      /* get next reserved Urno */ 
	}
  	
  if (igTestFktFlag)
  {
   dbg (DEBUG, "GetNextUrnoItem: Next Urno is : <%#010d>", *pipUrno) ; 
   dbg (DEBUG, "GetNextUrnoItem <<") ; 
  }
	return (ilRC) ;

} /* getNextUrnoItem () */



/********************************************************************/
static int GetLocalTimeStamp(char *cpTimeStamp)
{
 int  ilRC ;
 static char pclTime[iTIME_SIZE];
 time_t		  _llCurTime;
 struct tm	  *_tm;
 struct tm	  prlCurTime;
 
 if (igTestFktFlag)
 {
  dbg (DEBUG, "GetLocalTimeStamp >>") ; 
 }
 ilRC = RC_SUCCESS ; 
 memset((void*)pclTime, 0x00, iTIME_SIZE);
 _llCurTime = time(0L);
 _tm = (struct tm *) localtime(&_llCurTime);
 if (_tm != NULL)
 {
  prlCurTime = *_tm;
  if (_llCurTime > 0)
  {
   strftime(pclTime,15,"%" "Y%" "m%" "d%" "H%" "M%" "S", &prlCurTime);
  }
  else
  {
   dbg(TRACE,"GetLocalTimeStamp: _llCurTime is %ld", _llCurTime);
  }
 }
 else
 {
  dbg(TRACE,"GetLocalTimeStamp: localtime %ld failed", _llCurTime);
  ilRC = RC_FAIL ; 
 }
 strcpy (cpTimeStamp, pclTime) ; 
 if (igTestFktFlag)
 {
  dbg (DEBUG, "GetLocalTimeStamp <<") ; 
 }
 return (ilRC) ; 	
} /* GetLocalTimeStamp() */


/*-------------------------------------------------------------------*/
/*              FUNTIONS for FIELD STRUCT HANDLING                   */
/*-------------------------------------------------------------------*/

/* RESET_FLD_STRUCT */
/*********************************************************************/
static int ResetFldStruct (ST_EVT_DATA_HDL *sFldArr) 
{
 int ilRC ; 
 int ilZ ;
 
 if (igTestFktFlag)
 {
  dbg (DEBUG, "ResetFldStruct: >>") ; 
 }
 ilRC = RC_SUCCESS ;
 for (ilZ = 0; ilZ < C_MAXFIELDS; ilZ++)      
 {
  sFldArr->Status[ilZ] = C_DEAKTIV ;    /* reset field */
 }
 if (igTestFktFlag)
 {
  dbg (DEBUG, "ResetFldStruct: <<") ; 
 }
 return (ilRC) ; 
} /* ResetFldStruct () */ 


/* INSERT_FLD_STRUCT */
/*******************************************************************/
/* ATTENTION: USE THIS FUNCTION ONLY FOR HANDLE EVENT DATA         */
/*******************************************************************/
static int fillFldStruct (char *cpFldsStr, ST_EVT_DATA_HDL *sFldArr) 
{
 char delim[2] ;
 int  fldLen ; 
 int  fldPos ;
 long ilZ ; 
 int  ilRC ; 

 if (igTestFktFlag)
 {
  dbg (DEBUG, "fillFldStruct: >>") ; 
  dbg (DEBUG, "fillFldStruct: work with string <%s>", cpFldsStr) ; 
 }

 ilRC = RC_SUCCESS ; 
 fldPos = 1 ; 
 fldLen = 4 ;
 ilZ = 0 ; 
 strcpy (delim, ",") ;                        /* used delimiter */ 
 while(strlen (&cpFldsStr[fldPos-1]) >= fldLen )
 {
	if (fldPos < fldLen) fldPos-- ;
	strncpy (sFldArr->Fieldname[ilZ], &cpFldsStr[fldPos], fldLen) ;
	sFldArr->Fieldname[ilZ][fldLen] = 0x00 ;
	sFldArr->Status[ilZ] = C_AKTIV ;           /* set field active */
  sFldArr->FieldVal[ilZ][0] = 0x000 ; 

	sFldArr->Item[ilZ] = get_item_no(cpFldsStr,sFldArr->Fieldname[ilZ],5)+1;  
	sFldArr->StrItem[ilZ] = C_NOTSET ; 
  sFldArr->StrBeginPos[ilZ] = (ilZ + (ilZ * fldLen)) - 1 ;
	sFldArr->ValLength[ilZ] = 0 ;
	sFldArr->ReadFromEv[ilZ] = C_ENABLE ; 
	sFldArr->DoFldCmp[ilZ] = C_ENABLE ; 
	sFldArr->DoUpdate[ilZ] = C_DISABLE ; 
 	sFldArr->DoSelect[ilZ] = C_DISABLE ; 
 	sFldArr->DoInsert[ilZ] = C_DISABLE ; 
 	sFldArr->DoSearch[ilZ] = C_DISABLE ; 
  if (igTestFktFlag)
  {
   dbg(DEBUG,"fillFldStruct: interpret Fld=<%s>",sFldArr->Fieldname[ilZ]); 
  }
	ilZ++ ; 
  fldPos = ilZ + (ilZ * fldLen) ; 
 }
 if (igTestFktFlag)
 {
  dbg(DEBUG,"fillFldStruct: <<"); 
 }
 return (ilRC) ; 
} /* end of fillFldStruct */



 

/*******************************************************************/
/* ATTENTION: USE THIS FUNCTION ONLY FOR READ EVENT DATA           */
/*******************************************************************/
static int fillDataInFldStruct (char *cpDataStr, ST_EVT_DATA_HDL *sFldArr) 
{
 int ilRC = RC_SUCCESS ; 
 int ilZ ; 
 int ilLen ; 

 if (igTestFktFlag)
 {
	dbg(DEBUG, "fillDataInFldStruct: >>") ; 
 }
 ilZ = 0 ; 
 while (sFldArr->Status[ilZ] == C_AKTIV) 
 {
	if (sFldArr->Item[ilZ] > C_NOTSET) 
  { 
	 if (sFldArr->ReadFromEv[ilZ] == C_ENABLE) 
	 {
    ilLen=get_real_item(sFldArr->FieldVal[ilZ], cpDataStr, sFldArr->Item[ilZ]); 
	  /* str_trm_all(sFldArr->FieldVal[ilZ], " ", TRUE) ; */
/*    if (igTestFktFlag)
 *    {
 */
     dbg (DEBUG, "fillDataInFieldStruct: Fld=<%s> Data=<%s>", 
	  							sFldArr->Fieldname[ilZ], sFldArr->FieldVal[ilZ]) ;
/*    }
 */
    sFldArr->ValLength[ilZ] = strlen (sFldArr->FieldVal[ilZ]) ;
   }
  }
	else
	{
	 ilRC = RC_FAIL ; 
   dbg (DEBUG, "fillDataInFieldStruct: ERR: sFldArr ITEM > 0 !!! ") ;
  }

  ilZ++ ; 
 }
 if (igTestFktFlag)
 {
	dbg(DEBUG, "fillDataInFldStruct: <<") ; 
 }
 return (ilRC) ; 

} /* fillDataInFldStruct () */





/*******************************************************************/
static int AddFld2Struct (ST_EVT_DATA_HDL *sFldArr, char *cpFldName,
                          char *cpValStr ) 
{
 long ilZ ; 
 int  ilRC = RC_SUCCESS ; 

 if (igTestFktFlag)
 {
	dbg(DEBUG, "AddFld2Struct: >>") ; 
 }
 ilZ = 0 ;

 while (sFldArr->Status[ilZ] == C_AKTIV) ilZ++ ; 
 if (ilZ >= C_MAXFIELDS)
 {
  dbg (TRACE, "AddFldStruct: BUFFER OVERFLOW") ;
	ilRC = RC_FAIL ; 
 }
 if (ilRC == RC_SUCCESS) 
 {
	strcpy (sFldArr->Fieldname[ilZ], cpFldName) ;
	sFldArr->Status[ilZ] = C_AKTIV ;           /* set field active */
  strcpy (sFldArr->FieldVal[ilZ], cpValStr) ; 
	sFldArr->StrItem[ilZ] = C_NOTSET ; 
  sFldArr->StrBeginPos[ilZ] = 0 ;  
	sFldArr->Item[ilZ] = 0 ;  
	sFldArr->ValLength[ilZ] = strlen (cpValStr) ;
	sFldArr->LastAttach[ilZ] = C_NOTSET ; 
	sFldArr->ReadFromEv[ilZ] = C_DISABLE ;
	sFldArr->DoFldCmp[ilZ] = C_DISABLE ;
	sFldArr->DoUpdate[ilZ] = C_DISABLE ; 
 	sFldArr->DoSelect[ilZ] = C_DISABLE ; 
 	sFldArr->DoInsert[ilZ] = C_DISABLE ; 
 	sFldArr->DoSearch[ilZ] = C_DISABLE ; 
  if (igTestFktFlag)
  {
	 dbg(DEBUG, "AddFld2Struct: INSERT REC=FLD<%s>,STAT=AKTIV,DATA=<%s>",
	             sFldArr->Fieldname[ilZ], 
               sFldArr->FieldVal[ilZ]) ; 
	 dbg(DEBUG, "AddFld2Struct: <<") ; 
  }
 }
 return (ilRC) ; 
} /* end of AddFld2Struct */






/*******************************************************************/
static int InsOrReplFld2Struct (ST_EVT_DATA_HDL *sFldArr, char *cpFldName,
                                char *cpValStr ) 
{
 int  ilRC = RC_SUCCESS ; 

 if (igTestFktFlag)
 {
	dbg(DEBUG, "InsOrReplFld2Struct: >>") ; 
 }
 ilRC = DoesFldExistInStruct (cpFldName, *sFldArr) ;  
 if (ilRC == RC_FAIL)
 {
	 /* field does not exist */
  ilRC = AddFld2Struct (sFldArr, cpFldName, cpValStr ) ;  
 } 
 else    
 {
	/* field exist -> update the field */
	dbg(DEBUG, "InsOrReplFld2Struct: REPLACE NOT ACTIVE!!!") ; 
  /* ilRC = ReplaceFldDataInStruct (cpValStr, cpFldName, sFldArr) ; */   
 } 
 if (igTestFktFlag)
 {
	dbg(DEBUG, "InsOrReplFld2Struct: <<") ; 
 }
 return (ilRC) ; 
} /* end of InsOrReplFld2Struct */






/*******************************************************************/
static int ReplaceFldDataInStruct (char *cpDatStr, char *pclFld,  
                                   ST_EVT_DATA_HDL *sFldArr) 
{
 int ilRC = RC_SUCCESS ; 
 int ilZ ; 
 int ilStatus ; 

 if (igTestFktFlag)
 {
	dbg(DEBUG, "ReplaceFldDataInStruct: <<") ; 
 }
 ilZ = 0 ;
 ilStatus = C_NOTFOUND ; 
 while ((sFldArr->Status[ilZ] == C_AKTIV) && (ilStatus == C_NOTFOUND)) 
 {
	if (strcmp (pclFld, sFldArr->Fieldname[ilZ]) == 0) 
  {
	 /* found the field */
   strcpy (sFldArr->FieldVal[ilZ], cpDatStr); 
   ilStatus = C_FOUND ; 
   if (igTestFktFlag)
   {
    dbg (DEBUG, "ReplaceFldDataInStruct: Fld=<%s> Data=<%s>", pclFld,
								cpDatStr) ; 
   }
	}
  ilZ++ ; 
 }
 if (ilStatus == C_NOTFOUND)
 { 
  cpDatStr[0] = 0x00 ; 
	ilRC = RC_FAIL ;
	dbg (TRACE, "ReplaceFldDataInStruct: ERR: Field=<%s> NOT found", pclFld) ;   
 }
 if (igTestFktFlag)
 {
	dbg(DEBUG, "ReplaceFldDataInStruct: >>") ; 
 }
 return (ilRC) ;

} /* ReplaceDataInStruct () */ 




/*******************************************************************/
static int getFldDataFromStruct (char *cpRetStr, char *cpFld,  
                                 ST_EVT_DATA_HDL sFldArr) 
{
 int ilRC = RC_SUCCESS ; 
 int ilZ ; 
 int ilStatus ; 

 ilZ = 0 ;
 ilStatus = C_NOTFOUND ; 
 while ((sFldArr.Status[ilZ] == C_AKTIV) && (ilStatus == C_NOTFOUND)) 
 {
	if (strcmp (cpFld, sFldArr.Fieldname[ilZ]) == 0) 
  {
	 /* found the field */
   strcpy (cpRetStr, sFldArr.FieldVal[ilZ]); 
   ilStatus = C_FOUND ; 
   if (igTestFktFlag)
   {
    dbg (DEBUG, "getFldDataFromStruct: DATA=<%s> FLD=<%s>",
								cpRetStr, cpFld) ; 
   }
	}
  ilZ++ ; 
 }
 if (ilStatus == C_NOTFOUND)
 { 
	ilRC = RC_FAIL ;
	dbg (TRACE, "getFldDataFromStruct: CANT FIND FIELD: <%s>", cpFld) ;   
 }
 return (ilRC) ;

} /* getFldDataFromStruct () */ 





/*******************************************************************/
static int DoesFldExistInStruct (char *cpFld, ST_EVT_DATA_HDL sFldArr) 
{
 int ilRC = RC_SUCCESS ; 
 int ilZ ; 
 int ilStatus ; 

 if (igTestFktFlag)
 {
  dbg (DEBUG, "DoesFldExistInStruct: >>") ; 
 }
 ilZ = 0 ;
 ilStatus = C_NOTFOUND ; 
 while ((sFldArr.Status[ilZ] == C_AKTIV) && (ilStatus == C_NOTFOUND)) 
 {
	if (strcmp (cpFld, sFldArr.Fieldname[ilZ]) == 0) 
  {
	 /* found the field */
   if (igTestFktFlag)
   {
	  dbg (DEBUG, "DoesFldExistInStruct: Fld=<%s> exist", cpFld) ;   
   }
   ilStatus = C_FOUND ;
	}
  ilZ++ ; 
 }
 if (ilStatus == C_NOTFOUND)
 { 
	ilRC = RC_FAIL ;
  if (igTestFktFlag)
  {
	 dbg (DEBUG, "DoesFldExistInStruct: Fld=<%s> does not exist", cpFld) ;   
  }
 }
 if (igTestFktFlag)
 {
  dbg (DEBUG, "DoesFldExistInStruct: <<") ; 
 }
 return (ilRC) ;

} /* DoesFldExistInStruct () */ 







/*******************************************************************/
static int getFldStruct (char *pclFldsStr, char *pclDataStr, int iAction,  
												 ST_EVT_DATA_HDL *sFldArr) 
{
 int  ilZ ; 
 int  ilRC = RC_SUCCESS ; 
 int  ilCanPosAttach ; 
 int  ilDone ; 

 if (igTestFktFlag)
 {
  dbg (DEBUG, "getFldStruct >>") ; 
 }
 pclFldsStr[0] = 0x00 ; 
 pclDataStr[0] = 0x00 ; 
 ilRC = setNextFldPosInStruct (&ilCanPosAttach, iAction, sFldArr) ;  
 if (ilRC == RC_SUCCESS)    /* found another field */
 {
  /* get the active attach field */
  ilDone = FALSE ; 
	ilZ = 0 ; 
	do 
  { 
	 if (sFldArr->LastAttach[ilZ] == C_AKTIV)
	 {
    strcpy (pclFldsStr, sFldArr->Fieldname[ilZ]) ;
    strcpy (pclDataStr, sFldArr->FieldVal[ilZ]) ;
		ilDone = TRUE ;
		if (igTestFktFlag)
		{
		 dbg (DEBUG, "getFldStruct: READ DATA=<%s>, FLD=<%s>", pclDataStr,
																pclFldsStr) ; 
    }
   } /* if found field & data */
	 else 
	 {
		if (ilZ >= C_MAXFIELDS-1) 
		{
		 ilDone = TRUE ;
		 ilRC = RC_FAIL ;
		 if (igTestFktFlag)
		 {
		  dbg (DEBUG, "getFldStruct: NO FIELDPOS FOUND!") ; 
     }
    }
	 }
   ilZ++ ; 
  } while (ilDone == FALSE) ;
 }

 if (igTestFktFlag)
 {
  dbg (DEBUG, "getFldStruct <<") ; 
 }

 return (ilRC) ; 

} /* getFldStruct() */ 

 



/*******************************************************************/
static int setNextFldPosInStruct (int *ipActionResult, int iAction,  
											          	ST_EVT_DATA_HDL *sFldArr) 
{
 long ilZ ; 
 int  ilRC ; 
 int  ilDone ; 
 int  ilActionCode ;

 if (igTestFktFlag)
 {
  dbg (DEBUG, "setNextFldPosInStruct >>" ) ; 
 }

 ilRC = RC_SUCCESS ; 
 ilActionCode = C_NO_ERROR ; 
 ilDone = FALSE ; 
 switch (iAction)
 {
  case  C_FIRST:
	         if (igTestFktFlag)
           {
				   	dbg (DEBUG, "setNextFldPosInStruct: POS ON BEGINING!") ; 
           }
					 for (ilZ = 0; ilZ < C_MAXFIELDS; ilZ++) 
           {                                 /* reset all attach fields */ 
	          sFldArr->LastAttach[ilZ] = C_DEAKTIV ; 
           } 
					 if (sFldArr->Status[0] == C_AKTIV)
					 { 
            sFldArr->LastAttach[0] = C_AKTIV ;
           }
					 else 
					 {
            ilActionCode = C_NO_ACTIVE_FIELDS ;  
           }
         break ; 
  
	case C_NEXT: 
	         if (igTestFktFlag)
           {
					  dbg (DEBUG, "setNextFldPosInStruct: POS ON NEXT FIELD!") ; 
           }
					 ilZ = 0 ; 
           do 
					 { 
						if (sFldArr->Status[ilZ] == C_AKTIV)
						{
						 if (sFldArr->LastAttach[ilZ] == 1)
						 {
							if (ilZ >= C_MAXFIELDS)
							{ 
						   ilDone = TRUE ; 
               ilActionCode = C_SET_ON_LAST_FIELD ;  
              }
							else 
							{
               if(sFldArr->Status[ilZ+1] == C_AKTIV)
							 {
                sFldArr->LastAttach[ilZ] = C_DEAKTIV ;   /* reset old */
								sFldArr->LastAttach[ilZ+1] = C_AKTIV ;   /* set new */
								ilDone = TRUE ; 
               }
							 else
							 {
								ilDone = TRUE ; 
                ilActionCode = C_SET_ON_LAST_FIELD ;  
               } /* if next fldStr == AKTIV */
							} /* if not all fields */ 
						 } /* if last attach == AKTIV */
						} /* if status == AKTIV */ 
						else 
						{
						 ilDone = TRUE ; 
             ilActionCode = C_NO_ACTIVE_FIELDS ;  
            }
						ilZ++ ; 
           } while (ilDone == FALSE) ;  
         break ; 

	case C_PREV:  
	         if (igTestFktFlag)
           {
					  dbg (DEBUG, "setNextFldPosInStruct: POS ON PREVIOUS FIELD!") ; 
           }
					 ilZ = 0 ; 
           do 
					 { 
						if (sFldArr->Status[ilZ] == C_AKTIV)
						{
						 if (sFldArr->LastAttach[ilZ] == C_AKTIV)
						 {
							if (ilZ <= 0)      /* set on the first position */
							{ 
						   ilDone = TRUE ; 
               ilActionCode = C_SET_ON_FIRST_FIELD ;  
              }
							else 
							{
                sFldArr->LastAttach[ilZ] = C_DEAKTIV ; /* reset old */
								sFldArr->LastAttach[ilZ-1] = C_AKTIV ; /* set new */
								ilDone = TRUE ; 
							} /* if not all fields */ 
						 } /* if last attach == AKTIV */
						} /* if status == AKTIV */ 
						else 
						{
						 ilDone = TRUE ; 
             ilActionCode = C_NO_ACTIVE_FIELDS ;  
            }
						ilZ++ ; 
           } while (ilDone == FALSE) ;  
         break ; 

  case C_LAST:
	         if (igTestFktFlag)
           {
					  dbg (DEBUG, "setNextFldPosInStruct: POS ON LAST FIELD!") ; 
           }
           for (ilZ = 0; ilZ < C_MAXFIELDS; ilZ++) 
           {                                 /* reset all attach fields */ 
	          sFldArr->LastAttach[ilZ] = C_DEAKTIV ; 
           } 
					 ilZ = 0 ;      /* set on start */
					 while (sFldArr->Status[ilZ] == C_AKTIV)
					 {
						ilZ++ ; 
					 }        /* search the last field */
           if (ilZ == 0) 
					 {
            ilActionCode = C_NO_ACTIVE_FIELDS ;  
					 }
					 else 
					 {
            sFldArr->LastAttach[ilZ-1] = C_AKTIV ;
           }
          break ; 

   default : 

					 dbg (DEBUG, "setNextFldPosInStruct: UNKNOWN Cmd = <%d>", iAction) ;
           ilActionCode = C_UNKNOWN_COMMAND ;  
          break ; 

  } /* switch */

  if (ilActionCode != C_NO_ERROR)
	{
	 if (igTestFktFlag)
   {
	  dbg (DEBUG, "setNextFldPosInStruct: CONDITION REACHED -> CANT POS!") ; 
   }
	 ilRC = RC_FAIL ; 
  } 
  *ipActionResult = ilActionCode ;  
 
	if (igTestFktFlag)
  {
   dbg (DEBUG, "setNextFldPosInStruct <<" ) ; 
  }

  return ( ilRC) ; 

} /* function setNextFldPosInStruct() */ 






/*******************************************************************/
static int setChkFldinStruct4Fld (char *pclFld, int iDoWhat,
                                  ST_EVT_DATA_HDL *sFldArr) 
{
 int ilRC = RC_SUCCESS ; 
 int ilZ ; 
 int ilStatus ; 

 if (igTestFktFlag)
 {
  dbg (DEBUG, "setChkFldinStruct4Fld >>" ) ; 
 }
 ilZ = 0 ;
 ilStatus = C_NOTFOUND ; 
 while ((sFldArr->Status[ilZ] == C_AKTIV) && (ilStatus == C_NOTFOUND)) 
 {
	if (strcmp (pclFld, sFldArr->Fieldname[ilZ]) == 0) 
  {
	 /* found the field */
	 sFldArr->DoFldCmp[ilZ] = iDoWhat ; 
   ilStatus = C_FOUND ; 
   if (igTestFktFlag)
   {
    dbg (DEBUG, "setChkFldinStruct4Fld: Set fldCmp to=<%d> in FLD=<%s>",
								iDoWhat, pclFld) ; 
   }
	}
  ilZ++ ; 
 }
 if (ilStatus == C_NOTFOUND)
 { 
	ilRC = RC_FAIL ;
	dbg (TRACE, "setChkFldinStruct4Fld: CANT FIND FIELD: <%s>", pclFld) ;   
 }
 if (igTestFktFlag)
 {
  dbg (DEBUG, "setChkFldinStruct4Fld <<" ) ; 
 }

 return (ilRC) ; 
} /* setChkFldinStruct4Fld () */ 



 



/*******************************************************************/
/* if pclFld == "" -> do set Flag iDoWhat for all fields           */
/* if iKindOfFlg == 0 -> Update all Falgs with iDoWhat             */
/*******************************************************************/
static int setStatusInStruct (char *pclFld, int iDoWhat, int iKindOfFlg,
                              ST_EVT_DATA_HDL *sFldArr) 
{
 int  ilRC = RC_SUCCESS ; 
 int  ilZ ; 
 int  ilStatus ;
 char cActionKind[30] ; 

 ilZ = 0 ;
 ilStatus = C_NOTFOUND ;
 if (pclFld[0] == 0x00) 
 {
  while (sFldArr->Status[ilZ] == C_AKTIV)
	{
   switch (iKindOfFlg)
	 {
		case 0        : sFldArr->DoFldCmp[ilZ] = iDoWhat ; 
		                sFldArr->DoUpdate[ilZ] = iDoWhat ; 
		                sFldArr->DoSelect[ilZ] = iDoWhat ; 
		                sFldArr->DoInsert[ilZ] = iDoWhat ; 
		                sFldArr->DoSearch[ilZ] = iDoWhat ; 
                    strcpy (cActionKind, "ALL") ; 
									 break ;

 		case C_SEARCH : sFldArr->DoSearch[ilZ] = iDoWhat ; 
                    strcpy (cActionKind, "SEARCH") ; 
						    	break ;

 		case C_INSERT : sFldArr->DoInsert[ilZ] = iDoWhat ; 
                    strcpy (cActionKind, "INSERT") ; 
						    	break ;

 		case C_UPDATE : sFldArr->DoUpdate[ilZ] = iDoWhat ; 
                    strcpy (cActionKind, "UPDATE") ; 
						    	break ;

 		case C_SELECT : sFldArr->DoSelect[ilZ] = iDoWhat ; 
                    strcpy (cActionKind, "SELECT") ; 
						    	break ;

 		case C_FLDCMP : sFldArr->DoFldCmp[ilZ] = iDoWhat ; 
                    strcpy (cActionKind, "FLDCMP") ; 
						    	break ;

 		default       : dbg (TRACE, "SetStatusInStruct: UNKNOWN KIND OF FIELD") ; 
                    strcpy (cActionKind, "UNKNOWN") ; 
						    	break ;

   } /* switch() */
	 ilZ++ ;
   ilStatus = C_FOUND ;
	} /* while */
 
	if (igTestFktFlag)
  {
   switch (iDoWhat)
	 {
    case C_ENABLE :

          dbg(DEBUG,"setStatusInStruct: ENAB <%s> STAT FOR ALL FLDS",
					  			  cActionKind	);
					break ;

	  case C_DISABLE :

          dbg(DEBUG,"setStatusInStruct: DISAB <%s> STAT FOR ALL FLDS",
									  cActionKind	);
					break ;

    default :

          dbg(DEBUG,"setStatusInStruct: SET <%s> STAT = <%d> FOR ALL FLDS",
									  cActionKind, iDoWhat);
					break ;
   } /* switch */
  } /* if TestFlag */
 } /* if all */
 else 
 {           /* search for the field */
  while ((sFldArr->Status[ilZ] == C_AKTIV) && (ilStatus == C_NOTFOUND)) 
  {
   if (strcmp (pclFld, sFldArr->Fieldname[ilZ]) == 0) 
   {
  	/* found the field */
    switch (iKindOfFlg)
	  {
		  case 0        : sFldArr->DoFldCmp[ilZ] = iDoWhat ; 
		                  sFldArr->DoUpdate[ilZ] = iDoWhat ; 
		                  sFldArr->DoSelect[ilZ] = iDoWhat ; 
		                  sFldArr->DoInsert[ilZ] = iDoWhat ; 
		                  sFldArr->DoSearch[ilZ] = iDoWhat ; 
                      strcpy (cActionKind, "ALL") ; 
		  				   	  break ;

 		  case C_SEARCH : sFldArr->DoSearch[ilZ] = iDoWhat ; 
                      strcpy (cActionKind, "SEARCH") ; 
						    	  break ;

 		  case C_INSERT : sFldArr->DoInsert[ilZ] = iDoWhat ; 
                      strcpy (cActionKind, "INSERT") ; 
						    	  break ;

 		  case C_UPDATE : sFldArr->DoUpdate[ilZ] = iDoWhat ; 
                      strcpy (cActionKind, "UPDATE") ; 
						      	break ;

 		  case C_SELECT : sFldArr->DoSelect[ilZ] = iDoWhat ; 
                      strcpy (cActionKind, "SELECT") ; 
						    	  break ;

 		  case C_FLDCMP : sFldArr->DoFldCmp[ilZ] = iDoWhat ; 
                      strcpy (cActionKind, "FLDCMP") ; 
						    	  break ;

 		  default       : dbg (TRACE, "SetStatusInStruct: KIND OF FIELD") ; 
                      strcpy (cActionKind, "UNKNOWN") ; 
						    	  break ;

    } /* switch() */
    ilStatus = C_FOUND ;

    if (igTestFktFlag)
    {
     switch (iDoWhat)
		 {
      case C_ENABLE :

           dbg (DEBUG, "setStatusInStruct: ENAB <%s> STAT > FLD = <%s>",
											 cActionKind, pclFld);
					break ;

	  	 case C_DISABLE :

           dbg (DEBUG, "setStatusInStruct: DISAB <%s> STAT > FLD = <%s>",
											 cActionKind, pclFld);
					break ;

       default :

           dbg (DEBUG, "setStatusInStruct: SET <%s> STAT=<%d> > FLD = <%s>",
							         cActionKind, iDoWhat, pclFld) ;
					break ;
     } /* switch */
	  } /* if testFlag */
   } /* if strcmp == fld */
   ilZ++ ; 
  } /* while field not found */
 } /* else */

 if (ilStatus == C_NOTFOUND)
 { 
	ilRC = RC_FAIL ;
	/* dbg (TRACE, "setStatusInStruct: CANT FIND FIELD: <%s>", pclFld) ;   */
 }
 return (ilRC) ; 
} /* setStatusInStruct () */ 




/*******************************************************************/
static int createFldAndDataLst (char *cpFldLst, char *cpDataLst, int iKindOf,
                                ST_EVT_DATA_HDL sFldArr) 
{
 int ilRC = RC_SUCCESS ; 
 int ilZ ; 
 int ilFirstItem ;
 int iAddFldData ; 
 char cpDivStr[2] ; 

 if (igTestFktFlag)
 {
  dbg (DEBUG, "createFldAndDataLst >>" ) ; 
 }
 cpFldLst[0] = 0x00 ;
 cpDataLst[0] = 0x00 ;
 strcpy (cpDivStr, ",") ; 
 ilZ = 0 ;
 ilFirstItem = TRUE ; 
 while (sFldArr.Status[ilZ] == C_AKTIV) 
 {
	iAddFldData = FALSE ; 
	switch (iKindOf)
	{
 		case C_SEARCH : if (sFldArr.DoSearch[ilZ] == C_ENABLE) 
										{ 
										 iAddFldData = TRUE ; 
                    }
						    	break ;

		case C_INSERT : if (sFldArr.DoInsert[ilZ] == C_ENABLE) 
										{ 
										 iAddFldData = TRUE ; 
                    }
						    	break ;

 		case C_SELECT : if (sFldArr.DoSelect[ilZ] == C_ENABLE) 
										{ 
										 iAddFldData = TRUE ; 
                    }
						    	break ;

 		case C_UPDATE : if (sFldArr.DoUpdate[ilZ] == C_ENABLE) 
										{ 
										 iAddFldData = TRUE ; 
                    }
						    	break ;

 		case C_FLDCMP : if (sFldArr.DoFldCmp[ilZ] == C_ENABLE) 
										{ 
										 iAddFldData = TRUE ; 
                    }
						    	break ;

    } /* switch */
 
		if (iAddFldData == TRUE) 
		{
		 /* add item to field and data */
		 if (ilFirstItem == FALSE)
		 {
			strcat (cpFldLst, cpDivStr) ;
			strcat (cpDataLst, cpDivStr) ; 
     }
	   ilFirstItem = FALSE ; 
     strcat (cpFldLst, sFldArr.Fieldname[ilZ]) ; 
     strcat (cpDataLst, sFldArr.FieldVal[ilZ]) ; 
    } 
		ilZ++ ; 
	}  /* while not all flds */

  if (igTestFktFlag)
  {
	 dbg (DEBUG, "createFldAndDataLst: generated Fieldlist = <%s>", cpFldLst) ;
   dbg (DEBUG, "createFldAndDataLst: generated Datalist = <%s>", cpDataLst) ;
   dbg (DEBUG, "createFldAndDataLst <<" ) ; 
  }
	return (ilRC) ; 

} /* createFldAndDataLst () */







/*----------------------------------------------------------------------------*/
/* No longer time needed functions                                            */
/*----------------------------------------------------------------------------*/



/******************************************************************************/
static int ifRecordChanged (int *iChgFlg, char *pcgSearchFldLst,
														char *pcgDataArea, ST_EVT_DATA_HDL sFldArr) 
{
 /* compare the field/dataList with the struct values. Is in the struct */
 /* the information <DO NOT COMPARE> set the data field are ignored     */

 int ilRC = RC_SUCCESS ; 
 int fldPos ;
 int fldLen ;
 int ilZ ;
 int ilDoStatus = 0 ; 
 int ilSqlStrItem ; 
 int ilStatus ; 
 int ilStatusS ; 
 int ilLen ; 
 char delim[2] ; 
 char pclActSqlField[32] ; 
 char pclRetStr[2096] ;
 char pclSQLVal[2096] ; 
 
 if (igTestFktFlag)
 {
  dbg (DEBUG, "ifRecordChanged >>" ) ; 
 }
 *iChgFlg = FALSE ; 
 fldPos = 0 ; 
 fldLen = 4 ;
 ilStatus = C_NOTFOUND ;
 strcpy (delim, ",") ;                        /* used delimiter */ 

 while((strlen(&pcgSearchFldLst[fldPos]) >= fldLen) && (ilStatus==C_NOTFOUND))
 {
	strncpy (pclActSqlField, &pcgSearchFldLst[fldPos], fldLen) ;
  pclActSqlField[fldLen] = 0x00 ;
	fldPos += fldLen ; 
	if (&pcgSearchFldLst[fldPos] != 0x00) 
	{
   fldPos++ ; 
  } 

  /* search this field in the struct */
  ilZ = 0 ;
  ilStatusS = C_NOTFOUND ; 
  while ((sFldArr.Status[ilZ] == C_AKTIV) && (ilStatusS == C_NOTFOUND)) 
  {
   if (strcmp (pclActSqlField, sFldArr.Fieldname[ilZ]) == 0) 
   {
    /* found the field */
    strcpy (pclRetStr, sFldArr.FieldVal[ilZ]); 
    ilStatusS = C_FOUND ; 
    if (igTestFktFlag)
    {
     dbg (DEBUG, "ifRecordChanged: FOUND STRUCT ITEM = <%s>", pclRetStr) ; 
    }
	  if (sFldArr.DoFldCmp[ilZ] == C_ENABLE)
		{
     if (igTestFktFlag)
     {
		  dbg (DEBUG, "ifRecordChanged: Search Field <%s> is enabled !", 
									pclActSqlField ) ;
     }
		 ilStatus = ilDoStatus = 1 ; 
    }
    else 
    {
     if (igTestFktFlag)
     {
		  dbg (DEBUG, "ifRecordChanged: Search Field <%s> is DISABLED !",
  								 pclActSqlField );
     }
		 ilStatus = ilDoStatus = 0 ; 
    }
   }
   ilZ++ ; 
	} 

  if (ilStatusS == C_NOTFOUND)
  { 
   dbg(TRACE, "getFldDataFromStruct: CANT FIND FLD IN STRUCT: <%s>",
							pclActSqlField) ;
   /* Abort while not found */
   return (RC_FAIL) ; 
	}

	if ((ilRC == RC_SUCCESS) && (ilDoStatus != 0))
  {
   ilSqlStrItem = get_item_no(pcgSearchFldLst, pclActSqlField, 5) + 1 ; 
   ilLen=get_real_item (pclSQLVal, pcgDataArea, ilSqlStrItem) ; 
   if (igTestFktFlag)
   {
    dbg (DEBUG, "ifRecordChanged: SQL Data Value is = <%s>", pclSQLVal) ; 
   }
	 dbg (DEBUG, "compare Field <%s>", pclActSqlField );
	 dbg (DEBUG, "compare SQL<%s> with EV<%s>", pclSQLVal, pclRetStr) ;  
   if (strcmp (pclRetStr, pclSQLVal) == 0) 
   {
    /* no changes for this field */
   }
   else 
   {
    /* fieldval was changed */
    if (igTestFktFlag)
    {
		 dbg (DEBUG, "ifRecordChanged: Fld val was changed <%s>", pclActSqlField) ; 
    }
    *iChgFlg = TRUE ;
		return (RC_SUCCESS) ; 
   }
  } /* if compare */

 } /* while () */
 if (igTestFktFlag)
 {
  dbg (DEBUG, "ifRecordChanged <<" ) ; 
 }

 return (ilRC) ; 

} /* ifRecordChanged */





/******************************************************************************/
static int SQLInsertRecord (char *cpUsedFlds, char *cpUsedData, 
														char *cpTblNam, char *cpTblExt ) 
{
 int   ilRC = RC_SUCCESS ; 
 int   ilGetRc ; 
 short sql_cursor ; 
 short sql_fkt ; 
 char  pcSqlStatement[1024] ; 
 char  cplValStr[1024] ; 
 int   ilItems ;

 if (igTestFktFlag)
 {
  dbg (DEBUG, "SQLInsertRecord >>" ) ; 
 }
 ilRC = getItemCnt (cpUsedFlds, &ilItems) ;     
 ilRC = prepSqlValueStr (ilItems, cplValStr) ;  

 /* put in information for SQL statement */
 sprintf (pcSqlStatement, "INSERT INTO %s%s %s %s", 
          cpTblNam, cpTblExt, cpUsedFlds, cplValStr) ; 
 sql_cursor = 0 ; 
 sql_fkt = START ;
 ilGetRc = DB_SUCCESS ; 

 if (igRealImportFlag)
 {
  delton (cpUsedData) ; 
	ilGetRc = sql_if (sql_fkt, &sql_cursor, pcSqlStatement, cpUsedData) ;
  if (ilGetRc != DB_SUCCESS) 
  {
   dbg (TRACE, "-------------------------------------------------") ; 
	 dbg (TRACE, "SQLInsertRecord : CANT INSERT RECORD stat.: <%s>",
		  					pcSqlStatement) ; 
	 dbg (TRACE, "-------------------------------------------------") ; 

	 ilRC = RC_FAIL ; 
  }
	close_my_cursor (&sql_cursor) ; 
 }
 else        /* not a real update - work with logfile */
 {
  dbg (DEBUG, "SIR: FLDS ARE -> <%s>" , cpUsedFlds) ;  
  dbg (DEBUG, "SIR: DATA ARE -> <%s>" , cpUsedData) ; 
	ilRC = RC_SUCCESS ; 
 }
 if (igTestFktFlag)
 {
  dbg (DEBUG, "SQLInsertRecord <<" ) ; 
 }
 return (ilRC) ;

} /* SQLInsertRecord () */

/*
   Mark FKEY as insert running.
   This routine should be called in AFTTAB INSERT mode.
   If this routine returns FALSE, the insert already was running and an
   update has to be done to the flight record instead.
*/
void CheckImptabUsage ()
{
   int ilRC = TRUE;
   int ilGetRc = FALSE;
   char pclDataArea[32];
   
   if (igUseImpTab == -1)  /* presence of IMPTAB not validated */
   {
      memset(pclDataArea,0,32);
      ilRC = SQLSelectRecord (&ilGetRc, "TABLE_NAME", pclDataArea, 
			  						     	   "USER_TABLES", "", "TABLE_NAME", "IMPTAB" ) ;
      if (ilGetRc == FALSE)
      {
         dbg (TRACE, "no IMPTAB in DB ..." ) ; 
         igUseImpTab = 0;  /* no IMPTAB in DB */
      }
      else
      {
         dbg (TRACE, "will use IMPTAB of DB to sync insert and updates ..." ) ; 
         igUseImpTab = 1;  /* IMPTAB found in DB */
      }
   }
}

/*
   Mark FKEY as insert running.
   This routine should be called in AFTTAB INSERT mode.
   If this routine returns FALSE, the insert already was running and an
   update has to be done to the flight record instead.
*/
int MarkFkeyInsert (char *pclFkey)
{
   int ilRC = TRUE;
   int ilGetRc = FALSE;
   char pclDataArea[32];
   char  pcDummy[1024] ; 
   short sql_cursor ; 
   short sql_fkt ; 
   char  pcSqlStatement[1024] ; 
   
   if (igUseImpTab == 1)  /* IMPTAB found in DB */
   {
      ilGetRc = FALSE;
      memset(pclDataArea,0,32);
      ilRC = SQLSelectRecord (&ilGetRc, "FKEY", pclDataArea, 
		                            "IMP", "TAB", "FKEY", pclFkey ) ;
      if (ilGetRc == FALSE)
      {
        sql_cursor = 0 ; 
        sql_fkt = START ;
      
        sprintf (pcSqlStatement, "INSERT INTO IMPTAB VALUES ('%s')", pclFkey) ; 
        dbg (TRACE, "MarkFkeyInsert: %s",pcSqlStatement) ; 
        (void) memset(pcDummy,0,32);
        ilRC = sql_if (sql_fkt, &sql_cursor, pcSqlStatement, pcDummy) ;
        if (ilRC == DB_SUCCESS) 
        {
		     commit_work() ; 
        }
        else
        {
          dbg (TRACE, "MarkFkeyInsert: INSERT failed, <%s>",pcDummy) ; 
        }
        close_my_cursor (&sql_cursor) ; 
      }
   }
   return ilGetRc;
} /* MarkFkeyInsert */

/*
   Clear FKEY as insert running.
   This routine should be called in AFTTAB UPDATE mode.
   The caller has detected the FKEY in AFTTAB, so the entry in IMPTAB
   has to be removed. (Ignore return code! May be, FKEY already has been
   deleted from IMPTAB.)
*/
void ClearFkeyInsert (char *pclFkey)
{
   int ilRC = TRUE;
   int ilGetRc = FALSE;
   char  pcSqlStatement[1024] ; 
   char  pcDummy[1024] ; 
   short sql_cursor ; 
   short sql_fkt ; 

   if (igUseImpTab == 1)  /* IMPTAB found in DB */
   {
      sql_cursor = 0 ; 
      sql_fkt = START ;
       
      if (strcmp(pclFkey,"=ALL=") == 0)
      {
        sprintf (pcSqlStatement, "DELETE FROM IMPTAB") ; 
      }
      else
      {
        sprintf (pcSqlStatement, "DELETE FROM IMPTAB WHERE FKEY='%s'", pclFkey) ; 
      }
      dbg (TRACE, "ClearFkeyInsert: %s",pcSqlStatement) ; 
      (void) memset(pcDummy,0,32);
      ilRC = sql_if (sql_fkt, &sql_cursor, pcSqlStatement, pcDummy) ;
      if (ilRC == DB_SUCCESS) 
      {
         commit_work() ; 
      }
      /* NOT FOUND may be ignored
      else
      {
         dbg (TRACE, "ClearFkeyInsert: DELETE failed, <%s>",pcDummy) ; 
      }
      */
      close_my_cursor (&sql_cursor) ; 
   }
  return ;
} /* ClearFkeyInsert */


/******************************************************************************/
/* Space for Appendings (Source-Code Control)                                 */
/******************************************************************************/
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
