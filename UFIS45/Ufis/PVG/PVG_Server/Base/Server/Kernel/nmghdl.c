#ifndef _DEF_mks_version
  #define _DEF_mks_version
  #include "ufisvers.h" /* sets UFIS_VERSION, must be done before mks_version */
  static char mks_version[] MARK_UNUSED = "@(#) "UFIS_VERSION" $Id: Ufis/PVG/PVG_Server/Base/Server/Kernel/nmghdl.c 1.1 2005/04/08 16:31:16SGT mcu Exp  $";
#endif /* _DEF_mks_version */
/*****************************************************************************/
/*                                                                           */
/* CCS Program Skeleton                                                      */
/*                                                                           */
/* Author         : 				                                               */
/* Date           :                                                          */
/* Description    :                                                          */
/*                                                                           */
/* Priority       : PRIO_ADMIN > PRIO_AP > PRIO_DB > PRIO_ASCII > NOTHING    */
/*				  		  PRIO_ADMIN - Administrator		      			           */ 
/*					     PRIO_AP    - Alarm Printer                               */
/*					     PRIO_DB    - Datenbank                                   */
/*					     PRIO_ASCII - Ascii File                                  */
/*					     NOTHING    - keine Bearbeitung der Daten                 */
/*                                                                           */
/* Update history :                                                          */
/* 	18.06.98 Erweiterung HSB ...				      						        */
/*  20040810 jim : removed sccs_ version string                               */
/*                                                                           */
/*****************************************************************************/
/* This program is a CCS main program */
/*                                                                            */
/* source-code-control-system version string                                  */
/* be carefule with strftime or similar functions !!!                         */
/*****************************************************************************/

#define U_MAIN
#define UGCCS_PRG
#define STH_USE

#include <malloc.h>
#include <errno.h>
#include <signal.h>
#include <memory.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "ugccsma.h"
#include "glbdef.h"
#include "quedef.h"
#include "uevent.h"
#include "db_if.h"
#include "tools.h"
#include "debugrec.h"
#include "l_ccstime.h"
#include "helpful.h"
#include "new_catch.h"
#include "hsbsub.h"
#include "uniif.h"
#include "nmghdl.h"

/******************************************************************************/
/* External variables                                                         */
/******************************************************************************/
/* outp is defined in ugccsma.h! double definitions kill */
/* your process under HP-UX immediatly !!!!              */
/*FILE *outp                 = NULL;*/
int  debug_level           = TRACE;

/******************************************************************************/
/* Global variables                                                           */
/******************************************************************************/
static ITEM  			*prgItem   = NULL;        /* The queue item pointer  */
static EVENT 			*prgEvent  = NULL;        /* The event pointer       */
static BC_HEAD			*prgBCHead = NULL;        /* The bc_head pointer     */
static CMDBLK			*prgCmdblk = NULL;        /* The cmdblk pointer      */

/*//////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
////////// eigene Variablen 
*/
static int				igUsrLog;
static int				igConfLog;
static int				igPrimLog;
static int				igSecoLog;
static char				pcgTABEnd[iMIN];
static char				pcgLogTableName[iMIN]; /* 32 byte is enough */
static char				pcgMessageTextTableName[iMIN]; /* 32 byte is enough */
static char				pcgUsrLog[IUSER_LOG_SIZE];
static char				pcgConfLog[ICONF_LOG_SIZE];
static int				igUseUniCenter = FALSE;
static int				igNoOfUniCenterMsg;
static int				pigUniCenterMsgNo[1000];
static Msg				rgUsrMsgTxt;
static Msg				rgConfMsgTxt;
static Msg				rgErrorMsgTxt;
static MSG_INFO_BLK     rgMsgInfo;
static MSG_INFO_BLK		*prgMsgInfo	= &rgMsgInfo;

/*/ ------- Beschreibung des nachfolgenden Status-Words ---------------
// Bit 2^0  (0x0001) -> 1=DB-Connect, 0=kein Connect zur DB
// Bit 2^1  (0x0002) -> Prioritaet Ascii-File   
// Bit 2^2  (0x0004) -> Prioritaet Datenbank						
// Bit 2^3  (0x0008) -> Prioritaet Alarm-Drucker 				

// Bit 2^4  (0x0010) -> Prioritaet Administrator
// Bit 2^5  (0x0020) -> frei (fuer Prioritaet)
// Bit 2^6  (0x0040) -> frei (fuer Prioritaet)
// Bit 2^7  (0x0080) -> ASCII-File fur USER-Daten ist geoeffnet 

// Bit 2^8  (0x0100) -> ASCII-file fur CONFLICT-Daten ist geoeffnet 
// Bit 2^9  (0x0200) -> Primary-Device fur ERROR-Daten ist geoeffnet
// Bit 2^10 (0x0400) -> Secondary-Device fuer ERROR-Daten ist geoeffnet
// Bit 2^11 (0x0800) -> USER-Messagetexte aus Datenbank (MSGTAB) lesen 

// Bit 2^12 (0x1000) -> CONFLICT-Messagetexte aus Datenbank (MSGTAB) lesen
// Bit 2^13 (0x2000) -> ERROR-Messagetexte aus Datenbank (MSGTAB) lesen 
// Bit 2^14 (0x4000) -> Externe Message empfangen (von Application) 
// Bit 2^15 (0x8000) -> Initialisierung (init_nmghdl) des Prozesses ist OK
*/
static unsigned int	uigStatus = IINITIALIZE;	/* Status des Prozesses */

/******************************************************************************/
/* External functions                                                         */
/******************************************************************************/
extern int  send_message(int,int,int,int,char*); /* CCS msg-handler i/f  */
extern int	init_db();
extern int	SendRemoteShutdown(int);

/******************************************************************************/
/* Function prototypes	                                                      */
/******************************************************************************/
static int 	init_nmghdl();
static int	Reset(void);                       /* Reset program          */
static void	Terminate(int);                    /* Terminate program      */
static void	HandleSignal(int);                 /* Handles signals        */
static void	HandleErr(int);                    /* Handles general errors */
static void	HandleQueErr(int);                 /* Handles queuing errors */
static int	HandleData(void);                  /* Handles event data     */
static void HandleQueues(void);                /* Waiting for Sts.-switch*/

/*//////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
////////// eigene Funktionen 
*/
static int 	MessageHandler(void);	/* Message Handler */
static int 	CreateNewLogFile(void);  /* switch filename */
static int 	WriteToFile(void);       /* ins Ascii-File schreiben */
static int 	WriteToDB(void);         /* in Datenbank schreiben */
static int 	CreateMessageText(char **, char *, char *, char); /* wie oben */
static int 	GetNextUrno(int *);	    /* next urno */
static char *AppText(int);			/* Text fuer externe Message */
static char *GetLocalTimeStamp(void); /* format YYYYMMDDHHMISS */
static char *hhmmss_strlocaltimestamp(void);
static char *ddmmyy_strlocaltimestamp(void);
static void	WriteToUniCenter(char *, int);


/******************************************************************************/
/*                                                                            */
/* The MAIN program                                                           */
/*                                                                            */
/******************************************************************************/
MAIN
{
	int		ilRC = RC_SUCCESS;			/* Return code			*/
	int		ilRcTmp;
	int		ilCnt = 0;
	char		*pclSelection;
	char		*pclFields;
	char		pclFileName[512];
	
	INITIALIZE;			/* General initialization	*/
  dbg(TRACE, "MAIN: VERSION <%s>",mks_version);

	/* Handle signale */
	(void)SetSignals(HandleSignal);
	(void)UnsetSignals();

	/* Attach to the CCS queues */
	do
	{
		if ((ilRC = init_que()) != RC_SUCCESS)
		{
			sleep(6);		/* Wait for QCP to create queues */
			ilCnt++;
		}
	} while ((ilCnt < 10) && (ilRC != RC_SUCCESS));

	if (ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_que failed!");
		Terminate(30);
	}

	/* logon to DB */
	do
	{
		dbg(DEBUG,"<MAIN> calling init_db()");
		if ((ilRC = init_db()) != RC_SUCCESS)
		{
			sleep(6);		/* Wait for next try */
			ilCnt++;
		}
	}while ((ilCnt < 10) && (ilRC != RC_SUCCESS));

	if (ilRC != RC_SUCCESS)
	{
		dbg(TRACE,"MAIN: init_db failed!");
		Terminate(30);
	}

	/* transfer binary file to remote machine... */
	pclFileName[0] = 0x00;
	sprintf(pclFileName, "%s/%s", getenv("BIN_PATH"), argv[0]);
	dbg(TRACE,"<MAIN> %05d TransferFile: <%s>", __LINE__, pclFileName);
	if ((ilRC = TransferFile(pclFileName)) != RC_SUCCESS)
	{
		dbg(TRACE,"<MAIN> TransferFile <%s> returns: %d", pclFileName, ilRC);
		set_system_state(HSB_STANDALONE);
	}

	/* transfer configuration file to remote machine... */
	pclFileName[0] = 0x00;
	sprintf(pclFileName, "%s/%s.cfg", getenv("CFG_PATH"), argv[0]);
	dbg(TRACE,"<MAIN> %05d TransferFile: <%s>", __LINE__, pclFileName);
	if ((ilRC = TransferFile(pclFileName)) != RC_SUCCESS)
	{
		dbg(TRACE,"<MAIN> TransferFile <%s> returns: %d", pclFileName, ilRC);
		set_system_state(HSB_STANDALONE);
	}

	/* ask VBL about this!! */
	dbg(TRACE,"<MAIN> %05d SendRemoteShutdown: %d", __LINE__, mod_id);
	if ((ilRC = SendRemoteShutdown(mod_id)) != RC_SUCCESS)
	{
		dbg(TRACE,"<MAIN> SendRemoteShutdown(%d) returns: %d", mod_id, ilRC);
		set_system_state(HSB_STANDALONE);
	}

	dbg(DEBUG,"main initializing ...");
	if ((ilRC = init_nmghdl()) != RC_SUCCESS)
	{	
		uigStatus &= IRESET_INIT_OK;
	}	
	else
	{
		dbg(TRACE,"<MAIN> initializing OK");
		uigStatus |= IINIT_OK;
	}

	dbg(DEBUG,"<MAIN> uigStatus -> %X", uigStatus);

	/* forever */
	while (1)
	{
		/* get next item */
		ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,0,(char*)&prgItem);

		/* set event pointer 		*/
		prgEvent = (EVENT*)prgItem->text;
	
		/* check status */
		if( ilRC == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRC != RC_SUCCESS ) 
			{
				/* handle que_ack error */
				HandleQueErr(ilRC);
			} 
			
			switch (prgEvent->command)
			{
				case HSB_STANDBY:
					dbg(DEBUG,"<MAIN> %05d got STANDBY", __LINE__);
					ctrl_sta = prgEvent->command;
					ilRC = Reset();
					send_message(IPRIO_ASCII,HSB_REQUEST,0,0,NULL);
					break;	

				case HSB_COMING_UP:
					dbg(DEBUG,"<MAIN> %05d got COMING_UP", __LINE__);
					ctrl_sta = prgEvent->command;
					send_message(IPRIO_ASCII,HSB_REQUEST,0,0,NULL);
					break;	

				case HSB_ACTIVE:
					dbg(DEBUG,"<MAIN> %05d got ACTIVE", __LINE__);
					ctrl_sta = prgEvent->command;
					ilRC = Reset();
					dbg(DEBUG,"<MAIN> %05d Reset returns with: %d", __LINE__, ilRC);
					dbg(DEBUG,"<MAIN> %05d now calling send_message", __LINE__);
					send_message(IPRIO_ASCII,HSB_REQUEST,0,0,NULL);
					dbg(DEBUG,"<MAIN> %05d after calling send_message", __LINE__);
					break;

				case HSB_STANDALONE:
					dbg(DEBUG,"<MAIN> %05d got STANDALONE", __LINE__);
					ctrl_sta = prgEvent->command;
					ilRC = Reset();
					dbg(DEBUG,"<MAIN> %05d Reset returns with: %d", __LINE__, ilRC);
					dbg(DEBUG,"<MAIN> %05d now calling send_message", __LINE__);
					send_message(IPRIO_ASCII,HSB_REQUEST,0,0,NULL);
					dbg(DEBUG,"<MAIN> %05d after calling send_message", __LINE__);
					dbg(DEBUG,"<MAIN> %05d now calling ResetDBCounter", __LINE__);
					ResetDBCounter();
					dbg(DEBUG,"<MAIN> %05d after calling ResetDBCounter", __LINE__);
					break;	

				case HSB_ACT_TO_SBY:
					dbg(DEBUG,"<MAIN> %05d got ACT_TO_SBY", __LINE__);
					ctrl_sta = prgEvent->command;
					send_message(IPRIO_ASCII,HSB_REQUEST,0,0,NULL);
					break;	

				case HSB_DOWN:
					dbg(DEBUG,"<MAIN> %05d got DOWN", __LINE__);
					ctrl_sta = prgEvent->command;
					Terminate(0);
					break;	

				case SHUTDOWN:
					Terminate(0);
					break;
						
				case RESET:
					ilRC = Reset();
					break;
						
				case EVENT_DATA:
					/* handle received data here */
					
					dbg(TRACE,"%05d:",__LINE__);
					
					ilRC = HandleData();
					dbg(TRACE,"%05d:",__LINE__);

					if ((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
					{
						if (uigStatus & IMESSAGE_EXTERN)
						{
							dbg(DEBUG,"SME Status(1): %X", uigStatus);
							/* Pointer setzen */
							pclSelection = prgCmdblk->data;
							pclFields    = (char*)pclSelection + 
												strlen(pclSelection) + 1;
							
							/* senden der Nachricht */
							ilRcTmp = tools_send_sql_rc(
											  prgEvent->originator,
											  prgCmdblk->command,
											  prgCmdblk->obj_name,
											  prgCmdblk->data,
											  pclFields,
											  AppText(ilRC),
											  ilRC);
							dbg(DEBUG,"Tools-send-sql return: %d", ilRcTmp);

							/* zuruecksetzen des Status-Words */
							uigStatus &= IRESET_MESSAGE_EXTERN;
							dbg(DEBUG,"SME Status(2): %X", uigStatus);
						}
					}
					dbg(TRACE,"%05d:",__LINE__);
					break;

				case LOG_SWITCH:
					ilRC = CreateNewLogFile();
					break;

				case REMOTE_DB:
					HandleRemoteDB(prgEvent);
					break;
						
				case TRACE_ON:
					dbg_handle_debug(prgEvent->command);
					break;

				case TRACE_OFF:
					dbg_handle_debug(prgEvent->command);
					break;

				default:
					dbg(DEBUG,"main unknown event");
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
					break;
			} 
			
			/* Handle error conditions */
			if(ilRC != RC_SUCCESS)
			{
				HandleErr(ilRC);
			}
		} 
		else 
		{
			/* Handle queuing errors */
			HandleQueErr(ilRC);
		} 
	}
} 


/******************************************************************************/
/* The initialization routine                                                 */
/******************************************************************************/
static int init_nmghdl()
{
	int				i;
	int				ilNO;
	int				ilRC;
	int				ilLines;
	int				ilReCnt;
	int				ilCurNo;
	unsigned int	uilMsgStatus;
	char				pclCfgFile[IMIN_BUF];
	char				pclTmpBuf[IMIN_BUF];
	char				pclMsgBuffer[8196];
	char				pclEntryTyp[IMIN_BUF];
	char				pclDefaultTxtFile[IMIN_BUF];
	char				pclHeaderText[IMIN_BUF];
	char				pclLaCo[ILACO_SIZE];
	char				*pclCfgPath 	= NULL;
	MsgText			*prlMsgTxt		= NULL;
	Msg				*prlMsg      	= NULL;
	FILE				*pflFp 			= NULL;

	/* debug-Ausgabe zum nachschauen des aktuellen Stati */
	dbg(DEBUG,"<Init_Nmghdl>: uigStatus -> %X", uigStatus);

	/* loeschen der Buffer */
	memset ((void*)pcgUsrLog, 0x00, IUSER_LOG_SIZE);  /* USER-Buffer */
	memset ((void*)pcgConfLog, 0x00, ICONF_LOG_SIZE); /* CONFLICT-Buffer */

	dbg(DEBUG,"<init_nmghdl> CTRL_STA: <%s>", CTRL_STA(ctrl_sta));

	/* lesen des Config-Files */
	if ((pclCfgPath = getenv("CFG_PATH")) == NULL)
	{
		dbg(TRACE,"<Init_Nmghdl>: Error reading CFG_PATH");
	}
	else
	{
		/* zusammensetzen des Pfades/Filenamens */
		sprintf(pclCfgFile, "%s/nmghdl.cfg", pclCfgPath);

		/* ------------------------------------------------------------------- */
		/* ------------------------------------------------------------------- */
		memset ((void*)pclTmpBuf, 0x00, IMIN_BUF);
		if ((ilRC = iGetConfigEntry(pclCfgFile, "main", "MESSAGE_TO_UNIIF", CFG_STRING, pclTmpBuf)) != RC_SUCCESS)
		{
			/* default */
			strcpy(pclTmpBuf, "NO"); 
		}
		StringUPR((UCHAR*)pclTmpBuf);
		if (!strcmp(pclTmpBuf, "YES"))
			igUseUniCenter = TRUE;
		else if (!strcmp(pclTmpBuf, "NO"))
			igUseUniCenter = FALSE;
		else
		{
			dbg(TRACE,"<init_nmghdl> unknown value for MESSAGE_TO_UNIIF..");
			igUseUniCenter = FALSE;
		}

		/* ------------------------------------------------------------------- */
		/* ------------------------------------------------------------------- */
		if (igUseUniCenter == TRUE)
		{
			memset((void*)pclMsgBuffer, 0x00, 8196);
			if ((ilRC = iGetConfigEntry(pclCfgFile, "main", "UNIIF_MSGNO", CFG_STRING, pclMsgBuffer)) != RC_SUCCESS)
			{
				dbg(TRACE,"<init_nmghdl> can't find entry UNIIF_MSGNO...");
				igUseUniCenter = FALSE;
			}
			else
			{
				/* how many numbers ? */
				igNoOfUniCenterMsg = GetNoOfElements(pclMsgBuffer, ',');
				dbg(DEBUG,"<init_nmghdl> found %d message numbers...", igNoOfUniCenterMsg);
				for (ilCurNo=0; ilCurNo<igNoOfUniCenterMsg; ilCurNo++)
				{
					pigUniCenterMsgNo[ilCurNo] = atoi(GetDataField(pclMsgBuffer, ilCurNo, ','));
					dbg(DEBUG,"<init_nmghdl> current no: %d", pigUniCenterMsgNo[ilCurNo]);
				}
			}
		}

		/* ------------------------------------------------------------------- */
		/* ------------------------------------------------------------------- */
		/* Entrag fuer User-Logfile auslesen */
		memset ((void*)pclTmpBuf, 0x00, IMIN_BUF);
		if ((ilRC = iGetConfigEntry(pclCfgFile, "main", "USER_LOGFILE",
						    CFG_STRING, pcgUsrLog)) != RC_SUCCESS)
		{
			/* use default Logfile-name */
			strcpy (pcgUsrLog, SDEFAULT_USR_LOGFILE); 
		}

		/* Entrag fuer Conflict-Logfile auslesen */
		memset ((void*)pclTmpBuf, 0x00, IMIN_BUF);
		if ((ilRC = iGetConfigEntry(pclCfgFile, "main", "CONF_LOGFILE",
						    CFG_STRING, pcgConfLog)) != RC_SUCCESS)
		{
			/* use default Logfile-name */
			strcpy (pcgConfLog, SDEFAULT_CONF_LOGFILE); 
		}

		if ((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
		{
			/* Entrag fuer Prioritaet auslesen */
			memset ((void*)pclTmpBuf, 0x00, IMIN_BUF);
			if ((ilRC = iGetConfigEntry(pclCfgFile, "main", "PRIO",
								 CFG_STRING, pclTmpBuf)) != RC_SUCCESS)
			{
				/* Default Prio ist hoechste Prioritaet */
				sprintf(pclTmpBuf, "%d", IMAX_PRIO);
			}
		}
		else
		{
			/* hier keine DB-Verbindung... */		
			sprintf(pclTmpBuf, "%d", IDATA_TO_ASCII);
		}
		
		/* gucken welche Prioritaet gesetzt ist und Status-Word 
		   entsprechend setzen */
		switch (atoi(pclTmpBuf))
		{
			/*
			nachfolgende BREAKs wurden nicht! vergessen, die hoehere 
			Prioritaet schliesst die niedrigeren mit ein!!
			*/
			case IPRIO_ADMIN:
				uigStatus |= IDATA_TO_ADMIN;	
			case IPRIO_AP:
				uigStatus |= IDATA_TO_AP;	
			case IPRIO_DB:
				uigStatus |= IDATA_TO_DB;	
			case IPRIO_ASCII:
				uigStatus |= IDATA_TO_ASCII;	
			case IPRIO_NOTHING:
				/* in diesem Fall brauchen wir nix zu machen... */
				break;
			default:
				/* auch hier nur ASCII-File */
				uigStatus |= IDATA_TO_ASCII;
				break;
		}
		
		/* alle Indikatoren lesen und entsprechend reagieren */
		for (i=0; i<IINDI_COUNTER; i++)
		{
			switch (i)
			{
				case 0:
					/***********    U S E R   *************/
					/* Entrag fuer User-Message-Indikator auslesen */
					memset ((void*)pclTmpBuf, 0x00, IMIN_BUF);
					if ((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
					{
						if (iGetConfigEntry(pclCfgFile, "main", "USER_MSGINDI",
											CFG_STRING, pclTmpBuf) != RC_SUCCESS)
						{
							/* Default den Text aus der FILE lesen */
							sprintf(pclTmpBuf, "%d", ITEXT_AUS_FILE);
						}
					}
					else
					{
						/* Default den Text aus der FILE lesen */
						sprintf(pclTmpBuf, "%d", ITEXT_AUS_FILE);
					}

					/* Entry -Typ in CFG File setzen */
					memset ((void*)pclEntryTyp, 0x00, IMIN_BUF);
					strcpy (pclEntryTyp, "USER_MSGTEXT");

					/* Default Textfile */
					memset ((void*)pclDefaultTxtFile, 0x00, IMIN_BUF);
					strcpy (pclDefaultTxtFile, SDEFAULT_USR_TXTFILE);

					/* Status DB oder Textfile */
					uilMsgStatus = IUSER_DB_MSGTXT;

					/* Set local pointer to record */
					prlMsg = &rgUsrMsgTxt;

					break;

				case 1:
					/*********** C O N F L I C T *************/
					/* Entrag fuer User-Message-Indikator auslesen */
					memset ((void*)pclTmpBuf, 0x00, IMIN_BUF);
					if ((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
					{
						if (iGetConfigEntry(pclCfgFile, "main", "CONF_MSGINDI",
											CFG_STRING, pclTmpBuf) != RC_SUCCESS)
						{
							/* Default den Text aus der FILE lesen */
							sprintf(pclTmpBuf, "%d", ITEXT_AUS_FILE);
						}
					}
					else
					{
						/* Default den Text aus der FILE lesen */
						sprintf(pclTmpBuf, "%d", ITEXT_AUS_FILE);
					}

					/* Entry -Typ in CFG File setzen */
					memset ((void*)pclEntryTyp, 0x00, IMIN_BUF);
					strcpy (pclEntryTyp, "CONF_MSGTEXT");

					/* Default Textfile */
					memset ((void*)pclDefaultTxtFile, 0x00, IMIN_BUF);
					strcpy (pclDefaultTxtFile, SDEFAULT_CONF_TXTFILE);
					
					/* Status DB oder Textfile */
					uilMsgStatus = ICONF_DB_MSGTXT;

					/* Set local pointer to record */
					prlMsg = &rgConfMsgTxt;

					break;

				case 2:
					/***********   E R R O R   *************/
					/* Entrag fuer User-Message-Indikator auslesen */
					memset ((void*)pclTmpBuf, 0x00, IMIN_BUF);
					if ((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
					{
						if (iGetConfigEntry(pclCfgFile, "main", "ERRO_MSGINDI",
											CFG_STRING, pclTmpBuf) != RC_SUCCESS)
						{
							/* Default den Text aus der FILE lesen */
							sprintf(pclTmpBuf, "%d", ITEXT_AUS_FILE);
						}
					}
					else
					{
						/* Default den Text aus der FILE lesen */
						sprintf(pclTmpBuf, "%d", ITEXT_AUS_FILE);
					}

					/* Entry -Typ in CFG File setzen */
					memset ((void*)pclEntryTyp, 0x00, IMIN_BUF);
					strcpy (pclEntryTyp, "ERRO_MSGTEXT");

					/* Default Textfile */
					memset ((void*)pclDefaultTxtFile, 0x00, IMIN_BUF);
					strcpy (pclDefaultTxtFile, SDEFAULT_ERROR_TXTFILE);

					/* Status DB oder Textfile */
					uilMsgStatus = IERROR_DB_MSGTXT;

					/* Set local pointer to record */
					prlMsg = &rgErrorMsgTxt;

					break;
			}

			/* falls Message-Indikator = Textfile -> Textfilewerte auslesen */
			if (atoi(pclTmpBuf) == ITEXT_AUS_FILE)
			{
				/* Entrag fuer Message-TextFile auslesen */
				memset ((void*)pclTmpBuf, 0x00, IMIN_BUF);
				if (iGetConfigEntry(pclCfgFile, "main", pclEntryTyp,
									CFG_STRING, pclTmpBuf) != RC_SUCCESS)
				{
					/* Default */
					strcpy (pclTmpBuf, pclDefaultTxtFile); 
				}

				dbg(DEBUG,"<Init_Nmghdl>: Open File %s",pclTmpBuf);
				/* jetzt lesen wir den Message-Text-File in den Speicher */
				if ((pflFp = fopen(pclTmpBuf,"r")) == NULL)
				{
					/* Can't open File - use database */
					dbg(TRACE,"<Init_Nmghdl>: Can't open Message-File %s",pclTmpBuf);

					/* set status to DB */
					uigStatus |= uilMsgStatus;
				}
				else
				{
					/* get memory for messagetext */
					if ((prlMsgTxt = (MsgText*)malloc((size_t)(IREALLOC_COUNTER*sizeof(MsgText)))) == NULL)
					{
						/* memory is not available */
						dbg(TRACE,"<Init_Nmghdl>: Usr-Malloc-Error");

						/* set status again to DB */
						uigStatus |= uilMsgStatus;
					}
					else
					{
						/* speicheranforderung hat geklappt */
						/* read the file */
						ilLines = 0;
						ilReCnt = 1;
						while (!feof(pflFp))
						{
							/* tmp buffer loeschen */
							memset ((void*)pclTmpBuf, 0x00, IMIN_BUF);

							/* komplette Zeile einlesen */
							if (fscanf(pflFp, "%[^\n]%*c", pclTmpBuf) != 1)
							{
								fscanf(pflFp, "%*c");
								continue;
							}
							else
							{
								pclTmpBuf[strlen(pclTmpBuf)] = '\n';
								if (sscanf(pclTmpBuf,"%d %s %[^\n]%*c",
										   &ilNO, pclLaCo, pclTmpBuf) == 3)
								{
									/* move data to struct */
									prlMsgTxt[ilLines].iMsgNO = ilNO;
									strcpy(prlMsgTxt[ilLines].pcLaCo, pclLaCo);
									if ((prlMsgTxt[ilLines++].pcText = 
												(char*)strdup(pclTmpBuf)) == NULL)
									{
										/* kein Speicher mehr da */
										dbg(TRACE,"<Init_Nmghdl>: Usr-strdup-Error");
									}
								}
							}

							/* gucken ob mehr Zeilen da sind, als wir Speicher
							vorgesehen haben.
							*/
							if (!(ilLines  % IREALLOC_COUNTER) && ilLines > 0)
							{
								dbg(DEBUG,"realloc at %d lines", ilLines);
								ilReCnt++;
								dbg(DEBUG,"set new size to %d", ilReCnt*IREALLOC_COUNTER);
								if ((prlMsgTxt = (MsgText*)realloc(prlMsgTxt, (size_t)(ilReCnt*IREALLOC_COUNTER*sizeof(MsgText)))) == NULL)	
								{
									/* kein Speicher mehr da */
									dbg(TRACE,"<Init_Nmghdl>: Usr-realloc-Error");
								}
							}
						}

						/* Speicher jetzt an aktuelle Groesse anpassen 
						da der Speicher hier nicht mehr wachsen kann, koennen
						wir uns eine Abfrage auf NULL sparen...
						*/
						dbg(DEBUG,"set memory size to %d lines", ilLines);
						prlMsgTxt = (MsgText*)realloc(prlMsgTxt, (size_t)(ilLines*sizeof(MsgText)));	
					
						/* set global Pointer */
						prlMsg->prMsgText = prlMsgTxt;
						prlMsg->iAnzahl   = ilLines;
						prlMsgTxt         = NULL;
						prlMsg			  	= NULL;
					}
					/* close the file */
					fclose (pflFp);
				}
			}
			else
			{
				/* Status auf Messge-Text aus DB setzen */
				uigStatus |= uilMsgStatus;
			}
		}
	}
	
	/* Das USER + CONFLCIT + ERROR-Logfile oeffnen wir nur falls 
	// alles notwendige gesetzt ist...
	*/
	if (uigStatus & IDATA_TO_ASCII)
	{
		/* hier oeffnen wird die LOG-Files 
		// wird fangen mit dem CEDA.LOG (Error-Msg) an
		*/
		if ((igPrimLog = open (psptrec->pmsgdev, 
							   O_RDWR | O_CREAT | O_APPEND, IPERMS)) == -1)
		{
			/* Can't open primary device */
			dbg(TRACE,"<Init_Nmghdl>: Can't open primary device(%d),[%s]",
				errno, strerror(errno));	
		}
		else
		{
			/* das �ffnen des Devices hat geklappt - Status setzen */
			uigStatus |= IPRIM_LFOPEN;

			/* gucken ob das File schon da ist und falls nicht den 
			Header schreiben
			*/
			if (lseek(igPrimLog, 0L, SEEK_END) == 0)
			{
				memset ((void*)pclHeaderText, 0x00, IMIN_BUF);
				sprintf(pclHeaderText, SHEADER_TEXT, 
						psptrec->pmsgdev,
						ddmmyy_strlocaltimestamp(),
						hhmmss_strlocaltimestamp());
				write (igPrimLog, pclHeaderText, strlen(pclHeaderText)); 	
			}
		}

		/* oeffnen des secondary device */
		if (psptrec->devindic == IUSE_SECONDARY_DEVICE)
		{
			/* display on both devices */
			if ((igSecoLog = open (psptrec->smsgdev, 
								   O_RDWR | O_CREAT | O_APPEND, IPERMS)) == -1)
			{
				/* Can't open secondary device */
				dbg(TRACE,"<Init_Nmghdl>: Can't open secondary device(%d),[%s]",
					errno, strerror(errno));	
			}
			else
			{
				/* das �ffnen des Devices hat geklappt - Status setzen */
				uigStatus |= ISECO_LFOPEN;
				
				/* gucken ob das File schon da ist und falls nicht den 
				Header schreiben
				*/
				if (lseek(igSecoLog, 0L, SEEK_END) == 0)
				{
					memset ((void*)pclHeaderText, 0x00, IMIN_BUF);
					sprintf(pclHeaderText, SHEADER_TEXT, 
							psptrec->smsgdev,
							ddmmyy_strlocaltimestamp(),
							hhmmss_strlocaltimestamp());
					write (igSecoLog, pclHeaderText, strlen(pclHeaderText)); 	
				}
			}
		}
		
		/* oeffnen des USER-LogFiles */
		if ((igUsrLog = open (pcgUsrLog, 
							   O_RDWR | O_CREAT | O_APPEND, IPERMS)) == -1)
		{
			/* Can't open USER-Logfile */
			dbg(TRACE,"<Init_Nmghdl>: Can't open USER-Logfile(%d),[%s]",
				errno, strerror(errno));
		}
		else
		{
			/* das oeffnen des UserLogFiles hat geklappt -> Status setzen */
			uigStatus |= IUSER_LFOPEN;

			/* gucken ob das File schon da ist und falls nicht den 
			Header schreiben
			*/
			if (lseek(igUsrLog, 0L, SEEK_END) == 0)
			{
				memset ((void*)pclHeaderText, 0x00, IMIN_BUF);
				sprintf(pclHeaderText, SHEADER_TEXT, 
						pcgUsrLog,
						ddmmyy_strlocaltimestamp(),
						hhmmss_strlocaltimestamp());
				write (igUsrLog, pclHeaderText, strlen(pclHeaderText)); 	
			}
		}
		
		/* oeffnen des CONFLICT-LogFiles */
		if ((igConfLog = open (pcgConfLog, 
							   O_RDWR | O_CREAT | O_APPEND, IPERMS)) == -1)
		{
			/* Can't open CONFLICT-Logfile */
			dbg(TRACE,"<Init_Nmghdl>: Can't open CONFLICT-Logfile(%d),[%s]",
				errno, strerror(errno));
		}
		else
		{
			/* das oeffnen des UserLogFiles hat geklappt -> Status setzen */
			uigStatus |= ICONF_LFOPEN;

			/* gucken ob das File schon da ist und falls nicht den 
			Header schreiben
			*/
			if (lseek(igConfLog, 0L, SEEK_END) == 0)
			{
				memset ((void*)pclHeaderText, 0x00, IMIN_BUF);
				sprintf(pclHeaderText, SHEADER_TEXT, 
						pcgConfLog,
						ddmmyy_strlocaltimestamp(),
						hhmmss_strlocaltimestamp());
				write (igConfLog, pclHeaderText, strlen(pclHeaderText)); 	
			}
		}
	}	

	if ((ctrl_sta == HSB_STANDALONE) || (ctrl_sta == HSB_ACTIVE) || (ctrl_sta == HSB_ACT_TO_SBY))
	{
		if (!(uigStatus & IDB_CONNECT))
		{
			if (uigStatus & IDATA_TO_DB     ||
				 uigStatus & IUSER_DB_MSGTXT ||
				 uigStatus & ICONF_DB_MSGTXT ||
				 uigStatus & IERROR_DB_MSGTXT)
			{
				uigStatus |= IDB_CONNECT;
			}
		}
	}
	else
	{
		/* Prio DB entfernen */
		uigStatus &= IRESET_DATA_TO_DB;
		uigStatus &= IRESET_USER_DB_MSGTXT;
		uigStatus &= IRESET_CONF_DB_MSGTXT;
		uigStatus &= IRESET_ERROR_DB_MSGTXT;
	}

	/* get tablename */
	if (uigStatus & IDB_CONNECT)
	{
		/* next info from SGS.TAB, search TABEND... */
		memset((void*)pcgTABEnd, 0x00, iMIN);
		if ((ilRC = tool_search_exco_data("ALL", "TABEND", pcgTABEnd)) != RC_SUCCESS)
		{
			dbg(TRACE, "<init_nmghdl> can't find entry (\"TABEND\") in sgs.tab");
			Terminate(30);
		}
		dbg(DEBUG,"<init_nmghdl> found TABEND: <%s>", pcgTABEnd);

		memset ((void*)pcgLogTableName, 0x00, iMIN);
		if (iGetConfigEntry(pclCfgFile, "main", "MESSAGE_LOG_TABLE",
								CFG_STRING, pcgLogTableName) != RC_SUCCESS)
		{
			/* Default */
			dbg(DEBUG,"<init_nmghdl> error reading MESSAGE_LOG_TABLE...");
			dbg(DEBUG,"<init_nmghdl> setting tablename to: \"LOG\"");
			sprintf(pcgLogTableName, "%s%s", "LOG", pcgTABEnd);
		}
		dbg(DEBUG,"<init_nmghdl> LogTableName is: <%s>", pcgLogTableName);

		memset ((void*)pcgMessageTextTableName, 0x00, iMIN);
		if (iGetConfigEntry(pclCfgFile, "main", "MESSAGE_TEXT_TABLE",
								CFG_STRING, pcgMessageTextTableName) != RC_SUCCESS)
		{
			/* Default */
			dbg(DEBUG,"<init_nmghdl> error reading MESSAGE_TEXT_TABLE...");
			dbg(DEBUG,"<init_nmghdl> setting tablename to: \"MSG\"");
			sprintf(pcgMessageTextTableName, "%s%s", "MSG", pcgTABEnd);
		}
		dbg(DEBUG,"<init_nmghdl> MessageTextTableName is: <%s>", pcgMessageTextTableName);
	}

	/* debug-Ausgabe zum nachschauen des aktuellen Stati */
	dbg(DEBUG,"<Init_Nmghdl>: uigStatus -> %X", uigStatus);

	/* und zurueck */
	return ilRC;
} 


/******************************************************************************/
/* The Reset routine                                                          */
/******************************************************************************/
static int Reset()
{
	int	i;
	int	ilRC;
	int	ilDBConnect;

	/* falls noch LOG-Files offen sind sollten diese geschlossen werden
	// User-Daten logfile
	*/
	if (uigStatus & IUSER_LFOPEN)
	{
		close (igUsrLog);			
		uigStatus &= IRESET_USER_LFOPEN;
	}

	/* Conflict-Daten logfile */
	if (uigStatus & ICONF_LFOPEN)
	{
		close (igConfLog);			
		uigStatus &= IRESET_CONF_LFOPEN;
	}

	/* Primary Device Error-Daten logfile (CEDA.LOG) */
	if (uigStatus & IPRIM_LFOPEN)
	{
		close (igPrimLog);			
		uigStatus &= IRESET_PRIM_LFOPEN;
	}

	/* Secondary Device Error-Daten logfile (CEDA.LOG) */
	if (uigStatus & ISECO_LFOPEN)
	{
		close (igSecoLog);			
		uigStatus &= IRESET_SECO_LFOPEN;
	}

	/* Speicher der gelesenen MessageTextFile wieder freigben */
	if (!(uigStatus & IUSER_DB_MSGTXT))
	{
		/* Speicher von USER freigeben */
		for (i=0; i<rgUsrMsgTxt.iAnzahl; i++)
			free ((void*)rgUsrMsgTxt.prMsgText[i].pcText);
		free ((void*)rgUsrMsgTxt.prMsgText);
	}

	if (!(uigStatus & ICONF_DB_MSGTXT))
	{
		/* Speicher von CONF freigeben */
		for (i=0; i<rgConfMsgTxt.iAnzahl; i++)
			free ((void*)rgConfMsgTxt.prMsgText[i].pcText);
		free ((void*)rgConfMsgTxt.prMsgText);
	}
	
	if (!(uigStatus & IERROR_DB_MSGTXT))
	{
		/* Speicher von ERROR freigeben */
		for (i=0; i<rgErrorMsgTxt.iAnzahl; i++)
			free ((void*)rgErrorMsgTxt.prMsgText[i].pcText);
		free ((void*)rgErrorMsgTxt.prMsgText);
	}

	/* free memory */
	if (prgItem != NULL)
	{
		free((void*)prgItem);
		prgItem = NULL;
	}

	ilDBConnect = FALSE;
	if (uigStatus & IDB_CONNECT)
	{
		ilDBConnect = TRUE;
		uigStatus &= IRESET_DB_CONNECT;
	}

	/* clear status byte... */
	uigStatus = 0x0000;

	/* we don't close db-connection in reset, so set status */
	if (ilDBConnect == TRUE)
	{
		uigStatus |= IDB_CONNECT;
	}

	/* debug-Ausgabe zum nachschauen des aktuellen Stati */
	dbg(DEBUG,"<Reset>: uigStatus -> %X", uigStatus);

	dbg(DEBUG,"initializing in reset...");
	if ((ilRC = init_nmghdl()) != RC_SUCCESS)
	{	
		dbg(TRACE,"init_nmghdl in reset returns: %d", ilRC);
		Terminate(0);
	}	
	else
	{
		dbg(DEBUG,"main initializing in reset OK");
		uigStatus |= IINIT_OK;
	}
	dbg(DEBUG,"<Reset> uigStatus -> %X", uigStatus);

	/* pointer to event-structure... */
	prgEvent = (EVENT*)prgItem->text;

	dbg(DEBUG,"Reset now leaving...");
	return RC_SUCCESS;
}


/******************************************************************************/
/* The termination routine                                                    */
/******************************************************************************/
static void Terminate(int ipSleepTime)
{
	int		i;
	int		ilRC;
	
	if (ipSleepTime > 0)
	{
		dbg(DEBUG,"<Terminate> sleeping %d seconds...", ipSleepTime);
		sleep((UINT)ipSleepTime);
	}

	/* ignore all signals */
	(void)UnsetSignals();

	/* falls Verbindung zur DB da ist sollten wir diese hier beenden */
	if (uigStatus & IDB_CONNECT)
	{
		dbg(DEBUG,"<Terminate> logoff now...");
		if ((ilRC = logoff()) != RC_SUCCESS)
			dbg(TRACE,"<Terminate> - logoff failure(%d)", ilRC);	
		else
			/* Status-Word setzen */
			uigStatus &= IRESET_DB_CONNECT;	
	}

	/* falls noch LOG-Files offen sind sollten diese geschlossen werden
	// User-Daten logfile
	*/
	if (uigStatus & IUSER_LFOPEN)
	{
		close (igUsrLog);			
		uigStatus &= IRESET_USER_LFOPEN;
	}

	/* Conflict-Daten logfile */
	if (uigStatus & ICONF_LFOPEN)
	{
		close (igConfLog);			
		uigStatus &= IRESET_CONF_LFOPEN;
	}

	/* Primary Device Error-Daten logfile (CEDA.LOG) */
	if (uigStatus & IPRIM_LFOPEN)
	{
		close (igPrimLog);			
		uigStatus &= IRESET_PRIM_LFOPEN;
	}

	/* Secondary Device Error-Daten logfile (CEDA.LOG) */
	if (uigStatus & ISECO_LFOPEN)
	{
		close (igSecoLog);			
		uigStatus &= IRESET_SECO_LFOPEN;
	}

	/* Speicher der gelesenen MessageTextFile wieder freigben */
	if (!(uigStatus & IUSER_DB_MSGTXT))
	{
		/* Speicher von USER freigeben */
		for (i=0; i<rgUsrMsgTxt.iAnzahl; i++)
			free ((void*)rgUsrMsgTxt.prMsgText[i].pcText);
		free ((void*)rgUsrMsgTxt.prMsgText);
	}

	if (!(uigStatus & ICONF_DB_MSGTXT))
	{
		/* Speicher von CONF freigeben */
		for (i=0; i<rgConfMsgTxt.iAnzahl; i++)
			free ((void*)rgConfMsgTxt.prMsgText[i].pcText);
		free ((void*)rgConfMsgTxt.prMsgText);
	}
	
	if (!(uigStatus & IERROR_DB_MSGTXT))
	{
		/* Speicher von ERROR freigeben */
		for (i=0; i<rgErrorMsgTxt.iAnzahl; i++)
			free ((void*)rgErrorMsgTxt.prMsgText[i].pcText);
		free ((void*)rgErrorMsgTxt.prMsgText);
	}

	/* free memory */
	if (prgItem != NULL)
		free((void*)prgItem);

	/* debug-Ausgabe zum nachschauen des aktuellen Stati */
	dbg(DEBUG,"<TERMINATE>: uigStatus -> %X", uigStatus);
	
	/* default error message */
	dbg(TRACE,"<Terminate> now leaving ...");

	/* close logfile... */
	CLEANUP;

	/* exit ist immer gut */
	exit(0);
} /* end of Terminate */


/******************************************************************************/
/* The handle signals routine                                                 */
/******************************************************************************/
static void HandleSignal(int ipSig)
{
	if (ipSig != 18)
		dbg(DEBUG,"HandleSignal: signal <%d> received", ipSig);

	switch (ipSig)
	{
		case SIGCLD:
			/* don't call SetSignals here */
			break;

		default:
			exit(0);
			break;
	} 
	return;
}


/******************************************************************************/
/* The handle general error routine                                           */
/******************************************************************************/
static void HandleErr(int pipErr)
{
	switch (pipErr)
	{
		case IPRIO_FAIL:
			dbg(DEBUG,"<HandleErr> - prio failure(%d)", pipErr);
			break;
		case IFIELD1_FAIL:
			dbg(DEBUG,"<HandleErr> - missing field 1(%d)", pipErr);
			break;
		case IFIELD2_FAIL:
			dbg(DEBUG,"<HandleErr> - missing field 2(%d)", pipErr);
			break;
		case IFIELD3_FAIL:
			dbg(DEBUG,"<HandleErr> - missing field 3(%d)", pipErr);
			break;
		case IFIELD4_FAIL:
			dbg(DEBUG,"<HandleErr> - missing field 4(%d)", pipErr);
			break;
		case IFIELD5_FAIL:
			dbg(DEBUG,"<HandleErr> - missing field 5(%d)", pipErr);
			break;
		case IFIELD6_FAIL:
			dbg(DEBUG,"<HandleErr> - missing field 6(%d)", pipErr);
			break;
		case IFIELD7_FAIL:
			dbg(DEBUG,"<HandleErr> - missing field 7(%d)", pipErr);
			break;
		case IFIELD8_FAIL:
			dbg(DEBUG,"<HandleErr> - missing field 8(%d)", pipErr);
			break;
		case IFIELD9_FAIL:
			dbg(DEBUG,"<HandleErr> - missing field 9(%d)", pipErr);
			break;
		case IMSGNO_FAIL:
			dbg(DEBUG,"<HandleErr> - message number failure(%d)", pipErr);
			break;
		case ICOMMAND_FAIL:
			dbg(DEBUG,"<HandleErr> - unknown command(%d)", pipErr);
			break;
		case IORIGIN_FAIL:
			dbg(DEBUG,"<HandleErr> - origin failure(%d)", pipErr);
			break;
		default:
			dbg(DEBUG,"<HandleErr> - unknown error(%d)", pipErr);
			break;
	}
	return;
} /* end of HandleErr */


/******************************************************************************/
/* The handle queuing error routine                                           */
/******************************************************************************/
static void HandleQueErr(int pipErr)
{
	switch (pipErr) 
	{
		case	QUE_E_FUNC	:	/* Unknown function */
			dbg(TRACE,"<%d> : unknown function",pipErr);
			break;

		case	QUE_E_MEMORY	:	/* Malloc reports no memory */
			dbg(TRACE,"<%d> : malloc failed",pipErr);
			break;

		case	QUE_E_SEND	:	/* Error using msgsnd */
			dbg(TRACE,"<%d> : msgsnd failed",pipErr);
			break;

		case	QUE_E_GET	:	/* Error using msgrcv */
			dbg(TRACE,"<%d> : msgrcv failed",pipErr);
			break;

		case	QUE_E_EXISTS	:
			dbg(TRACE,"<%d> : route/queue already exists ",pipErr);
			break;

		case	QUE_E_NOFIND	:
			dbg(TRACE,"<%d> : route not found ",pipErr);
			break;

		case	QUE_E_ACKUNEX	:
			dbg(TRACE,"<%d> : unexpected ack received ",pipErr);
			break;

		case	QUE_E_STATUS	:
			dbg(TRACE,"<%d> :  unknown queue status ",pipErr);
			break;

		case	QUE_E_INACTIVE	:
			dbg(TRACE,"<%d> : queue is inaktive ",pipErr);
			break;

		case	QUE_E_MISACK	:
			dbg(TRACE,"<%d> : missing ack ",pipErr);
			break;

		case	QUE_E_NOQUEUES	:
			dbg(TRACE,"<%d> : queue does not exist",pipErr);
			break;

		case	QUE_E_RESP	:	/* No response on CREATE */
			dbg(TRACE,"<%d> : no response on create",pipErr);
			break;

		case	QUE_E_FULL	:
			dbg(TRACE,"<%d> : too many route destinations",pipErr);
			break;

		case	QUE_E_NOMSG	:	/* No message on queue */
			dbg(TRACE,"<%d> : no messages on queue",pipErr);
			break;

		case	QUE_E_INVORG	:	/* Mod id by que call is 0 */
			dbg(TRACE,"<%d> : invalid originator=0",pipErr);
			break;

		case	QUE_E_NOINIT	:	/* Queues is not initialized*/
			dbg(TRACE,"<%d> : queues are not initialized",pipErr);
			break;

		case	QUE_E_ITOBIG	:
			dbg(TRACE,"<%d> : requestet itemsize to big ",pipErr);
			break;

		case	QUE_E_BUFSIZ	:
			dbg(TRACE,"<%d> : receive buffer to small ",pipErr);
			break;

		default			:	/* Unknown queue error */
			dbg(TRACE,"<%d> : unknown error",pipErr);
			break;
	} /* end switch */
         
	return;

} /* end of HandleQueErr */


/******************************************************************************/
/* The handle queues routine                                                  */
/******************************************************************************/
static void HandleQueues()
{
	int	ilRC = RC_SUCCESS;			/* Return code */
	int	ilBreakOut = FALSE;
	
	do
	{
		/* get next item */
		ilRC = que(QUE_GETBIG,0,mod_id,PRIORITY_3,0,(char*)&prgItem);

		/* set event pointer */
		prgEvent = (EVENT*)prgItem->text;

		/* check returncode */
		if( ilRC == RC_SUCCESS )
		{
			/* Acknowledge the item */
			ilRC = que(QUE_ACK,0,mod_id,0,0,NULL);
			if( ilRC != RC_SUCCESS ) 
			{
				HandleQueErr(ilRC);
			} 
		
			switch (prgEvent->command)
			{
				case HSB_STANDBY	:
					dbg(DEBUG,"<HandleQueues> %05d got STANDBY", __LINE__);
					ctrl_sta = prgEvent->command;
					ilRC = Reset();
					send_message(IPRIO_ASCII,HSB_REQUEST,0,0,NULL);
					break;	
		
				case HSB_COMING_UP	:
					dbg(DEBUG,"<HandleQueues> %05d got COMING_UP", __LINE__);
					ctrl_sta = prgEvent->command;
					send_message(IPRIO_ASCII,HSB_REQUEST,0,0,NULL);
					break;	
		
				case HSB_ACTIVE	:
					dbg(DEBUG,"<HandleQueues> %05d got ACTIVE", __LINE__);
					ctrl_sta = prgEvent->command;
					ilRC = Reset();
					send_message(IPRIO_ASCII,HSB_REQUEST,0,0,NULL);
					ilBreakOut = TRUE;
					break;

				case HSB_STANDALONE	:
					dbg(DEBUG,"<HandleQueues> %05d got STANDALONE", __LINE__);
					ctrl_sta = prgEvent->command;
					ilRC = Reset();
					send_message(IPRIO_ASCII,HSB_REQUEST,0,0,NULL);
					ResetDBCounter();
					ilBreakOut = TRUE;
					break;	

				case HSB_ACT_TO_SBY	:
					dbg(DEBUG,"<HandleQueues> %05d got ACT_TO_SBY", __LINE__);
					ctrl_sta = prgEvent->command;
					break;	
		
				case HSB_DOWN	:
					dbg(DEBUG,"<HandleQueues> %05d got DOWN", __LINE__);
					ctrl_sta = prgEvent->command;
					Terminate(0);
					break;	
		
				case SHUTDOWN	:
					Terminate(0);
					break;
							
				case RESET		:
					ilRC = Reset();
					break;

				case EVENT_DATA	:
					dbg(DEBUG,"HandleQueues: wrong hsb status <%d>",ctrl_sta);
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
					break;

				case LOG_SWITCH:
					ilRC = CreateNewLogFile();
					break;

				case REMOTE_DB:
					HandleRemoteDB(prgEvent);
					break;
						
				case TRACE_ON :
					dbg_handle_debug(prgEvent->command);
					break;

				case TRACE_OFF :
					dbg_handle_debug(prgEvent->command);
					break;

				default			:
					dbg(DEBUG,"HandleQueues: unknown event");
					DebugPrintItem(TRACE,prgItem);
					DebugPrintEvent(TRACE,prgEvent);
					break;
			} /* end switch */
			
			/* Handle error conditions */
			if(ilRC != RC_SUCCESS)
			{
				HandleErr(ilRC);
			} /* end if */
		} 
		else 
		{
			/* Handle queuing errors */
			HandleQueErr(ilRC);
		} 
	} while (ilBreakOut == FALSE);
} 


static int GetStringFromXml(char *pcpKey,char *pcpData,char *pcpDest,int ipLen)
{
	int ilRc = RC_FAIL;
	char *pclStart, *pclEnd;
	char clToken[124];
	
/*	dbg(TRACE,"GetStringFromXml: Key <%s> Data <%s> Len %d Dest %p",
			pcpKey,pcpData,ipLen,pcpDest);*/
	sprintf(clToken,"<%s>",pcpKey);
	pclStart = strstr(pcpData,clToken);
	if (pclStart != NULL)
	{
		sprintf(clToken,"</%s>",pcpKey);
		pclEnd = strstr(pcpData,clToken);
		if (pclEnd != NULL)
		{
			pclStart = strchr(pclStart,'>') + 1;
			*pclEnd = '\0';
		    strncpy(pcpDest,pclStart,ipLen);
		    pcpDest[ipLen] = 0;
		    ilRc = RC_SUCCESS;
		    *pclEnd = '<';
		}
	}
	if (ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"Tag (%s) not found in (%s)",pcpKey,pcpData);
		return ilRc;
	}
	/*
	else
	{
		dbg(TRACE,"Found: <%s>",pcpDest);
	}*/
		
	return ilRc;
}



static int GetIntFromXml(char *pcpKey,char *pcpData,int *pipValue)
{
	int ilRc = RC_FAIL;
	char *pclStart, *pclEnd;
	char clToken[124];

/*	dbg(TRACE,"GetIntFromXml: Key <%s> Data <%s> ",
			pcpKey,pcpData);*/
	
	sprintf(clToken,"<%s>",pcpKey);
	pclStart = strstr(pcpData,clToken);
	if (pclStart != NULL)
	{
		sprintf(clToken,"</%s>",pcpKey);
		pclEnd = strstr(pcpData,clToken);
		if (pclEnd != NULL)
		{
			pclStart = strchr(pclStart,'>') + 1;
			*pclEnd = '\0';
			dbg(TRACE,"<%s>",pclStart);
		    *pipValue = atoi(pclStart);
		    ilRc = RC_SUCCESS;
			*pclEnd = '<';
		}
	}
	if (ilRc != RC_SUCCESS)
	{
		dbg(TRACE,"Tag (%s) not found in (%s)",pcpKey,pcpData);
	}
/*	else
	{
		dbg(TRACE,"Found: <%d>",*pipValue);
	}*/
		
	return ilRc;
}
/******************************************************************************/
/* The handle data routine                                                    */
/******************************************************************************/
static int HandleData()
{
	int					ilRC;
 	char					*pclMInfo  = NULL;
	char					*pclMBlk   = NULL;
	char					pclTmpBuf[IMIN_BUF];
	static MSG_INFO_BLK	rlMsgInfo;
	char                    *pclMsgInfo;
	int						ilTmp;
	char                    clTmp[1024];
	
	dbg(DEBUG,"<HandleData> ============================");
	dbg(DEBUG,"<HandleData> ----- START -----");

	/* Pointer auf BCHead */
	prgBCHead = (BC_HEAD*)((char*)prgEvent + sizeof(EVENT));

	/* Pointer auf Cmdblk */
	prgCmdblk = (CMDBLK*)((char*)prgBCHead->data);

	/* interne oder externe Message? */
					dbg(TRACE,"%05d:",__LINE__);
	if (!strcmp(prgCmdblk->command, SMESSAGE_INTERN))
	{
		/* hier haben wir eine intern Message empfangen, jetzt das
		Status-Word entsprechend setzen
		*/
		uigStatus &= IRESET_MESSAGE_EXTERN;  
					dbg(TRACE,"%05d:",__LINE__);

		/* hier Pointer auf Message-Info setzen */
		pclMsgInfo = (char*)prgCmdblk->data;
		prgMsgInfo = &rgMsgInfo;
		memset(prgMsgInfo,0,sizeof(rgMsgInfo)); 
		dbg(DEBUG,"%05d: <%s>",__LINE__,pclMsgInfo);
		if (GetIntFromXml("VAL",pclMsgInfo,&ilTmp) == RC_SUCCESS)
		{
			prgMsgInfo->msg_val = ilTmp;
		}
		if (GetIntFromXml("MSG_ORIGIN",pclMsgInfo,&ilTmp) == RC_SUCCESS)
		{
			prgMsgInfo->msg_origin = ilTmp;
		}
		if (GetStringFromXml("PROG_NAME",pclMsgInfo,clTmp,PROG_NAME_LEN) == RC_SUCCESS)
		{
			strcpy(prgMsgInfo->prog_name,clTmp);
		}
		if (GetIntFromXml("MSG_NO",pclMsgInfo,&ilTmp) == RC_SUCCESS)
		{
			prgMsgInfo->msgblk.msg_no = ilTmp;
		}
		if (GetIntFromXml("MSG_PRIO",pclMsgInfo,&ilTmp) == RC_SUCCESS)
		{
			prgMsgInfo->msgblk.prio = ilTmp;
		}

		if (GetStringFromXml("MSG_DATA",pclMsgInfo,clTmp,
			MSG_DATA_LEN) == RC_SUCCESS)
			{
				strcpy(prgMsgInfo->msgblk.msg_data,clTmp);
			}
		
		
		/* bei einer internen Message benutzen wir den
		Standard-LaCo aus der SGS.TAB
		*/
		if (tool_search_exco_data("MSG","LANG", prgMsgInfo->lang_code) != RC_SUCCESS)
		{
			/* Use default language code */
			strcpy(prgMsgInfo->lang_code, SDEFAULT_LACO);
		}
	} 
	else
	{ 
		if (!strcmp(prgCmdblk->command, SMESSAGE_EXTERN)) 
		{
			/* hier haben wir eine externe Message empfangen, jetzt das
			Status-Word entsprechend setzen
			*/ 
			uigStatus |= IMESSAGE_EXTERN; 

			/* Externe Message von einer Applikation */
			/* Pointer auf die drei Strings setzen */
			pclMInfo = (char*)prgCmdblk->data;
			pclMInfo = ((char*)pclMInfo + strlen(pclMInfo) + 1); /* hinter \0 */
			pclMBlk  = ((char*)pclMInfo + strlen(pclMInfo) + 1); /* hinter \0 */

			dbg(DEBUG,"pclMsgInfo: %s", pclMInfo);
			dbg(DEBUG,"pclMsgBlk : %s", pclMBlk);
			/* bei einer externen Message kommt die MSG_INFO_BLK-Struktur
			nicht vor, die Daten kommen in obigen Strings (Komma getrennt)
			- nun das umsetzen der Strings auf die MSG_INFO_BLK-Strukur zwecks
			enfacherer Weiterverarbeitung!! 
			*/

			/* zuerst die MSG_INFO_BLK Struktur ...*/
			if (flt_cp_nth_field(pclMInfo, 1, rlMsgInfo.lang_code) != RC_SUCCESS)
			{
				dbg(TRACE, "<HandleData> Can't get msginfo field no 1");
				return IFIELD1_FAIL;
			}
			else
			{
				memset((void*)pclTmpBuf, 0x00, IMIN_BUF);
				if (flt_cp_nth_field(pclMInfo, 2, pclTmpBuf) != RC_SUCCESS)
				{
					dbg(TRACE, "<HandleData> Can't get msginfo field no 2");
					return IFIELD2_FAIL;
				}
				else
				{
					rlMsgInfo.msg_val = atoi(pclTmpBuf);
					memset((void*)pclTmpBuf, 0x00, IMIN_BUF);
					if (flt_cp_nth_field(pclMInfo, 3, rlMsgInfo.prog_name) != RC_SUCCESS)
					{
						dbg(TRACE, "<HandleData> Can't get msginfo field no 3");
						return IFIELD3_FAIL;
					}
				}
			}

			/* ... dann die MSGBLK Struktur */
			memset((void*)pclTmpBuf, 0x00, IMIN_BUF);
			if (flt_cp_nth_field(pclMBlk, 1, pclTmpBuf) != RC_SUCCESS)
			{
				dbg(TRACE, "<HandleData> Can't get msgblk field no 1");
				return IFIELD4_FAIL;
			}
			else
			{
				rlMsgInfo.msgblk.msg_no = atoi(pclTmpBuf);
				memset((void*)pclTmpBuf, 0x00, IMIN_BUF);
				if (flt_cp_nth_field(pclMBlk, 2, pclTmpBuf) != RC_SUCCESS)
				{
					dbg(TRACE, "<HandleData> Can't get msgblk field no 2");
					return IFIELD5_FAIL;
				}
				else
				{
					rlMsgInfo.msgblk.prio = atoi(pclTmpBuf);
					memset((void*)pclTmpBuf, 0x00, IMIN_BUF);
					if (flt_cp_nth_field(pclMBlk, 3, rlMsgInfo.msgblk.flight_no) != RC_SUCCESS)
					{
						dbg(TRACE, "<HandleData> Can't get msgblk field no 3");
						return IFIELD6_FAIL;
					}
					else
					{
						if (flt_cp_nth_field(pclMBlk, 4, rlMsgInfo.msgblk.rot_no) != RC_SUCCESS)
						{
							dbg(TRACE, "<HandleData> Can't get msgblk field no 4");
							return IFIELD7_FAIL;
						}
						else
						{
							if (flt_cp_nth_field(pclMBlk, 5, rlMsgInfo.msgblk.ref_fld) != RC_SUCCESS)
							{
								dbg(TRACE, "<HandleData> Can't get msgblk field no 5");
								return IFIELD8_FAIL;
							}
							else
							{
								if (flt_cp_nth_field(pclMBlk, 6, rlMsgInfo.msgblk.msg_data) != RC_SUCCESS)
								{
									dbg(TRACE, "<HandleData> Can't get msgblk field no 6");
									return IFIELD9_FAIL;
								}
							}
						}
					}
				}
			}

			/* jetzt noch die msg_origin aus der msg_no bestimmen */
			/* in Abhaengigkeit vom Nummernkreis den Messagetyp bestimmen */
			if (IsUserMsg(rlMsgInfo.msgblk.msg_no))
				rlMsgInfo.msg_origin = IUSER_MSG;
			else
				if (IsConfMsg(rlMsgInfo.msgblk.msg_no))
					rlMsgInfo.msg_origin = ICONFLICT_MSG;
				else
					if (IsErrorMsg(rlMsgInfo.msgblk.msg_no))
						rlMsgInfo.msg_origin = IERROR_MSG;
					else
					{
						dbg(TRACE,"<HandleData> unknown message number");
						return IMSGNO_FAIL;
					}

			/* hier hat das umkopieren der Daten aus den drei Strings in die 
			lokale MSG_INGO-Struktur geklappt und wir koennen den globalen 
			MSG_INFO_BLK-Pointer positionieren...
			*/
			prgMsgInfo = &rlMsgInfo;
		}
		else
		{
			/* falsches Kommando */
			dbg(TRACE,"<HandleData> Unknown command: %s", prgCmdblk->command);
			return ICOMMAND_FAIL;
		}
	}

	/* jetzt koennen wir mit der eigentlichen Verarbeitung fortfahren...
	gucken ob in origin was sinnvolles steht... 
	*/
	if (prgMsgInfo->msg_origin < IFIRST_MSG || 
		prgMsgInfo->msg_origin > ILAST_MSG)
	{
		/* unsinnige Werte */
		dbg(TRACE, "<HandleData> Unknown message origin %d", prgMsgInfo->msg_origin);
		return IORIGIN_FAIL;
	}

	/* aufruf der eigentlichen Verarbeitung der Nachrichten
	*/
	ilRC = MessageHandler();

	dbg(DEBUG,"<HandleData> ----- END -----");
	dbg(DEBUG,"<HandleData> ============================\n\n");

	return ilRC;
} /* end of HandleData */

/*//////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////// Behandler der Message
*/
static int MessageHandler(void)
{
	int		ilRC = RC_SUCCESS;

	dbg(DEBUG,"<MessageHandler> ----- START -----");
	
	/* mit welcher Prioritaet beginnen wir die Verarbeitung?
	*/
	switch (prgMsgInfo->msgblk.prio)
	{
		/* ------------ Break wurde nicht vergessen ----------
		// falls eine Prioritaet ausgeweahlt wurde 
		// muessen alle nachfolgenden Prios (alle niedrigeren)
		// ebenfalls ausgef�hrt werden!!!
		*/
		case IPRIO_ADMIN:
			/* bei dieser Prioritaet den Systemadminsitrator benachrichtigen */
			if (uigStatus & IDATA_TO_ADMIN)
			{
			}
			else
			{
				dbg(DEBUG,"<MsgHandler>: Prio-Admin-Failure (Status: %X)", uigStatus);
			}

		case IPRIO_AP:
			/* bei dieser Prioritaet den Alarm-Drucker ansprechen */
			if (uigStatus & IDATA_TO_AP)
			{
			}
			else
			{
				dbg(DEBUG,"<MsgHandler>: Prio-AP-Failure (Status: %X)", uigStatus);
			}

		case IPRIO_DB:
			/* bei dieser Prioritaet in die Datenbank schreiben  */
			if (uigStatus & IDATA_TO_DB)
			{
				if ((ilRC = WriteToDB()) != RC_SUCCESS)
				{
					dbg(TRACE,"<MsgHandler> Can't write to DB %d", ilRC);
				}
			}
			else
			{
				dbg(DEBUG,"<MsgHandler>: Prio-DB-Failure (Status: %X)", uigStatus);
			}

		case IPRIO_ASCII:
			/* bei dieser Prioritaet ins ASCII-File schreiben */
			if (uigStatus & IDATA_TO_ASCII)
			{
				if ((ilRC = WriteToFile()) != RC_SUCCESS)
				{
					dbg(TRACE,"<MsgHandler> Can't write to file %d", ilRC);
				}
			}
			else
			{
				dbg(DEBUG,"<MsgHandler>: Prio-Ascii-Failure (Status: %X)", uigStatus);
			}
			break;

		case IPRIO_NOTHING:
			/* bei dieser Prioritaet nichts mit den Daten machen */
			dbg(DEBUG,"<MsgHandler>: Prio-nothing (Status: %X)",
				uigStatus);
			break;

		default:
			/* undefinierte Prioritaet */
			dbg(DEBUG,"<MessageHandler> ----- END -----");
			return IPRIO_FAIL;
	}

	dbg(DEBUG,"<MessageHandler> ----- END -----");
	/* und zurueck */
	return ilRC;
}

/*//////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////// Umbenennen der Ascii-Files
*/
static int CreateNewLogFile(void)
{
	int				i;
	int				*pilFp;
	unsigned int 	uilLogStatus;
	char				*pclFName;
	char				pclTmpBuf[IMIN_BUF];
	char				pclHeaderText[IMIN_BUF];


	for (i=0; i<ILOG_COUNTER; i++)
	{
		switch (i)
		{
			case 0:
				/* USER */
				/* debug info */
				dbg(DEBUG,"<CreateNewLogFile>: Create USER-LogFile (%X)",
								uigStatus);

				/* Logfile - Status */
				uilLogStatus = IUSER_LFOPEN;

				/* FilePointer */
				pilFp = &igUsrLog;			

				/* FileName */
				pclFName = pcgUsrLog;
				break;

			case 1:
				/* CONFLICT */
				/* debug info */
				dbg(DEBUG,"<CreateNewLogFile>: Create CONF-LogFile (%X)",
								uigStatus);

				/* Logfile - Status */
				uilLogStatus = ICONF_LFOPEN;

				/* FilePointer */
				pilFp = &igConfLog;			

				/* FileName */
				pclFName = pcgConfLog;
				break;

			case 2:
				/* ERROR - Primary device */
				/* debug info */
				dbg(DEBUG,"<CreateNewLogFile>: Create PRIM-LogFile (%X)",
								uigStatus);

				/* Logfile - Status */
				uilLogStatus = IPRIM_LFOPEN;

				/* FilePointer */
				pilFp = &igPrimLog;			

				/* FileName */
				pclFName = psptrec->pmsgdev;
				break;

			case 3:
				/* ERROR - Secondary device */
				/* was passiert denn falls das secondary devices ein tty
				o.ae. ist -> dann koennen wir uns das ganze hier sparen
				-> noch klaeren
				bis dahin machen wir hier nix weiter
				*/
				continue;
				/********************************************
				* debug info *
				dbg(DEBUG,"<CreateNewLogFile>: Create SECO-File (%X)",
								uigStatus);

				* Logfile - Status *
				uilLogStatus = ISECO_LFOPEN;

				* FilePointer *
				pilFp = &igSecoLog;			

				* FileName *
				pclFName = psptrec->smsgdev;
				break;
				*********************************************/

			default:
				return ILOG_FAIL;	
		}
		
		/* gucken welche Ascii-Files geoeffnet sind
		*/
		if (uigStatus & uilLogStatus)
		{
			/* Logfile ist geoeffnet */
			if (close (*pilFp) == -1)
			{
				dbg(TRACE,"<CreateNewLogFile>: Can't close file(%d),[%s]",
					errno, strerror(errno));
			}
			else
			{
				/* Status (zurueck)setzen
				*/
				uigStatus &= ~uilLogStatus;

				/* Filenamen umbennen 
				*/
				memset ((void*)pclTmpBuf, 0x00, IMIN_BUF);
				strcpy (pclTmpBuf, pclFName);
				strcat (pclTmpBuf, SLOG_EXTENSION); 
				if (rename (pclFName, pclTmpBuf) == -1)
				{
					/* cannot rename file - debug message 
					*/
					dbg(TRACE,"<CreateNewLogFile>: Can't rename Log-File(%d),[%s]",
						errno, strerror(errno));
				}

				/* neues File oeffen 
				*/
				if ((*pilFp = open (pclFName, 
								  O_RDWR | O_CREAT | O_APPEND, IPERMS)) == -1)
				{
					/* Can't open USER-Logfile */
					dbg(TRACE,"<CreateNewLogFile>: Can't open Log-File(%d),[%s]",
						errno, strerror(errno));
				}
				else
				{
					/* das oeffnen des UserLogFiles hat geklappt 
					-> Status setzen 
					*/
					uigStatus |= uilLogStatus;

					/* gucken ob das File schon da ist und falls nicht den 
					Header schreiben
					*/
					if (lseek(*pilFp, 0L, SEEK_END) == 0)
					{
						memset ((void*)pclHeaderText, 0x00, IMIN_BUF);
						sprintf(pclHeaderText, SHEADER_TEXT, 
								pclFName,
								ddmmyy_strlocaltimestamp(),
								hhmmss_strlocaltimestamp());
						write (*pilFp, pclHeaderText, strlen(pclHeaderText)); 	
					}
				}
			}
		}
	}

	/* was soll ich denn hier sinnvolles zurueckliefern? */
	return RC_SUCCESS;
}

/*//////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////// Zusammensetzen des Textes mit beliebiger Formatierung 
*/
static int CreateMessageText(char **pcpOut, char *pcIn, 
							 char *pcPara,  char cSeparator)
{
	int			ilPosition;
	char			*pclP;
	char			*pclPa;
	static char	pclOut[IMAX_BUF];

	ilPosition = 0;
	memset((void*)pclOut, 0x00, IMAX_BUF);
	for (pclP=pcIn, pclPa=pcPara; *pclP; pclP++)
	{
		if (*pclP != '%')
			pclOut[ilPosition++] = *pclP;
		else
		{
			pclP++;
			for (; *pclPa; pclPa++)
			{
				if (*pclPa == cSeparator)
				{
					while (*pclPa == cSeparator) 
						pclPa++;
					break;
				}
				else
					pclOut[ilPosition++] = *pclPa;
			}
		}
	}
	*pcpOut = pclOut;
	return 0;
}

/*//////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
//////////  Ins Ascii-File schreiben
*/
static int WriteToFile(void)
{
	int				i;
	int				ilFound;
	int				ilRC = RC_SUCCESS;
	int				*pilLog;
	short				slLocalCursor;
	unsigned int	uilFileOpen;
	unsigned int	uilDBMsgTxt;
	char				*pclMessageText;
	char				pclText[IMAX_BUF];
	char				pclSqlBuffer[ISQL_BUFFER_SIZE];
	char				pclDataArea[IDATA_AREA_SIZE];
	char				pclMessageTyp[IMESSAGE_TYP_SIZE];
	Msg				*prlMsg;

	dbg(DEBUG,"<WriteToFile> ----- START -----");

	switch (prgMsgInfo->msg_origin)
	{
		case IUSER_MSG:
			uilFileOpen = IUSER_LFOPEN; 	
			uilDBMsgTxt = IUSER_DB_MSGTXT;
			prlMsg      = &rgUsrMsgTxt;
			pilLog		= &igUsrLog;
			memset((void*)pclMessageTyp, 0x00, IMESSAGE_TYP_SIZE);
			strcpy(pclMessageTyp, SUSR_MESSAGE_TYP);
			break;

		case ICONFLICT_MSG:
			uilFileOpen = ICONF_LFOPEN; 	
			uilDBMsgTxt = ICONF_DB_MSGTXT;
			prlMsg      = &rgConfMsgTxt;
			pilLog		= &igConfLog;
			memset((void*)pclMessageTyp, 0x00, IMESSAGE_TYP_SIZE);
			strcpy(pclMessageTyp, SCONF_MESSAGE_TYP);
			break;

		case IERROR_MSG:
			uilFileOpen = IPRIM_LFOPEN; 	
			uilDBMsgTxt = IERROR_DB_MSGTXT;
			prlMsg      = &rgErrorMsgTxt;
			pilLog		= &igPrimLog;
			memset((void*)pclMessageTyp, 0x00, IMESSAGE_TYP_SIZE);
			strcpy(pclMessageTyp, SERROR_MESSAGE_TYP);
			break;

		default:
			dbg(TRACE,"<WriteToFile> unknown msg-origin....");
			dbg(DEBUG,"<WriteToFile> ----- END -----");
			return IORIGIN_FAIL;
	}

	/* gucken ob Ascii-logFile offen ist */
	if (uigStatus & uilFileOpen)
	{
		/* hier ist das File offen und wir gucken jetzt wo wir
		die Messagetexte herbekommen
		*/
		if (uigStatus & uilDBMsgTxt)
		{
			/* Messagetexte aus der DB */
			dbg(DEBUG,"Get MessageText from DB");
			memset((void*)pclSqlBuffer, 0x00, ISQL_BUFFER_SIZE);
			memset((void*)pclDataArea, 0x00, IDATA_AREA_SIZE);
			sprintf(pclSqlBuffer,"select mete from %s where meno='%d' and laco='%s' and mety='%s'", pcgMessageTextTableName, prgMsgInfo->msgblk.msg_no, prgMsgInfo->lang_code, pclMessageTyp); 
			dbg(DEBUG,"<WriteToFile> SQL-Buffer %s", pclSqlBuffer);
			slLocalCursor = 0;
			if ((ilRC = sql_if(START|REL_CURSOR, &slLocalCursor, 
						       pclSqlBuffer, pclDataArea)) != RC_SUCCESS)
			{
				dbg(TRACE,"<WriteToFile> Sql_if error %d", ilRC);
			}
			else
			{
				/* Daten aus dem DataArea holen */
				memset((void*)pclText, 0x00, IMAX_BUF);	
				get_fld(pclDataArea, 0, STR, IDATA_AREA_SIZE, pclDataArea);

				/* Messagetext aufbereiten */
				/* we found the message text */
				CreateMessageText(&pclMessageText, 
								   pclDataArea,
								   prgMsgInfo->msgblk.msg_data,
								   CDATA_SEPARATOR);

				/* write the Text to File */
				memset ((void*)pclText, 0x00, IMAX_BUF);
				sprintf(pclText, SFILE_TEXT, 
						ddmmyy_strlocaltimestamp(),
						hhmmss_strlocaltimestamp(),
						prgMsgInfo->prog_name,
						prgMsgInfo->msgblk.msg_no,
						pclMessageText);
				write (*pilLog, pclText, strlen(pclText)); 	

				/* gucken ob secondary device geoeffnet ist und ob error
				message ...
				*/
				if (prgMsgInfo->msg_origin == IERROR_MSG && 
				   (uigStatus & ISECO_LFOPEN))
				{
					write (igSecoLog, pclText, strlen(pclText));
				}

				/* NEW: send text to UniCenter */
				if (igUseUniCenter == TRUE)
					WriteToUniCenter(pclMessageText, prgMsgInfo->msgblk.msg_no);
			}
		}
		else
		{
			/* Messagetexte aus dem Ascii-File */
			dbg(DEBUG,"Get MessageText from File");
			for (i=0, ilFound=0; i<prlMsg->iAnzahl; i++)
			{
				/* suchen nach dem text der entsprechenden 
				Messagenumber...
				*/
				if ((prlMsg->prMsgText[i].iMsgNO == prgMsgInfo->msgblk.msg_no) && !strcmp(prlMsg->prMsgText[i].pcLaCo, prgMsgInfo->lang_code))
				{
					ilFound = 1;
					break;
				}
			}
			
			if (ilFound)
			{
				/* we found the message text */
				CreateMessageText(&pclMessageText, 
								   prlMsg->prMsgText[i].pcText,
								   prgMsgInfo->msgblk.msg_data,
								   CDATA_SEPARATOR);

				/* write the Text to File */
				memset ((void*)pclText, 0x00, IMAX_BUF);
				sprintf(pclText, SFILE_TEXT, 
						ddmmyy_strlocaltimestamp(),
						hhmmss_strlocaltimestamp(),
						prgMsgInfo->prog_name,
						prgMsgInfo->msgblk.msg_no,
						pclMessageText);
				write (*pilLog, pclText, strlen(pclText)); 	

				/* gucken ob secondary device geoeffnet ist und ob error
				message ...
				*/
				if (prgMsgInfo->msg_origin == IERROR_MSG && 
				   (uigStatus & ISECO_LFOPEN))
				{
					write (igSecoLog, pclText, strlen(pclText));
				}

				/* NEW: send text to UniCenter */
				if (igUseUniCenter == TRUE)
					WriteToUniCenter(pclMessageText, prgMsgInfo->msgblk.msg_no);
			}
			else
			{
				dbg(TRACE,"<WriteToFile> Can't find message text");
			}
		}
	}
	else
	{
		dbg(TRACE,"<WriteToFile> User-Logfile not open");
	}

	dbg(DEBUG,"<WriteToFile> ----- END -----");
	/* und tschuess */
	return  ilRC;
}

/*//////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
//////////  In die Datenbank schreiben
*/
static int WriteToDB(void)
{
	int				i;
	int				ilRC    = RC_SUCCESS;
	int				ilFound = 0;
	int				ilNextUrno;
	int				ilOldDbgLvl;
	short				slLocalCursor;
	unsigned int	uilDBMsgTxt;
	char				pclMessageTyp[IMESSAGE_TYP_SIZE];
	char				pclSqlBuffer[ISQL_BUFFER_SIZE];
	char				pclDataArea[IDATA_AREA_SIZE];
	Msg				*prlMsg;

	dbg(DEBUG,"<WriteToDB> ----- START -----");

	if (uigStatus & IDB_CONNECT)
	{
		/* DB-Verbindung ist hergestellt */
		/* Welche Art von Message ist es denn? */
		switch (prgMsgInfo->msg_origin)
		{
			case IUSER_MSG:
				uilDBMsgTxt = IUSER_DB_MSGTXT;
				prlMsg      = &rgUsrMsgTxt;
				memset((void*)pclMessageTyp, 0x00, IMESSAGE_TYP_SIZE);
				strcpy(pclMessageTyp, SUSR_MESSAGE_TYP);
				break;

			case ICONFLICT_MSG:
				uilDBMsgTxt = ICONF_DB_MSGTXT;
				prlMsg      = &rgConfMsgTxt;
				memset((void*)pclMessageTyp, 0x00, IMESSAGE_TYP_SIZE);
				strcpy(pclMessageTyp, SCONF_MESSAGE_TYP);
				break;

			case IERROR_MSG:
				uilDBMsgTxt = IERROR_DB_MSGTXT;
				prlMsg      = &rgErrorMsgTxt;
				memset((void*)pclMessageTyp, 0x00, IMESSAGE_TYP_SIZE);
				strcpy(pclMessageTyp, SERROR_MESSAGE_TYP);
				break;

			default:
				dbg(TRACE,"<WriteToDB> unknown msg-origin...");
				dbg(DEBUG,"<WriteToDB> ----- END -----");
				return IORIGIN_FAIL;
		}

		/* gucken von wo wir die Messagetexte holen muessen */
		if (uigStatus & uilDBMsgTxt)
		{
			/* Messagetext aus der DB */
			memset((void*)pclSqlBuffer, 0x00, ISQL_BUFFER_SIZE);
			memset((void*)pclDataArea,  0x00, IDATA_AREA_SIZE);
			sprintf(pclSqlBuffer,"select mete from %s where meno='%d' and laco='%s' and mety='%s'", pcgMessageTextTableName, prgMsgInfo->msgblk.msg_no, prgMsgInfo->lang_code, pclMessageTyp); 
			dbg(DEBUG,"<WriteToDB> SQL-Buffer <%s>", pclSqlBuffer);
			slLocalCursor = 0;
			if ((ilRC = sql_if(START|REL_CURSOR, &slLocalCursor, 
						       pclSqlBuffer, pclDataArea)) != RC_SUCCESS)
			{
				/* hier ist der Messagetext nicht da -> FEHLER 
				*/
				dbg(TRACE,"<WriteToDB> Sql_if error %d", ilRC);
			}
			else
			{
				/* Found message text in db */
				ilFound = 1;
			}
		}
		else
		{
			/* Messagetext aus den Ascii-Files */
			for (i=0; i<prlMsg->iAnzahl; i++)
			{
				/* suchen nach dem text der entsprechenden 
				Messagenumber...
				*/
				if ((prlMsg->prMsgText[i].iMsgNO == prgMsgInfo->msgblk.msg_no) && !strcmp(prlMsg->prMsgText[i].pcLaCo, prgMsgInfo->lang_code))
				{
					ilFound = 1;
					break;
				}
			}
		}

		if (ilFound)
		{
			/* get current system state... */
			ilOldDbgLvl = debug_level;
			debug_level = TRACE;
			ctrl_sta = get_system_state();
			debug_level = ilOldDbgLvl;

			/* check current state */
			if (ctrl_sta == HSB_STANDBY)
			{
				dbg(TRACE,"<WriteToDB> System state has switched to HSB_STANDBY");
				return RC_FAIL;
			}

			/* try to Get next urno */
			if (GetNextUrno(&ilNextUrno) != RC_SUCCESS)
			{
				dbg(TRACE,"<WriteToDB> GetNextUrno-Error");
			}
			else
			{
				/* hier hat das kreieren einer neuen Urno geklappt */
				/* Message in die Datenbank schreiben */
				/* Sql-Statement zusammensetzen */
				memset ((void*)pclSqlBuffer, 0x00, ISQL_BUFFER_SIZE);
				sprintf(pclSqlBuffer, "insert into %s (%s,%s,%s,%s,%s,%s,%s) values (:V1,:V2,:V3,:V4,:V5,:V6,:V7)", pcgLogTableName, "URNO","MENO","DATA","TIST","PRIO","PRNA","METY");
					
				dbg(DEBUG,"<WriteToDB> SQL-Buffer %s", pclSqlBuffer);
				/* Data-Area zusammensetzen */
				memset ((void*)pclDataArea, 0x00, IDATA_AREA_SIZE);
				sprintf(pclDataArea,"%d,%d,%s,%s,%d,%s,%s",
						ilNextUrno,
						prgMsgInfo->msgblk.msg_no, 
						prgMsgInfo->msgblk.msg_data,
						GetLocalTimeStamp(), 
						prgMsgInfo->msgblk.prio,
						prgMsgInfo->prog_name,
						pclMessageTyp);

				dbg(DEBUG,"<WriteToDB> DataArea: %s", pclDataArea);

				/* Delimiter to NULL */
				delton(pclDataArea);
								
				/* Sql-Statement absetzen */
				slLocalCursor = 0;

				/* get current system state... */
				ilOldDbgLvl = debug_level;
				debug_level = TRACE;
				ctrl_sta = get_system_state();
				debug_level = ilOldDbgLvl;

				/* check current state */
				if (ctrl_sta == HSB_STANDBY)
				{
					dbg(TRACE,"<WriteToDB> System state has switched to HSB_STANDBY");
					return RC_FAIL;
				}

				if ((ilRC = sql_if(START|REL_CURSOR|COMMIT, &slLocalCursor, 
								   pclSqlBuffer, pclDataArea)) != RC_SUCCESS)
				{
					/* command not performed */
					dbg(TRACE,"<WriteToDB> Sql_if error %d, can't write to DB",ilRC);

					/* rollback now */
					rollback();
				}
				else
				{
					/* hier hat der INSERT geklappt */
					dbg(DEBUG,"<WriteToDB> insert successfull");
				}
			}
		}
		else
		{
			dbg(TRACE,"<WriteToDB> Can't find message text");
		}
	}
	else
	{
		/* keine Verbindung zur DB */
		dbg(TRACE,"<WriteToDB> DataBase not connected");
		dbg(DEBUG,"<WriteToDB> ----- END -----");
		return RC_FAIL;
	}
	
	dbg(DEBUG,"<WriteToDB> ----- END -----");
	/* und tschuess */
	return ilRC;
}

/*//////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
////////// die naechste Urno ziehen 
*/
static int GetNextUrno(int *pipUrno)
{
	int				ilRC;
	int				ilOldDbgLvl;
	char				pclDataArea[IDATA_AREA_SIZE];
	
	/* clear buffer */
	memset((void*)pclDataArea, 0x00, IDATA_AREA_SIZE);

	/* get the next... */
	ilOldDbgLvl = debug_level;
	debug_level = TRACE;
	if ((ilRC = GetNextValues(pclDataArea, 1)) != RC_SUCCESS)
	{
		dbg(TRACE,"GetNextValues returns: %d", ilRC);
		Terminate(0);
	}
	debug_level = ilOldDbgLvl;

	/* convert to integer */
	*pipUrno = atoi(pclDataArea);

	/* hier ist alles prima */
	return RC_SUCCESS;
}

/*//////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
//////////  Texte an Application (bei externer Message)
*/
static char *AppText(int ipErrorCode)
{
	switch (ipErrorCode)
	{
		case RC_SUCCESS:
			return "OK";
		case IPRIO_FAIL:
			return "prio failure";
		case IFIELD1_FAIL:
			return "missing field 1";
		case IFIELD2_FAIL:
			return "missing field 2";
		case IFIELD3_FAIL:
			return "missing field 3";
		case IFIELD4_FAIL:
			return "missing field 4";
		case IFIELD5_FAIL:
			return "missing field 5";
		case IFIELD6_FAIL:
			return "missing field 6";
		case IFIELD7_FAIL:
			return "missing field 7";
		case IFIELD8_FAIL:
			return "missing field 8";
		case IFIELD9_FAIL:
			return "missing field 9";
		case IMSGNO_FAIL:
			return "message number failure";
		case ICOMMAND_FAIL:
			return "unknown command";
		case IORIGIN_FAIL:
			return "origin failure";
		default: 
			return "unknown error code";	
	}
}

/*//////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
//////////
*/
static char *GetLocalTimeStamp(void)
{
	static char pclTime[iTIME_SIZE];
	time_t		  _llCurTime;
	struct tm	  *_tm;
	struct tm	  prlCurTime;
/***********
	struct tm	  *prlCurTime;
************/
  
	memset((void*)pclTime, 0x00, iTIME_SIZE);
	_llCurTime = time(0L);
  _tm = (struct tm *) localtime(&_llCurTime);
  if (_tm != NULL)
  {
    prlCurTime = *_tm;
    if (_llCurTime > 0)
    {
       strftime(pclTime,15,"%" "Y%" "m%" "d%" "H%" "M%" "S", &prlCurTime);
    }
    else
    {
       dbg(TRACE,"GetLocalTimeStamp: _llCurTime is %ld", _llCurTime);
    }
  }
  else
  {
     dbg(TRACE,"GetLocalTimeStamp: localtime %ld failed",
                _llCurTime);
  }
/****************
	prlCurTime = (struct tm *) localtime(&_llCurTime);
	strftime(pclTime,15,"%" "Y%" "m%" "d%" "H%" "M%" "S",prlCurTime);
*****************/

	return pclTime;	
} 

/*//////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
//////////
*/
static char *ddmmyy_strlocaltimestamp(void)
{
  static char timebuf[9];
  time_t	    _llCurTime;
  struct tm   *_tm;
  struct tm	  CurTime;
/***********
  struct tm	  *CurTime;
************/

  timebuf[0] = '\0';
  _llCurTime = time(0L);
  _tm = (struct tm *) localtime(&_llCurTime);
  if (_tm != NULL)
  {               
     CurTime = *_tm;
     if (_llCurTime > 0)
     {  
         strftime(timebuf,9,"%" "d/%" "m/%" "y",&CurTime);
     }
     else
     {
        dbg(TRACE,"ddmmyy_strlocaltimestamp: _llCurTime is %ld",
                   _llCurTime);
     }
  }
  else
  {
     dbg(TRACE,"ddmmyy_strlocaltimestamp: localtime %ld failed",
                _llCurTime);
  }
/*********************
     CurTime = (struct tm *) localtime(&_CurTime);
     strftime(timebuf,9,"%" "d/%" "m/%" "y",CurTime);
**********************/

  return(timebuf);
}

/*//////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
//////////
*/
static char *hhmmss_strlocaltimestamp(void)
{
  static char timebuf[9];
  time_t	    _llCurTime;
  struct tm   *_tm;
  struct tm	  CurTime;
/***********
  struct tm	  *CurTime;
************/

  timebuf[0] = '\0';
  _llCurTime = time(0L);
  _tm = (struct tm *) localtime(&_llCurTime);
  if (_tm != NULL)
  {
     CurTime = *_tm;
     if (_llCurTime > 0)
     {
         strftime(timebuf,9,"%" "H:%" "M:%" "S",&CurTime);
     }
     else
     {
        dbg(TRACE,"hhmmss_strlocaltimestamp: _llCurTime is %ld",
                   _llCurTime);
     }
  }
  else
  {
     dbg(TRACE,"hhmmss_strlocaltimestamp: localtime %ld failed",
                _llCurTime);
  }
/********************  
  CurTime = (struct tm *) localtime(&_CurTime);
  strftime(timebuf,9,"%" "H:%" "M:%" "S",CurTime);
********************/

  return(timebuf);
}

/*//////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
//////////
*/
static void WriteToUniCenter(char *pcpMessageText, int ipMsgNo)
{
	int	ilCurMsgNo;

	for (ilCurMsgNo=0; ilCurMsgNo<igNoOfUniCenterMsg; ilCurMsgNo++)
		if (ipMsgNo == pigUniCenterMsgNo[ilCurMsgNo])
		{
			/* send it */
			dbg(DEBUG,"<WriteToUniCenter> sending: %d <%s>", ipMsgNo, pcpMessageText);
			(void)SendMsgToUniIf(pcpMessageText);
			break;
		}

	/* bye babe */
	return;
}


/******************************************************************************/
/* Space for Appendings (Source-Code Control)                                 */
/******************************************************************************/
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
